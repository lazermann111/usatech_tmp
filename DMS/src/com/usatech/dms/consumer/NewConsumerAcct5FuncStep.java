/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.consumer;

import java.io.IOException;
import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usatech.cardnumbergenerator.CardFormat0Simple;
import com.usatech.cardnumbergenerator.CardFormat1Simple;
import com.usatech.cardnumbergenerator.CardFormat2Simple;
import com.usatech.cardnumbergenerator.CardFormat3Simple;
import com.usatech.dms.util.Helper;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.util.KeyMgrAccountUpdater;
import com.usatech.layers.common.util.StringHelper;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Base64;
import simple.results.Results;
import simple.security.SecureHash;
import simple.security.SecurityUtils;
import simple.servlet.BasicServletUser;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;
import simple.lang.sun.misc.CRC16;
import simple.text.StringUtils;

public class NewConsumerAcct5FuncStep extends AbstractStep
{
	private static final simple.io.Log log = simple.io.Log.getLog();
	
	public static final SimpleDateFormat dateTimeDDMMYYYYFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
	public static final SimpleDateFormat dateTimeYYYYMMDDFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
    	String action = form.getString("action", false);
    	String doc = form.getString("csv", false);
    	StringBuilder out = new StringBuilder("");
    	String sectionMarker = "";
    	try{
	    	if(!StringHelper.isBlank(action) && "Download CSV Database".equals(action)){
	    		if(StringHelper.isBlank(doc)){
	    			out.append(buildPrintError("CSV file not found!", request, form));
	    			request.setAttribute("card_gen_output", out);
	    			return;	    			
	        	}else{
	        		Helper.sendFileToClient(response, "data.csv.txt", Base64.decodeToString(doc).replace("\n", "\r\n"));
	                return;
	        	}
	    	}
    	}catch(Exception e){
    		log.error("Exception occurred processing csv file!", e);
    		out.append(buildPrintError("Exception occurred processing csv file! : " + e, request, form));
			request.setAttribute("card_gen_output", out);
			return;
    	}
    	
    	int rowCount = -1;
    	Object[] result = null;
    	Results results = null;
    	boolean success = false;
	    Connection conn = null;
	    try{
	    	int consumer_acct_fmt_id = form.getInt("consumer_acct_fmt_id", false, -1);
	    	if(consumer_acct_fmt_id < 0){
	    		out.append("Wrong Consumer Acct Fmt ID ").append(consumer_acct_fmt_id).append("\n");
				request.setAttribute("card_gen_output", out.toString());
				return;
	    	}
	    	String consumer_acct_fmt_name = form.getString("consumer_acct_fmt_name", false);
	    	int authority_id = form.getInt("authority_id", false, -1);
	    	long merchant_id = form.getLong("merchant_id", false, -1);
	    	String merchant_name = form.getString("merchant_name", false);
	    	String merchant_desc = form.getString("merchant_desc", false);
	    	String merchant_cd = form.getString("merchant_cd", false);
	    	long terminal_id = form.getLong("terminal_id", false, -1);
	    	String terminal_desc = form.getString("terminal_desc", false);
	    	long customer_id = form.getLong("customer_id", false, -1);
	    	long corp_customer_id = form.getLong("corp_customer_id", false, -1);
	    	String currency_cd = form.getStringSafely("currency_cd", "USD");
	    	if (StringHelper.isBlank(currency_cd))
	    		currency_cd = "USD";
	    	long consumer_id = form.getLong("consumer_id", false, -1);
	    	String customer_name = form.getString("customer_name", false);
	    	String corp_customer_name = form.getStringSafely("corp_customer_name", "");
	    	String location_id = form.getString("location_id", false);
	    	String location_name = form.getString("location_name", false);
	    	int num_cards = form.getInt("num_cards", false, -1);
	    	if(num_cards < 1){
	    		out.append("Invalid number of cards: ").append(num_cards).append("\n");
				request.setAttribute("card_gen_output", out.toString());
				return;
	    	}
	    	String exp = form.getString("exp", false);
	    	String exp_display = form.getString("exp_display", false);
	    	String deact_date = form.getString("deactivation_date", false);
	    	int consumer_acct_sub_type_id = form.getInt("consumer_acct_sub_type_id", false, -1);
	    	
	    	String deactivation_date = null;
	    	Date dt = null;
	    	try {
	    		dt = dateTimeYYYYMMDDFormat.parse(deact_date);
	    	} catch (ParseException e) {
	    		throw new ServletException(e); 
	    	}
	    	deactivation_date = dateTimeDDMMYYYYFormat.format(dt);
	
	    	String create_date = form.getString("create_date", false);
	    	String balance = form.getString("balance", false);
	    	
	    	conn = DataLayerMgr.getConnection("OPER");
	    	
	    	String consumer_acct_sub_type_desc = "";
	    	if (consumer_acct_sub_type_id > 0) {
		    	results = DataLayerMgr.executeQuery(conn, "GET_CONSUMER_ACCT_SUB_TYPE_INFO", new Object[] {consumer_acct_sub_type_id});
		    	if (results.next())
		    		consumer_acct_sub_type_desc = results.getFormattedValue("consumer_acct_sub_type_desc");
	    	}
	    	
	    	out.append("<div align=\"left\"><pre>CARD GENERATION LOG:\n\n");
	    	out.append("Creating cards...\n");
	    	out.append("Card Type: ").append(consumer_acct_fmt_name).append("\n");
	    	out.append("Card Subtype: ").append(consumer_acct_sub_type_desc).append("\n");
	    	out.append("Customer: ").append(customer_name).append("\n");
	    	if (!StringUtils.isBlank(corp_customer_name))
	    		out.append("Reporting Customer: ").append(corp_customer_name).append("\n");
	    	out.append("Location: ").append(location_name).append("\n");
	    	out.append("Using deactivation date: ").append(deact_date).append("\n");
	    	
	    	if(merchant_id == -1){	    		
	    		results = DataLayerMgr.executeQuery(conn, "GET_MERCHANT_BY_CUST_ID_AND_AUTH_ID", new Object[] {customer_id, authority_id});
	    		if (results != null && results.next()){
	    			merchant_id = results.getValue("merchant_id", long.class);
	    			merchant_name = results.getFormattedValue("merchant_name");
	    			merchant_desc = results.getFormattedValue("merchant_desc");
	    			merchant_cd = results.getFormattedValue("merchant_cd");
	    			terminal_id = results.getValue("terminal_id", long.class);
	    		}
	    	}
	    	
	    	if(merchant_id == -1){
	    		// # lookup next merchant_cd for selected authority
	    		sectionMarker = "New Merchant";
	    		out.append("Creating new merchant account\n");
	    		results = DataLayerMgr.executeQuery(conn, "GET_NEXT_MERCHANT_CD", new Object[] {authority_id});
	    		if(results != null && results.next()){
	    			long merchantNumber = results.getValue("merchant_cd", long.class);
	    			if (merchantNumber % 1000 == 0)
	    				merchantNumber++;
	    			merchant_cd = String.format("%03d", merchantNumber);
	    		}
	    		if(StringHelper.isBlank(merchant_cd)){
	    			out.append(buildPrintError(sectionMarker+" : Failed to lookup next merchant_cd for authority_id " + authority_id, request, form));
	    			request.setAttribute("card_gen_output", out.toString());
	    			return;
	    		}
	    		out.append("New merchant code: ").append(merchant_cd).append("\n");
	    		
	    		// # create merchant
	    		result = DataLayerMgr.executeCall(conn, "INSERT_MERCHANT", new Object[] {merchant_cd, ((!(StringHelper.isBlank(merchant_name)) && merchant_name.length() > 255) ? merchant_name.substring(0, 255) : merchant_name ), ((!(StringHelper.isBlank(merchant_desc)) && merchant_desc.length() > 255) ? merchant_desc.substring(0, 255) : merchant_desc), 
	    				"USA Technologies, Inc.", authority_id, "USA Technologies"});
				if(result!=null && ((BigDecimal)result[1]).intValue() > 0){
					merchant_id = ((BigDecimal)result[1]).intValue();
				}else{
					out.append(buildPrintError(sectionMarker+" : Failed to create merchant!", request, form));
	    			request.setAttribute("card_gen_output", out.toString());
	    			return;
				}
				
				out.append("Created new merchant: ").append(merchant_id).append("\n");
				
				// # create customer_merchant
				rowCount = DataLayerMgr.executeUpdate(conn, "INSERT_CUSTOMER_MERCHANT", new Object[] {customer_id, merchant_id});
				if(rowCount != 1){
					out.append(buildPrintError(sectionMarker+" : Failed to create customer_merchant (" + customer_id + ", " + merchant_id + ")!", request, form));
	    			request.setAttribute("card_gen_output", out.toString());
	    			return;
				}
				
				out.append("Created new customer_merchant: ").append(customer_id).append(", ").append(merchant_id).append("\n");
				
	    	}else{	    		
	    		out.append("Using existing merchant: id=").append(merchant_id).append(", cd=").append(merchant_cd).append("\n");
	    	}
	    	
	    	if(terminal_id < 0){
	    		sectionMarker = "New Terminal";
	    		// # create terminal
	    		result = DataLayerMgr.executeCall(conn, "INSERT_TERMINAL", new Object[] {"000", //terminal_cd
	    																					1, //terminal_next_batch_num 
	    																					merchant_id,
	    																					((!(StringHelper.isBlank(terminal_desc)) && terminal_desc.length() > 255) ? terminal_desc.substring(0, 255) : terminal_desc ),
	    																					1,// terminal_state_id
	    																					999, // terminal_max_batch_num
	    																					999, // terminal_batch_max_tran
	    																					1, // terminal_batch_min_tran
	    																					1, // terminal_batch_cycle_num
	    																					850, // terminal_min_batch_num
	    																					24, // terminal_min_batch_close_hr
	    																					25 // terminal_max_batch_close_hr
	    																					});
				if(result!=null && ((BigDecimal)result[1]).intValue() > 0){
					terminal_id = ((BigDecimal)result[1]).intValue();
				}else{
					out.append(buildPrintError(sectionMarker+" : Failed to create terminal!", request, form));
	    			request.setAttribute("card_gen_output", out.toString());
	    			return;
				}
				
				out.append("Created new terminal: ").append(terminal_id).append("\n");
	    	}else{
	    		out.append("Using existing terminal ").append(terminal_id).append("\n");
	    	}
	    	
	    	// # lookup consumer using consumer_accounts linked to merchant via merchant_consumer_acct
	    	
	    	String consumer_email_addr = new StringBuilder("merchant_").append(merchant_id).append("@usatech.com").toString();
	    	
	    	results = DataLayerMgr.executeQuery(conn, "GET_CONSUMER_ID_BY_EMAIL_ADDR", new Object[]{consumer_email_addr});
	    	if(results != null && results.next()){
	    		sectionMarker = "Existing Consumer";
	    		consumer_id = new BigDecimal(results.getFormattedValue("consumer_id")).intValue();
	    		
	    		out.append("Using existing consumer ").append(consumer_id).append("\n");
	    	}else{
	    		sectionMarker = "Existing Consumer";
	    		if(StringHelper.isBlank(merchant_name)){
	    			
	    	    	results = DataLayerMgr.executeQuery(conn, "GET_MERCHANT_NAME_BY_MERCHANT_ID", new Object[]{merchant_id});
	    	    	if(results != null && results.next()){
	    	    		merchant_name = results.getFormattedValue("merchant_name");
	    	    		
	    	    		out.append("Using merchant name ").append(merchant_name).append("\n");
	    	    	}
	    			
	    		}
	    		
	    		// # create new if none exists
	    		result = DataLayerMgr.executeCall(conn, "INSERT_CONSUMER", new Object[] {consumer_email_addr, //terminal_cd
	    																					"Virtual Consumer", //consumer_fname 
	    																					merchant_name, //consumer_lname
	    																					4 //consumer_type_id
	    																					});
				if(result!=null && ((BigDecimal)result[1]).intValue() > 0){
					consumer_id = ((BigDecimal)result[1]).intValue();
				}else{
					out.append(buildPrintError(sectionMarker+" : Failed to create consumer " + consumer_email_addr + "!", request, form));
	    			request.setAttribute("card_gen_output", out.toString());
	    			return;
				}
	    		
				out.append("Created new consumer: ").append(consumer_id).append(" - ").append(consumer_email_addr).append("\n");
	    	}
	    		    	
	    	String consumer_acct_sql = "SELECT last_consumer_acct_cd FROM pss.consumer WHERE consumer_id = CAST (? AS numeric) AND last_consumer_acct_cd IS NOT NULL";	    	
	    	results = DataLayerMgr.executeSQL(conn, consumer_acct_sql, new Object[]{consumer_id}, null);
	    	String last_consumer_acct_cd = null;
	    	if(results != null && results.next()){
	    		last_consumer_acct_cd = results.getFormattedValue("last_consumer_acct_cd");
	    		out.append("Consumer accounts exist, last consumer account code: ").append(last_consumer_acct_cd).append("\n");
	    	}else
	    		out.append("No consumer accounts exist\n");
	    	
	    	String dtNow = dateTimeDDMMYYYYFormat.format(Calendar.getInstance().getTime());
			java.sql.Timestamp tsNow = null;
			java.sql.Timestamp tsDeact = null;
			try {
				tsNow = new java.sql.Timestamp((dateTimeDDMMYYYYFormat.parse(dtNow)).getTime());
			} catch(Exception e) {
			}
			try {
				tsDeact = new java.sql.Timestamp((dateTimeDDMMYYYYFormat.parse(deactivation_date)).getTime());
			} catch(Exception e) {
			}
	    	
	    	Map<String, Object> params = new HashMap<String, Object>();
			params.put("consumer_acct_active_yn_flag", "Y");
			params.put("consumer_acct_balance", StringHelper.isBlank(balance) ? 0 : balance);
			params.put("consumer_id", consumer_id);
			params.put("customer_id", customer_id);
			params.put("location_id", location_id);
			params.put("consumer_acct_issue_num", 1);
			params.put("payment_subtype_id", 1); // payment_subtype_id is deprecated but required
			params.put("consumer_acct_activation_ts", tsNow);
			params.put("consumer_acct_deactivation_ts", tsDeact);
			params.put("currency_cd", currency_cd);
			params.put("consumer_acct_fmt_id", consumer_acct_fmt_id);
			params.put("corp_customer_id", corp_customer_id == -1 ? null : corp_customer_id);
	    	
	    	List<String[]> cards_arr = new ArrayList<String[]>();
	    	long consumer_acct_id = -1;
	    	int groupID = Integer.parseInt(merchant_cd.substring(merchant_cd.length() - 3));
	    	int numGeneratedCards = 0;
	    	SecureRandom random = SecurityUtils.getSecureRandom();
			KeyMgrAccountUpdater keyMgrUpdater = new KeyMgrAccountUpdater();
			boolean okay = false;
			try {
		    	if(consumer_acct_fmt_id == 0){
		    		// Gift/Stored Value Card
		    		sectionMarker = "consumer_acct_fmt_id0";
		    		int lostCardNum = 0;
		    		
		    		CardFormat0Simple generator = new CardFormat0Simple(exp, last_consumer_acct_cd, groupID, lostCardNum, num_cards);
		    		
		    		out.append("Calling card generator: CardFormat0Simple ").append(exp).append(" ").append(last_consumer_acct_cd).append(" ").append(groupID).append(" ").append(lostCardNum).append(" ").append(num_cards).append("\n");
		    			    		
		    		long cardId = newFirstConsumerAcctIdentifier(num_cards);
		    		while (numGeneratedCards < num_cards) {	    				    		
			    		String card = generator.getNextCardData(); 	    			
			    				    				    			
		    			String[] fields = card.split(",");
		    			String magstripe = fields[0];
		    			String consumer_acct_cd = fields[1];
		    			//String account_id = fields[2];
		    			String vv = fields[3];
		    			String lostCardCD = fields[4];
		    			String security_cd = SecurityUtils.getRandomString(random, "0123456789", 4);
		    			
						consumer_acct_id = insertConsumerAcct(conn, request, form, params, merchant_id, out, magstripe, consumer_acct_cd, vv, lostCardCD, "0", 2, security_cd, random, cardId, consumer_acct_sub_type_id, keyMgrUpdater);
		    			
		    			String cardNumber = magstripe.substring(0, magstripe.indexOf('='));
		    			if(consumer_acct_id < 0){
		    				//error message already attached
		    				return;
		    			} else if (consumer_acct_id > 0) {
		    				cards_arr.add(new String[]{magstripe, cardNumber, StringUtils.formatCardNumberForDisplay(cardNumber), security_cd, String.valueOf(cardId), exp, exp_display, create_date, consumer_acct_fmt_name, customer_name, location_name, consumer_acct_sub_type_desc});
		    				out.append("Card: ").append(magstripe).append(", Security Code: ").append(security_cd).append(", Card ID: ").append(cardId).append("\n");
		    				numGeneratedCards++;
		    				cardId++;
		    			} else
		    				out.append("Card ").append(cardNumber).append(" already exists!\n");
		    		}
						okay = true;
		    	}else if(consumer_acct_fmt_id == 1){
		    		// Operator/Driver Maintenance Card
		    		sectionMarker = "consumer_acct_fmt_id1";
		    		
		    		String deviceTypes = ConvertUtils.getStringSafely(request.getAttribute("device_types"), "");
					String[] deviceTypesArray = deviceTypes.split(",");
					long[] permissionActionArray = new long[deviceTypesArray.length];
					int device_type_action_cd = -1;
	
		        	for (int i = 0; i < deviceTypesArray.length; i++) {
		        		String device_type_id = deviceTypesArray[i];
		        		int action_id = ConvertUtils.getInt(request.getAttribute(new StringBuilder("action_").append(device_type_id).toString()));
		        		
						if (device_type_action_cd == -1) {
				    		// # lookup device_type_action.device_type_action_cd to encode on card
				        	results = DataLayerMgr.executeQuery(conn, "GET_DEVICE_TYPE_ACTION_BY_ACTION_ID_AND_DEVICE_TYPE_ID", new Object[]{action_id, device_type_id});		        	
				        	if(results != null && results.next()){
				        		device_type_action_cd = ConvertUtils.getInt(results.get("device_type_action_cd"));
				        		out.append("Encoding device_type_action_cd ").append(device_type_action_cd).append(" on cards\n");
				        	}else{
				        		out.append(buildPrintError(sectionMarker+" : Failed to lookup device_type_action_cd for " + action_id + ", " + device_type_id + "!", request, form));
				    			request.setAttribute("card_gen_output", out.toString());
				    			return;
				        	}
						}
						
						String action_params = ConvertUtils.getStringSafely(request.getAttribute(new StringBuilder("action_").append(device_type_id).append("_params").toString()), "");		        	
		        		
			        	long permission_action_id = -1;
			        	long last_permission_action_id = -1;
			        	long this_permission_action_id = -1;
			        	StringBuilder permission_action_params = new StringBuilder();		        	
			        	
			        	// # lookup existing permission_actions for selected action
			        	results = DataLayerMgr.executeQuery(conn, "GET_PERMISSION_ACTION_INFO", new Object[]{action_id});
			        	if (results != null) {
				        	while(results.next()){
				        		this_permission_action_id = ConvertUtils.getLong(results.get("permission_action_id"));
				        		if (last_permission_action_id == -1)
				        			last_permission_action_id = this_permission_action_id;
				        		else if (last_permission_action_id != this_permission_action_id) {
			        				if (permission_action_params.toString().equalsIgnoreCase(action_params)) {
			        					permission_action_id = last_permission_action_id;
			        					break;
			        				}
				        			last_permission_action_id = this_permission_action_id;
				        			permission_action_params.setLength(0);
				        		}
				        		if (permission_action_params.length() > 0)
				        			permission_action_params.append(",");
				        		permission_action_params.append(results.getFormattedValue("action_param_id"));
				        	}
				        	if (permission_action_id == -1 && permission_action_params.toString().equalsIgnoreCase(action_params))
	        					permission_action_id = this_permission_action_id;
			        	}
			        	
			        	if(permission_action_id != -1){
			        		// # we found a matching permission_action
			        		out.append("Using existing permission action ").append(permission_action_id).append(" with device_type_id: ").append( device_type_id).append(", action_id: ").append(action_id).append(" and action_params: ").append(action_params).append("\n");
			        	}else{
			        		result = DataLayerMgr.executeCall(conn, "INSERT_PERMISSION_ACTION", new Object[] {action_id});
			    			if(result!=null && ((BigDecimal)result[1]).longValue() > 0){
			    				permission_action_id = ((BigDecimal)result[1]).longValue();		    				
			    			}else{
			    				out.append(buildPrintError(sectionMarker+" : Failed to create permission_action for action_id " + action_id + "!", request, form));
			        			request.setAttribute("card_gen_output", out.toString());
			        			return;
			    			}
			    			if (action_params != null && action_params.length() > 0) {
				    			int counter = 0;
				    			String[] action_params_array = action_params.split(",");
				    			for(String action_param_id : action_params_array){		    				
				    				rowCount = DataLayerMgr.executeUpdate(conn, "INSERT_PERMISSION_ACTION_PARAM", new Object[] {permission_action_id, action_param_id, counter++});
				    				if(rowCount != 1){
				    					out.append(buildPrintError(sectionMarker+" : Failed to create permission_action_param  (" + permission_action_id + ", " + action_param_id + ")!", request, form));
				    	    			request.setAttribute("card_gen_output", out.toString());
				    	    			return;
				    				}
				    			}
			    			}
			    			out.append("Created new permission_action ").append(permission_action_id).append(" with device_type_id: ").append( device_type_id).append(", action_id: ").append(action_id).append(" and action_params: ").append(action_params).append("\n");
			        	}
			        	
			        	permissionActionArray[i] = permission_action_id;
		        	}
		    		
		    		CardFormat1Simple generator = new CardFormat1Simple(exp, last_consumer_acct_cd, groupID, device_type_action_cd, num_cards);
		    		
		    		out.append("Calling card generator: CardFormat1Simple ").append(exp).append(" ").append(last_consumer_acct_cd).append(" ").append(groupID).append(" ").append(device_type_action_cd).append(" ").append(num_cards).append("\n");	    		
		    		
		    		long cardId = newFirstConsumerAcctIdentifier(num_cards);
		    		while (numGeneratedCards < num_cards) {
		    			String card = generator.getNextCardData();
		    			
		    			String[] fields = card.split(",");
		    			String magstripe = fields[0];
		    			String consumer_acct_cd = fields[1];
		    			//String account_id = fields[2];
		    			String vv = fields[3];
		    			String security_cd = SecurityUtils.getRandomString(random, "0123456789", 4);
		    			
						consumer_acct_id = insertConsumerAcct(conn, request, form, params, merchant_id, out, magstripe, consumer_acct_cd, vv, null, "0", 1, security_cd, random, cardId, consumer_acct_sub_type_id, keyMgrUpdater);
		    			
		    			String cardNumber = magstripe.substring(0, magstripe.indexOf('='));
		    			if(consumer_acct_id < 0){
		    				//error message already attached
		    				return;
		    			}else if (consumer_acct_id > 0){
		    				for (int i = 0; i < permissionActionArray.length; i++) {
			    				DataLayerMgr.executeUpdate(conn, "INSERT_CONSUMER_ACCT_PERMISSION", new Object[] {consumer_acct_id, permissionActionArray[i]});
			    				out.append("Inserted consumer_acct_permission (").append(consumer_acct_id).append(", ").append(permissionActionArray[i]).append("): ").append(consumer_acct_cd).append("\n");
		    				}
		    				cards_arr.add(new String[]{magstripe, cardNumber, StringUtils.formatCardNumberForDisplay(cardNumber), security_cd, String.valueOf(cardId), exp, exp_display, create_date, consumer_acct_fmt_name, customer_name, location_name, consumer_acct_sub_type_desc});
		    				out.append("Card: ").append(magstripe).append(", Security Code: ").append(security_cd).append(", Card ID: ").append(cardId).append("\n");
		    				numGeneratedCards++;
		    				cardId++;
		    			} else
		    				out.append("Card ").append(cardNumber).append(" already exists!\n");
		    		}
						okay = true;
		    	} else if(consumer_acct_fmt_id == 2){
		    		// Prepaid Card
		    		sectionMarker = "consumer_acct_fmt_id2";
		    		int lostCardNum = 0;
		    		
		    		CardFormat2Simple generator = new CardFormat2Simple(exp, last_consumer_acct_cd, groupID, lostCardNum, num_cards);
		    		
		    		out.append("Calling card generator: CardFormat2Simple ").append(exp).append(" ").append(last_consumer_acct_cd).append(" ").append(groupID).append(" ").append(lostCardNum).append(" ").append(num_cards).append("\n");
		    		
		    		long cardId = newFirstConsumerAcctIdentifier(num_cards);
		    		while (numGeneratedCards < num_cards) {
		    			String card = generator.getNextCardData();
		    			
		    			String[] fields = card.split(",");
		    			String magstripe = fields[0];
		    			String consumer_acct_cd = fields[1];
		    			//String account_id = fields[2];
		    			String vv = fields[3];
		    			String lostCardCD = fields[4];
		    			String security_cd = SecurityUtils.getRandomString(random, "0123456789", 4);
		    			
						consumer_acct_id = insertConsumerAcct(conn, request, form, params, merchant_id, out, magstripe, consumer_acct_cd, vv, lostCardCD, balance, 3, security_cd, random, cardId, consumer_acct_sub_type_id, keyMgrUpdater);
		    			
		    			String cardNumber = magstripe.substring(0, magstripe.indexOf('='));
		    			if(consumer_acct_id < 0){
		    				//error message already attached
		    				return;
		    			}else if (consumer_acct_id > 0){
		    				cards_arr.add(new String[]{magstripe, cardNumber, StringUtils.formatCardNumberForDisplay(cardNumber), security_cd, String.valueOf(cardId), exp, exp_display, create_date, consumer_acct_fmt_name, customer_name, location_name, consumer_acct_sub_type_desc});
		    				out.append("Card: ").append(magstripe).append(", Security Code: ").append(security_cd).append(", Card ID: ").append(cardId).append("\n");
		    				numGeneratedCards++;
		    				cardId++;
		    			} else
		    				out.append("Card ").append(cardNumber).append(" already exists!\n");
		    		}
		    		
		    		if (consumer_acct_sub_type_id == 1) {
		    			// charge customer EFT for USAT Serviced cards only
			    		BigDecimal balanceNumber = StringHelper.isBlank(balance) ? BigDecimal.ZERO : new BigDecimal(balance);
			    		if (numGeneratedCards > 0 && balanceNumber.compareTo(BigDecimal.ZERO) == 1) {
			    			BasicServletUser user = (BasicServletUser)request.getSession().getAttribute(SimpleServlet.ATTRIBUTE_USER);
			    			String chargeDescription = "Charge for the initial balance of " + balance + " " + currency_cd + " on " + numGeneratedCards + " new prepaid cards";
			    			BigDecimal chargeAmount = balanceNumber.multiply(new BigDecimal(numGeneratedCards));
			    			Object[] chargeResult = DataLayerMgr.executeCall(conn, "ADJUST_CUSTOMER_EFT", new Object[] {corp_customer_id, user.getEmailAddress(), currency_cd, chargeDescription, chargeAmount.negate()});
			    			String eftId = ConvertUtils.getStringSafely(chargeResult[1], "");
			    			out.append("Charged customer EFT ").append(eftId).append(" for " + String.format("%.2f", chargeAmount) + " " + currency_cd + " for the initial balance of " + String.format("%.2f", balanceNumber) + " " + currency_cd + " on " + numGeneratedCards + " new prepaid cards");
			    		}
		    		}
		    		
	    			// create a new prepaid virtual customer device
		    		try {
		    			DataLayerMgr.executeCall(conn, "GET_OR_CREATE_PREPAID_DEVICE", params);
		    		} catch (SQLException e) {
		    			if(e.getErrorCode() == 1403)
		    				throw new ServletException("No bank account exists for this customer", e);
		    			else
		    				throw e;
		    		}
	    			out.append("Set up prepaid virtual device serial #: ").append(params.get("prepaid_device_serial_cd")).append(", device ID: ").append(params.get("prepaid_device_id")).append("\n");
						
		    		okay = true;
				} else if (consumer_acct_fmt_id == 3) {
					// Payroll Deduct Card
					sectionMarker = "consumer_acct_fmt_id3";
					int lostCardNum = 0;

					CardFormat3Simple generator = new CardFormat3Simple(exp, last_consumer_acct_cd, groupID, lostCardNum, num_cards);

					out.append("Calling card generator: CardFormat3Simple ").append(exp).append(" ").append(last_consumer_acct_cd).append(" ").append(groupID).append(" ").append(lostCardNum).append(" ").append(num_cards).append("\n");

					long cardId = newFirstConsumerAcctIdentifier(num_cards);
					while (numGeneratedCards < num_cards) {
						String card = generator.getNextCardData();

						String[] fields = card.split(",");
						String magstripe = fields[0];
						String consumer_acct_cd = fields[1];
						//String account_id = fields[2];
						String vv = fields[3];
						String lostCardCD = fields[4];
						String security_cd = SecurityUtils.getRandomString(random, "0123456789", 4);
						
						params.put("consumer_acct_balance", 0); // no balance on payroll deduct until activated by the customer

						consumer_acct_id = insertConsumerAcct(conn, request, form, params, merchant_id, out, magstripe, consumer_acct_cd, vv, lostCardCD, "0", 7, security_cd, random, cardId, consumer_acct_sub_type_id, keyMgrUpdater);

						String cardNumber = magstripe.substring(0, magstripe.indexOf('='));
						if (consumer_acct_id < 0) {
							//error message already attached
							return;
						} else if (consumer_acct_id > 0) {
							cards_arr.add(new String[] {magstripe,cardNumber,StringUtils.formatCardNumberForDisplay(cardNumber),security_cd,String.valueOf(cardId),exp,exp_display,create_date,consumer_acct_fmt_name,customer_name,location_name,consumer_acct_sub_type_desc});
							out.append("Card: ").append(magstripe).append(", Security Code: ").append(security_cd).append(", Card ID: ").append(cardId).append("\n");
							numGeneratedCards++;
							cardId++;
						} else out.append("Card ").append(cardNumber).append(" already exists!\n");
					}
					okay = true;
				} else {
					out.append(buildPrintError(sectionMarker + " : Unhandled consumer account format: " + consumer_acct_fmt_id, request, form));
					request.setAttribute("card_gen_output", out.toString());
					return;
				}
			} finally {
				if(okay)
					keyMgrUpdater.send();
				else
					keyMgrUpdater.abort();
			}
	    	conn.commit();
	    	success = true;
	    	
	    	int card_count = cards_arr.size();
	    	StringBuilder csv_doc = new StringBuilder("");
	    	String[] headers = {"Magstripe", "Card Number", "Display Card Number", "Security Code", "Card ID", "Expiration Date", "Display Expiration Date", "Activation Date", "Card Type", "Customer", "Location", "Card Subtype"}; 
	    	csv_doc.append(StringUtils.join(headers, ",", "\""));
	    	csv_doc.append("\n");
	    	
	    	out.append("</pre></div>");
	    	out.append("<br/><form method='post' action='newConsumerAcct5.i'>\n");
	    	out.append("<center><font color=\"green\" align=\"center\"><b>").append(card_count).append(" cards generated successfully!</b></font><br/><br/>\n");
	    	out.append("<b>Card, Security Code, Card ID</b>\n");
	    	out.append("<textarea rows=\"20\" readonly=\"readonly\" style=\"width:99%\">");
	    	
	    	int ctr = 0;
	    	Iterator<String[]> it = cards_arr.iterator();
	    	String[] card_arr_ref;
	    	try{
		    	while(it.hasNext()){
		    		card_arr_ref = it.next();
		    		out.append(card_arr_ref[0]).append(", ").append(card_arr_ref[3]).append(", ").append(card_arr_ref[4]).append("\n");
		    		String joinedLine = StringUtils.join(card_arr_ref, ",", "\"");
		    		csv_doc.append(joinedLine);
		    		csv_doc.append("\n");
		    		ctr++;
		    	}
	    	}catch(Exception csve){
	    		log.error("Failed to generate card CSV database", csve);
	    		out.append(buildPrintError(sectionMarker+" : Failed to generate card CSV database! : " + csve.getMessage(), request, form));
				request.setAttribute("card_gen_output", out.toString());
				return;
	    	}
	    	
	    	if(ctr < 1){
		    	out.append(buildPrintError(sectionMarker+" : Failed to generate card CSV database!", request, form));
				request.setAttribute("card_gen_output", out.toString());
				return;
	    	}
	    	
	    	out.append("</textarea></center><br/>\n<br/>\n");
	    	out.append("<input type=\"hidden\" name=\"csv\" value=\"").append(Base64.encodeString(csv_doc.toString(), true)).append("\" >\n");
	    	out.append("<div align=\"center\"><input type=submit class=cssButton name=\"action\" value=\"Download CSV Database\" align=\"middle\" />");
	    	out.append("<input type=button class=cssButton value='Start Over' onClick=\"javascript:window.location = 'newConsumerAcct1.i';\" align=\"middle\" /></div><br/>\n<br/>\n<br/>\n");
	    	out.append("</form>");
	    	
	    	//request.setAttribute("csv_doc", csv_doc.toString());
	    	request.setAttribute("card_gen_output", out.toString());
    	}catch(Exception e){
    		log.error("Failed to generate card numbers", e);
    		out.append(buildPrintError("<br/>" + sectionMarker + " : Exception occured, Failed to generate card numbers! : " + e, request, form));
			request.setAttribute("card_gen_output", out.toString());
    	} finally {
    		if (!success)
    			ProcessingUtils.rollbackDbConnection(log, conn);    		
    		ProcessingUtils.closeDbConnection(log, conn);
    	}
    }
    
    /**
     * Insert a new consumer acct record based on the data passed in.
     * @param request
     * @param consumer_acct_fmt_id
     * @param merchant_id
     * @param consumer_id
     * @param location_id
     * @param deactivation_date
     * @param balance
     * @param out
     * @param consumer_acct_id
     * @param consumer_acct_cd
     * @param vv
     * @param lostCardCD
     * @return
     * @throws SQLException
     * @throws DataLayerException
     * @throws IOException 
     */
	private long insertConsumerAcct(Connection conn, HttpServletRequest request, InputForm form,
			Map<String, Object> params, long merchant_id, StringBuilder out, String magstripe,
			String consumer_acct_cd, String vv, String lostCardCD, String promo_balance, int consumer_acct_type_id, 
			String security_cd, SecureRandom random, long cardId, int consumer_acct_sub_type_id,
			KeyMgrAccountUpdater keyMgrAccountUpdater)
					throws SQLException, DataLayerException, NoSuchAlgorithmException, NoSuchProviderException, IOException {		
		byte[] accountCdHash = SecureHash.getUnsaltedHash(magstripe.substring(0, magstripe.indexOf('=')).getBytes());
		params.put("truncated_card_num", MessageResponseUtils.maskCardNumber(magstripe));
		params.put("last_consumer_acct_cd", consumer_acct_cd);
		// params.put("consumer_acct_validation_cd", vv);
		// params.put("consumer_acct_issue_cd", lostCardCD);
		params.put("consumer_acct_promo_total", StringHelper.isBlank(promo_balance) ? 0 : promo_balance);
		params.put("consumer_acct_type_id", consumer_acct_type_id);
		params.put("consumer_acct_cd_hash", accountCdHash);
		// *
		byte[] salt = SecureHash.getSalt(random, SecureHash.SALT_LENGTH);
		params.put("consumer_acct_raw_hash", SecureHash.getHash(magstripe.getBytes(), salt));
		params.put("consumer_acct_raw_salt", salt);
		params.put("consumer_acct_raw_alg", SecureHash.HASH_ALGORITHM_ITERS);
		// */
		salt = SecureHash.getSalt(random, SecureHash.SALT_LENGTH);
		params.put("security_cd_hash", SecureHash.getHash(security_cd.getBytes(), salt));
		params.put("security_cd_salt", salt);
		params.put("security_cd_hash_alg", SecureHash.HASH_ALGORITHM_ITERS);
		params.put("consumer_acct_identifier", cardId);
		params.put("consumer_acct_sub_type_id", consumer_acct_sub_type_id > 0 ? consumer_acct_sub_type_id : null);

		long consumer_acct_id = -1;
		Object[] result = DataLayerMgr.executeCall(conn, "INSERT_CONSUMER_ACCT", params);		
		if(result != null && result[1] != null){
			consumer_acct_id = ((BigDecimal)result[1]).longValue();
			if (consumer_acct_id < 1)
				return consumer_acct_id;
		}else{
			out.append("Failed to insert new consumer_acct: ").append(consumer_acct_cd).append("!");
			request.setAttribute("card_gen_output", out.toString());
			return -1;
		}
		
		out.append("Inserted consumer_acct ").append(consumer_acct_id).append(": ").append(consumer_acct_cd).append("\n");
		
		int rowCount = DataLayerMgr.executeUpdate(conn, "INSERT_MERCHANT_CONSUMER_ACCT", new Object[] {merchant_id, consumer_acct_id});
		if(rowCount != 1){
			out.append(buildPrintError("Failed to insert new merchant_consumer_acct!", request, form));
			request.setAttribute("card_gen_output", out.toString());
			return -1;
		}
		
		out.append("Inserted merchant_consumer_acct (").append(merchant_id).append(", ").append(consumer_acct_id).append("): ").append(consumer_acct_cd).append("\n");
		CRC16 crc = new CRC16();
		for(byte b : magstripe.getBytes())
			crc.update(b);
		keyMgrAccountUpdater.addCard(accountCdHash, cardId, crc.value, (short)0);
		return consumer_acct_id;
	}
    
	
	/**
	 * Builds an error message.
	 * @param errorMessage
	 * @param request
	 * @return
	 */
	private String buildPrintError(String errorMessage, HttpServletRequest request, InputForm form){
		StringBuilder error = new StringBuilder("");
		error.append("<font color=\"red\"><b>").append(errorMessage).append("</b></font><br />\n");
		error.append("<font color=\"red\"><b>Card generation failed!</b></font><br />\n</pre></div>");
		return error.toString();
	}
	
	protected long newFirstConsumerAcctIdentifier(int numberOfAccounts) throws SQLException, DataLayerException, ConvertException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("numberOfAccounts", numberOfAccounts);
		Object[] results = DataLayerMgr.executeCall("NEW_LAST_CONSUMER_ACCT_IDENTIFIER", params, true);
		return ConvertUtils.getLong(results[1]) - numberOfAccounts + 1;
	}
}
