/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.consumer;

import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

import com.usatech.dms.action.ConsumerAction;
import com.usatech.dms.model.Consumer;

/**
 * This step will process the search based on the combination of params passed in below, and
 * forward to different pages depends on the request parameter.
 */
public class ConsumerInfoStep extends AbstractStep
{

    /*
     * (non-Javadoc)
     * 
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm,
     * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
        String consumerId = form.getString(ConsumerConstants.PARAM_CONSUMER_CURRENT_CONSUMER_ID, false);
        
        StringBuilder condition = new StringBuilder("");

        if (consumerId != null && !consumerId.trim().equals(""))
        {
        	try{
        		Consumer consumer = (Consumer)ConsumerAction.getConsumer(consumerId);
        		List<com.usatech.dms.model.ConsumerAccount> accounts = (List<com.usatech.dms.model.ConsumerAccount>)ConsumerAction.getConsumerAccounts(consumerId);
        		consumer.setAccounts(accounts);
        		request.setAttribute(ConsumerConstants.PARAM_CONSUMER_CURRENT_CONSUMER, consumer);
        	}catch(Exception e){
        		throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
        	}
        }


    }


}
