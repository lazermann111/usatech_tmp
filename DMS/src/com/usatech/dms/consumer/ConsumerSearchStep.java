/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.consumer;

import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.util.LinkedHashMap;
import simple.util.NameValuePair;

import com.usatech.cardnumbergenerator.cd.Luhn;
import com.usatech.dms.util.DMSConstants;
import com.usatech.dms.util.DMSPaginationStep;
import com.usatech.dms.util.DMSValuesList;
import com.usatech.dms.util.Helper;
import com.usatech.dms.util.ValuesListLoader;
import com.usatech.ec2.EC2ClientUtils;
import com.usatech.layers.common.constants.CommonConstants;
import com.usatech.layers.common.util.StringHelper;

/**
 * This step will process the search based on the combination of params passed in below, and
 * forward to different pages depends on the request parameter.
 */
public class ConsumerSearchStep extends DMSPaginationStep
{
	protected static final char[] POSSIBLE_PROCESS_CODES = "52013489".toCharArray(); // most frequent first

	/*
	 * (non-Javadoc)
	 * 
	 * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm,
	 * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
    	try
        {
	        String cardNumber = form.getString(ConsumerConstants.PARAM_CONSUMER_CURRENT_CONSUMER_CARD, false);
	        String accountNumber = form.getStringSafely(ConsumerConstants.PARAM_CONSUMER_ACCOUNT_NUMBER, "").trim();
	        long startCardId = form.getLong(ConsumerConstants.PARAM_CONSUMER_START_CARD_ID, false, 0);
	        long endCardId = form.getLong(ConsumerConstants.PARAM_CONSUMER_END_CARD_ID, false, 0);
	        String firstName = form.getString(ConsumerConstants.PARAM_CONSUMER_CURRENT_FIRST_NAME, false);
	        String lastName = form.getString(ConsumerConstants.PARAM_CONSUMER_CURRENT_LAST_NAME, false);
	        String primaryEmail = form.getString(ConsumerConstants.PARAM_CONSUMER_CURRENT_PRIMARY_EMAIL, false);
	        String notificationEmail = form.getString(ConsumerConstants.PARAM_CONSUMER_CURRENT_NOTIFICATION_EMAIL, false);
	        String assignedLocationId = form.getString(ConsumerConstants.PARAM_CONSUMER_CURRENT_ASSIGNED_LOCATION_ID, false);
	        if(StringHelper.isBlank(assignedLocationId))
	        	assignedLocationId = form.getString("location_id", false);
	        String consumerMerchantId = form.getString(ConsumerConstants.PARAM_CONSUMER_CURRENT_CONSUMER_MERCHANT_ID, false);
	        int consumerAcctTypeId = form.getInt("consumer_acct_type_id", false, 0);
	        int consumerAcctSubTypeId = form.getInt("consumer_acct_sub_type_id", false, 0);
	        
	        StringBuilder condition = new StringBuilder(ConsumerConstants.SQL_CONSUMER_CARD_SEARCH_BASE);        
	        
	        Map<Integer, Object> params = new LinkedHashMap<Integer, Object>();
	        int paramsCnt = 0;
	        if (!StringHelper.isBlank(cardNumber))
	        {
	        	long globalAccountId = EC2ClientUtils.getGlobalAccountId(cardNumber);
	            condition.append(" AND c.consumer_id IN (SELECT ca.consumer_id FROM pss.consumer_acct ca JOIN pss.consumer_acct_base cab ON ca.consumer_acct_id = cab.consumer_acct_id WHERE cab.global_account_id = CAST(? AS numeric)) ");
	            params.put(paramsCnt++, globalAccountId);
			} else if(!StringHelper.isBlank(accountNumber)) {
	        	DMSValuesList cardTypeList = ValuesListLoader.getValuesList(request, DMSConstants.GLOBAL_LIST_CARD_TYPES);
				int i = 0;
				for(NameValuePair cardType : cardTypeList.getList()) {
					StringBuilder cardBuilder = new StringBuilder(accountNumber.length() + 9);
					cardBuilder.append(CommonConstants.USAT_IIN).append(cardType.getValue());
					String[] possibleCardNumbers;
					if(cardType.getValue().equals("1") && accountNumber.length() == 9) {
						cardBuilder.append('_').append(accountNumber).append('_');
						possibleCardNumbers = new String[POSSIBLE_PROCESS_CODES.length];
						for(int k = 0; k < POSSIBLE_PROCESS_CODES.length; k++) {
							cardBuilder.setCharAt(CommonConstants.USAT_IIN.length() + 1, POSSIBLE_PROCESS_CODES[k]);
							int luhn = Luhn.generate(cardBuilder.substring(0, cardBuilder.length() - 1));
							cardBuilder.setCharAt(cardBuilder.length() - 1, (char) ('0' + luhn));
							possibleCardNumbers[k] = cardBuilder.toString();
						}
					} else if(accountNumber.length() == 11) {
						cardBuilder.append(accountNumber);
						int luhn = Luhn.generate(cardBuilder.substring(0, cardBuilder.length() - 1));
						cardBuilder.append((char) ('0' + luhn));
						possibleCardNumbers = new String[] { cardBuilder.toString() };
					} else
						possibleCardNumbers = new String[0];
					for(String possibleCardNumber : possibleCardNumbers) {
						Long globalAccountId = EC2ClientUtils.getGlobalAccountId(possibleCardNumber);
						if(globalAccountId != null) {
							if(i++ > 0)
								condition.append(", ");
							else
								condition.append("  AND c.consumer_id IN (SELECT ca.consumer_id FROM pss.consumer_acct ca JOIN pss.consumer_acct_base cab ON ca.consumer_acct_id = cab.consumer_acct_id WHERE cab.global_account_id IN (");
							condition.append("?");
							params.put(paramsCnt++, globalAccountId);
						}
					}
				}
				if(i == 0)
					condition.append(" AND 1 = 0 ");
				else
					condition.append(")) ");
	        }
	        if (startCardId > 0 && endCardId > 0)
	        {
	        	condition.append(" AND c.consumer_id IN (SELECT consumer_id FROM pss.consumer_acct WHERE consumer_acct_identifier BETWEEN CAST (? AS NUMERIC) AND CAST (? AS NUMERIC)) ");
	        	params.put(paramsCnt++, startCardId);
	        	params.put(paramsCnt++, endCardId);
	        }
	        else if (startCardId > 0)
	        {
	        	condition.append(" AND c.consumer_id IN (SELECT consumer_id FROM pss.consumer_acct WHERE consumer_acct_identifier >= CAST (? AS NUMERIC)) ");
	        	params.put(paramsCnt++, startCardId);
	        }
	        else if (endCardId > 0)
	        {
	        	condition.append(" AND c.consumer_id IN (SELECT consumer_id FROM pss.consumer_acct WHERE consumer_acct_identifier <= CAST (? AS NUMERIC)) ");
	        	params.put(paramsCnt++, endCardId);
	        }
	        if (!StringHelper.isBlank(firstName))
	        {
	        	String wildCardChar = form.getString(ConsumerConstants.PARAM_CONSUMER_CURRENT_FIRST_NAME_SEARCH_CHAR, false);
	            // search by First Name
	            condition.append(" AND LOWER(c.consumer_fname) like lower(?) ");
	            params.put(paramsCnt++, Helper.buildSqlWildcardConditionString(wildCardChar, firstName));
	        }
	        if(!StringHelper.isBlank(lastName))
	        {
	        	String wildCardChar = form.getString(ConsumerConstants.PARAM_CONSUMER_CURRENT_LAST_NAME_SEARCH_CHAR, false);
	            // search by Last Name
	            condition.append(" AND LOWER(c.consumer_lname) like lower(?) ");
	            params.put(paramsCnt++, Helper.buildSqlWildcardConditionString(wildCardChar, lastName));
	        }
	        if(!StringHelper.isBlank(primaryEmail))
	        {
	        	String wildCardChar = form.getString(ConsumerConstants.PARAM_CONSUMER_CURRENT_PRIMARY_EMAIL_SEARCH_CHAR, false);
	            // search by Serial Number
	            condition.append(" AND LOWER(c.consumer_email_addr1) like lower(?) ");
	            params.put(paramsCnt++, Helper.buildSqlWildcardConditionString(wildCardChar, primaryEmail));
	        }
	        if(!StringHelper.isBlank(notificationEmail))
	        {
	        	String wildCardChar = form.getString(ConsumerConstants.PARAM_CONSUMER_CURRENT_NOTIFICATION_EMAIL_SEARCH_CHAR, false);
	            // search by Serial Number
	            condition.append(" AND LOWER(c.consumer_email_addr2) like lower(?) ");
	            params.put(paramsCnt++, Helper.buildSqlWildcardConditionString(wildCardChar, notificationEmail));
	        }
	        if(!StringHelper.isBlank(assignedLocationId) && !assignedLocationId.trim().equals("-1"))
	        {
	            // search by Serial Number
	            condition.append(" AND c.consumer_id IN ( SELECT consumer_id"
	                                      ).append(" FROM pss.consumer_acct"
	                                      ).append(" WHERE location_id = CAST (? AS NUMERIC) ) ");
	            
	            
	            params.put(paramsCnt++, assignedLocationId);
	        }
	        if(!StringHelper.isBlank(consumerMerchantId) && !consumerMerchantId.trim().equals("-1"))
	        {
	            condition.append(" AND c.consumer_id IN (SELECT c.consumer_id "
	               ).append("FROM pss.consumer c JOIN pss.consumer_acct ca ON c.consumer_id = ca.consumer_id "
	               ).append("JOIN pss.merchant_consumer_acct mca ON mca.merchant_id = CAST (? AS NUMERIC) AND ca.consumer_acct_id = mca.consumer_acct_id) ");
	            
	            params.put(paramsCnt++, consumerMerchantId);
	        }
	        if (consumerAcctTypeId > 0)
	        {
	        	condition.append(" AND c.consumer_id IN (SELECT consumer_id FROM pss.consumer_acct WHERE consumer_acct_type_id = CAST (? AS NUMERIC)) ");
	        	params.put(paramsCnt++, consumerAcctTypeId);
	        }
	        if (consumerAcctSubTypeId > 0)
	        {
	        	condition.append(" AND c.consumer_id IN (SELECT consumer_id FROM pss.consumer_acct WHERE consumer_acct_sub_type_id = CAST (? AS NUMERIC)) ");
	        	params.put(paramsCnt++, consumerAcctSubTypeId);
	        }
	        
			if (paramsCnt == 0)
				if (StringHelper.isBlank(accountNumber)) {
					request.setAttribute("errorMessage", "At least one search parameter is required.");
					return;
				}

            String queryBase = condition.toString();
            
            setPaginatedResultsOnRequest(form, request, ConsumerConstants.SQL_CONSUMER_CARD_SEARCH_START, queryBase, ConsumerConstants.SQL_END, ConsumerConstants.DEFAULT_SORT_INDEX_CONSUMER_CARD_SEARCH,
            		ConsumerConstants.SORT_FIELDS__CONSUMER_CARD_SEARCH, ConsumerConstants.PARAM_CONSUMER_CURRENT_CONSUMER_CARD_RESULTS, params != null && params.size() > 0 ? params.values().toArray() : null);

        }
        catch (Exception e)
        {
            throw new ServletException(e);
        }

    }
}
