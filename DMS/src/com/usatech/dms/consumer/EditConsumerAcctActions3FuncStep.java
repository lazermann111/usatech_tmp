/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.consumer;

import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usatech.layers.common.ProcessingUtils;

import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

public class EditConsumerAcctActions3FuncStep extends AbstractStep
{	
	private static final simple.io.Log log = simple.io.Log.getLog();
	
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
    	long consumer_acct_id = form.getLong("consumer_acct_id", false, -1);
    	if (consumer_acct_id < 1)
    		return;
    	
    	StringBuilder out = new StringBuilder("<div align=\"left\"><pre>CHANGE LOG:\n\n");
    	int rowCount = -1;
		Object[] result = null;
		Results results = null;
		String deviceTypes = ConvertUtils.getStringSafely(request.getAttribute("device_types"), "");
		String[] deviceTypesArray = deviceTypes.split(",");
		String[] permissionActionArray = new String[deviceTypesArray.length];
		boolean success = false;
    	Connection conn = null;
    	try {
			conn = DataLayerMgr.getConnection("OPER");

        	for (int i = 0; i < deviceTypesArray.length; i++) {
        		String device_type_id = deviceTypesArray[i];
        		int action_id = ConvertUtils.getInt(request.getAttribute(new StringBuilder("action_").append(device_type_id).toString()));
				String action_params = ConvertUtils.getStringSafely(request.getAttribute(new StringBuilder("action_").append(device_type_id).append("_param").toString()), "");
        		
	        	long permission_action_id = -1;
	        	long last_permission_action_id = -1;
	        	long this_permission_action_id = -1;
	        	StringBuilder permission_action_params = new StringBuilder();		        	
	        	
	        	results = DataLayerMgr.executeQuery(conn, "GET_PERMISSION_ACTION_INFO", new Object[]{action_id}, null);
	        	if (results != null) {
		        	while(results.next()){
		        		this_permission_action_id = ConvertUtils.getLong(results.get("permission_action_id"));
		        		if (last_permission_action_id == -1)
		        			last_permission_action_id = this_permission_action_id;
		        		else if (last_permission_action_id != this_permission_action_id) {
	        				if (permission_action_params.toString().equalsIgnoreCase(action_params)) {
	        					permission_action_id = last_permission_action_id;
	        					break;
	        				}
		        			last_permission_action_id = this_permission_action_id;
		        			permission_action_params.setLength(0);
		        		}
		        		if (permission_action_params.length() > 0)
		        			permission_action_params.append(",");
		        		permission_action_params.append(results.getFormattedValue("action_param_id"));
		        	}
		        	if (permission_action_id == -1 && permission_action_params.toString().equalsIgnoreCase(action_params))
    					permission_action_id = this_permission_action_id;
	        	}
	        	
	        	if(permission_action_id != -1){
	        		// # we found a matching permission_action
	        		out.append("Using existing permission action " + permission_action_id + " with device_type_id: " +  device_type_id + ", action_id: " + action_id + " and action_params: " + action_params + "\n");
	        	}else{
	        		result = DataLayerMgr.executeCall(conn, "INSERT_PERMISSION_ACTION", new Object[] {action_id});
	        		permission_action_id = ConvertUtils.getLongSafely(result[1], -1);
	        		if(permission_action_id < 1) {
	    				out.append(buildPrintError("Failed to create permission_action for action_id " + action_id + "!", request, form));
	        			request.setAttribute("action_output", out.toString());
	        			return;
	    			}
	    			if (action_params != null && action_params.length() > 0) {
		    			int counter = 0;
		    			String[] action_params_array = action_params.split(",");
		    			for(String action_param_id : action_params_array){		    				
		    				rowCount = DataLayerMgr.executeUpdate(conn, "INSERT_PERMISSION_ACTION_PARAM", new Object[] {permission_action_id, action_param_id, counter++});
		    				if(rowCount != 1){
		    					out.append(buildPrintError("Failed to create permission_action_param  (" + permission_action_id + ", " + action_param_id + ")!", request, form));
		    	    			request.setAttribute("action_output", out.toString());
		    	    			return;
		    				}
		    			}
	    			}
	    			out.append("Created new permission_action " + permission_action_id + " with device_type_id: " +  device_type_id + ", action_id: " + action_id + " and action_params: " + action_params + "\n");
	        	}
	        	
	        	permissionActionArray[i] = String.valueOf(permission_action_id);
        	}
        	
        	StringBuilder consumerAcctPermissions = new StringBuilder();
    		results = DataLayerMgr.executeQuery(conn, "GET_CONSUMER_ACCT_PERMISSION_ACTIONS", new Object[] {consumer_acct_id});
    		while (results.next()) {
    			if (consumerAcctPermissions.length() > 0)
    				consumerAcctPermissions.append(",");
    			consumerAcctPermissions.append(results.getFormattedValue("permissionActionId"));
    		}
    		
    		boolean changedPermissions = false;
    		boolean deletedPermissions = false;
    		String[] consumerAcctPermissionArray = consumerAcctPermissions.toString().split(",");
    		if (consumerAcctPermissionArray.length > permissionActionArray.length) {
    			DataLayerMgr.executeUpdate(conn, "DELETE_CONSUMER_ACCT_PERMISSIONS", new Object[] {consumer_acct_id});
    			out.append("Deleted consumer_acct_permission records for consumer_acct_id ").append(consumer_acct_id);
    			changedPermissions = true;
    			deletedPermissions = true;
    		}
    		    		
    		for (int i = 0; i < permissionActionArray.length; i++) {
    			if (deletedPermissions || i > consumerAcctPermissionArray.length - 1) {
    				DataLayerMgr.executeUpdate(conn, "INSERT_CONSUMER_ACCT_PERMISSION", new Object[] {consumer_acct_id, permissionActionArray[i]});
    				out.append("Inserted consumer_acct_permission (").append(consumer_acct_id).append(", ").append(permissionActionArray[i]).append(")\n");
    				changedPermissions = true;
    				
    			} else if (consumerAcctPermissionArray[i].equalsIgnoreCase(permissionActionArray[i])) {
    				out.append("Old permission_action_id ").append(consumerAcctPermissionArray[i]).append(" equals new permission_action_id ")
    					.append(permissionActionArray[i]).append(" (NO CHANGE)\n");
    			} else {
    				DataLayerMgr.executeUpdate(conn, "UPDATE_CONSUMER_ACCT_PERMISSION", new Object[] {consumer_acct_id, consumerAcctPermissionArray[i], permissionActionArray[i]});
    				out.append("Updated consumer_acct_permission for consumer_acct_id ").append(consumer_acct_id).append(" from ")
    					.append(consumerAcctPermissionArray[i]).append(" to ").append(permissionActionArray[i]).append("\n");
    				changedPermissions = true;
    			}
    		}    		    		
        	conn.commit();
        	success = true;
        	
        	out.append("</pre></div><br />\n<div align=\"center\"><font color=\"green\"><b>");
        	if (changedPermissions)
        		out.append("Card actions changed successfully!");
        	else
        		out.append("No change!");
        	out.append("</b></font></div><br />\n");
        	
	    	request.setAttribute("action_output", out.toString());
    	}catch(Exception e){
    		out.append(buildPrintError("Exception occured, Failed to change card actions! : " + e, request, form));
			request.setAttribute("action_output", out.toString());
    	} finally {
    		if (!success)
    			ProcessingUtils.rollbackDbConnection(log, conn);    		
    		ProcessingUtils.closeDbConnection(log, conn);
    	}
    }
	
	/**
	 * Builds an error message.
	 * @param errorMessage
	 * @param request
	 * @return
	 */
	private String buildPrintError(String errorMessage, HttpServletRequest request, InputForm form){
		StringBuilder error = new StringBuilder();
		error.append("<font color=\"red\"><b>").append(errorMessage).append("</b></font><br />\n");
		error.append("<font color=\"red\"><b>Operation failed!</b></font><br />\n</pre></div>");
		return error.toString();
	}
}
