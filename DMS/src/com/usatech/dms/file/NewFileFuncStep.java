/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.file;

import java.io.InputStream;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerMgr;
import simple.servlet.Dispatcher;
import simple.servlet.InputFile;
import simple.servlet.InputForm;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;

import com.usatech.dms.action.ConfigFileActions;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.util.StringHelper;

public class NewFileFuncStep extends AbstractStep
{
	private static final simple.io.Log log = simple.io.Log.getLog();
	
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
    	String errorMsg = "";
    	String action = form.getString("action", false);
    	String serverFileName = form.getString("file_name", false);
    	String fileData = form.getString("file_data", false);
    	Connection conn = null;
    	boolean success = false;
    	try{
    	
	    	if("Upload to Server".equalsIgnoreCase(action)){
	    		conn = DataLayerMgr.getConnection("OPER");
	    		
	    		String evNumber = form.getString("ev_number", false);
	    		if(!(StringHelper.isBlank(evNumber)) && "null".equalsIgnoreCase(evNumber)){evNumber="";}
	    		String deviceTypeId = form.getStringSafely("device_type_id", "");
	    		int fileType = form.getInt("file_type", false, -1);
	    		String comment = form.getString("file_comment", false);
	    		InputStream in = null;
	    		
		    	if(StringHelper.isBlank(serverFileName)){
	    			
	    			errorMsg = "Missing required parameter: file_name\n";
	    			form.setAttribute("errorMsg", errorMsg);
	                return;
	    		}
		    	
		    	if(fileType < 0){
	    			
	    			errorMsg = "Missing required parameter: file_type\n";
	    			form.setAttribute("errorMsg", errorMsg);
	                return;
	    		}
		    	
		    	if(StringHelper.isBlank(fileData)){
	    			errorMsg = "Missing required parameter: upload_filehandle\n";
	    			form.setAttribute("errorMsg", errorMsg);
	                return;
	    		}
		    	
		    	long fileSize = 0;
		    	try{
		    		InputFile inputFile = (InputFile)form.getAttribute("file_data");
		    		in = inputFile.getInputStream();
		    		fileSize = inputFile.getLength();
		    	}catch(Exception e){
		    		errorMsg = "File upload failed!  No data.\n";
	    			form.setAttribute("errorMsg", errorMsg);
	                return;
		    	}
    	        if(in == null){
    	        	errorMsg = "File upload failed!  No data.\n";
	    			form.setAttribute("errorMsg", errorMsg);
	                return;
    	        }
					Long sizeLimit = ConfigFileActions.getFileTransferTypeSizeLimit(fileType, conn);
					if ((sizeLimit != null) && (fileSize > sizeLimit)) {
						errorMsg = String.format(
								"The size of the uploaded file is bigger than size limit (%d bytes) for the selected file type.",
								sizeLimit);
						form.setAttribute("errorMsg", errorMsg);
						return;
					}
		    	
		    	long fileTransferId = ConfigFileActions.getNextFileTransferSequenceNum(conn);
		    	if(fileTransferId < 1){
	    			
	    			errorMsg = "Failed to get a new file_transfer_id!";
	    			form.setAttribute("errorMsg", errorMsg);
	                return;
	    		}
		    	
		    	try{
		    		ConfigFileActions.insertFileTransferWithId(fileTransferId, serverFileName, fileType, conn);
		    	}catch(ServletException e){
		    		errorMsg = "sql insert error: " + e.getMessage() + " : \n" + e.getStackTrace();
	    			form.setAttribute("errorMsg", errorMsg);
	                return;
		    	}
		    	
		    	Map<String, Object> params = null;
	    		params = new HashMap<String, Object>();
	    		params.put("file_content", in);
	    		params.put("comment", comment);
	    		params.put("file_id", fileTransferId);
	    		
	    		try{
	    			DataLayerMgr.executeCall(conn, "UPDATE_FILE_TRANSFER_CONTENT", params);
	    		}catch(Exception sqle){
	    			errorMsg = "sql insert error: " + sqle.getMessage() + " : \n" + sqle.getStackTrace();
	    			form.setAttribute("errorMsg", errorMsg);
	                return;
	    		}
	    		
	    		try {
	    			in.close();
	    		} catch (Exception e) { }
		    	
		    	if(StringHelper.isBlank(evNumber))
		    		form.setRedirectUri(new StringBuilder("/fileDetails.i?file_transfer_id=").append(fileTransferId).toString());
		    	else
		    		form.setRedirectUri(new StringBuilder("/fileDetailsFunc.i?file_transfer_id=").append(fileTransferId).append("&ev_number=").append(evNumber).append("&device_type_id=").append(deviceTypeId).append("&myaction=Upload+to+Device").toString());
		    	
		    	
		    	conn.commit();
	        	success = true;		    	
		    	return;		    	
	    	}else{
	    		errorMsg = "No Action Defined!";
    			form.setAttribute("errorMsg", errorMsg);
                return;
	    	}
    	}catch(Exception e){
            throw new ServletException(e);
    	 } finally {
     		if (!success)
     			ProcessingUtils.rollbackDbConnection(log, conn);
     		ProcessingUtils.closeDbConnection(log, conn);
         }
    }
}
