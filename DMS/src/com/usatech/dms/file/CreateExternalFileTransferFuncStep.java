/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.file;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;
import simple.util.NameValuePair;

import com.usatech.dms.action.ConfigFileActions;
import com.usatech.dms.action.DeviceActions;
import com.usatech.dms.action.FileActions;
import com.usatech.dms.action.PendingCmdActions;
import com.usatech.layers.common.model.Device;
import com.usatech.dms.util.Helper;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.util.StringHelper;

public class CreateExternalFileTransferFuncStep extends AbstractStep
{	
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
    	Map<String, String> errorMap = new HashMap<String, String>();
    	long deviceId = form.getLong("device_id", false, -1);
    	String deviceName = form.getString("device_name", false);
    	boolean bulk = form.getBoolean("bulk", false, false);
    	int deviceTypeId;
 
    	Device device = null;
    	try{
    		if (bulk)
    			deviceTypeId = form.getInt("device_type_id", true, -1);
    		else {
	    		if(deviceId>0){
	        		device = DeviceActions.loadDevice(deviceId, request); 
	        	}else if(!(StringHelper.isBlank(deviceName))){
	        		device = DeviceActions.loadDevice(deviceName, request);
	        	}else{
	        		if(StringHelper.isBlank(deviceName))
	        			throw new ServletException("Required Parameter Not Found: device_name");        		
	        		if(deviceId<0)
	        			throw new ServletException("Required Parameter Not Found: device_id");
	        	}
	        	
	        	if(device != null && device.getId()<0)
	        		throw new ServletException("No device found for device_id=" + deviceId + ", device_name=" + deviceName);
	        	
	        	if(device != null && (device.getTypeId() == DeviceType.EDGE.getValue()))
	        		throw new ServletException("Edge device " + deviceName + " does not support this operation");
	        	
	        	deviceTypeId = device.getTypeId();
    		}
        	
        	String protocol = form.getString("protocol", true);
        	String ipAddress = form.getString("ip_address", true).trim();					 
        	String port = form.getString("port", true).trim();
        	String loginName = form.getString("login_name", true).trim();					
        	String loginPassword = form.getString("login_password", true).trim();
        	String filePath = form.getStringSafely("file_path", "").trim();
        	if (!filePath.endsWith("/"))
        		filePath += "/";
        	String fileName = form.getString("file_name", true).trim();
        	String fileFormat = form.getString("file_format", true);					
        	String fileEncapsulation = form.getString("file_encapsulation", true);
        	String crcType = form.getString("crc_type", true);
        	String crc = form.getString("crc", true).trim();
        	String encryptionKey = form.getString("encryption_key", true).trim();
        	String transferStart = form.getString("transfer_start", true).trim();
        	String transferEnd = form.getString("transfer_end", true).trim();
        	String retries = form.getString("retries", true).trim();
        	String retryInterval = form.getString("retry_interval", true).trim();
        	String confirmEmail = form.getStringSafely("confirm_email", "").trim();
        	String clientFilePath = form.getStringSafely("client_file_path", "").trim();
    		if (!bulk)
    			Helper.validateExists(errorMap, deviceName, "device_name");
    		Helper.validateExists(errorMap, protocol, "protocol");
    		Helper.validateExists(errorMap, ipAddress, "ip_address");
    		Helper.validateExists(errorMap, port, "port");
    		Helper.validateExists(errorMap, loginName, "login_name");
    		Helper.validateExists(errorMap, loginPassword, "login_password");
    		Helper.validateExists(errorMap, filePath, "file_path");
    		Helper.validateExists(errorMap, fileName, "file_name");
    		Helper.validateExists(errorMap, fileFormat, "file_format");
    		Helper.validateExists(errorMap, fileEncapsulation, "file_encapsulation");
    		Helper.validateExists(errorMap, crcType, "crc_type");
    		Helper.validateExists(errorMap, crc, "crc");
    		
    		Helper.validateExists(errorMap, transferStart, "transfer_start");
    		Helper.validateExists(errorMap, transferEnd, "transfer_end");
    		Helper.validateExists(errorMap, retries, "retries");
    		Helper.validateExists(errorMap, retryInterval, "retry_interval");
    		
    		if(StringHelper.isBlank(encryptionKey) && (!(fileEncapsulation.equals("00")) || !(fileEncapsulation.equals("01")) || !(fileEncapsulation.equals("04")))){
    			errorMap.put("encryption_key","Required field not found: encryption_key");
    		}
    		
    		if(deviceTypeId == DeviceType.G4.getValue() 
    				|| deviceTypeId == DeviceType.GX.getValue()){
    			Helper.validateLength(errorMap, "Gx", loginName, "login_name", 1, 16);
    			Helper.validateLength(errorMap, "Gx", loginPassword, "login_password", 1, 16);
    			Helper.validateLength(errorMap, "Gx", filePath, "file_path", 1, 48);
    			Helper.validateLength(errorMap, "Gx", fileName, "file_name", 1, 48);
    		}
    		
    		if(deviceTypeId == DeviceType.KIOSK.getValue()){
    			Helper.validateExists(errorMap, clientFilePath, "client_file_path");
    			Helper.validateLength(errorMap, "Kiosk", clientFilePath, "client_file_path", 1, 256);
    		}
    		
    		String filePathName = filePath + fileName;
    		
    		Helper.validateRegex(errorMap, protocol, "protocol", FileActions.regexPatternAlpha);
    		Helper.validateRegex(errorMap, ipAddress, "ip_address", FileActions.regexPatternIpAddress);
    		Helper.validateRegex(errorMap, port, "port", FileActions.regexPatternPort);
    		Helper.validateRegex(errorMap, loginName, "login_name", FileActions.regexPatternAllCharTypesLength256);
    		Helper.validateRegex(errorMap, loginPassword, "login_password", FileActions.regexPatternAllCharTypesLength256);
    		Helper.validateRegex(errorMap, filePath, "file_path", FileActions.regexPatternAllCharTypesLength256);
    		Helper.validateRegex(errorMap, fileName, "file_name", FileActions.regexPatternAllCharTypesLength256);
    		Helper.validateRegex(errorMap, filePathName, "file_path_name", FileActions.regexPatternAllCharTypesLength256);
    		Helper.validateRegex(errorMap, fileFormat, "file_format", FileActions.regexPatternAlphaNumeric);
    		Helper.validateRegex(errorMap, fileEncapsulation, "file_encapsulation", FileActions.regexPatternAlphaNumeric);
    		Helper.validateRegex(errorMap, crcType, "crc_type", FileActions.regexPatternAlphaNumeric);
    		Helper.validateRegex(errorMap, crc, "crc", FileActions.regexPatternHex);
    		
    		Helper.validateRegex(errorMap, retries, "retries", FileActions.regexPatternNumericLength3);
    		Helper.validateRegex(errorMap, retryInterval, "retry_interval", FileActions.regexPatternNumericLength3);
    		
    		if(!(StringHelper.isBlank(encryptionKey)) && (Helper.validateRegex(errorMap, encryptionKey, "encryption_key", FileActions.regexPatternHexEncryption)) || 
    				((fileEncapsulation.equals("00")) || (fileEncapsulation.equals("01")) || (fileEncapsulation.equals("04")))){
    			//no-op
    		}else{
    			errorMap.put("encryption_key", "Field failed validation: encryption_key");
    		}
    		
    		if((!(StringHelper.isBlank(crcType)) && !(StringHelper.isBlank(crc))) &&  (crcType.equals("00") && crc.length() != 4)){
    			errorMap.put("crc", "CRC value does not correspond to type");
    		}
    		
    		if((!(StringHelper.isBlank(crcType)) && !(StringHelper.isBlank(crc))) &&  (crcType.equals("01") && crc.length() != 8)){
    			errorMap.put("crc", "CRC value does not correspond to type");
    		}
    		
    		if(!(StringHelper.isBlank(crc)) && !(crc.length() % 2 == 0)){
    			errorMap.put("crc", "Invalid crc hex value");
    		}
    		
    		if(!(StringHelper.isBlank(encryptionKey)) && !(encryptionKey.length() % 2 == 0)){
    			errorMap.put("crc", "Invalid crc hex value");
    		}
    		
    		String[] octets = ipAddress.split("\\.");
    		String octet1 = octets[0];
    		String octet2 = octets[1];
    		String octet3 = octets[2];
    		String octet4 = octets[3];
    		
    		String[] transferStartValues = transferStart.split(":| |-");
    		String[] transferEndValues = transferEnd.split(":| |-");
    		
    		if(transferStartValues.length != 6){
    			errorMap.put("transfer_start", "Field failed validation: transfer_start");
    		}
    		
    		if(transferEndValues.length != 6){
    			errorMap.put("transfer_end", "Field failed validation: transfer_end");
    		}
    		
    		if(errorMap.size()>0){
    			Map<String, String> errorMapSorted = new TreeMap<String, String>(new ConfigFileActions.KeySorter());
    			errorMapSorted.putAll(errorMap);
    			form.setAttribute("errorMap", errorMapSorted);
                return;
    		}
    		
    		//date format should be like 03-31-2010 09:51:26
    		StringBuilder dateBuilder = new StringBuilder("");
    		Date reTransferStart = null;
    		Date reTransferEnd = null;
    		long reTransferStartSeconds = 0;
    		long reTransferEndSeconds = 0;
    		try{
	    		//append date
	    		dateBuilder.append(transferStartValues[0]).append("-").append(transferStartValues[1]).append("-").append(transferStartValues[2]);
	    		dateBuilder.append(" ");
	    		//append time
	    		dateBuilder.append(transferStartValues[3]).append(":").append(transferStartValues[4]).append(":").append(transferStartValues[5]);
	    		reTransferStart = (Date)FileActions.dateTimeformatter.parse(dateBuilder.toString()); 
	    		reTransferStartSeconds = (reTransferStart.getTime()/1000);
	    		dateBuilder = new StringBuilder("");
    		}catch(Exception e){
    			errorMap.put("transfer_start", "Field failed validation: transfer_start");
    			Map<String, String> errorMapSorted = new TreeMap<String, String>(new ConfigFileActions.KeySorter());
    			errorMapSorted.putAll(errorMap);
    			form.setAttribute("errorMap", errorMapSorted);
                return;
    			
    		}	
	    	try{
	    		//append date
	    		dateBuilder.append(transferEndValues[0]).append("-").append(transferEndValues[1]).append("-").append(transferEndValues[2]);
	    		dateBuilder.append(" ");
	    		//append time
	    		dateBuilder.append(transferEndValues[3]).append(":").append(transferEndValues[4]).append(":").append(transferEndValues[5]);
	    		reTransferEnd = (Date)FileActions.dateTimeformatter.parse(dateBuilder.toString()); 
	    		reTransferEndSeconds = (reTransferEnd.getTime()/1000);
    		}catch(Exception e){
    			errorMap.put("transfer_end", "Field failed validation: transfer_end");
    			Map<String, String> errorMapSorted = new TreeMap<String, String>(new ConfigFileActions.KeySorter());
    			errorMapSorted.putAll(errorMap);
    			form.setAttribute("errorMap", errorMapSorted);
                return;
    		}    			    	
	    	
	    	StringBuilder message = new  StringBuilder("");
	    	//encode protocol - Perl char encoding = pack("a",
	    	message.append(StringHelper.encodeHexString(protocol));
	    	//encode ip octets - Perl num encoding = pack("a",
	    	message.append(Integer.toHexString(0x10000 | (new Integer(octet1)).intValue()).substring(3));
	    	message.append(Integer.toHexString(0x10000 | (new Integer(octet2)).intValue()).substring(3));
	    	message.append(Integer.toHexString(0x10000 | (new Integer(octet3)).intValue()).substring(3));
	    	message.append(Integer.toHexString(0x10000 | (new Integer(octet4)).intValue()).substring(3));
	    	//encode port - Perl num encoding = pack("n*",
	    	message.append(Integer.toHexString(0x10000 | (new Integer(port)).intValue()).substring(1));
	    	//encode port - Perl char encoding = pack("a*",
	    	message.append(Integer.toHexString(0x10000 | (new Integer(loginName.length())).intValue()).substring(3));
	    	//encode port - Perl char encoding = pack("a*",
	    	message.append(StringHelper.encodeHexString(loginName));
	    	message.append(Integer.toHexString(0x10000 | (new Integer(loginPassword.length())).intValue()).substring(3));
	    	message.append(StringHelper.encodeHexString(loginPassword));
	    	message.append(Integer.toHexString(0x10000 | (new Integer(filePathName.length())).intValue()).substring(3));
	    	message.append(StringHelper.encodeHexString(filePathName));
	    	//assuming drop-down values are already in hex
	    	message.append(fileFormat);
	    	message.append(fileEncapsulation);
	    	message.append(crcType);
	    	//if passed validation, should be valid Hex already
	    	message.append(crc);
	    	//divide encryption key length by 2
	    	message.append(Integer.toHexString(0x10000 | (new Integer(encryptionKey.length()/2)).intValue()).substring(3));
	    	//if passed validation, should be valid Hex already
	    	message.append(encryptionKey);
	    	//put the start and end times in hex
	    	message.append(Long.toHexString(reTransferStartSeconds));
	    	message.append(Long.toHexString(reTransferEndSeconds));
	    	//put the retry info in hex
	    	message.append(Integer.toHexString(0x10000 | (new Integer(retries)).intValue()).substring(3));
	    	message.append(Integer.toHexString(0x10000 | (new Integer(retryInterval)).intValue()).substring(3));
	    	
	    	if(deviceTypeId == DeviceType.KIOSK.getValue()){
	    		message.append(Integer.toHexString(0x10000 | (new Integer(clientFilePath.length())).intValue()).substring(3));
	    		message.append(StringHelper.encodeHexString(loginName));
	    	}
	    	String command = message.toString().toUpperCase();
	    	
	    	if(command.length() > 250 && (deviceTypeId == DeviceType.G4.getValue()
	    			||deviceTypeId == DeviceType.GX.getValue()) ){
	    		errorMap.put("message", "Total message is too long!");
	    		Map<String, String> errorMapSorted = new TreeMap<String, String>(new ConfigFileActions.KeySorter());
    			errorMapSorted.putAll(errorMap);
    			form.setAttribute("errorMap", errorMapSorted);
                return;
	    	}
	    	
	    	if (bulk) {
	    		form.setAttribute("eft", command);
	    		form.setAttribute(SimpleServlet.ATTRIBUTE_FORWARD_URI_QUERY, form.getString("forward", true));
            	return;
	    	} else {	
	    		PendingCmdActions.createPendingCommand(null, deviceName, FileActions.externalFileTransferDataType, command, "P", 0, true);
	    	
		    	Calendar calNow = Calendar.getInstance();
		    	Date dateNow = new Date(calNow.getTimeInMillis());
		    	String dateCreated = (String)FileActions.dateTimeformatter.format(dateNow);
		    	
		    	//get Descs for drop-downs
		    	String protocolDesc = "";
		    	for(NameValuePair nvp : FileActions.protocols){
		    		if(((String)nvp.getValue()).equals(protocol)){
		    			protocolDesc = (String)nvp.getName();
		    		}
		    	}
		    	
		    	String fileFormatDesc = "";
		    	for(NameValuePair nvp : FileActions.fileFormats){
		    		if(((String)nvp.getValue()).equals(fileFormat)){
		    			fileFormatDesc = (String)nvp.getName();
		    		}
		    	}
		    	
		    	String fileEncapDesc = "";
		    	for(NameValuePair nvp : FileActions.fileEncapsulations){
		    		if(((String)nvp.getValue()).equals(fileEncapsulation)){
		    			fileEncapDesc = (String)nvp.getName();
		    		}
		    	}
		    	
		    	String crcTypeDesc = "";
		    	for(NameValuePair nvp : FileActions.crcTypes){
		    		if(((String)nvp.getValue()).equals(crcType)){
		    			crcTypeDesc = (String)nvp.getName();
		    		}
		    	}
		    	
		    	StringBuilder t =  new StringBuilder("<form method=\"post\"><input type=\"hidden\" name=\"device_name\" value=\"" + device.getDeviceName() + "\"><input type=\"hidden\" name=\"device_id\" value=\"" + device.getId() + "471\">");
		    	t.append("<table width=\"100%\" cellpadding=\"2\" cellspacing=\"0\" align=\"center\" class=\"border\">");
		    	t.append("<tr>");
		    	t.append("<td colspan=\"2\" align=\"center\"><B>Creating new pending External File Transfer Request...</B></td>");
		    	t.append("</tr>");
		    	t.append("<tr>");
		    	t.append("<td>Machine ID</td>");
		    	t.append("<td>" + device.getDeviceName() + "</td>");
		    	t.append("</tr>");
		    	t.append("<tr>");
		    	t.append("<td>Create Time</td>");
		    	t.append("<td>" + dateCreated + "</td>");
		    	t.append("</tr>");
		    	t.append("<tr>");
		    	t.append("<td>Data Type</td>");
		    	t.append("<td>" + FileActions.externalFileTransferDataType + "</td>");
		    	t.append("</tr>");
		    	t.append("<tr>");
		    	t.append("<td>Command</td>");
		    	t.append("<td><input type=\"text\" name=\"message\" tabindex=\"4\" value=\"" + command + "\" style=\"width:99%\" /></td>");
		    	t.append("</tr>");
		    	t.append("<tr>");
		    	t.append("<td>Command Length</td>");
		    	t.append("<td>" + command.length() + " bytes</td>");
		    	t.append("</tr>");
		    	
		    	t.append("<tr>");
		    	t.append("<td  colspan=2 align=center><B>Input Variables</B></td>");
		    	t.append("</tr>");
		    	t.append("<tr>");
		    	t.append("<td>Protocol</td>");
		    	t.append("<td>" + protocol + " : " + protocolDesc + "</td>");
		    	t.append("</tr>");
	
		    	t.append("<tr>");
		    	t.append("<td>Server IP Address</td>");
		    	t.append("<td>" + ipAddress + "</td>");
		    	t.append("</tr>");
		    	t.append("<tr>");
		    	t.append("<td>Server Port</td>");
		    	t.append("<td>" + port + "</td>");
		    	t.append("</tr>");
		    	t.append("<tr>");
		    	t.append("<td>Login Name</td>");
		    	t.append("<td>" + loginName + "</td>");
	
		    	t.append("</tr>");
		    	t.append("<tr>");
		    	t.append("<td>Login Password</td>");
		    	t.append("<td>" + loginPassword + "</td>");
		    	t.append("</tr>");
		    	t.append("<tr>");
		    	t.append("<td>File Path</td>");
		    	t.append("<td>" + filePath + "</td>");
		    	t.append("</tr>");
		    	t.append("<tr>");
		    	t.append("<td>File Name</td>");
		    	t.append("<td>" + fileName + "</td>");
	
		    	t.append("</tr>");
		    	t.append("<tr>");
		    	t.append("<td>File Path/Name</td>");
		    	t.append("<td>" + filePathName + "</td>");
		    	t.append("</tr>");
		    	t.append("<tr>");
		    	t.append("<td>File Format</td>");
		    	t.append("<td>" + fileFormat + " : " + fileFormatDesc + "</td>");
		    	t.append("</tr>");
		    	t.append("<tr>");
		    	t.append("<td>File Encapsulation</td>");
		    	t.append("<td>" + fileEncapsulation + " : " + fileEncapDesc + "</td>");
	
		    	t.append("</tr>");
		    	t.append("<tr>");
		    	t.append("<td>CRC Type</td>");
		    	t.append("<td>" + crcType + " : " + crcTypeDesc + "</td>");
		    	t.append("</tr>");
		    	t.append("<tr>");
		    	t.append("<td>CRC Value</td>");
		    	t.append("<td>" + crc + "</td>");
		    	t.append("</tr>");
		    	t.append("<tr>");
		    	t.append("<td>Encryption Key</td>");
		    	t.append("<td>" + encryptionKey + "</td>");
	
		    	t.append("</tr>");
		    	t.append("<tr>");
		    	t.append("<td>Transfer Start</td>");
		    	t.append("<td>" + transferStart + "</td>");
		    	t.append("</tr>");
		    	t.append("<tr>");
		    	t.append("<td>Transfer End</td>");
		    	t.append("<td>" + transferEnd + "</td>");
		    	t.append("</tr>");
		    	t.append("<tr>");
		    	t.append("<td>Retries</td>");
		    	t.append("<td>" + retries + "</td>");
	
		    	t.append("</tr>");
		    	t.append("<tr>");
		    	t.append("<td>Retry Interval</td>");
		    	t.append("<td>" + retryInterval + "</td>");
		    	t.append("</tr>");
		    	
		    	if(deviceTypeId == DeviceType.KIOSK.getValue()){
		    		t.append("<tr>");
			    	t.append("<td>Client File Path</td>");
			    	t.append("<td>" + clientFilePath + "</td>");
			    	t.append("</tr>");
		    	}
		    	
		    	t.append("<tr>");
		    	t.append("<td>Result</td>");
		    	t.append("<td><b>Command created successfully!</b></td>");
		    	t.append("</tr>");
		    	
		    	boolean emailSuccess = true;
		    	try{
		    		Helper.sendEmail(Helper.DMS_EMAIL, confirmEmail, "External File Transfer Confirmation: " + (device.getDeviceName()), t.toString(), Helper.DMS_EMAIL_CONTENT_TYPE_HTML);
		    	}catch(Exception e){
		    		emailSuccess = false;
		    	}
		    	t.append("<tr>");
		    	t.append("<td>Email Result</td>");
		    	if(!(StringHelper.isBlank(confirmEmail))){
		    		if(emailSuccess){
		    			
		    			t.append("<td><B>Confirmation sent successfully to " + confirmEmail + "!</B></td>");
		    		}else{
		    			t.append("<td color=\"red\"><B>Confirmation failed!</B></td>");
		    		}
		    		
		    	}else{
		    		t.append("<td><b>No Confirmation reuquested.</b></td>");
		    	}
		    	t.append("</tr>");
	
		    	
		    	t.append("<tr>");
		    	t.append("<td colspan=2 class=header align=center><input type=\"button\" class=\"cssButton\" value=\"&lt; Back to " + device.getDeviceName() + "\" onClick=\"javascript:window.location='deviceConfig.i?device_id=" + device.getId() + "';\"</td>");
		    	t.append("</tr>");
		    	t.append("</table>");
		    	t.append("</form>");
	    		    	
	    		form.setAttribute("resultsTable", t);
	    	}
	    	
        }catch(Exception e){
        	throw new ServletException(e);
        }
    }
}
