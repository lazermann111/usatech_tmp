/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.file;

import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.*;
import simple.servlet.RecordRequestFilter.CallInputs;
import simple.servlet.steps.AbstractStep;

import com.usatech.dms.action.FileActions;
import com.usatech.dms.model.DmsPrivilege;
import com.usatech.dms.servlet.DMSRecordRequestFilter;
import com.usatech.dms.util.DMSConstants;
import com.usatech.dms.util.Helper;
import com.usatech.layers.common.constants.FileType;
import com.usatech.layers.common.util.StringHelper;
import com.usatech.layers.common.util.WebHelper;

public class FileDetailsStep extends AbstractStep {
	private static final simple.io.Log log = simple.io.Log.getLog();

	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		long file_id = form.getLong(FileConstants.PARAM_FILE_ID, false, -1);
		String userOP = request.getParameter("userOP");

		if (file_id < 1) {
			form.setAttribute("missingParam", true);
			return;
		}

		try {
			// Get resultSet for FileDetails
			Results fileInfoResults = DataLayerMgr.executeQuery("GET_FILE_INFO", new Object[] {file_id}, false);
			fileInfoResults.next();
			int fileType = fileInfoResults.getValue("fileTransferTypeCd", Integer.class);
			fileInfoResults.setRow(0);
			form.setAttribute("fileInfoResults", fileInfoResults);
			if ("transfers".equalsIgnoreCase(userOP)) {
				// Get resultSet for TransferEvents
				Results deviceFileTransfers = DataLayerMgr.executeQuery("GET_DEVICE_FILE_TRANSFERS_BY_FILE_TRANSFER_ID", new Object[] {file_id}, false);
				form.setAttribute("deviceFileTransfers", deviceFileTransfers);
			}

			// Get file Content for Preview
			String fileContent = FileActions.getFileTransfer(file_id, null, DMSConstants.FILE_PREVIEW_SIZE, null).getFileContent();
			if (fileContent == null || fileContent.length() == 0) {
				form.setAttribute("lineBreaks", "Empty File");
				form.setAttribute("file_content", "");
				form.setAttribute("dataMode", "ASCII");
				return;
			}

			form.setAttribute("filePreviewSizeKB", DMSConstants.FILE_PREVIEW_SIZE / 1024);
			form.setAttribute("lineBreaks", Helper.getLineBreakType(fileContent, true));

			if (StringHelper.isDataPrintable(fileContent))
				form.setAttribute("dataMode", "ASCII");
			else {
				form.setAttribute("dataMode", "Hex");
				fileContent = StringHelper.encodeHexString(fileContent);
			}

			BasicServletUser loggedUser = (BasicServletUser)request.getSession().getAttribute(SimpleServlet.ATTRIBUTE_USER);
			if (fileType == FileType.NACHA_ACH_CCD_FILE.getValue() && !loggedUser.hasPrivilege(DmsPrivilege.DMS_ACH_ADMINS.getValue())) {
				form.setAttribute("file_content", "PERMISSION DENIED (NOT ACH ADMIN)");
			} else {
				if (fileType == FileType.NACHA_ACH_CCD_FILE.getValue()) {
					WebHelper.publishAppRequestRecord(DMSConstants.APP_CD, request, new DMSRecordRequestFilter(), new CallInputs(), System.currentTimeMillis(), "View", "NACHA File", Long.toString(file_id), log);
				}
				form.setAttribute("file_content", fileContent);
			}
		} catch (SQLException | DataLayerException | ServiceException | ConvertException e) {
			throw new ServletException(e);
		}
	}
}
