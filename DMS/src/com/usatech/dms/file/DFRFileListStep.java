package com.usatech.dms.file;

import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.app.DialectResolver;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;
import simple.util.LinkedHashMap;

import com.usatech.layers.common.util.PaginationUtil;
import com.usatech.layers.common.util.StringHelper;

public class DFRFileListStep extends AbstractStep {

	public static final String DEFAULT_SORT_INDEX = "-4";
	
	private static final String SQL_START  = "SELECT FC.FILE_CACHE_ID, FC.FILE_NAME, FCS.FILE_CACHE_STATE_DESC, TO_CHAR(FC.CREATED_TS, 'MM/DD/YYYY HH24:MI:SS') CREATED_TS, TO_CHAR(FC.LAST_UPDATED_TS, 'MM/DD/YYYY HH24:MI:SS') LAST_UPDATED_TS ";
		
	private static final String SQL_BASE = " FROM REPORT.FILE_CACHE FC JOIN REPORT.FILE_CACHE_STATE FCS ON FC.FILE_CACHE_STATE_ID = FCS.FILE_CACHE_STATE_ID ";
		
	private static final String[] SORT_FIELDS = {
		"FC.FILE_CACHE_ID", "FC.FILE_NAME", "FCS.FILE_CACHE_STATE_DESC", "FC.CREATED_TS", "FC.LAST_UPDATED_TS"
	};
	
	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		try {
			String action = form.getString("action", false);

			if ("List Files".equalsIgnoreCase(action)) {
				Map<Integer, Object> params = new LinkedHashMap<Integer, Object>();
				int paramCnt = 0;

				String dfrFromDate = form.getString("dfr_from_date", true);
				String dfrFromTime = form.getString("dfr_from_time", true);
				String dfrToDate = form.getString("dfr_to_date", true);
				String dfrToTime = form.getString("dfr_to_time", true);

				StringBuilder queryBase = new StringBuilder(SQL_BASE);

				queryBase.append(!DialectResolver.isOracle() 
						? " WHERE FC.CREATED_TS BETWEEN TO_TIMESTAMP(? || ' ' || ?, 'MM/DD/YYYY HH24:MI:SS') AND TO_TIMESTAMP(? || ' ' || ?, 'MM/DD/YYYY HH24:MI:SS') " 
						: " WHERE FC.CREATED_TS BETWEEN TO_DATE(? || ' ' || ?, 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE(? || ' ' || ?, 'MM/DD/YYYY HH24:MI:SS') ");
				params.put(paramCnt++, dfrFromDate);
				params.put(paramCnt++, dfrFromTime);
				params.put(paramCnt++, dfrToDate);
				params.put(paramCnt++, dfrToTime);

				String paramTotalCount = PaginationUtil.getTotalField(null);
				String paramPageIndex = PaginationUtil.getIndexField(null);
				String paramPageSize = PaginationUtil.getSizeField(null);
				String paramSortIndex = PaginationUtil.getSortField(null);

				int totalCount = form.getInt(paramTotalCount, false, -1);
				if (totalCount == -1 || !StringHelper.isBlank(action)) {
					Results total = DataLayerMgr.executeSQL("REPORT", new StringBuilder("SELECT COUNT(1) ").append(queryBase).toString(), params.values().toArray(), null);
					if (total.next()) {
						totalCount = total.getValue(1, int.class);
					} else {
						totalCount = 0;
					}
					request.setAttribute(paramTotalCount, String.valueOf(totalCount));
				}

				int pageIndex = form.getInt(paramPageIndex, false, 1);
				int pageSize = form.getInt(paramPageSize, false, PaginationUtil.DEFAULT_PAGE_SIZE);
				int minRowToFetch = PaginationUtil.getStartNum(pageSize, pageIndex);
				int maxRowToFetch = PaginationUtil.getEndNum(pageSize, pageIndex);

				String sortIndex = form.getString(paramSortIndex, false);
				sortIndex = StringHelper.isBlank(sortIndex) ? DEFAULT_SORT_INDEX : sortIndex;
				String orderBy = PaginationUtil.constructOrderBy(SORT_FIELDS, sortIndex);

				String query;
				if (DialectResolver.isOracle()) {
					query = new StringBuilder("select * from (select pagination_temp.*, ROWNUM rnum from (")
							.append(SQL_START).append(queryBase).append(orderBy)
							.append(") pagination_temp where ROWNUM <= ?) where rnum  >= ? ").toString();

				} else {
					query = new StringBuilder("select * from (select pagination_temp.*, row_number()over() rnum from (")
							.append(SQL_START).append(queryBase).append(orderBy)
							.append(" ) pagination_temp limit CAST (? AS numeric)) sq_end where rnum  >= CAST (? AS numeric) ")
							.toString();
				}
				
				params.put(paramCnt++, String.valueOf(maxRowToFetch));
				params.put(paramCnt++, String.valueOf(minRowToFetch));
				Results results = DataLayerMgr.executeSQL("REPORT", query, params.values().toArray(), null);
				request.setAttribute("resultlist", results);
			}
		} catch (Exception e) {
			throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
		}
	}
}
