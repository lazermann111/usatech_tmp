/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.file;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.BasicServletUser;
import simple.servlet.Dispatcher;
import simple.servlet.InputFile;
import simple.servlet.InputForm;
import simple.servlet.RecordRequestFilter.CallInputs;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;

import com.usatech.dms.action.FileActions;
import com.usatech.dms.model.DmsPrivilege;
import com.usatech.dms.model.FileTransfer;
import com.usatech.dms.servlet.DMSRecordRequestFilter;
import com.usatech.dms.util.DMSConstants;
import com.usatech.dms.util.Helper;
import com.usatech.layers.common.constants.FileType;
import com.usatech.layers.common.util.DeviceUtils;
import com.usatech.layers.common.util.StringHelper;
import com.usatech.layers.common.util.WebHelper;

public class FileDetailsFuncStep extends AbstractStep {
	private static final simple.io.Log log = simple.io.Log.getLog();

	protected static final Pattern BREAK_PATTERN = Pattern.compile("\r\n|\r|\n");

	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		try {
			String errorMsg;
			String evNumber = form.getString("ev_number", false);

			String serialNumber = "";

			if (StringHelper.isBlank(evNumber)) {
				serialNumber = form.getString("serial_number", false);
				if (!StringHelper.isBlank(serialNumber)) {
					Results results = DataLayerMgr.executeQuery("GET_DEVICE_INFO_BY_SERIAL_NUMBER", new Object[] {serialNumber.toUpperCase()}, false);
					if (results.next()) {
						evNumber = (String)results.get("device_name");
						form.set("ev_number", evNumber);
					} else {
						//Can not get Device Name from Serial Number: 
						form.setAttribute("missingParam", true);
						errorMsg = "Can not get Device Name from Serial Number: ";
						form.setAttribute("errorMsg", errorMsg);
						return;
					}
				}
			}

			if (!Helper.isLong(form.getStringSafely("file_transfer_id", "").trim())) {
				form.setAttribute("errorMsg", "Required Parameter Not Found: file_id");
				return;
			}

			long fileId = form.getLong(FileConstants.PARAM_FILE_ID, false, -1);
			if (fileId == -1) {
				form.setAttribute("errorMsg", "Required Parameter Not Found: file_id");
				return;
			}

			String fileName = "";
			int fileType = -1;
			String createDate = "";
			String lastUpdate = "";
			String comment = "";
			String lineBreakFormat = null;
			String fileContent;
			String breakReplacementStr = null;

			// Get resultSet for FileDetails
			Results fileInfoResults = DataLayerMgr.executeQuery("GET_FILE_INFO", new Object[] {fileId}, false);
			form.setAttribute("fileInfoResults", fileInfoResults);
			if (fileInfoResults.next()) {
				fileName = fileInfoResults.getFormattedValue("fileTransferName");
				fileType = ConvertUtils.getIntSafely(fileInfoResults.get("fileTransferTypeCd"), -1);
				createDate = fileInfoResults.getFormattedValue("createdTs");
				lastUpdate = fileInfoResults.getFormattedValue("lastUpdatedTs");
				comment = fileInfoResults.getFormattedValue("fileTransferComment");
				form.setAttribute("file_name", fileName);
				form.setAttribute("file_type", fileType);
				form.setAttribute("file_type_name", fileInfoResults.getFormattedValue("fileTransferTypeName"));
				form.setAttribute("create_date", createDate);
				form.setAttribute("last_update", lastUpdate);
				form.setAttribute("comment", comment);
				form.setAttribute("file_size", fileInfoResults.getFormattedValue("fileSize"));
				form.setAttribute("G9EnvironmentLetter", fileInfoResults.getFormattedValue("G9EnvironmentLetter"));
				form.setAttribute("GxEnvironmentLetter", fileInfoResults.getFormattedValue("GxEnvironmentLetter"));
			} else {
				form.setAttribute("missingParam", true);
				errorMsg = "File does not exist!";
				form.setAttribute("errorMsg", errorMsg);
				return;
			}

			BasicServletUser loggedUser = (BasicServletUser)request.getSession().getAttribute(SimpleServlet.ATTRIBUTE_USER);
			if (fileType == FileType.NACHA_ACH_CCD_FILE.getValue() && !loggedUser.hasPrivilege(DmsPrivilege.DMS_ACH_ADMINS.getValue())) {
				errorMsg = "Permission Denied\n";
				form.setAttribute("errorMsg", errorMsg);
				return;
			}

			String action = form.getString("myaction", false);

			if (fileType == FileType.NACHA_ACH_CCD_FILE.getValue()) {
				WebHelper.publishAppRequestRecord(DMSConstants.APP_CD, request, new DMSRecordRequestFilter(), new CallInputs(), System.currentTimeMillis(), action, "ACH File Type", Long.toString(fileId), log);
			}

			if ("Download File".equalsIgnoreCase(action)) {
				if (fileName.contains("/"))
					fileName = fileName.substring(fileName.lastIndexOf("/") + 1);
				else if (fileName.contains("\\"))
					fileName = fileName.substring(fileName.lastIndexOf("\\") + 1);

				Helper.streamFileToClient(response, fileName, fileId);
				return;
			} else if ("Edit File".equalsIgnoreCase(action)) {
				if (fileType != FileType.EDGE_DEFAULT_CONFIGURATION_TEMPLATE.getValue()) {
					FileTransfer fileTransfer = FileActions.getFileTransfer(fileId, null, DMSConstants.MAX_EDITABLE_FILE_SIZE, null);
					fileContent = fileTransfer.getFileContent();
					long fileContentSize = fileTransfer.getFileSize();
					form.setAttribute("fileContentSize", fileContentSize);
					if (StringHelper.isDataPrintable(fileContent)) {
						form.setAttribute("dataMode", "ASCII");
						if (fileContentSize <= DMSConstants.MAX_EDITABLE_FILE_SIZE) {
							form.setAttribute("file_content", fileContent);
							lineBreakFormat = Helper.getLineBreakType(fileContent, false);
							form.setAttribute("lineBreaks", lineBreakFormat);
						}
					} else {
						form.setAttribute("dataMode", "Hex");
						if (fileContentSize <= DMSConstants.MAX_EDITABLE_FILE_SIZE)
							form.setAttribute("file_content", StringHelper.encodeHexString(fileContent));
					}
				} else {
					errorMsg = "File " + fileName + " cannot be edited!<br/><br/><br/><input type='button' value='< Back' onClick='javascript:history.go(-1);'> ";
					form.setAttribute("errorMsg", errorMsg);
				}
				return;
			} else if ("Save Changes".equalsIgnoreCase(action)) {
				boolean gotUploadedFile = false;

				if (FileType.EDGE_DEFAULT_CONFIGURATION_TEMPLATE.getValue() != fileType) {
					comment = form.getString("file_comment", false);
					lineBreakFormat = form.getString("line_break_format", false);
					String contentType = request.getContentType();

					Map<String, Object> params = new HashMap<String, Object>();

					InputStream in = null;
					String uploadFileName = form.getString("file_data", false);
					if (contentType != null && contentType.indexOf("multipart/form-data") >= 0 && !StringHelper.isBlank(uploadFileName)) {
						in = ((InputFile)form.getAttribute("file_data")).getInputStream();
						if (in != null) {
							gotUploadedFile = true;
							params.put("file_content", in);
						}
					} else {
						if ("Hex".equalsIgnoreCase(form.getString("dataMode", true))) {
							String hexFileContent = form.getString("file_content", true);
							if (hexFileContent == null)
								fileContent = "";
							else if (StringHelper.isBlank(hexFileContent))
								fileContent = hexFileContent;
							else if (!StringHelper.isHex(hexFileContent))
								throw new ServletException("Received invalid hex file content");
							else fileContent = StringHelper.decodeHexString(hexFileContent);
						} else {
							fileContent = form.getString("file_content", true);
							Matcher matcher = BREAK_PATTERN.matcher(fileContent);
							if ("Unix Line Breaks".equalsIgnoreCase(lineBreakFormat))
								breakReplacementStr = "\n";
							else if ("Windows Line Breaks".equalsIgnoreCase(lineBreakFormat))
								breakReplacementStr = "\r\n";
							fileContent = matcher.replaceAll(breakReplacementStr);
						}

						gotUploadedFile = true;
						params.put("file_content", fileContent);
						form.setAttribute("lineBreaks", lineBreakFormat);
					}

					if (!gotUploadedFile)
						throw new ServletException("Did not find uploaded file in request");

					params.put("comment", comment);
					params.put("file_id", fileId);

					try {
						DataLayerMgr.executeCall("UPDATE_FILE_TRANSFER_CONTENT", params, true);
					} catch (Exception sqle) {
						errorMsg = "Unable to update File Content for " + fileName + " with sql error:<br/> " + sqle + "<br/>";
						form.setAttribute("errorMsg", errorMsg);
						return;
					}

					try {
						if (in != null)
							in.close();
					} catch (Exception e) {
					}

					form.set(FileConstants.PARAM_FILE_ID, fileId);
					form.setRedirectUri(new StringBuilder("/fileDetails.i?file_transfer_id=").append(fileId).toString());
					return;
				} else {
					errorMsg = "File " + fileName + " cannot be edited!<br/><br/><br/><input type='button' value='< Back' onClick='javascript:history.go(-1);'> ";
					form.setAttribute("errorMsg", errorMsg);
					return;
				}

			} else if ("Upload to Device".equalsIgnoreCase(action)) {
				if (fileType == FileType.CONFIGURATION_FILE.getValue())
					form.setAttribute("errorMsg", "Please use configuration editor to upload device configuration changes");
				//send off to client
				return;
			} else if ("Schedule Device Upload".equalsIgnoreCase(action)) {
				int packetSize = form.getInt("packet_size", true, -1);
				fileId = form.getLong("file_transfer_id", true, -1);
				int executeOrder = form.getInt("execute_order", true, 0);

				if (StringHelper.isBlank(evNumber)) {
					errorMsg = "Invalid Device Name\n";
					form.setAttribute("errorMsg", errorMsg);
					return;
				}

				if ((evNumber.length() == 8) || (("EV".equalsIgnoreCase(evNumber.substring(0, 2)) || ("TD".equalsIgnoreCase(evNumber.substring(0, 2)))))) {
				} else {
					errorMsg = "Invalid Device Name\n";
					form.setAttribute("errorMsg", errorMsg);
					return;
				}

				if (packetSize < 1) {
					errorMsg = "Invalid Packet Size\n";
					form.setAttribute("errorMsg", errorMsg);
					return;
				}

				if (executeOrder < 0) {
					errorMsg = "Invalid Execute Order\n";
					form.setAttribute("errorMsg", errorMsg);
					return;
				}

				long deviceId = -1;
				int deviceType = -1;

				try {
					Results results = DataLayerMgr.executeQuery("GET_DEVICE_INFO_FOR_EDIT_CONF_BY_EVNUMBER", new String[] {evNumber}, false);
					if (results.next()) {
						deviceId = ConvertUtils.getLongSafely(results.get("device_id"), -1);
						deviceType = ConvertUtils.getIntSafely(results.get("device_type_id"), -1);
					} else {
						errorMsg = "No device found for " + evNumber;
						form.setAttribute("errorMsg", errorMsg);
						return;
					}
				} catch (SQLException e) {
					log.warn("Failed to load device information...", e);
				} catch (DataLayerException e) {
					log.warn("Failed to load device information...", e);
				}

				DeviceUtils.sendCommand(deviceType, deviceId, evNumber, fileId, -1, packetSize, executeOrder, null, StringHelper.encodeHexString(fileName), true);

				form.setRedirectUri(new StringBuilder("/deviceConfig.i?device_id=").append(deviceId).toString());
				return;
			} else if ("Delete File".equalsIgnoreCase(action)) {
				deleteFile(fileId, loggedUser.getFullName());
				form.setRedirectUri(new StringBuilder("/fileDetails.i?file_transfer_id=").append(fileId).toString());
			} else if ("count_pending".equalsIgnoreCase(action)) {
				long count = getPendingTransfersCount(fileId);
				response.getWriter().print(count);
			}
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}

	private long getPendingTransfersCount(long fileId) {
		long count = -1;
		try {
			Results results = DataLayerMgr.executeQuery("GET_PENDING_COMMANDS_FOR_FILE", new Long[] {fileId}, false);
			if (results.next()) {
				count = ConvertUtils.getLongSafely(results.get("count"), -1);
			}
		} catch (Exception ex) {
			log.error(String.format("Failed to count pending commands for file_transfer_id=%d", fileId), ex);
		}
		return count;
	}

	private void deleteFile(long fileId, String username) throws SQLException, DataLayerException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("file_id", fileId);
		Connection conn = DataLayerMgr.getConnectionForCall("CANCEL_PENDING_COMMANDS_FOR_FILE");
		try {
			DataLayerMgr.executeQuery(conn, "CANCEL_PENDING_COMMANDS_FOR_FILE", params, false);
			params.put("username", username);
			DataLayerMgr.executeQuery(conn, "DELETE_FILE", params, false);
			conn.commit();
		} catch (SQLException | DataLayerException ex) {
			conn.rollback();
			log.error(String.format("Failed to delete file_transfer_id=%d", fileId), ex);
		} finally {
			conn.close();
		}
	}
}
