package com.usatech.dms.file;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.zip.InflaterInputStream;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usatech.layers.common.ProcessingUtils;

import simple.db.DataLayerMgr;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

public class DFRFileFuncStep extends AbstractStep {
	private static final simple.io.Log log = simple.io.Log.getLog();

	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		long dfrFileId = form.getLong("dfrFileId", false, -1);
		if (dfrFileId < 1)
			return;
		String action = request.getParameter("action");
    if ("Download File".equalsIgnoreCase(action)) {	
    	String sql = "SELECT FILE_CACHE_TYPE_ID, FILE_CONTENT FROM REPORT.FILE_CACHE WHERE FILE_CACHE_ID = ?";
      PreparedStatement pstmt = null;
      ResultSet rs = null;
      InputStream in = null;
      ServletOutputStream out = null;
      boolean success = false;
      Connection conn = null;      
      try
      {      	
      	String fileName = request.getParameter("fileName");
      	response.setContentType("application/force-download");
    		response.setHeader("Content-Transfer-Encoding", "binary"); 
				response.setHeader("Content-Disposition", "attachment;filename=\""
						+ (fileName == null ? "" : fileName.replaceAll("\r", "").replaceAll("\n", "")) + "\"");
    		out = response.getOutputStream();
      	
    		conn = DataLayerMgr.getConnection("REPORT");
        pstmt = conn.prepareCall(sql);
        pstmt.setLong(1, dfrFileId);
        rs = pstmt.executeQuery();
        if (rs != null && rs.next()) {
        	int fileCacheTypeId = rs.getInt(1);
        	if (fileCacheTypeId > 1)
        		in = new InflaterInputStream(rs.getBinaryStream(2));
        	else
        		in = rs.getBinaryStream(2);
         	byte[] bytes = new byte[1024];
         	int bytesRead; 
        	while ((bytesRead = in.read(bytes)) > 0) {
        		out.write(bytes, 0, bytesRead);
        		out.flush();
        	}
        }
        success = true;
    } catch(Exception e) {
    	throw new ServletException("Error retrieving DFR file content for dfrFileId: " + dfrFileId, e);
    }
    finally
    {
    	if (in != null) {
    		try {
            in.close();
        } catch (IOException e) {
            log.warn(">>> Failed to close database input stream...", e);
        }
    	}
    	
    	if (out != null) {
    		try {
            out.close();
        } catch (IOException e) {
            log.warn(">>> Failed to close output stream...", e);
        }
    	}    	
    	
        // close the result set
        if (rs != null)
        {
            try
            {
                rs.close();
            }
            catch (SQLException se)
            {
                log.warn(">>> Failed to close result set...", se);
            }
        }
        // close the prepared statement
        if (pstmt != null)
        {
            try
            {
                pstmt.close();
            }
            catch (SQLException se)
            {
                log.warn(">>> Failed to close prepared statement...", se);
            }
        }
        // close the database connection
        	if (!success)
    			ProcessingUtils.rollbackDbConnection(log, conn);    		
    		ProcessingUtils.closeDbConnection(log, conn);
      }
    }
	}
}
