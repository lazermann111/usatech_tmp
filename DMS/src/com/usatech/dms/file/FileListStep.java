/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.file;

import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usatech.dms.util.DMSPaginationStep;
import com.usatech.dms.util.Helper;
import com.usatech.layers.common.util.StringHelper;

import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.util.LinkedHashMap;

public class FileListStep extends DMSPaginationStep
{
    

    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
    	boolean bulk = form.getBoolean("bulk", false, false);
        String file_name = form.getString(FileConstants.PARAM_FILE_NAME, false);
        long file_id = -1;
        if (Helper.isLong(form.getStringSafely(FileConstants.PARAM_FILE_ID, "").trim()))
        	file_id = form.getLong(FileConstants.PARAM_FILE_ID, false, -1);
        String ev_number = form.getString("ev_number", false);
        String file_transfer_type_cd = form.getString(FileConstants.PARAM_FILE_TYPE_CD, false);
		String call_from_date = form.getString("call_from_date", false);
		boolean defaultDateRange = false;
		if (file_id <= 0 && StringHelper.isBlank(call_from_date)) {
			defaultDateRange = true;
			call_from_date = Helper.getFirstOfMonth();
		}
		String call_to_date = form.getString("call_to_date", false);
		if (file_id <= 0 && StringHelper.isBlank(call_to_date)) {
			defaultDateRange = true;
			call_to_date = Helper.getDefaultEndDate();
		}
		form.set("call_from_date", call_from_date);
		form.set("call_to_date", call_to_date);
        String file_name_search_char = form.getString("file_name_search_char", false);
        if(StringHelper.isBlank(file_name_search_char))
        	file_name_search_char="B";
                
        if(file_name == null)
        	file_name = "";
        else
        	file_name = file_name.trim();
        form.set("file_transfer_name", file_name);
        
        // We don't want to show deleted (deactivated) files in list ever.
        StringBuilder sql = new StringBuilder(" WHERE COALESCE(status_cd, 'A') <> 'D' ");
        Map<Integer, String> params = new LinkedHashMap<Integer, String>();
        int paramCntr = 0;
        String queryHint = "";
        if (file_id <= 0) {
			sql.append(" AND created_ts >= to_timestamp(?, 'mm/dd/yyyy hh24:mi:ss') AND created_ts <= to_timestamp(?, 'mm/dd/yyyy hh24:mi:ss')");
			params.put(paramCntr++, call_from_date + " 0:00:00");
			params.put(paramCntr++, call_to_date + " 23:59:59");
		}
        if (file_transfer_type_cd != null && file_transfer_type_cd.length() > 0)
        {
        	queryHint = "/*+INDEX(ft INX_FILE_TRANSFER_TYPE_NAME)*/";
        	if(sql.length() == 0)
        		sql.append(" WHERE ");
        	else
        		sql.append(" AND ");
        	sql.append(" file_transfer_type_cd = CAST (? AS smallint) ");
            params.put(paramCntr++, file_transfer_type_cd);
        }        
        if (file_name != null && file_name.length() > 0)
        {
        	queryHint = "/*+INDEX(ft IDX_FILE_TRANSFER_NAME)*/";
        	if(sql.length() == 0)
        		sql.append(" WHERE ");
        	else
        		sql.append(" AND ");
        	sql.append(" file_transfer_name LIKE ? ");
        	if("B".equalsIgnoreCase(file_name_search_char))
        		params.put(paramCntr++, file_name+"%");
        	else
        		params.put(paramCntr++, file_name);
        }
        if (file_id > 0)
        {
        	queryHint = "/*+INDEX(ft PK_FILE_TRANSFER)*/";
        	if(sql.length() == 0)
        		sql.append(" WHERE ");
        	else
        		sql.append(" AND ");
        	sql.append(" file_transfer_id = CAST (? AS numeric) ");
            params.put(paramCntr++, String.valueOf(file_id));
        }        
        
        String queryStart = "SELECT " + queryHint + FileConstants.SQL_GET_FILE_LIST_START;
        String queryBase = FileConstants.SQL_GET_FILE_LIST_BASE + sql.toString();
        String itemURL;
        if (bulk)
        	itemURL = null;
        else if (StringHelper.isBlank(ev_number))
        	itemURL = defaultDateRange ? null : "/fileDetails.i?file_transfer_id=";        
        else {
        	StringBuilder sb = new StringBuilder("/fileDetailsFunc.i?ev_number=").append(ev_number);
        	String serial_number = form.getString("serial_number", false);
        	if (!StringHelper.isBlank(serial_number))
        		sb.append("&serial_number=").append(serial_number);
        	String device_type_id = form.getString("device_type_id", false);
        	if (!StringHelper.isBlank(device_type_id))
        		sb.append("&device_type_id=").append(device_type_id);
        	sb.append("&myaction=Upload+to+Device&file_transfer_id=").toString();
        	itemURL = sb.toString();
        }
        
        //USAT-715 File Upload function not showing all Card Reader Application Firmware files
        if (defaultDateRange)
        	itemURL = null;
        
        setPaginatedResultsOnRequest(form, request, queryStart, queryBase, FileConstants.SQL_END, FileConstants.DEFAULT_SORT_INDEX_GET_FILE_LIST,
        		FileConstants.SORT_FIELDS_GET_FILE_LIST, "fileList", params!=null&&params.size()>0?params.values().toArray():null,
        				"file_transfer_id", itemURL);

    }
    
}
