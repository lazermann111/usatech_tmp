/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.file;

import simple.app.DialectResolver;

public class FileConstants
{        
    public static final String PARAM_FILE_ID = "file_transfer_id";
    public static final String PARAM_FILE_NAME = "file_transfer_name";
    public static final String PARAM_FILE_TYPE_CD = "file_transfer_type_cd";
    public static final String PARAM_FILE_COMMENT = "file_transfer_comment";    
    public static final String PARAM_FILE_CREATED = "created_ts";
    public static final String PARAM_FILE_UPDATED = "last_updated_ts";
    public static final String PARAM_FILE_SIZE = "file_size";

    public static final String SQL_GET_FILE_LIST_START = " file_transfer_id,"
        + " file_transfer_name,"
        + " file_transfer_type_cd,"
        + " file_transfer_comment,"
        + " TO_CHAR(created_ts, 'MM/DD/YYYY HH24:MI:SS'),"
        + " TO_CHAR(last_updated_ts, 'MM/DD/YYYY HH24:MI:SS') ";
    public static final String SQL_GET_FILE_LIST_BASE = " FROM device.file_transfer ft ";
    public static final String SQL_END = " ";
           
    public static final String[] SORT_FIELDS_GET_FILE_LIST = {"file_transfer_id",
        "file_transfer_name", 
        "file_transfer_type_cd", 
        "file_transfer_comment", 
        "created_ts", 
        "last_updated_ts",
        (!DialectResolver.isOracle() ? "LENGTH(ft.file_transfer_content)" : "DBMS_LOB.GETLENGTH(ft.file_transfer_content)")
        };
    
    public static final String DEFAULT_SORT_INDEX_GET_FILE_LIST = "2"; 
}