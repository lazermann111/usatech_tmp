package com.usatech.dms.report;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;
import simple.text.StringUtils;

public class DeviceCountByPostalCodeStep extends AbstractStep {
    public static final String FORM_PARAM_POSTAL_CODES = "postalCodes";
    public static final String FORM_PARAM_SEARCH_RADIUS_MILES = "searchRadiusMiles";
    public static final String ORA_PARAM_POSTAL_CODES = "POSTAL_CODES";
    public static final String ORA_PARAM_SEARCH_RADIUS_MILES = "SEARCH_RADIUS_MILES";
    public static final String ORA_QUERY_REPORT_DATA = "GET_DEVICE_COUNT_BY_POSTAL_CODE";

    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
        try {
            String postalCodes = StringUtils.getCSVParamList(form.getString(FORM_PARAM_POSTAL_CODES, true));
            float searchRadiusMiles = getRadiusMilesParam(form);

            Results reportData = queryReportData(postalCodes, searchRadiusMiles);
            prepareResponse(response, buildReportName(postalCodes));
            writeReportDataToCsv(response, reportData);
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    private float getRadiusMilesParam(InputForm form) throws ServletException {
        String searchRadiusMilesStr = form.getString(FORM_PARAM_SEARCH_RADIUS_MILES, true);
        try {
            return Float.valueOf(searchRadiusMilesStr);
        } catch (NumberFormatException e) {
            throw new ServletException("searchRadiusMiles has wrong format, should be float, actual: " + searchRadiusMilesStr, e);
        }
    }

    private void writeReportDataToCsv(HttpServletResponse response, Results reportData) throws IOException {
        PrintWriter out = response.getWriter();
        writeCsvHeader(out);
        writeCsvBody(out, reportData);
    }

    private Results queryReportData(String postalCodes, float searchRadiusMiles) throws SQLException, DataLayerException {
        Map<String, Object> sqlParams = new HashMap<>();
        sqlParams.put(ORA_PARAM_POSTAL_CODES, postalCodes);
        sqlParams.put(ORA_PARAM_SEARCH_RADIUS_MILES, searchRadiusMiles);
        return DataLayerMgr.executeQuery(ORA_QUERY_REPORT_DATA, sqlParams);
    }

    private void prepareResponse(HttpServletResponse response, String fileName) {
        response.setContentType("application/force-download");
        response.setHeader("Content-Transfer-Encoding", "binary");
        response.setHeader("Content-Disposition", "attachment;filename=\"" + fileName + "\"");
    }

    private String buildReportName(String postalCodes) {
        StringBuilder sb = new StringBuilder("CustomerDevicesByPostalCodes");
        int codesInNameCounter = 0;
        String[] postalCodeArray = postalCodes.split(",");
        for (String postalCode : postalCodeArray) {
            sb.append("_").append(postalCode);
            if (codesInNameCounter >= 5) {
                // Limit codes count in file name
                break;
            }
            codesInNameCounter++;
        }
        sb.append(".csv");
        return sb.toString();
    }

    private void writeCsvHeader(PrintWriter out) {
        out.write("\"Customer Name\",");
        out.write("\"Customer ID\",");
        out.write("\"First Name\",");
        out.write("\"Last Name\",");
        out.write("\"Email\",");
        out.write("\"Phone\",");
        out.write("\"Communication Method\",");
        out.write("\"Device Count\"");
        out.print("\r\n");
        out.flush();
    }

    private void writeCsvBody(PrintWriter out, Results reportData) {
        int i = 0;
        while (reportData.next()) {
            i++;
            out.write("\"");
            out.write(reportData.getFormattedValue("CUSTOMER_NAME"));
            out.write("\",");
            out.write("\"");
            out.write(reportData.getFormattedValue("CUSTOMER_ID"));
            out.write("\",");
            out.write("\"");
            out.write(reportData.getFormattedValue("FIRST_NAME"));
            out.write("\",");
            out.write("\"");
            out.write(reportData.getFormattedValue("LAST_NAME"));
            out.write("\",");
            out.write("\"");
            out.write(reportData.getFormattedValue("EMAIL"));
            out.write("\",");
            out.write("\"");
            out.write(reportData.getFormattedValue("PHONE"));
            out.write("\",");
            out.write("\"");
            out.write(reportData.getFormattedValue("COMM_METHOD"));
            out.write("\",");
            out.write("\"");
            out.write(reportData.getFormattedValue("DEVICE_COUNT"));
            out.write("\"");
            out.print("\r\n");
            if (i % 10 == 0)
                out.flush();
        }
        out.flush();
    }
}