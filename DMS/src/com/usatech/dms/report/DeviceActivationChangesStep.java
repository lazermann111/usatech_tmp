package com.usatech.dms.report;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

import com.usatech.dms.util.Helper;

public class DeviceActivationChangesStep extends AbstractStep{
	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		try {
			String fromDate = form.getStringSafely("device_activation_from_date", Helper.getFirstOfMonth());
			String toDate = form.getStringSafely("device_activation_to_date", Helper.getCurrentDate());
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("FROM_DATE", fromDate);
			params.put("TO_DATE", toDate);
			
			response.setContentType("application/force-download");
			response.setHeader("Content-Transfer-Encoding", "binary"); 
			response.setHeader("Content-Disposition", new StringBuilder("attachment;filename=\"Device Activation Changes ")
					.append(fromDate.replaceAll("/", "-"))
					.append(" to ")
					.append(toDate.replaceAll("/", "-"))
					.append(".csv\"").toString());
			
			PrintWriter out = response.getWriter();
			out.write("\"Customer Name\",");
			out.write("\"Product Type\",");
			out.write("\"Device Serial #\",");
			out.write("\"Start Date\",");
			out.write("\"End Date\",");
			out.write("\"Activation\",");
			out.write("\"Deactivation\",");
			out.write("\"Net Change\",");
			out.write("\"Distributor Name\",");
			out.write("\"Activation Reason/Details\",");
			out.write("\"Activation Reason/Details Additional\",");
			out.write("\"Activation Sales Rep\",");
			out.write("\"Activation Commission\",");
			out.write("\"Deactivation Reason/Details\",");
			out.write("\"Deactivation Reason/Details Additional\",");
			out.write("\"Deactivation Sales Rep\",");
			out.write("\"Deactivation Commission\",");
			out.write("\"Pricing Tier\",");
			out.write("\"Sales Category\"");
			out.print("\r\n");
			out.flush();
			
			int i = 0;
			Results rs = DataLayerMgr.executeQuery("GET_DEVICE_ACTIVATION_CHANGES", params);
			while (rs.next()) {
				i++;
				out.write("\""); out.write(rs.getFormattedValue("CUSTOMER_NAME")); out.write("\",");
			out.write("\""); out.write(rs.getFormattedValue("PRODUCT_TYPE_NAME")); out.write("\",");
			out.write("\""); out.write(rs.getFormattedValue("EPORT_SERIAL_NUM")); out.write("\",");
			out.write("\""); out.write(rs.getFormattedValue("START_DATE")); out.write("\",");
			out.write("\""); out.write(rs.getFormattedValue("END_DATE")); out.write("\",");
			out.write("\""); out.write(rs.getFormattedValue("ACTIVATION")); out.write("\",");
			out.write("\""); out.write(rs.getFormattedValue("DEACTIVATION")); out.write("\",");
			out.write("\""); out.write(String.valueOf(rs.getValue("ACTIVATION", int.class) - rs.getValue("DEACTIVATION", int.class))); out.write("\",");
			out.write("\""); out.write(rs.getFormattedValue("DISTRIBUTOR_NAME")); out.write("\",");
			out.write("\""); out.write(rs.getFormattedValue("ACTIVATE_DETAIL")); out.write("\",");
			out.write("\""); out.write(rs.getFormattedValue("ACTIVATE_DETAIL2")); out.write("\",");
			out.write("\""); out.write(rs.getFormattedValue("ACTIVATION_SALES_REP")); out.write("\",");
			out.write("\""); out.write(rs.getFormattedValue("ACTIVATION_COMMISSION")); out.write("\",");
			out.write("\""); out.write(rs.getFormattedValue("DEACTIVATE_DETAIL")); out.write("\",");
			out.write("\""); out.write(rs.getFormattedValue("DEACTIVATE_DETAIL2")); out.write("\",");
			out.write("\""); out.write(rs.getFormattedValue("DEACTIVATION_SALES_REP")); out.write("\",");
			out.write("\""); out.write(rs.getFormattedValue("DEACTIVATION_COMMISSION")); out.write("\",");
			out.write("\""); out.write(rs.getFormattedValue("PRICING_TIER")); out.write("\",");
			out.write("\""); out.write(rs.getFormattedValue("SALES_CATEGORY")); out.write("\"");
			out.print("\r\n");
			if (i % 10 == 0)
				out.flush();
			}
			out.flush();
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}
}