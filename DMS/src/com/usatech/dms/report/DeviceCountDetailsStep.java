package com.usatech.dms.report;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

import com.usatech.dms.util.Helper;

public class DeviceCountDetailsStep extends AbstractStep{
	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
	    try {
			String fromDate = form.getStringSafely("device_count_from_date", Helper.getFirstOfMonth());
			String toDate = form.getStringSafely("device_count_to_date", Helper.getCurrentDate());
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("FROM_DATE", fromDate);
			params.put("TO_DATE", toDate);
			
		    response.setContentType("application/force-download");
		    response.setHeader("Content-Transfer-Encoding", "binary"); 
		    response.setHeader("Content-Disposition", new StringBuilder("attachment;filename=\"Device Count Details ")
		    	.append(fromDate.replaceAll("/", "-")).append(" to ").append(toDate.replaceAll("/", "-")).append(".csv\"").toString());
		    
		    PrintWriter out = response.getWriter();
			out.write("\"Customer Name\",");
			out.write("\"Product Type\",");
			out.write("\"Start Count\",");
			out.write("\"End Count\",");
			out.write("\"Net Change\"");
			out.print("\r\n");
			out.flush();
		    
			int i = 0;
			Results rs = DataLayerMgr.executeQuery("GET_DEVICE_COUNT_DETAILS", params);
			while (rs.next()) {
				i++;
				out.write("\""); out.write(rs.getFormattedValue("CUSTOMER_NAME")); out.write("\",");
			    out.write("\""); out.write(rs.getFormattedValue("PRODUCT_TYPE_NAME")); out.write("\",");
			    out.write("\""); out.write(rs.getFormattedValue("START_DEVICE_COUNT")); out.write("\",");
			    out.write("\""); out.write(rs.getFormattedValue("END_DEVICE_COUNT")); out.write("\",");
			    out.write("\""); out.write(rs.getFormattedValue("DEVICE_COUNT_DIFF")); out.write("\"");
			    out.print("\r\n");
			    if (i % 10 == 0)
			    	out.flush();
			}
			out.flush();
	    } catch (Exception e) {
	    	throw new ServletException(e);
	    }
	}
}