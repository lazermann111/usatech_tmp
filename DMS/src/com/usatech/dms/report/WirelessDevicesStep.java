package com.usatech.dms.report;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

public class WirelessDevicesStep extends AbstractStep{
	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
	    try {
			String commMethodCd = form.getStringSafely("comm_method_cd", "-");
		    int days = form.getInt("days", false, 0);
		    if (days < 0)
		    	days = 0;
		    Map <String, Object> params = new HashMap<String, Object>();
		    params.put("COMM_METHOD_CD", commMethodCd);
		    params.put("LAST_ACTIVITY_DAYS", days);
		    
		    SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
			String date = format.format(new Date());
		    response.setContentType("application/force-download");
		    response.setHeader("Content-Transfer-Encoding", "binary"); 
		    response.setHeader("Content-Disposition","attachment;filename=\"devices_last_" + days + "_days_" + date + ".csv.txt\"");
		    
		    PrintWriter out = response.getWriter();
			out.write("\"Device Type\",");
			out.write("\"Serial #\",");
			out.write("\"Device Name\",");
			out.write("\"Firmware Version\",");
			out.write("\"Comm Method\",");
			out.write("\"Last known modem status\",");
			out.write("\"Modem Mfgr\",");
			out.write("\"Last Activity\",");
			out.write("\"MEID\","); //CDMA
			out.write("\"CNUM\","); //CDMA
			out.write("\"ICCID\","); //GPRS/HSPA
			out.write("\"IMEI\","); //GPRS/HSPA
			out.write("\"Terminal #\",");
			out.write("\"USALive Customer\",");
			out.write("\"USALive Location\",");
			out.write("\"Address\",");
			out.write("\"City\",");
			out.write("\"State\",");
			out.write("\"Zip\"");			
			out.print("\r\n");
			out.flush();
		    
			int i = 0; 
			Results rs = DataLayerMgr.executeQuery("GET_ACTIVE_DEVICES_BY_COMM_METHOD", params);
			while (rs.next()) {
				i++;
				out.write("\""); out.write(rs.getFormattedValue("DEVICE_TYPE_DESC")); out.write("\",");
			    out.write("\""); out.write(rs.getFormattedValue("DEVICE_SERIAL_CD")); out.write("\",");
			    out.write("\""); out.write(rs.getFormattedValue("DEVICE_NAME")); out.write("\",");
			    out.write("\""); out.write(rs.getFormattedValue("FIRMWARE_VERSION")); out.write("\",");
			    out.write("\""); out.write(rs.getFormattedValue("COMM_METHOD_NAME")); out.write("\",");
				out.write("\""); out.write(rs.getFormattedValue("LAST_KNOWN_MODEM_STATUS")); out.write("\",");
			    out.write("\""); out.write(rs.getFormattedValue("MODEM_MFGR")); out.write("\",");
			    out.write("\""); out.write(rs.getFormattedValue("LAST_ACTIVITY_TS")); out.write("\",");
			    if ("E".equalsIgnoreCase(rs.getFormattedValue("COMM_METHOD_NAME")) || "P".equalsIgnoreCase(rs.getFormattedValue("COMM_METHOD_NAME")))
			    	out.write("\"\",\"\",\"\",\"\",");
			    else if (rs.getFormattedValue("COMM_METHOD_NAME").contains("CDMA")) {
			    	out.write("\""); out.write(rs.getFormattedValue("HOST_SERIAL_CD")); out.write("\",");
					out.write("\""); out.write(rs.getFormattedValue("CNUM")); out.write("\",");
					out.write("\"\",\"\",");
			    } else {
			    	out.write("\"\",\"\",");
			    	out.write("\""); out.write(rs.getFormattedValue("HOST_SERIAL_CD")); out.write("\",");
					out.write("\""); out.write(rs.getFormattedValue("HOST_LABEL_CD")); out.write("\",");
			    }
			    out.write("\""); out.write(rs.getFormattedValue("TERMINAL_NBR")); out.write("\",");
			    out.write("\""); out.write(rs.getFormattedValue("CUSTOMER_NAME")); out.write("\",");
			    out.write("\""); out.write(rs.getFormattedValue("LOCATION_NAME")); out.write("\",");
			    out.write("\""); out.write(rs.getFormattedValue("ADDRESS1")); out.write(" "); out.write(rs.getFormattedValue("ADDRESS2")); out.write("\",");
			    out.write("\""); out.write(rs.getFormattedValue("CITY")); out.write("\",");
			    out.write("\""); out.write(rs.getFormattedValue("STATE")); out.write("\",");
			    out.write("\""); out.write(rs.getFormattedValue("ZIP")); out.write("\"");			    
			    out.print("\r\n");
			    if (i % 10 == 0)
			    	out.flush();
			}
			out.flush();
	    } catch (Exception e) {
	    	throw new ServletException(e);
	    }
	}
}