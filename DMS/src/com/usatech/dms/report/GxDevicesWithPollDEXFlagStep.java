package com.usatech.dms.report;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

public class GxDevicesWithPollDEXFlagStep extends AbstractStep{
	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
	    try {
			String minimum_firmware_version = form.getStringSafely("minimum_firmware_version", null);
		    SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
			String date = format.format(new Date());
		    response.setContentType("application/force-download");
		    response.setHeader("Content-Transfer-Encoding", "binary"); 
		    response.setHeader("Content-Disposition","attachment;filename=\"gx_devices_with_poll_dex_flag_" + date + ".csv.txt\"");
		    
		    PrintWriter out = response.getWriter();
			out.write("\"Customer\",");
			out.write("\"Serial #\",");
			out.write("\"Firmware Version\",");
			out.write("\"DEX Read and Send Time\",");
			out.write("\"DEX Read and Send Interval Minutes\",");
			out.write("\"DEX Read and Send Retry Minutes\",");
			out.write("\"DEX Read and Send Retry Count\",");
			out.write("\"DEX Password Enable\",");
			out.write("\"DEX Line Status Checking\"");
			out.print("\r\n");
			out.flush();
		    
			int i = 0;
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("MINIMUM_FIRMWARE_VERSION", minimum_firmware_version);
			Results rs = DataLayerMgr.executeQuery("GET_GX_DEVICES_WITH_POLL_DEX_FLAG", params);
			while (rs.next()) {
				i++;
			    out.write("\""); out.write(rs.getFormattedValue("CUSTOMER_NAME")); out.write("\",");
			    out.write("\""); out.write(rs.getFormattedValue("DEVICE_SERIAL_CD")); out.write("\",");
			    out.write("\""); out.write(rs.getFormattedValue("FIRMWARE_VERSION")); out.write("\",");
			    out.write("\""); out.write(rs.getFormattedValue("DEX_READ_AND_SEND_TIME")); out.write("\",");
			    out.write("\""); out.write(rs.getFormattedValue("DEX_READ_AND_SEND_INTERVAL_MINUTES")); out.write("\",");
			    out.write("\""); out.write(rs.getFormattedValue("DEX_READ_AND_SEND_RETRY_MINUTES")); out.write("\",");
			    out.write("\""); out.write(rs.getFormattedValue("DEX_READ_AND_SEND_RETRY_COUNT")); out.write("\",");
			    out.write("\""); out.write(rs.getFormattedValue("DEX_PASSWORD_ENABLE")); out.write("\",");
			    out.write("\""); out.write(rs.getFormattedValue("DEX_LINE_STATUS_CHECKING")); out.write("\"");
			    out.print("\r\n");
			    if (i % 10 == 0)
			    	out.flush();
			}
			out.flush();
	    } catch (Exception e) {
	    	throw new ServletException(e);
	    }
	}
}