package com.usatech.dms.report;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;
import simple.text.StringUtils;

import com.usatech.layers.common.ProcessingUtils;

public class FirstDeviceActivationStep extends AbstractStep {
	private static final simple.io.Log log = simple.io.Log.getLog();
	
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
      Connection conn = null;  
    	try {
          String serialNumbers = StringUtils.getCSVParamList(form.getString("serialNumbers", true));
          Map<String, Object> params = new HashMap<>();
          String[] serialNumberArray = serialNumbers.split(",");
          int count = 0;
          conn = DataLayerMgr.getConnection("OPER");
          for (String serialNumber : serialNumberArray) {
          	if (StringUtils.isBlank(serialNumber))
          		continue;
          	count++;
          	params.put("entryName", serialNumber);
          	params.put("entryValue", count);
          	DataLayerMgr.executeCall(conn, "INSERT_NUMERIC_MAP", params);
          }
          Results reportData = DataLayerMgr.executeQuery(conn, "GET_FIRST_DEVICE_ACTIVATION", null);
          prepareResponse(response, "FirstDeviceActivation.csv");
          writeReportDataToCsv(response, reportData);
      } catch (Exception e) {
          throw new ServletException(e);
      } finally {
      	ProcessingUtils.closeDbConnection(log, conn);
      }
    }

    private void writeReportDataToCsv(HttpServletResponse response, Results reportData) throws IOException {
        PrintWriter out = response.getWriter();
        writeCsvHeader(out);
        writeCsvBody(out, reportData);
    }

    private void prepareResponse(HttpServletResponse response, String fileName) {
        response.setContentType("application/force-download");
        response.setHeader("Content-Transfer-Encoding", "binary");
        response.setHeader("Content-Disposition", "attachment;filename=\"" + fileName + "\"");
    }

    private void writeCsvHeader(PrintWriter out) {
    	out.write("\"Device Serial Number\",");  
    	out.write("\"First Customer Name\",");
      out.write("\"First Activation Date\"");
      out.print("\r\n");
      out.flush();
    }

    private void writeCsvBody(PrintWriter out, Results reportData) {
        int i = 0;
        while (reportData.next()) {
            i++;
            out.write("\"");
            out.write(reportData.getFormattedValue("deviceSerialNumber"));
            out.write("\",");
            out.write("\"");
            out.write(reportData.getFormattedValue("firstCustomerName"));
            out.write("\",");
            out.write("\"");
            out.write(reportData.getFormattedValue("firstActivationDate"));
            out.write("\"");
            out.print("\r\n");
            if (i % 10 == 0)
                out.flush();
        }
        out.flush();
    }
}