package com.usatech.dms.report;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

public class FirmwareUpgradeStatusStep extends AbstractStep{
	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
	    try {
			int firmwareUpgradeId = form.getInt("firmware_upgrade_id", false, -1);
		    SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
			String date = format.format(new Date());
		    response.setContentType("application/force-download");
		    response.setHeader("Content-Transfer-Encoding", "binary"); 
		    response.setHeader("Content-Disposition","attachment;filename=\"firmware_upgrade_status_" + date + ".csv.txt\"");
		    
		    PrintWriter out = response.getWriter();
			out.write("\"Firmware Upgrade\",");
			out.write("\"Customer\",");
			out.write("\"Serial #\",");
			out.write("\"Status\",");
			out.write("\"Status Time\",");
			out.write("\"Complete Step\",");
			out.write("\"Complete Step Time\",");
			out.write("\"Current Step\",");
			out.write("\"Current Step Time\",");
			out.write("\"Current Step Attempt\",");
			out.write("\"Error\"");
			out.print("\r\n");
			out.flush();
		    
			int i = 0;
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("FIRMWARE_UPGRADE_ID", firmwareUpgradeId);
			Results rs = DataLayerMgr.executeQuery("GET_DEVICE_FIRMWARE_UPGRADE_STATUS", params);
			while (rs.next()) {
				i++;
				out.write("\""); out.write(rs.getFormattedValue("FIRMWARE_UPGRADE_NAME")); out.write("\",");
			    out.write("\""); out.write(rs.getFormattedValue("CUSTOMER_NAME")); out.write("\",");
			    out.write("\""); out.write(rs.getFormattedValue("DEVICE_SERIAL_CD")); out.write("\",");
			    out.write("\""); out.write(rs.getFormattedValue("DEVICE_FW_UPG_STATUS_NAME")); out.write("\",");
			    out.write("\""); out.write(rs.getFormattedValue("DEVICE_FW_UPG_STATUS_TS")); out.write("\",");
			    out.write("\""); out.write(rs.getFormattedValue("COMPLETE_FW_UPG_STEP_NAME")); out.write("\",");
			    out.write("\""); out.write(rs.getFormattedValue("COMPLETE_FW_UPG_STEP_TS")); out.write("\",");
			    out.write("\""); out.write(rs.getFormattedValue("CURRENT_FW_UPG_STEP_NAME")); out.write("\",");
			    out.write("\""); out.write(rs.getFormattedValue("CURRENT_FW_UPG_STEP_TS")); out.write("\",");
			    out.write("\""); out.write(rs.getFormattedValue("CURRENT_FW_UPG_STEP_ATTEMPT")); out.write("\",");
			    out.write("\""); out.write(rs.getFormattedValue("ERROR_MESSAGE")); out.write("\"");
			    out.print("\r\n");
			    if (i % 10 == 0)
			    	out.flush();
			}
			out.flush();
	    } catch (Exception e) {
	    	throw new ServletException(e);
	    }
	}
}