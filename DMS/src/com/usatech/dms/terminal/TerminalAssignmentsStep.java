package com.usatech.dms.terminal;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerMgr;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;

public class TerminalAssignmentsStep extends AbstractStep{
	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		try {		
			String action = form.getString("action", false);
			if ("Submit".equalsIgnoreCase(action)) {
				int userId = form.getInt("user_id", true, -1);
				String deviceList = form.getStringSafely("dev_list", "").replace('\n', ',').replace("\r", "").replace(" ", "");
				String allowEdit = form.getString("allow_edit", false) != null ? "Y" : "N";			
				DataLayerMgr.executeUpdate("ASSIGN_USER_TERMINALS", new Object[] {userId, deviceList, allowEdit}, true);
				form.setRedirectUri("/terminalAssignments.i?user_id=" + userId + "&msg=Changes+saved");
			}
		}catch(Exception e){
    		throw new ServletException("Error updating fees", e);
    	}
	}
}
