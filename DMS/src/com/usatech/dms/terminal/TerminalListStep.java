/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.usatech.dms.terminal;

import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.util.LinkedHashMap;

import com.usatech.dms.util.DMSPaginationStep;

public class TerminalListStep extends DMSPaginationStep
{
    private static final simple.io.Log log = simple.io.Log.getLog();

    // by default, sort by "Terminal Number" ascending
    private static final String DEFAULT_SORT_INDEX = "1,2,4";

    private static final String SQL_START = "SELECT terminal.terminal_id, "
    + "terminal.terminal_cd, "
    + "terminal.terminal_desc, " 
    + "terminal_state.terminal_state_name, "
    + "merchant.merchant_id, "
    + "merchant.merchant_cd, "
    + "merchant.merchant_name, "
    + "merchant.merchant_desc, "
    + "merchant.merchant_bus_name, "
    + "authority.authority_id, " 
    + "authority.authority_name, "
    + "authority_type.authority_type_name, "
    + "authority_type.authority_type_desc, "
    + "terminal.terminal_next_batch_num ";
    
    private static final String SQL_BASE = "FROM pss.terminal, pss.merchant, authority.authority, pss.terminal_state, authority.authority_type "
    	+ "WHERE terminal.merchant_id = merchant.merchant_id "
    	+ "AND merchant.authority_id = authority.authority_id "
    	+ "AND terminal.terminal_state_id = terminal_state.terminal_state_id "
    	+ "AND authority.authority_type_id = authority_type.authority_type_id "
    	+ "AND merchant.status_cd = 'A' AND terminal.status_cd = 'A' ";

    private static final String[] SORT_FIELDS = {"authority.authority_name","merchant.merchant_name", "merchant.merchant_cd", 
    	"terminal.terminal_cd", 
    	"terminal_state.terminal_state_name",
    	"terminal.terminal_next_batch_num"};

    /*
     * (non-Javadoc)
     * 
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm,
     * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
        int authorityId = form.getInt("authority_id", false, -1);
        int merchantId = form.getInt("merchant_id", false, -1);

        try
        {
        	Map params = new LinkedHashMap();
        	int paramCntr = 0;
            StringBuilder sql = new StringBuilder();
            //authority_id
            //sql.append(" AND case when -1 = " + authorityId + " then 1 ");
            sql.append(" AND case when -1 = ? then 1 ");
            params.put(paramCntr++, authorityId);
            //sql.append(" when authority.authority_id = " + authorityId + " then 1 ");
            sql.append(" when authority.authority_id = ? then 1 ");
            params.put(paramCntr++, authorityId);
            sql.append(" else 0");
            sql.append("  end = 1");
            //merchant_id
            //sql.append(" AND case when -1 = " + merchantId + " then 1 ");
            sql.append(" AND case when -1 = ? then 1 ");
            params.put(paramCntr++, merchantId);
            //sql.append(" when merchant.merchant_id = " + merchantId + " then 1 ");
            sql.append(" when merchant.merchant_id = ? then 1 ");
            params.put(paramCntr++, merchantId);
            sql.append(" else 0");
            sql.append("  end = 1");
            
            String queryBase = SQL_BASE + sql.toString();
            
            setPaginatedResultsOnRequest(form, request, SQL_START, queryBase, "", DEFAULT_SORT_INDEX,
            		SORT_FIELDS, "terminalList", ((params!=null&params.size()>0)?params.values().toArray():null),
            		"terminal_id", "terminalInfo.i?terminal_id=");

        }
        catch (Exception e)
        {
            throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
        }

    }

}
