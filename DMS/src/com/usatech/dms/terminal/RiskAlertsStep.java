/**
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.usatech.dms.terminal;

import static simple.text.MessageFormat.format;

import java.util.Date;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.app.DialectResolver;
import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;
import simple.util.LinkedHashMap;

import com.usatech.dms.util.Helper;
import com.usatech.layers.common.device.DevicesConstants;
import com.usatech.layers.common.util.PaginationUtil;
import com.usatech.layers.common.util.StringHelper;

public class RiskAlertsStep extends AbstractStep {
  private static final String DEFAULT_SORT_INDEX = "-7";

  //                                                          1                   2               3                    4                5                6                                                           7                 8         9         10                11              12  
  private static final String SQL_START = !DialectResolver.isOracle()
			? "WITH ra AS (select ra.*, report.get_device_for_terminal(ra.terminal_id, ra.alert_time) as device_serial_cd from report.risk_alert ra WHERE ra.status_cd != 'D') select t.terminal_id, d.device_serial_cd, t.terminal_nbr, dt.device_type_desc, l.location_name, c.customer_name, to_char(ra.alert_time, 'MM/DD/YYYY') alert_time, ra.risk_alert_id, ra.score, ras.name status_name, ra.alert_message, ra.customer_id "
			: "select t.terminal_id, d.device_serial_cd, t.terminal_nbr, dt.device_type_desc, l.location_name, c.customer_name, to_char(ra.alert_time, 'MM/DD/YYYY') alert_time, ra.risk_alert_id, ra.score, ras.name status_name, ra.alert_message, ra.customer_id ";

  private static final String SQL_BASE = !DialectResolver.isOracle()
			? "from ra join report.risk_alert_status ras on ras.risk_alert_status_cd = ra.status_cd left outer join corp.customer c on c.customer_id = ra.customer_id left outer join report.terminal t on t.terminal_id = ra.terminal_id left outer join device.device d on d.device_serial_cd = ra.device_serial_cd and d.device_active_yn_flag = 'Y' left outer join device.device_type dt on dt.device_type_id = d.device_type_id left outer join report.location l on t.location_id = l.location_id WHERE 1 = 1"
			: "from report.risk_alert ra join report.risk_alert_status ras on ras.risk_alert_status_cd = ra.status_cd left outer join corp.customer c on c.customer_id = ra.customer_id left outer join report.terminal t on t.terminal_id = ra.terminal_id left outer join device.device d on d.device_serial_cd = report.get_device_for_terminal(ra.terminal_id, ra.alert_time) and d.device_active_yn_flag = 'Y' left outer join device.device_type dt on dt.device_type_id = d.device_type_id left outer join report.location l on t.location_id = l.location_id where ra.status_cd != 'D' ";

  private static final String[] SORT_FIELDS = {"t.terminal_id", "d.device_serial_cd", "t.terminal_nbr", "dt.device_type_desc", "l.location_name", "c.customer_name", "ra.alert_time", "ra.risk_alert_id", "ra.score", "ras.name", "ra.alert_message", "ra.customer_id" };

  @Override
  public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
    try {
      String search_param = form.getString(DevicesConstants.PARAM_SEARCH_PARAM, false);
      String search_type = form.getString(DevicesConstants.PARAM_SEARCH_TYPE, false);
      
      String deviceTypeId = form.getString(DevicesConstants.PARAM_DEVICE_TYPE_ID, false);
      String customerId = form.getString("customerId", false);
      String terminalId = form.getString("terminalId", false);
      String statusCd = form.getString("statusCd", false);
      int daysOld = form.getInt("daysOld", false, 0);
      int minScore = form.getInt("minScore", false, 0);
      
      StringBuilder queryBase = new StringBuilder(SQL_BASE);
      Map<Integer, Object> params = new LinkedHashMap<Integer, Object>();
      int paramCnt = 0;
      
      if(!StringHelper.isBlank(customerId)) {
        queryBase.append(" and ra.customer_id = CAST(? AS NUMERIC) ");
        params.put(paramCnt++, customerId);
      } else if(!StringHelper.isBlank(terminalId)) {
        queryBase.append(" and ra.terminal_id = CAST(? AS NUMERIC) ");
        params.put(paramCnt++, terminalId);
      } else if(!StringHelper.isBlank(search_param)) {
        if("serial_number".equalsIgnoreCase(search_type)) {
          queryBase.append(" and lower(d.device_serial_cd) like lower(?) ");
          params.put(paramCnt++, Helper.convertParam(search_param, false));
        } else if("terminal_number".equalsIgnoreCase(search_type)) {
          queryBase.append(" and lower(t.terminal_nbr) like lower(?) ");
          params.put(paramCnt++, Helper.convertParam(search_param, false));
        } else if("customer_name".equalsIgnoreCase(search_type)) {
          queryBase.append(" and lower(c.customer_name) like lower(?) ");
          params.put(paramCnt++, Helper.convertParam(search_param, false));
        } else if("location_name".equalsIgnoreCase(search_type)) {
          queryBase.append(" and lower(l.location_name) like lower(?) ");
          params.put(paramCnt++, Helper.convertParam(search_param, false));
        }
      }
      
      if(!StringHelper.isBlank(deviceTypeId)) {
        queryBase.append(" and d.device_type_id = CAST(? AS NUMERIC) ");
        params.put(paramCnt++, deviceTypeId);
      }
      
      if(!StringHelper.isBlank(statusCd)) {
        queryBase.append(" and ra.status_cd = ? ");
        params.put(paramCnt++, statusCd);
      }
      
      if(daysOld > 0) {
		queryBase.append(!DialectResolver.isOracle()
			? " and ra.alert_time > localtimestamp - interval '1 day' * ?::numeric "
			: " and ra.alert_time > sysdate - ? ");
		params.put(paramCnt++, daysOld);
      }
      
      if(minScore > 0) {
        queryBase.append(" and ra.score >= CAST(? AS NUMERIC) ");
        params.put(paramCnt++, minScore);
      }
      
      String paramTotalCount = PaginationUtil.getTotalField(null);
      String paramPageIndex = PaginationUtil.getIndexField(null);
      String paramPageSize = PaginationUtil.getSizeField(null);
      String paramSortIndex = PaginationUtil.getSortField(null);

      int totalCount = form.getInt(paramTotalCount, false, -1);
      if(totalCount == -1) {
        Results total = DataLayerMgr.executeSQL("REPORT", (!DialectResolver.isOracle() 
        		? "WITH ra AS (select ra.*, report.get_device_for_terminal(ra.terminal_id, ra.alert_time) as device_serial_cd from report.risk_alert ra WHERE ra.status_cd != 'D') select count(1), max(ra.risk_alert_id) " 
        		: "select count(1), max(ra.risk_alert_id) ")
        		+ queryBase.toString(), params.values().toArray(), null);
        if(total.next()) {
          totalCount = total.getValue(1, int.class);
          if(totalCount == 1) {
            form.setRedirectUri(new StringBuilder("/riskAlert.i?riskAlertId=").append(total.getValue(2, String.class)).toString());
            return;
          }

        } else {
          totalCount = 0;
        }
        request.setAttribute(paramTotalCount, String.valueOf(totalCount));
      }

      int pageIndex = form.getInt(paramPageIndex, false, 1);
      int pageSize = form.getInt(paramPageSize, false, PaginationUtil.DEFAULT_PAGE_SIZE);
      int minRowToFetch = PaginationUtil.getStartNum(pageSize, pageIndex);
      int maxRowToFetch = PaginationUtil.getEndNum(pageSize, pageIndex);

      String sortIndex = form.getString(paramSortIndex, false);
      sortIndex = StringHelper.isBlank(sortIndex) ? DEFAULT_SORT_INDEX : sortIndex;
      String orderBy = PaginationUtil.constructOrderBy(SORT_FIELDS, sortIndex);

      String query;
      if (!DialectResolver.isOracle()) {
    	  query = new StringBuilder("select * from (").append(" select pagination_temp.*, row_number() over() as rnum from (").append(SQL_START).append(queryBase).append(orderBy).append(" LIMIT ?::bigint) pagination_temp) sq_end where rnum  >= ?::bigint").toString(); 
      } else {
    	  query = new StringBuilder("select * from (").append(" select pagination_temp.*, ROWNUM rnum from (").append(SQL_START).append(queryBase).append(orderBy).append(") pagination_temp where ROWNUM <= ?) where rnum  >= ?").toString(); 
      }

      params.put(paramCnt++, String.valueOf(maxRowToFetch));
      params.put(paramCnt++, String.valueOf(minRowToFetch));

      Results results = DataLayerMgr.executeSQL("REPORT", query, params.values().toArray(), null);
      request.setAttribute("resultlist", results);
      
    } catch(Exception e) {
      throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
    }

  }
}
