/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.usatech.dms.terminal;

import static simple.text.MessageFormat.format;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;

import com.usatech.dms.action.TerminalActions;
import com.usatech.dms.model.RiskAlertDetails;

public class RiskAlertStep extends AbstractStep {
  @Override
  public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
    long riskAlertId = form.getLong("riskAlertId", true, 0);
    String action = form.getString("action", false);

    try {
      RiskAlertDetails riskAlert = TerminalActions.getRiskAlert(riskAlertId);
      
      if(action != null && action.equals("Save Changes")) {
        TerminalActions.updateRiskAlert(riskAlert.getId(), form.getString("statusCd", true), form.getString("notes", false));
        form.setRedirectUri(format("/riskAlert.i?riskAlertId={0}&statusMessage={1}", riskAlertId, "Risk Alert Updated"));
        return;
      }
      
      form.setAttribute("riskAlert", TerminalActions.getRiskAlert(riskAlertId));
    } catch(Exception e) {
      throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
    }
  }
}
