package com.usatech.dms.terminal;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usatech.dms.servlet.DMSRecordRequestFilter;
import com.usatech.dms.util.DMSConstants;
import com.usatech.dms.util.Helper;
import com.usatech.layers.common.util.StringHelper;
import com.usatech.layers.common.util.WebHelper;

import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RecordRequestFilter.CallInputs;
import simple.servlet.steps.AbstractStep;

public class UpdateProcessFeesStep extends AbstractStep{
	private static final simple.io.Log log = simple.io.Log.getLog();
	
	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		try {
			String list = form.getString("dev_list", false);
			if(StringHelper.isBlank(list))
				return;
	
			int transTypeId = form.getInt("trans_type_id", true, -1);
			
			BigDecimal feeAmount = form.getBigDecimal("fee_amount", false);
			if(feeAmount == null)
				feeAmount = new BigDecimal(0);
			BigDecimal feePercent = form.getBigDecimal("fee_percent", false);
			if(feePercent == null)
				feePercent = new BigDecimal(0);
			BigDecimal feeMinimum = form.getBigDecimal("fee_minimum", false);
			if(feeMinimum == null)
				feeMinimum = new BigDecimal(0);

			String terminal_date_str = form.getString("terminal_date", false);
			if (StringHelper.isBlank(terminal_date_str) || terminal_date_str.length() < 10)
				terminal_date_str = Helper.getCurrentDate();
			Date terminalDate = ConvertUtils.convert(Date.class, new StringBuilder(terminal_date_str).append(" ").append(form.getString("terminal_time", false)).toString());
			if (terminalDate == null)
				terminalDate = new Date();
			
			String effective_date_str = form.getString("effective_date", false);
			if (StringHelper.isBlank(effective_date_str) || effective_date_str.length() < 10)
				effective_date_str = Helper.getCurrentDate();
			Date effectiveDate = ConvertUtils.convert(Date.class, new StringBuilder(effective_date_str).append(" ").append(form.getString("effective_time", false)).toString());
			if (effectiveDate == null)
				effectiveDate = new Date();
			
			String override = form.getString("override", false) != null ? "Y" : "N";
		
			BigDecimal commissionAmount = form.getBigDecimal("commission_amount", false);
			BigDecimal commissionPercent = form.getBigDecimal("commission_percent", false);
			BigDecimal commissionMinimum = form.getBigDecimal("commission_minimum", false);
			BigDecimal commissionBankId = form.getBigDecimal("commission_bank_id", false);

			StringBuilder err = new StringBuilder();
			int updatedCount = 0;
			Results rs;
			String flatList = list.replace('\n', ',').replace("\r", "").replace(" ", "");
			Map<String, Object> params = new HashMap<String, Object>();
			if ("terminal".equalsIgnoreCase(form.getString("device_number_type", false))) {
				params.put("terminalNbrs", flatList);
				rs = DataLayerMgr.executeQuery("GET_TERMINALS", params);
			} else {
				params.put("eportSerialNums", flatList);
				params.put("terminalDate", terminalDate);
				rs = DataLayerMgr.executeQuery("GET_BULK_TERMINAL_INFO", params);
			}
			DMSRecordRequestFilter rrf = new DMSRecordRequestFilter();
			CallInputs ci = new CallInputs();
			while (rs.next()) {
				long startTsMs = System.currentTimeMillis();
				long terminalId = rs.getValue("terminalId", long.class);
				try {
					DataLayerMgr.executeUpdate("PROCESS_FEES_UPD", new Object[] {terminalId, transTypeId, feePercent, feeAmount, feeMinimum, effectiveDate, override, commissionPercent, commissionAmount, commissionMinimum, commissionBankId}, true);
					updatedCount++;
					WebHelper.publishAppRequestRecord(DMSConstants.APP_CD, request, rrf, ci, startTsMs, "Submit", "terminal", String.valueOf(terminalId), log);
				} catch (Exception e) {
					err.append("Error updating fee for terminal ").append(rs.getFormattedValue("terminalNbr")).append(": ").append(e.getMessage()).append("<br /><br />");
					log.error(new StringBuilder("Error updating fee for terminal ").append(rs.getFormattedValue("terminalNbr")).toString(), e);
				}
			}
			request.setAttribute("message", "Fees updated: " + updatedCount);
			if (err.length() > 0)
				request.setAttribute("error", err.toString());
		}catch(Exception e){
    		throw new ServletException("Error updating fees", e);
    	}
	}
}
