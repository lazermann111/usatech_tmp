/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.usatech.dms.terminal;

import java.io.PrintWriter;
import java.math.BigDecimal;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

import com.usatech.dms.action.TerminalActions;
import com.usatech.layers.common.model.CustomerBankTerminal;
import com.usatech.dms.model.Terminal;
import com.usatech.dms.model.TerminalEport;
import com.usatech.dms.util.Helper;
import com.usatech.layers.common.util.StringHelper;

public class EditTerminalsChangeStep extends AbstractStep
{
    /*
     * (non-Javadoc)
     * 
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm,
     * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
        Long terminalId = form.getLong("terminalId", false, 0);

        Long paymentScheduleId = form.getLong("paymentScheduleId", false, -1);
        Long feeCurrencyId = form.getLong("feeCurrencyId", false, -1);
        Long businessUnitId = form.getLong("businessUnitId", false, -1);
        String assetNumber = form.getString("assetNumber", false);
        String customerTerminalNumber = form.getString("customerTerminalNumber", false);
        String userOP = form.getString("userOP", false);
        
        BigDecimal avgTransAmt = form.getBigDecimal("avgTransAmt", false);
        avgTransAmt = avgTransAmt == null ? BigDecimal.ZERO : avgTransAmt;
        
        BigDecimal maxTransAmt = form.getBigDecimal("maxTransAmt", false);
        maxTransAmt = maxTransAmt == null ? BigDecimal.ZERO : maxTransAmt;
        
        BigDecimal avgTransCnt = form.getBigDecimal("avgTransCnt", false);
        avgTransCnt = avgTransCnt == null ? BigDecimal.ZERO : avgTransCnt;

        try
        {
            if ("SaveChanges".equals(userOP))
            {
                TerminalActions.updateTerminal(assetNumber, customerTerminalNumber, paymentScheduleId, feeCurrencyId, businessUnitId, avgTransAmt, maxTransAmt, avgTransCnt, terminalId);
            }

            PrintWriter out = response.getWriter();
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");

            out.println("<Terminals>");
            Terminal terminal = TerminalActions.getTerminalDetails(terminalId);
            setTerminalDetails(terminal, out);
            out.println("</Terminals>");
            out.close();
        }
        catch (Exception e)
        {
            throw new ServletException(e);
        }

    }

    private void setTerminalDetails(Terminal terminal, PrintWriter out)
    {
        out.println("<Terminal><TerminalId>" + terminal.getId() + "</TerminalId>");
        out.println("<TerminalNumber>" + (StringHelper.isBlank(terminal.getTerminalNumber()) ? "" : terminal.getTerminalNumber()) + "</TerminalNumber>");
        out.println("<CustomerName>" + (StringHelper.isBlank(terminal.getCustomerName()) ? "" : terminal.getCustomerName()) + "</CustomerName>");
        out.println("<LocationName>" + (StringHelper.isBlank(terminal.getLocationName()) ? "" : terminal.getLocationName()) + "</LocationName>");
        out.println("<AssetNumber>" + (StringHelper.isBlank(terminal.getAssetNumber()) ? "" : terminal.getAssetNumber()) + "</AssetNumber>");
        out.println("<CustomerTerminalNumber>" + (StringHelper.isBlank(terminal.getCustomerTerminalNumber()) ? "" : terminal.getCustomerTerminalNumber()) + "</CustomerTerminalNumber>");
        out.println("<PaymentScheduleId>" + terminal.getPaymentScheduleId() + "</PaymentScheduleId>");
        out.println("<CurrencyId>" + terminal.getFeeCurrencyId() + "</CurrencyId>");
        out.println("<BusinessUnitId>" + terminal.getBusinessUnitId() + "</BusinessUnitId>");
        for (TerminalEport eport : terminal.getTerminalEports())
        {
            out.println("<TerminalEport>");
            out.println("<TerminalEportId>" + eport.getId() + "</TerminalEportId>");
            out.println("<EportId>" + eport.getEportId() + "</EportId>");
            out.println("<StartDate>" + (eport.getStartDate() == null ? "" : Helper.getDateTime(eport.getStartDate())) + "</StartDate>");
            out.println("<EndDate>" + (eport.getEndDate() == null ? "" : Helper.getDateTime(eport.getEndDate())) + "</EndDate>");
            out.println("<EportSerialNumber>" + eport.getEportSerialNumber() + "</EportSerialNumber>");
            out.println("</TerminalEport>");
        }
        for (CustomerBankTerminal bank : terminal.getCustomerBankTerminals())
        {
            out.println("<CustomerBankTerminal>");
            out.println("<CustomerBankId>" + bank.getCustomerBankId() + "</CustomerBankId>");
            out.println("<StartDate>" + (bank.getStartDate() == null ? "" : Helper.getDateTime(bank.getStartDate())) + "</StartDate>");
            out.println("<EndDate>" + (bank.getEndDate() == null ? "" : Helper.getDateTime(bank.getEndDate())) + "</EndDate>");
            out.println("<BankAccountNumber>" + bank.getBankAccountNumber() + "</BankAccountNumber>");
            out.println("</CustomerBankTerminal>");
        }
        out.println("</Terminal>");
    }

}
