package com.usatech.dms.terminal;

import java.math.BigDecimal;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usatech.layers.common.util.StringHelper;

import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

public class ViewFeesStep  extends AbstractStep{
	private static final simple.io.Log log = simple.io.Log.getLog();

	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException 
	{
		try {
			String[] terminalId = form.getStringArray("terminalId",false);
			
			//update ePort Quick Start Inactivity days
			BigDecimal eportQSInactivityDays = form.getBigDecimal("eportQSInactivityDays",false);
			Object[] params = new Object[] {eportQSInactivityDays, terminalId};
			DataLayerMgr.executeUpdate("UPDATE_EPORT_QS_INACTIVE_DAYS", params, true);
			
			String[] ids = form.getStringArray("update",false);

			if(ids.length>0) {
				int minMaxErrorCount = 0;
				for(int i=0;i<ids.length;i++) {
					BigDecimal	dynamicMin = form.getBigDecimal("dynamicMin_"+ids[i],false);
					if(dynamicMin==null)
						dynamicMin=new BigDecimal(0);
					BigDecimal	dynamicMax = form.getBigDecimal("dynamicMax_"+ids[i],false);
					if(dynamicMax==null)
						dynamicMax=new BigDecimal(0);
					if (dynamicMin.compareTo(dynamicMax) == 1) minMaxErrorCount++;
				}
				if (minMaxErrorCount == 0){
					for(int i=0;i<ids.length;i++) {
						String name =ids[i].substring(0,1);
					
						if(name.equalsIgnoreCase("P")) {
							int transTypeId = Integer.parseInt(ids[i].substring(1));
						
							BigDecimal percent = form.getBigDecimal("feepercent_"+ids[i],false);
							if(percent==null)
								percent=new BigDecimal(0);
						
							BigDecimal amount = form.getBigDecimal("feeamount_"+ids[i],false);
							if(amount==null)
								amount=new BigDecimal(0);
						
							BigDecimal min = form.getBigDecimal("feemin_"+ids[i],false);
							if(min==null)
								min=new BigDecimal(0);
						
							Date startDate;
							String date1 = form.getString("date1_"+ids[i],false);
							if (StringHelper.isBlank(date1) || date1.length() < 19)
								startDate = new Date();
							else
								startDate = ConvertUtils.convert(Date.class, date1);
							if (startDate == null)
								startDate = new Date();
						
							String override = form.getString("override_"+ids[i],false);
							override = (override!=null)?"Y":"N";
						
							BigDecimal commissionPercent = form.getBigDecimal("commissionpercent_"+ids[i], false);
							BigDecimal commissionAmount = form.getBigDecimal("commissionamount_"+ids[i], false);
							BigDecimal commissionMin = form.getBigDecimal("commissionmin_"+ids[i], false);
							int commissionBankId = form.getInt("commissionbank_" + ids[i], false, 0);

							params = new Object[] {terminalId, transTypeId, percent, amount, min,startDate, override, commissionPercent, commissionAmount, commissionMin, commissionBankId};
							DataLayerMgr.executeCall("PROCESS_FEES_UPD", params, true);
						} else if(name.equalsIgnoreCase("S")) {
							int feeId = Integer.parseInt(ids[i].substring(1));
							int frequencyId = -1;
							if(feeId==8){// if fee is percent of net revenue, then frequency is when paid, otherwise monthly
								frequencyId=6;
							}else{
								frequencyId=form.getInt("serviceFeeFrequencyId_S"+feeId,false, 2);
							}

							BigDecimal amount = form.getBigDecimal("feeamount_"+ids[i],false);
							if(amount==null)
								amount=new BigDecimal(0);
							BigDecimal percent = form.getBigDecimal("feepercent_"+ids[i],false);
							if(percent==null)
								percent=new BigDecimal(0);
							BigDecimal	dynamicMin = form.getBigDecimal("dynamicMin_"+ids[i],false);
							if(dynamicMin==null)
								dynamicMin=new BigDecimal(0);
							BigDecimal	dynamicMax = form.getBigDecimal("dynamicMax_"+ids[i],false);
							if(dynamicMax==null)
								dynamicMax=new BigDecimal(0);
							BigDecimal	dynamicAmount = form.getBigDecimal("dynamicAmount_"+ids[i],false);
							if(dynamicAmount==null)
								dynamicAmount=new BigDecimal(0);
							Date startDate;
							String date1 = form.getString("date1_"+ids[i],false);
							if (StringHelper.isBlank(date1) || date1.length() < 19)
								startDate = new Date();
							else
								startDate = ConvertUtils.convert(Date.class, date1);
							if (startDate == null)
								startDate = new Date();

							String date2 = form.getString("date2_"+ids[i],false);
							Date endDate = ConvertUtils.convert(Date.class, date2);

							String override = form.getString("override_"+ids[i],false);	
							override = (override!=null)?"Y":"N";

							int graceDays = form.getInt("gracedays_" + ids[i], false, 60);
							String noTriggerEventFlag = form.getStringSafely("noTriggerEventFlag_"+ids[i], null);

							BigDecimal inactiveAmount = form.getBigDecimal("inactivefeeamount_" + ids[i], false);

							BigDecimal commissionAmount = form.getBigDecimal("commissionamount_"+ids[i], false);
							int commissionBankId = form.getInt("commissionbank_" + ids[i], false, 0);

							Integer inactiveFeeRemaining = ConvertUtils.convert(Integer.class, form.getAttribute("inactivefeeremaining_" + ids[i]));

							params = new Object[] { terminalId, feeId, frequencyId, amount, percent, startDate, endDate, override, graceDays, noTriggerEventFlag, inactiveAmount, inactiveFeeRemaining, commissionAmount, commissionBankId, dynamicMin, dynamicMax, dynamicAmount};
							DataLayerMgr.executeCall("SERVICE_FEES_UPD", params, true);
						}
					}
					request.setAttribute("statusMessage", "Fees updated");
					}else {
						request.setAttribute("statusMessage", "Error: Minimum value must be less than maximum value for dynamic fees.");
				}
			} else {
				request.setAttribute("statusMessage", "No fees selected for update");
			}
			
		} catch(Exception e) {
			request.setAttribute("statusMessage", "Error:"+e.getMessage());
			log.error("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
		}
	}
}
