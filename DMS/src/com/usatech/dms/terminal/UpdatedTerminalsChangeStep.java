/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.usatech.dms.terminal;

import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

import com.usatech.dms.action.TerminalActions;
import com.usatech.dms.model.TerminalChange;

public class UpdatedTerminalsChangeStep extends AbstractStep
{

    /*
     * (non-Javadoc)
     * 
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm,
     * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
        Long terminalId = form.getLong("terminalId", false, 0);

        try
        {
            PrintWriter out = response.getWriter();
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");

            out.println("<Terminals>");
            List<TerminalChange> terminalChanges = TerminalActions.getTerminalChanges(terminalId);
            for (TerminalChange terminalChange : terminalChanges)
            {
                out.println("<Terminal>");
                out.println("<EportSerialNumber>" + terminalChange.getEportSerialNumber() + "</EportSerialNumber>");
                out.println("<Attribute>" + terminalChange.getAttribute() + "</Attribute>");
                out.println("<OldValue>" + terminalChange.getOldValue() + "</OldValue>");
                out.println("<NewValue>" + terminalChange.getNewValue() + "</NewValue>");
                out.println("<UpdateDate>" + terminalChange.getUpdateDate() + "</UpdateDate>");
                out.println("</Terminal>");
            }
            out.println("</Terminals>");
            out.close();
        }
        catch (Exception e)
        {
            throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
        }

    }

}
