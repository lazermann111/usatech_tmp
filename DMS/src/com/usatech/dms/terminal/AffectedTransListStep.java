/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.usatech.dms.terminal;

import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;
import simple.text.StringUtils;

public class AffectedTransListStep extends AbstractStep
{
    /*
     * (non-Javadoc)
     * 
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm,
     * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
        String dateTimeTxt = form.getString("dateTimeTxt", false);
        String dateStr = form.getString("dateStr", false);
		try {
            PrintWriter out = response.getWriter();
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");

            Date originalDate = ConvertUtils.convert(Date.class, dateStr);
            Date newDate = ConvertUtils.convert(Date.class, dateTimeTxt);
			Date startDateParam;
			Date endDateParam;

			if(originalDate.after(newDate)) {
				startDateParam = newDate;
				endDateParam = originalDate;
			} else {
				startDateParam = originalDate;
				endDateParam = newDate;
            }

			Results result = DataLayerMgr.executeQuery("GET_ACTIVATION_AFFECTED_TRANS_SUMMARY", form);
			out.println("<tr class=\"gridHeader\"><td>Customer</td><td>Transactions</td><td>Total Amount</td><td>Earliest</td><td>Latest</td></tr>");
			while(result.next()) {
				out.print("<tr><td>");
				out.print(StringUtils.prepareHTML(result.getFormattedValue("customer_name")));
				out.print("</td><td>");
				out.print(StringUtils.prepareHTML(result.getFormattedValue("tran_count")));
				out.print("</td><td>");
				out.print(StringUtils.prepareHTML(result.getFormattedValue("total_amount")));
				out.print("</td><td>");
				out.print(StringUtils.prepareHTML(result.getFormattedValue("first_date")));
				out.print("</td><td>");
				out.print(StringUtils.prepareHTML(result.getFormattedValue("last_date")));
				out.println("</td></tr>");
            }
			out.print("<tr><td colspan=\"5\" class=\"center bold\">Showing transactions from ");
			out.print(StringUtils.prepareHTML(ConvertUtils.formatObject(startDateParam, "DATE:MM/dd/yyyy HH:mm")));
			out.println(" to ");
			out.print(StringUtils.prepareHTML(ConvertUtils.formatObject(endDateParam, "DATE:MM/dd/yyyy HH:mm")));
			out.println("</td></tr>");
			out.print("<script type=\"text/javascript\">document.getElementById(\"overrideChk\").disabled = ");
			out.print(result.getRowCount() == 0);
			out.println(";</script>");
            out.close();
        }
        catch (Exception e)
        {
            throw new ServletException(e);
        }
    }

}
