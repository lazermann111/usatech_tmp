package com.usatech.dms.terminal;

import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerMgr;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.util.StringHelper;

public class ColumnMappingDetailStep extends AbstractStep{
	private static final simple.io.Log log = simple.io.Log.getLog();

	@Override
	public void perform(Dispatcher dispatcher, InputForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException {
		
	    int terminalId = form.getInt("terminalId", false, -1);
	    String act = form.getString("act", false);
	    
		try{
			if(act.equalsIgnoreCase("updateMappings")){
				String mappings = form.getString("mappings", true);				
				Connection conn = null;
				boolean success = false;
				try {
					conn = DataLayerMgr.getConnection("REPORT");
					DataLayerMgr.executeCall(conn, "TERMINAL_COLUMN_MAP_DEL", new Object[] {terminalId});
					if (!StringHelper.isBlank(mappings)) {
						mappings = mappings.replaceAll("\r", "\n");
						String[] lines = mappings.split("\n", -1);
						for (String line: lines) {
							if (StringHelper.isBlank(line))
								continue;
							String[] mapping = line.split("=", -1);
							if (mapping.length > 1) {
								String mdb = mapping[0];
								String vend = mapping[1];
								if (!StringHelper.isBlank(mdb) && !StringHelper.isBlank(vend))
									DataLayerMgr.executeCall(conn, "TERMINAL_COLUMN_MAP_INS", new Object[] {terminalId, mdb.trim(), vend.trim()});
							}
						}
					}
					conn.commit();
					success = true;
					request.setAttribute("statusMessage", "Column Mappings updated");
				} finally {
	                if (!success)
	        			ProcessingUtils.rollbackDbConnection(log, conn);    		
	        		ProcessingUtils.closeDbConnection(log, conn);
				}
			}else if(act.equalsIgnoreCase("delcol")){
				String[] ids = form.getStringArray("pid",true);
				
				if(ids.length>0){
					for(int i=0;i<ids.length;i++){
				        Object[] params = new Object[] {terminalId, ids[i]};
				        DataLayerMgr.executeCall("TERMINAL_COLUMN_MAP_DEL2", params, true);
					}
				}
				request.setAttribute("statusMessage", "Column Mappings deleted");
		    }else if(act.equalsIgnoreCase("addnew")){
		    	String mdb = form.getString("mdb", true);
		    	String vend = form.getString("vend", true);
		    	if (!StringHelper.isBlank(mdb) && !StringHelper.isBlank(vend)) { 
		    		Object[] params = new Object[] {terminalId, mdb.trim(), vend.trim()};
		    		DataLayerMgr.executeCall("TERMINAL_COLUMN_MAP_INS", params, true);
		    		request.setAttribute("statusMessage", "Column Mapping added");
		    	}
		    }else if(act.equalsIgnoreCase("delete")){	    	
		        Object[] params = new Object[] {terminalId };
		        DataLayerMgr.executeCall("TERMINAL_COLUMN_MAP_DEL", params, true);
		        request.setAttribute("statusMessage", "Column Mappings deleted");
		    }else if(act.equalsIgnoreCase("copy")){
		    	String modelId = form.getString("modelId", true);
		    	
		        Object[] params = new Object[] {terminalId, modelId };
		        DataLayerMgr.executeCall("TERMINAL_COLUMN_MAP_COPY", params, true);
		        request.setAttribute("statusMessage", "Column Mappings copied");
		    }

		}catch(Exception e){
	        request.setAttribute("statusMessage", "Error:"+e.getMessage());
	        request.setAttribute("statusClass", "status-error");
    		log.error("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
    	}
	}
}
