/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.usatech.dms.terminal;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.app.DialectResolver;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;
import simple.text.StringUtils;

import com.usatech.dms.util.Helper;
import com.usatech.layers.common.util.PaginationUtil;
import com.usatech.layers.common.util.StringHelper;

public class NewTerminalsStep extends AbstractStep
{
    private static final simple.io.Log log = simple.io.Log.getLog();

    // by default, sort by "Terminal Number" ascending
    private static final String DEFAULT_SORT_INDEX = "1";

	private static final String SQL_START = "SELECT t.terminal_id as terminalId, t.customer_id as customerId, " + "t.terminal_nbr as terminalNumber, t.customer_name as customerName, "
			+ "t.location_name as locationName, TO_CHAR(t.create_date, 'MM/DD/YYYY HH24:MI:SS') as createdDate, "
			+ "t.eport_serial_num as eportSerialNumber, "
			+ "t.primary_contact as primaryContact, "
			+ "d.dealer_name as program, "
			+ "t.activated_by as createdBy, "
			+ "ad.ACTIVATE_DETAIL || ' ' || te.ACTIVATE_DETAIL as activationReason, "
			+ "sr.first_name || ' ' || sr.last_name as salesperson ";

	private static final String SQL_BASE = "FROM report.vw_new_terminals t "
			+ " LEFT JOIN REPORT.TERMINAL_EPORT te on te.TERMINAL_ID = t.TERMINAL_ID"
			+ " LEFT JOIN REPORT.EPORT e on e.EPORT_ID = te.EPORT_ID"
			+ " LEFT JOIN CORP.DEALER_EPORT de on te.EPORT_ID = de.EPORT_ID"
			+ " LEFT JOIN CORP.DEALER d on de.DEALER_ID = d.DEALER_ID "
			+ " LEFT JOIN REPORT.ACTIVATE_DETAIL ad ON te.ACTIVATE_DETAIL_ID = ad.ACTIVATE_DETAIL_ID"
			+ " LEFT JOIN REPORT.VW_current_device_sales_rep ce on e.EPORT_SERIAL_NUM = ce.device_serial_cd"
			+ " LEFT JOIN REPORT.sales_rep sr on sr.sales_rep_id = ce.sales_rep_id ";
			//+ " WHERE (TE.START_DATE IS NULL OR TE.START_DATE <= SYSDATE ) AND (TE.END_DATE IS NULL OR TE.END_DATE > SYSDATE)";

	private static final String[] SORT_FIELDS = {"terminalNumber", "customerName", "locationName",
			"t.create_date", "eportSerialNumber", "primaryContact", "program", "createdBy", "activationReason", "salesperson"};

    /*
     * (non-Javadoc)
     * 
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm,
     * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
        try
        {
        	String action = form.getString("action", false);		
    		if (!StringUtils.isBlank(action)) {    				
    			if ("Accept".equalsIgnoreCase(action)) {
    				String[] ids = form.getStringArray("pid", false);
    				if(ids.length > 0){
    					for(int i=0; i < ids.length; i++)
    				        DataLayerMgr.executeCall("ACCEPT_NEW_TERMINAL", new Object[] {ids[i]}, true);
    				}
    			}
    		}
    		
    		String queryBase = SQL_BASE;
        	String terminalNumber = form.getString("terminalNumber", false);
        	if (!StringHelper.isBlank(terminalNumber))
            	queryBase += "where terminal_nbr like ? ";

            String paramTotalCount = PaginationUtil.getTotalField(null);
            String paramPageIndex = PaginationUtil.getIndexField(null);
            String paramPageSize = PaginationUtil.getSizeField(null);
            String paramSortIndex = PaginationUtil.getSortField(null);

            // pagination parameters
            int totalCount = form.getInt(paramTotalCount, false, -1);
            if (totalCount == -1 || !StringHelper.isBlank(action))
            {
                // if the total count is not retrieved yet, get it now
                Results total = DataLayerMgr.executeSQL("REPORT", "SELECT COUNT(1) " + queryBase, (!StringHelper.isBlank(terminalNumber)?
                		new Object[]{Helper.convertParam(terminalNumber)}:null), null);
                if (total.next())
                {
                    totalCount = total.getValue(1, int.class);
                }
                else
                {
                    totalCount = 0;
                }
                log.info(">>>> " + totalCount + " Records found....");
                request.setAttribute(paramTotalCount, String.valueOf(totalCount));
            }

            int pageIndex = form.getInt(paramPageIndex, false, 1);
            int pageSize = form.getInt(paramPageSize, false, PaginationUtil.DEFAULT_PAGE_SIZE);
            int minRowToFetch = PaginationUtil.getStartNum(pageSize, pageIndex);
            int maxRowToFetch = PaginationUtil.getEndNum(pageSize, pageIndex);

            String sortIndex = form.getString(paramSortIndex, false);
            sortIndex = StringHelper.isBlank(sortIndex) ? DEFAULT_SORT_INDEX : sortIndex;
            String orderBy = PaginationUtil.constructOrderBy(SORT_FIELDS, sortIndex);

			String query;
			if (!DialectResolver.isOracle()) {
				query = "select * from (" + " select pagination_temp.*, row_number() over() as rnum from (" + SQL_START
						+ queryBase + orderBy + " limit ?::bigint) pagination_temp)sq_end where rnum  >= ?::bigint";
			} else {
				query = "select * from (" + " select pagination_temp.*, ROWNUM rnum from (" + SQL_START + queryBase
						+ orderBy + ") pagination_temp where ROWNUM <= ?) where rnum  >= ?";
			}

            Results terminalList = DataLayerMgr.executeSQL("REPORT", query, (!StringHelper.isBlank(terminalNumber)?
            		new Object[]{Helper.convertParam(terminalNumber),maxRowToFetch,minRowToFetch}:new Object[]{maxRowToFetch,minRowToFetch}), null);
            request.setAttribute("terminalList", terminalList);
        }
        catch (Exception e)
        {
            throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
        }
    }

}
