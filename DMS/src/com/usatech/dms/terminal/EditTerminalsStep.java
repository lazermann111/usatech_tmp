/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.usatech.dms.terminal;

import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertUtils;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;
import simple.text.StringUtils;

import com.usatech.dms.action.TerminalActions;
import com.usatech.dms.model.Terminal;
import com.usatech.dms.util.Helper;
import com.usatech.layers.common.util.StringHelper;

public class EditTerminalsStep extends AbstractStep
{

    /*
     * (non-Javadoc)
     * 
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm,
     * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
        Long terminalId = form.getLong("terminalId", false, 0);

        Long eportId = form.getLong("eportId", false, -1);
        Long terminalEportId = form.getLong("terminalEportId", false, -1);
        Long customerBankId = form.getLong("customerBankId", false, -1);

        String startDateStr = form.getString("startDateStr", false);
        String endDateStr = form.getString("endDateStr", false);
        String newDate = form.getString("dateTxt", false);
        if (StringHelper.isBlank(newDate) || newDate.length() < 10)
        	newDate = Helper.getCurrentDate();
        String newTime = form.getString("timeTxt", false);
        String overrideChk = form.getString("overrideChk", false);

        int action = form.getInt("dateAction", false, 0);

        try
        {
            if (action == TerminalActions.ACT_CHANGE_START_DATE)
            {
                Date startDateParam = ConvertUtils.convert(Date.class, newDate + " " + newTime);
                Date endDateParam = StringHelper.isBlank(endDateStr) ? null : ConvertUtils.convert(Date.class, endDateStr);
                TerminalActions.updateTerminalEport(terminalId, eportId, startDateParam, endDateParam, terminalEportId, overrideChk);
            }
            else if (action == TerminalActions.ACT_CHANGE_END_DATE)
            {
                Date startDateParam = ConvertUtils.convert(Date.class, startDateStr);
                Date endDateParam = ConvertUtils.convert(Date.class, newDate + " " + newTime);
                TerminalActions.updateTerminalEport(terminalId, eportId, startDateParam, endDateParam, terminalEportId, overrideChk);
            }
            else if (action == TerminalActions.ACT_CHANGE_BANK_ACCT_DATE)
            {
                Date updatedDate = ConvertUtils.convert(Date.class, newDate + " " + newTime);
                TerminalActions.reassignBankAccount(terminalId, customerBankId, updatedDate);
            }
            else if (action == TerminalActions.ACT_DEACTIVATE_EPORT)
            {
                Date updatedDate = ConvertUtils.convert(Date.class, newDate + " " + newTime);
                String deactivateDetail=form.getString("deactivateDetail", false);
                if(!StringUtils.isBlank(deactivateDetail)){
                	deactivateDetail=deactivateDetail.trim();
                }
                TerminalActions.updateTerminalEport(null, eportId, updatedDate, null, null, overrideChk, null,deactivateDetail,null,form.getInt("deactivateDetailId", false, 1));
            }
            
            if (action > 0) {
            	form.setRedirectUri(new StringBuilder("/editTerminals.i?terminalId=").append(terminalId).toString());
            	return;
            }
            
            form.setAttribute("customers", TerminalActions.getCustomers());
            form.setAttribute("paymentSchedules", TerminalActions.getPaymentSchedules());
            form.setAttribute("currencies", TerminalActions.getCurrencies());
            form.setAttribute("businessUnits", TerminalActions.getBusinessUnits());
            form.setAttribute("riskAlerts", TerminalActions.getRiskAlerts(terminalId));

            Terminal terminal = TerminalActions.getTerminalDetails(terminalId);
            form.setAttribute("terminal", terminal);

        }
        catch (Exception e)
        {
            throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
        }

    }

}
