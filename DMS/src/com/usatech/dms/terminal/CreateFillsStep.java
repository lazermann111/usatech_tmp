package com.usatech.dms.terminal;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usatech.dms.action.DeviceActions;
import com.usatech.dms.servlet.DMSRecordRequestFilter;
import com.usatech.dms.util.DMSConstants;
import com.usatech.dms.util.Helper;
import com.usatech.layers.common.util.StringHelper;
import com.usatech.layers.common.util.WebHelper;

import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RecordRequestFilter.CallInputs;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;

public class CreateFillsStep extends AbstractStep{
	private static final simple.io.Log log = simple.io.Log.getLog();
	
	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		try {		
			String list = form.getString("dev_list", false);
			if(StringHelper.isBlank(list))			
				return;
			String nums[] = list.split("\n", -1);
			String action = form.getString("myaction", true);
			if(StringHelper.isBlank(action) || !"Submit".equalsIgnoreCase(action))
				return;
			
			String fill_date = form.getString("fill_date", false);
			if (StringHelper.isBlank(fill_date) || fill_date.length() < 10)
				fill_date = Helper.getCurrentDate();
			String fill_time = form.getString("fill_time", false);
		
			StringBuilder err = new StringBuilder();
			int fillCount = 0;
			Object[] results;
			DMSRecordRequestFilter rrf = new DMSRecordRequestFilter();
			CallInputs ci = new CallInputs();
			for(int i=0;i<nums.length;i++){
				long startTsMs = System.currentTimeMillis();
		        String serialNum = nums[i].trim().toUpperCase();
				try {
					results = DataLayerMgr.executeCall("CREATE_FILL", new Object[] {serialNum, fill_date, fill_time}, true);
					if (ConvertUtils.getLongSafely(results[1], 0) > 0) {
						fillCount++;
						String deviceName = DeviceActions.getDeviceName(serialNum);
						WebHelper.publishAppRequestRecord(DMSConstants.APP_CD, request, rrf, ci, startTsMs, "Submit", "device", deviceName, log);
					}
				} catch (Exception e) {
					err.append("Error creating a fill for ").append(serialNum).append(": ").append(e.getMessage()).append("<br /><br />");
					log.error(new StringBuilder("Error creating a fill for ").append(serialNum).toString(), e);
				}				
			}			
			if (err.length() > 0) {
				request.setAttribute("message", "Fills created: " + fillCount);
				request.setAttribute("error", err.toString());
			} else
				form.setRedirectUri("/createFills.i?message=Fills+created:+" + fillCount);
		}catch(Exception e){
    		throw new ServletException("Error creating fills", e);
    	}
	}
}
