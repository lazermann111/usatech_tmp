package com.usatech.dms.terminal;

import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.app.DialectResolver;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;
import simple.util.LinkedHashMap;

import com.usatech.dms.util.Helper;
import com.usatech.layers.common.device.DevicesConstants;
import com.usatech.layers.common.util.PaginationUtil;
import com.usatech.layers.common.util.StringHelper;

public class TerminalListStep2 extends AbstractStep{
	
    private static final String DEFAULT_SORT_INDEX = "-6";

    private static final String SQL_START = "SELECT T.TERMINAL_ID, E.EPORT_SERIAL_NUM, T.TERMINAL_NBR, DT.DEVICE_TYPE_DESC, L.LOCATION_NAME, C.CUSTOMER_NAME, TO_CHAR(T.CREATE_DATE, 'MM/DD/YYYY HH24:MI:SS') CREATE_DATE, " +
    		"TO_CHAR(TE.START_DATE, 'MM/DD/YYYY HH24:MI:SS') START_DATE, TO_CHAR(TE.END_DATE, 'MM/DD/YYYY HH24:MI:SS') END_DATE, C.CUSTOMER_ID ";

    private static final String SQL_BASE = new StringBuilder("FROM report.terminal T JOIN CORP.CUSTOMER C ON T.CUSTOMER_ID = C.CUSTOMER_ID ")
    	.append("JOIN REPORT.LOCATION L ON T.LOCATION_ID = L.LOCATION_ID ")
    	.append("LEFT OUTER JOIN (SELECT DISTINCT ")
    	.append("FIRST_VALUE(TERMINAL_ID) OVER(PARTITION BY TERMINAL_ID ORDER BY END_DATE DESC, START_DATE DESC) TERMINAL_ID, ")
    	.append("FIRST_VALUE(EPORT_ID) OVER(PARTITION BY TERMINAL_ID ORDER BY END_DATE DESC, START_DATE DESC) EPORT_ID, ")
    	.append("FIRST_VALUE(START_DATE) OVER(PARTITION BY TERMINAL_ID ORDER BY END_DATE DESC, START_DATE DESC) START_DATE, ")
    	.append("FIRST_VALUE(END_DATE) OVER(PARTITION BY TERMINAL_ID ORDER BY END_DATE DESC, START_DATE DESC) END_DATE ")
    	.append("FROM REPORT.TERMINAL_EPORT) TE ON T.TERMINAL_ID = TE.TERMINAL_ID ")
    	.append("LEFT OUTER JOIN REPORT.EPORT E ON TE.EPORT_ID = E.EPORT_ID ")
    	.append("LEFT OUTER JOIN REPORT.DEVICE_TYPE DT ON E.DEVICE_TYPE_ID = DT.DEVICE_TYPE_ID ")
    	.append(" WHERE T.STATUS <> 'D' ").toString();

    private static final String[] SORT_FIELDS = {"T.TERMINAL_NBR", "E.EPORT_SERIAL_NUM", "DT.DEVICE_TYPE_DESC", "C.CUSTOMER_NAME", "L.LOCATION_NAME", "T.CREATE_DATE"
    	, "TE.START_DATE", "TE.END_DATE"};
    
	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request,
			HttpServletResponse response) throws ServletException {

		try{			
	        String search_param = form.getString(DevicesConstants.PARAM_SEARCH_PARAM, false);
	        String search_type = form.getString(DevicesConstants.PARAM_SEARCH_TYPE, false);
	        String deviceTypeId = form.getString(DevicesConstants.PARAM_DEVICE_TYPE_ID, false);
	        String customerId = form.getString("customerId", false);
	        String accountId = form.getString("accountId", false);
	        StringBuilder queryBase = new StringBuilder(SQL_BASE);
	        Map<Integer, String> params = new LinkedHashMap<Integer, String>();
			int paramCnt = 0;
	        if (!StringHelper.isBlank(customerId)) {
	        	queryBase.append(" AND T.CUSTOMER_ID = CAST(? AS NUMERIC) ");
	        	params.put(paramCnt++, customerId);
	        } else if (!StringHelper.isBlank(accountId)) {
	        	queryBase.append(" AND T.TERMINAL_ID IN (SELECT TERMINAL_ID FROM CORP.VW_CURRENT_BANK_ACCT WHERE CUSTOMER_BANK_ID = CAST(? AS NUMERIC)) ");
	        	params.put(paramCnt++, accountId);
	        } else if (!StringHelper.isBlank(search_param)) {
	        	if ("serial_number".equalsIgnoreCase(search_type)) {
	        		queryBase.append(" AND LOWER(E.EPORT_SERIAL_NUM) LIKE LOWER(?) ");
	        		params.put(paramCnt++, Helper.convertParam(search_param, false));
	        	} else if ("terminal_number".equalsIgnoreCase(search_type)) {
	        		queryBase.append(" AND LOWER(T.TERMINAL_NBR) LIKE LOWER(?) ");
	        		params.put(paramCnt++, Helper.convertParam(search_param, false));
	        	} else if ("customer_name".equalsIgnoreCase(search_type)) {
	        		queryBase.append(" AND LOWER(C.CUSTOMER_NAME) LIKE LOWER(?) ");
	        		params.put(paramCnt++, Helper.convertParam(search_param, false));
	        	} else if ("location_name".equalsIgnoreCase(search_type)) {
	        		queryBase.append(" AND LOWER(L.LOCATION_NAME) LIKE LOWER(?) ");
	        		params.put(paramCnt++, Helper.convertParam(search_param, false));
	        	} else if ("sales_order_number".equalsIgnoreCase(search_type)) {
	        		queryBase.append(" AND T.SALES_ORDER_NUMBER = ? ");
	        		params.put(paramCnt++, search_param);
	        	} else if ("purchase_order_number".equalsIgnoreCase(search_type)) {
	        		queryBase.append(" AND T.PURCHASE_ORDER_NUMBER = ? ");
	        		params.put(paramCnt++, search_param);
	        	}
	        }
	        
	        if (!StringHelper.isBlank(deviceTypeId))  {
	        	queryBase.append(" AND E.DEVICE_TYPE_ID = CAST(? AS NUMERIC) ");
	        	params.put(paramCnt++, deviceTypeId);
	        }
			
	        String paramTotalCount = PaginationUtil.getTotalField(null);
	        String paramPageIndex = PaginationUtil.getIndexField(null);
	        String paramPageSize = PaginationUtil.getSizeField(null);
	        String paramSortIndex = PaginationUtil.getSortField(null);

	        int totalCount = form.getInt(paramTotalCount, false, -1);
	        if (totalCount == -1)
	        {
	        	Results total = DataLayerMgr.executeSQL("REPORT", "SELECT COUNT(1), MAX(T.TERMINAL_ID) " + queryBase.toString(), params.values().toArray(), null);
	            if (total.next())
	            {
	                totalCount = total.getValue(1, int.class);
	                if (totalCount == 1) {
	                	form.setRedirectUri(new StringBuilder("/editTerminals.i?terminalId=").append(total.getValue(2, String.class)).toString());
	                	return;
	                }

	            }
	            else
	            {
	                totalCount = 0;
	            }
	            request.setAttribute(paramTotalCount, String.valueOf(totalCount));
	        }
	
	        int pageIndex = form.getInt(paramPageIndex, false, 1);
	        int pageSize = form.getInt(paramPageSize, false, PaginationUtil.DEFAULT_PAGE_SIZE);
	        int minRowToFetch = PaginationUtil.getStartNum(pageSize, pageIndex);
	        int maxRowToFetch = PaginationUtil.getEndNum(pageSize, pageIndex);
	
	        String sortIndex = form.getString(paramSortIndex, false);
	        sortIndex = StringHelper.isBlank(sortIndex) ? DEFAULT_SORT_INDEX : sortIndex;
	        String orderBy = PaginationUtil.constructOrderBy(SORT_FIELDS, sortIndex);
	
	        String query;
			if (!DialectResolver.isOracle()) {
				query = new StringBuilder("select * from (")
						.append(" select pagination_temp.*, row_number() over() as rnum from (").append(SQL_START)
						.append(queryBase).append(orderBy)
						.append(" LIMIT ?::numeric) pagination_temp ) sq_end where rnum  >= ?::numeric").toString();
			} else {
				query = new StringBuilder("select * from (").append(" select pagination_temp.*, ROWNUM rnum from (")
						.append(SQL_START).append(queryBase).append(orderBy)
						.append(") pagination_temp where ROWNUM <= ?) where rnum  >= ?").toString();
			}
	        params.put(paramCnt++, String.valueOf(maxRowToFetch));
			params.put(paramCnt++, String.valueOf(minRowToFetch));	        

	        Results results = DataLayerMgr.executeSQL("REPORT", query, params.values().toArray(), null);
	        request.setAttribute("resultlist", results);
	        
		}catch(Exception e){
    		throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
    	}

	}
}
