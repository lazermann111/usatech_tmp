/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.usatech.dms.terminal;

import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerMgr;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;
import simple.text.StringUtils;

import com.usatech.dms.action.TerminalActions;
import com.usatech.dms.model.Terminal;

public class UpdatedTerminalsStep extends AbstractStep
{

    /*
     * (non-Javadoc)
     * 
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm,
     * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
    	try {
	    	String action = form.getString("action", false);		
			if (!StringUtils.isBlank(action)) {    				
				if ("Accept".equalsIgnoreCase(action)) {
					String[] ids = form.getStringArray("terminalSelect", false);
					if(ids.length > 0){
						for(int i=0; i < ids.length; i++)
					        DataLayerMgr.executeCall("ACCEPT_TERMINAL_CHANGES", new Object[] {ids[i]}, true);
					}
				}
			}
			List<Terminal> terminals = TerminalActions.getChangedTerminals();
            form.setAttribute("terminals", terminals);
		} catch (Exception e) {
            new ServletException(e);
        }
    }
}
