package com.usatech.dms.terminal;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RecordRequestFilter.CallInputs;
import simple.servlet.steps.AbstractStep;
import simple.text.Censor;
import simple.text.StringUtils;

import com.usatech.dms.action.TerminalActions;
import com.usatech.dms.servlet.DMSRecordRequestFilter;
import com.usatech.dms.util.DMSConstants;
import com.usatech.dms.util.Helper;
import com.usatech.layers.common.util.StringHelper;
import com.usatech.layers.common.util.WebHelper;

public class UpdateTerminalsStep extends AbstractStep{
	private static final simple.io.Log log = simple.io.Log.getLog();
	
	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		try {		
			
			String list = form.getString("dev_list", false);
			if(StringHelper.isBlank(list))			
				return;
			String action = form.getString("myaction", true);
			if(StringHelper.isBlank(action))
				return;
			String craneCustomerId = form.getString("craneCustomerId", false);
			if(!StringUtils.isBlank(craneCustomerId)){
				craneCustomerId=craneCustomerId.trim();
			}
	
			int currencyId = form.getInt("currency_id", true, -1);
			int businessUnitId = form.getInt("business_unit_id", true, -1);
			String doingBusinessAs = Censor.sanitizeText(form.getStringSafely("doing_business_as", "").trim());
			int eportInactiveDays = form.getInt("eportQSInactivityDays", false, -1);
						
			String terminal_date_str = form.getString("terminal_date", false);
			if (StringHelper.isBlank(terminal_date_str) || terminal_date_str.length() < 10)
				terminal_date_str = Helper.getCurrentDate();
			Date terminalDate = ConvertUtils.convert(Date.class, new StringBuilder(terminal_date_str).append(" ").append(form.getString("terminal_time", false)).toString());
			if (terminalDate == null)
				terminalDate = new Date();
			
			String new_date_str = form.getString("new_date", false);
			if (StringHelper.isBlank(new_date_str) || new_date_str.length() < 10)
				new_date_str = Helper.getCurrentDate();
			Date newDate = ConvertUtils.convert(Date.class, new StringBuilder(new_date_str).append(" ").append(form.getString("new_time", false)).toString());
			if (newDate == null)
				newDate = new Date();
			String override = form.getString("override", false) != null ? "Y" : "N";	
		
			StringBuilder err = new StringBuilder();
			int updatedCount = 0;
			Results rs;
			String flatList = list.replace('\n', ',').replace("\r", ",").replace(" ", "").replace(",,", ",");
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("terminalDate", terminalDate);
			if ("terminal".equalsIgnoreCase(form.getString("device_number_type", false))) {
				params.put("terminalNbrs", flatList);
				rs = DataLayerMgr.executeQuery("GET_BULK_TERMINAL_INFO_BY_TERMINAL_NBR", params);
			} else {
				params.put("eportSerialNums", flatList);
				rs = DataLayerMgr.executeQuery("GET_BULK_TERMINAL_INFO", params);
			}
			DMSRecordRequestFilter rrf = new DMSRecordRequestFilter();
			CallInputs ci = new CallInputs();
			while (rs.next()) {
				long startTsMs = System.currentTimeMillis();
				long terminalId = rs.getValue("terminalId", long.class);
				try {
					if ("Start Date".equalsIgnoreCase(action))
						TerminalActions.updateTerminalEport(terminalId, rs.getValue("eportId", long.class), newDate, rs.getValue("endDate", Date.class), rs.getValue("terminalEportId", long.class), override);
					else if ("End Date".equalsIgnoreCase(action)) {
						if (rs.getValue("endDate") == null)
							continue;
						TerminalActions.updateTerminalEport(terminalId, rs.getValue("eportId", long.class), rs.getValue("startDate", Date.class), newDate, rs.getValue("terminalEportId", long.class), override);
					} else if ("Bank Acct Date".equalsIgnoreCase(action))
						TerminalActions.reassignBankAccount(terminalId, rs.getValue("customerBankId", long.class), newDate);
					else if ("Currency".equalsIgnoreCase(action))
						DataLayerMgr.executeUpdate("BULK_UPDATE_TERMINAL", new Object[] {rs.getValue("paymentScheduleId"), currencyId, rs.getValue("businessUnitId"), terminalId}, true);
					else if ("Business Unit".equalsIgnoreCase(action))
						DataLayerMgr.executeUpdate("BULK_UPDATE_TERMINAL", new Object[] {rs.getValue("paymentScheduleId"), rs.getValue("feeCurrencyId"), businessUnitId, terminalId}, true);
					else if ("Deactivate ePort".equalsIgnoreCase(action)){
						String deactivateDetail=form.getString("deactivateDetail", false);
						if(!StringUtils.isBlank(deactivateDetail)){
							deactivateDetail=deactivateDetail.trim();
						}
						TerminalActions.updateTerminalEport(null, rs.getValue("eportId", long.class), newDate, null, null, override, null, deactivateDetail, null, form.getInt("deactivateDetailId", false, 1));
					}
					else if ("Doing Business As".equalsIgnoreCase(action))
						DataLayerMgr.executeUpdate("UPDATE_TERMINAL_DBA", new Object[] {doingBusinessAs, terminalId, doingBusinessAs}, true);
					else if ("Crane Customer".equalsIgnoreCase(action)){
						DataLayerMgr.executeUpdate("UPDATE_TERMINAL_CRANE_CUSTOMER", new Object[] {craneCustomerId, terminalId}, true);
					}
					else if ("ePort Inactive Days".equalsIgnoreCase(action)){
						DataLayerMgr.executeUpdate("UPDATE_EPORT_QS_INACTIVE_DAYS", new Object[] {eportInactiveDays, terminalId}, true);
					}
					else if ("Activation Reason/Details".equalsIgnoreCase(action)){
						int activateDetailId = form.getInt("activateDetailId", false, 1);
						String activateDetailInfo = form.getString("activateDetail", false); 
						DataLayerMgr.executeUpdate("UPDATE_ACTIVATE_DETAIL", new Object[] {activateDetailId, activateDetailInfo, terminalId, terminalDate, terminalDate}, true);
					}
					else if ("Deactivation Reason/Details".equalsIgnoreCase(action)){
						int deactivateDetailId = form.getInt("deactivateDetailId", false, 1);
						String deactivateDetailInfo = form.getString("deactivateDetail", false);
						DataLayerMgr.executeUpdate("UPDATE_DEACTIVATE_DETAIL", new Object[] {deactivateDetailId, deactivateDetailInfo, terminalId, terminalDate, terminalDate}, true);
					}
					updatedCount++;
					WebHelper.publishAppRequestRecord(DMSConstants.APP_CD, request, rrf, ci, startTsMs, action, "terminal", String.valueOf(terminalId), log);
				} catch (Exception e) {
					err.append("Error changing ").append(action).append(" for terminal ").append(rs.getFormattedValue("terminalNbr")).append(": ").append(e.getMessage()).append("<br /><br />");
					log.error(new StringBuilder("Error changing ").append(action).append(" for terminal ").append(rs.getFormattedValue("terminalNbr")).toString(), e);
				}				
			}	
			request.setAttribute("message", "Terminals updated: " + updatedCount);
			if (err.length() > 0)
				request.setAttribute("error", err.toString());
		}catch(Exception e){
    		throw new ServletException("Error updating terminals", e);
    	}
	}
}
