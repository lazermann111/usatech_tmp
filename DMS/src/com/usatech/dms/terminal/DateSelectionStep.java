/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.usatech.dms.terminal;

import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertUtils;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

import com.usatech.dms.action.TerminalActions;
import com.usatech.dms.util.Helper;
import com.usatech.layers.common.util.StringHelper;

public class DateSelectionStep extends AbstractStep
{
    /*
     * (non-Javadoc)
     * 
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm,
     * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
        Date dateTimeTxt = new Date();
    	
        long eportId = form.getLong("eportId", false, -1);
        int action = form.getInt("dateAction", false, 0);
        String startDateStr = form.getString("startDateStr", false);
        String endDateStr = form.getString("endDateStr", false);

        try
        {
            if (action >= TerminalActions.ACT_CHANGE_START_DATE && action <= TerminalActions.ACT_CHANGE_FEE_END_DATE)
            {
                Date startDate = StringHelper.isBlank(startDateStr) ? Helper.getMinDate() : ConvertUtils.convert(Date.class, startDateStr);
                Date endDate = StringHelper.isBlank(endDateStr) ? Helper.getMaxDate() : ConvertUtils.convert(Date.class, endDateStr);

                Date date = getDate(action, startDate, endDate);
                
                if (action != TerminalActions.ACT_ASSIGN_EPORT && action != TerminalActions.ACT_DEACTIVATE_EPORT)
                    dateTimeTxt = date;

                Date preEndDate = null;
                if (action == TerminalActions.ACT_ASSIGN_EPORT || action == TerminalActions.ACT_CHANGE_START_DATE)
                {
                    preEndDate = TerminalActions.getPreEndDate(eportId, date);
                }

                Date postStartDate = null;
                if (action == TerminalActions.ACT_CHANGE_END_DATE || action == TerminalActions.ACT_DEACTIVATE_EPORT)
                {
                    postStartDate = TerminalActions.getPostStartDate(eportId, date);
                }
                form.setAttribute("date", date);
                form.setAttribute("startDate", startDate);
                form.setAttribute("endDate", endDate);
                form.setAttribute("preEndDate", preEndDate);
                form.setAttribute("postStartDate", postStartDate);
            }

            form.setAttribute("dateTxtStr", Helper.getDate(dateTimeTxt));
            form.setAttribute("timeTxtStr", Helper.getTime(dateTimeTxt));

        }
        catch (Exception e)
        {
            new ServletException(e);
        }
    }

    private Date getDate(int action, Date startDate, Date endDate)
    {
        Date date = null;
        switch (action)
        {
            case TerminalActions.ACT_CHANGE_FEE_EFFECTIVE_DATE :
            case TerminalActions.ACT_CHANGE_FEE_END_DATE :
            case TerminalActions.ACT_ASSIGN_EPORT :
                date = new Date();
                break;
            case TerminalActions.ACT_CHANGE_START_DATE :
                date = startDate;
                if ((endDate.getTime() - startDate.getTime()) / 1000 < 0)
                {
                    date = endDate;
                }
                break;
            case TerminalActions.ACT_CHANGE_END_DATE :
                date = new Date();
                break;
            case TerminalActions.ACT_DEACTIVATE_EPORT :
                date = ((endDate.getTime() - startDate.getTime()) / 1000 < 0) ? startDate : endDate;
                break;
            default:
                break;
        }
        return date;
    }
}
