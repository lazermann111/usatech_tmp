/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.template;

import java.util.regex.Pattern;

import simple.util.NameValuePair;

/**
 * Model class represents a property of the "Device Configuration Template"
 */
public class Property
{
    private final String label;
    private final String description;
    private final int displaySize;
    private final int maxLength;
    private final String defaultValue;
    private final boolean isMandatory;
    private final String id;
    private final DisplayType displayType;
    private final ValueType valueType;
    private final boolean isDisplay;
    private final boolean isEditable;
    private final boolean isBasic;
    private final String tailLabel;

    public static class PropertyBuilder
    {
        // required parameters
        private final String id;
        private final String label;

        // optional parameters - let JAVA initialize to default values
        private String description;
        private int displaySize;
        private int maxLength;
        private String defaultValue;
        private boolean isMandatory;
        private DisplayType displayType;
        private ValueType valueType;
        private boolean isDisplay;
        private boolean isEditable;
        private boolean isBasic;
        private String tailLabel;

        public PropertyBuilder(String id, String label)
        {
            this.id = id;
            this.label = label;
        }

        public PropertyBuilder description(String description)
        {
            this.description = description;
            return this;
        }

        public PropertyBuilder displaySize(int size)
        {
            this.displaySize = size;
            return this;
        }

        public PropertyBuilder maxLength(int length)
        {
            this.maxLength = length;
            return this;
        }

        public PropertyBuilder defaultValue(String value)
        {
            this.defaultValue = value;
            return this;
        }

        public PropertyBuilder isMandatory(boolean mandatory)
        {
            this.isMandatory = mandatory;
            return this;
        }

        public PropertyBuilder isDisplay(boolean display)
        {
            this.isDisplay = display;
            return this;
        }

        public PropertyBuilder isEditable(boolean editable)
        {
            this.isEditable = editable;
            return this;
        }

        public PropertyBuilder isBasic(boolean basic)
        {
            this.isBasic = basic;
            return this;
        }

        public PropertyBuilder displayType(DisplayType displayType)
        {
            this.displayType = displayType;
            return this;
        }

        public PropertyBuilder tailLabel(String tailLabel)
        {
            this.tailLabel = tailLabel;
            return this;
        }

        public PropertyBuilder valueType(ValueType valueType)
        {
            this.valueType = valueType;
            return this;
        }

        public Property build()
        {
            return new Property(this);
        }
    }

    private Property(PropertyBuilder builder)
    {
        id = builder.id;
        label = builder.label;
        description = builder.description;
        displaySize = builder.displaySize;
        maxLength = builder.maxLength;
        defaultValue = builder.defaultValue;
        isMandatory = builder.isMandatory;
        displayType = builder.displayType;
        isDisplay = builder.isDisplay;
        isEditable = builder.isEditable;
        isBasic = builder.isBasic;
        tailLabel = builder.tailLabel;
        valueType = builder.valueType;
    }

    /**
     * @return the valueType
     */
    public ValueType getValueType()
    {
        return valueType;
    }

    /**
     * @return the label
     */
    public String getLabel()
    {
        return label;
    }

    /**
     * @return the description
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * @return the displaySize
     */
    public int getDisplaySize()
    {
        return displaySize;
    }

    /**
     * @return the maxLength
     */
    public int getMaxLength()
    {
        return maxLength;
    }

    /**
     * @return the defaultValue
     */
    public String getDefaultValue()
    {
        return defaultValue;
    }

    /**
     * @return the isMandatory
     */
    public boolean isMandatory()
    {
        return isMandatory;
    }

    /**
     * @return the id
     */
    public String getId()
    {
        return id;
    }

    /**
     * @return the type
     */
    public DisplayType getDisplayType()
    {
        return displayType;
    }

    /**
     * @return the isDisplay
     */
    public boolean isDisplay()
    {
        return isDisplay;
    }

    /**
     * @return the isEditable
     */
    public boolean isEditable()
    {
        return isEditable;
    }

    /**
     * @return the isBasic
     */
    public boolean isBasic()
    {
        return isBasic;
    }

    /**
     * @return the tailLabel
     */
    public String getTailLabel()
    {
        return tailLabel;
    }

    /**
     * Check to see if the given value is valid or not for this valueType.
     * 
     * @param value
     * @return <code>null</code> if the value is valid
     */
    public String validate(String value)
    {
        String result = null;
        String display = getDisplayType().getDisplay();
        if (("select".equals(display) || "radio".equals(display)) && !getDisplayType().getOptions().isEmpty())
        {
            boolean validOption = false;
            // make sure the value is one of the options
            for (NameValuePair option : getDisplayType().getOptions())
            {
                if (option.getValue().equals(value))
                {
                    validOption = true;
                    break;
                }
            }
            if (!validOption)
            {
                result = '\'' + value + "' is not a valid option for \"" + getLabel() + "\"";
            }
        }

        if (result == null && getValueType() != null)
        {
            if (!Pattern.matches(getValueType().getPattern(), value))
            {
                result = getValueType().getMessage() + " for \"" + getLabel() + "\"";
            }
        }
        return result;
    }

}
