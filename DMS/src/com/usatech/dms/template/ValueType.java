/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.template;

import java.util.regex.Pattern;

/**
 * Model class represents a value type of the "Device Configuration Template"
 */
public class ValueType
{
    private final String id;
    private final String script;
    private final String pattern;
    private final String message;
    private final String functionName;

    public static class ValueTypeBuilder
    {
        private final String id;

        private String pattern;
        private String message;
        private String script;
        private String functionName;

        public ValueTypeBuilder(String id)
        {
            this.id = id;
        }

        public ValueTypeBuilder pattern(String pattern)
        {
            this.pattern = pattern;
            return this;
        }

        public ValueTypeBuilder message(String message)
        {
            this.message = message;
            return this;
        }

        public ValueTypeBuilder script(String script)
        {
            this.script = script;
            return this;
        }

        public ValueTypeBuilder functionName(String functionName)
        {
            this.functionName = functionName;
            return this;
        }

        public ValueType build()
        {
            return new ValueType(this);
        }
    }

    private ValueType(ValueTypeBuilder builder)
    {
        id = builder.id;
        pattern = builder.pattern;
        message = builder.message;
        functionName = builder.functionName;
        script = builder.script;
    }

    /**
     * @return the id
     */
    public String getId()
    {
        return id;
    }

    /**
     * @return the pattern
     */
    public String getPattern()
    {
        return pattern;
    }

    /**
     * @return the message
     */
    public String getMessage()
    {
        return message;
    }

    /**
     * @return the pattern
     */
    public String getScript()
    {
        return script;
    }

    /**
     * @return the functionName
     */
    public String getFunctionName()
    {
        return functionName;
    }

}
