/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.template;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import simple.util.NameValuePair;

/**
 * Model class represents a property type of the "Device Configuration Template"
 */
public class DisplayType
{
    private final String id;
    private final String display;
    private final List<NameValuePair> options;

    public static class DisplayTypeBuilder
    {
        // required parameters
        private final String id;
        private final String display;

        // optional parameter
        private List<NameValuePair> options;

        public DisplayTypeBuilder(String id, String display)
        {
            this.id = id;
            this.display = display;
        }

        public DisplayTypeBuilder options(List<NameValuePair> options)
        {
            this.options = options;
            return this;
        }

        public DisplayType build()
        {
            return new DisplayType(this);
        }
    }

    private DisplayType(DisplayTypeBuilder builder)
    {
        id = builder.id;
        display = builder.display;
        if (builder.options == null)
        {
            options = Collections.unmodifiableList(new ArrayList<NameValuePair>());
        }
        else
        {
            options = Collections.unmodifiableList(builder.options);
        }
    }

    /**
     * @return the id
     */
    public String getId()
    {
        return id;
    }

    /**
     * @return the display
     */
    public String getDisplay()
    {
        return display;
    }

    /**
     * @return the options
     */
    public List<NameValuePair> getOptions()
    {
        return options;
    }

}
