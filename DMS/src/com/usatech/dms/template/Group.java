/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.template;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Model class represents a group of the "Device Configuration Template"
 */
public class Group
{
    private final String id;
    private final String name;
    private final boolean hasBasic;
    private final List<Property> properties;

    public static class GroupBuilder
    {
        // required parameter
        private final String id;
        private String name;
        private boolean hasBasic;
        private List<Property> properties;

        public GroupBuilder(String id)
        {
            this.id = id;
        }

        public GroupBuilder name(String name)
        {
            this.name = name;
            return this;
        }

        public GroupBuilder hasBasic(boolean hasBasic)
        {
            this.hasBasic = hasBasic;
            return this;
        }

        public GroupBuilder properties(List<Property> properties)
        {
            this.properties = properties;
            return this;
        }

        public Group build()
        {
            return new Group(this);
        }
    }

    private Group(GroupBuilder builder)
    {
        id = builder.id;
        name = builder.name;
        hasBasic = builder.hasBasic;
        if (builder.properties == null)
        {
            properties = Collections.unmodifiableList(new ArrayList<Property>());
        }
        else
        {
            properties = Collections.unmodifiableList(builder.properties);
        }
    }

    /**
     * @return the id
     */
    public String getId()
    {
        return id;
    }

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @return the level
     */
    public boolean hasBasic()
    {
        return hasBasic;
    }

    /**
     * @return the properties
     */
    public List<Property> getProperties()
    {
        return properties;
    }

}
