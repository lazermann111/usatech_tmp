/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.template;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Model class represents the "Device Configuration template"
 */
public class ConfigTemplate
{
    private final String id;
    private final boolean setDefault;
    private final List<Group> groups;

    public static class ConfigTemplateBuilder
    {
        private final String id;
        private boolean setDefault;
        private List<Group> groups;

        public ConfigTemplateBuilder(String id)
        {
            this.id = id;
        }

        public ConfigTemplateBuilder setDefault(boolean setDefault)
        {
            this.setDefault = setDefault;
            return this;
        }

        public ConfigTemplateBuilder groups(List<Group> groups)
        {
            this.groups = groups;
            return this;
        }

        public ConfigTemplate build()
        {
            return new ConfigTemplate(this);
        }
    }

    private ConfigTemplate(ConfigTemplateBuilder builder)
    {
        id = builder.id;
        setDefault = builder.setDefault;
        if (builder.groups == null)
        {
            groups = Collections.unmodifiableList(new ArrayList<Group>());
        }
        else
        {
            groups = Collections.unmodifiableList(builder.groups);
        }
    }

    /**
     * @return the id
     */
    public String getId()
    {
        return id;
    }

    /**
     * 
     * @return the setDefault
     */
    public boolean canSetDefault()
    {
        return setDefault;
    }

    /**
     * @return the groups
     */
    public List<Group> getGroups()
    {
        return groups;
    }

}
