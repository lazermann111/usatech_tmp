package com.usatech.dms.customer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usatech.dms.action.CustomerAction;

import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

public class DeleteCustomerStep extends AbstractStep{

	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request,
			HttpServletResponse response) throws ServletException {
		
        try{
        	
	        int customer_id = form.getInt("customerId", false, -1);
	        if (CustomerAction.checkActiveTermOfCust(customer_id)) {
	        	request.setAttribute("statusMessage", "Can't delete.Customer has active terminals.");
	        }else {
	            CustomerAction.deleteCustomer(customer_id);
	            request.setAttribute("customerId",null);
	        	request.setAttribute("statusMessage", "Customer deleted.");
	        }
        } catch (Exception e) {
        	throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
        }
		
	}
}
