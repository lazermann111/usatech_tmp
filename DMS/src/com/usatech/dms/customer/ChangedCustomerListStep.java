/*
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.customer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.app.DialectResolver;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

import com.usatech.dms.action.CustomerAction;
import com.usatech.layers.common.util.PaginationUtil;
import com.usatech.layers.common.util.StringHelper;

public class ChangedCustomerListStep extends AbstractStep
{
    // by default, sort by "customer name" ascending
    private static final String DEFAULT_SORT_INDEX = "1";

    private static final String SQL_START = "SELECT CUSTOMER_ID," +
            "CUSTOMER_NAME," +
            "USER_ID," +
            "SALES_TERM," +
            "CREDIT_TERM," +
            "TO_CHAR(CREATE_DATE, 'MM/DD/YYYY HH24:MI:SS') CREATE_DATE," +
            "UPD_DATE," +
            "CREATE_BY," +
            "UPD_BY," +
            "CUSTOMER_ALT_NAME," +
            "ADDRESS_ID," +
            "ADDRESS_NAME," +
            "ADDRESS1," +
            "ADDRESS2," +
            "CITY," +
            "STATE," +
            "ZIP," +
            "LICENSE_NBR," +
            "STATUS " ;

    private static final String SQL_BASE = "FROM CORP.VW_CUSTOMER C WHERE STATUS IN('P', 'U') ";

    private static final String[] SORT_FIELDS = {"CUSTOMER_NAME", // customer name
        "C.CREATE_DATE", // create date
        "ADDRESS_NAME", // address to
        "ADDRESS1", // address1
        "ADDRESS2", // address2
        "CITY", // city
        "STATE", // state
        "ZIP", // zip
        "LICENSE_NBR", // license number
        "STATUS" // status
        };
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
		String[] ids = form.getStringArray("cid",false);
        if (ids.length>0)
        {
            try
            {
                // The second parameter of acceptCustomer was authenticated user's id in Eport
                // application,
                // but there is no user id in this application with ldap authentication
    			for(int i=0;i<ids.length;i++){
    	            int customer_id = Integer.parseInt(ids[i]);
                    CustomerAction.acceptCustomer(customer_id, null);
    			}
            }
            catch (Exception e)
            {
                throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
            }
        }

        String paramTotalCount = PaginationUtil.getTotalField(null);
        String paramPageIndex = PaginationUtil.getIndexField(null);
        String paramPageSize = PaginationUtil.getSizeField(null);
        String paramSortIndex = PaginationUtil.getSortField(null);

        try
        {
            // pagination parameters
            int totalCount = form.getInt(paramTotalCount, false, -1);
            if (totalCount == -1 || ids.length > 0)
            {
                // if the total count is not retrieved yet, get it now
                Results total = DataLayerMgr.executeSQL("REPORT", "SELECT COUNT(1) " + SQL_BASE, null, null);
                if (total.next())
                {
                    totalCount = total.getValue(1, int.class);
                }
                else
                {
                    totalCount = 0;
                }
                request.setAttribute(paramTotalCount, String.valueOf(totalCount));
            }

            int pageIndex = form.getInt(paramPageIndex, false, 1);
            int pageSize = form.getInt(paramPageSize, false, PaginationUtil.DEFAULT_PAGE_SIZE);
            int minRowToFetch = PaginationUtil.getStartNum(pageSize, pageIndex);
            int maxRowToFetch = PaginationUtil.getEndNum(pageSize, pageIndex);

            String sortIndex = form.getString(paramSortIndex, false);
            sortIndex = StringHelper.isBlank(sortIndex) ? DEFAULT_SORT_INDEX : sortIndex;
            String orderBy = PaginationUtil.constructOrderBy(SORT_FIELDS, sortIndex);

            String query;
			if (!DialectResolver.isOracle()) {
				query = "select * from (" + " select pagination_temp.*, row_number() over() as rnum from (" + SQL_START
						+ SQL_BASE + orderBy + " limit CAST (? AS BIGINT)) pagination_temp ) sq_end where rnum  >= CAST (? AS BIGINT)";
			} else {
				query = "select * from (" + " select pagination_temp.*, ROWNUM rnum from (" + SQL_START + SQL_BASE
						+ orderBy + ") pagination_temp where ROWNUM <= ?) where rnum  >= ?";
			}

            Results results = DataLayerMgr.executeSQL("REPORT", query,  new Object[]{maxRowToFetch,minRowToFetch}, null);
            request.setAttribute("customerList", results);
        }
        catch (Exception e)
        {
        	throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
        }

    }

}
