/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.customer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.app.DialectResolver;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;

import com.usatech.dms.util.Helper;
import com.usatech.layers.common.util.PaginationUtil;
import com.usatech.layers.common.util.StringHelper;

public class CustomerListStep extends AbstractStep
{
    // by default, sort by "customer name" ascending
    private static final String DEFAULT_SORT_INDEX = "1";

    private static final String SQL_START = "SELECT customer_id," +
            "customer_name," +
            "customer_city," +
            "country.country_name," +
            "customer_type.customer_type_desc ";

    private static final String SQL_BASE = "from location.customer " +
            "LEFT JOIN location.customer_type ON customer.customer_type_id = customer_type.customer_type_id " +
            "LEFT JOIN location.country ON customer.customer_country_cd = country.country_cd " + 
            "WHERE 1 = 1 ";

    private static final String[] SORT_FIELDS = {"customer_name", // customer name
        "customer_city", // customer city
        "country.country_name", // country name
        "customer_type.customer_type_desc" // customer type desc
        };
    
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
        String customer_name = form.getString("customer_name", false);
        String customer_type = form.getString("customer_type", false);
        String sql = null;
        if (customer_name == null && customer_type == null)
        {
            customer_name = "";
        }
        
        if (customer_name != null)
        {
        	sql = " AND lower(customer_name) like lower(?) ";
        }
        else
        {
        	sql = " AND customer.customer_type_id = CAST (? AS numeric) ";
        }

        String queryBase = SQL_BASE + sql;

        String paramTotalCount = PaginationUtil.getTotalField(null);
        String paramPageIndex = PaginationUtil.getIndexField(null);
        String paramPageSize = PaginationUtil.getSizeField(null);
        String paramSortIndex = PaginationUtil.getSortField(null);

        try
        {
            // pagination parameters
            int totalCount = form.getInt(paramTotalCount, false, -1);
            if (totalCount == -1)
            {
                // if the total count is not retrieved yet, get it now
                Results total = DataLayerMgr.executeSQL("OPER", "SELECT COUNT(1), MAX(customer_id) " + queryBase, new Object[]{(customer_name!=null?Helper.convertParam(customer_name, false):customer_type)}, null);
                if (total.next())
                {
                    totalCount = total.getValue(1, int.class);
                    if (totalCount == 1) {
                    	form.setRedirectUri(new StringBuilder("/editCustomer.i?customer_id=").append(total.getValue(2, String.class)).toString());
                    	return;
                    }
                }
                else
                {
                    totalCount = 0;
                }
                request.setAttribute(paramTotalCount, String.valueOf(totalCount));
            }

            int pageIndex = form.getInt(paramPageIndex, false, 1);
            int pageSize = form.getInt(paramPageSize, false, PaginationUtil.DEFAULT_PAGE_SIZE);
            int minRowToFetch = PaginationUtil.getStartNum(pageSize, pageIndex);
            int maxRowToFetch = PaginationUtil.getEndNum(pageSize, pageIndex);
            
            
            
            String sortIndex = form.getString(paramSortIndex, false);
            sortIndex = StringHelper.isBlank(sortIndex) ? DEFAULT_SORT_INDEX : sortIndex;
            String orderBy = PaginationUtil.constructOrderBy(SORT_FIELDS, sortIndex);

			String query;
			if (!DialectResolver.isOracle()) {
				query = "select * from (" + " select pagination_temp.*, row_number() over() as rnum from (" + SQL_START
						+ queryBase + orderBy + " limit ?::numeric) pagination_temp ) sq_end where rnum  >= ?::numeric";
			} else {
				query = "select * from (" + " select pagination_temp.*, ROWNUM rnum from (" + SQL_START + queryBase
						+ orderBy + ") pagination_temp where ROWNUM <= ?) where rnum  >= ?";
			}

            Results results = DataLayerMgr.executeSQL("OPER", query,  new Object[]{(customer_name!=null?Helper.convertParam(customer_name, false):customer_type),maxRowToFetch,minRowToFetch}, null);
            request.setAttribute("customerList", results);
        }
        catch (Exception e)
        {
        	throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
        }
    }

}
