/*
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.customer;

import java.sql.CallableStatement;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.SimpleAction;
import simple.servlet.steps.AbstractStep;

import com.usatech.layers.common.model.Customer;
import com.usatech.layers.common.ProcessingUtils;

public class EditCustomerFuncStep extends AbstractStep
{
	private static final simple.io.Log log = simple.io.Log.getLog();
	
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
        String myAction = form.getString("myaction", false);
        String temp = form.getString("customer_id", false);
        int customer_id = -1;
        if (temp != null && temp.length() > 0)
        {
            customer_id = Integer.parseInt(temp);
        }
        String errorMessage = null;
        Customer customer = null;
        try
        {
        	String redirectQuery = null;
            if (myAction.equalsIgnoreCase("Delete"))
            {
                String customer_name = form.getString("customer_name", false);
                if ((customer_id > 0 && customer_id != 1) || (customer_name != null && !customer_name.equalsIgnoreCase("Unknown")))
                {
                    Results customer_counts = DataLayerMgr.executeQuery("GET_CUSTOMER_COUNT_IN_POS", new Object[] {customer_id});
                    if (customer_counts.next())
                    {
                        int count = Integer.parseInt(customer_counts.getFormattedValue(1));
                        if (count > 0)
                        {
                            errorMessage = "Can not delete this customer because there are " + count + " devices currently assigned to it.  <br/>Please reassign all the devices to a different customer then retry";
                            request.setAttribute("errorMessage", errorMessage);
                            return;
                        }
                        DataLayerMgr.executeUpdate("UPDATE_CUSTOMER_ACTIVE_FLAG", new Object[] {"N", customer_id}, true);
                    }
                }
            }
            else if (myAction.equalsIgnoreCase("Undelete"))
            {
                DataLayerMgr.executeUpdate("UPDATE_CUSTOMER_ACTIVE_FLAG", new Object[] {"Y", customer_id}, true);
            }
            else
            {
                customer = new Customer();
                customer.setName(form.getString("customer_name", false));
                temp = form.getString("customer_addr1", false);
                customer.setAddress1((temp==null || temp.length()==0)?null:temp);
                temp = form.getString("customer_addr2", false);
                customer.setAddress2((temp==null || temp.length()==0)?null:temp);
                temp = form.getString("customer_city", false);
                customer.setCity((temp==null || temp.length()==0)?null:temp);
                temp = form.getString("customer_county", false);
                customer.setCountry((temp==null || temp.length()==0)?null:temp);
                temp = form.getString("customer_postal_cd", false);
                customer.setPostalCode((temp==null || temp.length()==0)?null:temp);
                temp = form.getString("customer_state_cd", false);
                customer.setStateCode((temp==null || temp.length()==0)?null:temp);
                temp = form.getString("customer_country_cd", false);
                customer.setCountryCode((temp==null || temp.length()==0)?null:temp);
                
                customer.setTypeId(form.getInt("customer_type_id", false, -1));
                temp = form.getString("app_user_name", false);
                customer.setAppUserName((temp==null || temp.length()==0)?null:temp);
                temp = form.getString("email_addr", false);
                customer.setEmailAddress((temp==null || temp.length()==0)?null:temp);
                temp = form.getString("subdomain", false);
                customer.setSubdomain((temp==null || temp.length()==0)?null:temp);

                if (myAction.equalsIgnoreCase("Save New"))
                {
                	boolean success = false;
                	Connection conn = null;
                	try{
	                    conn = DataLayerMgr.getConnection("OPER");
	                    String funcName = "{?=call pkg_customer_maint.add_customer(?,?,?,?,?,?,?,?,?,?,?,?)}";
	                    CallableStatement cs = conn.prepareCall(funcName);
	                    cs.registerOutParameter(1,java.sql.Types.NUMERIC);
	                    
	                    cs.setString(2, customer.getName());
	                    cs.setString(3, customer.getAddress1());
	                    cs.setString(4, customer.getAddress2());
	                    cs.setString(5, customer.getCity());
	                    cs.setString(6, customer.getCountry());
	                    cs.setString(7, customer.getPostalCode());
	                    cs.setString(8, customer.getStateCode());
	                    cs.setString(9, customer.getCountryCode());
	                    cs.setInt(10, customer.getTypeId());
	                    cs.setString(11, customer.getAppUserName());
	                    cs.setString(12, customer.getEmailAddress());
	                    cs.setString(13, customer.getSubdomain());
	                    cs.execute();
	                    conn.commit();
	                    success = true;
	                    	                    
	                    customer_id = cs.getBigDecimal(1).intValueExact();
	                    customer.setId((long)customer_id);
	                    
	                    if (customer_id > 0)
	                		redirectQuery = new StringBuilder("customer_id=").append(customer_id).toString();
                	}finally{
                		if (!success)
                			ProcessingUtils.rollbackDbConnection(log, conn);    		
                		ProcessingUtils.closeDbConnection(log, conn);
                	}
                }
                else if (myAction.equalsIgnoreCase("Save"))
                {
                    if ((customer_id > 0 && customer_id != 1) || (customer.getName() != null && !customer.getName().equalsIgnoreCase("Unknown")))
                    {
                        String old_name = form.getString("old_name", false);
                        if (!old_name.equalsIgnoreCase(customer.getName()))
                        {

                            Results customer_counts = DataLayerMgr.executeQuery("CHECK_CUSTOMER_BY_NAME", new Object[] {customer.getName()});
                            if (customer_counts.next())
                            {
                                int count = Integer.parseInt(customer_counts.getFormattedValue(1));
                                if (count > 0)
                                {
                                    errorMessage = "CUSTOMER => " + customer.getName() + "<= ALREADY EXISTS";
                                    request.setAttribute("errorMessage", errorMessage);
                                    return;
                                }
                            }
                        }
                        customer.setId((long)customer_id);
                        DataLayerMgr.executeUpdate("UPDATE_CUSTOMER", customer, true);
                    }
                }

            }
            request.setAttribute("customer_id", customer_id);
            
            if (RequestUtils.handleActionRedirect(form, request, response, redirectQuery) == SimpleAction.INVOKE_RESULT_REDIRECT)
        		return;
        }
        catch (Exception e)
        {
        	throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
        }

    }

}
