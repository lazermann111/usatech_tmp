/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.customer;

import java.math.BigDecimal;
import java.util.LinkedList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

import com.usatech.dms.action.CustomerAction;
import com.usatech.layers.common.model.Customer;
import com.usatech.dms.util.Helper;
import com.usatech.layers.common.util.StringHelper;

/**
 * @author rhuang
 * 
 */
public class EsudsPrivilegesEditFuncStep extends AbstractStep
{

    /*
     * (non-Javadoc)
     * 
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm,
     * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
        int user_id = form.getInt("app_user_id", false, -1);
        //String send_email = (String)form.get("send_email");
        
        String warningAlertMessage = null;
        
        try
        {
            Results current_permissions = DataLayerMgr.executeQuery("GET_APP_USER_PERMISSIONS_BY_UID", new Object[] {user_id});
            while(current_permissions.next()){
            	BigDecimal app_id = (BigDecimal)current_permissions.get("app_id");
            	BigDecimal app_object_type_id = (BigDecimal)current_permissions.get("app_object_type_id");
            	String object_cd = (String)current_permissions.get("object_cd");
            	String value_read = "N";
            	String value_write = "N";
            	String key = app_id+"_"+app_object_type_id+"_"+object_cd;
            	String keyr = "read__"+key;
            	String keyw = "write__"+key;
            	String keyd = "delete__"+key;
            	if("Y".equalsIgnoreCase((String)form.get(keyr))){
            		value_read = "Y";
            	}
            	
            	if("Y".equalsIgnoreCase((String)form.get(keyw))){
            		value_write = "Y";
            	}
            	
            	if("Y".equalsIgnoreCase((String)form.get(keyd))){
            		DataLayerMgr.executeCall("DELETE_APP_USER_PERMISSION_BY_UID_APPID_APPOBJTYPEID_OBJCD", new Object[] {user_id, app_id, app_object_type_id, object_cd}, true);
            	}else if ("N".equalsIgnoreCase(value_read) && "N".equalsIgnoreCase(value_write)) {
					//no-op # do not change state of the database put some javascript so that this is not the case
				}else{
					DataLayerMgr.executeCall("UPDATE_APP_USER_PERMISSION", new Object[] {value_read, value_write, user_id, app_id, app_object_type_id, object_cd}, true);
            	}            	
            }
            BigDecimal count = null;
            Results count_2_6 = DataLayerMgr.executeQuery("GET_APP_USER_PERMISSIONS_COUNT_BY_UID_APPID_APPOBJTYPEID_LOCID", new Object[] {user_id, 2, 6, 16}); //# Campus
            if(count_2_6.next()){
            	count = ConvertUtils.convert(BigDecimal.class, count_2_6.get("count"));
            	if(count==null || count.intValue()<1){
            		DataLayerMgr.executeCall("DELETE_APP_USER_PERMISSION_BY_UID_APPID_APPOBJTYPEID", new Object[] {user_id, 2,2}, true);
            	}
            }
            
            
            String user_admin_reports_read = (String)form.get("user_admin_reports_read");
            if(!StringHelper.isBlank(user_admin_reports_read)){
            	String read = ((!StringHelper.isBlank(user_admin_reports_read)) ? "Y" : "N");
            	String location_id = (String)form.get("user_admin_reports__location_id");
            	DataLayerMgr.executeCall("INSERT_APP_USER_PERMISSION", new Object[] {user_id, 6, 6,  location_id, "N", read, "N", "N"}, true);
            }
            
            String user_maintenance_read = (String)form.get("user_maintenance_read");
            String user_maintenance_write = (String)form.get("user_maintenance_write");
            if((!StringHelper.isBlank(user_maintenance_read)) || (!StringHelper.isBlank(user_maintenance_write))){
            	String read = ((!StringHelper.isBlank(user_maintenance_read)) ? "Y" : "N");
            	String write = ((!StringHelper.isBlank(user_maintenance_write)) ? "Y" : "N");
            	String location_id = (String)form.get("user_maintenance__location_id");
            	DataLayerMgr.executeCall("INSERT_APP_USER_PERMISSION", new Object[] {user_id, 3, 7,  location_id, "Y", read, write, "N"}, true);
            }
            
            String operator_access_campus_read = (String)form.get("operator_access_campus_read");
            String operator_access_campus_write = (String)form.get("operator_access_campus_write");
            if((!StringHelper.isBlank(operator_access_campus_read)) || (!StringHelper.isBlank(operator_access_campus_write))){
            	String customer_id = (String)form.get("operator_access_campus__customer_id");
            	Results bbbResults = DataLayerMgr.executeQuery("GET_APP_USER_OBJECT_CD", new Object[] {user_id, 2, 2});
            	String bbb = "";
            	if(bbbResults.next()){
            		bbb = (String)bbbResults.get("object_cd");
            		if((!StringHelper.isBlank(bbb)) && ((new BigDecimal(bbb)).intValue() > 0 && !(bbb.equalsIgnoreCase(customer_id)))){
            			// send back alert message
            			/* print "<script>
              				alert('You CAN NOT select different customer for OPERATOR ACCESS.');
          					</script>";
            			 */
            			
            			warningAlertMessage = "<script>"
              				+"alert('You CAN NOT select different customer for OPERATOR ACCESS CAMPUS.');"
          					+"</script>.";
            			form.setAttribute("warningAlertMessage", warningAlertMessage);
  
            		}else{
            			BigDecimal b_has_operatoraccess = null;
            			Results permissionCount = DataLayerMgr.executeQuery("GET_APP_USER_PERMISSIONS_COUNT_BY_UID_APPID_APPOBJTYPEID", new Object[] {user_id, 2, 2});
            			if(permissionCount.next()){
            				b_has_operatoraccess = (BigDecimal)permissionCount.get("count");
            				String read = ((!StringHelper.isBlank(operator_access_campus_read)) ? "Y" : "N");
                        	String write = ((!StringHelper.isBlank(operator_access_campus_write)) ? "Y" : "N");
                        	if(b_has_operatoraccess!=null && b_has_operatoraccess.intValue() == 0){
                        		DataLayerMgr.executeCall("INSERT_APP_USER_PERMISSION", new Object[] {user_id, 2, 2,  customer_id, "N", "Y", "Y", "N"}, true);
                        	}
                        	String location_id = (String)form.get("operator_access_campus__location_id");
                        	DataLayerMgr.executeCall("INSERT_APP_USER_PERMISSION", new Object[] {user_id, 2, 6,  location_id, "N", read, write, "N"}, true);
            			}
            		}
            	}
            }
            
            String operator_access_school_read = (String)form.get("operator_access_school_read");
            if((!StringHelper.isBlank(operator_access_school_read)) ) {
           
                String read = ((!StringHelper.isBlank(operator_access_school_read)) ? "Y" : "N");
                String location_id = (String)form.get("operator_access_school__location_id");  
                DataLayerMgr.executeCall("INSERT_APP_USER_PERMISSION", new Object[] {user_id, 2, 6,  location_id, "N", read, "N", "N"}, true);
             }
            
            String operator_reports_read = (String)form.get("operator_reports_read");
            String operator_reports_write = (String)form.get("operator_reports_write");
            if((!StringHelper.isBlank(operator_reports_read)) || (!StringHelper.isBlank(operator_reports_write))){
            	String read = ((!StringHelper.isBlank(operator_reports_read)) ? "Y" : "N");
            	String write = ((!StringHelper.isBlank(operator_reports_write)) ? "Y" : "N");
            	String customer_id = (String)form.get("operator_reports__customer_id");
            	DataLayerMgr.executeCall("INSERT_APP_USER_PERMISSION", new Object[] {user_id, 5, 2,  customer_id, "N", read, write, "N"}, true);
            }
                        
            /*if((!StringHelper.isBlank(send_email)) && ("Y".equalsIgnoreCase(send_email))){
            	String subject = "eSuds Privilege Change!";
            	String content = "Hi, your privileges have been changed.";
            	Helper.sendEmail(Helper.DMS_EMAIL, Helper.DMS_EMAIL, subject, content, Helper.DMS_EMAIL_CONTENT_TYPE_PLAIN);
            }*/
        }

        catch (Exception e)
        {
        	throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
        }

    }

}
