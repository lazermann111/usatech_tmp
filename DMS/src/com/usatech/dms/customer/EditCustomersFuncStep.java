/*
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.customer;

import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

import com.usatech.dms.action.CustomerAction;
import com.usatech.dms.model.CorpCustomer;
import com.usatech.layers.common.model.CustomerBankTerminal;
import com.usatech.dms.model.Terminal;
import com.usatech.dms.model.UserView;
import com.usatech.layers.common.util.StringHelper;

public class EditCustomersFuncStep extends AbstractStep
{

    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
        int customer_id = form.getInt("customer_id", false, -1);
        if (customer_id > 0)
        {
            boolean deleteFlag = false;
            try
            {
                CorpCustomer customer = null;
                String userOP = form.getString("userOP", false);
                if (userOP != null && userOP.equalsIgnoreCase("save"))
                {
                    customer = CustomerAction.getCustomerInfo(customer_id);
                    customer.setName(form.getString("name", false));
                    customer.setAltName(form.getString("altName", false));
                    customer.setAddressName(form.getString("addrName", false));
                    customer.setAddress1(form.getString("addr1", false));
                    customer.setAddress2(form.getString("addr2", false));
                    customer.setCity(form.getString("city", false));
                    customer.setState(form.getString("state", false));
                    customer.setZip(form.getString("zip", false));
                    customer.setUserId(form.getLong("userId", false, -1));
                    customer.setDealerId(form.getLong("dealerId", false, -1));
                    if (customer.getUserId() < 0)
                    {
                        customer.setUserId(null);
                    }
                    if (customer.getDealerId() < 0)
                    {
                        customer.setDealerId(null);
                    }
                    CustomerAction.updateCustomer(customer);
                }
                else if (userOP != null && userOP.equalsIgnoreCase("delete"))
                {
                    if (CustomerAction.checkActiveTermOfCust(customer_id))
                    {
                        deleteFlag = true;
                    }
                    else
                    {
                        CustomerAction.deleteCustomer(customer_id);
                    }
                }
                PrintWriter out = response.getWriter();
                response.setContentType("text/xml");
                response.setHeader("Cache-Control", "no-cache");
                if (deleteFlag)
                {
                    out.println("<errorMsg>Customer has active terminals!</errorMsg>");
                    return;
                }
                if (userOP == null || (!userOP.equalsIgnoreCase("save") && !userOP.equalsIgnoreCase("delete")))
                {
                    customer = CustomerAction.getCustomerInfo(customer_id);
                    out.println("<CorpCustomer>");
                    out.println("<Id>" + customer.getId() + "</Id>");
                    out.println("<Name>" + ((customer.getName() != null) ? customer.getName() : "") + "</Name>");
                    out.println("<AltName>" + ((customer.getAltName() != null) ? customer.getAltName() : "") + "</AltName>");
                    out.println("<AddrName>" + ((customer.getAddressName() != null) ? customer.getAddressName() : "") + "</AddrName>");

                    out.println("<Addr1>" + ((customer.getAddress1() != null) ? customer.getAddress1() : "") + "</Addr1>");

                    out.println("<Addr2>" + ((customer.getAddress2() != null) ? customer.getAddress2() : "") + "</Addr2>");

                    out.println("<City>" + ((customer.getCity() != null) ? customer.getCity() : "") + "</City>");

                    out.println("<State>" + ((customer.getState() != null) ? customer.getState() : "") + "</State>");

                    out.println("<Zip>" + ((customer.getZip() != null) ? customer.getZip() : "") + "</Zip>");

                    out.println("<LicenseNbr>" + ((customer.getLicenseNbr() != null) ? customer.getLicenseNbr() : "") + "</LicenseNbr>");

                    out.println("<DealerId>" + ((customer.getDealerId() != null) ? customer.getDealerId() : -1) + "</DealerId>");

                    out.println("<CUserId>" + ((customer.getUserId() != null) ? customer.getUserId() : "") + "</CUserId>");
                    List<Terminal> terminals = customer.getTerminals();
                    out.println("<Terminals>");
                    for (Terminal terminalItem : terminals)
                    {
                        out.println("<Terminal><TerminalId>" + terminalItem.getId() + "</TerminalId>");
                        out.println("<TerminalDetail>" + (StringHelper.isBlank(terminalItem.getTerminalNumber()) ? "" : terminalItem.getTerminalNumber() + "         " + terminalItem.getStatusLabel())
                                + "</TerminalDetail>");
                        out.println("</Terminal>");

                    }
                    out.println("</Terminals>");
                    List<UserView> users = customer.getUsers();
                    out.println("<Users>");
                    for (UserView userItem : users)
                    {
                        out.println("<User><UserId>" + userItem.getId() + "</UserId>");
                        if (customer.getUserId() != null && userItem.getId().longValue() == customer.getUserId().longValue())
                        {
                            out.println("<Telephone>" + ((userItem.getTelephone() != null) ? userItem.getTelephone() : "") + "</Telephone>");
                            out.println("<Fax>" + ((userItem.getFax() != null) ? userItem.getFax() : "") + "</Fax>");
                            out.println("<UserDetail>" + (StringHelper.isBlank(userItem.getFirstName()) ? "" : userItem.getFirstName()) + " "
                                    + (StringHelper.isBlank(userItem.getLastName()) ? "" : userItem.getLastName()) + (StringHelper.isBlank(userItem.getName()) ? "" : "(" + userItem.getName() + ")")
                                    + "         Primary" + "</UserDetail>");

                        }
                        else
                        {
                            out.println("<UserDetail>" + userItem.getFirstName() + " " + userItem.getLastName() + "(" + userItem.getName() + ")" + "</UserDetail>");

                        }
                        out.println("</User>");

                    }
                    out.println("</Users>");

                    List<CustomerBankTerminal> bankAccts = customer.getBankAccts();
                    out.println("<BankAccts>");
                    for (CustomerBankTerminal bankAcctItem : bankAccts)
                    {
                        out.println("<BankAcct><BankAcctId>" + bankAcctItem.getCustomerBankId() + "</BankAcctId>");
                        out.println("<BankAcctDetail>"
                                + (StringHelper.isBlank(bankAcctItem.getBankAccountNumber()) ? "" : bankAcctItem.getBankAccountNumber() + "         " + bankAcctItem.getStatusLabel())
                                + "</BankAcctDetail>");
                        out.println("</BankAcct>");

                    }
                    out.println("</BankAccts>");
                    out.println("</CorpCustomer>");
                }
                out.close();
            }
            catch (Exception e)
            {
            	throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
            }

        }

    }

}
