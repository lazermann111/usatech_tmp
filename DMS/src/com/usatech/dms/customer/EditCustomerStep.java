/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.customer;

import java.util.LinkedList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

import com.usatech.dms.action.CustomerAction;
import com.usatech.layers.common.model.Customer;

/**
 * @author rhuang
 * 
 */
public class EditCustomerStep extends AbstractStep
{

    /*
     * (non-Javadoc)
     * 
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm,
     * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
        int customer_id = form.getInt("customer_id", false, -1);
        String customer_name = form.getString("customer_name", false);
        String errorMessage = null;
        if (customer_id < 0 && customer_name == null)
        {
            errorMessage = "Required Parameter Not Found: customer_id";
            request.setAttribute("errorMessage", errorMessage);
            return;
        }
        try
        {
            String action_type = null;
            Results customer_ids;
            if (customer_id > 0)
            {
                customer_ids = DataLayerMgr.executeQuery("GET_CUSTOMER_ID_BY_ID", new Object[] {customer_id});
            }
            else
            {
                customer_ids = DataLayerMgr.executeQuery("GET_CUSTOMER_ID_BY_NAME", new Object[] {customer_name});

            }
            Customer customer = null;
            if (customer_ids.next())
            {
                customer_id = (customer_ids.getValue("customer_id", Integer.class)).intValue();
                customer = CustomerAction.getCustomer(customer_id);
                action_type = "Edit";
            }
            else
            {
                customer = new Customer();
                customer.setName(customer_name);
                customer.setCountryCode("US");
                customer.setStateCode("AL");
                customer.setTypeId(10);
                action_type = "New";
                customer.setId(null);
            }
            request.setAttribute("customer", customer);
            request.setAttribute("action_type", action_type);
            Results device_counts = DataLayerMgr.executeQuery("GET_DEVICE_COUNT_BY_CUSTOMER", new Object[] {customer_id});
            if (device_counts.next())
            {
                request.setAttribute("device_count", device_counts.getFormattedValue(1));
            }

            Results customer_types = DataLayerMgr.executeQuery("GET_CUSTOMER_TYPES", null);
            request.setAttribute("customer_types", customer_types);
            Results state_cds = DataLayerMgr.executeQuery("GET_STATE_CDS", new Object[] {customer.getCountryCode()});
            request.setAttribute("state_cds", state_cds);
            Results country_cds = DataLayerMgr.executeQuery("GET_COUNTRY_CDS", null);
            LinkedList<String[]> country_list = new LinkedList<String[]>();
            StringBuilder country_cd_str = new StringBuilder();
            String[] country_cd = null;
            while (country_cds.next())
            {
                country_cd = new String[] {country_cds.getFormattedValue(1), country_cds.getFormattedValue(2)};
                if (country_cds.getFormattedValue(1).equalsIgnoreCase("AU") || country_cds.getFormattedValue(1).equalsIgnoreCase("US"))
                {
                    country_list.addFirst(country_cd);
                }
                else
                {
                    country_list.addLast(country_cd);
                }
            }
            for (String[] countryCd : country_list)
            {
                country_cd_str.append(",'" + countryCd[0] + "'");
            }
            request.setAttribute("country_list", country_list);
            request.setAttribute("country_cds", country_cd_str.toString().substring(1));
        }

        catch (Exception e)
        {
        	throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
        }

    }

}
