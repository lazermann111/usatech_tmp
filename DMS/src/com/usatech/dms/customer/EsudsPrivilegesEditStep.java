/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.customer;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;
import simple.util.LinkedHashMap;

import com.usatech.dms.action.CustomerAction;
import com.usatech.layers.common.model.Customer;
import com.usatech.layers.common.util.StringHelper;

/**
 * @author rhuang
 * 
 */
public class EsudsPrivilegesEditStep extends AbstractStep
{

    /*
     * (non-Javadoc)
     * 
     * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm,
     * javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
    	int app_user_id = form.getInt("app_user_id", false, -1);
    	String loc_bg = "white";
    	StringBuilder userAdminReportsBuilder = new StringBuilder("");
    	StringBuilder userMaintReportsBuilder = new StringBuilder("");
    	StringBuilder userOpAccessSchoolBuilder = new StringBuilder("");
    	StringBuilder userOpAccessCampusBuilder = new StringBuilder("");
    	StringBuilder userOpAccessReportsBuilder = new StringBuilder("");
    	String userAdminPrivsList = (String)builderUserAdminPrivsSection(app_user_id, userAdminReportsBuilder, loc_bg, request);
    	request.setAttribute("userAdminPrivsList", userAdminPrivsList);
    	request.setAttribute("userAdminReportsSection", userAdminReportsBuilder.toString());
    	String userMaintPrivsList = (String)builderUserMaintPrivsSection(app_user_id, userMaintReportsBuilder, loc_bg, request);
    	request.setAttribute("userMaintPrivsList", userMaintPrivsList);
    	request.setAttribute("userMaintReportsSection", userMaintReportsBuilder.toString());
    	String userOpAccessSchoolPrivsList = "";
    	userOpAccessSchoolPrivsList = (String)builderOpAccessSchoolPrivsSection(app_user_id, userOpAccessSchoolBuilder, loc_bg, request);
    	request.setAttribute("userOpAccessSchoolPrivsList", userOpAccessSchoolPrivsList);
    	request.setAttribute("userOpAccessSchoolSection", userOpAccessSchoolBuilder.toString());
    	String userOpAccessCampusPrivsList = (String)builderOpAccessCampusPrivsSection(app_user_id, userOpAccessCampusBuilder, loc_bg, request);
    	request.setAttribute("userOpAccessCampusPrivsList", userOpAccessCampusPrivsList);
    	request.setAttribute("userOpAccessCampusSection", userOpAccessCampusBuilder.toString());
    	String userOpAccessReportsPrivsList = (String)builderOpAccessReportsPrivsSection(app_user_id, userOpAccessReportsBuilder, loc_bg, request);
    	request.setAttribute("userOpAccessReportsPrivsList", userOpAccessReportsPrivsList);
    	request.setAttribute("userOpAccessReportsSection", userOpAccessReportsBuilder.toString());
    	
    	try{
    		
    		request.setAttribute("userCustList", (Results)DataLayerMgr.executeQuery("GET_APP_USER_CUSTOMERS", null, false));
    		
    	}catch(Exception e){
    		throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
    	}
    	
    	
    	
    	request.setAttribute("loc_bg", loc_bg);

    }
    
    private String builderUserAdminPrivsSection(int app_user_id, StringBuilder userAdminReportsBuilder, String loc_bg, HttpServletRequest request)throws ServletException{
		
    	Map col1Map = new LinkedHashMap();
    	int col1 = 0;
		Map col2Map = new LinkedHashMap();
		int col2 = 0;
		Map col3Map = new LinkedHashMap();
		int col3 = 0;
		Map permissionsHashMap = new LinkedHashMap();
		Map customerMap = new LinkedHashMap();
		Map ancestorMap = new LinkedHashMap();
		Map descendantMap = new LinkedHashMap();
		Map rowMap = new LinkedHashMap();
		StringBuilder objectCdBuilder = new StringBuilder("");
    	try{
    		
    		
    		Results object_cds = DataLayerMgr.executeQuery("GET_APP_USER_OBJECT_CD", new Object[] {app_user_id, 6, 6}, false);
    		int object_cds_cntr = 0;
    		while(object_cds.next()){
    			object_cds_cntr++;
    			String object_cd = (String)object_cds.get("object_cd");
    			if(object_cds_cntr==1){
    				objectCdBuilder.append(object_cd);
    			}else{
    				objectCdBuilder.append(","+object_cd);
    			}
    		}
    		
    		if(object_cds_cntr>0){
	    		Results cust_loc_arr = DataLayerMgr.executeQuery("GET_APP_USER_CUST_LOC_ANCESTORS_DESCENDANTS_BY_ANCESTOR_LOC_IDS", new Object[] {objectCdBuilder.toString(), 
	    				objectCdBuilder.toString(), objectCdBuilder.toString()}, false);
	    		while(cust_loc_arr.next()){
	    			//col nums will act as rowSpan for the <td>
	    			BigDecimal customer_id = (BigDecimal)cust_loc_arr.get("customer_id");
	    			String customer_name = (String)cust_loc_arr.get("customer_name");
	    			if(col1Map.containsKey(customer_name)){
	    				col1++;
	    				col1Map.put(customer_name, col1);
	    			}else{
	    				col1=0;
	    				col1++;
	    				col1Map.put(customer_name, col1);
	    				customerMap.put(customer_name, customer_id);
	    			}
					
					
	    			BigDecimal ancestor_location_id = (BigDecimal)cust_loc_arr.get("ancestor_location_id");
	    			String ancestor_location_name = (String)cust_loc_arr.get("ancestor_location_name");    			
	    			if(col2Map.containsKey(ancestor_location_name)){
	    				col2++;
	    				col2Map.put(ancestor_location_name, col2);
	    			}else{
	    				col2=0;
	    				col2++;
	    				col2Map.put(ancestor_location_name, col2);
	    				ancestorMap.put(ancestor_location_name, ancestor_location_id);
	    				//get ancestor checkbox permissions
	    				Results checkBoxes = DataLayerMgr.executeQuery("GET_APP_USER_CHKBOX_SQL", new Object[] {app_user_id, 6, 6, ancestor_location_id}, false);
	    				String read_checked = null;
	    				while(checkBoxes.next()){
	    					//only care about the checked box for read here
	    					read_checked = (String)checkBoxes.get("allow_object_read_yn_flag");
	    				}
	    				//just need read and delete to display
	    				permissionsHashMap.put(ancestor_location_name, new Object[] {("read__6_6_"+ancestor_location_id), "Y".equalsIgnoreCase(read_checked) ? "checked=\"checked\"" : "", 
	    						"", "", ("delete__6_6_"+ancestor_location_id), ""});
	    			}
					
	    			BigDecimal descendent_location_id = (BigDecimal)cust_loc_arr.get("descendent_location_id");
	    			String descendent_location_name = (String)cust_loc_arr.get("descendent_location_name");
	    			if(col3Map.containsKey(descendent_location_name)){
	    				col3++;
	    				col3Map.put(descendent_location_name, col3);
	    			}else{
	    				col3=0;
	    				col3++;
	    				col3Map.put(descendent_location_name, col3);
	    				descendantMap.put(descendent_location_name, descendent_location_id);
	    			}
					
					rowMap.put(customer_id+"_"+ancestor_location_id+"_"+descendent_location_id, new Object[]{customer_id, customer_name, ancestor_location_id, ancestor_location_name, descendent_location_id, descendent_location_name});
	    		}
	    		
	    		int maxRows = rowMap.size();
	    		int ancCnt = 0;
	    		int desCnt = 0;
	    		for(int i=0;i<maxRows;i++){
	    			
	    			
	    			userAdminReportsBuilder.append("<tr>");
	    			//check size
	    			//if greater than 0 and ancesCnt = 0, build out row for customer
	    			if(col1Map.size()>0 && ancCnt==0){
	    				String columnInfo = addToColumn(userAdminReportsBuilder, loc_bg, col1Map);
	    				String[] infoData = columnInfo.split("\\|");
	    				ancCnt = (new BigDecimal((String)infoData[1])).intValue();
	    			}
	    			StringBuilder permissionsBuilder = new StringBuilder("");
	    			if(col2Map.size()>0 && desCnt==0){
	    				
	    				String columnInfo = addToColumn(userAdminReportsBuilder, loc_bg, col2Map);
	    				String[] infoData = columnInfo.split("\\|");
	    				buildPermissions(permissionsBuilder, loc_bg,
								permissionsHashMap, columnInfo);
	    				desCnt = (new BigDecimal((String)infoData[1])).intValue();
	    				
	    				
	    			}
	    			
	    			if(col3Map.size()>0){
	    				
	    				addToColumn(userAdminReportsBuilder, loc_bg, col3Map);
	    				ancCnt--;
	    				desCnt--;
	    				
	    			}
	    			
	    			if((!StringHelper.isBlank(permissionsBuilder.toString()))){
	    				userAdminReportsBuilder.append(permissionsBuilder.toString());
	    			}
	    			
	    			userAdminReportsBuilder.append("</tr>");
	    		}
    		
    		}else{
    			objectCdBuilder= new StringBuilder("-1");
    		}
    	}catch(Exception e){
    		throw new ServletException("builderUserAdminPrivsSection Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
    	}finally{
    		col1Map = null;
        	col2Map = null;
    		col3Map = null;
    		permissionsHashMap = null;
    		customerMap = null;
    		ancestorMap = null;
    		descendantMap = null;
    		rowMap =null;
    	}
    	
    	
    	
    	String[] userAdminOperatorCheckBoxes = new String[] {"user_admin_reports_read", "", ""};
    	
    	request.setAttribute("userAdminOperatorCheckBoxes", userAdminOperatorCheckBoxes);
    	
    	return objectCdBuilder.toString();
	}
    
    private String builderUserMaintPrivsSection(int app_user_id, StringBuilder userMaintReportsBuilder, String loc_bg, HttpServletRequest request)throws ServletException{
		
    	Map col1Map = new LinkedHashMap();
    	int col1 = 0;
		Map col2Map = new LinkedHashMap();
		int col2 = 0;
		Map col3Map = new LinkedHashMap();
		int col3 = 0;
		Map permissionsHashMap = new LinkedHashMap();
		Map customerMap = new LinkedHashMap();
		Map ancestorMap = new LinkedHashMap();
		Map descendantMap = new LinkedHashMap();
		Map parentChildMap = new LinkedHashMap();
		
		Map rowMap = new LinkedHashMap();
		StringBuilder objectCdBuilder = new StringBuilder("");
    	try{
    		
    		
    		Results object_cds = DataLayerMgr.executeQuery("GET_APP_USER_OBJECT_CD", new Object[] {app_user_id, 3, 7}, false);
    		int object_cds_cntr = 0;
    		while(object_cds.next()){
    			object_cds_cntr++;
    			String object_cd = (String)object_cds.get("object_cd");
    			if(object_cds_cntr==1){
    				objectCdBuilder.append(object_cd);
    			}else{
    				objectCdBuilder.append(","+object_cd);
    			}
    		}
    		
    		if(object_cds_cntr>0){
	    		Results cust_loc_arr = DataLayerMgr.executeQuery("GET_APP_USER_CUST_LOC_ANCESTORS_DESCENDANTS_BY_ANC_DEC_LOC_IDS", new Object[] {objectCdBuilder.toString(), 
	    				objectCdBuilder.toString(), objectCdBuilder.toString(), objectCdBuilder.toString(), 
	    				objectCdBuilder.toString(), objectCdBuilder.toString()}, false);
	    		while(cust_loc_arr.next()){
	    			//col nums will act as rowSpan for the <td>
	    			BigDecimal customer_id = (BigDecimal)cust_loc_arr.get("customer_id");
	    			String customer_name = (String)cust_loc_arr.get("customer_name");
	    			if(col1Map.containsKey(customer_name)){
	    				col1++;
	    				col1Map.put(customer_name, col1);
	    			}else{
	    				col1=0;
	    				col1++;
	    				col1Map.put(customer_name, col1);
	    				customerMap.put(customer_id, customer_name);
	    			}
					
	    			BigDecimal ancestor_location_id = (BigDecimal)cust_loc_arr.get("ancestor_location_id");
	    			String ancestor_location_name = (String)cust_loc_arr.get("ancestor_location_name");
	    			if(col2Map.containsKey(ancestor_location_name)){
	    				col2++;
	    				col2Map.put(ancestor_location_name, col2);
	    			}else{
	    				col2=0;
	    				col2++;
	    				col2Map.put(ancestor_location_name, col2);
	    				ancestorMap.put(ancestor_location_id, ancestor_location_name );
	    				/*Results checkBoxes = DataLayerMgr.executeQuery("GET_APP_USER_CHKBOX_SQL", new Object[] {app_user_id, 3, 7, ancestor_location_id}, false);
	    				String read_checked = null;
	    				while(checkBoxes.next()){
	    					//only care about the checked box for read here
	    					read_checked = (String)checkBoxes.get("allow_object_read_yn_flag");
	    				}
	    				//just need read and delete to display
	    				permissionsHashMap.put(ancestor_location_name, new Object[] {("read__3_7_"+ancestor_location_id), "Y".equalsIgnoreCase(read_checked) ? "checked=\"checked\"" : "", 
	    						"", "", ("delete__3_7_"+ancestor_location_id), ""});*/
	    			}
					
	    			BigDecimal descendent_location_id = (BigDecimal)cust_loc_arr.get("descendent_location_id");
	    			String descendent_location_name = (String)cust_loc_arr.get("descendent_location_name");
	    			if(col3Map.containsKey(descendent_location_name)){
	    				col3++;
	    				col3Map.put(descendent_location_name, col3);
	    			}else{
	    				col3=0;
	    				col3++;
	    				col3Map.put(descendent_location_name, col3);
	    				descendantMap.put(descendent_location_id, descendent_location_name);
	    			}
					
	    			//get ancestor checkbox permissions
					String parent_child_current = CustomerAction.parentOrChildLocation(app_user_id, 3, 7, ancestor_location_id.toString(), descendent_location_id.toString());
					if(("P"+ancestor_location_id).equals(parent_child_current)){
						parentChildMap.put(ancestor_location_id, parent_child_current);
					}else{
						if("C".equals(parent_child_current)){
							parentChildMap.put(descendent_location_id, parent_child_current);
						}
						
					}
	    			
					rowMap.put(customer_id+"_"+ancestor_location_id+"_"+descendent_location_id, new Object[]{customer_id, customer_name, ancestor_location_id, ancestor_location_name, descendent_location_id, descendent_location_name});
	    		}
	    		
	    		//get permissions based on parent child
	    		Map clonedParentChildMap = new LinkedHashMap();
	    		clonedParentChildMap.putAll(parentChildMap);
	    		Set clonedParentChildMapKeys = clonedParentChildMap.keySet();
	    		Iterator cloneIt = clonedParentChildMapKeys.iterator();
	    		if(cloneIt.hasNext()){
	    			cloneIt.next();
	    		}
	    		
	    		Set parentChildMapKeys = parentChildMap.keySet();
	    		Iterator parentChildIt = parentChildMapKeys.iterator();
	    		
	    		while(parentChildIt.hasNext()){
	    			BigDecimal key = (BigDecimal)parentChildIt.next(); 
	    			String value = (String)parentChildMap.get(key);
	    			Integer rowSpan = null;
	    			//if("C".equals(value)){
					Results checkBoxes = DataLayerMgr.executeQuery("GET_APP_USER_CHKBOX_SQL", new Object[] {app_user_id, 3, 7, key}, false);
					String read_checked = null;
					String write_checked = null;
					if(checkBoxes.next()){
						//only care about the checked box for read here
						read_checked = (String)checkBoxes.get("allow_object_read_yn_flag");
						write_checked = (String)checkBoxes.get("allow_object_modify_yn_flag");
					}
					String descendantName = (String)descendantMap.get(key);
					//just need read and delete to display
					if("C".equals(value)){
						rowSpan = new Integer(1);
	    				permissionsHashMap.put(descendantName, new Object[] {("read__3_7_"+key), ("Y".equalsIgnoreCase(read_checked) ? "checked=\"checked\"" : ""), 
	    						("write__3_7_"+key), ("Y".equalsIgnoreCase(write_checked) ? "checked=\"checked\"" : ""), ("delete__3_7_"+key), "", 1}); //set 1 as rowspan for all children
					}else{
						//get the name of the parent
	    				String ancestorName = (String)ancestorMap.get(key);
	    				if(!permissionsHashMap.containsKey(ancestorName)){
	    					//use the name to get the rowspan
	    					rowSpan = (Integer)col2Map.get(ancestorName);
							permissionsHashMap.put(ancestorName, new Object[] {("read__3_7_"+key), ("Y".equalsIgnoreCase(read_checked) ? "checked=\"checked\"" : ""), 
		    						("write__3_7_"+key), ("Y".equalsIgnoreCase(write_checked) ? "checked=\"checked\"" : ""), ("delete__3_7_"+key), "", rowSpan}); //set the rowSpan on the permissionsMap
						}
					}
					
				}
	    		
	    		int maxRows = rowMap.size();
	    		int ancCnt = 0;
	    		int desCnt = 0;
	    		for(int i=0;i<maxRows;i++){
	    			
	    			userMaintReportsBuilder.append("<tr>");
	    			//check size
	    			//if greater than 0 and ancesCnt = 0, build out row for customer
	    			if(col1Map.size()>0 && ancCnt==0){
	    				String columnInfo = addToColumn(userMaintReportsBuilder, loc_bg, col1Map);
	    				String[] infoData = columnInfo.split("\\|");
	    				ancCnt = (new BigDecimal((String)infoData[1])).intValue();
	    			}
	    			StringBuilder permissionsBuilder = new StringBuilder("");
	    			if(col2Map.size()>0 && desCnt==0){
	    				
	    				String columnInfo = addToColumn(userMaintReportsBuilder, loc_bg, col2Map);
	    				String[] infoData = columnInfo.split("\\|");
	    				if(permissionsHashMap.containsKey(infoData[0])){
	    					buildPermissions(permissionsBuilder, loc_bg,
								permissionsHashMap, columnInfo);
	    				}
	    				desCnt = (new BigDecimal((String)infoData[1])).intValue();
	    				
	    			}
	    			
	    			if(col3Map.size()>0){
	    				
	    				String columnInfo = addToColumn(userMaintReportsBuilder, loc_bg, col3Map);
	    				String[] infoData = columnInfo.split("\\|");
	    				if(permissionsHashMap.containsKey(infoData[0])){
	    					buildPermissions(permissionsBuilder, loc_bg,
								permissionsHashMap, columnInfo);
	    				}
	    				ancCnt--;
	    				desCnt--;
	    				
	    			}
	    			
	    			if((!StringHelper.isBlank(permissionsBuilder.toString()))){
	    				userMaintReportsBuilder.append(permissionsBuilder.toString());
	    			}
	    			userMaintReportsBuilder.append("</tr>");
	    		}
    		
    		}else{
    			objectCdBuilder= new StringBuilder("-1");
    		}
    	}catch(Exception e){
    		throw new ServletException("builderUserMaintPrivsSection Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
    	}finally{
    		col1Map = null;
        	col2Map = null;
    		col3Map = null;
    		permissionsHashMap = null;
    		customerMap = null;
    		ancestorMap = null;
    		descendantMap = null;
    		rowMap =null;
    		parentChildMap = null;
    	}
    	
    	
    	
    	String[] userMaintOperatorCheckBoxes = new String[] {"user_maintenance_read", "user_maintenance_write", ""};
    	
    	request.setAttribute("userMaintOperatorCheckBoxes", userMaintOperatorCheckBoxes);
    	
    	return objectCdBuilder.toString();
	}
    
    private String builderOpAccessSchoolPrivsSection(int app_user_id, StringBuilder userOpAccessSchoolBuilder, String loc_bg, HttpServletRequest request)throws ServletException{
		
    	Map col1Map = new LinkedHashMap();
    	int col1 = 0;
		Map col2Map = new LinkedHashMap();
		int col2 = 0;
		Map col3Map = new LinkedHashMap();
		int col3 = 0;
		Map permissionsHashMap = new LinkedHashMap();
		Map customerMap = new LinkedHashMap();
		Map ancestorMap = new LinkedHashMap();
		Map descendantMap = new LinkedHashMap();
		Map rowMap = new LinkedHashMap();
		StringBuilder objectCdBuilder = new StringBuilder("");
    	try{
    		
    		
    		Results object_cds = DataLayerMgr.executeQuery("GET_APP_USER_OBJECT_CD", new Object[] {app_user_id, 2, 6}, false);
    		int object_cds_cntr = 0;
    		while(object_cds.next()){
    			object_cds_cntr++;
    			String object_cd = (String)object_cds.get("object_cd");
    			if(object_cds_cntr==1){
    				objectCdBuilder.append(object_cd);
    			}else{
    				objectCdBuilder.append(","+object_cd);
    			}
    		}
    		
    		if(object_cds_cntr>0){
	    		Results cust_loc_arr = DataLayerMgr.executeQuery("GET_APP_USER_LOC_ANCESTORS_BY_ANC_LOC_IDS", new Object[] {objectCdBuilder.toString(), 
	    				objectCdBuilder.toString(), objectCdBuilder.toString()}, false);
	    		while(cust_loc_arr.next()){
	    			//col nums will act as rowSpan for the <td>
	    			String customer_id = "";
	    			String customer_name = "";
	    			col1++;
					//col1Map.put(customer_name, 1);
					customerMap.put(customer_name, customer_id);
					
					BigDecimal ancestor_location_id = (BigDecimal)cust_loc_arr.get("aValue");
	    			String ancestor_location_name = (String)cust_loc_arr.get("aLabel");
	    			if(col2Map.containsKey(ancestor_location_name)){
	    				col2++;
	    				col2Map.put(ancestor_location_name, col2);
	    			}else{
	    				col2=0;
	    				col2++;
	    				col2Map.put(ancestor_location_name, col2);
	    				customerMap.put(ancestor_location_name, ancestor_location_id);
	    				Results checkBoxes = DataLayerMgr.executeQuery("GET_APP_USER_CHKBOX_SQL", new Object[] {app_user_id, 2, 6, ancestor_location_id}, false);
	    				String read_checked = null;
	    				while(checkBoxes.next()){
	    					//only care about the checked box for read here
	    					read_checked = (String)checkBoxes.get("allow_object_read_yn_flag");
	    				}
	    				//just need read and delete to display
	    				permissionsHashMap.put(ancestor_location_name, new Object[] {("read__2_6_"+ancestor_location_id), "Y".equalsIgnoreCase(read_checked) ? "checked=\"checked\"" : "", 
	    						"", "", ("delete__2_6_"+ancestor_location_id), ""});
	    			}
					
					String descendent_location_id = "";
	    			String descendent_location_name = "";
	    			col3++;
					//col3Map.put(descendent_location_name, 1);
					descendantMap.put(descendent_location_name, descendent_location_id);
					
					rowMap.put(customer_id+"_"+ancestor_location_id+"_"+descendent_location_id, new Object[]{customer_id, customer_name, ancestor_location_id, ancestor_location_name, descendent_location_id, descendent_location_name});
	    		}
	    		
	    		int maxRows = rowMap.size();
	    		int ancCnt = 0;
	    		int desCnt = 0;
	    		for(int i=0;i<maxRows;i++){
	    			
	    			
	    			userOpAccessSchoolBuilder.append("<tr>");
	    			//check size
	    			//if greater than 0 and ancesCnt = 0, build out row for customer
	    			if(col1Map.size()>=0 && ancCnt==0){
	    				col1Map.put("", 1);
	    				addToColumn(userOpAccessSchoolBuilder, loc_bg, col1Map);
	    			}
	    			StringBuilder permissionsBuilder = new StringBuilder("");
	    			if(col2Map.size()>0 && desCnt==0){
	    				
	    				String columnInfo = addToColumn(userOpAccessSchoolBuilder, loc_bg, col2Map);
	    				String[] infoData = columnInfo.split("\\|");
	    				buildPermissions(permissionsBuilder, loc_bg,
								permissionsHashMap, columnInfo);
	    				ancCnt = (new BigDecimal((String)infoData[1])).intValue();
	    				
	    			}
	    			
	    			if(col3Map.size()>=0){
	    				
	    				col3Map.put("", 1);
	    				addToColumn(userOpAccessSchoolBuilder, loc_bg, col3Map);
	    				/*String columnInfo = addToColumn(userOpAccessSchoolBuilder, loc_bg, col3Map);
	    				String[] infoData = columnInfo.split("\\|");
	    				desCnt = (new BigDecimal((String)infoData[1])).intValue();*/
	    				ancCnt--;
	    				//desCnt--;
	    				
	    			}
	    			
	    			if((!StringHelper.isBlank(permissionsBuilder.toString()))){
	    				userOpAccessSchoolBuilder.append(permissionsBuilder.toString());
	    			}
	    			userOpAccessSchoolBuilder.append("</tr>");
	    		}
    		}else{
    			objectCdBuilder = new StringBuilder("-1");
    		}
    		
    		request.setAttribute("userOpAccessSchoolLocationsList", (Results)DataLayerMgr.executeQuery("GET_APP_USER_LOCATIONS_NOT_IN_LOC_ID", new Object[] {objectCdBuilder.toString(), 
    				objectCdBuilder.toString(), objectCdBuilder.toString()}, false));
    		
    	}catch(Exception e){
    		throw new ServletException("builderOpAccessSchoolPrivsSection Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
    	}finally{
    		col1Map = null;
    		col2Map = null;
    		col3Map = null;
    		permissionsHashMap = null;
    		customerMap = null;
    		ancestorMap = null;
    		descendantMap = null;
    		rowMap = null;
    	}
    	
    	String[] opAccessSchoolOperatorCheckBoxes = new String[] {"operator_access_school_read", "", ""};
    	
    	request.setAttribute("opAccessSchoolOperatorCheckBoxes", opAccessSchoolOperatorCheckBoxes);
    	
    	return objectCdBuilder.toString();
	}
    
    private String builderOpAccessCampusPrivsSection(int app_user_id, StringBuilder opAccessCampusBuilder, String loc_bg, HttpServletRequest request)throws ServletException{
		
    	Map col1Map = new LinkedHashMap();
    	int col1 = 0;
		Map col2Map = new LinkedHashMap();
		int col2 = 0;
		Map col3Map = new LinkedHashMap();
		int col3 = 0;
		Map permissionsHashMap = new LinkedHashMap();
		Map customerMap = new LinkedHashMap();
		Map ancestorMap = new LinkedHashMap();
		Map descendantMap = new LinkedHashMap();
		Map rowMap = new LinkedHashMap();
		StringBuilder objectCdBuilder = new StringBuilder("");
		String hasCustomer = null;
    	try{
    		
    		
    		Results object_cds = DataLayerMgr.executeQuery("GET_APP_USER_OBJECT_CD", new Object[] {app_user_id, 2, 6}, false);
    		int object_cds_cntr = 0;
    		while(object_cds.next()){
    			object_cds_cntr++;
    			String object_cd = (String)object_cds.get("object_cd");
    			if(object_cds_cntr==1){
    				objectCdBuilder.append(object_cd);
    			}else{
    				objectCdBuilder.append(","+object_cd);
    			}
    		}
    		
    		if(object_cds_cntr>0){
	    		Results hasCustomer_arr = DataLayerMgr.executeQuery("GET_APP_USER_OBJECT_CD", new Object[] {app_user_id, 2, 2}, false);
	    		if(hasCustomer_arr.next()){
	    			object_cds_cntr++;
	    			hasCustomer = (String)hasCustomer_arr.get("object_cd");
	    		}
	    		if(hasCustomer==null){
	    			hasCustomer = "-1";
	    			
	    		}
	    		
	    		Results cust_loc_arr = DataLayerMgr.executeQuery("GET_APP_USER_CUST_LOC_ANCESTORS_DESCENDANTS_BY_DESCENDANT_LOC_IDS_WITH_CUST_ID", new Object[] {hasCustomer, hasCustomer, objectCdBuilder.toString(), 
	    				objectCdBuilder.toString(), objectCdBuilder.toString()}, false);
	    		while(cust_loc_arr.next()){
	    			//col nums will act as rowSpan for the <td>
	    			BigDecimal customer_id = (BigDecimal)cust_loc_arr.get("customer_id");
	    			String customer_name = (String)cust_loc_arr.get("customer_name");
	    			if(col1Map.containsKey(customer_name)){
	    				col1++;
	    				col1Map.put(customer_name, col1);
	    			}else{
	    				col1=0;
	    				col1++;
	    				col1Map.put(customer_name, col1);
	    				customerMap.put(customer_id, customer_name);
	    			}
					
	    			BigDecimal ancestor_location_id = (BigDecimal)cust_loc_arr.get("ancestor_location_id");
	    			String ancestor_location_name = (String)cust_loc_arr.get("ancestor_location_name");
	    			if(col2Map.containsKey(ancestor_location_name)){
	    				col2++;
	    				col2Map.put(ancestor_location_name, col2);
	    			}else{
	    				col2=0;
	    				col2++;
	    				col2Map.put(ancestor_location_name, col2);
	    				ancestorMap.put(ancestor_location_id, ancestor_location_name);
	    			}
					
	    			BigDecimal descendent_location_id = (BigDecimal)cust_loc_arr.get("descendent_location_id");
	    			String descendent_location_name = (String)cust_loc_arr.get("descendent_location_name");
	    			col3++;
	    			
	    			if(col3Map.containsKey(descendent_location_name)){
	    				col3++;
	    				col3Map.put(descendent_location_name, col3);
	    			}else{
	    				col3=0;
	    				col3++;
	    				col3Map.put(descendent_location_name, col3);
	    				descendantMap.put(descendent_location_id, descendent_location_name);
	    				//get ancestor checkbox permissions
	    				Results checkBoxes = DataLayerMgr.executeQuery("GET_APP_USER_CHKBOX_SQL", new Object[] {app_user_id, 2, 6, descendent_location_id}, false);
	    				String read_checked = null;
	    				String write_checked = null;
	    				while(checkBoxes.next()){
	    					//only care about the checked box for read here
	    					read_checked = (String)checkBoxes.get("allow_object_read_yn_flag");
	    					write_checked = (String)checkBoxes.get("allow_object_modify_yn_flag");
	    				}
	    				//just need read and delete to display
	    				permissionsHashMap.put(descendent_location_name, new Object[] {("read__2_6_"+descendent_location_id), "Y".equalsIgnoreCase(read_checked) ? "checked=\"checked\"" : "", 
	    						("write__2_6_"+descendent_location_id), "Y".equalsIgnoreCase(write_checked) ? "checked=\"checked\"" : "", ("delete__2_6_"+descendent_location_id), ""});
	    			}
					
					rowMap.put(customer_id+"_"+ancestor_location_id+"_"+descendent_location_id, new Object[]{customer_id, customer_name, ancestor_location_id, ancestor_location_name, descendent_location_id, descendent_location_name});
	    		}
	    		
	    		int maxRows = rowMap.size();
	    		int ancCnt = 0;
	    		int desCnt = 0;
	    		for(int i=0;i<maxRows;i++){
	    			
	    			
	    			opAccessCampusBuilder.append("<tr>");
	    			//check size
	    			//if greater than 0 and ancesCnt = 0, build out row for customer
	    			if(col1Map.size()>0 && ancCnt==0){
	    				String columnInfo = addToColumn(opAccessCampusBuilder, loc_bg, col1Map);
	    				String[] infoData = columnInfo.split("\\|");
	    				ancCnt = (new BigDecimal((String)infoData[1])).intValue();
	    			}
	    			StringBuilder permissionsBuilder = new StringBuilder("");
	    			if(col2Map.size()>0 && desCnt==0){
	    				
	    				String columnInfo = addToColumn(opAccessCampusBuilder, loc_bg, col2Map);
	    				String[] infoData = columnInfo.split("\\|");
	    				desCnt = (new BigDecimal((String)infoData[1])).intValue();
	    				
	    			}
	    			
	    			if(col3Map.size()>0){
	    				
	    				String columnInfo = addToColumn(opAccessCampusBuilder, loc_bg, col3Map);
	    				String[] infoData = columnInfo.split("\\|");
	    				buildPermissions(permissionsBuilder, loc_bg,
								permissionsHashMap, columnInfo);
	    				ancCnt--;
	    				desCnt--;
	    				
	    			}
	    			
	    			if((!StringHelper.isBlank(permissionsBuilder.toString()))){
	    				opAccessCampusBuilder.append(permissionsBuilder.toString());
	    			}
	    			opAccessCampusBuilder.append("</tr>");
	    		}
    		}else{
    			objectCdBuilder = new StringBuilder("-1");
    		}
    		
    		if(!StringHelper.isBlank(hasCustomer)){
    			request.setAttribute("userOpAccessCampusCustList", (Results)DataLayerMgr.executeQuery("GET_APP_USER_CUSTOMERS_EQ_CUST_ID", new Object[]{hasCustomer}, false));
        	}else{
        		request.setAttribute("userOpAccessCampusCustList", (Results)DataLayerMgr.executeQuery("GET_APP_USER_CUSTOMERS", null, false));
        	}
    	
    	}catch(Exception e){
    		throw new ServletException("builderOpAccessCampusPrivsSection Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
    	}finally{
    		col1Map = null;
    		col2Map = null;
    		col3Map = null;
    		permissionsHashMap = null;
    		customerMap = null;
    		ancestorMap = null;
    		descendantMap = null;
    		rowMap = null;
    	}
    	
    	
    	
    	String[] opAccessCampusOperatorCheckBoxes = new String[] {"operator_access_campus_read", "operator_access_campus_write", ""};
    	
    	request.setAttribute("opAccessCampusOperatorCheckBoxes", opAccessCampusOperatorCheckBoxes);
    	
    	return objectCdBuilder.toString();
	}
    
    private String builderOpAccessReportsPrivsSection(int app_user_id, StringBuilder opAccessReportsBuilder, String loc_bg, HttpServletRequest request)throws ServletException{
		
    	Map col1Map = new LinkedHashMap();
    	int col1 = 0;
		Map col2Map = new LinkedHashMap();
		int col2 = 0;
		Map col3Map = new LinkedHashMap();
		int col3 = 0;
		Map permissionsHashMap = new LinkedHashMap();
		Map customerMap = new LinkedHashMap();
		Map ancestorMap = new LinkedHashMap();
		Map descendantMap = new LinkedHashMap();
		Map rowMap = new LinkedHashMap();
		StringBuilder objectCdBuilder = new StringBuilder("");
    	try{
    		
    		
    		Results object_cds = DataLayerMgr.executeQuery("GET_APP_USER_OBJECT_CD", new Object[] {app_user_id, 5, 2}, false);
    		int object_cds_cntr = 0;
    		while(object_cds.next()){
    			object_cds_cntr++;
    			String object_cd = (String)object_cds.get("object_cd");
    			if(object_cds_cntr==1){
    				objectCdBuilder.append(object_cd);
    			}else{
    				objectCdBuilder.append(","+object_cd);
    			}
    		}    		
    		
    		if(object_cds_cntr>0){
    		Results cust_loc_arr = DataLayerMgr.executeQuery("GET_APP_USER_CUST_LOC_ANCESTORS_DESCENDANTS_BY_CUST_IDS", new Object[] {objectCdBuilder.toString(), 
    				objectCdBuilder.toString(), objectCdBuilder.toString()}, false);
    		while(cust_loc_arr.next()){
    			//col nums will act as rowSpan for the <td>
    			BigDecimal customer_id = (BigDecimal)cust_loc_arr.get("customer_id");
    			String customer_name = (String)cust_loc_arr.get("customer_name");
    			if(col1Map.containsKey(customer_name)){
    				col1++;
    				col1Map.put(customer_name, col1);
    			}else{
    				col1=0;
    				col1++;
    				col1Map.put(customer_name, col1);
    				customerMap.put(customer_id,customer_name );
    				//get ancestor checkbox permissions
    				Results checkBoxes = DataLayerMgr.executeQuery("GET_APP_USER_CHKBOX_SQL", new Object[] {app_user_id, 5, 2, customer_id}, false);
    				String read_checked = null;
    				String write_checked = null;
    				while(checkBoxes.next()){
    					//only care about the checked box for read here
    					read_checked = (String)checkBoxes.get("allow_object_read_yn_flag");
    					write_checked = (String)checkBoxes.get("allow_object_modify_yn_flag");
    				}
    				//just need read and delete to display
    				permissionsHashMap.put(customer_name, new Object[] {("read__5_2_"+customer_id), "Y".equalsIgnoreCase(read_checked) ? "checked=\"checked\"" : "", 
    						("write__5_2_"+customer_id), "Y".equalsIgnoreCase(write_checked) ? "checked=\"checked\"" : "", ("delete__5_2_"+customer_id), ""});
    			}
				
    			BigDecimal ancestor_location_id = (BigDecimal)cust_loc_arr.get("ancestor_location_id");
    			String ancestor_location_name = (String)cust_loc_arr.get("ancestor_location_name");
    			if(col2Map.containsKey(ancestor_location_name)){
    				col2++;
    				col2Map.put(ancestor_location_name, col2);
    			}else{
    				col2=0;
    				col2++;
    				col2Map.put(ancestor_location_name, col2);
    				ancestorMap.put(ancestor_location_id, ancestor_location_name);
    			}
				
    			BigDecimal descendent_location_id = (BigDecimal)cust_loc_arr.get("descendent_location_id");
    			String descendent_location_name = (String)cust_loc_arr.get("descendent_location_name");
    			if(col3Map.containsKey(descendent_location_name)){
    				col3++;
    				col3Map.put(descendent_location_name, col3);
    			}else{
    				col3=0;
    				col3++;
    				col3Map.put(descendent_location_name, col3);
    				descendantMap.put(descendent_location_id, descendent_location_name);
    			}
				
				rowMap.put(customer_id+"_"+ancestor_location_id+"_"+descendent_location_id, new Object[]{customer_id, customer_name, ancestor_location_id, ancestor_location_name, descendent_location_id, descendent_location_name});
    		}
    		
    		int maxRows = rowMap.size();
    		int ancCnt = 0;
    		int desCnt = 0;
    		for(int i=0;i<maxRows;i++){
    			
    			
    			opAccessReportsBuilder.append("<tr>");
    			StringBuilder permissionsBuilder = new StringBuilder("");
    			//check size
    			//if greater than 0 and ancesCnt = 0, build out row for customer
    			if(col1Map.size()>0 && ancCnt==0){
    				String columnInfo = addToColumn(opAccessReportsBuilder, loc_bg, col1Map);
    				String[] infoData = columnInfo.split("\\|");
    				buildPermissions(permissionsBuilder, loc_bg,
							permissionsHashMap, columnInfo);
    				ancCnt = (new BigDecimal((String)infoData[1])).intValue();
    			}
    			
    			if(col2Map.size()>0 && desCnt==0){
    				
    				String columnInfo = addToColumn(opAccessReportsBuilder, loc_bg, col2Map);
    				String[] infoData = columnInfo.split("\\|");
    				desCnt = (new BigDecimal((String)infoData[1])).intValue();
    				
    			}
    			
    			if(col3Map.size()>0){
    				
    				addToColumn(opAccessReportsBuilder, loc_bg, col3Map);
    				ancCnt--;
    				desCnt--;
    				
    			}
    			
    			if((!StringHelper.isBlank(permissionsBuilder.toString()))){
    				opAccessReportsBuilder.append(permissionsBuilder.toString());
    			}
    			opAccessReportsBuilder.append("</tr>");
    		}
    		}else{
    			objectCdBuilder = new StringBuilder("-1");
    		}
    		request.setAttribute("userOpAccessReportsCustList", (Results)DataLayerMgr.executeQuery("GET_APP_USER_CUSTOMERS_NOT_IN_CUST_ID", new Object[] {objectCdBuilder.toString(), 
    				objectCdBuilder.toString(), objectCdBuilder.toString()}, false));
    		
    	
    	}catch(Exception e){
    		throw new ServletException("builderOpAccessReportsPrivsSection Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
    	}finally{
    		col1Map = null;
    		col2Map = null;
    		col3Map = null;
    		permissionsHashMap = null;
    		customerMap = null;
    		ancestorMap = null;
    		descendantMap = null;
    		rowMap = null;
    	}
    	
    	String[] opAccessReportsOperatorCheckBoxes = new String[] {"operator_reports_read", "operator_reports_write", ""};
    	
    	request.setAttribute("opAccessReportsOperatorCheckBoxes", opAccessReportsOperatorCheckBoxes);
    	
    	return objectCdBuilder.toString();
	}

    /**
     * Builds the <td>'s for the permissions read, write and execute for a single row.
     * @param builder
     * @param loc_bg
     * @param permissionsHashMap
     * @param columnInfo
     */
	private void buildPermissions(StringBuilder builder,
			String loc_bg, Map permissionsHashMap, String columnInfo) {
		String[] infoData = columnInfo.split("\\|");
		Object[] checkBoxInfo = (Object[])permissionsHashMap.get(infoData[0]);
		if(checkBoxInfo!=null){
			if((!StringHelper.isBlank((String)checkBoxInfo[0]))){
				builder.append("<td rowspan=\"" + infoData[1] + "\" bgColor=\"" +loc_bg+ "\"><label><input type=\"checkbox\" name=\"" + checkBoxInfo[0] + "\" value=\"Y\" tabindex=\"85\"" + checkBoxInfo[1] + " class=\"normal\" onclick=\"toggleColor(this)\" /></label></td>");
			}else{
				builder.append("<td rowspan=\"" + infoData[1] + "\" bgColor=\"" +loc_bg+ "\">&nbsp;</td>");
	
			}
			if((!StringHelper.isBlank((String)checkBoxInfo[2]))){
				builder.append("<td rowspan=\"" + infoData[1] + "\" bgColor=\"" +loc_bg+ "\"><label><input type=\"checkbox\" name=\"" + checkBoxInfo[2] + "\" value=\"Y\" tabindex=\"85\"" + checkBoxInfo[3] + " class=\"normal\" onclick=\"toggleColor(this)\" /></label></td>");
			}else{
				builder.append("<td rowspan=\"" + infoData[1] + "\" bgColor=\"" +loc_bg+ "\">&nbsp;</td>");
	
			}
			if((!StringHelper.isBlank((String)checkBoxInfo[4]))){
				builder.append("<td rowspan=\"" + infoData[1] + "\" bgColor=\"" +loc_bg+ "\"><label><input type=\"checkbox\" name=\"" + checkBoxInfo[4] + "\" value=\"Y\" tabindex=\"85\"" + checkBoxInfo[5] + " class=\"normal\" onclick=\"toggleColor(this)\" /></label></td>");
			}else{
				builder.append("<td rowspan=\"" + infoData[1] + "\" bgColor=\"" +loc_bg+ "\">&nbsp;</td>");
	
			}
		}
	}
	
	/**
	 * Adds a <td> to the row from the col<column_number>Map passed in. 
	 * @param builder
	 * @param loc_bg
	 * @param col1Map
	 * @return
	 */
	private String addToColumn(StringBuilder builder,
			String loc_bg, Map col1Map) {
		Set keys = col1Map.keySet();
		String name = "";
		Integer rowSpan = null;
		Iterator it = keys.iterator();
		if(it.hasNext()){
			name = (String)it.next();
			rowSpan = (Integer)col1Map.get(name);
			builder.append("<td rowspan=" + rowSpan + " bgColor=" +loc_bg+ ">"+name+"</td>");
		}
		it.remove();
		return name+"|"+rowSpan;
	}

}
