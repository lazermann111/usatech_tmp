package com.usatech.dms.util;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import com.usatech.layers.common.util.StringHelper;

import simple.io.ByteArrayUtils;

public class DMSSecurityUtil {
	private static final String KEY = "703449366F54514E34735039326C5947";
	
	/** Minimum length for a decent password */
	  public static final int MIN_LENGTH = 10;

	  /** The random number generator. */
	  //protected static java.util.Random r = new java.util.Random();
	  protected static java.security.SecureRandom r = new java.security.SecureRandom();
	  
	  /*
	   * Set of characters that is valid. Must be printable, memorable, and "won't
	   * break HTML" (i.e., not ' <', '>', '&', '=', ...). or break shell commands
	   * (i.e., not ' <', '>', '$', '!', ...). I, L and O are good to leave out,
	   * as are numeric zero and one, but we'll leave them here to be similar to the old DA app.
	   */
	  protected static char[] goodChar = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'j', 'k', 'm', 'n', 'o', 
		  'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '@', '%', '#',
	      'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 
	      'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
	      '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', };	  

	/**
	 * Encrypt the Input String with key passed in. 
	 * @param inKey
	 * @param input
	 * @return
	 * @throws Exception
	 */
	public static String rijndaelEncrypt(String inKey, String input) throws Exception{
		if (StringHelper.isBlank(input))
			return "";
		
		byte[] inputBytes;
		int remainder = input.length() % 16;
		
		String padding = new String(new byte[] {0x00});
		if (remainder > 0) {
			StringBuilder sb = new StringBuilder(input);
			for (int i = 0; i < 16 - remainder; i++)
				sb.append(padding);
			inputBytes = sb.toString().getBytes();
		} else
			inputBytes = input.getBytes();
		
		byte[] keyBytes = ByteArrayUtils.fromHex(inKey);
		
		SecretKeySpec key = new SecretKeySpec(keyBytes, "Rijndael");
	    Cipher cipher = Cipher.getInstance("AES/ECB/NoPadding"); 
	    
	    cipher.init(Cipher.ENCRYPT_MODE, key);
	    
	    byte[] encrypted = cipher.doFinal(inputBytes);
	    return new String(encrypted);
	}
	
	public static String rijndaelEncrypt(String input) throws Exception{
		return rijndaelEncrypt(KEY, input);
	}	
	
	/**
	 * Decrypt the Input String with key passed in. 
	 * @param inKey
	 * @param input
	 * @return
	 * @throws Exception
	 */
	public static String rijndaelDecrypt(String inKey, String input) throws Exception{
		if (StringHelper.isBlank(input))
			return "";
		
		byte[] keyBytes = ByteArrayUtils.fromHex(inKey);
		
		SecretKeySpec key = new SecretKeySpec(keyBytes, "Rijndael");
	    Cipher cipher = Cipher.getInstance("AES/ECB/NoPadding"); 
	    
	    cipher.init(Cipher.DECRYPT_MODE, key);
	    
	    byte[] decrypted = cipher.doFinal(input.getBytes());
	    String output = new String(decrypted);
	    
	    // remove trailing 0x00 bytes
	    int pos = output.indexOf(0);
	    if (pos > 0)
	    	return output.substring(0, pos);
	    else
	    	return output;
	}
	
	public static String rijndaelDecrypt(String input) throws Exception{
		return rijndaelDecrypt(KEY, input);
	}	
	
	/**
	 * Decrypt the hex Input String with key passed in. 
	 * @param inKey
	 * @param inputHex
	 * @return
	 * @throws Exception
	 */
	public static String rijndaelDecryptHex(String inputHex) throws Exception{
		if (StringHelper.isBlank(inputHex))
			return "";
		
		String input;
		int remainder = inputHex.length() % 32;
		if (remainder > 0) {
			StringBuilder sb = new StringBuilder(inputHex);
			for (int i = 0; i < 32 - remainder; i++)
				sb.append('0');
			input = StringHelper.decodeHexString(sb.toString());
		} else
			input = StringHelper.decodeHexString(inputHex);
		return rijndaelDecrypt(input);
	}
	
    /* Generate a Password object with a random password. */
    public static String getNextRandomPwd() {
      StringBuilder sb = new StringBuilder();
      for (int i = 0; i < MIN_LENGTH; i++) {
        sb.append(goodChar[r.nextInt(goodChar.length)]);
      }
      return sb.toString();
    }
    
}
