package com.usatech.dms.util;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;

import simple.db.DataLayerException;

public class FileTransferTypeList extends DMSValuesList{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7856405982370916912L;

	/**
	 * 
	 */	
	public FileTransferTypeList(){}
	
	public FileTransferTypeList(String inQueryName){
		
		super(inQueryName);
	}
	
}
