package com.usatech.dms.util;

import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.util.NameValuePair;

public class EsudsAlphaCharList extends DMSValuesList{

	/**
	 * 
	 */
	
	private static final long serialVersionUID = 8791735000163378634L;
	
	public static int locationCount = 0;
	
	public EsudsAlphaCharList(){}
	
	public EsudsAlphaCharList(String inQueryName, ServletContext servletContext) throws ServletException{
		super(inQueryName);
	}
	
	public List<NameValuePair> getList(ServletContext servletContext) throws ServletException{
		
		try{
			if(list == null){
				load();
				Iterator it = list.iterator();
				while(it.hasNext()){
					simple.util.NameValuePair esudsAlphaNVP = (simple.util.NameValuePair)it.next();
					String alphaChar = esudsAlphaNVP.getValue();
					List esudsAlpaSubList = (List<simple.util.NameValuePair>)servletContext.getAttribute("dms_values_list_esudsAlphaCharsList_sub_"+alphaChar);
					if(esudsAlpaSubList == null || esudsAlpaSubList.size() == 0){
						esudsAlpaSubList = new java.util.ArrayList();
						//create and add sublist to application scope
						String[] param = {alphaChar};
						Results location_custom_submenu = (Results) DataLayerMgr.executeQuery("GET_LOCATION_ESUDS_SUBMENU", param, false);
						while ( location_custom_submenu.next() )   {  
							
							java.math.BigDecimal location_id = (java.math.BigDecimal)location_custom_submenu.get("location_id");
							String location_name = location_custom_submenu.getFormattedValue("location_name");
							esudsAlpaSubList.add(new NameValuePair(location_name, location_id.toString()));
						}
						servletContext.setAttribute("dms_values_list_esudsAlphaCharsList_sub_"+alphaChar, esudsAlpaSubList);
						if (log.isDebugEnabled())
							log.debug("ValuesList 'EsudsAlphaCharList sub " + alphaChar + "' getList(): list object now loaded: list current size = (" +  esudsAlpaSubList.size() + ") records");
					}
				}
				//log.info("ValuesList 'LocationList' getList(): list object now loaded: list current size = (" +  list.size() + ") records");
			}
		}catch(Exception e){
			throw new ServletException(e.getMessage());
		}
		return list;
	}
	
	public void clearList(ServletContext servletContext){
		
		Iterator it = list.iterator();
		while(it.hasNext()){
			simple.util.NameValuePair esudsAlphaNVP = (simple.util.NameValuePair)it.next();
			String alphaChar = esudsAlphaNVP.getValue();
			servletContext.setAttribute("dms_values_list_esudsAlphaCharsList_sub_"+alphaChar, null);
			//application.setAttribute("dms_values_list_esudsAlphaCharsList_sub_"+alphaChar, null);
		}
		list = null;
		if (log.isDebugEnabled())
			log.debug("ValuesList '" + this.listType + "' clearList(): lazy load, will load on next getList() call: list current value = " +  list);
	}
}
