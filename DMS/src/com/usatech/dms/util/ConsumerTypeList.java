package com.usatech.dms.util;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;

import simple.db.DataLayerException;

public class ConsumerTypeList implements Serializable{

	/**
	 * 
	 */
	
	private static final simple.io.Log log = simple.io.Log.getLog();
	
	private static final long serialVersionUID = 8708023767024499216L;
	
	public static List list = null;
	
	private static ConsumerTypeList instance;
	
	private static String queryName = "GET_ALL_CONSUMER_TYPES";
	
	public ConsumerTypeList()throws ServletException, SQLException, DataLayerException{
		list = null;
	}


	public static ConsumerTypeList getInstance() throws ServletException, SQLException, DataLayerException{
		instance = null;
		instance = new ConsumerTypeList();
		if (log.isDebugEnabled())
			log.debug("ValuesList 'ConsumerTypeList' getInstance(): lazy load, will load on first getList() call: list current value = " +  list);
		return instance;
	}
	
	public static List getList()throws ServletException, SQLException, DataLayerException{
		if(list == null){
			load();
			if (log.isDebugEnabled())
				log.debug("ValuesList 'ConsumerTypeList' getList(): list object now loaded: list current size = (" +  list.size() + ") records");
		}
		return list;
	}
	
	private static synchronized void load()throws ServletException, SQLException, DataLayerException{ 
		if(list == null){
			list = Helper.buildSelectList(queryName, true, null);
		}
	} 
	
	public static void clearList(){
		list = null;
	}
}
