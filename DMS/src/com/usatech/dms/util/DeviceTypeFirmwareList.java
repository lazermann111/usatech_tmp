/**
 * $Id$
 *
 * Copyright 2009 USATechnologies. All rights reserved.
 *
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL.Use is subject to license terms.
 */

package com.usatech.dms.util;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.ServletException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.Results;

public class DeviceTypeFirmwareList extends DMSValuesList {
	private static final long serialVersionUID = 8708023767024499216L;

	private static int[] deviceTypes = null;

	public DeviceTypeFirmwareList() {
	}

	public DeviceTypeFirmwareList(String inQueryName) {
		super(inQueryName);
	}

	protected int[] getOrCreateDeviceTypes() throws ServletException {
		if(deviceTypes == null) {
			ResourceBundle bundle = ResourceBundle.getBundle(DMSConstants.APP_CD);
			try {
				deviceTypes = ConvertUtils.convert(int[].class, bundle.getString("device_id_menu_show_firmware"));
			} catch(ConvertException e) {
				throw new ServletException(e);
			}
		}
		return deviceTypes;
	}

	protected synchronized void load() throws ServletException, SQLException, DataLayerException {
		if(list == null) {
			list = new ArrayList();
			for(int deviceType : getOrCreateDeviceTypes()) {
				Results result = DataLayerMgr.executeQuery(queryName, new Object[] { deviceType }, false);
				Map<String,List<String>> nvpMap = new HashMap<String, List<String>>();
				List<String> firmwareList = new ArrayList<String>();
				while(result.next()) {
					firmwareList.add(new StringBuilder(result.getFormattedValue("device_setting_value")).append(Helper.FIELD_DELIMITER).append(" (").append(result.getFormattedValue("device_count")).append(")").toString());
				}
				nvpMap.put(Integer.toString(deviceType), firmwareList);
				list.add(nvpMap);
			}
		}
	}

	/**
	 * Convenience method checks to see if a list of firmware exists for a
	 * passed in deviceId, and if so, returns it.
	 * 
	 * @param deviceType
	 */
	public List getFirmwareListPerDeviceId(String deviceType) throws ServletException, SQLException, DataLayerException {
		List retFirmwareList = null;
		if(list == null) {
			getList();
		}
		Iterator it1 = list.iterator();
		while(it1.hasNext()) {
			Map tempMap = null;
			tempMap = (Map) it1.next();
			if(tempMap.containsKey(deviceType)) {
				retFirmwareList = (List) tempMap.get(deviceType);
				break;
			} else {
				continue;
			}
		}
		return retFirmwareList;

	}

	public static int[] getDeviceTypes() {
		return deviceTypes;
	}
	
	public static boolean hasFirmwareList(int deviceTypeId) {
		for (int i = 0; i < deviceTypes.length; i++) {
			if (deviceTypeId == deviceTypes[i])
				return true;
		}
		return false;
	}

	public static void setDeviceTypes(int[] deviceTypes) {
		DeviceTypeFirmwareList.deviceTypes = deviceTypes;
	}
}
