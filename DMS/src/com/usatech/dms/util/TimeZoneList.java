package com.usatech.dms.util;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import javax.servlet.ServletException;

import simple.db.DataLayerException;

public class TimeZoneList extends DMSValuesList{

	/**
	 * 
	 */
	
	private static final simple.io.Log log = simple.io.Log.getLog();
	
	private static final long serialVersionUID = -8898917184403711263L;
	
	private static String queryName = "GET_ALL_TIME_ZONES";
	
	public TimeZoneList(){}
	
	public TimeZoneList(String inQueryName){
		
		super(inQueryName);
	}
	
}