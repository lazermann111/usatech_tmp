package com.usatech.dms.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.process.ProcessType;
import simple.app.Publisher;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.BufferStream;
import simple.io.ByteInput;
import simple.io.HeapBufferStream;
import simple.results.Results;
import simple.servlet.BasicServletUser;
import simple.servlet.RequestUtils;
import simple.text.StringUtils;

public class DMSUtils {
	
	protected static Publisher<ByteInput> publisher;

	public static Publisher<ByteInput> getPublisher() {
		return publisher;
	}

	public static void setPublisher(Publisher<ByteInput> publisher) {
		DMSUtils.publisher = publisher;
	}
	
	public static long getUserIdByEmail(String email) throws SQLException, DataLayerException, ConvertException{
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userEmail", email);
		Results rs = DataLayerMgr.executeQuery("GET_USER_BY_EMAIL", params);
		if (rs.next()) {
			return rs.getValue("userId", long.class);
		} else{
			throw new DataLayerException("Cannot find user by " + email + " email");
		}
	}
	
	public static long registerProcessRequest(HttpServletRequest request, ProcessType processType, String processDescription, String notifyEmail, Map<String, Object> parameters, InputStream content) throws IOException, ServletException, ConvertException, SQLException, DataLayerException, ServiceException {
		
		BasicServletUser user = (BasicServletUser) RequestUtils.getUser(request);
		long userId = getUserIdByEmail(user.getEmailAddress());
		
		Map<String, Object> params = new HashMap<>();
		params.put("processDescription", processDescription);
		params.put("profileId", userId);
		params.put("userId", userId);
		params.put("processType", processType);
		params.put("notifyEmail", notifyEmail);
		params.put("processContent", content);
		BufferStream buffer = new HeapBufferStream();
		try (Writer writer = new OutputStreamWriter(buffer.getOutputStream())) {
			StringBuffer url = request.getRequestURL();
			if(url != null)
				StringUtils.writeEncodedNVP(writer, null, "requestURL", url.toString());
			StringUtils.writeEncodedNVP(writer, null, "remoteAddr", request.getRemoteAddr());
			StringUtils.writeEncodedNVP(writer, null, "remotePort", String.valueOf(request.getRemotePort()));
			HttpSession session = request.getSession(false);
			if(session != null)
				StringUtils.writeEncodedNVP(writer, null, "sessionId", session.getId());
			for(Enumeration<String> en = request.getHeaderNames(); en.hasMoreElements();) {
				String name = en.nextElement();
				StringUtils.writeEncodedNVP(writer, "HEADER", name, request.getHeader(name));
			}
			StringUtils.writeEncodedMap(writer, "PARAM", parameters);
		}
		params.put("processParams", buffer.getInputStream());
		DataLayerMgr.executeCall("CREATE_PROCESS_REQUEST", params, true);
		long processRequestId = ConvertUtils.getLong(params.get("processRequestId"));
		MessageChain mc = new MessageChainV11();
		MessageChainStep step = mc.addStep("usat.process.request.opr");
		step.setAttribute(CommonAttrEnum.ATTR_PROCESS_ID, processRequestId);
		step.setAttribute(CommonAttrEnum.ATTR_USER_ID, userId);
		MessageChainService.publish(mc, publisher);
		return processRequestId;
	}

}