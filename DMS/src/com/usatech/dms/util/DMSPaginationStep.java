package com.usatech.dms.util;

import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.app.DialectResolver;
import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;

import com.usatech.layers.common.util.PaginationUtil;
import com.usatech.layers.common.util.StringHelper;

public class DMSPaginationStep extends AbstractStep {

	protected static final simple.io.Log log = simple.io.Log.getLog();
	
	@Override
	public void perform(Dispatcher dispatcher, InputForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException {
		// TODO Auto-generated method stub

	}
	
	protected void setPaginatedResultsOnRequest(InputForm form,
			HttpServletRequest request, String queryStart, String queryBase, String queryEnd, String querySortIndex, String[] querySortFields, String resultsParamName)
			throws ServletException {
		setPaginatedResultsOnRequest(form, request, queryStart, queryBase, queryEnd, querySortIndex, querySortFields, resultsParamName, null, null, null);
	}
	
	protected void setPaginatedResultsOnRequest(InputForm form, HttpServletRequest request, String queryStart, String queryBase, String queryEnd, String querySortIndex, String[] querySortFields, String resultsParamName, Object[] inParams) throws ServletException {
		setPaginatedResultsOnRequest(form, request, queryStart, queryBase, queryEnd, querySortIndex, querySortFields, resultsParamName, inParams, null, null);
	}
	
	/**
	 * For steps that are required to forward paginated results.
	 * @param form
	 * @param request
	 * @param queryStart
	 * @param queryBase
	 * @param queryEnd
	 * @param querySortIndex
	 * @param querySortFields
	 * @param resultsParamName
	 * @throws ServletException
	 */
	protected void setPaginatedResultsOnRequest(InputForm form,
			HttpServletRequest request, String queryStart, String queryBase, String queryEnd, String querySortIndex, String[] querySortFields, String resultsParamName, Object[] inParams,
			String idFieldName, String itemURL)
			throws ServletException {
		

        String paramTotalCount = PaginationUtil.getTotalField(null);
        String paramPageIndex = PaginationUtil.getIndexField(null);
        String paramPageSize = PaginationUtil.getSizeField(null);
        String paramSortIndex = PaginationUtil.getSortField(null);

        try
        {
            // pagination parameters
            long totalCount = getTotalCount(form, request, queryBase, queryEnd, inParams, paramTotalCount, idFieldName, itemURL);
            
            request.setAttribute(paramTotalCount, String.valueOf(totalCount));

            int pageIndex = form.getInt(paramPageIndex, false, 1);
            int pageSize = form.getInt(paramPageSize, false, -1);
            if (pageSize < 0) {
            	if (form.getBoolean("bulk", false, false)) {
            		pageSize = Integer.MAX_VALUE;
            		request.setAttribute(paramPageSize, pageSize);
            	} else 
            		pageSize = PaginationUtil.DEFAULT_PAGE_SIZE;	        	
	        }
            int minRowToFetch = PaginationUtil.getStartNum(pageSize, pageIndex);
            int maxRowToFetch = PaginationUtil.getEndNum(pageSize, pageIndex);

            String sortIndex = form.getString(paramSortIndex, false);
            sortIndex = StringHelper.isBlank(sortIndex) ? querySortIndex : sortIndex;
            
            request.setAttribute(paramSortIndex, sortIndex);
            
            String orderBy = PaginationUtil.constructOrderBy(querySortFields, sortIndex);
            
            String query;
			if (!DialectResolver.isOracle()) {
				query = new StringBuilder("select * from (select * from (")
		                .append(" select pagination_temp.*, row_number() over() as rnum from (")
		                .append(queryStart).append(queryBase).append(orderBy).append(queryEnd)
		                .append(" limit ").append(maxRowToFetch).append(") pagination_temp) sq_end ")
		                .append(")tmp where rnum  >= ").append(minRowToFetch).toString();
			} else {
				query = new StringBuilder("select /*+ ALL_ROWS */ * from (select * from (")
		                .append(" select pagination_temp.*, ROWNUM rnum from (")
		                .append(queryStart).append(queryBase).append(orderBy).append(queryEnd)
		                .append(") pagination_temp) where rnum <= ").append(maxRowToFetch)
		                .append(") where rnum  >= ").append(minRowToFetch).toString();
			}
            
            Results results = DataLayerMgr.executeSQL("OPER", query, inParams, null);
            request.setAttribute(resultsParamName, results);
        }
        catch (Exception e)
        {
            throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
        }
	}
	
	protected void setPaginatedResultsOnRequestNamedSQL(InputForm form,
			HttpServletRequest request, String queryNameData, String queryNameCount, String querySortIndex, String resultsParamName, Object[] inParams, Object[] inOrderBy)
			throws ServletException {
		

        String paramTotalCount = PaginationUtil.getTotalField(null);
        String paramPageIndex = PaginationUtil.getIndexField(null);
        String paramPageSize = PaginationUtil.getSizeField(null);
        String paramSortIndex = PaginationUtil.getSortField(null);

        try
        {
            // pagination parameters
            long totalCount = getTotalCountNamedSQL(form, request, queryNameCount, inParams,
					paramTotalCount);
            
            request.setAttribute(paramTotalCount, String.valueOf(totalCount));

            int pageIndex = form.getInt(paramPageIndex, false, 1);
            int pageSize = form.getInt(paramPageSize, false, PaginationUtil.DEFAULT_PAGE_SIZE);
            int minRowToFetch = PaginationUtil.getStartNum(pageSize, pageIndex);
            int maxRowToFetch = PaginationUtil.getEndNum(pageSize, pageIndex);

            String sortIndex = form.getString(paramSortIndex, false);
            sortIndex = StringHelper.isBlank(sortIndex) ? querySortIndex : sortIndex;
            //String orderBy = PaginationUtil.constructOrderBy(querySortFields, sortIndex);
            
            request.setAttribute(paramSortIndex, sortIndex);
            
            for(int i=0;i<inOrderBy.length;i++){
            	inOrderBy[i]=sortIndex;
            }
            
            Object[] newParamArray = new Object[inParams.length + inOrderBy.length + 2];            
            
            System.arraycopy(inParams, 0, newParamArray, 0, inParams.length);
            System.arraycopy(inOrderBy, 0, newParamArray, inParams.length, inOrderBy.length);
            newParamArray[inParams.length+inOrderBy.length] = maxRowToFetch;
            newParamArray[inParams.length+inOrderBy.length+1] = minRowToFetch;
            
            Results results = DataLayerMgr.executeQuery(queryNameData, newParamArray, null);
            
            request.setAttribute(resultsParamName, results);
        }
        catch (Exception e)
        {
            throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
        }
	}

	private long getTotalCount(InputForm form, HttpServletRequest request,
			String queryBase, String queryEnd, Object[] inParams,
			String paramTotalCount, String idFieldName, String itemURL) throws ServletException, SQLException,
			DataLayerException, ConvertException {
		long totalCount = form.getLong(paramTotalCount, false, -1);
		if (totalCount == -1)
		{
		    // if the total count is not retrieved yet, get it now
		    StringBuilder sql = new StringBuilder("SELECT COUNT(1) ");
		    if (idFieldName != null)
		    	sql.append(", MAX(").append(idFieldName).append(") ");
		    sql.append(queryBase).append(queryEnd);
			Results total = DataLayerMgr.executeSQL("OPER", sql.toString(), inParams, null);
		    if (total.next())
		    {
		        totalCount = total.getValue(1, long.class);
		        if (idFieldName != null && totalCount == 1 && itemURL != null) {
		        	form.setRedirectUri(new StringBuilder(itemURL).append(total.getValue(2, String.class)).toString());
		        	return totalCount;
		        }
		    }
		    else
		    {
		        totalCount = 0;
		    }		    
		}
		return totalCount;
	}
	
	private long getTotalCountNamedSQL(InputForm form, HttpServletRequest request,
			String queryName, Object[] inParams,
			String paramTotalCount) throws ServletException, SQLException,
			DataLayerException, ConvertException {
		long totalCount = form.getLong(paramTotalCount, false, -1);
		if (totalCount == -1)
		{
		    // if the total count is not retrieved yet, get it now
		    Results total = DataLayerMgr.executeQuery(queryName, inParams, false);
		    //Results maxPriority = DataLayerMgr.executeQuery("GET_MAX_PRIORITY", new Object[] {pos_id}, true);
		    
		    if (total.next())
		    {
		        totalCount = total.getValue(1, long.class);
		    }
		    else
		    {
		        totalCount = 0;
		    }
		}
		return totalCount;
	}
}
