package com.usatech.dms.util;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;

import simple.db.DataLayerException;
import simple.text.StringUtils;
import simple.util.NameValuePair;

public abstract class DMSValuesList implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2259227691973903322L;

	/**
	 * 
	 */
	public DMSValuesList(){}
    protected DMSValuesList(String inQueryName, String listName) {
    	this.list = null;
    	this.queryName = inQueryName;
    	this.listType = listName;
    	if (log.isDebugEnabled())
    		log.debug("ValuesList '" + this.listType + "' init(): lazy load, will load on first getList() call: list current value = " +  list);
    	
    }
    protected DMSValuesList(String inQueryName) {
    	this.list = null;
    	this.queryName = inQueryName;
    	this.listType = this.getClass().getSimpleName();
    	if (log.isDebugEnabled())
    		log.debug("ValuesList '" + this.listType + "' init(): lazy load, will load on first getList() call: list current value = " +  list);
    	
    }
	
	protected static final simple.io.Log log = simple.io.Log.getLog();
	
	public List list;
	
	protected String listType;
	
	protected String queryName;
	
	public List<NameValuePair> getList()throws ServletException, SQLException, DataLayerException{
		if(list == null){
			try {
			load();
			} catch (Exception e) {
				log.error(new StringBuilder("Error loading value list, listType: ").append(listType).append(", queryName: ").append(queryName).append(", error: ").append(e.getMessage()).toString());
				if (log.isDebugEnabled())
					log.debug(StringUtils.exceptionToString(e));
			}
		}
		return list == null ? new ArrayList() : list;
	}
	
	public String getName(String value) throws ServletException, SQLException, DataLayerException {
		Iterator<NameValuePair> iterator = getList().iterator();
		while(iterator.hasNext()) {
			NameValuePair nvp = iterator.next();
			if (value.equals(nvp.getValue()))
				return nvp.getName();
		}
		return null;
	}
	
	protected synchronized void load()throws ServletException, SQLException, DataLayerException{ 
			list = Helper.buildSelectList(this.queryName, false, null);
			if (log.isDebugEnabled())
				log.debug("ValuesList '" + this.listType + "' load(): list object now loaded: list current size = (" +  list.size() + ") records");

	} 
	
	public void clearList(){
		list = null;
		if (log.isDebugEnabled())
			log.debug("ValuesList '" + this.listType + "' clearList(): lazy load, will load on next getList() call: list current value = " +  list);
	}
	
	public boolean isLoaded() {
		return !(list == null);
	}
}
