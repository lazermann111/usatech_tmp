package com.usatech.dms.util;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;

import simple.bean.ConvertException;
import simple.db.DataLayerException;

public class LocationList extends DMSValuesList{

	/**
	 * 
	 */
	
	private static final long serialVersionUID = 8791735000163378634L;
	
	public static int locationCount = 0;
	
	public LocationList(){}
	
	public LocationList(String inQueryName){
		
		super(inQueryName);
	}
	
	public List getList() throws ServletException, SQLException, DataLayerException{
		Integer curLocCount = new Integer(-1);
		try{
			curLocCount = getCountLocations();
		}catch(ConvertException e){
			log.info("ValuesList 'LocationList' ConvertException occurred: " + e + ": setting count of locations of to zero");
			curLocCount = 0;
		}
		if(list == null || locationCount < curLocCount.intValue()){
			locationCount = curLocCount.intValue();
			load();
			//log.info("ValuesList 'LocationList' getList(): list object now loaded: list current size = (" +  list.size() + ") records");
		}
		return list;
	}
	
	public Integer getCountLocations() throws SQLException, DataLayerException, ConvertException{
		return com.usatech.dms.action.LocationActions.getAllLocationCount();
	}
}
