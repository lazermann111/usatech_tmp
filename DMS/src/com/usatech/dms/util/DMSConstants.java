/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.util;


public class DMSConstants
{
	public static final String APP_CD = "DMS";
	public static final String HOME_PAGE = "/home.i";
    public static final String PARAM_ACTION_TYPE = "action";
    public static final String PARAM_ACTION_SAVE = "Save";
    public static final String PARAM_ACTION_SEARCH = "Search";
    public static final String PARAM_ACTION_EDIT = "Edit";
    public static final String PARAM_ACTION_SAVE_NEW = "Save New";
    public static final String PARAM_ACTION_NEW = "New";
    public static final String PARAM_ACTION_DELETE = "Delete";
    public static final String PARAM_ACTION_UNDELETE = "Undelete";
    public static final String PARAM_AJAX_SQL = "ajax_sql";
    public static final String PARAM_AJAX_SQL_PARAMS = "ajax_params";
    public static final String PARAM_AJAX_SQL_PARAMS_DELIM = "ajax_params_delim";
    public static final String PARAM_AJAX_HTML_FIELD_NAME = "ajax_html_field_name";
    public static final String PARAM_AJAX_HTML_FIELD_ID = "ajax_html_field_id";
    public static final String PARAM_AJAX_HTML_LABEL = "ajax_html_label";
    public static final String PARAM_AJAX_HTML_FIELD_TYPE = "ajax_html_field_type";
    public static final String PARAM_AJAX_SELECT_LIST_SELECTED_VALUE = "ajax_selected_value";
    public static final String PARAM_AJAX_SQL_UNDEFINED = "ajax_undefined"; // true/false
    public static final String PARAM_AJAX_STYLE = "ajax_style";
    public static final String PARAM_AJAX_ONCHANGE = "ajax_onchange";
    public static final String GLOBAL_LIST_BUSINESS_UNITS = "_businessUnitsList";
    public static final String GLOBAL_LIST_ADMIN_CMD_TYPES = "_adminCmdTypeList";
    public static final String GLOBAL_LIST_COMM_METHODS = "_commMethodsList";
    public static final String GLOBAL_LIST_CURRENCIES = "_currenciesList";
    public static final String GLOBAL_LIST_COUNTRIES = "_countryList";
    public static final String GLOBAL_LIST_CONSUMER_ACCT_TYPES = "_consumerAcctTypeList";
    public static final String GLOBAL_LIST_CONSUMER_ACCT_SUB_TYPES = "_consumerAcctSubtypeList";
    public static final String GLOBAL_LIST_STATES = "_stateList";
    public static final String GLOBAL_LIST_TIME_ZONES = "_timeZoneList";
    public static final String GLOBAL_LIST_LOCATION_TYPES = "_locationTypesList";
    public static final String GLOBAL_LIST_LOCATIONS = "_locationList";
    public static final String GLOBAL_LIST_FILE_TRANSFER_TYPES = "_fileTransferTypesList";
    public static final String GLOBAL_LIST_FILE_CONSUMER_MERCHANTS = "_consumerMerchantsList";
    public static final String GLOBAL_LIST_FILE_CUSTOMERS = "_customersList";
    public static final String GLOBAL_LIST_CARD_TYPES = "_cardTypesList";
    public static final String GLOBAL_LIST_DEVICE_TYPES = "_deviceTypesList";
    public static final String GLOBAL_LIST_DEVICE_TYPES_FIRMWARE = "_deviceTypesFirmwareList";
    public static final String GLOBAL_LIST_CONTEXT_PARAM = "dms_values_list";
    public static final String GLOBAL_LIST_PAY_CYCLES = "_payCyclesList";
    public static final String GLOBAL_EFT_STATUS = "_eftStatusList";
    public static final String GLOBAL_VL_NAMES = "dms_vl_list";
    public static final String GLOBAL_GX_CONFIGMAP_FIELD_NAMES = "dms_gx_configmap_field_names_map";
    public static final String GLOBAL_MEI_CONFIGMAP_FIELD_NAMES = "dms_mei_configmap_field_names_map";
    public static final String GLOBAL_LIST_CONSUMER_TYPES = "_consumerTypesList";
    public static final String GLOBAL_LIST_ESUDS_ALPHA = "_esudsAlphaCharsList";
    public static final String GLOBAL_LIST_DEFAULT_TMPL_DATA_MODES = "_defaultTemplateDataModes";
    public static final String GLOBAL_LIST_DEFAULT_TMPL_REGEX = "_defaultTemplateRegex";
    public static final String GLOBAL_LIST_DEFAULT_TMPL_CATEGORIES = "_defaultTemplateCategories";
    public static final String GLOBAL_LIST_DEFAULT_TMPL_ALIGN = "_defaultTemplateAlign";
    public static final String GLOBAL_LIST_DEFAULT_TMPL_PAD_CHAR = "_defaultTemplatePadChar";
    public static final String GLOBAL_LIST_EDGE_GENERIC_RESPONSE_S2C_ACTIONS = "_edgeGenericResponseS2CActions";
    public static final String LIST_VALUE_UNDEFINED = "Undefined";
    		
	public static final int MAX_EDITABLE_FILE_SIZE = 1024 * 1024;
	public static final int FILE_PREVIEW_SIZE = 100 * 1024;
}