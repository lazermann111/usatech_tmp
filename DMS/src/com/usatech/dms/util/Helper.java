/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspWriter;

import com.usatech.dms.action.FileActions;
import com.usatech.dms.user.AuthenticationStep;
import com.usatech.layers.common.device.DeviceTypeRegex;
import com.usatech.layers.common.util.DeviceUtils;
import com.usatech.layers.common.util.StringHelper;

import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.mail.Email;
import simple.mail.Emailer;
import simple.results.BeanException;
import simple.results.Results;
import simple.servlet.InputFile;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.text.StringUtils;
import simple.text.ThreadSafeDateFormat;
import simple.util.NameValuePair;

public class Helper {
	private static final simple.io.Log log = simple.io.Log.getLog();
	public static final String FIELD_DELIMITER = "\n\n\n";
	public static String DMS_EMAIL = "dms@usatech.com";
	public static String DMS_EMAIL_FROM_NAME = "DMS";

	public static final String DMS_EMAIL_CONTENT_TYPE_HTML = "text/html";
	public static final String DMS_EMAIL_CONTENT_TYPE_PLAIN = "text/plain";
	private static final String MAIL_SMTP_HOST = "mail.smtp.host";
	private static String mailHost = "mailhost.usatech.com";
	
	public static final DateFormat dateFormat = new ThreadSafeDateFormat(new SimpleDateFormat("MM/dd/yyyy"));
	public static final DateFormat timeFormat = new ThreadSafeDateFormat(new SimpleDateFormat("HH:mm:ss"));
	public static final DateFormat firstOfMonthFormat = new ThreadSafeDateFormat(new SimpleDateFormat("MM/01/yyyy"));

	public static String getDMS_EMAIL_FROM_NAME() {
		return DMS_EMAIL_FROM_NAME;
	}

	public static void setDMS_EMAIL_FROM_NAME(String dMS_EMAIL_FROM_NAME) {
		DMS_EMAIL_FROM_NAME = dMS_EMAIL_FROM_NAME;
	}
	
	public static String getMailHost() {
		return mailHost;
	}

	public static void setMailHost(String mailHost) {
		Helper.mailHost = mailHost;
	}

	public static List<NameValuePair> searchOptions;

	static {
		searchOptions = new ArrayList<NameValuePair>();
		searchOptions.add(new NameValuePair(new String("Contains"), new String("C")));
		searchOptions.add(new NameValuePair(new String("Begins"), new String("B")));
		searchOptions.add(new NameValuePair(new String("Ends"), new String("E")));
	}

	/** Don't let anyone instantiate this class. */
	private Helper() {
	}

	/**
	 * Converts the formatted search parameter to the proper sql condition.<br/>
	 * e.g.: <br/>
	 * "^TD" will be converted to "TD%" - all starts with "TD" <br/>
	 * "TD$" will be converted to "%TD" - all ends with "TD" <br/>
	 * "TD" will be converted to "%TD%" - all contains "TD"
	 * 
	 * @param param
	 * @param toUpper
	 * @return
	 */
	public static String convertParam(String param, boolean toUpper) {		
		if(toUpper)
			param = param.toUpperCase().trim();
		else
			param = param.trim();
		
		if (param.startsWith("^") && param.endsWith("$"))
			return param.replaceFirst("^\\^", "").replaceFirst("\\$$", "");
		
		StringBuilder result = new StringBuilder();		
		if (param.startsWith("^")) {
			result.append(param.replaceFirst("^\\^", ""));
			result.append("%");
		} else if (param.endsWith("$")) {
			result.append("%");
			result.append(param.replaceFirst("\\$$", ""));			
		} else {
			result.append("%");
			result.append(param);
			result.append("%");
		}
		return result.toString();
	}

	public static String convertParam(String param) {
		return convertParam(param, true);
	}

	/**
	 * <p>
	 * Translates a string into application/x-www-form-urlencoded format using a
	 * specific encoding scheme. This method uses the supplied encoding scheme
	 * to obtain the bytes for unsafe characters.
	 * </p>
	 * <p>
	 * This is mostly used to construct a link with parameters.
	 * </p>
	 * e.g.: we need to construct a link with the parameter "ev_number" whose
	 * value is "^TD%24$"<br/>
	 * this value have to be encoded, and the result will be: "%5ETD%2524%25",
	 * so the final link string will be:<br/>
	 * <br/>
	 * http://localhost:8080/devicelist.i?ev_number=%5ETD%2524%25
	 * 
	 * @param paramValue
	 * @return
	 */
	public static String encodeParamValue(String paramValue) {
		String result = paramValue;
		if(!StringHelper.isBlank(paramValue)) {
			try {
				result = URLEncoder.encode(paramValue, "UTF-8");
			} catch(UnsupportedEncodingException e) {
				log.warn(">> Failed to encode the parameter value [" + paramValue + "].", e);
			}
		}
		return result;
	}
	
	public static void sendFileToClient(HttpServletResponse response, String fileName, String fileContent) throws IOException {
		response.setContentType("application/force-download");
		response.setContentLength(fileContent.length());
		response.setHeader("Content-Transfer-Encoding", "binary"); 
		response.setHeader("Content-Disposition","attachment;filename=\"" + fileName + "\"");
        PrintWriter out = response.getWriter();
        out.print(fileContent);
        out.flush();
        out.close();
	}
	
	public static void streamFileToClient(HttpServletResponse response, String fileName, long fileId) throws IOException, ServletException {
		response.setContentType("application/force-download");
		response.setHeader("Content-Transfer-Encoding", "binary"); 
		response.setHeader("Content-Disposition","attachment;filename=\"" + fileName + "\"");
		ServletOutputStream out = response.getOutputStream();
		FileActions.streamFileTransfer(out, fileId, null);
		out.close();
	}

	/**
	 * Get the minimum date
	 * 
	 * @param param
	 * @return
	 */
	public static Date getMinDate() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, 100);
		calendar.set(Calendar.MONTH, 0);
		calendar.set(Calendar.DATE, 1);
		return calendar.getTime();
	}

	/**
	 * Get the maximum date
	 * 
	 * @param param
	 * @return
	 */
	public static Date getMaxDate() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, 9999);
		calendar.set(Calendar.MONTH, 11);
		calendar.set(Calendar.DATE, 31);
		return calendar.getTime();
	}

	public static String getDate(Date date) {
		if (date == null)
			return "";
		return dateFormat.format(date);
	}

	public static String getTime(Date date) {
		if (date == null)
			return "";
		return timeFormat.format(date);
	}

	public static String getDateTime(Date date) {
		if (date == null)
			return "";
		return getDate(date) + " " + getTime(date);
	}
	
	public static boolean validateEdgeSchedule (String key, String value, Map<String, String> errorMap ) {
		return DeviceUtils.validateEdgeSchedule(key, value, errorMap);
	}

	/**
	 * Pass in InputForm, requestParamName, queryName, if u need firstListValue,
	 * sqlParams, and sets a list of NVPs on the form. Uses the passed in
	 * firstListValue if firstListValue is not null and firstListValue == true.
	 * 
	 * @param form
	 * @param requestParamName
	 * @param queryName
	 * @param addUndefined
	 * @param sqlParams
	 * @param firstListValue
	 * @throws SQLException
	 * @throws DataLayerException
	 */
	public static void getSelectList(InputForm form, String requestParamName, String queryName, boolean addfirstListValue, Object[] sqlParams, String firstListValue, String resultLabelName,
			String resultValueName) throws SQLException, DataLayerException {
		@SuppressWarnings("unchecked")
		List<NameValuePair> selectList = (List<NameValuePair>) form.getAttribute(requestParamName);
		Results result = null;
		if(selectList == null || selectList.size() == 0) {
			selectList = new ArrayList<NameValuePair>();
			if(addfirstListValue == true && firstListValue == null) {
				selectList.add(new NameValuePair(DMSConstants.LIST_VALUE_UNDEFINED, DMSConstants.LIST_VALUE_UNDEFINED));
			}
			if(addfirstListValue == true && firstListValue != null) {
				selectList.add(new NameValuePair(firstListValue, firstListValue));
			}
			if(sqlParams == null)
				result = DataLayerMgr.executeQuery(queryName, null, false);
			else
				result = DataLayerMgr.executeQuery(queryName, sqlParams, false);
			while(result.next()) {
				if(StringHelper.isBlank(resultLabelName) && StringHelper.isBlank(resultValueName)) {
					selectList.add(new NameValuePair(result.getFormattedValue("aLabel"), result.getFormattedValue("aValue")));
				} else {
					selectList.add(new NameValuePair(result.getFormattedValue(resultLabelName), result.getFormattedValue(resultValueName)));
				}
			}
			form.set(requestParamName, selectList);
		}

	}

	/**
	 * Same as getSelectList, but returns the list of NVPs.
	 * 
	 * @param queryName
	 * @param addUndefined
	 * @param sqlParams
	 * @return
	 * @throws SQLException
	 * @throws DataLayerException
	 */
	public static List<NameValuePair> buildSelectList(String queryName, boolean addUndefined, Object[] sqlParams) throws SQLException, DataLayerException {
		List<NameValuePair> selectList = new ArrayList<NameValuePair>();
		Results result = null;
		if(addUndefined)
			selectList.add(new NameValuePair(DMSConstants.LIST_VALUE_UNDEFINED, DMSConstants.LIST_VALUE_UNDEFINED));
		if(sqlParams == null)
			result = DataLayerMgr.executeQuery(queryName, null, false);
		else
			result = DataLayerMgr.executeQuery(queryName, sqlParams, false);
		while(result.next()) {
			selectList.add(new NameValuePair(result.getFormattedValue("aLabel"), result.getFormattedValue("aValue")));
		}
		return selectList;

	}

	/**
	 * 
	 * @param queryName
	 * @param addUndefined
	 * @param sqlParams
	 * @return
	 * @throws SQLException
	 * @throws DataLayerException
	 */
	public static Object getValueFromDB(String queryName, Object[] sqlParams) throws SQLException, DataLayerException {

		Results result = null;
		Object returnVal = null;
		result = DataLayerMgr.executeQuery(queryName, sqlParams, false);
		while(result.next()) {
			returnVal = result.getValue(1);
		}

		return returnVal;

	}

	/**
	 * Pass in wildCard character and param to create proper conditional Search
	 * param string for a query.
	 * 
	 * @param wildCardChar
	 * @param sqlParam
	 * @return
	 */
	public static String buildSqlWildcardConditionString(String wildCardChar, Object sqlParam) {
		
		if(!StringHelper.isBlank(wildCardChar)) {
			StringBuilder returnStr = new StringBuilder();
			// "C" for contains
			if(wildCardChar.equalsIgnoreCase("C"))
				returnStr.append("%").append(sqlParam.toString()).append("%");
			// "B" for begins with
			else if(wildCardChar.equalsIgnoreCase("B"))
				returnStr.append(sqlParam.toString()).append("%");
			// "E" for ends with
			else if(wildCardChar.equalsIgnoreCase("E"))
				returnStr.append("%").append(sqlParam.toString());
			else
				returnStr.append(sqlParam.toString());
			return returnStr.toString();
		} else
			return sqlParam.toString();
	}

	/**
	 * Returns a displayable list of request params.
	 * 
	 * @param wildCardChar
	 * @param sqlParam
	 * @return
	 */
	public static String getRequestParams(InputForm form) {
		StringBuilder sb = new StringBuilder();
		for(Map.Entry<String, Object> param : form.getParameters().entrySet()) {
			sb.append(StringUtils.prepareHTML(param.getKey())).append('=').append(StringUtils.prepareHTML(ConvertUtils.getStringSafely(param.getValue()))).append("<br/>");
		}
		return sb.toString();
	}

	/**
	 * Pass in a fileContent string containing line separators and return a
	 * count of the lines.
	 * 
	 * @param fileContent
	 * @return
	 */
	public static int getFileContentLineCount(String fileContent) {
		StringReader sr = new StringReader(fileContent);
		BufferedReader br = new BufferedReader(sr);
		int lineCntr = 0;
		try {
			while(br.readLine() != null) {
				lineCntr++;
			}
		} catch(Exception e) {
			log.info("getFileContentLineCount::Unable to count lines in file content! " + fileContent);
		} finally {
			if(br != null) {
				try {
					br.close();
					br = null;
				} catch(IOException e) {
				}
			}
		}
		return lineCntr;
	}

	/**
	 * Pass in a fileContent string containing line separators, and a potential
	 * line separator, and return a count of the lines.
	 * 
	 * @param fileContent
	 * @param lb
	 * @return
	 */
	public static int getLineCountByLineBreak(String fileContent, String lb) {
		String[] lines = fileContent.split(lb, -1);
		return lines.length;

	}

	/**
	 * Pass in a fileContent string containing line separators, and check to see
	 * the pattern "\r\n" (windows) exists. If so, set type to windows, else,
	 * set it to unix.
	 * 
	 * @param fileContent
	 * @param lb
	 * @return
	 */
	public static String getLineBreakType(String fileContent, boolean checkLineCount) {
		if(!StringHelper.isBlank(fileContent)) {
			if(checkLineCount) {
				int lineCnt = Helper.getFileContentLineCount(fileContent);
				int winLineCnt = Helper.getLineCountByLineBreak(fileContent, "\r\n");
				int unixLineCnt = Helper.getLineCountByLineBreak(fileContent, "\n");

				if(unixLineCnt == winLineCnt && lineCnt <= winLineCnt) {
					return "Windows Line Breaks";
				} else {
					return "Unix Line Breaks";
				}

			} else {
				Pattern p = Pattern.compile("\r\n");
				Matcher m = p.matcher(fileContent);
				boolean result = m.find();
				if(result) {
					return "Windows Line Breaks";
				} else {
					return "Unix Line Breaks";
				}
			}
		} else {
			return "";
		}
	}

	/**
	 * Get the test and url values out of the menu item.
	 * 
	 * @param menuItem
	 * @return
	 */
	public static String[] getMenuTextUrlPair(String menuItem) {
		String[] menuItemData = menuItem.split(";", 2);
		String menuTextData = menuItemData[0];
		String[] textData = menuTextData.split("=");
		String text = textData[1];
		String url = null;
		try {
			String menuUrlData = menuItemData[1];
			menuUrlData = menuUrlData.replace("url=", "");
			if(menuUrlData.endsWith(";")) {
				int lastSemi = menuUrlData.lastIndexOf(";");
				menuUrlData = menuUrlData.substring(0, lastSemi);
			}

			url = menuUrlData;
		} catch(ArrayIndexOutOfBoundsException e) {
			url = "";
		}
		return new String[] { text, url };
	}

	/**
	 * Send off email.
	 * 
	 * @param from
	 * @param to
	 * @param subject
	 * @param body
	 * @throws MessagingException
	 */
	public static void sendEmail(String from, String to, String subject, String body, String contentType) throws MessagingException {
		body = body.replace("\\n", "\n");
		Properties properties = new Properties();
		properties.put(MAIL_SMTP_HOST, mailHost);
		Emailer emailer = new Emailer(properties);
		Email email = emailer.createEmail();
		email.setFrom(from);
		email.setTo(to);
		email.setSubject(subject);
		if(!StringHelper.isBlank(contentType) && DMS_EMAIL_CONTENT_TYPE_HTML.equalsIgnoreCase(contentType))
			email.setBodyHtml(body);
		else
			email.setBody(body);

		email.send();
	}

	/**
	 * Convenience method to validate regex on incoming external transfer
	 * fields.
	 * 
	 * @param errorMap
	 * @param value
	 * @param name
	 * @param regexPattern
	 * @return
	 */
	public static boolean validateRegex(Map<String, String> errorMap, String value, String name, String regexPattern) {
		Pattern p = Pattern.compile(regexPattern);
		// Create a matcher with an input string
		Matcher m = p.matcher(value);
		boolean result = false;
		result = m.find();
		if(!result) {
			if(!(errorMap.containsKey(name)))
				errorMap.put(name, "Field failed validation: " + name);
			return false;
		}
		return true;
	}

	/**
	 * Convenience method to validate regex on incoming external transfer
	 * fields.
	 * 
	 * @param errorMap
	 * @param value
	 * @param name
	 * @param regexPattern
	 * @return
	 */
	public static boolean validateRegexWithMessage(Map<String, String> errorMap, String value, String name, String regexPattern, String errorMessage) {
		return StringHelper.validateRegexWithMessage(errorMap, value, name, regexPattern, errorMessage);
	}

	/**
	 * Convenience method to validate length requirements on incoming external
	 * transfer fields.
	 * 
	 * @param errorMap
	 * @param desc
	 * @param value
	 * @param name
	 * @param min
	 * @param max
	 * @return
	 */
	public static boolean validateLength(Map<String, String> errorMap, String desc, String value, String name, int min, int max) {
		if(!(StringHelper.isBlank(value)) && !(value.length() > min && value.length() <= max)) {
			if(!(errorMap.containsKey(name)))
				errorMap.put(name, "Field failed " + desc + " length validation: " + name);
			return false;
		}
		return true;
	}

	/**
	 * Convenience method to validate that a value exists for an incoming
	 * external transfer field.
	 * 
	 * @param errorMap
	 * @param value
	 * @param name
	 * @return
	 */
	public static boolean validateExists(Map<String, String> errorMap, String value, String name) {
		if(StringHelper.isBlank(value)) {
			if(!(errorMap.containsKey(name)))
				errorMap.put(name, "Required field not found: " + name);
			return false;
		}
		return true;
	}

	/**
	 * For the dbObjType (TABLE, COLUMN), the owner, the table name and name of
	 * the db obj, find out if the db obj exists.
	 * 
	 * @param dbObjType
	 * @param ownerName
	 * @param tableName
	 * @param dbObjName
	 * @return
	 */
	public static boolean checkDBObjExists(String dbObjType, String ownerName, String tableName, String dbObjName) {
		Results ddlCount = null;
		BigDecimal exists = new BigDecimal(-1);
		try {
			if(!StringHelper.isBlank(dbObjType) && "TABLE".equalsIgnoreCase(dbObjType))
				ddlCount = (Results) DataLayerMgr.executeQuery("GET_TABLE_NAME_EXISTS", new Object[] { ownerName, tableName }, null);
			if(!StringHelper.isBlank(dbObjType) && "COLUMN".equalsIgnoreCase(dbObjType) && !StringHelper.isBlank(tableName) && !StringHelper.isBlank(dbObjName))
				ddlCount = (Results) DataLayerMgr.executeQuery("GET_COLUMN_NAME_EXISTS", new Object[] { ownerName, tableName, dbObjName }, null);
			if(ddlCount.next()) {
				exists = (BigDecimal) ddlCount.get("count");
			}
		} catch(Exception e) {
			log.info("Exception occurred in Helper : checkDBObjExists :: " + e.getClass().getName() + " :: " + e.getMessage(), e);
		}
		if(exists != null && exists.intValue() < 1) {
			return false;
		}
		return true;
	}

	public static boolean isNumeric(String str)   
	{   
		return StringHelper.isNumeric(str);
	}
	
	public static boolean isInteger(String str)   
	{   
		return StringHelper.isInteger(str);
	}
	
	public static boolean isLong(String str)   
	{   
		return StringHelper.isLong(str);
	}
	
	public static String getCurrentDate() {        
        return dateFormat.format(Calendar.getInstance().getTime());
	}
	
	public static String getCurrentTime() {        
        return timeFormat.format(Calendar.getInstance().getTime());
	}
	
	public static String getFirstOfMonth() {
		return firstOfMonthFormat.format(Calendar.getInstance().getTime());
	}
	
	public static String getDefaultStartDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR_OF_DAY, -4);
        return dateFormat.format(calendar.getTime());
	}
	
	public static String getDefaultStartTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR_OF_DAY, -4);
        return timeFormat.format(calendar.getTime());
	}
	
	public static String getDate(int currentTimeHourDiff) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR_OF_DAY, currentTimeHourDiff);
        return dateFormat.format(calendar.getTime());
	}	
	
	public static String getServerTimeZone() {
		return Calendar.getInstance().getTimeZone().getDisplayName();
	}
	
	public static String getDefaultEndDate() {
		return getCurrentDate();
	}
	
	public static String getDefaultEndTime() {
		return getCurrentTime();
	}
	
	public static String getDisplayedConfigValue(final String dataMode, final String editorType, final String configValue) {
		if ("H".equalsIgnoreCase(dataMode) && editorType != null && !editorType.startsWith("TEXT") && !StringHelper.isBlank(configValue)) {		    				
			String decodedUserValue = StringHelper.decodeHexString(configValue);
			if (StringHelper.PRINTABLE_ASCII_EXPRESSION.matcher(decodedUserValue).matches())
				return decodedUserValue;
			else
				return new StringBuilder("Hex: ").append(configValue).toString();
		}
		return configValue;
	}
	
	public static String generateHiddenInputs(HttpServletRequest request) {
		StringBuilder sb = new StringBuilder();
		for(Enumeration<?> en = request.getParameterNames(); en.hasMoreElements(); ) {
	        String s = (String)en.nextElement();
	        if(AuthenticationStep.PARAM_USERNAME.equalsIgnoreCase(s) 
	        		|| AuthenticationStep.PARAM_CREDENTIAL.equalsIgnoreCase(s)
	        		|| AuthenticationStep.ATTRIBUTE_FORWARD.equalsIgnoreCase(s))
	        	continue;
	        String[] v = request.getParameterValues(s);
	        for(int i = 0; i < v.length; i++)
	        	sb.append("<input type=\"hidden\" name=\"").append(StringUtils.prepareCDATA(s))
	        		.append("\" value=\"").append(StringUtils.prepareCDATA(v[i])).append("\" />\n");
	    }
		return sb.toString();
	}
	
	public static String generateHiddenInputs(InputForm form) {
		StringBuilder sb = new StringBuilder();
		for(Map.Entry<String,Object> param : form.getParameters().entrySet()) {
			String paramKey = param.getKey();
			if(AuthenticationStep.PARAM_USERNAME.equalsIgnoreCase(paramKey) 
					|| AuthenticationStep.PARAM_CREDENTIAL.equalsIgnoreCase(paramKey)
					|| AuthenticationStep.ATTRIBUTE_FORWARD.equalsIgnoreCase(paramKey))
				continue;
	        if(param.getValue() instanceof String)
				sb.append("<input type=\"hidden\" name=\"").append(StringUtils.prepareCDATA(paramKey))
					.append("\" value=\"").append(StringUtils.prepareCDATA((String)param.getValue())).append("\" />\n");
			else if(param.getValue() instanceof String[]) {
				String[] values = (String[])param.getValue();
				for(int i = 0; i < values.length; i++)
					sb.append("<input type=\"hidden\" name=\"").append(StringUtils.prepareCDATA(paramKey))
						.append("\" value=\"").append(StringUtils.prepareCDATA(values[i])).append("\" />\n");
			} else if(param.getValue() instanceof InputFile || param.getValue() instanceof InputFile[])
				RequestUtils.storeForNextRequest(((HttpServletRequest)form.getRequest()).getSession(), paramKey, param.getValue());
			else if(param.getValue() != null) {
				String value = ConvertUtils.getStringSafely(param.getValue());
		        sb.append("<input type=\"hidden\" name=\"").append(StringUtils.prepareCDATA(paramKey))
		        	.append("\" value=\"").append(StringUtils.prepareCDATA(value)).append("\" />\n");
			}
		}
		return sb.toString();
	}	
	
	public static String getBasicStyle() {
		return new StringBuilder()
			.append("<style>")
			.append("<!--")
			.append("body{margin: 0; padding: 0; font-family:Verdana, Arial, Helvetica, Tahoma, Sans-Serif; font-size:12px; color:#333333;} ")
			.append("pre{background: #EEEEEE; font-family: Courier New, Verdana, Arial, Helvetica, Tahoma, Sans-Serif; font-size: 11px; border: 1px solid #a9a9a9; margin: 5px; padding: 5px;} ")
			.append("th{background:#d6d6d6; font-weight:bold; text-align:center; padding:3px 5px 3px 5px; border:1px solid #a9a9a9; margin:10px 0px 2px 0px;} ")
			.append(".gridHeader {background:#d6d6d6; font-weight:bold; text-align:center; padding:3px 5px 3px 5px; border:1px solid #a9a9a9; margin:5px 0px 2px 0px;} ")
			.append(".row0 {background-color:#EEEEEE;} ")
			.append(".tabNormal{padding:0px; border:0px; border-collapse:collapse;} ")
			.append(".tabNormal td{padding:3px 5px 3px 5px; border:1px solid #a9a9a9 !important; font-size:12px;} ")			
			.append("-->")
			.append("</style>")
			.toString();
	}
	
	public static void valueListOptions(HttpServletRequest request, JspWriter out, String listName, String selectedValue) throws IOException, ServletException, SQLException, DataLayerException {
		List<NameValuePair> list = ((DMSValuesList)request.getSession().getServletContext().getAttribute(DMSConstants.GLOBAL_LIST_CONTEXT_PARAM + listName)).getList();
    	for (NameValuePair nvp: list) {
    		out.print("<option value=\"");
    		out.print(nvp.getValue());    		
    		out.print("\"");
    		if (!StringUtils.isBlank(selectedValue) && selectedValue.equals(nvp.getValue()))
    			out.print(" selected=\"selected\"");
    		out.print(">");
    		out.print(nvp.getName());
    		out.println("</option>");
    	}
	}	

	public static String getDMS_EMAIL() {
		return DMS_EMAIL;
	}

	public static void setDMS_EMAIL(String dMS_EMAIL) {
		DMS_EMAIL = dMS_EMAIL;
	}
	
	public static LinkedList<DeviceTypeRegex> getDeviceTypeRegexList() throws SQLException, DataLayerException, BeanException {
		LinkedList<DeviceTypeRegex> list = new LinkedList<DeviceTypeRegex>(); 
		Results results = DataLayerMgr.executeQuery("GET_DEVICE_TYPE_REGEX", null);
		while (results.next()) {
			DeviceTypeRegex deviceTypeRegex = new DeviceTypeRegex();
			results.fillBean(deviceTypeRegex);
			list.add(deviceTypeRegex);
		}
		return list;
	}
	
	public static byte getDeviceTypeBySerialNumber(LinkedList<DeviceTypeRegex> deviceTypeRegexList, String serialNumber) {
		if (deviceTypeRegexList == null || serialNumber == null)
			return -1;
		for (DeviceTypeRegex deviceTypeRegex : deviceTypeRegexList) {
			if (deviceTypeRegex.getDeviceTypeSerialCdPattern().matcher(serialNumber).matches())
				return deviceTypeRegex.getDeviceTypeId();
		}
		return -1;
	}

}
