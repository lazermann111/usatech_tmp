/**
 * $Id$
 *
 * Copyright 2009 USATechnologies. All rights reserved.
 *
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL.Use is subject to license terms.
 */

package com.usatech.dms.util;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;

import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.BeanException;
import simple.results.Results;
import simple.util.NameValuePair;

public class CardTypeList extends DMSValuesList{

	/**
	 * 
	 */
	
	private static final long serialVersionUID = 8708023767024499216L;
	
	public CardTypeList(){}
	
	public CardTypeList(String inQueryName){
		
		super(inQueryName);
	}
		
	protected synchronized void load() throws ServletException, SQLException, DataLayerException{ 
		if(list == null){
			list = new ArrayList();
			
				Results result = null;
				result = DataLayerMgr.executeQuery(queryName,null, false);
				while (result.next())
	            {
					NameValuePair nvp = null;
					com.usatech.dms.model.CardType cardType = new com.usatech.dms.model.CardType();
					//Map cardTypeMap = new HashMap();
					try{
						result.fillBean(cardType);
					}catch(BeanException e){
						throw new ServletException("ValuesList 'LocationList' BeanException occurred: " + e);
						
					}
					//cardTypeMap.put(cardType.getId(), cardType);
					list.add(new NameValuePair(cardType.getFmtName(), cardType.getId().toString()));
					
	            }
			
		}
	} 
	
	/**
	 * Convenience method checks to see if a CardType exists for a passed in cardTypeId, and
	 * if so, returns it.
	 * @param deviceId
	 */
	public com.usatech.dms.model.CardType getCardTypeByCardTypeId(Long cardTypeId)throws ServletException, SQLException, DataLayerException, BeanException{
		com.usatech.dms.model.CardType retCardType = null;
				
		Results result = null;
		result = DataLayerMgr.executeQuery("GET_CONSUMER_CARD_TYPE_BY_ID",new Object[]{cardTypeId}, false);
		while(result.next()){
			retCardType = new com.usatech.dms.model.CardType();
			result.fillBean(retCardType);
			
		}
	
		return retCardType;
		
	}
}
