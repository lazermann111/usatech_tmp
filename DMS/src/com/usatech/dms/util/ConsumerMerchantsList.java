package com.usatech.dms.util;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;

import simple.db.DataLayerException;

public class ConsumerMerchantsList extends DMSValuesList{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4615233772646010801L;

	/**
	 * 
	 */	
	public ConsumerMerchantsList(){}
	
	public ConsumerMerchantsList(String inQueryName){
		
		super(inQueryName);
	}
	
}
