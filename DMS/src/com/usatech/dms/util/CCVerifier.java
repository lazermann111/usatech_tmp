/**
 * Checks for valid credit card number using Luhn algorithm.
 * @author unknown
 * see http://www.merriampark.com/anatomycc.htm for more info
 *
 */
package com.usatech.dms.util;

import java.math.BigInteger;

public abstract class CCVerifier {

	/**
	 * Filter out non-digit characters
	 * @param s
	 * @return
	 */
	private static String getDigitsOnly (String s) {
	 StringBuilder digitsOnly = new StringBuilder ();
	 char c;
	 for (int i = 0; i < s.length (); i++) {
	   c = s.charAt (i);
	   if (Character.isDigit (c)) {
	     digitsOnly.append (c);
	   }
	 }
	 return digitsOnly.toString ();
	}

	/**
	 * Perform Luhn check
	 * @param cardNumber
	 * @return
	 */
	public static boolean isValid (String cardNumber) {
	 int modulus = getModulus(cardNumber);
	 return modulus == 0;
	
	}
	
	/**
	 * Returns modulus.
	 * @param cardNumber
	 * @return
	 */
	private static int getModulus(String cardNumber) {
		String digitsOnly = getDigitsOnly (cardNumber);
		 int sum = 0;
		 int digit = 0;
		 int addend = 0;
		 boolean timesTwo = true;
		
		 for (int i = digitsOnly.length () - 1; i >= 0; i--) {
		   digit = Integer.parseInt (digitsOnly.substring (i, i + 1));
		   if (timesTwo) {
		     addend = digit * 2;
		     if (addend > 9) {
		       addend -= 9;
		     }
		   }
		   else {
		     addend = digit;
		   }
		   sum += addend;
		   timesTwo = !timesTwo;
		 }
		
		 int modulus = sum % 10;
		return modulus;
	}

	/**
	 * Returns the check digit.
	 * @param cardNumber
	 * @return
	 */
	public static int getCheckDigit (String cardNumber) {
		return getModulus(cardNumber);
		
	}
	/**
	 * Checks validity of CCNum.
	 * @param ccNum
	 * @return
	 */
	public static boolean isValid2(String ccNum) {
	 
	    char number[] = ccNum.toCharArray();
	 
	    int len = number.length;
	    int sum = 0;
	    for (int i = 0; i < len - 1; i++) {
	      int num = mapChar(number[i]);
	 
	      // Double all the odd digits
	      if (i % 2 != 0)
	        num *= 2;
	 
	      // Combine digits.  i.e., 16 = (1 + 6) = 7
	      if (num > 9)
	        num = (num % 10) + (num / 10);
	      sum += num;
	    }
	 
	    int checkDigit = mapChar(number[number.length - 1]);
	 
	    // This is the mathmatical modulus - not the remainder.  i.e., 10 mod 7 = 3
	    int mod = (10 - (sum % 10)) % 10;
	    if (mod == checkDigit) {
	      return true;
	    }
	 
	    return false;
	}
 
	/**
	 * Standard & Poor's maps A..Z to 10..35 
	 * @param c
	 * @return numeric value of the letter
	 */
	private static int mapChar(char c) {
		if (c >= '0' && c <= '9')
			return c - '0';
		return c - 'A' + 10;
	}


	/**
	 * Computes Mod97 check.
	 * @param digits
	 * @return
	 */
	public static int computeMod97Check(String digits) {
		BigInteger l= new BigInteger(digits);
		l = l.multiply(new BigInteger("100"));
		BigInteger mod = l.mod(new BigInteger("97"));
		BigInteger substract = new BigInteger("98");
		BigInteger substractResult =substract.subtract(mod);
		return (substractResult.mod(new BigInteger("97"))).intValue();
	
		//return ((int)(98 - (l * 100) % 97L)) % 97;
		
	}

	/**
	 * Formats a given number by the number of leading zeros passed in.
	 * @param num
	 * @param numLeadingZeros
	 * @return
	 */
	public static String formatNumLeadZeros(int num, int numLeadingZeros){
		String format = String.format("%%0%dd", numLeadingZeros); 
	    return String.format(format, num);
	
	}

}
