package com.usatech.dms.util;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import com.usatech.dms.action.PendingCmdActions;
import com.usatech.layers.common.constants.DeviceType;


public class ValuesListLoader {

	protected static final simple.io.Log log = simple.io.Log.getLog();

	public static void createValidValuesList(ServletContext servletContext) throws Exception {		
		
		Map<String, String> vlMap = new HashMap<String, String>();
		Map<String, String> vlMapSorted = new TreeMap<String, String>(new KeySorter());
		
		addValuesList(servletContext, vlMap, DMSConstants.GLOBAL_LIST_ADMIN_CMD_TYPES, "Admin Command Types", new GenericList("GET_ADMIN_CMD_TYPES", "Admin Command Types"));
		
		addValuesList(servletContext, vlMap, DMSConstants.GLOBAL_LIST_TIME_ZONES, "Time Zone", new TimeZoneList("GET_ALL_TIME_ZONES"));
		// For Country
		addValuesList(servletContext, vlMap, DMSConstants.GLOBAL_LIST_COUNTRIES, "Country", new GenericList("GET_ALL_COUNTRIES", "Country"));
		// For Location Types
		addValuesList(servletContext, vlMap, DMSConstants.GLOBAL_LIST_LOCATION_TYPES, "Location Type", new LocationTypeList("GET_LOCATION_TYPE_DESC_LIST"));
		// For Device Types
		addValuesList(servletContext, vlMap, DMSConstants.GLOBAL_LIST_DEVICE_TYPES, "Device Type", new DeviceTypeList("GET_DEVICE_TYPE_MENU_ITEM"));
		addValuesList(servletContext, vlMap, DMSConstants.GLOBAL_LIST_COMM_METHODS, "Comm Methods", new GenericList("GET_COMM_METHOD_MENU_ITEM", "Comm Methods"));
		addValuesList(servletContext, vlMap, DMSConstants.GLOBAL_LIST_CONSUMER_ACCT_TYPES, "Consumer Account Types", new GenericList("GET_CONSUMER_ACCT_TYPE_MENU_ITEM", "Consumer Account Types"));
		addValuesList(servletContext, vlMap, DMSConstants.GLOBAL_LIST_CONSUMER_ACCT_SUB_TYPES, "Consumer Account Subtypes", new GenericList("GET_CONSUMER_ACCT_SUB_TYPE_MENU_ITEM", "Consumer Account Subtypes"));
		// For Device Type Firmware versions
		addValuesList(servletContext, vlMap, DMSConstants.GLOBAL_LIST_DEVICE_TYPES_FIRMWARE, "Firmware", new DeviceTypeFirmwareList("GET_DEVICE_TYPE_MENU_SUB_ITEM"));
		// For Consumer Types
		addValuesList(servletContext, vlMap, DMSConstants.GLOBAL_LIST_LOCATIONS, "Location", new LocationList("GET_ALL_LOCATIONS_FORMATTED"));
		// For All Card Types
		addValuesList(servletContext, vlMap, DMSConstants.GLOBAL_LIST_CARD_TYPES, "Card Type", new CardTypeList("GET_CONSUMER_CARD_TYPES"));
		//For All File Types
		addValuesList(servletContext, vlMap, DMSConstants.GLOBAL_LIST_FILE_TRANSFER_TYPES, "File Transfer Type", new FileTransferTypeList("GET_FILE_TRANSFER_TYPE_MENU_ITEM"));
		//For All Consumer Merchants
		addValuesList(servletContext, vlMap, DMSConstants.GLOBAL_LIST_FILE_CONSUMER_MERCHANTS, "Consumer Merchant", new ConsumerMerchantsList("GET_ALL_CONSUMER_MERCHANT_FORMATTED"));
		//For Esuds Alpha Chars
		addValuesList(servletContext, vlMap, DMSConstants.GLOBAL_LIST_ESUDS_ALPHA, "ESUDS Alpha Character Location", new EsudsAlphaCharList("GET_LOCATION_ESUDS_MENU_ALPA", servletContext));
		//For Default Tmpl Data Modes
		addValuesList(servletContext, vlMap, DMSConstants.GLOBAL_LIST_DEFAULT_TMPL_DATA_MODES, "Default Tmpl Data Modes", new GenericList("GET_CONFIG_TEMPLATE_DATA_MODE", "DefaultTmplDataModes"));
		//For Default Tmpl Regex
		addValuesList(servletContext, vlMap, DMSConstants.GLOBAL_LIST_DEFAULT_TMPL_REGEX, "Default Tmpl Regex", new GenericList("GET_CONFIG_TEMPLATE_REGEX", "DefaultTemplateRegex"));
		//For Default Tmpl Categories
		addValuesList(servletContext, vlMap, DMSConstants.GLOBAL_LIST_DEFAULT_TMPL_CATEGORIES, "Default Tmpl Categories", new GenericList("GET_CONFIG_TEMPLATE_CATEGORIES", "DefaultTemplateCategories"));
		//For Default Tmpl Align
		addValuesList(servletContext, vlMap, DMSConstants.GLOBAL_LIST_DEFAULT_TMPL_ALIGN, "Default Tmpl Align", new GenericList("GET_CONFIG_TEMPLATE_ALIGN", "DefaultTemplateAlign"));
		//For Default Pad Char
		addValuesList(servletContext, vlMap, DMSConstants.GLOBAL_LIST_DEFAULT_TMPL_PAD_CHAR, "Default Pad Char", new GenericList("GET_CONFIG_TEMPLATE_PAD_CHAR", "DefaultTemplatePadChar"));
		//For All States
		addValuesList(servletContext, vlMap, DMSConstants.GLOBAL_LIST_STATES, "States", new GenericList("GET_ALL_STATES", "States"));
		
		addValuesList(servletContext, vlMap, DMSConstants.GLOBAL_LIST_BUSINESS_UNITS, "Business Units", new GenericList("GET_BUSINESS_UNIT_MENU_ITEM", "BusinessUnits"));
		addValuesList(servletContext, vlMap, DMSConstants.GLOBAL_LIST_CURRENCIES, "Currencies", new GenericList("GET_CURRENCY_MENU_ITEM", "Currencies"));
		addValuesList(servletContext, vlMap, DMSConstants.GLOBAL_LIST_PAY_CYCLES, "Pay Cycles", new GenericList("GET_PAY_CYCLE_MENU_ITEM", "PayCycles"));
		addValuesList(servletContext, vlMap, DMSConstants.GLOBAL_LIST_EDGE_GENERIC_RESPONSE_S2C_ACTIONS, "Edge Generic Response Server to Client Actions", new GenericList("GET_EDGE_GENERIC_RESPONSE_S2C_ACTIONS", "EdgeGenericResponseServerToClientActions"));
		
		addValuesList(servletContext, vlMap, DMSConstants.GLOBAL_EFT_STATUS, "EFT Statuses", new GenericList("GET_EFT_STATUS_MENU_ITEM", "EFTStatuses"));

		vlMapSorted.putAll(vlMap);
		servletContext.setAttribute(DMSConstants.GLOBAL_VL_NAMES, vlMapSorted);
		
		servletContext.setAttribute(DMSConstants.GLOBAL_GX_CONFIGMAP_FIELD_NAMES, null);
		servletContext.setAttribute(DMSConstants.GLOBAL_MEI_CONFIGMAP_FIELD_NAMES, null);
	}
	
	@SuppressWarnings("unchecked")
	public static Map<String, String> getConfigParamMap(ServletContext servletContext, int deviceTypeId) throws ServletException {
		Map<String, String> mapConfigNames = null;
		if (deviceTypeId == DeviceType.GX.getValue() || deviceTypeId == DeviceType.G4.getValue()) {
			mapConfigNames = (Map<String, String>) servletContext.getAttribute(DMSConstants.GLOBAL_GX_CONFIGMAP_FIELD_NAMES);
			if(mapConfigNames == null){
				mapConfigNames = PendingCmdActions.constructConfigParamMap(DeviceType.G4.getValue());
				servletContext.setAttribute(DMSConstants.GLOBAL_GX_CONFIGMAP_FIELD_NAMES, mapConfigNames);
				if (log.isDebugEnabled())
					log.debug("GLOBAL_GX_CONFIGMAP_FIELD_NAMES: load complete");
			}
		} else if (deviceTypeId == DeviceType.MEI.getValue()) {
			mapConfigNames = (Map<String, String>) servletContext.getAttribute(DMSConstants.GLOBAL_MEI_CONFIGMAP_FIELD_NAMES);
			if(mapConfigNames == null){
				mapConfigNames = PendingCmdActions.constructConfigParamMap(DeviceType.MEI.getValue());
				servletContext.setAttribute(DMSConstants.GLOBAL_MEI_CONFIGMAP_FIELD_NAMES, mapConfigNames);
				if (log.isDebugEnabled())
					log.debug("GLOBAL_MEI_CONFIGMAP_FIELD_NAMES: load complete");
			}
		}
		return mapConfigNames;
	}
	
	/**
	 * Add a global values list.
	 * @param servletContext
	 * @param vlMap
	 * @param globalListPostFix - the ending portion of the name to pull from session context.
	 * @param sql
	 * @param mapKey - the name of the list
	 */
	private static void addValuesList(ServletContext servletContext, Map<String, String> vlMap, String globalListPostFix, String mapKey, DMSValuesList valuesList) {
		String fullListName = new StringBuilder(DMSConstants.GLOBAL_LIST_CONTEXT_PARAM).append(globalListPostFix).toString();
		if(servletContext.getAttribute(fullListName) == null)
			servletContext.setAttribute(fullListName, valuesList);
		else
			((DMSValuesList)servletContext.getAttribute(fullListName)).clearList();
		vlMap.put(mapKey, fullListName);
	}
	
	public static DMSValuesList getValuesList(HttpServletRequest request, String listName) {
		return (DMSValuesList) request.getSession().getServletContext().getAttribute(new StringBuilder(DMSConstants.GLOBAL_LIST_CONTEXT_PARAM).append(listName).toString());
	}
	
	public static class KeySorter implements Comparator<Object> {
        public int compare(Object o1, Object o2) {
        	try{
              return new Integer((String) o1).compareTo(new Integer((String) o2));
        	}catch(Exception e){
        		return ((String) o1).compareTo(((String) o2));
        	}
        }
	}
}
