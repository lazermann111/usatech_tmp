package com.usatech.dms.bank;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usatech.layers.common.util.PaginationUtil;
import com.usatech.layers.common.util.StringHelper;

import simple.app.DialectResolver;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

public class ChangedBankAccountStep extends AbstractStep{

    private static final String DEFAULT_SORT_INDEX = "1";
    
	private static final String SQL_START  = " SELECT BANK_NAME,BANK_ACCT_NBR,BANK_ROUTING_NBR,ACCOUNT_TITLE,to_char(create_date,'mm/dd/yyyy hh24:mi:ss') CREATE_DATE,CUSTOMER_BANK_ID,"+
	"STATUS_TEXT,ACCOUNT_TYPE_TEXT,CUSTOMER_NAME, "+
	"BANK_ADDRESS,BANK_CITY,BANK_STATE,BANK_ZIP,CONTACT_NAME,CONTACT_TITLE,CONTACT_TELEPHONE,CONTACT_FAX,CUSTOMER_ID";
	    

    private static final String SQL_BASE = 	" FROM CORP.VW_CHANGED_CUSTOMER_BANK CCB ";
    	
    private static final String[] SORT_FIELDS = {
    	"BANK_NAME","BANK_ACCT_NBR","BANK_ROUTING_NBR","ACCOUNT_TITLE",
    	"CCB.CREATE_DATE", "STATUS_TEXT","ACCOUNT_TYPE_TEXT",
    	"BANK_ADDRESS","BANK_CITY","BANK_STATE","BANK_ZIP","CONTACT_NAME","CONTACT_TITLE","CONTACT_TELEPHONE","CONTACT_FAX",    	
    	"CUSTOMER_NAME", "CUSTOMER_BANK_ID" };
    
	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request,
			HttpServletResponse response) throws ServletException {

		try{
			String[] ids = form.getStringArray("bankid",false);
			
			if(ids.length>0){
				for(int i=0;i<ids.length;i++){
			        Object[] params = new Object[] {ids[i], null };
			        DataLayerMgr.executeCall("ACTIVATE_ACCOUNT", params, true);
				}
			}
			
			String queryBase = SQL_BASE;
	        String paramTotalCount = PaginationUtil.getTotalField(null);
	        String paramPageIndex = PaginationUtil.getIndexField(null);
	        String paramPageSize = PaginationUtil.getSizeField(null);
	        String paramSortIndex = PaginationUtil.getSortField(null);

	        int totalCount = form.getInt(paramTotalCount, false, -1);
	        if (totalCount == -1 || ids.length > 0)
	        {
	            Results total = DataLayerMgr.executeSQL("REPORT", "SELECT COUNT(1) " + queryBase, null, null);
	            if (total.next())
	            {
	                totalCount = total.getValue(1, int.class);
	            }
	            else
	            {
	                totalCount = 0;
	            }
	            request.setAttribute(paramTotalCount, String.valueOf(totalCount));
	        }
	
	        int pageIndex = form.getInt(paramPageIndex, false, 1);
	        int pageSize = form.getInt(paramPageSize, false, PaginationUtil.DEFAULT_PAGE_SIZE);
	        int minRowToFetch = PaginationUtil.getStartNum(pageSize, pageIndex);
	        int maxRowToFetch = PaginationUtil.getEndNum(pageSize, pageIndex);
	
	        String sortIndex = form.getString(paramSortIndex, false);
	        sortIndex = StringHelper.isBlank(sortIndex) ? DEFAULT_SORT_INDEX : sortIndex;
	        String orderBy = PaginationUtil.constructOrderBy(SORT_FIELDS, sortIndex);
	
	        String query;
			if (!DialectResolver.isOracle()) {				
				query = "select * from (" + " select pagination_temp.*, row_number() over() as rnum from (" + SQL_START
						+ queryBase + orderBy + " limit ?::numeric) pagination_temp ) sq_end where rnum  >= ?::numeric";
			} else {
				query = "select * from (" + " select pagination_temp.*, ROWNUM rnum from (" + SQL_START + queryBase
						+ orderBy + ") pagination_temp where ROWNUM <= ?) where rnum  >= ?";
			}
	
	        Results results = DataLayerMgr.executeSQL("REPORT", query, new Object[]{maxRowToFetch,minRowToFetch}, null);
	        request.setAttribute("resultlist", results);
	        
		}catch(Exception e){
    		throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
    	}

	}

}
