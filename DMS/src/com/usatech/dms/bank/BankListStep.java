package com.usatech.dms.bank;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.app.DialectResolver;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;

import com.usatech.dms.util.Helper;
import com.usatech.layers.common.util.PaginationUtil;
import com.usatech.layers.common.util.StringHelper;

public class BankListStep extends AbstractStep{

    private static final String DEFAULT_SORT_INDEX = "1";
    
	private static final String SQL_START  = " SELECT BANK_NAME,BANK_ACCT_NBR,BANK_ROUTING_NBR,ACCOUNT_TITLE,to_char(create_date,'mm/dd/yyyy hh24:mi:ss') CREATE_DATE,CUSTOMER_BANK_ID,"+
	"STATUS_TEXT,ACCOUNT_TYPE_TEXT,CUSTOMER_NAME, "+
	"BANK_ADDRESS,BANK_CITY,BANK_STATE,BANK_ZIP,CONTACT_NAME,CONTACT_TITLE,CONTACT_TELEPHONE,CONTACT_FAX,CUSTOMER_ID,PAY_CYCLE_NAME";
	    

    private static final String SQL_BASE = 	" FROM "
    + " (SELECT CB.BANK_ACCT_NBR, CB.BANK_ROUTING_NBR, CB.ACCOUNT_TITLE, CB.CREATE_DATE, CB.CUSTOMER_BANK_ID, CB.CUSTOMER_ID, CB.STATUS, "
    + " (CASE WHEN CB.STATUS = 'P' THEN 'PENDING' WHEN CB.STATUS = 'U' THEN 'UPDATED' WHEN CB.STATUS = 'A' THEN 'ACTIVE' ELSE CB.STATUS END) STATUS_TEXT,"
    + " (CASE WHEN CB.ACCOUNT_TYPE = 'C' THEN 'Checking' WHEN CB.ACCOUNT_TYPE = 'S' THEN 'Saving' ELSE 'Unknown' END) ACCOUNT_TYPE_TEXT,"
    + " CB.BANK_NAME, CB.BANK_ADDRESS, CB.BANK_CITY, CB.BANK_STATE, CB.BANK_ZIP, CB.CONTACT_NAME, CB.CONTACT_TITLE,"
    + " CB.CONTACT_TELEPHONE, CB.CONTACT_FAX, C.CUSTOMER_NAME, CB.CREATE_BY, PC.PAY_CYCLE_NAME"
    + " FROM corp.CUSTOMER_BANK CB"
    + " inner join corp.CUSTOMER C on CB.CUSTOMER_ID = C.CUSTOMER_ID AND CB.STATUS='A'"
    + " INNER JOIN CORP.PAY_CYCLE PC ON CB.PAY_CYCLE_ID = PC.PAY_CYCLE_ID"
    + ") A";
    	
    private static final String[] SORT_FIELDS = {
    	"BANK_NAME","BANK_ACCT_NBR","BANK_ROUTING_NBR","ACCOUNT_TITLE",
    	"A.CREATE_DATE", "STATUS_TEXT","ACCOUNT_TYPE_TEXT",
    	"BANK_ADDRESS","BANK_CITY","BANK_STATE","BANK_ZIP","CONTACT_NAME","CONTACT_TITLE","CONTACT_TELEPHONE","CONTACT_FAX",    	
    	"CUSTOMER_NAME", "CUSTOMER_BANK_ID", "PAY_CYCLE_NAME" };
    
	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request,
			HttpServletResponse response) throws ServletException {

		try{
			
	        String customer_name = form.getString("customer_name", false);
			
	        String sql = "";
	        if (!StringHelper.isBlank(customer_name)) {
	        	sql = " where lower(customer_name) like lower(?) ";	        	
	        }

	        String queryBase = SQL_BASE + sql;
			
	        String paramTotalCount = PaginationUtil.getTotalField(null);
	        String paramPageIndex = PaginationUtil.getIndexField(null);
	        String paramPageSize = PaginationUtil.getSizeField(null);
	        String paramSortIndex = PaginationUtil.getSortField(null);

	        int totalCount = form.getInt(paramTotalCount, false, -1);
	        if (totalCount == -1)
	        {
	        	Results total=null;
	        	if (!StringHelper.isBlank(customer_name)) {
	        		total = DataLayerMgr.executeSQL("REPORT", "SELECT COUNT(1), MAX(CUSTOMER_BANK_ID), MAX(CUSTOMER_ID) " + queryBase, new Object[]{(Helper.convertParam(customer_name, false))}, null);	
	        	}else{
	        		total = DataLayerMgr.executeSQL("REPORT", "SELECT COUNT(1), MAX(CUSTOMER_BANK_ID), MAX(CUSTOMER_ID) " + queryBase,null, null);
	        	}
	            
	            if (total.next())
	            {
	                totalCount = total.getValue(1, int.class);
	                if (totalCount == 1) {
	                	form.setRedirectUri(new StringBuilder("/account.i?accountId=").append(total.getValue(2, String.class)).append("&customerId=").append(total.getValue(3, String.class)).toString());
	                	return;
	                }
	            }
	            else
	            {
	                totalCount = 0;
	            }
	            request.setAttribute(paramTotalCount, String.valueOf(totalCount));
	        }
	
	        int pageIndex = form.getInt(paramPageIndex, false, 1);
	        int pageSize = form.getInt(paramPageSize, false, PaginationUtil.DEFAULT_PAGE_SIZE);
	        int minRowToFetch = PaginationUtil.getStartNum(pageSize, pageIndex);
	        int maxRowToFetch = PaginationUtil.getEndNum(pageSize, pageIndex);
	
	        String sortIndex = form.getString(paramSortIndex, false);
	        sortIndex = StringHelper.isBlank(sortIndex) ? DEFAULT_SORT_INDEX : sortIndex;
	        String orderBy = PaginationUtil.constructOrderBy(SORT_FIELDS, sortIndex);
	
	        String query;
			if (!DialectResolver.isOracle()) {
				query = "select * from (" + " select pagination_temp.*, row_number() over() as rnum from (" + SQL_START
						+ queryBase + orderBy + " limit ?::numeric) pagination_temp ) sq_end where rnum  >= ?::numeric";
			} else {
				query = "select * from (" + " select pagination_temp.*, ROWNUM rnum from (" + SQL_START + queryBase
						+ orderBy + ") pagination_temp where ROWNUM <= ?) where rnum  >= ?";
			}
	        
	        Object[] params=null;
	        if (StringHelper.isBlank(customer_name)) {
		        params = new Object[]{maxRowToFetch,minRowToFetch};
	        }else{
	        	params = new Object[]{Helper.convertParam(customer_name, false),maxRowToFetch,minRowToFetch};
	        }

	        Results results = DataLayerMgr.executeSQL("REPORT", query, params, null);
	        request.setAttribute("resultlist", results);
	        
		}catch(Exception e){
    		throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
    	}

	}
}
