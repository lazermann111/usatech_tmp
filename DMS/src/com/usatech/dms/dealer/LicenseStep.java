package com.usatech.dms.dealer;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

import com.usatech.layers.common.ProcessingUtils;

public class LicenseStep extends AbstractStep{
	private static final simple.io.Log log = simple.io.Log.getLog();

	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("licenseId", form.getLong("licenseId", true, -1));
		Connection conn = null;
		boolean success = false;
		try{
			conn = DataLayerMgr.getConnection("REPORT");
			String[] deviceSubTypes = form.getStringArray("deviceSubTypes", false); // this is an array of <device_type_id>.<device_sub_type_id> (and no dot when device_sub_type_id is null)
			DataLayerMgr.executeCall(conn, "LICENSE_DEVICE_SUB_TYPE_CLEAR", params);
			if(deviceSubTypes != null)
				for(String deviceSubType : deviceSubTypes) {
					int p = deviceSubType.indexOf('.');
					if(p < 0 || p >= deviceSubType.length() - 1) {
						params.put("deviceTypeId", ConvertUtils.getInt(deviceSubType));
						params.remove("deviceSubTypeId");
					} else {
						params.put("deviceTypeId", ConvertUtils.getInt(deviceSubType.substring(0, p)));
						params.put("deviceSubTypeId", ConvertUtils.getInt(deviceSubType.substring(p + 1)));
					}
					DataLayerMgr.executeCall(conn, "LICENSE_DEVICE_SUB_TYPE_INSERT", params);
				}
			Results results = DataLayerMgr.executeQuery(conn, "GET_LICENSE_PROCESS_FEES", params);
			while (results.next()) {
				int itemId = results.getValue("TRANS_TYPE_ID", int.class);
				params.put("transTypeId", itemId);
				params.put("feePercent", form.getBigDecimal("processFeePercent_" + itemId, false));
				params.put("feeAmount", form.getBigDecimal("processFeeAdditional_" + itemId, false));
				params.put("minAmount", form.getBigDecimal("processFeeMinimum_" + itemId, false));
				DataLayerMgr.executeCall(conn, "LICENSE_PF_UPDATE", params);
			}
			results = DataLayerMgr.executeQuery(conn, "GET_LICENSE_SERVICE_FEES", params);
			while (results.next()) {
				int itemId = results.getValue("FEE_ID", int.class);
				params.put("feeId", itemId);
				BigDecimal serviceFeeAmount = form.getBigDecimal("serviceFeeAmount_" + itemId, false);
				params.put("feeAmount", serviceFeeAmount);
				if (serviceFeeAmount == null || serviceFeeAmount == new BigDecimal(0)) {
					params.put("dynamicMin", form.getBigDecimal("serviceFeeDynamicMin_" + itemId, false));
					params.put("dynamicMax", form.getBigDecimal("serviceFeeDynamicMax_" + itemId, false));
					params.put("dynamicAmount", form.getBigDecimal("serviceFeeDynamicAmount_" + itemId, false));
				} else {
					params.put("dynamicMin", null);
					params.put("dynamicMax", null);
					params.put("dynamicAmount", null);
				}
				params.put("immediate", form.getString("serviceFeeImmediate_" + itemId, false));
				params.put("inactiveFeeAmount", form.getBigDecimal("serviceFeeInactiveAmount_" + itemId, false));
				params.put("inactiveMonths", form.getInt("serviceFeeInactiveMonths_" + itemId, false, 0));
				int frequency=form.getInt("serviceFeeFrequencyId" + itemId, false, -1);
				if( frequency>0){
					params.put("frequency", frequency);
				}else {
					params.put("frequency", null);
				}
				DataLayerMgr.executeCall(conn, "LICENSE_SF_UPDATE", params);
			}
			conn.commit();
			success = true;
		}catch(Exception e){
    		throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
		}finally{
			if (!success)
    			ProcessingUtils.rollbackDbConnection(log, conn);    		
    		ProcessingUtils.closeDbConnection(log, conn);
		}
	}
}
