package com.usatech.dms.dealer;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerMgr;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

public class DealerStep extends AbstractStep{

	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request,
			HttpServletResponse response) throws ServletException {
	
		if(request.getParameter("action") == null)
			return;
		
		try{
			if(request.getParameter("action").equals("delete")){
				Long id = form.getLong("dealerId", false, 0);
				DataLayerMgr.executeCall("DEALER_DEL", new Long[] {id}, true);
                createMessage(response, "Succesfully deleted");
			}else if(request.getParameter("action").equals("update")){
				Long id = form.getLong("dealerId", true, 0);
				String name = form.getString("dealerName", true);
		        Object[] params = new Object[] {id,name };
		        DataLayerMgr.executeCall("DEALER_UPD", params, true);
		        createMessage(response, "Succesfully updated");
			}else if(request.getParameter("action").equals("insert")){
				Long id = form.getLong("dealerId", false, 0);
				String name = form.getString("dealerName", true);
		        Object[] params = new Object[] {id,name };
		        DataLayerMgr.executeCall("DEALER_INS", params, true);
		        createMessage(response, "Succesfully created");                
			}
		}catch(Exception e){
    		throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
    	}
	}
	
	private void createMessage( HttpServletResponse response, String msg) throws IOException{
        PrintWriter out = response.getWriter();
        response.setContentType("text");
        response.setHeader("Cache-Control", "no-cache");
        out.write(msg);
        out.close();
	}

}
