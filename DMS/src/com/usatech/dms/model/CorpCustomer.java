/*
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.usatech.layers.common.model.CustomerBankTerminal;

public class CorpCustomer implements Serializable
{
    private static final long serialVersionUID = 6717502974418036419L;

    private Long id;
    private String name;
    private Date createDate;
    private Date updDate;
    private String createBy;
    private String updBy;
    private String altName;
    private Long addressId;
    private String addressName;
    private String address1;
    private String address2;
    private String city;
    private String state;
    private String zip;
    private String licenseNbr;
    private String status;
    private Long userId;
    private Long dealerId;
    
    private Long affiliateId;
    private String affiliateName;
    private Long pricingTierId;
    private String pricingTierName;
    private Long categoryId;
    private String categoryName;
    
    private List<UserView> users;
    private List<CustomerBankTerminal> bankAccts;
    private List<Terminal> terminals;

    /**
     * @return the id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * @return the createDate
     */
    public Date getCreateDate()
    {
        return createDate;
    }

    /**
     * @param createDate the createDate to set
     */
    public void setCreateDate(Date createDate)
    {
        this.createDate = createDate;
    }

    /**
     * @return the updDate
     */
    public Date getUpdDate()
    {
        return updDate;
    }

    /**
     * @param updDate the updDate to set
     */
    public void setUpdDate(Date updDate)
    {
        this.updDate = updDate;
    }

    /**
     * @return the createBy
     */
    public String getCreateBy()
    {
        return createBy;
    }

    /**
     * @param createBy the createBy to set
     */
    public void setCreateBy(String createBy)
    {
        this.createBy = createBy;
    }

    /**
     * @return the updBy
     */
    public String getUpdBy()
    {
        return updBy;
    }

    /**
     * @param updBy the updBy to set
     */
    public void setUpdBy(String updBy)
    {
        this.updBy = updBy;
    }

    /**
     * @return the altName
     */
    public String getAltName()
    {
        return altName;
    }

    /**
     * @param altName the altName to set
     */
    public void setAltName(String altName)
    {
        this.altName = altName;
    }

    /**
     * @return the addressId
     */
    public Long getAddressId()
    {
        return addressId;
    }

    /**
     * @param addressId the addressId to set
     */
    public void setAddressId(Long addressId)
    {
        this.addressId = addressId;
    }

    /**
     * @return the addressName
     */
    public String getAddressName()
    {
        return addressName;
    }

    /**
     * @param addressName the addressName to set
     */
    public void setAddressName(String addressName)
    {
        this.addressName = addressName;
    }

    /**
     * @return the address1
     */
    public String getAddress1()
    {
        return address1;
    }

    /**
     * @param address1 the address1 to set
     */
    public void setAddress1(String address1)
    {
        this.address1 = address1;
    }

    /**
     * @return the address2
     */
    public String getAddress2()
    {
        return address2;
    }

    /**
     * @param address2 the address2 to set
     */
    public void setAddress2(String address2)
    {
        this.address2 = address2;
    }

    /**
     * @return the city
     */
    public String getCity()
    {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city)
    {
        this.city = city;
    }

    /**
     * @return the state
     */
    public String getState()
    {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(String state)
    {
        this.state = state;
    }

    /**
     * @return the zip
     */
    public String getZip()
    {
        return zip;
    }

    /**
     * @param zip the zip to set
     */
    public void setZip(String zip)
    {
        this.zip = zip;
    }

    /**
     * @return the licenseNbr
     */
    public String getLicenseNbr()
    {
        return licenseNbr;
    }

    /**
     * @param licenseNbr the licenseNbr to set
     */
    public void setLicenseNbr(String licenseNbr)
    {
        this.licenseNbr = licenseNbr;
    }

    /**
     * @return the status
     */
    public String getStatus()
    {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status)
    {
        this.status = status;
    }

    /**
     * @return the userId
     */
    public Long getUserId()
    {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    /**
     * @return the dealerId
     */
    public Long getDealerId()
    {
        return dealerId;
    }

    /**
     * @param dealerId the dealerId to set
     */
    public void setDealerId(Long dealerId)
    {
        this.dealerId = dealerId;
    }
    
    public long getAffiliateId() {
      return affiliateId;
    }

    public void setAffiliateId(long affiliateId) {
      this.affiliateId = affiliateId;
    }

    public String getAffiliateName() {
      return affiliateName;
    }

    public void setAffiliateName(String affiliateName) {
      this.affiliateName = affiliateName;
    }

    public long getPricingTierId() {
      return pricingTierId;
    }

    public void setPricingTierId(long pricingTierId) {
      this.pricingTierId = pricingTierId;
    }

    public String getPricingTierName() {
      return pricingTierName;
    }

    public void setPricingTierName(String pricingTierName) {
      this.pricingTierName = pricingTierName;
    }

    public long getCategoryId() {
      return categoryId;
    }

    public void setCategoryId(long categoryId) {
      this.categoryId = categoryId;
    }

    public String getCategoryName() {
      return categoryName;
    }

    public void setCategoryName(String categoryName) {
      this.categoryName = categoryName;
    }

    /**
     * @return the users
     */
    public List<UserView> getUsers()
    {
        return users;
    }

    /**
     * @param users the users to set
     */
    public void setUsers(List<UserView> users)
    {
        this.users = users;
    }

    /**
     * @return the bankAccts
     */
    public List<CustomerBankTerminal> getBankAccts()
    {
        return bankAccts;
    }

    /**
     * @param bankAccts the bankAccts to set
     */
    public void setBankAccts(List<CustomerBankTerminal> bankAccts)
    {
        this.bankAccts = bankAccts;
    }

    /**
     * @return the terminals
     */
    public List<Terminal> getTerminals()
    {
        return terminals;
    }

    /**
     * @param terminals the terminals to set
     */
    public void setTerminals(List<Terminal> terminals)
    {
        this.terminals = terminals;
    }
    
    public String getFormattedAddress(boolean multiLine) {
      StringBuilder sb = new StringBuilder();
      if (address1 != null) {
        sb.append(address1);
        if (multiLine)
          sb.append("\n");
        else
          sb.append(" ");
      }
      if (address2 != null) {
        sb.append(address2);
        if (multiLine)
          sb.append("\n");
        else
          sb.append(" ");
      }
      if (city != null) {
        sb.append(city);
          sb.append(" ");
      }
      if (state != null) {
        sb.append(state);
          sb.append(" ");
      }
      if (zip != null) {
        sb.append(zip);
          sb.append(" ");
      }
      return sb.toString().trim();
    }

}
