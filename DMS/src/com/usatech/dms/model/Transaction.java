/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.usatech.dms.model;

import java.io.Serializable;
import java.util.Date;


/**
 * Model class represents the Transaction.
 */
public class Transaction implements Serializable
{
    private static final long serialVersionUID = 6717502974418036418L;

    private Long id;
    private Date endTime;
    private Date startTime;
    private Date uploadTime;
    private String consumerAcctCd;
    private Long consumerAcctId;
    private String deviceSerialCd;
    private Long deviceId;
    private String stateCd;
    private String stateName;
    private String tranDesc; 
    private String deviceTranCd;
    private String createdBy;
    private Date createdDate;
    private String lastUpdatedBy;
    private Date lastUpdatedDate;
    private Long posPtaId;
    private String deviceResultTypeCd;
    private String glogalTransCd;
    private Long legacyTransNo;
    private Long parentTranId;
    private String previousTranStateCd;
    
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the endTime
	 */
	public Date getEndTime() {
		return endTime;
	}
	/**
	 * @param endTime the endTime to set
	 */
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	/**
	 * @return the startTime
	 */
	public Date getStartTime() {
		return startTime;
	}
	/**
	 * @param startTime the startTime to set
	 */
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	/**
	 * @return the stateName
	 */
	public String getStateName() {
		return stateName;
	}
	/**
	 * @param stateName the stateName to set
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	/**
	 * @return the consumerAcctCd
	 */
	public String getConsumerAcctCd() {
		return consumerAcctCd;
	}
	/**
	 * @param consumerAcctCd the consumerAcctCd to set
	 */
	public void setConsumerAcctCd(String consumerAcctCd) {
		this.consumerAcctCd = consumerAcctCd;
	}
	/**
	 * @return the consumerAcctId
	 */
	public Long getConsumerAcctId() {
		return consumerAcctId;
	}
	/**
	 * @param consumerAcctId the consumerAcctId to set
	 */
	public void setConsumerAcctId(Long consumerAcctId) {
		this.consumerAcctId = consumerAcctId;
	}
	/**
	 * @return the deviceSerialCd
	 */
	public String getDeviceSerialCd() {
		return deviceSerialCd;
	}
	/**
	 * @param deviceSerialCd the deviceSerialCd to set
	 */
	public void setDeviceSerialCd(String deviceSerialCd) {
		this.deviceSerialCd = deviceSerialCd;
	}
	/**
	 * @return the deviceId
	 */
	public Long getDeviceId() {
		return deviceId;
	}
	/**
	 * @param deviceId the deviceId to set
	 */
	public void setDeviceId(Long deviceId) {
		this.deviceId = deviceId;
	}
	/**
	 * @return the uploadTime
	 */
	public Date getUploadTime() {
		return uploadTime;
	}
	/**
	 * @param uploadTime the uploadTime to set
	 */
	public void setUploadTime(Date uploadTime) {
		this.uploadTime = uploadTime;
	}
	/**
	 * @return the stateCd
	 */
	public String getStateCd() {
		return stateCd;
	}
	/**
	 * @param stateCd the stateCd to set
	 */
	public void setStateCd(String stateCd) {
		this.stateCd = stateCd;
	}
	/**
	 * @return the tranDesc
	 */
	public String getTranDesc() {
		return tranDesc;
	}
	/**
	 * @param tranDesc the tranDesc to set
	 */
	public void setTranDesc(String tranDesc) {
		this.tranDesc = tranDesc;
	}
	/**
	 * @return the tranDeviceTranCd
	 */
	public String getTranDeviceTranCd() {
		return deviceTranCd;
	}
	/**
	 * @param tranDeviceTranCd the tranDeviceTranCd to set
	 */
	public void setTranDeviceTranCd(String tranDeviceTranCd) {
		this.deviceTranCd = tranDeviceTranCd;
	}
	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}
	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	/**
	 * @return the lastUpdatedBy
	 */
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	/**
	 * @param lastUpdatedBy the lastUpdatedBy to set
	 */
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	/**
	 * @return the lastUpdatedDate
	 */
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	/**
	 * @param lastUpdatedDate the lastUpdatedDate to set
	 */
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	/**
	 * @return the posPtaId
	 */
	public Long getPosPtaId() {
		return posPtaId;
	}
	/**
	 * @param posPtaId the posPtaId to set
	 */
	public void setPosPtaId(Long posPtaId) {
		this.posPtaId = posPtaId;
	}
	/**
	 * @return the deviceResultTypeCd
	 */
	public String getDeviceResultTypeCd() {
		return deviceResultTypeCd;
	}
	/**
	 * @param deviceResultTypeCd the deviceResultTypeCd to set
	 */
	public void setDeviceResultTypeCd(String deviceResultTypeCd) {
		this.deviceResultTypeCd = deviceResultTypeCd;
	}
	/**
	 * @return the glogalTransCd
	 */
	public String getGlogalTransCd() {
		return glogalTransCd;
	}
	/**
	 * @param glogalTransCd the glogalTransCd to set
	 */
	public void setGlogalTransCd(String glogalTransCd) {
		this.glogalTransCd = glogalTransCd;
	}
	/**
	 * @return the legacyTransNo
	 */
	public Long getLegacyTransNo() {
		return legacyTransNo;
	}
	/**
	 * @param legacyTransNo the legacyTransNo to set
	 */
	public void setLegacyTransNo(Long legacyTransNo) {
		this.legacyTransNo = legacyTransNo;
	}
	/**
	 * @return the parentTranId
	 */
	public Long getParentTranId() {
		return parentTranId;
	}
	/**
	 * @param parentTranId the parentTranId to set
	 */
	public void setParentTranId(Long parentTranId) {
		this.parentTranId = parentTranId;
	}
	/**
	 * @return the previousTranStateCd
	 */
	public String getPreviousTranStateCd() {
		return previousTranStateCd;
	}
	/**
	 * @param previousTranStateCd the previousTranStateCd to set
	 */
	public void setPreviousTranStateCd(String previousTranStateCd) {
		this.previousTranStateCd = previousTranStateCd;
	}
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
