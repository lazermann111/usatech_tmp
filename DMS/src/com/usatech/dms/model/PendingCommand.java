/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.model;

import java.io.Serializable;

import com.usatech.layers.common.constants.FileType;
import com.usatech.layers.common.util.StringHelper;

/**
 * Model class represents the pending command data.
 */
public class PendingCommand implements Serializable
{
    private static final long serialVersionUID = -4505735726824837530L;

    private String id; // machine_command_pending_id
    private String type; // data_type
    private String command; // command
    private String executeCd; // execute_cd
    private String executeDate; // execute_date
    private String executeOrder; // execute_order
    private String createdTs; // created_ts
    private String fileTransId; // file_transfer_id
    private String fileTransName; // file_transfer_name
    private String fileTransTypeCd; // file_transfer_type_cd
    private String deviceFileTransferId; //device_file_transfer_id
    private String actionDesc = null;
    private String firmwareUpgradeId; //firmware_upgrade_id
    private String firmwareUpgradeName; //firmware_upgrade_name
    private String firmwareUpgradeDesc; //firmware_upgrade_desc
    private String deviceFirmwareUpgradeStatusName; //device_fw_upg_status_name
    private String firmwareUpgradeFileId; //fw_upg_file_transfer_id
    private String firmwareUpgradeFileName; //fw_upg_file_transfer_name
    private String errorMessage; //error_message
    private String globalSessionCd; //global_session_cd
    private String sessionId; //session_id
    private String attemptCount; //attempt_count

	private boolean pendingPvl;

    private String typeDesc;
    private String commandDesc;
    private String statusDesc;
    private boolean firmwareUpdate;

    /**
     * @return the id
     */
    public String getId()
    {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id)
    {
        this.id = id;
    }

    /**
     * @return the type
     */
    public String getType()
    {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type)
    {
        this.type = type;
    }

    /**
     * @return the command
     */
    public String getCommand()
    {
        return command;
    }

    /**
     * @param command the command to set
     */
    public void setCommand(String command)
    {
        this.command = command;
    }

    /**
     * @return the executeCd
     */
    public String getExecuteCd()
    {
        return executeCd;
    }

    /**
     * @param executeCd the executeCd to set
     */
    public void setExecuteCd(String executeCd)
    {
        this.executeCd = executeCd;
    }

    /**
     * @return the executeDate
     */
    public String getExecuteDate()
    {
        return executeDate;
    }

    /**
     * @param executeDate the executeDate to set
     */
    public void setExecuteDate(String executeDate)
    {
        this.executeDate = executeDate;
    }

    /**
     * @return the executeOrder
     */
    public String getExecuteOrder()
    {
        return executeOrder;
    }

    /**
     * @param executeOrder the executeOrder to set
     */
    public void setExecuteOrder(String executeOrder)
    {
        this.executeOrder = executeOrder;
    }

    /**
     * @return the createdTs
     */
    public String getCreatedTs()
    {
        return createdTs;
    }

    /**
     * @param createdTs the createdTs to set
     */
    public void setCreatedTs(String createdTs)
    {
        this.createdTs = createdTs;
    }

    /**
     * @return the fileTransId
     */
    public String getFileTransId()
    {
        return fileTransId;
    }

    /**
     * @param fileTransId the fileTransId to set
     */
    public void setFileTransId(String fileTransId)
    {
        this.fileTransId = fileTransId;
    }

    /**
     * @return the fileTransName
     */
    public String getFileTransName()
    {
        return fileTransName;
    }

    /**
     * @param fileTransName the fileTransName to set
     */
    public void setFileTransName(String fileTransName)
    {
        this.fileTransName = fileTransName;
    }

    /**
     * @return the fileTransTypeCd
     */
    public String getFileTransTypeCd()
    {
        return fileTransTypeCd;
    }

    /**
     * @param fileTransTypeCd the fileTransTypeCd to set
     */
    public void setFileTransTypeCd(String fileTransTypeCd)
    {
        this.fileTransTypeCd = fileTransTypeCd;
        pendingPvl = !StringHelper.isBlank(fileTransTypeCd) && FileType.PROPERTY_LIST.getValue() == Integer.parseInt(fileTransTypeCd);
    }

    /**
     * @return the pendingPvl
     */
    public boolean isPendingPvl()
    {
        return pendingPvl;
    }

    /**
     * @return the typeDesc
     */
    public String getTypeDesc()
    {
        return typeDesc;
    }

    /**
     * @param typeDesc the typeDesc to set
     */
    public void setTypeDesc(String typeDesc)
    {
        this.typeDesc = typeDesc;
    }

    /**
     * @return the commandDesc
     */
    public String getCommandDesc()
    {
        return commandDesc;
    }

    /**
     * @param commandDesc the commandDesc to set
     */
    public void setCommandDesc(String commandDesc)
    {
        this.commandDesc = commandDesc;
    }

    /**
     * @return the statusDesc
     */
    public String getStatusDesc()
    {
        return statusDesc;
    }

    /**
     * @param statusDesc the statusDesc to set
     */
    public void setStatusDesc(String statusDesc)
    {
        this.statusDesc = statusDesc;
    }

    /**
     * @return the long value of fileTransId, default to -1
     */
    public long getFileTransIdNumber()
    {
        return StringHelper.isBlank(fileTransId) ? -1 : Long.parseLong(fileTransId);
    }
    
    public String getActionDesc() {
		return actionDesc;
	}

	public void setActionDesc(String actionDesc) {
		this.actionDesc = actionDesc;
	}

	public String getFirmwareUpgradeId() {
		return firmwareUpgradeId;
	}

	public void setFirmwareUpgradeId(String firmwareUpgradeId) {
		this.firmwareUpgradeId = firmwareUpgradeId;
	}

	public String getFirmwareUpgradeName() {
		return firmwareUpgradeName;
	}

	public void setFirmwareUpgradeName(String firmwareUpgradeName) {
		this.firmwareUpgradeName = firmwareUpgradeName;
	}

	public String getFirmwareUpgradeDesc() {
		return firmwareUpgradeDesc;
	}

	public void setFirmwareUpgradeDesc(String firmwareUpgradeDesc) {
		this.firmwareUpgradeDesc = firmwareUpgradeDesc;
	}
	
	public long getFirmwareUpgradeIdNumber()
    {
        return StringHelper.isBlank(firmwareUpgradeId) ? -1 : Long.parseLong(firmwareUpgradeId);
    }

	public String getGlobalSessionCd() {
		return globalSessionCd;
	}

	public void setGlobalSessionCd(String globalSessionCd) {
		this.globalSessionCd = globalSessionCd;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getDeviceFileTransferId() {
		return deviceFileTransferId;
	}

	public void setDeviceFileTransferId(String deviceFileTransferId) {
		this.deviceFileTransferId = deviceFileTransferId;
	}

	public String getDeviceFirmwareUpgradeStatusName() {
		return deviceFirmwareUpgradeStatusName;
	}

	public void setDeviceFirmwareUpgradeStatusName(String deviceFirmwareUpgradeStatusName) {
		this.deviceFirmwareUpgradeStatusName = deviceFirmwareUpgradeStatusName;
	}

	public String getFirmwareUpgradeFileId() {
		return firmwareUpgradeFileId;
	}

	public void setFirmwareUpgradeFileId(String firmwareUpgradeFileId) {
		this.firmwareUpgradeFileId = firmwareUpgradeFileId;
	}
	
    public long getFirmwareUpgradeFileIdNumber()
    {
        return StringHelper.isBlank(firmwareUpgradeFileId) ? -1 : Long.parseLong(firmwareUpgradeFileId);
    }

	public String getFirmwareUpgradeFileName() {
		return firmwareUpgradeFileName;
	}

	public void setFirmwareUpgradeFileName(String firmwareUpgradeFileName) {
		this.firmwareUpgradeFileName = firmwareUpgradeFileName;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getAttemptCount() {
		return attemptCount;
	}

	public void setAttemptCount(String attemptCount) {
		this.attemptCount = attemptCount;
	}

    public boolean isFirmwareUpdate() {
        return firmwareUpdate;
    }

    public void setFirmwareUpdate(boolean firmwareUpdate) {
        this.firmwareUpdate = firmwareUpdate;
    }
}
