/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.usatech.dms.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Model class represents the Consumer.
 */
public class Consumer implements Serializable
{
	
	
	private static final long serialVersionUID = 5268043178974183847L;
	
	private Long id;
    private String fname;
    private String lname;
    private String mname;
    private String salutation;
    private String   title;
    private Long   typeId;
    private String address1;
    private String address2;
    private String city;
    private String stateCode;
    private String county;
	private String postalCode;
    private String countryCode;
    private String emailAddress1;
    private String emailAddress2;
    private String phoneNumWork;
    private String phoneNumCell;
    private String phoneNumHome;
    private String phoneNumFax;
    private String pagerCd;
    private Date   createdDate; 
    private String createdBy;
    private Date   lastUpdatedDate; 
    private String lastUpdatedBy; 
    private Long numCards; 
    private Long numTransactions;
    private List<ConsumerAccount> accounts;
    private List<Transaction> transactions;
    private Integer isisTranCount;
    private Integer isisPromoTranCount;
    private String consumerIdentifier;
    private String customerAssignedId;
    private String departmentName;
    /**
	 * @return the county
	 */
	public String getCounty() {
		return county;
	}
	/**
	 * @param county the county to set
	 */
	public void setCounty(String county) {
		this.county = county;
	}
    /**
	 * @return the transactions
	 */
	public List<Transaction> getTransactions() {
		return transactions;
	}
	/**
	 * @param transactions the transactions to set
	 */
	public void setTransactions(List<Transaction> transactions) {
		this.transactions = transactions;
	}
	/**
	 * @return the accounts
	 */
	public List<ConsumerAccount> getAccounts() {
		return accounts;
	}
	/**
	 * @param accounts the accounts to set
	 */
	public void setAccounts(List<ConsumerAccount> accounts) {
		this.accounts = accounts;
	}
	
    /**
	 * @return the numTransactions
	 */
	public Long getNumTransactions() {
		return numTransactions;
	}
	/**
	 * @param numTransactions the numTransactions to set
	 */
	public void setNumTransactions(Long numTransactions) {
		this.numTransactions = numTransactions;
	}
	/**
	 * @return the numCards
	 */
	public Long getNumCards() {
		return numCards;
	}
	/**
	 * @param numCards the numCards to set
	 */
	public void setNumCards(Long numCards) {
		this.numCards = numCards;
	}
	
    
    /**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the fname
	 */
	public String getFname() {
		return fname;
	}
	/**
	 * @param fname the fname to set
	 */
	public void setFname(String fname) {
		this.fname = fname;
	}
	/**
	 * @return the lname
	 */
	public String getLname() {
		return lname;
	}
	/**
	 * @param lname the lname to set
	 */
	public void setLname(String lname) {
		this.lname = lname;
	}
	/**
	 * @return the mname
	 */
	public String getMname() {
		return mname;
	}
	/**
	 * @param mname the mname to set
	 */
	public void setMname(String mname) {
		this.mname = mname;
	}
	/**
	 * @return the salutation
	 */
	public String getSalutation() {
		return salutation;
	}
	/**
	 * @param salutation the salutation to set
	 */
	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the typeId
	 */
	public Long getTypeId() {
		return typeId;
	}
	/**
	 * @param typeId the typeId to set
	 */
	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}
	/**
	 * @return the address1
	 */
	public String getAddress1() {
		return address1;
	}
	/**
	 * @param address1 the address1 to set
	 */
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	/**
	 * @return the address2
	 */
	public String getAddress2() {
		return address2;
	}
	/**
	 * @param address2 the address2 to set
	 */
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * @return the stateCode
	 */
	public String getStateCode() {
		return stateCode;
	}
	/**
	 * @param stateCode the stateCode to set
	 */
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
	/**
	 * @return the postalCode
	 */
	public String getPostalCode() {
		return postalCode;
	}
	/**
	 * @param postalCode the postalCode to set
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	/**
	 * @return the countryCode
	 */
	public String getCountryCode() {
		return countryCode;
	}
	/**
	 * @param countryCode the countryCode to set
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	/**
	 * @return the emailAddress1
	 */
	public String getEmailAddress1() {
		return emailAddress1;
	}
	/**
	 * @param emailAddress1 the emailAddress1 to set
	 */
	public void setEmailAddress1(String emailAddress1) {
		this.emailAddress1 = emailAddress1;
	}
	/**
	 * @return the emailAddress2
	 */
	public String getEmailAddress2() {
		return emailAddress2;
	}
	/**
	 * @param emailAddress2 the emailAddress2 to set
	 */
	public void setEmailAddress2(String emailAddress2) {
		this.emailAddress2 = emailAddress2;
	}
	/**
	 * @return the phoneNumWork
	 */
	public String getPhoneNumWork() {
		return phoneNumWork;
	}
	/**
	 * @param phoneNumWork the phoneNumWork to set
	 */
	public void setPhoneNumWork(String phoneNumWork) {
		this.phoneNumWork = phoneNumWork;
	}
	/**
	 * @return the phoneNumCell
	 */
	public String getPhoneNumCell() {
		return phoneNumCell;
	}
	/**
	 * @param phoneNumCell the phoneNumCell to set
	 */
	public void setPhoneNumCell(String phoneNumCell) {
		this.phoneNumCell = phoneNumCell;
	}
	/**
	 * @return the phoneNumHome
	 */
	public String getPhoneNumHome() {
		return phoneNumHome;
	}
	/**
	 * @param phoneNumHome the phoneNumHome to set
	 */
	public void setPhoneNumHome(String phoneNumHome) {
		this.phoneNumHome = phoneNumHome;
	}
	/**
	 * @return the phoneNumFax
	 */
	public String getPhoneNumFax() {
		return phoneNumFax;
	}
	/**
	 * @param phoneNumFax the phoneNumFax to set
	 */
	public void setPhoneNumFax(String phoneNumFax) {
		this.phoneNumFax = phoneNumFax;
	}
	/**
	 * @return the pagerCd
	 */
	public String getPagerCd() {
		return pagerCd;
	}
	/**
	 * @param pagerCd the pagerCd to set
	 */
	public void setPagerCd(String pagerCd) {
		this.pagerCd = pagerCd;
	}
	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}
	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the lastUpdatedDate
	 */
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	/**
	 * @param lastUpdatedDate the lastUpdatedDate to set
	 */
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	/**
	 * @return the lastUpdatedBy
	 */
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	/**
	 * @param lastUpdatedBy the lastUpdatedBy to set
	 */
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public Integer getIsisTranCount() {
		return isisTranCount;
	}
	public void setIsisTranCount(Integer isisTranCount) {
		this.isisTranCount = isisTranCount;
	}
	public Integer getIsisPromoTranCount() {
		return isisPromoTranCount;
	}
	public void setIsisPromoTranCount(Integer isisPromoTranCount) {
		this.isisPromoTranCount = isisPromoTranCount;
	}
	public String getConsumerIdentifier() {
		return consumerIdentifier;
	}
	public void setConsumerIdentifier(String consumerIdentifier) {
		this.consumerIdentifier = consumerIdentifier;
	}
	public String getCustomerAssignedId() {
		return customerAssignedId;
	}
	public void setCustomerAssignedId(String customerAssignedId) {
		this.customerAssignedId = customerAssignedId;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
}