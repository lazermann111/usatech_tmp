/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 * Note: for numeric values in the db, if they are nullable, you may want to type them
 * in the model classes here as String, in order for BeanUtils methods to convert properly during
 * copying of properties.
 * 
 * Mods:
 * Date			Author		Bug#			Description
 * ---------------------------------------------------------------------------------
 */
package com.usatech.dms.model;

public class Hold extends Auth {

}
