/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.model;

import java.io.Serializable;

/**
 * Model class represents the "action value", "action description" and "payload value" pair which
 * will be used for the EDGE devices in "Device Configuration".
 */
public class ActionPayloadTriple implements Serializable
{
    private static final long serialVersionUID = 3727644152088541160L;

    private String actionValue;
    private String actionDesc;
    private String payloadValue;

    /**
     * @return the actionValue
     */
    public String getActionValue()
    {
        return actionValue;
    }

    /**
     * @param actionValue the actionValue to set
     */
    public void setActionValue(String actionValue)
    {
        this.actionValue = actionValue;
    }

    /**
     * @return the actionDesc
     */
    public String getActionDesc()
    {
        return actionDesc;
    }

    /**
     * @param actionDesc the actionDesc to set
     */
    public void setActionDesc(String actionDesc)
    {
        this.actionDesc = actionDesc;
    }

    /**
     * @return the payloadValue
     */
    public String getPayloadValue()
    {
        return payloadValue;
    }

    /**
     * @param payloadValue the payloadValue to set
     */
    public void setPayloadValue(String payloadValue)
    {
        this.payloadValue = payloadValue;
    }

}
