/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.usatech.dms.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Model class represents the Terminal Eport.
 */
public class TerminalEport implements Serializable
{
    private static final long serialVersionUID = 6717502974418036418L;

    private Long id;
    private Long eportId;
    private Date startDate;
    private Date endDate;
    private String eportSerialNumber;

    /**
     * @return the id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     * @return the eportId
     */
    public Long getEportId()
    {
        return eportId;
    }

    /**
     * @param eportId the eportId to set
     */
    public void setEportId(Long eportId)
    {
        this.eportId = eportId;
    }

    /**
     * @return the startDate
     */
    public Date getStartDate()
    {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(Date startDate)
    {
        this.startDate = startDate;
    }

    /**
     * @return the endDate
     */
    public Date getEndDate()
    {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(Date endDate)
    {
        this.endDate = endDate;
    }

    /**
     * @return the eportSerialNumber
     */
    public String getEportSerialNumber()
    {
        return eportSerialNumber;
    }

    /**
     * @param eportSerialNumber the eportSerialNumber to set
     */
    public void setEportSerialNumber(String eportSerialNumber)
    {
        this.eportSerialNumber = eportSerialNumber;
    }

}
