/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.usatech.dms.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.usatech.layers.common.model.CustomerBankTerminal;

/**
 * Model class represents the Terminal.
 */
public class Terminal implements Serializable
{
    private static final long serialVersionUID = 6717502974418036418L;

    private Long id;
    private Long customerId;
    private String terminalNumber;
    private String customerName;
    private String locationName;
    private String createdDate;
    private String eportSerialNumber;
    private String telephone;
    private String dialPrefix;
    private String primaryContact;
    private String authorizationMode;
    private String timeZone;
    private String assetNumber;
    private String customerTerminalNumber;
    private String dexData;
    private String receipt;
    private String status;
    private Double vends;
    private Long paymentScheduleId;
    private Long feeCurrencyId;
    private Long businessUnitId;
    private List<TerminalEport> terminalEports;
    private List<CustomerBankTerminal> customerBankTerminals;
    private String salesOrderNumber;
    private String purchaseOrderNumber;
    private Double avgTransAmt;
    private Double maxTransAmt;
    private Double avgTransCnt;
    private Date lastRiskCheck;

    /**
     * @return the id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     * @return the customerId
     */
    public Long getCustomerId()
    {
        return customerId;
    }

    /**
     * @param customerId the customerId to set
     */
    public void setCustomerId(Long customerId)
    {
        this.customerId = customerId;
    }

    /**
     * @return the terminalNumber
     */
    public String getTerminalNumber()
    {
        return terminalNumber;
    }

    /**
     * @param terminalNumber the terminalNumber to set
     */
    public void setTerminalNumber(String terminalNumber)
    {
        this.terminalNumber = terminalNumber;
    }

    /**
     * @return the customerName
     */
    public String getCustomerName()
    {
        return customerName;
    }

    /**
     * @param customerName the customerName to set
     */
    public void setCustomerName(String customerName)
    {
        this.customerName = customerName;
    }

    /**
     * @return the locationName
     */
    public String getLocationName()
    {
        return locationName;
    }

    /**
     * @param locationName the locationName to set
     */
    public void setLocationName(String locationName)
    {
        this.locationName = locationName;
    }

    /**
     * @return the createDate
     */
    public String getCreatedDate()
    {
        return createdDate;
    }

    /**
     * @param createDate the createDate to set
     */
    public void setCreatedDate(String createdDate)
    {
        this.createdDate = createdDate;
    }

    /**
     * @return the eportSerialNumber
     */
    public String getEportSerialNumber()
    {
        return eportSerialNumber;
    }

    /**
     * @param eportSerialNumber the eportSerialNumber to set
     */
    public void setEportSerialNumber(String eportSerialNumber)
    {
        this.eportSerialNumber = eportSerialNumber;
    }

    /**
     * @return the telephone
     */
    public String getTelephone()
    {
        return telephone;
    }

    /**
     * @param telephone the telephone to set
     */
    public void setTelephone(String telephone)
    {
        this.telephone = telephone;
    }

    /**
     * @return the dialPrefix
     */
    public String getDialPrefix()
    {
        return dialPrefix;
    }

    /**
     * @param dialPrefix the dialPrefix to set
     */
    public void setDialPrefix(String dialPrefix)
    {
        this.dialPrefix = dialPrefix;
    }

    /**
     * @return the primaryContact
     */
    public String getPrimaryContact()
    {
        return primaryContact;
    }

    /**
     * @param primaryContact the primaryContact to set
     */
    public void setPrimaryContact(String primaryContact)
    {
        this.primaryContact = primaryContact;
    }

    /**
     * @return the authorizationMode
     */
    public String getAuthorizationMode()
    {
        return authorizationMode;
    }

    /**
     * @param authorizationMode the authorizationMode to set
     */
    public void setAuthorizationMode(String authorizationMode)
    {
        this.authorizationMode = authorizationMode;
    }

    /**
     * @return the timeZone
     */
    public String getTimeZone()
    {
        return timeZone;
    }

    /**
     * @param timeZone the timeZone to set
     */
    public void setTimeZone(String timeZone)
    {
        this.timeZone = timeZone;
    }

    /**
     * @return the avgTransAmt
     */
    public Double getAvgTransAmt()
    {
        return avgTransAmt;
    }

    /**
     * @param avgTransAmt the avgTransAmt to set
     */
    public void setAvgTransAmt(Double avgTransAmt)
    {
        this.avgTransAmt = avgTransAmt;
    }

    /**
     * @return the dexData
     */
    public String getDexData()
    {
        return dexData;
    }

    /**
     * @param dexData the dexData to set
     */
    public void setDexData(String dexData)
    {
        this.dexData = dexData;
    }

    /**
     * @return the receipt
     */
    public String getReceipt()
    {
        return receipt;
    }

    /**
     * @param receipt the receipt to set
     */
    public void setReceipt(String receipt)
    {
        this.receipt = receipt;
    }

    /**
     * @return the status
     */
    public String getStatus()
    {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status)
    {
        this.status = status;
    }
    
    public String getStatusLabel(){
        if (status!=null){
            if (status.equalsIgnoreCase("A"))
                return "Active";
            if (status.equalsIgnoreCase("P"))
                return "New";
            if (status.equalsIgnoreCase("U"))
                return "Auto-generated";
            if (status.equalsIgnoreCase("D"))
                return "Deactivated";
            return status;
        }
        return "";
    }

    /**
     * @return the vends
     */
    public Double getVends()
    {
        return vends;
    }

    /**
     * @param vends the vends to set
     */
    public void setVends(Double vends)
    {
        this.vends = vends;
    }
    
    /**
     * @return the assetNumber
     */
    public String getAssetNumber()
    {
        return assetNumber;
    }

    /**
     * @param assetNumber the assetNumber to set
     */
    public void setAssetNumber(String assetNumber)
    {
        this.assetNumber = assetNumber;
    }

    /**
     * @return the customerTerminalNumber
     */
    public String getCustomerTerminalNumber()
    {
        return customerTerminalNumber;
    }

    /**
     * @param customerTerminalNumber the customerTerminalNumber to set
     */
    public void setCustomerTerminalNumber(String customerTerminalNumber)
    {
        this.customerTerminalNumber = customerTerminalNumber;
    }    

    /**
     * @return the paymentScheduleId
     */
    public Long getPaymentScheduleId()
    {
        return paymentScheduleId;
    }

    /**
     * @param paymentScheduleId the paymentScheduleId to set
     */
    public void setPaymentScheduleId(Long paymentScheduleId)
    {
        this.paymentScheduleId = paymentScheduleId;
    }

    /**
     * @return the feeCurrencyId
     */
    public Long getFeeCurrencyId()
    {
        return feeCurrencyId;
    }

    /**
     * @param feeCurrencId the feeCurrencId to set
     */
    public void setFeeCurrencyId(Long feeCurrencyId)
    {
        this.feeCurrencyId = feeCurrencyId;
    }

    /**
     * @return the businessUnitId
     */
    public Long getBusinessUnitId()
    {
        return businessUnitId;
    }

    /**
     * @param businessUnitId the businessUnitId to set
     */
    public void setBusinessUnitId(Long businessUnitId)
    {
        this.businessUnitId = businessUnitId;
    }

    /**
     * @return the terminalEports
     */
    public List<TerminalEport> getTerminalEports()
    {
        return terminalEports;
    }

    /**
     * @param terminalEports the terminalEports to set
     */
    public void setTerminalEports(List<TerminalEport> terminalEports)
    {
        this.terminalEports = terminalEports;
    }

    /**
     * @return the customerBankTerminals
     */
    public List<CustomerBankTerminal> getCustomerBankTerminals()
    {
        return customerBankTerminals;
    }

    /**
     * @param customerBankTerminals the customerBankTerminals to set
     */
    public void setCustomerBankTerminals(List<CustomerBankTerminal> customerBankTerminals)
    {
        this.customerBankTerminals = customerBankTerminals;
    }

	public String getSalesOrderNumber() {
		return salesOrderNumber;
	}

	public void setSalesOrderNumber(String salesOrderNumber) {
		this.salesOrderNumber = salesOrderNumber;
	}

	public String getPurchaseOrderNumber() {
		return purchaseOrderNumber;
	}

	public void setPurchaseOrderNumber(String purchaseOrderNumber) {
		this.purchaseOrderNumber = purchaseOrderNumber;
	}
	
	public Double getMaxTransAmt() {
		return maxTransAmt;
	}
	
	public void setMaxTransAmt(Double maxTransAmt) {
		this.maxTransAmt = maxTransAmt;
	}
	
	public Double getAvgTransCnt() {
		return avgTransCnt;
	}
	
	public void setAvgTransCnt(Double avgTransCnt) {
		this.avgTransCnt = avgTransCnt;
	}
	
	public Date getLastRiskCheck() {
		return lastRiskCheck;
	}
	
	public void setLastRiskCheck(Date lastRiskCheck) {
		this.lastRiskCheck = lastRiskCheck;
	}

}
