/**
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.model;

import java.io.Serializable;

public class RiskAlertDetails extends RiskAlert implements Serializable {
  private static final long serialVersionUID = 1L;
  
  private String customerName;
  private String deviceSerialCd;
  private String terminalNbr;
  private String deviceTypeDesc;
  private String locationName;
  
  public RiskAlertDetails(Long id) {
    super(id);
  }

  public String getCustomerName() {
    return customerName;
  }

  public void setCustomerName(String customerName) {
    this.customerName = customerName;
  }

  public String getDeviceSerialCd() {
    return deviceSerialCd;
  }

  public void setDeviceSerialCd(String deviceSerialCd) {
    this.deviceSerialCd = deviceSerialCd;
  }
  
  public String getTerminalNbr() {
    return terminalNbr;
  }
  
  public void setTerminalNbr(String terminalNbr) {
    this.terminalNbr = terminalNbr;
  }

  public String getDeviceTypeDesc() {
    return deviceTypeDesc;
  }

  public void setDeviceTypeDesc(String deviceTypeDesc) {
    this.deviceTypeDesc = deviceTypeDesc;
  }

  public String getLocationName() {
    return locationName;
  }

  public void setLocationName(String locationName) {
    this.locationName = locationName;
  }

}
