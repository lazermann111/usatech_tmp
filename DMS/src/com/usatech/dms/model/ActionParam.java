package com.usatech.dms.model;

import java.util.Date;

public class ActionParam {
	private Long actionParamId;
	private String actionParamName;
	private String actionParamDesc;
	private String actionParamCd;
	private String 	createdBy;
    private Date   	createdDate;
    private String 	lastUpdatedBy;
    private Date   	lastUpdatedDate;
    private String  protocolBitIndex;
	/**
	 * @return the actionParamId
	 */
	public Long getActionParamId() {
		return actionParamId;
	}
	/**
	 * @param actionParamId the actionParamId to set
	 */
	public void setActionParamId(Long actionParamId) {
		this.actionParamId = actionParamId;
	}
	/**
	 * @return the actionParamName
	 */
	public String getActionParamName() {
		return actionParamName;
	}
	/**
	 * @param actionParamName the actionParamName to set
	 */
	public void setActionParamName(String actionParamName) {
		this.actionParamName = actionParamName;
	}
	/**
	 * @return the actionParamDesc
	 */
	public String getActionParamDesc() {
		return actionParamDesc;
	}
	/**
	 * @param actionParamDesc the actionParamDesc to set
	 */
	public void setActionParamDesc(String actionParamDesc) {
		this.actionParamDesc = actionParamDesc;
	}
	/**
	 * @return the actionParamCd
	 */
	public String getActionParamCd() {
		return actionParamCd;
	}
	/**
	 * @param actionParamCd the actionParamCd to set
	 */
	public void setActionParamCd(String actionParamCd) {
		this.actionParamCd = actionParamCd;
	}
	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}
	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	/**
	 * @return the lastUpdatedBy
	 */
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	/**
	 * @param lastUpdatedBy the lastUpdatedBy to set
	 */
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	/**
	 * @return the lastUpdatedDate
	 */
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	/**
	 * @param lastUpdatedDate the lastUpdatedDate to set
	 */
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	/**
	 * @return the protocolBitIndex
	 */
	public String getProtocolBitIndex() {
		return protocolBitIndex;
	}
	/**
	 * @param protocolBitIndex the protocolBitIndex to set
	 */
	public void setProtocolBitIndex(String protocolBitIndex) {
		this.protocolBitIndex = protocolBitIndex;
	}
    
}
