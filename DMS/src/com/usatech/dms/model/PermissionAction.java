package com.usatech.dms.model;

import java.util.List;

public class PermissionAction {
	private Long permissionActionId;
	private Long actionId;
	private Action action;
	private List<PermissionActionParam> permissionActionParams;
	/**
	 * @return the permissionActionId
	 */
	public Long getPermissionActionId() {
		return permissionActionId;
	}
	/**
	 * @param permissionActionId the permissionActionId to set
	 */
	public void setPermissionActionId(Long permissionActionId) {
		this.permissionActionId = permissionActionId;
	}
	/**
	 * @return the actionId
	 */
	public Long getActionId() {
		return actionId;
	}
	/**
	 * @param actionId the actionId to set
	 */
	public void setActionId(Long actionId) {
		this.actionId = actionId;
	}
	/**
	 * @return the action
	 */
	public Action getAction() {
		return action;
	}
	/**
	 * @param action the action to set
	 */
	public void setAction(Action action) {
		this.action = action;
	}
	/**
	 * @return the permissionActionParams
	 */
	public List<PermissionActionParam> getPermissionActionParams() {
		return permissionActionParams;
	}
	/**
	 * @param permissionActionParams the permissionActionParams to set
	 */
	public void setPermissionActionParams(
			List<PermissionActionParam> permissionActionParams) {
		this.permissionActionParams = permissionActionParams;
	}
	
}
