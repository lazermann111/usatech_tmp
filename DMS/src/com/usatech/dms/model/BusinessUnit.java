/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.usatech.dms.model;

import java.io.Serializable;

/**
 * Model class represents the BusinessUnit.
 */
public class BusinessUnit implements Serializable
{
    private static final long serialVersionUID = 6717502974418036418L;

    private Long id;
    private Long defaultPaymentScheduleId;
    private String name;
    private String description;

    /**
     * @return the id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     * @return the defaultPaymentScheduleId
     */
    public Long getDefaultPaymentScheduleId()
    {
        return defaultPaymentScheduleId;
    }

    /**
     * @param defaultPaymentScheduleId the defaultPaymentScheduleId to set
     */
    public void setDefaultPaymentScheduleId(Long defaultPaymentScheduleId)
    {
        this.defaultPaymentScheduleId = defaultPaymentScheduleId;
    }

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description)
    {
        this.description = description;
    }

}
