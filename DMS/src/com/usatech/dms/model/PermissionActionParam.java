package com.usatech.dms.model;

public class PermissionActionParam extends ActionParam {
	private Long permissionActionId;
	private Long permissionActionParamOrder;
	/**
	 * @return the permissionActionId
	 */
	public Long getPermissionActionId() {
		return permissionActionId;
	}
	/**
	 * @param permissionActionId the permissionActionId to set
	 */
	public void setPermissionActionId(Long permissionActionId) {
		this.permissionActionId = permissionActionId;
	}
	/**
	 * @return the permissionActionParamOrder
	 */
	public Long getPermissionActionParamOrder() {
		return permissionActionParamOrder;
	}
	/**
	 * @param permissionActionParamOrder the permissionActionParamOrder to set
	 */
	public void setPermissionActionParamOrder(Long permissionActionParamOrder) {
		this.permissionActionParamOrder = permissionActionParamOrder;
	}
	
	
}
