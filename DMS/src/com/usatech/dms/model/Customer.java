/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.usatech.dms.model;

import java.io.Serializable;

/**
 * Model class represents the Customer.
 */
public class Customer implements Serializable
{
    private static final long serialVersionUID = 6717502974418036418L;

    private Long id;
    private String name;
    private String address1;
    private String address2;
    private String city;
    private String country;
    private String postalCode;
    private String countryCode;
    private Integer typeId;
    private String stateCode;
    private String activeFlag;
    private String altName;
    private String salesTerm;
    private String creditTerm;
    private String status;
    private String appUserName;
    private String emailAddress;
    private String subdomain;
    

    /**
     * @return the id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * @return the address1
     */
    public String getAddress1()
    {
        return address1;
    }

    /**
     * @param address1 the address1 to set
     */
    public void setAddress1(String address1)
    {
        this.address1 = address1;
    }

    /**
     * @return the address2
     */
    public String getAddress2()
    {
        return address2;
    }

    /**
     * @param address2 the address2 to set
     */
    public void setAddress2(String address2)
    {
        this.address2 = address2;
    }

    /**
     * @return the city
     */
    public String getCity()
    {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city)
    {
        this.city = city;
    }

    /**
     * @return the country
     */
    public String getCountry()
    {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country)
    {
        this.country = country;
    }

    /**
     * @return the postalCode
     */
    public String getPostalCode()
    {
        return postalCode;
    }

    /**
     * @param postalCode the postalCode to set
     */
    public void setPostalCode(String postalCode)
    {
        this.postalCode = postalCode;
    }

    /**
     * @return the countryCode
     */
    public String getCountryCode()
    {
        return countryCode;
    }

    /**
     * @param countryCode the countryCode to set
     */
    public void setCountryCode(String countryCode)
    {
        this.countryCode = countryCode;
    }

    /**
     * @return the stateCode
     */
    public String getStateCode()
    {
        return stateCode;
    }

    /**
     * @param stateCode the stateCode to set
     */
    public void setStateCode(String stateCode)
    {
        this.stateCode = stateCode;
    }

    /**
     * @return the typeId
     */
    public Integer getTypeId()
    {
        return typeId;
    }

    /**
     * @param typeId the typeId to set
     */
    public void setTypeId(Integer typeId)
    {
        this.typeId = typeId;
    }

    /**
     * @return the activeFlag
     */
    public String getActiveFlag()
    {
        return activeFlag;
    }

    /**
     * @param activeFlag the activeFlag to set
     */
    public void setActiveFlag(String activeFlag)
    {
        this.activeFlag = activeFlag;
    }

    /**
     * @return the altName
     */
    public String getAltName()
    {
        return altName;
    }

    /**
     * @param altName the altName to set
     */
    public void setAltName(String altName)
    {
        this.altName = altName;
    }

    /**
     * @return the salesTerm
     */
    public String getSalesTerm()
    {
        return salesTerm;
    }

    /**
     * @param salesTerm the salesTerm to set
     */
    public void setSalesTerm(String salesTerm)
    {
        this.salesTerm = salesTerm;
    }

    /**
     * @return the creditTerm
     */
    public String getCreditTerm()
    {
        return creditTerm;
    }

    /**
     * @param creditTerm the creditTerm to set
     */
    public void setCreditTerm(String creditTerm)
    {
        this.creditTerm = creditTerm;
    }

    /**
     * @return the status
     */
    public String getStatus()
    {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status)
    {
        this.status = status;
    }

    /**
     * @return the appUserName
     */
    public String getAppUserName()
    {
        return appUserName;
    }

    /**
     * @param appUserName the appUserName to set
     */
    public void setAppUserName(String appUserName)
    {
        this.appUserName = appUserName;
    }

    /**
     * @return the emailAddress
     */
    public String getEmailAddress()
    {
        return emailAddress;
    }

    /**
     * @param emailAddress the emailAddress to set
     */
    public void setEmailAddress(String emailAddress)
    {
        this.emailAddress = emailAddress;
    }

    /**
     * @return the subdomain
     */
    public String getSubdomain()
    {
        return subdomain;
    }

    /**
     * @param subdomain the subdomain to set
     */
    public void setSubdomain(String subdomain)
    {
        this.subdomain = subdomain;
    }

    
}
