package com.usatech.dms.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class TransactionProcessingDetail implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5203123074156109778L;
	
	private Long id;
	private Long tranId;
	private String typeDesc;
	private String stateName;
	private String parsedAcctData;
	private String acctEntryMethodDesc;
	private BigDecimal amt;
	private Date authDate;
	private String resultDesc;
	private String respCd;
	private String respDesc;
	private String authorityTranCd;
	private String authorityRefCd;
	private Date authorityDate;
	private Date createdDate;
	private Date lastUpdatedDate;
	private String createdBy;
	private String lastUpdatedBy;
	private Long terminalBatchId;
	private BigDecimal amtApproved;
	private String authorityMiscData;
	private BigDecimal authorityAmtRqst;
	private BigDecimal authorityAmtRcvd;
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the tranId
	 */
	public Long getTranId() {
		return tranId;
	}
	/**
	 * @param tranId the tranId to set
	 */
	public void setTranId(Long tranId) {
		this.tranId = tranId;
	}
	/**
	 * @return the typeDesc
	 */
	public String getTypeDesc() {
		return typeDesc;
	}
	/**
	 * @param typeDesc the typeDesc to set
	 */
	public void setTypeDesc(String typeDesc) {
		this.typeDesc = typeDesc;
	}
	/**
	 * @return the stateName
	 */
	public String getStateName() {
		return stateName;
	}
	/**
	 * @param stateName the stateName to set
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	/**
	 * @return the parsedAcctData
	 */
	public String getParsedAcctData() {
		return parsedAcctData;
	}
	/**
	 * @param parsedAcctData the parsedAcctData to set
	 */
	public void setParsedAcctData(String parsedAcctData) {
		this.parsedAcctData = parsedAcctData;
	}
	/**
	 * @return the acctEntryMethodDesc
	 */
	public String getAcctEntryMethodDesc() {
		return acctEntryMethodDesc;
	}
	/**
	 * @param acctEntryMethodDesc the acctEntryMethodDesc to set
	 */
	public void setAcctEntryMethodDesc(String acctEntryMethodDesc) {
		this.acctEntryMethodDesc = acctEntryMethodDesc;
	}
	/**
	 * @return the amt
	 */
	public BigDecimal getAmt() {
		return amt;
	}
	/**
	 * @param amt the amt to set
	 */
	public void setAmt(BigDecimal amt) {
		this.amt = amt;
	}
	/**
	 * @return the authDate
	 */
	public Date getAuthDate() {
		return authDate;
	}
	/**
	 * @param authDate the authDate to set
	 */
	public void setAuthDate(Date authDate) {
		this.authDate = authDate;
	}
	/**
	 * @return the resultDesc
	 */
	public String getResultDesc() {
		return resultDesc;
	}
	/**
	 * @param resultDesc the resultDesc to set
	 */
	public void setResultDesc(String resultDesc) {
		this.resultDesc = resultDesc;
	}
	/**
	 * @return the respCd
	 */
	public String getRespCd() {
		return respCd;
	}
	/**
	 * @param respCd the respCd to set
	 */
	public void setRespCd(String respCd) {
		this.respCd = respCd;
	}
	/**
	 * @return the respDesc
	 */
	public String getRespDesc() {
		return respDesc;
	}
	/**
	 * @param respDesc the respDesc to set
	 */
	public void setRespDesc(String respDesc) {
		this.respDesc = respDesc;
	}
	/**
	 * @return the authorityTranCd
	 */
	public String getAuthorityTranCd() {
		return authorityTranCd;
	}
	/**
	 * @param authorityTranCd the authorityTranCd to set
	 */
	public void setAuthorityTranCd(String authorityTranCd) {
		this.authorityTranCd = authorityTranCd;
	}
	/**
	 * @return the authorityRefCd
	 */
	public String getAuthorityRefCd() {
		return authorityRefCd;
	}
	/**
	 * @param authorityRefCd the authorityRefCd to set
	 */
	public void setAuthorityRefCd(String authorityRefCd) {
		this.authorityRefCd = authorityRefCd;
	}
	/**
	 * @return the authorityDate
	 */
	public Date getAuthorityDate() {
		return authorityDate;
	}
	/**
	 * @param authorityDate the authorityDate to set
	 */
	public void setAuthorityDate(Date authorityDate) {
		this.authorityDate = authorityDate;
	}
	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}
	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	/**
	 * @return the lastUpdatedDate
	 */
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	/**
	 * @param lastUpdatedDate the lastUpdatedDate to set
	 */
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the lastUpdatedBy
	 */
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	/**
	 * @param lastUpdatedBy the lastUpdatedBy to set
	 */
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	/**
	 * @return the terminalBatchId
	 */
	public Long getTerminalBatchId() {
		return terminalBatchId;
	}
	/**
	 * @param terminalBatchId the terminalBatchId to set
	 */
	public void setTerminalBatchId(Long terminalBatchId) {
		this.terminalBatchId = terminalBatchId;
	}
	/**
	 * @return the amtApproved
	 */
	public BigDecimal getAmtApproved() {
		return amtApproved;
	}
	/**
	 * @param amtApproved the amtApproved to set
	 */
	public void setAmtApproved(BigDecimal amtApproved) {
		this.amtApproved = amtApproved;
	}
	/**
	 * @return the authorityMiscData
	 */
	public String getAuthorityMiscData() {
		return authorityMiscData;
	}
	/**
	 * @param authorityMiscData the authorityMiscData to set
	 */
	public void setAuthorityMiscData(String authorityMiscData) {
		this.authorityMiscData = authorityMiscData;
	}
	/**
	 * @return the authorityAmtRqst
	 */
	public BigDecimal getAuthorityAmtRqst() {
		return authorityAmtRqst;
	}
	/**
	 * @param authorityAmtRqst the authorityAmtRqst to set
	 */
	public void setAuthorityAmtRqst(BigDecimal authorityAmtRqst) {
		this.authorityAmtRqst = authorityAmtRqst;
	}
	/**
	 * @return the authorityAmtRcvd
	 */
	public BigDecimal getAuthorityAmtRcvd() {
		return authorityAmtRcvd;
	}
	/**
	 * @param authorityAmtRcvd the authorityAmtRcvd to set
	 */
	public void setAuthorityAmtRcvd(BigDecimal authorityAmtRcvd) {
		this.authorityAmtRcvd = authorityAmtRcvd;
	}
	
}
