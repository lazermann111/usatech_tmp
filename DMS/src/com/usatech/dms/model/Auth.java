/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 * Note: for numeric values in the db, if they are nullable, you may want to type them
 * in the model classes here as String, in order for BeanUtils methods to convert properly during
 * copying of properties.
 * 
 * Mods:
 * Date			Author		Bug#			Description
 * ---------------------------------------------------------------------------------
 */
package com.usatech.dms.model;

import java.util.Date;

import com.usatech.layers.common.model.Device;
import com.usatech.layers.common.model.Location;

public class Auth {
	    
    private Long 	authId;
    private String  authTypeCode;
    private Long  authStateId;
    private String  authParsedAcctData;
    private String  acctEntryMethodCode;
    private String  authAmt;
    private Date   	authDate;
    private String 	authResultCode;
    private String 	authRespCode;
    private String 	authRespDesc;
    private String 	authAuthorityTranCd;
    private String 	authAuthorityRefCd;
    private Date   	authorityDate;
    private String 	createdBy;
    private Date   	createdDate;
    private String 	lastUpdatedBy;
    private Date   	lastUpdatedDate;
    private String 	terminalBatchId;
    private String  authAmtApproved;
    private String  authMiscData;
    private String  authAmtRqst;
    private String  authAmtRcvd;
    private String  traceNumber;
    private String  authBalanceAmt;
    private String 	authActionId;
    private String 	authActionBitmap;
    private String 	previousAuthStateId;
    private char 	authHoldUsed;
    private Long	locationId;
    private Long	deviceId;
    private Location location;
    private Device device;
    
    
    
	/**
	 * @return the locationId
	 */
	public Long getLocationId() {
		return locationId;
	}
	/**
	 * @param locationId the locationId to set
	 */
	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}
	/**
	 * @return the deviceId
	 */
	public Long getDeviceId() {
		return deviceId;
	}
	/**
	 * @param deviceId the deviceId to set
	 */
	public void setDeviceId(Long deviceId) {
		this.deviceId = deviceId;
	}
	/**
	 * @return the location
	 */
	public Location getLocation() {
		return location;
	}
	/**
	 * @param location the location to set
	 */
	public void setLocation(Location location) {
		this.location = location;
	}
	/**
	 * @return the device
	 */
	public Device getDevice() {
		return device;
	}
	/**
	 * @param device the device to set
	 */
	public void setDevice(Device device) {
		this.device = device;
	}
	/**
	 * @return the authId
	 */
	public Long getAuthId() {
		return authId;
	}
	/**
	 * @param authId the authId to set
	 */
	public void setAuthId(Long authId) {
		this.authId = authId;
	}
	/**
	 * @return the authTypeCode
	 */
	public String getAuthTypeCode() {
		return authTypeCode;
	}
	/**
	 * @param authTypeCode the authTypeCode to set
	 */
	public void setAuthTypeCode(String authTypeCode) {
		this.authTypeCode = authTypeCode;
	}
	/**
	 * @return the authStateId
	 */
	public Long getAuthStateId() {
		return authStateId;
	}
	/**
	 * @param authStateId the authStateId to set
	 */
	public void setAuthStateId(Long authStateId) {
		this.authStateId = authStateId;
	}
	/**
	 * @return the authParsedAcctData
	 */
	public String getAuthParsedAcctData() {
		return authParsedAcctData;
	}
	/**
	 * @param authParsedAcctData the authParsedAcctData to set
	 */
	public void setAuthParsedAcctData(String authParsedAcctData) {
		this.authParsedAcctData = authParsedAcctData;
	}
	/**
	 * @return the acctEntryMethodCode
	 */
	public String getAcctEntryMethodCode() {
		return acctEntryMethodCode;
	}
	/**
	 * @param acctEntryMethodCode the acctEntryMethodCode to set
	 */
	public void setAcctEntryMethodCode(String acctEntryMethodCode) {
		this.acctEntryMethodCode = acctEntryMethodCode;
	}
	/**
	 * @return the authAmt
	 */
	public String getAuthAmt() {
		return authAmt;
	}
	/**
	 * @param authAmt the authAmt to set
	 */
	public void setAuthAmt(String authAmt) {
		this.authAmt = authAmt;
	}
	/**
	 * @return the authDate
	 */
	public Date getAuthDate() {
		return authDate;
	}
	/**
	 * @param authDate the authDate to set
	 */
	public void setAuthDate(Date authDate) {
		this.authDate = authDate;
	}
	/**
	 * @return the authResultCode
	 */
	public String getAuthResultCode() {
		return authResultCode;
	}
	/**
	 * @param authResultCode the authResultCode to set
	 */
	public void setAuthResultCode(String authResultCode) {
		this.authResultCode = authResultCode;
	}
	/**
	 * @return the authRespCode
	 */
	public String getAuthRespCode() {
		return authRespCode;
	}
	/**
	 * @param authRespCode the authRespCode to set
	 */
	public void setAuthRespCode(String authRespCode) {
		this.authRespCode = authRespCode;
	}
	/**
	 * @return the authRespDesc
	 */
	public String getAuthRespDesc() {
		return authRespDesc;
	}
	/**
	 * @param authRespDesc the authRespDesc to set
	 */
	public void setAuthRespDesc(String authRespDesc) {
		this.authRespDesc = authRespDesc;
	}
	/**
	 * @return the authAuthorityTranCd
	 */
	public String getAuthAuthorityTranCd() {
		return authAuthorityTranCd;
	}
	/**
	 * @param authAuthorityTranCd the authAuthorityTranCd to set
	 */
	public void setAuthAuthorityTranCd(String authAuthorityTranCd) {
		this.authAuthorityTranCd = authAuthorityTranCd;
	}
	/**
	 * @return the authAuthorityRefCd
	 */
	public String getAuthAuthorityRefCd() {
		return authAuthorityRefCd;
	}
	/**
	 * @param authAuthorityRefCd the authAuthorityRefCd to set
	 */
	public void setAuthAuthorityRefCd(String authAuthorityRefCd) {
		this.authAuthorityRefCd = authAuthorityRefCd;
	}
	/**
	 * @return the authorityDate
	 */
	public Date getAuthorityDate() {
		return authorityDate;
	}
	/**
	 * @param authorityDate the authorityDate to set
	 */
	public void setAuthorityDate(Date authorityDate) {
		this.authorityDate = authorityDate;
	}
	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}
	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	/**
	 * @return the lastUpdatedBy
	 */
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	/**
	 * @param lastUpdatedBy the lastUpdatedBy to set
	 */
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	/**
	 * @return the lastUpdatedDate
	 */
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	/**
	 * @param lastUpdatedDate the lastUpdatedDate to set
	 */
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	/**
	 * @return the terminalBatchId
	 */
	public String getTerminalBatchId() {
		return terminalBatchId;
	}
	/**
	 * @param terminalBatchId the terminalBatchId to set
	 */
	public void setTerminalBatchId(String terminalBatchId) {
		this.terminalBatchId = terminalBatchId;
	}
	/**
	 * @return the authAmtApproved
	 */
	public String getAuthAmtApproved() {
		return authAmtApproved;
	}
	/**
	 * @param authAmtApproved the authAmtApproved to set
	 */
	public void setAuthAmtApproved(String authAmtApproved) {
		this.authAmtApproved = authAmtApproved;
	}
	/**
	 * @return the authMiscData
	 */
	public String getAuthMiscData() {
		return authMiscData;
	}
	/**
	 * @param authMiscData the authMiscData to set
	 */
	public void setAuthMiscData(String authMiscData) {
		this.authMiscData = authMiscData;
	}
	/**
	 * @return the authAmtRqst
	 */
	public String getAuthAmtRqst() {
		return authAmtRqst;
	}
	/**
	 * @param authAmtRqst the authAmtRqst to set
	 */
	public void setAuthAmtRqst(String authAmtRqst) {
		this.authAmtRqst = authAmtRqst;
	}
	/**
	 * @return the authAmtRcvd
	 */
	public String getAuthAmtRcvd() {
		return authAmtRcvd;
	}
	/**
	 * @param authAmtRcvd the authAmtRcvd to set
	 */
	public void setAuthAmtRcvd(String authAmtRcvd) {
		this.authAmtRcvd = authAmtRcvd;
	}
	/**
	 * @return the traceNumber
	 */
	public String getTraceNumber() {
		return traceNumber;
	}
	/**
	 * @param traceNumber the traceNumber to set
	 */
	public void setTraceNumber(String traceNumber) {
		this.traceNumber = traceNumber;
	}
	/**
	 * @return the authBalanceAmt
	 */
	public String getAuthBalanceAmt() {
		return authBalanceAmt;
	}
	/**
	 * @param authBalanceAmt the authBalanceAmt to set
	 */
	public void setAuthBalanceAmt(String authBalanceAmt) {
		this.authBalanceAmt = authBalanceAmt;
	}
	/**
	 * @return the authActionId
	 */
	public String getAuthActionId() {
		return authActionId;
	}
	/**
	 * @param authActionId the authActionId to set
	 */
	public void setAuthActionId(String authActionId) {
		this.authActionId = authActionId;
	}
	/**
	 * @return the authActionBitmap
	 */
	public String getAuthActionBitmap() {
		return authActionBitmap;
	}
	/**
	 * @param authActionBitmap the authActionBitmap to set
	 */
	public void setAuthActionBitmap(String authActionBitmap) {
		this.authActionBitmap = authActionBitmap;
	}
	/**
	 * @return the previousAuthStateId
	 */
	public String getPreviousAuthStateId() {
		return previousAuthStateId;
	}
	/**
	 * @param previousAuthStateId the previousAuthStateId to set
	 */
	public void setPreviousAuthStateId(String previousAuthStateId) {
		this.previousAuthStateId = previousAuthStateId;
	}
	/**
	 * @return the authHoldUsed
	 */
	public char getAuthHoldUsed() {
		return authHoldUsed;
	}
	/**
	 * @param authHoldUsed the authHoldUsed to set
	 */
	public void setAuthHoldUsed(char authHoldUsed) {
		this.authHoldUsed = authHoldUsed;
	}
    
    

}
