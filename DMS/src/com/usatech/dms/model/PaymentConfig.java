/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.usatech.dms.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Model class represents the Payment Config.
 * 
 */
public class PaymentConfig implements Serializable
{
    private static final long serialVersionUID = -1912502333008495284L;

    private Integer paymentSubtypeKeyId;
    private String paymentSubtypeName;
    private Date deactivationTs;
    private Date activationTs;
    private String statusTxtDisabled;
    private String statusTxtEnabled;
    private String paymentSubtypeTableName;
    private String paymentSubtypeKeyName;
    private String paymentEntryMethodDesc;
    private String paymentEntryMethodCd;
    private String merchantDesc;
    private String prefAuthAmount;

    /**
     * @return the paymentSubtypeKeyId
     */
    public Integer getPaymentSubtypeKeyId()
    {
        return paymentSubtypeKeyId;
    }

    /**
     * @param paymentSubtypeKeyId the paymentSubtypeKeyId to set
     */
    public void setPaymentSubtypeKeyId(Integer paymentSubtypeKeyId)
    {
        this.paymentSubtypeKeyId = paymentSubtypeKeyId;
    }

    /**
     * @return the paymentSubtypeName
     */
    public String getPaymentSubtypeName()
    {
        return paymentSubtypeName;
    }

    /**
     * @param paymentSubtypeName the paymentSubtypeName to set
     */
    public void setPaymentSubtypeName(String paymentSubtypeName)
    {
        this.paymentSubtypeName = paymentSubtypeName;
    }

    /**
     * @return the deactivationTs
     */
    public Date getDeactivationTs()
    {
        return deactivationTs;
    }

    /**
     * @param deactivationTs the deactivationTs to set
     */
    public void setDeactivationTs(Date deactivationTs)
    {
        this.deactivationTs = deactivationTs;
    }

    /**
     * @return the statusTxtDisabled
     */
    public String getStatusTxtDisabled()
    {
        return statusTxtDisabled;
    }

    /**
     * @param statusTxtDisabled the statusTxtDisabled to set
     */
    public void setStatusTxtDisabled(String statusTxtDisabled)
    {
        this.statusTxtDisabled = statusTxtDisabled;
    }

    /**
     * @return the activationTs
     */
    public Date getActivationTs()
    {
        return activationTs;
    }

    /**
     * @param activationTs the activationTs to set
     */
    public void setActivationTs(Date activationTs)
    {
        this.activationTs = activationTs;
    }

    /**
     * @return the statusTxtEnabled
     */
    public String getStatusTxtEnabled()
    {
        return statusTxtEnabled;
    }

    /**
     * @param statusTxtEnabled the statusTxtEnabled to set
     */
    public void setStatusTxtEnabled(String statusTxtEnabled)
    {
        this.statusTxtEnabled = statusTxtEnabled;
    }

    /**
     * @return the paymentSubtypeTableName
     */
    public String getPaymentSubtypeTableName()
    {
        return paymentSubtypeTableName;
    }

    /**
     * @param paymentSubtypeTableName the paymentSubtypeTableName to set
     */
    public void setPaymentSubtypeTableName(String paymentSubtypeTableName)
    {
        this.paymentSubtypeTableName = paymentSubtypeTableName;
    }

    /**
     * @return the paymentEntryMethodDesc
     */
    public String getPaymentEntryMethodDesc()
    {
        return paymentEntryMethodDesc;
    }

    /**
     * @param paymentEntryMethodDesc the paymentEntryMethodDesc to set
     */
    public void setPaymentEntryMethodDesc(String paymentEntryMethodDesc)
    {
        this.paymentEntryMethodDesc = paymentEntryMethodDesc;
    }

    /**
     * @return the paymentEntryMethodCd
     */
    public String getPaymentEntryMethodCd()
    {
        return paymentEntryMethodCd;
    }

    /**
     * @param paymentEntryMethodCd the paymentEntryMethodCd to set
     */
    public void setPaymentEntryMethodCd(String paymentEntryMethodCd)
    {
        this.paymentEntryMethodCd = paymentEntryMethodCd;
    }

    /**
     * @return the paymentSubtypeKeyName
     */
    public String getPaymentSubtypeKeyName()
    {
        return paymentSubtypeKeyName;
    }

    /**
     * @param paymentSubtypeKeyName the paymentSubtypeKeyName to set
     */
    public void setPaymentSubtypeKeyName(String paymentSubtypeKeyName)
    {
        this.paymentSubtypeKeyName = paymentSubtypeKeyName;
    }

    /**
     * @return the merchantDesc
     */
    public String getMerchantDesc()
    {
        return merchantDesc;
    }

    /**
     * @param merchantDesc the merchantDesc to set
     */
    public void setMerchantDesc(String merchantDesc)
    {
        this.merchantDesc = merchantDesc;
    }

    /**
     * @return the prefAuthAmount
     */
    public String getPrefAuthAmount()
    {
        return prefAuthAmount;
    }

    /**
     * @param prefAuthAmount the prefAuthAmount to set
     */
    public void setPrefAuthAmount(String prefAuthAmount)
    {
        this.prefAuthAmount = prefAuthAmount;
    }

}
