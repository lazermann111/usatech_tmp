/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.usatech.dms.model;

import java.io.Serializable;

/**
 * Model class represents the Payment Schedule.
 */
public class PaymentSchedule implements Serializable
{
    private static final long serialVersionUID = 6717502974418036418L;

    private Long id;
    private Long months;
    private Long days;
    private Long offsetHours;
    private String description;
    private String interval;
    private boolean selectable;

    /**
     * @return the id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     * @return the months
     */
    public Long getMonths()
    {
        return months;
    }

    /**
     * @param months the months to set
     */
    public void setMonths(Long months)
    {
        this.months = months;
    }

    /**
     * @return the days
     */
    public Long getDays()
    {
        return days;
    }

    /**
     * @param days the days to set
     */
    public void setDays(Long days)
    {
        this.days = days;
    }

    /**
     * @return the offsetHours
     */
    public Long getOffsetHours()
    {
        return offsetHours;
    }

    /**
     * @param offsetHours the offsetHours to set
     */
    public void setOffsetHours(Long offsetHours)
    {
        this.offsetHours = offsetHours;
    }

    /**
     * @return the description
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description)
    {
        this.description = description;
    }

    /**
     * @return the interval
     */
    public String getInterval()
    {
        return interval;
    }

    /**
     * @param interval the interval to set
     */
    public void setInterval(String interval)
    {
        this.interval = interval;
    }

    /**
     * @return the selectable
     */
    public boolean isSelectable()
    {
        return selectable;
    }

    /**
     * @param selectable the selectable to set
     */
    public void setSelectable(boolean selectable)
    {
        this.selectable = selectable;
    }

}
