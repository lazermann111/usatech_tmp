/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * 
 * Mods:
 * Date			Author		Bug#			Description
 * ---------------------------------------------------------------------------------
 * 2009-12-03	ecooney		DMS migration	change name of consumerAcctIssueId to consumerAcctIssueCd.
 */
package com.usatech.dms.model;

import java.io.Serializable;
import java.util.List;

public class ConsumerAccount implements Serializable {
	private static final long serialVersionUID = -8712425738508896498L;
	
	private Long 	consumerAcctId;
    private String 	consumerAcctCd;
    private String  consumerAcctBalance;
    private Long 	locationId;
    private String 	locationName;
    private String 	consumerAcctActiveFlag;
    private Long 	consumerAcctFmtId;
    private String 	consumerAcctFmtName;
    private Long 	consumerAcctTypeId;
    private String 	consumerAcctTypeDesc;
    private Long  	consumerId;
    private String	consumerEmailAddr1;
    private String 	createdBy;
    private String  createdDate;
    private String 	lastUpdatedBy;
    private String  lastUpdatedDate;
    private String 	consumerAcctConfCd;
    private Long 	paymentSubtypeId;
    private Long 	consumerAcctIssueNum;
    private String  consumerAcctActivationDate;
    private String  consumerAcctDeactivationDate;
    private String 	currencyCd;
    private String 	consumerAcctBalrefAmt;
    private Long 	consumerAcctBalrefPeriodHr;
    private String  consumerAcctLastBalrefDate;
    private String 	consumerAcctValidationCd;
    private String 	consumerAcctIssueCd;
    private Long	corpCustomerId;
    private String	corpCustomerName;
    private String	consumerAcctPromoTotal;
    private String	loyaltyDiscountTotal;
    private String	consumerAcctReplenishTotal;
    private Long	consumerAcctIdentifier;
    private Long	loyaltyPointsBalance;
    private Long 	loyaltyPointsTotal;
    private Long 	consumerAcctSubTypeId;
    private String 	consumerAcctSubTypeDesc;
    private List<Hold> 	holds;
    private List<PermissionAction> 	permissionActions;
    private String consumerAcctPromoBalance;
    private String consumerAcctReplenishBalance;
    private String authHoldInd;
    private String allowNegativeBalance;
    private String closeDate;
    private String topUpAmt;
    private Boolean operatorServiced;
    private Boolean usatServiced;
    private Boolean moreEnabled;
    private Boolean usaliveEnabled;
    private Boolean usatGenerated;
    private String	purchaseDiscountTotal;
    
    public String getPurchaseDiscountTotal() {
			return purchaseDiscountTotal;
		}
		public void setPurchaseDiscountTotal(String purchaseDiscountTotal) {
			this.purchaseDiscountTotal = purchaseDiscountTotal;
		}
		/**
	 * @return the consumerAcctIssueCd
	 */
	public String getConsumerAcctIssueCd() {
		return consumerAcctIssueCd;
	}
	/**
	 * @param consumerAcctIssueCd the consumerAcctIssueCd to set
	 */
	public void setConsumerAcctIssueCd(String consumerAcctIssueCd) {
		this.consumerAcctIssueCd = consumerAcctIssueCd;
	}
	/**
	 * @return the permissionAction
	 */
	public List<PermissionAction> getPermissionActions() {
		return permissionActions;
	}
	/**
	 * @param permissionAction the permissionAction to set
	 */
	public void setPermissionActions(List<PermissionAction> permissionActions) {
		this.permissionActions = permissionActions;
	}
	/**
	 * @return the holds
	 */
	public List<Hold> getHolds() {
		return holds;
	}
	/**
	 * @param holds the holds to set
	 */
	public void setHolds(List<Hold> holds) {
		this.holds = holds;
	}
	/**
	 * @return the consumerAcctId
	 */
	public Long getConsumerAcctId() {
		return consumerAcctId;
	}
	/**
	 * @param consumerAcctId the consumerAcctId to set
	 */
	public void setConsumerAcctId(Long consumerAcctId) {
		this.consumerAcctId = consumerAcctId;
	}
	/**
	 * @return the consumerAcctCd
	 */
	public String getConsumerAcctCd() {
		return consumerAcctCd;
	}
	/**
	 * @param consumerAcctCd the consumerAcctCd to set
	 */
	public void setConsumerAcctCd(String consumerAcctCd) {
		this.consumerAcctCd = consumerAcctCd;
	}
	/**
	 * @return the consumerAcctBalance
	 */
	public String getConsumerAcctBalance() {
		return consumerAcctBalance;
	}
	/**
	 * @param consumerAcctBalance the consumerAcctBalance to set
	 */
	public void setConsumerAcctBalance(String consumerAcctBalance) {
		this.consumerAcctBalance = consumerAcctBalance;
	}
	/**
	 * @return the locationId
	 */
	public Long getLocationId() {
		return locationId;
	}
	/**
	 * @param locationId the locationId to set
	 */
	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}
	/**
	 * @return the locationName
	 */
	public String getLocationName() {
		return locationName;
	}
	/**
	 * @param locationName the locationName to set
	 */
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	/**
	 * @return the consumerAcctActiveFlag
	 */
	public String getConsumerAcctActiveFlag() {
		return consumerAcctActiveFlag;
	}
	/**
	 * @param consumerAcctActiveFlag the consumerAcctActiveFlag to set
	 */
	public void setConsumerAcctActiveFlag(String consumerAcctActiveFlag) {
		this.consumerAcctActiveFlag = consumerAcctActiveFlag;
	}
	/**
	 * @return the consumerAcctFmtId
	 */
	public Long getConsumerAcctFmtId() {
		return consumerAcctFmtId;
	}
	/**
	 * @param consumerAcctFmtId the consumerAcctFmtId to set
	 */
	public void setConsumerAcctFmtId(Long consumerAcctFmtId) {
		this.consumerAcctFmtId = consumerAcctFmtId;
	}
	/**
	 * @return the consumerAcctFmtName
	 */
	public String getConsumerAcctFmtName() {
		return consumerAcctFmtName;
	}
	/**
	 * @param consumerAcctFmtName the consumerAcctFmtName to set
	 */
	public void setConsumerAcctFmtName(String consumerAcctFmtName) {
		this.consumerAcctFmtName = consumerAcctFmtName;
	}
	/**
	 * @return the consumerId
	 */
	public Long getConsumerId() {
		return consumerId;
	}
	/**
	 * @param consumerId the consumerId to set
	 */
	public void setConsumerId(Long consumerId) {
		this.consumerId = consumerId;
	}
	public String getConsumerEmailAddr1() {
		return consumerEmailAddr1;
	}
	public void setConsumerEmailAddr1(String consumerEmailAddr1) {
		this.consumerEmailAddr1 = consumerEmailAddr1;
	}
	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the createdDate
	 */
	public String getCreatedDate() {
		return createdDate;
	}
	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	/**
	 * @return the lastUpdatedBy
	 */
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	/**
	 * @param lastUpdatedBy the lastUpdatedBy to set
	 */
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	/**
	 * @return the lastUpdatedDate
	 */
	public String getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	/**
	 * @param lastUpdatedDate the lastUpdatedDate to set
	 */
	public void setLastUpdatedDate(String lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	/**
	 * @return the consumerAcctConfCd
	 */
	public String getConsumerAcctConfCd() {
		return consumerAcctConfCd;
	}
	/**
	 * @param consumerAcctConfCd the consumerAcctConfCd to set
	 */
	public void setConsumerAcctConfCd(String consumerAcctConfCd) {
		this.consumerAcctConfCd = consumerAcctConfCd;
	}
	/**
	 * @return the paymentSubtypeId
	 */
	public Long getPaymentSubtypeId() {
		return paymentSubtypeId;
	}
	/**
	 * @param paymentSubtypeId the paymentSubtypeId to set
	 */
	public void setPaymentSubtypeId(Long paymentSubtypeId) {
		this.paymentSubtypeId = paymentSubtypeId;
	}
	/**
	 * @return the consumerAcctIssueNum
	 */
	public Long getConsumerAcctIssueNum() {
		return consumerAcctIssueNum;
	}
	/**
	 * @param consumerAcctIssueNum the consumerAcctIssueNum to set
	 */
	public void setConsumerAcctIssueNum(Long consumerAcctIssueNum) {
		this.consumerAcctIssueNum = consumerAcctIssueNum;
	}
	/**
	 * @return the consumerAcctActivationDate
	 */
	public String getConsumerAcctActivationDate() {
		return consumerAcctActivationDate;
	}
	/**
	 * @param consumerAcctActivationDate the consumerAcctActivationDate to set
	 */
	public void setConsumerAcctActivationDate(String consumerAcctActivationDate) {
		this.consumerAcctActivationDate = consumerAcctActivationDate;
	}
	/**
	 * @return the consumerAcctDeactivationDate
	 */
	public String getConsumerAcctDeactivationDate() {
		return consumerAcctDeactivationDate;
	}
	/**
	 * @param consumerAcctDeactivationDate the consumerAcctDeactivationDate to set
	 */
	public void setConsumerAcctDeactivationDate(String consumerAcctDeactivationDate) {
		this.consumerAcctDeactivationDate = consumerAcctDeactivationDate;
	}
	/**
	 * @return the currencyCd
	 */
	public String getCurrencyCd() {
		return currencyCd;
	}
	/**
	 * @param currencyCd the currencyCd to set
	 */
	public void setCurrencyCd(String currencyCd) {
		this.currencyCd = currencyCd;
	}
	/**
	 * @return the consumerAcctBalrefAmt
	 */
	public String getConsumerAcctBalrefAmt() {
		return consumerAcctBalrefAmt;
	}
	/**
	 * @param consumerAcctBalrefAmt the consumerAcctBalrefAmt to set
	 */
	public void setConsumerAcctBalrefAmt(String consumerAcctBalrefAmt) {
		this.consumerAcctBalrefAmt = consumerAcctBalrefAmt;
	}
	/**
	 * @return the consumerAcctBalrefPeriodHr
	 */
	public Long getConsumerAcctBalrefPeriodHr() {
		return consumerAcctBalrefPeriodHr;
	}
	/**
	 * @param consumerAcctBalrefPeriodHr the consumerAcctBalrefPeriodHr to set
	 */
	public void setConsumerAcctBalrefPeriodHr(Long consumerAcctBalrefPeriodHr) {
		this.consumerAcctBalrefPeriodHr = consumerAcctBalrefPeriodHr;
	}
	/**
	 * @return the consumerAcctLastBalrefDate
	 */
	public String getConsumerAcctLastBalrefDate() {
		return consumerAcctLastBalrefDate;
	}
	/**
	 * @param consumerAcctLastBalrefDate the consumerAcctLastBalrefDate to set
	 */
	public void setConsumerAcctLastBalrefDate(String  consumerAcctLastBalrefDate) {
		this.consumerAcctLastBalrefDate = consumerAcctLastBalrefDate;
	}
	/**
	 * @return the consumerAcctValidationCd
	 */
	public String getConsumerAcctValidationCd() {
		return consumerAcctValidationCd;
	}
	/**
	 * @param consumerAcctValidationCd the consumerAcctValidationCd to set
	 */
	public void setConsumerAcctValidationCd(String consumerAcctValidationCd) {
		this.consumerAcctValidationCd = consumerAcctValidationCd;
	}
	/**
	 * @return the consumerAcctIssueCd
	 */
	public String getconsumerAcctIssueCd() {
		return consumerAcctIssueCd;
	}
	/**
	 * @param consumerAcctIssueCd the consumerAcctIssueCd to set
	 */
	public void setconsumerAcctIssueCd(String consumerAcctIssueCd) {
		this.consumerAcctIssueCd = consumerAcctIssueCd;
	}
	public Long getConsumerAcctTypeId() {
		return consumerAcctTypeId;
	}
	public void setConsumerAcctTypeId(Long consumerAcctTypeId) {
		this.consumerAcctTypeId = consumerAcctTypeId;
	}
	public String getConsumerAcctTypeDesc() {
		return consumerAcctTypeDesc;
	}
	public void setConsumerAcctTypeDesc(String consumerAcctTypeDesc) {
		this.consumerAcctTypeDesc = consumerAcctTypeDesc;
	}
	public Long getCorpCustomerId() {
		return corpCustomerId;
	}
	public void setCorpCustomerId(Long corpCustomerId) {
		this.corpCustomerId = corpCustomerId;
	}
	public String getCorpCustomerName() {
		return corpCustomerName;
	}
	public void setCorpCustomerName(String corpCustomerName) {
		this.corpCustomerName = corpCustomerName;
	}
	public String getConsumerAcctPromoTotal() {
		return consumerAcctPromoTotal;
	}
	public void setConsumerAcctPromoTotal(String consumerAcctPromoTotal) {
		this.consumerAcctPromoTotal = consumerAcctPromoTotal;
	}
	public String getLoyaltyDiscountTotal() {
		return loyaltyDiscountTotal;
	}
	public void setLoyaltyDiscountTotal(String loyaltyDiscountTotal) {
		this.loyaltyDiscountTotal = loyaltyDiscountTotal;
	}
	public String getConsumerAcctReplenishTotal() {
		return consumerAcctReplenishTotal;
	}
	public void setConsumerAcctReplenishTotal(String consumerAcctReplenishTotal) {
		this.consumerAcctReplenishTotal = consumerAcctReplenishTotal;
	}
	public Long getConsumerAcctIdentifier() {
		return consumerAcctIdentifier;
	}
	public void setConsumerAcctIdentifier(Long consumerAcctIdentifier) {
		this.consumerAcctIdentifier = consumerAcctIdentifier;
	}
	public Long getLoyaltyPointsBalance() {
		return loyaltyPointsBalance;
	}
	public void setLoyaltyPointsBalance(Long loyaltyPointsBalance) {
		this.loyaltyPointsBalance = loyaltyPointsBalance;
	}
	public Long getLoyaltyPointsTotal() {
		return loyaltyPointsTotal;
	}
	public void setLoyaltyPointsTotal(Long loyaltyPointsTotal) {
		this.loyaltyPointsTotal = loyaltyPointsTotal;
	}
	public Long getConsumerAcctSubTypeId() {
		return consumerAcctSubTypeId;
	}
	public void setConsumerAcctSubTypeId(Long consumerAcctSubTypeId) {
		this.consumerAcctSubTypeId = consumerAcctSubTypeId;
	}
	public String getConsumerAcctSubTypeDesc() {
		return consumerAcctSubTypeDesc;
	}
	public void setConsumerAcctSubTypeDesc(String consumerAcctSubTypeDesc) {
		this.consumerAcctSubTypeDesc = consumerAcctSubTypeDesc;
	}
	public String getConsumerAcctPromoBalance() {
		return consumerAcctPromoBalance;
	}
	public void setConsumerAcctPromoBalance(String consumerAcctPromoBalance) {
		this.consumerAcctPromoBalance = consumerAcctPromoBalance;
	}
	public String getConsumerAcctReplenishBalance() {
		return consumerAcctReplenishBalance;
	}
	public void setConsumerAcctReplenishBalance(String consumerAcctReplenishBalance) {
		this.consumerAcctReplenishBalance = consumerAcctReplenishBalance;
	}
	public String getAuthHoldInd() {
		return authHoldInd;
	}
	public void setAuthHoldInd(String authHoldInd) {
		this.authHoldInd = authHoldInd;
	}
	public String getAllowNegativeBalance() {
		return allowNegativeBalance;
	}
	public void setAllowNegativeBalance(String allowNegativeBalance) {
		this.allowNegativeBalance = allowNegativeBalance;
	}
	public String getCloseDate() {
		return closeDate;
	}
	public void setCloseDate(String closeDate) {
		this.closeDate = closeDate;
	}
	public String getTopUpAmt() {
		return topUpAmt;
	}
	public void setTopUpAmt(String topUpAmt) {
		this.topUpAmt = topUpAmt;
	}
	public Boolean getOperatorServiced() {
		return operatorServiced == null ? false : operatorServiced;
	}
	public void setOperatorServiced(Boolean operatorServiced) {
		this.operatorServiced = operatorServiced;
	}
	public Boolean getUsatServiced() {
		return usatServiced == null ? false : usatServiced;
	}
	public void setUsatServiced(Boolean usatServiced) {
		this.usatServiced = usatServiced;
	}
	public Boolean getMoreEnabled() {
		return moreEnabled == null ? false : moreEnabled;
	}
	public void setMoreEnabled(Boolean moreEnabled) {
		this.moreEnabled = moreEnabled;
	}
	public Boolean getUsaliveEnabled() {
		return usaliveEnabled == null ? false : usaliveEnabled;
	}
	public void setUsaliveEnabled(Boolean usaliveEnabled) {
		this.usaliveEnabled = usaliveEnabled;
	}
	public Boolean getUsatGenerated() {
		return usatGenerated == null ? false : usatGenerated;
	}
	public void setUsatGenerated(Boolean usatGenerated) {
		this.usatGenerated = usatGenerated;
	}
	
	
}
