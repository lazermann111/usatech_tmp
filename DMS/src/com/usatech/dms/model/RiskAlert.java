/**
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.model;

import java.io.Serializable;
import java.util.Date;

public class RiskAlert implements Serializable, Comparable<RiskAlert> {

  private static final long serialVersionUID = 1;

  public static enum Status {
    N("New"),
    I("Investigating"),
    C("Closed-Confirmed"),
    D("Closed-Dismissed");

    private String name;

    private Status(String name) {
      this.name = name;
    }
    
    public String getName() {
      return name;
    }
  }

  private Long id;
  private Date alertTime;
  private Date lastUpdated;
  private int score;
  private Status status;
  private String message;
  private String notes;
  private long customerId;
  private long terminalId;
  
  public RiskAlert() {
    
  }
  
  public RiskAlert(Long id) {
    this.id = id;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Date getAlertTime() {
    return alertTime;
  }

  public void setAlertTime(Date alertTime) {
    this.alertTime = alertTime;
  }
  
  public Date getLastUpdated() {
    return lastUpdated;
  }
  
  public void setLastUpdated(Date lastUpdated) {
    this.lastUpdated = lastUpdated;
  }

  public int getScore() {
    return score;
  }

  public void setScore(int score) {
    this.score = score;
  }

  public Status getStatus() {
    return status;
  }

  public void setStatus(Status status) {
    this.status = status;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }
  
  public String getNotes() {
    return notes;
  }
  
  public void setNotes(String notes) {
    this.notes = notes;
  }
  
  public long getCustomerId() {
    return customerId;
  }
  
  public void setCustomerId(long customerId) {
    this.customerId = customerId;
  }
  
  public boolean hasCustomer() {
    return customerId > 0;
  }
  
  public long getTerminalId() {
    return terminalId;
  }
  
  public boolean hasTerminal() {
    return terminalId > 0;
  }
  
  public void setTerminalId(long terminalId) {
    this.terminalId = terminalId;
  }

  @Override
  public int compareTo(RiskAlert o) {
    if (score != o.score)
      return Integer.compare(score, o.score);
    return alertTime.compareTo(o.alertTime);
  }

}
