/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.model;

/**
 * Enumeration represents the DMS user privileges.
 */
public enum DmsPrivilege
{    
	DMS_BULK_CARD_CONFIGURATION_ADMINS("DMS Bulk Card Configuration Admins", "DMS_BULK_CARD_CONFIGURATION_ADMINS"),
    DMS_BULK_DEVICE_CONFIGURATION_ADMINS("DMS Bulk Device Configuration Admins", "DMS_BULK_DEVICE_CONFIGURATION_ADMINS"),
    DMS_CONFIG_TEMPLATE_ADMINS("DMS Config Template Admins", "DMS_CONFIG_TEMPLATE_ADMINS"),
    DMS_CONSUMER_ACCOUNT_ADMINS("DMS Consumer Account Admins", "DMS_CONSUMER_ACCOUNT_ADMINS"),
    DMS_CUSTOMER_ADMINS("DMS Customer Admins", "DMS_CUSTOMER_ADMINS"),
    DMS_CUSTOMER_SERVICE("DMS Customer Service", "DMS_CUSTOMER_SERVICE"),
    DMS_DEVICE_CLONING_ADMINS("DMS Device Cloning Admins", "DMS_DEVICE_CLONING_ADMINS"),
    DMS_DEVICE_PROFILE_ADMINS("DMS Device Profile Admins", "DMS_DEVICE_PROFILE_ADMINS"),
    DMS_EFT_ADMINS("DMS EFT Admins", "DMS_EFT_ADMINS"),
    DMS_ESUDS_WEB_PRIVILEGE_ADMINS("DMS eSuds Web Privilege Admins", "DMS_ESUDS_WEB_PRIVILEGE_ADMINS"),
    DMS_FILE_ADMINS("DMS File Admins", "DMS_FILE_ADMINS"),
    DMS_GPRS_WEB_ADMINS("DMS GPRS Web Admins", "DMS_GPRS_WEB_ADMINS"),
    DMS_LOCATION_ADMINS("DMS Location Admins", "DMS_LOCATION_ADMINS"),
    DMS_MENU_CONTENT_ADMINS("DMS Menu Content Admins", "DMS_MENU_CONTENT_ADMINS"),
    DMS_PAYMENT_ADMINS("DMS Payment Admins", "DMS_PAYMENT_ADMINS"),
    DMS_POSM_ADMINS("DMS POSM Admins", "DMS_POSM_ADMINS"),
    DMS_SERIAL_NUMBER_ALLOCATION_ADMINS("DMS Serial Number Allocation Admins", "DMS_SERIAL_NUMBER_ALLOCATION_ADMINS"),
    DMS_TERMINAL_ADMINS("DMS Terminal Admins", "DMS_TERMINAL_ADMINS"),
    DMS_SALES_ADMINS("DMS Sales Admins", "DMS_SALES_ADMINS"),
    DMS_ACH_ADMINS("DMS ACH Admins", "DMS_ACH_ADMINS"),
    DMS_USERS("DMS Users", "DMS_USERS");

    private String label;
    private String value;

    private DmsPrivilege(String label, String value)
    {
        this.label = label;
        this.value = value;
    }

    /**
     * Gets the DmsPrivilege whose label matches the given string. The label is not case sensitive.
     * @param label the give label
     * @return <code>null</code> if not found
     */
    public static DmsPrivilege forLabel(String label)
    {
        DmsPrivilege result = null;
        for (DmsPrivilege item : DmsPrivilege.values())
        {
            if (item.getLabel().equalsIgnoreCase(label))
            {
                result = item;
                break;
            }
        }
        return result;
    }

    /**
     * Gets the DmsPrivilege whose value matches the given value.
     * @param label the give value
     * @return <code>null</code> if not found
     */
    public static DmsPrivilege forValue(String value)
    {
        DmsPrivilege result = null;
        for (DmsPrivilege item : DmsPrivilege.values())
        {
            if (item.getValue().equals(value))
            {
                result = item;
                break;
            }
        }
        return result;
    }

    /**
     * @return the label of this privilege
     */
    public String getLabel()
    {
        return label;
    }

    /**
     * @return the value of this privilege
     */
    public String getValue()
    {
        return value;
    }
}
