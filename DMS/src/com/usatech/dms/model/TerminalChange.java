/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.usatech.dms.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Model class represents the Terminal Change.
 */
public class TerminalChange implements Serializable
{
    private static final long serialVersionUID = 6717502974418036418L;

    private String eportSerialNumber;
    private String attribute;
    private String oldValue;
    private String newValue;
    private Long terminalId;
    private Date updateDate;

    /**
     * @return the eportSerialNumber
     */
    public String getEportSerialNumber()
    {
        return eportSerialNumber;
    }

    /**
     * @param eportSerialNumber the eportSerialNumber to set
     */
    public void setEportSerialNumber(String eportSerialNumber)
    {
        this.eportSerialNumber = eportSerialNumber;
    }

    /**
     * @return the attribute
     */
    public String getAttribute()
    {
        return attribute;
    }

    /**
     * @param attribute the attribute to set
     */
    public void setAttribute(String attribute)
    {
        this.attribute = attribute;
    }

    /**
     * @return the oldValue
     */
    public String getOldValue()
    {
        return oldValue;
    }

    /**
     * @param oldValue the oldValue to set
     */
    public void setOldValue(String oldValue)
    {
        this.oldValue = oldValue;
    }

    /**
     * @return the newValue
     */
    public String getNewValue()
    {
        return newValue;
    }

    /**
     * @param newValue the newValue to set
     */
    public void setNewValue(String newValue)
    {
        this.newValue = newValue;
    }

    /**
     * @return the terminalId
     */
    public Long getTerminalId()
    {
        return terminalId;
    }

    /**
     * @param terminalId the terminalId to set
     */
    public void setTerminalId(Long terminalId)
    {
        this.terminalId = terminalId;
    }

    /**
     * @return the updateDate
     */
    public Date getUpdateDate()
    {
        return updateDate;
    }

    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(Date updateDate)
    {
        this.updateDate = updateDate;
    }

}
