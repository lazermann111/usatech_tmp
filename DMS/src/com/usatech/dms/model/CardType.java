package com.usatech.dms.model;

import java.io.Serializable;

public class CardType implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5099465658361263334L;
	
	public Long id;
	private String fmtRegex;
	private String fmtRegexBref;
	public String fmtName;
	public String fmtDesc;
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the fmtRetgex
	 */
	public String getFmtRetgex() {
		return fmtRegex;
	}
	/**
	 * @param fmtRegex the fmtRegex to set
	 */
	public void setFmtRegex(String fmtRegex) {
		this.fmtRegex = fmtRegex;
	}
	/**
	 * @return the fmtRegexBref
	 */
	public String getFmtRegexBref() {
		return fmtRegexBref;
	}
	/**
	 * @param fmtRegexBref the fmtRegexBref to set
	 */
	public void setFmtRegexBref(String fmtRegexBref) {
		this.fmtRegexBref = fmtRegexBref;
	}
	/**
	 * @return the fmtName
	 */
	public String getFmtName() {
		return fmtName;
	}
	/**
	 * @param fmtName the fmtName to set
	 */
	public void setFmtName(String fmtName) {
		this.fmtName = fmtName;
	}
	/**
	 * @return the fmtDesc
	 */
	public String getFmtDesc() {
		return fmtDesc;
	}
	/**
	 * @param fmtDesc the fmtDesc to set
	 */
	public void setFmtDesc(String fmtDesc) {
		this.fmtDesc = fmtDesc;
	}
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	

}
