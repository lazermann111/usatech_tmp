package com.usatech.dms.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class TransactionLineItem implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7214929528975017462L;
	
	private Long id;
	private BigDecimal amt;
	private BigDecimal tax;
	private String tranLineItemDesc;
	private Long typeId;
	private Long quantity;
	private Long hostId;
	private String batchTypeCd;
	private String typeDesc;
	private String batchTypeDesc;
	private BigDecimal totalAmt;
	private String positionCd;
	private Date lineItemDate;
	private String saleResultDesc;
	private String createdBy;
	private Date createdDate;
	private String lastUpdatedBy;
	private Date lastUpdatedDate;
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the amt
	 */
	public BigDecimal getAmt() {
		return amt;
	}
	/**
	 * @param amt the amt to set
	 */
	public void setAmt(BigDecimal amt) {
		this.amt = amt;
	}
	/**
	 * @return the tax
	 */
	public BigDecimal getTax() {
		return tax;
	}
	/**
	 * @param tax the tax to set
	 */
	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}
	/**
	 * @return the tranLineItemDesc
	 */
	public String getTranLineItemDesc() {
		return tranLineItemDesc;
	}
	/**
	 * @param tranLineItemDesc the tranLineItemDesc to set
	 */
	public void setTranLineItemDesc(String tranLineItemDesc) {
		this.tranLineItemDesc = tranLineItemDesc;
	}
	/**
	 * @return the typeId
	 */
	public Long getTypeId() {
		return typeId;
	}
	/**
	 * @param typeId the typeId to set
	 */
	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}
	/**
	 * @return the quantity
	 */
	public Long getQuantity() {
		return quantity;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}
	/**
	 * @return the hostId
	 */
	public Long getHostId() {
		return hostId;
	}
	/**
	 * @param hostId the hostId to set
	 */
	public void setHostId(Long hostId) {
		this.hostId = hostId;
	}
	/**
	 * @return the batchTypeCd
	 */
	public String getBatchTypeCd() {
		return batchTypeCd;
	}
	/**
	 * @param batchTypeCd the batchTypeCd to set
	 */
	public void setBatchTypeCd(String batchTypeCd) {
		this.batchTypeCd = batchTypeCd;
	}
	/**
	 * @return the typeDesc
	 */
	public String getTypeDesc() {
		return typeDesc;
	}
	/**
	 * @param typeDesc the typeDesc to set
	 */
	public void setTypeDesc(String typeDesc) {
		this.typeDesc = typeDesc;
	}
	/**
	 * @return the batchTypeDesc
	 */
	public String getBatchTypeDesc() {
		return batchTypeDesc;
	}
	/**
	 * @param batchTypeDesc the batchTypeDesc to set
	 */
	public void setBatchTypeDesc(String batchTypeDesc) {
		this.batchTypeDesc = batchTypeDesc;
	}
	/**
	 * @return the totalAmt
	 */
	public BigDecimal getTotalAmt() {
		return totalAmt;
	}
	/**
	 * @param totalAmt the totalAmt to set
	 */
	public void setTotalAmt(BigDecimal totalAmt) {
		this.totalAmt = totalAmt;
	}
	/**
	 * @return the positionCd
	 */
	public String getPositionCd() {
		return positionCd;
	}
	/**
	 * @param positionCd the positionCd to set
	 */
	public void setPositionCd(String positionCd) {
		this.positionCd = positionCd;
	}
	/**
	 * @return the lineItemDate
	 */
	public Date getLineItemDate() {
		return lineItemDate;
	}
	/**
	 * @param lineItemDate the lineItemDate to set
	 */
	public void setLineItemDate(Date lineItemDate) {
		this.lineItemDate = lineItemDate;
	}
	/**
	 * @return the saleResultDesc
	 */
	public String getSaleResultDesc() {
		return saleResultDesc;
	}
	/**
	 * @param saleResultDesc the saleResultDesc to set
	 */
	public void setSaleResultDesc(String saleResultDesc) {
		this.saleResultDesc = saleResultDesc;
	}
	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}
	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	/**
	 * @return the lastUpdatedBy
	 */
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	/**
	 * @param lastUpdatedBy the lastUpdatedBy to set
	 */
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	/**
	 * @return the lastUpdatedDate
	 */
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	/**
	 * @param lastUpdatedDate the lastUpdatedDate to set
	 */
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	
	
	
}
