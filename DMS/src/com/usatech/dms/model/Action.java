package com.usatech.dms.model;

import java.util.Date;
import java.util.List;

public class Action {
	private Long actionId;
	private String actionName;
	private String actionDesc;
	private Integer deviceTypeId;
	private String deviceTypeDesc;
	private String 	createdBy;
    private Date   	createdDate;
    private String 	lastUpdatedBy;
    private Date   	lastUpdatedDate;
    private String actionParamTypeCd;
    private String actionParamSize;
    private String actionClearParamCd;
    private List<ActionParam> actionParams;
    
    
	/**
	 * @return the actionParams
	 */
	public List<ActionParam> getActionParams() {
		return actionParams;
	}
	/**
	 * @param actionParams the actionParams to set
	 */
	public void setActionParams(List<ActionParam> actionParams) {
		this.actionParams = actionParams;
	}
	/**
	 * @return the actionId
	 */
	public Long getActionId() {
		return actionId;
	}
	/**
	 * @param actionId the actionId to set
	 */
	public void setActionId(Long actionId) {
		this.actionId = actionId;
	}
	/**
	 * @return the actionName
	 */
	public String getActionName() {
		return actionName;
	}
	/**
	 * @param actionName the actionName to set
	 */
	public void setActionName(String actionName) {
		this.actionName = actionName;
	}
	/**
	 * @return the actionDesc
	 */
	public String getActionDesc() {
		return actionDesc;
	}
	/**
	 * @param actionDesc the actionDesc to set
	 */
	public void setActionDesc(String actionDesc) {
		this.actionDesc = actionDesc;
	}
	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}
	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	/**
	 * @return the lastUpdatedBy
	 */
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	/**
	 * @param lastUpdatedBy the lastUpdatedBy to set
	 */
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	/**
	 * @return the lastUpdatedDate
	 */
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	/**
	 * @param lastUpdatedDate the lastUpdatedDate to set
	 */
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	/**
	 * @return the actionParamTypeCd
	 */
	public String getActionParamTypeCd() {
		return actionParamTypeCd;
	}
	/**
	 * @param actionParamTypeCd the actionParamTypeCd to set
	 */
	public void setActionParamTypeCd(String actionParamTypeCd) {
		this.actionParamTypeCd = actionParamTypeCd;
	}
	/**
	 * @return the actionParamSize
	 */
	public String getActionParamSize() {
		return actionParamSize;
	}
	/**
	 * @param actionParamSize the actionParamSize to set
	 */
	public void setActionParamSize(String actionParamSize) {
		this.actionParamSize = actionParamSize;
	}
	/**
	 * @return the actionClearParamCd
	 */
	public String getActionClearParamCd() {
		return actionClearParamCd;
	}
	/**
	 * @param actionClearParamCd the actionClearParamCd to set
	 */
	public void setActionClearParamCd(String actionClearParamCd) {
		this.actionClearParamCd = actionClearParamCd;
	}
    
	public Integer getDeviceTypeId() {
		return deviceTypeId;
	}
	
	public void setDeviceTypeId(Integer deviceTypeId) {
		this.deviceTypeId = deviceTypeId;
	}
	
	public String getDeviceTypeDesc() {
		return deviceTypeDesc;
	}
	
	public void setDeviceTypeDesc(String deviceTypeDesc) {
		this.deviceTypeDesc = deviceTypeDesc;
	}

}
