/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.user;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.servlet.BasicServletUser;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.ServletUser;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractLogonStep;
import simple.servlet.steps.LoginFailureException;

import com.usatech.dms.action.LdapLoginActions;
import com.usatech.dms.model.DmsPrivilege;
import com.usatech.dms.util.DMSConstants;

/**
 * This step will perform the user authentication and authorization.
 * 
 * @see simple.servlet.steps.AbstractLogonStep
 */
public class AuthenticationStep extends AbstractLogonStep {
	private static final simple.io.Log log = simple.io.Log.getLog();
	public static final Pattern LOGIN_FORWARD_REGEX = Pattern.compile("^/log(?:in|on|out)(?:\\.i)?$");
	public static final String PARAM_USERNAME = "userName";
	public static final String PARAM_CREDENTIAL = "credential";
	
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		if(isRequireSecureConnection() && !request.isSecure()) 
			throw new ServletException("A secure connection is required to log in to this system");
		ServletUser user;
		try {
			user = authenticateUser(form);
		} catch(ServletException e) {
			handleAuthenticationFailure(dispatcher, form, e);
			return;
		}
		int clientHeight = form.getInt("clientHeight", false, -1);
		int contentHeight = clientHeight < 650 ? 500 : clientHeight - 150;
		request.getSession().setAttribute("contentHeight", String.valueOf(contentHeight));
		request.getSession().setAttribute("remoteUser", user.getUserName());
		request.getSession().setAttribute(SimpleServlet.ATTRIBUTE_USER, user);
		if(RequestUtils.isXsrfProtectionEnabled(request))
			form.setAttribute(SimpleServlet.REQUEST_SESSION_TOKEN, RequestUtils.getSessionToken(request));
		String forward = (String) form.getAttribute(ATTRIBUTE_FORWARD);
		if (forward == null || forward.length() == 0 || LOGIN_FORWARD_REGEX.matcher(forward).matches())
			forward = DMSConstants.HOME_PAGE;
		else if (!forward.contains("."))
			forward += ".i";
		if (log.isDebugEnabled())
			log.debug("Logon successful, redirecting to " + forward);
		form.setRedirectUri(forward);
	}

	@Override
	public ServletUser authenticateUser(InputForm form) throws ServletException {
		String username = form.getString(PARAM_USERNAME, false);
		String password = form.getString(PARAM_CREDENTIAL, false);

		// if username/password is not found, set error message and return.
		if(username == null || username.trim().equals("") || password == null || password.trim().equals("")) {
			throw new LoginFailureException("Please enter the username/password and try again.", LoginFailureException.INVALID_USERNAME);
		}
		log.info("Authenticating user " + username);

		BasicServletUser user = LdapLoginActions.authenticate(username, password);
		convertPrivileges(user);
		
		// if the user is not a member of "DMS Users", login failed.
		if(!user.hasPrivilege(DmsPrivilege.DMS_USERS.getValue()))
			throw new LoginFailureException("Authorization Failure!", LoginFailureException.INVALID_CREDENTIALS);
		
		user.setEmailAddress(username + "@usatech.com");
		log.info("User " + user.getUserName() + " was successfully authenticated");

		// set the menu visibilities for the logged on user
		form.setAttribute(DmsMenuManager.ATTRIBUTE_MENU_MANAGER, new DmsMenuManager(user), RequestUtils.SESSION_SCOPE);
		if (log.isDebugEnabled())
			log.debug("      - storing MenuManager in '" + DmsMenuManager.ATTRIBUTE_MENU_MANAGER + "'");

		return user;
	}

	public static void convertPrivileges(BasicServletUser user) {
		Set<String> groups = user.getPrivileges();
		Set<String> privileges = new HashSet<String>();
		for(String group : groups) {
			DmsPrivilege priv = DmsPrivilege.forLabel(group);
			if(priv != null)
				privileges.add(priv.getValue());
		}
		// 1. remove the string privilege name
		user.removePrivileges(groups.toArray(new String[groups.size()]));
		// 2. add the int privilege value
		user.addPrivileges(privileges.toArray(new String[privileges.size()]));
	}
}
