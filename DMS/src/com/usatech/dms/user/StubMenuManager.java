package com.usatech.dms.user;

/**
 * Stub menu manager, should be used for debugging only.
 */
public class StubMenuManager
{
    public static final String ATTRIBUTE_MENU_MANAGER = "com.usatech.dms.MenuManager";

    /* ====================================================================== */
    /* top menu items */
    /* ====================================================================== */
    /** top menu item "Device" -&gt; "EV Number" */
    private boolean topItemDeviceNumber = true;
    /** top menu "Device" -&gt; "View Devices" */
    private boolean topMenuDViewDevices = true;
    /** top menu item "Device" -&gt; "Advance Tools" -&gt; "Device Configuration Wizard" */
    private boolean topItemDATDeviceConfWizard = true;
    /** top menu item "Device" -&gt; "Advance Tools" -&gt; "Serial Number Allocations" */
    private boolean topItemDATSerialNumAlloc = true;
    /** top menu item "Device" -&gt; "Advance Tools" -&gt; "Device Cloning" */
    private boolean topItemDATDeviceCloning = true;

    /** top menu "Locations" */
    private boolean topMenuLocations = true;

    /** top menu item "Customers" -&gt; "Customer Name" */
    private boolean topItemCustName = true;
    /** top menu "Customers" -&gt; "Advance Tools" */
    private boolean topMenuCustAdTools = true;

    /** top menu item "Consumers" -&gt; "Card Search" */
    private boolean topItemConsCardSearch = true;
    /** top menu "Consumers" -&gt; "Card Creator Wizard" */
    private boolean topItemConsCardWizard = true;

    /** top menu "Files" */
    private boolean topMenuFiles = true;

    /* ====================================================================== */
    /* left menu items */
    /* ====================================================================== */
    /** left menu "Transactions" */
    private boolean leftMenuTransactions = true;

    /** left menu "Reports" */
    private boolean leftMenuReports = true;

    /** left menu item "Advance Tools" -&gt; "Access GPRS Web" */
    private boolean leftItemATGprsWeb = true;
    /** left menu item "Advance Tools" -&gt; "Configure various application Templates" */
    private boolean leftItemATConfigTemplates = true;
    /** left menu item "Advance Tools" -&gt; "Refresh Menu Content" */
    private boolean leftItemATRefreshMenu = true;

    /** left menu item "Bank Accounts" -&gt; "Bank Account New" */
    private boolean leftItemBANew = true;
    /** left menu item "Bank Accounts" -&gt; "Bank Account Edit" */
    private boolean leftItemBAEdit = true;

    /** left menu "Customer", "Dealer", "ePorts", "License Agreements" and "No License Agreement" */
    private boolean leftMenu4CustomerService = true;

    /** left menu item "EFT" -&gt; "Pending" */
    private boolean leftItemEftPending = true;
    /** left menu item "EFT" -&gt; "Approved" */
    private boolean leftItemEftApproved = true;

    /** left menu item "Terminals" -&gt; "Terminal New" and "Terminal Edit" */
    private boolean leftItemTNewEdit = true;
    /** left menu item "Terminals" -&gt; "Terminal Updated" */
    private boolean leftItemTUpdated = true;

    /**
     * Constructs a instance of this class, and cache all the menu items' visibilities base on the
     * logged on user, so that we don't want to calculate it every time, means generates once, use
     * for the whole session.
     * @param user the logged on user
     */
    public StubMenuManager()
    {
    }

    /**
     * @return <code>true</code> if the following top menu items are visible:<br/> "Devices" -&gt;
     *         "Device Number"<br/>"Devices" -&gt; "Serial Number".
     */
    public boolean showTopDeviceNumber()
    {
        return topItemDeviceNumber;
    }

    /**
     * @return <code>true</code> if the following top menu and its sub menu items are visible:<br/>
     *         "Devices" -&gt; "View Devices"
     */
    public boolean showTopDevicesView()
    {
        return topMenuDViewDevices;
    }

    /**
     * @return <code>true</code> if the following top menu item is visible:<br/> "Device" -&gt;
     *         "Advance Tools" -&gt; "Device Configuration Wizard"
     */
    public boolean showTopDeviceConfWizard()
    {
        return topItemDATDeviceConfWizard;
    }

    /**
     * @return <code>true</code> if the following top menu item is visible:<br/> "Device" -&gt;
     *         "Advance Tools" -&gt; "Serial Number Allocations"
     */
    public boolean showTopDeviceSerialNumAlloc()
    {
        return topItemDATSerialNumAlloc;
    }

    /**
     * @return <code>true</code> if the following top menu item is visible:<br/> "Device" -&gt;
     *         "Advance Tools" -&gt; "Device Cloning"
     */
    public boolean showTopDeviceCloning()
    {
        return topItemDATDeviceCloning;
    }

    /**
     * @return <code>true</code> if the following top menu is visible:<br/> "Device" -&gt;
     *         "Advance Tools"
     */
    public boolean showTopDeviceAdvTools()
    {
        return showTopDeviceConfWizard() || showTopDeviceSerialNumAlloc() || showTopDeviceCloning();
    }

    /**
     * @return <code>true</code> if the following top menu is visible:<br/> "Device"
     */
    public boolean showTopMenuDevices()
    {
        return showTopDeviceNumber() || showTopDevicesView() || showTopDeviceAdvTools();
    }

    /**
     * @return <code>true</code> if the following top menu and its sub menu items are visible:<br/>
     *         "Locations"
     */
    public boolean showTopMenuLocations()
    {
        return topMenuLocations;
    }

    /**
     * @return <code>true</code> if the following top menu items are visible:<br/> "Customers" -&gt;
     *         "Customer Name"<br/>"Customers" -&gt; "New Customer Name"
     */
    public boolean showTopCustomerName()
    {
        return topItemCustName;
    }

    /**
     * @return <code>true</code> if the following top menu and its sub menu items are visible:<br/>
     *         "Customers" -&gt; "Advance Tools"
     */
    public boolean showTopCustomerAdvTools()
    {
        return topMenuCustAdTools;
    }

    /**
     * @return <code>true</code> if the following top menu and its sub menu items are visible:<br/>
     *         "Customers"
     */
    public boolean showTopMenuCustomers()
    {
        return showTopCustomerName() || showTopCustomerAdvTools();
    }

    /**
     * @return <code>true</code> if the following top menu items are visible:<br/> "Consumers" -&gt;
     *         "Consumer/Card Search"<br/>"Consumers" -&gt; "Card Search"
     */
    public boolean showTopConsumerCardSearch()
    {
        return topItemConsCardSearch;
    }

    /**
     * @return <code>true</code> if the following top menu item is visible:<br/> "Consumers" -&gt;
     *         "Card Creator Wizard"
     */
    public boolean showTopConsumerCardWizard()
    {
        return topItemConsCardWizard;
    }

    /**
     * @return <code>true</code> if the following top menu and its sub menu items are visible:<br/>
     *         "Consumers"
     */
    public boolean showTopMenuConsumers()
    {
        return showTopConsumerCardSearch() || showTopConsumerCardWizard();
    }

    /**
     * @return <code>true</code> if the following top menu and its sub menu items are visible:<br/>
     *         "Files"
     */
    public boolean showTopMenuFiles()
    {
        return topMenuFiles;
    }

    /**
     * @return <code>true</code> if the top menu and its sub menu items are visible.
     */
    public boolean showTopMenu()
    {
        return showTopMenuDevices() || showTopMenuLocations() || showTopMenuCustomers() || showTopMenuConsumers() || showTopMenuFiles();
    }

    /**
     * @return <code>true</code> if the following left menu and its sub menu items are visible:<br/>
     *         "Transactions"
     */
    public boolean showLeftMenuTransactions()
    {
        return leftMenuTransactions;
    }

    /**
     * @return <code>true</code> if the following left menu and its sub menu items are visible:<br/>
     *         "Reports"
     */
    public boolean showLeftMenuReports()
    {
        return leftMenuReports;
    }

    /**
     * @return <code>true</code> if the following left menu item is visible:<br/> "Advance Tools"
     *         -&gt; "Access GPRS Web"
     */
    public boolean showLeftAdvToolGprs()
    {
        return leftItemATGprsWeb;
    }

    /**
     * @return <code>true</code> if the following left menu item is visible:<br/> "Advance Tools"
     *         -&gt; "Configure various application Templates"
     */
    public boolean showLeftAdvToolConfTemplate()
    {
        return leftItemATConfigTemplates;
    }

    /**
     * @return <code>true</code> if the following left menu item is visible:<br/> "Advance Tools"
     *         -&gt; "Refresh Menu Content"
     */
    public boolean showLeftAdvToolRefreshMenu()
    {
        return leftItemATRefreshMenu;
    }

    /**
     * @return <code>true</code> if the following left menu and its sub menu items are visible:<br/>
     *         "Advance Tools"
     */
    public boolean showLeftMenuAdvTools()
    {
        return showLeftAdvToolGprs() || showLeftAdvToolConfTemplate() || showLeftAdvToolRefreshMenu();
    }

    /**
     * @return<code>true</code> if the following left menu item is visible:<br/> "Bank Accounts"
     *                          -&gt; "New"
     */
    public boolean showLeftBANew()
    {
        return leftItemBANew;
    }

    /**
     * @return<code>true</code> if the following left menu item is visible:<br/> "Bank Accounts"
     *                          -&gt; "Edit"
     */
    public boolean showLeftBAEdit()
    {
        return leftItemBAEdit;
    }

    /**
     * @return<code>true</code> if the following left menu and its sub menu items are visible:<br/>
     *                          "Bank Accounts"
     */
    public boolean showLeftMenuBankAccounts()
    {
        return showLeftBANew() || showLeftBAEdit();
    }

    /**
     * @return<code>true</code> if the following left menus and their sub menu items are visible:
     *                          <ul> <li> "Customers"</li> <ul> <li> - &gt; "New"</li> <li> - &gt;
     *                          "Edit"</li> </ul> <li> "Dealers"</li> <li> "ePorts"</li> <ul> <li>
     *                          - &gt; "Add"</li> <li> - &gt; "Edit"</li> </ul> <li>
     *                          "Service Agreement"</li> <li> "No Service Agreement"</li> </ul>
     */
    public boolean showLeftMenus4CustService()
    {
        return leftMenu4CustomerService;
    }

    /**
     * @return<code>true</code> if the following left menu item is visible:<br/> "EFT" -&gt;
     *                          "Pending"
     */
    public boolean showLeftEftPending()
    {
        return leftItemEftPending;
    }

    /**
     * @return<code>true</code> if the following left menu item is visible:<br/> "EFT" -&gt;
     *                          "Approved"
     */
    public boolean showLeftEftApproved()
    {
        return leftItemEftApproved;
    }

    /**
     * @return<code>true</code> if the following left menu and its sub menu items are visible:<br/>
     *                          "EFT"
     */
    public boolean showLeftMenuEft()
    {
        return showLeftEftPending() || showLeftEftApproved();
    }

    /**
     * @return<code>true</code> if the following left menu item is visible:<br/> "Terminals" -&gt;
     *                          "New" and "Edit"
     */
    public boolean showLeftTNewEdit()
    {
        return leftItemTNewEdit;
    }

    /**
     * @return<code>true</code> if the following left menu item is visible:<br/> "Terminal" -&gt;
     *                          "Updated"
     */
    public boolean showLeftTUpdated()
    {
        return leftItemTUpdated;
    }

    /**
     * @return<code>true</code> if the following left menu and its sub menu items are visible:<br/>
     *                          "Terminals"
     */
    public boolean showLeftMenuTerminals()
    {
        return showLeftTNewEdit() || showLeftTUpdated();
    }
}
