/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.user;

import com.usatech.dms.model.DmsPrivilege;

import simple.servlet.BasicServletUser;

/**
 * Manages the menu items' visibilities in DMS.
 */
public class DmsMenuManager
{
    public static final String ATTRIBUTE_MENU_MANAGER = "com.usatech.dms.MenuManager";

    /* ====================================================================== */
    /* top menu items */
    /* ====================================================================== */
    /** top menu item "Device" -&gt; "Device Name" */
    private boolean topItemDeviceNumber;
    /** top menu "Device" -&gt; "View Devices" */
    private boolean topMenuDViewDevices;
    /** top menu item "Device" -&gt; "Advance Tools" -&gt; "Device Configuration Wizard" */
    private boolean topItemDATDeviceConfWizard;
    /** top menu item "Device" -&gt; "Advance Tools" -&gt; "Serial Number Allocations" */
    private boolean topItemDATSerialNumAlloc;
    /** top menu item "Device" -&gt; "Advance Tools" -&gt; "Device Cloning" */
    private boolean topItemDATDeviceCloning;

    /** top menu "Locations" */
    private boolean topMenuLocations;

    /** top menu item "Customers" -&gt; "Customer Name" */
    private boolean topItemCustName;
    /** top menu "Customers" -&gt; "Advance Tools" */
    private boolean topMenuCustAdTools;

    /** top menu item "Consumers" -&gt; "Card Search" */
    private boolean topItemConsCardSearch;
    /** top menu "Consumers" -&gt; "Card Creator Wizard" */
    private boolean topItemConsCardWizard;

    /** top menu "Files" */
    private boolean topMenuFiles;
    
    private boolean posmAdmin;

    /* ====================================================================== */
    /* left menu items */
    /* ====================================================================== */
    /** left menu "Transactions" */
    private boolean leftMenuTransactions;

    /** left menu "Reports" */
    private boolean leftMenuReports;

    /** left menu item "Advance Tools" -&gt; "Access GPRS Web" */
    private boolean leftItemATGprsWeb;
    /** left menu item "Advance Tools" -&gt; "Configure various application Templates" */
    private boolean leftItemATConfigTemplates;
    /** left menu item "Advance Tools" -&gt; "Refresh Menu Content" */
    private boolean leftItemATRefreshMenu;

    /** left menu item "Bank Accounts" -&gt; "Bank Account New" */
    private boolean leftItemBANew;
    /** left menu item "Bank Accounts" -&gt; "Bank Account Edit" */
    private boolean leftItemBAEdit;

    /** left menu "Customer", "Dealer", "ePorts", "License Agreements" and "No License Agreement" */
    private boolean leftMenu4CustomerService;

    /** left menu item "EFT" -&gt; "Pending" */
    private boolean leftItemEftPending;
    /** left menu item "EFT" -&gt; "Approved" */
    private boolean leftItemEftApproved;
    
    private boolean leftMenuFees;

    /** left menu item "Terminals" -&gt; "Terminal New" and "Terminal Edit" */
    private boolean leftItemTNewEdit;
    /** left menu item "Terminals" -&gt; "Terminal Updated" */
    private boolean leftItemTUpdated;
    
    private boolean leftMenuSales;
    private boolean paymentAdmin;

    /**
     * Constructs a instance of this class, and cache all the menu items' visibilities base on the
     * logged on user, so that we don't want to calculate it every time, means generates once, use
     * for the whole session.
     * @param user the logged on user
     */
    public DmsMenuManager(BasicServletUser user)
    {
        /* ================================================================== */
        /* Authorization legacy from Device Admin application. */
        /* ================================================================== */
        // available menu items for "DMS Users"
        if (user.hasPrivilege(DmsPrivilege.DMS_USERS.getValue()))
        {
            topMenuDViewDevices = true;
            leftMenuTransactions = true;
            leftMenuReports = true;
        }
        // available menu items for "DMS Customer Admins"
        if (user.hasPrivilege(DmsPrivilege.DMS_CUSTOMER_ADMINS.getValue()))
        {
            topItemCustName = true;
        }
        // available menu items for "DMS Location Admins"
        if (user.hasPrivilege(DmsPrivilege.DMS_LOCATION_ADMINS.getValue()))
        {
            topMenuLocations = true;
        }
        // available menu items for "DMS GPRS Web Admins"
        if (user.hasPrivilege(DmsPrivilege.DMS_GPRS_WEB_ADMINS.getValue()))
        {
            leftItemATGprsWeb = true;
        }
        // available menu items for "DMS Bulk Card Configuration Admins"
        if (user.hasPrivilege(DmsPrivilege.DMS_BULK_CARD_CONFIGURATION_ADMINS.getValue()))
        {
            topItemConsCardWizard = true;
        }
        // available menu items for "DMS Bulk Device configuration Admins"
        if (user.hasPrivilege(DmsPrivilege.DMS_BULK_DEVICE_CONFIGURATION_ADMINS.getValue()))
        {
            topItemDATDeviceConfWizard = true;
        }
        // available menu items for "DMS Config Template Admins"
        if (user.hasPrivilege(DmsPrivilege.DMS_CONFIG_TEMPLATE_ADMINS.getValue()))
        {
            leftItemATConfigTemplates = true;
        }
        // available menu items for "DMS Consumer Account Admins"
        if (user.hasPrivilege(DmsPrivilege.DMS_CONSUMER_ACCOUNT_ADMINS.getValue()))
        {
            topItemConsCardSearch = true;
        }
        // available menu items for "DMS Device Cloning Admins"
        if (user.hasPrivilege(DmsPrivilege.DMS_DEVICE_CLONING_ADMINS.getValue()))
        {
            topItemDATDeviceCloning = true;
        }
        // available menu items for "DMS Device Profile Admins"
        if (user.hasPrivilege(DmsPrivilege.DMS_DEVICE_PROFILE_ADMINS.getValue()))
        {
            topItemDeviceNumber = true;
        }
        // available menu items for "DMS eSuds Web Privilege Admins"
        if (user.hasPrivilege(DmsPrivilege.DMS_ESUDS_WEB_PRIVILEGE_ADMINS.getValue()))
        {
            topMenuCustAdTools = true;
        }
        // available menu items for "DMS File Admins"
        if (user.hasPrivilege(DmsPrivilege.DMS_FILE_ADMINS.getValue()))
        {
            topMenuFiles = true;
        }
        // available menu items for "DMS Menu Content Admins"
        if (user.hasPrivilege(DmsPrivilege.DMS_MENU_CONTENT_ADMINS.getValue()))
        {
            leftItemATRefreshMenu = true;
        }
        // available menu items for "DMS Serial Number Allocation Admins"
        if (user.hasPrivilege(DmsPrivilege.DMS_SERIAL_NUMBER_ALLOCATION_ADMINS.getValue()))
        {
            topItemDATSerialNumAlloc = true;
        }

        /* ================================================================== */
        /* Authorization legacy from ePort Manager. */
        /* ================================================================== */
        // available menu items for "DMS Customer Service"
        if (user.hasPrivilege(DmsPrivilege.DMS_CUSTOMER_SERVICE.getValue()))
        {
            leftItemBANew = true;
            leftMenu4CustomerService = true;
            leftItemTUpdated = true;
        }
        // available menu items for "DMS Payment Admins"
        if (user.hasPrivilege(DmsPrivilege.DMS_PAYMENT_ADMINS.getValue()))
        {
            leftItemBAEdit = true;
            leftMenuFees = true;
            paymentAdmin = true;
        }
        // available menu items for "DMS EFT Admins"
        if (user.hasPrivilege(DmsPrivilege.DMS_EFT_ADMINS.getValue()))
        {
        	leftItemEftPending = true;
            leftItemEftApproved = true;
        }
        // available menu items for "DMS Terminal Admins"
        if (user.hasPrivilege(DmsPrivilege.DMS_TERMINAL_ADMINS.getValue()))
        {
            leftItemTNewEdit = true;
        }
        
        if (user.hasPrivilege(DmsPrivilege.DMS_POSM_ADMINS.getValue()))
        	posmAdmin = true;
        
        if (user.hasPrivilege(DmsPrivilege.DMS_SALES_ADMINS.getValue()))
        	leftMenuSales = true;
    }

    /**
     * @return <code>true</code> if the following top menu items are visible:<br/> "Devices" -&gt;
     *         "Device Number"<br/>"Devices" -&gt; "Serial Number".
     */
    public boolean showTopDeviceNumber()
    {
        return topItemDeviceNumber;
    }

    /**
     * @return <code>true</code> if the following top menu and its sub menu items are visible:<br/>
     *         "Devices" -&gt; "View Devices"
     */
    public boolean showTopDevicesView()
    {
        return topMenuDViewDevices;
    }

    /**
     * @return <code>true</code> if the following top menu item is visible:<br/> "Device" -&gt;
     *         "Advance Tools" -&gt; "Device Configuration Wizard"
     */
    public boolean showTopDeviceConfWizard()
    {
        return topItemDATDeviceConfWizard;
    }

    /**
     * @return <code>true</code> if the following top menu item is visible:<br/> "Device" -&gt;
     *         "Advance Tools" -&gt; "Serial Number Allocations"
     */
    public boolean showTopDeviceSerialNumAlloc()
    {
        return topItemDATSerialNumAlloc;
    }

    /**
     * @return <code>true</code> if the following top menu item is visible:<br/> "Device" -&gt;
     *         "Advance Tools" -&gt; "Device Cloning"
     */
    public boolean showTopDeviceCloning()
    {
        return topItemDATDeviceCloning;
    }

    /**
     * @return <code>true</code> if the following top menu is visible:<br/> "Device" -&gt;
     *         "Advance Tools"
     */
    public boolean showTopDeviceAdvTools()
    {
        return topMenuDViewDevices || showTopDeviceConfWizard() || showTopDeviceSerialNumAlloc() || showTopDeviceCloning();
    }

    /**
     * @return <code>true</code> if the following top menu is visible:<br/> "Device"
     */
    public boolean showTopMenuDevices()
    {
        return showTopDeviceNumber() || showTopDevicesView() || showTopDeviceAdvTools();
    }

    /**
     * @return <code>true</code> if the following top menu and its sub menu items are visible:<br/>
     *         "Locations"
     */
    public boolean showTopMenuLocations()
    {
        return topMenuLocations;
    }

    /**
     * @return <code>true</code> if the following top menu items are visible:<br/> "Customers" -&gt;
     *         "Customer Name"<br/>"Customers" -&gt; "New Customer Name"
     */
    public boolean showTopCustomerName()
    {
        return topItemCustName;
    }

    /**
     * @return <code>true</code> if the following top menu and its sub menu items are visible:<br/>
     *         "Customers" -&gt; "Advance Tools"
     */
    public boolean showTopCustomerAdvTools()
    {
        return topMenuCustAdTools;
    }

    /**
     * @return <code>true</code> if the following top menu and its sub menu items are visible:<br/>
     *         "Customers"
     */
    public boolean showTopMenuCustomers()
    {
        return showTopCustomerName() || showTopCustomerAdvTools();
    }

    /**
     * @return <code>true</code> if the following top menu items are visible:<br/> "Consumers" -&gt;
     *         "Consumer/Card Search"<br/>"Consumers" -&gt; "Card Search"
     */
    public boolean showTopConsumerCardSearch()
    {
        return topItemConsCardSearch;
    }

    /**
     * @return <code>true</code> if the following top menu item is visible:<br/> "Consumers" -&gt;
     *         "Card Creator Wizard"
     */
    public boolean showTopConsumerCardWizard()
    {
        return topItemConsCardWizard;
    }

    /**
     * @return <code>true</code> if the following top menu and its sub menu items are visible:<br/>
     *         "Consumers"
     */
    public boolean showTopMenuConsumers()
    {
        return showTopConsumerCardSearch() || showTopConsumerCardWizard();
    }

    /**
     * @return <code>true</code> if the following top menu and its sub menu items are visible:<br/>
     *         "Files"
     */
    public boolean showTopMenuFiles()
    {
        return topMenuFiles;
    }

    /**
     * @return <code>true</code> if the top menu and its sub menu items are visible.
     */
    public boolean showTopMenu()
    {
        return showTopMenuDevices() || showTopMenuLocations() || showTopMenuCustomers() || showTopMenuConsumers() || showTopMenuFiles();
    }

    /**
     * @return <code>true</code> if the following left menu and its sub menu items are visible:<br/>
     *         "Transactions"
     */
    public boolean showLeftMenuTransactions()
    {
        return leftMenuTransactions;
    }

    /**
     * @return <code>true</code> if the following left menu and its sub menu items are visible:<br/>
     *         "Reports"
     */
    public boolean showLeftMenuReports()
    {
        return leftMenuReports;
    }

    /**
     * @return <code>true</code> if the following left menu item is visible:<br/> "Advance Tools"
     *         -&gt; "Access GPRS Web"
     */
    public boolean showLeftAdvToolGprs()
    {
        return leftItemATGprsWeb;
    }

    /**
     * @return <code>true</code> if the following left menu item is visible:<br/> "Advance Tools"
     *         -&gt; "Configure various application Templates"
     */
    public boolean showLeftAdvToolConfTemplate()
    {
        return leftItemATConfigTemplates;
    }

    /**
     * @return <code>true</code> if the following left menu item is visible:<br/> "Advance Tools"
     *         -&gt; "Refresh Menu Content"
     */
    public boolean showLeftAdvToolRefreshMenu()
    {
        return leftItemATRefreshMenu;
    }

    /**
     * @return <code>true</code> if the following left menu and its sub menu items are visible:<br/>
     *         "Advanced Tools"
     */
    public boolean showLeftMenuAdvTools()
    {
        return showLeftAdvToolGprs() || showLeftAdvToolConfTemplate() || showLeftAdvToolRefreshMenu();
    }

    /**
     * @return<code>true</code> if the following left menu item is visible:<br/> "Bank Accounts"
     *                          -&gt; "New"
     */
    public boolean showLeftBANew()
    {
        return leftItemBANew;
    }

    /**
     * @return<code>true</code> if the following left menu item is visible:<br/> "Bank Accounts"
     *                          -&gt; "Edit"
     */
    public boolean showLeftBAEdit()
    {
        return leftItemBAEdit;
    }

    /**
     * @return<code>true</code> if the following left menu and its sub menu items are visible:<br/>
     *                          "Bank Accounts"
     */
    public boolean showLeftMenuBankAccounts()
    {
        return showLeftBANew() || showLeftBAEdit();
    }

    /**
     * @return<code>true</code> if the following left menus and their sub menu items are visible:
     *                          <ul> <li> "Customers"</li> <ul> <li> - &gt; "New"</li> <li> - &gt;
     *                          "Edit"</li> </ul> <li> "Dealers"</li> <li> "ePorts"</li> <ul> <li>
     *                          - &gt; "Add"</li> <li> - &gt; "Edit"</li> </ul> <li>
     *                          "Service Agreement"</li> <li> "No Service Agreement"</li> </ul>
     */
    public boolean showLeftMenus4CustService()
    {
        return leftMenu4CustomerService;
    }

    /**
     * @return<code>true</code> if the following left menu item is visible:<br/> "EFT" -&gt;
     *                          "Pending"
     */
    public boolean showLeftEftPending()
    {
        return leftItemEftPending;
    }

    /**
     * @return<code>true</code> if the following left menu item is visible:<br/> "EFT" -&gt;
     *                          "Approved"
     */
    public boolean showLeftEftApproved()
    {
        return leftItemEftApproved;
    }

    /**
     * @return<code>true</code> if the following left menu and its sub menu items are visible:<br/>
     *                          "EFT"
     */
    public boolean showLeftMenuEft()
    {
        return showLeftEftPending() || showLeftEftApproved();
    }
    
    public boolean showLeftMenuFees()
    {
    	return leftMenuFees;
    }
    
    public boolean isPaymentAdmin()
    {
    	return paymentAdmin;
    }

    /**
     * @return<code>true</code> if the following left menu item is visible:<br/> "Terminals" -&gt;
     *                          "New" and "Edit"
     */
    public boolean showLeftTNewEdit()
    {
        return leftItemTNewEdit;
    }

    /**
     * @return<code>true</code> if the following left menu item is visible:<br/> "Terminal" -&gt;
     *                          "Updated"
     */
    public boolean showLeftTUpdated()
    {
        return leftItemTUpdated;
    }

    /**
     * @return<code>true</code> if the following left menu and its sub menu items are visible:<br/>
     *                          "Terminals"
     */
    public boolean showLeftMenuTerminals()
    {
        return showLeftTNewEdit() || showLeftTUpdated();
    }
    
    public boolean showLeftMenuSales() {
    	return leftMenuSales;
    }
    
    public boolean isPOSMAdmin()
    {
    	return posmAdmin;
    }
}
