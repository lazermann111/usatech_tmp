package com.usatech.dms.transaction;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerMgr;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;

import com.usatech.layers.common.util.StringHelper;

public class EditTerminalStep extends AbstractStep {
	@Override
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		try { 
			String action = form.getString("action", false);
			if (!StringHelper.isBlank(action)) {
				if ("Add Terminal".equalsIgnoreCase(action)) {
					 Map<String, Object> params = new HashMap<String, Object>();
					 params.put("terminal_state_id", 1);
					 params.put("terminal_cd", form.getString("terminal_cd", true));
					 params.put("terminal_desc", form.getString("terminal_desc", true));
					 params.put("merchant_id", form.getLong("terminal_merchant_id", true, -1));
					 params.put("terminal_next_batch_num", form.getLong("terminal_next_batch_num", true, 1));
					 params.put("terminal_batch_cycle_num", form.getLong("terminal_batch_cycle_num", true, 1));
					 params.put("terminal_min_batch_num", form.getLong("terminal_min_batch_num", true, 1));
					 params.put("terminal_max_batch_num", form.getLong("terminal_max_batch_num", true, 999));
					 params.put("terminal_batch_min_tran", form.getLong("terminal_batch_min_tran", true, 4999));
					 params.put("terminal_batch_max_tran", form.getLong("terminal_batch_max_tran", true, 4999));
					 params.put("terminal_min_batch_close_hr", form.getLong("terminal_min_batch_close_hr", true, 12));
					 params.put("terminal_max_batch_close_hr", form.getLong("terminal_max_batch_close_hr", true, 12));
					 Object[] result = DataLayerMgr.executeCall("INSERT_TERMINAL", params, true);
					 if (result != null && result[1] != null) {
						 long itemId = ((BigDecimal) result[1]).longValue();
						 if (itemId > 0)
							 form.setRedirectUri(new StringBuilder("/terminalInfo.i?terminal_id=").append(itemId).append("&msg=Terminal+created").toString());
					 }
				} else if ("Save".equalsIgnoreCase(action)) {
					long terminalId = form.getLong("terminal_id", true, -1);
					Map<String, Object> params = new HashMap<String, Object>();
					params.put("terminal_desc", form.getString("terminal_desc", true));
					params.put("terminal_batch_min_tran", form.getLong("terminal_batch_min_tran", true, 4999));
					params.put("terminal_batch_max_tran", form.getLong("terminal_batch_max_tran", true, 4999));
					params.put("terminal_min_batch_close_hr", form.getLong("terminal_min_batch_close_hr", true, 12));
					params.put("terminal_max_batch_close_hr", form.getLong("terminal_max_batch_close_hr", true, 12));
					params.put("terminal_id", form.getLong("terminal_id", true, -1));
					int updateCount = DataLayerMgr.executeUpdate("UPDATE_OPER_TERMINAL", params, true);
					if (updateCount > 0)
						form.setRedirectUri(new StringBuilder("/terminalInfo.i?terminal_id=").append(terminalId).append("&msg=Terminal+updated").toString());
					else
						form.setRedirectUri(new StringBuilder("/terminalInfo.i?terminal_id=").append(terminalId).append("&err=Terminal+name+already+exists").toString());
				} else if ("Delete".equalsIgnoreCase(action)) {
					long terminalId = form.getLong("terminal_id", true, -1);
					DataLayerMgr.executeUpdate("DELETE_TERMINAL", new Object[] {terminalId} , true);
					form.setRedirectUri("/tranMenu.i?msg=Terminal+deleted");
				}
			}
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}
}
