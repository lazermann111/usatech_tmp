package com.usatech.dms.transaction;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerMgr;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;
import simple.text.Censor;
import simple.text.StringUtils;

import com.usatech.layers.common.util.StringHelper;

public class EditMerchantStep extends AbstractStep {
	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		try { 
			String action = form.getString("action", false);
			if (!StringHelper.isBlank(action)) {
				if ("Add Merchant".equalsIgnoreCase(action)) {
					Map<String, Object> params = new HashMap<String, Object>();
					params.put("merchant_cd", StringUtils.trim(form.getString("merchant_cd", true)));
					params.put("merchant_name", StringUtils.trim(form.getString("merchant_name", true)));
					params.put("merchant_desc", StringUtils.trim(form.getString("merchant_desc", true)));
					params.put("merchant_bus_name", StringUtils.trim(form.getString("merchant_bus_name", true)));
					params.put("authority_id", form.getLong("merchant_authority_id", true, -1));
					params.put("doing_business_as", Censor.sanitizeText(form.getStringSafely("doing_business_as", "").trim()));
					params.put("mcc", form.getBigDecimal("mcc", false));
					params.put("authority_assigned_num", StringUtils.trim(form.getString("authority_assigned_num", false)));
					String merchantGroup = StringUtils.trim(form.getString("merchant_group_cd", false));
					if(StringUtils.isBlank(merchantGroup))
						merchantGroup = StringUtils.trim(form.getString("new_merchant_group_cd", false));
					params.put("merchant_group_cd", merchantGroup);

					Object[] result = DataLayerMgr.executeCall("INSERT_MERCHANT", params, true);
					if(result != null && result[1] != null) {
						long itemId = ((BigDecimal) result[1]).longValue();
						if(itemId > 0)
							form.setRedirectUri(new StringBuilder("/merchantInfo.i?merchant_id=").append(itemId).append("&msg=Merchant+created").toString());
					}
				} else if ("Save".equalsIgnoreCase(action)) {
					long merchantId = form.getLong("merchant_id", true, -1);
					Map<String, Object> params = new HashMap<String, Object>();
					params.put("merchant_name", StringUtils.trim(form.getString("merchant_name", true)));
					params.put("merchant_desc", StringUtils.trim(form.getString("merchant_desc", true)));
					params.put("merchant_bus_name", StringUtils.trim(form.getString("merchant_bus_name", true)));
					params.put("doing_business_as", Censor.sanitizeText(form.getStringSafely("doing_business_as", "").trim()));
					params.put("merchant_id", merchantId);
					params.put("mcc", form.getBigDecimal("mcc", false));
					params.put("authority_assigned_num", StringUtils.trim(form.getString("authority_assigned_num", false)));
					String merchantGroup = StringUtils.trim(form.getString("merchant_group_cd", false));
					if(StringUtils.isBlank(merchantGroup))
						merchantGroup = StringUtils.trim(form.getString("new_merchant_group_cd", false));
					params.put("merchant_group_cd", merchantGroup);

					int updateCount = DataLayerMgr.executeUpdate("UPDATE_MERCHANT", params, true);
					if (updateCount > 0)
						form.setRedirectUri(new StringBuilder("/merchantInfo.i?merchant_id=").append(merchantId).append("&msg=Merchant+updated").toString());
					else
						form.setRedirectUri(new StringBuilder("/merchantInfo.i?merchant_id=").append(merchantId).append("&err=Merchant+name+already+exists").toString());
				} else if ("Delete".equalsIgnoreCase(action)) {
					long merchantId = form.getLong("merchant_id", true, -1);
					DataLayerMgr.executeUpdate("DELETE_MERCHANT", new Object[] {merchantId} , true);
					form.setRedirectUri("/tranMenu.i?msg=Merchant+deleted");
				}
			}
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}
}
