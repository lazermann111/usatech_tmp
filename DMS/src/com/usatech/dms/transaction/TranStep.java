package com.usatech.dms.transaction;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;

public class TranStep extends AbstractStep
{
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
        String action = form.getStringSafely("action", "");
        if (!"Change".equalsIgnoreCase(action))
        	return;
        long tranId = form.getLong("tran_id", true, 0);
        String newTranStateCd = form.getString("new_tran_state_cd", true);
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("TRAN_ID", tranId);
        params.put("TRAN_STATE_CD", newTranStateCd);
        try {
					DataLayerMgr.executeUpdate("UPDATE_TRAN", params, true);
				} catch (SQLException | DataLayerException e) {
					throw new ServletException("Error updating transaction", e);
				}
        form.setRedirectUri(new StringBuilder("/tran.i?tran_id=").append(tranId).append("&msg=Transaction+state+updated").toString());
    }
}
