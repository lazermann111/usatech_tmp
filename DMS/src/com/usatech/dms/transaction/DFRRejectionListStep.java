package com.usatech.dms.transaction;

import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usatech.layers.common.util.PaginationUtil;
import com.usatech.layers.common.util.StringHelper;

import simple.app.DialectResolver;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;
import simple.text.StringUtils;
import simple.util.LinkedHashMap;

public class DFRRejectionListStep extends AbstractStep {

	public static final String DEFAULT_SORT_INDEX = "2";
	
	private static final String SQL_START_1  = "SELECT T.TRAN_ID, DLA.DEVICE_SERIAL_CD, TO_CHAR(T.TRAN_UPLOAD_TS, 'MM/DD/YYYY HH24:MI:SS') TRAN_UPLOAD_TS, A.AUTH_AMT AMOUNT, T.TRAN_RECEIVED_RAW_ACCT_DATA CARD, FC.FILE_CACHE_ID, REPLACE(REGEXP_REPLACE(T.TRAN_GLOBAL_TRANS_CD, '^([^:]+:)(.*)', '\\2'), ':', '-') MERCHANT_ORDER ";
	private static final String SQL_START_2  = "SELECT T.TRAN_ID, DLA.DEVICE_SERIAL_CD, TO_CHAR(R.REFUND_ISSUE_TS, 'MM/DD/YYYY HH24:MI:SS') TRAN_UPLOAD_TS, R.REFUND_AMT AMOUNT, T.TRAN_RECEIVED_RAW_ACCT_DATA CARD, FC.FILE_CACHE_ID, REPLACE(REGEXP_REPLACE(T.TRAN_GLOBAL_TRANS_CD, '^([^:]+:)(.*)', '\\2'), ':', '-') MERCHANT_ORDER ";
		
	private static final String SQL_BASE_1 = " FROM REPORT.FILE_CACHE FC JOIN PSS.AUTH A ON FC.FILE_CACHE_ID = A.DFR_FILE_CACHE_ID JOIN PSS.TRAN T ON A.TRAN_ID = T.TRAN_ID JOIN DEVICE.DEVICE_LAST_ACTIVE DLA ON T.DEVICE_NAME = DLA.DEVICE_NAME ";
	private static final String SQL_BASE_2 = " FROM REPORT.FILE_CACHE FC JOIN PSS.REFUND R ON FC.FILE_CACHE_ID = R.DFR_FILE_CACHE_ID JOIN PSS.TRAN T ON R.TRAN_ID = T.TRAN_ID JOIN DEVICE.DEVICE_LAST_ACTIVE DLA ON T.DEVICE_NAME = DLA.DEVICE_NAME ";
		
	private static final String[] SORT_FIELDS = {
		"TRAN_ID", "DEVICE_SERIAL_CD", "TRAN_UPLOAD_TS", "AMOUNT", "CARD", "MERCHANT_ORDER", "FILE_CACHE_ID"
	};
	
	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		try {
			String action = form.getString("action", false);
			long fileCacheId = form.getLong("fileCacheId", false, 0);

			if ("List Rejections".equalsIgnoreCase(action) || fileCacheId > 0) {
				Map<Integer, Object> params = new LinkedHashMap<Integer, Object>();
				int paramCnt = 0;
				StringBuilder queryBase1 = new StringBuilder(SQL_BASE_1);
				StringBuilder queryBase2 = new StringBuilder(SQL_BASE_2);

				String code;
				if (fileCacheId > 0) {
					code = " WHERE FC.FILE_CACHE_ID = ? ";
					queryBase1.append(code);
					params.put(paramCnt++, fileCacheId);
					queryBase2.append(code);					
					params.put(paramCnt++, fileCacheId);
				} else {
					String dfrFromDate = form.getString("dfr_from_date", true);
					String dfrFromTime = form.getString("dfr_from_time", true);
					String dfrToDate = form.getString("dfr_to_date", true);
					String dfrToTime = form.getString("dfr_to_time", true);
	
					code = !DialectResolver.isOracle() 
							? " WHERE FC.CREATED_TS BETWEEN TO_TIMESTAMP(? || ' ' || ?, 'MM/DD/YYYY HH24:MI:SS') AND TO_TIMESTAMP(? || ' ' || ?, 'MM/DD/YYYY HH24:MI:SS') " 
							: " WHERE FC.CREATED_TS BETWEEN TO_DATE(? || ' ' || ?, 'MM/DD/YYYY HH24:MI:SS') AND TO_DATE(? || ' ' || ?, 'MM/DD/YYYY HH24:MI:SS') ";
					queryBase1.append(code);
					params.put(paramCnt++, dfrFromDate);
					params.put(paramCnt++, dfrFromTime);
					params.put(paramCnt++, dfrToDate);
					params.put(paramCnt++, dfrToTime);
					queryBase2.append(code);
					params.put(paramCnt++, dfrFromDate);
					params.put(paramCnt++, dfrFromTime);
					params.put(paramCnt++, dfrToDate);
					params.put(paramCnt++, dfrToTime);	
				}
				queryBase1.append(" AND A.AUTH_TYPE_CD = 'U' AND A.AUTH_STATE_ID = 8 ");
				queryBase2.append(" AND R.REFUND_STATE_ID = 7 ");

				String paramTotalCount = PaginationUtil.getTotalField(null);
				String paramPageIndex = PaginationUtil.getIndexField(null);
				String paramPageSize = PaginationUtil.getSizeField(null);
				String paramSortIndex = PaginationUtil.getSortField(null);
				
				int totalCount = form.getInt(paramTotalCount, false, -1);
				if (totalCount == -1 || !StringHelper.isBlank(action)) {
					StringBuilder sb = new StringBuilder("SELECT SUM(TOTAL)");
					if (fileCacheId > 0)
						sb.append(", MAX(DFR_TS) DFR_TS");
					sb.append(" FROM (SELECT COUNT(1) TOTAL ");
					if (fileCacheId > 0)
						sb.append(", MAX(TO_CHAR(FC.CREATED_TS, 'MM/DD/YYYY HH24:MI:SS')) DFR_TS ");
					sb.append(queryBase1)
					.append(" UNION ALL SELECT COUNT(1) TOTAL ");
					if (fileCacheId > 0)
						sb.append(", MAX(TO_CHAR(FC.CREATED_TS, 'MM/DD/YYYY HH24:MI:SS')) DFR_TS ");
					sb.append(queryBase2).append(")");
					Results total = DataLayerMgr.executeSQL("OPER", sb.toString(), params.values().toArray(), null);
					if (total.next()) {
						totalCount = total.getValue(1, int.class);						
						if (fileCacheId > 0) {
							String dfrTs = total.getValue(2, String.class);							
							if (!StringUtils.isBlank(dfrTs)) {
								String dfrDate = dfrTs.substring(0, 10);
								request.setAttribute("dfr_from_date", dfrDate);							
								request.setAttribute("dfr_to_date", dfrDate);
								String dfrTime = dfrTs.substring(11);
								request.setAttribute("dfr_from_time", dfrTime);							
								request.setAttribute("dfr_to_time", dfrTime);
							}
						}
					} else {
						totalCount = 0;
					}
					request.setAttribute(paramTotalCount, String.valueOf(totalCount));
				}

				int pageIndex = form.getInt(paramPageIndex, false, 1);
				int pageSize = form.getInt(paramPageSize, false, PaginationUtil.DEFAULT_PAGE_SIZE);
				int minRowToFetch = PaginationUtil.getStartNum(pageSize, pageIndex);
				int maxRowToFetch = PaginationUtil.getEndNum(pageSize, pageIndex);

				String sortIndex = form.getString(paramSortIndex, false);
				sortIndex = StringHelper.isBlank(sortIndex) ? DEFAULT_SORT_INDEX : sortIndex;
				String orderBy = PaginationUtil.constructOrderBy(SORT_FIELDS, sortIndex);
				
				String query;
				if (DialectResolver.isOracle()) {
					query = new StringBuilder("select * from (select pagination_temp.*, ROWNUM rnum from (")
							.append(SQL_START_1).append(queryBase1).append(" UNION ALL ").append(SQL_START_2).append(queryBase2).append(orderBy)
							.append(") pagination_temp where ROWNUM <= ?) where rnum  >= ? ").toString();

				} else {
					query = new StringBuilder("select * from (select pagination_temp.*, row_number()over() rnum from (")
							.append(SQL_START_1).append(queryBase1).append(" UNION ALL ").append(SQL_START_2).append(queryBase2).append(orderBy)
							.append(" ) pagination_temp limit CAST (? AS numeric)) sq_end where rnum  >= CAST (? AS numeric) ")
							.toString();
				}
				
				params.put(paramCnt++, String.valueOf(maxRowToFetch));
				params.put(paramCnt++, String.valueOf(minRowToFetch));
				Results results = DataLayerMgr.executeSQL("OPER", query, params.values().toArray(), null);
				request.setAttribute("resultlist", results);
			}
		} catch (Exception e) {
			throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
		}
	}
}
