package com.usatech.dms.transaction;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usatech.layers.common.util.PaginationUtil;
import com.usatech.layers.common.util.StringHelper;

import simple.app.DialectResolver;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;
import simple.util.LinkedHashMap;

public class PendingReversalListStep extends AbstractStep {

	public static final String DEFAULT_SORT_INDEX = "-4";
	
	private static final String SQL_START_1  = "SELECT T.TRAN_ID, DLA.DEVICE_SERIAL_CD, 'Sale' TRAN_TYPE, AT.AUTH_TYPE_DESC REVERSAL_TYPE, TO_CHAR(T.TRAN_UPLOAD_TS, 'MM/DD/YYYY HH24:MI:SS') TRAN_UPLOAD_TS, A.AUTH_AMT AMOUNT, T.TRAN_RECEIVED_RAW_ACCT_DATA CARD, T.TRAN_GLOBAL_TRANS_CD ";
	private static final String SQL_START_2  = "SELECT T.TRAN_ID, DLA.DEVICE_SERIAL_CD, 'Refund' TRAN_TYPE, RT.REFUND_TYPE_DESC REVERSAL_TYPE, TO_CHAR(R.REFUND_ISSUE_TS, 'MM/DD/YYYY HH24:MI:SS') TRAN_UPLOAD_TS, R.REFUND_AMT AMOUNT, T.TRAN_RECEIVED_RAW_ACCT_DATA CARD, T.TRAN_GLOBAL_TRANS_CD ";
		
	private static final String SQL_BASE_1 = " FROM PSS.TRAN T JOIN PSS.AUTH A ON A.TRAN_ID = T.TRAN_ID AND A.AUTH_TYPE_CD = 'E' JOIN DEVICE.DEVICE_LAST_ACTIVE DLA ON T.DEVICE_NAME = DLA.DEVICE_NAME JOIN PSS.AUTH_TYPE AT ON A.AUTH_TYPE_CD = AT.AUTH_TYPE_CD WHERE T.TRAN_STATE_CD = '!' ";
	private static final String SQL_BASE_2 = " FROM PSS.TRAN T JOIN PSS.REFUND R ON R.TRAN_ID = T.TRAN_ID AND R.REFUND_TYPE_CD = 'R' JOIN DEVICE.DEVICE_LAST_ACTIVE DLA ON T.DEVICE_NAME = DLA.DEVICE_NAME JOIN PSS.REFUND_TYPE RT ON R.REFUND_TYPE_CD = RT.REFUND_TYPE_CD WHERE T.TRAN_STATE_CD = '!' ";
		
	private static final String[] SORT_FIELDS = {
		"TRAN_ID", "DEVICE_SERIAL_CD", "TRAN_UPLOAD_TS", "AMOUNT", "CARD", "TRAN_GLOBAL_TRANS_CD", "TRAN_TYPE", "REVERSAL_TYPE"
	};
	
	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		try {
			String action = form.getString("action", false);
			Map<String, Object> actionParams = new HashMap<String, Object>();
			if ("View Reversals".equalsIgnoreCase(action)) {
				Map<Integer, Object> params = new LinkedHashMap<Integer, Object>();
				int paramCnt = 0;
				String queryBase1 = SQL_BASE_1;
				String queryBase2 = SQL_BASE_2;

				String paramTotalCount = PaginationUtil.getTotalField(null);
				String paramPageIndex = PaginationUtil.getIndexField(null);
				String paramPageSize = PaginationUtil.getSizeField(null);
				String paramSortIndex = PaginationUtil.getSortField(null);
				
				int totalCount = form.getInt(paramTotalCount, false, -1);
				if (totalCount == -1 || !StringHelper.isBlank(action)) {
					StringBuilder sb = new StringBuilder("SELECT SUM(TOTAL)");
					sb.append(" FROM (SELECT COUNT(1) TOTAL ");
					sb.append(queryBase1)
					.append(" UNION ALL SELECT COUNT(1) TOTAL ");
					sb.append(queryBase2).append(")");
					Results total = DataLayerMgr.executeSQL("OPER", sb.toString(), params.values().toArray(), null);
					if (total.next()) {
						totalCount = total.getValue(1, int.class);
					} else {
						totalCount = 0;
					}
					request.setAttribute(paramTotalCount, String.valueOf(totalCount));
				}

				int pageIndex = form.getInt(paramPageIndex, false, 1);
				int pageSize = form.getInt(paramPageSize, false, PaginationUtil.DEFAULT_PAGE_SIZE);
				int minRowToFetch = PaginationUtil.getStartNum(pageSize, pageIndex);
				int maxRowToFetch = PaginationUtil.getEndNum(pageSize, pageIndex);

				String sortIndex = form.getString(paramSortIndex, false);
				sortIndex = StringHelper.isBlank(sortIndex) ? DEFAULT_SORT_INDEX : sortIndex;
				String orderBy = PaginationUtil.constructOrderBy(SORT_FIELDS, sortIndex);
				
				String query;
				if (DialectResolver.isOracle()) {
					query = new StringBuilder("select * from (select pagination_temp.*, ROWNUM rnum from (")
							.append(SQL_START_1).append(queryBase1).append(" UNION ALL ").append(SQL_START_2).append(queryBase2).append(orderBy)
							.append(") pagination_temp where ROWNUM <= ?) where rnum  >= ? ").toString();

				} else {
					query = new StringBuilder("select * from (select pagination_temp.*, row_number()over() rnum from (")
							.append(SQL_START_1).append(queryBase1).append(" UNION ALL ").append(SQL_START_2).append(queryBase2).append(orderBy)
							.append(" ) pagination_temp limit CAST (? AS numeric)) sq_end where rnum  >= CAST (? AS numeric) ")
							.toString();
				}
				
				params.put(paramCnt++, String.valueOf(maxRowToFetch));
				params.put(paramCnt++, String.valueOf(minRowToFetch));
				Results results = DataLayerMgr.executeSQL("OPER", query, params.values().toArray(), null);
				request.setAttribute("resultlist", results);
			} else if ("Approve All".equalsIgnoreCase(action)) {
		        actionParams.put("TRAN_STATE_CD", '8');
		        DataLayerMgr.executeUpdate("UPDATE_PENDING_REVERSALS", actionParams, true);
		        request.setAttribute("msg", "All pending reversals have been approved");
			} else if ("Reject All".equalsIgnoreCase(action)) {
		        actionParams.put("TRAN_STATE_CD", 'E');
		        DataLayerMgr.executeUpdate("UPDATE_PENDING_REVERSALS", actionParams, true);
		        request.setAttribute("msg", "All pending reversals have been rejected");
			} else if ("Approve Selected".equalsIgnoreCase(action)) {
				String[] tranIds = form.getStringArray("tranId", false);
				if(tranIds.length > 0){
					actionParams.put("TRAN_STATE_CD", '8');
					for(int i = 0; i < tranIds.length; i++) {
						actionParams.put("TRAN_ID", tranIds[i]);
						DataLayerMgr.executeCall("UPDATE_TRAN", actionParams, true);
					}
					request.setAttribute("msg", "Selected pending reversals have been approved");
				} else
					request.setAttribute("msg", "No pending reversals selected");
			} else if ("Reject Selected".equalsIgnoreCase(action)) {
				String[] tranIds = form.getStringArray("tranId", false);
				if(tranIds.length > 0){
					actionParams.put("TRAN_STATE_CD", 'E');
					for(int i = 0; i < tranIds.length; i++) {
						actionParams.put("TRAN_ID", tranIds[i]);
						DataLayerMgr.executeCall("UPDATE_TRAN", actionParams, true);
					}
					request.setAttribute("msg", "Selected pending reversals have been rejected");
				} else
					request.setAttribute("msg", "No pending reversals selected");
			}
		} catch (Exception e) {
			throw new ServletException("Exception occured in " + getClass().getName() + " :: " + (e.getClass().getName() + " : " + e.getMessage()), e);
		}
	}
}
