/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.transaction;

public class TranConstants
{        
    public static final String PARAM_TRAN_ID = "tran_id";
    public static final String PARAM_TRAN_START_TS_NAME = "tran_start_ts";
    public static final String PARAM_TRAN_BEFORE_DATE = "tran_before_date";
    public static final String PARAM_TRAN_AFTER_DATE = "tran_after_date";
    public static final String PARAM_TRAN_SERIAL = "serial_number";
    public static final String NVP_TERMINAL_LIST_FORMATTED = "terminal_list_formatted";
    public static final String NVP_AUTHORITY_LIST_FORMATTED = "authority_list_formatted";
    public static final String NVP_MERCHANT_LIST_FORMATTED = "merchant_list_formatted";
    
    public static final String PARAM_BATCH_MERCHANT_ID = "merchant_id";
    public static final String PARAM_BATCH_MERCHANT_NAME = "merchant_name";
    public static final String PARAM_BATCH_TERMINAL_ID = "terminal_id";
    public static final String PARAM_BATCH_TERMINAL_BATCH_ID = "terminal_batch_id";
    public static final String PARAM_BATCH_TERMINAL_BATCH_NUM = "terminal_batch_num";
    public static final String PARAM_BATCH_TERMINAL_STATE_NAME = "state";
    public static final String PARAM_BATCH_TERMINAL_BATCH_OPEN_BEFORE = "open_before";
    public static final String PARAM_BATCH_TERMINAL_BATCH_CLOSED_BEFORE = "close_before";
    public static final String PARAM_BATCH_TERMINAL_BATCH_OPENED_AFTER = "open_after";
    public static final String PARAM_BATCH_TERMINAL_BATCH_CLOSED_AFTER = "close_after";
    public static final String PARAM_BATCH_AUTHORITY_ID = "authority_id";
    public static final String PARAM_BATCH_GREEN_TOP_LIMIT = "green_top_limit";
    public static final String PARAM_BATCH_GOLD_TOP_LIMIT = "gold_top_limit";
    public static final String PARAM_BATCH_WITH_TRAN = "with_tran";
    
    public static final String SQL_GET_TRAN_LIST_START = "select t.tran_id as s_tran_id, " + 
	  "to_char(t.tran_start_ts, 'MM/DD/YYYY HH24:MI:SS') as tran_start_ts, " +
	  "ts.tran_state_desc as tran_state_desc, " +
	  "s.sale_amount sale_amount, " +
	  "t.tran_received_raw_acct_data as tran_received_raw_acct_data, " +
	  "t.tran_global_trans_cd as tran_global_trans_cd, " +
	  "t.payment_subtype_class as payment_subtype_class, " +
	  "d.device_name as device_name, " +
	  "d.device_serial_cd as device_serial_cd ";
    
    public static final String SQL_GET_TRAN_LIST_BASE = "from pss.tran t " +
      "join pss.tran_state ts on t.tran_state_cd = ts.tran_state_cd " +
      "join pss.pos_pta pp on t.pos_pta_id = pp.pos_pta_id " +
      "join pss.pos p on pp.pos_id = p.pos_id " +
      "join device.device d on p.device_id = d.device_id " +
      "left outer join pss.sale s on t.tran_id = s.tran_id " +
      "where t.tran_id > 0 ";
           
    public static final String[] SORT_FIELDS_GET_TRAN_LIST = {"s_tran_id", "tran_start_ts", "tran_state_desc", 
      "sale_amount", "tran_received_raw_acct_data", "tran_global_trans_cd", "payment_subtype_class",
      "device_name", "device_serial_cd"};
    
    public static final String DEFAULT_SORT_INDEX_GET_TRAN_LIST = "-2";
    
    public static final String SQL_GET_BATCH_LIST_START = "SELECT terminal_batch.terminal_batch_id as terminal_batch_id, "
	       + "terminal_batch.terminal_batch_num as terminal_batch_num, " 
	       + "to_char(terminal_batch.terminal_batch_open_ts, 'MM/DD/YYYY HH24:MI:SS') as terminal_batch_open_ts, "
	       + "to_char(terminal_batch.terminal_batch_close_ts, 'MM/DD/YYYY HH24:MI:SS') as terminal_batch_close_ts, "
	       + "terminal.terminal_id as terminal_id, " 
	       + "terminal.terminal_cd as terminal_cd, " 
	       + "terminal.terminal_desc as terminal_desc, " 
	       + "terminal_state.terminal_state_name as terminal_state_name, "
	       + "merchant.merchant_id as merchant_id, " 
	       + "merchant.merchant_cd as merchant_cd, "
	       + "merchant.merchant_name as merchant_name, "
	       + "merchant.merchant_desc as merchant_desc, "
	       + "merchant.merchant_bus_name as merchant_bus_name, "
	       + "authority.authority_id as authority_id, " 
	       + "authority.authority_name as authority_name, "
	       + "authority_type.authority_type_name as authority_type_name, "
	       + "authority_type.authority_type_desc as authority_type_desc ";
    
    public static final String SQL_GET_BATCH_LIST_BASE = " FROM pss.terminal_batch, pss.terminal, pss.merchant, authority.authority, pss.terminal_state, authority.authority_type "
	       + "WHERE terminal_batch.terminal_id = terminal.terminal_id "
	       + "AND terminal.merchant_id = merchant.merchant_id "
	       + "AND merchant.authority_id = authority.authority_id "
	       + "AND terminal.terminal_state_id = terminal_state.terminal_state_id "
	       + "AND authority.authority_type_id = authority_type.authority_type_id ";
    
    public static final String[] SORT_FIELDS_GET_BATCH_LIST = {"merchant_name", 
    	"terminal_cd",
    	"terminal_batch_num",
    	"state",
        "terminal_batch_open_ts",
        "terminal_batch_close_ts",
        "authority_name",
        
        //"terminal_id",
        //"terminal_cd",
        //"terminal_desc",
        //"merchant_id", 
        //"merchant_cd",
        //"merchant_name",
        //"merchant_desc",
        //"merchant_bus_name",
        //"authority_id",
        
       // "authority_type_name", 
        //"authority_type_desc"
        };
    
    public static final String DEFAULT_SORT_INDEX_GET_BATCH_LIST = "5";
    
    public static final String[] SORT_FIELDS__BATCH_LIST = {"LOWER(merchant.merchant_name)", 
        "terminal.terminal_cd", 
        "terminal_batch.terminal_batch_num",
        null,
        "COALESCE(terminal_batch.terminal_batch_open_ts, TO_DATE('1970', 'YYYY'))", 
        "COALESCE(terminal_batch.terminal_batch_close_ts, TO_DATE('1970', 'YYYY'))",
        "authority.authority_name" 
        };
    
    
}