/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.transaction;

import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.util.LinkedHashMap;

import com.usatech.dms.util.DMSPaginationStep;
import com.usatech.layers.common.util.StringHelper;

public class TranListStep extends DMSPaginationStep
{
    

    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
        
        int tranId = form.getInt("tran_id", false, -1);
        String tranAfterDate = form.getString("tran_after_date", false);
        String tranBeforeDate = form.getString("tran_before_date", false);
        String serialNumber = form.getString("serial_number", false);
        StringBuilder sql = new StringBuilder("");
        Map params = new LinkedHashMap();
        int paramCntr = 0;
        if (tranId != -1)
        {
        	sql.append(" AND tran_id = ? ");
        	params.put(paramCntr++, tranId);
        
        }
        
        if(!StringHelper.isBlank(tranAfterDate))
        {
        	sql.append(" AND tran_start_ts >= to_date(?, 'MM/DD/YYYY') ");
        	params.put(paramCntr++, tranAfterDate);
        
        }
        
        if(!StringHelper.isBlank(tranBeforeDate))
        {
        	sql.append(" AND tran_start_ts < to_date(?, 'MM/DD/YYYY') + 1 ");
        	params.put(paramCntr++, tranBeforeDate);
        }
        
        if (!StringHelper.isBlank(serialNumber))
        {
        	sql.append(" AND device_serial_cd = ? ");
        	params.put(paramCntr++, serialNumber);
        }
        
        
        String queryBase = TranConstants.SQL_GET_TRAN_LIST_BASE + sql.toString();
        
        setPaginatedResultsOnRequest(form, request, TranConstants.SQL_GET_TRAN_LIST_START, queryBase, "", TranConstants.DEFAULT_SORT_INDEX_GET_TRAN_LIST,
        		TranConstants.SORT_FIELDS_GET_TRAN_LIST, "tranList",((params!=null&&params.size()>0)?params.values().toArray():null),
        		"t.tran_id", "/tran.i?tran_id=");
    }
}
