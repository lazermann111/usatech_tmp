/**
 * $Id$
 * 
 * Copyright 2009 USATechnologies. All rights reserved.
 * 
 * USATECHNOLOGIES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.usatech.dms.transaction;

import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.util.LinkedHashMap;

import com.usatech.dms.util.DMSPaginationStep;
import com.usatech.layers.common.util.StringHelper;

public class BatchListStep extends DMSPaginationStep
{
    

    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
    	String terminalIdTemp = form.getString("terminal_id", false);
    	int terminalId = -1;
    	if(!(StringHelper.isBlank(terminalIdTemp)) && !("Undefined".equalsIgnoreCase(terminalIdTemp))){terminalId   = form.getInt("terminal_id", false, -1);}
        String terminalBatchNum = form.getString("terminal_batch_num", false);
        
        String state = form.getString("state", false);
        String withTran = form.getString("with_tran", false);
                
        int authorityId = form.getInt("authority_id", false, -1);
        String openAfter = form.getString("open_after", false);
        String openBefore = form.getString("open_before", false);
        String closeAfter = form.getString("close_after", false);
        String closeBefore = form.getString("close_before", false);
        
        StringBuilder sql = new StringBuilder("");
        Map<Integer, Object> params = new LinkedHashMap<Integer, Object>();
        int paramCntr = 0;
        if (terminalId != -1)
        {
        	sql.append(" and terminal.terminal_id = CAST (? AS NUMERIC) ");
        	params.put(paramCntr++, terminalId);
        }
        
        if(!StringHelper.isBlank(terminalBatchNum))
        {
        	sql.append("  and terminal_batch.terminal_batch_num = ? ");
        	params.put(paramCntr++, terminalBatchNum);
        }
        
        if(!StringHelper.isBlank(state))
        {
        	if("open".equalsIgnoreCase(state))
        		sql.append(" and terminal_batch.terminal_batch_close_ts is null ");
        	else if("closed".equalsIgnoreCase(state))
        		sql.append(" and terminal_batch.terminal_batch_close_ts is not null ");
        }
        
        if(!StringHelper.isBlank(withTran))
        {
        	if("Y".equalsIgnoreCase(withTran))
        		sql.append(" and (select count(1) from pss.auth where terminal_batch_id = terminal_batch.terminal_batch_id) + (select count(1) from pss.refund where terminal_batch_id = terminal_batch.terminal_batch_id) > 0 ");
        	else if("N".equalsIgnoreCase(withTran))
        		sql.append(" and (select count(1) from pss.auth where terminal_batch_id = terminal_batch.terminal_batch_id) + (select count(1) from pss.refund where terminal_batch_id = terminal_batch.terminal_batch_id) = 0 ");
        }
        
        if (authorityId != -1)
        {
        	sql.append(" and authority.authority_id = ? ");
        	params.put(paramCntr++, authorityId);
        
        }
        
        if(!StringHelper.isBlank(openAfter))
        {
        	sql.append("  and terminal_batch.terminal_batch_open_ts >= to_date(?, 'MM/DD/YYYY') ");
        	params.put(paramCntr++, openAfter);
        }
        
        if(!StringHelper.isBlank(openBefore))
        {
        	sql.append("  and terminal_batch.terminal_batch_open_ts < to_date(?, 'MM/DD/YYYY') + 1 ");
        	params.put(paramCntr++, openBefore);
        	
        }
        
        if(!StringHelper.isBlank(closeAfter))
        {
        	sql.append("  and terminal_batch.terminal_batch_close_ts >= to_date(?, 'MM/DD/YYYY') ");
        	params.put(paramCntr++, closeAfter);
        	
        }
        
        if(!StringHelper.isBlank(closeBefore))
        {
        	sql.append("  and terminal_batch.terminal_batch_close_ts < to_date(?, 'MM/DD/YYYY') + 1 ");
        	params.put(paramCntr++, closeBefore);
        }
        
        String queryBase = TranConstants.SQL_GET_BATCH_LIST_BASE + sql.toString();
        
        setPaginatedResultsOnRequest(form, request, TranConstants.SQL_GET_BATCH_LIST_START, queryBase, "", TranConstants.DEFAULT_SORT_INDEX_GET_BATCH_LIST,
        		TranConstants.SORT_FIELDS__BATCH_LIST, "batchList",((params!=null&params.size()>0)?params.values().toArray():null),
        		"terminal_batch_id", "terminalBatch.i?terminal_batch_id=");
    }
}
