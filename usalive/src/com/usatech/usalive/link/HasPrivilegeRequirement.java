package com.usatech.usalive.link;

import javax.servlet.ServletRequest;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;

import com.usatech.report.ReportingPrivilege;
import com.usatech.usalive.servlet.UsaliveUser;

public class HasPrivilegeRequirement implements LinkRequirement {
	private static final simple.io.Log log = simple.io.Log.getLog();
	private int requirementId;
	private int privilegeId;

	public HasPrivilegeRequirement(int privilegeId){
		this.privilegeId = privilegeId;
	}

	public HasPrivilegeRequirement(Integer privilegeId){
		this.privilegeId = privilegeId.intValue();
	}

	public HasPrivilegeRequirement(String privilegeFieldName) {
		try {
			this.privilegeId = ConvertUtils.convert(ReportingPrivilege.class, privilegeFieldName).getValue();
		} catch (ConvertException e) {
			log.error(e.getMessage(),e);
		}
	}

	public boolean meetsRequirement(UsaliveUser user, ServletRequest request) {
		return user != null && user.hasPrivilege(Integer.toString(privilegeId));
	}

	public int getRequirementId() {
		return requirementId;
	}

	public void setRequirementId(int id) {
		this.requirementId = id;
	}
}
