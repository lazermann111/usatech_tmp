package com.usatech.usalive.link;

import javax.servlet.ServletRequest;

import com.usatech.usalive.servlet.UsaliveUser;

public interface LinkRequirement {	
	public boolean meetsRequirement(UsaliveUser user, ServletRequest request);
	
	public int getRequirementId();
	
	public void setRequirementId(int id);
}
