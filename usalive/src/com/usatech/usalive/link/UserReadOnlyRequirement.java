package com.usatech.usalive.link;

import javax.servlet.ServletRequest;

import com.usatech.usalive.servlet.UsaliveUser;

public class UserReadOnlyRequirement implements LinkRequirement {
	private int requirementId;
	private boolean readOnly;
	
	/**
	 * @param requirementId The database identifier for this requirement
	 * @param isLoggedIn If true, this requires the user to be logged in. If false,
	 * this requires the user to not be logged in.
	 */
	public UserReadOnlyRequirement(boolean readOnly){
		this.readOnly = readOnly;
	}
	
	public UserReadOnlyRequirement(Boolean readOnly){
		this.readOnly = readOnly.booleanValue();
	}

	public boolean meetsRequirement(UsaliveUser user, ServletRequest request) {
		return (user != null && user.isReadOnly() == readOnly);
	}

	public int getRequirementId() {
		return requirementId;
	}
	
	public void setRequirementId(int id) {
		this.requirementId = id;
	}
}
