package com.usatech.usalive.link;

import javax.servlet.ServletRequest;

import com.usatech.usalive.servlet.UsaliveUser;

public class UserLoginRequirement implements LinkRequirement {
	private int requirementId;
	private boolean isLoggedIn;
	
	/**
	 * @param requirementId The database identifier for this requirement
	 * @param isLoggedIn If true, this requires the user to be logged in. If false,
	 * this requires the user to not be logged in.
	 */
	public UserLoginRequirement(boolean isLoggedIn){
		this.isLoggedIn = isLoggedIn;
	}
	
	public UserLoginRequirement(Boolean isLoggedIn){
		this.isLoggedIn = isLoggedIn.booleanValue();
	}

	public boolean meetsRequirement(UsaliveUser user, ServletRequest request) {
		// if isLoggedIn is false, they are not supposed to be logged in
		return ((user == null) || (user.getUserId() == 0)) ? !this.isLoggedIn : this.isLoggedIn;
	}

	public int getRequirementId() {
		return requirementId;
	}
	
	public void setRequirementId(int id) {
		this.requirementId = id;
	}
}
