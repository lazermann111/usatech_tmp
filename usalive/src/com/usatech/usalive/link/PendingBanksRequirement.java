package com.usatech.usalive.link;

import javax.servlet.ServletRequest;

import com.usatech.usalive.servlet.UsaliveUser;

public class PendingBanksRequirement implements LinkRequirement {
	private int requirementId;

	public PendingBanksRequirement(){}

	public boolean meetsRequirement(UsaliveUser user, ServletRequest request) {		
		return user != null && user.getPendingBankAccountCount() > 0;
	}

	public int getRequirementId() {
		return requirementId;
	}

	public void setRequirementId(int id) {
		this.requirementId = id;
	}
}
