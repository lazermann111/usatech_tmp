package com.usatech.usalive.link;

import javax.servlet.ServletRequest;

import com.usatech.usalive.servlet.UsaliveUser;

public class LicenseRequirement implements LinkRequirement {
	private int requirementId;
	private boolean hasLicense;
	
	/**
	 * @param requirementId The database identifier for this requirement
	 * @param hasLicense If true, this requires the user to be licensed. If false,
	 * this requires the user to not be licensed.
	 */
	public LicenseRequirement(boolean hasLicense){
		this.hasLicense = hasLicense;
	}
	
	public LicenseRequirement(Boolean hasLicense){
		this.hasLicense = hasLicense.booleanValue();
	}

	public boolean meetsRequirement(UsaliveUser user, ServletRequest request) {
		// if hasLicense is false, they are not supposed to have one
		return user != null && user.isMissingLicense() ? !this.hasLicense : this.hasLicense;
	}

	public int getRequirementId() {
		return requirementId;
	}
	
	public void setRequirementId(int id) {
		this.requirementId = id;
	}
}
