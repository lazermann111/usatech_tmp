package com.usatech.usalive.link;

import javax.servlet.ServletRequest;
import com.usatech.usalive.servlet.UsaliveUser;

public class IsPartnerRequirement implements LinkRequirement {
	private int requirementId;
	@Override
	public boolean meetsRequirement(UsaliveUser user, ServletRequest request) {
		if(user!=null&& user.isPartner()){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public int getRequirementId() {
		return requirementId;
	}

	@Override
	public void setRequirementId(int id) {
		requirementId=id;
	}

}
