package com.usatech.usalive.link;

import javax.servlet.ServletRequest;
import com.usatech.usalive.servlet.UsaliveUser;

public class HasTerminalRequirement implements LinkRequirement {
	private int requirementId;
	
	@Override
	public boolean meetsRequirement(UsaliveUser user, ServletRequest request) {
		if(user!=null&& user.getTerminalCount()>0){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public int getRequirementId() {
		return requirementId;
	}

	@Override
	public void setRequirementId(int id) {
		requirementId=id;
	}

}
