package com.usatech.usalive.link;

import javax.servlet.ServletRequest;

import com.usatech.usalive.servlet.UsaliveUser;

public class IsOriginalLoginRequirement implements LinkRequirement {
	private int requirementId;
	private boolean isOriginal;
	
	/**
	 * @param requirementId The database identifier for this requirement
	 * @param isLoggedIn If true, this requires the user to be logged in. If false,
	 * this requires the user to not be logged in.
	 */
	public IsOriginalLoginRequirement(boolean isOriginal) {
		this.isOriginal = isOriginal;
	}
	
	public IsOriginalLoginRequirement(Boolean isOriginal) {
		this.isOriginal = isOriginal.booleanValue();
	}

	public boolean meetsRequirement(UsaliveUser user, ServletRequest request) {
		return user != null && user.getUserId() != 0 && (user.getUserId() == user.getMasterUserId() ? this.isOriginal : !this.isOriginal);
	}

	public int getRequirementId() {
		return requirementId;
	}
	
	public void setRequirementId(int id) {
		this.requirementId = id;
	}
}
