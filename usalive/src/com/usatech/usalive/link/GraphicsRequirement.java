package com.usatech.usalive.link;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.servlet.ServletRequest;

import simple.io.Log;
import simple.util.concurrent.RunOnGetFuture;

import com.usatech.usalive.servlet.UsaliveUser;

public class GraphicsRequirement implements LinkRequirement {
	private static final Log log = Log.getLog();
	private int requirementId;
	protected static final RunOnGetFuture<Boolean> available = new RunOnGetFuture<Boolean>() {
		@Override
		protected Boolean call(Object... params) throws Exception {
			long start = System.currentTimeMillis();
			try {
				java.awt.GraphicsEnvironment.getLocalGraphicsEnvironment(); //ensure that graphs work
			} catch(Throwable e) {
				log.warn("Display is NOT available", e);
				return false;
			}
			if(log.isDebugEnabled()) {
				log.debug("Took " + (System.currentTimeMillis() - start) + " milliseconds to get graphics environment");
				log.debug("Display is available with DISPLAY=" + System.getProperty("DISPLAY"));
			}
			return true;
		}
	};

	public boolean meetsRequirement(UsaliveUser user, ServletRequest request) {		
		try {
			return available.get(3000, TimeUnit.MILLISECONDS);
		} catch(InterruptedException e) {
			log.warn("Getting graphics availibility was interrupted", e);
		} catch(ExecutionException e) {
			log.warn("Could not get graphics availibility", e);
		} catch(TimeoutException e) {
			log.warn("Graphics not available in 3000 milliseconds", e);
		}
		return false;
	}

	public int getRequirementId() {
		return requirementId;
	}

	public void setRequirementId(int id) {
		this.requirementId = id;
	}
}
