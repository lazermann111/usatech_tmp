package com.usatech.usalive.link;

import javax.servlet.ServletRequest;

import com.usatech.report.ReportingPrivilege;
import com.usatech.usalive.servlet.UsaliveUser;

public class InternalUserLoginAsCustomerRequirement implements LinkRequirement {
	private int requirementId;
	private boolean internal;

	public InternalUserLoginAsCustomerRequirement() {
		this(true);
	}

	public InternalUserLoginAsCustomerRequirement(boolean customer) {
		this.internal = customer;
	}

	public InternalUserLoginAsCustomerRequirement(Boolean customer) {
		this.internal = customer;
	}

	public boolean meetsRequirement(UsaliveUser user, ServletRequest request) {		
		if(user!=null){
			UsaliveUser masterUser=user.getMasterUser();
			if(masterUser==null){
				return true;
			}else{
				if(masterUser.isInternal()){
					return masterUser.hasPrivilege(ReportingPrivilege.PRIV_LOGIN_AS_CUST);
				}else{
					return true;
				}
			}
		}else{
			return true;
		}
	}

	public int getRequirementId() {
		return requirementId;
	}

	public void setRequirementId(int id) {
		this.requirementId = id;
	}

	public boolean isInternal() {
		return internal;
	}

	public void setInternal(boolean internal) {
		this.internal = internal;
	}
}
