package com.usatech.usalive.link;

import javax.servlet.ServletRequest;

import com.usatech.report.ReportingPrivilege;
import com.usatech.usalive.servlet.UsaliveUser;

public class MassUpdatesRequirement implements LinkRequirement {
	private int requirementId;

	public boolean meetsRequirement(UsaliveUser user, ServletRequest request) {		
		return user != null && (
				user.hasPrivilege(ReportingPrivilege.PRIV_ACTIVATE_TERMINAL)
				|| user.hasPrivilege(ReportingPrivilege.PRIV_CONFIGURE_DEVICE) 
				|| user.hasPrivilege(ReportingPrivilege.PRIV_MANAGE_PREPAID_CONSUMERS));
	}

	public int getRequirementId() {
		return requirementId;
	}

	public void setRequirementId(int id) {
		this.requirementId = id;
	}
}
