package com.usatech.usalive.link;

import javax.servlet.ServletRequest;

import com.usatech.usalive.servlet.UsaliveUser;

public class InternalUserRequirement implements LinkRequirement {
	private int requirementId;
	private boolean internal;

	public InternalUserRequirement() {
		this(true);
	}

	public InternalUserRequirement(boolean customer) {
		this.internal = customer;
	}

	public InternalUserRequirement(Boolean customer) {
		this.internal = customer;
	}

	public boolean meetsRequirement(UsaliveUser user, ServletRequest request) {		
		return (user != null && user.isInternal()) ? internal : !internal;
	}

	public int getRequirementId() {
		return requirementId;
	}

	public void setRequirementId(int id) {
		this.requirementId = id;
	}

	public boolean isInternal() {
		return internal;
	}

	public void setInternal(boolean internal) {
		this.internal = internal;
	}
}
