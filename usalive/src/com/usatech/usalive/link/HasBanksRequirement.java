package com.usatech.usalive.link;

import javax.servlet.ServletRequest;

import com.usatech.usalive.servlet.UsaliveUser;

public class HasBanksRequirement implements LinkRequirement {
	private int requirementId;

	public boolean meetsRequirement(UsaliveUser user, ServletRequest request) {		
		return user != null && user.getActiveBankAccountCount() > 0;
	}

	public int getRequirementId() {
		return requirementId;
	}

	public void setRequirementId(int id) {
		this.requirementId = id;
	}
}
