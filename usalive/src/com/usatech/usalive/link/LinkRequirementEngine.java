package com.usatech.usalive.link;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletRequest;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.Results;

import com.usatech.usalive.servlet.UsaliveUser;

public class LinkRequirementEngine {
	private static final simple.io.Log log = simple.io.Log.getLog();
	private LinkRequirement[] linkRequirements;

	public LinkRequirementEngine(){
		log.debug("Initializing LinkRequirementEngine in constructor");
		loadRequirements();
	}

	public void loadRequirements(){
		HashSet<LinkRequirement> linkRequirementsSet = new HashSet<LinkRequirement>();

		try {
			Connection conn = DataLayerMgr.getConnectionForCall("GET_LINK_REQUIREMENTS");
			try {
				Results results = DataLayerMgr.executeQuery(conn,"GET_LINK_REQUIREMENTS",null);
				while(results.next()) {
					Object id = results.get("REQUIREMENT_ID");
					try {
						LinkRequirement lr = null;
						Class<?> requirementClass = Class.forName((String)results.get("REQUIREMENT_CLASS"));
						String paramClassName = (String)results.get("REQUIREMENT_PARAM_CLASS");

						if((!"".equals(paramClassName)) && (paramClassName != null)){
							Class<?>[] paramClasses = new Class<?>[1];
							Object[] paramValues = new Object[1];

							paramClasses[0] = Class.forName(paramClassName);
							paramValues[0] = ConvertUtils.convert(paramClasses[0], results.get("REQUIREMENT_PARAM_VALUE"));

							lr = (LinkRequirement)requirementClass.getConstructor(paramClasses).newInstance(paramValues);
						} else {
							lr = (LinkRequirement)requirementClass.newInstance();
						}

						lr.setRequirementId(ConvertUtils.convert(Integer.class,id).intValue());

						linkRequirementsSet.add(lr);
						log.debug("Loaded link requirement "+lr.getRequirementId()+" in the engine");
					} catch (ConvertException e) {
						log.error("Could not load link requirment " + id, e);
					} catch (ClassNotFoundException e) {
						log.error("Could not load link requirment " + id, e);
					} catch (IllegalArgumentException e) {
						log.error("Could not load link requirment " + id, e);
					} catch (SecurityException e) {
						log.error("Could not load link requirment " + id, e);
					} catch (InstantiationException e) {
						log.error("Could not load link requirment " + id, e);
					} catch (IllegalAccessException e) {
						log.error("Could not load link requirment " + id, e);
					} catch (InvocationTargetException e) {
						log.error("Could not load link requirment " + id, e);
					} catch (NoSuchMethodException e) {
						log.error("Could not load link requirment " + id, e);
					}
				}
			} finally {
				conn.close();
			}
		} catch (SQLException e) {
			log.error(e.getMessage(),e);
		} catch (DataLayerException e) {
			log.error(e.getMessage(),e);
		}
		this.linkRequirements = linkRequirementsSet.toArray(new LinkRequirement[linkRequirementsSet.size()]);
	}

	public Set<Integer> getSatisfiedRequirements(UsaliveUser user, ServletRequest request){
		Set<Integer> requirementIds = new HashSet<Integer>();
		for(LinkRequirement lr : this.linkRequirements){
			if(lr.meetsRequirement(user, request)){
				requirementIds.add(lr.getRequirementId());
				log.debug("User '"+(user == null ? null : user.getUserName())+"' met link requirement "+lr.getRequirementId());
			} else {
				log.debug("User '"+(user == null ? null : user.getUserName())+"' failed link requirement "+lr.getRequirementId());
			}
		}

		return requirementIds;
	}
}
