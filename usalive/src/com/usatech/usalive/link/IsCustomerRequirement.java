package com.usatech.usalive.link;

import javax.servlet.ServletRequest;

import com.usatech.usalive.servlet.UsaliveUser;

public class IsCustomerRequirement implements LinkRequirement {
	private int requirementId;
	private boolean customer;

	public IsCustomerRequirement() {
		this(true);
	}

	public IsCustomerRequirement(boolean customer) {
		this.customer = customer;
	}

	public IsCustomerRequirement(Boolean customer) {
		this.customer = customer;
	}

	public boolean meetsRequirement(UsaliveUser user, ServletRequest request) {		
		return (user != null && user.getCustomerId() != 0 && user.getCustomerId() != -1) ? customer : !customer;
	}

	public int getRequirementId() {
		return requirementId;
	}

	public void setRequirementId(int id) {
		this.requirementId = id;
	}

	public boolean isCustomer() {
		return customer;
	}

	public void setCustomer(boolean customer) {
		this.customer = customer;
	}
}
