package com.usatech.usalive.link;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import simple.db.DataLayerException;
import simple.falcon.engine.Executor;
import simple.falcon.engine.Report;
import simple.falcon.engine.Scene;
import simple.falcon.servlet.RunReportPreprocessor;
import simple.servlet.InputForm;

import com.usatech.usalive.servlet.USALiveUtils;

public class AddMenuLinksPreprocessor implements RunReportPreprocessor {
	public void preprocess(Report report, Scene scene, Executor executor, InputForm form) throws ServletException {
		try {
			scene.getProperties().put("menuLinks", USALiveUtils.getLinks(form, "M"));
		} catch(SQLException e) {
			throw new ServletException(e);
		} catch(DataLayerException e) {
			throw new ServletException(e);
		} catch(ParserConfigurationException e) {
			throw new ServletException(e);
		} catch(IOException e) {
			throw new ServletException(e);
		} catch(SAXException e) {
			throw new ServletException(e);
		}
	}
}
