package com.usatech.usalive.link;

import java.util.regex.Pattern;

import javax.servlet.ServletRequest;

import com.usatech.usalive.servlet.UsaliveUser;

public class SubdomainRequirement implements LinkRequirement {
	private int requirementId;
	private final Pattern subdomainPattern;
	private final boolean negative;

	/**
	 * @param requirementId The database identifier for this requirement
	 * @param subdomainRegex A regular expression that matches the host name
	 * @param negative If true, this requires the request to be against the specified host name.
	 *                 If false, this requires the request to NOT be against the specified host name.
	 */
	public SubdomainRequirement(String subdomainRegex, boolean negative) {
		this.subdomainPattern = Pattern.compile(subdomainRegex, Pattern.CASE_INSENSITIVE);
		this.negative = negative;
	}

	/**
	 * @param requirementId The database identifier for this requirement
	 * @param subdomain A regular expression that matches the host name. If the host name starts with '!' it is taken as a negative
	 */
	public SubdomainRequirement(String subdomainRegex) {
		this(subdomainRegex.charAt(0) == '!' ? subdomainRegex.substring(1).trim() : subdomainRegex, subdomainRegex.charAt(0) == '!');
	}

	public boolean meetsRequirement(UsaliveUser user, ServletRequest request) {
		String host = request.getServerName();
		if(subdomainPattern.matcher(host).matches())
			return !negative;
		else
			return negative;
	}

	public int getRequirementId() {
		return requirementId;
	}

	public void setRequirementId(int id) {
		this.requirementId = id;
	}
}
