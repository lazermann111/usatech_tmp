package com.usatech.usalive.link;

import javax.servlet.ServletRequest;

import com.usatech.report.ReportingPrivilege;
import com.usatech.usalive.servlet.UsaliveUser;

public class SystemSectionRequirement implements LinkRequirement {
	private int requirementId;

	public boolean meetsRequirement(UsaliveUser user, ServletRequest request) {		
		return user != null && (
				user.hasPrivilege(ReportingPrivilege.PRIV_CUSTOMER_SERVICE)
				|| user.hasPrivilege(ReportingPrivilege.PRIV_FOLIO_DESIGN)
				|| user.hasPrivilege(ReportingPrivilege.PRIV_SYSTEM_ADMIN));
	}

	public int getRequirementId() {
		return requirementId;
	}

	public void setRequirementId(int id) {
		this.requirementId = id;
	}
}
