package com.usatech.usalive.ws.handler;

import static simple.text.MessageFormat.format;

import java.util.HashSet;
import java.util.Set;

import com.usatech.usalive.ws.UsaLiveResponse;

public abstract class AbstractUsaLiveWebServiceHandler
{
	public static final String CONTACT_PROMPT = "We are working to resolve this. Please try again later.";
	public static final String INTERNAL_ERROR = "An internal error occurred on the server. " + CONTACT_PROMPT;

	public static <T extends UsaLiveResponse> T createContactUsResponse(String cause, Class<T> classType)
	{
		return createResponse(UsaLiveResponse.ERROR, format("{0} {1}", cause, CONTACT_PROMPT), classType);
	}
	
	public static <T extends UsaLiveResponse> T createResponse(int responseCode, String responseMessage, Class<T> classType)
	{
		Set<String> responseMessages = new HashSet<String>();
		responseMessages.add(responseMessage);
		
		return createResponse(responseCode, responseMessages, classType);
	}

	public static <T extends UsaLiveResponse> T createResponse(int responseCode, Set<String> responseMessages, Class<T> classType)
	{
		try {
			T response = classType.newInstance();
			response.setResponseCode(responseCode);;
			response.setResponseMessages(responseMessages.toArray(new String[responseMessages.size()]));
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
