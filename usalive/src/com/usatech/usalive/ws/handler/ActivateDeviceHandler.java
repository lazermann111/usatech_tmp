package com.usatech.usalive.ws.handler;

import static simple.text.MessageFormat.format;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.DbUtils;
import simple.db.NotEnoughRowsException;
import simple.io.Log;
import simple.io.logging.PrefixedLog;
import simple.lang.Holder;
import simple.results.BeanException;
import simple.results.Results;
import simple.servlet.InputForm;
import simple.servlet.MapInputForm;
import simple.servlet.SimpleServlet;
import simple.text.StringUtils;
import simple.util.NameValuePair;

import com.usatech.layers.common.AppLayerUtils;
import com.usatech.layers.common.AppLayerUtils.InitInfo;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.fees.EntryType;
import com.usatech.layers.common.fees.ExchangeType;
import com.usatech.layers.common.fees.FeeType;
import com.usatech.layers.common.fees.ProcessFeeType;
import com.usatech.layers.common.fees.RateType;
import com.usatech.layers.common.fees.ServiceFeeType;
import com.usatech.layers.common.fees.TerminalFees;
import com.usatech.layers.common.messagedata.MessageData_AD;
import com.usatech.report.TerminalInfo;
import com.usatech.usalive.servlet.UsaliveUser;
import com.usatech.usalive.ws.ActivateDeviceRequest;
import com.usatech.usalive.ws.Device;
import com.usatech.usalive.ws.DeviceIds;
import com.usatech.usalive.ws.Fees;
import com.usatech.usalive.ws.Location;
import com.usatech.usalive.ws.UsaLiveResponse;

public class ActivateDeviceHandler extends AbstractUsaLiveWebServiceHandler
{
	private static final Log log = Log.getLog();
	
	private static final DeviceIds INTERNAL_ERROR_RESPONSE = createResponse(UsaLiveResponse.ERROR, INTERNAL_ERROR, DeviceIds.class);
	
	// hard coding some of this for now to get it done faster
	private static final long encryptionKeyMinAgeMin = 1440;
	
	private static final String DEVICE_TYPE = "KIOSK";
	private static final int DEVICE_SUB_TYPE = 3;
	private static final Pattern WEBSERVICE_SERIAL_PATTERN = Pattern.compile("^K3([A-Z]{2})[0-9A-Z]{6,26}$");
	
	private static final String MACHINE_MAKE = "TBD";
	private static final String MACHINE_MODEL = "To Be Determined";
	
	private static final int PAY_SCHEDULE_AS_ACCUMULATED = 1;
	
	private static final String DEFAULT_LOCATION_TYPE_STRING = "- Not Specified -";
	private static final String DEFAULT_PRODUCT_TYPE_STRING = "- Not Specified -";
	private static final String DEFAULT_BUSINESS_TYPE_STRING = "ePort Mobile";
	
	private static final String DEFAULT_POS_PTA_TMPL_SETTING_CODE = "USALIVE_WEBSERVICE_K3_DEFAULT_POS_PTA_TMPL_ID";
	
	private static Pattern ORA_ERROR_PATTERN = Pattern.compile("ORA-\\d+: (.*?)$.*$", Pattern.DOTALL|Pattern.MULTILINE);
	
	public static DeviceIds activateDevice(ActivateDeviceRequest request, UsaliveUser creatingUser)
	{
		// TODO: track time for WebHelper.publishAppRequestRecord
		long startTsMs = System.currentTimeMillis();

		// validate the easy declarative stuff first
		Set<String> violations = ValidationUtil.validate(request);
		if(!violations.isEmpty()) {
			return createResponse(UsaLiveResponse.ERROR, violations, DeviceIds.class);
		}

		Device device = request.getDevice();
		String serialNumber = device.getSerialNumber();

		// dump out immediately if we didn't get a creating user; this shouldn't happen
		if(creatingUser == null) {
			log.warn("Attempt to activateDevice with null creatingUser: serialNumber={0}", serialNumber);
			return INTERNAL_ERROR_RESPONSE;
		}

		// TODO: eventually we'll need to handle different device types and thus different serial number formats
		if(!WEBSERVICE_SERIAL_PATTERN.matcher(serialNumber).matches()) {
			violations.add(format("Invalid device serial number ''{0}''.", serialNumber));
		}

		// validate the location and lookup the city/state

		Location deviceLocation = device.getLocation();

		InputForm timeZoneForm = new MapInputForm(new HashMap<String, Object>());
		timeZoneForm.setAttribute("countryCd", deviceLocation.getCountryCd());
		timeZoneForm.setAttribute("postalCd", deviceLocation.getPostalCd());

		int timeZoneId = -1;

		try {
			DataLayerMgr.selectInto("LOOKUP_POSTAL", timeZoneForm);
			timeZoneId = timeZoneForm.getInt("timeZoneId", true, -1);
		} catch(ServletException | DataLayerException | SQLException | BeanException e) {
			violations.add(format("Invalid device location postal code ''{0}'' or country code ''{1}''.", deviceLocation.getPostalCd(), deviceLocation.getCountryCd()));
		}
		
		// validate these here?  ACTIVATE_DEVICE call will validate them but it would be nicer to throw a validation 
		// error now and cut off processing earlier than that
		String locationType = device.getLocationType() != null ? device.getLocationType() : DEFAULT_LOCATION_TYPE_STRING;
		String productType = device.getProductType() != null ? device.getProductType() : DEFAULT_PRODUCT_TYPE_STRING;
		String businessType = device.getBusinessType() != null ? device.getBusinessType() : DEFAULT_BUSINESS_TYPE_STRING;

		String assetNumber = device.getCustomerAssetNumber();
		if(StringUtils.isBlank(assetNumber))
			assetNumber = "TBD";

		if(!violations.isEmpty()) {
			return createResponse(UsaLiveResponse.ERROR, violations, DeviceIds.class);
		}

		Log myLog = new PrefixedLog(log, format("activateDevice(creatingUser={0}, serialNumber={1}): ", creatingUser.getUserName(), device.getSerialNumber()));

		// make sure this device does not already exist
		/*
		boolean deviceExists = false;
		try {
			Results deviceExistsResults = DataLayerMgr.executeQuery("GET_DEVICE_COUNT_BY_SERIAL_NUMBER", new Object[] {serialNumber});
			if(deviceExistsResults.next()) {
				deviceExists = (deviceExistsResults.getValue("device_count", Integer.class) > 0);
			}
		} catch(SQLException | DataLayerException e) {
			myLog.warn("GET_DEVICE_COUNT_BY_SERIAL_NUMBER threw exception", e);
			return INTERNAL_ERROR_RESPONSE;
		} catch(ConvertException e) {
			myLog.warn("deviceCount is not an integer");
			return INTERNAL_ERROR_RESPONSE;
		}

		if(deviceExists) {
			return createContactUsResponse(format("Device serial number ''{0}'' already exists.", serialNumber), DeviceIds.class);
		}
		*/
		InputForm creatingUserForm = new MapInputForm(new HashMap<String, Object>());
		creatingUserForm.setAttribute(SimpleServlet.ATTRIBUTE_USER, creatingUser);

		// verify this user can activate a device on this bank account
		long commissionBankAccountId = request.getCommissionBankAccountId();

		Results commissionBankAccountsResults = null;
		try {
			commissionBankAccountsResults = DataLayerMgr.executeQuery("GET_RESELLER_BANK_ACCOUNTS", creatingUserForm, false);
		} catch(NotEnoughRowsException e) {
			myLog.debug("no commission bank accounts available");
			return createContactUsResponse(format("No active bank accounts were found."), DeviceIds.class);
		} catch(SQLException | DataLayerException e) {
			myLog.warn("GET_BANK_ACCOUNTS threw exception", e);
			return INTERNAL_ERROR_RESPONSE;
		}

		boolean foundCommissionBank = false;
		try {
			while(commissionBankAccountsResults.next()) {
				long id = commissionBankAccountsResults.getValue("custBankId", Long.class);
				if(commissionBankAccountId == id) {
					foundCommissionBank = true;
					break;
				}
			}
		} catch(ConvertException e) {
			myLog.warn("commission custBankId {0} is not an integer", commissionBankAccountsResults.getValue("custBankId"));
			return INTERNAL_ERROR_RESPONSE;
		}

		if(!foundCommissionBank) {
			myLog.debug("commission bank account {0} was not found", commissionBankAccountId);
			return createContactUsResponse(format("Commission bank account ''{0}'' was not found.", commissionBankAccountId), DeviceIds.class);
		}

		// verify this user can activate a device on this bank account
		long bankAccountId = request.getBankAccountId();

		Results bankAccountsResults = null;
		try {
			bankAccountsResults = DataLayerMgr.executeQuery("GET_BANK_ACCTS_FOR_CUSTOMER", creatingUserForm, false);
		} catch(NotEnoughRowsException e) {
			myLog.debug("no child bank accounts available");
			return createContactUsResponse(format("No active merchant bank accounts were found."), DeviceIds.class);
		} catch(SQLException | DataLayerException e) {
			myLog.warn("GET_RESELLER_BANK_ACCOUNTS threw exception", e);
			return INTERNAL_ERROR_RESPONSE;
		}

		boolean foundBank = false;
		try {
			while(bankAccountsResults.next()) {
				long id = bankAccountsResults.getValue("custBankId", Long.class);
				if(bankAccountId == id) {
					foundBank = true;
					break;
				}
			}
		} catch(ConvertException e) {
			myLog.warn("merchant custBankId {0} is not an integer", bankAccountsResults.getValue("custBankId"));
			return INTERNAL_ERROR_RESPONSE;
		}

		if(!foundBank) {
			myLog.debug("Bank account {0} was not found", bankAccountId);
			return createContactUsResponse(format("Bank account ''{0}'' was not found.", bankAccountId), DeviceIds.class);
		}
		
		// now find the relevant ids for this bank account
		InputForm lookupDealerForm = new MapInputForm(new HashMap<String, Object>());
		lookupDealerForm.setAttribute("customerBankId", bankAccountId);

		long customerId = -1;
		long dealerId = -1;
		long licenseId = -1;
		String licenseNbr = null;
		
		try {
			DataLayerMgr.selectInto("GET_CUSTOMER_DEALER_LICENSE_BY_BANK_ACCOUNT", lookupDealerForm);
			customerId = lookupDealerForm.getLong("customerId", true, -1);
			dealerId = lookupDealerForm.getLong("dealerId", true, -1);
			licenseId = lookupDealerForm.getLong("licenseId", true, -1);
			licenseNbr = lookupDealerForm.getString("licenseNbr", true);
		} catch(SQLException e) {
			myLog.debug("customer/dealer/license data was not found for bank account {0}", bankAccountId);
			return createContactUsResponse(format("A license is not configured for bank account ''{0}''.", bankAccountId), DeviceIds.class);
		} catch(DataLayerException | BeanException | ServletException e) {
			myLog.warn("GET_DEALER_BY_BANK_ACCOUNT for bankAccountId={0} threw exception", bankAccountId, e);
			return INTERNAL_ERROR_RESPONSE;
		}

		// now verify the quickconnect username and lookup the quickconnect id
		String quickConnectUserName = device.getQuickConnectUserName();

		InputForm lookupQuickConnectForm = new MapInputForm(new HashMap<String, Object>());
		lookupQuickConnectForm.setAttribute("username", quickConnectUserName);

		long quickConnectId = -1;
		try {
			DataLayerMgr.selectInto("GET_CREDENTIAL_INFO_BY_USERNAME", lookupQuickConnectForm);
			quickConnectId = lookupQuickConnectForm.getLong("credentialId", true, -1);
		} catch(SQLException e) {
			myLog.debug("credential id not found for username {0}", quickConnectUserName);
			return createContactUsResponse(format("Invalid quick connect username ''{0}''.", quickConnectUserName), DeviceIds.class);
		} catch(DataLayerException | BeanException | ServletException e) {
			myLog.warn("GET_CREDENTIAL_INFO_BY_USERNAME for username={0} threw exception", quickConnectUserName, e);
			return INTERNAL_ERROR_RESPONSE;
		}

		// now we can start creating the device
		// first initialize the device
		boolean success = false;
		Connection connection = null;

		try {
			connection = DataLayerMgr.getConnection("report", false);
			
			// TODO: add serial number validation such that a customer can only
			// activate serial numbers in ranges assigned to them
			
			// register this eport serial number first
			/*
			long eportId = -1;

			InputForm registerEportForm = new MapInputForm(new HashMap<String, Object>());
			registerEportForm.setAttribute("serialNumber", serialNumber);
			registerEportForm.setAttribute("deviceType", DeviceType.valueOf(DEVICE_TYPE).getValue());
			registerEportForm.setAttribute("dealerId", dealerId);

			try {
				DataLayerMgr.executeUpdate(connection, "EPORT_INS", registerEportForm);
				eportId = (Long)registerEportForm.getLong("eportId", true, -1);
			} catch(Exception e) {
				myLog.warn("EPORT_INS threw exception", e);
				return INTERNAL_ERROR_RESPONSE;
			}
			
			try {
				DataLayerMgr.executeUpdate(connection, "DEALER_EPORT_INS", registerEportForm);
			} catch(Exception e) {
				myLog.warn("DEALER_EPORT_INS threw exception", e);
				return INTERNAL_ERROR_RESPONSE;
			}
			*/
			// activated the device before initializing it.  This prevents unactivated devices from
			// sending transactions with no associated USALive customer account

			//InputForm activationForm = new MapInputForm(new HashMap<String, Object>());
			TerminalInfo terminal = new TerminalInfo(serialNumber);
			terminal.USER_ID = creatingUser.getUserId();
			terminal.DEALER_ID = dealerId;
			terminal.ASSET_NUMBER = assetNumber;
			terminal.MACHINE_MAKE = MACHINE_MAKE;
			terminal.MACHINE_MODEL = MACHINE_MODEL;
			terminal.TELEPHONE = device.getLocationPhone();
			terminal.REGION = device.getCustomerRegion();
			terminal.LOCATION_NAME = device.getLocationName();
			terminal.LOCATION_DETAILS = device.getLocationSpecify();
			terminal.ADDRESS = deviceLocation.getAddress();
			terminal.ZIP = deviceLocation.getPostalCd();
			terminal.COUNTRY = deviceLocation.getCountryCd();
			terminal.CUSTOMER_BANK_ID = bankAccountId;
			terminal.PAY_SCHEDULE = PAY_SCHEDULE_AS_ACCUMULATED;
			terminal.LOCATION_TYPE = locationType;
			terminal.LOCATION_TYPE_SPECIFY = device.getLocationTypeSpecify();
			terminal.PRODUCT_TYPE = productType;
			terminal.PRODUCT_TYPE_SPECIFY = device.getProductTypeSpecify();
			terminal.TIME_ZONE = timeZoneId;
			terminal.DOING_BUSINESS_AS = device.getDba();
			terminal.COMMISSION_BANK_ID = commissionBankAccountId;
			terminal.BUSINESS_TYPE = businessType;
			terminal.CUSTOMER_SERVICE_PHONE = device.getCustomerServicePhone();
			terminal.CUSTOMER_SERVICE_EMAIL = device.getCustomerServiceEmail();

			// figure out the appropriate card entry capability from the passed indicators
			char mobileIndicator = device.getMobileDeviceIndicator().toUpperCase().charAt(0);
			char cardEntryKeyed = device.getEntryTypeKeypadIndicator().toUpperCase().charAt(0);
			char cardEntrySwipe = device.getEntryTypeCardSwipeIndicator().toUpperCase().charAt(0);
			char cardEntryProximity = device.getEntryTypeProximityIndicator().toUpperCase().charAt(0);
			char cardEntryEMV = device.getEntryTypeEmvIndicator().toUpperCase().charAt(0);
			char signatureCaptured = device.getSignatureCapturedIndicator().toUpperCase().charAt(0);

			Set<Character> entryList = new HashSet<Character>();
			char posEnvironment = '?';
			switch(mobileIndicator) {
				case 'Y':
					entryList.add('M');
					posEnvironment = 'C'; // Mobile
					break;
				default:
					char attendedIndicator = device.getAttendedIndicator().toUpperCase().charAt(0);
					switch(attendedIndicator) {
						case 'Y':
							if(entryList.contains('S'))
								posEnvironment = 'A'; // Attended
							else
								posEnvironment = 'M'; // MOTO
							break;
						default:
							if(!entryList.contains('M') || entryList.contains('S') || entryList.contains('P') || entryList.contains('E'))
								posEnvironment = 'U'; // Unattended
							else
								posEnvironment = 'E'; // Ecommerce
							break;
					}
			}

			if(cardEntryKeyed == 'Y')
				entryList.add('M');
			if(cardEntrySwipe == 'Y')
				entryList.add('S');
			if(cardEntryProximity == 'Y')
				entryList.add('P');
			if(cardEntryEMV == 'Y')
				entryList.add('E');
			if(signatureCaptured == 'Y')
				entryList.add('X');

			char pinCapability = (entryList.contains('M') ? 'Y' : 'N');

			char[] entryCapabilityChars = new char[entryList.size()];
			int i = 0;
			for(Character ec: entryList)
				entryCapabilityChars[i++] = ec;

			String entryCapability = new String(entryCapabilityChars);

			terminal.POS_ENVIRONMENT = posEnvironment;
			terminal.ENTRY_CAPABILITY = entryCapability;
			terminal.PIN_CAPABILITY = pinCapability;

			long terminalId = -1;

			try {
				DataLayerMgr.executeUpdate(connection, "ACTIVATE_DEVICE", terminal);
				terminalId = terminal.TERMINAL_ID;
			}  catch(SQLException e) {
				int errorCode = Math.abs(e.getErrorCode());
				boolean returnSqlError = false;
				String sqlMessage = null;
				switch(errorCode) {
					case 20201: // Invalid serial number
					case 20211: // Invalid location type name
					case 20212: // Invalid product type name
					case 20213: // Invalid business type name, Invalid region id
					case 20220: // Invalid state/country
					case 20205: // Eport has already been activated
					case 20206: // Eport already in use
					case 20202: // Invalid program specified
					case 20203: // Invalid customer bank, Invalid primary contact
					case 20204: // No license agreement found for this customer, No license agreement found for this dealer
						if (e.getMessage() != null) {
							Matcher matcher = ORA_ERROR_PATTERN.matcher(e.getMessage());
							if (matcher.matches()) {
								returnSqlError = true;
								sqlMessage = matcher.group(1);
							}
						}
				}
				if (returnSqlError) {
					myLog.warn("ACTIVATE_DEVICE failed: {0}", sqlMessage);
					return createResponse(UsaLiveResponse.ERROR, sqlMessage, DeviceIds.class);
				} else {
					myLog.warn("ACTIVATE_DEVICE threw exception", e);
					return INTERNAL_ERROR_RESPONSE;
				}
			} catch(DataLayerException e) {
				myLog.warn("ACTIVATE_DEVICE threw exception", e);
				return INTERNAL_ERROR_RESPONSE;
			}
			
			// immediately accept the terminal
			try {
				DataLayerMgr.executeCall(connection, "ACCEPT_NEW_TERMINAL", new Long[] {terminalId});
			} catch(DataLayerException e) {
				myLog.warn("ACCEPT_NEW_TERMINAL threw exception: terminalId={0}", terminalId, e);
				return INTERNAL_ERROR_RESPONSE;
			}
			
			// now initialize the device
			Holder<InitInfo> initInfoHolder = new Holder<>();

			MessageData_AD messageData = new MessageData_AD();
			messageData.setDeviceType(DeviceType.valueOf(DEVICE_TYPE));
			messageData.setDeviceSerialNum(serialNumber);

			try {
				AppLayerUtils.processOfflineInit(messageData, log, encryptionKeyMinAgeMin, true, initInfoHolder);
				//AppLayerUtils.processInit(message, data, myLog, credentialId, defaultFilePacketSize)
			} catch(Exception e) {
				myLog.warn("AppLayerUtils.processOfflineInit threw exception", e);
				return INTERNAL_ERROR_RESPONSE;
			}

			if(initInfoHolder.getValue() == null || initInfoHolder.getValue() == null || initInfoHolder.getValue().deviceId <= 0) {
				myLog.warn("AppLayerUtils.processOfflineInit returned invalid InitInfo");
				return INTERNAL_ERROR_RESPONSE;
			}

			// setup the quick connect credentials 
			long deviceId = initInfoHolder.getValue().deviceId;

			try {
				DataLayerMgr.executeUpdate(connection, "UPDATE_DEVICE_CREDENTIAL_BY_SERIAL_NUMBER", new Object[] {quickConnectId, serialNumber});
			} catch(DataLayerException e) {
				myLog.warn("UPDATE_DEVICE_CREDENTIAL_BY_SERIAL_NUMBER threw exception: credentialId={0}, serialNumber={1}", quickConnectId, serialNumber, e);
				return INTERNAL_ERROR_RESPONSE;
			}

			// lookup the default payment template from app_settings
			long posPtaTmplId = -1;

			InputForm defaultTemplateForm = new MapInputForm(new HashMap<String, Object>());
			defaultTemplateForm.setAttribute("code", DEFAULT_POS_PTA_TMPL_SETTING_CODE);

			try {
				DataLayerMgr.selectInto(connection, "GET_APP_SETTING", defaultTemplateForm);
				posPtaTmplId = defaultTemplateForm.getLong("value", true, -1);
			} catch(Exception e) {
				myLog.warn("GET_APP_SETTING threw exception: code={0}", DEFAULT_POS_PTA_TMPL_SETTING_CODE, e);
				return INTERNAL_ERROR_RESPONSE;
			}

			// import the payment template so the device can accept cards immediately
			InputForm importTemplateForm = new MapInputForm(new HashMap<String, Object>());
			importTemplateForm.setAttribute("device_id", deviceId);
			importTemplateForm.setAttribute("pos_pta_template_id", posPtaTmplId);
			importTemplateForm.setAttribute("mode_cd", "CO");
			importTemplateForm.setAttribute("order_cd", "AE");
			importTemplateForm.setAttribute("set_terminal_cd_to_serial", "N");
			importTemplateForm.setAttribute("only_no_two_tier_pricing", "N");

			Object[] importTemplateResults = null;

			try {
				importTemplateResults = DataLayerMgr.executeCall(connection, "SP_IMPORT_POS_PTA_TEMPLATE", importTemplateForm);
			} catch(Exception e) {
				myLog.warn("SP_IMPORT_POS_PTA_TEMPLATE threw exception: deviceId={0}, posPtaTmplId={1}", deviceId, posPtaTmplId, e);
			}

			if(importTemplateResults == null || importTemplateResults.length != 3) {
				myLog.warn("SP_IMPORT_POS_PTA_TEMPLATE returned unexpected results: deviceId={0}, posPtaTmplId={1}", deviceId, posPtaTmplId);
				return INTERNAL_ERROR_RESPONSE;
			}

			if(ConvertUtils.getIntSafely(importTemplateResults[1], -1) != 1) {
				myLog.warn("SP_IMPORT_POS_PTA_TEMPLATE returned error result code: code={0}, message={1}, deviceId={2}, posPtaTmplId={3}", importTemplateResults[1], importTemplateResults[2], deviceId, posPtaTmplId);
				return INTERNAL_ERROR_RESPONSE;
			}
			
			Fees fees = request.getFees();
			
			FeeType cardPresent = FeeType.getById(EntryType.PROCESS_FEE, ProcessFeeType.CREDIT_CARD_PRESENT_ID);
			FeeType cardNotPresent = FeeType.getById(EntryType.PROCESS_FEE, ProcessFeeType.CREDIT_MANUAL_ENTRY_ID);
			FeeType monthlyServiceFee = FeeType.getById(EntryType.SERVICE_FEE, ServiceFeeType.STANDARD_SERVICE_FEE_ID);

			TerminalFees terminalFees = TerminalFees.loadByTerminalId(connection, terminalId);
			terminalFees.getFee(EntryType.PROCESS_FEE, ExchangeType.SELL_RATE, cardPresent, RateType.PERCENT).setValue(fees.getCardPresentFee().getPercent());
			terminalFees.getFee(EntryType.PROCESS_FEE, ExchangeType.SELL_RATE, cardPresent, RateType.FLAT).setValue(fees.getCardPresentFee().getAmount());
			terminalFees.getFee(EntryType.PROCESS_FEE, ExchangeType.SELL_RATE, cardPresent, RateType.MIN).setValue(fees.getCardPresentFee().getMinimumAmount());
			terminalFees.getFee(EntryType.PROCESS_FEE, ExchangeType.SELL_RATE, cardNotPresent, RateType.PERCENT).setValue(fees.getCardNotPresentFee().getPercent());
			terminalFees.getFee(EntryType.PROCESS_FEE, ExchangeType.SELL_RATE, cardNotPresent, RateType.FLAT).setValue(fees.getCardNotPresentFee().getAmount());
			terminalFees.getFee(EntryType.PROCESS_FEE, ExchangeType.SELL_RATE, cardNotPresent, RateType.MIN).setValue(fees.getCardNotPresentFee().getMinimumAmount());
			terminalFees.getFee(EntryType.SERVICE_FEE, ExchangeType.SELL_RATE, monthlyServiceFee, RateType.FLAT).setValue(fees.getServiceFee().getAmount());
			terminalFees.setCommissionBankId(commissionBankAccountId);
			
			try {
				terminalFees.saveToDatabase(connection);
			} catch (Exception e) {
				myLog.warn("terminalFees.saveToDatabase threw exception: terminalId={0}", terminalId, e);
				return INTERNAL_ERROR_RESPONSE;
			}

			//WebHelper.publishAppRequestRecord(USALiveUtils.APP_CD, request, ci, startTsMs, "Activate Device", "terminal", String.valueOf(terminalId), columnValues, attributes, log);

			success = true;
			
			DeviceIds response = new DeviceIds();
			response.setDeviceId(deviceId);
			// response.setEportId(eportId);
			response.setTerminalId(terminalId);

			myLog.info("SUCCESS: {0}", response);

			return response;
		} catch(Throwable e) {
			log.error("caught unexpected exception", e);
			return INTERNAL_ERROR_RESPONSE;
		} finally {
			if(connection != null) {
				try {
					DbUtils.commitOrRollback(connection, success, true);
				} catch(SQLException e) {
					log.warn("final {0} failed", (success ? "commit" : "rollback"), e);
				}
			}
		}
	}

	public static List<NameValuePair> getSerialFormat(int deviceTypeId) throws SQLException, DataLayerException
	{
		List<NameValuePair> serialFormat = new ArrayList<NameValuePair>();
		Results result = DataLayerMgr.executeQuery("GET_SERIAL_FORMAT", new Integer[] {deviceTypeId});
		while(result.next()) {
			serialFormat.add(new NameValuePair(result.getFormattedValue("device_serial_format_id"), result.getFormattedValue("device_serial_format_name")));
		}
		return serialFormat;
	}

}
