package com.usatech.usalive.ws.handler;

import static simple.text.MessageFormat.format;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.servlet.ServletException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.DbUtils;
import simple.db.NotEnoughRowsException;
import simple.io.Log;
import simple.io.logging.PrefixedLog;
import simple.results.BeanException;
import simple.servlet.GenerateUtils;
import simple.servlet.GenerateUtils.Country;
import simple.servlet.InputForm;
import simple.servlet.MapInputForm;
import simple.servlet.SimpleServlet;
import simple.text.Censor;

import com.usatech.layers.common.fees.EntryType;
import com.usatech.layers.common.fees.ExchangeType;
import com.usatech.layers.common.fees.FeeType;
import com.usatech.layers.common.fees.LicenseFees;
import com.usatech.layers.common.fees.ProcessFeeType;
import com.usatech.layers.common.fees.RateType;
import com.usatech.layers.common.fees.ServiceFeeType;
import com.usatech.usalive.servlet.CheckPasswordStep;
import com.usatech.usalive.servlet.UsaliveUser;
import com.usatech.usalive.ws.BankAccount;
import com.usatech.usalive.ws.BankContact;
import com.usatech.usalive.ws.CreateCustomerRequest;
import com.usatech.usalive.ws.Customer;
import com.usatech.usalive.ws.CustomerIds;
import com.usatech.usalive.ws.Location;
import com.usatech.usalive.ws.UsaLiveResponse;
import com.usatech.usalive.ws.User;

public class CreateCustomerHandler extends AbstractUsaLiveWebServiceHandler
{
	private static final Log log = Log.getLog();
	
	private static final CustomerIds INTERNAL_ERROR_RESPONSE = createResponse(UsaLiveResponse.ERROR, INTERNAL_ERROR, CustomerIds.class);
	
	
	public static CustomerIds createCustomer(CreateCustomerRequest request, UsaliveUser creatingUser)
	{
		// validate the easy declarative stuff first
		Set<String> violations = ValidationUtil.validate(request);
		if(!violations.isEmpty()) {
			return createResponse(UsaLiveResponse.ERROR, violations, CustomerIds.class);
		}

		Customer customer = request.getCustomer();
		Location customerLocation = customer.getLocation();
		User primaryUser = request.getPrimaryUser();
		BankAccount bankAccount = request.getBankAccount();
		com.usatech.usalive.ws.Fees fees = request.getFees();

		// dump out immediately if we didn't get a creating user; this shouldn't happen
		if(creatingUser == null) {
			log.warn("Attempt to createCustomer with null creatingUser: customerName={0}, email={1}", customer.getName(), primaryUser.getEmail());
			return INTERNAL_ERROR_RESPONSE;
		}
		
		long creatingUserCustomerId = creatingUser.getCustomerId();

		Log myLog = new PrefixedLog(log, format("createCustomer(creatingUser={0}, customerName={1}, email={2}): ", creatingUser.getUserName(), customer.getName(), primaryUser.getEmail()));

		boolean success = false;
		Connection connection = null;

		try {
			connection = DataLayerMgr.getConnection("report", false);
			
			// now validate stuff that can't be done easily declaratively
			Country customerCountry = GenerateUtils.getCountry(customerLocation.getCountryCd());
			if(!GenerateUtils.checkPostal(customerCountry.code, customerLocation.getPostalCd(), false, null)) {
				violations.add(format("Customer postal code ''{0}'' is invalid format for country ''{1}''.", customerLocation.getPostalCd(), customerLocation.getCountryCd()));
			}

			try {
				if(customer.getDba() != null) {
					String dba = Censor.sanitizeText(customer.getDba());
					if(!dba.equals(customer.getDba())) {
						violations.add(format("Censored word detected in dba ''{0}''.", customer.getDba()));
					}
				}
			} catch(IOException e) {
				myLog.warn("Censor.sanitizeText threw exception", e);
				return INTERNAL_ERROR_RESPONSE;
			}

			// start building a collection of data to pass to the stored proc
			InputForm createCustomerForm = new MapInputForm(new HashMap<String, Object>());

			// many data layer method expect this attribute
			createCustomerForm.setAttribute(SimpleServlet.ATTRIBUTE_USER, creatingUser);

			createCustomerForm.setAttribute("username", primaryUser.getUserName());
			createCustomerForm.setAttribute("firstName", primaryUser.getFirstName());
			createCustomerForm.setAttribute("lastName", primaryUser.getLastName());
			createCustomerForm.setAttribute("email", primaryUser.getEmail());
			createCustomerForm.setAttribute("editPassword", primaryUser.getPassword());
			createCustomerForm.setAttribute("editConfirm", primaryUser.getPassword());
			createCustomerForm.setAttribute("customerName", customer.getName());
			createCustomerForm.setAttribute("address1", customerLocation.getAddress());
			createCustomerForm.setAttribute("postalCd", customerLocation.getPostalCd());
			createCustomerForm.setAttribute("postal", customerLocation.getPostalCd());
			createCustomerForm.setAttribute("countryCd", customerCountry.code);
			createCustomerForm.setAttribute("country", customerCountry.code);
			createCustomerForm.setAttribute("telephone", primaryUser.getPhone());
			createCustomerForm.setAttribute("fax", primaryUser.getFax());
			createCustomerForm.setAttribute("doingBusinessAs", customer.getDba());
			createCustomerForm.setAttribute("parentCustomerId", creatingUserCustomerId);

			try {
				DataLayerMgr.selectInto(connection, "LOOKUP_POSTAL", createCustomerForm);
				TimeZone timeZone = TimeZone.getTimeZone(createCustomerForm.getString("timeZoneGuid", true));
				if(timeZone == null)
					throw new ServletException();
				createCustomerForm.setAttribute("timeZone", timeZone);
			} catch(ServletException | NotEnoughRowsException | BeanException e) {
				violations.add(format("Invalid customer postal code ''{0}'' or country code ''{1}''.", customerLocation.getPostalCd(), customerCountry.code));
			}

			// validate bank account
			Location bankLocation = bankAccount.getLocation();

			Country bankCountry = GenerateUtils.getCountry(bankLocation.getCountryCd());
			if(!GenerateUtils.checkPostal(bankCountry.code, bankLocation.getPostalCd(), false, null)) {
				violations.add(format("Bank account postal code ''{0}'' is invalid format for country ''{1}''.", bankLocation.getPostalCd(), bankCountry.code));
			}

			InputForm lookupBankCityStateForm = new MapInputForm(new HashMap<String, Object>());
			lookupBankCityStateForm.setAttribute("postalCd", bankLocation.getPostalCd());
			lookupBankCityStateForm.setAttribute("countryCd", bankCountry.code);

			try {
				DataLayerMgr.selectInto(connection, "LOOKUP_POSTAL", lookupBankCityStateForm);
			} catch(NotEnoughRowsException | BeanException e) {
				violations.add(format("Invalid bank postal code ''{0}'' or country code ''{1}''.", bankLocation.getPostalCd(), bankCountry.code));
			}
			
			// validate bank billing location
			Location bankBillingLocation = bankAccount.getBillingLocation();

			Country bankBillingLocationCountry = GenerateUtils.getCountry(bankBillingLocation.getCountryCd());
			if(!GenerateUtils.checkPostal(bankBillingLocationCountry.code, bankBillingLocation.getPostalCd(), false, null)) {
				violations.add(format("Bank account billing postal code ''{0}'' is invalid format for country ''{1}''.", bankBillingLocation.getPostalCd(), bankBillingLocationCountry.code));
			}

			InputForm lookupBankBillingCityStateForm = new MapInputForm(new HashMap<String, Object>());
			lookupBankBillingCityStateForm.setAttribute("postalCd", bankBillingLocation.getPostalCd());
			lookupBankBillingCityStateForm.setAttribute("countryCd", bankBillingLocationCountry.code);

			try {
				DataLayerMgr.selectInto(connection, "LOOKUP_POSTAL", lookupBankBillingCityStateForm);
			} catch(NotEnoughRowsException | BeanException e) {
				violations.add(format("Invalid bank billing address postal code ''{0}'' or country code ''{1}''.", bankBillingLocation.getPostalCd(), bankBillingLocationCountry.code));
			}

			// this will validate password and set the password hash and salt in the params
			try {
				new CheckPasswordStep().checkPassword(createCustomerForm);
			} catch(ServletException e) {
				if(e.getCause() != null) {
					violations.add(e.getCause().getMessage());
				} else {
					violations.add("invalid password");
				}
			}

			// all the user input validation is complete at this point so bail if there was a violation
			if(!violations.isEmpty()) {
				return createResponse(UsaLiveResponse.ERROR, violations, CustomerIds.class);
			}

			// now we should have all the required data to call the create stored procs
			
			FeeType cardPresent = FeeType.getById(EntryType.PROCESS_FEE, ProcessFeeType.CREDIT_CARD_PRESENT_ID);
			FeeType cardNotPresent = FeeType.getById(EntryType.PROCESS_FEE, ProcessFeeType.CREDIT_MANUAL_ENTRY_ID);
			FeeType monthlyServiceFee = FeeType.getById(EntryType.SERVICE_FEE, ServiceFeeType.STANDARD_SERVICE_FEE_ID);
			
			LicenseFees licenseFees = LicenseFees.loadByCustomerId(creatingUserCustomerId);
			licenseFees.getFee(EntryType.PROCESS_FEE, ExchangeType.SELL_RATE, cardPresent, RateType.PERCENT).setValue(fees.getCardPresentFee().getPercent());
			licenseFees.getFee(EntryType.PROCESS_FEE, ExchangeType.SELL_RATE, cardPresent, RateType.FLAT).setValue(fees.getCardPresentFee().getAmount());
			licenseFees.getFee(EntryType.PROCESS_FEE, ExchangeType.SELL_RATE, cardPresent, RateType.MIN).setValue(fees.getCardPresentFee().getMinimumAmount());
			licenseFees.getFee(EntryType.PROCESS_FEE, ExchangeType.SELL_RATE, cardNotPresent, RateType.PERCENT).setValue(fees.getCardNotPresentFee().getPercent());
			licenseFees.getFee(EntryType.PROCESS_FEE, ExchangeType.SELL_RATE, cardNotPresent, RateType.FLAT).setValue(fees.getCardNotPresentFee().getAmount());
			licenseFees.getFee(EntryType.PROCESS_FEE, ExchangeType.SELL_RATE, cardNotPresent, RateType.MIN).setValue(fees.getCardNotPresentFee().getMinimumAmount());
			licenseFees.getFee(EntryType.SERVICE_FEE, ExchangeType.SELL_RATE, monthlyServiceFee, RateType.FLAT).setValue(fees.getServiceFee().getAmount());
			
			long licenseId = -1;
			
			try {
				licenseId = licenseFees.createChildLicense(connection, creatingUser.getCustomerName(), customer.getName());
			} catch (DataLayerException | SQLException e) {
				log.warn("createChildLicense failed", e);
				return INTERNAL_ERROR_RESPONSE;
			}

			createCustomerForm.setAttribute("licenseId", licenseId);

			// now we can create the customer and user

			Object[] createCustomerResults = null;
			try {
				createCustomerResults = DataLayerMgr.executeCall(connection, "CREATE_CUSTOMER", createCustomerForm);
			} catch(SQLException e) {
				switch(e.getErrorCode()) {
					case 20100:
						log.warn("createCustomer failed with error code 20100, program is invalid or not unique");
						return INTERNAL_ERROR_RESPONSE;
					case 100:
					case 1:
						try {
							DataLayerMgr.selectInto(connection, "CHECK_CUSTOMER_NAME", createCustomerForm);
							if(createCustomerForm.getBoolean("customerNameExists", true, true)) {
								return createContactUsResponse(format("customer name ''{0}'' already exists", customer.getName()), CustomerIds.class);
							} else {
								return createContactUsResponse(format("username ''{0}'' already exists", primaryUser.getUserName()), CustomerIds.class);
							}
						} catch(Exception e2) {
							log.warn("createCustomer failed with error code {0} and CHECK_CUSTOMER_NAME threw exception", e.getErrorCode(), e2);
							return INTERNAL_ERROR_RESPONSE;
						}
					default:
						log.warn("CREATE_CUSTOMER threw exception", e);
						return INTERNAL_ERROR_RESPONSE;
				}
			} catch(DataLayerException e) {
				log.warn("CREATE_CUSTOMER threw exception", e);
				return INTERNAL_ERROR_RESPONSE;
			}

			// out params from stored proc are new userId, customerId
			if(createCustomerResults == null || createCustomerResults.length != 3) {
				log.warn("CREATE_CUSTOMER returned unexpected results");
				return INTERNAL_ERROR_RESPONSE;
			}
			
			CustomerIds response = new CustomerIds();
			response.setFeesId(licenseId);

			try {
				response.setUserId(ConvertUtils.getLong(createCustomerResults[1]));
			} catch(ConvertException e) {
				log.warn("userId {0} is not an integer", createCustomerResults[1]);
				return INTERNAL_ERROR_RESPONSE;
			}

			try {
				response.setCustomerId(ConvertUtils.getInt(createCustomerResults[2]));
			} catch(ConvertException e) {
				log.warn("customerId {0} is not an integer", createCustomerResults[2]);
				return INTERNAL_ERROR_RESPONSE;
			}

			// now that we have a customer and user we can add the bank account
			UsaliveUser newUser = new UsaliveUser();
			newUser.setCustomerId(response.getCustomerId());
			newUser.setUserId(response.getUserId());

			BankContact bankContact = bankAccount.getBankContact();

			int payCycleId = 8; // default daily
			switch(customer.getPaymentCycle()) {
				case "W":
					payCycleId = 3; // Weekly - Friday
					break;
				case "M":
					payCycleId = 4; // Monthly - First Friday
					break;
			}

			InputForm createBankAccountForm = new MapInputForm(new HashMap<String, Object>());
			createBankAccountForm.setAttribute(SimpleServlet.ATTRIBUTE_USER, newUser); // this has to be the new user we just created not the calling user
			createBankAccountForm.setAttribute("bank_name", bankAccount.getBankName());
			createBankAccountForm.setAttribute("bank_address1", bankLocation.getAddress());
			createBankAccountForm.setAttribute("bank_city", lookupBankCityStateForm.getAttribute("city"));
			createBankAccountForm.setAttribute("bank_state", lookupBankCityStateForm.getAttribute("state"));
			createBankAccountForm.setAttribute("bank_postal", bankLocation.getPostalCd());
			createBankAccountForm.setAttribute("bank_country", bankCountry.code);
			createBankAccountForm.setAttribute("account_title", bankAccount.getAccountTitle());
			createBankAccountForm.setAttribute("account_type", bankAccount.getAccountType());
			createBankAccountForm.setAttribute("aba", bankAccount.getAbaNumber());
			createBankAccountForm.setAttribute("account_number", bankAccount.getAccountNumber());
			createBankAccountForm.setAttribute("contact_name", bankContact.getName());
			createBankAccountForm.setAttribute("contact_title", bankContact.getTitle());
			createBankAccountForm.setAttribute("contact_telephone", bankContact.getPhone());
			createBankAccountForm.setAttribute("contact_fax", bankContact.getFax());
			createBankAccountForm.setAttribute("swift_code", bankAccount.getSwiftCode() != null ? bankAccount.getSwiftCode().toUpperCase() : null);
			createBankAccountForm.setAttribute("pay_cycle_id", payCycleId);
			createBankAccountForm.setAttribute("taxIdNbr", bankAccount.getTaxId());
			createBankAccountForm.setAttribute("address1", bankBillingLocation.getAddress());
			createBankAccountForm.setAttribute("city", lookupBankBillingCityStateForm.getAttribute("city"));
			createBankAccountForm.setAttribute("state", lookupBankBillingCityStateForm.getAttribute("state"));
			createBankAccountForm.setAttribute("postal", bankBillingLocation.getPostalCd());
			createBankAccountForm.setAttribute("country", bankBillingLocationCountry.code);

			Object[] createBankAccountResults = null;
			try {
				createBankAccountResults = DataLayerMgr.executeCall(connection, "CREATE_BANK_ACCT", createBankAccountForm);
			} catch(SQLException | DataLayerException e) {
				log.warn("CREATE_BANK_ACCT threw exception", e);
				return INTERNAL_ERROR_RESPONSE;
			}

			if(createBankAccountResults == null || createBankAccountResults.length != 2) {
				log.warn("CREATE_BANK_ACCT returned unexpected results");
				return INTERNAL_ERROR_RESPONSE;
			}

			try {
				response.setBankAccountId(ConvertUtils.getLong(createBankAccountResults[1]));
			} catch(ConvertException e) {
				log.warn("bankAccountId {0} is not an integer", createBankAccountResults[1]);
				return INTERNAL_ERROR_RESPONSE;
			}
			
			if (customer.getCustomerServiceEmail() != null || customer.getCustomerServiceEmail() != null) {
				Map<String, Object> customerServiceDbaParams = new HashMap<String, Object>();
				customerServiceDbaParams.put(SimpleServlet.ATTRIBUTE_USER, newUser);
				customerServiceDbaParams.put("doingBusinessAs", customer.getDba());
				customerServiceDbaParams.put("customerServicePhone", customer.getCustomerServicePhone());
				customerServiceDbaParams.put("customerServiceEmail", customer.getCustomerServiceEmail());
				
				try {
					DataLayerMgr.executeUpdate(connection, "UPDATE_CUSTOMER_SERVICE_DBA", customerServiceDbaParams);
				} catch(SQLException | DataLayerException e) {
					log.warn("UPDATE_CUSTOMER_SERVICE_DBA threw exception", e);
					return INTERNAL_ERROR_RESPONSE;
				}
			}

			success = true;
			log.info("SUCCESS: {0}", response);

			return response;
		} catch(Throwable e) {
			log.error("caught unexpected exception", e);
			return INTERNAL_ERROR_RESPONSE;
		} finally {
			if(connection != null) {
				try {
					DbUtils.commitOrRollback(connection, success, true);
				} catch(SQLException e) {
					log.warn("final {0} failed", (success ? "commit" : "rollback"), e);
				}
			}
		}
	}
}
