package com.usatech.usalive.ws.handler;

import java.util.HashSet;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.groups.Default;

public class ValidationUtil
{
	private static Validator validator;
	
	public static synchronized Validator getValidator()
	{
		if (validator == null) {
			validator = Validation.buildDefaultValidatorFactory().getValidator();
		}
		
		return validator;
	}
	
	public static Set<String> validate(Object...objects)
	{
		Validator validator = ValidationUtil.getValidator();
		
		Set<String> messages = new HashSet<String>();
		
		for(Object object : objects) {
//			Class<?> clazz = object.getClass();
			Set<ConstraintViolation<Object>> violations = validator.validate(object, Default.class);
			for(ConstraintViolation<Object> violation : violations) {
				messages.add(violation.getMessage());
			}
		}
		
		return messages;
	}
}
