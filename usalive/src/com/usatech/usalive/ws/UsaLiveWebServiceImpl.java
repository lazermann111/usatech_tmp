package com.usatech.usalive.ws;

import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.servlet.ServletException;

import simple.io.Log;
import simple.servlet.InputForm;
import simple.servlet.MapInputForm;
import simple.servlet.NotAuthorizedException;
import simple.servlet.steps.LoginFailureException;

import com.usatech.usalive.servlet.LoginStep;
import com.usatech.usalive.servlet.UsaliveUser;
import com.usatech.usalive.ws.handler.*;

@WebService(name="usalive", 
			serviceName="usalive",
			portName="usalive",
			targetNamespace="http://ws.usalive.usatech.com",
			endpointInterface="com.usatech.usalive.ws.UsaLiveWebServiceAPI")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT,
			 use = SOAPBinding.Use.ENCODED,
			 parameterStyle = SOAPBinding.ParameterStyle.WRAPPED)
public class UsaLiveWebServiceImpl implements UsaLiveWebServiceAPI
{
	private static final Log log = Log.getLog();
	
	protected final LoginStep loginStep;
	
	public UsaLiveWebServiceImpl() throws NoSuchAlgorithmException
	{
		loginStep = new LoginStep();
	}
	
	@Override
	@WebMethod
	@WebResult(targetNamespace="http://ws.usalive.usatech.com")
	public String ping()
	{
		return "pong";
	}

	@Override
	@WebMethod
	@WebResult(targetNamespace="http://ws.usalive.usatech.com")
	public boolean checkCredentials(Credentials credentials)
	{
		try {
			login(credentials);
			return true;
		} catch (Throwable e) {
			return false;
		}
	}
	
	@Override
	@WebMethod
	@WebResult(targetNamespace="http://ws.usalive.usatech.com")
	public CustomerIds createCustomer(Credentials credentials, CreateCustomerRequest request)
	{
		CustomerIds response = new CustomerIds();
		UsaliveUser user = checkCredentials(credentials, response);
		if (user == null)
			return response;
		
		return CreateCustomerHandler.createCustomer(request, user);
	}
	
	@Override
	@WebMethod
	@WebResult(targetNamespace="http://ws.usalive.usatech.com")
	public DeviceIds activateDevice(Credentials credentials, ActivateDeviceRequest request)
	{
		DeviceIds response = new DeviceIds();
		UsaliveUser user = checkCredentials(credentials, response);
		if (user == null)
			return response;
		
		return ActivateDeviceHandler.activateDevice(request, user);
	}
	
	@WebMethod(exclude=true)
	protected UsaliveUser checkCredentials(Credentials credentials, UsaLiveResponse response)
	{
		try {
			return login(credentials);
		} catch (NotAuthorizedException e) {
			response.setResponseCode(UsaLiveResponse.ERROR);
			response.setResponseMessages(new String[] {e.getMessage()});
		} catch (LoginFailureException e) {
			response.setResponseCode(UsaLiveResponse.ERROR);
			response.setResponseMessages(new String[] {e.getMessage()});
		} catch (Throwable e) {
			log.warn(String.format("checkCredentials failed, %s: userName=%s", e.getMessage(), credentials.getUserName()), e);
			response.setResponseCode(UsaLiveResponse.ERROR);
			response.setResponseMessages(new String[]{AbstractUsaLiveWebServiceHandler.INTERNAL_ERROR});
		}
		
		return null;
	}
	
	@WebMethod(exclude=true)
	protected UsaliveUser login(Credentials credentials) throws LoginFailureException, ServletException
	{
		if (credentials == null)
			throw new LoginFailureException("Missing credentials", LoginFailureException.INVALID_CREDENTIALS);
		
		Set<String> messages = ValidationUtil.validate(credentials);
		if (!messages.isEmpty()) {
			throw new LoginFailureException(messages.iterator().next(), LoginFailureException.INVALID_CREDENTIALS);
		}
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("username", credentials.getUserName());
		map.put("password", credentials.getPassword());
		InputForm form = new MapInputForm(map);
		
		UsaliveUser user = (UsaliveUser) loginStep.authenticateUser(form);
		
		long[] groupIds = user.getUserGroupIds();
		long[] permittedIds = {1L, 3L, 7L, 29L};  // admin, customer service, developer, partner
		
		for(long groupId : groupIds) {
			for(long permittedId : permittedIds) {
				if (groupId == permittedId) {
					return user;
				}
			}
		}
		
		throw new NotAuthorizedException("Your account is not authorized to access USALive WebServices. Please contact Customer Service for assistance.");
	}

}
