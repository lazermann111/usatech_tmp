/*
 * Created on Jul 19, 2005
 *
 */
package com.usatech.usalive.messaging;

import java.sql.Connection;
import java.sql.SQLException;

import simple.db.Call;
import simple.db.ParameterException;
import simple.event.TaskListener;
import simple.lang.Decision;

public class MessagingCall extends Call {

    public MessagingCall() {
        super();
    }

    /** Notifies Messaging System of call
     * @see simple.db.Call#executeCall(java.sql.Connection, java.lang.Object[])
     */
    @Override
    protected Object[] executeCall(Connection connection, Object[] params, Decision<Long> shouldCache, TaskListener taskListener) throws SQLException, ParameterException {
        Object[] result = super.executeCall(connection, params, shouldCache, taskListener);
        if(result != null && result.length > 0 && result[0] instanceof Number && ((Number)result[0]).intValue() > 0) {
            sendSQLUpdateMessage(getSql(), params, result);
        }
        return result;
    }

    protected void sendSQLUpdateMessage(String sql, Object[] params, Object[] result) {

    }

}
