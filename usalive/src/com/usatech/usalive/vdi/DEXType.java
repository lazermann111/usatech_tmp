
/**
 * DEXType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.usatech.usalive.vdi;
            

            /**
            *  DEXType bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class DEXType
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = DEXType
                Namespace URI = urn:vdi.usalive.usatech.com
                Namespace Prefix = ns1
                */
            

                        /**
                        * field for RawDEX
                        */

                        
                                    protected java.lang.String localRawDEX ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRawDEXTracker = false ;

                           public boolean isRawDEXSpecified(){
                               return localRawDEXTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getRawDEX(){
                               return localRawDEX;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RawDEX
                               */
                               public void setRawDEX(java.lang.String param){
                            localRawDEXTracker = param != null;
                                   
                                            this.localRawDEX=param;
                                    

                               }
                            

                        /**
                        * field for ParsedDEX
                        */

                        
                                    protected java.lang.String localParsedDEX ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localParsedDEXTracker = false ;

                           public boolean isParsedDEXSpecified(){
                               return localParsedDEXTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getParsedDEX(){
                               return localParsedDEX;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ParsedDEX
                               */
                               public void setParsedDEX(java.lang.String param){
                            localParsedDEXTracker = param != null;
                                   
                                            this.localParsedDEX=param;
                                    

                               }
                            

                        /**
                        * field for UserData
                        */

                        
                                    protected java.lang.String localUserData ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUserDataTracker = false ;

                           public boolean isUserDataSpecified(){
                               return localUserDataTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getUserData(){
                               return localUserData;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UserData
                               */
                               public void setUserData(java.lang.String param){
                            localUserDataTracker = param != null;
                                   
                                            this.localUserData=param;
                                    

                               }
                            

                        /**
                        * field for ReadDateTime
                        * This was an Attribute!
                        */

                        
                                    protected java.util.Calendar localReadDateTime ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getReadDateTime(){
                               return localReadDateTime;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ReadDateTime
                               */
                               public void setReadDateTime(java.util.Calendar param){
                            
                                            this.localReadDateTime=param;
                                    

                               }
                            

                        /**
                        * field for GMTOffSet
                        * This was an Attribute!
                        */

                        
                                    protected byte localGMTOffSet ;
                                

                           /**
                           * Auto generated getter method
                           * @return byte
                           */
                           public  byte getGMTOffSet(){
                               return localGMTOffSet;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GMTOffSet
                               */
                               public void setGMTOffSet(byte param){
                            
                                            this.localGMTOffSet=param;
                                    

                               }
                            

                        /**
                        * field for FileSize
                        * This was an Attribute!
                        */

                        
                                    protected int localFileSize ;
                                

                           /**
                           * Auto generated getter method
                           * @return int
                           */
                           public  int getFileSize(){
                               return localFileSize;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param FileSize
                               */
                               public void setFileSize(int param){
                            
                                            this.localFileSize=param;
                                    

                               }
                            

                        /**
                        * field for DexReason
                        * This was an Attribute!
                        */

                        
                                    protected int localDexReason ;
                                

                           /**
                           * Auto generated getter method
                           * @return int
                           */
                           public  int getDexReason(){
                               return localDexReason;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DexReason
                               */
                               public void setDexReason(int param){
                            
                                            this.localDexReason=param;
                                    

                               }
                            

                        /**
                        * field for DexType
                        * This was an Attribute!
                        */

                        
                                    protected int localDexType ;
                                

                           /**
                           * Auto generated getter method
                           * @return int
                           */
                           public  int getDexType(){
                               return localDexType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DexType
                               */
                               public void setDexType(int param){
                            
                                            this.localDexType=param;
                                    

                               }
                            

                        /**
                        * field for ResponseCode
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localResponseCode ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getResponseCode(){
                               return localResponseCode;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ResponseCode
                               */
                               public void setResponseCode(java.lang.String param){
                            
                                            this.localResponseCode=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:vdi.usalive.usatech.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":DEXType",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "DEXType",
                           xmlWriter);
                   }

               
                   }
               
                                            if (localReadDateTime != null){
                                        
                                                writeAttribute("urn:vdi.usalive.usatech.com",
                                                         "ReadDateTime",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localReadDateTime), xmlWriter);

                                            
                                      }
                                    
                                      else {
                                          throw new org.apache.axis2.databinding.ADBException("required attribute localReadDateTime is null");
                                      }
                                    
                                                   if (localGMTOffSet!=java.lang.Byte.MIN_VALUE) {
                                               
                                                writeAttribute("urn:vdi.usalive.usatech.com",
                                                         "GMTOffSet",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGMTOffSet), xmlWriter);

                                            
                                      }
                                    
                                      else {
                                          throw new org.apache.axis2.databinding.ADBException("required attribute localGMTOffSet is null");
                                      }
                                    
                                                   if (localFileSize!=java.lang.Integer.MIN_VALUE) {
                                               
                                                writeAttribute("urn:vdi.usalive.usatech.com",
                                                         "FileSize",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localFileSize), xmlWriter);

                                            
                                      }
                                    
                                      else {
                                          throw new org.apache.axis2.databinding.ADBException("required attribute localFileSize is null");
                                      }
                                    
                                                   if (localDexReason!=java.lang.Integer.MIN_VALUE) {
                                               
                                                writeAttribute("urn:vdi.usalive.usatech.com",
                                                         "DexReason",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDexReason), xmlWriter);

                                            
                                      }
                                    
                                      else {
                                          throw new org.apache.axis2.databinding.ADBException("required attribute localDexReason is null");
                                      }
                                    
                                                   if (localDexType!=java.lang.Integer.MIN_VALUE) {
                                               
                                                writeAttribute("urn:vdi.usalive.usatech.com",
                                                         "DexType",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDexType), xmlWriter);

                                            
                                      }
                                    
                                      else {
                                          throw new org.apache.axis2.databinding.ADBException("required attribute localDexType is null");
                                      }
                                    
                                            if (localResponseCode != null){
                                        
                                                writeAttribute("urn:vdi.usalive.usatech.com",
                                                         "ResponseCode",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localResponseCode), xmlWriter);

                                            
                                      }
                                    
                                      else {
                                          throw new org.apache.axis2.databinding.ADBException("required attribute localResponseCode is null");
                                      }
                                     if (localRawDEXTracker){
                                    namespace = "urn:vdi.usalive.usatech.com";
                                    writeStartElement(null, namespace, "RawDEX", xmlWriter);
                             

                                          if (localRawDEX==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("RawDEX cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localRawDEX);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localParsedDEXTracker){
                                    namespace = "urn:vdi.usalive.usatech.com";
                                    writeStartElement(null, namespace, "ParsedDEX", xmlWriter);
                             

                                          if (localParsedDEX==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("ParsedDEX cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localParsedDEX);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localUserDataTracker){
                                    namespace = "urn:vdi.usalive.usatech.com";
                                    writeStartElement(null, namespace, "UserData", xmlWriter);
                             

                                          if (localUserData==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("UserData cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localUserData);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:vdi.usalive.usatech.com")){
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                 if (localRawDEXTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com",
                                                                      "RawDEX"));
                                 
                                        if (localRawDEX != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRawDEX));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("RawDEX cannot be null!!");
                                        }
                                    } if (localParsedDEXTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com",
                                                                      "ParsedDEX"));
                                 
                                        if (localParsedDEX != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localParsedDEX));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("ParsedDEX cannot be null!!");
                                        }
                                    } if (localUserDataTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com",
                                                                      "UserData"));
                                 
                                        if (localUserData != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUserData));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("UserData cannot be null!!");
                                        }
                                    }
                            attribList.add(
                            new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com","ReadDateTime"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localReadDateTime));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com","GMTOffSet"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGMTOffSet));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com","FileSize"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localFileSize));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com","DexReason"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDexReason));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com","DexType"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDexType));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com","ResponseCode"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localResponseCode));
                                

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static DEXType parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            DEXType object =
                new DEXType();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"DEXType".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (DEXType)com.usatech.usalive.vdi.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    // handle attribute "ReadDateTime"
                    java.lang.String tempAttribReadDateTime =
                        
                                reader.getAttributeValue("urn:vdi.usalive.usatech.com","ReadDateTime");
                            
                   if (tempAttribReadDateTime!=null){
                         java.lang.String content = tempAttribReadDateTime;
                        
                                                 object.setReadDateTime(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(tempAttribReadDateTime));
                                            
                    } else {
                       
                               throw new org.apache.axis2.databinding.ADBException("Required attribute ReadDateTime is missing");
                           
                    }
                    handledAttributes.add("ReadDateTime");
                    
                    // handle attribute "GMTOffSet"
                    java.lang.String tempAttribGMTOffSet =
                        
                                reader.getAttributeValue("urn:vdi.usalive.usatech.com","GMTOffSet");
                            
                   if (tempAttribGMTOffSet!=null){
                         java.lang.String content = tempAttribGMTOffSet;
                        
                                                 object.setGMTOffSet(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToByte(tempAttribGMTOffSet));
                                            
                    } else {
                       
                               throw new org.apache.axis2.databinding.ADBException("Required attribute GMTOffSet is missing");
                           
                    }
                    handledAttributes.add("GMTOffSet");
                    
                    // handle attribute "FileSize"
                    java.lang.String tempAttribFileSize =
                        
                                reader.getAttributeValue("urn:vdi.usalive.usatech.com","FileSize");
                            
                   if (tempAttribFileSize!=null){
                         java.lang.String content = tempAttribFileSize;
                        
                                                 object.setFileSize(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(tempAttribFileSize));
                                            
                    } else {
                       
                               throw new org.apache.axis2.databinding.ADBException("Required attribute FileSize is missing");
                           
                    }
                    handledAttributes.add("FileSize");
                    
                    // handle attribute "DexReason"
                    java.lang.String tempAttribDexReason =
                        
                                reader.getAttributeValue("urn:vdi.usalive.usatech.com","DexReason");
                            
                   if (tempAttribDexReason!=null){
                         java.lang.String content = tempAttribDexReason;
                        
                                                 object.setDexReason(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(tempAttribDexReason));
                                            
                    } else {
                       
                               throw new org.apache.axis2.databinding.ADBException("Required attribute DexReason is missing");
                           
                    }
                    handledAttributes.add("DexReason");
                    
                    // handle attribute "DexType"
                    java.lang.String tempAttribDexType =
                        
                                reader.getAttributeValue("urn:vdi.usalive.usatech.com","DexType");
                            
                   if (tempAttribDexType!=null){
                         java.lang.String content = tempAttribDexType;
                        
                                                 object.setDexType(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(tempAttribDexType));
                                            
                    } else {
                       
                               throw new org.apache.axis2.databinding.ADBException("Required attribute DexType is missing");
                           
                    }
                    handledAttributes.add("DexType");
                    
                    // handle attribute "ResponseCode"
                    java.lang.String tempAttribResponseCode =
                        
                                reader.getAttributeValue("urn:vdi.usalive.usatech.com","ResponseCode");
                            
                   if (tempAttribResponseCode!=null){
                         java.lang.String content = tempAttribResponseCode;
                        
                                                 object.setResponseCode(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribResponseCode));
                                            
                    } else {
                       
                               throw new org.apache.axis2.databinding.ADBException("Required attribute ResponseCode is missing");
                           
                    }
                    handledAttributes.add("ResponseCode");
                    
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com","RawDEX").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"RawDEX" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRawDEX(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com","ParsedDEX").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"ParsedDEX" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setParsedDEX(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com","UserData").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"UserData" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setUserData(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    