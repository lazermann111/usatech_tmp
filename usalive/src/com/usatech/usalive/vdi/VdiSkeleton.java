
/**
 * VdiSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
    package com.usatech.usalive.vdi;
    /**
     *  VdiSkeleton java skeleton for the axisService
     */
    public class VdiSkeleton{
        
         
        /**
         * Auto generated method signature
         * 
                                     * @param getDex 
             * @return getDexResponse 
         */
        
                 public com.usatech.usalive.vdi.GetDexResponse getDex
                  (
                  com.usatech.usalive.vdi.GetDex getDex
                  )
            {
                	 return VDIRequestHandler.getDex(getDex);
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param uploadDex 
             * @return uploadDexResponse 
         */
        
                 public com.usatech.usalive.vdi.UploadDexResponse uploadDex
                  (
                  com.usatech.usalive.vdi.UploadDex uploadDex
                  )
            {
                	 return VDIRequestHandler.uploadDex(uploadDex);
        }
     
    }
    