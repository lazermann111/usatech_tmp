
/**
 * VDITransactionType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.usatech.usalive.vdi;
            

            /**
            *  VDITransactionType bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class VDITransactionType
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = VDITransactionType
                Namespace URI = urn:vdi.usalive.usatech.com
                Namespace Prefix = ns1
                */
            

                        /**
                        * field for DEXList
                        */

                        
                                    protected com.usatech.usalive.vdi.DEXListType localDEXList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDEXListTracker = false ;

                           public boolean isDEXListSpecified(){
                               return localDEXListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.usatech.usalive.vdi.DEXListType
                           */
                           public  com.usatech.usalive.vdi.DEXListType getDEXList(){
                               return localDEXList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DEXList
                               */
                               public void setDEXList(com.usatech.usalive.vdi.DEXListType param){
                            localDEXListTracker = param != null;
                                   
                                            this.localDEXList=param;
                                    

                               }
                            

                        /**
                        * field for OtherCollectionsOrLists
                        */

                        
                                    protected java.lang.String localOtherCollectionsOrLists ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOtherCollectionsOrListsTracker = false ;

                           public boolean isOtherCollectionsOrListsSpecified(){
                               return localOtherCollectionsOrListsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getOtherCollectionsOrLists(){
                               return localOtherCollectionsOrLists;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OtherCollectionsOrLists
                               */
                               public void setOtherCollectionsOrLists(java.lang.String param){
                            localOtherCollectionsOrListsTracker = param != null;
                                   
                                            this.localOtherCollectionsOrLists=param;
                                    

                               }
                            

                        /**
                        * field for UserData
                        */

                        
                                    protected java.lang.String localUserData ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUserDataTracker = false ;

                           public boolean isUserDataSpecified(){
                               return localUserDataTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getUserData(){
                               return localUserData;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UserData
                               */
                               public void setUserData(java.lang.String param){
                            localUserDataTracker = param != null;
                                   
                                            this.localUserData=param;
                                    

                               }
                            

                        /**
                        * field for VDIXMLVersion
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localVDIXMLVersion ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getVDIXMLVersion(){
                               return localVDIXMLVersion;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VDIXMLVersion
                               */
                               public void setVDIXMLVersion(java.lang.String param){
                            
                                            this.localVDIXMLVersion=param;
                                    

                               }
                            

                        /**
                        * field for TransactionReason
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localTransactionReason ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getTransactionReason(){
                               return localTransactionReason;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TransactionReason
                               */
                               public void setTransactionReason(java.lang.String param){
                            
                                            this.localTransactionReason=param;
                                    

                               }
                            

                        /**
                        * field for TransactionID
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localTransactionID ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getTransactionID(){
                               return localTransactionID;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TransactionID
                               */
                               public void setTransactionID(java.lang.String param){
                            
                                            this.localTransactionID=param;
                                    

                               }
                            

                        /**
                        * field for TransactionTime
                        * This was an Attribute!
                        */

                        
                                    protected java.util.Calendar localTransactionTime ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getTransactionTime(){
                               return localTransactionTime;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TransactionTime
                               */
                               public void setTransactionTime(java.util.Calendar param){
                            
                                            this.localTransactionTime=param;
                                    

                               }
                            

                        /**
                        * field for ProviderID
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localProviderID ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getProviderID(){
                               return localProviderID;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ProviderID
                               */
                               public void setProviderID(java.lang.String param){
                            
                                            this.localProviderID=param;
                                    

                               }
                            

                        /**
                        * field for CustomerID
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localCustomerID ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCustomerID(){
                               return localCustomerID;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomerID
                               */
                               public void setCustomerID(java.lang.String param){
                            
                                            this.localCustomerID=param;
                                    

                               }
                            

                        /**
                        * field for ApplicationID
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localApplicationID ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getApplicationID(){
                               return localApplicationID;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ApplicationID
                               */
                               public void setApplicationID(java.lang.String param){
                            
                                            this.localApplicationID=param;
                                    

                               }
                            

                        /**
                        * field for ApplicationVersion
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localApplicationVersion ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getApplicationVersion(){
                               return localApplicationVersion;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ApplicationVersion
                               */
                               public void setApplicationVersion(java.lang.String param){
                            
                                            this.localApplicationVersion=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:vdi.usalive.usatech.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":VDITransactionType",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "VDITransactionType",
                           xmlWriter);
                   }

               
                   }
               
                                            if (localVDIXMLVersion != null){
                                        
                                                writeAttribute("urn:vdi.usalive.usatech.com",
                                                         "VDIXMLVersion",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVDIXMLVersion), xmlWriter);

                                            
                                      }
                                    
                                            if (localTransactionReason != null){
                                        
                                                writeAttribute("urn:vdi.usalive.usatech.com",
                                                         "TransactionReason",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTransactionReason), xmlWriter);

                                            
                                      }
                                    
                                            if (localTransactionID != null){
                                        
                                                writeAttribute("urn:vdi.usalive.usatech.com",
                                                         "TransactionID",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTransactionID), xmlWriter);

                                            
                                      }
                                    
                                            if (localTransactionTime != null){
                                        
                                                writeAttribute("urn:vdi.usalive.usatech.com",
                                                         "TransactionTime",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTransactionTime), xmlWriter);

                                            
                                      }
                                    
                                            if (localProviderID != null){
                                        
                                                writeAttribute("urn:vdi.usalive.usatech.com",
                                                         "ProviderID",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localProviderID), xmlWriter);

                                            
                                      }
                                    
                                            if (localCustomerID != null){
                                        
                                                writeAttribute("urn:vdi.usalive.usatech.com",
                                                         "CustomerID",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCustomerID), xmlWriter);

                                            
                                      }
                                    
                                            if (localApplicationID != null){
                                        
                                                writeAttribute("urn:vdi.usalive.usatech.com",
                                                         "ApplicationID",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localApplicationID), xmlWriter);

                                            
                                      }
                                    
                                            if (localApplicationVersion != null){
                                        
                                                writeAttribute("urn:vdi.usalive.usatech.com",
                                                         "ApplicationVersion",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localApplicationVersion), xmlWriter);

                                            
                                      }
                                     if (localDEXListTracker){
                                            if (localDEXList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("DEXList cannot be null!!");
                                            }
                                           localDEXList.serialize(new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com","DEXList"),
                                               xmlWriter);
                                        } if (localOtherCollectionsOrListsTracker){
                                    namespace = "urn:vdi.usalive.usatech.com";
                                    writeStartElement(null, namespace, "OtherCollectionsOrLists", xmlWriter);
                             

                                          if (localOtherCollectionsOrLists==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("OtherCollectionsOrLists cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localOtherCollectionsOrLists);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localUserDataTracker){
                                    namespace = "urn:vdi.usalive.usatech.com";
                                    writeStartElement(null, namespace, "UserData", xmlWriter);
                             

                                          if (localUserData==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("UserData cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localUserData);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:vdi.usalive.usatech.com")){
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                 if (localDEXListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com",
                                                                      "DEXList"));
                            
                            
                                    if (localDEXList==null){
                                         throw new org.apache.axis2.databinding.ADBException("DEXList cannot be null!!");
                                    }
                                    elementList.add(localDEXList);
                                } if (localOtherCollectionsOrListsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com",
                                                                      "OtherCollectionsOrLists"));
                                 
                                        if (localOtherCollectionsOrLists != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOtherCollectionsOrLists));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("OtherCollectionsOrLists cannot be null!!");
                                        }
                                    } if (localUserDataTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com",
                                                                      "UserData"));
                                 
                                        if (localUserData != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUserData));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("UserData cannot be null!!");
                                        }
                                    }
                            attribList.add(
                            new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com","VDIXMLVersion"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localVDIXMLVersion));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com","TransactionReason"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTransactionReason));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com","TransactionID"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTransactionID));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com","TransactionTime"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTransactionTime));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com","ProviderID"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localProviderID));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com","CustomerID"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCustomerID));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com","ApplicationID"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localApplicationID));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com","ApplicationVersion"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localApplicationVersion));
                                

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static VDITransactionType parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            VDITransactionType object =
                new VDITransactionType();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"VDITransactionType".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (VDITransactionType)com.usatech.usalive.vdi.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    // handle attribute "VDIXMLVersion"
                    java.lang.String tempAttribVDIXMLVersion =
                        
                                reader.getAttributeValue("urn:vdi.usalive.usatech.com","VDIXMLVersion");
                            
                   if (tempAttribVDIXMLVersion!=null){
                         java.lang.String content = tempAttribVDIXMLVersion;
                        
                                                 object.setVDIXMLVersion(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribVDIXMLVersion));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("VDIXMLVersion");
                    
                    // handle attribute "TransactionReason"
                    java.lang.String tempAttribTransactionReason =
                        
                                reader.getAttributeValue("urn:vdi.usalive.usatech.com","TransactionReason");
                            
                   if (tempAttribTransactionReason!=null){
                         java.lang.String content = tempAttribTransactionReason;
                        
                                                 object.setTransactionReason(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribTransactionReason));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("TransactionReason");
                    
                    // handle attribute "TransactionID"
                    java.lang.String tempAttribTransactionID =
                        
                                reader.getAttributeValue("urn:vdi.usalive.usatech.com","TransactionID");
                            
                   if (tempAttribTransactionID!=null){
                         java.lang.String content = tempAttribTransactionID;
                        
                                                 object.setTransactionID(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribTransactionID));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("TransactionID");
                    
                    // handle attribute "TransactionTime"
                    java.lang.String tempAttribTransactionTime =
                        
                                reader.getAttributeValue("urn:vdi.usalive.usatech.com","TransactionTime");
                            
                   if (tempAttribTransactionTime!=null){
                         java.lang.String content = tempAttribTransactionTime;
                        
                                                 object.setTransactionTime(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(tempAttribTransactionTime));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("TransactionTime");
                    
                    // handle attribute "ProviderID"
                    java.lang.String tempAttribProviderID =
                        
                                reader.getAttributeValue("urn:vdi.usalive.usatech.com","ProviderID");
                            
                   if (tempAttribProviderID!=null){
                         java.lang.String content = tempAttribProviderID;
                        
                                                 object.setProviderID(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribProviderID));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("ProviderID");
                    
                    // handle attribute "CustomerID"
                    java.lang.String tempAttribCustomerID =
                        
                                reader.getAttributeValue("urn:vdi.usalive.usatech.com","CustomerID");
                            
                   if (tempAttribCustomerID!=null){
                         java.lang.String content = tempAttribCustomerID;
                        
                                                 object.setCustomerID(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribCustomerID));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("CustomerID");
                    
                    // handle attribute "ApplicationID"
                    java.lang.String tempAttribApplicationID =
                        
                                reader.getAttributeValue("urn:vdi.usalive.usatech.com","ApplicationID");
                            
                   if (tempAttribApplicationID!=null){
                         java.lang.String content = tempAttribApplicationID;
                        
                                                 object.setApplicationID(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribApplicationID));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("ApplicationID");
                    
                    // handle attribute "ApplicationVersion"
                    java.lang.String tempAttribApplicationVersion =
                        
                                reader.getAttributeValue("urn:vdi.usalive.usatech.com","ApplicationVersion");
                            
                   if (tempAttribApplicationVersion!=null){
                         java.lang.String content = tempAttribApplicationVersion;
                        
                                                 object.setApplicationVersion(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribApplicationVersion));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("ApplicationVersion");
                    
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com","DEXList").equals(reader.getName())){
                                
                                                object.setDEXList(com.usatech.usalive.vdi.DEXListType.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com","OtherCollectionsOrLists").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"OtherCollectionsOrLists" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setOtherCollectionsOrLists(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com","UserData").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"UserData" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setUserData(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    