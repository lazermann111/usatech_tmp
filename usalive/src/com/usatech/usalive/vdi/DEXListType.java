
/**
 * DEXListType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.usatech.usalive.vdi;
            

            /**
            *  DEXListType bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class DEXListType
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = DEXListType
                Namespace URI = urn:vdi.usalive.usatech.com
                Namespace Prefix = ns1
                */
            

                        /**
                        * field for DexTransmission
                        * This was an Array!
                        */

                        
                                    protected com.usatech.usalive.vdi.DexTransmissionType[] localDexTransmission ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDexTransmissionTracker = false ;

                           public boolean isDexTransmissionSpecified(){
                               return localDexTransmissionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.usatech.usalive.vdi.DexTransmissionType[]
                           */
                           public  com.usatech.usalive.vdi.DexTransmissionType[] getDexTransmission(){
                               return localDexTransmission;
                           }

                           
                        


                               
                              /**
                               * validate the array for DexTransmission
                               */
                              protected void validateDexTransmission(com.usatech.usalive.vdi.DexTransmissionType[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param DexTransmission
                              */
                              public void setDexTransmission(com.usatech.usalive.vdi.DexTransmissionType[] param){
                              
                                   validateDexTransmission(param);

                               localDexTransmissionTracker = param != null;
                                      
                                      this.localDexTransmission=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.usatech.usalive.vdi.DexTransmissionType
                             */
                             public void addDexTransmission(com.usatech.usalive.vdi.DexTransmissionType param){
                                   if (localDexTransmission == null){
                                   localDexTransmission = new com.usatech.usalive.vdi.DexTransmissionType[]{};
                                   }

                            
                                 //update the setting tracker
                                localDexTransmissionTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localDexTransmission);
                               list.add(param);
                               this.localDexTransmission =
                             (com.usatech.usalive.vdi.DexTransmissionType[])list.toArray(
                            new com.usatech.usalive.vdi.DexTransmissionType[list.size()]);

                             }
                             

                        /**
                        * field for UserData
                        */

                        
                                    protected java.lang.String localUserData ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUserDataTracker = false ;

                           public boolean isUserDataSpecified(){
                               return localUserDataTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getUserData(){
                               return localUserData;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UserData
                               */
                               public void setUserData(java.lang.String param){
                            localUserDataTracker = param != null;
                                   
                                            this.localUserData=param;
                                    

                               }
                            

                        /**
                        * field for RecordsCount
                        * This was an Attribute!
                        */

                        
                                    protected int localRecordsCount ;
                                

                           /**
                           * Auto generated getter method
                           * @return int
                           */
                           public  int getRecordsCount(){
                               return localRecordsCount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RecordsCount
                               */
                               public void setRecordsCount(int param){
                            
                                            this.localRecordsCount=param;
                                    

                               }
                            

                        /**
                        * field for DEXEncoding
                        * This was an Attribute!
                        */

                        
                                    protected int localDEXEncoding ;
                                

                           /**
                           * Auto generated getter method
                           * @return int
                           */
                           public  int getDEXEncoding(){
                               return localDEXEncoding;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DEXEncoding
                               */
                               public void setDEXEncoding(int param){
                            
                                            this.localDEXEncoding=param;
                                    

                               }
                            

                        /**
                        * field for DEXCompressionType
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localDEXCompressionType ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getDEXCompressionType(){
                               return localDEXCompressionType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DEXCompressionType
                               */
                               public void setDEXCompressionType(java.lang.String param){
                            
                                            this.localDEXCompressionType=param;
                                    

                               }
                            

                        /**
                        * field for DEXCompressionParam
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localDEXCompressionParam ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getDEXCompressionParam(){
                               return localDEXCompressionParam;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DEXCompressionParam
                               */
                               public void setDEXCompressionParam(java.lang.String param){
                            
                                            this.localDEXCompressionParam=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:vdi.usalive.usatech.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":DEXListType",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "DEXListType",
                           xmlWriter);
                   }

               
                   }
               
                                                   if (localRecordsCount!=java.lang.Integer.MIN_VALUE) {
                                               
                                                writeAttribute("urn:vdi.usalive.usatech.com",
                                                         "RecordsCount",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRecordsCount), xmlWriter);

                                            
                                      }
                                    
                                      else {
                                          throw new org.apache.axis2.databinding.ADBException("required attribute localRecordsCount is null");
                                      }
                                    
                                                   if (localDEXEncoding!=java.lang.Integer.MIN_VALUE) {
                                               
                                                writeAttribute("urn:vdi.usalive.usatech.com",
                                                         "DEXEncoding",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDEXEncoding), xmlWriter);

                                            
                                      }
                                    
                                            if (localDEXCompressionType != null){
                                        
                                                writeAttribute("urn:vdi.usalive.usatech.com",
                                                         "DEXCompressionType",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDEXCompressionType), xmlWriter);

                                            
                                      }
                                    
                                            if (localDEXCompressionParam != null){
                                        
                                                writeAttribute("urn:vdi.usalive.usatech.com",
                                                         "DEXCompressionParam",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDEXCompressionParam), xmlWriter);

                                            
                                      }
                                     if (localDexTransmissionTracker){
                                       if (localDexTransmission!=null){
                                            for (int i = 0;i < localDexTransmission.length;i++){
                                                if (localDexTransmission[i] != null){
                                                 localDexTransmission[i].serialize(new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com","DexTransmission"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("DexTransmission cannot be null!!");
                                        
                                    }
                                 } if (localUserDataTracker){
                                    namespace = "urn:vdi.usalive.usatech.com";
                                    writeStartElement(null, namespace, "UserData", xmlWriter);
                             

                                          if (localUserData==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("UserData cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localUserData);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:vdi.usalive.usatech.com")){
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                 if (localDexTransmissionTracker){
                             if (localDexTransmission!=null) {
                                 for (int i = 0;i < localDexTransmission.length;i++){

                                    if (localDexTransmission[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com",
                                                                          "DexTransmission"));
                                         elementList.add(localDexTransmission[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("DexTransmission cannot be null!!");
                                    
                             }

                        } if (localUserDataTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com",
                                                                      "UserData"));
                                 
                                        if (localUserData != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUserData));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("UserData cannot be null!!");
                                        }
                                    }
                            attribList.add(
                            new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com","RecordsCount"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRecordsCount));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com","DEXEncoding"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDEXEncoding));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com","DEXCompressionType"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDEXCompressionType));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com","DEXCompressionParam"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDEXCompressionParam));
                                

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static DEXListType parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            DEXListType object =
                new DEXListType();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"DEXListType".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (DEXListType)com.usatech.usalive.vdi.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    // handle attribute "RecordsCount"
                    java.lang.String tempAttribRecordsCount =
                        
                                reader.getAttributeValue("urn:vdi.usalive.usatech.com","RecordsCount");
                            
                   if (tempAttribRecordsCount!=null){
                         java.lang.String content = tempAttribRecordsCount;
                        
                                                 object.setRecordsCount(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(tempAttribRecordsCount));
                                            
                    } else {
                       
                               throw new org.apache.axis2.databinding.ADBException("Required attribute RecordsCount is missing");
                           
                    }
                    handledAttributes.add("RecordsCount");
                    
                    // handle attribute "DEXEncoding"
                    java.lang.String tempAttribDEXEncoding =
                        
                                reader.getAttributeValue("urn:vdi.usalive.usatech.com","DEXEncoding");
                            
                   if (tempAttribDEXEncoding!=null){
                         java.lang.String content = tempAttribDEXEncoding;
                        
                                                 object.setDEXEncoding(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(tempAttribDEXEncoding));
                                            
                    } else {
                       
                                           object.setDEXEncoding(java.lang.Integer.MIN_VALUE);
                                       
                    }
                    handledAttributes.add("DEXEncoding");
                    
                    // handle attribute "DEXCompressionType"
                    java.lang.String tempAttribDEXCompressionType =
                        
                                reader.getAttributeValue("urn:vdi.usalive.usatech.com","DEXCompressionType");
                            
                   if (tempAttribDEXCompressionType!=null){
                         java.lang.String content = tempAttribDEXCompressionType;
                        
                                                 object.setDEXCompressionType(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribDEXCompressionType));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("DEXCompressionType");
                    
                    // handle attribute "DEXCompressionParam"
                    java.lang.String tempAttribDEXCompressionParam =
                        
                                reader.getAttributeValue("urn:vdi.usalive.usatech.com","DEXCompressionParam");
                            
                   if (tempAttribDEXCompressionParam!=null){
                         java.lang.String content = tempAttribDEXCompressionParam;
                        
                                                 object.setDEXCompressionParam(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribDEXCompressionParam));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("DEXCompressionParam");
                    
                    
                    reader.next();
                
                        java.util.ArrayList list1 = new java.util.ArrayList();
                    
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com","DexTransmission").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list1.add(com.usatech.usalive.vdi.DexTransmissionType.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone1 = false;
                                                        while(!loopDone1){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone1 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com","DexTransmission").equals(reader.getName())){
                                                                    list1.add(com.usatech.usalive.vdi.DexTransmissionType.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone1 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setDexTransmission((com.usatech.usalive.vdi.DexTransmissionType[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.usatech.usalive.vdi.DexTransmissionType.class,
                                                                list1));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com","UserData").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"UserData" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setUserData(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    