
/**
 * ExtensionMapper.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

        
            package com.usatech.usalive.vdi;
        
            /**
            *  ExtensionMapper class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class ExtensionMapper{

          public static java.lang.Object getTypeObject(java.lang.String namespaceURI,
                                                       java.lang.String typeName,
                                                       javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{

              
                  if (
                  "urn:vdi.usalive.usatech.com".equals(namespaceURI) &&
                  "DexTransmissionType".equals(typeName)){
                   
                            return  com.usatech.usalive.vdi.DexTransmissionType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:vdi.usalive.usatech.com".equals(namespaceURI) &&
                  "DEXListType".equals(typeName)){
                   
                            return  com.usatech.usalive.vdi.DEXListType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:vdi.usalive.usatech.com".equals(namespaceURI) &&
                  "VDITransactionType".equals(typeName)){
                   
                            return  com.usatech.usalive.vdi.VDITransactionType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:vdi.usalive.usatech.com".equals(namespaceURI) &&
                  "DexCollectionType".equals(typeName)){
                   
                            return  com.usatech.usalive.vdi.DexCollectionType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:vdi.usalive.usatech.com".equals(namespaceURI) &&
                  "VDIReturnType".equals(typeName)){
                   
                            return  com.usatech.usalive.vdi.VDIReturnType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:vdi.usalive.usatech.com".equals(namespaceURI) &&
                  "ArrayOfString".equals(typeName)){
                   
                            return  com.usatech.usalive.vdi.ArrayOfString.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:vdi.usalive.usatech.com".equals(namespaceURI) &&
                  "DEXType".equals(typeName)){
                   
                            return  com.usatech.usalive.vdi.DEXType.Factory.parse(reader);
                        

                  }

              
             throw new org.apache.axis2.databinding.ADBException("Unsupported type " + namespaceURI + " " + typeName);
          }

        }
    