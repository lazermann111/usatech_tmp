
/**
 * DexTransmissionType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.usatech.usalive.vdi;
            

            /**
            *  DexTransmissionType bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class DexTransmissionType
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = DexTransmissionType
                Namespace URI = urn:vdi.usalive.usatech.com
                Namespace Prefix = ns1
                */
            

                        /**
                        * field for DexCollection
                        */

                        
                                    protected com.usatech.usalive.vdi.DexCollectionType localDexCollection ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDexCollectionTracker = false ;

                           public boolean isDexCollectionSpecified(){
                               return localDexCollectionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.usatech.usalive.vdi.DexCollectionType
                           */
                           public  com.usatech.usalive.vdi.DexCollectionType getDexCollection(){
                               return localDexCollection;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DexCollection
                               */
                               public void setDexCollection(com.usatech.usalive.vdi.DexCollectionType param){
                            localDexCollectionTracker = true;
                                   
                                            this.localDexCollection=param;
                                    

                               }
                            

                        /**
                        * field for UserData
                        */

                        
                                    protected java.lang.String localUserData ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getUserData(){
                               return localUserData;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UserData
                               */
                               public void setUserData(java.lang.String param){
                            
                                            this.localUserData=param;
                                    

                               }
                            

                        /**
                        * field for DeviceID
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localDeviceID ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getDeviceID(){
                               return localDeviceID;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DeviceID
                               */
                               public void setDeviceID(java.lang.String param){
                            
                                            this.localDeviceID=param;
                                    

                               }
                            

                        /**
                        * field for TransmitTime
                        * This was an Attribute!
                        */

                        
                                    protected java.util.Calendar localTransmitTime ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getTransmitTime(){
                               return localTransmitTime;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TransmitTime
                               */
                               public void setTransmitTime(java.util.Calendar param){
                            
                                            this.localTransmitTime=param;
                                    

                               }
                            

                        /**
                        * field for GMTOffSet
                        * This was an Attribute!
                        */

                        
                                    protected byte localGMTOffSet ;
                                

                           /**
                           * Auto generated getter method
                           * @return byte
                           */
                           public  byte getGMTOffSet(){
                               return localGMTOffSet;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GMTOffSet
                               */
                               public void setGMTOffSet(byte param){
                            
                                            this.localGMTOffSet=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:vdi.usalive.usatech.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":DexTransmissionType",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "DexTransmissionType",
                           xmlWriter);
                   }

               
                   }
               
                                            if (localDeviceID != null){
                                        
                                                writeAttribute("urn:vdi.usalive.usatech.com",
                                                         "DeviceID",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDeviceID), xmlWriter);

                                            
                                      }
                                    
                                      else {
                                          throw new org.apache.axis2.databinding.ADBException("required attribute localDeviceID is null");
                                      }
                                    
                                            if (localTransmitTime != null){
                                        
                                                writeAttribute("urn:vdi.usalive.usatech.com",
                                                         "TransmitTime",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTransmitTime), xmlWriter);

                                            
                                      }
                                    
                                      else {
                                          throw new org.apache.axis2.databinding.ADBException("required attribute localTransmitTime is null");
                                      }
                                    
                                                   if (localGMTOffSet!=java.lang.Byte.MIN_VALUE) {
                                               
                                                writeAttribute("urn:vdi.usalive.usatech.com",
                                                         "GMTOffSet",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGMTOffSet), xmlWriter);

                                            
                                      }
                                    
                                      else {
                                          throw new org.apache.axis2.databinding.ADBException("required attribute localGMTOffSet is null");
                                      }
                                     if (localDexCollectionTracker){
                                    if (localDexCollection==null){

                                        writeStartElement(null, "urn:vdi.usalive.usatech.com", "DexCollection", xmlWriter);

                                       // write the nil attribute
                                      writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                      xmlWriter.writeEndElement();
                                    }else{
                                     localDexCollection.serialize(new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com","DexCollection"),
                                        xmlWriter);
                                    }
                                }
                                    namespace = "urn:vdi.usalive.usatech.com";
                                    writeStartElement(null, namespace, "UserData", xmlWriter);
                             

                                          if (localUserData==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localUserData);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:vdi.usalive.usatech.com")){
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                 if (localDexCollectionTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com",
                                                                      "DexCollection"));
                            
                            
                                    elementList.add(localDexCollection==null?null:
                                    localDexCollection);
                                }
                                      elementList.add(new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com",
                                                                      "UserData"));
                                 
                                         elementList.add(localUserData==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUserData));
                                    
                            attribList.add(
                            new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com","DeviceID"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDeviceID));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com","TransmitTime"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTransmitTime));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com","GMTOffSet"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localGMTOffSet));
                                

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static DexTransmissionType parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            DexTransmissionType object =
                new DexTransmissionType();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"DexTransmissionType".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (DexTransmissionType)com.usatech.usalive.vdi.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    // handle attribute "DeviceID"
                    java.lang.String tempAttribDeviceID =
                        
                                reader.getAttributeValue("urn:vdi.usalive.usatech.com","DeviceID");
                            
                   if (tempAttribDeviceID!=null){
                         java.lang.String content = tempAttribDeviceID;
                        
                                                 object.setDeviceID(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribDeviceID));
                                            
                    } else {
                       
                               throw new org.apache.axis2.databinding.ADBException("Required attribute DeviceID is missing");
                           
                    }
                    handledAttributes.add("DeviceID");
                    
                    // handle attribute "TransmitTime"
                    java.lang.String tempAttribTransmitTime =
                        
                                reader.getAttributeValue("urn:vdi.usalive.usatech.com","TransmitTime");
                            
                   if (tempAttribTransmitTime!=null){
                         java.lang.String content = tempAttribTransmitTime;
                        
                                                 object.setTransmitTime(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(tempAttribTransmitTime));
                                            
                    } else {
                       
                               throw new org.apache.axis2.databinding.ADBException("Required attribute TransmitTime is missing");
                           
                    }
                    handledAttributes.add("TransmitTime");
                    
                    // handle attribute "GMTOffSet"
                    java.lang.String tempAttribGMTOffSet =
                        
                                reader.getAttributeValue("urn:vdi.usalive.usatech.com","GMTOffSet");
                            
                   if (tempAttribGMTOffSet!=null){
                         java.lang.String content = tempAttribGMTOffSet;
                        
                                                 object.setGMTOffSet(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToByte(tempAttribGMTOffSet));
                                            
                    } else {
                       
                               throw new org.apache.axis2.databinding.ADBException("Required attribute GMTOffSet is missing");
                           
                    }
                    handledAttributes.add("GMTOffSet");
                    
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com","DexCollection").equals(reader.getName())){
                                
                                      nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                      if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                          object.setDexCollection(null);
                                          reader.next();
                                            
                                            reader.next();
                                          
                                      }else{
                                    
                                                object.setDexCollection(com.usatech.usalive.vdi.DexCollectionType.Factory.parse(reader));
                                              
                                        reader.next();
                                    }
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:vdi.usalive.usatech.com","UserData").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setUserData(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                                }
                              
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    