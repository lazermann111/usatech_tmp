package com.usatech.usalive.vdi;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.lang.management.ManagementFactory;
import java.nio.charset.Charset;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicLong;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.axis2.context.MessageContext;
import org.apache.axis2.transport.http.HTTPConstants;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.sun.org.apache.xml.internal.utils.XMLChar;
import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.constants.DEXReason;
import com.usatech.layers.common.constants.EventCodePrefix;
import com.usatech.layers.common.constants.FileType;
import com.usatech.layers.common.constants.LoadDataAttrEnum;
import com.usatech.layers.common.constants.MessageAttrEnum;

import simple.app.DialectResolver;
import simple.app.Publisher;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.ByteInput;
import simple.io.IOUtils;
import simple.io.Log;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;
import simple.results.BeanException;
import simple.results.Results;
import simple.text.MinimalCDataCharset;
import simple.text.StringUtils;
import simple.text.ThreadSafeDateFormat;
import simple.xml.MapXMLLoader;
import simple.xml.MapXMLLoader.ParentMap;

public class VDIRequestHandler {
	private static final Log log = Log.getLog();
	protected static final MinimalCDataCharset cdataCharset = new MinimalCDataCharset();
	protected static final DateFormat TIMESTAMP_FORMAT = new ThreadSafeDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"));
	protected static final DateFormat DEX_FILE_TS_FORMAT = new ThreadSafeDateFormat(new SimpleDateFormat("HHmmss-MMddyy"));
	protected static final String GLOBAL_CODE_PREFIX = EventCodePrefix.VDI.getValue() + ":";
	protected static final String GLOBAL_SESSION_CODE_PREFIX = GLOBAL_CODE_PREFIX + ManagementFactory.getRuntimeMXBean().getName() + ':' + Long.toHexString(System.currentTimeMillis()).toUpperCase() + ':';
	protected static final AtomicLong sessionIDGenerator = new AtomicLong(Double.doubleToLongBits(Math.random()));
	protected static ResourceFolder resourceFolder;
	protected static Publisher<ByteInput> publisher;
	
	public static GetDexResponse getDex(GetDex getDex) {
		MessageContext msgContext = MessageContext.getCurrentMessageContext();		
		HttpServletRequest httpRequest = (HttpServletRequest) msgContext.getProperty(HTTPConstants.MC_HTTP_SERVLETREQUEST);
        String username = httpRequest.getUserPrincipal().getName();
        ArrayOfString deviceList = getDex.getDeviceList();
        ArrayOfString outletList = getDex.getOutletList();
        Calendar onOrAfter = getDex.getOnOrAfter();
        Calendar onOrBefore = getDex.getOnOrBefore();
		log.info(new StringBuilder("Received GetDex request")
			.append(", UserName: '").append(username).append("'")
			.append(", TransactionID: '").append(getDex.getTransactionID()).append("'")			
			.append(", ProviderID: '").append(getDex.getProviderID()).append("'")
			.append(", CustomerID: '").append(getDex.getCustomerID()).append("'")
			.append(", ApplicationID: '").append(getDex.getApplicationID()).append("'")
			.append(", ApplicationVersion: '").append(getDex.getApplicationVersion()).append("'")
			.append(", DeviceList: '").append(deviceList == null ? "null" : StringUtils.join(deviceList.getString(), ", ")).append("'")
			.append(", OutletList: '").append(outletList == null ? "null" : StringUtils.join(outletList.getString(), ", ")).append("'")
			.append(", OnOrAfter: '").append(onOrAfter == null ? "null" : onOrAfter.getTime()).append("'")
			.append(", OnOrBefore: '").append(onOrBefore == null ? "null" : onOrBefore.getTime()).append("'")
			.append(", ReturnSet: '").append(getDex.getReturnSet()).append("'")
			.append(", UserData: '").append(getDex.getUserData()).append("'")
			.append(", VDIXMLVersion: '").append(getDex.getVDIXMLVersion()).append("'")
			.toString());
		
		GetDexResponse getDexResponse = new GetDexResponse();
		VDIReturnType vdiReturn = new VDIReturnType();
		VDITransactionType vdiTran = new VDITransactionType();
		Connection conn = null;
		try {
			DEXListType dexList = new DEXListType();
			ArrayList<DexTransmissionType> dexTransmissionArrayList = new ArrayList<DexTransmissionType>();
			int dexTranIndex = 0;
							        
	        HashMap<String, Object> params = new HashMap<String, Object>();
	        params.put("userName", username);
	        params.put("customerName", getDex.getCustomerID());
	        if (deviceList != null)
	        	params.put("eportSerialNums", deviceList.getString());
	        if (outletList != null)
	        	params.put("assetNums", outletList.getString());
	        params.put("onOrAfter", onOrAfter);
	        params.put("onOrBefore", onOrBefore);
	        
	        String returnSet = getDex.getReturnSet();
	        boolean returnFirst = "FIRST".equalsIgnoreCase(returnSet);
	        boolean returnLast = "LAST".equalsIgnoreCase(returnSet);
	        String previousEportSerialNum = "";
	        conn = DataLayerMgr.getConnection("REPORT");	        
	        Results results = DataLayerMgr.executeQuery(conn, "GET_DEX_FILES", params);
	        TimeZone dbTimeZone = null;
	        final Charset cdataCharset = new MinimalCDataCharset();
			while(results.next()) {
				DexTransmissionType dexTransmission = new DexTransmissionType();
				String eportSerialNum = results.getValue("eportSerialNum", String.class);
				if (returnFirst || returnLast) {
					if (eportSerialNum.equalsIgnoreCase(previousEportSerialNum)) {
						if (returnFirst)
							continue;
						else {
							dexTranIndex--;
							dexTransmissionArrayList.remove(dexTranIndex);
						}
					} else
						previousEportSerialNum = eportSerialNum;
				}
				dexTransmission.setDeviceID(eportSerialNum);
				
				TimeZone deviceTimeZone = TimeZone.getTimeZone(results.getValue("deviceTimeZoneGuid", String.class));
				if (dbTimeZone == null)
					dbTimeZone = TimeZone.getTimeZone(results.getValue("dbTimeZoneGuid", String.class));
				
				long deviceFileTransferTsMs = results.getValue("deviceFileTransferTsMs", long.class);				
				long deviceFileTransferUtcTsMs = ConvertUtils.getMillisUTC(deviceFileTransferTsMs, dbTimeZone);				
				long transmitTimeMs = ConvertUtils.getLocalTime(deviceFileTransferUtcTsMs, deviceTimeZone);
				Calendar transmitTime = Calendar.getInstance();
				transmitTime.clear();
				transmitTime.setTimeZone(TimeZone.getTimeZone("GMT"));				
				transmitTime.setTimeInMillis(transmitTimeMs);
				dexTransmission.setTransmitTime(transmitTime);
				
				byte transmitTimeUtcOffsetHours = (byte) (deviceTimeZone.getOffset(deviceFileTransferUtcTsMs) / 60 / 60 / 1000);				
				dexTransmission.setGMTOffSet(transmitTimeUtcOffsetHours);
				
				int dexReason = results.getValue("dexType", int.class);
				switch (dexReason) {
					case 1: //SCHEDULED
					case 2: //INTERVAL
						dexReason = 0; //Scheduled DEX Read
						break;
					case 3: //FILL
						dexReason = 1; //Service Button Pressed
						break;
					case 4: //ALARM
						dexReason = 5; //Alert/VMC initiated
						break;
					default:
						dexReason = 99; //Other
				}
				
				DEXType dex = new DEXType();
				dex.setResponseCode("OK");
				dex.setDexReason(dexReason);				
				dex.setDexType(0); //Full Dex

				String dexContent = null;
				if (DialectResolver.isOracle()) {
					Clob fileContent = results.getValue("fileContent", Clob.class);
					dexContent = IOUtils.readFully(new InputStreamReader(fileContent.getAsciiStream(), cdataCharset));
				} else {
					byte[] fileContent = results.getValue("fileContent", byte[].class);
					ByteArrayInputStream in = new ByteArrayInputStream(fileContent);
					dexContent = IOUtils.readFully(new InputStreamReader(in, cdataCharset));
				}
				
				dex.setFileSize(dexContent.length());
				dex.setRawDEX(dexContent);
				
				long dexDateMs = results.getValue("dexDateMs", long.class);
				Calendar readDateTime = Calendar.getInstance();
				readDateTime.clear();
				readDateTime.setTimeZone(TimeZone.getTimeZone("GMT"));
				readDateTime.setTimeInMillis(dexDateMs);
				dex.setReadDateTime(readDateTime);
				
				long dexDateUtcMs = ConvertUtils.getMillisUTC(deviceFileTransferTsMs, deviceTimeZone);
				byte dexDateUtcOffsetHours = (byte) (deviceTimeZone.getOffset(dexDateUtcMs) / 60 / 60 / 1000);
				dex.setGMTOffSet(dexDateUtcOffsetHours);
								
				DexCollectionType dexCollection = new DexCollectionType();
				dexCollection.setRecordsCount(1);
				dexCollection.setDEX(new DEXType[] {dex});
				dexTransmission.setDexCollection(dexCollection);				
				dexTransmissionArrayList.add(dexTranIndex, dexTransmission);
				dexTranIndex++;
			}
			
			DexTransmissionType[] dexTransmissionArray = new DexTransmissionType[dexTransmissionArrayList.size()]; 
			dexTransmissionArrayList.toArray(dexTransmissionArray);
			dexList.setDexTransmission(dexTransmissionArray);
			dexList.setRecordsCount(dexTransmissionArrayList.size());
			vdiTran.setDEXList(dexList);
			
			vdiReturn.setCode(0);
			vdiReturn.setMessage("Success");
			getDexResponse.setVDIReturn(vdiReturn);			
		} catch (Exception e) {
			vdiReturn.setCode(1);
			vdiReturn.setMessage("Error getting DEX data");
			log.error("Error getting DEX data: ", e);
		} finally {
        	ProcessingUtils.closeDbConnection(log, conn);
        }
		getDexResponse.setVDIReturn(vdiReturn);
		getDexResponse.setVDITransaction(vdiTran);
		log.info(new StringBuilder("Sending GetDex response")
			.append(", Code: '").append(vdiReturn.getCode()).append("'")
			.append(", Message: '").append(vdiReturn.getMessage()).append("'")
			.append(", RecordsCount: '").append(vdiTran.getDEXList() == null ? "null" : vdiTran.getDEXList().getRecordsCount()).append("'")
			.toString());
		return getDexResponse;
	}

	public static UploadDexResponse uploadDex(UploadDex uploadDex) {
		MessageContext msgContext = MessageContext.getCurrentMessageContext();		
		HttpServletRequest httpRequest = (HttpServletRequest) msgContext.getProperty(HTTPConstants.MC_HTTP_SERVLETREQUEST);
        String username = httpRequest.getUserPrincipal().getName();
        log.info(new StringBuilder("Received UploadDex request")
			.append(", UserName: '").append(username).append("'")
			.append(", TransactionID: '").append(uploadDex.getTransactionId()).append("'")			
			.append(", ProviderID: '").append(uploadDex.getProviderId()).append("'")
			.append(", CustomerID: '").append(uploadDex.getCustomerId()).append("'")
			.append(", ApplicationID: '").append(uploadDex.getApplicationId()).append("'")
			.append(", ApplicationVersion: '").append(uploadDex.getApplicationVersion()).append("'")
			.append(", DexCompressionParam: '").append(uploadDex.getDexCompressionParam()).append("'")
			.append(", DexCompressionType: '").append(uploadDex.getDexCompressionType()).append("'")
			.append(", DexEncoding: '").append(uploadDex.getDexEncoding()).append("'")
			.append(", UserData: '").append(uploadDex.getUserData()).append("'")
			.append(", VDIXMLVersion: '").append(uploadDex.getVdiXmlVersion()).append("'")
			.toString());
        String globalSessionCode = new StringBuilder().append(GLOBAL_SESSION_CODE_PREFIX)
        		.append(Long.toHexString(sessionIDGenerator.getAndIncrement()).toUpperCase()).append(':')
        		.append(httpRequest.getRemoteAddr()).append(':').append(httpRequest.getRemotePort()).toString();
        
		// parse xml into dex file content and info and save
		UploadDexResponse response = new UploadDexResponse();
		if(StringUtils.isBlank(username)) {
			log.error("No User Name was provided");
			response.setUploadDexResult(formatVdiResponse(1, "No credentials provided"));
			return response;
		} else if(username.toLowerCase().endsWith("@usatech.com")) {
			log.error("Internal User Not Allowed");
			response.setUploadDexResult(formatVdiResponse(2, "This account may not upload DEX files"));
			return response;
		}
		MapXMLLoader xmlLoader = new MapXMLLoader();
		final String xml = uploadDex.getVdiXml();
		ParentMap root;
		try {
			try (Reader reader = new Reader() {
				protected int pos = 0;

				public int read(char[] cbuf, int off, int len) throws IOException {
					if(len <= 0)
						return 0;
					if(pos >= xml.length())
						return -1;
					int r = 0;
					while(true) {
						char ch = xml.charAt(pos++);
						if(ch == '&' && pos < xml.length() && xml.charAt(pos) == '#') { // look for &#_;
							int sc = xml.indexOf(';', pos + 1);
							if(sc > 0) {
								int v = ConvertUtils.getIntSafely(xml.substring(pos + 1, sc), -1);
								if(v >= 0 && XMLChar.isInvalid(v)) {
									// skip it
									pos = sc + 1;
									if(pos >= xml.length())
										return r;
									continue;
								}
							}
						}
						cbuf[off + r] = ch;
						r++;
						if(r >= len || pos >= xml.length())
							return r;
						ch = xml.charAt(pos);
					}
				}

				@Override
				public void close() throws IOException {
				}
			}) {
				root = xmlLoader.load(new InputSource(reader));
			}
		} catch(IOException | SAXException | ParserConfigurationException e) {
			log.error("Invalid VDI XML", e);
			response.setUploadDexResult(formatVdiResponse(11, "Invalid VDI XML"));
			return response;
		}
		ParentMap[] dexLists = root.getChildren("DEXList");
		if(dexLists == null || dexLists.length == 0)
			response.setUploadDexResult(formatVdiResponse(12, "No DEX Files Listed"));
		else
			try {
				long eventId = ConvertUtils.getLong(root.get("TransactionID"));
				String customerCd = ConvertUtils.getString(root.get("CustomerID"), true);
				int count = 0;
				Map<String, Object> params = new HashMap<>();
				for(ParentMap dexList : dexLists) {
					Charset dexEncoding = getCharsetForDexEncoding(ConvertUtils.getIntSafely(dexList.get("DEXEncoding"), 1));
					ParentMap[] dexTransmissions = dexList.getChildren("DexTransmission");
					if(dexTransmissions != null)
						for(ParentMap dexTransmission : dexTransmissions) {
							String deviceSerialCd = dexTransmission.get("DeviceID");
							if(StringUtils.isBlank(deviceSerialCd)) {
								log.error("DeviceID was not provided on the DexTransmission");
								response.setUploadDexResult(formatVdiResponse(21, "DeviceID was not provided on the DexTransmission"));
								return response;
							}
							String localTime = dexTransmission.get("TransmitTime");
							String gmtOffset = dexTransmission.get("GMTOffSet");
							long transmitTs;
							if(!StringUtils.isBlank(localTime) && !StringUtils.isBlank(gmtOffset))
								try {
									transmitTs = ConvertUtils.getLocalTime(TIMESTAMP_FORMAT.parse(localTime).getTime() - (long) (ConvertUtils.getDouble(gmtOffset) * 60 * 60 * 1000L), TimeZone.getDefault());
								} catch(ParseException | ConvertException e) {
									log.error("Could not parse tranmission time and GMT Offset '" + localTime + " " + gmtOffset + "'", e);
									transmitTs = System.currentTimeMillis();
								}
							else
								transmitTs = System.currentTimeMillis();
							params.put("userName", username);
							params.put("deviceSerialCd", deviceSerialCd);
							params.put("effectiveTs", transmitTs);
							DataLayerMgr.selectInto("CHECK_DEVICE_UPLOAD_DEX_PERMISSION", params);
							if(!ConvertUtils.getBoolean(params.get("permitted"), false)) {
								log.error("User '" + username + "' does not have Edit Permission on device " + deviceSerialCd);
								response.setUploadDexResult(formatVdiResponse(3, "This account may not upload DEX files to device " + deviceSerialCd));
								return response;
							}
							String savedCustomerCd = ConvertUtils.getString(params.get("customerCd"), false);
							if(!ConvertUtils.areEqual(customerCd, savedCustomerCd)) {
								log.error("Customer Code '" + customerCd + "' for device " + deviceSerialCd + " does not match what is saved, '" + savedCustomerCd + "'; continuing anyway");
							}
							long deviceId = ConvertUtils.getLong(params.get("deviceId"));
							String deviceName = ConvertUtils.getString(params.get("deviceName"), true);
							ParentMap[] dexCollections = dexTransmission.getChildren("DexCollection");
							if(dexCollections != null)
								for(ParentMap dexCollection : dexCollections) {
									ParentMap[] dexs = dexCollection.getChildren("DEX");
									if(dexs != null)
										for(ParentMap dex : dexs) {
											ParentMap[] contents = dex.getChildren("RawDEX");
											if(contents == null || contents.length != 1) {
												String msg = "Invalid Vdi XML: " + (contents == null ? 0 : contents.length) + "<RawDEX> elements found under one DEX parent";
												log.error(msg);
												response.setUploadDexResult(formatVdiResponse(13, msg));
												return response;
											}
											String dexContent = contents[0].getText();
											if(StringUtils.isBlank(dexContent)) {
												String msg = "Invalid Vdi XML: <RawDEX> element is empty";
												log.error(msg);
												response.setUploadDexResult(formatVdiResponse(14, msg));
												return response;
											}
											count++;
											localTime = dex.get("ReadDateTime");
											gmtOffset = dex.get("GMTOffSet");
											long dexTime;
											Date dexDate;
											if(!StringUtils.isBlank(localTime) && !StringUtils.isBlank(gmtOffset))
												try {
													dexDate = TIMESTAMP_FORMAT.parse(localTime);
													dexTime = ConvertUtils.getLocalTime(dexDate.getTime() - (long) (ConvertUtils.getDouble(gmtOffset) * 60 * 60 * 1000L), TimeZone.getDefault());
												} catch(ParseException | ConvertException e) {
													log.error("Could not parse ReadDateTime and GMT Offset '" + localTime + " " + gmtOffset + "'; using current time", e);
													dexDate = new Date();
													dexTime = dexDate.getTime();
												}
											else {
												log.error("Either ReadDateTime or GMT Offset is null: '" + localTime + " " + gmtOffset + "'; using current time");
												dexDate = new Date();
												dexTime = dexDate.getTime();
											}
											Integer vdiDexReason = ConvertUtils.convertSafely(Integer.class, dex.get("DexReason"), null);
											DEXReason dexReason;
											if(vdiDexReason == null) {
												log.error("Could not parse DexReason: '" + dex.get("DexReason") + "'; using UNKNOWN");
												dexReason = DEXReason.UNKNOWN;
											} else
												dexReason = DEXReason.fromVdiDexReason(vdiDexReason);
											Long fileSize = ConvertUtils.convertSafely(Long.class, dex.get("FileSize"), null);
											if(fileSize == null)
												log.error("Could not parse FileSize: '" + dex.get("FileSize") + "'");
											else if(dexContent.length() != fileSize)
												log.error("Stated FileSize: '" + dex.get("FileSize") + "' does not match actual " + dexContent.length());
											String dexHeader = formatDexHeader(deviceSerialCd, dexDate, dexReason);
											// Determine newline
											String newline = detectNewline(dexContent);
											Resource resource = getResourceFolder().getResource(dexHeader + ".log", ResourceMode.CREATE);
											try {
												try (Writer writer = new OutputStreamWriter(resource.getOutputStream(), dexEncoding)) {
													writer.append(dexHeader);
													writer.append(newline);
													writer.append(dexContent);
													writer.flush();
												}
											} finally {
												resource.release();
											}
											MessageChain mc = new MessageChainV11();
											MessageChainStep step = mc.addStep("usat.inbound.vdi.dex");
											step.setAttribute(CommonAttrEnum.ATTR_RESOURCE, resource.getKey());
											step.setAttribute(LoadDataAttrEnum.ATTR_FILE_TRANSFER_TS, transmitTs);
											step.setAttribute(CommonAttrEnum.ATTR_FILE_NAME, resource.getName());
											step.setAttribute(CommonAttrEnum.ATTR_FILE_TYPE, dexReason == DEXReason.FILL ? FileType.DEX_FILE_FOR_FILL_TO_FILL : FileType.DEX_FILE);
											step.setAttribute(LoadDataAttrEnum.ATTR_DEVICE_SERIAL_CD, deviceSerialCd);
											step.setAttribute(MessageAttrEnum.ATTR_DEVICE_NAME, deviceName);
											step.setAttribute(LoadDataAttrEnum.ATTR_DEVICE_ID, deviceId);
											step.setAttribute(MessageAttrEnum.ATTR_GLOBAL_SESSION_CODE, globalSessionCode);
											step.setAttribute(LoadDataAttrEnum.ATTR_DEVICE_CHARSET, ProcessingConstants.ISO8859_1_CHARSET);

											if(dexReason == DEXReason.FILL) {
												StringBuilder sb = new StringBuilder();
												sb.append(GLOBAL_CODE_PREFIX).append(deviceName).append(':').append(eventId);
												if(count > 1)
													sb.append(':').append(count);
												String globalEventCd = sb.toString();
												step.setAttribute(LoadDataAttrEnum.ATTR_EVENT_ID, eventId);
												step.setAttribute(LoadDataAttrEnum.ATTR_TRAN_GLOBAL_TRANS_CD, globalEventCd);
												step.setAttribute(LoadDataAttrEnum.ATTR_EVENT_TIME, dexTime);
											}
											MessageChainService.publish(mc, getPublisher());
										}
								}
						}
				}
				log.info("Received " + count + " DEX Files for user " + username + " from " + httpRequest.getRemoteAddr() + ":" + httpRequest.getRemotePort());
				response.setUploadDexResult(formatVdiResponse(0, "Success"));
			} catch(IOException | SQLException | DataLayerException | BeanException | ConvertException | ServiceException e) {
				log.error("Could not process UploadDex", e);
				response.setUploadDexResult(formatVdiResponse(6, "Processing error. Please try again"));
				return response;
			}
		return response;
	}

	protected static String detectNewline(String dexContent) {
		for(int i = 0; i < dexContent.length(); i++) {
			char ch = dexContent.charAt(i);
			switch(ch) {
				case '\n':
				case '\r':
					if(++i >= dexContent.length())
						return String.valueOf(ch);
					char ch2 = dexContent.charAt(i);
					if(ch == ch2)
						return String.valueOf(ch);
					switch(ch2) {
						case '\n':
						case '\r':
							return dexContent.substring(i - 1, i + 1);
					}
					return String.valueOf(ch);
			}
		}
		return "\r\n";
	}

	protected static String formatDexHeader(String deviceSerialCd, Date dexDate, DEXReason dexReason) {
		StringBuilder sb = new StringBuilder(28 + deviceSerialCd.length());
		sb.append("DEX-").append(deviceSerialCd).append('-');
		sb.append(DEX_FILE_TS_FORMAT.format(dexDate));
		sb.append('-').append(dexReason);
		return sb.toString();
	}

	protected static String formatVdiResponse(int status, String message) {
		StringBuilder sb = new StringBuilder(125 + (message == null ? 0 : message.length()));
		sb.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<VDIReturn>\n\t<Code>").append(status).append("</Code>\n\t<Message>");
		try {
			StringUtils.appendCDATA(sb, message);
		} catch(IOException e) {
			log.error("Unexpected error", e);
		}
		sb.append("</Message>\n</VDIReturn>\n");
		return sb.toString();
	}

	protected static Charset getCharsetForDexEncoding(int dexEncoding) {
		switch(dexEncoding) {
			case 1:
				return cdataCharset; // Charset.forName("ISO8859-1");
			case 2:
				return Charset.forName("UTF-8");
			default:
				return Charset.defaultCharset();
		}
	}
	public static ResourceFolder getResourceFolder() {
		return resourceFolder;
	}

	public static void setResourceFolder(ResourceFolder resourceFolder) {
		VDIRequestHandler.resourceFolder = resourceFolder;
	}

	public static Publisher<ByteInput> getPublisher() {
		return publisher;
	}

	public static void setPublisher(Publisher<ByteInput> publisher) {
		VDIRequestHandler.publisher = publisher;
	}
}
