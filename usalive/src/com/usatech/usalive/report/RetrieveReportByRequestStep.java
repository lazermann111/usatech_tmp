package com.usatech.usalive.report;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.NotEnoughRowsException;
import simple.io.IOUtils;
import simple.io.WriterOutputStream;
import simple.io.resource.Resource;
import simple.io.resource.ResourceMode;
import simple.results.BeanException;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestInfoUtils;
import simple.servlet.RequestUtils;
import simple.servlet.steps.AbstractStep;

import com.usatech.usalive.hybrid.HybridServlet;
import com.usatech.usalive.servlet.UsaliveUser;

/**
 * This class is for the user to get the generated report.
 *
 * @author bkrug
 *
 */
public class RetrieveReportByRequestStep extends AbstractStep {
	protected final static Pattern multipartParserPattern = Pattern.compile("multipart/[^;]+(?:\\s*;\\s*\\w+\\s*=\\s*(?:\"[^\"]*\"|'[^']*'))*(?:\\s*;\\s*type\\s*=\\s*(\"[^\"]*\"|'[^']*'))(?:\\s*;\\s*\\w+\\s*=\\s*(?:\"[^\"]*\"|'[^']*'))*\\s*;?\\s*", Pattern.DOTALL);

	public RetrieveReportByRequestStep() {
	}
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		AsyncReportEngine arEngine = (AsyncReportEngine) form.getAttribute(HybridServlet.ATTRIBUTE_ASYNCH_REPORT_ENGINE);
		try {
			long requestId = form.getLong("requestId", true, -1L);
			int pageId = form.getInt("pageId", false, 0);
			StoredFileInfo fileInfo = arEngine.getReportResult(requestId, pageId);
			if(fileInfo == null) { // try to get from database
				long storedFileId = form.getLong("storedFileId", true, -1L);
				Map<String,Object> params = new HashMap<String,Object>();
				params.put("fileId", storedFileId);
				params.put("pageId", pageId);
				try {
					DataLayerMgr.selectInto("GET_STORED_FILE_INFO", params);
				} catch(NotEnoughRowsException e) {
					throw new ServletException("Invalid request Id: " + requestId);
				}
				String fileName = ConvertUtils.getString(params.get("fileName"), true);
				String fileKey = ConvertUtils.getString(params.get("fileKey"), true);
				String fileContentType = ConvertUtils.getString(params.get("fileContentType"), true);
				long fileExpiration = ConvertUtils.getLong(params.get("fileExpiration"));
				fileInfo = new StoredFileInfo(storedFileId, fileName, fileKey, fileContentType, fileExpiration);
				arEngine.setReportResult(requestId, pageId, fileInfo);
			}
			Resource dataSource = arEngine.getResource(fileInfo.getFileKey(), ResourceMode.READ);
			try {
				dataSource.setContentType(fileInfo.getFileContentType());
				dataSource.setName(fileInfo.getFileName());
				renderOutput(dataSource, response, request.getAttribute("javax.servlet.include.request_uri") != null);
			} finally {
				dataSource.release();
			}
			UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
			user.pulledRequest(requestId, pageId);
		} catch(ConvertException e) {
			throw new ServletException(e);
		} catch(IOException e) {
			throw new ServletException(e);
		} catch(ExecutionException e) {
			throw new ServletException(e);
		} catch(DataLayerException e) {
			throw new ServletException(e);
		} catch(BeanException e) {
			throw new ServletException(e);
		} catch(SQLException e) {
			throw new ServletException(e);
		}
	}

	public void renderOutput(Resource dataSource, HttpServletResponse response, boolean included) throws IOException {
		OutputStream ostream;
		if(included)
			ostream = new WriterOutputStream(response.getWriter());
		else
			ostream = response.getOutputStream();
		String contentType = dataSource.getContentType();
		/* It works better with browsers to down load multipart files then open
		Matcher matcher = multipartParserPattern.matcher(contentType);
		if(matcher.matches()) {
			String tmp = matcher.group(1);
			contentType = tmp.substring(1, tmp.length() - 1);
		}*/
		// NOTE: this is to accomodate IE7 and IE8 which always prompt for "application/xhtml+xml"
		response.setContentType(RequestInfoUtils.getContentTypeForLegacyBrowsers(dataSource.getContentType()));
		// if it is html, render it directly, others will prompt download
		if(!RequestInfoUtils.isDirectlyRendered(contentType))
			response.setHeader("Content-Disposition", "attachment; filename=\"" + dataSource.getName() + "\"");
		IOUtils.copy(dataSource.getInputStream(), ostream);
	}
}
