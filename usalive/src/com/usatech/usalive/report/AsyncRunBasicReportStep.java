package com.usatech.usalive.report;

import java.io.IOException;
import java.sql.SQLException;
import java.text.Format;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.ExecutionException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.usatech.report.Generator;
import com.usatech.report.ReportRequest;
import com.usatech.report.ReportRequestFactory;
import com.usatech.report.ReportScheduleType;
import com.usatech.report.ReportingUser;
import com.usatech.usalive.hybrid.HybridServlet;
import com.usatech.usalive.servlet.USALiveUtils;
import com.usatech.usalive.servlet.UsaliveUser;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.falcon.engine.Scene;
import simple.falcon.servlet.FolioStepHelper;
import simple.io.Log;
import simple.lang.InvalidIntValueException;
import simple.results.BeanException;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.NotAuthorizedException;
import simple.servlet.RequestUtils;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;

public class AsyncRunBasicReportStep extends AbstractStep {
	private static final Log log = Log.getLog();
	protected static final Format DATE_FORMAT = ConvertUtils.getFormat("DATE", "MM-dd-yyyy HH:mm:ss.SSS");
	protected final Set<String> keepParamNames = new HashSet<String>();
	protected final Set<String> restrictedParamNames = new HashSet<String>();

	public AsyncRunBasicReportStep() {
		keepParamNames.add("outputType");
		keepParamNames.add("customerIds");
		// keepParamNames.add("regionIds");
		// keepParamNames.add("terminalIds");
		// keepParamNames.add("is_custom_report");
		restrictedParamNames.add("query");
		restrictedParamNames.add("is_custom_report");
		restrictedParamNames.add("isSQLFolio");
		restrictedParamNames.add("callId");
		restrictedParamNames.add("folioId");
		restrictedParamNames.add("reportId");
	}

	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		UsaliveUser user = (UsaliveUser) RequestUtils.getUser(request);
		if(user == null || user.getUserGroupIds() == null || user.getUserGroupIds().length == 0)
			throw new ServletException("You do not have access to run reports");
		try {
			AsyncReportEngine arEngine = RequestUtils.getAttribute(request, HybridServlet.ATTRIBUTE_ASYNCH_REPORT_ENGINE, AsyncReportEngine.class, true);
			Long masterUserId = RequestUtils.getAttribute(request, "masterUserId", Long.class, false);
			Long refreshFileId = RequestUtils.getAttribute(request, "refreshFileId", Long.class, false);
			int basicReportId = RequestUtils.getAttribute(request, "basicReportId", int.class, true);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("reportId", basicReportId);
			params.put("userId", user.getUserId());
			DataLayerMgr.selectInto("GET_BASIC_REPORT", params);
			String reportName = ConvertUtils.getString(params.get("name"), true);
			boolean permitted = ConvertUtils.getBoolean(params.get("permitted"), false);
			if(!permitted)
				throw new ServletException("You do not have permission to run '" + reportName + "'");
			String title = form.getStringSafely("reportTitle", ConvertUtils.getString(params.get("title"), false));
			int generatorId = ConvertUtils.getInt(params.get("generatorId"));
			ReportScheduleType scheduleType = ConvertUtils.convertRequired(ReportScheduleType.class, params.get("scheduleTypeId"));
			if(scheduleType == ReportScheduleType.EVENT)
				scheduleType = ConvertUtils.convertSafely(ReportScheduleType.class, RequestUtils.getAttribute(request, "batchType", false), scheduleType);
			
			Scene scene = FolioStepHelper.createScene(form, request, form.getTranslator());
			scene.getProperties().put("menuLinks", USALiveUtils.getLinks(form, "M"));
			SortedMap<String, String> reportParameters = new TreeMap<String, String>();
			reportParameters.put("report.basicReportId", String.valueOf(basicReportId));
			Integer selectedMenuItem = RequestUtils.getAttribute(request, SimpleServlet.ATTRIBUTE_SELECTED_MENU_ITEM, Integer.class, false, "session");
			if (selectedMenuItem != null)
				reportParameters.put("report.selectedMenuItem", String.valueOf(selectedMenuItem));
			params.put("configurableFillins", ReportRequestFactory.Fillin.getConfigurableValues());
			try (Results fillinResults = DataLayerMgr.executeQuery("GET_CONFIGURABLE_BASIC_REPORT_FILLINS", params)) {
				while(fillinResults.next()) {
					ReportRequestFactory.Fillin fillin = fillinResults.getValue("fillin", ReportRequestFactory.Fillin.class);
					if(restrictedParamNames.contains(fillin.getParamName()))
						continue;
					String value = form.getStringSafely(fillin.getParamName(), null);
					if(value != null) {
						// check permission
						switch(fillin) {
							case BATCH:
								params.clear();
								params.put("checkUser", user);
								params.put("batchId", value);
								DataLayerMgr.selectInto("CHECK_" + scheduleType + "_BATCH_PERMISSION", params);
								if(!ConvertUtils.getBoolean(params.get("permitted"), false)) {
									// this might have to go in actions.xml
									RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "run-basic-report-batch-not-viewable", "You specified a {0} # that you do not have permission to view", scheduleType.getDescription(), value);
									RequestUtils.redirectWithCarryOver(request, response, "basic_report_prompt.html?reportId=" + basicReportId);
									return;
								}
								break;
							case REGIONS:
								long[] regionIds = ConvertUtils.convert(long[].class, value);
								for(long regionId : regionIds)
									try {
										user.checkPermission(ReportingUser.ResourceType.REGION, ReportingUser.ActionType.VIEW, regionId);
									} catch(NotAuthorizedException e) {
										// this might have to go in actions.xml
										RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "run-basic-report-region-not-viewable", "You specified a region that you do not have permission to view", value);
										RequestUtils.redirectWithCarryOver(request, response, "basic_report_prompt.html?reportId=" + basicReportId);
										return;
									}
								break;
							case CUSTOMERS:
								long[] customerIds = ConvertUtils.convert(long[].class, value);
								for(long customerId : customerIds)
									try {
										user.checkPermission(ReportingUser.ResourceType.CUSTOMER, ReportingUser.ActionType.VIEW, customerId);
									} catch(NotAuthorizedException e) {
										// this might have to go in actions.xml
										RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "run-basic-report-customer-not-viewable", "You specified a customer that you do not have permission to view", value);
										RequestUtils.redirectWithCarryOver(request, response, "basic_report_prompt.html?reportId=" + basicReportId);
										return;
									}
								break;
							case TERMINALS:
								try {
									user.checkPermission(ReportingUser.ResourceType.TERMINALS, ReportingUser.ActionType.VIEW, value);
								} catch(NotAuthorizedException e) {
									// this might have to go in actions.xml
									RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "run-basic-report-terminals-not-viewable", "You specified a terminal that you do not have permission to view", value);
									RequestUtils.redirectWithCarryOver(request, response, "basic_report_prompt.html?reportId=" + basicReportId);
									return;
								}
								break;
						}

						reportParameters.put("report." + fillin.getParamName(), value);
					}
				}
			}

			params.clear();
			params.put("reportId", basicReportId);
			try (Results paramResults = DataLayerMgr.executeQuery("GET_CONFIGURABLE_BASIC_REPORT_PARAMS", params)) {
				while(paramResults.next()) {
					String name = paramResults.getValue("paramName", String.class);
					if(restrictedParamNames.contains(name))
						continue;
					String value = form.getStringSafely(name, null);
					if(value != null)
						reportParameters.put("report." + name, value);
				}
			}
			/*
			switch(scheduleType) {
				case TIMED:
					Date beginDate = ConvertUtils.convert(Date.class, form.getAttribute("beginDate"));
					Date endDate = ConvertUtils.convert(Date.class, form.getAttribute("endDate"));
					if(beginDate != null)
						reportParameters.put("report.beginDate", DATE_FORMAT.format(beginDate));
					if(endDate != null)
						reportParameters.put("report.endDate", DATE_FORMAT.format(endDate));
					break;
				default:
					Long batchId = ConvertUtils.convert(Long.class, form.getAttribute("batchId"));
					if(batchId != null)
						reportParameters.put("report.batchId", ConvertUtils.getStringSafely(batchId));
			}*/
			for(String keepParamName : keepParamNames) {
				String value = form.getStringSafely(keepParamName, null);
				if(value != null)
					reportParameters.put("report." + keepParamName, value);
			}
			reportParameters.put("report.batchType", String.valueOf(scheduleType.getValue()));
			reportParameters.put("report.frequencyName", "Manual");
			ReportRequest<Void> reportRequest = new ReportRequest<Void>(Generator.getByValue(generatorId), null, scene, user.getUserId(), masterUserId == null ? -1 : masterUserId, reportParameters, title);
			long requestId = arEngine.requestReport(reportRequest, user.getUserId(), masterUserId, refreshFileId);
			try {
				user.addRequest(requestId, 0, user.getUserId());
			} catch(InterruptedException e) {
				// do nothing
			}
			StoredFileInfo fileInfo;
			try {
				fileInfo = arEngine.getReportResult(requestId, 0);
			} catch(ExecutionException e) {
				throw new ServletException(e);
			}
			if(fileInfo != null) {
				form.set("storedFileInfo", 1);
				try {
					user.readiedRequest(requestId, 0, user.getUserId());
				} catch(InterruptedException e) {
					// do nothing
				}
			} else
				form.set("storedFileInfo", 0);
			form.set("requestId", requestId);
			form.set("pageId", 0L);
			form.set("profileId", user.getUserId());
			form.set("pollInterval", 1000);
			form.set("pollIntervalIndex", 1);
		} catch(SQLException | DataLayerException | BeanException | ConvertException | InvalidIntValueException | ExecutionException | ParserConfigurationException | IOException | SAXException e) {
			log.warn("Exception while requesting basic report", e);
			throw new ServletException(e);
		}
	}

	public String[] getKeepParamNames() {
		return keepParamNames.toArray(new String[keepParamNames.size()]);
	}

	public void setKeepParamNames(String[] keepParamNames) {
		this.keepParamNames.clear();
		this.keepParamNames.addAll(Arrays.asList(keepParamNames));
	}
}
