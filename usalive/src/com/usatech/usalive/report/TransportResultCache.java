package com.usatech.usalive.report;

import java.util.Map;
import java.util.concurrent.ExecutionException;

import simple.app.Publisher;
import simple.app.ServiceException;
import simple.io.ByteInput;
import simple.util.concurrent.LockLinkedSegmentCache;
import simple.util.concurrent.SegmentCache;
import simple.util.concurrent.WaitForUpdateFuture;

import com.usatech.app.GenericAttribute;
import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.constants.TransportAttrEnum;
import com.usatech.transport.TransportReason;
/**
 * 
 * @author yhe
 *
 */
public class TransportResultCache {
	
	
	public TransportResultCache() {
		super();
	}

	protected class GetTestTransportResult extends WaitForUpdateFuture<TransportResult>{
		protected long transportResultId;
		protected Map<String, Object> params;

		public GetTestTransportResult(long transportResultId, Map<String, Object> params) {
			super();
			this.transportResultId = transportResultId;
			this.params=params;
		}
		
		@Override
		protected void run() throws ExecutionException {
			try {
				requestTestTransport(transportResultId, params);
			} catch(ServiceException e) {
				throw new ExecutionException(e);
			}
		}
	}
	
	public void requestTestTransport(long transportResultId, Map<String, Object> params) throws ServiceException {
		MessageChain mc = new MessageChainV11();
		MessageChainStep transportStep = mc.addStep("usat.ccs.transport."+params.get("transport.transportTypeId"));
		transportStep.setTemporary(true);
		transportStep.addStringAttribute("transport.transportReason", TransportReason.TEST.toString());  
		transportStep.addIntAttribute(TransportAttrEnum.ATTR_TRANSPORT_MAX_RETRY_ALLOWED.getValue(), 0);
		for(Map.Entry<String, Object> entry : params.entrySet())
			transportStep.setAttribute(new GenericAttribute(entry.getKey()), entry.getValue());

		MessageChainStep transportResultStep = mc.addStep("usat.ccs.transport.result", true);
		transportResultStep.setTemporary(true);
		transportResultStep.addReferenceAttribute(TransportAttrEnum.ATTR_SENT_SUCCESS.getValue(), transportStep, TransportAttrEnum.ATTR_SENT_SUCCESS.getValue());
		transportResultStep.addReferenceAttribute(TransportAttrEnum.ATTR_SENT_DETAILS.getValue(), transportStep, TransportAttrEnum.ATTR_SENT_DETAILS.getValue());
		transportResultStep.addLongAttribute(TransportAttrEnum.ATTR_TRANSPORT_RESULT_ID.getValue(), transportResultId);
		transportStep.setNextSteps(0, transportResultStep);
		MessageChainService.publish(mc, publisher);
	}
	protected final LockLinkedSegmentCache<Long, GetTestTransportResult, RuntimeException> keyCache = new LockLinkedSegmentCache<Long, GetTestTransportResult, RuntimeException>(1024, SegmentCache.DEFAULT_INITIAL_CAPACITY, SegmentCache.DEFAULT_LOAD_FACTOR, SegmentCache.DEFAULT_SEGMENTS) {
		@Override
		protected GetTestTransportResult createValue(final Long key, Object... additionalInfo) {
			return new GetTestTransportResult(key, (Map<String, Object>) additionalInfo[0]);
		}
	};

	public LockLinkedSegmentCache<Long, GetTestTransportResult, RuntimeException> getKeyCache() {
		return keyCache;
	}
	
	protected Publisher<ByteInput> publisher;
	public Publisher<ByteInput> getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher<ByteInput> publisher) {
		this.publisher = publisher;
	}
	
	
}
