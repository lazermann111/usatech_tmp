package com.usatech.usalive.report;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.ServletUser;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;
import simple.text.StringUtils;

public class SaveReportStep extends AbstractStep {
	private static final Log log = Log.getLog();
	protected static final String ENCODING = "UTF-8";
	protected static final int[] CALENDAR_FIELDS = new int[] { 
		Calendar.YEAR,
		Calendar.MONTH, 
		Calendar.DAY_OF_MONTH, 
		Calendar.HOUR_OF_DAY, 
		Calendar.MINUTE,
		0
	};
	/* This indicates which value is the first value of a particular field */
	protected static final int[] CALENDAR_FIRSTS = new int[] { 
		0,
		0, 
		1, 
		0, 
		0,
		0
	};
	protected static final long[] CALENDAR_FACTORS = new long[] {
		365 * 24 * 60 * 60 * 1000L,
		30 * 24 * 60 * 60 * 1000L,
		24 * 60 * 60 * 1000L,
		60 * 60 * 1000L,
		60 * 1000L,
		1000L
	};
	protected static final String[] CALENDAR_FORMAT_NAME = new String[] {
		"YEAR", 
		"MONTH", 
		"DAY", 
		"HOUR"
	};

	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		long requestedTime = form.getLong("requestedTime", false, System.currentTimeMillis());
		String referrer = form.getString("referrer", false);
		String runReportAction = form.getString("runReportAction", false);
		StringBuilder url = new StringBuilder("./");
		if(!StringUtils.isBlank(referrer) && referrer.endsWith("_parameters.i")) {
			referrer = urlEncode(referrer);
			url.append(referrer, 0, referrer.length() - 13).append(".i?");
		} else if(!StringUtils.isBlank(runReportAction)) {
			int pos = 0;
			char ch;
			while(pos < runReportAction.length() && ((ch = runReportAction.charAt(pos)) == '.' || ch == '/'))
				pos++;
			if(pos > 0)
				runReportAction = runReportAction.substring(pos);
			url.append(runReportAction);
			if(runReportAction.indexOf('?') < 0)
				url.append('?');
			else
				url.append('&');
		} else
			url.append("run_report_async.i?");
		String title = form.getString("reportTitle", true);
		int maxFirstSet = 0;
		class DateParameter {
			public final String name;
			public final long diff;
			public final char sign;
			public DateParameter(String name, long diff, char sign) {
				this.name = name;
				this.diff = diff;
				this.sign = sign;
			}
		}
		List<DateParameter> dateParameters = null;
		for(Map.Entry<String, Object> entry : form.getParameters().entrySet()) {
			if(entry.getKey() == null || entry.getKey().length() == 0 || entry.getKey().equals("reportTitle") || entry.getKey().equals("saveAction"))
				continue;
			String sval = ConvertUtils.convertSafely(String.class, entry.getValue(), null);
			if(StringUtils.isBlank(sval))
				continue;
			if(entry.getKey().endsWith("Date") && !(sval.trim().startsWith("{") && sval.trim().endsWith("}"))) {
				Calendar cal = ConvertUtils.convertSafely(Calendar.class, entry.getValue(), null);
				if(cal != null) {
					long diff = requestedTime - cal.getTimeInMillis();
					int firstSet = 0;
					for(int i = CALENDAR_FACTORS.length - 1; i >= 0; i--) {
						if(CALENDAR_FIELDS[i] != 0 && cal.get(CALENDAR_FIELDS[i]) > CALENDAR_FIRSTS[i]) {
							firstSet = i;
							break;
						}
					}
					if(maxFirstSet < firstSet)
						maxFirstSet = firstSet;
					char sign;
					if(diff < 0) {
						sign = '+';
						diff = -diff;
					} else
						sign = '-';
					if(dateParameters == null)
						dateParameters = new ArrayList<>(3);
					dateParameters.add(new DateParameter(entry.getKey(), diff, sign));
					continue;
				}
			}
			url.append(urlEncode(entry.getKey())).append('=').append(urlEncode(sval)).append('&');
		}
		if(dateParameters != null)
			for(DateParameter dateParameter : dateParameters) {
				for(int i = 0; i < CALENDAR_FACTORS.length; i++) {
					if(CALENDAR_FIELDS[i] == 0 || maxFirstSet == i) {
						long amt;
						switch(dateParameter.sign) {
							case '+':
								amt = (long) Math.ceil(dateParameter.diff / (double) CALENDAR_FACTORS[i]);
								break;
							case '-':
							default:
								amt = (dateParameter.diff / CALENDAR_FACTORS[i]);
								break;
						}
						String sval;
						if(CALENDAR_FORMAT_NAME.length > i)
							sval = "{*" + CALENDAR_FORMAT_NAME[i] + dateParameter.sign + amt + '}';
						else
							sval = "{*" + dateParameter.sign + (amt * CALENDAR_FACTORS[i]) + '}';
						url.append(urlEncode(dateParameter.name)).append('=').append(urlEncode(sval)).append('&');
						break;
					}
				}
			}
		url.setLength(url.length() - 1);
		Map<String, Object> params = new HashMap<String, Object>();
		ServletUser user = RequestUtils.getUser(form);
		params.put(SimpleServlet.ATTRIBUTE_USER, user);
		params.put("title", title);
		params.put("url", url.toString());
		try {
			DataLayerMgr.executeCall("CREATE_USER_LINK", params, true);
		} catch(SQLException e) {
			form.setAttribute("displayErrorMessage", "An error occurred while saving your report, please try again or contact Customer Service");
			log.warn("Could not save report for user " + user.getUserName(), e);
			return;
		} catch(DataLayerException e) {
			form.setAttribute("displayErrorMessage", "An error occurred while saving your report, please try again or contact Customer Service");
			log.warn("Could not save report for user " + user.getUserName(), e);
			return;
		}
		RequestUtils.getOrCreateMessagesSource(form).addMessage("success", "save-user-defined-report-success", "User defined report \"{0}\" has been added", title);
	}

	protected String urlEncode(String s) throws ServletException {
		try {
			return URLEncoder.encode(s, ENCODING);
		} catch(UnsupportedEncodingException e) {
			throw new ServletException(e);
		}
	}
}
