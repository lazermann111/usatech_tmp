package com.usatech.usalive.report;


import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usatech.usalive.hybrid.HybridServlet;
import com.usatech.usalive.servlet.UsaliveUser;

import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.NotAuthorizedException;
import simple.servlet.RequestUtils;
import simple.servlet.steps.AbstractStep;
/**
 * @author yhe
 *
 */
public class BulkResendReportStep extends AbstractStep {
	private static final Log log = Log.getLog();
	
	public void perform(Dispatcher dispatcher, InputForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException {
		String[] checkForResend;
		if(form.get("checkForResend") instanceof String){
			checkForResend=new String[1];
			checkForResend[0]=form.getString("checkForResend", true);
		}else{
			checkForResend=form.getStringArray("checkForResend", true);
		}
		Map<String, Object> params = new HashMap<String, Object>();
		UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
		StringBuilder msg=new StringBuilder();
		try{
		Connection conn = DataLayerMgr.getConnectionForCall("RESEND_REPORT");
			try{
				ArrayList<Integer> retransmitReport=new ArrayList<Integer>();
				int resendError=0;
				for(String oneReport:checkForResend){
					params.clear();
					String[] oneReportParams=oneReport.split(",");
					params.put("userReportId", oneReportParams[0]);
					params.put("checkUser", user);
					DataLayerMgr.selectInto("CHECK_USER_REPORT_PERMISSION", params);
					if(!ConvertUtils.getBoolean(params.get("permitted"), false))
						throw new NotAuthorizedException("User '" + user.getUserName() + "' is not permitted to resend the userReportId="+ oneReportParams[0]);
					if(ConvertUtils.getInt(oneReportParams[5])==9){//retransmit
						retransmitReport.add(ConvertUtils.getInt(oneReportParams[4]));
					}else{
						params.put("userReportId", oneReportParams[0]);
						params.put("batchId", oneReportParams[1]);
						params.put("beginDate", oneReportParams[2]);
						params.put("endDate", oneReportParams[3]);
						params.put("batchTypeId", oneReportParams[5]);
						params.put("simple.servlet.ServletUser.userId", user.getUserId());
						try{
							DataLayerMgr.executeUpdate(conn, "RESEND_REPORT", params);
						}catch(SQLException e){
							if(e.getErrorCode()==1){
								log.info(oneReportParams[6]+" is already scheduled for re-transmission. ");
							}else{
								msg.append(oneReportParams[6]+" fails to be scheduled for re-transmission. ");
								resendError++;
							}
						}
					}
				}
				if(resendError>0){
					try { conn.rollback(); } catch(SQLException e1) { log.warn("Could not rollback"); } 
					form.set("message", msg.toString());
					form.set("type", "error");
				}else{
					for(int fileId:retransmitReport){
						AsyncReportEngine arEngine=(AsyncReportEngine)form.getAttribute(HybridServlet.ATTRIBUTE_ASYNCH_REPORT_ENGINE);
						arEngine.publishRetransmitReport(fileId);
					}
					form.set("message", "You have successfully scheduled all checked reports for regeneration and re-tranmission");
					conn.commit();
				}
				
			}catch(Exception e){
				try { conn.rollback(); } catch(SQLException e1) { log.warn("Could not rollback"); } 
				throw new ServletException(e);
			}finally {
				conn.close();
			}
		}catch (SQLException e) {
			throw new ServletException(e);
		} catch (DataLayerException e) {
			throw new ServletException(e);
        }
		
	}
}
