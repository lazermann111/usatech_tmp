package com.usatech.usalive.report;

import java.sql.SQLException;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.falcon.engine.ExecuteEngine;
import simple.falcon.engine.Executor;
import simple.falcon.engine.Param;
import simple.falcon.engine.Report;
import simple.falcon.engine.Scene;
import simple.falcon.servlet.AbstractRunReportStep;
import simple.falcon.servlet.PromptOptions;
import simple.servlet.InputForm;

import com.usatech.usalive.hybrid.HybridServlet;
import com.usatech.usalive.servlet.UsaliveUser;
/**
 * Base class to run a folio asynchronously
 * @author bkrug
 *
 */
public class AsyncRunReportStep extends AbstractRunReportStep {

    public AsyncRunReportStep(){
		super();
	}

    
	@Override
	protected void sendReportPage(ExecuteEngine executeEngine, Report report, Scene scene, Executor executor, PromptOptions promptOption, InputForm form, Map<String, Param> paramPrompts, Map<String, Object> reportParams, Iterable<String> keepParamNames, HttpServletResponse response) throws ServletException {
		AsyncReportEngine arEngine = (AsyncReportEngine) form.getAttribute(HybridServlet.ATTRIBUTE_ASYNCH_REPORT_ENGINE);
		long userId = form.getLong("masterUserId", true, -1);
        Long refreshFileId = ConvertUtils.convertSafely(Long.class, form.get("refreshFileId"), null);
		
        long requestId;
		try {
			requestId = arEngine.requestReport(report, executor, scene, reportParams, userId, refreshFileId, keepParamNames);
		} catch(ConvertException e) {
			throw new ServletException(e);
		} catch(ExecutionException e) {
			throw new ServletException(e);
		} catch(SQLException e) {
			throw new ServletException(e);
		} catch(DataLayerException e) {
			throw new ServletException(e);
		} 
		UsaliveUser user = (UsaliveUser)executor;
		try {
			user.addRequest(requestId, 0, executor.getUserId());
		} catch(InterruptedException e) {
			//do nothing
		}
        StoredFileInfo fileInfo;
		try {
			fileInfo = arEngine.getReportResult(requestId,0);
		} catch(ExecutionException e) {
			throw new ServletException(e);
		}
		if(fileInfo != null) {
			form.set("storedFileInfo", 1);
			try {
				user.readiedRequest(requestId, 0, executor.getUserId());
			} catch(InterruptedException e) {
				//do nothing
			}
		} else
			form.set("storedFileInfo", 0);
		form.set("requestId", requestId);
		form.set("pageId", 0L);
		form.set("profileId", executor.getUserId());
		form.set("pollInterval", 1000);
		form.set("pollIntervalIndex", 1);
	}
}
