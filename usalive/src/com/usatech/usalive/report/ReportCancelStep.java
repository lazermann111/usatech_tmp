package com.usatech.usalive.report;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usatech.usalive.servlet.UsaliveUser;

import simple.bean.ConvertUtils;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.steps.AbstractStep;

/**
 * This class handles the cancelling of a report request for the user.
 *
 * @author yhe
 *
 */
public class ReportCancelStep extends AbstractStep {

	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		// TODO: broad cast to all the queue to remove the report request from cache
		UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
		try{
			user.removeRequest(ConvertUtils.getLong(form.get("requestId")),0);
		}catch(Exception e){
			throw new ServletException(e);
		}
	}

}
