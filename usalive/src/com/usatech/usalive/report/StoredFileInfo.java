package com.usatech.usalive.report;

public class StoredFileInfo {
	protected final String fileName;
	protected final String fileKey;
	protected final String fileContentType;
	protected final long fileId;
	protected final long expiration;
	public StoredFileInfo(long fileId, String fileName, String fileKey, String fileContentType, long expiration) {
		super();
		this.fileId = fileId;
		this.fileName = fileName;
		this.fileKey = fileKey;
		this.fileContentType = fileContentType;
		this.expiration = expiration;
	}
	public String getFileName() {
		return fileName;
	}
	public String getFileKey() {
		return fileKey;
	}
	public String getFileContentType() {
		return fileContentType;
	}
	public long getFileId() {
		return fileId;
	}
	public long getExpiration() {
		return expiration;
	}
}
