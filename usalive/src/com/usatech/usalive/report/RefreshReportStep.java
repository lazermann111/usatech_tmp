package com.usatech.usalive.report;

import java.util.concurrent.ExecutionException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.app.ServiceException;
import simple.bean.ConvertUtils;
import simple.falcon.engine.ExecuteEngine;
import simple.falcon.run.ReportRunner;
import simple.falcon.servlet.FalconServlet;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.steps.AbstractStep;

import com.usatech.usalive.hybrid.HybridServlet;
import com.usatech.usalive.servlet.UsaliveUser;
/**
 * Base class to run a folio asynchronously
 * @author bkrug
 *
 */
public class RefreshReportStep extends AbstractStep {

    public RefreshReportStep(){
		super();
	}
	/**
	 * @see simple.servlet.Step#perform(simple.servlet.Dispatcher, simple.servlet.InputForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		ExecuteEngine executeEngine = (ExecuteEngine) form.getAttribute(FalconServlet.ATTRIBUTE_EXECUTE_ENGINE);
		ReportRunner reportRunner = new ReportRunner(executeEngine);
		AsyncReportEngine arEngine = (AsyncReportEngine) form.getAttribute(HybridServlet.ATTRIBUTE_ASYNCH_REPORT_ENGINE);
		long requestId = form.getLong("requestId", true, -1);
        Long refreshFileId = ConvertUtils.convertSafely(Long.class, form.get("refreshFileId"), null);
        long profileId = form.getLong("profileId", true, -1);
        long userId = form.getLong("masterUserId", true, -1);

        long newRequestId;
		try {
			newRequestId = arEngine.refreshReport(reportRunner, requestId, refreshFileId, profileId, userId, form);
		} catch(ServiceException e) {
			throw new ServletException(e);
		}
		UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
		user.refreshedRequest(requestId);
		try {
			user.addRequest(newRequestId, 0, profileId);
		} catch(InterruptedException e) {
			//do nothing
		}
        StoredFileInfo fileInfo;
		try {
			fileInfo = arEngine.getReportResult(newRequestId, 0);
		} catch(ExecutionException e) {
			throw new ServletException(e);
		}

		if(fileInfo != null) {
			form.set("storedFileInfo", 1);
			try {
				user.readiedRequest(requestId, 0, profileId);
			} catch(InterruptedException e) {
				//do nothing
			}
		} else
			form.set("storedFileInfo", 0);
		form.set("requestId", newRequestId);
		form.set("pageId", 0L);
		form.set("profileId", profileId);
		form.set("pollInterval", 1000);
		form.set("pollIntervalIndex", 1);
	}
}
