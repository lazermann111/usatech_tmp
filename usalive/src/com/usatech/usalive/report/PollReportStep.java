package com.usatech.usalive.report;

import java.io.IOException;
import java.io.PrintStream;
import java.util.concurrent.ExecutionException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestInfoUtils;
import simple.servlet.RequestUtils;
import simple.servlet.steps.AbstractStep;
import simple.translator.Translator;

import com.usatech.usalive.hybrid.HybridServlet;

/**
 * This class check the resultCache to get fileId for the generated report. -1
 * indicates that the report has not yet generated.
 *
 * @author yhe, bkrug
 *
 */
public class PollReportStep extends AbstractStep {
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		try {
			long profileId = form.getLong("profileId", true, -1);
			long requestId = form.getLong("requestId", true, -1);
			int pageId = form.getInt("pageId", false, 0);
			AsyncReportEngine arEngine = (AsyncReportEngine) form.getAttribute(HybridServlet.ATTRIBUTE_ASYNCH_REPORT_ENGINE);
			StoredFileInfo fileInfo = arEngine.getReportResult(requestId, pageId);
			PrintStream out = new PrintStream(response.getOutputStream());
			out.println("<script type=\"text/javascript\">//<![CDATA[");
			if(fileInfo == null) {
				int pollIntervalIndex = form.getInt("pollIntervalIndex", true, 1);
				pollIntervalIndex++;
				int pollInterval = getNextTimeInterval(pollIntervalIndex);
				out.print("setTimeout(\"sendAjaxPoll(");
				out.print(pollIntervalIndex);
				out.print(")\",");
				out.print(pollInterval);
				out.print(");");
				if(true) {
					out.print("window.status='Waiting #");
					out.print(pollIntervalIndex);
					out.print(" (");
					out.print(pollInterval);
					out.print(" ms)';");
				}
			} else {
				String sessionToken = RequestUtils.getSessionTokenIfPresent(request);
				if(fileInfo.getFileContentType() != null && RequestInfoUtils.isDirectlyRendered(fileInfo.getFileContentType())) {
					if(form.getBoolean("embedded", false, false)) {
						out.print("new Request.HTML({ url : \"retrieve_report_by_user.i\", evalScripts: true, data:{requestId: ");
						out.print(requestId);
						out.print(", pageId: ");
						out.print(pageId);
						out.print(", profileId: ");
						out.print(profileId);
						if(sessionToken != null) {
							out.print(", \"session-token\": \"");
							out.print(sessionToken);
							out.print("\"");
						}
						out.print(", fragment: true}, update: $('cancelScreen')}).send();");
					} else {
						out.print("window.location.replace(\"retrieve_report_by_user.i?requestId=");
						out.print(requestId);
						out.print("&profileId=");
						out.print(profileId);
						out.print("&pageId=");
						out.print(pageId);
						if(sessionToken != null) {
							out.print("&session-token=");
							out.print(sessionToken);
						}
						out.print("\");$('cancelScreen').dispose();");
					}
				} else {
					StringBuilder downloadUrl = new StringBuilder();
					downloadUrl.append("retrieve_report_by_user.i?requestId=").append(requestId).append("&profileId=").append(profileId).append("&pageId=").append(pageId);
					if(sessionToken != null) {
						downloadUrl.append("&session-token=").append(sessionToken);
					}
					out.print("$('hiddenFrame').src=\"");
					out.print(downloadUrl);
					out.print("\";");
					// out.print("if(window.history && window.history.back) { window.history.back(); } else {");
					out.print("$('cancelScreen').setHTML('");
					Translator translator = form.getTranslator();
					// String html = translator.translate("report-generation-complete",
					// "<p class=\"confirmText\">Downloading your report...<br/><a href=\"report_request_history.i?isByProfile=true\">View Report History</a></p>");
					String html = translator.translate("report-generation-complete", "<div class=\"confirmText\"><div>Downloading your report...</div><div class=\"confirmNote\">(if your browser blocks the download, <a href=\"{0}\">click here</a> to manually download it)</div><div><a onclick=\"window.history.back();\">Back to original report</a></div></div>", downloadUrl);
					out.print(html.replace("\"", "\\u0022").replace("]]>", "]\\u005D>"));
					out.print("');");
					// out.print("}");
				}
			}
			out.print("//]]></script>");
			out.println();
		} catch(ExecutionException e) {
			throw new ServletException(e);
		} catch(IOException e) {
			throw new ServletException(e);
		}
	}

	/**
	 * use Fibonacci sequence to get the proper timeInterval, index start with 1
	 * 1, 1, 2, 3, 5, 8, ....
	 *
	 * @param pollIntervalIndex
	 * @return timeInterval
	 */
	public static int getNextTimeInterval(int pollIntervalIndex) {
		int n0 = 1, n1 = 1, n2 = 1;
		for(int i = 1; i < pollIntervalIndex; i++) {
			n2 = n1 + n0;
			n0 = n1;
			n1 = n2;
		}
		return n2 * 1000;

	}

}