package com.usatech.usalive.report;

import java.io.StringReader;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.falcon.engine.ExecuteEngine;
import simple.falcon.engine.Folio;
import simple.falcon.engine.standard.StandardDesignEngine;
import simple.falcon.engine.standard.StandardFilterItem;
import simple.falcon.servlet.FalconServlet;
import simple.falcon.servlet.FolioStepHelper;
import simple.falcon.servlet.UpdateFolioStep;
import simple.io.Log;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.xml.ObjectBuilder;
/**
 * 
 * @author yhe
 *
 */
public class UpdateReportRegisterFolio extends UpdateFolioStep {
	private static final Log log = simple.io.Log.getLog();
	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request,
            HttpServletResponse response) throws ServletException {
		super.perform(dispatcher, form, request, response);
		int filterBy=form.getInt("filterBy", true, 1);
		Folio folio = (Folio) form.getAttribute("folio");
		try{
			if(filterBy==1||filterBy==2){
				ExecuteEngine executeEngine = (ExecuteEngine) form.getAttribute(FalconServlet.ATTRIBUTE_EXECUTE_ENGINE);
				StandardDesignEngine designEngine = (StandardDesignEngine) executeEngine.getDesignEngine();
				StandardFilterItem filter = new StandardFilterItem(designEngine);
				ObjectBuilder filterBuilder;
				if(filterBy==1){
					form.set("params.GeneratorId", "3");
					filterBuilder=new ObjectBuilder(new StringReader("<filter fieldId=\"8834\" fieldLabel=\"Report Request Generator Id\" operatorId=\"2\"><param name=\"GeneratorId\" value=\"\" label=\"GeneratorId\" prompt=\"Enter the values for GeneratorId\" sqlType=\"NUMERIC\"><editor pattern=\"NUMBER\"/></param></filter>"));
					
				}else{
					filterBuilder=new ObjectBuilder(new StringReader("<filter fieldId=\"861\" fieldLabel=\"Sent User ReportId\" operatorId=\"1\"><param name=\"UserReportId\" value=\"\" label=\"UserReportId\" prompt=\"Enter the values for UserReportId\" sqlType=\"NUMERIC\"><editor pattern=\"NUMBER\"/></param></filter>"));
				}
				filter.readXML(filterBuilder);
				FolioStepHelper.addFilter(folio, filter, "AND");
			}
		}catch (Exception e){
			log.warn("Couldn't update report register folio", e);
            throw new ServletException(e);
		}
	}
}
