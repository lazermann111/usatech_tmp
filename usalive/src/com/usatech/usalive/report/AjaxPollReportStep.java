package com.usatech.usalive.report;

import java.io.IOException;
import java.util.Collection;
import java.util.concurrent.ExecutionException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.sax.SAXSource;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.steps.AbstractStep;
import simple.xml.AbstractXMLReader;

import com.usatech.usalive.hybrid.HybridServlet;
import com.usatech.usalive.servlet.UsaliveUser;
import com.usatech.usalive.servlet.UserRequest;

/**
 * This class will return the javascript for the client side to update the page.
 * It handles the response for report_request_history polling and banner global
 * polling.
 * 
 * @author yhe
 * 
 */
public class AjaxPollReportStep extends AbstractStep {
	protected static final String NS = "http://simple/bean/1.0";
	protected static final String[] REQUEST_ATTR_NAMES = {"requestId", "profileId", "status", "fileName"};
	protected static final String[] REQUESTS_ATTR_NAMES = {"ajaxPollInterval", "ajaxPollMaxCountReached"};
	protected long delayUntilReady = 15000;
    public long getDelayUntilReady() {
		return delayUntilReady;
	}
	public void setDelayUntilReady(long delayUntilReady) {
		this.delayUntilReady = delayUntilReady;
	}
	protected class AjaxPollResultAdapter extends AbstractXMLReader<InputSource> {
		protected final UsaliveUser user;
		protected final AsyncReportEngine arEngine;
		public AjaxPollResultAdapter(UsaliveUser user, AsyncReportEngine arEngine) {
			this.user = user;
			this.arEngine = arEngine;
		}
		@Override
		public void generateEvents(InputSource inputSource) throws IOException, SAXException {
			startElement("requests", REQUESTS_ATTR_NAMES, String.valueOf(arEngine.getAjaxPollInterval()), user.getAjaxPollCount()<arEngine.getAjaxPollMaxCount()?"N":"Y");
			Collection<UserRequest> userRequests;
			try {
				userRequests = user.getUserRequests();
			} catch(InterruptedException e) {
				throw new SAXException(e);
			} catch(ServletException e) {
				throw new SAXException(e);
			}
			for(UserRequest userRequest : userRequests) {
				try {
					StoredFileInfo fileInfo = arEngine.getReportResult(userRequest.getRequestId(),userRequest.getPageId());
					String fileName;
					if(fileInfo != null) {
						fileName = fileInfo.getFileName();
						userRequest.statusNew(getDelayUntilReady());
					} else {
						fileName = null;
					}
					startElement("request", REQUEST_ATTR_NAMES, 
							String.valueOf(userRequest.getRequestId()), 
							String.valueOf(userRequest.getProfileId()),
							userRequest.getStatus().toString(),
							fileName);
					endElement("request");
				} catch(ExecutionException e) {
					throw new SAXException(e);
				}				
			}
			
			endElement("requests");
		}

		@Override
		protected String getNameSpaceURI() {
			return NS;
		}
	};
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
		if(user != null) {
			if(form.getBoolean("resetAjaxPollCount", false, false)){
				user.setAjaxPollCount(1);
			}else{
				user.incrementAjaxPollCount();
			}
			AsyncReportEngine arEngine = (AsyncReportEngine) form.getAttribute(HybridServlet.ATTRIBUTE_ASYNCH_REPORT_ENGINE);
			form.setAttribute("ajaxPollResults", new SAXSource(new AjaxPollResultAdapter(user, arEngine), null));
		}
			/*
			boolean isGlobal = ConvertUtils.getBoolean(form.get("isGlobal"));
			PrintStream out = new PrintStream(response.getOutputStream());
			out.println("<script type=\"text/javascript\">");
			boolean needPolling = false;	
			// from request history page, need to update cancel button if needed
			if(pollRequests != null)
				for(String pollRequest : pollRequests) {
					int p = pollRequest.indexOf(':');
					if(p < 0) {
						throw new ServletException("Invalid input: requests=" + form.get("requests"));
					}
					int requestId = ConvertUtils.getInt(pollRequest.substring(0, p));
					int profileId = ConvertUtils.getInt(pollRequest.substring(p+1));
					StoredFileInfo fileInfo = arEngine.getReportResult(requestId);
					if(fileInfo != null) {
						if(isGlobal)
							out.print("handleReportComplete(");
						else 
							out.print("updateReportRequestElement(");
						out.print(requestId);
						out.print(',');
						out.print(profileId);					
						out.print(",\"");
						out.print(StringUtils.prepareScript(fileInfo.getFileName()));				
						out.print("\");");
					} else {
						needPolling = true;
					}
				}
			if(needPolling) {
				out.println("setTimeout('reportRequestAjaxPoll()', " + arEngine.getAjaxPollInterval() + ");");
			} else {
				out.println("isPollingNow=false;"); 
			}
			out.println("</script>");
			out.flush();
		} catch(IOException e) {
			throw new ServletException(e);
		} catch(ExecutionException e) {
			throw new ServletException(e);
			*/

	}

}