package com.usatech.usalive.report;

import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;

import com.usatech.report.build.AbstractBuildActivityFolio;
import com.usatech.usalive.servlet.UsaliveUser;

import simple.falcon.engine.DesignException;
import simple.falcon.engine.ExecuteEngine;
import simple.falcon.engine.Folio;
import simple.falcon.engine.Scene;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.util.CaseInsensitiveHashMap;
import simple.util.CollectionUtils;

public class BuildFolioStep extends AbstractRunCustomFolioStep {

	private AbstractBuildActivityFolio buildActivityFolio = null;

	protected Map<String, String> queryOptimizerHints = new CaseInsensitiveHashMap<String>();

	// Dependency for queryOptimizerHints. This hack because DU framework does
	// not support object injection
	private String buildActivityFolioClassName = null;

	public BuildFolioStep() {
		super();
		keepParamNames.add("sortBy");
		keepParamNames.add("showValues");
		keepParamNames.add("showPercents");
		keepParamNames.add("showAverages");
		keepParamNames.add("rangeType");
		keepParamNames.add("customerId");
		keepParamNames.add("terminals");
		keepParamNames.add("customerId_regionId");
		keepParamNames.add("outputType");
		keepParamNames.add("referrer");
		keepParamNames.add("saveAction");
	}

	@Override
	protected void updateFolio(Folio folio, InputForm form) throws ServletException {
		getBuildActivityFolio().updateFolio(folio, form, isCustomerUser(form));
	} 

	@Override
	protected Folio getFolio(ExecuteEngine executeEngine, InputForm form, Scene scene) throws ServletException {
		return getBuildActivityFolio().getFolio(executeEngine, form, scene);
	}

	protected boolean isCustomerUser(InputForm form) {
		UsaliveUser user = (UsaliveUser) RequestUtils.getUser(form);
		return user != null && user.getCustomerId() != 0 && user.getUserType() == 8;
	}

	@Override
	protected Set<String> getKeepParamNames(InputForm form) {
			Set<String> superKeepParamNames = super.getKeepParamNames(form);
			Object tmp = form.get("additionalKeepParamNames");
			if (tmp instanceof Set<?>) {
				Set<String> addKeepParamNames = CollectionUtils.uncheckedCollection((Set<?>) tmp, String.class);
				if (getBuildActivityFolio().keepAdditionalParamNames() && superKeepParamNames != null)
					addKeepParamNames.addAll(superKeepParamNames);
				return addKeepParamNames;
			}
			return superKeepParamNames;
	}

	/**
	 * com.usatech.usalive.report.AbstractRunCustomFolioStep#adjustOutputType(
	 * simple.falcon.engine.Folio, simple.servlet.InputForm,
	 * simple.falcon.engine.ExecuteEngine)
	 */
	@Override
	protected void adjustOutputType(Folio folio, InputForm form, ExecuteEngine executeEngine) throws ServletException {
		if (folio.getOutputType() == null)
			try {
				folio.setOutputType(executeEngine.getDesignEngine().getOutputType(buildActivityFolio.getDefaultOutputTypeId()));
			} catch (DesignException e) {
				throw new ServletException(e);
			}
		super.adjustOutputType(folio, form, executeEngine);
	}

	public AbstractBuildActivityFolio getBuildActivityFolio() {
		if (buildActivityFolio == null) {
			buildActivityFolio = createBuildActivityFolio(buildActivityFolioClassName);
			if (!queryOptimizerHints.isEmpty()) {
				buildActivityFolio.setQueryOptimizerHints(queryOptimizerHints);
			}
		}
		return buildActivityFolio;
	}

	public void setQueryOptimizerHint(String rangeType, String queryOptimizerHint) {
		this.queryOptimizerHints.put(rangeType, queryOptimizerHint);
	}

	private AbstractBuildActivityFolio createBuildActivityFolio(String clazz) {
		try {
			return (AbstractBuildActivityFolio) (Class.forName(clazz)).newInstance();
		} catch (Exception exception) {
			throw new RuntimeException("Failed to create new instance class " + clazz, exception);
		}

	} 

	public String getBuildActivityFolioClassName() {
		return buildActivityFolioClassName;
	}

	public void setBuildActivityFolioClassName(String buildActivityFolioClassName) {
		this.buildActivityFolioClassName = buildActivityFolioClassName;
	}
}
