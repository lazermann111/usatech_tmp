package com.usatech.usalive.report;

public class ResultCacheKey {
	protected final long requestId;
	protected final int pageId;
	public ResultCacheKey(long requestId, int pageId) {
		super();
		this.requestId = requestId;
		this.pageId = pageId;
	}
	
	public long getRequestId() {
		return requestId;
	}

	public int getPageId() {
		return pageId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + pageId;
		result = prime * result + (int) (requestId ^ (requestId >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultCacheKey other = (ResultCacheKey) obj;
		if (pageId != other.pageId)
			return false;
		if (requestId != other.requestId)
			return false;
		return true;
	}
	
}
