package com.usatech.usalive.report;

import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.falcon.engine.DesignEngine;
import simple.falcon.engine.DesignException;
import simple.falcon.engine.ExecuteEngine;
import simple.falcon.engine.Folio;
import simple.falcon.engine.Layout;
import simple.falcon.engine.Report;
import simple.falcon.engine.Scene;
import simple.falcon.servlet.FalconServlet;
import simple.falcon.servlet.FolioStepHelper;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.steps.AbstractStep;
import simple.text.LiteralFormat;
import simple.translator.Translator;

import com.usatech.usalive.hybrid.HybridServlet;
import com.usatech.usalive.servlet.UsaliveUser;

public class DashboardChartStep extends AbstractStep {

	public DashboardChartStep() {
	}

	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		ExecuteEngine executeEngine = (ExecuteEngine) form.getAttribute(FalconServlet.ATTRIBUTE_EXECUTE_ENGINE);
		DesignEngine designEngine;
		try {
			designEngine = executeEngine.getDesignEngine();
		} catch(DesignException e) {
			throw new ServletException(e);
		}
		Translator translator = form.getTranslator();
		Folio folio;
		try {
			folio = designEngine.getFolio(667, translator);
		} catch(DesignException e) {
			throw new ServletException(e);
		}
		folio.setTitle(new LiteralFormat("Sales by Type"));
		folio.setDirective("show-run-date", "false");
		folio.setDirective("show-count", "false");
		folio.setChartType("XYStackedBarChart");

		AsyncReportEngine arEngine = (AsyncReportEngine) form.getAttribute(HybridServlet.ATTRIBUTE_ASYNCH_REPORT_ENGINE);
		long userId = form.getLong("masterUserId", true, -1);
		UsaliveUser user = (UsaliveUser) RequestUtils.getUser(request);
		if(user.getUserGroupIds() == null || user.getUserGroupIds().length == 0)
			throw new ServletException("You do not have access to run reports");

		Scene scene = FolioStepHelper.createScene(form, request, translator);
		scene.setLayout(Layout.FRAGMENT);
		scene.setRunReportAction("run_report_async.i?fragment=false");
		Report report;
		try {
			report = designEngine.newReport(scene.getTranslator());
		} catch(DesignException e) {
			throw new ServletException(e);
		}
		report.setFolios(new Folio[] { folio });
		report.setName(folio.getName());
		report.setOutputType(folio.getOutputType());
		for(String dir : folio.getDirectiveNames())
			report.setDirective(dir, folio.getDirective(dir));

		Map<String, Object> reportParams = new HashMap<String, Object>();
		reportParams.put("params.StartDate", "{*DAY-90}");
		reportParams.put("params.EndDate", "{*DAY}");
		reportParams.put("params.TransTypeId", "13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32");
		Set<String> keepParamNames = Collections.emptySet();

		long requestId;
		try {
			requestId = arEngine.requestReport(report, user, scene, reportParams, userId, 0L, keepParamNames);
		} catch(ConvertException e) {
			throw new ServletException(e);
		} catch(ExecutionException e) {
			throw new ServletException(e);
		} catch(SQLException e) {
			throw new ServletException(e);
		} catch(DataLayerException e) {
			throw new ServletException(e);
		}
		try {
			user.addRequest(requestId, 0, user.getUserId());
		} catch(InterruptedException e) {
			// do nothing
		}
		StoredFileInfo fileInfo;
		try {
			fileInfo = arEngine.getReportResult(requestId, 0);
		} catch(ExecutionException e) {
			throw new ServletException(e);
		}
		if(fileInfo != null) {
			form.set("storedFileInfo", 1);
			try {
				user.readiedRequest(requestId, 0, user.getUserId());
			} catch(InterruptedException e) {
				// do nothing
			}
		} else
			form.set("storedFileInfo", 0);
		form.set("requestId", requestId);
		form.set("pageId", 0L);
		form.set("profileId", user.getUserId());
		form.set("pollInterval", 1000);
		form.set("pollIntervalIndex", 1);
		form.set("fragment", true);
		form.set("embedded", true);
	}

}
