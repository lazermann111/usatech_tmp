package com.usatech.usalive.report;

import java.io.IOException;
import java.io.PrintStream;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicLong;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.math.RandomUtils;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.MaskedString;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.IOUtils;
import simple.io.Log;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputFile;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.steps.AbstractStep;
import simple.text.StringUtils;

import com.usatech.usalive.hybrid.HybridServlet;
import com.usatech.usalive.report.TransportResultCache.GetTestTransportResult;
/**
 * 
 * @author yhe
 *
 */
public class TestTransportStep extends AbstractStep{
	private static final Log log = simple.io.Log.getLog();
	private char[] javascriptEscape={'\'', '"','\\'};
	private AtomicLong transportResultIdSeed=new AtomicLong(RandomUtils.nextLong());
	
	protected static int timeoutSeconds=300;
	
	protected static boolean testVDISoap=true;
	
	
	
	public static boolean isTestVDISoap() {
		return testVDISoap;
	}

	public static void setTestVDISoap(boolean testVDISoap) {
		TestTransportStep.testVDISoap = testVDISoap;
	}

	public static int getTimeoutSeconds() {
		return timeoutSeconds;
	}

	public static void setTimeoutSeconds(int timeoutSeconds) {
		TestTransportStep.timeoutSeconds = timeoutSeconds;
	}

	public void perform(Dispatcher dispatcher, InputForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException {
		try {
			int transportTypeId=form.getInt("transportTypeId", true, 0);
			TransportResult testTransportResult=null;
			boolean isUpdateTransportStatus=true;
			Map<Integer, String> testTransportMap = new HashMap<Integer, String>();
			if(transportTypeId==7&&!testVDISoap){
				testTransportResult=new TransportResult(true, "Currently no test available for VDI Soap Transport.");
			}else{
				Results results = DataLayerMgr.executeQuery("GET_TRANSPORT_PROPERTY_TYPES", form);
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("transport.transportTypeId", String.valueOf(transportTypeId));
				while(results.next()) {
			        int tptId = results.getValue("propertyTypeId", Integer.class);
			        String tptName = results.getValue("propertyTypeName", String.class);
					boolean sensitive = results.getValue("propertyTypeSensitive", Boolean.class);
		            Object v = form.getAttribute("tpv_" + tptId);
		            String value;
		            if(v instanceof InputFile) {
		            	try {
							value = IOUtils.readFully(((InputFile)v).getInputStream());
						} catch(IOException e) {
							throw new ServletException("Could not read file '" + ((InputFile)v).getPath() + "' for '" + tptName + "'", e);
						}
		            } else {
		                try {
							value = ConvertUtils.convert(String.class, v);
						} catch(ConvertException e) {
							throw new ServletException("Value for '" + tptName + " could not be converted to text", e);
						}
		            }
					if(sensitive)
						params.put("transport." + tptName.toUpperCase(), new MaskedString(value));
					else
						params.put("transport." + tptName.toUpperCase(), value);
		            testTransportMap.put(tptId, value==null||value.equals("")?null:value);
		        }
				long transportResultId=transportResultIdSeed.incrementAndGet();
	            TransportResultCache transportResultCache=(TransportResultCache) form.getAttribute(HybridServlet.ATTRIBUTE_TRANSPORT_RESULT_CACHE);
	            GetTestTransportResult getTask=transportResultCache.keyCache.getOrCreate(transportResultId, params);
	            
	            try{
	            	testTransportResult=getTask.get(timeoutSeconds,TimeUnit.SECONDS);
	            }catch(TimeoutException e){
	            	testTransportResult=new TransportResult(false, "Test Transport Failure: timed out after "+timeoutSeconds+" seconds.");
	            	isUpdateTransportStatus=false;
	            }finally{
	            	transportResultCache.keyCache.remove(transportResultId);
	            }
			}
	       if(form.get("transportAction").equals("test")){
        	   Results results = DataLayerMgr.executeQuery("GET_TRANSPORT_PROPERTY_VALUES_BYID", form);
    		   Map<Integer, String> transportMap = new HashMap<Integer, String>();
    		   while(results.next()) {
    			   transportMap.put(results.getValue("propertyTypeId", Integer.class), results.getValue("value", String.class));
    		   }
    		   
    		   String updateTransportName="";
    		   if(isUpdateTransportStatus&&testTransportMap.equals(transportMap)){
	            	char status;
	            	if(testTransportResult.isSuccess()){
	            		status='N';
	            		updateTransportName="updateTransportName(0);";
	            	}else{
	            		status='R';
	            	}
	            	form.set("transportStatus", status);
	            	DataLayerMgr.executeCall("UPDATE_CCS_TRANSPORT_STATUS", form, true);
	            	
	            }
	        	// This is for the Test Transport Button response.
	        	PrintStream out = new PrintStream(response.getOutputStream());
	    		out.println("<script type=\"text/javascript\">//<![CDATA[");
	    		String status=null;
	    		String statusClass=null;
	    		if(testTransportResult.isSuccess()){
	    			statusClass="info-success";
	    			status="Transport Result Success: ";
	    		}else{
	    			statusClass="info-failure";
	    			status="Transport Result Failure: ";
	    		}
	    		String details=testTransportResult.getDetails();
	    		if(details!=null){
	    			details=details.replaceAll("[\n\r]", "");
	    		}
	    		log.info("details:"+details);
	    		out.print("$('session-token').value='"+RequestUtils.getSessionToken(request)+"';updateTransportStatus('"+status+"\""+StringUtils.escape(details, javascriptEscape, '\\')+"\"','"+statusClass+"');");
	    		out.print(updateTransportName);
	    		out.print("//]]></script>");
	    		out.println();
	    		out.flush();
            }else{
            	// This is for add/update transport
            	
	            request.setAttribute("transportResultSuccess", testTransportResult.isSuccess());
            	if(!testTransportResult.isSuccess()){
            		PrintStream out = new PrintStream(response.getOutputStream());
            		out.println("<script type=\"text/javascript\">//<![CDATA[");
            		String status=null;
            		String statusClass=null;
            		statusClass="info-failure";
            		status="Transport was not saved. Transport Test Result Failure: ";
            		out.print("$('session-token').value='"+RequestUtils.getSessionToken(request)+"';updateTransportStatus('"+status+"\""+StringUtils.escape(testTransportResult.getDetails(), javascriptEscape, '\\')+"\"','"+statusClass+"');");
            		out.print("//]]></script>");
            		out.println();
            		out.flush();
            	}
            }
            
		} catch(SQLException e) {
			writeErrorResponse(request,response);
			log.error(e.getMessage(), e);
		} catch(IOException e) {
			writeErrorResponse(request,response);
			log.error(e.getMessage(), e);
		} catch(DataLayerException e) {
			writeErrorResponse(request,response);
			log.error(e.getMessage(), e);
		} catch(ConvertException e) {
			writeErrorResponse(request,response);
			log.error(e.getMessage(), e);
		} catch(ExecutionException e) {
			writeErrorResponse(request,response);
			log.error(e.getMessage(), e);
		} catch(InterruptedException e) {
			writeErrorResponse(request,response);
			log.error(e.getMessage(), e);
		}					
	}
	
	public void writeErrorResponse(HttpServletRequest request,HttpServletResponse response) throws ServletException{
		try{
			PrintStream out = new PrintStream(response.getOutputStream());
			out.println("<script type=\"text/javascript\">//<![CDATA[");
			out.print("$('session-token').value='"+RequestUtils.getSessionToken(request)+"';updateTransportStatus('An application error occurred.','info-failure');");
			out.print("//]]></script>");
			out.println();
			out.flush();
		}catch(IOException e){
			throw new ServletException(e);
		}
	}
	
}
