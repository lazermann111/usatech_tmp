package com.usatech.usalive.report;

import java.util.Map;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.io.Log;

import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;
import com.usatech.layers.common.constants.TransportAttrEnum;
import com.usatech.usalive.report.TransportResultCache.GetTestTransportResult;
/**
 * 
 * @author yhe
 *
 */
public class TestTransportResultTask  implements MessageChainTask{
	private static final Log log = simple.io.Log.getLog();
	protected TransportResultCache transportResultCache;
	
	public TransportResultCache getTransportResultCache() {
		return transportResultCache;
	}

	public void setTransportResultCache(TransportResultCache transportResultCache) {
		this.transportResultCache = transportResultCache;
	}

	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		Map<String, Object> attributes = taskInfo.getStep().getAttributes();
		try{
			long transportResultId=ConvertUtils.getLong(attributes.get(TransportAttrEnum.ATTR_TRANSPORT_RESULT_ID.getValue()));
			GetTestTransportResult result=transportResultCache.keyCache.getIfPresent(transportResultId);
			if(result==null){
				log.error("transportResultCache does not contain record for transportResultId="+transportResultId);
			}else{
				TransportResult transportResult=new TransportResult(ConvertUtils.getBoolean(attributes.get(TransportAttrEnum.ATTR_SENT_SUCCESS.getValue())),ConvertUtils.getStringSafely(attributes.get(TransportAttrEnum.ATTR_SENT_DETAILS.getValue())));
				result.set(transportResult);
			}
		}catch(ConvertException e) {
			throw new ServiceException(e);
		}	
		return 0;
	}
	
}
