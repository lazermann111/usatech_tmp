/**
 *
 */
package com.usatech.usalive.report;

import java.util.Map;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;

import com.usatech.app.AttributeConversionException;
import com.usatech.app.MessageChainTask;
import com.usatech.app.MessageChainTaskInfo;

/**
 * @author Brian S. Krug
 *
 */
public class ReportResultTask implements MessageChainTask {
	protected AsyncReportEngine asyncReportEngine;

	public int process(MessageChainTaskInfo taskInfo) throws ServiceException {
		Long requestId;
		try {
			requestId = taskInfo.getStep().getAttribute(AsyncReportEngine.ReportEngineAttribute.REQUEST_ID, Long.class, false);
			if(requestId!=null){
				Map<String, Object> attributes = taskInfo.getStep().getAttributes();
				try {
					StoredFileInfo fileInfo = new StoredFileInfo(
						ConvertUtils.getLong(attributes.get("fileId")),
						ConvertUtils.getString(attributes.get("fileName"), true),
						ConvertUtils.getString(attributes.get("fileKey"), true),
						ConvertUtils.getString(attributes.get("fileContentType"), false),
						ConvertUtils.getLong(attributes.get("fileExpiration")));
					int pageId= ConvertUtils.getInt(attributes.get("pageId"), 0);
					getNotNullAsyncReportEngine().setReportResult(requestId, pageId, fileInfo);
				} catch(ConvertException e) {
					throw new ServiceException(e);
				}
			} else {
				Long pingId = taskInfo.getStep().getAttribute(AsyncReportEngine.ReportEngineAttribute.PING_ID, Long.class, true);
				getNotNullAsyncReportEngine().setPingResult(pingId);
			}
		} catch(AttributeConversionException e) {
			throw new ServiceException(e);
		}	
		
		return 0;
	}
	protected AsyncReportEngine getNotNullAsyncReportEngine() throws ServiceException {
		AsyncReportEngine are = this.asyncReportEngine;
		if(are == null)
			throw new ServiceException("asyncReportEngine was not set on " + this);
		return are;
	}
	public AsyncReportEngine getAsyncReportEngine() {
		return asyncReportEngine;
	}
	public void setAsyncReportEngine(AsyncReportEngine asyncReportEngine) {
		this.asyncReportEngine = asyncReportEngine;
	}

}
