package com.usatech.usalive.report;


import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usatech.usalive.hybrid.HybridServlet;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.results.BeanException;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;
/**
 * Step to use transport attributes to construct a message chain to transport the generated report.
 * @author yhe
 *
 */
public class TransportErrorActivateStep extends AbstractStep {

	public void perform(Dispatcher dispatcher, InputForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException {
		try{
			int errorId=form.getInt("errorId", true, -1);
			AsyncReportEngine arEngine=(AsyncReportEngine)form.getAttribute(HybridServlet.ATTRIBUTE_ASYNCH_REPORT_ENGINE);
			form.set("transportId", arEngine.publishTransportErrorActivate(errorId));
		}catch(SQLException e){
			throw new ServletException(e);
		}catch(DataLayerException e){
			throw new ServletException(e);
		}catch(ConvertException e){
			throw new ServletException(e);
		}catch(ServiceException e){
			throw new ServletException(e);
		}catch(BeanException e){
			throw new ServletException(e);
		}
		
		
	}
}
