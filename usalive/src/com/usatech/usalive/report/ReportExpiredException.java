package com.usatech.usalive.report;

import javax.servlet.ServletException;

public class ReportExpiredException extends ServletException {
	private static final long serialVersionUID = 610757416801L;
	public ReportExpiredException() {
		// TODO Auto-generated constructor stub
	}

	public ReportExpiredException(String message) {
		super(message);
	}

	public ReportExpiredException(Throwable rootCause) {
		super(rootCause);
	}

	public ReportExpiredException(String message, Throwable rootCause) {
		super(message, rootCause);
	}

}
