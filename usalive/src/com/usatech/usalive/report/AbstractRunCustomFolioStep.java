package com.usatech.usalive.report;

import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usatech.usalive.servlet.UsaliveUser;

import simple.falcon.engine.DesignException;
import simple.falcon.engine.ExecuteEngine;
import simple.falcon.engine.Executor;
import simple.falcon.engine.Folio;
import simple.falcon.engine.OutputType;
import simple.falcon.engine.Report;
import simple.falcon.engine.Scene;
import simple.falcon.servlet.PromptOptions;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
/**
 * Base class to run a folio asynchronously
 * @author bkrug
 *
 */
public abstract class AbstractRunCustomFolioStep extends AsyncRunReportStep {
	protected int folioDefaultMaxRows = 100000;

	public AbstractRunCustomFolioStep(){
		super();
	}
	/**
	 * @see simple.falcon.servlet.AbstractRunReportStep#handleReportRequest(simple.falcon.engine.ExecuteEngine, simple.falcon.engine.Report, simple.falcon.engine.Scene, simple.falcon.engine.Executor, java.util.Map, simple.servlet.InputForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void handleReportRequest(ExecuteEngine executeEngine, Report report, Scene scene, Executor executor, Map<String, Object> reportParams, InputForm form, HttpServletRequest request,
			HttpServletResponse response) throws ServletException {
		sendReportPage(executeEngine, report, scene, executor, PromptOptions.NEVER, form, null, reportParams, getKeepParamNames(form), response);
	}

	/**
	 * @throws ServletException
	 */
	protected Folio getFolio(ExecuteEngine executeEngine, InputForm form, Scene scene) throws ServletException {
		return (Folio) form.getAttribute("folio");

	}

	/**
	 * @see simple.falcon.servlet.AbstractRunReportStep#getReport(simple.falcon.engine.ExecuteEngine, simple.servlet.InputForm, simple.falcon.engine.Scene)
	 */
	@Override
	protected Report getReport(ExecuteEngine executeEngine, InputForm form, Scene scene) throws ServletException {
		Folio folio = getFolio(executeEngine, form, scene);
		adjustOutputType(folio, form, executeEngine);
		updateFolio(folio, form);
		Report report;
		try {
			report = executeEngine.getDesignEngine().newReport(scene.getTranslator());
		} catch(DesignException e) {
			throw new ServletException(e);
		}
        report.setFolios(new Folio[] {folio});
        report.setName(folio.getName());
        report.setOutputType(folio.getOutputType());
        for(String dir : folio.getDirectiveNames())
            report.setDirective(dir, folio.getDirective(dir));
       return report;
	}

	protected void adjustOutputType(Folio folio, InputForm form, ExecuteEngine executeEngine) throws ServletException {
		int outputTypeId = form.getInt("outputType", false, 0);
		if(outputTypeId > 0) {
			OutputType outputType;
			try {
				outputType = executeEngine.getDesignEngine().getOutputType(outputTypeId);
			} catch(DesignException e) {
				throw new ServletException(e);
			}
			if(outputType != null)
				folio.setOutputType(outputType);
		}
		if(getPath() != null) {
			folio.getOutputType().setPath(getPath());
		}
		UsaliveUser user = (UsaliveUser) RequestUtils.getUser(form);
		if(!user.isInternal()){
			if(folio.getMaxRows() < 1) {
				switch(outputTypeId) {
					case 22:
					case 24:
					case 27:
						folio.setMaxRows(getFolioDefaultMaxRows());
						break;
				}
			}
		}
	}

	protected abstract void updateFolio(Folio folio, InputForm form) throws ServletException ;

	public int getFolioDefaultMaxRows() {
		return folioDefaultMaxRows;
	}

	public void setFolioDefaultMaxRows(int folioDefaultMaxRows) {
		this.folioDefaultMaxRows = folioDefaultMaxRows;
	}
}
