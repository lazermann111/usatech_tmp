package com.usatech.usalive.report;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.IOUtils;
import simple.io.resource.Resource;
import simple.io.resource.ResourceMode;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestInfoUtils;
import simple.servlet.steps.AbstractStep;

import com.usatech.usalive.hybrid.HybridServlet;

/**
 * This class is for the user to get the generated report.
 *
 * @author bkrug
 *
 */
public class RetrieveReportByPasscodeStep extends AbstractStep {

	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		AsyncReportEngine arEngine = (AsyncReportEngine) form.getAttribute(HybridServlet.ATTRIBUTE_ASYNCH_REPORT_ENGINE);
		Map<String,Object> params = new HashMap<String,Object>();
		try {
			DataLayerMgr.executeCall("GET_STORED_FILE_INFO_BY_PASSCODE", form, params, false);
			String fileName = ConvertUtils.getString(params.get("fileName"), true);
			String fileKey = ConvertUtils.getString(params.get("fileKey"), true);
			String fileContentType = ConvertUtils.getString(params.get("fileContentType"), true);
			Date expirationDate = ConvertUtils.convertRequired(Date.class, params.get("expirationDate"));
			Date now = new Date(System.currentTimeMillis());
			if(now.after(expirationDate))
				throw new ReportExpiredException();
			Resource dataSource = arEngine.getResource(fileKey, ResourceMode.READ);
			try {
				dataSource.setContentType(fileContentType);
				dataSource.setName(fileName);
				renderOutput(dataSource, response);
			} finally {
				dataSource.release();
			}
		} catch(SQLException e) {
			form.set("type", "error");
			form.set("message", form.getTranslator().translate("report-view-error"));
			dispatcher.dispatch("message_confirm", true);
		} catch(ConvertException e) {
			throw new ServletException(e);
		} catch(IOException e) {
			throw new ServletException(e);
		} catch(DataLayerException e) {
			throw new ServletException(e);
		}		
	}

	public void renderOutput(Resource dataSource, HttpServletResponse response) throws IOException {
		OutputStream ostream = response.getOutputStream();
		// NOTE: this is to accomodate IE7 and IE8 which always prompt for "application/xhtml+xml"
		response.setContentType(RequestInfoUtils.getContentTypeForLegacyBrowsers(dataSource.getContentType()));
		// if it is html, render it directly, others will prompt download
		if(!RequestInfoUtils.isDirectlyRendered(dataSource.getContentType()))
			response.setHeader("Content-Disposition", "attachment; filename=\"" + dataSource.getName() + "\"");
		IOUtils.copy(dataSource.getInputStream(), ostream);
	}

}
