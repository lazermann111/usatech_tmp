package com.usatech.usalive.report;

import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.NotAuthorizedException;
import simple.servlet.steps.AbstractStep;
/**
 * This class validate the passcode
 * @author yhe
 *
 */
public class ValidateFilePassCodeStep extends AbstractStep {

	public void perform(Dispatcher dispatcher, InputForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException {
		try{
			Results results = DataLayerMgr.executeQuery("GET_FILEPASSCODE", form);
			String filePasscodeFromUser=ConvertUtils.getString(form.getAttribute("filePasscode"), false);
			if(results.next()) {
		        String filePasscode = results.getValue("filePasscode", String.class);
		        if(!filePasscode.equals(filePasscodeFromUser)){
		        	throw new NotAuthorizedException("The filePasscode to retrieve the report is invalid.");
		        }
			}else{
				throw new NotAuthorizedException("The filePasscode to retrieve the report does not exist.");
			}
		} catch(SQLException e) {
			throw new ServletException(e);
		} catch(DataLayerException e) {
			throw new ServletException(e);
		} catch(ConvertException e) {
			throw new ServletException(e);
		}
	}

}
