package com.usatech.usalive.report;
/**
 * 
 * @author yhe
 *
 */
public class TransportResult {
	protected boolean success;
	protected String details;
	public TransportResult(boolean success, String details) {
		super();
		this.success = success;
		this.details = details;
	}
	public boolean isSuccess() {
		return success;
	}
	public String getDetails() {
		return details;
	}
}
