package com.usatech.usalive.report;

import java.io.IOException;
import java.io.PrintStream;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.steps.AbstractStep;
/**
 * 
 * @author yhe
 *
 */
public class DeleteTransportStep extends AbstractStep {
	private static final Log log = simple.io.Log.getLog();
	public DeleteTransportStep() {
	}
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		try {
			
			DataLayerMgr.executeUpdate("DELETE_CCS_TRANSPORT", form, true);
			PrintStream out = new PrintStream(response.getOutputStream());
			out.println("<script type=\"text/javascript\">//<![CDATA[");
			String statusClass, status;
			if(form.getInt("deleted", true, 0)>0){
				statusClass="info-success";
				status="Transport was deleted.";
				out.print("$('transportSelect').getSelected().dispose();");
			}else{
				statusClass="info-failure";
				status="Transport delete failed. There is user report associated with this transport.";
			}
			out.print("Form.injectSessionToken('"+RequestUtils.getSessionToken(request)+"');updateTransportDeleteStatus('"+status+"','"+statusClass+"');");
			out.print("//]]></script>");
			out.println();
			out.flush();
		} catch(SQLException e) {
			writeErrorResponse(request,response);
			log.error(e.getMessage(), e);
		} catch(DataLayerException e) {
			writeErrorResponse(request,response);
			log.error(e.getMessage(), e);
		} catch(IOException e) {
			writeErrorResponse(request,response);
			log.error(e.getMessage(), e);
		} 
	}
	
	public void writeErrorResponse(HttpServletRequest request,HttpServletResponse response) throws ServletException{
		try{
			PrintStream out = new PrintStream(response.getOutputStream());
			out.println("<script type=\"text/javascript\">//<![CDATA[");
			out.print("Form.injectSessionToken('"+RequestUtils.getSessionTokenIfPresent(request)+"');updateTransportDeleteStatus('Transport delete failed. An application error occurred.','info-failure');");
			out.print("//]]></script>");
			out.println();
			out.flush();
		}catch(IOException e){
			throw new ServletException(e);
		}
	}
}
