package com.usatech.usalive.report;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

public class GetDexFileStep extends AbstractStep {

	public void perform(Dispatcher dispatcher, InputForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException {
		try{
			Results results = DataLayerMgr.executeQuery("GET_DEX_FILE", form);
			if(results.next()) {
				String fileName = results.getValue("dexFileName", String.class);
		        String fileContent = new String (results.getValue("dexFileContent", byte[].class));
		        OutputStream ostream = response.getOutputStream();
				response.setContentType("text/plain");
				response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
				ostream.write(fileContent.getBytes());
			}else{
				throw new ServletException("The Dex file requested does not exist.");
			}
		} catch(SQLException e) {
			throw new ServletException(e);
		} catch(DataLayerException e) {
			throw new ServletException(e);
		} catch(ConvertException e) {
			throw new ServletException(e);
		} catch(IOException e) {
			throw new ServletException(e);
		} 
	}

}
