package com.usatech.usalive.report;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.SortedMap;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.InflaterInputStream;

import javax.servlet.ServletException;
import javax.sql.rowset.serial.SerialBlob;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.usatech.app.Attribute;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainService.MessageChainAttribute;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.constants.TransportAttrEnum;
import com.usatech.report.BasicReport;
import com.usatech.report.GenerateAttrEnum;
import com.usatech.report.Generator;
import com.usatech.report.ReportRequest;
import com.usatech.report.StandbyAllowance;
import com.usatech.usalive.servlet.USALiveUtils;

import simple.app.DialectResolver;
import simple.app.Publisher;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.falcon.engine.DesignException;
import simple.falcon.engine.ExecuteException;
import simple.falcon.engine.Executor;
import simple.falcon.engine.Layout;
import simple.falcon.engine.Report;
import simple.falcon.engine.Scene;
import simple.falcon.engine.standard.StandardOutputType;
import simple.falcon.engine.util.EngineUtils;
import simple.falcon.run.ReportRunner;
import simple.falcon.servlet.FolioStepHelper;
import simple.io.ByteInput;
import simple.io.Log;
import simple.io.resource.Resource;
import simple.io.resource.ResourceFolder;
import simple.io.resource.ResourceMode;
import simple.lang.EnumStringValueLookup;
import simple.lang.InvalidValueException;
import simple.results.BeanException;
import simple.results.Results;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.text.StringUtils;
import simple.util.FilterMap;
import simple.util.concurrent.AbstractFutureCache;
import simple.util.concurrent.LockSegmentFutureCache;
import simple.xml.ObjectBuilder;

/**
 * Asynchronous report that provides services to interact with the queue and serve asynchronous report generating.
 * @author yhe, bkrug
 *
 */
public class AsyncReportEngine {
	
	private static final Log log = simple.io.Log.getLog();
		
	protected static final Pattern PATTERN_REMOVE_TITLE_MARKERS = Pattern.compile("(?i)^(.+?)\\s*(?:after|for|from|in|\\(?as of|[-_]|)\\s*\\{.+\\}\\)?(.*)$");
	
	public static enum ReportEngineAttribute implements Attribute {
		REQUEST_ID("requestId"),
		PING_ID("pingId");

		private final String value;
	    
	    protected final static EnumStringValueLookup<ReportEngineAttribute> lookup = new EnumStringValueLookup<ReportEngineAttribute>(ReportEngineAttribute.class);
	    public static ReportEngineAttribute getByValue(String value) throws InvalidValueException {
	    	return lookup.getByValue(value);
	    }

		/**
		 * Internal constructor.
		 *
		 * @param value
		 */
		private ReportEngineAttribute(String value) {
			this.value = value;
		}

		/**
		 * Get the intrinsic value of the type.
		 *
		 * @return a char
		 */
		public String getValue() {
			return value;
		}
	};
	
	protected Publisher<ByteInput> publisher;
	protected final AbstractFutureCache<ReportRequest<?>, Long> requestCache = new LockSegmentFutureCache<ReportRequest<?>, Long>(500, 100, 0.75f, 16) {
		@Override
		protected Long createValue(ReportRequest<?> key, Object... additionalInfo) throws Exception {
			return createRequest(key, additionalInfo);
		}
	};
	
    protected final AbstractFutureCache<ResultCacheKey, StoredFileInfo> resultCache = new LockSegmentFutureCache<ResultCacheKey, StoredFileInfo>(500, 100, 0.75f, 16) {
		@Override
		protected StoredFileInfo createValue(ResultCacheKey key, Object... additionalInfo) throws Exception {
			return createStoredFileInfo(key, additionalInfo);
		}
    };
    protected ResourceFolder resourceFolder;
    protected int ajaxPollInterval;
    protected int ajaxPollMaxCount=100;
	protected long maxStandbyWait = 10000L;

	protected String generateQueueKey;
	protected String transportQueueKeyPrefix;
	protected String updateSentQueueKey;
	protected String transportErrorQueueKey;
	protected String resultQueueKey;
	protected final AtomicLong pingIdGenerator = new AtomicLong(Double.doubleToLongBits(Math.random()));
	protected final ConcurrentMap<Long, CountDownLatch> pingRequests = new ConcurrentHashMap<Long, CountDownLatch>();

	public AsyncReportEngine() {
		super();
	}

	public void destroy(){
		requestCache.clear();
		resultCache.clear();
	}

	/*
	protected class ReportRequestResult {
		protected final long requestId;
		protected final ReportRequest<Report> reportRequest;
		protected StoredFileInfo storedFileInfo;

		public ReportRequestResult(long requestId, ReportRequest<Report> reportRequest) {
			super();
			this.requestId = requestId;
			this.reportRequest = reportRequest;
		}
		public StoredFileInfo getStoredFileInfo() {
			return storedFileInfo;
		}
		public void setStoredFileInfo(StoredFileInfo storedFileInfo) {
			this.storedFileInfo = storedFileInfo;
		}
		public long getRequestId() {
			return requestId;
		}
		public ReportRequest<Report> getReportRequest() {
			return reportRequest;
		}
	}

	protected ReportRequestResult createReportRequestResult(ReportRunner reportRunner, long requestId, Object... additionalInfo) throws Exception {
		Map<String, Object> params = new HashMap<String, Object>(5);
        params.put("requestId", requestId);
		DataLayerMgr.selectInto("GET_REPORT_RESULT_WITH_STORED_INFO", params);
		Scene scene = reportRunner.createScene(ConvertUtils.convert(Locale.class, params.get("locale")), ConvertUtils.convert(String.class, params.get("baseUrl")));
		Report report = reportRunner.decodeReport(scene, ConvertUtils.convert(InputStream.class, params.get("reportXml")));
		long profileId = ConvertUtils.getLong(params.get("profileId"));
		SortedMap<String,String> userParams = ReportRequest.decodeParameters(ConvertUtils.convert(InputStream.class, params.get("reportParams")));
		ReportRequest<Report> rr = new ReportRequest<Report>(Generator.FOLIO, report, scene, profileId, userParams);
		ReportRequestResult rrr = new ReportRequestResult(requestId, rr);
		if(params.get("fileId") != null){
			rrr.setStoredFileInfo(new StoredFileInfo(
					ConvertUtils.getLong(params.get("fileId")),
					ConvertUtils.getString(params.get("fileName"), true),
					ConvertUtils.getString(params.get("fileKey"), true),
					ConvertUtils.getString(params.get("fileContentType"), false)));
		}
    	return rrr;
	}*/
	protected StoredFileInfo createStoredFileInfo(ResultCacheKey resultCacheKey, Object... additionalInfo) throws Exception {
		Map<String, Object> params = new HashMap<String, Object>(5);
        params.put("requestId", resultCacheKey.getRequestId());
        params.put("pageId", resultCacheKey.getPageId());
		DataLayerMgr.selectInto("GET_REPORT_RESULT", params);
		if(params.get("fileId") != null){
			return new StoredFileInfo(
					ConvertUtils.getLong(params.get("fileId")),
					ConvertUtils.getString(params.get("fileName"), true),
					ConvertUtils.getString(params.get("fileKey"), true),
					ConvertUtils.getString(params.get("fileContentType"), false),
					ConvertUtils.getLong(params.get("fileExpiration")));
		}
    	return null;
	}
	/**
     * additionalInfo are
     * reportName
     * userId
     * title
     * folioXML
     */
    protected long createRequest(ReportRequest<?> key, Object... additionalInfo) throws Exception {
    	long requestId;//check db for request id
		Connection conn = DataLayerMgr.getConnectionForCall("REPORT_REQUEST_SELECT_INS");
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("request", key);
			params.put("reportName", determineReportName(key));			
			if(additionalInfo != null && additionalInfo.length > 0 && additionalInfo[0] != null)
				params.put("refreshFileId", additionalInfo[0]);
			DataLayerMgr.executeUpdate(conn, "REPORT_REQUEST_SELECT_INS", params);
			conn.commit(); // must commit to release user application lock in database
			requestId = ConvertUtils.getLong(params.get("requestId"));
			Character reusing = ConvertUtils.convert(Character.class, params.get("reusingRequest"));
			Long storedFileId = ConvertUtils.convert(Long.class, params.get("storedFileId"));
			if(storedFileId != null) {
				log.debug("Report is already generated");
				StoredFileInfo storedFileInfo = new StoredFileInfo(
						storedFileId,
						ConvertUtils.getString(params.get("fileName"), true),
						ConvertUtils.getString(params.get("fileKey"), true),
						ConvertUtils.getString(params.get("fileContentType"), false),
						ConvertUtils.getLong(params.get("fileExpiration")));
				//add to the requestresult cache
				resultCache.put(new ResultCacheKey(requestId,0), storedFileInfo);
			} else if(reusing == null || reusing != 'Y') {
				log.debug("Publishing report to generation queue");
				DataLayerMgr.executeUpdate(conn, "REQUESTED_REPORT_REQUEST", params);
				publishReportRequest(requestId, key);
				conn.commit();
			} else {
				log.debug("Report is already requested but not yet generated");
			}
		} catch(Exception e) {
			try { conn.rollback(); } catch(SQLException e1) { log.warn("Could not rollback"); } // must rollback to release user application lock in database
			throw e;
		} catch(Error e) {
			try { conn.rollback(); } catch(SQLException e1) { log.warn("Could not rollback"); } // must rollback to release user application lock in database
			throw e;
		} finally {
			conn.close();
		}

		return requestId;
	}
    
    protected String determineReportName(ReportRequest<?> reportRequest) {
    	String reportName;
    	if(reportRequest.getReport() instanceof Report) {
			Report report = (Report)reportRequest.getReport();
			reportName = report.getOutputType().generateFileName(report, EngineUtils.createReportParameters(new FilterMap<Object>(reportRequest.getParameters(), "report.", null), null, reportRequest.getScene()));
		} else if(reportRequest.getReport() instanceof BasicReport) {
			Generator generator=reportRequest.getGenerator();
			String contentType=generator.getContentType();
			if(contentType==null){
				reportName = ((BasicReport)reportRequest.getReport()).getTitle();
			} else {
				reportName = StandardOutputType.fixFileName(StandardOutputType.appendFileExtention(new StringBuilder(((BasicReport) reportRequest.getReport()).getTitle()), contentType)).toString();
			}
		} else {
			reportName = reportRequest.getTitle();
		}
    	if (!StringUtils.isBlank(reportName)) {
    		// Remove any markers such as "{b} to {e}", etc. from the report title so markers are not displayed in USALive Recent Reports for canceled reports 
    		Matcher matcher = PATTERN_REMOVE_TITLE_MARKERS.matcher(reportName);
    		if (matcher.matches()) {
    			reportName = matcher.group(1);
    			if (!StringUtils.isBlank(matcher.group(2)))
    				reportName += matcher.group(2);
    		}
    	}
    	return reportName;
    }
    public int publishTransportErrorActivate(int errorId) throws SQLException, BeanException, DataLayerException, ConvertException, ServiceException{
    	Map<String, Object> params=new HashMap<String, Object>(11);
        params.put("errorId", errorId);
		DataLayerMgr.selectInto("GET_TRANSPORT_DETAILS_BY_ERROR_ID", params);

		MessageChainV11 mc=new MessageChainV11();
		MessageChainStep transportStep=mc.addStep(getTransportQueueKeyPrefix() + ConvertUtils.getStringSafely(params.get("transportTypeId")));
		MessageChainStep updateReportSentStep=mc.addStep(getUpdateSentQueueKey());
		MessageChainStep transportErrorStep=mc.addStep(getTransportErrorQueueKey());
		transportStep.setNextSteps(0, updateReportSentStep);
		transportStep.setNextSteps(1, transportErrorStep);

		transportStep.addStringAttribute("transport.transportReason", params.get("batchId")==null?"TIMED_REPORT":"BATCH_REPORT");
		transportStep.addStringAttribute("file.fileName", ConvertUtils.getStringSafely(params.get("fileName")));
		transportStep.addStringAttribute("file.fileKey", ConvertUtils.getStringSafely(params.get("fileKey")));
		transportStep.addStringAttribute("file.fileContentType", ConvertUtils.getStringSafely(params.get("fileContentType")));
		transportStep.addStringAttribute("transport.fileId", ConvertUtils.getStringSafely(params.get("fileId")) );
		transportStep.addStringAttribute("transport.filePasscode",ConvertUtils.getStringSafely(params.get("filePasscode")));
		transportStep.addStringAttribute("transport.retryProps",ConvertUtils.getStringSafely(params.get("retryProps")));
		addTransportPropertiesToStep(transportStep, params);

		Results results = DataLayerMgr.executeQuery("GET_CCS_TRANSPORT_ERROR_ATTR", new Object[] { errorId }, false);
		while(results.next()) {
			transportErrorStep.addStringAttribute(ConvertUtils.getStringSafely(results.getValue("attrName")), ConvertUtils.getStringSafely(results.getValue("attrValue")));
			transportStep.addStringAttribute(ConvertUtils.getStringSafely(results.getValue("attrName")), ConvertUtils.getStringSafely(results.getValue("attrValue")));
		}
		transportErrorStep.addReferenceAttribute("error.transportError", transportStep,"error.transportError" );
		transportErrorStep.addReferenceAttribute("error.errorText", transportStep,"error.errorText" );
		transportErrorStep.addReferenceAttribute("error.errorTs", transportStep,"error.errorTs" );
		transportErrorStep.addReferenceAttribute("error.errorTypeId", transportStep, "error.errorTypeId");
		transportErrorStep.addReferenceAttribute("error.retryProps", transportStep, "error.retryProps");
		transportErrorStep.addStringAttribute("reportSentId", ConvertUtils.getStringSafely(params.get("reportSentId")));
		transportErrorStep.addBooleanAttribute("retry", true);
		transportErrorStep.addIntAttribute("errorId", errorId);
		transportErrorStep.addStringAttribute("transportId", ConvertUtils.getStringSafely(params.get("transportId")));

		updateReportSentStep.addStringAttribute("reportSentId", ConvertUtils.getStringSafely(params.get("reportSentId")));
		updateReportSentStep.addReferenceAttribute("sentDate", transportStep, "sentDate");
		updateReportSentStep.addReferenceAttribute(TransportAttrEnum.ATTR_SENT_DETAILS.getValue(), transportStep, TransportAttrEnum.ATTR_SENT_DETAILS.getValue());
		updateReportSentStep.addBooleanAttribute("retry", true);
		updateReportSentStep.addIntAttribute("errorId", errorId);
		updateReportSentStep.addStringAttribute("transportId", ConvertUtils.getStringSafely(params.get("transportId")));

		MessageChainService.publish(mc, publisher);
		return ConvertUtils.getInt(params.get("transportId"));
	}
    
    public void publishRetransmitReport(int storedFileId) throws SQLException, BeanException, DataLayerException, ConvertException, ServiceException{
    	Map<String, Object> params=new HashMap<String, Object>();
        params.put("fileId", storedFileId);
		DataLayerMgr.selectInto("GET_TRANSPORT_DETAILS_BY_STORED_FILE_ID", params);

		MessageChainV11 mc=new MessageChainV11();
		MessageChainStep transportStep=mc.addStep(getTransportQueueKeyPrefix() + ConvertUtils.getStringSafely(params.get("transportTypeId")));
		MessageChainStep updateReportSentStep=mc.addStep(getUpdateSentQueueKey());
		MessageChainStep transportErrorStep=mc.addStep(getTransportErrorQueueKey());
		transportStep.setNextSteps(0, updateReportSentStep);
		transportStep.setNextSteps(1, transportErrorStep);

		transportStep.addStringAttribute("transport.transportReason", params.get("batchId")==null?"TIMED_REPORT":"BATCH_REPORT");
		transportStep.addStringAttribute("file.fileName", ConvertUtils.getStringSafely(params.get("fileName")));
		transportStep.addStringAttribute("file.fileKey", ConvertUtils.getStringSafely(params.get("fileKey")));
		transportStep.addStringAttribute("file.fileContentType", ConvertUtils.getStringSafely(params.get("fileContentType")));
		transportStep.addStringAttribute("transport.fileId", ConvertUtils.getStringSafely(params.get("fileId")) );
		transportStep.addStringAttribute("transport.filePasscode",ConvertUtils.getStringSafely(params.get("filePasscode")));
		addTransportPropertiesToStep(transportStep, params);

		transportErrorStep.addReferenceAttribute("error.transportError", transportStep,"error.transportError" );
		transportErrorStep.addReferenceAttribute("error.errorText", transportStep,"error.errorText" );
		transportErrorStep.addReferenceAttribute("error.errorTs", transportStep,"error.errorTs" );
		transportErrorStep.addReferenceAttribute("error.errorTypeId", transportStep, "error.errorTypeId");
		transportErrorStep.addReferenceAttribute("error.retryProps", transportStep, "error.retryProps");
		transportErrorStep.addStringAttribute("reportSentId", ConvertUtils.getStringSafely(params.get("reportSentId")));
		transportErrorStep.addStringAttribute("transportId", ConvertUtils.getStringSafely(params.get("transportId")));

		updateReportSentStep.addStringAttribute("reportSentId", ConvertUtils.getStringSafely(params.get("reportSentId")));
		updateReportSentStep.addReferenceAttribute("sentDate", transportStep, "sentDate");
		updateReportSentStep.addReferenceAttribute(TransportAttrEnum.ATTR_SENT_DETAILS.getValue(), transportStep, TransportAttrEnum.ATTR_SENT_DETAILS.getValue());
		updateReportSentStep.addStringAttribute("transportId", ConvertUtils.getStringSafely(params.get("transportId")));

		MessageChainService.publish(mc, publisher);
	}

	protected long getReportRequestId(ReportRequest<?> key, Long refreshStoredFileId) throws ExecutionException {
		return requestCache.getOrCreate(key, refreshStoredFileId);
	}

	public StoredFileInfo getReportResult(long requestId, int pageId) throws ExecutionException{
		return resultCache.getOrCreate(new ResultCacheKey(requestId, pageId));
	}

	public void setReportResult(long requestId, int pageId, StoredFileInfo fileInfo) {
		resultCache.put(new ResultCacheKey(requestId, pageId), fileInfo);
	}

	protected void publishReportRequest(long requestId, ReportRequest<?> reportRequest) throws ServiceException, ConvertException {
		MessageChainV11 mc = new MessageChainV11();
		MessageChainStep generateStep = mc.addStep(getGenerateQueueKey());
		MessageChainStep reportResultStep = mc.addStep(getResultQueueKey(), true); // use virtual topic
		reportResultStep.setTemporary(true);
		
		reportRequest.populateGenerateStep(generateStep, requestId);
		generateStep.setAttribute(GenerateAttrEnum.ATTR_USE_STANDBY, StandbyAllowance.NONE);
		generateStep.setAttribute(GenerateAttrEnum.ATTR_MAX_STANDBY_WAIT, getMaxStandbyWait());
		generateStep.setCorrelation(reportRequest.getUserId());
		generateStep.setNextSteps(0, reportResultStep);
		reportResultStep.addReferenceAttribute("fileId", generateStep, "transport.fileId");
		reportResultStep.addReferenceAttribute("fileName", generateStep, "file.fileName");
		reportResultStep.addReferenceAttribute("fileKey", generateStep, "file.fileKey");
		reportResultStep.addReferenceAttribute("fileContentType", generateStep, "file.fileContentType");
		reportResultStep.addReferenceAttribute("fileExpiration", generateStep, "file.fileExpiration");
		reportResultStep.addReferenceAttribute("pageId", generateStep, "file.pageId");
		reportResultStep.addLongAttribute("requestId", requestId);
		MessageChainService.publish(mc, publisher);
	}

	public long pingReportGenerator(long timeout) throws ServiceException {
		MessageChainV11 mc = new MessageChainV11();
		MessageChainStep generateStep = mc.addStep(getGenerateQueueKey());
		generateStep.setAttribute(MessageChainAttribute.PING_ONLY, true);
		generateStep.setTemporary(true);
		MessageChainStep reportResultStep = mc.addStep(getResultQueueKey(), true);
		generateStep.setNextSteps(0, reportResultStep);
		long pingId = pingIdGenerator.incrementAndGet();
		CountDownLatch result = new CountDownLatch(1);
		pingRequests.put(pingId, result);
		try {
			reportResultStep.setAttribute(ReportEngineAttribute.PING_ID, pingId);
			reportResultStep.setTemporary(true);
			long pingStartTime = System.currentTimeMillis();
			if(timeout > 0) {
				generateStep.setAttribute(MessageChainAttribute.PING_EXPIRATION, pingStartTime + timeout);
			}
			MessageChainService.publish(mc, publisher);
			try {
				boolean okay = result.await(timeout, TimeUnit.MILLISECONDS);
				if(!okay)
					throw new ServiceException("Ping did not complete in " + timeout + " ms");
			} catch(InterruptedException e) {
				throw new ServiceException("Thread was interrupted");
			}
			long time = System.currentTimeMillis() - pingStartTime;
			if(log.isInfoEnabled())
				log.info("Ping ReportGenerator completed in " + time + " ms");
			return time;
		} finally {
			pingRequests.remove(pingId);
		}
	}

	public void setPingResult(long pingId) {
		CountDownLatch result = pingRequests.get(pingId);
		if(result != null)
			result.countDown();
	}

	public long refreshReport(ReportRunner reportRunner, long requestId, Long refreshFileId, long profileId, long userId, InputForm form) throws ServiceException {
		try {
			Connection conn = DataLayerMgr.getConnectionForCall("REFRESH_REPORT_RESULT");
			boolean okay = false;
			try {
				Map<String, Object> params = new HashMap<String, Object>(5);
		        params.put("requestId", requestId);
		        params.put("refreshFileId", refreshFileId);
		        params.put("request.profileId", profileId);
				DataLayerMgr.executeCall(conn, "REFRESH_REPORT_RESULT", params);
				Layout layout;
				try {
					layout = FolioStepHelper.determineLayout(form);
				} catch(ServletException e1) {
					log.warn("Could not determine layout of report", e1);
					layout = null;
				}
				Scene scene = reportRunner.createScene(
						ConvertUtils.convert(TimeZone.class, params.get("request.scene.timeZone")),
						ConvertUtils.convert(Locale.class, params.get("request.scene.locale")),
						ConvertUtils.convert(String.class, params.get("request.scene.baseUrl")), 
						ConvertUtils.convert(String.class, params.get("request.scene.runReportAction")),
						true,
						layout);
				scene.setUserAgent(form.getHeader("user-agent"));

				String sessionToken = RequestUtils.getSessionTokenIfPresent(form);
				if(sessionToken != null)
					scene.getProperties().put("session-token", sessionToken);
				scene.getProperties().put("menuLinks", USALiveUtils.getLinks(form, "M"));
				Blob reportBlob = null;
				Blob paramsBlob = null;
				if (DialectResolver.isOracle()) {
					reportBlob = ConvertUtils.convert(Blob.class, params.get("request.reportXml"));
					paramsBlob = ConvertUtils.convert(Blob.class, params.get("request.reportParams"));
				} else {
					byte[] reportBytes = ConvertUtils.convert(byte[].class, params.get("request.reportXml"));
					reportBlob = new SerialBlob(reportBytes);
					byte[] paramsBytes = ConvertUtils.convert(byte[].class, params.get("request.reportParams"));
					paramsBlob = new SerialBlob(paramsBytes);
				} 
				boolean compressing = ConvertUtils.getBoolean(params.get("request.compressing"), true);
				SortedMap<String,String> userParams;
				if(paramsBlob != null && paramsBlob.length() > 0)
					userParams = ReportRequest.decodeParameters(paramsBlob.getBinaryStream(), compressing);
				else
					userParams = new TreeMap<String, String>();
				Generator generator = ConvertUtils.convertRequired(Generator.class, params.get("request.generator"));
				Object report;
				String title;
				if(reportBlob != null && reportBlob.length() > 0) {
					Reader reader = new InputStreamReader(compressing ? new InflaterInputStream(reportBlob.getBinaryStream()) : reportBlob.getBinaryStream(), ReportRunner.XML_CHARSET);
			    	try {
			    		ObjectBuilder builder = new ObjectBuilder(reader);
			    		String className = builder.getAttrValue("class");
						if(className == null) {
			    			report = null;
							title = null;
						} else if(className.equals(Report.class.getName())) {
			    			Report falconReport = reportRunner.getExecuteEngine().getDesignEngine().newReport(scene.getTranslator());
			    	        falconReport.readXML(builder);
							ReportRunner.adjustSceneForReport(scene, falconReport);
			    	        report = falconReport;
							title = falconReport.getOutputType().generateFullTitle(falconReport, EngineUtils.createReportParameters(new FilterMap<Object>(userParams, "report.", null), null, scene));
			    		} else {
			    			report = builder.read(Class.forName(className));
							title = builder.get("title", String.class);
			    		}
			    	} catch(ParserConfigurationException e) {
						throw new ServiceException("Could not read report definition", e);
					} finally {
			    		reader.close();
			    	}
				} else {
					report = null;
					title = null;
				}

				ReportRequest<?> rr = new ReportRequest<Object>(generator, report, scene, profileId, userId, userParams, title);
				long newRequestId = ConvertUtils.getLong(params.get("newRequestId"));
				requestCache.put(rr, newRequestId);
				Character reusing = ConvertUtils.convert(Character.class, params.get("reusingRequest"));
				Long storedFileId = ConvertUtils.convert(Long.class, params.get("storedFileId"));
				if(storedFileId != null) {
					log.debug("Report is already generated");
					StoredFileInfo storedFileInfo = new StoredFileInfo(
							storedFileId,
							ConvertUtils.getString(params.get("fileName"), true),
							ConvertUtils.getString(params.get("fileKey"), true),
							ConvertUtils.getString(params.get("fileContentType"), false),
							ConvertUtils.getLong(params.get("fileExpiration")));
					//add to the requestresult cache
					resultCache.put(new ResultCacheKey(newRequestId,0), storedFileInfo);
				} else if(reusing == null || reusing != 'Y') {
					log.debug("Publishing report to generation queue");
					publishReportRequest(newRequestId, rr);
				} else {
					log.debug("Report is already requested but not yet generated");
				}
				params.clear();
				params.put("requestId", newRequestId);
				params.put("userId", userId);
				params.put("pageId", 0);
				DataLayerMgr.executeUpdate(conn, "REPORT_REQUEST_USER_INS", params);
				conn.commit();  // must commit to release user application lock in database
				okay = false;
				return newRequestId;
			} catch(ConvertException e) {
				throw new ServiceException(e);
			} catch(IOException e) {
				throw new ServiceException(e);
			} catch(ClassNotFoundException e) {
				throw new ServiceException(e);
			} catch(SAXException e) {
				throw new ServiceException(e);
			} catch(DesignException e) {
				throw e.getCause() instanceof ServiceException ? (ServiceException) e.getCause() : new ServiceException(e);
			} catch(ExecuteException e) {
				throw e.getCause() instanceof ServiceException ? (ServiceException) e.getCause() : new ServiceException(e);
			} catch(SQLException e) {
				throw new ServiceException(e);
			} catch(DataLayerException e) {
				throw new ServiceException(e);
			} catch(ParserConfigurationException e) {
				throw new ServiceException(e);
			} finally {
				if(!okay)
					try {
						conn.rollback();
					} catch(SQLException e1) {
						log.warn("Could not rollback");
					} // must rollback to release user application lock in database
				conn.close();
			}
		} catch(SQLException e) {
			throw new ServiceException(e);
		} catch(DataLayerException e) {
			throw new ServiceException(e);
		}
	}
	public long requestReport(Report report, Executor executor, Scene scene, Map<String,Object> reportParams, long userId, Long refreshFileId, Iterable<String> keepParamNames) throws ConvertException, ExecutionException, SQLException, DataLayerException {
		SortedMap<String, String> extractedParams = new TreeMap<String, String>();
		EngineUtils.extractParamsForAsync(report, scene, reportParams, new FilterMap<String>(extractedParams, "report.", null), keepParamNames);
		String title = report.getOutputType().generateFullTitle(report, EngineUtils.createReportParameters(new FilterMap<Object>(extractedParams, "report.", null), null, scene));
		ReportRequest<Report> reportRequest = new ReportRequest<Report>(Generator.FOLIO, report, scene, executor.getUserId(), userId, extractedParams, title);
		return requestReport(reportRequest, executor.getUserId(), userId, refreshFileId);
	}
	
	public long requestReport(ReportRequest<?> reportRequest, long profileId, long userId, Long refreshFileId) throws ExecutionException, SQLException, DataLayerException {
		long requestId = getReportRequestId(reportRequest, refreshFileId);
        HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("requestId", requestId);
		params.put("userId", userId);
		params.put("pageId", 0);
		DataLayerMgr.executeUpdate("REPORT_REQUEST_USER_INS", params, true);
		return requestId;
	}
	
	public Resource getResource(String fileKey, ResourceMode mode) throws IOException{
		return resourceFolder.getResource(fileKey, mode);
	}

	public int getAjaxPollInterval() {
		return ajaxPollInterval;
	}
	public static void addTransportPropertiesToStep(MessageChainStep step, Map<String, Object> tranParams)throws ConvertException, DataLayerException, SQLException{
		//logic to add transport
        String transportClassName = ConvertUtils.convert(String.class, tranParams.get("transportClassName"));
        Results propResults = DataLayerMgr.executeQuery("GET_TRANSPORT_PROPERTIES", tranParams, false);
        while(propResults.next()) {
        	step.addStringAttribute("transport."+ConvertUtils.convert(String.class, propResults.getValue("name")), ConvertUtils.convert(String.class, propResults.getValue("value")));
        }
        step.addStringAttribute("transport.transportTypeId", ConvertUtils.getStringSafely(tranParams.get("transportTypeId")));
        step.addStringAttribute("transport.transportClassName", transportClassName);
	}

	public String getGenerateQueueKey() {
		return generateQueueKey;
	}

	public void setGenerateQueueKey(String generateQueueKey) {
		this.generateQueueKey = generateQueueKey;
	}

	public String getTransportQueueKeyPrefix() {
		return transportQueueKeyPrefix;
	}

	public void setTransportQueueKeyPrefix(String transportQueueKey) {
		this.transportQueueKeyPrefix = transportQueueKey;
	}

	public String getUpdateSentQueueKey() {
		return updateSentQueueKey;
	}

	public void setUpdateSentQueueKey(String updateSentQueueKey) {
		this.updateSentQueueKey = updateSentQueueKey;
	}

	public String getTransportErrorQueueKey() {
		return transportErrorQueueKey;
	}

	public void setTransportErrorQueueKey(String transportErrorQueueKey) {
		this.transportErrorQueueKey = transportErrorQueueKey;
	}

	public String getResultQueueKey() {
		return resultQueueKey;
	}

	public void setResultQueueKey(String resultQueueKey) {
		this.resultQueueKey = resultQueueKey;
	}

	public Publisher<ByteInput> getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher<ByteInput> publisher) {
		this.publisher = publisher;
	}

	public void setAjaxPollInterval(int ajaxPollInterval) {
		this.ajaxPollInterval = ajaxPollInterval;
	}

	public ResourceFolder getResourceFolder() {
		return resourceFolder;
	}

	public void setResourceFolder(ResourceFolder resourceFolder) {
		this.resourceFolder = resourceFolder;
	}

	public int getAjaxPollMaxCount() {
		return ajaxPollMaxCount;
	}

	public void setAjaxPollMaxCount(int ajaxPollMaxCount) {
		this.ajaxPollMaxCount = ajaxPollMaxCount;
	}

	public long getMaxStandbyWait() {
		return maxStandbyWait;
	}

	public void setMaxStandbyWait(long maxStandbyWait) {
		this.maxStandbyWait = maxStandbyWait;
	}
	
	

}
