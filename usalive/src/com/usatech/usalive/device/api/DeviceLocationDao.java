package com.usatech.usalive.device.api;

import com.usatech.usalive.device.dto.DeviceLocation;

/**
 * DAO to get device network location
 */
public interface DeviceLocationDao {
    /**
     * Get device geo location for devices which connected via wireless mobile network
     *
     * @param deviceId    Device ID
     * @return  Device location
     */
    DeviceLocation getLatestDeviceLocation(long deviceId);

    /**
     * Schedule device network location update ASAP
     *
     * @param deviceId    Device ID
     */
    void scheduleAsapDeviceLocationUpdate(long deviceId);
}
