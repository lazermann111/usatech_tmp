package com.usatech.usalive.device.dto;

import java.io.Serializable;
import java.util.Date;

import com.usatech.layers.common.util.WebHelper;

/**
 * Device location info DTO
 */
public class DeviceLocation implements Serializable {

	private static final long serialVersionUID = -174907701327315050L;

	private Long deviceId;

	private Date updatedAt;

	private String country;

	private String state;

	private String city;

	private String zip;

	private Float latitude;

	private Float longitude;

	private Date requestedAt;

	private boolean updateRestricted;

	private String errorCode;
	
	private String gooleMapUrl;

	public DeviceLocation() {
	}

	public boolean isUpdateRestricted() {
		return updateRestricted;
	}

	public void setUpdateRestricted(boolean updateRestricted) {
		this.updateRestricted = updateRestricted;
	}

	public Long getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(Long deviceId) {
		this.deviceId = deviceId;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public Float getLatitude() {
		return latitude;
	}

	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}

	public Float getLongitude() {
		return longitude;
	}

	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}

	public Date getRequestedAt() {
		return requestedAt;
	}

	public void setRequestedAt(Date requestedAt) {
		this.requestedAt = requestedAt;
	}

	public String getStrUpdatedAt() {
		if (getUpdatedAt() == null) {
			return null;
		}
		return WebHelper.convertDateToLocalStr(getUpdatedAt());
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getGooleMapUrl() {
		return gooleMapUrl;
	}

	public void setGooleMapUrl(String gooleMapUrl) {
		this.gooleMapUrl = gooleMapUrl;
	}
}
