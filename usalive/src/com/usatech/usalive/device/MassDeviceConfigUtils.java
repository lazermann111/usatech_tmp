package com.usatech.usalive.device;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.DeviceProperty;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.FileType;
import com.usatech.layers.common.device.DeviceConfigurationUtils;
import com.usatech.layers.common.device.DevicesConstants;
import com.usatech.layers.common.model.ConfigTemplateSetting;
import com.usatech.layers.common.util.DeviceUtils;
import com.usatech.layers.common.util.StringHelper;
import com.usatech.layers.common.util.WebHelper;
import com.usatech.usalive.servlet.USALiveUtils;
import com.usatech.usalive.servlet.UsaliveUser;

import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.text.StringUtils;

public class MassDeviceConfigUtils {
	private static final Log log = Log.getLog();
		
	protected static String convenienceFeeNotifications;
	
	public static boolean processMassDeviceConfig(InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
		String action = form.getString("action", false);
    	if (!"Save and Send".equalsIgnoreCase(action))
    		return false;
		
    	Connection conn = null;
    	Results results = null;
    	boolean success = false;
    	try {
    		UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
    		String userName = user.getUserName();
    		String serialNumberList = form.getStringSafely("serial_number_list", "").replace('\n', ',').replace("\r", "").replace(" ", "");
    		
    		Map<String, Object> params = new HashMap<String, Object>();
    		params.put("checkUser", user);
    		params.put("eportSerialNums", serialNumberList);
    		
    		conn = DataLayerMgr.getConnection("OPER");
    		results = DataLayerMgr.executeQuery(conn, "GET_DEVICES_FOR_MASS_CONFIGURATION", params);
			
	    	long deviceId;	    	
	        int deviceTypeId = -1;
	        int prevDeviceTypeId = -2;
	        String deviceSerialCd;
	        String deviceName;
	        String customerName;
	        int plv = -1;
	        int prevPlv = -2;	        
	        int updatedDeviceCount = 0;
	        LinkedHashMap<String, ConfigTemplateSetting> defaultSettingData = null;
	        LinkedHashMap<String, String> deviceSettingData;
	        String[] paramsToChange = null;
	        boolean coinPulseMsg = false;
	        Map<String, String> errorMap = new HashMap<String, String>();
	        StringBuilder errorMessage = new StringBuilder();
	        StringBuilder twoTierContent = new StringBuilder();
	        StringBuilder deviceTwoTierContent = new StringBuilder();
	    	while (results.next()){
	    		deviceId = results.getValue("DEVICE_ID", long.class);
	    		if (deviceId < 1)
	                continue;
	            deviceTypeId = results.getValue("DEVICE_TYPE_ID", int.class);
	            deviceSerialCd = results.getValue("DEVICE_SERIAL_CD", String.class);
	            deviceName = results.getValue("DEVICE_NAME", String.class);
	            plv = results.getValue("PROPERTY_LIST_VERSION", int.class);
	            customerName = results.getFormattedValue("CUSTOMER_NAME");
	    	
		    	boolean isGx = deviceTypeId == DeviceType.GX.getValue() || deviceTypeId == DeviceType.G4.getValue();
		    	boolean isEdge = deviceTypeId == DeviceType.EDGE.getValue();
		    	boolean isMEI = deviceTypeId == DeviceType.MEI.getValue();
		    	boolean isCrane = WebHelper.isCraneDevice(deviceSerialCd);
		    	
		    	if (prevDeviceTypeId != deviceTypeId) {
		    		if (isEdge && prevPlv != plv || !isEdge) {
			    		try {
							defaultSettingData = DeviceConfigurationUtils.getDefaultConfFileDataByDeviceTypeId(deviceTypeId, plv, null, "Y", deviceId, conn);
						} catch (Exception e) {
							throw new ServletException(e);
						}
			    		prevPlv = plv;
		    		}
					paramsToChange = request.getParameterValues(new StringBuilder("param_to_change_").append(deviceTypeId).toString());
					prevDeviceTypeId = deviceTypeId;
		    	}
		    	
		    	if (paramsToChange == null)
		    		continue;
		    	
				deviceSettingData = DeviceUtils.getDeviceSettingData(defaultSettingData, deviceId, deviceTypeId, true, conn);
		    	if (deviceSettingData == null)
		    		continue;
		    	
		    	int changeCount = 0;
				int callInTimeChangeCount = 0;
				int callInTimeWindowChangeCount = 0;
				int serverOnlyChangeCount = 0;
				StringBuilder newIniFileShort = new StringBuilder();
				boolean coinPulseChange = false;
				errorMap.clear();
				deviceTwoTierContent.setLength(0);
	    		
				for (String key: paramsToChange) {
					ConfigTemplateSetting defaultSetting = defaultSettingData.get(key);
    				if (!defaultSetting.isSupported())
    					continue;
					if (isEdge||isCrane) {
						int keyNumber = StringHelper.isInteger(key) ? Integer.valueOf(key) : -1;
						boolean storedInPennies = "Y".equalsIgnoreCase(defaultSetting.getStoredInPennies());
						String editorType = defaultSetting.getEditorType();
	    				String oldParamValue = deviceSettingData.get(key);
	    				if (oldParamValue == null)
	    					oldParamValue = "";
	    				String paramValue = form.getString(new StringBuilder("cfg_field_").append(deviceTypeId).append("_").append(key).toString(), false);	    				
	    				String enteredValue = paramValue;
	    				boolean isDailyExtendedSchedule=WebHelper.isDailyExtendedSchedule(defaultSetting);
	    				if (paramValue == null&&!isDailyExtendedSchedule)
	    					paramValue = oldParamValue;
	    				if ("SCHEDULE".equalsIgnoreCase(editorType)&&!isDailyExtendedSchedule)
							paramValue = WebHelper.buildSchedule(request, defaultSetting, new StringBuilder().append(deviceTypeId).append("_").append(key).toString(), paramValue, results.getValue("device_utc_offset_min", int.class));
	    				String defaultConfigValue = defaultSetting.getConfigTemplateSettingValue();
	    				if (defaultConfigValue == null)
	    					defaultConfigValue = "";			
	    				
	    				// if a DNS host name or IP address doesn't have two dots, use the default value
						if(DeviceUtils.EDGE_COMM_SETTING_EXPRESSION.matcher(key).matches() && !DeviceUtils.TWO_DOT_EXPRESSION.matcher(paramValue).find()) {
							errorMap.put("Value validation failed for field: " + defaultSetting.getLabel(), "value: \"" + paramValue + "\"");
							continue;
	    				}

	    				String regex = defaultSetting.getRegex();
	    				if(!StringHelper.isBlank(regex)){
							if(isDailyExtendedSchedule){
								if(!StringUtils.isBlank(paramValue)){
									String[] paramValueList=paramValue.split(",");
									for(String paramValueListValue:paramValueList){
										if(!StringHelper.validateRegexWithMessage(errorMap, paramValueListValue, key, regex, "Value validation failed for field: " + defaultSetting.getLabel())){
											continue;
										}
									}
								}
								paramValue=WebHelper.buildScheduleDailyExtended(paramValue);
							}else if(!StringHelper.validateRegexWithMessage(errorMap, paramValue, key, regex, "Value validation failed for field: " + defaultSetting.getLabel())){
								continue;
							}
						}
	    				
						if(DeviceUtils.EDGE_SCHEDULE_EXPRESSION.matcher(key).matches() && !DeviceUtils.validateEdgeSchedule(key, paramValue, errorMap, defaultSetting.getLabel()))
	    					continue;
						
						if (storedInPennies && StringHelper.isNumeric(paramValue))
		    				paramValue = String.valueOf((new BigDecimal(paramValue).multiply(BigDecimal.valueOf(100)).intValue()));
	    					
	    				if (defaultSetting.isServerOnly()) {
	    					if (!StringHelper.equalConfigValues(paramValue, oldParamValue, storedInPennies)) {
	    						DeviceConfigurationUtils.upsertDeviceSetting(deviceId, key, paramValue, conn, userName);
	    						changeCount++;
	    						serverOnlyChangeCount++;
	    						if (key.startsWith("CALL_IN_TIME_WINDOW_"))
	    							callInTimeWindowChangeCount++;
	    					}
	    					continue;
	    				}
	    					    				
	    				if(!deviceSettingData.containsKey(key) || !StringHelper.equalConfigValues(oldParamValue, paramValue, storedInPennies)){
	    					if (keyNumber == DeviceProperty.CONVENIENCE_FEE.getValue() && StringHelper.isNumeric(paramValue) && new BigDecimal(paramValue).compareTo(BigDecimal.valueOf(10)) == 1) {
		    					errorMap.put("New " + defaultSetting.getName() + " value cannot be greater than 0.10. Please correct.", "value: \"" + paramValue + "\"");
		    					continue;
			    			}
	    					
	    					if(StringHelper.equalConfigValues(defaultConfigValue, paramValue, storedInPennies)) {
	    						newIniFileShort.append(key).append("!\n");
	    					} else
	    						newIniFileShort.append(key).append("=").append(paramValue).append("\n");
	    					changeCount++;
	    					
	    					if ("SCHEDULE".equalsIgnoreCase(defaultSetting.getEditorType()))
	    						callInTimeChangeCount++;
		    				DeviceConfigurationUtils.upsertDeviceSetting(deviceId, key, paramValue, null, conn, userName);
		    				
		    				if (keyNumber == DeviceProperty.CONVENIENCE_FEE.getValue() 
			    					&& ((StringHelper.isBlank(oldParamValue) || new BigDecimal(oldParamValue).compareTo(BigDecimal.ZERO) == 0)
			    					&& !StringHelper.isBlank(enteredValue) && new BigDecimal(enteredValue).compareTo(BigDecimal.ZERO) == 1
			    					|| (StringHelper.isBlank(enteredValue) || new BigDecimal(enteredValue).compareTo(BigDecimal.ZERO) == 0)
			    					&& !StringHelper.isBlank(oldParamValue) && new BigDecimal(oldParamValue).compareTo(BigDecimal.ZERO) == 1))
		    					buildTwoTierContent(deviceTwoTierContent, customerName, deviceSerialCd, enteredValue);
		    				
		    				if (DevicesConstants.EDGE_COIN_PULSE_FIELDS.contains(keyNumber) && keyNumber != DeviceProperty.VMC_INTERFACE_TYPE.getValue())
			    				coinPulseChange = true;
	    				}	    				
		    		} else if (isGx || isMEI) {
		    			int offSet = defaultSetting.getFieldOffset();
		    			int size = defaultSetting.getFieldSize();
		    			
		    			String name          = defaultSetting.getName();
		    		    String align         = defaultSetting.getAlign();
		    			String padWith       = defaultSetting.getPadChar();
		    			String dataMode      = defaultSetting.getDataMode();
						String eeromLocation = defaultSetting.getEeromLocation();
		    			String regex 		 = defaultSetting.getRegex();
		    			boolean storedInPennies = "Y".equalsIgnoreCase(defaultSetting.getStoredInPennies());
		    			
		    			if (DevicesConstants.GX_MAP_PROTECTED_FIELDS.contains(offSet) && isGx) {
		    				DeviceConfigurationUtils.upsertDeviceSetting(deviceId, key, deviceSettingData.get(key), null, conn, userName);
		    				continue;
		    			}
		    		    
		    			String oldParamValue = deviceSettingData.get(key);
	    				if (oldParamValue == null)
	    					oldParamValue = "";
		    		    String paramValue = form.getString(new StringBuilder("cfg_field_").append(deviceTypeId).append("_").append(key).toString(), false);
		    		    String enteredValue = paramValue;
		    			if (paramValue == null)
		    				paramValue = oldParamValue;		    			
		    			
		    			if(!StringHelper.isBlank(paramValue) && !StringHelper.isBlank(regex)) {
		    				if (!StringHelper.validateRegexWithMessage(errorMap, paramValue, name, regex, "Value validation failed for field: " + defaultSetting.getLabel()))
		    					continue;
		    			}
		    			
		    			if (storedInPennies && StringHelper.isNumeric(paramValue))
		    				paramValue = String.valueOf((new BigDecimal(paramValue).multiply(BigDecimal.valueOf(100)).intValue()));
		    				
		    			if (defaultSetting.isServerOnly()) {
		    				if (!StringHelper.equalConfigValues(paramValue, oldParamValue, storedInPennies)) {
		    					DeviceConfigurationUtils.upsertDeviceSetting(deviceId, key, paramValue, null, conn, userName);
		    					if (key.startsWith("CALL_IN_TIME_WINDOW_"))
		    						callInTimeWindowChangeCount++;
		    					changeCount++;				
		    				}
		    				continue;
		    			}
		    		
		    			// data changed
		    			if (!StringHelper.equalConfigValues(paramValue, oldParamValue, storedInPennies)) {
		    				if (offSet == DevicesConstants.GX_MAP_CONVENIENCE_FEE && StringHelper.isNumeric(paramValue) && new BigDecimal(paramValue).compareTo(BigDecimal.valueOf(10)) == 1) {
		    					errorMap.put("New " + name + " value cannot be greater than 0.10. Please correct.", "value: \"" + paramValue + "\"");
		    					continue;
			    			}
		    				
		    				changeCount++;
		    				
		    				if (offSet == DevicesConstants.GX_MAP_CALL_IN_TIME && isGx || offSet == DevicesConstants.MEI_MAP_CALL_IN_TIME && isMEI)
		    					callInTimeChangeCount++;
		    		
	    					// insert a Poke for this memory location
	    					int pokeLoc = 0;
	    					int pokeSize = 0;

	    					if(isGx) {
	    						// Gx uses a 2 byte per address scheme, ugh...
	    						// $eerom_location should already be adjusted
	    						pokeLoc = Integer.parseInt(eeromLocation, 16);
	    						pokeSize = size;

	    						if(pokeSize == 1) {
	    							// you must send at least 2 bytes
	    							pokeSize = 2;
	    						}
	    					} else if (isMEI) {
	    					    pokeLoc = 0;
	    						pokeSize = DevicesConstants.MEI_CONFIG_SIZE;
	    					}

	    					DeviceUtils.createPoke(conn, deviceName, pokeLoc, pokeSize, 0);
	    					
			    			if (("H".equals(dataMode) && paramValue.length() < (size * 2)) || (!"H".equals(dataMode)) && paramValue.length() < size) {
			    			    String[] paddedResult = DeviceUtils.pad(paramValue, padWith, size, align, dataMode);
			    			    paramValue = paddedResult[1];
			    			}
			    			
			    			String outStr;
			    			int outLength;
			    			if ("H".equalsIgnoreCase(dataMode)) {
			    				outStr = paramValue.toUpperCase();
			    				outLength = paramValue.length() / 2;
			    			} else { 
			    				outStr = StringHelper.encodeHexString(paramValue);
			    				outLength = paramValue.length();
			    			}
			    			if (outLength != size)
			    			    throw new Exception("Fatal Error: the length of OUT value is invalid! expecting " + size + " but got " + outLength);
			    			
			    			DeviceConfigurationUtils.upsertDeviceSetting(deviceId, key, outStr, null, conn, userName);
			    			
			    			if (offSet == DevicesConstants.GX_MAP_CONVENIENCE_FEE && isGx 
			    					&& ((StringHelper.isBlank(oldParamValue) || new BigDecimal(oldParamValue).compareTo(BigDecimal.ZERO) == 0)
			    					&& !StringHelper.isBlank(enteredValue) && new BigDecimal(enteredValue).compareTo(BigDecimal.ZERO) == 1
			    					|| (StringHelper.isBlank(enteredValue) || new BigDecimal(enteredValue).compareTo(BigDecimal.ZERO) == 0)
			    					&& !StringHelper.isBlank(oldParamValue) && new BigDecimal(oldParamValue).compareTo(BigDecimal.ZERO) == 1))
			    				buildTwoTierContent(deviceTwoTierContent, customerName, deviceSerialCd, enteredValue);
			    			
			    			if (DevicesConstants.GX_MAP_COIN_PULSE_FIELDS.contains(offSet) && offSet != DevicesConstants.GX_MAP_VENDOR_INTERFACE_TYPE)
			    				coinPulseChange = true;
		    			}
					}
				}

				if(errorMap.size() > 0) {
					Collection<String> errors = (Collection<String>) errorMap.keySet();
					for(String error : errors) {
						if (errorMessage.indexOf(new StringBuilder(error).append("\n").toString()) < 0)
							errorMessage.append(error).append("\n");
					}
					conn.rollback();
				} else {
					if(isEdge && changeCount > serverOnlyChangeCount) {
						long newFileId = DeviceUtils.getNextFileTransferSequenceNum(conn);
						DeviceUtils.saveFile(new StringBuilder(deviceName).append("-CFG-").append(newFileId).toString(), newIniFileShort.toString(), newFileId, FileType.PROPERTY_LIST.getValue(), conn);
						DeviceUtils.sendCommand(deviceTypeId, deviceId, deviceName, newFileId, newIniFileShort.length(), DeviceUtils.DEFAULT_PACKET_SIZE, DeviceUtils.DEFAULT_EXECUTE_ORDER, conn, null, false);
					}

					if(callInTimeWindowChangeCount > 0)
						DataLayerMgr.executeCall(conn, "RESET_DEVICE_CALL_IN_TIME_NORMALIZATION", new Object[] { 1, deviceId });
					else if(callInTimeChangeCount > 0)
						DataLayerMgr.executeCall(conn, "RESET_DEVICE_CALL_IN_TIME_NORMALIZATION", new Object[] { 0, deviceId });
										
					if (!coinPulseMsg && coinPulseChange) {
						if (isGx && !"07".equalsIgnoreCase(deviceSettingData.get(String.valueOf(DevicesConstants.GX_MAP_VENDOR_INTERFACE_TYPE))))
							coinPulseMsg = true;
						else if (isEdge) {
							String vmcInterfaceType = deviceSettingData.get(String.valueOf(DeviceProperty.VMC_INTERFACE_TYPE.getValue()));
							if (!"3".equalsIgnoreCase(vmcInterfaceType) && !"4".equalsIgnoreCase(vmcInterfaceType) && !"5".equalsIgnoreCase(vmcInterfaceType))
								coinPulseMsg = true;
						}
					}
					conn.commit();
					
					if (changeCount > 0) {
						updatedDeviceCount++;
						if (deviceTwoTierContent.length() > 0)
							twoTierContent.append(deviceTwoTierContent.toString());
					}
	    		}
	    	}
	    	
	    	if (twoTierContent.length() > 0) {
	    		sendConvenienceFeeNotification(conn, twoTierContent.toString());
	    		conn.commit();
	    	}
	    	
	    	StringBuilder successMessage = new StringBuilder();
			if (updatedDeviceCount > 0)
				successMessage.append("Configuration updates scheduled for ").append(updatedDeviceCount).append(" devices.\n");
			else
				successMessage.append("No configuration changes.\n");
			if (coinPulseMsg)
				successMessage.append("Please enable coin pulse Vendor Interface Type.\n");
			form.set("successMessage", successMessage.toString());
    	
    		if (errorMessage.length() > 0) {
    			form.set("errorMessage", errorMessage.toString());    			
    			return false;
    		} else {
				request.getRequestDispatcher("mass_device_configuration_1.html.jsp").forward(request, response);
	    		return true;
    		}
    	} catch (Exception e) {
    		throw new ServletException(e);
    	}finally{
    		if (results != null)
    			results.close();
    		if (!success)
    			ProcessingUtils.rollbackDbConnection(log, conn);    		
    		ProcessingUtils.closeDbConnection(log, conn);
    	}
    }
	
	protected static void buildTwoTierContent(StringBuilder sb, String customerName, String deviceSerialCd, String enteredValue) {
		sb.append("Two-Tier Pricing has been ");
		if (StringHelper.isNumeric(enteredValue) && new BigDecimal(enteredValue).compareTo(BigDecimal.ZERO) == 1)
			sb.append("enabled");
		else
			sb.append("disabled");
		sb.append("\nCustomer: ").append(customerName);
		sb.append("\nDevice Serial #: ").append(deviceSerialCd);
		sb.append("\nNew Two-Tier Pricing: ").append(enteredValue).append("\n\n");
	}
	
	protected static void sendConvenienceFeeNotification(Connection conn, String body) throws SQLException, DataLayerException {
		ProcessingUtils.sendEmail(USALiveUtils.getUsaliveEmail(), USALiveUtils.getUsaliveEmail(), convenienceFeeNotifications, convenienceFeeNotifications, "USALive Two-Tier Pricing Changes", body, conn);
	}

	public static String getConvenienceFeeNotifications() {
		return convenienceFeeNotifications;
	}

	public static void setConvenienceFeeNotifications(String convenienceFeeNotifications) {
		MassDeviceConfigUtils.convenienceFeeNotifications = convenienceFeeNotifications;
	}
}
