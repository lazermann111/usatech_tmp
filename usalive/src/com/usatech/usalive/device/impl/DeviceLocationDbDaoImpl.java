package com.usatech.usalive.device.impl;

import com.usatech.usalive.device.api.DeviceLocationDao;
import com.usatech.usalive.device.dto.DeviceLocation;
import com.usatech.usalive.device.exception.DataAccessException;
import simple.db.DataLayerMgr;
import simple.results.Results;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import simple.io.Log;

/**
 * Receive device location from database
 */
public class DeviceLocationDbDaoImpl implements DeviceLocationDao {
    static final Log log = Log.getLog();

    @Override
    public DeviceLocation getLatestDeviceLocation(long deviceId) throws DataAccessException {
        try {
            Map<String,Object> params = new HashMap<>();
            params.put("device_id", deviceId);
            log.debug("deviceId: "+deviceId);
            Results deviceNetInfo = DataLayerMgr.executeQuery("GET_DEVICE_NET_INFO", params);
            boolean hasNetInfo = deviceNetInfo != null && deviceNetInfo.next();
            log.debug("hasNetInfo: "+hasNetInfo);
            return hasNetInfo ? convertResultsToDto(deviceId, deviceNetInfo) : null;
        } catch (Exception e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    public void scheduleAsapDeviceLocationUpdate(long deviceId) {
        try {
            Map<String,Object> params = new HashMap<>();
            params.put("device_id", deviceId);
            DataLayerMgr.executeUpdate("UPDATE_DEVICE_NET_INFO_SET_UPDATE_REQ", params, true);
        } catch (Exception e) {
            throw new DataAccessException(e);
        }
    }

    DeviceLocation convertResultsToDto(long deviceId, Results rs) {
        DeviceLocation dto = new DeviceLocation();
        dto.setDeviceId(deviceId);
        dto.setUpdatedAt((Date) rs.get("NET_UPDATED_TS"));
        dto.setCountry(rs.getFormattedValue("COUNTRY_CD"));
        dto.setState(rs.getFormattedValue("NET_STATE_CD"));
        dto.setCity(rs.getFormattedValue("NET_CITY"));
        dto.setZip(rs.getFormattedValue("NET_POSTAL_CD"));
        dto.setLatitude(rs.get("NET_LATITUDE") == null ? null : ((BigDecimal)rs.get("NET_LATITUDE")).floatValue());
        dto.setLongitude(rs.get("NET_LONGITUDE") == null ? null : ((BigDecimal)rs.get("NET_LONGITUDE")).floatValue());
        dto.setRequestedAt((Date) rs.get("NET_UPDATE_REQUESTED_TS"));
        dto.setErrorCode(rs.getFormattedValue("NET_LOCATION_ERROR_CD"));
        return dto;
    }
}
