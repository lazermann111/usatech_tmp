package com.usatech.usalive.device;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.DeviceProperty;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.constants.FileType;
import com.usatech.layers.common.device.DeviceConfigurationUtils;
import com.usatech.layers.common.device.DevicesConstants;
import com.usatech.layers.common.model.ConfigTemplateSetting;
import com.usatech.layers.common.util.DeviceUtils;
import com.usatech.layers.common.util.StringHelper;
import com.usatech.layers.common.util.WebHelper;
import com.usatech.usalive.servlet.USALiveUtils;
import com.usatech.usalive.servlet.UsaliveUser;

import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.text.StringUtils;

public class DeviceConfigUtils {
	private static final Log log = Log.getLog();
		
	protected static String convenienceFeeNotifications;
	
	
	public static boolean processDeviceConfig(InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
    	Connection conn = null;
    	boolean success = false;
    	try {
    		UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
    		String userName = user.getUserName();
    		long terminalId = form.getLong("terminalId", true, -1);
			Results results = DataLayerMgr.executeQuery("GET_TERMINAL_DETAILS", form);
	    	long deviceId;
	        int deviceTypeId;
	        String deviceSerialCd;
	        String deviceName;
	        String customerName;
	        boolean allowEdit;
	        int plv;
	    	if (results.next()){
	    		deviceId = results.getValue("deviceId", long.class);
	    		if (deviceId < 1)
	                throw new ServletException("Device not found");
	            deviceTypeId = results.getValue("deviceTypeId", int.class);
	            deviceSerialCd = results.getValue("deviceSerialCd", String.class);
	            deviceName = results.getValue("deviceName", String.class);
	            plv = results.getValue("propertyListVersion", int.class);
	            form.set("plv", plv);
	            customerName = results.getFormattedValue("customerName");
	            allowEdit = !user.isReadOnly() && "Y".equals(results.getValue("allowEdit", String.class));
	            form.set("allowEdit", allowEdit);
	    	} else
	    		throw new ServletException("Device not found");
	    	
	    	boolean isGx = deviceTypeId == DeviceType.GX.getValue() || deviceTypeId == DeviceType.G4.getValue();
	    	boolean isEdge = deviceTypeId == DeviceType.EDGE.getValue();
	    	boolean isMEI = deviceTypeId == DeviceType.MEI.getValue();
	    	boolean isCrane = WebHelper.isCraneDevice(deviceSerialCd);
	    	
	    	
	    	conn = DataLayerMgr.getConnection("OPER");
			LinkedHashMap<String, ConfigTemplateSetting> defaultSettingData;
			try {
				defaultSettingData = DeviceConfigurationUtils.getDefaultConfFileDataByDeviceTypeId(deviceTypeId, plv, null, "Y", deviceId, conn);
			} catch (Exception e) {
				throw new ServletException(e);
			}			
	    	
			LinkedHashMap<String, String> deviceSettingData = DeviceUtils.getDeviceSettingData(defaultSettingData, deviceId, deviceTypeId, true, conn);
	    	if (deviceSettingData == null)
	    		throw new ServletException("Device configuration not found for " + deviceSerialCd);
	    	
	    	StringBuilder redirect = new StringBuilder();
	    	String action = form.getString("action", false);
	    	if ("Save and Send".equalsIgnoreCase(action)) {
	    		if (!allowEdit)
	    			throw new ServletException("Not authorized");
				int changeCount = 0;
				int callInTimeChangeCount = 0;
				int callInTimeWindowChangeCount = 0;
				int serverOnlyChangeCount = 0;
				Map<String, String> errorMap = new HashMap<String, String>();
				StringBuilder newIniFileShort = new StringBuilder();
				boolean coinPulseChange = false;
	    		
	    		if (isEdge||isCrane) {
					for(Map.Entry<String, ConfigTemplateSetting> entry : defaultSettingData.entrySet()) {
	    				ConfigTemplateSetting defaultSetting = entry.getValue();
	    				if (!defaultSetting.isSupported())
	    					continue;
	    			
	    				String key = defaultSetting.getKey();
						int keyNumber = StringHelper.isInteger(key) ? Integer.valueOf(key) : -1;
						boolean storedInPennies = "Y".equalsIgnoreCase(defaultSetting.getStoredInPennies());
						String editorType = defaultSetting.getEditorType();
	    				String oldParamValue = deviceSettingData.get(key);
	    				if (oldParamValue == null)
	    					oldParamValue = "";
	    				String paramValue = form.getString(new StringBuilder("cfg_field_").append(key).toString(), false);	    				
	    				String enteredValue = paramValue;
	    				boolean isDailyExtendedSchedule=WebHelper.isDailyExtendedSchedule(defaultSetting);
	    				if (paramValue == null&&!isDailyExtendedSchedule)
	    					paramValue = oldParamValue;
						if ("SCHEDULE".equalsIgnoreCase(editorType)&&!isDailyExtendedSchedule){
							paramValue = WebHelper.buildSchedule(request, defaultSetting, key, paramValue, results.getValue("device_utc_offset_min", int.class));
						}
	    				String defaultConfigValue = defaultSetting.getConfigTemplateSettingValue();
	    				if (defaultConfigValue == null)
	    					defaultConfigValue = "";			
	    				
	    				// if a DNS host name or IP address doesn't have two dots, use the default value
						if(DeviceUtils.EDGE_COMM_SETTING_EXPRESSION.matcher(key).matches() && !DeviceUtils.TWO_DOT_EXPRESSION.matcher(paramValue).find()) {
							errorMap.put("Value validation failed for field: " + defaultSetting.getLabel(), "value: \"" + paramValue + "\"");
							continue;
	    				}

	    				String regex = defaultSetting.getRegex();
						if(!StringHelper.isBlank(regex)){
							if(isDailyExtendedSchedule){
								if(!StringUtils.isBlank(paramValue)){
									String[] paramValueList=paramValue.split(",");
									for(String paramValueListValue:paramValueList){
										if(!StringHelper.validateRegexWithMessage(errorMap, paramValueListValue, key, regex, "Value validation failed for field: " + defaultSetting.getLabel())){
											continue;
										}
									}
								}
								paramValue=WebHelper.buildScheduleDailyExtended(paramValue);
							}else if(!StringHelper.validateRegexWithMessage(errorMap, paramValue, key, regex, "Value validation failed for field: " + defaultSetting.getLabel())){
								continue;
							}
						}
	    					
	    				
						if(DeviceUtils.EDGE_SCHEDULE_EXPRESSION.matcher(key).matches() && !DeviceUtils.validateEdgeSchedule(key, paramValue, errorMap, defaultSetting.getLabel()))
	    					continue;
						
						if (storedInPennies && StringHelper.isNumeric(paramValue))
		    				paramValue = String.valueOf((new BigDecimal(paramValue).multiply(BigDecimal.valueOf(100)).intValue()));
	    					
	    				if (defaultSetting.isServerOnly()) {
	    					if (!StringHelper.equalConfigValues(paramValue, oldParamValue, storedInPennies)) {
	    						deviceSettingData.put(key, paramValue);
	    						DeviceConfigurationUtils.upsertDeviceSetting(deviceId, key, paramValue, conn, userName);
	    						changeCount++;
	    						serverOnlyChangeCount++;
	    						if (key.startsWith("CALL_IN_TIME_WINDOW_"))
	    							callInTimeWindowChangeCount++;
	    					}
	    					continue;
	    				}
	    					    				
	    				if(!deviceSettingData.containsKey(key) || !StringHelper.equalConfigValues(oldParamValue, paramValue, storedInPennies)){
	    					if (keyNumber == DeviceProperty.CONVENIENCE_FEE.getValue() && StringHelper.isNumeric(paramValue) && new BigDecimal(paramValue).compareTo(BigDecimal.valueOf(10)) == 1) {
		    					errorMap.put("New " + defaultSetting.getName() + " value cannot be greater than 0.10. Please correct.", "value: \"" + paramValue + "\"");
		    					continue;
			    			}
	    					
	    					if(StringHelper.equalConfigValues(defaultConfigValue, paramValue, storedInPennies)) {
	    						newIniFileShort.append(key).append("!\n");
	    					} else
	    						newIniFileShort.append(key).append("=").append(paramValue).append("\n");
	    					changeCount++;
	    					
	    					if ("SCHEDULE".equalsIgnoreCase(defaultSetting.getEditorType()))
	    						callInTimeChangeCount++;
	    					deviceSettingData.put(key, paramValue);
		    				DeviceConfigurationUtils.upsertDeviceSetting(deviceId, key, paramValue, null, conn, userName);
		    				
		    				if (keyNumber == DeviceProperty.CONVENIENCE_FEE.getValue() 
			    					&& ((StringHelper.isBlank(oldParamValue) || new BigDecimal(oldParamValue).compareTo(BigDecimal.ZERO) == 0)
			    					&& !StringHelper.isBlank(enteredValue) && new BigDecimal(enteredValue).compareTo(BigDecimal.ZERO) == 1
			    					|| (StringHelper.isBlank(enteredValue) || new BigDecimal(enteredValue).compareTo(BigDecimal.ZERO) == 0)
			    					&& !StringHelper.isBlank(oldParamValue) && new BigDecimal(oldParamValue).compareTo(BigDecimal.ZERO) == 1))
			    				sendConvenienceFeeNotification(conn, customerName, deviceSerialCd, enteredValue);
		    				
		    				if (DevicesConstants.EDGE_COIN_PULSE_FIELDS.contains(keyNumber) && keyNumber != DeviceProperty.VMC_INTERFACE_TYPE.getValue())
			    				coinPulseChange = true;
	    				}	    				
					}
	    		} else if (isGx || isMEI) {
		    		for (Map.Entry<String, ConfigTemplateSetting> entry : defaultSettingData.entrySet()) {
		    			ConfigTemplateSetting defaultSetting = entry.getValue();
		    			int offSet = defaultSetting.getFieldOffset();
		    			int size = defaultSetting.getFieldSize();
		    			
		    			String key           = defaultSetting.getKey();
		    			String name          = defaultSetting.getName();
		    		    String align         = defaultSetting.getAlign();
		    			String padWith       = defaultSetting.getPadChar();
		    			String dataMode      = defaultSetting.getDataMode();
						String eeromLocation = defaultSetting.getEeromLocation();
		    			String regex 		 = defaultSetting.getRegex();
		    			boolean storedInPennies = "Y".equalsIgnoreCase(defaultSetting.getStoredInPennies());
		    			
		    			if (DevicesConstants.GX_MAP_PROTECTED_FIELDS.contains(offSet) && isGx) {
		    				deviceSettingData.put(key, StringHelper.rightPad("", size * 2, '0'));
		    				DeviceConfigurationUtils.upsertDeviceSetting(deviceId, key, deviceSettingData.get(key), null, conn, userName);
		    				continue;
		    			}
		    		    
		    			String oldParamValue = deviceSettingData.get(key);
	    				if (oldParamValue == null)
	    					oldParamValue = "";
		    		    String paramValue = form.getString(new StringBuilder("cfg_field_").append(key).toString(), false);
		    		    String enteredValue = paramValue;
		    			if (paramValue == null)
		    				paramValue = oldParamValue;		    			
		    			
		    			if(!StringHelper.isBlank(paramValue) && !StringHelper.isBlank(regex)) {
		    				if (!StringHelper.validateRegexWithMessage(errorMap, paramValue, name, regex, "Value validation failed for field: " + defaultSetting.getLabel()))
		    					continue;
		    			}
		    			
		    			if (storedInPennies && StringHelper.isNumeric(paramValue))
		    				paramValue = String.valueOf((new BigDecimal(paramValue).multiply(BigDecimal.valueOf(100)).intValue()));
		    				
		    			if (defaultSetting.isServerOnly()) {
		    				if (!StringHelper.equalConfigValues(paramValue, oldParamValue, storedInPennies)) {
		    					deviceSettingData.put(key, paramValue);
		    					DeviceConfigurationUtils.upsertDeviceSetting(deviceId, key, paramValue, null, conn, userName);
		    					if (key.startsWith("CALL_IN_TIME_WINDOW_"))
		    						callInTimeWindowChangeCount++;
		    					changeCount++;				
		    				}
		    				continue;
		    			}
		    		
		    			// data changed
		    			if (!StringHelper.equalConfigValues(paramValue, oldParamValue, storedInPennies)) {
		    				if (offSet == DevicesConstants.GX_MAP_CONVENIENCE_FEE && StringHelper.isNumeric(paramValue) && new BigDecimal(paramValue).compareTo(BigDecimal.valueOf(10)) == 1) {
		    					errorMap.put("New " + name + " value cannot be greater than 0.10. Please correct.", "value: \"" + paramValue + "\"");
		    					continue;
			    			}
		    				
		    				changeCount++;
		    				
		    				if (offSet == DevicesConstants.GX_MAP_CALL_IN_TIME && isGx || offSet == DevicesConstants.MEI_MAP_CALL_IN_TIME && isMEI)
		    					callInTimeChangeCount++;
		    		
	    					// insert a Poke for this memory location
	    					int pokeLoc = 0;
	    					int pokeSize = 0;

	    					if(isGx) {
	    						// Gx uses a 2 byte per address scheme, ugh...
	    						// $eerom_location should already be adjusted
	    						pokeLoc = Integer.parseInt(eeromLocation, 16);
	    						pokeSize = size;

	    						if(pokeSize == 1) {
	    							// you must send at least 2 bytes
	    							pokeSize = 2;
	    						}
	    					} else if (isMEI) {
	    					    pokeLoc = 0;
	    						pokeSize = DevicesConstants.MEI_CONFIG_SIZE;
	    					}

	    					DeviceUtils.createPoke(conn, deviceName, pokeLoc, pokeSize, 0);
	    					
			    			if (("H".equals(dataMode) && paramValue.length() < (size * 2)) || (!"H".equals(dataMode)) && paramValue.length() < size) {
			    			    String[] paddedResult = DeviceUtils.pad(paramValue, padWith, size, align, dataMode);
			    			    paramValue = paddedResult[1];
			    			}
			    			
			    			String outStr;
			    			int outLength;
			    			if ("H".equalsIgnoreCase(dataMode)) {
			    				outStr = paramValue.toUpperCase();
			    				outLength = paramValue.length() / 2;
			    			} else { 
			    				outStr = StringHelper.encodeHexString(paramValue);
			    				outLength = paramValue.length();
			    			}
			    			if (outLength != size)
			    			    throw new Exception("Fatal Error: the length of OUT value is invalid! expecting " + size + " but got " + outLength);
			    			
			    			deviceSettingData.put(key, outStr);
			    			DeviceConfigurationUtils.upsertDeviceSetting(deviceId, key, outStr, null, conn, userName);
			    			
			    			if (offSet == DevicesConstants.GX_MAP_CONVENIENCE_FEE && isGx 
			    					&& ((StringHelper.isBlank(oldParamValue) || new BigDecimal(oldParamValue).compareTo(BigDecimal.ZERO) == 0)
			    					&& !StringHelper.isBlank(enteredValue) && new BigDecimal(enteredValue).compareTo(BigDecimal.ZERO) == 1
			    					|| (StringHelper.isBlank(enteredValue) || new BigDecimal(enteredValue).compareTo(BigDecimal.ZERO) == 0)
			    					&& !StringHelper.isBlank(oldParamValue) && new BigDecimal(oldParamValue).compareTo(BigDecimal.ZERO) == 1))
			    				sendConvenienceFeeNotification(conn, customerName, deviceSerialCd, enteredValue);
			    			
			    			if (DevicesConstants.GX_MAP_COIN_PULSE_FIELDS.contains(offSet) && offSet != DevicesConstants.GX_MAP_VENDOR_INTERFACE_TYPE)
			    				coinPulseChange = true;
		    			}
		    		}
				}

				if(errorMap.size() > 0) {
					Collection<String> errors = (Collection<String>) errorMap.keySet();
					StringBuilder sb = new StringBuilder();
					for(String error : errors)
						sb.append(error).append("\n");
					form.set("errorMessage", sb.toString());
				} else {
					if(isEdge && changeCount > serverOnlyChangeCount) {
						long newFileId = DeviceUtils.getNextFileTransferSequenceNum(conn);
						DeviceUtils.saveFile(new StringBuilder(deviceName).append("-CFG-").append(newFileId).toString(), newIniFileShort.toString(), newFileId, FileType.PROPERTY_LIST.getValue(), conn);
						DeviceUtils.sendCommand(deviceTypeId, deviceId, deviceName, newFileId, newIniFileShort.length(), DeviceUtils.DEFAULT_PACKET_SIZE, DeviceUtils.DEFAULT_EXECUTE_ORDER, conn, null, false);
					}

					if(callInTimeWindowChangeCount > 0)
						DataLayerMgr.executeCall(conn, "RESET_DEVICE_CALL_IN_TIME_NORMALIZATION", new Object[] { 1, deviceId });
					else if(callInTimeChangeCount > 0)
						DataLayerMgr.executeCall(conn, "RESET_DEVICE_CALL_IN_TIME_NORMALIZATION", new Object[] { 0, deviceId });

					redirect.append("terminal_details.i?terminalId=").append(terminalId).append("&successMessage=");
					if(changeCount > 0)
						redirect.append("Configuration+update+scheduled");
					else
						redirect.append("No+configuration+changes");
					
					if (coinPulseChange) {
						if (isGx && !"07".equalsIgnoreCase(deviceSettingData.get(String.valueOf(DevicesConstants.GX_MAP_VENDOR_INTERFACE_TYPE))))
							redirect.append(".+Please+enable+coin+pulse+Vendor+Interface+Type.");
						else if (isEdge) {
							String vmcInterfaceType = deviceSettingData.get(String.valueOf(DeviceProperty.VMC_INTERFACE_TYPE.getValue()));
							if (!"3".equalsIgnoreCase(vmcInterfaceType) && !"4".equalsIgnoreCase(vmcInterfaceType) && !"5".equalsIgnoreCase(vmcInterfaceType))
								redirect.append(".+Please+enable+coin+pulse+VMC+Interface+Type.");
						}
					}

					if(conn != null) {
						conn.commit();
						success = true;
		    		}
	    		}
	    	}
	    	
	    	if (redirect.length() == 0) {
		    	request.setAttribute("defaultSettingData", defaultSettingData);
		    	request.setAttribute("targetSettingData", deviceSettingData);		    	
		        form.set("device_id", deviceId);	        
		        form.set("device_type_id", deviceTypeId);
		        form.set("device_serial_cd", deviceSerialCd);
		        return false;
	    	} else {
	    		response.sendRedirect(redirect.toString());
	    		return true;
	    	}
    	} catch (Exception e) {
    		throw new ServletException(e);
    	}finally{
    		if (!success)
    			ProcessingUtils.rollbackDbConnection(log, conn);    		
    		ProcessingUtils.closeDbConnection(log, conn);
    	}
    }
	
	protected static void sendConvenienceFeeNotification(Connection conn, String customerName, String deviceSerialCd, String enteredValue) throws SQLException, DataLayerException {
		StringBuilder body = new StringBuilder("Two-Tier Pricing has been ");
		if (StringHelper.isNumeric(enteredValue) && new BigDecimal(enteredValue).compareTo(BigDecimal.ZERO) == 1)
			body.append("enabled");
		else
			body.append("disabled");
		body.append(" in USALive:\n");
		body.append("\nCustomer: ").append(customerName);
		body.append("\nDevice Serial #: ").append(deviceSerialCd);
		body.append("\nNew Two-Tier Pricing: ").append(enteredValue);
		ProcessingUtils.sendEmail(USALiveUtils.getUsaliveEmail(), USALiveUtils.getUsaliveEmail(), convenienceFeeNotifications, convenienceFeeNotifications, "Two-Tier Pricing Change", body.toString(), conn);
	}

	public static String getConvenienceFeeNotifications() {
		return convenienceFeeNotifications;
	}

	public static void setConvenienceFeeNotifications(String convenienceFeeNotifications) {
		DeviceConfigUtils.convenienceFeeNotifications = convenienceFeeNotifications;
	}

	
}
