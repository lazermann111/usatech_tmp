package com.usatech.usalive.web;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.servlet.InputForm;
import simple.servlet.Interceptor;

public class RMAUtils {
	private static final Log log = Log.getLog();
	public static String rmaFromEmail="customerservice@usatech.com";
	public static String rmaFromName="USA Technologies Inc";
	public static String rmaToCustomerServiceEmail="SoftwareDevelopmentTeam@usatech.com";
	public static String rmaToCustomerServiceName="USAT RMA";
	public static final java.text.SimpleDateFormat rmaDateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static final int partsMaxQuantity=9999;
	public static final String partImageSuffix=".pni";
	
	
	public static String getRmaFromEmail() {
		return rmaFromEmail;
	}

	public static void setRmaFromEmail(String rmaFromEmail) {
		RMAUtils.rmaFromEmail = rmaFromEmail;
	}

	public static String getRmaFromName() {
		return rmaFromName;
	}

	public static void setRmaFromName(String rmaFromName) {
		RMAUtils.rmaFromName = rmaFromName;
	}

	public static String getRmaToCustomerServiceEmail() {
		return rmaToCustomerServiceEmail;
	}

	public static void setRmaToCustomerServiceEmail(String rmaToCustomerServiceEmail) {
		RMAUtils.rmaToCustomerServiceEmail = rmaToCustomerServiceEmail;
	}

	public static String getRmaToCustomerServiceName() {
		return rmaToCustomerServiceName;
	}

	public static void setRmaToCustomerServiceName(String rmaToCustomerServiceName) {
		RMAUtils.rmaToCustomerServiceName = rmaToCustomerServiceName;
	}

	public static void sendEmail(HttpServletRequest request, String fromEmail, String fromName, String subject, String toEmail, String toName, String jspPage) throws ServletException, SQLException, DataLayerException {
		String body = Interceptor.intercept(request, jspPage);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("fromEmail", fromEmail);
		params.put("fromName", fromName);
		params.put("subject", subject);
		params.put("toEmail", toEmail);
		params.put("toName", toName);
		params.put("body", body);
		DataLayerMgr.executeCall((Connection)request.getAttribute("RMA_DB_CONN"),"SEND_EMAIL", params);
		log.info("Request sending RMA email to "+toEmail);
	}
	
	public static String validateStringSize(InputForm inputForm, String name, int size) throws ServletException{
		if(inputForm.get(name)!=null){
			String rmaDescription=inputForm.getString(name, false).trim();
			if(rmaDescription.length()>size){
				return name+" field exceeds the maximum allowed size of "+size;
			}else{
				inputForm.set(name, rmaDescription);
			}
		}
		return null;
	}
	
	public static String validatePartsQuantity(InputForm inputForm, String name) throws ServletException{
		if(inputForm.get(name)==null){
			return "Return part quantity field is not provided.";
		}else{
			int returnQuantity=inputForm.getInt(name, true, -1);
			if(returnQuantity<=0||returnQuantity>partsMaxQuantity){
				return "Return part quantity field exceeds the maximum allowed maximium quantity "+partsMaxQuantity;
			}
			return null;
		}
	}
	
}
