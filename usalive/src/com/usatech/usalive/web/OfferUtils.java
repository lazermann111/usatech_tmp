package com.usatech.usalive.web;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.text.StringUtils;

import com.usatech.layers.common.ProcessingUtils;
import com.usatech.report.ReportingPrivilege;
import com.usatech.usalive.servlet.USALiveUtils;
import com.usatech.usalive.servlet.UsaliveUser;

public class OfferUtils {
	public static void processOfferSignup(InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		int offerId = form.getInt("offer_id", false, 0);
		String signup = form.getStringSafely("signup", "");
		if (offerId > 0 && ("Y".equalsIgnoreCase(signup) || "N".equalsIgnoreCase(signup))) {
			String privId = form.getStringSafely("priv_id", "0");
			UsaliveUser user = (UsaliveUser) RequestUtils.getUser(request);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("userId", user.getUserId());
			params.put("offerId", offerId);
			params.put("privId", privId);
			params.put("signupInd", signup);
			try {
				DataLayerMgr.executeCall("USER_OFFER_SIGNUP", params, true);
				if ("Y".equalsIgnoreCase(signup)) {					
					if ("Y".equals(params.get("masterUserInd")) || user.hasPrivilege(ReportingPrivilege.PRIV_ADMIN_ALL_CUST_USERS)) {						
						if (!user.hasPrivilege(privId))
							user.addPrivilege(privId);
						USALiveUtils.clearLinks(form, "M");
						if (params.get("webLinkUrl") == null)
							RequestUtils.redirectWithCarryOver(request, response, "home.i");
						else {
							StringBuilder location = new StringBuilder(String.valueOf(params.get("webLinkUrl")))
								.append("?selectedMenuItem=").append(params.get("webLinkId")).append("&profileId=").append(user.getUserId())
								.append("&signup=Y");
							RequestUtils.redirectWithCarryOver(request, response, location.toString());
						}
					} else {
						String offerName = String.valueOf(params.get("offerName"));
						String publicSignupPage = ConvertUtils.getStringSafely(params.get("publicSignupPage"), "");
						String usaliveSignupPage = ConvertUtils.getStringSafely(params.get("usaliveSignupPage"), "");
						String masterUserEmail = String.valueOf(params.get("masterUserEmail"));
						StringBuilder sb = new StringBuilder();
						if (offerName.indexOf("ePort Online") > -1) {
							String baseUrl = RequestUtils.getBaseUrl(request, false);
							if (!baseUrl.endsWith("/"))
								baseUrl += "/";
							sb.append("<html><head><style>body {font-family: Arial, Verdana, Helvetica;}</style></head><body>")
							.append(StringUtils.prepareHTML(user.getFirstName())).append(" ").append(StringUtils.prepareHTML(user.getLastName())).append(" <a href=\"mailto:").append(StringUtils.prepareCDATA(user.getEmail())).append("\">").append(StringUtils.prepareHTML(user.getEmail())).append("</a>")
							.append(" recently requested access to USA Technologies' new ePort Online payment portal via USALive. USA Technologies requires approval from the USALive system administrator or an authorized agent of this company to complete this request.") 
							.append("<br/><br/><b>Please log onto USALive <a href=\"").append(baseUrl).append(usaliveSignupPage).append("\">").append(baseUrl).append(usaliveSignupPage).append("</a>")
							.append(" and complete the enrollment process for your company or complete this online registration form: <a href=\"")
							.append(baseUrl).append("public/").append(publicSignupPage).append("\">").append(baseUrl).append("public/").append(publicSignupPage).append("</a>").append("</b>")
							.append("<br/><br/><b>ePort Online</b> gives you the ability to process credit and debit transactions from any computer or mobile device.")
							.append("<ul>")
							.append("<li>Receive payment for orders immediately through an online payment portal</li>")
							.append("<li>Reduce or eliminate Invoices</li>")
							.append("<li>Manage customer accounts</li>")
							.append("<li>View all credit/debit transactions in one place through USAT's cloud based portal, USALive</li>")
							.append("<li>Set up recurring payments (coming soon)</li>")
							.append("</ul>")
							.append("After completing the online enrollment, please add any additional employees to USALive who you would like to have access to this service. There is no limit to the number of employees enrolled during this promotional period.") 
							.append("<br/><br/><b>Still have questions? Please contact your sales representative at 800-633-0340.</b>")
							.append("</body></html>");
							ProcessingUtils.sendEmail(USALiveUtils.getUsaliveEmail(), USALiveUtils.getUsaliveEmail(), masterUserEmail, masterUserEmail, "IMPORTANT: Action needed on your USALive account!", sb.toString(), null);
							
							sb.setLength(0);
							sb.append("Thanks for your interest in ePort Online!") 
							.append("<br/><br/>Unfortunately, you do not have permission to add this exciting new feature to your account. Please contact your USALive system administrator or an authorized agent of the company to complete your online enrollment. The USALive system administrator on file has also been sent an email regarding your request. ")
							.append("Enrollment can be completed by the administrator online via USALive at <a href=\"").append(baseUrl).append(usaliveSignupPage).append("\">").append(baseUrl).append(usaliveSignupPage).append("</a>, or using this online form: <a href=\"")
							.append(baseUrl).append("public/").append(publicSignupPage).append("\">").append(baseUrl).append("public/").append(publicSignupPage).append("</a>").append(".")
							.append("<br/><br/>The administrator on file for your company is: ").append(StringUtils.prepareHTML(String.valueOf(params.get("masterUserFirstName")))).append(" ").append(StringUtils.prepareHTML(String.valueOf(params.get("masterUserLastName")))).append(" ")
							.append("<a href=\"mailto:").append(StringUtils.prepareCDATA(masterUserEmail)).append("\">").append(StringUtils.prepareHTML(masterUserEmail)).append("</a>.").append(" Please feel free to forward these instructions to expedite your enrollment.")
							.append("<br/><br/><b>If you feel you have received this message in error, please contact your sales representative or customer service at 800-633-0340.</b>")
							.append("<br/><br/>Thank you!");
							request.setAttribute("message", sb.toString());
						} else
							RequestUtils.redirectWithCarryOver(request, response, "home.i");
					}
				}
				else if ("N".equalsIgnoreCase(signup))
					RequestUtils.redirectWithCarryOver(request, response, "home.i");
			} catch(SQLException e) {
				throw new ServletException(e);
			} catch(DataLayerException e) {
				throw new ServletException(e);
			} catch (IOException e) {
				throw new ServletException(e);
			}
		}
	}
}
