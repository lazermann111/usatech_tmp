package com.usatech.usalive.web;

import simple.lang.EnumIntValueLookup;
import simple.lang.InvalidIntValueException;

public enum RMAType {
	RENTRAL_FOR_CREDIT(1, "Rental For Credit"),
	DEVICE_AND_REPLACEMENT(2, "Device And Replacement"),
	PARTS_AND_REPLACEMENT(3, "Parts And Replacement"),
	ATT_2G_UPGRADE(4, "ATT 2G Upgrade"),
	ATT_2G_UPGRADE_LIKE_FOR_LIKE(5, "ATT 2G Upgrade - Like for Like"),
	ATT_2G_UPGRADE_G9(6, "ATT 2G Upgrade - Upgrade to G9 ePort"), ;

	private final int value;
	private final String description;

	private RMAType(int value, String description) {
		this.value = value;
		this.description = description;
	}

	public int getValue() {
		return value;
	}

	public String getDescription() {
		return description;
	}

	protected final static EnumIntValueLookup<RMAType> lookup = new EnumIntValueLookup<RMAType>(RMAType.class);

	public static RMAType getByValue(int value) throws InvalidIntValueException {
		return lookup.getByValue(value);
	}
}
