package com.usatech.usalive.web;

import java.math.BigDecimal;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.NotEnoughRowsException;
import simple.io.Log;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.text.StringUtils;

import com.usatech.ec.ECResponse;
import com.usatech.ec2.EC2AuthResponse;
import com.usatech.ec2.EC2ClientUtils;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.ProcessingUtils;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.constants.TranDeviceResultType;
import com.usatech.layers.common.util.DeviceUtils;
import com.usatech.layers.common.util.StringHelper;
import com.usatech.usalive.servlet.USALiveUtils;
import com.usatech.usalive.servlet.UsaliveUser;


public class OrderUtils {
	private static final Log log = Log.getLog();
	
	protected static String orderNotifications;
	protected static String orderVirtualDeviceSerialCd;
	protected static boolean orderAmountVerified;
			
	public static void processCompleteOrder(InputForm form, HttpServletRequest request, HttpServletResponse response) {
		StringBuilder responseMessage = new StringBuilder();
		UsaliveUser user = (UsaliveUser) RequestUtils.getUser(request);
		String orderNumber = null;
		String maskedCardNumber = null;
		BigDecimal amount = null;
		String currencyCd = null;
		int chargeReturnCode = ECResponse.RES_FAILED;
		try {
			amount = form.getBigDecimal("total_amount", true);
			String country = form.getString("country", true);
			currencyCd = "CA".equalsIgnoreCase(country) ? "CAD" : "USD";
			String deviceSerialCd = new StringBuilder(orderVirtualDeviceSerialCd).append("-").append(currencyCd).toString();
			if (amount.compareTo(BigDecimal.ONE) == -1 || amount.compareTo(new BigDecimal("99999.99")) == 1) {
				request.setAttribute("errorMessage", "Please enter a valid Total Amount");
				return;
			}
			long amountInPennies = amount.multiply(new BigDecimal("100")).longValue();
			String expYear = form.getString("exp_year", true);
			String expMonth = form.getString("exp_month", true);
			Calendar now = Calendar.getInstance();
			int currYear = now.get(Calendar.YEAR);
			int currMonth = now.get(Calendar.MONTH) + 1;
			if (Integer.valueOf(expYear) < currYear || Integer.valueOf(expYear) == currYear && Integer.valueOf(expMonth) < currMonth) {
				request.setAttribute("errorMessage", "Please enter a valid Expiration Date");
				return;
			}
			String postal = form.getString("postal", true);
			if ("US".equalsIgnoreCase(country))
				postal = postal.replaceAll(" |-", "");
			orderNumber = form.getString("order_number", true);
			Map<String, Object> params = new HashMap<>();
			params.put("orderNumber", orderNumber);
			try {
				DataLayerMgr.selectInto("GET_ORF_ORDER", params);
			} catch(NotEnoughRowsException e) {
				log.error("Order Number " + orderNumber + " not found", e);
				request.setAttribute("errorMessage", "Order Number " + orderNumber + " was not found. Please enter the Order Number from your invoice");
				return;
			}
			// verify open
			boolean open = ConvertUtils.getBoolean(params.get("orderOpenFlag"), false);
			if(!open) {
				log.warn("Order Number " + orderNumber + " is not open");
				request.setAttribute("errorMessage", "Order Number " + orderNumber + " has already been paid. Please double check the Order Number and contact customer service if needed.");
				return;
			}
			if(isOrderAmountVerified()) {
				// verify amount
				BigDecimal orderAmount = ConvertUtils.convertRequired(BigDecimal.class, params.get("orderAmount"));
				if(orderAmount.compareTo(amount) != 0) {
					log.warn("Amounts do not match for Order Number " + orderNumber + ". User entered " + amount + " but real amount is " + orderAmount);
					request.setAttribute("errorMessage", "The amount you entered does not match what we have on file for Order Number " + orderNumber + ". Please double check the amount and contact customer service if needed.");
					return;
				}
			}
			maskedCardNumber = MessageResponseUtils.maskCardNumber(form.getString("card_number", true));
			// Card Number|YYMM Expiration Date|Security Code|Cardholder Name|Zip Code|Street Address
			StringBuilder cardData = new StringBuilder().append(form.getString("card_number", true))
					.append("|").append(expYear.substring(2, 4)).append(expMonth)
					.append("|").append(form.getString("security_code", true))
					.append("||").append(postal)
					.append("|").append(form.getString("address1", true));
			// Format Code|Item 1 ID|Price|Quantity|Description
			StringBuilder tranDetails = new StringBuilder("A0|205|").append(amountInPennies).append("|1|").append("USALive Order|403||1|").append(orderNumber).append("|207||1|By ");
			if(user.getMasterUser() != null && !StringUtils.isBlank(user.getMasterUser().getUserName()))
				tranDetails.append(user.getMasterUser().getUserName().replace('|', ' ')).append(" for ");
			if(!StringUtils.isBlank(user.getUserName()))
				tranDetails.append(user.getUserName().replace('|', ' '));
			else
				tranDetails.append("UNKNOWN");
			log.info(new StringBuilder("Received Complete Order request, quote #: ").append(orderNumber).append(", amount: ").append(amount)
					.append(", card #: ").append(maskedCardNumber));
			long tranId = DeviceUtils.getNextMasterIdBySerial(deviceSerialCd, null);
			EC2AuthResponse chargeResponse = EC2ClientUtils.getEportConnect().chargePlain(null, null, deviceSerialCd, 
					tranId, amountInPennies, cardData.toString(), String.valueOf(EntryType.MANUAL.getValue()), 
					String.valueOf(TranDeviceResultType.SUCCESS.getValue()), tranDetails.toString(), "requireCVVMatch=true\nrequireAVSMatch=true\n");
			chargeReturnCode = chargeResponse.getReturnCode();
			if (chargeReturnCode == ECResponse.RES_APPROVED) {
				responseMessage.append("Order processed successfully");
				if (!StringHelper.isBlank(chargeResponse.getReturnMessage()))
					responseMessage.append(", response: ").append(chargeResponse.getReturnMessage());
				response.sendRedirect("/complete_order.html?successMessage=" + URLEncoder.encode(responseMessage.toString(), "UTF-8"));
			} else {
				responseMessage.append("Error processing order");
				if (!StringHelper.isBlank(chargeResponse.getReturnMessage()))
					responseMessage.append(", response: ").append(chargeResponse.getReturnMessage());
				request.setAttribute("errorMessage", responseMessage.toString());
			}
			log.info(new StringBuilder("Processed Complete Order request, quote #: ").append(orderNumber).append(", amount: ").append(amount)
					.append(", response: ").append(responseMessage.toString()).toString());
			try {
				sendOrderNotification(user, chargeReturnCode, responseMessage.toString(), orderNumber, amount, currencyCd, maskedCardNumber, deviceSerialCd, tranId);
			} catch (Exception e) {
				log.error("Error sending order notification", e);
			}
			String emailTo = form.getStringSafely("email_to", "");
			if (chargeReturnCode == ECResponse.RES_APPROVED && !StringUtils.isBlank(emailTo)) {
				try {
					sendOrderReceipt(user, emailTo, orderNumber, amount, currencyCd, maskedCardNumber, deviceSerialCd, tranId);
				} catch (Exception e) {
					log.error("Error sending order receipt", e);
				}
			}
		} catch (Exception e) {
			responseMessage.append("Unexpected error occurred");
			request.setAttribute("errorMessage", responseMessage.toString());
			log.error("Error processing order", e);
		}
	}
	
	protected static void sendOrderNotification(UsaliveUser user, int chargeReturnCode, String responseMessage, String quoteNumber, BigDecimal amount, String currencyCd, String maskedCardNumber, String deviceSerialCd, long tranId) throws SQLException, DataLayerException {
		if (StringHelper.isBlank(orderNotifications)) {
			log.warn("orderNotifications email address is not configured");
			return;
		}
		UsaliveUser masterUser = user.getMasterUser();
		String subject;
		StringBuilder body = new StringBuilder();
		if (chargeReturnCode == ECResponse.RES_APPROVED) {
			subject = "USALive Order APPROVED";
			body.append("A USALive order payment has been APPROVED.\n");
		} else if (chargeReturnCode == ECResponse.RES_DECLINED) {
			subject = "USALive Order DECLINED";
			body.append("A USALive order payment has been DECLINED.\n");
		} else {
			subject = "USALive Order FAILED";
			body.append("A USALive order payment FAILED.\n");
		}
		body.append("\nProcessing details: ").append(responseMessage);
		body.append("\nCustomer: ").append(user.getCustomerName());
		body.append("\nUsername: ").append(masterUser == null ? user.getUserName() : masterUser.getUserName());
		body.append("\nFull Name: ").append(masterUser == null ? user.getFirstName() : masterUser.getFirstName());
		body.append(" ").append(masterUser == null ? user.getLastName() : masterUser.getLastName());
		if (quoteNumber != null)
			body.append("\nQuote number: ").append(quoteNumber);
		if (amount != null)
			body.append("\nTotal amount: ").append(amount).append(" ").append(currencyCd);
		if (maskedCardNumber != null)
			body.append("\nCard number: ").append(maskedCardNumber);
		body.append("\nDevice: ").append(deviceSerialCd);
		body.append("\nTransaction ID: ").append(tranId);
		ProcessingUtils.sendEmail(USALiveUtils.getUsaliveEmail(), USALiveUtils.getUsaliveEmail(), orderNotifications, orderNotifications, subject, body.toString(), null);
	}
	
	protected static void sendOrderReceipt(UsaliveUser user, String emailTo, String quoteNumber, BigDecimal amount, String currencyCd, String maskedCardNumber, String deviceSerialCd, long tranId) throws SQLException, DataLayerException {
		java.util.Date now = new java.util.Date();
		java.text.DateFormat df = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm:ss a z");
		String nowFormatted = df.format(now);
		java.text.NumberFormat nf = new java.text.DecimalFormat("###,###,##0.00");
		UsaliveUser masterUser = user.getMasterUser();
		String subject = "USALive Order Receipt";
		StringBuilder body = new StringBuilder();
		body.append("USALive Order Receipt");
		body.append("\n\nTime: ").append(nowFormatted);
		body.append("\nCustomer: ").append(user.getCustomerName());
		body.append("\nUsername: ").append(masterUser == null ? user.getUserName() : masterUser.getUserName());
		body.append("\nFull Name: ").append(masterUser == null ? user.getFirstName() : masterUser.getFirstName());
		body.append(" ").append(masterUser == null ? user.getLastName() : masterUser.getLastName());
		if (quoteNumber != null)
			body.append("\nQuote number: ").append(quoteNumber);
		if (amount != null)
			body.append("\nTotal amount: ").append(nf.format(amount)).append(" ").append(currencyCd);
		if (maskedCardNumber != null)
			body.append("\nCard number: ").append(maskedCardNumber);
		body.append("\nDevice: ").append(deviceSerialCd);
		body.append("\nTransaction ID: ").append(tranId);
		body.append("\n\nThank You for Your Order!");
		ProcessingUtils.sendEmail(USALiveUtils.getUsaliveEmail(), USALiveUtils.getUsaliveEmail(), emailTo, emailTo, subject, body.toString(), null);
	}	

	public static String getOrderNotifications() {
		return orderNotifications;
	}

	public static void setOrderNotifications(String orderNotifications) {
		OrderUtils.orderNotifications = orderNotifications;
	}

	public static String getOrderVirtualDeviceSerialCd() {
		return orderVirtualDeviceSerialCd;
	}

	public static void setOrderVirtualDeviceSerialCd(String orderVirtualDeviceSerialCd) {
		OrderUtils.orderVirtualDeviceSerialCd = orderVirtualDeviceSerialCd;
	}

	public static boolean isOrderAmountVerified() {
		return orderAmountVerified;
	}

	public static void setOrderAmountVerified(boolean orderAmountVerified) {
		OrderUtils.orderAmountVerified = orderAmountVerified;
	}
}