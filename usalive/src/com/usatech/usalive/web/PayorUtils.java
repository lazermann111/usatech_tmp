package com.usatech.usalive.web;

import java.beans.IntrospectionException;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.BodyContent;

import org.quartz.CronExpression;

import com.caucho.hessian.client.HessianRuntimeException;
import com.usatech.app.Attribute;
import com.usatech.app.GenericAttribute;
import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.ec.ECResponse;
import com.usatech.ec2.EC2AuthResponse;
import com.usatech.ec2.EC2CardId;
import com.usatech.ec2.EC2ClientUtils;
import com.usatech.ec2.EC2TokenResponse;
import com.usatech.layers.common.MessageResponseUtils;
import com.usatech.layers.common.constants.EntryType;
import com.usatech.layers.common.constants.TranDeviceResultType;
import com.usatech.layers.common.util.BackendPayorUtils;
import com.usatech.layers.common.util.DeviceUtils;
import com.usatech.layers.common.util.WebHelper;
import com.usatech.usalive.servlet.UsaliveUser;

import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.DbUtils;
import simple.db.NotEnoughRowsException;
import simple.io.ByteArrayUtils;
import simple.io.Log;
import simple.results.BeanException;
import simple.servlet.GenerateUtils;
import simple.servlet.RequestUtils;
import simple.text.StringUtils;
import simple.text.StringUtils.Justification;
import simple.xml.sax.MessagesInputSource;


public class PayorUtils extends BackendPayorUtils {
	private static final Log log = Log.getLog();

	public static Long replacePayor(UsaliveUser user, long payorId, String payorName, String payorEmail, long bankAcctId, String cardNumber, int expMonth, int expYear, String securityCode, String billingPostal, BigDecimal recurAmount, String recurSchedule, String recurTimeZone, Date recurStartTs, Date recurEndTs, String recurDescFormat, MessagesInputSource messages) {
		try {
			Connection conn = DataLayerMgr.getConnection("report");
			Long newPayorId;
			try {
				newPayorId = addOrUpdatePayor(user, payorName, payorEmail, bankAcctId, cardNumber, expMonth, expYear, securityCode, billingPostal, payorId, recurAmount, recurSchedule, recurTimeZone, recurStartTs, recurEndTs, recurDescFormat, messages, conn);
				conn.commit();
			} finally {
				DbUtils.closeSafely(conn);
			}
			return newPayorId;
		} catch(SQLException e) {
			log.warn("Could not add payor", e);
			messages.addMessage("error", "usalive-payor-edit-system-error", "We could not process your request at this time. Please try again.");
			return null;
		} catch(DataLayerException e) {
			log.warn("Could not add payor", e);
			messages.addMessage("error", "usalive-payor-edit-system-error", "We could not process your request at this time. Please try again.");
			return null;
		}
	}

	public static Long addPayor(UsaliveUser user, String payorName, String payorEmail, long bankAcctId, String cardNumber, int expMonth, int expYear, String securityCode, String billingPostal, BigDecimal recurAmount, String recurSchedule, String recurTimeZone, Date recurStartTs, Date recurEndTs, String recurDescFormat, MessagesInputSource messages) {
		try {
			Connection conn = DataLayerMgr.getConnection("report");
			Long payorId;
			try {
				payorId = addOrUpdatePayor(user, payorName, payorEmail, bankAcctId, cardNumber, expMonth, expYear, securityCode, billingPostal, null, recurAmount, recurSchedule, recurTimeZone, recurStartTs, recurEndTs, recurDescFormat, messages, conn);
				conn.commit();
			} finally {
				DbUtils.closeSafely(conn);
			}
			return payorId;
		} catch(SQLException e) {
			log.warn("Could not add payor", e);
			messages.addMessage("error", "usalive-payor-add-system-error", "We could not process your request at this time. Please try again.");
			return null;
		} catch(DataLayerException e) {
			log.warn("Could not add payor", e);
			messages.addMessage("error", "usalive-payor-add-system-error", "We could not process your request at this time. Please try again.");
			return null;
		}
	}

	protected static Long addOrUpdatePayor(UsaliveUser user, String payorName, String payorEmail, long bankAcctId, String cardNumber, int expMonth, int expYear, String securityCode, String billingPostal, Long payorId, BigDecimal recurAmount, String recurSchedule, String recurTimeZone, Date recurStartTs, Date recurEndTs, String recurDescFormat, MessagesInputSource messages, Connection conn) {
		String type;
		String callId;
		if(payorId == null) {
			type = "add";
			callId = "ADD_PAYOR";
		} else {
			type = "edit";
			callId = "EDIT_PAYOR_NEW_CARD";
		}
		String maskedCardNumber = MessageResponseUtils.maskCardNumber(cardNumber);
		int currYear = GenerateUtils.getCurrentYear();
		if(expYear < currYear) {
			messages.addMessage("warn", "usalive-payor-" + type + "-card-expired", "The card {1} has already expired. Please use a different card.", maskedCardNumber);
			return null;
		}
		if(expYear > currYear + 100) {
			messages.addMessage("warn", "usalive-payor-" + type + "-card-expiration-invalid", "The card {1} has an expiration date too far into the future. Please use a different card.", maskedCardNumber);
			return null;
		}
		if(expMonth < 1 || expMonth > 12) {
			messages.addMessage("warn", "usalive-payor-" + type + "-card-expiration-invalid-month", "The card {1} has an invalid month. Please use a different card.", maskedCardNumber);
			return null;
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("bankAcctId", bankAcctId);
		if(recurSchedule != null && !(recurSchedule = recurSchedule.trim()).isEmpty()) {
			CronExpression cron;
			try {
				cron = new CronExpression(recurSchedule);
			} catch(ParseException e) {
				log.warn("Invalid Cron Expression '" + recurSchedule + "'", e);
				messages.addMessage("warn", "usalive-payor-" + type + "-schedule-invalid", "The recurring schedule is not valid. Please select the schedule again.", recurSchedule);
				return null;
			}
			recurSchedule = cron.toString();
			if(recurTimeZone == null)
				recurTimeZone = TimeZone.getDefault().getID();
			else
				cron.setTimeZone(TimeZone.getTimeZone(recurTimeZone));
			if(recurStartTs == null)
				recurStartTs = new Date();
			Date recurNextTs = cron.getTimeAfter(recurStartTs);
			params.put("recurSchedule", recurSchedule);
			params.put("recurNextTs", recurNextTs);
			params.put("recurAmount", recurAmount);
			params.put("recurTimeZone", recurTimeZone);
			params.put("recurStartTs", recurStartTs);
			params.put("recurEndTs", recurEndTs);
			params.put("recurDescFormat", recurDescFormat);
		}

		boolean okay = false;
		try {
			DataLayerMgr.executeCall(conn, "GET_OR_CREATE_BACKOFFICE_DEVICE", params);
			okay = true;
			conn.commit();
			String deviceSerialCd = ConvertUtils.getString(params.get("deviceSerialCd"), true);
			long deviceTranCd = DeviceUtils.getNextMasterIdBySerial(deviceSerialCd, null);
			String expDate = StringUtils.pad(String.valueOf((expYear % 100) * 100 + expMonth), '0', 4, Justification.RIGHT);
			log.info("Sending EC2 tokenizePlain request with deviceSerialCd=" + deviceSerialCd + "; deviceTranCd=" + deviceTranCd);
			EC2TokenResponse response = EC2ClientUtils.getEportConnect().tokenizePlain(null, null, deviceSerialCd, deviceTranCd, cardNumber, expDate, securityCode, null, billingPostal, null, initialAttributes);
			try {
				log.info("Received EC2 response: " + StringUtils.toStringProperties(response, SENSITIVE_PROPERTIES));
			} catch(IntrospectionException e) {
				log.warn("Could not read properties from EC2 response", e);
			}
			switch(response.getReturnCode()) {
				case ECResponse.RES_APPROVED:
				case ECResponse.RES_CVV_MISMATCH:
				case ECResponse.RES_AVS_MISMATCH:
				case ECResponse.RES_CVV_AND_AVS_MISMATCH:
					params.put("payorId", payorId);
					params.put("payorName", payorName);
					params.put("payorEmail", payorEmail);
					params.put("userId", user.getUserId());
					params.put("cardId", response.getCardId());
					params.put("maskedCardNumber", maskedCardNumber);
					params.put("token", ByteArrayUtils.fromHex(response.getTokenHex()));
					params.put("consumerId", response.getConsumerId());
					okay = false;
					try {
						DataLayerMgr.executeCall(conn, callId, params);
					} catch(SQLException e) {
						switch(e.getErrorCode()) {
							case 100:
							case 1:
								log.info("Cannot add payor because it would create a duplicate");
								messages.addMessage("warn", "usalive-payor-add-already-exists", "An Account with {1} card {2} has already been added", payorName, response.getCardType(), maskedCardNumber);
								return null;
							default:
								throw e;
						}
					}
					okay = true;
					if(payorId == null)
						payorId = ConvertUtils.getLong(params.get("payorId"));
					messages.addMessage("success", "usalive-payor-" + type + "-success", "Account {0} has been " + ("add".equalsIgnoreCase(type) ? "created" : "once".equalsIgnoreCase(type) ? "verified" : "updated") + " for {1} card {2}. Details: {3}.", payorName, response.getCardType(), maskedCardNumber, response.getReturnMessage());
					if(recurAmount != null && !StringUtils.isBlank(recurSchedule))
						notifyRecurChecker(payorId);
					return payorId;
				case ECResponse.RES_DECLINED:
					messages.addMessage("warn", "usalive-payor-" + type + "-declined", "The {1} card {2} was declined. Please double-check all fields or use a different card.", payorName, response.getCardType(), maskedCardNumber, response.getReturnMessage());
					return null;
				case ECResponse.RES_RESTRICTED_DEBIT_CARD_TYPE:
					messages.addMessage("warn", "usalive-payor-" + type + "-declined-debit", "The {1} card {2} was declined because debit cards are not accepted. Please use a different card.", payorName, response.getCardType(), maskedCardNumber, response.getReturnMessage());
					return null;
				case ECResponse.RES_RESTRICTED_PAYMENT_METHOD:
					messages.addMessage("warn", "usalive-payor-" + type + "-invalid-card", "The {1} card {2} is not a valid card. Please use a different card.", payorName, response.getCardType(), maskedCardNumber, response.getReturnMessage());
					return null;
				default:
					messages.addMessage("error", "usalive-payor-" + type + "-failed", "Failed while processing this request. Sorry for the inconvenience. Please try again.", payorName, response.getCardType(), maskedCardNumber, response.getReturnMessage());
					return null;
			}
		} catch(SQLException e) {
			log.warn("Could not add/edit payor", e);
			messages.addMessage("error", "usalive-payor-" + type + "-system-error", "We could not process your request at this time. Please try again.");
			return null;
		} catch(DataLayerException e) {
			log.warn("Could not add/edit payor", e);
			messages.addMessage("error", "usalive-payor-" + type + "-system-error", "We could not process your request at this time. Please try again.");
			return null;
		} catch(ConvertException e) {
			log.warn("Could not add/edit payor", e);
			messages.addMessage("error", "usalive-payor-" + type + "-system-error", "We could not process your request at this time. Please try again.");
			return null;
		} catch(HessianRuntimeException e) {
			log.warn("Could not add/edit payor", e);
			messages.addMessage("error", "usalive-payor-" + type + "-system-error", "We could not process your request at this time. Please try again.");
			return null;
		} finally {
			if(!okay)
				DbUtils.rollbackSafely(conn);
		}
	}

	protected static final Attribute ATTR_PAYOR_ID = new GenericAttribute("payorId");

	protected static void notifyRecurChecker(long payorId) {
		MessageChain mc = new MessageChainV11();
		MessageChainStep step = mc.addStep("usat.recur.charge.notify", true);
		step.setTemporary(true);
		step.setAttribute(ATTR_PAYOR_ID, payorId);
		try {
			MessageChainService.publish(mc, WebHelper.getDefaultPublisher());
		} catch(ServiceException e) {
			log.warn("Could not notify Recur Checker", e);
		}
	}
	public static void removePayor(UsaliveUser user, long payorId, MessagesInputSource messages) {
		try {
			Connection conn = DataLayerMgr.getConnection("report");
			try {
				String payorName = removePayor(user, payorId, conn);
				conn.commit();
				messages.addMessage("success", "usalive-payor-remove-success", "Removed Account {0}", payorName);
			} finally {
				DbUtils.closeSafely(conn);
			}
		} catch(SQLException e) {
			log.warn("Could not remove payor", e);
			messages.addMessage("error", "usalive-payor-remove-system-error", "We could not process your request at this time. Please try again.");
		} catch(DataLayerException e) {
			log.warn("Could not remove payor", e);
			messages.addMessage("error", "usalive-payor-remove-system-error", "We could not process your request at this time. Please try again.");
		}
	}

	protected static String removePayor(UsaliveUser user, long payorId, Connection conn) throws SQLException, DataLayerException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("payorId", payorId);
		params.put("userId", user.getUserId());
		DataLayerMgr.executeCall(conn, "REMOVE_PAYOR", params);
		return ConvertUtils.getStringSafely(params.get("payorName"));
	}

	public static boolean editPayor(UsaliveUser user, long payorId, String payorName, String payorEmail, long bankAcctId, BigDecimal recurAmount, String recurSchedule, String recurTimeZone, Date recurStartTs, Date recurEndTs, String recurDescFormat, MessagesInputSource messages) {
		return editPayor(user, payorId, true, payorName, payorEmail, bankAcctId, recurAmount, recurSchedule, recurTimeZone, recurStartTs, recurEndTs, recurDescFormat, messages);
	}

	public static boolean editPayorRecur(UsaliveUser user, long payorId, String payorEmail, BigDecimal recurAmount, String recurSchedule, String recurTimeZone, Date recurStartTs, Date recurEndTs, String recurDescFormat, MessagesInputSource messages) {
		return editPayor(user, payorId, false, null, payorEmail, 0L, recurAmount, recurSchedule, recurTimeZone, recurStartTs, recurEndTs, recurDescFormat, messages);
	}

	protected static boolean editPayor(UsaliveUser user, long payorId, boolean fullUpdate, String payorName, String payorEmail, long bankAcctId, BigDecimal recurAmount, String recurSchedule, String recurTimeZone, Date recurStartTs, Date recurEndTs, String recurDescFormat, MessagesInputSource messages) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("payorId", payorId);
		if(fullUpdate) {
			params.put("payorName", payorName);
			params.put("bankAcctId", bankAcctId);
			params.put("full", 'Y');
		} else
			params.put("full", 'N');

		params.put("payorEmail", payorEmail);
		if(recurSchedule != null && !(recurSchedule = recurSchedule.trim()).isEmpty()) {
			CronExpression cron;
			try {
				cron = new CronExpression(recurSchedule);
			} catch(ParseException e) {
				log.warn("Invalid Cron Expression '" + recurSchedule + "'", e);
				messages.addMessage("warn", "usalive-payor-edit-schedule-invalid", "The recurring schedule is not valid. Please select the schedule again.", recurSchedule);
				return false;
			}
			recurSchedule = cron.toString();
			if(recurTimeZone == null)
				recurTimeZone = TimeZone.getDefault().getID();
			else
				cron.setTimeZone(TimeZone.getTimeZone(recurTimeZone));
			if(recurStartTs == null)
				recurStartTs = new Date();
			Date recurNextTs = cron.getTimeAfter(recurStartTs);
			params.put("recurSchedule", recurSchedule);
			params.put("recurNextTs", recurNextTs);
			params.put("recurAmount", recurAmount);
			params.put("recurTimeZone", recurTimeZone);
			params.put("recurStartTs", recurStartTs);
			params.put("recurEndTs", recurEndTs);
			params.put("recurDescFormat", recurDescFormat);
		}
		params.put("userId", user.getUserId());
		try {
			if(fullUpdate)
				DataLayerMgr.executeCall("GET_OR_CREATE_BACKOFFICE_DEVICE", params, true);
			DataLayerMgr.executeCall("EDIT_PAYOR", params, true);
			if(!fullUpdate)
				payorName = ConvertUtils.getStringSafely(params.get("payorName"), payorName);
			messages.addMessage("success", "usalive-payor-edit-success", "Updated Account {0}", payorName);
			if(recurAmount != null && !StringUtils.isBlank(recurSchedule))
				notifyRecurChecker(payorId);

			return true;
		} catch(SQLException e) {
			switch(e.getErrorCode()) {
				case 100:
				case 1:
					log.info("Cannot change payor's bank account because it would create a duplicate");
					messages.addMessage("warn", "usalive-payor-edit-already-exists", "An Account with this card for this bank account already exists", payorName);				
					return false;
				default:
					log.warn("Could not edit payor", e);
					messages.addMessage("error", "usalive-payor-edit-system-error", "We could not process your request at this time. Please try again.");
					return false;
			}
		} catch(DataLayerException e) {
			log.warn("Could not edit payor", e);
			messages.addMessage("error", "usalive-payor-edit-system-error", "We could not process your request at this time. Please try again.");
			return false;
		}
	}

	public static boolean chargePayor(UsaliveUser user, long payorId, String payorEmail, BigDecimal chargeAmount, String chargeReference, String chargeDesc, long lastChargeTime, boolean sendReceipt, HttpServletRequest request) throws ConvertException {
		return chargePayor(user, payorId, payorEmail, chargeAmount, chargeReference, chargeDesc, lastChargeTime, sendReceipt, RequestUtils.getLocale(request), RequestUtils.getBaseUrl(request, false), RequestUtils.getOrCreateMessagesSource(request));
	}

	public static boolean chargePayor(UsaliveUser user, long payorId, String payorEmail, BigDecimal chargeAmount, String chargeReference, String chargeDesc, long lastChargeTime, boolean sendReceipt, Locale locale, String baseUrl, MessagesInputSource messages) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("payorId", payorId);
		params.put("lastChargeTime", lastChargeTime);
		params.put("simple.servlet.ServletUser", user);
		try {
			boolean okay = false;
			Connection conn = DataLayerMgr.getConnection("REPORT");
			try {
				try {
					DataLayerMgr.executeCall(conn, "BEGIN_PAYOR_CHARGE", params); // get device serial cd, device_id, cardId, token, payor name, card #, timezone
				} catch(NotEnoughRowsException e) {
					messages.addMessage("warn", "usalive-payor-not-found", "You no longer have permission to charge this payer. Please select a different payor.");
					return false;
				} catch(SQLException e) {
					if("02000".equals(e.getSQLState())) {
						messages.addMessage("warn", "usalive-payor-not-found", "You no longer have permission to charge this payer. Please select a different payor.");
					} else {
						log.warn("Could not charge payor", e);
						messages.addMessage("error", "usalive-payor-add-system-error", "We could not process your request at this time. Please try again.");
					}
					return false;
				}
				long savedLastChargeTime = ConvertUtils.getLong(params.get("lastChargeTs"), 0L);
				if(savedLastChargeTime != lastChargeTime) {
					messages.addMessage("warn", "usalive-payor-charge-in-progress", "This payor has been charged since you refreshed the page. Please check the last charged time and try again if you still want to charge them.");
					return false;
				}
				byte[] token = ByteArrayUtils.fromHex(ConvertUtils.getString(params.get("token"), false));
				if(token == null || token.length == 0) {
					messages.addMessage("warn", "usalive-payor-not-tokenized", "This payor's credentials are not saved. Please edit the payor and add their card info again.");
					return false;
				}
				String deviceSerialCd = ConvertUtils.getString(params.get("deviceSerialCd"), true);
				long deviceTranCd = DeviceUtils.getNextMasterIdBySerial(deviceSerialCd, conn);
				long cardId = ConvertUtils.getLong(params.get("cardId"));
				String payorName = ConvertUtils.getString(params.get("payorName"), true);
				String cardNumber = ConvertUtils.getString(params.get("cardNumber"), true);
				String timeZoneGuid = ConvertUtils.getString(params.get("timeZoneGuid"), true);
				String doingBusinessAs = ConvertUtils.getString(params.get("doingBusinessAs"), true);

				String userName;
				if(user.getMasterUser() != null && !StringUtils.isBlank(user.getMasterUser().getUserName()))
					userName = user.getMasterUser().getUserName();
				else
					userName = user.getUserName();
				String oldPayorEmail = ConvertUtils.getString(params.get("payorEmail"), false);
				if(payorEmail != null && !(payorEmail = payorEmail.trim()).isEmpty() && (oldPayorEmail == null || !(oldPayorEmail = oldPayorEmail.trim()).equalsIgnoreCase(payorEmail))) {
					// Update email
					params.put("payorEmail", payorEmail);
					try {
						DataLayerMgr.executeUpdate(conn, "UPDATE_PAYOR_EMAIL", params);
					} catch(SQLException e) {
						log.warn("Could not update payor email; continuing with charge", e);
					}
				}
				EC2AuthResponse response = chargePayor(payorId, deviceSerialCd, deviceTranCd, cardId, token, userName, doingBusinessAs, chargeAmount, chargeReference, null, chargeDesc);
				String maskedCardNumber = MessageResponseUtils.maskCardNumber(cardNumber);
				switch(response.getReturnCode()) {
					case ECResponse.RES_APPROVED:
					case ECResponse.RES_CVV_MISMATCH:
					case ECResponse.RES_AVS_MISMATCH:
					case ECResponse.RES_CVV_AND_AVS_MISMATCH:
						params.put("chargeAmount", chargeAmount);
						long chargeTime = ConvertUtils.getLocalTime(ConvertUtils.getMillisUTC(System.currentTimeMillis()), timeZoneGuid);
						params.put("chargeTs", chargeTime);
						DataLayerMgr.executeCall(conn, "UPDATE_PAYOR_LAST_CHARGED", params);
						okay = true;
						conn.commit();
						messages.addMessage("success", "usalive-payor-charge-success", "Charged Account {0} on {1} card {2} for {3,CURRENCY}. Details: {4}. (It may take up to a minute or two to appear in transaction history)", payorName, response.getCardType(), maskedCardNumber, chargeAmount, response.getReturnMessage());
						if(sendReceipt && !StringUtils.isBlank(payorEmail)) {
							String address = ConvertUtils.getString(params.get("address"), false);
							String city = ConvertUtils.getString(params.get("city"), false);
							String stateCd = ConvertUtils.getString(params.get("stateCd"), false);
							String postalCd = ConvertUtils.getString(params.get("postalCd"), false);
							String countryCd = ConvertUtils.getString(params.get("countryCd"), false);
							String phone = ConvertUtils.getString(params.get("phone"), false);
							String email = ConvertUtils.getString(params.get("email"), false);
							sendReceipt(user, payorName, payorEmail, baseUrl, deviceTranCd, deviceSerialCd, maskedCardNumber, response.getCardType(), chargeTime, chargeAmount, chargeDesc, null, chargeReference, doingBusinessAs, address, city, stateCd, postalCd, countryCd, phone, email, locale, messages);
						}
						return true;
					case ECResponse.RES_DECLINED:
						messages.addMessage("warn", "usalive-payor-charge-declined", "Could not charge Account {0}: the {1} card {2} was declined. Please use a different card. Details: {3}.", payorName, response.getCardType(), maskedCardNumber, response.getReturnMessage());
						break;
					case ECResponse.RES_RESTRICTED_DEBIT_CARD_TYPE:
						messages.addMessage("warn", "usalive-payor-charge-declined-debit", "Could not charge Account {0}: the {1} card {2} was declined because debit cards are not accepted. Please use a different card. Details: {3}.", payorName, response.getCardType(), maskedCardNumber, response.getReturnMessage());
						break;
					case ECResponse.RES_RESTRICTED_PAYMENT_METHOD:
						messages.addMessage("warn", "usalive-payor-invalid-card", "Could not charge Account {0}: the {1} card {2} is not a valid card. Please use a different card. Details: {3}.", payorName, response.getCardType(), maskedCardNumber, response.getReturnMessage());
						break;
					default:
						messages.addMessage("error", "usalive-payor-charge-failed", "Failed while processing this request. Sorry for the inconvenience. Please try again. Details: {3}.", payorName, response.getCardType(), maskedCardNumber, response.getReturnMessage());
						break;
				}
				okay = true;
			} finally {
				if(!okay)
					DbUtils.rollbackSafely(conn);
				conn.close();
			}
		} catch(SQLException e) {
			log.warn("Could not charge payor", e);
			messages.addMessage("error", "usalive-payor-add-system-error", "We could not process your request at this time. Please try again.");
		} catch(DataLayerException e) {
			log.warn("Could not charge payor", e);
			messages.addMessage("error", "usalive-payor-add-system-error", "We could not process your request at this time. Please try again.");
		} catch(ConvertException e) {
			log.warn("Could not charge payor", e);
			messages.addMessage("error", "usalive-payor-add-system-error", "We could not process your request at this time. Please try again.");
		} catch(HessianRuntimeException e) {
			log.warn("Could not charge payor", e);
			messages.addMessage("error", "usalive-payor-add-system-error", "We could not process your request at this time. Please try again.");
		}
		return false;
	}
	public static Long oneTimeChargePayor(UsaliveUser user, String payorName, String payorEmail, long bankAcctId, String cardNumber, int expMonth, int expYear, String securityCode, String billingPostal, BigDecimal chargeAmount, String chargeReference, String chargeDesc, boolean sendReceipt, HttpServletRequest request) throws ConvertException {
		return oneTimeChargePayor(user, payorName, payorEmail, bankAcctId, cardNumber, expMonth, expYear, securityCode, billingPostal, chargeAmount, chargeReference, chargeDesc, sendReceipt, RequestUtils.getLocale(request), RequestUtils.getBaseUrl(request, false), RequestUtils.getOrCreateMessagesSource(request));
	}

	protected static Long oneTimeChargePayor(UsaliveUser user, String payorName, String payorEmail, long bankAcctId, String cardNumber, int expMonth, int expYear, String securityCode, String billingPostal, BigDecimal chargeAmount, String chargeReference, String chargeDesc, boolean sendReceipt, Locale locale, String baseUrl, MessagesInputSource messages) {
		String maskedCardNumber = MessageResponseUtils.maskCardNumber(cardNumber);
		Map<String, Object> params = new HashMap<String, Object>();
		try {
			int currYear = GenerateUtils.getCurrentYear();
			if(expYear < currYear) {
				messages.addMessage("warn", "usalive-payor-once-card-expired", "The card {1} has already expired. Please use a different card.", maskedCardNumber);
				return null;
			}
			if(expYear > currYear + 100) {
				messages.addMessage("warn", "usalive-payor-once-card-expiration-invalid", "The card {1} has an expiration date too far into the future. Please use a different card.", maskedCardNumber);
				return null;
			}
			if(expMonth < 1 || expMonth > 12) {
				messages.addMessage("warn", "usalive-payor-once-card-expiration-invalid-month", "The card {1} has an invalid month. Please use a different card.", maskedCardNumber);
				return null;
			}
			Connection conn = DataLayerMgr.getConnection("report");
			boolean okay = false;
			try {
				params.put("bankAcctId", bankAcctId);
				try {
					DataLayerMgr.executeCall(conn, "GET_OR_CREATE_BACKOFFICE_DEVICE", params);
					okay = true;
					conn.commit();
					String deviceSerialCd = ConvertUtils.getString(params.get("deviceSerialCd"), true);
					String expDate = StringUtils.pad(String.valueOf((expYear % 100) * 100 + expMonth), '0', 4, Justification.RIGHT);
					StringBuilder sb = new StringBuilder();
					sb.append(cardNumber).append('|');
					sb.append(expDate).append('|'); // expDate - YYMM
					sb.append(securityCode).append('|');// cvv
					sb.append("").append('|'); // name
					sb.append(billingPostal).append('|');// postal
					sb.append(""); // address
					String cardData = sb.toString();
					EC2CardId cardIdResponse = EC2ClientUtils.getEportConnect().getCardIdPlain(null, null, deviceSerialCd, cardData.toString(), String.valueOf(EntryType.MANUAL.getValue()), initialAttributes);
					try {
						log.info("Received EC2 response: " + StringUtils.toStringProperties(cardIdResponse, SENSITIVE_PROPERTIES));
					} catch(IntrospectionException e) {
						log.warn("Could not read properties from EC2 response", e);
					}
					params.put("payorName", payorName);
					params.put("payorEmail", payorEmail);
					params.put("userId", user.getUserId());
					long cardId= cardIdResponse.getCardId();
					params.put("cardId",cardId);
					params.put("maskedCardNumber", maskedCardNumber);

					okay = false;
					try {
						DataLayerMgr.executeCall(conn, "ADD_PAYOR_BEFORE_CHARGE", params);
					} catch(SQLException e) {
						switch(e.getErrorCode()) {
							case 100:
							case 1:
								log.info("Cannot add payor because it would create a duplicate");
								messages.addMessage("warn", "usalive-payor-add-already-exists", "An Account with this card for this bank account already exists", payorName);
								return null;
							default:
								throw e;
						}
					}
					okay = true;
					conn.commit();
					long payorId = ConvertUtils.getLong(params.get("payorId"));

					long deviceTranCd = DeviceUtils.getNextMasterIdBySerial(deviceSerialCd, null);
					DataLayerMgr.selectInto(conn, "GET_DEVICE_INFO_FOR_CHARGE", params);
					String timeZoneGuid = ConvertUtils.getString(params.get("timeZoneGuid"), true);
					String doingBusinessAs = ConvertUtils.getString(params.get("doingBusinessAs"), true);

					// <card number>|<card expiration date>|<card security code>|<card holder name>|<card holder zip code>|<card holder address>
					long amountInPennies = chargeAmount.movePointRight(2).longValue();
					if(!StringUtils.isBlank(chargeDesc))
						chargeDesc = chargeDesc.replace('|', '/');
					else if(!StringUtils.isBlank(doingBusinessAs))
						chargeDesc = "Online Charge By " + doingBusinessAs.replace('|', '/');
					else
						chargeDesc = "Online Charge";
					StringBuilder tranDetails = new StringBuilder("A0|205|").append(amountInPennies).append("|1|").append(chargeDesc);
					if(!StringUtils.isBlank(chargeReference))
						tranDetails.append("|403||1|").append(chargeReference.replace('|', ' '));
					tranDetails.append("|207||1|By ").append(user.getMasterUser() != null ? user.getMasterUser().getUserName() : user.getUserName());
					log.info(new StringBuilder("Charging card ").append(maskedCardNumber).append(" for ").append(chargeAmount));
					EC2AuthResponse response = EC2ClientUtils.getEportConnect().chargePlain(null, null, deviceSerialCd, deviceTranCd, amountInPennies, cardData.toString(), String.valueOf(EntryType.MANUAL.getValue()), String.valueOf(TranDeviceResultType.SUCCESS.getValue()), tranDetails.toString(), initialAttributes);
					try {
						log.info("Received EC2 response: " + StringUtils.toStringProperties(response, SENSITIVE_PROPERTIES));
					} catch(IntrospectionException e) {
						log.warn("Could not read properties from EC2 response", e);
					}
					switch(response.getReturnCode()) {
						case ECResponse.RES_APPROVED:
						case ECResponse.RES_CVV_MISMATCH:
						case ECResponse.RES_AVS_MISMATCH:
						case ECResponse.RES_CVV_AND_AVS_MISMATCH:
							long chargedCardId=response.getCardId();
							params.put("consumerId", response.getConsumerId());
							params.put("chargeAmount", chargeAmount);
							long chargeTime = ConvertUtils.getLocalTime(ConvertUtils.getMillisUTC(System.currentTimeMillis()), timeZoneGuid);
							params.put("chargeTs", chargeTime);

							okay = false;
							DataLayerMgr.executeCall(conn, "UPDATE_PAYOR_LAST_CHARGED", params);
							if(cardId==chargedCardId){
								params.put("cardId", chargedCardId);
								DataLayerMgr.executeCall(conn, "UPDATE_PAYOR_CARD_ID", params);
								log.info("Update Payor old globalAccountId="+cardId+" new globalAccountId="+chargedCardId);
							}
							okay = true;
							conn.commit();
							messages.addMessage("success", "usalive-payor-once-success", "Account {0} on {1} card {2} has been charged for {3,CURRENCY}. Details: {4}. (It may take up to a minute or two to appear in transaction history)", payorName, response.getCardType(), maskedCardNumber, chargeAmount, response.getReturnMessage());
							if(sendReceipt && !StringUtils.isBlank(payorEmail)) {
								String address = ConvertUtils.getString(params.get("address"), false);
								String city = ConvertUtils.getString(params.get("city"), false);
								String stateCd = ConvertUtils.getString(params.get("stateCd"), false);
								String postalCd = ConvertUtils.getString(params.get("postalCd"), false);
								String countryCd = ConvertUtils.getString(params.get("countryCd"), false);
								String phone = ConvertUtils.getString(params.get("phone"), false);
								String email = ConvertUtils.getString(params.get("email"), false);
								sendReceipt(user, payorName, payorEmail, baseUrl, deviceTranCd, deviceSerialCd, maskedCardNumber, response.getCardType(), chargeTime, chargeAmount, chargeDesc, null, chargeReference, doingBusinessAs, address, city, stateCd, postalCd, countryCd, phone, email, locale, messages);
							}
							return payorId;
						case ECResponse.RES_DECLINED:
							messages.addMessage("warn", "usalive-payor-once-declined", "The {1} card {2} was declined. Please use a different card. Details: {3}.", payorName, response.getCardType(), maskedCardNumber, response.getReturnMessage());
							return null;
						case ECResponse.RES_RESTRICTED_DEBIT_CARD_TYPE:
							messages.addMessage("warn", "usalive-payor-once-declined-debit", "The {1} card {2} was declined because debit cards are not accepted. Please use a different card. Details: {3}.", payorName, response.getCardType(), maskedCardNumber, response.getReturnMessage());
							return null;
						case ECResponse.RES_RESTRICTED_PAYMENT_METHOD:
							messages.addMessage("warn", "usalive-payor-once-invalid-card", "The {1} card {2} is not a valid card. Please use a different card. Details: {3}.", payorName, response.getCardType(), maskedCardNumber, response.getReturnMessage());
							return null;
						default:
							messages.addMessage("error", "usalive-payor-once-failed", "Failed while processing this request. Sorry for the inconvenience. Please try again. Details: {3}.", payorName, response.getCardType(), maskedCardNumber, response.getReturnMessage());
							return null;
					}
				} catch(SQLException e) {
					log.warn("Could not add payor", e);
					messages.addMessage("error", "usalive-payor-once-system-error", "We could not process your request at this time. Please try again.");
					return null;
				} catch(DataLayerException e) {
					log.warn("Could not add payor", e);
					messages.addMessage("error", "usalive-payor-once-system-error", "We could not process your request at this time. Please try again.");
					return null;
				} catch(ConvertException e) {
					log.warn("Could not add payor", e);
					messages.addMessage("error", "usalive-payor-once-system-error", "We could not process your request at this time. Please try again.");
					return null;
				} catch(HessianRuntimeException e) {
					log.warn("Could not add payor", e);
					messages.addMessage("error", "usalive-payor-once-system-error", "We could not process your request at this time. Please try again.");
					return null;
				} catch(BeanException e) {
					log.warn("Could not add payor", e);
					messages.addMessage("error", "usalive-payor-once-system-error", "We could not process your request at this time. Please try again.");
					return null;
				}
			} finally {
				if(!okay)
					DbUtils.rollbackSafely(conn);
				try {
					conn.close();
				} catch(SQLException e) {
					// ignore
				}
			}
		} catch(SQLException e) {
			log.warn("Could not charge payor", e);
			messages.addMessage("error", "usalive-payor-once-system-error", "We could not process your request at this time. Please try again.");
		} catch(DataLayerException e) {
			log.warn("Could not charge payor", e);
			messages.addMessage("error", "usalive-payor-once-system-error", "We could not process your request at this time. Please try again.");
		} catch(HessianRuntimeException e) {
			log.warn("Could not charge payor", e);
			messages.addMessage("error", "usalive-payor-once-system-error", "We could not process your request at this time. Please try again.");
		}
		return null;
	}

	public static void sendReceipt(UsaliveUser user, String payorName, String payorEmail, String baseUrl, long deviceTranCd, String deviceSerialCd, String maskedCardNumber, String cardType, long chargeTime, BigDecimal chargeAmount, String chargeDesc, Date invoiceDate, String chargeReference, String doingBusinessAs, String address, String city, String stateCd, String postalCd, String countryCd,
			String phone, String email, Locale locale, MessagesInputSource messages) {
		try {
			sendReceipt(payorName, payorEmail, baseUrl, deviceTranCd, deviceSerialCd, maskedCardNumber, cardType, chargeTime, chargeAmount, chargeDesc, invoiceDate, chargeReference, doingBusinessAs, address, city, stateCd, postalCd, countryCd, phone, email, locale);
			messages.addMessage("success", "usalive-payor-charge-receipt-success", "Sent a receipt of this charge to Account {0} at {1}", payorName, payorEmail);
		} catch(ServletException e) {
			log.warn("Could not send email receipt to " + payorEmail, e);
			messages.addMessage("warn", "usalive-payor-charge-email-failure", "Could not send a receipt to Account {0} at {1} for this charge. We apologize for any inconvenience.", payorName, payorEmail);
		} catch(SQLException e) {
			log.warn("Could not send email receipt to " + payorEmail, e);
			messages.addMessage("warn", "usalive-payor-charge-email-failure", "Could not send a receipt to Account {0} at {1} for this charge. We apologize for any inconvenience.", payorName, payorEmail);
		} catch(DataLayerException e) {
			log.warn("Could not send email receipt to " + payorEmail, e);
			messages.addMessage("warn", "usalive-payor-charge-email-failure", "Could not send a receipt to Account {0} at {1} for this charge. We apologize for any inconvenience.", payorName, payorEmail);
		} catch(ConvertException e) {
			log.warn("Could not send email receipt to " + payorEmail, e);
			messages.addMessage("warn", "usalive-payor-charge-email-failure", "Could not send a receipt to Account {0} at {1} for this charge. We apologize for any inconvenience.", payorName, payorEmail);
		}
	}

	public static void sendReceiptForTran(UsaliveUser user, long tranId, String payorName, String payorEmail, Locale locale, String baseUrl, MessagesInputSource messages) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("tranId", tranId);
		params.put("simple.servlet.ServletUser", user);
		try {
			DataLayerMgr.selectInto("GET_PAYOR_TRAN_RECEIPT_INFO", params);
			/*
			long deviceTranCd = ConvertUtils.getLong(params.get("deviceTranCd"));
			String deviceSerialCd = ConvertUtils.getString(params.get("deviceSerialCd"), true);
			String maskedCardNumber = ConvertUtils.getString(params.get("cardNumber"), true);
			String cardType = ConvertUtils.getString(params.get("cardType"), true);
			long chargeTime = ConvertUtils.getLong(params.get("chargeTime"));
			BigDecimal chargeAmount = ConvertUtils.convertRequired(BigDecimal.class, params.get("chargeAmount"));
			String chargeDesc = ConvertUtils.getString(params.get("chargeDesc"), false);
			String chargeReference = ConvertUtils.getString(params.get("chargeReference"), false);
			*/
			try {
				sendReceipt(payorName, payorEmail, baseUrl, params, locale);
				messages.addMessage("success", "usalive-payor-charge-receipt-success", "Sent a receipt of this charge to Account {0} at {1}", payorName, payorEmail);
			} catch(ServletException e) {
				log.warn("Could not send email receipt to " + payorEmail, e);
				messages.addMessage("warn", "usalive-payor-charge-email-failure", "Could not send a receipt to Account {0} at {1} for this charge. We apologize for any inconvenience.", payorName, payorEmail);
			} catch(SQLException e) {
				log.warn("Could not send email receipt to " + payorEmail, e);
				messages.addMessage("warn", "usalive-payor-charge-email-failure", "Could not send a receipt to Account {0} at {1} for this charge. We apologize for any inconvenience.", payorName, payorEmail);
			} catch(DataLayerException e) {
				log.warn("Could not send email receipt to " + payorEmail, e);
				messages.addMessage("warn", "usalive-payor-charge-email-failure", "Could not send a receipt to Account {0} at {1} for this charge. We apologize for any inconvenience.", payorName, payorEmail);
			} catch(ConvertException e) {
				log.warn("Could not send email receipt to " + payorEmail, e);
				messages.addMessage("warn", "usalive-payor-charge-email-failure", "Could not send a receipt to Account {0} at {1} for this charge. We apologize for any inconvenience.", payorName, payorEmail);
			}
		} catch(NotEnoughRowsException e) {
			messages.addMessage("error", "usalive-payor-receipt-invalid-tran", "You do not have permission to view this transaction");
		} catch(SQLException e) {
			log.warn("Could not produce receipt", e);
			messages.addMessage("error", "usalive-payor-receipt-system-error", "We could not process your request at this time. Please try again.");
		} catch(DataLayerException e) {
			log.warn("Could not produce receipt", e);
			messages.addMessage("error", "usalive-payor-receipt-system-error", "We could not process your request at this time. Please try again.");
		} catch(BeanException e) {
			log.warn("Could not produce receipt", e);
			messages.addMessage("error", "usalive-payor-receipt-system-error", "We could not process your request at this time. Please try again.");
		}
	}

	public static void writeReferrerInputs(PageContext pageContext, HttpServletRequest request) throws ServletException, ConvertException, IOException {
		String referrer = RequestUtils.getAttribute(request, "referrer", String.class, false);
		if(StringUtils.isBlank(referrer))
			return;
		JspWriter writer = pageContext.getOut();
		if(writer instanceof BodyContent)
			writer = ((BodyContent) writer).getEnclosingWriter();
		writer.print("<input type=\"hidden\" name=\"referrer\" value=\"");
		StringUtils.appendCDATA(writer, referrer);
		writer.println("\"/>");
		for(Map.Entry<String, Object> entry : RequestUtils.getInputForm(request).getParameters().entrySet()) {
			if(entry.getKey() != null && entry.getKey().startsWith("referrer.")) {
				writer.print("<input type=\"hidden\" name=\"");
				StringUtils.appendCDATA(writer, entry.getKey());
				writer.print("\" value=\"");
				StringUtils.appendCDATA(writer, ConvertUtils.convertSafely(String.class, entry.getValue(), ""));
				writer.println("\"/>");
			}
		}
	}

	public static void redirectToReferrer(HttpServletRequest request, HttpServletResponse response, String defaultReferrer) throws ServletException, ConvertException, IOException {
		String referrer = RequestUtils.getAttribute(request, "referrer", String.class, false);
		if(StringUtils.isBlank(referrer) || referrer.matches("[^\\/]+[]\\/].*"))
			referrer = defaultReferrer;
		else {
			referrer = StringUtils.prepareURLPart(referrer);
			for(Map.Entry<String, Object> entry : RequestUtils.getInputForm(request).getParameters().entrySet()) {
				if(entry.getKey() != null && entry.getKey().startsWith("referrer."))
					RequestUtils.storeForNextRequest(request.getSession(), entry.getKey().substring("referrer.".length()), entry.getValue());
			}
		}
		RequestUtils.redirectWithCarryOver(request, response, referrer);
	}
}