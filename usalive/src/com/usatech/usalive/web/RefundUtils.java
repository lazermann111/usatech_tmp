package com.usatech.usalive.web;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.text.StringUtils;
import simple.text.ThreadSafeDateFormat;
import simple.translator.Translator;

import com.usatech.layers.common.util.WebHelper;
import com.usatech.usalive.servlet.UsaliveUser;

public class RefundUtils {
	private static final Log log = Log.getLog();
	public static String refundFromEmail="customerservice@usatech.com";
	public static String refundFromName="USA Technologies Inc";
	public static DecimalFormat refundDisplayFormat = new DecimalFormat( );
	public static final SimpleDateFormat simpleDateOnlyFormat = new SimpleDateFormat("MM/dd/yyyy");
	public static final ThreadSafeDateFormat dateOnlyFormat = new ThreadSafeDateFormat(simpleDateOnlyFormat);
	static{
		refundDisplayFormat.applyPattern( "###,###,##0.00" );
	}
	
	public static void sendEmail(String subject, String toEmail, String toName, String body) throws ConvertException, ServletException, SQLException, DataLayerException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("fromEmail", refundFromEmail);
		params.put("fromName", refundFromName);
		params.put("subject", subject);
		params.put("toEmail", toEmail);
		params.put("toName", toName);
		params.put("body", body);
		DataLayerMgr.executeCall("SEND_EMAIL", params, true);
		log.info("Request sending pending refund email to "+toEmail);
	}
	
	public static ArrayList<AdminUser> getNotifyAdminUser(long userId) throws DataLayerException,SQLException{
		ArrayList<AdminUser> userList=new ArrayList<AdminUser>();
		Results userResult=DataLayerMgr.executeQuery("GET_ADMIN_EMAILS", new Object[]{userId});
		while(userResult.next()){
			String email=ConvertUtils.getStringSafely(userResult.get("email"));
			if(!StringUtils.isBlank(email)){
				try{
					WebHelper.checkEmail(email);
				}catch(MessagingException e){
					log.info("admin user userName="+userResult.get("userName")+" email is invalid");
					continue;
				}
				userList.add(new AdminUser(ConvertUtils.getStringSafely(userResult.get("userName")), ConvertUtils.getStringSafely(userResult.get("firstName")),ConvertUtils.getStringSafely(userResult.get("lastName")),email));
			}
		}
		return userList;
	}
	
	public static void notifyAdminUser(HttpServletRequest request, InputForm form, HashMap<Long,BigDecimal> multiRefundMap) throws ServletException{
		try{
			UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
			ArrayList<AdminUser> userList=getNotifyAdminUser(user.getUserId());
			if(userList.size()>0){
				Translator translator=RequestUtils.getTranslator(request);
				String baseUrl = RequestUtils.getBaseUrl(request, false);
				if(multiRefundMap==null){// single refund case
					ArrayList<Object> tranIds=new ArrayList<Object>();
					if(form.get("transactionId")!=null){
						tranIds.add(form.get("transactionId"));
					}
					Results refundResult=DataLayerMgr.executeQuery("GET_ISSUED_REFUNDS", new Object[]{user.getUserId(), tranIds});
					while(refundResult.next()){
						String body=translator.translate("usalive-pending-refund-body","Refund Date: {0, DATE,MM/dd/yy @ h:mma z}\nIssuer: {1}\nRefund Reason: {2}\nRefund Comment: {3}\nPreference: {4}\n\nTransactions:\n\nTransaction Id: {5}\nCard Number: {6}\nTotal Amount: {7}\n", 
								refundResult.get("transactionDate"),
								refundResult.get("issuer"),
								refundResult.get("refundReason"),
								refundResult.get("refundComment"),
								baseUrl.toString()+"/preferences.i",
								refundResult.get("transactionId"),
								refundResult.get("cardNumber"),
								refundResult.get("currencySymbol")+refundDisplayFormat.format(refundResult.get("totalAmount"))+" "+refundResult.get("currencyCode"));
						for(AdminUser adminUser: userList){
							RefundUtils.sendEmail("A pending refund was created", adminUser.getEmail(), adminUser.getUserName(), body);
						}
					}
				}else{//multiple refund set notification
					Results refundResult=DataLayerMgr.executeQuery("GET_ISSUED_REFUNDS", new Object[]{user.getUserId(), multiRefundMap.keySet()});
					String body=null;
					StringBuilder fullBody=new StringBuilder();
					while(refundResult.next()){
						if(body==null){
							body=translator.translate("usalive-pending-refund-body","Refund Date: {0, DATE,MM/dd/yy @ h:mma z}\nIssuer: {1}\nRefund Reason: {2}\nRefund Comment: {3}\nPreference: {4}\n\nTransactions:\n\n", 
							refundResult.get("transactionDate"),
							refundResult.get("issuer"),
							refundResult.get("refundReason"),
							refundResult.get("refundComment"),
							baseUrl.toString()+"/preferences.i");
							fullBody.append(body);
						}
						String totalAmount=refundDisplayFormat.format(multiRefundMap.get(ConvertUtils.getLong(refundResult.get("transactionId"))));
						fullBody.append("Transaction Id: ").append(refundResult.get("transactionId")).append("\n").append("Card Number: ").append(refundResult.get("cardNumber")).append("\n").append("Total Amount: ").append(refundResult.get("currencySymbol")).append("-").append(totalAmount).append(" ").append(refundResult.get("currencyCode")).append("\n\n");
					}
					for(AdminUser adminUser: userList){
						String subject=multiRefundMap.size()>1?"Pending refunds were created":"A pending refund was created";
						RefundUtils.sendEmail(subject, adminUser.getEmail(), adminUser.getUserName(), fullBody.toString());
					}
				}
			}
		}catch(DataLayerException|SQLException|ConvertException e){
			log.warn("Failed to notify a pending refund", e);
            throw new ServletException(e);
		}
	}
	
	public static String getRefundFromEmail() {
		return refundFromEmail;
	}
	public static void setRefundFromEmail(String refundFromEmail) {
		RefundUtils.refundFromEmail = refundFromEmail;
	}
	public static String getRefundFromName() {
		return refundFromName;
	}
	public static void setRefundFromName(String refundFromName) {
		RefundUtils.refundFromName = refundFromName;
	}
	
	
}
