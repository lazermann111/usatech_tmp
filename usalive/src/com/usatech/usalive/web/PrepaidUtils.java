package com.usatech.usalive.web;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Pattern;

import javax.servlet.ServletException;

import simple.app.Publisher;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.DbUtils;
import simple.db.NotEnoughRowsException;
import simple.io.ByteInput;
import simple.io.Log;
import simple.lang.InvalidByteValueException;
import simple.results.BeanException;
import simple.text.StringUtils;
import simple.xml.sax.MessagesInputSource;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.app.task.BridgeTask;
import com.usatech.layers.common.InteractionUtils;
import com.usatech.layers.common.ProcessingConstants;
import com.usatech.layers.common.constants.AuthorityAttrEnum;
import com.usatech.layers.common.constants.EventCodePrefix;
import com.usatech.layers.common.constants.ReceiptResult;
import com.usatech.layers.common.constants.ResultCode;
import com.usatech.layers.common.constants.SaleResult;
import com.usatech.layers.common.constants.SaleType;
import com.usatech.layers.common.constants.TranBatchType;
import com.usatech.layers.common.constants.TranDeviceResultType;
import com.usatech.report.ReportingUser;
import com.usatech.report.MassDeviceUpdate.FileType;
import com.usatech.report.ReportingUser.ActionType;
import com.usatech.report.ReportingUser.ResourceType;
import com.usatech.usalive.servlet.UsaliveUser;

public class PrepaidUtils {
	private static final Log log = Log.getLog();
	protected static String bridgeQueueKey = "usat.bridge.to.mst";
	protected static boolean passwordUpperCaseRequired = true;
	protected static boolean passwordLowerCaseRequired = true;
	protected static boolean passwordNonAlphaRequired = true;
	protected static int passwordMinLength = 8;
	protected static final Pattern hasUpperCheck = Pattern.compile("[A-Z]");
	protected static final Pattern hasLowerCheck = Pattern.compile("[a-z]");
	protected static final Pattern hasNonAlphaCheck = Pattern.compile("[!-@\\[-^`{-~]");
	protected static final SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
	public static Date MAX_DATE;
	public static FileType MASS_CARD_UPDATE_FILE_TYPE = FileType.EXCEL;
	public static int MAX_MASS_CARD_UPDATE_ROW = 100;
	public static BigDecimal MAX_CARD_BALANCE_CHANGE = new BigDecimal(100);
	public static BigDecimal MIN_CARD_BALANCE_CHANGE = new BigDecimal(0);
	static{
		try{
			MAX_DATE=formatter.parse("12/31/2099");
		}catch(Exception e){}
	}

	public static boolean decreasePrepaidBalance(long consumerAcctId, BigDecimal decreaseAmount, String reason, UsaliveUser user, MessagesInputSource messages, Publisher<ByteInput> publisher) throws ServletException {
		return decreasePrepaidBalance(consumerAcctId, decreaseAmount, reason, user, messages, publisher,"");
	}
	public static boolean decreasePrepaidBalance(long consumerAcctId, BigDecimal decreaseAmount, String reason, UsaliveUser user, MessagesInputSource messages, Publisher<ByteInput> publisher, String messagePrefix) throws ServletException {
		user.checkPermission(ReportingUser.ResourceType.PREPAID_CONSUMER_ACCT, ReportingUser.ActionType.EDIT, consumerAcctId);
		// get info
		long tranId = 0;
		boolean checkTerminal;
		char tranImportNeeded = 'N';
		Map<String, Object> params = new HashMap<>();
		try {
			Connection conn = DataLayerMgr.getConnection("OPER");
			boolean okay = false;
			try {
				params.put("consumerAcctId", consumerAcctId);
				DataLayerMgr.selectInto(conn, "GET_PREPAID_ACCT_INFO", params);
				DataLayerMgr.executeCall(conn, "GET_OR_CREATE_PREPAID_DEVICE", params);
				DataLayerMgr.selectInto(conn, "GET_PREPAID_POS_PTA", params);
				long posId = ConvertUtils.getLong(params.get("posId"), 0L);
				if(posId == 0) {
					// create pos
					DataLayerMgr.executeCall(conn, "CREATE_POS", params);
				}
				long posPtaId = ConvertUtils.getLong(params.get("posPtaId"), 0L);
				if(posPtaId == 0) {
					// create pos pta
					DataLayerMgr.executeCall(conn, "CREATE_POS_PTA", params);
				}
				conn.commit();
				// get next master id
				DataLayerMgr.executeCall(conn, "NEXT_MASTER_ID_BY_SERIAL", params);
				conn.commit();
				// lock auth hold
				DataLayerMgr.executeCall(conn, "LOCK_AUTH_HOLD", params);

				// get balance
				BigDecimal balanceAmount = ConvertUtils.convertRequired(BigDecimal.class, params.get("balanceAmount"));
				BigDecimal onHoldAmount = ConvertUtils.convert(BigDecimal.class, params.get("onHoldAmount"));
				decreaseAmount = decreaseAmount.abs();

				// check balance
				if(balanceAmount.compareTo(decreaseAmount) < 0) {
					messages.addMessage("error", "usalive-prepaid-acct-decrease-too-great", messagePrefix+"The balance on this card is only {0,CURRENCY}. You may not decrease the balance below zero.", balanceAmount, decreaseAmount);
					return false;
				}
				if(onHoldAmount != null && onHoldAmount.signum() != 0 && balanceAmount.subtract(onHoldAmount).compareTo(decreaseAmount) < 0) {
					messages.addMessage("error", "usalive-prepaid-acct-decrease-too-great-with-hold", messagePrefix+"The available balance on this card is only {0,CURRENCY} (including a hold of {2,CURRENCY}). You may not decrease the available balance below zero.", balanceAmount, decreaseAmount, onHoldAmount);
					return false;
				}

				// insert auth
				params.put("authHoldUsed", true);
				params.put("sentToDevice", true);
				params.put("passThru", false);
				long deviceTranCd = ConvertUtils.getLong(params.get("masterId"));
				String deviceName = ConvertUtils.getString(params.get("deviceName"), true);
				params.put("globalEventCd", "A:" + deviceName + ":" + deviceTranCd);
				params.put("eventId", deviceTranCd);
				params.put("invalidEventId", "N");
				long time = System.currentTimeMillis();
				long offsetMillis = TimeZone.getDefault().getOffset(time);
				long offsetMin = offsetMillis / (60 * 1000);
				long localTime = time + offsetMillis;

				params.put("tranStartTime", localTime);
				params.put("authTime", time);
				params.put("authResultCd", 'Y');
				params.put("entryMethod", 'M');
				params.put("authorityRespCd", "0");
				params.put("authorityRespDesc", "Admin Balance Decrease by " + user.getUserName());
				params.put("authAuthorityTs", time);
				int minorCurrencyFactor = ConvertUtils.getInt(params.get("minorCurrencyFactor"));
				BigDecimal decreaseAmountMinor = decreaseAmount.multiply(BigDecimal.valueOf(minorCurrencyFactor));
				params.put("authAmount", decreaseAmountMinor);
				params.put("balanceAmount", balanceAmount.multiply(BigDecimal.valueOf(minorCurrencyFactor)));
				params.put("requestedAmount", decreaseAmountMinor);
				params.put("approvedAmount", decreaseAmountMinor);
				params.put("addAuthHoldDays", 1);

				DataLayerMgr.executeCall(conn, "INSERT_AUTHORIZATION", params);
				
				// insert sale & line items
				params.put("globalSessionCode", "U:" + deviceName + ":" + deviceTranCd);
				params.put("globalEventCdPrefix", EventCodePrefix.APP_LAYER.getValue());
				params.put("deviceBatchId", 0);
				params.put("saleUtcTsMs", time);
				params.put("saleUtcOffsetMin", offsetMin);
				params.put("tranDeviceResultTypeCd", TranDeviceResultType.SUCCESS.getValue());
				params.put("receiptResultCd", ReceiptResult.NOT_REQUESTED.getValue());
				params.put("tranBatchTypeCd", TranBatchType.ACTUAL.getValue());
				params.put("hashTypeCd", ProcessingConstants.HASH_TYPE);
				params.put("saleTax", BigDecimal.ZERO);
				params.put("saleTypeCd", SaleType.ACTUAL.getValue());
				params.put("deviceTranCd", deviceTranCd);
				params.put("saleResultId", SaleResult.SUCCESS.getValue());
				params.put("saleAmount", decreaseAmountMinor);
				params.put("voidAllowed", 'N');
				params.put("tranLineItemHash", "00");
				DataLayerMgr.executeCall(conn, "CREATE_SALE", params);

				ResultCode resultCd = ResultCode.getByValue(ConvertUtils.convert(Byte.class, params.get("resultCd")));
				String errorMessage = ConvertUtils.convert(String.class, params.get("errorMessage"));
				switch(resultCd) {
					case SUCCESS:
						if(errorMessage != null)
							log.warn(errorMessage);

						tranId = ConvertUtils.getLong(params.get("tranId"));
						char tranStateCd = ConvertUtils.convert(Character.class, params.get("tranStateCd"));
						log.info("Created sale for tranId: " + tranId);
						// create line items
						params.put("hostPortNum", 0);
						params.put("tliTax", BigDecimal.ZERO);
						params.put("tliUtcTsMs", time);
						params.put("tliUtcOffsetMin", offsetMin);
						params.put("tliPositionCd", null);
						params.put("tliSaleResultId", SaleResult.SUCCESS.getValue());
						params.put("hostPositionNum", 0);

						params.put("tliTypeId", 210);
						params.put("tliQuantity", 1);
						params.put("tliAmount", decreaseAmountMinor);
						params.put("tliDesc", reason);
						DataLayerMgr.executeCall(conn, "CREATE_TRAN_LINE_ITEM", params);

						StringBuilder sb = new StringBuilder();
						sb.append("By ");
						if(user.getMasterUser() != null && !StringUtils.isBlank(user.getMasterUser().getUserName()))
							sb.append(user.getMasterUser().getUserName());
						else if(!StringUtils.isBlank(user.getUserName()))
							sb.append(user.getUserName());
						else
							sb.append("UNKNOWN");

						params.put("tliTypeId", 207);
						params.put("tliQuantity", 1);
						params.put("tliAmount", null);
						params.put("tliDesc", sb.toString());
						DataLayerMgr.executeCall(conn, "CREATE_TRAN_LINE_ITEM", params);

						params.put("saleDurationSec", 0);
						DataLayerMgr.executeCall(conn, "FINALIZE_SALE", params);
						resultCd = ResultCode.getByValue(ConvertUtils.convert(Byte.class, params.get("resultCd")));
						errorMessage = ConvertUtils.convert(String.class, params.get("errorMessage"));
						if(errorMessage != null)
							log.warn(errorMessage);
						try {
							tranImportNeeded = ConvertUtils.convertRequired(Character.class, params.get("tranImportNeeded"));
						} catch(ConvertException e) {
							throw new ServletException("Could not convert tranImportNeeded to a char", e);
						}

						if(resultCd == ResultCode.SUCCESS)
							log.info("Finalized sale for tranId: " + tranId);
						else
							throw new ServletException(errorMessage);
						switch(tranStateCd) {
							case '8':
							case '9':
								checkTerminal = true;
								break;
							default:
								checkTerminal = false;
						}
						break;
					case ILLEGAL_STATE:
					case DUPLICATE:
					case SALE_VOIDED:
					default:
						throw new ServletException(errorMessage);
				}
				okay = true;
				conn.commit();
				messages.addMessage("success", "usalive-prepaid-acct-decrease-successful", messagePrefix+"Decreased the balance on this account by {0,CURRENCY}. The change will be shown shortly.", decreaseAmount, reason);
			} catch(NotEnoughRowsException e) {
				messages.addMessage("error", "usalive-prepaid-acct-not-found",messagePrefix+ "Invalid account. Please refresh your page");
				log.warn("Consumer Acct " + consumerAcctId + " was not found", e);
				return false;
			} finally {
				if(!okay)
					DbUtils.rollbackSafely(conn);
				DbUtils.closeSafely(conn);
			}
		} catch(SQLException | DataLayerException | ConvertException | BeanException | InvalidByteValueException e) {
			throw new ServletException(e);
		}
		// publish import
		if(tranImportNeeded == 'Y' && tranId > 0) {
			MessageChain mc = new MessageChainV11();
			MessageChainStep step = mc.addStep(getBridgeQueueKey());
			step.setAttribute(BridgeTask.BRIDGE_TO_QUEUE_KEY, InteractionUtils.getTranImportQueueKey());
			step.setAttribute(AuthorityAttrEnum.ATTR_TRAN_ID, tranId);
			try {
				MessageChainService.publish(mc, publisher);
			} catch(ServiceException e) {
				log.error("Could not publish Import Tran for " + tranId, e);
			}
		}
		if(checkTerminal) {
			MessageChain mc = new MessageChainV11();
			MessageChainStep step = mc.addStep(getBridgeQueueKey());
			step.setTemporary(true);
			step.setAttribute(BridgeTask.BRIDGE_TO_QUEUE_KEY, InteractionUtils.getCheckTerminalQueueKey());
			step.setAttribute(AuthorityAttrEnum.ATTR_TRAN_ID, tranId);
			step.setAttribute(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_CLASS, params.get("paymentSubtypeClass"));
			step.setAttribute(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_KEY_ID, params.get("paymentSubtypeKeyId"));
			try {
				MessageChainService.publish(mc, publisher);
			} catch(ServiceException e) {
				log.error("Could not publish Check Terminal for " + tranId, e);
			}
		}
		return true;
	}

	public static boolean increasePrepaidBalance(long consumerAcctId, BigDecimal increaseAmount, String reason, UsaliveUser user, MessagesInputSource messages, Publisher<ByteInput> publisher) throws ServletException {
		return increasePrepaidBalance(consumerAcctId, increaseAmount, reason, user, messages, publisher,"");
	}
	public static boolean increasePrepaidBalance(long consumerAcctId, BigDecimal increaseAmount, String reason, UsaliveUser user, MessagesInputSource messages, Publisher<ByteInput> publisher, String messagePrefix) throws ServletException {
		user.checkPermission(ReportingUser.ResourceType.PREPAID_CONSUMER_ACCT, ReportingUser.ActionType.EDIT, consumerAcctId);
		// get info
		long tranId = 0;
		Map<String, Object> params = new HashMap<>();
		try {
			Connection conn = DataLayerMgr.getConnection("OPER");
			boolean okay = false;
			try {
				params.put("consumerAcctId", consumerAcctId);
				DataLayerMgr.selectInto(conn, "GET_PREPAID_ACCT_INFO", params);
				DataLayerMgr.executeCall(conn, "GET_OR_CREATE_PREPAID_DEVICE", params);
				DataLayerMgr.selectInto(conn, "GET_PREPAID_POS_PTA", params);
				long posId = ConvertUtils.getLong(params.get("posId"), 0L);
				if(posId == 0) {
					// create pos
					DataLayerMgr.executeCall(conn, "CREATE_POS", params);
				}
				long posPtaId = ConvertUtils.getLong(params.get("posPtaId"), 0L);
				if(posPtaId == 0) {
					// create pos pta
					DataLayerMgr.executeCall(conn, "CREATE_POS_PTA", params);
				}
				conn.commit();
				// get next master id
				DataLayerMgr.executeCall(conn, "NEXT_MASTER_ID_BY_SERIAL", params);
				conn.commit();


				long deviceTranCd = ConvertUtils.getLong(params.get("masterId"));
				String deviceName = ConvertUtils.getString(params.get("deviceName"), true);
				long time = System.currentTimeMillis();
				long offsetMillis = TimeZone.getDefault().getOffset(time);
				long offsetMin = offsetMillis / (60 * 1000);
				// long localTime = time + offsetMillis;
				int minorCurrencyFactor = ConvertUtils.getInt(params.get("minorCurrencyFactor"));
				increaseAmount = increaseAmount.abs();
				BigDecimal increaseAmountMinor = increaseAmount.multiply(BigDecimal.valueOf(minorCurrencyFactor)).negate();

				// insert sale & line items
				params.put("globalSessionCode", "U:" + deviceName + ":" + deviceTranCd);
				params.put("globalEventCdPrefix", EventCodePrefix.APP_LAYER.getValue());
				params.put("deviceBatchId", 0);
				params.put("saleUtcTsMs", time);
				params.put("saleUtcOffsetMin", offsetMin);
				params.put("tranDeviceResultTypeCd", TranDeviceResultType.SUCCESS.getValue());
				params.put("receiptResultCd", ReceiptResult.NOT_REQUESTED.getValue());
				params.put("tranBatchTypeCd", TranBatchType.ACTUAL.getValue());
				params.put("hashTypeCd", ProcessingConstants.HASH_TYPE);
				params.put("saleTax", BigDecimal.ZERO);
				params.put("saleTypeCd", SaleType.ACTUAL.getValue());
				params.put("deviceTranCd", deviceTranCd);
				params.put("saleResultId", SaleResult.SUCCESS.getValue());
				params.put("saleAmount", increaseAmountMinor);
				params.put("voidAllowed", 'N');
				params.put("tranLineItemHash", "00");
				DataLayerMgr.executeCall(conn, "CREATE_SALE", params);

				ResultCode resultCd = ResultCode.getByValue(ConvertUtils.convert(Byte.class, params.get("resultCd")));
				String errorMessage = ConvertUtils.convert(String.class, params.get("errorMessage"));
				switch(resultCd) {
					case SUCCESS:
						if(errorMessage != null)
							log.warn(errorMessage);

						tranId = ConvertUtils.getLong(params.get("tranId"));
						// char tranStateCd = ConvertUtils.convert(Character.class, params.get("tranStateCd"));
						log.info("Created sale for tranId: " + tranId);
						// create line items
						params.put("hostPortNum", 0);
						params.put("tliTax", BigDecimal.ZERO);
						params.put("tliUtcTsMs", time);
						params.put("tliUtcOffsetMin", offsetMin);
						params.put("tliPositionCd", null);
						params.put("tliSaleResultId", SaleResult.SUCCESS.getValue());
						params.put("hostPositionNum", 0);

						params.put("tliTypeId", 210);
						params.put("tliQuantity", 1);
						params.put("tliAmount", increaseAmountMinor);
						params.put("tliDesc", reason);
						DataLayerMgr.executeCall(conn, "CREATE_TRAN_LINE_ITEM", params);

						StringBuilder sb = new StringBuilder();
						sb.append("By ");
						if(user.getMasterUser() != null && !StringUtils.isBlank(user.getMasterUser().getUserName()))
							sb.append(user.getMasterUser().getUserName());
						else if(!StringUtils.isBlank(user.getUserName()))
							sb.append(user.getUserName());
						else
							sb.append("UNKNOWN");

						params.put("tliTypeId", 207);
						params.put("tliQuantity", 1);
						params.put("tliAmount", null);
						params.put("tliDesc", sb.toString());
						DataLayerMgr.executeCall(conn, "CREATE_TRAN_LINE_ITEM", params);

						params.put("saleDurationSec", 0);
						DataLayerMgr.executeCall(conn, "FINALIZE_SALE", params);
						resultCd = ResultCode.getByValue(ConvertUtils.convert(Byte.class, params.get("resultCd")));
						errorMessage = ConvertUtils.convert(String.class, params.get("errorMessage"));
						if(errorMessage != null)
							log.warn(errorMessage);

						if(resultCd == ResultCode.SUCCESS)
							log.info("Finalized sale for tranId: " + tranId);
						else
							throw new ServletException(errorMessage);

						// insert refund
						params.put("increaseAmount", increaseAmount);
						params.put("refundDesc", "Admin Balance Increase by " + user.getUserName());
						params.put("refundTime", time);
						params.put("issuedBy", user.getUserName());

						DataLayerMgr.executeCall(conn, "CREATE_PREPAID_BALANCE_INCREASE_REFUND", params);
						break;
					case ILLEGAL_STATE:
					case DUPLICATE:
					case SALE_VOIDED:
					default:
						throw new ServletException(errorMessage);
				}
				okay = true;
				conn.commit();
				messages.addMessage("success", "usalive-prepaid-acct-increase-successful", messagePrefix+"Increased the balance on this account by {0,CURRENCY}. The change will be shown shortly.", increaseAmount, reason);
			} catch(NotEnoughRowsException e) {
				messages.addMessage("error", "usalive-prepaid-acct-not-found", messagePrefix+"Invalid account. Please refresh your page");
				log.warn("Consumer Acct " + consumerAcctId + " was not found", e);
				return false;
			} finally {
				if(!okay)
					DbUtils.rollbackSafely(conn);
				DbUtils.closeSafely(conn);
			}
		} catch(SQLException | DataLayerException | ConvertException | BeanException | InvalidByteValueException e) {
			throw new ServletException(e);
		}
		// publish import
		if(tranId > 0) {
			MessageChain mc = new MessageChainV11();
			MessageChainStep step = mc.addStep(getBridgeQueueKey());
			step.setAttribute(BridgeTask.BRIDGE_TO_QUEUE_KEY, InteractionUtils.getTranImportQueueKey());
			step.setAttribute(AuthorityAttrEnum.ATTR_TRAN_ID, tranId);
			try {
				MessageChainService.publish(mc, publisher);
			} catch(ServiceException e) {
				log.error("Could not publish Import Tran for " + tranId, e);
			}
		}
		MessageChain mc = new MessageChainV11();
		MessageChainStep step = mc.addStep(getBridgeQueueKey());
		step.setTemporary(true);
		step.setAttribute(BridgeTask.BRIDGE_TO_QUEUE_KEY, InteractionUtils.getCheckTerminalQueueKey());
		step.setAttribute(AuthorityAttrEnum.ATTR_TRAN_ID, tranId);
		step.setAttribute(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_CLASS, params.get("paymentSubtypeClass"));
		step.setAttribute(AuthorityAttrEnum.ATTR_PAYMENT_SUBTYPE_KEY_ID, params.get("paymentSubtypeKeyId"));
		try {
			MessageChainService.publish(mc, publisher);
		} catch(ServiceException e) {
			log.error("Could not publish Check Terminal for " + tranId, e);
		}
		return true;
	}

	public static String getBridgeQueueKey() {
		return bridgeQueueKey;
	}

	public static void setBridgeQueueKey(String bridgeQueueKey) {
		PrepaidUtils.bridgeQueueKey = bridgeQueueKey;
	}
	
	
	public static boolean checkPassword(MessagesInputSource mis, String password, String confirm) {
		if(!password.equals(confirm)) {
			mis.addMessage("error", "prepaid-password-not-confirmed", "The password does not match the confirmation. Please re-type both.");
			return false;
		}
		if(password.length() < passwordMinLength) {
			mis.addMessage("error", "prepaid-password-too-short", "The password must be at least " + passwordMinLength + " digits. Please choose another.", passwordMinLength);
			return false;
		}
		if(passwordUpperCaseRequired && !hasUpperCheck.matcher(password).find()) {
			mis.addMessage("error", "prepaid-password-no-uppercase", "The password must contain at least 1 uppercase letter.");
			return false;
		}
		if(passwordLowerCaseRequired && !hasLowerCheck.matcher(password).find()) {
			mis.addMessage("error", "prepaid-password-no-lowercase", "The password must contain at least 1 lowercase letter.");
			return false;
		}
		if(passwordNonAlphaRequired && !hasNonAlphaCheck.matcher(password).find()) {
			mis.addMessage("error", "prepaid-password-no-nonalpha", "The password must contain at least 1 number or 1 punctuation symbol.");
			return false;
		}
		return true;
	}

	public static FileType getMASS_CARD_UPDATE_FILE_TYPE() {
		return MASS_CARD_UPDATE_FILE_TYPE;
	}

	public static void setMASS_CARD_UPDATE_FILE_TYPE(
			FileType mASS_CARD_UPDATE_FILE_TYPE) {
		MASS_CARD_UPDATE_FILE_TYPE = mASS_CARD_UPDATE_FILE_TYPE;
	}

	public static int getMAX_MASS_CARD_UPDATE_ROW() {
		return MAX_MASS_CARD_UPDATE_ROW;
	}

	public static void setMAX_MASS_CARD_UPDATE_ROW(int mAX_MASS_CARD_UPDATE_ROW) {
		MAX_MASS_CARD_UPDATE_ROW = mAX_MASS_CARD_UPDATE_ROW;
	}

	public static BigDecimal getMAX_CARD_BALANCE_CHANGE() {
		return MAX_CARD_BALANCE_CHANGE;
	}

	public static void setMAX_CARD_BALANCE_CHANGE(BigDecimal mAX_CARD_BALANCE_CHANGE) {
		MAX_CARD_BALANCE_CHANGE = mAX_CARD_BALANCE_CHANGE;
	}

	public static BigDecimal getMIN_CARD_BALANCE_CHANGE() {
		return MIN_CARD_BALANCE_CHANGE;
	}

	public static void setMIN_CARD_BALANCE_CHANGE(BigDecimal mIN_CARD_BALANCE_CHANGE) {
		MIN_CARD_BALANCE_CHANGE = mIN_CARD_BALANCE_CHANGE;
	}
	
	
}
