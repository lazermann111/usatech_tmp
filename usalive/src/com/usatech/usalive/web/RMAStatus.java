package com.usatech.usalive.web;

import simple.lang.EnumIntValueLookup;
import simple.lang.InvalidIntValueException;



public enum RMAStatus {
    NEW(0,"New"),
    CONFIRMED(1,"Confirmed"),
    CANCELED(-1,"Canceled"),
    PROCESSED(2,"Processed"),
    DELETED(-2,"Deleted")
    ;

    private final int value;
    private final String description;
    private RMAStatus(int value, String description) {
        this.value = value;
        this.description=description;
    }
    public int getValue() {
        return value;
    }
    
    public String getDescription() {
		return description;
	}

	protected final static EnumIntValueLookup<RMAStatus> lookup = new EnumIntValueLookup<RMAStatus>(RMAStatus.class);
    public static RMAStatus getByValue(int value) throws InvalidIntValueException {
    	return lookup.getByValue(value);
    }
}
