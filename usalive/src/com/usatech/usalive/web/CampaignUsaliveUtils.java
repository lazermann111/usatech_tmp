package com.usatech.usalive.web;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;


import com.usatech.usalive.servlet.UsaliveUser;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.BeanException;
import simple.results.Results;
import simple.servlet.InputForm;


public class CampaignUsaliveUtils {
	public static float minHoursBetweenEmails= 4.0f;
	
	public static float getMinHoursBetweenEmails() {
		return minHoursBetweenEmails;
	}

	public static void setMinHoursBetweenEmails(float minHoursBetweenEmails) {
		CampaignUsaliveUtils.minHoursBetweenEmails = minHoursBetweenEmails;
	}

	public static boolean checkAllow(String callId, InputForm inputForm) throws BeanException, DataLayerException, SQLException, ConvertException{
		Map<String, Object> params = new HashMap<String, Object>();
		DataLayerMgr.selectInto(callId, inputForm, params);
		return ConvertUtils.getBoolean(params.get("permitted"), false);
	}
	
	public static void addCampaignBlast(Long campaignId, Long consumerId, UsaliveUser user, InputForm form) throws DataLayerException,SQLException,ConvertException,ServiceException{
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("campaignId", campaignId);
		params.put("userId", user.getUserId());
		if(form.get("sendTime")==null){
			params.put("sendTime", Calendar.getInstance().getTime());
		}else{
			params.put("sendTime", form.get("sendTime"));
		}
		if(form.get("toMarketing")!=null){
			params.put("toMarketing", form.get("toMarketing"));
		}else{
			params.put("email", form.get("email"));
		}
		if(consumerId!=null){
			params.put("consumerId", consumerId);
		}
		String isForAllUser=ConvertUtils.getStringSafely(form.get("isForAllUser"), "Y");
		if(isForAllUser.equalsIgnoreCase("N")){
			params.put("consumerRegisteredDate", form.get("consumerRegisteredDate"));
		}
		params.put("timeZoneGuid", form.get("timeZoneGuid"));
		DataLayerMgr.executeCall("ADD_CAMPAIGN_BLAST", params, true);
	}
	
	public static Long chooseMarketingEmailConsumer(Long campaignId) throws DataLayerException,SQLException,ConvertException{
		Long consumerId=null;
		Results consumerResults = DataLayerMgr.executeQuery("CHOOSE_CAMPAIGN_TEST_CONSUMER", new Object[]{campaignId});
		if(consumerResults.next()){
			consumerId=ConvertUtils.getLong(consumerResults.get("consumerId"));
		}
		return consumerId;
	}
}
