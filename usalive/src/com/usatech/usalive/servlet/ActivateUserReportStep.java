package com.usatech.usalive.servlet;

import java.io.IOException;
import java.io.PrintStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;
/**
 * 
 * @author yhe
 *
 */
public class ActivateUserReportStep extends AbstractStep {
	private static final Log log = Log.getLog();

	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String statusMessage;
		PrintStream out = null;
		try{
			out=new PrintStream(response.getOutputStream());
			out.println("<script type=\"text/javascript\">//<![CDATA[");
			try {
	    		DataLayerMgr.executeCall("ACTIVATE_USER_REPORT", form, true);
	    		statusMessage="updateStatus('Successfully activated.','info-success');$('mainSelect').getSelected().set('style', 'color:#336699');";
			}catch(Exception e) {
				statusMessage="updateStatus('Failed to activate the user report. Please try again.','info-failure');";
				log.error("Failed to activate the user report.", e);
			} 
			out.println(statusMessage);
			out.print("//]]></script>");
			out.println();
			out.flush();
		}catch(IOException e){
			throw new ServletException("Failed to activate the user report.", e);
		}
	}

}