package com.usatech.usalive.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.usatech.usalive.device.api.DeviceLocationDao;
import com.usatech.usalive.device.dto.DeviceLocation;
import com.usatech.usalive.device.impl.DeviceLocationDbDaoImpl;
import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class DeviceLocationResolverStep extends AbstractStep {
	static final Log log = Log.getLog();

	private volatile DeviceLocationDao deviceLocationDao;
	public static long minRequestPeriodMinutes;
	public static String googleMapsApiKey;

	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response)
			throws ServletException {
		// Checking preconditions
		Results result = (Results) form.getAttribute("results");
		if (result == null) {
			throw new IllegalStateException("Expected input form should containts results attribute");
		}

		int oldRowNum = result.getRow();

		if (oldRowNum == 0 && !result.next()) {
			throw new IllegalStateException("Results is empty; Expected results with deviceId");
		}

		// Resolving device locations
		long deviceId = extractDeviceId(result);
		DeviceLocation ret = getDeviceLocationDao().getLatestDeviceLocation(deviceId);
		if (ret != null) {
			ret.setUpdateRestricted(!isUpdateAllowed(ret));
		}

		// NOTE: it's hack for backward capability with JSP page and maybe other
		// places
		result.setRow(oldRowNum);
		// adding device location to model
		if (ret == null) {
			ret = new DeviceLocation();
			ret.setUpdateRestricted(true);
		}
		String iccid = getIccidByDeviceId(deviceId);
		log.debug("iccid: "+iccid);
		if (iccid == null) {
			ret.setDeviceId(null);
		}
		form.setAttribute("deviceLocation", ret);
		form.setAttribute("locationImgMapUrl", hasGeoInfo(ret)
				? USALiveUtils.buildGoogleMapAreaUrl(ret.getLatitude(), ret.getLongitude(), googleMapsApiKey) : "");
		form.setAttribute("googleMapsApiKey", googleMapsApiKey);
	}

	private boolean hasGeoInfo(DeviceLocation dl) {
		if (dl == null) {
			return false;
		}

		if (dl.getLatitude() == null || dl.getLongitude() == null) {
			return false;
		}

		return true;
	}

	String getIccidByDeviceId(long deviceId) throws ServletException {
		try {
			Map<String, Object> params = new HashMap<>();
			params.put("device_id", deviceId);
			Results res = DataLayerMgr.executeQuery("GET_DEVICE_ICCID_BY_ID", params);
			if (res.next()) {
				return res.getFormattedValue("device_iccid");
			}
			return null;
		} catch (SQLException sqlException) {
			throw new ServletException("Failed to resolve deviceIccid", sqlException);
		} catch (DataLayerException dataLayerException) {
			throw new ServletException("Failed to resolve deviceIccid", dataLayerException);
		}
	}

	private boolean isUpdateAllowed(DeviceLocation dl) {
		if (dl.getRequestedAt() != null) {
			return false;
		}

		if (dl.getUpdatedAt() == null) {
			return true;
		}

		return dl.getUpdatedAt().getTime() + TimeUnit.MINUTES.toMillis(getMinRequestPeriodMinutes()) < System
				.currentTimeMillis();
	}

	private Long extractDeviceId(Results result) {
		try {
			return result.getValue("deviceId", long.class);
		} catch (ConvertException convertException) {
			throw new IllegalStateException("Incorrect deviceId type; Expected long type", convertException);
		} catch (Exception exception) {
			throw new IllegalStateException("Incorrect deviceId type", exception);
		}
	}

	public DeviceLocationDao getDeviceLocationDao() {
		if (deviceLocationDao == null) {
			synchronized (this) {
				if (deviceLocationDao == null) {
					deviceLocationDao = new DeviceLocationDbDaoImpl();
				}
			}
		}
		return deviceLocationDao;
	}

	public void setDeviceLocationDao(DeviceLocationDao deviceLocationDao) {
		this.deviceLocationDao = deviceLocationDao;
	}

	public static long getMinRequestPeriodMinutes() {
		return minRequestPeriodMinutes;
	}

	public static void setMinRequestPeriodMinutes(long minRequestPeriodMinutes) {
		DeviceLocationResolverStep.minRequestPeriodMinutes = minRequestPeriodMinutes;
	}

	public static String getGoogleMapsApiKey() {
		return googleMapsApiKey;
	}

	public static void setGoogleMapsApiKey(String googleMapsApiKey) {
		DeviceLocationResolverStep.googleMapsApiKey = googleMapsApiKey;
	}
}
