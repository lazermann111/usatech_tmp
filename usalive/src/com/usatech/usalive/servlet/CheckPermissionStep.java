package com.usatech.usalive.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usatech.report.ReportingUser;
import com.usatech.report.ReportingUser.ActionType;
import com.usatech.report.ReportingUser.ResourceType;

import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.steps.AbstractStep;
import simple.text.StringUtils;
/**
 * Updates the transport properties
 * 
 * @author Brian S. Krug
 *  
 */
public class CheckPermissionStep extends AbstractStep {
	protected ReportingUser.ResourceType resource;
	protected ReportingUser.ActionType action;
	protected String bean;
	
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		UsaliveUser checkUser = (UsaliveUser) RequestUtils.getUser(request);
		Object resourceId = form.getAttribute(getBean());
		if (resourceId == null && "terminalId".equalsIgnoreCase(getBean()) && !StringUtils.isBlank(form.getString("deviceSerialCd", false))) {
			resourceId = USALiveUtils.getTerminalId(form.getString("deviceSerialCd", false));
			form.setAttribute(getBean(), resourceId);
		}
		checkUser.checkPermission(getResource(), getAction(), resourceId);
	}
	public ReportingUser.ActionType getAction() {
		return action;
	}
	public void setAction(ReportingUser.ActionType action) {
		this.action = action;
	}
	public ReportingUser.ResourceType getResource() {
		return resource;
	}
	public void setResource(ReportingUser.ResourceType resource) {
		this.resource = resource;
	}
	public String getBean() {
		return bean;
	}
	public void setBean(String bean) {
		this.bean = bean;
	}
}
