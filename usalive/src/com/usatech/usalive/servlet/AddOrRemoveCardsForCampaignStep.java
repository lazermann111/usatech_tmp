package com.usatech.usalive.servlet;

import java.io.PrintStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.steps.AbstractStep;
/**
 * 
 * @author yhe
 *
 */
public class AddOrRemoveCardsForCampaignStep extends AbstractStep {
	private static final Log log = Log.getLog();

	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		PrintStream out = null;
		String actionType=ConvertUtils.getStringSafely(RequestUtils.getAttribute(request, "actionType", true));
		try {
			out=new PrintStream(response.getOutputStream());
			out.println("<script type=\"text/javascript\">//<![CDATA[");
			int campaignId;
			int[] cardIds;
			try{
				campaignId=form.getInt("campaignId", true, 0);
			}catch(Exception e){
				out.print("addOrRemoveSelectedResult('"+actionType+"',-1, 'Please select at least one campaign.');//]]></script>");
				out.flush();
				return;
			}
			try{
				cardIds=form.getIntArray("cardIds", true);
			}catch(Exception e){
				out.print("addOrRemoveSelectedResult('"+actionType+"',-1,'Please select at least one card.');//]]></script>");
				out.flush();
				return;
			}
			if(actionType.equals("add")){
				DataLayerMgr.executeCall("ADD_CARDS_TO_CAMPAIGN", form, true);
				out.print("addOrRemoveSelectedResult('add',1, '');");
			}else if(actionType.equals("remove")){
				DataLayerMgr.executeCall("REMOVE_CARDS_FROM_CAMPAIGN", form, true);
				out.print("addOrRemoveSelectedResult('remove',1, '');");
			}else{
				log.error("Unknown actionType "+actionType);
				out.print("addOrRemoveSelectedResult('unknown',-1, 'Unknown action. Please try again.');//]]></script>");
				out.flush();
				return;
			}
			
		}catch(Exception e) {
			out.print("addOrRemoveSelectedResult('"+actionType+"',-1, 'An error occurred on the server.');");
			log.error("Failed to "+actionType+" cards to the campaign. Please try again.", e);
		} 
		out.print("//]]></script>");
		out.println();
		out.flush();
		
	}

}