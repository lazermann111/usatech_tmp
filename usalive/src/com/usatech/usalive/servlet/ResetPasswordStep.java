package com.usatech.usalive.servlet;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.security.SecurityUtils;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;
import simple.text.Hasher;

public class ResetPasswordStep extends AbstractStep {

	protected final Hasher hasher;
	protected final Random random = new Random();
	protected int saltSize = 8;
	
	public ResetPasswordStep() throws NoSuchAlgorithmException {
		hasher = new Hasher("SHA1", true);
	}
	
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		resetPassword(form);
	}
	
	public void resetPassword(InputForm form) throws ServletException {
		String password = SecurityUtils.generatePassword();
		byte[] salt = new byte[getSaltSize()];
		random.nextBytes(salt); 
		byte[] hash = hasher.hashRaw(password.getBytes(), salt);
		form.setAttribute("passwordHash", hash);
		form.setAttribute("passwordSalt", salt);
		try {
			DataLayerMgr.executeCall("UPDATE_USER_PASSWORD", form, false);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DataLayerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Results results;
		String email="", firstName="", lastName="";
		try {
			results =  DataLayerMgr.executeQuery("GET_CUSTOMER_USER_INFO", form);
			
			if (results.next()){
				try {
					email = results.getValue("email", String.class);
					firstName = results.getValue("firstName", String.class);
					lastName = results.getValue("lastName", String.class);
				} catch (ConvertException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("email", email);
			params.put("name", firstName + " " + lastName);
			params.put("subject", "Your customized USA Live website password was reset.");
			StringBuilder content = new StringBuilder();
			content.append(firstName).append(" - Your customized USA Live website password was reset.").append('\n').
				append("To access the USALive website, please visit this address or click here - https://usalive.usatech.com").append('\n').
				append("Your username is ").append(email).append(".").append('\n').
				append("Your new temporary password is ").append(password).append(".").append('\n').append("Please change the password the first time you visit the site by clicking on the \"Change Password\" link under General at the top left of your USALive site.");
			params.put("content", content.toString());
			try {
				DataLayerMgr.executeQuery("SEND_EMAIL_TO_NEW_CUSTOMER", params);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (DataLayerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (DataLayerException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}
	
	public int getSaltSize() {
		return saltSize;
	}
		
}
