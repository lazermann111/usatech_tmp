package com.usatech.usalive.servlet;


import java.io.OutputStream;
import java.sql.Blob;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.io.IOUtils;
import simple.io.Log;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.steps.AbstractStep;
import simple.sql.ByteArrayBlob;

public class GetCompanyLogoStep extends AbstractStep {
	
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		Log log = Log.getLog();
		InputForm inputForm = RequestUtils.getInputForm(request);
		int customerId=ConvertUtils.getIntSafely(inputForm.get("customerId"), -1);
		try{
			if(customerId>0){
				Map<String, Object> params = new HashMap<String, Object>(8);
				DataLayerMgr.selectInto("GET_COMPANY_LOGO", new Object[]{USALiveUtils.companyLogoPrefix+customerId}, params);
				if(params.get("contentType")!=null){
					response.setContentType(ConvertUtils.getStringSafely(params.get("contentType")));
					response.setContentLength(ConvertUtils.getInt(params.get("length")));
					Object content=params.get("content");
					Blob blob;
					if(content instanceof Blob){
						blob = (Blob) content;
					}else if(content instanceof byte[]){
						blob = new ByteArrayBlob((byte[]) content);
					}else{
						blob = ConvertUtils.convert(Blob.class, content);
					}
					OutputStream outputS=null;
					try{
						outputS= response.getOutputStream();
						if(outputS!=null){
							IOUtils.copy(blob.getBinaryStream(), outputS);
							outputS.flush();
						}
					} catch(Exception e) {
						log.error("No image found with name="+inputForm.get("fileName"), e);
						throw new ServletException(e);
					}finally{
						if(outputS!=null){
							outputS.close();
						}
					}
				}
			}
		}catch(Exception e){
			throw new ServletException(e);
		}
	}
	
}
