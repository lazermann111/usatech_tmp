package com.usatech.usalive.servlet;

import java.sql.SQLException;
import java.util.Arrays;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.steps.AbstractStep;
import simple.xml.sax.MessagesInputSource;
/**
 * Updates the user-BankAcct privileges
 * 
 * @author Brian S. Krug
 *  
 */
public class UpdateUserBankAcctStep extends AbstractStep {
	private static final Log log = Log.getLog();
	public UpdateUserBankAcctStep() {
	}
	
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		long[] noneBankAcctIds;
		long[] viewBankAcctIds;
		try {
			noneBankAcctIds = ConvertUtils.convert(long[].class, form.get("none"));
			viewBankAcctIds = ConvertUtils.convert(long[].class, form.get("view"));
		} catch(ConvertException e) {
			throw new ServletException(e);
		}
		try {
			DataLayerMgr.executeCall("UPDATE_USER_BANK_ACCT_PRIVS", form, true);
		} catch(SQLException e) {
			throw new ServletException(e);
		} catch(DataLayerException e) {
			throw new ServletException(e);
		}		
		log.info("Removed privileges from " + Arrays.toString(noneBankAcctIds) + "; add View privilege to " + Arrays.toString(viewBankAcctIds));
		final MessagesInputSource mis = RequestUtils.getOrCreateMessagesSource(form);
		if(noneBankAcctIds != null && noneBankAcctIds.length > 0) {
			mis.addMessage("success", "user.bankacct.update.revoke", noneBankAcctIds.length); //("Removed privileges for " + noneBankAcctIds.length + " BankAccts");
		}
		if(viewBankAcctIds != null && viewBankAcctIds.length > 0) {
			mis.addMessage("success", "user.bankacct.update.view", viewBankAcctIds.length); //"Added view privilege for " + viewBankAcctIds.length + " BankAccts");
		}
		if(mis.getMessageCount() == 0)
			mis.addMessage("failed", "user.bankacct.update.none"); //"There were no updates to process");		
	}
}
