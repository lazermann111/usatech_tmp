package com.usatech.usalive.servlet.verizon;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;

/**
 * Validate that this request is from Verizon servers
 */
public class VerizonCallbackSecurityVerifier {
    public VerizonCallbackSecurityVerifier() {
    }

    public boolean isAllowedRequest(@NotNull HttpServletRequest req) {
        return true;
        // return req.isSecure() && isWhiteListedIP(req.getRemoteAddr());
    }

    public boolean isValidCredentials(@Nonnull String username, @Nonnull String password) {
        return false;
        //TODO: inject valid values from properties and test them
    }

    private boolean isWhiteListedIP(@NotNull String ip) {
        //TODO: STUB to allow local addresses only. Please implement real white list
    		// TODO: Move the Verizon's IP to a *.properties-file
        return ip.startsWith("10.") || ip.startsWith("192.") || "137.117.33.109".equals(ip);
    }
}
