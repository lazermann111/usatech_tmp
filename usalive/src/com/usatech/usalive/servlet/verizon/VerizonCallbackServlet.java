package com.usatech.usalive.servlet.verizon;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.annotation.Nonnull;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import simple.app.ServiceException;
import simple.io.Log;
import simple.rest.gson.GsonAnnotationExclusionStrategy;
import simple.service.modem.dao.generic.CallbackProcessor;
import simple.service.modem.dao.verizon.dto.activate.VerizonDeviceId;
import simple.service.modem.dao.verizon.dto.activate.VerizonDeviceKind;
import simple.service.modem.dao.verizon.dto.callback.VerizonCallbackResult;
import simple.service.modem.dao.verizon.dto.callback.VerizonDeviceResponse;
import simple.service.modem.service.dto.DeviceOperationResult;
import simple.service.modem.service.dto.ModemState;
import simple.service.modem.service.dto.OperationStatus;
import simple.service.modem.service.dto.USATDevice;
import simple.util.ClassSerializer;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.util.WebHelper;

/**
 * Servlet to processing responses results from Verizon modem API
 */
public class VerizonCallbackServlet extends HttpServlet implements CallbackProcessor<VerizonCallbackResult> {
    private static final long serialVersionUID = 6952203752912366461L;
    private final static Log log = Log.getLog();
    private VerizonCallbackSecurityVerifier securityVerifier = null;
    private final ClassSerializer<DeviceOperationResult[]> resultsSerializer = new ClassSerializer<>();
    private Gson gson;

    @Override
    public void init() throws ServletException {
        super.init();
        securityVerifier = new VerizonCallbackSecurityVerifier();
        gson = new GsonBuilder()
                .setExclusionStrategies(new GsonAnnotationExclusionStrategy())
                .create();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (securityVerifier.isAllowedRequest(req)) {
            if (!isContentTypeAllowed(req)) {
            		log.warn("Invalid content type, awaiting: application/json, got: " + req.getContentType());
                resp.sendError(HttpServletResponse.SC_BAD_REQUEST,
                        "Invalid content type, awaiting: application/json, got: " + req.getContentType());
                return;
            }
            VerizonCallbackResult result = process(req, resp);
            log.info(gson.toJson(result));

			if (result == null) {
				log.warn("Invalid request content");
				resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid request content");
				return;
			}

			if (result.getDeviceIds() == null) {
				log.warn("Invalid request content: no device IDs");
				resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid request content: no device IDs");
				return;
			}

            DeviceOperationResult[] results = new DeviceOperationResult[result.getDeviceIds().length];
            for (int i=0;i<results.length;i++) {
		          	results[i] = new DeviceOperationResult();
		          	results[i].setDevice(createDeviceFromVerizonCallbackResult(result));
		          	results[i].setOperationStatus(OperationStatus.COMPLETED);
		          	results[i].setRequestId(result.getRequestId());
		          	results[i].setModemState(getModemState(result.getDeviceResponse()));
		          	if (result.getFaultResponse() != null) {
				          	results[i].setErrorCode(result.getFaultResponse().getFaultCode());
				          	results[i].setErrorString(result.getFaultResponse().getFaultString());
		          	}
            }
            forwardModemsStatuses(results);
        } else {
            resp.sendError(HttpServletResponse.SC_FORBIDDEN);
            return;
        }
        resp.setContentType("text/html;charset=UTF-8");
        resp.setStatus(HttpServletResponse.SC_OK);
        PrintWriter out = resp.getWriter();
        try {
            out.write("OK");
        } finally {
            out.close();
        }
    }

		private USATDevice createDeviceFromVerizonCallbackResult(VerizonCallbackResult result) {
			USATDevice device = new USATDevice();
			for (VerizonDeviceId deviceId : result.getDeviceIds()) {
					if (deviceId.getKind() == VerizonDeviceKind.ICCID) {
						  device.setIccid(deviceId.getId());
						  device.setSerialCode(deviceId.getId());
					} else if (deviceId.getKind() == VerizonDeviceKind.MEID) {
						  device.setMeid(deviceId.getId());
						  device.setSerialCode(deviceId.getId());
					}
			}
			return device;
		}

		private ModemState getModemState(VerizonDeviceResponse deviceResponse) {
				if (deviceResponse != null) {
						if (deviceResponse.getActivateResponse() != null) {
								return ModemState.ACTIVE;
						}
						if (deviceResponse.getSuspendResponse() != null) {
								return ModemState.SUSPENDED;
						}
						if (deviceResponse.getResumeResponse() != null) {
								return ModemState.ACTIVE;
						}
						if (deviceResponse.getDeactivateResponse() != null) {
								return ModemState.DEACTIVATED;
						}
				}
				return null;
		}

		private void forwardModemsStatuses(DeviceOperationResult[] results) throws ServletException {
	      MessageChain mc = new MessageChainV11();
	      MessageChainStep resultProcessorStep = mc.addStep("usat.device.modem.result");
	      resultProcessorStep.setAttribute(CommonAttrEnum.ATTR_OBJECT, resultsSerializer.toBytes(results));
	      publishMessage(mc);
		}

		private void publishMessage(MessageChain mc) throws ServletException {
				try {
					MessageChainService.publish(mc, WebHelper.getDefaultPublisher());
				} catch (ServiceException serviceException) {
					throw new ServletException("Failed to send message", serviceException);
				}
		}

		private boolean isContentTypeAllowed(HttpServletRequest req) {
			if (req.getContentType() != null &&
					req.getContentType().contains("application/json")) {
				return true;
			}
			return false;
		}

    @Override
    public void destroy() {
        super.destroy();
    }

    @Override
    @Nonnull
    public VerizonCallbackResult process(@Nonnull HttpServletRequest req, @Nonnull HttpServletResponse resp)
            throws ServletException, IOException {
        StringBuilder callbackJson = new StringBuilder("");
        BufferedReader in = req.getReader();
        try {
            String line;
            while ((line = in.readLine()) != null) {
                callbackJson.append(line).append("\n");
            }
        } finally {
            in.close();
        }
        log.info("Got this from Verizon: " + callbackJson.toString());
        return gson.fromJson(callbackJson.toString(), VerizonCallbackResult.class);
    }
}
