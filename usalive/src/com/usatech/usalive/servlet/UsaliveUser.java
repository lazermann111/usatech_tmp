/*
 * User.java
 *
 * Created on November 1, 2001, 5:06 PM
 */

package com.usatech.usalive.servlet;

/**
 *
 * @author  bkrug
 * @version
 */
import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.servlet.ServletException;

import com.usatech.report.ReportingUser;
import com.usatech.usalive.report.ResultCacheKey;

import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.lang.Initializer;
import simple.results.Results;

public class UsaliveUser extends ReportingUser {
	private static final long serialVersionUID = 6786616439697722350L;

	protected int ajaxPollCount=0;
	protected UsaliveUser masterUser;
	protected int requestDelayUntilOld = 15000;
	protected final ConcurrentMap<ResultCacheKey, UserRequest> userRequests = new ConcurrentHashMap<ResultCacheKey, UserRequest>();
	protected final Collection<UserRequest> userRequestsUnmod = Collections.unmodifiableCollection(userRequests.values());
	protected final Initializer<ServletException> initializer = new Initializer<ServletException>() {
		@Override
		protected void doInitialize() throws ServletException {
			// Get from database
			try {
				Results results = DataLayerMgr.executeQuery("GET_REPORT_READY_BY_USER", UsaliveUser.this);
				while(results.next()) {
					Long fileId = results.getValue("fileId", Long.class);
					int pageId = results.getValue("pageId", Integer.class).intValue();
					UserRequest userRequest = new UserRequest(results.getValue("requestId", Long.class), pageId, results.getValue("profileId", Long.class), UserRequestStatus.PENDING);
					if(fileId != null && fileId != 0)
						userRequest.statusNew(getRequestDelayUntilOld());
					if(pageId==0){
						userRequests.put(new ResultCacheKey(userRequest.getRequestId(), pageId), userRequest);
					}
				}
			} catch(ConvertException e) {
				throw new ServletException(e);
			} catch(SQLException e) {
				throw new ServletException(e);
			} catch(DataLayerException e) {
				throw new ServletException(e);
			}
		}
		
		@Override
		protected void doReset() {
			userRequests.clear();
		}
	};
	    
	/** Creates new User */
	public UsaliveUser() {
	}
	
	public boolean addRequest(long requestId, int pageId, long profileId) throws InterruptedException, ServletException {
		initializer.initialize();
		UserRequest old = userRequests.putIfAbsent(new ResultCacheKey(requestId, pageId), new UserRequest(requestId, pageId, profileId, UserRequestStatus.PENDING));
		if(old != null) {
			old.statusReset();
			return false;
		}
		return true;
	}

	/**
	 * @param requestId
	 * @param pageId
	 * @throws InterruptedException
	 * @throws ServletException
	 */
	public void removeRequest(long requestId, int pageId) throws InterruptedException, ServletException {
		userRequests.remove(new ResultCacheKey(requestId, pageId));
	}
	public boolean notifyRequest(long requestId, int pageId, long profileId)  throws InterruptedException, ServletException {
		initializer.initialize();
		UserRequest old = userRequests.putIfAbsent(new ResultCacheKey(requestId,pageId), new UserRequest(requestId, pageId, profileId, UserRequestStatus.PENDING_NOTIFY));
		if(old != null) {
			old.statusNotify(getRequestDelayUntilOld());
			return true;
		}
		return true;
	}
	public boolean readiedRequest(long requestId, int pageId, long profileId) throws InterruptedException, ServletException {
		initializer.initialize();
		UserRequest tmpUserRequest = new UserRequest(requestId, pageId, profileId, UserRequestStatus.PENDING);
		UserRequest tmpUserRequest2 = userRequests.putIfAbsent(new ResultCacheKey(requestId, pageId), tmpUserRequest);
		final UserRequest realUserRequest = (tmpUserRequest2 == null ? tmpUserRequest : tmpUserRequest2);
		return realUserRequest.statusNew(getRequestDelayUntilOld());
	}
	public boolean pulledRequest(long requestId, int pageId) throws ServletException {
		if(userRequests.remove(new ResultCacheKey(requestId, pageId)) != null||pageId>0) {
			//Mark as pulled
			Map<String,Object> params = new HashMap<String,Object>();
			params.put("masterUserId", getMasterUserId());
			params.put("requestId", requestId);
			params.put("pageId", pageId);
			try {
				DataLayerMgr.executeCall("REPORT_REQUEST_USER_UPD", params, true);
			} catch(SQLException e) {
				throw new ServletException(e);
			} catch(DataLayerException e) {
				throw new ServletException(e);
			}		
			return true;
		}
		return false;
	}
	public boolean refreshedRequest(long requestId) {
		return (userRequests.remove(new ResultCacheKey(requestId,0)) != null);
	}
	public Collection<UserRequest> getUserRequests() throws InterruptedException, ServletException {
		initializer.initialize();
		return userRequestsUnmod;
	}

	public UsaliveUser getMasterUser() {
		return masterUser;
	}

	public void setMasterUser(UsaliveUser masterUser) {
		this.masterUser = masterUser;
	}

	public int getRequestDelayUntilOld() {
		return requestDelayUntilOld;
	}

	public void setRequestDelayUntilOld(int requestDelayUntilOld) {
		this.requestDelayUntilOld = requestDelayUntilOld;
	}

	public int getAjaxPollCount() {
		return ajaxPollCount;
	}
	
	public void incrementAjaxPollCount(){
		ajaxPollCount++;
	}

	public void setAjaxPollCount(int ajaxPollCount) {
		this.ajaxPollCount = ajaxPollCount;
	}
}