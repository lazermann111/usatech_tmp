package com.usatech.usalive.servlet;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;
import simple.servlet.steps.LoginFailureException;
/**
 * Updates the transport properties
 * 
 * @author Brian S. Krug
 *  
 */
public class UnBeUserStep extends AbstractStep {
	//private static final Log log = Log.getLog();
	
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		UsaliveUser user;
		try {
			user = RequestUtils.getAttribute(request, SimpleServlet.ATTRIBUTE_USER, UsaliveUser.class, true, "session");
		} catch(ConvertException e) {
			throw new ServletException(e);
		}

		UsaliveUser newUser = new UsaliveUser();
		newUser.setUserId(user.getMasterUserId());
		newUser.setMasterUserId(user.getMasterUserId());
		newUser.setTimeZone(user.getTimeZone());
		try{
			newUser.setInternal(ConvertUtils.getBoolean(form.getAttribute("isOriginalUserInternal", RequestUtils.SESSION_SCOPE), false));
		}catch(ConvertException e){
			throw new ServletException(e);
		}
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("user", newUser);
		params.put("profileId", user.getMasterUserId());
		
		try {
			DataLayerMgr.executeCall("LOGIN_AS_USER", params, true);
		} catch(SQLException e) {
			if(e.getErrorCode() == 20100) {
				throw new LoginFailureException("You may not log out to requested user (" + user.getMasterUserId() + ")", LoginFailureException.INVALID_CREDENTIALS);
			}
			throw new ServletException(e);
		} catch(DataLayerException e) {
			throw new ServletException(e);
		} 
		LoginStep.userLoggedIn(user.getUserName(), newUser, form);   
		RequestUtils.setAttribute(request, SimpleServlet.ATTRIBUTE_USER, newUser, "session");
	}
}
