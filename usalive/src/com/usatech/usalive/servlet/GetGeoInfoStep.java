package com.usatech.usalive.servlet;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.NoSuchElementException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.HttpException;

import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.NotEnoughRowsException;
import simple.results.BeanException;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;
import simple.tools.GeoInfoLookup;
import simple.tools.GeoInfoLookup.GeoInfo;

public class GetGeoInfoStep extends AbstractStep {
	protected GeoInfoLookup gil = new GeoInfoLookup();

    public GetGeoInfoStep() {
    }

	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request,
			HttpServletResponse response) throws ServletException {
		try {
			try {
				DataLayerMgr.selectInto("GET_GEO_INFO", form);
			} catch(NotEnoughRowsException e) {
				GeoInfo gi = gil.lookupGeoInfo(form.getString("postalCd", true), form.getString("countryCd", true));
				form.set("geoInfo", gi);
				DataLayerMgr.executeCall("ADD_GEO_INFO", form, true);
			}
		} catch(NoSuchElementException e) {
			throw new ServletException(e);
		} catch(HttpException e) {
			throw new ServletException(e);
		} catch(SecurityException e) {
			throw new ServletException(e);
		} catch(IllegalArgumentException e) {
			throw new ServletException(e);
		} catch(IOException e) {
			throw new ServletException(e);
		} catch(NoSuchMethodException e) {
			throw new ServletException(e);
		} catch(IllegalAccessException e) {
			throw new ServletException(e);
		} catch(InvocationTargetException e) {
			throw new ServletException(e);
		} catch(SQLException e) {
			throw new ServletException(e);
		} catch(DataLayerException e) {
			throw new ServletException(e);
		} catch(BeanException e) {
			throw new ServletException(e);
		}
	}
}
