package com.usatech.usalive.servlet;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConditionUtils;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.DbUtils;
import simple.io.Log;
import simple.security.SecurityUtils;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.SimpleServlet;
import simple.text.Censor;
import simple.text.Hasher;

import com.usatech.layers.common.fees.LicenseFees;
import com.usatech.report.custom.GenerateUtils;

public class CreateCustomerStep extends LoginStep 
{
	private static final Log log = Log.getLog();
	
	public CreateCustomerStep() throws NoSuchAlgorithmException 
	{
		super();
	}

	protected final Hasher hasher = new Hasher("SHA1", true);
	protected final Random random = new Random();
	protected int saltSize = 8;
		
	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException 
	{
		String userName = form.getString("newUserName", true);
		String password = "";
		
		boolean valid = true;
		if (!ConditionUtils.check(userName, "NOT MATCH", ".*@usatech\\.com")) {
			valid = false;
			RequestUtils.getOrCreateMessagesSource(form).addMessage("error", null, "You may not create a user with a User Name that includes '@usatech.com'. Please enter a different User Name.");
		}
		
		String email = form.getString("email", true);
		if (!ConditionUtils.check(email, "EMAIL", null)) {
			valid = false;
			RequestUtils.getOrCreateMessagesSource(form).addMessage("error", null, "Invalid email. Please enter a valid email address.");
		}
		
		if (!valid) {
			dispatcher.dispatch("new_customer", false);
			return;
		}
		
		UsaliveUser creatingUser = (UsaliveUser) RequestUtils.getUser(request);
		
		// we should really do additional validation here and not depend on client-side validation
		
		String customerName = form.getString("customerName", true);

		UsaliveUser newUser = null;
		
		boolean success = false;
		Connection connection = null;
		
		try {
			GenerateUtils.formatPostal(null, form);
			form.setAttribute("timeZone", RequestUtils.getTimeZone(form));
			form.set("doingBusinessAs", Censor.sanitizeText(form.getStringSafely("doingBusinessAs", "").trim()));
			
			try {
				connection = DataLayerMgr.getConnection("report", false);
			} catch (Exception e) {
				log.warn("failed to get a database connection to report", e);
				throw e;
			}
			

			if (creatingUser.isPartner()) {
				form.set("parentCustomerId", creatingUser.getCustomerId());
			
				long licenseId = -1;
				LicenseFees fees = LicenseFees.loadByCustomerId(creatingUser.getCustomerId());
				if (creatingUser.isReseller()) {
					fees.updateFromForm(form, false);
						
					try {
						licenseId = fees.createChildLicense(connection, creatingUser.getCustomerName(), customerName);
					} catch (Exception e) {
						log.warn("createChildLicense failed", e);
						throw e;
					}
				} else {
					// use  the license of the partner
					licenseId = fees.getLicenseId();
				}
				
				form.set("licenseId", licenseId);
			}
			
			// if not a partner then license will be selected on the page
			
			form.setAttribute("username", userName);
			
			Object[] createCustomerResults = null;
			try {
				password = SecurityUtils.generatePassword();
				byte[] salt = new byte[getSaltSize()];
				random.nextBytes(salt); 
				byte[] hash = hasher.hashRaw(password.getBytes(), salt);
				form.setAttribute("passwordHash", hash);
				form.setAttribute("passwordSalt", salt);
				createCustomerResults = DataLayerMgr.executeCall(connection, "CREATE_CUSTOMER", form, false);
				
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("email", form.get("email"));
				params.put("name", form.get("firstName") + " " + form.get("lastName"));
				params.put("subject", "Your customized USA Live website is setup.");
				StringBuilder content = new StringBuilder();
				content.append(form.get("firstName")).append(" - Your customized USA Live website is setup and ready for you.").append('\n').
					append("To access the USALive website, please visit this address or click here - https://usalive.usatech.com").append('\n').
					append("Your username is ").append(form.get("email")).append(".").append('\n').
					append("Your temporary password is ").append(password).append(".").append('\n').append("Please change the password the first time you visit the site by clicking on the \"Change Password\" link under General at the top left of your USALive site.");
				params.put("content", content.toString());
				DataLayerMgr.executeQuery("SEND_EMAIL_TO_NEW_CUSTOMER", params);
				
				long sproutVendorId=form.getLong("sproutVendorId", false, -1);
				if(sproutVendorId>0){
					try{
						DataLayerMgr.executeCall(connection, "INSERT_SPROUT_VENDOR", new Object[]{sproutVendorId,createCustomerResults[2]}, false);
					}catch(SQLException e){
						RequestUtils.getOrCreateMessagesSource(form).addMessage("error", "invalid-sprout-vendor-id","Invalid sprout vendor id");
						log.error("Invalid sprout vendor id ''{0}''", form.get("sproutVendorId"),e);
						throw e;
					}
				}
				newUser = new UsaliveUser();
				newUser.setUserId(ConvertUtils.getLong(createCustomerResults[1]));
				newUser.setCustomerId(ConvertUtils.getInt(createCustomerResults[2]));
				
				form.set(SimpleServlet.ATTRIBUTE_USER, newUser);
				DataLayerMgr.executeUpdate(connection, "UPDATE_CUSTOMER_SERVICE_DBA", form); // TODO: merge into CREATE_CUSTOMER proc
				
				success = true;
			} catch(SQLException e) {
				switch(e.getErrorCode()) {
					case 20100:
						RequestUtils.getOrCreateMessagesSource(form).addMessage("error", "invalid-dealer-not-unique","Program is invalid or not unique. Please choose another name or contact USA Technologies to resolve.");
						break;
					case 100:
					case 1:
						DataLayerMgr.selectInto("CHECK_CUSTOMER_NAME", form);
						if(form.getBoolean("customerNameExists", true, true)){
							RequestUtils.getOrCreateMessagesSource(form).addMessage("error", "customer-name-taken","Customer name already exists. Please choose another name or contact USA Technologies to resolve.");
						} else {
							RequestUtils.getOrCreateMessagesSource(form).addMessage("error", "user-name-taken", "User name already exists. Please choose another name or contact USA Technologies to resolve.");
						}
						break;
					default:
						throw e;
				}
			}
		} catch(Throwable e) {
			log.warn("Could not create customer ''{0}''", customerName, e);
			if (e instanceof SQLException)	{
				switch(((SQLException) e).getErrorCode()) {  
					case 20200:
						RequestUtils.getOrCreateMessagesSource(form).addMessage("error", null, "An error occurred on the server during sub-customer creating. Please check parent license and try again.");
						break;
					default:
						RequestUtils.getOrCreateMessagesSource(form).addMessage("error", null, "An error occurred on the server. We are working to resolve this. Please try again later.");
				}
			} else {
				RequestUtils.getOrCreateMessagesSource(form).addMessage("error", null, "An error occurred on the server. We are working to resolve this. Please try again later.");
			}
		} finally {
			if(connection != null) {
				try {
					DbUtils.commitOrRollback(connection, success, true);
				} catch(SQLException e) {
					log.warn("final {0} failed", (success ? "commit" : "rollback"), e);
				}
			}
		}
		
		if (!success) {
			dispatcher.dispatch("new_customer", false);
		} else {
			if (creatingUser.isPartner()) {
				form.setAttribute("partner-new-user", newUser, RequestUtils.SESSION_SCOPE);
				RequestUtils.getOrCreateMessagesSource(form).addMessage("success", "create-customer-success-partner", "Customer \"{0}\" has been registered in USALive. Create a bank account for the customer to complete the setup.", customerName);
				String forward = String.format("new_bank_acct.i?customerId=%s", newUser.getCustomerId());
				try {
					response.sendRedirect(forward);
				} catch (Exception e) {
					log.debug("sendRedirect to {0} failed, trying forward", forward, e);
					dispatcher.forward(forward);
				}
			} else { 
				RequestUtils.getOrCreateMessagesSource(form).addMessage("success", "create-customer-success-cs", "Customer \"{0}\" has been registered in USALive and you are now logged in as the primary user.", customerName);
				//form.setAttribute("simple.servlet.steps.LogonStep.forward", "license_instructions.i");
				form.setAttribute("password", password);
				super.perform(dispatcher, form, request, response);
			}
		}
	}
	
	public int getSaltSize() {
		return saltSize;
	}
		
}
