package com.usatech.usalive.servlet;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.security.auth.Subject;
import javax.servlet.ServletRequest;

import org.eclipse.jetty.jaas.JAASPrincipal;
import org.eclipse.jetty.security.DefaultIdentityService;
import org.eclipse.jetty.security.IdentityService;
import org.eclipse.jetty.security.LoginService;
import org.eclipse.jetty.server.UserIdentity;
import org.eclipse.jetty.util.component.AbstractLifeCycle;

import simple.io.Log;
import simple.servlet.InputForm;
import simple.servlet.MapInputForm;

public class USALiveLoginService extends AbstractLifeCycle implements LoginService
{
	private static final Log log = Log.getLog();
	
    protected String _realmName;
    protected String _loginModuleName;
    protected IdentityService _identityService;
	protected final LoginStep loginStep;

	public USALiveLoginService() throws NoSuchAlgorithmException {
		loginStep = new LoginStep();
    }
    
	public USALiveLoginService(String name) throws NoSuchAlgorithmException {
        this();
        _realmName = name;
        _loginModuleName = name;
    }

    /**
     * Get the name of the realm.
     *
     * @return name or null if not set.
     */
    public String getName()
    {
        return _realmName;
    }

    /**
     * Set the name of the realm
     *
     * @param name a <code>String</code> value
     */
    public void setName (String name)
    {
        _realmName = name;
    }

    /** Get the identityService.
     * @return the identityService
     */
    public IdentityService getIdentityService()
    {
        return _identityService;
    }

    /** Set the identityService.
     * @param identityService the identityService to set
     */
    public void setIdentityService(IdentityService identityService)
    {
        _identityService = identityService;
    }

    /**
     * Set the name to use to index into the config
     * file of LoginModules.
     *
     * @param name a <code>String</code> value
     */
    public void setLoginModuleName (String name)
    {
        _loginModuleName = name;
    }

    /**
     * @see org.eclipse.jetty.util.component.AbstractLifeCycle#doStart()
     */
    protected void doStart() throws Exception
    {
        if (_identityService == null)
            _identityService = new DefaultIdentityService();
        super.doStart();
    }

    public UserIdentity login(final String username, final Object credentials)
    {
    	try {
    		Map<String, Object> map = new HashMap<String, Object>();
    		map.put("username", username);
    		map.put("password", credentials.toString());
    		InputForm form = new MapInputForm(map);
    		UsaliveUser user = (UsaliveUser) loginStep.authenticateUser(form);
    		
    		long[] groupIds = user.getUserGroupIds();
    		Arrays.sort(groupIds);
    		
			List<String> rolesList = new ArrayList<String>();
    		rolesList.add("user");
    		if (Arrays.binarySearch(user.getUserGroupIds(), 29L) >= 0)
    			rolesList.add("partner");
    		if (Arrays.binarySearch(user.getUserGroupIds(), 7L) >= 0)
    			rolesList.add("developer");
    		if (Arrays.binarySearch(user.getUserGroupIds(), 1L) >= 0 || Arrays.binarySearch(user.getUserGroupIds(), 3L) >= 0)
    			rolesList.add("admin");
    		String[] roles = new String[rolesList.size()];
    		rolesList.toArray(roles);
    		
    		JAASPrincipal userPrincipal = new JAASPrincipal(user.getUserName());
    		Subject subject = new Subject();
        subject.getPrincipals().add(userPrincipal);
        return _identityService.newUserIdentity(subject, userPrincipal, roles);
    	} catch (Exception e) {
    		log.warn(e);
    	}
    	return null;
    }

    public boolean validate(UserIdentity user)
    {
        return true;
    }

    public void logout(UserIdentity user)
    {
    }

	@Override
	public UserIdentity login(String username, Object credentials, ServletRequest request) {
		return login(username, credentials);
	}
}
