package com.usatech.usalive.servlet;

import java.sql.SQLException;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.lang.InvalidStringValueException;
import simple.results.BeanException;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.steps.AbstractStep;
import simple.text.StringUtils;
/**
 * Updates the transport properties
 * 
 * @author Brian S. Krug
 *  
 */
public class CheckUsernameStep extends AbstractStep {
	protected Pattern internalUsernamePattern = LoginStep.DEFAULT_INTERNAL_USERNAME_PATTERN;
	protected boolean existingUser = true;
	
	public boolean isExistingUser() {
		return existingUser;
	}

	public void setExistingUser(boolean existingUser) {
		this.existingUser = existingUser;
	}

	public Pattern getInternalUsernamePattern() {
		return internalUsernamePattern;
	}

	public void setInternalUsernamePattern(Pattern internalUsernamePattern) {
		this.internalUsernamePattern = internalUsernamePattern;
	}

	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String username = form.getString("editUsername", true);
		if(isExistingUser()) {
			try {
				DataLayerMgr.selectInto("GET_USER_TYPE", form);
			} catch (SQLException e) {
				throw new ServletException(e);
			} catch (DataLayerException e) {
				throw new ServletException(e);
			} catch (BeanException e) {
				throw new ServletException(e);
			}
		}		
		long editUserId = form.getLong("editUserId", false, -1);
		UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
		if (user != null && editUserId == user.getUserId()) {
			USALiveUtils.clearLinks(form, "M");
			if (!StringUtils.isBlank(form.getStringSafely("privs", ""))) {
				user.clearPrivileges();
				user.addPrivileges(form.getStringArray("privs", true));
			}
		}
		int userType = form.getInt("userType", true, 0);
		boolean internalUsername = getInternalUsernamePattern().matcher(username).matches();
		boolean internalUserType = (userType != 8);
		if(internalUsername && !internalUserType)
			handleException(form, "This username '" + username + "' is reserved for internal use only. Please choose another.", username);
		if(!internalUsername && internalUserType)
			handleException(form, "This username '" + username + "' is not allowed for an internal user. Please choose another.", username);
	}
	
	protected void handleException(InputForm form, String message, String value) throws ServletException {
		form.setAttribute("messageClass", "status-info-failure");
		form.setAttribute("messageText", message);
		throw new ServletException(new InvalidStringValueException(message, value));
	}
}
