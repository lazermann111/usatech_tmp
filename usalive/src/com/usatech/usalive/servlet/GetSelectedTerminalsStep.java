package com.usatech.usalive.servlet;

import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usatech.report.ReportingUser;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.steps.AbstractStep;

public class GetSelectedTerminalsStep extends AbstractStep {
	
    public GetSelectedTerminalsStep() {
    }

    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
    	try {
	    	Map<String, Map<String, Set<Long>>> selectedItems = new HashMap<String, Map<String, Set<Long>>>();
			Object tmp = form.getAttribute("params.customerIds");
			if(tmp == null)
				tmp = form.getAttribute("customerId");
	    	if(tmp != null) {
		    	Iterable<?> customers = ConvertUtils.asIterable(tmp);
		    	if(customers != null)
			    	for(Object customer : customers) {
			    		String customerId = ConvertUtils.getString(customer, false);
			    		if(customerId != null && (customerId=customerId.trim()).length() > 0) {
			    			Map<String, Set<Long>> regionMap = selectedItems.get(customerId);
			    			if(regionMap == null) {
			    				regionMap = new HashMap<String, Set<Long>>();
			    				selectedItems.put(customerId, regionMap);
			    			}
			    		}
		    		}
	    		}
	    	tmp = form.getAttribute("customerId_regionId");
	    	if(tmp != null) {
		    	Iterable<?> regions = ConvertUtils.asIterable(tmp);
		    	if(regions != null)
			    	for(Object region : regions) {
			    		String regionStr = ConvertUtils.getString(region, false);
			    		int p;
			    		if(regionStr != null && (p=(regionStr=regionStr.trim()).indexOf('_')) > 0) {
			    			String customerId = regionStr.substring(0, p).trim();
				    		Map<String, Set<Long>> regionMap = selectedItems.get(customerId);
			    			if(regionMap == null) {
			    				regionMap = new HashMap<String, Set<Long>>();
			    				selectedItems.put(customerId, regionMap);
			    			}
			    			Set<Long> terminals = regionMap.get(regionStr);
			    			if(terminals == null) {
			    				terminals = new HashSet<Long>();
			    				regionMap.put(regionStr, terminals);
			    			}
			    		}
			    	}
	    	}
			tmp = form.get("params.terminalIds");
			if(tmp == null)
				tmp = form.get("terminals");
			if(tmp != null) {
				Results results = DataLayerMgr.executeQuery("GET_SELECTED_LOCATIONS", Collections.singletonMap("terminals", tmp));
	    		while(results.next()) {
	    			String customerId = results.getValue("customerId", String.class);
	    			String regionId = results.getValue("regionId", String.class);
	    			String regionStr = customerId + '_' + regionId;
	    			long terminalId = results.getValue("terminalId", Long.class);
	    			Map<String, Set<Long>> regionMap = selectedItems.get(customerId);
	    			if(regionMap == null) {
	    				regionMap = new HashMap<String, Set<Long>>();
	    				selectedItems.put(customerId, regionMap);
	    			}
	    			Set<Long> terminals = regionMap.get(regionStr);
	    			if(terminals == null) {
	    				terminals = new HashSet<Long>();
	    				regionMap.put(regionStr, terminals);
	    			}
	    			terminals.add(terminalId);
	    		}
	    	}
			if(selectedItems.isEmpty()) {
				ReportingUser user = (ReportingUser) RequestUtils.getUser(request);
				if(!user.isInternal() && user.getCustomerId() > 0) {
					Map<String, Set<Long>> regionMap = Collections.emptyMap();
					selectedItems.put(String.valueOf(user.getCustomerId()), regionMap);
				}
			}
	    	form.setAttribute("selectedItems", selectedItems);
	    } catch(ConvertException e) {
    		throw new ServletException(e);
    	} catch(SQLException e) {
    		throw new ServletException(e);
		} catch(DataLayerException e) {
    		throw new ServletException(e);
		}
    }
}
