package com.usatech.usalive.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usatech.report.ReportingUser;
import com.usatech.report.ReportingUser.ActionType;
import com.usatech.report.ReportingUser.ResourceType;

import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.NotEnoughRowsException;
import simple.io.Log;
import simple.results.BeanException;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputFile;
import simple.servlet.InputForm;
import simple.servlet.NotAuthorizedException;
import simple.servlet.RequestUtils;
import simple.servlet.steps.AbstractStep;
import simple.translator.Translator;
import simple.xml.sax.MessagesInputSource;

public class RemoteFileUploadStep extends AbstractStep {
	private static final Log log = Log.getLog();

    public RemoteFileUploadStep() {
    }

	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request,
			HttpServletResponse response) throws ServletException {
		Translator translator = form.getTranslator();
		final MessagesInputSource mis = RequestUtils.getOrCreateMessagesSource(form);
		// Get the file type, the file and list of devices
        try {
			//int fileTypeId = form.getInt("fileTypeId", true, 0);
			try {
				DataLayerMgr.selectInto("GET_REMOTE_FILE_TYPE_INFO", form);
			} catch(NotEnoughRowsException e) {
				mis.addMessage("error", "device.remoteupload.error.invalid.filetype", form.getInt("fileTypeId", true, 0));
				return;
			} catch(SQLException e) {
				throw new ServletException(e);
			} catch(DataLayerException e) {
				throw new ServletException(e);
			} catch(BeanException e) {
				throw new ServletException(e);
			}
			String fileTypePath = form.getString("fileTypePath", true);
			int byteLimit = form.getInt("byteLimit", true, 0);

			InputFile inputFile = (InputFile)form.getAttribute("file");
			if(inputFile == null) throw new ServletException("The file to upload was not specified in the request");
			if(byteLimit > 0 && inputFile.getLength() > byteLimit) {
				mis.addMessage("error", "device.remoteupload.error.toobig", byteLimit, inputFile.getLength());
				return;
			}
			Results results;
			try {
				results = DataLayerMgr.executeQuery("GET_CURRENT_DEVICES", form);
			} catch(SQLException e) {
				throw new ServletException(e);
			} catch(DataLayerException e) {
				throw new ServletException(e);
			}
			if(!results.next()) {
				mis.addMessage("error", "device.remoteupload.error.nodevices");
				return;
			}

			// First upload the file
			UsaliveUser user = (UsaliveUser) RequestUtils.getUser(form);

			//New way (directly)
			Map<String,Object> params = new HashMap<String, Object>();
			params.put("fileName", fileTypePath);
			params.put("fileType", 16);
			params.put("fileComment", "Remote File Upload requested by '" + user.getUserName() + "' from USALive");
			params.put("fileContent", inputFile.getInputStream());
			Connection conn = DataLayerMgr.getConnectionForCall("ADD_FILE_TRANSFER");
			try {
				DataLayerMgr.executeCall(conn, "ADD_FILE_TRANSFER", params);

				//For each device schedule the upload
				do {
					try {
						int terminalId = results.getValue("terminalId", Integer.class);
						String terminalDesc = results.getValue("terminalDesc", String.class);
						try {
							user.checkPermission(ReportingUser.ResourceType.TERMINAL, ReportingUser.ActionType.EDIT, terminalId);
						} catch(NotAuthorizedException e) {
							mis.addMessage("error", "device.remoteupload.schedule.notpermitted", "User ''{1}'' is not permitted to upload files to {0}", terminalDesc, user.getUserName());
						} catch(ServletException e) {
							mis.addMessage("error", "device.remoteupload.error.sql", e);
							log.info("Could not upload file for device remote file upload", e);
						}
						String deviceSerialCd = results.getValue("deviceSerialNum", String.class);
						params.put("deviceSerialCd", deviceSerialCd);
						DataLayerMgr.executeUpdate(conn, "ADD_PENDING_FILE_TRANSFER", params);
						mis.addMessage("info", "device.remoteupload.schedule.success", "Successfully scheduled upload of file to {0}", terminalDesc);
					} catch(ConvertException e) {
						mis.addMessage("error", "device.remoteupload.error.conversion", e);
						log.info("Could not schedule device remote file upload", e);
					} catch(SQLException e) {
						mis.addMessage("error", "device.remoteupload.error.sql", e);
						log.info("Could not schedule device remote file upload", e);
					} catch(DataLayerException e) {
						mis.addMessage("error", "device.remoteupload.error.sql", e);
						log.info("Could not upload file for device remote file upload", e);
					}
				} while(results.next()) ;
				conn.commit();
			} catch (SQLException e) {
				mis.addMessage("error", "device.remoteupload.error.sql", e);
				log.info("Could not upload file for device remote file upload", e);
				try { conn.rollback(); } catch(SQLException e1) { log.info("Could not rollback", e1); }
			} catch(DataLayerException e) {
				mis.addMessage("error", "device.remoteupload.error.sql", e);
				log.info("Could not upload file for device remote file upload", e);
				try { conn.rollback(); } catch(SQLException e1) { log.info("Could not rollback", e1); }
			}
        } catch(IOException e) {
        	throw new ServletException(e);
		} catch(SQLException e) {
			throw new ServletException(e);
		} catch(DataLayerException e) {
			throw new ServletException(e);
		}
	}
}
