package com.usatech.usalive.servlet;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.steps.AbstractStep;
import simple.text.ThreadSafeDateFormat;

import com.usatech.layers.common.model.PayrollSchedule;
import com.usatech.layers.common.model.PayrollSchedule.PayPeriod;

public class SetPayrollScheduleDatesStep extends AbstractStep {
	
	private static ThreadSafeDateFormat formatter = new ThreadSafeDateFormat(new SimpleDateFormat("MM/dd/YYYY"));
	
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		UsaliveUser user = (UsaliveUser) RequestUtils.getUser(request);
		if (user == null)
			return;
		
		try {
			PayrollSchedule payrollSchedule = PayrollSchedule.loadByUserId(user.getUserId());
			if (payrollSchedule == null || payrollSchedule.isNone())
				return;
			
			payrollSchedule.setCurrentDate(LocalDate.now().plus(Period.ofDays(1)));
			PayPeriod payPeriod = payrollSchedule.getPrevPayPeriod();
			
			form.setAttribute("defaultStartDate", formatter.format(payPeriod.getStartDate()));
			form.setAttribute("defaultEndDate", formatter.format(payPeriod.getEndDate()));
		} catch(Exception e) {
			throw new ServletException(e);
		}
	}
}
