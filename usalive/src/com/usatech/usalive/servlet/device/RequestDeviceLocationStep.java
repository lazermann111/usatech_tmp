package com.usatech.usalive.servlet.device;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.device.LocationAttrEnum;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.util.WebHelper;
import com.usatech.usalive.device.api.DeviceLocationDao;
import com.usatech.usalive.device.dto.DeviceLocation;
import com.usatech.usalive.device.impl.DeviceLocationDbDaoImpl;
import com.usatech.usalive.servlet.USALiveUtils;

import simple.app.ServiceException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;
import simple.service.modem.dao.verizon.dto.location.IdKind;
import simple.service.modem.dao.verizon.dto.location.VerizonDeviceLocationId;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.JsonStep;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Controller to serve JSON AJAX requests from "device information" web page
 */
public class RequestDeviceLocationStep extends JsonStep {
	static final Log log = Log.getLog();
	private DeviceLocationDao deviceLocationDao;
	private static long minRequestPeriodMinutes;
	private static String googleMapsApiKey;

	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response)
			throws ServletException {

		setupJsonStep();
		long deviceId = extractDeviceId(form);
		DeviceLocation dl = getDeviceLocationById(deviceId);
		if (dl != null) {
			dl.setUpdateRestricted(!isUpdateAllowed(dl));
			if (!dl.isUpdateRestricted()) {
				dl.setRequestedAt(new Date());
				publishLocationUpdateTask(deviceId);
			}
		}
		dl.setGooleMapUrl(hasGeoInfo(dl) ? fixAndBuildGoogleMapUrl(dl) : "");
		form.setAttribute("deviceNetworkLocation", dl);
		super.perform(dispatcher, form, request, response);
	}

	private String fixAndBuildGoogleMapUrl(DeviceLocation dl) {
		// NOTICE It's workaround for ajaxe request
		String ret = USALiveUtils.buildGoogleMapAreaUrl(dl.getLatitude(), dl.getLongitude(), googleMapsApiKey);
		if (ret == null) {
			return null;
		}
		return ret.replaceAll("amp;", "");
	}

	private boolean hasGeoInfo(DeviceLocation dl) {
		if (dl == null) {
			return false;
		}

		if (dl.getLatitude() == null || dl.getLongitude() == null) {
			return false;
		}

		return true;
	}

	private boolean isUpdateAllowed(DeviceLocation dl) {
		if (dl.getRequestedAt() != null) {
			return false;
		}

		if (dl.getUpdatedAt() == null) {
			return true;
		}

		return dl.getUpdatedAt().getTime() + TimeUnit.MINUTES.toMillis(getMinRequestPeriodMinutes()) < System
				.currentTimeMillis();
	}

	private DeviceLocation getDeviceLocationById(long deviceId) {
		return getDeviceLocationDao().getLatestDeviceLocation(deviceId);
	}

	public void publishLocationUpdateTask(long deviceId) throws ServletException {
		MessageChain mc = new MessageChainV11();
		
		Long deviceIccId = getIccidByDeviceIdForEsEye(deviceId);
		if (deviceIccId != null) {
			MessageChainStep esEyeStep = mc.addStep("usat.location.network.eseye");
			esEyeStep.setAttribute(CommonAttrEnum.ATTR_ITEM_ID, deviceIccId);

			MessageChainStep updateStep = mc.addStep("usat.location.update");
			updateStep.setReferenceAttribute(CommonAttrEnum.ATTR_ITEM_ID, esEyeStep, CommonAttrEnum.ATTR_ITEM_ID);
			updateStep.setReferenceAttribute(LocationAttrEnum.ATTR_LATITUDE, esEyeStep, LocationAttrEnum.ATTR_LATITUDE);
			updateStep.setReferenceAttribute(LocationAttrEnum.ATTR_LONGITUDE, esEyeStep, LocationAttrEnum.ATTR_LONGITUDE);
			updateStep.setReferenceAttribute(LocationAttrEnum.ATTR_COUNTRY_CODE, esEyeStep,
					LocationAttrEnum.ATTR_COUNTRY_CODE);
			updateStep.setReferenceAttribute(CommonAttrEnum.ATTR_ERROR, esEyeStep, CommonAttrEnum.ATTR_ERROR);

			esEyeStep.setNextSteps(0, updateStep);
		}
		else {
			VerizonDeviceLocationId deviceLocationId = getIccidByDeviceIdForVerizon(deviceId);
			if (deviceLocationId != null) {
				MessageChainStep verizonStep = mc.addStep("usat.location.network.verizon");

				Object attrValue = preareAttributeValue(deviceLocationId);
				verizonStep.setAttribute(CommonAttrEnum.ATTR_OBJECT, attrValue);

				MessageChainStep updateStep = mc.addStep("usat.verizon.location.update");
				updateStep.setReferenceAttribute(CommonAttrEnum.ATTR_ITEM_ID, verizonStep, CommonAttrEnum.ATTR_ITEM_ID);
				updateStep.setReferenceAttribute(LocationAttrEnum.ATTR_LATITUDE, verizonStep, LocationAttrEnum.ATTR_LATITUDE);
				updateStep.setReferenceAttribute(LocationAttrEnum.ATTR_LONGITUDE, verizonStep, LocationAttrEnum.ATTR_LONGITUDE);
				//updateStep.setReferenceAttribute(LocationAttrEnum.ATTR_COUNTRY_CODE, verizonStep, LocationAttrEnum.ATTR_COUNTRY_CODE);
				updateStep.setReferenceAttribute(CommonAttrEnum.ATTR_ERROR_MESSAGE, verizonStep, CommonAttrEnum.ATTR_ERROR_MESSAGE);

				verizonStep.setNextSteps(0, updateStep);
			}
		}

		log.debug(" before step count: "+mc.getStepCount());//+", current step: "+mc.getCurrentStep());
		if (mc.getStepCount() == 0) {
			throw new IllegalStateException("Cannot create MessageChain for device_id=" + deviceId);
		}
		getDeviceLocationDao().scheduleAsapDeviceLocationUpdate(deviceId);
		publishMessage(mc);
	}

	private void publishMessage(MessageChain mc) throws ServletException {
		try {
			MessageChainService.publish(mc, WebHelper.getDefaultPublisher());
		} catch (ServiceException serviceException) {
			throw new ServletException("Failed to send message", serviceException);
		}
	}

	public DeviceLocationDao getDeviceLocationDao() {
		if (deviceLocationDao == null) {
			synchronized (this) {
				if (deviceLocationDao == null) {
					deviceLocationDao = new DeviceLocationDbDaoImpl();
				}
			}
		}
		return deviceLocationDao;
	}

	private long extractDeviceId(InputForm form) throws ServletException {
		return form.getLong("device_id", true, -1L);
	}

	private void setupJsonStep() {
		setIncluded(null);
		setExcluded(null);
		setScope("request");
		setBean("deviceNetworkLocation");
	}

	Long getIccidByDeviceIdForEsEye(long deviceId) throws ServletException {
		// if (1 == 2 - 1) {
		// return 8944538523006819760L;
		// }
		try {
			Map<String, Object> params = new HashMap<>();
			params.put("device_id", deviceId);
			Results res = DataLayerMgr.executeQuery("GET_DEVICE_ICCID_BY_ID_FOR_ESEYE", params);
			if (res.next()) {
				return Long.valueOf(res.getFormattedValue("device_iccid"));
			}
			return null;
			//throw new IllegalStateException("Device with id = " + deviceId + " should have ICCID, but it does not.");
		} catch (SQLException sqlException) {
			throw new ServletException("Failed to resolve deviceIccid", sqlException);
		} catch (DataLayerException dataLayerException) {
			throw new ServletException("Failed to resolve deviceIccid", dataLayerException);
		}
	}

	private VerizonDeviceLocationId getIccidByDeviceIdForVerizon(long deviceId) throws ServletException {
		try {
			Map<String, Object> params = new HashMap<>();
			params.put("device_id", deviceId);
			Results res = DataLayerMgr.executeQuery("GET_DEVICE_ICCID_BY_ID_FOR_VERIZON", params);
			if (res.next()) {
				IdKind kind = IdKind.getValueOf(res.getFormattedValue("hostTypeDesc"));
				if (kind == null)
					return null;
				String deviceLocationId = kind.checkDeviceId(res.getFormattedValue("hostSerial"), res.getFormattedValue("hostLabel"));
				if (deviceLocationId == null)
					return null;
				String mdn = kind.checkMdn(res.getFormattedValue("hostDialNumber"));
				log.debug("deviceId, mdn: " + deviceId + ',' + res.getFormattedValue("hostDialNumber") + ',' + mdn);
				if (mdn == null)
					return null;
				VerizonDeviceLocationId vdli = new VerizonDeviceLocationId(kind.getVerizonDeviceKind(), deviceLocationId, mdn);
				return vdli;
			}
			return null;
			//throw new IllegalStateException("Device with id = " + deviceId + " should have MEID or IMEI, but it does not.");
		} catch (SQLException sqlException) {
			throw new ServletException("Failed to resolve device location id", sqlException);
		} catch (DataLayerException dataLayerException) {
			throw new ServletException("Failed to resolve device location id", dataLayerException);
		}
	}

	private Object preareAttributeValue(VerizonDeviceLocationId deviceLocationId) {
		Gson gson = new Gson();
		return gson.toJson(deviceLocationId);
	}

	public static long getMinRequestPeriodMinutes() {
		return minRequestPeriodMinutes;
	}

	public static void setMinRequestPeriodMinutes(long minRequestPeriodMinutes) {
		RequestDeviceLocationStep.minRequestPeriodMinutes = minRequestPeriodMinutes;
	}

	public static String getGoogleMapsApiKey() {
		return googleMapsApiKey;
	}

	public static void setGoogleMapsApiKey(String googleMapsApiKey) {
		RequestDeviceLocationStep.googleMapsApiKey = googleMapsApiKey;
	}

}
