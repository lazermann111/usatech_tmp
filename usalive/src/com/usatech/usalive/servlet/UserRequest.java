package com.usatech.usalive.servlet;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import simple.util.concurrent.CustomThreadFactory;

public class UserRequest {
	protected static final ScheduledThreadPoolExecutor userRequestUpdateService = new ScheduledThreadPoolExecutor(1, new CustomThreadFactory("UserRequestUpdate-", true, 3));
	protected final long requestId;
	protected final int pageId;
	protected final long profileId;
	protected final AtomicReference<UserRequestStatus> status = new AtomicReference<UserRequestStatus>(UserRequestStatus.PENDING);
	
	public UserRequest(long requestId, int pageId, long profileId, UserRequestStatus status) {
		this.requestId = requestId;
		this.pageId=pageId;
		this.profileId = profileId;
		this.status.set(status);
	}
	public long getRequestId() {
		return requestId;
	}
	
	public int getPageId() {
		return pageId;
	}
	public long getProfileId() {
		return profileId;
	}
	public UserRequestStatus getStatus() {
		return status.get();
	}
	public boolean statusNew(long delayUntilReady) {
		while(true) {
			UserRequestStatus old = status.get();
			switch(old) {
				case PENDING: 
					if(status.compareAndSet(old, UserRequestStatus.READY)) {
						return true;
					}
					break;
				case PENDING_NOTIFY: case NOTIFY:
					if(status.compareAndSet(old, UserRequestStatus.NOTIFY)) {
						if(delayUntilReady <= 0) {
							status.compareAndSet(UserRequestStatus.NOTIFY, UserRequestStatus.READY);
						} else {
							userRequestUpdateService.schedule(new Runnable() {
								public void run() {
									status.compareAndSet(UserRequestStatus.NOTIFY, UserRequestStatus.READY);
						    	}
							}, delayUntilReady, TimeUnit.MILLISECONDS);
						}
						return true;
					}
					break;
				case PULLED: case READY:
					return false;
			}
		}
	}
	public boolean statusNotify(long delayUntilReady) {
		while(true) {
			UserRequestStatus old = status.get();
			switch(old) {
				case PENDING: 
					if(status.compareAndSet(old, UserRequestStatus.PENDING_NOTIFY)) {
						return true;
					}
					break;
				case PENDING_NOTIFY: case NOTIFY:
					return false;
				case PULLED: case READY:
					if(status.compareAndSet(old, UserRequestStatus.NOTIFY)) {
						if(delayUntilReady <= 0) {
							status.compareAndSet(UserRequestStatus.NOTIFY, UserRequestStatus.READY);
						} else {
							userRequestUpdateService.schedule(new Runnable() {
								public void run() {
									status.compareAndSet(UserRequestStatus.NOTIFY, UserRequestStatus.READY);
						    	}
							}, delayUntilReady, TimeUnit.MILLISECONDS);
						}
						return true;
					}
					break;
			}
		}
	}
	public boolean statusPulled() {
		return status.getAndSet(UserRequestStatus.PULLED) != UserRequestStatus.PULLED;
	}
	public boolean statusReset() {
		return status.getAndSet(UserRequestStatus.PENDING) != UserRequestStatus.PENDING;
	}
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof UserRequest))
			return false;
		UserRequest ur = (UserRequest)obj;
		return ur.requestId == requestId;
	}
	@Override
	public int hashCode() {
		return (int)requestId;
	}
	@Override
	public String toString() {
		return "Request " + requestId + " [" + status.get() + "]";
	}	
}
