package com.usatech.usalive.servlet;

import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.steps.AbstractStep;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class InitPerformerUserStep extends AbstractStep {
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
		RequestUtils.setPerformerUserAttribute(form,
				user.getUserId(),
				user.getMasterUser() == null ? 0 : user.getMasterUser().getUserId(),
				user.isInternal(),
				user.getMasterUser() != null && user.getMasterUser().isInternal());
	}
}
