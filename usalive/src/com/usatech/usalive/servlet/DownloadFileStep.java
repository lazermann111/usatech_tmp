package com.usatech.usalive.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.app.DialectResolver;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;
import simple.text.StringUtils;

import com.usatech.layers.common.ProcessingUtils;

public class DownloadFileStep extends AbstractStep {
	private static final Log log = Log.getLog();

	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {		
		String fileName = form.getStringSafely("file_name", "");
		if (StringUtils.isBlank(fileName))
			return;
		
		ServletOutputStream out = null;
		try {
			response.setContentType("application/force-download");
			response.setHeader("Content-Transfer-Encoding", "binary"); 
			response.setHeader("Content-Disposition","attachment;filename=\"" + fileName + "\"");
			out = response.getOutputStream();		
			
			String sql = "SELECT file_transfer_content FROM device.file_transfer WHERE file_transfer_name = ? and file_transfer_type_cd = 30 ORDER BY created_ts";
	        PreparedStatement pstmt = null;
	        ResultSet rs = null;
	        InputStream in = null;
	        Connection conn = null;
	        
	        boolean success = false;
	        try
	        {
	        	conn = DataLayerMgr.getConnection("OPER");
	        		
	            pstmt = conn.prepareCall(sql);
	            pstmt.setString(1, fileName);
	            rs = pstmt.executeQuery();
	            if (rs != null && rs.next()) {
	            	if (DialectResolver.isOracle()) {
	            		Blob fileBlob = rs.getBlob(1);
	            		in = fileBlob.getBinaryStream();
	            	} else {
	            		in = rs.getBinaryStream(1);
	            	}
	            		
	            	int bytesRead; 
	            	byte[] bytes = new byte[1024];
	            	while ((bytesRead = in.read(bytes)) > 0) {
	            		out.write(bytes, 0, bytesRead);
	            		out.flush();
	            	}
	            }
	            success = true;
	        } catch(Exception e) {
	        	throw new ServletException("Error retrieving file content for fileName: " + fileName, e);
	        }
	        finally
	        {
	        	if (in != null) {
	        		try {
	                    in.close();
	                } catch (IOException e) {
	                    log.warn("Error closing database input stream", e);
	                }
	        	}
	        	
	            if (rs != null)
	            {
	                try {
	                    rs.close();
	                } catch (SQLException e) {
	                    log.warn("Error closing result set", e);
	                }
	            }
	            
	            if (pstmt != null)
	            {
	                try {
	                    pstmt.close();
	                }
	                catch (SQLException e) {
	                    log.warn("Error closing prepared statement", e);
	                }
	            }
	            	if (!success)
	        			ProcessingUtils.rollbackDbConnection(log, conn);    		
	        		ProcessingUtils.closeDbConnection(log, conn);
	        }
		} catch (IOException e) {
			throw new ServletException("Error downloading file " + fileName, e);
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
                    log.warn("Error closing output stream", e);
                }
			}
		}
	}
}
