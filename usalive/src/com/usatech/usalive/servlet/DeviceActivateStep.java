package com.usatech.usalive.servlet;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.DbUtils;
import simple.db.ParameterException;
import simple.io.Log;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.steps.AbstractStep;
import simple.text.StringUtils;

import com.usatech.layers.common.fees.TerminalFees;
import com.usatech.report.TerminalInfo;
import com.usatech.report.custom.GenerateUtils;
import com.usatech.usalive.servlet.ActivateDeviceUtil.UserReportableException;
/**
 * Updates the transport properties
 * 
 * @author Brian S. Krug
 *  
 */
public class DeviceActivateStep extends AbstractStep 
{
	private static final Log log = Log.getLog();

	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException 
	{
		UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
		if(user.isMissingLicense()) {
			dispatcher.dispatch("no_terminals", true);
			return;
		}
		
		// TODO: change form field names to match bulk activate
		
		boolean success = false;
		Connection connection = null;
		
		try {
			connection = DataLayerMgr.getConnection("report", false);
			
			String deviceSerialCd = ConvertUtils.getString(RequestUtils.getAttribute(request, "serial", true), true).trim().toUpperCase();
			
			Long custBankId = ConvertUtils.convert(Long.class, form.getAttribute("custBankId"));
			if(custBankId != null) {
				try {
					ActivateDeviceUtil.validateCustomerBank(connection, user, custBankId);
				} catch (UserReportableException e) {
					form.setAttribute("errorMessage", form.getTranslator().translate("terminal.activate.error.bankacct", e.getMessage()));
					dispatcher.dispatch("new_terminal", true);
					return;
				}
			} else if (user.isReseller()) {
				form.setAttribute("errorMessage", form.getTranslator().translate("terminal.activate.error.bankacct", "Bank acount is required."));
				dispatcher.dispatch("new_terminal", true);
				return;
			}
			
			GenerateUtils.formatPostal(null, form);
			
			Long dealerId = form.getLong("dealerId", false, -1);
			long quickConnectId = -1;
			
			if(user.isReseller()) {
				try {
					ActivateDeviceUtil.validateCommissionBank(connection, user, form.getLong("COMMISSION_BANK_ID", false, 0));
				} catch (UserReportableException e) {
					form.setAttribute("errorMessage", form.getTranslator().translate("terminal.activate.error.commissionBank", e.getMessage()));
					dispatcher.dispatch("new_terminal", true);
					return;
				}
				
				dealerId = ActivateDeviceUtil.lookupDealerByCustomerBankId(connection, custBankId);
				form.setAttribute("dealerId", dealerId);
			}
			
			// TODO: merge all this entry card acceptance stuff into a util class
			char posEnvironment = '?';
			String entryCapability = null;
			char pinCapability = '?';

			if(deviceSerialCd.startsWith("K3")) {
				String quickConnectUserName = form.getString("quickConnect", false);
				if(!StringUtils.isBlank(quickConnectUserName))
					try {
						quickConnectId = ActivateDeviceUtil.lookupQuickConnectIdByUsername(connection, quickConnectUserName);
					} catch(UserReportableException e) {
						form.setAttribute("errorMessage", form.getTranslator().translate("terminal.activate.error.quickConnect", e.getMessage()));
						dispatcher.dispatch("new_terminal", true);
						return;
					}

				char mobileIndicator = ConvertUtils.convertRequired(Character.class, RequestUtils.getAttribute(request, "mobileIndicator", true));
				Set<Character> entryList = ConvertUtils.convert(Set.class, Character.class, RequestUtils.getAttribute(request, "entryList", false));
				switch(mobileIndicator) {
					case 'Y':
						entryList.add('M');
						posEnvironment = 'C'; // Mobile
						break;
					case 'N':
						char terminalActivator = ConvertUtils.convertRequired(Character.class, RequestUtils.getAttribute(request, "terminalActivator", true));
						switch(terminalActivator) {
							case 'C':
								if(!entryList.contains('M') || entryList.contains('S') || entryList.contains('P') || entryList.contains('E'))
									posEnvironment = 'U'; // Unattended
								else
									posEnvironment = 'E'; // Ecommerce
								break;
							case 'A':
								if(entryList.contains('S'))
									posEnvironment = 'A'; // Attended
								else
									posEnvironment = 'M'; // MOTO
								break;
							default:
								form.setAttribute("errorMessage", form.getTranslator().translate("terminal.change.invalid.terminalActivator", "Invalid value for Card Data Entered By; Please select a valid option"));
								return;
						}
						break;
					default:
						form.setAttribute("errorMessage", form.getTranslator().translate("terminal.change.invalid.mobileIndicator", "Invalid value for Machine is Tablet / Phone; Please select a valid option"));
						return;
				}
				
				pinCapability = entryList.contains('M') ? 'Y' : 'N';
				
				StringBuilder sb = new StringBuilder();
				for(Character ec : entryList)
					if(ec != null)
						sb.append(ec);
				
				entryCapability = sb.toString();
				
				form.set("posEnvironment", posEnvironment);
				form.set("pinCapability", pinCapability);
				form.set("entryCapability", entryCapability);
			}
			
			long terminalId = -1;
			
			TerminalInfo terminal = new TerminalInfo(deviceSerialCd);
			terminal.USER_ID = user.getUserId();
			terminal.MASTER_USER_ID = user.getMasterUserId();
			terminal.DEALER_ID = dealerId;
			terminal.ASSET_NUMBER = form.getString("asset", true);
			terminal.TELEPHONE = form.getString("telephone", false);
			terminal.REGION = form.getString("region", false);
			if (StringUtils.isBlank(terminal.REGION))
				terminal.REGION = form.getString("regionName", false);
			terminal.LOCATION_NAME = form.getString("location", true);
			terminal.LOCATION_DETAILS = form.getString("locationDetails", false);
			terminal.ADDRESS = form.getString("address1", true);
			terminal.ZIP = form.getString("postal", true);
			terminal.COUNTRY = form.getString("country", true);
			terminal.CUSTOMER_BANK_ID = custBankId;
			terminal.PAY_SCHEDULE = form.getInt("paymentScheduleId", true, 0);
			terminal.LOCATION_TYPE = form.getString("locationType", true);
			terminal.LOCATION_TYPE_SPECIFY = form.getString("locationTypeSpecify", false);
			terminal.PRODUCT_TYPE = form.getString("productType", true);
			terminal.PRODUCT_TYPE_SPECIFY = form.getString("productTypeSpecify", false);
			terminal.TIME_ZONE = form.getInt("timeZone", true, 0);
			if(user.isInternal()||(user.getMasterUser()!=null&&user.getMasterUser().isInternal())){ 
				terminal.DOING_BUSINESS_AS = form.getString("doingBusinessAs", false);
				terminal.BUSINESS_TYPE = form.getString("businessType", true);
			}
			terminal.COMMISSION_BANK_ID = form.getLong("COMMISSION_BANK_ID", user.isReseller(), 0);
			terminal.CUSTOMER_SERVICE_PHONE = form.getString("customerServicePhone", false);
			terminal.CUSTOMER_SERVICE_EMAIL = form.getString("customerServiceEmail", false);
			terminal.POS_ENVIRONMENT = posEnvironment;
			terminal.ENTRY_CAPABILITY = entryCapability;
			terminal.PIN_CAPABILITY = pinCapability;
			
			String machine = form.getString("machine", true);
			if (!StringUtils.isBlank(machine)) {
				try {
					String[] makeModel = machine.split(" - - ");
					terminal.MACHINE_MAKE = makeModel[0];
					terminal.MACHINE_MODEL = makeModel[1];
				} catch (Exception e) {
					form.setAttribute("errorMessage", form.getTranslator().translate("terminal.activate.error.machine", "Invalid machine make/model."));
					dispatcher.dispatch("new_terminal", true);
					return;
				}
			} else {
				terminal.MACHINE_MAKE = form.getString("make", true);
				terminal.MACHINE_MODEL = form.getString("model", true);
			}
			
			try {
				DataLayerMgr.executeCall(connection, "ACTIVATE_DEVICE", terminal);
			} catch(SQLException e) {
				log.warn("Exception while attemping to activate terminal", e);
				switch(e.getErrorCode()) {
					case 20201:
						form.setAttribute("errorMessage", form.getTranslator().translate("terminal.activate.invalid.serial", "Device serial number \"{0}\" is invalid", form.getAttribute("serial")));
						break;
					case 20202:
						form.setAttribute("errorMessage", form.getTranslator().translate("terminal.activate.invalid.dealer", "Invalid program specified", form.getAttribute("serial")));
						break;
					case 20203:
						form.setAttribute("errorMessage", form.getTranslator().translate("terminal.activate.invalid.bankacct", "Invalid bank account", form.getAttribute("serial")));
						break;
					case 20204:
						form.setAttribute("errorMessage", form.getTranslator().translate("terminal.activate.error.nolicense", "No license agreement found for this customer", form.getAttribute("serial")));
						break;
					case 20205:
						form.setAttribute("errorMessage", form.getTranslator().translate("terminal.activate.error.alreadyactive", "You have already activated device \"{0}\"", form.getAttribute("serial")));
						break;
					case 20206:
						log.error("User, " + user.getUserName() + ", is trying to activate an e-Port that has already been activated by another user.");
						form.setAttribute("errorMessage", form.getTranslator().translate("terminal.activate.error.inusebyother", "Device \"{0}\" is not available. Please contact USA Technologies", form.getAttribute("serial")));
						break;
					case 20220:
						form.setAttribute("errorMessage", form.getTranslator().translate("terminal.activate.invalid.state", "Invalid state \"{1}\" for country \"{2}\" specified for device \"{0}\"", form.getAttribute("serial"), form.getAttribute("state"), form.getAttribute("country")));
						break;
					default:
						log.warn("Unexpected error while activating terminal", e);
						form.setAttribute("errorMessage", form.getTranslator().translate("terminal.activate.error.unknown", "Could not activate device \"{0}\" with the provided information. Please try again", form.getAttribute("serial")));
				}
				
				dispatcher.dispatch("new_terminal", true);
				return;
			} catch(ParameterException e) {
				log.warn("Unexpected error while activating terminal", e);
				form.setAttribute("errorMessage", form.getTranslator().translate("terminal.activate.error.unknown", "Could not activate device \"{0}\" with the provided information. Please try again", form.getAttribute("serial")));
				dispatcher.dispatch("new_terminal", true);
				return;
			}
			
			terminalId = terminal.TERMINAL_ID;
			form.set("terminalId", terminalId);
			user.setTerminalCount(user.getTerminalCount() + 1);
			
			if(user.isPartner() || user.isReseller())
				ActivateDeviceUtil.acceptTerminal(connection, terminalId);

			if(quickConnectId > 0)
				ActivateDeviceUtil.setQuickConnectCredentials(connection, quickConnectId, deviceSerialCd);

			if (user.isReseller()) {
				TerminalFees fees = TerminalFees.loadByTerminalId(connection, terminalId);
				fees.updateFromForm(form, false);
				if (fees.isDirty()) {
					fees.saveToDatabase(connection);
				}
			}

			success = true;
			form.setAttribute("successMessage", form.getTranslator().translate("terminal.activate.success", "Device \"{0}\" was activated for location \"{1}\"", form.getAttribute("serial"), form.getAttribute("location")));
		} catch(ConvertException e) {
			throw new ServletException("Invalid parameters", e);
		} catch(SQLException e) {
			throw new ServletException("Could not access database", e);
		} catch(DataLayerException e) {
			throw new ServletException("Could not access database", e);
		} finally {
			if(connection != null) {
				try {
					DbUtils.commitOrRollback(connection, success, true);
				} catch(SQLException e) {
					log.warn("final {0} failed", (success ? "commit" : "rollback"), e);
				}
			}
		}
		
		dispatcher.dispatch("terminal_details", true);
	}
}
