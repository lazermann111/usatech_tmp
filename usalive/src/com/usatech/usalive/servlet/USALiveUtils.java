package com.usatech.usalive.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.sax.SAXSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.layers.common.constants.CommonAttrEnum;
import com.usatech.layers.common.process.ProcessType;
import com.usatech.layers.common.util.WebHelper;
import com.usatech.usalive.hybrid.HybridServlet;
import com.usatech.usalive.link.LinkRequirementEngine;
import com.usatech.usalive.report.AsyncReportEngine;

import simple.app.Publisher;
import simple.app.ServiceException;
import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.falcon.engine.HttpServletRequestScene;
import simple.io.BufferStream;
import simple.io.ByteInput;
import simple.io.HeapBufferStream;
import simple.io.Log;
import simple.results.Results;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.SimpleServlet;
import simple.text.StringUtils;
import simple.xml.XMLUtils;
import simple.xml.XPathEvaluator;
import simple.xml.sax.MessagesInputSource;
import simple.xml.sax.ResultsAdapter;
import simple.xml.sax.ResultsInputSource;

public class USALiveUtils {
	private static final Log log = Log.getLog();
	public static final String APP_CD = "USALive";
	public static String companyLogoPrefix = "company_logo_";
	protected static final String LINKS_ATTRIBUTE_PREFIX = "com.usatech.usalive.servlet.USALiveUtils.links_";
	protected static final String LINK_RESULTS_ATTRIBUTE_PREFIX = "com.usatech.usalive.servlet.USALiveUtils.link_results_";
	protected static String usaliveEmail = "usalive@usatech.com";

	public static Node getLinks(HttpServletRequestScene scene, String usage)
			throws SQLException, DataLayerException, ParserConfigurationException, IOException, SAXException {
		return getLinks((InputForm) scene.getRequest().getAttribute(SimpleServlet.ATTRIBUTE_FORM), usage);
	}

	public static Node getLinks(InputForm form, String usage)
			throws SQLException, DataLayerException, ParserConfigurationException, IOException, SAXException {
		UsaliveUser user = (UsaliveUser) form.getAttribute(SimpleServlet.ATTRIBUTE_USER, RequestUtils.SESSION_SCOPE);
		String key = LINKS_ATTRIBUTE_PREFIX + usage + "_" + (user != null ? user.getUserId() : 0);
		Node links = (Node) form.getAttribute(key, RequestUtils.SESSION_SCOPE);
		if (links == null) {
			Results results = getLinkResults(form, usage);
			ResultsAdapter ra = new ResultsAdapter();
			ra.setGroupingEnabled(false);
			links = XMLUtils.getDOMNode(new SAXSource(ra, new ResultsInputSource(results)));
			form.setAttribute(key, links, RequestUtils.SESSION_SCOPE);
		}
		return links;
	}

	public static Results getLinkResults(InputForm form, String usage) throws SQLException, DataLayerException {
		UsaliveUser user = (UsaliveUser) form.getAttribute(SimpleServlet.ATTRIBUTE_USER, RequestUtils.SESSION_SCOPE);
		String key = LINK_RESULTS_ATTRIBUTE_PREFIX + usage + "_" + (user != null ? user.getUserId() : 0);
		Results results = (Results) form.getAttribute(key, RequestUtils.SESSION_SCOPE);
		if (results == null) {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("user", user);
			params.put("usage", usage);
			LinkRequirementEngine lre = (LinkRequirementEngine) form
					.getAttribute("com.usatech.usalive.link.LinkRequirementEngine");
			params.put("satisfiedRequirementIds", lre.getSatisfiedRequirements(user, form.getRequest()));
			results = DataLayerMgr.executeQuery("GET_WEB_LINKS_WITH_PRIVS", params);
			form.setAttribute(key, results, RequestUtils.SESSION_SCOPE);
		}
		return results.clone();
	}

	public static boolean removeLink(long linkId, InputForm form, String usage) throws XPathExpressionException {
		UsaliveUser user = (UsaliveUser) form.getAttribute(SimpleServlet.ATTRIBUTE_USER, RequestUtils.SESSION_SCOPE);
		String key = LINKS_ATTRIBUTE_PREFIX + usage + "_" + (user != null ? user.getUserId() : 0);
		Node links = (Node) form.getAttribute(key, RequestUtils.SESSION_SCOPE);
		if (links == null)
			return false;
		XPath xpath = XPathEvaluator.getXPathFactory().newXPath();
		Node newLinks = (Node) xpath.evaluate("//*[row/id/@value != '" + linkId + "']", links, XPathConstants.NODE);
		return !newLinks.equals(links);
	}

	public static void clearLinks(InputForm form, String usage) {
		UsaliveUser user = (UsaliveUser) form.getAttribute(SimpleServlet.ATTRIBUTE_USER, RequestUtils.SESSION_SCOPE);
		String key = LINKS_ATTRIBUTE_PREFIX + usage + "_" + (user != null ? user.getUserId() : 0);
		form.setAttribute(key, null, RequestUtils.SESSION_SCOPE);
		key = LINK_RESULTS_ATTRIBUTE_PREFIX + usage + "_" + (user != null ? user.getUserId() : 0);
		form.setAttribute(key, null, RequestUtils.SESSION_SCOPE);
		if (user != null && user.getMasterUserId() > 0) {
			key = LINKS_ATTRIBUTE_PREFIX + usage + "_" + user.getMasterUserId();
			form.setAttribute(key, null, RequestUtils.SESSION_SCOPE);
			key = LINK_RESULTS_ATTRIBUTE_PREFIX + usage + "_" + user.getMasterUserId();
			form.setAttribute(key, null, RequestUtils.SESSION_SCOPE);
		}
	}

	public static void writeUsageTerms(HttpServletResponse response, PrintWriter writer) {
		writeHeader(response, writer);
		writer.println("NOTICE TO USERS<br/><br/>");

		writer.println("This computer system is the private property of USA Technologies, Inc.<br/>");
		writer.println("*** It Is For Authorized Use Only. ***<br/><br/>");

		writer.println("Any or all uses of this system may be intercepted,<br/>");
		writer.println("audited, inspected, and monitored, recorded, disclosed<br/>");
		writer.println("to authorized site, government, and law enforcement personnel, as well<br/>");
		writer.println("as authorized officials of government agencies, both domestic and foreign.<br/>");
		writer.println("By using this system, the user consents to such interception, monitoring,<br/>");
		writer.println("recording, auditing, inspection, and disclosure at the discretion of<br/>");
		writer.println("such personnel or officials. Unauthorized or improper use of this system may<br/>");
		writer.println("result in civil and criminal penalties, and administrative or disciplinary<br/>");
		writer.println("action, as appropriate. By continuing to use this system you indicate your<br/>");
		writer.println("awareness of and consent to these terms and conditions of use.<br/><br/>");

		writer.println("LOG OFF IMMEDIATELY if you do not agree to the conditions stated in this warning.<br/><br/>");

		writeFooter(response, writer);
	}

	public static void writeHeader(HttpServletResponse response, PrintWriter writer) {
		response.addHeader("Expires", "-1");
		response.setContentType("text/html; charset=utf-8");

		writer.println("<html>");
		writer.println("<title>USA Technologies USALive Web Service</title>");
		writer.println("<body>");
		writer.println("<div align=\"center\" style=\"font-family: Arial, Verdana, Tahoma, Sans-Serif;\">");
		writer.println("<h3>USA Technologies USALive Web Service</h3>");
	}

	public static void writeFooter(HttpServletResponse response, PrintWriter writer) {
		writer.print("<a href=\"https://www.usatech.com\" target=\"_blank\">");
		writer.print("USA Technologies, Inc. - &copy; Copyright ");
		writer.print(Calendar.getInstance().get(Calendar.YEAR));
		writer.println(" - Confidential</a>");

		writer.println("</div>");
		writer.println("</body>");
		writer.println("</html>");

		writer.flush();
	}

	public static void writeError(HttpServletResponse response, PrintWriter writer, String errorMessage) {
		writeHeader(response, writer);
		writer.println("<h4>ERROR</h4>");
		writer.print("<h6>");
		writer.print(new Date());
		writer.println("</h6>");
		writer.println(errorMessage);
		writer.println("<br/><br/>");
		writeFooter(response, writer);
	}

	public static String getUsaliveEmail() {
		return usaliveEmail;
	}

	public static void setUsaliveEmail(String usaliveEmail) {
		USALiveUtils.usaliveEmail = usaliveEmail;
	}

	public static Long getTerminalId(String deviceSerialCd) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("SERIAL_NUMBER", deviceSerialCd);
		try {
			DataLayerMgr.executeCall("GET_TERMINAL_ID", params, false);
			return ConvertUtils.convert(Long.class, params.get("TERMINAL_ID"));
		} catch (Exception e) {
			log.warn("Error getting terminalId for deviceSerialCd: " + deviceSerialCd, e);
			return null;
		}
	}

	public static String getQueryString(HttpServletRequest request) {
		UsaliveUser user = (UsaliveUser) RequestUtils.getUser(request);
		if (user != null)
			return new StringBuilder("profileId=").append(user.getUserId()).toString();
		return "";
	}

	public static String getHiddenInputs(HttpServletRequest request) {
		UsaliveUser user = (UsaliveUser) RequestUtils.getUser(request);
		if (user != null)
			return new StringBuilder("<input type=\"hidden\" name=\"profileId\" value=\"").append(user.getUserId())
					.append("\" />").append("\n").toString();
		return "";
	}

	public static boolean verifyEmail(String email, String label, MessagesInputSource messages) {
		InternetAddress[] ias;
		try {
			ias = InternetAddress.parse(email);
		} catch (AddressException e) {
			log.warn("Unparseable email: `" + email + "'", e);
			messages.addMessage("error", "verification-email-unparseable",
					"The {0} email address is not formatted correctly. (It may contain invalid characters). Please re-type it.",
					label, e.getMessage());
			return false;
		}
		switch (ias.length) {
		case 0:
			log.warn("Blank email: `" + email + "'");
			messages.addMessage("error", "verification-email-blank", "Please enter {0} email address", label);
			return false;
		case 1:
			try {
				WebHelper.checkEmail(email);
			} catch (MessagingException e) {
				log.warn("Invalid email: `" + email + "'", e);
				messages.addMessage("error", "verification-email-invalid",
						"The {0} email address is not valid. Please re-type it.", label, e.getMessage());
				return false;
			}
			return true;
		default:
			log.warn("Multiple emails: `" + email + "'");
			messages.addMessage("error", "verification-email-multiple",
					"Please do not use commas or semi-colons in the {0} email address", label);
			return false;
		}
	}

	public static void writeFormProfileId(PageContext pageContext) throws IOException {
		if (!(pageContext.getRequest() instanceof HttpServletRequest))
			return;
		UsaliveUser user = (UsaliveUser) RequestUtils.getUser((HttpServletRequest) pageContext.getRequest());
		if (user == null || user.getMasterUserId() == 0 || user.getUserId() == user.getMasterUserId())
			return;
		JspWriter writer = pageContext.getOut();
		writer.append("<input type=\"hidden\" name=\"profileId\" value=\"").append(String.valueOf(user.getUserId()))
				.append("\"/>");
	}

	public static long registerProcessRequest(HttpServletRequest request, ProcessType processType,
			String processDescription, String notifyEmail, Map<String, Object> parameters, InputStream content)
			throws IOException, ServletException, ConvertException, SQLException, DataLayerException, ServiceException {
		UsaliveUser user = (UsaliveUser) RequestUtils.getUser(request);
		Map<String, Object> params = new HashMap<>();
		params.put("processDescription", processDescription);
		params.put("profileId", user.getUserId());
		params.put("userId", user.getMasterUserId());
		params.put("processType", processType);
		params.put("notifyEmail", notifyEmail);
		params.put("processContent", content);
		BufferStream buffer = new HeapBufferStream();
		try (Writer writer = new OutputStreamWriter(buffer.getOutputStream())) {
			StringBuffer url = request.getRequestURL();
			if (url != null)
				StringUtils.writeEncodedNVP(writer, null, "requestURL", url.toString());
			StringUtils.writeEncodedNVP(writer, null, "remoteAddr", request.getRemoteAddr());
			StringUtils.writeEncodedNVP(writer, null, "remotePort", String.valueOf(request.getRemotePort()));
			HttpSession session = request.getSession(false);
			if (session != null)
				StringUtils.writeEncodedNVP(writer, null, "sessionId", session.getId());
			for (Enumeration<String> en = request.getHeaderNames(); en.hasMoreElements();) {
				String name = en.nextElement();
				StringUtils.writeEncodedNVP(writer, "HEADER", name, request.getHeader(name));
			}
			StringUtils.writeEncodedMap(writer, "PARAM", parameters);
		}
		params.put("processParams", buffer.getInputStream());
		DataLayerMgr.executeCall("CREATE_PROCESS_REQUEST", params, true);
		long processRequestId = ConvertUtils.getLong(params.get("processRequestId"));
		MessageChain mc = new MessageChainV11();
		MessageChainStep step = mc.addStep("usat.process.request");
		step.setAttribute(CommonAttrEnum.ATTR_PROCESS_ID, processRequestId);
		step.setAttribute(CommonAttrEnum.ATTR_USER_ID, user.getUserId());
		AsyncReportEngine arEngine = RequestUtils.getAttribute(request, HybridServlet.ATTRIBUTE_ASYNCH_REPORT_ENGINE,
				AsyncReportEngine.class, true);
		Publisher<ByteInput> publisher = arEngine.getPublisher();
		MessageChainService.publish(mc, publisher);
		return processRequestId;
	}

	public static String buildGoogleMapAreaUrl(double lat, double lng, String apiKey) {
		String geo = String.valueOf(lat) + "," + String.valueOf(lng);
		StringBuilder path = new StringBuilder("https://maps.google.com/maps/api/staticmap?center=").append(geo)
				.append("&amp;zoom=13&amp;size=450x180&amp;maptype=roadmap&amp;key=").append(apiKey).append("&amp;")
				.append("path=fillcolor:0xFFCC0033|weight:2|color:0xFF0000AA");
		double circleRadiusX = 0.011D;
		double circleRadiusY = circleRadiusX * 1.35D;
		int cordLenDergee = 8;
		for (int i = 0; i <= 360; i += cordLenDergee) {
			double lat1 = lat + circleRadiusX * Math.sin(Math.toRadians(i));
			double lng1 = lng + circleRadiusY * Math.cos(Math.toRadians(i));
			path.append("|").append(lat1).append(",").append(lng1);
		}

		return path.toString();
	}
}
