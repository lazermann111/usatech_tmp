package com.usatech.usalive.servlet;

import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;
import simple.servlet.*;

public class CreateChildBankAccountStep extends LoginStep 
{
	private static final Log log = Log.getLog();
	
	public CreateChildBankAccountStep() throws NoSuchAlgorithmException 
	{
		super();
	}

	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException 
	{
		// if creating a bank account for a specific customer, make sure this user can admin the customer 
		// and switch the form input to the primary user of the customer
		int customerId = form.getInt("customerId", false, -1);
		if (customerId > 0) {
			UsaliveUser creatingUser = (UsaliveUser) RequestUtils.getUser(request);
			if (creatingUser == null) {
				throw new ServletException("attempt to create bank account with no creatingUser");
			}
			
			if (!(creatingUser.isPartner() || creatingUser.isCustomerService())) {
				throw new ServletException(String.format("User %s does not have permission to create a bank account for customer %s", creatingUser.getUserId(), customerId));
			}
			
			InputForm getCustomersForm = new MapInputForm(new HashMap<String, Object>());
			getCustomersForm.setAttribute(SimpleServlet.ATTRIBUTE_USER, creatingUser);

			UsaliveUser primaryUser = null;
			
			try {
				Results customerListResults = DataLayerMgr.executeQuery("GET_CUSTOMERS", getCustomersForm, false);
				while(customerListResults.next()) {
					Integer id = customerListResults.getValue("customerId", Integer.class);
					if(id != null && customerId == id) {
						primaryUser = new UsaliveUser();
						primaryUser.setCustomerId(id);
						primaryUser.setUserId(customerListResults.getValue("userId", Long.class));
						break;
					}
				}
			} catch (Exception e) {
				throw new ServletException("child customer check failed", e);
			}
			
			if (primaryUser == null) {
				RequestUtils.getOrCreateMessagesSource(form).addMessage("error", null, "Customer {0} not found.", customerId);
				log.warn("User {0} can not create bank account for customer {1}", creatingUser.getUserName(), customerId);
				dispatcher.dispatch("new_bank_acct", false);
				return;
			}
			
			form.setAttribute(SimpleServlet.ATTRIBUTE_USER, primaryUser);
		}
	}
}
