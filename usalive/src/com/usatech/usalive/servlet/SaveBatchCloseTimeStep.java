package com.usatech.usalive.servlet;

import java.io.PrintStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;
/**
 * 
 * @author yhe
 *
 */
public class SaveBatchCloseTimeStep extends AbstractStep {
	private static final Log log = Log.getLog();

	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		PrintStream out = null;
		try {
			out=new PrintStream(response.getOutputStream());
			out.println("<script type=\"text/javascript\">//<![CDATA[");
			String batchCloseTime=form.getString("batchCloseTime", true);
			if(batchCloseTime.matches("^((12|0*[1-5]):([0-5][0-9]))|(0*6:00)$")){
				if (batchCloseTime.startsWith("12"))
					batchCloseTime = "00" + batchCloseTime.substring(2);
				if (batchCloseTime.matches("^[1-6]:[0-5][0-9]$"))
					batchCloseTime = "0" + batchCloseTime;
				form.set("batchCloseTime", batchCloseTime);
				DataLayerMgr.executeCall("SAVE_BATCH_CLOSE_TIME", form, true);
				out.print("editBatchCloseTimeResult(1,'');");
			}else{
				out.print("editBatchCloseTimeResult(-1,'Failed to save the batch close time. The batch close time format is invalid. Please check.');");
			}
		}catch(Exception e) {
			out.print("editBatchCloseTimeResult(-1,'Failed to save the batch close time. Please contact us for assistance.');");
			log.error("Failed to save the batch close time.", e);
		} 
		out.print("//]]></script>");
		out.println();
		out.flush();
		
	}

}