package com.usatech.usalive.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

public class RequestPasswordResetStep extends AbstractStep {
		
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		form.set("internalUser", LoginStep.DEFAULT_INTERNAL_USERNAME_PATTERN.matcher(form.getString("username", true)).matches() ? "Y" : "N");
	}
	
}
