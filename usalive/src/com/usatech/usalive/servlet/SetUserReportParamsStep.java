package com.usatech.usalive.servlet;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;
import simple.text.StringUtils;

public class SetUserReportParamsStep extends AbstractStep {
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		long userReportId = form.getLong("userReportId", true, 0L);
		Map<String, Object> params = new HashMap<>();
		params.put("userReportId", userReportId);
		try {
			try (Connection conn = DataLayerMgr.getConnectionForCall("GET_USER_REPORT_PARAMS")) {
				boolean okay = true;
				try (Results results = DataLayerMgr.executeQuery(conn, "GET_USER_REPORT_PARAMS", form)) {
					while(results.next()) {
						String paramName = results.getFormattedValue("paramName");
						if(!StringUtils.isBlank(paramName)) {
							String paramValue = form.getString("params." + paramName, false);
							params.put("paramName", paramName);
							params.put("paramValue", paramValue);
							DataLayerMgr.executeUpdate(conn, "UPSERT_USER_REPORT_PARAM", params);
						}
					}
					okay = true;
				} finally {
					if(okay)
						conn.commit();
					else
						conn.rollback();
				}
			}
		} catch(SQLException | DataLayerException e) {
			throw new ServletException("Failed to update the user report.", e);
		}
	}
}
