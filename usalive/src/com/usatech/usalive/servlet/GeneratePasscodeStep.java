package com.usatech.usalive.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.security.SecureHash;
import simple.security.SecurityUtils;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;
/**
 * 
 * @author yhe
 *
 */
public class GeneratePasscodeStep extends AbstractStep {
	private static final Log log = Log.getLog();

	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String statusMessage;
		try {
			String passcode=SecurityUtils.getRandomPasscode();
			byte[] salt = SecureHash.getSalt();
    		byte[] hash = SecureHash.getHash(passcode, salt);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("newPasswordHash", hash);
    		params.put("newPasswordSalt", salt);
    		params.put("terminalId", form.get("terminalId"));
    		DataLayerMgr.executeCall("SET_DEVICE_TEMP_PASSCODE", params, true);
    		statusMessage="<p class=\"status-info-success\">Successfully generated the passcode:&#160;"+passcode+"</p><p class=\"status-info-success\">Please use this passcode on the device. This passcode will never be displayed again.</p>";
		}catch(Exception e) {
			statusMessage="<p class=\"status-info-failure\">Failed to generate the passcode. Please try again.</p>";
			log.error("Failed to generate the passcode. Please try again.", e);
		} 
		try{
			response.getOutputStream().write(statusMessage.getBytes());
		}catch(IOException e){
			throw new ServletException("Failed to generate the passcode. Please try again.", e);
		}
	}

}