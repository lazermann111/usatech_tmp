package com.usatech.usalive.servlet;

import java.io.IOException;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.io.IOUtils;
import simple.io.Log;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputFile;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.steps.AbstractStep;
import simple.text.StringUtils;
/**
 * Updates the transport properties
 * 
 * @author Brian S. Krug
 *  
 */
public class UpdateTransportPropertiesStep extends AbstractStep {
	private char[] javascriptEscape={'\'', '"', '\\'};
	private static final Log log = simple.io.Log.getLog();
	public UpdateTransportPropertiesStep() {
	}
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String transportAction=form.getString("transportAction", true);
		try {
			Connection conn = DataLayerMgr.getConnectionForCall("ADD_CCS_TRANSPORT");
			try {
				if(transportAction.equals("add")){
					DataLayerMgr.executeUpdate(conn, "ADD_CCS_TRANSPORT", form);
				}else{
					DataLayerMgr.executeUpdate(conn,"UPDATE_CCS_TRANSPORT_NAME_TYPE", form);
				}
				Results results = DataLayerMgr.executeQuery("GET_TRANSPORT_PROPERTY_TYPES", form);
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("transportId", form.getAttribute("transportId"));
				while(results.next()) {
			        int tptId = results.getValue("propertyTypeId", Integer.class);
			        String tptName = results.getValue("propertyTypeName", String.class);
			        boolean ignore = form.getBoolean("ignore_tpv_" + tptId, false, false);
			        if(!ignore) {
			            Object v = form.getAttribute("tpv_" + tptId);
			            String value;
			            if(v instanceof InputFile) {
			            	try {
								value = IOUtils.readFully(((InputFile)v).getInputStream());
							} catch(IOException e) {
								throw new ServletException("Could not read file '" + ((InputFile)v).getPath() + "' for '" + tptName + "'", e);
							}
			            } else {
			                try {
								value = ConvertUtils.convert(String.class, v);
							} catch(ConvertException e) {
								throw new ServletException("Value for '" + tptName + " could not be converted to text", e);
							}
			            }
			            params.put("propertyTypeId", tptId);
			            params.put("value", value);
			            DataLayerMgr.executeUpdate(conn, "UPDATE_TRANSPORT_PROPERTY", params);
			        }
			    }
				conn.commit();
				
				PrintStream out = new PrintStream(response.getOutputStream());
				out.println("<script type=\"text/javascript\">//<![CDATA[");
				String transportName=form.get("transportName").toString();
				out.print("$('session-token').value='"+RequestUtils.getSessionToken(request)+"';updateTransportStatus('Transport was saved.','info-success');");
				if(transportAction.equals("update")){
					out.print("selectedTransportInvalid=0;$('transportSelect').getSelected().set('text', '"+StringUtils.escape(transportName, javascriptEscape, '\\')+"');$('transportSelect').getSelected().set('title', '"+StringUtils.escape(transportName, javascriptEscape, '\\')+"');$('transportSelect').getSelected().set('onselect',"+"\"onAvailableTransportChange("+form.get("transportTypeId")+",0);$('transportSelect').getSelected().set('data-transportTypeId',"+form.get("transportTypeId")+");\");");
				}else{
					out.print("$('transportSelect').adopt(new Element(\"option\", { 'data-transportTypeId': "+form.get("transportTypeId")+", selected: 'selected',value:"+form.get("transportId")+", text:'"+StringUtils.escape(transportName, javascriptEscape, '\\')+"', title:'"+StringUtils.escape(transportName, javascriptEscape, '\\')+"',onselect: \"onAvailableTransportChange("+form.get("transportTypeId")+",0);\""+"}));");
				}
				if(transportAction.equals("update")){
					out.print("refreshUserReportSection();");
				}
				out.print("//]]></script>");
				out.println();
				out.flush();
			} catch(Exception e) {
				try { conn.rollback(); } catch(SQLException e1) { log.warn("Could not rollback"); } 
				writeErrorResponse(request, response, e.getMessage());
				log.error(e.getMessage(), e);
			}finally{
				conn.close();
			}
		}catch(Exception e){
			writeErrorResponse(request, response, e.getMessage());
			log.error(e.getMessage(), e);
		}
		
	}
	
	public void writeErrorResponse(HttpServletRequest request,HttpServletResponse response, String errorMsg) throws ServletException{
		try{
			PrintStream out = new PrintStream(response.getOutputStream());
			out.println("<script type=\"text/javascript\">//<![CDATA[");
			if(errorMsg.contains("ORA-00001")){
				errorMsg="Transport Name already exists. Please choose a different name.";
			}else{
				errorMsg="An application error occurred.";
			}
			out.print("$('session-token').value='"+RequestUtils.getSessionToken(request)+"';updateTransportStatus('"+errorMsg.trim()+"','info-failure');");
			out.print("//]]></script>");
			out.println();
			out.flush();
		}catch(IOException e){
			throw new ServletException(e);
		}
	}
}
