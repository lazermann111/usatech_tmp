package com.usatech.usalive.servlet;

import java.io.IOException;
import java.io.PrintStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.steps.AbstractStep;
import simple.text.StringUtils;
/**
 * 
 * @author yhe
 *
 */
public class ColumnMapSearchStep extends AbstractStep{
	private static final Log log = simple.io.Log.getLog();
	public static char[] javascriptEscape={'\'', '"'};
	public void perform(Dispatcher dispatcher, InputForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException {
		try {
			StringBuilder responseText=new StringBuilder();
			String searchFor=form.getString("searchFor", true);
			Integer userCustomerId = RequestUtils.getAttribute(request,"customerId", Integer.class, false);
			int isServiceAccountUser=0;
			UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
			if(userCustomerId!=null&&userCustomerId<1&&user.getUserType()==8){
				isServiceAccountUser=1;
			}
			form.set("isServiceAccountUser", isServiceAccountUser);
			if(searchFor.equalsIgnoreCase("customer")){
				Results results = DataLayerMgr.executeQuery("GET_CUSTOMERS", form);
				StringBuilder sb=new StringBuilder("");
				sb.append("<option value='-1'>");
				sb.append("NO FILTER FOR CUSTOMER");
				sb.append("</option>");
				int resultCount=0;
				while(results.next()){
					resultCount++;
					sb.append("<option value='").append(results.get("customerId")).append("'>");
					sb.append(StringUtils.prepareCDATA(results.getFormattedValue("customerName")));
					sb.append("</option>");
				}
				if(resultCount>0){
					responseText.append("$('customerId').set('html',\""+sb.toString()+"\");setMessage(0,'');clearAll();onCustomerChange();");
				}else{
					responseText.append("setMessage(-1,'No customer found.');");
				}
			}else if(searchFor.equalsIgnoreCase("regionAndMap")){
				Results results = DataLayerMgr.executeQuery("GET_TERMINAL_REGIONS", form);
				StringBuilder sb=new StringBuilder("");
				sb.append("<option value='-1'>");
				sb.append("NO FILTER FOR REGION");
				sb.append("</option>");
				while(results.next()){
					sb.append("<option value='").append(results.get("regionId")).append("'>");
					sb.append(StringUtils.prepareCDATA(results.getFormattedValue("regionName")));
					sb.append("</option>");
				}
				if(sb.length()>0){
					responseText.append("$('regionId').set('html',\""+sb.toString()+"\");");
					responseText.append("$('mappedRegionId').set('html',\""+sb.toString()+"\");");
				}
				results = DataLayerMgr.executeQuery("GET_COLUMN_MAP_TEMPLATES", form);
				sb=new StringBuilder("");
				while(results.next()){
					String columnMapName=StringUtils.prepareCDATA(results.getFormattedValue("columnMapName"));
					Object customerId=results.get("customerId");
					String columnMapNameWithScope;
					String customerName = (String)results.get("customerName");
					if(customerId==null){
						columnMapNameWithScope="[Global] "+columnMapName;
					}else{
						columnMapNameWithScope="[" + customerName + "] "+columnMapName;
					}
					
					sb.append("<option title='").append(columnMapName).append("' value='").append(results.get("columnMapId")).append("' editable='").append(results.get("editable")).append("'>");
					sb.append(columnMapNameWithScope);
					sb.append("</option>");
				}
				if(sb.length()>0){
					responseText.append("$('columnMapId').set('html',\""+sb.toString()+"\");setMessage(0,'');");
				}
			}else if(searchFor.equalsIgnoreCase("location")){
				Results results = DataLayerMgr.executeQuery("GET_TERMINAL_LOCATIONS", form);
				StringBuilder sb=new StringBuilder("");
				sb.append("<option value='-1'>");
				sb.append("NO FILTER FOR LOCATION");
				sb.append("</option>");
				int resultCount=0;
				String locationName;
				while(results.next()){
					resultCount++;
					locationName=StringUtils.prepareCDATA(results.getFormattedValue("locationName"));
					sb.append("<option value='").append(locationName).append("'>");
					sb.append(locationName);
					sb.append("</option>");
				}
				if(resultCount>0){
					responseText.append("$('locationName').set('html',\""+sb.toString()+"\");setMessage(0,'');$('locationName').selectedIndex=1;");
				}else{
					responseText.append("setMessage(-1,'No location found.');");
				}
				
			}else if(searchFor.equalsIgnoreCase("mappedLocation")){
				Results results = DataLayerMgr.executeQuery("GET_MAPPED_TERMINAL_LOCATIONS", form);
				StringBuilder sb=new StringBuilder("");
				sb.append("<option value='-1'>");
				sb.append("NO FILTER FOR LOCATION");
				sb.append("</option>");
				int resultCount=0;
				String locationName;
				while(results.next()){
					resultCount++;
					locationName=StringUtils.prepareCDATA(results.getFormattedValue("mappedLocationName"));
					sb.append("<option value='").append(locationName).append("'>");
					sb.append(locationName);
					sb.append("</option>");
				}
				if(resultCount>0){
					responseText.append("$('mappedLocationName').set('html',\""+sb.toString()+"\");setMessage(0,'');$('mappedLocationName').selectedIndex=1;");
				}else{
					responseText.append("setMessage(-1,'No location found.');");
				}
				
			}else if(searchFor.equalsIgnoreCase("mapped")){
				String mappedDeviceSerialCd=form.getString("mappedDeviceSerialCd", false);
				if(!StringUtils.isBlank(mappedDeviceSerialCd)){
					if(mappedDeviceSerialCd.contains(",")){
						form.set("mappedDeviceSerialCd", mappedDeviceSerialCd.replaceAll("\\s","").split(","));
					}else{
						form.set("mappedDeviceSerialCdRegex", mappedDeviceSerialCd.replaceAll("\\s",""));
						
						form.set("mappedDeviceSerialCd", "");
					}
					
				}
				Results results = DataLayerMgr.executeQuery("GET_MAPPED_TERMINALS", form);
				StringBuilder sb=new StringBuilder("");
				int resultCount=0;
				String isAutoUpdatedMap;
				while(results.next()){
					resultCount++;
					isAutoUpdatedMap = results.getFormattedValue("autoUpdateColumnMap");
					String toHighlite = "";
					if (isAutoUpdatedMap.equals("N")) {
						toHighlite = " style='color:PURPLE; font-weight:bold' ";
					}
					sb.append("<option").append(toHighlite).append(" value='").append(results.get("terminalId")).append("_").append(results.get("columnMapId")).append("'>");
					sb.append(StringUtils.prepareCDATA(results.getFormattedValue("deviceSerialCd"))+"  ("+StringUtils.prepareCDATA(results.getFormattedValue("locationName"))+")");
					sb.append("</option>");
				}
				if(resultCount>0){
					responseText.append("$('mappedDeviceList').set('html',\""+sb.toString()+"\");setMessage(0,'');$('mappedDeviceList').selectedIndex=-1;");
				}else{
					responseText.append("setMessage(-1,'No mapped device found.');$('mappedDeviceList').set('html','');");
				}
			}else if(searchFor.equalsIgnoreCase("mappedByTemplate")){
					Results results = DataLayerMgr.executeQuery("GET_MAPPED_TERMINALS_BY_TEMPLATE", form);
					StringBuilder sb=new StringBuilder("");
					int resultCount=0;
					String isAutoUpdatedMap;
					while(results.next()){
						resultCount++;
						isAutoUpdatedMap = results.getFormattedValue("autoUpdateColumnMap");
						String toHighlite = "";
						if (isAutoUpdatedMap.equals("N")) {
							toHighlite = " style='color:PURPLE; font-weight:bold' ";
						}
						sb.append("<option").append(toHighlite).append(" value='").append(results.get("terminalId")).append("_").append(results.get("columnMapId")).append("'>");
						sb.append(StringUtils.prepareCDATA(results.getFormattedValue("deviceSerialCd"))+"  ("+StringUtils.prepareCDATA(results.getFormattedValue("locationName"))+")");
						sb.append("</option>");
					}
					if(resultCount>0){
						responseText.append("$('mappedDeviceList').set('html',\""+sb.toString()+"\");setMessage(0,'');$('mappedDeviceList').selectedIndex=-1;");
					}else{
						responseText.append("setMessage(-1,'No mapped device found.');$('mappedDeviceList').set('html','');");
					}
			}else if(searchFor.equalsIgnoreCase("unmapped")){
				String unmappedDeviceSerialCd=form.getString("unmappedDeviceSerialCd", false);
				if(!StringUtils.isBlank(unmappedDeviceSerialCd)){
					if(unmappedDeviceSerialCd.contains(",")){
						form.set("unmappedDeviceSerialCd", unmappedDeviceSerialCd.replaceAll("\\s","").split(","));
					}else{
						form.set("unmappedDeviceSerialCdRegex", unmappedDeviceSerialCd.replaceAll("\\s",""));
						form.set("unmappedDeviceSerialCd", "");
					}
					
				}
				Results results = DataLayerMgr.executeQuery("GET_UNMAPPED_TERMINALS", form);
				StringBuilder sb=new StringBuilder("");
				int resultCount=0;
				String isAutoUpdatedMap;
				while(results.next()){
					resultCount++;
					isAutoUpdatedMap = results.getFormattedValue("autoUpdateColumnMap");
					String toHighlite = "";
					if (isAutoUpdatedMap.equals("N")) {
						toHighlite = " style='color:PURPLE; font-weight:bold' ";
					}
					sb.append("<option").append(toHighlite).append(" value='").append(StringUtils.prepareCDATA(results.getFormattedValue("terminalId"))).append("'>");
					sb.append(StringUtils.prepareCDATA(results.getFormattedValue("deviceSerialCd"))+"  ("+StringUtils.prepareCDATA(results.getFormattedValue("locationName"))+")");
					sb.append("</option>");
				}
				if(resultCount>0){
					responseText.append("$('unmappedDeviceList').set('html',\""+sb.toString()+"\");setMessage(0,'');");
				}else{
					responseText.append("setMessage(-1,'No unmapped device found.');$('unmappedDeviceList').set('html','');");
				}
			}else if(searchFor.equalsIgnoreCase("columnMap")){
				
				Results results = DataLayerMgr.executeQuery("GET_COLUMN_MAP", form);
				StringBuilder sb=new StringBuilder("");
				while(results.next()){
					sb.append(StringUtils.escape(results.get("columnMapValue").toString(), javascriptEscape, '\\'));
				}
				if(sb.length()>0){
					responseText.append("$('columnMapContent').set('value','"+StringUtils.prepareJavascriptPrintable(sb.toString())+"');$('columnMapName').value=$('columnMapId').getSelected()[0].title;setMessage(0,'');");
				}else{
					responseText.append("setMessage(-1,'No column map value found.');");
				}
			}
			writeResponse(response, responseText.toString());
			log.info("searchFor="+searchFor+" is completed.");
		} catch(Exception e) {
			writeResponse(response, "setTemplateMessage(-1,'An application error occurred.');");
			log.error(e.getMessage(), e);
		} 				
	}
	
	public void writeResponse(HttpServletResponse response, String responseText) throws ServletException{
		try{
			PrintStream out = new PrintStream(response.getOutputStream());
			out.println("<script type=\"text/javascript\">//<![CDATA[");
			out.println(responseText);
			out.print("//]]></script>");
			out.println();
			out.flush();
		}catch(IOException e){
			throw new ServletException(e);
		}
	}
	
}
