package com.usatech.usalive.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertException;
import simple.io.Log;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;

public class ClearLinksStep extends AbstractStep {
	private static final Log log = Log.getLog();
	protected String usage;
	
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		try {
            String usage = getUsage();
            if(usage == null || (usage=usage.trim()).length() == 0)
            	usage = RequestUtils.getAttribute(request, "usage", String.class, true);
		    UsaliveUser user = RequestUtils.getAttribute(request, SimpleServlet.ATTRIBUTE_USER, UsaliveUser.class, false);
		    String key = usage + "_" + (user != null ? user.getUserId() : 0) + "_links";
			form.setAttribute(key, null, RequestUtils.SESSION_SCOPE);
		} catch(ConvertException e) {
			log.warn("Exception while clearing links", e);
			throw new ServletException("Could not clear links", e);
		} 
	}

	public String getUsage() {
		return usage;
	}

	public void setUsage(String usage) {
		this.usage = usage;
	}
}
