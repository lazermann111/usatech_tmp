package com.usatech.usalive.servlet;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import com.usatech.usalive.web.RefundUtils;

import simple.bean.ConvertUtils;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

public class RefundNotifyStep extends AbstractStep {
	@Override
	public void perform(Dispatcher dispatcher, InputForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException {
		int returnValue=ConvertUtils.getIntSafely(request.getAttribute("returnValue"),-1);
		if(returnValue==0){//refund success
			RefundUtils.notifyAdminUser(request, form, null);
		}
	}

}
