package com.usatech.usalive.servlet;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.DbUtils;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.steps.AbstractStep;

public class RegionsUpdateStep extends AbstractStep {

	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		try {
			Connection conn = DataLayerMgr.getConnectionForCall("UPDATE_TERMINALS_REGION");
			boolean okay = false;
			try {
				Map<String,Object> params = new HashMap<String,Object>();
				String region;
				for(int i = 0; (region = form.getString("region" + i,false)) != null; i++) {
					if(region.length() == 0) 
						params.remove("regionName");
					else {
						params.put("regionName", region);
					}
					int[] tids = form.getIntArray("terminals" + i,false);
					if(tids != null && tids.length > 0) {
						params.put("terminalIds", tids);
						params.put("action", "VIEW");
						params.put("checkUser", RequestUtils.getUser(request));
						params.put("terminalIds", tids);
						DataLayerMgr.executeCall(conn, "CHECK_TERMINALS_PERMISSION", params);
						boolean permitted = ConvertUtils.getBoolean(params.get("permitted"));
						if(!permitted) {
							form.set("onloadMessage", form.getTranslator().translate("region.change.notauthorized", "You are not authorized to change the region for one of the devices listed. Please try again with the new list."));
							return;
						}
						DataLayerMgr.executeCall(conn, "UPDATE_TERMINALS_REGION", params);
					}
				}
				conn.commit();
				okay = true;
			} finally {
				if(!okay)
					DbUtils.rollbackSafely(conn);
				conn.close();
			}
		} catch(SQLException e) {
			throw new ServletException("Could not update regions because of system error. Please try again.", e);
		} catch(DataLayerException e) {
			throw new ServletException("Could not update regions because of system error. Please try again.", e);
		} catch(ConvertException e) {
			throw new ServletException("Could not update regions because of system error. Please try again.", e);
		}
	}
}
