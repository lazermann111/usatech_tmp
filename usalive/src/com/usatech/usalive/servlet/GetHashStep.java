package com.usatech.usalive.servlet;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.MaskedString;
import simple.bean.ReflectionUtils;
import simple.io.Base64;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.StepConfigException;
import simple.servlet.steps.AbstractStep;

public class GetHashStep extends AbstractStep {
	protected String algorithm = "SHA1";
	protected String bean;
	protected String result;
	protected MessageDigest digest;
	protected boolean base64 = true;
	protected final ReentrantLock lock = new ReentrantLock();

    public GetHashStep() {
    }

    @Override
    public void configure(Map<String,?> props) throws StepConfigException {
    	super.configure(props);
		try {
			digest = MessageDigest.getInstance(getAlgorithm());
		} catch(NoSuchAlgorithmException e) {
			throw new StepConfigException(e);
		}
    }
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
    	Object value;
        try {
			value = ReflectionUtils.getProperty(form, getBean());
		} catch (IntrospectionException e) {
			throw new ServletException(e);
		} catch (IllegalAccessException e) {
			throw new ServletException(e);
		} catch (InvocationTargetException e) {
			throw new ServletException(e);
		} catch(ParseException e) {
			throw new ServletException(e);
		}
		if(value != null)
			form.setAttribute(getResult(), hash(value));
    }
    protected String hash(Object value) throws ServletException {
    	if(value == null)
    		return null;
    	byte[] bytes;
		if(value instanceof MaskedString)
			bytes = ((MaskedString)value).getValue().getBytes();
		else try {
			bytes = ConvertUtils.convert(byte[].class, value);
		} catch(ConvertException e) {
			throw new ServletException(e);
		}
		if(bytes == null)
    		return null;
    	lock.lock();
		try {
			bytes = digest.digest(bytes);
		} finally {
			lock.unlock();
		}
		if(isBase64())
			return Base64.encodeBytes(bytes, true);
		else
			return new String(bytes);
	}

	public String getAlgorithm() {
		return algorithm;
	}

	public void setAlgorithm(String algorithm) {
		this.algorithm = algorithm;
	}

	public String getBean() {
		return bean;
	}

	public void setBean(String bean) {
		this.bean = bean;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public boolean isBase64() {
		return base64;
	}

	public void setBase64(boolean base64) {
		this.base64 = base64;
	}

}
