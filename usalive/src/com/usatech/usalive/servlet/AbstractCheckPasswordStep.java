package com.usatech.usalive.servlet;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.lang.InvalidStringValueException;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;
import simple.text.Hasher;
/**
 * Updates the transport properties
 * 
 * @author Brian S. Krug
 *  
 */
public abstract class AbstractCheckPasswordStep extends AbstractStep {
	static final Log log = Log.getLog();
	
	protected static final Pattern hasUpperCheck = Pattern.compile("[A-Z]");
	protected static final Pattern hasLowerCheck = Pattern.compile("[a-z]");
	protected static final Pattern hasNonAlphaCheck = Pattern.compile("[!-@\\[-^`{-~]");

	protected final Hasher hasher;
	protected final SecureRandom random = new SecureRandom();
	protected int saltSize = 32;
	protected int minLength = 8;
	protected boolean upperCaseRequired = true;
	protected boolean lowerCaseRequired = true;
	protected boolean nonAlphaRequired = true;
	protected String passwordFieldName = "editPassword";
	protected String confirmFieldName = "editConfirm";
	protected String userIdFieldName = "editUserId";
	
	public AbstractCheckPasswordStep() throws NoSuchAlgorithmException {
		hasher = new Hasher("SHA1", true);
	}
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		checkPassword(form);
	}

	abstract public void checkPassword(InputForm form) throws ServletException;
	
	public int getMinLength() {
		return minLength;
	}
	public void setMinLength(int minLength) {
		this.minLength = minLength;
	}
	public boolean isUpperCaseRequired() {
		return upperCaseRequired;
	}
	public void setUpperCaseRequired(boolean upperCaseRequired) {
		this.upperCaseRequired = upperCaseRequired;
	}
	public boolean isLowerCaseRequired() {
		return lowerCaseRequired;
	}
	public void setLowerCaseRequired(boolean lowerCaseRequired) {
		this.lowerCaseRequired = lowerCaseRequired;
	}
	public boolean isNonAlphaRequired() {
		return nonAlphaRequired;
	}
	public void setNonAlphaRequired(boolean nonAlphaRequired) {
		this.nonAlphaRequired = nonAlphaRequired;
	}
	public int getSaltSize() {
		return saltSize;
	}
	public void setSaltSize(int saltSize) {
		this.saltSize = saltSize;
	}
	public String getPasswordFieldName()
	{
		return passwordFieldName;
	}
	public void setPasswordFieldName(String passwordFieldName)
	{
		this.passwordFieldName = passwordFieldName;
	}
	public String getConfirmFieldName()
	{
		return confirmFieldName;
	}
	public void setConfirmFieldName(String confirmFieldName)
	{
		this.confirmFieldName = confirmFieldName;
	}

}
