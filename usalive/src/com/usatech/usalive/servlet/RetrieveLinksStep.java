package com.usatech.usalive.servlet;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.ScrollableResults;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;

import com.usatech.usalive.link.LinkRequirementEngine;

public class RetrieveLinksStep extends AbstractStep {
	private static final Log log = Log.getLog();
	protected String usage;
	protected String results = "links";
	
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		try {
            String usage = getUsage();
            if(usage == null || (usage=usage.trim()).length() == 0)
            	usage = RequestUtils.getAttribute(request, "usage", String.class, true);
		    UsaliveUser user = RequestUtils.getAttribute(request, SimpleServlet.ATTRIBUTE_USER, UsaliveUser.class, false);
		    String key = usage + "_" + (user != null ? user.getUserId() : 0) + "_links";
		    ScrollableResults links =  RequestUtils.getAttribute(request, key, ScrollableResults.class, false);
		    if(links == null) {
	            Map<String,Object> params = new HashMap<String,Object>();
	            params.put("user", user);
	            params.put("usage", usage);
                LinkRequirementEngine lre = RequestUtils.getAttribute(request, "com.usatech.usalive.link.LinkRequirementEngine", LinkRequirementEngine.class, true);
                params.put("satisfiedRequirementIds", lre.getSatisfiedRequirements(user, request));
                links = (ScrollableResults)DataLayerMgr.executeQuery("GET_WEB_LINKS_WITH_PRIVS", params);             
	            form.setAttribute(key, links, "session");
	        }
	        form.setAttribute(getResults(), links.clone());
		} catch (SQLException e) {
            log.warn("Exception while retrieving links", e);
            throw new ServletException("Could not retrieve links",e);
        } catch (DataLayerException e) {
            log.warn("Exception while retrieving links", e);
            throw new ServletException("Could not retrieve links",e);
        } catch(ConvertException e) {
        	log.warn("Exception while retrieving links", e);
            throw new ServletException("Could not retrieve links",e);
		} 
	}

	public String getUsage() {
		return usage;
	}

	public void setUsage(String usage) {
		this.usage = usage;
	}

	public String getResults() {
		return results;
	}

	public void setResults(String results) {
		this.results = results;
	}
}
