package com.usatech.usalive.servlet;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.SimpleServlet;
import simple.servlet.steps.AbstractStep;
import simple.servlet.steps.LoginFailureException;
/**
 * Updates the transport properties
 * 
 * @author Brian S. Krug
 *  
 */
public class BeUserStep extends AbstractStep {
	//private static final Log log = Log.getLog();
	protected Pattern internalUsernamePattern = Pattern.compile("([^@]+)@usatech\\.com");
	
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		UsaliveUser user;
		long profileId;
		try {
			user = RequestUtils.getAttribute(request, SimpleServlet.ATTRIBUTE_USER, UsaliveUser.class, true, "session");
			profileId = RequestUtils.getAttribute(request, "editUserId", Long.class, true);
		} catch(ConvertException e) {
			throw new ServletException(e);
		}
		UsaliveUser newUser = new UsaliveUser();
		newUser.setUserId(profileId);
		newUser.setMasterUserId(user.getMasterUserId());
		newUser.setMasterUser(user);
		newUser.setTimeZone(user.getTimeZone());
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("user", newUser);
		params.put("profileId", profileId);
		
		try {
			DataLayerMgr.executeCall("LOGIN_AS_USER", params, true);
		} catch(SQLException e) {
			if(e.getErrorCode() == 20100) {
				throw new LoginFailureException("User '" + user.getUserName() + "' may not log on requested user (" + profileId + ")", LoginFailureException.INVALID_CREDENTIALS);
			}
			throw new ServletException(e);
		} catch(DataLayerException e) {
			throw new ServletException(e);
		} 
		if(newUser.getUserType() != 8 && internalUsernamePattern.matcher(newUser.getUserName()).matches()){
			newUser.setInternal(true);
		}
		LoginStep.userLoggedIn(user.getUserName(), newUser, form);   
		RequestUtils.setAttribute(request, SimpleServlet.ATTRIBUTE_USER, newUser, "session");
	}
	
}
