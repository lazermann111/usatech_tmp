package com.usatech.usalive.servlet;

public enum UserRequestStatus {
	PENDING, PENDING_NOTIFY, NOTIFY, READY, PULLED
}
