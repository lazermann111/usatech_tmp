package com.usatech.usalive.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;
import simple.text.Censor;

public class PreferenceUpdateStep extends AbstractStep {

	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		try {
			form.set("doingBusinessAs", Censor.sanitizeText(form.getStringSafely("doingBusinessAs", "").trim()));
		} catch(IOException e) {
			throw new ServletException(e);
		}
	}
}
