package com.usatech.usalive.servlet;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.steps.AbstractStep;

public class FilterCollectionStep extends AbstractStep {
	protected String collection;
	protected String filter;
	protected String result;
	protected String scope;
	protected Class<?> componentType;
	
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		final Collection<Object> resultObject = new HashSet<Object>();
		Set<?> filterObject;
		try {
			Object value = form.getAttribute(getCollection());
			if(value != null) {			
				Collection<?> collectionObject = ConvertUtils.asCollection(value);
				if(getComponentType() == null)
					resultObject.addAll(collectionObject);
				else {
					for(Object o : collectionObject) {
						resultObject.add(ConvertUtils.convert(getComponentType(), o));
					}
				}
			}
			value = form.getAttribute(getFilter());
			if(value != null) {			
				if(getComponentType() == null)
					filterObject = ConvertUtils.convert(Set.class, value);
				else {
					Collection<?> tmp = ConvertUtils.asCollection(value);
					Set<Object> f = new HashSet<Object>();
					for(Object o : tmp) {
						f.add(ConvertUtils.convert(getComponentType(), o));
					}
					filterObject = f;
				}
				resultObject.retainAll(filterObject);
			}
		} catch(ConvertException e) {
			throw new ServletException(e);
		}
		form.setAttribute(getResult(), resultObject, getScope() == null ? RequestUtils.REQUEST_SCOPE : getScope());
	}
	public String getCollection() {
		return collection;
	}
	public void setCollection(String collection) {
		this.collection = collection;
	}
	public String getFilter() {
		return filter;
	}
	public void setFilter(String filter) {
		this.filter = filter;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	public Class<?> getComponentType() {
		return componentType;
	}
	public void setComponentType(Class<?> componentType) {
		this.componentType = componentType;
	}

}
