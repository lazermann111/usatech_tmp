/*
 * Created on May 9, 2005
 *
 */
package com.usatech.usalive.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import simple.bean.ConvertException;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.Results;

public class SubdomainFeatureFilter implements Filter {
    public static final String CSS_PATH_ATTRIBUTE = "CUSTOM_CSS_PATH";
    public static final String FEATURES_ATTRIBUTE = "CUSTOM_FEATURES";   
    
    protected static class FeatureCache {
        public Map<String, String> features;
        public String cssPath;
    }
    
    public SubdomainFeatureFilter() {
        super();
    }

    public void init(FilterConfig filterConfig) throws ServletException {
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
            ServletException {
        //get the css path from the subdomain
        if(request instanceof HttpServletRequest) {  
            String hostName = request.getServerName();
            HttpSession session = ((HttpServletRequest)request).getSession();
            FeatureCache cache = (FeatureCache)session.getAttribute(hostName);
            if(cache == null) {
                cache = new FeatureCache();
                session.setAttribute(hostName, cache);
                try {
                    cache.features = getFeatures(hostName);
                } catch(SQLException e) {
                    throw new ServletException(e);
                } catch(DataLayerException e) {
                    throw new ServletException(e);
                } catch(ConvertException e) {
                    throw new ServletException(e);
                }
                cache.cssPath = getCSSPath(cache.features);               
            }               
            request.setAttribute(CSS_PATH_ATTRIBUTE, cache.cssPath);
            request.setAttribute(FEATURES_ATTRIBUTE, cache.features);
        }
        chain.doFilter(request, response);
    }

    protected Map<String,String> getFeatures(String hostName) throws SQLException, DataLayerException, ConvertException {
        Map<String,String> features = new HashMap<String,String>();
        Results results = DataLayerMgr.executeQuery("GET_SUBDOMAIN_FEATURES", new Object[] {hostName});
        try { 
            while(results.next()) {
                features.put(results.getFormattedValue("FEATURE_KEY"), results.getFormattedValue("FEATURE_VALUE"));
            }
        } finally {
            results.close();
        }
        return features;
    }

    protected String getCSSPath(Map<String,String> features) {
        return features.get(CSS_PATH_ATTRIBUTE);
    }
    
    public void destroy() {
    }
}
