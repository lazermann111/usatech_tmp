package com.usatech.usalive.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.db.DbUtils;
import simple.io.Log;
import simple.results.Results;
import simple.servlet.*;
import simple.servlet.steps.AbstractStep;
import simple.servlet.steps.DbCacheStep;
import simple.text.Censor;
import simple.text.StringUtils;

import com.usatech.report.custom.GenerateUtils;

public class DeviceUpdateStep extends AbstractStep {
	private static final Log log = Log.getLog();

	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		try {
			Connection conn = DataLayerMgr.getConnectionForCall("UPDATE_TERMINAL");
			boolean okay = false;
			try {
				Integer custBankId = RequestUtils.getAttribute(request, "custBankId", Integer.class, false);
				if(custBankId != null && custBankId != 0) {
					Results results = DataLayerMgr.executeQuery(conn, "CHECK_BANK_ACCT_FOR_TERMINAL", form);
					if(!results.next()) {
						form.setAttribute("errorMessage", form.getTranslator().translate("terminal.change.error.invalid.bankacct", "An invalid Bank Account was selected. Please choose a different one.", custBankId));
						return;
					}
					
					if(!"A".equals(results.getValue("bankAcctStatus", String.class))) {
						form.setAttribute("errorMessage", form.getTranslator().translate("terminal.change.error.inactive.bankacct", "An inactive Bank Account was selected. Please choose a different one.", custBankId));
						return;
					}
				}
				
				Object addressId = form.getAttribute("addressId");
				if(addressId == null || addressId.toString().isEmpty()) {
					form.setAttribute("addressId", 0);
				}
				GenerateUtils.formatPostal(null, form);
				
				Integer primaryContactId = RequestUtils.getAttribute(request, "primaryContactId", Integer.class, false);
				
				// Integer locationTypeId = RequestUtils.getAttribute(request, "locationTypeId", Integer.class, false);
				// Character dexData = RequestUtils.getAttribute(request, "dexData", Character.class, false);
				// Character receipt = RequestUtils.getAttribute(request, "receipt", Character.class, false);
				
				String error = null;
				if(primaryContactId == null || primaryContactId == 0) {
					error = form.getTranslator().translate("terminal.change.error.missing", "A value for \"{0}\" was not provided. Please enter one.", "Primary Contact");
				} /*else if(locationTypeId == null || locationTypeId == 0) {
					error = form.getTranslator().translate("terminal.change.error.missing", "A value for \"{0}\" was not provided. Please enter one.", "Location Type");
					} else if(dexData == null) {
					error = form.getTranslator().translate("terminal.change.error.missing", "A value for \"{0}\" was not provided. Please enter one.", "DEX Data Capture");
					} else if(receipt == null) {
					error = form.getTranslator().translate("terminal.change.error.missing", "A value for \"{0}\" was not provided. Please enter one.", "Print a receipt?");
					}*/
				
				Results results = DataLayerMgr.executeQuery(conn, "GET_DEVICE_TYPE_FOR_TERMINAL", form);
				Integer deviceTypeId;
				String deviceSerialCd;
				boolean posTypeEditable=false;
				if(results.next()) {
					deviceTypeId = results.getValue("deviceTypeId", Integer.class);
					deviceSerialCd = results.getValue("deviceSerialCd", String.class);
					posTypeEditable = results.getValue("posTypeEditable", Boolean.class);
				} else {
					deviceTypeId = null;
					deviceSerialCd = null;
				}
				
				boolean isKiosk = deviceTypeId != null && deviceTypeId == 11 && deviceSerialCd != null && deviceSerialCd.startsWith("K3");
				
				if(error != null && deviceTypeId != null && deviceTypeId != 6) {
					form.setAttribute("errorMessage", error);
					return;
				}

				String qsFeeInactivityDaysStr = form.getString("qsFeeInactivityDays", false);
				if (!StringUtils.isBlank(qsFeeInactivityDaysStr)) {
					if (!qsFeeInactivityDaysStr.matches("^\\d+$")) {
						form.setAttribute("errorMessage", "Non-digital value for Inactivity Days!");
						return;
					}
				}

				Integer machineId = RequestUtils.getAttribute(request, "machineId", Integer.class, false);
				if(machineId == null || machineId == 0) { 
					DataLayerMgr.executeCall(conn, "CREATE_MACHINE", form);
					DbCacheStep.clearCache(request, "GET_MACHINE_TYPES");
				}
				
				if(isKiosk&&posTypeEditable) {
					char mobileIndicator = ConvertUtils.convertRequired(Character.class, RequestUtils.getAttribute(request, "mobileIndicator", true));
					Set<Character> entryList = ConvertUtils.convert(Set.class, Character.class, RequestUtils.getAttribute(request, "entryList", false));
					char posEnvironment = '?';
					switch(mobileIndicator) {
						case 'Y':
							entryList.add('M');
							posEnvironment = 'C'; // Mobile
							break;
						case 'N':
							char terminalActivator = ConvertUtils.convertRequired(Character.class, RequestUtils.getAttribute(request, "terminalActivator", true));
							switch(terminalActivator) {
								case 'C':
									if(!entryList.contains('M') || entryList.contains('S') || entryList.contains('P') || entryList.contains('E'))
										posEnvironment = 'U'; // Unattended
									else
										posEnvironment = 'E'; // Ecommerce
									break;
								case 'A':
									if(entryList.contains('S'))
										posEnvironment = 'A'; // Attended
									else
										posEnvironment = 'M'; // MOTO
									break;
								default:
									form.setAttribute("errorMessage", form.getTranslator().translate("terminal.change.invalid.terminalActivator", "Invalid value for Card Data Entered By; Please select a valid option"));
									return;
							}
							break;
						default:
							form.setAttribute("errorMessage", form.getTranslator().translate("terminal.change.invalid.mobileIndicator", "Invalid value for Machine is Tablet / Phone; Please select a valid option"));
							return;
					}
					
					form.set("posEnvironment", posEnvironment);
					form.set("pinCapability", entryList.contains('M') ? 'Y' : 'N');
					
					char[] entryCapability = new char[entryList.size()];
					int i = 0;
					for(Character ec : entryList)
						if(ec != null)
							entryCapability[i++] = ec;
					form.set("entryCapability", new String(entryCapability, 0, i));
				}
				
				try {
					form.set("doingBusinessAs", Censor.sanitizeText(form.getStringSafely("doingBusinessAs", "").trim()));
					DataLayerMgr.executeCall(conn, "UPDATE_TERMINAL", form);
				} catch(SQLException e) {
					log.warn("Exception while attemping to update terminal", e);
					switch(e.getErrorCode()) {
						case 20220:
							form.setAttribute("errorMessage", form.getTranslator().translate("terminal.change.invalid.state", "Invalid state \"{1}\" for country \"{2}\" specified for device \"{0}\"", form.getAttribute("serial"), form.getAttribute("state"), form.getAttribute("country")));
							break;
						case 20217:
							form.setAttribute("errorMessage", form.getTranslator().translate("terminal.change.invalid.paysched", "Not permitted to change payment schedule as requested for device \"{0}\"", form.getAttribute("serial")));
							break;
						case 20218:
							form.setAttribute("errorMessage", form.getTranslator().translate("terminal.change.invalid.region", "The Region you select is no longer valid for device \"{0}\". Please select another region", form.getAttribute("serial")));
							break;
						default:
							throw e;
					}
					return;
				}
				
				conn.commit();
				okay = true;
				form.setAttribute("successMessage", form.getTranslator().translate("terminal.change.success", "Successfully updated Device \"{0}\" at location \"{1}\"", form.getAttribute("serial"), form.getAttribute("location")));
			} finally {
				if(!okay)
					DbUtils.rollbackSafely(conn);
				conn.close();
			}
		} catch(SQLException e) {
			throw new ServletException("Could not update terminal because of system error. Please try again.", e);
		} catch(DataLayerException e) {
			throw new ServletException("Could not update terminal because of system error. Please try again.", e);
		} catch(ConvertException e) {
			throw new ServletException("Could not update terminal because of system error. Please try again.", e);
		} catch(IOException e) {
			throw new ServletException("Could not update terminal because of system error. Please try again.", e);
		}
	}

}
