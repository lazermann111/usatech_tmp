package com.usatech.usalive.servlet;

import java.awt.image.BufferedImage;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.lang.InvalidValueException;
import simple.servlet.Dispatcher;
import simple.servlet.InputFile;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.steps.AbstractStep;
import simple.text.StringUtils;

public class ManageCompanyLogoStep extends AbstractStep {
	public static int imageMaxSize=51200;
	public static int imageMaxHeight=75;
	public static int imageWidth=100;
	private static final Log log = Log.getLog();
	public ManageCompanyLogoStep(){
		super();
	}
	
	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		InputForm inputForm = RequestUtils.getInputForm(request);
		String actionType=ConvertUtils.getStringSafely(inputForm.get("actionType"));
		if(!StringUtils.isBlank(actionType)&&actionType.equalsIgnoreCase("deleteCompanyLogo")){
			UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
			try{
				DataLayerMgr.executeCall("DELETE_COMPANY_LOGO", new Object[]{USALiveUtils.companyLogoPrefix+user.getCustomerId()}, true);
				RequestUtils.setAttribute(request, "successMessage", "Company logo deleted successfully", "request");
			} catch(Exception e) {
				log.info("Failed to delete customer company logo", e);
				RequestUtils.setAttribute(request, "errorMessage", "Failed to delete company logo. Please try again", "request");
			} 
		}else{
			Object logoImage = inputForm.getAttribute("companyLogo");
			if(logoImage!=null){
				
				try {
					validateImage(logoImage);
					UsaliveUser user = (UsaliveUser) RequestUtils.getUser(request);
					if(logoImage instanceof InputFile) {
		        		InputFile inputFile = (InputFile)logoImage;
		        		if(inputFile.getLength()>0){
			        		InputStream inputStream = inputFile.getInputStream();
			        		BufferedImage readImage =ImageIO.read(inputStream);
			        		int height = readImage.getHeight();
			        		if(height>imageMaxHeight){
			        			throw new InvalidValueException("Company logo image file height exceeds 75. Please try another one");
			        		}
			        		inputStream = inputFile.getInputStream();
			        		inputForm.set("companyLogo", inputStream);
			        		inputForm.set("contentType", inputFile.getContentType());
			        		inputForm.set("fileName", USALiveUtils.companyLogoPrefix+user.getCustomerId());
				        	DataLayerMgr.executeCall("UPSERT_COMPANY_LOGO", inputForm, true);
		        		}
		        	}
				} catch(Exception e) {
					log.info("Failed to upsert customer company logo", e);
					throw new ServletException(e);
				} 
			}
		}
	}
	
	public void validateImage(Object logoImage) throws InvalidValueException{
		if(logoImage instanceof InputFile) {
			InputFile inputFile = (InputFile)logoImage;
			if(inputFile.getLength()!=0){
				if(inputFile.getLength()>imageMaxSize){
					throw new InvalidValueException("Company logo image file exceeds the maximum size. Please try another one");
				}else{
					String contentType=inputFile.getContentType();
					if(!(!StringUtils.isBlank(contentType)&&(contentType.contains("image/jpeg")||contentType.contains("image/png")||contentType.contains("image/gif")))){
						throw new InvalidValueException("Company logo image file needs to be in JPEG,PNG or GIF format. Please try another one");
					}
				}
			}
		}else{
			throw new InvalidValueException("Company logo image file is invalid. Please try another one");
		}
	}

	public static int getImageMaxSize() {
		return imageMaxSize;
	}

	public static void setImageMaxSize(int imageMaxSize) {
		ManageCompanyLogoStep.imageMaxSize = imageMaxSize;
	}

	public static int getImageMaxHeight() {
		return imageMaxHeight;
	}

	public static void setImageMaxHeight(int imageMaxHeight) {
		ManageCompanyLogoStep.imageMaxHeight = imageMaxHeight;
	}

	public static int getImageWidth() {
		return imageWidth;
	}

	public static void setImageWidth(int imageWidth) {
		ManageCompanyLogoStep.imageWidth = imageWidth;
	}
	
}
