package com.usatech.usalive.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.steps.AbstractStep;
/**
 * 
 * @author yhe
 *
 */
public class ConfigBestVendorReportingStep extends AbstractStep {
	private static final Log log = Log.getLog();

	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String statusMessage;
		String actionType=null;
		try {
			actionType=ConvertUtils.getString(RequestUtils.getAttribute(request, "actionType", true), true);
			if(actionType.equalsIgnoreCase("add")){
				DataLayerMgr.executeCall("ADD_BEST_VENDOR_REPORTING", form, true);
				statusMessage="<p class=\"status-info-success\">Successfully added best vendor reporting for the customer.</p>";
			}else{
				DataLayerMgr.executeCall("REMOVE_BEST_VENDOR_REPORTING", form, true);
				statusMessage="<p class=\"status-info-success\">Successfully removed best vendor reporting for the customer.</p>";
			}
		}catch(Exception e) {
			statusMessage="<p class=\"status-info-failure\">Failed to "+actionType+" best vendor reporting for the customer. Exception:"+e.getMessage()+"</p>";
			log.error("Failed to "+actionType+" best vendor reporting", e);
		} 
		try{
			response.getOutputStream().write(statusMessage.getBytes());
		}catch(IOException e){
			throw new ServletException("Failed to "+actionType+" best vendor reporting", e);
		}
	}

}
