package com.usatech.usalive.servlet;


import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usatech.layers.common.util.WebHelper;

import simple.lang.InvalidStringValueException;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;
import simple.text.StringUtils;

public class CheckEmailStep extends AbstractStep {
	
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String email = form.getString("editEmail", false);
		if(!StringUtils.isBlank(email)){
			try{
				WebHelper.checkEmail(email);
			}catch(MessagingException e){
				String message="Invalid email address. Please input a valid one.";
				form.setAttribute("messageClass", "status-info-failure");
				form.setAttribute("messageText", message);
				throw new ServletException(new InvalidStringValueException(message));
			}
		}
	}
	
}
