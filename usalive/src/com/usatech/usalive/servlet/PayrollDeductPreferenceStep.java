package com.usatech.usalive.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerMgr;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

import com.usatech.layers.common.model.PayrollSchedule;

public class PayrollDeductPreferenceStep extends AbstractStep {
	
	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		try {
			PayrollSchedule payrollSchedule = PayrollSchedule.loadFrom(form);
			if (payrollSchedule == null)
				return;
			
			form.setAttribute("payrollScheduleType", payrollSchedule.getPayrollScheduleType().toString());
			form.setAttribute("payrollScheduleData", payrollSchedule.getPayrollScheduleData());
				
			DataLayerMgr.executeCall("UPDATE_CUSTOMER_PAY_PERIOD", form, true);
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}

}
