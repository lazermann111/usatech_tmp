package com.usatech.usalive.servlet;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.steps.AbstractStep;

import com.usatech.usalive.web.RefundUtils;

public class IssueRefundsetStep extends AbstractStep {
	private static final Log log = Log.getLog();

	public IssueRefundsetStep(){
		super();
	}
	

	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
		String[] transactionIdSet=form.get("transactionIdSet").toString().split(",");
		StringBuilder refundResult=new StringBuilder();
		try {
			BigDecimal refundAmount=ConvertUtils.convert(BigDecimal.class, form.get("refundAmount"));
			BigDecimal remainingAmount, refundedAmount;
			if(form.getBoolean("managerOverride", false, false)){
				form.setAttribute("override", 'Y');
			}else{
				form.setAttribute("override", 'N');
			}
			HashMap<Long,BigDecimal> multiRefundMap=new HashMap<Long,BigDecimal>();
			for(int i=0;i<transactionIdSet.length;i++){
				form.set("transactionId", transactionIdSet[i]);
				if(i!=transactionIdSet.length-1){
					form.set("hasMore", 'Y');
				}else{
					form.set("hasMore", 'N');
				}
				RequestUtils.setPerformerUserAttribute(form,
						user.getUserId(),
						user.getMasterUser() == null ? 0 : user.getMasterUser().getUserId(),
						user.isInternal(),
						user.getMasterUser() != null && user.getMasterUser().isInternal());
				try {
					DataLayerMgr.executeCall("ISSUE_REFUND_WITH_REMAINS", form, true);
				} catch(SQLException e) {
					if((e.getErrorCode() >= 20000) && (e.getErrorCode() <= 20999)) {
						refundResult.append("<div class='status-info-failure'>Could not create refund for transactionId=" + transactionIdSet[i] + ": " + getFirstOraExceptionMessage(e.getMessage()) + "\n</div>");
						form.set("returnValue", -100);
					} else {
						throw e;
					}
				}
				int returnValue=form.getInt("returnValue", true, 0);
				remainingAmount=form.getBigDecimal("remainingAmount", false);
				remainingAmount = remainingAmount == null ? new BigDecimal(0) : remainingAmount;
				refundedAmount=refundAmount.subtract(remainingAmount);
				refundAmount=remainingAmount;
				form.set("refundAmount", refundAmount);
				log.info("Refund for transactionId="+transactionIdSet[i]+" has returnValue="+returnValue);
				if(returnValue==0){
					if(!user.isInternal() && (user.getMasterUser() == null || !user.getMasterUser().isInternal())) {
						multiRefundMap.put(ConvertUtils.getLong(transactionIdSet[i]), refundedAmount);
					}
					refundResult.append("<div class='status-info-success'>Successfully created refund for transactionId="+transactionIdSet[i]+" refundedAmount="+RefundUtils.refundDisplayFormat.format(refundedAmount)+"\n</div>");
				}else if(returnValue==1){
					refundResult.append("<div class='status-info-success'>Successfully created manager override refund for transactionId="+transactionIdSet[i]+" refundedAmount="+RefundUtils.refundDisplayFormat.format(refundedAmount)+"\n</div>");
				}else if(returnValue==2){
					if(!user.isInternal() && (user.getMasterUser() == null || !user.getMasterUser().isInternal())) {
						multiRefundMap.put(ConvertUtils.getLong(transactionIdSet[i]), refundedAmount);
					}
					refundResult.append("<div class='status-info-success'>Successfully created refund for transactionId="+transactionIdSet[i]+" refundedAmount="+RefundUtils.refundDisplayFormat.format(refundedAmount)+"\n</div>");
				}else if(returnValue==3){
					refundResult.append("<div class='status-info-success'>Successfully recognized previous refund and no refund for transactionId="+transactionIdSet[i]+" </div>");
				}else if(returnValue==-1){
					refundResult.append("<div class='status-info-failure'>Failed to create refund for transactionId="+transactionIdSet[i]+" refundedAmount="+RefundUtils.refundDisplayFormat.format(refundedAmount)+"\n</div>");
				}else if(returnValue==-2){
					refundResult.append("<div class='status-info-failure'>Failed to create manager override refund for transactionId="+transactionIdSet[i]+" refundedAmount="+RefundUtils.refundDisplayFormat.format(refundedAmount)+"\n</div>");
				}else if(returnValue==-100){
					// A message is already appended to the refundResult in case of DB-exception
				}else{
					refundResult.append("<div class='status-info-failure'>Unknown refund result code="+returnValue+" for transactionId="+transactionIdSet[i]+" refundedAmount="+RefundUtils.refundDisplayFormat.format(refundedAmount)+"\n</div>");
				}
				if(refundAmount.compareTo(BigDecimal.ZERO)<=0){
					break;
				}
			}
			if(multiRefundMap.size()>0){
				RefundUtils.notifyAdminUser(request, form, multiRefundMap);
			}
			OutputStream ostream = response.getOutputStream();
			response.setContentType("text/html");
			ostream.write(refundResult.toString().getBytes());
		}catch(ConvertException e){
			throw new ServletException(e);
		}catch(SQLException e) {
			throw new ServletException(e);
		} catch(DataLayerException e) {
			throw new ServletException(e);
		}catch(IOException e) {
			throw new ServletException(e);
		}
	}
	
	private String getFirstOraExceptionMessage(String message) {
		Integer firstORAIndex = findFirstORAIndex(message);
		if (firstORAIndex == null) {
			return null;
		}
		Integer secondORAIndex = findSecondORAIndex(message);
		if (secondORAIndex == null) {
			return null;
		}
		return message.substring(firstORAIndex + 11, secondORAIndex - 1);
	}

	private Integer findSecondORAIndex(String message) {
		Integer ret = message.indexOf("ORA-", 11);
		return ret == -1 ? null : ret;
	}

	private Integer findFirstORAIndex(String message) {
		if (message.startsWith("ORA-")) {
			return 0;
		}
		return null;
	}

}
