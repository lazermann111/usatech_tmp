package com.usatech.usalive.servlet;

import java.sql.SQLException;
import java.util.Arrays;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.steps.AbstractStep;
import simple.xml.sax.MessagesInputSource;
/**
 * Updates the user-terminal privileges
 * 
 * @author Brian S. Krug
 *  
 */
public class UpdateUserTerminalStep extends AbstractStep {
	private static final Log log = Log.getLog();
	public UpdateUserTerminalStep() {
	}
	
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		long[] noneTerminalIds;
		long[] viewTerminalIds;
		long[] editTerminalIds;
		try {
			noneTerminalIds = ConvertUtils.convert(long[].class, form.get("none"));
			viewTerminalIds = ConvertUtils.convert(long[].class, form.get("view"));
			editTerminalIds = ConvertUtils.convert(long[].class, form.get("edit"));
		} catch(ConvertException e) {
			throw new ServletException(e);
		}
		try {
			DataLayerMgr.executeCall("UPDATE_USER_TERMINAL_PRIVS", form, true);
		} catch(SQLException e) {
			throw new ServletException(e);
		} catch(DataLayerException e) {
			throw new ServletException(e);
		}		
		log.info("Removed privileges from " + Arrays.toString(noneTerminalIds) + "; add View privilege to " + Arrays.toString(viewTerminalIds) + "; add Edit privilege to " + Arrays.toString(editTerminalIds));
		final MessagesInputSource mis = RequestUtils.getOrCreateMessagesSource(form);
		if(noneTerminalIds != null && noneTerminalIds.length > 0) {
			mis.addMessage("success", "user.terminal.update.revoke", noneTerminalIds.length); //("Removed privileges for " + noneTerminalIds.length + " terminals");
		}
		if(viewTerminalIds != null && viewTerminalIds.length > 0) {
			mis.addMessage("success", "user.terminal.update.view", viewTerminalIds.length); //"Added view privilege for " + viewTerminalIds.length + " terminals");
		}
		if(editTerminalIds != null && editTerminalIds.length > 0) {
			mis.addMessage("success", "user.terminal.update.edit", editTerminalIds.length); //"Added edit privilege for " + editTerminalIds.length + " terminals");
		}
		if(mis.getMessageCount() == 0)
			mis.addMessage("failed", "user.terminal.update.none"); //"There were no updates to process");		
	}
}
