package com.usatech.usalive.servlet;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.usatech.layers.common.util.WebHelper;

import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;
import simple.servlet.RecordRequestFilter;
import simple.servlet.RequestUtils;
import simple.text.StringUtils;

public class USALiveRecordRequestFilter extends RecordRequestFilter {
	private static final Log log = Log.getLog();
		
    protected void recordRequest(HttpServletRequest request, long duration, String exceptionText) {
    	try {
    		String servletPath = request.getServletPath();
			CallInputs ci = new CallInputs();
			ci.setDuration(duration);
			ci.setExceptionText(exceptionText);
			updateWithRequest(ci, request);
			Map<String, String> parameters = ci.getParameters();
    		if ("POST".equalsIgnoreCase(request.getMethod())) {
	    		if (!StringUtils.isBlank(parameters.get("action")))
	    			ci.setActionName(parameters.get("action"));
	    		else if (!StringUtils.isBlank(parameters.get("myaction")))
	    			ci.setActionName(parameters.get("myaction"));
	    		else {
	    			ci.setActionName(servletPath.replaceFirst("^/", "").replaceFirst("\\.i$", "").replaceFirst("\\.html$", ""));
	    		}
    		}
    		if (servletPath.endsWith("/report_ajax_poll.i") || servletPath.endsWith("/postalLookup.i"))
    			return;
    		if (!StringUtils.isBlank(ci.getActionName()) && (
    				"report_ajax_poll_retrieve".equalsIgnoreCase(ci.getActionName()) 
    				|| "select_terminal_customers_getall".equalsIgnoreCase(ci.getActionName())))
    			return;
    		UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
    		ci.setAppCd(USALiveUtils.APP_CD);
    		Map<String, String> attributes = new HashMap<String, String>();
    		if (user != null) {
    			attributes.put("masterUserId", String.valueOf(user.getMasterUserId()));
    			attributes.put("userId", String.valueOf(user.getUserId()));
    		}
    		Map<String, Object> params = new HashMap<String, Object>();
    		if (!StringUtils.isBlank(parameters.get("terminalId"))) {
    			if (servletPath.endsWith("/device_configuration.html") && user != null) {
    				params.put("terminalId", parameters.get("terminalId"));	    			
	    			params.put("simple.servlet.ServletUser.userId", user.getUserId());
	    			Results results = DataLayerMgr.executeQuery("GET_TERMINAL_DETAILS", params);
	    			if (results.next()) {
	    				ci.setObjectTypeCd("device");
	    				ci.setObjectCd(results.getFormattedValue("deviceName"));
	    			} else {
	    				ci.setObjectTypeCd("terminal");
	        			ci.setObjectCd(parameters.get("terminalId"));
	    			}
    			} else {
    				ci.setObjectTypeCd("terminal");
        			ci.setObjectCd(parameters.get("terminalId"));
    			}
    		} else if (!StringUtils.isBlank(parameters.get("customerId"))) {
    			ci.setObjectTypeCd("reporting_customer");
    			ci.setObjectCd(parameters.get("customerId"));
    		} else if (!StringUtils.isBlank(parameters.get("editUserId"))) {
    			ci.setObjectTypeCd("user");
    			ci.setObjectCd(parameters.get("editUserId"));
    		}
    		WebHelper.publishAppRequestRecord(ci, attributes, log);
		} catch (Exception e) {
			log.error("Could not record request", e);
		}
    }	
}
