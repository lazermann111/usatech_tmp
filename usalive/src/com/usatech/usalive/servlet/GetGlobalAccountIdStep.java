package com.usatech.usalive.servlet;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.MaskedString;
import simple.bean.ReflectionUtils;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.StepConfigException;
import simple.servlet.steps.AbstractStep;
import simple.text.StringUtils;

import com.usatech.ec2.EC2ClientUtils;
import com.usatech.ec2.EC2ExecutionException;
import com.usatech.layers.common.MessageResponseUtils;

public class GetGlobalAccountIdStep extends AbstractStep {
	protected String bean;
	protected String result;

    public GetGlobalAccountIdStep() {
    }

    @Override
    public void configure(Map<String,?> props) throws StepConfigException {
    	super.configure(props);
    }
    public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
    	Object value;
        try {
			value = ReflectionUtils.getProperty(form, getBean());
		} catch (IntrospectionException e) {
			throw new ServletException(e);
		} catch (IllegalAccessException e) {
			throw new ServletException(e);
		} catch (InvocationTargetException e) {
			throw new ServletException(e);
		} catch(ParseException e) {
			throw new ServletException(e);
		}
		if(value != null)
			form.setAttribute(getResult(), getGlobalAccountId(value, form));
    }

	protected Long getGlobalAccountId(Object value, InputForm form) throws ServletException {
    	if(value == null)
    		return null;
		String cardNum;
		if(value instanceof MaskedString)
			cardNum = ((MaskedString) value).getValue();
		else try {
				cardNum = ConvertUtils.convert(String.class, value);
		} catch(ConvertException e) {
			throw new ServletException(e);
		}
		if(StringUtils.isBlank(cardNum))
    		return null;
		if(!MessageResponseUtils.isValidCardNumber(cardNum)) {
			form.setAttribute("displayErrorMessage", "The card number you entered is not valid");
			throw new ServletException("Invalid card number");
		}
		try {
			return EC2ClientUtils.getGlobalAccountId(cardNum);
		} catch(EC2ExecutionException e) {
			throw new ServletException(e);
		}
	}

	public String getBean() {
		return bean;
	}

	public void setBean(String bean) {
		this.bean = bean;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
}
