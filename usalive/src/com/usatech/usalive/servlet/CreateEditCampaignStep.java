package com.usatech.usalive.servlet;

import java.io.PrintStream;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.steps.AbstractStep;
import simple.text.StringUtils;

import com.usatech.layers.common.util.CampaignUtils;
/**
 * 
 * @author yhe
 *
 */
public class CreateEditCampaignStep extends AbstractStep {
	private static final Log log = Log.getLog();

	public void setDescription(InputForm form){
		String campaignDesc2=ConvertUtils.getStringSafely(form.get("campaignDesc2"));
		String campaignDesc;
		if(!StringUtils.isBlank(campaignDesc2)){
			campaignDesc=ConvertUtils.getStringSafely(form.get("campaignDesc"))+"\n"+campaignDesc2;
		}else{
			campaignDesc=ConvertUtils.getStringSafely(form.get("campaignDesc"));
		}
		if(campaignDesc.length()>43){
			campaignDesc=campaignDesc.substring(0,43);
		}
		form.set("campaignDesc", campaignDesc);
	}
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		PrintStream out = null;
		String actionType=ConvertUtils.getStringSafely(RequestUtils.getAttribute(request, "actionType", true));
		try {
			out=new PrintStream(response.getOutputStream());
			out.println("<script type=\"text/javascript\">");
			String campaignName;
			if(actionType.equalsIgnoreCase("add")){
				campaignName=form.getString("campaignName", true);
				if(campaignName.length()>60){
					campaignName=campaignName.substring(0,60);
				}
				String campaignRecurSchedule=getCampaignRecurSchedule(form);
				form.set("campaignRecurSchedule", campaignRecurSchedule);
				setDescription(form);
				String campaignDesc=form.getString("campaignDesc", true);
				String assignToAllDevices=form.getStringSafely("assignToAllDevices", "");
				String assignToAllCards=form.getStringSafely("assignToAllCards", "");
				String assignToAllPayrollCards=form.getStringSafely("assignToAllPayrollCards", "");
				String productCampaign;
				String passAllowed;
				
				assignToAllDevices=form.getStringSafely("assignToAllDevices", "N");
				form.set("assignToAllDevices", assignToAllDevices);
				assignToAllCards=form.getStringSafely("assignToAllCards", "N");
				form.set("assignToAllCards",assignToAllCards);
				assignToAllPayrollCards=form.getStringSafely("assignToAllPayrollCards", "N");
				form.set("assignToAllPayrollCards",assignToAllPayrollCards);
				productCampaign=form.getStringSafely("productCampaign", "N");
				form.set("productCampaign", productCampaign);
				passAllowed=form.getStringSafely("passAllowed", "N");
				form.set("passAllowed", passAllowed);
				
				String promoCodeSelect=form.getString("promoCodeSelect",false);
				if(!StringUtils.isBlank(promoCodeSelect)){
					form.set("promoCode", promoCodeSelect);
					form.set("reusePromoCode", "Y");
				}else{
					String promoCode=form.getString("promoCode", false);
					if(!StringUtils.isBlank(promoCode)){
						form.set("promoCode", promoCode.trim());
						form.set("reusePromoCode", "N");
					}
					
				}
				DataLayerMgr.executeCall("CREATE_CAMPAIGN", form, true);
				out.print("addCampaignResult(");
				out.print(ConvertUtils.getInt(form.get("campaignId")));
				out.print(",'");
				out.print(StringUtils.prepareScript(campaignName));
				out.print("','");
				out.print(StringUtils.prepareScript(campaignDesc));
				out.print("','");
				out.print(StringUtils.prepareScript(campaignRecurSchedule));
				out.print("','','");
				out.print(StringUtils.prepareScript(assignToAllDevices));
				out.print("','");
				out.print(StringUtils.prepareScript(assignToAllCards));
				out.print("','");
				out.print(StringUtils.prepareScript(assignToAllPayrollCards));
				out.print("','");
				out.print(StringUtils.prepareScript(ConvertUtils.getStringSafely(form.get("promoCode"),"")));
				out.print("');");
			}else if(actionType.equalsIgnoreCase("edit")){
				String editAction=form.getString("editAction", true);
				if(editAction.equals("save") || editAction.equals("finish") || editAction.equals("start")){
					if (editAction.equals("save") || editAction.equals("start")){
						campaignName=form.getString("campaignName", true);
						if(campaignName.length()>60){
							campaignName=campaignName.substring(0,60);
						}
						boolean currentCampaignIsToAll=form.getBoolean("currentCampaignIsToAll", true, false);
						String assignToAllDevices="";
						String assignToAllCards="";
						String assignToAllPayrollCards="";
						if(currentCampaignIsToAll){
							assignToAllDevices=form.getStringSafely("assignToAllDevices", "N");
							form.set("assignToAllDevices", assignToAllDevices);
						
							assignToAllCards=form.getStringSafely("assignToAllCards", "N");
							form.set("assignToAllCards",assignToAllCards);
						
							assignToAllPayrollCards=form.getStringSafely("assignToAllPayrollCards", "N");
							form.set("assignToAllPayrollCards",assignToAllPayrollCards);
						}
						String productCampaign="";
						String passAllowed;
						productCampaign=form.getStringSafely("productCampaign", "N");
						form.set("productCampaign", productCampaign);
						passAllowed=form.getStringSafely("passAllowed", "N");
						form.set("passAllowed", passAllowed);
						setDescription(form);
						String campaignDesc=form.getString("campaignDesc", true);
						String campaignRecurSchedule=getCampaignRecurSchedule(form);
						form.set("campaignRecurSchedule", campaignRecurSchedule);
						if (editAction.equals("start")){
							DataLayerMgr.executeCall("START_CAMPAIGN", form, true);
							out.print("editCampaignResult(3,'");
						}else{
							DataLayerMgr.executeCall("EDIT_CAMPAIGN", form, true);
							out.print("editCampaignResult(1,'");
						}				
						out.print(StringUtils.prepareScript(campaignName));
						out.print("','");
						out.print(StringUtils.prepareScript(campaignDesc));
						out.print("','");
						out.print(StringUtils.prepareScript(campaignRecurSchedule));
						out.print("','','");
						out.print(StringUtils.prepareScript(assignToAllDevices));
						out.print("','");
						out.print(StringUtils.prepareScript(assignToAllCards));
						out.print("','");
						out.print(StringUtils.prepareScript(assignToAllPayrollCards));
						out.print("','");
						out.print(StringUtils.prepareScript(productCampaign));
						out.print("','");
						if (editAction.equals("start")){
							out.print("STARTED");
						}else{
							out.print("DRAFT");
						}
						out.print("');");
					}else{
						if (editAction.equals("finish")){
							DataLayerMgr.executeCall("FINISH_CAMPAIGN", form, true);
							out.print("editCampaignResult(2,'','','','','','','','','FINISHED');");					
						}
					}
				}else if(editAction.equals("delete")){
					//delete
					DataLayerMgr.executeCall("DELETE_CAMPAIGN", form, true);
					out.print("editCampaignResult(0);");
				}
			}else if (actionType.equalsIgnoreCase("validate")){
				Results results;
				results = DataLayerMgr.executeQuery("VALIDATE_CAMPAIGN", form, false);
				if (results.next()){
					out.print("campaignValidation('"+ results.getFormattedValue("startDateVal") + "','" + 
													results.getFormattedValue("endDateVal") + "','" + 
													results.getFormattedValue("devicesVal") + "','" +
													results.getFormattedValue("productsVal") + "','" +
													results.getFormattedValue("cardBrandVal") + "','" + 
													results.getFormattedValue("promoPassVal") + "','" + 
													results.getFormattedValue("payTypeVal") + "');");
				}
			}
    		
		}catch(SQLIntegrityConstraintViolationException e) {
			if(actionType.equalsIgnoreCase("add")){
				out.print("addCampaignResult(-1,'','', '','Campaign name already exists.');");
			}else if(actionType.equalsIgnoreCase("edit")){
				out.print("editCampaignResult(-1,'','', '','Campaign name already exists.');");
			}
			log.error("Failed to "+actionType+" the campaign. Please try again.", e);
		}catch(IllegalArgumentException e){
			log.error("Failed to get campaign recur schedule.", e);
			if(actionType.equalsIgnoreCase("add")){
				out.print("addCampaignResult(-1,'','','', '");
				out.print(StringUtils.prepareScript(e.getMessage()));
				out.print("');");
			}else if(actionType.equalsIgnoreCase("edit")){
				out.print("editCampaignResult(-1,'','','', '");
				out.print(StringUtils.prepareScript(e.getMessage()));
				out.print("');");
			}
		}catch(SQLException e) {
			String errorMsg=null;
			if(e.getErrorCode()==20230){
				errorMsg="The promo code already exists in the system.";
			}else if (e.getErrorCode()==20301){
				errorMsg="Campaign status was changed by another user. Please refresh campaign page to get updates.";
			}else{
				errorMsg="An error occur on the server.";
			}
			if(actionType.equalsIgnoreCase("add")){
				out.print("addCampaignResult(-1,'','','','"+errorMsg+"');");
			}else if(actionType.equalsIgnoreCase("edit")){
				out.print("editCampaignResult(-1,'','','','"+errorMsg+"');");
			}
			log.error("Failed to "+actionType+" the campaign. Please try again.", e);
		}catch(Exception e) {
			if(actionType.equalsIgnoreCase("add")){
				out.print("addCampaignResult(-1,'','','','An error occur on the server.');");
			}else if(actionType.equalsIgnoreCase("edit")){
				out.print("editCampaignResult(-1,'','','','An error occur on the server.');");
			}
			log.error("Failed to "+actionType+" the campaign. Please try again.", e);
		} 
		out.print("</script>");
		out.println();
		out.flush();
		
	}
	
	public String getCampaignRecurSchedule(InputForm form) throws ServletException{
		String campaignRecurSchedule=form.getString("campaignRecurSchedule", false);
		String returnResultStr=null;
		if(campaignRecurSchedule==null){
			return "";
		}else{
			String campaignRecurHour=form.getString("campaignRecurHour", false).replaceAll("\\s","");
			if(CampaignUtils.validateCampaignRecurScheduleHour(campaignRecurHour)){
				if(campaignRecurSchedule.equals("M")){
					String campaignRecurMonth=form.getString("campaignRecurMonth", false).replaceAll("\\s","");
					if(CampaignUtils.validateCampaignRecurScheduleMonth(campaignRecurMonth)){
						returnResultStr="M:"+campaignRecurMonth+"|"+campaignRecurHour;
					}
				}else if(campaignRecurSchedule.equals("W")){
					String campaignRecurWeek=form.getString("campaignRecurWeek", false);
					if(campaignRecurWeek==null){
						throw new IllegalArgumentException("Day of Week is empty.");
					}else{
						campaignRecurWeek=campaignRecurWeek.replaceAll("\\s","");
						if(CampaignUtils.validateCampaignRecurScheduleWeek(campaignRecurWeek)){
							returnResultStr="W:"+campaignRecurWeek+"|"+campaignRecurHour;
						}
					}
				}else if(campaignRecurSchedule.equals("S")){
					String campaignRecurDay=form.getString("campaignRecurDay", false);
					if(campaignRecurDay==null){
						throw new IllegalArgumentException("Specific Days is empty.");
					}else{
						campaignRecurDay=campaignRecurDay.replaceAll("\\s","");
						if(CampaignUtils.validateCampaignRecurScheduleDay(campaignRecurDay)){
							returnResultStr="S:"+campaignRecurDay+"|"+campaignRecurHour;
						}
					}
				}else{
					returnResultStr="D|"+campaignRecurHour;
				}
			}
			if(returnResultStr.length()>4000){
				throw new IllegalArgumentException("Recurring schedule expression is too long.");
			}
			return returnResultStr;
		}
	}

}