package com.usatech.usalive.servlet;

import static simple.text.MessageFormat.format;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;

import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.db.NotEnoughRowsException;
import simple.io.Log;
import simple.lang.Holder;
import simple.results.Results;
import simple.servlet.*;
import simple.text.StringUtils;

import com.usatech.layers.common.AppLayerUtils;
import com.usatech.layers.common.AppLayerUtils.InitInfo;
import com.usatech.layers.common.constants.DeviceType;
import com.usatech.layers.common.messagedata.MessageData_AD;

public class ActivateDeviceUtil 
{
	private static final String DEFAULT_POS_PTA_TMPL_SETTING_CODE = "USALIVE_WEBSERVICE_K3_DEFAULT_POS_PTA_TMPL_ID";
	
	public static void validateCustomerBank(Connection connection, UsaliveUser user, long customerBankId) throws UserReportableException, ServletException
	{
		try {
			Map<String,Object> params = new HashMap<String,Object>();
			params.put(SimpleServlet.ATTRIBUTE_USER, user);
			params.put("custBankId", customerBankId);
			
			try {
				DataLayerMgr.selectInto("CHECK_BANK_ACCT", params);
			} catch(NotEnoughRowsException e) {
				throw new UserReportableException("The bank account is not valid.", e);
			} 
			
			if('D' == ConvertUtils.getChar(params.get("bankAcctStatus"))) {
				throw new UserReportableException("The selected bank account is not available for activations.");
			}
			
			if(ConvertUtils.getChar(params.get("bankAcctAllowEdit"), '-') == '-') {
				throw new UserReportableException("The selected bank account is not available for this user.");
			}
		} catch (Exception e) {
			throw new ServletException(format("CHECK_BANK_ACCT for user {0}, customerBankId {1} failed", user.getUserName(), customerBankId), e);
		}
	}
	
	public static boolean checkIfDeviceExists(Connection connection, String deviceSerialCd) throws ServletException
	{
		try {
			Results deviceExistsResults = DataLayerMgr.executeQuery("GET_DEVICE_COUNT_BY_SERIAL_NUMBER", new Object[] {deviceSerialCd});
			deviceExistsResults.next();
			return (deviceExistsResults.getValue("device_count", Integer.class) > 0);
		} catch (Exception e) {
			throw new ServletException(format("GET_DEVICE_COUNT_BY_SERIAL_NUMBER for deviceSerialCd {0} failed", deviceSerialCd), e);
		}
	}

	public static long registerEport(Connection connection, String deviceSerialCd, long dealerId) throws ServletException
	{
		// TODO: add serial number validation such that a customer can only
		// activate serial numbers in ranges assigned to them
			
		// register this eport serial number first
		long eportId = -1;

		Map<String, Object> registerEportParams = new HashMap<String, Object>();
		registerEportParams.put("serialNumber", deviceSerialCd);
		registerEportParams.put("deviceType", DeviceType.KIOSK.getValue());
		registerEportParams.put("dealerId", dealerId);

		try {
			DataLayerMgr.executeUpdate(connection, "EPORT_INS", registerEportParams);
			eportId = ConvertUtils.getLong(registerEportParams.get("eportId"));
		} catch(Exception e) {
			throw new ServletException(format("EPORT_INS for deviceSerialCd {0} failed", deviceSerialCd), e);
		}
		
		try {
			DataLayerMgr.executeUpdate(connection, "DEALER_EPORT_INS", registerEportParams);
		} catch(Exception e) {
			throw new ServletException(format("DEALER_EPORT_INS for deviceSerialCd {0} failed", deviceSerialCd), e);
		}
		
		return eportId;
	}
	
	public static void validateCommissionBank(Connection connection, ServletUser user, long commissionBankId) throws UserReportableException, ServletException
	{
		if (commissionBankId <= 0) {
			throw new UserReportableException("Commission bank account is required.");
		}
		
		Map<String, Object> params = new HashMap<>();
		params.put(SimpleServlet.ATTRIBUTE_USER, user);
	
		try {
			Results commissionBankResults = DataLayerMgr.executeQuery(connection, "GET_RESELLER_BANK_ACCOUNTS", params, false);
			while(commissionBankResults.next()) {
				long id = commissionBankResults.getValue("custBankId", Long.class);
				if(id == commissionBankId) {
					return;
				}
			}
		} catch (Exception e) {
			throw new ServletException(format("GET_RESELLER_BANK_ACCOUNTS for commissionBankId {0} failed", commissionBankId), e);
		}
		
		throw new UserReportableException("Commission bank account was invalid.");
	}
	
	public static long lookupDealerByCustomerBankId(Connection connection, long customerBankId) throws ServletException
	{
		InputForm lookupDealerForm = new MapInputForm(new HashMap<String, Object>());
		lookupDealerForm.setAttribute("customerBankId", customerBankId);
			
		try {
			DataLayerMgr.selectInto(connection, "GET_CUSTOMER_DEALER_LICENSE_BY_BANK_ACCOUNT", lookupDealerForm);
			return lookupDealerForm.getLong("dealerId", true, -1);
		} catch (Exception e) {
			throw new ServletException(format("GET_CUSTOMER_DEALER_LICENSE_BY_BANK_ACCOUNT for customerBankId {0} failed", customerBankId), e);
		}
	}
	
	public static long lookupQuickConnectIdByUsername(Connection connection, String quickConnectUserName) throws UserReportableException, ServletException
	{
		if (StringUtils.isBlank(quickConnectUserName)) {
			throw new UserReportableException("Quick connect username is required");
		}
		
		Map<String, Object> params = new HashMap<>();
		params.put("username", quickConnectUserName);

		try {
			DataLayerMgr.selectInto(connection, "GET_CREDENTIAL_INFO_BY_USERNAME", params);
			return ConvertUtils.getLong(params.get("credentialId"));
		} catch(Exception e) {
			throw new ServletException(format("GET_CREDENTIAL_INFO_BY_USERNAME for username {0} failed", quickConnectUserName), e);
		}
	}
	
	public static void acceptTerminal(Connection connection, long terminalId) throws ServletException
	{
		try {
			DataLayerMgr.executeCall(connection, "ACCEPT_NEW_TERMINAL", new Long[] {terminalId});
		} catch(Exception e) {
			throw new ServletException(format("ACCEPT_NEW_TERMINAL for terminalId {0} failed", terminalId), e);
		}
	}

	public static long initializeDevice(Connection connection, Log log, String deviceSerialCd) throws ServletException
	{
		Holder<InitInfo> initInfoHolder = new Holder<>();

		MessageData_AD messageData = new MessageData_AD();
		messageData.setDeviceType(DeviceType.KIOSK);
		messageData.setDeviceSerialNum(deviceSerialCd);

		try {
			AppLayerUtils.processOfflineInit(messageData, log, 1440, true, initInfoHolder);
			//AppLayerUtils.processInit(message, data, myLog, credentialId, defaultFilePacketSize)
		} catch(Exception e) {
			throw new ServletException(format("AppLayerUtils.processOfflineInit for deviceSerialCd {0} failed", deviceSerialCd), e);
		}

		if(initInfoHolder.getValue() == null || initInfoHolder.getValue() == null || initInfoHolder.getValue().deviceId <= 0) {
			throw new ServletException(format("AppLayerUtils.processOfflineInit for deviceSerialCd {0} returned invalid InitInfo", deviceSerialCd));
		}
		
		return initInfoHolder.getValue().deviceId;
	}
	
	public static void setQuickConnectCredentials(Connection connection, long quickConnectId, String deviceSerialCd) throws ServletException
	{
		try {
			DataLayerMgr.executeUpdate(connection, "UPDATE_DEVICE_CREDENTIAL_BY_SERIAL_NUMBER", new Object[] {quickConnectId, deviceSerialCd});
		} catch(Exception e) {
			throw new ServletException(format("UPDATE_DEVICE_CREDENTIAL_BY_SERIAL_NUMBER for deviceSerialCd {0} failed", deviceSerialCd), e);
		}
	}
	
	public static void importDefaultPaymentTemplate(Connection connection, long deviceId) throws ServletException
	{
		// lookup the default payment template from app_settings
		long posPtaTmplId = -1;

		Map<String, Object> defaultTemplateParams = new HashMap<String, Object>();
		defaultTemplateParams.put("code", DEFAULT_POS_PTA_TMPL_SETTING_CODE);

		try {
			DataLayerMgr.selectInto(connection, "GET_APP_SETTING", defaultTemplateParams);
			posPtaTmplId = ConvertUtils.getLong(defaultTemplateParams.get("value"));
		} catch(Exception e) {
			throw new ServletException(format("GET_APP_SETTING for code {0} failed", DEFAULT_POS_PTA_TMPL_SETTING_CODE), e);
		}

		Map<String, Object> importTemplateParams = new HashMap<String, Object>();
		importTemplateParams.put("device_id", deviceId);
		importTemplateParams.put("pos_pta_template_id", posPtaTmplId);
		importTemplateParams.put("mode_cd", "CO");
		importTemplateParams.put("order_cd", "AE");
		importTemplateParams.put("set_terminal_cd_to_serial", "N");
		importTemplateParams.put("only_no_two_tier_pricing", "N");

		Object[] importTemplateResults = null;

		try {
			importTemplateResults = DataLayerMgr.executeCall(connection, "SP_IMPORT_POS_PTA_TEMPLATE", importTemplateParams);
		} catch(Exception e) {
			throw new ServletException(format("SP_IMPORT_POS_PTA_TEMPLATE for deviceId {0} failed", deviceId), e);
		}

		if(importTemplateResults == null || importTemplateResults.length != 3) {
			throw new ServletException(format("SP_IMPORT_POS_PTA_TEMPLATE for deviceId {0} returned unexpected results", deviceId));
		}

		if(ConvertUtils.getIntSafely(importTemplateResults[1], -1) != 1) {
			throw new ServletException(format("SP_IMPORT_POS_PTA_TEMPLATE returned error result code: code={0}, message={1}, deviceId={2}, posPtaTmplId={3}", importTemplateResults[1], importTemplateResults[2], deviceId, posPtaTmplId));
		}
	}
	
	public static class UserReportableException extends Exception
	{
		private static final long serialVersionUID = 1L;

		public UserReportableException(String s) {
			super(s);
		}
		
		public UserReportableException(String s, Exception e) {
			super(s, e);
		}
	}
}
