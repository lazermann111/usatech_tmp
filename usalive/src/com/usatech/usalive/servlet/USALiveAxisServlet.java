package com.usatech.usalive.servlet;

import java.io.IOException;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.axis2.transport.http.AxisServlet;

@SuppressWarnings("serial")
public class USALiveAxisServlet extends AxisServlet 
{
	public static final String SOAP_URI = "/soap";
	public static final Pattern SOAP_PATTERN = Pattern.compile(SOAP_URI);
	private static final Pattern SERVICE_URI_PATTERN = Pattern.compile("(/soap|/soap/(pepsi|pepsi\\..+|vdi|vdi\\..+|usalive|usalive\\..+)).*");
	private static final Pattern QUERY_STRING_PATTERN = Pattern.compile("(wsdl|wsdl2|xsd)");

	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if ("POST".equalsIgnoreCase(req.getMethod()) &&
					req.getQueryString() == null &&
					req.getRequestURI() != null &&
					SERVICE_URI_PATTERN.matcher(req.getRequestURI()).matches()) {
			super.doPost(req, resp);
		} else if ("GET".equalsIgnoreCase(req.getMethod()) && 
								req.getRequestURI() != null &&
								SERVICE_URI_PATTERN.matcher(req.getRequestURI()).matches() && 
								req.getQueryString() != null &&
								QUERY_STRING_PATTERN.matcher(req.getQueryString()).matches()) {
			super.doGet(req, resp);
		} else {
			USALiveUtils.writeUsageTerms(resp, resp.getWriter());
		}
	}
}
