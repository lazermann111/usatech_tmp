/*
 * LoginActions.java
 *
 * Created on October 11, 2001, 3:08 PM
 */

package com.usatech.usalive.servlet;

/**
 *
 * @author  bkrug
 * @version
 */
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.bean.MaskedString;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;
import simple.security.PasswordExpirationException;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.LdapUtils;
import simple.servlet.LdapUtils.LdapAcctInfo;
import simple.servlet.NotAuthorizedException;
import simple.servlet.RequestUtils;
import simple.servlet.ServletUser;
import simple.servlet.SimpleAction;
import simple.servlet.steps.AbstractLogonStep;
import simple.servlet.steps.LoginFailureException;
import simple.text.Hasher;
import simple.text.ThreadSafeDateFormat;

public class LoginStep extends AbstractLogonStep {
	private final static Log log = Log.getLog();
	public static final Pattern DEFAULT_INTERNAL_USERNAME_PATTERN = Pattern.compile("([^@]+)@usatech\\.com");
	public static final Pattern DEFAULT_INTERNAL_HOST_PATTERN = Pattern.compile("(192\\.168\\.\\d{1,3}|10\\.0\\.[0-1])\\.\\d{1,3}|127\\.0\\.0\\.1|0\\:0\\:0\\:0\\:0\\:0\\:0\\:1");
	protected static final ThreadSafeDateFormat dateFormat = new ThreadSafeDateFormat(new SimpleDateFormat("MM/dd/yyyy hh:mm a Z"));
	protected static final String PASSWORD_EXPIRATION_AND_HISTORY_ENABLED_CD = "PASSWORD_EXPIRATION_AND_HISTORY_ENABLED";
	protected Pattern internalUsernamePattern = DEFAULT_INTERNAL_USERNAME_PATTERN;
	protected Pattern internalHostPattern = DEFAULT_INTERNAL_HOST_PATTERN;
	protected final Hasher hasher;
	protected String[] requiredGroups = null; //{"USALive User"};
	protected int maxFailures = 10;
	public LoginStep() throws NoSuchAlgorithmException {
		hasher = new Hasher("SHA1", true);
	}

	@Override
	protected boolean rememberUserName(InputForm form) {
		return true;
	}

	@Override
	public ServletUser authenticateUser(InputForm form) throws ServletException {
		String username = form.getString("username",false);
		if(username == null || username.trim().equals("")) {
			throw new LoginFailureException("Please enter your username and password and try again.", LoginFailureException.INVALID_USERNAME);
		}
		String password;
		Object tmp = form.getAttribute("password");
		if(tmp instanceof MaskedString)
			password = ((MaskedString)tmp).getValue();
		else if(tmp == null || (password = tmp.toString().trim()).isEmpty()) {
			throw new LoginFailureException("Please enter your password and try again.", LoginFailureException.INVALID_PASSWORD);
		}
		
		//profileId is the userId of the user this request wants to assume
		Integer profileId = ConvertUtils.convertSafely(Integer.class, form.getAttribute("profileId"), null);
		UsaliveUser user = new UsaliveUser();
		
		if (!isUserPermitted(username)) {
			log.warn(String.format("User %s tried to login but was soft-locked or did not exist.", username));
			if (!authenticateInternalUserWithAnyName(user, username, profileId, password, form.getRequest() == null ? null : form.getRequest().getRemoteAddr())) {
				throw new LoginFailureException("Please enter your username and password and try again.", LoginFailureException.INVALID_USERNAME);
			}
		}
		
		//determine if it is a usatech account
		Matcher matcher = getInternalUsernamePattern().matcher(username);
		try {
			TimeZone timeZone = RequestUtils.getTimeZone(form);
			user.setTimeZone(timeZone);
		} catch(ServletException e) {
			log.warn("Could not obtain timezone from request", e);
		}
		boolean originalUserInternal;
		if(matcher.matches()) {
			authenticateInternalUser(user, username, matcher.group(1), profileId, password);
			originalUserInternal = true;
		} else {
			authenticateExternalUser(user, username, profileId, password, form.getRequest() == null ? null : form.getRequest().getRemoteAddr());
			originalUserInternal = user.isInternal();
		}
		if(form.getRequest() instanceof HttpServletRequest) {
			HttpSession session = ((HttpServletRequest) form.getRequest()).getSession(false);
			if(session != null)
				session.invalidate(); // Per Veracode recommendations for "Session Fixation" vunerability
		}
		form.setAttribute("isOriginalUserInternal", originalUserInternal, RequestUtils.SESSION_SCOPE);
		if(user.getPasswordExpiration() != null) {
			long untilExpires = user.getPasswordExpiration().getTime() - System.currentTimeMillis();
			if(untilExpires <= 0)
				throw new LoginFailureException("Your password expired at " + dateFormat.format(user.getPasswordExpiration()), LoginFailureException.PASSWORD_EXPIRED);
			else if(untilExpires <= 7 * 24 * 60 * 60 * 1000L)
				try {
					RequestUtils.getOrCreateMessagesSource(form).addHtmlMessage("warning", "user-password-expires-soon-html", "<span>Your password expires in {0} day(s). Please&#160;<a href=\"edit_password.html\">click here</a> to change your password now</span>", untilExpires / 24 / 60 / 60 / 1000L, (untilExpires / 60 / 60 / 1000L) % 24);
				} catch(IOException e) {
					log.error("Could not format password expiration message", e);
				} catch(SAXException e) {
					log.error("Could not format password expiration message", e);
				} catch(ParserConfigurationException e) {
					log.error("Could not format password expiration message", e);
				}
			else
				try {
					RequestUtils.getOrCreateMessagesSource(form).addHtmlMessage("info", "user-password-expiration-html", "<span>Your password expires in {0} day(s). You may change it by&#160;<a href=\"edit_password.html\">clicking here</a></span>", untilExpires / 24 / 60 / 60 / 1000L, (untilExpires / 60 / 60 / 1000L) % 24);
				} catch(IOException e) {
					log.error("Could not format password expiration message", e);
				} catch(SAXException e) {
					log.error("Could not format password expiration message", e);
				} catch(ParserConfigurationException e) {
					log.error("Could not format password expiration message", e);
				}
		}
		user.setUserId(profileId == null ? user.getMasterUserId() : profileId);			
		form.setAttribute("masterUserId", user.getMasterUserId(), RequestUtils.SESSION_SCOPE);
		userLoggedIn(username, user, form);
		if (user.getCustomerId() > 0) {
			Map<String, Object> offerParams = new HashMap<String, Object>();
			offerParams.put("userId", user.getUserId());
			try {
				Results offerResults = DataLayerMgr.executeQuery("GET_USER_OFFER", offerParams);
				if (offerResults.next() && !"-".equalsIgnoreCase(offerResults.getFormattedValue("offerPage")))
					form.setAttribute(ATTRIBUTE_FORWARD, offerResults.getFormattedValue("offerPage"));
			} catch(SQLException | DataLayerException e) {
				throw new ServletException(e);
			}
		}
		return user;
	}

	protected void authenticateInternalUser(UsaliveUser user, String username, String loginName, Integer profileId, String password) throws ServletException {
		LdapAcctInfo ldapAcctInfo = null;
		try {
			ldapAcctInfo = LdapUtils.authenticate(loginName, password);
		} catch (LoginFailureException e) {
			updateLoginFail(username);
			throw e;
		}
		// Ensure user is allowed to use USALive
		String[] requiredGroups = getRequiredGroups();
		if(requiredGroups != null) {
			for(String group : requiredGroups) {
				if(!ldapAcctInfo.getGroups().contains(group))
					throw new NotAuthorizedException("Your user account is not allowed to access USALive. Please contact the Network Administrator.");
			}
		}
		user.setPasswordExpiration(ldapAcctInfo.getPasswordExpiration());
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("username", username);
		params.put("profileId", profileId);
		params.put("user", user);
		params.put("email", username);
		String fullName = ldapAcctInfo.getFullName();
		if(fullName != null && (fullName = fullName.trim()).length() > 0) {
			int p = fullName.lastIndexOf(' ');
			if(p < 0) {
				params.put("lastName", fullName);
			} else {
				params.put("lastName", fullName.substring(p + 1));
				params.put("firstName", fullName.substring(0, p).trim());
			}
		}
		try {
			DataLayerMgr.executeCall("LOGIN_INTERNAL_USER", params, true);
		} catch(SQLException e) {
			if(e.getErrorCode() == 20100) {
				throw new LoginFailureException("User '" + username + "' may not log on requested user (" + profileId + ")", LoginFailureException.INVALID_CREDENTIALS);
			}
			throw new ServletException(e);
		} catch(DataLayerException e) {
			throw new ServletException(e);
		}
		if(profileId == null || internalUsernamePattern.matcher(user.getUserName()).matches())
			user.setInternal(true);
	}

	protected void authenticateExternalUser(UsaliveUser user, String username, Integer profileId, String password, String remoteIp) throws ServletException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("username", username);
		params.put("profileId", profileId);
		params.put("user", user);
		try {
			DataLayerMgr.executeCall("CHECK_EXTERNAL_USER", params, true);
		} catch(SQLException e) {
			if(e.getErrorCode() == 20100) {
				throw new LoginFailureException("User '" + username + "' may not log on requested user (" + profileId + ")", LoginFailureException.INVALID_CREDENTIALS);
			} else if(e.getErrorCode() == 1403) {
				if (!authenticateInternalUserWithAnyName(user, username, profileId, password, remoteIp)) {
					throw new LoginFailureException("Invalid username or password for User '" + username + "'", LoginFailureException.INVALID_CREDENTIALS);
				}
				return;
			}
			throw new ServletException(e);
		} catch(DataLayerException e) {
			throw new ServletException(e);
		}
		int failureCount = ConvertUtils.getIntSafely(params.get("failureCount"), 0);
		if(failureCount >= getMaxFailures())
			throw new LoginFailureException("Incorrect username or password.", LoginFailureException.ACCOUNT_LOCKED);
		byte[] salt;
		byte[] hash;
		try {
			salt = ConvertUtils.convertRequired(byte[].class, params.get("passwordSalt"));
			hash = ConvertUtils.convertRequired(byte[].class, params.get("passwordHash"));
		} catch(ConvertException e) {
			throw new ServletException(e);
		}
		byte[] testHash = hasher.hashRaw(password.getBytes(), salt);
		if(!Arrays.equals(hash, testHash)) {
			updateLoginFail(username);
			throw new LoginFailureException("Invalid username or password for User '" + username + "'", LoginFailureException.INVALID_CREDENTIALS);
		}
		try {
			DataLayerMgr.executeCall("RECORD_EXTERNAL_LOGIN", params, true);
		} catch(SQLException e) {
			log.warn("Could not record login failure for user '" + username + "'", e);
		} catch(DataLayerException e) {
			log.warn("Could not record login failure for user '" + username + "'", e);
		}
		if (isPasswordExpirationEnabled()) {
			Date now = new Date();
			// check password expiration
			Date passwordExpirationTs = user.getPasswordExpiration();
			if (passwordExpirationTs != null && now.after(passwordExpirationTs)) {
				try {
					throw new PasswordExpirationException("Password is expired. Please change the password.", null);
				} catch (PasswordExpirationException e) {
					throw new ServletException(e);
				}
			}
		}
	}

	private boolean isPasswordExpirationEnabled() {
		Object [] param = {PASSWORD_EXPIRATION_AND_HISTORY_ENABLED_CD};
		try {
			Results res = DataLayerMgr.executeQuery("GET_APP_SETTING", param);
			String passwordExpirationEnabled = (res.next() ? res.getFormattedValue("value") : "N");
			return "Y".equals(passwordExpirationEnabled);
		} catch (SQLException e1) {
			log.debug("SQL exception: ", e1);
		} catch (DataLayerException e1) {
			log.error("Mgr exception: ", e1);
		}
		return false;
	}

	private boolean authenticateInternalUserWithAnyName(UsaliveUser user, String username, Integer profileId,
			String password, String remoteIp) throws ServletException, LoginFailureException {
		if (remoteIp != null && internalHostPattern.matcher(remoteIp).matches()) {
			if (!username.contains("@")) {
				authenticateInternalUser(user, username + "@usatech.com", username, profileId, password);
				return true;
			} else {
				Matcher matcher = getInternalUsernamePattern().matcher(username);
				if (matcher.matches()) {
					authenticateInternalUser(user, username, matcher.group(1), profileId, password);
					return true;
				}
			}
		}
		return false;
	}

	public static void userLoggedIn(String username, UsaliveUser user, InputForm form) {
		user.getUserProperties().put("fullName", (user.getFirstName() + ' ' + user.getLastName()).trim());
	    setUserGroups(user);    
		String remoteUser = username.equals(user.getUserName()) ? username : username + " as " + user.getUserName();
		form.setAttribute("remoteUser", remoteUser, RequestUtils.SESSION_SCOPE);
		form.setAttribute("refreshMenu", true);
		log.info("User '" + username + "' logged in as '" + user.getUserName() + "'");
	}
	
	protected static void setUserGroups(UsaliveUser user) {
		/*
        USER_TYPE_ID  USER_TYPE                      DESCRIPTION
        ------------- ------------------------------ --------------------------------------------------
        5             Application Process            Application process that can update the database
        3             System Admin                   System Administrator
        2             Accounting                     Accounting User
        1             Customer Service               Customer Service User
        4             Management                     Management User
        7             Development                    Systems Analyst/Programmer
        6             System Process                 System process that can update the database
        8             Customer                       A USA Tech Customer (for web reports)
        9             Sales                          Sales Force
        */
    	Set<Long> userGroups = new HashSet<Long>(10);
        switch(user.getUserType()) {
            case 3: userGroups.add(1L); break;
            case 2: userGroups.add(2L); break;
            case 1: userGroups.add(3L); break;
            case 4: userGroups.add(4L); break;
            case 9: userGroups.add(5L); break;
            case 8: userGroups.add(8L); break;
            case 7: userGroups.add(7L); break;
        }

        userGroups.add(100L); // via report.user_terminal
        userGroups.add(120L); // via report.user_customer_bank
        if(user.hasPrivilege("7") || user.hasPrivilege("6")) {// PPRIV_VIEW_ALL_TERMS(6), PRIV_EDIT_ALL_TERMS(7)
        	switch(user.getUserType()) {
        		case 8:
        			userGroups.add(101L); break; // all terminals where customer_id is equal
        		case 3:
                case 2:
                case 1:
                case 4:
                case 9:
                	userGroups.add(102L); break; // all terminals
            }
        }
        if(user.hasPrivilege("11")) {//PRIV_VIEW_ALL_BANK_ACCTS(11)
        	switch(user.getUserType()) {
        		case 8:
        			userGroups.add(121L); break; // all bank accts where customer_id is equal
        		case 3:
                case 2:
                case 1:
                case 4:
                case 9:
                	userGroups.add(122L); break; // all bank accts
            }
        }
        if (user.hasPrivilege(Long.toString(29L))) {
        	userGroups.add(29L);
        }
        long[] tmp = new long[userGroups.size()];
        Iterator<Long> iter = userGroups.iterator();
        for(int i = 0; i < tmp.length; i++)
        	tmp[i] = iter.next();
        user.setUserGroupIds(tmp);
	}
	@Override
	protected void handleAuthenticationFailure(Dispatcher dispatcher, InputForm form, ServletException exception) throws ServletException {
		if(exception instanceof LoginFailureException) {
			String loginPromptPage = form.getString("loginPromptPage", false);
			if(loginPromptPage != null && !(loginPromptPage=loginPromptPage.trim()).isEmpty()) {
				String username = form.getString("username",false);
				log.warn("Login Failure for user '" + username + "'");
				String key;
				switch(((LoginFailureException)exception).getType()) {
					case LoginFailureException.INVALID_USERNAME: key = "login-invalid-username"; break;
					case LoginFailureException.INVALID_PASSWORD: key = "login-invalid-password"; break;
					case LoginFailureException.INVALID_CREDENTIALS: key = "login-invalid-credentials"; break;
					case LoginFailureException.ACCOUNT_LOCKED: key = "login-account-locked"; break;
					case LoginFailureException.PASSWORD_EXPIRED:
						form.setAttribute(SimpleAction.ATTRIBUTE_EXCEPTION, exception);
						dispatcher.dispatch("edit_password.html.jsp", true);
						return;
					default: key = "login-invalid-credentials";
				}
				form.setAttribute("errorMessage", form.getTranslator().translate(key, "Please enter your username and password and try again", username));
				dispatcher.dispatch(loginPromptPage, true);
				return;
			}
		}
		super.handleAuthenticationFailure(dispatcher, form, exception);
	}
	
	protected String hash(String password) {
		return hasher.hash(password);
	}

	public Pattern getInternalUsernamePattern() {
		return internalUsernamePattern;
	}

	public void setInternalUsernamePattern(Pattern internalUsernamePattern) {
		this.internalUsernamePattern = internalUsernamePattern;
	}

	public String[] getRequiredGroups() {
		return requiredGroups;
	}

	public void setRequiredGroups(String[] requiredGroups) {
		this.requiredGroups = requiredGroups;
	}

	public int getMaxFailures() {
		return maxFailures;
	}

	public void setMaxFailures(int maxFailures) {
		this.maxFailures = maxFailures;
	}
	
	private void updateLoginFail(String username) {
		try {
			Map<String, Object> params = new HashMap<>();
			params.put("username", username);
			DataLayerMgr.executeCall("RECORD_FAILED_LOGIN", params, true);
		} catch(SQLException e) {
			log.warn("Could not record login failure for user '" + username + "'", e);
		} catch(DataLayerException e) {
			log.warn("Could not record login failure for user '" + username + "'", e);
		}
	}
	
	private boolean isUserPermitted(String username) {
		Results results = null;
		try{
			results=DataLayerMgr.executeQuery("IS_USER_PERMITTED", new Object[]{username, username + "@usatech.com"});
			if(results.next()){
				return ConvertUtils.getInt(results.get("cnt"), 0) >= 1;
			}
		}catch(Exception e){
				log.error("Failed to retrieve consumer card count", e);
		}
		return false;
	}

}