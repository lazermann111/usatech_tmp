package com.usatech.usalive.servlet;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.bean.ConvertException;
import simple.bean.ConvertUtils;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.lang.InvalidStringValueException;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;
import simple.text.Hasher;
/**
 * Updates the transport properties
 * 
 * @author Brian S. Krug
 *  
 */
public class CheckPasswordStep extends AbstractCheckPasswordStep {
	static final Log log = Log.getLog();
	
	// for submit_password_reset to check last passwords
	protected String userNameFieldName = "username";
	
	public CheckPasswordStep() throws NoSuchAlgorithmException {
		super();
	}

	public void checkPassword(InputForm form) throws ServletException {
		String password = form.getString(passwordFieldName, true);
		String confirm = form.getString(confirmFieldName, true);
		try {
			if(!password.equals(confirm)) {
				throw new InvalidStringValueException("The password does not match the confirmation. Please re-type both.", null);
			} 
			if(password.length() < getMinLength()) {
				throw new InvalidStringValueException("The password must be at least " + getMinLength() + " digits. Please choose another.", null);
	        }
	        if(isUpperCaseRequired() && !hasUpperCheck.matcher(password).find()) {
	        	throw new InvalidStringValueException("The password must contain at least 1 uppercase letter.", null);
			}
	        if(isLowerCaseRequired() && !hasLowerCheck.matcher(password).find()) {
	        	throw new InvalidStringValueException("The password must contain at least 1 lowercase letter.", null);
			}
	        if(isNonAlphaRequired() && !hasNonAlphaCheck.matcher(password).find()) {
	        	throw new InvalidStringValueException("The password must contain at least 1 number or 1 punctuation symbol.", null);
			}
		} catch(InvalidStringValueException e) {
			throw new ServletException(e);
		}
		
		// check previous passwords
		long userId = form.getLong(userIdFieldName, false, -1);
		if (userId < 0) {
			String username  = form.getString(userNameFieldName, true);
			try {
				Results result = DataLayerMgr.executeQuery("GET_USER_ID", new Object[] { username }, true);
				if (!result.next())
					throw new DataLayerException("User with user name " + username + " is not found!");
				userId = result.getValue("userId", Long.class);
			} catch (SQLException | DataLayerException | ConvertException e) {
				throw new ServletException(e);
			}
			
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userId", userId);
		try {
			DataLayerMgr.executeCall("GET_LAST_PASSWORDS", params, true);
		} catch (SQLException | DataLayerException e) {
			throw new ServletException(e);
		}
		byte[][] salts;
		byte[][] hashes;
		try {
			salts = ConvertUtils.convertRequired(byte[][].class, params.get("passwordSalts"));
			hashes = ConvertUtils.convertRequired(byte[][].class, params.get("passwordHashes"));
		} catch(ConvertException e) {
			throw new ServletException(e);
		}
		int nPasswords = salts.length;
		try {
			for (int i = 0; i < nPasswords; i++) {
				byte[] prevSalt = salts[i];
				byte[] prevHash = hashes[i];
				byte[] testHash = hasher.hashRaw(password.getBytes(), prevSalt);
				log.debug("testHash: " + testHash +", prrevGash: " + prevHash + ", prevSalt: " + prevSalt);
				if (Arrays.equals(testHash, prevHash)) {
					throw new InvalidStringValueException("This password was recently used. Please choose another", null);
				}
			}
		} catch (InvalidStringValueException e) {
			throw new ServletException(e);
		}

		byte[] salt = new byte[getSaltSize()];
		random.nextBytes(salt); 
		byte[] hash = hasher.hashRaw(password.getBytes(), salt);
		form.setAttribute("passwordHash", hash);
		form.setAttribute("passwordSalt", salt);	
	}
	
}
