package com.usatech.usalive.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.results.Results;
import simple.results.Results.Aggregate;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;
import simple.text.StringUtils;
import simple.util.sheet.SheetUtils;

public class MassDeviceDownloadStep extends AbstractStep {
	public static enum FileType {EXCEL, FIXED_V1, CSV};

	protected FileType fileType = FileType.EXCEL;

	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		Connection conn = null;
		try {
			conn = DataLayerMgr.getConnection("report");

			Results results = DataLayerMgr.executeQuery(conn, "GET_TERMINALS", form);
			Integer customCols = results.getAggregate("CUSTOM_COLUMNS", null, Aggregate.MAX, Integer.class);
			int outputCols;
			if(customCols == null)
				outputCols = results.getColumnCount() - 9;
			else
				outputCols = results.getColumnCount() - 9 + customCols;
			int[] cols = new int[outputCols];
			for(int i = 0; i < cols.length; i++)
				cols[i] = i + 1;

			switch (fileType) {
				case CSV:
					outputDevicesAsCSV(response, results, cols);
					break;
				case EXCEL:
					outputDevicesAsExcel(response, results, cols);
					break;
				case FIXED_V1: // currently not supported, ergo, fall through
				default:
					throw new ServletException("File type " + fileType + " not supported");

			}

		} catch (SQLException sqle) {
			throw new ServletException(sqle);
		} catch (DataLayerException dle) {
			throw new ServletException(dle);
		} catch (Exception e) {
			throw new ServletException(e);
		} finally {
			try { if (conn != null) conn.close(); } catch (SQLException e) {}
		}
	}

	private void outputDevicesAsExcel(HttpServletResponse response, Results results, int[] cols) throws Exception, IOException {
		response.setContentType("application/xls");
		response.setHeader("Content-Disposition", "attachment;filename=Devices.xls");

		SheetUtils.writeExcel(results, response.getOutputStream(), true, cols);
	}

	private void outputDevicesAsCSV(HttpServletResponse response, Results results, int[] cols) throws Exception, IOException {
		response.setContentType("application/csv");
		response.setHeader("Content-Disposition", "attachment;filename=Devices.csv");

		StringUtils.writeCSV(results, new PrintWriter(response.getOutputStream()), true, cols);
	}

	public FileType getFileType() {
		return fileType;
	}

	public void setFileType(FileType fileType) {
		this.fileType = fileType;
	}
}
