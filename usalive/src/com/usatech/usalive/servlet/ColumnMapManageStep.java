package com.usatech.usalive.servlet;

import java.io.IOException;
import java.io.PrintStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.collect.Sets;

import simple.bean.ConvertUtils;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.results.Results;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.steps.AbstractStep;
import simple.text.StringUtils;
/**
 * 
 * @author yhe
 *
 */
public class ColumnMapManageStep extends AbstractStep{
	private static final Log log = simple.io.Log.getLog();
	
	public static int MIN_DIFF_ALLOWED=5;
	
	protected class ColumnMap {
		protected int columnMapId;
		protected String columnMapName;
		protected TreeSet<String> columnMapValue;
		int isSuperSet;
		int diffSize;
		private ColumnMap(int columnMapId, String columnMapName,
				TreeSet<String> columnMapValue) {
			super();
			this.columnMapId = columnMapId;
			this.columnMapName = columnMapName;
			this.columnMapValue = columnMapValue;
		}
		public int getColumnMapId() {
			return columnMapId;
		}
		public String getColumnMapName() {
			return columnMapName;
		}
		
		public TreeSet<String> getColumnMapValue() {
			return columnMapValue;
		}
		/**
		 * 
		 * @param anotherMap
		 */
		public int compareSuperSet(TreeSet<String> anotherMap){
			if(columnMapValue.size()>=anotherMap.size()){
				if(columnMapValue.containsAll(anotherMap)){
					if(columnMapValue.size()==anotherMap.size()){
						isSuperSet=0;
					}else{
						isSuperSet=1;
					}
				}else{
					isSuperSet=-1;
				}
			}else{
				isSuperSet=-1;
			}
			return isSuperSet;
		}
		
		public int isSuperSet() {
			return isSuperSet;
		}
		public int compareDiffSize(TreeSet<String> anotherMap){
			Sets.SetView<String> intersectionSet=Sets.intersection(columnMapValue, anotherMap);
			diffSize=anotherMap.size()-intersectionSet.size();
			return diffSize;
		}
		public int getDiffSize() {
			return diffSize;
		}
		
	}
	
	public ColumnMap findMatchedColumnMap(InputForm form, TreeSet<String> newMap) throws Exception{
		ArrayList<ColumnMap> allMaps=new ArrayList<ColumnMap> ();
		Results results = DataLayerMgr.executeQuery("GET_COLUMN_MAP_FOR_MATCH", form);
		while(results.next()){
			String columnMapValue=ConvertUtils.getString(results.get("columnMapValue"), true);
			TreeSet<String> columnMapSet=new TreeSet<String>();
			String[] columnMapValueArray=columnMapValue.split(",");
			for(String keyValue:columnMapValueArray){
				columnMapSet.add(keyValue);
			}
			allMaps.add(new ColumnMap(ConvertUtils.getInt(results.get("columnMapId")),ConvertUtils.getString(results.get("columnMapName"), true), columnMapSet));
		}
		int minDiffSize=Integer.MAX_VALUE;
		ColumnMap theMinDiffMap=null;
		ColumnMap superMap=null;
		for(ColumnMap aMap:allMaps){
			if(aMap.compareSuperSet(newMap)>=0){
				if(aMap.isSuperSet()==0){
					return aMap;
				}else{
					superMap=aMap;
				}
			}else{
				int diffSize=aMap.compareDiffSize(newMap);
				if(diffSize<minDiffSize){
					minDiffSize=diffSize;
					theMinDiffMap=aMap;
				}
			}
		}
		if(superMap!=null){
			return superMap; 
		}else if(minDiffSize<=MIN_DIFF_ALLOWED){
			return theMinDiffMap;
		}else{
			return null;
		}
	}
	
	public void perform(Dispatcher dispatcher, InputForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException {
		try {
			StringBuilder responseText=new StringBuilder();
			String actionType=form.getString("actionType", true);
			if(actionType.equalsIgnoreCase("apply")){
				DataLayerMgr.executeUpdate("APPLY_COLUMN_MAP_TEMPLATE", form, true);
				responseText.append("setMessage(1,'Successfully applied the template to the selected devices.');moveUnmappeddToMapped();searchMappedByTemplate();");
			}else if(actionType.equalsIgnoreCase("remove")){
				DataLayerMgr.executeUpdate("REMOVE_COLUMN_MAP_TEMPLATE", form, true);
				responseText.append("setMessage(1,'Successfully removed the template from the selected devices.');moveMappedToUnmapped();");
			}else if(actionType.equalsIgnoreCase("disableAutoUpdate")){
				DataLayerMgr.executeUpdate("DISABLE_COLUMN_MAP_UPDATE", form, true);
				responseText.append("setMessage(1,'Successfully disabled auto update for the selected devices.');moveEnableToDisable();");
			}else if(actionType.equalsIgnoreCase("enableAutoUpdate")){
				DataLayerMgr.executeUpdate("ENABLE_COLUMN_MAP_UPDATE", form, true);
				responseText.append("setMessage(1,'Successfully enabled auto update for the selected devices.');moveDisableToEnable();");
			}else if(actionType.equalsIgnoreCase("delete")){
				int deletedCount=DataLayerMgr.executeUpdate("DELETE_COLUMN_MAP_TEMPLATE", form, true);
				if(deletedCount==1){
					responseText.append("setTemplateMessage(1,'Successfully deleted the template.');mapDeleted();");
				}else{
					responseText.append("setTemplateMessage(-1,'Failed to delete the template. There are still devices using the template.');");
				}
			}else if(actionType.equalsIgnoreCase("saveNew")||actionType.equalsIgnoreCase("save")){
				UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
				String columnMapName=form.getString("columnMapName", true);
				String columnMapValue=form.getString("columnMapContent", true).trim();
				columnMapValue=columnMapValue.replaceAll("\r", "");
				columnMapValue=columnMapValue.replaceAll("==", "=");
				ArrayList<String> mdbNumber=new ArrayList<String>();
				ArrayList<String> vendColumn=new ArrayList<String>();
				ArrayList<String> code=new ArrayList<String>();
				String[] keyValueTrio=columnMapValue.split("\n");
				TreeSet<String> newMapSet=new TreeSet<String>();
				for(String aTrio:keyValueTrio){
					if(aTrio.contains("=")){
						String[] keyValue=aTrio.split("=");
						if(keyValue.length>3){
							responseText.append("setTemplateMessage(-1,'Column map value cannot contain more than 3 values, divided with = sign. Please correct it.');");
							writeResponse(response, responseText.toString());
							return;
						}
						String mdbNumberStr=(keyValue.length>0) ? keyValue[0].trim() : "";
						String vendColumnStr=(keyValue.length>1) ? keyValue[1].trim() : "";
						String codeStr = (keyValue.length==3) ? keyValue[2].trim() : null;
						if(mdbNumberStr.length()>0&&vendColumnStr.length()>0){
							if(mdbNumber.contains(mdbNumberStr)){
								responseText.append("setTemplateMessage(-1,'Template has duplicate MDB # ("+StringUtils.prepareCDATA(mdbNumberStr)+") mapped to different vend column.');");
								writeResponse(response, responseText.toString());
								return;
							}else{
								mdbNumber.add(mdbNumberStr);
								vendColumn.add(vendColumnStr);
								code.add(codeStr);
								newMapSet.add(mdbNumberStr+"="+vendColumnStr+"="+codeStr);
							}
						}else{
							responseText.append("setTemplateMessage(-1,'Bad format for column map content.');");
							writeResponse(response, responseText.toString());
							return;
						}
					}else{
						responseText.append("setTemplateMessage(-1,'Bad format for column map content.');");
						writeResponse(response, responseText.toString());
						return;
					}
				}
				ColumnMap matchedMap=null;
				if(actionType.equalsIgnoreCase("saveNew")&& form.get("forceToSave")==null){
					matchedMap=findMatchedColumnMap(form, newMapSet);
				}
				if(matchedMap!=null){
					StringBuilder mapContentValue=new StringBuilder();
					for(String aTrio:matchedMap.getColumnMapValue()){
						mapContentValue.append(aTrio+"\\n");
					}
					if(matchedMap.isSuperSet()>0){
						responseText.append("$('columnMapContentCompare').set('value','"+StringUtils.prepareJavascriptPrintable(StringUtils.escape(mapContentValue.toString(), ColumnMapSearchStep.javascriptEscape, '\\'))+"');setTemplateMessage(-1,'The map you try to create is a subset of map:"+StringUtils.prepareCDATA(matchedMap.getColumnMapName())+". Are you sure you want to create it? If so, select \"Force to save\" and Save.'); enableForceSave("+matchedMap.getColumnMapId()+");");
					}else if(matchedMap.isSuperSet()==0){
						responseText.append("setTemplateMessage(-1,'The map you try to create is identical to map:"+StringUtils.prepareCDATA(matchedMap.getColumnMapName())+". Please just use that map instead.');enableSelectCompareButton("+matchedMap.getColumnMapId()+");");
					}else{
						responseText.append("$('columnMapContentCompare').set('value','"+StringUtils.prepareJavascriptPrintable(StringUtils.escape(mapContentValue.toString(), ColumnMapSearchStep.javascriptEscape, '\\'))+"');setTemplateMessage(-1,'The map you try to create has only "+matchedMap.getDiffSize()+" fields not covered by map:"+StringUtils.prepareCDATA(matchedMap.getColumnMapName())+". Please check the difference and if you still want to create the map, select Force to save \"Yes\" and Save.');enableForceSave("+matchedMap.getColumnMapId()+");");
					}
				}else{
					Map<String, Object> params = new HashMap<String, Object>();
					params.put("userId", user.getUserId());
					params.put("mdbNumber", mdbNumber.toArray());
					params.put("vendColumn", vendColumn.toArray());
					params.put("columnMapName", columnMapName);
					params.put("code", code);
					params.put("columnMapId", form.get("columnMapId"));
					DataLayerMgr.executeUpdate("SAVE_COLUMN_MAP_TEMPLATE", params, true);
					String scope;
					if(user.isInternal()){
						scope="[Global] ";
					}else{
						scope="[Customer] ";
					}
					if(actionType.equalsIgnoreCase("saveNew")){
						responseText.append("setTemplateMessage(1,'Successfully created the template.');mapCreated("+params.get("columnMapId")+",'"+scope+"');");
					}else{
						responseText.append("setTemplateMessage(1,'Successfully saved the template.');mapEdited("+params.get("columnMapId")+",'"+scope+"');");
					}
				}
			}else{
				responseText.append("setTemplateMessage(1,'Unknown actionType.');");
				log.info("Unknown actionType="+actionType);
			}
			writeResponse(response, responseText.toString());
			log.info("actionType="+actionType+" is completed.");
		}catch(SQLException e){
			String errorMsg;
			if(e.getErrorCode()==1){
				errorMsg="Column map template name already exists. Please choose a different name.";
			}else if(e.getErrorCode()==12899){
				errorMsg="Vend Column maximum size is 100. Code maximum size is 50. Please correct the one which exceeds this size.";
			}else{
				errorMsg="An application error occured.";
			}
			writeResponse(response, "setTemplateMessage(-1,'"+errorMsg+"');");
			log.error(e.getMessage(), e);
		}catch(Exception e) {
			writeResponse(response, "setTemplateMessage(-1,'An application error occurred.');");
			log.error(e.getMessage(), e);
		} 				
	}
	
	public void writeResponse(HttpServletResponse response, String responseText) throws ServletException{
		try{
			PrintStream out = new PrintStream(response.getOutputStream());
			out.println("<script type=\"text/javascript\">//<![CDATA[");
			out.println(responseText);
			out.print("//]]></script>");
			out.println();
			out.flush();
		}catch(IOException e){
			throw new ServletException(e);
		}
	}
	
}
