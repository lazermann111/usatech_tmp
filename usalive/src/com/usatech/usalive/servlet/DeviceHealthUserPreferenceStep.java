package com.usatech.usalive.servlet;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.io.Log;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.RequestUtils;
import simple.servlet.steps.AbstractStep;

public class DeviceHealthUserPreferenceStep extends AbstractStep {
	private static final Log log = Log.getLog();
	private static final String DEVICE_HEALTH_PARAM_PREFIX="deviceHealthPreferenceId-";
	public DeviceHealthUserPreferenceStep(){
		super();
	}

	@Override
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		UsaliveUser user = (UsaliveUser) RequestUtils.getUser(request);
		long userId=user.getUserId();
		Map<String, Object> params=form.getParameters();
		Map<String, Object> dbParam=new HashMap<String, Object>();
		dbParam.put("userId", userId);
		try {
			for(Map.Entry<String, Object> entry:params.entrySet()){
				String paramKey=entry.getKey();
				if(paramKey.startsWith(DEVICE_HEALTH_PARAM_PREFIX)){
					dbParam.put("deviceHealthPreferenceId", paramKey.substring(DEVICE_HEALTH_PARAM_PREFIX.length()));
					dbParam.put("deviceHealthPreferenceValue", entry.getValue());
					DataLayerMgr.executeCall("UPSERT_DEVICE_HEALTH_USER_PREFERENCES", dbParam, true);
				}
			}
		} catch(SQLException e) {
			log.info("Could not upsert device health preference for userId=" + userId, e);
			throw new ServletException(e);
		} catch(DataLayerException e) {
			throw new ServletException(e);
		}
	}

}
