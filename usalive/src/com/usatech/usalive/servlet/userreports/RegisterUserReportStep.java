package com.usatech.usalive.servlet.userreports;

import com.usatech.report.build.AbstractBuildActivityFolio;
import com.usatech.report.build.BuildActivityFolioResolver;
import com.usatech.usalive.servlet.SetUserReportParamsStep;
import com.usatech.usalive.servlet.UsaliveUser;
import simple.db.DataLayerException;
import simple.db.DataLayerMgr;
import simple.falcon.engine.*;
import simple.falcon.engine.standard.StandardReport;
import simple.falcon.servlet.FalconServlet;
import simple.falcon.servlet.FolioStepHelper;
import simple.results.Results;
import simple.servlet.*;
import simple.servlet.steps.AbstractStep;
import simple.text.StringUtils;
import simple.translator.Translator;
import simple.xml.XMLBuilder;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.sql.SQLException;
import java.util.*;

/**
 * Processing situation where user custom report selected and we need to register this
 * new report in REPORTS table before can continue with others steps. User custom reports
 * will have negative IDs in REPORTS.
 */
public class RegisterUserReportStep extends AbstractStep {
	/**
	 * When creating new report for user custom report in report generator we should only use params related to
	 * business logic of this report and omit all unneeded params.
	 */
	private final static Set<String> EXCLUDED_REPORT_PARAMS;

	private final static long RESTORE_DEFAULT_REPORTS_ID = 987654321012345678L;

	static {
		Set<String> tmp = new HashSet<>();
		tmp.add("scene");
		tmp.add("request");
		tmp.add("session");
		tmp.add("token");
		EXCLUDED_REPORT_PARAMS = Collections.unmodifiableSet(tmp);
	}

	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response)
			throws ServletException {
		Long userReportId = form.getLong("userAvaReportId", false, -1L);
		if (userReportId != -1L) {
			// User defined report selected, so add it to system reports table
			addUserReportToRegister(form, request, userReportId);
			replaceAttributeInForm(form, "insertReport", "Y");
		} else {
			long reportId = form.getLong("reportId", false, -1L);
			if (RESTORE_DEFAULT_REPORTS_ID == reportId) {
				restoreDefaultReports(dispatcher, form, request, response);
			} else {
				replaceAttributeInForm(form, "insertReport", "Y");
			}
		}
	}

	private void restoreDefaultReports(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		List<Long> defaultReportIds = getDefaultReportIds();
		Iterator<Long> iterator = defaultReportIds.iterator();
		while (iterator.hasNext()) {
			Long reportId = iterator.next();
			if (isReportAlreadyRegistered(form, reportId)) {
				iterator.remove();
			}
		}
		int defaultReportsSize = defaultReportIds.size();
		int i = 0;
		for (Long reportId : defaultReportIds) {
			replaceReportIdInForm(form, reportId);
			if (i >= defaultReportsSize - 1) {
				//HACK: We should leave last report to be registered with default actions procedure
				//HACK: So we will able to reuse existing code of report register to proceed changes
				replaceAttributeInForm(form, "insertReport", "Y");
				break;
			}
			registerReport(form);
			registerReportParams(dispatcher, form, request, response);
			i++;
		}
	}

	private void replaceReportIdInForm(InputForm form, long newReportId) {
		replaceAttributeInForm(form, "reportId", newReportId);
	}

	private void replaceAttributeInForm(InputForm form, String attrName, Object value) {
		form.set(attrName, value);
		form.setAttribute(attrName, value);
		RequestUtils.setAttribute(form.getRequest(), attrName, value, "request");
	}

	private List<Long> getDefaultReportIds() throws ServletException {
		try {
			Map<String, Object> params = new HashMap<>();
			Results defaultReports = DataLayerMgr.executeQuery("GET_DEFAULT_USER_REPORTS", params);
			if (defaultReports.getRowCount() > 0) {
				List<Long> rv = new ArrayList<>();
				while (defaultReports.next()) {
					Long defaultReportId = defaultReports.getValue("defaultReportId", Long.class);
					rv.add(defaultReportId);
				}
				return rv;
			}
			throw new ServletException("Default reports not found");
		} catch (Exception e) {
			throw new ServletException("Failed to get default user reports", e);
		}
	}

	private void registerReport(InputForm form) throws ServletException {
		try {
			Map<String, Object> reportParams = new HashMap<>();
			reportParams.put("simple.servlet.ServletUser.userId", form.getLong("simple.servlet.ServletUser.userId", true, -1L));
			reportParams.put("reportId", form.getLong("reportId", true, -1L));
			reportParams.put("transportId", form.getLong("transportId", true, -1L));
			reportParams.put("email_errors_to", form.getStringSafely("email_errors_to", ""));
			Integer userReportId = (Integer)(DataLayerMgr.executeCall("INSERT_USER_REPORT", reportParams, true)[0]);
			replaceAttributeInForm(form, "userReportId", userReportId);
		} catch (Exception e) {
			throw new ServletException("Failed to add user report to database", e);
		}
	}

	private void registerReportParams(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		SetUserReportParamsStep nextStep = new SetUserReportParamsStep();
		nextStep.perform(dispatcher, form, request, response);
	}

	private boolean isReportAlreadyRegistered(InputForm form, Long reportId) throws ServletException {
		try {
			Map<String, Object> params = new HashMap<>();
			params.put("simple.servlet.ServletUser.userId", form.getLong("simple.servlet.ServletUser.userId", true, -1L));
			params.put("reportId", reportId == null ? form.getLong("reportId", true, -1L) : reportId);
			params.put("transportId", form.getLong("transportId", true, -1L));
			Results registeredStatus = DataLayerMgr.executeQuery("IS_USER_REPORT_ALREADY_REGISTERED_IN_REPORT_REGISTER", params);
			if (registeredStatus.next()) {
				String status = registeredStatus.getFormattedValue("registerStatus");
				return "Y".equals(status);
			}
			return false;
		} catch (Exception e) {
			throw new ServletException("Failed to get report register status", e);
		}
	}

	protected boolean isCustomerUser(InputForm form) {
		UsaliveUser user = (UsaliveUser) RequestUtils.getUser(form);
		return user != null && user.getCustomerId() != 0 && user.getUserType() == 8;
	}

	private void addUserReportToRegister(InputForm form, HttpServletRequest request, Long userReportId) throws ServletException {
		// Get user custom report (URL) and title
		Map<String,String> urlAndTitle = getCustomReportUrlAndName(request, userReportId);
		String customReportUrl = (String) urlAndTitle.keySet().toArray()[0];
		String title = urlAndTitle.get(customReportUrl);
		if (customReportUrl == null) {
			throw new ServletException("User defined report not found");
		}
		// Parse URL to get Folio instance name to use
		String actionName = getActionByUrl(customReportUrl);
		Map<String, Object> params = StringUtils.getQueryParams(customReportUrl);
		if (params != null) {
			for (String key : params.keySet()) {
				form.setAttribute(key, params.get(key));
			}
		}
		BuildActivityFolioResolver buildActivityFolioResolver = new BuildActivityFolioResolver();
		Class<? extends AbstractBuildActivityFolio> buildFolioClass = buildActivityFolioResolver.resolve(actionName);
		// Instantiate Folio
		AbstractBuildActivityFolio buildFolio = instantiateBuildActivityFolio(buildFolioClass.getName());
		// Call updateFolio
		ExecuteEngine executeEngine = (ExecuteEngine) form.getAttribute(FalconServlet.ATTRIBUTE_EXECUTE_ENGINE);
		Executor executor = (Executor) form.getAttribute(SimpleServlet.ATTRIBUTE_USER, "session");
		if (executor.getUserGroupIds() == null || executor.getUserGroupIds().length == 0) {
			throw new ServletException("You do not have access to run reports");
		}
		Translator translator = form.getTranslator();
		Scene scene = FolioStepHelper.createScene(form, request, translator);
		Folio folio = getFolio(buildFolio, executeEngine, form, scene);
		buildFolio.updateFolio(folio, form, isCustomerUser(form));
		// Use AbstractBuildActivityFolio::buildReport to build Report
		StandardReport report = buildReport(executeEngine, folio, scene);
		Long reportId = addReportToDatabase(report, title, params);
		replaceReportIdInRequest(request, reportId);
	}

	private void replaceReportIdInRequest(HttpServletRequest request, Long reportId) {
		RequestUtils.setAttribute(request, "reportId", reportId, "request");
	}

	private Long addReportToDatabase(StandardReport report, String reportTitle, Map<String, Object> params) throws ServletException {
		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			XMLBuilder xmlBuilder = new XMLBuilder(out);
			report.writeXML(null, xmlBuilder);
			String xmlStr = new String(out.toByteArray(), "UTF-8");
			Map<String, Object> reportParams = new HashMap<>();
			Long nextUserReportId = generateNextUserReportId();
			reportParams.put("reportId", nextUserReportId);
			reportParams.put("title", reportTitle);
			reportParams.put("reportName", reportTitle);
			DataLayerMgr.executeCall("INSERT_REPORT", reportParams, true);
			addReportParamToDatabase(nextUserReportId, "reportXML", xmlStr);
			Set<String> keys = params.keySet();
			for (String key : keys) {
				if (isExcludedParam(key)) {
					continue;
				}
				addReportParamToDatabase(nextUserReportId, key, params.get(key));
			}
			return nextUserReportId;
		} catch (Exception e) {
			throw new ServletException("Failed to add report to database", e);
		}
	}

	private boolean isExcludedParam(String sourceStr) {
		for (String subStr : EXCLUDED_REPORT_PARAMS) {
			if (sourceStr.contains(subStr)) {
				return true;
			}
		}
		return false;
	}

	private void addReportParamToDatabase(long reportId, String paramName, Object paramValue) throws SQLException, DataLayerException {
		Map<String, Object> reportParamsParams = new HashMap<>();
		reportParamsParams.put("reportId", reportId);
		reportParamsParams.put("paramName", paramName);
		// Exclude exact report date values. We don't need them in Report Register
		if("params.beginDate".equals(paramName)) {
			reportParamsParams.put("paramValue", "{b}");
		} else if("params.endDate".equals(paramName)) {
			reportParamsParams.put("paramValue", "{e}");
		} else {
			reportParamsParams.put("paramValue", paramValue);
		}
		if ("reportXML".equals(paramName)) {
			DataLayerMgr.executeCall("INSERT_REPORT_PARAM", reportParamsParams, true);
		} else {
			DataLayerMgr.executeCall("INSERT_SHORT_REPORT_PARAM", reportParamsParams, true);
		}
	}

	Long generateNextUserReportId() throws ServletException {
		try {
			Map<String, Object> params = new HashMap<>();
			Results results = DataLayerMgr.executeQuery("GET_NEXT_USER_REPORT_ID", params);
			if (results.getRowCount() > 0) {
				if (results.next()) {
					return results.getValue("nextUserReportId", Long.class);
				}
			}
			throw new ServletException("Can't generate user defined report ID");
		} catch (Exception e) {
			throw new ServletException("Failed to generate user defined report ID", e);
		}
	}

	private StandardReport buildReport(ExecuteEngine executeEngine, Folio folio, Scene scene) throws ServletException {
		try {
			Report report = executeEngine.getDesignEngine().newReport(scene.getTranslator());
			report.setFolios(new Folio[] { folio });
			report.setName(folio.getName());
			report.setOutputType(folio.getOutputType());
			for (String dir : folio.getDirectiveNames()) {
				report.setDirective(dir, folio.getDirective(dir));
			}
			if (!(report instanceof StandardReport)) {
				throw new ServletException("report should be instance of StandardReport");
			}
			return (StandardReport)report;
		} catch (DesignException designException) {
			throw new ServletException("Failed to build report object,", designException);
		}
	}

	Folio getFolio(AbstractBuildActivityFolio buildFolio, ExecuteEngine executeEngine, InputForm form, Scene scene)
			throws ServletException {
		return buildFolio.getFolio(executeEngine, form, scene);
	}

	String getActionByUrl(String url) {
		int iIndex = url.indexOf(".i");
		if (iIndex == -1) {
			throw new IllegalArgumentException("This URL do not contain action part. URL = " + url);
		}
		int startIdx = url.startsWith("./") ? 2 : 0;
		return url.substring(startIdx, iIndex);
	}

	private AbstractBuildActivityFolio instantiateBuildActivityFolio(String clazz) throws ServletException {
		try {
			return (AbstractBuildActivityFolio) (Class.forName(clazz)).newInstance();
		} catch (Exception exception) {
			throw new ServletException("Failed to create new instance class " + clazz, exception);
		}

	}

	Map<String,String> getCustomReportUrlAndName(HttpServletRequest request, Long userReportId) throws ServletException {
		try {
			Map<String, Object> params = new HashMap<>();
			UsaliveUser user = (UsaliveUser) RequestUtils.getUser(request);
			params.put("simple.servlet.ServletUser.userId", user.getUserId());
			Results userReports = DataLayerMgr.executeQuery("GET_AVAILABLE_USER_REPORTS", params);
			// Results userReports = RequestUtils.getAttribute(request,
			// "userAvaReports", Results.class, true);
			if (userReports.getRowCount() > 0) {
				while (userReports.next()) {
					Integer id = userReports.getValue("id", Integer.class);
					if (userReportId.equals(id.longValue())) {
						Map<String,String> rv = new HashMap<>();
						rv.put(userReports.getFormattedValue("href"), userReports.getFormattedValue("title"));
						return rv;
					}
				}
			}
			throw new ServletException("User defined report not found");
		} catch (Exception e) {
			throw new ServletException("Failed to get custom report URL", e);
		}
	}
}
