package com.usatech.usalive.servlet;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simple.app.Publisher;
import simple.app.ServiceException;
import simple.io.ByteInput;
import simple.servlet.Dispatcher;
import simple.servlet.InputForm;
import simple.servlet.steps.AbstractStep;

import com.usatech.app.MessageChain;
import com.usatech.app.MessageChainService;
import com.usatech.app.MessageChainStep;
import com.usatech.app.MessageChainV11;
import com.usatech.usalive.hybrid.HybridServlet;
import com.usatech.usalive.report.AsyncReportEngine;

public class PublishStep extends AbstractStep {
	protected String queueKey;
	protected boolean multicast;
	protected final Map<String,Object> attributes = new HashMap<String, Object>();
	
	public void perform(Dispatcher dispatcher, InputForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
		AsyncReportEngine arEngine = (AsyncReportEngine) form.getAttribute(HybridServlet.ATTRIBUTE_ASYNCH_REPORT_ENGINE);
		Publisher<ByteInput> publisher = arEngine.getPublisher();
		MessageChain mc = new MessageChainV11();
		MessageChainStep step = mc.addStep(queueKey, multicast);
		step.getAttributes().putAll(attributes);
		try {
			MessageChainService.publish(mc, publisher);
		} catch(ServiceException e) {
			throw new ServletException(e);
		}
	}

	public String getQueueKey() {
		return queueKey;
	}

	public void setQueueKey(String queueKey) {
		this.queueKey = queueKey;
	}

	public boolean isMulticast() {
		return multicast;
	}

	public void setMulticast(boolean multicast) {
		this.multicast = multicast;
	}

	public Object getAttribute(String attributeName) {
		return attributes.get(attributeName);
	}
	
	public void setAttribute(String attributeName, Object attributeValue) {
		attributes.put(attributeName, attributeValue);
	}
}
