
/**
 * PepsiSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.4  Built on : Dec 28, 2015 (10:03:39 GMT)
 */
    package com.usatech.usalive.pepsi;

/**
     *  PepsiSkeleton java skeleton for the axisService
     */
    public class PepsiSkeleton{  
         
        /**
         * Auto generated method signature
         * 
                                     * @param hardwareDeployment 
             * @return hardwareDeploymentResponse 
         */
        
                 public com.usatech.usalive.pepsi.HardwareDeploymentResponseE hardwareDeployment
                  (
                  com.usatech.usalive.pepsi.HardwareDeploymentE hardwareDeployment
                  )
            {                	 
                	 return PepsiRequestHandler.hardwareDeployment(hardwareDeployment);
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param hardwareActivation 
             * @return hardwareActivationResponse 
         */
        
                 public com.usatech.usalive.pepsi.HardwareActivationResponseE hardwareActivation
                  (
                  com.usatech.usalive.pepsi.HardwareActivationE hardwareActivation
                  )
            {
                	 return PepsiRequestHandler.hardwareActivation(hardwareActivation);
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param hardwareUninstall 
             * @return hardwareUninstallResponse 
         */
        
                 public com.usatech.usalive.pepsi.HardwareUninstallResponseE hardwareUninstall
                  (
                  com.usatech.usalive.pepsi.HardwareUninstallE hardwareUninstall
                  )
            {
                	 return PepsiRequestHandler.hardwareUninstall(hardwareUninstall);
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param hardwareDeactivation 
             * @return hardwareDeactivationResponse 
         */
        
                 public com.usatech.usalive.pepsi.HardwareDeactivationResponseE hardwareDeactivation
                  (
                  com.usatech.usalive.pepsi.HardwareDeactivationE hardwareDeactivation
                  )
            {
                	 return PepsiRequestHandler.hardwareDeactivation(hardwareDeactivation);
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param hardwareStatus 
             * @return hardwareStatusResponse 
         */
        
                 public com.usatech.usalive.pepsi.HardwareStatusResponseE hardwareStatus
                  (
                  com.usatech.usalive.pepsi.HardwareStatusE hardwareStatus
                  )
            {        
                	 return PepsiRequestHandler.hardwareStatus(hardwareStatus);
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param hardwareUpdate 
             * @return hardwareUpdateResponse 
         */
        
                 public com.usatech.usalive.pepsi.HardwareUpdateResponseE hardwareUpdate
                  (
                  com.usatech.usalive.pepsi.HardwareUpdateE hardwareUpdate
                  )
            {
                	 return PepsiRequestHandler.hardwareUpdate(hardwareUpdate);
        }
     
    }
    