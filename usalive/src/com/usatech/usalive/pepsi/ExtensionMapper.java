
/**
 * ExtensionMapper.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.4  Built on : Dec 28, 2015 (10:04:10 GMT)
 */

        
            package com.usatech.usalive.pepsi;
        
            /**
            *  ExtensionMapper class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class ExtensionMapper{

          public static java.lang.Object getTypeObject(java.lang.String namespaceURI,
                                                       java.lang.String typeName,
                                                       javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{

              
                  if (
                  "http://pepsi.usalive.usatech.com/".equals(namespaceURI) &&
                  "HardwareStatusResponse".equals(typeName)){
                   
                            return  com.usatech.usalive.pepsi.HardwareStatusResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://pepsi.usalive.usatech.com/".equals(namespaceURI) &&
                  "WirelessStatus".equals(typeName)){
                   
                            return  com.usatech.usalive.pepsi.WirelessStatus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://pepsi.usalive.usatech.com/".equals(namespaceURI) &&
                  "EquipmentStatus".equals(typeName)){
                   
                            return  com.usatech.usalive.pepsi.EquipmentStatus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://pepsi.usalive.usatech.com/".equals(namespaceURI) &&
                  "HardwareDeployment".equals(typeName)){
                   
                            return  com.usatech.usalive.pepsi.HardwareDeployment.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://pepsi.usalive.usatech.com/".equals(namespaceURI) &&
                  "Wireless".equals(typeName)){
                   
                            return  com.usatech.usalive.pepsi.Wireless.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://pepsi.usalive.usatech.com/".equals(namespaceURI) &&
                  "HardwareActivation".equals(typeName)){
                   
                            return  com.usatech.usalive.pepsi.HardwareActivation.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://pepsi.usalive.usatech.com/".equals(namespaceURI) &&
                  "HardwareUninstall".equals(typeName)){
                   
                            return  com.usatech.usalive.pepsi.HardwareUninstall.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://pepsi.usalive.usatech.com/".equals(namespaceURI) &&
                  "Result".equals(typeName)){
                   
                            return  com.usatech.usalive.pepsi.Result.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://pepsi.usalive.usatech.com/".equals(namespaceURI) &&
                  "EquipmentDetails".equals(typeName)){
                   
                            return  com.usatech.usalive.pepsi.EquipmentDetails.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://pepsi.usalive.usatech.com/".equals(namespaceURI) &&
                  "HardwareUpdateResponse".equals(typeName)){
                   
                            return  com.usatech.usalive.pepsi.HardwareUpdateResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://pepsi.usalive.usatech.com/".equals(namespaceURI) &&
                  "HardwareStatus".equals(typeName)){
                   
                            return  com.usatech.usalive.pepsi.HardwareStatus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://pepsi.usalive.usatech.com/".equals(namespaceURI) &&
                  "HardwareActivationResponse".equals(typeName)){
                   
                            return  com.usatech.usalive.pepsi.HardwareActivationResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://pepsi.usalive.usatech.com/".equals(namespaceURI) &&
                  "HardwareDeploymentResponse".equals(typeName)){
                   
                            return  com.usatech.usalive.pepsi.HardwareDeploymentResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://pepsi.usalive.usatech.com/".equals(namespaceURI) &&
                  "HardwareDeactivationResponse".equals(typeName)){
                   
                            return  com.usatech.usalive.pepsi.HardwareDeactivationResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://pepsi.usalive.usatech.com/".equals(namespaceURI) &&
                  "Location".equals(typeName)){
                   
                            return  com.usatech.usalive.pepsi.Location.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://pepsi.usalive.usatech.com/".equals(namespaceURI) &&
                  "HardwareDeactivation".equals(typeName)){
                   
                            return  com.usatech.usalive.pepsi.HardwareDeactivation.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://pepsi.usalive.usatech.com/".equals(namespaceURI) &&
                  "HardwareUninstallResponse".equals(typeName)){
                   
                            return  com.usatech.usalive.pepsi.HardwareUninstallResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://pepsi.usalive.usatech.com/".equals(namespaceURI) &&
                  "HardwareDetails".equals(typeName)){
                   
                            return  com.usatech.usalive.pepsi.HardwareDetails.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://pepsi.usalive.usatech.com/".equals(namespaceURI) &&
                  "HardwareUpdate".equals(typeName)){
                   
                            return  com.usatech.usalive.pepsi.HardwareUpdate.Factory.parse(reader);
                        

                  }

              
             throw new org.apache.axis2.databinding.ADBException("Unsupported type " + namespaceURI + " " + typeName);
          }

        }
    