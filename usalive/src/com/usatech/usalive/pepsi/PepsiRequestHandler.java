package com.usatech.usalive.pepsi;

public class PepsiRequestHandler {
	protected static boolean serviceEnabled;

	public static HardwareDeploymentResponseE hardwareDeployment(HardwareDeploymentE hardwareDeployment) {
		Result result = new Result();
		if (!serviceEnabled) {
			result.setResponseCode("ERROR");
			result.setResponseText("Service not enabled in this environment");
		} else {
			result.setResponseCode("CPD0001");
			result.setResponseText("Deployment Successful");
		}
		HardwareDeploymentResponse resp = new HardwareDeploymentResponse();
		resp.setResult(result);
		resp.setSENComponentID(hardwareDeployment.getHardwareDeployment().getSENComponentID());
		resp.setSENEquipmentID("Test");
		resp.setBottlerID(hardwareDeployment.getHardwareDeployment().getBottlerID());

		HardwareDeploymentResponseE respE = new HardwareDeploymentResponseE();
		respE.setHardwareDeploymentResponse(resp);
		return respE;
	}

	public static HardwareActivationResponseE hardwareActivation(HardwareActivationE hardwareActivation) {
		Result result = new Result();
		if (!serviceEnabled) {
			result.setResponseCode("ERROR");
			result.setResponseText("Service not enabled in this environment");
		} else {
			result.setResponseCode("CPD0001");
			result.setResponseText("Activation Successful");
		}
		HardwareActivationResponse resp = new HardwareActivationResponse();
		resp.setResult(result);
		resp.setBottlerID(hardwareActivation.getHardwareActivation().getBottlerID());
		resp.setSENComponentID(hardwareActivation.getHardwareActivation().getSENComponentID());

		HardwareActivationResponseE respE = new HardwareActivationResponseE();
		respE.setHardwareActivationResponse(resp);
		return respE;
	}

	public static HardwareUninstallResponseE hardwareUninstall(HardwareUninstallE hardwareUninstall) {
		Result result = new Result();
		if (!serviceEnabled) {
			result.setResponseCode("ERROR");
			result.setResponseText("Service not enabled in this environment");
		} else {
			result.setResponseCode("CPU0001");
			result.setResponseText("Uninstall Successful");
		}
		HardwareUninstallResponse resp = new HardwareUninstallResponse();
		resp.setResult(result);
		resp.setBottlerID(hardwareUninstall.getHardwareUninstall().getBottlerID());
		resp.setSENComponentID(hardwareUninstall.getHardwareUninstall().getSENComponentID());

		HardwareUninstallResponseE respE = new HardwareUninstallResponseE();
		respE.setHardwareUninstallResponse(resp);
		return respE;
	}

	public static HardwareDeactivationResponseE hardwareDeactivation(HardwareDeactivationE hardwareDeactivation) {
		Result result = new Result();
		if (!serviceEnabled) {
			result.setResponseCode("ERROR");
			result.setResponseText("Service not enabled in this environment");
		} else {
			result.setResponseCode("CPD0001");
			result.setResponseText("Deactivation Successful");
		}
		HardwareDeactivationResponse resp = new HardwareDeactivationResponse();
		resp.setResult(result);
		resp.setBottlerID(hardwareDeactivation.getHardwareDeactivation().getBottlerID());
		resp.setSENComponentID(hardwareDeactivation.getHardwareDeactivation().getSENComponentID());

		HardwareDeactivationResponseE respE = new HardwareDeactivationResponseE();
		respE.setHardwareDeactivationResponse(resp);
		return respE;
	}

	public static HardwareStatusResponseE hardwareStatus(HardwareStatusE hardwareStatus) {
		HardwareStatusResponse resp = new HardwareStatusResponse();
		if (!serviceEnabled) {
			resp.setComponentState("ERROR");
		} else {
			resp.setComponentState("CPD0001");
		}

		resp.setBottlerID(hardwareStatus.getHardwareStatus().getBottlerID());
		resp.setSENComponentID(hardwareStatus.getHardwareStatus().getSENComponentID());
		resp.setBottlerWarehouseID("Test");

		HardwareDetails hd = new HardwareDetails();
		hd.setComponentManufacturer("Test");
		hd.setComponentModel("Test");
		resp.setHardwareDetails(hd);

		WirelessStatus ws = new WirelessStatus();
		ws.setActivationDateTime("");
		ws.setAirtimeStatus("Test");
		ws.setMDN("Test");
		resp.setWirelessStatus(ws);

		EquipmentStatus es = new EquipmentStatus();
		es.setEquipmentID("Test");
		es.setLastHeartbeat("Test");
		resp.setEquipmentStatus(es);

		HardwareStatusResponseE respE = new HardwareStatusResponseE();
		respE.setHardwareStatusResponse(resp);
		return respE;
	}

	public static HardwareUpdateResponseE hardwareUpdate(HardwareUpdateE hardwareUpdate) {
		Result result = new Result();
		if (!serviceEnabled) {
			result.setResponseCode("ERROR");
			result.setResponseText("Service not enabled in this environment");
		} else {
			result.setResponseCode("CPD0001");
			result.setResponseText("Update Successful");
		}
		HardwareUpdateResponse resp = new HardwareUpdateResponse();
		resp.setResult(result);
		resp.setSENComponentID(hardwareUpdate.getHardwareUpdate().getSENComponentID());
		resp.setSENEquipmentID("Test");
		resp.setBottlerID(hardwareUpdate.getHardwareUpdate().getBottlerID());
		resp.setBottlerWarehouseID(hardwareUpdate.getHardwareUpdate().getBottlerWarehouseID());

		HardwareUpdateResponseE respE = new HardwareUpdateResponseE();
		respE.setHardwareUpdateResponse(resp);
		return respE;
	}

	public static boolean isServiceEnabled() {
		return serviceEnabled;
	}

	public static void setServiceEnabled(boolean serviceEnabled) {
		PepsiRequestHandler.serviceEnabled = serviceEnabled;
	}
}
