
/**
 * PepsiMessageReceiverInOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.4  Built on : Dec 28, 2015 (10:03:39 GMT)
 */
        package com.usatech.usalive.pepsi;

        /**
        *  PepsiMessageReceiverInOut message receiver
        */

        public class PepsiMessageReceiverInOut extends org.apache.axis2.receivers.AbstractInOutMessageReceiver{


        public void invokeBusinessLogic(org.apache.axis2.context.MessageContext msgContext, org.apache.axis2.context.MessageContext newMsgContext)
        throws org.apache.axis2.AxisFault{

        try {

        // get the implementation class for the Web Service
        Object obj = getTheImplementationObject(msgContext);

        PepsiSkeleton skel = (PepsiSkeleton)obj;
        //Out Envelop
        org.apache.axiom.soap.SOAPEnvelope envelope = null;
        //Find the axisOperation that has been set by the Dispatch phase.
        org.apache.axis2.description.AxisOperation op = msgContext.getOperationContext().getAxisOperation();
        if (op == null) {
        throw new org.apache.axis2.AxisFault("Operation is not located, if this is doclit style the SOAP-ACTION should specified via the SOAP Action to use the RawXMLProvider");
        }

        java.lang.String methodName;
        if((op.getName() != null) && ((methodName = org.apache.axis2.util.JavaUtils.xmlNameToJavaIdentifier(op.getName().getLocalPart())) != null)){


        

            if("hardwareDeployment".equals(methodName)){
                
                com.usatech.usalive.pepsi.HardwareDeploymentResponseE hardwareDeploymentResponse25 = null;
	                        com.usatech.usalive.pepsi.HardwareDeploymentE wrappedParam =
                                                             (com.usatech.usalive.pepsi.HardwareDeploymentE)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.usalive.pepsi.HardwareDeploymentE.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               hardwareDeploymentResponse25 =
                                                   
                                                   
                                                         skel.hardwareDeployment(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), hardwareDeploymentResponse25, false, new javax.xml.namespace.QName("http://pepsi.usalive.usatech.com/",
                                                    "hardwareDeployment"));
                                    } else 

            if("hardwareActivation".equals(methodName)){
                
                com.usatech.usalive.pepsi.HardwareActivationResponseE hardwareActivationResponse27 = null;
	                        com.usatech.usalive.pepsi.HardwareActivationE wrappedParam =
                                                             (com.usatech.usalive.pepsi.HardwareActivationE)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.usalive.pepsi.HardwareActivationE.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               hardwareActivationResponse27 =
                                                   
                                                   
                                                         skel.hardwareActivation(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), hardwareActivationResponse27, false, new javax.xml.namespace.QName("http://pepsi.usalive.usatech.com/",
                                                    "hardwareActivation"));
                                    } else 

            if("hardwareUninstall".equals(methodName)){
                
                com.usatech.usalive.pepsi.HardwareUninstallResponseE hardwareUninstallResponse29 = null;
	                        com.usatech.usalive.pepsi.HardwareUninstallE wrappedParam =
                                                             (com.usatech.usalive.pepsi.HardwareUninstallE)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.usalive.pepsi.HardwareUninstallE.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               hardwareUninstallResponse29 =
                                                   
                                                   
                                                         skel.hardwareUninstall(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), hardwareUninstallResponse29, false, new javax.xml.namespace.QName("http://pepsi.usalive.usatech.com/",
                                                    "hardwareUninstall"));
                                    } else 

            if("hardwareDeactivation".equals(methodName)){
                
                com.usatech.usalive.pepsi.HardwareDeactivationResponseE hardwareDeactivationResponse31 = null;
	                        com.usatech.usalive.pepsi.HardwareDeactivationE wrappedParam =
                                                             (com.usatech.usalive.pepsi.HardwareDeactivationE)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.usalive.pepsi.HardwareDeactivationE.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               hardwareDeactivationResponse31 =
                                                   
                                                   
                                                         skel.hardwareDeactivation(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), hardwareDeactivationResponse31, false, new javax.xml.namespace.QName("http://pepsi.usalive.usatech.com/",
                                                    "hardwareDeactivation"));
                                    } else 

            if("hardwareStatus".equals(methodName)){
                
                com.usatech.usalive.pepsi.HardwareStatusResponseE hardwareStatusResponse33 = null;
	                        com.usatech.usalive.pepsi.HardwareStatusE wrappedParam =
                                                             (com.usatech.usalive.pepsi.HardwareStatusE)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.usalive.pepsi.HardwareStatusE.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               hardwareStatusResponse33 =
                                                   
                                                   
                                                         skel.hardwareStatus(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), hardwareStatusResponse33, false, new javax.xml.namespace.QName("http://pepsi.usalive.usatech.com/",
                                                    "hardwareStatus"));
                                    } else 

            if("hardwareUpdate".equals(methodName)){
                
                com.usatech.usalive.pepsi.HardwareUpdateResponseE hardwareUpdateResponse35 = null;
	                        com.usatech.usalive.pepsi.HardwareUpdateE wrappedParam =
                                                             (com.usatech.usalive.pepsi.HardwareUpdateE)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.usatech.usalive.pepsi.HardwareUpdateE.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               hardwareUpdateResponse35 =
                                                   
                                                   
                                                         skel.hardwareUpdate(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), hardwareUpdateResponse35, false, new javax.xml.namespace.QName("http://pepsi.usalive.usatech.com/",
                                                    "hardwareUpdate"));
                                    
            } else {
              throw new java.lang.RuntimeException("method not found");
            }
        

        newMsgContext.setEnvelope(envelope);
        }
        }
        catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
        }
        
        //
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.usalive.pepsi.HardwareDeploymentE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.usalive.pepsi.HardwareDeploymentE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.usalive.pepsi.HardwareDeploymentResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.usalive.pepsi.HardwareDeploymentResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.usalive.pepsi.HardwareActivationE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.usalive.pepsi.HardwareActivationE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.usalive.pepsi.HardwareActivationResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.usalive.pepsi.HardwareActivationResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.usalive.pepsi.HardwareUninstallE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.usalive.pepsi.HardwareUninstallE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.usalive.pepsi.HardwareUninstallResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.usalive.pepsi.HardwareUninstallResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.usalive.pepsi.HardwareDeactivationE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.usalive.pepsi.HardwareDeactivationE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.usalive.pepsi.HardwareDeactivationResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.usalive.pepsi.HardwareDeactivationResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.usalive.pepsi.HardwareStatusE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.usalive.pepsi.HardwareStatusE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.usalive.pepsi.HardwareStatusResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.usalive.pepsi.HardwareStatusResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.usalive.pepsi.HardwareUpdateE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.usalive.pepsi.HardwareUpdateE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.usatech.usalive.pepsi.HardwareUpdateResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.usatech.usalive.pepsi.HardwareUpdateResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.usalive.pepsi.HardwareDeploymentResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.usalive.pepsi.HardwareDeploymentResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.usalive.pepsi.HardwareDeploymentResponseE wrapHardwareDeployment(){
                                com.usatech.usalive.pepsi.HardwareDeploymentResponseE wrappedElement = new com.usatech.usalive.pepsi.HardwareDeploymentResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.usalive.pepsi.HardwareActivationResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.usalive.pepsi.HardwareActivationResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.usalive.pepsi.HardwareActivationResponseE wrapHardwareActivation(){
                                com.usatech.usalive.pepsi.HardwareActivationResponseE wrappedElement = new com.usatech.usalive.pepsi.HardwareActivationResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.usalive.pepsi.HardwareUninstallResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.usalive.pepsi.HardwareUninstallResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.usalive.pepsi.HardwareUninstallResponseE wrapHardwareUninstall(){
                                com.usatech.usalive.pepsi.HardwareUninstallResponseE wrappedElement = new com.usatech.usalive.pepsi.HardwareUninstallResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.usalive.pepsi.HardwareDeactivationResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.usalive.pepsi.HardwareDeactivationResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.usalive.pepsi.HardwareDeactivationResponseE wrapHardwareDeactivation(){
                                com.usatech.usalive.pepsi.HardwareDeactivationResponseE wrappedElement = new com.usatech.usalive.pepsi.HardwareDeactivationResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.usalive.pepsi.HardwareStatusResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.usalive.pepsi.HardwareStatusResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.usalive.pepsi.HardwareStatusResponseE wrapHardwareStatus(){
                                com.usatech.usalive.pepsi.HardwareStatusResponseE wrappedElement = new com.usatech.usalive.pepsi.HardwareStatusResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.usatech.usalive.pepsi.HardwareUpdateResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.usatech.usalive.pepsi.HardwareUpdateResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.usatech.usalive.pepsi.HardwareUpdateResponseE wrapHardwareUpdate(){
                                com.usatech.usalive.pepsi.HardwareUpdateResponseE wrappedElement = new com.usatech.usalive.pepsi.HardwareUpdateResponseE();
                                return wrappedElement;
                         }
                    


        /**
        *  get the default envelope
        */
        private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory){
        return factory.getDefaultEnvelope();
        }


        private  java.lang.Object fromOM(
        org.apache.axiom.om.OMElement param,
        java.lang.Class type,
        java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault{

        try {
        
                if (com.usatech.usalive.pepsi.HardwareActivationE.class.equals(type)){
                
                        return com.usatech.usalive.pepsi.HardwareActivationE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (com.usatech.usalive.pepsi.HardwareActivationResponseE.class.equals(type)){
                
                        return com.usatech.usalive.pepsi.HardwareActivationResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (com.usatech.usalive.pepsi.HardwareDeactivationE.class.equals(type)){
                
                        return com.usatech.usalive.pepsi.HardwareDeactivationE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (com.usatech.usalive.pepsi.HardwareDeactivationResponseE.class.equals(type)){
                
                        return com.usatech.usalive.pepsi.HardwareDeactivationResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (com.usatech.usalive.pepsi.HardwareDeploymentE.class.equals(type)){
                
                        return com.usatech.usalive.pepsi.HardwareDeploymentE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (com.usatech.usalive.pepsi.HardwareDeploymentResponseE.class.equals(type)){
                
                        return com.usatech.usalive.pepsi.HardwareDeploymentResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (com.usatech.usalive.pepsi.HardwareStatusE.class.equals(type)){
                
                        return com.usatech.usalive.pepsi.HardwareStatusE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (com.usatech.usalive.pepsi.HardwareStatusResponseE.class.equals(type)){
                
                        return com.usatech.usalive.pepsi.HardwareStatusResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (com.usatech.usalive.pepsi.HardwareUninstallE.class.equals(type)){
                
                        return com.usatech.usalive.pepsi.HardwareUninstallE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (com.usatech.usalive.pepsi.HardwareUninstallResponseE.class.equals(type)){
                
                        return com.usatech.usalive.pepsi.HardwareUninstallResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (com.usatech.usalive.pepsi.HardwareUpdateE.class.equals(type)){
                
                        return com.usatech.usalive.pepsi.HardwareUpdateE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (com.usatech.usalive.pepsi.HardwareUpdateResponseE.class.equals(type)){
                
                        return com.usatech.usalive.pepsi.HardwareUpdateResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
        } catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
           return null;
        }



    

        /**
        *  A utility method that copies the namepaces from the SOAPEnvelope
        */
        private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env){
        java.util.Map returnMap = new java.util.HashMap();
        java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
        while (namespaceIterator.hasNext()) {
        org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
        returnMap.put(ns.getPrefix(),ns.getNamespaceURI());
        }
        return returnMap;
        }

        private org.apache.axis2.AxisFault createAxisFault(java.lang.Exception e) {
        org.apache.axis2.AxisFault f;
        Throwable cause = e.getCause();
        if (cause != null) {
            f = new org.apache.axis2.AxisFault(e.getMessage(), cause);
        } else {
            f = new org.apache.axis2.AxisFault(e.getMessage());
        }

        return f;
    }

        }//end of class
    