/*
 * Created on Jun 8, 2005
 *
 */
package com.usatech.usalive.hybrid;

import simple.falcon.servlet.FalconServlet;

import com.usatech.usalive.report.AsyncReportEngine;

public class HybridServlet extends FalconServlet {
    private static final long serialVersionUID = 239450245025815L;
	public static final String ATTRIBUTE_LINK_REQUIREMENT_ENGINE = "com.usatech.usalive.link.LinkRequirementEngine";
	public static final String ATTRIBUTE_ASYNCH_REPORT_ENGINE = "com.usatech.usalive.report.AsyncReportEngine";
	public static final String ATTRIBUTE_TRANSPORT_RESULT_CACHE = "com.usatech.usalive.report.TransportResultCache";
	
    public HybridServlet() {
        super();
        restrictedParameterNames.add("user");
        restrictedParameterNames.add(ATTRIBUTE_LINK_REQUIREMENT_ENGINE);
        restrictedParameterNames.add("masterUserId");
        restrictedParameterNames.add(ATTRIBUTE_ASYNCH_REPORT_ENGINE);
    }

	/**
	 * @see simple.servlet.SimpleServlet#getAppName()
	 */
	@Override
	protected String getAppName() {
		return "usalive";
	}

    @Override
    public void destroy() {
    	super.destroy();
    	AsyncReportEngine arEngine=(AsyncReportEngine)getServletContext().getAttribute(ATTRIBUTE_ASYNCH_REPORT_ENGINE);
		if(arEngine!=null){
			arEngine.destroy();
    		getServletContext().removeAttribute(ATTRIBUTE_ASYNCH_REPORT_ENGINE);
		}
		getServletContext().removeAttribute(ATTRIBUTE_LINK_REQUIREMENT_ENGINE);
    }
}
