# resolve links - $0 may be a softlink
PRG="$0"

while [ -h "$PRG" ]; do
  ls=`ls -ld "$PRG"`
  link=`expr "$ls" : '.*-> \(.*\)$'`
  if expr "$link" : '/.*' > /dev/null; then
    PRG="$link"
  else
    PRG=`dirname "$PRG"`/"$link"
  fi
done

# Get standard environment variables
PRGDIR=`dirname "$PRG"`

JAVA_ENDORSED_DIRS="$PRGDIR"/../endorsed
CATALINA_OPTS="-Duser.country=US -Djavax.xml.transform.TransformerFactory=com.jclark.xsl.trax.TransformerFactoryImpl -XX:MaxPermSize=96M -Xmx256m -Djavax.net.ssl.keyStore=/opt/USAT/conf/keystore.ks -Djavax.net.ssl.keyStorePassword=usatech -Djavax.net.ssl.trustStore=/opt/USAT/conf/truststore.ts -Dcom.sun.management.jmxremote -Djmx.remote.x.rmiRegistryPort=7801 -Djmx.remote.x.rmiServerPort=7802 -Djmx.remote.x.login.config=JMXControl -Djmx.remote.x.access.file=/opt/USAT/conf/jmx.access -Djava.security.auth.login.config=/opt/USAT/conf/jmx.login.config -javaagent:$PRGDIR/../lib/usat-jmx-agent-1.0.1.jar"
JAVA_HOME=/usr/jdk/jdk1.6.0_13
