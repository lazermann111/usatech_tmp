<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.usatech.usalive.servlet.USALiveUtils"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.RequestUtils"%>
<jsp:include page="include/header.jsp"/>
<%InputForm form = RequestUtils.getInputForm(request);%>
<span class="caption">Help</span>
<%
Results results = USALiveUtils.getLinkResults(form, "H");
while(results.next()) {
%>
	<table class="selectBody">
	<tr><td valign="top">
    <div class="selectSection">
	<div class="sectionTitle"><%=results.getFormattedValue("title")%></div>		
    <table class="input-rows">
        <tr><td><%=results.getFormattedValue("description")%></td></tr>		
        <tr><td>
		<iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" width="788.54" height="443" type="text/html" src="<%=results.getFormattedValue("href")%>?autoplay=0&fs=1&iv_load_policy=3&showinfo=0&rel=0&cc_load_policy=0&start=0&end=0"allowfullscreen></iframe>
		</td></tr>
    </table>
   	</div>
	</td></tr>
	</table>
	<div class="spacer10"></div>
<%}%>
<jsp:include page="include/footer.jsp"/>