<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="simple.io.Log"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.sql.SQLIntegrityConstraintViolationException"%>
<%@page import="simple.text.StringUtils"%>
<script type="text/javascript">
<%
	Log log = Log.getLog();
	UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
	boolean hasPri=false;
	if((user.isInternal()&&user.hasPrivilege(ReportingPrivilege.PRIV_CUSTOMER_SERVICE))||user.hasPrivilege(ReportingPrivilege.PRIV_RMA)){
		hasPri=true;
	}
	InputForm inputForm = RequestUtils.getInputForm(request);
	String actionType=inputForm.getString("actionType", true);
	int rmaShippingCarrierId=-1;
	int notAllowed=1;
	if(hasPri && "POST".equalsIgnoreCase(request.getMethod())){
		try{
			if(actionType.equals("add")){
				DataLayerMgr.executeCall("INSERT_RMA_SHIPPING_CARRIER", inputForm, true);
				rmaShippingCarrierId=inputForm.getInt("rmaShippingCarrierId", true, -1);
				%>editNewCarrierResult("add", "", <%=rmaShippingCarrierId%>, "<%=StringUtils.prepareScript(inputForm.getStringSafely("carrier", ""))%>", "<%=StringUtils.prepareScript(inputForm.getStringSafely("carrierAccountNumber", ""))%>");<%
			}else if (actionType.equals("delete")){
				DataLayerMgr.executeCall("CHECK_ALLOWED_DELETE_CARRIER", inputForm, true);
				notAllowed=inputForm.getInt("notAllowed", true, 1);
				if(notAllowed==1){
					%>editNewCarrierResult("error", "Shipping carrier account is being used by another RMA. Delete is not allowed.");</script><%
					return;
				}
				DataLayerMgr.executeCall("DELETE_RMA_SHIPPING_CARRIER", inputForm, true);
				%>editNewCarrierResult("delete", "");<%
			}
		}catch(SQLIntegrityConstraintViolationException e){
			%>editNewCarrierResult("error", "Unique shipping carrier account is required.");<%
			log.error("Failed to requests RMA for devicess. Please try again.", e);
		}catch(Exception e) {
			%>editNewCarrierResult("error", "Editing carrier account failed. Please retry or contact customer service for assistance.");<%
			log.error("Failed to requests editing RMA carrrier. Please try again.", e);
		}
	};
%>
</script>
