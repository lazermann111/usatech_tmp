<%@page import="com.usatech.report.ReportingUser.ActionType"%>
<%@page import="com.usatech.report.ReportingUser.ResourceType"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="simple.servlet.NotAuthorizedException"%>
<%@page import="com.usatech.usalive.web.PayorUtils"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
if(!user.hasPrivilege(ReportingPrivilege.PRIV_BACK_OFFICE))
    throw new NotAuthorizedException("You are not authorized to perform this function.");
if("POST".equalsIgnoreCase(request.getMethod())) {
	long payorId = RequestUtils.getAttribute(request, "payorId", Long.class, true);
	user.checkPermission(ResourceType.PAYOR, ActionType.DELETE, payorId);
	PayorUtils.removePayor(user, payorId, RequestUtils.getOrCreateMessagesSource(request));
}
RequestUtils.redirectWithCarryOver(request, response, "payor_config.html");
%>