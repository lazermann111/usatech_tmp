<%@page import="java.sql.SQLIntegrityConstraintViolationException"%>
<%@page import="com.usatech.report.ReportingUser.ActionType"%>
<%@page import="com.usatech.report.ReportingUser.ResourceType"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="simple.xml.sax.MessagesInputSource"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.servlet.RequestUtils"%>
<%
long regionId = RequestUtils.getAttribute(request, "regionId", long.class, true);
UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
if("POST".equalsIgnoreCase(request.getMethod())) {
	user.checkPermission(ResourceType.REGION, ActionType.EDIT, regionId);
    MessagesInputSource messages = RequestUtils.getOrCreateMessagesSource(request);
	try {
		DataLayerMgr.executeUpdate("DELETE_REGION", RequestUtils.getInputForm(request), true);
		messages.addMessage("success", "region.delete.success", "Region was deleted");
	} catch(SQLIntegrityConstraintViolationException e) {
		messages.addMessage("warn", "region.delete.references", "Region has devices assigned to it. Please reassign these devices first or merge regions");
    }
}
RequestUtils.redirectWithCarryOver(request, response, "region_admin.html?regionId=" + regionId);
%>