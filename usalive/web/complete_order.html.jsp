<%@page import="com.usatech.usalive.servlet.USALiveUtils"%>
<%@page import="simple.servlet.ResponseUtils"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="com.usatech.usalive.web.OrderUtils"%>
<%@page import="simple.servlet.GenerateUtils"%>
<%@page import="java.util.Calendar"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.text.StringUtils"%>

<%
InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
UsaliveUser user = (UsaliveUser) RequestUtils.getUser(request);
if (user.getCustomerId() < 1)
	response.sendRedirect("/home.i");
else {
if ("POST".equalsIgnoreCase(request.getMethod()))
	OrderUtils.processCompleteOrder(inputForm, request, response);
String email;
if (user.getMasterUser() == null)
	email = user.getEmail();
else
	email = user.getMasterUser().getEmail();
%>
<jsp:include page="include/header.jsp"/>
<span class="caption">Complete Order</span>
<form autocomplete="off" method="post" action="complete_order.html" onsubmit="processInput(); return validateUSATForm(this);">
<%
ResponseUtils.writeXsfrFormProtection(pageContext);
USALiveUtils.writeFormProfileId(pageContext);
String successMessage = inputForm.getString("successMessage", false);
if (!StringUtils.isBlank(successMessage)) {%>
<div class="message-success"><%=StringUtils.prepareHTML(successMessage)%></div>
<div class="spacer5"></div>
<%
} else {
	String errorMessage = inputForm.getString("errorMessage", false);
	if (!StringUtils.isBlank(errorMessage)) {%>
	<div class="message-error"><%=StringUtils.prepareHTML(errorMessage)%></div>
	<div class="spacer5"></div>
	<%}%>
<%}%>
<p>
This section is used for paying for product which is referenced by a USA Technologies order number. The funds from this transaction will be paid to USA Technologies against the order number you enter below. Our system will verify the order number before payment will be accepted. 
</p>
<p>If you want to enter a transaction for services or product you have sold your customer, please use ‘ePort Online’. If it is not available on your USALive account, please contact USA Technologies to have this feature activated.  
<p>If you have any questions please don’t hesitate to call 888-561-4748 and select option 1 when prompted.</p>    
<table id="completeOrder">
	<tr class="tableHeader">
		<th colspan="2"><div align="center">Complete Order</div></th>
	</tr>
    <tr>
        <td width="50%">Order Number<span class="required">*</span></td>
        <td width="50%"><input type="text" name="order_number" valid="^\d+$" value="<%=StringUtils.prepareCDATA(inputForm.getStringSafely("order_number", ""))%>" label="Order Number" placeholder="Enter order number" title="Enter the order number you received from USA Technologies" maxlength="20" usatRequired="true" /></td>
    </tr>
    <tr>
        <td>Total Amount<span class="required">*</span></td>
        <td><input type="text" name="total_amount" value="<%=StringUtils.prepareCDATA(inputForm.getStringSafely("total_amount", ""))%>" label="Total Amount" placeholder="Enter total order amount" title="Enter total order amount you received from USA Technologies" valid="^[0-9]+(?:\.[0-9]{1,2})?$" maxlength="10" usatRequired="true" minValue="1" maxValue="99999.99" /></td>
    </tr>
    <tr>
        <td>Credit Card Number<span class="required">*</span></td>
        <td><input type="text" name="card_number" label="Credit Card Number" placeholder="Enter card number" title="Enter your credit card number" valid="^[0-9]{15,19}$" maxlength="19" usatRequired="true" autocomplete="off" value="<%=StringUtils.prepareCDATA(inputForm.getStringSafely("card_number", ""))%>"/></td>
    </tr>
    <tr>
    	<td>Expiration Date<span class="required">*</span></td>
    	<td>
    		<select name="exp_month" label="Expiration Month" title="Enter your credit card expiration month" usatRequired="true">
    			<option value="">Select month</option>
    		<%
    		Integer selectedMonth = RequestUtils.getAttribute(request, "exp_month", Integer.class, false);
    		for(int m = 1; m <= 12; m++) {
    			String month = StringUtils.pad(Integer.toString(m), '0', 2, StringUtils.Justification.RIGHT);
    		%>    			
			    <option value="<%=month%>"<%if(selectedMonth != null && selectedMonth.intValue() == m) { %> selected="selected"<%} %>><%=month%></option>
			<%}%>
			</select>
			<select name="exp_year" label="Expiration Year" title="Enter your credit card expiration year" usatRequired="true">
				<option value="">Select year</option>
			<%
			Calendar now = Calendar.getInstance();
			Integer selectedYear = RequestUtils.getAttribute(request, "exp_year", Integer.class, false);
            int currYear = now.get(Calendar.YEAR);
			for(int y = currYear; y <= currYear + 10; y++) {
			%>
    			<option value="<%=y%>"<%if(selectedYear != null && selectedYear.intValue() == y) { %> selected="selected"<%} %>><%=y%></option>
    		<%}%>
    		</select>
    	</td>
    </tr>
    <tr>
        <td>Security Code<span class="required">*</span></td>
        <td><input type="password" name="security_code" label="Security Code" placeholder="Enter 3 or 4 digit code" title="The 3 or 4 digit security code found on the back or front of your credit card" maxlength="4" valid="^[0-9]{3,4}$" usatRequired="true"  value="<%=StringUtils.prepareCDATA(inputForm.getStringSafely("security_code", ""))%>"/></td>
    </tr>
    <tr>
    	<td>Billing Country<span class="required">*</span></td>
    	<td>
    		<select name="country" label="Billing Country" title="Enter the country of your credit card billing address" onchange="countryChange(this)" usatRequired="true">
		    	<option value="">Select your country</option>
		    <%for(GenerateUtils.Country c : GenerateUtils.getCountries()) {%>
		        <option value="<%=StringUtils.prepareCDATA(c.code)%>"<%if (inputForm.getStringSafely("country", "").equalsIgnoreCase(c.code)) {%> selected="selected"<%}%> usatValid="<%=StringUtils.prepareScript(c.postalRegex)%>" usatLabel="<%=("US".equalsIgnoreCase(c.code) ? "Zip Code" : "Postal Code")%>"><%=StringUtils.prepareHTML(c.name)%></option>
		    <%}%>
     		</select>
    	</td>
    </tr>
    <tr>
    	<td>Billing Address<span class="required">*</span></td>
    	<td><input type="text" name="address1" value="<%=StringUtils.prepareCDATA(inputForm.getStringSafely("address1", ""))%>" label="Billing Address" placeholder="Enter your street address" title="Enter the street address of your credit card billing address" maxlength="50" usatRequired="true" /></td>
    </tr>
    <tr>
    	<td>Billing <span id="postalCodeLabel">Zip Code</span><span class="required">*</span></td>
    	<td><input type="text" name="postal" value="<%=StringUtils.prepareCDATA(inputForm.getStringSafely("postal", ""))%>" label="Billing Zip Code" placeholder="Enter your zip code" maxlength="20" usatRequired="true" title="Enter the zip code of your credit card billing address" /></td>
    </tr>
    <tr>
    	<td>Email Receipt To (comma-separated list)</td>
    	<td><input type="text" name="email_to" id="email_to" value="<%=StringUtils.prepareCDATA(inputForm.getStringSafely("email_to", email))%>" label="Email Receipt To" placeholder="Comma-separated list of email addresses" maxlength="4000" title="Comma-separated list of email addresses to email receipt to" valid="^([\w+-.%]+@[\w-.]+\.[A-Za-z]{2,4},?)+$" /></td>
    </tr>
    <tr>
    	<td colspan="2"><span class="required">*</span> Required field</td>
    </tr>
    <tr>
    	<td colspan="2" align="center">
    		<input type="submit" class="cssButton" name="action" value="Complete Order" />
    	</td>
    </tr>
</table>

<div class="spacer5"></div>

</form>

<script type="text/javascript">
function countryChange(country) {
	var selectedCountry = country.options[country.selectedIndex];
	var postalElem = country.form.postal;
	var label = selectedCountry.getAttribute("usatLabel");
	document.getElementById("postalCodeLabel").innerHTML = label;
	postalElem.setAttribute("label", "Billing " + label);
	postalElem.setAttribute("placeholder", "Enter your " + label.toLowerCase());
	postalElem.setAttribute("title", "Enter the " + label.toLowerCase() + " of your credit card billing address");
	postalElem.setAttribute("valid", selectedCountry.getAttribute("usatValid"));
}
function processInput() {
	document.getElementById("email_to").value = document.getElementById("email_to").value.replace(/\s/g, "");
}
</script>

<jsp:include page="include/footer.jsp"/>
<%}%>