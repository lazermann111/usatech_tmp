<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.servlet.RequestUtils,simple.results.Results"%>
<%! protected int maxFileSizeToDisplay=20480; %>
<%
Integer dexFileId=RequestUtils.getAttribute(request, "dexFileId", Integer.class, true);
InputForm form = RequestUtils.getInputForm(request);
DataLayerMgr.executeCall("VALIDATE_GET_DEX_FILE", form, true);
if(!form.getBoolean("canViewDexFile", false, false)) {
	String msg = RequestUtils.getTranslator(request).translate("dex-invalid-profile");%>
	<jsp:include page="include/header.jsp"/>
	<p class="errorText"><%=StringUtils.prepareHTML(msg) %></p>
	<jsp:include page="include/footer.jsp"/><%
	return;
}
Results results = DataLayerMgr.executeQuery("GET_DEX_FILE", form);
if(results.next()) {
	String fileName = results.getValue("dexFileName", String.class);
    String fileContent = new String (results.getValue("dexFileContent", byte[].class));
%>
<jsp:include page="include/header.jsp"/>
<table class="folio">
	<tr class="headerRow"><td>FileName:<%=StringUtils.prepareHTML(fileName)%></td></tr><%
	if(fileContent == null) {
	   %><tr><td>Dex file has expired. Content not available</td></tr><%
	} else if(fileContent.length() < maxFileSizeToDisplay) {
	   %> <tr><td><pre><%=StringUtils.prepareHTML(fileContent)%></pre><a href="./get_dex_file_complete.i?dexFileId=<%=dexFileId%>"> Download the file here</a></td></tr><%
	} else {
	   %><tr><td><pre><%=StringUtils.prepareHTML(fileContent.substring(0,maxFileSizeToDisplay-1))%>...</pre></td></tr>
	   <tr><td>Only displaying <%=maxFileSizeToDisplay%> characters.<a href="./get_dex_file_complete.i?dexFileId=<%=dexFileId%>"> Download the complete file here</a></td></tr><%
	}%>
</table>    	
<%}else{%>
	<p>The requested Dex file does not exist.</p>
<%}%>
<jsp:include page="include/footer.jsp"/>