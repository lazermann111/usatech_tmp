<%@page import="java.util.Date"%>
<%@page import="simple.servlet.GenerateUtils"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="java.util.Locale"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="java.util.regex.Matcher"%>
<%@page import="java.util.regex.Pattern"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%! protected final Pattern PARSE_BASE_URL_PATTERN = Pattern.compile("(\\w+://)?([^/:]+)(:\\w+)?(/.*)?"); %>
<% 
Locale locale = RequestUtils.getLocale(request);
String linkUrl = RequestUtils.getAttribute(request, "linkUrl", String.class, false);
String linkText;
if(StringUtils.isBlank(linkUrl)) {
	linkText = null;
} else {
	Matcher matcher = PARSE_BASE_URL_PATTERN.matcher(linkUrl);
	if(matcher.matches())
	    linkText = matcher.group(2);
	else
	    linkText = linkUrl;
}
String ref = RequestUtils.getAttribute(request, "chargeReference", String.class, false);
String desc = RequestUtils.getAttribute(request, "chargeDesc", String.class, false);
if(StringUtils.isBlank(desc)) 
	desc = "Online Charge";
Date chargeTime = RequestUtils.getAttribute(request, "chargeTime", Date.class, true);
Date invDate = RequestUtils.getAttribute(request, "invoiceDate", Date.class, false);
Boolean fragment = RequestUtils.getAttribute(request, "fragment", Boolean.class, false);
String company = RequestUtils.getAttribute(request, "company", String.class, false);
String logoUrl = RequestUtils.getAttribute(request, "logoUrl", String.class, false);
if(fragment == null || !fragment.booleanValue()) { %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<title></title>
<style type="text/css">
p { margin: 0 0 0 0;}
a {
  color:#0088cc;
  text-decoration: none;
  }
a:hover, a:focus {
  color: #005580;
  text-decoration: underline !important;
}
</style>
<meta name="robots" content="noindex,nofollow"></meta>
</head>
<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="width: 100%; margin: 0; padding: 0; text-align: center;"><%} %>
<div style='font-family: "Helvetica Neue", Helvetica, Arial, sans-serif; font-size: 18px; -webkit-text-size-adjust: none; -ms-text-size-adjust: none; color: #333333; text-align: center;'>
<!-- VENDOR LOGO AND INFO TABLE  -->
<table width="512" cellpadding="0" cellspacing="0" border="0"><tr><td align="center"><br/>
<table width="85%" cellpadding="0" cellspacing="0" border="0" style="border:3px double #cccccc; color: #333333;">
<tr><th style='font-family: "Helvetica Neue", Helvetica, Arial, sans-serif; font-size: 18px; padding: 5px 10px;'><%=StringUtils.prepareHTML(company) %></th></tr>
<%
String address = RequestUtils.getAttribute(request, "address", String.class, false);
String city = RequestUtils.getAttribute(request, "city", String.class, false);
String stateCd = RequestUtils.getAttribute(request, "stateCd", String.class, false);
String postalCd = RequestUtils.getAttribute(request, "postalCd", String.class, false);
String countryCd = RequestUtils.getAttribute(request, "countryCd", String.class, false);
String phone = RequestUtils.getAttribute(request, "phone", String.class, false);
String email = RequestUtils.getAttribute(request, "email", String.class, false);        
if(!StringUtils.isBlank(city) && !StringUtils.isBlank(stateCd) && !StringUtils.isBlank(postalCd)) {
	if(!StringUtils.isBlank(address)) {%><tr><td align="center"><%=StringUtils.prepareHTML(address) %></td></tr><%}
	%><tr><td align="center"><%=StringUtils.prepareHTML(city) %>, <%=StringUtils.prepareHTML(stateCd) %> <%=StringUtils.prepareHTML(postalCd) %></td></tr><%
}
if(!StringUtils.isBlank(phone)) {%><tr><td align="center"><%=StringUtils.prepareHTML(phone) %></td></tr><%}
if(!StringUtils.isBlank(email)) {%><tr><td align="center"><a href="mailto:<%=StringUtils.prepareCDATA(email) %>"><%=StringUtils.prepareHTML(email) %></a></td></tr><%}
%></table><br/></td></tr></table><%
if(!StringUtils.isBlank(logoUrl) || !StringUtils.isBlank(linkUrl)) {%>
<table width="512" cellpadding="0" cellspacing="0" border="0" style="margin: 10px 0;">
<tr>
<td style="border-collapse: collapse;" align="center"><%if(!StringUtils.isBlank(logoUrl)) {%>
<p><img onerror='this.style.display = "none"' src="<%=StringUtils.prepareCDATA(logoUrl)%>" style="height: auto; line-height: 100%; outline: none; text-decoration: none; display: block;"/></p>
<p></p><%}
if(!StringUtils.isBlank(linkUrl)) {%>
<p><a href="<%=StringUtils.prepareCDATA(linkUrl)%>"><%=StringUtils.prepareHTML(linkText)%></a><p>
<p></p><%} %>
</td>
</tr>
</table><%} %>
<!-- TOTAL CHARGE AND DATE AND TIME TABLE  -->
<table width="512" cellpadding="0" cellspacing="0" border="0">
<tr>
    <td style="border-collapse: collapse;" align="center">
        <p style="font-size: 36px; color: #B22222; font-weight: bold; margin: 20px 0 5px 0;">
        </p>
    </td>
</tr>
<tr>
<td style="border-collapse: collapse;" align="center">
<p style="font-size: 56px; line-height: 1em; color: #000000; font-weight: bold; margin-bottom: 5px;"><%=StringUtils.prepareHTML(ConvertUtils.formatObject(RequestUtils.getAttribute(request, "chargeAmount", true), "CURRENCY", locale)) %></p>
<p style="margin: 0 0 20px 0;">
<%=StringUtils.prepareHTML(ConvertUtils.formatObject(chargeTime, "DATE:EEE, MMM d, yyyy 'at' h:mm:ss a", locale)) %>
<br />Sale Description: <%=StringUtils.prepareHTML(desc) %></p>
<hr style="display: block; height: 1px; border: 0; border-top: 1px solid #cccccc; margin: 1em 0; padding: 0; width: 85%;"/>
</td>
</tr>
</table>
<!-- RECEIPT BREAKDOWN TABLE  -->
<table width="512" cellpadding="0" cellspacing="0" border="0">
<tr>
<td align="center">
<b>Card used:</b> <%=StringUtils.prepareHTML(RequestUtils.getAttribute(request, "cardType", String.class, true)) %> <%=StringUtils.prepareHTML(RequestUtils.getAttribute(request, "cardNumber", String.class, true)) %><br/>&#160;
</td>
</tr>
</table>
<!-- CREDIT CARD INFO TABLE  -->
<table width="512" cellpadding="0" cellspacing="0" border="0" bgcolor="#f4f8fc">
<tr>
<td style="border-collapse: collapse;" align="center">
<p style="line-height: 20px;">&#160;</p>
<table width="85%" cellpadding="3" cellspacing="0" border="0" style="border: solid 1px #84929f; margin: 0 0 10px 0;">
<tr bgcolor="#84929f" style="font-size: 14px; font-weight: bold; color: #ffffff;">
<td style="border-collapse: collapse;" align="left">Date</td>
<td style="border-collapse: collapse;" align="left">Description</td>
<td style="border-collapse: collapse;" align="right">Amount</td>
</tr>
<tr style="font-size: 14px;">
<td style="border-collapse: collapse;" align="left"><%=StringUtils.prepareHTML(ConvertUtils.formatObject(invDate != null ? invDate : chargeTime, "DATE:MM/dd/yyyy", locale)) %></td>
<td style="border-collapse: collapse;" align="left"><%=StringUtils.prepareHTML(desc) %></td>
<td style="border-collapse: collapse;" align="right"><%=StringUtils.prepareHTML(ConvertUtils.formatObject(RequestUtils.getAttribute(request, "chargeAmount", true), "CURRENCY", locale)) %></td>
</tr>
</table>
<table width="85%" cellpadding="4" cellspacing="0" border="0" bgcolor="#ffffff" style="border-bottom: dotted 2px #dddddd;"><%
if(!StringUtils.isBlank(ref)) {%>
<tr style="border-collapse: collapse; font-size: 14px;" align="left">
<td>
<p style="margin: 10px 0 10px 0;"><b>Invoice/Reference:</b> <%=StringUtils.prepareHTML(ref) %> </p>
</td>
</tr><%}%>
<tr style="border-collapse: collapse; font-size: 14px;" align="left">
<td>
<p style="margin: 0 0 10px 0;"><b>Transaction ID:</b> <%=RequestUtils.getAttribute(request, "deviceTranCd", Long.class, true)%></p>
</td>
</tr>
<tr style="border-collapse: collapse; font-size: 14px;" align="left">
<td>
<p style="margin: 0 0 10px 0;"><b>ePort Terminal:</b> <%=StringUtils.prepareHTML(RequestUtils.getAttribute(request, "deviceSerialCd", String.class, true)) %></p>
</td>
</tr>
</table>
<tr>
<td align="center"><br/>
<table width="85%" cellpadding="0" cellspacing="0" border="0">
<tr>
<td style="border-collapse: collapse; font-size: 13px;" align="left">
<p style="margin: 0 0 10px 0;">Online Transaction charged <%
if(!StringUtils.isBlank(company)) {%>by <%=StringUtils.prepareHTML(company)%><%} %> via the ePort Online payment portal by USA Technologies, Inc. <em>The market leader in forward-thinking cashless payment, telemetry &amp; customer engagement services.</em><br /></p>
<p style="margin: 0 0 10px 0;">
<b>USA Technologies</b><br />
100 Deerfield Lane, Suite 300<br />
Malvern, PA 19355<br />
<a href="http://www.usatech.com">www.usatech.com</a></p>
<p style="margin: 0 0 10px 0;">
For questions about this transaction, please call Customer Service at 888-561-4748 or email: <a href="mailto:support@usatech.com">support@usatech.com</a></p>
<p style="margin: 0 0 25px 0;">&copy; <%=GenerateUtils.getCurrentYear() %>, All Rights Reserved</p>
</td>
</tr>
</table>
<td>
</tr>
</table>
</div><%
if(fragment == null || !fragment.booleanValue()) {%>
</body>
</html>
<%}%>