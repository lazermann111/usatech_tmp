<%@page import="simple.servlet.GenerateUtils"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="java.util.Locale"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="java.util.regex.Matcher"%>
<%@page import="java.util.regex.Pattern"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%! protected final Pattern PARSE_BASE_URL_PATTERN = Pattern.compile("(\\w+://)?([^/:]+)(:\\w+)?(/.*)?"); %>
<% 
Locale locale = RequestUtils.getLocale(request);
String linkUrl = RequestUtils.getAttribute(request, "linkUrl", String.class, true);
Matcher matcher = PARSE_BASE_URL_PATTERN.matcher(linkUrl);
String linkText;
if(matcher.matches())
    linkText = matcher.group(2);
else
    linkText = linkUrl;
String ref = RequestUtils.getAttribute(request, "reference", String.class, false);
String desc = RequestUtils.getAttribute(request, "description", String.class, false);
if(StringUtils.isBlank(desc)) 
	desc = "ePort Mobile Charge";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<title></title>
<style type="text/css">
body {
  width: 100% !important;
  font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 18px;
  -webkit-text-size-adjust: none;
  -ms-text-size-adjust: none;
  margin: 0;
  padding: 0;
  text-align: center;
  color: #333333;
  }
img {
  height: auto;
  line-height: 100%;
  outline: none;
  text-decoration: none;
  display: block;
  }
p { margin: 0 0 0 0;}
hr {
    display: block;
    height: 1px;
    border: 0;
    border-top: 1px solid #cccccc;
    margin: 1em 0;
    padding: 0;
    width: 85%;
}
a {
  color:#0088cc;
  text-decoration: none;
  }
a:hover, a:focus {
  color: #005580;
  text-decoration: underline !important;
}
</style>
<meta name="robots" content="noindex,nofollow"></meta>
</head>
<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<!-- VENDOR LOGO AND INFO TABLE  -->
<table width="512" cellpadding="0" cellspacing="0" border="0" style="margin: 20px 0 0 0;">
<tr>
<td style="border-collapse: collapse;" align="center">
<p><img onerror='this.style.display = "none"' src="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "logoUrl", String.class, true))%>"/></p>
<p></p>
<p><a href="<%=StringUtils.prepareCDATA(linkUrl)%>"><%=StringUtils.prepareHTML(linkText)%></a><p>
<p></p>
</td>
</tr>
</table>
<!-- TOTAL CHARGE AND DATE AND TIME TABLE  -->
<table width="512" cellpadding="0" cellspacing="0" border="0">
<tr>
    <td style="border-collapse: collapse;" align="center">
        <p style="font-size: 36px; color: #B22222; font-weight: bold; margin: 20px 0 5px 0;">
        </p>
    </td>
</tr>
<tr>
<td style="border-collapse: collapse;" align="center">
<p style="font-size: 56px; color: #000000; font-weight: bold; margin: 20px 0 5px 0;"><%=StringUtils.prepareHTML(ConvertUtils.formatObject(RequestUtils.getAttribute(request, "amount", true), "CURRENCY", locale)) %></p>
<p style="margin: 0 0 20px 0;">
<%=StringUtils.prepareHTML(ConvertUtils.formatObject(RequestUtils.getAttribute(request, "date", true), "DATE:EEE, MMM d, yyyy 'at' h:m:s A", locale)) %>
<br />Sale Description: <%=StringUtils.prepareHTML(desc) %></p>
<hr />
</td>
</tr>
</table>
<!-- RECEIPT BREAKDOWN TABLE  -->
<table width="512" cellpadding="0" cellspacing="0" border="0" style="margin: 0 0 20px 0;">
<tr>
<td align="center">
<p style="margin: 0 0 3px 0;">
<b>Card used:</b> <%=StringUtils.prepareHTML(RequestUtils.getAttribute(request, "cardType", String.class, true)) %> <%=StringUtils.prepareHTML(RequestUtils.getAttribute(request, "cardNumber", String.class, true)) %></p>
</td>
</tr>
<!-- 
<tr>
<td style="border-collapse: collapse; width: 85%px;" align="center">
<table width="85%" cellpadding="4" cellspacing="0" border="0" bgcolor="#ffffff" style="border-top: solid 2px #dddddd;">
    <tr>
        <td style="border-collapse: collapse;" align="left">
            Initial Amount:</td>
        <td style="border-collapse: collapse;" align="right">
            $16.00</td>
    </tr>
    <tr>
        <td style="border-collapse: collapse;" align="left">
            - Discount:</td>
        <td style="border-collapse: collapse;" align="right">
            ($0.00)</td>
    </tr>
<tr>
<td style="border-collapse: collapse;" align="left">
Charge:</td>
<td style="border-collapse: collapse; border-top: dotted 1px #888888;" align="right">
$16.00</td>
</tr>
<tr>
<td style="border-collapse: collapse;" align="left">
+ Tax:</td>
<td style="border-collapse: collapse;" align="right">
$0.96</td>
</tr>
<tr>
    <td style="border-collapse: collapse;" align="left">
        </td>
    <td style="border-collapse: collapse;" align="right">
        </td>
</tr>
<tr>
<td style="border-collapse: collapse;" align="left">
Subtotal:</td>
<td style="border-collapse: collapse; border-top: dotted 1px #888888;" align="right">
$16.96</td>
</tr>
<tr>
<td style="border-collapse: collapse;" align="left">
+ Tip:</td>
<td style="border-collapse: collapse;" align="right">
$0.00</td>
</tr>
<tr bgcolor="#f3f3f3">
<td style="border-collapse: collapse;" align="left">
<b>TOTAL</b></td>
<td style="border-collapse: collapse;  border-top: dotted 1px #888888;" align="right">
<b>$16.96</b></td>
</tr>
</table>
</td>
</tr> -->
</table>
<!-- CREDIT CARD INFO TABLE  -->
<table width="512" cellpadding="0" cellspacing="0" border="0" bgcolor="#f4f8fc">
<tr>
<td style="border-collapse: collapse;" align="center">
<p style="margin: 20px 0 5px 0; font-size: 14px;">
</p>
<table width="85%" cellpadding="3" cellspacing="0" border="0" style="border: solid 1px #84929f; margin: 0 0 10px 0;">
<tr bgcolor="#84929f" style="font-size: 14px; font-weight: bold; color: #ffffff;">
<td style="border-collapse: collapse;" align="left">Date</td>
<td style="border-collapse: collapse;" align="left">Description</td>
<td style="border-collapse: collapse;" align="right">Amount</td>
</tr>
<tr style="font-size: 14px;">
<td style="border-collapse: collapse;" align="left"><%=StringUtils.prepareHTML(ConvertUtils.formatObject(RequestUtils.getAttribute(request, "date", true), "DATE:MM/dd/yyyy", locale)) %></td>
<td style="border-collapse: collapse;" align="left"><%=StringUtils.prepareHTML(desc) %></td>
<td style="border-collapse: collapse;" align="right"><%=StringUtils.prepareHTML(ConvertUtils.formatObject(RequestUtils.getAttribute(request, "amount", true), "CURRENCY", locale)) %></td>
</tr>
</table>
<table width="85%" cellpadding="4" cellspacing="0" border="0" bgcolor="#ffffff" style="border-bottom: dotted 2px #dddddd;"><%
if(!StringUtils.isBlank(ref)) {%>
<tr style="border-collapse: collapse; font-size: 14px;" align="left">
<td>
<p style="margin: 10px 0 10px 0;"><b>Invoice/Reference:</b> <%=StringUtils.prepareHTML(ref) %> </p>
</td>
</tr><%} %>
<tr style="border-collapse: collapse; font-size: 14px;" align="left">
<td>
<p style="margin: 0 0 10px 0;"><b>Transaction ID:</b> <%=RequestUtils.getAttribute(request, "tranId", Long.class, true)%></p>
</td>
</tr>
<tr style="border-collapse: collapse; font-size: 13px;" align="left">
<td>
<p style="margin: 0 0 10px 0;"><b>ePort Terminal:</b> <%=StringUtils.prepareHTML(RequestUtils.getAttribute(request, "deviceSerialCd", String.class, true)) %></p>
</td>
</tr>
</table>
<tr>
<td><br/></td>
</tr>
<tr>
<td align="center">
<table width="85%" cellpadding="0" cellspacing="0" border="0">
<tr>
<td style="border-collapse: collapse; font-size: 13px;" align="left">
<p style="margin: 0 0 10px 0;">Mobile Transaction generated by USA Technologies, Inc. <em>The market leader in forward-thinking cashless payment, telemetry &amp; customer engagement services.</em><br /></p>
<p style="margin: 0 0 10px 0;">
<b>USA Technologies</b><br />
100 Deerfield Lane, Suite 300<br />
Malvern, PA 19355<br />
<a href="http://www.usatech.com">www.usatech.com</a></p>
<p style="margin: 0 0 10px 0;">
For questions about this transaction, please call Customer Service at 888-561-4748 or email: <a href="mailto:support@usatech.com">support@usatech.com</a></p>
<p style="margin: 0 0 25px 0;">&copy; <%=GenerateUtils.getCurrentYear() %>, All Rights Reserved</p>
</td>
</tr>
</table>
<td>
</tr>
</table>
</body>
</html>
