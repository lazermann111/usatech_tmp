<%@page import="simple.text.StringUtils"%>
<%@page import="com.usatech.usalive.web.OfferUtils"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<jsp:include page="include/header.jsp"/>

<%
InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
OfferUtils.processOfferSignup(inputForm, request, response);
String message = inputForm.getStringSafely("message", "");
%>

<style>	
	html { font-size: 62.5%;}
	
	img, embed, object, video {max-width: 100%;}
	
	.eoWrapper {
	  text-align: left;	  
	  margin: 5px auto 30px;
	  background-color: #ffffff;
	}
	
	.eoWrapper p {
	  margin: 10px 6%;
	}
	
	.eoWrapper ul {
	  margin: 10px 6%;
	}
	
	#eoMain { 
	  width: 90%;
	  width: 820px;
	  padding-bottom: 10px;
	  margin: 0;
	  font-family: Helvetica, 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans',  Arial, Verdana, sans-serif;
	  font-size: 16px;
	  font-size: 1.6rem;  /* 16px */
	  line-height: 1.5; /* 24px */
	  font-weight: 300;
	  color: #333333;
	}
	
	.offerHeader {
	  padding: 5px 6% 0;
	}
	
	#enroll-form {
	  background:#ffffff;	  
	  margin: 0;
	}
	
	#enroll-form input[type="button"], input[type="submit"] {
	font:400 16px/16px Helvetica, Arial, sans-serif;
	cursor:pointer;
	width:40%;
	border:none;
	background:#ef7f13;
	background-image:linear-gradient(bottom, #ef7f13 0%, #fab228 52%);
	background-image:-moz-linear-gradient(bottom, #ef7f13 0%, #fab228 52%);
	background-image:-webkit-linear-gradient(bottom, #ef7f13 0%, #fab228 52%);
	color:#222222;
	font-weight: 700;	
	padding:10px;
	border-radius:5px;
	}
	#enroll-form input[type="button"]:hover, input[type="submit"]:hover {
	background-image:linear-gradient(bottom, #d87312 0%, #f4ab20 52%);
	background-image:-moz-linear-gradient(bottom, #d87312 0%, #f4ab20 52%);
	background-image:-webkit-linear-gradient(bottom, #d87312 0%, #f4ab20 52%);
	-webkit-transition:background 0.3s ease-in-out;
	-moz-transition:background 0.3s ease-in-out;
	transition:background-color 0.3s ease-in-out;
	}
	#enroll-form input[type="button"]:hover, input[type="submit"]:active {
	box-shadow:inset 0 1px 3px rgba(0,0,0,0.5);
	}
	
	.content {
		font-family: Helvetica, 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans',  Arial, Verdana, sans-serif;
		font-size:16px !important;
		line-height:140% !important;
	}
		
	 @media only screen and (max-width: 35em) {
   	/* Style adjustments for viewports that meet the condition */

	.wrap, main {
		width: 98%;
	}
</style>

<%if (StringUtils.isBlank(message)) {%>

<div class="spacer10"></div>

<div class="eoWrapper">

	<div id="eoMain">
		<h2 class="offerHeader">Whether you are in the office or on the road, accept payment anytime, anywhere.</h2>
				
		<form id="enroll-form" method="post" action="eport_online_eport_mobile_offer.html">
	   	<input type="hidden" name="offer_id" value="5" />
	   	<input type="hidden" name="signup" value="Y" />
	   	<input type="hidden" name="priv_id" value="26" />

		<table class="content">
		<tr>
		  <td align="center">          
          <img src="/images/eport-online300.png" alt="ePort Online" />
          </td>
          <td>
          <img src="/images/eport-mobile300.png" alt="ePort Mobile" />
          </td>
        </tr>
        <tr>
          <td>
          	<p style="margin-top: 0;">Use your computer or tablet to process credit/debit and simplify billing for things like catering and OCS.</p>
	          <p style="color: #EB4102; font-weight: bold;">Special offer through 9/30/14.</p>
			  <ul>
	          <li>No activation fees*</li>
	          <li>No monthly service fees</li>
	          </ul>
	          
	          <p style="text-decoration: underline; margin-bottom: 0;">Benefits include:</p>
	          <ul style="margin-top: 0;">
	          <li>Simple blended rates</li>
	          <li>Cloud based payment/reporting through your existing USALive account</li>
	          <li>Set up recurring payments (coming soon)</li>
	          <li>Keep credit/debit on file for regular customers</li>
	          <li>Keep online payments integrated with the rest of your business</li>
	          </ul>                    				          
          </td>
          
          <td>          
	          <p style="margin-top: 0;">Use your smartphone to accept credit/debit at the point of service anytime, anywhere.</p>
	
			<p style="color: #EB4102; font-weight: bold;">
			Special offer through 9/30/14.
			</p>

			<ul>
			  <li>No monthly fees through 12/31/14</li>
	          <li>No activation fees**</li>
	          <li>Free shipping</li>
	          <li>Free reader</li>
	         </ul>
	          
	          <p style="text-decoration: underline; margin-bottom: 0;">Benefits include:</p>
	          <ul style="margin-top: 0;">
	          <li>Simple blended rates</li>          
	          <li>Cloud based payment/reporting through your existing USALive account</li>
	          <li>All-in-one package available</li>
	          <li>Keep payments integrated with the rest of your business</li>
			  </ul>
          </td>          
       </tr>
       <tr>
       	<td align="center"><input name="submit" type="submit" value="ENROLL ME NOW***" /></td>
       	<td align="center"><input type="button" onclick="window.location = 'eport_mobile_signup.html'" value="SIGN ME UP" /></td>
       </tr>
       <tr>          
          <td colspan="2">
          <p>Sign up for both today to be sure that you can accept payment no matter where or when you are doing business!</p>          
          </td>
       </tr>
       <tr>
         <td colspan="2" align="center">
       		<input type="button" onclick="window.location = 'eport_online_eport_mobile_signup.html'" value="I WANT BOTH!" />
       	 </td>
       	</tr>
       	<tr>
       	  <td colspan="2" align="center">
       		<p><a href="home.i">Remind me later</a> &nbsp;&nbsp;&nbsp; <a href="eport_online_signup.html?offer_id=4&signup=N">Don't show again</a></p>
       	  </td>
       	</tr>
       	<tr>
       		<td colspan="2">
       		<p>*ePort Online Enrollment after September 30, 2014 may be subject to a $10 activation fee per user.</p> 
			<p>**ePort Mobile Enrollment after September 30, 2014 may be subject to a $25 activation fee per user.</p> 
			<p>***If you are not a USALive administrator or authorized agent of your company, your enrollment may be processed pending approval.</p>
       	  </td>
       </tr>
       </table> 
       </form>
                 
         <div align="center">
         	<h2>One company, one point of contact, one call.</h2>
         </div>
         
         <div align="center">
         <a href="http://www.usatech.com"><img src="/images/logo_usatech.jpg" alt="USA Technologies" /></a>
         <p>
         800.633.0340  |  <a href="http://www.usatech.com" target="_blank">www.usatech.com</a>
         </div>
        
        </div> <!-- /wrapper -->       
	
	</div>  <!-- /main  -->

<%} else {%>
<%=message %>
<%} %>

<jsp:include page="include/footer.jsp"/>
