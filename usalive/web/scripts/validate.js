function validatePat (name, srcStr, regexPat, submitId) { 
	var submitButton=document.getElementById(submitId);
	if (srcStr.match(regexPat)==null) {
		alert("The "+name+" is not valid.");
		submitButton.disabled=true;
		return false;
	}
	submitButton.disabled=false;
	return true;
}
