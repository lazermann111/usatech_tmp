var DDSPEED = 10;
var DDTIMER = 15;
var OFFSET = -2;
var ZINT = 100;

function ddMenu(id,d, fixedheight){
  var h = $(id + '-ddheader');
  var c = $(id + '-ddcontent');
  clearInterval(c.timer);
  if(d == 1){
    clearTimeout(h.timer);
    c.style.display = 'block';
    if(c.maxh && c.maxh <= c.offsetHeight){return}
    else if(!c.maxh){
      c.style.right = (h.offsetWidth+OFFSET) + 'px';
      c.maxh =fixedheight;
      c.style.height = fixedheight+'px';
    }
    ZINT = ZINT + 1;
    c.style.zIndex = ZINT;
    c.timer = setInterval(function(){ddSlide(c,1)},DDTIMER);
  }else{
    h.timer = setTimeout(function(){ddCollapse(c)},50);
  }
}

function ddCollapse(c){
  c.timer = setInterval(function(){ddSlide(c,-1)},DDTIMER);
}

function cancelHide(id){
  var h = $(id + '-ddheader');
  var c = $(id + '-ddcontent');
  clearTimeout(h.timer);
  clearInterval(c.timer);
  if(c.offsetHeight < c.maxh){
    c.timer = setInterval(function(){ddSlide(c,1)},DDTIMER);
  }
}

function ddSlide(c,d){
  var currh = c.offsetHeight;
  var dist;
  if(d == 1){
    dist = Math.round((c.maxh - currh) / DDSPEED);
    c.style.opacity = currh / c.maxh;
  }else{
    dist = Math.round(currh / DDSPEED);
    c.style.opacity = 0;
  }
  if(dist <= 1 && d == 1){
    dist = 1;
  }
  
  if (Browser.ie){
	if(Browser.version < 9){
		  if(d < 0){
			  dist = 0;
			  c.style.filter = 'alpha(opacity=' + (currh * 100 / c.maxh)*100 + ')';
		  }
  	}
  }
  
  c.style.height = currh + (dist * d) + 'px';
  c.style.filter = 'alpha(opacity=' + (currh * 100 / c.maxh) + ')';

  if(currh > (c.maxh - 2) && d == 1){
    clearInterval(c.timer);
  }else if(dist < 1 && d != 1){
    clearInterval(c.timer);
    c.style.display = 'none';
  }
}