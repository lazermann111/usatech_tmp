function sellRateChanged(sellRateField) {
	name = sellRateField.get('name');
	sellRate = Number.from(sellRateField.get('value'));
	arr = name.split("_");
	buyRate = Number.from($(arr[0] + '_' + arr[1] + '_BR_' + arr[3] + "_" + arr[4]).get('value'));
	precision = decimalPlaces(sellRate);
	precision = precision < 2 ? 2 : precision > 4 ? 4 : precision;
	commissionField = $(arr[0] + '_' + arr[1] + '_CM_' + arr[3] + "_" + arr[4]);
	commissionField.set('value', sellRate - buyRate);
	formatNumber(commissionField, precision);
	formatNumber(sellRateField, precision);
}

function formatNumber(field, precision) {
	var v = field.get('value');
	if(!!v) {
		n = Number.from(v);
		field.set('value', n.format({decimal: ".", group: ",", decimals: precision}));
	}
}

function decimalPlaces(n) {
	var result= /^-?[0-9]+\.([0-9]+)$/.exec(n);
	return result === null ? 0 : result[1].length;
}
