function cleanEmail(email){
	var cleanEmailArray=email.replace(',',';').split(';');
	var cleanEmail="";
	for(i=0;i<cleanEmailArray.length;i++){
		if(cleanEmailArray[i].length!=0){
			if(i==0){
				cleanEmail=cleanEmailArray[i].trim();
			}else{
				cleanEmail=cleanEmail+";"+cleanEmailArray[i].trim();
			}
		}
	}
	return cleanEmail;
}