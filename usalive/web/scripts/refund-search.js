function initDates(formId){
	var form = document.getElementById(formId);
	var cur = new Date();
	var start = form.startDateId;
	var end = form.endDateId;

	if(((new Date(start.value)) == 'Invalid Date') || ((new Date(start.value)) == 'NaN')){
		start.value = (cur.getMonth()+1) + '/1/' + cur.getFullYear();
	}

	if(((new Date(end.value)) == 'Invalid Date') || ((new Date(end.value)) == 'NaN')){
		end.value = (cur.getMonth()+1) + '/' + cur.getDate() + '/' + cur.getFullYear();
	}
}

function validateDates(form){
	var start = new Date(form.startDateId.value);
	var end = new Date(form.endDateId.value);
	var valid = false;

	if((start == 'Invalid Date') || (end == 'Invalid Date') || (start == 'NaN') || (end == 'NaN')){
		alert('You must enter a valid start and end date.');
	} else {
		if(start > end){
			alert('The end date must occur after the start date.');
		} else {
			if(start > (new Date())){
				valid = confirm('The date range is in the future, do you want to do this search anyway?');
			} else {
				valid = true;
			}
		}
	}

	return valid;
}

function validateSearchFields(form){
	var startCC = (form.searchCCFirst ? form.searchCCFirst.value.replace(/^[\s\xA0]*/, '').replace(/[\s\xA0]*$/, '') : '');
	var endCC = (form.searchCCLast ? form.searchCCLast.value.replace(/^[\s\xA0]*/, '').replace(/[\s\xA0]*$/, '') : '');
	var amt = (form.searchAmt ? form.searchAmt.value.replace(/^[\s\xA0]*/, '').replace(/[\s\xA0]*$/, '') : '');
	var apcode = (form.searchAPCode ? form.searchAPCode.value.replace(/^[\s\xA0]*/, '').replace(/[\s\xA0]*$/, '') : '');
	var serial = (form.searchSerial ? form.searchSerial.value.replace(/^[\s\xA0]*/, '').replace(/[\s\xA0]*$/, '') : '');
	var location = (form.searchLocation ? form.searchLocation.value.replace(/^[\s\xA0]*/, '').replace(/[\s\xA0]*$/, '') : '');
	var evn = (form.searchEVNumber ? form.searchEVNumber.value.replace(/^[\s\xA0]*/, '').replace(/[\s\xA0]*$/, '') : '');
	var fullCard = (form.searchFullCard ? form.searchFullCard.value.replace(/^[\s\xA0]*/, '').replace(/[\s\xA0]*$/, '') : '');
	var deviceTranId = (form.searchDeviceTranId ? form.searchDeviceTranId.value.replace(/^[\s\xA0]*/, '').replace(/[\s\xA0]*$/, '') : '');
	var cardId = (form.searchCardId ? form.searchCardId.value.replace(/^[\s\xA0]*/, '').replace(/[\s\xA0]*$/, '') : '');

	if(fullCard.length > 0) {
		if(startCC.length == 0) {
			startCC = fullCard.substring(0, 2);
			if(form.searchCCFirst) form.searchCCFirst.value = startCC;
		}
		if(endCC.length == 0) {
			endCC = fullCard.substring(fullCard.length - 4, fullCard.length);
			if(form.searchCCLast) form.searchCCLast.value = endCC;
		}
	}
	if((startCC.length + endCC.length + amt.length + apcode.length + serial.length + location.length + evn.length + fullCard.length + deviceTranId.length + cardId.length) > 0){
		if((endCC.length > 0) && (endCC.length < 4)){
			alert("You must enter all of the last 4 digits in the credit card number to search on that term.");
			return false;
		} else {
			if(amt.length > 0){
				if(!isValidNumber(amt)){
					alert("The transaction amount entered is not a valid number.");
					return false;
				}
			}
		}
	} else {
		var arr = new Array();
		if(form.searchCCFirst) arr[arr.length] = "credit card first two";
		if(form.searchCCLast) arr[arr.length] = "credit card last four";
		if(form.searchAmt) arr[arr.length] = "transaction amount";
		if(form.searchAPCode) arr[arr.length] = "ap code";
		if(form.searchSerial) arr[arr.length] = "device serial number";
		if(form.searchLocation) arr[arr.length] = "location";
		if(form.searchEVNumber) arr[arr.length] = "ev number";
		if(form.searchFullCard) arr[arr.length] = "full card number";
		if(form.searchDeviceTranId) arr[arr.length] = "device tran id";
		if(form.searchCardId) arr[arr.length] = "card id";

		var s = "";
		for(var i = 0; i < arr.length; i++) {
			if(i == arr.length - 1) s += ", or ";
			else if(i > 0) s += ", ";
			s += arr[i];
		}
		alert("You must use at least one search field (" + s + ").");
		return false;
	}
	if(form.filter_field_1) form.filter_field_1.value = (startCC.length > 0 ? form.filter_field_1.getAttribute("usevalue") : "");
	if(form.filter_field_2) form.filter_field_2.value = (endCC.length > 0 ? form.filter_field_2.getAttribute("usevalue") : "");
	if(form.filter_field_3) form.filter_field_3.value = (apcode.length > 0 ? form.filter_field_3.getAttribute("usevalue") : "");
	if(form.filter_field_4) form.filter_field_4.value = (location.length > 0 ? form.filter_field_4.getAttribute("usevalue") : "");
	if(form.filter_field_5) form.filter_field_5.value = (serial.length > 0 ? form.filter_field_5.getAttribute("usevalue") : "");
	if(form.filter_field_6) form.filter_field_6.value = (evn.length > 0 ? form.filter_field_6.getAttribute("usevalue") : "");
	if(form.filter_field_7) form.filter_field_7.value = (fullCard.length > 0 ? form.filter_field_7.getAttribute("usevalue") : "");
	if(form.filter_field_8) form.filter_field_8.value = (deviceTranId.length > 0 ? form.filter_field_8.getAttribute("usevalue") : "");
	if(form.filter_field_9) form.filter_field_9.value = (cardId.length > 0 ? form.filter_field_9.getAttribute("usevalue") : "");
	return true;
}

function isValidNumber(value){
	return !isNaN(new Number(value));
}

function doSearch(form){

	if(form.validSearch) form.validSearch.value = "true";

	if (!validateDates(form) || !validateSearchFields(form)) {
		if(form.validSearch)form.validSearch.value = "false";
		return false;
	} 
	
	$("waitImageReport").style.display='block';
	
	return	new Form.Request(form, $("searchResults"), 
					{async: true, onSuccess: function(){$("searchResultsToggle").showSearchResults(); $("waitImageReport").style.display='none';},resetForm: false, extraData: {fragment: true}}).send();
	
}

function createRefund(transactionId, actionType){
	new Request.HTML({url: "create_"+actionType + ".i", onSuccess: function(){$("issueRefundSectionToggle").showIssueRefundSection();}, data: {transactionId: transactionId, fragment: true}, update: $("issueRefundSection")}).send();
}

function mouseOver(rowId){
	var row = document.getElementById(rowId);
	row.style.color = 'blue';
}

function mouseOut(rowId){
	var row = document.getElementById(rowId);
	row.style.color = 'black';
}
function isValidRefundAmount(refundAmountValue){
	var validRefundFormat = new RegExp("^\\d{1,6}([.]\\d{1,2}){0,1}$");
	var refundAmountMatch = validRefundFormat.exec(refundAmountValue.trim());
    if (!refundAmountMatch) {
    	return false;
    }else{
    	return true;
    }
}
function submitRefund(formId,actionType){
	var form = document.getElementById(formId);
	var isOverride = false;
	var overrideMsg;
	var hasFee = false;
	var validFee = true;
	var doSubmit = false;
	var refundAmount = 0;
	var feeAmount = 0;

	if(typeof form.feeAmount != 'undefined'){
		hasFee = true;
		if(!isValidNumber(form.feeAmount.value)){
			alert('You must enter a valid number for the chargeback fee.');
			validFee = false;
		} else {
			feeAmount = Number(form.feeAmount.value);

			if(feeAmount < 0){
				alert('You must enter a valid amount (0 or greater) for the chargeback fee.');
				validFee = false;
			}
		}
	}

	if(!(isValidNumber(form.refundAmount.value)&&isValidRefundAmount(form.refundAmount.value))){
		alert('You must enter a valid number for the amount to refund.');
	} else {
		refundAmount = Number(form.refundAmount.value);

		if(!hasFee || validFee){
			if(refundAmount < .01){
				alert('You must enter a positive amount to be refunded.');
			} else {
				if(typeof form.managerOverride == 'undefined'){
					overrideMsg = "You must contact a manager that has the appropriate permission to issue this refund."
				} else {
					isOverride = form.managerOverride.checked;
					overrideMsg = "You must check the manager override box to issue this refund."
				}

				if(isOverride){
					form.action = 'issue_'+actionType+'_override.i';
					doSubmit = true;
				} else {
					if(actionType=='chargeback_reversal'){
						if((Number(form.previousChargebackReversalTotal.value)+refundAmount + feeAmount) > Number(form.transactionTotal.value)){
							alert('A manager override is needed to issue a chargeback reversal that is larger ' +
								'than the original transaction amount. ' + overrideMsg);
							if(typeof form.managerOverride != 'undefined'){
								form.managerOverride.disabled = false;
							}
						} else {
							form.action = 'issue_'+actionType+'.i';
							doSubmit = true;
						}
					}else{
						if((Math.abs(Number(form.refundSum.value)) + refundAmount + feeAmount) > Number(form.transactionTotal.value)){
							alert('A manager override is needed to issue a refund that is larger ' +
								'than the original transaction amount. ' + overrideMsg);
							if(typeof form.managerOverride != 'undefined'){
								form.managerOverride.disabled = false;
							}
						} else {
							form.action = 'issue_'+actionType+'.i';
							doSubmit = true;
						}
					}
				}
			}
		}
	}
	if(doSubmit){
		if(actionType=='chargeback_reversal'){
			document.getElementById("submitReversalButton").disabled = true;
		}else{
			document.getElementById("submitButton").disabled = true;
		}
	}
	return doSubmit;
}

function checkAmounts(formId){
	var form = document.getElementById(formId);
	var refundAmount = 0;
	var feeAmount = 0;

	if(typeof form.feeAmount != 'undefined'){
		if(isValidNumber(form.feeAmount.value)){
			feeAmount = Number(form.feeAmount.value);
		}
	}

	if(isValidNumber(form.refundAmount.value) && isValidRefundAmount(form.refundAmount.value)){
		refundAmount = Number(form.refundAmount.value);
	}
	
	if(typeof form.managerOverride != 'undefined'){
		if(Number((Math.abs(Number(form.refundSum.value)) + refundAmount + feeAmount).toFixed(2)) > Number(Number(form.transactionTotal.value).toFixed(2))){
			document.getElementById('managerText').style.color = "black";
			form.managerOverride.disabled = false;
		} else {
			document.getElementById('managerText').style.color = "gray";
			form.managerOverride.disabled = true;
		}
	}
}

function addHiddenInput(form, name, value) {
	var inp = document.createElement("input");
	inp.type = "hidden";
	inp.name = name;
	inp.value = value;
	form.appendChild(inp);
}

function submitIssueRefund(form){
	return (submitRefund('issueRefund','refund') && new Form.Request(form, $("refundChargebackMessage"), { onSuccess: function(){$("issueRefundSectionToggle").showIssueRefundSection();}, resetForm: false, extraData: {fragment: true}}).send());
}

function submitIssueRefundSet(form){
	return (submitRefund('issueRefundSet','refundset') && new Form.Request(form, $("refundChargebackMessage"), {onSuccess: function(){$("issueRefundSectionToggle").showIssueRefundSection();},resetForm: false, extraData: {fragment: true}}).send());
}

function submitIssueChargeback(form){
	return (submitRefund('issueRefund','chargeback') && new Form.Request(form, $("refundChargebackMessage"), {onSuccess: function(){$("issueRefundSectionToggle").showIssueRefundSection();}, resetForm: false, extraData: {fragment: true}}).send());
}

function submitIssueChargebackReversal(form){
	return (submitRefund('issueRefund','chargeback_reversal') && new Form.Request(form, $("refundChargebackMessage"), {onSuccess: function(){$("issueRefundSectionToggle").showIssueRefundSection();}, resetForm: false, extraData: {fragment: true}}).send());
}

