<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%
UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
%>
<jsp:include page="include/header.jsp"/>
<div class="instruction-page"><div class="title2">Automating Mass Device Updates</div>
<dl>
<dt>Background</dt>
<dd>Mass Device Update is an HTTP POST of <a href="http://www.w3.org/TR/html401/interact/forms.html#h-17.13.4.2" title="Relevant HTTP specification">multipart/form-data</a> to "<%=StringUtils.prepareHTMLBlank(RequestUtils.getBaseUrl(request, false, false)) %>/update_devices_bg_excel.html" with the following parts:
<table>
<tr><th>Name</th><th>Type</th><th>Description</th></tr>
<tr><td>username</td><td>form-data</td><td>Your USALive username (<%=StringUtils.prepareHTMLBlank(user.getUserName()) %>)</td></tr>
<tr><td>password</td><td>form-data</td><td>Your USALive password</td></tr>
<tr><td>updateFile</td><td>file</td><td>The spreadsheet file containing the devices and their information to update</td></tr>
<tr><td>notifyEmail</td><td>form-data</td><td>An email that will receive a message when USALive has fully processed the updateFile</td></tr>
</table></dd>
<dt>Steps</dt>
<dd>To automate this you need to:
<ol><li>Create a Comma-separated or Excel file that contains the devices and their information that you wish to update. It must be formatted as just like the file for <a href="update_devices_instructs.i">manual Mass Device Updates</a></li>
<li>Send that file to USALive using HTTP POST (See example below)</li>
<li>Interpret the result (NOTE: To receive text-only responses use a header of "Accept: text/plain")</li>
</ol>
</dd>
<dt>Example of HTTP POST</dt>
<dd>You can use any HTTP Client program that supports sending multipart/form-data content with the POST method. <a href="http://curl.haxx.se/">cURL</a> is widely available, has ports for most operating systems, and has been tested with USALive. cURL is used in the example below:
<code>
curl -c nul -L -H "Accept: text/plain" -F username=<%=StringUtils.prepareHTMLBlank(user.getUserName()) %> -F password=SECRET -F "updateFile=@Device_Update_today.xls" -F notifyEmail=<%=StringUtils.isBlank(user.getEmailAddress()) ? "someone@somewhere.com" : StringUtils.prepareHTMLBlank(user.getEmailAddress()) %> <%=StringUtils.prepareHTMLBlank(RequestUtils.getBaseUrl(request, false, false)) %>/update_devices_bg_excel.html
</code>
</dl>
<dt>Response</dt>
<dd>The plain text response will contain one line indicating the overall success of processing ("SUCCESS" or "ERROR") and an additional line providing greater details.<br/> 
<ul><li>"SUCCESS" means your file has been accepted and will be processed in the background</li>
<li>"ERROR" means your file has not been accepted and the additional line(s) will describe the reason</li></ul></dd>
</div>
<jsp:include page="include/footer.jsp"/>