<%@page import="com.usatech.report.ReportingUser.ActionType"%>
<%@page import="com.usatech.report.ReportingUser.ResourceType"%>
<%@page import="com.usatech.usalive.web.PrepaidUtils"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.GenerateUtils"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="simple.db.Call.Sorter"%>
<%@page import="simple.io.Log"%>
<%@page import="com.usatech.ec2.EC2ClientUtils"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%@page import="simple.servlet.ResponseUtils"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.Date"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="simple.text.StringUtils"%>
<%
	Log log = Log.getLog();
	UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
	InputForm inputForm = RequestUtils.getInputForm(request);
	int consumerAcctId=inputForm.getInt("consumerAcctId", true, -1);
	boolean hasPri=false;
	if(user.isInternal()||user.hasPrivilege(ReportingPrivilege.PRIV_MANAGE_PREPAID_CONSUMERS)){
		hasPri=true;
	}
	user.checkPermission(ResourceType.PREPAID_CONSUMER_ACCT, ActionType.VIEW, consumerAcctId);
	Results results=DataLayerMgr.executeQuery("GET_CONSUMER_ACCT", inputForm);
	String currencyFormat="CURRENCY";
	results.setFormat("cardBalance", currencyFormat);
	results.setFormat("loyaltyDiscountTotal", currencyFormat);
	results.setFormat("consumerAcctPromoBalance", currencyFormat);
	results.setFormat("consumerAcctReplenishBalance", currencyFormat);
	results.setFormat("consumerAcctPromoTotal", currencyFormat);
	results.setFormat("consumerAcctReplenishTotal", currencyFormat);
	results.setFormat("purchaseDiscountTotal", currencyFormat);
	String dateFormat="DATE:MM/dd/yyyy hh:mm a";
	results.setFormat("cardBalance", currencyFormat);
	results.setFormat("createdTs", dateFormat);
	results.setFormat("lastUpdatedTs", dateFormat);
	results.setFormat("activationTs", dateFormat);
	results.setFormat("deactivationTs", dateFormat);
	results.setFormat("closeDate", dateFormat);
	boolean isClosed=false;
	boolean operServiced=false;
	int consumerAcctTypeId = -1;
	boolean isPayrollDeduct = false;
%>
<jsp:include page="include/header.jsp"/>
<script type="text/javascript">
function onToggleStatus(actionType, toggleText){
	if ($("increaseAmount"))
		$("increaseAmount").set("data-validators","");
	if ($("decreaseAmount"))
		$("decreaseAmount").set("data-validators","");
	if(confirm("Are you sure you want to toggle "+toggleText)){
		$("actionType").value=actionType;
		$("consumerAcctForm").submit();
	}
}

function updateTopUpAmt(){
	$("topUpAmt").set("data-validators","required minimum:0 maximum:1000");
	$("actionType").value='updateTopUpAmt';
	$("consumerAcctForm").submit();
}

function onCloseAccount(){
	if ($("increaseReason")) {
		$("increaseReason").set("data-validators","");
		$("increaseAmount").set("data-validators","");
	}
	if ($("decreaseReason")) {
		$("decreaseReason").set("data-validators","");
		$("decreaseAmount").set("data-validators","");
	}
	$('consumerAcctForm').getElements("div[class=validation-advice])").each(function(el) {
		el.dispose();
	});
	$('consumerAcctForm').getElements("input[class=validation-failed])").each(function(el) {
		el.set("class", "");
	});
	if(confirm("Are you sure you want to close the account?")){
		$("actionType").value='closeAccount';
		$("consumerAcctForm").submit();
	}
}

function onIncreaseCardAmount(){
	$('consumerAcctForm').getElements("div[class=validation-advice])").each(function(el) {
		el.dispose();
	});
	$('consumerAcctForm').getElements("input[class=validation-failed])").each(function(el) {
		el.set("class", "");
	});
	$("decreaseAmount").set("data-validators","");
	$("decreaseReason").set("data-validators","");
	$("increaseAmount").set("data-validators","required minimum:0 maximum:100");
	$("increaseReason").set("data-validators","required maxLength:100");
	$("actionType").value="increaseCardAmount";
	$("consumerAcctForm").submit();
}

function onDecreaseCardAmount(){
	$('consumerAcctForm').getElements("div[class=validation-advice])").each(function(el) {
		el.dispose();
	});
	$('consumerAcctForm').getElements("input[class=validation-failed])").each(function(el) {
		el.set("class", "");
	});
	$("increaseAmount").set("data-validators","");
	$("increaseReason").set("data-validators","");
	$("decreaseAmount").set("data-validators","required minimum:0 maximum:100");
	$("decreaseReason").set("data-validators","required maxLength:100");
	$("actionType").value="decreaseCardAmount";
	$("consumerAcctForm").submit();
}

window.addEvent('domready', function() {
	new Form.Validator.Inline.Mask($("consumerAcctForm"));

});
</script>
<%if(hasPri){ %>
<form name="consumerAcctForm" id="consumerAcctForm" action="consumer_acct_edit.html">
<input id="actionType" type="hidden" name="actionType" value="">
<input id="consumerAcctId" type="hidden" name="consumerAcctId" value="<%=consumerAcctId%>">
<%if(results.next()){
	String currencyCd=results.getFormattedValue("currencyCd");
	Date closeDate=results.getValue("closeDate", Date.class);
	if(closeDate!=null&&closeDate.before(new Date())){
		isClosed=true;
		%>
		<p>Status: Account is closed on <%=StringUtils.prepareHTML(results.getFormattedValue("closeDate")) %></p>
		<%
	}
	operServiced = ConvertUtils.getBoolean(results.get("operatorServicedInd"));
	consumerAcctTypeId = ConvertUtils.getInt(results.get("consumerAcctTypeId"));
	isPayrollDeduct = consumerAcctTypeId == 7 ? true : false;
	Date deactivationDate=results.getValue("deactivationTs", Date.class);
	results.setFormat("topUpAmt", "NUMBER:####0.00");
	String topUpAmt = RequestUtils.getAttribute(request, "topUpAmt", String.class, false);
	if(topUpAmt == null) {
		topUpAmt = results.getFormattedValue("topUpAmt");
	}
%>
<input id="consumerAcctTypeId" type="hidden" name="consumerAcctTypeId" value="<%=consumerAcctTypeId%>">
<div class="selectSection">
<div class="sectionTitle">Consumer Account (<%=StringUtils.prepareHTML(results.getFormattedValue("consumerAcctSubTypeDesc")) %>)</div>
<br/>
<div class="sectionRow">
<table class="input-rows">
<tbody>
<tr>
<td>
Card Number
</td>
<td>
<%=StringUtils.prepareHTML(results.getFormattedValue("cardNum")) %>
</td>
<td>
Card Id
</td>
<td>
<%=StringUtils.prepareHTML(results.getFormattedValue("cardId")) %>
</td>
</tr>
<tr>
<td>
Currency
</td>
<td>
<%=StringUtils.prepareHTML(currencyCd) %>
</td>
<td>
Balance
</td>
<td>
<%=StringUtils.prepareHTML(results.getFormattedValue("cardBalance")) %> <%=StringUtils.prepareHTML(currencyCd) %>
</td>
</tr>
<tr>
<td>
Active
</td>
<td >
<div id="statusText" style="display:inline"><%if(results.getFormattedValue("active").equals("Y")){ %> <span class="status-on">Yes</span> <%}else{ %> <span class="status-off">No</span><%} %> </div><%if(operServiced && !isClosed){ %><input id="toggleStatus" type="button" value="Toggle" onClick="onToggleStatus('toggleStatus', 'Account Active Status');"><%} %>
</td>
<td>
Created Date
</td>
<td>
<%=StringUtils.prepareHTML(results.getFormattedValue("createdTs")) %>
</td>
</tr>
<tr>
<td>
Last Updated Date
</td>
<td>
<%=StringUtils.prepareHTML(results.getFormattedValue("lastUpdatedTs")) %>
</td>
<td>
Activation Date
</td>
<td>
<%=StringUtils.prepareHTML(results.getFormattedValue("activationTs")) %>
</td>
</tr>
<tr>
<td>
Deactivation Date
</td>
<td>
<%if(deactivationDate!=null&&deactivationDate.before(PrepaidUtils.MAX_DATE)){ %>
<%=StringUtils.prepareHTML(results.getFormattedValue("deactivationTs")) %>
<%} %>
</td>
<td>
Expiration Date
</td>
<td>
<%=StringUtils.prepareHTML(results.getFormattedValue("expirationDate")) %>
</td>
</tr>
<tr>
<td>
Card Type
</td>
<td>
<%=StringUtils.prepareHTML(results.getFormattedValue("cardType")) %>
</td>
<td>
Account Type
</td>
<td>
<%=StringUtils.prepareHTML(results.getFormattedValue("consumerAcctTypeDesc")) %>
</td>
</tr>
<tr>
<td>
Consumer
</td>
<td>
<a href="consumer.html?consumerId=<%=results.getFormattedValue("consumerId") %>"> <%=StringUtils.prepareHTML(results.getFormattedValue("firstName")) %> <%=StringUtils.prepareHTML(results.getFormattedValue("lastName")) %></a>
</td>
<td>
Allow Negative Balance
</td>
<td>
<div id="allowNegativeText" style="display:inline"><%if(results.getFormattedValue("allowNegativeBalance").equals("Y")){ %> <span class="status-on">Yes</span> <%}else{ %> <span class="status-off">No</span><%} %> </div><%if(operServiced && !isClosed){ %><input id="toggleAllowNegative" type="button" value="Toggle" onClick="onToggleStatus('toggleNegativeBalance', 'Allow Negative Balance');"><%} %>
</td>
</tr>
<tr>
<td>
Reporting Customer
</td>
<td>
<%=StringUtils.prepareHTML(results.getFormattedValue("corpCustomerName")) %>
</td>
<td>
Loyalty Discount Total
</td>
<td>
<%=StringUtils.prepareHTML(results.getFormattedValue("loyaltyDiscountTotal")) %>
</td>
</tr>
<tr>
<td>
Promo Balance
</td>
<td>
<%=StringUtils.prepareHTML(results.getFormattedValue("consumerAcctPromoBalance")) %> <%=StringUtils.prepareHTML(currencyCd) %>
</td>
<td>
Replenish Balance
</td>
<td>
<%=StringUtils.prepareHTML(results.getFormattedValue("consumerAcctReplenishBalance")) %> <%=StringUtils.prepareHTML(currencyCd) %>
</td>
</tr>
<tr>
<td>
Promo Total
</td>
<td>
<%=StringUtils.prepareHTML(results.getFormattedValue("consumerAcctPromoTotal")) %> <%=StringUtils.prepareHTML(currencyCd) %>
</td>
<td>
Replenish Total
</td>
<td>
<%=StringUtils.prepareHTML(results.getFormattedValue("consumerAcctReplenishTotal")) %> <%=StringUtils.prepareHTML(currencyCd) %>
</td>
</tr>
<tr>
<td>Purchase Discount Total</td>
<td>
<%=StringUtils.prepareHTML(results.getFormattedValue("purchaseDiscountTotal")) %> <%=StringUtils.prepareHTML(currencyCd) %>
</td>
</tr>
<% if (isPayrollDeduct) { %>
<tr>
<td>Periodic Spending Limit</td>
<td>
<% if (!isClosed) { %>
$<input type="text" name="topUpAmt" id="topUpAmt" size="8" value="<%=StringUtils.prepareCDATA(topUpAmt)%>"/> <input type="button" value="Update" onclick="updateTopUpAmt();"/>
<% } else { %>
$<%=StringUtils.prepareHTML(topUpAmt)%>
<% } %>
<%=StringUtils.prepareHTML(currencyCd) %>
</td>
<td></td>
<td></td>
</tr>
<% } %>
<%if(operServiced && !isClosed){ %>
<tr>
<td colspan="2">
<input id="closeAccount" type="button" value="Close Account" onClick="onCloseAccount();">
</td>
<td colspan="2">
<input id="refresh" type="button" value="Refresh" onClick="location.href='consumer_acct.html?consumerAcctId=<%=consumerAcctId%>'">
</td>
<%} %>
</tr>
</tbody>
</table>
<%if(operServiced && !isClosed){ %>
<br/>
<div class="sectionTitle">Manage Balance</div>
<table class="input-rows">
<tbody>
<tr>
<td>Reason: <input id="increaseReason" type="text" value="" name="increaseReason" title="Enter the reason to increase money to this card"></td>
<td>Amount: <input id="increaseAmount" type="text" value="" name="increaseAmount" title="Enter the increase amount to this card"></td>
<td><input id="addToCardButton" type="button" value="Increase Card Balance" onClick="onIncreaseCardAmount();"></td>
</tr>
</tbody>
</table>
<table class="input-rows">
<tbody>
<tr>
<td>Reason: <input id="decreaseReason" type="text" value="" name="decreaseReason" title="Enter the reason to decrease money from this card"></td>
<td>Amount:<input id="decreaseAmount" type="text" value="" name="decreaseAmount" title="Enter the decrease amount from this card"></td>
<td><input id="chargeCardButton" type="button" value="Decrease Card Balance" onClick="onDecreaseCardAmount();"></td>
</tr>
</tbody>
</table>
<%} %>
</div>
</div>
</form>
<%} %>
<%}else{%>
	<p class="not-authorized">User '<%=StringUtils.prepareHTML(user.getUserName()) %>' does not have permission to view this page. Please contact USA Technologies if you need access.</p>
<%}%>
<jsp:include page="include/footer.jsp"/>
