<%@page import="simple.lang.Holder"%>
<%@page import="simple.security.SecureHash"%>
<%@page import="simple.xml.sax.MessagesInputSource"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.usatech.usalive.web.PrepaidUtils"%>
<%@page import="com.usatech.report.ReportingUser.ActionType"%>
<%@page import="com.usatech.report.ReportingUser.ResourceType"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.GenerateUtils"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="simple.db.Call.Sorter"%>
<%@page import="simple.io.Log"%>
<%@page import="com.usatech.ec2.EC2ClientUtils"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%@page import="simple.servlet.ResponseUtils"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.Date"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="simple.text.StringUtils"%>
<%
	Log log = Log.getLog();
	UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
	InputForm inputForm = RequestUtils.getInputForm(request);
	int consumerId=inputForm.getInt("consumerId", true, -1);
	boolean hasPri=false;
	int consumerTypeId = inputForm.getInt("consumerTypeId", true, 0);
	boolean isPayrollDeduct = consumerTypeId == 9 ? true : false;
	if(user.isInternal()||user.hasPrivilege(ReportingPrivilege.PRIV_MANAGE_PREPAID_CONSUMERS)){
		hasPri=true;
	}else{
		return;
	}
	String actionType = RequestUtils.getAttribute(request, "actionType", String.class, false);
	if(actionType!=null){
		user.checkPermission(ResourceType.PREPAID_CONSUMER, ActionType.EDIT, consumerId);
		MessagesInputSource messages=RequestUtils.getOrCreateMessagesSource(request);
		if(actionType.equals("resetPassword")){
			String password=inputForm.getString("newpassword", true);
			String confirm=inputForm.getString("newconfirm", true);
			if(StringUtils.isBlank(password)||StringUtils.isBlank(confirm)){
				messages.addMessage("error", "consumer-missing-input", "You did not provide all required fields. Please try again.");
			}else{
				boolean checkPasswordResult=PrepaidUtils.checkPassword(messages, password, confirm);
				if(checkPasswordResult){
					String credentialAlg = "SHA-256/1000";
					byte[] credentialSalt = SecureHash.getSalt(32);
					byte[] credentialHash = SecureHash.getHash(password.getBytes(), credentialSalt, credentialAlg);
					inputForm.setAttribute("credentialAlg", credentialAlg);
					inputForm.setAttribute("credentialSalt", credentialSalt);
					inputForm.setAttribute("credentialHash", credentialHash);
					DataLayerMgr.executeUpdate("UPDATE_PASSWORD", inputForm, true);
					messages.addMessage("success", "usalive-prepaid-reset-password-success", "Consumer password is reset");
				}
			}
			RequestUtils.redirectWithCarryOver(request, response, "consumer.html?consumerId="+consumerId, false, true);
		}else if(actionType.equals("updateSimple")){
			String firstname=inputForm.getString("firstname", true);
			String lastname=inputForm.getString("lastname", true);
			String region=inputForm.getString("region", false);
			if(StringUtils.isBlank(firstname)||StringUtils.isBlank(lastname)){
				messages.addMessage("error", "consumer-missing-input", "You did not provide all required fields. Please try again.");
			}else{
				DataLayerMgr.executeUpdate("UPDATE_USER_SIMPLE", new Object[]{consumerId, firstname, lastname, region}, true);
				messages.addMessage("success", "usalive-prepaid-update-simple-success", "Consumer firstname and lastname is updated.");
			}
			RequestUtils.redirectWithCarryOver(request, response, "consumer.html?consumerId="+consumerId, false, true);
		}else if(actionType.equals("updateConsumer")){
			String firstname=inputForm.getString("firstname", true);
			String lastname= inputForm.getString("lastname", true);
			String address1 = inputForm.getString("address1", isPayrollDeduct ? false : true);
			String postal = inputForm.getString("postal", isPayrollDeduct ? false : true);
			String country = inputForm.getString("country", isPayrollDeduct ? false : true);
			if (isPayrollDeduct) {
				if(StringUtils.isBlank(firstname) || StringUtils.isBlank(lastname)) {
					messages.addMessage("error", "consumer-missing-input", "You did not provide all required fields. Please try again.");
				} else {
					DataLayerMgr.executeUpdate("UPDATE_PAYROLL_DEDUCT_CONSUMER", inputForm, true);
					messages.addMessage("success", "usalive-prepaid-update-consumer-success", "Consumer settings are updated.");
				}
			} else {
				if(StringUtils.isBlank(firstname) || StringUtils.isBlank(lastname) || StringUtils.isBlank(address1) || StringUtils.isBlank(postal) ||StringUtils.isBlank(country)) {
					messages.addMessage("error", "consumer-missing-input", "You did not provide all required fields. Please try again.");
				} else {
					Holder<String> formattedPostal = new Holder<String>();
					if(!GenerateUtils.checkPostal(country, postal, true, formattedPostal)) {
						messages.addMessage("error", "prepaid-invalid-postal", "You entered an invalid postal code for the country {1}. Please try again.", postal, country);
					}else{
						DataLayerMgr.executeUpdate("UPDATE_USER", inputForm, true);
						messages.addMessage("success", "usalive-prepaid-update-consumer-success", "Consumer settings are updated.");
					}
				}
			}
			RequestUtils.redirectWithCarryOver(request, response, "consumer.html?consumerId="+consumerId, false, true);
		}
	}
	
%>

