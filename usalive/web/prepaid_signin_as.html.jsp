<%@page import="simple.text.StringUtils"%>
<%@page import="javax.crypto.spec.SecretKeySpec"%>
<%@page import="javax.crypto.SecretKey"%>
<%@page import="simple.security.crypt.CryptUtils"%>
<%@page import="simple.io.Base64"%>
<%@page import="java.security.SecureRandom"%>
<%@page import="simple.io.ByteArrayUtils"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="com.usatech.report.ReportingUser.ActionType"%>
<%@page import="com.usatech.report.ReportingUser.ResourceType"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%! SecureRandom random = new SecureRandom(); %>
<%
long consumerId = RequestUtils.getAttribute(request, "consumerId", Long.class, true);
UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
user.checkPermission(ResourceType.PREPAID_CONSUMER, ActionType.USE, consumerId);
Map<String,Object> params = new HashMap<>();
params.put("consumerId", consumerId);
byte[] newPasscode = new byte[32]; //keysize
random.nextBytes(newPasscode);
params.put("newPasscode", Base64.encodeBytes(newPasscode, true));
DataLayerMgr.executeCall("GET_PREPAID_SIGNIN_PASSCODE", params, true);
long passcodeId = ConvertUtils.convertRequired(Long.class, params.get("passcodeId"));
String passcodeBase64 = ConvertUtils.convertRequired(String.class, params.get("passcode"));
SecretKey key = new SecretKeySpec(Base64.decode(passcodeBase64), "AES");
DataLayerMgr.selectInto("GET_SUBDOMAIN_FOR_CONSUMER", params);
String targetSite = ConvertUtils.convertRequired(String.class, params.get("subdomain"));
StringBuilder url = new StringBuilder();
if(targetSite.startsWith("localhost") && (targetSite.length() == 9 || targetSite.charAt(9) == '/'))
	url.append("http://").append(targetSite, 0, 9).append(":8580").append(targetSite, 9, targetSite.length());
else
    url.append("https://").append(targetSite);
url.append("/signin_as.html?sid=").append(passcodeId).append("&skey=");
byte[] data = new byte[32];
ByteArrayUtils.writeLong(data, 0, random.nextLong());
ByteArrayUtils.writeLong(data, 8, System.currentTimeMillis());
ByteArrayUtils.writeLong(data, 16, consumerId);
ByteArrayUtils.writeLong(data, 24, user.getUserId());
byte[] encrypted = CryptUtils.encrypt("AES", key, data); // may need to set ivspec
url.append(StringUtils.prepareURLPart(Base64.encodeBytes(encrypted, true)));
response.sendRedirect(url.toString());
%>