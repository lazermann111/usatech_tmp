<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.xml.sax.MessagesInputSource"%>
<%@page import="java.util.Map"%>
<%@page import="simple.util.sheet.SheetUtils"%>
<%@page import="com.usatech.usalive.web.PrepaidUtils"%>
<%@page import="simple.util.sheet.SheetUtils.RowValuesIterator"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.servlet.InputFile"%>
<%@page import="java.io.InputStream"%>
<%@page import="com.usatech.report.ReportingUser.ActionType"%>
<%@page import="com.usatech.report.ReportingUser.ResourceType"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.io.Log"%>
<%@page import="simple.servlet.ResponseUtils"%>
<%@page import="java.io.InputStream"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="simple.text.StringUtils"%>
<%
	Log log = Log.getLog();
	UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
	InputForm form = RequestUtils.getInputForm(request);
	boolean hasPri=false;
	if(user.isInternal() || (user.hasPrivilege(ReportingPrivilege.PRIV_MANAGE_PREPAID_CONSUMERS))) {
		hasPri=true;
	}

%>
<jsp:include page="include/header.jsp"/>

<%if(hasPri){ %>
<div class="toggle-heading">
	<span class="caption">Mass Card Update</span>
</div>
<div class="selectSection" style="width:70em;">

<form enctype="multipart/form-data" name="balanceUpdateForm" id="balanceUpdateForm" method="post" action="consumer_card_mass_update.html">
<input id="actionType" type="hidden" name="actionType" value="balanceUpdate">
<div class="sectionTitle">Mass Card Balance Update</div>
<div class="sectionRow">
<table class="input-rows">
<tbody>
<tr>
<td>Card Balance Change Spreadsheet/CSV File:</td>
<td><input type="file" title="Select the file that contains list of card that need to change balance" value="" required="required" name="updateFile" ></td>
</tr>
<tr>
<td align="center" colspan="1"><input id="increaseMassUpdate" name="increaseMassUpdate" type="submit" value="Increase Card Balance"></td>
<td align="center" colspan="1"><input id="decreaseMassUpdate" name="decreaseMassUpdate" type="submit" value="Decrease Card Balance"></td>
</tr>
<tr>
<td colspan="2">
<div class="columnNote">
Please upload a spreadsheet for the Mass Card Balance Increase or Decrease with the columns specified below in the first row and the data in rows under that. Supported file types: Excel 2007 and higher (*.xlsx), Excel 97-2003 (*.xls), Comma-Separated Values (*.csv). All columns are required. Column headers are required. Maximum 100 records per file.
<ul style="padding-top:1em;">
<li>CARD_ID <span class="columnNote">6 digit Card ID</span></li>
<li>AMOUNT <span class="columnNote">The amount to increase or decrease</span></li>
<li>REASON <span class="columnNote">The reason for the increase or decrease</span></li>
</ul>
Here is a sample <a class="filelinks" title="Download Sample Update Excel File" href="card/sample-card-update-file.xlsx">Excel file</a> or <a class="filelinks" title="Download Sample Update CSV File" href="card/sample-card-update-file.csv">CSV file</a>
</div>
</td>
</tr>
</tbody>
</table>
</div>
</form>

<div class="whitespacer"></div>

<form enctype="multipart/form-data" name="allowNegativeBalanaceForm" id="allowNegativeBalanaceForm" method="post" action="consumer_card_mass_update.html">
<input id="actionType" type="hidden" name="actionType" value="allowNegativeBalanceMassUpdate">
<div class="sectionTitle">Mass Card Allow Negative Balance Update</div>
<div class="sectionRow">
<table class="input-rows">
<tbody>
<tr>
<td>Negative Balance Setting Spreadsheet/CSV File:</td>
<td><input type="file" required="required" title="Select the file containing the list of cards that need to change the allow negative balance setting" value="" name="updateFile"></td>
</tr>
<tr>
<td align="center" colspan="2"><input id="allowNegativeBalanceMassUpdate" type="submit" value="Update Allow Negative Balance Setting" ></td>
</tr>
<tr>
<td colspan="2">
<div class="columnNote">
Please upload a spreadsheet for the Negative Balance Setting with the columns specified below in the first row and the data in rows under that. Supported file types: Excel 2007 and higher (*.xlsx), Excel 97-2003 (*.xls), Comma-Separated Values (*.csv). All columns are required. Column headers are required. Maximum 100 records per file.
<ul style="padding-top:1em;">
<li>CARD_ID <span class="columnNote">6 digit Card ID</span></li>
<li>ALLOW_NEGATIVE_BALANCE <span class="columnNote">Use a single character for the value: Y=Yes, N=No</span></li>
</ul>
Here is a sample <a class="filelinks" title="Download Sample Update Excel File" href="card/sample-card-anb-update-file.xlsx">Excel file</a> or <a class="filelinks" title="Download Sample Update CSV File" href="card/sample-card-anb-update-file.csv">CSV file</a>
</div>
</td>
</tr>
</tbody>
</table>
</div>
</form>
<div class="whitespacer"></div>

<form enctype="multipart/form-data" name="accountActivateForm" id="accountActivateForm" method="post" action="consumer_card_mass_update.html">
<div class="sectionTitle">Mass Account Activation<br/>
(Payroll Deduct Cards Only)</div>
<input id="actionType" type="hidden" name="actionType" value="accountActivate">
<div class="sectionRow">
<table class="input-rows">
<tbody>
<tr>
<td>Account Data Spreadsheet/CSV File:</td>
	<td><input type="file" required="required" title="Select the file containing the list of cards and associated cardholder data." value="" name="updateFile"></td>
</tr>
<tr>
<td align="center" colspan="2"><input id="accountActivate" type="submit" value="Activate Cards" ></td>
</tr>
<tr>
<td colspan="2">
<div class="columnNote">
Please upload a spreadsheet containing cardholder data to associate with each account, with the columns specified below in the first row and the data in rows under that. Each account must have a valid unique email address. Supported file types: Excel 2007 and higher (*.xlsx), Excel 97-2003 (*.xls), Comma-Separated Values (*.csv). Column headers are required. Maximum 100 records per file.<br/>
<ul style="padding-top:1em;">
<li>CARD_ID <span class="columnNote">6 digit Card ID (Required)</span></li>
<li>FIRST_NAME <span class="columnNote">Cardholder First Name (Required)</span></li>
<li>LAST_NAME <span class="columnNote">Cardholder Last Name (Required)</span></li>
<li>EMAIL <span class="columnNote">Cardholder Email Address (Required)</span></li>
<li>CARDHOLDER_ID <span class="columnNote">Custom Cardholder Identifier, such as Employee ID, Payroll ID, Student ID (Required)</span></li>
<li>REGION_NAME <span class="columnNote">Cardholder Region (From Administration > Regions) (Not Required)</span></li>
<li>DEPARTMENT_NAME <span class="columnNote">Cardholder Department Name (Not Required)</span></li>
<li>PERIODIC_LIMIT<span class="columnNote">Periodic Spending Limit (Required)</span></li>
</ul>
Here is a sample <a class="filelinks" title="Download Sample Update Excel File" href="card/sample-cardholder-activate-file.xlsx">Excel file</a> or <a class="filelinks" title="Download Sample Update CSV File" href="card/sample-cardholder-activate-file.csv">CSV file</a>
</div>
</td>
</tr>
</tbody>
</table>
</div>
</form>

</div>
<%}else{%>
	<p class="not-authorized">User '<%=StringUtils.prepareHTML(user.getUserName()) %>' does not have permission to view this page. Please contact USA Technologies if you need access.</p>
<%}%>
<script type="text/javascript">

window.addEvent('domready', function() {
	new Form.Validator.Inline.Mask($("balanceUpdateForm"));
	new Form.Validator.Inline.Mask($("allowNegativeBalanaceForm"));
});

</script>
<jsp:include page="include/footer.jsp"/>
