<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.io.Log"%>
<%@page import="simple.db.ParameterException"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Spring Order Form</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="/css/more_normalize.css">
        <link rel="stylesheet" href="/css/more_main.css">
        <script src="/scripts/modernizr-2.8.3.min.js"></script>
        <script type="text/javascript">
        var hasCombo=0;
	    var hasTeleOnly=0;
	    var sum=0;
        	function updateTotal(){
        		var q1,q2,q3; 
        	    q1 = document.getElementById("q1").value; 
        	    q2 = document.getElementById("q2").value; 
        	    q3 = document.getElementById("q3").value;
        	    sum=0;
        	    if(!isNaN(parseInt(q1)) && parseInt(q1)>0){
        	    	sum = parseInt(q1);
        	    	hasCombo=1;
        	    }else{
        	    	hasCombo=0;
        	    }
        	    if(!isNaN(parseInt(q2)) && parseInt(q2)>0){
        	    	sum =(sum+parseInt(q2));
        	    	hasTeleOnly=1;
        	    }else{
        	    	hasTeleOnly=0;
        	    }
        	    if(!isNaN(parseInt(q3)) && parseInt(q3)>0){
        	    	sum =(sum+parseInt(q3));
        	    }
				if( document.getElementById("needEportOnlineSignup").checked){
        	    	sum=(sum+1);
        	    }
        	    document.forms[0].total.value=sum;
        	    document.forms[0].totalQuantity.value=sum;
        	    document.forms[0].hasCombo.value=hasCombo;
        	    document.forms[0].hasTeleOnly.value=hasTeleOnly;
        	    var comboSection=document.getElementById("comboSection");
        	    if(hasCombo > 0){
        	    	comboSection.style.display="block";
        	    }else{
        	    	comboSection.style.display="none";
        	    }
        	    
        	    var teleOnlySection=document.getElementById("teleOnlySection");
        	    if(hasTeleOnly > 0){
        	    	teleOnlySection.style.display="block";
        	    }else{
        	    	teleOnlySection.style.display="none";
        	    }
        	    
        	}
        	
        	function onSubmit(){
        		if(sum==0){
        			alert("Please choose at least one product. Thank you.");
        			return false;
        		}else{
        			return true;
        		}
        	}
        	
        </script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

<%
	String action = RequestUtils.getAttribute(request, "submit", String.class, false);
	if(!StringUtils.isBlank(action)) { 
		boolean okay = false;
       
		try {
			InputForm inputForm=RequestUtils.getInputForm(request);
			String purchaseType="";
			String details="";
			int hasCombo=inputForm.getInt("hasCombo", true, 0);
			if(hasCombo>0){
				purchaseType="Combo:"+inputForm.getStringSafely("purchaseType1", "");
				details="Combo:"+inputForm.getStringSafely("details1", "");
			}
			int hasTeleOnly=inputForm.getInt("hasTeleOnly", true, 0);
			if(hasTeleOnly>0){
				purchaseType=purchaseType + " Telemeter:"+inputForm.getStringSafely("purchaseType2", "");
				details=details +" Telemeter:"+inputForm.getStringSafely("details2", "");
			}
			inputForm.set("purchaseType", purchaseType);
			inputForm.set("details", details);
			DataLayerMgr.executeCall("ADD_OFFER_INTEREST", inputForm, true);
			okay = true;
		} catch(ParameterException e) {
			Log.getLog().error("Invalid parameters for MORE special interest for '" + RequestUtils.getAttribute(request, "email", String.class, false) + "'", e);
			%><div class="alert alert-error">Please provide valid values for all fields</div>
			<%
		} catch(Exception e) {
			Log.getLog().error("Could not add spring special interest for '" + RequestUtils.getAttribute(request, "email", String.class, false) + "'", e);
			%>
			<div class="alert alert-error">We are so sorry! We were unable to process your request at this time. Please try again.</div><%
		}
		String errorMessage = RequestUtils.getAttribute(request, "errorMessage", String.class, false);
		if(!StringUtils.isBlank(errorMessage)) {
			%><div class="alert alert-error"><%=StringUtils.prepareHTML(errorMessage) %></div>
			<%
		} else if(okay) {
		%>
		<div class="wrapper">
          <img src="/images/thankyou.png" alt="Thank you for your order" />
          <!-- confirmation message -->
          <p style="padding: 0 45px;">Thank you for participating in our MORE Special. A sales representative will contact you within three business days to confirm and complete your order.
          </p> 
          <p>&nbsp;
          </p>
        </div>  <!-- /end wrapper --><%
        	return;
		}
		
	}	%> 

<div class="wrapper">
    <div>
        <img src="/images/more-logo.png" alt="more"  style="margin-top: 25px;" />
    </div>

    <div class="presale-text">
        <p>
        For a limited time only, the <em>MORE.</em> Loyalty and Rewards program from USAT is available with no monthly service fees!</p>
    </div>

    <div class="sale-text">
             <img src="/images/free2.png" alt="Free through 3-31-15" />
            <p>
        Increase sales and participation with <em>MORE.</em>  Offer discounts, email product information, collect consumer feedback, and connect all  of the elements of your business with one streamlined Loyalty and Rewards program that spans across your entire business.</p>
    </div>

<div id="main">

<h2>Sign me up!</h2>

<!-- form -->
      <form id="contact-form" name="contact-form" action="" method="post" onSubmit="return onSubmit();">
      <input type="hidden" name="purchaseType" value=""/>
      <input type="hidden" name="details" value=""/>
      <input type="hidden" name="hasCombo" value=""/>
      <input type="hidden" name="hasTeleOnly" value=""/>
      <input type="hidden" name="totalQuantity" value=""/>
      <input type="hidden" name="offerId" value="7" />     

<hr />
<fieldset class="order-total"  style="padding-top: 2em;">
<div>
<div style="width: 82%; float: left; margin-bottom: .6em;">
 <label> <span># of ePort Cardreader &amp; Telemeters:</span></label>
 
 </div>
 <div style="width: 18%; float: right; margin-bottom: .6em;"> <input id="q1" type="text" value="" name="allInOneQuantity" maxlength="3" onchange="updateTotal();"></div>
 <div class="clearfix"></div>
</div>

<div id="comboSection" style="display:none">
<div>
<label><span style="font-weight: normal">Is this a rental, lease, or purchase?*</span></label>
          <input type = "radio"
                 name = "purchaseType1"
                 id = "comboRental"
                 value = "rental" />
          <label for = "rental" style="margin-right: 1em;">Rental</label>
          <input type = "radio"
                 name = "purchaseType1"
                 id = "comboLease"
                 value = "lease" />
          <label for = "lease" style="margin-right: 1em;">Lease</label>
          <input checked="checked" type = "radio"
                 name = "purchaseType1"
                 id = "comboPurchase"
                 value = "purchase" />
          <label for = "purchase">Purchase ($259)</label>
</div>
<p><span class="fineprint"><a href='javascript:void(0)' onclick='window.open("/ePort-Rental-and-Lease-Programs.pdf","Rental terms","height=650,width=750,toolbar=0,status=0,menubar=0");'>Rental and Lease details</a></span></p>
<div>
<label><span style="font-weight: normal">Do your machines have MDB or Pulse interfaces?</span></label>
          <input checked="checked" type = "radio"
                 name = "details1"
                 id = "comboMDB"
                 value = "mdb" />
          <label for = "mdb" style="margin-right: 1em;">MDB</label>
          <input type = "radio"
                 name = "details1"
                 id = "comboPulse"
                 value = "pulse" />
          <label for = "rental">Pulse</label>
</div>
<p>
<span class="fineprint">
 View Rental <a href='javascript:void(0)' onclick='window.open("/USAT-Rental-Terms-Conditions.pdf","Rental terms","height=650,width=750,toolbar=0,status=0,menubar=0");'>Terms and Conditions</a>.<br />
 View Lease <a href='javascript:void(0)' onclick='window.open("/USAT-Lease-Terms-Conditions.pdf","Rental terms","height=650,width=750,toolbar=0,status=0,menubar=0");'>Terms and Conditions</a>.</span></p>
<p>
<span class="fineprint">*Please refer to your existing ePort Connect agreement for processing rates and monthly service fees. </span></p>
 <hr />
</div>

<div>
<div style="width: 82%; float: left; margin-bottom: .6em;">
 <label> <span># of Telemeter only:</span></label>
 </div>
 <div style="width: 18%; float: right; margin-bottom: .6em;"> <input id="q2" type="text" value="" name="purchase" maxlength="3" onchange="updateTotal();"></div>
 <div class="clearfix"></div>
</div>

<div id="teleOnlySection" style="display:none">
<div>
<label><span style="font-weight: normal">Is this a rental, lease, or purchase?*</span></label>
          <input type = "radio"
                 name = "purchaseType2"
                 id = "teleRental"
                 value = "rental" />
          <label for = "rental" style="margin-right: 1em;">Rental</label>
          <input type = "radio"
                 name = "purchaseType2"
                 id = "teleLease"
                 value = "lease" />
          <label for = "lease" style="margin-right: 1em;">Lease</label>
          <input checked="checked" type = "radio"
                 name = "purchaseType2"
                 id = "telePucahse"
                 value = "purchase" />
          <label for = "purchase">Purchase ($189)</label>
</div>
<p><span class="fineprint"><a href='javascript:void(0)' onclick='window.open("/ePort-Rental-and-Lease-Programs.pdf","Rental terms","height=650,width=750,toolbar=0,status=0,menubar=0");'>Rental and Lease details</a></span></p>
<div>
<label><span style="font-weight: normal">Do your machines have MDB or Pulse interfaces?</span></label>
          <input checked="checked" type = "radio"
                 name = "details2"
                 id = "teleMDB"
                 value = "mdb" />
          <label for = "mdb" style="margin-right: 1em;">MDB</label>
          <input type = "radio"
                 name = "details2"
                 id = "telePulse"
                 value = "pulse" />
          <label for = "rental">Pulse</label>
</div>
<p>
<span class="fineprint">
 View Rental <a href='javascript:void(0)' onclick='window.open("/USAT-Rental-Terms-Conditions.pdf","Rental terms","height=650,width=750,toolbar=0,status=0,menubar=0");'>Terms and Conditions</a>.<br />
 View Lease <a href='javascript:void(0)' onclick='window.open("/USAT-Lease-Terms-Conditions.pdf","Rental terms","height=650,width=750,toolbar=0,status=0,menubar=0");'>Terms and Conditions</a>.</span></p>
<p>
<span class="fineprint">*Please refer to your existing ePort Connect agreement for processing rates and monthly service fees. </span></p>
 <hr />
</div>


<div>
<div style="width: 82%; float: left; margin-bottom: .6em;">
 <label> <span># of ePort Mobiles:</span></label>
 <span class="fineprint">Free Hardware, no activation fee.  View <a href='javascript:void(0)' onclick='window.open("/ePort_Micro_Markets.pdf","Rental terms","height=650,width=750,toolbar=0,status=0,menubar=0");'>Terms and Conditions</a></span>
 </div>
  <div style="width: 18%; float: right; margin-bottom: .6em;"> <input id="q3" type="text" value="" name="eportMobileQuantity" maxlength="3" onchange="updateTotal();"></div>
 <div class="clearfix"></div>
</div>

<div>
<div style="width: 82%; float: left;">
<label>
<span>Check to sign up for ePort Online:</span>
</label>
</div>
<div style="width: 18%; float: right;">
<input id="needEportOnlineSignup" type="checkbox" name="needEportOnlineSignup" value="Y" onclick="updateTotal();">
<label for="needEportOnlineSignup"></label>
<hr>
</div>
<div class="clearfix"></div>
</div>

<div >
<div style="width: 82%; float: left; margin-bottom: .6em;">
 <label> <span>TOTAL</span></label>
 <div style="margin-top: .6em;">
 <label> <span>Remember: 10 or more qualifies your company to get the new MORE loyalty and reward system with no monthly service fee!</span></label>
 </div>
 <span class="fineprint">Shipping rates will apply.</span>
 </div>
 <div style="width: 18%; float: right; margin-bottom: .6em;"> <input type="text" style="width: 100%; text-align: right;" maxlength="3" value="" name="total" disabled="true">
 </div>

 <div class="clearfix"></div>
</div>
</fieldset>

<fieldset>
<div>
<span style="font-weight: normal;">Did you know USAT offers payment processing for Dining Point-of-sale and Micromarkets?</span>
<div style="width: 90%; float: left; margin-bottom: .6em;">
 <label> <span>Send me more information!</span></label>
 </div>
 <div style="width: 10%; float: right; margin-bottom: .6em;"><input type="checkbox" value="Y" id="ckbx" name="sendMeMoreInfo" /></div>
 <div class="clearfix"></div>
</div>
</fieldset>

<hr />

     <div>
        <label for="firstname"> <span>FIRST NAME: (required)</span></label>
        <input placeholder="Please enter your first name" type="text" maxlength="50" required="required" name="first" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "first", String.class, false))%>"/>
     </div>
     <div>
        <label for="lastname"><span>LAST NAME: (required)</span></label>
          <input placeholder="Please enter your last name" type="text" maxlength="50" required="required" name="last" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "last", String.class, false))%>"/>
     </div>
     <div>
        <label> <span>EMAIL: (required)</span></label>
          <input placeholder="Please enter your email address" type="text" maxlength="100" required="required" name="email" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "email", String.class, false))%>"/>
     </div>
     <div>
        <label><span>PHONE NUMBER: (required)</span></label>
          <input placeholder="Please enter your phone number" type="text" maxlength="20" required="required" name="phone" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "phone", String.class, false))%>"/>
      </div>
      <div>
        <label><span>COMPANY NAME: (required)</span></label>
          <input placeholder="Please enter your company name" type="text" maxlength="200" required="required" name="company" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "company", String.class, false))%>"/>
      </div>
      <div>
        <label><span>STREET ADDRESS TO SHIP TO: (required)</span></label>
          <input placeholder="Please enter your street address" type="text" maxlength="255" required="required" name="address" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "address", String.class, false))%>"/>
      </div>
      <div>
        <label> <span>CITY: (required)</span></label>
          <input placeholder="Please enter your city" type="text" maxlength="50" required="required" name="city" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "city", String.class, false))%>"/>

      </div>
      <div>
        <label><span>STATE: (required)</span></label>
          <input placeholder="Please enter your state" type="text" maxlength="50" required="required" name="state" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "state", String.class, false))%>"/>

      </div>
      <div>
        <label><span>ZIP: (required)</span></label>
          <input placeholder="Please enter your zip or postal code" type="text" maxlength="20" required="required" name="postal" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "postal", String.class, false))%>"/>
      </div>
      <p>
      <span>
          <input type="checkbox" onclick="if (this.checked) {this.form.billingAddress.value = this.form.address.value; this.form.billingCity.value = this.form.city.value; this.form.billingState.value = this.form.state.value; this.form.billingPostal.value = this.form.postal.value;}" />
          Billing address is the same as shipping address.
      </span>
      </p>
      <div>
        <label><span>BILLING STREET ADDRESS: (required)</span></label>
          <input placeholder="Please enter your billing street address" type="text" maxlength="255" required="required" name="billingAddress" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "billingAddress", String.class, false))%>"/>
      </div>
      <div>
        <label><span>BILLING CITY: (required)</span> </label>
          <input placeholder="Please enter your billing city" type="text" maxlength="50" required="required" name="billingCity" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "billingCity", String.class, false))%>"/>
      </div>
      <div>
        <label><span>BILLING STATE: (required)</span></label>
          <input placeholder="Please enter your billing state" type="text" maxlength="50" required="required" name="billingState" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "billingState", String.class, false))%>"/>
      </div>
      <div>
        <label><span>BILLING ZIP: (required)</span></label>
          <input placeholder="Please enter your billing zip or postal code" type="text" maxlength="20" required="required" name="billingPostal" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "billingPostal", String.class, false))%>"/>
      </div>

<div>
     <button name="submit" type="submit" id="contact-submit" value="optIn">SUBMIT</button>
</div>

</form>  <!-- /form -->

</div>

</div>


    </body>
</html>
