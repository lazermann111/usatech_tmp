<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="simple.text.StringUtils"%>

<% UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
	boolean hasPri = false;
	if((user.isInternal() && user.hasPrivilege(ReportingPrivilege.PRIV_CUSTOMER_SERVICE)) || user.hasPrivilege(ReportingPrivilege.PRIV_RMA)) {
		hasPri = true;
	} %>
<jsp:include page="include/header.jsp"/>
<% if(hasPri) { %>
<span class="caption">ATT 2G Upgrade</span>
<form name="rmaQuestionForm" id="rmaQuestionForm" action="rma_att.html" method="get">
	<div class="selectSection" style="width:60em;">
		<table class="input-rows" style="width:100%;">
			<tr class="sectionTitle" style="width:60em; text-align:left;">
				<td colspan="2">
					Select the AT&amp;T 2G upgrade type
				</td>
			</tr>
			<tr class="sectionRow">
				<td><input type="radio" name="rmaTypeId" id="rmaTypeId5" value="5" required="required"></td>
				<td><label for="rmaTypeId5">Like for Like (A similar ePort will be sent - the serial number will change) (* G8 and EDGE ePorts only)</label></td>
			</tr>
			<tr>
				<td><input type="radio" name="rmaTypeId" id="rmaTypeId6" value="6" required="required"></td>
				<td><label for="rmaTypeId6">Upgrade to G9 ePort</label></td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="submit" value="Next &gt;"/>
				</td>
			</tr>
		</table>
	</div>
</form>
<% } else { %>
	<p class="not-authorized">User '<%=StringUtils.prepareHTML(user.getUserName()) %>' does not have permission to view this page. Please contact USA Technologies if you need access.</p>
<% } %>
<jsp:include page="include/footer.jsp"/>
