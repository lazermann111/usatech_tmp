<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.db.ParameterException"%>
<%@page import="simple.io.Log"%>
<%@page import="java.sql.SQLException"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.RequestUtils"%>

<jsp:include page="include/header.jsp"/>

<style>
/*! HTML5 Boilerplate v4.3.0 | MIT License | http://h5bp.com/ */

html { font-size: 62.5%;}

img, embed, object, video {max-width: 100%;}

.links {
  width: 90%;
  margin: 10px auto;
  text-align: center;
}

.links span {
  float: left;
  font-size: 85%;
}

.links span > a {
  position: relative;
  display: block;
  padding: 2px 15px;
}

.links span >  a:hover,
.links span >  a:focus {
  text-decoration: none;
  background-color: #eee;
}

.wrapper p {
  margin: 10px 6%;
}

#main {
	width: 600px;
	font-family: Helvetica, 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans',  Arial, Verdana, sans-serif;
	font-size:16px !important;
	line-height:140% !important;
}

#main h2 {
  padding: 20px 6% 0;
}

#contact-form input[type="text"],
#contact-form input[type="email"],
#contact-form input[type="tel"],
#contact-form input[type="url"],
#contact-form textarea,
#contact-form button[type="submit"] {
font:400 16px/16px "Helvetica Neue", Helvetica, Arial, sans-serif;
}
#contact-form {
text-shadow:0 1px 0 #ffffff;
background:#ffffff;
padding: 0 6% 45px;
}

#contact-form span {
cursor:pointer;
color:#222222;
display:block;
margin:5px 0;
font-weight:900;
}
#contact-form input[type="text"],
#contact-form input[type="email"],
#contact-form input[type="tel"],
#contact-form input[type="url"],
#contact-form textarea {
width:100%;
box-shadow:inset 0 1px 2px #DDD, 0 1px 0 #FFF;
-webkit-box-shadow:inset 0 1px 2px #DDD, 0 1px 0 #FFF;
-moz-box-shadow:inset 0 1px 2px #DDD, 0 1px 0 #FFF;
border:1px solid #ccc;
background:#fff;
margin:0 0 5px;
padding:10px;
border-radius:5px;
}
#contact-form input[type="text"]:hover,
#contact-form input[type="email"]:hover,
#contact-form input[type="tel"]:hover,
#contact-form input[type="url"]:hover,
#contact-form textarea:hover {
-webkit-transition:border-color 0.3s ease-in-out;
-moz-transition:border-color 0.3s ease-in-out;
transition:border-color 0.3s ease-in-out;
border:1px solid #aaa;
}

#contact-form fieldset {
margin: 5px 0;
}

#contact-form fieldset label {
margin-right: 20px;
}

#contact-form button[type="submit"] {
cursor:pointer;
width:300px;
border:none;
background:#ef7f13;
background-image:linear-gradient(bottom, #ef7f13 0%, #fab228 52%);
background-image:-moz-linear-gradient(bottom, #ef7f13 0%, #fab228 52%);
background-image:-webkit-linear-gradient(bottom, #ef7f13 0%, #fab228 52%);
color:#222222;
font-weight: 700;
margin:30px 25% 0;
padding:10px;
border-radius:5px;
}
#contact-form button[type="submit"]:hover {
background-image:linear-gradient(bottom, #d87312 0%, #f4ab20 52%);
background-image:-moz-linear-gradient(bottom, #d87312 0%, #f4ab20 52%);
background-image:-webkit-linear-gradient(bottom, #d87312 0%, #f4ab20 52%);
-webkit-transition:background 0.3s ease-in-out;
-moz-transition:background 0.3s ease-in-out;
transition:background-color 0.3s ease-in-out;
}
#contact-form button[type="submit"]:active {
box-shadow:inset 0 1px 3px rgba(0,0,0,0.5);
}
#contact-form input:focus,
#contact-form textarea:focus {
outline:0;
border:1px solid #999;
}

.slogan p {
font-size: 130%;
font-weight: 200;
text-align: center;
}

.usatinfo {
text-align: center;
}

.footer {
background-color: #ffffff;
text-align: center;
color: #222222;
font-size: 85%;
margin-bottom: 40px;
}

</style>

	<%
	InputForm form = RequestUtils.getInputForm(request);
	UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
	String action = RequestUtils.getAttribute(request, "submit", String.class, false);
	if(!StringUtils.isBlank(action)) { 
		boolean okay = false;
		try {
			DataLayerMgr.executeCall("ADD_OFFER_INTEREST", RequestUtils.getInputForm(request), true);
			okay = true;
		} catch(ParameterException e) {
			Log.getLog().error("Invalid parameters for eport mobile interest for '" + RequestUtils.getAttribute(request, "email", String.class, false) + "'", e);
			%><div class="alert alert-error">Please provide valid values for all fields</div>
			<%
		} catch(Exception e) {
			Log.getLog().error("Could not add eport mobile interest for '" + RequestUtils.getAttribute(request, "email", String.class, false) + "'", e);
			%>
			<div class="alert alert-error">We are so sorry! We were unable to process your request at this time. Please try again.</div><%
		}
		String errorMessage = RequestUtils.getAttribute(request, "errorMessage", String.class, false);
		if(!StringUtils.isBlank(errorMessage)) {
			%><div class="alert alert-error"><%=StringUtils.prepareHTML(errorMessage) %></div>
			<%
		} else if(okay) {%>
		<div class="wrapper">
		  <div id="main">
          <img src="/images/eport-online_eport-mobile-thanks.png" alt="Thank you for signing up">
          <!-- confirmation message -->
          <p>Congratulations! You have taken the first step toward faster payments, streamlined invoice and account management, and eliminating billing hassles by allowing your customers to pay where and when they want! Watch for an order confirmation email within 2 business days that will include information on how to log into the ePort Mobile app (available on the App Store or Google Play) and how to make the most out of your new ePort Mobile and ePort Online services!</p>
          <p>&nbsp;</p>
          <img src="/images/iphone6.jpg" alt="iphone 6" />
          <p>Did you know... USA Technologies is giving a FREE iPhone 6 to all customers that commit to 100 new connections before 9/30/14. Contact your sales rep for details: 800-633-0340. Terms and conditions will apply.</p>
          <p>&nbsp;</p>
          </div>
        </div>  <!-- /end wrapper --><%
        	return;
		}
	}	%> 
        <div class="wrapper">
          <br/>
          <img src="/images/eport-online_eport-mobile.png" alt="ePort Online and ePort Mobile">
          <div id="main">
            <p>Please complete the form below to sign up for ePort Online and ePort Mobile.</p>
      <h2>ePort Services Form</h2>
              <!-- form -->
      <form id="contact-form" action="eport_online_eport_mobile_signup.html" method="post">
      <input type="hidden" name="offerId" value="4" />
      <div>
        <label>
          <span>COMPANY NAME: (required)</span>
          <input placeholder="Please enter your company name" type="text" maxlength="200" required="required" name="company" value="<%=StringUtils.prepareCDATA(form.getStringSafely("company", user.getCustomerName()))%>"/>
        </label>
      </div>
      <div>
        <label>
          <span>STREET ADDRESS TO SHIP TO: (required)</span>
          <input placeholder="Please enter your street address" type="text" maxlength="255" required="required" name="address" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "address", String.class, false))%>"/>
        </label>
      </div>
      <div>
        <label>
          <span>CITY: (required)</span>
          <input placeholder="Please enter your city" type="text" maxlength="50" required="required" name="city" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "city", String.class, false))%>"/>
        </label>
      </div>
      <div>
        <label>
          <span>STATE: (required)</span>
          <input placeholder="Please enter your state" type="text" maxlength="50" required="required" name="state" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "state", String.class, false))%>"/>
        </label>
      </div>
      <div>
        <label>
          <span>ZIP: (required)</span>
          <input placeholder="Please enter your zip or postal code" type="text" maxlength="20" required="required" name="postal" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "postal", String.class, false))%>"/>
        </label>
      </div>
      <span>
          <input type="checkbox" onclick="if (this.checked) {this.form.billingAddress.value = this.form.address.value; this.form.billingCity.value = this.form.city.value; this.form.billingState.value = this.form.state.value; this.form.billingPostal.value = this.form.postal.value;}" />
          BILLING ADDRESS IS SAME AS SHIPPING        
      </span>
      <div>
        <label>
          <span>BILLING STREET ADDRESS: (required)</span>
          <input placeholder="Please enter your billing street address" type="text" maxlength="255" required="required" name="billingAddress" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "billingAddress", String.class, false))%>"/>
        </label>
      </div>
      <div>
        <label>
          <span>BILLING CITY: (required)</span>
          <input placeholder="Please enter your billing city" type="text" maxlength="50" required="required" name="billingCity" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "billingCity", String.class, false))%>"/>
        </label>
      </div>
      <div>
        <label>
          <span>BILLING STATE: (required)</span>
          <input placeholder="Please enter your billing state" type="text" maxlength="50" required="required" name="billingState" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "billingState", String.class, false))%>"/>
        </label>
      </div>
      <div>
        <label>
          <span>BILLING ZIP: (required)</span>
          <input placeholder="Please enter your billing zip or postal code" type="text" maxlength="20" required="required" name="billingPostal" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "billingPostal", String.class, false))%>"/>
        </label>
      </div>
      <div>
        <label>
          <span>QUANTITY OF READERS FOR iPHONES OR iPADS: (required)</span>
          <input placeholder="Enter the number of readers you would like" type="text" maxlength="10" required="required" name="iOSQuantity" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "iOSQuantity", String.class, false))%>"/>
        </label>
      </div>
      <div>
        <label>
          <span>QUANTITY OF READERS FOR ANDROID DEVICES: (required)</span>
          <input placeholder="Enter the number of readers you would like" type="text" maxlength="10" required="required" name="androidQuantity" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "androidQuantity", String.class, false))%>"/>
        </label>
      </div>
      <div>
        <label>
          <span>QUANTITY OF ALL IN ONE PACKAGE (PHONE + READER + DATA PLAN): (required)</span>
          <input placeholder="Enter the number of readers you would like" type="text" maxlength="10" required="required" name="allInOneQuantity" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "allInOneQuantity", String.class, false))%>"/>
        </label>
      </div>
      <div>
			<label>
				<span>FIRST NAME: (required)</span>
				<input placeholder="Please enter your first name" type="text" maxlength="50" required="required" name="first" value="<%=StringUtils.prepareCDATA(form.getStringSafely("first", user.getFirstName()))%>"/>
			</label>
		</div>
		<div>
			<label>
				<span>LAST NAME: (required)</span>
				<input placeholder="Please enter your last name" type="text" maxlength="50" required="required" name="last" value="<%=StringUtils.prepareCDATA(form.getStringSafely("last", user.getLastName()))%>"/>
			</label>
		</div>
		<div>
			<label>
				<span>EMAIL: (required)</span>
				<input placeholder="Please enter your email address" type="text" maxlength="100" required="required" name="email" value="<%=StringUtils.prepareCDATA(form.getStringSafely("email", user.getEmail()))%>"/>
			</label>
		</div>
		<span>All orders are shipped via UPS Ground. ePort Mobile monthly service fee of $6.95 (card reader only plan) will begin on or around 12/30/14. See Terms and Conditions for details and all in one package rates.</span>
        <p>By submitting this form, I acknowledge that I have read and agree to the <a href="/USAT_ePort-Mobile_Addendum.pdf">Terms and Conditions</a> (PDF).</p>
		<div>
			<button name="submit" type="submit" id="contact-submit" value="optIn">SUBMIT</button>
		</div>
		</form>
		<!-- /form -->
		<div class="slogan">
         <p>
         One company, one point of contact, one call.</p>
         </div>  <!-- /slogan -->
         <div class="footer">
         <a href="http://www.usatech.com"><img src="/images/logo_usatech.jpg" alt="USA Technologies" /></a>
         <p>
         800.633.0340  |  <a href="http://www.usatech.com">www.usatech.com</a>
         </div>  <!-- /footer -->
 		</div>  <!-- /main  -->
    </div> <!-- /wrapper -->

<jsp:include page="include/footer.jsp"/>
