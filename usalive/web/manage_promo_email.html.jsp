<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="simple.servlet.CalendarHelper"%>
<%@page import="java.util.Calendar"%>
<%@page import="simple.io.Log"%>
<%@page import="com.usatech.usalive.web.CampaignUsaliveUtils"%>
<%@page import="javax.mail.MessagingException"%>
<%@page import="com.usatech.layers.common.util.WebHelper"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="com.usatech.usalive.servlet.USALiveUtils"%>
<%@page import="simple.servlet.ResponseUtils"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>

<%
	Log log = Log.getLog();
	UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
	long campaignId = ConvertUtils.getInt(RequestUtils.getAttribute(request, "campaignId", true));
	Results campaignResults=DataLayerMgr.executeQuery("GET_CAMPAIGN_NAME", new Object[]{campaignId});
	String campaignName =null;
	String mode = "finished";
	if(campaignResults.next()){
		campaignName=campaignResults.getFormattedValue("campaignName");
		if (campaignResults.getFormattedValue("campaignState").equals("STARTED")) {mode = "started";}
		
	}
	String backToManageCampaignLink=null;
	boolean hasPri=false;
	String userEmail=user.getEmail();
	String email=ConvertUtils.getStringSafely(RequestUtils.getAttribute(request, "email", false), userEmail);
	boolean userEmailValid=true;
	if(!StringUtils.isBlank(userEmail)){
		try{
			WebHelper.checkEmail(userEmail);
		}catch(MessagingException e){
			userEmailValid=false;
		}
	}
	if(user.isInternal()){
		if(user.hasPrivilege(ReportingPrivilege.PRIV_CUSTOMER_SERVICE)){
			hasPri=true;
		}
		backToManageCampaignLink="manage_campaign_internal.i?campaignId="+campaignId;
	}else{
		if(user.hasPrivilege(ReportingPrivilege.PRIV_MANAGE_CAMPAIGN)){
			hasPri=true;
		}
		backToManageCampaignLink="manage_campaign.i?campaignId="+campaignId;
	}
	Results camPromoTextResult=null;
	Results scheduleResult=null;
	Results timeZoneResult=null;
	Calendar calendar=Calendar.getInstance();
	String monthOption=null,dayOption=null,hourOption=null, minOption=null;
	int year=-1;
	
	String promoTextStatusConfigured="A";
	SimpleDateFormat dateFormat=new SimpleDateFormat("MMMM dd,yyyy hh:mm a");
	if(hasPri){
		InputForm inputForm = RequestUtils.getInputForm(request);
		String actionType=inputForm.getString("actionType", false);
		CalendarHelper calHelper=RequestUtils.getCalendarHelper(request, null, null);
		year=ConvertUtils.getIntSafely(inputForm.get("year"),calendar.get(Calendar.YEAR));
		int month=ConvertUtils.getIntSafely(inputForm.get("month"),calendar.get(Calendar.MONTH));
		int day=ConvertUtils.getIntSafely(inputForm.get("day"),calendar.get(Calendar.DAY_OF_MONTH));
		int hour=ConvertUtils.getIntSafely(inputForm.get("hour"),calendar.get(Calendar.HOUR_OF_DAY));
		int minute=ConvertUtils.getIntSafely(inputForm.get("minute"),calendar.get(Calendar.MINUTE));
		monthOption=WebHelper.generateMonthOptions("month", "month", month, calHelper.getMonthNames());
		dayOption=WebHelper.generateDayOrMinOptions("day", "day", 31,day);
		hourOption=WebHelper.generateHourOptions("hour", "hour", hour,true);
		minOption=WebHelper.generateDayOrMinOptions("minute", "minute", 59, minute);
		camPromoTextResult = DataLayerMgr.executeQuery("GET_CAM_PROMO_TEXT", new Object[]{campaignId});
		scheduleResult=DataLayerMgr.executeQuery("GET_CAMPAIGN_BLAST_SCHEDULE", new Object[]{campaignId});
		//scheduleResult.setFormat("sendTime", "DATE:MMMM dd,yyyy hh:mm a");
		scheduleResult.setFormat("consumerRegisteredDate", "DATE:MMMM dd,yyyy z");
		
		HashMap<String, String> settings=new HashMap<String, String>();
		settings.put("code","CAMPAIGN_CUSTOM_TEXT_REQUIRE_APPROVAL");					
		DataLayerMgr.selectInto("GET_APP_SETTING", settings);
		if(!StringUtils.isBlank(settings.get("value"))&&settings.get("value").equals("N")){
			promoTextStatusConfigured="A";
		}else{
			promoTextStatusConfigured="N";
		}
		
		timeZoneResult= DataLayerMgr.executeQuery("GET_BLAST_TIMEZONES",null);
		
	}
%>
<jsp:include page="include/header.jsp"/>
<%if(hasPri){ %>
<script type="text/javascript">
window.addEvent('domready', function() { 
	new Form.Validator.Inline.Mask(document.promoEmailForm);
});

function backToManageCampaign(){
	location.href="<%=backToManageCampaignLink%>";
}


function onSendCampaignEmails() {
	if(confirm("Please confirm that you want to send a promotional email to prepaid card holders about this campaign!")){
		$("actionType").value="send";
		$("promoEmailForm").submit();
	}
}

function onDeleteCustomText() {
	if(confirm("Please confirm that you want to delete the custom text for campaign email")){
		$("actionType").value="deleteText";
		$("promoEmailForm").submit();
	}
}

function onAllUserChange(isAllUser){
	if(isAllUser==0){
		$("consumerRegisteredDate").set("data-validators","required validate-date dateFormat:'mm/dd/yyyy'");
	}else{
		$("consumerRegisteredDate").set("data-validators","validate-date dateFormat:'mm/dd/yyyy'");
	}
}

</script>
<div class="caption">Manage Campaign Email</div>
<form name="promoEmailForm" id="promoEmailForm" action="promo_email.html"> 
<div class="selectSection">
<div class="sectionTitle">Test Campaign Email</div>
<input type="hidden" id="campaignId" name="campaignId" value="<%=campaignId%>"/>
<input type="hidden" id="campaignName" name="campaignName" value="<%=StringUtils.prepareCDATA(campaignName)%>"/>
<input type="hidden" id="actionType" name="actionType" value="send"/>
<input type="hidden" id="promoTextStatus" name="promoTextStatus" value="<%=StringUtils.prepareCDATA(promoTextStatusConfigured)%>"/>
<input type="hidden" id="campaignBlastId" name="campaignBlastId" value=""/>
<%
ResponseUtils.writeXsfrFormProtection(pageContext);
USALiveUtils.writeFormProfileId(pageContext);
%>
<div class="sectionRow">
<table class="input-rows">
<tr>
<td width="25%">
Campaign:
</td>
<td>
<%=StringUtils.prepareHTML(campaignName) %>
</td>
</tr>
<tr>
<td>
Email to receive test campaign content:<span class="required">*</span>
</td>
<td>
<input id="email" type="text" size="50" maxlength="255" data-validators="validate-email" required="required" value="<%=StringUtils.prepareCDATA(email)%>" name="email">
</td>
</tr>
<%if(StringUtils.isBlank(userEmail)){ %>
<tr>
<td>
<div class="status-info-failure" style="display:inline">Your user default email is blank. Please enter a valid email address above to receive the approved Campaign Email if you have custom text.</div>
</td>
<td>
<input id="updateUserEmailButton" type="submit" value="Update email" onclick="$('actionType').value='updateEmail';">
</td>
</tr>
<%}else if(!userEmailValid){ %>
<tr>
<td>
<div class="status-info-failure" style="display:inline">Your user default email <%=StringUtils.prepareHTML(userEmail) %> is invalid. Please enter a valid email address above to receive the approved Campaign Email if you have custom text.</div>
</td>
<td>
<input id="updateUserEmailButton" type="submit" value="Update email" onclick="$('actionType').value='updateEmail';">
</td>
</tr>
<%} %>
<tr>
<td>
<div align="center" style="float: center">
<input id="testPromoEmailButton" type="submit" value="Send test campaign email" onclick="$('actionType').value='testsend';">
<input type="button" value="Back To Manage Campaign" onclick="backToManageCampaign();"/>
</div>
</td>
</tr>
</table>
</div>
</div>
<hr/>
<div class="selectSection">
<div class="sectionTitle">Campaign Email Custom Text</div>
<div class="sectionRow">
<table class="input-rows">
<%if(camPromoTextResult.next()){
	String promoTextStatus=camPromoTextResult.getValue("status", String.class);
%>
<tr>
<td>Status:</td>
<td><%=StringUtils.prepareHTML(promoTextStatus) %> <%if(!promoTextStatus.equals("Approved")){%>(Please note that the email blast <div class="status-info-failure" style="display:inline">will not be sent</div> with Custom Text until it has been approved by USA Technologies Marketing Department
)<%} else{ if(promoTextStatusConfigured.equals("N")){%><div class="status-info-success" style="display:inline">(any additional change will be resubmitted through the approval process)</div><%}} %></td>
</tr>
<tr>
<td>
Custom Text:<span class="required">*</span>
</td>
<td>
<textarea data-validators="maxLength:4000" id="promoText" style="width: 100%; resize: none;" rows="20" name="promoText">
<%=StringUtils.prepareCDATA(camPromoTextResult.getValue("promoText", String.class))%>
</textarea>
</td>
</tr>
<tr>
<td colspan="2">
<div align="center" style="float: center">
<input id="editText" type="submit" value="Save" onclick="$('actionType').value='editText';"> &nbsp;&nbsp;&nbsp;&nbsp;
<input id="deleteText" type="button" value="Delete" onclick="onDeleteCustomText();"> &nbsp;&nbsp;&nbsp;&nbsp;
<% if(promoTextStatusConfigured.equals("N")){ %>
<input id="approveButton" type="submit" value="Submit For Approval" onclick="$('actionType').value='approveRequest';">
<%} %>
</div>
</td>
</tr>
<%} else{ %>
<tr>
<td>
Custom Text:
</td>
<td>
<textarea id="promoText" style="width: 100%; resize: none;" rows="20" name="promoText"></textarea>
</td>
</tr>
<tr>
<td colspan="2">
<input id="addText" type="submit" value="Add Text" onclick="$('actionType').value='addText';">
</td>
</tr>
<%} %>
</table>
</div>
</div>
<hr/>
<div class="selectSection">
<div class="sectionTitle">Schedule campaign email blast</div>
<div class="sectionRow">
<table class="input-rows">
<tr>
<td>
<%=monthOption%>
<%=dayOption%>
<select name="year" id="year">
<option <%if(calendar.get(Calendar.YEAR)==year){ %>selected="selected" <%} %> value="<%=StringUtils.prepareCDATA(String.valueOf(calendar.get(Calendar.YEAR)))%>"><%=StringUtils.prepareHTML(String.valueOf(calendar.get(Calendar.YEAR)))%></option>
<option <%if(calendar.get(Calendar.YEAR)+1==year){ %>selected="selected" <%} %> value="<%=StringUtils.prepareCDATA(String.valueOf(calendar.get(Calendar.YEAR)+1))%>"><%=StringUtils.prepareHTML(String.valueOf(calendar.get(Calendar.YEAR)+1))%></option>
</select>
Hour:<%=hourOption%>
Minute:<%=minOption%>
TimeZone:<select name="timeZoneGuid" id="timeZoneGuid">
<%while(timeZoneResult.next()){ %>
<option value="<%=StringUtils.prepareCDATA(timeZoneResult.getFormattedValue("timeZoneGuid"))%>"
<%if(timeZoneResult.getFormattedValue("timeZoneGuid").equals("US/Eastern")){%>selected="selected"<%}%>>
<%=StringUtils.prepareHTML(timeZoneResult.getFormattedValue("timeZoneGuid"))%></option>
<%} %>
</select>
</td>
</tr>
<tr>
<td>
<tr>
<td>
<input id="isForAllUserY" type="radio" value="Y" name="isForAllUser"  checked="checked" onClick="onAllUserChange(1);">To All Users
<input id="isForAllUserN" type="radio" value="N" name="isForAllUser" onClick="onAllUserChange(0);">To New Users Registered After Date:
<input id="consumerRegisteredDate" type="text" data-format="%m/%d/%Y" data-toggle="calendar" data-validators="validate-date dateFormat:'mm/dd/yyyy'" size="10" name="consumerRegisteredDate">
<input id="sendCampaignEmailsButton" type="button" onclick="onSendCampaignEmails();" value="Send Promotional Emails" <%if (mode.equals("finished")) {%>disabled="true"<%} %>>
</td>
</tr>
</table>
</div>
</div>
<hr/>
<b>Previously scheduled campaign blast:</b>
<table class="folio">
<% int count=0;
String blastStatus=null;
String toUser=null;
while(scheduleResult.next()){
	blastStatus=scheduleResult.getFormattedValue("status");
	toUser=scheduleResult.getFormattedValue("consumerRegisteredDate");
	if(StringUtils.isBlank(toUser)){
		toUser="All Users";
	}else{
		toUser="New Users Registered After "+toUser;
	}
	Date sendTimeDate=scheduleResult.getValue("sendTime", Date.class);
	//System.out.println(sendTimeDate);
	dateFormat.setTimeZone(TimeZone.getTimeZone(scheduleResult.getFormattedValue("timeZoneGuid")));
	//Date sendTime=new Date(ConvertUtils.getLocalTime(date.getTime(), scheduleResult.getFormattedValue("timeZoneGuid")));
if(count++==0){%>
<tbody>
<tr class="headerRow">
<th>Send Time</th><th>By User</th><th>Send To</th><th>Status</th>
</tr>
</tbody>
<%} %>
<tbody>
<tr class="<%if(count%2==0){ %>evenRowHover<%}else{%>oddRowHover<%}%>">
<td>
<%=StringUtils.prepareHTML(dateFormat.format(sendTimeDate))%> <%=StringUtils.prepareHTML(scheduleResult.getFormattedValue("timeZoneGuid"))%>
</td>
<td>
<%=StringUtils.prepareHTML(scheduleResult.getFormattedValue("userName"))%>
</td>
<td>
<%=StringUtils.prepareHTML(toUser)%>
</td>
<td>
<% if (blastStatus.equals("S")){%>
Sent
<%}else if (blastStatus.equals("D")){ %>
Cancelled
<%}else{ %>
<input id="cancelCampaignBlast" type="submit" value="Cancel" onclick="$('actionType').value='cancelBlast';$('campaignBlastId').value='<%=StringUtils.prepareCDATA(scheduleResult.getFormattedValue("campaignBlastId"))%>'">
<%} %>
</td>
</tr>
</tbody>
<%} 
if(count==0){
%>
<tr class="oddRowHover">
<td>
None
</td>
</tr>
<%} %>
</table>
</form>

<%}else{%>
	<p class="not-authorized">User '<%=StringUtils.prepareHTML(user.getUserName()) %>' does not have permission to view this page. Please contact USA Technologies if you need access.</p>
<%}%>
<jsp:include page="include/footer.jsp"/>