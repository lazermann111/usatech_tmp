<%@page import="com.usatech.usalive.servlet.USALiveRecordRequestFilter"%>
<%@page import="simple.servlet.RecordRequestFilter.CallInputs"%>
<%@page import="com.usatech.usalive.servlet.USALiveUtils"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.math.RoundingMode"%>
<%@page import="com.usatech.layers.common.util.WebHelper"%>
<%@page import="simple.xml.sax.MessagesInputSource"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.usatech.usalive.web.PrepaidUtils"%>
<%@page import="com.usatech.report.ReportingUser.ActionType"%>
<%@page import="com.usatech.report.ReportingUser.ResourceType"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.GenerateUtils"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="simple.db.Call.Sorter"%>
<%@page import="simple.io.Log"%>
<%@page import="com.usatech.ec2.EC2ClientUtils"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%@page import="simple.servlet.ResponseUtils"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.Date"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="simple.text.StringUtils"%>
<%
	Log log = Log.getLog();
	UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
	InputForm inputForm = RequestUtils.getInputForm(request);
	int consumerAcctId=inputForm.getInt("consumerAcctId", true, -1);
	boolean hasPri=false;
	if(user.isInternal()||user.hasPrivilege(ReportingPrivilege.PRIV_MANAGE_PREPAID_CONSUMERS)){
		hasPri=true;
	}else{
		return;
	}
	String actionType = RequestUtils.getAttribute(request, "actionType", String.class, false);
	if(actionType!=null){
		long startTsMs = System.currentTimeMillis();
		user.checkPermission(ResourceType.PREPAID_CONSUMER_ACCT, ActionType.EDIT, consumerAcctId);
		MessagesInputSource messages=RequestUtils.getOrCreateMessagesSource(request);
		if(actionType.equals("toggleStatus")){
			int updateCount=DataLayerMgr.executeUpdate("UPDATE_CONSUMER_ACCT_STATUS", inputForm, true);
			if(updateCount==1){
				messages.addMessage("success", "usalive-prepaid-status-toggle-success", "Consumer account active status is toggled.");
				RequestUtils.redirectWithCarryOver(request, response, "consumer_acct.html?consumerAcctId="+consumerAcctId, false, true);
			}else{
				messages.addMessage("error", "usalive-prepaid-status-toggle-error", "You do not have the permission to toggle consumer account active status. Please contact customer service for assistance");
				RequestUtils.redirectWithCarryOver(request, response, "consumer_acct.html?consumerAcctId="+consumerAcctId, false, true);
			}
		}else if(actionType.equals("toggleNegativeBalance")){
			int updateCount=DataLayerMgr.executeUpdate("UPDATE_CONSUMER_ACCT_ALLOW_NEGATIVE", inputForm, true);
			if(updateCount==1){
				messages.addMessage("success", "usalive-prepaid-allownegative-toggle-success", "Consumer account \"Allow Negative Balance\" is toggled");
				RequestUtils.redirectWithCarryOver(request, response, "consumer_acct.html?consumerAcctId="+consumerAcctId, false, true);
			}else{
				messages.addMessage("error", "usalive-prepaid-allownegative-toggle-error", "You do not have the permission to toggle \"Allow Negative Balance\". Please contact customer service for assistance");
				RequestUtils.redirectWithCarryOver(request, response, "consumer_acct.html?consumerAcctId="+consumerAcctId, false, true);
			}
		}else if(actionType.equals("closeAccount")){
			DataLayerMgr.executeCall("CLOSE_CONSUMER_ACCT", new Object[] {consumerAcctId, user.getEmailAddress()}, true);
			messages.addMessage("success", "usalive-prepaid-close-account-success", "Consumer account is closed");
			RequestUtils.redirectWithCarryOver(request, response, "consumer_acct.html?consumerAcctId="+consumerAcctId, false, false);
		}else if(actionType.equals("increaseCardAmount")){
			int consumerAcctTypeId = inputForm.getInt("consumerAcctTypeId", true, -1);
			String reason=inputForm.getString("increaseReason", false);
			if(!StringUtils.isBlank(reason)){
				reason=reason.trim();
			}
			BigDecimal amount = RequestUtils.getAttribute(request, "increaseAmount", BigDecimal.class, true);
			amount=amount.setScale(2, RoundingMode.HALF_UP);
			if (consumerAcctTypeId == 7) {
				Map<String, Object> increaseParams = new HashMap<>();
				increaseParams.put("consumerAcctId", consumerAcctId);
				increaseParams.put("simple.servlet.ServletUser.userId", user.getUserId());
				increaseParams.put("amount", amount);
				increaseParams.put("reason", reason);
				DataLayerMgr.executeCall("INCREASE_PAYROLL_DEDUCT_ACCT", increaseParams, true);
				messages.addMessage("success", "usalive-payroll-deduct-acct-increase-successful", "Increased the balance on this account by {0,CURRENCY}.", amount);
				WebHelper.publishAppRequestRecord(USALiveUtils.APP_CD, request, new USALiveRecordRequestFilter(), new CallInputs(), startTsMs, "Increase Payroll Deduct Balance", "consumer_acct", String.valueOf(consumerAcctId), increaseParams, null, log);
			} else {
				PrepaidUtils.increasePrepaidBalance(consumerAcctId, amount, reason, user, messages, WebHelper.getDefaultPublisher());
			}
			RequestUtils.redirectWithCarryOver(request, response, "consumer_acct.html?consumerAcctId="+consumerAcctId, false, true);
		}else if(actionType.equals("decreaseCardAmount")){
			int consumerAcctTypeId = inputForm.getInt("consumerAcctTypeId", true, -1);
			String reason=inputForm.getString("decreaseReason", false);
			if(!StringUtils.isBlank(reason)){
				reason=reason.trim();
			}
			BigDecimal amount = RequestUtils.getAttribute(request, "decreaseAmount", BigDecimal.class, true);
			amount=amount.setScale(2, RoundingMode.HALF_UP);
			if (consumerAcctTypeId == 7) {
				Map<String, Object> decreaseParams = new HashMap<>();
				decreaseParams.put("consumerAcctId", consumerAcctId);
				decreaseParams.put("simple.servlet.ServletUser.userId", user.getUserId());
				decreaseParams.put("amount", amount);
				decreaseParams.put("reason", reason);
				DataLayerMgr.executeCall("DECREASE_PAYROLL_DEDUCT_ACCT", decreaseParams, true);
				messages.addMessage("success", "usalive-payroll-deduct-acct-decrease-successful", "Decreased the balance on this account by {0,CURRENCY}.", amount);
				WebHelper.publishAppRequestRecord(USALiveUtils.APP_CD, request, new USALiveRecordRequestFilter(), new CallInputs(), startTsMs, "Decrease Payroll Deduct Balance", "consumer_acct", String.valueOf(consumerAcctId), decreaseParams, null, log);
			} else {
				PrepaidUtils.decreasePrepaidBalance(consumerAcctId, amount, reason, user, messages, WebHelper.getDefaultPublisher());
			}
			RequestUtils.redirectWithCarryOver(request, response, "consumer_acct.html?consumerAcctId="+consumerAcctId, false, true);
		}else if(actionType.equals("balanceTransfer")){
			int toConsumerAcctId=inputForm.getInt("toConsumerAcctId", true, -1);
			user.checkPermission(ResourceType.PREPAID_CONSUMER_ACCT, ActionType.EDIT, toConsumerAcctId);
			int consumerId=inputForm.getInt("consumerId", true, -1);
			String reason=inputForm.getString("balanceTransferReason", false);
			String closeFromCard=inputForm.getStringSafely("closeFromCard", "N");
			if(!StringUtils.isBlank(reason)){
				reason=reason.trim();
			}
			Results result=DataLayerMgr.executeQuery("GET_ACTIVE_CONSUMER_CARDS_OPER_SERVICED", inputForm);
			BigDecimal amount=null;
			if(result.next()){
				amount = ConvertUtils.convertRequired(BigDecimal.class, result.getValue("cardBalance"));
				amount=amount.setScale(2, RoundingMode.HALF_UP);
				boolean decreaseResult=PrepaidUtils.decreasePrepaidBalance(consumerAcctId, amount, reason, user, messages, WebHelper.getDefaultPublisher(), "From card: ");
				if(decreaseResult){
					boolean increaseResult=PrepaidUtils.increasePrepaidBalance(toConsumerAcctId, amount, reason, user, messages, WebHelper.getDefaultPublisher(), "To card: ");
					if(!increaseResult){
						messages.addMessage("error", "usalive-prepaid-bt-increase-failed", "Decrease succeeded but increase on the target card failed. Please contact customer service for assistance.");
					}else if(closeFromCard.equals("Y")){
						DataLayerMgr.executeCall("BT_CLOSE_CONSUMER_ACCT", new Object[] {consumerAcctId, user.getEmailAddress()}, true);
						messages.addMessage("success", "usalive-prepaid-close-account-success", "From card account is closed");
					}
				}
			}else{
				messages.addMessage("error", "usalive-prepaid-bt-amount-notfound", "Failed to lookup from card balance. Please contact customer service for assistance.");
			}
			log.info("Transfer balance from consumerAcctId="+consumerAcctId+" toConsumerAcctId="+toConsumerAcctId+" closeFromCard="+closeFromCard+" amount="+amount+" reason="+reason);
			RequestUtils.redirectWithCarryOver(request, response, "consumer.html?consumerId="+consumerId, false, true);
		} else if (actionType.equals("updateTopUpAmt")) {
			BigDecimal topUpAmt = inputForm.getBigDecimal("topUpAmt", true);
			if (topUpAmt.compareTo(BigDecimal.ZERO) < 0 || topUpAmt.compareTo(BigDecimal.valueOf(1000)) > 0) {
				messages.addMessage("error", "usalive-invalid-top-up-amt", "Invalid top-top amount, value must be $0 to $1000.");
			} else {
				DataLayerMgr.executeCall("UPDATE_TOP_UP_AMT", new Object[] {consumerAcctId, user.getUserId(), topUpAmt}, true);
				messages.addMessage("success", "usalive-top-up-update-success", "Periodic spending limit updated.");
			}
			
			RequestUtils.redirectWithCarryOver(request, response, "consumer_acct.html?consumerAcctId="+consumerAcctId, false, true);
		}
			
	}
	
%>

