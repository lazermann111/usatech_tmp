<%@page import="simple.results.Results"%>
<%@page import="java.text.Format"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
Map<String,Object> params = new HashMap<String,Object>();
params.put("simple.servlet.ServletUser", user);
StringBuilder detailsLink = new StringBuilder();
detailsLink.append("run_report_async.i?folioId=1584&amp;profileId=").append(user.getUserId());
Results results = DataLayerMgr.executeQuery("DASHBOARD_DEVICE_HEALTH", params);
Map<String,Number> counts = new HashMap<String,Number>();
int total = 0;
Integer noCommDaysMin = null;
Integer noCashlessDaysMin = null;
while(results.next()) {
	if(results.isGroupBeginning(0)) {
		noCommDaysMin = results.getValue("noCommDaysMin", Integer.class);
		noCashlessDaysMin = results.getValue("noCashlessDaysMin", Integer.class);  
	}
	Number n = results.getValue("count", Number.class);
	counts.put(results.getValue("classification", String.class), n);
	if(n != null)
		total += n.intValue();
}
Format countFormat = ConvertUtils.getFormat("NUMBER"); 
%>
<div class="title2">Device Health</div>
<table class="dashboard-chart">
<tr><th>Device Status</th><th>Count</th></tr>
<tr><td title="All devices in your view">Total Devices</td><td class="dashboard-count"><a href="<%=detailsLink.toString() %>&amp;params.ShowAll=1&amp;params.HealthClassification=-" title="Click to view the full Device Health Report"><%=StringUtils.prepareHTML(ConvertUtils.formatObject(total, countFormat)) %></a></td></tr>
<tr><td title="Active Devices that have commuicated and transacted recently">Active Devices</td><td class="dashboard-count"><a href="<%=detailsLink.toString() %>&amp;params.HealthClassification=ACTIVE" title="Click to view details"><%=StringUtils.prepareHTML(ConvertUtils.formatObject(counts.get("ACTIVE"), countFormat)) %></a></td></tr>
<tr><td title="Devices that have never called in">Inventory (Never Called In)</td><td class="dashboard-count"><a href="<%=detailsLink.toString() %>&amp;params.HealthClassification=INVENTORY" title="Click to view details"><%=StringUtils.prepareHTML(ConvertUtils.formatObject(counts.get("INVENTORY"), countFormat)) %></a></td></tr>
<tr><td title="Rental devices that are not communicating but are being assessed a monthly fee">Inactive Devices (Rentals)</td><td class="dashboard-count"><a href="<%=detailsLink.toString() %>&amp;params.HealthClassification=INACTIVE_RENTAL" title="Click to view details"><%=StringUtils.prepareHTML(ConvertUtils.formatObject(counts.get("INACTIVE_RENTAL"), countFormat)) %></a></td></tr>
<tr><td title="Purchased devices that are not communicating but are being assessed a monthly fee">Inactive Devices (Owned)</td><td class="dashboard-count"><a href="<%=detailsLink.toString() %>&amp;params.HealthClassification=INACTIVE_OWNED" title="Click to view details"><%=StringUtils.prepareHTML(ConvertUtils.formatObject(counts.get("INACTIVE_OWNED"), countFormat)) %></a></td></tr>
<tr><td title="Inactive devices that are not being assessed a monthly fee">Inactive Devices (Other)</td><td class="dashboard-count"><a href="<%=detailsLink.toString() %>&amp;params.HealthClassification=INACTIVE" title="Click to view details"><%=StringUtils.prepareHTML(ConvertUtils.formatObject(counts.get("INACTIVE"), countFormat)) %></a></td></tr>
<tr><td>No Cashless in Last <%=StringUtils.prepareHTML(ConvertUtils.formatObject(noCashlessDaysMin, countFormat)) %> Days</td><td class="dashboard-count"><a href="<%=detailsLink.toString() %>&amp;params.HealthClassification=NO_CASHLESS" title="Click to view details"><%=StringUtils.prepareHTML(ConvertUtils.formatObject(counts.get("NO_CASHLESS"), countFormat)) %></a></td></tr>
<tr><td>No Communication in Last <%=StringUtils.prepareHTML(ConvertUtils.formatObject(noCommDaysMin, countFormat)) %> Days</td><td class="dashboard-count"><a href="<%=detailsLink.toString() %>&amp;params.HealthClassification=NO_COMM" title="Click to view details"><%=StringUtils.prepareHTML(ConvertUtils.formatObject(counts.get("NO_COMM"), countFormat)) %></a></td></tr>
</table>