<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.text.StringUtils"%>
<%
int rmaTypeId=ConvertUtils.getIntSafely(RequestUtils.getAttribute(request, "rmaTypeId", false),-1);
String needReplacement=ConvertUtils.getStringSafely(RequestUtils.getAttribute(request, "needReplacement", false), "N");
%>
<script type="text/javascript">
function onAddNewCarrier(){
	$("editRMAMessage").set("html","");
	if($("carrierAccountNumber").get("class")=="validation-failed"){
		if($("advice-validate-regex-nospace-carrierAccountNumber")){
			$("advice-validate-regex-nospace-carrierAccountNumber").set("html", "Error: Invalid account number");
			return;
		}
	}
	$("actionType").value="add";
	new Request.HTML({ 
        url: "rma_edit_carrier.html",
        method: 'post',
        data: $("returnForm"), 
        async: false,
        evalScripts: true
    }).send();
	
	if($("carrierSelect").selectedIndex!=-1){
		$("upsGround").checked=false;
		$("ups2ndDay").checked=false;
		$("upsNextDayAir").checked=false;
		$("usatechSupport").checked=false;
		
		var el=$("carrierSelect").getSelected();
		if(el.get("data-carrier")=="UPS"){
			$("upsCarrier").checked="checked";
			$("fedexCarrier").checked="";
			$("dhlCarrier").checked="";
		
		}else if(el.get("data-carrier")=="FEDEX"){
			$("upsCarrier").checked="";
			$("fedexCarrier").checked="checked";
			$("dhlCarrier").checked="";

		}else{
			$("upsCarrier").checked="";
			$("fedexCarrier").checked="";
			$("dhlCarrier").checked="checked";
		}
	}
	
}

function onClickCarrier(){
	$("carrierAccountNumber").value="";
	$("carrierSelect").selectedIndex=-1;
	$("editRMAMessage").set("html","");

}

function onDeleteCarrier(){
	if($("carrierSelect").selectedIndex==-1){
		alert("Please select the customer shipping carrier.");
		return;
	}
	$("actionType").value="delete";
	new Request.HTML({ 
        url: "rma_edit_carrier.html",
        method: 'post',
        data: $("returnForm"), 
        evalScripts: true,
        async: false
    }).send();
	if($("carrierSelect").getElements("option").length==0){
		$("upsGround").checked="true";
	}
}

function editNewCarrierResult(resultType, message, rmaShippingCarrierId, carrier, carrierAccountNumber){
	if(resultType=='add'){
		$("editRMAMessage").set("html","<p class='status-info-success'>Successfully added customer carrier account.</p>");
		var newOption = new Element('option', {
    	    value: rmaShippingCarrierId,
    	    "data-carrier": carrier,
    	    "data-carrier-account": carrierAccountNumber,
    	    text: carrier+' '+carrierAccountNumber,
    	    selected: "selected"
    	});
		$("carrierSelect").adopt(newOption);
	}else if(resultType=='delete'){
		$("editRMAMessage").set("html","<p class='status-info-success'>Successfully deleted customer carrier account.</p>");
		$("carrierSelect").getSelected().dispose();
	}else{
		//error
		$("editRMAMessage").set("html","<p class='status-info-failure'>"+message+"</p>");
	}
}

function onCarrierChange(){
	var el=$("carrierSelect").getSelected();
	$("carrierAccountNumber").value=el.get("data-carrier-account");
	if(el.get("data-carrier")=="UPS"){
		$("upsCarrier").checked="checked";
		$("fedexCarrier").checked="";
		$("dhlCarrier").checked="";
	}else if(el.get("data-carrier")=="FEDEX"){
		$("upsCarrier").checked="";
		$("fedexCarrier").checked="checked";
		$("dhlCarrier").checked="";
	}else{
		$("upsCarrier").checked="";
		$("fedexCarrier").checked="";
		$("dhlCarrier").checked="checked";
	}
	$('returnForm').getElements("input[type=radio][name='ups'])").each(function(el) {
		el.checked="";
	});
	$("editRMAMessage").set("html","");
}

function onShippingChange(){
	new Request.HTML({ 
        url: "rma_shipping_address.html",
        method: 'post',
        data: $("returnForm"), 
        update : $("shippingAddrSection"),
        async: false,
        evalScripts: true
    }).send();
	postalCityStateChanged();
	enableShippingAddr(false);
	$("removeShippingAddressMsg").set("html","");
}

function onRemoveShippingAddress(rmaShippingAddrId){
	if(confirm("Are you sure that you want to remove this address?")){
		new Request.HTML({ 
	        url: "rma_remove_shipping_address.html",
	        method: 'post',
	        data: {'rmaShippingAddrId':rmaShippingAddrId}, 	       
	        async: false,
	        evalScripts: true
	    }).send();
		
		new Request.HTML({ 
	        url: "rma_shipping_address.html",
	        method: 'tet',
	        update: $("shippingAddrSection"), 	       
	        async: false
	    }).send();
		new Form.Validator.Inline.Mask(document.returnForm);
	}
}

function enableShippingAddr(enable){
	if(enable){
		$('shippingAddrSection').getElements("input[type=text]").each(function(el) {
			el.disabled="";
		});
		$('shippingAddrSection').getElements("select").each(function(el) {
			el.disabled="";
		});
		new Form.Validator.Inline.Mask(document.returnForm);
	}else{
		$('shippingAddrSection').getElements("input[type=text]").each(function(el) {
			el.disabled="true";
		});
		$('shippingAddrSection').getElements("select").each(function(el) {
			el.disabled="true";
		});
	}
}
function onSaveNewAddress(){
	if($('saveNewAddress').checked){
		$('rmaShippingAddrId').selectedIndex=-1;
		enableShippingAddr(true);
		$("shippingAddrName").required="required";
	}else{
		$("shippingAddrName").required="";
	}
}

function cleanAddress(){
	$('rmaShippingAddrId').selectedIndex=-1;
	
}

function onNeedReplacement(){
	if($("needReplacement").checked){
		$("replacementSection").style.display="block";
		$("replacementSection").getElements("input").each(function(el) {
			el.set("disabled",false);
		});
	}else{
		$("replacementSection").style.display="none";
		$("replacementSection").getElements("input").each(function(el) {
			el.set("disabled",true);
		});
	}
	
}

function onSelectCountry(countryCd){
	if(countryCd=='US'){
		$("usAllowed").style.display="block";
		$("caAllowed").style.display="none";
		$("upsGround").checked=true;
		$("phone").required=null;
	}else{//CA
		$("usAllowed").style.display="none";
		$("caAllowed").style.display="block";
		$("usatechSupport").checked=true;
		$("phone").required="required";
	}
}

function postalCityStateChanged(){
	var countryCd=$("countryCd").value;
	if(countryCd=='US'){
		if($("state").value=='AK'||$("state").value=='PR'||$("state").value=='HI'){
			$("usAllowed").style.display="none";
			$("caAllowed").style.display="block";
			$("usatechSupport").checked=true;
			$("phone").required="required";
		}else{
			$("usAllowed").style.display="block";
			$("caAllowed").style.display="none";
			$("upsGround").checked=true;
			$("phone").required=null;
		}
	}else{
		$("usAllowed").style.display="none";
		$("caAllowed").style.display="block";
		$("usatechSupport").checked=true;
		$("phone").required="required";
	}
}

</script>
<div class="selectSection">
<div class="sectionTitle"> Shipping Address:</div>
<div class="sectionRow">
<label class="itemLabel" for="mainSelect">Existing RMA Shipping Address:</label>
		<div id="rmaShippingAddr"></div>
		<div id="removeShippingAddressMsg"></div>
<div id="shippingAddrSection">
<jsp:include page="rma_shipping_address.html.jsp"/>
</div>
</div>
</div>
<hr/>
<%if(needReplacement.equals("Y")){%>
<div id="replacementChoiceSection">
<div id="replacementSection">
<input type="hidden" id="needReplacement" name="needReplacement" value="Y">
<%}else{ %>
<div id="replacementChoiceSection" style="display:none">
Advance Replacement Request:<input type="checkbox" id="needReplacement" name="needReplacement" value="Y" onClick="onNeedReplacement();">
<div id="replacementSection" style="display:none">
<%} %>
<p>G9 ePort Kit Advance Replacement Request Process: </p>
<p>
Once the replacement ePort Kit is received, please return the defective item(s) in the advance replacement kit received and use the emailed UPS Call Tag, based on the account referenced above.  If USAT doesn't receive the advance replacement ePort kit including the item(s) replaced within 30 days, the cost of the kit will be debited from your USALive EFT funds.
</p> 
<p>
For example, a complete G9 ePort kit is shipped and if the only item requiring replacement is the Card Reader then place the defective Card Reader in this kit and return everything else in this kit using the UPS Call Tag.
</p> 
<p>
When the returned kit is received, any defective items not covered under our warranty will be chargeable repairs.
</p> 
<div id="rmaReplacementQuantitySection">
How many G9 ePort Kits are you requesting: <input id="rmaReplacementQuantity" name="rmaReplacementQuantity" type="text" value="1" size="3" disabled="disabled" required="required" data-validators="minimum:1"/>
</div>
<p>Please select the shipping method for each replacement kit requested or provide your own shipping account number below:</p>
<div class="selectSection">
<div class="sectionTitle"> Shipping Carrier:</div>
<br/>
<br/>
<div class="sectionRow">
	<div id="usAllowed">
	<table>
	<tbody>
	<tr>
	<td>
		<input type="radio" id="upsGround" name="ups" value="1" onClick="onClickCarrier();" disabled="disabled" checked="checked"> UPS Ground $8.00 per kit
	</td>
	</tr>
	<tr>
	<td><input type="radio" id="ups2ndDay" name="ups" value="2" onClick="onClickCarrier();" disabled="disabled"> UPS 2nd Day Air $18.00 per kit</td>
	</tr>
	<tr>
	<td><input type="radio" id="upsNextDayAir" name="ups" value="3" onClick="onClickCarrier();" disabled="disabled"> UPS Next Day Air $30.00 per kit</td>
	</tr>
	</tbody>
	</table>
	</div>
	<div id="caAllowed">
	<table>
	<tbody>
	<tr>
	<td><input type="radio" id="usatechSupport" name="ups" value="0" onClick="onClickCarrier();" disabled="disabled"> Please call Customer Service 888-561-4748 and select option 1 twice for the shipping cost</td>
	</tr>
	</tbody>
	</table>
	</div>
	<br/>
	<table>
	<tbody>
	<tr>
	<td>
	<label class="itemLabel" for="mainSelect">Customer Shipping Carrier:</label>
		<div id="userShippingCarrier"></div>
	</td>
	</tr>
	<tr>
	<td>
	<input type="radio" id="upsCarrier" name="carrier" value="UPS" checked="checked" onClick="onClickCarrier();" disabled="disabled"> UPS
	<input type="radio" id="fedexCarrier" name="carrier"value="FEDEX" onClick="onClickCarrier();" disabled="disabled"> FEDEX
	<input type="radio" id="dhlCarrier" name="carrier"value="DHL" onClick="onClickCarrier();" disabled="disabled"> DHL
	Account #:<input type="text" id="carrierAccountNumber" name="carrierAccountNumber" value="" disabled="disabled" data-validators="validate-regex-nospace:'<%=StringUtils.prepareScript("^[A-Za-z0-9]{1,9}$")%>'"> 
	</td>
	</tr>
	<tr>
	<td><input id="addNewCarrier" type="button" onclick="onAddNewCarrier();" value="Add New Carrier Account"/>
	<input id="deleteCarrier" type="button" onclick="onDeleteCarrier();" value="Delete Carrier Account"/></td>
	</tr>
	<tr><td><div id="editRMAMessage"></div></td></tr>
	</tbody>
	</table>
	</div>
<hr/>
</div>
<hr/>
</div>
</div>