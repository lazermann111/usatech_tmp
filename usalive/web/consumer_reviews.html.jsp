<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.usatech.usalive.servlet.USALiveUtils"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="java.util.HashMap"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.results.Results"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="com.usatech.usalive.web.OrderUtils"%>
<%@page import="simple.servlet.GenerateUtils"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.usatech.usalive.device.DeviceConfigUtils"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.Map"%>

<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.text.StringUtils"%>

<%@page import="com.usatech.layers.common.constants.DeviceType"%>
<%@page import="com.usatech.layers.common.util.WebHelper"%>
<%
InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
UsaliveUser user = (UsaliveUser) RequestUtils.getUser(request);
long userId = user.getUserId();
long customerId = user.getCustomerId();
long totalReviewCount = 0;
long weightedReviewCount = 0;
String startDate = inputForm.getString("start_date", false);
if (StringUtils.isBlank(startDate))
	startDate = StringUtils.getDate(customerId > 0 ? -30 : -365);
String endDate = inputForm.getString("end_date", false);
if (StringUtils.isBlank(endDate))
	endDate = StringUtils.getDate(0);
Map<String, Object> params = new HashMap<String, Object>();
params.put("customerId", customerId);
params.put("userId", userId);
params.put("startDate", startDate);
params.put("endDate", endDate);
Results totals = DataLayerMgr.executeQuery("GET_SURVEY_RESPONSE_RATING_TOTALS", params);
Results totals2 = totals.clone();
while (totals2.next()) {
	totalReviewCount += totals2.getValue("starCount", int.class);
	weightedReviewCount += totals2.getValue("starCount", int.class) * totals2.getValue("star", int.class);
}
String filter = inputForm.getStringSafely("filter", "");
String filterValue = inputForm.getStringSafely("filter_value", "").trim();
DecimalFormat countFormat = new DecimalFormat("###,###,###,##0");
DecimalFormat starFormat = new DecimalFormat("#.#");
String queryString = StringUtils.prepareCDATA(new StringBuilder("start_date=").append(StringUtils.prepareURLPart(startDate)).append("&end_date=").append(StringUtils.prepareURLPart(endDate))
	.append("&filter=").append(StringUtils.prepareURLPart(filter)).append("&filter_value=").append(StringUtils.prepareURLPart(filterValue)).toString());
%>
<jsp:include page="include/header.jsp"/>

<style>
a {
text-decoration: none;
}
</style>

<div class="title2">Consumer Reviews</div>
<div class="spacer5"></div>

<table>
	<tr>
		<td valign="top">
			<span class="strong"><a href="consumer_reviews.html?<%=USALiveUtils.getQueryString(request)%>&<%=queryString%>"><%=countFormat.format(totalReviewCount)%> Reviews</a></span><br/>
			<%while (totals.next()) {%>			
			<a href="consumer_reviews.html?stars=<%=StringUtils.prepareCDATA(StringUtils.prepareURLPart(totals.getFormattedValue("star")))%>&<%=USALiveUtils.getQueryString(request)%>&<%=queryString%>"><%=StringUtils.prepareCDATA(totals.getFormattedValue("star"))%> star:</a> <span class="rating stars star<%=StringUtils.prepareCDATA(totals.getFormattedValue("star"))%>"></span>&nbsp;<span class="ratingActual" style="padding-left:<%=totalReviewCount > 0 ? Math.round(80 * totals.getValue("starCount", long.class) / totalReviewCount) : 0%>px;"></span><span class="ratingRemaining" style="padding-left:<%=80 - (totalReviewCount > 0 ? Math.round(80 * totals.getValue("starCount", long.class) / totalReviewCount) : 0)%>px;"></span>&nbsp;(<%=countFormat.format(totals.getValue("starCount", int.class)) %>)
			<div class="spacer2"></div>
			<%}%>
		</td>
		<td width="20px">&nbsp;</td>
		<td valign="top">
			<span class="strong">Average Consumer Rating</span>
			<div class="spacer3"></div>
			<span class="rating stars star<%=totalReviewCount > 0 ? Math.round(weightedReviewCount / totalReviewCount) : 0%>" title="<%=totalReviewCount > 0 ? starFormat.format((double) weightedReviewCount / totalReviewCount) : 0%> out of 5 stars"></span>
			<div class="spacer10"></div>
			<form method="get" action="consumer_reviews.html" onsubmit="return validateForm()">
				<%=USALiveUtils.getHiddenInputs(request)%>
				<span class="label">Start Date</span> 
				<input type="text" id="start_date" size="8" maxlength="10" name="start_date" value="<%=startDate %>" data-toggle="calendar" data-format="%m/%d/%Y"/> 
				&nbsp;<span class="label">End Date</span> 
				<input type="text" id="end_date" size="8" maxlength="10" name="end_date" value="<%=endDate %>" data-toggle="calendar" data-format="%m/%d/%Y"/> 
				<br/><span class="label">Filter by</span>
				<select name="filter">
					<option></option>
					<option value="address"<%if (filter.equalsIgnoreCase("address")) {%> selected="selected"<%}%>>Address</option>
					<option value="city"<%if (filter.equalsIgnoreCase("city")) {%> selected="selected"<%}%>>City</option>
					<option value="customer"<%if (filter.equalsIgnoreCase("customer")) {%> selected="selected"<%}%>>Customer</option>
					<option value="device"<%if (filter.equalsIgnoreCase("device")) {%> selected="selected"<%}%>>Device</option>
					<option value="location"<%if (filter.equalsIgnoreCase("location")) {%> selected="selected"<%}%>>Location</option>
					<option value="postal"<%if (filter.equalsIgnoreCase("postal")) {%> selected="selected"<%}%>>Postal</option>
					<option value="state"<%if (filter.equalsIgnoreCase("state")) {%> selected="selected"<%}%>>State</option>
					<option value="survey"<%if (filter.equalsIgnoreCase("survey")) {%> selected="selected"<%}%>>Survey</option>
				</select>
				<input type="text" name="filter_value" maxlength="500" value="<%=StringUtils.prepareCDATA(filterValue)%>" />
				<input type="submit" value="Submit" />
			</form>
		</td>
	</tr>
</table>

<script type="text/javascript">
var fromDate = document.getElementById("start_date");
var toDate = document.getElementById("end_date");

function validateForm() {
	if(!isDate(fromDate.value)) {
    	fromDate.focus();
    	return false;
	}
	if(!isDate(toDate.value)) {
    	toDate.focus();
    	return false;
	}
	return true;
}

</script>

<%
String sortField = PaginationUtil.getSortField(null);
String sortIndex = inputForm.getString(sortField, false);
sortIndex = (sortIndex == null || sortIndex.trim().equals("")) ? "-2" : sortIndex;
%>

<div class="spacer10"></div>

<table class="tabDataDisplayBorder">
	<tr class="gridHeader">
		<td>
			<a href="<%=PaginationUtil.getSortingScript(null, "1", sortIndex)%>">Rating</a>
			<%=PaginationUtil.getSortingIconHtml("1", sortIndex)%>
		</td>
		<td>
			<a href="<%=PaginationUtil.getSortingScript(null, "2", sortIndex)%>">Review Date</a>
			<%=PaginationUtil.getSortingIconHtml("2", sortIndex)%>
		</td>
		<td>
			<a href="<%=PaginationUtil.getSortingScript(null, "3", sortIndex)%>">Survey</a>
			<%=PaginationUtil.getSortingIconHtml("3", sortIndex)%>
		</td>
		<td>
			<a href="<%=PaginationUtil.getSortingScript(null, "12", sortIndex)%>">Customer</a>
			<%=PaginationUtil.getSortingIconHtml("12", sortIndex)%>
		</td>
		<td>
			<a href="<%=PaginationUtil.getSortingScript(null, "5", sortIndex)%>">Location</a>
			<%=PaginationUtil.getSortingIconHtml("5", sortIndex)%>
		</td>
		<td>
			<a href="<%=PaginationUtil.getSortingScript(null, "4", sortIndex)%>">Device</a>
			<%=PaginationUtil.getSortingIconHtml("4", sortIndex)%>
		</td>
		<td>
			<a href="<%=PaginationUtil.getSortingScript(null, "6", sortIndex)%>">Address</a>
			<%=PaginationUtil.getSortingIconHtml("6", sortIndex)%>
		</td>
		<td>
			<a href="<%=PaginationUtil.getSortingScript(null, "7", sortIndex)%>">City</a>
			<%=PaginationUtil.getSortingIconHtml("7", sortIndex)%>
		</td>
		<td>
			<a href="<%=PaginationUtil.getSortingScript(null, "8", sortIndex)%>">State</a>
			<%=PaginationUtil.getSortingIconHtml("8", sortIndex)%>
		</td>
		<td>
			<a href="<%=PaginationUtil.getSortingScript(null, "9", sortIndex)%>">Postal</a>
			<%=PaginationUtil.getSortingIconHtml("9", sortIndex)%>
		</td>
		<td>
			<a href="<%=PaginationUtil.getSortingScript(null, "10", sortIndex)%>">Country</a>
			<%=PaginationUtil.getSortingIconHtml("10", sortIndex)%>
		</td>
		<td>
			<a href="<%=PaginationUtil.getSortingScript(null, "11", sortIndex)%>">Email</a>
			<%=PaginationUtil.getSortingIconHtml("11", sortIndex)%>
		</td>
	</tr>
	<% 
	if (totalReviewCount > 0) {
	String sqlStart = new StringBuilder("SELECT SQR.SURVEY_QUESTION_RESPONSE_VALUE, TO_CHAR(SR.CREATED_TS, 'MM/DD/YYYY HH24:MI:SS') CREATED_TS, S.SURVEY_NAME, E.EPORT_SERIAL_NUM, L.LOCATION_NAME")
			.append(", TA.ADDRESS1, TA.CITY, TA.STATE, TA.ZIP, C.COUNTRY_NAME, SR.CONSUMER_EMAIL_ADDR, CUS.CUSTOMER_NAME, SR.SURVEY_RESPONSE_ID, SR.ANSWER_COUNT ").toString();
	
	String[] sortFields = { "SQR.SURVEY_QUESTION_RESPONSE_VALUE", "SR.CREATED_TS", "S.SURVEY_NAME", "E.EPORT_SERIAL_NUM", "L.LOCATION_NAME",
		"TA.ADDRESS1", "TA.CITY", "TA.STATE", "TA.ZIP", "C.COUNTRY_NAME", "SR.CONSUMER_EMAIL_ADDR", "CUS.CUSTOMER_NAME" };
	
	String sqlBase = new StringBuilder("FROM REPORT.SURVEY_RESPONSE SR")
			.append(" JOIN REPORT.SURVEY S ON SR.SURVEY_ID = S.SURVEY_ID")
			.append(" JOIN REPORT.SURVEY_QUESTION SQ ON S.SURVEY_ID = SQ.SURVEY_ID")
			.append(" JOIN REPORT.SURVEY_QUESTION_RESPONSE SQR ON SR.SURVEY_RESPONSE_ID = SQR.SURVEY_RESPONSE_ID AND SQ.SURVEY_QUESTION_ID = SQR.SURVEY_QUESTION_ID")
			.append(" JOIN REPORT.TERMINAL T ON SR.TERMINAL_ID = T.TERMINAL_ID")
			.append(" JOIN REPORT.EPORT E ON SR.EPORT_ID = E.EPORT_ID")
			.append(" JOIN CORP.CUSTOMER CUS ON T.CUSTOMER_ID = CUS.CUSTOMER_ID")
			.append(" LEFT OUTER JOIN REPORT.LOCATION L ON T.LOCATION_ID = L.LOCATION_ID")
			.append(" LEFT OUTER JOIN REPORT.TERMINAL_ADDR TA ON L.ADDRESS_ID = TA.ADDRESS_ID ")
			.append(" LEFT OUTER JOIN CORP.COUNTRY C ON TA.COUNTRY_CD = C.COUNTRY_CD ").toString();
	
	int paramCnt = 0;
	Map<Integer, Object> queryParams = new LinkedHashMap<Integer, Object>();
	StringBuilder sql = new StringBuilder(sqlBase);
	sql.append(" WHERE (? = 0 OR SR.TERMINAL_ID IN (SELECT TERMINAL_ID FROM REPORT.VW_USER_TERMINAL WHERE USER_ID = ?))")
		.append(" AND SR.CREATED_TS >= TO_DATE(?, 'MM/DD/YYYY') AND SR.CREATED_TS < TO_DATE(?, 'MM/DD/YYYY') + 1");	
	queryParams.put(paramCnt++, customerId);
	queryParams.put(paramCnt++, userId);
	queryParams.put(paramCnt++, startDate);
	queryParams.put(paramCnt++, endDate);
	int stars = inputForm.getInt("stars", false, 0);
	if (stars > 0 && stars < 6) {
		sql.append(" AND SQR.SURVEY_QUESTION_RESPONSE_VALUE = ?");	
		queryParams.put(paramCnt++, stars);
	}
	if (!StringUtils.isBlank(filter) && !StringUtils.isBlank(filterValue)) {
		String partialFilterValue = new StringBuilder("%").append(filterValue.toLowerCase()).append("%").toString();
		if ("address".equalsIgnoreCase(filter)) {
			sql.append(" AND LOWER(TA.ADDRESS1) LIKE ?");	
			queryParams.put(paramCnt++, partialFilterValue);	
		} else if ("city".equalsIgnoreCase(filter)) {
			sql.append(" AND LOWER(TA.CITY) LIKE ?");	
			queryParams.put(paramCnt++, partialFilterValue);
		} else if ("customer".equalsIgnoreCase(filter)) {
			sql.append(" AND LOWER(CUS.CUSTOMER_NAME) LIKE ?");	
			queryParams.put(paramCnt++, partialFilterValue);
		} else if ("device".equalsIgnoreCase(filter)) {
			sql.append(" AND LOWER(E.EPORT_SERIAL_NUM) LIKE ?");	
			queryParams.put(paramCnt++, partialFilterValue);
		} else if ("location".equalsIgnoreCase(filter)) {
			sql.append(" AND LOWER(L.LOCATION_NAME) LIKE ?");	
			queryParams.put(paramCnt++, partialFilterValue);
		} else if ("postal".equalsIgnoreCase(filter)) {
			sql.append(" AND LOWER(TA.ZIP) LIKE ?");	
			queryParams.put(paramCnt++, partialFilterValue);
		} else if ("state".equalsIgnoreCase(filter)) {
			sql.append(" AND LOWER(TA.STATE) LIKE ?");	
			queryParams.put(paramCnt++, partialFilterValue);
		} else if ("survey".equalsIgnoreCase(filter)) {
			sql.append(" AND LOWER(S.SURVEY_NAME) LIKE ?");	
			queryParams.put(paramCnt++, partialFilterValue);
		}
	}
	
	String paramTotalCount = PaginationUtil.getTotalField(null);
	String paramPageIndex = PaginationUtil.getIndexField(null);
	String paramPageSize = PaginationUtil.getSizeField(null);
	String paramSortIndex = PaginationUtil.getSortField(null);

	// pagination parameters
	long totalCount = inputForm.getLong(paramTotalCount, false, -1);
	if(totalCount == -1) {
		// if the total count is not retrieved yet, get it now
		Results total = DataLayerMgr.executeSQL("report", new StringBuilder("SELECT COUNT(1) ").append(sql).append(" AND SQ.EDITOR = 'RATING'").toString(), queryParams.values().toArray(), null);
		if(total.next())
			totalCount = total.getValue(1, long.class);
		else
			totalCount = 0;
		request.setAttribute(paramTotalCount, String.valueOf(totalCount));
	}

	int pageIndex = inputForm.getInt(paramPageIndex, false, 1);
	int pageSize = inputForm.getInt(paramPageSize, false, PaginationUtil.DEFAULT_PAGE_SIZE);
	int minRowToFetch = PaginationUtil.getStartNum(pageSize, pageIndex);
	int maxRowToFetch = PaginationUtil.getEndNum(pageSize, pageIndex);	

	String orderBy = new StringBuilder(PaginationUtil.constructOrderBy(sortFields, sortIndex)).append(", SQ.SURVEY_QUESTION_ORDER").toString();

	String query = new StringBuilder("SELECT R.*, SQ.EDITOR, SQ.SURVEY_QUESTION_NAME, SQ.NEGATIVE_RESPONSE_REGEX, SQR.SURVEY_QUESTION_RESPONSE_VALUE")
			.append(" FROM (select * from (select pagination_temp.*, ROWNUM rnum from (").append(sqlStart).append(sql).append(" AND SQ.EDITOR = 'RATING'").append(orderBy)
			.append(") pagination_temp where ROWNUM <= ?) where rnum  >= ?) R")
			.append(" LEFT OUTER JOIN REPORT.SURVEY_QUESTION_RESPONSE SQR ON R.SURVEY_RESPONSE_ID = SQR.SURVEY_RESPONSE_ID")
			.append(" LEFT OUTER JOIN REPORT.SURVEY_QUESTION SQ ON SQR.SURVEY_QUESTION_ID = SQ.SURVEY_QUESTION_ID")		
			.append(" ORDER BY R.RNUM, SQ.SURVEY_QUESTION_ORDER").toString();

	queryParams.put(paramCnt++, String.valueOf(maxRowToFetch));
	queryParams.put(paramCnt++, String.valueOf(minRowToFetch));
	Results results = DataLayerMgr.executeSQL("report", query, queryParams.values().toArray(), null);	
	
	int i = 0; 
	String rowClass = "row0";
	String oldEditor = "";
	String editor = "";
	StringBuilder answers = new StringBuilder();
	while(results.next()) {
		editor = results.getFormattedValue("editor");
		if ("RATING".equalsIgnoreCase(editor)) {
			i++;		
			if (answers.length() > 0) {
	%>
				<tr class="<%=rowClass%>"><td colspan="11"><span class="strong">Answers:</span> <%=answers.toString()%></td></tr>
			<%
				answers.setLength(0);
			}
			rowClass = (i%2 == 0) ? "row1" : "row0";
			%>
			<tr class="<%=rowClass%>">		
			    <td<%if (results.getValue("ANSWER_COUNT", int.class) > 1) {%> rowspan="2"<%}%>><span class="rating stars star<%=StringUtils.prepareCDATA(results.getFormattedValue("SURVEY_QUESTION_RESPONSE_VALUE"))%>" title="<%=StringUtils.prepareCDATA(results.getFormattedValue("SURVEY_QUESTION_RESPONSE_VALUE"))%> out of 5 stars"></span></td>
			    <td><%=StringUtils.prepareHTML(results.getFormattedValue("CREATED_TS"))%></td>
			    <td><%=StringUtils.prepareHTML(results.getFormattedValue("SURVEY_NAME"))%></td>
			    <td><%=StringUtils.prepareHTML(results.getFormattedValue("CUSTOMER_NAME"))%></td>
			    <td><%=StringUtils.prepareHTML(results.getFormattedValue("LOCATION_NAME"))%></td>
			    <td><%=StringUtils.prepareHTML(results.getFormattedValue("EPORT_SERIAL_NUM"))%></td>
			    <td><%=StringUtils.prepareHTML(results.getFormattedValue("ADDRESS1"))%></td>
			    <td><%=StringUtils.prepareHTML(results.getFormattedValue("CITY"))%></td>
			    <td><%=StringUtils.prepareHTML(results.getFormattedValue("STATE"))%></td>
			    <td><%=StringUtils.prepareHTML(results.getFormattedValue("ZIP"))%></td>
			    <td><%=StringUtils.prepareHTML(results.getFormattedValue("COUNTRY_NAME"))%></td>
			    <td>
			    	<%if (StringUtils.isBlank(results.getFormattedValue("CONSUMER_EMAIL_ADDR"))) {%>
			    	&nbsp;
			    	<%} else {%>
			    	<a href="mailto:<%=StringUtils.prepareCDATA(results.getFormattedValue("CONSUMER_EMAIL_ADDR"))%>"><%=StringUtils.prepareHTML(results.getFormattedValue("CONSUMER_EMAIL_ADDR"))%></a>
			    	<%}%>
			    </td>
			</tr>
	<% } else {
		if (answers.length() > 0)
			answers.append("; ");
		answers.append("<span class=\"question\">").append(StringUtils.prepareHTML(results.getFormattedValue("SURVEY_QUESTION_NAME"))).append("</span> - <span class=\"answer");
		if (results.getFormattedValue("SURVEY_QUESTION_RESPONSE_VALUE").matches(results.getFormattedValue("NEGATIVE_RESPONSE_REGEX")))
			answers.append("-negative");
		answers.append("\">").append(StringUtils.prepareHTML(results.getFormattedValue("SURVEY_QUESTION_RESPONSE_VALUE"))).append("</span>");
	}
	}
	if (answers.length() > 0) {
	%>
			<tr class="<%=rowClass%>"><td colspan="11"><span class="strong">Answers:</span> <%=answers.toString()%></td></tr>
	<%
	}
	} else {
	%>
		<tr><td colspan="12" align="center">No reviews found</td></tr>
	<%} %>
</table>

<div class="spacer5"></div>

<%String storedNames = PaginationUtil.encodeStoredNames(new String[]{"profileId", "stars", "start_date", "end_date", "filter", "filter_value"});%>

<jsp:include page="include/pagination.jsp" flush="true">
    <jsp:param name="_param_request_url" value="consumer_reviews.html" />
	<jsp:param name="_param_stored_names" value="<%=storedNames%>" />
</jsp:include>

<jsp:include page="include/footer.jsp"/>