<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.ServletUser"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.servlet.NotAuthorizedException"%>
<%@page isErrorPage="true"%><%
if(exception == null) {
	exception = (Throwable)request.getAttribute("simple.servlet.SimpleAction.exception");
	if(exception == null)
		exception = new Throwable("Stack Trace");
}
String[] contentTypes = RequestUtils.getPreferredContentType(request, "text/html", "application/xhtml+xml", "text/plain");
boolean textResult = (contentTypes != null && contentTypes.length > 0 && contentTypes[0].equalsIgnoreCase("text/plain"));
if(textResult) {
	if(exception instanceof NotAuthorizedException) {
	    ServletUser user = RequestUtils.getUser(request);
	    if(user == null)
	       RequestUtils.writeTextResponse(response, "error", "You do not have permission to view this page. Please contact USA Technologies if you need access.");
	    else 
	       RequestUtils.writeTextResponse(response, "error", "User '" + user.getUserName() + "' does not have permission to view this page. Please contact USA Technologies if you need access.");
	} else {
	    simple.io.Log.getLog("Error Page").warn("",exception);
	    //response.setStatus(response.SC_INTERNAL_SERVER_ERROR);
	    response.setHeader("Success", "false");
	    String displayErrorMessage = RequestUtils.getAttribute(request, "displayErrorMessage", String.class, false);
	    if(displayErrorMessage == null)
	        displayErrorMessage = "An error occurred on the server. We are working to resolve this. Please try again later.";
	    RequestUtils.writeTextResponse(response, "error", displayErrorMessage);
	}
}
%>
<jsp:include page="include/header.jsp"/>
<p style="font-family: Verdana; font-size: 10px; color: red">
<%
if(exception instanceof NotAuthorizedException) {
	ServletUser user = RequestUtils.getUser(request);
	if(user == null) {
	   %>You do<%
	} else {
	   %>User '<%=StringUtils.prepareHTML(user.getUserName())%>' does<%
	}%> not have permission to view this page. Please contact USA Technologies if you need access.<%
} else {
	simple.io.Log.getLog("Error Page").warn("",exception);
    //response.setStatus(response.SC_INTERNAL_SERVER_ERROR);
    response.setHeader("Success", "false");
    String displayErrorMessage = RequestUtils.getAttribute(request, "displayErrorMessage", String.class, false);
    if(displayErrorMessage == null)
        displayErrorMessage = "An error occurred on the server. We are working to resolve this. Please try again later.";
    %><b>Application Error:</b> <%=StringUtils.prepareHTML(displayErrorMessage) %><%
}
%>
</p>
<jsp:include page="include/footer.jsp"/>