<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<% 
long processRequestId = RequestUtils.getAttribute(request, "processRequestId", Long.class, true);
int n = DataLayerMgr.executeUpdate("CANCEL_PROCESS_REQUEST", RequestUtils.getInputForm(request), true);
switch(n) {
	case 1:
		RequestUtils.getOrCreateMessagesSource(request).addMessage("success", "usalive-process-request-cancel-success", "Request has been canceled", processRequestId);
		break;
	case 0:
		RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "usalive-process-request-cancel-failure", "Request could not be canceled", processRequestId);
		break;
	default:
		RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "usalive-process-request-cancel-error", "Request could not be canceled", processRequestId);
		break;
}
RequestUtils.redirectWithCarryOver(request, response, "process_request_history.html?processRequestId=" + processRequestId);
%>