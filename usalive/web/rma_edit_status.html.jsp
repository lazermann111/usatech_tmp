<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="simple.io.Log"%>
<%@page import="java.lang.Exception"%>
<%@page import="simple.text.StringUtils"%>
<script type="text/javascript">
<%
	Log log = Log.getLog();
	UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
	boolean hasPri=false;
	if(user.isInternal()&&user.hasPrivilege(ReportingPrivilege.PRIV_CUSTOMER_SERVICE)){
		hasPri=true;
	}
	InputForm inputForm = RequestUtils.getInputForm(request);
	if(hasPri && "POST".equalsIgnoreCase(request.getMethod())){
		try{
				DataLayerMgr.executeCall("UPDATE_RMA_STATUS", inputForm, true);
				%>updateStatus('success', '',<%=StringUtils.prepareScript(inputForm.getStringSafely("rmaId", ""))%>, <%=StringUtils.prepareScript(inputForm.getStringSafely("status",""))%>);<%
		}catch(Exception e) {
			%>
			updateStatus('error', 'Editing RMA status failed. Please retry.',<%=StringUtils.prepareScript(inputForm.getStringSafely("rmaId", ""))%>, <%=StringUtils.prepareScript(inputForm.getStringSafely("status",""))%>);<%
			log.error("Failed to edit RMA status. Please try again.", e);
		}
	};
%>
</script>
