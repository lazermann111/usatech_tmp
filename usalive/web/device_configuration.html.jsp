<%@page import="com.usatech.layers.common.util.DeviceUtils"%>
<%@page import="com.usatech.layers.common.model.Device"%>
<%@page import="com.usatech.usalive.servlet.USALiveUtils"%>
<%@page import="simple.servlet.ResponseUtils"%>
<%@page import="com.usatech.report.ReportingUser.ActionType"%>
<%@page import="com.usatech.report.ReportingUser.ResourceType"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.usatech.usalive.device.DeviceConfigUtils"%>
<%@page language="java" contentType="text/html; charset=iso-8859-1" pageEncoding="iso-8859-1"%>

<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.Map"%>

<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.text.StringUtils"%>

<%@page import="com.usatech.layers.common.constants.DeviceType"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<%@page import="com.usatech.layers.common.model.ConfigTemplateSetting"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%@page import="com.usatech.layers.common.util.WebHelper"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%
InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
long terminalId = inputForm.getLong("terminalId", false, -1);
Map<String,Object> params = new HashMap<String,Object>();
params.put("terminal_id", terminalId);
Results pendingCfgCommandsCountResult = DataLayerMgr.executeQuery("GET_PENDING_CFG_COMMANDS_COUNT_FOR_DEVICE", params);
boolean hasPendingCfgCommands = pendingCfgCommandsCountResult != null && pendingCfgCommandsCountResult.next();
if (hasPendingCfgCommands) {
	hasPendingCfgCommands = ConvertUtils.convert(BigDecimal.class, pendingCfgCommandsCountResult.getValue("PENDING_CFG_COMMANDS_COUNT")).longValue() > 0L;
}
UsaliveUser checkUser = (UsaliveUser) RequestUtils.getUser(request);
checkUser.checkPermission(ResourceType.DEVICE_CONFIG, ActionType.EDIT, terminalId);
if (DeviceConfigUtils.processDeviceConfig(inputForm, request, response))
	return;
String device_serial_cd = inputForm.getStringSafely("device_serial_cd", "");
long device_id  = inputForm.getLong("device_id", false, -1);
int device_type_id = inputForm.getInt("device_type_id", false, -1);
int plv = inputForm.getInt("plv", false, -1);
boolean isEdge = device_type_id == DeviceType.EDGE.getValue();
boolean isGx = device_type_id == DeviceType.GX.getValue() || device_type_id == DeviceType.G4.getValue();
boolean isMEI = device_type_id == DeviceType.MEI.getValue();
LinkedHashMap<String, ConfigTemplateSetting> defaultSettingData = (LinkedHashMap<String, ConfigTemplateSetting>) inputForm.getAttribute("defaultSettingData");
LinkedHashMap<String, String> targetSettingData = (LinkedHashMap<String, String>) inputForm.getAttribute("targetSettingData");
LinkedHashMap<String, String> currentSettingData = (LinkedHashMap<String, String>) inputForm.getAttribute("currentSettingData");
if (currentSettingData == null)
	currentSettingData = targetSettingData;
int deviceUtcOffsetMin = 0;
if (device_id > 0 && isEdge) {
	Device device = DeviceUtils.generateDevice(device_id);
	deviceUtcOffsetMin = device.getDeviceUtcOffsetMin();
}
%>
<jsp:include page="include/header.jsp"/>
<div class="tableDataContainer">
<form name="editConfig" method="post" action="device_configuration.html" onsubmit="return validateUSATForm(this) && validateDeviceConfiguration(this);">
<input type="hidden" name="terminalId" value="<%=terminalId%>" />
<input type="hidden" name="plv" id="plv" value="<%=plv%>" />
<%
ResponseUtils.writeXsfrFormProtection(pageContext);
USALiveUtils.writeFormProfileId(pageContext);
%>
<script defer="defer" type="text/javascript">
function validateDeviceConfiguration(theForm) {
	<%if (isGx || isEdge) {%>
	var elem;
	<%if (isGx) {%>
		var twoTierPricingField = '362';
	<%} else if (isEdge) {%>
		var twoTierPricingField = '1202';
		var interfaceType = theForm.elements["cfg_field_1500"].value;
		if (interfaceType == '3' || interfaceType == '4' || interfaceType == '5') {
			elem = theForm.elements["cfg_field_1200"];
			var elemValue = parseFloat(elem.value);
			if (elemValue < parseFloat(theForm.elements["cfg_field_1503"].value) || elemValue < parseFloat(theForm.elements["cfg_field_1506"].value)
					|| elemValue < parseFloat(theForm.elements["cfg_field_1508"].value) || elemValue < parseFloat(theForm.elements["cfg_field_1510"].value)
					|| elemValue < parseFloat(theForm.elements["cfg_field_1512"].value) || elemValue < parseFloat(theForm.elements["cfg_field_1514"].value)
					|| elemValue < parseFloat(theForm.elements["cfg_field_1516"].value) || elemValue < parseFloat(theForm.elements["cfg_field_1518"].value)
					|| elemValue < parseFloat(theForm.elements["cfg_field_1520"].value) || elemValue < parseFloat(theForm.elements["cfg_field_1523"].value)) {
				invalidInput(elem, elem.getAttribute("label") + " cannot be less than a Coin Pulse monetary value. Please correct.");
				return false;
			}
		}
	<%}%>
		elem = theForm.elements["cfg_field_" + twoTierPricingField];
		elem.value = trim(elem.value);
		var oldElem = theForm.elements["cfg_original_" + twoTierPricingField];
		if (!isNaN(elem.value) && !isNaN(oldElem.value) && parseFloat(elem.value) > 0.10 && parseFloat(elem.value) != parseFloat(oldElem.value)) {
			invalidInput(elem, "New " + elem.getAttribute("label") + " value cannot be greater than 0.10. Please correct.");
			return false;
		}
	<%}%>
	return true;
}
function change_schedule(item) {
	var plv = document.getElementById('plv').value;
	document.getElementById(item.name.replace("cfg_field_", "span_time_")).style.display = item.value == "" ? "none" : "inline-block";
	var schedules = (plv && plv < 20) ? ["I", "W"] : ["I", "W", "E"];
	for (var i = 0; i < schedules.length; i++) {
		if (document.getElementById(item.name.replace("cfg_field_", "span_" + schedules[i] + "_"))) {
			document.getElementById(item.name.replace("cfg_field_", "span_" + schedules[i] + "_")).style.display = item.value == schedules[i] ? "inline-block" : "none";
		}
	}
	if (item.value == 'E' && document.getElementById(item.name.replace("cfg_field_", "span_time_"))) {
		document.getElementById(item.name.replace("cfg_field_", "span_time_")).style.display = "none";
	}
	var timelist_elems = document.getElementsByName(item.name.replace("cfg_field_", "sch_timelist_"));
	for (var i=timelist_elems.length-1; i>=0; i--) {
		if (!timelist_elems[i].value) {
			var tr_elem = timelist_elems[i].parentNode;
			tr_elem.parentNode.remove(tr_elem);
			this[document.getElementById(item.name.replace("cfg_field_", "timeCount_"))] = this[document.getElementById(item.name.replace("cfg_field_", "timeCount_"))] - 1;
		}
	}
	if (document.getElementById(item.name.replace("cfg_field_", "add_time_button_"))) {
		document.getElementById(item.name.replace("cfg_field_", "add_time_button_")).disabled=(this[document.getElementById(item.name.replace("cfg_field_", "timeCount_"))] >= 6);
	}
}
function enableOptions(options, values, fn) {
	for(var i = 0; i < options.length; i++) {
		var valid = (values && values.contains && values.contains(fn ? fn(options[i].value) : options[i].value));
		options[i].disabled = !valid;
		if(!valid && options[i].selected)
			options[i].selected = false;
	}
}
function checkSupport(select) {
	select = $(select);
	if(select.hasClass("unsupported") && select.selectedIndex >= 0 && !select.options[select.selectedIndex].disabled)
		select.removeClass("unsupported");
}
</script>
<%
String errorMessage = inputForm.getString("errorMessage", false);
if (!StringHelper.isBlank(errorMessage)) {%>
<div class="message-error"><%=StringUtils.prepareHTML(errorMessage).replace("\n", "<br/>")%><br/></div>
<%}%>
	<%if (hasPendingCfgCommands) {%>
	<table cellspacing="0" cellpadding="5" align="center" width="100%" style="text-align: center;">
		<tr>
			<td class="warn-solid">
				Note: configuration updates are pending
			</td>
		</tr>
	</table>
	<%} %>
<table class="full">
	<tr class="tableHeader"><th><div align="center">Device Configuration - <%=StringUtils.prepareHTML(device_serial_cd)%></div></th></tr>
</table>
<span class="required-normal-note">* Denotes required parameter</span>
<%if (isEdge || isGx || isMEI) {%>
<table class="tabDataDisplayNoBorder">
	<tr>
		<td colspan="2">
		<div id="section_index"></div>
		<div class="sectionIndex">
		<table class="tabDataDisplayNoBorder">
			<tr>
			<td align="left" valign="top" style="width: 110px;">
				<b>Section Index:</b>
			</td>
			<td align="left">
			<%
			int currentCategoryId = -1;
			for (Map.Entry<String, ConfigTemplateSetting> entry : defaultSettingData.entrySet()) {
				ConfigTemplateSetting defaultSetting = entry.getValue();
				int categoryId = defaultSetting.getCategoryId();
				String categoryName = defaultSetting.getCategoryName();
				if (StringHelper.isBlank(categoryName) || categoryId == currentCategoryId)
					continue;
				currentCategoryId = categoryId;
			%>
				<a href="?terminalId=<%=terminalId%>#category_<%=categoryId%>"><%=categoryName%></a><br/>
			<%} %>
			</td>
			</tr>		
		</table>
		</div>
		</td>
	</tr>
</table>
<%}%>
 
<div class="spacer5"></div>

<table class="tabDataDisplayBorder">
	<col width="50%" />
	<col width="50%" />

<%
StringBuilder hiddenInputs = new StringBuilder();
int categoryId = 0;
String currentCategoryName = "";
int counter = 0;
for (Map.Entry<String, ConfigTemplateSetting> entry : defaultSettingData.entrySet()) {
	ConfigTemplateSetting defaultSetting = entry.getValue();
	boolean isServerOnly = defaultSetting.isServerOnly();
	String key = defaultSetting.getKey();
	int keyNumber;
	if (!isServerOnly && (isGx || isEdge))
		keyNumber = ConvertUtils.getIntSafely(key, -1);
	else
		keyNumber = -1;
	boolean isSupported = defaultSetting.isSupported();
	
	if (isGx && DevicesConstants.GX_MAP_PROTECTED_FIELDS.contains(keyNumber))
		continue;
		
	String label = defaultSetting.getLabel();
	String description = defaultSetting.getDescription();
	String lineDesc = defaultSetting.getLineDesc();
	String editorType = defaultSetting.getEditorType();
	boolean required = defaultSetting.isRequired();
	counter = defaultSetting.getCounter();
	boolean storedInPennies = "Y".equalsIgnoreCase(defaultSetting.getStoredInPennies());
	String defaultValue = defaultSetting.getConfigTemplateSettingValue();
	if (defaultValue == null)
		defaultValue = "";
	else if (storedInPennies && StringHelper.isNumeric(defaultValue))
		defaultValue = String.format("%.2f", new BigDecimal(defaultValue).divide(BigDecimal.valueOf(100)));
	
	String value;
	if (!isSupported && isEdge)
		value = defaultSetting.getUnsupportedDefaultValue();
	else
		value = targetSettingData.get(key);
	if (value == null)
		value = "";
	else if (storedInPennies && StringHelper.isNumeric(value))
		value = String.format("%.2f", new BigDecimal(value).divide(BigDecimal.valueOf(100)));
	
	if ("SCHEDULE".equalsIgnoreCase(editorType) && value.startsWith("I^"))
		value = WebHelper.buildIntervalScheduleValue(value, deviceUtcOffsetMin);
	
	String originalValue = currentSettingData.get(key);
	if (originalValue == null)
		originalValue = "";
	else if (storedInPennies && StringHelper.isNumeric(originalValue))
		originalValue = String.format("%.2f", new BigDecimal(originalValue).divide(BigDecimal.valueOf(100)));
	
	hiddenInputs.append("<input name=\"cfg_original_").append(StringUtils.prepareCDATA(key)).append("\" id=\"cfg_original_").append(counter).append("\" value=\"").append(StringUtils.prepareCDATA(originalValue)).append("\" type=\"hidden\" />\n");
	hiddenInputs.append("<input name=\"cfg_default_").append(StringUtils.prepareCDATA(key)).append("\" id=\"cfg_default_").append(counter).append("\" value=\"").append(StringUtils.prepareCDATA(defaultValue)).append("\" type=\"hidden\" />\n");
	hiddenInputs.append("<input name=\"cfg_holder_").append(StringUtils.prepareCDATA(key)).append("\" id=\"cfg_holder_").append(counter).append("\" value=\"").append(StringUtils.prepareCDATA(value)).append("\" type=\"hidden\" />\n");
	
	if ((isEdge || isGx || isMEI)) {
		String categoryName = defaultSetting.getCategoryName();
		if (!StringHelper.isBlank(categoryName) && !categoryName.equals(currentCategoryName)) {
			currentCategoryName = categoryName;
			categoryId = defaultSetting.getCategoryId(); %>
			<tr class="tableHeader">
				<th colspan="2">
					<div id="category_<%=categoryId%>" align="center">
					<%=StringUtils.prepareHTML(categoryName)%>
					<a href="?terminalId=<%=terminalId%>#section_index"><img src="/images/arrow-up-icon.png" style="vertical-align:bottom; border:0;"/></a>
					</div>
				</th>
			</tr>
<%
		}
	}
%>
	<tr bgcolor="#FEFEFE">
		<td>
			<b><%=StringUtils.prepareHTML(label)%><%if(required) {%><span class="required-normal">*</span><%}%></b><%if (!isSupported) {%><span class="unsupported"><br/>Unsupported by this device firmware. Please contact Customer Service for firmware upgrade.</span><%}%>
		</td>
		<td valign="top" style="vertical-align:top;"><%=WebHelper.generateInput(defaultSetting, inputForm.getStringSafely(new StringBuilder("cfg_field_").append(defaultSetting.getKey()).toString(), value), device_type_id, plv, isSupported, defaultValue, false, null, "")%></td>
	</tr>
	<tr>
		<td colspan="2" bgcolor="#EEEEEE">
			<%=description%><%if ((isEdge || isGx || isMEI) && !isServerOnly) out.write(WebHelper.generateDefaultValueString(defaultSetting, defaultValue, description));%>
		</td>
	</tr>
<%} %>

</table>

<%=hiddenInputs.toString()%>
<input type="hidden" id="elem_count" value="<%=counter%>"/>

<div class="spacer5"></div>
<div align="center">
<table>
	<tr>
		<%if (inputForm.getBoolean("allowEdit", false, false)) {%>
		<td valign="top" align="center">
		<input type="submit" class="cssButton" name="action" value="Save and Send"/>
		</td>
		<%}%>
		<td valign="top" align="center" rowspan="2">
		<input type="button" class="cssButton" value="Device Profile" onClick="window.location = 'terminal_details.i?terminalId=<%=terminalId%>'" />
		</td>
	</tr>
</table>
</div>

<div class="spacer5"></div>

</form>
</div>
<jsp:include page="include/footer.jsp"/>