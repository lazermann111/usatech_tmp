<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="java.util.Collections"%>
<%@page import="simple.bean.ConvertException"%>
<%@page import="simple.servlet.steps.LoginFailureException"%>
<%@page import="simple.servlet.SimpleServlet,simple.servlet.InputForm,java.util.Map,simple.bean.ConvertUtils,simple.servlet.InputFile,simple.servlet.RequestUtils,simple.text.StringUtils"%>
<%@page import="java.util.Set,java.util.HashSet,java.util.Arrays,simple.servlet.SimpleAction,simple.servlet.NotLoggedOnException,simple.servlet.NoSessionTokenException,simple.servlet.InvalidSessionTokenException"%>
<%!
protected static final Set<String> dontForwardPaths = new HashSet<String>(Arrays.asList(new String[] {
	"/login_prompt.i",
	"/logout.i",
	"/login.i",
	"/login_dialog.i",
	"/login.html"
})); 
protected static final Set<String> ignoreParameters = new HashSet<String>(Arrays.asList(new String[] {
    "username",
    "password",
    "loginPromptPage",
    "simple.servlet.steps.LogonStep.forward",
    "content",
    "session-token",
    "selectedMenuItem",
    "editPassword",
    "editConfirm"
}));
protected boolean shouldForwardAfterLogin(String contentPath) {
		if(StringUtils.isBlank(contentPath))
			return false;
		for(String suffix : dontForwardPaths)
			if(contentPath.endsWith(suffix))
				return false;
		return true;
	}
%><%
RequestUtils.setAttribute(request, "subtitle", "Log In", "request");
boolean isFragment = Boolean.TRUE.equals(RequestUtils.getAttribute(request, "fragment", Boolean.class, false));
String contentPath = RequestUtils.getRequestURIQuery(request); //request.getRequestURI()
Map<String,Object> queryParams;
if(request.getQueryString() != null)
	queryParams = RequestUtils.parseParameters(request.getQueryString());
else
	queryParams = Collections.emptyMap();
Integer selectedMenuItem;
try {
    selectedMenuItem = RequestUtils.getAttribute(request, "selectedMenuItem", Integer.class, false);
} catch(ConvertException e) {
    selectedMenuItem = null;
}
if(!shouldForwardAfterLogin(contentPath)) {
    contentPath = "/home.i";
	selectedMenuItem = 110;
}
Throwable exception = (Throwable)RequestUtils.getAttribute(request, SimpleAction.ATTRIBUTE_EXCEPTION, false);
boolean expired;
if(session != null) {
    if(session.isNew())
        expired = false;
    else if(session.getMaxInactiveInterval() > 0)
        expired = (System.currentTimeMillis() - session.getLastAccessedTime())/ 1000 > session.getMaxInactiveInterval();
    else
        expired = false;
} else
    expired = false;
if(isFragment){
		%><script type="text/javascript" > 
		window.location.href="login_dialog.i";
		</script>
<%
	}else{%>
<jsp:include page="include/header.jsp"/>
<div>
	<form name="info" method="post" action="update_password.html" name="changePasswordForm" onsubmit="App.updateClientTimestamp(this)" data-toggle="validateInline" autocomplete="off">
	   <input type="hidden" name="forward-to" value="<%=StringUtils.prepareCDATA(contentPath)%>"/>
       <input type="hidden" name="session-token" value="<%=StringUtils.prepareCDATA(RequestUtils.getSessionToken(request))%>"/><%
if(selectedMenuItem != null) { %>
    <input type="hidden" name="selectedMenuItem" value="<%=selectedMenuItem%>" /><%
}
InputForm form = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
for(Map.Entry<String,Object> param : form.getParameters().entrySet()) {
    if(ignoreParameters.contains(param.getKey()) || queryParams.containsKey(param.getKey())) 
    	continue;
    if(param.getValue() instanceof String) {
        %><input type="hidden" name="<%=StringUtils.prepareCDATA(param.getKey())%>" value="<%=StringUtils.prepareCDATA((String)param.getValue())%>"/><%
    } else if(param.getValue() instanceof String[]) {
        String[] values = (String[])param.getValue();
        for(int i = 0; i < values.length; i++) {
            %><input type="hidden" name="<%=StringUtils.encodeForHTMLAttribute(param.getKey())%>" value="<%=StringUtils.encodeForHTMLAttribute(values[i])%>"/><%
        }
    } else if(param.getValue() instanceof InputFile || param.getValue() instanceof InputFile[]) {
        RequestUtils.storeForNextRequest(request.getSession(), param.getKey(), param.getValue());
    } else if(param.getValue() != null) {
        String value = ConvertUtils.getStringSafely(param.getValue());
        %><input type="hidden" name="<%=StringUtils.encodeForHTMLAttribute(param.getKey())%>" value="<%=StringUtils.encodeForHTMLAttribute(value)%>"/><%
    }
}
%>
<div class="caption">Change Password</div>
<%
UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
if(user != null && !user.isInternal()) {
%><input type="hidden" name="editUserId" value="<%=user.getUserId()%>"/><%
}
%>
<table>
<tr><td class="requirementTopNote">Password must be at least 8 characters and contain 1 uppercase letter,<br/> 1 lowercase letter, and 1 number or punctuation.</td></tr>
<tr><td>
<table class="loginLayout"><%
if(user == null) {
    Cookie cookie = form.getCookie("username");
    String username = RequestUtils.getAttribute(request, "editUsername", String.class, false);
    if(StringUtils.isBlank(username) && cookie != null)
        username = cookie.getValue();
    %>
    <tr><th>User:&#160;</th><td><input type="text" name="editUsername" value="<%=StringUtils.prepareCDATA(username)%>"/></td></tr>
    <tr><th>Old Password:</th><td><input type="password" name="password" value="" autocomplete="off"/></td></tr><%    
} else if(user.isInternal()) {%>
    <tr><th>Old Password:</th><td><input type="password" name="password" value="" autocomplete="off"/></td></tr><%    
}%>
    <tr><th>New Password:</th><td><input id="password" maxlength="20" type="password" name="editPassword" data-validators="validate-password"/></td></tr>
    <tr><th>Confirm Password:</th><td><input maxlength="20" type="password" name="editConfirm" data-validators="validate-match" data-validator-properties="{matchInput:'password', matchName:'password'}"/></td></tr>
    <tr class="loginButtonRow"><td colspan="2"><input type="submit" name="submitButton" value="Save Changes"/></td></tr>
</table>
</td>
</tr>        
</table>
</form><%
if(user == null) {%>
<script type="text/javascript"> 
window.addEvent('domready', function() {
	try {
		var form = $(document.forms.changePasswordForm);
		if(form.username.getValue() == "")
			form.username.focus();
		else
			form.password.focus();
	} catch (e) {
	}
});
</script><%
}%>
</div>
<jsp:include page="include/footer.jsp"/>
<%}%>