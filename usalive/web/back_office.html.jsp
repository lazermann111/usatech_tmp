<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<jsp:include page="include/header.jsp"/>

<%
UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
if ("Y".equalsIgnoreCase(inputForm.getStringSafely("signup", ""))) {
%>
<div class="message-success">Congratulations! You have taken the first step to faster payments, streamlined invoice and account management and rebate eligibility.<br/>Please follow the instructions below to get started today, and watch for more information on how to use and make the most of your new ePort Online service in the coming weeks!</div>
<div class="spacer10"></div>
<%}%>

<span class="caption">ePort Online</span>
<div class="spacer10"></div>

<ul>
	<li>To add new ePort Online customer information, click on the "Create Account" button below and follow the prompts.</li>
	<li>To process a new payment for an existing ePort Online customer,  click "Saved Accounts" and select the "Charge" button to the right of the customer information.</li>
	<%if (user.hasPrivilege(ReportingPrivilege.PRIV_ADMIN_ALL_CUST_USERS)) {%>
	<li>To enable or disable authorized users of ePort Online, please visit the "Users" page and add or remove the ePort Online privilege.</li>
	<%} %>
</ul>
<div class="spacer10"></div>

<button onclick="window.location = 'payor_add.html'" title="Add new account">Create Account</button>
<button onclick="window.location = 'payor_config.html'">Saved Accounts</button>
<button onclick="window.location = 'back_office_transactions.html'">Transaction History</button>
<button onclick="window.location = 'payor_add.html?oneTime=true'" title="Charge a one-time account ">One-time Charge</button>

<jsp:include page="include/footer.jsp"/>
