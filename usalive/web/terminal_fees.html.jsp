<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.usatech.layers.common.fees.TerminalFees"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.results.Results"%>
<%@page import="com.usatech.usalive.servlet.USALiveUtils"%>
<%@page import="simple.servlet.ResponseUtils"%>
<%@page import="com.usatech.report.ReportingUser.ActionType"%>
<%@page import="com.usatech.report.ReportingUser.ResourceType"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%
InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
long terminalId = inputForm.getLong("terminalId", false, -1);
UsaliveUser user = (UsaliveUser) RequestUtils.getUser(request);
user.checkPermission(ResourceType.TERMINAL, ActionType.EDIT, terminalId);

Results custBankResults = DataLayerMgr.executeQuery("GET_RESELLER_BANK_ACCOUNTS", inputForm);

Results terminalDetails = DataLayerMgr.executeQuery("GET_TERMINAL_DETAILS", new Object[] {terminalId, user.getUserId()});
terminalDetails.next();
String deviceSerialCd = terminalDetails.getFormattedValue("deviceSerialCd");

TerminalFees fees = TerminalFees.loadByTerminalId(terminalId);

String action = inputForm.getString("action", false);
if (action != null && action.equalsIgnoreCase("Save Changes")) {
  fees.updateFromForm(inputForm, false);
  if (fees.isDirty()) {
    fees.saveToDatabase();
    RequestUtils.getOrCreateMessagesSource(inputForm).addMessage("success", "update-terminal-fees-success", "Terminal fees updated successfully.");
  }
}
%>
<jsp:include page="include/header.jsp"/>
<div class="tableDataContainer">
<form name="terminalFees" method="post" action="terminal_fees.html" onsubmit="return validateUSATForm(this);">
<script src="/scripts/fees.js"></script>
<script type="text/javascript">
//<![CDATA[
  window.addEvent("domready", function() {
    new Form.Validator.Inline.Mask(document.terminalFees);
  });
//]]>
</script>
<input type="hidden" name="terminalId" value="<%=terminalId%>" />
<%
ResponseUtils.writeXsfrFormProtection(pageContext);
USALiveUtils.writeFormProfileId(pageContext);
String errorMessage = inputForm.getString("errorMessage", false);
if (!StringHelper.isBlank(errorMessage)) {%>
<div class="message-error"><%=StringUtils.prepareHTML(errorMessage)%><br/></div>
<%}%>
    
<table class="fields">
  <tr class="tableDataShade">
    <th colspan="2" class="terminalHdr">Terminal Fees - <%=StringUtils.prepareHTML(deviceSerialCd)%></th>
  </tr>
  <tr>
    <td colspan="2">
      <div class="spacer5"></div>
    </td>
  </tr>
  <tr class="tableDataShade">
    <td colspan="2">
      <% fees.writeHtmlTable(pageContext, true); %>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <div class="spacer5"></div>
    </td>
  </tr>
  <tr class="tableHeader">
    <th colspan="2">Commission Payment Information</th>
  </tr>
  <tr class="tableDataShade">
    <th>Bank Account To Pay<span class="required">*</span></th>
    <td><select name="COMMISSION_BANK_ID"><%
Long bankAcctId = ConvertUtils.convertSafely(Long.class, RequestUtils.getAttribute(request,"COMMISSION_BANK_ID", false), null);
if(bankAcctId == null)
	bankAcctId = fees.getCommissionBankId();
while(custBankResults.next()) {
	long baId = custBankResults.getValue("custBankId", Long.class);
      %><option value="<%=baId%>"<%if(bankAcctId != null && baId == bankAcctId) {%> selected="selected"<%} %>>
      <%=user.isPartner() ?  StringUtils.prepareHTML(custBankResults.getFormattedValue("custName")) + " - " : ""%>
      <%=StringUtils.prepareHTML(custBankResults.getFormattedValue("bankName"))%>
      - #<%=StringUtils.prepareHTML(custBankResults.getFormattedValue("bankAcctNum"))%>
      - <%=StringUtils.prepareHTML(custBankResults.getFormattedValue("accountTitle"))%>
        <%=custBankResults.getFormattedValue("status").equalsIgnoreCase("P") ? " (PENDING APPROVAL)" : "" %></option><%
  }%></select>
    </td>
  </tr>
  <tr>
    <td colspan="2" align="left"><span class="required">* Required Fields</span></td>
  </tr>
  <tr>
    <td colspan="2" style="text-align:center">
      <div class="spacer5"></div>
      <input type="submit" class="cssButton" id="action" name="action" value="Save Changes"/>
      <input type="button" class="cssButton" value="Device Profile" onClick="window.location = 'terminal_details.i?terminalId=<%=terminalId%>'" />
    </td>
  </tr>
</table>
</form>
</div>
<jsp:include page="include/footer.jsp"/>
