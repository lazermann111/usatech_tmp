<%@page import="simple.servlet.NotAuthorizedException"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="com.usatech.report.ReportingUser.ActionType"%>
<%@page import="com.usatech.report.ReportingUser.ResourceType"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.db.Call.Sorter"%>
<%@page import="simple.servlet.GenerateUtils"%>
<%@page import="java.util.concurrent.TimeUnit"%>
<%@page import="simple.bean.ConvertException"%>
<%@page import="java.util.Date"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.usalive.servlet.USALiveUtils"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.text.StringUtils"%>
<%
InputForm form = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
UsaliveUser user = (UsaliveUser) RequestUtils.getUser(request);
if(!user.hasPrivilege(ReportingPrivilege.PRIV_BACK_OFFICE))
    throw new NotAuthorizedException("You are not authorized to perform this function.");
form.setAttribute("allowedStatusCds", "AD");
Results payorResults = DataLayerMgr.executeQuery("GET_PAYORS", form);
Sorter sorter = DataLayerMgr.getGlobalDataLayer().findCall("GET_PAYOR_TRAN_HISTORY").getSorter();
String startDateText = form.getString("startDate", false);
Date startDate;
try {
	startDate = ConvertUtils.convert(Date.class, startDateText);
} catch(ConvertException e) {
	RequestUtils.getOrCreateMessagesSource(request).addMessage("warn", "usalive-backoffice-trans-invalid-date", "Invalid {0} date ''{1}''", "start", startDateText);
	startDate = null;
}
if(startDate == null) {
    startDate = new Date(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(30));
    form.setAttribute("startDate", startDate);
}
	
String endDateText = form.getString("endDate", false);
Date endDate;
try {
	endDate = ConvertUtils.convert(Date.class, endDateText);
} catch(ConvertException e) {
    RequestUtils.getOrCreateMessagesSource(request).addMessage("warn", "usalive-backoffice-trans-invalid-date", "Invalid {0} date ''{1}''", "end", endDateText);
    endDate = null;
}
if(endDate == null) {
	endDate = new Date(System.currentTimeMillis());
    form.setAttribute("endDate", endDate);
}
long payorId = form.getLong("payorId", false, 0L);
if(payorId > 0)
	user.checkPermission(ResourceType.PAYOR, ActionType.VIEW, payorId);

String[] sortBy = ConvertUtils.toUniqueArray(form.getStringArray("sortBy", false), String.class, "-tranDate", "sortablePayorName");
form.setAttribute("sortBy", sortBy);
int pageIndex = form.getInt("pageIndex", false, 1);
int pageSize = form.getInt("pageSize", false, PaginationUtil.DEFAULT_PAGE_SIZE);
Results results = sorter.executeSorted(form, pageSize, pageIndex, sortBy);
%>
<jsp:include page="include/header.jsp"/>
<div class="caption">ePort Online: Transaction History</div>
<div class="spacer5"></div>
<form method="get" action="back_office_transactions.html" data-toggle="validate">
	<%=USALiveUtils.getHiddenInputs(request)%>
	<div class="payor-transactions">
	<span class="label">Start Date</span> 
	<input type="text" id="startDate" size="8" maxlength="10" name="startDate" data-validators="required validate-date dateFormat:'mm/dd/yyyy'" data-label="Start Date" value="<%=ConvertUtils.formatObject(startDate, "DATE:MM/dd/yyyy")%>" data-toggle="calendar" data-format="%m/%d/%Y"/> 
	&nbsp;<span class="label">End Date</span> 
	<input type="text" id="endDate" size="8" maxlength="10" name="endDate" data-validators="required validate-date dateFormat:'mm/dd/yyyy'" data-label="End Date" value="<%=ConvertUtils.formatObject(endDate, "DATE:MM/dd/yyyy") %>"  data-toggle="calendar" data-format="%m/%d/%Y"/> 
	<span class="label">Account</span>
	<select name="payorId">
		<option value="0">-- All --</option>
		<%
		while (payorResults.next()) {
			if(payorResults.getValue("userAccess", Boolean.class)) {		    
				String status;
				if(payorResults.isGroupBeginning("status") && !StringUtils.isBlank((status=payorResults.getValue("status", String.class)))) {
			        %><optgroup label="[<%=StringUtils.prepareCDATA(status) %>]"><%
			    } %><option value="<%=payorResults.getValue("payorId", long.class)%>"<%if (payorId == payorResults.getValue("payorId", long.class)) {%> selected="selected"<%}%>><%=StringUtils.prepareHTML(payorResults.getFormattedValue("payorName"))%></option>
	<%			if(payorResults.isGroupEnding("status") && !StringUtils.isBlank((status=payorResults.getValue("status", String.class)))) {
		            %></optgroup><%
				}
			}
		}%>
	</select>
	<button type="submit">Search</button>
	<button  class="right" title="Add new payor" type="button" onclick="location.href='payor_add.html'">Add Account</button>
<div class="spacer5"></div>
<table class="sortable-table">
	<tr class="gridHeader"><%
	GenerateUtils.writeSortedTableHeader(pageContext, "tranDate", "Date", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortableSettleState", "State", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortablePayorName", "Account", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortableTransType", "Transaction Type", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortableCardCompany", "Card Type", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "cardNumber", "Card Number", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "globalAccountId", "Card Id", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "totalAmount", "Amount", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortableLineItems", "Details", sortBy);	
	%>
	<th>Action</th>
	</tr><%	
results.setFormat("tranDate", "DATE:MM/dd/yyyy HH:mm:ss");
results.setFormat("totalAmount", "CURRENCY");
while(results.next()) {
    String status = results.getValue("payorStatus", String.class);
%>
	<tr>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("tranDate"))%></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("settleState"))%></td>
	    <td><%if("A".equals(status) || "1".equals(status)) { %><a href="payor_update.html?payorId=<%=results.getValue("payorId", long.class)%>&action=edit&referrer=back_office_transactions.html&referrer.payorId=<%=payorId %>&referrer.startDate=<%=StringUtils.encodeForURL(ConvertUtils.formatObject(startDate, "DATE:MM/dd/yyyy"))%>&referrer.endDate=<%=StringUtils.encodeForURL(ConvertUtils.formatObject(endDate, "DATE:MM/dd/yyyy")) %>"><%}%><%=StringUtils.encodeForHTML(results.getFormattedValue("payorName"))%><%if("A".equals(status) || "1".equals(status)) { %></a><%} %></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("transType"))%></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("cardCompany"))%></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("cardNumber"))%></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("globalAccountId"))%></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("totalAmount"))%></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("lineItems"))%></td>
	    <td align="center">
	       <button type="button" onclick="location.href='payor_receipt_view.html?tranId=<%=results.getValue("tranId", long.class)%>'" title="View a receipt for this transaction">View Receipt</button>
	       <%if("A".equals(status)) { %><button type="button" onclick="location.href='payor_update.html?payorId=<%=results.getValue("payorId", long.class)%>&amp;action=charge&amp;chargeAmount=<%=results.getValue("totalAmount", Number.class) %>&amp;referrer=back_office_transactions.html'" title="Charge this account now">New Charge</button><%} %></td>
	</tr><%
}
int rows = results.getRowCount();
int count;
if(pageIndex == 1 && rows < PaginationUtil.DEFAULT_PAGE_SIZE) { 
    count = rows;
} else { 
    count = sorter.executeCount(form);
}%>
<tr><td colspan="10" class="pagination-container">
<jsp:include page="include/pagination2.jsp">
    <jsp:param name="pageIndex" value="<%=pageIndex %>"/>
    <jsp:param name="pageSize" value="<%=pageSize %>"/>
    <jsp:param name="count" value="<%=count %>"/>
</jsp:include>
</td></tr>
</table>
</div>
</form>
<jsp:include page="include/footer.jsp"/>