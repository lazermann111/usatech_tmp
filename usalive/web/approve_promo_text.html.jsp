<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.io.ByteInput"%>
<%@page import="simple.app.Publisher"%>
<%@page import="com.usatech.app.MessageChainService"%>
<%@page import="com.usatech.usalive.hybrid.HybridServlet"%>
<%@page import="com.usatech.usalive.report.AsyncReportEngine"%>
<%@page import="com.usatech.app.MessageChainStep"%>
<%@page import="com.usatech.app.MessageChainV11"%>
<%@page import="com.usatech.app.MessageChain"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.io.Log"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.ResponseUtils"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.db.DataLayerMgr"%>

<%
	Log log = Log.getLog();
	InputForm inputForm = RequestUtils.getInputForm(request);
	String errorMsg=null;
	String message=null;
	try{
		int updateCount=DataLayerMgr.executeUpdate("APPROVE_PROMO_TEXT", inputForm, true);
		if(updateCount==1){
			message="Custom text for campaign email approved successfully";
			MessageChain mc = new MessageChainV11();
			MessageChainStep step = mc.addStep("usat.campaign.approved");
			step.addLongAttribute("campaignId", ConvertUtils.getLong(inputForm.get("campaignId")));
			AsyncReportEngine arEngine = (AsyncReportEngine) inputForm.getAttribute(HybridServlet.ATTRIBUTE_ASYNCH_REPORT_ENGINE);
			Publisher<ByteInput> publisher = arEngine.getPublisher();
			MessageChainService.publish(mc, publisher);
		}else{
			errorMsg="Failed to approve custom text for campaign email. Please try again";
		}
	}catch(Exception e){
		log.error("Failed to approve custom text for campaign email campaignId="+inputForm.get("campaignId"),e);
		errorMsg="Failed to approve custom text for campaign email. Please try again";
	}

%>
<jsp:include page="include/header.jsp"/>
<div>
<%if(errorMsg!=null){ %>
<div class="status-info-failure"><%=StringUtils.prepareHTML(errorMsg)%></div>
<%} else if(message!=null){ %>
<div class="status-info-success"><%=StringUtils.prepareHTML(message)%></div>
<%} %>
</div>
<jsp:include page="include/footer.jsp"/>