<%@page import="simple.text.MessageFormat"%>
<%@page import="com.usatech.layers.common.util.ExtendedCronExpression"%>
<%@page import="java.util.Date"%>
<%@page import="com.usatech.usalive.servlet.USALiveUtils"%>
<%@page import="simple.servlet.ResponseUtils"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="simple.servlet.NotAuthorizedException"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="com.usatech.report.ReportingUser.ActionType"%>
<%@page import="com.usatech.report.ReportingUser.ResourceType"%>
<%@page import="java.util.Calendar"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.results.Results"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="com.usatech.usalive.web.PayorUtils"%>
<%@page import="simple.xml.sax.MessagesInputSource"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
if(!user.hasPrivilege(ReportingPrivilege.PRIV_BACK_OFFICE))
    throw new NotAuthorizedException("You are not authorized to perform this function.");
InputForm form = RequestUtils.getInputForm(request);
boolean oneTime = form.getBoolean("oneTime", false, false);
long bankAcctId = form.getLong("bankAcctId", false, 0L);
if (bankAcctId > 0 && "POST".equalsIgnoreCase(request.getMethod())) {
	user.checkPermission(ResourceType.BANK_ACCT, ActionType.VIEW, bankAcctId); // TODO: user ActionType.EDIT eventually
    MessagesInputSource messages = RequestUtils.getOrCreateMessagesSource(request);
	String payorName = form.getString("name", true).trim();
	String payorEmail = form.getString("email", true);
	if(payorEmail == null || (payorEmail = payorEmail.trim()).isEmpty() || USALiveUtils.verifyEmail(payorEmail, "Account Contact", messages)) {	    
	    String cardNumber = form.getString("card", true).replaceAll("\\s+", ""); 
		int expMonth = form.getInt("expMonth", true, 0);
		int expYear = form.getInt("expYear", true, 0);
		String securityCode = form.getString("cvv", true).trim();
		String billingPostal = form.getString("postal", true).trim();
		if(oneTime) {
	        BigDecimal chargeAmount = RequestUtils.getAttribute(request, "chargeAmount", BigDecimal.class, true);
	        String chargeReference = RequestUtils.getAttribute(request, "chargeReference", String.class, false);        
	        String chargeDesc = RequestUtils.getAttribute(request, "chargeDesc", String.class, false);
	        if(PayorUtils.oneTimeChargePayor(user, payorName, payorEmail, bankAcctId, cardNumber, expMonth, expYear, securityCode, billingPostal, chargeAmount, chargeReference, chargeDesc, !StringUtils.isBlank(payorEmail) && "Y".equals(RequestUtils.getAttribute(request, "sendReceipt", String.class, false)), request) != null) {
	            RequestUtils.redirectWithCarryOver(request, response, "back_office.html");
	            return;
	        }
	    } else {
	    	boolean okay = true;
	    	String recurSchedule;
	        Character recurType = RequestUtils.getAttribute(request, "recurType", Character.class, false);
	        if(recurType == null)
	            recurSchedule = null;
	        else {
	            int recurHour = RequestUtils.getAttribute(request, "recurHour", Integer.class, true); 
	            switch(recurType.charValue()) {
	                case 'Y': // yearly
	                    Character recurYearlySubType = RequestUtils.getAttribute(request, "recurYearlySubType", Character.class, true);
	                    switch(recurYearlySubType) {
	                        case 'D':
	                            int recurYearlyDayMonthOfYear = RequestUtils.getAttribute(request, "recurYearlyDayMonthOfYear", Integer.class, true);
	                            int recurYearlyDayOfMonth = RequestUtils.getAttribute(request, "recurYearlyDayOfMonth", Integer.class, true); 
	                            recurSchedule = ExtendedCronExpression.buildExpression(recurHour, new int[] {recurYearlyDayOfMonth}, new int[] {recurYearlyDayMonthOfYear}, null, 0);
	                            break;
	                        case 'T':
	                            int recurYearlyWeekOfMonth = RequestUtils.getAttribute(request, "recurYearlyWeekOfMonth", Integer.class, true);
	                            int recurYearlyDayOfWeek = RequestUtils.getAttribute(request, "recurYearlyDayOfWeek", Integer.class, true); 
	                            int recurYearlyWeekMonthOfYear = RequestUtils.getAttribute(request, "recurYearlyWeekMonthOfYear", Integer.class, true);  
	                            recurSchedule = ExtendedCronExpression.buildExpression(recurHour, null, new int[] {recurYearlyWeekMonthOfYear}, new int[] {recurYearlyDayOfWeek}, recurYearlyWeekOfMonth);
	                            break;
	                        default:
	                            messages.addMessage("warn", "usalive-payor-edit-schedule-invalid", "The recurring schedule is not valid. Please select the schedule again");
	                            okay = false;
	                            recurSchedule = null;
	                    }
	                    break;
	                case 'M': // monthly
	                    Character recurMonthlySubType = RequestUtils.getAttribute(request, "recurMonthlySubType", Character.class, true);
	                    switch(recurMonthlySubType) {
	                        case 'N':
	                            int[] recurMonthlyDaysOfMonth = RequestUtils.getAttribute(request, "recurMonthlyDaysOfMonth", int[].class, true);
	                            int[] recurMonthlyDayMonths = RequestUtils.getAttribute(request, "recurMonthlyDayMonths", int[].class, false);
	                            recurSchedule = ExtendedCronExpression.buildExpression(recurHour, recurMonthlyDaysOfMonth, recurMonthlyDayMonths, null, 0);
	                            break;
	                        case 'K':
	                            int recurMonthlyWeekOfMonth = RequestUtils.getAttribute(request, "recurMonthlyWeekOfMonth", Integer.class, true);
	                            int[] recurMonthlyDayOfWeek = RequestUtils.getAttribute(request, "recurMonthlyDayOfWeek", int[].class, true);
	                            int[] recurMonthlyWeekMonths = RequestUtils.getAttribute(request, "recurMonthlyWeekMonths", int[].class, false);
	                            recurSchedule = ExtendedCronExpression.buildExpression(recurHour, null, recurMonthlyWeekMonths, recurMonthlyDayOfWeek, recurMonthlyWeekOfMonth);
	                            break;
	                        default:
	                            messages.addMessage("warn", "usalive-payor-edit-schedule-invalid", "The recurring schedule is not valid. Please select the schedule again");
	                            okay = false;
	                            recurSchedule = null;
	                    }
	                    break;
	                case 'W': // weekly
	                    int[] recurWeeklyDayOfWeek = RequestUtils.getAttribute(request, "recurWeeklyDayOfWeek", int[].class, true);
	                    recurSchedule = ExtendedCronExpression.buildExpression(recurHour, null, null, recurWeeklyDayOfWeek, 0);
	                    break;
	                case 'D': //daily
	                    recurSchedule = ExtendedCronExpression.buildExpression(recurHour, null, null, null, 0);
	                    break;
	                default:
	                    messages.addMessage("warn", "usalive-payor-edit-schedule-invalid", "The recurring schedule is not valid. Please select the schedule again");
	                    okay = false;
	                    recurSchedule = null;
	            }
	        }
	        BigDecimal recurAmount = RequestUtils.getAttribute(request, "recurAmount", BigDecimal.class, false);
	        String recurTimeZone = RequestUtils.getAttribute(request, "recurTimeZone", String.class, false);
	        Date recurStartTs = RequestUtils.getAttribute(request, "recurStartTs", Date.class, false);
	        Date recurEndTs = RequestUtils.getAttribute(request, "recurEndTs", Date.class, false);
	        int recurDescFormatCount = RequestUtils.getAttributeDefault(request, "recurDescFormat.count", int.class, 0);
	        String recurDescFormat; // 0 = date; 1 = installment
	        if(recurDescFormatCount > 0) {
	            StringBuilder sb = new StringBuilder();
	            for(int i = 1; i <= recurDescFormatCount; i++) {
	                String part = RequestUtils.getAttribute(request, "recurDescFormat." + i, String.class, false);
	                if(StringUtils.isBlank(part))
	                    continue;
	                if((i%2) == 1)
	                    part = MessageFormat.escape(part);
	                sb.append(part);
	            }
	            recurDescFormat = sb.toString();
	        } else
	               recurDescFormat = RequestUtils.getAttribute(request, "recurDescFormat", String.class, false);
	        if(!StringUtils.isBlank(recurDescFormat) && ConvertUtils.getFormat("MESSAGE", recurDescFormat) == null) {
	            messages.addMessage("warn", "usalive-payor-add-recur-desc-invalid", "The recurring description is not valid. Please enter it again");
	            okay = false;
	        }
	        if(okay) {
		        Long payorId = PayorUtils.addPayor(user, payorName, payorEmail, bankAcctId, cardNumber, expMonth, expYear, securityCode, billingPostal, recurAmount, recurSchedule, recurTimeZone, recurStartTs, recurEndTs, recurDescFormat, messages);
		        if(payorId != null) {
		        	RequestUtils.redirectWithCarryOver(request, response, "payor_update.html?payorId=" + payorId + "&action=edit");
	                return;
		        }
	        }
	    }
	}
}
Results bankAcctResults = DataLayerMgr.executeQuery("GET_ACTIVE_BANK_ACCTS", form);
%>
<jsp:include page="include/header.jsp"/>
<% if(bankAcctResults.isGroupEnding(0)) {
	%>You have no active bank accounts. Please set up a bank account or ask your company administrator to grant you access to an existing bank account.<%
} else {
%>
<div class="caption">ePort Online: <%if(oneTime) {%>One-Time Charge<%} else { %>Create<%} %> Account</div>
<div class="spacer10"></div>
<form autocomplete="off" class="payor-form" method="post" action="payor_add.html" data-toggle="validateInline">
<%
ResponseUtils.writeXsfrFormProtection(pageContext);
USALiveUtils.writeFormProfileId(pageContext);
%>
    <fieldset><legend>New Account</legend>
    <table>
    <tr><td><label>Account Name:<span class="required">*</span></label></td><td><input type="text" name="name" data-validators="required" placeholder="The company or customer name" title="Enter the name of the account that will identify the person or business paying" maxlength="50" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "name", String.class, false))%>"/></td></tr>
    <tr><td><label>Account Contact Email:</label></td><td><input type="email" name="email" data-validators="validate-email" placeholder="The email of the account" title="Enter the email of the account" maxlength="60" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "email", String.class, false))%>"/></td></tr>
    <tr><td><label>Pay To Bank Account:<span class="required">*</span></label></td><td><select name="bankAcctId" data-validators="required"><%
    long selectedBankAcctId = ConvertUtils.getLongSafely(RequestUtils.getAttribute(request, "bankAcctId", false), 0);
    if(selectedBankAcctId == 0L) {
        %><option value="" selected="selected">-- None --</option><%
    }
    while(bankAcctResults.next()) {
    	long acctId = bankAcctResults.getValue("bankAcctId", Long.class);
    	String country = bankAcctResults.getValue("bankCountryCd", String.class);
    	%><option value="<%=acctId %>"<%if(selectedBankAcctId == acctId) { %> selected="selected"<%} %> onselect="countryChange(this, '<%=StringUtils.prepareScript(country) %>')">
    	<%=StringUtils.prepareHTML(bankAcctResults.getFormattedValue("bankName"))%>
            - #<%=StringUtils.prepareHTML(bankAcctResults.getFormattedValue("accountNumber"))%>
            - <%=StringUtils.prepareHTML(bankAcctResults.getFormattedValue("accountTitle"))%>
         (<%=StringUtils.prepareHTML(bankAcctResults.getValue("currencyCd", String.class))%>) </option><%
    }
    %></select></td></tr>
    <tr><td><label>Card Number:<span class="required">*</span></label></td><td><input type="text" name="card" data-validators="required validate-card" placeholder="The credit card number" title="Enter the full card number that will be charged" maxlength="19" autocomplete="off" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "card", String.class, false))%>"/></td></tr>   
    <tr><td><label>Card Expiration:<span class="required">*</span></label></td><td>
    <fieldset class="no-border-grouper" data-validators="required" >
    <select name="expMonth" class="autosize" ><%
int month = ConvertUtils.getIntSafely(RequestUtils.getAttribute(request, "expMonth", false), 0);
int year = ConvertUtils.getIntSafely(RequestUtils.getAttribute(request, "expYear", false), 0);
Calendar now = Calendar.getInstance();
int currYear = now.get(Calendar.YEAR);%>
<option value=""<%if(month < 1 || month > 12) {%> selected="selected"<%} %>>Month</option><%
for(int m = 1; m <= 12; m++) {%>
    <option<%if(month == m) {%> selected="selected"<%} %> value="<%=m%>"><%=StringUtils.pad(Integer.toString(m), '0', 2, StringUtils.Justification.RIGHT)%></option><%
}%></select>
<select name="expYear" class="autosize">
<option value=""<%if(year < currYear) {%> selected="selected"<%} %>>Year</option><%
for(int y = currYear; y <= currYear + 10; y++) {%>
    <option<%if(year == y) {%> selected="selected"<%} %> value="<%=y%>"><%=y%></option><%
}%></select>
    </fieldset></td></tr>
    <tr><td><label>Card Security Code:<span class="required">*</span></label></td><td><input type="password" name="cvv" data-validators="required validate-cvv" placeholder="3 or 4 digit security code" title="Enter the 3 or 4 digit security code found on the back of the customer's card" maxlength="4" autocomplete="off" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "cvv", String.class, false))%>"/></td></tr>   
    <tr><td><label>Card Postal Code:<span class="required">*</span></label></td><td><input type="text" name="postal" data-validators="required" placeholder="Postal code of the card billing address" title="Enter the postal code of the billing address for the card that will be charged" maxlength="10" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "postal", String.class, false))%>"/></td></tr>
    <%if(oneTime) {%>
    <tr><td><label>Amount:<span class="required">*</span></label>
    </td><td><input type="hidden" name="oneTime" value="true"/>
    <input type="text" name="chargeAmount" data-validators="required validate-regex:'^([1-9]{1}[0-9]*|0)(\.[0-9]{0,2})?$' maximum:100000" placeholder="Amount to charge" title="Enter the amount to charge the payor" maxlength="15" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "chargeAmount", String.class, false))%>"/></td></tr>
    <tr><td><label>Invoice #:</label></td><td><input type="text" name="chargeReference" placeholder="An invoice number for this charge" title="Enter an invoice number for this charge" maxlength="60" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "chargeReference", String.class, false))%>"/></td></tr>
    <tr><td><label>Description:</label></td><td><input type="text" name="chargeDesc" placeholder="The description of this charge" title="Enter the description of this charge" maxlength="200" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "chargeDesc", String.class, false))%>"/></td></tr>
    <tr><td><label>Email Receipt?:</label></td><td><input type="checkbox" name="sendReceipt" title="Should a receipt be sent to the Account Contact Email?" value="Y"<%if("Y".equals(RequestUtils.getAttribute(request, "sendReceipt", String.class, false))) {%> checked="checked"<%} %>/></td></tr>
    <%}%>
    </table>    
    <div class="spacer10"></div>
    <button type="submit"><%if(oneTime) {%>Charge<%} else { %>Add<%} %> Account</button>
    </fieldset>
</form>
<script type="text/javascript">
function countryChange(option, countryCd) {
	var postalEl = $(option).getParent().form.postal;
	App.updateValidation(postalEl, 'required validate-postalcode-' + countryCd.toLowerCase(), 'Fixed.Postal' + countryCd.toUpperCase());
	if("US" == countryCd) {
		postalEl.placeholder = "The zip code of the billing address for the card"; 
		postalEl.title = "Enter the zip code of the billing address for the card that will be charged";
		var labelEl = postalEl.getPrevious("label");
		if(labelEl != null) {
			var text = labelEl.get("text");
			if(text != null && text.length > 0) {
				var newText = text.replace(/\bpostal\b/, "zip");
				if(text != newText)
					labelEl.set("html", newText);
			}
		}
	} else {
		postalEl.placeholder = "The postal code of the billing address for the card";
		postalEl.title = "Enter the postal code of the billing address for the card that will be charged";
		var labelEl = postalEl.getPrevious("label");
        if(labelEl != null) {
            var text = labelEl.get("text");
            if(text != null && text.length > 0) {
                var newText = text.replace(/\bzip\b/, "postal");
                if(text != newText)
                    labelEl.set("html", newText);
            }
        }
	}
}
</script>
<%}%>
<jsp:include page="include/footer.jsp"/>