<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.usatech.usalive.servlet.USALiveUtils"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.RequestUtils"%>
<jsp:include page="include/header.jsp"/>
<%InputForm form = RequestUtils.getInputForm(request);%>
<div class="caption"><%=form.getStringSafely("caption", "")%></div>
<%
Results results = USALiveUtils.getLinkResults(form, form.getStringSafely("usage", ""));
while(results.next()) {
%>
	<input type="button" onclick="document.location.href='<%=results.getFormattedValue("href")%>'" value="<%=results.getFormattedValue("title")%>"/>
<%}%>
<jsp:include page="include/footer.jsp"/>