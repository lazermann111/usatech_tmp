<%@page import="com.usatech.usalive.servlet.USALiveUtils"%>
<%@page import="simple.servlet.ResponseUtils"%>
<%@page import="com.usatech.usalive.web.RMAUtils"%>
<%@page import="simple.io.Log"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="java.io.InputStream"%>
<%@page import="simple.servlet.InputFile"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="simple.text.StringUtils,simple.servlet.RequestUtils,simple.results.Results"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.db.DataLayerMgr"%>
<%
	Log log = Log.getLog();
	UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
	InputForm inputForm = RequestUtils.getInputForm(request);
	boolean hasPri=false;
	if(user.isInternal()&&user.hasPrivilege(ReportingPrivilege.PRIV_CUSTOMER_SERVICE)){
		hasPri=true;
	}
	Results results=null;
	int imageWidth=150;
	String message=null;
	String status=null;
	if(hasPri){
		String actionType=inputForm.getString("actionType", false);
		if(actionType!=null && "POST".equalsIgnoreCase(request.getMethod())){
			if(actionType.equals("add")){
				Object partImage = inputForm.getAttribute("partImage");
				if(partImage instanceof InputFile) {
	        		InputFile inputFile = (InputFile)partImage;
	        		InputStream inputStream = inputFile.getInputStream();
	        		inputForm.set("partImage", inputStream);
	        	}else {
	        		InputStream inputStream = ConvertUtils.convert(InputStream.class, partImage);
	        		inputForm.set("partImage", inputStream);
	        	}
				try{
	        		DataLayerMgr.executeCall("INSERT_RMA_ALLOWED_PARTS", inputForm, true);
	        		message="Successfully added the part";
	        		status="status-info-success";
				}catch(Exception e){
					message="Failed to create rma allowed part. Please try again.";
					status="status-info-failure";
					log.error(message, e);
				}
				
			}else if(actionType.equals("delete")){
				try{
					DataLayerMgr.executeCall("DELETE_RMA_ALLOWED_PARTS", inputForm, true);
					message="Successfully deleted the part";
	        		status="status-info-success";
				}catch(Exception e){
					message="Failed to delete the part because it was in an existing RMA";
	        		status="status-info-failure";
				}
			}
		}
		results =  DataLayerMgr.executeQuery("GET_RMA_ALLOWED_PARTS", null);
	}
%>
<jsp:include page="include/header.jsp"/>
<%if(hasPri){ %>
<script type="text/javascript">

function onManageParts(){
	$("partNumber").value=$("partNumber").value.trim();
	$("partCategory").value=$("partCategory").value.trim();
	$("description").value=$("description").value.trim();
}
function onViewLargeImage(el, partNumber){
	if($("largeImage_"+partNumber).firstChild){
		el.set("text", "View Larger Image")
		$("largeImage_"+partNumber).set("html","");
	}else{
		el.set("text", "Close Larger Image")
		var imgEl = new Element('img', {
		    src: partNumber+'<%=RMAUtils.partImageSuffix%>',
		    width: 500,
		    height:600
		});
		$("largeImage_"+partNumber).set("html","");
		$("largeImage_"+partNumber).adopt(imgEl);
	}
}
window.addEvent('domready', function() { 
	new Form.Validator.Inline.Mask(document.rmaPartsForm);
});


</script>
<span class="caption">Parts</span>
<div class="spacer5"></div>
<form enctype="multipart/form-data" name="rmaPartsForm" id="rmaPartsForm" method="post" action="rma_manage_allowed_parts.html" onSubmit="return onManageParts();">
<%
ResponseUtils.writeXsfrFormProtection(pageContext);
USALiveUtils.writeFormProfileId(pageContext);
%>
<input type="hidden" id="actionType" name="actionType" value=""/>
<div id="message">
<% if(message!=null){ %>
<p class="<%=status%>"><%=StringUtils.prepareHTML(message)%></p>
<%} %>
</div>
<div class="selectSection">
<div class="sectionTitle">Add New Part</div>
<table class="input-rows">
<tbody>
<tr>
<td>Part Number: <span class="required">*</span></td><td><input required="required" id="partNumber" type="text" value="" name="partNumber"><td/>
</tr>
<tr>
<td>Part Category:</td><td><input id="partCategory" type="text" value="" name="partCategory"><td/>
</tr>
<tr>
<td>Part Image: <span class="required">*</span></td><td><input type="file" title="Select the jpeg file for the part" value="" required="required" name="partImage" id="partImage"><td/>
</tr>
<tr>
<td>Description: <span class="required">*</span></td><td><input id="description" class="maxLength:4000" type="text" size="100" value="" name="description"><td/>
</tr>
<tr>
<td colspan="2"><input id="addPart" type="submit" value="Add Part" onClick="$('rmaPartsForm').actionType.value='add';"><td/>
</tr>
</tbody>
</table>
</div>
</form>
<form name="rmaPartsForm" id="rmaPartsDeleteForm" method="post" action="rma_manage_allowed_parts.html">
<%
ResponseUtils.writeXsfrFormProtection(pageContext);
USALiveUtils.writeFormProfileId(pageContext);
%>
<input type="hidden" id="rmaAllowedPartsId" name="rmaAllowedPartsId" value=""/>
<input type="hidden" id="actionType" name="actionType" value=""/>
<table>
<tbody>
<tr class="headerRow">
<th>Part</th><th>Description</th><th>Category</th><th>Delete</th>
</tr>
<% while(results.next()) {
	String partNumber=results.getFormattedValue("partNumber");
%>
<tr>
<td><img src="<%=StringUtils.prepareScript(partNumber)%><%=RMAUtils.partImageSuffix%>" width="<%=imageWidth%>"/>
<br/>
Part #: <%=StringUtils.prepareHTML(partNumber)%>
<br/>
<div style="text-align:center"><a onClick="onViewLargeImage(this,'<%=StringUtils.prepareScript(partNumber)%>');" target="_blank"> View Larger Image </a></div></td>
<td><%=StringUtils.prepareHTML(results.getFormattedValue("description"))%></td>
<td><%=StringUtils.prepareHTML(results.getFormattedValue("partCategory"))%></td>
<td><input id="deletePart" type="submit" value="Delete" onClick="$('rmaPartsDeleteForm').actionType.value='delete';$('rmaAllowedPartsId').value=<%=StringUtils.prepareScript(results.getFormattedValue("rmaAllowedPartsId"))%>;"></td>
</tr>
<tr><td id="largeImage_<%=StringUtils.prepareScript(partNumber)%>" colspan="4"></td></tr>
<%} %>
</tbody>
</table>
</form>
<%}else{%>
	<p class="not-authorized">User '<%=StringUtils.prepareHTML(user.getUserName())%>' does not have permission to view this page. Please contact USA Technologies if you need access.</p>
<%}%>
<jsp:include page="include/footer.jsp"/>
