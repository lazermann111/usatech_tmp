/* Submit to the appropriate search action */
function openSelectWindow(url){
	if(url == null)
		url = './common/blank.html';
	window.name='search_frame';
	var win = window.open(url,'select_frame','location=no,resizable=yes,height=480,width=345,scrollbars=yes,toolbar=no,menubar=no');
	win.focus();
	return win;
}

/* Checks that dates are valid and at least one device is selected */
function validateSearch(form) {
	return validateDates(form) && validateDeviceList(form);
}

function validateDeviceList(form) {
	var radios = [form.radioAllDevices, form.radioSelectDevices];
	var allDevices = false;
	for(var i = 0; i < radios.length; i++) {
		if(radios[i].checked){
			allDevices = radios[i].value;
		}
	}
	if(allDevices == 1) return true;
	var terminalSelect = document.getElementById("terminalSelectFrame").contentWindow.document.getElementById("terminalIds");
	var s = "";
	for(var i = 0; i < terminalSelect.options.length; i++) {
		if(i > 0) s += ",";
		s += terminalSelect.options[i].value;
	}
	form.terminalIds.value = s;
	return s.length > 0;
}

/* Delete selected terminals in the select box */
function removeSelectedTerminals(selectBox){
	var box = document.getElementById(selectBox);
	var last = -1;

	if(box != null){
		if(box.options != null){
			for(var i=0;i < box.options.length;i++){
				if(box.options[i].selected){
					last = i;
					box.options[i]=null;
					i--;
				}
			}
		}
		if((last != -1) && (box.options.length != 0)){
			if(box.options[last] != null){
				box.options[last].selected = 'true';
			} else {
				box.options[box.options.length-1].selected = 'true';
			}
		}
		if(box.onchange) box.onchange();
	}
}

function terminalsSelected(form) {
	var terminalSelect = window.opener.document.getElementById("terminalSelectFrame").contentWindow.document.getElementById("terminalIds");
	for(var i = 0; i < terminalSelect.options.length; i++) {
		addHiddenInput(form, "terminalId", terminalSelect.options[i].value);
	}
	var radio = window.opener.document.getElementById("radioSelectDevices");
	radio.checked = true;
	radio.onclick();
	window.opener.focus();
	setTimeout("window.close()", 1);
}

function addHiddenInput(form, name, value) {
	var input = form.ownerDocument.createElement("input");
	input.type = "hidden";
	input.name = name;
	input.value = value;
	form.appendChild(input);
}

function toggleAll(button, checksName) {
	var checked;
	if(button.value == "Select All") {
		button.value = "Deselect All";
		checked = true;
	} else {
		button.value = "Select All";
		checked = false;
	}
	var checks = button.form.elements[checksName];
	if(typeof(checks.length) == "undefined") checks = new Array(checks);
	for(var i = 0; i < checks.length; i++) {
		checks[i].checked = checked;
	}
}

/*
function setParentTerminalIdValue(selectSrc) {
	var input = window.parent.document.getElementById("terminalIds");
	alert("Value = " + selectSrc.value);
	input.value = selectSrc.value;
	var selectRadio = window.parent.document.getElementById("radioSelectDevices");
	if(selectSrc.value)
		selectRadio.checked = "true";
	else
		selectRadio.checked = "false";
	switchDeviceSelect(window.parent.document, selectRadio);
}

function switchDeviceSelect(doc,radio){
	var iframe = doc.getElementById('terminalSelection');

	if(radio.id == 'radioSelectDevices'){
		if(!radio.checked){
			iframe.className = 'terminalSelectFrame';
		} else {
			iframe.className = 'terminalSelectFrameHidden';
		}
	} else {
		if(radio.checked){
			iframe.className = 'terminalSelectFrame';
		} else {
			iframe.className = 'terminalSelectFrameHidden';
		}
	}
}
*/

/* View the next level of granularity */
function drillDownItems(formId,searchType,selectedDest){
	if(window.parent != null){
		var target = document.getElementById(selectedDest);
		var html = '';

		var inputs = window.parent.document.getElementsByTagName('input');

		for(var i=0;i < inputs.length;i++){
			if((inputs[i].getAttribute('type')=='radio') || (inputs[i].getAttribute('type')=='checkbox')){
				if(inputs[i].checked){
					html += '\n<input type="hidden" name="'+inputs[i].name+'" value="'+inputs[i].value+'" />';
				}
			}
		}

		target.innerHTML = html;
	}

	var form = document.getElementById(formId);

	form.target = "select_frame";

	if((searchType == 'location') || (searchType == 'terminal')){
		form.action = searchType+'_select.i';

		form.submit();
	}
}

function initDates(formId){
	var form = document.getElementById(formId);
	var cur = new Date();
	var start = form.startDateId;
	var end = form.endDateId;

	if(((new Date(start.value)) == 'Invalid Date') || ((new Date(start.value)) == 'NaN')){
		start.value = (cur.getMonth()+1) + '/1/' + cur.getFullYear();
	}

	if(((new Date(end.value)) == 'Invalid Date') || ((new Date(end.value)) == 'NaN')){
		end.value = (cur.getMonth()+1) + '/' + cur.getDate() + '/' + cur.getFullYear();
	}
}

function validateDates(form) {
	form.startDateId.value=form.startDateId.value.trim();
	form.endDateId.value=form.endDateId.value.trim();
	var selectDateFormat = new RegExp("^([0-9]{1,2})/([0-9]{1,2})/([0-9]{4})$");
    var matches = selectDateFormat.exec(form.startDateId.value);
    if (!matches) {
    	alert('You must enter a valid start date.');
    	return false;
    }
    matches = selectDateFormat.exec(form.endDateId.value);
    if (!matches) {
    	alert('You must enter a valid end date.');
    	return false;
    }

	var start = new Date(form.startDateId.value);
	var end = new Date(form.endDateId.value);
	var valid = false;
	
	if((start == 'Invalid Date') || (end == 'Invalid Date')){
		alert('You must enter a valid start and end date.');
	} else {
		if(start > end){
			alert('The end date must occur after the start date.');
		} else {
			if(start > (new Date())){
				valid = confirm('The date range is in the future, do you want to run this report anyway?');
			} else {
				valid = true;
			}
		}
	}

	return valid;
}

function updateDeviceTypeList(targetId){
	var target = document.getElementById(targetId);
	var inputs = document.getElementsByTagName("input");
	var text = "";

	for(var i=0; i < inputs.length; i++){
		if(inputs[i].getAttribute("device") != null){
			if(inputs[i].checked){
				if(text != "") text += ","
				text += " " + inputs[i].getAttribute("device");
			}
		}
	}

	if(text == "") text = " None. Select some device types.";

	target.innerHTML = text;
}

function getTodayText() {
	var currentDate = new Date();
	var day   = currentDate.getDate();
    var month = currentDate.getMonth()+1;
    var year  = currentDate.getFullYear();
	return month+"/"+day+"/"+year;
}

function setDatesToLastWeek() {
    var currentDate = new Date();
    var lastw = new Date(currentDate.getTime()-1000*60*60*24*7);
    var date = lastw.getDate();
    var day = lastw.getDay();
    if (day == 4){
       var lsat = new Date(lastw.getTime()+1000*60*60*24*2);
       var lsun = new Date(lastw.getTime()-1000*60*60*24*4);}
    if (day == 3){
       var lsat = new Date(lastw.getTime()+1000*60*60*24*3);
       var lsun = new Date(lastw.getTime()-1000*60*60*24*3);
	}
    if (day == 2){
       var lsat = new Date(lastw.getTime()+1000*60*60*24*4);
       var lsun = new Date(lastw.getTime()-1000*60*60*24*2);
	}
    if (day == 1){
       var lsat = new Date(lastw.getTime()+1000*60*60*24*5);
       var lsun = new Date(lastw.getTime()-1000*60*60*24*1);
	}
    if (day == 0){
       var lsat = new Date(lastw.getTime()+1000*60*60*24*6);
       var lsun = new Date(lastw.getTime());
	}
    if (day == 5){
       var lsat = new Date(lastw.getTime()+1000*60*60*24*1);
       var lsun = new Date(lastw.getTime()-1000*60*60*24*5);
	}
    if (day == 6){
       var lsat = new Date(lastw.getTime());
       var lsun = new Date(lastw.getTime()-1000*60*60*24*6);
	}

    var lsund= lsun.getMonth()+1+"/"+lsun.getDate()+"/"+lsun.getFullYear();
    var lsatd= lsat.getMonth()+1+"/"+lsat.getDate()+"/"+lsat.getFullYear();
    setDates(lsund,lsatd);
}

function setDatesToWeekToDate() {
	var currentDate = new Date();
	var day = currentDate.getDate();
	var month = currentDate.getMonth() + 1;
	var year = currentDate.getFullYear();
	var offset = currentDate.getDay();
	var week;
	if(offset != 0) {
		day = day - offset;
		if(day < 1) {
			if(month == 1) day = 31 + day;
			if(month == 2) day = 31 + day;
			if(month == 3) {
				if(( year == 00) || ( year == 04)) {
					day = 29 + day;
				} else {
					day = 28 + day;
	   			}
			}
			if (month == 4) day = 31 + day;
			if (month == 5) day = 30 + day;
			if (month == 6) day = 31 + day;
			if (month == 7) day = 30 + day;
			if (month == 8) day = 31 + day;
			if (month == 9) day = 31 + day;
			if (month == 10) day = 30 + day;
			if (month == 11) day = 31 + day;
			if (month == 12) day = 30 + day;
			if (month == 1) {
				month = 12;
				year = year - 1;
			} else {
				month = month - 1;
	      	}
	  	}
	}

	week = month + "/" + day + "/" + year;
	var today = getTodayText();
	setDates(week,today);
}

function setDatesToMonthToDate(){
    var currentDate = new Date();
    var month = currentDate.getMonth()+1;
    var year  = currentDate.getFullYear();
    currentDate.setDate(1);
    var day = currentDate.getDate();
    var firstDIMonth = month+"/"+day+"/"+year;
    var today = getTodayText();
    setDates(firstDIMonth,today);
}

function setDatesToYesterday() {
 	var currentDate = new Date();
    currentDate.setDate(currentDate.getDate()-1);
    var month = currentDate.getMonth()+1;
    var year  = currentDate.getFullYear();
    var day   = currentDate.getDate();
    var yesterday = month+"/"+day+"/"+year;
    var today = getTodayText();
    setDates(yesterday,today);
}

function setDatesToToday() {
	var today = getTodayText();
    setDates(today,today);
}

function setDates(start,end) {
    $("startDateId").value=start;
    $("endDateId").value=end;
}

function setDatesToLastMonth(){
    var currentDate = new Date();
    var lastMonth=currentDate.getMonth();
    var lastMonthYear=currentDate.getFullYear();
    if(lastMonth==0){
    	lastMonth=12;
    	lastMonthYear=lastMonthYear-1;
    }
    var firstOfPreviousMonth = lastMonth+"/1/"+lastMonthYear;
    var firstOfCurrentMonth=new Date(currentDate.getFullYear(), currentDate.getMonth(), 1);
    var lastOfPreviousMonth=new Date(firstOfCurrentMonth-1);
    setDates(firstOfPreviousMonth,(lastOfPreviousMonth.getMonth()+1)+"/"+lastOfPreviousMonth.getDate()+"/"+lastOfPreviousMonth.getFullYear());
}
