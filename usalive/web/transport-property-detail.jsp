<%@page import="simple.text.StringUtils,simple.bean.ConvertUtils,simple.servlet.RequestUtils,simple.results.Results"%>
<%
Results transportTypes = RequestUtils.getAttribute(request, "transportTypes", Results.class, true);
Results transportPropertyValues = RequestUtils.getAttribute(request, "transportPropertyValues", Results.class, true);
Integer userCcsTransportTypeId=RequestUtils.getAttribute(request, "transportTypeId", Integer.class, true);
Integer userCcsTransportId=RequestUtils.getAttribute(request, "transportId", Integer.class, false);
Integer userCcsTransportInvalid=RequestUtils.getAttribute(request, "transportInvalid", Integer.class, false);
if(userCcsTransportId==null){
	userCcsTransportId=-1;
}
String userCcsTransportName=RequestUtils.getAttribute(request, "transportName", String.class, false);
if(userCcsTransportName==null){
	userCcsTransportName="";
}
%>
<form name="transportForm" id="transportForm" method="post" enctype="multipart/form-data">
	<table>
			<tr>
				<th><label for="transportStatus">Transport Status</label></th>
				<td><%if(userCcsTransportInvalid==null) {
						%><div id="transportStatus" class="status-info"></div><%} else{%> <%if(userCcsTransportInvalid>0) {
						%> <div id="transportStatus" class="status-info-failure">Invalid</div><%} else{%> 
						<div id="transportStatus" class="status-info-success">Valid</div>
						<%}}%>
				</td>
			</tr>
			<tr>
				<th><label for="transportName">Transport Name</label><span class="required">*</span></th>
				<td><input size="60" id="transportName" name="transportName" value="<%=StringUtils.prepareCDATA(userCcsTransportName)%>" maxlength="100" type="text" data-validators="required"/></td>
			</tr>
			<tr id="tranportTypeRow">
				<th><label for="transportTypeId">Transport Type</label></th>
				<td><select id="transportTypeId" name="transportTypeId" onchange="onTransportTypeChange($(this).value)" data-validators="required"><%
				while(transportTypes.next()) {
					Integer transportTypeId = transportTypes.getValue("transportTypeId", Integer.class);%>
					<option value="<%=transportTypeId%>"<%if(transportTypeId.equals(userCcsTransportTypeId)) {
						%> selected="selected"<%} %>><%=StringUtils.prepareHTML(transportTypes.getFormattedValue("transportTypeName")) %></option><%
	            }%>
				</select></td>
			</tr>
			<%
			while(transportPropertyValues.next()) {
				Integer transportTypeId = transportPropertyValues.getValue("transportTypeId", Integer.class);
				Integer propertyTypeId = transportPropertyValues.getValue("propertyTypeId", Integer.class);
				boolean required = 	transportPropertyValues.getValue("required", Boolean.class);
				String editor = transportPropertyValues.getValue("editor", String.class);
				if(editor == null)
					editor = "";
				String value = transportPropertyValues.getValue("value", String.class);
				String regex = transportPropertyValues.getFormattedValue("propertyTypeRegex");
			%>
			<tr id="tpt_<%=transportTypeId %>">
				<th><label for="tpv_<%=propertyTypeId %>"><%=StringUtils.prepareHTML(transportPropertyValues.getFormattedValue("propertyTypeName")) %></label>
				<%if(required) {%><span class="required">*</span><%} %>
				</th>
				<td><%if(editor.equalsIgnoreCase("FILE")) {
					String ua = request.getHeader("user-agent");%>
					<span class="file-input-overlay<%
		                    if(ua != null && ua.contains(" AppleWebKit/")) { %>-webkit<%} %>" id="tpv_<%=propertyTypeId %>_overlay">File of <%=(value == null ? 0 : value.length()) %> bytes</span>
					<input size="35" id="tpv_<%=propertyTypeId %>" name="tpv_<%=propertyTypeId %>" onchange="fileInputChanged(this);"
						maxlength="255" type="file"<%if(required) { %> data-validators="required"<%} %>/>
					<input type="hidden" name="ignore_tpv_<%=propertyTypeId %>" value="true"/>
					<%} else if(editor.equalsIgnoreCase("PASSWORD")) { %>
					<input size="35" id="tpv_<%=propertyTypeId %>" name="tpv_<%=propertyTypeId %>" value="<%=StringUtils.prepareCDATA(value) %>" maxlength="255" autocomplete="off" type="password" <%if(required) { %> data-validators="required"<%} %>/>
					<%} else if(editor.equalsIgnoreCase("CHECKBOX")) { %>
					<input id="tpv_<%=propertyTypeId %>" name="tpv_<%=propertyTypeId %>" value="true" type="checkbox"<%
						if(ConvertUtils.getBoolean(value, false)) {%> checked="checked"<%} %>/>
					<%} else {
						StringBuilder sb = new StringBuilder();
						if(required)
							sb.append("required");
						if(!StringUtils.isBlank(regex)) {
							if(sb.length() > 0)
								sb.append(' ');
							if(regex.charAt(0) == '/' && regex.charAt(regex.length() - 1) == '/')
								regex = regex.substring(1, regex.length() - 1);
							sb.append("validate-regex:'").append(StringUtils.prepareScript(regex)).append('\'');
						}%>
					<input size="35" id="tpv_<%=propertyTypeId %>" name="tpv_<%=propertyTypeId %>" value="<%=StringUtils.prepareCDATA(value) %>" maxlength="255" type="text"<%if(sb.length() > 0) { %> data-validators="<%=sb.toString() %>"<%} %>/>
					<%} %>
				</td>
			</tr>
			<%} %>
			<tr>
				<td colspan="2">
					<span class="required">* - Required</span>
				</td>
			</tr>
			<%if(userCcsTransportTypeId==3||userCcsTransportTypeId==5) {%>
				<tr id="dateFormatedFolderNote">
					<td colspan="2">
						<span class="required">NOTE - 'Date Formatted Folder' input needs to comply with Java's java.text.SimpleDateFormat's date and time pattern. If you specify the pattern, the system will create a date formatted folder under your ftp server's remote path.</span>
					</td>
				</tr>
			<%} %>	
	</table>
	<div id="transportTestStatus" class="status-none"></div><div id="waitImage" style="display:none"><img alt="Please wait" src="images/pleasewait.gif"/></div>
	<input type="button" name="testTransport" value="Test Transport" onclick="sendAjaxTestTransport();"/>
	<input type="button" name="saveTransport" value="<%if(userCcsTransportId<0) {
		%>Add Transport" onclick=" $('transportAction').value='add';<%} else{ %>Save" onclick="$('transportAction').value='update';<%} %>sendAjaxSaveTransport();"/>
	<input type="hidden" name="transportAction" id="transportAction" value=""/>
	<input type="hidden" name="transportId" value="<%=userCcsTransportId%>"/>
	<input type="hidden" name="transportNameOriginal" id="transportNameOriginal" value="<%=StringUtils.prepareCDATA(userCcsTransportName)%>"/>
	<input type="hidden" name="fragment" value="true"/>
	<input type="hidden" name="session-token" id="session-token" value="<%=StringUtils.prepareScript(RequestUtils.getSessionTokenIfPresent(request))%>" />
	</form>
	<script type="text/javascript">
	
	function sendAjaxTestTransport(){
		$('transportAction').value='test';
		var ajaxTestTransport =new Request({ 
			url : "test_transport.i", 
			method: 'post',
			evalScripts: true,
			data: $('transportForm')
		});
		new Form.Validator.Inline.Mask($('transportForm'));
		if($('transportForm').validate()){
			updateTransportStatus("Test Transport in progress.Please wait", "info");
			$("waitImage").style.display='block';
			ajaxTestTransport.send();
		}
	}
	
	function sendAjaxSaveTransport(){
		var ajaxSaveTransport = new Request({ 
			url : "save_transport.i", 
			method: 'post',
			evalScripts: true,
			data: $('transportForm')
		});
		new Form.Validator.Inline.Mask($('transportForm'));
		if($('transportForm').validate()){
			updateTransportStatus("Save Transport in progress.Please wait", "info");
			$("waitImage").style.display='block';
			ajaxSaveTransport.send();
		}
	}

	function updateTransportStatus(msg, type) {
		if(!msg || msg.trim().length == 0)
			$("transportTestStatus").set("html", "&amp;#160;");
		else 
			$("transportTestStatus").set("text", msg);
		$("transportTestStatus").className = "status-" + type;
		
		if(type == 'info-failure'){
			$("transportStatus").set('text','Invalid');
			$("transportStatus").className = "status-" + type;
		}else if(type == 'info-success'){
			$("transportStatus").set('text','Valid');
			$("transportStatus").className = "status-" + type;
		}
		$("waitImage").style.display='none';
	}
	
	if(!window.FormData) {
		window.addEvent("domready", function() {
			$("transportForm").getElements("input[type=file]").each(function(item) {
		    	var overlay = $(item.id + "_overlay");
		    	if(overlay) {
		    		overlay.setText("Your browser does not support uploading files with javascript. Please upgrade your browser to use this feature.");
		    		overlay.set("class", "error");
		    	}
		    	item.disabled = true;
		    	item.set("class", "hide");
		    });
		});
	}
	</script>
