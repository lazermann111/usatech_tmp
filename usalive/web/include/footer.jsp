<%@page import="com.usatech.report.custom.GenerateUtils"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.falcon.engine.Layout"%>
<%@page import="simple.falcon.servlet.FolioStepHelper"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="simple.servlet.SimpleServlet"%>
<% 
InputForm form = RequestUtils.getInputForm(request);
Layout layout = FolioStepHelper.determineLayout(form);
UsaliveUser user = (UsaliveUser) request.getSession().getAttribute(SimpleServlet.ATTRIBUTE_USER);
if(layout == Layout.FRAGMENT)
	return;
if(layout != Layout.UNFRAMED) {
	if (user == null) {%>
        <tr id="footerContainer">
            <td>
                <div id="footer"><a href="https://www.usatech.com" target="_blank">v.<%=StringUtils.prepareHTMLBlank(GenerateUtils.getApplicationVersion()) %> - USA Technologies, Inc. - &#169; Copyright <%=GenerateUtils.getCurrentYear() %> - Confidential</a></div>
            </td>
        </tr>
	</table>	
<%} else { %>	
    <div class="push"></div>
      </div>          
      </div>
      <div id="footer"><a href="https://www.usatech.com" target="_blank">v.<%=StringUtils.prepareHTMLBlank(GenerateUtils.getApplicationVersion()) %> - USA Technologies, Inc. - &#169; Copyright <%=GenerateUtils.getCurrentYear() %> - Confidential</a></div>
<% 
	}
}
String sessionToken = RequestUtils.getSessionToken(request);
if (!StringUtils.isBlank(sessionToken)) {
%>
<script type="text/javascript">
<%
	StringBuilder token = new StringBuilder("Form.injectSessionToken(\"").append(sessionToken).append("\"");
	if (user != null)
		token.append(", \"").append(user.getUserId()).append("\"");
	token.append(");");
	out.write(token.toString());
%>
</script>
<%}%>
</body></html>