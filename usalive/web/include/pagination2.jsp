<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<table class="pagination">
<%
int count = ConvertUtils.getInt(request.getParameter("count"));
int pageIndex = ConvertUtils.getInt(request.getParameter("pageIndex"), 1);
int pageSize = ConvertUtils.getInt(request.getParameter("pageSize"), PaginationUtil.DEFAULT_PAGE_SIZE);
String[] sortBy = RequestUtils.getAttribute(request, "sortBy", String[].class, false);
if(pageIndex > 1 || count >= PaginationUtil.DEFAULT_PAGE_SIZE) {
    int pageCount = (int)Math.ceil(count / (double) pageSize);
    if(pageIndex > pageCount)
        pageIndex = pageCount;
    int rangeStart;
    if(pageIndex < 8)
        rangeStart = 1;
    else if(pageCount < 16)
        rangeStart = 1;
    else if(pageIndex > pageCount - 8)
        rangeStart = pageCount - 14;
    else
        rangeStart = pageIndex - 7;
    int rangeEnd;
    if(pageIndex > pageCount - 8)
        rangeEnd = pageCount;
    else if(pageCount < 16)
        rangeEnd = pageCount;
    else if(pageIndex < 8)
        rangeEnd = 15;
    else
        rangeEnd = pageIndex + 7;
    
    %>
<tr>
    <td colspan="<%=rangeEnd - rangeStart + 7%>">Range <%=(pageIndex - 1) * pageSize + 1%> - <%=Math.min(pageIndex * pageSize, count)%> of <%=count%> Records</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>
    <%if (pageIndex > 1) { 
        %><button name="pageIndex" value="1" class="navigation"><%} %><img src="/images/skip_backward.png" alt="First Page" title="First Page" border="0" align="middle"/><%if (pageIndex > 1) { %></button><%} %>
    &nbsp;<%if (pageIndex > 1) { %><button name="pageIndex" value="<%=(pageIndex - 1)%>" class="navigation"><%} %><img src="/images/rewind.png" alt="Previous Page" title="Previous Page" border="0" align="middle"/><%if (pageIndex > 1) { %></button><%} %>
</td>
<td>&nbsp;</td>
<%
if(rangeStart > 1) { %><td><button name="pageIndex" value="<%=Math.max(rangeStart - 8, 8)%>" class="navigation"><%=Math.max(rangeStart - 15, 8)%>...</button></td><%}
for(int pi = rangeStart; pi <= rangeEnd; pi++) {
    %><td><%if(pi == pageIndex) {%><span class="selectedPage">&nbsp;<%=pi%>&nbsp;</span><%
    } else {%><button name="pageIndex" value="<%=pi%>" class="navigation"><%=pi%></button><%
    }%></td><%
}
if(rangeEnd < pageCount) { %><td><button name="pageIndex" value="<%=Math.min(rangeEnd + 8, pageCount - 8)%>" class="navigation"><%=Math.min(rangeEnd + 8, pageCount - 8)%>...</button></td><%}
%>
<td>&nbsp;</td>
<td>   
    <%if (pageIndex < pageCount) { %><button name="pageIndex" value="<%=pageIndex+1 %>" class="navigation"><%} %><img src="/images/fast_forward.png" alt="Next Page" title="Next Page" border="0" align="middle"><%if (pageIndex < pageCount) { %></button><%} %>
    &nbsp;<%if (pageIndex < pageCount) { %><button name="pageIndex" value="<%=pageCount%>" class="navigation"><%} %><img src="/images/skip_forward.png" alt="Last Page" title="Last Page" border="0" align="middle"><%if (pageIndex < pageCount) { %></button><%} %>
</td>
<td>&nbsp;</td>
</tr>
<tr>
<td colspan="<%=rangeEnd - rangeStart + 7%>">Go to Page <select tabindex="1" onchange="submitForm(this.form, $(this).get('value'))">
<%for (int index = 1; index <= pageCount; index++) {
%>
<option<%if(pageIndex == index) {%> selected="selected"<%}%> value="<%=index%>"><%=index%></option>
<%}%>
</select> of <b><%=pageCount%></b> | Show <select tabindex="2" name="pageSize" onchange="submitForm(this.form, Math.ceil(<%=count%> / $(this).get('value')))"><%
for(int size : new int[] {15,25,50,100,250,500,1000,5000,10000,25000}) {
    %><option<%if(size == pageSize) {%> selected="selected"<%}%> value="<%=size%>"><%=size%></option><%
}
%>
</select> Rows Per Page</td>
</tr>
<%} else {%>
<tr>
    <td>Range 1 - <%=count%> of <%=count%> Records</td>
</tr>
<%} %>
</table><%
if(sortBy != null) {
    for(String s : sortBy) {
    	%><input type="hidden" name="sortBy" value="<%=StringUtils.prepareCDATA(s)%>"/><%
    }
}%>
<script type="text/javascript">
function submitForm(form, pageIndex) {
	form = $(form);
	form.adopt(new Element("input", {type: "hidden", name: "pageIndex", value: pageIndex}));
	form.submit();
}
</script>