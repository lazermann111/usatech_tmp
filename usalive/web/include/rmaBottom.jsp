<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.text.StringUtils"%>
<% int rmaTypeId = ConvertUtils.getIntSafely(RequestUtils.getAttribute(request, "rmaTypeId", false), -1); %>
<br/>

<div id="rmaBottom" class="rmaBottom">
<ul>
<li>
Please allow 1-2 business days for the RMA request to be entered into the system
</li>
<li>
Please allow 3-5 business days for the RMA request to be processed
</li>
<% if (rmaTypeId !=1) { %>
<li>
For advanced replacements, depending on your location - please allow 5-7 business days for devices to arrive
</li>
<%} %>
<li>
Shipping directions will be provided for you within 1-2 business days
</li>
</ul>
Please direct all questions to our Customer Service Department at 1-888-561-4748 or email using<br/>
<a title="" href="mailto:rma@usatech.com">rma@usatech.com</a><br/>
<br/>
Please use the following return shipping address and reference your RMA # for proper processing and shipping directions will be sent:<br/>
<br/>
USA Technologies Inc.<br/>
Fulfillment Center
<% if (rmaTypeId == 4 || rmaTypeId == 5 || rmaTypeId == 6) { %><br/>
ATT 2G RMA Returns
<% } %><br/>
RMA # <%=RequestUtils.getAttribute(request, "rmaNumber", true) %>
<br/>
3103 Phoenixville Pike<br/>
Malvern, PA 19355<br/>
</div>
