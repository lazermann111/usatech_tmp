<%@page import="simple.bean.ConvertException"%>
<%@page import="simple.results.Results"%>
<%@page import="com.usatech.usalive.servlet.USALiveUtils"%>
<%@page import="com.usatech.report.custom.GenerateUtils"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.falcon.engine.Layout"%>
<%@page import="simple.falcon.servlet.FolioStepHelper"%>
<%@page import="simple.xml.sax.MessagesInputSource,simple.servlet.RequestUtils,simple.text.StringUtils,java.util.Locale,simple.translator.Translator,simple.servlet.SimpleServlet,java.util.List"%>
<% 
response.addHeader("Expires", "-1");
InputForm form = RequestUtils.getInputForm(request);
Translator translator = RequestUtils.getTranslator(request); 
String title = translator.translate("main-title", "USALive");
UsaliveUser user = (UsaliveUser) request.getSession().getAttribute(SimpleServlet.ATTRIBUTE_USER);
Layout layout = FolioStepHelper.determineLayout(form);
List<String> stylesheets = new ArrayList<String>();
if(layout == Layout.FRAGMENT) {
	StringUtils.split(RequestUtils.getAttribute(request, "extra-stylesheets", String.class, false), StringUtils.STANDARD_DELIMS, false, stylesheets);
	for(String ss : stylesheets) {
	    if(!StringUtils.isBlank(ss)) {
	        %><link href="<%=StringUtils.prepareCDATA(RequestUtils.addLastModifiedToUri(request, null, ss.trim())) %>" type="text/css" rel="stylesheet"/>
	        <%
	    }
	}
	return;
}
if(layout == null)
	layout = Layout.FULL;
%><!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <head>
        <meta charset="<%=StringUtils.prepareCDATA(response.getCharacterEncoding())%>">
        <meta name="description" content="">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="Cache-Control" content="no-cache" />
        <meta http-equiv="Pragma" content="no-cache" />
        <title><%=StringUtils.prepareHTMLBlank(title)%><%if(user != null && !StringUtils.isBlank(user.getDisplayName())) {%> - <%=StringUtils.prepareHTMLBlank(user.getDisplayName()) %><%}
  		String subtitle = RequestUtils.getAttribute(request, "subtitle", String.class, false); 
        if(!StringUtils.isBlank(subtitle)) {
            %> <%=StringUtils.prepareHTMLBlank(subtitle) %><%
        }%></title>
        <base href="<%=StringUtils.prepareCDATA(RequestUtils.getBaseUrl(request)) %>" />
        <%
StringUtils.split(translator.translate("app-stylesheets"), StringUtils.STANDARD_DELIMS, false, stylesheets);
if(user != null)
	StringUtils.split(user.getUserProperties().get("CUSTOM_CSS_PATH"), StringUtils.STANDARD_DELIMS, false, stylesheets);
StringUtils.split(RequestUtils.getAttribute(request, "extra-stylesheets", String.class, false), StringUtils.STANDARD_DELIMS, false, stylesheets);
for(String ss : stylesheets) {
    if(!StringUtils.isBlank(ss)) {
        %><link href="<%=StringUtils.prepareCDATA(RequestUtils.addLastModifiedToUri(request, null, ss.trim())) %>" type="text/css" rel="stylesheet"/>
        <%
    }
}%><link rel="stylesheet" type="text/css" media="all" href="<%=RequestUtils.addLastModifiedToUri(request, null, "css/calendar-blue.css")%>" title="blue" />
<%
for(String s : new String[] {"scripts/mootools-core-1.4.5-full-nocompat.js", "scripts/mootools-more-1.4.0.1.js","scripts/meio-mask-2.0.1.0.js","scripts/app.js"}) {
	%><script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, s)%>"></script>
	   <%
}
%>
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "scripts/calendar-control.js") %>"></script>
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "scripts/calendar-control-en.js") %>"></script>
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "scripts/calendar-control-setup.js") %>"></script>
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "scripts/calendar-control-swap-dates.js") %>"></script>
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "scripts/dateTime.js") %>"></script>
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "scripts/site.js") %>"></script>
<%
Locale locale = RequestUtils.getLocale(request);
if(locale != null) {%>
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "scripts/mootools-more-locale-" + locale.toString() + ".js") %>"></script>
<%}
%><script type="text/javascript">
<%=GenerateUtils.getCountriesConfigScript(translator)%><%
if(layout == Layout.FULL && user != null) {
	%>
addReportRunner();
	<%
}
if(layout == Layout.FULL) {%>
if(top.frames.length != 0)
    top.location = self.document.location;
<%
}
%>      </script>
    </head><%
if(layout == Layout.UNFRAMED) {
    %><body class="unframed"><%
    return;
}%>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->                
        <%if (user != null) {%>
        <div class="wrapper">
            <%if(layout == Layout.FULL) {%>
            <div id="banner">
                <a href="/home.i" title="My Homepage" class="headerLogo"></a>
                <div id="banner-center">
                    <div id="banner-welcome"><%if(user != null && !StringUtils.isBlank(user.getDisplayName())) { 
                    %><%=StringUtils.prepareHTMLBlank(user.getDisplayName())%><% }%></div>
                </div>
                <%if(user != null) {%>
                <fieldset id="report-request-group"><legend>Report Requests:</legend>
                    <div id="report-requests">
                        <div id="reports-pending-count"></div>
                        <a title="View Reports" href="./report_request_history.i?isByProfile=false" id="reports-ready-count"><%=StringUtils.prepareHTML(translator.translate("report-request-summary-retrieval", "Loading..."))%></a>
                    </div>
                </fieldset>
                <div id="report-ready-box" class="request-complete-box">
                <div class="box-top-left"></div>
                <div class="box-top"></div>
                <div class="box-top-right"></div>
                <div class="box-left"></div>
                <div class="box-container"><div class="close-btn" onclick="$('report-ready-box').setStyle('opacity', 0);" 
                    onmouseenter="$(this).setStyle('backgroundImage', 'url(images/info_dialog_close_hover.png)');"
                    onmouseleave="$(this).setStyle('backgroundImage', 'url(images/info_dialog_close.png)');"
                    onmousedown="$(this).setStyle('backgroundImage', 'url(images/info_dialog_close_down.png)');"
                    onmouseup="$(this).setStyle('backgroundImage', 'url(images/info_dialog_close.png)');"></div>
                    <div id="report-ready-content"></div>
                </div>
                <div class="box-right"></div>
                <div class="box-bottom-left"></div>
                <div class="box-bottom"></div>
                <div class="box-bottom-right"></div>
                </div>
                <%} %>
            </div>                            
            <%
            int selectedMenuItem = form.getInt("selectedMenuItem", false, 0);
            String requestLink;
            String uri = request.getRequestURI();
            if (uri.contains("be_user.i")) {
            	requestLink = "./home.i";
            	selectedMenuItem = 0;
            } else {
	            StringBuilder sb = new StringBuilder();
	            if (!uri.startsWith("."))
	            	sb.append(".");
	            if (!uri.startsWith("/"))
            		sb.append("/");
	            sb.append(uri);
	            if (!StringUtils.isBlank(request.getQueryString()))
	            	sb.append("?").append(request.getQueryString());
	            requestLink = sb.toString();
            }
            Results results = USALiveUtils.getLinkResults(form, "M");
            long userId = 0;
            if (user != null) {
            	userId = user.getUserId();
            	Integer previousMenuItemObj = RequestUtils.getAttribute(request, SimpleServlet.ATTRIBUTE_SELECTED_MENU_ITEM, Integer.class, false, "session");
            	int previousMenuItem = previousMenuItemObj == null ? 0 : previousMenuItemObj.intValue();
            	if (selectedMenuItem > 0) {
            		if (selectedMenuItem != previousMenuItem)
            			RequestUtils.setAttribute(request, SimpleServlet.ATTRIBUTE_SELECTED_MENU_ITEM, selectedMenuItem, "session");
            	} else if (previousMenuItem > 0) {
            		Results menuResults = results.clone();
            		while (menuResults.next()) {
            			if (requestLink.contains(menuResults.getFormattedValue("href"))) {
            				selectedMenuItem = menuResults.getValue("id", int.class);
            				break;
            			}
            		}
            		if (selectedMenuItem <= 0)
            			selectedMenuItem = previousMenuItem;
            	}
            }
            results.addGroup("category");
            StringBuilder menu = new StringBuilder();
            StringBuilder menuCategory = new StringBuilder();
            StringBuilder subMenu = null;
            int selectedCategoryNumber = 0;
            int currentCategoryNumber = 0;
            String category;
            while(results.next()) {                	
            	if(results.isGroupBeginning("category")) {
            		currentCategoryNumber++;
            		menuCategory.setLength(0);
            	}
            	int id = results.getValue("id", int.class);
            	String link = results.getFormattedValue("href");
            	StringBuilder href = new StringBuilder(link).append(link.indexOf('?') >= 0 ? "&" : "?").append("selectedMenuItem=").append(id);
            	boolean ignoreProfileId = results.getValue("ignoreProfileId", Boolean.class);
            	if(!ignoreProfileId && userId > 0)
            		href.append("&profileId=").append(userId);
            	menuCategory.append("<li");
            	if (selectedCategoryNumber == 0 && (selectedMenuItem > 0 && selectedMenuItem == id || selectedMenuItem <= 0 && requestLink.contains(link))) {            		
            		selectedCategoryNumber = currentCategoryNumber;
            		menuCategory.append(" class=\"submenuItemSelected\"");
            	}
            	menuCategory.append("><a title=\"").append(StringUtils.prepareCDATA(results.getFormattedValue("description"))).append("\" href=\"")
            			.append(StringUtils.prepareCDATA(href.toString())).append("\" target=\"_top\">")
            			.append(StringUtils.prepareHTML(results.getFormattedValue("title").toLowerCase())).append("</a></li>");
                if(results.isGroupEnding("category")) {
                	menu.append("<li class=\"topMenuItem");
                	if (selectedCategoryNumber == currentCategoryNumber) {
                		menu.append(" topMenuItemSelected");
                		subMenu = new StringBuilder(menuCategory);
                	}
                	if (subMenu == null)
            			subMenu = new StringBuilder(menuCategory);
                	category = results.getFormattedValue("category").toLowerCase();
                	menu.append("\"><a href=\"#\"><img src=\"/images/").append(category).append(".png\" />").append(StringUtils.prepareHTML(category)).append("</a><ul>");
                	menu.append(menuCategory);
                	menu.append("</ul></li>");
                }
            } 
            %>
			<div id="menuContainer">
              	<div class="topMenu">
                	<ul><%=menu%></ul>
              	</div>
                <%if (subMenu != null && subMenu.length() > 0) {%>
                <div class="submenu">
                	<ul><%=subMenu%></ul>
                </div>
				<%}%>
            </div>
            <%} %>
            <div id="content"<%if(layout != Layout.FULL) {%> class="unframed"<%} %>>
                <%
            List<MessagesInputSource.Message> messages = RequestUtils.getMessages(request);
            if(!messages.isEmpty()) {%>
            <table class="messages"><%
            int c = 0;    
            for(MessagesInputSource.Message message : messages) {
                if("header".equalsIgnoreCase(message.getType())) {%>
                    <tr class="message-<%=StringUtils.prepareCDATA(message.getType()) %>"><td><%=message.toHtml() %></td></tr><%
                    } else if(!"footer".equalsIgnoreCase(message.getType()))
                    	c++;
                }
                if(c > 0) {%>
                <tr><td><ul class="message-list"><%
	                for(MessagesInputSource.Message message : messages) {
	                    if(!"header".equalsIgnoreCase(message.getType()) && !"footer".equalsIgnoreCase(message.getType())) {%>
	                    <li class="message-<%=StringUtils.prepareCDATA(message.getType()) %>"><%=message.toHtml() %></li><%
	                    }
	                }%>
	            </ul></td></tr><%
	            }    
                for(MessagesInputSource.Message message : messages) {
                    if("footer".equalsIgnoreCase(message.getType())) {%>
                    <tr class="message-<%=StringUtils.prepareCDATA(message.getType()) %>"><td><%=message.toHtml() %></td></tr><%
                    }
                }%>
            </table><%
            }
        }        
%>