<%@page import="java.sql.Connection"%>
<%@page import="com.usatech.usalive.web.RMAType"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="com.usatech.usalive.web.RMAUtils"%>
<%
	UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
	boolean hasPri=false;
	if((user.isInternal()&&user.hasPrivilege(ReportingPrivilege.PRIV_CUSTOMER_SERVICE))||user.hasPrivilege(ReportingPrivilege.PRIV_RMA)){
			hasPri=true;
	}
	String rmaNumber=null;
	InputForm inputForm = RequestUtils.getInputForm(request);
	Results rmaDeviceResults=null;
	Results rmaInfoResult=null;
	String rmaCreateTs=null;
	String rmaDescription=null;
	int rmaReplacementQuantity=0;
	String carrierInfo=null;
	String rmaType=null;
	//@Todo test
	//inputForm.set("rmaId", 3);
	if(hasPri){
		Connection conn=(Connection)request.getAttribute("RMA_DB_CONN");
		if(conn==null){
			rmaInfoResult =  DataLayerMgr.executeQuery("GET_RMA_INFO", inputForm);
		}else{
			rmaInfoResult =  DataLayerMgr.executeQuery(conn,"GET_RMA_INFO", inputForm);
		}
		if(rmaInfoResult.next()){
			rmaNumber=rmaInfoResult.getFormattedValue("rmaNumber");
			request.setAttribute("rmaNumber", rmaNumber);
			rmaCreateTs=RMAUtils.rmaDateFormat.format(ConvertUtils.convert(Date.class, rmaInfoResult.get("rmaCreateTs")));
			rmaDescription=rmaInfoResult.getFormattedValue("rmaDescription");
			rmaReplacementQuantity=ConvertUtils.getInt(rmaInfoResult.get("rmaReplacementQuantity"));
			String carrierDescription=rmaInfoResult.getFormattedValue("carrierDescription");
			String carrierAccountNumber=rmaInfoResult.getFormattedValue("carrierAccountNumber");
			StringBuilder sb=new StringBuilder(rmaInfoResult.getFormattedValue("carrier"));
			
			if(!StringUtils.isBlank(carrierAccountNumber)){
				sb.append(" Account #: ").append(carrierAccountNumber);
			}
			if(!StringUtils.isBlank(carrierDescription)){
				sb.append(" (").append(carrierDescription+")");
			}
			carrierInfo=sb.toString();
			rmaType=RMAType.getByValue(ConvertUtils.getInt(rmaInfoResult.get("rmaTypeId"))).getDescription();
		}
		if(conn==null){
			rmaDeviceResults =  DataLayerMgr.executeQuery("GET_RMA_DEVICES", inputForm);
		}else{
			rmaDeviceResults =  DataLayerMgr.executeQuery(conn,"GET_RMA_DEVICES", inputForm);
		}
	}
%>
<%if(hasPri){ %>
<div class="instruct center">USA Technologies Return Material Authorization (RMA) Request Form:</div> 
<BR/>
<table class="folio" border="1">
<tr class="headerRowRMA"><th> RMA # Assigned: <%=StringUtils.prepareHTML(rmaNumber)%></th><th></th></tr>
<tr><td>Date of Request:</td><td><%=rmaCreateTs%></td></tr>
<tr><td>USALive User:</td><td><%=rmaInfoResult.getFormattedValue("rmaUserName")%></td></tr>
<tr><td>USALive Customer:</td><td><%=StringUtils.prepareHTML(rmaInfoResult.getFormattedValue("rmaCustomerName"))%></td></tr>
<tr><td>RMA Type:</td><td><%=rmaType %></td></tr>
<tr><td>Replacement Kit Quantity:</td><td><%=rmaReplacementQuantity%></td></tr>
<tr><td>RMA Description:</td><td> <%=rmaDescription%></td></tr>
</table>
<br/>
<table class="folio" border="1">
<tr class="headerRowRMA"><th></th><th>ePort Serial #</th><th>Rental</th><th>Fee</th><th>Telemetry Only</th><th>Item #</th><th>Description of issue</th></tr>
<%	int i=1; while(rmaDeviceResults!=null&& rmaDeviceResults.next()) {
				    String rentalFee=StringUtils.prepareCDATA(rmaDeviceResults.getFormattedValue("rentalFee"));
				    String terminalFee=StringUtils.prepareCDATA(rmaDeviceResults.getFormattedValue("terminalFee"));
				    String quickStartFee=StringUtils.prepareCDATA(rmaDeviceResults.getFormattedValue("quickStartFee")); 
				    %>
				    <tr>
				    <td><%=i++%></td>
				    <td><%=StringUtils.prepareCDATA(rmaDeviceResults.getFormattedValue("deviceSerialCd")) %></td>
				    <td><%=StringUtils.prepareCDATA(rmaDeviceResults.getFormattedValue("isRental")) %></td>
				    <td><%if (!rentalFee.equals("")) {%><%=rentalFee%><br><%}%>
				    	<%if (!terminalFee.equals("")) {%><%=terminalFee%><br><%}%>
				    	<%if (!quickStartFee.equals("")) {%><%=quickStartFee%><%}%></td>
				    <td><%=StringUtils.prepareCDATA(rmaDeviceResults.getFormattedValue("isTelemetryOnly")) %></td>
				    <td><%=StringUtils.prepareCDATA(rmaDeviceResults.getFormattedValue("itemNumber")) %></td>
				    <td><%=StringUtils.prepareCDATA(rmaDeviceResults.getFormattedValue("issue")) %></td>
				    </tr>
				    <%
				}
				%>
</table>
<br/>
<br/>
<table class="folio" border="1">
<tr class="headerRowRMA"><th colspan=2> Shipping Info:</th></tr>
<tr><td>Replacement Shipping Carrier:</td><td><%=carrierInfo%></td></tr>
<tr><td>Contact Name:</td><td><%=StringUtils.prepareCDATA(rmaInfoResult.getFormattedValue("contactName"))%></td></tr>
<tr><td>Address:</td><td><%=StringUtils.prepareCDATA(rmaInfoResult.getFormattedValue("address"))%></td></tr>
<tr><td>City:</td><td><%=StringUtils.prepareCDATA(rmaInfoResult.getFormattedValue("city"))%></td></tr>
<tr><td>State:</td><td><%=StringUtils.prepareCDATA(rmaInfoResult.getFormattedValue("state"))%></td></tr>
<tr><td>Postal Code:</td><td><%=StringUtils.prepareCDATA(rmaInfoResult.getFormattedValue("postalCd"))%></td></tr>
<tr><td>Country:</td><td><%=StringUtils.prepareCDATA(rmaInfoResult.getFormattedValue("countryCd"))%></td></tr>
<tr><td>Email:</td><td><%=StringUtils.prepareCDATA(rmaInfoResult.getFormattedValue("email"))%></td></tr>
<tr><td>Phone:</td><td><%=StringUtils.prepareCDATA(rmaInfoResult.getFormattedValue("phone"))%></td></tr>
</table>
<jsp:include page="include/rmaBottom.jsp"/>
<%}else{%>
	<p class="not-authorized">User '<%=user.getUserName() %>' does not have permission to view this page. Please contact USA Technologies if you need access.</p>
<%}%>
