<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.Date"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="com.usatech.layers.common.util.CampaignUtils"%>
<%@page import="com.usatech.usalive.web.CampaignUsaliveUtils"%>

<%
	InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
	Results deviceCheckResults=DataLayerMgr.executeQuery("GET_DEVICE_WORKS_FOR_PRODUCT", inputForm);
	String deviceSN;
	Integer works;
	
%>
	<select size="16" name="deviceIds" id="deviceIds" style="width: 200px; height: 400px;">
			<%
			while(deviceCheckResults.next()) {
				deviceSN = deviceCheckResults.getValue("deviceSN", String.class);
				works = deviceCheckResults.getValue("works", Integer.class);
			    %>
			    <option value="<%=deviceSN%>" id="<%=deviceSN%>" <%if (works == 0){%>style="color: red"<%}%>><%=StringUtils.prepareHTML(deviceCheckResults.getFormattedValue("deviceSN")) %></option>
			    <%
			}
			%>
	</select>


