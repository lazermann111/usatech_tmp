<%@page import="com.usatech.report.ReportingUser.ActionType"%>
<%@page import="com.usatech.report.ReportingUser.ResourceType"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.GenerateUtils"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="simple.db.Call.Sorter"%>
<%@page import="simple.io.Log"%>
<%@page import="com.usatech.ec2.EC2ClientUtils"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%@page import="simple.servlet.ResponseUtils"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.Date"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="simple.text.StringUtils"%>
<%
	Log log = Log.getLog();
	UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
	InputForm inputForm = RequestUtils.getInputForm(request);
	int consumerId=inputForm.getInt("consumerId", true, -1);
	user.checkPermission(ResourceType.PREPAID_CONSUMER, ActionType.VIEW, consumerId);
	boolean hasPri=false;
	if(user.isInternal()||user.hasPrivilege(ReportingPrivilege.PRIV_MANAGE_PREPAID_CONSUMERS)){
		hasPri=true;
	}
	Results results=DataLayerMgr.executeQuery("GET_USER_INFO", inputForm);
	Results carrierResults = DataLayerMgr.executeQuery("GET_SMS_GATEWAYS",null);
	Results regionResults = DataLayerMgr.executeQuery("GET_REGIONS_FOR_CONSUMER", new Object[]{consumerId});
	Results activeCardsResults = DataLayerMgr.executeQuery("GET_ACTIVE_CONSUMER_CARDS_OPER_SERVICED", inputForm);
	String currencyFormat="CURRENCY";
	activeCardsResults.setFormat("cardBalance", currencyFormat);
	StringBuilder cardOptions=new StringBuilder();
	int activeCardsCount=0;
	while(activeCardsResults.next()) {
		activeCardsCount++;
		long consumerAcctId = activeCardsResults.getValue("consumerAcctId", Long.class);
		long cardId = activeCardsResults.getValue("cardId", Long.class);
		String cardNum = activeCardsResults.getValue("cardNum", String.class);
		cardOptions.append("<option value='"+consumerAcctId+"'>"+cardNum+" ("+cardId+") "+activeCardsResults.getFormattedValue("cardBalance") +"</option>");
	}
	String cardOptionsStr=cardOptions.toString();
	Long selectedRegionId=null;
	if(!regionResults.isGroupEnding(0)) {
		selectedRegionId = RequestUtils.getAttribute(request, "region", Long.class, false);
	}
	boolean isSimpleUser=false;
	boolean isMoreEnabled=false;
	boolean isPayrollDeduct=false;
	String actionType2 = RequestUtils.getAttribute(request, "actionType2", String.class, false);
	Results consumerCardResults=null;
	String[] sortBy=null;
	int pageIndex = inputForm.getInt("pageIndex", false, 1);
	int pageSize = inputForm.getInt("pageSize", false, PaginationUtil.DEFAULT_PAGE_SIZE);
	Sorter sorter=null;
	if(actionType2!=null && actionType2.equals("search")){
		sorter = DataLayerMgr.getGlobalDataLayer().findCall("GET_CONSUMER_CARDS_BY_CONSUMER").getSorter();
		sortBy = ConvertUtils.toUniqueArray(inputForm.getStringArray("sortBy", false), String.class);
		inputForm.setAttribute("sortBy", sortBy);
		consumerCardResults = sorter.executeSorted(inputForm, pageSize, pageIndex, sortBy);
	}

	String baseUrl=RequestUtils.getBaseUrl(request, false);
	String postalDesc="";
	String postalPattern="";

%>
<jsp:include page="include/header.jsp"/>

<%if(hasPri){ %>
<div id="message"></div>
<form name="consumerForm" id="consumerForm" action="consumer_edit.html" autocomplete="off">
<div class="selectSection">
<div class="sectionTitle">Consumer</div>
<br/>
<input id="actionType" type="hidden" name="actionType" value="search">
<input id="consumerId" type="hidden" name="consumerId" value="<%=consumerId%>">
<div class="sectionRow">
<%if(results.next()){
	int consumerTypeId=ConvertUtils.getIntSafely(results.getFormattedValue("consumerTypeId"),5);
	isPayrollDeduct = consumerTypeId == 9 ? true : false;
	int preferredCommType=ConvertUtils.getIntSafely(results.getFormattedValue("preferredCommType"), 1);
	int carrierId=ConvertUtils.getIntSafely(results.getFormattedValue("carrierId"), -1);
	String email=results.getFormattedValue("email");
	String mobile=results.getFormattedValue("mobile");
	isMoreEnabled = ConvertUtils.getBoolean("moreEnabledInd");

	if(StringUtils.isBlank(email) && StringUtils.isBlank(mobile)){
		isSimpleUser=true;
	}
	String country=results.getFormattedValue("country");
	String firstname=RequestUtils.getAttribute(request, "firstname", String.class, false);
	if(firstname==null){
		firstname=results.getFormattedValue("firstname");
	}
	String lastname=RequestUtils.getAttribute(request, "lastname", String.class, false);
	if(lastname==null){
		lastname=results.getFormattedValue("lastname");
	}
	String address1=RequestUtils.getAttribute(request, "address1", String.class, false);
	if(address1==null){
		address1=results.getFormattedValue("address1");
	}
	String postal=RequestUtils.getAttribute(request, "postal", String.class, false);
	if(postal==null){
		postal=results.getFormattedValue("postal");
	}
	if(!StringUtils.isBlank(country)&&"US".equalsIgnoreCase(country)){
		postalDesc="5-digit zip code";
	}else{
		postalDesc="postal code";
	}
	for(GenerateUtils.Country c : GenerateUtils.getCountries())	{
		if(c.code.equalsIgnoreCase(country)) {
			postalPattern=StringUtils.prepareHTML(StringUtils.prepareScript(c.postalRegex));
		}
	}
	String department = RequestUtils.getAttribute(request, "department", String.class, false);
	if (department == null) {
		department=results.getFormattedValue("department");
	}
	String customerAssignedId = RequestUtils.getAttribute(request, "customerAssignedId", String.class, false);
	if (customerAssignedId == null) {
		customerAssignedId=results.getFormattedValue("customerAssignedId");
	}
	%>
		<input id="consumerTypeId" type="hidden" name="consumerTypeId" value="<%=results.getValue("consumerTypeId")%>">
		<input id="email" type="hidden" name="email" value="<%=email%>">
		<input id="mobile" type="hidden" name="mobile" value="<%=mobile%>">
		<input id="preferredCommType" type="hidden" name="preferredCommType" value="<%=preferredCommType%>">
		<%if(carrierId>0){ %>
			<input id="carrierId" type="hidden" name="carrierId" value="<%=carrierId%>">
		<%} %>
	<%
%>
<%if(isSimpleUser){ %>
<p>Consumer is registered as a simple user with no email and mobile. Consumer can login to MORE website to upgrade to full account at 'Account Settings'.</p>
<%} %>
<table class="input-rows">
<tbody>
<tr>
<td>
First Name
</td>
<td >
<input type="text" id="firstname" name="firstname" title="Your first name" value="<%=StringUtils.prepareHTML(firstname) %>" />
</td>
<td>
Last Name
</td>
<td>
<input type="text" id="lastname" name="lastname" title="Your last name" value="<%=StringUtils.prepareHTML(lastname) %>" />
</td>
</tr>
<%if(!isSimpleUser){%>
<tr>
<td>
Preferred Communication Method
</td>
<td>
<input id="preferredCommType" type="radio" value="1" name="preferredCommType" <%if(preferredCommType==1){ %>checked="checked" <%} %> disabled='disabled'>Email
<input id="preferredCommType" type="radio" value="2" name="preferredCommType" <%if(preferredCommType==2){ %>checked="checked" <%} %> disabled='disabled'>SMS (Text)
</td>
<td>
Email
</td>
<td>
<%=StringUtils.prepareHTML(email) %>
</td>
</tr>
<% if (!isPayrollDeduct) { %>
<tr>
<td>
Mobile
</td>
<td>
<%=StringUtils.prepareHTML(mobile) %>
</td>
<td>
Mobile Phone Carrier
</td>
<td>
<%if(preferredCommType == 2) {
	while(carrierResults.next()) {
		Integer value = carrierResults.getValue("carrierId", Integer.class);
		if (value != null && value.intValue() == carrierId) {%>
		<%=StringUtils.prepareHTML(carrierResults.getFormattedValue("carrierName")) %>
		<%
		}
	}
}%>
</td>
</tr>
<tr>
<td>
Address
</td>
<td>
<input type="text" id="address1" name="address1" title="Your street address" value="<%=StringUtils.prepareHTML(address1) %>"/>
</td>
<td>
Zip Code
</td>
<td>
<input type="text" id="postal" name="postal" title="Your postal code" value="<%=StringUtils.prepareHTML(postal) %>"/>
</td>
</tr>
<% } %>
<tr>
<td>
Region
</td>
<td>
<select id="region" name="region" title="Please select the most appropriate option" >
					<option value="">--</option><%
					while(regionResults.next()) {
						long regionId = regionResults.getValue("regionId", Long.class);
						boolean selected;
						if(selectedRegionId == null)
							selected = regionResults.getValue("selected", Boolean.class);
						else
							selected = (selectedRegionId.longValue() == regionId);
					%>
						<option value="<%=regionId %>"<%if(selected){%> selected="selected"<%} %>><%=StringUtils.prepareHTML(regionResults.getValue("regionName", String.class)) %></option><%
					} %>
				</select>
</td>
<td>
Country
</td>
<td>
<select id="country" name="country" title="Your country">
					<% for(GenerateUtils.Country c : GenerateUtils.getCountries()) {%>
						<option value="<%=StringUtils.prepareCDATA(c.code)%>" onselect="updatePostalPrompts('<%=("US".equalsIgnoreCase(c.code) ? "5-digit zip code" : "postal code") %>', '<%=StringUtils.prepareScript(c.postalRegex)%>');" <%
						if(c.code.equalsIgnoreCase(country)) { %> selected="selected"<%
						}%>><%=StringUtils.prepareHTML(c.name)%></option><%
					 }%></select>
</td>
</tr>
<%} %>
<% if (isPayrollDeduct) { %>
<tr>
	<td>Department</td>
	<td><input type="text" id="department" name="department" title="Department" value="<%=StringUtils.prepareCDATA(department) %>"/></td>
	<td>Cardholder ID</td>
	<td><input type="text" id="customerAssignedId" name="customerAssignedId" title="Cardholder ID" value="<%=StringUtils.prepareCDATA(customerAssignedId) %>"/></td>
<% } %>
<tr>
<td align="center" colspan="4">
	<% if (isMoreEnabled) { %>
	<input id="loginAs" type="button" value="Sign Into MORE" onclick="window.open('<%=StringUtils.prepareCDATA(baseUrl)%>/prepaid_signin_as.html?consumerId=<%=consumerId%>')">
	<% } %>
	<%if(isSimpleUser){%>
	<input id="updateSimple" type="button" value="Update" onClick="onUpdateSimple();">
	<%}else{%>
		<input id="update" type="button" value="Update" onClick="onUpdate();">
	<%} %>
</td>

</tr>
</tbody>
</table>
</div>
</div>
</form>
<br/>
<%if(!isSimpleUser && isMoreEnabled){%>
<form name="consumerFormReset" id="consumerFormReset" action="consumer_edit.html" autocomplete="off">
<input type="hidden" name="consumerId" value="<%=consumerId%>">
<input type="hidden" name="actionType" value="resetPassword">
<div class="selectSection">
<div class="sectionTitle">Reset Password</div>
<div class="sectionRow">
<table class="input-rows">
<tbody>
<tr>
<td>Password: </td>
<td><input id="newpassword" type="password" value="" name="newpassword" title="A password with at least 8 characters and 1 uppercase letter, 1 lowercase letter, and a number or punctuation symbol"></td>
</tr>
<tr>
<td>Confirm Password: </td>
<td><input id="newconfirm" type="password" value="" name="newconfirm" title="Must match the password entered above"></td>
</tr>
<tr>
<td align="center" colspan="2"><input id="resetPassword" type="button" value="Reset Password" onClick="onResetPassword();"></td>
</tr>
</tbody>
</table>
</div>
</div>
</form>
<%} %>
<%} %>
<br/>
<% if(activeCardsCount>=2 && !isPayrollDeduct){ %>
<form name="balanceTransferForm" id="balanceTransferForm" action="consumer_acct_edit.html" autocomplete="off">
<input type="hidden" name="consumerId" value="<%=consumerId%>">
<input type="hidden" name="actionType" value="balanceTransfer">
<input type="hidden" id="consumerAcctId" name="consumerAcctId" value="">
<input type="hidden" id="toConsumerAcctId" name="toConsumerAcctId" value="">
<div class="selectSection">
<div class="sectionTitle">Balance Transfer</div>
<div class="sectionRow">
<table class="input-rows">
<tbody>
<tr>
<td>
From Card:
</td>
<td>
<select id="fromCard" name="fromCard" title="Please the balance transfer from card" >
		<%=cardOptionsStr%>
</select>
<input type="checkbox" name="closeFromCard" value="Y"> Close Card After Balance Transfer
</td>
</tr>
<tr>
<td>
To Card:
</td>
<td>
<select id="toCard" name="toCard" title="Please the balance transfer to card" >
		<%=cardOptionsStr%>
</select>
</td>
</tr>
<tr>
<td>Reason:</td><td>
<input maxLength="100" size="60" required="required" id="balanceTransferReason" type="text" title="Enter the reason for the balance transfer" name="balanceTransferReason" value="">
</td>
</tr>
<tr>
<td align="center" colspan="2">
<input id="balanceTransfer" type="button" onclick="onBalanceTransfer();" value="Transfer Balance">
</td>
</tr>
</tbody>
</table>
</div>
</div>
</form>
<%} %>
<br/>
<div align="center" style="float: center"><input id="listCards" type="button" value="List Cards" onClick="onListCards();"></div>
<br/>
<form name="consumerForm2" id="consumerForm2" action="consumer.html">
<input id="actionType2" type="hidden" name="actionType2" value="search">
<input type="hidden" name="consumerId" value="<%=consumerId%>">
<%if(actionType2!=null && actionType2.equals("search")){ %>
<div class="instruct center">Search Results</div>
<div class="payor-transactions">
<div class="spacer5"></div>
<table class="sortable-table">
	<tr class="gridHeader"><%
	GenerateUtils.writeSortedTableHeader(pageContext, "signIntoPrepaid", "Sign Into MORE", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "consumerAcctTypeDesc", "Account Type", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "firstName", "First Name", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "lastName", "Last Name", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "email", "Email", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "mobile", "Mobile", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "cardNum", "Card Number", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "cardId", "Card Id", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "cardBalance", "Card Balance", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "active", "Status", sortBy);
	%>
	</tr>
	<% while(consumerCardResults.next()) {%>
	<tr>
		<td><% if (ConvertUtils.getBoolean(consumerCardResults.getValue("moreEnabledInd"), false)) { %><input id="loginAs" type="button" value="Sign Into MORE" onclick="window.open('<%=StringUtils.prepareCDATA(baseUrl)%>/prepaid_signin_as.html?consumerId=<%=consumerId%>')"><% } %></td>
		<td><%=StringUtils.prepareHTML(consumerCardResults.getFormattedValue("consumerAcctTypeDesc"))%></td>
		<td><a href="consumer.html?consumerId=<%=consumerCardResults.getValue("consumerId", long.class)%>"><%=StringUtils.prepareHTML(consumerCardResults.getFormattedValue("firstName"))%></a></td>
		<td><a href="consumer.html?consumerId=<%=consumerCardResults.getValue("consumerId", long.class)%>"><%=StringUtils.prepareHTML(consumerCardResults.getFormattedValue("lastName"))%></a></td>
		<td><%=StringUtils.prepareHTML(consumerCardResults.getFormattedValue("email"))%></td>
		<td><%=StringUtils.prepareHTML(consumerCardResults.getFormattedValue("mobile"))%></td>
		<td><a href="consumer_acct.html?consumerAcctId=<%=consumerCardResults.getValue("consumerAcctId", long.class)%>"><%=StringUtils.prepareHTML(consumerCardResults.getFormattedValue("cardNum"))%></a></td>
		<td><%=StringUtils.prepareHTML(consumerCardResults.getFormattedValue("cardId"))%></td>
		<td><%=StringUtils.prepareHTML(consumerCardResults.getFormattedValue("cardBalance"))%></td>
		<td><%if (consumerCardResults.getFormattedValue("active").equals("Y")){%> Active <%}else{ %> Inactive<%} %></td>
	</tr><%
	}
int rows = consumerCardResults.getRowCount();
int count;
if(pageIndex == 1 && rows < PaginationUtil.DEFAULT_PAGE_SIZE) {
	count = rows;
} else {
	count = sorter.executeCount(inputForm);
}%>
<tr><td colspan="10" class="pagination-container">
<jsp:include page="include/pagination2.jsp">
	<jsp:param name="pageIndex" value="<%=pageIndex %>"/>
	<jsp:param name="pageSize" value="<%=pageSize %>"/>
	<jsp:param name="count" value="<%=count %>"/>
</jsp:include>
</td></tr>
</table>
</div>
<%} %>
</form>
<%}else{%>
	<p class="not-authorized">User '<%=StringUtils.prepareHTML(user.getUserName()) %>' does not have permission to view this page. Please contact USA Technologies if you need access.</p>
<%}%>
<script type="text/javascript">
function onResetPassword(){
	clearMessage();
	if(confirm("Are you sure you want to reset the password for the account?")){
		$("newpassword").set("data-validators","validate-regex-nospace:'<%=StringUtils.prepareScript(".{8,}")%>'");
		$("newconfirm").set("data-validators","validate-regex-nospace:'<%=StringUtils.prepareScript(".{8,}")%>'");
		$("consumerFormReset").submit();
	}
}

function onUpdateSimple(){
	$("firstname").set("data-validators","required");
	$("lastname").set("data-validators","required");
	$("firstname").value=$("firstname").value.trim();
	$("lastname").value=$("lastname").value.trim();
	$("actionType").value='updateSimple';
	$("consumerForm").submit();
}

function onUpdate(){
	clearMessage();
	$("firstname").set("data-validators","required");
	$("lastname").set("data-validators","required");
<% if (!isPayrollDeduct) { %>
	$("address1").set("data-validators","required");
	if($("region")){
		$("region").set("data-validators","required");
	}
<% } else { %>
	$("customerAssignedId").set("data-validators","required");
<% } %>
	$("country").set("data-validators","required");
	$("firstname").value=$("firstname").value.trim();
	$("lastname").value=$("lastname").value.trim();
	if ($("postal"))
		$("postal").value=$("postal").value.trim();
	$("lastname").value=$("lastname").value.trim();
	$("actionType").value='updateConsumer';
	$("consumerForm").submit();
}

function onListCards(){
	clearMessage();
	$("actionType2").value='search';
	$("consumerForm2").action="consumer.html"
	$("consumerForm2").submit();
}
window.addEvent('domready', function() {
	new Form.Validator.Inline.Mask($("consumerForm"));
	if ($("consumerFormReset"))
		new Form.Validator.Inline.Mask($("consumerFormReset"));
	var btForm = $("balanceTransferForm");
	if(btForm != null)
		new Form.Validator.Inline.Mask(btForm);
	updatePostalPrompts('<%=postalDesc%>','<%=postalPattern%>');
});

var postalEl;
function updatePostalPrompts(desc, pattern) {
	if ($("postal")) {
		if(!postalEl)
			postalEl = $("consumerForm").postal;
		postalEl.title = "Your " + desc;
		postalEl.set("data-validators","validate-regex:'"+pattern+"'");
	}
}

function onBalanceTransfer(){
	clearMessage();
	var fromSelected=$("fromCard").getSelected();
	var toSelected=$("toCard").getSelected();
	if($("fromCard").value==$("toCard").value){
		$("message").set("class","status-info-failure");
		$("message").set("html", "From Card and To Card is the same. Please choose a different card to transfer balance.");
	}else{
		$("consumerAcctId").value=$("fromCard").value;
		$("toConsumerAcctId").value=$("toCard").value;
		$("balanceTransferForm").submit();
	}
}

function clearMessage(){
	$("message").set("class","");
	$("message").set("html", "");
}
</script>
<jsp:include page="include/footer.jsp"/>
