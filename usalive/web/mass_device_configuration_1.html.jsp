<%@page import="simple.text.StringUtils"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="com.usatech.usalive.servlet.USALiveUtils"%>
<%@page import="simple.servlet.ResponseUtils"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<jsp:include page="include/header.jsp"/>
<div class="tableDataContainer">
<form method="post" action="mass_device_configuration_2.html" onsubmit="return validateMassConfigUpdate();">
<%
InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
ResponseUtils.writeXsfrFormProtection(pageContext);
USALiveUtils.writeFormProfileId(pageContext);
%>
<script defer="defer" type="text/javascript">
function validateMassConfigUpdate() {
	if (document.getElementById("serial_number_list").value.trim() == "") {
		alert("Please enter device serial number list");
		return false;
	}
	return true;
}
</script>
<div class="toggle-heading">
	<span class="caption">Mass Device Configuration</span>
</div>

<%
String successMessage = inputForm.getString("successMessage", false);
if (!StringHelper.isBlank(successMessage)) {%>
<div class="spacer10"></div>
<div class="message-success"><%=StringUtils.prepareHTML(successMessage).replace("\n", "<br/>")%><br/></div>
<%}%>

<div class="spacer10"></div>

<table>
    <tr>
		<td valign="top">Device Serial Number List<br/>(1 per line)</td>
		<td>&nbsp;</td>
		<td>
			<textarea name="serial_number_list" id="serial_number_list" rows="10" style="width: 240px;" usatRequired="true"><%=inputForm.getStringSafely("serial_number_list", "").replaceAll(",", "\n")%></textarea>
		</td>
	</tr>
	<tr>
		<td><div class="spacer5"></div></td>
	</tr>
	<tr>
		<td colspan="3" align="center"><input type="submit" name="action" value="Next &gt;" /></td>
	</tr>
</table>

<div class="spacer5"></div>

</form>
</div>
<jsp:include page="include/footer.jsp"/>