<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.ResponseUtils"%>
<%@page import="java.util.Date"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="com.usatech.report.ReportingUser"%>
<% 
	UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
	InputForm inputForm = RequestUtils.getInputForm(request);
	int customerId=inputForm.getInt("customerId", false, -1);
	if(customerId<0){
		customerId=user.getCustomerId();
		inputForm.set("customerId",customerId);
	}
	int i=0;
	String canLoginAs;
%>
<jsp:include page="include/header.jsp"/>
<%
try{
	user.checkPermission(ReportingUser.ResourceType.CUSTOMER, ReportingUser.ActionType.VIEW, customerId);
	Results userReportResults = DataLayerMgr.executeQuery("GET_CUSTOMER_ALL_USER_REPORTS", inputForm);
	String dateFormat= "DATE:MM/dd/yyyy HH:mm:ss";
	userReportResults.setFormat("createdTs",dateFormat);
	userReportResults.setFormat("lastSentTs", dateFormat);
	int count=0;
	Date createdTs, lastSentTs;
	%>
	<script type="text/javascript">
		function onSubmit(userId, userReportId){
			$("editUserId").value=userId;
			$("userReportId").value=userReportId;
		}
	</script>
	<div class="caption">User Reports</div>
	<form name="beUser" id="beUserForm" action="be_user_user_report.i">
	<input type="hidden" id="editUserId" name="editUserId" value="" />
	<input type="hidden" id="userReportId" name="userReportId" value="" />
	<input type="hidden" name="session-token" value="<%=RequestUtils.getSessionToken(request)%>" />
	<table class="folio">
		<tr class="headerRow">
			<th></th>
			<th><a title="Sort Rows by User Name" data-toggle="sort" data-sort-type="STRING">User Name</a></th>
			<th><a title="Sort Rows by Report Name" data-toggle="sort" data-sort-type="STRING">Report Name</a></th>
			<th><a title="Sort Rows by User Report Status" data-toggle="sort" data-sort-type="STRING">User Report Status</a></th>
			<th><a title="Sort Rows by Batch Type" data-toggle="sort" data-sort-type="STRING">Batch Type</a></th>
			<th><a title="Sort Rows by Frequency" data-toggle="sort" data-sort-type="STRING">Frequency</a></th>
			<th><a title="Sort Rows by Created Time" data-toggle="sort" data-sort-type="NUMBER">Created Time</a></th>
			<th><a title="Sort Rows by Last Sent Time" data-toggle="sort" data-sort-type="NUMBER">Last Sent Time</a></th>
			<th><a title="Sort Rows by Transport Type" data-toggle="sort" data-sort-type="STRING">Transport Type</a></th>
			<th><a title="Sort Rows by Transport Name" data-toggle="sort" data-sort-type="STRING">Transport Name</a></th>
			<th><a title="Sort Rows by Transport Properties" data-toggle="sort" data-sort-type="STRING">Transport Properties</a></th>
			<th><a title="Sort Rows by Transport Status" data-toggle="sort" data-sort-type="STRING">Transport Status</a></th>
		</tr>
		<tbody>
		<%while(userReportResults.next()){ 
			count++;
			createdTs=userReportResults.getValue("createdTs", Date.class);
			lastSentTs=userReportResults.getValue("lastSentTs", Date.class);
			long createdTsSortValue=createdTs==null?0:createdTs.getTime();
			long lastSentTsSortValue=lastSentTs==null?0:lastSentTs.getTime();
			canLoginAs=userReportResults.getFormattedValue("canAdminUser");
			%>
			<tr class="<%=(++i % 2) == 0 ? "even" : "odd" %>RowHover">
			<%if(canLoginAs.equals("Y")){ %>
				<td><input type="submit" value="Login As" onclick="onSubmit(<%=StringUtils.prepareCDATA(userReportResults.getFormattedValue("userId"))%>,<%=StringUtils.prepareCDATA(userReportResults.getFormattedValue("userReportId"))%>);"/></td>
			<%}else if(canLoginAs.equals("S")){ %>
			<td><a href="user_report.i?userReportId=<%=StringUtils.prepareCDATA(userReportResults.getFormattedValue("userReportId"))%>">Go to Report Register</a></td>
			<%}else{ %>
			<td></td>
			<%} %>
			<td data-sort-value="<%=StringUtils.prepareScript(userReportResults.getFormattedValue("userName")) %>"><%=StringUtils.prepareHTML(userReportResults.getFormattedValue("userName")) %></td>
			<td data-sort-value="<%=StringUtils.prepareScript(userReportResults.getFormattedValue("reportName")) %>"><%=StringUtils.prepareHTML(userReportResults.getFormattedValue("reportName")) %></td>
			<td data-sort-value="<%=StringUtils.prepareScript(userReportResults.getFormattedValue("userReportStatus")) %>"><%=StringUtils.prepareHTML(userReportResults.getFormattedValue("userReportStatus")) %></td>
			<td data-sort-value="<%=StringUtils.prepareScript(userReportResults.getFormattedValue("batchType")) %>"><%=StringUtils.prepareHTML(userReportResults.getFormattedValue("batchType")) %></td>
			<td data-sort-value="<%=StringUtils.prepareScript(userReportResults.getFormattedValue("frequency")) %>"><%=StringUtils.prepareHTML(userReportResults.getFormattedValue("frequency")) %></td>
			<td data-sort-value="<%=StringUtils.prepareScript(String.valueOf(createdTsSortValue)) %>"><%=StringUtils.prepareHTML(userReportResults.getFormattedValue("createdTs")) %></td>
			<td data-sort-value="<%=StringUtils.prepareScript(String.valueOf(lastSentTsSortValue)) %>"><%=StringUtils.prepareHTML(userReportResults.getFormattedValue("lastSentTs")) %></td>
			<td data-sort-value="<%=StringUtils.prepareScript(userReportResults.getFormattedValue("transportTypeName")) %>"><%=StringUtils.prepareHTML(userReportResults.getFormattedValue("transportTypeName")) %></td>
			<td data-sort-value="<%=StringUtils.prepareScript(userReportResults.getFormattedValue("transportName")) %>"><%=StringUtils.prepareHTML(userReportResults.getFormattedValue("transportName")) %></td>
			<td data-sort-value="<%=StringUtils.prepareScript(userReportResults.getFormattedValue("transportProp")) %>"><%=StringUtils.prepareHTML(userReportResults.getFormattedValue("transportProp")) %></td>
			<td data-sort-value="<%=StringUtils.prepareScript(userReportResults.getFormattedValue("transportStatus")) %>"><%=StringUtils.prepareHTML(userReportResults.getFormattedValue("transportStatus")) %></td>
			</tr>
		<%
		} //end of while
		%>
		<%if(count==0){ %>
		<tr><td>No user reports found for this customer.</td></tr>
		<%} %>
		</tbody>
	</table>
	</form>
	<% 
}catch(Exception e){

	%>
	<p class="not-authorized">User '<%=StringUtils.prepareHTML(user.getUserName()) %>' does not have permission to view this customer. Please contact USA Technologies if you need access.</p>
	<% 
}
%>
<jsp:include page="include/footer.jsp"/>
