<%@page import="java.math.BigDecimal"%>
<%@page import="com.usatech.usalive.servlet.USALiveUtils"%>
<%@page import="simple.servlet.ResponseUtils"%>
<%@page import="simple.servlet.NotAuthorizedException"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.text.StringUtils"%>
<%
UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
if(!user.hasPrivilege(ReportingPrivilege.PRIV_BACK_OFFICE))
    throw new NotAuthorizedException("You are not authorized to perform this function.");
InputForm form = RequestUtils.getInputForm(request);
form.setAttribute("allowedStatusCds", "A");
Results payorResults = DataLayerMgr.executeQuery("GET_PAYORS", form);
%>
<jsp:include page="include/header.jsp"/>
<div class="caption">ePort Online: Saved Accounts</div>
<div class="spacer10"></div>
<form action="">
<div><span class="label">Name:</span><input type="text" name="nameFilter" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "nameFilter", String.class, false))%>"/><button type="submit">Search</button></div>
</form>
<div class="spacer10"></div>
<div id="payorsPane">
<table class="sortable-table">
<tr class="gridHeader">
<th class="sortASC"><a data-toggle="sort" data-sort-type="STRING">Name</a></th>
<th><a data-toggle="sort" data-sort-type="STRING">Card</a></th>
<th><a data-toggle="sort" data-sort-type="STRING">Pay To</a></th>
<th><a data-toggle="sort" data-sort-type="NUMBER">Last Charged</a></th>
<th><a data-toggle="sort" data-sort-type="NUMBER">Next Scheduled</a></th>
<th><button onclick="window.location = 'payor_add.html'" title="Add new payor">Add</button></th>
</tr>
<tbody><%
Boolean userAccess = null;
while(payorResults.next()) {
	boolean currentUserAccess = payorResults.getValue("userAccess", Boolean.class);
	if(currentUserAccess)
		userAccess = true;
	else {
		if(userAccess == null)
			  userAccess = false;
		continue;
	}
	long payorId = payorResults.getValue("payorId", Long.class);
	String payTo = payorResults.getFormattedValue("bankName") + " - #" + payorResults.getFormattedValue("accountNumber") + " - " + payorResults.getFormattedValue("accountTitle") + " (" + payorResults.getFormattedValue("currencyCd") + ")";
	Long lastChargeTime = payorResults.getValue("lastChargeTs", Long.class);
	if (lastChargeTime == null)
		lastChargeTime = 0L;
	String recurLastTs = payorResults.getFormattedValue("recurLastTs");
    String lastChargeTs = payorResults.getFormattedValue("lastChargeTs");
    BigDecimal recurAmount = payorResults.getValue("recurAmount", BigDecimal.class);
    Long recurEndTime = payorResults.getValue("recurEndTs", Long.class);
    Long recurNextTime = payorResults.getValue("recurNextTs", Long.class);
    %><tr>
	<td data-sort-value="<%=StringUtils.prepareCDATA(payorResults.getFormattedValue("payorName")) %>"><%if (userAccess) {%><a href="payor_update.html?payorId=<%=payorId%>&action=edit" title="Click to change the name or pay to of this payor"><%}%><%=StringUtils.prepareHTML(payorResults.getFormattedValue("payorName")) %><%if (userAccess) {%></a><%} %></td>
    <td data-sort-value="<%=StringUtils.prepareCDATA(payorResults.getFormattedValue("card")) %>"><%=StringUtils.prepareHTML(payorResults.getFormattedValue("card")) %></td>
    <td data-sort-value="<%=StringUtils.prepareCDATA(payTo) %>"><%=StringUtils.prepareHTML(payTo) %></td>
    <td data-sort-value="<%=lastChargeTime %>"><% 
if(lastChargeTime > 0) {
    if (userAccess) {
        %><a href="back_office_transactions.html?payorId=<%=payorId%>&amp;startDate={<%=lastChargeTime%>}&amp;endDate={<%=lastChargeTime%>}"><%
    }%><%=StringUtils.prepareHTML(payorResults.getFormattedValue("lastChargeAmount")) %> <%
    if("Y".equals(payorResults.getFormattedValue("lastChargeRecurFlag")) && !StringUtils.isBlank(recurLastTs) && !recurLastTs.equals(lastChargeTs)) { 
        %>for <%=StringUtils.prepareHTML(recurLastTs) %> (on <%=StringUtils.prepareHTML(lastChargeTs) %>)<%
    } else {
        %>on <%=StringUtils.prepareHTML(lastChargeTs) %><%
    }
    if (userAccess) {%></a><%}
}
%></td>
    <td data-sort-value="<%=recurNextTime == null ? 0 : recurNextTime%>"><% 
    if(recurAmount != null && recurAmount.signum() > 0 && recurNextTime != null && recurNextTime > 0 && (recurEndTime == null || recurEndTime > recurNextTime)) {
        %><%=StringUtils.prepareHTML(payorResults.getFormattedValue("recurAmount")) %> on <%=StringUtils.prepareHTML(payorResults.getFormattedValue("recurNextTs")) %><%
    }%></td>
    <td>
    <form action="payor_update.html">
    <input type="hidden" name="payorId" value="<%=payorId%>"/>
    <button type="submit" name="action" value="charge" title="Charge this account now">Charge</button>
    <button type="submit" name="action" value="remove" title="Remove this account">Remove</button>
    </form>
    </td>
<%
} 
if(userAccess == null) {
	%><tr><td colspan="6">No records found</td></tr>
<%} else if(!userAccess) {
    %><tr><td colspan="6">No access, please ask your administrator to grant you access to the appropriate bank account</td></tr>
<%}%>
	</tbody>
</table>
</div>

<jsp:include page="include/footer.jsp"/>
