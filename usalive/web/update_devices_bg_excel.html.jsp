<%@page import="java.io.BufferedInputStream"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.util.sheet.SheetUtils.RowValuesIterator"%>
<%@page import="simple.util.sheet.SheetUtils"%>
<%@page import="com.usatech.layers.common.process.ProcessType"%>
<%@page import="javax.mail.MessagingException"%>
<%@page import="com.usatech.layers.common.util.WebHelper"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.servlet.InputFile"%>
<%@page import="java.io.InputStream"%>
<%@page import="simple.xml.sax.MessagesInputSource"%>
<%@page import="com.usatech.usalive.servlet.USALiveUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.usatech.report.MassDeviceUpdate"%>
<%@page import="simple.servlet.NotAuthorizedException"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="simple.servlet.InputForm"%>
<%
String[] contentTypes = RequestUtils.getPreferredContentType(request, "text/html", "application/xhtml+xml", "text/plain");
boolean textResult = (contentTypes != null && contentTypes.length > 0 && contentTypes[0].equalsIgnoreCase("text/plain"));
InputForm form = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
UsaliveUser user = (UsaliveUser) RequestUtils.getUser(request);
if(!user.hasPrivilege(ReportingPrivilege.PRIV_ACTIVATE_TERMINAL))
    throw new NotAuthorizedException("You are not authorized to perform this function.");
Map<String,Object> parameters = new LinkedHashMap<>();
Object updateFile = form.getAttribute("updateFile");
if(updateFile == null) {
   RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "usalive-missing-parameter", "The {0} was not specified in the request", "file to upload");
   if(textResult) 
	   RequestUtils.writeTextResultFromMessages(request, response);
   else
	   RequestUtils.redirectWithCarryOver(request, response, "update_devices_instructs.i", false, true);
   return;
}
String notifyEmail = form.getString("notifyEmail", false);
if(!StringUtils.isBlank(notifyEmail))
    try {
        WebHelper.checkEmail(notifyEmail);
    } catch(MessagingException e) {
        RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "usalive-invalid-email", "You entered an invalid email: {0}. Please enter a valid one", e.getMessage());
        if(textResult) 
            RequestUtils.writeTextResultFromMessages(request, response);
        else
            RequestUtils.redirectWithCarryOver(request, response, "update_devices_instructs.i", false, true);
        return;
    }
InputStream content;
String path;
long processRequestId;
if(updateFile instanceof InputFile) {
    InputFile inputFile = (InputFile)updateFile;
    path = inputFile.getPath();
    content = inputFile.getInputStream();
} else {
    path = "updateFile";
	content =  ConvertUtils.convert(InputStream.class, updateFile);
}
try {
	DataLayerMgr.selectInto("GET_MASS_DEVICE_UPDATE_MAX_ALLOWED", form);  
	int maxMassDeviceUpdateRows=form.getInt("maxMassDeviceUpdateRows", false,5000);
	RowValuesIterator rvIter = SheetUtils.getExcelRowValuesIterator(content);
	String[] columnNames = rvIter.getColumnNames();
	int i=0;
	int count=0;
	while (rvIter.hasNext()) {
		count++;
		Map<String, Object> columnValues = rvIter.next(true);
		if(columnValues.isEmpty()) {
			continue;
		}else{
			for (Object value : columnValues.values()) {
				if(value!=null){
					i++;
					break;
				}
			}
		}
		if(i>maxMassDeviceUpdateRows){
			 RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "device.update.error.exceed.max", "Records in the file is more than {0}. Please split the file and try again.", maxMassDeviceUpdateRows);
			 if(textResult) 
         		RequestUtils.writeTextResultFromMessages(request, response);
     		else
         		RequestUtils.redirectWithCarryOver(request, response, "update_devices_instructs.i", false, true);
			 return;
		}
	}
	if(updateFile instanceof InputFile) {
    	InputFile inputFile = (InputFile)updateFile;
    	content = inputFile.getInputStream();
	} else {
		content =  ConvertUtils.convert(InputStream.class, updateFile);
	}
	parameters.put("updateFile", path);
	Long customerBankId = ConvertUtils.convert(Long.class, form.get("custBankId"));
	if(customerBankId != null) {
	    try {
	    	MassDeviceUpdate.checkBankAcct(customerBankId, user.getUserId());
	    } catch(ServletException e) {
	        RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "device.update.error.invalid.bankacct", "Bank Account could not be verified: {0}. Please try again", e.getMessage());
	        if(textResult) 
	            RequestUtils.writeTextResultFromMessages(request, response);
	        else
	            RequestUtils.redirectWithCarryOver(request, response, "update_devices_instructs.i", false, true);
	        return;
	    }
	    parameters.put("custBankId", customerBankId);
	}
	Long dealerId = ConvertUtils.convert(Long.class, form.get("dealerId"));
	if(dealerId != null)
		parameters.put("dealerId", dealerId);
	boolean override = ConvertUtils.getBoolean(form.get("override"), false);
	parameters.put("override", override);
	String desc = RequestUtils.getTranslator(request).translate("device.update.process.description", "Mass Device Update ''{0}''", path);
	try {
		processRequestId = USALiveUtils.registerProcessRequest(request, ProcessType.MASS_DEVICE_UPDATE_EXCEL, desc, notifyEmail, parameters, content);
	} catch(ServletException e) {
        RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "device.update.error.enqueue", "Sorry for the inconvenience. We could not process your request at this time. Please try again.", e.getMessage());
        if(textResult) 
            RequestUtils.writeTextResultFromMessages(request, response);
        else
            RequestUtils.redirectWithCarryOver(request, response, "update_devices_instructs.i", false, true);
        return;
    }
}catch(Exception|Error e){
  String errorMsg=e.getMessage()==null?"":e.getMessage();
  RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "device.update.error.other", "Sorry for the inconvenience. Please try again. "+errorMsg, e.getMessage());
  if(textResult){
      RequestUtils.writeTextResultFromMessages(request, response);
  }
  else{
      RequestUtils.redirectWithCarryOver(request, response, "update_devices_instructs.i", false, true);
  }
  return;
	
} finally {
	content.close();
}
RequestUtils.setAttribute(request, "processRequestId", processRequestId, RequestUtils.REQUEST_SCOPE);
MessagesInputSource mis = RequestUtils.getOrCreateMessagesSource(request);
mis.addMessage("success", "device.update.enqueued", "Successfully registered your request to process ''{0}''. {2,NULL,An email will be sent to ''{2}'' when processing is complete. }{3,MATCH,false=You may view the request status below}", path, processRequestId, notifyEmail, textResult);
if(textResult) 
    RequestUtils.writeTextResultFromMessages(request, response);
else
    RequestUtils.redirectWithCarryOver(request, response, "process_request_history.html?processRequestId=" + processRequestId);
%>