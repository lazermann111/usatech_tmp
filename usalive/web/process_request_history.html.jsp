<%@page import="com.usatech.layers.common.process.ProcessType"%>
<%@page import="simple.servlet.GenerateUtils"%>
<%@page import="com.usatech.usalive.servlet.USALiveUtils"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.db.Call.Sorter"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%
UsaliveUser user = (UsaliveUser) RequestUtils.getUser(request);
Long highlightProcessRequestId = RequestUtils.getAttribute(request, "processRequestId", Long.class, false);
Sorter sorter = DataLayerMgr.getGlobalDataLayer().findCall("GET_PROCESS_HISTORY").getSorter();
InputForm form = RequestUtils.getInputForm(request);
String[] sortBy = ConvertUtils.toUniqueArray(form.getStringArray("sortBy", false), String.class, "-requestedTs", "processRequestId");
form.setAttribute("sortBy", sortBy);
int pageIndex = form.getInt("pageIndex", false, 1);
int pageSize = form.getInt("pageSize", false, 20);
StringBuilder pendingIds = null;
int count;
try (Results results = sorter.executeSorted(form, pageSize, pageIndex, sortBy)) {
%>
<jsp:include page="include/header.jsp"/>
<div class="title2">Process Request History for <%=StringUtils.prepareHTML(user.getDisplayName()) %> (<%=StringUtils.prepareHTML(user.getUserName())%>)</div>
<form name="processRequestHistoryForm" action="process_request_history.html" method="get">
<%=USALiveUtils.getHiddenInputs(request)%>
<table class="sortable-table">
    <tr class="gridHeader"><%
    GenerateUtils.writeSortedTableHeader(pageContext, "sortableProcessType", "Type", sortBy);
    GenerateUtils.writeSortedTableHeader(pageContext, "sortableProcessDesc", "Process", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "requestedTs", "Requested", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortableRequestedBy", "Requested By", sortBy);
    GenerateUtils.writeSortedTableHeader(pageContext, "sortableProcessStatus", "Status", sortBy);
    GenerateUtils.writeSortedTableHeader(pageContext, "completedTs", "Completed", sortBy);
    GenerateUtils.writeSortedTableHeader(pageContext, "sortableNotifyEmail", "Email to Notify", sortBy);
    %><th>Result</th></tr><%
while(results.next()) {
    long processRequestId = results.getValue("processRequestId", long.class);
    ProcessType processType = results.getValue("processTypeId", ProcessType.class);%>
    <tr<%if(highlightProcessRequestId != null && highlightProcessRequestId == processRequestId) {%> class="selected"<%} %>>
        <td><a<%switch(processType) {
        	case MASS_DEVICE_UPDATE_EXCEL: %> href="update_devices_instructs.i?profileId=<%=user.getUserId()%>"<% break;
        } %>><%=StringUtils.prepareHTML(results.getFormattedValue("processType"))%></a></td>
        <td><%=StringUtils.prepareHTML(results.getFormattedValue("processDesc"))%></td>
        <td><%=StringUtils.prepareHTML(results.getFormattedValue("requestedTs"))%></td>
        <td><%=StringUtils.prepareHTML(results.getFormattedValue("requestedBy"))%></td>
        <td id="status_<%=processRequestId%>"><%=StringUtils.prepareHTML(results.getFormattedValue("processStatus"))%></td>
        <td id="completed_<%=processRequestId%>"><%=StringUtils.prepareHTML(results.getFormattedValue("completedTs"))%></td>
        <td><%=StringUtils.prepareHTML(results.getFormattedValue("notifyEmail"))%></td>
        <td id="result_<%=processRequestId%>"><%
        	String processStatusCd = results.getValue("processStatusCd", String.class);
            switch(processStatusCd) {
            	case "I":
            		if(pendingIds == null)
                        pendingIds = new StringBuilder();
                    else
                        pendingIds.append(',');
                    pendingIds.append(processRequestId);
            	case "D": case "E": 
            	    %><div class="toggle-expand" data-toggle="expand" data-target="response_<%=processRequestId%>"><%=StringUtils.prepareHTML(results.getFormattedValue("summary")) %></div><ul class="hide response-list" id="response_<%=processRequestId%>"><%=results.getFormattedValue("response")%></ul><%
            		break;
                case "P": case "R":
                     %><div class="center"><button type="button" onclick="location.href='process_request_cancel.html?processRequestId=<%=processRequestId%>'" title="Cancel this request">Cancel</button></div><%
                    if(pendingIds == null)
                        pendingIds = new StringBuilder();
                    else
                        pendingIds.append(',');
                    pendingIds.append(processRequestId);
                    break;   
            }%></td>
    </tr><%
}
int rows = results.getRowCount();
if(pageIndex == 1 && rows < 20) { 
    count = rows;
} else { 
    count = sorter.executeCount(form);
}
}%>
<tr><td colspan="10" class="pagination-container">
<jsp:include page="include/pagination2.jsp">
    <jsp:param name="pageIndex" value="<%=pageIndex %>"/>
    <jsp:param name="pageSize" value="<%=pageSize %>"/>
    <jsp:param name="count" value="<%=count %>"/>
</jsp:include>
</td></tr>
</table><%
if(pendingIds != null) {
	%><script type="text/javascript">
	window.setTimeout(function() {
		new Request.HTML({url: "process_request_history_refresh.html?processRequestIds=<%=pendingIds%>", 
				data: {fragment: true}, 
				evalScripts: true, noCache: true}).get();
	}, 5000);
	</script><%
}
%>
</form>
<jsp:include page="include/footer.jsp"/>