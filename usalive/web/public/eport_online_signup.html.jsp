<%@page import="simple.db.ParameterException"%>
<%@page import="simple.io.Log"%>
<%@page import="java.sql.SQLException"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>ePort Online</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="stylesheet" href="/css/normalize.css">
        <link rel="stylesheet" href="/css/main.css">
        <script src="/scripts/modernizr-2.6.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
	<div class="links">
          <span><a href="http://www.usatech.com">USAT HOME</a>
          </span>
          <span><a href="http://www.usatech.com/eport/eport_connect.php">EPORT CONNECT</a>
          </span>
          <span><a  href="http://www.usatech.com/eport/index.php">EPORT</a>
          </span>
          <span><a  href="http://www.eportmobile.com">EPORT MOBILE</a>
          </span>
          <span><a  href="http://www.eportgo.com">EPORT GO</a>
          </span>
          <span><a  href="http://www.usatech.com/eport/quick_connect.php">QUICKCONNECT</a>
          </span>
          <div class="clearfix"></div>
    </div>  <!--  /links  -->
	<%
	String action = RequestUtils.getAttribute(request, "submit", String.class, false);
	if(!StringUtils.isBlank(action)) { 
		boolean okay = false;
		try {
			DataLayerMgr.executeCall("ADD_OFFER_INTEREST", RequestUtils.getInputForm(request), true);
			okay = true;
		} catch(ParameterException e) {
			Log.getLog().error("Invalid parameters for eport online interest for '" + RequestUtils.getAttribute(request, "email", String.class, false) + "'", e);
			%><div class="alert alert-error">Please provide valid values for all fields</div>
			<%
		} catch(Exception e) {
			Log.getLog().error("Could not add eport online interest for '" + RequestUtils.getAttribute(request, "email", String.class, false) + "'", e);
			%>
			<div class="alert alert-error">We are so sorry! We were unable to process your request at this time. Please try again.</div><%
		}
		String errorMessage = RequestUtils.getAttribute(request, "errorMessage", String.class, false);
		if(!StringUtils.isBlank(errorMessage)) {
			%><div class="alert alert-error"><%=StringUtils.prepareHTML(errorMessage) %></div>
			<%
		} else if(okay) {%>
		<div class="wrapper">
          <img src="/images/eport-online-thankyou.png" alt="Thank you for opting in" />
          <!-- confirmation message -->
          <p>Congratulations! You have taken the first step to faster payments, streamlined invoice and enhanced account management. Watch for more information on how to use and make the most of your new ePort Online service in the coming weeks!</p>
          <p>&nbsp;</p>
          <img src="/images/iphone6.jpg" alt="iphone 6" />
          <p>Did you know... USA Technologies is giving a FREE iPhone 6 to all customers that commit to 100 new connections before 9/30/14. Contact your sales rep for details: 800-633-0340. Terms and conditions will apply.</p>
          <p>&nbsp;</p>
        </div>  <!-- /end wrapper --><%
        	return;
		}
	}	%> 
        <div class="wrapper">
          <img class="mainimg" src="/images/eport-online.png" alt="ePort Online">
          <div id="main">
            <p>There are two easy ways to opt in to the FREE ePort Online Service.  Either complete the form below <b>OR</b> simply sign into <a href="/eport_online_eport_mobile_offer.html">USALive</a> and follow the instructions for instant access to ePort Online.</p>
      <h2>ePort Online Form</h2>
              <!-- form -->
      <form id="contact-form" action="" method="post">
      <input type="hidden" name="offerId" value="5" />
      <div>
        <label>
          <span>COMPANY NAME: (required)</span>
          <input placeholder="Please enter your company name" type="text" maxlength="200" required="required" name="company" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "company", String.class, false))%>"/>
        </label>
      </div>
      <div>
        <label>
          <span>STREET ADDRESS: (required)</span>
          <input placeholder="Please enter your street address" type="text" maxlength="255" required="required" name="address" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "address", String.class, false))%>"/>
        </label>
      </div>
      <div>
        <label>
          <span>CITY: (required)</span>
          <input placeholder="Please enter your city" type="text" maxlength="50" required="required" name="city" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "city", String.class, false))%>"/>
        </label>
      </div>
      <div>
        <label>
          <span>STATE: (required)</span>
          <input placeholder="Please enter your state" type="text" maxlength="50" required="required" name="state" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "state", String.class, false))%>"/>
        </label>
      </div>
      <div>
        <label>
          <span>ZIP: (required)</span>
          <input placeholder="Please enter your zip or postal code" type="text" maxlength="20" required="required" name="postal" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "postal", String.class, false))%>"/>
        </label>
      </div>
      <div>
			<label>
				<span>FIRST NAME: (required)</span>
				<input placeholder="Please enter your first name" type="text" maxlength="50" required="required" name="first" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "first", String.class, false))%>"/>
			</label>
		</div>
		<div>
			<label>
				<span>LAST NAME: (required)</span>
				<input placeholder="Please enter your last name" type="text" maxlength="50" required="required" name="last" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "last", String.class, false))%>"/>
			</label>
		</div>
		<div>
			<label>
				<span>EMAIL: (required)</span>
				<input placeholder="Please enter your email address" type="text" maxlength="100" required="required" name="email" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "email", String.class, false))%>"/>
			</label>
		</div>
      <div>
        <p>By submitting this form, I acknowledge that I have read and agree to the <a href="/USAT_ePort-Online_Addendum.pdf">Terms and Conditions</a> (PDF).</p>
      </div>
			<div>
				<button name="submit" type="submit" id="contact-submit" value="optIn">SUBMIT</button>
			</div>
		</form>
		<!-- /form -->
  </div>  <!-- /main  -->
    <div class="slogan">
          <p>
          One company, one point of contact, one call.</p>
          </div>  <!-- /slogan -->
          <div class="footer">
          <a href="http://www.usatech.com"><img src="/images/logo_usatech.jpg" alt="USA Technologies" /></a>
          <p>
          800.633.0340  |  <a href="http://www.usatech.com">www.usatech.com</a>
          </div>  <!-- /footer -->
        </div> <!-- /wrapper -->
    </body>
</html>
