<%@page import="simple.db.ParameterException"%>
<%@page import="simple.io.Log"%>
<%@page import="java.sql.SQLException"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Fall Special</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="stylesheet" href="/css/normalize.css">
        <link rel="stylesheet" href="/css/main.css">
        <script src="/scripts/modernizr-2.6.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
	<div class="links">
          <span><a href="http://www.usatech.com">USAT HOME</a>
          </span>
          <span><a href="http://www.usatech.com/eport/eport_connect.php">EPORT CONNECT</a>
          </span>
          <span><a  href="http://www.usatech.com/eport/index.php">EPORT</a>
          </span>
          <span><a  href="http://www.eportmobile.com">EPORT MOBILE</a>
          </span>
          <span><a  href="http://www.eportgo.com">EPORT GO</a>
          </span>
          <span><a  href="http://www.usatech.com/eport/quick_connect.php">QUICKCONNECT</a>
          </span>
          <div class="clearfix"></div>
    </div>  <!--  /links  -->
	<%
	String action = RequestUtils.getAttribute(request, "submit", String.class, false);
	if(!StringUtils.isBlank(action)) { 
		boolean okay = false;
		try {
			DataLayerMgr.executeCall("ADD_OFFER_INTEREST", RequestUtils.getInputForm(request), true);
			okay = true;
		} catch(ParameterException e) {
			Log.getLog().error("Invalid parameters for fall special interest for '" + RequestUtils.getAttribute(request, "email", String.class, false) + "'", e);
			%><div class="alert alert-error">Please provide valid values for all fields</div>
			<%
		} catch(Exception e) {
			Log.getLog().error("Could not add fall special interest for '" + RequestUtils.getAttribute(request, "email", String.class, false) + "'", e);
			%>
			<div class="alert alert-error">We are so sorry! We were unable to process your request at this time. Please try again.</div><%
		}
		String errorMessage = RequestUtils.getAttribute(request, "errorMessage", String.class, false);
		if(!StringUtils.isBlank(errorMessage)) {
			%><div class="alert alert-error"><%=StringUtils.prepareHTML(errorMessage) %></div>
			<%
		} else if(okay) {%>
		<div class="wrapper">
          <img src="/images/thankyou.jpg" alt="Thank you for your order" />
          <!-- confirmation message -->
          <p>Thank you for participating in our Fall Special. A sales representative will contact you within three business days to confirm and complete your order.</p>
          <p>&nbsp;
          </p>
        </div>  <!-- /end wrapper --><%
        	return;
		}
	}	%> 
        <div class="wrapper">
          <img class="mainimg" src="/images/areyouready-fallspecial.jpg" alt="Fall Special">
          <div id="main">

	          <p>To take advantage of our Fall Special call us at 800-633-0340 during business hours or complete the online form below in full and submit your order request before midnight on Tuesday, September 30, 2014. Don't delay!</p>
	          
	          <p><b>Online orders:</b> By completing the form below, you are agreeing to take shipment of the ePort Quantity indicated. If you are a new customer, or we require additional information, a sales representative will contact you to complete your order.</p>

			  <p>*Discount off ePort retail price of $259</p>

      <h2>ePort Ordering Form</h2>
      <!-- form -->
      <form id="contact-form" action="" method="post">
      <input type="hidden" name="offerId" value="2" />      
		<div>
			<label>
				<span>FIRST NAME: (required)</span>
				<input placeholder="Please enter your first name" type="text" maxlength="50" required="required" name="first" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "first", String.class, false))%>"/>
			</label>
		</div>
		<div>
			<label>
				<span>LAST NAME: (required)</span>
				<input placeholder="Please enter your last name" type="text" maxlength="50" required="required" name="last" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "last", String.class, false))%>"/>
			</label>
		</div>
		<div>
			<label>
				<span>EMAIL: (required)</span>
				<input placeholder="Please enter your email address" type="text" maxlength="100" required="required" name="email" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "email", String.class, false))%>"/>
			</label>
		</div>
		<div>
			<label>
				<span>PHONE NUMBER: (required)</span>
				<input placeholder="Please enter your phone number" type="text" maxlength="20" required="required" name="phone" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "phone", String.class, false))%>"/>
			</label>
		</div>
		<div>
	       <label>
	         <span>COMPANY NAME: (required)</span>
	         <input placeholder="Please enter your company name" type="text" maxlength="200" required="required" name="company" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "company", String.class, false))%>"/>
		  </label>
		</div>
		 <div>
        <label>
          <span>STREET ADDRESS TO SHIP TO: (required)</span>
          <input placeholder="Please enter your street address" type="text" maxlength="255" required="required" name="address" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "address", String.class, false))%>"/>
        </label>
      </div>
      <div>
        <label>
          <span>CITY: (required)</span>
          <input placeholder="Please enter your city" type="text" maxlength="50" required="required" name="city" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "city", String.class, false))%>"/>
        </label>
      </div>
      <div>
        <label>
          <span>STATE: (required)</span>
          <input placeholder="Please enter your state" type="text" maxlength="50" required="required" name="state" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "state", String.class, false))%>"/>
        </label>
      </div>
      <div>
        <label>
          <span>ZIP: (required)</span>
          <input placeholder="Please enter your zip or postal code" type="text" maxlength="20" required="required" name="postal" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "postal", String.class, false))%>"/>
        </label>
      </div>
      <span>
          <input type="checkbox" onclick="if (this.checked) {this.form.billingAddress.value = this.form.address.value; this.form.billingCity.value = this.form.city.value; this.form.billingState.value = this.form.state.value; this.form.billingPostal.value = this.form.postal.value;}" />
          BILLING ADDRESS IS SAME AS SHIPPING        
      </span>
      <div>
        <label>
          <span>BILLING STREET ADDRESS: (required)</span>
          <input placeholder="Please enter your billing street address" type="text" maxlength="255" required="required" name="billingAddress" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "billingAddress", String.class, false))%>"/>
        </label>
      </div>
      <div>
        <label>
          <span>BILLING CITY: (required)</span>
          <input placeholder="Please enter your billing city" type="text" maxlength="50" required="required" name="billingCity" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "billingCity", String.class, false))%>"/>
        </label>
      </div>
      <div>
        <label>
          <span>BILLING STATE: (required)</span>
          <input placeholder="Please enter your billing state" type="text" maxlength="50" required="required" name="billingState" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "billingState", String.class, false))%>"/>
        </label>
      </div>
      <div>
        <label>
          <span>BILLING ZIP: (required)</span>
          <input placeholder="Please enter your billing zip or postal code" type="text" maxlength="20" required="required" name="billingPostal" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "billingPostal", String.class, false))%>"/>
        </label>
      </div>
		<div>
	        <fieldset>
	        <legend><span>ARE YOU A CURRENT USAT CUSTOMER? (required)</span></legend>     				 
	        <label>
	          <input type="radio" name="current" value="Y" checked="checked" />Yes
	        </label>        			
	        <label>
	          <input type="radio" name="current" value="N" />No
	        </label>
	        </fieldset>
      	</div>
		<div>
			<label>
				<span>NUMBER OF G9 EPORTS TO PURCHASE: (required)</span>
				<input placeholder="Please enter the number of ePorts" type="text" maxlength="50" required="required" name="purchase" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "purchase", String.class, false))%>"/>
			</label>
		</div>
		<span>All orders are shipped via UPS Ground and are subject to NET 30 payment terms.</span>
		<div>
			<button name="submit" type="submit" id="contact-submit" value="optIn">SUBMIT</button>
		</div>
		</form>
		<!-- /form -->
  </div>  <!-- /main  -->
    <div class="slogan">
          <p>
          One company, one point of contact, one call.</p>
          </div>  <!-- /slogan -->
          <div class="footer">
          <a href="http://www.usatech.com"><img src="/images/logo_usatech.jpg" alt="USA Technologies" /></a>
          <p>
          800.633.0340  |  <a href="http://www.usatech.com">www.usatech.com</a>
          </div>  <!-- /footer -->
        </div> <!-- /wrapper -->
    </body>
</html>
