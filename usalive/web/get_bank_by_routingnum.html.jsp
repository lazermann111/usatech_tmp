<%@page import="simple.io.Log"%>
<%@page import="simple.text.StringUtils,simple.servlet.RequestUtils,simple.results.Results"%>
<%@page import="simple.db.DataLayerMgr"%>
<script type="text/javascript">
<%
Log log = Log.getLog();
String aba = RequestUtils.getAttribute(request, "aba", String.class, true);
String bankCountry = RequestUtils.getAttribute(request, "bank_country", String.class, true);
if(!StringUtils.isBlank(aba)&&!StringUtils.isBlank(bankCountry) ){
	aba=aba.trim();
	bankCountry=bankCountry.trim();
try{
	Results result=DataLayerMgr.executeQuery("GET_BANK_BY_ROUTINGNUM", new Object[]{aba,bankCountry}, true);
	if(result.next()){
	%>
		$("bankAcctForm").bank_name.value='<%=StringUtils.prepareScript(result.getFormattedValue("bankName"))%>';
		$("bankAcctForm").bank_address1.value='<%=StringUtils.prepareScript(result.getFormattedValue("bankAddress"))%>';
		$("bankAcctForm").bank_city.value='<%=StringUtils.prepareScript(result.getFormattedValue("bankCity"))%>';
		$("bankAcctForm").bank_state.value='<%=StringUtils.prepareScript(result.getFormattedValue("bankState"))%>';
		$("bankAcctForm").bank_postal.value='<%=StringUtils.prepareScript(result.getFormattedValue("bankPostal"))%>';
		$("bankAcctForm").contact_telephone.value='<%=StringUtils.prepareScript(result.getFormattedValue("bankPhone"))%>';
		var e = $("bankAcctForm").contact_name; if(!e.value) e.value='Customer Service';
		e = $("bankAcctForm").contact_title; if(!e.value) e.value='Rep';
        $("bankAcctForm").bank_country.getElements('option[value=<%=StringUtils.prepareScript(result.getFormattedValue("countryCd"))%>]').each(function (el) {
		       el.set("selected",true);
		    });
		if($("advice-required-input_bank_postal")){
			$("advice-required-input_bank_postal").dispose();
		}
		$("input_bank_postal").set("class","addressPostal");
		$("bankRoutingMsg").set("html","Bank rounting number verified");
		$("bankRoutingMsg").set("class","status-info-success");
		$("aba").set("class","validation-passed");
		isBankRoutingNumValid=1;
	<% 
	}else{
		
		if(bankCountry.equals("US")){
			%>
			isBankRoutingNumValid=0;
			$("bankRoutingMsg").set("html","Invalid US bank routing number. Please enter a valid one.");
			$("bankRoutingMsg").set("class","status-info-failure");
			<%
		}else{
			%>
			isBankRoutingNumValid=1;
			$("bankRoutingMsg").set("html","Not able to verify the bank rounting number. Please double check before submit.");
			$("bankRoutingMsg").set("class","status-info-success");
			<%
		}
	}
}catch(Exception e){
	log.error("Failed to lookup bank info by aba="+aba, e);
}
}
%>
</script>

