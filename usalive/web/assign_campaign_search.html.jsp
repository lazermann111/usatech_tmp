<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.Date"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="com.usatech.layers.common.util.CampaignUtils"%>
<%@page import="com.usatech.usalive.web.CampaignUsaliveUtils"%>

<%
	InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
	String searchBy = inputForm.getString("searchBy", true);
	Results results=null;
	if(searchBy.equalsIgnoreCase("customer")){
		results = DataLayerMgr.executeQuery("GET_PREPAID_CUSTOMERS_BY_NAME", inputForm);
%>
		<select size="6" name="customerId" id="customerId"  class="assignCampaignSelect">
		<%
		while(results.next()) {
		    %>
		    <option onclick="clearAssignSection();" value="<%=results.getValue("customerId", int.class) %>"><%=StringUtils.prepareHTML(results.getFormattedValue("customerName")) %></option>
		    <%
		}
		%>
		</select>
<% 
	}else if(searchBy.equalsIgnoreCase("region")){
		results = DataLayerMgr.executeQuery("GET_PREPAID_REGIONS_BY_CUSTOMER", inputForm);
		%>
				<select multiple="multiple" size="6" name="regionIds" id="regionIds"  class="assignCampaignSelect">
				<%
				while(results.next()) {
				    %>
				    <option value="<%=results.getValue("regionId", int.class) %>"><%=StringUtils.prepareHTML(results.getFormattedValue("regionName")) %></option>
				    <%
				}
				%>
				</select>
<% 
	}else if(searchBy.equalsIgnoreCase("all")){
		%>
<jsp:include page="assign_campaign_searchall.jsp"/>
<%
	}else if(searchBy.equalsIgnoreCase("campaignApply")){
		String deviceSerialCd=inputForm.getString("appliedDevice", true).replaceAll("\\s","");
		inputForm.set("appliedDevice", deviceSerialCd);
		String applyingTimestamp=inputForm.getString("applyingTimestamp", true).trim();
		Date applyDate=null;
		try{
			applyDate=CampaignUtils.validateApplyCampaignTime(applyingTimestamp);
			if(!CampaignUsaliveUtils.checkAllow("CHECK_ALLOW_DEVICE", inputForm)){
				%>
				<p class='status-info-failure'>The device does not exist or you don't have permission to query campaign for the device.</p>
				<% 
			}else{
				inputForm.set("applyingTimestamp", applyDate);
				results = DataLayerMgr.executeQuery("GET_APPLYING_CAMPAIGN", inputForm);
				if(results.next()&&results.get("campaignId")!=null){
				%>
					<p class='status-info-success'>Found campaign: <%=StringUtils.prepareHTML(results.getFormattedValue("campaignName")) %> discount: <%=StringUtils.prepareHTML(results.getFormattedValue("campaignDiscountPercent")) %>%</p>
				<% 
				}else{
				%>
					<p class='status-info-success'>No campaign is found for the device at the given timestamp.</p>
				<% 
				}
			}
		}catch(IllegalArgumentException e){
			%>
			<p class='status-info-failure'><%=StringUtils.prepareHTML(e.getMessage())%></p>
			<% 
		}
} %>
