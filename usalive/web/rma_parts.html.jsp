<%@page import="com.usatech.usalive.servlet.USALiveUtils"%>
<%@page import="simple.servlet.ResponseUtils"%>
<%@page import="com.usatech.usalive.web.RMAUtils"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="simple.text.StringUtils,simple.servlet.RequestUtils,simple.results.Results"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%
	UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
	boolean hasPri=false;
	if((user.isInternal()&&user.hasPrivilege(ReportingPrivilege.PRIV_CUSTOMER_SERVICE))||user.hasPrivilege(ReportingPrivilege.PRIV_RMA)){
		hasPri=true;
	}
	Results results =  DataLayerMgr.executeQuery("GET_RMA_ALLOWED_PARTS", null);
	int imageWidth=150;
%>
<jsp:include page="include/header.jsp"/>
<%if(hasPri){ %>
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "scripts/utils.js") %>"></script>
<script type="text/javascript">
var emailInvalid=0;
function onCreateRMA(){
	if($("returnForm").getElements("input[type=checkbox][name=rmaAllowedPartsIds]:checked").length==0){
		alert("You need to select at least one part for the current RMA. Please select. Thank you.");
		return false;
	}else{
		if($("rmaShippingAddrId") && $("rmaShippingAddrId").selectedIndex==-1){
			$("shippingAddrName").value=$("shippingAddrName").value.trim();
			var isNameInvalid=0;
			$('rmaShippingAddrId').getElements("option").each(function(el) {
				if($("shippingAddrName").value == el.getText()){
					alert("Please enter unique shipping address name.");
					isNameInvalid=1;
					return;
				}
			});
			if(isNameInvalid==1){
				return false;
			}
		}
		if($('rmaDescription').value && $('rmaDescription').value.trim()!=""){
			$('rmaDescription').value=$('rmaDescription').value.substring(0,4000);
		}
		if(!document.getElements("input[class=validation-failed]")){
			$("email").disabled="";
		}
		$("email").value=cleanEmail($("email").value);
		new Request.HTML({ 
	        url: "rma_email_check.html",
	        method: 'post',
	        data: $("returnForm"), 
	        evalScripts: true,
			async: false
	    }).send();
		if(emailInvalid==1){
			$("checkEmailMsg").set("html","Invalid emails. Please enter valid ones.");
			$("checkEmailMsg").set("class","status-info-failure");
			return false;
		}else{
			$("checkEmailMsg").set("html","");
			$("checkEmailMsg").set("class","");
			return true;
		}
	}
}

function onClickPart(el){
	if(el.checked){
		$("rmaPartsQuantity_"+el.value).required="required";
	}else{
		$("rmaPartsQuantity_"+el.value).required="";
	}
}

function onViewLargeImage(el, partNumber){
	if($("largeImage_"+partNumber).firstChild){
		el.set("text", "View Larger Image")
		$("largeImage_"+partNumber).set("html","");
	}else{
		el.set("text", "Close Larger Image")
		var imgEl = new Element('img', {
		    src: partNumber+'<%=RMAUtils.partImageSuffix%>',
		    width: 500,
		    height:600
		});
		$("largeImage_"+partNumber).set("html","");
		$("largeImage_"+partNumber).adopt(imgEl);
	}
}
window.addEvent('domready', function() { 	
	new Request.HTML({ 
        url: "rma_carrier_account_select.html",
        method: 'post',
        update : $("userShippingCarrier"),
        evalScripts: true
    }).send();
	
	new Request.HTML({ 
        url: "rma_shipping_address_select.html",
        method: 'post',
        update : $("rmaShippingAddr"),
        evalScripts: true
    }).send();
	$("replacementChoiceSection").set("style","display:block");
	$("rmaReplacementQuantitySection").set("style","display:none");
	onNeedReplacement();
	$("postalCd").set("class", "");
	$("advice-required-postalCd").dispose();
	new Form.Validator.Inline.Mask(document.returnForm);
});


</script>
<span class="caption">Create Parts RMA</span>
<div class="spacer5"></div>
<div class="instruct">Select the Parts and fill in the quantity for return</div>
<br/>
<form name="returnForm" method="POST" id="returnForm" action="rma_create_parts.html" onSubmit="return onCreateRMA();">
<input type="hidden" id="rmaTypeId" name="rmaTypeId" value="3"/>
<input type="hidden" id="actionType" name="actionType" value=""/>
<input type="hidden" id="transportTypeId" name="transportTypeId" value="1"/>
<input type="hidden" id="transportAction" name="transportAction" value="test"/>
<input type="hidden" id="tpv_1" name="tpv_1" value=""/>
<%
ResponseUtils.writeXsfrFormProtection(pageContext);
USALiveUtils.writeFormProfileId(pageContext);
%>
<table>
<tbody>
<tr class="headerRow">
<th>Part</th><th>Description</th><th>Select for return</th><th>Return quantity</th><th>Description of issue</th>
</tr>
<% while(results.next()) {
	String partNumber=results.getFormattedValue("partNumber");
%>
<tr>
<td><img src="<%=StringUtils.prepareScript(partNumber)%><%=RMAUtils.partImageSuffix%>" width="<%=imageWidth%>"/>
<br/>
Part #: <%=StringUtils.prepareHTML(partNumber)%>
<br/>
<div style="text-align:center"><a onClick="onViewLargeImage(this,'<%=StringUtils.prepareScript(partNumber)%>');" target="_blank"> View Larger Image </a></div></td>
<td><%=StringUtils.prepareHTML(results.getFormattedValue("description"))%></td>
<td><input type="checkbox" onClick="onClickPart(this);" name="rmaAllowedPartsIds" value="<%=results.getFormattedValue("rmaAllowedPartsId")%>"></td>
<td><input type="text" data-validators="minimum:1 maximum:99999"  id="rmaPartsQuantity_<%=results.getFormattedValue("rmaAllowedPartsId")%>" name="rmaPartsQuantity_<%=results.getFormattedValue("rmaAllowedPartsId")%>" value="" size="2"></td>
<td><input type="text" class="maxLength:4000" id="rmaPartsIssue_<%=results.getFormattedValue("rmaAllowedPartsId")%>" name="rmaPartsIssue_<%=results.getFormattedValue("rmaAllowedPartsId")%>" value="" size="100" ></td>
</tr>
<tr><td id="largeImage_<%=StringUtils.prepareScript(partNumber)%>" colspan="4"></td></tr>
<%} %>
</tbody>
</table>
RMA Description:<input id="rmaDescription" class="maxLength:4000" type="text" size="100" value="" name="rmaDescription">
<jsp:include page="rma_replacement.jsp"/>
<input id="createRMA" type="submit" value="Create RMA">
</form>
<%}else{%>
	<p class="not-authorized">User '<%=StringUtils.prepareHTML(user.getUserName())%>' does not have permission to view this page. Please contact USA Technologies if you need access.</p>
<%}%>
<jsp:include page="include/footer.jsp"/>
