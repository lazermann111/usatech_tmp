<%@page import="simple.bean.ConvertUtils"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@page import="simple.text.StringUtils,simple.servlet.RequestUtils"%>
<%
Date beginDate=RequestUtils.getAttribute(request, "beginDate", Date.class, false);
if(beginDate==null){
	beginDate= new Date (System.currentTimeMillis() -  24 * 60 * 60 * 1000);
}
Date endDate=RequestUtils.getAttribute(request, "endDate", Date.class, false);
if(endDate==null){
	endDate= new Date();
}
%>
<div id="selectDateRange" class="selectSection" style="padding:0px;margin:0px;">
<div class="sectionTitle">Select Date Range</div>
<div class="sectionRow" style="padding:0px;margin: 0px;">
<table>
<tbody>
<tr>
<td>
<div>
<label class="radioLabel">
<input type="radio" onclick="setDatesToYesterday()" value="Yesterday" name="dateRange">
Yesterday
</label>
<label class="radioLabel">
<input type="radio" onclick="setDatesToWeekToDate()" value="Week to Date" name="dateRange">
Week to Date
</label>
<label class="radioLabel">
<input type="radio" onclick="setDatesToLastWeek()" value="Last Week" name="dateRange">
Last Week
</label>
<label class="radioLabel">
<input type="radio" onclick="setDatesToMonthToDate()" value="Month to Date" name="dateRange">
Month to Date
</label>
<label class="radioLabel">
<input type="radio" onclick="setDatesToLastMonth()" value="Previous Month" name="dateRange">
Previous Month
</label>
</div>
</td>
</tr>
<tr>
<td>
<div>
<label for="startDateId">Start</label>
<input id="startDateId" type="text" data-validators="required validate-date dateFormat:'mm/dd/yyyy'" value="<%=ConvertUtils.formatObject(beginDate, "DATE:MM/dd/yyyy")%>" name="beginDate" data-format="%m/%d/%Y" data-toggle="calendar" size="10">
<label for="endDateId">End</label>
<input id="endDateId" type="text" data-validators="required validate-date dateFormat:'mm/dd/yyyy'" value="<%=ConvertUtils.formatObject(endDate, "DATE:MM/dd/yyyy")%>" name="endDate" data-format="%m/%d/%Y" data-toggle="calendar" size="10">
</div>
</td>
</tr>
</tbody>
</table>
</div>
</div>
