<%@page import="simple.text.StringUtils,simple.bean.ConvertUtils,simple.servlet.RequestUtils,simple.results.Results"%>
<%
Integer selectedId=RequestUtils.getAttribute(request, "userReportId", Integer.class, true);
%>
<select id="mainSelect" tabindex="1" onchange="onMasterChange(this)" size="18"><%
	Results results = RequestUtils.getAttribute(request, "userReports", Results.class, true);
	while(results.next()) {
	String itemId = results.getValue("userReportId", String.class);
	%><option value="<%=StringUtils.prepareCDATA(itemId)%>" <% if(selectedId != null && ConvertUtils.areSimilar(itemId, selectedId)) { %> selected="selected"<%} %>><%=StringUtils.prepareHTMLBlank(results.getFormattedValue("userReportName"))%></option>	    	
	<%}%>
</select>
