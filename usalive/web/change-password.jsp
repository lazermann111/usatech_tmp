<%@page import="simple.text.StringUtils,simple.servlet.RequestUtils,com.usatech.usalive.servlet.UsaliveUser"%>
<% UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);%>
<form autocomplete="off" id="changePass" method="post" action="modify_password.i" name="changePasswordForm">
	<% String errorMsg = RequestUtils.getAttribute(request, "onloadMessage", String.class, false);
	if(errorMsg != null && errorMsg.trim().length() > 0) { 
	%><div class="errorText"><%=StringUtils.prepareHTML(errorMsg)%></div><% 
	}%>
	<div class="caption">Change Password</div>
            <input type="hidden" name="editUserId" value="<%=user.getUserId()%>"/>
            <table><tr><td>
        <div class="selectSection">
    <div class="sectionTitle">Change password for '<%=StringUtils.prepareHTML(user.getUserName())%>':</div>
    <div class="sectionRow">
        <table class="input-rows">
            <tr>
            	<td colspan="2" class="requirementNote">Must be at least 8 characters and contain 1 uppercase letter,<br/> 1 lowercase letter, and 1 number or punctuation.</td>
            </tr>
            <tr>
            	<td>Password:</td>
            	<td><input id="password" maxlength="20" type="password" name="editPassword" data-validators="validate-password"/></td>                
            </tr>
            <tr>
            	<td>Confirm Password:</td>
            	<td><input maxlength="20" type="password" name="editConfirm" data-validators="validate-match" data-validator-properties="{matchInput:'password', matchName:'password'}"/></td>
            </tr>
            <tr>
            	<td colspan="2" align="center" valign="bottom">
                	<input type="submit" name="submitButton" value="Save Changes"/>
                	<input type="submit" onClick="onResetPassword();" name="submitButton" value="Reset Password"/>
           		</td>
           </tr>
        </table>
    </div>
</div>
</td></tr></table>
<script type="text/javascript">
function onResetPassword(){
	$('changePass').action="reset_password.i";
}

window.addEvent("domready", function() {
    new Form.Validator.Inline.Mask(document.changePasswordForm); 
});
</script>
<input type="hidden" name="session-token" value="<%=StringUtils.prepareCDATA(RequestUtils.getSessionToken(request))%>"/>
</form>