<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="java.util.HashSet"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Arrays"%>
<%@page import="simple.param.ParamEditor"%>
<%@page import="simple.param.html.DefaultParamRenderer"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.results.Results"%>
<%
	UsaliveUser user = (UsaliveUser) RequestUtils.getUser(request);
	String userName = user.getUserName();
	Results userReportDetail = RequestUtils.getAttribute(request, "userReportDetail", Results.class, false);
	Results userReportParams = RequestUtils.getAttribute(request, "userReportParams", Results.class, false);
	Results exportReports = RequestUtils.getAttribute(request, "exportReports", Results.class, true);
	Results userReports = RequestUtils.getAttribute(request, "userAvaReports", Results.class, false);
	Results frequencies = RequestUtils.getAttribute(request, "frequencies", Results.class, true);
	Results userTransport = RequestUtils.getAttribute(request, "userTransport", Results.class, true);

	exportReports.addGroup("reportId");
	boolean existing = (userReportDetail != null && userReportDetail.next());
	Integer transportId;
	Set<Integer> notAllowedTransportTypes;
	boolean isPayPeriodFrequency = false;
	if (existing) {
		transportId = userReportDetail.getValue("transportId", Integer.class);
		notAllowedTransportTypes = ConvertUtils.convert(Set.class, Integer.class, userReportDetail.getValue("notAllowedTransportTypes"));
		Integer frequencyId = userReportDetail.getValue("frequencyId", Integer.class);
		if (frequencyId != null && frequencyId == 12)
			isPayPeriodFrequency = true;
	} else {
		transportId = null;
		notAllowedTransportTypes = null;
	}
	DefaultParamRenderer paramRenderer = new DefaultParamRenderer();
	paramRenderer.setSelectAllMinimum(4);
	paramRenderer.setLabelAfterCheckControl(true);
	paramRenderer.setBeforeOption("<label class=\"checkbox\">");
	paramRenderer.setAfterOption("</label>");
	paramRenderer.setOnChange("onDetailChange()");
%>
<div class="selectSection">
	<div class="sectionTitle"><%if (!existing) {%>New <% } %>Report Information</div>
	<div class="sectionRow">
		<table id="reportConfiguration" class="input-rows">
			<tr>
				<td colspan="3" class="attention">New column(s) may be added to report(s) at any time. If you use an automated process to import report data into your system, please ensure that it will not be negatively affected if new column(s) are added to an existing report. Email attachments larger than 10 MB will be sent as Email With Link.</td>
			</tr>
			<tr>
				<th><label for="reportId">Report Name</label></th>
				<td class="reportSelection">
					<%
						if (!existing) {
					%> <select id="reportId" name="reportId" data-validators="required">
						<option value="" onselect="setReport({formatType: '', description: '', batchType: '', hasFrequency: false, hasBatchType: false})">-- Please Select --</option>
						<%
						StringBuilder sb = new StringBuilder();	
					    while (exportReports.next()) {
					    	String paramName = exportReports.getFormattedValue("paramName");
					    	if(!StringUtils.isBlank(paramName)) {
					    		paramRenderer.render(sb, "params." + paramName, exportReports.getValue("paramEditor", ParamEditor.class), exportReports.getFormattedValue("paramLabel"), exportReports.getFormattedValue("paramValue"));
					    	}
							if(exportReports.isGroupEnding("reportId")) {
								int batchType = exportReports.getValue("batchTypeId", Integer.class);
								int[] natt = exportReports.getValue("notAllowedTransportTypes", int[].class);
								Set<Integer> nattSet = new HashSet<Integer>();
								for(int tt : natt)
									nattSet.add(tt);
								if(notAllowedTransportTypes == null)
									notAllowedTransportTypes = nattSet;
								else
									notAllowedTransportTypes.retainAll(nattSet);			
						%>
						<option value="<%=exportReports.getValue("reportId", long.class)%>"
							onselect="setReport({formatType: '<%=StringUtils.prepareScript(exportReports.getFormattedValue("formatType"))
							%>', description: '<%=StringUtils.prepareScript(exportReports.getFormattedValue("description"))
							%>', batchType: '<%=StringUtils.prepareScript(exportReports.getFormattedValue("batchTypeName"))
							%>', hasFrequency: <%=(batchType == 0)%>, hasBatchType: <%=(batchType != 0)%>, params: '<%=StringUtils.prepareScript(sb.toString())
							%>', notAllowedTransportTypes: <%=Arrays.toString(natt)
							%>}); show('runSample');"><%=StringUtils.prepareHTML(exportReports.getFormattedValue("reportName"))%></option><%
						        sb.setLength(0);
							}
						}
					int[] natt = new int[]{9};
					Set<Integer> nattSet = new HashSet<>();
					for (int tt : natt)
						nattSet.add(tt);
					if (notAllowedTransportTypes == null) {
						notAllowedTransportTypes = nattSet;
					} else {
						notAllowedTransportTypes.retainAll(nattSet);
					}
					if (userReports != null && userReports.getRowCount() > 0) {
						sb.delete(0, sb.length());
						sb.append("<tr><th><label for=\"userAvaReportId\">User report</label></th><td>&nbsp;<select id=\"userAvaReportId\" name=\"userAvaReportId\" data-validators=\"required\">");
						while (userReports.next()) {
							sb.append("<option value=\"" + userReports.getValue("id", Integer.class) + "\">");
							sb.append(userReports.getFormattedValue("title"));
							sb.append("</option>");
						}
						sb.append("</select></td></tr>");
						%>
						<option value="0"
								onselect="setReport({formatType: 'BUILD_FOLIO',
								description: 'Custom user defined report',
								batchType: 'TIME PERIOD EXPORT TYPE',
								hasFrequency: true,
								hasBatchType: false, params: '<%=StringUtils.prepareScript(sb.toString())
								%>', notAllowedTransportTypes: <%=Arrays.toString(natt)
								%>}); hide('runSample');">:: User Defined Report ::
						</option>
						<%
						sb.setLength(0);
					}
					%>
					<option value="987654321012345678"
							onselect="setReport({formatType: 'BUILD_FOLIO',
									description: 'Add user default reports to report register. Reports to be restored: Payment Detail for EFT, Transactions Included in EFT, Sales Activity By Batch, Pending Payment Summary (Html).',
									batchType: 'TIME PERIOD EXPORT TYPE',
									hasFrequency: false,
									hasBatchType: false, params: '', notAllowedTransportTypes: <%=Arrays.toString(natt)
									%>}); hide('runSample');">:: Default Reports ::
					</option>
				</select><%
						} else {
					%><input type="hidden" name="reportId" value="<%=userReportDetail.getValue("reportId", long.class)%>"/><span id="reportId"><%=StringUtils.prepareHTML(userReportDetail.getFormattedValue("reportName"))%></span><%
						} %>
				</td><td><a id="runSample" onclick="if(!this.disabled) { window.location.href='/run_sample_basic_report_async.html?' + $('mainForm').toQueryString(); }">Run Report Sample</a></td>
			</tr>
			<tr>
				<th><label for="description">Description</label></th>
				<td colspan="2"><span id="description">
						<%
							if (existing) {
						%><%=StringUtils.prepareHTML(userReportDetail.getFormattedValue("description"))%>
						<%
							}
						%>
				</span></td>
			</tr>
			<tr>
				<th><label for="formatType">Format Type</label></th>
				<td><span id="formatType">
						<%
							if (existing) {
						%><%=StringUtils.prepareHTML(userReportDetail.getFormattedValue("formatType"))%>
						<%
							}
						%>
				</span></td>
			</tr>
			<%
				Integer batchType;
				if (existing)
					batchType = userReportDetail.getValue("batchTypeId",
							Integer.class);
				else
					batchType = null;
				if (batchType == null || batchType == 0) {
			%>
			<tr id="frequencyRow">
				<th><label for="frequencyId">Frequency</label></th>
				<td><select id="frequencyId" name="frequencyId" data-validators="required" onchange="frequencyChanged()"><%
				while (frequencies.next()) {
					Integer frequencyId = frequencies.getValue("frequencyId", Integer.class);
					if ("MINUTE".equals(frequencies.getValue("dateField", String.class)) && !"USATMaster".equals(userName))
						continue;
				%>
					<option value="<%=frequencyId%>" <%
					if (existing && frequencyId == userReportDetail.getValue("frequencyId", Integer.class)) {
					   %>selected="selected" <%}%>><%=StringUtils.prepareHTML(frequencies.getFormattedValue("frequencyName"))%></option><%
				}%>
				</select></td>
			</tr>
			<%}
			if (batchType == null || batchType != 0) { %>
			<tr id="batchTypeRow">
				<th><label for="batchType">Batch Type</label></th>
				<td><span id="batchType"><%
			if (existing) {
		%><%=StringUtils.prepareHTML(userReportDetail.getFormattedValue("batchTypeName"))%>
		<%	}%>
				</span></td>
			</tr>
			<%
		}
			%>
			<tr id="paramsRow"><td colspan="3"><table id="paramsTable"><%
			if (existing) {
            	while(userReportParams.next()) {
            		String paramName = userReportParams.getFormattedValue("paramName");
                    if(!StringUtils.isBlank(paramName)) {
                        paramRenderer.render(out, "params." + paramName, userReportParams.getValue("paramEditor", ParamEditor.class), userReportParams.getFormattedValue("paramLabel"), userReportParams.getFormattedValue("paramValue"));
                    }
            	}
            }%></table></td></tr>            
			<tr>
				<th>Available Transports</th>
				<td>
					<div class="itemMasterSelect">
						<select name="transportId" id="transportSelect" tabindex="1" data-validators="required" >
							<%
							boolean selected = false;
                            while (userTransport.next()) {
								Integer userCcsTransportId = userTransport.getValue("userCcsTransportId", Integer.class);
								String userCcsTransportName = userTransport.getValue("userCcsTransportName", String.class);
								Integer userCcsTransportTypeId = userTransport.getValue("userCcsTransportTypeId", Integer.class);
								String userCcsTransportStatusCd = userTransport.getValue("userCcsTransportStatusCd", String.class);
								boolean disabled = notAllowedTransportTypes.contains(userCcsTransportTypeId);
								if (userCcsTransportStatusCd.equals("E")) {
							%>
							<option data-transportTypeId="<%=userCcsTransportTypeId%>" title="<%=StringUtils.prepareCDATA(userCcsTransportName)%>[INVALID]" value="<%=userCcsTransportId%>"
								<%if (existing && transportId.equals(userCcsTransportId) && !disabled) { selected = true;%>
								selected="selected" <%} else if(disabled) {%>disabled="disabled" <%}
                                %>onselect="onAvailableTransportChange(<%=userCcsTransportTypeId%>, 1);"><%=StringUtils.prepareCDATA(userCcsTransportName)%>[INVALID]
							</option>
							<%
								} else {
							%> 
							<option data-transportTypeId="<%=userCcsTransportTypeId%>" title="<%=StringUtils.prepareCDATA(userCcsTransportName)%>" value="<%=userCcsTransportId%>"
								<%if (existing && transportId.equals(userCcsTransportId) && !disabled) { selected = true;%>
								selected="selected" <%} else if(disabled) {%>disabled="disabled" <%}
								%>onselect="onAvailableTransportChange(<%=userCcsTransportTypeId%>, 0);"><%=StringUtils.prepareCDATA(userCcsTransportName)%>
							</option>
							<%
								}
							}
                            %><option value=""<%if(!selected) { %> selected="selected"<%} %>>-- Please Select --</option>
						</select>
					</div>
				</td>
			</tr>
            <tr><td></td>		
				<td colspan="2">
					<input type="button" name="addTransport"
					value="Add Transport"
					onclick="updateTransportDeleteStatus('', 'none');onAddTransport();" />
					<input type="button" name="editTransport"
					value="Edit Transport"
					onclick="updateTransportDeleteStatus('', 'none');onEditTransport();" />
					<input type="button" name="deleteTransport"
					value="Delete Transport"
					onclick="updateTransportDeleteStatus('', 'none');onDeleteTransport();" />
				</td>
			</tr>
			<tr>
				<th><label for="errorEmail">Email Transport Errors To</label></th>
				<td>
					<input type="text" name="email_errors_to" id="email_errors_to" value="<%if (existing) {%><%=StringUtils.prepareHTML(userReportDetail.getFormattedValue("errorEmailList"))%><%}%>" label="Email transport errors to" maxlength="4000" title="List of email addresses to email transport errors" valid="^([\w+-.%]+@[\w-.]+\.[A-Za-z]{2,4},?)+$" />
				</td>
			</tr>
			<tr><td colspan="3" class="footnote"><%=StringUtils.prepareHTML(RequestUtils.getTranslator(request).translate("report-register-ip-content"))%></td></tr>
			<tr id="payPeriodNote" class="<%= isPayPeriodFrequency ? "" : "hide" %>"><td colspan="3" class="footnote">Be sure to configure your Pay Period in Setup > Company Info</td></tr>
		</table>
		<div id="transportDeleteStatus" class="status-none"></div>
	</div>
	<script type="text/javascript">
function fileInputChanged(input) {
    var ignore = input.form.elements["ignore_" + input.name];
    if(ignore)
        ignore.value = "false";
    var overlay = $(input.name + "_overlay");
    if(overlay)
    	overlay.hide();
}
function show(id) {
	$(id).removeClass("hide");
}
function hide(id) {
	$(id).addClass("hide");
}
function setReport(options) {
    $("formatType").setText(options.formatType);
    $("description").setText(options.description);
    $("batchType").setText(options.batchType);
    $("frequencyId").disabled = !options.hasFrequency;
    $("frequencyRow").setStyle("display", options.hasFrequency ? "" : "none");
    $("batchTypeRow").setStyle("display", options.hasBatchType ? "" : "none");
    if(options.params) {
    	 $("paramsTable").setHTML(options.params);
    	 $("paramsRow").setStyle("display", "");
    } else
    	 $("paramsRow").setStyle("display", "none");
    var transportSelect = $("transportSelect");
    if($("reportId").selectedIndex > 0) {
    	if(options.notAllowedTransportTypes &amp;&amp; options.notAllowedTransportTypes.length > 0){
	    	$("transportSelect").getChildren().each(function(el) { 
	    		el.disabled = Array.from(options.notAllowedTransportTypes).some(function(tt) { return tt == el.get('data-transportTypeId');});
	    	});
	    } else {
    		$("transportSelect").getChildren().each(function(el) { 
    			el.disabled=false;
    		});
    	}
    	if(transportSelect.selectedIndex &lt; 0 || !transportSelect.getSelected().some(function(op) { return !op.disabled;})) 
    		transportSelect.setValue("");
    	$("runSample").disabled = false;
    } else {
    	$("transportSelect").getChildren().each(function(el) { 
    		   el.disabled=false;
    	});
        $("runSample").disabled = true;
    }
}
function onTransportTypeChange(transportTypeId) {
	//hide and disable all transport property inputs
   	var tr = $("tranportTypeRow");
   	var tab = tr.getParent();
   	var i = tr.rowIndex;
   	for(tr = $(tab.rows.item(++i)); tr.id != null &amp;&amp; tr.id.substr(0, 4) == "tpt_"; tr = $(tab.rows.item(++i))) {
   		var hide = (tr.id != "tpt_" + transportTypeId);
   		if(hide)
   			tr.addClass("hide");
   		else
   			tr.removeClass("hide");
   		var inp = tr.getFirst("input");
   		if(inp)
   			 inp.disabled = hide;
   	}
   	if(transportTypeId==3||transportTypeId==5){
   		if($("dateFormatedFolderNote")){
   			$("dateFormatedFolderNote").removeClass("hide");
   		}
   		
   	}else{
   		if($("dateFormatedFolderNote")){
   			$("dateFormatedFolderNote").addClass("hide");
   		}
   	}
}
window.addEvent("domready", function() {
	<%if (!existing) {%>var reportId = $("reportId"); 
		if(reportId.getSelected() != null) reportId.getSelected().fireCustomEvent('select');
	<%}%>
});

var selectedTransportTypeId;
var selectedTransportInvalid;

function onEditTransport(){
	$("transportSelect").getSelected().fireCustomEvent('select');
	 var selectedTransportId=$("transportSelect").getValue();
	 if(selectedTransportId==''){
		 alert("You need to select an available Transport.");
		 return;
	 }
	 if($("userTransportDetails")){
		 $("userTransportDetails").dispose();
	 }
	 new Request.HTML(
			 {url: "transport_property_detail.i", 
				 async: false,
				 evalScripts: false,
				 data: {transportId: selectedTransportId, transportTypeId:selectedTransportTypeId, transportInvalid:selectedTransportInvalid,fragment:true}, 
				 onSuccess: function(responseTree, responseElements, responseHTML, responseJavaScript){
					 $('mainContainer').adopt(new Element("div", {
							id: "userTransportDetails",
							html: responseHTML
						}));
					 if(responseJavaScript) Browser.exec(responseJavaScript);
					 onTransportTypeChange(selectedTransportTypeId);
				 }}).send();
	 if($("transportForm")){
		 var transportDetailsDialog= new Dialog({
				title : "Edit Transport",
				content: $('userTransportDetails'),
				height: 400, 
				width : 600,
				destroyOnClose: true,
				close: true,
				movable: false
			});
		 transportDetailsDialog.show();
	 }
}

function onAddTransport(){
	 new Request.HTML(
			 {url: "transport_property_new.i", 
				 async: false,
				 evalScripts: false,
				 data: {transportTypeId:1, fragment:true}, 
				 onSuccess: function(responseTree, responseElements, responseHTML, responseJavaScript){
					 $('mainContainer').adopt(new Element("div", {
							id: "userTransportDetails",
							html: responseHTML
						}));
					 if(responseJavaScript) Browser.exec(responseJavaScript);
					 onTransportTypeChange(1);
				 }}).send();
	 if($("transportForm")){
		 var transportDetailsDialog= new Dialog({
				title : "Add Transport",
				content: $('userTransportDetails'),
				height: 400, 
				width : 600,
				destroyOnClose: true,
				close: true,
				movable: false
			});
		 transportDetailsDialog.show();
	 }
}

function onDeleteTransport(){
	$("transportSelect").getSelected().fireCustomEvent('select');
	 var selectedTransportId=$("transportSelect").getValue();
	 if(selectedTransportId==''){
		 alert("You need to select an available Transport.");
		 return;
	 }
	 new Request(
			 {url: "delete_transport.i", 
				 data: {transportId: selectedTransportId, fragment:true}, 
				 evalScripts: true,
				 onFailure: function(){
					 updateTransportDeleteStatus('Transport delete failed. An application error occurred.','info-failure');
				 }
			 }).send();
}
function updateTransportDeleteStatus(msg, type) {
	if(!msg || msg.trim().length == 0)
		$('transportDeleteStatus').set("html", "&amp;#160;");
	else 
		$('transportDeleteStatus').set("text", msg);
	$('transportDeleteStatus').className = "status-" + type;
}

function onAvailableTransportChange(transportTypeId,transportInvalid){
	selectedTransportTypeId=transportTypeId;
	selectedTransportInvalid=transportInvalid;
	updateTransportDeleteStatus("", "none");
}

function updateTransportName(status){
	var selectedOp=$("transportSelect").getSelected();
	selectedOp.set("onselect", "onAvailableTransportChange("+selectedTransportTypeId+",0);");
	if(status>0){
		selectedOp.set("text",$("transportNameOriginal").value+"[INVALID]");
		selectedOp.set("title",$("transportNameOriginal").value+"[INVALID]");
		selectedTransportInvalid=1;
	}else{
		selectedOp.set("text",$("transportNameOriginal").value);
		selectedOp.set("title",$("transportNameOriginal").value);
		selectedTransportInvalid=0;
	}
}

function refreshUserReportSection(){
	$("mainSelect").getSelected().fireCustomEvent('select');
	var selectedUserReportId=$("mainSelect").getValue();
	new Request.HTML(
			 {url: "refresh_user_report_section.i", 
				 data: {userReportId:selectedUserReportId}, 
				 onSuccess: function(responseTree, responseElements, responseHTML, responseJavaScript){
					 $('mainSelectSection').set('html',responseHTML);
				 }}).send();
}

function itemMasterExtraCheck(){
	$('reportId').set('data-validators','required');
	if(selectedTransportInvalid==1){
		alert("Selected transport is invalid.");
		return false;
	}else{
		return true;
	}
}


function onMasterChangeExtra(){
	var activateBtn = $("userReportStatusButton");
	if(activateBtn)
		activateBtn.dispose();
	<%if (existing && userReportDetail.getFormattedValue("userReportStatus").equals("E")) {%>
	$('itemMasterButtons').adopt(new Element("input", {type: "button", id:"userReportStatusButton", value:'Activate', onclick:"activateUserReport("+$('mainSelect').value+"); $(this).dispose();"}));
	updateStatus('Deactivated due to transport error', 'info-failure');
	<%}else{%>
		if($('userReportStatusButton')){
			$('userReportStatusButton').dispose();
		}
	<%}%>
}

function activateUserReport(userReportIdInput){
	new Request.HTML(
			 {url: "user_report_activiate.i", 
				 data: {userReportId:userReportIdInput}, 
				 method: 'post',
				 evalScripts: true
				}).send();
}

function frequencyChanged() {
	var frequencyId = $('frequencyId').value;
	if (frequencyId == 12) {
		$('payPeriodNote').removeClass("hide");
	} else {
		$('payPeriodNote').addClass("hide");
	}
}
</script>
</div>

