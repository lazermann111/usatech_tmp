<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.usatech.layers.common.ProcessingUtils"%>
<%@page import="com.usatech.layers.common.util.WebHelper"%>
<%@page import="javax.mail.MessagingException"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="simple.io.Log"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.Connection"%>
<%@page import="com.usatech.usalive.web.RMAUtils"%>
<%@page import="java.sql.SQLIntegrityConstraintViolationException"%>
<%@page import="simple.translator.Translator"%>
<%@page import="simple.text.StringUtils"%>
<% Log log = Log.getLog();
	UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
	boolean hasPri = false;
	if ((user.isInternal() && user.hasPrivilege(ReportingPrivilege.PRIV_CUSTOMER_SERVICE)) || user.hasPrivilege(ReportingPrivilege.PRIV_RMA)) {
		hasPri = true;
	}
	
	String rmaNumber = null;
	InputForm inputForm = RequestUtils.getInputForm(request);
	int notAllowed = 1;
	int rmaTypeId = inputForm.getInt("rmaTypeId", true, 1);
	boolean okay = false;
	
	if (hasPri && "POST".equalsIgnoreCase(request.getMethod())) {
		Connection conn = DataLayerMgr.getConnectionForCall("CHECK_ALLOWED_RENTRAL_DEVICE");
		String email = null;
		try {
			if (rmaTypeId == 1) {
				inputForm.set("rmaReplacementQuantity", 0);
				DataLayerMgr.executeCall("CHECK_ALLOWED_RENTRAL_DEVICE", inputForm, true);
				notAllowed = inputForm.getInt("notAllowed", true, 1);
				if (notAllowed == 0) {
					if (inputForm.get("rmaShippingAddrId") == null) {
						DataLayerMgr.executeCall(conn, "INSERT_RMA_SHIPPING_ADDRESS", inputForm);
					}
					
					if (inputForm.get("rmaShippingCarrierId") == null) {
						inputForm.set("rmaShippingCarrierId", inputForm.get("ups"));
					}
					
					String message = RMAUtils.validateStringSize(inputForm, "rmaDescription", 4000);
					if (message != null){
						log.error("Failed to requests RMA due to failing description." + message);
						%><p class="not-authorized">User '<%=StringUtils.prepareHTML(user.getUserName()) %>' create RMA failed: <%=StringUtils.prepareHTML(message) %></p><%
						return;
					}
					
					DataLayerMgr.executeCall(conn, "CREATE_RMA_NUMBER", inputForm);
					email = inputForm.getString("email", false);
					DataLayerMgr.executeCall(conn, "INSERT_RENTAL_CREDIT_RMA_DEVICE", inputForm);
					rmaNumber = inputForm.getStringSafely("rmaNumber", "");
				}
			} else if (rmaTypeId == 2 || rmaTypeId == 4 || rmaTypeId == 5 || rmaTypeId == 6) {
				DataLayerMgr.executeCall("CHECK_ALLOWED_ALL_DEVICE", inputForm, true);
				notAllowed = inputForm.getInt("notAllowed", true, 1);
				if (notAllowed == 0) {
					if (inputForm.get("rmaShippingAddrId") == null) {
						DataLayerMgr.executeCall(conn, "INSERT_RMA_SHIPPING_ADDRESS", inputForm);
					}
					
					if (inputForm.get("rmaShippingCarrierId") == null) {
						inputForm.set("rmaShippingCarrierId", inputForm.get("ups"));
					}
					if (rmaTypeId==2){
						String needReplacement=inputForm.getString("needReplacement", false);
						int rmaReplacementQuantity=0;
						if(needReplacement!=null&&needReplacement.equals("Y")){
							int[] rmaDevices=inputForm.getIntArray("rmaDevices", false);
							rmaReplacementQuantity=rmaDevices.length;
							
						}
						inputForm.set("rmaReplacementQuantity",rmaReplacementQuantity);
					}
					if (rmaTypeId == 4) {
						if(StringUtils.isBlank(inputForm.getString("rmaDescription", false))) {
							log.error("Failed to requests RMA. RMA description is required");
							%><p class="not-authorized">User '<%=StringUtils.prepareHTML(user.getUserName()) %>' create RMA failed: description is required.</p><%
							return;
						}
					} else if (rmaTypeId == 5) {
						String rmaAttFeeAgree = inputForm.getStringSafely("rmaAttFeeAgree", "N");
						if (!"Y".equalsIgnoreCase(rmaAttFeeAgree)) {
							log.error("Failed to requests RMA. User did not agree to per device exchange fee.");
							%><p class="not-authorized">User '<%=StringUtils.prepareHTML(user.getUserName()) %>' create RMA failed: user did not agree to per device exchange fee.</p><%
							return;
						}
						inputForm.set("rmaDescription", "User agrees to a $29 fee per Like for Like device exchange.");
					}
					
					String message = RMAUtils.validateStringSize(inputForm, "rmaDescription", 4000);
					if (message != null) {
						log.error("Failed to requests RMA due to invalid description." + message);
						%><p class="not-authorized">User '<%=StringUtils.prepareHTML(user.getUserName()) %>' create RMA failed: <%=StringUtils.prepareHTML(message) %></p><%
						return;
					}
					
					DataLayerMgr.executeCall(conn, "CREATE_RMA_NUMBER", inputForm);
					email = inputForm.getString("email", false);
					long[] rmaDevices = inputForm.getLongArray("rmaDevices", true);
					for (long deviceId : rmaDevices) {
						inputForm.set("deviceId", deviceId);
						String isTelemetryOnly = inputForm.getStringSafely(deviceId + "-isTelemetryOnly", "N");
						inputForm.set("isTelemetryOnly", isTelemetryOnly);
						message = RMAUtils.validateStringSize(inputForm, deviceId + "-desc", 4000);
						if (message != null) {
							log.error("Failed to requests RMA due to invalid description." + message);
							%><p class="not-authorized">User '<%=StringUtils.prepareHTML(user.getUserName()) %>' create RMA failed: <%=StringUtils.prepareHTML(message)%></p><%
							return;
						}
						inputForm.set("issue", inputForm.get(deviceId + "-desc"));
						DataLayerMgr.executeCall(conn, "INSERT_RMA_DEVICE", inputForm);
					}
				}
			}
			
			request.setAttribute("RMA_DB_CONN", conn);
			Translator translator = RequestUtils.getTranslator(request);
			String toEmail = translator.translate("rma-to-cs-email", RMAUtils.rmaToCustomerServiceEmail);
			String toName = translator.translate("rma-to-cs-name", RMAUtils.rmaToCustomerServiceName);
			RMAUtils.sendEmail(request, translator.translate("rma-from-email", RMAUtils.rmaFromEmail), translator.translate("rma-from-name", RMAUtils.rmaFromName), "RMA:" + inputForm.get("rmaNumber"), toEmail, toName, "/rma_receipt.jsp");
			if (!StringUtils.isBlank(email)) {
				RMAUtils.sendEmail(request, translator.translate("rma-from-email", RMAUtils.rmaFromEmail), translator.translate("rma-from-name", RMAUtils.rmaFromName), "RMA:" + inputForm.get("rmaNumber"), email, user.getUserName(), "/rma_receipt.jsp");
			}
			okay = true;
		} catch (SQLIntegrityConstraintViolationException e) {
			%><p class="not-authorized">User '<%=StringUtils.prepareHTML(user.getUserName()) %>' create RMA failed: unique shipping address name is required.</p><%
			log.error("Failed to requests RMA for devicess. Please try again.", e);
			return;
		} catch (Exception e) {
			%><p class="not-authorized">User '<%=StringUtils.prepareHTML(user.getUserName()) %>' create RMA failed: please retry or contact customer service for assistance.</p><%
			log.error("Failed to requests RMA for devicess. Please try again.", e);
			return;
		} finally {
			if (okay) {
				conn.commit();
			} else {
				ProcessingUtils.rollbackDbConnection(log, conn);
			}
			ProcessingUtils.closeDbConnection(log, conn);
		}
	};
%>
<jsp:include page="include/header.jsp"/>
<% if (notAllowed == 0) { %>
	<% if (rmaTypeId == 1) { %>
		<jsp:include page="rma_rental_for_credit_display.jsp"/>
	<% } else if (rmaTypeId == 4 || rmaTypeId == 5 || rmaTypeId == 6) { %>
		<jsp:include page="rma_att_display.jsp"/>
	<% } else { %>
		<jsp:include page="rma_all_device_display.jsp"/>
	<%}; %>
<% } else { %>
	<p class="not-authorized">User '<%=StringUtils.prepareHTML(user.getUserName())%>' create RMA failed: access denied.</p>
<% }; %>
<jsp:include page="include/footer.jsp"/>
