<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.ResponseUtils"%>
<%@page import="com.usatech.usalive.web.RMAStatus"%>
<%@page import="java.util.Date"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="com.usatech.usalive.web.RMAUtils"%>
<%@page import="com.usatech.usalive.web.RMAType"%>
<% UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
	boolean isCustomerService = false;
	boolean hasPri = false;
	if (user.isInternal() && user.hasPrivilege(ReportingPrivilege.PRIV_CUSTOMER_SERVICE)) {
		hasPri = true;
		isCustomerService = true;
	} else if(user.hasPrivilege(ReportingPrivilege.PRIV_RMA)) {
		hasPri = true;
	}
	
	InputForm inputForm = RequestUtils.getInputForm(request);
	Results rmaResults = null;
	String rmaNumber = inputForm.getStringSafely("rmaNumber", "").trim();
	int rmaTypeId = inputForm.getInt("rmaTypeId", false, -1);
	if (hasPri) {
		if (rmaTypeId != -1) {
			boolean isValidRMANumber=true;
			if(!StringUtils.isBlank(rmaNumber)&&!rmaNumber.matches("^[0-9]*$")){
				isValidRMANumber=false;
			}
			if(isValidRMANumber){
				rmaResults =  DataLayerMgr.executeQuery("GET_RMA_SUMMARY", inputForm);
			}
		}
	}
	int i = 0; %>
<jsp:include page="include/header.jsp"/>
<script type="text/javascript">
function onViewRMA(){
	if ($("rmaNumber").value.length > 0) {
		$("rmaNumber").value = $("rmaNumber").value.trim();
		if(isNaN($("rmaNumber").value)){
			alert("Please input a number for RMA Number and try again.");
			return false;
		}
	}
	return true;
}

function updateStatus(resultType, message, rmaId, rmaStatusId){
	if (resultType == 'error') {
		$("message").set("html", "<p class='status-info-failure'>" + message + "</p>");
	} else {
		$("message").set("html", "<p class='status-info-success'>Successfully updated the RMA status</p>");
		var rmaStatus = 'New';
		if (rmaStatusId == 1) {
			rmaStatus = "Confirmed";
			$("td_actions_" + rmaId).set("html", "<input type=\"button\" value=\"Processed\" onClick=\"requestUpdateStatus(" + rmaId + ", 2);\">");
		} else if(rmaStatusId == -1) {
			rmaStatus = "Canceled";
			$("td_actions_" + rmaId).set("html", "");
		} else if(rmaStatusId == 2) {
			rmaStatus = "Processed";
			$("td_actions_" + rmaId).set("html", "");
		} else if(rmaStatusId == -2) {
			rmaStatus = "Deleted";
			$("td_actions_" + rmaId).set("html", "");
		}
		$("td_status_" + rmaId).set("html", rmaStatus);
	}
}

function requestUpdateStatus(rmaId, rmaStatusId) {
	var rmaStatus = 'New';
	if (rmaStatusId == 1) {
		rmaStatus = "Confirmed";
	} else if (rmaStatusId == -1) {
		rmaStatus = "Canceled";
	} else if (rmaStatusId == 2) {
		rmaStatus = "Processed";
	} else if(rmaStatusId==-2) {
		rmaStatus = "Deleted";
	}
	if(confirm("Are you sure you want to update this RMA status to " + rmaStatus)) {
		new Request.HTML({ 
			url: "rma_edit_status.html",
			method: 'post',
			data: {'rmaId':rmaId,'status':rmaStatusId<%
			ResponseUtils.writeXsfrJSONProtection(pageContext, false, true);%>}, 
			evalScripts: true,
			async: true
		}).send();
	}
}
</script>
<% if (hasPri) { %>
<span class="caption">View RMA</span>
<form name="rmaForm" id="rmaForm" action="rma_view.html" onSubmit="return onViewRMA();">
	<div id="message"></div>
	<div class="sectionRow">
		<table class="paramTable">
			<tr>
				<td>
					<b>Search by RMA Number:</b>
					<input id="rmaNumber" type="text" title="Empty means all RMA" size="12" name="rmaNumber" value="<%=StringUtils.prepareScript(rmaNumber) %>">
				</td>
				<td>
					<b>Type:</b><select id="rmaTypeId" name="rmaTypeId" tabindex="1" >
					<option value="0" <% if (rmaTypeId == 0){ %> selected<% } %>>All</option>
					<% for (RMAType rmaType : RMAType.values()) { %>
					<option value="<%=rmaType.getValue()%>" <% if (rmaTypeId == rmaType.getValue()) { %> selected<% } %>><%=rmaType.getDescription()%></option>
					<% } %>
					</select>
				</td>
				<td>
					<b>From Date:</b><input id="rmaFromDate" type="text" data-format="%m/%d/%Y" data-toggle="calendar" data-validators="validate-date dateFormat:'mm/dd/yyyy'" size="10" name="rmaFromDate" <% if (inputForm.get("rmaFromDate") != null) { %>value="<%=StringUtils.prepareHTML(inputForm.getStringSafely("rmaFromDate", ""))%>"<% } %> >
				</td>
				<td>
					<b>To Date:</b><input id="rmaToDate" type="text" data-format="%m/%d/%Y" data-toggle="calendar" data-validators="validate-date dateFormat:'mm/dd/yyyy'" size="10" name="rmaToDate" <% if (inputForm.get("rmaToDate") != null) { %>value="<%=StringUtils.prepareHTML(inputForm.getStringSafely("rmaToDate", ""))%>"<% } %> >
				</td>
				<td>
					<input id="search" type="submit" value="Search">
				</td>
			</tr>
		</table>
	</div>
</form>
<table class="folio">
	<tr class="headerRow">
		<th><a title="Sort Rows by Request Date" data-toggle="sort" data-sort-type="STRING">Request Date</a></th>
		<th><a title="Sort Rows by RMA Number" data-toggle="sort" data-sort-type="STRING">RMA Number</a></th>
		<th><a>Receipt</a></th>
		<th><a title="Sort Rows by Type" data-toggle="sort" data-sort-type="STRING">Type</a></th>
		<th><a title="Sort Rows by Replacement Kit Quantity" data-toggle="sort" data-sort-type="STRING">Replacement Kit Quantity</a></th>
		<th><a title="Sort Rows by Description" data-toggle="sort" data-sort-type="STRING">Description</a></th>
		<th><a title="Sort Rows by Status" data-toggle="sort" data-sort-type="STRING">Status</a></th>
		<% if(isCustomerService) { %>
		<th><a>Actions</a></th>
		<% } %>
	</tr>
	<tbody>
<% String rmaType = null;
	String rmaStatus = null;
	int rmaStatusId = 0;
	int count = 0;
	while (rmaResults != null && rmaResults.next()) {
		count++;
		rmaType = RMAType.getByValue(rmaResults.getValue("rmaTypeId", int.class)).getDescription();
		rmaStatusId = rmaResults.getValue("rmaStatus", int.class);
		rmaStatus = RMAStatus.getByValue(rmaStatusId).getDescription(); %>
		<tr class="<%=(++i % 2) == 0 ? "even" : "odd" %>RowHover">
			<td data-sort-value="<%=RMAUtils.rmaDateFormat.format(ConvertUtils.convert(Date.class, rmaResults.get("rmaCreateTs")))%>"><%=RMAUtils.rmaDateFormat.format(ConvertUtils.convert(Date.class, rmaResults.get("rmaCreateTs")))%></td>
			<td data-sort-value="<%=StringUtils.prepareScript(rmaResults.getFormattedValue("rmaNumber")) %>"><%=StringUtils.prepareHTML(rmaResults.getFormattedValue("rmaNumber")) %></td>
			<td><a href="rma_receipt_usalive.html?rmaId=<%=rmaResults.get("rmaId")%>&rmaTypeId=<%=rmaResults.get("rmaTypeId")%>">View Receipt</a></td>
			<td data-sort-value="<%=StringUtils.prepareScript(rmaType)%>"><%=StringUtils.prepareHTML(rmaType)%></td>
			<td data-sort-value="<%=StringUtils.prepareScript(rmaResults.getFormattedValue("rmaReplacementQuantity")) %>"><%=StringUtils.prepareHTML(rmaResults.getFormattedValue("rmaReplacementQuantity")) %></td>
			<td data-sort-value="<%=StringUtils.prepareScript(rmaResults.getFormattedValue("rmaDescription")) %>"><%=StringUtils.prepareHTML(rmaResults.getFormattedValue("rmaDescription")) %></td>
			<td id="td_status_<%=rmaResults.get("rmaId")%>" data-sort-value="<%=StringUtils.prepareScript(rmaStatus)%>"><%=StringUtils.prepareHTML(rmaStatus)%></td>
			<% if (isCustomerService) { %>
			<td id="td_actions_<%=rmaResults.get("rmaId")%>">
			<% if (rmaStatusId == 0) { %>
				<input type="button" value="Confirm" onClick="requestUpdateStatus(<%=rmaResults.get("rmaId")%>, 1);"> <input type="button" value="Cancel" onClick="requestUpdateStatus(<%=rmaResults.get("rmaId")%>, -1);"> <input type="button" value="Delete" onClick="requestUpdateStatus(<%=rmaResults.get("rmaId")%>, -2);">
			<% } else if (rmaStatusId == 1) { %>
				<input type="button" value="Processed" onClick="requestUpdateStatus(<%=rmaResults.get("rmaId")%>, 2);"> <input type="button" value="Delete" onClick="requestUpdateStatus(<%=rmaResults.get("rmaId")%>, -2);">
			<% } else { %>
				<input type="button" value="Delete" onClick="requestUpdateStatus(<%=rmaResults.get("rmaId")%>, -2);">
			<% } %>
			</td>
			<% } %>
		</tr>
<% }
	if (rmaTypeId != -1 && count == 0) { %>
		<tr>
			<td>No RMAs Found</td>
		</tr>
<% } %>
	</tbody>
</table>
<% }else{ %>
<p class="not-authorized">User '<%=StringUtils.prepareHTML(user.getUserName()) %>' does not have permission to view this page. Please contact USA Technologies if you need access.</p>
<% } %>
<jsp:include page="include/footer.jsp"/>
