<%@page import="com.usatech.usalive.servlet.USALiveUtils"%>
<%@page import="simple.servlet.ResponseUtils"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.results.BeanException"%>
<%@page import="simple.db.DataLayerException"%>
<%@page import="java.sql.SQLException"%>
<%@page import="simple.db.NotEnoughRowsException"%>
<%@page import="simple.io.Log"%>
<%@page import="simple.servlet.NotAuthorizedException"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
if(!user.hasPrivilege(ReportingPrivilege.PRIV_BACK_OFFICE))
    throw new NotAuthorizedException("You are not authorized to perform this function.");
InputForm form = RequestUtils.getInputForm(request);
%><jsp:include page="include/header.jsp"/><%
try {
    DataLayerMgr.selectInto("GET_PAYOR_TRAN_RECEIPT_INFO", form);
    if(user.getCustomerId()>0){
    	form.set("fileName", USALiveUtils.companyLogoPrefix+user.getCustomerId());
    	DataLayerMgr.selectInto("HAS_COMPANY_LOGO", form);
    	if(form.getInt("companyLogoCount", false, 0)>0){
	    	String baseUrl = RequestUtils.getBaseUrl(request, false);
	    	form.setAttribute("logoUrl", baseUrl + "/get_company_logo.i?customerId="+user.getCustomerId());
    	}
    }
    
} catch(NotEnoughRowsException e) {
	Log.getLog().warn("Could not produce receipt", e);
    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "usalive-payor-receipt-invalid-tran", "You do not have permission to view this transaction");
    %><jsp:include page="include/footer.jsp"/><%
    return;
} catch(SQLException e) {
	Log.getLog().warn("Could not produce receipt", e);
	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "usalive-payor-receipt-system-error", "We could not process your request at this time. Please try again");
    %><jsp:include page="include/footer.jsp"/><%
    return;
} catch(DataLayerException e) {
	Log.getLog().warn("Could not produce receipt", e);
	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "usalive-payor-receipt-system-error", "We could not process your request at this time. Please try again");
    %><jsp:include page="include/footer.jsp"/><%
    return;
} catch(BeanException e) {
	Log.getLog().warn("Could not produce receipt", e);
	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "usalive-payor-receipt-system-error", "We could not process your request at this time. Please try again");
    %><jsp:include page="include/footer.jsp"/><%
    return;
}
//form.setAttribute("logoUrl", baseUrl + "/images/usa_tech_logo_trimmed.gif"); // "/repository/logo." + ext + "?id=" + id);
//form.setAttribute("linkUrl", baseUrl);
form.setAttribute("fragment", true);	
%>
<form action="payor_receipt_send.html" method="post" data-toggle="validateInline">
<%
ResponseUtils.writeXsfrFormProtection(pageContext);
USALiveUtils.writeFormProfileId(pageContext);
%>
<input type="hidden" name="tranId" value="<%=form.getLong("tranId", true, 0L) %>"/>
<table>
    <tr class="no-print"><td class="center"><div class="caption">ePort Online: View Receipt</div></td></tr>
    <tr><td ><div class="preview">
		<jsp:include page="shared/backoffice_receipt.jsp"/>
		</div></td></tr>
	<tr class="no-print"><td>
	<fieldset>
	<legend>Email this Receipt</legend>
	<table>
    <tr><td><label>Name:</label></td><td><input type="text" name="payorName" data-validators="required" placeholder="The company or customer name" title="Enter the name of the person or business to whom the receipt will be sent" maxlength="50" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "payorName", String.class, false))%>"/></td></tr>
    <tr><td><label>Email:</label></td><td><input type="email" name="payorEmail" data-validators="required validate-email" placeholder="The email" title="Enter the email to which the receipt will be sent" maxlength="60" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "payorEmail", String.class, false))%>"/></td></tr>
    <tr><td colspan="2" class="center"><button type="submit">Send Receipt</button></td></tr>
    </table>
    </fieldset>
    </td></tr>
</table>
</form>
<jsp:include page="include/footer.jsp"/>