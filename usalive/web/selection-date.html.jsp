<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@page import="simple.text.StringUtils,simple.servlet.RequestUtils,simple.results.Results"%>
<%
Integer basicReportId=RequestUtils.getAttribute(request, "basicReportId", Integer.class, true);
Results result=DataLayerMgr.executeQuery("GET_REPORT_NAME", new Object[]{basicReportId});
String reportName=null;
if(result.next()){
	reportName=result.getFormattedValue("reportName");
}
String dateName=RequestUtils.getAttribute(request, "dateName", String.class, true);
Integer outputType=RequestUtils.getAttribute(request, "outputType", Integer.class, false);
Date beginDate=RequestUtils.getAttribute(request, "beginDate", Date.class, false);
if(beginDate==null){
	beginDate= new Date (System.currentTimeMillis() -  24 * 60 * 60 * 1000);
}
%>
<jsp:include page="include/header.jsp"/>
<script type="text/javascript">
function setDateToOld(numDays) {
 	var currentDate = new Date();
    currentDate.setDate(currentDate.getDate()-numDays);
    var month = currentDate.getMonth()+1;
    var year  = currentDate.getFullYear();
    var day   = currentDate.getDate();
    var oldDay = month+"/"+day+"/"+year;
    $("startDateId").value=oldDay;
}
</script>
<table class="selectBody">
<caption><%if(!StringUtils.isBlank(reportName)){%><%=StringUtils.prepareHTML(reportName)%><%} %></caption>
<tbody>
<tr><td>
<div id="selectDateRange" class="selectSection">
<div class="sectionTitle">Select Date</div>
<div class="sectionRow">
<table>
	<tr>
	<td>
	<div>
	<label class="radioLabel">
	<input type="radio" onclick="setDateToOld(1)" value="Yesterday" name="dateRange">
	Yesterday
	</label>
	<label class="radioLabel">
	<input type="radio" onclick="setDateToOld(7)" value="7 Days Ago" name="dateRange">
	7 Days Ago
	</label>
	<label class="radioLabel">
	<input type="radio" onclick="setDateToOld(30)" value="30 Days Ago" name="dateRange">
	30 Days Ago
	</label>
	</div>
	</td>
</tr>
	<tr>
		<td>
			<form id="searchForm" name="searchForm" action="run_sql_folio_report_async.i">
			<input type="hidden" name="basicReportId" value="<%=basicReportId%>" />
			<%if(outputType!=null){ %>
				<input type="hidden" name="outputType" value="<%=outputType%>" />
			<%} %>
			<label for="startDateId"><%=StringUtils.prepareHTML(dateName) %></label>
			<input id="startDateId" type="text" value="<%=ConvertUtils.formatObject(beginDate, "DATE:MM/dd/yyyy")%>" name="beginDate" data-format="%m/%d/%Y" data-toggle="calendar" size="10">
			<div style="text-align: center; clear: both;">
				<input type="submit" value="Run Report" />
			</div>
			</form>
		</td>
	</tr>
</table>
</div>
</div>
</td>
</tr>
</tbody>
</table>

<jsp:include page="include/footer.jsp"/>