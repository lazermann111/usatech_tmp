<%@page import="simple.results.Results"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
String type = RequestUtils.getAttribute(request, "eftType", String.class, true);
String callId = "GET_DASHBOARD_" + type.toUpperCase() + "_EFTS";
String noneVerbage = ("pending".equalsIgnoreCase(type) ? "Pending" : "Recent");
String amountVerbage = ("pending".equalsIgnoreCase(type) ? "Pending " : "");
Results results = DataLayerMgr.executeQuery(callId, RequestUtils.getInputForm(request));
int n = results.getRowCount();
%><div class="title2"><%=noneVerbage %> EFTs</div>
<div class="dashboard-eft-section">
<%
switch(n) {
    case 0:%>No <%=noneVerbage %> EFTs<% break;
    case 1:%>Payment Amount <% 
    results.next();
    String currencyCd = results.getFormattedValue("currencyCd");
    String currencyFormat = (StringUtils.isBlank(currencyCd) ? "CURRENCY" : "CURRENCY:" + currencyCd);
    %><a href="run_report_async.i?reportId=2&amp;params.DocId=<%=results.getValue("docId", Number.class) %>" title="Click to view details"><%=StringUtils.prepareHTML(ConvertUtils.formatObject(results.getValue("amount"), currencyFormat)) %></a><br/>
    Payment Date <%=StringUtils.prepareHTML(results.getFormattedValue("payDate")) %><% 	
    break;
    default:%>
    <table class="sortable-table">
    <tr class="gridHeader"><th><a data-toggle="sort" data-sort-type="STRING">Customer</a></th><th><a data-toggle="sort" data-sort-type="NUMBER"><%=amountVerbage %>Amount</a></th><th><a data-toggle="sort" data-sort-type="NUMBER">Payment Date</a></th></tr>
    <tbody>
    <% while(results.next()) {
    	 currencyCd = results.getFormattedValue("currencyCd");
    	 currencyFormat = (StringUtils.isBlank(currencyCd) ? "CURRENCY" : "CURRENCY:" + currencyCd);
    	 %><tr><td data-sort-value="<%=StringUtils.prepareCDATA(results.getFormattedValue("customerName")) %>"><%=StringUtils.prepareHTML(results.getFormattedValue("customerName")) %></td>
    	 <td data-sort-value="<%=results.getValue("amount", Number.class) %>"><a href="run_report_async.i?reportId=2&amp;params.DocId=<%=results.getValue("docId", Number.class) %>" title="Click to view details"><%=StringUtils.prepareHTML(ConvertUtils.formatObject(results.getValue("amount"), currencyFormat)) %></a></td> 
    	 <td data-sort-value="<%=results.getValue("payDate", Long.class) %>"><%=StringUtils.prepareHTML(results.getFormattedValue("payDate")) %></td></tr><%
    }
    %></tbody>
    </table><%
}
%>
</div>