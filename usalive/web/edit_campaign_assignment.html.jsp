<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="com.usatech.report.ReportingUser.ResourceType"%>
<%@page import="com.usatech.report.ReportingUser.ActionType"%>
<%@page import="com.usatech.usalive.web.CampaignUsaliveUtils"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="simple.io.Log"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.Connection"%>
<%! 
private static final Log log = Log.getLog();
%>
<%
UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
if(!((user.isInternal()&&user.hasPrivilege(ReportingPrivilege.PRIV_CUSTOMER_SERVICE))||user.hasPrivilege(ReportingPrivilege.PRIV_MANAGE_CAMPAIGN))){
	%><p class="not-authorized">User '<%=StringUtils.prepareHTML(user.getUserName()) %>' does not have permission to view this page. Please contact USA Technologies if you need access.</p>
<% return;
}
InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
user.checkPermission(ResourceType.CAMPAIGN, ActionType.EDIT, inputForm.getAttribute("campaignId"));
String actionType = inputForm.getString("actionType", true);
boolean isSuccess=false;%>
<script type="text/javascript"><%
if(!CampaignUsaliveUtils.checkAllow("CHECK_ALLOW_CUSTOMER", inputForm)){
	%>addOrRemoveSelectedResult('<%=StringUtils.prepareScript(actionType)%>',-1, 'You have no permission to assign for this customer.');<%
}else if(!CampaignUsaliveUtils.checkAllow("CHECK_ALLOW_REGIONS", inputForm)){
	%>addOrRemoveSelectedResult('<%=StringUtils.prepareScript(actionType)%>',-1, 'You have no permission to assign for the regions.');<%
}else if(!CampaignUsaliveUtils.checkAllow("CHECK_ALLOW_TERMINALS", inputForm)){
	%>addOrRemoveSelectedResult('<%=StringUtils.prepareScript(actionType)%>',-1, 'You have no permission to assign for the terminals.');<%
}else{
	Connection conn = DataLayerMgr.getConnectionForCall("ADD_CAMPAIGN_CUSTOMER");
	try{
		if(actionType.equalsIgnoreCase("add")){
			if(inputForm.get("assignCustomerId")!=null){
				DataLayerMgr.executeUpdate(conn, "ADD_CAMPAIGN_CUSTOMER", inputForm);
			}
			if(inputForm.get("assignRegionIds")!=null){
				DataLayerMgr.executeUpdate(conn, "ADD_CAMPAIGN_REGION", inputForm);
			}
			if(inputForm.get("assignTerminalIds")!=null){
				DataLayerMgr.executeUpdate(conn, "ADD_CAMPAIGN_TERMINAL", inputForm);
			}
			%>addOrRemoveSelectedResult('add',1, '');<%
			isSuccess=true;
		}else if(actionType.equalsIgnoreCase("remove")){
			if(inputForm.get("assignCustomerId")!=null){
				DataLayerMgr.executeUpdate(conn, "DELETE_CAMPAIGN_CUSTOMER", inputForm);
			}
			if(inputForm.get("assignRegionIds")!=null){
				DataLayerMgr.executeUpdate(conn, "DELETE_CAMPAIGN_REGION", inputForm);
			}
			if(inputForm.get("assignTerminalIds")!=null){
				DataLayerMgr.executeUpdate(conn, "DELETE_CAMPAIGN_TERMINAL", inputForm);
			}
			%>addOrRemoveSelectedResult('remove',1, '');<%
			isSuccess=true;
		}else{
			%>addOrRemoveSelectedResult('',-1, 'Unknown action.');<%
		}
		conn.commit();
	}catch(Exception e) {
		try { conn.rollback(); } catch(SQLException e1) { log.warn("Could not rollback"); } 
		%>addOrRemoveSelectedResult('<%=StringUtils.prepareScript(actionType)%>',-1, 'An error occurred on the server.');<%
		log.error("Failed to assign the campaign. Please try again.", e);
	}finally{
		conn.close();
	}
}
%>
</script>
<% if(isSuccess){ %>
<jsp:include page="assign_campaign_searchall.jsp"/>
<%} %>