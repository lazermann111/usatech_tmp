<%@page import="com.usatech.usalive.servlet.USALiveUtils"%>
<%@page import="simple.servlet.ResponseUtils"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>

<%
	UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
	int selectedCampaignId = ConvertUtils.getInt(RequestUtils.getAttribute(request, "campaignId", true));
	String mode = ConvertUtils.getString(RequestUtils.getAttribute(request, "mode", false), "view");
	String backToManageCampaignLink=null;
	Results campaignResults=null;
	boolean hasPri=false;
	if(user.isInternal()){
		if(user.hasPrivilege(ReportingPrivilege.PRIV_CUSTOMER_SERVICE)){
			hasPri=true;
		}
		backToManageCampaignLink="manage_campaign_internal.i"+"?campaignId="+selectedCampaignId;
		campaignResults=DataLayerMgr.executeQuery("GET_CAMPAIGNS_INTERNAL", null);
	}else{
		if(user.hasPrivilege(ReportingPrivilege.PRIV_MANAGE_CAMPAIGN)){
			hasPri=true;
		}
		backToManageCampaignLink="manage_campaign.i"+"?campaignId="+selectedCampaignId;
		campaignResults=DataLayerMgr.executeQuery("GET_CAMPAIGNS", new Object[]{RequestUtils.getAttribute(request, "simple.servlet.ServletUser.userId", int.class, true)});
	}
%>
<jsp:include page="include/header.jsp"/>
<%if(hasPri){ %>
<script type="text/javascript">
window.addEvent('domready', function() { 
	var select = $("campaignId");
	Form.attachOnSelect(select);
	var frm = $("whichForm");
	var content = $("whichContent");
	var wrap = new Element("div");
	wrap.adopt(content);
	new Form.Validator.Inline.Mask(frm);
    new Form.Request(frm, wrap);
	var whichDialog = new Dialog({ content: wrap, title: "Find which Campaign will be Applied", destroyOnClose: false, height: 135 });
	content.removeClass("hide");
	window.showWhichDialog = function(terminalId) {
	    frm.terminalId.value = terminalId; 
	    wrap.empty();
	    wrap.adopt(content);
	    whichDialog.show();
	};
	window.hideWhichDialog = function() {
		whichDialog.hide();
	};
});
function backToManageCampaign(){
	location.href="<%=backToManageCampaignLink%>";
}

function onSearchCustomer(){
	clearAssignSection();
	$("customerName").value=$("customerName").value.trim();
	$('searchBy').value="customer";
	$("waitImage").style.display='block';
	new Request.HTML({ 
    	url: "assign_campaign_search.html",
    	data: $("assignCampaignForm"), 
    	method: 'post',
    	update : $("customerSelect"),
    	onComplete : function() {
    		$("waitImage").style.display='none';
    	},
		evalScripts: true
	}).send();
}

function onSearchRegion(){
	$("message").set("html","");
	if($("customerId").selectedIndex==-1){
		alert("Please select a customer.");
	}else{
		$('searchBy').value="region";
		$("waitImage").style.display='block';
		new Request.HTML({ 
	    	url: "assign_campaign_search.html",
	    	data: $("assignCampaignForm"), 
	    	method: 'post',
	    	update : $("regionSelect"),
	    	onComplete : function() {
	    		$("waitImage").style.display='none';
	    	},
			evalScripts: true
		}).send();
	}
	
}

function clearAssignSection(){
	$("message").set("html","");
	$("campaignAssignSectionButton").style.display='none';
	$("campaignAssignSection").set("html", "");
}

function onSelectAllRegionButton(){
	$('regionIds').getElements("option").each(function(el) {
		el.selected="selected";
	});
}

function onUnSelectAllRegionButton(){
	$('regionIds').getElements("option").each(function(el) {
		el.selected="";
	});
}

function onSearchAll(){
	$("message").set("html","");
	if($("customerId").selectedIndex==-1){
		alert("Please select a customer.");
	}else{
		$('searchBy').value="all";
		$("waitImage").style.display='block';
		$("deviceSerialCd").value="";
		$("location").value="";
		$("assetNbr").value="";
		if($('deviceSearchByText').value && $('deviceSearchByText').value.trim()!=""){
			if($('deviceSearchBy').value==1){
				$("deviceSerialCd").value=$('deviceSearchByText').value.replace(/\s/g, "");;
			}else if($('deviceSearchBy').value==2){
				$("location").value=$('deviceSearchByText').value.trim();
			}else{
				$("assetNbr").value=$('deviceSearchByText').value.trim();
			}
		}
		new Request.HTML({ 
	    	url: "assign_campaign_search.html",
	    	data: $("assignCampaignForm"), 
	    	method: 'post',
	    	update : $("campaignAssignSection"),
	    	onComplete : function() {
	    		$("waitImage").style.display='none';
	    		$("campaignAssignSectionButton").style.display='block';
	    	},
			evalScripts: true
		}).send();
	}
	
}

function onSelectAllCheckbox(){
	$("message").set("html","");
	if($("selectAllCheckbox").checked){
		$('campaignAssignSection').getElements("input[type=checkbox])").each(function(el) {
			el.checked=true;
		});
	}else{
		$('campaignAssignSection').getElements("input[type=checkbox])").each(function(el) {
			el.checked=false;
		});
	}
}

function onAddSelected(){
	if(!validateCheckbox()){
		alert("Please select at least one to assign the campaign.");
		return;
	}
	$("waitImage").style.display='block';
	$('actionType').value='add';
	$("message").set("html","");
	new Request.HTML({ 
    	url: "edit_campaign_assignment.html",
    	data: $("assignCampaignForm"), 
    	method: 'post',
    	update : $("campaignAssignSection"),
    	onComplete : function() {
    		$("waitImage").style.display='none';
    	},
		evalScripts: true
	}).send();
}

function addOrRemoveSelectedResult(actionType, status, errorMsg){
	$("waitImage").style.display='none';
	if(status>0){
		if(actionType=="add"){
			$("message").set("html","<p class='status-info-success'>Successfully added the selected to the campaign.</p>");
		}else{
			$("message").set("html","<p class='status-info-success'>Successfully removed the selected from the campaign.</p>");
		}
	}else{
		if(actionType=="add"){
			$("message").set("html","<p class='status-info-failure'>Failed to assign selected to the campaign. "+errorMsg+" Please try again.</p>");
		}else{
			$("message").set("html","<p class='status-info-failure'>Failed to remove selected from the campaign. "+errorMsg+" Please try again.</p>");
		}
	}
}

function onCancel(){
	$("message").set("html","");
	$('campaignAssignSection').getElements("input[type=checkbox])").each(function(el) {
		if($(el).get("campaignId") > 0){
			el.checked=true;
		}else{
			el.checked=false;
		}
	});
}

function onRemoveSelected(){
	if(!validateCheckbox()){
		alert("Please select at least one to remove the campaign from.");
		return;
	}
	$("waitImage").style.display='block';
	$('actionType').value='remove';
	$("message").set("html","");
	new Request.HTML({ 
    	url: "edit_campaign_assignment.html",
    	data: $("assignCampaignForm"), 
    	method: 'post',
    	update : $("campaignAssignSection"),
    	onComplete : function() {
    		$("waitImage").style.display='none';
    	},
		evalScripts: true
	}).send();
}

function validateCheckbox(){
	var assignElements=$('campaignAssignSection').getElements("input[type=checkbox])");
	for (var i=0;i<assignElements.length;i++){
		if(assignElements[i].checked){
			return true;
		}
	}
	return false;
}

function onSearchCampaignApply(){
	$("message").set("html","");
	if($("customerId").selectedIndex==-1){
		alert("Please select a customer.");
	}else{
		$('searchBy').value="campaignApply";
		$("waitImage").style.display='block';
		new Request.HTML({ 
	    	url: "assign_campaign_search.html",
	    	data: $("assignCampaignForm"), 
	    	method: 'post',
	    	update : $("message"),
	    	onComplete : function() {
	    		$("waitImage").style.display='none';
	    	},
			evalScripts: true
		}).send();
	}
	
}
function onWhichCampaignApplies(){
    $("message").set("html","");
    $("waitImage").style.display='block';
    new Request.HTML({ 
        url: "assign_campaign_search.html",
        data: $("whichForm"), 
        method: 'post',
        update : $("whichContent"),
        onComplete : function() {
            $("waitImage").style.display='none';
        },
        evalScripts: true
    }).send();
}
//var deviceTimeCampaignDialog = new Dialog.Help({ helpKey: 'campaign-find-apply-by-time', destroyOnClose: false, height: 200 });
var deviceDialog = new Dialog.Help({ helpKey: 'campaign-device-search', destroyOnClose: false, height: 200 });

var currentDate  = new Date(),
    currentDay   = currentDate.getDate() < 10 
                 ? '0' + currentDate.getDate() 
                 : currentDate.getDate(),
    currentMonth = currentDate.getMonth() < 9 
                 ? '0' + (currentDate.getMonth() + 1) 
                 : (currentDate.getMonth() + 1),
    dateC = currentMonth + '/' + currentDay + '/' +  currentDate.getFullYear() + ' 00:00';

function submitEnter(e)
    {
        var keycode;
        if (window.event) keycode = window.event.keyCode;
        else if (e) keycode = e.which;
        else return true;

        if (keycode == 13)
        {
            onSearchAll();
            return false;
        }
        else
            return true;
    }

</script>
<form name="assignCampaignForm" id="assignCampaignForm">
<input type="hidden" id="fragment" name="fragment" value="true"/>
<input type="hidden" id="searchBy" name="searchBy" value="true"/>
<input type="hidden" id="actionType" name="actionType" value="true"/>
<input type="hidden" id="deviceSerialCd" name="deviceSerialCd" value=""/>
<input type="hidden" id="location" name="location" value=""/>
<input type="hidden" id="assetNbr" name="assetNbr" value=""/>
<%
ResponseUtils.writeXsfrFormProtection(pageContext);
USALiveUtils.writeFormProfileId(pageContext);
%>
<table class="full"><tr><td>
<div class="caption">Assign Devices to Campaign</div>
<div id="waitImage" style="display:none">Please wait...<img alt="Please wait" src="images/pleasewait.gif"/></div>
<div id="message" class="manageCampaignMessage"></div>
<div class="selectSection">
            <div class="sectionTitle">Select Campaign</div>
                <div class="sectionRow">
Campaign Name: <select name="campaignId" id="campaignId" ><%
        while(campaignResults.next()) {
        	int resultCampaignTypeId=ConvertUtils.getInt(campaignResults.get("campaignTypeId"));
	        if(resultCampaignTypeId==1||resultCampaignTypeId==4||resultCampaignTypeId==5){%>
	        <option onselect="clearAssignSection();" value="<%=StringUtils.prepareCDATA(campaignResults.getFormattedValue("campaignId"))%>" 
	        <%if(selectedCampaignId==ConvertUtils.getInt(campaignResults.get("campaignId"))){%> selected="selected"<%}%>
	        >
	        <%=StringUtils.prepareCDATA(campaignResults.getFormattedValue("campaignName"))%>
	       	</option><%
	        }
		}%>
        </select> 
        <table class="subSectionTable">      
    <tr><td>
<% if(user.isInternal()){ %>
			<div class="sectionTitle">Select Customer</div>
				<div class="sectionRow">
					<div>Name:<input title="Search Customer only returns customers whose device has prepaid payment setup" name="customerName" id="customerName" type="text" /><input title="Search Customer only returns customers whose device has prepaid payment setup" id="searchCustomer" type="button" onclick="onSearchCustomer();" value="Search Customer"/>
					</div>
					<div id="customerSelect">
       					<select size="6" name="customerId" id="customerId"  class="assignCampaignSelect"></select>
       				</div>		
    			</div>
</td><td>
<%}else{ %>
	<input type="hidden" id="customerId" name="customerId" value="<%=RequestUtils.getAttribute(request, "simple.servlet.ServletUser.customerId", int.class, true)%>" />
<%} %>
	<div class="sectionTitle">Select Region</div>
	<div class="sectionRow">
		<div>
				<input title="Get All Region only returns regions where device has prepaid payment setup" id="searchRegion" type="button" onclick="onSearchRegion();" value="Get All Region"/>
				<input id="selectAllRegionButton" type="button" onclick="onSelectAllRegionButton();" value="Select All Region"/>
				<input id="unselectAllRegionButton" type="button" onclick="onUnSelectAllRegionButton();" value="Unselect All Region"/>
		</div>
		<div id="regionSelect">
				<select multiple="multiple" size="6" name="regionIds" id="regionIds"  class="assignCampaignSelect"></select>
				</div>
 			</div>
    </td></tr></table>
<div class="sectionTitle">Select Device</div>
                <div class="sectionRow">               
<select name="deviceSearchBy" id="deviceSearchBy" size="1">
<option value="1" selected="selected" >By Serial Number</option>
<option value="2">By Location</option>
<option value="3">By Asset Nbr</option>
</select>
<input id="deviceSearchByText" type="text" name="deviceSearchByText" size="35" title="Empty means all device." onKeyPress="return submitEnter(event)">
<a class="help-button" onclick="deviceDialog.show();"><div>&nbsp;</div></a>
</div>
<div class="center">
	<input id="searchAll" type="button" onclick="onSearchAll();" value="Search"/>
	<input type="button" value="Back To Manage Campaign" onclick="backToManageCampaign();"/>
</div>
</div>
</div>
</td>
</tr>
</table>
<div id="campaignAssignSectionButton" style="display:none">
	<table>
	<tr>
	<td>
	<input id="addSelect" type="button" onclick="onAddSelected();" value="Assign selected" <% if (mode.equals("view")) {%> disabled="true"<%} %>/>
	<input id="cancelSelect" type="button" onclick="onCancel();" value="Cancel" <% if (mode.equals("view")) {%> disabled="true"<%} %>/>
	<input id="removeSelect" type="button" onclick="onRemoveSelected();" value="Remove selected" <% if (mode.equals("view")) {%> disabled="true"<%} %>/>
	</td>
	</tr>
	<tr><td>
	<span class="impliedCampaign">* Campaign will be implicitly applied because it is assigned at the region or customer level </span>
	</td></tr>
	</table>
</div>
<div id="campaignAssignSection">
</div>
</form>
<div id="whichContent" class="hide">
    <form id="whichForm" name="whichForm" action="assign_campaign_which.html">
    <input type="hidden" name="terminalId"/>
    Enter the date and time: <input title="Timestamp in the format of 01/01/2014 14:30" name="applyingTimestamp" id="applyingTimestamp" type="text" value="" data-validators="validate-date" data-validator-properties="{dateFormat:'mm/dd/yyyy HH:MM'}" required/>
    <div class="center"><input type="submit" value="Submit"/></div>
    </form>
</div>
<%}else{%>
	<p class="not-authorized">User '<%=StringUtils.prepareHTML(user.getUserName()) %>' does not have permission to view this page. Please contact USA Technologies if you need access.</p>
<%}%>
<jsp:include page="include/footer.jsp"/>