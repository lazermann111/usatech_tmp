<%@page import="simple.text.StringUtils,simple.servlet.RequestUtils,simple.results.Results"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.db.DataLayerMgr"%>
<%
InputForm inputForm = RequestUtils.getInputForm(request);
Results results =  DataLayerMgr.executeQuery("GET_USER_RMA_SHIPPING_ADDRESS", inputForm);
%>
<select id="rmaShippingAddrId" name="rmaShippingAddrId" tabindex="1" onchange="onShippingChange(this)" size="4" style="width: 200px;" ><%
	while(results.next()) {
	Object rmaShippingAddrId = results.getValue("rmaShippingAddrId");
	String name = results.getFormattedValue("name");
	%><option id="data-rmaShippingAddrId-<%=rmaShippingAddrId%>" value="<%=rmaShippingAddrId%>"><%=StringUtils.prepareCDATA(name)%></option>	    	
	<%}%>
</select>
