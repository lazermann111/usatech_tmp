<%@page import="simple.servlet.NotAuthorizedException"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
UsaliveUser checkUser = (UsaliveUser) RequestUtils.getUser(request);
if(!checkUser.hasPrivilege(ReportingPrivilege.PRIV_ADVANCED_DASHBOARD)) {
	throw new NotAuthorizedException("You are not authorized to perform this function.");
}
//TODO: figure out how this page should look based on user permissions and preferences
%>
<jsp:include page="include/header.jsp"/>
<div class="caption">Dashboard</div>
<table id="dashboardContainer">
<tr>
<td>
<div id="deviceHealthSummary">
<jsp:include page="dashboard_device_health.html.jsp"/>
</div>
</td>
<td>
<div id="pendingEFTSummary"><%RequestUtils.setAttribute(request, "eftType", "pending", "request"); %>
<jsp:include page="dashboard_eft.html.jsp"/>
</div>
</td>
<td>
<div id="lastEFTSummary"><%RequestUtils.setAttribute(request, "eftType", "last", "request"); %>
<jsp:include page="dashboard_eft.html.jsp"/>
</div>
</td>
</tr>
<tr>
<td colspan="3">
<div id="transactionTrends">
<jsp:include page="dashboard_chart.i"/>
</div>
</td>
</tr>
</table>
<jsp:include page="include/footer.jsp"/>