<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.usatech.layers.common.ProcessingUtils"%>
<%@page import="com.usatech.layers.common.util.WebHelper"%>
<%@page import="javax.mail.MessagingException"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="simple.io.Log"%>
<%@page import="java.lang.Exception"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.Connection"%>
<%@page import="com.usatech.usalive.web.RMAUtils"%>
<%@page import="java.sql.SQLIntegrityConstraintViolationException"%>
<%@page import="simple.translator.Translator"%>
<%@page import="simple.text.StringUtils"%>
<%
	Log log = Log.getLog();
	UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
	boolean hasPri=false;
	if((user.isInternal()&&user.hasPrivilege(ReportingPrivilege.PRIV_CUSTOMER_SERVICE))||user.hasPrivilege(ReportingPrivilege.PRIV_RMA)){
		hasPri=true;
	}
	String rmaNumber=null;
	InputForm inputForm = RequestUtils.getInputForm(request);
	int notAllowed=1;
	int rmaTypeId=inputForm.getInt("rmaTypeId", true, 3);
	boolean okay=false;
	if(hasPri && "POST".equalsIgnoreCase(request.getMethod())){
		Connection conn = DataLayerMgr.getConnectionForCall("CHECK_ALLOWED_PARTS");
		String email=null;
		try{
			if(rmaTypeId==3){
				DataLayerMgr.executeCall("CHECK_ALLOWED_PARTS", inputForm, true);
				notAllowed=inputForm.getInt("notAllowed", true, 1);
				if(notAllowed==0){
					long[] rmaAllowedPartsIds=inputForm.getLongArray("rmaAllowedPartsIds", true);
					String needReplacement=inputForm.getString("needReplacement", false);
					int rmaReplacementQuantity=0;
					if(needReplacement!=null&&needReplacement.equals("Y")){
						for(long rmaAllowedPartsId:rmaAllowedPartsIds){
							int partsCount=inputForm.getInt("rmaPartsQuantity_"+rmaAllowedPartsId, false, 0);
							rmaReplacementQuantity=rmaReplacementQuantity+partsCount;
						}
						
					}
					inputForm.set("rmaReplacementQuantity",rmaReplacementQuantity);
					if(inputForm.get("rmaShippingAddrId")==null){
						DataLayerMgr.executeCall(conn, "INSERT_RMA_SHIPPING_ADDRESS", inputForm);
					}
					if(inputForm.get("rmaShippingCarrierId")==null){
						inputForm.set("rmaShippingCarrierId",inputForm.get("ups"));
					}
					String message=RMAUtils.validateStringSize(inputForm, "rmaDescription", 4000);
					if(message!=null){
						log.error("Failed to requests RMA due to failing rma description."+message);
						%><p class="not-authorized">User '<%=StringUtils.prepareHTML(user.getUserName()) %>' requests RMA for parts failed. <%=StringUtils.prepareHTML(message)%></p><%
						return;
					}
					DataLayerMgr.executeCall(conn, "CREATE_RMA_NUMBER", inputForm);
					email=inputForm.getString("email", false);
					
					for(long rmaAllowedPartsId:rmaAllowedPartsIds){
						inputForm.set("rmaAllowedPartsId", rmaAllowedPartsId);
						message=RMAUtils.validateStringSize(inputForm, "rmaPartsIssue_"+rmaAllowedPartsId, 4000);
						if(message!=null){
							log.error("Failed to requests RMA due to failing parts issue description."+message);
							%><p class="not-authorized">User '<%=StringUtils.prepareHTML(user.getUserName()) %>' requests RMA for parts failed. <%=StringUtils.prepareHTML(message)%></p><%
							return;
						}
						message=RMAUtils.validatePartsQuantity(inputForm, "rmaPartsQuantity_"+rmaAllowedPartsId);
						if(message!=null){
							log.error("Failed to requests RMA due to failing parts quantity."+message);
							%><p class="not-authorized">User '<%=StringUtils.prepareHTML(user.getUserName()) %>' requests RMA for parts failed. <%=StringUtils.prepareHTML(message)%></p><%
							return;
						}			
						inputForm.set("issue", inputForm.get("rmaPartsIssue_"+rmaAllowedPartsId));
						inputForm.set("quantity", inputForm.get("rmaPartsQuantity_"+rmaAllowedPartsId));
						DataLayerMgr.executeCall(conn, "INSERT_RMA_PARTS", inputForm);
					}
				}
			}
			request.setAttribute("RMA_DB_CONN", conn);
			Translator translator=RequestUtils.getTranslator(request);
			String toEmail=translator.translate("rma-to-cs-email",RMAUtils.rmaToCustomerServiceEmail);
			String toName=translator.translate("rma-to-cs-name",RMAUtils.rmaToCustomerServiceName);;
			RMAUtils.sendEmail(request, translator.translate("rma-from-email",RMAUtils.rmaFromEmail), translator.translate("rma-from-name",RMAUtils.rmaFromName), "RMA:"+inputForm.get("rmaNumber"), toEmail, toName, "/rma_receipt.jsp");
			if(!StringUtils.isBlank(email)){
				RMAUtils.sendEmail(request, translator.translate("rma-from-email",RMAUtils.rmaFromEmail), translator.translate("rma-from-name",RMAUtils.rmaFromName), "RMA:"+inputForm.get("rmaNumber"), email, user.getUserName(), "/rma_receipt.jsp");
			}
			okay=true;
		}catch(SQLIntegrityConstraintViolationException e){
			%><p class="not-authorized">User '<%=StringUtils.prepareHTML(user.getUserName()) %>' requests RMA for parts failed. Unique shipping address name is required.</p><%
			log.error("Failed to requests RMA for devicess. Please try again.", e);
			return;
		}catch(Exception e) {
			%><p class="not-authorized">User '<%=StringUtils.prepareHTML(user.getUserName()) %>' requests RMA for parts failed. Please retry or contact customer service for assistance.</p><%
			log.error("Failed to requests RMA for devicess. Please try again.", e);
			return;
		}finally{
			if(okay) {
				conn.commit();
			}else{
				ProcessingUtils.rollbackDbConnection(log, conn);
			}
			ProcessingUtils.closeDbConnection(log, conn);
		}
	};
%>
<jsp:include page="include/header.jsp"/>
<%if(notAllowed==0){ %>
	<jsp:include page="rma_parts_display.jsp"/>
<%}else{ %>
	<p class="not-authorized">User '<%=StringUtils.prepareHTML(user.getUserName())%>' requests RMA for parts not allowed for return.</p>
<%};%>
<jsp:include page="include/footer.jsp"/>
