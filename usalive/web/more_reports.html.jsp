<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.regex.Matcher"%>
<%@page import="simple.text.RegexUtils"%>
<%@page import="simple.results.Results.Aggregate"%>
<%@page import="simple.results.ScrollableResults"%>
<%@page import="java.util.Map"%>
<%@page import="com.usatech.usalive.link.LinkRequirementEngine"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.regex.Pattern"%>
<%@page import="com.usatech.usalive.link.HasMoreAcctRequirement"%>
<%@page import="simple.servlet.ResponseUtils"%>
<%@page import="java.util.Date"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="simple.text.StringUtils"%>
<%
	UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
	HasMoreAcctRequirement hasMore=new HasMoreAcctRequirement();
	boolean hasPri=false;
	ScrollableResults cannedReports=null;
	if(hasMore.meetsRequirement(user, request)){
			hasPri=true;
			Map<String,Object> params = new HashMap<String,Object>();
			params.put("user", user);
			params.put("usage", "P");
			LinkRequirementEngine lre = RequestUtils.getAttribute(request, "com.usatech.usalive.link.LinkRequirementEngine", LinkRequirementEngine.class, true);
			params.put("satisfiedRequirementIds", lre.getSatisfiedRequirements(user, request));
			cannedReports = (ScrollableResults)DataLayerMgr.executeQuery("GET_WEB_LINKS_WITH_PRIVS", params);
	}else{
			hasPri=false;
	}
%>
<jsp:include page="include/header.jsp"/>
<span class="caption">More Reports</span>
<div class="spacer5"></div>
<script type="text/javascript">
function showTab(tab) {
	var selectedDiv = $("div_" + tab.id.substring(tab.id.lastIndexOf("_")+1));
	tab.getSiblings().each(function(li) {
		li.removeClass("selectedTab");
	});
	tab.addClass("selectedTab");
	if(selectedDiv) {
		selectedDiv.getSiblings().each(function(li) {
			li.removeClass("selectedTabContentDiv");
		});
		selectedDiv.addClass("selectedTabContentDiv");
	}
}
window.addEvent('domready', function() { showTab($("tab_1")); });
</script>
<%if(hasPri){ %>
<div class="topMenu">
<div id="tabContainer">
<%
cannedReports.addGroup("category");
String[] categories;
if(cannedReports.next()) {
	categories = cannedReports.getAggregate("category", null, Aggregate.SET, String[].class);
} else {
	categories = null;
}
String linkExtra;
if(user != null)
	linkExtra = "&amp;profileId=" + user.getUserId();
else
	linkExtra = "";
%>
<ul><%
if(categories != null)
	for(int i = 0; i < categories.length; i++) {
		String cat = (categories[i] ==  null || categories[i].trim().length() == 0 ? "Other" : categories[i]);
		%><li id="tab_<%=i+1%>" class="topMenuItem topMenuItemSelected"><a onclick="showTab($(this).getParent());"
		title="A list of all <%=StringUtils.prepareCDATA(cat)%> reports" onmouseover="window.status = 'A list of all <%=StringUtils.prepareScript(cat)%> reports'; return true;"
		onmouseout="window.status = ''"><%=StringUtils.prepareHTML(cat)%></a></li><%
	}%>
</ul>
</div>
<div class="tabContents">
<%
int n = 1;
do {
	if(cannedReports.isGroupBeginning("category")) {
		 %><div id="div_<%=n++%>" class="tabContentDiv"><ul class="cannedReportList"><%
	}
	String href = cannedReports.getFormattedValue("href");
	%><li><a href="<%=StringUtils.prepareCDATA(href)
	%><%=href.contains("?")?"&amp;":"?"%>reportTitle=<%=StringUtils.prepareURLPart(cannedReports.getFormattedValue("title"))%><%=linkExtra %>"
	title="<%=StringUtils.prepareCDATA(cannedReports.getFormattedValue("description"))%>" onmouseover="window.status = '<%=StringUtils.prepareScript(cannedReports.getFormattedValue("description"))%>'; return true;"
	onmouseout="window.status = ''"><%=StringUtils.prepareHTML(cannedReports.getFormattedValue("title"))%></a>
	<% String[] groups = RegexUtils.match(".+[?](?:(.+)&)?referrer=([^&]+)(?:&(.+))?", href, null);
	if(groups != null && groups.length > 3 && !StringUtils.isBlank(groups[2])) { %>
	<a class="configure" href="<%=StringUtils.prepareURLPart(groups[2])%>?<%
		if(!StringUtils.isBlank(groups[1])) {%><%=StringUtils.prepareCDATA(groups[1])%><%
			if(!StringUtils.isBlank(groups[3])) {%>&amp;<% }
		}
		if(!StringUtils.isBlank(groups[3])) {%><%=StringUtils.prepareCDATA(groups[3])%><% }
		%>" title="Configure"><div></div></a><%
	}
		String sep="&amp;";
		%>
		<a class="report-link-csv" href="<%=StringUtils.prepareCDATA(href)%><%=sep%>outputType=21" title="View in Comma-separated values"><div></div></a>
		<a class="report-link-pdf" href="<%=StringUtils.prepareCDATA(href)%><%=sep%>outputType=24" title="View in PDF"><div></div></a>
		<a class="report-link-doc" href="<%=StringUtils.prepareCDATA(href)%><%=sep%>outputType=23" title="View in Word Document"><div></div></a>
		<a class="report-link-excel" href="<%=StringUtils.prepareCDATA(href)%><%=sep%>outputType=27" title="View in Excel"><div></div></a>
		<%
	%>
	</li><%
	if(cannedReports.isGroupEnding("category")) {
		%></ul></div><%
	}
} while(cannedReports.next()) ;%></div>
</div>
<%}else{%>
	<p class="not-authorized">User '<%=StringUtils.prepareHTML(user.getUserName()) %>' does not have permission to view this page. Please contact USA Technologies if you need access.</p>
<%}%>
<jsp:include page="include/footer.jsp"/>
