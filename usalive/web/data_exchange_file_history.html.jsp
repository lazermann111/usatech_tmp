<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.NotAuthorizedException"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="simple.db.Call.Sorter"%>
<%@page import="simple.servlet.GenerateUtils"%>
<%@page import="java.util.concurrent.TimeUnit"%>
<%@page import="simple.bean.ConvertException"%>
<%@page import="java.util.Date"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.usalive.servlet.USALiveUtils"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.text.StringUtils"%>
<%
InputForm form = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
UsaliveUser user = (UsaliveUser) RequestUtils.getUser(request);
if(!user.hasPrivilege(ReportingPrivilege.PRIV_CUSTOMER_SERVICE) && !user.hasPrivilege(ReportingPrivilege.PRIV_SYSTEM_ADMIN))
    throw new NotAuthorizedException("You are not authorized to perform this function.");
Sorter sorter = DataLayerMgr.getGlobalDataLayer().findCall("GET_DATA_EXCHANGE_FILE_HISTORY").getSorter();
String startDateText = form.getString("startDate", false);
Date startDate;
try {
	startDate = ConvertUtils.convert(Date.class, startDateText);
} catch(ConvertException e) {
	RequestUtils.getOrCreateMessagesSource(request).addMessage("warn", "usalive-backoffice-trans-invalid-date", "Invalid {0} date ''{1}''", "start", startDateText);
	startDate = null;
}
if(startDate == null) {
    startDate = new Date(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(30));
    form.setAttribute("startDate", startDate);
}
	
String endDateText = form.getString("endDate", false);
Date endDate;
try {
	endDate = ConvertUtils.convert(Date.class, endDateText);
} catch(ConvertException e) {
    RequestUtils.getOrCreateMessagesSource(request).addMessage("warn", "usalive-backoffice-trans-invalid-date", "Invalid {0} date ''{1}''", "end", endDateText);
    endDate = null;
}
if(endDate == null) {
    endDate = new Date(System.currentTimeMillis());
    form.setAttribute("endDate", endDate);
}
String[] sortBy = ConvertUtils.toUniqueArray(form.getStringArray("sortBy", false), String.class, "-createdTs","-fileId");
form.setAttribute("sortBy", sortBy);
int pageIndex = form.getInt("pageIndex", false, 1);
int pageSize = form.getInt("pageSize", false, PaginationUtil.DEFAULT_PAGE_SIZE);
int count;
try (Results results = sorter.executeSorted(form, pageSize, pageIndex, sortBy)) {
%>
<jsp:include page="include/header.jsp"/>
<span class="caption">Data Exchange File History</span>
<div class="spacer5"></div>
<form method="get" action="" data-toggle="validate">
	<%=USALiveUtils.getHiddenInputs(request)%>
	<div class="payor-transactions">
	<span class="label">Start Date</span> 
	<input type="text" id="startDate" size="8" maxlength="10" name="startDate" data-validators="required validate-date dateFormat:'mm/dd/yyyy'" data-label="Start Date" value="<%=ConvertUtils.formatObject(startDate, "DATE:MM/dd/yyyy")%>" data-toggle="calendar" data-format="%m/%d/%Y"/> 
	&nbsp;<span class="label">End Date</span> 
	<input type="text" id="endDate" size="8" maxlength="10" name="endDate" data-validators="validate-date dateFormat:'mm/dd/yyyy'" data-label="End Date" value="<%=ConvertUtils.formatObject(endDate, "DATE:MM/dd/yyyy") %>"  data-toggle="calendar" data-format="%m/%d/%Y"/> 
	<button type="submit">Search</button>
<div class="spacer5"></div>
<table class="sortable-table">
	<tr class="gridHeader"><%
	GenerateUtils.writeSortedTableHeader(pageContext, "fileId", "File Id", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortableFileName", "File Name", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "fileSize", "File Size", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "direction", "Direction", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "createdTs", "Created", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "customerId", "Customer ID", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortableCustomerName", "Customer Name", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "foreignEntityId", "Foreign Entity Id", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortableItemTypeDesc", "File Type", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "processCompleteTs", "Processing Complete", sortBy);
    GenerateUtils.writeSortedTableHeader(pageContext, "sortableProcessStatus", "Process Status", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortableProcessDetail", "Process Detail", sortBy);
	%>
	</tr><%	
results.setFormat("createdTs", "DATE:MM/dd/yyyy HH:mm:ss");
results.setFormat("processCompleteTs", "DATE:MM/dd/yyyy HH:mm:ss");
while(results.next()) {
    long fileId = results.getValue("fileId", Long.class);
    char direction = results.getValue("direction", char.class);
%>
	<tr>
	    <td><%=fileId%></td>
	    <td><a href="data_exchange_file_view.html?fileId=<%=fileId%>" title="View Contents"><%=StringUtils.prepareHTML(results.getFormattedValue("fileName"))%></a></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("fileSize"))%></td>
	    <td><%switch(direction) { case 'I': %>Incoming<% break; case 'O': %>Outgoing<% break; default: %>Unknown (<%=StringUtils.prepareHTMLBlank(String.valueOf(direction)) %>)<%} %></td>
        <td><%=StringUtils.prepareHTML(results.getFormattedValue("createdTs"))%></td>
        <td><%=StringUtils.prepareHTML(results.getFormattedValue("customerId"))%></td>
        <td><%=StringUtils.prepareHTML(results.getFormattedValue("customerName"))%></td>
        <td><%=StringUtils.prepareHTML(results.getFormattedValue("foreignEntityId"))%></td>
        <td><%=StringUtils.prepareHTML(results.getFormattedValue("itemTypeDesc"))%></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("processCompleteTs"))%></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("processStatus"))%></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("processDetail"))%></td>
	</tr><%
}
int rows = results.getRowCount();
if(pageIndex == 1 && rows < PaginationUtil.DEFAULT_PAGE_SIZE) { 
    count = rows;
} else { 
    count = sorter.executeCount(form);
}
}%>
<tr><td colspan="12" class="pagination-container">
<jsp:include page="include/pagination2.jsp">
    <jsp:param name="pageIndex" value="<%=pageIndex %>"/>
    <jsp:param name="pageSize" value="<%=pageSize %>"/>
    <jsp:param name="count" value="<%=count %>"/>
</jsp:include>
</td></tr>
</table>
</div>
</form>
<jsp:include page="include/footer.jsp"/>