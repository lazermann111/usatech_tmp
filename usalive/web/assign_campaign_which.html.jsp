<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.io.Log"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.usatech.report.ReportingUser.ActionType"%>
<%@page import="com.usatech.report.ReportingUser.ResourceType"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="java.util.Date"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.db.DataLayerMgr"%>
<%
try {
	long terminalId = RequestUtils.getAttribute(request, "terminalId", Long.class, true);
	Date date = RequestUtils.getAttribute(request, "applyingTimestamp", Date.class, true);
	UsaliveUser checkUser = (UsaliveUser) RequestUtils.getUser(request);
	checkUser.checkPermission(ResourceType.TERMINAL, ActionType.VIEW, terminalId);
	Results results = DataLayerMgr.executeQuery("GET_APPLYING_CAMPAIGN_BY_TERMINAL", RequestUtils.getInputForm(request));
	Results resultsPromo = DataLayerMgr.executeQuery("GET_APPLYING_PROMO_CAMPAIGN_BY_TERMINAL", RequestUtils.getInputForm(request));
	Results resultsPurchaseDiscount = DataLayerMgr.executeQuery("GET_APPLYING_PURCHASE_CAMPAIGN_BY_TERMINAL", RequestUtils.getInputForm(request));
	int campaignCount=0;
	%>
	<table class="which-campaign-result">
	<%
	if (results.next()) {
		campaignCount++;
		results.setFormat("campaignDiscountPercent", "NUMBER:##0.########%");
		%>
		<% if(campaignCount==1){%>
		<tr><th>Device:</th><td><%=StringUtils.prepareHTML(results.getFormattedValue("deviceSerialCd")) %></td></tr>
		<tr><th>Date/Time:</th><td><%=ConvertUtils.formatObject(date, "DATE:MM/dd/yyyy hh:mm a") %></td></tr>
        <tr><th>Campaign:</th><td><%=StringUtils.prepareHTML(results.getFormattedValue("campaignName")) %></td></tr>
        <tr><th>Discount:</th><td><%=StringUtils.prepareHTML(results.getFormattedValue("campaignDiscountPercent")) %></td></tr>
        <% }
        while (results.next()) {
			campaignCount++; %>
			<tr><th>Or </th><td></td></tr>
			<tr><th>Campaign:</th><td><%=StringUtils.prepareHTML(results.getFormattedValue("campaignName")) %></td></tr>
        	<tr><th>Discount:</th><td><%=StringUtils.prepareHTML(results.getFormattedValue("campaignDiscountPercent")) %></td></tr><%
        } 
    } 
	if(resultsPromo.next()) {
		campaignCount++;
		resultsPromo.setFormat("freeVendMaxAmount", "CURRENCY");
		%>
		<% if(campaignCount==1){%>
		<tr><th>Device:</th><td><%=StringUtils.prepareHTML(resultsPromo.getFormattedValue("deviceSerialCd")) %></td></tr>
		<tr><th>Date/Time:</th><td><%=ConvertUtils.formatObject(date, "DATE:MM/dd/yyyy hh:mm a") %></td></tr>
		<% } else{%>
		<tr><th>Or </th><td></td></tr>
		<%} %>
        <tr><th>Promo:</th><td><%=StringUtils.prepareHTML(resultsPromo.getFormattedValue("campaignName")) %></td></tr>
        <tr><th>Promo Purchase:</th><td>Every <%=StringUtils.prepareHTML(resultsPromo.getFormattedValue("nthVendFreeNum")) %> Purchase</td></tr>
        <tr><th>Promo Max Amount:</th><td><%=StringUtils.prepareHTML(resultsPromo.getFormattedValue("freeVendMaxAmount")) %></td></tr>
		<%
		while (resultsPromo.next()) {
			campaignCount++; %>
			<tr><th>Or </th><td></td></tr>
			<tr><th>Promo:</th><td><%=StringUtils.prepareHTML(resultsPromo.getFormattedValue("campaignName")) %></td></tr>
        	<tr><th>Promo Purchase:</th><td>Every <%=StringUtils.prepareHTML(resultsPromo.getFormattedValue("nthVendFreeNum")) %> Purchase</td></tr>
        	<tr><th>Promo Max Amount:</th><td><%=StringUtils.prepareHTML(resultsPromo.getFormattedValue("freeVendMaxAmount")) %></td></tr>
			<%
        } 
	} 
	if(resultsPurchaseDiscount.next()) {
		campaignCount++;
		resultsPurchaseDiscount.setFormat("purchaseDiscount", "CURRENCY");
		%>
		<% if(campaignCount==1){%>
		<tr><th>Device:</th><td><%=StringUtils.prepareHTML(resultsPurchaseDiscount.getFormattedValue("deviceSerialCd")) %></td></tr>
		<tr><th>Date/Time:</th><td><%=ConvertUtils.formatObject(date, "DATE:MM/dd/yyyy hh:mm a") %></td></tr>
		<% } else{%>
		<tr><th>Or </th><td></td></tr>
		<%} %>
        <tr><th>Campaign:</th><td><%=StringUtils.prepareHTML(resultsPurchaseDiscount.getFormattedValue("campaignName")) %></td></tr>
        <tr><th>Discount:</th><td><%=StringUtils.prepareHTML(resultsPurchaseDiscount.getFormattedValue("purchaseDiscount")) %></td></tr>
        <%
        while (resultsPurchaseDiscount.next()) {
			campaignCount++; %>
			<tr><th>Or </th><td></td></tr>
			<tr><th>Campaign:</th><td><%=StringUtils.prepareHTML(resultsPurchaseDiscount.getFormattedValue("campaignName")) %></td></tr>
        	<tr><th>Discount:</th><td><%=StringUtils.prepareHTML(resultsPurchaseDiscount.getFormattedValue("purchaseDiscount")) %></td></tr>
			<%
        }
	} 
	%></table>
	<%
	if(campaignCount==0){%><p class='status-info-success'>No Campaigns will be applied on Device at <%=ConvertUtils.formatObject(date, "DATE:MM/dd/yyyy hh:mm a") %></p><%
	}
} catch(Exception e) {
	Log.getLog().warn("Could not process request", e);
	%><p class='message-error center'>An error occurred while looking up which campaign will apply. Please close this window and try again.</p><%
}%>
<div class="center"><input type="button" value="Okay" onclick="hideWhichDialog();"/></div>
