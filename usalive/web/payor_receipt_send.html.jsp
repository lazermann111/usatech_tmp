<%@page import="simple.xml.sax.MessagesInputSource"%>
<%@page import="com.usatech.usalive.servlet.USALiveUtils"%>
<%@page import="com.usatech.usalive.web.PayorUtils"%>
<%@page import="simple.servlet.NotAuthorizedException"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
if(!user.hasPrivilege(ReportingPrivilege.PRIV_BACK_OFFICE))
    throw new NotAuthorizedException("You are not authorized to perform this function.");
if("POST".equalsIgnoreCase(request.getMethod())) {
	long tranId = RequestUtils.getAttribute(request, "tranId", Long.class, true);
	String payorName = RequestUtils.getAttribute(request, "payorName", String.class, true);
	String payorEmail = RequestUtils.getAttribute(request, "payorEmail", String.class,  true);
	MessagesInputSource messages = RequestUtils.getOrCreateMessagesSource(request);
	if(USALiveUtils.verifyEmail(payorEmail, "receipt", messages))
	    PayorUtils.sendReceiptForTran(user, tranId, payorName, payorEmail, RequestUtils.getLocale(request), RequestUtils.getBaseUrl(request, false),  messages);
}
RequestUtils.redirectWithCarryOver(request, response, "back_office_transactions.html");
%>