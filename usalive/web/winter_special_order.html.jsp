<%@page import="simple.servlet.InputForm"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="java.util.Date"%>
<%@page import="simple.db.ParameterException"%>
<%@page import="simple.io.Log"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%
//request.setAttribute("extra-stylesheets", "/css/normalize.css,/css/winter_main.css");
request.setAttribute("subtitle", "WINTER SPECIALS");
%>
<jsp:include page="include/header.jsp"/>
<style>
/*! HTML5 Boilerplate v4.3.0 | MIT License | http://h5bp.com/ */

html { font-size: 62.5%;}

img, embed, object, video {max-width: 100%;}

.links {
  width: 90%;
  margin: 10px auto;
  text-align: center;
}

.links span {
  float: left;
  font-size: 85%;
}

.links span > a {
  position: relative;
  display: block;
  padding: 2px 15px;
}

.links span >  a:hover,
.links span >  a:focus {
  text-decoration: none;
  background-color: #eee;
}

.offer-wrapper p {
  margin: 10px 6%;
}

.offer-wrapper {
    max-width:600px;
    width:95%;
    margin:10px auto 25px;
    position:relative;
    background-color: #ffffff;
    }

.presale-text, .postsale-text {
    background-color: #ffffff;
    padding: 0px 40px 0px;
    text-align: center;
    font-weight: 300;
    }
    
.presale-text p, .postsale-text p {
    font-size: 24px;
    line-height: 1.4em;
    }
    
.sale-text {
    background-color: #f4f7f8;
    margin: 20px 30px;
    padding: 20px;
    }
    
.sale-text h2, #main h2 {
    padding: 0;
    margin: 15px 0 0;
    line-height: 1em;
    }
    
.sale-text p {
    font-size: 20px;
    line-height: 1.4em;
    }
    
.info-text {
    background-color: #ffffff;
    margin: 20px 45px;
    }
    
    
#main {
    background: #ffffff;
    }
    
#main h2 {
    padding: 20px 45px 10px;
    line-height: 1em;
    }
    


#contact-form input[type="text"],
#contact-form input[type="email"],
#contact-form input[type="tel"],
#contact-form input[type="url"],
#contact-form textarea,
#contact-form button[type="submit"] {
    font:400 16px/16px "Helvetica Neue", Helvetica, Arial, sans-serif;
}
#contact-form {
    text-shadow:0 1px 0 #ffffff;
    background:#ffffff;
    padding: 0 45px 45px;
}

#contact-form span {
    cursor:pointer;
    color:#222222;
    display:block;
    margin:5px 0;
    font-weight:900;
}
#contact-form input[type="text"],
#contact-form input[type="email"],
#contact-form input[type="tel"],
#contact-form input[type="url"],
#contact-form textarea {
    width:100%;
    box-shadow:inset 0 1px 2px #DDD, 0 1px 0 #FFF;
    -webkit-box-shadow:inset 0 1px 2px #DDD, 0 1px 0 #FFF;
    -moz-box-shadow:inset 0 1px 2px #DDD, 0 1px 0 #FFF;
    border:1px solid #ccc;
    background:#fff;
    margin:0 0 5px;
    padding:10px;
    border-radius:5px;
}
#contact-form input[type="text"]:hover,
#contact-form input[type="email"]:hover,
#contact-form input[type="tel"]:hover,
#contact-form input[type="url"]:hover,
#contact-form textarea:hover {
    -webkit-transition:border-color 0.3s ease-in-out;
    -moz-transition:border-color 0.3s ease-in-out;
    transition:border-color 0.3s ease-in-out;
    border:1px solid #aaa;
}

#contact-form fieldset {
    margin: 5px 0;
}

#contact-form fieldset label {
    margin-right: 20px;
}

#contact-form button[type="submit"] {
    cursor:pointer;
    width:60%;
    border:none;
    background:#ef7f13;
    background-image:linear-gradient(bottom, #ef7f13 0%, #fab228 52%);
    background-image:-moz-linear-gradient(bottom, #ef7f13 0%, #fab228 52%);
    background-image:-webkit-linear-gradient(bottom, #ef7f13 0%, #fab228 52%);
    color:#222222;
    font-weight: 700;
    margin:30px 20% 0;
    padding:10px;
    border-radius:5px;
}
#contact-form button[type="submit"]:hover {
    background-image:linear-gradient(bottom, #d87312 0%, #f4ab20 52%);
    background-image:-moz-linear-gradient(bottom, #d87312 0%, #f4ab20 52%);
    background-image:-webkit-linear-gradient(bottom, #d87312 0%, #f4ab20 52%);
    -webkit-transition:background 0.3s ease-in-out;
    -moz-transition:background 0.3s ease-in-out;
    transition:background-color 0.3s ease-in-out;
}
#contact-form button[type="submit"]:active {
    box-shadow:inset 0 1px 3px rgba(0,0,0,0.5);
}
#contact-form input:focus,
#contact-form textarea:focus {
    outline:0;
    border:1px solid #999;
}

.slogan p {
font-size: 130%;
font-weight: 200;
text-align: center;
}

.usatinfo {
text-align: center;
}

.footer {
background-color: #ffffff;
text-align: center;
color: #222222;
font-size: 85%;
margin-bottom: 40px;
}

.wrapper {
background: #cae7fc;
}
#menu {
background: #EDEDED;
}
</style>
<script type="text/javascript" src="/scripts/modernizr-2.6.2.min.js"></script>
	<%
	InputForm form = RequestUtils.getInputForm(request);
	UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
	String action = RequestUtils.getAttribute(request, "submit", String.class, false);
	if(!StringUtils.isBlank(action)) { 
		String purchaseType = RequestUtils.getAttribute(request, "purchaseType", String.class, false);
		boolean okay = false;
        if(StringUtils.isBlank(purchaseType)) {
			%><div class="alert alert-error">Please choose whether this is a JumpStart order or not</div>
            <%
		} else {
		try {
			DataLayerMgr.executeCall("ADD_OFFER_INTEREST", RequestUtils.getInputForm(request), true);
			okay = true;
		} catch(ParameterException e) {
			Log.getLog().error("Invalid parameters for winter special interest for '" + RequestUtils.getAttribute(request, "email", String.class, false) + "'", e);
			%><div class="alert alert-error">Please provide valid values for all fields</div>
			<%
		} catch(Exception e) {
			Log.getLog().error("Could not add winter special interest for '" + RequestUtils.getAttribute(request, "email", String.class, false) + "'", e);
			%>
			<div class="alert alert-error">We are so sorry! We were unable to process your request at this time. Please try again.</div><%
		}
		String errorMessage = RequestUtils.getAttribute(request, "errorMessage", String.class, false);
		if(!StringUtils.isBlank(errorMessage)) {
			%><div class="alert alert-error"><%=StringUtils.prepareHTML(errorMessage) %></div>
			<%
		} else if(okay) {%>
		<div class="offer-wrapper">
          <img src="/images/thankyou.jpg" alt="Thank you for your order" />
          <!-- confirmation message -->
          <p style="padding: 0 45px;">Thank you for participating in our Winter Special. A sales representative will contact you within three business days to confirm and complete your order.</p>
          <p>&nbsp;
          </p>
        </div>  <!-- /end wrapper --><%
        	return;
		}
		}
	}	%> 
<div class="offer-wrapper">
    <div>
    <img src="/images/usat-winterspecials.gif" alt="Winter Specials from USAT"  style="margin-top: 25px;" />
    </div>
    
    <div class="presale-text">
        <p>
        USA Technologies is getting into the holiday spirit by offering some of the best deals of the year, but <span style="color: #ff0000; font-weight: 600;">ACT FAST!</span> The sooner you place your order, the deeper your discount!</p>
    </div>


    <div class="sale-text">
    <%
    long time20141203 = 1417582800000L; //12/03/2014
    long time20141210 = 1418187600000L; //12/10/2014
    long time20141217 = 1418792400000L; //12/17/2014    
    long time = System.currentTimeMillis();
    if(request.getRemoteAddr().startsWith("10.") || request.getRemoteAddr().equals("0:0:0:0:0:0:0:1")) {
    	Double addDays = RequestUtils.getAttribute(request, "addDays", Double.class, false);
    	if(addDays != null && addDays != 0) {
    		time += (addDays * 24 * 60 * 60 * 1000L);
    		%><div style="font-style:italic; font-weight: bold; color: #999999">As of <%=new Date(time) %></div><%
    	}
    }
    if(time <= time20141203) { %>
	    <h2 style="color: #404040 !important;">NOW through December 2 - <span style="color: #ff0000 !important; font-weight: 600;">$25 OFF</span></h2>
	    <hr style="border-top: 2px solid #999;">
	    <p>
	    <span style="font-weight: 600; color: #12b7ed;">ePort G9 $234 MDB/$284 Pulse</span><br>
	    <span style="font-weight: 600; color: #12b7ed;">FREE</span> shipping<br>
	    <span style="font-weight: 600; color: #12b7ed;">1 FREE</span> High Gain Antenna for every five ePorts ordered (max of 3 free)</p><%
	 } else if(time <= time20141210) {%>
	    <h2 style="color: #404040 !important;">December 3 through December 9 - <span style="color: #ff0000 !important; font-weight: 600;">$20 OFF</span></h2>
	    <hr style="border-top: 2px solid #999;">
	    <p>
	    <span style="font-weight: 600; color: #12b7ed;">ePort G9 $239 MDB/$289 Pulse</span><br>
	    <span style="font-weight: 600; color: #12b7ed;">FREE</span> Shipping <br>
	    <span style="font-weight: 600; color: #12b7ed;">1 FREE</span> High Gain Antenna for every five ePorts ordered (max of 2 free)</p><%
	 } else if(time <= time20141217) {%>
	    <h2 style="color: #404040 !important; font-size:23px">December 10 though December 16 - <span style="color: #ff0000 !important; font-weight: 600;">$10 OFF</span></h2>
	    <hr style="border-top: 2px solid #999;">
	    <p>
	    <span style="font-weight: 600; color: #12b7ed;">ePort G9 $249 MDB/$299 Pulse</span><br>
	    <span style="font-weight: 600; color: #12b7ed;">FREE</span> Shipping<br>
	    <span style="font-weight: 600; color: #12b7ed;">1 FREE</span> High Gain Antenna for every five ePorts ordered (max of 1 free)</p><%
	 } else {%> 
        <h2 style="color: #404040 !important;">Offer has expired</h2>
     <%} %>         
    </div>  <!-- close /sale-text  -->
    <div class="info-text">
                <p>
                <span style="font-weight: 600;">NOTE:</span> Quick start orders are not eligible for Winter Special promotions. ePort prices above do not include a $7.95 monthly fee.</p>
                <p>
                To reserve your ePorts with these special deals, you MUST complete the Holiday Sale Order Form below or speak to your sales rep. Supplies are limited. First come, first served.</p>

    </div>
        <div style="padding: 0 45px;">
        <hr style="border-top: 2px solid #999;">
        </div>
        
        <div id="main">
        
        <h2>Order Form</h2>
      <!-- form -->
      <form id="contact-form" action="" method="post">
      <input type="hidden" name="offerId" value="6" />      
		<div>
			<label>
				<span>FIRST NAME: (required)</span>
				<input placeholder="Please enter your first name" type="text" maxlength="50" required="required" name="first" value="<%=StringUtils.prepareCDATA(form.getStringSafely("first", user.getFirstName()))%>"/>
			</label>
		</div>
		<div>
			<label>
				<span>LAST NAME: (required)</span>
				<input placeholder="Please enter your last name" type="text" maxlength="50" required="required" name="last" value="<%=StringUtils.prepareCDATA(form.getStringSafely("last", user.getLastName()))%>"/>
			</label>
		</div>
		<div>
			<label>
				<span>EMAIL: (required)</span>
				<input placeholder="Please enter your email address" type="text" maxlength="100" required="required" name="email" value="<%=StringUtils.prepareCDATA(form.getStringSafely("email", user.getEmail()))%>"/>
			</label>
		</div>
		<div>
			<label>
				<span>PHONE NUMBER: (required)</span>
				<input placeholder="Please enter your phone number" type="text" maxlength="20" required="required" name="phone" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "phone", String.class, false))%>"/>
			</label>
		</div>
		<div>
	       <label>
	         <span>COMPANY NAME: (required)</span>
	         <input placeholder="Please enter your company name" type="text" maxlength="200" required="required" name="company" value="<%=StringUtils.prepareCDATA(form.getStringSafely("company", user.getCustomerName()))%>"/>
		  </label>
		</div>
		 <div>
        <label>
          <span>STREET ADDRESS TO SHIP TO: (required)</span>
          <input placeholder="Please enter your street address" type="text" maxlength="255" required="required" name="address" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "address", String.class, false))%>"/>
        </label>
      </div>
      <div>
        <label>
          <span>CITY: (required)</span>
          <input placeholder="Please enter your city" type="text" maxlength="50" required="required" name="city" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "city", String.class, false))%>"/>
        </label>
      </div>
      <div>
        <label>
          <span>STATE: (required)</span>
          <input placeholder="Please enter your state" type="text" maxlength="50" required="required" name="state" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "state", String.class, false))%>"/>
        </label>
      </div>
      <div>
        <label>
          <span>ZIP: (required)</span>
          <input placeholder="Please enter your zip or postal code" type="text" maxlength="20" required="required" name="postal" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "postal", String.class, false))%>"/>
        </label>
      </div>
      <span>
          <input type="checkbox" onclick="if (this.checked) {this.form.billingAddress.value = this.form.address.value; this.form.billingCity.value = this.form.city.value; this.form.billingState.value = this.form.state.value; this.form.billingPostal.value = this.form.postal.value;}" />
          BILLING ADDRESS IS SAME AS SHIPPING        
      </span>
      <div>
        <label>
          <span>BILLING STREET ADDRESS: (required)</span>
          <input placeholder="Please enter your billing street address" type="text" maxlength="255" required="required" name="billingAddress" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "billingAddress", String.class, false))%>"/>
        </label>
      </div>
      <div>
        <label>
          <span>BILLING CITY: (required)</span>
          <input placeholder="Please enter your billing city" type="text" maxlength="50" required="required" name="billingCity" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "billingCity", String.class, false))%>"/>
        </label>
      </div>
      <div>
        <label>
          <span>BILLING STATE: (required)</span>
          <input placeholder="Please enter your billing state" type="text" maxlength="50" required="required" name="billingState" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "billingState", String.class, false))%>"/>
        </label>
      </div>
      <div>
        <label>
          <span>BILLING ZIP: (required)</span>
          <input placeholder="Please enter your billing zip or postal code" type="text" maxlength="20" required="required" name="billingPostal" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "billingPostal", String.class, false))%>"/>
        </label>
      </div>
      <div>
      <% String purchaseType = RequestUtils.getAttribute(request, "purchaseType", String.class, false); %>
            <fieldset>
                <legend><span>WILL THIS BE A JUMPSTART ORDER? (required)</span></legend>                
                <label>
                <input type="radio" name="purchaseType" value="JumpStart"<%if(purchaseType != null && purchaseType.contains("JumpStart")) {%> checked="checked"<%} %>/>
                Yes (minumum of 5)
                </label>               
                <label>
                <input type="radio" name="purchaseType" value="Full Purchase"<%if(purchaseType != null && !purchaseType.contains("Full Purchase")) {%> checked="checked"<%} %>/>
                No
                </label>
            </fieldset>
        </div>
		<input type="hidden" name="current" value="Y"/>
		<div>
      <% String details = RequestUtils.getAttribute(request, "details", String.class, false); %>
            <fieldset>
                <legend><span>DO YOUR MACHINES HAVE MDB OR PULSE INTERFACES? (required)</span></legend>                
                <label>
                <input type="radio" name="details" value="MDB Interface"<%if(details != null && details.contains("MDB Interface")) {%> checked="checked"<%} %>/>
                MDB
                </label>               
                <label>
                <input type="radio" name="details" value="Pulse Interface"<%if(details != null && details.contains("Pulse Interface")) {%> checked="checked"<%} %>/>
                Pulse
                </label>
            </fieldset>
        </div>  
		<div>
			<label>
				<span>NUMBER OF G9 EPORTS TO PURCHASE: (required)</span>
				<input placeholder="Please enter the number of ePorts" type="text" maxlength="50" required="required" name="purchase" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "purchase", String.class, false))%>"/>
			</label>
		</div>
		<span>All orders are shipped via UPS Ground and are subject to NET 30 payment terms.</span>
		<div>
			<button name="submit" type="submit" id="contact-submit" value="optIn">SUBMIT</button>
		</div>
		</form>
		<!-- /form -->
  </div>  <!-- /main  -->
    <div class="slogan">
          <p>
          One company, one point of contact, one call.</p>
          </div>  <!-- /slogan -->
          <div class="footer">
          <a href="http://www.usatech.com"><img src="/images/logo_usatech.jpg" alt="USA Technologies" /></a>
          <p>
          800.633.0340  |  <a href="http://www.usatech.com">www.usatech.com</a>
          </div>  <!-- /footer -->
        </div> <!-- /wrapper -->
<jsp:include page="include/footer.jsp"/>
