<%@page import="com.usatech.usalive.web.RefundUtils"%>
<%@page import="simple.servlet.ResponseUtils"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.Date"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="simple.text.StringUtils"%>
<%
	UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
	boolean isCustomerService=false;
	boolean hasPri=false;
	if(user.isInternal()&&user.hasPrivilege(ReportingPrivilege.PRIV_CUSTOMER_SERVICE)){
			hasPri=true;
			isCustomerService=true;
	}else if(user.hasPrivilege(ReportingPrivilege.PRIV_ADMIN_REFUND)){
			hasPri=true;
	}
	InputForm inputForm = RequestUtils.getInputForm(request);
	String actionType=inputForm.getStringSafely("actionType", "");
	Results refundResults=null;
	if(hasPri){
		if(actionType.equals("search")){
			refundResults =  DataLayerMgr.executeQuery("GET_ADMIN_REFUNDS", inputForm);
		}
	}
	int i=0;
	Date twoDaysAgo = new Date(System.currentTimeMillis() - 2L * 24 * 3600 * 1000);
%>
<jsp:include page="include/header.jsp"/>
<script type="text/javascript">

function updateStatus(resultType, message, refundId, refundStatusId){
	if(resultType=='error'){
		$("message").set("html","<p class='status-info-failure'>"+message+"</p>");
	}else{
		var refundStatus='Waiting';
		if(refundStatusId==1){
			$("message").set("html","<p class='status-info-success'>Successfully approved the refund</p>");
			refundStatus="Approved";
			$("td_actions_"+refundId).set("html", "<input type=\"button\" value=\"Cancel\" onClick=\"requestUpdateStatus("+refundId+", -1);\">");
		}else if(refundStatusId==-1){
			$("message").set("html","<p class='status-info-success'>Successfully canceled the refund</p>");
			refundStatus="Canceled";
			$("td_actions_"+refundId).set("html", "<input type=\"button\" value=\"Approve\" onClick=\"requestUpdateStatus("+refundId+", 1);\">");
		}
		$("td_status_"+refundId).set("html", refundStatus);
	}
}

function updatePreferenceMessage(status){
	if(status==1){
		$("message").set("html","<p class='status-info-success'>Successfully updated notification preference</p>");
	}else{
		$("message").set("html","<p class='status-info-failure'>Failed to update notification preference. Please try again</p>");
	}
}

function requestUpdateStatus(refundId, refundStatusId){
	var refundStatus='New';
	if(refundStatusId==1){
		refundStatus="Approved";
	}else if(refundStatusId==-1){
		refundStatus="Canceled";
	}
	if(confirm("Are you sure you want to update this refund status to "+refundStatus)){
		new Request.HTML({ 
	        url: "refund_admin_edit.html",
	        method: 'post',
	        data: {'refundId':refundId,'status':refundStatusId<%
	        ResponseUtils.writeXsfrJSONProtection(pageContext, false, true);%>}, 
	        evalScripts: true,
	        async: true
	    }).send();
	}
}

function requestNotificationPreference(){
	new Request.HTML({ 
        url: "refund_admin_edit.html",
        method: 'post',
        data: $("refundAdminForm"), 
        evalScripts: true,
        async: true
    }).send();
}
</script>
<%if(hasPri){ %>
<div class="toggle-heading">
	<span class="caption">Admin Refunds</span>
</div>
<div id="message"></div>
<% if(!user.isInternal()){%>
<form name="refundAdminForm" id="refundAdminForm" action="refund_admin_edit.html">
<fieldset id="refundPreferences"><legend>Refund Preferences:</legend>
<%
Results refundPreferenceResults=DataLayerMgr.executeQuery("GET_USER_REFUND_PREFERENCES", inputForm);
if(refundPreferenceResults.next()){%>
		<div><label><%=StringUtils.prepareHTML(refundPreferenceResults.getValue("refundPreferenceLabel", String.class))%>:</label>
		<select name="refundPreferenceValue">
		  <option <%if(refundPreferenceResults.getValue("refundPreferenceValue", String.class).equalsIgnoreCase("Y")){ %>selected="selected" <%} %> value="Y">Yes</option>
		  <option <%if(refundPreferenceResults.getValue("refundPreferenceValue", String.class).equalsIgnoreCase("N")){ %>selected="selected" <%} %> value="N">No</option>
		</select>
		<input type="hidden" name="refundPreferenceId" value="<%=refundPreferenceResults.getValue("refundPreferenceId")%>"/>
		</div>
<%}%>
<div>
<input type="button" value="Save Changes" onClick="requestNotificationPreference();">
</div>
</fieldset>
</form>
<%}%>
<div class="spacer5"></div>
<form name="refundForm" id="refundForm" action="refund_admin.html">
<input type="hidden" name="actionType" value="search"/>
<div id="refundSearch">
	<span class="label">From Date:</span> 
	<input id="fromTs" type="text" data-format="%m/%d/%Y" data-toggle="calendar" data-validators="validate-date dateFormat:'mm/dd/yyyy'" size="10" name="fromTs" <%if (inputForm.get("fromTs")!=null){ %>value="<%=StringUtils.prepareHTML(inputForm.getStringSafely("fromTs",""))%>"<%}else{ %>value="<%=RefundUtils.dateOnlyFormat.format(twoDaysAgo) %>" <%} %> /> 
	&nbsp;<span class="label">To Date:</span> 
	<input id="toTs" type="text" data-format="%m/%d/%Y" data-toggle="calendar" data-validators="validate-date dateFormat:'mm/dd/yyyy'" size="10" name="toTs" <%if (inputForm.get("toTs")!=null){ %>value="<%=StringUtils.prepareHTML(inputForm.getStringSafely("toTs",""))%>"<%} %> /> 
	<button type="submit">Search</button>

<div class="spacer5"></div>

<table class="folio">
		<tbody>
			<tr class="headerRow">
			<th><a title="Sort Rows by Transaction Id" data-toggle="sort" data-sort-type="NUMBER">Transaction Id</a></th>
			<th><a title="Sort Rows by Device" data-toggle="sort" data-sort-type="STRING">Device</a></th>
			<th><a title="Sort Rows by Card Number" data-toggle="sort" data-sort-type="STRING">Card Number</a></th>
			<th>
			<a title="Sort Rows by Amount" data-toggle="sort" data-sort-type="STRING">Amount</a>
			</th>
			<th>
			<a title="Sort Rows by Refund Date" data-toggle="sort" data-sort-type="TIMESTAMP">Refund Date</a>
			</th>
			<th>
			<a title="Sort Rows by Issuer" data-toggle="sort" data-sort-type="STRING">Issuer</a>
			</th>
			<th>
			<a title="Sort Rows by Status" data-toggle="sort" data-sort-type="STRING">Settle Status</a>
			</th>
			<th>
			<a title="Sort Rows by Settle Date" data-toggle="sort" data-sort-type="TIMESTAMP">Settle Date</a>
			</th>
			<th>
			<a title="Sort Rows by Settle Date" data-toggle="sort" data-sort-type="TIMESTAMP">Refund Reason</a>
			</th>
			<th>
			<a title="Sort Rows by Settle Date" data-toggle="sort" data-sort-type="TIMESTAMP">Refund Comment</a>
			</th>
			<th><a title="Sort Rows by hour" data-toggle="sort" data-sort-type="STRING">Hours left before Auto Processing</a></th>
			<th>
			<a title="Sort Rows by Processed Flag" data-toggle="sort" data-sort-type="STRING">Process Status</a>
			</th>
			<th>
			<a>Actions</a>
			</th>
			</tr>
			</tbody>
			<tbody>
			<%
				int count=0;
				while(refundResults!=null&&refundResults.next()) {
					count++;
					int refundId=ConvertUtils.getInt(refundResults.get("refundId"));
					char processedFlag=ConvertUtils.getChar(refundResults.get("processedFlag"));
				    %>
				    <tr class="<%=(++i % 2) == 0 ? "even" : "odd" %>RowHover">
				    <td data-sort-value="<%=StringUtils.prepareScript(refundResults.getFormattedValue("transactionId")) %>"><%=StringUtils.prepareHTML(refundResults.getFormattedValue("transactionId")) %></td>
				    <td data-sort-value="<%=StringUtils.prepareScript(refundResults.getFormattedValue("deviceSerialCd")) %>"><%=StringUtils.prepareHTML(refundResults.getFormattedValue("deviceSerialCd")) %></td>
				    <td data-sort-value="<%=StringUtils.prepareScript(refundResults.getFormattedValue("cardNumber")) %>"><%=StringUtils.prepareHTML(refundResults.getFormattedValue("cardNumber")) %></td>
				    <td data-sort-value="<%=StringUtils.prepareScript(refundResults.getFormattedValue("totalAmount")) %>"><%=StringUtils.prepareHTML(refundResults.getFormattedValue("totalAmount")) %></td>
				    <td data-sort-value="<%=StringUtils.prepareScript(refundResults.getFormattedValue("transactionDate")) %>"><%=StringUtils.prepareHTML(refundResults.getFormattedValue("transactionDate")) %></td>
				    <td data-sort-value="<%=StringUtils.prepareScript(refundResults.getFormattedValue("issuer")) %>"><%=StringUtils.prepareHTML(refundResults.getFormattedValue("issuer")) %></td>
				    <td data-sort-value="<%=StringUtils.prepareScript(refundResults.getFormattedValue("transactionStatus")) %>"><%=StringUtils.prepareHTML(refundResults.getFormattedValue("transactionStatus")) %></td>
				    <td data-sort-value="<%=StringUtils.prepareScript(refundResults.getFormattedValue("settledDate")) %>"><%=StringUtils.prepareHTML(refundResults.getFormattedValue("settledDate")) %></td>
				    <td data-sort-value="<%=StringUtils.prepareScript(refundResults.getFormattedValue("refundReason")) %>"><%=StringUtils.prepareHTML(refundResults.getFormattedValue("refundReason")) %></td>
				    <td data-sort-value="<%=StringUtils.prepareScript(refundResults.getFormattedValue("refundComment")) %>"><%=StringUtils.prepareHTML(refundResults.getFormattedValue("refundComment")) %></td>
				    <td data-sort-value="<%=StringUtils.prepareScript(refundResults.getFormattedValue("leftHour")) %>"><%=StringUtils.prepareHTML(refundResults.getFormattedValue("leftHour")) %></td>
				    <td id="td_status_<%=refundId%>" data-sort-value="<%=StringUtils.prepareScript(refundResults.getFormattedValue("processedFlagDes")) %>"><%=StringUtils.prepareHTML(refundResults.getFormattedValue("processedFlagDes")) %></td>
				    <td id="td_actions_<%=refundId%>">
					<%if(processedFlag=='W'){ %>
						<input type="button" value="Approve" onClick="requestUpdateStatus(<%=refundId%>, 1);">    <input type="button" value="Cancel" onClick="requestUpdateStatus(<%=refundId%>, -1);">
					<%}else if(processedFlag=='C'){%>
						<input type="button" value="Approve" onClick="requestUpdateStatus(<%=refundId%>, 1);">
					<%}else if(processedFlag=='A'){%>
						<input type="button" value="Cancel" onClick="requestUpdateStatus(<%=refundId%>, -1);">
					<%}%>
					</td>
				    </tr>
				    <%
				}
				if(count==0 && actionType.equals("search")){
				%>
				<tr><td>No Refund Found</td></tr>
				<%} %>
		</tbody>
</table>
</div>
</form>
<%}else{%>
	<p class="not-authorized">User '<%=StringUtils.prepareHTML(user.getUserName()) %>' does not have permission to view this page. Please contact USA Technologies if you need access.</p>
<%}%>
<jsp:include page="include/footer.jsp"/>
