<%@page import="simple.servlet.RequestUtils"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.text.StringUtils,simple.servlet.RequestUtils,simple.bean.ConvertUtils"%>
<%@page import="simple.text.Censor"%>

<%
	
	UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
	boolean isCustomerService=false;
	boolean hasPrivilege=false;
	if(user.isInternal()&&user.hasPrivilege(ReportingPrivilege.PRIV_CUSTOMER_SERVICE)){
			hasPrivilege=true;
			isCustomerService=true;
	}
	Results msgResults = DataLayerMgr.executeQuery("GET_HOME_PAGE_MSG", null);
	Results infoMsgResults = DataLayerMgr.executeQuery("GET_HOME_PAGE_INFO_MSG", null);
	String msg ="";
	
	InputForm inputForm = RequestUtils.getInputForm(request);	
	if (msgResults.next()){
		msg = msgResults.getValue("message", String.class);
	}
	String infoMsg ="";
	if (infoMsgResults.next()){
		infoMsg = infoMsgResults.getValue("message", String.class);
	} 
		
%>
<jsp:include page="include/header.jsp"/>

<script type="text/javascript">
	
	function onPreview(){
		$('action').value = 'preview';
		new Request.HTML({ 
	   	url: "homepage_info_save.html",
	   	data: $("msgForm"), 
	   	method: 'post',
	   	update: $("msgUpdateResult"), 
	   	onComplete : function() {
	   		document.getElementById('previewArea').empty();
			if ($('msgType').value == "addMsg"){
				if ($('msgUpdated').innerHTML == "null") {
					$('inputAreaA').value = '';
				}else{
					$('inputAreaA').value = $('msgUpdated').innerHTML;
				}
				text = $('inputAreaA').value;
				var div = document.createElement('div');
				div.setAttribute('class', 'someClass');
				div.setAttribute('style', 'width: 600px; height:100%; border:thin; border-style:solid; border-color: #CCC;');
				div.innerHTML = $('inputAreaA').value;
				document.getElementById('previewArea').appendChild(div);
			}else{
				if ($('msgUpdated').innerHTML == "null") {
					$('inputAreaM').value = '';
				}else{
					$('inputAreaM').value = $('msgUpdated').innerHTML;
				}
				text = $('inputAreaM').value;
				var div = document.createElement('div');
				div.setAttribute('class', 'someClass');
				div.setAttribute('style', 'width: 600px; height:100%; border:thin; border-style:solid; border-color: #CCC;');
				div.innerHTML = $('inputAreaM').value;
				document.getElementById('previewArea').appendChild(div);
			}
	   	},
		evalScripts: true
		}).send();
		
	}
	
	function onSave(){
		$('action').value = 'save';
		new Request.HTML({ 
	   	url: "homepage_info_save.html",
	   	data: $("msgForm"), 
	   	method: 'post',
	   	update: $("msgUpdateResult"), 
	   	onComplete : function() {
	   		document.getElementById('previewArea').empty();
			if ($('msgType').value == "addMsg"){
				if ($('msgUpdated').innerHTML == "null") {
					$('inputAreaA').value = '';
				}else{
					$('inputAreaA').value = $('msgUpdated').innerHTML;
				}
				text = $('inputAreaA').value;
				var div = document.createElement('div');
				div.setAttribute('class', 'someClass');
				div.setAttribute('style', 'width: 600px; height:100%; border:thin; border-style:solid; border-color: #CCC;');
				div.innerHTML = $('inputAreaA').value;
				document.getElementById('previewArea').appendChild(div);
			}else{
				if ($('msgUpdated').innerHTML == "null") {
					$('inputAreaM').value = '';
				}else{
					$('inputAreaM').value = $('msgUpdated').innerHTML;
				}
				text = $('inputAreaM').value;
				var div = document.createElement('div');
				div.setAttribute('class', 'someClass');
				div.setAttribute('style', 'width: 600px; height:100%; border:thin; border-style:solid; border-color: #CCC;');
				div.innerHTML = $('inputAreaM').value;
				document.getElementById('previewArea').appendChild(div);
			}
	   	},
		evalScripts: true
		}).send();
	}
	
	function onMsgTypeChange(){
		if ($('msgType').value == "addMsg"){
			document.getElementById('previewArea').empty();
			$('inputAreaMain').set("style","display:none");	
			$('inputAreaAdd').set("style","display:table-row");	
		}else{
			document.getElementById('previewArea').empty();
			$('inputAreaAdd').set("style","display:none");	
			$('inputAreaMain').set("style","display:table-row");
		}
	}
</script>
<form name="msgForm" id="msgForm">
<input type="hidden" name="action" id="action" value=""/>
<input type="hidden" id="fragment" name="fragment" value="true"/> 
<%if(hasPrivilege){ %>
<span class="caption">Welcome Info</span>
<div class="spacer5"></div>
<div>
<div id="msgUpdateResult" name="msgUpdateResult"></div>
<table >
	<tr>
		<td>
			<select id="msgType" name="msgType" onchange="onMsgTypeChange();">
				<option id="addMsg" value="addMsg" selected="true">Additional homepage message</option>
				<option id="mainMsg" value="mainMsg">Main homepage message</option>
			</select>
		</td>
	</tr>
	<tr><td><span>Please enter or update text of homepage information message:</span></td></tr>
	<tr id="inputAreaMain" style="display:none">
		<td><textarea id="inputAreaM" name="inputAreaM" style="width: 600px; height:300px; border-color: #EB842D;"><%=msg%></textarea></td>
	</tr>
	<tr id="inputAreaAdd">
		<td><textarea id="inputAreaA" name="inputAreaA" style="width: 600px; height:300px; border-color: #EB842D;"><%=infoMsg%></textarea></td>
	</tr>
	<tr>
		<td>
			<input type="button" id="preview" value="Preview" style="font-weight: bold" onclick="onPreview();"/>
			<input type="button" id="save" value="Save" style="font-weight: bold" onclick="onSave();"/>
		</td>
	</tr>
	<tr><td><span>Message will look as shown below. Please preview and make corrections if needed.</span></td></tr>
	<tr><td><span>*Message text will be available on Homepage for all USALive users in a few minutes after saving.</span></td></tr>
	<tr>
		<td id="previewArea"></td>
	</tr>
</table>
</div>
<%}else{%>
	<p class="not-authorized">User '<%=StringUtils.prepareHTML(user.getUserName()) %>' does not have permission to view this page. Please contact USA Technologies if you need access.</p>
<%}%>
</form>
<jsp:include page="include/footer.jsp"/>
