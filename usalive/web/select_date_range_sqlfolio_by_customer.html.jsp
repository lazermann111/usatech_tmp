<%@page import="simple.servlet.InputForm"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@page import="simple.text.StringUtils,simple.servlet.RequestUtils,simple.results.Results"%>
<%
UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
Integer basicReportId=null;
Results result=null;
Results customerResult=null;
String reportName=null;
InputForm inputForm = RequestUtils.getInputForm(request);
basicReportId=inputForm.getInt("basicReportId", true,-1);
	result=DataLayerMgr.executeQuery("GET_REPORT_NAME", new Object[]{basicReportId});
	if(result.next()){
		reportName=result.getFormattedValue("reportName");
	}
	
	if(inputForm.get("actionType")!=null&&inputForm.get("actionType").equals("customerSearch")){
		customerResult=DataLayerMgr.executeQuery("GET_CUSTOMERS", inputForm);
	}
%>
<jsp:include page="include/header.jsp"/>
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "selection/report-selection-scripts.js") %>"></script>
<script type="text/javascript">
window.addEvent('domready', function() { 
	new Form.Validator.Inline.Mask(document.reportForm);
	
});
function searchCustomerName(){
	$('customerName').value=$('customerName').value.trim();
}  

function onUnselectAllCustomers(){
	$('customerIds').selectedIndex=-1;
}
</script>
<div class="sectionTitle"><caption><%if(!StringUtils.isBlank(reportName)){%><%=StringUtils.prepareHTML(reportName)%><%} %></caption></div>
<form onsubmit="searchCustomerName();" id="reportForm" method="post" name="reportForm">
<input type="hidden" name="actionType" value="customerSearch" />
<div class="message-header">
Customer Name:
<span title="Search customer name that contains: (input nothing for all customers)">
<input id="customerName" type="text" name="customerName">
</span>
<input type="submit" value="Search" > &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" id="unselectCustomers" value="Unselect All Customers" onclick="onUnselectAllCustomers();" />
</div>
</form>
<form id="reportForm2" method="post" name="reportForm2" action="run_sql_folio_report_async.i">
<input type="hidden" name="basicReportId" value="<%=basicReportId%>" />
<table class="selectBody" >
<tbody>
<tr>
<td>
<div class="selectSection">
<div class="sectionTitle">Customers</div>
<div class="sectionRow">
<select id="customerIds" style="width: 100%;" name="customerIds" size="15" multiple>
<%if(customerResult!=null){%>
	<%while(customerResult.next()){ %>
		<option value="<%=StringUtils.prepareCDATA(customerResult.getFormattedValue("customerId"))%>"><%=StringUtils.prepareHTML(customerResult.getFormattedValue("customerName"))%></option>
	<%} %>
<%} %>
</select>
</div>
</div>
</td>
</tr>
<tr>
<td style="padding:0px">
<jsp:include page="selection-date-range.jsp"/>
</td>
</tr>
<tr>
<td style="padding:0px; text-align: center; background: #D9E6FB; vertical-align: middle;">
			<label id="report-button-html">
<input type="radio" checked="checked" value="22" name="outputType">
<div class="output-type-label"></div>
(Html)
</label>
<label id="report-button-excel">
<input type="radio" value="27" name="outputType">
<div class="output-type-label"></div>
(Excel)
</label>
<label id="report-button-pdf">
<input type="radio" value="24" name="outputType">
<div class="output-type-label"></div>
(Pdf)
</label>
<label id="report-button-doc" >
<input type="radio" value="23" name="outputType">
<div class="output-type-label"></div>
(Word)
</label>
<label id="report-button-csv" >
<input type="radio" value="21" name="outputType">
<div class="output-type-label"></div>
(Csv)
</label>
</td>
</tr>
<tr>
<td style="padding:0px; text-align: center; background: #D9E6FB; vertical-align: middle;">
			<input type="submit" value="Run Report" />

</td>
</tr>
</tbody>
</table>
</form>
<jsp:include page="include/footer.jsp"/>