<%@page import="simple.servlet.RequestUtils,simple.results.Results"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.db.DataLayerMgr"%>
<%
InputForm inputForm = RequestUtils.getInputForm(request);
Results results =  DataLayerMgr.executeQuery("GET_RMA_SHIPPING_CARRIER", inputForm);
%>
<select id="carrierSelect" name="rmaShippingCarrierId" tabindex="1" onchange="onCarrierChange(this)" size="4" style="width: 200px;" ><%
	while(results.next()) {
	Object rmaShippingCarrierId = results.getValue("rmaShippingCarrierId");
	String carrier = results.getFormattedValue("carrier");
	String carrierAccountNumber = results.getFormattedValue("carrierAccountNumber");
	String optionValue=carrier+" "+carrierAccountNumber;
	%><option value="<%=rmaShippingCarrierId%>" data-carrier="<%=carrier%>" data-carrier-account="<%=carrierAccountNumber%>"><%=optionValue %></option>	    	
	<%}%>
</select>
