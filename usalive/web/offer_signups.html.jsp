<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.NotAuthorizedException"%>
<%@page import="simple.db.Call.Sorter"%>
<%@page import="simple.servlet.GenerateUtils"%>
<%@page import="java.util.concurrent.TimeUnit"%>
<%@page import="simple.bean.ConvertException"%>
<%@page import="java.util.Date"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.usalive.servlet.USALiveUtils"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.text.StringUtils"%>
<%
InputForm form = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
UsaliveUser user = (UsaliveUser) RequestUtils.getUser(request);
if(!user.isInternal())
    throw new NotAuthorizedException("You are not authorized to perform this function.");
Results offerResults = DataLayerMgr.executeQuery("GET_OFFERS", form);
Sorter sorter = DataLayerMgr.getGlobalDataLayer().findCall("GET_OFFER_SIGNUPS").getSorter();
String startDateText = form.getString("startDate", false);
Date startDate;
try {
	startDate = ConvertUtils.convert(Date.class, startDateText);
} catch(ConvertException e) {
	RequestUtils.getOrCreateMessagesSource(request).addMessage("warn", "usalive-backoffice-trans-invalid-date", "Invalid {0} date ''{1}''", "start", startDateText);
	startDate = null;
}
if(startDate == null) {
    startDate = new Date(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(30));
    form.setAttribute("startDate", startDate);
}
	
String endDateText = form.getString("endDate", false);
Date endDate;
try {
	endDate = ConvertUtils.convert(Date.class, endDateText);
} catch(ConvertException e) {
    RequestUtils.getOrCreateMessagesSource(request).addMessage("warn", "usalive-backoffice-trans-invalid-date", "Invalid {0} date ''{1}''", "end", endDateText);
    endDate = null;
}
if(endDate == null) {
	endDate = new Date(System.currentTimeMillis());
    form.setAttribute("endDate", endDate);
}
long offerId = form.getLong("offerId", false, -1);
String optedIn = form.getStringSafely("optedIn", "");
String privGranted = form.getStringSafely("privGranted", "");
String dataFormat = form.getStringSafely("dataFormat", "");
String[] sortBy = ConvertUtils.toUniqueArray(form.getStringArray("sortBy", false), String.class, "-signupDate", "sortableCompanyName", "sortableEmail");
form.setAttribute("sortBy", sortBy);
int pageIndex = form.getInt("pageIndex", false, 1);
int pageSize = form.getInt("pageSize", false, PaginationUtil.DEFAULT_PAGE_SIZE);
Results results = null;
if (offerId > -1) {
	results = sorter.executeSorted(form, pageSize, pageIndex, sortBy);
	results.setFormat("signupDate", "DATE:MM/dd/yyyy HH:mm:ss");
}
if ("CSV".equalsIgnoreCase(dataFormat) && results != null) {
	out.clear();
	response.setContentType("application/force-download");
	response.setHeader("Content-Transfer-Encoding", "binary"); 
	response.setHeader("Content-Disposition", new StringBuilder("attachment;filename=\"offer_signups.csv.txt\"").toString());
	StringBuilder sb = new StringBuilder();
	String[] headers = {"Date", "Offer", "Company", "First Name", "Last Name", "Email", "Phone", "Address", "City", "State", "Postal", "Source", "Opted In", "Privilege", "Admin User", "Current Customer", "Purchase Quantity", "iOS Quantity", "Android Quantity", "All In One Quantity", "Billing Address", "Billing City", "Billing State", "Billing Postal", "Purchase Type", "Details"};         	    	
	out.print(StringUtils.join(headers, ",", "\""));
	out.flush();
	while (results.next()) {
		sb.setLength(0);    	    		
		sb.append("\""); sb.append(results.getFormattedValue("signupDate")); sb.append("\",");
		sb.append("\""); sb.append(results.getFormattedValue("offerName")); sb.append("\",");
		sb.append("\""); sb.append(results.getFormattedValue("companyName")); sb.append("\",");
		sb.append("\""); sb.append(results.getFormattedValue("firstName")); sb.append("\",");
		sb.append("\""); sb.append(results.getFormattedValue("lastName")); sb.append("\",");
		sb.append("\""); sb.append(results.getFormattedValue("email")); sb.append("\",");
		sb.append("\""); sb.append(results.getFormattedValue("telephone")); sb.append("\",");
		sb.append("\""); sb.append(results.getFormattedValue("address")); sb.append("\",");
		sb.append("\""); sb.append(results.getFormattedValue("city")); sb.append("\",");
		sb.append("\""); sb.append(results.getFormattedValue("state")); sb.append("\",");
		sb.append("\""); sb.append(results.getFormattedValue("postal")); sb.append("\",");
		sb.append("\""); sb.append(results.getFormattedValue("signupSource")); sb.append("\",");
		sb.append("\""); sb.append(results.getFormattedValue("signupInd")); sb.append("\",");
		sb.append("\""); sb.append(results.getFormattedValue("privGranted")); sb.append("\",");
		sb.append("\""); sb.append(results.getFormattedValue("adminUserInd")); sb.append("\",");
		sb.append("\""); sb.append(results.getFormattedValue("currentCustomerInd")); sb.append("\",");
		sb.append("\""); sb.append(results.getFormattedValue("purchase")); sb.append("\",");
		sb.append("\""); sb.append(results.getFormattedValue("iOSQuantity")); sb.append("\",");
		sb.append("\""); sb.append(results.getFormattedValue("androidQuantity")); sb.append("\",");
		sb.append("\""); sb.append(results.getFormattedValue("allInOneQuantity")); sb.append("\",");
		sb.append("\""); sb.append(results.getFormattedValue("billingAddress")); sb.append("\",");
		sb.append("\""); sb.append(results.getFormattedValue("billingCity")); sb.append("\",");
		sb.append("\""); sb.append(results.getFormattedValue("billingState")); sb.append("\",");
		sb.append("\""); sb.append(results.getFormattedValue("billingPostal")); sb.append("\",");
		sb.append("\""); sb.append(results.getFormattedValue("purchaseType")); sb.append("\",");
		sb.append("\""); sb.append(results.getFormattedValue("details")); sb.append("\"");
		out.print("\r\n");
		out.print(sb.toString().replaceAll("\r|\n", ""));
		out.flush();
	};
	out.close();
} else {
%>
<jsp:include page="include/header.jsp"/>

<div class="caption">Offer Signups</div>
<div class="spacer5"></div>

<form method="get" action="offer_signups.html" data-toggle="validate">
	<%=USALiveUtils.getHiddenInputs(request)%>
	<div class="offer-signups">
	<span class="label">Start Date</span> 
	<input type="text" id="startDate" size="8" maxlength="10" name="startDate" data-validators="required validate-date dateFormat:'mm/dd/yyyy'" data-label="Start Date" value="<%=ConvertUtils.formatObject(startDate, "DATE:MM/dd/yyyy")%>" data-toggle="calendar" data-format="%m/%d/%Y"/> 
	&nbsp;<span class="label">End Date</span> 
	<input type="text" id="endDate" size="8" maxlength="10" name="endDate" data-validators="required validate-date dateFormat:'mm/dd/yyyy'" data-label="End Date" value="<%=ConvertUtils.formatObject(endDate, "DATE:MM/dd/yyyy") %>"  data-toggle="calendar" data-format="%m/%d/%Y"/> 
	<span class="label">Offer</span>
	<select name="offerId">
		<option value="0">-- All --</option>
		<%while (offerResults.next()) {%>
		<option value="<%=offerResults.getValue("offerId", long.class)%>"<%if (offerId == offerResults.getValue("offerId", long.class)) {%> selected="selected"<%}%>><%=StringUtils.prepareHTML(offerResults.getFormattedValue("offerName"))%></option>
		<%}%>
	</select>
	<span class="label">Opted In</span>
	<select name="optedIn">
		<option value="A">-- All --</option>
		<option value="N"<%if ("N".equalsIgnoreCase(optedIn)) {%> selected="selected"<%}%>>N</option>
		<option value="Y"<%if ("Y".equalsIgnoreCase(optedIn)) {%> selected="selected"<%}%>>Y</option>
	</select>
	<span class="label">Privilege</span>
	<select name="privGranted">
		<option value="A">-- All --</option>
		<option value="N"<%if ("N".equalsIgnoreCase(privGranted)) {%> selected="selected"<%}%>>N</option>
		<option value="Y"<%if ("Y".equalsIgnoreCase(privGranted)) {%> selected="selected"<%}%>>Y</option>
	</select>
	<span class="label">Format</span>
	<select name="dataFormat">
    	<option value="HTML"<%if ("HTML".equalsIgnoreCase(dataFormat)) out.write (" selected=\"selected\""); %>>HTML</option>
    	<option value="CSV"<%if ("CSV".equalsIgnoreCase(dataFormat)) out.write (" selected=\"selected\""); %>>CSV</option>
    </select>
	<button type="submit">Submit</button>
<div class="spacer5"></div>
<%if (results != null) {%>
<table class="sortable-table">
	<tr class="gridHeader"><%
	GenerateUtils.writeSortedTableHeader(pageContext, "signupDate", "Date", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortableOfferName", "Offer", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortableCompanyName", "Company", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortableFirstName", "First Name", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortableLastName", "Last Name", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortableEmail", "Email", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortableTelephone", "Phone", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortableAddress", "Address", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortableCity", "City", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortableState", "State", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortablePostal", "Postal", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortableSignupSource", "Source", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortableSignupInd", "Opted In", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortablePrivGranted", "Privilege", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortableAdminUserInd", "Admin User", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortableCurrentCustomerInd", "Current Customer", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortablePurchase", "Purchase Quantity", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortableIOSQuantity", "iOS Quantity", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortableAndroidQuantity", "Android Quantity", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortableAllInOneQuantity", "All In One Quantity", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortableBillingAddress", "Billing Address", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortableBillingCity", "Billing City", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortableBillingState", "Billing State", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortableBillingPostal", "Billing Postal", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortablePurchaseType", "Purchase Type", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortableDetails", "Details", sortBy);
	%>
	</tr><%	
while(results.next()) {%>
	<tr>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("signupDate"))%></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("offerName"))%></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("companyName"))%></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("firstName"))%></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("lastName"))%></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("email"))%></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("telephone"))%></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("address"))%></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("city"))%></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("state"))%></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("postal"))%></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("signupSource"))%></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("signupInd"))%></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("privGranted"))%></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("adminUserInd"))%></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("currentCustomerInd"))%></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("purchase"))%></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("iOSQuantity"))%></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("androidQuantity"))%></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("AllInOneQuantity"))%></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("billingAddress"))%></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("billingCity"))%></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("billingState"))%></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("billingPostal"))%></td>
        <td><%=StringUtils.prepareHTML(results.getFormattedValue("purchaseType"))%></td>
        <td><%=StringUtils.prepareHTML(results.getFormattedValue("details"))%></td>
	</tr><%
}
int rows = results.getRowCount();
int count;
if(pageIndex == 1 && rows < PaginationUtil.DEFAULT_PAGE_SIZE) { 
    count = rows;
} else { 
    count = sorter.executeCount(form);
}%>
<tr><td colspan="<%=results.getColumnCount()%>" class="pagination-container">
<jsp:include page="include/pagination2.jsp">
    <jsp:param name="pageIndex" value="<%=pageIndex %>"/>
    <jsp:param name="pageSize" value="<%=pageSize %>"/>
    <jsp:param name="count" value="<%=count %>"/>
</jsp:include>
</td></tr>
</table>
<%} %>
</div>
</form>
<jsp:include page="include/footer.jsp"/>
<%}%>