<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="com.usatech.usalive.servlet.USALiveUtils"%>
<%@page import="simple.servlet.GenerateUtils"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.text.StringUtils"%>
<% UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
  if(!user.hasPrivilege(ReportingPrivilege.PRIV_CREATE_BANK_ACCT)) {
%>
<p class="not-authorized">User '<%=StringUtils.prepareHTML(user.getUserName())%>' does not have permission to view this page. Please contact USA Technologies if you need access.</p>
<% } else {
  InputForm form = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
  form.setAttribute(SimpleServlet.ATTRIBUTE_USER, user);
  Results results = DataLayerMgr.executeQuery("GET_CUSTOMER_BANK_ACCOUNTS", form);
  int bankCount = 0;
%>
<jsp:include page="include/header.jsp" />
<div class="toggle-heading">
	<span class="caption">Bank Accounts</span>
</div>
<div class="spacer5"></div>
<%=USALiveUtils.getHiddenInputs(request)%>
<% if(results != null) { %>
<table class="fields" style="width:60em;">
<% while(results.next()) { 
  bankCount++;
  String country = "USA";
  String countryCd = results.getValue("BANK_COUNTRY_CD", String.class);
  if (countryCd != null && countryCd.equals("CA")) {
    country = "Canada"; 
  } %>
  <tr class="tableHeader">
    <th>Bank ID <%=results.getValue("CUSTOMER_BANK_ID", Long.class)%></th>
  </tr>
  <tr class="tableSpacer"><td></td></tr>
  <tr>
    <td>
      <table class="fields">
        <tr class="tableSpacer"><td></td></tr>
        <tr class="sideButtonPane">
          <th align="left" style="width:25em;">Banking Information</th>
          <td align="left">This must be a U.S. or Canadian Domestic Bank</td>
        </tr>
        <tr class="tableSpacer"><td></td></tr>
        <tr class="tableDataShade">
          <th>The Name of the Bank</th>
          <td><span class="readonlybackgroud readonlyField" style="width:25em;"><%=results.getFormattedValue("BANK_NAME", "HTML")%></span></td>
        </tr>
        <tr class="tableDataShade">
          <th>The Bank's Address</th>
          <td><span class="readonlybackgroud readonlyField" style="width:25em;"><%=results.getFormattedValue("BANK_ADDRESS", "HTML")%></span>
            <table id="bank_postalLookup">
              <tr>
                <td><span class="readonlybackgroud readonlyField" style="width:11em;"><%=results.getFormattedValue("BANK_CITY", "HTML")%></span>,</td>
                <td>&nbsp;<span class="readonlybackgroud readonlyField" style="width:2em;"><%=results.getFormattedValue("BANK_STATE", "HTML")%></span></td>
                <td><span class="readonlybackgroud readonlyField" style="width:4em;"><%=results.getFormattedValue("BANK_ZIP", "HTML")%></span></td>
                <td><span class="readonlybackgroud readonlyField" style="width:4.5em;"><%=country%></span></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr class="tableDataShade">
          <th>The Title on the Bank Account<br />(Must read exactly as listed on bank statement)</th>
          <td><span class="readonlybackgroud readonlyField" style="width:25em;"><%=results.getFormattedValue("ACCOUNT_TITLE", "HTML")%></span></td>
        </tr>
        <tr class="tableDataShade">
          <th>The Account Type</th>
          <% String accountType = results.getValue("ACCOUNT_TYPE", String.class); %>
          <td><input type="radio" value="S" readonly="readonly" disabled="disabled" <%if(accountType != null && accountType.equals("S")) {%> checked="checked" <%}%> />Savings&#160;&#160; <input type="radio" readonly="readonly" value="C" disabled="disabled" <%if(accountType != null && accountType.equals("C")) {%> checked="checked" <%}%> />Checking</td>
        </tr>
        <tr class="tableSpacer"><td></td></tr>
        <tr class="sideButtonPane">
          <th align="left">EFT Information</th>
          <td align="left">Obtain this information directly from the bank</td>
        </tr>
        <tr class="tableSpacer"><td></td></tr>
        <tr class="tableDataShade">
          <th>The Bank ABA Number (Routing Number)</th>
          <td><span class="readonlybackgroud readonlyField" style="width:25em;"><%=results.getFormattedValue("BANK_ROUTING_NBR", "HTML")%></span></td>
        </tr>
        <tr class="tableDataShade">
          <th>The Bank Account Number</th>
          <td><span class="readonlybackgroud readonlyField" style="width:25em;"><%=results.getFormattedValue("BANK_ACCT_NBR", "HTML")%></span></td>
        </tr>
<% if (results.getValue("SWIFT_CODE") != null) { %>
        <tr class="tableDataShade" id="swiftCodeField">
          <th>The Bank SWIFT Code</th>
          <td><span class="readonlybackgroud readonlyField" style="width:25em;"><%=results.getFormattedValue("SWIFT_CODE", "HTML")%></span></td>
        </tr>
<% } %>
        <tr class="tableSpacer"><td></td></tr>
        <tr class="sideButtonPane">
          <th align="left">Bank Contact</th>
          <td align="left">Person at the bank who we can contact to verify Banking Information</td>
        </tr>
        <tr class="tableSpacer"><td></td></tr>
        <tr class="tableDataShade">
          <th>Contact's Name</th>
          <td><span class="readonlybackgroud readonlyField" style="width:25em;"><%=results.getFormattedValue("CONTACT_NAME", "HTML")%></span></td>
        </tr>
        <tr class="tableDataShade">
          <th>Contact's Title</th>
          <td><span class="readonlybackgroud readonlyField" style="width:25em;"><%=results.getFormattedValue("CONTACT_TITLE", "HTML")%></span></td>
        </tr>
        <tr class="tableDataShade">
          <th>Contact's Phone</th>
          <td><span class="readonlybackgroud readonlyField" style="width:25em;"><%=results.getFormattedValue("CONTACT_PHONE", "HTML")%></span></td>
        </tr>
        <tr class="tableDataShade">
          <th>Contact's Fax</th>
          <td><span class="readonlybackgroud readonlyField" style="width:25em;"><%=results.getFormattedValue("CONTACT_FAX", "HTML")%></span></td>
        </tr>
        <tr class="tableSpacer"><td></td></tr>
        <tr class="sideButtonPane">
          <th align="left">Banking Billing Information</th>
          <td align="left">This must be a U.S. or Canadian Domestic Bank</td>
        </tr>
        <tr class="tableSpacer"><td></td></tr>
        <tr class="tableDataShade">
          <th>The Bank's Billing Address</th>
          <td><span class="readonlybackgroud readonlyField" style="width:25em;"><%=results.getFormattedValue("bankBillingAddress1", "HTML")%></span>
            <table id="bank_postalLookup">
              <tr>
                <td><span class="readonlybackgroud readonlyField" style="width:11em;"><%=results.getFormattedValue("bankBillingCity", "HTML")%></span>,</td>
                <td>&nbsp;<span class="readonlybackgroud readonlyField" style="width:2em;"><%=results.getFormattedValue("bankBillingState", "HTML")%></span></td>
                <td><span class="readonlybackgroud readonlyField" style="width:4em;"><%=results.getFormattedValue("bankBillingZip", "HTML")%></span></td>
                <td><span class="readonlybackgroud readonlyField" style="width:4.5em;"><%=results.getFormattedValue("bankBillingCountryCd", "HTML")%></span></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr class="tableDataShade">
          <th>Customer Tax ID</th>
          <td><span class="readonlybackgroud readonlyField" style="width:25em;"><%=results.getFormattedValue("bankTaxId", "HTML")%></span></td>
        </tr>
        <tr class="tableSpacer"><td></td></tr>
        <tr class="sideButtonPane">
          <th align="left">Standing</th>
          <td align="left"></td>
        </tr>
        <tr class="tableDataShade">
          <th>Status</th>
          <% String statusCd = results.getValue("STATUS", String.class);
             boolean isApproved = statusCd != null && statusCd.equalsIgnoreCase("A"); %>
          <td><span style="color:<%=isApproved ? "green" : "orange"%>; font-weight:900;"><%=isApproved ? "APPROVED" : "PENDING APPROVAL"%></span>
          <% if (!isApproved) { %>
            <br/><br/>
            <span style="font-weight:bold;">
            <a href="/eft_auth_instructions.i?bankAcctId=<%=results.getValue("CUSTOMER_BANK_ID", Long.class)%><%=(RequestUtils.getSessionTokenIfPresent(request) != null ? ("&session-token=" + RequestUtils.getSessionTokenIfPresent(request)) : "")%>">Print EFT Authorization Form</a> - 
            You must sign and fax this form back to USA Technologies before payments can be made to this Bank Account for Device transactions.</span>
          <% } %>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr><td>&nbsp;</td></tr>
<% } %>
</table>
<% } %>
<% if (bankCount == 0) { %>
No bank accounts are configued. <a href="/new_bank_acct.i">Setup a new bank account</a>.
<% } %>
<jsp:include page="include/footer.jsp" />
<% } %>

