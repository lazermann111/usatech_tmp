<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.GenerateUtils"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="simple.db.Call.Sorter"%>
<%@page import="simple.io.Log"%>
<%@page import="com.usatech.ec2.EC2ClientUtils"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="simple.text.StringUtils"%>
<%
	Log log = Log.getLog();
	UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
	InputForm inputForm = RequestUtils.getInputForm(request);

	boolean hasPri=false;
	if(user.isInternal()||user.hasPrivilege(ReportingPrivilege.PRIV_MANAGE_PREPAID_CONSUMERS)){
		hasPri=true;
	}
	String actionType = RequestUtils.getAttribute(request, "actionType", String.class, false);
	Results consumerCardResults=null;
	String cardNum=inputForm.getString("cardNum", false);
	long cardId= inputForm.getLong("cardId", false, -1);
	String firstName=inputForm.getString("firstName", false);
	String lastName= inputForm.getString("lastName", false);
	String email= inputForm.getString("email", false);
	String mobile= inputForm.getString("mobile", false);
	String[] sortBy=null;
	int pageIndex = inputForm.getInt("pageIndex", false, 1);
	int pageSize = inputForm.getInt("pageSize", false, PaginationUtil.DEFAULT_PAGE_SIZE);
	Sorter sorter=null;
	if(actionType!=null && actionType.equals("search")){

		if (!StringHelper.isBlank(cardNum)){
			cardNum=cardNum.trim();
			if(cardNum.length()==4){
				inputForm.setAttribute("last4", "%"+cardNum);
			}else{
				long globalAccountId=-1;
				try{
					globalAccountId= EC2ClientUtils.getGlobalAccountId(cardNum);
				}catch(Throwable e) {
					log.info("cardNum globalAccountId not exists");
				}
				inputForm.setAttribute("globalAccountId", globalAccountId);
			}
		}
		sorter = DataLayerMgr.getGlobalDataLayer().findCall("GET_CONSUMER_CARDS").getSorter();
		sortBy = ConvertUtils.toUniqueArray(inputForm.getStringArray("sortBy", false), String.class);
		inputForm.setAttribute("sortBy", sortBy);
		consumerCardResults = sorter.executeSorted(inputForm, pageSize, pageIndex, sortBy);
		consumerCardResults.setFormat("cardBalance", "CURRENCY");
	}
	String baseUrl=RequestUtils.getBaseUrl(request, false);
%>
<jsp:include page="include/header.jsp"/>
<script type="text/javascript">
function onConsumerSearch(){
	$("cardNum").value=$("cardNum").value.trim();
	$("cardId").value=$("cardId").value.trim();
	$("firstName").value=$("firstName").value.trim();
	$("lastName").value=$("lastName").value.trim();
	$("email").value=$("email").value.trim();
	$("mobile").value=$("mobile").value.trim();
}

</script>
<%if(hasPri){ %>
<div class="toggle-heading">
	<span class="caption">Consumers</span>
</div>
<form name="consumerForm" id="consumerForm" action="consumer_search.html" onSubmit="return onConsumerSearch();">
<div class="selectSection">
<div class="sectionTitle">Consumer and Card Search</div>
<br/>
<input type="hidden" name="actionType" value="search">
<div id="message"></div>
<div class="sectionRow">
<table class="input-rows">
<tbody>
<tr>
<td width="30%">
Card Number (Full 19/15/16 digit or last 4 digit):
</td>
<td>
<input id="cardNum" type="text" title="Empty means all card number"  name="cardNum" <%if(!StringHelper.isBlank(cardNum)){ %> value="<%=StringUtils.prepareCDATA(cardNum) %>" <%}else{ %> value="" <%} %> autocomplete="off">
</td>
</tr>
<tr>
<td>
Card Id:
</td>
<td>
<input id="cardId" type="text" title="Empty means all card id" name="cardId"  <%if(cardId>0){ %> value="<%=cardId%>" <%}else{ %> value="" <%} %>>
</td>
</tr>
<tr>
<td>
First Name:
</td>
<td>
<input id="firstName" type="text" title="Empty means all first name" name="firstName"  <%if(!StringHelper.isBlank(firstName)){ %> value="<%=StringUtils.prepareCDATA(firstName) %>" <%}else{ %> value="" <%} %>>
</td>
</tr>
<tr>
<td>
Last Name:
</td>
<td>
<input id="lastName" type="text" title="Empty means all last name" name="lastName" <%if(!StringHelper.isBlank(lastName)){ %> value="<%=StringUtils.prepareCDATA(lastName) %>" <%}else{ %> value="" <%} %>>
</td>
</tr>
<tr>
<td>
Primary Email:
</td>
<td>
<input id="email" type="text" title="Empty means all email" name="email" <%if(!StringHelper.isBlank(email)){ %> value="<%=StringUtils.prepareCDATA(email) %>" <%}else{ %> value="" <%} %>>
</td>
</tr>
<tr>
<td>
Mobile Phone Number:
</td>
<td>
<input id="mobile" type="text" title="Empty means all cell phone number" name="mobile" <%if(!StringHelper.isBlank(mobile)){ %> value="<%=StringUtils.prepareCDATA(mobile)%>" <%}else{ %> value="" <%} %>>
</td>
</tr>
<tr>
<td align="center" colspan="2">
<input id="search" type="submit" value="Search">
</td>
</tr>
</tbody>
</table>
</div>
</div>
<%if(actionType!=null && actionType.equals("search")){ %>
<div class="instruct center">Search Results</div>
<div class="payor-transactions">
<div class="spacer5"></div>
<table class="sortable-table">
	<tr class="gridHeader"><%
	GenerateUtils.writeSortedTableHeader(pageContext, "signIntoPrepaid", "Sign Into MORE", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "cardTypeDesc", "Consumer Card Type", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "firstName", "First Name", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "lastName", "Last Name", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "email", "Email", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "mobile", "Mobile", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "cardNum", "Card Number", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "cardId", "Card Id", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "cardBalance", "Card Balance", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "active", "Status", sortBy);
	%>
	</tr>
	<% while(consumerCardResults.next()) {%>
	<tr>
		<td><% if (ConvertUtils.getBoolean(consumerCardResults.getValue("moreEnabledInd"), false)) { %><input id="loginAs" type="button" value="Sign Into MORE" onclick="window.open('<%=baseUrl%>/prepaid_signin_as.html?consumerId=<%=consumerCardResults.getValue("consumerId", long.class)%>', 'prepaid')"><% } %></td>
		<td><%=StringUtils.prepareHTML(consumerCardResults.getFormattedValue("cardTypeDesc"))%></td>
		<td><a href="consumer.html?consumerId=<%=consumerCardResults.getValue("consumerId", long.class)%>"><%=StringUtils.prepareHTML(consumerCardResults.getFormattedValue("firstName"))%></a></td>
		<td><a href="consumer.html?consumerId=<%=consumerCardResults.getValue("consumerId", long.class)%>"><%=StringUtils.prepareHTML(consumerCardResults.getFormattedValue("lastName"))%></a></td>
		<td><%=StringUtils.prepareHTML(consumerCardResults.getFormattedValue("email"))%></td>
		<td><%=StringUtils.prepareHTML(consumerCardResults.getFormattedValue("mobile"))%></td>
		<td><a href="consumer_acct.html?consumerAcctId=<%=consumerCardResults.getValue("consumerAcctId", long.class)%>"><%=StringUtils.prepareHTML(consumerCardResults.getFormattedValue("cardNum"))%></a></td>
		<td><%=StringUtils.prepareHTML(consumerCardResults.getFormattedValue("cardId"))%></td>
		<td><%=StringUtils.prepareHTML(consumerCardResults.getFormattedValue("cardBalance"))%></td>
		<td><%if (consumerCardResults.getFormattedValue("active").equals("Y")){%> Active <%}else{ %> Inactive<%} %></td>
	</tr><%
	}
int rows = consumerCardResults.getRowCount();
int count;
if(pageIndex == 1 && rows < PaginationUtil.DEFAULT_PAGE_SIZE) {
	count = rows;
} else {
	count = sorter.executeCount(inputForm);
}%>
<tr><td colspan="10" class="pagination-container">
<jsp:include page="include/pagination2.jsp">
	<jsp:param name="pageIndex" value="<%=pageIndex %>"/>
	<jsp:param name="pageSize" value="<%=pageSize %>"/>
	<jsp:param name="count" value="<%=count %>"/>
</jsp:include>
</td></tr>
</table>
</div>
<%} %>
</form>
<%}else{%>
	<p class="not-authorized">User '<%=StringUtils.prepareHTML(user.getUserName()) %>' does not have permission to view this page. Please contact USA Technologies if you need access.</p>
<%}%>
<jsp:include page="include/footer.jsp"/>
