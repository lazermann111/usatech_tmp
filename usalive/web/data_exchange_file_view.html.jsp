<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.Reader"%>
<%@page import="com.usatech.usalive.hybrid.HybridServlet"%>
<%@page import="simple.io.resource.ResourceMode"%>
<%@page import="simple.io.resource.Resource"%>
<%@page import="com.usatech.usalive.report.AsyncReportEngine"%>
<%@page import="simple.db.NotEnoughRowsException"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="simple.servlet.NotAuthorizedException"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
if(!user.hasPrivilege(ReportingPrivilege.PRIV_CUSTOMER_SERVICE) && !user.hasPrivilege(ReportingPrivilege.PRIV_SYSTEM_ADMIN))
    throw new NotAuthorizedException("You are not authorized to perform this function.");
InputForm form = RequestUtils.getInputForm(request);
AsyncReportEngine arEngine = (AsyncReportEngine) form.getAttribute(HybridServlet.ATTRIBUTE_ASYNCH_REPORT_ENGINE);
long dataExchangeFileId = form.getLong("fileId", true, 0L);
Map<String,Object> params = new HashMap<String,Object>();
params.put("fileId", dataExchangeFileId);
try {
    DataLayerMgr.selectInto("GET_DATA_EXCHANGE_FILE_INFO", params);
} catch(NotEnoughRowsException e) {
    throw new ServletException("Invalid file Id: " + dataExchangeFileId);
}
String fileKey = ConvertUtils.getString(params.get("fileKey"), true);
long fileSize = ConvertUtils.getLong(params.get("fileSize"));
char direction  = ConvertUtils.getChar(params.get("direction"));
Resource resource = arEngine.getResource(fileKey, ResourceMode.READ);
try {
%>
<jsp:include page="include/header.jsp"/>
<table class="detail-info">
<tr class="header"><th colspan="4" >Data Exchange File Details</th>
<tr><th>File Name:</th><td><%=StringUtils.prepareHTML(ConvertUtils.getString(params.get("fileName"), true)) %></td><th>File Size:</th><td><%=fileSize%></td></tr>
<tr><th>Customer ID:</th><td><%=StringUtils.prepareHTML(ConvertUtils.getString(params.get("customerId"), false)) %></td>
<th>Customer Name:</th><td><%=StringUtils.prepareHTML(ConvertUtils.getString(params.get("customerName"), false))  %></td></tr>
<tr><th>Foreign Entity ID:</th><td><%=StringUtils.prepareHTML(ConvertUtils.getString(params.get("foreignEntityId"), false)) %></td>
<th>File Type:</th><td><%=StringUtils.prepareHTML(ConvertUtils.getString(params.get("itemTypeDesc"), false))  %></td></tr>
<tr><th>Content Type:</th><td><%=StringUtils.prepareHTML(resource.getContentType()) %></td><th>Direction:</th><td><%switch(direction) { case 'I': %>Incoming<% break; case 'O': %>Outgoing<% break; default: %>Unknown (<%=StringUtils.prepareHTMLBlank(String.valueOf(direction)) %>)<%} %></td></tr>
<tr><th>Created:</th><td><%=ConvertUtils.formatObject(params.get("createdTs"), "DATE:MM/dd/yyyy HH:mm:ss") %></td><th>Processing Complete:</th><td><%=ConvertUtils.formatObject(params.get("processCompleteTs"), "DATE:MM/dd/yyyy HH:mm:ss") %></td></tr>
<tr><th>Processing Status:</th><td><%=StringUtils.prepareHTML(ConvertUtils.getString(params.get("processStatus"), false)) %></td>
<th>Processing Details:</th><td><%=StringUtils.prepareHTML(ConvertUtils.getString(params.get("processDetail"), false))  %></td></tr>
<tr class="spacer"><th colspan="4"></th></tr>
<tr class="header"><th colspan="4">Content:</th></tr>
<tr><td colspan="4"><div class="detail-scroll-box"><%try(Reader in = new InputStreamReader(resource.getInputStream())) {
	StringUtils.appendHTMLBlank(out, in);
}%></div>
</td></tr>
</table><%
} finally {
	resource.release();
}%>
<jsp:include page="include/footer.jsp"/>