<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="simple.io.Log"%>
<%@page import="java.lang.Exception"%>
<%@page import="simple.text.StringUtils"%>
<script type="text/javascript">
<%
	Log log = Log.getLog();
	UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
	boolean hasPri=false;
	if(user.hasPrivilege(ReportingPrivilege.PRIV_ADMIN_REFUND)){
		hasPri=true;
	}
	InputForm inputForm = RequestUtils.getInputForm(request);
	if(hasPri && "POST".equalsIgnoreCase(request.getMethod())){
		int refundPreferenceId=-1;
		try{
				refundPreferenceId=inputForm.getInt("refundPreferenceId", false, -1);
				if(refundPreferenceId>0){
					DataLayerMgr.executeUpdate("UPSERT_REFUND_USER_PREFERENCES", inputForm, true);
					%>
					updatePreferenceMessage(1);
					<%
				}else{//edit status
					int requestedStatus=inputForm.getInt("status", true, 0);
					if(requestedStatus==1){
						int updateCount=DataLayerMgr.executeUpdate("APPROVE_REFUND", inputForm, true);
						if(updateCount==1){
							%>
							updateStatus('success', '',<%=StringUtils.prepareScript(inputForm.getStringSafely("refundId", ""))%>, 1);
							<%
						}else{
							%>
							updateStatus('error', 'Failed to approve refund. Please retry.',<%=StringUtils.prepareScript(inputForm.getStringSafely("refundId", ""))%>, 1);
							<%
						}
					}else if (requestedStatus==-1){
						int updateCount=DataLayerMgr.executeUpdate("CANCEL_REFUND", inputForm, true);
						if(updateCount==1){
							%>
							updateStatus('success', '',<%=StringUtils.prepareScript(inputForm.getStringSafely("refundId", ""))%>, -1);
							<%
						}else{
							%>
							updateStatus('error', 'Failed to cancel refund. Please retry.',<%=StringUtils.prepareScript(inputForm.getStringSafely("refundId", ""))%>, -1);
							<%
						}
					}else{
						%>
						updateStatus('error', 'Unknown action. Please contact customer service for assistance.',<%=StringUtils.prepareScript(inputForm.getStringSafely("refundId", ""))%>, 0);
						<%
					}
				}
		}catch(Exception e) {
			if(refundPreferenceId>0){
				%>
				updatePreferenceMessage(-1);
				<%
			}else{
			%>
			updateStatus('error', 'Editing refund status failed. Please retry.',<%=StringUtils.prepareScript(inputForm.getStringSafely("refundId", ""))%>, <%=StringUtils.prepareScript(inputForm.getStringSafely("status",""))%>);<%
			}
			log.error("Failed to edit refund status. Please try again.", e);
		}
	};
%>
</script>
