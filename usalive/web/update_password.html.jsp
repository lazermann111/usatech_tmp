<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.NotLoggedOnException"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="java.util.regex.Matcher"%>
<%@page import="com.usatech.usalive.servlet.LoginStep"%>
<%@page import="simple.servlet.LdapUtils"%>
<%@page import="com.usatech.report.ReportingUser.ResourceType"%>
<%@page import="simple.lang.InvalidStringValueException"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="java.security.NoSuchAlgorithmException"%>
<%@page import="com.usatech.usalive.servlet.CheckPasswordStep"%>
<%@page import="com.usatech.report.ReportingUser.ActionType"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="simple.servlet.steps.LoginFailureException"%>
<%!
protected static final CheckPasswordStep checkPassword;
static {
	CheckPasswordStep cps;
	try {
		cps = new CheckPasswordStep();
	} catch(NoSuchAlgorithmException e) {
		cps = null;
	}
	checkPassword = cps;
}
%>
<%
if("POST".equalsIgnoreCase(request.getMethod())) {
UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
Long editUserId = RequestUtils.getAttribute(request, "editUserId", Long.class, false);
InputForm form = RequestUtils.getInputForm(request);
if(editUserId != null) {
	if(user == null)
    	throw new NotLoggedOnException();
    user.checkPermission(ResourceType.USER, ActionType.EDIT, editUserId);
    try {
        checkPassword.checkPassword(form);
    } catch(ServletException e) {
    	if(e.getCause() instanceof InvalidStringValueException) {
	    	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "edit-password-invalid-new-password", "{0}", e.getCause().getMessage());
	    	RequestUtils.redirectWithCarryOver(request, response, "edit_password.html");
	        return;
    	}
    	throw e;
    }
    DataLayerMgr.executeCall("UPDATE_USER_PASSWORD", form, true);
    RequestUtils.getOrCreateMessagesSource(request).addMessage("success", "user-update-password");
} else {
	String username;
	String loginName;
	if(user == null) {
		username = RequestUtils.getAttribute(request, "editUsername", String.class, true);
		Matcher m = LoginStep.DEFAULT_INTERNAL_USERNAME_PATTERN.matcher(username);
        if(m.matches()) {
        	loginName = m.group(1);
        } else {
        	String remoteIp = request.getRemoteAddr();
        	if(username.contains("@") || remoteIp == null || !LoginStep.DEFAULT_INTERNAL_HOST_PATTERN.matcher(remoteIp).matches()) {
        		RequestUtils.redirectWithCarryOver(request, response, "reset_password_prompt.i");
        	    return;
        	}
        	loginName = username;
            username = loginName + "@usatech.com";
        }       
	} else {
		username = user.getUserName();
		if(!user.isInternal()) {
			RequestUtils.redirectWithCarryOver(request, response, "reset_password_prompt.i");
            return;
		}
		Matcher m = LoginStep.DEFAULT_INTERNAL_USERNAME_PATTERN.matcher(username);
		if(!m.matches())
			throw new ServletException("User is internal but username '" + username + "' does not match expected pattern");
		loginName = m.group(1);
	}
	String password = RequestUtils.getAttribute(request, "password", String.class, true);
    String newPassword = RequestUtils.getAttribute(request, "editPassword", String.class, true);
	String confirmPassword = RequestUtils.getAttribute(request, "editConfirm", String.class, true);
	if(!newPassword.equals(confirmPassword)) {
		RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "edit-password-not-confirmed-password", "The password does not match the confirmation. Please re-type both.");
        RequestUtils.redirectWithCarryOver(request, response, "edit_password.html");
        return;
    } 
	try {
		LdapUtils.authenticateAndChangePassword(loginName, password, newPassword);
	} catch(LoginFailureException e) {
		switch(e.getType()) {
			case LoginFailureException.INVALID_CREDENTIALS:
			case LoginFailureException.INVALID_USERNAME:
			case LoginFailureException.INVALID_PASSWORD:
	            RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "login-failed-prompt", "Your username or password is invalid; please re-enter your login information.");
				break;
			case LoginFailureException.PASSWORD_EXPIRED:
				RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "login-password-expired", "Your password has expired; please contact NetOps to reset your account.");
                break;
            default:
            	RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "system-error", "An error occurred on the server. Please try again.");
                break;
		}
		RequestUtils.redirectWithCarryOver(request, response, "edit_password.html");
        return;
	} catch(ServletException e) {
		RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "system-error", "An error occurred on the server. Please try again.");
        RequestUtils.redirectWithCarryOver(request, response, "edit_password.html");
        return;
	}
	RequestUtils.getOrCreateMessagesSource(request).addMessage("success", "user-update-password", username);
}
}
String forwardTo = RequestUtils.getAttribute(request, "forward-to", String.class, false);
if(StringUtils.isBlank(forwardTo))
	forwardTo = "home.i";
RequestUtils.redirectWithCarryOver(request, response, forwardTo);
%>