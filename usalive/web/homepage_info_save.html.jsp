<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.text.StringUtils,simple.servlet.RequestUtils,simple.bean.ConvertUtils"%>
<%@page import="simple.text.Censor"%>

<%
	
	UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
	boolean isCustomerService=false;
	boolean hasPrivilege=false;
	if(user.isInternal()&&user.hasPrivilege(ReportingPrivilege.PRIV_CUSTOMER_SERVICE)){
			hasPrivilege=true;
			isCustomerService=true;
	}
	
	InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);	
	String saveResult ="";
	boolean isSuccess;
	String msgType = inputForm.getStringSafely("msgType", "");
	String validMsg = " ";
	String fieldUpdated = "";
	try
	{
		if ((inputForm.getStringSafely("action", " ")).equals("save")){
			if (msgType.equals("addMsg")){
				validMsg = StringUtils.getValidSafeHTML("inputAreaA", (inputForm.getStringSafely("inputAreaA", "")).replace("< ","<"), 4000, true);
				validMsg = Censor.sanitizeText(validMsg);
				inputForm.setAttribute("inputAreaA", validMsg);
				Results results = DataLayerMgr.executeQuery("UPDATE_HOMEPAGE_INFO_MESSAGE", inputForm);
				saveResult = "New message text was successfully saved.";
				isSuccess = true;
			}else if (msgType.equals("mainMsg")){
				validMsg = StringUtils.getValidSafeHTML("inputAreaM", (inputForm.getStringSafely("inputAreaM", "")).replace("< ","<"), 4000, true);
				validMsg = Censor.sanitizeText(validMsg);
				inputForm.setAttribute("inputAreaM", validMsg);
				Results results = DataLayerMgr.executeQuery("UPDATE_HOMEPAGE_MESSAGE", inputForm);
				saveResult = "New message text was successfully saved.";
				isSuccess = true;
			}else{
				saveResult = "Save operation failed. Please try again.";
				isSuccess = false;
			}
		}else{
			if (msgType.equals("addMsg")){
				validMsg = StringUtils.getValidSafeHTML("inputAreaA", (inputForm.getStringSafely("inputAreaA", "")).replace("< ","<"), 4000, true);
				validMsg = Censor.sanitizeText(validMsg);
				isSuccess = true;
			}else if (msgType.equals("mainMsg")){
				validMsg = StringUtils.getValidSafeHTML("inputAreaM", (inputForm.getStringSafely("inputAreaM", "")).replace("< ","<"), 4000, true);
				validMsg = Censor.sanitizeText(validMsg);
				isSuccess = true;
			}else{
				saveResult = "Save operation failed. Please try again.";
				isSuccess = false;
			}
		}
	}catch (Exception e){
		saveResult = "Operation failed. Please try again.";
		isSuccess = false;	
	}
%>
<div id="msgUpdateResultText" name="msgUpdateResultText" style="color:<%if (isSuccess) {%>GREEN<%}else{%>RED<%}%>;"><%=saveResult%></div>
<div id="msgUpdated" name="msgUpdated" style="display:none"><%=validMsg%></div>

	
