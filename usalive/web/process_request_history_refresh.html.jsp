<%@page import="simple.text.StringUtils"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.RequestUtils"%>
<%
InputForm form = RequestUtils.getInputForm(request);
try (Results results = DataLayerMgr.executeQuery("GET_PROCESS_STATUS_UPDATES", form)) {
	if(!results.isGroupEnding(0)) {
		%><script type="text/javascript">var e;<%
		StringBuilder pendingIds = null;
		OUTER: while(results.next()) {
			long processRequestId = results.getValue("processRequestId", long.class);
		    String processStatusCd = results.getValue("processStatusCd", String.class);
		    String processStatusText = results.getValue("processStatus", String.class);
            switch(processStatusCd) {
            	case "I":
	                //update response
	                %>e=$("response_<%=processRequestId%>"); if(e) { e.setHTML("<%=StringUtils.prepareScript(results.getFormattedValue("response"))%>"); } else { window.location.reload(); } <%
	        	case "P": case "R":
	                if(pendingIds == null)
	                    pendingIds = new StringBuilder();
	                else
	                    pendingIds.append(',');
	                pendingIds.append(processRequestId);
	                break; 
	        	default:
                    %>window.location.reload();<%
                    pendingIds = null;
                    break OUTER;       
	        }
	        %>e=$("status_<%=processRequestId%>"); if(e) { e.setHTML("<%=StringUtils.prepareScript(processStatusText)%>"); } <%
		}
		if(pendingIds != null) { %>
window.setTimeout(function() {
    new Request.HTML({url: "process_request_history_refresh.html?processRequestIds=<%=pendingIds%>", 
    	data: {fragment: true}, 
    	evalScripts: true, noCache: true}).get();
}, 5000);<%
        }%></script><%
	}
}
%>