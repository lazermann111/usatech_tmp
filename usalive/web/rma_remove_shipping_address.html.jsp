<%@page import="simple.text.StringUtils,simple.servlet.RequestUtils,simple.results.Results"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.db.DataLayerMgr"%>
<script type="text/javascript">
<%
Long rmaShippingAddrId = RequestUtils.getAttribute(request, "rmaShippingAddrId", Long.class, true);
try{
	DataLayerMgr.executeCall("REMOVE_RMA_SHIPPING_ADDRESS", new Object[]{rmaShippingAddrId,RequestUtils.getAttribute(request, "simple.servlet.ServletUser.userId", int.class, true)}, true);
	%>
		$("<%="data-rmaShippingAddrId-"+rmaShippingAddrId%>").dispose();
		$("removeShippingAddressMsg").set("html","Address removed successfully");
		$("removeShippingAddressMsg").set("class","status-info-success");
		$("removeShippingAddress").set("disabled","true");
		cleanAddress();
	<% 
}catch(Exception e){
	%>
	$("removeShippingAddressMsg").set("html","Failed to remove the address. Please contact customer service for assistance.");
	$("removeShippingAddressMsg").set("class","status-info-failure");
	<% 
}
%>
</script>

