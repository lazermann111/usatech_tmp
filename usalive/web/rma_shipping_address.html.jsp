<%@page import="simple.text.StringUtils,simple.servlet.RequestUtils,simple.results.Results"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.db.DataLayerMgr"%>
<%
InputForm inputForm = RequestUtils.getInputForm(request);
String countryCd="US";
String contactName="";
String address="";
String city="";
String state="";
String postalCd="";
String email="";
String phone="";
int rmaShippingAddrId=inputForm.getInt("rmaShippingAddrId", false, -1);
if(rmaShippingAddrId>0){
	Results results =  DataLayerMgr.executeQuery("GET_USER_RMA_SHIPPING_ADDRESS_BY_ID", inputForm);
	if(results.next()){
		countryCd=StringUtils.prepareCDATA(results.getFormattedValue("countryCd"));
		contactName=StringUtils.prepareCDATA(results.getFormattedValue("contactName"));
		address=StringUtils.prepareCDATA(results.getFormattedValue("address")) ;
		postalCd=StringUtils.prepareCDATA(results.getFormattedValue("postalCd"));
		city=StringUtils.prepareCDATA(results.getFormattedValue("city"));
		state=StringUtils.prepareCDATA(results.getFormattedValue("state"));
		email=StringUtils.prepareCDATA(results.getFormattedValue("email"));
		phone=StringUtils.prepareCDATA(results.getFormattedValue("phone"));
	}
}
%>
<table class="input-rows">
<tbody>
<%if(rmaShippingAddrId>0){ %>
	<tr><td><input id="removeShippingAddress" type="button" value="Remove This Address" onclick="onRemoveShippingAddress(<%=rmaShippingAddrId%>)"></td>
	<td></td></tr>
<%} %>
<tr>
<td>Contact Name:<span class="required">*</span></td>
<td colspan="2"><input type="text" id="contactName" name="contactName" value="<%=StringUtils.prepareScript(contactName)%>" required="required"  class="maxLength:100" size="50">
</td>
</tr>
<tr>
<td>Country:</td>
<td colspan="2">
<select data-validators="required" name="countryCd" id="countryCd" >
<option <%if(countryCd.equals("US")){%>selected="selected"<%} %> onselect="App.updateValidation($(this).getParent().form.postalCd, 'required validate-postalcode-lookup-us', 'Fixed.PostalUS'); onSelectCountry('US');" value="US">USA</option>
<option <%if(countryCd.equals("CA")){%>selected="selected"<%} %>onselect="App.updateValidation($(this).getParent().form.postalCd, 'required validate-postalcode-lookup-ca', 'Fixed.PostalCA'); onSelectCountry('CA');" value="CA">Canada</option>
</select>
</td>
</tr>
<tr>
<td>Street Address:<span class="required">*</span></td>
<td colspan="2"><input type="text" id="address" name="address" value="<%=StringUtils.prepareScript(address)%>" required="required"  class="maxLength:255" size="50" >
</td>
</tr>
<tr>
<td>Postal Code:<span class="required">*</span></td>
<td colspan="2">
<input type="text" id="postalCd" name="postalCd"  value="<%=StringUtils.prepareScript(postalCd) %>" required="required"  data-validators="required validate-postalcode-lookup-us" edit-mask="Fixed.PostalUS" size="50">
</td>
</tr>
<tr>
<td>City:<span class="required">*</span></td>
<td colspan="2">
<input type="text" id="city" name="city" value="<%=StringUtils.prepareScript(city)%>" required="required"  size="50">
</td>
</tr>
<tr>
<td>State:<span class="required">*</span></td>
<td colspan="2">
<input type="text" id="state" name="state" value="<%=StringUtils.prepareScript(state)%>" required="required"  size="50">
</td>
</tr>
<tr>
<td>Email:<span class="required">*</span></td>
<td colspan="2"><input type="text" id="email" name="email" value="<%=StringUtils.prepareScript(email)%>"  required="required" data-validators="validate-regex:'^(?:\u005cs*\u005cw+[\u005cw\u005c.-]*\u005c@\u005cw+[\u005cw\u005c.-]*?\u005c.[A-Za-z]{2,6}\u005cs*(?:[,;]|$))+$'" maxlength="255" size="50"><div id="checkEmailMsg"></div>
</td>
</tr>
<tr>
<td>Phone:<span class="required">(* Required for Canada or Non-continental US Address)</span></td>
<td colspan="2"><input type="text" id="phone" name="phone" value="<%=StringUtils.prepareScript(phone) %>"  data-validators="validate-digits" edit-mask="Fixed.PhoneUs" size="50">
</td>
</tr>
<tr>
<td colspan="3">Save As New Address<input type="checkbox" id="saveNewAddress" name="saveNewAddress" value="Y" onClick="onSaveNewAddress();">
Name:<span class="required">(* Required for saving new address)</span><input type="text" id="shippingAddrName" name="shippingAddrName" class="maxLength:100" value=""  size="50">
</td>
</tr>
</tbody>
</table>
