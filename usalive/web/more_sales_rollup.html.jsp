<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@page import="simple.text.StringUtils,simple.servlet.RequestUtils,simple.results.Results"%>
<%
UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
boolean hasPri=false;
Integer folioId=null;
Results result=null;
Integer outputType=null;
Results campaignResults=null;
String reportName=null;
if(user.hasPrivilege(ReportingPrivilege.PRIV_MANAGE_PREPAID_CONSUMERS)){
	hasPri=true;
	folioId=RequestUtils.getAttribute(request, "folioId", Integer.class, true);
	result=DataLayerMgr.executeQuery("GET_FOLIO_NAME", new Object[]{folioId});
	if(result.next()){
		reportName=result.getFormattedValue("folioName");
	}
	outputType=RequestUtils.getAttribute(request, "outputType", Integer.class, false);
	campaignResults = DataLayerMgr.executeQuery("GET_MORE_CAMPAIGN_SUMMARY", RequestUtils.getInputForm(request));
}
%>
<jsp:include page="include/header.jsp"/>
<%if(hasPri){ %>
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "selection/report-selection-scripts.js") %>"></script>
<script type="text/javascript">
function onUnselectAllCampaigns(){
	$('params.campaignIds').selectedIndex=-1;
}

function onSubmit(){
	$("params.beginDate").value=$("reportForm").beginDate.value;
	$("params.endDate").value=$("reportForm").endDate.value;
}
</script>
<form id="reportForm" name="reportForm" action="run_report_async.i" onSubmit="onSubmit();" >
<input type="hidden" name="unframed" value="false" />
<input type="hidden" name="promptForParams" value="false" />
<input type="hidden" name="profileId" value="<%=user.getUserId() %>" />
<input type="hidden" id ="params.beginDate" name="params.beginDate" value="" />
<input type="hidden" id ="params.endDate" name="params.endDate" value="" />
<input type="hidden" name="folioId" value="<%=folioId %>" />
			<%if(outputType!=null){ %>
				<input type="hidden" name="outputType" value="<%=outputType%>" />
			<%} %>
			
<table class="selectBody">
<caption><%if(!StringUtils.isBlank(reportName)){%><%=StringUtils.prepareHTML(reportName)%><%} %></caption>
<tbody>
<tr>
<td>
        <div class="selectSection">
            <div class="sectionTitle">Campaigns</div>
                <div class="sectionRow">
                    <select  size="10" name="params.campaignIds" id="params.campaignIds"><%
        while(campaignResults.next()) {
            long resultsCampaignId = campaignResults.getValue("campaignId", long.class);
        %><option value="<%=StringUtils.prepareCDATA(campaignResults.getFormattedValue("campaignName"))%>" >
        <%=StringUtils.prepareCDATA(campaignResults.getFormattedValue("campaignName"))%> (<%=StringUtils.prepareCDATA(campaignResults.getFormattedValue("promoCode"))%>)
        </option><%
        }%>
        </select>
    </div></div>
    <input type="button" id="unselectCampaigns" value="Unselect All Campaigns" onclick="onUnselectAllCampaigns();" />
</td>
<td width="90%">
<jsp:include page="selection-date-range.jsp"/>
<span class="shownOnWebsiteNote">+</span>
<span class="shownOnWebsiteDesc">If no campaign is selected, the report will report on all transactions</span>
<div style="text-align: center; clear: both;">
			<input type="submit" value="Run Report" />
		</div>
</td>
</tr>
<tr>
</tr>
</tbody>
</table>
</form>
<%}else{%>
	<p class="not-authorized">User '<%=StringUtils.prepareHTML(user.getUserName()) %>' does not have permission to view this page. Please contact USA Technologies if you need access.</p>
<%}%>
<jsp:include page="include/footer.jsp"/>