<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.db.DataLayerMgr"%>
<%
		InputForm inputForm = RequestUtils.getInputForm(request);
		Results result=DataLayerMgr.executeQuery("GET_CAMPAIGN_IND", inputForm);
 		if(result.next()){
 			inputForm.set("isForAllCards", result.get("isForAllCards"));
 		}
		
		Results customerCamResults = DataLayerMgr.executeQuery("GET_CUSTOMER_CAMPAIGNS", inputForm);
		Results regionCamResults = DataLayerMgr.executeQuery("GET_REGION_CAMPAIGNS", inputForm);
		Results terminalCamResults = DataLayerMgr.executeQuery("GET_TERMINAL_CAMPAIGNS", inputForm);
		int i = 0;
		%>
		<table class="folio">
		<tbody>
			<tr class="headerRow">
			<th><a title="Sort Rows by Type" data-toggle="sort" data-sort-type="STRING">Type</a></th>
			<th>
			<input id="selectAllCheckbox" type="checkbox" onclick="onSelectAllCheckbox();">
			</th>
			<th>
			<a title="Sort Rows by Customer Name" data-toggle="sort" data-sort-type="STRING">Customer Name</a>
			</th>
			<th>
			<a title="Sort Rows by Region Name" data-toggle="sort" data-sort-type="STRING">Region Name</a>
			</th>
			<th>
			<a title="Sort Rows by Device Serial Number" data-toggle="sort" data-sort-type="STRING">Device Serial Number</a>
			</th>
			<th>
			<a title="Sort Rows by location" data-toggle="sort" data-sort-type="STRING">Location</a>
			</th>
			<th>
			<a title="Sort Rows by Asset Nbr" data-toggle="sort" data-sort-type="STRING">Asset Nbr</a>
			</th>
			<th>
			<a title="Sort Rows by Assigned Campaign" data-toggle="sort" data-sort-type="STRING">All Assigned Campaigns</a>
			</th>
			</tr>
			</tbody>
			<tbody>
			<%
				while(customerCamResults.next()) {
				    %>
				    <tr class="<%=(++i % 2) == 0 ? "even" : "odd" %>RowHover">
				    <td data-sort-value="Customer"><div class="assignCustomer">Customer</div></td>
				    <td align="center"><input type="checkbox" name="assignCustomerId" value="<%=StringUtils.prepareCDATA(customerCamResults.getFormattedValue("customerId")) %>" <%if(customerCamResults.get("campaignId")!=null) {%>checked="checked" campaignId="<%=customerCamResults.getValue("campaignId", Long.class)%>" <%}%>></td>
				    <td data-sort-value="<%=StringUtils.prepareCDATA(customerCamResults.getFormattedValue("customerName")) %>"><%=StringUtils.prepareHTML(customerCamResults.getFormattedValue("customerName")) %></td>
				    <td data-sort-value=""></td>
				    <td data-sort-value=""></td>
				    <td data-sort-value=""></td>
				    <td data-sort-value=""></td>
				    <td data-sort-value="<%=StringUtils.prepareCDATA(customerCamResults.getFormattedValue("allCampaigns")) %>"><%=StringUtils.prepareHTML(customerCamResults.getFormattedValue("allCampaigns")) %></td>
				    </tr>
				    <%
				} 
				while(regionCamResults.next()) {
					String seperator="";
					if(!regionCamResults.getFormattedValue("allCampaigns").equals("")){
						seperator=",";
					}
				    %>
				    <tr class="<%=(++i % 2) == 0 ? "even" : "odd" %>RowHover">
				    <td data-sort-value="Region"><div class="assignRegion">Region</div></td>
				    <td align="center"><input type="checkbox" name="assignRegionIds" value="<%=StringUtils.prepareCDATA(regionCamResults.getFormattedValue("regionId")) %>" <%if(regionCamResults.get("campaignId")!=null) {%>checked="checked" campaignId="<%=regionCamResults.getValue("campaignId", Long.class)%>" <%}%>></td>
				    <td data-sort-value="<%=StringUtils.prepareCDATA(regionCamResults.getFormattedValue("customerName")) %>"><%=StringUtils.prepareHTML(regionCamResults.getFormattedValue("customerName")) %></td>
				    <td data-sort-value="<%=StringUtils.prepareCDATA(regionCamResults.getFormattedValue("regionName")) %>"><%=StringUtils.prepareHTML(regionCamResults.getFormattedValue("regionName")) %></td>
				    <td data-sort-value=""></td>
				    <td data-sort-value=""></td>
				    <td data-sort-value=""></td>
				    <td data-sort-value="<%=StringUtils.prepareCDATA(regionCamResults.getFormattedValue("allCampaigns")) %>"><%=StringUtils.prepareHTML(regionCamResults.getFormattedValue("allCampaigns"))%><%if(regionCamResults.get("campaignId")==null&&regionCamResults.get("implied")!=null) {%><%=seperator%><span class="impliedCampaign">* <%=StringUtils.prepareHTML(regionCamResults.getValue("implied", String.class))%></span><%}%></td>
				    </tr>
				    <%
				}
				while(terminalCamResults.next()) {
					String seperator="";
					if(!terminalCamResults.getFormattedValue("allCampaigns").equals("")){
						seperator=",";
					}
				    %>
				    <tr class="<%=(++i % 2) == 0 ? "even" : "odd" %>RowHover">
				    <td data-sort-value="Device"><div class="assignDevice">Device</div></td>
				    <td align="center"><input type="checkbox" name="assignTerminalIds" value="<%=StringUtils.prepareCDATA(terminalCamResults.getFormattedValue("terminalId")) %>" <%if(terminalCamResults.get("campaignId")!=null) {%>checked="checked" campaignId="<%=terminalCamResults.get("campaignId")%>" <%}%>></td>
				    <td data-sort-value="<%=StringUtils.prepareCDATA(terminalCamResults.getFormattedValue("customerName")) %>"><%=StringUtils.prepareHTML(terminalCamResults.getFormattedValue("customerName")) %></td>
				    <td data-sort-value="<%=StringUtils.prepareCDATA(terminalCamResults.getFormattedValue("regionName")) %>"><%=StringUtils.prepareHTML(terminalCamResults.getFormattedValue("regionName")) %></td>
				    <td data-sort-value="<%=StringUtils.prepareCDATA(terminalCamResults.getFormattedValue("deviceSerialCd")) %>"><%=StringUtils.prepareHTML(terminalCamResults.getFormattedValue("deviceSerialCd")) %></td>
				    <td data-sort-value="<%=StringUtils.prepareCDATA(terminalCamResults.getFormattedValue("location")) %>"><%=StringUtils.prepareHTML(terminalCamResults.getFormattedValue("location")) %></td>
				    <td data-sort-value="<%=StringUtils.prepareCDATA(terminalCamResults.getFormattedValue("assetNbr")) %>"><%=StringUtils.prepareHTML(terminalCamResults.getFormattedValue("assetNbr")) %></td>
				    <td data-sort-value="<%=StringUtils.prepareCDATA(terminalCamResults.getFormattedValue("allCampaigns")) %>"><%=StringUtils.prepareHTML(terminalCamResults.getFormattedValue("allCampaigns")) %><%if(terminalCamResults.get("campaignId")==null&&terminalCamResults.get("implied")!=null) {%><%=seperator%><span class="impliedCampaign">* <%=StringUtils.prepareHTML(terminalCamResults.getValue("implied", String.class))%></span><%}%>
				    &#160;<a href="javascript: showWhichDialog(<%=terminalCamResults.getValue("terminalId", Long.class)%>);" title="Find out which campaign will be applied at the date and time you enter">Which will apply?</a>
				    </td>
				    </tr>
				    <%
				}
				%>
		</tbody>
		</table>
