<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.Date"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="com.usatech.layers.common.util.CampaignUtils"%>
<%@page import="com.usatech.usalive.web.CampaignUsaliveUtils"%>
<%@page import="simple.servlet.RequestUtils"%>

<%
	InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
	String action = RequestUtils.getAttribute(request, "action", String.class, true);
	String notification = "";
	if (action.equals("add")){
		notification = (String)((DataLayerMgr.executeCall("ADD_CAMPAIGN_PRODUCT", inputForm, true))[1]);
		if (notification == null) notification = "";
	}
	if (action.equals("remove")){
		DataLayerMgr.executeCall("DELETE_CAMPAIGN_PRODUCT", inputForm, true);
	}
	Results results = DataLayerMgr.executeQuery("GET_CAMPAIGN_PRODUCTS", inputForm);
	String productCode;
	String productName;
	String productId;
	
%>

	<div id="productList" name="productList">
		<div id="notification" name="notification" style="color:red"><%=notification%></div>
		<select size="16" name="productCodes" id="productCodes" onchange="onCampaignProductSelect();" style="width: 100%; height:150px;">
		<%
		while(results.next()) {
			productId = results.getValue("rowNum", String.class);
			productCode = results.getValue("productCode", String.class);
			productName = results.getValue("productName", String.class);
		 	%>
			<option value="" id="product_<%=productId%>" ><%=productCode%>=<%=productName%></option>
			<%
		}
		%>
	</select>
	</div>


