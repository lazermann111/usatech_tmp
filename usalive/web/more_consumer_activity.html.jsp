<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@page import="simple.text.StringUtils,simple.servlet.RequestUtils,simple.results.Results"%>
<%
UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
boolean hasPri=false;
Integer basicReportId=null;
Results result=null;
Integer outputType=null;
Results regionResults=null;
String reportName=null;
if(!user.isInternal()){
	hasPri=true;
	basicReportId=RequestUtils.getAttribute(request, "basicReportId", Integer.class, true);
	result=DataLayerMgr.executeQuery("GET_REPORT_NAME", new Object[]{basicReportId});
	if(result.next()){
		reportName=result.getFormattedValue("reportName");
	}
	outputType=RequestUtils.getAttribute(request, "outputType", Integer.class, false);
	regionResults = DataLayerMgr.executeQuery("GET_REGION_DETAILS", RequestUtils.getInputForm(request));
}
%>
<jsp:include page="include/header.jsp"/>
<%if(hasPri){ %>
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "selection/report-selection-scripts.js") %>"></script>
<script type="text/javascript">
function onUnselectAllRegions(){
	$('regionsIds').selectedIndex=-1;
}
</script>
<form id="reportForm" name="reportForm" action="run_sql_folio_report_async.i">
<input type="hidden" name="basicReportId" value="<%=basicReportId%>" />
			<%if(outputType!=null){ %>
				<input type="hidden" name="outputType" value="<%=outputType%>" />
			<%} %>
			
<table class="selectBody">
<caption><%if(!StringUtils.isBlank(reportName)){%><%=StringUtils.prepareHTML(reportName)%><%} %></caption>
<tbody>
<tr>
<% if(!user.isInternal()){ %>
<td>
        <div class="selectSection">
            <div class="sectionTitle">Regions</div>
                <div class="sectionRow">
                    <select multiple size="15" name="regionIds" id="regionsIds"><%
        while(regionResults.next()) {
            long resultsRegionId = regionResults.getValue("regionId", long.class);
        %><option value="<%=resultsRegionId%>" >
        <%=StringUtils.prepareCDATA(regionResults.getFormattedValue("regionName"))%>
        </option><%
        }%>
        </select>
    </div></div>
    <% if(!user.isInternal()){ %><input type="button" id="unselectRegions" value="Unselect All Regions" onclick="onUnselectAllRegions();" />
			<%} %>
</td>
<%} %>
<td width="90%">
<jsp:include page="selection-date-range.jsp"/>
<span class="shownOnWebsiteNote">+</span>
<span class="shownOnWebsiteDesc">If no region is selected, the report will report on all regions</span>
<div style="text-align: center; clear: both;">
			<input type="submit" value="Run Report" />
		</div>
</td>
</tr>
<tr>
</tr>
</tbody>
</table>
</form>
<%}else{%>
	<p class="not-authorized">User '<%=StringUtils.prepareHTML(user.getUserName()) %>' does not have permission to view this page. Please contact USA Technologies if you need access.</p>
<%}%>
<jsp:include page="include/footer.jsp"/>