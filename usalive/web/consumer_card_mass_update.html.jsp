<%@page import="java.util.Collections"%>
<%@page import="java.util.stream.Collectors"%>
<%@page import="java.util.AbstractMap"%>
<%@page import="java.util.stream.Stream"%>
<%@page import="simple.servlet.RecordRequestFilter.CallInputs"%>
<%@page import="com.usatech.usalive.servlet.USALiveRecordRequestFilter"%>
<%@page import="com.usatech.usalive.servlet.USALiveUtils"%>
<%@page import="java.io.PushbackInputStream"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Arrays"%>
<%@page import="simple.db.helpers.OracleHelper"%>
<%@page import="java.sql.SQLException"%>
<%@page import="simple.text.MessageFormat"%>
<%@page import="simple.db.NotEnoughRowsException"%>
<%@page import="java.util.HashMap"%>
<%@page import="simple.servlet.NotAuthorizedException"%>
<%@page import="simple.bean.ConvertException"%>
<%@page import="java.math.RoundingMode"%>
<%@page import="java.util.Map"%>
<%@page import="simple.util.sheet.SheetUtils.RowValuesIterator"%>
<%@page import="simple.util.sheet.SheetUtils"%>
<%@page import="simple.servlet.InputFile"%>
<%@page import="java.io.InputStream"%>
<%@page import="com.usatech.layers.common.util.WebHelper"%>
<%@page import="simple.xml.sax.MessagesInputSource"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.usatech.usalive.web.PrepaidUtils"%>
<%@page import="com.usatech.report.ReportingUser.ActionType"%>
<%@page import="com.usatech.report.ReportingUser.ResourceType"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.io.Log"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%@page import="simple.servlet.ResponseUtils"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.Date"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="simple.text.StringUtils"%>
<%
	Log log = Log.getLog();
	UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
	InputForm form = RequestUtils.getInputForm(request);
	
	String[] BALANCE_UPDATE_HEADERS = {"CARD_ID", "AMOUNT", "REASON"};
	String[] ANB_HEADERS = {"CARD_ID", "ALLOW_NEGATIVE_BALANCE"};
	String[] ACTIVATE_HEADERS = {"CARD_ID", "FIRST_NAME", "LAST_NAME", "EMAIL", "CARDHOLDER_ID", "REGION_NAME", "DEPARTMENT_NAME", "PERIODIC_LIMIT"};
	
	boolean hasPri = false;
	if (user.isInternal() || user.hasPrivilege(ReportingPrivilege.PRIV_MANAGE_PREPAID_CONSUMERS)) {
		hasPri=true;
	} else {
		return;
	}
	
	String actionType = RequestUtils.getAttribute(request, "actionType", String.class, false);
	if (!StringUtils.isBlank(actionType)) {
		Object updateFile = form.getAttribute("updateFile");
		if (updateFile == null) 
			throw new ServletException("The file to upload was not specified in the request");
		
		String increaseType = RequestUtils.getAttribute(request, "increaseMassUpdate", String.class, false);
		String decreaseType = RequestUtils.getAttribute(request, "decreaseMassUpdate", String.class, false);
		if (actionType.equals("balanceUpdate")) {
			if (StringUtils.isBlank(increaseType) && StringUtils.isBlank(decreaseType)) {
				throw new ServletException("The balance update type was not specified in the request");
			}
		}

		InputStream inputStream;
		String path;
		if (updateFile instanceof InputFile) {
			InputFile inputFile = (InputFile)updateFile;
			inputStream = inputFile.getInputStream();
			path = inputFile.getPath();
		} else {
			inputStream = ConvertUtils.convert(InputStream.class, updateFile);
			path = "updateFile";
		}
		
		List<String> emails = new ArrayList<>();
		
		MessagesInputSource messages = RequestUtils.getOrCreateMessagesSource(request);

		RowValuesIterator rvIter = SheetUtils.getExcelRowValuesIterator(inputStream);
		String[] columnNames = rvIter.getColumnNames();
		if (columnNames == null || columnNames.length == 0 || !columnNames[0].equals("CARD_ID")) {
			messages.addMessage("error", "usalive-prepaid-card-mass-missing-header-row", "ERROR: The first row of your upload file must contain column headers as described.");
			RequestUtils.redirectWithCarryOver(request, response, "consumer_card_mass_manage.html", false, true);
			return;
		}
		
		int rowNum;
		int rowDiff = 2;
		while (rvIter.hasNext()) {
			long startTsMs = System.currentTimeMillis();
			
			rowNum = rvIter.getCurrentRowNum();
			if(rowNum + 1 > PrepaidUtils.MAX_MASS_CARD_UPDATE_ROW) {
				messages.addMessage("error", "usalive-prepaid-card-mass-exceed-max", "Maximum records reached. Records beyond {0} are not processed.", PrepaidUtils.MAX_MASS_CARD_UPDATE_ROW);
				break;
			}
			
			Map<String,Object> columnValues = rvIter.next();
			if (columnValues.isEmpty()) {
				messages.addMessage("error", "usalive-prepaid-card-mass-empty-row", "ROW {0}: Ignored empty record", (rowNum+rowDiff));
			} else {
				Map<String, Object> params = new HashMap<String, Object>();
				int cardId = -1;
				long consumerAcctId = -1;
				long consumerAcctTypeId = -1;
				try {
					cardId = ConvertUtils.getInt(columnValues.get("CARD_ID"));
					
					params.put("checkUser", user);
					params.put("cardId", cardId);
					DataLayerMgr.selectInto("CHECK_CONSUMER_CARD_PERMISSION_EDIT", params);
					
					consumerAcctId = ConvertUtils.getLong(params.get("consumerAcctId"));
					consumerAcctTypeId = ConvertUtils.getLong(params.get("consumerAcctTypeId"));
					
					if (actionType.equals("balanceUpdate")) {
						if (!Arrays.equals(columnNames, BALANCE_UPDATE_HEADERS)) {
							messages.addMessage("error", "usalive-prepaid-card-mass-invalid-header-row", "ERROR: The first row of your upload file must contain column headers as described.");
							RequestUtils.redirectWithCarryOver(request, response, "consumer_card_mass_manage.html", false, true);
							return;
						}
						
						BigDecimal amount = ConvertUtils.convertRequired(BigDecimal.class, columnValues.get("AMOUNT"));
						amount = amount.setScale(2, RoundingMode.HALF_UP);
						String reason = ConvertUtils.getString(columnValues.get("REASON"), true);
						
						if (amount.compareTo(PrepaidUtils.MAX_CARD_BALANCE_CHANGE) > 0 || amount.compareTo(PrepaidUtils.MIN_CARD_BALANCE_CHANGE) <= 0) {
							messages.addMessage("error", "usalive-prepaid-card-mass-required-missing", "ROW {0}, CARD_ID {1}: Amount needs to be >{1} and <= {2}", (rowNum+rowDiff), cardId, PrepaidUtils.MIN_CARD_BALANCE_CHANGE, PrepaidUtils.MAX_CARD_BALANCE_CHANGE);
						} else if (StringUtils.isBlank(reason)) {
							messages.addMessage("error", "usalive-prepaid-card-mass-required-missing", "ROW {0}, CARD_ID {1}: Required value missing", (rowNum+rowDiff), cardId);
						} else {
							if (!StringUtils.isBlank(increaseType)) {
								String messagePrefix = MessageFormat.format("ROW {0}, CARD_ID {1}: Increase balance by {2}: ", (rowNum+rowDiff), cardId, amount);
								if (consumerAcctTypeId == 7) {
									Map<String, Object> increaseParams = new HashMap<>();
									increaseParams.put("consumerAcctId", consumerAcctId);
									increaseParams.put("simple.servlet.ServletUser.userId", user.getUserId());
									increaseParams.put("amount", amount);
									increaseParams.put("reason", reason);
									DataLayerMgr.executeCall("INCREASE_PAYROLL_DEDUCT_ACCT", increaseParams, true);
									messages.addMessage("success", "usalive-payroll-deduct-acct-increase-successful", messagePrefix + "Success");
									WebHelper.publishAppRequestRecord(USALiveUtils.APP_CD, request, new USALiveRecordRequestFilter(), new CallInputs(), startTsMs, "Increase Payroll Deduct Balance", "consumer_acct", String.valueOf(consumerAcctId), increaseParams, null, log);
								} else {
									PrepaidUtils.increasePrepaidBalance(consumerAcctId, amount, reason, user, messages, WebHelper.getDefaultPublisher(),messagePrefix);
								}
							} else {
								String messagePrefix = MessageFormat.format("ROW {0}, CARD_ID {1}: Decrease balance by {2}: ", (rowNum+rowDiff), cardId, amount);
								if (consumerAcctTypeId == 7) {
									Map<String, Object> decreaseParams = new HashMap<>();
									decreaseParams.put("consumerAcctId", consumerAcctId);
									decreaseParams.put("simple.servlet.ServletUser.userId", user.getUserId());
									decreaseParams.put("amount", amount);
									decreaseParams.put("reason", reason);
									DataLayerMgr.executeCall("DECREASE_PAYROLL_DEDUCT_ACCT", decreaseParams, true);
									messages.addMessage("success", "usalive-payroll-deduct-acct-decrease-successful", messagePrefix + "Success");
									WebHelper.publishAppRequestRecord(USALiveUtils.APP_CD, request, new USALiveRecordRequestFilter(), new CallInputs(), startTsMs, "Decrease Payroll Deduct Balance", "consumer_acct", String.valueOf(consumerAcctId), decreaseParams, null, log);
								} else {
									PrepaidUtils.decreasePrepaidBalance(consumerAcctId, amount, reason, user, messages, WebHelper.getDefaultPublisher(), messagePrefix);
								}
							}
						}
					} else if (actionType.equals("allowNegativeBalanceMassUpdate")) {
						if (!Arrays.equals(columnNames, ANB_HEADERS)) {
							messages.addMessage("error", "usalive-prepaid-card-mass-invalid-header-row", "ERROR: The first row of your upload file must contain column headers as described.");
							RequestUtils.redirectWithCarryOver(request, response, "consumer_card_mass_manage.html", false, true);
							return;
						}

						char allowNegativeBalance = ConvertUtils.getChar(columnValues.get("ALLOW_NEGATIVE_BALANCE"));
						params.put("allowNegativeBalance", allowNegativeBalance);
						int updateCount = DataLayerMgr.executeUpdate("UPDATE_CONSUMER_ACCT_ANB", params, true);
						if (updateCount == 1) {
							messages.addMessage("success", "usalive-prepaid-anb-update-success", "ROW {0}, CARD_ID {1}: Allow negative balance set to {2} ", (rowNum+rowDiff), cardId, allowNegativeBalance);
						} else {
							messages.addMessage("error", "usalive-prepaid-status-toggle-error", "ROW {0}, CARD_ID {1}: Failed to set allow negative balance to {2}. Please contact customer service for assistance.", (rowNum+rowDiff), cardId, allowNegativeBalance);
						}
					} else if (actionType.equals("accountActivate")) {
						if (!Arrays.equals(columnNames, ACTIVATE_HEADERS)) {
							messages.addMessage("error", "usalive-prepaid-card-mass-invalid-header-row", "ERROR: The first row of your upload file must contain column headers as described.");
							RequestUtils.redirectWithCarryOver(request, response, "consumer_card_mass_manage.html", false, true);
							return;
						}
						
						String email = ConvertUtils.getString(columnValues.get("EMAIL"), true).toLowerCase();
						if (emails.contains(email)) {
							log.debug("ROW {0}, CARD_ID {1}: Email {2} was already used in this file. Each account must have a valid unique email address.", (rowNum+rowDiff), cardId, email);
							messages.addMessage("error", "usalive-prepaid-card-mass-dup-email", "ROW {0}, CARD_ID {1}: Email {2} was already used in this file. Each account must have a valid unique email address.", (rowNum+rowDiff), cardId, email);
						} else {
							Map<String, Object> cardholderParams = new HashMap<>();
							cardholderParams.put("consumerAcctId", consumerAcctId);
							cardholderParams.put("firstname", ConvertUtils.getString(columnValues.get("FIRST_NAME"), true));
							cardholderParams.put("lastname", ConvertUtils.getString(columnValues.get("LAST_NAME"), true));
							cardholderParams.put("email", email);
							cardholderParams.put("customerAssignedId", ConvertUtils.getString(columnValues.get("CARDHOLDER_ID"), true));
							cardholderParams.put("region", ConvertUtils.getString(columnValues.get("REGION_NAME"), false));
							cardholderParams.put("department", ConvertUtils.getString(columnValues.get("DEPARTMENT_NAME"), false));
							cardholderParams.put("topUpAmt", ConvertUtils.getDouble(columnValues.get("PERIODIC_LIMIT")));
							cardholderParams.put(SimpleServlet.ATTRIBUTE_USER, user);
							DataLayerMgr.executeCall("ACTIVATE_PAYROLL_DEDUCT_ACCT", cardholderParams, true);
							messages.addMessage("success", "usalive-cardholder-activate-success", "ROW {0}, CARD_ID {1}: Account activated", (rowNum+rowDiff), cardId);
							emails.add(email);
						}
					}
				} catch (NotEnoughRowsException e) {
					log.debug("ROW {0}, CARD_ID {1}: Card not found.", (rowNum+rowDiff), cardId, e);
					messages.addMessage("error", "usalive-prepaid-card-mass-card-not-permitted", "ROW {0}, CARD_ID {1}: Card not found", (rowNum+rowDiff), cardId);
				} catch(ConvertException e) {
					log.debug("ROW {0}, CARD_ID {1}: Required value missing", (rowNum+rowDiff), cardId, e);
					messages.addMessage("error", "usalive-prepaid-card-mass-required-missing", "ROW {0}, CARD_ID {1}: Required value missing", (rowNum+rowDiff), cardId);
				} catch(SQLException e) {
					log.debug("ROW {0}, CARD_ID {1}: {2}: {3}", (rowNum+rowDiff), cardId, e.getErrorCode(), e.getMessage(), e);
					String msg = OracleHelper.getORAErrorMessage(e);
					if (msg != null) { 
						messages.addMessage("error", "usalive-prepaid-card-mass-other", "ROW {0}, CARD_ID {1}: {2}", (rowNum+rowDiff), cardId, msg);
					} else {
						throw e;
					}
				}
			}
		}
		RequestUtils.redirectWithCarryOver(request, response, "consumer_card_mass_manage.html", false, true);
	}
	
%>

