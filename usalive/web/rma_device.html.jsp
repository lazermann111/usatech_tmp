<%@page import="simple.bean.ConvertUtils"%>
<%@page import="com.usatech.usalive.servlet.USALiveUtils"%>
<%@page import="simple.servlet.ResponseUtils"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="simple.text.StringUtils"%>

<%
	UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
	boolean hasPri=false;
	if((user.isInternal()&&user.hasPrivilege(ReportingPrivilege.PRIV_CUSTOMER_SERVICE))||user.hasPrivilege(ReportingPrivilege.PRIV_RMA)){
		hasPri=true;
	}
	String needReplacement=ConvertUtils.getStringSafely(RequestUtils.getAttribute(request, "needReplacement", false), "N");
%>
<jsp:include page="include/header.jsp"/>
<%if(hasPri){ %>
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "scripts/utils.js") %>"></script>
<script type="text/javascript">
var emailInvalid=0;
function onSearch(){
	$("message").set("html","");
	$("waitImage").style.display='block';
	$("searchResultSection").style.display='block';
	$("deviceSerialCd").value="";
	$("addSelect").style.display='block';
	$("removeSelect").style.display='none';
	if($('deviceSearchByText').value && $('deviceSearchByText').value.trim()!=""){
		$("deviceSerialCd").value=$('deviceSearchByText').value;
	}
	new Request.HTML({ 
    	url: "rma_device_search.html",
    	data: $("searchForm"), 
    	method: 'post',
    	update : $("searchResultSection"),
    	onComplete : function() {
    		$("waitImage").style.display='none';
    	},
		evalScripts: true
	}).send();
}

function onHideSearch(){
	$("message").set("html","");
	if($("searchResultSection").style.display!="none"){
		$("searchResultSection").style.display='none';
		$("hideSearch").value="Display Previous Search";
		$("addSelect").style.display='none';
		$("removeSelect").style.display='block';
	}else{
		$("searchResultSection").style.display='block';
		$("hideSearch").value="Hide Search";
		$("addSelect").style.display='block';
		$("removeSelect").style.display='none';
	}
}

function onAddSelected(){
	var count=0;
	$('searchResultSection').getElements('input[type="checkbox"][name="rmaDevices"]:checked').each(function (el) {
	       //alert(el.getParent().getParent().get("data-return-device-id"));
	        count++;
	       if(el.id!="selectAllCheckbox" && $('currentReturnTable').getElements('input[type="checkbox"][value="'+el.value+'"]').length==0){
		       var copy = el.getParent().getParent().clone();
		       copy.class="evenRowHover";
		       var newTd1 = new Element('td');
		       var newInput1 = new Element('input', {
		    	    type: 'checkbox',
		    	    'name': copy.get("data-return-device-id")+'-isTelemetryOnly',
		    	    'value':'Y'
		    	});
		       newTd1.adopt(newInput1);
		       var newTd2 = new Element('td');
		       var newInput2 = new Element('input', {
		    	    type: 'text',
		    	    'name': copy.get("data-return-device-id")+'-desc',
		    	    size: 100,
		    	    class: "maxLength:4000"
		    	    
		    	});
		       newTd2.adopt(newInput2);
		       copy.adopt(newTd1);
		       copy.adopt(newTd2);
		       $('currentReturnTable').adopt(copy);	     
		      
	       }
	    });
	if(count>0){
		$("message").set("html","<p class='status-info-success'>Successfully added for a RMA</p>");
		$("removeSelect").style.display='block';
	}
}


function onRemoveSelected(){
	var count=0;
	$('currentReturnSection').getElements('input[type="checkbox"][name="rmaDevices"]:checked').each(function (el) {
	       //alert(el.getParent().getParent().get("data-return-device-id"));
	       if(el.id!="selectAllReturnCheckbox"){
	       	el.getParent().getParent().dispose();
	       }
	       count++;
	    });
	if(count>0){
		$("message").set("html","<p class='status-info-success'>Successfully removed for a RMA</p>");
	}
	
}
function onSelectAllCheckbox(){
	$("message").set("html","");
	if($("selectAllCheckbox").checked){
		$('searchResultSection').getElements("input[type=checkbox][name=rmaDevices])").each(function(el) {
			el.checked=true;
		});
	}else{
		$('searchResultSection').getElements("input[type=checkbox][name=rmaDevices])").each(function(el) {
			el.checked=false;
		});
	}
}
function onSelectAllReturnCheckbox(){
	$("message").set("html","");
	if($("selectAllReturnCheckbox").checked){
		$('currentReturnSection').getElements("input[type=checkbox][name=rmaDevices])").each(function(el) {
			el.checked=true;
		});
	}else{
		$('currentReturnSection').getElements("input[type=checkbox][name=rmaDevices])").each(function(el) {
			el.checked=false;
		});
	}
}

function onCreateRMA(){
	if($('currentReturnSection').getElements('input[type="checkbox"][name="rmaDevices"]').length==0){
		alert("You need to have at least one device for the current RMA. Please search and select. Thank you.");
		return false;
	}else{
		
		$('currentReturnSection').getElements('input[type="checkbox"][name="rmaDevices"]').each(function (el) {
			el.checked=true;
		});
		if($("rmaShippingAddrId") && $("rmaShippingAddrId").selectedIndex==-1){
			$("shippingAddrName").value=$("shippingAddrName").value.trim();
			var isNameInvalid=0;
			$('rmaShippingAddrId').getElements("option").each(function(el) {
				if($("shippingAddrName").value == el.getText()){
					alert("Please enter unique shipping address name.");
					isNameInvalid=1;
					return;
				}
			});
			if(isNameInvalid==1){
				return false;
			}
		}
		$('currentReturnSection').getElements("input[type=checkbox])").each(function(el) {
			el.set("disabled", "");
		});
		if($('rmaDescription').value && $('rmaDescription').value.trim()!=""){
			$('rmaDescription').value=$('rmaDescription').value.substring(0,4000);
		}
		if(!document.getElements("input[class=validation-failed]")){
			$("email").disabled="";
		}
		$("email").value=cleanEmail($("email").value);
		new Request.HTML({ 
	        url: "rma_email_check.html",
	        method: 'post',
	        data: $("returnForm"), 
	        evalScripts: true,
			async: false
	    }).send();
		if(emailInvalid==1){
			$("checkEmailMsg").set("html","Invalid emails. Please enter valid ones.");
			$("checkEmailMsg").set("class","status-info-failure");
			return false;
		}else{
			$("checkEmailMsg").set("html","");
			$("checkEmailMsg").set("class","");
			return true;
		}
	}
}

window.addEvent('domready', function() { 	
	new Request.HTML({ 
        url: "rma_carrier_account_select.html",
        method: 'post',
        update : $("userShippingCarrier"),
        evalScripts: true
    }).send();
	
	new Request.HTML({ 
        url: "rma_shipping_address_select.html",
        method: 'post',
        update : $("rmaShippingAddr"),
        evalScripts: true
    }).send();
	$("postalCd").set("class", "");
	$("advice-required-postalCd").dispose();
	<%if(needReplacement.equals("Y")){%>
		$("replacementSection").getElements("input").each(function(el) {
			el.set("disabled",false);
		});
		$("rmaReplacementQuantitySection").set("style","display:none");
	<%}%>
	new Form.Validator.Inline.Mask(document.returnForm);
});


</script>
<span class="caption">Create RMA</span>
<form name="searchForm" id="searchForm">
<input type="hidden" id="fragment" name="fragment" value="true"/>
<input type="hidden" id="deviceSerialCd" name="deviceSerialCd" value=""/>
<input type="hidden" id="rmaTypeId" name="rmaTypeId" value="2"/>
<%if(needReplacement.equals("Y")){%>
<input type="hidden" id="needReplacement" name="needReplacement" value="<%=StringUtils.prepareCDATA(needReplacement)%>">
<%} %>
<table class="full"><tr><td>
<div class="topmessage">
<div id="waitImage" style="display:none">Please wait...<img alt="Please wait" src="images/pleasewait.gif"/></div>
<div id="message" class="manageCampaignMessage"></div>
</div>
<div class="sectionTitle">Search And Select Devices For Return</div>
                <div class="sectionRow">               
Search by Device Serial Number: (1 per line)<br/><br/>
<textarea id="deviceSearchByText" style="width: 10%; resize: none;" rows="10" name="deviceSearchByText"></textarea>
<input id="search" type="button" onclick="onSearch();" value="Search"/>
<input id="hideSearch" type="button" onclick="onHideSearch();" value="Hide Search"/>
</div>
</div>
</td>
</tr>
</table>
<div id="searchResultSection">
</div>
<table>
<tr>
<td><input id="addSelect" type="button" onclick="onAddSelected();" value="Add selected" style="display:none"/></td>
</tr>
</table>
</form>
<form name="returnForm" method="POST" id="returnForm" action="rma_create_device.html" onSubmit="return onCreateRMA();">
<input type="hidden" id="rmaTypeId" name="rmaTypeId" value="2"/>
<input type="hidden" id="actionType" name="actionType" value=""/>
<input type="hidden" id="transportTypeId" name="transportTypeId" value="1"/>
<input type="hidden" id="transportAction" name="transportAction" value="test"/>
<input type="hidden" id="tpv_1" name="tpv_1" value=""/>
<%
ResponseUtils.writeXsfrFormProtection(pageContext);
USALiveUtils.writeFormProfileId(pageContext);
%>
<input id="removeSelect" type="button" onclick="onRemoveSelected();" value="Remove selected" style="display:none"/>
<div class="sectionTitle">Device For Current RMA</div>
<div id="currentReturnSection">
<table id="currentReturnTable" class="folio">
<tr class="headerRow">
			<th><input id="selectAllReturnCheckbox" type="checkbox" onclick="onSelectAllReturnCheckbox();"></th>
			<th>
			<a title="Sort Rows by Device Serial Number" data-toggle="sort" data-sort-type="STRING">Device Serial Number</a>
			</th>
			<th>
			<a title="Sort Rows by Region Name" data-toggle="sort" data-sort-type="STRING">Region Name</a>
			</th>
			<th>
			<a title="Sort Rows by location" data-toggle="sort" data-sort-type="STRING">Location</a>
			</th>
			<th>
			<a title="Sort Rows by Asset Nbr" data-toggle="sort" data-sort-type="STRING">Asset Nbr</a>
			</th>
			<th>
			<a title="Sort Rows by Rental" data-toggle="sort desc" data-sort-type="STRING">Rental</a>
			</th>
			<th>
			<a title="Sort Rows by Rental Fee" data-toggle="sort" data-sort-type="STRING">Rental Fee</a>
			</th>
			<th>
			<a title="Sort Rows by Telemetry" data-toggle="sort" data-sort-type="STRING">Telemetry Only</a>
			</th>
			<th>
			<a>Description of issue</a>
			</th>
			</tr>
</table>
</div>
RMA Description: <input id="rmaDescription" name="rmaDescription" type="text" value="" size="100" class="maxLength:4000"/>
<br/>
<br/>
<jsp:include page="rma_replacement.jsp"/>
<input id="createRMA" type="submit" value="Create RMA"/>
</form>
<%}else{%>
	<p class="not-authorized">User '<%=StringUtils.prepareHTML(user.getUserName()) %>' does not have permission to view this page. Please contact USA Technologies if you need access.</p>
<%}%>
<jsp:include page="include/footer.jsp"/>