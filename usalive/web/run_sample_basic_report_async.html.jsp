<%@page import="simple.servlet.ResponseUtils"%>
<%@page import="simple.db.NotEnoughRowsException"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="com.usatech.report.ReportScheduleType"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.results.Results"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.usatech.report.ReportingUser"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.RequestUtils"%>
<%
// calculdate necessary params (batchId or begin/enddate)
int basicReportId = RequestUtils.getAttribute(request, "reportId", int.class, true);
ReportingUser user = (ReportingUser)RequestUtils.getUser(request);
user.checkPermission(ReportingUser.ResourceType.BASIC_REPORT, ReportingUser.ActionType.VIEW, basicReportId);
InputForm form = RequestUtils.getInputForm(request);
Map<String,Object> params = new HashMap<>();
params.put("reportId", basicReportId);
params.put("userId", user.getUserId());
StringBuilder query = new StringBuilder();
ResponseUtils.appendQueryParam("basicReportId", basicReportId, query);
try (Results results = DataLayerMgr.executeQuery("GET_BASIC_REPORT", params)) {
	if(!results.next())
        throw new ServletException("Invalid report (" + basicReportId + ")");      
  ReportScheduleType scheduleType = results.getValue("scheduleTypeId", ReportScheduleType.class);
  String reportName = results.getFormattedValue("name");
  boolean permitted = ConvertUtils.getBoolean(results.getValue("permitted"), false);
  if(!permitted)
      throw new ServletException("You do not have permission to run '" + reportName + "'");
  params.clear();
  for(Map.Entry<String,Object> entry : form.getParameters().entrySet()) 
      if(entry.getKey() != null && entry.getKey().startsWith("params.") && entry.getValue() != null) {
    	  String paramName = entry.getKey().substring(7);
          ResponseUtils.appendQueryParam(paramName, entry.getValue(), query);
          params.put(paramName, entry.getValue());         
      }
  switch(scheduleType) {
	  case TIMED:
		  int frequencyId = RequestUtils.getAttribute(request, "frequencyId", int.class, true);
		  params.put("frequencyId", frequencyId);
		  DataLayerMgr.selectInto("GET_FREQUENCY_DATE_RANGE", params);
          ResponseUtils.appendQueryParam("frequencyName", params.get("frequencyName"), query);
          ResponseUtils.appendQueryParam("beginDate", params.get("beginDate"), query);
          ResponseUtils.appendQueryParam("endDate", params.get("endDate"), query);
          break;
      default:
    	  //get first sample batch_id
    	  params.put("userId", user.getUserId());
    	  params.put("maxRows", 1);
    	  try {
    		  DataLayerMgr.selectInto("GET_SAMPLE_" + scheduleType + "_BATCH_ID", params);
    	  } catch(NotEnoughRowsException e) {
    		  RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "run-sample-basic-report-batch-none-found", "You do not have any {0} batches to provide a sample", scheduleType.getDescription());
    		  Long userReportId = RequestUtils.getAttribute(request, "userReportId", Long.class, false);
    		  RequestUtils.redirectWithCarryOver(request, response, userReportId == null ? "user_report.i" : "user_report.i?userReportId=" + userReportId);
              return;
    	  }
    	  ResponseUtils.appendQueryParam("batchId", params.get("batchId"), query);
    	  if(scheduleType == ReportScheduleType.EVENT)
    		  ResponseUtils.appendQueryParam("batchType", params.get("batchType"), query);         
    }
}
query.insert(0, "run_basic_report_async.i?");
RequestUtils.redirectWithCarryOver(request, response,  query.toString());
%>