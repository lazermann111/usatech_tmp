<%@page import="java.util.Calendar"%>
<%@page import="simple.text.StringUtils,simple.servlet.RequestUtils,simple.results.Results"%>
<%
String reportName=RequestUtils.getAttribute(request, "reportName", String.class, true);
Integer basicReportId=RequestUtils.getAttribute(request, "basicReportId", Integer.class, true);
Calendar today=Calendar.getInstance();
int monthInt=today.get(Calendar.MONTH);
int previousMonth=monthInt-1;
if(previousMonth<0){
	previousMonth=11;
}
int yearInt=today.get(Calendar.YEAR);
%>
<form id="searchForm" name="searchForm" action="run_basic_report_async.i"
				onsubmit="return validateYearMonth()">
<script type="text/javascript">
	
	function validateYearMonth() {
		if($('year').value &lt; 1970 || $('year').value  &gt; <%=yearInt%>){
			alert("Year is invalid.");
			return false;
		}else if($('year').value==<%=yearInt%>){
			if($('month').value &gt; <%=monthInt%>){
				alert("Month is invalid.");
				return false;
			}
		}
		var endDate=new Date($('year').value, $('month').value, 1,0,0,0,0);
		$("endDate").value=endDate.getMonth() + 1+ '/' + endDate.getDate()+ '/' +endDate.getFullYear();
		return true;
	}
</script>
<div class="sectionRow">
<table class="selectBody" style="width: 335px;">
	<caption><%=StringUtils.prepareHTML(reportName)%></caption>
	<tr>
		<td>
			<input type="hidden" name="basicReportId" value="<%=basicReportId%>" />
			<input id="endDate" type="hidden" name="endDate" value="" />
			Month:<select id="month" name="month">
				<option value="0" <%if(previousMonth==0) {%> selected="selected"<%} %>>January</option>
				<option value="1" <%if(previousMonth==1) {%> selected="selected"<%} %>>February</option>
				<option value="2" <%if(previousMonth==2) {%> selected="selected"<%} %>>March</option>
				<option value="3" <%if(previousMonth==3) {%> selected="selected"<%} %>>April</option>
				<option value="4" <%if(previousMonth==4) {%> selected="selected"<%} %>>May</option>
				<option value="5" <%if(previousMonth==5) {%> selected="selected"<%} %>>June</option>
				<option value="6" <%if(previousMonth==6) {%> selected="selected"<%} %>>July</option>
				<option value="7" <%if(previousMonth==7) {%> selected="selected"<%} %>>August</option>
				<option value="8" <%if(previousMonth==8) {%> selected="selected"<%} %>>September</option>
				<option value="9" <%if(previousMonth==9) {%> selected="selected"<%} %>>October</option>
				<option value="10" <%if(previousMonth==10) {%> selected="selected"<%} %>>November</option>
				<option value="11" <%if(previousMonth==11) {%> selected="selected"<%} %>>December</option>
				</select>
			Year: <input id="year" type="text" name="year" size="4" maxlength="4" value="<%=yearInt%>"/>
			<div style="text-align: center; clear: both;">
				<input type="submit" value="Run Report" />
			</div>
		</td>
	</tr>
</table>
</div>
</form>