<%@page import="simple.text.StringUtils,simple.bean.ConvertUtils"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.results.Results"%>
<script type="text/javascript">
<%
InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
if(inputForm.getAttribute("reportId")==null){
	DataLayerMgr.selectInto("GET_REPORTID_BY_USERREPORT", inputForm);
}
Results results = DataLayerMgr.executeQuery("GET_NOT_ALLOWED_TRANSPORT_TYPE", inputForm);
int i = 0;
if(results.next()) {
    %>
    notAllowedTransportTypeIds=<%=StringUtils.prepareScript(results.getValue("notAllowedTransportTypeIds", String.class)) %>;
    <%
}else{%>
	notAllowedTransportTypeIds=[];
<%
}%>
</script>
