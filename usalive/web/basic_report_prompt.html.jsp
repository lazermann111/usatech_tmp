<%@page import="simple.results.BeanException"%>
<%@page import="simple.io.Log"%>
<%@page import="simple.db.DataLayerException"%>
<%@page import="java.sql.SQLException"%>
<%@page import="com.usatech.report.ReportScheduleType"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.usatech.report.ReportRequestFactory"%>
<%@page import="simple.param.ParamEditor"%>
<%@page import="simple.param.html.DefaultParamRenderer"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="com.usatech.report.ReportingUser"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.RequestUtils"%>
<%
int reportId = RequestUtils.getAttribute(request, "reportId", Integer.class, true);
ReportingUser user = (ReportingUser)RequestUtils.getUser(request);
user.checkPermission(ReportingUser.ResourceType.BASIC_REPORT, ReportingUser.ActionType.VIEW, reportId);
Map<String,Object> params = new HashMap<>();
params.put("reportId", reportId);
params.put("userId", user.getUserId());
try (Results results = DataLayerMgr.executeQuery("GET_BASIC_REPORT", params)) {
	try (Results paramResults = DataLayerMgr.executeQuery("GET_CONFIGURABLE_BASIC_REPORT_PARAMS", params)) {
        params.put("configurableFillins", ReportRequestFactory.Fillin.getConfigurableValues());
        try (Results fillinResults = DataLayerMgr.executeQuery("GET_CONFIGURABLE_BASIC_REPORT_FILLINS", params)) {
	    	if(!results.next())
				  throw new ServletException("Invalid report (" + reportId + ")");      
		    ReportScheduleType scheduleType = results.getValue("scheduleTypeId", ReportScheduleType.class);
			int generatorId = results.getValue("generatorId", Integer.class);
			String reportName = results.getFormattedValue("name");
			boolean permitted = ConvertUtils.getBoolean(results.getValue("permitted"), false);
            if(!permitted)
            	throw new ServletException("You do not have permission to run '" + reportName + "'");
			if(paramResults.isGroupEnding(0) && fillinResults.isGroupEnding(0) && scheduleType != ReportScheduleType.EVENT && generatorId != 6) {
				//no params, so go directly to report
				RequestUtils.redirectWithCarryOver(request, response, "run_basic_report_async.i?basicReportId=" + reportId);
				return;
			}
				
            RequestUtils.setAttribute(request, "extra-stylesheets", "selection/report-selection-scripts.js", "request");
			%><jsp:include page="include/header.jsp" />
			<div class="selectionCriteria">
			<table id="basicReportParams">
			<caption class="title2"><%=StringUtils.prepareHTML(reportName) %></caption>
			<tr>
			<td>
			<form action="run_basic_report_async.i" data-toggle="validateInline">
            <input type="hidden" name="basicReportId" value="<%=reportId%>"/>
            <div class="selectSection">
			<div class="sectionTitle">Configure report parameters</div>
			<div class="sectionRow">	
			<table class="input-rows">
	        <% 
	        String beginDateId = null;
	        while(fillinResults.next()) {
	            ReportRequestFactory.Fillin fillin = fillinResults.getValue("fillin", ReportRequestFactory.Fillin.class);
	            Object value = RequestUtils.getAttribute(request, fillin.getParamName(), String.class, false);
	            String label = fillin.getDescription();
	            if(fillin == ReportRequestFactory.Fillin.BATCH) {
	            	label = scheduleType.getDescription() + " #";
	            } %><tr><th><%=label %></th><td><%
	            String text;
                switch(fillin) {
	            	case BEGIN:
	            		beginDateId = "beginDateId";
	            		%><input id="<%=beginDateId%>" type="text" size="10" data-toggle="calendar" data-format="%m/%d/%Y" name="<%=fillin.getParamName() %>" value="<%=StringUtils.encodeForHTMLAttribute(ConvertUtils.formatObject(value, "DATE:MM/dd/yyyy", true))%>" data-validators="required msgPos:'<%=StringUtils.encodeForHTMLAttribute(fillin.getParamName()) %>_advice'"/>
                        <span id="<%=fillin.getParamName()%>_advice"></span>
	            		<% break;
	            	case END:
                        %><input type="text" size="10" data-toggle="calendar" data-format="%m/%d/%Y" name="<%=fillin.getParamName() %>" value="<%=ConvertUtils.formatObject(value, "DATE:MM/dd/yyyy", true)%>" data-validators="required<%
                        if(!StringUtils.isBlank(beginDateId)) { %> validate-after-date afterElement:'<%=beginDateId%>'<%} %> msgPos:'<%=fillin.getParamName() %>_advice'"/>
                        <span id="<%=fillin.getParamName()%>_advice"></span>
                        <% break;
	            	case BATCH:
	            		Long batchId = ConvertUtils.convertSafely(Long.class, value, null);
                        %><input type="text" size="10" name="<%=fillin.getParamName() %>" value="<% if(batchId != null) { out.print(batchId); }%>" data-validators="required validate-positive-integer"/>
                        <% break;
	            	case TERMINALS: case REGIONS:
	            		%><input type="text" size="100" name="<%=StringUtils.encodeForHTMLAttribute(fillin.getParamName()) %>" value="<%  if(value != null && (text=ConvertUtils.getStringSafely(value)) != null) { out.print(StringUtils.encodeForHTMLAttribute(text)); }%>" data-validators="validate-regex:'^\\s*\\d+(\\s*,\\s*\\d+)*\\s*$'"/>
                        <% break;
                    default:
                    	%><input type="text" size="100" name="<%=fillin.getParamName() %>" value="<% if(value != null && (text=ConvertUtils.getStringSafely(value)) != null) { out.print(StringUtils.prepareCDATA(text)); }%>"/>
                        <% break;
                        
	            }%></td></tr><%
	        }
	        if(scheduleType == ReportScheduleType.EVENT) {
                int batchType = ConvertUtils.getIntSafely(params.get("batchType"), 0);
                %><tr><th>Batch Type</th><td><select name="batchType">
                    <option value="6"<%if(batchType == 6) {%> selected="selected"<%} %>>Event</option>
                    <option value="7"<%if(batchType == 7) {%> selected="selected"<%} %>>DEX</option>   
                </select></td></tr><%
	        }
	        DefaultParamRenderer paramRenderer = new DefaultParamRenderer();
	        paramRenderer.setSelectAllMinimum(4);
	        paramRenderer.setLabelAfterCheckControl(true);
	        paramRenderer.setBeforeOption("<label class=\"checkbox\">");
	        paramRenderer.setAfterOption("</label>");
	        //paramRenderer.setOnChange("onDetailChange()");
	
	        while(paramResults.next()) {
	            String paramName = paramResults.getFormattedValue("paramName");
	            if(!StringUtils.isBlank(paramName)) {
	                paramRenderer.render(out, paramName, paramResults.getValue("paramEditor", ParamEditor.class), paramResults.getFormattedValue("paramLabel"), paramResults.getFormattedValue("paramValue"));
	            }
	        }
	        if(generatorId == 6) {
	        	int outputType = 0;
                try {// calculate output type - first via report_param (outputType) then by reportId, folioIds, folioId
                	  DataLayerMgr.selectInto("GET_BASIC_REPORT_OUTPUT_TYPE", params);
                	  outputType = ConvertUtils.getIntSafely(params.get("outputType"), 0);
                } catch(SQLException | DataLayerException | BeanException e) {
                	//ignore
                	Log.getLog().warn("Could not get default output type", e);
                }
                boolean graph = ReportRequestFactory.isGraphOutputType(outputType); 
                int currentOutputType;
	        	%><tr>
	        	    <th>Format</th>
	        	    <td><fieldset class="checkbox-grouping">
	        	    <label class="checkbox" id="report-button-html<% if(graph) { currentOutputType = 28; %>-chart<%} else { currentOutputType = 22; } %>"><input type="radio" name="outputType" value="<%=currentOutputType%>"<%if(outputType == 0 || outputType == currentOutputType) {%> checked="checked"<%} %>/>
	        	    <div class="output-type-label"></div> (Html)</label>
	        	    <label class="checkbox" id="report-button-excel<% if(graph) { currentOutputType = 34; %>-chart<%} else { currentOutputType = 27; } %>"><input type="radio" name="outputType" value="<%=currentOutputType%>"<%if(outputType == currentOutputType) {%> checked="checked"<%} %>/>
	        	    <div class="output-type-label"></div> (Excel)</label>        
	        	    <label class="checkbox" id="report-button-pdf<% if(graph) { currentOutputType = 31; %>-chart<%} else { currentOutputType = 24; } %>"><input type="radio" name="outputType" value="<%=currentOutputType%>"<%if(outputType == currentOutputType) {%> checked="checked"<%} %>/>
	        	    <div class="output-type-label"></div> (Pdf)</label>
	        	    <label class="checkbox" id="report-button-doc<% if(graph) { currentOutputType = 35; %>-chart<%} else { currentOutputType = 23; } %>"><input type="radio" name="outputType" value="<%=currentOutputType%>"<%if(outputType == currentOutputType) {%> checked="checked"<%} %>/>
	        	    <div class="output-type-label"></div> (Word)</label>
	        	    <label class="checkbox" id="report-button-csv"><input type="radio" name="outputType" value="21"<%if(outputType == 21) {%> checked="checked"<%} %>/>
	        	    <div class="output-type-label"></div> (Csv)</label></fieldset>
	        	    </td>
	        	  </tr><%
	        }
	        %>
        </table>
        </div>
        </div>
        <div class="center"><button type="submit">Run Report</button></div>
        </form>
        </td></tr></table></div>
		<jsp:include page="include/footer.jsp" /><%		
	    }
	}
}
%>
