<%@page import="simple.text.StringUtils"%>
<%@page import="com.usatech.usalive.web.OfferUtils"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.InputForm"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<jsp:include page="include/header.jsp"/>

<%
InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
OfferUtils.processOfferSignup(inputForm, request, response);
String message = inputForm.getStringSafely("message", "");
%>

<style>	
	html { font-size: 62.5%;}
	
	img, embed, object, video {max-width: 100%;}
	
	.eoWrapper {
	  text-align: left;	  
	  margin: 5px auto 30px;
	  background-color: #ffffff;
	}
	
	.eoWrapper p {
	  margin: 10px 6%;
	}
	
	.eoWrapper ul {
	  margin: 10px 6%;
	}
	
	#eoMain { 
	  width: 90%;
	  width: 720px;
	  padding-bottom: 10px;
	  margin: 0;
	  font-family: Helvetica, 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans',  Arial, Verdana, sans-serif;
	  font-size: 16px;
	  font-size: 1.6rem;  /* 16px */
	  line-height: 1.5; /* 24px */
	  font-weight: 300;
	  color: #333333;
	}
	
	#eoMain h2 {
	  padding: 20px 6% 0;
	}
	
	#enroll-form {
	  background:#ffffff;
	  padding: 0 6% 20px;
	  margin: 0;
	  text-align: center;
	}
	
	#enroll-form button[type="submit"] {
	font:400 16px/16px Helvetica, Arial, sans-serif;
	}
	
	#enroll-form button[type="submit"] {
	cursor:pointer;
	width:40%;
	border:none;
	background:#ef7f13;
	background-image:linear-gradient(bottom, #ef7f13 0%, #fab228 52%);
	background-image:-moz-linear-gradient(bottom, #ef7f13 0%, #fab228 52%);
	background-image:-webkit-linear-gradient(bottom, #ef7f13 0%, #fab228 52%);
	color:#222222;
	font-weight: 700;
	margin:10px 30% 0;
	padding:10px;
	border-radius:5px;
	}
	#enroll-form button[type="submit"]:hover {
	background-image:linear-gradient(bottom, #d87312 0%, #f4ab20 52%);
	background-image:-moz-linear-gradient(bottom, #d87312 0%, #f4ab20 52%);
	background-image:-webkit-linear-gradient(bottom, #d87312 0%, #f4ab20 52%);
	-webkit-transition:background 0.3s ease-in-out;
	-moz-transition:background 0.3s ease-in-out;
	transition:background-color 0.3s ease-in-out;
	}
	#enroll-form button[type="submit"]:active {
	box-shadow:inset 0 1px 3px rgba(0,0,0,0.5);
	}
</style>

<%if (StringUtils.isBlank(message)) {%>

<img alt="Introducing ePort Online" src="images/eport-online.png" class="mainimg" />

<div class="spacer10"></div>

<div class="eoWrapper">

          <div id="eoMain">

            <p>The <span style="font-weight: bold;">NEW ePort Online</span> gives you the ability to process credit and debit transactions from any computer or mobile device.</p>
            <p style="color: #1f82cf; text-align: center; font-weight: bold; font-size: 18px;">Special introductory offer for existing customers only!</p>

            <p>Access this great new service <span style="color: #ee1303; font-weight: bold;">FREE OF CHARGE</span> if you* enroll by June 30, 2014.** There is NO LIMIT to the number of users per company.</p>            
            
        	<p>By submitting this form, I acknowledge that I have read and agree to the <a href="USAT_ePort-Online_Addendum.pdf">Terms and Conditions</a> (PDF).</p>

    <!-- form -->

    <form id="enroll-form" method="post" action="eport_online_signup.html">
    	<input type="hidden" name="offer_id" value="1" />    	
    	<input type="hidden" name="signup" value="Y" />
    	<input type="hidden" name="priv_id" value="26" />
    
			<div>
				<button name="submit" type="submit" id="enroll-submit">ENROLL ME NOW</button>
			</div>

      <p><a href="home.i">Remind me later</a> &nbsp;&nbsp;&nbsp; <a href="eport_online_signup.html?offer_id=1&signup=N">No thanks</a></p>

		</form>

		<!-- /form -->

      <p style="font-weight: bold;">ePort Online allows you to:</p>

    <ul>
      <li>Receive payment for orders immediately through an online payment portal</li>
      <li>Reduce or eliminate Invoices</li>
      <li>Manage customer accounts</li>
      <li>View all credit/debit transactions in one place through USAT's cloud based portal, USALive</li>
      <li>Set up recurring payments (coming soon)</li>
    </ul>

    <p>
      Watch for additional roll out information and an invitation to attend one of the upcoming online training sessions!</p>

      <p>
      	* If you are not a USALive administrator or authorized agent of your company, your enrollment may be processed pending approval.
        <br/>** Enrollment after June 30, 2014 may be subject to a $10 registration fee per user.
       </p>

  </div>  <!-- /main  -->



        </div> <!-- /wrapper -->
<%} else {%>
<%=message %>
<%} %>

<jsp:include page="include/footer.jsp"/>
