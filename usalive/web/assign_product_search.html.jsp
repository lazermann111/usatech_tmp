<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.Date"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.db.DataLayerMgr"%>

<%
	InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
	Results columnMappingResults=DataLayerMgr.executeQuery("GET_CAMPAIGN_COLUMN_MAPPINGS", inputForm);
	String vendColumn;
	String vendColumnId;
	
%>
	<select size="16" name="products" id="products"  style="width: 100%; height:150px;">
			<%
			while(columnMappingResults.next()) {
				
				vendColumn=columnMappingResults.getValue("vendColumn", String.class);
			    vendColumnId=columnMappingResults.getValue("rowNum", String.class);
			    %>
			    <option value="" id="<%=vendColumnId%>" >
			    		<%=StringUtils.prepareHTML(columnMappingResults.getFormattedValue("vendColumn"))%>=<%=StringUtils.prepareHTML(columnMappingResults.getFormattedValue("coil"))%>=<%=StringUtils.prepareHTML(columnMappingResults.getFormattedValue("code"))%></option>
			    <%
			}
			%>
	</select>


