<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@page import="simple.text.StringUtils,simple.servlet.RequestUtils,simple.results.Results"%>
<%
UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
Integer basicReportId=null;
Results result=null;
String reportName=null;
	basicReportId=RequestUtils.getAttribute(request, "basicReportId", Integer.class, true);
	result=DataLayerMgr.executeQuery("GET_REPORT_NAME", new Object[]{basicReportId});
	if(result.next()){
		reportName=result.getFormattedValue("reportName");
	}
%>
<jsp:include page="include/header.jsp"/>
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "selection/report-selection-scripts.js") %>"></script>
<script type="text/javascript">
window.addEvent('domready', function() { 
	new Form.Validator.Inline.Mask(document.reportForm);
	
});
</script>
<form id="reportForm" name="reportForm" action="run_sql_folio_report_async.i">
<input type="hidden" name="basicReportId" value="<%=basicReportId%>" />
			
<table class="selectBody" >
<caption><%if(!StringUtils.isBlank(reportName)){%><%=StringUtils.prepareHTML(reportName)%><%} %></caption>
<tbody>
<tr>
<td width="90%" style="padding:0px">
<jsp:include page="selection-date-range.jsp"/>
</td>
</tr>
<tr>
<td style="padding:0px; text-align: center; background: #D9E6FB; vertical-align: middle;">
			Low Sales Amount: <span class="required">*</span> <input value="10" data-validators="required validate-regex-nospace:'<%=StringUtils.prepareScript("^\\d{1,8}(.\\d{1,2}){0,1}$")%>'" id="lowAmount" type="text" value="" name="params.lowAmount">
</td>
</tr>
<tr>
<td style="padding:0px; text-align: center; background: #D9E6FB; vertical-align: middle;">
			<label id="report-button-html">
<input type="radio" checked="checked" value="22" name="outputType">
<div class="output-type-label"></div>
(Html)
</label>
<label id="report-button-excel">
<input type="radio" value="27" name="outputType">
<div class="output-type-label"></div>
(Excel)
</label>
<label id="report-button-pdf">
<input type="radio" value="24" name="outputType">
<div class="output-type-label"></div>
(Pdf)
</label>
<label id="report-button-doc" >
<input type="radio" value="23" name="outputType">
<div class="output-type-label"></div>
(Word)
</label>
<label id="report-button-csv" >
<input type="radio" value="21" name="outputType">
<div class="output-type-label"></div>
(Csv)
</label>
</td>
</tr>
<tr>
<td style="padding:0px; text-align: center; background: #D9E6FB; vertical-align: middle;">
			<input type="submit" value="Run Report" />

</td>
</tr>
</tbody>
</table>
</form>
<jsp:include page="include/footer.jsp"/>