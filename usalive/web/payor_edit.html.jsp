<%@page import="java.math.BigDecimal"%>
<%@page import="simple.text.MessageFormat"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.TimeZone"%>
<%@page import="com.usatech.layers.common.util.ExtendedCronExpression"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.xml.sax.MessagesInputSource"%>
<%@page import="com.usatech.report.ReportingUser.ActionType"%>
<%@page import="com.usatech.report.ReportingUser.ResourceType"%>
<%@page import="simple.servlet.NotAuthorizedException"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="com.usatech.usalive.web.PayorUtils"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
if(!user.hasPrivilege(ReportingPrivilege.PRIV_BACK_OFFICE))
    throw new NotAuthorizedException("You are not authorized to perform this function.");
long payorId = RequestUtils.getAttribute(request, "payorId", Long.class, true);
if("POST".equalsIgnoreCase(request.getMethod())) {
	RequestUtils.setAttribute(request, "action", "edit", "request");
	user.checkPermission(ResourceType.PAYOR, ActionType.EDIT, payorId);
	boolean fullUpdate = RequestUtils.getAttributeDefault(request, "full", Boolean.class, true);  
	String payorName = RequestUtils.getAttribute(request, "payorName", String.class, fullUpdate);
	if(payorName != null)
		payorName = payorName.trim();        
	String payorEmail = RequestUtils.getAttribute(request, "payorEmail", String.class, false);
	if(payorEmail != null)
		payorEmail = payorEmail.trim();
	Long bankAcctId = RequestUtils.getAttribute(request, "bankAcctId", Long.class, fullUpdate);
	Boolean replace = RequestUtils.getAttribute(request, "changeCard", Boolean.class, false);
	MessagesInputSource messages = RequestUtils.getOrCreateMessagesSource(request);
    String recurSchedule;
    Character recurType = RequestUtils.getAttribute(request, "recurType", Character.class, false);
	if(recurType == null)
		recurSchedule = null;
	else {
		int recurHour = RequestUtils.getAttribute(request, "recurHour", Integer.class, true); 
		switch(recurType.charValue()) {
			case 'Y': // yearly
				Character recurYearlySubType = RequestUtils.getAttribute(request, "recurYearlySubType", Character.class, true);
				switch(recurYearlySubType) {
                    case 'D':
                    	int recurYearlyDayMonthOfYear = RequestUtils.getAttribute(request, "recurYearlyDayMonthOfYear", Integer.class, true);
                    	int recurYearlyDayOfMonth = RequestUtils.getAttribute(request, "recurYearlyDayOfMonth", Integer.class, true); 
                    	recurSchedule = ExtendedCronExpression.buildExpression(recurHour, new int[] {recurYearlyDayOfMonth}, new int[] {recurYearlyDayMonthOfYear}, null, 0);
                        break;
                    case 'T':
                    	int recurYearlyWeekOfMonth = RequestUtils.getAttribute(request, "recurYearlyWeekOfMonth", Integer.class, true);
                    	int recurYearlyDayOfWeek = RequestUtils.getAttribute(request, "recurYearlyDayOfWeek", Integer.class, true); 
                    	int recurYearlyWeekMonthOfYear = RequestUtils.getAttribute(request, "recurYearlyWeekMonthOfYear", Integer.class, true);  
                    	recurSchedule = ExtendedCronExpression.buildExpression(recurHour, null, new int[] {recurYearlyWeekMonthOfYear}, new int[] {recurYearlyDayOfWeek}, recurYearlyWeekOfMonth);
                        break;
                    default:
                        messages.addMessage("warn", "usalive-payor-edit-schedule-invalid", "The recurring schedule is not valid. Please select the schedule again");
                        RequestUtils.redirectWithCarryOver(request, response, "payor_update.html?payorId=" + payorId + "&action=edit");
                        return;
                }
                break;
			case 'M': // monthly
				Character recurMonthlySubType = RequestUtils.getAttribute(request, "recurMonthlySubType", Character.class, true);
				switch(recurMonthlySubType) {
					case 'N':
						int[] recurMonthlyDaysOfMonth = RequestUtils.getAttribute(request, "recurMonthlyDaysOfMonth", int[].class, true);
						int[] recurMonthlyDayMonths = RequestUtils.getAttribute(request, "recurMonthlyDayMonths", int[].class, false);
						recurSchedule = ExtendedCronExpression.buildExpression(recurHour, recurMonthlyDaysOfMonth, recurMonthlyDayMonths, null, 0);
		                break;
					case 'K':
						int recurMonthlyWeekOfMonth = RequestUtils.getAttribute(request, "recurMonthlyWeekOfMonth", Integer.class, true);
						int[] recurMonthlyDayOfWeek = RequestUtils.getAttribute(request, "recurMonthlyDayOfWeek", int[].class, true);
						int[] recurMonthlyWeekMonths = RequestUtils.getAttribute(request, "recurMonthlyWeekMonths", int[].class, false);
                        recurSchedule = ExtendedCronExpression.buildExpression(recurHour, null, recurMonthlyWeekMonths, recurMonthlyDayOfWeek, recurMonthlyWeekOfMonth);
		                break;
					default:
						messages.addMessage("warn", "usalive-payor-edit-schedule-invalid", "The recurring schedule is not valid. Please select the schedule again");
						RequestUtils.redirectWithCarryOver(request, response, "payor_update.html?payorId=" + payorId + "&action=edit");
					    return;
				}
			    break;
			case 'W': // weekly
			    int[] recurWeeklyDayOfWeek = RequestUtils.getAttribute(request, "recurWeeklyDayOfWeek", int[].class, true);
				recurSchedule = ExtendedCronExpression.buildExpression(recurHour, null, null, recurWeeklyDayOfWeek, 0);
                break;
			case 'D': //daily
				recurSchedule = ExtendedCronExpression.buildExpression(recurHour, null, null, null, 0);
			    break;
			default:
				messages.addMessage("warn", "usalive-payor-edit-schedule-invalid", "The recurring schedule is not valid. Please select the schedule again");
                RequestUtils.redirectWithCarryOver(request, response, "payor_update.html?payorId=" + payorId + "&action=edit");
                return;
		}
	}
	BigDecimal recurAmount = RequestUtils.getAttribute(request, "recurAmount", BigDecimal.class, false);
    String recurTimeZone = RequestUtils.getAttribute(request, "recurTimeZone", String.class, false);
	Date recurStartTs = RequestUtils.getAttribute(request, "recurStartTs", Date.class, false);
	Date recurEndTs = RequestUtils.getAttribute(request, "recurEndTs", Date.class, false);
	int recurDescFormatCount = RequestUtils.getAttributeDefault(request, "recurDescFormat.count", int.class, 0);
	String recurDescFormat; // 0 = date; 1 = installment
	if(recurDescFormatCount > 0) {
		StringBuilder sb = new StringBuilder();
		for(int i = 1; i <= recurDescFormatCount; i++) {
			String part = RequestUtils.getAttribute(request, "recurDescFormat." + i, String.class, false);
			if(StringUtils.isBlank(part))
				continue;
			if((i%2) == 1)
				part = MessageFormat.escape(part);
			sb.append(part);
		}
		recurDescFormat = sb.toString();
	} else
		   recurDescFormat = RequestUtils.getAttribute(request, "recurDescFormat", String.class, false);
    if(!StringUtils.isBlank(recurDescFormat) && ConvertUtils.getFormat("MESSAGE", recurDescFormat) == null) {
    	messages.addMessage("warn", "usalive-payor-edit-recur-desc-invalid", "The recurring description is not valid. Please enter it again");
    } else if(replace != null && replace.booleanValue()) {
		InputForm form = RequestUtils.getInputForm(request);
	    String cardNumber = form.getString("card", true).replaceAll("\\s+", ""); 
	    int expMonth = form.getInt("expMonth", true, 0);
	    int expYear = form.getInt("expYear", true, 0);
	    String securityCode = form.getString("cvv", true).trim();
	    String billingPostal = form.getString("postal", true).trim();
	    Long newPayorId = PayorUtils.replacePayor(user, payorId, payorName, payorEmail, bankAcctId, cardNumber, expMonth, expYear, securityCode, billingPostal, recurAmount, recurSchedule, recurTimeZone, recurStartTs, recurEndTs, recurDescFormat, messages);
	    if(newPayorId != null)
	    	payorId = newPayorId;
	    else
	    	RequestUtils.storeAllParamsForNextRequest(request);
	} else if(fullUpdate)
	    PayorUtils.editPayor(user, payorId, payorName, payorEmail, bankAcctId, recurAmount, recurSchedule, recurTimeZone, recurStartTs, recurEndTs, recurDescFormat, messages);
	else
		PayorUtils.editPayorRecur(user, payorId, payorEmail, recurAmount, recurSchedule, recurTimeZone, recurStartTs, recurEndTs, recurDescFormat, messages);
}
RequestUtils.redirectWithCarryOver(request, response, "payor_update.html?payorId=" + payorId + "&action=edit");
%>