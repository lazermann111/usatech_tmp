<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.NotAuthorizedException"%>
<%@page import="com.usatech.layers.common.fees.LicenseFees"%>
<%@page import="com.usatech.layers.common.fees.Fees.Fee"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.results.Results"%>
<%@page import="com.usatech.usalive.servlet.USALiveUtils"%>
<%@page import="simple.servlet.ResponseUtils"%>
<%@page import="com.usatech.report.ReportingUser.ActionType"%>
<%@page import="com.usatech.report.ReportingUser.ResourceType"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%
InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
long customerId = inputForm.getLong("customerId", false, -1);
UsaliveUser user = (UsaliveUser) RequestUtils.getUser(request);
user.checkPermission(ResourceType.CUSTOMER, ActionType.EDIT, customerId);
if (user.getCustomerId() == customerId)
  throw new NotAuthorizedException("User '" + user.getUserName() + "' is not permitted to edit this");
Results customerInfoResults = DataLayerMgr.executeQuery("GET_CUSTOMER_INFO", new Object[] {customerId});
if (!customerInfoResults.next())
  throw new NotAuthorizedException("User '" + user.getUserName() + "' is not permitted to edit this");
String customerName = customerInfoResults.getFormattedValue("customerName");
LicenseFees fees = LicenseFees.loadByCustomerId(customerId);
fees.setShowZerosAsBlank(false);
String action = inputForm.getString("action", false);
if (action != null && action.equalsIgnoreCase("Save Changes")) {
  fees.updateFromForm(inputForm, false);
  if (fees.isDirty()) {
    fees.saveToDatabase();
    RequestUtils.getOrCreateMessagesSource(inputForm).addMessage("success", "update-customer-fees-success", "Customer fees updated successfully.");
  }
}
%>
<jsp:include page="include/header.jsp"/>
<div class="tableDataContainer">
<form name="customerFees" method="post" action="customer_fees.html" onsubmit="return validateUSATForm(this);">
<script src="/scripts/fees.js"></script>
<script type="text/javascript">
//<![CDATA[
	window.addEvent("domready", function() {
		new Form.Validator.Inline.Mask(document.customerFees);
	});
//]]>
</script>
<input type="hidden" name="customerId" value="<%=customerId%>" />
<%
ResponseUtils.writeXsfrFormProtection(pageContext);
USALiveUtils.writeFormProfileId(pageContext);
String errorMessage = inputForm.getString("errorMessage", false);
if (!StringHelper.isBlank(errorMessage)) {%>
<div class="message-error"><%=StringUtils.prepareHTML(errorMessage)%><br/></div>
<%}%>
    
<table class="fields">
  <tr class="tableDataShade">
    <th colspan="2" class="terminalHdr">Customer Fees - <%=StringUtils.prepareHTML(customerName)%></th>
  </tr>
  <tr>
    <td colspan="2">
      <div class="spacer5"></div>
    </td>
  </tr>
  <tr class="tableDataShade">
    <td colspan="2">
    <% fees.writeHtmlTable(pageContext, true); %>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <div class="spacer5"></div>
    </td>
  </tr>
  <tr>
    <td colspan="2" align="left"><span class="required">* Required Fields</span></td>
  </tr>
  <tr>
    <td colspan="2" align="left"><span class="baNote">* Customer fee changes apply to new device activations only.</span></td>
  </tr>
  <tr>
    <td colspan="2" style="text-align:center">
      <div class="spacer5"></div>
      <input type="submit" class="cssButton" id="action" name="action" value="Save Changes"/>
      <input type="button" class="cssButton" value="Customers" onClick="window.location = '/customer_admin_search.i'" />
    </td>
  </tr>
</table>
</form>
</div>
<jsp:include page="include/footer.jsp"/>
