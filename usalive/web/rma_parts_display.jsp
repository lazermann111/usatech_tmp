<%@page import="com.usatech.usalive.web.RMAType"%>
<%@page import="java.sql.Connection"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="com.usatech.usalive.web.RMAUtils"%>
<%
	UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
	boolean hasPri=false;
	if((user.isInternal()&&user.hasPrivilege(ReportingPrivilege.PRIV_CUSTOMER_SERVICE))||user.hasPrivilege(ReportingPrivilege.PRIV_RMA)){
		hasPri=true;
	}
	String rmaNumber=null;
	InputForm inputForm = RequestUtils.getInputForm(request);
	Results rmaPartsResults=null;
	Results rmaInfoResult=null;
	String rmaCreateTs=null;
	String rmaDescription=null;
	int rmaReplacementQuantity=0;
	String carrierInfo=null;
	String rmaType=null;
	//@Todo test
	//inputForm.set("rmaId", 3);
	if(hasPri){
		Connection conn=(Connection)request.getAttribute("RMA_DB_CONN");
		if(conn==null){
			rmaInfoResult =  DataLayerMgr.executeQuery("GET_RMA_INFO", inputForm);
		}else{
			rmaInfoResult =  DataLayerMgr.executeQuery(conn,"GET_RMA_INFO", inputForm);
		}
		if(rmaInfoResult.next()){
			rmaNumber=rmaInfoResult.getFormattedValue("rmaNumber");
			request.setAttribute("rmaNumber", rmaNumber);
			rmaCreateTs=RMAUtils.rmaDateFormat.format(ConvertUtils.convert(Date.class, rmaInfoResult.get("rmaCreateTs")));
			rmaDescription=rmaInfoResult.getFormattedValue("rmaDescription");
			rmaReplacementQuantity=ConvertUtils.getInt(rmaInfoResult.get("rmaReplacementQuantity"));
			String carrierDescription=rmaInfoResult.getFormattedValue("carrierDescription");
			String carrierAccountNumber=rmaInfoResult.getFormattedValue("carrierAccountNumber");
			StringBuilder sb=new StringBuilder(rmaInfoResult.getFormattedValue("carrier"));
			
			if(!StringUtils.isBlank(carrierAccountNumber)){
				sb.append(" Account #: ").append(carrierAccountNumber);
			}
			if(!StringUtils.isBlank(carrierDescription)){
				sb.append(" (").append(carrierDescription+")");
			}
			carrierInfo=sb.toString();
			
			rmaType=RMAType.getByValue(ConvertUtils.getInt(rmaInfoResult.get("rmaTypeId"))).getDescription();
		}
		if(conn==null){
			rmaPartsResults =  DataLayerMgr.executeQuery("GET_RMA_PARTS", inputForm);
		}else{
			rmaPartsResults =  DataLayerMgr.executeQuery(conn,"GET_RMA_PARTS", inputForm);
		}
	}
%>
<%if(hasPri){ %>
<div class="instruct center">USA Technologies Return Material Authorization (RMA) Request Form:</div> 
<BR/>
<table class="folio" border="1">
<tr class="headerRowRMA"><th> RMA # Assigned: <%=StringUtils.prepareHTML(rmaNumber)%></th><th></th></tr>
<tr><td>Date of Request:</td><td><%=StringUtils.prepareHTML(rmaCreateTs)%></td></tr>
<tr><td>USALive User:</td><td><%=StringUtils.prepareHTML(rmaInfoResult.getFormattedValue("rmaUserName"))%></td></tr>
<tr><td>USALive Customer:</td><td><%=StringUtils.prepareHTML(rmaInfoResult.getFormattedValue("rmaCustomerName"))%></td></tr>
<tr><td>RMA Type:</td><td><%=StringUtils.prepareHTML(rmaType) %></td></tr>
<tr><td>Replacement Kit Quantity:</td><td><%=rmaReplacementQuantity%></td></tr>
<tr><td>RMA Description:</td><td> <%=StringUtils.prepareHTML(rmaDescription)%></td></tr>
</table>
<br/>
<table class="folio" border="1">
<tr class="headerRowRMA"><th colspan="3">Miscellaneous Parts:</th>
<tr class="headerRowRMA"><th>Quantity</th><th>Part Number</th><th>Description of issue</th></tr>
<% while(rmaPartsResults!=null&& rmaPartsResults.next()) {
				    %>
				    <tr>
				    <td><%=StringUtils.prepareHTML(rmaPartsResults.getFormattedValue("quantity")) %></td>
				    <td><%=StringUtils.prepareHTML(rmaPartsResults.getFormattedValue("partNumber")) %></td>
				    <td><%=StringUtils.prepareHTML(rmaPartsResults.getFormattedValue("issue")) %></td>
				    </tr>
				    <%
				}
				%>
</table>
<br/>
<br/>
<table class="folio" border="1">
<tr class="headerRowRMA"><th colspan=2> Shipping Info:</th></tr>
<tr><td>Replacement Shipping Carrier:</td><td><%=StringUtils.prepareHTML(carrierInfo)%></td></tr>
<tr><td>Contact Name:</td><td><%=StringUtils.prepareHTML(rmaInfoResult.getFormattedValue("contactName"))%></td></tr>
<tr><td>Address:</td><td><%=StringUtils.prepareHTML(rmaInfoResult.getFormattedValue("address"))%></td></tr>
<tr><td>City:</td><td><%=StringUtils.prepareHTML(rmaInfoResult.getFormattedValue("city"))%></td></tr>
<tr><td>State:</td><td><%=StringUtils.prepareHTML(rmaInfoResult.getFormattedValue("state"))%></td></tr>
<tr><td>Postal Code:</td><td><%=StringUtils.prepareHTML(rmaInfoResult.getFormattedValue("postalCd"))%></td></tr>
<tr><td>Country:</td><td><%=StringUtils.prepareHTML(rmaInfoResult.getFormattedValue("countryCd"))%></td></tr>
<tr><td>Email:</td><td><%=StringUtils.prepareHTML(rmaInfoResult.getFormattedValue("email"))%></td></tr>
<tr><td>Phone:</td><td><%=StringUtils.prepareHTML(rmaInfoResult.getFormattedValue("phone"))%></td></tr>
</table>
<jsp:include page="include/rmaBottom.jsp"/>
<%}else{%>
	<p class="not-authorized">User '<%=StringUtils.prepareHTML(user.getUserName()) %>' does not have permission to view this page. Please contact USA Technologies if you need access.</p>
<%}%>
