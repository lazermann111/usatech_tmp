<%@page import="java.util.Map"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="simple.text.LiteralFormat"%>
<%@page import="simple.text.MessageFormat.FormatObject"%>
<%@page import="simple.text.MessageFormat"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.TimeZone"%>
<%@page import="com.usatech.layers.common.util.ExtendedCronExpression"%>
<%@page import="java.util.Date"%>
<%@page import="com.usatech.usalive.web.PayorUtils"%>
<%@page import="com.usatech.usalive.servlet.USALiveUtils"%>
<%@page import="simple.servlet.ResponseUtils"%>
<%@page import="java.util.Calendar"%>
<%@page import="simple.servlet.NotAuthorizedException"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.results.Results"%>
<%@page import="com.usatech.report.ReportingUser.ActionType"%>
<%@page import="com.usatech.report.ReportingUser.ResourceType"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
if(!user.hasPrivilege(ReportingPrivilege.PRIV_BACK_OFFICE))
    throw new NotAuthorizedException("You are not authorized to perform this function.");
InputForm form = RequestUtils.getInputForm(request);
long payorId = RequestUtils.getAttribute(request, "payorId", Long.class, true);
Results results = DataLayerMgr.executeQuery("GET_PAYOR_INFO", form);
if(!results.next()) {
    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "usalive-payor-invalid-payor", "Invalid Account selected; please try again");
    RequestUtils.redirectWithCarryOver(request, response, "payor_config.html");
    return;
}
long bankAcctId = results.getValue("bankAcctId", Long.class);
user.checkPermission(ResourceType.BANK_ACCTS, ActionType.VIEW, bankAcctId);
String action = RequestUtils.getAttribute(request, "action", String.class, true);
Long lastChargeTime = results.getValue("lastChargeTs", Long.class);
if("remove".equalsIgnoreCase(action)) {%>
<jsp:include page="include/header.jsp"/>
<div class="caption">ePort Online: Remove Account</div>
<div class="spacer10"></div>
<form class="payor-form" method="post" action="payor_remove.html">
<%
ResponseUtils.writeXsfrFormProtection(pageContext);
USALiveUtils.writeFormProfileId(pageContext);
%>
Are you sure you want to remove Account <%=StringUtils.prepareHTML(results.getFormattedValue("payorName"))%>?
<input type="hidden" name="payorId" value="<%=payorId%>"/>
<button type="button" onclick="window.location = 'payor_config.html'">Cancel</button>
<button type="submit">Remove Account</button>
</form>
<jsp:include page="include/footer.jsp"/><%
return;
}
if(!"charge".equalsIgnoreCase(action) && !"edit".equalsIgnoreCase(action)) {
    RequestUtils.getOrCreateMessagesSource(request).addMessage("error", "usalive-payor-invalid-action", "Invalid action specified; Action ignored", form);
    RequestUtils.redirectWithCarryOver(request, response, "payor_config.html");
    return;
}
Results timeZoneResults = DataLayerMgr.executeQuery("GET_RECUR_TIME_ZONES", form);   
Locale locale = RequestUtils.getLocale(request);
java.text.DateFormatSymbols dfs = new java.text.DateFormatSymbols(locale);
String[] weekdays = dfs.getWeekdays();
String[] months = dfs.getMonths();
String recurSchedule = results.getFormattedValue("recurSchedule");
String recurTimeZone = results.getFormattedValue("recurTimeZone");
TimeZone recurTimeZoneObj;
if(StringUtils.isBlank(recurTimeZone)) {
    recurTimeZoneObj = TimeZone.getDefault();
    recurTimeZone = recurTimeZoneObj.getID();
} else {
    recurTimeZoneObj = TimeZone.getTimeZone(recurTimeZone);
}
Calendar today = Calendar.getInstance(recurTimeZoneObj, locale);
Date recurStartTs = results.getValue("recurStartTs", Date.class);
Date recurEndTs = results.getValue("recurEndTs", Date.class);
Date recurNextTs = results.getValue("recurNextTs", Date.class);
Date recurExceptionTs = results.getValue("recurExceptionTs", Date.class);
int retryCount=ConvertUtils.getIntSafely(results.get("recurRetryCount"),0);
Date currTs = today.getTime();
if(recurStartTs == null)
    recurStartTs = currTs;
char recurType;
int[] recurWeeklyDayOfWeek = new int[] {today.get(Calendar.DAY_OF_WEEK)};
char recurMonthlySubType = 'N';
int[] recurMonthlyDaysOfMonth = new int[] {today.get(Calendar.DAY_OF_MONTH)};
String recurMonthlyDayMonths = "";
int recurMonthlyWeekOfMonth = today.get(Calendar.WEEK_OF_MONTH);
int recurMonthlyDayOfWeek = today.get(Calendar.DAY_OF_WEEK);
String recurMonthlyWeekMonths = "";
char recurYearlySubType = 'D';
int recurYearlyDayMonthOfYear = today.get(Calendar.MONTH);
int recurYearlyDayOfMonth = today.get(Calendar.DAY_OF_MONTH);
int recurYearlyWeekMonthOfYear = today.get(Calendar.MONTH);
int recurYearlyWeekOfMonth = today.get(Calendar.WEEK_OF_MONTH);
int recurYearlyDayOfWeek = today.get(Calendar.DAY_OF_WEEK);
int recurHour = today.get(Calendar.HOUR_OF_DAY);
if(!StringUtils.isBlank(recurSchedule)) {
    ExtendedCronExpression ece = new ExtendedCronExpression(recurSchedule);
    int[] monthsOfYear = ece.getMonths();
    int[] daysOfWeek = ece.getDaysOfWeek();
    int[] daysOfMonth = ece.getDaysOfMonth();
    int weekOfMonth = ece.getWeekOfMonth();
    int[] hoursOfDay = ece.getHours();
    if(monthsOfYear != null && monthsOfYear.length > 0 && monthsOfYear.length < 12) {
        recurYearlyDayMonthOfYear = monthsOfYear[0];
        recurYearlyWeekMonthOfYear = monthsOfYear[0];
        recurMonthlyDayMonths = ConvertUtils.getString(monthsOfYear, false);
        recurMonthlyWeekMonths = recurMonthlyDayMonths;
        if(monthsOfYear.length == 1)
            recurType = 'Y';
        else
            recurType = 'M';
    } else if(weekOfMonth > 0 || (daysOfMonth != null && daysOfMonth.length > 0 && daysOfMonth.length < 31))
        recurType = 'M';
    else if(daysOfWeek != null && daysOfWeek.length > 0)
        recurType = 'W';
    else 
        recurType = 'D';
    if(daysOfWeek != null && daysOfWeek.length > 0) {
        recurWeeklyDayOfWeek = daysOfWeek;
        recurMonthlyDayOfWeek = daysOfWeek[0];
        recurYearlyDayOfWeek = daysOfWeek[0];   
    }
    if(daysOfMonth != null && daysOfMonth.length > 0 && daysOfMonth.length < 31) {
        recurMonthlyDaysOfMonth = daysOfMonth;
        recurYearlyDayOfMonth = daysOfMonth[0];
    }
    if(weekOfMonth != 0) {
        recurMonthlyWeekOfMonth = weekOfMonth;
        recurYearlyWeekOfMonth = weekOfMonth;
        recurMonthlySubType = 'K';
        recurYearlySubType = 'T';
    }
    
    if(hoursOfDay == null || hoursOfDay.length < 1)
        recurHour = today.get(Calendar.HOUR_OF_DAY);
    else
        recurHour = hoursOfDay[0];
} else
    recurType = '-';

char status = results.getValue("status", Character.class);
if("charge".equalsIgnoreCase(action)) {
	String payTo = results.getFormattedValue("bankName") + " - #" + results.getFormattedValue("accountNumber") + " - " + results.getFormattedValue("accountTitle") + " (" + results.getFormattedValue("currencyCd") + ")";
    if(lastChargeTime == null)
        lastChargeTime = 0L;
    
%>
    <jsp:include page="include/header.jsp"/>
    <div class="caption">ePort Online: Charge Account</div>
    <div class="spacer10"></div>
    <form class="payor-form" method="post" action="payor_charge.html" data-toggle="validateInline">
    <input type="hidden" name="lastChargeTime" value="<%=lastChargeTime%>"/>
    <input type="hidden" name="full" value="false"/>
<%
ResponseUtils.writeXsfrFormProtection(pageContext);
USALiveUtils.writeFormProfileId(pageContext);
PayorUtils.writeReferrerInputs(pageContext, request);
%>    <fieldset><legend>Charge Account</legend>
    <table>
    <tr><td><label>Account Name:</label></td><td class="label"><%=StringUtils.prepareHTML(results.getFormattedValue("payorName"))%></td></tr>
    <tr><td><label>Pay To Bank Account:</label></td><td class="label"><%=StringUtils.prepareHTML(payTo)%></td></tr>
    <tr><td><label>Card Number:</label></td><td class="label"><%=StringUtils.prepareHTML(results.getFormattedValue("cardNumber"))%></td></tr>
<% } else { //if("edit".equalsIgnoreCase(action)) {
	Results bankAcctResults = DataLayerMgr.executeQuery("GET_ACTIVE_BANK_ACCTS", form);
%>
    <jsp:include page="include/header.jsp"/>
    <%
    if(bankAcctResults.isGroupEnding(0)) {
    %>You have no active bank accounts. Please set up a bank account or ask your company administrator to grant you access to an existing bank account.
	    <jsp:include page="include/footer.jsp"/><%
	    return;
	}
	String payorName = RequestUtils.getAttribute(request, "payorName", String.class, false);
	if(StringUtils.isBlank(payorName))
		payorName = results.getFormattedValue("payorName");
	String payorEmail = RequestUtils.getAttribute(request, "payorEmail", String.class, false);
	if(StringUtils.isBlank(payorEmail))
	   	payorEmail = results.getFormattedValue("payorEmail");
%>
    <div class="caption">ePort Online: Edit <%if(status == '1') {%>One-Time <%} %>Account</div>
    <div class="spacer10"></div>
    <form class="payor-form" method="post" action="payor_edit.html" data-toggle="validateInline" data-detectchanges="true">
<%
	ResponseUtils.writeXsfrFormProtection(pageContext);
	USALiveUtils.writeFormProfileId(pageContext);
	PayorUtils.writeReferrerInputs(pageContext, request);
%>
    <fieldset><legend>Edit Account</legend>
    <table>
    <tr><td><label>Account Name:<span class="required">*</span></label></td><td><input type="text" name="payorName" data-validators="required" placeholder="The name of the payor" title="Enter the name of the payor that will identify the person or business paying" maxlength="50" value="<%=StringUtils.prepareCDATA(payorName)%>"/></td></tr>
    <tr><td><label>Account Contact Email:</label></td><td><input type="email" name="payorEmail" data-validators="validate-email" placeholder="The email of the payor" title="Enter the email of the payor" maxlength="60" value="<%=StringUtils.prepareCDATA(payorEmail)%>"/></td></tr>
    <tr><td><label>Pay To Bank Account:<span class="required">*</span></label></td><td><select name="bankAcctId" data-validators="required"><%
    long selectedBankAcctId = ConvertUtils.getLongSafely(RequestUtils.getAttribute(request, "bankAcctId", false), bankAcctId);
    while(bankAcctResults.next()) {
        long acctId = bankAcctResults.getValue("bankAcctId", Long.class);
        //String country = bankAcctResults.getValue("bankCountryCd", String.class);
        %><option value="<%=acctId %>"<%if(selectedBankAcctId == acctId) { %> selected="selected"<%} %>>
        <%=StringUtils.prepareHTML(bankAcctResults.getFormattedValue("bankName"))%>
            - #<%=StringUtils.prepareHTML(bankAcctResults.getFormattedValue("accountNumber"))%>
            - <%=StringUtils.prepareHTML(bankAcctResults.getFormattedValue("accountTitle"))%>
         (<%=StringUtils.prepareHTML(bankAcctResults.getValue("currencyCd", String.class))%>) </option><%
    }
    if(selectedBankAcctId == 0L) {
        %><option value="" selected="selected">-- None --</option><%
    }   
    String card = RequestUtils.getAttribute(request, "card", String.class, false);
    boolean changeCard = !StringUtils.isBlank(card);
    %></select></td></tr>
    <tr><td colspan="2"><input type="checkbox" name="changeCard" value="true" onclick="$('cardFields').disabled=!this.checked;"<%if(changeCard) {%> checked="checked"<%} %>/> <%if(status == '1') {%>Save For Re-use<% } else { %>Change Card<%} %>
    <fieldset id="cardFields" <%if(!changeCard) {%> disabled="disabled"<%} %>><table>
        <tr><td><label>Card Number:<span class="required">*</span></label></td><td><input type="text" name="card" data-validators="required validate-card" placeholder="The credit card number" title="Enter the full card number that will be charged" maxlength="19" autocomplete="off" value="<%=StringUtils.prepareCDATA(card)%>"/></td></tr>   
    <tr><td><label>Card Expiration:<span class="required">*</span></label></td><td>
    <fieldset class="no-border-grouper" data-validators="required" >
    <select name="expMonth" class="autosize" ><%
	int month = ConvertUtils.getIntSafely(RequestUtils.getAttribute(request, "expMonth", false), 0);
	int year = ConvertUtils.getIntSafely(RequestUtils.getAttribute(request, "expYear", false), 0);
	Calendar now = Calendar.getInstance();
	int currYear = now.get(Calendar.YEAR);%>
	<option value=""<%if(month < 1 || month > 12) {%> selected="selected"<%} %>>Month</option><%
	for(int m = 1; m <= 12; m++) {%>
	    <option<%if(month == m) {%> selected="selected"<%} %> value="<%=m%>"><%=StringUtils.pad(Integer.toString(m), '0', 2, StringUtils.Justification.RIGHT)%></option><%
	}%></select>
	<select name="expYear" class="autosize">
	<option value=""<%if(year < currYear) {%> selected="selected"<%} %>>Year</option><%
	for(int y = currYear; y <= currYear + 10; y++) {%>
	    <option<%if(year == y) {%> selected="selected"<%} %> value="<%=y%>"><%=y%></option><%
	}%></select>
    </fieldset></td></tr>
    <tr><td><label>Card Security Code:<span class="required">*</span></label></td><td><input type="password" name="cvv" data-validators="required validate-cvv" placeholder="3 or 4 digit security code" title="Enter the 3 or 4 digit security code found on the back of the customer's card" maxlength="4" autocomplete="off" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "cvv", String.class, false))%>"/></td></tr>   
    <tr><td><label>Card Postal Code:<span class="required">*</span></label></td><td><input type="text" name="postal" data-validators="required" placeholder="Postal code of the card billing address" title="Enter the postal code of the billing address for the card that will be charged" maxlength="10" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "postal", String.class, false))%>"/></td></tr>
    </table></fieldset>
    </td></tr>
<%} //end of "edit"%>Last
	<%if(retryCount>0){%>
	<tr><td>Next Scheduled Date:</td><td><div class="form-info"><%=ConvertUtils.formatObject(recurNextTs, "DATETZ:" + recurTimeZone + ";MM/dd/yyyy h a", locale) %><%if(retryCount<5){ %><div style="display:inline;color:#CCCC00"> - Failing. Please update your credit card or wait for retry.</div><%}else{ %><div style="display:inline;color:red"> - Failed. Please update your credit card.</div><%} %></div></td></tr>
	<tr><td>Last Attempted Time:</td><td><div class="form-info"><%=ConvertUtils.formatObject(recurExceptionTs, "DATETZ:" + recurTimeZone + ";MM/dd/yyyy hh:mm aaa", locale) %></div></td></tr>
	<tr><td>Last Attempted Error:</td><td><div class="form-info"><% if(StringUtils.isBlank(results.getFormattedValue("recurErrorMsg"))){%>No details<%}else{ %><%=StringUtils.prepareHTML(results.getFormattedValue("recurErrorMsg"))%><%} %></div></td></tr>
	<%} %>
    <tr><td>Last Charge:</td><td><div class="form-info"><%
    if(lastChargeTime != null && lastChargeTime.longValue() > 0) {
        %><%=StringUtils.prepareHTML(results.getFormattedValue("lastChargeAmount")) %> <% 
        String recurLastTs = results.getFormattedValue("recurLastTs");
        String lastChargeTs = results.getFormattedValue("lastChargeTs");
        if("Y".equals(results.getFormattedValue("lastChargeRecurFlag")) && !StringUtils.isBlank(recurLastTs) && !recurLastTs.equals(lastChargeTs)) { %>for <%=StringUtils.prepareHTML(recurLastTs) %> (on <%=StringUtils.prepareHTML(lastChargeTs) %>)<%
        } else {%>on <%=StringUtils.prepareHTML(lastChargeTs) %><%} 
    } else {%>Never<%} %></div></td></tr>
    <tr><td><label>Recurring Charge:</label></td><td class="recur-detail"><select name="recurType"><%
    if("charge".equalsIgnoreCase(action)) { %>
        <option value="*" selected="selected" onselect="handleRecurOptionChange(null, true);"><%if(recurType != '-') {%>Manual override<%} else { %>None<%} %></option><%
    } else {%>        
        <option value="" <%if(recurType == '-') {%>selected="selected" <%}%>onselect="handleRecurOptionChange();">None</option><%
    }%>
        <option value="Y" <%if(!"charge".equalsIgnoreCase(action) && recurType == 'Y') {%>selected="selected" <%}%>onselect="handleRecurOptionChange('recurYearlyOptions');">Yearly</option>
        <option value="M" <%if(!"charge".equalsIgnoreCase(action) && recurType == 'M') {%>selected="selected" <%}%>onselect="handleRecurOptionChange('recurMonthlyOptions');">Monthly</option>
        <option value="W" <%if(!"charge".equalsIgnoreCase(action) && recurType == 'W') {%>selected="selected" <%}%>onselect="handleRecurOptionChange('recurWeeklyOptions');">Weekly</option>
        <option value="D" <%if(!"charge".equalsIgnoreCase(action) && recurType == 'D') {%>selected="selected" <%}%>onselect="handleRecurOptionChange('recurOptions');">Daily</option>
        </select></td></tr>
        <%
    if("charge".equalsIgnoreCase(action)) {%>
        <tbody id="recurOverrideOptions">
		    <tr><td><label>Amount:<span class="required">*</span></label></td><td><input type="text" name="chargeAmount" data-validators="required validate-regex:'^([1-9]{1}[0-9]*|0)(\.[0-9]{0,2})?$' maximum:100000" placeholder="Amount to charge" title="Enter the amount to charge the payor" maxlength="15" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "chargeAmount", String.class, false))%>"/></td></tr>
		    <tr><td><label>Invoice #:</label></td><td><input type="text" name="chargeReference" placeholder="An invoice number for this charge" title="Enter an invoice number for this charge" maxlength="60" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "chargeReference", String.class, false))%>"/></td></tr>
		    <tr><td><label>Description:</label></td><td><input type="text" name="chargeDesc" placeholder="The description of this charge" title="Enter the description of this charge" maxlength="200" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "chargeDesc", String.class, false))%>"/></td></tr>        
        </tbody><%
        } %>
        <tr><td></td><td><fieldset id="recurOptions"><legend>Recurring Options</legend>
        <div id="recurAmountOptions">
        <label>Amount:<span class="required">*</span></label><input type="text" name="recurAmount" data-validators="required validate-regex:'^([1-9]{1}[0-9]*|0)(\.[0-9]{0,2})?$' maximum:100000" placeholder="Amount to charge" title="Enter the amount to charge the payor" maxlength="15" value="<%=results.getFormattedValue("recurAmount")%>"/>
        </div>
        <fieldset class="no-border-grouper<%if(recurType != 'W') {%> hide<%} %>" data-validators="validate-reqchk-bynode" id="recurWeeklyOptions"><%
        for(int i = 1; i < weekdays.length; i++) {
            %><label><input type="checkbox" name="recurWeeklyDayOfWeek" value="<%=i%>"<%if(Arrays.binarySearch(recurWeeklyDayOfWeek, i) >= 0) {%> checked="checked"<%}%>/><%=StringUtils.prepareHTML(weekdays[i]) %></label><%
        }%>        
        </fieldset>
        <div id="recurMonthlyOptions"<%if(recurType != 'M') {%> class="hide"<%} %>>
        <label><input type="radio" name="recurMonthlySubType" value="N"<%if(recurMonthlySubType == 'N') {%> checked="checked"<%}%>>On Day(s) 
        <input type="text" name="recurMonthlyDaysOfMonth" size="4" data-validators="required validate-regex:'^\\s*(([1-2]\\d|3[0-1]|[1-9])\\s*,\\s*)*([1-2]\\d|3[0-1]|[1-9])\\s*$'" value="<%=ConvertUtils.getStringSafely(recurMonthlyDaysOfMonth)%>"/> of 
        <select name="recurMonthlyDayMonths">
            <option value=""<%if(StringUtils.isBlank(recurMonthlyDayMonths)) {%> selected="selected"<%}%>>every month</option>
            <option value="1,4,7,10"<%if("1,4,7,10".equals(recurMonthlyDayMonths)) {%> selected="selected"<%}%>><%
            	    for(int i = 0; i < 12; i+=3) { 
            	    	if(i > 0) out.print(", ");
            	    	out.print(months[i]);
            	    }%></option>
            <option value="1,7"<%if("1,7".equals(recurMonthlyDayMonths)) {%> selected="selected"<%}%>><%
                    for(int i = 0; i < 12; i+=6) { 
                        if(i > 0) out.print(", ");
                        out.print(months[i]);
                    }%></option>
        </select></label><br/>
        <label><input type="radio" name="recurMonthlySubType" value="K"<%if(recurMonthlySubType == 'K') {%> checked="checked"<%}%>>On the <select name="recurMonthlyWeekOfMonth">
            <option value="1"<%if(recurMonthlyWeekOfMonth == 1) {%> selected="selected"<%}%>>First</option>
            <option value="2"<%if(recurMonthlyWeekOfMonth == 2) {%> selected="selected"<%}%>>Second</option>
            <option value="3"<%if(recurMonthlyWeekOfMonth == 3) {%> selected="selected"<%}%>>Third</option>
            <option value="4"<%if(recurMonthlyWeekOfMonth == 4) {%> selected="selected"<%}%>>Fourth</option>
            <option value="-1"<%if(recurMonthlyWeekOfMonth == -1 || recurMonthlyWeekOfMonth > 4) {%> selected="selected"<%}%>>Last</option></select>
        <select name="recurMonthlyDayOfWeek"><%
        for(int i = 1; i < weekdays.length; i++) {
            %><option value="<%=i%>"<%if(recurMonthlyDayOfWeek == i) {%> selected="selected"<%}%>><%=StringUtils.prepareHTML(weekdays[i]) %></option><%
        }%></select> of <select name="recurMonthlyWeekMonths">
            <option value=""<%if(StringUtils.isBlank(recurMonthlyWeekMonths)) {%> selected="selected"<%}%>>every month</option>
            <option value="1,4,7,10"<%if("1,4,7,10".equals(recurMonthlyWeekMonths)) {%> selected="selected"<%}%>><%
                    for(int i = 0; i < 12; i+=3) { 
                        if(i > 0) out.print(", ");
                        out.print(months[i]);
                    }%></option>
            <option value="1,7"<%if("1,7".equals(recurMonthlyWeekMonths)) {%> selected="selected"<%}%>><%
                    for(int i = 0; i < 12; i+=6) { 
                        if(i > 0) out.print(", ");
                        out.print(months[i]);
                    }%></option>
        </select></label>
        </div>
        <div id="recurYearlyOptions"<%if(recurType != 'Y') {%> class="hide"<%} %>>
        <input type="radio" name="recurYearlySubType" value="D"<%if(recurYearlySubType == 'D') {%> checked="checked"<%}%>>On <select name="recurYearlyDayMonthOfYear"><%
        for(int i = 0; i < months.length; i++) {
        	if(StringUtils.isBlank(months[i])) continue;
            %><option value="<%=i+1%>"<%if(recurYearlyDayMonthOfYear == i+1) {%> selected="selected"<%}%>><%=StringUtils.prepareHTML(months[i]) %></option><%
        }%></select> <input type="text" name="recurYearlyDayOfMonth" size="2" value="<%=recurYearlyDayOfMonth%>" data-validators="required minimum:1 maximum:31"/><br/>
        <input type="radio" name="recurYearlySubType" value="T"<%if(recurYearlySubType == 'T') {%> checked="checked"<%}%>>On the <select name="recurYearlyWeekOfMonth">
        <option value="1"<%if(recurYearlyWeekOfMonth == 1) {%> selected="selected"<%}%>>First</option>
            <option value="2"<%if(recurYearlyWeekOfMonth == 2) {%> selected="selected"<%}%>>Second</option>
            <option value="3"<%if(recurYearlyWeekOfMonth == 3) {%> selected="selected"<%}%>>Third</option>
            <option value="4"<%if(recurYearlyWeekOfMonth == 4) {%> selected="selected"<%}%>>Fourth</option>
            <option value="-1"<%if(recurYearlyWeekOfMonth == -1 || recurYearlyWeekOfMonth > 4) {%> selected="selected"<%}%>>Last</option></select>
        <select name="recurYearlyDayOfWeek"><%
        for(int i = 1; i < weekdays.length; i++) {
            %><option value="<%=i%>"<%if(recurYearlyDayOfWeek == i) {%> selected="selected"<%}%>><%=StringUtils.prepareHTML(weekdays[i]) %></option><%
        }%></select> of 
        <select name="recurYearlyWeekMonthOfYear"><%
        for(int i = 0; i < months.length; i++) {
            if(StringUtils.isBlank(months[i])) continue;
            %><option value="<%=i+1%>"<%if(recurYearlyWeekMonthOfYear == i+1) {%> selected="selected"<%}%>><%=StringUtils.prepareHTML(months[i]) %></option><%
        }%></select> 
        </div><div id="recurTimeOptions"><label for="recurHourId">At <select name="recurHour" id="recurHourId"><%
        for(int i = 0; i < 24; i++) {
            %><option value="<%=i%>"<%if(recurHour == i) {%> selected="selected"<%}%>><%
            if(i == 0) {
            	out.print("Midnight"); 
            } else if(i == 12) {
            	out.print("Noon"); 
            } else if(i < 12) { 
            	out.print(i); 
            	out.print(":00 AM"); 
            } else { 
            	out.print(i-12); 
                out.print(":00 PM"); 
            }%></option><%
        }%></select></label><label for="recurStartTsId">From
        <input id="recurCurrTsId" type="hidden" value="<%=ConvertUtils.formatObject(recurStartTs == null || recurStartTs.after(currTs) ? currTs : recurStartTs, "DATE:MM/dd/yyyy")%>"/>
<input id="recurStartTsId" type="text" value="<%=ConvertUtils.formatObject(recurStartTs, "DATE:MM/dd/yyyy")%>" name="recurStartTs" data-format="%m/%d/%Y" data-toggle="calendar" size="10" data-validators="validate-after-date afterElement:'recurCurrTsId' msgPos:'recurStartTs_advice'"/>
<span id="recurStartTs_advice"></span>
</label><label for="recurEndTsId">to
<input id="recurEndTsId" type="text" value="<%=ConvertUtils.formatObject(recurEndTs, "DATE:MM/dd/yyyy")%>" name="recurEndTs" data-format="%m/%d/%Y" data-toggle="calendar" size="10" data-validators="validate-after-date afterElement:'recurStartTsId' msgPos:'recurEndTs_advice'"/>      
<span id="recurEndTs_advice"></span>
</label><br/>
<label>Time Zone <select name="recurTimeZone"><%
while(timeZoneResults.next()) {
    String guid = timeZoneResults.getFormattedValue("timeZoneGuid");
    %><option value="<%=StringUtils.prepareCDATA(guid)%>"<%if(recurTimeZone != null && recurTimeZone.equals(guid)) {%> selected="selected"<%}%>><%=StringUtils.prepareHTML(timeZoneResults.getFormattedValue("timeZoneDesc")) %></option><%
} %>
</select></label></div><%
if(!StringUtils.isBlank(recurSchedule)) {
	%><div id="recurNextList"><label>Next Scheduled Dates:</label><%
	Long recurLastTime = results.getValue("recurLastTs", Long.class);
	if(recurLastTime != null && recurLastTime.longValue() > recurStartTs.getTime())
		recurStartTs = new Date(recurLastTime);
	List<Date> dates = ExtendedCronExpression.getNextDates(recurSchedule, recurTimeZone, recurStartTs, recurEndTs, 6);
	if(dates == null) { %>Invalid<%
	} else if(dates.isEmpty()) { %>None<%
	} else {
		Iterator<Date> iter = dates.iterator();
		for(int i = 0; iter.hasNext() && i < 5; i++) {
			Date date = iter.next();
			if(i > 0)
				out.print(", ");
			out.print(ConvertUtils.formatObject(date, "DATETZ:" + recurTimeZone + ";MM/dd/yyyy h a", locale));
		}
		if(iter.hasNext())
			out.print(", ...");
	}%></div><%
}
%>
<div id="recurDesc">Description:
<%
String recurDescFormat = results.getValue("recurDescFormat", String.class);
%><input type="text" name="recurDescFormat.1" size="100"<%
if(!StringUtils.isBlank(recurDescFormat)) {
	try {
		FormatObject[] fos = new MessageFormat(recurDescFormat).getFormatObjects();
		for(int k = 0; k < fos.length; k++) {
			if(fos[k].format instanceof LiteralFormat) {
				 %> value="<%=StringUtils.prepareCDATA(((LiteralFormat)fos[k].format).toPattern())%>"<%
				break;
			}
		}
	} catch(IllegalArgumentException e) {
	}
}
%>/>
<input type="hidden" name="recurDescFormat.count" value="1"/>
</div>
</fieldset>
        </td></tr>
<%if("charge".equalsIgnoreCase(action)) { %>
    <tr><td><label id="emailLabel">Email Receipt?:</label></td><td><span id="emailPrefix"><input type="checkbox" name="receiptFlag" placeholder="Email a receipt" title="Select to send an email receipt" value="true"<%
    String payorEmail = results.getFormattedValue("payorEmail");
    if(!StringUtils.isBlank(payorEmail)) {%> checked="checked"<%} %>/> to </span><input type="email" name="payorEmail" data-validators="validate-email" placeholder="The email to which a receipt is sent" title="Enter the email to which a receipt will be sent" maxlength="60" value="<%=StringUtils.prepareCDATA(payorEmail)%>"/></td></tr>
    <tr><td colspan="2" data-bluronenter="true" class="center"><button type="submit" id="chargeButton">Charge Account</button></td></tr>
<%} else { %>        
    <tr><td colspan="2" data-bluronenter="true" class="center"><button type="submit">Update Account</button><button type="reset">Cancel Changes</button><button type="button" onclick="window.location.href='payor_config.html'" data-unchanged-only="true">View Saved Accounts</button><%if(status == 'A') {%><button type="button" onclick="window.location.href='payor_update.html?action=charge&payorId=<%=payorId %>'" data-unchanged-only="true">Charge Account</button><%} %></td></tr>
<%} %>    
    </table>
    <input type="hidden" name="payorId" value="<%=payorId%>"/>
    </fieldset>
    </form>
<script type="text/javascript">
var recurOptionIds = ['recurWeeklyOptions', 'recurMonthlyOptions', 'recurYearlyOptions'];
function handleRecurOptionChange(selectedOptionId<% if("charge".equalsIgnoreCase(action)) {%>, override<%}%>) {
	recurOptionIds.each(function(id) { 
		if(id == selectedOptionId) 
			$(id).show();
		else
			$(id).hide();
	});<%
	if("charge".equalsIgnoreCase(action)) {%>
	var chargeButton = $('chargeButton');
	if(override) {
	    $('recurOverrideOptions').show();
	    chargeButton.setText('Charge Account');
	    chargeButton.form.action = 'payor_charge.html';
	    $('recurOptions').disabled = true;
        $('emailLabel').setText('Email Receipt?:');
        $('emailPrefix').show();<%
	    switch(recurType) {
	    	case 'Y': %>
    	$('recurYearlyOptions').show();
    	$('recurOptions').show();<%break;
	    	case 'M': %>
    	$('recurMonthlyOptions').show();
    	$('recurOptions').show();<%break;
	    	case 'W': %>
    	$('recurWeeklyOptions').show();
    	$('recurOptions').show();<%break;
	    	case 'D': %>
	    $('recurOptions').show();<%break;
	        default: %>
	    $('recurOptions').hide();<%
	    }%>	    
    } else {
	    $('recurOverrideOptions').hide();
	    chargeButton.setText('Update Account'); 
        chargeButton.form.action = 'payor_edit.html';
        $('recurOptions').disabled = false;
        if(selectedOptionId)
            $('recurOptions').show();
        else
            $('recurOptions').hide();
        $('emailLabel').setText('Account Contact Email:');
        $('emailPrefix').hide();
	}
	<%} else {%>
	if(selectedOptionId)
        $('recurOptions').show();
    else
        $('recurOptions').hide();<%
    }%>
	if(document.forms['payor-form']) {
		var validator = $(document.forms['payor-form']).retrieve('validator');
		if(validator)
			$('recurOptions').getElements('data-validators').each(function(el) {validator.resetField(el);});
	}
}
</script>    
<jsp:include page="include/footer.jsp"/>
