<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.db.DataLayerMgr"%>
<% InputForm inputForm = RequestUtils.getInputForm(request);
	int rmaTypeId = inputForm.getInt("rmaTypeId", true, 1);
	String deviceSerialCd = ConvertUtils.getStringSafely(inputForm.get("deviceSerialCd"));
	if (!StringUtils.isBlank(deviceSerialCd)) {
		String[] deviceSerialCdList = deviceSerialCd.split("\n");
		if(deviceSerialCdList !=null && deviceSerialCdList.length > 1) {
			ArrayList<String> deviceSerialCdArrayList = new ArrayList<String>(deviceSerialCdList.length);
			for (int i=0; i<deviceSerialCdList.length; i++) {
				if (deviceSerialCdList[i] != null && !StringUtils.isBlank(deviceSerialCdList[i])) {
					deviceSerialCdArrayList.add(deviceSerialCdList[i].trim().replace("\r", "").replace(" ", ""));
				}
			}
			if (deviceSerialCdArrayList.size() > 1) {
				inputForm.set("deviceSerialCdList", deviceSerialCdArrayList);
				inputForm.set("deviceSerialCd", "");
			} else {
				inputForm.set("deviceSerialCd", deviceSerialCdArrayList.get(0));
			}
		}else{
			deviceSerialCd = deviceSerialCd.trim().replace("\r", "").replace(" ", "");
			inputForm.set("deviceSerialCd", deviceSerialCd);
		}
	}
	Results deviceResults = null;
	if (rmaTypeId == 1) {
		deviceResults =  DataLayerMgr.executeQuery("GET_RETURN_RENTAL_DEVICES", inputForm);
	} else if (rmaTypeId == 2) {
		deviceResults =  DataLayerMgr.executeQuery("GET_RETURN_ALL_DEVICES", inputForm);
	} else if (rmaTypeId == 4 || rmaTypeId == 5) {
		deviceResults =  DataLayerMgr.executeQuery("GET_ATT2G_DEVICES", inputForm);
	} else if (rmaTypeId == 6) {
		deviceResults =  DataLayerMgr.executeQuery("GET_ALL_ATT2G_DEVICES", inputForm);
	} %>
<table class="folio">
	<tr class="headerRow">
		<th><input id="selectAllCheckbox" type="checkbox" onclick="onSelectAllCheckbox();"></th>
		<th><a title="Sort Rows by Device Serial Number" data-toggle="sort" data-sort-type="STRING">Device Serial Number</a></th>
		<th><a title="Sort Rows by Region Name" data-toggle="sort" data-sort-type="STRING">Region Name</a></th>
		<th><a title="Sort Rows by location" data-toggle="sort" data-sort-type="STRING">Location</a></th>
		<th><a title="Sort Rows by Asset Nbr" data-toggle="sort" data-sort-type="STRING">Asset Nbr</a></th>
		<th><a title="Sort Rows by Rental" data-toggle="sort" data-sort-type="STRING">Rental</a></th>
		<th><a title="Sort Rows by Rental" data-toggle="sort" data-sort-type="STRING">Rental Fee</a></th>
	</tr>
	<tbody>
	<% int rowCount = 0;
		 boolean hasAtt2G = false;
		 boolean hasLegacy = false;
		while(deviceResults.next()) { 
				rowCount++;
				String serialCd = deviceResults.getFormattedValue("deviceSerialCd");
				boolean isAtt2G = ConvertUtils.getBooleanSafely(deviceResults.getFormattedValue("isAtt2G"), false); 
				boolean isLegacyDevice = serialCd.matches("^(G4|G5|M1).+"); %>
		<tr data-return-device-id="<%=StringUtils.prepareScript(deviceResults.getFormattedValue("deviceId")) %>" class="oddRowHover">
			<td align="center"><% if (rmaTypeId == 2 && isAtt2G) {
					hasAtt2G = true; %>*<% 
				} else if (rmaTypeId == 2 && isLegacyDevice) { 
					hasLegacy = true; %>**<% 
				} else { %>
					<input data-serial-cd="<%=StringUtils.prepareScript(serialCd) %>" type="checkbox" name="rmaDevices" value="<%=StringUtils.prepareScript(deviceResults.getFormattedValue("deviceId")) %>">
			<% } %></td>
			<td data-sort-value="<%=StringUtils.prepareScript(serialCd) %>"><%=StringUtils.prepareHTML(serialCd) %></td>
			<td data-sort-value="<%=StringUtils.prepareScript(deviceResults.getFormattedValue("regionName")) %>"><%=StringUtils.prepareHTML(deviceResults.getFormattedValue("regionName")) %></td>
			<td data-sort-value="<%=StringUtils.prepareScript(deviceResults.getFormattedValue("location")) %>"><%=StringUtils.prepareHTML(deviceResults.getFormattedValue("location")) %></td>
			<td data-sort-value="<%=StringUtils.prepareScript(deviceResults.getFormattedValue("assetNbr")) %>"><%=StringUtils.prepareHTML(deviceResults.getFormattedValue("assetNbr")) %></td>
			<td data-sort-value="<%=StringUtils.prepareScript(deviceResults.getFormattedValue("isRental")) %>"><input type="checkbox"  <%if (deviceResults.getFormattedValue("isRental").equals("Y")){ %> checked="checked"<%}%> disabled="disabled" value="<%=StringUtils.prepareScript(deviceResults.getFormattedValue("isRental")) %>" name="<%=StringUtils.prepareScript(deviceResults.getFormattedValue("deviceId")) %>-isRental"></td>
			<td data-sort-value="<%=StringUtils.prepareScript(deviceResults.getFormattedValue("rentalFee")) %>"><%=StringUtils.prepareHTML(deviceResults.getFormattedValue("rentalFee"))%></td>
		</tr>
	<% } %>
	</tbody>
	<% if (rowCount == 0) { %>
		<tr>
			<td colspan="7" style="text-align:center; padding:.5em; background:#D3D3D3;font-weight:bold;">
			<% if (rmaTypeId == 4 || rmaTypeId == 5 || rmaTypeId == 6) { %>
				No devices found that are eligible for this upgrade.
			<% } else { %>
				No devices found.
			<% } %>
			</td>
		</tr>
	<% } %>
	<% if (hasAtt2G) { %>
		<tr>
			<td colspan="7" class="footnote">
				* Use the <a href="/rma_att_question.html">ATT 2G Upgrade</a> page to RMA ATT 2G devices.
			</td>
		</tr>
	<% } %>
	<% if (hasLegacy) { %>
		<tr>
			<td colspan="7" class="footnote">
				** Not eligible for RMA.
			</td>
		</tr>
	<% } %>
</table>
