<%@page import="com.usatech.usalive.servlet.USALiveUtils"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.ResponseUtils"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>	
<%@page import="simple.results.Results,simple.bean.ConvertUtils,simple.servlet.RequestUtils,simple.text.StringUtils"%><%
InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
String name = RequestUtils.getAttribute(request, "itemName", String.class, true); 
String codeName = RequestUtils.getAttribute(request, "itemCode", String.class, true); 
String title = RequestUtils.getAttribute(request, "itemTitle", String.class, false);
if(title == null || title.trim().length() == 0)
	title = name;
String label = RequestUtils.getAttribute(request, "itemLabel", String.class, false);
String itemDetailJsp = RequestUtils.getAttribute(request, "itemDetailJsp", String.class, true);
String statusMessage = RequestUtils.getAttribute(request, "statusMessage", String.class, false);
String statusType = RequestUtils.getAttribute(request, "statusType", String.class, false);
if(statusMessage == null || (statusMessage=statusMessage.trim()).length() == 0)
	statusMessage = "";
else
	statusMessage = StringUtils.prepareCDATA(statusMessage);
if(statusType == null || (statusType=statusType.trim()).length() == 0)
	statusType = "none";

Boolean noCreateBtn = RequestUtils.getAttribute(request, "noCreateBtn", Boolean.class, false);
if(noCreateBtn != null )
	noCreateBtn = true;
else
	noCreateBtn = false;
String urlBase = RequestUtils.getAttribute(request, SimpleServlet.ATTRIBUTE_CURRENT_PATH, String.class, true);
int selectSize = ConvertUtils.getInt(RequestUtils.getAttribute(request, "selectSize", false), 1);
%>
<div id="mainContainer" class="<%=StringUtils.prepareCDATA(codeName)%>Container">
<span class="caption">Report Register</span>
<div class="spacer5"></div>
<form name="mainForm" id="mainForm" method="post">
<%
ResponseUtils.writeXsfrFormProtection(pageContext);
USALiveUtils.writeFormProfileId(pageContext);
%>
<table>
<tr class="itemMasterTitle"><th colspan="2"><%=StringUtils.prepareCDATA(title) %></th></tr>
<tr><td id="itemMaster">
<div class="itemMasterSelect">
<input type="hidden" name="<%=StringUtils.prepareCDATA(codeName) %>Id"<%Object selectedId = simple.servlet.RequestUtils.getAttribute(request, codeName + "Id", false); if(selectedId != null) {%> value="<%=StringUtils.prepareCDATA(ConvertUtils.getString(selectedId, false))%>"<%} %>/>
<%if(!StringUtils.isBlank(label)) { %><label class="itemLabel" for="mainSelect"><%=StringUtils.prepareHTML(label) %></label><%} %>
<div id="mainSelectSection">
<select id="mainSelect" tabindex="1" onchange="onMasterChange(this)" size="<%=selectSize%>"><!-- <option value="">- - Please Select - -</option> --><%
	Results results = RequestUtils.getAttribute(request, codeName + "s", Results.class, true);
	while(results.next()) {
	Object itemId = results.getValue(codeName + "Id");
	%><option <% if(codeName.equals("userReport")&&results.getValue("userReportStatus", String.class).equals("E")){ %> style="color:red" <%} %> value="<%=StringUtils.prepareCDATA(ConvertUtils.getStringSafely(itemId,""))%>" <% if(selectedId != null && ConvertUtils.areSimilar(itemId, selectedId)) { %> selected="selected"<%} %>><%=StringUtils.prepareCDATA(results.getFormattedValue(codeName + "Name"))%></option>	    	
	<%}%>
</select>
</div>
</div>
<div id="status" class="status-<%=StringUtils.prepareCDATA(statusType) %>"><%=StringUtils.prepareHTML(statusMessage) %></div>
<div class="itemMasterButtons" id="itemMasterButtons" >
	<input type="hidden" name="selectedMenuItem" value="<%=inputForm.getStringSafely("selectedMenuItem", "")%>" />
	<input name="saveBtn" id="saveBtn" type="submit" value="Save" onclick="saveItem(); return false;" disabled="disabled"/>
	<input name="cancelBtn" id="cancelBtn" type="button" value="Cancel" onclick="cancelItem()" disabled="disabled"/>
     <% if (!noCreateBtn){ %>
    <input name="createBtn" id="createBtn" type="button" value="Create" onclick="createItem()"/>
     <%} %>			     
	<input name="deleteBtn" id="deleteBtn" type="button" value="Delete" onclick="deleteItem()"<%if(selectedId == null) {%> disabled="disabled"<%} %>/>
</div>
</td><td id="itemDetail"><jsp:include page="<%=itemDetailJsp%>" flush="true" /></td></tr></table>
</form>
<script type="text/javascript">
window.addEvent("domready", function() {
	window.createBtn = $("createBtn");
	window.deleteBtn = $("deleteBtn");
	window.saveBtn = $("saveBtn");
	window.cancelBtn = $("cancelBtn");
	window.mainForm = $("mainForm");
	window.mainSelect = $("mainSelect");
	window.statusBox = $("status");
	window.itemIdInput = $(mainForm.<%=StringUtils.prepareScript(codeName) %>Id);
	var detail = $("itemDetail");
	var hookChanges = function() {
		new Form.Validator.Inline.Mask(mainForm);
		detail.getElements("input[name], textarea[name], select[name]").each(function(item) {
			item = $(item);
			if(item.id == "mainSelect" || (item.tagName == "input" &amp;&amp; item.type == "button"))
				return;
			var onchange = item.onchange;
			if(onchange)
				item.onchange = function() {
					onDetailChange();
					if(onchange)
						onchange.apply(item);
				};
			else
				item.onchange = onDetailChange;	
		});
	};
	window.changeRequest = new Request.HTML({
		url: ".<%=StringUtils.prepareScript(urlBase) %>_detail.i",
		async: false,
		update: detail, 
		evalScripts: true,
		onSuccess: hookChanges
	});
	if(itemIdInput.getValue() != mainSelect.getValue() || itemIdInput.getValue() != "<%=StringUtils.prepareScript(ConvertUtils.getString(selectedId, false))%>")
		onMasterChange(mainSelect);
	else
		hookChanges();	
	detail.addEvent("mouseleave", function(event) {
		var active;
		if(saveBtn.disabled &amp;&amp; document.activeElement &amp;&amp; document.activeElement.form == mainForm 
				&amp;&amp; (active=$(document.activeElement)).getParent("#itemDetail") != null &amp;&amp; active.get("tag") != "select") {
			document.activeElement.blur();
		}			
	});
});

function deleteItem() {
	if(!itemIdInput.getValue()) {
		onMasterChange(mainSelect);	
    } else {
    	if(!confirm("Do you really want to delete the <%=StringUtils.prepareScript(name)%> '" +  mainSelect.options[mainSelect.selectedIndex].text + "'?"))
            return;
    	var validator = mainForm.retrieve('validator');
        if(validator)
        	validator.stop();
    	mainForm.submit(".<%=StringUtils.prepareScript(urlBase) %>_delete.i");
    }
}

function saveItem() {
	if(typeof itemMasterExtraCheck == 'function') {
		if(!itemMasterExtraCheck()){
			return;
		}
	}
	mainForm.submit(".<%=StringUtils.prepareScript(urlBase) %>" + (itemIdInput.getValue() == "" ? "_insert" : "_update") + ".i");
}

function createItem() {
	mainSelect.setValue("");
	onMasterChange(mainSelect);
	<%if(!noCreateBtn) {%>createBtn.disable = true;<%}%>
}
function cancelItem() {
	onMasterChange(mainSelect);
}
function onMasterChange(select) {
	var value = select.getValue();
	itemIdInput.setValue(value);
	saveBtn.disabled = true;
    cancelBtn.disabled = true;
    <%if(!noCreateBtn) {%>createBtn.disabled = false;<%}%>
    deleteBtn.disabled = (value == "");
	changeRequest.send("fragment=true&amp;<%=StringUtils.prepareScript(codeName) %>Id=" + value);
    updateStatus("", "none");
    if(typeof onMasterChangeExtra == 'function') {
    	onMasterChangeExtra();
	}
}
function onDetailChange() {
	saveBtn.disabled = false;
    cancelBtn.disabled = false;
    <%if(!noCreateBtn) {%>createBtn.disabled = true;<%}%>
    deleteBtn.disabled = true;
    mainSelect.disable = true;
	updateStatus("Editing in process. Click 'Cancel' to select a different <%=StringUtils.prepareScript(name)%>", "info");
}
function updateStatus(msg, type) {
	if(!msg || msg.trim().length == 0)
		statusBox.set("html", "&amp;#160;");
	else 
		statusBox.set("text", msg);
	statusBox.className = "status-" + type;
}

</script>
</div>
