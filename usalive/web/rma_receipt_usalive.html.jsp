<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.RequestUtils"%>
<% InputForm inputForm = RequestUtils.getInputForm(request);
	int rmaTypeId = inputForm.getInt("rmaTypeId", true, 1); %>
<jsp:include page="include/header.jsp"/>
	<% if(rmaTypeId == 1) { %>
		<jsp:include page="rma_rental_for_credit_display.jsp"/>
	<% } else if(rmaTypeId == 2) { %>
		<jsp:include page="rma_all_device_display.jsp"/>
	<% } else if(rmaTypeId == 3) { %>
		<jsp:include page="rma_parts_display.jsp"/>
	<% } else if(rmaTypeId == 4 || rmaTypeId == 5 || rmaTypeId == 6) { %>
		<jsp:include page="rma_att_display.jsp"/>
	<%}; %>
<jsp:include page="include/footer.jsp"/>
