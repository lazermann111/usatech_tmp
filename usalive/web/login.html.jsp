<%@page import="java.util.Collections"%>
<%@page import="java.util.regex.Pattern"%>
<%@page import="simple.bean.ConvertException"%>
<%@page import="simple.servlet.steps.LoginFailureException"%>
<%@page import="simple.servlet.SimpleServlet,simple.servlet.InputForm,java.util.Map,simple.bean.ConvertUtils,simple.servlet.InputFile,simple.servlet.RequestUtils,simple.text.StringUtils"%>
<%@page import="java.util.Set,java.util.HashSet,java.util.Arrays,simple.servlet.SimpleAction,simple.servlet.NotLoggedOnException,simple.servlet.NoSessionTokenException,simple.servlet.InvalidSessionTokenException"%>
<%!
protected static final Set<String> dontForwardPaths = new HashSet<String>(Arrays.asList(new String[] {
	"/login_prompt.i",
	"/logout.i",
	"/login.i",
	"/login_dialog.i",
	"/login.html"
})); 
protected static final Set<String> ignoreParameters = new HashSet<String>(Arrays.asList(new String[] {
    "username",
    "password",
    "loginPromptPage",
    "simple.servlet.steps.LogonStep.forward",
    "content",
    "session-token",
    "selectedMenuItem"
}));
protected boolean shouldForwardAfterLogin(String contentPath) {
		if(StringUtils.isBlank(contentPath))
			return false;
		for(String suffix : dontForwardPaths)
			if(contentPath.endsWith(suffix))
				return false;
		return true;
	}
%><%
String[] contentTypes = RequestUtils.getPreferredContentType(request, "text/html", "application/xhtml+xml", "text/plain");
boolean textResult = (contentTypes != null && contentTypes.length > 0 && contentTypes[0].equalsIgnoreCase("text/plain"));
InputForm form = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
Throwable exception = (Throwable)RequestUtils.getAttribute(request, SimpleAction.ATTRIBUTE_EXCEPTION, false);
if(exception instanceof LoginFailureException && ((LoginFailureException)exception).getType() == LoginFailureException.PASSWORD_EXPIRED) {
	if(textResult)
		RequestUtils.writeTextResponse(response, "error", exception.getMessage());
	else {
		form.setAttribute("onloadMessage", exception.getMessage());
	    RequestUtils.redirectWithCarryOver(request, response, "edit_password.html");
	}
    return;
}
boolean expired;
if(session != null) {
    if(session.isNew())
        expired = false;
    else if(session.getMaxInactiveInterval() > 0)
        expired = (System.currentTimeMillis() - session.getLastAccessedTime())/ 1000 > session.getMaxInactiveInterval();
    else
        expired = false;
} else
    expired = false;
if(textResult) {
	if(exception instanceof LoginFailureException)
		RequestUtils.writeTextResponse(response, "error", RequestUtils.getTranslator(request).translate("login-failed-prompt", "Your username or password is invalid; please re-enter your login information."));
	else if((exception instanceof NotLoggedOnException && expired)
	        || (exception instanceof NoSessionTokenException && (null != RequestUtils.getSessionToken(request) || null != request.getParameter(SimpleServlet.REQUEST_SESSION_TOKEN))))
		RequestUtils.writeTextResponse(response, "error", RequestUtils.getTranslator(request).translate("login-expired-prompt", "Your session has expired; please re-enter your login information.")); 
	else if(exception instanceof InvalidSessionTokenException) 
	    RequestUtils.writeTextResponse(response, "error", RequestUtils.getTranslator(request).translate("invalid-request", "The request was invalid, please login again for the protection of your account."));
	else {
		String infoMessage = RequestUtils.getAttribute(request, "onloadMessage", String.class, false);
	    if(infoMessage != null && infoMessage.trim().length() > 0)
	        RequestUtils.writeTextResponse(response, "info", infoMessage);
	    else
	    	RequestUtils.writeTextResponse(response, "info", "Please include your username and password to login to USALive");
	}
	return;
}
RequestUtils.setAttribute(request, "subtitle", "Log In", "request");
boolean isFragment = Boolean.TRUE.equals(RequestUtils.getAttribute(request, "fragment", Boolean.class, false));
String contentPath = RequestUtils.getRequestURIQuery(request); //request.getRequestURI()
Map<String,Object> queryParams;
if(request.getQueryString() != null)
	queryParams = RequestUtils.parseParameters(request.getQueryString());
else
	queryParams = Collections.emptyMap();
Integer selectedMenuItem;
try {
    selectedMenuItem = RequestUtils.getAttribute(request, "selectedMenuItem", Integer.class, false);
} catch(ConvertException e) {
    selectedMenuItem = null;
}
if(!shouldForwardAfterLogin(contentPath)) {
    contentPath = "/home.i";
	selectedMenuItem = 110;
}
if(isFragment){
		  %><div id="loginPopup">
    <script type="text/javascript">
        var loginDialog = new Dialog.Url({url: "login_dialog.i", data : {
            "simple.servlet.steps.LogonStep.forward":"<%=StringUtils.prepareScript(contentPath)%>"<%
if(selectedMenuItem != null) { %>
           ,"selectedMenuItem":<%=selectedMenuItem%><%
}
for(Map.Entry<String,Object> param : form.getParameters().entrySet()) {
    if(ignoreParameters.contains(param.getKey())) continue;
    if(param.getValue() instanceof String) {
        %>,"<%=StringUtils.prepareScript(param.getKey())%>":"<%=StringUtils.prepareScript((String)param.getValue())%>"<%
    } else if(param.getValue() instanceof String[]) {
        String[] values = (String[])param.getValue();
        for(int i = 0; i < values.length; i++) {
            %>,"<%=StringUtils.encodeForJavaScript(param.getKey())%>":"<%=StringUtils.encodeForJavaScript(values[i])%>"<%
        }
    } else if(param.getValue() instanceof InputFile || param.getValue() instanceof InputFile[]) {
        RequestUtils.storeForNextRequest(request.getSession(), param.getKey(), param.getValue());
    } else if(param.getValue() != null) {
        String value = ConvertUtils.getStringSafely(param.getValue());
        %>"<%=StringUtils.encodeForJavaScript(param.getKey())%>":"<%=StringUtils.encodeForJavaScript(value)%>"<%
    }
}%>
        }, width:255, height:185, title: "Please Log In"});
        loginDialog.show();
    </script>
    </div><%
	}else{%>
<jsp:include page="include/header.jsp"/>
	<table id="layoutContainer">
	<tr id="signInBody">
	<td>
	<div id="signInPanel">
	<form name="info" method="post" action="<%=StringUtils.prepareCDATA(contentPath)%>" onsubmit="App.updateClientTimestamp(this)" autocomplete="off" id="signInForm">
           <input type="hidden" name="session-token" value="<%=StringUtils.prepareCDATA(RequestUtils.getSessionToken(request))%>"/><%
if(selectedMenuItem != null) { %>
    <input type="hidden" name="selectedMenuItem" value="<%=selectedMenuItem%>" /><%
}
for(Map.Entry<String,Object> param : form.getParameters().entrySet()) {
    if(ignoreParameters.contains(param.getKey()) || queryParams.containsKey(param.getKey())) 
    	continue;
    if(param.getValue() instanceof String) {
        %><input type="hidden" name="<%=StringUtils.prepareCDATA(param.getKey())%>" value="<%=StringUtils.prepareCDATA((String)param.getValue())%>"/><%
    } else if(param.getValue() instanceof String[]) {
        String[] values = (String[])param.getValue();
        for(int i = 0; i < values.length; i++) {
            %><input type="hidden" name="<%=StringUtils.encodeForHTMLAttribute(param.getKey())%>" value="<%=StringUtils.encodeForHTMLAttribute(values[i])%>"/><%
        }
    } else if(param.getValue() instanceof InputFile || param.getValue() instanceof InputFile[]) {
        RequestUtils.storeForNextRequest(request.getSession(), param.getKey(), param.getValue());
    } else if(param.getValue() != null) {
        String value = ConvertUtils.getStringSafely(param.getValue());
        %><input type="hidden" name="<%=StringUtils.encodeForHTMLAttribute(param.getKey())%>" value="<%=StringUtils.encodeForHTMLAttribute(value)%>"/><%
    }
}
String infoMessage = RequestUtils.getAttribute(request, "onloadMessage", String.class, false);
if(!StringUtils.isBlank(infoMessage)) { 
%><div class="confirmText"><%=StringUtils.prepareHTML(infoMessage)%></div><% 
}
String errorMessage = RequestUtils.getAttribute(request, "errorMessage", String.class, false);
if(!StringUtils.isBlank(errorMessage)) { 
%><div class="errorText"><%=StringUtils.prepareHTML(errorMessage)%></div><% 
}
if(exception instanceof LoginFailureException) { 
%><div class="errorText"><%=RequestUtils.getTranslator(request).translate("login-failed-prompt", "Your username or password is invalid; please re-enter your login information.") %></div><% 
} else if((exception instanceof NotLoggedOnException && expired)
        || (exception instanceof NoSessionTokenException && (null != RequestUtils.getSessionToken(request) || null != request.getParameter(SimpleServlet.REQUEST_SESSION_TOKEN)))) { 
%><div class="errorText"><%=RequestUtils.getTranslator(request).translate("login-expired-prompt", "Your session has expired; please re-enter your login information.") %></div><% 
} else if(exception instanceof InvalidSessionTokenException) { 
%><div class="errorText"><%=RequestUtils.getTranslator(request).translate("invalid-request", "The request was invalid, please login again for the protection of your account.") %></div><% 
}
Cookie cookie = form.getCookie("username");
String username = RequestUtils.getAttribute(request, "username", String.class, false);
if(StringUtils.isBlank(username) && cookie != null)
	username = cookie.getValue();
%>
<table><tr><td class="loginChoiceExisting">
<table class="loginLayout">
    <tr><td colspan="2" align="center"><div class="headerLogo"></div></td></tr>
    <tr><th>User Name:&#160;</th><td><input type="text" name="username" value="<%=StringUtils.prepareCDATA(username)%>" required="required"/></td></tr>
    <tr><th>Password:</th><td><input type="password" name="password" value="" autocomplete="off" required="required"/></td></tr>
    <tr class="loginButtonRow"><td colspan="2" align="right"><input type="submit" tabindex="0" value="Sign In"/></td></tr>
</table>
  <p><a tabindex="0" href="reset_password_prompt.i" onmouseover="window.status = 'Forgot your password?'; return true;"
        onmouseout="window.status = ''">Forgot your password?</a><br/></p>
</td>
</tr>        
</table>
</form>
</div>
<script type="text/javascript"> 
window.addEvent('domready', function() {
	try {
		if (document.forms["info"].elements["username"].value == "")
	    	document.forms["info"].elements["username"].focus();
		else
			document.forms["info"].elements["password"].focus();
	} catch (e) { }
});
</script>
</td>
</tr>
<jsp:include page="include/footer.jsp"/>
<%}%> 
