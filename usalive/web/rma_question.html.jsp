<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="simple.text.StringUtils"%>

<%
	UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
	boolean hasPri=false;
	String rmaQuestion=RequestUtils.getAttribute(request, "rmaQuestion", String.class, false);
	if((user.isInternal()&&user.hasPrivilege(ReportingPrivilege.PRIV_CUSTOMER_SERVICE))||user.hasPrivilege(ReportingPrivilege.PRIV_RMA)){
		hasPri=true;
		if(!StringUtils.isBlank(rmaQuestion)&&rmaQuestion.equalsIgnoreCase("Yes")){
			RequestUtils.redirectWithCarryOver(request, response, "rma_device.html", false, true);
		}
		
	}
%>
<jsp:include page="include/header.jsp"/>
<span class="caption">Create RMA</span>
<%if(hasPri){ %>
	<%if(!StringUtils.isBlank(rmaQuestion)&&rmaQuestion.equalsIgnoreCase("No")){ %>
		<p class="instruct">
		Please contact your sales representative to initiate the correct process through USALive to support your request, thank you.
		</p>
	<%}else{ %>
		<p class="instruct">
		Are you entering devices that are requiring inspection / repair that USAT will return to you?
		</p>
		<form name="rmaQuestionForm" id="rmaQuestionForm" action="rma_question.html">
		<input type="checkbox" id="needReplacement" name="needReplacement" value="Y" > Check this box if you need advance replacement for this RMA (Only devices serial number starting with VJ or K3VS can be in the RMA)
		<br/>
		<br/>
		<input type="submit" name="rmaQuestion" value="Yes"/>
		<input type="submit" name="rmaQuestion" value="No"/>
		</form>
	<%} %>
<%}else{%>
	<p class="not-authorized">User '<%=StringUtils.prepareHTML(user.getUserName()) %>' does not have permission to view this page. Please contact USA Technologies if you need access.</p>
<%}%>
<jsp:include page="include/footer.jsp"/>