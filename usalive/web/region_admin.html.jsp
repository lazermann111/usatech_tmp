<%@page import="java.sql.SQLIntegrityConstraintViolationException"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.NotAuthorizedException"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="com.usatech.report.ReportingUser.ActionType"%>
<%@page import="com.usatech.report.ReportingUser.ResourceType"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="simple.xml.sax.MessagesInputSource"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%
Long regionId = RequestUtils.getAttribute(request, "regionId", Long.class, false);
UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
if(regionId != null && regionId != 0 && "POST".equalsIgnoreCase(request.getMethod())) {
	String regionName = RequestUtils.getAttribute(request, "regionName", String.class, true);
	MessagesInputSource messages = RequestUtils.getOrCreateMessagesSource(request);
	if(regionId > 0) {
		user.checkPermission(ResourceType.REGION, ActionType.EDIT, regionId);
	    try {
			int n = DataLayerMgr.executeUpdate("UPDATE_REGION_DETAILS", RequestUtils.getInputForm(request), true);
			switch(n) {
				case 1:
					messages.addMessage("success", "region.update.success", "Region {0} was updated", regionName);
					break;
				case 0:
					messages.addMessage("error", "region.update.not-found", "Region {0} does not exist. Please choose a valid region", regionName);
		            break;
				default:
					messages.addMessage("error", "region.update.error", "Region {0} could not be updated. Please try again.", regionName);
		            break;
			}
		} catch(SQLIntegrityConstraintViolationException e) {
			messages.addMessage("warn", "region.update.duplicate", "Region {0} already exists. Choose a different name or merge regions", regionName);
        }
	} else {
		if(user.getCustomerId() <= 0)
			throw new NotAuthorizedException("Only a user of a customer may create new regions. User '" + user.getUserName() + "' may not");
		DataLayerMgr.executeUpdate("INSERT_REGION", RequestUtils.getInputForm(request), true);
		regionId = RequestUtils.getAttribute(request, "regionId", long.class, true);
		char newFlag = RequestUtils.getAttribute(request, "newFlag", char.class, true);
        switch(newFlag) {
            case 'Y':
                messages.addMessage("success", "region.insert.success", "Region {0} was created", regionName);
                break;
            case 'N':
                messages.addMessage("warn", "region.insert.duplicate", "Region {0} already exists", regionName);
                break;
            default:
                messages.addMessage("error", "region.insert.error", "Region {0} could not be created. Please try again.", regionName);
                break;
        }
        
	}
	RequestUtils.redirectWithCarryOver(request, response, "region_admin.html?regionId=" + regionId);
	return;
}
if(!user.hasPrivilege(ReportingPrivilege.PRIV_REGION_ADMIN))
	throw new NotAuthorizedException("User '" + user.getUserName() + "' is not permitted to administer Regions");
Results regionResults = DataLayerMgr.executeQuery("GET_REGION_DETAILS", RequestUtils.getInputForm(request));
Results parentResults = DataLayerMgr.executeQuery("GET_PARENT_REGIONS", RequestUtils.getInputForm(request));
%>
<jsp:include page="include/header.jsp"/>
<span class="caption">Regions</span>
<form id="regionsForm" action="region_admin.html" method="POST" data-toggle="validateInline">
<table class="selectBody">
    <tr><td width="30%">
        <div class="selectSection">
            <div class="sectionTitle">Regions</div>
                <div class="sectionRow">
                    <select size="15" name="regionId" class="full"><%
        while(regionResults.next()) {
            long resultsRegionId = regionResults.getValue("regionId", long.class);
            Long employeeCount = regionResults.getValue("employeeCount", Long.class);
            Long parentRegionId = regionResults.getValue("parentRegionId", Long.class);
            Integer subRegionCount = regionResults.getValue("subRegionCount", Integer.class);
            
        %><option value="<%=resultsRegionId
        %>" onselect="setDetails(this, {regionName: '<%=StringUtils.prepareScript(regionResults.getFormattedValue("regionName"))
        %>', accountNum: '<%=StringUtils.prepareScript(regionResults.getFormattedValue("accountNum"))
        %>', employeeCount: '<%=(employeeCount == null ? "" : String.valueOf(employeeCount))
        %>', parentRegionId: '<%=(parentRegionId == null ? "" : String.valueOf(parentRegionId))
        %>', subRegionCount: <%=(subRegionCount == null ? "0" : String.valueOf(subRegionCount))
        %>});"<%
        if(regionId != null && resultsRegionId == regionId) {%> selected="selected"<%} %>>
        <%=StringUtils.prepareCDATA(regionResults.getFormattedValue("regionName"))%>
        </option><%
        }%>
        </select>
        <div align="center"><%if(user.getCustomerId() > 0) {%><input type="button" id="addButton" value="Add Region" onclick="addItem(this.form.regionId, 'Region');"/>&#160;<%} %><input type="button" id="deleteButton" value="Delete Region" onclick="deleteRegion(this.form);" disabled="disabled"/></div>
    </div></div>
    </td>
    <td>
    <div class="selectSection">
            <div class="sectionTitle" id="campaignInfoTitle" >Region Information</div>
    <table class="campaignInfo">
        <tr><td>Region Name<span class="required">*</span></td><td><input name="regionName" type="text" placeholder="The region name" data-validators="required maxLength:60" onChange="detailsChanged(this.form.regionId);"/></td></tr>
        <tr><td>GL Account #</td><td><input name="accountNum" type="text" placeholder="The GL account number" onChange="detailsChanged(this.form.regionId);"/></td></tr>
        <tr><td>Num of Employees</td><td><input name="employeeCount" type="number" placeholder="The number of employees in this region" onChange="detailsChanged(this.form.regionId);"/></td></tr>
        <% if(!parentResults.isGroupEnding(0)) {%>
        <tr><td>Parent Region</td><td><select name="parentRegionId" onChange="detailsChanged(this.form.regionId);">
            <option value="">-- None --</option><%
            while(parentResults.next()) {
            	%><option value="<%=parentResults.getValue("regionId", Long.class)%>"><%=StringUtils.prepareHTML(parentResults.getValue("regionName", String.class))%></option><%
            }
         %>
        </select>
        </td></tr><%} %>
    <tr><td colspan="2" onmouseover="checkChanges($('regionsForm'))"><input type="submit" id="saveButton" value="Save" disabled="disabled"/>
    &#160;<input type="button" id="cancelButton" value="Cancel" onclick="cancelItem(this.form.regionId);" disabled="disabled"/>
    </tr>
    <tr><td colspan="4"><span class="required">* Required</span></td></tr>
    </table>
    </div>
    </td>
    </tr>
</table>
</form>
<script type="text/javascript">
function setDetails(select, details) {
	var form = $(select.form);
	Object.each(details, function(item, key) {
		if(form.elements[key])
			form.elements[key].setValue(item);
	});
	if(details['subRegionCount'] == 0) {
		form.parentRegionId.disabled = false;
		Array.each(form.parentRegionId.options, function(item) {
	        item.disabled = (item.getValue() == form.regionId.getValue());
	    });
	} else
		form.parentRegionId.disabled = true;
	setUnsaved(select, form.parentRegionId.getValue() != details['parentRegionId']);
}

function cancelItem(select) {
	select = $(select);
	if(select.selectedIndex == -1)
		select.form.reset();
	else {
		select.options[select.selectedIndex].fireCustomEvent("select");         
		if(select.options[select.selectedIndex].getValue() == -1)
			select.options[select.selectedIndex].dispose();
	}
	setUnsaved(select, false);
}
function addItem(select, label) {
	select = $(select);
	var details = {};
	Array.each(select.form.elements, function(el) {
		if(el.name && el != select)
			 details[el.name] = "";
	});
	var fn = function() { setDetails(select, details);};
	select.adopt(new Element("option", {value: "-1", text: "-- New " + (label || "Item") + " --", selected: true, events: { select: fn}}));
	fn.apply();
	setUnsaved(select, true);
}
function deleteRegion(form) {
	form = $(form);
    if(confirm("Do you want to delete region '" + $(form.elements["regionName"]).getValue() + "'\n(Devices or consumer assigned to this region will be moved to <NO REGION>)?'")) {
    	form.action = "region_delete.html";
		form.submit();
	}
}
function detailsChanged(select) {
	setUnsaved(select, true);
}
function checkChanges(form) {
	form = $(form);
    if(!form.retrieve("unsaved") && document.activeElement && $(document.activeElement.form) == form) {
    	var el = $(document.activeElement);
    	el.blur();
    	el.focus();
    }    	
}
function setUnsaved(select, value) {
	<%if(user.getCustomerId() > 0) {%>$("addButton").disabled = value;
	<%}%>$("deleteButton").disabled = value || select.selectedIndex == -1;   
	$("saveButton").disabled = !value;
	$("cancelButton").disabled = !value; 
	Array.each($(select).options, function(el) {
        if(!el.selected || !value)
            el.disabled = value;
    });
	$(select.form).store("unsaved", value);
}
</script>
<jsp:include page="include/footer.jsp"/>