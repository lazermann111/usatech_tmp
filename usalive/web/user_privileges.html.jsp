<%@page import="java.util.Set"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.LinkedHashSet"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.NotAuthorizedException"%>
<%@page import="simple.db.Call.Sorter"%>
<%@page import="simple.servlet.GenerateUtils"%>
<%@page import="com.usatech.layers.common.util.PaginationUtil"%>
<%@page import="com.usatech.usalive.servlet.USALiveUtils"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.text.StringUtils"%>
<%
InputForm form = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
UsaliveUser user = (UsaliveUser) RequestUtils.getUser(request);
if(!user.isInternal())
    throw new NotAuthorizedException("You are not authorized to perform this function.");
Results privResults = DataLayerMgr.executeQuery("GET_ALL_PRIVILEGES", form);
Sorter sorter = DataLayerMgr.getGlobalDataLayer().findCall("GET_USER_PRIVILEGES").getSorter();
int privId = form.getInt("privId", false, -1);
String privGranted = form.getStringSafely("privGranted", "");
String[] sortBy = ConvertUtils.toUniqueArray(form.getStringArray("sortBy", false), String.class, "sortableCustomerName","sortableEmail","sortablePrivName");
form.setAttribute("sortBy", sortBy);
int pageIndex = form.getInt("pageIndex", false, 1);
int pageSize = form.getInt("pageSize", false, PaginationUtil.DEFAULT_PAGE_SIZE);
Results results = null;
if (privId > -1)
	results = sorter.executeSorted(form, pageSize, pageIndex, sortBy);
%>
<jsp:include page="include/header.jsp"/>

<div class="title4">User Privileges</div>
<div class="spacer5"></div>

<form method="get" action="user_privileges.html" data-toggle="validate">
	<%=USALiveUtils.getHiddenInputs(request)%>
	<div class="full"> 
	<span class="label">Privilege</span>
	<select name="privId">
		<option value="0">-- All --</option>
		<%while (privResults.next()) {%>
		<option value="<%=privResults.getValue("privId", int.class)%>"<%if (privId == privResults.getValue("privId", int.class)) {%> selected="selected"<%}%>><%=StringUtils.prepareHTML(privResults.getFormattedValue("description"))%></option>
		<%}%>
	</select>
	<span class="label">Granted</span>
	<select name="privGranted">
		<option value="A">-- All --</option>
		<option value="N"<%if ("N".equalsIgnoreCase(privGranted)) {%> selected="selected"<%}%>>N</option>
		<option value="Y"<%if ("Y".equalsIgnoreCase(privGranted)) {%> selected="selected"<%}%>>Y</option>
	</select>
	<button type="submit">Submit</button>
<div class="spacer5"></div>
<%if (results != null) {%>
<table class="sortable-table">
	<tr class="gridHeader"><%
	GenerateUtils.writeSortedTableHeader(pageContext, "sortableCustomerName", "Customer", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortableFirstName", "First Name", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortableLastName", "Last Name", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortableUserName", "User Name", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortableEmail", "Email", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortableTelephone", "Phone", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortableCity", "City", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortableState", "State", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortablePrivName", "Privilege", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortablePrivGranted", "Granted", sortBy);
	GenerateUtils.writeSortedTableHeader(pageContext, "sortableAdminUserInd", "Admin User", sortBy);
	%>
	</tr><%
while(results.next()) {%>
	<tr>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("customerName"))%></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("firstName"))%></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("lastName"))%></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("userName"))%></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("email"))%></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("telephone"))%></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("city"))%></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("state"))%></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("privName"))%></td>
	    <td><%=StringUtils.prepareHTML(results.getFormattedValue("privGranted"))%></td>
 <td><%=StringUtils.prepareHTML(results.getFormattedValue("adminUserInd"))%></td>
	</tr><%
}
int rows = results.getRowCount();
int count;
if(pageIndex == 1 && rows < PaginationUtil.DEFAULT_PAGE_SIZE) { 
    count = rows;
} else { 
    count = sorter.executeCount(form);
}%>
<tr><td colspan="11" class="pagination-container">
<jsp:include page="include/pagination2.jsp">
    <jsp:param name="pageIndex" value="<%=pageIndex %>"/>
    <jsp:param name="pageSize" value="<%=pageSize %>"/>
    <jsp:param name="count" value="<%=count %>"/>
</jsp:include>
</td></tr>
</table>
<%}%>
</div>
</form>
<jsp:include page="include/footer.jsp"/>