<%@page import="java.util.ArrayList"%>
<%@page import="com.usatech.layers.common.device.DeviceConfigurationUtils"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.usatech.usalive.servlet.USALiveUtils"%>
<%@page import="simple.servlet.ResponseUtils"%>
<%@page import="com.usatech.report.ReportingUser.ActionType"%>
<%@page import="com.usatech.report.ReportingUser.ResourceType"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.usatech.usalive.device.MassDeviceConfigUtils"%>
<%@page language="java" contentType="text/html; charset=iso-8859-1" pageEncoding="iso-8859-1"%>

<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.Map"%>

<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.text.StringUtils"%>

<%@page import="com.usatech.layers.common.constants.DeviceType"%>
<%@page import="com.usatech.layers.common.device.DevicesConstants"%>
<%@page import="com.usatech.layers.common.model.ConfigTemplateSetting"%>
<%@page import="com.usatech.layers.common.util.StringHelper"%>
<%@page import="com.usatech.layers.common.util.WebHelper"%>

<jsp:include page="include/header.jsp"/>

<div class="tableDataContainer">
<form name="editConfig" method="post" action="mass_device_configuration_2.html" onsubmit="return validateUSATForm(this) && validateDeviceConfiguration(this);">

<%
InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
if (MassDeviceConfigUtils.processMassDeviceConfig(inputForm, request, response))
	return;
UsaliveUser checkUser = (UsaliveUser) RequestUtils.getUser(request);
String serialNumberList = inputForm.getStringSafely("serial_number_list", "").replace('\n', ',').replace("\r", "").replace(" ", "");

ResponseUtils.writeXsfrFormProtection(pageContext);
USALiveUtils.writeFormProfileId(pageContext);
%>

<table class="full">
	<tr class="tableHeader"><th><div id="device_type_index" align="center">Mass Device Configuration</div></th></tr>
</table>

<%
String errorMessage = inputForm.getString("errorMessage", false);
if (!StringHelper.isBlank(errorMessage)) {%>
<div class="spacer10"></div>
<div class="message-error"><%=StringUtils.prepareHTML(errorMessage).replace("\n", "<br/>")%><br/></div>
<%}%>

<%
String successMessage = inputForm.getString("successMessage", false);
if (!StringHelper.isBlank(successMessage)) {%>
<div class="spacer10"></div>
<div class="message-success"><%=StringUtils.prepareHTML(successMessage).replace("\n", "<br/>")%><br/></div>
<%}%>

<input type="hidden" name="serial_number_list" value="<%=serialNumberList%>" />

<%
Map<Integer, String> deviceTypes = new LinkedHashMap<Integer, String>();
Map<String, Object> params = new HashMap<String, Object>();
params.put("checkUser", checkUser);
params.put("eportSerialNums", serialNumberList);
Results results = DataLayerMgr.executeQuery("GET_DEVICE_TYPES_FOR_MASS_CONFIGURATION", params);
while (results.next()) {
	deviceTypes.put(results.getValue("DEVICE_TYPE_ID", Integer.class), results.getFormattedValue("DEVICE_TYPE_DESC"));
}
results.close();
if (deviceTypes.size() == 0) {%>
	<div class="message-error">Devices not found or unauthorized to edit<br/></div>
<%
	return;
}

DataLayerMgr.selectInto("GET_MIN_PLV_FOR_MASS_CONFIGURATION", params);
int plv = ConvertUtils.getIntSafely(params.get("plv"), -1);
%>
<input type="hidden" name="plv" id="plv" value="<%=plv%>" />
<div class="spacer5"></div>
<span class="set-default-note">* Please note: in order for the changes to take effect you must check the checkboxes of the properties you want to change</span>
<div class="spacer5"></div>

<table class="tabDataDisplayNoBorder">
	<tr>
		<td colspan="2">
		<div class="sectionIndex">
		<table class="tabDataDisplayNoBorder">
			<tr>
			<td align="left" valign="top">
				<b>Device Types:</b>
			</td>
			<td>&nbsp;</td>
			<td align="left">
			<%for(Map.Entry<Integer, String> deviceType : deviceTypes.entrySet()) {%>
				<a href="#device_type_section_<%=deviceType.getKey()%>"><%=deviceType.getValue()%></a><br/>
			<%}%>
			</td>
			</tr>
		</table>
		</div>
		</td>
	</tr>
</table>

<div class="spacer5"></div>

<%
for(Map.Entry<Integer, String> deviceType : deviceTypes.entrySet()) {
int deviceTypeId = deviceType.getKey();
String deviceTypeDesc = deviceType.getValue();
String keyPrefix = new StringBuilder().append(deviceTypeId).append("_").toString();
boolean isEdge = deviceTypeId == DeviceType.EDGE.getValue();
boolean isGx = deviceTypeId == DeviceType.G4.getValue();
boolean isMEI = deviceTypeId == DeviceType.MEI.getValue();
LinkedHashMap<String, ConfigTemplateSetting> defaultSettingData = DeviceConfigurationUtils.getDefaultConfFileDataByDeviceTypeId(deviceTypeId, null, null, "Y", null, null);
LinkedHashMap<String, ConfigTemplateSetting> targetSettingData = DeviceConfigurationUtils.getDefaultConfFileDataByDeviceTypeId(deviceTypeId, null);
%>

<table class="full">
	<tr class="tableHeader">
		<th>
		<div id="device_type_section_<%=deviceTypeId%>" align="center">
		<%=deviceTypeDesc%> Settings
		<a href="#device_type_index"><img src="/images/arrow-up-icon.png" style="vertical-align:bottom; border:0;"/></a>
		</div>
		</th>
	</tr>
</table>
<span class="required-normal-note">* Denotes required parameter</span>
<table class="tabDataDisplayNoBorder">
	<tr>
		<td colspan="2">
		<div id="device_type_section_<%=deviceTypeId%>"></div>
		<div class="sectionIndex">
		<table class="tabDataDisplayNoBorder">
			<tr>
			<td align="left" valign="top">
				<b><%=deviceTypeDesc%> Section Index:</b>
			</td>
			<td>&nbsp;</td>
			<td align="left">
			<%
			int currentCategoryId = -1;
			for (Map.Entry<String, ConfigTemplateSetting> entry : defaultSettingData.entrySet()) {
				ConfigTemplateSetting defaultSetting = entry.getValue();
				int categoryId = defaultSetting.getCategoryId();
				String categoryName = defaultSetting.getCategoryName();
				if (StringHelper.isBlank(categoryName) || categoryId == currentCategoryId)
					continue;
				currentCategoryId = categoryId;
			%>
				<a href="#category_<%=deviceTypeId%>_<%=categoryId%>"><%=categoryName%></a><br/>
			<%} %>
			</td>
			</tr>
		</table>
		</div>
		</td>
	</tr>
</table>

<div class="spacer5"></div>

<table class="tabDataDisplayBorder">
	<col width="50%" />
	<col width="50%" />

<%
int categoryId = 0;
String currentCategoryName = "";
int counter = 0;
for (Map.Entry<String, ConfigTemplateSetting> entry : defaultSettingData.entrySet()) {
	ConfigTemplateSetting defaultSetting = entry.getValue();
	defaultSetting.setDeviceTypeId(deviceTypeId);
	boolean isServerOnly = defaultSetting.isServerOnly();
	String key = defaultSetting.getKey();
	int keyNumber;
	if (!isServerOnly && (isGx || isEdge))
		keyNumber = ConvertUtils.getIntSafely(key, -1);
	else
		keyNumber = -1;
	
	if (isGx && DevicesConstants.GX_MAP_PROTECTED_FIELDS.contains(keyNumber))
		continue;
	
	String label = defaultSetting.getLabel();
	String description = defaultSetting.getDescription();
	String lineDesc = defaultSetting.getLineDesc();
	boolean required = defaultSetting.isRequired();
	counter = defaultSetting.getCounter();
	boolean storedInPennies = "Y".equalsIgnoreCase(defaultSetting.getStoredInPennies());
	String defaultValue = defaultSetting.getConfigTemplateSettingValue();
	if (defaultValue == null)
		defaultValue = "";
	else if (storedInPennies && StringHelper.isNumeric(defaultValue))
		defaultValue = String.format("%.2f", new BigDecimal(defaultValue).divide(BigDecimal.valueOf(100)));
	
	String value = targetSettingData.get(key).getConfigTemplateSettingValue();
	if (value == null)
		value = "";
	else if (storedInPennies && StringHelper.isNumeric(value))
		value = String.format("%.2f", new BigDecimal(value).divide(BigDecimal.valueOf(100)));
		
		String categoryName = defaultSetting.getCategoryName();
		if (!StringHelper.isBlank(categoryName) && !categoryName.equals(currentCategoryName)) {
			currentCategoryName = categoryName;
			categoryId = defaultSetting.getCategoryId();
%>
			<tr class="tableHeader">
				<th colspan="2">
					<div id="category_<%=deviceTypeId%>_<%=categoryId%>" align="center">
					<%=StringUtils.prepareHTML(categoryName)%>
					<a href="#device_type_section_<%=deviceTypeId%>"><img src="/images/arrow-up-icon.png" style="vertical-align:bottom; border:0;"/></a>
					</div>
				</th>
			</tr>
<%
		}
%>
	<tr bgcolor="#FEFEFE">
		<td>
			<input type="checkbox" id="cb_<%=counter %>_cat_<%=deviceTypeId%>_<%=categoryId %>" value="<%=key %>" name="param_to_change_<%=deviceTypeId %>" />
			<b><%=StringUtils.prepareHTML(label)%><%if(required) {%><span class="required-normal">*</span><%}%></b>
		</td>
		<td valign="top" style="vertical-align:top;"><%=WebHelper.generateInput(defaultSetting, value, deviceTypeId, plv, true, defaultValue, true, null, keyPrefix)%></td>
	</tr>
	<tr>
		<td colspan="2" bgcolor="#EEEEEE">
			<%=description%><%if ((isEdge || isGx || isMEI) && !isServerOnly) out.write(WebHelper.generateDefaultValueString(defaultSetting, defaultValue, description));%>
		</td>
	</tr>
<%} %>

</table>

<div class="spacer15"></div>

<%}%>

<div align="center">
<table>
	<tr>
		<td valign="top" align="center">
		<input type="submit" class="cssButton" name="action" value="Save and Send"/>
		</td>
	</tr>
</table>
</div>

<div class="spacer15"></div>

</form>
</div>

<script defer="defer" type="text/javascript">
function validateTwoTierField(elem) {
	elem.value = trim(elem.value);
	if (!isNaN(elem.value) && parseFloat(elem.value) > 0.10) {
		invalidInput(elem, elem.getAttribute("label") + " value cannot be greater than 0.10. Please correct.");
		return false;
	}
	return true;
}
function change_schedule(item) {
	var plv = document.getElementById('plv').value;
	document.getElementById(item.name.replace("cfg_field_", "span_time_")).style.display = item.value == "" ? "none" : "inline-block";
	var schedules = (plv && plv < 20) ? ["I", "W"] : ["I", "W", "E"];
	for (var i = 0; i < schedules.length; i++) {
		if (document.getElementById(item.name.replace("cfg_field_", "span_" + schedules[i] + "_"))) {
			document.getElementById(item.name.replace("cfg_field_", "span_" + schedules[i] + "_")).style.display = item.value == schedules[i] ? "inline-block" : "none";
		}
	}
	if (item.value == 'E' && document.getElementById(item.name.replace("cfg_field_", "span_time_"))) {
		document.getElementById(item.name.replace("cfg_field_", "span_time_")).style.display = "none";
	}
	var timelist_elems = document.getElementsByName(item.name.replace("cfg_field_", "sch_timelist_"));
	for (var i=timelist_elems.length-1; i>=0; i--) {
		if (!timelist_elems[i].value) {
			var tr_elem = timelist_elems[i].parentNode;
			tr_elem.parentNode.remove(tr_elem);
			this[document.getElementById(item.name.replace("cfg_field_", "timeCount_"))] = this[document.getElementById(item.name.replace("cfg_field_", "timeCount_"))] - 1;
		}
	}
	if (document.getElementById(item.name.replace("cfg_field_", "add_time_button_"))) {
		document.getElementById(item.name.replace("cfg_field_", "add_time_button_")).disabled=(this[document.getElementById(item.name.replace("cfg_field_", "timeCount_"))] >= 6);
	}
}
function enableOptions(options, values, fn) {
	for(var i = 0; i < options.length; i++) {
		var valid = (values && values.contains && values.contains(fn ? fn(options[i].value) : options[i].value));
		options[i].disabled = !valid;
		if(!valid && options[i].selected)
			options[i].selected = false;
	}
}
function checkSupport(select) {
	select = $(select);
	if(select.hasClass("unsupported") && select.selectedIndex >= 0 && !select.options[select.selectedIndex].disabled)
		select.removeClass("unsupported");
}
function validateDeviceConfiguration(theForm) {
<%for (Integer deviceTypeId: deviceTypes.keySet()) {%>
	<%if (deviceTypeId == DeviceType.GX.getValue() || deviceTypeId == DeviceType.G4.getValue()) {%>
		if (!validateTwoTierField(theForm.elements["cfg_field_<%=deviceTypeId%>_362"]))
			return false;
	<%} else if (deviceTypeId == DeviceType.EDGE.getValue()) {%>
		var interfaceType = theForm.elements["cfg_field_<%=deviceTypeId%>_1500"].value;
		if (interfaceType == '3' || interfaceType == '4' || interfaceType == '5') {
			var elem = theForm.elements["cfg_field_<%=deviceTypeId%>_1200"];
			var elemValue = parseFloat(elem.value);
			if (elemValue < parseFloat(theForm.elements["cfg_field_<%=deviceTypeId%>_1503"].value) || elemValue < parseFloat(theForm.elements["cfg_field_<%=deviceTypeId%>_1506"].value)
					|| elemValue < parseFloat(theForm.elements["cfg_field_<%=deviceTypeId%>_1508"].value) || elemValue < parseFloat(theForm.elements["cfg_field_<%=deviceTypeId%>_1510"].value)
					|| elemValue < parseFloat(theForm.elements["cfg_field_<%=deviceTypeId%>_1512"].value) || elemValue < parseFloat(theForm.elements["cfg_field_<%=deviceTypeId%>_1514"].value)
					|| elemValue < parseFloat(theForm.elements["cfg_field_<%=deviceTypeId%>_1516"].value) || elemValue < parseFloat(theForm.elements["cfg_field_<%=deviceTypeId%>_1518"].value)
					|| elemValue < parseFloat(theForm.elements["cfg_field_<%=deviceTypeId%>_1520"].value) || elemValue < parseFloat(theForm.elements["cfg_field_<%=deviceTypeId%>_1523"].value)) {
				invalidInput(elem, elem.getAttribute("label") + " cannot be less than a Coin Pulse monetary value. Please correct.");
				return false;
			}
		}
		if (!validateTwoTierField(theForm.elements["cfg_field_<%=deviceTypeId%>_1202"]))
			return false;
	<%}%>
<%}%>
	return true;
}
</script>

<jsp:include page="include/footer.jsp"/>