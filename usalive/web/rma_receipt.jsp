<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.RequestUtils"%>
<% InputForm inputForm = RequestUtils.getInputForm(request);
	int rmaTypeId=inputForm.getInt("rmaTypeId", true, 1); %>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8">
		<title></title>
		<style type="text/css">
			table.folio {
				border-spacing: 0;
				border-collapse: collapse;
				page-break-after: auto;
				width: 100%;
			}
			.headerRow {
				background-color: #2763AA;
				text-align: center;
				color: #fff;
			}
			.instruct, .instructions {
				font-weight: bold;
			}
			.headerRowRMA {
				background-color:  	#C0C0C0;
				text-align: left;
				color:  #000000;
			}
			.rmaBottom {
				font-style: italic;
				font-weight:bold;
				text-align: center;
			}
		</style>
	</head>
	<body>
	<% if (rmaTypeId == 1) { %>
		<jsp:include page="rma_rental_for_credit_display.jsp"/>
	<% } else if (rmaTypeId == 2) { %>
		<jsp:include page="rma_all_device_display.jsp"/>
	<% } else if (rmaTypeId == 3) { %>
		<jsp:include page="rma_parts_display.jsp"/>
	<% } else if(rmaTypeId == 4 || rmaTypeId == 5 || rmaTypeId == 6) { %>
		<jsp:include page="rma_att_display.jsp"/>
	<% }; %>
	</body>
</html>
