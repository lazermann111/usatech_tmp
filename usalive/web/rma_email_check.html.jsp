<%@page import="simple.io.Log"%>
<%@page import="javax.mail.MessagingException"%>
<%@page import="com.usatech.layers.common.util.WebHelper"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.RequestUtils,simple.results.Results"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.db.DataLayerMgr"%>
<script type="text/javascript">
<%
Log log = Log.getLog();
InputForm inputForm = RequestUtils.getInputForm(request);
String email=inputForm.getString("email", false);
if(!StringUtils.isBlank(email)){
	try{
		String[] emails=email.split(";");
		for(String email1:emails){
			WebHelper.checkEmail(email1);
		}
		
		%>emailInvalid=0;<%
	}catch(MessagingException e){
		log.error("Failed to requests RMA due to failing email check", e);
		%>emailInvalid=1;<%
	}
}
%>
</script>
