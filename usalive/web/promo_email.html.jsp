<%@page import="simple.xml.sax.MessagesInputSource"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="simple.servlet.CalendarHelper"%>
<%@page import="java.util.Calendar"%>
<%@page import="simple.io.Log"%>
<%@page import="com.usatech.usalive.web.CampaignUsaliveUtils"%>
<%@page import="javax.mail.MessagingException"%>
<%@page import="com.usatech.layers.common.util.WebHelper"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="com.usatech.usalive.servlet.USALiveUtils"%>
<%@page import="simple.servlet.ResponseUtils"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>

<%
	Log log = Log.getLog();
	UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
	long campaignId = ConvertUtils.getInt(RequestUtils.getAttribute(request, "campaignId", true));
	boolean hasPri=false;
	if(user.isInternal()){
		if(user.hasPrivilege(ReportingPrivilege.PRIV_CUSTOMER_SERVICE)){
			hasPri=true;
		}
	}else{
		if(user.hasPrivilege(ReportingPrivilege.PRIV_MANAGE_CAMPAIGN)){
			hasPri=true;
		}
	}
	Calendar calendar=Calendar.getInstance();
	int year=-1;
	
	if(hasPri){
		InputForm inputForm = RequestUtils.getInputForm(request);
		String actionType=inputForm.getString("actionType", false);
		CalendarHelper calHelper=RequestUtils.getCalendarHelper(request, null, null);
		year=ConvertUtils.getIntSafely(inputForm.get("year"),calendar.get(Calendar.YEAR));
		int month=ConvertUtils.getIntSafely(inputForm.get("month"),calendar.get(Calendar.MONTH));
		int day=ConvertUtils.getIntSafely(inputForm.get("day"),calendar.get(Calendar.DAY_OF_MONTH));
		int hour=ConvertUtils.getIntSafely(inputForm.get("hour"),calendar.get(Calendar.HOUR_OF_DAY));
		int minute=ConvertUtils.getIntSafely(inputForm.get("minute"),calendar.get(Calendar.MINUTE));
		MessagesInputSource messages=RequestUtils.getOrCreateMessagesSource(request);
		if(!StringUtils.isBlank(actionType)){
			if(actionType.equalsIgnoreCase("testsend")){
				String email=inputForm.getString("email", true);
				if(StringUtils.isBlank(email)){
					messages.addMessage("error", "promo_invalid_email", "This is an invalid email address. Please input a valid one");
				}else{
					email=email.trim();
					try{
						WebHelper.checkEmail(email);
					}catch(MessagingException e){
						messages.addMessage("error", "promo_invalid_email", "This is an invalid email address. Please input a valid one");
					}
					Long consumerId=CampaignUsaliveUtils.chooseMarketingEmailConsumer(campaignId);
					if(consumerId==null){
						messages.addMessage("error", "promo_no_consumer_acct", "Failed to request a test campaign email. There is no consumer card associated with this campaign. Please assign it to cards first");
					}else{
						inputForm.set("email",email);
						RequestUtils.setAttribute(request, "email", email, "request"); 
						CampaignUsaliveUtils.addCampaignBlast(campaignId, consumerId, user, inputForm);
						messages.addMessage("success", "promo_test_request_success", "Test email is scheduled successfully");
					}
				}
				
			}else if(actionType.equalsIgnoreCase("send")){
				Results camPromoTextStatusResult=DataLayerMgr.executeQuery("GET_CAM_PROMO_TEXT_STATUS", new Object[] { campaignId});
				boolean needApproval=false;
				if(camPromoTextStatusResult.next()){
					String status=ConvertUtils.getStringSafely(camPromoTextStatusResult.get("status"));
					if(status.equalsIgnoreCase("N")){
						try{
							needApproval=true;
							Long consumerId=CampaignUsaliveUtils.chooseMarketingEmailConsumer(campaignId);
							if(consumerId==null){
								messages.addMessage("error", "promo_no_consumer_acct_approve", "Failed to submit for approval to the USA technologies Marketing Department. There is no consumer card associated with this campaign. Please assign it to cards first");		
							}else{
								inputForm.set("status","P");
								DataLayerMgr.executeCall("REQUEST_PROMO_TEXT_APPROVE", inputForm, true);
								inputForm.set("toMarketing","Y");
								CampaignUsaliveUtils.addCampaignBlast(campaignId, consumerId, user, inputForm);
								messages.addMessage("success", "promo_approve_request_success", "Your Campaign email has been submitted for approval to the USA technologies Marketing Department");
							}
						}catch(Exception e){
							log.error("Failed to request approval for campaignId="+campaignId,e);
							messages.addMessage("error", "promo_custom_text_approve_failure", "Failed to request approval to the USA technologies Marketing Department. Please try again");
						}
					}else if(status.equalsIgnoreCase("P")){
						needApproval=true;
					}
				}
	    				inputForm.set("toMarketing","N");
	    				inputForm.set("sendTime",WebHelper.getDate(year, month, day, hour, minute, 0,inputForm.getStringSafely("timeZoneGuid", "US/Eastern")));
	    				CampaignUsaliveUtils.addCampaignBlast(campaignId, null, user, inputForm);
	    				if(needApproval){
		    				messages.addMessage("success", "promo_blast_request_success", "Campaign email schedule is set. Waiting for USAT approval");
	    				}else{
	    					messages.addMessage("success", "promo_blast_request_success", "Campaign email schedule is set");
	    				}
				
			}else if(actionType.equalsIgnoreCase("addText")){
				try{
					if(StringUtils.isBlank(ConvertUtils.getStringSafely(inputForm.get("promoText")))){
						messages.addMessage("error", "promo_custom_text_no_content", "Failed.Please input custom text. Please try again");
					}else{
						DataLayerMgr.executeCall("INSERT_PROMO_TEXT", inputForm, true);
						messages.addMessage("success", "promo_text_add_success", "Custom text added successfully");
					}
				}catch(Exception e){
					log.error("Failed to add custom text for campaignId="+campaignId,e);
					messages.addMessage("error", "promo_custom_text_add_failure", "Failed to add custom text. The maximum size of the text cannot exceed 4000. Please try again");		
				}
			}else if(actionType.equalsIgnoreCase("editText")){
				try{
					if(StringUtils.isBlank(ConvertUtils.getStringSafely(inputForm.get("promoText")))){
						messages.addMessage("error", "promo_custom_text_edit_no_content", "Failed.Please input custom text. Please try again");
					}else{
						DataLayerMgr.executeCall("UPDATE_PROMO_TEXT", inputForm, true);
						messages.addMessage("success", "promo_text_edit_success", "Custom text updated successfully");
					}
				}catch(Exception e){
					log.error("Failed to edit custom text for campaignId="+campaignId,e);
					messages.addMessage("error", "promo_custom_text_edit_failure", "Failed to edit custom text. The maximum size of the text cannot exceed 4000. Please try again");		
				}
			}else if(actionType.equalsIgnoreCase("deleteText")){
				try{
						DataLayerMgr.executeCall("DELETE_PROMO_TEXT", inputForm, true);
						messages.addMessage("success", "promo_text_delete_success", "Custom text deleted successfully");
				}catch(Exception e){
					log.error("Failed to delete custom text for campaignId="+campaignId,e);
					messages.addMessage("error", "promo_custom_text_delete_failure", "Failed to delete custom text. Please try again");
				}
			}else if(actionType.equalsIgnoreCase("approveRequest")){
				try{
					Long consumerId=CampaignUsaliveUtils.chooseMarketingEmailConsumer(campaignId);
					if(consumerId==null){
						messages.addMessage("error", "promo_no_consumer_acct_approve", "Failed to submit for approval to the USA technologies Marketing Department. There is no consumer card associated with this campaign. Please assign it to cards first");		
					}else{
						inputForm.set("status","P");
						DataLayerMgr.executeCall("REQUEST_PROMO_TEXT_APPROVE", inputForm, true);
						inputForm.set("toMarketing","Y");
						CampaignUsaliveUtils.addCampaignBlast(campaignId, consumerId, user, inputForm);
						messages.addMessage("success", "promo_approve_request_success", "Your Campaign email has been submitted for approval to the USA technologies Marketing Department");
					}
				}catch(Exception e){
					log.error("Failed to request approval for campaignId="+campaignId,e);
					messages.addMessage("error", "promo_custom_text_approve_failure", "Failed to request approval to the USA technologies Marketing Department. Please try again");
				}
			}else if(actionType.equalsIgnoreCase("cancelBlast")){
				try{
						int updateCount=DataLayerMgr.executeUpdate("CANCEL_CAMPAIGN_BLAST", inputForm, true);
						if(updateCount==1){
							messages.addMessage("success", "promo_blast_cancel_success", "Campaign email blast cancelled successfully");
						}else{
							messages.addMessage("error", "promo_blast_cancel_failure", "Failed to cancel campaign email blast. It is already sent or cancelled");
						}
				}catch(Exception e){
					log.error("Failed to cancel campaign email blast for campaignId="+campaignId,e);
					messages.addMessage("error", "promo_blast_cancel_failure", "Failed to cancel campaign email blast. Please try again");
				}
			}else if(actionType.equalsIgnoreCase("updateEmail")){
				String email=inputForm.getString("email", true);
				if(StringUtils.isBlank(email)){
					messages.addMessage("error", "promo_invalid_email", "This is an invalid email address. Please input a valid one");
				}else{
					email=email.trim();
					try{
						WebHelper.checkEmail(email);
						try{
							DataLayerMgr.executeUpdate("UPDATE_USER_EMAIL", inputForm, true);
							messages.addMessage("success", "usalive_user_email_update_success", "User email updated successfully");
							user.setEmail(email);
						}catch(Exception e){
							log.error("Failed to update user email", e);
							messages.addMessage("error", "usalive_user_email_update_failure", "Failed to update user email. Please try again");
						}
					}catch(MessagingException e){
						messages.addMessage("error", "promo_invalid_email", "This is an invalid email address. Please input a valid one");
					}
				}
				
			}
		}
		
		RequestUtils.redirectWithCarryOver(request, response, "manage_promo_email.html?campaignId="+campaignId, false, true);
	}
%>
