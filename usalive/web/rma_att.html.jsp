<%@page import="com.usatech.usalive.web.RMAType"%>
<%@page import="com.usatech.usalive.servlet.USALiveUtils"%>
<%@page import="simple.servlet.ResponseUtils"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="simple.text.StringUtils"%>

<% UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
	boolean hasPri = false;
	if((user.isInternal() && user.hasPrivilege(ReportingPrivilege.PRIV_CUSTOMER_SERVICE)) || user.hasPrivilege(ReportingPrivilege.PRIV_RMA)) {
		hasPri = true;
	}
	int rmaTypeId = RequestUtils.getAttribute(request, "rmaTypeId", Integer.class, true);
	if (rmaTypeId != 5 && rmaTypeId != 6) {
		throw new ServletException("Invalid RMA type: " + rmaTypeId);
	} 
	RMAType rmaType = RMAType.getByValue(rmaTypeId); %>
<jsp:include page="include/header.jsp"/>
<% if (hasPri) { %>
<span class="caption"><%=rmaType.getDescription() %></span>
<script type="text/javascript" src="<%=RequestUtils.addLastModifiedToUri(request, null, "scripts/utils.js") %>"></script>
<script type="text/javascript">
var emailInvalid = 0;
function onSearch() {
	$("message").set("html", "");
	$("waitImage").style.display = 'block';
	$("searchResultSection").style.display = 'block';
	$("deviceSerialCd").value = "";
	$("addSelect").style.display = 'block';
	$("removeSelect").style.display = 'none';
	if ($('deviceSearchByText').value && $('deviceSearchByText').value.trim()!="") {
			$("deviceSerialCd").value = $('deviceSearchByText').value;
	}
	
	new Request.HTML({ 
		url: "rma_device_search.html",
		data: $("searchForm"), 
		method: 'post',
		update : $("searchResultSection"),
		onComplete : function() {
			$("waitImage").style.display='none';
		},
		evalScripts: true
	}).send();
}

function onAddSelected() {
	var count = 0;
	$('searchResultSection').getElements('input[type="checkbox"][name="rmaDevices"]:checked').each(function (el) {
		//alert(el.getParent().getParent().get("data-return-device-id"));
		count++;
		if (el.id!="selectAllCheckbox" && $('currentReturnTable').getElements('input[type="checkbox"][value="'+el.value+'"]').length == 0) {
			var copy = el.getParent().getParent().clone();
			copy.class = "evenRowHover";
			$('currentReturnTable').adopt(copy);
		}
	});
	if (count > 0) {
		$("message").set("html", "<p class='status-info-success'>Successfully added to RMA</p>");
	}
}

function onRemoveSelected() {
	var count = 0;
	$('currentReturnSection').getElements('input[type="checkbox"][name="rmaDevices"]:checked').each(function (el) {
		//alert(el.getParent().getParent().get("data-return-device-id"));
		if (el.id!="selectAllReturnCheckbox") {
			el.getParent().getParent().dispose();
		}
		count++;
	});
	if (count > 0) {
		$("message").set("html", "<p class='status-info-success'>Successfully removed from RMA</p>");
	}
}

function onSelectAllCheckbox() {
	$("message").set("html", "");
	if ($("selectAllCheckbox").checked) {
		$('searchResultSection').getElements("input[type=checkbox][name=rmaDevices])").each(function(el) {
			el.checked = true;
		});
	} else {
		$('searchResultSection').getElements("input[type=checkbox][name=rmaDevices])").each(function(el) {
			el.checked = false;
		});
	}
}

function onSelectAllReturnCheckbox() {
	$("message").set("html", "");
	if ($("selectAllReturnCheckbox").checked) {
		$('currentReturnSection').getElements("input[type=checkbox][name=rmaDevices])").each(function(el) {
			el.checked = true;
		});
	} else {
		$('currentReturnSection').getElements("input[type=checkbox][name=rmaDevices])").each(function(el) {
			el.checked = false;
		});
	}
}

function onCreateRMA() {
	if ($('currentReturnSection').getElements('input[type="checkbox"][name="rmaDevices"]').length==0) {
		alert("You need to have at least one device for the current RMA. Please search and select. Thank you.");
		return false;
	} else {
		$('currentReturnSection').getElements('input[type="checkbox"][name="rmaDevices"]').each(function (el) {
			el.checked=true;
		});
		if ($("rmaShippingAddrId") && $("rmaShippingAddrId").selectedIndex==-1) {
			$("shippingAddrName").value = $("shippingAddrName").value.trim();
			var isNameInvalid = 0;
			$('rmaShippingAddrId').getElements("option").each(function(el) {
				if($("shippingAddrName").value == el.getText()) {
					alert("Please enter unique shipping address name.");
					isNameInvalid = 1;
					return;
				}
			});
			if (isNameInvalid == 1) {
				return false;
			}
		}
		if (!document.getElements("input[class=validation-failed]")) {
			$("email").disabled = "";
		}
		$("email").value=cleanEmail($("email").value);
		new Request.HTML({ 
			url: "rma_email_check.html",
			method: 'post',
			data: $("returnForm"), 
			evalScripts: true,
			async: false
		}).send();
		
		if (emailInvalid == 1) {
			$("checkEmailMsg").set("html", "Invalid emails. Please enter valid ones.");
			$("checkEmailMsg").set("class", "status-info-failure");
			return false;
		} else {
			$("checkEmailMsg").set("html", "");
			$("checkEmailMsg").set("class", "");
			return true;
		}
	}
}

function toggleAgree(input) {
	input = $(input);
	if(input.checked)
		$("createRMA").set('disabled', '');
	else
		$("createRMA").set('disabled', 'disabled');
}

window.addEvent('domready', function() {
	new Form.Validator.Inline.Mask(document.returnForm);
	new Request.HTML({
		url: "rma_carrier_account_select.html",
		method: 'post',
		update : $("userShippingCarrier"),
		evalScripts: true
	}).send();
	new Request.HTML({ 
		url: "rma_shipping_address_select.html",
		method: 'post',
		update : $("rmaShippingAddr"),
		evalScripts: true
	}).send();
	onNeedReplacement();
	$("postalCd").set("class", "");
	$("advice-required-postalCd").dispose();
	$("replacementChoiceSection").set('style', 'display:none');
});
</script>
<div class="topmessage">
	<div id="message" class="manageCampaignMessage" style="text-align:left;"></div>
</div>
<form name="searchForm" id="searchForm">
	<input type="hidden" id="fragment" name="fragment" value="true"/>
	<input type="hidden" id="deviceSerialCd" name="deviceSerialCd" value=""/>
	<input type="hidden" id="rmaTypeId" name="rmaTypeId" value="<%=rmaTypeId%>"/>
	<div id="searchResultSection">
		<jsp:include page="rma_device_search.html.jsp"></jsp:include>
	</div>
	<table>
		<tr>
			<td><input id="addSelect" type="button" onclick="onAddSelected();" value="Add selected"/></td>
			<% if (rmaTypeId == 5) { %>
			<td class="required">* A $29 fee applies to each Like for Like device exchange.</td>
			<% } %>
		</tr>
	</table>
</form>
&nbsp;
<form name="returnForm" method="POST" id="returnForm" action="rma_create_device.html" onSubmit="return onCreateRMA();">
<% ResponseUtils.writeXsfrFormProtection(pageContext);
	USALiveUtils.writeFormProfileId(pageContext); %>
	<input type="hidden" id="rmaTypeId" name="rmaTypeId" value="<%=rmaTypeId%>"/>
	<div id="currentReturnSection">
		<table id="currentReturnTable" class="folio">
			<tr class="headerRow">
				<th colspan="7">Devices For Current RMA</th>
			</tr>
			<tr class="headerRow">
				<th><input id="selectAllReturnCheckbox" type="checkbox" onclick="onSelectAllReturnCheckbox();"></th>
				<th><a title="Sort Rows by Device Serial Number" data-toggle="sort" data-sort-type="STRING">Device Serial Number</a></th>
				<th><a title="Sort Rows by Region Name" data-toggle="sort" data-sort-type="STRING">Region Name</a></th>
				<th><a title="Sort Rows by location" data-toggle="sort" data-sort-type="STRING">Location</a></th>
				<th><a title="Sort Rows by Asset Nbr" data-toggle="sort" data-sort-type="STRING">Asset Nbr</a></th>
				<th><a title="Sort Rows by Rental" data-toggle="sort" data-sort-type="STRING">Rental</a></th>
				<th><a title="Sort Rows by Rental Fee" data-toggle="sort" data-sort-type="STRING">Rental Fee</a></th>
			</tr>
		</table>
	</div>
	<input id="removeSelect" type="button" onclick="onRemoveSelected();" value="Remove selected"/><br/>
	&nbsp;
	<jsp:include page="rma_replacement.jsp"/>
	<table>
		<tr>
			<td style="padding-right:1em;"><input id="createRMA" type="submit" value="Create RMA" <% if (rmaTypeId == 5) { %>disabled="disabled" <% } %>/></td>
			<% if (rmaTypeId == 5) { %>
			<td>
				<div class="" style="padding:.25em 1em .25em 1em; border:1px solid #2763AA;">
					<input type="checkbox" id="rmaAttFeeAgree" name="rmaAttFeeAgree" value="Y" onchange="toggleAgree(this)">
					<label for="rmaAttFeeAgree">* I agree USA Technologies will bill my account $29 per device exchange.</label>
				</div>
			</td>
			<% } %>
		</tr>
	</table>
</form>
<% } else { %>
	<p class="not-authorized">User '<%=StringUtils.prepareHTML(user.getUserName()) %>' does not have permission to view this page. Please contact USA Technologies if you need access.</p>
<% } %>
<jsp:include page="include/footer.jsp"/>
