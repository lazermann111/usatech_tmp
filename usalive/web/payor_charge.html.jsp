<%@page import="simple.text.StringUtils"%>
<%@page import="com.usatech.report.ReportingUser.ActionType"%>
<%@page import="com.usatech.report.ReportingUser.ResourceType"%>
<%@page import="simple.servlet.NotAuthorizedException"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="com.usatech.usalive.web.PayorUtils"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
if(!user.hasPrivilege(ReportingPrivilege.PRIV_BACK_OFFICE))
    throw new NotAuthorizedException("You are not authorized to perform this function.");
if("POST".equalsIgnoreCase(request.getMethod())) {
	long payorId = RequestUtils.getAttribute(request, "payorId", Long.class, true);
	user.checkPermission(ResourceType.PAYOR, ActionType.USE, payorId);
	BigDecimal chargeAmount = RequestUtils.getAttribute(request, "chargeAmount", BigDecimal.class, true);
	String chargeReference = RequestUtils.getAttribute(request, "chargeReference", String.class, false);        
	String chargeDesc = RequestUtils.getAttribute(request, "chargeDesc", String.class, false);
	String payorEmail = RequestUtils.getAttribute(request, "payorEmail", String.class, false);
	Boolean emailReceipt = RequestUtils.getAttribute(request, "receiptFlag", Boolean.class, false);
	long lastChargeTime = RequestUtils.getAttributeDefault(request, "lastChargeTime", Long.class, 0L);
	PayorUtils.chargePayor(user, payorId, payorEmail, chargeAmount, chargeReference, chargeDesc, lastChargeTime, emailReceipt != null && emailReceipt.booleanValue() && !StringUtils.isBlank(payorEmail), request);
}
PayorUtils.redirectToReferrer(request, response, "payor_config.html");
%>