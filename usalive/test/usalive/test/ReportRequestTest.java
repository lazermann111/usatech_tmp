package usalive.test;

import java.beans.IntrospectionException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import com.sshtools.j2ssh.sftp.SftpFileInputStream;
import com.usatech.report.ReportRequest;

import simple.bean.ConvertException;
import simple.db.BasicDataSourceFactory;
import simple.db.CacheableCall;
import simple.db.DataLayerMgr;
import simple.db.config.ConfigException;
import simple.db.config.ConfigLoader;
import simple.db.specific.DbSpecific;
import simple.db.specific.OracleSpecific;
import simple.falcon.engine.DesignException;
import simple.falcon.engine.ExecuteEngine;
import simple.falcon.engine.Folio;
import simple.falcon.engine.Param;
import simple.falcon.engine.Report;
import simple.falcon.engine.Scene;
import simple.falcon.engine.standard.StandardDesignEngine2;
import simple.falcon.engine.standard.StandardExecuteEngine;
import simple.falcon.run.BasicExecutor;
import simple.falcon.run.ReportRunner;
import simple.io.ConfigSource;
import simple.io.IOUtils;
import simple.io.LoadingException;
import simple.io.Log;
import simple.io.resource.Resource;
import simple.io.resource.ResourceMode;
import simple.io.resource.sftp.SftpResourceFolder;
import simple.results.Results;
import simple.sql.BaseMutableExpression;
import simple.sql.MutableExpression;
import simple.test.UnitTest;
import simple.text.StringUtils;
import simple.xml.ObjectBuilder;
import simple.xml.XMLBuilder;
public class ReportRequestTest extends UnitTest{

	@Before
	public void setUp() throws Exception {
		setupLog();
		try {
            ConfigLoader.loadConfig(ConfigSource.createConfigSource("../xsl/report-data-layer.xml"));
        } catch(ConfigException e) {
        	e.printStackTrace();
        } catch(LoadingException e) {
        	e.printStackTrace();
        } catch(java.io.IOException e) {
        	e.printStackTrace();
        }
		BasicDataSourceFactory dsf = new BasicDataSourceFactory();
	    dsf.addDataSource("REPORT", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=DEVDBP02)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=USADEV04.WORLD)))", "USALIVE_APP", "USALIVE_APP");
	    dsf.addDataSource("METADATA", "oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=DEVDBP02)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=USADEV04.WORLD)))", "FOLIO_CONF", "FOLIO_CONF");
	    DataLayerMgr.getGlobalDataLayer().setDataSourceFactory(dsf);
	}

	@Test
	public void writeReportSQL() throws DesignException, InstantiationException, IllegalAccessException, ClassNotFoundException, IntrospectionException, InvocationTargetException, ConvertException, SAXException, ParseException, IOException, ParserConfigurationException {
		ExecuteEngine ee2 = new StandardExecuteEngine(new StandardDesignEngine2());
		ee2.setDesignDataSourceName("REPORT");
        //ee2.initialize();
        Report report = ee2.getDesignEngine().newReport();
        report.readXML(new ObjectBuilder(new FileInputStream("C:\\Users\\bkrug\\Documents\\report_request_3222.xml")));
        int i = 1;
        for(Folio folio : report.getFolios()) {
	        CacheableCall call = ee2.getDesignEngine().getCall(folio, new long[] {1});
	        log.info("Folio #" + i++ + " SQL is:\n" + call.getSql());
        }
    }
	@Test
	public void writeReportXML() throws DesignException, InstantiationException, IllegalAccessException, ClassNotFoundException, IntrospectionException, InvocationTargetException, ConvertException, SAXException, ParseException, IOException {
		ExecuteEngine ee2 = new StandardExecuteEngine(new StandardDesignEngine2());
		ReportRunner reportRunner = new ReportRunner(ee2);
		ee2.setDesignDataSourceName("REPORT");
        //ee2.initialize();
        //Report report = reportRunner.getReportFromFolio(441, new Scene());
        Report report = reportRunner.getReport(2, new Scene());
        String reportXML = XMLBuilder.toXML(report);
        log.debug("Report XML is " + reportXML.length() + " characters");
        byte[] compressedValue = IOUtils.compressByteArray(reportXML.getBytes(Charset.forName("UTF-8")));
		log.debug("Compressed is " + compressedValue.length + " bytes");

    }
	@Test
	public void checkReportRequestHashes() throws Exception {
		ExecuteEngine ee2 = new StandardExecuteEngine(new StandardDesignEngine2());
		ReportRunner reportRunner = new ReportRunner(ee2);
		ee2.setDesignDataSourceName("REPORT");
        //ee2.initialize();
        Scene scene = new Scene();
        BasicExecutor executor = new BasicExecutor();
        SortedMap<String,String> params = new TreeMap<String, String>();
        SortedMap<String,String> params2 = new TreeMap<String, String>();
        PrintWriter out = new PrintWriter("C:\\Documents and Settings\\bkrug\\My Documents\\Folio-Hashes-3.csv");
		StringUtils.writeCSVLine(new String[] {"FOLIO_ID", "FOLIO_HASH_CODE", "FILTER_HASH_CODE", "FILTER_EXPRESSION", "PILLARS_HASH_CODE", "REPORT_HASH_CODE", "REPORT_HASH_CODE_HEX", "PARAMS", "PARAMS_HEX", "REPORT_AND_PARAMS_HASH", "REPORT_AND_PARAMS_HASH_HEX", "PARAMS_2", "PARAMS_HEX_2", "REPORT_AND_PARAMS_HASH_2", "REPORT_AND_PARAMS_HASH_HEX_2", "CONSISTENT_EQUALS", "CONSISTENT_HASH"}, out);
		Results results = DataLayerMgr.executeQuery("GET_FOLIOS", null);
		int v = 1;
		DbSpecific dbSpecific = new OracleSpecific();
        while(results.next()) {
        	long folioId = results.getValue("folioId", Long.class);
        	Report report;
        	try {
        		report = reportRunner.getReportFromFolio(folioId, scene);
        	} catch(Exception e) {
        		log.error("Could not get report for folioId = " + folioId, e);
        		continue;
        	}
        	Report report2;
        	try {
        		report2 = reportRunner.getReportFromFolio(folioId, scene);
        	} catch(Exception e) {
        		log.error("Could not get report for folioId = " + folioId, e);
        		continue;
        	}

        	Param[] folioParams = report.getFolios()[0].getFilter().gatherParams();
        	for(Param p : folioParams)
        		params2.put(p.getName(), String.valueOf(v++));
        	ReportRequest<Report> reportRequest = new ReportRequest<Report>(null, report, scene, executor.getUserId(), executor.getUserId(), params, "Test");
        	ReportRequest<Report> reportRequest2 = new ReportRequest<Report>(null, report, scene, executor.getUserId(), executor.getUserId(), params2, "Test2");
        	//MutableExpression filterExp = new BaseMutableExpression(new HashSet<Column>(), new ArrayList<Object>());
        	MutableExpression filterExp = new BaseMutableExpression(new ArrayList<Object>());
        	report.getFolios()[0].getFilter().buildExpression(filterExp, dbSpecific);
            StringUtils.writeCSVLine(new Object[] {folioId, report.getFolios()[0].hashCode(), report.getFolios()[0].getFilter().hashCode(), filterExp, Arrays.deepHashCode(report.getFolios()[0].getPillars()), report.hashCode(), Integer.toHexString(report.hashCode()), reportRequest.getParameters().toString(), Integer.toHexString(reportRequest.getParameters().hashCode()), reportRequest.getReportAndParamsHash(), Long.toHexString(reportRequest.getReportAndParamsHash()), reportRequest2.getParameters().toString(), Integer.toHexString(reportRequest2.getParameters().hashCode()), reportRequest2.getReportAndParamsHash(), Long.toHexString(reportRequest2.getReportAndParamsHash()), report.equals(report2), report.hashCode() == report2.hashCode()}, out);
            log.info("Folio " + folioId + " has hash of " + report.hashCode());
        }
        out.flush();
		out.close();
    }
	/*
	//@Test
	public void makeRequest() throws UnsupportedEncodingException, DataFormatException, ExecutionException, ServiceException, ConvertException{
		Properties prop=new Properties();
		AsyncReportEngine arEngine=new AsyncReportEngine();
		Map<String, Object>parameters=new HashMap<String,Object>();
		parameters.put("folioId", 244);
		parameters.put("promptForParams", "fix");
		parameters.put("BatchId", 49964);
		String baseUrl="http://localhost:8080/CustomReporting/frame.i";
		Locale locale=new Locale("en","","");
		ReportRequest key=new ReportRequest(baseUrl, parameters, locale.toString(), 7, "", 932);
		int requestId=arEngine.getReportRequestId(key, "Transaction Export");
		log.debug("request id is:"+requestId);
		log.debug("###################");
		requestId=arEngine.getReportRequestId(key);
		log.debug("request id is:"+requestId);

		StoredFileInfo fileResource=arEngine.getReportResult(requestId);
		log.debug("fileResource is:"+fileResource);
		fileResource=arEngine.getReportResult(requestId);
		log.debug("fileResource is:"+fileResource);
	}
	*/
	//@Test
	public void testSelectInto() throws Exception{
        Map<String, Integer> params=new HashMap<String, Integer>();
        params.put("fileId", 1);
        DataLayerMgr.selectInto("GET_STORED_FILE_INFO_FOR_RESOURCE",params);
	}
	//@Test
	public void testSftpFiles() throws Exception{
		try{

		SftpResourceFolder sftpFolder=new SftpResourceFolder("usadev01.usatech.com", "yhe", "Emma052008", "/home/yhe/reporttest");
		String fileName="test";
		for(int i=0; i<1000; i++){
			Resource resource=sftpFolder.getResource("test2/"+fileName+i,ResourceMode.CREATE);
			FileInputStream fis=new FileInputStream("c:\\public\\temp\\test.txt");
			IOUtils.copy(fis, resource.getOutputStream());
			//PrintWriter out=new PrintWriter(resource.getOutputStream());
			//out.write("this is a test for sftp resource folder");
			//out.flush();
			//out.close();
			fis.close();
			System.out.println("writeloop:"+i);
			//sftpFolder.close();
		}

		}catch(IOException ioe){
			ioe.printStackTrace();
		}
	}
	@Test
	public void testSftpFilesRead() throws Exception{
		try{
			int fileNum=1000;
			Resource[] resourceArray=new Resource[fileNum];
			int count=0;
		SftpResourceFolder sftpFolder=new SftpResourceFolder("usadev01.usatech.com", "yhe", "Emma052008", "/home/yhe/reporttest/test");


		for(int i=101; i<fileNum; i++){
			String fileName="test"+i;

			System.out.println("fileName:"+fileName);
			resourceArray[count]=sftpFolder.getResource(fileName, ResourceMode.READ);
			count++;
		}
		for(int i=0; i<fileNum; i++){
			SftpFileInputStream is=(SftpFileInputStream)resourceArray[i].getInputStream();
			IOUtils.copy(is, System.out);
			System.out.println("readLoop:"+i);
		}

		}catch(IOException ioe){
			Thread.sleep(200000);
			ioe.printStackTrace();
		}
	}
	@After
	public void tearDown() throws Exception {
		Log.finish();
	}
}
