package usalive.test;

import java.io.StringWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.swing.JOptionPane;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.axis2.client.Options;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.axis2.transport.http.HttpTransportProperties;
import org.junit.Test;

import com.thoughtworks.xstream.XStream;
import com.usatech.layers.common.constants.DEXReason;
import com.usatech.transport.soap.vdi.VDIDexUploadInput;
import com.usatech.usalive.vdi.VdiStub;
import com.usatech.usalive.vdi.VdiStub.ArrayOfString;
import com.usatech.usalive.vdi.VdiStub.GetDex;
import com.usatech.usalive.vdi.VdiStub.GetDexResponse;
import com.usatech.usalive.vdi.VdiStub.UploadDex;
import com.usatech.usalive.vdi.VdiStub.UploadDexResponse;
import com.usatech.usalive.vdi.VdiStub.VDIReturnType;
import com.usatech.usalive.vdi.VdiStub.VDITransactionType;

import simple.io.resource.ByteArrayResource;
import simple.swt.UIUtils;
import simple.text.MinimalCDataCharset;
import simple.xml.TemplatesLoader;
import simple.xml.sax.BeanAdapter1;
import simple.xml.sax.BeanInputSource;


public class VdiSoapTest {
	//public static final String VDI_SOAP_URL = "http://localhost:8880/soap/vdi";
	public static final String VDI_SOAP_URL;
	public static final String LIB_DIRECTORY = "../ePortConnectClient/lib";
	public static final String USERNAME = "vditest";
	public static final String PASSWORD = "VDITesting1234";
	
	protected final static XStream xstream = new XStream();	
	protected static VdiStub vdi = null;
	
	static {
		System.setProperty("app.servicename", "VDITest");
		System.setProperty("javax.net.ssl.trustStore", "../ServerLayers/LayersCommon/conf/net/truststore.ts");
		System.setProperty("javax.net.ssl.trustStorePassword", "usatech");
		boolean secure = true;
		String hostName = "usalive-int.usatech.com";/*
		try {
			hostName = InetAddress.getLocalHost().getHostName() + ":8880";
		} catch(UnknownHostException e) {
			hostName = "localhost:8880";
		}
		// hostName = "usalive-ecc.usatech.com";*/
		VDI_SOAP_URL = "http" + (secure ? "s" : "") + "://" + hostName + "/soap/vdi";
	}
	
	// java -cp bin usalive.test.VdiSoapTest
	public static void main(String[] args) throws Exception {
		processMain(args, VDI_SOAP_URL, VdiSoapTest.class.getName());
	}
	
	public static String getUsage(String url, String className) {
		String separator = System.getProperty("path.separator");
		String commandPrefix = new StringBuilder("java -cp ").append("bin").append(separator).append(LIB_DIRECTORY).append("/*")
				.append(" ").append(className).toString();
		
		return new StringBuilder()
			.append("\nPlease provide arguments to use one of the functions below. You may need to change the classpath for your environment.\n\n")
			.append("GetDex <url>\n")
			.append("\nExamples:\n\n")
			.append(commandPrefix).append(" GetDex ").append(url).append("\n")		
			.append("UploadDex <url>\n")
			.append("\nExamples:\n\n")
			.append(commandPrefix).append(" UploadDex ").append(url).append("\n")		
			.toString();
	}
	
	public static void processMain(String[] args, String url, String className) throws Exception {
		if (args.length < 1) {
			System.out.print(getUsage(url, className));
			return;
		}
		String function = args[0];
		if ("GetDex".equals(function)) {
			if (args.length < 9)
				System.out.print(getUsage(url, className));
			else {
				initializeVdi(args[1], USERNAME, PASSWORD);
				getDex();
			}
		} else if("UploadDex".equals(function)) {
			if(args.length < 9)
				System.out.print(getUsage(url, className));
			else {
				initializeVdi(args[1], USERNAME, PASSWORD);
				uploadDex();
			}
		} else
			System.out.print(getUsage(url, className));
	}
	
	protected static void initializeVdi() throws Exception {
		String env = (String) JOptionPane.showInputDialog(null, "Which Environment do you wish to hit?", "Select Environment", JOptionPane.QUESTION_MESSAGE, null, new String[] { "LOCAL", "DEV", "INT", "ECC", "USA" }, "LOCAL");
		String hostName;
		String username;
		String password;
		boolean secure;

		switch(env) {
			case "LOCAL":
				try {
					hostName = InetAddress.getLocalHost().getHostName() + ":8880";
				} catch(UnknownHostException e) {
					hostName = "localhost:8880";
				}
				secure = false;
				username = USERNAME;
				password = PASSWORD;
				break;
			case "DEV":
			case "INT":
			case "ECC":
				hostName = "usalive-" + env.toLowerCase() + ".usatech.com";
				secure = true;
				username = USERNAME;
				password = PASSWORD;
				break;
			case "USA":
				hostName = "usalive.usatech.com";
				secure = true;
				username = System.getProperty("user.name") + "@usatech.com";
				password = UIUtils.promptForPassword("Enter the USALive password for " + username, "USALive Login", null);
				break;
			default:
				throw new Exception("Invalid environment: " + env);
		}
		String url = "http" + (secure ? "s" : "") + "://" + hostName + "/soap/vdi";
		initializeVdi(url, username, password);
	}

	protected static void initializeVdi(String url, String username, String password) throws Exception {
		if (vdi == null)
			vdi = new VdiStub(url);
		HttpTransportProperties.Authenticator basicAuth = new HttpTransportProperties.Authenticator();
		basicAuth.setPreemptiveAuthentication(true); 
		basicAuth.setUsername(username);
		basicAuth.setPassword(password);
		final Options clientOptions = vdi._getServiceClient().getOptions();
		clientOptions.setProperty(HTTPConstants.AUTHENTICATE, basicAuth);
		clientOptions.setTimeOutInMilliSeconds(120 * 1000L);
	}

	protected static void getDex() throws Exception {
		GetDex request = new GetDex();
		request.setApplicationID("USATVDI");
		request.setApplicationVersion("1.0.0");
		//request.setCustomerID("USA Technologies Test Acct #2"); //dev04
		//request.setCustomerID("Boom Vending Inc."); //dev02
		ArrayOfString deviceList = new ArrayOfString();
		//deviceList.setString(new String[] {"EE100000001", "EE100000045"}); //dev04
		//deviceList.setString(new String[] {"VJ000000014", "G5063235"}); //dev02
		// deviceList.setString(new String[] {"EE100017247", "EE100028965"}); //ecc
		deviceList.setString(new String[] { "VJ100022519, VJ100022518" }); // usa
		request.setDeviceList(deviceList);
		/*ArrayOfString outletList = new ArrayOfString();
		outletList.setString(new String[] {"123456789", "Outlet 1234"});
		request.setOutletList(outletList);*/
		Calendar onOrAfter = Calendar.getInstance();
		//onOrAfter.add(Calendar.DAY_OF_MONTH, -365 * 5);
		onOrAfter.add(Calendar.DAY_OF_MONTH, -5);
		request.setOnOrAfter(onOrAfter);
		Calendar onOrBefore = Calendar.getInstance();
		request.setOnOrBefore(onOrBefore);
		request.setProviderID("USAT");
		request.setReturnSet("LAST");
		request.setTransactionID(String.valueOf(System.currentTimeMillis()));
		request.setVDIXMLVersion("1.0");
		GetDexResponse response = vdi.getDex(request);
		VDIReturnType vdiReturn = response.getVDIReturn();
		VDITransactionType vdiTran = response.getVDITransaction();
		System.out.println("GetDex VDIReturn: " + xstream.toXML(vdiReturn));
		System.out.println("GetDex VDITransaction: " + xstream.toXML(vdiTran));
		if (vdiTran.getDEXList() != null)
			System.out.println("GetDex RecordsCount: " + vdiTran.getDEXList().getRecordsCount());
	}
	
	protected static void uploadDex() throws Exception {
		long time = System.currentTimeMillis();
		UploadDex request = new UploadDex();
		request.setApplicationId("USATVDI");
		request.setApplicationVersion("1.0.0");
		request.setCustomerId("ABC"); // dev04
		// request.setCustomerId("Boom Vending Inc."); //dev02
		request.setDexEncoding(1);
		request.setProviderId("USAT");
		request.setTransactionId(String.valueOf(time));
		request.setVdiXmlVersion("1.0");

		VDIDexUploadInput input = new VDIDexUploadInput();
		input.setApplicationId(request.getApplicationId());
		input.setApplicationVersion(request.getApplicationVersion());
		input.setCustomerId(request.getCustomerId());
		input.setDeviceSerialCd("K3BK000001");
		input.setDexDate(new Date(time - 65 * 60 * 1000L));
		input.setDexReason(DEXReason.FILL.getVdiDexReason());
		input.setTimeZone(TimeZone.getTimeZone("US/Central"));
		input.setTransactionId(request.getTransactionId());
		input.setDexCompressionParam(request.getDexCompressionParam());
		input.setDexCompressionType(request.getDexCompressionType());
		input.setDexEncoding(request.getDexEncoding());
		input.setProviderId(request.getProviderId());
		input.setXmlVersion(request.getVdiXmlVersion());
		
		StringBuilder sb = new StringBuilder();
		sb.append("DXS*0000000000*VA*V1/1*1\r\nST*001*0001\r\nID1*0**9985\u0000\u0001\u0002***\r\nID4*2*1\r\nVA1*4752880*52934*4752880*52934*>*<*&*'*\"\r\nVA2*0*0*0*0\r\nCA1*000802120275*VN4510 MDB  *313**0\r\nBA1*002080104256*VN2500/AE24 *3460**0\r\nCA2*0*0*0*0\r\nCA3*5269670*1069195*511875*36886*5269670*1069195*511875*36886\r\nCA4*531120*12535*531120*12535\r\nCA5*0\r\nCA6*0\r\nDA1*0*0*0**0\r\nDA2*5165*38*5165*38*0\r\nTA2*0*0*0*0\r\nLS*0001\r\nPA1*1*135*\r\nPA2*15986*1603250*15986*1603250\r\nPA5*120830*1905*2856\r\nPA1*2*135*\r\nPA2*9338*937375*9338*937375\r\nPA5*120801*2126*1767\r\nPA1*3*125*\r\nPA2*3138*316510*3138*316510\r\nPA5*120727*1639*458\r\nPA1*4*135*\r\nPA2*2367*228755*2367*228755\r\nPA5*120727*1715*570\r\nPA1*5*135*\r\nPA2*3138*315480*3138*315480\r\nPA5*120727*1802*1302\r\nPA1*6*125*\r\nPA2*1193*148975*1193*148975\r\nPA5*120727*1802*590\r\nPA1*7*150*\r\nPA2*6530*410625*6530*410625\r\nPA5*120727*1804*815\r\nPA1*8*150*\r\nPA2*8567*545625*8567*545625\r\nPA5*120727*1804*1285\r\nPA1*9*150*\r\nPA2*2677*246285*2677*246285\r\nPA5*120801*0501*2470\r\nLE*0001\r\nEA2*DO*1935\r\nEA2*CR**0\r\nEA7*32*32\r\nMA5*SWITCH*UNLOCK*1,4,6**10\r\nMA5*SEL1*1*15481*16300\r\nMA5*SEL2*2*8837*9408\r\nMA5*SEL3*3*2561*2660\r\nMA5*SEL4*4*2045*2188\r\nMA5*SEL5*5*2830*3266\r\nMA5*SEL6*6*1107*1121\r\nMA5*SEL7*7*6079*6261\r\nMA5*SEL8*8*7962*8279\r\nMA5*SEL9*9*2602*3537\r\nMA5*ERROR*CJ9*");
		sb.append(time);
		sb.append("*\r\nMA5*ERROR*CJ1*\r\nMA5*ERROR*DS*");
		sb.append(time);
		sb.append("*\r\nMA5*ERROR*DS*\r\nMA5*ERROR*DS*\r\nMA5*ERROR*DS*\r\nMA5*ERROR*DS*\r\nMA5*ERROR*TS*\r\nMA5*ERROR*TS*\r\nMA5*ERROR*TS*\r\nMA5*ERROR*TS*\r\nMA5*ERROR*TS*\r\nMA5*ERROR*BOPN*\r\nMA5*ERROR*BOPN*\r\nMA5*ERROR*BOPN*\r\nMA5*ERROR*BOPN*\r\nMA5*ERROR*BOPN*\r\nMA5*TUBE1**20*104*52*11\r\n");
		sb.append("EA1*EAP*\r\n");
		sb.append("EA2*ENJ*\r\n");
		sb.append("EA1*EB*\r\n");
		sb.append("EA1*EBE*\r\n");
		sb.append("EA1*EBJ*\r\n");
		sb.append("EA1*EC*\r\n");
		sb.append("G85*093C\r\nSE*77*0001\r\nDXE*1*1");
		CharBuffer cb = new MinimalCDataCharset().decode(ByteBuffer.wrap(sb.toString().getBytes()));
		input.setDexFile(new ByteArrayResource(cb.toString().getBytes(), "DEX_FILE_" + time, "DEX_FILE.log"));
		//input.setDexFile(new FileResource(""));
		
		StringWriter writer = new StringWriter();
		TemplatesLoader.getInstance(null).newTransformer("resource:com/usatech/transport/soap/vdi/vdiDexUpload.xsl", 0L).transform(
    			new SAXSource(new BeanAdapter1(), new BeanInputSource(input, "info")), new StreamResult(writer));
		request.setVdiXml(writer.toString());
		
		UploadDexResponse response = vdi.uploadDex(request);
		System.out.println("UploadDex Result:");
		System.out.println(response.getUploadDexResult());
	}

	@Test
	public void testGetDex() throws Exception {
		initializeVdi();
		getDex();
	}

	@Test
	public void testUploadDex() throws Exception {
		initializeVdi();
		uploadDex();
	}
}
