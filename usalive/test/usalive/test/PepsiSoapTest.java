package usalive.test;

import java.io.InputStream;

import javax.xml.stream.XMLStreamException;

import org.apache.axiom.om.impl.builder.StAXOMBuilder;
import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.neethi.Policy;
import org.apache.neethi.PolicyEngine;
import org.apache.rampart.RampartMessageData;
import org.junit.Test;

import com.usatech.usalive.pepsi.PepsiStub;
import com.usatech.usalive.pepsi.PepsiStub.EquipmentDetails;
import com.usatech.usalive.pepsi.PepsiStub.HardwareActivation;
import com.usatech.usalive.pepsi.PepsiStub.HardwareActivationE;
import com.usatech.usalive.pepsi.PepsiStub.HardwareActivationResponseE;
import com.usatech.usalive.pepsi.PepsiStub.HardwareDeactivation;
import com.usatech.usalive.pepsi.PepsiStub.HardwareDeactivationE;
import com.usatech.usalive.pepsi.PepsiStub.HardwareDeactivationResponseE;
import com.usatech.usalive.pepsi.PepsiStub.HardwareDeployment;
import com.usatech.usalive.pepsi.PepsiStub.HardwareDeploymentE;
import com.usatech.usalive.pepsi.PepsiStub.HardwareDeploymentResponseE;
import com.usatech.usalive.pepsi.PepsiStub.HardwareDetails;
import com.usatech.usalive.pepsi.PepsiStub.HardwareStatus;
import com.usatech.usalive.pepsi.PepsiStub.HardwareStatusE;
import com.usatech.usalive.pepsi.PepsiStub.HardwareStatusResponseE;
import com.usatech.usalive.pepsi.PepsiStub.HardwareUninstall;
import com.usatech.usalive.pepsi.PepsiStub.HardwareUninstallE;
import com.usatech.usalive.pepsi.PepsiStub.HardwareUninstallResponseE;
import com.usatech.usalive.pepsi.PepsiStub.HardwareUpdate;
import com.usatech.usalive.pepsi.PepsiStub.HardwareUpdateE;
import com.usatech.usalive.pepsi.PepsiStub.HardwareUpdateResponseE;
import com.usatech.usalive.pepsi.PepsiStub.Location;

import simple.bean.ReflectionUtils;

public class PepsiSoapTest {
	public static final String PEPSI_SOAP_URL = "http://localhost:8880/soap/pepsi";
	//public static final String PEPSI_SOAP_URL = "https://usalive-dev.usatech.com/soap/pepsi";
	//public static final String PEPSI_SOAP_URL = "https://usalive-int.usatech.com/soap/pepsi";
	public static final String USERNAME = "pepsitest";
	public static final String PASSWORD = "BET+dkCsn2HAkU98+bxx";
	
	protected static PepsiStub service = null;
	
	static {
		System.setProperty("app.servicename", "PrepaidServiceSOAPTest");
		//Uncomment trustStore properties below for Dev that uses a certificate signed by USAT internal Certification Authority
		System.setProperty("javax.net.ssl.trustStore", "../ServerLayers/LayersCommon/conf/net/truststore.ts");
		System.setProperty("javax.net.ssl.trustStorePassword", "usatech");
		System.setProperty("log4j.configuration", "log4j-dev.properties");
		System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog");
		System.setProperty("org.apache.commons.logging.simplelog.showdatetime", "true");
		System.setProperty("org.apache.commons.logging.simplelog.log.httpclient.wire", "debug");
		System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.commons.httpclient", "debug");
	}
	
	protected Policy loadPolicy(String name) throws XMLStreamException {
        ClassLoader loader = PepsiSoapTest.class.getClassLoader();
        InputStream resource = loader.getResourceAsStream(name);
        StAXOMBuilder builder = new StAXOMBuilder(resource);
        return PolicyEngine.getPolicy(builder.getDocumentElement());
    }	
	
	protected void init() throws AxisFault, XMLStreamException {
		if (service == null) {
			HttpConnectionManagerParams connectionManagerParams = new HttpConnectionManagerParams();
			connectionManagerParams.setDefaultMaxConnectionsPerHost(100);
			connectionManagerParams.setMaxTotalConnections(100);
			connectionManagerParams.setConnectionTimeout(3000);
			connectionManagerParams.setSoTimeout(30000);
			connectionManagerParams.setLinger(1000);
			connectionManagerParams.setTcpNoDelay(true);
			connectionManagerParams.setStaleCheckingEnabled(true);
			
			MultiThreadedHttpConnectionManager connectionManager = new MultiThreadedHttpConnectionManager();
			connectionManager.setParams(connectionManagerParams);
			HttpClient httpClient = new HttpClient(connectionManager);

			service = new PepsiStub(PEPSI_SOAP_URL);
			ServiceClient client = service._getServiceClient();
			Options options = client.getOptions(); 
			options.setProperty(HTTPConstants.REUSE_HTTP_CLIENT, Boolean.TRUE);
			options.setProperty(HTTPConstants.CACHED_HTTP_CLIENT, httpClient);
			options.setProperty(RampartMessageData.KEY_RAMPART_POLICY, loadPolicy("pepsi-policy-client.xml"));
			options.setUserName(USERNAME);
			options.setPassword(PASSWORD);
			client.engageModule("rampart");
		}
	}	
	
	protected void cleanup() throws AxisFault {
		if (service != null)
			service._getServiceClient().cleanupTransport();
	}
	
	@Test
	public void testHardwareActivation() throws Exception {
		init();
		HardwareActivation req = new HardwareActivation();
		req.setUsername(USERNAME);
		req.setPassword(PASSWORD);
		req.setBottlerID("Test");
		HardwareDetails hd = new HardwareDetails();
		hd.setComponentManufacturer("Test");
		hd.setComponentModel("Test");
		req.setHardwareDetails(hd);		
		req.setSENComponentID("Test");
		HardwareActivationE reqE = new HardwareActivationE();
		reqE.setHardwareActivation(req);		
		try {
			HardwareActivationResponseE response = service.hardwareActivation(reqE);
			System.out.println("Response: " + ReflectionUtils.toPropertyMap(response.getHardwareActivationResponse().getResult())
				+ ReflectionUtils.toPropertyMap(response.getHardwareActivationResponse()));
		} finally {
			cleanup();
		}
	}	
	
	@Test
	public void testHardwareDeactivation() throws Exception {
		init();
		HardwareDeactivation req = new HardwareDeactivation();
		req.setUsername(USERNAME);
		req.setPassword(PASSWORD);
		req.setBottlerID("Test");		
		req.setSENComponentID("Test");
		HardwareDeactivationE reqE = new HardwareDeactivationE();
		reqE.setHardwareDeactivation(req);		
		try {
			HardwareDeactivationResponseE response = service.hardwareDeactivation(reqE);
			System.out.println("Response: " + ReflectionUtils.toPropertyMap(response.getHardwareDeactivationResponse().getResult())
				+ ReflectionUtils.toPropertyMap(response.getHardwareDeactivationResponse()));
		} finally {
			cleanup();
		}
	}		

	@Test
	public void testHardwareDeployment() throws Exception {
		init();
		HardwareDeployment req = new HardwareDeployment();
		req.setUsername(USERNAME);
		req.setPassword(PASSWORD);
		req.setBottlerID("Test");
		req.setBottlerWarehouseID("Test");
		req.setSENComponentID("Test");
		req.setComponentSerialNumber("Test");
		EquipmentDetails ed = new EquipmentDetails();
		ed.setAssetLocation("Test");
		req.setEquipmentDetails(ed);
		Location location = new Location();
		location.setAddress("Test");
		location.setCity("Test");
		location.setCountry("Test");
		location.setStateorProvince("Test");
		location.setPostalCode("Test");
		location.setCustomerName("Test");
		req.setLocation(location);
		HardwareDeploymentE reqE = new HardwareDeploymentE();
		reqE.setHardwareDeployment(req);		
		try {
			HardwareDeploymentResponseE response = service.hardwareDeployment(reqE);
			System.out.println("Response: " + ReflectionUtils.toPropertyMap(response.getHardwareDeploymentResponse().getResult())
				+ ReflectionUtils.toPropertyMap(response.getHardwareDeploymentResponse()));
		} finally {
			cleanup();
		}
	}
	
	@Test
	public void testHardwareStatus() throws Exception {
		init();
		HardwareStatus req = new HardwareStatus();
		req.setUsername(USERNAME);
		req.setPassword(PASSWORD);
		req.setBottlerID("Test");
		req.setSENComponentID("Test");
		HardwareStatusE reqE = new HardwareStatusE();
		reqE.setHardwareStatus(req);		
		try {
			HardwareStatusResponseE response = service.hardwareStatus(reqE);
			System.out.println("Response: " + ReflectionUtils.toPropertyMap(response.getHardwareStatusResponse()));
		} finally {
			cleanup();
		}
	}	
	
	@Test
	public void testHardwareUninstall() throws Exception {
		init();
		HardwareUninstall req = new HardwareUninstall();
		req.setUsername(USERNAME);
		req.setPassword(PASSWORD);
		req.setBottlerID("Test");
		req.setSENComponentID("Test");
		req.setComponentSerialNumber("Test");
		EquipmentDetails ed = new EquipmentDetails();
		ed.setAssetLocation("Test");
		req.setEquipmentDetails(ed);
		HardwareUninstallE reqE = new HardwareUninstallE();
		reqE.setHardwareUninstall(req);		
		try {
			HardwareUninstallResponseE response = service.hardwareUninstall(reqE);
			System.out.println("Response: " + ReflectionUtils.toPropertyMap(response.getHardwareUninstallResponse().getResult())
				+ ReflectionUtils.toPropertyMap(response.getHardwareUninstallResponse()));
		} finally {
			cleanup();
		}
	}
	
	@Test
	public void testHardwareUpdate() throws Exception {
		init();
		HardwareUpdate req = new HardwareUpdate();
		req.setUsername(USERNAME);
		req.setPassword(PASSWORD);
		req.setBottlerID("Test");
		req.setBottlerWarehouseID("Test");
		req.setSENComponentID("Test");
		EquipmentDetails ed = new EquipmentDetails();
		ed.setAssetLocation("Test");
		req.setEquipmentDetails(ed);
		Location location = new Location();
		location.setAddress("Test");
		location.setCity("Test");
		location.setCountry("Test");
		location.setStateorProvince("Test");
		location.setPostalCode("Test");
		location.setCustomerName("Test");
		req.setLocation(location);
		HardwareUpdateE reqE = new HardwareUpdateE();
		reqE.setHardwareUpdate(req);		
		try {
			HardwareUpdateResponseE response = service.hardwareUpdate(reqE);
			System.out.println("Response: " + ReflectionUtils.toPropertyMap(response.getHardwareUpdateResponse().getResult())
				+ ReflectionUtils.toPropertyMap(response.getHardwareUpdateResponse()));
		} finally {
			cleanup();
		}
	}
}
