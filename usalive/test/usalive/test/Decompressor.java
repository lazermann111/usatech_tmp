/**
 *
 */
package usalive.test;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.util.SortedMap;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

import simple.io.FromHexInputStream;
import simple.io.IOUtils;
import simple.io.ToHexOutputStream;
import simple.results.CachedResultsCreator.EOFMarker;
import simple.text.StringUtils;

import com.usatech.report.ReportRequest;

/**
 * @author Brian S. Krug
 *
 */
public class Decompressor {

	/**
	 * @param args
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public static void main(String[] args) throws Exception {
		// decodeHexString(args);
		// decodeReport(args);
		decodeParams(args);
	}
	protected static void decodeParams(String[] args) throws FileNotFoundException, IOException {
		String file = "C:\\Users\\bkrug\\Documents\\report_request_params_6157.gz";
		SortedMap<String, String> params = ReportRequest.decodeParameters(new FileInputStream(file), true);
		System.out.println(params);
	}
	protected static void decodeReport(String[] args) throws FileNotFoundException, IOException {
		String file = "C:\\Users\\bkrug\\Documents\\report_request_6157.xml.gz";
		InflaterInputStream iis = new InflaterInputStream(new FileInputStream(file));
    	Reader reader = new InputStreamReader(iis, Charset.forName("UTF-8"));
    	IOUtils.copy(reader, new PrintWriter(new FileWriter(file.replaceAll("\\.gz$", ""))));
	}
	protected static void decodeHex(String[] args) throws FileNotFoundException, IOException {
		String file = "C:\\Users\\bkrug\\Documents\\report_data.hex";
		InflaterInputStream iis = new InflaterInputStream(new FromHexInputStream(new FileReader(file)));
    	Reader reader = new InputStreamReader(iis, Charset.forName("UTF-8"));
    	IOUtils.copy(reader, new PrintWriter(new FileWriter(file.replaceAll("\\.hex$", ".html"))));
	}

	protected static void decodeHexString(String[] args) throws FileNotFoundException, IOException {
		String hex = "789ce5c9cb0d02211485e10a6e0f163092cb63607233314120844490015cd8b255c9ac2cc2b3fa4fbe3d3f7dc8377035156ac1fa37b898138df04863662f2457a3b4408928349af5f44668184a2610cf9b29ba4dabebf1b21eeec59304d742245cf8e45fd9318843aac48d604231be69c6a582585b9fe0fa415c2c0897ffdde70b5b07422a";
		InflaterInputStream iis = new InflaterInputStream(new FromHexInputStream(new StringReader(hex)));
		System.out.println("Decoding hex of " + (hex.length() / 2) + " byte(s) into:");
		IOUtils.copy(iis, new ToHexOutputStream(new PrintWriter(System.out)));
		System.out.println();
		System.out.println("-----------------------");
		System.out.println();
		System.out.println("As UTF-8:");
		iis = new InflaterInputStream(new FromHexInputStream(new StringReader(hex)));
		Reader reader = new InputStreamReader(iis, Charset.forName("UTF-8"));
		IOUtils.copy(reader, new PrintWriter(System.out));
		System.out.println();
	}
	
	protected static void decodeCachedResults(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
		String inputFile = "C:\\Users\\bkrug\\Documents\\cached-results-9105753030922967206.dat";
		//String outputFile = inputFile.replaceAll("\\.dat$", ".csv");
		ObjectInputStream cachedStream = new ObjectInputStream(new BufferedInputStream(new InflaterInputStream(new FileInputStream(inputFile), new Inflater(), 512), 1024));
		//FileWriter out = new FileWriter(outputFile);
		PrintWriter pw = new PrintWriter(System.out);
		try {
			int row = 0;
			while(row < 1000) {
				System.out.println("Reading row " + ++row);
				Object o = cachedStream.readUnshared();
		        try {
					Object[] data = (Object[]) o;
		            StringUtils.writeCSVLine(data, pw);
		        } catch(ClassCastException e) {
		            if(o instanceof EOFMarker)
		            	break;
		            else
		                throw e;
		        }
			}
			System.out.println("*** Read " + row + " rows ***");
		} finally {
			pw.flush();
			pw.close();
        }
	}
}
