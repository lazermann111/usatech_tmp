package usalive.test;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import simple.io.Log;
import simple.test.UnitTest;

public class ReportRequestObjTest extends UnitTest {

	@Before
	public void setUp() throws Exception {
		setupLog();
	}
	
	@Test
	public void testHashCodeEqual() throws Exception {
		Map<String, Object> parameters=new HashMap<String, Object>();
		parameters.put("folioId", 244);
		parameters.put("batchId", 10000);
		String baseUrl="http://localhost:8080/CustomReporting/frame.i";
		Locale locale=new Locale("en","","");
		/*ReportRequest obj1=new ReportRequest(ReportRequest.stringfyMap(parameters), baseUrl, locale.toString(), 3, "", 932);
		parameters=new HashMap<String, Object>();
		parameters.put("folioId", 244);
		parameters.put("batchId", 10000);
		ReportRequest obj2=new ReportRequest(ReportRequest.stringfyMap(parameters), baseUrl, locale.toString(), 3, "", 932);	
		Assert.assertTrue(obj1.equals(obj2));
		Assert.assertTrue(obj1.hashCode()==obj2.hashCode());
		log.debug("obj1 hashCode:"+obj1.hashCode());*/
	}
	@Test
	public void testHashCodeNotEqual() throws Exception {
		Map<String, Object> parameters=new HashMap<String, Object>();
		parameters.put("folioId", 244);
		parameters.put("batchId", 10000);
		String baseUrl="http://localhost:8080/CustomReporting/frame.i";
		Locale locale=new Locale("en","","");
		/*ReportRequest obj1=new ReportRequest(ReportRequest.stringfyMap(parameters), baseUrl, locale.toString(), 3, "", 932);
		parameters=new HashMap<String, Object>();
		parameters.put("folioId", 245);
		parameters.put("batchId", 10000);
		ReportRequest obj2=new ReportRequest(ReportRequest.stringfyMap(parameters), baseUrl, locale.toString(), 3, "", 932);	
		Assert.assertTrue(!obj1.equals(obj2));
		log.debug("obj1 hashCode:"+obj1.hashCode()+" obj2 hashCode:"+obj2.hashCode());*/
	}
	
	@After
	public void tearDown() throws Exception {
		Log.finish();
	}
}
