<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fr="http://simple/falcon/report/1.0"
	xmlns:x2="http://simple/translate/2.0"
	exclude-result-prefixes="fr x2">
	<xsl:output method="xml" />
	<xsl:template match="fr:report">
		<xsl:call-template name="report" />
	</xsl:template>

	<xsl:template name="report">
		<report>
			<xsl:for-each select="fr:folio">
				<xsl:choose>
					<xsl:when test="fr:error">
						<x2:translate key="report-error">
							An error occurred
						</x2:translate>
						:
						<xsl:value-of select="fr:error" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="folio-content" />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</report>
	</xsl:template>

	<xsl:template name="folio-content">
		<xsl:call-template name="group-layout" />
	</xsl:template>

	<xsl:template name="group-layout">
		<xsl:choose>
			<xsl:when test="count(fr:group) &gt; 0">
				<xsl:for-each select="fr:group">
					<xsl:call-template name="group0">
					</xsl:call-template>
				</xsl:for-each>
			</xsl:when>
			<xsl:otherwise>No data found.</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="group0">
		<xsl:for-each select="fr:group">
			<xsl:call-template name="group5" />
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="group5">
		<payments
			paymentReferenceNumber="{fr:group-data/fr:text[@fr:id=1]}"
			paymentDate="{fr:group-data/fr:text[@fr:id=2]}"
			bankAccountNumber="{fr:group-data/fr:text[@fr:id=3]}"
			bankRoutingNumber="{fr:group-data/fr:text[@fr:id=4]}"
			region="{fr:group-data/fr:text[@fr:id=5]}">
			<xsl:for-each select="fr:group">
				<xsl:call-template name="group4" />
			</xsl:for-each>
		</payments>
	</xsl:template>

	<xsl:template name="group4">
		<xsl:variable name="grossCreditVar"
			select="fr:group-data/fr:text[@fr:id = 11]" />
		<xsl:variable name="failedCreditVar"
			select="fr:group-data/fr:text[@fr:id = 12]" />
		<xsl:variable name="netCreditVar"
			select="fr:group-data/fr:text[@fr:id = 13]" />
		<xsl:variable name="processFeesVar"
			select="fr:group-data/fr:text[@fr:id = 14]" />
		<xsl:variable name="serviceFeesVar"
			select="fr:group-data/fr:text[@fr:id = 15]" />
		<xsl:variable name="refundAndChargebacksVar"
			select="fr:group-data/fr:text[@fr:id = 16]" />
		<xsl:variable name="adjustmentsVar"
			select="fr:group-data/fr:text[@fr:id = 17]" />
		<xsl:variable name="netAmountVar"
			select="fr:group-data/fr:text[@fr:id = 18]" />
		<xsl:variable name="startDateVar"
			select="fr:group-data/fr:text[@fr:id = 19]" />
		<xsl:variable name="endDateVar"
			select="fr:group-data/fr:text[@fr:id = 20]" />
		<xsl:variable name="scheduleVar"
			select="fr:group-data/fr:text[@fr:id = 21]" />

		<payment reconcileGroup="{fr:group-data/fr:text[@fr:id=6]}"
			location="{fr:group-data/fr:text[@fr:id=7]}"
			device="{fr:group-data/fr:text[@fr:id=8]}"
			assetNumber="{fr:group-data/fr:text[@fr:id=9]}"
			numberOfTransactions="{fr:group-data/fr:text[@fr:id=10]}"
			grossCredit="{$grossCreditVar}" failedCredit="{$failedCreditVar}"
			netCredit="{$netCreditVar}" processFees="{$processFeesVar}"
			serviceFees="{$serviceFeesVar}"
			refundAndChargebacks="{$refundAndChargebacksVar}"
			adjustments="{$adjustmentsVar}" netAmount="{$netAmountVar}"
			startDate="{$startDateVar}" endDate="{$endDateVar}"
			schedule="{$scheduleVar}">
			<xsl:choose>
				<xsl:when
					test="fr:group-data/fr:text[@fr:id=10] &gt; 0">
					<transactions>
						<xsl:for-each select="fr:group">
							<xsl:call-template name="group3" />
						</xsl:for-each>
					</transactions>
				</xsl:when>
				<xsl:otherwise></xsl:otherwise>
			</xsl:choose>
		</payment>
	</xsl:template>

	<xsl:template name="group3">
		<xsl:for-each select="fr:group">
			<xsl:call-template name="group2" />
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="group2">
		<xsl:choose>
			<xsl:when
				test="fr:group-data/fr:text[@fr:id=31]!=5 and string-length(normalize-space(fr:group-data/fr:text[@fr:id=24])) > 0">
				<transaction
					transactionNumber="{fr:group-data/fr:text[@fr:id=24]}"
					transactionDate="{fr:group-data/fr:text[@fr:id=25]}"
					cardType="{fr:group-data/fr:text[@fr:id=26]}"
					amount="{fr:group-data/fr:text[@fr:id=27]}"
					apCode="{fr:group-data/fr:text[@fr:id=28]}"
					details="{fr:group-data/fr:text[@fr:id=29]}"
					batchNumber="{fr:group-data/fr:text[@fr:id=30]}">
				</transaction>
			</xsl:when>
			<xsl:otherwise></xsl:otherwise>
		</xsl:choose>
	</xsl:template>

</xsl:stylesheet>
