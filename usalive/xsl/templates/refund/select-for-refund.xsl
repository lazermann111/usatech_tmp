<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns="http://www.w3.org/1999/xhtml">
	<xsl:import href="select-for-chargeback.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:template name="extra-scripts-2">
		<script type="text/javascript">
			function verifyOneSelected(form, actionType) {
				for(var i = 0; i &lt; form.elements.length; i++) {
					var elem = form.elements[i];
					if(elem.name == "tranIds" &amp;&amp; elem.checked) {
						return true;
					}
				}
				if(actionType=='refund'){
					alert("Please select at least one transaction to issue refunds.");
				}else{
					alert("Please select at least one transaction to show on the receipt.");
				}
				return false;
			}
			function generateReceipt(){
				$('refundForm').action="show_receipt.i";
				$('refundForm').target="_blank";
				$('refundForm').submit();
				$('refundForm').target="";
			}
			function issueRefunds(form){
					form.action="create_refund_set.i";
					form.randomNumber.value=Math.floor(Math.random()*1000000000);
					new Form.Request(form, $("issueRefundSection"), 
					{onSuccess: function(){$("issueRefundSectionToggle").showIssueRefundSection();}, 
					resetForm: false, 
					extraData: {fragment: true}
					}).send();
			}
			
			var isSelectAllSearchResults=true;
			
			function selectAllSearchResults(){
				if(isSelectAllSearchResults){
					$('searchResults').getElements("input[type=checkbox])").each(function(el) {
						el.checked=true;
					});
					$('selectAllSearchResultsButton').value="Deselect All";
					isSelectAllSearchResults=false;
				}else{
					$('searchResults').getElements("input[type=checkbox])").each(function(el) {
						el.checked=false;
					});
					$('selectAllSearchResultsButton').value="Select All";
					isSelectAllSearchResults=true;
				}
			}
			
		</script>
	</xsl:template>

	<xsl:template name="folio-content">
		<form id="refundForm" target="">
			<div style="text-align: center; width: 100%;">
			<input type="button" id="selectAllSearchResultsButton" value="Select All" onclick="selectAllSearchResults();"/>
			<input type="button" value="Generate Receipt" onclick="verifyOneSelected($('refundForm'), 'receipt')&amp;&amp;generateReceipt();"/>
			<input type="button" value="Issue Refunds" onclick="verifyOneSelected($('refundForm'), 'refund' )&amp;&amp;issueRefunds($('refundForm'));"/>
			<input type="hidden" name="randomNumber" value="" />
			</div>
			<div>
			<xsl:call-template name="group-layout" />
			</div>
		</form>
	</xsl:template>
</xsl:stylesheet>