<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils" 
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:b="http://simple/bean/1.0"
	xmlns:r="http://simple/results/1.0" 
	xmlns:p="http://simple/xml/parameters/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect r a p">
	<xsl:import href="create-rf-cb-base.xsl"/>
	<xsl:output method="xhtml"/>
	
	<xsl:template name="subtitle">Issue Chargeback</xsl:template>
	<xsl:template name="type-name">chargeback</xsl:template>
	<xsl:template name="create-form">
		<form id="issueRefund" name="issueRefund" method="post" action="issue_chargeback.i" >
			<input type="hidden" name="unframed" value="{/a:base/a:parameters/p:parameters/p:parameter[@p:name='unframed']/@p:value}" />
			<input type="hidden" name="transactionId" value="{/a:base/a:transactions/r:results/r:row/r:transactionId}" />
			<input type="hidden" name="refundSum" value="{$totalSum}" />
			<input type="hidden" name="previousChargebackReversalTotal" value="{reflect:getProperty($context,'previousChargebackReversalTotal')}" />
			<input type="hidden" name="transactionTotal" value="{substring(/a:base/a:transactions/r:results/r:row/r:totalAmount,2,string-length(/a:base/a:transactions/r:results/r:row/r:totalAmount)-4)}" />
			<div id="refundChargebackMessage" />
			<table>
				<tr>
					<td style="width: 50%; text-align: right;">Chargeback Amount:</td>
					<td>
						<input type="text" name="refundAmount" value="{substring(/a:base/a:transactions/r:results/r:row/r:totalAmount,2,string-length(/a:base/a:transactions/r:results/r:row/r:totalAmount)-4)}">
							<xsl:if test="count(/a:base/b:privs/b:priv[number()=14]) &gt; 0">
								<xsl:attribute name="onblur">checkAmounts('issueRefund');</xsl:attribute>
							</xsl:if>
						</input>
					</td>
				</tr>
				<tr>
					<td style="width: 50%; text-align: right;">Chargeback Fee:</td>
					<td>
						<input type="text" name="feeAmount" value="0">
							<xsl:if test="count(/a:base/b:privs/b:priv[number()=14]) &gt; 0">
								<xsl:attribute name="onblur">checkAmounts('issueRefund');</xsl:attribute>
							</xsl:if>
						</input>
					</td>
				</tr>
				<tr>
					<td style="width: 50%; text-align: right;">Comment:</td>
					<td><input type="text" name="refundComment" value="" /></td>
				</tr>
			<xsl:if test="count(/a:base/b:privs/b:priv[number()=15]) &gt; 0">
				<tr>
					<td style="width: 50%; text-align: right;">
						<span id="managerText" style="color: gray;">Manager Override:</span>
					</td>
					<td>
						<input type="checkbox" name="managerOverride" disabled="disabled" value="true" />
					</td>
				</tr>
			</xsl:if>
				<tr>
					<td style="width: 50%; text-align: right;">
						<input type="button" id="submitButton" value="Issue Chargeback" onclick="submitIssueChargeback($('issueRefund'))"/>
					</td>
					<td>
						<input type="button" onclick="doSearch($('searchForm'))" value="Cancel" />
					</td>
					<xsl:if test="reflect:getProperty($context,'previousChargebackCount') &gt; 0">
					<td>
						<input type="button" id="submitReversalButton" value="Issue Chargeback Reversal" onclick="submitIssueChargebackReversal($('issueRefund'))"/>
					</td>
					</xsl:if>
					
				</tr>
			</table>
		</form>
	</xsl:template>
</xsl:stylesheet>