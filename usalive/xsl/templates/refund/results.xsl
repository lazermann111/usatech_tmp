<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect">
	<xsl:import href="../general/app-base.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:param name="context" />

	<xsl:template name="subtitle">Select Transaction</xsl:template>
	<xsl:template name="contents">
		<xsl:param name="returnValue" select="reflect:getProperty($context, 'returnValue')" />
		<xsl:choose>
			<xsl:when test="number($returnValue) = 0">
				<div class='status-info-success'>Successfully created <xsl:value-of select="reflect:getProperty($context,'actionType')" />.</div>
			</xsl:when>
			<xsl:when test="number($returnValue) = 1">
				<div class='status-info-success'>Successfully created manager override <xsl:value-of select="reflect:getProperty($context,'actionType')" />.</div>
			</xsl:when>
			<xsl:when test="number($returnValue) = -1">
				<div class='status-info-failure'>Failed to create <xsl:value-of select="reflect:getProperty($context,'actionType')" />.</div>
			</xsl:when>
			<xsl:when test="number($returnValue) = -2">
				<div class='status-info-failure'>Failed to create manager override <xsl:value-of select="reflect:getProperty($context,'actionType')" />.</div>
			</xsl:when>
			<xsl:when test="number($returnValue) = -3">
                <div class='status-info-failure'>Could not create <xsl:value-of select="reflect:getProperty($context,'actionType')" /> because <xsl:value-of select="reflect:getProperty($context,'errorMessage')" />.</div>
            </xsl:when>
            <xsl:otherwise>
				<div class='status-info-failure'>Function returned with code <xsl:value-of select="$returnValue" /></div>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>