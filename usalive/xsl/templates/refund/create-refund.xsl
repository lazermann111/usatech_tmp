<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils" 
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:b="http://simple/bean/1.0"
	xmlns:r="http://simple/results/1.0" 
	xmlns:p="http://simple/xml/parameters/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect r a p">
	<xsl:import href="create-rf-cb-base.xsl"/>
	<xsl:output method="xhtml"/>
	
	<xsl:template name="subtitle">Issue Refund</xsl:template>
	<xsl:template name="type-name">refund</xsl:template>
	<xsl:template name="create-form">
		<form id="issueRefund" name="issueRefund" method="post" action="issue_refund.i">
			<input type="hidden" name="unframed" value="{/a:base/a:parameters/p:parameters/p:parameter[@p:name='unframed']/@p:value}" />
			<input type="hidden" name="transactionId" value="{/a:base/a:transactions/r:results/r:row/r:transactionId}" />
			<input type="hidden" name="refundSum" value="{$totalSum}" />
			<input type="hidden" name="transactionTotal" value="{substring(/a:base/a:transactions/r:results/r:row/r:totalAmount,2,string-length(/a:base/a:transactions/r:results/r:row/r:totalAmount)-4)}" />
			<div id="refundChargebackMessage" />
			<table>
				<tr>
					<td style="width: 25%; text-align: right;">Refund Amount:</td>
					<td>
						<input type="text" name="refundAmount" maxlength="9" size="9" value="{substring(/a:base/a:transactions/r:results/r:row/r:totalAmount,2,string-length(/a:base/a:transactions/r:results/r:row/r:totalAmount)-4)}">
							<xsl:if test="count(/a:base/b:privs/b:priv[number()=14]) &gt; 0">
								<xsl:attribute name="onblur">checkAmounts('issueRefund');</xsl:attribute>
							</xsl:if>
						</input>
					</td>
				</tr>
				<tr>
					<td style="text-align: right;">Refund Reason:</td>
					<td>
						<select name="refundReason">
							<xsl:for-each select="/a:base/a:reasons/r:results/r:row">
								<xsl:sort select="r:reasonText" />
								<option>
									<xsl:attribute name="value">
										<xsl:value-of select="r:reasonId" />
									</xsl:attribute>
									<xsl:value-of select="r:reasonText" />
								</option>
							</xsl:for-each>
						</select>
					</td>
				</tr>
				<tr>
					<td style="text-align: right;">Comment:</td>
					<td>
						<input type="text" name="refundComment" value="" size="50"/>
					</td>
				</tr>
			<xsl:if test="count(/a:base/b:privs/b:priv[number()=14]) &gt; 0">
				<tr>
					<td style="text-align: right;">
						<span id="managerText" style="color: gray;">Manager Override:</span>
					</td>
					<td>
						<input type="checkbox" name="managerOverride" disabled="disabled" value="true" />
					</td>
				</tr>
			</xsl:if>
				<tr>
					<td style="text-align: right;">
						<input type="button" id="submitButton" value="Issue Refund" onclick="submitIssueRefund($('issueRefund'))"/>
					</td>
					<td>
						<input type="button" onclick="doSearch($('searchForm'))" value="Cancel" />
					</td>
				</tr>
			</table>
		</form>
	</xsl:template>
</xsl:stylesheet>