<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:b="http://simple/bean/1.0"
	xmlns:r="http://simple/results/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect r a b">
	<xsl:import href="../general/app-base.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:variable name="totalSum">
		<xsl:call-template name="sumAmounts">
			<xsl:with-param name="nodes" select="/a:base/a:tranRefunds/r:results/r:row/r:totalAmount"/>
			<xsl:with-param name="sum" select="0"/>
		</xsl:call-template>
	</xsl:variable>
	
	<xsl:variable name="totalRefundSum">
		<xsl:call-template name="sumAmounts">
			<xsl:with-param name="nodes" select="/a:base/a:transactions/r:results/r:row/r:totalAmount"/>
			<xsl:with-param name="sum" select="0"/>
		</xsl:call-template>
	</xsl:variable>

	<xsl:template name="extra-scripts">
		<script type="text/javascript">
			<xsl:attribute name="src"><xsl:call-template name="uri"><xsl:with-param name="uri">scripts/refund-search.js</xsl:with-param></xsl:call-template></xsl:attribute>
		</script>
	</xsl:template>

	<xsl:template name="contents">
		<xsl:attribute name="onload">checkAmounts('issueRefundSet');</xsl:attribute>
		<xsl:choose>
			<xsl:when test="count(/a:base/a:transactions/r:results/r:row) &gt; 0">
				<table class="results">
						<thead>
							<tr class="headerRow">
								<th>Transaction Id</th>
								<th>Device</th>
								<th>Location</th>
								<th>Amount</th>
								<th>Transaction Date</th>
								<th>Approval Code</th>
								<th>Card Number</th>
							</tr>
						</thead>
						<tbody>
							<xsl:for-each select="/a:base/a:transactions/r:results/r:row">
								<tr>
									<xsl:attribute name="class">
										<xsl:choose>
											<xsl:when test="(position() mod 2) = 0">evenRow</xsl:when>
											<xsl:otherwise>oddRow</xsl:otherwise>
										</xsl:choose>
									</xsl:attribute>
									<td><xsl:value-of select="r:transactionId" /></td>
									<td><xsl:value-of select="r:eportNumber" /></td>
									<td><xsl:value-of select="r:locationName" /></td>
									<td><xsl:value-of select="r:totalAmount" /></td>
									<td><xsl:value-of select="r:transactionDate" /></td>
									<td><xsl:value-of select="r:approvalCode" /></td>
									<td><xsl:value-of select="r:creditCardNumber" /></td>
								</tr>
							</xsl:for-each>
							<tr class="totalRow">
								<td colspan="7">
									<xsl:text>Total: </xsl:text>
									<xsl:value-of select="substring(/a:base/a:refunds/r:results/r:row/r:totalAmount[1],1,1)" />
									<xsl:value-of select="format-number($totalRefundSum,'###,###,##0.00')" />
									<xsl:value-of select="substring(/a:base/a:refunds/r:results/r:row/r:totalAmount[1],string-length(/a:base/a:refunds/r:results/r:row/r:totalAmount[1])-3)" />
								</td>
							</tr>
						</tbody>
					</table>
				<!--<div class="spacer"><xsl:text> </xsl:text></div>
				<div style="text-align: right">
					<a><xsl:attribute name="href"><xsl:call-template name="type-name"/>_history.i?cardNumber=<xsl:value-of select="/a:base/a:transactions/r:results/r:row/r:creditCardNumber"/></xsl:attribute>Previous Refunds/Chargebacks On This Card</a>
				</div>-->
				<xsl:call-template name="create-form"/>
                <div class="spacer"><xsl:text> </xsl:text></div>
				<xsl:if test="count(/a:base/a:tranRefunds/r:results/r:row) &gt; 0">
					<hr/>
					<table class="results">
						<caption>Previous Refunds/Chargebacks Against This Transaction</caption>
						<thead>
							<tr class="headerRow">
								<th>Transaction Id</th>
								<th>Amount</th>
								<th>Refund Date</th>
								<th>Issuer</th>
								<th>Status</th>
								<th>Settle Date</th>
								<th>Reason</th>
							</tr>
						</thead>
						<tbody>
							<xsl:for-each select="/a:base/a:tranRefunds/r:results/r:row">
								<tr>
									<xsl:attribute name="class">
										<xsl:choose>
											<xsl:when test="(position() mod 2) = 0">evenRow</xsl:when>
											<xsl:otherwise>oddRow</xsl:otherwise>
										</xsl:choose>
									</xsl:attribute>
									<td><xsl:value-of select="r:transactionId" /></td>
									<td><xsl:value-of select="r:totalAmount" /></td>
									<td><xsl:value-of select="r:transactionDate" /></td>
									<td><xsl:value-of select="r:issuer" /></td>
									<td><xsl:value-of select="r:transactionStatus" /></td>
									<td><xsl:value-of select="r:settledDate" /></td>
									<td><xsl:value-of select="r:refundReason" /></td>
								</tr>
								<xsl:if test="string-length(r:refundComment) > 0">
									<tr>
										<xsl:attribute name="class">
											<xsl:choose>
												<xsl:when test="(position() mod 2) = 0">evenRow</xsl:when>
												<xsl:otherwise>oddRow</xsl:otherwise>
											</xsl:choose>
										</xsl:attribute>
										<td colspan="7" class="comment">
											<xsl:text>Comment: </xsl:text>
											<xsl:value-of select="r:refundComment" />
										</td>
									</tr>
								</xsl:if>
							</xsl:for-each>
							<tr class="totalRow">
								<td colspan="7">
									<xsl:text>Total: </xsl:text>
									<xsl:value-of select="substring(/a:base/a:refunds/r:results/r:row/r:totalAmount[1],1,1)" />
									<xsl:value-of select="format-number($totalSum,'###,###,##0.00')" />
									<xsl:value-of select="substring(/a:base/a:refunds/r:results/r:row/r:totalAmount[1],string-length(/a:base/a:refunds/r:results/r:row/r:totalAmount[1])-3)" />
								</td>
							</tr>
						</tbody>
					</table>
					<div class="spacer"><xsl:text> </xsl:text></div>
				</xsl:if>
				<xsl:if test="count(/a:base/a:cardRefunds/r:results/r:row) &gt; 0">
					<hr />
					<table class="results">
						<caption>
							<xsl:text>Other Refunds Against the Same Card</xsl:text>
						</caption>
						<thead>
							<tr class="headerRow">
								<th>Transaction Id</th>
								<th>Amount</th>
								<th>Refund Date</th>
								<th>Issuer</th>
								<th>Status</th>
								<th>Settle Date</th>
								<th>Reason</th>
							</tr>
						</thead>
						<tbody>
							<xsl:for-each select="/a:base/a:cardRefunds/r:results/r:row">
								<tr>
									<xsl:attribute name="class">
										<xsl:choose>
											<xsl:when test="(position() mod 2) = 0">evenRow</xsl:when>
											<xsl:otherwise>oddRow</xsl:otherwise>
										</xsl:choose>
									</xsl:attribute>
									<td><xsl:value-of select="r:transactionId" /></td>
									<td><xsl:value-of select="r:totalAmount" /></td>
									<td><xsl:value-of select="r:transactionDate" /></td>
									<td><xsl:value-of select="r:issuer" /></td>
									<td><xsl:value-of select="r:transactionStatus" /></td>
									<td><xsl:value-of select="r:settledDate" /></td>
									<td><xsl:value-of select="r:refundReason" /></td>
								</tr>
								<xsl:if test="string-length(r:refundComment) > 0">
									<tr>
										<xsl:attribute name="class">
											<xsl:choose>
												<xsl:when test="(position() mod 2) = 0">evenRow</xsl:when>
												<xsl:otherwise>oddRow</xsl:otherwise>
											</xsl:choose>
										</xsl:attribute>
										<td colspan="7" class="comment">
											<xsl:text>Comment: </xsl:text>
											<xsl:value-of select="r:refundComment" />
										</td>
									</tr>
								</xsl:if>
							</xsl:for-each>
						</tbody>
					</table>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<div>The search specified did not match any transactions.</div>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="sumAmounts">
		<xsl:param name="nodes"/>
		<xsl:param name="sum"/>
		<xsl:choose>
			<xsl:when test="$nodes">
				<xsl:call-template name="sumAmounts">
					<xsl:with-param name="nodes" select="$nodes[position() &gt; 1]" />
					<xsl:with-param name="sum" select="$sum + number(substring($nodes[1],2,string-length($nodes[1])-4))"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$sum"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>