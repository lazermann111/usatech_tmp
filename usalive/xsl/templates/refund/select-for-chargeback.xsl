<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:fr="http://simple/falcon/report/1.0"
	xmlns:x2="http://simple/translate/2.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="fr x2 reflect">
	<xsl:import href="../../simple/falcon/templates/report/show-report-base.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:param name="context" />
	<xsl:template name="extra-scripts">
		<script type="text/javascript">
			var verifyCardIdDialog = new Dialog.Url({
				url: "verifyCardId.i",
				title: "Verifying Full Card", 
				height: 120, 
				width: 300,
				destroyOnClose: false
			});
			function verifyCardId(cardId) {
				if(cardId == null) {
					alert("This transaction has no card data");
					return;
				}
				var card = prompt("Enter the card number:", "");
				if(card == null)
					return;
				verifyCardIdDialog.show({data: {cardId: cardId, card: card, fragment: true }});
			}
		</script>
		<xsl:call-template name="extra-scripts-2"/>
	</xsl:template>

	<xsl:template name="extra-scripts-2"/>

</xsl:stylesheet>