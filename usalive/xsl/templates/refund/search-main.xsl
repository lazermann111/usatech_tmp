<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:str="http://exslt.org/strings"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:r="http://simple/results/1.0"
	xmlns:x="http://simple/translate/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect r a x str">
	<xsl:import href="../selection/search-base.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:param name="context" />
	
	<xsl:variable name="folioId" select="reflect:getProperty($context, 'folioId')"/>
	<xsl:variable name="frameHeight" select="reflect:getProperty($context, 'frameHeight')"/>
	<xsl:variable name="searchType" select="reflect:getProperty($context, 'searchType')"/>

	<xsl:template name="contents">
		<xsl:choose>
			<xsl:when test="string-length($searchType) &lt; 1">
				<body>This action is missing the required parameter 'searchType'.</body>
			</xsl:when>
			<xsl:when test="string-length($folioId) &lt; 1">
				<body>This action is missing the required parameter 'folioId'.</body>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="render-search-form"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name="render-search-form">
		<div class="toggle-heading">
			<span class="caption"><xsl:value-of select="reflect:getProperty($context,'pageTitle')"/></span>
		</div>
		<div id="searchSection" class="content">
			<form id="searchForm" name="searchForm" onsubmit="doSearch(this);return false;">
			     <xsl:attribute name="method">
                    <xsl:choose>
                        <xsl:when test="str:split(reflect:getProperty($context,'searchFields'), ',')[string(.) = 'card-id']">post</xsl:when>
                        <xsl:otherwise>get</xsl:otherwise>
                    </xsl:choose>
                </xsl:attribute>
				<xsl:attribute name="action">
					<xsl:choose>
						<xsl:when test="reflect:getProperty($context,'actionType')"><xsl:value-of select="reflect:getProperty($context,'actionType')"/></xsl:when>
						<xsl:otherwise>run_custom_folio.i</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
				<input type="hidden" name="folioId" value="{$folioId}" />
				<input type="hidden" name="promptForParams" value="false" />
				<input type="hidden" name="unframed" value="true" />
				<table class="selectBody">
					<tr style="vertical-align: top;">
						<td>
							<xsl:call-template name="date-range">
								<xsl:with-param name="startParamName" select="'params.startDate'" />
								<xsl:with-param name="endParamName" select="'params.endDate'" />
							</xsl:call-template>
							<div class="spacer"><xsl:text> </xsl:text></div>
							<div class="spacer10"></div>
							<div align="center">
								<!--<input type="hidden" name="actionType">
									<xsl:attribute name="value">
										<xsl:value-of select="reflect:getProperty($context,'actionType')" />
									</xsl:attribute>
								</input>-->
								<input type="hidden" name="validSearch" value="true"/>
								<input type="submit"><xsl:attribute name="value"><x:translate>Find Transactions</x:translate></xsl:attribute></input>
							</div>
						</td>
						<td>
							<div class="selectSection" style="text-align: right;">
								<div class="sectionTitle"><x:translate>Search Fields</x:translate></div>
								<div class="alignRight">
								<input type="hidden" name="filter_max_index" value="9"/>
								<table class="sectionRow">
									<tr>
									<xsl:for-each select="str:split(reflect:getProperty($context,'searchFields'), ',')[position() mod 2 = 1]">
										<xsl:variable name="searchField" select="normalize-space(.)"/>
										<xsl:if test="string-length($searchField) > 0">
											<xsl:call-template name="write-search-field">
												<xsl:with-param name="searchField" select="$searchField"/>
											</xsl:call-template>
										</xsl:if>
									</xsl:for-each>
									</tr>
									<tr>
									<xsl:for-each select="str:split(reflect:getProperty($context,'searchFields'), ',')[position() mod 2 = 0]">
										<xsl:variable name="searchField" select="normalize-space(.)"/>
										<xsl:if test="string-length($searchField) > 0">
											<xsl:call-template name="write-search-field">
												<xsl:with-param name="searchField" select="$searchField"/>
											</xsl:call-template>
										</xsl:if>
									</xsl:for-each>
									</tr>
								</table>
								</div>
							</div>
						</td>
					</tr>
				</table>
			</form>
			<hr/>
		</div>
		<div id="waitImageReport" style="display:none"><img alt="Please wait" src="images/pleasewait.gif"/>&#160;Please wait, loading data...</div>
		<h4 class="toggle-section"><div id="searchResultsToggle" class="toggle-div"><span>+</span> Search Results</div></h4>
		<div id="searchResults"/>
		<h4 class="toggle-section"><div id="issueRefundSectionToggle" class="toggle-div"><span>+</span> Issue <xsl:value-of select="reflect:getProperty($context,'pageTitle')"/></div></h4>
		<div id="issueRefundSection"/>
	</xsl:template>
	<xsl:template name="write-search-field">
		<xsl:variable name="isInternal" select="reflect:getProperty($context, 'simple.servlet.ServletUser.internal')"/>
		<xsl:param name="searchField"/>
			<xsl:choose>
				<xsl:when test="$searchField = 'first-two'">					
					<td><span><x:translate>Card First Two To Six</x:translate></span></td>
					<td>
						<input type="text" maxlength="6" id="searchCCFirst" name="params.searchCCFirst" value="{reflect:getProperty($context,'searchCCFirst')}" />
						<input type="hidden" name="filter_field_1" usevalue="787"/>
						<input type="hidden" name="filter_op_1" value="23"/>
						<input type="hidden" name="filter_param_name_1" value="searchCCFirst"/>
					</td>
				</xsl:when>
				<xsl:when test="$searchField = 'last-four'">
					<td><span><x:translate>Card Last Four</x:translate></span></td>
					<td>
						<input type="text" maxlength="4" id="searchCCLast" name="params.searchCCLast" value="{reflect:getProperty($context,'searchCCLast')}" />
						<input type="hidden" name="filter_field_2" usevalue="464"/>
						<input type="hidden" name="filter_op_2" value="1"/>
						<input type="hidden" name="filter_param_name_2" value="searchCCLast"/>
					</td>
				</xsl:when>
				<xsl:when test="$searchField = 'amount'">
					<xsl:choose>
			         <xsl:when test="$isInternal = 'true'" > 
						<td><span><x:translate>Transaction Amount</x:translate></span></td>
						<td>
							<input type="text" id="searchAmt" name="params.searchAmt" value="{reflect:getProperty($context,'searchAmt')}" />
						</td>
					</xsl:when>
			         <xsl:otherwise>
			         </xsl:otherwise>
			       </xsl:choose>
		       </xsl:when>
				<xsl:when test="$searchField = 'device-serial'">
					<td><span><x:translate>Device Serial Number</x:translate></span></td>
					<td>
						<input type="text" id="searchSerial" name="params.searchSerial" value="{reflect:getProperty($context,'searchSerial')}" />
						<input type="hidden" name="filter_field_5" usevalue="60"/>
						<input type="hidden" name="filter_op_5" value="26"/>
						<input type="hidden" name="filter_param_name_5" value="searchSerial"/>
					</td>
				</xsl:when>
				<xsl:when test="$searchField = 'device-tran-id'">
					<td><span><x:translate>Device Tran Id</x:translate></span></td>
					<td>
						<input type="text" id="searchDeviceTranId" name="params.searchDeviceTranId" value="{reflect:getProperty($context,'searchDeviceTranId')}" />
						<input type="hidden" name="filter_field_8" usevalue="9588"/>
						<input type="hidden" name="filter_op_8" value="25"/>
						<input type="hidden" name="filter_param_name_8" value="searchDeviceTranId"/>
					</td>
				</xsl:when>
			</xsl:choose>
	</xsl:template>

	<xsl:template name="extra-styles">
		<script type="text/javascript">
			<xsl:attribute name="src"><xsl:call-template name="uri"><xsl:with-param name="uri">scripts/refund-search.js</xsl:with-param></xsl:call-template></xsl:attribute>
		</script>
	</xsl:template>
	<xsl:template name="extra-scripts">
		<script type="text/javascript">
			<xsl:attribute name="src"><xsl:call-template name="uri"><xsl:with-param name="uri">selection/report-selection-scripts.js</xsl:with-param></xsl:call-template></xsl:attribute>
		</script>
		<script type="text/javascript">
        	window.addEvent('domready', function() {
					var searchResults = new Fx.Slide('searchResults');
					var issueRefundSection = new Fx.Slide('issueRefundSection');
					
					$('searchResultsToggle').addEvent('click', function(event){
					    event.stop();
					    searchResults.toggle();
					});
					
					$('issueRefundSectionToggle').addEvent('click', function(event){
					    event.stop();
					    issueRefundSection.toggle();
					});
					
  					searchResults.addEvent('complete', function() {
						if(searchResults.open){
							$('searchResultsToggle').getFirst().set('text', '-');
							$('issueRefundSectionToggle').getFirst().set('text', '+');
							issueRefundSection.hide();
						}else{
							$('searchResultsToggle').getFirst().set('text', '+');
						}
  					});
  					
  					issueRefundSection.addEvent('complete', function() {
						if(issueRefundSection.open){
							$('searchResultsToggle').getFirst().set('text', '+');
							$('issueRefundSectionToggle').getFirst().set('text', '-');
							searchResults.hide();
						}else{
							$('issueRefundSectionToggle').getFirst().set('text', '+');
						}
  					});
  					
  					searchResultsToggle.showSearchResults=function() {
						searchResults.show();
						issueRefundSection.hide();
						$('searchResultsToggle').getFirst().set('text', '-');
						$('issueRefundSectionToggle').getFirst().set('text', '+');
  					};
  					
  					issueRefundSectionToggle.showIssueRefundSection=function(){
						issueRefundSection.show();
						searchResults.hide();
						$('searchResultsToggle').getFirst().set('text', '+');
						$('issueRefundSectionToggle').getFirst().set('text', '-');
					};
  					
  					searchResults.hide();
  					issueRefundSection.hide();
			});
			
		</script>
	</xsl:template>
</xsl:stylesheet>