<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:b="http://simple/bean/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="b">
	<xsl:import href="../general/app-base.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:param name="context" />
	<xsl:template name="contents">
		<table style="width:100%; height: 100%;"><tr><td style="text-align:center">
		<xsl:variable name="user-card-id" select="number(*/b:user-card-id)"/>
		<xsl:variable name="real-card-id" select="number(*/b:real-card-id)"/>
		<xsl:choose>
			<xsl:when test="$real-card-id = $user-card-id">
				<div class="message-success">The card number matches what you entered.</div>
			</xsl:when>
			<xsl:otherwise>
				<div class="message-error">The card number does NOT match what you entered.</div>
			</xsl:otherwise>
		</xsl:choose>
		</td></tr>
		<tr><td style="text-align:center">
		<form><input type="button" onclick="verifyCardIdDialog.hide();" value="Close"/></form>
		</td></tr></table>
	</xsl:template>
</xsl:stylesheet>