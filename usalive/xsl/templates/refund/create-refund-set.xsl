<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils" 
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:b="http://simple/bean/1.0"
	xmlns:r="http://simple/results/1.0" 
	xmlns:p="http://simple/xml/parameters/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect r a p">
	<xsl:import href="create-rf-set-base.xsl"/>
	<xsl:output method="xhtml"/>
	
	<xsl:template name="subtitle">Issue Refund</xsl:template>
	<xsl:template name="type-name">refund</xsl:template>
	<xsl:template name="create-form">
		<xsl:variable name="transactionIdSet">
			<xsl:for-each select="/a:base/a:transactions/r:results/r:row">
			  <xsl:value-of select="concat(r:transactionId, ',')"/>
			</xsl:for-each>
		</xsl:variable>
		<form id="issueRefundSet" name="issueRefundSet" method="post" action="issue_refund_set.i" onsubmit="return submitRefund('issueRefund','refund');">
			<input type="hidden" name="unframed" value="{/a:base/a:parameters/p:parameters/p:parameter[@p:name='unframed']/@p:value}" />
			<input type="hidden" name="transactionIdSet" value="{$transactionIdSet}" />
			<input type="hidden" name="refundSum" value="{format-number($totalSum,'###,###,##0.00')}" />
			<input type="hidden" name="transactionTotal" value="{format-number($totalRefundSum,'###,###,##0.00')}" />
			<div id="refundChargebackMessage" />
			<table>
				<tr>
					<td style="width: 25%; text-align: right;">Refund Amount:</td>
					<td>
						<xsl:choose>
						  <xsl:when test="($totalRefundSum+$totalSum) &lt;= 0">
						    <input type="text" name="refundAmount" maxlength="9" size="9" value="">
								<xsl:if test="count(/a:base/b:privs/b:priv[number()=14]) &gt; 0">
									<xsl:attribute name="onblur">checkAmounts('issueRefundSet');</xsl:attribute>
								</xsl:if>
							</input>
						  </xsl:when>
						  <xsl:otherwise>
						    <input type="text" name="refundAmount" maxlength="9" size="9" value="{format-number($totalRefundSum+$totalSum,'###,###,##0.00')}">
							<xsl:if test="count(/a:base/b:privs/b:priv[number()=14]) &gt; 0">
								<xsl:attribute name="onblur">checkAmounts('issueRefundSet');</xsl:attribute>
							</xsl:if>
							</input>
						  </xsl:otherwise>
						</xsl:choose> 
						
					</td>
				</tr>
				<tr>
					<td style="text-align: right;">Refund Reason:</td>
					<td>
						<select name="refundReason">
							<xsl:for-each select="/a:base/a:reasons/r:results/r:row">
								<xsl:sort select="r:reasonText" />
								<option>
									<xsl:attribute name="value">
										<xsl:value-of select="r:reasonId" />
									</xsl:attribute>
									<xsl:value-of select="r:reasonText" />
								</option>
							</xsl:for-each>
						</select>
					</td>
				</tr>
				<tr>
					<td style="text-align: right;">Comment:</td>
					<td>
						<input type="text" name="refundComment" value="" size="50"/>
					</td>
				</tr>
			<xsl:if test="count(/a:base/b:privs/b:priv[number()=14]) &gt; 0">
				<tr>
					<td style="text-align: right;">
						<span id="managerText" style="color: gray;">Manager Override:</span>
					</td>
					<td>
						<input type="checkbox" name="managerOverride" disabled="disabled" value="true" />
					</td>
				</tr>
			</xsl:if>
				<tr>
					<td style="text-align: right;">
						<input type="button" id="submitButton" value="Issue Refund" onclick="submitIssueRefundSet($('issueRefundSet'))"/>
					</td>
					<td>
						<input type="button" onclick="doSearch($('searchForm'))" value="Cancel" />
					</td>
				</tr>
			</table>
		</form>
	</xsl:template>
</xsl:stylesheet>