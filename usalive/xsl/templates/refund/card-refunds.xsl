<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:b="http://simple/bean/1.0"
	xmlns:r="http://simple/results/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect r a b">
	<xsl:import href="../general/app-base.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:param name="context" />

	<xsl:template name="subtitle">Issue Refund</xsl:template>

	<xsl:template name="extra-scripts">
		<script type="text/javascript">
			<xsl:attribute name="src"><xsl:call-template name="uri"><xsl:with-param name="uri">scripts/refund-search.js</xsl:with-param></xsl:call-template></xsl:attribute>
		</script>
	</xsl:template>

	<xsl:template name="contents">
		<xsl:choose>
			<xsl:when test="count(/a:base/a:refunds/r:results/r:row) &gt; 0">
				<table class="results">
					<caption>
						<xsl:text>Previous Refunds Against Card #</xsl:text>
						<xsl:value-of select="reflect:getProperty($context, 'cardNumber')" />
					</caption>
					<thead>
						<tr class="headerRow">
							<th>Amount</th>
							<th>Date</th>
							<th>Issuer</th>
							<th>Status</th>
							<th>Settle Date</th>
							<th>Reason</th>
						</tr>
					</thead>
					<tbody>
						<xsl:for-each select="/a:base/a:refunds/r:results/r:row">
							<tr>
								<xsl:attribute name="class">
									<xsl:choose>
										<xsl:when test="(position() mod 2) = 0">evenRow</xsl:when>
										<xsl:otherwise>oddRow</xsl:otherwise>
									</xsl:choose>
								</xsl:attribute>
								<td><xsl:value-of select="r:totalAmount" /></td>
								<td><xsl:value-of select="r:transactionDate" /></td>
								<td><xsl:value-of select="r:issuer" /></td>
								<td><xsl:value-of select="r:transactionStatus" /></td>
								<td><xsl:value-of select="r:settledDate" /></td>
								<td><xsl:value-of select="r:refundReason" /></td>
							</tr>
							<xsl:if test="string-length(r:refundComment) > 0">
								<tr>
									<xsl:attribute name="class">
										<xsl:choose>
											<xsl:when test="(position() mod 2) = 0">evenRow</xsl:when>
											<xsl:otherwise>oddRow</xsl:otherwise>
										</xsl:choose>
									</xsl:attribute>
									<td colspan="6" class="comment">
										<xsl:text>Comment: </xsl:text>
										<xsl:value-of select="r:refundComment" />
									</td>
								</tr>
							</xsl:if>
						</xsl:for-each>
					</tbody>
				</table>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>There are no previous refunds on card #</xsl:text>
				<xsl:value-of select="reflect:getProperty($context,'cardNumber')" />.
			</xsl:otherwise>
		</xsl:choose>
		<input type="button" value="&lt;&lt; Go Back" onclick="window.history.back()" />
	</xsl:template>
</xsl:stylesheet>