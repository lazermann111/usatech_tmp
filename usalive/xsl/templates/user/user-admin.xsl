<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:r="http://simple/results/1.0"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:x2="http://simple/translate/2.0"
	xmlns:b="http://simple/bean/1.0"
	xmlns:p="http://simple/xml/parameters/1.0"
	xmlns:prep="http://simple/xml/extensions/java/simple.text.StringUtils"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="r x2 a p prep b">
	<xsl:import href="../general/app-base.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:param name="context" />

	<xsl:template name="subtitle"><x2:translate key="user-admin-title">User Administration</x2:translate></xsl:template>
	
	<xsl:variable name="isUserAdmin" select="count(/a:base/b:user/b:privileges/b:privilege[number()=1]) &gt; 0"/>

	<xsl:template name="extra-scripts">
		<script type="text/javascript"><![CDATA[
    var changed = false;
    var prevUserId = null;
    function clearUser() {
    	setUser();
    }
    function setUserToIndex(index) {
        var op = index < 0 ? null : document.forms["userForm"].elements["editUserId"].options[index];
        if(op == null)
        	return setUser();
        if(!op.onselect)
            op.onselect =  new Function(op.getAttribute("onselect"));
        else if(typeOf(op.onselect) != "function")
            op.onselect = new Function(op.onselect);
        return op.onselect();
    }
    function blankIfUndef(val) {
    	if(val == undefined || val == null) return "";
    	return val;
    }
    function setUser(userId, customerId, customerName, userName, firstName, lastName, email, telephone, fax, userPrivs, readOnly, userIsAdmin, lockoutUntilTs) {
    	]]>
		<xsl:if test="$isUserAdmin">
		$('messageText').set('html', '');
		</xsl:if><![CDATA[
    	
        var frm = $(document.forms["userForm"]);
        if(changed) {
            if(userId != prevUserId) {
                alert("The changes you made to the current user are not saved. Please save or change the changes before administering a different user.");
                $(frm.elements["editUserId"]).setValue(prevUserId);
            }
            return false;
        }
        frm.requestAction.value = "update";
        disableForm(false);

		]]>
		<xsl:if test="$isUserAdmin">
        var lockedMsg = null;
        if (blankIfUndef(lockoutUntilTs) == '')
            lockedMsg = '';
        else
            lockedMsg = 'User is locked until ' + lockoutUntilTs;
        $("userLockedMsg").set("text",lockedMsg);
        var isLocked = (lockedMsg != '');
		</xsl:if><![CDATA[
		
        frm.elements["editUsername"].value = blankIfUndef(userName);
        frm.elements["editUsername"].readOnly = readOnly;
        frm.elements["editFirstName"].value = blankIfUndef(firstName);
        frm.elements["editFirstName"].readOnly = readOnly;
        frm.elements["editLastName"].value = blankIfUndef(lastName);
        frm.elements["editLastName"].readOnly = readOnly;
        frm.elements["editEmail"].value = blankIfUndef(email);
        frm.elements["editEmail"].readOnly = readOnly;
        frm.elements["editPassword"].value = "*****";
        frm.elements["editPassword"].readOnly = readOnly;
        frm.elements["editConfirm"].value = "*****";
        frm.elements["editConfirm"].readOnly = readOnly;
        frm.elements["editTelephone"].value = blankIfUndef(telephone);
        frm.elements["editTelephone"].readOnly = readOnly;
        frm.elements["editFax"].value = blankIfUndef(fax);
        frm.elements["editFax"].readOnly = readOnly;
        $("customer").set("text",blankIfUndef(customerName));

        var b = userId == null || userId == ]]><xsl:choose>
            <xsl:when test="number(/a:base/b:user/b:masterUser/b:userId)"><xsl:value-of select="number(/a:base/b:user/b:masterUser/b:userId)"/></xsl:when>
            <xsl:when test="number(/a:base/b:user/b:userId)"><xsl:value-of select="number(/a:base/b:user/b:userId)"/></xsl:when>
            <xsl:otherwise>0</xsl:otherwise>
        </xsl:choose><![CDATA[;
        var privs = document.getElementsByName("privs");
        $('allPrivs').checked=false;
        for(var i = 0; i < privs.length; i++) {
            privs[i].checked = false;
            privs[i].readOnly = readOnly;
            if(userPrivs != null)
	            for(var k = 0; k < userPrivs.length; k++) {
	                if(privs[i].value == userPrivs[k]) {
	                    privs[i].checked = true;
	                    break;
	                }
	            }
            var flag = privs[i].get("internalExternalFlag");
            privs[i].disabled = b || ]]><xsl:choose>
                <xsl:when test="/a:base/b:user/b:internal = 'true' or /a:base/b:user/b:masterUser/b:internal = 'true'"><![CDATA[(customerId != 0 && flag == "I")]]></xsl:when>
                <xsl:otherwise><![CDATA[(flag != "B" && flag != "E")]]></xsl:otherwise>
            </xsl:choose>;
        }
		<xsl:choose>
			<xsl:when test="$isUserAdmin">
        frm.deleteButton.disabled = (b || readOnly);
        frm.beButton.disabled = b;
        frm.terminalsButton.disabled = (b || readOnly);
        frm.bankAcctsButton.disabled = (b || readOnly);
        frm.addButton.disabled = false;
        frm.unlockButton.disabled = !isLocked;
        		</xsl:when>
        		<xsl:otherwise>
        $(frm.elements["editUserId"]).setValue(userId);
        		</xsl:otherwise>
        	</xsl:choose><![CDATA[
        if (userIsAdmin) {
            frm.deleteButton.disabled = true;
        }
        frm.submitButton.disabled = true;
        frm.cancelButton.disabled = true;
        changed = false;
        prevUserId = $(frm.elements["editUserId"]).getValue();
        App.updateValidation($("password"));
        if(validator)
        	validator.reset();
        return true;
    }
    
    function addUser() {
        clearUser();
        disableForm(false);
        var frm = document.userForm;
        $(frm.editUserId).setValue("0");
        $(frm.editPassword).setValue("");
        $(frm.editConfirm).setValue("");
        $(frm.requestAction).setValue("insert");
        userChanged();
        App.updateValidation($("password"), "validate-password");
    }

    function deleteUser() {
        if(!confirm("Are you sure you want to delete this user?")) return;
        var frm = $(document.userForm);
        $(frm.requestAction).setValue("delete");
        document.location.href = frm.action + "?" + frm.toQueryString();
    }

    function unlockUser() {
        //if(!confirm("Are you sure you want to unlock this user?")) return;
        var frm = $(document.userForm);
        $(frm.requestAction).setValue("unlock");
        document.location.href = frm.action + "?" + frm.toQueryString();
    }

    function pwdChanged() {
        if($('userId').length==0||$('userId').selectedIndex==-1){
        	return;
        }
        userChanged();
        if(document.userForm.requestAction.value == "update")
            document.userForm.requestAction.value = "password";
        App.updateValidation($("password"), "validate-password");
    }

	function privChanged(el){
		if (!el.checked){
			$('allPrivs').checked = false;
		}
	}

    function userChanged() {
        if($('userId').length==0||$('userId').selectedIndex==-1){
        	return;
        }
        changed = true;
        with(document.userForm) {
            submitButton.disabled = false;
            cancelButton.disabled = false;]]>
            <xsl:if test="$isUserAdmin">addButton.disabled = true;
            terminalsButton.disabled = true;
            bankAcctsButton.disabled = true;
            </xsl:if><![CDATA[
        }
    }
    
	function removeSpace(){
		$('userNameField').value=$('userNameField').value.trim();
		$('userNameField').value=$('userNameField').value.replace(/\s+/g, '');
		$('password').value=$('password').value.trim();
		$('confirmPasswordField').value=$('confirmPasswordField').value.trim();
		$('emailField').value=$('emailField').value.trim();
		if(document.userForm.requestAction.value == "update") {
			document.userForm.editPassword.disabled = true;
			document.userForm.editConfirm.disabled = true;
		}]]>
		<xsl:if test="$isUserAdmin">
		document.userForm.userNameSearch.value=$('userNameSearch').value;
		</xsl:if><![CDATA[
	}

    function disableForm(disable) {
        with(document.userForm) {
            editUsername.disabled = disable;
            editFirstName.disabled = disable;
            editLastName.disabled = disable;
            editEmail.disabled = disable;
            editTelephone.disabled = disable;
            editFax.disabled = disable;
            editPassword.disabled = disable;
            editConfirm.disabled = disable;
            submitButton.disabled = disable;
            cancelButton.disabled = disable;
        }
        var privs = document.getElementsByName("privs");
        for(var i = 0; i < privs.length; i++) {
        	privs[i].disabled = disable;
        }
        
    }

    function cancelChanges() {
        changed = false;]]>
    <xsl:choose>
    	<xsl:when test="$isUserAdmin">setUserToIndex(document.userForm.editUserId.selectedIndex);</xsl:when>
    	<xsl:otherwise>
   	    	<xsl:for-each select="/a:base/a:users/r:results/r:row[r:userId = /a:base/b:user/b:userId]">
    		setUser(<xsl:value-of select="r:userId" />,<!--
			--> <xsl:value-of select="r:customerId" />,<!--
			--> "<xsl:value-of select="prep:prepareScript(string(r:customerName))" />",<!--
			--> "<xsl:value-of select="prep:prepareScript(string(r:userName))" />",<!--
			--> "<xsl:value-of select="prep:prepareScript(string(r:firstName))" />",<!--
			--> "<xsl:value-of select="prep:prepareScript(string(r:lastName))" />",<!--
			--> "<xsl:value-of select="prep:prepareScript(string(r:email))" />",<!--
			--> "<xsl:value-of select="prep:prepareScript(string(r:telephone))" />",<!--
			--> "<xsl:value-of select="prep:prepareScript(string(r:fax))" />",<!--
			--> new Array(<xsl:value-of select="r:userPrivs" />),<!--
            --> <xsl:value-of select="string(r:readOnly) = 'true'"/>,<!--
            --> <xsl:value-of select="string(r:userIsAdmin) = 'Y'"/>,<!--
			--> "<xsl:value-of select="prep:prepareScript(string(r:lockoutUntilTs))" />");
			</xsl:for-each>
    	</xsl:otherwise>
    </xsl:choose><![CDATA[
    }
    function navigateTo(base) {
    	var form = $(document.userForm);
    	var path = base;
    	var first = true;
    	[form.editUserId, form.editUsername, $(form["session-token"])].each(function(el) {
    		if(first) {
    			path = path + "?";
    			first = false;
   			} else
   				path = path + "&";
    		path = path + encodeURIComponent(el.name) + '=' + encodeURIComponent(el.getValue());
    	});
    	document.location.href = path;
    }
    function beUser() {
    	navigateTo("be_user.i");
    }

    function gotoBankAccounts() {
    	navigateTo("user_bank_accts.i");
    }

    function gotoTerminals() {
    	navigateTo("user_terminals.i");
    }
	function createArray() {
		return arguments;
	}
]]>
    var validator;
	window.addEvent('domready', function() {
		validator = new Form.Validator.Inline.Mask(document.userForm);
		<xsl:choose> 	 
			<xsl:when test="$isUserAdmin">
				disableForm(true);
				document.userForm.deleteButton.disabled=true;
				document.userForm.beButton.disabled=true;
				document.userForm.unlockButton.disabled=true;
			</xsl:when> 	 
			<xsl:otherwise>cancelChanges();</xsl:otherwise> 	 
		</xsl:choose> 	 
	});
	function searchUserName(){
    	$('userNameSearch').value=$('userNameSearch').value.trim();
    }  
    
    function onAllPrivs(cb){
    	var privs = document.getElementsByName("privs");
    	if (cb.checked){
    		for(var i = 0; i &lt; privs.length; i++) {
            	if (privs[i].disabled == false){
            		privs[i].checked = true;
            	}
        	}
        }else{
        	for(var i = 0; i &lt; privs.length; i++) {
            	if (privs[i].disabled == false){
            		privs[i].checked = false;
            	}
        	}
        }
    }
		</script>
	</xsl:template>

	<xsl:template name="contents">
		<xsl:call-template name="messages-dialog"/>
		<xsl:if test="$isUserAdmin">
			<xsl:choose>
			 	<xsl:when test="string-length(/a:base/b:messageText) &gt; 0">
					<div id="messageText" class="{/a:base/b:messageClass}"><xsl:value-of select="/a:base/b:messageText" /></div>
				</xsl:when>
				<xsl:otherwise>
					<div id="messageText"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>		
		<span class="caption">Users</span>
		<div>
		<xsl:if test="$isUserAdmin">
		<form name="searchForm" action="user_admin_search.i" method="post" id="searchUserForm" onSubmit="searchUserName();">
			<div class="message-header">User Name: <span title="Search user name that contains: (input nothing for all users)"><input name="userNameSearch" id="userNameSearch" type="text" value="{/a:base/b:userNameSearch}" /></span>
				<input type="submit" value="Search" />
			</div>
			<div id="userLockedMsg" class="userLockedNote"></div>			
		</form>
		</xsl:if>
		<form id="userForm" name="userForm" action="modify_user.i" method="post" onSubmit="removeSpace();">
			<input name="userNameSearch" type="hidden" value="{/a:base/b:userNameSearch}" />
			<input type="hidden" name="requestAction" value="update"/>

		<xsl:choose>
			<xsl:when test="$isUserAdmin"></xsl:when>
			<xsl:otherwise>
				<xsl:choose>
				 	<xsl:when test="string-length(/a:base/b:messageText) &gt; 0">
						<div class="{/a:base/b:messageClass}"><xsl:value-of select="/a:base/b:messageText" /></div>
					</xsl:when>
					<xsl:otherwise>
						<div id="messageText"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
			<table class="selectBody">
				<tr>
					<xsl:if test="$isUserAdmin">
						<td valign="top" width="30%">
						<div class="selectSection">
							<div class="sectionTitle">
								<x2:translate key="user-list-header">Users</x2:translate>
							</div>
							<div class="sectionRow">
								<table>
									<tr>
										<td>
										<select id="userId" style="width: 100%;" size="15" name="editUserId">
											<xsl:for-each select="/a:base/a:users/r:results/r:row">
												<option value="{r:userId}">
													<xsl:attribute name="onselect"><!--
													 -->return setUser(<xsl:value-of select="r:userId" />,<!--
													 --><xsl:value-of select="r:customerId" />,<!--
													 -->"<xsl:value-of select="prep:prepareScript(string(r:customerName))" />",<!--
													 -->"<xsl:value-of select="prep:prepareScript(string(r:userName))" />",<!--
													 -->"<xsl:value-of select="prep:prepareScript(string(r:firstName))" />",<!--
													 -->"<xsl:value-of select="prep:prepareScript(string(r:lastName))" />",<!--
													 -->"<xsl:value-of select="prep:prepareScript(string(r:email))" />",<!--
													 -->"<xsl:value-of select="prep:prepareScript(string(r:telephone))" />",<!--
													 -->"<xsl:value-of select="prep:prepareScript(string(r:fax))" />",<!--
													 -->createArray(<xsl:value-of select="r:userPrivs" />),<!--
                                                     --> <xsl:value-of select="string(r:readOnly) = 'true'"/>,<!--
            										 --> <xsl:value-of select="string(r:userIsAdmin) = 'Y'"/>,<!--
                                                     -->"<xsl:value-of select="prep:prepareScript(string(r:lockoutUntilTs))"/>");<!--
													--></xsl:attribute>
													<xsl:value-of select="r:userName" /> - <xsl:value-of select="r:firstName" />&#160;<xsl:value-of select="r:lastName" />
												</option>
											</xsl:for-each>
										</select>
										</td>
									</tr>
									<tr>
										<td align="center">
											<input name="addButton" type="button" value="Add User" onclick="addUser()" />
											<input name="deleteButton" type="button" value="Delete User" onclick="deleteUser()"/>
											<input name="beButton" type="button" value="Login As" onclick="beUser()"/>
											<input name="unlockButton" type="button" value="Unlock User" onclick="unlockUser()"/>
										</td>
									</tr>
								</table>
							</div>
						</div>
					</td>
					</xsl:if>
					<td valign="top">
	    				<div class="selectSection">
						<div class="sectionTitle">
							<x2:translate key="user-info-header">User Information</x2:translate>
						</div>
						<div class="sectionRow">
							<table class="input-rows">
						        <tr><td>Customer</td><td><span id="customer"></span></td></tr>
						        <tr><td>User Name</td><td><input size="50" maxlength="255" type="text" id="userNameField" name="editUsername" data-validators="required minLength:3" value="" onchange="userChanged()"/></td></tr>
						        <tr><td>First Name</td><td><input size="50" maxlength="50" type="text" name="editFirstName" data-validators="required" value="" onchange="userChanged()"/></td></tr>
						        <tr><td>Last Name</td><td><input size="50" maxlength="50" type="text" name="editLastName" data-validators="required" value="" onchange="userChanged()"/></td></tr>
						        <tr><td colspan="2" class="requirementNote">Password must be at least 8 characters and contain 1 uppercase letter, 1 lowercase letter, and 1 number or punctuation.</td></tr>
						        <tr><td>Password</td><td><input id="password" autocomplete="off" size="50" maxlength="20" type="password" name="editPassword" data-validators="validate-password" value="*****" onchange="pwdChanged()"/></td></tr>						        
						        <tr><td>Confirm Password</td><td><input size="50" maxlength="20" autocomplete="off" type="password" id="confirmPasswordField" name="editConfirm" data-validators="validate-match" data-validator-properties="{{matchInput:'password', matchName:'password'}}" value="*****" onchange="pwdChanged()"/></td></tr>
						        <tr><td>Email</td><td><input size="50" maxlength="255" type="text" id="emailField" name="editEmail" data-validators="required validate-email" value="" onchange="userChanged()"/></td></tr>
						        <tr><td>Telephone</td><td><input size="50" maxlength="20" type="text" name="editTelephone" edit-mask="Fixed.PhoneUs" data-validators="validate-digits minLength:10" value="" onchange="userChanged()"/></td></tr>
						        <tr><td>Fax</td><td><input size="50" maxlength="20" type="text" name="editFax" edit-mask="Fixed.PhoneUs" data-validators="validate-digits minLength:10" value="" onchange="userChanged()"/></td></tr>
								<tr><td colspan="2"><input type="checkbox" name="allPrivs" id="allPrivs" onchange="userChanged(); onAllPrivs(this);" selected="false"/> <b>Select All</b></td></tr>
								<tr>
									<td colspan="2" class="noBorder">
									<table>
									<tr>
										<td class="noBorder vTop">
										<table>
									        <xsl:for-each select="/a:base/a:privileges/r:results/r:row[position() mod 3 = 0]">
									        	<tr><td width="20px"><input type="checkbox" name="privs" value="{r:privId}" onchange="privChanged(this);userChanged()" internalExternalFlag="{r:internalExternalFlag}"/></td><td><xsl:value-of select="r:description"/></td></tr>
									        </xsl:for-each>
									    </table>
							        	</td>
							        	<td class="noBorder vTop">
										<table>								
									        <xsl:for-each select="/a:base/a:privileges/r:results/r:row[position() mod 3 = 1]">
									        	<tr><td width="20px"><input type="checkbox" name="privs" value="{r:privId}" onchange="privChanged(this);userChanged()" internalExternalFlag="{r:internalExternalFlag}"/></td><td><xsl:value-of select="r:description"/></td></tr>
									        </xsl:for-each>
									    </table>
							        	</td>
							        	<td class="noBorder vTop">
										<table>
									        <xsl:for-each select="/a:base/a:privileges/r:results/r:row[position() mod 3 = 2]">
									        	<tr><td width="20px"><input type="checkbox" name="privs" value="{r:privId}" onchange="privChanged(this);userChanged()" internalExternalFlag="{r:internalExternalFlag}"/></td><td><xsl:value-of select="r:description"/></td></tr>
									        </xsl:for-each>
									    </table>
							        	</td>
							        </tr>
							        </table>
							        </td>
						        </tr>
						        <tr><td colspan="2" align="center" valign="bottom">
								<xsl:choose>
									<xsl:when test="$isUserAdmin">
										<input type="button" name="terminalsButton" value="Devices" onclick="gotoTerminals()" disabled="disabled"/>
										<input type="button" name="bankAcctsButton" value="Bank Accounts" onclick="gotoBankAccounts()" disabled="disabled"/>
									</xsl:when>
									<xsl:otherwise>
										<input type="hidden" id="userId" name="editUserId" value="{/a:base/b:user/b:userId}"/>
									</xsl:otherwise>
								</xsl:choose>
						        </td></tr>
						        <tr><td colspan="2" align="center" valign="bottom">
						        	<input type="submit" name="submitButton" value="Save Changes" />
						            <input type="button" name="cancelButton" value="Cancel Changes" onclick="cancelChanges()"/>
					           </td></tr>
						    </table>
					    </div>
					    </div>
					</td>
				</tr>
			</table>
		</form>
		</div>
	</xsl:template>
</xsl:stylesheet>