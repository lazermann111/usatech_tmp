<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:b="http://simple/bean/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect a b">
	<xsl:import href="../general/app-base.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:param name="context" />
	<xsl:template name="extra-scripts">
	<script type="text/javascript">
		var isCancelNeeded=<xsl:value-of select="/a:base/b:isCancelNeeded"/>;
		if(isCancelNeeded &lt; 0) {
			if(updateStatusToNew) updateStatusToNew(<xsl:value-of select="/a:base/b:requestId"/>, <xsl:value-of select="/a:base/b:profileId"/>);
		} else{
			if(updateStatusToCanceled) updateStatusToCanceled(<xsl:value-of select="/a:base/b:requestId"/>, <xsl:value-of select="/a:base/b:profileId"/>);
		}
	</script>
	</xsl:template>
	<xsl:template name="contents">
		<p>
			<xsl:attribute name="class">
				<xsl:choose>
					<xsl:when test="reflect:getProperty($context, 'type') = 'error'">errorText</xsl:when>
					<xsl:otherwise>confirmText</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:value-of select="reflect:getProperty($context, 'message')"/></p>
	</xsl:template>
</xsl:stylesheet>