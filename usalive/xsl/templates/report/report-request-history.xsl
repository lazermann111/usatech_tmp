<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:b="http://simple/bean/1.0"
	xmlns:x2="http://simple/translate/2.0"
	xmlns:r="http://simple/results/1.0"
	xmlns:prep="http://simple/xml/extensions/java/simple.text.StringUtils"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect x2 a b r prep">
	<xsl:import href="../general/app-base.xsl" />
	<xsl:output method="xhtml" />
	<xsl:param name="context" />
	
	<xsl:template name="subtitle">
		Report Request History
	</xsl:template>
	 
	<xsl:template name="extra-scripts">
		<style>
			.leftcolumn {float:left}
			.dropdown {display:block; position:relative; }
			.dropdown dd {border:2px solid #9ac1c9; position:absolute; top:0; overflow-x:hidden; overflow-y:auto; display:none; background:#fff; width:120px; }
		</style>
		<script type="text/javascript">
			<xsl:attribute name="src"><xsl:call-template name="uri"><xsl:with-param name="uri">scripts/dropdown.js</xsl:with-param></xsl:call-template></xsl:attribute>
		</script>
		<script type="text/javascript">
			var builtPageDetails = new Object();
			function setUpMinMax(isNext){
				var frm = document.forms["reportRequestHistoryForm"];
				if(isNext) {
					frm.rowNumMin.value = <xsl:value-of select="number(/a:base/b:rowNumMin) + number(/a:base/b:rowNumCount)"/>;
					frm.rowNumMax.value = <xsl:value-of select="number(/a:base/b:rowNumMin) + 2 * number(/a:base/b:rowNumCount) - 1"/>;
					frm.isFromNext.value = 'true';
				} else{
					frm.rowNumMin.value = <xsl:value-of select="number(/a:base/b:rowNumMin) - number(/a:base/b:rowNumCount)"/>;
					frm.rowNumMax.value = <xsl:value-of select="number(/a:base/b:rowNumMin) - 1"/>;
					frm.isFromNext.value = 'false';
				}	
			}
			function updateStatusToNew(requestId, profileId) {
				var statusTd = $("request_id_status_" + requestId);
				if(statusTd &amp;&amp; statusTd.getProperty("data-sort-value") == "Pending") {
					statusTd.empty();
					statusTd.setProperty("data-sort-value", "New Report");
					statusTd.adopt(new Element("a", {
						title: "New Report",
						href: "retrieve_report_by_user.i?requestId=" + requestId + "&amp;pageId=0&amp;profileId=" + profileId + "&amp;session-token=<xsl:value-of select="prep:prepareURLPart(string(reflect:getProperty($context, 'session-token')))"/>",
						text: "New Report"
					}));
					$("request_id_" + requestId).empty();
				}
			}
			function updateStatusToCanceled(requestId) {
				var statusTd = $("request_id_status_"+requestId.toString());
				if(statusTd &amp;&amp; statusTd.getProperty("data-sort-value") == "Pending") {
					statusTd.empty();
					statusTd.setProperty("data-sort-value", "Cancelled");
					statusTd.set("text","Cancelled");
					$("request_id_" + requestId).empty();
				}
			}
			function openCancel(requestId, profileId){
				var cancelDialog = new Dialog.Url({
					url: "report_request_history_cancel.i", 
					data: { requestId: requestId, profileId: profileId, fragment: true }, 
					title: "Canceling Requested Report", 
					height: 120, 
					width: 300});
				cancelDialog.show();
				var statusTd = $("request_id_status_"+requestId.toString());
				var buttonTd = $("request_id_"+requestId.toString());
				statusTd.empty();
				statusTd.setText("Canceled");
				statusTd.setProperty("data-sort-value", "Canceled");
				buttonTd.empty();
			}	
			function showPageDetails(reportName, profileId, requestId, pageId){
				if(builtPageDetails[requestId] == undefined){
		  			var totalPage=pageId+1;
		  			for (var i = 0; i &lt; totalPage; i++) {
		  				var newDiv=new Element("div", {
						style: "float:left;"
						})
						newDiv.adopt(new Element("a", {
						title: ""+(i+1),
						href: "retrieve_report_page_by_user.i?pollInterval=1000&amp;pollIntervalIndex=1&amp;requestId=" + requestId + "&amp;profileId=" + profileId + "&amp;pageId="+i,
						text: ""+(i+1),
						events: {
							mouseenter: function() { this.style.background='#00FFFF'; },
							mouseleave: function() { this.style.background=''; }
						}
						}));
						
						newDiv.adopt(new Element("label", {
						text: "\u00a0\u00a0"
						}));
						$('pageDetails'+requestId).adopt(newDiv);
					}
					builtPageDetails[requestId]=1;
				}
			}	
			
		</script>
	</xsl:template>
	
	<xsl:template name="contents">
	    <span class="caption">Recent Reports</span>
		<table class="reportTitle">
			<tr>
			<td>
			<div class="message-header">Report Request History for
			<!--
			<xsl:choose>
				<xsl:when test="string(/a:base/b:isByProfile) ='true'">Profile</xsl:when>
				<xsl:otherwise>User</xsl:otherwise>
			</xsl:choose>
			-->
			<xsl:value-of select="/a:base/b:displayName"/> 
		</div>
		<form name="reportRequestHistoryForm" action="report_request_history.i" method="post">
		<input type="hidden" name="isByProfile" value="{/a:base/b:isByProfile}"/>
		<input type="hidden" name="rowNumMin" value="{/a:base/b:rowNumMin}"/>
		<input type="hidden" name="rowNumMax" value="{/a:base/b:rowNumMax}"/>
		<input type="hidden" name="rowNumCount" value="{/a:base/b:rowNumCount}"/>
		<input type="hidden" name="displayName" value="{/a:base/b:displayName}"/>
		<input type="hidden" name="isFromNext"/>
		<div>
			<xsl:attribute name="class">
				<xsl:choose>
					<xsl:when test="reflect:getProperty($context, 'type') = 'error'">errorText</xsl:when>
					<xsl:otherwise>confirmText</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:value-of select="reflect:getProperty($context, 'message')"/>
	    </div>
		<xsl:if test="count(a:base/a:userRequestDetails/r:results/r:row) &gt; 0">
			<input type="submit" value="&lt;&lt;" title="Previous" onclick="setUpMinMax(false);" class="nav-button">
				<xsl:if test="a:base/b:rowNumMin &lt; 2"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
			</input>
			<span class="title3">Record <xsl:value-of select="/a:base/b:rowNumMin"/> to <xsl:value-of select="/a:base/b:rowNumMax"/> (by Requested Time)</span>
			<input type="submit" value="&gt;&gt;" title="Next" onclick="setUpMinMax(true);" class="nav-button">
				<xsl:if test="count(a:base/a:userRequestDetails/r:results/r:row) &lt; /a:base/b:rowNumCount"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
			</input>
		</xsl:if>
		</form>
		<table class="folio">
			<tbody>
			<tr class="headerRow">
			    <th><a data-toggle="sort" data-sort-type="STRING">Report File Name</a></th>
				<th><a data-toggle="sort" data-sort-type="DATE">Requested Time</a></th>
				<th><a data-toggle="sort" data-sort-type="STRING">Request Type</a></th>
				<th><a data-toggle="sort" data-sort-type="STRING">Requested By</a></th>
				<th><a data-toggle="sort" data-sort-type="STRING">Requested For</a></th>
				<th><a data-toggle="sort" data-sort-type="STRING">Status</a></th>
				<th/>
			</tr>
			</tbody>
			<tbody>
			<xsl:choose>
				<xsl:when test="count(a:base/a:userRequestDetails/r:results/r:row) &gt; 0">
					<xsl:for-each select="/a:base/a:userRequestDetails/r:results/r:row">
						<tr>
							<xsl:attribute name="class">
								<xsl:choose>
									<xsl:when test="(position() mod 2) = 0">evenRow</xsl:when>
									<xsl:otherwise>oddRow</xsl:otherwise>
								</xsl:choose>
							</xsl:attribute>
						 	<td data-sort-value="{r:reportName}">
								<xsl:value-of select="r:reportName" />
							</td>
							<td data-sort-value="{r:createdTs}">
								<xsl:value-of select="r:createdTs" />
							</td>
							<td data-sort-value="{r:requestTypeName}">
								<xsl:value-of select="r:requestTypeName" />
							</td>
							<td data-sort-value="{r:userName}">
								<xsl:value-of select="r:userName" />
							</td>
							<td data-sort-value="{r:profileName}">
								<xsl:value-of select="r:profileName" />
							</td>
							<xsl:choose>
							<xsl:when test="r:status &lt; 0">
								<td data-sort-value="Cancelled" style="text-align: center">Canceled</td><td/>
							</xsl:when>
							<xsl:when test="r:status = 3">
								<td data-sort-value="Failed" style="text-align: center">Failed</td>
								<td style="text-align: center">
									<xsl:if test="r:generatorId = 1 or r:generatorId = 2 or r:generatorId = 5 or r:generatorId = 6">
										<form action="refresh_report_data.i">
											<input type="hidden" name="requestId" value="{r:requestId}"/>
											<input type="hidden" name="profileId" value="{r:profileId}"/>
											<input type="hidden" name="refreshFileId" value="{r:fileId}"/>
											<input type="submit" title="Schedule this report to be regenerated" value="Regenerate" onclick="this.enabled=false"/>
										</form>
									</xsl:if>
								</td>
							</xsl:when>
							<xsl:when test="r:fileId &gt; 0">
								<xsl:choose>
								<xsl:when test="r:status = 0 and r:requestTypeId != 2">
                                    <td data-sort-value="New Report" style="text-align: center">
                                        <a title="New Report" href="retrieve_report_by_user.i?profileId={r:profileId}&amp;requestId={r:requestId}&amp;pageId=0">New Report</a>
                                    </td>
                                    <td/>
                                </xsl:when>
                                <xsl:when test="r:status = 2">
                                    <td data-sort-value="View Original" style="text-align: center">
                                        <a title="View Original Report" href="retrieve_report_by_user.i?profileId={r:profileId}&amp;requestId={r:requestId}&amp;pageId=0">View Original</a>
                                    </td>
                                    <td style="text-align: center">Refreshed</td>
                                </xsl:when>
                                <xsl:when test="r:status = 1 or r:requestTypeId = 2">
									<td data-sort-value="View Report" style="text-align: center">
										<xsl:choose>
  											<xsl:when test="r:pageId = 0">
  												<a title="View Report" href="retrieve_report_by_user.i?profileId={r:profileId}&amp;requestId={r:requestId}&amp;pageId=0">View Report </a>
  											</xsl:when>
  											<xsl:otherwise>
  											<div class="leftcolumn">
  												<div class="dropdown">
  												<dt id="{r:requestId}-ddheader" onmouseover="showPageDetails('{r:reportName}',{r:profileId},{r:requestId},{r:pageId});ddMenu({r:requestId},1, 60)" onmouseout="ddMenu({r:requestId},-1)">
												<a href="retrieve_report_by_user.i?profileId={r:profileId}&amp;requestId={r:requestId}&amp;pageId=0" >View Page <img src="images/rightarrow.png" width="10%" height="10%"/></a>
												</dt>
												<dd id="{r:requestId}-ddcontent" onmouseover="cancelHide({r:requestId})" onmouseout="ddMenu({r:requestId},-1);">
													<div id="pageDetails{r:requestId}"></div>
												</dd>
												</div>
											</div>
											</xsl:otherwise> 
										</xsl:choose>
									</td>
									<td style="text-align: center">
										<xsl:if test="r:generatorId = 1 or r:generatorId = 2 or r:generatorId = 5 or r:generatorId = 6">
											<form action="refresh_report_data.i">
												<input type="hidden" name="requestId" value="{r:requestId}"/>
												<input type="hidden" name="profileId" value="{r:profileId}"/>
												<input type="hidden" name="refreshFileId" value="{r:fileId}"/>
												<input type="submit" title="Schedule this report to be regenerated" value="Refresh Data" onclick="this.enabled=false"/>
											</form>
										</xsl:if>
									</td>
								</xsl:when>
								<xsl:otherwise><td/><td/></xsl:otherwise>
								</xsl:choose>
							</xsl:when>
							<xsl:when test="r:status = 1">
								<td data-sort-value="Expired" style="text-align: center">Expired</td>
								<td style="text-align: center">
									<xsl:if test="r:generatorId = 1 or r:generatorId = 2 or r:generatorId = 5 or r:generatorId = 6">
										<form action="refresh_report_data.i">
											<input type="hidden" name="requestId" value="{r:requestId}"/>
											<input type="hidden" name="profileId" value="{r:profileId}"/>
											<input type="hidden" name="refreshFileId" value="{r:fileId}"/>
											<input type="submit" title="Schedule this report to be regenerated" value="Regenerate" onclick="this.enabled=false"/>
										</form>
									</xsl:if>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td id="request_id_status_{r:requestId}" data-sort-value="Pending" style="text-align: center">Pending<img src="images/pending.gif"/></td>
								<td id="request_id_{r:requestId}" style="text-align: center">
									<input type="button" title="Cancel this report" value="Cancel" onclick="openCancel({r:requestId},{r:profileId});"/>
								</td>
							</xsl:otherwise>
							</xsl:choose>
						</tr>
					</xsl:for-each>				
				</xsl:when>
				<xsl:otherwise>
					<tr>
						<td colspan="6"><x2:translate key="report-request-history-no-request">No Reports</x2:translate></td>
					</tr>
				</xsl:otherwise>
			</xsl:choose>
			</tbody>
		</table>
		&#160;
		<div class="copyright"></div>
		</td>
		</tr>
		</table>
	</xsl:template>
	
</xsl:stylesheet>