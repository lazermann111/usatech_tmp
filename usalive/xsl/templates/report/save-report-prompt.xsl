<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:a="http://simple/aggregating/1.0"
    xmlns:b="http://simple/bean/1.0"
    xmlns:p="http://simple/xml/parameters/1.0"
    xmlns="http://www.w3.org/1999/xhtml"
    exclude-result-prefixes="p b a">
	<xsl:import href="../general/app-base.xsl"/>
    
	<xsl:output method="xhtml" />
	<xsl:param name="context" />
	
	<xsl:template name="subtitle">
		Save Report
	</xsl:template>
	
	<xsl:template name="extra-scripts">
	<!--
		<script type="text/javascript">
	window.addEvent("domready", function() {
		new Dialog({content: $("save-report-form"), close: false});
	});
		</script>  -->
	</xsl:template>
	
	<xsl:template name="contents">
	   <form id="save-report-form" action="save_report_params.i">
	       <xsl:call-template name="parameters-form">
	           <xsl:with-param name="overrides">
                    <p:ignore name="reportTitle" />
                </xsl:with-param>
	       </xsl:call-template>
	       <table class="fields">
	           <tr class="tableHeader"><th colspan="2">Save Report Configuration</th></tr>
               <tr class="tableDataShade"><th>Report Title:</th><td><input type="text" name="reportTitle" value=""/></td></tr>
	           <tr><td colspan="2" style="text-align: center"><input type="submit" value="Save"/></td></tr>
	       </table>
	   </form>
	</xsl:template>
</xsl:stylesheet>

