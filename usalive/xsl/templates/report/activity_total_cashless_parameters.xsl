<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:b="http://simple/bean/1.0"
	xmlns:x2="http://simple/translate/2.0"
	xmlns:r="http://simple/results/1.0"
	xmlns:p="http://simple/xml/parameters/1.0"
	xmlns:cmn="http://exslt.org/common"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect x2 a b r p cmn">
	<xsl:import href="../selection/select-terminal-customers.xsl"/>
	<xsl:import href="parameters_base.xsl"/>
	<xsl:import href="../general/tabs.xsl" />
    <xsl:output method="xhtml" />
	<xsl:param name="context" />
	
	<xsl:template name="subtitle">
		Activity Total Annual Cashless Parameters
	</xsl:template>
	
	<xsl:template name="extra-scripts">
		<script type="text/javascript">
	function validateSubmit() {
        setDates();
        return true;
    }

	function setDates() {
           $("beginDate").value =  "January 1, " + $("beginYear").getValue();
           $("endDate").value =  $("endMonth").getValue() + " " + $("endDay").getValue() + ", " + $("endYear").getValue();
           $("rangeType").value = "DAY";

    }
    	</script>
    	<xsl:call-template name="extra-scripts-select-terminal"/>
	</xsl:template>

	<xsl:template name="tab-contents">
	<div class="builderTabs">
	<form id="criteriaForm" name="criteria" method="POST" action="total_annual_cashless_sales.i" onsubmit="return validateSubmit();">
	<input type="hidden" name="params.beginDate" id="beginDate"/>
	<input type="hidden" name="params.endDate" id="endDate"/>
	<input type="hidden" name="rangeType" id="rangeType"/>
	<input type="hidden" name="referrer" value="activity_total_cashless_parameters.i"/>
    <input type="hidden" name="saveAction" value="save_report_params_prompt.i"/>
    <input type="hidden" name="params.tranType" value="15,16,17,18,19,23"/>
    <div class="caption">Total Annual Cashless Sales</div>
	<table class="selectBody">
	<tr><td id="criteriaTd"><div id="beginDR" class="buildCritDiv">Beginning:<span class="required">*</span><br/>
	    <select id="beginYear">
    		<xsl:call-template name="dateNumberOptions">
				<xsl:with-param name="value" select="/a:base/b:calendar/b:minYear" />
				<xsl:with-param name="select-value" select="/a:base/b:calendar/b:beginYear" />
				<xsl:with-param name="max-value" select="/a:base/b:calendar/b:maxYear" />	 
			</xsl:call-template>
        </select>
    </div>
    <div id="endDR" class="buildCritDiv">Ending:<span class="required">*</span><br/>
    	<div style="display:none">
    	<select id="endMonth">
        	<xsl:call-template name="monthOptions">
	    		<xsl:with-param name="select-month" select="/a:base/b:calendar/b:endMonth"/>
	    	</xsl:call-template>
        </select>
		<select id="endDay">
        	<xsl:call-template name="dateNumberOptions">
				<xsl:with-param name="value" select="1" />
				<xsl:with-param name="select-value" select="/a:base/b:calendar/b:endDay" />
				<xsl:with-param name="max-value" select="31" />	 
			</xsl:call-template>
        </select>
        </div>
		<select id="endYear">
    		<xsl:call-template name="dateNumberOptions">
				<xsl:with-param name="value" select="/a:base/b:calendar/b:minYear" />
				<xsl:with-param name="select-value" select="/a:base/b:calendar/b:endYear" />
				<xsl:with-param name="max-value" select="/a:base/b:calendar/b:maxYear" />	 
			</xsl:call-template>
        </select>
    </div>
  	</td>
	</tr>
<tr><td colspan="2"><span class="required">* Required</span></td></tr>
<tr><td colspan="2" align="center"><input type="submit" value="Run Report" id="Submit" /></td></tr></table></form>
	</div>
	</xsl:template>
</xsl:stylesheet>
