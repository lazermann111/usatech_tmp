<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:b="http://simple/bean/1.0"
	xmlns:x2="http://simple/translate/2.0"
	xmlns:prep="http://simple/xml/extensions/java/simple.text.StringUtils"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect a b x2 prep">
	<xsl:output method="xhtml"/>

	<xsl:param name="context" />
	
	<xsl:template match="/">
		<xsl:processing-instruction name="output">
			<xsl:text>omit-dtd="true"</xsl:text>
		</xsl:processing-instruction>
		<xsl:variable name="requests" select="a:base/a:requests/b:requests"/>
		<xsl:variable name="pending-count" select="count($requests/b:request[normalize-space(@b:status) = 'PENDING' or normalize-space(@b:status) = 'PENDING_NOTIFY'])"/>		
		<xsl:variable name="ready-count" select="count($requests/b:request[normalize-space(@b:status) != 'PENDING' and normalize-space(@b:status) != 'PENDING_NOTIFY'])"/>		
		<xsl:variable name="new-count" select="count($requests/b:request[normalize-space(@b:status) = 'NOTIFY'])"/>		
		<script type="text/javascript">
			var rrc=$("reports-ready-count");
			if(rrc){
				rrc.setText("Ready (<xsl:value-of select="$ready-count"/>)");
				rrc.setStyle("font-weight", "<xsl:choose><xsl:when  test="$new-count > 0">bold</xsl:when><xsl:otherwise>normal</xsl:otherwise></xsl:choose>");
			}
			$("reports-pending-count").setText("Pending (<xsl:value-of select="$pending-count"/>)");
			<xsl:for-each select="$requests/b:request[normalize-space(@b:status) = 'NOTIFY']">
			if(window.updateStatusToNew) 
				updateStatusToNew(<xsl:value-of select="@b:requestId"/>, <xsl:value-of select="@b:profileId"/>);
			if(window.handleNewReport)
				handleNewReport("retrieve_report_by_user.i?requestId=<xsl:value-of select="@b:requestId"/>&amp;profileId=<xsl:value-of select="@b:profileId"/>&amp;session-token=<xsl:value-of select="prep:prepareURLPart(string(reflect:getProperty($context, 'session-token')))"/>", "<xsl:value-of select="prep:prepareScript(string(@b:fileName))"/>");
			</xsl:for-each>
			if(window.updateStatusToNew) {
			<xsl:for-each select="$requests/b:request[normalize-space(@b:status) = 'READY']">
			updateStatusToNew(<xsl:value-of select="@b:requestId"/>, <xsl:value-of select="@b:profileId"/>);
			</xsl:for-each>
			}
			<xsl:if test="$pending-count > 0 and $requests/@b:ajaxPollMaxCountReached ='N' ">setTimeout(function() {
				new Request.HTML({ 
			    	url : "report_ajax_poll.i",
					evalScripts: true,
					append: $("banner_request")
				}).send();
			}, <xsl:value-of select="$requests/@b:ajaxPollInterval"/>);</xsl:if>
		</script>
	</xsl:template>
</xsl:stylesheet>