<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:b="http://simple/bean/1.0"
	xmlns:x2="http://simple/translate/2.0"
	xmlns:r="http://simple/results/1.0"
	xmlns:p="http://simple/xml/parameters/1.0"
	xmlns:cmn="http://exslt.org/common"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect x2 a b r p cmn">
	<xsl:import href="../selection/select-terminal-customers.xsl"/>
	<xsl:import href="parameters_base.xsl"/>
	<xsl:import href="../general/tabs.xsl" />
    <xsl:output method="xhtml" />
	<xsl:param name="context" />
	
	<xsl:template name="subtitle">
		Activity Summary Parameters
	</xsl:template>
	
	<xsl:template name="extra-scripts">
		<script type="text/javascript">
	function validateSubmit() {
    	if(!validateTerminals()) return false;
        var tranTypeFields = $("criteriaForm").getElements("input[name='params.tranType']");
	  	var ok = tranTypeFields.getProperty("checked").some(function(item, index) { return item; });
        if(!ok) {
            alert("Please select at least one transaction type.");
            return false;
        }
        var valueFields = $("criteriaForm").getElements("input[name='showValues']");
        ok = valueFields.getProperty("checked").some(function(item, index) { return item; });
        if(!ok) {
            alert("Please select at least one data value.");
            return false;
        }
        var groupByFields = $("criteriaForm").getElements("select[name='sortBy']");
        ok = groupByFields.some(function(item, index) { return item.getValue() != "0"; });
        if(!ok) {
            alert("Please select at least one column.");
            return false;
        }
        setDates();
        var beginDate = new Date($("beginMonth").getValue() + " " + $("beginDay").getValue() + ", " + $("beginYear").getValue());
        var endDate = new Date($("endMonth").getValue() + " " + $("endDay").getValue() + ", " + $("endYear").getValue());
        if (Math.round((endDate.getTime() - beginDate.getTime())/(24*60*60*1000)) &gt; 180) {
        	if (confirm("The Sales Rollup report is recommended for large date ranges. Would you like to run the Sales Rollup report instead?")) {
        		window.location = "/activity_rollup_parameters.i?folioId=969&amp;startParamName=params.beginDate&amp;endParamName=params.endDate&amp;params.beginDate=" + encodeURIComponent(formatDate(beginDate, "MM/dd/yyyy")) + "&amp;params.endDate=" + encodeURIComponent(formatDate(endDate, "MM/dd/yyyy"));
        		return false;
        	}
        }
        return true;
    }

	function setDates() {
		var evt = 0;
		var groupByFields = $("criteriaForm").getElements("select[name='sortBy']");
        groupByFields.forEach(function(item, index) {
        var value = item.getValue();
        	if(value &gt; 2 &amp;&amp; value &lt; 7 &amp;&amp; (evt == "0" || evt &gt; value)) 
        		evt = value;
       	});
        
        if(evt == 4) {
            $("beginDate").value =  $("beginMonth").getValue() + ", " + $("beginYear").getValue();
            $("endDate").value =  $("endMonth").getValue() + ", " + $("endYear").getValue();
            $("rangeType").value = "MONTH";
        } else if(evt == 6) {
            $("beginDate").value =  $("beginMonth").getValue() + " " + $("beginDay").getValue() + ", " + $("beginYear").getValue();
            $("endDate").value =  $("endMonth").getValue() + " " + $("endDay").getValue() + ", " + $("endYear").getValue();
            $("rangeType").value = "DAY";
        } else if(evt == 5) {
            $("beginDate").value =  $("beginMonth").getValue() + " " + $("beginDay").getValue() + ", " + $("beginYear").getValue();
            $("endDate").value =  $("endMonth").getValue() + " " + $("endDay").getValue() + ", " + $("endYear").getValue();
            $("rangeType").value = "WEEK";
        } else if(evt == 3){ //FILL
            $("beginDate").value =  $("beginMonth").getValue() + " " + $("beginDay").getValue() + ", " + $("beginYear").getValue();
            $("endDate").value =  $("endMonth").getValue() + " " + $("endDay").getValue() + ", " + $("endYear").getValue();
            $("rangeType").value = "FILL";
        } else { //Use Close Date
            $("beginDate").value =  $("beginMonth").getValue() + " " + $("beginDay").getValue() + ", " + $("beginYear").getValue();
            $("endDate").value =  $("endMonth").getValue() + " " + $("endDay").getValue() + ", " + $("endYear").getValue();
            $("rangeType").value = "DAY";
        }
    }
    
	function sortByChanged() {
        var groupByFields = $("criteriaForm").getElements("select[name='sortBy']");
        if(groupByFields.getLast().getValue() != "0") {
        	var tab = $("filterTable");
            var row = tab.insertRow(tab.rows.length);
            var cell = $(row.insertCell(row.cells.length));
            cell.set("text","Then By:");
            cell = $(row.insertCell(row.cells.length));
            var gbf = groupByFields.getLast().clone();
            gbf.setValue("0");
            cell.adopt(gbf);
        }
        var hasItems = groupByFields.some(function(item, index) { return item.getValue() == 11; });
        var valueFields = $("criteriaForm").getElements("input[name='showValues']");
        var checkedValues = [];<!-- 
        NOTE: Can't use item.getValue() for these because if they are disabled they return false for getValue() -->
        valueFields.forEach(function(item, index) { 
	        checkedValues[item.value] = item.checked; 
	        item.disabled = (hasItems &amp;&amp; item.value == 1);
	        if(item.disabled){
	        	item.checked=false;
	        }
	        return item.disabled; }
        );
        var percentFields = $("criteriaForm").getElements("input[name='showPercents']");
        percentFields.forEach(function(item, index) { return item.disabled = (hasItems &amp;&amp; item.value == 1) || !checkedValues[item.value]; });
        var averageFields = $("criteriaForm").getElements("input[name='showAverages']");
        averageFields.forEach(function(item, index) { return item.disabled = hasItems; });
	}

	window.addEvent('domready', function() {
		sortByChanged(); 
	});
    	</script>
    	<xsl:call-template name="extra-scripts-select-terminal"/>
	</xsl:template>

	<xsl:template name="tab-contents">
	<div class="caption"><xsl:value-of select="reflect:getProperty($context, 'reportTitle')" /></div>
	<div class="builderTabs">
	<form id="criteriaForm" name="criteria" method="POST" action="activity_summary.i" onsubmit="return validateSubmit();">
	<input type="hidden" name="params.beginDate" id="beginDate"/>
	<input type="hidden" name="params.endDate" id="endDate"/>
	<input type="hidden" name="rangeType" id="rangeType"/>
	<input type="hidden" name="referrer" value="activity_summary_parameters.i"/>
    <input type="hidden" name="saveAction" value="save_report_params_prompt.i"/>
	<table class="selectBody">
	<tr><td><div id="beginDR" class="buildCritDiv">Beginning:<span class="required">*</span><br/>
  		<select id="beginMonth">
	    	<xsl:call-template name="monthOptions">
	    		<xsl:with-param name="select-month" select="/a:base/b:calendar/b:beginMonth"/>
	    	</xsl:call-template>
        </select>
        <select id="beginDay">
        	<xsl:call-template name="dateNumberOptions">
				<xsl:with-param name="value" select="1" />
				<xsl:with-param name="select-value" select="/a:base/b:calendar/b:beginDay" />
				<xsl:with-param name="max-value" select="31" />	 
			</xsl:call-template>
        </select>
	    <select id="beginYear">
    		<xsl:call-template name="dateNumberOptions">
				<xsl:with-param name="value" select="/a:base/b:calendar/b:minYear" />
				<xsl:with-param name="select-value" select="/a:base/b:calendar/b:beginYear" />
				<xsl:with-param name="max-value" select="/a:base/b:calendar/b:maxYear" />	 
			</xsl:call-template>
        </select>
    </div>
    <div id="endDR" class="buildCritDiv">Ending:<span class="required">*</span><br/>
    	<select id="endMonth">
        	<xsl:call-template name="monthOptions">
	    		<xsl:with-param name="select-month" select="/a:base/b:calendar/b:endMonth"/>
	    	</xsl:call-template>
        </select>
		<select id="endDay">
        	<xsl:call-template name="dateNumberOptions">
				<xsl:with-param name="value" select="1" />
				<xsl:with-param name="select-value" select="/a:base/b:calendar/b:endDay" />
				<xsl:with-param name="max-value" select="31" />	 
			</xsl:call-template>
        </select>
		<select id="endYear">
    		<xsl:call-template name="dateNumberOptions">
				<xsl:with-param name="value" select="/a:base/b:calendar/b:minYear" />
				<xsl:with-param name="select-value" select="/a:base/b:calendar/b:endYear" />
				<xsl:with-param name="max-value" select="/a:base/b:calendar/b:maxYear" />	 
			</xsl:call-template>
        </select>
    </div>
	<div id="tType" class="buildCritDiv">
	<div class="closedTreeNode" id="tranCategoryTreeAll">
    <a class="treeImage" onclick="hideTranTypeAll()">&#160;&#160;</a>
    <input id="tranTypeAll" type="checkbox" checked="checked" onclick="checkAllTriggered(this.checked);checkAll(this, 'params.tranType')" title="Toggle Transaction Types" />
    <a class="treeLabel" onclick="hideTranTypeAll()">
    &#160;Transaction Type: <span class="required">*</span>
    </a>
    </div>
    	<xsl:variable name="tran-type" select="concat(',', translate(reflect:getProperty($context, 'params.tranType'), ' ', ''), ',')"/>
    	<fieldset class="no-border-grouper" name="params.tranType" data-validators="validate-reqchk-byname">
    		<xsl:attribute name="data-validator-properties">{label:'transaction type'}</xsl:attribute>  
    		<ul class="customers tree"> 
    		<li class="closedTreeNode" id="tranCategoryTree1">
    		<a class="treeImage" onclick="hideTranType(1)">&#160;&#160;</a>
    		<input checked="checked" id="tranCategoryCheckbox1" type="checkbox" name="tranCategoryCheckbox1" onclick="toggleTranType(1, this.checked)"/>
			<a class="treeLabel" onclick="hideTranType(1)" title="Cashless transactions with electronic funds transfer between USAT and Operator"> Cashless (Payment)</a>
			<div id="tranCategory1" class="childTreeDiv" style="display:none">
			<ul class="customers tree"> 
		   	<xsl:for-each select="a:base/a:transTypes/r:results/r:row[r:tranCategory='1']">
		   	<li class="openTreeNode">
		   		<label  class="checkbox" data-trancategory="{r:tranCategory}">
		   			<input type="checkbox" data-trancategory="{r:tranCategory}" value="{r:transTypeId}" name="params.tranType">
		   			<xsl:if test="$tran-type = ',,' or contains($tran-type, concat(',', r:transTypeId, ','))"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
		   			</input>
		   			<xsl:value-of select="r:transTypeName"/>
		   		</label>
		   	</li>
			</xsl:for-each>
			</ul>
			</div>
			</li>
			</ul>
    	</fieldset>
    	<fieldset class="no-border-grouper" name="params.tranType" data-validators="validate-reqchk-byname">
    		<xsl:attribute name="data-validator-properties">{label:'transaction type'}</xsl:attribute>  
    		<ul class="customers tree"> 
    		<li class="closedTreeNode" id="tranCategoryTree2">
    		<a class="treeImage" onclick="hideTranType(2)">&#160;&#160;</a>
    		<input checked="checked" id="tranCategoryCheckbox2" type="checkbox" name="tranCategoryCheckbox2" onclick="toggleTranType(2, this.checked)"/>
			<a class="treeLabel" onclick="hideTranType(2)" title="Cashless transactions with no electronic funds transfer between USAT and Operator"> Cashless (No Payment)</a>
			<div id="tranCategory2" class="childTreeDiv" style="display:none">
			<ul class="customers tree"> 
		   	<xsl:for-each select="a:base/a:transTypes/r:results/r:row[r:tranCategory='2']">
		   	<li class="openTreeNode">
		   		<label  class="checkbox" data-trancategory="{r:tranCategory}">
		   			<input type="checkbox" data-trancategory="{r:tranCategory}" value="{r:transTypeId}" name="params.tranType">
		   			<xsl:if test="$tran-type = ',,' or contains($tran-type, concat(',', r:transTypeId, ','))"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
		   			</input>
		   			<xsl:value-of select="r:transTypeName"/>
		   		</label>
		   	</li>
			</xsl:for-each>
			</ul>
			</div>
			</li>
			</ul>
    	</fieldset>
    	<fieldset class="no-border-grouper" name="params.tranType" data-validators="validate-reqchk-byname">
    		<xsl:attribute name="data-validator-properties">{label:'transaction type'}</xsl:attribute>  
    		<ul class="customers tree"> 
    		<li class="closedTreeNode" id="tranCategoryTree3">
    		<a class="treeImage" onclick="hideTranType(3)">&#160;&#160;</a>
    		<input checked="checked" id="tranCategoryCheckbox3" type="checkbox" name="tranCategoryCheckbox3" onclick="toggleTranType(3, this.checked)"/>
			<a class="treeLabel" onclick="hideTranType(3)" title="Cash transactions"> Cash</a>
			<div id="tranCategory3" class="childTreeDiv" style="display:none">
			<ul class="customers tree"> 
		   	<xsl:for-each select="a:base/a:transTypes/r:results/r:row[r:tranCategory='3']">
		   	<li class="openTreeNode">
		   		<label  class="checkbox" data-trancategory="{r:tranCategory}">
		   			<input type="checkbox" data-trancategory="{r:tranCategory}" value="{r:transTypeId}" name="params.tranType">
		   			<xsl:if test="$tran-type = ',,' or contains($tran-type, concat(',', r:transTypeId, ','))"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
		   			</input>
		   			<xsl:value-of select="r:transTypeName"/>
		   		</label>
		   	</li>
			</xsl:for-each>
			</ul>
			</div>
			</li>
			</ul>
    	</fieldset>
	</div>
  	<div id="groupings" class="buildCritDiv">Columns:<br/>
  		<table id="filterTable">
    	<xsl:variable name="sortBy" select="concat(normalize-space(reflect:getProperty($context, 'sortBy')), ',')"/>    	
		<tr><td style="white-space: nowrap;">Total By:<span class="required">*</span></td><td>
    		<xsl:call-template name="groupBySelect">
				<xsl:with-param name="value">
					<xsl:choose>
						<xsl:when test="number(substring-before($sortBy, ',')) &gt; 0"><xsl:value-of select="number(substring-before($sortBy, ','))"/></xsl:when>
						<xsl:otherwise>0</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param> 
			</xsl:call-template></td></tr>
    		<xsl:call-template name="thenByGrouping">
				<xsl:with-param name="sortByRemains" select="substring-after($sortBy, ',')" /> 
			</xsl:call-template>
    	</table>
    </div>
  	<div id="dValues" class="buildCritDiv">Data Values:<span class="required">*</span>&#160;<input type="checkbox" onclick="checkAll(this, 'showValues,showPercents,showAverages')" title="Toggle Data Values" /><br/>
  		<xsl:call-template name="valuesCheckboxes">
			<xsl:with-param name="advanced" select="/a:base/b:advancedFeatures" /> 
		</xsl:call-template>
  	</div>
  	</td>
	<td valign="top">
		<img alt="Sample Summary Report">
			<xsl:attribute name="src"><xsl:call-template name="uri"><xsl:with-param name="uri">images/rpt2dgm.gif</xsl:with-param></xsl:call-template></xsl:attribute>
		</img>
	</td></tr>
	<tr><td colspan="2" class="buildCrit">
	<div id="terms" class="buildCritDiv">Companies / Regions / Devices:<span class="required">*</span><br/>
		<xsl:call-template name="contents-select-terminal"/>
	</div>
</td></tr>
<tr>
    <td colspan="2" class="buildCrit">
    <div id="outputType" class="buildCritDiv">Format:<span class="required">*</span><br/>
    <xsl:variable name="outputType" select="reflect:getProperty($context, 'outputType')"/>
    <label class="checkbox" id="report-button-html"><input type="radio" name="outputType" value="22">
    <xsl:if test="$outputType = 22 or not(boolean($outputType))"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
                </input><div class="output-type-label"/> (Html)</label>
    <label class="checkbox" id="report-button-excel"><input type="radio" name="outputType" value="27">
    <xsl:if test="$outputType = 27"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
                </input><div class="output-type-label"/> (Excel)</label>        
    <label class="checkbox" id="report-button-pdf"><input type="radio" name="outputType" value="24">
    <xsl:if test="$outputType = 24"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
                </input><div class="output-type-label"/> (Pdf)</label>
    <label class="checkbox" id="report-button-doc"><input type="radio" name="outputType" value="23">
    <xsl:if test="$outputType = 23"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
                </input><div class="output-type-label"/> (Word)</label>
    <label class="checkbox" id="report-button-csv"><input type="radio" name="outputType" value="21">
    <xsl:if test="$outputType = 21"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
                </input><div class="output-type-label"/> (Csv)</label>
    </div>
    </td>
  </tr>
<tr><td colspan="2"><span class="required">* Required</span></td></tr>
<tr><td colspan="2" align="center"><input type="submit" value="Run Report" id="Submit" /></td></tr></table></form>
	</div>
	</xsl:template>
	
	<xsl:template name="thenByGrouping">
		<xsl:param name="sortByRemains" select="0"/>
		<xsl:variable name="thenByValue" select="number(substring-before($sortByRemains, ','))"/>
		<tr><td>Then By:</td><td>
    		<xsl:call-template name="groupBySelect">
				<xsl:with-param name="value">
					<xsl:choose>
						<xsl:when test="$thenByValue &gt; 0"><xsl:value-of select="$thenByValue"/></xsl:when>
						<xsl:otherwise>0</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param> 
			</xsl:call-template></td></tr>
		<xsl:variable name="thenByRemains" select="normalize-space(substring-after($sortByRemains, ','))"/>
		<xsl:if test="$thenByValue &gt; 0 and string-length($thenByRemains) &gt; 0"> 
			<xsl:call-template name="thenByGrouping">
				<xsl:with-param name="sortByRemains" select="$thenByRemains" /> 
			</xsl:call-template>
		</xsl:if>
   </xsl:template>
</xsl:stylesheet>

