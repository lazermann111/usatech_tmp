<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:b="http://simple/bean/1.0"
	xmlns:x2="http://simple/translate/2.0"
	xmlns:p="http://simple/xml/parameters/1.0"
    xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect a b x2 p">
	<xsl:import href="../general/app-base.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:param name="context" />
	<xsl:variable name="embedded" select="normalize-space(/a:base/b:embedded) = 'true'"/>
	<xsl:template name="extra-scripts">	
		<script type="text/javascript">
		var ajax = new Request({ 
            url : "report_ajax_poll_retrieve.i", 
            method: 'post',
            evalScripts: true,
            data:{requestId: <xsl:value-of select="/a:base/b:requestId"/>, pageId: <xsl:value-of select="/a:base/b:pageId"/>, profileId: <xsl:value-of select="/a:base/b:profileId"
                 />, fragment: true<xsl:if test="$embedded">, embedded: true</xsl:if>}
        });
        function sendAjaxPoll(pIndex){
            ajax.options.data.pollIntervalIndex = pIndex;
            ajax.send();
        }
        function ajaxCancel(){
            ajax.cancel();
        }
        window.addEvent("domready", function() { setTimeout("sendAjaxPoll(1)",  500); });
        </script>		
	</xsl:template>
	<xsl:template name="contents">
		<iframe id="hiddenFrame" name="hiddenFrame" style="width:0px; height:0px; border: 0px" src=""></iframe>
  		<div id="cancelScreen">
		<div class="confirmText">
		  <div><x2:translate key="report-please-wait-header">Your report is being generated.</x2:translate><img alt="Please wait" src="images/pleasewait.gif"/></div>
		  <xsl:if test="not($embedded)">
		  <div class="confirmNote">Depending on your selection it may take a while to complete.<br/>You may view the report later by selecting the "Pick up later" button. 
		    <xsl:if test="not(a:base/b:inProgress)">
		    	Or cancel the report by selecting the "Cancel" button.
		    </xsl:if>
			</div>
			</xsl:if>
		</div>
		<xsl:if test="not($embedded)">	
		<form name="cancelChoiceForm" action="report_cancel_choice_result.i" method="post" target="_top">
		<input type="hidden" name="requestAction" value="pollReport" />
		<input type="hidden" name="requestId" value="{/a:base/b:requestId}"/>
		<input type="hidden" name="pageId" value="{/a:base/b:pageId}"/>
		<input type="hidden" name="profileId" value="{/a:base/b:profileId}"/>
		<span style="text-align: center; width: 100%;"><input type="submit" value="Pick up later" onclick="form.requestAction.value='pickUpLater';"/></span>
		<xsl:if test="not(a:base/b:inProgress)">
			<span style="text-align: center; width: 100%;"><input type="submit" value="Cancel" onclick="form.requestAction.value='cancel';ajaxCancel();"/></span>
		</xsl:if>
		</form>
		</xsl:if>
		</div>
	</xsl:template>
</xsl:stylesheet>