<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:convert="http://simple/xml/extensions/java/simple.bean.ConvertUtils"
    xmlns:a="http://simple/aggregating/1.0"
	xmlns:b="http://simple/bean/1.0"
	xmlns:x2="http://simple/translate/2.0"
	xmlns:r="http://simple/results/1.0"
	xmlns:p="http://simple/xml/parameters/1.0"
	xmlns:cmn="http://exslt.org/common"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect x2 a b r p cmn convert">
	<xsl:import href="../selection/select-terminal-customers.xsl"/>
	<xsl:import href="parameters_base.xsl"/>
	<xsl:import href="../general/tabs.xsl" />
    <xsl:output method="xhtml" />
	<xsl:param name="context" />
	
	<xsl:template name="subtitle">
		Activity Parameters
	</xsl:template>
	
	<xsl:template name="extra-scripts">
		<script type="text/javascript">
	var minDate = new Date("<xsl:value-of select="convert:formatObject(reflect:getProperty($context, 'minDate'), 'DATE:MM/dd/yyyy HH:')"/>00");
	var minAllDate = new Date("<xsl:value-of select="convert:formatObject(reflect:getProperty($context, 'minDetailDate'), 'DATE:MM/dd/yyyy HH:')"/>00");
	window.addEvent("domready", function() {
		//new Form.Validator.Inline.Mask($("criteriaForm"));
	});
    
	function eventChanged(evt) {
        if(evt == "MONTH") {
            $("beginMonth").disabled = false;
            $("endMonth").disabled = false;
            $("beginDay").disabled = true;
            $("endDay").disabled = true;
            $("beginYear").disabled = false;
            $("endYear").disabled = false;
            $("beginTime").disabled = true;
            $("endTime").disabled = true;
        } else if(evt == "DAY") {
            $("beginMonth").disabled = false;
            $("endMonth").disabled = false;
            $("beginDay").disabled = false;
            $("endDay").disabled = false;
            $("beginYear").disabled = false;
            $("endYear").disabled = false;
            $("beginTime").disabled = true;
            $("endTime").disabled = true;
        } else if(evt == "WEEK") {
            $("beginMonth").disabled = false;
            $("endMonth").disabled = false;
            $("beginDay").disabled = false;
            $("endDay").disabled = false;
            $("beginYear").disabled = false;
            $("endYear").disabled = false;
            $("beginTime").disabled = true;
            $("endTime").disabled = true;
        } else { //FILL
            $("beginMonth").disabled = false;
            $("endMonth").disabled = false;
            $("beginDay").disabled = false;
            $("endDay").disabled = false;
            $("beginYear").disabled = false;
            $("endYear").disabled = false;
            $("beginTime").disabled = false;
            $("endTime").disabled = false;
        }
    }
    
	function setDates() {
    	var evt = $("rangeType").getValue();
        if(evt == "MONTH") {
            var beginDate = new Date($("beginYear").getValue(), $("beginMonth").selectedIndex);
            if(beginDate.getFullYear() &lt; minDate.getFullYear() || (beginDate.getFullYear() == minDate.getFullYear() &amp;&amp; beginDate.getMonth() &lt; minDate.getMonth())) {
                alert("You selected a Begin Date that is earlier than allowed. The Begin Date will be adjusted to the earliest allowed");
                $("beginMonth").selectedIndex = minDate.getMonth();
                $("beginYear").setValue(minDate.getFullYear());
                return false;
            }
            var endDate = new Date($("endYear").getValue(), $("endMonth").selectedIndex);
            if(endDate.getTime() &lt; beginDate.getTime()) {
                alert("You selected an End Date that is earlier than the Begin Date. The End Date will be adjusted");
                $("endMonth").selectedIndex = beginDate.getMonth();
                $("endYear").setValue(beginDate.getFullYear());
                return false;
            }
            $("beginDate").value = ($("beginMonth").getValue() + ", " +  $("beginYear").getValue());
            $("endDate").value = ($("endMonth").getValue() + ", " + $("endYear").getValue());
            $("dateFormat").value = ("MMMM, yyyy");
        } else if(evt == "DAY") {
            var beginDate = new Date($("beginYear").getValue(), $("beginMonth").selectedIndex, $("beginDay").getValue());
            if(beginDate.getFullYear() &lt; minDate.getFullYear() || (beginDate.getFullYear() == minDate.getFullYear() &amp;&amp; beginDate.getMonth() &lt; minDate.getMonth())
             || (beginDate.getFullYear() == minDate.getFullYear() &amp;&amp; beginDate.getMonth() == minDate.getMonth() &amp;&amp; beginDate.getDate() &lt; minDate.getDate())) {
                alert("You selected a Begin Date that is earlier than allowed. The Begin Date will be adjusted to the earliest allowed");
                $("beginDay").setValue(minDate.getDate());
                $("beginMonth").selectedIndex = minDate.getMonth();
                $("beginYear").setValue(minDate.getFullYear());
                return false;
            }
            var endDate = new Date($("endYear").getValue(), $("endMonth").selectedIndex, $("endDay").getValue());
            if(endDate.getTime() &lt; beginDate.getTime()) {
                alert("You selected an End Date that is earlier than the Begin Date. The End Date will be adjusted");
                $("endDay").setValue(beginDate.getDate());
                $("endMonth").selectedIndex = beginDate.getMonth();
                $("endYear").setValue(beginDate.getFullYear());
                return false;
            }
            $("beginDate").value = ($("beginMonth").getValue() + " " + $("beginDay").getValue() + ", " + $("beginYear").getValue());
            $("endDate").value = ($("endMonth").getValue() + " " + $("endDay").getValue() + ", " + $("endYear").getValue());
            $("dateFormat").value = ("MMMM dd, yyyy");
        } else if(evt == "WEEK") {
            var beginDate = new Date($("beginYear").getValue(), $("beginMonth").selectedIndex, $("beginDay").getValue());
            if(beginDate.getFullYear() &lt; minDate.getFullYear() || (beginDate.getFullYear() == minDate.getFullYear() &amp;&amp; beginDate.getMonth() &lt; minDate.getMonth())
             || (beginDate.getFullYear() == minDate.getFullYear() &amp;&amp; beginDate.getMonth() == minDate.getMonth() &amp;&amp; beginDate.getDate() &lt; minDate.getDate())) {
                alert("You selected a Begin Date that is earlier than allowed. The Begin Date will be adjusted to the earliest allowed");
                $("beginDay").setValue(minDate.getDate());
                $("beginMonth").selectedIndex = minDate.getMonth();
                $("beginYear").setValue(minDate.getFullYear());
                return false;
            }
            var endDate = new Date($("endYear").getValue(), $("endMonth").selectedIndex, $("endDay").getValue());
            if(endDate.getTime() &lt; beginDate.getTime()) {
                alert("You selected an End Date that is earlier than the Begin Date. The End Date will be adjusted");
                $("endDay").setValue(beginDate.getDate());
                $("endMonth").selectedIndex = beginDate.getMonth();
                $("endYear").setValue(beginDate.getFullYear());
                return false;
            }
            $("beginDate").value = ($("beginMonth").getValue() + " " + $("beginDay").getValue() + ", " + $("beginYear").getValue());
            $("endDate").value = ($("endMonth").getValue() + " " + $("endDay").getValue() + ", " + $("endYear").getValue());
            $("dateFormat").value = ("MMMM dd, yyyy");
        } else { //FILL
           var beginDate = new Date($("beginYear").getValue(), $("beginMonth").selectedIndex, $("beginDay").getValue(), parseInt($("beginTime").getValue()));
            if(beginDate.getTime() &lt; minAllDate.getTime()) {
                alert("You selected a Begin Date that is earlier than allowed. The Begin Date will be adjusted to the earliest allowed");
                $("beginDay").setValue(minAllDate.getDate());
                $("beginMonth").selectedIndex = minAllDate.getMonth();
                $("beginYear").setValue(minAllDate.getFullYear());
                $("beginTime").setValue(minAllDate.getHours() + ":00");
                return false;
            }
            var endDate = new Date($("endYear").getValue(), $("endMonth").selectedIndex, $("endDay").getValue(), parseInt($("endTime").getValue()));
            if(endDate.getTime() &lt;= beginDate.getTime()) {
                alert("You selected an End Date that is not greater than the Begin Date. The End Date will be adjusted");
                beginDate = new Date(beginDate.getTime() + (60*60*1000));
                $("endDay").setValue(beginDate.getDate());
                $("endMonth").selectedIndex = beginDate.getMonth();
                $("endYear").setValue(beginDate.getFullYear());
                $("endTime").setValue(beginDate.getHours() + ":00");
                return false;
            }
            $("beginDate").value = ($("beginMonth").getValue() + " " + $("beginDay").getValue() + ", " + $("beginYear").getValue() + " " + $("beginTime").getValue());
            $("endDate").value = ($("endMonth").getValue() + " " + $("endDay").getValue() + ", " + $("endYear").getValue() + " " + $("endTime").getValue());
            $("dateFormat").value = ("MMMM dd, yyyy h:mma");
        }
    }
    
	window.addEvent('domready', function() {
		eventChanged($("rangeType").getValue()); 
	});
	
    	</script>
    	<xsl:call-template name="extra-scripts-select-terminal"/>
	</xsl:template>
	
	<xsl:template name="tab-contents">
	<span class="caption"><xsl:value-of select="reflect:getProperty($context, 'reportTitle')" /></span>
	<form id="criteriaForm" name="criteria" method="post" action="activity.i" onsubmit="return validateTerminals()&amp;&amp;setDates();">
	<input type="hidden" name="dateFormat" id="dateFormat"/>
	<input type="hidden" name="params.beginDate" id="beginDate"/>
	<input type="hidden" name="params.endDate" id="endDate"/>
	<input type="hidden" name="referrer" value="activity_parameters.i"/>
    <input type="hidden" name="saveAction" value="save_report_params_prompt.i"/>
	<table class="selectBody"><tr><td>
	<div class="selectSection" id="selectDevices">
		<div class="sectionTitle"><x2:translate key="selection-terminal-list" is-markup="true">Please search for devices or expand by clicking on the name or + sign<br/>and select the company(ies), region(s), and/or device(s):</x2:translate></div>
		<div class="sectionRow">
		<xsl:call-template name="contents-select-terminal"/>
		</div>
	</div>
	</td></tr>
	<tr><td>
	<div class="selectSection" id="selectRangeAndType">
		<div class="sectionTitle"><x2:translate key="selection-range-and-type" is-markup="true">Now select the event type, date range, and transaction type:</x2:translate></div>
		<div class="sectionRow">
<table class="paramTable" cellSpacing="3" cellPadding="3" border="1">
  <tr>
    <td class="paramCell">Event Type</td>
    <td class="paramCell">
    	<xsl:call-template name="rangeTypeSelect">
    		<xsl:with-param name="extra-attributes"><attributes><attribute name="onchange" value="eventChanged($(this).getValue())"/></attributes></xsl:with-param>
    	</xsl:call-template>
    </td>
    <td class="paramCell" colSpan="3"><font size="1">Note: Fill date may
      include transactions outside selected date ranges</font></td></tr>
  <tr>
  	<td class="paramCell">Beginning</td>
  	<td class="paramCell">
    	<select id="beginMonth">
	    	<xsl:call-template name="monthOptions">
	    		<xsl:with-param name="select-month" select="/a:base/b:calendar/b:beginMonth"/>
	    	</xsl:call-template>
        </select>
  	</td>
  	<td class="paramCell">
  		<select id="beginDay">
        	<xsl:call-template name="dateNumberOptions">
				<xsl:with-param name="value" select="1" />
				<xsl:with-param name="select-value" select="/a:base/b:calendar/b:beginDay" />
				<xsl:with-param name="max-value" select="31" />	 
			</xsl:call-template>
        </select>
    </td>
    <td class="paramCell">
    	<select id="beginYear">
    		<xsl:call-template name="dateNumberOptions">
				<xsl:with-param name="value" select="/a:base/b:calendar/b:minYear" />
				<xsl:with-param name="select-value" select="/a:base/b:calendar/b:beginYear" />
				<xsl:with-param name="max-value" select="/a:base/b:calendar/b:maxYear" />	 
			</xsl:call-template>
        </select>
    </td>
    <td class="paramCell">
    	<select id="beginTime">
    		<xsl:call-template name="hourOptions">
    			<xsl:with-param name="select-hour" select="/a:base/b:calendar/b:beginHour" />
			</xsl:call-template>
        </select>
  	</td>
  </tr>
  <tr>
    <td class="paramCell">Ending</td>
    <td class="paramCell">
    	<select id="endMonth">
        	<xsl:call-template name="monthOptions">
	    		<xsl:with-param name="select-month" select="/a:base/b:calendar/b:endMonth"/>
	    	</xsl:call-template>
        </select>
  	</td>
  <td class="paramCell">
  		<select id="endDay">
        	<xsl:call-template name="dateNumberOptions">
				<xsl:with-param name="value" select="1" />
				<xsl:with-param name="select-value" select="/a:base/b:calendar/b:endDay" />
				<xsl:with-param name="max-value" select="31" />	 
			</xsl:call-template>
        </select>
    </td>
    <td class="paramCell">
    	<select id="endYear">
    		<xsl:call-template name="dateNumberOptions">
				<xsl:with-param name="value" select="/a:base/b:calendar/b:minYear" />
				<xsl:with-param name="select-value" select="/a:base/b:calendar/b:endYear" />
				<xsl:with-param name="max-value" select="/a:base/b:calendar/b:maxYear" />	 
			</xsl:call-template>
        </select>
    </td>
    <td class="paramCell">
    	<select id="endTime">
    		<xsl:call-template name="hourOptions">
    			<xsl:with-param name="select-hour" select="/a:base/b:calendar/b:endHour" />
			</xsl:call-template>
        </select>
  	</td>
  </tr>
  <tr>
    <td class="paramCell nowrap">
    <div class="closedTreeNode" id="tranCategoryTreeAll">
    <a class="treeImage" onclick="hideTranTypeAll()">&#160;&#160;</a>
    <input id="tranTypeAll" type="checkbox" checked="checked" onclick="checkAllTriggered(this.checked);checkAll(this, 'params.tranType')" title="Toggle Transaction Types" />
    <a class="treeLabel" onclick="hideTranTypeAll()">
    &#160;Transaction Type
    </a>
    </div>
    </td>
    <td class="paramCell" colSpan="4">
    	<xsl:variable name="tran-type" select="concat(',', translate(reflect:getProperty($context, 'params.tranType'), ' ', ''), ',')"/>
    	<fieldset class="no-border-grouper" name="params.tranType" data-validators="validate-reqchk-byname">
    		<xsl:attribute name="data-validator-properties">{label:'transaction type'}</xsl:attribute>  
    		<ul class="customers tree"> 
    		<li class="closedTreeNode" id="tranCategoryTree1">
    		<a class="treeImage" onclick="hideTranType(1)">&#160;&#160;</a>
    		<input checked="checked" id="tranCategoryCheckbox1" type="checkbox" name="tranCategoryCheckbox1" onclick="toggleTranType(1, this.checked)"/>
			<a class="treeLabel" onclick="hideTranType(1)" title="Cashless transactions with electronic funds transfer between USAT and Operator"> Cashless (Payment)</a>
			<div id="tranCategory1" class="childTreeDiv" style="display:none">
			<ul class="customers tree"> 
		   	<xsl:for-each select="a:base/a:transTypes/r:results/r:row[r:tranCategory='1']">
		   	<li class="openTreeNode">
		   		<label  class="checkbox" data-trancategory="{r:tranCategory}">
		   			<input type="checkbox" data-trancategory="{r:tranCategory}" value="{r:transTypeId}" name="params.tranType">
		   			<xsl:if test="$tran-type = ',,' or contains($tran-type, concat(',', r:transTypeId, ','))"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
		   			</input>
		   			<xsl:value-of select="r:transTypeName"/>
		   		</label>
		   	</li>
			</xsl:for-each>
			</ul>
			</div>
			</li>
			</ul>
    	</fieldset>
    	<fieldset class="no-border-grouper" name="params.tranType" data-validators="validate-reqchk-byname">
    		<xsl:attribute name="data-validator-properties">{label:'transaction type'}</xsl:attribute>  
    		<ul class="customers tree"> 
    		<li class="closedTreeNode" id="tranCategoryTree2">
    		<a class="treeImage" onclick="hideTranType(2)">&#160;&#160;</a>
    		<input checked="checked" id="tranCategoryCheckbox2" type="checkbox" name="tranCategoryCheckbox2" onclick="toggleTranType(2, this.checked)"/>
			<a class="treeLabel" onclick="hideTranType(2)" title="Cashless transactions with no electronic funds transfer between USAT and Operator"> Cashless (No Payment)</a>
			<div id="tranCategory2" class="childTreeDiv" style="display:none">
			<ul class="customers tree"> 
		   	<xsl:for-each select="a:base/a:transTypes/r:results/r:row[r:tranCategory='2']">
		   	<li class="openTreeNode">
		   		<label  class="checkbox" data-trancategory="{r:tranCategory}">
		   			<input type="checkbox" data-trancategory="{r:tranCategory}" value="{r:transTypeId}" name="params.tranType">
		   			<xsl:if test="$tran-type = ',,' or contains($tran-type, concat(',', r:transTypeId, ','))"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
		   			</input>
		   			<xsl:value-of select="r:transTypeName"/>
		   		</label>
		   	</li>
			</xsl:for-each>
			</ul>
			</div>
			</li>
			</ul>
    	</fieldset>
    	<fieldset class="no-border-grouper" name="params.tranType" data-validators="validate-reqchk-byname">
    		<xsl:attribute name="data-validator-properties">{label:'transaction type'}</xsl:attribute>  
    		<ul class="customers tree"> 
    		<li class="closedTreeNode" id="tranCategoryTree3">
    		<a class="treeImage" onclick="hideTranType(3)">&#160;&#160;</a>
    		<input checked="checked" id="tranCategoryCheckbox3" type="checkbox" name="tranCategoryCheckbox3" onclick="toggleTranType(3, this.checked)"/>
			<a class="treeLabel" onclick="hideTranType(3)" title="Cash transactions"> Cash</a>
			<div id="tranCategory3" class="childTreeDiv" style="display:none">
			<ul class="customers tree"> 
		   	<xsl:for-each select="a:base/a:transTypes/r:results/r:row[r:tranCategory='3']">
		   	<li class="openTreeNode">
		   		<label  class="checkbox" data-trancategory="{r:tranCategory}">
		   			<input type="checkbox" data-trancategory="{r:tranCategory}" value="{r:transTypeId}" name="params.tranType">
		   			<xsl:if test="$tran-type = ',,' or contains($tran-type, concat(',', r:transTypeId, ','))"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
		   			</input>
		   			<xsl:value-of select="r:transTypeName"/>
		   		</label>
		   	</li>
			</xsl:for-each>
			</ul>
			</div>
			</li>
			</ul>
    	</fieldset>
    </td>
  </tr>
  <tr>
    <td class="paramCell nowrap">Group By</td>
    <td class="paramCell" colSpan="4">
        <xsl:variable name="sortBy" select="concat(',', translate(reflect:getProperty($context, 'sortBy'), ' ', ''), ',')"/>
        <fieldset class="no-border-grouper" name="sortBy">
	        <xsl:if test="boolean($user) and (reflect:getProperty($user, 'customerId') = 0 or reflect:getProperty($user, 'userType') != 8)">
	        <label class="checkbox">
                <input type="checkbox" value="1" name="sortBy">
                <xsl:if test="$sortBy = ',,' or contains($sortBy, ',1,')"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
                </input>
               Customer
            </label>
            </xsl:if>
            <label class="checkbox">
                <input type="checkbox" value="13" name="sortBy">
                <xsl:if test="$sortBy = ',,' or contains($sortBy, ',13,')"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
                </input>
                Region
            </label>
            <label class="checkbox">
                <input type="checkbox" value="14" name="sortBy">
                <xsl:if test="contains($sortBy, ',14,')"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
                </input>
                Client
            </label>
            <label class="checkbox">
                <input type="checkbox" value="2" name="sortBy">
                <xsl:if test="$sortBy = ',,' or contains($sortBy, ',2,')"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
                </input>
                Device <!--  TODO: use preference[0] -->
            </label>
            
        </fieldset>
    </td>
  </tr>
  <tr>
    <td class="paramCell">Format</td>
    <td class="paramCell" colspan="4">
        <xsl:variable name="outputType" select="reflect:getProperty($context, 'outputType')"/>
    <label class="checkbox" id="report-button-html"><input type="radio" name="outputType" value="22">
    <xsl:if test="$outputType = 22 or not(boolean($outputType))"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
                </input><div class="output-type-label"/> (Html)</label>
    <label class="checkbox" id="report-button-excel"><input type="radio" name="outputType" value="27">
    <xsl:if test="$outputType = 27"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
                </input><div class="output-type-label"/> (Excel)</label>        
    <label class="checkbox" id="report-button-pdf"><input type="radio" name="outputType" value="24">
    <xsl:if test="$outputType = 24"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
                </input><div class="output-type-label"/> (Pdf)</label>
    <label class="checkbox" id="report-button-doc"><input type="radio" name="outputType" value="23">
    <xsl:if test="$outputType = 23"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
                </input><div class="output-type-label"/> (Word)</label>
    <label class="checkbox" id="report-button-csv"><input type="radio" name="outputType" value="21">
    <xsl:if test="$outputType = 21"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
                </input><div class="output-type-label"/> (Csv)</label>
    </td>
  </tr>
  </table>
</div></div>
</td></tr>
<tr><td><div align="center" style="float: center"><input type="submit" value="Run Report" name="Submit"/></div></td></tr>
</table>
</form>
	</xsl:template>
</xsl:stylesheet>

