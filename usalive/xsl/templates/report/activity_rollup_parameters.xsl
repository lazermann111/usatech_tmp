<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:conv="http://simple/xml/extensions/java/simple.bean.ConvertUtils"
    xmlns:a="http://simple/aggregating/1.0"
	xmlns:b="http://simple/bean/1.0"
	xmlns:x2="http://simple/translate/2.0"
	xmlns:r="http://simple/results/1.0"
	xmlns:p="http://simple/xml/parameters/1.0"
	xmlns:cmn="http://exslt.org/common"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect x2 a b r p cmn conv">
	<xsl:import href="../selection/select-terminal-customers.xsl"/>
	<xsl:import href="parameters_base.xsl"/>
	<xsl:import href="../general/tabs.xsl" />
    <xsl:output method="xhtml" />
	<xsl:param name="context" />
	
	<xsl:template name="subtitle">
		Activity Rollup Parameters
	</xsl:template>
	
	<xsl:template name="extra-scripts">
        <script type="text/javascript">
            <xsl:attribute name="src"><xsl:call-template name="uri"><xsl:with-param name="uri">selection/report-selection-scripts.js</xsl:with-param></xsl:call-template></xsl:attribute>
        </script>
    	<xsl:call-template name="extra-scripts-select-terminal"/>
    	<xsl:call-template name="validateDateRangeScript"/>
	</xsl:template>
	
	<xsl:template name="tab-contents">
	<span class="caption"><xsl:value-of select="reflect:getProperty($context, 'reportTitle')" /></span>
	<form id="criteriaForm" name="criteria" method="post" action="activity_rollup.i" onsubmit="return validateTerminals() &amp;&amp; validateDateRange('startDateId', 'endDateId')">
	<input type="hidden" name="dateFormat" id="dateFormat" value="MM/dd/yyyy"/>
	<input type="hidden" name="referrer" value="activity_rollup_parameters.i"/>
    <input type="hidden" name="saveAction" value="save_report_params_prompt.i"/>
	<table class="selectBody"><tr><td>
	<div class="selectSection" id="selectDevices">
		<div class="sectionTitle"><x2:translate key="selection-terminal-list" is-markup="true">Please search for devices or expand by clicking on the name or + sign<br/>and select the company(ies), region(s), and/or device(s):</x2:translate></div>
		<div class="sectionRow">
		<xsl:call-template name="contents-select-terminal"/>
		</div>
	</div>
	</td></tr>
	<tr><td>
	<div class="selectSection" id="selectRangeAndType">
		<div class="sectionTitle"><x2:translate key="selection-range-and-type" is-markup="true">Now select the date range, transaction type, and grouping:</x2:translate></div>
		<div class="sectionRow">
<table class="paramTable" cellSpacing="3" cellPadding="3" border="1">
  <tr>
    <td class="paramCell">Date Range</td>
    <td class="paramCell" colspan="4">
    	<div>
	       <label for="startDateId">Start</label>
	       <input type="text" id="startDateId" size="10" data-toggle="calendar" data-format="%m/%d/%Y" name="params.beginDate">
	           <xsl:attribute name="value">
	           <xsl:choose>
	               <xsl:when test="string-length(reflect:getProperty($context, 'params.beginDate')) &gt; 0">
	                   <xsl:value-of select="conv:formatObject(reflect:getProperty($context, 'params.beginDate'), 'DATE:MM/dd/yyyy')" />
	               </xsl:when>
	               <xsl:otherwise>
	                   <xsl:value-of select="conv:formatObject(reflect:getProperty($context, 'defaultStartDate'), 'DATE:MM/dd/yyyy')" />
	               </xsl:otherwise>
	           </xsl:choose>
	           </xsl:attribute>
	       </input>
	       
	       <label for="endDateId">End</label>
	       <input type="text" id="endDateId" size="10" data-toggle="calendar" data-format="%m/%d/%Y" name="params.endDate">
	           <xsl:attribute name="value">
	               <xsl:choose>
	                   <xsl:when test="string-length(reflect:getProperty($context, 'params.endDate')) &gt; 0">
	                       <xsl:value-of select="conv:formatObject(reflect:getProperty($context, 'params.endDate'), 'DATE:MM/dd/yyyy')" />
	                   </xsl:when>
	                   <xsl:otherwise>
	                       <xsl:value-of select="conv:formatObject(reflect:getProperty($context, 'defaultEndDate'), 'DATE:MM/dd/yyyy')" />
	                   </xsl:otherwise>
	               </xsl:choose>
	           </xsl:attribute>
	       </input>
         </div>
         <div>
           <label class="radioLabel"><input type="radio" name="dateRange" value="Yesterday" onclick="setDatesToYesterday()"/>Yesterday</label>
           <label class="radioLabel"><input type="radio" name="dateRange" value="Week to Date" onclick="setDatesToWeekToDate()"/>Week to Date</label>
           <label class="radioLabel"><input type="radio" name="dateRange" value="Last Week" onclick="setDatesToLastWeek()" />Last Week</label>
           <label class="radioLabel"><input type="radio" name="dateRange" value="Month to Date" onclick="setDatesToMonthToDate()"/>Month to Date</label>
           <label class="radioLabel"><input type="radio" name="dateRange" value="Previous Month" onclick="setDatesToLastMonth()"/>Previous Month</label>
       </div>
    </td>
  </tr>
  <tr>
    <td class="paramCell nowrap">
    <div class="closedTreeNode" id="tranCategoryTreeAll">
    <a class="treeImage" onclick="hideTranTypeAll()">&#160;&#160;</a>
    <input id="tranTypeAll" type="checkbox" checked="checked" onclick="checkAllTriggered(this.checked);checkAll(this, 'params.tranType')" title="Toggle Transaction Types" />
    <a class="treeLabel" onclick="hideTranTypeAll()">
    &#160;Transaction Type
    </a>
    </div>
    </td>
    <td class="paramCell" colSpan="4">
    	<xsl:variable name="tran-type" select="concat(',', translate(reflect:getProperty($context, 'params.tranType'), ' ', ''), ',')"/>
    	<fieldset class="no-border-grouper" name="params.tranType" data-validators="validate-reqchk-byname">
    		<xsl:attribute name="data-validator-properties">{label:'transaction type'}</xsl:attribute>  
    		<ul class="customers tree"> 
    		<li class="closedTreeNode" id="tranCategoryTree1">
    		<a class="treeImage" onclick="hideTranType(1)">&#160;&#160;</a>
    		<input checked="checked" id="tranCategoryCheckbox1" type="checkbox" name="tranCategoryCheckbox1" onclick="toggleTranType(1, this.checked)"/>
			<a class="treeLabel" onclick="hideTranType(1)" title="Cashless transactions with electronic funds transfer between USAT and Operator"> Cashless (Payment)</a>
			<div id="tranCategory1" class="childTreeDiv" style="display:none">
			<ul class="customers tree"> 
		   	<xsl:for-each select="a:base/a:transTypes/r:results/r:row[r:tranCategory='1']">
		   	<li class="openTreeNode">
		   		<label  class="checkbox" data-trancategory="{r:tranCategory}">
		   			<input type="checkbox" data-trancategory="{r:tranCategory}" value="{r:transTypeId}" name="params.tranType">
		   			<xsl:if test="$tran-type = ',,' or contains($tran-type, concat(',', r:transTypeId, ','))"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
		   			</input>
		   			<xsl:value-of select="r:transTypeName"/>
		   		</label>
		   	</li>
			</xsl:for-each>
			</ul>
			</div>
			</li>
			</ul>
    	</fieldset>
    	<fieldset class="no-border-grouper" name="params.tranType" data-validators="validate-reqchk-byname">
    		<xsl:attribute name="data-validator-properties">{label:'transaction type'}</xsl:attribute>  
    		<ul class="customers tree"> 
    		<li class="closedTreeNode" id="tranCategoryTree2">
    		<a class="treeImage" onclick="hideTranType(2)">&#160;&#160;</a>
    		<input checked="checked" id="tranCategoryCheckbox2" type="checkbox" name="tranCategoryCheckbox2" onclick="toggleTranType(2, this.checked)"/>
			<a class="treeLabel" onclick="hideTranType(2)" title="Cashless transactions with no electronic funds transfer between USAT and Operator"> Cashless (No Payment)</a>
			<div id="tranCategory2" class="childTreeDiv" style="display:none">
			<ul class="customers tree"> 
		   	<xsl:for-each select="a:base/a:transTypes/r:results/r:row[r:tranCategory='2']">
		   	<li class="openTreeNode">
		   		<label  class="checkbox" data-trancategory="{r:tranCategory}">
		   			<input type="checkbox" data-trancategory="{r:tranCategory}" value="{r:transTypeId}" name="params.tranType">
		   			<xsl:if test="$tran-type = ',,' or contains($tran-type, concat(',', r:transTypeId, ','))"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
		   			</input>
		   			<xsl:value-of select="r:transTypeName"/>
		   		</label>
		   	</li>
			</xsl:for-each>
			</ul>
			</div>
			</li>
			</ul>
    	</fieldset>
    	<fieldset class="no-border-grouper" name="params.tranType" data-validators="validate-reqchk-byname">
    		<xsl:attribute name="data-validator-properties">{label:'transaction type'}</xsl:attribute>  
    		<ul class="customers tree"> 
    		<li class="closedTreeNode" id="tranCategoryTree3">
    		<a class="treeImage" onclick="hideTranType(3)">&#160;&#160;</a>
    		<input checked="checked" id="tranCategoryCheckbox3" type="checkbox" name="tranCategoryCheckbox3" onclick="toggleTranType(3, this.checked)"/>
			<a class="treeLabel" onclick="hideTranType(3)" title="Cash transactions"> Cash</a>
			<div id="tranCategory3" class="childTreeDiv" style="display:none">
			<ul class="customers tree"> 
		   	<xsl:for-each select="a:base/a:transTypes/r:results/r:row[r:tranCategory='3']">
		   	<li class="openTreeNode">
		   		<label  class="checkbox" data-trancategory="{r:tranCategory}">
		   			<input type="checkbox" data-trancategory="{r:tranCategory}" value="{r:transTypeId}" name="params.tranType">
		   			<xsl:if test="$tran-type = ',,' or contains($tran-type, concat(',', r:transTypeId, ','))"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
		   			</input>
		   			<xsl:value-of select="r:transTypeName"/>
		   		</label>
		   	</li>
			</xsl:for-each>
			</ul>
			</div>
			</li>
			</ul>
    	</fieldset>
    </td>
  </tr>
  <tr>
    <td class="paramCell nowrap">Group By</td>
    <td class="paramCell" colSpan="4">
        <xsl:variable name="sortBy" select="concat(',', translate(reflect:getProperty($context, 'sortBy'), ' ', ''), ',')"/>
        <fieldset class="no-border-grouper" name="sortBy">
	        <xsl:if test="boolean($user) and (reflect:getProperty($user, 'customerId') = 0 or reflect:getProperty($user, 'userType') != 8)">
	        <label class="checkbox">
                <input type="checkbox" value="1" name="sortBy">
                <xsl:if test="contains($sortBy, ',1,')"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
                </input>
               Customer
            </label>
            </xsl:if>
            <label class="checkbox">
                <input type="checkbox" value="13" name="sortBy">
                <xsl:if test="contains($sortBy, ',13,')"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
                </input>
                Region
            </label>
            <label class="checkbox">
                <input type="checkbox" value="14" name="sortBy">
                <xsl:if test="contains($sortBy, ',14,')"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
                </input>
                Client
            </label>
            <label class="checkbox">
                <input type="checkbox" value="2" name="sortBy">
                <xsl:if test="contains($sortBy, ',2,')"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
                </input>
                Device <!--  TODO: use preference[0] -->
            </label>
            
        </fieldset>
    </td>
  </tr>
  <tr>
    <td class="paramCell">Format</td>
    <td class="paramCell" colspan="4">
        <xsl:variable name="outputType" select="reflect:getProperty($context, 'outputType')"/>
    <label class="checkbox" id="report-button-excel"><input type="radio" name="outputType" value="27">
    <xsl:if test="$outputType = 27 or not(boolean($outputType))"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
                </input><div class="output-type-label"/> (Excel)</label>        
    <label class="checkbox" id="report-button-html"><input type="radio" name="outputType" value="22">
    <xsl:if test="$outputType = 22"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
                </input><div class="output-type-label"/> (Html)</label>
    <label class="checkbox" id="report-button-pdf"><input type="radio" name="outputType" value="24">
    <xsl:if test="$outputType = 24"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
                </input><div class="output-type-label"/> (Pdf)</label>
    <label class="checkbox" id="report-button-doc"><input type="radio" name="outputType" value="23">
    <xsl:if test="$outputType = 23"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
                </input><div class="output-type-label"/> (Word)</label>
    <label class="checkbox" id="report-button-csv"><input type="radio" name="outputType" value="21">
    <xsl:if test="$outputType = 21"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
                </input><div class="output-type-label"/> (Csv)</label>
    </td>
  </tr>
  </table>
</div></div>
</td></tr>
<tr><td><div align="center" style="float: center"><input type="submit" value="Run Report" name="Submit"/></div></td></tr>
</table>
</form>
	</xsl:template>
</xsl:stylesheet>

