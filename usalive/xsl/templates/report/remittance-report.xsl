<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils" 
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:r="http://simple/results/1.0" 
	xmlns="http://www.w3.org/1999/XSL/Format"
	exclude-result-prefixes="reflect r a">
	<xsl:output method="xml" omit-xml-declaration="yes"/>
	
	<xsl:param name="context" />

	<xsl:template match="/">
		<root language="en" hyphenate="true" font-size="11pt" font-family="serif" country="US">
			<layout-master-set>
			   <simple-page-master master-name="master-pages-for-all" 
			   		page-height="11in" page-width="8.5in" margin-bottom=".5in" 
			   		margin-top="1in" margin-left="1in" margin-right="1in">
					<region-before extent="0in"/>
			       	<region-body margin-top="0in" margin-bottom="5mm"/>
					<region-after extent="5mm"/>
			   </simple-page-master>

			   <page-sequence-master master-name="sequence-of-pages">
				    <repeatable-page-master-reference master-reference="master-pages-for-all" />
			   </page-sequence-master>
		  </layout-master-set>

 		  <page-sequence master-reference="sequence-of-pages">
			    <static-content flow-name="xsl-region-before">
			    </static-content>

				<static-content flow-name="xsl-region-after">
					<block text-align="center"><page-number /></block>
				</static-content>
			    <!--  <page-sequence master-reference="document" initial-page-number="1" format="1">-->
				<flow flow-name="xsl-region-body">
					<block unicode-bidi="embed">
						<xsl:call-template name="contents" />	
					</block>
				</flow>
			</page-sequence>
		</root>
	</xsl:template>
	
	<xsl:template name="header">
		<block>USA Technologies, Inc.</block>
		<block>100 Deerfield Lane, Suite 300</block>
		<block>Malvern, PA 19355</block>
		<block space-before="3mm">Customer Service: 1-800-633-0340</block>
	</xsl:template>
	
	<xsl:template name="contents">
		<xsl:variable name="doc" select="a:base/a:docs/r:results/r:row"/>
		<xsl:for-each select="$doc">
			<xsl:sort select="r:customerId" />
			<xsl:variable name="docId" select="r:docId" />
			<xsl:variable name="customerId" select="r:customerId" />
			<xsl:if test="count(/a:base/a:revenue/r:results/r:row[r:docId=$docId]) &gt; 0">
				<block>
					<xsl:if test="position() != last()">
						<xsl:attribute name="break-after">page</xsl:attribute>
					</xsl:if>
					<xsl:call-template name="header" />
				
					<xsl:call-template name="address">
						<xsl:with-param name="address" select="/a:base/a:address/r:results/r:row[r:customerId=$customerId]" />
					</xsl:call-template>
					
					<block space-before="10mm">
						<xsl:text>Remittance for batches dated: </xsl:text>
						<xsl:value-of select="substring(r:startDate,6,2)" />
						<xsl:text>/</xsl:text>
						<xsl:value-of select="substring(r:startDate,9,2)" />
						<xsl:text>/</xsl:text>
						<xsl:value-of select="substring(r:startDate,1,4)" />
						<xsl:text> - </xsl:text>
						<xsl:value-of select="substring(r:endDate,6,2)" />
						<xsl:text>/</xsl:text>
						<xsl:value-of select="substring(r:endDate,9,2)" />
						<xsl:text>/</xsl:text>
						<xsl:value-of select="substring(r:endDate,1,4)" />
					</block>
					
					<xsl:call-template name="revenue">
						<xsl:with-param name="rows" select="/a:base/a:revenue/r:results/r:row[r:docId=$docId]" />
					</xsl:call-template>
					
					<xsl:call-template name="share">
						<xsl:with-param name="rows" select="/a:base/a:fees/r:results/r:row[r:customerId=$customerId]" />
					</xsl:call-template>
				
					<xsl:call-template name="summary" />
				</block>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	
	<xsl:template name="address">
		<xsl:param name="address" />
		<block space-before="10mm" margin-left="5mm">
			<block><xsl:value-of select="$address/r:name" /></block>
			<block><xsl:value-of select="$address/r:address1" /></block>
			<block><xsl:value-of select="$address/r:address2" /></block>
			<block>
				<xsl:value-of select="$address/r:city" />, 
				<xsl:value-of select="$address/r:state" />
				<xsl:text> </xsl:text>
				<xsl:value-of select="$address/r:zip" />
			</block>
		</block>
	</xsl:template>
	
	<xsl:template name="revenue">
		<xsl:param name="rows" />
		<xsl:variable name="totalRevenueSum">
			<xsl:call-template name="sumAmounts">
				<xsl:with-param name="nodes" select="$rows/r:totalRevenue" />
				<xsl:with-param name="sum" select="0" />
 			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="netRevenueSum">
			<xsl:call-template name="sumAmounts">
				<xsl:with-param name="nodes" select="$rows/r:netRevenue" />
				<xsl:with-param name="sum" select="0" />
 			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="otherChargeSum">
			<xsl:call-template name="sumAmounts">
				<xsl:with-param name="nodes" select="$rows/r:otherCharges" />
				<xsl:with-param name="sum" select="0" />
 			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="remittance" select="$netRevenueSum - $otherChargeSum" />
		<!--<table-and-caption>-->
			<table space-after="5mm" font-size="small" border-collapse="collapse">
				<table-column column-width="19mm"/>	
				<table-column column-width="35mm"/>
				<table-column column-width="40mm"/>
				<table-column column-width="25mm"/>
				<table-column column-width="24mm"/>
				<table-column column-width="27mm"/>
	
				<table-header>
					<table-row>
						<table-cell>
							<block font-weight="bold" border-bottom-width=".1mm" border-bottom-color="black" 
								border-bottom-style="solid" border-top-width=".1mm" border-top-color="black" 
								border-top-style="solid">
								Device
							</block>
						</table-cell>
						<table-cell>
							<block font-weight="bold" border-bottom-width=".1mm" border-bottom-color="black" 
								border-bottom-style="solid" border-top-width=".1mm" border-top-color="black" 
								border-top-style="solid">
								Location
							</block>
						</table-cell>
						<table-cell>
							<block font-weight="bold" border-bottom-width=".1mm" border-bottom-color="black" 
								border-bottom-style="solid" border-top-width=".1mm" border-top-color="black" 
								border-top-style="solid">
								Transaction Type
							</block>
						</table-cell>
						<table-cell>
							<block font-weight="bold" border-bottom-width=".1mm" border-bottom-color="black" 
								border-bottom-style="solid" border-top-width=".1mm" border-top-color="black" 
								border-top-style="solid" text-align="right">
								Total Revenue
							</block>
						</table-cell>
						<table-cell>
							<block font-weight="bold" border-bottom-width=".1mm" border-bottom-color="black" 
								border-bottom-style="solid" border-top-width=".1mm" border-top-color="black" 
								border-top-style="solid" text-align="right">
								Net Revenue
							</block>
						</table-cell>
						<table-cell>
							<block font-weight="bold" border-bottom-width=".1mm" border-bottom-color="black" 
								border-bottom-style="solid" border-top-width=".1mm" border-top-color="black" 
								border-top-style="solid" text-align="right">
								Other Charges
							</block>
						</table-cell>
					</table-row>
				</table-header>
				
				<table-body>
					<xsl:for-each select="$rows">
						<xsl:sort select="r:terminalNumber" />
						<xsl:variable name="pos" select="position()" />
						<xsl:if test="$rows[$pos]/r:terminalNumber != $rows[$pos - 1]/r:terminalNumber">
							<table-row><table-cell number-columns-spanned="7"><block height="3mm">&#160;</block></table-cell></table-row>
						</xsl:if>
						<table-row>
							<xsl:choose>
								<xsl:when test="$rows[$pos]/r:terminalNumber = $rows[$pos - 1]/r:terminalNumber">
									<table-cell number-columns-spanned="2"><block>&#160;</block></table-cell>
								</xsl:when>
								<xsl:otherwise>
									<table-cell><block><xsl:value-of select="r:terminalNumber"/></block></table-cell>
									<table-cell><block><xsl:value-of select="r:location"/></block></table-cell>
								</xsl:otherwise>
							</xsl:choose>
							<table-cell><block><xsl:value-of select="r:transactionType"/></block></table-cell>
							<!-- TODO: We're going to have to do something to deal with currency types -->
							<table-cell>
								<block text-align="right">
									<xsl:choose>
										<xsl:when test="number(r:totalRevenue) = 0">
											&#160;
										</xsl:when>
										<xsl:otherwise>
											$<xsl:value-of select="format-number(r:totalRevenue,'#,##0.00')"/>
										</xsl:otherwise>
									</xsl:choose>
								</block>
							</table-cell>
							<table-cell>
								<block text-align="right">
									<xsl:choose>
										<xsl:when test="number(r:netRevenue) = 0">
											&#160;
										</xsl:when>
										<xsl:otherwise>
											$<xsl:value-of select="format-number(r:netRevenue,'#,##0.00')"/>
										</xsl:otherwise>
									</xsl:choose>
								</block>
							</table-cell>
							<table-cell>
								<block text-align="right">
									<xsl:choose>
										<xsl:when test="number(r:otherCharges) = 0">
											&#160;
										</xsl:when>
										<xsl:otherwise>
											$<xsl:value-of select="format-number(r:otherCharges,'#,##0.00')"/>
										</xsl:otherwise>
									</xsl:choose>
								</block>
							</table-cell>
						</table-row>
					</xsl:for-each>
					<table-row>
						<table-cell number-columns-spanned="2"><block padding-before="3mm">&#160;</block></table-cell>
						<table-cell><block padding-before="3mm" font-weight="bold" text-align="right">Totals:</block></table-cell>
						<table-cell>
							<block font-weight="bold" text-align="right" padding-before="3mm">
								$<xsl:value-of select="format-number($totalRevenueSum,'#,##0.00')" />
							</block>
						</table-cell>
						<table-cell>
							<block font-weight="bold" text-align="right" padding-before="3mm">
								$<xsl:value-of select="format-number($netRevenueSum,'#,##0.00')" />
							</block>
						</table-cell>
						<table-cell>
							<block font-weight="bold" text-align="right" padding-before="3mm">
								$<xsl:value-of select="format-number($otherChargeSum,'#,##0.00')" />
							</block>
						</table-cell>
					</table-row>
				</table-body>
			</table>
		<!--</table-and-caption>-->
		<block margin-left="13mm" margin-right="12mm" space-after="5mm">
			<block>
				<xsl:choose>
					<xsl:when test="$remittance >= 0">
						<xsl:text>Please find enclosed a CHECK in the sum of $</xsl:text>
						<xsl:value-of select="format-number($remittance,'#,##0.00')" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>
							This is an INVOICE __________, requesting payment to USA Technologies, Inc.
							in the sum of $
						</xsl:text>
						<xsl:value-of select="format-number(0-$remittance,'#,##0.00')" />
					</xsl:otherwise>
				</xsl:choose>
			</block>
			<block>This amount is the difference between the OTHER CHARGES and the NET REVENUE.</block>
		</block>
	</xsl:template>
	
	<xsl:template name="share">
		<xsl:param name="rows" />
		<block border-top-width=".1mm" border-top-color="black" border-top-style="solid">
			Revenue Sharing Explanation:
		</block>
		<xsl:for-each select="$rows">
			<xsl:sort select="r:terminalNumber" />
			<xsl:variable name="pos" select="position()" />
			<block margin-left="5mm">
				<xsl:choose>
					<xsl:when test="$rows[$pos]/r:terminalNumber = $rows[$pos - 1]/r:terminalNumber">
						<inline width="20mm">Further</inline>
					</xsl:when>
					<xsl:otherwise>
						<xsl:attribute name="padding-before">3mm</xsl:attribute>
						<inline width="20mm"><xsl:value-of select="r:terminalNumber" /></inline>
					</xsl:otherwise>
				</xsl:choose>
				: You receive <xsl:value-of select="r:customerPercentage" />% of
				<xsl:value-of select="r:transactionType" /> revenue.
			</block>
			<xsl:if test="number(r:customerFee) > 0">
				<block margin-left="5mm">
					<inline width="20mm">Further</inline>
					: You pay a fee of $<xsl:value-of select="format-number(r:customerFee,'#0.00')" />
					on <xsl:value-of select="r:transactionType" /> revenue.
				</block>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	
	<xsl:template name="summary">
		<block border-top-width=".1mm" border-top-color="black" border-top-style="solid" space-before="5mm">
			Remittance Report Summary:
		</block>
		<block margin-left="5mm" text-indent="5mm" space-before="5mm">
		   The proper reporting and payment of any local sales or use taxes if applicable is 
		   solely the responsibility of the operator. USA Technologies, Inc. as the owner of 
		   the proprietary software and technology has contracted with the operator to provide 
		   credit card transaction services to the operator only.
		</block>
		<block margin-left="5mm" text-indent="5mm" space-before="5mm">
		   The period of time inclusive of this report summarizes transactions based
		   on the date received at USA Technologies, Inc. computer center, irrespective of 
		   the date when the transaction originally occurred. Devices are programmed to remit
		   transaction information at periodic rates of typically one, two or three day intervals. 
		   A transaction occurring on the first of the month, which was recorded at the processing
		   center on the third of the month would be included on this report if the remittance
		   period included the third of the month but excluded the first of the month.
		</block>
		<block margin-left="5mm" text-indent="5mm" space-before="5mm">
		   We appreciate your business and want to better serve you. If you have questions or 
		   comments related to this summary or would like to learn more about what USA 
		   Technologies can offer you please contact customer service at 800-633-0340 or visit
		   our web site at WWW.USATECH.COM
		</block>
	</xsl:template>
	
	<xsl:template name="sumAmounts">
		<xsl:param name="nodes"/>
		<xsl:param name="sum"/>
		<xsl:choose>
			<xsl:when test="$nodes">
				<xsl:call-template name="sumAmounts">
					<xsl:with-param name="nodes" select="$nodes[position() &gt; 1]" />
					<xsl:with-param name="sum" select="$sum + number($nodes[1])"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$sum"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
