<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:convert="http://simple/xml/extensions/java/simple.bean.ConvertUtils"
    xmlns:a="http://simple/aggregating/1.0"
	xmlns:b="http://simple/bean/1.0"
	xmlns:x2="http://simple/translate/2.0"
	xmlns:r="http://simple/results/1.0"
	xmlns:cmn="http://exslt.org/common"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect x2 a b r cmn convert">
	<xsl:output method="xhtml" />
	<xsl:param name="context" />
		
	<xsl:template name="validateDateRangeScript">
	   <script type="text/javascript">
    var minDate = new Date("<xsl:value-of select="convert:formatObject(reflect:getProperty($context, 'minDate'), 'DATE:MM/dd/yyyy')"/>");
    function getDateLabel(el, def) {
        var labels = el.getLabels();
        var desc;
        if(labels)
            desc = String(labels.get("text"));
        else
            desc = String(def);
        if(desc.search(/date/i) &lt; 0)
            desc = desc + " Date"; 
        return desc;
    }
    function validateDateRange(beginDateInput, endDateInput) {
        var beginEl = $(beginDateInput);
        var beginDate = new Date(beginEl.getValue());
        if(isNaN(beginDate)) {
            alert("You entered an invalid " + getDateLabel(beginEl, "Begin Date") + ". Please correct.");
            return false;
        }
        if(beginDate.getTime() &lt; minDate.getTime()) {
            var desc = getDateLabel(beginEl, "Begin Date");
            alert("You selected a " + desc + " that is earlier than allowed. The " + desc + " will be adjusted to the earliest allowed");
            beginEl.setValue((1 + minDate.getMonth()) + "/" + minDate.getDate() + "/" + minDate.getFullYear());
            return false;
        }
        var endEl = $(endDateInput);
        var endDate = new Date(endEl.getValue());
        if(isNaN(endDate)) {
            alert("You entered an invalid " + getDateLabel(endEl, "End Date") + ". Please correct.");
            return false;
        }
        if(endDate.getTime() &lt; beginDate.getTime()) {
	        var desc = getDateLabel(endEl, "End Date");
            alert("You selected an " + desc + " that is earlier than the " + getDateLabel(beginEl, "Begin Date") + ". The " + desc + " will be adjusted");
            endEl.setValue(beginEl.getValue());
            return false;
        }
        return true;
    }
        </script>
    </xsl:template>
	<xsl:template name="rangeTypeSelect">
		<xsl:param name="extra-attributes"/>
		<xsl:variable name="current-value" select="normalize-space(reflect:getProperty($context, 'rangeType'))"/>
		<xsl:variable name="extra-attribute-set" select="cmn:node-set($extra-attributes)/*"/>
		<select name="rangeType" id="rangeType">
			<xsl:for-each select="$extra-attribute-set/*">
				<xsl:attribute name="{@name}"><xsl:value-of select="@value"/></xsl:attribute>
			</xsl:for-each>
			<option value="DAY"><xsl:if test="$current-value = 'DAY'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>Transaction Date - By Day</option>
			<option value="WEEK"><xsl:if test="$current-value = 'WEEK'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>Transaction Date - By Week</option>
			<option value="MONTH"><xsl:if test="$current-value = 'MONTH'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>Transaction Date - By Month</option>
			<option value="ALL"><xsl:if test="$current-value = 'ALL'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>Transaction Date - Entire Range</option>
			<option value="FILL"><xsl:if test="$current-value = 'FILL'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>Fill Date</option>
		</select>
	</xsl:template>
	
	<xsl:template name="dateNumberOptions">
		<xsl:param name="value"/>
		<xsl:param name="select-value" select="1"/>
		<xsl:param name="max-value"/>
		<option value="{$value}">
			<xsl:if test="$value = $select-value"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
			<xsl:value-of select="concat(substring('0000000000000000', 1, string-length($max-value) - string-length($value)), $value)"/>
		</option>
		<xsl:if test="$value &lt; $max-value">
			<xsl:call-template name="dateNumberOptions">
				<xsl:with-param name="value" select="$value + 1" />
				<xsl:with-param name="select-value" select="$select-value" />
				<xsl:with-param name="max-value" select="$max-value" />	 
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	
	<xsl:template name="hourOptions">
		<xsl:param name="hour" select="0"/>
		<xsl:param name="select-hour" select="0"/>
		<option value="{$hour}:00">
			<xsl:if test="$hour = $select-hour"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
			<xsl:choose>
				<xsl:when test="$hour = 0">12:00 am</xsl:when>
				<xsl:when test="$hour = 12">12:00 pm</xsl:when>
				<xsl:when test="$hour &lt; 12"><xsl:value-of select="$hour" />:00 am</xsl:when>
				<xsl:otherwise><xsl:value-of select="$hour - 12" />:00 pm</xsl:otherwise>
			</xsl:choose>
		</option>
		<xsl:if test="$hour &lt; 23">
			<xsl:call-template name="hourOptions">
				<xsl:with-param name="hour" select="$hour + 1" />
				<xsl:with-param name="select-hour" select="$select-hour" />
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	
	<xsl:template name="monthOptions">
		<xsl:param name="select-month" select="1"/>
		<xsl:for-each select="/a:base/b:calendar/b:monthNames/b:monthName[string-length() &gt; 0]">
    		<option value="{.}">
    			<xsl:if test="$select-month = position()"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
    			<xsl:value-of select="."/>
    		</option>		
    	</xsl:for-each>
	</xsl:template>
	
	<xsl:template name="groupBySelect">
		<xsl:param name="value" />
		<select name="sortBy" onchange='sortByChanged()'>
			<option value="0">-- None --</option>
			<xsl:for-each select="/a:base/a:groupBys/r:results/r:row">
    		<option value="{r:key}">
    			<xsl:if test="$value = r:key"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
    			<xsl:value-of select="r:label"/>
    		</option>		
    	</xsl:for-each>
		</select>	
	</xsl:template>
	
	<xsl:template name="valuesCheckboxes">
		<fieldset class="no-border-grouper">
		<xsl:param name="advanced" />
		<xsl:call-template name="valueCheckbox">
			<xsl:with-param name="label" select="'Sale Amount'"/>
			<xsl:with-param name="value" select="1"/>
			<xsl:with-param name="advanced" select="$advanced"/>
		</xsl:call-template>
		<xsl:call-template name="valueCheckbox">
			<xsl:with-param name="label" select="'# of Trans'"/>
			<xsl:with-param name="value" select="2"/>
			<xsl:with-param name="advanced" select="$advanced"/>
		</xsl:call-template>
		<xsl:call-template name="valueCheckbox">
			<xsl:with-param name="label" select="'# of Items'"/>
			<xsl:with-param name="value" select="3"/>
			<xsl:with-param name="advanced" select="$advanced"/>
		</xsl:call-template>
		
		<xsl:if test="$advanced">
			<xsl:call-template name="averageCheckbox">
				<xsl:with-param name="label" select="'Amt / Trans'"/>
				<xsl:with-param name="value" select="1"/>
			</xsl:call-template>
			<xsl:call-template name="averageCheckbox">
				<xsl:with-param name="label" select="'Items / Trans'"/>
				<xsl:with-param name="value" select="2"/>
			</xsl:call-template>
		</xsl:if>
		</fieldset>
	</xsl:template>
	
	<xsl:template name="valueCheckbox">
		<xsl:param name="label" />
		<xsl:param name="value" />
		<xsl:param name="advanced" />
		<span class="checkbox">
			<input type="checkbox" value="{$value}" name="showValues">
				<xsl:if test="contains(concat(',', translate(normalize-space(reflect:getProperty($context, 'showValues')), ' ', ''), ','), concat(',', $value, ','))"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
				<xsl:if test="$advanced"><xsl:attribute name="onclick">$("showPercents_<xsl:value-of select="$value" />").disabled = (!this.checked);</xsl:attribute></xsl:if>
			</input>
			<xsl:value-of select="$label" />	
		</span>
		<xsl:if test="$advanced">
			<span class="checkbox">
				<input type="checkbox" value="{$value}" name="showPercents" id="showPercents_{$value}">
					<xsl:if test="not(contains(concat(',', translate(normalize-space(reflect:getProperty($context, 'showValues')), ' ', ''), ','), concat(',', $value, ',')))"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>			
					<xsl:if test="contains(concat(',', translate(normalize-space(reflect:getProperty($context, 'showPercents')), ' ', ''), ','), concat(',', $value, ','))"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
				</input>% of <xsl:value-of select="$label" />
			</span>
		</xsl:if>
	</xsl:template>
	
	<xsl:template name="averageCheckbox">
		<xsl:param name="label" />
		<xsl:param name="value" />
		<span class="checkbox">
			<input type="checkbox" value="{$value}" name="showAverages">
				<xsl:if test="contains(concat(',', translate(normalize-space(reflect:getProperty($context, 'showAverages')), ' ', ''), ','), concat(',', $value, ','))"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
			</input>
			<xsl:value-of select="$label" />
		</span>
	</xsl:template>
	
	<xsl:template name="valuesRadios">
		<table class="sunkenTable" cellspacing="3">
			<tr>
			<xsl:call-template name="valueRadio">
				<xsl:with-param name="label" select="'Sale Amount'"/>
				<xsl:with-param name="value" select="1"/>
			</xsl:call-template>
			<xsl:call-template name="valueRadio">
				<xsl:with-param name="label" select="'# of Trans'"/>
				<xsl:with-param name="value" select="2"/>
			</xsl:call-template>
			<xsl:call-template name="valueRadio">
				<xsl:with-param name="label" select="'# of Items'"/>
				<xsl:with-param name="value" select="3"/>
			</xsl:call-template>
			</tr>
		</table>
	</xsl:template>
	
	<xsl:template name="valueRadio">
		<xsl:param name="label" />
		<xsl:param name="value" />
		<td>
			<input type="radio" value="{$value}" name="showValues">
				<xsl:if test="contains(concat(',', translate(normalize-space(reflect:getProperty($context, 'showValues')), ' ', ''), ','), concat(',', $value, ','))"><xsl:attribute name="checked">true</xsl:attribute></xsl:if>
			</input>
			<xsl:value-of select="$label" />&#160;
		</td>
	</xsl:template>
	
</xsl:stylesheet>

