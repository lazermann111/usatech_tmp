<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:b="http://simple/bean/1.0"
	xmlns:x2="http://simple/translate/2.0"
	xmlns:r="http://simple/results/1.0"
	xmlns:p="http://simple/xml/parameters/1.0"
	xmlns:cmn="http://exslt.org/common"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect x2 a b r p cmn">
	<xsl:import href="../general/app-base.xsl" />
	<xsl:import href="../selection/select-terminal-customers.xsl"/>
	<xsl:import href="parameters_base.xsl"/>
	<xsl:output method="xhtml" />
	<xsl:param name="context" />
	
	<xsl:template name="subtitle">
		Diagnostic Parameters
	</xsl:template>
	
	<xsl:template name="extra-scripts">
		<script type="text/javascript">
	window.addEvent("domready", function() {
		new Form.Validator.Inline.Mask(document.criteria);
	});
    
	function updateDateRange() {
    	$("beginDate").value = ($("beginMonth").getValue() + " " + $("beginDay").getValue() + ", " + $("beginYear").getValue() + " " + $("beginTime").getValue());
        $("endDate").value = ($("endMonth").getValue() + " " + $("endDay").getValue() + ", " + $("endYear").getValue() + " " + $("endTime").getValue());
        $("dateFormat").value = ("MMMM dd, yyyy h:mma");
        return true;
    }
		</script>
    	<xsl:call-template name="extra-scripts-select-terminal"/>
    	<script type="text/javascript">
			<xsl:attribute name="src"><xsl:call-template name="uri"><xsl:with-param name="uri">selection/report-selection-scripts.js</xsl:with-param></xsl:call-template></xsl:attribute>
		</script>
	</xsl:template>

	<xsl:template name="contents">
	<span class="caption">Diagnostics</span>
	<div class="spacer5"></div>
	<form id="criteriaForm" name="criteria" method="post" action="diagnostic.i" onsubmit="return validateTerminals()&amp;&amp;updateDateRange();">
	<input type="hidden" name="dateFormat" id="dateFormat"/>
	<input type="hidden" name="params.beginDate" id="beginDate"/>
	<input type="hidden" name="params.endDate" id="endDate"/>
	<input type="hidden" name="referrer" value="diagnostic_parameters.i"/>
    <input type="hidden" name="saveAction" value="save_report_params_prompt.i"/>
	<table><tr><td>
	<div class="selectSection" id="selectDevices">
		<div class="sectionTitle"><x2:translate key="selection-terminal-list" is-markup="true">Please search for devices or expand by clicking on the name or + sign<br/>and select the company(ies), region(s), and/or device(s):</x2:translate></div>
		<div class="sectionRow">
		<xsl:call-template name="contents-select-terminal"/>
		</div>
	</div>
	</td></tr>
	<tr><td>
	<div class="selectSection" id="selectDevices">
		<div class="sectionTitle"><x2:translate key="selection-alert-type-list" is-markup="true">Please select the alert types:</x2:translate></div>
		<div class="sectionRow">
			<xsl:variable name="alert-type" select="concat(',', normalize-space(reflect:getProperty($context, 'params.alertType')), ',')"/>
    		<fieldset class="no-border-grouper" data-validator-properties="{'{'}label:'alert type'}" data-validators="validate-reqchk-byname" name="params.alertType">				
				<xsl:for-each select="/a:base/a:alertTypeResults/r:results/r:group">
				    <fieldset class="checkbox-grouping">
				        <legend><xsl:value-of select="r:alertSourceDesc"/>
				            <xsl:if test="r:row[1]/r:alertSource = 'DEVICE'"><span class="only-edge-note">(Edge/G9 Devices Only)</span></xsl:if>
				        </legend>
					<xsl:for-each select="r:row">
						<label class="checkbox">
							<input type="checkbox" value="{r:alertTypeId}" name="params.alertType">
								<xsl:if test="$alert-type = ',,' or contains($alert-type, concat(',', r:alertTypeId, ','))"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
	    					</input>
	    					<xsl:value-of select="r:alertTypeName"/>
	    				</label>
						<input type="hidden" name="possibleAlertType" value="{r:alertTypeId}"/>
					</xsl:for-each>
					</fieldset>
				</xsl:for-each>
			</fieldset>
			<div class="center">
				<input type="button" onclick="toggleAll(this, 'params.alertType')">
					<xsl:attribute name="value">
						<xsl:choose><xsl:when test="$alert-type = ',,'">Deselect All</xsl:when><xsl:otherwise>Select All</xsl:otherwise></xsl:choose>
					</xsl:attribute>
				</input>
			</div>
		</div>
	</div>
	</td></tr>	
	<tr><td>
	<div class="selectSection" id="selectRangeAndType">
		<div class="sectionTitle"><x2:translate key="selection-date-range" is-markup="true">Now select the date range:</x2:translate></div>
		<div class="sectionRow">
<table class="paramTable" cellSpacing="3" cellPadding="3">
  <tr>
  	<td class="paramCell">Beginning</td>
  	<td class="paramCell">
    	<select id="beginMonth">
	    	<xsl:call-template name="monthOptions">
	    		<xsl:with-param name="select-month" select="/a:base/b:calendar/b:beginMonth"/>
	    	</xsl:call-template>
        </select>
  	</td>
  	<td class="paramCell">
  		<select id="beginDay">
        	<xsl:call-template name="dateNumberOptions">
				<xsl:with-param name="value" select="1" />
				<xsl:with-param name="select-value" select="/a:base/b:calendar/b:beginDay" />
				<xsl:with-param name="max-value" select="31" />	 
			</xsl:call-template>
        </select>
    </td>
    <td class="paramCell">
    	<select id="beginYear">
    		<xsl:call-template name="dateNumberOptions">
				<xsl:with-param name="value" select="/a:base/b:calendar/b:minYear" />
				<xsl:with-param name="select-value" select="/a:base/b:calendar/b:beginYear" />
				<xsl:with-param name="max-value" select="/a:base/b:calendar/b:maxYear" />	 
			</xsl:call-template>
        </select>
    </td>
    <td class="paramCell">
    	<select id="beginTime">
    		<xsl:call-template name="hourOptions">
    			<xsl:with-param name="select-hour" select="/a:base/b:calendar/b:beginHour" />
			</xsl:call-template>
        </select>
  	</td>
  </tr>
  <tr>
    <td class="paramCell">Ending</td>
    <td class="paramCell">
    	<select id="endMonth">
        	<xsl:call-template name="monthOptions">
	    		<xsl:with-param name="select-month" select="/a:base/b:calendar/b:endMonth"/>
	    	</xsl:call-template>
        </select>
  	</td>
  <td class="paramCell">
  		<select id="endDay">
        	<xsl:call-template name="dateNumberOptions">
				<xsl:with-param name="value" select="1" />
				<xsl:with-param name="select-value" select="/a:base/b:calendar/b:endDay" />
				<xsl:with-param name="max-value" select="31" />	 
			</xsl:call-template>
        </select>
    </td>
    <td class="paramCell">
    	<select id="endYear">
    		<xsl:call-template name="dateNumberOptions">
				<xsl:with-param name="value" select="/a:base/b:calendar/b:minYear" />
				<xsl:with-param name="select-value" select="/a:base/b:calendar/b:endYear" />
				<xsl:with-param name="max-value" select="/a:base/b:calendar/b:maxYear" />	 
			</xsl:call-template>
        </select>
    </td>
    <td class="paramCell">
    	<select id="endTime">
    		<xsl:call-template name="hourOptions">
    			<xsl:with-param name="select-hour" select="/a:base/b:calendar/b:endHour" />
			</xsl:call-template>
        </select>
  	</td>
  </tr>
  </table>
</div></div>
</td></tr>
<tr><td><div align="center" style="float: center"><input type="submit" value="Run Report" name="Submit"/></div></td></tr>
</table>
</form>
	</xsl:template>
</xsl:stylesheet>

