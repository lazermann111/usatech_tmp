<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils" 
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:r="http://simple/results/1.0" 
	xmlns="http://www.w3.org/1999/XSL/Format"
	exclude-result-prefixes="reflect r a">
	<xsl:output method="xml" omit-xml-declaration="yes"/>
	
	<xsl:param name="context" />
	<xsl:variable name="title" select="reflect:getProperty($context,'title')"/>
	<xsl:template match="/a:base">
		<root language="en" hyphenate="true" country="US">
			<layout-master-set>
			   <simple-page-master master-name="master-pages-for-all" 
			   		page-height="11in" page-width="8.5in"
			   		margin-top="0.12in" margin-right="0.12in" margin-bottom="0.12in" margin-left="0.12in">
			   		<!-- FOP 0.20.5 does not seem to obey margins here -->
					<region-before extent="0in"/>
			       	<region-body />
					<region-after extent="0in"/>
			   </simple-page-master>

			   <page-sequence-master master-name="sequence-of-pages">
				    <repeatable-page-master-reference master-reference="master-pages-for-all" />
			   </page-sequence-master>
		  	</layout-master-set>

 		  	<page-sequence master-reference="sequence-of-pages">
 		  		<title><xsl:call-template name="subtitle"/></title>
			    <static-content flow-name="xsl-region-before"/>
				<static-content flow-name="xsl-region-after"/>
			    <!--  <page-sequence master-reference="document" initial-page-number="1" format="1">-->
				<flow flow-name="xsl-region-body">
					<block unicode-bidi="embed">
						<xsl:call-template name="content"/>
					</block>
				</flow>
			</page-sequence>
		</root>
	</xsl:template>
	
	<xsl:template name="subtitle">
		<xsl:choose>
			<xsl:when test="string-length(normalize-space($title)) > 0">
				<xsl:value-of select="$title"/>
			</xsl:when>
			<xsl:otherwise>
				Receipt
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name="header">
		<block text-align="center" font-weight="bold" space-after="1mm" space-before="1mm"><xsl:call-template name="subtitle"/></block>
	</xsl:template>
	
	<xsl:template name="footer">
		<block space-before="5mm" text-align="center" font-size="10pt">
			<block>USA Technologies, Inc.</block>
			<block>100 Deerfield Lane, Suite 300</block>
			<block>Malvern, PA 19355</block>
			<block space-before="3mm">Customer Service: 1-888-561-4748</block>
		</block>
	</xsl:template>
	
	<xsl:template name="content">
		<xsl:for-each select="/a:base/a:receipts/r:results/r:group">
			<table border="0.14mm solid rgb(210,210,210)" space-after="3mm">
				<table-column column-width="0.05in" text-align="left"/>
				<table-column column-width="3.9in" text-align="left"/>
				<table-column column-width="0.05in" text-align="left"/>
				<table-body>
					<table-row keep-together="always">
						<table-cell/>
						<table-cell>
							<table text-align="left">
								<table-column column-width="0.9in" text-align="left"/>
								<table-column column-width="3.0in" text-align="left"/>
								<table-header>
									<table-row>
										<table-cell number-columns-spanned="2"><xsl:call-template name="header"/></table-cell>
									</table-row>
								</table-header>
								<table-body>
									<table-row>
										<table-cell><block>Location:</block></table-cell>
										<table-cell><block><xsl:value-of select=".//r:locationName[1]"/></block></table-cell>
									</table-row>
									<table-row>
										<table-cell><block>Tran ID#:</block></table-cell>
										<table-cell><block><xsl:value-of select=".//r:receiptTransNum[1]"/></block></table-cell>
									</table-row>
									<table-row>
										<table-cell><block>Date:</block></table-cell>
										<table-cell><block><xsl:value-of select=".//r:tranDate[1]"/></block></table-cell>
									</table-row>
									<table-row>
										<table-cell><block>Time:</block></table-cell>
										<table-cell><block><xsl:value-of select=".//r:tranTime[1]"/></block></table-cell>
									</table-row>
									<table-row>
										<table-cell><block>Card:</block></table-cell>
										<table-cell><block><xsl:value-of select=".//r:maskedCardNum[1]"/></block></table-cell>
									</table-row>
									<table-row>
										<table-cell number-columns-spanned="2">
											<xsl:variable name="use-sub-amounts" select="round(1000 * sum(.//r:row/r:lineItemAmount/@r:value)) = round(1000 * number(.//r:totalAmount[1]/@r:value))"/>
											<table space-before="3mm">
												<table-column column-width="1.2in" text-align="left"/>
												<table-column column-width="1.4in" text-align="left"/>					
												<table-column column-width="0.5in" text-align="right"/>					
												<table-column column-width="0.8in" text-align="right"/>					
												<table-header>
													<table-row>
														<table-cell border-bottom="0.14mm solid rgb(0,0,0)"><block>Equipment</block></table-cell>
														<table-cell border-bottom="0.14mm solid rgb(0,0,0)"><block>Item</block></table-cell>
														<table-cell border-bottom="0.14mm solid rgb(0,0,0)" text-align="right"><block>Qty</block></table-cell>
														<table-cell border-bottom="0.14mm solid rgb(0,0,0)" text-align="right"><block><xsl:if test="$use-sub-amounts">Subtotal</xsl:if></block></table-cell>
													</table-row>
												</table-header>
												<table-body>
													<xsl:for-each select=".//r:row">
														<table-row font-size="10pt">
															<table-cell><block><xsl:value-of select="r:hostTypeName"/></block></table-cell>
															<table-cell><block><xsl:value-of select="r:lineItemDesc"/></block></table-cell>
															<table-cell text-align="right"><block><xsl:value-of select="r:lineItemQuantity"/></block></table-cell>
															<table-cell text-align="right">
																<block>
																	<xsl:if test="$use-sub-amounts"><xsl:value-of select="r:lineItemAmount"/></xsl:if>
																</block>
															</table-cell>
														</table-row>
													</xsl:for-each>
													<table-row>
														<table-cell number-columns-spanned="2" border-top="0.14mm solid rgb(0,0,0)"><block>Total:</block></table-cell>
														<table-cell border-top="0.14mm solid rgb(0,0,0)" text-align="right"><block><xsl:value-of select="sum(.//r:row/r:lineItemQuantity/@r:value)"/></block></table-cell>
														<table-cell border-top="0.14mm solid rgb(0,0,0)" text-align="right"><block><xsl:value-of select=".//r:totalAmount[1]"/></block></table-cell>
													</table-row>								
												</table-body>
											</table>								
										</table-cell>
									</table-row>
									<table-row>
										<table-cell number-columns-spanned="2"><xsl:call-template name="footer"/></table-cell>
									</table-row>
								</table-body>						
							</table>						
						</table-cell>
						<table-cell/>						
					</table-row>
				</table-body>
			</table>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
