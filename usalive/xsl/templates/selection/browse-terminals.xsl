<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:r="http://simple/results/1.0"
	xmlns:x="http://simple/translate/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="r a x">
	<xsl:import href="../general/app-base.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:template name="subtitle"><x:translate>Device Selection</x:translate></xsl:template>
	<xsl:template name="extra-scripts">
		<script type="text/javascript" src="./selection/report-selection-scripts.js"/>
	</xsl:template>
	<xsl:template name="contents">
		<xsl:choose>
			<xsl:when test="count(/a:base/a:selectItems/r:results/r:row) &gt; 0">
				<form id="browseForm" name="browseForm" action="selection_terminals.i"
					target="terminalSelectFrame" onsubmit="terminalsSelected(this);">
					<table style="width: 100%">
					<tr><td><div class="selection-scroll-box">
						<table class="results">
							<caption>Select Devices</caption>
							<thead>
								<tr class="headerRow">
									<th></th>
									<th><x:translate>Device</x:translate></th>
									<th><x:translate>Location</x:translate></th>
									<th><x:translate>Customer</x:translate></th>
									<th><x:translate>Device Type</x:translate></th>
								</tr>
							</thead>
							<tbody>
								<xsl:for-each select="/a:base/a:selectItems/r:results/r:row">
									<tr>
										<xsl:attribute name="class">
											<xsl:choose>
												<xsl:when test="(position() mod 2) = 0">evenRow</xsl:when>
												<xsl:otherwise>oddRow</xsl:otherwise>
											</xsl:choose>
										</xsl:attribute>
										<td>
											<input type="checkbox" name="terminalId">
												<xsl:attribute name="value">
													<xsl:value-of select="r:terminalId" />
												</xsl:attribute>
											</input>
										</td>
										<td>
											<xsl:value-of select="r:deviceSerialNum" />
										</td>
										<td><xsl:value-of select="r:locationName" /></td>
										<td><xsl:value-of select="r:customerName" /></td>
										<td><xsl:value-of select="r:deviceType" /></td>
									</tr>
								</xsl:for-each>
							</tbody>
						</table>
					</div>
					</td></tr>
					<tr><td>
					<div class="browseButtons">
						<input type="submit"><xsl:attribute name="value"><x:translate>&lt;&lt; Add Selected</x:translate></xsl:attribute></input>
						<input type="hidden" name="unframed" value="true" />
					</div>
					</td></tr>
					</table>
				</form>
			</xsl:when>
			<xsl:otherwise>
				<div><x:translate>The search specified did not match any devices.</x:translate></div>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>