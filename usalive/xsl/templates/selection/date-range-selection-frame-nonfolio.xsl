<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:x="http://simple/translate/1.0"
	xmlns:p="http://simple/xml/parameters/1.0"
	xmlns:r="http://simple/results/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect a r x p">
	<xsl:import href="search-base.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:param name="context" />

    <xsl:variable name="startParamNameValue" select="normalize-space(reflect:getProperty($context,'startParamName'))"/>
    <xsl:variable name="startParamName">
        <xsl:choose>
            <xsl:when test="string-length($startParamNameValue) &gt; 0"><xsl:value-of select="$startParamNameValue"/></xsl:when>
            <xsl:otherwise>beginDate</xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <xsl:variable name="endParamNameValue" select="normalize-space(reflect:getProperty($context,'endParamName'))"/>
    <xsl:variable name="endParamName">
        <xsl:choose>
            <xsl:when test="string-length($endParamNameValue) &gt; 0"><xsl:value-of select="$endParamNameValue"/></xsl:when>
            <xsl:otherwise>endDate</xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
       
	<xsl:template name="subtitle"><xsl:value-of select="reflect:getProperty($context,'reportName')"/> - Select Date Range</xsl:template>
    
    <xsl:variable name="reportTitle" select="normalize-space(reflect:getProperty($context,'reportTitle'))"/>
             
	<xsl:template name="contents">
	    <div class="selectionCriteria">
			<xsl:choose>
				<xsl:when test="string-length(reflect:hasProperty($context,'basicReportId')) &gt; 0">
					<table class="selectBody">
						<caption><xsl:value-of select="reflect:getProperty($context,'reportName')"/></caption>
						<tr>
							<td>
								<form id="searchForm" name="searchForm" onsubmit="return validateDates(this)">
								    <xsl:variable name="user-agent" select="normalize-space(reflect:getProperty($context, 'header:user-agent'))"/>
								    <xsl:attribute name="action"><xsl:value-of select="reflect:getProperty($context,'actionUrl')"/></xsl:attribute>
                                    <input type="hidden" name="unframed" value="false"/>
									<xsl:call-template name="parameters-form">
										<xsl:with-param name="overrides">
											<p:ignore name="{$startParamName}"/>
											<p:ignore name="{$endParamName}"/>
											<p:ignore name="unframed"/>
											<p:override name="promptForParams" value="false"/>
											<xsl:if test="string-length($reportTitle) &gt; 0">
											<p:override name="reportTitle" value="{concat($reportTitle, ' - {b} to {e}')}"/>
                                            </xsl:if>
										</xsl:with-param>
									</xsl:call-template>
	
									<xsl:call-template name="date-range">
										<xsl:with-param name="startParamName" select="string($startParamName)" />
										<xsl:with-param name="endParamName" select="string($endParamName)" />
									    <xsl:with-param name="radioParamName" select="'radio'"/>
									</xsl:call-template>
									<div class="center">
										<input type="submit" value="Run Report" />
									</div>
								</form>	
							</td>
						</tr>
					</table>
				</xsl:when>
				<xsl:otherwise>
					<div><x:translate>Invalid parameters. This page requires 'basicReportId'.</x:translate></div>
				</xsl:otherwise>
			</xsl:choose>
	 	</div>
	 	<hr/>
		<div id="date-range-selection-target">
		</div>
	</xsl:template>
</xsl:stylesheet>
