<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:x="http://simple/translate/1.0"
	xmlns:r="http://simple/results/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect a r x">
	<xsl:import href="search-base.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:param name="context" />

	<xsl:template name="subtitle">Report Selection</xsl:template>
	<xsl:template name="contents">
		<xsl:choose>
			<xsl:when test="string-length(reflect:hasProperty($context,'folioId')) &gt; 0">
				<table class="selectBody" style="width: 335px;">
					<caption><xsl:value-of select="a:base/a:folioName/r:results/r:row[1]/r:folioName"/></caption>
					<tr>
						<td>
							<form id="searchForm" name="searchForm" action="run_report_async.i"
								onsubmit="return validateDates(this)">
								<input type="hidden" name="folioId" value="{reflect:getProperty($context,'folioId')}" />
								<input type="hidden" name="promptForParams" value="false" />
								<input type="hidden" name="params.AllTerminals" value="1" />
								<input type="hidden" name="params.DeviceTypeId" value="0,1,2,3,4,5,6,7,8,9,10,11,12,13,14" />
								<input type="hidden" name="outputType" value="{reflect:getProperty($context,'outputType')}" />
								<xsl:call-template name="date-range">
									<xsl:with-param name="startParamName" select="'params.StartDate'" />
									<xsl:with-param name="endParamName" select="'params.EndDate'" />
								</xsl:call-template>
								<div style="text-align: center; clear: both;">
									<input type="submit" value="Run Report" />
								</div>
							</form>
						</td>
					</tr>
				</table>
			</xsl:when>
			<xsl:otherwise>
				<div><x:translate>Invalid parameters. This page requires 'folioId'.</x:translate></div>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>