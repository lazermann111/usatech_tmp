<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:x="http://simple/translate/1.0"
	xmlns:p="http://simple/xml/parameters/1.0"
	xmlns:r="http://simple/results/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect a r x p">
	<xsl:import href="search-base.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:param name="context" />

    <xsl:variable name="startParamNameValue" select="normalize-space(reflect:getProperty($context,'startParamName'))"/>
    <xsl:variable name="startParamName">
        <xsl:choose>
            <xsl:when test="string-length($startParamNameValue) &gt; 0"><xsl:value-of select="$startParamNameValue"/></xsl:when>
            <xsl:otherwise>params.StartDate</xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <xsl:variable name="endParamNameValue" select="normalize-space(reflect:getProperty($context,'endParamName'))"/>
    <xsl:variable name="endParamName">
        <xsl:choose>
            <xsl:when test="string-length($endParamNameValue) &gt; 0"><xsl:value-of select="$endParamNameValue"/></xsl:when>
            <xsl:otherwise>params.EndDate</xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
       
	<xsl:template name="subtitle"><xsl:value-of select="a:base/a:folioName/r:results/r:row[1]/r:folioName"/> - Select Date Range</xsl:template>
             
	<xsl:template name="contents">
	    <div class="selectionCriteria">
			<xsl:choose>
				<xsl:when test="string-length(reflect:hasProperty($context,'folioId')) &gt; 0">
					<table class="selectBody">
						<caption><xsl:value-of select="a:base/a:folioName/r:results/r:row[1]/r:folioName"/></caption>
						<tr>
							<td>
								<form id="searchForm" name="searchForm" onsubmit="return validateDates(this)">
								    <xsl:variable name="user-agent" select="normalize-space(reflect:getProperty($context, 'header:user-agent'))"/>
							        <xsl:attribute name="action">run_report_async.i</xsl:attribute>
                                    <input type="hidden" name="unframed" value="false"/>
									<xsl:call-template name="parameters-form">
										<xsl:with-param name="overrides">
											<p:ignore name="{$startParamName}"/>
											<p:ignore name="{$endParamName}"/>
											<p:ignore name="unframed"/>
											<p:override name="promptForParams" value="false"/>
										</xsl:with-param>
									</xsl:call-template>
	
									<xsl:call-template name="date-range">
										<xsl:with-param name="startParamName" select="string($startParamName)" />
										<xsl:with-param name="endParamName" select="string($endParamName)" />
									    <xsl:with-param name="radioParamName" select="'radio'"/>
									</xsl:call-template>
	
									<div class="center">
										<input type="submit" value="Run Report" />
									</div>
								</form>	
							</td>
						</tr>
					</table>
				</xsl:when>
				<xsl:otherwise>
					<div><x:translate>Invalid parameters. This page requires 'folioId'.</x:translate></div>
				</xsl:otherwise>
			</xsl:choose>
	 	</div>
	 	<hr/>
		<div id="date-range-selection-target">
	      <xsl:if test="normalize-space(/a:base/a:parameters/p:parameters/p:parameter[@p:name='promptForParams']/@p:value) = 'false'">
	          <xsl:attribute name="src">run_report_async.i?<xsl:call-template name="parameters-query">
                      <xsl:with-param name="overrides">
                          <p:override name="unframed" value="false"/>
                          <p:override name="promptForParams" value="false"/>
                      </xsl:with-param>
                  </xsl:call-template>
	          </xsl:attribute>
	      </xsl:if>
		</div>
	</xsl:template>
</xsl:stylesheet>
