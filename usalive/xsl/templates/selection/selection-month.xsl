<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:x="http://simple/translate/1.0"
	xmlns:r="http://simple/results/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect a r x">
	<xsl:import href="search-base.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:param name="context" />

	<xsl:template name="subtitle">Report Selection</xsl:template>
	<xsl:template name="contents">
		<script type="text/javascript">
				window.onload=function(){
					var today=new Date();
					var previousMonth=today.getMonth()-1;
					if(previousMonth &lt; 0){
						previousMonth=11;
					}
					$('year').value=today.getFullYear();
				};
			</script>
		<xsl:choose>
			<xsl:when test="string-length(reflect:hasProperty($context,'basicReportId')) &gt; 0">
				<table class="selectBody" style="width: 335px;">
					<caption>Report: <xsl:value-of select="reflect:getProperty($context,'reportName')"/></caption>
					<tr>
						<td>
							<form id="searchForm" name="searchForm" action="run_basic_report_async.i"
								onsubmit="return validateMonth(this)">
								<input type="hidden" name="basicReportId" value="{reflect:getProperty($context,'basicReportId')}" />
								Month:<select name="month">
									<option value="0">January</option>
									<option value="1">February</option>
									<option value="2">March</option>
									<option value="3">April</option>
									<option value="4">May</option>
									<option value="5">June</option>
									<option value="6">July</option>
									<option value="7">August</option>
									<option value="8">September</option>
									<option value="9">October</option>
									<option value="10">November</option>
									<option value="11">December</option>
									</select>
								<input id="year" type="text" name="year" size="4" maxlength="4"/>
								<div style="text-align: center; clear: both;">
									<input type="submit" value="Run Report" />
								</div>
							</form>
						</td>
					</tr>
				</table>
			</xsl:when>
			<xsl:otherwise>
				<div><x:translate>Invalid parameters. This page requires 'basicReportId'.</x:translate></div>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>