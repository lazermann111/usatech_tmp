<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:r="http://simple/results/1.0"
	xmlns:p="http://simple/xml/parameters/1.0"
	xmlns:b="http://simple/bean/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="a b r p reflect">
	<xsl:output method="xhtml"/>
	
	<xsl:param name="context" />

	<xsl:template match="/">
		<xsl:variable name="customerId" select="reflect:getProperty($context, 'customerId')" />
		<ul class="regions tree">
			<xsl:for-each select="a:base/a:regionResults/r:results/r:row[r:subRegionFlag='N']">
                <xsl:call-template name="region-line"/>
            </xsl:for-each>
		</ul>
		<script type="text/javascript" defer="true">
			window.addEvent('domready', function() {
			<xsl:for-each select="/a:base/b:selectedItems/b:entry[b:key = $customerId]/b:value/b:entry">
				<xsl:if test="count(b:value/b:value-item) &gt; 0">
				toggleRegionTree($("terminals_of_<xsl:value-of select="b:key" />").getPrevious(), <xsl:value-of select="substring-before(b:key, '_')" />, <xsl:value-of select="substring-after(b:key, '_')" />, "<!--
				  --><xsl:for-each select="b:value/b:value-item"><xsl:value-of select="." />,</xsl:for-each><!--
				  -->");
					</xsl:if>				
			</xsl:for-each>
			});
		</script>
	</xsl:template>
	
	<xsl:template name="region-line">
        <li class="closedTreeNode" treelevel="customerRegion">
            <div class="parentTreeDiv">
            <a class="treeImage" onclick="toggleRegionTree(this, {r:customerId}, {r:regionId})">&#160;</a>
            <input id="c-{r:customerId}_{r:regionId}" type="checkbox" name="customerId_regionId" value="{r:customerId}_{r:regionId}" onclick="regionSelectionChanged(this)" >
                <xsl:if test="string(reflect:getProperty($context, concat('selectedItems.', r:customerId, '.', r:customerId, '_', r:regionId))) = '[]'">
                    <xsl:attribute name="checked">checked</xsl:attribute>
                </xsl:if>
            </input>
            <a class="treeLabel" onclick="toggleRegionTree(this, {r:customerId}, {r:regionId})"><xsl:value-of select="r:regionName"/></a>
            <xsl:if test="r:childCount &gt; 0"><div class="childTreeDiv" id="terminals_of_{r:customerId}_{r:regionId}">Loading...</div></xsl:if>
            <xsl:variable name="parentRegionId" select="r:regionId"/>
            <xsl:if test="count(following-sibling::*[r:parentRegionId = $parentRegionId]) &gt; 0">
            <ul class="childTreeDiv subregions" id="subregions_of_{r:customerId}_{r:regionId}">
               <xsl:for-each select="following-sibling::*[r:parentRegionId = $parentRegionId]">
                   <xsl:call-template name="region-line"/>
               </xsl:for-each>
            </ul>
            </xsl:if>
            </div>
        </li>
    </xsl:template>
</xsl:stylesheet>
