<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:r="http://simple/results/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="r a">
	<xsl:import href="../general/app-base.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:template name="subtitle">Report Selection</xsl:template>

	<xsl:template name="contents">
		<xsl:choose>
			<xsl:when test="count(/a:base/a:folios/r:results/r:row) &gt; 0">
				<div class="selectSection">
					<div class="sectionTitle">Select Report to Run</div>
					<div>
						<ul>
							<xsl:for-each select="/a:base/a:folios/r:results/r:row">
								<li>
									<a>
										<xsl:attribute name="href">
											<xsl:value-of select="r:url" />
										</xsl:attribute>
										<xsl:value-of select="r:displayName" />
									</a>
								</li>
							</xsl:for-each>
						</ul>
					</div>
				</div>
			</xsl:when>
			<xsl:otherwise>
				<div>There are no available reports.</div>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>