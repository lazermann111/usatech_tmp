<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect">
	<xsl:import href="../general/app-base.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:param name="context" />

	<xsl:template name="subtitle">Report Selection</xsl:template>
	<xsl:template name="contents">
		<xsl:choose>
			<xsl:when test="(string-length(reflect:getProperty($context,'searchAction')) > 0) and (string-length(reflect:hasProperty($context,'folioId')) > 0)">
				<table class="selectBody">
					<caption>Report Selection</caption>
					<tr>
						<td>
							<iframe id="search_frame" name="search_frame" class="searchFrame" frameborder="0"
								src="{reflect:getProperty($context,'searchAction')}.i?unframed=true&amp;folioId={reflect:getProperty($context,'folioId')}&amp;{reflect:getProperty($context,'defaultParamString')}">
							</iframe>
						</td>
						<td>
							<iframe id="select_frame" name="select_frame" class="selectFrame" frameborder="0" src="common/blank.html?unframed=true">
							</iframe>
						</td>
					</tr>
				</table>
			</xsl:when>
			<xsl:otherwise>
				<div>Invalid parameters. This page requires 'searchAction' and 'folioId'.</div>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>