<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:r="http://simple/results/1.0"
	xmlns:x2="http://simple/translate/2.0"
	xmlns:b="http://simple/bean/1.0"
	xmlns:p="http://simple/xml/parameters/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="a r x2 b p reflect">
	<xsl:import href="../general/app-base.xsl"/>
	<xsl:import href="select-terminal-customers-tree.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:param name="context" />

	<xsl:template name="subtitle"><x2:translate key="terminal-list-title">Device List</x2:translate></xsl:template>

	<xsl:template name="extra-scripts">
		<xsl:call-template name="extra-scripts-select-terminal"/>
	</xsl:template>
	
	<xsl:template name="extra-scripts-select-terminal">
		<script type="text/javascript" defer="defer">
			var checkboxTristateImages = (
				Browser.safari || Browser.chrome ? {normal:"url(images/form/checkbox-tristate-safari.png)", hover:"url(images/form/checkbox-tristate-safari.png)", down:"url(images/form/checkbox-tristate-safari.png)"}
							  : {normal:"url(images/form/checkbox-tristate.png)", hover:"url(images/form/checkbox-tristate-hover.png)", down:"url(images/form/checkbox-tristate-down.png)"});
		    function toggleAllCustomer(a, expandOnly){
				var liEl=$(a).getParent();
			
				if(liEl.hasClass("closedTreeNode")) {
					if(!expandOnly){
			    		if(!liEl.childrenLoaded) {
							new Request.HTML({
								url : 'select_terminal_customers_getall.i', 
								method : "post",
								async : true,
								update : $('customerTreeAll'),
								evalScripts : true,
								link : "cancel",
								data :{fragment:true},
								onComplete : function() {liEl.childrenLoaded = true; }
							}).send();
			    		}
			    		if($('selectAllCheckbox').disabled){
		   					$('selectAllCheckbox').disabled=false;
		   					$('selectAllCheckbox').style.display = 'inline';
		   				}
		    		}
		    		liEl.addClass("openTreeNode");
		    		liEl.removeClass("closedTreeNode");
		    	} else {
		    		liEl.addClass("closedTreeNode");
		    		liEl.removeClass("openTreeNode");
		   		}		   		   		
			}
		    function toggleCustomerTree(a, customerId, regions, terminals) {
		    	var userData = {customerId: customerId};
		    	if(regions)
		    		userData.customerId_regionId = regions;
		    	if(terminals)
		    		userData.terminals = terminals;
		    	toggleTree(a,"regions_of_"+customerId, "select_terminal_regions.i", userData);
		    }
		    function toggleRegionTree(a, customerId, regionId, terminals) {
		    	var userData = {customerId: customerId, regionId: regionId};
		    	if(terminals)
		    		userData.terminals = terminals;
		    	toggleTree(a,"terminals_of_"+customerId+"_"+regionId, "select_terminal_children.i", userData);
		    }
		    function toggleTree(a, updateId, url, userData) {
		    	a = $(a);
		    	var li = a.getParent().getParent();
		    	if(li.hasClass("closedTreeNode")) {
		    		if(!li.childrenLoaded) {
		    			var updateDiv = $(updateId);
                        if(updateDiv) {
	                        var checkbox = a.getParent().getElement("input[type=checkbox]");
			    			var selectAll = checkbox.checked;
			    			var data = {profileId:"<xsl:value-of select="a:base/b:userId"/>",fragment:true};
							if(userData)
								Object.append(data, userData);
							new Request.HTML({
								url : url, 
								method : "post",
								async : true,
								update : updateDiv,
								evalScripts : true,
								link : "cancel",
								onComplete : function() {
									li.childrenLoaded = true;
									if(selectAll) {
										updateDiv.getElements("input[type=checkbox]").each(function(el) {
								        	el.checked = true;
								        });
										//setMixedCheckbox(checkbox, true);
									}
								},
								data : data
							}).send();
						}
		    		}
		    		li.addClass("openTreeNode");
		    		li.removeClass("closedTreeNode");
		    	} else {
		    		li.addClass("closedTreeNode");
		    		li.removeClass("openTreeNode");
		   		}
		    }

		    function toggleSelectAll(selectAllCheckbox, parent) {
		        parent.getElements('input[type=checkbox]').each(function(el) {
		        	el.checked = selectAllCheckbox.checked;
		        	if(el.name == "params.customerIds" || el.name == "customerId_regionId")
		        		setMixedCheckbox(el, false);
		        });
		    }
		    function setChecked(checkboxes, checked) {
		    	checkboxes.each(function(el) {
		        	el.checked = checked;
		        	setMixedCheckbox(el, false);
		        });
		    }
		    function getCustomerCheckbox(regionCheckbox) {
		     	return $("regions_of_" + regionCheckbox.value.split("_")[0]).getParent().getChildren().filter("input[type='checkbox']")[0];
		    }
		    function customerSelectionChanged(checkbox) {
		    	checkbox = $(checkbox);
		    	setChecked($("regions_of_" + checkbox.value).getElements("input[type=checkbox]"), checkbox.checked);
		    	setMixedCheckbox(checkbox, false);
		    }
		    function regionSelectionChanged(checkbox) {
		    	checkbox = $(checkbox);
		    	var terminals = $("terminals_of_" + checkbox.value);
		    	if(terminals)
		    	    setChecked(terminals.getElements("input[type=checkbox]"), checkbox.checked);
		    	var subregions = $("subregions_of_" + checkbox.value);
                if(subregions)
                    setChecked(subregions.getElements("input[type=checkbox]"), checkbox.checked);
                setMixedCheckbox(checkbox, false);
                var customerCheckbox = getCustomerCheckbox(checkbox);
                var parentCheckbox = checkbox.getParent().getParent(".parentTreeDiv &gt; input[type=checkbox]");
                while(parentCheckbox &amp;&amp; parentCheckbox != customerCheckbox) {
                    setMixedCheckbox(parentCheckbox, true);
                    parentCheckbox = parentCheckbox.getParent().getParent(".parentTreeDiv &gt; input[type=checkbox]");
                }
                setMixedCheckbox(customerCheckbox, true);
		    }
		    function terminalSelectionChanged(checkbox) {
		    	checkbox = $(checkbox);
		    	var par = checkbox.getParent();
		    	while(par != null &amp;&amp; !/^terminals_of_\d+/.test(par.id)) {
		    		par = par.getParent();
		    	}
		    	if(par != null) {
		    		var regionCheckbox = par.getParent().getChildren().filter("input[type='checkbox']")[0];
		    		if(regionCheckbox != null) {
		    			setMixedCheckbox(regionCheckbox, true);
				    	var customerCheckbox = getCustomerCheckbox(regionCheckbox);
						var parentCheckbox = checkbox.getParent().getParent(".parentTreeDiv &gt; input[type=checkbox]");
		                while(parentCheckbox &amp;&amp; parentCheckbox != customerCheckbox) {
		                    setMixedCheckbox(parentCheckbox, true);
		                    parentCheckbox = parentCheckbox.getParent().getParent(".parentTreeDiv &gt; input[type=checkbox]");
		                }
		                setMixedCheckbox(customerCheckbox, true);
	    			}
	    		}
		    }
		    function setMixedCheckbox(checkbox, mixed) {
		    	if(!mixed) {
		    		if(checkbox.mixedCover)
		    			checkbox.mixedCover.setStyle("display","none");
		    	} else {
			    	if(!checkbox.mixedCover) {
			    		checkbox.mixedCover = new Element("div", {
			    			styles:{position:"absolute",
								height:checkbox.offsetHeight,
								width:(Browser.firefox  ? checkbox.offsetWidth : Browser.opera ? checkbox.offsetWidth + 4 : checkbox.offsetWidth + 2),
								zIndex:99,
								top:checkbox.offsetTop,
								left:(Browser.firefox ? checkbox.offsetLeft : Browser.ie ? 12 : Browser.opera ? 11 : 13),
								backgroundImage:checkboxTristateImages.normal,
								backgroundPosition:"center center",
								backgroundRepeat:"no-repeat",
								backgroundColor:"white"
							},
			    			events:{
			    				click:function() {
			    					checkbox.checked = true;
			    					if(checkbox.onclick)
			    						checkbox.onclick();
			    				},
			    				mouseenter:function() {
			    					this.setStyle("backgroundImage", checkboxTristateImages.hover);
		    					},
			    				mouseleave:function() {
			    					this.setStyle("backgroundImage", checkboxTristateImages.normal);
		    					},
			    				mousedown:function() {
			    					this.setStyle("backgroundImage", checkboxTristateImages.down);
		    					},
			    				mouseup:function() {
			    					this.setStyle("backgroundImage", checkboxTristateImages.hover);
		    					}
		    				}
			    		});
			    		checkbox.mixedCover.inject(checkbox, "before");
			    	}
		    		checkbox.checked = false;
			    	checkbox.mixedCover.setStyle("display","block");
		    	}
		    }

		    function validateTerminals() {
		    	if($('isFilterTerminal').value=='true'){
		    		var checkboxes = $("terminals_of_filter").getElements("input[type=checkbox]");
			    	for(var i = 0; i &lt; checkboxes.length; i++) {
	                    if(checkboxes[i].checked) {
	                        return true;
	                    }
	                }
			        alert("Please select at least one device.");
			        return false;
		    	}else{
			    	if(!$("terminalsTree")){
			    		alert("Please expand customers tree or search then select at least one device.");
			        	return false;
			    	}
			    	var checkboxes = $("terminalsTree").getElements("input[type=checkbox]");
			    	for(var i = 0; i &lt; checkboxes.length; i++) {
	                    if(checkboxes[i].checked) {
	                        return true;
	                    }
	                }
			        alert("Please select at least one device.");
			        return false;
		        }
		    }
		    
		    function removeFilter() {
		       $('isFilterTerminal').value=false;
	           $('filteredTerminals').hide();
	           $('terminals_of_filter').setHTML("");
	           if($("terminalsTree")){
	           	$("terminalsTree").show();       
	           }
	           if($("terminalsTreeCustomerAll")){
	           	$("terminalsTreeCustomerAll").show();       
	           }   
	           $('selectAllCheckboxFiltered').checked=false;
	        }
	        
	        function doSearch(){
	        	var searchTextValue=$('searchText').value.trim();
	        	if(searchTextValue.length &lt; 3){
	        		alert("Please enter at least 3 characters for search.");
	        		return;
	        	}else{
		        	$("terminals_of_filter").setHTML("&amp;#160;&amp;#160;Loading..."); 
		        	if($("terminalsTree")){
                     	$("terminalsTree").hide();
                     }
                     if($("terminalsTreeCustomerAll")){
                     	$("terminalsTreeCustomerAll").hide();
                     }
                     if(!$('selectAllCheckbox').disabled){
	                     $('selectAllCheckbox').checked=false;
	                     toggleSelectAll($('selectAllCheckbox'), $('terminalsTree'));
                     }
                     $('filteredTerminals').show();
                     $('isFilterTerminal').value=true;
                     $('selectAllCheckboxFiltered').checked=false;
                     $('filterText').setText(searchTextValue);
		        	 new Request.HTML({
							url : 'filter_terminals_checkbox.i', 
							method : "post",
							async : true,
							update : $("terminals_of_filter"),
							data: {searchText: searchTextValue, fragment:true, searchField: $('searchField').getValue()}
						}).send();
				}
	        }
	        
	        function toggleSelectAllFiltered() {
		        $('terminals_of_filter').getElements('input[type=checkbox]').each(function(el) {
		        	el.checked = $('selectAllCheckboxFiltered').checked;
		        });
		    }
		    
		    function searchKeyDown(event){
		    	if( event.which ) {
					keyCode = event.which;
				} else if( event.keyCode ) {
					keyCode = event.keyCode;
				}
				if( 13 == keyCode ) {
					if(event.preventDefault) {
						event.preventDefault();
					}else{
						event.returnValue = false;
					}
					doSearch();
				}
		    }
		    
		    var tranCategory1Hide=1;
		    var tranCategory2Hide=1;
		    var tranCategory3Hide=1;
		    
		    function getTranCategoryMode(category){
		    	if(category==1){
		    		if(tranCategory1Hide==1){
		    			tranCategory1Hide=0;
		    		}else{
		    			tranCategory1Hide=1;
		    		}
		    		return tranCategory1Hide;
		    	}else if(category==2){
		    		if(tranCategory2Hide==1){
		    			tranCategory2Hide=0;
		    		}else{
		    			tranCategory2Hide=1;
		    		}
		    		return tranCategory2Hide;
		    	}else{
		    		if(tranCategory3Hide==1){
		    			tranCategory3Hide=0;
		    		}else{
		    			tranCategory3Hide=1;
		    		}
		    		return tranCategory3Hide;
		    	}
		    }
		    
		    function hideTranType(category){
		    	var categoryName="tranCategory"+category;
		    	var tranCategoryMode=getTranCategoryMode(category);
		    	if(tranCategoryMode==1){
		    		if($(categoryName)){
		    			$(categoryName).set("style", "display:none");
		    			$("tranCategoryTree"+category).set("class","closedTreeNode");
		    		}
		    	}else{
		    		if($(categoryName)){
			    		$(categoryName).set("style", "display:block");
			    		$("tranCategoryTree"+category).set("class","openTreeNode");
		    		}
		    	}
		    	
		    }
		    function hideTranTypeAll(){
		    	
		    	if($("tranCategoryTreeAll").get("class")=="closedTreeNode"){
		    		$("tranCategoryTreeAll").set("class","openTreeNode");
		    		 tranCategory1Hide=1;
		    		 tranCategory2Hide=1;
		             tranCategory3Hide=1;
		    	}else{
		    		$("tranCategoryTreeAll").set("class","closedTreeNode");
		    		tranCategory1Hide=0;
		    		tranCategory2Hide=0;
		            tranCategory3Hide=0;
		    	}
		    	hideTranType(1);
		    	hideTranType(2);
		    	hideTranType(3);
		    }
		    
			function toggleTranType(category, checked){
				var matchExp="input[type=checkbox][data-trancategory="+category+"]";
					$("criteriaForm").getElements(matchExp).each(function(el) {
							if(checked){
								el.checked=true;
							}else{
								el.checked=false;
							}
		        		});
				
			}
			function checkAllTriggered(checked){
				if(checked){
					$("tranCategoryCheckbox1").checked=true;
					$("tranCategoryCheckbox2").checked=true;
					if($("tranCategoryCheckbox3")){
						$("tranCategoryCheckbox3").checked=true;
					}
				}else{
					$("tranCategoryCheckbox1").checked=false;
					$("tranCategoryCheckbox2").checked=false;
					if($("tranCategoryCheckbox3")){
						$("tranCategoryCheckbox3").checked=false;
					}
				}
			}
			
		    window.addEvent('domready', function() {
			<xsl:for-each select="/a:base/b:selectedItems/b:entry">
				<xsl:if test="count(b:value/b:entry) &gt; 0">
				toggleCustomerTree($("regions_of_<xsl:value-of select="b:key" />").getPrevious(), <xsl:value-of select="b:key" />, "<!--
				  --><xsl:for-each select="b:value/b:entry/b:key"><xsl:value-of select="." />,</xsl:for-each><!--
				  -->", "<!--
				  --><xsl:for-each select="b:value/b:entry/b:value/b:value-item"><xsl:value-of select="." />,</xsl:for-each><!--
				  -->");
					</xsl:if>				
			</xsl:for-each>
			});   
	        
		</script>
	</xsl:template>

	<xsl:template name="contents">
		<xsl:call-template name="contents-select-terminal"/>
	</xsl:template>
	
	<xsl:template name="contents-select-terminal">
		<xsl:variable name="isInternal" select="reflect:getProperty($context, 'simple.servlet.ServletUser.internal')"/>
		<input type="hidden" name="isFilterTerminal" id="isFilterTerminal" value="false"/>
		<table class="paramTable">
		<tr>
		<td valign="top">
			<fieldset class="no-border-grouper" data-validators="validate-reqchk-bynode">
		    <div class="selectTerminalsScroll">
		    <div class="sideButtonPane">Device Search: 
		    <select  id="searchField" name="searchField">
	        	<option value="serial_nbr">Serial Number</option>
	        	<option value="terminal_nbr">Terminal Number</option>
	        	<option value="asset_nbr">Asset Number</option>
	        	<option value="location_name">Location Name</option>
	        	<option value="client">Client</option>
            </select> 
		    <input id="searchText" name="searchText" type="text" onkeypress="return searchKeyDown(event);" /><input type="button" onclick="doSearch();" value="Search"/></div>
		    <xsl:attribute name="data-validator-properties">{label:'location'}</xsl:attribute>
		    <div class="spacer5"></div>
		    <ul class="hide filterTree" id="filteredTerminals">
            <li class="openTreeNode">
            	<input type="checkbox" onclick="toggleSelectAllFiltered()" id="selectAllCheckboxFiltered" name="selectAllCheckboxFiltered" />
                <span class="filterLabel">Devices filtered by "<span id="filterText"></span>"&#160;<button class="cancelFilter" title="Remove Filter" onclick="removeFilter();" type="button" ><div/></button></span>
                <div class="childTreeDiv" id="terminals_of_filter">&#160;&#160;Loading...</div>
            </li>
        	</ul>
   		<xsl:choose>
         <xsl:when test="$isInternal = 'true'" > 
		           <xsl:choose>
		           <xsl:when test="count(/a:base/b:selectedItems/b:entry) &gt; 0" > 
		            <ul id="terminalsTreeCustomerAll" class="customers tree">
				    <li class="openTreeNode">
				    <a class="treeImage" onclick="toggleAllCustomer(this, false)">&#160;</a>
				    	<input type="checkbox" onclick="toggleSelectAll(this, $('terminalsTree'))" id="selectAllCheckbox" name="selectAllCheckbox" /><a class="treeLabel" onclick="toggleAllCustomer(this, false)">All</a>
				    	<div id="customerTreeAll" class="childTreeDiv" >
		          			<xsl:call-template name="select-customer-tree"/>
		          		</div>
				    	</li>
				    </ul>
				    </xsl:when>
         			<xsl:otherwise>
         			<ul id="terminalsTreeCustomerAll" class="customers tree">
         				<li class="closedTreeNode">
         				<a class="treeImage" onclick="toggleAllCustomer(this, false)">&#160;</a>
				    	<input disabled="disabled" style="display:none;" type="checkbox" onclick="toggleSelectAll(this, $('terminalsTree'))" id="selectAllCheckbox" name="selectAllCheckbox" /><a class="treeLabel" onclick="toggleAllCustomer(this, false)">All</a>
				    	<div id="customerTreeAll" class="childTreeDiv" />
				    	</li>
				    </ul>
         			</xsl:otherwise>
         			</xsl:choose>
         </xsl:when>
         <xsl:otherwise>
				<ul id="terminalsTreeCustomerAll" class="customers tree">
				    <li class="openTreeNode">
				    	<a class="treeImage" onclick="toggleAllCustomer(this,true)">&#160;</a>
				    	<input type="checkbox" onclick="toggleSelectAll(this, $('terminalsTree'))" id="selectAllCheckbox" name="selectAllCheckbox" /><a class="treeLabel" onclick="toggleAllCustomer(this,true)">All</a>
				  		<div id="customerTreeAll" class="childTreeDiv">
		          			<xsl:call-template name="select-customer-tree"/>
		          		</div>
		          </li>
				 </ul>
         </xsl:otherwise>
       </xsl:choose>
       		</div>
			</fieldset>
			</td>
		</tr>
		</table>
	</xsl:template>
</xsl:stylesheet>
