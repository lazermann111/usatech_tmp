<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:r="http://simple/results/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect r a">
	<xsl:import href="search-base.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:param name="context" />

	<xsl:template name="contents">
		<xsl:attribute name="class">selectSectionArea</xsl:attribute>
		<form>
			<select id="terminalIds" multiple="multiple" size="10">
				<optgroup label="Device - Location - Customer">
					<xsl:for-each select="a:base/a:terminals/r:results/r:row">
						<option value="{r:terminalId}">
							<xsl:value-of select="r:deviceSerialNum" />
							<xsl:text> - </xsl:text>
							<xsl:value-of select="r:locationName" />
							<xsl:text> - </xsl:text>
							<xsl:value-of select="r:customerName" />
						</option>
					</xsl:for-each>
				</optgroup>
			</select>
			<div style="text-align: center;">
				<input type="button" value="Remove Selected" onclick="removeSelectedTerminals('terminalIds');" />
			</div>
		</form>
	</xsl:template>
</xsl:stylesheet>