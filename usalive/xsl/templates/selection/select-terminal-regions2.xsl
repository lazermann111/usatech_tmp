<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:r="http://simple/results/1.0"
	xmlns:p="http://simple/xml/parameters/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="a r p">
	<xsl:output method="xhtml"/>

	<xsl:template match="/">
		<ul class="regions tree">
			<xsl:for-each select="a:base/a:regionResults/r:results/r:row[r:subRegionFlag='N']">
                <xsl:call-template name="region-line"/>
            </xsl:for-each>
		</ul>
	</xsl:template>
	
	<xsl:template name="region-line">
        <li class="closedTreeNode">
            <div class="parentTreeDiv">
            <a class="treeImage" onclick="toggleRegionTree(this, {r:customerId}, {r:regionId})">&#160;</a>
            <xsl:variable name="terminals" select="r:terminals"/>
            <input id="{r:customerId}_{r:regionId}" type="checkbox" name="terminals" value="{r:terminals}" onclick="regionSelectionChanged(this)">
	            <xsl:if test="boolean(/a:base/a:parameters/p:parameters/p:parameter[@p:name='terminals' and @p:value = $terminals])">
	                <xsl:attribute name="checked">checked</xsl:attribute>
	            </xsl:if>
            </input>
            <a class="treeLabel" onclick="toggleRegionTree(this, {r:customerId}, {r:regionId})"><xsl:value-of select="r:regionName"/></a>
            <xsl:if test="r:childCount &gt; 0"><div class="childTreeDiv" id="terminals_of_{r:customerId}_{r:regionId}">Loading...</div></xsl:if>
            <xsl:variable name="parentRegionId" select="r:regionId"/>
            <xsl:if test="count(following-sibling::*[r:parentRegionId = $parentRegionId]) &gt; 0">
            <ul class="childTreeDiv subregions" id="subregions_of_{r:customerId}_{r:regionId}">
               <xsl:for-each select="following-sibling::*[r:parentRegionId = $parentRegionId]">
                   <xsl:call-template name="region-line"/>
               </xsl:for-each>
            </ul>
            </xsl:if>
            </div>
        </li>
    </xsl:template>
</xsl:stylesheet>
