<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:r="http://simple/results/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect r a">
	<xsl:import href="search-base.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:param name="context" />

	<xsl:template name="contents">
		<form id="searchForm" name="searchForm" method="get" action="customer_select">
			<input type="hidden" name="folioId" value="{reflect:getProperty($context,'folioId')}" />
			<input type="hidden" name="promptForParams" value="false" />
			<input type="hidden" name="searchAction" value="{reflect:getProperty($context,'simple.servlet.SimpleServlet.SearchPath')}" />
			<xsl:call-template name="date-range">
				<xsl:with-param name="startParamName" select="'StartDate'" />
				<xsl:with-param name="endParamName" select="'EndDate'" />
			</xsl:call-template>
			<div class="spacer"><xsl:text> </xsl:text></div>
			<xsl:call-template name="fee-select">
				<xsl:with-param name="paramName" select="'FeeTypeId'" />
				<xsl:with-param name="feeRows" select="a:base/a:fees/r:results/r:row" />
			</xsl:call-template>
			<div class="spacer"><xsl:text> </xsl:text></div>
			<xsl:call-template name="device-type">
				<xsl:with-param name="paramName" select="'DeviceTypeId'" />
				<xsl:with-param name="deviceTypeRows" select="a:base/a:deviceTypes/r:results/r:row" />
			</xsl:call-template>
			<div class="spacer"><xsl:text> </xsl:text></div>
			<xsl:call-template name="currency-select">
				<xsl:with-param name="paramName" select="'CurrencyTypeId'" />
				<xsl:with-param name="currencyRows" select="a:base/a:currencies/r:results/r:row" />
			</xsl:call-template>
			<div class="spacer"><xsl:text> </xsl:text></div>
			<xsl:call-template name="terminal-search">
				<xsl:with-param name="paramName" select="'TerminalId'" />
				<xsl:with-param name="terminalRows" select="a:base/a:selectedTerminals/r:results/r:row" />
			</xsl:call-template>
			<div style="text-align: left; padding-top: 10px; padding-left: 115px;">
				<input type="button" onclick="runReport('searchForm')" value="Run Report" />
			</div>
		</form>
		<script type="text/javascript" defer="defer">
			initDates('searchForm');
		</script>
	</xsl:template>
</xsl:stylesheet>