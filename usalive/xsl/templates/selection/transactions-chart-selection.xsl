<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:p="http://simple/xml/parameters/1.0"
	xmlns:cnv="http://simple/xml/extensions/java/simple.bean.ConvertUtils"
	xmlns:r="http://simple/results/1.0"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="p cnv r a">
	<xsl:import href="../selection/search-base.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:variable name="folioId" select="/a:base/a:parameters/p:parameters/p:parameter[@p:name = 'folioId']/@p:value"/>
	
	<xsl:template name="contents">
		<script defer="defer" type="text/javascript">
			function validateForm(form) {
				var dt = form.elements.namedItem("params.StartDate").value;
				
				if (dt.length == 0) {
					alert("Please enter a start date.");
					return false;
				} else {
					if (isNaN(new Date(dt))) {
						alert("'" + dt + "' is not a valid date. Please enter a start date in mm/dd/yyyy format.");
						return false;
					}
				}
				
				dt = form.elements.namedItem("params.EndDate").value;
				if (dt.length == 0) {
					alert("Please enter an end date.");
					return false;
				} else {
					if (isNaN(new Date(dt))) {
						alert("'" + dt + "' is not a valid date. Please enter an end date in mm/dd/yyyy format.");
						return false;
					}
				}
				
				var ids = form.elements["params.TransTypeId"];
				for (var i = 0 ; i &lt; ids.length ; i++) {
					if (ids[i].checked == true) {
						return true;
					}
				}
				
				alert("Please select at least on transaction type to graph.");
				return false;
			}
		</script>

		<form id="searchForm" name="searchForm" method="get" onsubmit="return validateForm(this);">
			<xsl:attribute name="action">run_report_async.i</xsl:attribute>
			<input type="hidden" name="folioId" value="{$folioId}"/>
			<input type="hidden" name="outputType" value="{/a:base/a:parameters/p:parameters/p:parameter[@p:name = 'outputType']/@p:value}"/>
            <input type="hidden" name="promptForParams" value="false"/>
			<input type="hidden" name="unframed" value="false"/>
			<table class="selectBody">
				<caption>Transaction Totals Graph</caption>
				<tr style="vertical-align: top;">
					<td>
						<xsl:call-template name="date-range">
							<xsl:with-param name="startParamName" select="'params.StartDate'"/>
							<xsl:with-param name="endParamName" select="'params.EndDate'"/>
						</xsl:call-template>
						<div class="selectSection" id="selectTransType">
							<div class="sectionTitle">Select Transaction Types</div>
							<div class="sectionRow">
								<xsl:for-each select="/a:base/a:trans/r:results/r:row">
									<span><span class="selectOptionCluster">
										<input type="checkbox" name="params.TransTypeId" value="{r:id}">
											<xsl:if test="r:id = 16 or r:id = 22"><xsl:attribute name="checked" value="true"/></xsl:if>
										</input>
										<xsl:value-of select="r:name"/>
									</span></span>
								</xsl:for-each>
								<input type="button" class="checkbox-button" value="Select All" onclick="var ops=['Select All','Deselect All'];var i=ops.indexOf(this.value);this.value=ops[(i+1)%2];Array.from($(this).getParent().getElements('input[type=checkbox]')).each(function(cb) {{cb.checked=(i==0);});"/>
							</div>
						</div>
						<div class="spacer"><xsl:text> </xsl:text></div>
					</td>
				</tr>
				<tr align="center">
					<td>
						<div style="text-align: center; padding-top: 10px;">
							<input type="submit"><xsl:attribute name="value">Create Graph</xsl:attribute></input>
						</div>
					</td>
				</tr>
			</table>
		</form>
		<hr/>
	</xsl:template> 
</xsl:stylesheet>	