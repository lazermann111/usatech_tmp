<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:r="http://simple/results/1.0"
	xmlns:p="http://simple/xml/parameters/1.0"
	xmlns:b="http://simple/bean/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="a b r p reflect">
	<xsl:output method="xhtml"/>

	<xsl:param name="context" />

	<xsl:template match="/">
		<table class="terminalChildren" width="514" border="0" cellPadding="2" cellSpacing="3">
			<tr class="tableHeader">
				<th width="20"> </th>
				<th align="left" width="231">Device</th>
				<th align="left" width="251">Location</th>
				<th align="left" width="231">Asset #</th>
			</tr>
			<xsl:for-each select="a:base/a:terminalResults/r:results/r:row">
				<xsl:variable name="terminalId" select="r:terminalId"/>
				<tr class="tchecklist">
					<td width="20">
						<input type="checkbox" onclick="terminalSelectionChanged(this)" value="{r:terminalId}" name="params.terminalIds" id="terminals">
							<xsl:if test="boolean(reflect:getProperty($context, concat('selectedTerminals.', r:terminalId)))">
								<xsl:attribute name="checked">checked</xsl:attribute>
							</xsl:if>
						</input>
					</td>
					<td align="left" width="231"><xsl:value-of select="r:serialNum"/></td>
					<td align="left" width="251"><xsl:value-of select="r:location"/></td>
					<td align="left" width="231"><xsl:value-of select="r:assetNbr"/></td>
				</tr>
			</xsl:for-each>
		</table>
	</xsl:template>
</xsl:stylesheet>
