<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:r="http://simple/results/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	exclude-result-prefixes="a r reflect">
	<xsl:output method="xhtml"/>
	<xsl:param name="context" />
	
	<xsl:template name="select-customer-tree">
		<ul class="customers tree" id="terminalsTree">
					<xsl:for-each select="/a:base/a:customerResults/r:results/r:row">
						<li class="closedTreeNode">
							<div class="parentTreeDiv">
							<a class="treeImage" onclick="toggleCustomerTree(this, {r:customerId})">&#160;</a>
							<input type="checkbox" name="params.customerIds" value="{r:customerId}" onclick="customerSelectionChanged(this)">
								<xsl:if test="string(reflect:getProperty($context, concat('selectedItems.', r:customerId))) = '{}'">
									<xsl:attribute name="checked">checked</xsl:attribute>
								</xsl:if>
							</input>
							<a class="treeLabel" onclick="toggleCustomerTree(this, {r:customerId})"><xsl:value-of select="r:customerName"/></a>
						    <div class="childTreeDiv" id="regions_of_{r:customerId}">&#160;&#160;Loading...</div>
						    </div>
						</li>
					</xsl:for-each>
				</ul>
	</xsl:template>
</xsl:stylesheet>
