<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:cond="http://simple/xml/extensions/java/simple.bean.ConditionUtils"
	xmlns:r="http://simple/results/1.0"
	xmlns:x="http://simple/translate/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect r x cond">
	<xsl:import href="../general/app-base.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:param name="context" />

	<xsl:template name="subtitle"><x:translate>Selection Criteria</x:translate></xsl:template>
	
	<xsl:template name="extra-scripts">
		<script type="text/javascript">
			<xsl:attribute name="src"><xsl:call-template name="uri"><xsl:with-param name="uri">selection/report-selection-scripts.js</xsl:with-param></xsl:call-template></xsl:attribute>
		</script>
	</xsl:template>

	<xsl:template name="date-range">
		<xsl:param name="startParamName" select="'StartDate'" />
		<xsl:param name="endParamName" select="'EndDate'" />
	    <xsl:param name="radioParamName"/>
		<div class="selectSection" id="selectDateRange">
			<div class="sectionTitle"><x:translate>Select Date Range</x:translate></div>
			<div class="sectionRow">
			<table>
			     <xsl:if test= "$radioParamName">
				    <tr>
				 	<td>
					 	<div>
						 	<label class="radioLabel"><input type="radio" name="dateRange" value="Yesterday" onclick="setDatesToYesterday()"/>Yesterday</label>
						 	<label class="radioLabel"><input type="radio" name="dateRange" value="Week to Date" onclick="setDatesToWeekToDate()"/>Week to Date</label>
						 	<label class="radioLabel"><input type="radio" name="dateRange" value="Last Week" onclick="setDatesToLastWeek()" />Last Week</label>
					 	    <label class="radioLabel"><input type="radio" name="dateRange" value="Month to Date" onclick="setDatesToMonthToDate()"/>Month to Date</label>
					 	    <label class="radioLabel"><input type="radio" name="dateRange" value="Previous Month" onclick="setDatesToLastMonth()"/>Previous Month</label>
						</div>
					 </td>
				     </tr>
			      </xsl:if>
				<tr>
				<td>
				<div>
				<label for="startDateId"><x:translate>Start</x:translate></label>
				<input type="text" id="startDateId" size="10" data-toggle="calendar" data-format="%m/%d/%Y">
					<xsl:attribute name="name">
						<xsl:value-of select="$startParamName" />
					</xsl:attribute>
					<xsl:attribute name="value">
					<xsl:choose>
						<xsl:when test="string-length(reflect:getProperty($context, $startParamName)) &gt; 0">
							<xsl:value-of select="reflect:getProperty($context, $startParamName)" />
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="reflect:getProperty($context, 'defaultStartDate')" />
						</xsl:otherwise>
					</xsl:choose>
					</xsl:attribute>
				</input>
				
				<label for="endDateId"><x:translate>End</x:translate></label>
				<input type="text" id="endDateId" size="10" data-toggle="calendar" data-format="%m/%d/%Y">
					<xsl:attribute name="name">
						<xsl:value-of select="$endParamName" />
					</xsl:attribute>
					<xsl:attribute name="value">
						<xsl:choose>
							<xsl:when test="string-length(reflect:getProperty($context, $endParamName)) &gt; 0">
								<xsl:value-of select="reflect:getProperty($context, $endParamName)" />
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="reflect:getProperty($context, 'defaultEndDate')" />
							</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
				</input>
				</div>
				</td></tr>
			</table>
			</div>
		</div>
	</xsl:template>

	<xsl:template name="device-type">
		<xsl:param name="paramName" select="'DeviceTypeId'" />
		<xsl:param name="deviceTypeRows" />

		<xsl:if test="count($deviceTypeRows) &gt; 0">
			<div class="selectSection" id="selectDeviceType">
				<div class="sectionTitle"><x:translate>Device Type</x:translate></div>
				<div class="sectionRow"> <input type="button" class="toggleAll" onclick="toggleAll(this, '{$paramName}');" value="Deselect All"/>
					<xsl:for-each select="$deviceTypeRows">
						<xsl:sort select="string-length(r:deviceTypeDesc)" data-type="number" />
						<!-- Double span is needed for IE's wonderful rendering of "white-space: nowrap" -->
						<span><span class="selectOptionCluster">
							<input type="checkbox" device="{r:deviceTypeDesc}" name="{$paramName}" value="{r:deviceTypeId}">
								<xsl:if test="contains(concat(',', reflect:getProperty($context, $paramName), ','), r:deviceTypeId) or string-length(normalize-space(reflect:getProperty($context, $paramName))) = 0">
									<xsl:attribute name="checked">checked</xsl:attribute>
								</xsl:if>
							</input>
							<xsl:value-of select="r:deviceTypeDesc" />
						</span></span>
					</xsl:for-each>
				</div>
			</div>
		</xsl:if>
	</xsl:template>

	<xsl:template name="device-type-filter">
		<xsl:param name="paramName" select="'DeviceTypeId'" />
		<xsl:param name="deviceTypeRows" />
		<xsl:param name="filterIndex" select="1" />

		<xsl:if test="count($deviceTypeRows) &gt; 0">
			<input type="hidden" name="filter_field_{$filterIndex}" value="321"/> <!-- Device Type Id -->
			<input type="hidden" name="filter_op_{$filterIndex}" value="21"/><!--  MEMBER OF -->
			<input type="hidden" name="filter_param_name_{$filterIndex}" value="{$paramName}"/>
			<div class="selectSection" id="selectDeviceType">
				<div class="sectionTitle"><x:translate>Device Type</x:translate> <input type="button" class="toggleAll" onclick="toggleAll(this, '{$paramName}');" value="Deselect All"/></div>
				<div class="sectionRow">
					<xsl:for-each select="$deviceTypeRows">
						<xsl:sort select="string-length(r:deviceTypeDesc)" data-type="number" />
						<!-- Double span is needed for IE's wonderful rendering of "white-space: nowrap" -->
						<span><span class="selectOptionCluster">
							<input type="checkbox" device="{r:deviceTypeDesc}" name="{$paramName}" value="{r:deviceTypeId}">
								<xsl:if test="contains(concat(',', reflect:getProperty($context, $paramName), ','), r:deviceTypeId) or string-length(normalize-space(reflect:getProperty($context, $paramName))) = 0">
									<xsl:attribute name="checked">checked</xsl:attribute>
								</xsl:if>
							</input>
							<xsl:value-of select="r:deviceTypeDesc" />
						</span></span>
					</xsl:for-each>
				</div>
			</div>
		</xsl:if>
	</xsl:template>
	
	<xsl:template name="currency-select">
		<xsl:param name="paramName" select="'CurrencyTypeId'" />
		<xsl:param name="currencyRows" />

		<xsl:if test="count($currencyRows) &gt; 0">
			<div class="selectSection" id="selectCurrencyType">
				<div class="sectionTitle"><x:translate>Select Currency Type</x:translate></div>
				<div class="sectionRow">
					<xsl:for-each select="$currencyRows">
						<xsl:sort select="string-length(r:currencyName)" data-type="number" />
						<!-- Double span is needed for IE's wonderful rendering of "white-space: nowrap" -->
						<span class="selectOptionCluster">
							<input type="radio">
								<xsl:variable name="propName">
									<xsl:value-of select="r:currencyId" />
								</xsl:variable>
								<xsl:if test="(reflect:getProperty($context, $paramName) = $propName) or ((string-length(reflect:getProperty($context, $paramName)) = 0) and ($propName = '1'))">
									<xsl:attribute name="checked">checked</xsl:attribute>
								</xsl:if>
								<xsl:attribute name="name">
									<xsl:value-of select="$paramName" />
								</xsl:attribute>
								<xsl:attribute name="value">
									<xsl:value-of select="r:currencyId" />
								</xsl:attribute>
							</input>
							<xsl:value-of select="r:currencyName" />
						</span>
					</xsl:for-each>
				</div>
			</div>
		</xsl:if>
	</xsl:template>

	<xsl:template name="fee-select">
		<xsl:param name="paramName" select="'FeeTypeId'" />
		<xsl:param name="feeRows" />

		<xsl:if test="count($feeRows) &gt; 0">
			<div class="selectSection" id="selectFeeType">
				<div class="sectionTitle"><x:translate>Select Fee Types</x:translate></div>
				<div class="sectionRow">
					<xsl:for-each select="$feeRows">
						<xsl:sort select="string-length(r:feeName)" data-type="number" />
						<!-- Double span is needed for IE's wonderful rendering of "white-space: nowrap" -->
						<span><span class="selectOptionCluster">
							<input type="checkbox">
								<xsl:variable name="feeId">
									<xsl:value-of select="r:feeId" />
								</xsl:variable>
								<xsl:if test="reflect:getProperty($context, concat($paramName,$feeId)) = $feeId">
									<xsl:attribute name="checked">checked</xsl:attribute>
								</xsl:if>
								<xsl:attribute name="name">
									<xsl:value-of select="$paramName" />
								</xsl:attribute>
								<xsl:attribute name="value">
									<xsl:value-of select="$feeId" />
								</xsl:attribute>
							</input>
							<xsl:value-of select="r:feeName" />
						</span></span>
					</xsl:for-each>
				</div>
			</div>
		</xsl:if>
	</xsl:template>

	<xsl:template name="terminal-search">
		<div class="selectSection" id="searchDevices">
			<div class="sectionTitle"><x:translate>Search for Devices</x:translate></div>
			<div class="sectionRow">
			<table>
				<tr><td>
					<form action="customer_select.i" target="select_frame" onsubmit="openSelectWindow();">
						<span><x:translate>Customer</x:translate></span>
						<input type="text" id="searchCustomer" name="searchCustomer" value="{reflect:getProperty($context, 'searchCustomer')}" />
						<input type="submit" value="Search &gt;&gt;"/>
					   <input type="hidden" name="unframed" value="true"/>
                    </form>
				</td></tr>
				<tr><td>
					<form action="location_select.i" target="select_frame" onsubmit="openSelectWindow();">
						<span><x:translate>Location</x:translate></span>
						<input type="text" id="searchLocation" name="searchLocation" value="{reflect:getProperty($context, 'searchLocation')}" />
						<input type="submit" value="Search &gt;&gt;"/>
						<input type="hidden" name="unframed" value="true"/>
					</form>
				</td></tr>
				<tr><td>
					<form action="terminal_select.i" target="select_frame" onsubmit="openSelectWindow();">
						<span><x:translate>Device</x:translate></span>
						<input type="text" id="searchTerminal" name="searchTerminal" value="{reflect:getProperty($context, 'searchTerminal')}" />
						<input type="submit" value="Search &gt;&gt;"/>
					   <input type="hidden" name="unframed" value="true"/>
                    </form>
				</td></tr>
			</table>
			</div>
		</div>
	</xsl:template>

	<xsl:template name="terminal-search2">
		<div class="selectSection" id="searchDevices">
			<div class="sectionTitle"><x:translate>Search for Devices</x:translate></div>
			<div class="sectionRow">
			<table>
				<xsl:if test="cond:check(8, 'NOT IN', reflect:getProperty($context, 'simple.servlet.ServletUser.userGroupIds'))">
				<tr><th>
					<label for="searchCustomer"><x:translate>Customer</x:translate></label>
					</th>
					<td>
					<input type="text" id="searchCustomer" name="searchCustomer" value="{reflect:getProperty($context, 'searchCustomer')}" />
					</td>
					<td>
					<input type="button" value="Search &gt;&gt;" onclick="openSelectWindow('customer_select.i?searchCustomer='+encodeURIComponent(document.getElementById('searchCustomer').value));"/>
					</td>
				</tr>
				</xsl:if>
				<tr>
					<th><label for="searchLocation"><x:translate>Location</x:translate></label></th>
					<td><input type="text" id="searchLocation" name="searchLocation" value="{reflect:getProperty($context, 'searchLocation')}" /></td>
					<td><input type="button" value="Search &gt;&gt;" onclick="openSelectWindow('location_select.i?searchLocation='+encodeURIComponent(document.getElementById('searchLocation').value));"/></td>
				</tr>
				<tr>
					<th><label for="searchTerminal"><x:translate>Device</x:translate></label></th>
					<td><input type="text" id="searchTerminal" name="searchTerminal" value="{reflect:getProperty($context, 'searchTerminal')}" /></td>
					<td><input type="button" value="Search &gt;&gt;" onclick="openSelectWindow('terminal_select.i?searchTerminal='+encodeURIComponent(document.getElementById('searchTerminal').value));"/></td>
				</tr>
			</table>
			</div>
		</div>
	</xsl:template>

	<xsl:template name="terminal-select">
		<xsl:param name="paramName" select="'TerminalId'" />
		<xsl:param name="allParamName" select="'AllTerminals'" />
		<xsl:param name="terminalRows" />
		<xsl:param name="allTerminals" select="not(reflect:getProperty($context, 'AllTerminals') = '0')" />

		<div class="selectSection" id="selectDevices">
			<div class="sectionTitle"><x:translate>Select Devices</x:translate></div>
			<div class="sectionRow">
				<input type="radio" id="radioAllDevices" name="{$allParamName}" value="1" onclick="$('terminalSelection').hide();">
					<xsl:if test="$allTerminals">
						<xsl:attribute name="checked">true</xsl:attribute>
					</xsl:if>
				</input>
				<x:translate>All Devices</x:translate>
				<input type="radio" id="radioSelectDevices" name="{$allParamName}" value="0" onclick="$('terminalSelection').show();">
					<xsl:if test="not($allTerminals)">
						<xsl:attribute name="checked">true</xsl:attribute>
					</xsl:if>
				</input>
				<x:translate>Selected Devices</x:translate>
			</div>
			<div id="terminalSelection">
				<xsl:attribute name="style">display:
					<xsl:choose>
						<xsl:when test="$allTerminals">
							none
						</xsl:when>
						<xsl:otherwise>
							block
						</xsl:otherwise>
					</xsl:choose>;
				</xsl:attribute>
				<input type="hidden" name="{$paramName}" id="terminalIds"/>
				<table><tr><td>
				<iframe id="terminalSelectFrame" name="terminalSelectFrame" class="terminalSelectFrame" frameborder="0" scrolling="no">
					<xsl:attribute name="src">
						selection_terminals.i?unframed=true&amp;terminalId=
						<xsl:for-each select="$terminalRows">
							<xsl:if test="position() &gt; 1">,</xsl:if>
							<xsl:value-of select="r:terminalId" />
						</xsl:for-each>
					</xsl:attribute>
				</iframe>
				</td></tr></table>
				<!--
				<select id="terminalIds" multiple="multiple" size="8" style="width: 300px; overflow: auto;">
					<xsl:attribute name="name">
						<xsl:value-of select="$paramName" />
					</xsl:attribute>
					<optgroup label="Device - Location - Customer">
						<xsl:for-each select="$terminalRows">
							<option value="{r:terminalId}">
								<xsl:value-of select="r:deviceSerialNum" />
								<xsl:text> - </xsl:text>
								<xsl:value-of select="r:locationName" />
								<xsl:text> - </xsl:text>
								<xsl:value-of select="r:customerName" />
							</option>
						</xsl:for-each>
					</optgroup>
				</select>
				<input type="button" value="Remove Selected" onclick="removeSelectedTerminals('terminalSelectId');" />
				-->
			</div>
		</div>
	</xsl:template>

	<xsl:template name="alert-type">
		<xsl:param name="paramName" select="'AlertTypeId'" />
		<xsl:param name="alertTypeRows" />
		<xsl:if test="count($alertTypeRows) &gt; 0">
			<div class="selectSection">
				<div class="sectionTitle"><x:translate>Alert Type</x:translate></div>
				<div class="sectionRow">
				<div style="width:530px;height:100px;overflow:auto;text-align:center">
				<table>
					<xsl:for-each select="$alertTypeRows">
					<xsl:if test="(position()mod 3)=1">
					<tr><xsl:variable name="position" select="position()" />
						<xsl:for-each select="$alertTypeRows[position() &gt;=$position and position()&lt;$position+3 ] ">
						<td align="left">
							<input type="checkbox" name="{$paramName}" value="{r:alertTypeId}" id="alertType{r:alertTypeId}">
								<xsl:if test="r:isSelected = 'Y'">
									<xsl:attribute name="checked"> checked </xsl:attribute>
								</xsl:if>
							</input>
							<label for="alertType{r:alertTypeId}"> <xsl:value-of select="r:alertTypeName" />
							</label>
						</td>
						</xsl:for-each>
					</tr>
					</xsl:if>
					</xsl:for-each>
				</table>
				</div>
				</div>
				</div>
			<div class="center">
				<input type="button"  name="selectButton" onclick="toggleAll(this, 'AlertTypeId');" value="Select All"/>
			</div>
		</xsl:if>
	</xsl:template>

</xsl:stylesheet>