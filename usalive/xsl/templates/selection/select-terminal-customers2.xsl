<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:r="http://simple/results/1.0"
	xmlns:x2="http://simple/translate/2.0"
	xmlns:b="http://simple/bean/1.0"
	xmlns:p="http://simple/xml/parameters/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="a r x2 b p">
	<xsl:import href="../general/app-base.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:template name="select-customers">
		<xsl:param name="div-class-name"/>
		<script type="text/javascript" defer="true">
			var checkboxTristateImages = (
				Browser.safari || Browser.chrome ? {normal:"url(images/form/checkbox-tristate-safari.png)", hover:"url(images/form/checkbox-tristate-safari.png)", down:"url(images/form/checkbox-tristate-safari.png)"}
							  : {normal:"url(images/form/checkbox-tristate.png)", hover:"url(images/form/checkbox-tristate-hover.png)", down:"url(images/form/checkbox-tristate-down.png)"});		    
		    function toggleCustomerTree(a, customerId) {
		    	toggleTree(a,"regions_of_"+customerId, "select_terminal_regions2.i", {customerId:customerId});
		    }
		    function toggleRegionTree(a, customerId, regionId) {
		    	toggleTree(a,"terminals_of_"+customerId+"_"+regionId, "select_terminal_children.i", {customerId:customerId, regionId:regionId});
		    }
		    function toggleTree(a, updateId, url, userData) {
		    	a = $(a);
		    	var li = a.getParent().getParent();
		    	if(li.hasClass("closedTreeNode")) {
		    		if(!li.childrenLoaded) {
		    			var updateDiv = $(updateId);
                        if(updateDiv) {
	                        var checkbox = a.getParent().getElement("input[type=checkbox]");
			    			var selectAll = checkbox.checked;
			    			var data = {fragment:true};
							if(userData)
								Object.append(data, userData);
							new Request.HTML({
								url : url, 
								method : "post",
								async : true,
								update : updateDiv,
								evalScripts : true,
								link : "cancel",
								onComplete : function() {
									li.childrenLoaded = true;
									if(selectAll) {
										updateDiv.getElements("input[type=checkbox]").each(function(el) {
								        	el.checked = true;
								        });
										//setMixedCheckbox(checkbox, true);
									}
								},
								data : data
							}).send();
						}
		    		}
		    		li.addClass("openTreeNode");
		    		li.removeClass("closedTreeNode");
		    	} else {
		    		li.addClass("closedTreeNode");
		    		li.removeClass("openTreeNode");
		   		}
		    }

		    function toggleSelectAll(button) {
		        if(button.ison) {
		            button.ison = false;
		            button.value = "Select All";
		        } else {
		            button.ison = true;
		            button.value = "Deselect All";
		        }
		        $('terminalsTree').getElements('input[type=checkbox]').each(function(el) {
		        	el.checked = button.ison;
		        	if(el.name == "customerId" || el.getParent().getLast().id.test(/^terminals_of/))
		        		setMixedCheckbox(el, false);
		        });
		    }
		    function setChecked(checkboxes, checked) {
		    	checkboxes.each(function(el) {
		        	el.checked = checked;
		        	setMixedCheckbox(el, false);
		        });
		    }
		    function getCustomerCheckbox(regionCheckbox) {
		        var chks = $("regions_of_" + regionCheckbox.id.split("_")[0]).getSiblings("input[type=checkbox]");
		     	return chks[0];
		    }
		    function customerSelectionChanged(checkbox) {
		    	checkbox = $(checkbox);
		    	setChecked($("regions_of_" + checkbox.value).getElements("input[type=checkbox]"), checkbox.checked);
		    	setMixedCheckbox(checkbox, false);
		    }
		    function regionSelectionChanged(checkbox) {
		    	checkbox = $(checkbox);
		    	setChecked(checkbox.getParent().getLast().getElements("input[type=checkbox]"), checkbox.checked);
		    	setMixedCheckbox(checkbox, false);
		    	var customerCheckbox = getCustomerCheckbox(checkbox);
		    	var parentCheckbox = checkbox.getParent().getParent(".parentTreeDiv").getFirst("input[type=checkbox]");
                while(parentCheckbox &amp;&amp; parentCheckbox != customerCheckbox) {
                    setMixedCheckbox(parentCheckbox, true);
                    parentCheckbox = parentCheckbox.getParent().getParent(".parentTreeDiv").getFirst("input[type=checkbox]");
                }
                setMixedCheckbox(customerCheckbox, true);
		    }
		    function terminalSelectionChanged(checkbox) {
		    	checkbox = $(checkbox);
		    	var par = checkbox.getParent();
		    	while(par != null &amp;&amp; !/^terminals_of_\d+/.test(par.id)) {
		    		par = par.getParent();
		    	}
		    	if(par != null) {
		    		var regionCheckbox = par.getParent().getChildren().filter("input[type='checkbox']")[0];
		    		if(regionCheckbox != null) {
		    			setMixedCheckbox(regionCheckbox, true);
				    	var customerCheckbox = getCustomerCheckbox(regionCheckbox);
				    	var parentCheckbox = checkbox.getParent().getParent(".parentTreeDiv").getFirst("input[type=checkbox]");
		                while(parentCheckbox &amp;&amp; parentCheckbox != customerCheckbox) {
		                    setMixedCheckbox(parentCheckbox, true);
		                    parentCheckbox = parentCheckbox.getParent().getParent(".parentTreeDiv").getFirst("input[type=checkbox]");
		                }
		                setMixedCheckbox(customerCheckbox, true);
	    			}
	    		}
		    }
		    function setMixedCheckbox(checkbox, mixed) {
		    	if(!mixed) {
		    		if(checkbox.mixedCover)
		    			checkbox.mixedCover.setStyle("display","none");
		    	} else {
			    	if(!checkbox.mixedCover) {
			    		checkbox.mixedCover = new Element("div", {
			    			styles:{position:"absolute",
								height:checkbox.offsetHeight,
								width:(Browser.firefox  ? checkbox.offsetWidth : Browser.opera ? checkbox.offsetWidth + 4 : checkbox.offsetWidth + 2),
								zIndex:99,
								top:checkbox.offsetTop,
								left:(Browser.firefox ? checkbox.offsetLeft : Browser.ie ? 12 : Browser.opera ? 11 : 13),
								backgroundImage:checkboxTristateImages.normal,
								backgroundPosition:"center center",
								backgroundRepeat:"no-repeat",
								backgroundColor:"white"
							},
			    			events:{
			    				click:function() {
			    					checkbox.checked = true;
			    					if(checkbox.onclick)
			    						checkbox.onclick();
			    				},
			    				mouseenter:function() {
			    					this.setStyle("backgroundImage", checkboxTristateImages.hover);
		    					},
			    				mouseleave:function() {
			    					this.setStyle("backgroundImage", checkboxTristateImages.normal);
		    					},
			    				mousedown:function() {
			    					this.setStyle("backgroundImage", checkboxTristateImages.down);
		    					},
			    				mouseup:function() {
			    					this.setStyle("backgroundImage", checkboxTristateImages.hover);
		    					}
		    				}
			    		});
			    		checkbox.mixedCover.inject(checkbox, 'before');
			    	}
		    		checkbox.checked = false;
			    	checkbox.mixedCover.setStyle("display","block");
		    	}
		    }

		    function validateTerminals() {
		    	var checkboxes = $("terminalsTree").getElements("input[type=checkbox]");
		    	for(var i = 0; i &lt; checkboxes.length; i++) {
                    if(checkboxes[i].checked) {
                    	window.setTimeout(function() {pickDevicesDialog.hide();} , 1);
                        return true;
                    }
                }
		        alert("Please select at least one device.");
		        return false;
		    }
		</script>
		<style>
			.customers a.treeLabel { font-weight: bold; color: #0000CC; }
			.regions a.treeLabel { font-weight: bold; color: #CC0000; }
			.customers li { margin-left: 10px; }
			.terminalChildren {margin-left: 15px; }
			.selectTerminalsScroll {position: relative; height: 400px; background: white; }
			.tree { margin: 0px; }
			.tree input { position: static; }
			.tchecklist td {
				border: none;
				font-weight: bold;
				height: 8px;
				vertical-align: top;
			}
			.tchecklist input {
				position: relative;
				bottom: 3px;
				height: 16px;
			}		
		</style>
		<div class="{$div-class-name}">
		<ul class="customers tree" id="terminalsTree">
				<xsl:for-each select="a:base/a:customerResults/r:results/r:row">
					<li class="closedTreeNode">
						<div class="parentTreeDiv">
						<a class="treeImage" onclick="toggleCustomerTree(this, {r:customerId})">&#160;</a>
						<xsl:variable name="customerId" select="r:customerId"/>
						<input type="checkbox" name="customerId" value="{r:customerId}" onclick="customerSelectionChanged(this)">
							<xsl:if test="boolean(/a:base/a:parameters/p:parameters/p:parameter[@p:name='customerId' and @p:value = $customerId])">
								<xsl:attribute name="checked">checked</xsl:attribute>
							</xsl:if>
						</input>
						<a class="treeLabel" onclick="toggleCustomerTree(this, {r:customerId})"><xsl:value-of select="r:customerName"/></a>
					    <div class="childTreeDiv" id="regions_of_{r:customerId}">&#160;&#160;Loading...</div>
					    </div>
					</li>
				</xsl:for-each>
			</ul>
			<xsl:if test="count(/a:base/a:customerResults/r:results/r:row) = 1">
			<script type="text/javascript">
				window.onload=function(){var img =  $("terminalsTree").getElement(".treeImage"); if(img) img.onclick();};
			</script>
			</xsl:if>
		</div>
	</xsl:template>
</xsl:stylesheet>
