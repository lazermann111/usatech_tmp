 <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:p="http://simple/xml/parameters/1.0"
	xmlns:x="http://simple/translate/1.0"
	xmlns:r="http://simple/results/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect a r x">
	<xsl:import href="search-base.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:param name="context" />

	<xsl:template name="subtitle">Report Selection</xsl:template>
	<xsl:template name="contents">
		<table class="selectBody" style="width: 420Px;">
			<caption>Sony Printer Alerts Summary</caption>
			<tr>
				<td>
					<form id="searchForm" name="searchForm" action="sony_printer_alerts_summary.i" target="reportFrame">
						<input type="hidden" name="promptForParams" value="false" />
						<xsl:call-template name="parameters-form">
							<xsl:with-param name="overrides">
								<p:ignore name="username" />
								<p:ignore name="password" />
								<p:ignore name="StartDate" />
								<p:ignore name="EndDate" />
								<p:override name="promptForParams" value="false" />
							</xsl:with-param>
						</xsl:call-template>
						<xsl:call-template name="date-range">
							<xsl:with-param name="startParamName" select="'StartDate'" />
							<xsl:with-param name="endParamName" select="'EndDate'" />
							<xsl:with-param name="radioParamName" select="'radio'" />
						</xsl:call-template>
						<div style="text-align: center; clear: both;">
							<input type="submit" value="Run Report" />
						</div>
					</form>
				</td>
			</tr>
		</table>
	</xsl:template>
</xsl:stylesheet>