<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:r="http://simple/results/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect r a">
	<xsl:import href="search-base.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:param name="context" />

	<xsl:template name="contents">
		<form id="searchForm" name="searchForm" method="get" action="remittance_selection.i">
			<div class="title">Remittance Reports</div>
			<div>
				<div>
					<input type="radio" name="AllApproved" value="1" />
					<span>Run for all currently approved Docs</span>
				</div>
				<div>
					<input type="radio" name="AllApproved" value="0" />
					<span>Search for Docs:</span>
				</div>
				<div>
					<div>
						<span>Start Date</span>
						<input type="text" id="startDateId" name="StartDate" data-toggle="calendar" data-format="%m/%d/%Y">
							<xsl:attribute name="value">
								<xsl:value-of select="reflect:getProperty($context, 'StartDate')" />
							</xsl:attribute>
						</input>
					</div>
					<div>
						<span>End Date</span>
						<input type="text" id="endDateId" name="EndDate" data-toggle="calendar" data-format="%m/%d/%Y">
							<xsl:attribute name="value">
								<xsl:value-of select="reflect:getProperty($context, 'EndDate')" />
							</xsl:attribute>
						</input>
					</div>
					<div>
						<span>Customer</span>
						<input type="text" name="CustomerName">
							<xsl:attribute name="value">
								<xsl:value-of select="reflect:getProperty($context, 'CustomerName')" />
							</xsl:attribute>
						</input>
					</div>
					<input type="button" value="Search &gt;&gt;" onclick="doSearch('searchForm')" />
				</div>
				<xsl:call-template name="doc-items">
					<xsl:with-param name="rows" select="a:base/a:docs/r:results/r:row" />
				</xsl:call-template>
				<div>
					<input type="button" value="Run Report &gt;&gt;" onclick="runReport('searchForm')" />
				</div>
			</div>
		</form>
	</xsl:template>

	<xsl:template name="doc-items">
		<xsl:param name="rows" />

		<xsl:if test="count($rows) &gt; 0">
			<table>
				<thead>
					<td></td>
					<td>Customer</td>
					<td>Status</td>
					<td>Start Date</td>
					<td>End Date</td>
					<td style="text-align: right">Amount</td>
				</thead>
				<tbody>
					<xsl:for-each select="$rows">
						<tr>
							<td>
								<input type="checkbox" name="DocId">
									<xsl:attribute name="value">
										<xsl:value-of select="r:docId" />
									</xsl:attribute>
								</input>
							</td>
							<td><xsl:value-of select="r:customerName" /></td>
							<td><xsl:value-of select="r:status" /></td>
							<td>
								<xsl:value-of select="substring(r:startDate,6,2)" />
								<xsl:text>/</xsl:text>
								<xsl:value-of select="substring(r:startDate,9,2)" />
								<xsl:text>/</xsl:text>
								<xsl:value-of select="substring(r:startDate,1,4)" />
							</td>
							<td>
								<xsl:value-of select="substring(r:endDate,6,2)" />
								<xsl:text>/</xsl:text>
								<xsl:value-of select="substring(r:endDate,9,2)" />
								<xsl:text>/</xsl:text>
								<xsl:value-of select="substring(r:endDate,1,4)" />
							</td>
							<td style="text-align: right">
								<xsl:choose>
									<xsl:when test="string-length(r:amount) &gt; 0">
										<xsl:value-of select="format-number(r:amount,'#,###.00')" />
									</xsl:when>
									<xsl:otherwise>
										<xsl:text>N/A</xsl:text>
									</xsl:otherwise>
								</xsl:choose>
							</td>
						</tr>
					</xsl:for-each>
				</tbody>
			</table>
		</xsl:if>
	</xsl:template>

	<xsl:template name="extra-styles">
		<script type="text/javascript">
			<xsl:text>
function doSearch(formId){
	if(validateDates(formId)){
		var form = document.getElementById(formId);

		form.action = 'remittance_select.i';
		form.target = '_self';
		form.submit();
	}
}

function runReport(formId){
	if(validateDates(formId)){
		var form = document.getElementById(formId);

		form.action = 'run_remittance.i';
		form.submit();
	}
}

function validateDates(formId){
	var form = document.getElementById(formId);
	var start = new Date(form.startDateId.value);
	var end = new Date(form.endDateId.value);
	var valid = false;

	if((start == 'Invalid Date') || (end == 'Invalid Date')){
		alert('You must enter a valid start and end date.');
	} else {
		if(start &gt; end){
			alert('The end date must occur after the start date.');
		} else {
			if(start &gt; (new Date())){
				valid = confirm('The date range is in the future, do you want to run this report anyway?');
			} else {
				valid = true;
			}
		}
	}

	return valid;
}
			</xsl:text>
		</script>
	</xsl:template>
</xsl:stylesheet>