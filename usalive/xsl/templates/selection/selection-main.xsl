<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:x="http://simple/translate/1.0"
	xmlns:r="http://simple/results/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect a r x">
	<xsl:import href="search-base.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:param name="context" />

	<xsl:template name="subtitle"><x:translate>Report Selection</x:translate></xsl:template>
	<xsl:template name="extra-styles">
		<style type="text/css">
			#selectDateRange { float: left; clear: left; width: 330px; }
			#selectDeviceType { float: left; clear: left; width: 330px;}
			#selectFeeType { float: left; clear: left; width: 330px;}
			#selectDevices { float: right; clear: right; width: 330px;}
			#searchDevices { float: right; clear: right; width: 330px; }
			#searchDevices td {	text-align: right; }
			.selectBody { width: 675px; }
		</style>
	</xsl:template>
	<xsl:template name="contents">
		<xsl:choose>
			<xsl:when test="string-length(reflect:hasProperty($context,'folioId')) &gt; 0">
				<table class="selectBody">
					<caption><xsl:value-of select="a:base/a:folioName/r:results/r:row[1]/r:folioName"/></caption>
					<tr>
						<td>
							<xsl:call-template name="terminal-search"/>
							<form id="searchForm" name="searchForm" action="run_report_async.i" onsubmit="return validateSearch(this);">
								<input type="hidden" name="folioId" value="{reflect:getProperty($context,'folioId')}" />
								<input type="hidden" name="promptForParams" value="false" />
								<input type="hidden" name="searchAction" value="{reflect:getProperty($context,'simple.servlet.SimpleServlet.SearchPath')}" />
								<xsl:call-template name="date-range">
									<xsl:with-param name="startParamName" select="'params.StartDate'" />
									<xsl:with-param name="endParamName" select="'params.EndDate'" />
								</xsl:call-template>
								<xsl:call-template name="fee-select">
									<xsl:with-param name="paramName" select="'params.FeeTypeId'" />
									<xsl:with-param name="feeRows" select="a:base/a:fees/r:results/r:row" />
								</xsl:call-template>
								<xsl:call-template name="device-type">
									<xsl:with-param name="paramName" select="'params.DeviceTypeId'" />
									<xsl:with-param name="deviceTypeRows" select="a:base/a:deviceTypes/r:results/r:row" />
								</xsl:call-template>
								<!--
								<xsl:call-template name="currency-select">
									<xsl:with-param name="paramName" select="'params.CurrencyTypeId'" />
									<xsl:with-param name="currencyRows" select="a:base/a:currencies/r:results/r:row" />
								</xsl:call-template>
								-->
								<xsl:call-template name="terminal-select">
									<xsl:with-param name="paramName" select="'params.TerminalId'" />
									<xsl:with-param name="allParamName" select="'params.AllTerminals'" />
									<xsl:with-param name="terminalRows" select="a:base/a:terminals/r:results/r:row" />
								</xsl:call-template>
								<div style="text-align: center; clear: both;">
									<input type="submit"><xsl:attribute name="value"><x:translate>Run Report</x:translate></xsl:attribute></input>
								</div>
							</form>
						</td>
					</tr>
				</table>
			</xsl:when>
			<xsl:otherwise>
				<div><x:translate>Invalid parameters. This page requires 'folioId'.</x:translate></div>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>