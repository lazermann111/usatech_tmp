<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:r="http://simple/results/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="a r">
	<xsl:import href="select-terminal-customers-tree.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:template match="/">
		<xsl:call-template name="select-customer-tree"/>
	</xsl:template>

</xsl:stylesheet>
