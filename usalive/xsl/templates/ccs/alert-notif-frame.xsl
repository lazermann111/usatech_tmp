<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:x2="http://simple/translate/2.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="x2">
	<xsl:import href="../general/app-base.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:template name="subtitle"><x2:translate key="alert-register-title">Alert Register</x2:translate></xsl:template>

	<xsl:template name="body">
			<frameset cols="35%,*" framespacing="0" resizable="false" noresize="noresize" border="0" frameborder="0">
			   <frame src="alert_register_admin.i" name="adminFrame"
				marginwidth="0" marginheight="0" scrolling="no"/>
			   <frame name="detailsFrame"
					marginwidth="0" marginheight="0" scrolling="auto"/>
			</frameset>
	</xsl:template>

</xsl:stylesheet>

