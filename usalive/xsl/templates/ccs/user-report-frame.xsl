<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:b="http://simple/bean/1.0"
	xmlns:req="http://simple/xml/extensions/java/simple.servlet.RequestInfo"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
    xmlns:x2="http://simple/translate/2.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="x2 a b req reflect">
	<xsl:import href="../general/wrapper.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:template name="subtitle"><x2:translate key="report-register-title">Report Register</x2:translate></xsl:template>

	<xsl:template name="extra-scripts">
		<script type="text/javascript">
			<xsl:attribute name="src"><xsl:call-template name="uri"><xsl:with-param name="uri">selection/report-selection-scripts.js</xsl:with-param></xsl:call-template></xsl:attribute>
		</script>
		<script type="text/javascript">
		var userId=<xsl:value-of select="/a:base/b:userId"/>;
		var isSelectAll=false;
		function beforeRequestSend(){
			$('reportSentButtonSection').set('style', 'display:none');
			$("waitImageReport").style.display='block';
			$("userReportHistoryContainer").set('html','');
			$('resendAllChecked').disabled=true;
			$('selectAllButton').disabled=true;
		}
		
		function requestReportSent(){
			if($('filterBy').value==2&amp;&amp;$('mainSelect').selectedIndex==-1){
				alert('Please select a user report to view recently sent report.');
			}else{
				if(validateDates($('reportSentForm'))){
					beforeRequestSend();
					if($('reportSentForm').checkForResend){
						$('reportSentForm').checkForResend=undefined;
					}
					new Request.HTML({ 
							async: true,
					    	url: "run_report_register_folio.i?title=MESSAGE:Sent+Reports+-+{params.StartDate,DATE,MM/dd/yyyy}+to+{params.EndDate,DATE,MM/dd/yyyy}&amp;fragment=true&amp;folioId=382&amp;params.UserId="+userId+"&amp;filterBy="+$('filterBy').value+"&amp;params.StartDate="+$('startDateId').value.trim()+"&amp;params.EndDate="+$('endDateId').value.trim()+"&amp;params.UserReportId="+$('mainSelect').value+"&amp;directiveNames=show-alt-output-types,show-run-date&amp;directiveValues=false,false",
							update: $("userReportHistoryContainer"),
							onSuccess: function(){
	   					 		$("waitImageReport").style.display='none';
	   					 		isSelectAll=false;
								enableResendAllChecked();
	   				 }}).send();
				}
			}
		}
		function enableResendAllChecked(){
			$('reportSentButtonSection').set('style', 'display:block');
			if($('reportSentForm').checkForResend){
				$('resendAllChecked').disabled=false;
				$('selectAllButton').disabled=false;
			}else{
				$('resendAllChecked').disabled=true;
				$('selectAllButton').disabled=true;
			}
		}
		
		function requestReportPending(){
			$('reportSentButtonSection').set('style', 'display:none');
			if($('filterBy').value==2&amp;&amp;$('mainSelect').selectedIndex==-1){
				alert('Please select a user report to view recently pending report.');
			}else{
				if(validateDates($('reportSentForm'))){
					beforeRequestSend();
					new Request.HTML({
			async: true,
				    	url: "run_report_register_folio.i?title=MESSAGE:Pending+Reports+-+{params.StartDate,DATE,MM/dd/yyyy}+to+{params.EndDate,DATE,MM/dd/yyyy}&amp;fragment=true&amp;folioId=1138&amp;params.UserId="+userId+"&amp;filterBy="+$('filterBy').value+"&amp;params.StartDate="+$('startDateId').value.trim()+"&amp;params.EndDate="+$('endDateId').value.trim()+"&amp;directiveNames=show-alt-output-types,show-run-date&amp;directiveValues=false,false",
						update: $("userReportHistoryContainer"),
						onSuccess: function(){
							$("waitImageReport").style.display='none';
						}
					}).send();
				}
				$('resendAllChecked').disabled=true;
				$('selectAllButton').disabled=true;
			}
		}
		
		window.addEvent('domready', function() {
			window.ccsActivateRequest = new Request.HTML({
			async: true,
			    	url: "ccs_transport_error_upd.i?fragment=true",
					update: $("userReportHistoryContainer")
				});	
			window.ccsDisabledListRequest = new Request.HTML({
			async: true,
			    	url: "ccs_transport_disabled_list.i?fragment=true",
					update: $("userReportHistoryContainer"),
					onSuccess: function(){
						$("waitImageReport").style.display='none';
					}
				});	
			window.ccsEnableRequest = new Request.HTML({
			async: true,
			    	url: "ccs_transport_enable.i?fragment=true",
					update: $("userReportHistoryContainer")
				});	
		});
		function promptDialogWithData(titleStr, urlStr, dataValue){
			new Dialog.Url({title: titleStr, url: urlStr, data: dataValue, height: 110, width: 300, register: true}).show();
		}
		function onSubmitResendAllChecked(){
			var hasChecked=false;
			$('reportSentForm').getElements("input[type=checkbox]").each(function(el) {
				if(el.checked){
					hasChecked=true;
					return;
				}
			});
			if(hasChecked){
				promptDialogWithData('Report Resent','bulk_resend_report.i', $("reportSentForm") );
			}else{
				alert("Please select at least one report for resend.");
			}
		}
		
		function onSelectAll(){
			if(isSelectAll){
				$('reportSentForm').getElements("input[type=checkbox]:checked").each(function(el) {
						el.checked=false;
				});
				isSelectAll=false;
				$('selectAllButton').value='Select All';
				
			}else{
				$('reportSentForm').getElements("input[type=checkbox]").each(function(el) {
						el.checked=true;
				});
				isSelectAll=true;
				$('selectAllButton').value='Deselect All';
			}
		}
		
		function requestTransportErrorHistory(){
			if(validateDates($('reportSentForm'))){
				beforeRequestSend();
				new Request.HTML({
			async: true,
				    	url: "ccs_transport_error_history.i?fragment=true&amp;params.StartDate="+$('startDateId').value.trim()+"&amp;params.EndDate="+$('endDateId').value.trim(),
						update: $("userReportHistoryContainer"),
						onSuccess: function(){
   					 		$("waitImageReport").style.display='none';
   				 }}).send();
			}
		}
		
		function requestTransportErrorHistoryDetails(ccsTransportErrorId){
			beforeRequestSend();
			new Request.HTML({
			async: true,
			    	url: "ccs_transport_error_history_details.i?fragment=true&amp;ccsTransportErrorId="+ccsTransportErrorId,
					update: $("userReportHistoryContainer"),
					onSuccess: function(){
  					 		$("waitImageReport").style.display='none';
  				 }}).send();
		}
		
		function onShowSentReports(){
			$('tabnav_ccs_error_history').set('class','topMenuItem');
			$('tabnav_ccs_enable').set('class','topMenuItem');
			$('tabnav_ccs_error').set('class','topMenuItem');
			$('tabnav_report').set('class','topMenuItem topMenuItemSelected');
			$('tabnav_report_unsent').set('class','topMenuItem');
			requestReportSent();
			enableResendAllChecked();
		}
		
		function requestFailingTransports(){
			if(validateDates($('reportSentForm'))){
			new Request.HTML({
			async: true,
			    	url: "ccs_transport_error.i?fragment=true&amp;params.StartDate="+$('startDateId').value.trim()+"&amp;params.EndDate="+$('endDateId').value.trim(),
					update: $("userReportHistoryContainer"),
					onSuccess: function(){
						$("waitImageReport").style.display='none';
					}
				}).send();	
			}
		}
		
		</script>
	</xsl:template>
	<xsl:template name="content-footer">
		<br/>
		<div class="userReportHistory">
			<form name="reportSentForm" id="reportSentForm" action="bulk_resend_report.i" method="post" >
			<input type="hidden" name="fragment" value="true" />
			<b>Filter By:</b><select id="filterBy" name="filterBy"
					style="width: 200px">
				<option value="1">All Reports Except DEX File</option>
				<option value="2">User Report (Select one above)</option>
				<option value="3">All Reports</option>
			</select>
			<b>Start Date:</b><input id="startDateId" type="text" size="10" name="params.StartDate" value="{reflect:getProperty($context, 'defaultStartDate')}" data-toggle="calendar" data-format="%m/%d/%Y"/>
			<b>End Date:</b><input id="endDateId" type="text" size="10" name="params.EndDate" value="{reflect:getProperty($context, 'defaultEndDate')}" data-toggle="calendar" data-format="%m/%d/%Y"/>
			<input type="button" name="showSentReports" id="showSentReports" value="Sent Reports" onclick="onShowSentReports();"/>
			<br/><br/>
			<div class="topMenu">
			<div id="tabContainer">
			<ul>
				<li id="tabnav_report" class="topMenuItem">
					<a onclick="onShowSentReports();">sent reports</a>
				</li>
				<li id="tabnav_report_unsent" class="topMenuItem">
					<a onclick="$('tabnav_ccs_error_history').set('class','topMenuItem');$('tabnav_ccs_enable').set('class','topMenuItem');$('tabnav_ccs_error').set('class','topMenuItem');$('tabnav_report').set('class','topMenuItem');$('tabnav_report_unsent').set('class','topMenuItem topMenuItemSelected');requestReportPending();" >pending reports</a>
				</li>
				<li id="tabnav_ccs_error" class="topMenuItem">
					<a onclick="$('tabnav_ccs_error_history').set('class','topMenuItem');$('tabnav_ccs_enable').set('class','topMenuItem');$('tabnav_ccs_error').set('class','topMenuItem topMenuItemSelected');$('tabnav_report').set('class','topMenuItem');$('tabnav_report_unsent').set('class','topMenuItem');beforeRequestSend();requestFailingTransports();" >failing transports</a>
				</li>
				<li id="tabnav_ccs_error_history" class="topMenuItem">
					<a onclick="$('tabnav_ccs_error_history').set('class','topMenuItem topMenuItemSelected');$('tabnav_ccs_enable').set('class','topMenuItem');$('tabnav_ccs_error').set('class','topMenuItem');$('tabnav_report').set('class','topMenuItem');$('tabnav_report_unsent').set('class','topMenuItem');requestTransportErrorHistory();" >transport error history</a>
				</li>
				<li id="tabnav_ccs_enable" class="topMenuItem">
					<a onclick="$('tabnav_ccs_error_history').set('class','topMenuItem');$('tabnav_ccs_enable').set('class','topMenuItem topMenuItemSelected');$('tabnav_ccs_error').set('class','topMenuItem');$('tabnav_report').set('class','topMenuItem');$('tabnav_report_unsent').set('class','topMenuItem');beforeRequestSend();ccsDisabledListRequest.send()" >disabled transports</a>
				</li>
			</ul>
			</div>
			</div>
			<div>
				<div id="waitImageReport" style="display:none"><img alt="Please wait" src="images/pleasewait.gif"/>&#160;Please wait, loading data...</div>
			</div>
			<div id="tabContents">
		  		<xsl:call-template name="tab-contents"/>
			</div>
			</form>
		</div>
	</xsl:template>
	
	<xsl:template name="tab-contents"><div id="reportSentButtonSection" style="display:none">
			<input type="button" style="float: right;" name="resendAllChecked" id="resendAllChecked" value="Resend All Checked" title="Check the recently sent reports for resending" disabled="disabled"
			onclick="onSubmitResendAllChecked();"/>
			<input type="button" style="float: right;" name="selectAllButton" id="selectAllButton" value="Select All" disabled="disabled" onclick="onSelectAll();"/></div><div id="userReportHistoryContainer" /></xsl:template>
</xsl:stylesheet>

