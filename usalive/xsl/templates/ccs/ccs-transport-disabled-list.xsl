<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:x2="http://simple/translate/2.0"
	xmlns:r="http://simple/results/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect a r">
	<xsl:import href="../general/app-base.xsl" />
	<xsl:output method="xhtml" />
	<xsl:param name="context" />
	
	<xsl:template name="subtitle">
		<x2:translate key="ccs-transport-enable-title">Disabled Transports</x2:translate>
	</xsl:template>
	
	<xsl:template name="extra-scripts">
		<script type="text/javascript">
	    	function enableRequest(transportId){
	    		if(confirm('Are you sure you want to enable this transport?')){
	    			ccsEnableRequest.send('transportId='+transportId);
	    		}
	    	}
	    </script>
	</xsl:template>
	
	<xsl:template name="contents">
	<p class="title2">Disabled Transports</p>
	<form name="ctEnableForm" method="post">
	<p>
			<xsl:attribute name="class">
				<xsl:choose>
					<xsl:when test="reflect:getProperty($context, 'type') = 'error'">errorText</xsl:when>
					<xsl:otherwise>confirmText</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:value-of select="reflect:getProperty($context, 'message')"/></p>
		<table class="reportTitle">
			<tr class="headerRow">
				<th><a data-toggle="sort" data-sort-type="STRING">Transport Name</a></th>
				<th><a data-toggle="sort" data-sort-type="STRING">Tranposrt Type</a></th>
				<th>Transport Properties</th>
				<th>Enable</th>
			</tr>
			<xsl:choose>
				<xsl:when
					test="count(a:base/a:ct_disabled/r:results/r:group) &gt; 0">
					<xsl:for-each
						select="/a:base/a:ct_disabled/r:results/r:group">
						<tr>
							<td data-sort-value="{r:row[1]/r:transportName}">
								<xsl:value-of select="r:row[1]/r:transportName" />
							</td>
							<td data-sort-value="{r:row[1]/r:transportTypeName}">
								<xsl:value-of select="r:row[1]/r:transportTypeName" />
							</td>
							<td>
								<ul>
								<xsl:for-each select="r:row">
									 <li><xsl:value-of select="r:transportProp" /></li>
							 	</xsl:for-each>
							 	</ul>
							</td>
							<td>
								<input type="button" value="Enable disabled transport" onclick="enableRequest({r:transportId});"/>
							</td>
						</tr>
					</xsl:for-each>
				</xsl:when>
				<xsl:otherwise>
					<tr>
						<td colspan="6">No disabled transports found</td>
					</tr>
				</xsl:otherwise>
			</xsl:choose>
		</table>
		</form>
	</xsl:template>
</xsl:stylesheet>