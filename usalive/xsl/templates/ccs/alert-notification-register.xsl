<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:b="http://simple/bean/1.0"
	xmlns:r="http://simple/results/1.0"
	xmlns:x2="http://simple/translate/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect r a x2 b">
 <xsl:import href="../selection/search-base.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:param name="context" />

	<xsl:template name="subtitle"><x2:translate key="alert-notification-register-title">Alert Notification Register</x2:translate></xsl:template>
	<xsl:template name="extra-styles">
		<style type="text/css">
			.selectSection { width: 530px; }
		</style>
	</xsl:template>

<xsl:template name="contents">
	<xsl:call-template name="messages-dialog"/>
	<script defer="defer" type="text/javascript">
		var adminFrame = window.parent.frames["adminFrame"];
		var changed = false;

		function userNotificationChanged(){
       		changed = true;
			var rf = $("reportForm");
			rf.elements.namedItem("submitButton").disabled = false;
            rf.elements.namedItem("cancelButton").disabled = false;
       		if(adminFrame &amp;&amp; adminFrame != self &amp;&amp; adminFrame.userNotificationChanged) adminFrame.userNotificationChanged();
   		}

		function cancelChanges() {
			var rf = $("reportForm");
			rf.reset();
			rf.elements.namedItem("submitButton").disabled = true;
            rf.elements.namedItem("cancelButton").disabled = true;
		<xsl:if test="not(a:base/b:transportOnly)">
			rf.elements.namedItem("selectButton").value="Select All";
		</xsl:if>
		    changed = false;
    		if($("radioSelectDevices").checked) {
    			var tsf = $("terminalSelectFrame");
    			tsf.src = tsf.src;
    			$("terminalSelectFrame").show();
			} else
				$("terminalSelectFrame").hide();

    		if(adminFrame &amp;&amp; adminFrame.changesCancelled) adminFrame.changesCancelled();
    	}

    	function validateForm(form){
    		if(String.from(form.elements.namedItem("email").value).trim() == ""){
    			<xsl:choose>
    				<xsl:when test="not(a:base/b:transportOnly)">
           		alert("Please enter your email.");
           		return false;
           			</xsl:when>
           			<xsl:otherwise>
           		return true;
           			</xsl:otherwise>
           		</xsl:choose>
      	    }
       		if(form.elements.namedItem("email").value.indexOf(".")&lt;3 || form.elements.namedItem("email").value.indexOf("@")== -1){
         		alert ("Please enter your valid email.");
         		return false;
         	}
       		return validateDeviceList(form);
		}
 	</script>
<form id="reportForm" name="reportForm" onsubmit="return validateForm(this);" action="alert_notif_update.i">
<xsl:if test="not(a:base/b:transportOnly)"><xsl:attribute name="target">contentFrame</xsl:attribute></xsl:if>
<xsl:variable name="row" select="/a:base/a:userAlertsDetail/r:results/r:row"/>
<input type="hidden" name="alertNotifId" value="{$row/r:alertNotifId}" />
<input type="hidden" name="transportId" value="{$row/r:transportId}"/>
<input type="hidden" name="requestAction">
<xsl:attribute name="value">
<xsl:choose>
	<xsl:when test="$row">update</xsl:when>
	<xsl:otherwise>insert</xsl:otherwise>
</xsl:choose>
</xsl:attribute>
</input>
<table cellspacing="0" cellpadding="0" class="selectBody">
<caption><x2:translate key="alert-notification-register-title">Alert Notification Register</x2:translate></caption>
<tr><td>
<div class="selectSection">
	<div class="sectionTitle">Contact</div>
	<div class="sectionRow">
		<input type="hidden" name="transportTypeId" value="1"/>
		<table class="input-rows" >
			<tr>
				<th><label for="email">Email</label></th>
				<td>
					<input size="35" id="email" name="tpv_1" value="{/a:base/a:transportPropertyValues/r:results/r:row[r:propertyTypeId=1]/r:value}"/>
				</td>
			</tr>
			<tr>
				<th><label for="cc">CC</label></th>
				<td>
					<input size="35" id="cc" name="tpv_2" value="{/a:base/a:transportPropertyValues/r:results/r:row[r:propertyTypeId=2]/r:value}"/>
				</td>
			</tr>
		</table>
	</div>
</div>
</td></tr>
<xsl:if test="not(a:base/b:transportOnly)">
<tr><td>
	<xsl:call-template name="alert-type">
		<xsl:with-param name="paramName" select="'AlertTypeId'" />
		<xsl:with-param name="alertTypeRows" select="a:base/a:selectedItems/r:results/r:row" />
	</xsl:call-template>
</td></tr>
</xsl:if>
<tr><td>
<xsl:call-template name="terminal-search2"/>
</td></tr>
<tr><td>
	<xsl:call-template name="terminal-select">
		<xsl:with-param name="paramName" select="'terminalId'" />
		<xsl:with-param name="terminalRows" select="a:base/a:selectedTerminals/r:results/r:row" />
		<xsl:with-param name="allTerminals" select="not($row/r:allTerminals = 'N')"/>
	</xsl:call-template>
</td>
</tr>
<tr>
	<td colspan="2" align="center" valign="bottom">
	<input type="submit" name="submitButton" value="Save Changes"/>
	<input type="button" name="cancelButton" value="Cancel Changes" onclick="cancelChanges()" /></td>
</tr>
</table>
</form>
<script defer="defer" type="text/javascript">
	$("reportForm").getElements("input, textarea, select").each(function(input){
   		input.addEvent("change", window.userNotificationChanged);
	});
	var doc;
	var frame = $("terminalSelectFrame");
	if(frame) {
		if(frame.contentWindow) doc = frame.contentWindow.document;
		else doc = frame.document;
		if(doc) {
			var el = $(doc.getElementById("terminalIds"));
			if(el) el.addEvent("change", window.userNotificationChanged);
		}
	}
</script>
</xsl:template>
</xsl:stylesheet>