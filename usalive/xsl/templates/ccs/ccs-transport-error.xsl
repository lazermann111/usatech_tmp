<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:x2="http://simple/translate/2.0"
	xmlns:r="http://simple/results/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect a r">
	<xsl:import href="../general/app-base.xsl" />
	<xsl:output method="xhtml" />
	<xsl:param name="context" />
	
	<xsl:template name="subtitle">
		<x2:translate key="ccs-transport-errors-title">Failing Transports</x2:translate>
	</xsl:template>
	
	<xsl:template name="extra-scripts">
		<style type="text/css">
	    .divScroll {
	    height:200px;
	    width:300px;
	    overflow:scroll;
	    }
	    </style>
	    <script type="text/javascript">
	    	function activateRequest(errorId){
	    		if(confirm('Are you sure you want to retry to transport this user report?')){
	    			ccsActivateRequest.send('errorId='+errorId);
	    		}
	    	}
	    </script>
	</xsl:template>
	
	<xsl:template name="contents">
	<p class="title2">Failing Transports</p>
	<form name="cteaForm" method="post">
	<input type="hidden" name="errorId" value="" />
	<p>
			<xsl:attribute name="class">
				<xsl:choose>
					<xsl:when test="reflect:getProperty($context, 'type') = 'error'">errorText</xsl:when>
					<xsl:otherwise>confirmText</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:value-of select="reflect:getProperty($context, 'message')"/></p>
		<table class="reportTitle">
			<tr class="headerRow">
				<th><a data-toggle="sort" data-sort-type="STRING">Report Name</a></th>
				<th><a data-toggle="sort" data-sort-type="STRING">Transport Name</a></th>
				<th><a data-toggle="sort" data-sort-type="STRING">Transport Type</a></th>
				<th>Transport Properties</th>
				<th>Error Reason</th>
				<th><a data-toggle="sort" data-sort-type="NUMBER">Error Time</a></th>
				<th><a data-toggle="sort" data-sort-type="NUMBER">Reattempt Time</a></th>
				<th><a data-toggle="sort" data-sort-type="STRING">Error Type</a></th>
				<th><a data-toggle="sort" data-sort-type="NUMBER">Retry Count</a></th>
				<th>Retry</th>
			</tr>
			<xsl:choose>
				<xsl:when
					test="count(a:base/a:ct_error/r:results/r:group) &gt; 0">
					<xsl:for-each
						select="/a:base/a:ct_error/r:results/r:group">
						<tr>
						<xsl:attribute name="class">
			                <xsl:choose>
			                    <xsl:when test="(position() mod 2) = 0">evenRow</xsl:when>
			                    <xsl:otherwise>oddRow</xsl:otherwise>
			                </xsl:choose>
			            </xsl:attribute>
							<td data-sort-value="{r:row[1]/r:reportName}">
								<xsl:value-of select="r:row[1]/r:reportName" />
							</td>
							<td data-sort-value="{r:row[1]/r:transportName}">
								<xsl:value-of select="r:row[1]/r:transportName" />
							</td>
							<td data-sort-value="{r:row[1]/r:transportTypeName}">
								<xsl:value-of select="r:row[1]/r:transportTypeName" />
							</td>
							<td>
								<ul>
								<xsl:for-each select="r:row">
									 <li><xsl:value-of select="r:transportProp" /></li>
							 	</xsl:for-each>
							 	</ul>
							</td>
							<td>
								<xsl:value-of select="r:row[1]/r:errorText" />
							</td>
							<td data-sort-value="{r:row[1]/r:errorTs}">
								<xsl:value-of select="r:row[1]/r:errorTs" />
							</td>
							<td data-sort-value="{r:row[1]/r:reattemptedTs}">
								<xsl:value-of select="r:row[1]/r:reattemptedTs" />
							</td>
							<td data-sort-value="{r:row[1]/r:errorType}">
								<xsl:value-of select="r:row[1]/r:errorType" />
							</td>
							<td data-sort-value="{r:row[1]/r:retryCount}">
								<a href="javascript:requestTransportErrorHistoryDetails({r:errorId})"><xsl:value-of select="r:row[1]/r:retryCount" /></a>
							</td>
							<td>
								<input type="button" value="Retry Now" onclick="activateRequest({r:errorId});"/>
							</td>
						</tr>
					</xsl:for-each>
				</xsl:when>
				<xsl:otherwise>
					<tr>
						<td colspan="6">No transports are currently failing</td>
					</tr>
				</xsl:otherwise>
			</xsl:choose>
		</table>
		</form>
	</xsl:template>
</xsl:stylesheet>