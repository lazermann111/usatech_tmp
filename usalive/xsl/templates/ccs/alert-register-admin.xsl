<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:r="http://simple/results/1.0"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:x2="http://simple/translate/2.0"
	xmlns:p="http://simple/xml/parameters/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="r x2 a p">
	<xsl:import href="../general/app-base.xsl"/>
	<xsl:output method="xhtml"/>
	<xsl:param name="context" />

	<xsl:template name="subtitle"><x2:translate key="alert-register-admin-title">Registered Alerts</x2:translate></xsl:template>
	<xsl:template name="extra-scripts">
		<style type="text/css"> body { border-right: 1px solid black; } </style>
	</xsl:template>

	<xsl:template name="contents">
		<xsl:call-template name="messages-dialog"/>
		<script defer="defer" type="text/javascript">

    function setUserNotification(select) {
    	try {
	    	var changed = detailsFrame.changed;
	        if(changed) {
	            alert("The changes you made are not saved. Please save or change the changes before selecting a different report.");
	            return false;
	        }
        } catch(e) {
        }
        setValue(document.forms.namedItem("buttonForm").elements.namedItem("alertNotifId"), getValue(select));
        select.form.submit();
        return true;
    }

    function addUserNotification() {
    	userNotificationChanged();
        with(document.forms.namedItem("listNotificaForm")) {
            submit();
        }
    }

    function deleteUserNotification() {
        if(!confirm("Are you sure you want to delete this notification?")) return;
        with(document.forms.namedItem("buttonForm")) {
            submit();
         }
    }

    function userNotificationChanged() {
    	with(document.forms.namedItem("listNotificaForm")) {
            elements.namedItem("alertNotifId").disabled = true;
        }
        with(document.forms.namedItem("buttonForm")) {
            elements.namedItem("addButton").disabled = true;
            elements.namedItem("deleteButton").disabled = true;
        }
    }

    function changesCancelled() {
    	with(document.forms.namedItem("listNotificaForm")) {
            elements.namedItem("alertNotifId").disabled = false;
        }
        with(document.forms.namedItem("buttonForm")) {
            elements.namedItem("addButton").disabled = false;
            elements.namedItem("deleteButton").disabled =(elements.namedItem("alertNotifId").selectedIndex == -1);
        }
    }

		</script>
		<table cellspacing="0" cellpadding="0" class="selectBody">
		<caption><x2:translate key="alert-register-admin-title">Registered Alerts</x2:translate></caption>
			<tr>
				<td>
					<div class="selectSection">
						<div class="sectionTitle">
							<x2:translate key="alert-register-list">Registered Alerts</x2:translate>
						</div>
						<div class="sectionRow">
								<table>

									<tr>
										<td>
										<form name="listNotificaForm" action="alert_notif_detail.i" method="post" target="detailsFrame">
											<select style="width: 330px; height: 330px;" size="10" name="alertNotifId"
												onchange="return setUserNotification(this)">

												<!--  <xsl:for-each select="/a:base/a:userAlertsType/r:results/r:group" >
													<xsl:variable name="alertNotifId" select="r:alertNotifId"/>
													<option value="{r:alertNotifId}">
														<xsl:for-each select="r:row">
															<xsl:value-of select="r:alertTypeName" />
														</xsl:for-each>
														<xsl:variable name="terminalRows" select="/a:base/a:userAlertsTerminal/r:results/r:group[r:alertNotifId=$alertNotifId]/r:row"/>
														<xsl:variable name="transportRows" select="/a:base/a:userTransportValue/r:results/r:group[r:alertNotifId=$alertNotifId]/r:row"/>
														<xsl:value-of select="r:terminalId" /><xsl:value-of select="r:alertTypeName" />(<xsl:value-of select="r:transportPropertyValue" />)
													</option>
												</xsl:for-each>-->

												<xsl:for-each select="/a:base/a:userAlertsType/r:results/r:group" >
													<xsl:variable name="alertNotifId" select="r:alertNotifId"/>
													<option value="{r:alertNotifId}">
													<xsl:variable name="alertTypeNumber" select="count(/a:base/a:userAlertsType/r:results/r:group[r:alertNotifId=$alertNotifId]/r:row)" />
													<xsl:variable name="terminalNumber" select="count(/a:base/a:userAlertsTerminal/r:results/r:group[r:alertNotifId=$alertNotifId]/r:row)" />
														<xsl:if test="$alertTypeNumber = 34">
															<xsl:text>All alerts</xsl:text>
															<xsl:text>&#160;</xsl:text>
														</xsl:if>
														<xsl:if test="$alertTypeNumber &lt; 3" >
															<xsl:for-each select="r:row">
																 <xsl:value-of select="r:alertTypeName" />
														 		 <xsl:text>-</xsl:text>
														 	</xsl:for-each>
														</xsl:if>
														<xsl:if test="$alertTypeNumber &gt; 2 and $alertTypeNumber &lt;34 ">
															<xsl:value-of select="$alertTypeNumber" />
															<xsl:text>&#160;alerts</xsl:text>
															<xsl:text>&#160;</xsl:text>
														</xsl:if>
														 <xsl:choose>
														 	<xsl:when test="$terminalNumber &gt;2">
														 		<xsl:value-of select="$terminalNumber" />
														 		<xsl:text>&#160;terminals</xsl:text>
														 		<xsl:text>&#160;</xsl:text>
														 	</xsl:when>
														 	<xsl:otherwise>
														 		<xsl:copy-of select="/a:base/a:userAlertsTerminal/r:results/r:group[r:alertNotifId=$alertNotifId]/r:row"/>
														 	</xsl:otherwise>
														 </xsl:choose>
														 <xsl:copy-of select="/a:base/a:userTransportValue/r:results/r:group[r:alertNotifId=$alertNotifId]/r:row"/>
							 						</option>
												</xsl:for-each>

											</select>
										</form>
										</td>
									</tr>
									<tr>
										<td align="center"><form name="buttonForm" action="alert_notif_delete.i" method="post">
											<input name="alertNotifId" type="hidden"/>
											<input name="addButton" type="button" value="Add Notification" onclick="addUserNotification()" />
											<input name="deleteButton" type="button" value="Delete Notification" onclick="deleteUserNotification()" />
											</form>
										</td>
									</tr>
								</table>
						</div>
					</div>
				</td>

			</tr>
		</table>
		<script defer="defer" type="text/javascript">
			var ur = document.forms.namedItem("listNotificaForm").elements.namedItem("alertNotifId");
			<xsl:variable name="alertNotifId" select="number(/a:base/a:parameters/p:parameters/p:parameter[@p:name='alertNotifId']/@p:value)"/>
			<xsl:if test="boolean($alertNotifId)">setValue(ur, <xsl:value-of select="$alertNotifId"/>);</xsl:if>
			setUserNotification(ur);
		</script>
	</xsl:template>
</xsl:stylesheet>

