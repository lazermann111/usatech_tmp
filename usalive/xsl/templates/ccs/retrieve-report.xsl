<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:x2="http://simple/translate/2.0"
	xmlns:r="http://simple/results/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect a r x2">
	<xsl:import href="../general/app-base.xsl" />
	<xsl:output method="xhtml" />
	<xsl:param name="context" />

	<xsl:template name="subtitle">
		<x2:translate key="retrieve_report_title">
			Retrieve Report
		</x2:translate>
	</xsl:template>

	<xsl:template name="contents">
		<p class="title1">Retrieve Report</p>
		<p>
			<xsl:attribute name="class">
				<xsl:choose>
					<xsl:when test="reflect:getProperty($context, 'type') = 'error'">errorText</xsl:when>
					<xsl:otherwise>confirmText</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:value-of select="reflect:getProperty($context, 'message')" />
		</p>
		<xsl:choose>
			<xsl:when test="count(a:base/a:reportRequest/r:results/r:row) &gt; 0">
				<xsl:for-each
					select="/a:base/a:reportRequest/r:results/r:row">
					<input type="submit" value="Regenerate the report"
						onclick="window.location='refresh_report_data.i?requestId={r:requestId}&amp;refreshFileId={reflect:getProperty($context, 'fileId')}&amp;profileId={r:profileId}';" />
				</xsl:for-each>
			</xsl:when>
			<xsl:otherwise>
				<p>
					No report record found. Please contact the
					administrator for further help.
				</p>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>