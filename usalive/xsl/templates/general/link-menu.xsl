<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:r="http://simple/results/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect r a">
	<xsl:import href="app-base.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:template name="extra-scripts">
		
	</xsl:template>
	<xsl:template name="contents">
		<xsl:choose>
			<xsl:when test="count(/a:base/a:links/r:results/r:row) &gt; 0">
				<table class="linkMenuList"><tr><td>
				<xsl:for-each select="/a:base/a:links/r:results/r:row/r:category">
					<xsl:variable name="category" select="."/>
					<xsl:variable name="pos" select="position()"/>
					<xsl:if test="(string-length(/a:base/a:links/r:results/r:row[$pos - 1]/r:category) &lt; 1) or ($category != /a:base/a:links/r:results/r:row[$pos - 1]/r:category)">
						<div class="selectSection">
							<div class="sectionTitle">
								<xsl:value-of select="." />
							</div>
							<div>
								<ul>
									<xsl:for-each select="/a:base/a:links/r:results/r:row[r:category = $category]">
										<li>
											<a title="{r:description}" href="{r:href}">
												<xsl:value-of select="r:title" />
											</a>
										</li>
									</xsl:for-each>
								</ul>
							</div>
						</div>
						<div class="spacer">&#160;</div>
					</xsl:if>
				</xsl:for-each>
				</td></tr></table>
			</xsl:when>
			<xsl:otherwise>
				<div>There are no available links.</div>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>