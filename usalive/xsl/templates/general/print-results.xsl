<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:b="http://simple/bean/1.0"
	xmlns:x2="http://simple/translate/2.0"
	xmlns:r="http://simple/results/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="x2 a b r">
	<xsl:import href="../general/app-base.xsl" />
	<xsl:output method="xhtml" />
	<xsl:param name="context" />
	
	<xsl:template name="subtitle">
		Browse Data
	</xsl:template>
	
	<xsl:template name="extra-scripts">
	</xsl:template>

	<xsl:template name="contents">
		<xsl:for-each select="/a:base/a:results/r:results">
			<table class="folio">
				<tbody>
					<tr class="headerRow">
						<xsl:choose>
							<xsl:when test="count(r:column) &gt; 0">
								<xsl:for-each select="r:column">
									<th><a data-toggle="sort" data-sort-type="STRING"><xsl:value-of select="@r:name"/></a></th>
								</xsl:for-each>
							</xsl:when>
							<xsl:otherwise>
								<xsl:for-each select="r:row[1]/*">
									<th><a data-toggle="sort" data-sort-type="STRING"><xsl:value-of select="local-name()"/></a></th>
								</xsl:for-each>
							</xsl:otherwise>	
						</xsl:choose>
					</tr>
				</tbody>
				<tbody>
					<xsl:for-each select="r:row">
						<tr>
							<xsl:attribute name="class">
								<xsl:choose>
									<xsl:when test="(position() mod 2) = 0">evenRow</xsl:when>
									<xsl:otherwise>oddRow</xsl:otherwise>
								</xsl:choose>
							</xsl:attribute>
							<xsl:for-each select="*">
								<td data-sort-value="{@r:value}"><xsl:value-of select="."/></td>
							</xsl:for-each>
						</tr>
					</xsl:for-each>
				</tbody>
			</table>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
