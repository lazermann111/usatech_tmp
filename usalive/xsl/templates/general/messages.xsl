<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns="http://www.w3.org/1999/xhtml">
	<xsl:import href="../general/app-base.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:template match="/">
		<xsl:processing-instruction name="output">
			<xsl:text>omit-dtd="true"</xsl:text>
		</xsl:processing-instruction>
		<xsl:call-template name="messages"/>
	</xsl:template>
</xsl:stylesheet>