<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:r="http://simple/results/1.0"
	xmlns:x2="http://simple/translate/2.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="a r x2">
	<xsl:import href="app-base.xsl" />
	<xsl:output method="xhtml"/>

	<xsl:param name="context" />

	<xsl:template name="contents">
		<xsl:copy-of select="/a:base/a:content/*"/>
	</xsl:template>
</xsl:stylesheet>
