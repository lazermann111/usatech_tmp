<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:x2="http://simple/translate/2.0"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:p="http://simple/xml/parameters/1.0"
    xmlns:r="http://simple/results/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:str="http://simple/xml/extensions/java/java.lang.String"
	exclude-result-prefixes="a x2 r p">
	<xsl:import href="app-base.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:template name="contents">
	    <xsl:choose>
            <xsl:when test="count(/a:base/a:tabs/r:results/r:row) &gt; 0">
	    <xsl:variable name="selectedTabId" select="/a:base/a:parameters/p:parameters/p:parameter[@p:name='selectedTab']/@p:value"/>
	    <div class="topMenu">
	    <span class="caption">Build a Report</span>
	    <div class="spacer5"></div>
		<div id="tabContainer">
	    <ul>
			<xsl:for-each select="/a:base/a:tabs/r:results/r:row">
			<li id="tabnav_{r:id}" title="{r:description}" class="topMenuItem">
				    <xsl:if test="$selectedTabId = r:id"><xsl:attribute name="class">topMenuItem topMenuItemSelected</xsl:attribute></xsl:if>
				<a href="{r:href}"><xsl:value-of select="str:toLowerCase(string(r:title))"/></a></li>
			</xsl:for-each>
		</ul>
		</div>
		</div>
		<div id="tabContents">
		  <xsl:call-template name="tab-contents"/>
		</div>
		</xsl:when>
            <xsl:otherwise><xsl:call-template name="tab-contents"/></xsl:otherwise>
        </xsl:choose>
	</xsl:template>
	
	<xsl:template name="tab-contents">Please select a tab</xsl:template>    
</xsl:stylesheet>