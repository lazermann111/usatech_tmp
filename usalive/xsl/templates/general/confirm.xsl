<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:a="http://simple/aggregating/1.0"
    xmlns:p="http://simple/xml/parameters/1.0"
    exclude-result-prefixes="reflect a p">
	<xsl:import href="app-base.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:param name="context" />

	<xsl:template name="contents">
		<div>
			<xsl:attribute name="class">
				<xsl:choose>
					<xsl:when test="reflect:getProperty($context, 'type') = 'error'">errorText</xsl:when>
					<xsl:otherwise>confirmText</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:value-of select="reflect:getProperty($context, 'message')"/></div>
		<xsl:variable name="registryIndex" select="normalize-space(/*/a:parameters/p:parameters/p:parameter[@p:name='registryIndex']/@p:value)"/>
		<xsl:variable name="fragment" select="normalize-space(/*/a:parameters/p:parameters/p:parameter[@p:name='fragment']/@p:value) = 'true'"/>
        <xsl:choose>
            <xsl:when test="$registryIndex &gt; 0 and $fragment">
                <div style="text-align: center; width: 100%;"><input type="button" value="Close" onclick="Dialog.Registry[{$registryIndex - 1}].hide()"/></div>
            </xsl:when>   
            <xsl:when test="not($fragment)">
            <div style="text-align: center; width: 100%;"><input type="button" value="Close" onclick="window.close()"/></div>
            </xsl:when>
        </xsl:choose>
	</xsl:template>
</xsl:stylesheet>