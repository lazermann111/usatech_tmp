<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:r="http://simple/results/1.0"
	xmlns:x2="http://simple/translate/2.0"
	xmlns:b="http://simple/bean/1.0"
	xmlns:p="http://simple/xml/parameters/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	exclude-result-prefixes="a r x2 b p reflect">
	<xsl:import href="../general/app-base.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:param name="context" />

	<xsl:template name="subtitle"><x2:translate key="login-title">Please Log In</x2:translate></xsl:template>

	<xsl:template name="extra-scripts">
		<script type="text/javascript">
			function sendForm(frm, update) {
				new Request.HTML({
					url : frm.action, 
					update : update,
					link : "cancel",
					data : frm
				}).send();
				loginDialog.hide();
			}
		</script>
	</xsl:template>

	<xsl:template name="contents">
		<div id="loginPopup">			
				<xsl:variable name="msg" select="normalize-space(reflect:getProperty($context, 'onloadMessage'))" />
				<xsl:choose>
					<xsl:when test="string-length($msg) &gt; 0"><div class="errorText"><xsl:value-of select="$msg"/></div></xsl:when>
					<xsl:otherwise><p class="loginInstructs"><x2:translate key="login-expired-prompt">Your session has expired, please re-enter your login information.</x2:translate></p></xsl:otherwise>
				</xsl:choose>
			
			<form name="loginForm" method="post" action="login.i" onsubmit="App.updateClientTimestamp(this); sendForm(this, $('loginPopup').getParent()); return false;" autocomplete="off">
				<table class="loginLayout">
				    <tr><th>User:&#160;</th><td><input type="text" name="username" value="{reflect:getProperty($context, 'username')}"/></td></tr>
				    <tr><th>Password:</th><td><input type="password" name="password" value="" autocomplete="off"/></td></tr>
				    <tr class="loginButtonRow"><td colspan="2"><input type="submit" tabindex="0" value="Login"/></td></tr>
				</table>
				<xsl:for-each select="*/a:parameters/p:parameters/p:parameter[@p:name != 'username' and @p:name != 'password' and @p:name != 'loginPromptPage']">
					<input type="hidden" name="{@p:name}" value="{@p:value}"/>
				</xsl:for-each>
				<input type="hidden" name="loginPromptPage" value="login_dialog"/>
			</form>
		</div>
	</xsl:template>
</xsl:stylesheet>