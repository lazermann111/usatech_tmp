<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:x2="http://simple/translate/2.0"
	xmlns:b="http://simple/bean/1.0"
	xmlns:m="http://simple/messages/1.0"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:p="http://simple/xml/parameters/1.0"
	xmlns:prep="http://simple/xml/extensions/java/simple.text.StringUtils"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:req="http://simple/xml/extensions/java/simple.servlet.RequestInfo"
	xmlns:u="http://simple/xml/extensions/java/com.usatech.usalive.servlet.USALiveUtils"
	xmlns:gen="http://simple/xml/extensions/java/com.usatech.report.custom.GenerateUtils"
	xmlns:cmn="http://exslt.org/common"
	xmlns:fr="http://simple/falcon/report/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="p b a x2 m prep u cmn reflect fr req gen">
	<xsl:import href="resource:/simple/falcon/templates/general/html-base.xsl"/>
	<xsl:output method="xhtml"/>
	
	<xsl:variable name="layout">
		<xsl:choose>
			<xsl:when test="not(/fr:report/fr:scene) and normalize-space(reflect:getProperty($context, 'fragment')) = 'true'">fragment</xsl:when>	
			<xsl:when test="not(/fr:report/fr:scene) and normalize-space(reflect:getProperty($context, 'unframed')) = 'true'">unframed</xsl:when>	
			<xsl:when test="not(/fr:report/fr:scene)">full</xsl:when>	
			<xsl:when test="/fr:report/fr:scene/@fr:layout"><xsl:value-of select="normalize-space(/fr:report/fr:scene/@fr:layout)"/></xsl:when>	
			<xsl:otherwise>offline</xsl:otherwise>	
		</xsl:choose>
	</xsl:variable>
	
	<xsl:variable name="user" select="reflect:getProperty($context, 'simple.servlet.ServletUser')"/>

	<xsl:variable name="userId">
		<xsl:choose>
			<xsl:when test="boolean($user)"><xsl:value-of select="normalize-space(reflect:getProperty($user, 'userId'))"/></xsl:when>   
			<xsl:otherwise><xsl:value-of select="normalize-space(/fr:report/fr:executor/@fr:userId)"/></xsl:otherwise>  
		</xsl:choose>
	</xsl:variable>
	
	<xsl:variable name="displayName">
		<xsl:choose>
			<xsl:when test="boolean($user)"><xsl:value-of select="normalize-space(reflect:getProperty($user, 'displayName'))"/></xsl:when>   
			<xsl:when test="/fr:report/fr:executor/@fr:displayName"><xsl:value-of select="normalize-space(/fr:report/fr:executor/@fr:displayName)"/></xsl:when>  
		</xsl:choose>
	</xsl:variable>
	
	<xsl:template name="messages-dialog">
		<xsl:param name="msg" select="normalize-space(/a:base/b:onloadMessage)" />
		<xsl:if test="string-length($msg) &gt; 0">
			<script type="text/javascript">
				window.addEvent('domready', function() { alert("<xsl:value-of select="prep:prepareScript($msg)" />"); });
			</script>
		</xsl:if>
	</xsl:template>

	<xsl:template name="messages">
		<xsl:variable name="message-count" select="count(/a:base/a:messages/m:message)"/>
		<xsl:if test="$message-count &gt; 0">
			<table class="messages">
				<xsl:for-each select="/a:base/a:messages/m:message[@m:type = 'header']">
					<tr class="message-{@m:type}"><td><xsl:copy-of select="."/></td></tr>
				</xsl:for-each>
				<tr><td><ul class="message-list">
					<xsl:for-each select="/a:base/a:messages/m:message[@m:type != 'header' and @m:type != 'footer']">
						<li class="message-{@m:type}"><xsl:copy-of select="."/></li>
					</xsl:for-each>
				</ul></td></tr>
				<xsl:for-each select="/a:base/a:messages/m:message[@m:type = 'footer']">
					<tr class="message-{@m:type}"><td><xsl:copy-of select="."/></td></tr>
				</xsl:for-each>
			</table>
		</xsl:if>
	</xsl:template>
	
	<xsl:template name="report-footer">
		<xsl:if test="$layout = 'unframed' or $layout = 'offline'"><div class="copyright">v.<xsl:value-of select="gen:getApplicationVersion()"/> - USA Technologies, Inc. - &#169; Copyright <xsl:value-of select="gen:getCurrentYear()"/> - Confidential</div></xsl:if>
	</xsl:template>

</xsl:stylesheet>

