<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:b="http://simple/bean/1.0"
	xmlns:x2="http://simple/translate/2.0"
	xmlns:p="http://simple/xml/parameters/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="a b x2 p">
	<xsl:import href="app-base.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:param name="context" />

	<xsl:template name="contents">
		<xsl:variable name="topic" select="normalize-space(a:base/a:parameters/p:parameters/p:parameter[@p:name='helpKey']/@p:value)"/>
		<xsl:variable name="title" select="a:base/b:helpTitle"/>
		<xsl:variable name="content" select="a:base/b:helpContent"/>
		<xsl:choose>
			<xsl:when test="normalize-space($content)">
				<p class="helpTitle"><xsl:value-of select="$title"/></p>
				<p class="helpContent"><xsl:value-of select="$content" disable-output-escaping="yes"/></p>
			</xsl:when>
			<xsl:otherwise>
				<p class="error">I can not find help on this topic (<xsl:value-of select="$topic"/>). Please contact USA Technologies for assistance.</p>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
