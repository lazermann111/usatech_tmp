<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:resp="http://simple/xml/extensions/java/simple.text.StringUtils"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="resp">

	<xsl:template match="*" mode="serialize"><!--
		 -->&lt;<xsl:value-of select="name(.)"/><!--
		 	--><xsl:for-each select="@*"><!--
		 		--><xsl:text> </xsl:text><xsl:value-of select="name(.)"/>=&quot;<xsl:value-of select="resp:prepareScript(string(.))"/>&quot;<!--
		 	--></xsl:for-each><!--
		 --><xsl:choose>
		 	<xsl:when test="count(*|text()) = 0">/&gt;</xsl:when>
		 	<xsl:otherwise>&gt;<xsl:apply-templates mode="serialize" select="*|text()"/><!--
		 	-->&lt;/<xsl:value-of select="name(.)"/>&gt;</xsl:otherwise>
		 </xsl:choose>
	</xsl:template>

	<xsl:template match="/">
		<xsl:apply-templates mode="serialize" select="*"/>
	</xsl:template>

</xsl:stylesheet>

