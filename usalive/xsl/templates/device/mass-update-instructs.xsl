<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:r="http://simple/results/1.0"
	xmlns:x2="http://simple/translate/2.0"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:p="http://simple/xml/parameters/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect r x2 a p">
	<xsl:import href="../selection/search-base.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:param name="context" />

	<xsl:template name="subtitle"><x2:translate key="device.update.title">Update Devices</x2:translate></xsl:template>
	
	<xsl:template name="contents">
		<script type="text/javascript">
			var pickDevicesDialog = new Dialog.Url({
				url: "download_devices.i",
				data: {fragment : true},
				height: 420, 
				width : 610, 
				title : "Select Devices",
				destroyOnClose: false
			});
			
			function downloadDevices(regions) {
				pickDevicesDialog.show();
			}
			
			var bankAcctInstructsDialog = new Dialog.Help({
				helpKey: 'bank-account-instructions',
				height: (Browser.ie ? 205 : Browser.safari || Browser.chrome ? 230 : Browser.opera ? 260 : 240), 
				width : (Browser.ie ? 340 : Browser.safari || Browser.chrome ? 350 : 300), 
				destroyOnClose: false
			});
			
			function showHelpDialog() {
				bankAcctInstructsDialog.show();
			}
			window.addEvent("domready", function() {
			    new Form.Validator.Inline.Mask(document.updateDeviceForm); 
			});
		</script>
		<div class="toggle-heading">
			<div class="caption">Mass Device Update</div>
		</div>
		<!-- <a class="history-link" href="process_request_history.html">Recent Updates</a> -->
		<form name="updateDeviceForm" method="post" enctype="multipart/form-data" action="update_devices_bg_excel.html">
			<xsl:variable name="single-bank-acct" select="normalize-space(/a:base/a:parameters/p:parameters/p:parameter[@p:name='singleBankAcct']/@p:value)='true'"/>
			<xsl:variable name="file-name"><xsl:choose><xsl:when test="$single-bank-acct">sample-cust-update-file</xsl:when><xsl:otherwise>sample-full-update-file</xsl:otherwise></xsl:choose></xsl:variable>
			<ol class="instructions">
				<li>Upload a spreadsheet with the columns specified below in the first row and the data in rows under that.<br/>Supported file types: Excel 2007  and higher (*.xlsx), Excel 97-2003 (*.xls), Comma-Separated Values (*.csv).<br/>Download the settings of your&#160;<a onclick="downloadDevices()" title="Download Current Active Devices">current active devices</a> or a sample&#160;
					<a href="device/{$file-name}.xls" title="Download Sample Update Excel File">Excel file</a> or&#160;
					<a href="device/{$file-name}.csv" title="Download Sample Update Comma-Separated Values File">CSV file</a>.
					You may also&#160;<a href="process_request_history.html" title="View Recent Updates">View Recent Updates</a>.<br/>
					<!-- </x2:translate> -->
					<ul>
						<li>ACTION<span class="required-activate">*</span><span class="required-change">&#135;</span><span class="columnNote">('A' to activate, 'C' to change, or 'I' to ignore)</span></li>
						<li>SERIAL_NUMBER<span class="required-activate">*</span><span class="required-change">&#135;</span></li>
						<xsl:if test="not($single-bank-acct)">
							<li>DEALER_NAME<span class="required-activate">*</span></li>
							<li>BANK_ACCT_ID<span class="columnNote">(Contact USA Technologies, Inc. for this number)</span></li>
						</xsl:if>
						<li>ASSET_NUMBER</li>
						<li>CLIENT</li>
						<li>TELEPHONE</li>
						<li>LOCATION_NAME<span class="required-activate">*</span></li>
						<li>ADDRESS</li>
						<li>CITY<span class="required-activate">*</span></li>
						<li>STATE<span class="required-activate">*</span><span class="columnNote">(Use two-letter abbreviation)</span></li>
						<li>POSTAL<span class="required-activate">*</span></li>
						<li>COUNTRY<span class="columnNote">(Use two-letter abbreviation)</span></li>
						<li>LOCATION_DETAILS</li>
						<li>LOCATION_TYPE
							<span class="columnNote">(One of:
								<xsl:for-each select="/a:base/a:locationTypes/r:results/r:row"><xsl:if test="position() > 1">, </xsl:if><xsl:value-of select="r:locationTypeName"/></xsl:for-each>)
							</span>
						</li>
						<li>TIME_ZONE<span class="required-activate">*</span><span class="columnNote">(Use three-letter abbreviation)</span></li>
						<li>MACHINE_MAKE<span class="required-activate">*</span></li>
						<li>MACHINE_MODEL<span class="required-activate">*</span></li>
						<li>PRODUCT_TYPE<span class="required-activate">*</span>
							<span class="columnNote">(One of:
								<xsl:for-each select="/a:base/a:productTypes/r:results/r:row"><xsl:if test="position() > 1">, </xsl:if><xsl:value-of select="r:productTypeName"/></xsl:for-each>)
							</span></li>
						<li>REGION</li>
						<li>PAY_SCHEDULE<span class="columnNote">('1' for As Accumulated Payment, or '2' for Fill to Fill Payment)</span></li>
						<li>BUSINESS_TYPE
							<xsl:choose>
							<xsl:when test="reflect:getProperty($context, 'simple.servlet.ServletUser.internal') = 'true' or (reflect:getProperty($context, 'simple.servlet.ServletUser.masterUser') !='' and reflect:getProperty($context, 'simple.servlet.ServletUser.masterUser.internal') ='true')">
							<span class="required-activate">*</span>
							</xsl:when>
							<xsl:otherwise>
							- For customer user, this field is read only.
							</xsl:otherwise>
							</xsl:choose>
						    <span class="columnNote">(One of:
                                <xsl:for-each select="/a:base/a:businessTypes/r:results/r:row"><xsl:if test="position() > 1">, </xsl:if><xsl:value-of select="r:businessTypeName"/></xsl:for-each>)
                            </span></li>
						<!-- <li>CURRENCY<span class="columnNote">('1' for United States Dollars)</span></li>	 -->
                        <li>DOING_BUSINESS_AS
                        	<xsl:choose>
                        	<xsl:when test="reflect:getProperty($context, 'simple.servlet.ServletUser.internal') = 'true' or (reflect:getProperty($context, 'simple.servlet.ServletUser.masterUser') !='' and reflect:getProperty($context, 'simple.servlet.ServletUser.masterUser.internal') ='true')">
							</xsl:when>
							<xsl:otherwise>
							- For customer user, this field is read only.
							</xsl:otherwise>
							</xsl:choose>
                        </li>
                        <li>CUSTOMER_SERVICE_PHONE</li>
                        <li>CUSTOMER_SERVICE_EMAIL</li>           
						<li>MOBILE<span class="required-webservice">%</span><span class="columnNote">('Y' if this is a tablet or mobile phone, 'N' otherwise)</span></li>
						<li>ATTENDED<span class="required-webservice">%</span><span class="columnNote">('Y' if there is an attendant or cashier present, 'N' otherwise)</span></li>
						<li>SIGNATURE_CAPTURE<span class="required-webservice">%</span><span class="columnNote">('E' if the signature of the consumer is electronically captured, 'M' if the signature of the customer is manually captured)</span></li>
						<li>COLUMN_MAP_NAME<span class="columnNote">(The name of an existing column map from the system to which you want the device to be connected or a new unique name you want to use when the system creates the map. )</span></li>						
						<li>COLUMN_MAP<span class="columnNote">(A pipe delimited list of mdb number, product description and column value combinations. example 01=Coke=A1|02=Snickers bar=B5. This is only needed if you want to create a new map or update the values of an existing map.)</span></li>
					</ul>
					<p class="required-activate">* - Required for Action 'A'</p>
					<p class="required-change">&#135; - Required for Action 'C'</p>
					<p class="required-webservice">% - Required for Quick Connect</p>
				</li>
				<li><x2:translate key="device.update.prompt.eachrow">Update an existing row or add a new row to your file for each device you wish to update with the appropriate information in each column and save that file.</x2:translate></li>
				<li><x2:translate key="device.update.prompt.updatefile">Click the Browse button and select the file you just saved:</x2:translate><br/>
				<span class="required"><x2:translate key="device.update.prompt.note">Please note: Both Excel 2007 and higher files (*.xlsx) and Excel 97-2003 files (*.xls) are now supported</x2:translate></span><br/>
				<span class="required">Maximum allowed non-empty records in the file is <xsl:value-of select="reflect:getProperty($context,'maxMassDeviceUpdateRows')"/></span><br/>
				<input name="updateFile" type="file" value="{/a:base/a:parameters/p:parameters/p:parameter[@p:name='updateFile']/@p:value}"
								title="Select the file containing the list of devices to update" size="68" data-validators="required"/></li>
				<xsl:if test="$single-bank-acct">
                    <input type="hidden" name="singleBankAcct" value="true"/>
                    <li><x2:translate key="device.update.prompt.dealer">Select the Program</x2:translate><br/>
                        <xsl:variable name="dealerId" select="/a:base/a:parameters/p:parameters/p:parameter[@p:name='dealerId']/@p:value"/>
                        <select name="dealerId" title="Select the Program" data-validators="required">
                            <xsl:for-each select="/a:base/a:dealers/r:results/r:row">
                                <option value="{r:dealerId}">
                                    <xsl:if test="$dealerId = r:dealerId or (not($dealerId) and 18 = r:dealerId)"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
                                    <xsl:value-of select="r:dealerName"/>
                                </option>
                            </xsl:for-each>
                        </select>
                    </li>
                    <li><x2:translate key="device.update.prompt.bankacct">Select the Bank Account to pay</x2:translate><br/>                                        
                        <xsl:variable name="custBankId" select="/a:base/a:parameters/p:parameters/p:parameter[@p:name='custBankId']/@p:value"/>
                        <xsl:variable name="customerId" select="reflect:getProperty($context, 'simple.servlet.ServletUser.customerId')"/>
                        <select name="custBankId" title="Select the Bank Account to pay">
                            <xsl:choose>
                                <xsl:when test="$customerId &gt; 0"><option/></xsl:when>
                                <xsl:otherwise><xsl:attribute name="data-validators">required</xsl:attribute></xsl:otherwise>
                            </xsl:choose> 
                            <xsl:for-each select="/a:base/a:bankAccts/r:results/r:row">
                                <option value="{r:custBankId}">
                                    <xsl:if test="$custBankId = r:custBankId or (string-length(normalize-space($custBankId)) = 0 and position() = 1)"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
                                    <xsl:value-of select="r:bankName"/> - #<xsl:value-of select="r:bankAcctNum"/> (<xsl:value-of select="r:custBankId"/>)</option>
                            </xsl:for-each>
                        </select>
                        <input name="override" type="checkbox"/>Override existing bank account
                        <span class="help-button">
                            <a onclick="showHelpDialog();">&#160;</a>
                        </span>
                    </li>
                </xsl:if>
                <li><x2:translate key="device.update.prompt.notify">Enter an email that will receive notification that the processing is complete (optional):</x2:translate><br/>
                <xsl:variable name="email"><xsl:choose>
                    <xsl:when test="/a:base/a:parameters/p:parameters/p:parameter[@p:name='custBankId']/@p:value"><xsl:value-of select="/a:base/a:parameters/p:parameters/p:parameter[@p:name='custBankId']/@p:value"/></xsl:when>
                    <xsl:otherwise><xsl:value-of select="reflect:getProperty($context, 'simple.servlet.ServletUser.email')"/></xsl:otherwise>
                </xsl:choose></xsl:variable>
                <input type="email" name="notifyEmail" value="{$email}"/></li>
                <li><x2:translate key="device.update.prompt.submit">Click the Submit button and wait while your file is uploaded:</x2:translate><br/>
				<input type="submit" value="Submit"/></li>
			</ol>
		</form>
		<p class="footnote"><a href="mass_device_update_automation_instructs.html">Click here</a> for information and examples on how to automate these updates</p>
	</xsl:template>
</xsl:stylesheet>