<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:x2="http://simple/translate/2.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect x2">
	<xsl:import href="../general/app-base.xsl" />
	<xsl:output method="xhtml"/>

	<xsl:param name="context" />

	<xsl:template name="preprocess">
		<xsl:call-template name="set-user-agent"/>
	</xsl:template>

	<xsl:template name="subtitle"><x2:translate key="main-title">USA Technologies, Inc.</x2:translate></xsl:template>

	<xsl:template name="body">
		<frameset rows="135,*" framespacing="0">
			<frame name="selectionFrame" frameborder="1"
				marginwidth="0" marginheight="0" scrolling="no">
					<xsl:attribute name="src">sony_date_selection.i?<xsl:call-template name="parameters-query"/></xsl:attribute>
			</frame>
			<frame name="reportFrame" frameborder="1"
				marginwidth="0" marginheight="0" scrolling="yes">
				<!--<xsl:attribute name="src">sony_report_frame.i?<xsl:call-template name="parameters-query"/></xsl:attribute> -->
			</frame>
		</frameset>
	</xsl:template>
</xsl:stylesheet>
