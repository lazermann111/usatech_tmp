<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:b="http://simple/bean/1.0"
	xmlns:r="http://simple/results/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect a b r">
	<xsl:import href="../general/app-base.xsl" />
	<xsl:output method="xhtml" />
	<xsl:param name="context" />
	
	<xsl:template name="subtitle">
		Devices without a bank account
	</xsl:template>
	
	<xsl:template name="extra-scripts">
	</xsl:template>
	
	<xsl:template name="contents">
		<div class="nontree"><xsl:copy-of select="/a:base/a:content/*"/></div>
	</xsl:template>
</xsl:stylesheet>