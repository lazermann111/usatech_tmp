<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:r="http://simple/results/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="a r">
	<xsl:output method="xhtml"/>

	<xsl:template name="customer-line-extra"/>
	
	<xsl:template name="customer-tree">
		<ul class="customers tree" id="terminalsTree">
			<xsl:for-each select="a:base/a:customerResults/r:results/r:row">
				<li class="closedTreeNode">
				    <a class="treeImage" onclick="toggleCustomerTree(this, {r:customerId})">&#160;</a>
					<a class="treeLabel" onclick="toggleCustomerTree(this, {r:customerId})"><xsl:value-of select="r:customerName"/></a>
					<xsl:call-template name="customer-line-extra"/>
				    <div class="childTreeDiv" id="regions_of_{r:customerId}">&#160;&#160;Loading...</div>
				</li>
			</xsl:for-each>
		</ul>
	</xsl:template>
</xsl:stylesheet>
