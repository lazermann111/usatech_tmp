<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:x2="http://simple/translate/2.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="x2">
	<xsl:import href="../general/app-base.xsl" />
	<xsl:output method="xhtml"/>

	<xsl:template name="preprocess">
		<xsl:call-template name="set-user-agent"/>
	</xsl:template>

	<xsl:template name="subtitle"><x2:translate key="main-title">USA Technologies, Inc.</x2:translate></xsl:template>

	<xsl:template name="body">
		<frameset rows="225,*" framespacing="0" resizable="false" noresize="noresize" border="0" >
				<frame name="alertSummaryFrame"
					marginwidth="0" marginheight="0" scrolling="yes" frameborder="0">
				 	<xsl:attribute name="src">sony_alerts_summary.i?<xsl:call-template name="parameters-query"/></xsl:attribute>
				</frame>
				<frame name="alertDetailsFrame"
					marginwidth="0" marginheight="0" scrolling="auto" frameborder="1">
				<!--  <xsl:attribute name="src">sony_alerts_details.i?<xsl:call-template name="parameters-query"/></xsl:attribute>-->
				</frame>
		</frameset>
	</xsl:template>
</xsl:stylesheet>
