<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:r="http://simple/results/1.0"
	xmlns:x="http://simple/translate/1.0"
	xmlns:p="http://simple/xml/parameters/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect p r a x">
	<xsl:import href="../general/app-base.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:param name="context" />

	<xsl:template name="subtitle"><x:translate>sony printer alerts summary</x:translate></xsl:template>
	<xsl:template name="extra-scripts">
		<style type="text/css">
			table.alert_results td {
				text-align: center;
				padding: 1px 2px;
			}
			table.printer_info th {
				padding: 1px 2px;
				vertical-align: top;
				text-align: left;
			 	font-weight: normal;
			 	}
			table.printer_info td {
				padding: 1px 2px;
				vertical-align: top;
				font-weight: bold;
			}
			.printer_img{
				position: relative; left: 145px; top: 2px;}
		</style>
		<script type="text/javascript" src="./selection/report-selection-scripts.js"/>
		<script type="text/javascript">
			function openPrinterAlertWindow(){
				var win = window.open("","printer_alert_page","height=650,width=750,toolbar=0,status=0,menubar=0");
				win.focus();
				return win;
			}
		</script>
	</xsl:template>
	<xsl:template name="contents">
		<table class="printer_info">
		 	<tr>
		 		<td>
		 			<xsl:value-of select="reflect:getProperty($context, 'StartDate')" />
					<xsl:text>&#160;&#160;-&#160;&#160;</xsl:text>
					<xsl:value-of select="reflect:getProperty($context, 'EndDate')" />
		 		</td>
		 	 	<td>
		 	 		Alerts
		 	 	</td>
		 		<td class="printer_img">
		 			<a>
						<xsl:attribute name="href">javascript:window.print()</xsl:attribute>
						<img height="15" width="15" border="0" alt="Print">
							<xsl:attribute name="src"><x:translate key="sony-printer-status-image">./images/sony/printerfriendly.gif</x:translate></xsl:attribute>
						</img>
					</a>
				</td>
			</tr>
		</table>
		<table class="alert_results">
			<tr>
				<td>
					<!--<xml><xsl:copy-of select="/a:base/a:terminalResults/r:results/r:group"></xsl:copy-of></xml>-->
					<table>
						<caption></caption>
						<thead>
							<tr class="headerRow">
								<th>
									<x:translate>Status</x:translate>
								</th>
								<th>
									<x:translate>Location</x:translate>
								</th>
								<th>
									<x:translate>Last Call-in</x:translate>
								</th>
								<th>
									<x:translate>#Printer(s)</x:translate>
								</th>
								<th>
									<x:translate>View</x:translate>
								</th>
							</tr>
						</thead>
						<tbody>
							<xsl:for-each select="/a:base/a:terminalResults/r:results/r:group">
								<xsl:variable name="deviceSerialNumber" select="r:row[1]/r:deviceSerialNumber" />
								<xsl:variable name="terminalRows" select="r:row[1]" />
								<xsl:variable name="sonyRows"
									select="/a:base/a:sonyPrinters/r:results/r:row[r:deviceSerialNumber=$deviceSerialNumber]" />
								<tr>
									<xsl:attribute name="class">
										<xsl:choose>
											<xsl:when test="(position() mod 2) = 0">evenRow</xsl:when>
											<xsl:otherwise>oddRow</xsl:otherwise>
										</xsl:choose>
									</xsl:attribute>
									<td>
										<img height="9" width="9" title="Priority {$sonyRows/r:priority}" alt="Priority {$sonyRows/r:priority}">
											<xsl:attribute name="src">
												<xsl:choose>
													<xsl:when test="$sonyRows/r:priority &gt; 10">./images/sony/red_dot.gif</xsl:when>
													<xsl:when test="$sonyRows/r:priority &gt; 0">./images/sony/yellow_dot.gif</xsl:when>
													<xsl:otherwise>./images/sony/green_dot.gif</xsl:otherwise>
												</xsl:choose>
											</xsl:attribute>
										</img>
									</td>
									<td>
										<xsl:value-of select="$terminalRows/r:locationName" />
									</td>
									<td>
										<xsl:value-of select="$sonyRows/r:lastCallIn" />
									</td>
									<td>
										<xsl:value-of select="$sonyRows/r:totalPrinterNum" />
									</td>
									<td>

										<form name="summaryAlertsForm" action="sony_report_frame.i" target="printer_alert_page"
											onsubmit="openPrinterAlertWindow();">
											<input type="hidden" name="terminalId" value="{r:terminalId}" />
											<input type="hidden" name="StartDate"
												value="{/a:base/a:parameters/p:parameters/p:parameter[@p:name='StartDate']/@p:value}" />
											<input type="hidden" name="EndDate"
												value="{/a:base/a:parameters/p:parameters/p:parameter[@p:name='EndDate']/@p:value}" />
											<!--<a>
												<xsl:attribute name="href">
												sony_report_frame.i?terminalId=<xsl:value-of select="r:terminalId" />
												</xsl:attribute>
												Details
												</a> -->
											<input type="submit">
												<xsl:attribute name="value"><x:translate>Details</x:translate></xsl:attribute>
											</input>
										</form>
									</td>
								</tr>
							</xsl:for-each>
						</tbody>
					</table>
				</td>
			</tr>
		</table>
	</xsl:template>
</xsl:stylesheet>