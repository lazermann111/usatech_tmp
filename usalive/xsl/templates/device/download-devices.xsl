<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:r="http://simple/results/1.0"
	xmlns:x2="http://simple/translate/2.0"
	xmlns:b="http://simple/bean/1.0"
	xmlns:p="http://simple/xml/parameters/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="a r x2 b p">
	<xsl:import href="../selection/select-terminal-customers2.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:param name="context" />

	<xsl:template name="subtitle"><x2:translate key="terminal-list-title">Terminal List</x2:translate></xsl:template>

	<xsl:template name="contents">
		<form name="selectDevices" method="post" action="download_devices_excel.i" onsubmit="return validateTerminals();">
			<div class="downloadDevicesOptions">
				<table width="100%">
					<tr><td><fieldset class="" title="File Format">
						<input type="radio" name="fileType" value="download_devices_excel.i" checked="checked" onclick="this.form.action=this.value"/>Excel
						<xsl:text>&#xA0;&#xA0;&#xA0;</xsl:text> 
						<input type="radio" name="fileType" value="download_devices_csv.i" onclick="this.form.action=this.value"/>CSV 
					</fieldset></td><td colspan="4">
						<input id="AllTerminalsButton" type="button" value="Select All"
							onclick="toggleSelectAll(this)"/>
						<input type="submit" value="OK"/>
					</td></tr>                
				</table>
				<hr/>
			</div>
			<xsl:call-template name="select-customers">
				<xsl:with-param name="div-class-name">downloadDevicesLocations</xsl:with-param>
			</xsl:call-template>
		</form>
	</xsl:template>
</xsl:stylesheet>
