<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:r="http://simple/results/1.0"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:x2="http://simple/translate/2.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="r x2 a">
	<xsl:import href="../selection/search-base.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:param name="context" />

	<xsl:template name="subtitle"><x2:translate key="remote-file-upload-title">Remote File Upload</x2:translate></xsl:template>

	<xsl:template name="contents">
		<script defer="defer" type="text/javascript">
    function validateForm(form) {
    	if(String.from(form.elements.namedItem("fileTypeId").value).trim() == "") {
            alert("Please select the type of file.");
            return false;
        }
        if(String.from(form.elements.namedItem("file").value).trim() == "") {
            alert("Please select the file to upload.");
            return false;
        }
        return validateDeviceList(form);
    }
</script>
<form name="uploadForm" action="upload_file_to_devices.i" method="post" enctype="multipart/form-data" onsubmit="return validateForm(this) &amp;&amp; disableSubmit(this);">
<table cellspacing="0" cellpadding="0" class="selectBody">
<caption><x2:translate key="remote-file-upload-title">Remote File Upload</x2:translate></caption>
<tr><td>
<div class="selectSection">
	<div class="sectionTitle">File Information</div>
	<div class="sectionRow">
		<table class="input-rows">
			<tr>
				<th><label for="fileTypeId">File Type</label></th>
				<td>
					<select id="fileTypeId" name="fileTypeId">
		                <option value="">-- Please Select --</option>
						<xsl:for-each select="/a:base/a:fileTypes/r:results/r:row">
							<option value="{r:fileTypeId}">
								<xsl:value-of select="r:fileTypeName"/>
		                    </option>
		                </xsl:for-each>
					</select>
				</td>
			</tr>
			<tr>
				<th><label for="file">File</label></th>
				<td>
					<input size="35" id="file" name="file" type="file"/>
				</td>
			</tr>
		</table>
	</div>
</div>
</td></tr>
<tr><td>
<xsl:call-template name="terminal-search2"/>
</td></tr>
<tr><td>
	<xsl:call-template name="terminal-select">
		<xsl:with-param name="paramName" select="'TerminalId'" />
		<xsl:with-param name="terminalRows" select="a:base/a:terminals/r:results/r:row" />
	</xsl:call-template>
</td>
</tr>
<tr><td align="center">
<div class="submitButtonDiv"><input type="submit" value="Submit File"/></div>
</td>
</tr>
</table>
</form>
</xsl:template>

</xsl:stylesheet>