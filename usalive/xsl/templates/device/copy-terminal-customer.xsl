<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:r="http://simple/results/1.0"
	xmlns:x2="http://simple/translate/2.0"
	xmlns:b="http://simple/bean/1.0"
	xmlns:prep="http://simple/xml/extensions/java/simple.text.StringUtils"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="a r x2 b prep">
	<xsl:import href="./terminal-customers.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:template name="subtitle"><x2:translate key="user-terminal-list-title">Copy Device List</x2:translate></xsl:template>
	
	<xsl:template name="terminals-url">copy_terminal_settings_children.i</xsl:template>
	<xsl:template name="regions-url">terminal_regions.i</xsl:template>
		
	<xsl:template name="extra-scripts-2">
		<script type="text/javascript" defer="defer">
			if(Browser.ie || Browser.opera) {
				window.addEvent("domready", function() {
					var div = $("scrollParent1");
					var container = $("content");
					var updateDimensions = function(event) {
						div.setStyle("height", (container.getSize().y - div.getCoordinates(container).top) + "px");
					};
				
					window.addEvent("resize",updateDimensions);
					window.addEvent("scroll",updateDimensions);
					div.setStyle("height", (container.getSize().y - div.getParent("tr").getPrevious().getCoordinates(container).bottom) + "px");
				});
			}		
		</script>
	</xsl:template>
	
	<xsl:template name="contents">
		<table class="fillVerticalSpace">
			<tr><td><p class="instruct"><x2:translate key="copy-terminal-list-prompt">Please select from which device settings should be copied:</x2:translate></p></td></tr>
			<tr class="fillVerticalSpace"><td colspan="2"><div class="listEditPane" id="scrollParent1"><div class="listEditScroll"><xsl:call-template name="customer-tree"/></div></div></td></tr>
		</table>
	</xsl:template>	
</xsl:stylesheet>
