<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:r="http://simple/results/1.0"
	xmlns:x2="http://simple/translate/2.0"
	xmlns:b="http://simple/bean/1.0"
	xmlns:prep="http://simple/xml/extensions/java/simple.text.StringUtils"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="a r x2 b prep">
	<xsl:import href="./terminal-customers.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:template name="subtitle"><x2:translate key="user-terminal-list-title">User Terminal List</x2:translate></xsl:template>
	
	<xsl:template name="terminals-url">terminal_children_priv.i?privProfileId=<xsl:value-of select="/a:base/b:privProfileId"/></xsl:template>
	<xsl:template name="regions-url">terminal_region_priv.i?privProfileId=<xsl:value-of select="/a:base/b:privProfileId"/></xsl:template>
		
	<xsl:template name="extra-scripts-2">
		<script type="text/javascript" defer="defer">
			var request;
			var privChanges = {};
			window.addEvent("domready", function() {
				request = new Request.HTML({
					url: "update_user_terminal.i",
					method: "post",
					async: false,
					update: $("messageInfo")
				});
				if(Browser.ie || Browser.opera) {
					var div = $("scrollParent1");
					var container = $("content");
					var updateDimensions = function(event) {
						div.setStyle("height", (container.getSize().y - div.getCoordinates(container).top) + "px");
					};
				
					window.addEvent("resize",updateDimensions);
					window.addEvent("scroll",updateDimensions);
					div.setStyle("height", (container.getSize().y - div.getParent("tr").getPrevious().getCoordinates(container).bottom) + "px");
				}
			});

			function submitForm(frm) {
				var query = "privProfileId=<xsl:value-of select="/a:base/b:privProfileId" />&amp;privUsername=<xsl:value-of select="prep:prepareURLPart(string(/a:base/b:privUserName))"/>&amp;fragment=true";
				Object.each(privChanges, function(item, index) {
					if(item == "E")
						query += "&amp;edit=" + index;
					else if(item == "V")
						query += "&amp;view=" + index;
					else if(item == "-")
						query += "&amp;none=" + index;
				});
				request.send({data: query});
				privChanges = {};		
			}
			function handlePrivChange(inp) {
				if(inp.type == "checkbox" &amp;&amp; !inp.disabled) {
					var newPriv = null;
					if(inp.checked) {
						if(inp.name == "edit") {
							newPriv = "E";
							var inpv = $("v" + inp.value);
							inpv.checked = true;
							inpv.disabled = true;
						} else if(inp.name == "view")
							newPriv = "V";
					} else {
						newPriv = "-";
						if(inp.name == "edit") {
							var inpv = $("v" + inp.value);
							inpv.checked = false;
							inpv.disabled = false;
						}
					}
					if(newPriv != null)
						privChanges[inp.value] = newPriv;
				}
			}
			function toggleAllRegions(button, customerId, name) {
				button = $(button);
                var container = $("regions_of_" + customerId);
				var li = button.getParent();
				var complete1 = function() {
			    	var sublis = container.getElements("li");
					var done = 0;
					var complete2 = function() { 
						if(++done == sublis.length)
							toggleSelectAll(button, container, name, name.capitalize()); 
					};
		    	
			    	Array.each(sublis, function(subli) {
						if(!subli.childrenLoaded) {
							var subcontainer = subli.getElement("div");
							var regionId = subcontainer.id.match("terminals_of_" + customerId + "_(\\d+)")[1];
							toggleTree(subli, subcontainer, "<xsl:call-template name="terminals-url"/>", {customerId:customerId, regionId:regionId}, complete2);
						} else
							complete2();
					});
				};
		    	if(!li.childrenLoaded)
					toggleTree(li, container, "<xsl:call-template name="regions-url"/>", {customerId:customerId}, complete1);
				else
					complete1();
			}
			function toggleAllTerminals(button, customerId, regionId, name) {
				button = $(button);
                var container = $("terminals_of_" + customerId + "_" + regionId);
				var li = button.getParent();
				var complete = function() { toggleSelectAll(button, container, name, name.capitalize()); };
		    	if(!li.childrenLoaded)
					toggleTree(li, container, "<xsl:call-template name="terminals-url"/>", {customerId:customerId, regionId:regionId}, complete);
				else
					complete();	
			}
			function toggleSelectAll(button, parent, name, text) {
		        button = $(button);
                if(button.ison) {
		            button.ison = false;
		            button.value = text + " All";
		        } else {
		            button.ison = true;
		            button.value = text + " None";
		        }
		        parent.getElements('input[name="' + name + '"][type=checkbox]').each(function(el) {
		        	if(!el.disabled)
		        		el.checked = button.ison;
		        		el.onclick();
		        		//fireEvent("click", new Event({type: "click", target: el, returnValue: true}))
		        });
		    }
		</script>
	</xsl:template>
	
	<xsl:template name="contents">
		<!-- TODO: Allow selection or de-selection of all in customer or region -->
		<!-- TODO: Exclude those in edit param from those in view param -->
		<span class="caption">User Devices</span>
		<form method="post" name="privForm" action="update_user_terminal.i" onsubmit="submitForm(this); return false;" class="fillVerticalSpace">
			<input type="hidden" name="privProfileId" value="{/a:base/b:privProfileId}"/>
			<input type="hidden" name="privUsername" value="{/a:base/b:privUserName}"/>
			<input type="hidden" name="fragment" value="true"/>
			<table class="fillContent">
			<tr><td colspan="2"><div id="messageInfo"/></td></tr>
			<tr><td><p class="instruct"><x2:translate key="user-terminal-list-prompt">Please select which devices '<xsl:value-of select="/a:base/b:privUserName" />' can view and/or edit:</x2:translate></p></td>
			<td><input type="submit" value="Update"/></td></tr>
			<tr class="fillVerticalSpace"><td colspan="2"><div class="listEditPane" id="scrollParent1"><div class="listEditScroll"><xsl:call-template name="customer-tree"/></div></div></td>
			</tr></table>
		</form>
	</xsl:template>	
	
	<xsl:template name="customer-line-extra">
		<input type="button" value="View All" onclick="toggleAllRegions(this, {r:customerId}, 'view');"/>
		<input type="button" value="Edit All" onclick="toggleAllRegions(this, {r:customerId}, 'edit');"/>
	</xsl:template>
	
</xsl:stylesheet>
