<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:r="http://simple/results/1.0"
	xmlns:x="http://simple/translate/1.0"
	xmlns:p="http://simple/xml/parameters/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect r a x p">
	<xsl:import href="../general/app-base.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:param name="context" />

	<xsl:template name="subtitle"><x:translate>sony detail alert</x:translate></xsl:template>

	<xsl:template name="extra-scripts">
		<style type="text/css">
			table.alert_results td {
				text-align: center;
				padding: 1px 2px;
			}
			table.printer_info th {
				padding: 1px 2px;
				vertical-align: top;
				text-align: left;
			 	font-weight: normal;
			 	}
			table.printer_info td {
				padding: 1px 2px;
				vertical-align: top;
				font-weight: bold;
			}
		</style>
		<script type="text/javascript" src="./selection/report-selection-scripts.js"/>
	</xsl:template>

	<xsl:template name="contents">
		<table class="printer_info">
			<xsl:variable name="locationRow" select="/a:base/a:sonyDetails/r:results/r:row"/>
			<tr>
		 		<th><label for="DateRange">Date Range:</label></th>
		 		<td colspan="3">
		 			<xsl:value-of select="/a:base/a:parameters/p:parameters/p:parameter[@p:name='StartDate']/@p:value" />
					<xsl:text>&#160;-&#160;</xsl:text>
					<xsl:value-of select="/a:base/a:parameters/p:parameters/p:parameter[@p:name='EndDate']/@p:value" />
		 		</td>
		 	</tr>
			<tr><th><label for="location">Location:</label></th>
				<td>
					<xsl:value-of select="$locationRow/r:locationName"/>
				</td>
				<th><label for="printer#">Printer # :</label></th>
				<td>
					<xsl:value-of select="/a:base/a:parameters/p:parameters/p:parameter[@p:name='printerNum']/@p:value"/>
				</td>
		 	</tr>
		 </table>
		 <table>
				<form name="detailsForm">
				 	<table style="width: 100%">
					<tr>
						<td>
						<table class="alert_results">
							<caption/>
							<thead>
								<tr class="headerRow">
									<th><x:translate>Date Time</x:translate></th>
									<th><x:translate>Alarm</x:translate></th>
									<th><x:translate>Code</x:translate></th>
								</tr>
							</thead>
							<tbody>
							<xsl:choose>
								<xsl:when test="string-length(/a:base/a:sonyAlarm/r:results/r:row/r:alarm) &lt;1">
									<tr>
										<td colspan="3">
											<xsl:text>No alarms</xsl:text>
										</td>
									</tr>
								</xsl:when>
								<xsl:otherwise>
									<xsl:for-each select="/a:base/a:sonyAlarm/r:results/r:row">
									<tr>
										<xsl:attribute name="class">
											<xsl:choose>
												<xsl:when test="(position() mod 2) = 0">evenRow</xsl:when>
												<xsl:otherwise>oddRow</xsl:otherwise>
											</xsl:choose>
										</xsl:attribute>
										<td>
											<xsl:value-of select="r:alertTime"/>
										</td>
										<td>
											<xsl:value-of select="r:alarm"/>
										</td>
										<td>
											<xsl:value-of select="r:code"/>
										</td>
									</tr>
									</xsl:for-each>
								</xsl:otherwise>
							</xsl:choose>
							</tbody>
						</table>
					</td></tr>
					</table>
				</form>
		</table>
	</xsl:template>
</xsl:stylesheet>