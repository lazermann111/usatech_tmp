<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:r="http://simple/results/1.0" xmlns:x="http://simple/translate/1.0"
	xmlns:p="http://simple/xml/parameters/1.0"
	xmlns:prep="http://simple/xml/extensions/java/simple.text.StringUtils"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect r a x p prep">
	<xsl:import href="../general/app-base.xsl" />
	<xsl:output method="xhtml" />
	<xsl:param name="context" />
	<xsl:template name="subtitle">
		<x:translate>sony alert summary</x:translate>
	</xsl:template>
	<xsl:template name="extra-scripts">
		<style type="text/css">
			table.alert_results td { text-align: center; padding: 1px 2px; }
			table.printer_info th {
				padding: 1px 2px;
				vertical-align: top;
				text-align: left;
			 	font-weight: normal;
			 	}
			table.printer_info td {
				padding: 1px 2px;
				vertical-align: top;
				font-weight: bold;
			}
			.printer_img{
				position: relative; left: 55px; top: 2px;}
		</style>
		<script type="text/javascript" src="./selection/report-selection-scripts.js" />
		<script type="text/javascript">
	function showAlertDetailWindow(printerNum){
		var win = window.open('sony_printer_alert_detail.i?terminalId=<xsl:value-of select="/a:base/a:parameters/p:parameters/p:parameter[@p:name='terminalId']/@p:value"
			/>&amp;StartDate=<xsl:value-of select="prep:prepareURLPart(string(/a:base/a:parameters/p:parameters/p:parameter[@p:name='StartDate']/@p:value))"
			/>&amp;EndDate=<xsl:value-of select="prep:prepareURLPart(string(/a:base/a:parameters/p:parameters/p:parameter[@p:name='EndDate']/@p:value))"
			/>&amp;printerNum='+printerNum,'alert_details_page','location=no,resizable=yes,height=350,width=400,scrollbars=yes,toolbar=no,menubar=no');
		win.focus();
		return win;
	}
		</script>
	</xsl:template>
	<xsl:template name="contents">
		<xsl:variable name="row" select="/a:base/a:sonyDetails/r:results/r:row" />
		<table class="printer_info">
			<tr>
				<th>Date Range:</th>
				<td>
					<xsl:value-of select="reflect:getProperty($context, 'StartDate')" />
					<xsl:text>&#160;&#160;-&#160;&#160;</xsl:text>
					<xsl:value-of select="reflect:getProperty($context, 'EndDate')" />
				</td>
				<td class="printer_img">
					<a>
						<xsl:attribute name="href">javascript:window.print()</xsl:attribute>
						<img height="15" width="15" border="0" alt="Print">
							<xsl:attribute name="src"><x:translate key="sony-printer-status-image">./images/sony/printerfriendly.gif</x:translate></xsl:attribute>
						</img>
					</a>
				</td>
			</tr>
			<tr>
				<th>Location:</th>
				<td>
					<xsl:value-of select="$row/r:locationName" />
				</td>
				<th>Device:</th>
				<td>
					<xsl:value-of select="$row/r:deviceSerialNumber" />
				</td>
			</tr>
		</table>
		<table>
			<tr>
				<td>
				<form name="alertDetailsForm" action="sony_alerts_details.i" target="alertDetailsFrame">
					<input type="hidden" name="StartDate" value="{/a:base/a:parameters/p:parameters/p:parameter[@p:name='StartDate']/@p:value}" />
					<input type="hidden" name="EndDate" value="{/a:base/a:parameters/p:parameters/p:parameter[@p:name='EndDate']/@p:value}" />
					<input type="hidden" name="deviceSerialNumber" value="{$row/r:deviceSerialNumber}" />
					<input type="hidden" name="terminalId" value="{/a:base/a:parameters/p:parameters/p:parameter[@p:name='terminalId']/@p:value}" />
					<table class="alert_results">
						<caption></caption>
						<thead>
							<tr class="headerRow">
								<th></th>
								<th>
									<x:translate>Status</x:translate>
								</th>
								<th>
									<x:translate>Last Alert Message</x:translate>
								</th>
								<th>
									<x:translate>Last Alert Time</x:translate>
								</th>
								<th>
									<x:translate>Printer</x:translate>
								</th>
								<th>
									<x:translate>View</x:translate>
								</th>
							</tr>
						</thead>
						<tbody>
							<xsl:for-each select="/a:base/a:sonySummary/r:results/r:row">
								<tr>
									<xsl:attribute name="class">
										<xsl:choose>
											<xsl:when test="(position() mod 2) = 0">evenRow</xsl:when>
											<xsl:otherwise>oddRow</xsl:otherwise>
										</xsl:choose>
									</xsl:attribute>
									<td>
										<input type="radio" id="printerNumber" name="printerNumber" value="{r:printerNumber}" onclick="this.form.submit();" />
									</td>
									<td>
										<img height="9" width="9" title="Priority {r:priority}" alt="Priority {r:priority}">
											<xsl:attribute name="src">
												<xsl:choose>
													<xsl:when test="r:priority &gt;= 20">./images/sony/red_dot.gif</xsl:when>
													<xsl:when test="r:priority &gt;= 10">./images/sony/yellow_dot.gif</xsl:when>
													<xsl:otherwise>./images/sony/green_dot.gif</xsl:otherwise>
												</xsl:choose>
											</xsl:attribute>
										</img>
									</td>
									<td>
										<xsl:choose>
											<xsl:when test="string-length(r:lastAlertDisplay) &gt; 0">
												<xsl:value-of select="r:lastAlertDisplay" />
											</xsl:when>
											<xsl:otherwise>
												<xsl:text>-</xsl:text>
											</xsl:otherwise>
										</xsl:choose>
									</td>
									<td>
										<xsl:choose>
											<xsl:when test="string-length(r:lastAlertTime) &gt;0">
												<xsl:value-of select="r:lastAlertTime" />
											</xsl:when>
											<xsl:otherwise>
												<xsl:text>-</xsl:text>
											</xsl:otherwise>
										</xsl:choose>
									</td>
									<td>
										<xsl:choose>
											<xsl:when test="string-length(r:printerLabel) &gt;0">
												<xsl:value-of select="r:printerLabel" /><xsl:text> (#</xsl:text><xsl:value-of select="r:printerNumber" /><xsl:text>)</xsl:text>
											</xsl:when>
											<xsl:otherwise>
												<xsl:text>#</xsl:text><xsl:value-of select="r:printerNumber" />
											</xsl:otherwise>
										</xsl:choose>
									</td>
									<td>
										<input type="button" onclick="showAlertDetailWindow({r:printerNumber})">
												<xsl:attribute name="value"><x:translate>Details</x:translate></xsl:attribute>
										</input>
									</td>
								</tr>
							</xsl:for-each>
						</tbody>
					</table>
				</form>
				</td>
			</tr>
		</table>
		<script type="text/javascript" defer="defer">
		var radioPrinter = document.getElementsByName("printerNumber");
		if(radioPrinter &amp;&amp; radioPrinter.length > 0) radioPrinter[0].click();
		</script>
	</xsl:template>
</xsl:stylesheet>