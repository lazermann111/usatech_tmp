<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:r="http://simple/results/1.0"
	xmlns:x2="http://simple/translate/1.0"
	xmlns:p="http://simple/xml/parameters/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="reflect r x2 a p">
	<xsl:import href="../selection/search-base.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:param name="context" />

	<xsl:template name="subtitle"><x2:translate key="sony-alerts-detail-title">Sony Alerts Detail </x2:translate></xsl:template>
	<xsl:template name="extra-styles">
		<style type="text/css">
			.selectSectionRed {
				background: #FFC0CB ;
				width: 600px;
				margin: 2px;
			}
			.selectSectionYellow {
				background: #FFFFCC ;
				width: 600px;
				margin: 2px;
			}
			table.printer_info th {
				padding: 1px 2px;
				vertical-align: top;
				text-align: left;
			 	font-weight: normal;
			 	}
			table.printer_info td {
				padding: 1px 2px;
				vertical-align: top;
				font-weight: bold;
			}
			.printer_img{
				position: relative; left: 8px; top: 2px;}
		</style>
	</xsl:template>
<xsl:template name="contents">
		<xsl:call-template name="messages-dialog"/>
		<script defer="defer" type="text/javascript">
  		</script>
<table class="selectBody">
	<tr>
		<td>
			<table class="printer_info" width="100%">
				<xsl:variable name="row" select="/a:base/a:sonyDetails/r:results/r:row"/>
				<tr>
					<th><label for="location">Location:</label></th>
					<td ><xsl:value-of select="$row/r:locationName"/></td>
					<th><label for="device">Device:</label></th>
					<td><xsl:value-of select="$row/r:deviceSerialNumber"/></td>
					<th><label for="printer">Printer #:</label></th>
					<td><xsl:value-of select="/a:base/a:parameters/p:parameters/p:parameter[@p:name='printerNumber']/@p:value"/></td>
					<td class="printer_img">
		 				<a>
							<xsl:attribute name="href">javascript:window.print()</xsl:attribute>
							<img height="15" width="15" border="0" alt="Print">
								<xsl:attribute name="src"><x2:translate key="sony-printer-status-image">./images/sony/printerfriendly.gif</x2:translate></xsl:attribute>
							</img>
						</a>
					</td>
				</tr>
			</table>
		</td>
	</tr>

	<tr>
		<td><div class="selectSectionRed"><div class="sectionRow">
			<table>
				<tr valign="top">
					<td>
						<table>
 						<xsl:for-each select="/a:base/a:sonyAlerts/r:results/r:row[r:priority &gt; 10]">
 						<xsl:if test="(position()mod 4)=1">
						 	<tr><xsl:variable name="position" select="position()" />
						 	   <xsl:for-each select=".|following-sibling::node()[position() &lt; 4 and r:priority &gt; 10]">
								 <td width="9">
								 	<img height="9" width="9" alt="Priority {r:priority}">
								 		<xsl:attribute name="src">
								 			<xsl:choose>
								 				<xsl:when test="r:isExist = 'Y'">./images/sony/red_dot.gif</xsl:when>
								 				<xsl:otherwise>./images/sony/green_dot.gif</xsl:otherwise>
								 			</xsl:choose>
								 		</xsl:attribute>
								 	</img>
								 </td>
								 <td>
								 	<xsl:value-of select="r:alertDisplayMsg" />
								 </td>
							   </xsl:for-each>
							</tr>
						</xsl:if>
						</xsl:for-each>
					 	</table>
					</td>
				</tr>
			</table>
			</div></div>
		</td>
	</tr>
	<tr>
	</tr>
	<tr>
		 <td><div class="selectSectionYellow"><div class="sectionRow">
			<table>
				<tr valign="top">
					<td>
						<table>
 						<xsl:for-each select="/a:base/a:sonyAlerts/r:results/r:row[r:priority &lt;= 10]">
 						<xsl:if test="(position()mod 4)=1">
						 	<tr><xsl:variable name="position" select="position()" />
						 	   <xsl:for-each select=".|following-sibling::node()[position() &lt; 4 and r:priority &lt;= 10]">
								 <td width="9">
								 	<img height="9" width="9" alt="Priority {r:priority}">
								 		<xsl:attribute name="src">
								 			<xsl:choose>
								 				<xsl:when test="r:isExist = 'Y'">./images/sony/yellow_dot.gif</xsl:when>
								 				<xsl:otherwise>./images/sony/green_dot.gif</xsl:otherwise>
								 			</xsl:choose>
								 		</xsl:attribute>
								 	</img>
								 </td>
								 <td>
								 	<xsl:value-of select="r:alertDisplayMsg" />
								 </td>
							   </xsl:for-each>
							</tr>
						</xsl:if>
						</xsl:for-each>
					 	</table>
					</td>
				</tr>
			</table>
			</div>
			</div>
		</td>
	</tr>
</table>

 </xsl:template>
</xsl:stylesheet>