<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:b="http://simple/bean/1.0"
	xmlns:p="http://simple/xml/parameters/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="a b p">
	<xsl:import href="../general/app-base.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:template name="body">
		<div style="margin: 2px;">
		<table class="folio">
			<tbody>
				<tr class="headerRow">
					<th colId="0">Retry Queue</th>
					<th colId="1">Original Queue</th>
					<th colId="2">Order</th>
					<th colId="3">Attributes</th>
				</tr>
			</tbody>
			<tbody>
			<xsl:call-template name="recurseMessageRows"/>
			</tbody>
		</table>
		</div>
	</xsl:template>
	
	<xsl:template name="recurseMessageRows">
		<xsl:param name="queueIndex" select="1"/>				
		<xsl:param name="oddEven" select="0"/>				
		
		<xsl:variable name="messages" select="a:base/*[local-name() = concat('retry', $queueIndex, 'Messages')]"/>
		<xsl:choose>
			<xsl:when test="$messages">
				<xsl:call-template name="messageRows">
					<xsl:with-param name="messages" select="$messages"/>
					<xsl:with-param name="retryQueue" select="concat('_RETRY.', $queueIndex)"/>				
					<xsl:with-param name="oddEven" select="$oddEven"/>				
				</xsl:call-template>
				
				<xsl:call-template name="recurseMessageRows">
					<xsl:with-param name="queueIndex" select="1 + $queueIndex"/>
					<xsl:with-param name="oddEven" select="$oddEven + count($messages/*)"/>				
				</xsl:call-template>
			</xsl:when>	
			<xsl:otherwise>
				<xsl:call-template name="messageRows">
					<xsl:with-param name="messages" select="a:base/b:dlqMessages"/>
					<xsl:with-param name="retryQueue" select="'_DLQ'"/>				
					<xsl:with-param name="oddEven" select="$oddEven"/>				
				</xsl:call-template>		
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name="messageRows">
		<xsl:param name="messages"/>
		<xsl:param name="retryQueue"/>				
		<xsl:param name="oddEven" select="0"/>
		<xsl:for-each select="$messages/*">
			<tr>
				<xsl:attribute name="class">
					<xsl:choose>
						<xsl:when test="(($oddEven + position()) mod 2) = 0">evenRow</xsl:when>
						<xsl:otherwise>oddRow</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
				<td style="text-align: left;"><span><xsl:value-of select="$retryQueue"/></span></td>
				<td style="text-align: left;"><span><xsl:value-of select="b:currentStep/b:queueKey"/></span></td>
				<td style="text-align: left;"><span><xsl:value-of select="position()"/></span></td>
				<td style="text-align: left;"><span>
					<xsl:for-each select="b:currentStep/b:attributes/b:entry">
						<xsl:value-of select="b:key"/>=<xsl:value-of select="b:value"/>; 
					</xsl:for-each>
				</span></td>
			</tr>
		</xsl:for-each>
		<xsl:if test="count($messages/*) = 0">
			<tr class="totalRow" style="background-color: #C0C0C0;">
				<td style="text-align: left;" colspan="4"><span>No messages in "<xsl:value-of select="$retryQueue"/>"</span></td>
			</tr>
		</xsl:if>
	</xsl:template>
			
</xsl:stylesheet>
