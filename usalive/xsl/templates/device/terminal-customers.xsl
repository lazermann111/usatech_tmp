<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:r="http://simple/results/1.0"
	xmlns:x2="http://simple/translate/2.0"
	xmlns:b="http://simple/bean/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
	exclude-result-prefixes="a r x2 b reflect">
	<xsl:import href="../general/app-base.xsl"/>
	<xsl:import href="terminal-customers-tree.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:param name="context" />

	<xsl:template name="subtitle"><x2:translate key="terminal-list-title">Device List</x2:translate></xsl:template>

	<xsl:template name="extra-scripts">
		<script type="text/javascript" defer="defer">
		
			function toggleAllCustomer(a){
				var liEl=$(a).getParent();
			
				if(liEl.hasClass("closedTreeNode")) {
		    		if(!liEl.childrenLoaded) {
			    		var data = {fragment:true};
						new Request.HTML({
							url : 'terminal_customers_getall.i', 
							method : "post",
							async : true,
							update : $('customerTreeAll'),
							evalScripts : true,
							link : "cancel",
							onComplete : function() {liEl.childrenLoaded = true; }
						}).send();
		    		}
		    		liEl.addClass("openTreeNode");
		    		liEl.removeClass("closedTreeNode");
		    	} else {
		    		liEl.addClass("closedTreeNode");
		    		liEl.removeClass("openTreeNode");
		   		}
			}
		    function toggleCustomerTree(a, customerId) {
		    	toggleTree($(a).getParent(),"regions_of_"+customerId, "<xsl:call-template name="regions-url"/>", {customerId:customerId});
		    }
		    function toggleRegionTree(a, customerId, regionId) {
		    	toggleTree($(a).getParent(),"terminals_of_"+customerId+"_"+regionId, "<xsl:call-template name="terminals-url"/>", {customerId:customerId, regionId:regionId});
		    }
		    function toggleTree(li, updateId, url, userData, afterwards) {
		    	if(li.hasClass("closedTreeNode")) {
		    		if(!li.childrenLoaded) {
		    		    var updateDiv = $(updateId);
		    		    if(updateDiv) {
				    		var data = {fragment:true};
							if(userData)
								Object.append(data, userData);
							new Request.HTML({
								url : url, 
								method : "post",
								async : true,
								update : updateDiv,
								evalScripts : true,
								link : "cancel",
								onComplete : function() {li.childrenLoaded = true; if(afterwards) afterwards(); },
								data : data
							}).send();
						}
		    		}
		    		li.addClass("openTreeNode");
		    		li.removeClass("closedTreeNode");
		    	} else {
		    		li.addClass("closedTreeNode");
		    		li.removeClass("openTreeNode");
		   		}
		    }
		    function changeRegionName(button, customerId, regionId) {
		        button = $(button);
		        var label = $("label_of_" + customerId + "_" + regionId);
		        var editor = new Element("input", {
		             type: "text",
		             value: (label.getText() == "&lt;NO REGION&gt;" ? "" : label.getText()),
		             maxlength: 50
		        });
		        var update = new Element("input", {
		             type: "button",
		             value: "Update",
		             events: {
		                 click: function() {
		                     new Request.JSON({
	                            url : "update_region.i", 
	                            method : "post",
	                            async : false,
	                            link : "cancel",
	                            onSuccess : function(result) { 
	                                if(result.success) {
	                                    if(editor.getValue() != null &amp;&amp; editor.getValue().trim().length > 0)
	                                        label.setText(editor.getValue());
	                                    else
	                                        label.setText("&lt;NO REGION&gt;");
	                                    editor.dispose();
	                                    update.dispose();
	                                    label.show();
                                        button.show();
	                                } else {
	                                    alert("Region Name Update Failed: " + result.errorMsg);
	                                }
	                            },
	                            data : { customerId: customerId, regionId: regionId, regionName: editor.getValue() }
                              }).send();
		                 }
		             }
		        });
		        label.hide();
                button.hide();
                editor.inject(button, "after");
                update.inject(editor, "after");
		    }
		    var freq;
		    window.addEvent('domready', function() {
			    if(document.searchForm) {
			      var filteredTerminals = $("filteredTerminals");
			      var filterText = $("filterText");
			      var update = $("terminals_of_filter");
                  new Form.Validator.Inline.Mask(document.searchForm);  
			      freq = new Form.Request(document.searchForm, update, {resetForm: false, extraData: { fragment: true }});
			      freq.addEvent("send", function() {
			         update.setHTML("&amp;#160;&amp;#160;Loading..."); 
			         filterText.setText($(document.searchForm.searchText).getValue());
			         if($("terminalsTree")){
                     	$("terminalsTree").hide();
                     }
                     if($("terminalsTreeCustomerAll")){
                     	$("terminalsTreeCustomerAll").hide();
                     }
                     filteredTerminals.show();
                  });
		        } 
	        }); 
	        function doSearch(){
	        	if(document.searchForm.validate()){
		        	document.searchForm.randomNumber.value=Math.floor(Math.random()*1000000000);
		        	freq.send();
	        	}
	        }
	        
	        function removeFilter(button) {
	           var ul = button.getParent("ul");
	           if(ul)
	               ul.hide();
	           if($("terminalsTree")){
	           	$("terminalsTree").show();       
	           }
	           if($("terminalsTreeCustomerAll")){
	           	$("terminalsTreeCustomerAll").show();       
	           }   
	        }
		</script>
		<xsl:call-template name="extra-scripts-2"/>
	</xsl:template>

	<xsl:template name="extra-scripts-2"/>	

	<xsl:template name="terminals-url">terminal_children.i</xsl:template>
	<xsl:template name="regions-url">terminal_regions.i</xsl:template>
	
	<xsl:template name="contents">
		<xsl:variable name="isInternal" select="reflect:getProperty($context, 'simple.servlet.ServletUser.internal')"/>
	    <table class="terminalListLayout"><tr><td><x2:translate key="terminal-list-prompt"><span class="caption">Devices</span><div class="spacer10"></div></x2:translate>
	    <ul class="hide filterTree" id="filteredTerminals">
            <li class="openTreeNode">
                <span class="filterLabel">Devices filtered by "<span id="filterText"></span>"&#160;<button class="cancelFilter" title="Remove Filter" onclick="removeFilter($(this));"><div/></button></span>
                <div class="childTreeDiv" id="terminals_of_filter">&#160;&#160;Loading...</div>
            </li>
        </ul>
	    <xsl:choose>
         <xsl:when test="$isInternal = 'true'" > 
           <ul id="terminalsTreeCustomerAll" class="customers tree">
		    <li class="closedTreeNode">
		    	<a class="treeImage" onclick="toggleAllCustomer(this)">&#160;</a>
		    	<a class="treeLabel" onclick="toggleAllCustomer(this)">All Customers</a>
		    	<div id="customerTreeAll" class="childTreeDiv"/>
		    </li>
		    </ul>
         </xsl:when>
         <xsl:otherwise>
          <xsl:call-template name="customer-tree"/>
         </xsl:otherwise>
       </xsl:choose>
	    </td>
	    <td><div class="terminalSearchBox"><form name="searchForm" action="filter_terminals.i"><fieldset><legend>Search</legend>
          <div>
          <select name="searchField">
	          <option value="serial_nbr">Serial Number</option>
	          <option value="terminal_nbr">Terminal Number</option>
	          <option value="asset_nbr">Asset Number</option>
	          <option value="location_name">Location Name</option>
	          <option value="client">Client</option>
          </select> 
          <input name="searchText" type="text" data-validators="required minLength:3"/></div><div><input type="button" onclick="doSearch();" value="Search"/>
          <input type="hidden" name="randomNumber" value="" /></div>
        </fieldset></form></div></td>
	    </tr></table>	
	</xsl:template>
	
</xsl:stylesheet>
