<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:r="http://simple/results/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="a r">
	<xsl:output method="xhtml"/>

	<xsl:template match="/">
		<ul class="regions tree">
			<xsl:for-each select="a:base/a:regionResults/r:results/r:row[r:subRegionFlag='N']">
			    <xsl:call-template name="region-line"/>
			</xsl:for-each>
		</ul>
	</xsl:template>
	
	<xsl:template name="region-line">
	   <li class="closedTreeNode">
            <a class="treeImage" onclick="toggleRegionTree(this, {r:customerId}, {r:regionId})">&#160;</a>
            <a class="treeLabel" id="label_of_{r:customerId}_{r:regionId}" onclick="toggleRegionTree(this, {r:customerId}, {r:regionId})"><xsl:value-of select="r:regionName"/></a>
            <xsl:call-template name="region-line-extra"/>
            <xsl:if test="r:childCount &gt; 0"><div class="childTreeDiv" id="terminals_of_{r:customerId}_{r:regionId}">Loading...</div></xsl:if>
            <xsl:variable name="parentRegionId" select="r:regionId"/>
            <xsl:if test="count(following-sibling::*[r:parentRegionId = $parentRegionId]) &gt; 0">
            <ul class="childTreeDiv subregions" id="subregions_of_{r:customerId}_{r:regionId}">
               <xsl:for-each select="following-sibling::*[r:parentRegionId = $parentRegionId]">
                   <xsl:call-template name="region-line"/>
               </xsl:for-each>
            </ul>
            </xsl:if>
        </li>
	</xsl:template>
	
	<xsl:template name="region-line-extra"/>
</xsl:stylesheet>
