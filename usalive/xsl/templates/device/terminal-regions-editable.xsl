<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:r="http://simple/results/1.0"
	xmlns:x2="http://simple/translate/2.0"
	xmlns:b="http://simple/bean/1.0"
	xmlns:prep="http://simple/xml/extensions/java/simple.text.StringUtils"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="a r x2 b prep">
	<xsl:import href="./terminal-regions.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:template name="region-line-extra">
	    <xsl:if test="r:editable = 'Y'">
		<button class="edit" title="Edit Region Name..." onclick="changeRegionName($(this), {r:customerId}, {r:regionId});"><div></div></button>
        <a class="details" title="View/Edit Region Details..." href="region_admin.html?regionId={r:regionId}"><div></div></a>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
