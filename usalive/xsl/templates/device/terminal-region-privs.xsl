<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:r="http://simple/results/1.0"
	xmlns:x2="http://simple/translate/2.0"
	xmlns:b="http://simple/bean/1.0"
	xmlns:prep="http://simple/xml/extensions/java/simple.text.StringUtils"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="a r x2 b prep">
	<xsl:import href="./terminal-regions.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:template name="region-line-extra">
		<input type="button" value="View All" onclick="toggleAllTerminals(this, {r:customerId}, {r:regionId}, 'view');"/>
		<input type="button" value="Edit All" onclick="toggleAllTerminals(this, {r:customerId}, {r:regionId}, 'edit');"/>
	</xsl:template>
</xsl:stylesheet>
