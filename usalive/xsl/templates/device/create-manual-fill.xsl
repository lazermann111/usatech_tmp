<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:x2="http://simple/translate/2.0"
	xmlns:p="http://simple/xml/parameters/1.0"
	xmlns:fr="http://simple/falcon/report/1.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="x2 a p fr">
	<xsl:import href="resource:/simple/falcon/templates/report/show-report-base.xsl"/>
	<xsl:output method="xhtml"/>

	<xsl:param name="context" />

	<xsl:template name="subtitle"><x2:translate key="create-fill-title">Create Manual Fill</x2:translate></xsl:template>

	<xsl:template name="extra-scripts">
	</xsl:template>

	<xsl:template name="contents">
		<xsl:call-template name="messages-dialog"/>
		<form name="fillForm" action="create_fill.i" method="post" onsubmit="return validate();">
			<table class="selectBody">
				<tr>
					<td>
						<div class="selectSection">
							<div class="sectionTitle">
								<x2:translate key="select-a-device">Select the device</x2:translate>
							</div>
							<div class="sectionRow">
								<div class="deviceChoice">
								<xsl:for-each select="/a:base/a:terminals/fr:report/fr:folio">
									<xsl:call-template name="folio-content"/>
								</xsl:for-each>
								</div>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td>
	    				<div class="selectSection">
							<div class="sectionTitle">
								<x2:translate key="select-a-device">Enter the Date of the Fill</x2:translate>
							</div>
							<div class="sectionRow">
								<input type="text" name="fillDate" value="{/a:base/a:parameters/p:parameters/p:parameter[@p:name='fillDate']/@p:value}" size="25" title="Enter the date of the fill" label="Fill Date" data-toggle="calendar" data-format="%m/%d/%Y"/>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</form>
		<xsl:variable name="selectedTerminalId" select="/a:base/a:parameters/p:parameters/p:parameter[@p:name='terminalId']/@p:value"/>
		<xsl:if test="$selectedTerminalId">
		<script defer="defer" type="text/javascript">
			var terms = document.getElementsByName["terminaldId"];
			for(var i = 0; i &lt; terms.length; i++) {
				if(terms[i].value == <xsl:value-of select="$selectedTerminalId"/>) {
					terms.checked = true;
					break;
				}
			}
		</script>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>