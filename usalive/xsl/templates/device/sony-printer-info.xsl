<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:r="http://simple/results/1.0"
	xmlns:a="http://simple/aggregating/1.0"
	xmlns:b="http://simple/bean/1.0"
	xmlns:x2="http://simple/translate/2.0"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="r x2 a b">
	<xsl:import href="../selection/search-base.xsl" />
	<xsl:output method="xhtml" />

	<xsl:param name="context" />

	<xsl:template name="subtitle">
		<x2:translate key="remote-file-upload-title">Prints Remaining</x2:translate>
	</xsl:template>

	<xsl:template name="contents">
		<table class="spaceAround"><tr><td>
			<table class="results">
				<caption><xsl:call-template name="title"/></caption>
				<tr class="headerRow">
					<th>Location</th>
					<th>Device</th>
					<xsl:for-each select="/a:base/b:printerLabels/b:printerLabel">
						<xsl:sort data-type="text" select="."/>
						<th><xsl:value-of select="."/></th>
					</xsl:for-each>
				</tr>
				<xsl:for-each select="/a:base/a:terminals/r:results/r:group/r:row">
					<tr>
						<xsl:attribute name="class">
							<xsl:choose>
								<xsl:when test="(position() mod 2) = 0">evenRow</xsl:when>
								<xsl:otherwise>oddRow</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<td><xsl:value-of select="r:locationName"/></td>
						<td><xsl:value-of select="r:deviceSerialNumber"/></td>
						<xsl:variable name="deviceSerialNumber" select="r:deviceSerialNumber"/>
						<xsl:for-each select="/a:base/b:printerLabels/b:printerLabel">
							<xsl:sort data-type="text" select="."/>
							<td style="text-align: center;">
								<xsl:variable name="printerLabel" select="."/>
								<xsl:variable name="printerRows" select="/a:base/a:printerInfo/r:results/r:row[r:deviceSerialNumber = $deviceSerialNumber and r:printerLabel = $printerLabel]"/>
								<xsl:for-each select="$printerRows">
									<xsl:if test="position() &gt; 1"> / </xsl:if>
									<span title="Prints Remaining for Printer #{r:printerNumber}"><xsl:value-of select="r:printsRemaining"/></span>
								</xsl:for-each></td>
						</xsl:for-each>
					</tr>
				</xsl:for-each>
			</table>
			<div class="copyright"><x2:translate key="report-copyright"></x2:translate></div>
		</td></tr></table>
	</xsl:template>

</xsl:stylesheet>
