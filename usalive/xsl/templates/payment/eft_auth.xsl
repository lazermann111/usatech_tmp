<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fo="http://www.w3.org/1999/XSL/Format" 
    xmlns:r="http://simple/results/1.0"
    xmlns:a="http://simple/aggregating/1.0"
    xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
    xmlns:req="http://simple/xml/extensions/java/simple.servlet.RequestInfo"
    xmlns:regex="http://simple/xml/extensions/java/simple.text.RegexUtils"
    exclude-result-prefixes="r a reflect req regex">
    <xsl:output method="xml"/>
    <xsl:param name="context" />
    <xsl:variable name="baseUrl" select="string(regex:substitute('[^/]*$',req:getBaseUrl($context),'',''))"/>
    
	<xsl:template match="/">
<fo:root language="en" hyphenate="true" country="US">
    <fo:layout-master-set>
        <fo:simple-page-master master-name="master-pages-for-all" page-height="11in" page-width="8.5in" margin-top="0.2in" margin-right="0.4in" margin-bottom="0.2in" margin-left="0.4in">
            <!-- FOP 0.20.5 does not seem to obey margins here -->
            <fo:region-before extent="0in"/>
            <fo:region-body/>
            <fo:region-after extent="0in"/>
        </fo:simple-page-master>
        <fo:page-sequence-master master-name="sequence-of-pages">
            <fo:repeatable-page-master-reference master-reference="master-pages-for-all"/>
        </fo:page-sequence-master>
    </fo:layout-master-set>
    <fo:page-sequence master-reference="sequence-of-pages">
        <fo:title>Authorization EFT and Substitute W-9</fo:title>
        <fo:static-content flow-name="xsl-region-before"/>
        <fo:static-content flow-name="xsl-region-after"/>
        <!-- <fo:page-sequence master-reference="document" initial-page-number="1" format="1"> -->
        <fo:flow flow-name="xsl-region-body">
            <fo:block unicode-bidi="embed">
                <fo:table table-layout="fixed" font-size="8.5pt" font-family="Tahoma,Trebuchet MS,Arial,sans-serif" color="black" text-align="left" display-align="after">
                    <!-- <fo:table-column column-width="235.5pt" /> <fo:table-column column-width="433.5pt" /> -->
                    <fo:table-column column-width="210px"/>
                    <fo:table-column column-width="200px"/>
                    <fo:table-column column-width="150px"/>
                    <fo:table-body>
                        <fo:table-row>
                            <fo:table-cell>
                                <fo:block>
                                    <fo:external-graphic  src="{$baseUrl}/images/usa_tech_logo.gif"/>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding="1pt" font-size="11pt" font-weight="bold" text-align="center" number-columns-spanned="2">
                                <fo:block>AUTHORIZATION FOR ELECTRONIC FUNDS TRANSFER AND SUBSTITUTE W-9 CERTIFICATION</fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" color="black" border-bottom="1pt solid black"/>
                            <fo:table-cell padding="1pt" color="black" border-bottom="1pt solid black" number-columns-spanned="2"/>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" padding-bottom="0pt" font-size="11pt" font-weight="bold" border="1pt solid black" display-align="center">
                                <fo:block>TRADING PARTNER NAME</fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black" number-columns-spanned="2" font-size="10pt" >
                                <fo:block color="#0000C0"><xsl:value-of select="/a:base/a:info/r:results/r:row[1]/r:customerName"/></fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black"/>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black" number-columns-spanned="2"/>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" background-color="#FFFF00" padding-bottom="0pt" border="1pt solid black">
                                <fo:block>REMIT TO ADDRESS</fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding="1pt" background-color="#FFFF00" padding-bottom="0pt" border="1pt solid black" number-columns-spanned="2">
                                <fo:block>This should be the remit to address shown on your invoices</fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black">
                                <fo:block>Street Address / PO Box:</fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black" number-columns-spanned="2">
                                <fo:block color="#0000C0"><xsl:value-of select="/a:base/a:info/r:results/r:row[1]/r:bankBillingAddress1"/><xsl:choose>
							    <xsl:when test="string-length(/a:base/a:info/r:results/r:row[1]/r:bankBillingAddress2) &gt; 0">
							        ,<xsl:value-of select="/a:base/a:info/r:results/r:row[1]/r:bankBillingAddress2" />
							    </xsl:when>
							</xsl:choose></fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black">
                                <fo:block>City, State, Zip:</fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black" number-columns-spanned="2">
                                <fo:block color="#0000C0"><xsl:value-of select="/a:base/a:info/r:results/r:row[1]/r:bankBillingCity"/>,&#160;
                                <xsl:value-of select="/a:base/a:info/r:results/r:row[1]/r:bankBillingState"/>&#160;
                                <xsl:value-of select="/a:base/a:info/r:results/r:row[1]/r:bankBillingZip"/></fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" padding-top="3px" border-top="1pt solid black" border-left="1pt solid black" border-bottom="1pt solid black"/>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black" number-columns-spanned="2"/>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" background-color="#FFFF00" padding-bottom="0pt" border="1pt solid black">
                                <fo:block>BANKING INFORMATION</fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding="1pt" background-color="#FFFF00" padding-bottom="0pt" border="1pt solid black" number-columns-spanned="2">
                                <fo:block>This must be a U.S. Domestic Bank to use this form</fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black">
                                <fo:block>Name of Bank:</fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding="1pt" color="black" border="1pt solid black" number-columns-spanned="2">
                                <fo:block color="#0000C0"><xsl:value-of select="/a:base/a:info/r:results/r:row[1]/r:bankName"/></fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" color="black" border="1pt solid black">
                                <fo:block>Street Address / PO Box:</fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding="1pt" color="black" border="1pt solid black" number-columns-spanned="2">
                                <fo:block color="#0000C0"><xsl:value-of select="/a:base/a:info/r:results/r:row[1]/r:bankAddress1"/></fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black">
                                <fo:block>City, State, Zip:</fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black" number-columns-spanned="2">
                                <fo:block color="#0000C0"><xsl:value-of select="/a:base/a:info/r:results/r:row[1]/r:bankCity"/>,&#160;
                                <xsl:value-of select="/a:base/a:info/r:results/r:row[1]/r:bankState"/>&#160;
                                <xsl:value-of select="/a:base/a:info/r:results/r:row[1]/r:bankZip"/></fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black">
                                <fo:block>Company Name or Individual's Name on Bank Account: (Must read exactly as listed on bank statement)</fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black" number-columns-spanned="2">
                                <fo:block color="#0000C0"><xsl:value-of select="/a:base/a:info/r:results/r:row[1]/r:accountTitle"/></fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black">
                                <fo:block>Checking or Savings Account? (Please check appropriate box)</fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black" display-align="center" number-columns-spanned="2">
                                <fo:block><fo:table>
                                        <fo:table-column column-width="70pt"/>
                                        <fo:table-column column-width="25pt"/>
                                        <fo:table-column column-width="73pt"/>
                                        <fo:table-column column-width="25pt"/>
                                        <fo:table-body><fo:table-row>
                                            <fo:table-cell><fo:block>Checking Account</fo:block></fo:table-cell>
                                            <fo:table-cell color="#0000C0" border-bottom="1px solid #808080" text-align="center"><fo:block><xsl:if test="'C' = /a:base/a:info/r:results/r:row[1]/r:accountType">X</xsl:if></fo:block></fo:table-cell>
                                            <fo:table-cell text-align="right" padding-right="2px"><fo:block>Savings Account</fo:block></fo:table-cell>
                                            <fo:table-cell border-bottom="1px solid #808080" text-align="center"><fo:block><xsl:if test="'S' = /a:base/a:info/r:results/r:row[1]/r:accountType">X</xsl:if></fo:block></fo:table-cell>                                            
                                        </fo:table-row></fo:table-body></fo:table>
                               </fo:block>  
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black"/>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black" number-columns-spanned="2"/>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" background-color="#FFFF00" padding-bottom="0pt" border="1pt solid black">
                                <fo:block>EFT INFORMATION</fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding="1pt" background-color="#FFFF00" padding-bottom="0pt" border="1pt solid black" number-columns-spanned="2">
                                <fo:block>Obtain this information directly from your bank</fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black">
                                <fo:block>Bank ABA/Routing Number for ACH/EFT Payments: (may be different than ABA for checks)</fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black" display-align="center" number-columns-spanned="2">
                                <fo:block><xsl:call-template name="blankOrValueTable">
					                <xsl:with-param name="value" select="/a:base/a:info/r:results/r:row[1]/r:routingNumber"/>
					                <xsl:with-param name="times" select="9"/>
					                <xsl:with-param name="extra-text"> (must be 9-digit number)</xsl:with-param>          
					            </xsl:call-template></fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black">
                                <fo:block>Bank Account Number:</fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black" number-columns-spanned="2">
                                <fo:block color="#0000C0"><xsl:value-of select="/a:base/a:info/r:results/r:row[1]/r:accountNumber"/></fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <xsl:choose>
						<xsl:when test="/a:base/a:info/r:results/r:row[1]/r:bankCountryCd = 'CA'">
						<fo:table-row>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black">
                                <fo:block>Bank SWIFT Code:</fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black" number-columns-spanned="2">
                                <fo:block color="#0000C0"><xsl:value-of select="/a:base/a:info/r:results/r:row[1]/r:swiftCode"/></fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        </xsl:when>
						<xsl:otherwise></xsl:otherwise>            
                        </xsl:choose>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black"/>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black" number-columns-spanned="2"/>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" background-color="#FFFF00" padding-bottom="0pt" border="1pt solid black">
                                <fo:block>YOUR BANK CONTACT</fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding="1pt" background-color="#FFFF00" padding-bottom="0pt" border="1pt solid black" number-columns-spanned="2">
                                <fo:block>Person at your bank who we can contact to verify Banking Information</fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black">
                                <fo:block>Contact Name / Title:</fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black" border-right="none">
                                <fo:block>Name: <fo:inline color="#0000C0"><xsl:value-of select="/a:base/a:info/r:results/r:row[1]/r:contactName"/></fo:inline></fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black" border-left="none">
                                <fo:block>Title: <fo:inline color="#0000C0"><xsl:value-of select="/a:base/a:info/r:results/r:row[1]/r:contactTitle"/></fo:inline></fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black">
                                <fo:block>Contact Phone / Fax:</fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black" border-right="none">
                                <fo:block>Phone: <fo:inline color="#0000C0"><xsl:value-of select="/a:base/a:info/r:results/r:row[1]/r:contactTelephone"/></fo:inline></fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black" border-left="none">
                                <fo:block>Fax: <fo:inline color="#0000C0"><xsl:value-of select="/a:base/a:info/r:results/r:row[1]/r:contactFax"/></fo:inline></fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" background-color="#FFFF00" font-weight="bold" padding-bottom="0pt" border="1pt solid black">
                                <fo:block>IRS form W-9 Requirement Part I</fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding="1pt" background-color="#FFFF00" padding-bottom="0pt" border="1pt solid black" number-columns-spanned="2">
                                <fo:block>Taxpayer Identification Number (TIN)</fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" font-size="8pt" font-weight="bold" font-style="italic" border="1pt solid black" number-columns-spanned="3">
                                <fo:block>Enter your TIN in the appropriate box. The TIN provided must match the name given on the "Trading Partner Name" line to avoid backup Withholding. For Individuals, this should be your
                                    social security number (SSN)</fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" padding-top="3px" padding-bottom="3px"  border="1pt solid black" display-align="center" number-columns-spanned="3">
                                <fo:block>
                                <fo:table>
                                <fo:table-column column-width="100pt"/>
                                <fo:table-column column-width="3pt"/>                    
						        <xsl:call-template name="blankOrValueTableColumn">
						            <xsl:with-param name="column-width" select="'15pt'"/>
						            <xsl:with-param name="times" select="2"/>          
						        </xsl:call-template>
						        <fo:table-column column-width="6pt"/>                                                   
						        <xsl:call-template name="blankOrValueTableColumn">
                                    <xsl:with-param name="column-width" select="'15pt'"/>
                                    <xsl:with-param name="times" select="7"/>          
                                </xsl:call-template>
                                <fo:table-column column-width="3pt"/>                    
                                <fo:table-column column-width="90pt"/>
                                <fo:table-column column-width="3pt"/>                    
                                <xsl:call-template name="blankOrValueTableColumn">
                                    <xsl:with-param name="column-width" select="'15pt'"/>
                                    <xsl:with-param name="times" select="3"/>          
                                </xsl:call-template>
                                <fo:table-column column-width="6pt"/>                                                   
                                <xsl:call-template name="blankOrValueTableColumn">
                                    <xsl:with-param name="column-width" select="'15pt'"/>
                                    <xsl:with-param name="times" select="2"/>          
                                </xsl:call-template>
                                <fo:table-column column-width="6pt"/>                                                   
                                <xsl:call-template name="blankOrValueTableColumn">
                                    <xsl:with-param name="column-width" select="'15pt'"/>
                                    <xsl:with-param name="times" select="4"/>          
                                </xsl:call-template>                                
                                <fo:table-body><fo:table-row>
                                <fo:table-cell><fo:block>Company Tax ID Number:</fo:block></fo:table-cell>
                                <fo:table-cell/>
						        <xsl:call-template name="blankOrValueTableCell">
						            <xsl:with-param name="value" select="/a:base/a:info/r:results/r:row[1]/r:bankTaxId"/>
						            <xsl:with-param name="times" select="2"/>          
						        </xsl:call-template>
						        <fo:table-cell text-align="center"><fo:block>-</fo:block></fo:table-cell>
                                <xsl:call-template name="blankOrValueTableCell">
                                    <xsl:with-param name="value" select="substring(/a:base/a:info/r:results/r:row[1]/r:bankTaxId, 3)"/>
                                    <xsl:with-param name="times" select="7"/>          
                                </xsl:call-template>
                                <fo:table-cell/>
                                <fo:table-cell><fo:block>OR SSN for Individuals:</fo:block></fo:table-cell>
                                <fo:table-cell/>
                                <xsl:call-template name="blankOrValueTableCell">
                                    <xsl:with-param name="value" select="''"/>
                                    <xsl:with-param name="times" select="3"/>          
                                </xsl:call-template>
                                <fo:table-cell text-align="center"><fo:block>-</fo:block></fo:table-cell>
                                <xsl:call-template name="blankOrValueTableCell">
                                    <xsl:with-param name="value" select="''"/>
                                    <xsl:with-param name="times" select="2"/>          
                                </xsl:call-template>
                                <fo:table-cell text-align="center"><fo:block>-</fo:block></fo:table-cell>
                                <xsl:call-template name="blankOrValueTableCell">
                                    <xsl:with-param name="value" select="''"/>
                                    <xsl:with-param name="times" select="4"/>  
                                </xsl:call-template>
                                </fo:table-row></fo:table-body>
						        </fo:table>
						        </fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black" display-align="center">
                                <fo:block>Check the appropriate line for federal tax</fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black" display-align="center" number-columns-spanned="2">
                                <fo:block>___ Individual ___ C Corporation ___S Corporation ___ P Partnership ___ Trust/Estate</fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black">
                                <fo:block>If Limited Liability Company, enter Tax classification</fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black" display-align="center" number-columns-spanned="2">
                                <fo:block>___Corporation ___Corporation ___Partnership ___ N/A</fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black"/>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black" number-columns-spanned="2"/>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" background-color="#FFFF00" padding-bottom="0pt" font-weight="bold" border="1pt solid black">
                                <fo:block>IRS form W-9 Requirement Part II</fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding="1pt" background-color="#FFFF00" padding-bottom="0pt" font-weight="bold" border="1pt solid black" number-columns-spanned="2">
                                <fo:block>Certification</fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" font-size="9pt" border="1pt solid black" display-align="before" number-columns-spanned="3">
                                <fo:block>Under penalties of perjury, I certify that:</fo:block>
                                <fo:block>1. The number shown on this form is my correct taxpayer identification number (or I am waiting for a number to be issued to me), and</fo:block>
                                <fo:block>2. I am not subject to backup withholding because: (a) I am exempt from backup withholding, or (b) I have not been notified by the Internal Revenue Service that I am subject to
                                    backup withholding as a result of a failure to report all interest or dividends, or (c) the IRS has notified me that I am no longer subject to backup withholding, and</fo:block>
                                <fo:block>3. I am a U.S. person.</fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black"/>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black" number-columns-spanned="2"/>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" padding-bottom="0pt" background-color="#FFFF00" font-size="pt" font-weight="bold" border="1pt solid black" display-align="center">
                                <fo:block>AUTHORIZATION AND CERTIFICATION</fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding="1pt" background-color="#FFFF00" padding-bottom="0pt" font-weight="bold" font-style="italic" border="1pt solid black" number-columns-spanned="2">
                                <fo:block>The Internal Revenue Service does not require your consent to any provision of this document other than the certifications required to avoid backup withholding.</fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black">
                                <fo:block>Authorized Signature: (Must be Signed)</fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding="1pt" padding-top="3px" color="black" border="1pt solid black" border-right="none">
                                <fo:block>Signature:</fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding="1pt" padding-top="3px" color="black" border="1pt solid black" border-left="none">
                                <fo:block>Date:</fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black">
                                <fo:block>Name / Title:</fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black" border-right="none">
                                <fo:block>Name: <fo:inline color="#0000C0"><xsl:value-of select="reflect:getProperty($context, 'bankContact.userProperties.fullName')"/></fo:inline></fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black" border-left="none">
                                <fo:block>Title: <fo:inline color="#0000C0"></fo:inline></fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black">
                                <fo:block>Phone / Fax:</fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black" border-right="none">
                                <fo:block>Phone: <fo:inline color="#0000C0"><xsl:value-of select="/a:base/a:info/r:results/r:row[1]/r:customerTelephone"/></fo:inline></fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black" border-left="none">
                                <fo:block>Fax: <fo:inline color="#0000C0"><xsl:value-of select="/a:base/a:info/r:results/r:row[1]/r:customerFax"/></fo:inline></fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black">
                                <fo:block>E-mail:</fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding="1pt" padding-top="3px" border="1pt solid black" number-columns-spanned="2">
                                <fo:block color="#0000C0"><xsl:value-of select="reflect:getProperty($context, 'bankContact.email')"/></fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" font-weight="bold" text-align="center" number-columns-spanned="3">
                                <fo:block>A copy of a voided check is recommended to validate your EFT banking information.</fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" text-align="center" number-columns-spanned="3">
                                <fo:block>Please fax this form along with a copy of a voided check to (610) 989-9695.</fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" font-weight="bold" number-columns-spanned="3">
                                <fo:block>Direct Credit Authorization:</fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" font-size="7pt" number-columns-spanned="3">
                                <fo:block>You hereby authorize USA Technologies, Inc. and its designated financial agents to initiate credit entries to the account listed above in connection with agreed upon Electronic Data
                                    Interchange (EDI) transactions between our companies. You agree that such transactions will be governed by the National Automated Clearing House Association (ACH) rules. This authority is to
                                    remain in effect until USA Technologies, Inc. has received written notification of termination in such time and such manner as to afford USA Technologies, Inc. a reasonable opportunity to act
                                    on it. You also authorize the Bank listed above to verify your account information as necessary to establish the EFT. IN NO EVENT SHALL USA TECHNOLOGIES, INC. BE LIABLE FOR ANY SPECIAL,
                                    INCIDENTAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES AS A RESULT OF THE DAILY, OMISSION OR ERROR OF AN ELECTRONIC CREDIT ENTRY, EVEN IF USA TECHNOLOGIES, INC. HAS BEEN ADVISED OF THE POSSIBILITY OF
                                    SUCH DAMAGES. This agreement shall be governed by the laws of the State of Pennsylvania.</fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" font-weight="bold">
                                <fo:block>Direct Debit Authorization:</fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding="1pt" text-align="left" number-columns-spanned="2"/>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" font-size="7pt" number-columns-spanned="3">
                                <fo:block>You hereby authorize USA Technologies, Inc. and its designated financial agents to initiate an ACH debit (automatic withdrawal) entry to the account listed above for payment of
                                    monthly network service fees and other fees agreed to in your License Agreement, and to debit the entry to the company's account (as listed above). This authorization is to remain in full
                                    force and effect until USA Technologies, Inc. receives notification from an official or authorized agent of your company of the termination. To revoke this payment authorization, you must
                                    contact USA Technologies, Inc. at (610) 989-0340 no later than 2 business days prior to the payment (settlement) date. You also authorize USA Technologies, Inc. and the financial institutions
                                    involved in the processing of the electronic payment of fees and charges to receive confidential information necessary to answer inquiries and resolve issues related to the payment.</fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="1pt" font-size="7pt" border-bottom="1pt solid black" number-columns-spanned="3">
                                <fo:block>On the agreed upon day of your weekly EFT payment, your account must have a net balance of $25.00 or more to receive an EFT, otherwise, all funds roll over into the next weekly EFT
                                    pay period and continuing until at which time your account has a net balance of $25.00 or greater.</fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                    </fo:table-body>
                </fo:table>
            </fo:block>
        </fo:flow>
    </fo:page-sequence>
</fo:root>
	</xsl:template>
	
	<xsl:template name="blankOrValueTable">
        <xsl:param name="value"/>
        <xsl:param name="times"/>
        <xsl:param name="extra-text"/>
        <fo:table>
        <xsl:call-template name="blankOrValueTableColumn">
            <xsl:with-param name="column-width" select="'15pt'"/>
            <xsl:with-param name="times" select="$times"/>          
        </xsl:call-template>
        <xsl:if test="string-length(normalize-space($extra-text)) &gt; 0">
            <fo:table-column column-width="3pt"/>        
            <fo:table-column column-width="*"/> 
        </xsl:if>
        <fo:table-body><fo:table-row>
        <xsl:call-template name="blankOrValueTableCell">
            <xsl:with-param name="value" select="$value"/>
            <xsl:with-param name="times" select="$times"/>          
        </xsl:call-template>
        <xsl:if test="string-length(normalize-space($extra-text)) &gt; 0">
            <fo:table-cell/>
            <fo:table-cell><fo:block><xsl:value-of select="$extra-text"/></fo:block></fo:table-cell>
        </xsl:if>
        </fo:table-row></fo:table-body>
        </fo:table>
    </xsl:template>
    
    <xsl:template name="blankOrValueTableColumn">
        <xsl:param name="column-width" select="'15pt'"/>
        <xsl:param name="times"/>
        <fo:table-column column-width="{$column-width}"/>        
        <xsl:if test="$times &gt; 1">
            <fo:table-column column-width="3pt"/>        
            <xsl:call-template name="blankOrValueTableColumn">
                <xsl:with-param name="column-width" select="$column-width"/>
                <xsl:with-param name="times" select="$times - 1"/>          
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
                            
	<xsl:template name="blankOrValueTableCell">
	    <xsl:param name="value"/>
	    <xsl:param name="times" select="1"/>
	    <fo:table-cell color="#0000C0" border-bottom="1px solid #808080" text-align="center"><fo:block><xsl:value-of select="substring($value, 1 , 1)"/></fo:block></fo:table-cell>
	    <xsl:if test="$times &gt; 1">
	        <fo:table-cell/>
			<xsl:call-template name="blankOrValueTableCell">
			    <xsl:with-param name="value" select="substring($value, 2)"/>
			    <xsl:with-param name="times" select="$times - 1"/>          
			</xsl:call-template>
        </xsl:if>
	</xsl:template>
</xsl:stylesheet>