<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:cnv="http://simple/xml/extensions/java/simple.bean.ConvertUtils"
	xmlns:fr="http://simple/falcon/report/1.0"
	xmlns:reflect="http://simple/xml/extensions/java/simple.bean.ReflectionUtils"
    xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="fr cnv reflect">
	<xsl:import href="../../simple/falcon/templates/report/show-report-base.xsl"/>
	<xsl:output method="xhtml"/>
	
	<xsl:param name="context" />
    <xsl:variable name="folioId" select="/fr:report/fr:folio[1]/@fr:folioId"/>
	<xsl:variable name="year" select="/fr:report/fr:parameters/fr:parameter[@fr:name = 'params.year']/@fr:value"/>
	
	<xsl:template name="content-header">
		<div class="caption">
		<xsl:choose>
			<xsl:when test="$folioId = 1101">Pending Account Balance:</xsl:when>
			<xsl:otherwise>Payments In <xsl:value-of select="$year"/>:</xsl:otherwise>
		</xsl:choose>	
		</div>
		<div class="spacer5"></div>
	</xsl:template>
	
	<xsl:template name="subtitle">Payments In <xsl:value-of select="$year"/></xsl:template>
    
	<xsl:template name="content-footer">
		<xsl:variable name="currentYear" select="number(cnv:formatObject('{*0}', 'DATE:yyyy'))"/>
        <xsl:variable name="min-year" select="number(/fr:report/fr:parameters/fr:parameter[@fr:name = 'minYear']/@fr:value)"/>

		<hr/>
		<table border="0" cellspacing="5px">
			<tr><th colspan="6" align="left">View Payments from a different year:</th></tr>
			<tr>
				<td>
       			<a href="payment_list.i?folioId=1101">Pending</a>
       			</td>
				<xsl:call-template name="next-year">
	               <xsl:with-param name="year" select="$currentYear"/>
	               <xsl:with-param name="min-year" select="$min-year"/>
	           </xsl:call-template>
			</tr>
		</table>
	</xsl:template>
	
	<xsl:template name="next-year">
	   <xsl:param name="year"/>
       <xsl:param name="min-year"/>       
	   <td>
           <xsl:call-template name="year-link">
               <xsl:with-param name="yearLink" select="$year"/>
           </xsl:call-template>
       </td>
       <xsl:if test="$year &gt; $min-year">
            <xsl:call-template name="next-year">
               <xsl:with-param name="year" select="$year - 1"/>
               <xsl:with-param name="min-year" select="$min-year"/>
           </xsl:call-template>
       </xsl:if>
	</xsl:template>
    
	<xsl:template name="year-link">
		<xsl:param name="yearLink"/>
		<xsl:choose>
			<xsl:when test="$yearLink = $year"><b><xsl:value-of select="$yearLink"/></b></xsl:when>
			<xsl:otherwise><a href="payment_list.i?folioId=635&amp;params.year={$yearLink}"><xsl:value-of select="$yearLink"/></a></xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>