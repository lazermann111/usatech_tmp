<xsl:stylesheet 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:html="http://www.w3.org/1999/xhtml"
	version="1.0">
	<xsl:output method="xml"/>	
	
	<xsl:template match="/">
		<project name="get-folio-sqls">
			<target name="get-folio-sqls">
				<xsl:for-each select="//html:a/@href[starts-with(., 'run_report.i?unframed=true&amp;folioId=')]">
					<xsl:variable name="folioId" select="substring(.,string-length('run_report.i?unframed=true&amp;folioId=') + 1)"/>
					<get>
						<xsl:attribute name="dest">${sql-dir}/folio-<xsl:value-of select="$folioId"/>.sql</xsl:attribute>
						<xsl:attribute name="src">${url}/login.i?username=${app-username}&amp;password=${app-password}&amp;simple.servlet.steps.LogonStep.forward=/get_sql.i&amp;folioId=<xsl:value-of select="$folioId"/></xsl:attribute>
					</get>
			 	</xsl:for-each>
		 	</target>
	 	</project>
	</xsl:template>
</xsl:stylesheet>
