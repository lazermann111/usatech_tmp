<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page
	import="simple.servlet.RequestUtils,simple.text.StringUtils,simple.results.Results"%>
<%
	Results regionResults = RequestUtils.getAttribute(request,
			"regionResults", Results.class, false);
	Results mappedRegionResults = RequestUtils.getAttribute(request,
			"mappedRegionResults", Results.class, false);		
	Results columnMapTemplateResults = RequestUtils.getAttribute(
			request, "columnMapTemplateResults", Results.class, false);
	Integer customerId = RequestUtils.getAttribute(request,
			"customerId", Integer.class, false);
	boolean isServiceAccountUser=false;
	UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
	if(customerId!=null&&customerId<1&&user.getUserType()==8){
		isServiceAccountUser=true;
	}
%>
<form name="columnMapForm" id="columnMapForm" method="post"
	action="column_mapping.i">
	<script type="text/javascript">
	var searchRequest=new Request.HTML({ 
    	url: "column_mapping_search.i",
    	data: $("columnMapForm"), 
    	method: 'post',
		evalScripts: true
	});
	
	var manageRequest=new Request.HTML({ 
    	url: "column_mapping_manage.i",
    	data: $("columnMapForm"), 
    	method: 'post',
		evalScripts: true
	});
	
	function clearMessage(){
		$("message").set("html","");
		$("templateMessage").set("html","");
	}
	
	function setMessage(status, msg){
		$("waitImage").style.display='none';
		if(status<0){
			$("message").set("html","<p class='status-info-failure'>"+msg+"</p>");
		}else if(status>0){
			$("message").set("html","<p class='status-info-success'>"+msg+"</p>");
		}
	}
	
	function setTemplateMessage(status, msg){
		$("waitImage2").style.display='none';
		if(status<0){
			$("templateMessage").set("html","<p class='status-info-failure'>"+msg+"</p>");
		}else if(status>0){
			$("templateMessage").set("html","<p class='status-info-success'>"+msg+"</p>");
		}
	}
	function searchByCustomerName(){
		clearMessage();
		$("waitImage").style.display='block';
		$("searchFor").value="customer";
		$("customerName").value=$("customerName").value.trim();
		searchRequest.send();
	}
	function clearAll(){
		clearMessage();
		$('regionId').set('html','<option value="-1">NO FILTER FOR REGION</option>');
		$('locationName').set('html','<option value="-1">NO FILTER FOR LOCATION</option>');
		$('mappedRegionId').set('html','<option value="-1">NO FILTER FOR REGION</option>');
		$('mappedLocationName').set('html','<option value="-1">NO FILTER FOR LOCATION</option>');
		$('mappedDeviceList').set('html','');
		$('unmappedDeviceList').set('html','');
		$('columnMapId').set('html','');
		$('columnMapContent').set('value','');
		$('unmappedDeviceSerialCd').set('value','');
		$('mappedDeviceSerialCd').set('value','');
		$('columnMapName').set('value','');
		$('addNewTemplateButton').disabled=true;
		$('saveTemplateButton').disabled=true;
		$('deleteTemplateButton').disabled=true;
		$('saveNewTemplateButton').disabled=true;
		$('cancelTemplateButton').disabled=true;
		$('mappedDeviceSearchByTemplate').disabled=true;
		$('mappedNote').set('text', "--Use filters to find mapped devices--");
		$('unmappedNote').set('text', "--Use filters to find unmapped devices--");
		disableForceSave();
	}
	function onCustomerChange(){
		clearAll();
		$("waitImage").style.display='block';
		$("searchFor").value="regionAndMap";
		searchRequest.send();
		$('saveNewTemplateButton').disabled=false;
		$('cancelTemplateButton').disabled=false;
		$('mappedDeviceSearchByTemplate').disabled=false;
	}
	function onRegionChange(){
		clearMessage();
		if($('regionId').value==-1){
			return;
		}
		$("waitImage").style.display='block';
		$("searchFor").value="location";
		searchRequest.send();
	}
	
	function searchLocation(){
		clearMessage();
		$("waitImage").style.display='block';
		$("searchFor").value="location";
		$("locationNameText").value=$("locationNameText").value.trim();
		searchRequest.send();
	}
	function searchMappedLocation(){
		clearMessage();
		$("waitImage").style.display='block';
		$("searchFor").value="mappedLocation";
		$("mappedLocationNameText").value=$("mappedLocationNameText").value.trim();
		searchRequest.send();
	}
	
	function searchMapped(){
		clearMessage();
		$("waitImage").style.display='block';
		$("searchFor").value="mapped";
		
		getSearchNote('mapped');
		searchRequest.send();
	}
	
	function searchMappedByTemplate(){
		if($('columnMapId').selectedIndex==-1){
			alert("Please select a column map template.");
			return;
		}
		clearMessage();
		$("waitImage").style.display='block';
		$("searchFor").value="mappedByTemplate";
		$('mappedNote').set('text','Searched by template: '+$('columnMapId').getSelected().getText());
		searchRequest.send();
	}
	
	function getSearchNote(type){
		if (type == 'mapped'){
			var noteText = 'Searched by ';
			if($('customerId').type!='hidden'){
				if($('customerId').selectedIndex!=0){
					noteText = noteText + "Customer: " + $('customerId').getSelected().getText() + "; ";
				}else{
					noteText = noteText + "all customers; ";
				}
			}
			if ($('mappedDeviceSerialCd').value.trim() != ""){
				noteText = noteText + "Devices like: " + $('mappedDeviceSerialCd').value + "; ";
			}else{
				noteText = noteText + "any device serial number; ";
			} 
			if ($('mappedRegionId').selectedIndex!=0){
				noteText = noteText + "Region: " + $('mappedRegionId').getSelected().getText() + "; ";
			}else{
				noteText = noteText + "all regions; ";
			}
			if ($('mappedLocationName').selectedIndex!=0){
				noteText = noteText + "Location: " + $('mappedLocationName').getSelected().getText() + "; ";
			}else{
				noteText = noteText + "all locations; "
			}
			$('mappedNote').set('text',noteText);
		}else{
			var noteText = 'Searched by ';
			if($('customerId').type!='hidden'){
				if($('customerId').selectedIndex!=0){
					noteText = noteText + "Customer: " + $('customerId').getSelected().getText() + "; ";
				}else{
					noteText = noteText + "all customers; ";
				}
			}
			if ($('unmappedDeviceSerialCd').value.trim() != ""){
				noteText = noteText + "Devices like: " + $('unmappedDeviceSerialCd').value + "; ";
			}else{
				noteText = noteText + "any device serial number; ";
			} 
			if($('regionId').selectedIndex!=0){
				noteText = noteText + "Region: " + $('regionId').getSelected().getText() + "; ";
			}else{
				noteText = noteText + "all regions; ";
			}
			if($('locationName').selectedIndex!=0){
				noteText = noteText + "Location: " + $('locationName').getSelected().getText() + "; ";
			}else{
				noteText = noteText + "all locations; "
			}
			$('unmappedNote').set('text',noteText);
		}
	}
	
	function clearMappedSearch(){
		if($('customerId').type!='hidden'){
				$('mappedSearchCustomer').set("text","all");
		}
		$('mappedSearchRegion').set("text","all");
		$('mappedSearchLocation').set("text","all");
	}
	function searchUnmapped(){
		clearMessage();
		$("waitImage").style.display='block';
		$("searchFor").value="unmapped";
		getSearchNote('unMapped');
		searchRequest.send();
	}
	
	function onSelectAll(list, selectedText){
		list.getElements("option").each(function(el) {
			el.selected=selectedText;
		});
	}
	
	function onChangeMappedDeviceList(value){
		if($('mappedDeviceList').getSelected().length==1){
			var columnMapValue=value.substring(value.indexOf("_")+1);
			var cssSelectText='option[value="'+columnMapValue+'"]';
			var selectedColumnMapIndexBefore=$("columnMapId").selectedIndex;
			if($('columnMapId').getElement(cssSelectText)){
				$('columnMapId').getElement(cssSelectText).set('selected', 'selected');
			}
			var selectedColumnMapIndexAfter=$("columnMapId").selectedIndex;
			if(selectedColumnMapIndexBefore!=selectedColumnMapIndexAfter){
				onChangeColumnMap();
			}
		}else{
			onSelectAllMapped();
		}
	}
	
	function onChangeColumnMap(){
		clearMessage();
		$("waitImage").style.display='block';
		$("searchFor").value="columnMap";
		var editable=$('columnMapId').getSelected().get("editable");
		if(editable =='Y'){
			$('columnMapContent').set("readonly", false);
			$('columnMapContent').set("class", "");
			$('columnMapName').set("disabled","");
			$('saveTemplateButton').disabled=false;
			$('deleteTemplateButton').disabled=false;
			$('cancelTemplateButton').disabled=false;
			$('addNewTemplateButton').disabled=false;
			$('saveNewTemplateButton').disabled=true;
		}else{
			$('columnMapContent').set("readonly", true);
			$('columnMapContent').set("class", "readonlybackgroud");
			$('columnMapName').set("disabled","disabled");
			$('saveTemplateButton').disabled=true;
			$('deleteTemplateButton').disabled=true;
			$('cancelTemplateButton').disabled=true;
			if(<%=StringUtils.prepareCDATA(Boolean.toString(isServiceAccountUser))%>){
				$('addNewTemplateButton').disabled=true;
			}else{
				$('addNewTemplateButton').disabled=false;
			}
			$('saveNewTemplateButton').disabled=true;
		}
		searchRequest.send();
		$('mappedDeviceSearchByTemplate').disabled=false;
		disableForceSave();
	}
	
	function onApplyTemplate(){
		if($('columnMapId').selectedIndex==-1){
			alert("Please select the column map template.");
			return;
		}
		
		if($('unmappedDeviceList').selectedIndex==-1){
			alert("Please select at least one unmapped device to apply the template.");
			return;
		}
		$("actionType").value="apply";
		manageRequest.send();
	}
	
	function onRemoveTemplate(){
		if($('mappedDeviceList').selectedIndex==-1){
			alert("Please select one mapped device to remove the template.");
			return;
		}
		if(confirm("Are you sure you want to remove the template for the selected devices?")){
			$('mappedDeviceList').getSelected().each(function(el) {
				el.value=el.value.substring(0,el.value.indexOf('_'));
			});
			$("actionType").value="remove";
			manageRequest.send();
		}
	}
	
	function moveUnmappeddToMapped(){
		var columnMapIdValue=$('columnMapId').value;
		$('unmappedDeviceList').getSelected().each(function(el) {
			el.value=el.value+'_'+columnMapIdValue;
			el.set("selected","");
			var mappedEl=el.dispose();
			$('mappedDeviceList').grab(mappedEl,'top');
		});
	}
	
	function moveMappedToUnmapped(){
		$('mappedDeviceList').getSelected().each(function(el) {
			el.set("selected","");
			var unmappedEl=el.dispose();
			$('unmappedDeviceList').grab(unmappedEl, 'top');
		});
	}
	function moveDisableToEnable(){
		$('mappedDeviceList').getSelected().each(function(el) {
			el.set("style","color:LIGHT BLUE; font-weight:");
		});
	}
	function moveEnableToDisable(){
		$('mappedDeviceList').getSelected().each(function(el) {
			el.set("style","color:PURPLE; font-weight:bold");
		});
	}
	function onCancelTemplate(){
		clearMessage()
		disableForceSave();
		if($('columnMapId').selectedIndex!=-1){
			onChangeColumnMap();
		}else{
			$('columnMapContent').set('value','');
			$('columnMapName').set('value','');
		}
	}
	function onDeleteTemplate(){
		if(confirm("Are you sure you want to delete the template?")){
			$("actionType").value="delete";
			manageRequest.send();
		}
	}
	function mapSaveNewMode(){
		$('columnMapContent').set('value','');
		$('columnMapName').set('value','');
		$('saveNewTemplateButton').disabled=false;
		$('cancelTemplateButton').disabled=false;
		$('saveTemplateButton').disabled=true;
		$('deleteTemplateButton').disabled=true;
		$('addNewTemplateButton').disabled=true;
	}
	function mapDeleted(){
		$('columnMapId').getSelected().dispose();
		mapSaveNewMode();
	}
	
	function onSelectAllMapped(){
		$('columnMapId').selectedIndex=-1;
		mapSaveNewMode();
	}
	
	function mapCreated(mapId, scope){
		$('columnMapId').grab(new Element("option",{
			selected: 'selected',
			value:mapId,
			title:$("columnMapName").value,
			text:scope+$("columnMapName").value,
			editable: 'Y'
		}),'top');
		
		$('saveTemplateButton').disabled=false;
		$('deleteTemplateButton').disabled=false;
		$('cancelTemplateButton').disabled=false;
		$('addNewTemplateButton').disabled=false;
		$('saveNewTemplateButton').disabled=true;
		disableForceSave();
	}
	
	function mapEdited(mapId, scope){
		$('columnMapId').getSelected().set('title', $("columnMapName").value);
		$('columnMapId').getSelected().set('text', scope+$("columnMapName").value);
		disableForceSave();
	}
	function onAddNewTemplate(){
		clearMessage()
		$('columnMapId').selectedIndex=-1;
		$('columnMapContent').set("readonly", false);
		$('columnMapContent').set("class", "");
		$('columnMapName').set("disabled","");
		mapSaveNewMode();
	}
	
	function validateColumnMapContent(){
		$('columnMapContent').value=$('columnMapContent').value.trim();
		$('columnMapName').value=$('columnMapName').value.trim();
		if($('columnMapContent').value.length==0){
			alert("Please input the column map content.");
			return false;
		}else if ($('columnMapName').value.length==0||$('columnMapName').value.length > 100){
			alert("Please input a column map name that is less than 100 characters.");
			return false;
		}else if($('columnMapContent').value.indexOf('\\')>0){
			alert("Backslash is not allowed for column map value. Please remove it.");
			return false;
		}else if($('columnMapContent').value.indexOf('==')>0){
			alert("Double equal sign is not allowed for column map value. Please correct it.");
			return false;
		}else {
			return true;
		}
	}
	function onSaveNewTemplate(){
		if(validateColumnMapContent()){
			clearMessage()
			$("actionType").value="saveNew";
			$('columnMapId').selectedIndex=-1;
			manageRequest.send();
			$('forceToSave').disabled=true;
		}
	}
	
	function onSaveTemplate(){
		if(validateColumnMapContent()){
			clearMessage()
			$("actionType").value="save";
			manageRequest.send();
			$('forceToSave').disabled=true;
		}
	}
	function enableForceSave(compareMapId){
		$('forceToSave').disabled=false;
		$('columnMapContent').set('style','width: 130px; resize: none;');
		$('columnMapContentCompareDiv').set('style','display:inline');
		enableSelectCompareButton(compareMapId);
	}
	
	function enableSelectCompareButton(compareMapId){
		$('selectCompareButton').set('compareMapId', compareMapId);
		$('selectCompareButton').set('style', 'display:inline');
	}
	
	function disableForceSave(){
		$('forceToSave').disabled=true;
		$('forceToSave').set("checked", false);
		$('columnMapContent').set('style','width: 100%; resize: none;');
		$('columnMapContentCompareDiv').set('style','display:none');
		$('selectCompareButton').set('style', 'display:none');
	}
	
	function onSelectCompareButton(){
		var columnMapValue=$('selectCompareButton').get('compareMapId');
		var cssSelectText='option[value="'+columnMapValue+'"]';
		$('columnMapId').getElement(cssSelectText).set('selected', 'selected');
		onChangeColumnMap();
	}
	
	function onDisableAutoUpdate(){
		if($('mappedDeviceList').selectedIndex==-1){
			alert("Please select one mapped device to disable column map auto update.");
			return;
		}
		if(confirm("Are you sure you want to disable column map auto update?")){
			$('mappedDeviceList').getSelected().each(function(el) {
				el.value=el.value.substring(0,el.value.indexOf('_'));
			});
			$("actionType").value="disableAutoUpdate";
			manageRequest.send();
		}
	}
	
	function onEnableAutoUpdate(){
		if($('mappedDeviceList').selectedIndex==-1){
			alert("Please select one mapped device to enable column map auto update.");
			return;
		}
		if(confirm("Are you sure you want to enable column map auto update?")){
			$('mappedDeviceList').getSelected().each(function(el) {
				el.value=el.value.substring(0,el.value.indexOf('_'));
			});
			$("actionType").value="enableAutoUpdate";
			manageRequest.send();
		}
	}
	
	function focusNoScrollMethod(level){
		$('columnMapId').focus({preventScroll: true});
	}
	
</script>
<div class="toggle-heading">
	<span class="caption">Column Maps</span>
	(<a title="Column Map Instructions" href="download_file.i?file_name=USALive+Column+Map+Instructions.pdf">Instructions</a>)
</div>
<div id="waitImage" style="display: none;">
	Please wait...<img alt="Please wait" src="images/pleasewait.gif" />
</div>
<div id="message"></div>

	<input type="hidden" name="searchFor" id="searchFor" value="" /> <input
		type="hidden" name="actionType" id="actionType" value="" />
	<input type="hidden" id="redirectHome" name="redirectHome" value="true"/>
	<input type="hidden" name="session-token" value="<%=RequestUtils.getSessionToken(request)%>" />
<%if (customerId == null) {%>	
	<table class="selectBody">
		<tbody>
		<tr>
		<td>
		<div class="selectSection">
		<div class="sectionTitle">Filter to find devices to assign column map</div>
		<div class="sectionRow">
			<table>
			<tr>
				<td colspan="2"><b>Customers:</b> <span
					title="Search customer name that contains: (input nothing for all customers)"><input
						type="text" name="customerName" id="customerName" /></span> <input
					type="button" name="customerNameSearch" id="customerNameSearch"
					value="Search Customer" onClick="searchByCustomerName();"
					title="Search customer will populate the filter list on the right" />
					<select id="customerId" onchange="onCustomerChange();"
					name="customerId" style="width: 200px">
						<option value="-1">NO FILTER FOR CUSTOMER</option>
				</select></td>
			</tr>
			</table>
		</div>
		</div>
		</td>
		</tr>			
		</tbody>
	</table>
<%} else {%>
<input type="hidden" name="customerId" id="customerId"
	value="<%=customerId%>" />
<%
}
if(isServiceAccountUser){ %>
	<input type="hidden" name="regionId" id="regionId" value="-1" />
	<input type="hidden" name="locationName" id="locationName" value="-1" />
<%}%>	
	<table class="selectBody">
		<tbody>
			<tr>
				<td valign="top" width="50%">
					<div class="selectSection">
						<div class="sectionTitle">Unmapped Device Serial:</div>
						<div class="sectionRow">
							<table>
								<tr>
									<td>
										<table>
											<tr>
												<td>
													<b>Device Serial Number:</b>
												</td>
												<td>
													<span title="Use comma to input multiple device serial for exact match or input text for partial search">
														<input type="text" name="unmappedDeviceSerialCd" id="unmappedDeviceSerialCd" />
													</span> 
												</td>
											</tr>
											<%if(!isServiceAccountUser){%>
											<tr>
												<td>
													<b>Region:</b> 
												</td>
												<td>
													<select id="regionId" name="regionId" style="width: 200px">
														<option value="-1">NO FILTER FOR REGION</option>
														<%
															while (regionResults != null && regionResults.next()) {
														%><option
																value="<%=StringUtils.prepareCDATA(regionResults
																	.getFormattedValue("regionId"))%>"><%=StringUtils.prepareHTML(regionResults
																	.getFormattedValue("regionName"))%></option>
														<%
														}
														%>
													</select>
												</td>
											</tr>
											<tr>
												<td>
													<b>Location:</b> 
												</td>
												<td>
													<span title="Search location name that contains: (input nothing for all location)">
														<input type="text" name="locationNameText" id="locationNameText" />
													</span>
													<input type="button" name="locationNameSearch"
														id="locationNameSearch" value="Search Location" onClick="searchLocation();"
														title="Search location will populate the filter list on the right" />
													<select id="locationName" name="locationName" style="width: 200px;">
														<option value="-1">NO FILTER FOR LOCATION</option>
													</select>
												</td>
											</tr>
											<%}%>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										<input type="button" name="unmappedDeviceSearch" id="unmappedDeviceSearch"
											value="Search" onClick="searchUnmapped();" title="Search using the region, location, and device serial filter" />
									</td>
								</tr>
								<tr>
									<td>
										<div id="unmappedNote" style="color:green">--Use filters to find unmapped devices--</div> 
									</td>
								</tr>
								<tr>
									<td><select id="unmappedDeviceList" style="width: 100%"
										name="unmappedDeviceList" size="8" multiple="true"></select></td>
								</tr>
								<tr>
									<td><input type="button" id="selectAllUnmapped"
										value="Select All"
										onclick="onSelectAll($('unmappedDeviceList'),'selected');" />
										<input type="button" id="unselectAllUnmapped"
										value="Unselect All"
										onclick="onSelectAll($('unmappedDeviceList'),'');" /> <input
										type="button" id="applyTemplateButton" value="Apply Template"
										onclick="onApplyTemplate();"
										title="Apply the column map template to the unmapped device" /></td>
								</tr>
							</table>
						</div>
					</div>
				</td>
				<td valign="top" width="50%">
					<div class="selectSection">
						<div class="sectionTitle">Mapped Device Serial:</div>
						<div class="sectionRow">
							<table>
								<tr>
									<td>
										<table>
											<tr>
												<td>
													<b>Device Serial Number:</b>
												</td>
												<td>
													<span title="Use comma to input multiple device serial for exact match or input text for partial search">
														<input type="text" name="mappedDeviceSerialCd" id="mappedDeviceSerialCd" /> 
													</span>
												</td>
											</tr>
											<%if(!isServiceAccountUser){%>
												<tr>
													<td>
														<b>Region:</b> 
													</td>
													<td>
														<select id="mappedRegionId" name="mappedRegionId" style="width: 200px">
															<option value="-1">NO FILTER FOR REGION</option>
															<%
															while (mappedRegionResults != null && mappedRegionResults.next()) {
															%><option
																	value="<%=StringUtils.prepareCDATA(mappedRegionResults
																	.getFormattedValue("regionId"))%>"><%=StringUtils.prepareHTML(mappedRegionResults
																	.getFormattedValue("regionName"))%></option>
															<%
															}
															%>
														</select>
													</td>
												</tr>
												<tr>
													<td>
														<b>Location:</b> 
													</td>
													<td>
														<span title="Search location name that contains: (input nothing for all location)">
															<input type="text" name="mappedLocationNameText" id="mappedLocationNameText" />
														</span>
														<input type="button" name="mappedLocationNameSearch"
															id="mappedLocationNameSearch" value="Search Location" onClick="searchMappedLocation();"
															title="Search location will populate the filter list on the right" />
														<select id="mappedLocationName" name="mappedLocationName" style="width: 200px;">
															<option value="-1">NO FILTER FOR LOCATION</option>
														</select>
													</td>
												</tr>
											<%}%>
										</table>
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<input type="button" name="mappedDeviceSearch" id="mappedDeviceSearch"
											value="Search" onClick="searchMapped();" title="Search using region, location, and device serial filter" />
										<input type="button" name="mappedDeviceSearchByTemplate" id="mappedDeviceSearchByTemplate" 
											value="Search By Template" onClick="searchMappedByTemplate();" disabled="true"
											title="Search only by selected column map template" />
									</td>
								</tr>
								<tr>
									<td>
										<div id="mappedNote" style="color:green">--Use filters to find mapped devices--</div> 
									</td>
								</tr>
								<tr>
									<td><select id="mappedDeviceList" style="width: 100%"
										name="mappedDeviceList" size="8"
										onChange="onChangeMappedDeviceList(value);" multiple="true"></select></td>
								</tr>
								<tr>
									<td><input type="button" id="selectAllMapped"
										value="Select All"
										onclick="onSelectAll($('mappedDeviceList'),'selected');onSelectAllMapped();" />
										<input type="button" id="unselectAllMapped"
										value="Unselect All"
										onclick="onSelectAll($('mappedDeviceList'),'');onSelectAllMapped();" />
										<input type="button" id="removeTemplateButton"
										value="Remove Template" onclick="onRemoveTemplate();"
										title="Remove the column map template from the mapped device" />
										<input type="button" id="disableAutoUpdate" style="color: PURPLE"
										value="Disable Map Auto Update" onclick="onDisableAutoUpdate();"
										title="Disable Auto Update of Device's Column Map"/>
										<input type="button" id="enableAutoUpdate"
										value="Enable Map Auto Update" onclick="onEnableAutoUpdate();"
										title="Enable Auto Update of Device's Column Map" />
									</td>
								</tr>
							</table>
						</div>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<div style="width: 700px">
		<table>
			<tr>
				<td>
					<div id="waitImage2" style="display: none;">
						Please wait...<img alt="Please wait" src="images/pleasewait.gif" />
					</div>
				</td>
				<td>
					<div id="templateMessage"></div>
				</td>
			</tr>
		</table>
	</div>
	<table class="selectBody">
		<tbody>
			<tr>
				<td valign="top" width="50%">
					<div class="selectSection">
						<div class="sectionTitle">Column Map Templates:</div>
						<div class="sectionRow">
							<table>
								<tr>
									<td> <select id="columnMapId" style="width: 100%; height: 310px;"
												name="columnMapId" size="25" onChange="onChangeColumnMap();" 
											>
											<%
												while (columnMapTemplateResults != null
														&& columnMapTemplateResults.next()) {
											%><option 
												value="<%=StringUtils.prepareCDATA(columnMapTemplateResults
						.getFormattedValue("columnMapId"))%>"
												title="<%=StringUtils.prepareHTML(columnMapTemplateResults
						.getFormattedValue("columnMapName"))%>"
												editable="<%=StringUtils.prepareHTML(columnMapTemplateResults
						.getFormattedValue("editable"))%>">
												<%
													if (columnMapTemplateResults.get("customerId") == null) {
												%>[Global]
												<%
													} else {
												%>
												[<%=StringUtils.prepareCDATA(columnMapTemplateResults
						.getFormattedValue("customerName"))%>]
												<%
													}
												%>
												<%=StringUtils.prepareHTML(columnMapTemplateResults
						.getFormattedValue("columnMapName"))%></option>
											<%
												}
											%>
									</select></td>
								</tr>
							</table>
						</div>
					</div>
				</td>
				<td valign="top" width="50%">
					<div class="selectSection">
						<div class="sectionTitle">
							Column Map Content (MDB #=Vend Column=Code): <span class="required">*</span>
						</div>
						<div class="sectionRow">
							<table height="318px">
								<tr>
									<td><textarea id="columnMapContent"
											name="columnMapContent" rows="13"
											style="width: 100%; resize: none;">
											</textarea>
										<div id="columnMapContentCompareDiv" style="display: none">
											Compare:
											<textarea id="columnMapContentCompare"
												name="columnMapContentCompare" rows="13"
												style="width: 130px; resize: none;"
												class="readonlybackgroud">
											</textarea>
										</div></td>
								</tr>
								<tr>
									<td colspan="2"><b>Name:</b> <span class="required">*</span><input
										type="text" name="columnMapName" id="columnMapName" size="45" /></td>
								</tr>
								<tr>
									<td><b>Force to save:</b> <input type="checkbox"
										name="forceToSave" id="forceToSave" disabled="disabled" /> <input
										type="button" id="selectCompareButton"
										value="Select the recommended map"
										onclick="onSelectCompareButton();" style="display: none" /></td>
								</tr>
								<tr>
									<td><input type="button" id="addNewTemplateButton"
										value="Add New" onclick="onAddNewTemplate();" disabled="disabled" /> <input type="button"
										id="saveNewTemplateButton" value="Save New"
										onclick="onSaveNewTemplate();" <%if(isServiceAccountUser) {%> disabled="disabled" <%} %>/> <input
										type="button" id="saveTemplateButton" value="Save"
										onclick="onSaveTemplate();" disabled="disabled" /> <input
										type="button" id="cancelTemplateButton" value="Cancel"
										onclick="onCancelTemplate();" <%if(isServiceAccountUser) {%> disabled="disabled" <%} %> /><input
										type="button" id="deleteTemplateButton" value="Delete"
										onclick="onDeleteTemplate();" disabled="disabled" /></td>
								</tr>
							</table>
						</div>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
</form>
<script>
 if (navigator.userAgent.indexOf('Chrome') != -1){
	$('columnMapId').set("onmousedown","focusNoScrollMethod();");
}
</script>

