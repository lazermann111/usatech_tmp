<%@page
	import="simple.servlet.RequestUtils,simple.text.StringUtils,simple.results.Results"%>
<%
	Results userRequestDetails = RequestUtils.getAttribute(request,
			"userRequestDetails", Results.class, false);
	String displayName = RequestUtils.getAttribute(request,
			"displayName", String.class, true);
	int rowNum = 1;
%>
<div>
	<table class="reportTitle">
		<tr>
			<td>
				<div class="title2">
					Ready Report for
					<%=StringUtils.prepareHTML(displayName)%>
				</div>
				<table class="folio">
					<tbody>
						<tr class="headerRow">
							<th><a data-toggle="sort" data-sort-type="STRING" title="">Report File Name</a></th>
							<th><a data-toggle="sort" data-sort-type="DATE" title="">Requested Time</a></th>
							<th><a data-toggle="sort" data-sort-type="STRING" title="">Request Type</a></th>
							<th><a data-toggle="sort" data-sort-type="STRING" title="">Requested By</a></th>
							<th><a data-toggle="sort" data-sort-type="STRING" title="">Requested For</a></th>
							<th><a data-toggle="sort" data-sort-type="STRING" title="">Status</a></th>
						</tr>
					</tbody>
					<tbody>
						<%
							while (userRequestDetails != null && userRequestDetails.next()) {
						%>
						<%
							if (rowNum % 2 == 0) {
						%>
						<tr class="evenRow">
							<%
								} else {
							%>
						
						<tr class="oddRow">
							<%
								}
							%>
							<td
								data-sort-value="<%=StringUtils.prepareCDATA(userRequestDetails
						.getFormattedValue("reportName"))%>">
								<%=StringUtils.prepareCDATA(userRequestDetails
						.getFormattedValue("reportName"))%>
							</td>
							<td
								data-sort-value="<%=StringUtils.prepareCDATA(userRequestDetails
						.getFormattedValue("createdTs"))%>">
								<%=StringUtils.prepareCDATA(userRequestDetails
						.getFormattedValue("createdTs"))%>
							</td>
							<td data-sort-value="User Requested">User Requested</td>
							<td
								data-sort-value="<%=StringUtils.prepareCDATA(userRequestDetails
						.getFormattedValue("userName"))%>">
								<%=StringUtils.prepareCDATA(userRequestDetails
						.getFormattedValue("userName"))%>
							</td>
							<td
								data-sort-value="<%=StringUtils.prepareCDATA(userRequestDetails
						.getFormattedValue("profileName"))%>">
								<%=StringUtils.prepareCDATA(userRequestDetails
						.getFormattedValue("profileName"))%>
							</td>
							<td data-sort-value="New Report" style="text-align: center"><a
								title="New Report"
								href="retrieve_report_by_user.i?profileId=<%=StringUtils.prepareCDATA(userRequestDetails
						.getFormattedValue("profileId"))%>&amp;requestId=<%=StringUtils.prepareCDATA(userRequestDetails
						.getFormattedValue("requestId"))%>&amp;pageId=0">New
									Report</a></td>
						</tr>
						<%
							rowNum++;
							}
						%>
						<%
							if (rowNum == 1) {
						%>
						<tr>
							<td colspan="6">No Reports</td>
						</tr>
						<%
							}
						%>
					</tbody>
				</table> &#160;
				<div class="copyright"></div>
			</td>
		</tr>
	</table>
</div>