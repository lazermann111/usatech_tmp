<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="simple.servlet.RequestUtils"%>
<% UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);%>
<table width="630px"><%
if(user.getCustomerId() != 0) {
    boolean canActivate = true;
    if(user.isMissingLicense()) {
        canActivate = false;
        %><tr><td><ul class="required" style="margin-left: 20px"><li><%
        if(user.hasPrivilege(ReportingPrivilege.PRIV_CREATE_BANK_ACCT)) {
            %>We have not received your signed License Agreement. If you have
     already faxed us this document with your signature, thank you - your account will be active soon. If not, please goto <a href="javascript: gotoPage('license_instructions');" onmouseover="window.status = 'Print Your License Agreement'; return true;"
            onmouseout="window.status = ''" title="Print Your License Agreement">License Agreement</a> and follow the instructions.<%
        } else {
            %>We have not received a signed License Agreement for your company. Please 
        ask your USALive administrator to print and fax this document to USA Technologies.<%
        }%></li></ul></td></tr><%
    } 
    if(user.isMissingBankAccount()) {
        canActivate = false;
        %><tr><td><ul class="required" style="margin-left: 20px"><li><%
        if(user.hasPrivilege(ReportingPrivilege.PRIV_CREATE_BANK_ACCT)) {
        %>You have not yet established a Bank Account for Direct Credit Payments from USA Technologies. Please goto <a href="javascript: gotoPage('new_bank_acct');" onmouseover="window.status = 'Create a New Bank Account'; return true;"
            onmouseout="window.status = ''" title="Establish a Bank Account to Receive Payments from USA Technologies">New Bank Acct</a> to setup your Bank Account.<%
        } else {
        %>A Bank Account for Direct Credit Payments has not yet been established for your company. Please 
    ask your USALive administrator to log on to USALive Online and set up a Bank Account.<%
            }%></li></ul></td></tr><%
    }
    if(user.hasPrivilege(ReportingPrivilege.PRIV_CREATE_BANK_ACCT) && user.getPendingBankAccountCount() > 0) {
        if(user.getActiveBankAccountCount() == 0) canActivate = false;
        %><tr><td><ul class="required" style="margin-left: 20px"><li>We have not received a signed Authorization For Electronic Funds Transfer Form for <%=user.getPendingBankAccountCount()%> of
        your bank accounts. If you have already faxed us this form with your signature, thank you - your account will be active soon. If not, please goto <a href="javascript: gotoPage('eft_auth_instructions');" onmouseover="window.status = 'Print an EFT Authorization Form'; return true;"
                onmouseout="window.status = ''" title="Print an EFT Authorization Form">EFT Auth Form</a> and follow the instructions.</li></ul></td></tr><%
    } 
    if(user.getTerminalCount() == 0 && canActivate) {
        %><tr><td><ul class="required" style="margin-left: 20px"><li><%
        if(user.hasPrivilege(ReportingPrivilege.PRIV_ACTIVATE_TERMINAL)) { 
            %>Your devices have not been activated yet. Please goto <a href="javascript: gotoPage('new_terminal');" onmouseover="window.status = 'Activate Terminal'; return true;"
                    onmouseout="window.status = ''" title="Activate Devices so you can begin Cashless Vending!">Activate Device</a> to activate your terminals.<%
        } else {
            %>No active devices are assigned to you. Please ask your USALive administrator to assign devices to you.<%
        }%></li></ul></td></tr><%
    }
} else if(user.getTerminalCount() == 0) {%>
<tr><td><ul style="margin-left: 20px"><li>No active devices are assigned to you.</li></ul></td></tr> 
<%}%></table>