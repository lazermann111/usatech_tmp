<%@page import="simple.servlet.RequestUtils,simple.results.Results,simple.text.StringUtils"%><%
Results results = RequestUtils.getAttribute(request, "results", Results.class, false);
boolean data;
if(results != null && results.next()) {
	String tf = "simple.text.StringFormat:###-###-####";
	results.setFormat("customerTelephone", tf);
	results.setFormat("customerFax", tf);
	results.setFormat("contactTelephone", tf);
	results.setFormat("contactFax", tf);
	data = true;
} else {
	data = false;
}
%>
<html><head><title>Authorization For Electronic Funds Transfer</title>
<style type="text/css">
body { FONT-FAMILY: Arial, sans-serif; }
.title { FONT-SIZE: 14pt; FONT-WEIGHT: bold; text-align: center}
.subtitle { FONT-SIZE: 10pt; text-align: left; height: 13pt;}
.info { FONT-SIZE: 10pt; FONT-WEIGHT: bold;}
.fine { FONT-SIZE: 8pt; }
.info TH {BACKGROUND: #CCCCCC; text-align: left; }
.spacer {HEIGHT: 4px; FONT-SIZE: 4px;}
.field { FONT-WEIGHT: normal; BORDER-BOTTOM: 1pt solid black; VERTICAL-ALIGN: bottom; }
.fillin { TEXT-DECORATION: underline; }
.note { FONT-WEIGHT: normal; }
.row { HEIGHT: 20px; }
.instructs { FONT-SIZE: 10pt; text-align: center; }
</style>
</head>
<body leftmargin="0" topmargin="0" onload="window.print()">
<table border="0" width="674" cellspacing="0"><col width="113"/><col/><tr valign="top"><td style="BORDER-BOTTOM: 2pt double black; width: 113px; height: 70px"><img height="62" src="<%=StringUtils.prepareCDATA(request.getContextPath())%>/images/usa_tech_logo.gif" width="113" alt=""/></td>
    <td class="title" style="BORDER-BOTTOM: 2pt double black">AUTHORIZATION FOR ELECTRONIC FUNDS TRANSFER</td></tr>
<tr><th class="subtitle" colspan="2">Direct Credit Authorization:</th></tr>
<tr><td class="fine" colspan="2">You hereby authorize USA Technologies, Inc. and its designated financial agents to initiate credit entries to the account listed below in 
connection with agreed upon Electronic Data Interchange (EDI) transactions between our companies.   You agree that such transactions will 
be governed by the National Automated Clearing House Association (ACH) rules.  This authority is to remain in effect until USA Technologies,  
Inc. has received written notification of termination in such time and such manner as to afford USA Technologies, Inc. a reasonable opportunity  
to act on it.  You also authorize the Bank listed below to verify your account information as necessary to establish the EFT.  IN NO EVENT SHALL  
USA TECHNOLOGIES, INC. BE LIABLE FOR ANY SPECIAL, INCIDENTAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES AS A RESULT OF 
THE DAILY, OMISSION OR ERROR OF AN ELECTRONIC CREDIT ENTRY, EVEN IF USA TECHNOLOGIES, INC. HAS BEEN ADVISED OF 
THE POSSIBILITY OF SUCH DAMAGES.  This agreement shall be governed by the laws of the State of Pennsylvania.</td></tr>
<tr><th class="subtitle" colspan="2">Direct Debit Authorization:</th></tr>
<tr><td class="fine" colspan="2">You hereby authorize USA Technologies, Inc. and its designated financial agents to initiate an ACH debit (automatic withdrawal) entry to the 
account listed below for payment of monthly network service fees and other fees agreed to in your License Agreement, and to debit the entry 
to the company's account (as listed below).  This authorization is to remain in full force and effect until USA Technologies, Inc. receives 
notification from an official or authorized agent of your company of the termination.  To revoke this payment authorization, you must contact  
USA Technologies, Inc. at (610) 989-0340 no later than 2 business days prior to the payment (settlement) date.  You also authorize USA 
Technologies, Inc. and the financial institutions involved in the processing of the electronic payment of fees and charges to receive confidential 
information necessary to answer inquiries and resolve issues related to the payment.</td></tr>
<tr><td class="fine" colspan="2">*Please Note that the minimum daily ACH amount is $25.00</td></tr>
<tr><td colspan="2"><br/><table class="info" width="100%" cellspacing="0" cellpadding="1">
<col width="208"/><col/>
<tr><th>REMIT TO ADDRESS</th><th>This should be the remit to address shown on your invoices</th></tr>
<tr class="row"><td>Company Name:</td><td class="field"><%=(data ? StringUtils.prepareHTML(results.getFormattedValue("customerName")) : "&#160;")%></td></tr>
<tr class="row"><td>Street Address / PO Box:</td><td class="field"><%=(data ? StringUtils.prepareHTML(results.getFormattedValue("customerAddress1")) : "&#160;")%></td></tr>
<tr class="row"><td>City, State, Zip:</td><td class="field"><%=(data ? StringUtils.prepareHTML(results.getFormattedValue("customerCity")) + ",&#160;" + StringUtils.prepareHTML(results.getFormattedValue("customerState")) + "&#160;" + StringUtils.prepareHTML(results.getFormattedValue("customerZip")) : "&#160;")%></td></tr>   
<tr class="row"><td>Company Tax ID Number:</td><td class="field"><%=(data ? StringUtils.prepareHTML(results.getFormattedValue("bankTaxId")) : "&#160;")%></td></tr>   
<tr><td class="spacer" colspan="2">&#160;</td></tr>
<tr><th>BANKING INFORMATION</th><th>This must be a U.S. Domestic Bank to use this form</th></tr> 
<tr class="row"><td>Name of Bank:</td><td class="field"><%=(data ? StringUtils.prepareHTML(results.getFormattedValue("bankName")) : "&#160;")%></td></tr>   
<tr class="row"><td>Street Address / PO Box:</td><td class="field"><%=(data ? StringUtils.prepareHTML(results.getFormattedValue("bankAddress1")) : "&#160;")%></td></tr>   
<tr class="row"><td>City, State, Zip:</td><td class="field"><%=(data ? StringUtils.prepareHTML(results.getFormattedValue("bankCity")) + ",&#160;" + StringUtils.prepareHTML(results.getFormattedValue("bankState")) + "&#160;" + StringUtils.prepareHTML(results.getFormattedValue("bankZip")) : "&#160;")%></td></tr>
<tr class="row"><td>Title on Bank Account:<br/><span class="note">(Exactly as listed on bank statement)</span></td><td class="field"><%=(data ? StringUtils.prepareHTML(results.getFormattedValue("accountTitle")) : "&#160;")%></td></tr>   
<tr><td>Checking or Savings Account?<br/><span class="note">(Please check appropriate box)</span></td><td>Checking Account&#160;<span class="fillin">&#160;&#160;<%=(data && "C".equalsIgnoreCase(results.getFormattedValue("accountType")) ? "X" : "&#160;")%>&#160;&#160;</span>&#160;&#160;&#160;&#160;&#160;Savings Account<span class="fillin">&#160;&#160;<%=(data && "S".equalsIgnoreCase(results.getFormattedValue("accountType")) ? "X" : "&#160;")%>&#160;&#160;</span></td></tr>                                       
<tr><td class="spacer" colspan="2">&#160;</td></tr>
<tr><th>EFT INFORMATION</th><th>Obtain this information directly from your bank</th></tr> 
<tr class="row"><td>Bank ABA Number:<br/><span class="note">(also known as Bank Routing Number)</span></td><td><%
    String tmp;
    if(data && (tmp=results.getFormattedValue("routingNumber").trim()).length() == 9) {
        for(int i = 0; i < tmp.length(); i++) {
            %><span class="fillin">&#160;<%=StringUtils.prepareHTML(tmp.substring(i, i+1))%>&#160;</span>&#160;<%
        }
    } else {
        for(int i = 0; i < 9; i++) {
            %>____&#160;<%
        }
    } 
%><span class="note">(must be 9-digit number)</span></td></tr>
<tr class="row"><td>Bank Account Number:</td><td class="field"><%=(data ? StringUtils.prepareHTML(results.getFormattedValue("accountNumber")) : "&#160;")%></td></tr>   
<tr><td class="spacer" colspan="2">&#160;</td></tr>
<tr><th>YOUR BANK CONTACT</th><th>Person at your bank who we can contact to verify Banking Information</th></tr> 
<tr class="row"><td>Contact Name:</td><td class="field"><%=(data ? StringUtils.prepareHTML(results.getFormattedValue("contactName")) : "&#160;")%></td></tr>
<tr class="row"><td>Contact Title:</td><td class="field"><%=(data ? StringUtils.prepareHTML(results.getFormattedValue("contactTitle")) : "&#160;")%></td></tr>
<tr class="row"><td>Contact Phone:</td><td class="field"><%=(data ? StringUtils.prepareHTML(results.getFormattedValue("contactTelephone")) : "&#160;")%></td></tr>
<tr class="row"><td>Contact Fax:</td><td class="field"><%=(data ? StringUtils.prepareHTML(results.getFormattedValue("contactFax")) : "&#160;")%></td></tr>
<tr><td class="spacer" colspan="2">&#160;</td></tr>
<tr><th colspan="2">AUTHORIZATION</th></tr> 
<tr class="row"><td>Name:</td><td class="field"><%=StringUtils.prepareHTML(RequestUtils.getAttribute(request, "simple.servlet.ServletUser.userProperties.fullName", String.class, false))%></td></tr>
<tr class="row"><td>Title:</td><td class="field">&#160;</td></tr>
<tr class="row"><td>Phone:</td><td class="field"><%=(data ? StringUtils.prepareHTML(results.getFormattedValue("customerTelephone")) : "&#160;")%></td></tr>
<tr class="row"><td>Fax:</td><td class="field"><%=(data ? StringUtils.prepareHTML(results.getFormattedValue("customerFax")) : "&#160;")%></td></tr>
<tr class="row"><td>E-mail:</td><td class="field"><%=StringUtils.prepareHTML(RequestUtils.getAttribute(request, "simple.servlet.ServletUser.email", String.class, false))%></td></tr>   
<tr><td>Authorized Signature:<br/><span class="note">(Must be Signed)</span></td><td><table style="border: 2pt solid black; font-size: 10pt;" width="100%"><col width="345"/><col/><tr><td>&#160;</td><td>Date:</td></tr></table></td></tr>
<tr><td class="spacer" colspan="2">&#160;</td></tr>
</table></td></tr>
<tr><td class="instructs" colspan="2">Please fax this form along with a copy of a voided check to (610) 989-9695.</td></tr> 
</table>
</body>
</html>