<%@page import="simple.servlet.InputForm"%>
<%@page import="com.usatech.layers.common.model.PayrollSchedule.PayrollScheduleType"%>
<%@page import="java.text.Normalizer.Form"%>
<%@page import="java.time.DayOfWeek"%>
<%@page import="com.usatech.layers.common.model.PayrollSchedule"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.servlet.RequestUtils"%>
<form enctype="multipart/form-data" name="criteria" method="post" action="preference_update.i" data-toggle="validateInline">
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="session-token" value="<%=RequestUtils.getSessionToken(request)%>" />
<script type="text/javascript" defer="defer">
	var terminalDisplaySelects = [];
	window.addEvent("domready", function() {
		new Form.Validator.Inline.Mask(document.forms["criteria"]);
<% Results userPreferenceResults = RequestUtils.getAttribute(request, "userPreferenceResults", Results.class, true);
	while(userPreferenceResults.next()){ %>
		addChoice("<%=userPreferenceResults.getValue("preferenceId", Integer.class)%>", <%=userPreferenceResults.isGroupBeginning(0)%>);
<% } %>
		addChoice(null, false);
	
	});
	function displaySelected() {
		if(this.getValue() != "") {
			if(this == terminalDisplaySelects.getLast()) {
				addChoice(null, false);
			}
		} else if(this != terminalDisplaySelects.getLast()) {
			this.getParent("tr").destroy();
			terminalDisplaySelects.erase(this);
		}
	}
	function addChoice(value, required) {
		var select = new Element("select", {name: "terminalDisplay", 'data-validators': required ? "required validate-unique-byname" : "validate-unique-byname"});
		select.addEvent("change", displaySelected.bind(select));
		if(!required) 
			select.adopt(new Element("option", {value: "", text: "-- None --"}));
<% Results preferenceResults = RequestUtils.getAttribute(request, "preferenceResults", Results.class, true);
	while(preferenceResults.next()){ %>
			select.adopt(new Element("option", {
			value: "<%=preferenceResults.getValue("preferenceId", Integer.class)%>", 
			text: "<%=StringUtils.prepareScript(preferenceResults.getFormattedValue("preferenceLabel"))%>", 
			selected: ("<%=preferenceResults.getValue("preferenceId", Integer.class)%>" == value ? "selected" : null)}));
<% } %>
		terminalDisplaySelects.push(select);
		var tr = new Element("tr");
		var td = new Element("td");
		td.adopt(select);
		if(required)
			td.adopt(new Element("span", {'class': "required", text: "*"}));
		tr.adopt(td);
		$("displayTable").adopt(tr);
	}
	function payrollScheduleTypeChanged() {
		var payrollScheduleType = $('payrollScheduleType').value;
		if (!payrollScheduleType || /NONE/.test(payrollScheduleType)) {
			$('payDay1Div').hide();
			$('payDay1').setAttribute('data-validators', '')
			$('payDay2Div').hide();
			$('payDay2').setAttribute('data-validators', '')
			$('payDayNote').hide();
		} else if (/SEMIMONTHLY/.test(payrollScheduleType)) {
			$('payDay1Div').show();
			$('payDay1').setAttribute('data-validators', 'required validate-date dateFormat:\'mm/dd/yyyy\'')
			$('payDay2Div').show();
			$('payDay2').setAttribute('data-validators', 'required validate-date dateFormat:\'mm/dd/yyyy\'')
			$('payDayNote').show();
		} else {
			$('payDay1Div').show();
			$('payDay1').setAttribute('data-validators', 'required validate-date dateFormat:\'mm/dd/yyyy\'')
			$('payDay2Div').hide();
			$('payDay2').setAttribute('data-validators', '')
			$('payDayNote').show();
		}
	}
</script><%
UsaliveUser user = (UsaliveUser) RequestUtils.getUser(request);
String errorMessage = RequestUtils.getAttribute(request, "errorMessage", String.class, false);
String successMessage = RequestUtils.getAttribute(request, "successMessage", String.class, false);
if((errorMessage != null && errorMessage.trim().length() > 0) || (successMessage != null && successMessage.trim().length() > 0)) {%>
<table class="messages">
<tr><td><ul class="message-list" id="message-list" ><%if(errorMessage != null && errorMessage.trim().length() > 0) {%>
		<li class="message-error"><%=StringUtils.prepareHTML(errorMessage)%></li><%}
		if(successMessage != null && successMessage.trim().length() > 0) {%>
		<li class="message-success"><%=StringUtils.prepareHTML(successMessage)%></li><%}%></ul>
</td></tr>
</table>
<%} %>
<div class="toggle-heading">
	<span class="caption">Preferences</span>
</div>
<div class="spacer5"></div>
<table class="fields" style="width:60em;">
<% int imageWidth=100;
	PayrollSchedule payrollSchedule = null;
	Results customerInfoResults = RequestUtils.getAttribute(request, "customerInfoResults", Results.class, false);
	if(customerInfoResults != null && customerInfoResults.next()) {
		InputForm form = RequestUtils.getInputForm(request);
		payrollSchedule = PayrollSchedule.loadFrom(customerInfoResults, form, true);
	String moreNote=""; 
	%>
	<tr class="tableHeader"><th colspan="3">Credit Card Statement Information</th></tr>
	<tr class="tableDataShade"><th>Doing Business As:</th><td colspan="2">
	<%if(user.isInternal()||(user.getMasterUser()!=null&&user.getMasterUser().isInternal())){ %>
	<input type="text" name="doingBusinessAs" maxlength="21" value="<%=StringUtils.prepareCDATA(StringUtils.trim(RequestUtils.getAttributeDefault(request, "doingBusinessAs", String.class, customerInfoResults.getFormattedValue("doingBusinessAs"))))%>" />
	<%}else{ %>
	<%=StringUtils.prepareHTML(customerInfoResults.getFormattedValue("doingBusinessAs"))%>
	<%} %>
	</td></tr>
	<tr class="tableDataShade"><th>Customer Service Phone:</th><td colspan="2"><input type="tel" name="customerServicePhone" data-validators="validate-phone" maxlength="20" value="<%=StringUtils.prepareCDATA(StringUtils.trim(RequestUtils.getAttributeDefault(request, "customerServicePhone", String.class, customerInfoResults.getFormattedValue("customerServicePhone"))))%>" /></td></tr>
	<tr class="tableDataShade"><th>Customer Service Email:</th><td colspan="2"><input type="email" name="customerServiceEmail" data-validators="validate-email" maxlength="70" value="<%=StringUtils.prepareCDATA(StringUtils.trim(RequestUtils.getAttributeDefault(request, "customerServiceEmail", String.class, customerInfoResults.getFormattedValue("customerServiceEmail"))))%>" /></td></tr>
	<%if(customerInfoResults.get("companyLogoName")!=null){ 
		moreNote=" Select and Save to Update"; %>
	<tr class="tableDataShade"><th><img onerror='this.style.display = "none"' src="<%=StringUtils.prepareScript("get_company_logo.i?customerId="+user.getCustomerId())%>" width="<%=imageWidth%>"/></th><td colspan="2"><input id="deleteCompanyLogo" type="submit" name="deleteCompanyLogo" value="Delete Company Logo" onclick="$('actionType').value='deleteCompanyLogo';"/></td></tr>
	<%} %>
	<tr class="tableDataShade"><th>Customer Company Logo:<%=moreNote %></th><td colspan="2"><input id="companyLogo" type="file" name="companyLogo" value="" title="Select the image file as company logo"/></td></tr>
	<tr><td colspan="3"><span class="status-info-success"><%=StringUtils.prepareHTML(RequestUtils.getTranslator(request).translate("company-logo-instruction","For company logo, please upload a 72 pixel resolution image file in JPEG, PNG or GIF format. The maximum height is 75px and maximum size is 50kb. The company logo will be displayed on ePort Online receipt, campaign email and MORE website."))%> </span></td></tr>
<%
/*
if(user.hasPrivilege(ReportingPrivilege.PRIV_MANAGE_CAMPAIGN)) {
	%><tr class="tableDataShade"><th>MORE Branded Site Sub-directory:</th><td colspan="2"><input type="text" name="prepaidSiteSubDirectory" data-validators="" value="<%=StringUtils.prepareCDATA(customerInfoResults.getFormattedValue("prepaidSiteSubDirectory"))%>"/></td></tr><%
}
if(user.hasPrivilege(ReportingPrivilege.PRIV_BACK_OFFICE)) {
	%><tr class="tableDataShade"><th>ePort Online Receipt Image:</th><td colspan="2"><img src="<%=StringUtils.prepareCDATA(customerInfoResults.getFormattedValue("backofficeImageUrl"))%>" height="30px"/><input type="file" name="backofficeReceiptImage" /></td></tr>
	<tr class="tableDataShade"><th>>ePort Online Link Url:</th><td colspan="2"><input type="text" name="backofficeLinkUrl" value="<%=StringUtils.prepareCDATA(customerInfoResults.getFormattedValue("backofficeLinkUrl"))%>"/></td></tr><%	
}*/
%>
<tr><td></td></tr>
<% }%>
<tr class="tableHeader"><th colspan="3">Show these device attributes in all reports</th></tr>
<tr><td colspan="3">
<table id="displayTable">
</table>
</td></tr>
<tr><td></td></tr>
<tr class="tableHeader"><th colspan="3">Configure device health limits:</th></tr>
<% Results userDeviceHealthPreferenceResults = RequestUtils.getAttribute(request, "userDeviceHealthPreferenceResults", Results.class, true);
	while(userDeviceHealthPreferenceResults.next()){%>
		<tr>
		<td><%=StringUtils.prepareHTML(userDeviceHealthPreferenceResults.getValue("deviceHealthPreferenceLabel", String.class))%>: <span class="required">*</span></td>
		<td><input type="text" value="<%=userDeviceHealthPreferenceResults.getValue("deviceHealthPreferenceValue", Integer.class)%>" data-validators="required validate-positive-integer" name="deviceHealthPreferenceId-<%=userDeviceHealthPreferenceResults.getValue("deviceHealthPreferenceId", Integer.class)%>" maxlength="4" size="4" /></td>
		<td>( Default is <%=userDeviceHealthPreferenceResults.getValue("deviceHealthPreferenceDefaultValue", Integer.class)%> )</td>
		</tr>
<% }%>


<% if (user.getUserType() == 8) {

	Results userDexResults = RequestUtils.getAttribute(request, "userDexResults", Results.class, true);	
	if(userDexResults.next()){%>
		<tr><td></td></tr>
		<tr class="tableHeader"><th colspan="3">DEX file parsing</th></tr>
		<tr>
		<td><%=StringUtils.prepareHTML(userDexResults.getValue("dexPreferenceLabel", String.class))%>: </td>
		<td><input type="checkbox" onclick="document.getElementById('dexPreferenceId-<%=userDexResults.getValue("dexPreferenceId", Integer.class)%>').value = this.checked ? 'Y' : 'N'" name="dexCheckbox-<%=userDexResults.getValue("dexPreferenceId", Integer.class)%>" <%if (userDexResults.getValue("dexPreferenceValue",char.class)=='Y') {%> checked="checked" <%}%> /></td>
		<td></td>
		</tr>
		<tr>
		<td></td>
		<td><input type="hidden" value="<%=userDexResults.getValue("dexPreferenceValue", char.class)%>" name="dexPreferenceId-<%=userDexResults.getValue("dexPreferenceId", Integer.class)%>" id="dexPreferenceId-<%=userDexResults.getValue("dexPreferenceId", Integer.class)%>"  /></td>
		<td></td>
		</tr>
<%}} %>

<tr><td></td></tr>
<% if (payrollSchedule != null) { %>
<tr class="tableHeader"><th colspan="3">Company Info</th></tr>
<tr>
	<td colspan="3">
		<table id="displayTable">
			<tr>
				<td style="width:8em;">Pay Period</td>
				<td>
					<% PayrollSchedule.PayrollScheduleType payrollScheduleType = payrollSchedule.getPayrollScheduleType(); %>
					<select name="payrollScheduleType" id="payrollScheduleType" onchange="payrollScheduleTypeChanged()">
					<% for(PayrollSchedule.PayrollScheduleType type : PayrollSchedule.PayrollScheduleType.values()) { %>
						<option value="<%=type.toString()%>" <%=payrollScheduleType == type ? "selected=\"selected\"" : "" %>><%=type.getDesc() %></option>
					<% } %>
					</select>
				</td>
				<td style="padding-left:1em;">
					<div id="payDay1Div" style="display:<%=payrollScheduleType != PayrollScheduleType.NONE ? "block" : "none"%>">
						<div style="width:12em; display:inline-block;">Pay Period End:</div>
						<input type="text" id="payDay1" size="8" maxlength="10" name="payDay1" data-validators="validate-date dateFormat:'mm/dd/yyyy'" data-label="Last Pay Date" data-toggle="calendar" data-format="%m/%d/%Y" value="<%=StringUtils.prepareCDATA(payrollSchedule.getPayrollScheduleData(1)) %>"/>
						<span class="required">*</span>
					</div>
					<div id="payDay2Div" style="display:<%=payrollScheduleType != PayrollScheduleType.NONE && payrollScheduleType.isMultiDate() ? "block" : "none"%>">
						<div style="width:12em; display:inline-block;">Pay Period End 2:</div>
						<input type="text" id="payDay2" size="8" maxlength="10" name="payDay2" data-validators="validate-date dateFormat:'mm/dd/yyyy'" data-label="Last Pay Date" data-toggle="calendar" data-format="%m/%d/%Y" value="<%=StringUtils.prepareCDATA(payrollSchedule.getPayrollScheduleData(2)) %>"/>
						<span class="required">*</span>
					</div>
					<div id="payDayNote" style="font-style:italic; padding-top:1em; display:<%=payrollScheduleType != PayrollScheduleType.NONE ? "block" : "none"%>">Select example last days of the pay period for any week or month. For end-of-month, choose the 31st.</div>
				</td>
			</tr>
		</table>
	</td>
</tr>
<%} %>
<tr><td></td></tr>

<% if (user.getUserType() == 8 && customerInfoResults != null) {%>
<tr class="tableHeader"><th colspan="3">Default settings</th></tr>
<tr colspan="3">
	<%
	char autoUpdate = 'N';
	autoUpdate = customerInfoResults.getFormattedValue("columnMapAutoUpdate").charAt(0);
	%>
	<td>Enable column map auto update</td>
	<td><input type="checkbox" name="columnMapAutoUpdate" id="columnMapAutoUpdate" value="<%=autoUpdate%>" onclick="document.getElementById('columnMapAutoUpdate').value = this.checked ? 'Y' : 'N'" <%if (autoUpdate == 'Y') {%> checked="checked"<%}%> /></td>
</tr>
<tr><td></td></tr>
<%}%>

<tr><td colspan="3"><span class="required">* Required</span></td></tr>
<tr>
	<td colspan="3" align="center">
		<input type="submit" value="Save Changes" name="Submit"/>
		<%if (!user.isInternal()){%>
		<input type="button" value="Batch Close Time" onclick="window.location.href = '/set_batch_close_time.i'" />
		<%}%>
	</td>
</tr>
</table>
</form>