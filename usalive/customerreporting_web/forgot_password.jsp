<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.RequestUtils"%>

<table id="layoutContainer">
	<tr id="signInBody">
	<td>
		<div id="signInPanel">
		<form name="info" method="post" action="request_password_reset.i" autocomplete="off" id="signInForm">
		<table align="center" class="loginLayout">		    
			<%
			String infoMessage = RequestUtils.getAttribute(request, "onloadMessage", String.class, false);
			if(!StringUtils.isBlank(infoMessage)) { 
			%>
			<tr><td><div class="successText"><%=StringUtils.prepareHTML(infoMessage)%></div></td></tr>
			<% 
			}
			String errorMessage = RequestUtils.getAttribute(request, "errorMessage", String.class, false);
			if(!StringUtils.isBlank(errorMessage)) { 
			%>
			<tr><td><div class="errorText"><%=StringUtils.prepareHTML(errorMessage)%></div></td></tr>
			<% } %>
		    <tr><td align="center"><div class="headerLogo"></div></td></tr>
		    <tr>
		    	<td><h4>Reset Password</h4></td>
		    </tr>
		    <tr>
		    	<td>
		    		Please enter your username and click "Reset Password" to reset your password.<br/>
		    		We will send you an email that will allow you to create a new password:
		    	</td>
		    </tr>
		    <tr>
		      <td>
		      	<input type="text" name="username" required="required" style="width: 250px"/>
		      	<input type="submit" value="Reset Password" class="button" />
		      	<a href="/" class="button">Cancel</a>		      	
		      </td>
		     </tr>
		</table>
		</form>
		</div>
	</td>
	</tr>