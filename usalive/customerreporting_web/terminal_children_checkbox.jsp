<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.text.StringUtils"%>
<jsp:useBean id="terminalResults" type="simple.results.Results" scope="request" />
<div><%
String instructions = RequestUtils.getAttribute(request, "instructions", String.class, false);
if(instructions != null && (instructions=instructions.trim()).length() > 0) {%><span class="instructions"><%=StringUtils.prepareHTML(instructions) %></span><%}
while(terminalResults.next()) {
	Integer[] columnIndexes = terminalResults.getValue("columnIndexes", Integer[].class);
	long terminalId = terminalResults.getValue("terminalId", long.class);
    if(terminalResults.isGroupBeginning(0)) {
		%><table class="terminalChecklist">
        <thead><tr class="tableHeader"><th></th><%
        String[] columnLabels = terminalResults.getValue("columnLabels", String[].class);
        for(int i = 0; i < columnIndexes.length; i++) {
        	if(columnIndexes[i] != null) {
            %><th><a data-toggle="sort" data-sort-type="STRING"><%=StringUtils.prepareHTML(columnLabels[i])%></a></th><%
        	}
        } %></tr></thead><%
	}
    %><tr><td><input id="terminals" type="checkbox" name="terminals" value="<%=terminalId%>" /></td><%
    for(int i = 0; i < columnIndexes.length; i++) {
        if(columnIndexes[i] != null) {
            %><td data-sort-value="<%=StringUtils.encodeForHTMLAttribute(terminalResults.getFormattedValue("data" + columnIndexes[i]))%>">
            <%=StringUtils.prepareHTML(terminalResults.getFormattedValue("data" + columnIndexes[i]))%></td><%
        }
    }%></tr><%
    if(terminalResults.isGroupEnding(0)) {
    	%></table><%
    }
}
if(terminalResults.getRow() <= 1) { 
	%><span class="no-terminals">No devices found</span><%
} %></div>