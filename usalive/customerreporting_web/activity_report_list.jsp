<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.usatech.report.ReportRequestFactory"%>
<%@page import="simple.io.Log"%>
<%@page import="simple.results.BeanException"%>
<%@page import="simple.db.DataLayerException"%>
<%@page import="java.sql.SQLException"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="java.util.regex.Matcher"%>
<%@page import="java.util.regex.Pattern"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.text.RegexUtils"%>
<%@page import="simple.results.Results.Aggregate"%>
<%@page import="simple.text.StringUtils"%>
<jsp:useBean id="cannedReports" type="simple.results.Results" scope="request" />
<jsp:useBean id="userDefinedReports" type="simple.results.Results" scope="request" />
<jsp:useBean id="reportRegisterResults" type="simple.results.Results" scope="request" />
<%! 
protected final Pattern IS_FOLIO_REPORT_PATTERN = Pattern.compile("(?:./)?(?:(activity|activity_summary|activity_graph|activity_ext|activity_detail|activity_detail_ext|activity_rollup|diagnostic|dex_status)\\.i(\\?.*)?|(run_report|run_report_async|selection_date_range|select_date_range_frame|transaction_totals|run_sql_folio_report_async|select_date_range_frame_sqlfolio|select_payperiod_frame_sqlfolio|selection-date)\\.(?:i|html)(\\?(?:.+&)?(folioId|reportId|basicReportId)=(\\d+)(?:&|$).*))");
//Groups: 1 - base configurable parameters url, 2 - query string, 3 - base run report url, 4 - query string, 5 - run report param name, 6 - run report param value
protected final Pattern OUTPUT_TYPE_PATTERN = Pattern.compile("[?&]outputType=(\\d+)(&|$)");
%>
<span class="caption">Saved Reports</span>
<div class="spacer5"></div>
<div class="topMenu">
<div id="tabContainer">

<script type="text/javascript">
    function showTab(tab) {
        var selectedDiv = $("div_" + tab.id.substring(tab.id.lastIndexOf("_")+1));
       	tab.getSiblings().each(function(li) {
			li.removeClass("topMenuItemSelected");
		});
		tab.addClass("topMenuItemSelected");
		if(selectedDiv) {
			selectedDiv.getSiblings().each(function(li) {
				li.removeClass("selectedTabContentDiv");
			});
			selectedDiv.addClass("selectedTabContentDiv");			
		}
    }
    window.addEvent('domready', function() { showTab($("tab_1")); });
</script><%
cannedReports.addGroup("category");
String[] categories;
if(cannedReports.next()) {
	categories = cannedReports.getAggregate("category", null, Aggregate.SET, String[].class);
} else {
	categories = null;
}
String linkExtra;
UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
if(user != null)
	linkExtra = "&amp;profileId=" + user.getUserId();
else
	linkExtra = "";	
%>
<ul><%
if(categories != null)
	for(int i = 0; i < categories.length; i++) {
		String cat = (categories[i] ==  null || categories[i].trim().length() == 0 ? "Other" : categories[i]);
		%>
		<li id="tab_<%=i+1%>" class="topMenuItem">
			<a onclick="showTab($(this).getParent());"
	    		title="A list of all <%=StringUtils.prepareCDATA(cat)%> reports" onmouseover="window.status = 'A list of all <%=StringUtils.prepareScript(cat)%> reports'; return true;"
	    		onmouseout="window.status = ''"><%=StringUtils.prepareHTML(cat.toLowerCase())%></a>
	    </li>
	<%
	}
	if(!reportRegisterResults.isGroupEnding(0)) {
	%>
		<li id="tab_rr" class="topMenuItem">
			<a onclick="showTab($(this).getParent());"
    			title="A list of Report Register reports you can manually run" onmouseover="window.status = 'A list of Report Register reports you can manually run'; return true;"
    			onmouseout="window.status = ''">report register</a>
    	</li><%
	}%>
		<li id="tab_0" class="topMenuItem">
			<a onclick="showTab($(this).getParent());"
    			title="A list of all reports designed and saved by you" onmouseover="window.status = 'A list of all reports designed and saved by you'; return true;"
    			onmouseout="window.status = ''">user defined</a>
    	</li>
</ul>
</div>
<div class="tabContents">
<div id="div_0" class="tabContentDiv"><%
while(userDefinedReports.next()) {
	if(userDefinedReports.isGroupBeginning(0)) {
		%><ul class="userDefinedReportList"><%
	}
	String href = userDefinedReports.getFormattedValue("href");
    %><li><a href="<%=StringUtils.prepareCDATA(href)
    %><%=href.contains("?")?"&amp;":"?"%>reportTitle=<%=StringUtils.prepareURLPart(userDefinedReports.getFormattedValue("title"))%><%=linkExtra %>"
    title="<%=StringUtils.encodeForHTMLAttribute(userDefinedReports.getFormattedValue("description"))%>" onmouseover="window.status = '<%=StringUtils.encodeForJavaScript(userDefinedReports.getFormattedValue("description"))%>'; return true;"
    onmouseout="window.status = ''"><%=StringUtils.prepareHTML(userDefinedReports.getFormattedValue("title"))%></a>&#160;
    <a class="delete" href="delete_saved_report_link.i?linkId=<%=StringUtils.prepareURLPart(userDefinedReports.getFormattedValue("id"))%>&amp;reportTitle=<%=StringUtils.prepareURLPart(userDefinedReports.getFormattedValue("title"))%>&amp;session-token=<%=StringUtils.prepareURLPart(RequestUtils.getSessionToken(request)) %>" title="Delete"><div></div></a>
    <% String[] groups = RegexUtils.match(".+[?](?:(.+)&)?referrer=([^&]+)(?:&(.+))?", href, null);
    if(groups != null && groups.length > 3 && !StringUtils.isBlank(groups[2])) { %>
    <a class="configure" href="<%=StringUtils.prepareURLPart(groups[2])%>?<%
	    if(!StringUtils.isBlank(groups[1])) {%><%=StringUtils.prepareCDATA(groups[1])%><%
	        if(!StringUtils.isBlank(groups[3])) {%>&amp;<% }
	    }
        if(!StringUtils.isBlank(groups[3])) {%><%=StringUtils.prepareCDATA(groups[3])%><% }            
	    %>" title="Configure"><div></div></a><%
    }
    Matcher matcher = IS_FOLIO_REPORT_PATTERN.matcher(href);
    if(matcher.matches()) { 
    	boolean graph = false;
        Matcher matcher2 = OUTPUT_TYPE_PATTERN.matcher(href);
    	if(matcher2.find()) {
    		if("22".equals(matcher2.group(1))) 
    			href = href.substring(0, matcher2.start() + (StringUtils.isBlank(matcher2.group(2)) ? 0 : 1)) + href.substring(matcher2.end() + 1);
    		else if("28".equals(matcher2.group(1))) {
                href = (StringUtils.isBlank(matcher2.group(2)) ? href.substring(0, matcher2.start()) : href.substring(0, matcher2.start() + 1) + href.substring(matcher2.end() + 1));
                graph = true;
            } else
    			href = null;		
    	} else if(!StringUtils.isBlank(matcher.group(1))) {
    		switch(matcher.group(1)) {
    			case "activity_graph":
    				graph = true;
    				break;
    		}
    	} else if(!StringUtils.isBlank(matcher.group(5)) && !StringUtils.isBlank(matcher.group(6))) {
    		long id = ConvertUtils.getLongSafely(matcher.group(6), 0L);
    		if(id > 0) {
    			Map<String,Object> params = new HashMap<>();
    			try {
                    switch(matcher.group(5)) {
                        case "basicReportId":
                            params.put("reportId", id);
		                    DataLayerMgr.selectInto("GET_BASIC_REPORT_OUTPUT_TYPE", params);
		                    break;
		    			case "reportId":
		    				params.put("reportId", id);
	                        DataLayerMgr.selectInto("GET_FOLIO_REPORT_OUTPUT_TYPE", params);
	                        break;
		    			case "folioId":
		    				params.put("folioId", id);
	                        DataLayerMgr.selectInto("GET_FOLIO_OUTPUT_TYPE", params);
	                        break;
		    		}
                } catch(SQLException | DataLayerException | BeanException e) {
                    //ignore
                    Log.getLog().warn("Could not get default output type", e);
                }               
                int outputType = ConvertUtils.getIntSafely(params.get("outputType"), 0);
                if(outputType == 0)
                    href = null;
                else
                    graph = ReportRequestFactory.isGraphOutputType(outputType); 
    		}
    	}
    	if(!StringUtils.isBlank(href)) {
	        String sep;
	        if(!StringUtils.isBlank(matcher.group(2)) || !StringUtils.isBlank(matcher.group(4)))
	        	sep = "&amp;";
	        else
	        	sep = "?";%>
            <a class="report-link-csv" href="<%=StringUtils.prepareCDATA(href)%><%=sep%>outputType=21" title="View in Comma-separated values"><div></div></a>
            <a class="report-link-pdf<% if(graph) { %>-chart<%} %>" href="<%=StringUtils.encodeForHTMLAttribute(href)%><%=sep%>outputType=<%=graph ? 31 : 24 %>" title="View in PDF"><div></div></a>
            <a class="report-link-doc<% if(graph) { %>-chart<%} %>" href="<%=StringUtils.prepareCDATA(href)%><%=sep%>outputType=<%=graph ? 35 : 23 %>" title="View in Word Document"><div></div></a>
            <a class="report-link-excel<% if(graph) { %>-chart<%} %>" href="<%=StringUtils.prepareCDATA(href)%><%=sep%>outputType=<%=graph ? 34 : 27 %>" title="View in Excel"><div></div></a>
	    	<%
	    }
    } %>    
    </li><% 
    if(userDefinedReports.isGroupEnding(0)) {
   	%></ul><%
    }
}
if(userDefinedReports.getRow() <= 1) {
    %>You have not yet built and saved any reports. <a href="tabs.i?usage=B" onmouseover="window.status = 'Sales Activity'; return true;"
        onmouseout="window.status = ''" title="Create your own criteria for viewing Sales Information">Click here</a> to build a report or select one of the tabs above to view a list of pre-defined reports.<%
}%></div><%
if(!reportRegisterResults.isGroupEnding(0)) {
	%><div id="div_rr" class="tabContentDiv"><ul class="reportRegisterList"><%
	while(reportRegisterResults.next()) {
		%><li><a href="basic_report_prompt.html?reportId=<%=reportRegisterResults.getValue("id", Integer.class)%><%=linkExtra %>"
			    title="<%=StringUtils.encodeForHTMLAttribute(reportRegisterResults.getFormattedValue("description"))%>" onmouseover="window.status = '<%=StringUtils.prepareScript(reportRegisterResults.getFormattedValue("description"))%>'; return true;"
			    onmouseout="window.status = ''"><%=StringUtils.prepareHTML(reportRegisterResults.getFormattedValue("title"))%></a></li><%			    
	}%></ul></div><%
}
int n = 1;
do {
    if(cannedReports.isGroupBeginning("category")) {
    	 %><div id="div_<%=n++%>" class="tabContentDiv"><ul class="cannedReportList"><%
    }
    String href = cannedReports.getFormattedValue("href");
    %><li><a href="<%=StringUtils.prepareCDATA(href)
    %><%=href.contains("?")?"&amp;":"?"%>reportTitle=<%=StringUtils.prepareURLPart(cannedReports.getFormattedValue("title"))%><%=linkExtra %>"
    title="<%=StringUtils.prepareCDATA(cannedReports.getFormattedValue("description"))%>" onmouseover="window.status = '<%=StringUtils.prepareScript(cannedReports.getFormattedValue("description"))%>'; return true;"
    onmouseout="window.status = ''"><%=StringUtils.prepareHTML(cannedReports.getFormattedValue("title"))%></a>
    <% String[] groups = RegexUtils.match(".+[?](?:(.+)&)?referrer=([^&]+)(?:&(.+))?", href, null);
    if(groups != null && groups.length > 3 && !StringUtils.isBlank(groups[2])) { %>
    <a class="configure" href="<%=StringUtils.prepareURLPart(groups[2])%>?<%
        if(!StringUtils.isBlank(groups[1])) {%><%=StringUtils.prepareCDATA(groups[1])%><%
            if(!StringUtils.isBlank(groups[3])) {%>&amp;<% }
        }
        if(!StringUtils.isBlank(groups[3])) {%><%=StringUtils.prepareCDATA(groups[3])%><% }            
        %>" title="Configure"><div></div></a><%
    }
    Matcher matcher = IS_FOLIO_REPORT_PATTERN.matcher(href);
    if(matcher.matches()) { 
    	Log.getLog().info("Url matches with groups: [1=" + matcher.group(1) + "; 2=" + matcher.group(2) + "; 3=" + matcher.group(3) + "; 4=" + matcher.group(4) + "; 5=" + matcher.group(5) + "; 6=" + matcher.group(6) + "]");
        boolean graph = false;
        Matcher matcher2 = OUTPUT_TYPE_PATTERN.matcher(href);
        if(matcher2.find()) {
            if("22".equals(matcher2.group(1))) 
                href = (StringUtils.isBlank(matcher2.group(2)) ? href.substring(0, matcher2.start()) : href.substring(0, matcher2.start() + 1) + href.substring(matcher2.end() + 1));
            else if("28".equals(matcher2.group(1))) {
                href = (StringUtils.isBlank(matcher2.group(2)) ? href.substring(0, matcher2.start()) : href.substring(0, matcher2.start() + 1) + href.substring(matcher2.end() + 1));
                graph = true;
            } else
                href = null;        
        } else if(!StringUtils.isBlank(matcher.group(1))) {
            switch(matcher.group(1)) {
                case "activity_graph":
                	graph = true;
                    break;
            }
        } else if(!StringUtils.isBlank(matcher.group(5)) && !StringUtils.isBlank(matcher.group(6))) {
            long id = ConvertUtils.getLongSafely(matcher.group(6), 0L);
            if(id > 0) {
                Map<String,Object> params = new HashMap<>();
                try {
                    switch(matcher.group(5)) {
                        case "basicReportId":
                            params.put("reportId", id);
                            DataLayerMgr.selectInto("GET_BASIC_REPORT_OUTPUT_TYPE", params);
                            break;
                        case "reportId":
                            params.put("reportId", id);
                            DataLayerMgr.selectInto("GET_FOLIO_REPORT_OUTPUT_TYPE", params);
                            break;
                        case "folioId":
                            params.put("folioId", id);
                            DataLayerMgr.selectInto("GET_FOLIO_OUTPUT_TYPE", params);
                            break;
                    }
                } catch(SQLException | DataLayerException | BeanException e) {
                    //ignore
                    Log.getLog().warn("Could not get default output type", e);
                }               
                int outputType = ConvertUtils.getIntSafely(params.get("outputType"), 0);
                if(outputType == 0)
                	href = null;
                else
                    graph = ReportRequestFactory.isGraphOutputType(outputType); 
            }
        }
        if(!StringUtils.isBlank(href)) {
	        String sep;
	        if(!StringUtils.isBlank(matcher.group(2)) || !StringUtils.isBlank(matcher.group(3)))
	            sep = "&amp;";
	        else
	            sep = "?";%>
	        <a class="report-link-csv" href="<%=StringUtils.prepareCDATA(href)%><%=sep%>outputType=21" title="View in Comma-separated values"><div></div></a>
	        <a class="report-link-pdf<% if(graph) { %>-chart<%} %>" href="<%=StringUtils.prepareCDATA(href)%><%=sep%>outputType=<%=graph ? 31 : 24 %>" title="View in PDF"><div></div></a>
	        <a class="report-link-doc<% if(graph) { %>-chart<%} %>" href="<%=StringUtils.encodeForHTMLAttribute(href)%><%=sep%>outputType=<%=graph ? 35 : 23 %>" title="View in Word Document"><div></div></a>
	        <a class="report-link-excel<% if(graph) { %>-chart<%} %>" href="<%=StringUtils.prepareCDATA(href)%><%=sep%>outputType=<%=graph ? 34 : 27 %>" title="View in Excel"><div></div></a>
	        <% 
        }
    }%>    
    </li><%
    if(cannedReports.isGroupEnding("category")) {
        %></ul></div><%       
    }
} while(cannedReports.next()) ;%></div>
</div>