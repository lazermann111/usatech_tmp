<jsp:useBean id="countResults" type="simple.results.Results" scope="request" />
<%@page import="simple.servlet.RequestUtils,com.usatech.usalive.servlet.UsaliveUser"%>
<%
UsaliveUser user = RequestUtils.getAttribute(request, "simple.servlet.ServletUser", UsaliveUser.class, true, "session"); %>
<span class="caption">Dashboard</span>
<div class="spacer5"></div>
<div>
<table class="myStats">
    <tr><th class="headerRow" colspan="2">Your Profile:</th></tr><%
    int countRecentAlert;
    int countDeviceWithoutBankAcct;
    int countDevice;
    int countNoComm;
    int countNoCredit;
    int countNoCash;
    int countNoFill;
    int countNoDex;
    int prefNoCommDays;
    int prefNoCreditDays;
    int prefNoCashDays;
    int prefNoFillDays;
    int prefNoDexDays;
    if(countResults.next()) {
    	countRecentAlert = countResults.getValue("countRecentAlert", Integer.class);
    	countDeviceWithoutBankAcct = countResults.getValue("countDeviceWithoutBankAcct", Integer.class);   	
    	countDevice = countResults.getValue("countDevice", Integer.class);
    	countNoComm = countResults.getValue("countNoComm", Integer.class);
        countNoCredit = countResults.getValue("countNoCredit", Integer.class);
        countNoCash = countResults.getValue("countNoCash", Integer.class);
        countNoFill = countResults.getValue("countNoFill", Integer.class);
        countNoDex = countResults.getValue("countNoDex", Integer.class);
        prefNoCommDays = countResults.getValue("prefNoCommDays", Integer.class);
        prefNoCreditDays = countResults.getValue("prefNoCreditDays", Integer.class);
        prefNoCashDays = countResults.getValue("prefNoCashDays", Integer.class);
        prefNoFillDays = countResults.getValue("prefNoFillDays", Integer.class);
        prefNoDexDays = countResults.getValue("prefNoDexDays", Integer.class);
    } else {
    	countRecentAlert = 0;
    	countDeviceWithoutBankAcct = 0;
    	countDevice = 0;
    	countNoComm = 0;
    	countNoCredit = 0;
    	countNoCash = 0;
    	countNoFill = 0;
    	countNoDex = 0;
    	prefNoCommDays = 0;
        prefNoCreditDays = 0;
        prefNoCashDays = 0;
        prefNoFillDays = 0;
        prefNoDexDays = 0;
    }%>
    <tr><th>Devices:</th><td><a href="run_report_async.i?folioId=1087&amp;outputTypeId=22&amp;params.ShowAll=1" title="View Details"><%=countDevice%></a></td></tr>
    <tr><th>Bank Accounts:</th><td><% if(user.hasPrivilege("3" /*Create Bank Acct*/) && user.getPendingBankAccountCount() > 0) { %><span class="required">*</span>&#160;<%} %><a href="run_report_async.i?folioId=493&amp;promptForParams=no" title="View Details"><%=user.getActiveBankAccountCount() + user.getPendingBankAccountCount()%></a></td></tr>
    <tr><th>Alerts in the last 24 hours:</th><td><a href="diagnostic.i?params.beginDate={*-86400000}&amp;params.endDate={*0}" title="View Details"><%=countRecentAlert%></a></td></tr><%
    if(countDeviceWithoutBankAcct > 0) { %>
    <tr><th>Devices Without a Bank Account:</th><td><span class="baNote">*</span>&#160;<a href="assign_bank_acct_to_terminal.i" title="View Details"><%=countDeviceWithoutBankAcct%></a></td></tr><% } 
    if(countNoComm > 0) { %>
    <tr><th>Active Devices Not Communicating in the last <%=prefNoCommDays%> days:</th><td><a href="run_report_async.i?folioId=1087&amp;outputTypeId=22&amp;params.NoComm=Y&amp;params.NoCredit=N&amp;params.NoCash=N&amp;params.NoFill=N&amp;params.NoDex=N" title="View Details of Devices Not Communicating"><%=countNoComm%></a></td></tr><%
    } if(countNoCredit > 0) { %>
    <tr><th>Active Devices Without Credit Sales in the last <%=prefNoCreditDays%> days:</th><td><a href="run_report_async.i?folioId=1087&amp;outputTypeId=22&amp;params.NoComm=N&amp;params.NoCredit=Y&amp;params.NoCash=N&amp;params.NoFill=N&amp;params.NoDex=N" title="View Details of Devices Without Recent Credit Sales"><%=countNoCredit%></a></td></tr><%
    } if(countNoCash > 0) { %>
    <tr><th>Active Devices Without Cash Sales in the last <%=prefNoCashDays%> days:</th><td><a href="run_report_async.i?folioId=1087&amp;outputTypeId=22&amp;params.NoComm=N&amp;params.NoCredit=N&amp;params.NoCash=Y&amp;params.NoFill=N&amp;params.NoDex=N" title="View Details of Devices Without Recent Cash Sales"><%=countNoCash%></a></td></tr><%
    } if(countNoFill > 0) { %>
    <tr><th>Active Devices Without Driver Fill in the last <%=prefNoFillDays%> days:</th><td><a href="run_report_async.i?folioId=1087&amp;outputTypeId=22&amp;params.NoComm=N&amp;params.NoCredit=N&amp;params.NoCash=N&amp;params.NoFill=Y&amp;params.NoDex=N" title="View Details of Devices Without Recent Driver Fill"><%=countNoFill%></a></td></tr><%
    } if(countNoDex > 0) { %>
    <tr><th>Active Devices Without DEX Transfer in the last <%=prefNoDexDays%> days:</th><td><a href="run_report_async.i?folioId=1087&amp;outputTypeId=22&amp;params.NoComm=N&amp;params.NoCredit=N&amp;params.NoCash=N&amp;params.NoFill=N&amp;params.NoDex=Y" title="View Details of Devices Without Recent DEX Transfer"><%=countNoDex%></a></td></tr><%
    } %> 
    <tr><td colspan="2"><a href="activity_total_cashless_parameters.i" title="Total Annual Cashless Sales">Total Annual Cashless Sales</a></td></tr>
</table>
<%
if(countDeviceWithoutBankAcct > 0) { %>
        <ul class="baNote"><li>A device must be assigned to a bank account in order for you to receive its revenue. Click&#160;<a href="assign_bank_acct_to_terminal.i">HERE</a> to assign.</li></ul>
    <% } %>
<ul class="baNote"><li>To configure device health preferences, click&#160;<a href="preferences.i">HERE</a></li></ul>
<jsp:include page="no_terminals.jsp" flush="true"/>
</div>
