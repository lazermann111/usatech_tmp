<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%@page import="simple.servlet.RequestUtils"%>
<%/* we are going to hard code this for now (BSK 09-22-04)*/
UsaliveUser user = RequestUtils.getAttribute(request, SimpleServlet.ATTRIBUTE_USER, UsaliveUser.class, true);
String deviceName = "Device";
String usatBrand = "";
String agreeType = "License";
Long dealerId = RequestUtils.getAttribute(request, "dealerId", Long.class, false);
if(dealerId != null && dealerId == 302L) { /* MEI */
	deviceName = "MEI System";
	usatBrand = "USALive&#174;";
	agreeType = "Services";
}
%>
<div class="licenseInstructions">
<p><a href="javascript: openLicense();" onmouseover="window.status = 'Click to print Your <%=agreeType%> Agreement'; return true;"
        onmouseout="window.status = ''" title="Print Your <%=agreeType%> Agreement">Please click here</a> to open a window with the <%=usatBrand%> <%=agreeType%> Agreement that you 
must sign before you can activate any <%=deviceName%>s you have obtained and before 
any transactions are credited to your account and before you 
are paid for those transactions. Please print the <%=agreeType%> Agreement, sign it,
and fax the form back to USA Technologies at (610) 989-9695. If you have any 
questions do not hesitate to call USA Technologies at (888) 561-4748. Thank you.
</p><%
if(user.isMissingBankAccount()) {
       %><p>When you have finished printing, signing, and faxing the <%=agreeType%> Agreement, then goto&#160;
    <a href="new_bank_acct.i" onmouseover="window.status = 'Create a New Bank Account'; return true;"
        onmouseout="window.status = ''" title="Establish a Bank Account to Receive Payments from USA Technologies">New Bank Acct</a> 
    to establish a Bank Account.</p><%
} %>      
<script type="text/javascript" defer="defer">
function openLicense() {
   window.open("ePortLicense.pdf","LicenseAgreement","directories=no,height=200,width=400,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=yes");
}
//openLicense();
</script>
</div> 