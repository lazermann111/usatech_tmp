<%@page import="simple.servlet.SimpleServlet,simple.servlet.InputForm,java.util.Map,simple.bean.ConvertUtils,simple.servlet.InputFile,simple.servlet.RequestUtils,simple.text.StringUtils"%>
<%@page import="java.util.Set,java.util.HashSet,java.util.Arrays,simple.servlet.SimpleAction,simple.servlet.NotLoggedOnException,simple.servlet.NoSessionTokenException,simple.servlet.InvalidSessionTokenException"%>
<%! protected static final Set<String> dontForwardPaths = new HashSet<String>(Arrays.asList(new String[] {
	"/login_prompt.i",
	"/logout.i",
	"/login.i",
	"/login_dialog.i",
})); 
protected static final Set<String> ignoreParameters = new HashSet<String>(Arrays.asList(new String[] {
	"username",
	"password",
	"loginPromptPage",
	"simple.servlet.steps.LogonStep.forward",
	"content",
	"session-token",
	"selectedMenuItem"
})); %><%
Long selectedMenuItem = RequestUtils.getAttribute(request, "selectedMenuItem", Long.class, false);
String contentPath = request.getServletPath();
if(contentPath == null || dontForwardPaths.contains(contentPath)) {
	contentPath = "/home.i";
	selectedMenuItem = 110L;
}
boolean fragment = Boolean.TRUE.equals(RequestUtils.getAttribute(request, "fragment", Boolean.class, false));
boolean redirectHome = Boolean.TRUE.equals(RequestUtils.getAttribute(request, "redirectHome", Boolean.class, false));
Throwable exception = (Throwable)RequestUtils.getAttribute(request, SimpleAction.ATTRIBUTE_EXCEPTION, false);
boolean notLoggedOn = (exception instanceof NotLoggedOnException) || (exception instanceof NoSessionTokenException);
boolean invalidRequest = (exception instanceof InvalidSessionTokenException);
if(fragment) {
	%><div id="loginPopup">
	<script type="text/javascript">
		var loginDialog = new Dialog.Url({url: "login_dialog.i", data : {
			"simple.servlet.steps.LogonStep.forward":"<%=StringUtils.prepareScript(contentPath)%>"<%
if(selectedMenuItem != null) { %>
           , selectedMenuItem:<%=selectedMenuItem%><%
}
InputForm form = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
for(Map.Entry<String,Object> param : form.getParameters().entrySet()) {
	if(ignoreParameters.contains(param.getKey())) continue;
	if(param.getValue() instanceof String) {
		%>,"<%=StringUtils.prepareScript(param.getKey())%>":"<%=StringUtils.prepareScript((String)param.getValue())%>"<%
	} else if(param.getValue() instanceof String[]) {
		String[] values = (String[])param.getValue();
		for(int i = 0; i < values.length; i++) {
            %>,"<%=StringUtils.encodeForJavaScript(param.getKey())%>":"<%=StringUtils.encodeForJavaScript(values[i])%>"<%
        }
	} else if(param.getValue() instanceof InputFile || param.getValue() instanceof InputFile[]) {
		RequestUtils.storeForNextRequest(request.getSession(), param.getKey(), param.getValue());
	} else if(param.getValue() != null) {
		String value = ConvertUtils.getStringSafely(param.getValue());
        %>,"<%=StringUtils.encodeForJavaScript(param.getKey())%>":"<%=StringUtils.encodeForJavaScript(value)%>"<%
	}
}%>
		}, width:255, height:185, title: "Please Log In"});
		loginDialog.show();
	</script>
	</div><%
} else {
	if(redirectHome){
		%><script type="text/javascript" > 
		window.location.href="home.i";
		</script>
<%
	}else{
	%>
	<table id="layoutContainer">
	<tr id="signInBody">
	<td>
	<div id="signInPanel">
	<form name="info" method="post" action="login.i" onsubmit="App.updateClientTimestamp(this)" autocomplete="off" id="signInForm">
	<input type="hidden" name="simple.servlet.steps.LogonStep.forward" value="<%=StringUtils.prepareCDATA(contentPath)%>" />
	<input type="hidden" name="loginPromptPage" value="login_prompt"/><%
if(selectedMenuItem != null) { %>
    <input type="hidden" name="selectedMenuItem" value="<%=selectedMenuItem%>" /><%
}
InputForm form = (InputForm) request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
for(Map.Entry<String,Object> param : form.getParameters().entrySet()) {
	if(ignoreParameters.contains(param.getKey())) continue;
	if(param.getValue() instanceof String) {
		%><input type="hidden" readonly="readonly" name="<%=StringUtils.prepareCDATA(param.getKey())%>" value="<%=StringUtils.encodeForHTMLAttribute(param.getValue())%>"/><%
	} else if(param.getValue() instanceof String[]) {
		String[] values = (String[])param.getValue();
		for(int i = 0; i < values.length; i++) {
            %><input type="hidden" readonly="readonly" name="<%=StringUtils.encodeForHTMLAttribute(param.getKey())%>" value="<%=StringUtils.encodeForHTMLAttribute(values[i])%>"/><%
        }
	} else if(param.getValue() instanceof InputFile || param.getValue() instanceof InputFile[]) {
		RequestUtils.storeForNextRequest(request.getSession(), param.getKey(), param.getValue());
	} else if(param.getValue() != null) {
		String value = ConvertUtils.getStringSafely(param.getValue());
        %><input type="hidden" readonly="readonly" name="<%=StringUtils.encodeForHTMLAttribute(param.getKey())%>" value="<%=StringUtils.encodeForHTMLAttribute(value)%>"/><%
	}
}
String infoMessage = RequestUtils.getAttribute(request, "onloadMessage", String.class, false);
if(!StringUtils.isBlank(infoMessage)) { 
%><div class="confirmText"><%=StringUtils.prepareHTML(infoMessage)%></div><% 
}
String errorMessage = RequestUtils.getAttribute(request, "errorMessage", String.class, false);
if(!StringUtils.isBlank(errorMessage)) { 
%><div class="errorText"><%=StringUtils.prepareHTML(errorMessage)%></div><% 
}
if(notLoggedOn && (null != RequestUtils.getSessionToken(request) || null != request.getParameter(SimpleServlet.REQUEST_SESSION_TOKEN))) { 
%><div class="errorText"><%=RequestUtils.getTranslator(request).translate("login-expired-prompt", "Your session has expired, please re-enter your login information.") %></div><% 
} else if(invalidRequest) { 
%><div class="errorText"><%=RequestUtils.getTranslator(request).translate("invalid-request", "The request was invalid, please login again for the protection of your account.") %></div><% 
}
String tmp;%>
<table><tr><td class="loginChoiceExisting">
<table class="loginLayout">
    <tr><td colspan="2" align="center"><div class="headerLogo"></div></td></tr>
    <tr><th>User Name:&#160;</th><td><input type="text" name="username" value="<%=((tmp=form.getStringSafely("username", null)) == null ? "" : StringUtils.prepareCDATA(tmp))%>" required="required"/></td></tr>
    <tr><th>Password:</th><td><input type="password" name="password" value="" autocomplete="off" required="required"/></td></tr>
    <tr class="loginButtonRow"><td colspan="2" align="right"><input type="submit" tabindex="0" value="Sign In"/></td></tr>
</table>
  <p><a tabindex="0" href="reset_password_prompt.i" onmouseover="window.status = 'Forgot your password?'; return true;"
        onmouseout="window.status = ''">Forgot your password?</a><br/></p>
</td>
</tr>        
</table>
</form>
</div>
<script type="text/javascript" defer="defer"> 
window.addEvent('domready', function() {
	try {
		if (document.forms["info"].elements["username"].value == "")
	    	document.forms["info"].elements["username"].focus();
		else
			document.forms["info"].elements["password"].focus();
	} catch (e) { }
});
</script>
</td>
</tr>
<%}}%>