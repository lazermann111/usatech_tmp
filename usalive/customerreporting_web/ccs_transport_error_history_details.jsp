<%@page
	import="simple.servlet.RequestUtils,simple.text.StringUtils,simple.results.Results"%>
<%
	Results ccsTransportErrorResults = RequestUtils.getAttribute(
			request, "ccsTransportErrorResults", Results.class, false);
	int rowNum = 1;
%>
<div>
	<table class="reportTitle">
		<tr>
			<td>
				<div class="title2">Transport Error History Details </div>

				<table class="reportTitle">
					<tr class="headerRow">
						<th><a data-toggle="sort" data-sort-type="NUMBER" title="">Retry Index</a></th>
						<th>Error Reason</th>
						<th><a data-toggle="sort" data-sort-type="STRING" title="">Reattempt Time</a></th>
					</tr>
					<%
						while (ccsTransportErrorResults != null
								&& ccsTransportErrorResults.next()) {
					%>
					<%
						if (rowNum % 2 == 0) {
					%>
					<tr class="evenRow">
						<%
							} else {
						%>
					
					<tr class="oddRow">
						<%
							}
						%><td
							data-sort-value="<%=StringUtils.prepareCDATA(ccsTransportErrorResults
						.getFormattedValue("retryIndex"))%>">
							<%=StringUtils.prepareCDATA(ccsTransportErrorResults
						.getFormattedValue("retryIndex"))%>
						</td>
						<td
							data-sort-value="<%=StringUtils.prepareCDATA(ccsTransportErrorResults
						.getFormattedValue("errorText"))%>"><%=StringUtils.prepareCDATA(ccsTransportErrorResults
						.getFormattedValue("errorText"))%></td>
						<td
							data-sort-value="<%=StringUtils.prepareCDATA(ccsTransportErrorResults
						.getFormattedValue("reattemptedTs"))%>"><%=StringUtils.prepareCDATA(ccsTransportErrorResults
						.getFormattedValue("reattemptedTs"))%></td>
					</tr>

					<%
						rowNum++;
						}
					%>
					<%
						if (rowNum == 1) {
					%>

					<tr>
						<td colspan="9">No transport error details found.</td>
					</tr>
					<%
						}
					%>
				</table> &#160;
				<div class="copyright"></div>
			</td>
		</tr>
	</table>
</div>