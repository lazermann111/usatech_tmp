<%@page import="java.util.Collections"%>
<%@page import="simple.text.MessageFormat"%>
<%@page import="simple.servlet.ResponseUtils"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="com.usatech.report.custom.GenerateUtils,simple.text.StringUtils,simple.servlet.RequestUtils"%>
<jsp:useBean id="customerResults" type="simple.results.Results" scope="request" />
<% UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request); %>
<%/* we are going to hard code this for now (BSK 09-22-04)*/
String deviceName;
Long customerId = RequestUtils.getAttribute(request, "customerId", Long.class, false);
if (customerId == null)
	customerId = 0L;
Integer dealerId = RequestUtils.getAttribute(request, "dealerId", Integer.class, false);
if(dealerId != null && dealerId == 302) { /* MEI */
	deviceName = "MEI System";
} else {
	deviceName = "Device";
}%>
<form id="bankAcctForm" name="bankAcctForm" action="create_bank_acct.i" method="post" onSubmit="return onSubmit();">
<input type="hidden" name="session-token" value="<%=RequestUtils.getSessionToken(request)%>" />
<script type="text/javascript" defer="defer">
var isBankRoutingNumValid=1;
window.addEvent("domready", function() {
	new Form.Validator.Inline.Mask(document.bankAcctForm); 
	var countryOption = $(document.bankAcctForm.bank_country).getSelected();
	if(countryOption && countryOption.length == 1 && countryOption[0])
		countryChanged(countryOption[0]);
});
function countryChanged(countryOption) {
	if(countryOption.getValue().toUpperCase()=='CA'){
		$('swiftCodeField').set('class','tableDataShade');
	}else{
		$('swiftCodeField').set('class','hide');
	}
	App.updateValidation(countryOption.getParent().form.aba, 'required validate-aba-' + countryOption.getValue().toLowerCase(), 'Fixed.Aba' + countryOption.getValue().toUpperCase());
}

function onRoutingNumChange(){
	new Request.HTML({ 
        url: "get_bank_by_routingnum.html",
        method: 'post',
        data: {'aba':$('aba').value, 'bank_country':$("bankAcctForm").bank_country.value}, 	       
        async: true,
        evalScripts: true
    }).send();
}

function onSubmit(){
	if(isBankRoutingNumValid==0){
		alert("Please correct the bank rounting number.");
		return false;
	}
	if($("useTaxIdForAll").checked){
		if(!confirm("Are you sure you want to use this tax id for all other existing banks?")){
			$("useTaxIdForAll").set("checked", "");
			return false;
		}
	}
	return true;
}
</script>
<span class="caption">New Bank Account</span>
	<p class="instructions">Please enter the information below to establish a bank account to receive Direct Credit Payments from USA Technologies:</p>
	<% if(user.isMissingBankAccount()) {%><span class="required">Warning: You must establish a bank account before any transactions occur on your <%=deviceName%>(s).</span><%}%>
	<table>
		<tr><td>
				<table class="fields">
				<% if (user.isPartner()) { %>
					<tr class="tableHeader">
						<th align="left">Customer</th>
						<td align="left"></td>
					</tr>
					<tr class="tableDataShade">
						<th>Select the customer<span class="required">*</span></th>
						<td><select name="customerId" id="customerId" style="width: 100%;">
							<option value=""><%=user.getCustomerName()%></option>
						<%
							while(customerResults!=null && customerResults.next()) { 
								long cid = customerResults.getValue("customerId", Long.class); %>
								<option value="<%=cid%>" <%=cid==customerId ? "selected=\"selected\"" : ""%>>
									<%=StringUtils.encodeForHTML(customerResults.getFormattedValue("customerName"))%>
									(<%=StringUtils.encodeForHTML(customerResults.getFormattedValue("firstName"))%> <%=StringUtils.encodeForHTML(customerResults.getFormattedValue("lastName"))%>)
								</option>
							<% } %>
						</select></td>
					</tr>
					<tr>
						<td colspan="2" class="spacer"></td>
					</tr>
					<% } %>
					<tr class="tableHeader">
						<th align="left">EFT Information</th>
						<td align="left">Obtain this information directly from your bank</td>
					</tr>
					<tr class="tableDataShade">
						<td colspan="2"><div id="bankRoutingMsg"/></td>
					</tr>
					<tr class="tableDataShade">
						<th>Bank Country</th>
						<%GenerateUtils.writeCountryTdElement(pageContext, "bank_", RequestUtils.getAttribute(request, "bank_country", String.class, false), GenerateUtils.getBankCountries(), "countryChanged"); %>
					</tr>
					<tr class="tableDataShade">     
						<th>Enter the Bank ABA Number<br/>(the Bank Routing Number)<span class="required">*</span></th>
						<td><input id="aba" onchange="onRoutingNumChange();" size="50" type="text" name="aba"  data-validators="required validate-aba-us" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "aba", String.class, false))%>"/></td>
					</tr>
					<tr class="tableDataShade">
						<th>Enter the Bank Account Number<span class="required">*</span></th>
						<td><input size="50" maxlength="20" type="text" name="account_number" data-validators="required" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "account_number", String.class, false))%>"/></td>
					</tr>
					<tr class="tableDataShade" id="swiftCodeField">
						<th>Enter the Bank SWIFT Code</th>
						<td><input size="50" maxlength="12" type="text" name="swift_code" onchange="this.value=this.value.toUpperCase();" data-validators="validate-alphanum minLength:8 maxLength:12" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "swift_code", String.class, false))%>"/></td>
					</tr>
					<tr>
						<td colspan="2" class="spacer"></td>
					</tr>
					<tr class="tableHeader">
						<th align="left">Banking Information</th>
						<td align="left">This must be a	U.S. or Canadian Domestic Bank</td>
					</tr>
					<tr class="tableDataShade">
						<th>Enter the Name of the Bank<span class="required">*</span></th>
						<td><input size="50" maxlength="50" type="text" name="bank_name" data-validators="required" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "bank_name", String.class, false))%>"/></td>
					</tr>
					<tr class="tableDataShade">
						<th>Enter the Bank's Address<span class="required">*</span></th>
						<td>
							<input size="50" maxlength="50" type="text" name="bank_address1" data-validators="required" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "bank_address1", String.class, false))%>"/>
							<%GenerateUtils.writePostalTableElement(pageContext, "bank_", RequestUtils.getAttribute(request, "bank_postal", String.class, false), RequestUtils.getAttribute(request, "bank_city", String.class, false), RequestUtils.getAttribute(request, "bank_state", String.class, false), RequestUtils.getAttribute(request, "bank_country", String.class, false), false, GenerateUtils.getBankCountries(), "countryChanged", true, false); %>        					        
						</td>
					</tr>
					<tr class="tableDataShade">
						<th>Enter the Title on your Bank Account<br/>(Must read exactly as listed on bank statement)<span class="required">*</span></th>
						<td><input size="50" maxlength="50" type="text" name="account_title" data-validators="required" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "account_title", String.class, false))%>"/></td>
					</tr>
					<tr class="tableDataShade">
						<th>Choose the Account Type<span class="required">*</span></th>
						<td><fieldset class="no-border-grouper" data-validators="validate-reqchk-bynode"><%String accountType = RequestUtils.getAttribute(request, "account_type", String.class, false); %>
		    					<input type="radio" name="account_type" value="S"<%if(accountType != null && accountType.equals("S")) { %> checked="checked"<%} %>/>Savings&#160;&#160;
								<input type="radio" name="account_type" value="C"<%if(accountType != null && accountType.equals("C")) { %> checked="checked"<%} %>/>Checking
							</fieldset>
						</td>
					</tr>
					<tr class="tableHeader">
						<th align="left">Banking Billing Information</th>
						<td align="left">This must be a	U.S. or Canadian Domestic Bank</td>
					</tr>
					<tr class="tableDataShade">
				    <td>Customer Tax ID<span class="required">*</span></td>
				    <td colspan="2"><input size="50" maxlength="9" type="text" name="taxIdNbr" edit-mask="Fixed.TaxId" data-validators="required validate-digits minLength:9" value='<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "taxIdNbr", String.class, false))%>'/> <input id="useTaxIdForAll" name="useTaxIdForAll" type="checkbox" value="Y" /> Use This Tax Id For All Other Existing Banks</td>
				  	</tr>
				  	<tr class="tableDataShade">
				    <td>Enter Bank Billing Address<span class="required">*</span></td>
				    <td colspan="2"><input size="50" maxlength="50" type="text" name="address1" data-validators="required" value='<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "address1", String.class, false))%>'/>
				    <% GenerateUtils.writePostalTableElement(pageContext, null, RequestUtils.getAttribute(request, "postal", String.class, false), RequestUtils.getAttribute(request, "city", String.class, false), RequestUtils.getAttribute(request, "state", String.class, false), RequestUtils.getAttribute(request, "country", String.class, false), false, GenerateUtils.getCustomerCountries()); %>
				    </td>
				  </tr>
					<% if(user.isPartner()) { %>
					<tr class="tableDataShade">
						<th>EFT Schedule</th>
						<td>
							<select name="pay_cycle">
								<option value="8">Daily</option>
								<option value="3">Weekly - Friday</option>
								<option value="3">Monthly - First Friday</option>
							</select>
						</td>
					</tr>
					<% } %>
					<tr>
						<td colspan="2" class="spacer"></td>
					</tr>
					<tr class="tableHeader">
						<th align="left">Your Bank Contact</th>
						<td align="left">Person at your bank who we can contact to verify Banking Information</td>
					</tr>
					<tr class="tableDataShade">
						<th>Enter the Contact's Name<span class="required">*</span></th>
						<td><input size="50" maxlength="50" type="text" name="contact_name" data-validators="required" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "contact_name", String.class, false))%>"/></td>
					</tr>
					<tr class="tableDataShade">
						<th>Enter the Contact's Title<span class="required">*</span></th>
						<td><input size="50" maxlength="50" type="text" name="contact_title" data-validators="required" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "contact_title", String.class, false))%>"/></td>
					</tr>
					<tr class="tableDataShade">
						<th>Enter the Contact's Phone<span class="required">*</span></th>
						<td><input size="50" maxlength="20" type="text" name="contact_telephone" edit-mask="Fixed.PhoneUs" data-validators="required validate-digits minLength:10" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "contact_telephone", String.class, false))%>"/></td>
					</tr>
					<tr class="tableDataShade">
						<th>Enter your Contact's Fax</th>
						<td><input size="50" maxlength="20" type="text" name="contact_fax" edit-mask="Fixed.PhoneUs" data-validators="validate-digits minLength:10" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "contact_fax", String.class, false))%>"/></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr><td><span class="required">* Required Fields</span></td></tr>
		<tr><td align="center"><input type="submit" value="Submit"/></td></tr>
	</table>
</form>