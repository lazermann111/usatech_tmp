<%@page import="java.text.SimpleDateFormat"%>
<%@page import="simple.text.StringUtils,simple.servlet.RequestUtils,simple.bean.ConvertUtils"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="simple.results.Results"%>
<%@page import="simple.db.DataLayerMgr"%>
<%
	SimpleDateFormat campaignFormat = new java.text.SimpleDateFormat("MM/dd/yyyy");
	String campaignScope = ConvertUtils.getString(RequestUtils.getAttribute(request, "campaignScope", String.class, false), "");
	String manageCampaignCardsAction=campaignScope.equals("[Global]")?"manage_campaign_cards_internal.i":"manage_campaign_cards.i";
	String manageCampaignPayrollCardsAction=campaignScope.equals("[Global]")?"manage_campaign_payroll_cards_internal.i":"manage_campaign_payroll_cards.i";
	String manageProductsAction=campaignScope.equals("[Global]")?"manage_campaign_products_internal.i":"manage_campaign_products.i";
	int selectedCampaignId = ConvertUtils.getIntSafely(RequestUtils.getAttribute(request, "campaignId", false),-1);
	
	//extract campaign type's parameters
	Map<Integer, List<Integer>> campaignSettings = new HashMap<>();
	Map<String, Integer> tranTypeIds = new HashMap<>();
	
	Map<String, Object> params = new HashMap<String, Object>();
	Results results;
	if (campaignScope.equals("[Global]")){
		results = DataLayerMgr.executeQuery("GET_CAMPAIGN_SETTINGS_INTERNAL", params);
	}else{
		results = DataLayerMgr.executeQuery("GET_CAMPAIGN_SETTINGS", params);
	}
	StringBuilder propertyString = (new StringBuilder()).append("[");
	int i=0;
	while (results.next()){
		int campaignTypeId = results.getValue("campaignTypeId", int.class);
		int acctTypeId = results.getValue("acctTypeId", int.class);
		int tranTypeId = results.getValue("tranTypeId", int.class);
		String tranTypeName = results.getValue("tranTypeName", String.class);
		String propertyDesc = results.getValue("propertyDesc", String.class);
		if (i !=0) {
			propertyString.append(",");
		}
		propertyString.append("{");
		if (campaignTypeId != 0){
			propertyString.append("campaignTypeId:").append(campaignTypeId).append(", ");
		}
		if (acctTypeId != 0){
			propertyString.append("accountTypeId:").append(acctTypeId).append(", ");
		}
		if (tranTypeId != 0){
			propertyString.append("tranTypeId:").append(tranTypeId).append(", ");
			tranTypeIds.put(tranTypeName, tranTypeId);
		}
		propertyString.append("propertyDesc:").append('"').append(propertyDesc).append('"');
		propertyString.append("}");
		i++;
	}
	propertyString.append("]");
	String settings = propertyString.toString();
%>
<jsp:useBean id="campaignTypeResults" type="simple.results.Results" scope="request" />
<jsp:useBean id="campaignResults" type="simple.results.Results" scope="request" />
<jsp:useBean id="promocodeCampaignResults" type="simple.results.Results" scope="request" />
<jsp:useBean id="transTypeResults" type="simple.results.Results" scope="request" />
<jsp:useBean id="cardBrandResults" type="simple.results.Results" scope="request"/>

<% 
	StringBuilder resultCampaignTransType = (new StringBuilder()).append("[");
	i=0;
	while (transTypeResults.next()){
		int campaignId = transTypeResults.getValue("campaignId", int.class);
		int transTypeId = transTypeResults.getValue("tranTypeId", int.class);
		if (i !=0) {
			resultCampaignTransType.append(",");
		}
		resultCampaignTransType.append("{");
		resultCampaignTransType.append("campaignId:").append(campaignId).append(", ");
		resultCampaignTransType.append("transTypeId:").append(transTypeId);
		resultCampaignTransType.append("}");
		i++;
	}
	resultCampaignTransType.append("]");
	String  campaignTransType = resultCampaignTransType.toString();
	
	StringBuilder resultCampaignCardBrand = (new StringBuilder()).append("[");
	i=0;
	while (cardBrandResults.next()){
		int campaignId = cardBrandResults.getValue("campaignId", int.class);
		String cardBrand = cardBrandResults.getValue("cardBrand", String.class);
		if (i != 0){
			resultCampaignCardBrand.append(",");
		}
		resultCampaignCardBrand.append("{");
		resultCampaignCardBrand.append("campaignId:").append(campaignId).append(", ");
		resultCampaignCardBrand.append("cardBrand:'").append(cardBrand).append("'");
		resultCampaignCardBrand.append("}");
		i++;
	}
	resultCampaignCardBrand.append("]");
	String  campaignCardBrand = resultCampaignCardBrand.toString();
%>

<div>
<div class="toggle-heading">
	<span class="caption">Campaigns</span>
</div>
<form id="promoEmailForm" name="promoEmailForm" action="manage_promo_email.html">
<input type="hidden" name="campaignId" value=""/>
</form>
<form name="campaignForm" id="campaignForm">
<input type="hidden" id="redirectHome" name="redirectHome" value="true"/>
<input type="hidden" name="campaignScope" id="campaignScope" value="<%=StringUtils.encodeForHTMLAttribute(campaignScope)%>"/>
<input type="hidden" name="session-token" value="<%=RequestUtils.getSessionToken(request)%>" />
<script type="text/javascript">
	var currentCampaignState;
	var actionType;
	var isCancel=false;
	var campaignValueChanged=false;
	var currentCampaignId;
	var currentToAllDevices;
	var currentToAllCards;
	var currentToAllPayrolCards;
	var settingsList = <%=settings%>;
	var tranTypes = new Map();
	for (var i=0; i < settingsList.length; i++){
	 if (settingsList[i].propertyDesc == "Tran id") {
	 	if (tranTypes.get(settingsList[i].tranTypeId)){
	 		(tranTypes.get(settingsList[i].tranTypeId)).campaignIds.push(settingsList[i].campaignTypeId);
	 	}else{
	 		var campaignId = settingsList[i].campaignTypeId;
	 		if (!campaignId) {
	 		 campaignId = null;
	 		}
	 		tranTypes.set(settingsList[i].tranTypeId, {acctTypeId:settingsList[i].accountTypeId, campaignIds:[campaignId]});
	 	}
	 }
	}
	var campaignTranTypes = <%=campaignTransType%>;
	var campaignCardBrands = <%=campaignCardBrand%>;
		 	
	function acctTypesAvailable(campTypeId){
		var accts = [];
		var i = 0; 
		do {
		 if (settingsList[i].campaignTypeId != campTypeId || settingsList[i].propertyDesc != "Acct type") {i++;}
		 else {
		 	if (settingsList[i].accountTypeId == null) {
		 		accts[0] = "all";
		 		i = settingsList.length;
		 	}
		 	else accts.push(settingsList[i].accountTypeId);
		 	i++;
		 } 
		}
		while (i < settingsList.length);
		return accts;
	}
	
	function acctTypesAvailableList(){
		var accts = [];
		var i = 0; 
		do {
			if (settingsList[i].propertyDesc == "Accts") {
		 		accts.push(settingsList[i].accountTypeId);
		 	}
		 	i++;
		}
		while (i < settingsList.length);
		return accts;
	}
	
	function cardBrandsAv(){
		for (var i=0; i < settingsList.length; i++){
	 		if (settingsList[i].propertyDesc == "Card Brand") {
	 			return true;
	 		}
	 	}	
	 	return false;
	}
	
	var cardBrandsAv = cardBrandsAv();
	
	function displayAcctTypes(acctTypes, mode){
		if (mode == 'show' || mode == 'checked'){
			if (acctTypes.indexOf(3) >= 0) {
				$("isForMoreCardsTd").set("style","display:inline");
				if (mode == 'show') {
					$("isForMoreCards").disable="";
					$("isForMoreCards").checked="";
				}else{
					$("isForMoreCards").disable="true";
					$("isForMoreCards").checked="true";
				}
			}else{
				$("isForMoreCardsTd").set("style","display:none");
				$("isForMoreCards").checked="";
			}
			if (acctTypes.indexOf(7) >= 0) {
				$("isForPayrollDeductCardsTd").set("style","display:inline");
				if (mode == 'show') {
					$("isForPayrollDeductCards").disable="";
					$("isForPayrollDeductCards").checked="";
				}else{
					$("isForPayrollDeductCards").disable="true";
					$("isForPayrollDeductCards").checked="true";
				}
			}else{
				$("isForPayrollDeductCardsTd").set("style","display:none");
				$("isForPayrollDeductCards").checked="";
			}
			if (acctTypes.indexOf(5) >= 0) {
				$("isForAllCardsTd").set("style","display:inline");
				if (mode == 'show') {
					$("isForAllCards").disable="";
					$("isForAllCards").checked="";
				}else{
					$("isForAllCards").disable="true";
					$("isForAllCards").checked="true";
				}
			}else{
				$("isForAllCardsTd").set("style","display:none");
				$("isForAllCards").checked="";
			}
		}else if (mode == 'hide'){
			if (acctTypes.indexOf(3) >= 0 || acctTypes[0]=='all') $("isForMoreCardsTd").set("style","display:none");
			if (acctTypes.indexOf(7) >= 0 || acctTypes[0]=='all') $("isForPayrollDeductCardsTd").set("style","display:none");
			if (acctTypes.indexOf(5) >= 0 || acctTypes[0]=='all') $("isForAllCardsTd").set("style","display:none");
		}		
	}
	
	function productCampaignAvailable (campTypeId){
		var i = 0;
		do {
			if (settingsList[i].campaignTypeId == campTypeId && settingsList[i].propertyDesc == "Product camp") {
				return 'Y';
			} 
			i++;
		}
		while (i < settingsList.length);
		return 'N';
	}
	function totalDiscountAmountAv (campTypeId){
		var i = 0;
		do {
			if (settingsList[i].campaignTypeId == campTypeId && settingsList[i].propertyDesc == "Discount total") {
				return 'Y';
			} 
			i++;
		}
		while (i < settingsList.length);
		return 'N';
	}
	
	function onManageCards(){
		var mode;
		if (currentCampaignState == "DRAFT") {
			mode = "edit";
		}else{
			mode = "view";
		}
		location.href="<%=manageCampaignCardsAction%>?campaignId="+$("campaignId").value+"&cardType=N"+"&mode="+mode;
	}
	function onManagePayrollCards(){
		var mode;
		if (currentCampaignState == "DRAFT") {
			mode = "edit";
		}else{
			mode = "view";
		}
		location.href="<%=manageCampaignPayrollCardsAction%>?campaignId="+$("campaignId").value+"&cardType=P"+"&mode="+mode;
	}
	
	function onManageProducts(){
		var mode;
		if (currentCampaignState == "DRAFT") {
			mode = "edit";
		}else{
			mode = "view";
		}
		location.href="<%=manageProductsAction%>?campaignId="+$("campaignId").value+"&mode="+mode;
	}
	
	function onManagePromoEmail(){
		$("promoEmailForm").campaignId.value=$("campaignId").value;
		$("promoEmailForm").submit();
	}
	
	function clearValidation(){
		$('campaignForm').getElements("div[class=validation-advice])").each(function(el) {
				el.dispose();
			});
        $('campaignForm').getElements("input[class=validation-failed])").each(function(el) {
			el.set("class","");
		});
        $('campaignDesc').set("class","maxLength:21");
        $('campaignDesc2').set("class","maxLength:21");
	}
	
	var isDate = function (date) {
	    return!!(function(d){return(d!=='Invalid Date'&&!isNaN(d))})(new Date(date));
	}
	
	function showCampaignActiveStatus(campaignStartDate, campaignEndDate){
		var today=new Date();
		var startDate=new Date(campaignStartDate);
		var endDate=new Date(campaignEndDate);
		var active=0;
		if(isDate(startDate)){
			if(today-startDate >0){
				if(isDate(endDate)){
					if(today-endDate < 0){
						active=1;
					}
				}else{
					active=1;
				}
			}
		}else{
			if(isDate(endDate)){
				if(today-endDate < 0){
					active=1;
				}
			}else{
				active=1;
			}
		}
		if(active==1){
			$("campaignActiveStatus").set("html",'(Active)');
			$("campaignActiveStatus").set("style", 'Color: Green');
		}else{
			$("campaignActiveStatus").set("html",'(Inactive)');
			$("campaignActiveStatus").set("style", 'Color: Red');
		}
	}
	
	//Display transaction types available depends on account type and view/edit mode.
	function displayTranTypes(acctTypeId){
		var acctTypeIds = acctTypesAvailable($("campaignTypeId").value);
		var campaignTrans =[];
		if ($("campaignId").selectedIndex != -1){
			for (var i=0; i < campaignTranTypes.length; i++){
				if (campaignTranTypes[i].campaignId == $('campaignId').value){
					campaignTrans.push(campaignTranTypes[i].transTypeId);
				} 
			}
		}
		tranTypes.forEach(function(item, key){
			if (acctTypeId == null && (acctTypeIds[0]=='all' || acctTypeIds.indexOf(item.acctTypeId) != -1)  && (item.campaignIds[0]==null || item.campaignIds[0]!=null && item.campaignIds.indexOf(parseInt($("campaignTypeId").value)) !=-1) ||  
				acctTypeId !=null && item.acctTypeId == acctTypeId && (item.campaignIds[0]==null || item.campaignIds.indexOf(parseInt($("campaignTypeId").value)) !=-1)){
				$("tranTypeIdL"+key).set("style", "display:table-row");
				$("tranTypeId"+key).disabled=false;
				if (campaignTrans.indexOf(key) != -1) {
					$("tranTypeId"+key).checked="true";
				}else{
					$("tranTypeId"+key).checked="";
				}
			}else{
				$("tranTypeIdL"+key).set("style", "display:none");
				$("tranTypeId"+key).checked="";
			}
			if (currentCampaignState == "STARTED" || currentCampaignState == "FINISHED"){
				$("tranTypeId"+key).disabled = true;
			}else{
				$("tranTypeId"+key).disabled = false;
			}
		});
	} 
	
	function displayAssignCardButtons(campaignTypeId, isForAllCards, allPrepaidAssigned, allPayrollAssigned){
		if ($("campaignId").selectedIndex != -1){
			if ($("promoCode").value =="" && (campaignTypeId==1||campaignTypeId==4||campaignTypeId==5)){
				$("manageCampaignCardsButton").set("style","display:none");
        		$("manageCampaignPayrollCardsButton").set("style","display:none");
        	}else{
        		var availableAccTypes = acctTypesAvailableList();
				if (campaignTypeId==1 && (isForAllCards==""||isForAllCards==null)||campaignTypeId==4||campaignTypeId==5){
					if (availableAccTypes.indexOf(3)!=-1 && allPrepaidAssigned=='N'){
						$("manageCampaignCardsButton").set("style","display:inline");
					}else{
						$("manageCampaignCardsButton").set("style","display:none");
					}
    				if (availableAccTypes.indexOf(7)!=-1 && allPayrollAssigned=='N'){
    					$("manageCampaignPayrollCardsButton").set("style","display:inline");
    				}else{
    					$("manageCampaignPayrollCardsButton").set("style","display:none");
    				}
				}else if(isForAllCards=='Y' ){
					$("manageCampaignCardsButton").set("style","display:none");
        			$("manageCampaignPayrollCardsButton").set("style","display:none");
				}else if(isForAllCards=='N'){
					if (availableAccTypes.indexOf(3)!=-1 && allPrepaidAssigned=='N'){
						$("manageCampaignCardsButton").set("style","display:inline");
					}else{
						$("manageCampaignCardsButton").set("style","display:none");
					}
        			$("manageCampaignPayrollCardsButton").set("style","display:none");
				}else if(isForAllCards=='P'){
					$("manageCampaignCardsButton").set("style","display:none");
        			if (availableAccTypes.indexOf(7)!=-1 && allPayrollAssigned=='N'){
    					$("manageCampaignPayrollCardsButton").set("style","display:inline");
    				}else{
    					$("manageCampaignPayrollCardsButton").set("style","display:none");
    				}
				}
			}		
		}else{
			$("manageCampaignCardsButton").set("style","display:none");
			$("manageCampaignPayrollCardsButton").set("style","display:none");
		}
	}
	
	function setCampaign(campaignName, campaignDesc, campaignTypeId, campaignDiscountPercent, campaignDonationPercent, campaignStartDate, campaignEndDate, campaignRecurSchedule, campaignThresholdAmount,campaignAssignedToAllDevices,campaignAssignedToAllCards, campaignAssignedToAllPayrollCards, promoCode,campaignNthVendFree, freeVendMaxAmount, isForAllCards, productCampaign, campaignPurchaseDiscountAmount, passAllowed,freeVendMaxCount,totalDiscountAmount,totalDiscountAmountUsed, state) {
		currentCampaignState = state;
		var confirmResult=false;
		if(campaignValueChanged){
			confirmResult=confirm("You have unsaved changes for the campaign, are you sure you want to switch to another one?")
		}else{
			confirmResult=false;
		}
		if(campaignValueChanged&&!confirmResult){
			$('campaignId').getElements("option)").each(function(el) {
				if(el.value==currentCampaignId){
					el.selected=true;
				}else{
					el.selected=false;
				}
			});
			return;
		}
		campaignValueChanged=false;
		$("waitImage").style.display='block';
		$("message").set("html","");
		$("campaignName").value=campaignName;
		
		var descValue;
		if(campaignDesc.indexOf("\u000a") !=-1){
			descValue=campaignDesc.split("\u000a");
		}else{
			descValue=campaignDesc.split("\n");
		}
		$('campaignTypeId').getElements("option").each(function(el) {
			if(el.value==campaignTypeId){
				el.selected=true;
			}
			if (campaignName == ""){
        		el.disabled=false;
        	}
        	else{
        		el.disabled=true;
        	}
		});
		if (state == 'STARTED' || state == 'FINISHED'){
			$("campaignName").disabled=true;
			$("campaignDesc").disabled=true;
			$("campaignDesc2").disabled=true;
		}else{
			$("campaignName").disabled=false;
			$("campaignDesc").disabled=false;
			$("campaignDesc2").disabled=false;
		}
		$('campaignState').value=state;
		$('campaignState').set("html",state);
		onChangeCampaignType(state);
		//Display available card types to view/choose.
        if (campaignName == ""){
        	onAllCardTypes();
        	displayTranTypes(null);
        	$("isForAll").checked="";
			$("isForAll").disabled=false;
			$('campaignState').value="";
			$('campaignState').set("html","");
			$('startButton').set("style","display: none");
			$('finishButton').set("style","display: none");
			$("campaignActiveStatus").set("html",'');
		}else{
        	$('campaignState').value=state;
        	$('campaignState').set("html",state);
        	if (state == 'DRAFT'){
        		$('startButton').set("style","display: inline");
				$('finishButton').set("style","display: none");
				$("campaignActiveStatus").set("html",'');
        	}else if (state == 'STARTED'){
        		$('startButton').set("style","display: none");
				$('finishButton').set("style","display: inline");
				showCampaignActiveStatus(campaignStartDate, campaignEndDate);
        	}else{
        		$('startButton').set("style","display: none");
				$('finishButton').set("style","display: none");
				$("campaignActiveStatus").set("html",'');
        	}
        	var acctType =[];
        	if (isForAllCards == 'Y') {
        		acctType.push(5);
        		$('titleCardBrandList').set("style","display:table-row");
        		$("cardBrandList").set("style","display:table-cell");
        	}else if (isForAllCards == 'N') {
        		acctType.push(3);
        	}else if (isForAllCards == 'P') {
        		acctType.push(7);
        	}
        	displayAcctTypes(acctType, 'checked');
        	if (acctType.length < 1){
        		displayTranTypes(null);
        		$('titleCardBrandList').set("style","display:table-row");
        		$("cardBrandList").set("style","display:table-cell");
        		cardBrandSelect(false, state);
        	}else{
        		displayTranTypes(acctType[0]);
        	} 
        	if (isForAllCards =="" || isForAllCards ==null){
				$("isForAll").checked="true";
				$("isForAll").disabled=true;
				$("isForAllAccs").set("style","display:table-row");
			}else{
				$("isForAllAccs").set("style","display:none");
			}
        }
        if(campaignName!=""){
			cardBrandSelect($("campaignId").value, state);
        	$("promoCode").value=promoCode;
			$("promoCode").disabled=true;
			$("promoCode").set("data-validators","");
			if($("advice-required-promoCode")){
				$("advice-required-promoCode").dispose();
			}
			$("promoCode").set("class","");
			$("promoCodeCol").set("style","display:table-row");
			if(passAllowed == 'Y'){
				$("passAllowed").checked="true"; 
			}else{
				$("passAllowed").checked="";
			}
			assignCheckboxes(campaignTypeId);
			if (campaignTypeId == 4){
    			if (productCampaign == 'Y'){
    				$("freeVendMaxAmount").set("data-validators","validate-regex-nospace:'<%=StringUtils.prepareScript("^([1-9][0-9]{0,1}){1}(\\\\.[0-9]{1,2})?$")%>'");
					$('nthVendMaxAmountRequired').set("style","display:none");
					if($("advice-required-freeVendMaxAmount")){
						$("advice-required-freeVendMaxAmount").dispose();
					}
				}else{
					$("freeVendMaxAmount").set("data-validators","required validate-regex-nospace:'<%=StringUtils.prepareScript("^([1-9][0-9]{0,1}){1}(\\\\.[0-9]{1,2})?$")%>'");
					$('nthVendMaxAmountRequired').set("style","display:inline");
				}
			}
		}else{
			$("promoCode").value="";
			$("promoCode").disabled=false;
			if ($("campaignTypeId").value == 1 || $("campaignTypeId").value == 5){
				$("promoCode").set("data-validators"," validate-regex-nospace:'<%=StringUtils.prepareScript("^[a-zA-Z0-9]{1,20}$")%>'");
			}else{
				$("promoCode").set("data-validators","required validate-regex-nospace:'<%=StringUtils.prepareScript("^[a-zA-Z0-9]{1,20}$")%>'");
			}
			$("manageCampaignProducts").set("style","display:none");
			$("passAllowed").checked="";
			passAllowed = 'N';
			$("isForAllCards").checked="";
			$("isForMoreCards").checked="";
			$("isForPayrollDeductCards").checked="";
			if ($('campaignTypeId').value == 4){
    			$('productCampaign').checked = "";
    			$("freeVendMaxAmount").set("data-validators","required validate-regex-nospace:'<%=StringUtils.prepareScript("^([1-9][0-9]{0,1}){1}(\\\\.[0-9]{1,2})?$")%>'");
				$('nthVendMaxAmountRequired').set("style","display:inline");
			}
		}
		if(descValue.length==1){
        	$("campaignDesc").value=campaignDesc;
        	$("campaignDesc2").value="";
		}else{
			$("campaignDesc").value=descValue[0];
			$("campaignDesc2").value=descValue[1];
		}
        clearValidation();
        if (productCampaign=='Y'){
        	$("productCampaign").checked = "true";
        	if (campaignName!=""){
				$("manageCampaignProducts").set("style","display:inline");
			}
        }else{
        	$("productCampaign").checked = "";
        	$("manageCampaignProducts").set("style","display:none");
        } 
        if ($("totalDiscountAmount")){
        	$("totalDiscountAmount").value = totalDiscountAmount;
        	$("totalDiscountAmountUsed").value = totalDiscountAmountUsed;
        }
        $('campaignThresholdAmount').value=campaignThresholdAmount;
        if(campaignRecurSchedule ==""){
        	$('campaignRecurScheduleD').set('checked',true);
        	clearDisableField($('campaignRecurMonth'));
        	clearDisableField($('campaignRecurDay'));
        	clearDisableWeek();
        	$('campaignRecurHour').value="";
        }else{
        	if(campaignRecurSchedule.indexOf("M:")==0){
        		$('campaignRecurScheduleM').set('checked',true);
        		$('campaignRecurMonth').value=campaignRecurSchedule.substring(2,campaignRecurSchedule.indexOf("|"));
        		$('campaignRecurMonth').disabled=false;
        		clearDisableField($('campaignRecurDay'));
        		clearDisableWeek();
        	}else if(campaignRecurSchedule.indexOf("W:")==0){
        		$('campaignRecurScheduleW').set('checked',true);
        		var weekstr=campaignRecurSchedule.substring(2,campaignRecurSchedule.indexOf("|"));
        		$('campaignForm').getElements("input[type=checkbox][name=campaignRecurWeek])").each(function(el) {
        			el.disabled=false;
        			if(weekstr.indexOf(el.value)>-1){
        				el.checked=true;
        			}else{
        				el.checked=false;
        			}
        		});
        		clearDisableField($('campaignRecurMonth'));
        		clearDisableField($('campaignRecurDay'));
        	} else if(campaignRecurSchedule.indexOf("S:")==0){
        		$('campaignRecurScheduleS').set('checked',true);
        		$('campaignRecurDay').value=campaignRecurSchedule.substring(2,campaignRecurSchedule.indexOf("|"));
        		$('campaignRecurDay').disabled=false;
        		clearDisableField($('campaignRecurMonth'));
        		clearDisableWeek();
        	}else{
        		$('campaignRecurScheduleD').set('checked',true);
        		clearDisableField($('campaignRecurMonth'));
        		clearDisableWeek();
        		clearDisableField($('campaignRecurDay'));
        	}
        	if(campaignRecurSchedule.indexOf("|")>0){
        		$('campaignRecurHour').value=campaignRecurSchedule.substring(campaignRecurSchedule.indexOf("|")+1);
        	}
        }
        if (state=="STARTED" || state=="FINISHED"){
			$('campaignRecurScheduleM').disabled = true;
			$('campaignRecurScheduleW').disabled = true;
			$('campaignRecurScheduleD').disabled = true;
			$('campaignRecurScheduleS').disabled = true;
			$('isForAllCards').disabled = true;
			$('isForPayrollDeductCards').disabled = true;
			$('isForMoreCards').disabled = true;
			disableWeek()
			$('campaignRecurMonth').disabled = true;
			$('campaignRecurHour').disabled = true;
			$("startDate").set("style","display:none");
			$("campaignStartDateView").set("style","display:inline");
			$("endDate").set("style","display:none");
			$("campaignEndDateView").set("style","display:inline");
			$("cardBrandAll").disabled = true;
			$("tranTypeIdAll").disabled = true;
		}else{
			$("startDate").set("style","display:table-cell");
			$("campaignStartDateView").set("style","display:none");
			$("endDate").set("style","display:table-cell");
			$("campaignEndDateView").set("style","display:none");
			$("cardBrandAll").disabled = false;
			$("tranTypeIdAll").disabled = false;
			$('isForAllCards').disabled = false;
			$('isForPayrollDeductCards').disabled = false;
			$('isForMoreCards').disabled = false;
		}
		if (state=="DRAFT"){
			$('isForAllCards').disabled = true;
			$('isForPayrollDeductCards').disabled = true;
			$('isForMoreCards').disabled = true;
		}
        $("campaignDiscountPercent").value=campaignDiscountPercent;
        $("campaignDonationPercent").value=campaignDonationPercent;
        $("campaignStartDate").value=campaignStartDate;
        $("campaignStartDateView").value=campaignStartDate;
        $("campaignEndDate").value=campaignEndDate;
        $("campaignEndDateView").value=campaignEndDate;
        $("campaignPurchaseDiscountAmount").value=campaignPurchaseDiscountAmount;
        if($("campaignId").selectedIndex >= 0){
        	$("addButton").disabled=false;
        	if (state == "DRAFT"){
        		$("saveButton").disabled = false;
        		$("managePromoEmailButton").set("style","display:none");
			}else{
        		$("saveButton").disabled = true;
        		$("managePromoEmailButton").set("style","display:inline");
			} 
        	if (state == "STARTED"){
        		$("deleteButton").disabled=true;
        	}else{
        		$("deleteButton").disabled=false;
        	}
        	$("cancelButton").disabled=false;
        	$("saveNewButton").disabled=true;
        	$("copyButton").disabled=false;
        	if(campaignTypeId==1||campaignTypeId==4||campaignTypeId==5){
        		$('manageCampaignAssignmentButton').set("style","display:inline");
        	}else{
        		$('manageCampaignAssignmentButton').set("style","display:none");;
        	}
        	$("managePromoEmailButton").disabled=false;
        	$("campaignInfoTitle").setText("Selected Campaign Information");
        }else{
        	$("addButton").disabled=true;
        	$("saveButton").disabled=true;
        	$("deleteButton").disabled=true;
        	$("cancelButton").disabled=false;
        	$("saveNewButton").disabled=false;
        	$("copyButton").disabled=true;
        	$("manageCampaignCardsButton").set("style","display:none");
        	$("manageCampaignPayrollCardsButton").set("style","display:none");
        	$('manageCampaignAssignmentButton').set("style","display:none");
        	$("managePromoEmailButton").set("style","display:none");
        	$("campaignInfoTitle").setText("New Campaign Information");
        }
        if(campaignAssignedToAllDevices!=""||campaignAssignedToAllCards!=""||campaignAssignedToAllPayrollCards!=""){   
        	$("currentCampaignIsToAll").value=true; 
        }else{
			  currentToAllDevices=false;
			  $("currentCampaignIsToAll").value=false;
			}	
        if($('campaignTypeId').value == 1 || $('campaignTypeId').value == 4 || $('campaignTypeId').value == 5){
			if(campaignAssignedToAllDevices=='Y'){
				if ($("assignToAllDevices")){
						$("assignToAllDevices").checked="checked";
						currentToAllDevices=true;
				}	
				$("manageCampaignAssignmentButton").set("style","display:none");
			}else{
				if ($("assignToAllDevices")){
					$("assignToAllDevices").checked="";
					currentToAllDevices=false;
				}
			}
			if ($("assignToAllDevices")) $("assignToAllDevices").disabled=false;
		}else{
			if ($("assignToAllDevices")){
				$("assignToAllDevices").checked="";
				$("assignToAllDevices").disabled=true;
		        currentToAllDevices=false;
			}
        	$("manageCampaignAssignmentButton").set("style","display:none");
        }
        if(campaignAssignedToAllCards=='Y'){
			if ($("assignToAllCards")) $("assignToAllCards").checked="checked";
			currentToAllCards=true;
		}else{
			if ($("assignToAllCards")) $("assignToAllCards").checked="";
			currentToAllCards=false;
			}
		if(campaignAssignedToAllPayrollCards=='Y'){
			if ($("assignToAllPayrollCards")) $("assignToAllPayrollCards").checked="checked";
			currentToAllPayrollCards=true;
		}else{
			if ($("assignToAllPayrollCards")) $("assignToAllPayrollCards").checked="";
			currentToAllPayrollCards=false;
			}
		displayAssignCardButtons(campaignTypeId,isForAllCards,campaignAssignedToAllCards,campaignAssignedToAllPayrollCards);
		
		$("campaignNthVendFree").value=campaignNthVendFree;
        $("freeVendMaxAmount").value=freeVendMaxAmount;
        $("freeVendMaxCount").value=freeVendMaxCount;
        isCancel=false;
        $("waitImage").style.display='none';
        currentCampaignId=$('campaignId').value;
        
        if (state == "STARTED" || state == "FINISHED"){
        	$("assignToAllDevices").disabled = true;
        	if ($("assignToAllCards")) $("assignToAllCards").disabled = true;
        	if ($("assignToAllPayrollCards")) $("assignToAllPayrollCards").disabled = true;
        }
    }
	
	window.addEvent('domready', function() { 
    	var select = $("campaignId");
    	if(select.selectedIndex < 0) {
    		setCampaign("","","","","","","","","","","","","","","","","","", "","","","");
		} else {
				var event; 
				if (document.createEvent) {
  					event = document.createEvent("HTMLEvents");
  					event.initEvent("onchange", true, true);
  					select.dispatchEvent(event);
				} else {
  						event = document.createEventObject();
  						event.eventType = "submit";
  						select.fireEvent("change");
				}
		}
    	Form.attachOnSelect(select);
    	new Form.Validator.Inline.Mask(document.campaignForm);
    });
    
    function onCampaignChange(){
    	campaignValueChanged=true;
    }
	
    function onAssignCardsChange(){
    	onCampaignChange();
    	if ($("campaignId").value > 0) 
    	{
    		if ($("assignToAllCards").checked){
    			$("manageCampaignCardsButton").set("style","display:none");	
    		}
    		else{
    			$("manageCampaignCardsButton").set("style","display:inline");	
    		}
    	}
    	else $("manageCampaignCardsButton").set("style","display:none");
    }
    
    function onAssignPayrollCardsChange(){
    	onCampaignChange();
    	if ($("campaignId").value > 0) 
    	{
    		if ($("assignToAllPayrollCards").checked){
    			$("manageCampaignPayrollCardsButton").set("style","display:none");	
    		}
    		else{
    			$("manageCampaignPayrollCardsButton").set("style","display:inline");	
    			}
    	}
    	else $("manageCampaignPayrollCardsButton").set("style","display:none");
    }
    
    function onAssignToAllDevices(){
    	onCampaignChange();
    	if ($("campaignId").value > 0) 
    	{
    	if ($("assignToAllDevices").checked){
    		$("manageCampaignAssignmentButton").set("style","display:none");	
    	}
    	else{
    		$("manageCampaignAssignmentButton").set("style","display:inline");	
    		}
    	}
    	else $("manageCampaignAssignmentButton").set("style","display:none");
    }
    
	function onCancel(){
		isCancel=true;
		campaignValueChanged=false;
		if($("campaignId").selectedIndex < 0){
			setCampaign("","","","","","","","","","","","","","","","","","","","","","");
		}else{
			$("campaignId").fireEvent("change");
		}
		$("campaignForm").getElements(".validation-advice)").each(function(el) {
			el.dispose();
		});
		$("campaignForm").getElements(".validation-failed)").each(function(el) {
			el.set("class", "");
		});
	}
	
	function onSaveNew(){
		if ($("campaignName").value.indexOf(',') != -1){
			alert("Campaign name shouldn't contain ',' sign.");
			return;
		}
		if($("campaignForm").validate()&&validateDate()){
			if(!validateRecurSchedule()||!validateCashback()||!validateAcctType()){
				return;
			}
			$("waitImage").style.display='block';
			new Request.HTML({ 
		    	url: "add_campaign.i",
		    	data: $("campaignForm"), 
		    	method: 'post',
				evalScripts: true
			}).send();
		}
	}
	
	function onAdd(){
		$("campaignId").selectedIndex=-1;
		setCampaign("","","","","","","","","","","","","","","","","","","","","","");
	}
	
	function onCopy(){
		var el = $('campaignId').getSelected();
        eval((el[0].getAttribute("onSelect")).replace("STARTED", "DRAFT").replace("FINISHED", "DRAFT")); 
        currentCampaignState = "DRAFT";
        $("campaignId").selectedIndex=-1;
		$("campaignName").value = $("campaignName").value + " - Copy";
		$("addButton").disabled=true;
    	$("saveButton").disabled=true;
    	$("deleteButton").disabled=true;
    	$("cancelButton").disabled=false;
    	$("saveNewButton").disabled=false;
    	$("copyButton").disabled=true;
    	$("startButton").set("style","display:none");
    	$("finishButton").set("style","display:none");
    	$("manageCampaignCardsButton").set("style","display:none");
    	$("manageCampaignPayrollCardsButton").set("style","display:none");
    	$('manageCampaignAssignmentButton').set("style","display:none");
    	$("managePromoEmailButton").set("style","display:none");
    	$("campaignInfoTitle").setText("New Campaign Information");
    	$("campaignActiveStatus").set("html",'');
    	if ($('totalDiscountAmountUsed')) { $('totalDiscountAmountUsed').value=0;}
    	$("campaignState").set("html", '');
		if ($("campaignTypeId").value == 1 || $("campaignTypeId").value == 5 || $("campaignTypeId").value == 4){
			$("promoCodeSelCol").set("style","display:table-row");
			$('promoCodeSelect').selectedIndex = 0;
			$("promoCode").disabled=false;
			if ($("campaignTypeId").value == 1 || $("campaignTypeId").value == 5){
				$("promoCode").set("data-validators"," validate-regex-nospace:'<%=StringUtils.prepareScript("^[a-zA-Z0-9]{1,20}$")%>'");
			}else{
				$("promoCode").set("data-validators","required validate-regex-nospace:'<%=StringUtils.prepareScript("^[a-zA-Z0-9]{1,20}$")%>'");
			}
		}
		$('isForMoreCards').disabled="";
		$('isForPayrollDeductCards').disabled="";
		$('isForAllCards').disabled="";
		if (($('campaignTypeId').value == 1) && !($('isForMoreCards').checked || $('isForPayrollDeductCards').checked || $('isForAllCards').checked)) {
			$('isForAll').checked = true;
			$('isForAllAccs').set("style","display:inline");
		}
    	campaignValueChanged=true;
	}
	
	function updateCardBrands(campaignId){
		var i = 0; 
			while (i < campaignCardBrands.length) {
				if (campaignCardBrands[i].campaignId == campaignId){
					campaignCardBrands.splice(i,1);
				}else{
					i++;
				}
			}
		if ($("American Express").checked){
			campaignCardBrands.push({"campaignId":campaignId, "cardBrand":"American Express"});
		}
		if ($("Discover Card").checked){
			campaignCardBrands.push({"campaignId":campaignId, "cardBrand":"Discover Card"});
		}
		if ($("MasterCard").checked){
			campaignCardBrands.push({"campaignId":campaignId, "cardBrand":"MasterCard"});
		}
		if ($("Visa").checked){
			campaignCardBrands.push({"campaignId":campaignId, "cardBrand":"Visa"});
		}
	} 
	
	function addCampaignResult(campaignId, name, desc, campaignRecurSchedule, errorMsg, campaignAssignedToAllDevices, campaignAssignedToAllCards, campaignAssignedToAllPayrollCards, promoCode){
		$("waitImage").style.display='none';
		if(campaignId>0){
			var i = 0; 
			while (i < campaignTranTypes.length) {
				if (campaignTranTypes[i].campaignId == campaignId){
					campaignTranTypes.splice(i,1);
				}else{
					i++;
				}
			}
			tranTypes.forEach(function(item, key){
				if ($("tranTypeId"+key).checked){
					campaignTranTypes.push({"campaignId":campaignId, "transTypeId":key});
				}
			});
			updateCardBrands(campaignId);
			campaignValueChanged=false;
			var optionText;
			var isForAllCards
			if($("isForAllCards").checked){
				isForAllCards='Y';
			}else if($("isForMoreCards").checked){
				isForAllCards='N';
			}else if ($("isForPayrollDeductCards").checked){
				isForAllCards='P';
			}else{
				isForAllCards=null;
			}
			
			if($('campaignTypeId').value==4){
					optionText=$("campaignName").value+" (Promotion)";
			} else if($('campaignTypeId').value==5){
				optionText=$("campaignName").value+" (Purchase Discount)";
			} else {
        		if(isForAllCards=='Y'){
        			optionText=$("campaignName").value+" (All other cards)";
        		} else if(isForAllCards=='N'){
        			optionText=$("campaignName").value+" (MORE cards)";
        		} else if(isForAllCards=='P'){
        			optionText=$("campaignName").value+" (Payroll Deduct cards)";
        		} else if($('campaignTypeId').value==1){
        			optionText=$("campaignName").value+" (Loyalty Discount)";
        		}
        	}
			
			var campaignTypeId = $("campaignTypeId").value;
			var campaignDiscountPercent =  $("campaignDiscountPercent").value;
			var campaignDonationPercent =  $("campaignDonationPercent").value;
			var campaignStartDate = $("campaignStartDate").value;
			var campaignEndDate = $("campaignEndDate").value;
			var campaignThresholdAmount = $("campaignThresholdAmount").value;
			var campaignNthVendFree = $("campaignNthVendFree").value;
			var freeVendMaxAmount = $("freeVendMaxAmount").value;
			var freeVendMaxCount = $("freeVendMaxCount").value;
			var campaignPurchaseDiscountAmount = $("campaignPurchaseDiscountAmount").value;
			var productCampaign;
			var passAllowed;
			if($("productCampaign").checked){
				productCampaign='Y';
			}else productCampaign='N';
			if($("passAllowed").checked){
				passAllowed='Y';
			}else passAllowed='N';
			var totalDiscountAmount="";
			if ($("totalDiscountAmount")) {  
				totalDiscountAmount = $("totalDiscountAmount").value;
			}
			
			var setCampFunc = "setCampaign('" + name + "', '" + desc + "', '" + campaignTypeId + "', '" + campaignDiscountPercent + "', '" + campaignDonationPercent + "','" + campaignStartDate + "', '" + campaignEndDate + "', '" + campaignRecurSchedule + "', '" + campaignThresholdAmount + "','" + campaignAssignedToAllDevices + "', '" + campaignAssignedToAllCards + "', '" + campaignAssignedToAllPayrollCards + "','" + promoCode + "','" + campaignNthVendFree + "','" + freeVendMaxAmount + "','" + isForAllCards + "', '" + productCampaign + "','" + campaignPurchaseDiscountAmount + "', '" + passAllowed + "','" + freeVendMaxCount + "','" + totalDiscountAmount + "', '0' , 'DRAFT');";
			$('campaignId').adopt(new Element("option",{
				selected: 'selected',
				value:campaignId,
				text:optionText,
				onselect: setCampFunc
			}));
			var el = $('campaignId').getSelected();
			el.removeEvents("select");
            el.addEvent("select", function() {
            	setCampaign(name, desc, campaignTypeId, campaignDiscountPercent, campaignDonationPercent,campaignStartDate, campaignEndDate, campaignRecurSchedule, campaignThresholdAmount,campaignAssignedToAllDevices, campaignAssignedToAllCards, campaignAssignedToAllPayrollCards, promoCode,campaignNthVendFree,freeVendMaxAmount,isForAllCards, productCampaign, campaignPurchaseDiscountAmount, passAllowed,freeVendMaxCount,totalDiscountAmount,0, "DRAFT");
			});
			
			$("addButton").disabled=false;
        	$("saveButton").disabled=false;
        	$("deleteButton").disabled=false;
        	$("cancelButton").disabled=false;
        	$("saveNewButton").disabled=true;
        	
        	$("managePromoEmailButton").disabled=false;
        	$("campaignInfoTitle").setText("Selected Campaign Information");
        	Form.attachOnSelect($("campaignId"));
        	showCampaignActiveStatus(campaignStartDate,campaignEndDate);
        	if(promoCode!=""){
        		$('promoCodeSelect').adopt(new Element("option",{
    				value:promoCode,
    				text:$("campaignName").value+' ('+promoCode+')'
    			}));
        	}
        	setCampaign(name, desc, campaignTypeId, campaignDiscountPercent, campaignDonationPercent,campaignStartDate, campaignEndDate, campaignRecurSchedule, campaignThresholdAmount,campaignAssignedToAllDevices, campaignAssignedToAllCards, campaignAssignedToAllPayrollCards, promoCode,campaignNthVendFree,freeVendMaxAmount,isForAllCards, productCampaign, campaignPurchaseDiscountAmount, passAllowed,freeVendMaxCount,totalDiscountAmount,0, "DRAFT");
        	if($("campaignTypeId").value==1||$("campaignTypeId").value==4){
        		$("message").set("html","<p class='status-info-success'>Successfully created the campaign. Now promotion code "+promoCode+" can be used for user to register their cards at MORE web site.</p>");
        	}else{
        		$("message").set("html","<p class='status-info-success'>Successfully created the campaign.</p>");
        	}
        }else{
			$("message").set("html","<p class='status-info-failure'>Failed to create the campaign. "+errorMsg+" Please try again.</p>");
		}
	}
	
	function onEditSave(){
		if ($("campaignName").value.indexOf(',') != -1){
			alert("Campaign name shouldn't contain ',' sign.");
			return;
		}
		if($("campaignForm").validate()&&validateDate()){
			var confirmMsg="";
			if(currentToAllDevices && !$("assignToAllDevices").checked){
				confirmMsg=confirmMsg+"Unchecking Assign to all devices will unassign all devices from the campaign.\n";
			}
			if(currentToAllCards && !$("assignToAllCards").checked){
				confirmMsg=confirmMsg+"Unchecking Assign to all cards will unassign all cards from the campaign.\n";
			}
			if(currentToAllPayrollCards && !$("assignToAllPayrollCards").checked){
				confirmMsg=confirmMsg+"Unchecking Assign to all payroll deduct cards will unassign all cards from the campaign.\n";
			}
			var confirmResult=true;
			if(confirmMsg!=""){
				confirmResult=confirm(confirmMsg+"Are you sure you want to continue?");
			}
			if(!confirmResult){
					return;
			}
			if(!validateRecurSchedule()||!validateCashback()){
				return;
			}
			$("message").set("html","");
			$("waitImage").style.display='block';
			new Request.HTML({ 
		    	url: "edit_campaign.i",
		    	data: $("campaignForm"), 
		    	method: 'post',
				evalScripts: true
			}).send();
		}
	}
	
	function campaignValidation(startDateVal, endDateVal, devicesVal, productsVal, cardBrandVal, promoPassVal, payTypeVal){
		var validationResult = '';
		if (startDateVal != '') validationResult = validationResult + '\n'+ '-   ' + startDateVal;
		if (endDateVal != '') validationResult = validationResult + '\n'+ '-   ' + endDateVal;
		if (devicesVal != '') validationResult = validationResult + '\n'+ '-   ' + devicesVal;
		if (productsVal != '') validationResult = validationResult + '\n'+ '-   ' + productsVal;
		if (cardBrandVal != '') validationResult = validationResult + '\n'+ '-   ' + cardBrandVal;
		if (promoPassVal != '') validationResult = validationResult + '\n'+ '-   ' + promoPassVal;
		if (payTypeVal != '') validationResult = validationResult + '\n'+ '-   ' + payTypeVal;
		 if (validationResult != '') {
		 	alert(validationResult);
		 	return;
		 }
		$("message").set("html","");
			$("waitImage").style.display='block';
			new Request.HTML({ 
		    	url: "edit_campaign.i",
		    	data: $("campaignForm"), 
		    	method: 'post',
				evalScripts: true
			}).send();
	}
	
	function validateBeforeStart(){
		
		new Request.HTML({ 
		    	url: "validate_campaign.i",
		    	data: $("campaignForm"), 
		    	method: 'post',
				evalScripts: true
			}).send();
	
	}
	
	function onStateChange(newState){
		if (newState == 'start'){
			if (campaignValueChanged == true){
				alert('Campaign has unsaved changes. Save changes and try again.');
				return;
			}
			if ($('editAction').value == 'start'){
				validateBeforeStart();
			}
		}else{
			$("message").set("html","");
			$("waitImage").style.display='block';
			new Request.HTML({ 
		    	url: "edit_campaign.i",
		    	data: $("campaignForm"), 
		    	method: 'post',
				evalScripts: true
			}).send();
		}
	}
		
	function editCampaignResult(status, name, desc, campaignRecurSchedule, errorMsg,campaignAssignedToAllDevices, campaignAssignedToAllCards, campaignAssignedToAllPayrollCards, productCampaign, state){
		$("waitImage").style.display='none';
		if(status == 1 || status == 3){
			var campaignId = $("campaignId").value;
			var i = 0; 
			while (i < campaignTranTypes.length) {
				if (campaignTranTypes[i].campaignId == campaignId){
					campaignTranTypes.splice(i,1);
				}else{
					i++;
				}
			}
			tranTypes.forEach(function(item, key){
				if ($("tranTypeId"+key).checked){
					campaignTranTypes.push({"campaignId":campaignId, "transTypeId":key});
				}
			});
			updateCardBrands(campaignId);
			campaignValueChanged=false;
			var compaignTypeId = $("campaignTypeId").value;
			$('campaignState').value=state;
			$('campaignState').set("html",state);
			var isForAllCards;
			if($("isForAllCards").checked){
				isForAllCards='Y';
			}else if($("isForMoreCards").checked){
				isForAllCards='N';
			}else if($("isForPayrollDeductCards").checked){
				isForAllCards='P';
			}else isForAllCards=null; 
			var productCampaign;
			if($("productCampaign").checked){
				productCampaign='Y';
			}else productCampaign='N';
			if($("passAllowed").checked){
				passAllowed='Y';
			}else passAllowed='N';
            var campaignDiscountPercent =  $("campaignDiscountPercent").value;
            var campaignDonationPercent =  $("campaignDonationPercent").value;
            var campaignStartDate = $("campaignStartDate").value;
            var campaignEndDate = $("campaignEndDate").value;
            var campaignThresholdAmount = $("campaignThresholdAmount").value;
            var campaignNthVendFree = $("campaignNthVendFree").value;
            var freeVendMaxAmount=$("freeVendMaxAmount").value;
            var freeVendMaxCount=$("freeVendMaxCount").value;
            var campaignPurchaseDiscountAmount = $("campaignPurchaseDiscountAmount").value;
            var promoCode = $("promoCode").value;
            var el = $('campaignId').getSelected();
            var totalDiscountAmount="";
            var totalDiscountAmountUsed=0;
            if ($("totalDiscountAmount")){
            	var totalDiscountAmount = $("totalDiscountAmount").value;
            	var totalDiscountAmountUsed = $("totalDiscountAmountUsed").value;
            }
            el.removeEvents("select");
            el.addEvent("select", function() {
                setCampaign(name, desc, compaignTypeId, campaignDiscountPercent, campaignDonationPercent, campaignStartDate, campaignEndDate, campaignRecurSchedule, campaignThresholdAmount,campaignAssignedToAllDevices, campaignAssignedToAllCards, campaignAssignedToAllPayrollCards, promoCode,campaignNthVendFree,freeVendMaxAmount,isForAllCards, productCampaign, campaignPurchaseDiscountAmount,passAllowed,freeVendMaxCount,totalDiscountAmount,totalDiscountAmountUsed, state);
            });           
           	if(compaignTypeId==4){
           		$('campaignId').getSelected().set("text", name+" (Promotion)");
           	}else if(compaignTypeId==5){
           		$('campaignId').getSelected().set("text", name+" (Purchase Discount)");
           	}else{
           		if(isForAllCards=='Y'){
       				$('campaignId').getSelected().set("text", name+" (All other cards)");
           		}else if(isForAllCards=='N'){
           			$('campaignId').getSelected().set("text", name+" (MORE cards)");
           		}else if(isForAllCards=='P'){
           			$('campaignId').getSelected().set("text", name+" (Payroll deduct cards)");
           		}else{
           			$('campaignId').getSelected().set("text", name+" (Loyalty Discount)");	
           		} 
           	}
			if($("campaignTypeId").value==1||$("campaignTypeId").value==4||$("campaignTypeId").value==5){
				if ($("assignToAllDevices")){
					if($("assignToAllDevices").checked){
						$('manageCampaignAssignmentButton').set("style","display:none");
					}else{
        				$('manageCampaignAssignmentButton').set("style","display:inline");
        			}
				}
        	}else{
        		$('manageCampaignAssignmentButton').set("style","display:none");
        	}
        	if (state == "STARTED") {el[0].outerHTML = el[0].outerHTML.replace("DRAFT", "STARTED");}
        	currentCampaignId = campaignId;
        	$('campaignId').getElements("option").each(function(el) {
				if(el.value==currentCampaignId){
					el.selected=true;
				}else{
					el.selected=false;
				}
			});
        	setCampaign(name, desc, compaignTypeId, campaignDiscountPercent, campaignDonationPercent, campaignStartDate, campaignEndDate, campaignRecurSchedule, campaignThresholdAmount,campaignAssignedToAllDevices, campaignAssignedToAllCards, campaignAssignedToAllPayrollCards, promoCode,campaignNthVendFree,freeVendMaxAmount,isForAllCards, productCampaign, campaignPurchaseDiscountAmount,passAllowed,freeVendMaxCount,totalDiscountAmount,totalDiscountAmountUsed, state);
			$("message").set("html","<p class='status-info-success'>Successfully edited the campaign.</p>");
		}else if (status==2) {
			var campaignId = $("campaignId").value;
			campaignValueChanged=false;
			var el = $('campaignId').getSelected();
			var setCampFunc = (el[0].getAttribute("onSelect")).replace("STARTED", "FINISHED");
            el.removeEvents("select");
            el.addEvent("select", function() {
            	eval(setCampFunc); 
            }); 
            el.outerHTML = el[0].outerHTML.replace("STARTED", "FINISHED");
            currentCampaignId = campaignId;
            eval(setCampFunc);
            $("message").set("html","<p class='status-info-success'>Successfully changed the status of campaign.</p>");
		}else if(status==0){
			campaignValueChanged=false;
			var currentCampaignId=$("campaignId").value;
			var promoCodes = document.getElementById("promoCodeSelect");
			for(i = 0; i < promoCodes.options.length; i++) {
				if (promoCodes[i].text == $('campaignName').value + ' (' + $('promoCode').value + ')') {
					promoCodes.remove(i);
				};
			}
			$("campaignId").getSelected().dispose();
			setCampaign("","","","","","","","","","","","","","","","","","","","","","","");
			$("message").set("html","<p class='status-info-success'>Successfully deleted the campaign.</p>");
		}else{
			$("message").set("html","<p class='status-info-failure'>Failed to edit the campaign. "+errorMsg+" Please try again.</p>");
		}
	}
	
	function clearDisableField(el){
		el.value="";
		el.disabled=true;
	}
	
	function clearDisableWeek(){
		$('campaignForm').getElements("input[type=checkbox][name=campaignRecurWeek])").each(function(el) {
			el.checked=false;
			el.disabled=true;
		});
	}
	
	function disableWeek(){
		$('campaignForm').getElements("input[type=checkbox][name=campaignRecurWeek])").each(function(el) {
			el.disabled = true;
		});
	}
	
	function enableWeek(){
		$('campaignForm').getElements("input[type=checkbox][name=campaignRecurWeek])").each(function(el) {
			el.disabled=false;
		});
	}
	
	function validateRecurSchedule(){
		if($('campaignRecurScheduleW').checked){
			var weekElements=$('campaignForm').getElements("input[type=checkbox][name=campaignRecurWeek])");
			for (var i=0;i<weekElements.length;i++){
				if(weekElements[i].checked){
					return true;
				}
			 }
			alert("Please select at least one value for Day of Week.");
			return false;
		}else if($('campaignRecurScheduleM').checked){
			if($('campaignRecurMonth').value.trim().length==0){
				alert("Please input expression for Day of Month. You can use single day or duration of days separated by comma.Valid days are 1-31. eg. 1,2,20-31");
				return false;
			}else{
				return true;
			}
		}else{
			return true;
		}
	}
	
	function assignCheckboxes(campaignTypeId){
		if ($("campaignId").selectedIndex != -1 && (campaignTypeId==1 || campaignTypeId==4 || campaignTypeId==5) && $("promoCode").value=="" ){
			if($("assignToAllCards")){
				$("assignToAllCards").checked="";
				$("assignToAllCards").disabled=true;
			}
			if($("assignToAllPayrollCards")){
				$("assignToAllPayrollCards").checked="";
				$("assignToAllPayrollCards").disabled=true;
			}
		}else{
			var accTypes = acctTypesAvailable (campaignTypeId);
			if (accTypes[0] == 'all'){
				var allAccTypes = acctTypesAvailableList();
				if (allAccTypes.indexOf(3)!=-1){
		 			if($("assignToAllCards")){
						$("assignToAllCards").checked="";
						$("assignToAllCards").disabled=false;
					}
		 		}else{
		 			if($("assignToAllCards")){
						$("assignToAllCards").checked="";
						$("assignToAllCards").disabled=true;
					}
		 		}
		 		if (allAccTypes.indexOf(7)!=-1){
		 			if($("assignToAllPayrollCards")){
						$("assignToAllPayrollCards").checked="";
						$("assignToAllPayrollCards").disabled=false;
					}
		 		}else{
		 			if($("assignToAllPayrollCards")){
						$("assignToAllPayrollCards").checked="";
						$("assignToAllPayrollCards").disabled=true;
					}
		 		} 
			}else{
		 		if (accTypes.indexOf(3)!=-1 && ($("isForMoreCards").checked || $("isForAll").checked)){
		 			if($("assignToAllCards")){
						$("assignToAllCards").checked="";
						$("assignToAllCards").disabled=false;
					}
		 		}else{
		 			if($("assignToAllCards")){
						$("assignToAllCards").checked="";
						$("assignToAllCards").disabled=true;
					}
		 		}
		 		if (accTypes.indexOf(7)!=-1 && ($("isForPayrollDeductCards").checked || $("isForAll").checked)){
		 			if($("assignToAllPayrollCards")){
						$("assignToAllPayrollCards").checked="";
						$("assignToAllPayrollCards").disabled=false;
					}
		 		}else{
		 			if($("assignToAllPayrollCards")){
						$("assignToAllPayrollCards").checked="";
						$("assignToAllPayrollCards").disabled=true;
					}
		 		} 
			}
		}
	}
	
	function onChangeCampaignType(state){
		//Disable/enable card types depends on campaign type id.
		if ($("campaignId").selectedIndex == -1){
			displayAcctTypes(acctTypesAvailable($('campaignTypeId').value), 'show');
			onAllCardTypes();
		}
		$("isForAll").checked="";
		if ($('campaignTypeId').value == 1){
			$("isForAllAccs").set("style", "display:table-row");
		}else{
			$("isForAllAccs").set("style", "display:none");
		}
		//Trans types.
		$("tranTypeIdAll").checked = "";
		if ($('campaignTypeId').value == 2 || $('campaignTypeId').value == 6){
			$('transTypesList').set("style","display:none");
			$('titleTransTypesList').set("style","display:none");
		}else{
			$('transTypesList').set("style","display:block"); 
			$('titleTransTypesList').set("style","display:table-row");
		}
		//Card brand
		$("cardBrandAll").checked = "";
		cardBrandSelect(false, state);
		if ($("campaignTypeId").value == 2 || $('campaignTypeId').value == 6){
			if (cardBrandsAv){
				$('titleCardBrandList').set("style","display:table-row");
				$("cardBrandList").set("style","display:table-cell");
			}else{
				$('titleCardBrandList').set("style","display:none");
				$("cardBrandList").set("style","display:none");
			}
		}else{
			if ($("campaignId").selectedIndex == -1 && (acctTypesAvailable($("campaignTypeId").value)[0] == 'all' || acctTypesAvailable($("campaignTypeId").value).indexOf(5) > 0)){
				$('titleCardBrandList').set("style","display:table-row");
				$("cardBrandList").set("style","display:table-cell");
			}else{
				$('titleCardBrandList').set("style","display:none");
				$("cardBrandList").set("style","display:none");
			}
		}
		//Schedule
		if($('campaignTypeId').value==1||$('campaignTypeId').value==5){
			$('recurringSchedule0').set("style", "display:table-row");
			$('recurringSchedule1').set("style", "display:table-row");
			$('recurringSchedule2').set("style", "display:table-row");
			$('recurringSchedule3').set("style", "display:table-row");
			$('recurringSchedule4').set("style", "display:table-row");
			$('recurringSchedule5').set("style", "display:table-row");
			$('campaignStartDate').disabled=false;
			$('campaignEndDate').disabled=false;
			$('campaignRecurScheduleM').disabled =false;
			$('campaignRecurScheduleW').disabled =false;
			$('campaignRecurScheduleD').disabled =false;
			$('campaignRecurScheduleD').checked=true;
			$('campaignRecurScheduleS').disabled =false;
			enableWeek();
			$('campaignRecurHour').disabled=false;
			
			if($("campaignId").selectedIndex==-1){
				$("promoCodeSelCol").set("style","display:table-row");
			}
		}else{
			$('campaignRecurScheduleM').checked =false;
			$('campaignRecurScheduleW').checked =false;
			$('campaignRecurScheduleD').checked =false;
			$('campaignRecurMonth').value="";
			$('campaignRecurHour').value="";
			$('campaignRecurDay').value="";
			$('recurringSchedule0').set("style", "display:none");
			$('recurringSchedule1').set("style", "display:none");
			$('recurringSchedule2').set("style", "display:none");
			$('recurringSchedule3').set("style", "display:none");
			$('recurringSchedule4').set("style", "display:none");
			$('recurringSchedule5').set("style", "display:none");
		}
		if ($('campaignTypeId').value == 1 || $('campaignTypeId').value == 5) {
			$("promoCode").set("data-validators","validate-regex-nospace:'<%=StringUtils.prepareScript("^[a-zA-Z0-9]{1,20}$")%>'");
			if($("advice-required-promoCode")){
				$("advice-required-promoCode").dispose();
			}
		}else{
			$("promoCode").set("data-validators","required validate-regex-nospace:'<%=StringUtils.prepareScript("^[a-zA-Z0-9]{1,20}$")%>'");
		}	
		//Donation percent
		if($('campaignTypeId').value==2||$('campaignTypeId').value==6){
			$("donationPercentRow").set("style","display:table-row");
		}else{
			$("campaignDonationPercent").value ="";
			$("donationPercentRow").set("style","display:none");
		}
		//Nth free params
		if($('campaignTypeId').value==4){
			$("nthParams").set("style","display:table-row");
			$("campaignNthVendFree").set("data-validators","required validate-regex-nospace:'<%=StringUtils.prepareScript("^([1-9]|[1-9][0-9])$")%>'");
			if ($('productCampaign').checked){
				$("freeVendMaxAmount").set("data-validators","validate-regex-nospace:'<%=StringUtils.prepareScript("^([1-9][0-9]{0,1}){1}(\\\\.[0-9]{1,2})?$")%>'");
				$('nthVendMaxAmountRequired').set("style","display:none");
				if($("advice-required-freeVendMaxAmount")){
						$("advice-required-freeVendMaxAmount").dispose();
					}
			}else{
				$("freeVendMaxAmount").set("data-validators","required validate-regex-nospace:'<%=StringUtils.prepareScript("^([1-9][0-9]{0,1}){1}(\\\\.[0-9]{1,2})?$")%>'");
				$('nthVendMaxAmountRequired').set("style","display:inline");
			}
			$("freeVendMaxCount").set("data-validators","validate-regex-nospace:'<%=StringUtils.prepareScript("^(|[1-9]|[1-9][0-9])$")%>'");
			$("nthVendFreeRequired").set("style","display:inline");
		}else{
			$("nthParams").set("style","display:none");
			$("campaignNthVendFree").disabled =true;
			$("freeVendMaxAmount").disabled=true;
			$("freeVendMaxCount").disabled=true;
			$("campaignNthVendFree").value ="";
			$("freeVendMaxAmount").value="";
			$("freeVendMaxCount").value="";
			$("nthVendFreeRequired").set("style","display:none");
			$("nthVendMaxAmountRequired").set("style","display:none");
		}
		//Total discount amount
		if ($("totalDiscountAmount")){
			if (totalDiscountAmountAv($('campaignTypeId').value) == 'Y'){
				$("totalDiscountAmountRow").set("style","display:table-row");
			}else{
				$("totalDiscountAmount").value="";
				$("totalDiscountAmountRow").set("style","display:none");
			}
		}
		//ThresholdAmount
		if($('campaignTypeId').value==1){
			$("thresholdAmountRow").set("style","display:table-row");
			$('campaignThresholdAmount').value="";
			$('campaignThresholdAmount').set("data-validators","validate-regex-nospace:'<%=StringUtils.prepareScript("^\\\\d{1,8}(\\\\.\\\\d{1,2}){0,1}$")%>'");
			$('thresholdAmountRequired').set("style", "display:none");
		}else if($('campaignTypeId').value==4 || $('campaignTypeId').value==5){
				$("thresholdAmountRow").set("style","display:none");
		}else{
			$("thresholdAmountRow").set("style","display:table-row");
			$('campaignThresholdAmount').set("data-validators","required validate-regex-nospace:'<%=StringUtils.prepareScript("^\\\\d{1,8}(\\\\.\\\\d{1,2}){0,1}$")%>'");
			$('thresholdAmountRequired').set("style", "display:inline");
		}	
		if($("advice-required-campaignThresholdAmount")){
			$("advice-required-campaignThresholdAmount").dispose();
		}
		//Assign to all devices
		if($('campaignTypeId').value==1 || $('campaignTypeId').value==4 || $('campaignTypeId').value==5){
			if($("assignToAllDevices")){
				$("assignToAllDevices").disabled=false;
			}
		}else{
			if($("assignToAllDevices")){
				$("assignToAllDevices").disabled=true;
			}
		}
		//Discount amount
		if($('campaignTypeId').value==5||$('campaignTypeId').value==6){
			$('purchaseDiscountAmountRow').set("style","display:table-row");
			$('campaignPurchaseDiscountAmount').set("data-validators","required validate-regex-nospace:'<%=StringUtils.prepareScript("^\\\\d{1,8}(\\\\.\\\\d{1,2}){0,1}$")%>'");
		}else{
			if($("advice-required-campaignPurchaseDiscountAmount")){
				$("advice-required-campaignPurchaseDiscountAmount").dispose();
			}
			$('campaignPurchaseDiscountAmount').value="";
			$('purchaseDiscountAmountRow').set("style","display:none");
		}
		//Discount percent
		if($('campaignTypeId').value==2||$('campaignTypeId').value==3||$('campaignTypeId').value==1){
			$('discountPercentRow').set("style","display:table-row");
			$('campaignDiscountPercent').set("data-validators","required validate-regex-nospace:'<%=StringUtils.prepareScript("^\\\\d{1,2}(\\\\.\\\\d{1,2}){0,1}$")%>'");
		}else{
			if($("advice-required-campaignDiscountPercent")){
				$("advice-required-campaignDiscountPercent").dispose();
			}
			$('campaignDiscountPercent').value="";
			$('discountPercentRow').set("style","display:none");
		}
		
		if($('campaignTypeId').value==1||$('campaignTypeId').value==4||$('campaignTypeId').value==5){
			$("promoCode").set("style","display:inline");
			if($("campaignId").selectedIndex==-1){
				$("promoCodeSelCol").set("style","display:table-row");
				$('promoCodeSelect').selectedIndex = 0;
				$("promoCode").disabled=false;
			}else{
				$("promoCodeSelCol").set("style","display:none");
				$("promoCode").disabled = true;
			}
		}else{
			if ($('advice-required-promoCode')){
				$('advice-required-promoCode').dispose();
			}
			$("promoCodeSelect").selectedIndex=0;
			$("promoCodeSelCol").set("style","display:none");
			$("promoCode").set("style","display:none");
			$("promoCode").disabled = true;
    		$("promoCode").set("data-validators","");
		}
		$("assignToExistingCardsTr").set("style","display:none");
		if($("campaignId").selectedIndex==-1){		
			assignCheckboxes($('campaignTypeId').value);
		}
		//Assign products availability for different campaign types.
		if (productCampaignAvailable($('campaignTypeId').value) == 'Y'){
			$("productCampaignAv").set("style","display:inline");
		}else{
			$("productCampaign").checked=false;
			$("productCampaignAv").set("style","display:none");
		}
		//Pass availability by campaign types.
		if ($('campaignTypeId').value==1||$('campaignTypeId').value==4||$('campaignTypeId').value==5){
        	$("passAllowedLabel").set("style","display:inline");
        }else{
        	$("passAllowedLabel").set("style","display:none");
        }
        
        if (state=="STARTED" || state=="FINISHED"){
				$("campaignNthVendFree").disabled = true;
				$("freeVendMaxAmount").disabled = true;
				$("freeVendMaxCount").disabled = true;
				$("campaignDonationPercent").disabled = true;
				$('campaignThresholdAmount').disabled = true;
				$('campaignPurchaseDiscountAmount').disabled = true;
				$('campaignDiscountPercent').disabled = true;
				$('passAllowed').disabled = true;
				$("productCampaign").disabled = true;
				$('campaignRecurDay').disabled=true;
				if ($("totalDiscountAmount")){
					$("totalDiscountAmount").disabled = true;
				}
			}else{
				$("campaignNthVendFree").disabled =false;
				$("freeVendMaxAmount").disabled =false;
				$("freeVendMaxCount").disabled =false;	
				$("campaignDonationPercent").disabled = false;
				$('campaignThresholdAmount').disabled = false;
				$('campaignPurchaseDiscountAmount').disabled = false;
				$('campaignDiscountPercent').disabled = false;
				$('passAllowed').disabled = false;
				$("productCampaign").disabled = false;
				if ($("totalDiscountAmount")){
					$("totalDiscountAmount").disabled = false;
				}
			} 
	}
	
	function validateDate(){
		if($('campaignStartDate')!=null&&$('campaignStartDate').value.trim()!=''&&$('campaignEndDate')!=null&&$('campaignEndDate').value.trim()!=''){
			var start = new Date($('campaignStartDate').value);
			var end = new Date($('campaignEndDate').value);
			if((start == 'Invalid Date') || (end == 'Invalid Date')){
				alert('You must enter a valid start and end date.');
				return false;
			} else {
				if(start>=end){
					alert("The end date must occur after the start date.");
					return false;
				}
			}
		}
		return true;
	}
	
	function validateCashback(){
		if($('campaignTypeId').value==2||$('campaignTypeId').value==3){
			if($('campaignThresholdAmount').value.trim().length==0){
				alert("Please input the Threshold Amount for the cashback reward type.");
				return false;
			}else{
				return true;
			}
		}else{
			return true;
		}
	}
	
	//Check that card type checked if campaign requires that.
	function validateAcctType(){
		if(acctTypesAvailable($('campaignTypeId').value)[0] != "all" &&
			!$('isForMoreCards').checked && !$('isForAllCards').checked && !$('isForPayrollDeductCards').checked &&
			!$("isForAll").checked){
				alert("Please choose card type. Current campaign type requres card type to be choosen.");
				return false;
		}else{
			return true;
		}
	}
	
	function onAssignCampaign(){
		var mode;
		if (currentCampaignState == "DRAFT") {
			mode = "edit";
		}else{
			mode = "view";
		}
		location.href="assign_campaign.html?campaignId="+$('campaignId').value+"&mode="+mode;
	}
	
    function campaignStatus(status){
		if(status == 0)
			$("message").set("html","<p class='status-info-success'>A request to send campaign emails has been created.</p>");
		else if(status == 1)
			$("message").set("html","<p class='status-info-failure'>Another request to send promotional emails was created less than 4 hours ago.</p>");
		else
			$("message").set("html","<p class='status-info-failure'>An error occurred while processing the request to send campaign emails.</p>");
	}
    
    var dayOfMonthDialog = new Dialog.Help({ helpKey: 'campaign-day-of-month-instructions', destroyOnClose: false, height: 200 });
    var dayOfWeekDialog = new Dialog.Help({ helpKey: 'campaign-day-of-week-instructions', destroyOnClose: false, height: 200 });
    var specificDaysDialog = new Dialog.Help({ helpKey: 'campaign-specific-days-instructions', destroyOnClose: false, height: 200 });
    var hoursDialog = new Dialog.Help({ helpKey: 'campaign-hour-instructions', destroyOnClose: false, height: 200 });
    var donationDialog = new Dialog.Help({ helpKey: 'campaign-donation-instructions', destroyOnClose: false, height: 200 });
    var allCardsDialog = new Dialog.Help({ helpKey: 'campaign-allcards-instructions', destroyOnClose: false, height: 200 });
    var moreCardsDialog = new Dialog.Help({ helpKey: 'campaign-morecards-instructions', destroyOnClose: false, height: 200 });
    var payrollDeductCardsDialog = new Dialog.Help({ helpKey: 'campaign-payrolldeductcards-instructions', destroyOnClose: false, height: 200 });
    var assignToExistingDialog = new Dialog.Help({ helpKey: 'campaign-assign-existing-instructions', destroyOnClose: false, height: 200 });
    
    function onAllCardTypes(){
    	displayTranTypes(null);
    }
    
    function onForAllCards(campaignTypeId){
    	if($("assignToAllCards")){
    		$("assignToAllCards").disabled=true;
    		$("assignToAllCards").checked = "";
    	}
    	if($("assignToAllPayrollCards")){
    		$("assignToAllPayrollCards").disabled=true;
    		$("assignToAllPayrollCards").checked = "";
    	}
    	displayTranTypes(5);
    	$('titleCardBrandList').set("style","display:table-row");
    	$("cardBrandList").set("style","display:table-cell");
    }
    
    function onForMoreCards(campaignTypeId){
    	if($("assignToAllCards")){
    		$("assignToAllCards").disabled=false;
    	}
    	if($("assignToAllPayrollCards")){
    		$("assignToAllPayrollCards").disabled=true;
    		$("assignToAllPayrollCards").checked = "";
    	}
    	displayTranTypes(3);
    	if ($("campaignTypeId").value !=2 && $("campaignTypeId").value !=6){
    		$('titleCardBrandList').set("style","display:none");
    		$("cardBrandList").set("style","display:none");
    		$("cardBrandAll").checked="";
    		cardBrandSelect(false, "DRAFT");
    	}
    }
    
    function onForPayrollDeductCards(campaignTypeId){
    	if($("assignToAllCards")){
    		$("assignToAllCards").disabled=true;
    		$("assignToAllCards").checked = "";
    	}
    	if($("assignToAllPayrollCards")){
    		$("assignToAllPayrollCards").disabled=false;
    	}
    	displayTranTypes(7);
    	if ($("campaignTypeId").value !=2 && $("campaignTypeId").value !=6){
    		$('titleCardBrandList').set("style","display:none");
    		$("cardBrandList").set("style","display:none");
    		$("cardBrandAll").checked="";
    		cardBrandSelect(false, "DRAFT");
    	}
    }
    
    function onPromoCodeSelect(){
    	if($("promoCodeSelect").selectedIndex==0){
    		$("assignToExistingCardsTr").set("style","display:none");
    		$("promoCode").value="";
    		$("promoCode").disabled=false;
    		$("promoCode").set("data-validators","required validate-regex-nospace:'<%=StringUtils.prepareScript("^[a-zA-Z0-9]{1,20}$")%>'");
    	}else{
    		$("assignToExistingCardsTr").set("style","display:table-row");
    		$("promoCode").value=$("promoCodeSelect").value;
    		$("promoCode").disabled=true;
    		$("promoCode").set("data-validators","");
    		if($("advice-required-promoCode")){
    			$("advice-required-promoCode").dispose();
    		}
    		$("promoCode").set("class","");
    	}
    }
    
    function onProductCampaignChange(){
    	onCampaignChange();
    	if ($("campaignId").value > 0) 
    	{
    		if ($('productCampaign').checked) {
    			$('manageCampaignProducts').set("style","display:inline");
    			
    		}else{
    			$('manageCampaignProducts').set("style","display:none");
    		}
    	}
    	if ($('campaignTypeId').value == 4){
    		if ($('productCampaign').checked){
    			$("freeVendMaxAmount").set("data-validators","validate-regex-nospace:'<%=StringUtils.prepareScript("^([1-9][0-9]{0,1}){1}(\\\\.[0-9]{1,2})?$")%>'");
				$('nthVendMaxAmountRequired').set("style","display:none");
				if($("advice-required-freeVendMaxAmount")){
						$("advice-required-freeVendMaxAmount").dispose();
					}
			}else{
				$("freeVendMaxAmount").set("data-validators","required validate-regex-nospace:'<%=StringUtils.prepareScript("^([1-9][0-9]{0,1}){1}(\\\\.[0-9]{1,2})?$")%>'");
				$('nthVendMaxAmountRequired').set("style","display:inline");
			}
		}	
    }
    
    function onForAllAcctTypes(){
    	if ($("isForAll")){
    		if ($("isForAll").checked){
    			displayAcctTypes(['all'], 'hide');
				onAllCardTypes();
				if($("assignToAllCards")){
    				$("assignToAllCards").disabled=false;
    			}
    			if($("assignToAllPayrollCards")){
    				$("assignToAllPayrollCards").disabled=false;
    			}
    		}
    		else{
    			displayAcctTypes(acctTypesAvailableList($("campaignTypeId").value), 'show');
				onAllCardTypes();
				if($("assignToAllCards")){
    				$("assignToAllCards").disabled=true;
    			}
    			if($("assignToAllPayrollCards")){
    				$("assignToAllPayrollCards").disabled=true;
    			}
		}
    }
    $('titleCardBrandList').set("style","display:table-row");
    $("cardBrandList").set("style","display:table-cell");
}

function onSelectAllTransTypes(){
	if ($("tranTypeIdAll").checked){
		tranTypes.forEach(function(item, key){
			if ($("tranTypeIdL"+key).style.display != 'none') {
				$("tranTypeId"+key).checked="true";
			}
		});
	}else{
		tranTypes.forEach(function(item, key){
			if ($("tranTypeIdL"+key).style.display != 'none') {
				$("tranTypeId"+key).checked="";
			}
		});
	}
}

function onAllCardBrand(){
	if ($("cardBrandAll").checked){
		cardBrandSelect(true, "DRAFT");
	}else{
		cardBrandSelect(false, "DRAFT");
	}
}

function cardBrandSelect(value, state){
	if (value == true){
		$("American Express").checked = "true";
		$("Discover Card").checked = "true";
		$("MasterCard").checked = "true";
		$("Visa").checked = "true";
	}else if (value == false){
		$("American Express").checked = "";
		$("Discover Card").checked = "";
		$("MasterCard").checked = "";
		$("Visa").checked = "";
	}else{
		i=0;
		while (i < campaignCardBrands.length){
			if (campaignCardBrands[i].campaignId == value){
				$(campaignCardBrands[i].cardBrand).checked = "true";
			}
			i++;
		}
	}	
	if (state == "STARTED" || state == "FINISHED"){
		$("American Express").disabled = true;
		$("Discover Card").disabled = true;
		$("MasterCard").disabled = true;
		$("Visa").disabled = true;
	}else{
		$("American Express").disabled = false;
		$("Discover Card").disabled = false;
		$("MasterCard").disabled = false;
		$("Visa").disabled = false;
	}
}

</script>
<div class="manageCampaign">
<div>
	<table>
		<tr>
			<td>
				<div id="waitImage" style="display:none">Please wait...<img alt="Please wait" src="images/pleasewait.gif"/></div>
			</td>
			<td>
				<div id="message" class="manageCampaignMessage"></div>
			</td>
		</tr>
	</table>
</div>

<table class="selectBody">
	<tr><td width="30%">
		<div class="selectSection">
			<div class="sectionTitle">Existing Campaigns</div>
				<div class="sectionRow">
					<select size="15" name="campaignId" id="campaignId" style="width: 100%;"><%
        while(campaignResults.next()) {%><option <%if(selectedCampaignId== ConvertUtils.getInt(campaignResults.get("campaignId"))){%> selected="true" <%} %>value="<%=StringUtils.encodeForHTMLAttribute(campaignResults.getFormattedValue("campaignId"))
        %>" onselect="setCampaign('<%=StringUtils.encodeForJavaScript(campaignResults.getFormattedValue("campaignName"))
        %>','<%=StringUtils.prepareScript(campaignResults.getFormattedValue("campaignDesc"))
        %>','<%=StringUtils.prepareScript(campaignResults.getFormattedValue("campaignTypeId"))
        %>','<%=StringUtils.prepareScript(campaignResults.getFormattedValue("campaignDiscountPercent"))
        %>','<%=StringUtils.prepareScript(campaignResults.getFormattedValue("campaignDonationPercent"))
        %>','<%=StringUtils.prepareScript(campaignResults.getValue("campaignStartDate")==null?"":campaignFormat.format(campaignResults.getValue("campaignStartDate")))
        %>','<%=StringUtils.prepareScript(campaignResults.getValue("campaignEndDate")==null?"":campaignFormat.format(campaignResults.getValue("campaignEndDate")))
        %>','<%=StringUtils.prepareScript(campaignResults.getFormattedValue("campaignRecurSchedule"))
        %>','<%=StringUtils.prepareScript(campaignResults.getFormattedValue("campaignThresholdAmount"))
        %>','<%=StringUtils.prepareScript(campaignResults.getFormattedValue("campaignAssignedToAllDevices"))
        %>','<%=StringUtils.prepareScript(campaignResults.getFormattedValue("campaignAssignedToAllCards"))
        %>','<%=StringUtils.prepareScript(campaignResults.getFormattedValue("campaignAssignedToAllPayrollCards"))
        %>','<%=StringUtils.encodeForJavaScript(campaignResults.getFormattedValue("promoCode"))
        %>','<%=StringUtils.prepareScript(campaignResults.getFormattedValue("campaignNthVendFree"))
        %>','<%=StringUtils.prepareScript(campaignResults.getFormattedValue("freeVendMaxAmount"))
        %>','<%=StringUtils.prepareScript(campaignResults.getFormattedValue("isForAllCards"))
        %>','<%=StringUtils.prepareScript(campaignResults.getFormattedValue("productCampaign"))
        %>','<%=StringUtils.encodeForJavaScript(campaignResults.getFormattedValue("campaignPurchaseDiscountAmount"))
        %>','<%=StringUtils.prepareScript(campaignResults.getFormattedValue("passAllowed"))
        %>','<%=StringUtils.prepareScript(campaignResults.getFormattedValue("freeVendMaxCount"))
        %>','<%=StringUtils.prepareScript(campaignResults.getFormattedValue("totalDiscountAmount"))
        %>','<%=StringUtils.prepareScript(campaignResults.getFormattedValue("totalDiscountAmountUsed"))
        %>','<%=StringUtils.prepareScript(campaignResults.getFormattedValue("state"))%>');">
        <%=StringUtils.prepareCDATA(campaignResults.getFormattedValue("campaignName"))%> 
        <%if(ConvertUtils.getInt(campaignResults.get("campaignTypeId"))==4){ %> 
        	(Promotion)
        <%}else if(ConvertUtils.getInt(campaignResults.get("campaignTypeId"))==5){ %> 
      	(Purchase Discount)
      	<%}else if(ConvertUtils.getInt(campaignResults.get("campaignTypeId"))==1 && ConvertUtils.getStringSafely(campaignResults.get("isForAllCards"),"").equals("")){ %> 
      	(Loyalty Discount)
        <%}else if(ConvertUtils.getStringSafely(campaignResults.get("isForAllCards"),"N").equals("N")){ %> (MORE cards)
        <%}else if(ConvertUtils.getStringSafely(campaignResults.get("isForAllCards"),"N").equals("Y")){ %> (All other cards)
        <%}else { %> (Payroll deduct cards)
        <%} %>
       	</option><%
		}%>
        </select>
    </div></div>
    </td>
    <td>
    <div class="selectSection">
			<div class="sectionTitle" id="campaignInfoTitle" >Selected Campaign Information</div>
    <table class="campaignInfo">
        
    <tr>
    <td>
    <table class="campaignInfo">
    	<tr><td colspan="2" class="nowrap">Campaign Name:<span class="required">*</span><input name="campaignName" id="campaignName" type="text" data-validators="required maxLength:60" onChange="onCampaignChange();"/></td></tr>
        <tr>
        	<td colspan="2">
        		<span><b>Status: </b></span>
        		<span id="campaignState" name="campaignState" value="DRAFT"></span>
        		<span id="campaignActiveStatus"></span>
        		<input type="button" id="startButton" value="Start campaign" style="font-weight: bold" onclick="$('editAction').value='start';onStateChange('start');"/>
			    <input type="button" id="finishButton" value="Finish campaign" style="font-weight: bold" onclick="$('editAction').value='finish';onStateChange('finish');"/>
			</td>
        </tr>
        <tr>
        	<td>
        		<input type="button" id="managePromoEmailButton" value="Manage Campaign Email" onclick="onManagePromoEmail();"/>
        	</td>
        </tr>
    	<tr><td colspan="2" class="campaignSettings"><strong>Campaign Settings</strong></td></tr>
        <tr><td>Type:<span class="required">*</span></td>
        <td><select onchange="onChangeCampaignType();" name="campaignTypeId" id="campaignTypeId" data-validators="required" ><%
        while(campaignTypeResults.next()) {%><option value="<%=StringUtils.prepareCDATA(campaignTypeResults.getFormattedValue("campaignTypeId"))%>" >
        <%=StringUtils.prepareCDATA(campaignTypeResults.getFormattedValue("campaignTypeName"))%>
       	</option><%
		}%>
        </select>  
        </td>
        </tr>
        <tr>
        	<td colspan="2">
        		<label id="productCampaignAv"><input id="productCampaign" type="checkbox" value="Y" name="productCampaign" onchange="onProductCampaignChange();" /> Product campaign</label>
        		<input type="button" id="manageCampaignProducts" value="Assign Products" onclick="onManageProducts();"/>     
        	</td>
        </tr>
        <tr><td colspan="2"><label id="isForAllAccs"><input name="isForAll" value="Y" id="isForAll" type="checkbox" onClick="onForAllAcctTypes();"/> For All Card Types.</label></td></tr>
        <tr>
        	<td colspan="2">
        		<div id="isForMoreCardsTd"><input type="radio" id="isForMoreCards" name="isForAllCards" value="N" onClick="onForMoreCards();"/>For MORE Cards <a class="help-button" onclick="moreCardsDialog.show();"><div></div></a></div>
        		<div id="isForPayrollDeductCardsTd"><input type="radio" id="isForPayrollDeductCards" name="isForAllCards" value="P" onClick="onForPayrollDeductCards(3);" />For Payroll Deduct Cards <a class="help-button" onclick="payrollDeductCardsDialog.show();"><div></div></a></div>
        		<div id="isForAllCardsTd"><input type="radio" id="isForAllCards" name="isForAllCards" value="Y" onClick="onForAllCards(1);"/>For All Other Bank Cards <a class="help-button" onclick="allCardsDialog.show();"><div></div></a></div>
        	</td>
        </tr>
        <tr><td></td></tr>
        <tr id="promoCodeSelCol" >
        <td colspan="2">
        	Promo Code:
        	<select id="promoCodeSelect" name="promoCodeSelect" onchange="onPromoCodeSelect();">
        	<option value="">Create New Promo Code</option>
			 <% while(promocodeCampaignResults.next()) {%><option value="<%=StringUtils.prepareCDATA(promocodeCampaignResults.getFormattedValue("promoCode"))%>" >
	        <%=StringUtils.prepareCDATA(promocodeCampaignResults.getFormattedValue("campaignName"))%> (<%=StringUtils.prepareCDATA(promocodeCampaignResults.getFormattedValue("promoCode"))%> )
	       	</option><%
			}%>
			</select>
		</td>
		</tr>
		<tr id="promoCodeCol">
			<td>Promo Code:</td>
			<td>
				<input name="promoCode" id="promoCode" type="text" size="20" style="display:inline"/>
				<label id="passAllowedLabel"><input name="passAllowed" value="Y" id="passAllowed" type="checkbox" onChange="onCampaignChange();"/> Allow Apple Promo Pass</label></td>
		</tr>
		<tr id="assignToExistingCardsTr" style="display:none">
		<td colspan="3">
			Assign to existing cards that have the Promo Code<input id="assignToExistingCards" name="assignToExistingCards"  type="checkbox" value="1" checked="checked"/><a class="help-button" onclick="assignToExistingDialog.show();"><div></div></a>
        </td>
        </tr>
        <tr><td>Offer Title:<span class="shownOnWebsiteNote">+</span></td><td><input name="campaignDesc" id="campaignDesc" type="text" size="21" class="maxLength:21" onChange="onCampaignChange();"/><br/>
        <input name="campaignDesc2" id="campaignDesc2" type="text" size="21" class="maxLength:21" onChange="onCampaignChange();"/></td></tr>
        <%if(!StringUtils.isBlank(campaignScope)){%>
        <tr id="totalDiscountAmountRow">
        	<td>Max Total Discount Amount, $: </td>
        	<td>
        		<input onChange="onCampaignChange();"  name="totalDiscountAmount" id="totalDiscountAmount" type="text" size="10" title="Maximum total discount amount, which will be paid back to customer. Works for global campaigns only. Campaign is active until all amount is spent." data-validators="validate-regex-nospace:'<%=StringUtils.prepareScript("^([1-9][0-9]{0,9})$")%>'"/>
        		<span> Used Discount Amount, $: </span>
        		<input name="totalDiscountAmountUsed" id="totalDiscountAmountUsed" type="text" disabled="true" size="10"/>
        	</td>
        </tr>
        <%} %>
        <tr></tr>
        <tr id="nthParams">
        <td colspan="3">
        	<table>
        	<tr>
        	<td>Nth Purchase Free: </td>
        	</tr>
        	<tr>
        		<td>
        			N=<div id="nthVendFreeRequired"><span class="required">*</span></div>
        		</td>
        		<td>
        			<input onChange="onCampaignChange();" disabled="disabled" name="campaignNthVendFree" id="campaignNthVendFree" type="text" size="2" title="Number of vends on which a free vend is given. Required when the campagin type is 'Nth Purchase Free'." data-validators="validate-regex-nospace:'<%=StringUtils.prepareScript("^([1-9]|[1-9][0-9])$")%>'"/>
        		</td>
        	</tr>
        	<tr>
        		<td>
        			Maximum $ Amount: <div id="nthVendMaxAmountRequired" style="display:inline"><span class="required">*</span></div>
        		</td>
        		<td>
        			<input onChange="onCampaignChange();" disabled="disabled" name="freeVendMaxAmount" id="freeVendMaxAmount" type="text" title="Required for non product campaigns only. It is a maximum dollar amount for purchase(product) to be available for free." size="4" data-validators="validate-regex-nospace:'<%=StringUtils.prepareScript("^([1-9][0-9]{0,1}){1}(\\.[0-9]{1,2})?$")%>'"/>
        		</td>
        	</tr>
        	<tr>
        		<td>
        			Maximum Quantity: 
        		</td>
        		<td>
        			<input onChange="onCampaignChange();" disabled="disabled" id="freeVendMaxCount" name="freeVendMaxCount" type="text" title="Quantity of free purchases(products) which campaign offers to consumers by each of theirs cards and passes." size="2" value="" data-validators="validate-regex-nospace:'<%=StringUtils.prepareScript("^(|[1-9]|[1-9][0-9])$")%>'"/>
        		</td>
        	</tr>
        	<tr></tr>
        	</table>
        <td>
        </tr>
        <tr></tr>
        <tr id="thresholdAmountRow">
        <td style="min-width:150px">Threshold Amount, $:<div id="thresholdAmountRequired"><span class="required">*</span></div></td>
        <td><input onChange="onCampaignChange();" disabled="disabled" name="campaignThresholdAmount" id="campaignThresholdAmount" type="text" size="2" title="Required when the campagin type is 'Replenish Reward' or 'Spend Reward'. It is a dollar amount with maxium 2 decimal points." data-validators="validate-regex-nospace:'<%=StringUtils.prepareScript("^\\d{1,8}(\\.\\d{1,2}){0,1}$")%>'"/></td>
        </tr>
        <tr id="discountPercentRow"><td>Discount Percent:<div id="campaignDiscountPercentRequired" style="display:inline"><span class="required">*</span></div></td><td><input onChange="onCampaignChange();" disabled="disabled" name="campaignDiscountPercent" id="campaignDiscountPercent" type="text" size="2" data-validators="validate-regex-nospace:'<%=StringUtils.prepareScript("^\\d{1,2}(\\.\\d{1,2}){0,1}$")%>'"/>%</td>
        </tr>
        <tr id="donationPercentRow"><td>Donation Percent:</td><td><input onChange="onCampaignChange();" name="campaignDonationPercent" id="campaignDonationPercent" type="text" size="2" data-validators="validate-regex-nospace:'<%=StringUtils.prepareScript("^\\d{1,2}(\\.\\d{1,2}){0,1}$")%>'"/>% <a class="help-button" onclick="donationDialog.show();"><div></div></a></td>
        </tr>
        <tr id="purchaseDiscountAmountRow">
        <td>Discount Amount:<div id="purchaseDiscountRequired" style="display:inline"><span class="required">*</span></div></td><td><input onChange="onCampaignChange();" disabled="disabled" name="campaignPurchaseDiscountAmount" id="campaignPurchaseDiscountAmount" type="text" size="2" title="Required when the campagin type is 'Purchase Discount'. It is a dollar amount with maxium 2 decimal points." data-validators="validate-regex-nospace:'<%=StringUtils.prepareScript("^\\d{1,8}(\\.\\d{1,2}){0,1}$")%>'"/></td>
        </tr>
        <tr>
        	<td>Start Date:</td>
        	<td id="startDate">
        		<input onChange="onCampaignChange();" name="campaignStartDate" id="campaignStartDate" type="text" size="10" data-validators="validate-date dateFormat:'mm/dd/yyyy'" data-toggle="calendar" data-format="%m/%d/%Y"/>
			</td>
			<td>
        		<input name="campaignStartDateView" id="campaignStartDateView" type="text" size="10" disabled="true"/>
        	</td>
        </tr>
        <tr>
        	<td>End Date:</td>
        	<td id="endDate">
        		<input onChange="onCampaignChange();" name="campaignEndDate" id="campaignEndDate" type="text" size="10" data-validators="validate-date dateFormat:'mm/dd/yyyy'" data-toggle="calendar" data-format="%m/%d/%Y"/>
        	</td>
        	<td>
        		<input name="campaignEndDateView" id="campaignEndDateView" type="text" size="10" disabled="true"/>
        	</td>
        </tr>
        <tr id="recurringSchedule0"><td colspan="2">Recurring Schedule:</td></tr>
        <tr id="recurringSchedule1"><td><input onChange="onCampaignChange();" type="radio" name="campaignRecurSchedule" value="M" id="campaignRecurScheduleM" onclick="clearDisableWeek();clearDisableField($('campaignRecurDay'));$('campaignRecurMonth').disabled=false; "/> Day of Month: </td><td><input onChange="onCampaignChange();" name="campaignRecurMonth" id="campaignRecurMonth" type="text" size="40" data-validators="validate-regex-nospace:'<%=StringUtils.prepareScript("^([1-9]|[12][0-9]|3[01]|(([1-9]|[12][0-9]|3[0])[-]([1-9]|[12][0-9]|3[01]))){1}([,]([1-9]|[12][0-9]|3[01]|(([1-9]|[12][0-9]|3[0])[-]([1-9]|[12][0-9]|3[01]))))*$")%>'" title="Required when Day of Month is selected. Campaign will only apply on the indicated days. You can use single day or duration of days separated by comma or semicolon.Valid days are 1-31. eg. 1,2,20-31"/>
        <a class="help-button" onclick="dayOfMonthDialog.show();"><div></div></a>
		</td></tr>
        <tr id="recurringSchedule2"><td><input onChange="onCampaignChange();" type="radio" name="campaignRecurSchedule" value="W" id="campaignRecurScheduleW" onclick="clearDisableField($('campaignRecurMonth'));clearDisableField($('campaignRecurDay'));enableWeek();" title="Required when Day of Week is selected. Campaign will only apply on the indicated days in the week. Use the checkbox to select the days."/> Day of Week:</td>
        <td>
        	<input onChange="onCampaignChange();" name="campaignRecurWeek"  type="checkbox" value="1" />Sun
        	<input onChange="onCampaignChange();" name="campaignRecurWeek"  type="checkbox" value="2" />Mon
        	<input onChange="onCampaignChange();" name="campaignRecurWeek"  type="checkbox" value="3" />Tue
        	<input onChange="onCampaignChange();" name="campaignRecurWeek"  type="checkbox" value="4" />Wed
        	<input onChange="onCampaignChange();" name="campaignRecurWeek"  type="checkbox" value="5" />Thu
        	<input onChange="onCampaignChange();" name="campaignRecurWeek"  type="checkbox" value="6" />Fri
        	<input onChange="onCampaignChange();" name="campaignRecurWeek"  type="checkbox" value="7" />Sat
        
        	<a class="help-button" onclick="dayOfWeekDialog.show();"><div></div></a>
		</td>
        </tr>
        <tr id="recurringSchedule3"><td><input onChange="onCampaignChange();" type="radio" name="campaignRecurSchedule" value="S" id="campaignRecurScheduleS" onclick="clearDisableWeek();clearDisableField($('campaignRecurMonth'));$('campaignRecurDay').disabled=false;"/> Specific Days:</td><td><input onChange="onCampaignChange();" name="campaignRecurDay" id="campaignRecurDay" type="text" size="40" data-validators="validate-multi-dates dateFormat:'mm/dd/yyyy'" title="Required when Specific Days is selected. Campaign will only apply on the indicated specific days. You can use date in MM/DD/YYYY and sperated by comma or semicolon for multiple dates. eg. 01/01/2014,01/02/2014"/>
        
            <a class="help-button" onclick="specificDaysDialog.show();"><div></div></a></td></tr>
        <tr id="recurringSchedule4"><td colspan="2"><input onChange="onCampaignChange();" type="radio" name="campaignRecurSchedule" value="D" id="campaignRecurScheduleD" onclick="clearDisableWeek();clearDisableField($('campaignRecurMonth'));clearDisableField($('campaignRecurDay'));"/> Every Day</td></tr>
        <tr id="recurringSchedule5"><td>Hours: </td><td><input onChange="onCampaignChange();" name="campaignRecurHour" id="campaignRecurHour" type="text" size="40" data-validators="validate-multi-hour-duration" title="Not required. Empty value means all hours. Once specified, campaign will only apply for the indicated hours on the allowed days. You can use 24hr format or 12hr format for duration separated by comma or semicolon. eg. 09:00-11:00,13:00-17:00"/>
        
        <a class="help-button" onclick="hoursDialog.show();"><div></div></a>
        </td></tr>
        <tr><td></td></tr>
        <tr>
        	<td colspan="3">	
        		Assign to all devices: <input id="assignToAllDevices" type="checkbox" value="Y" name="assignToAllDevices" onchange="onAssignToAllDevices();" />
	    		<input type="button" id="manageCampaignAssignmentButton" value="Assign Devices" onclick="onAssignCampaign();"/>
        	</td>
        </tr>
        <tr>
        	<td colspan="3">
        		<%if(StringUtils.isBlank(campaignScope)){%>
	    					Assign to all prepaid cards:<input id="assignToAllCards" type="checkbox" value="Y" name="assignToAllCards" onchange="onAssignCardsChange();" />
	    		<%}%>
		    	<input type="button" id="manageCampaignCardsButton" value="Assign Prepaid Cards" onclick="onManageCards();"/>  
        	</td>
        </tr>
        <tr>
        	<td colspan="3">	
        		<%if(StringUtils.isBlank(campaignScope)){%>
        			Assign to all payroll deduct cards:<input id="assignToAllPayrollCards" type="checkbox" value="Y" name="assignToAllPayrollCards" onchange="onAssignPayrollCardsChange();" />    	
	    		<%}%>
	    		<input type="button" id="manageCampaignPayrollCardsButton" value="Assign Payroll Deduct Cards" onclick="onManagePayrollCards();"/> 
        	</td>
        </tr>
        <tr>
    	<td colspan="2" align="left">
	    	<table>
			    <tr><td>&nbsp;</td></tr>
			    <tr class="groupContainer">
			    </tr>
			    <tr>
			    	<td colspan="2">
				    <input type="button" id="addButton" value="Add New" onclick="onAdd();"/>
				    &#160;<input type="button" id="saveNewButton" value="Save New" onclick="onSaveNew();"/>
				    &#160;<input type="button" id="saveButton" value="Save" onclick="$('editAction').value='save';onEditSave();"/>
				    &#160;<input type="button" id="cancelButton" value="Cancel" onclick="onCancel();"/>
				    &#160;<input type="button" id="deleteButton" value="Delete" onclick="$('editAction').value='delete';onEditSave();"/>
				    &#160;<input type="button" id="copyButton" value="Copy" onclick="onCopy();"/>
				    </td>
			    </tr>
			    <tr><td colspan="2"><span class="required">* Required</span></td></tr>
			    <tr><td colspan="2"><span class="shownOnWebsiteNote">+</span><span class="shownOnWebsiteDesc">Shown on MORE website and promotional emails</span></td></tr>    	
    	</table>
    	</td>
    </tr>
    </table>
     </td>
     <td>
     <table class="campaignInfo">
     	<tr id="titleTransTypesList">
        	<td> Check appropriate box(es) to select payment type(s). <br />Empty selection means no restriction.</td>
        </tr>
        <tr>
        	<td id="transTypesList">
        		<table class="sectionBox">
        			<col width="20">
       				<tr>
       					<th><input id="tranTypeIdAll" type="checkbox" onclick="onSelectAllTransTypes();onCampaignChange();"/></th>
       					<th>Payment Type</th>
       				</tr>
       				<%String[] tranTypeNames = new String[tranTypeIds.size()];
       				tranTypeIds.keySet().toArray(tranTypeNames);
       				Arrays.sort(tranTypeNames);
       				for (int j=0; j<tranTypeNames.length; j++) {
       					Integer typeId = tranTypeIds.get(tranTypeNames[j]);
       					String typeDesc = tranTypeNames[j]; %>
       					<tr id="tranTypeIdL<%=typeId%>">
       						<td><input value="<%=typeId%>" name="tranTypeSelected" id="tranTypeId<%=typeId%>" type="checkbox" onChange="onCampaignChange();"/></td>
       						<td><%=typeDesc%></td>
       					</tr>
       				<%} %>
        		</table>
        	</td>
        </tr>
        <tr id="titleCardBrandList">
        	<td> Check appropriate box(es) to select card brand(s). <br />Empty selection means no restriction.</td>
        </tr>
        <tr>
        	<td id="cardBrandList">
        		<table class="sectionBox">
        			<col width="20">
       				<tr>
       					<th align="center"><input id="cardBrandAll" type="checkbox" onclick="onAllCardBrand();onCampaignChange();"/></th>
       					<th>Card Brand</th>
       				</tr>
       				<tr id="cardBrand-American Express">
       					<td align="center"><input value="American Express" name="cardBrandSelected" id="American Express" type="checkbox" onChange="onCampaignChange();"/></td>
       					<td>American Express</td>
       				</tr>
       				<tr id="cardBrand-Discover Card">
       					<td align="center"><input value="Discover Card" name="cardBrandSelected" id="Discover Card" type="checkbox" onChange="onCampaignChange();"/></td>
       					<td>Discover Card</td>
       				</tr>
       				<tr id="cardBrand-MasterCard">
       					<td align="center"><input value="MasterCard" name="cardBrandSelected" id="MasterCard" type="checkbox" onChange="onCampaignChange();"/></td>
       					<td>MasterCard</td>
       				</tr>
       				<tr id="cardBrand-Visa">
       					<td align="center"><input value="Visa" name="cardBrandSelected" id="Visa" type="checkbox" onChange="onCampaignChange();"/></td>
       					<td>Visa</td>
       				</tr>
        		</table>
        	</td>
        </tr>
    </table>        
    </td>
    </tr>
    
</table>
</div>
</td>
</tr>
</table>

</div>
<input type="hidden" name="editAction" id="editAction" value=""/>
<input type="hidden" name="currentCampaignIsToAll" id="currentCampaignIsToAll" value=""/>
</form>
</div>
