<%@page import="simple.text.StringUtils,simple.bean.ConvertUtils"%>
<jsp:useBean id="campaignCardsResults" type="simple.results.Results" scope="request" />
<table class="folio">
<tbody><tr class="headerRow">
<th><input type="checkbox" id="selectAllCheckbox" onclick="onSelectAllCheckbox();"></th>
<th><a data-toggle="sort" data-sort-type="STRING" title="Sort Rows by Name">Customer Name</a></th>
<th><a data-toggle="sort" data-sort-type="STRING" title="Sort Rows by Number">Card Number</a></th>
<th><a data-toggle="sort" data-sort-type="NUMBER" title="Sort Rows by Identifier">Card Identifier</a></th>
<th><a data-toggle="sort" data-sort-type="STRING" title="Sort Rows by Name">Consumer Name</a></th>
<th><a data-toggle="sort" data-sort-type="STRING" title="Sort Rows by Location">Consumer Location</a></th>
<th><a data-toggle="sort" data-sort-type="STRING" title="Sort Rows by Region">Consumer Region</a></th>
<th><a data-toggle="sort" data-sort-type="STRING" title="Sort Rows by Campaigns">All Assigned Campaigns</a></th>
</tr>
</tbody>
<tbody><%
int i = 0;
while(campaignCardsResults.next()) {
	String customerName = campaignCardsResults.getValue("customerName", String.class);
    %><tr class="<%=(++i % 2) == 0 ? "even" : "odd" %>RowHover">
    <td><input <%if(ConvertUtils.getInt(campaignCardsResults.get("campaignId"))>0){%> checked="checked" campaignId="<%=campaignCardsResults.getValue("campaignId", Long.class) %>" <%}%> type="checkbox" title="" value="<%=campaignCardsResults.getValue("consumerAcctId", Long.class) %>" name="cardIds"></td>
	<td data-sort-value="<%=StringUtils.prepareCDATA(customerName == null ? null : customerName.toUpperCase()) %>"><%=StringUtils.prepareHTML(customerName) %></td>
	<td data-sort-value="<%=StringUtils.encodeForHTMLAttribute(campaignCardsResults.getValue("consumerAcctCd", String.class)) %>"><%=StringUtils.encodeForHTML(campaignCardsResults.getFormattedValue("consumerAcctCd")) %></td>
	<td data-sort-value="<%=StringUtils.encodeForHTMLAttribute(campaignCardsResults.getValue("consumerAcctIdentifier", String.class)) %>"><%=StringUtils.encodeForHTML(campaignCardsResults.getFormattedValue("consumerAcctIdentifier")) %></td>
    <td data-sort-value="<%
	String firstName = campaignCardsResults.getValue("consumerFirstName", String.class);
	String lastName = campaignCardsResults.getValue("consumerLastName", String.class);
	StringBuilder sortName = new StringBuilder();
	StringBuilder displayName = new StringBuilder();
    if(firstName != null) {
    	sortName.append(firstName.toUpperCase());
    	displayName.append(firstName).append(" ");
    }
    sortName.append('_');
	if(lastName != null) {
		sortName.append(lastName.toUpperCase());
		displayName.append(lastName);
	} 
    %><%=StringUtils.prepareCDATA(sortName.toString()) %>"><%=StringUtils.prepareHTML(displayName.toString()) %></td>
    <td data-sort-value="<%
	String state = campaignCardsResults.getValue("consumerState", String.class);
	String city = campaignCardsResults.getValue("consumerCity", String.class);
	StringBuilder sortLocation = new StringBuilder();
	StringBuilder displayLocation = new StringBuilder();
	if(city != null) {
		sortLocation.append(city.toUpperCase());
		displayLocation.append(city).append(",");
    }
	sortLocation.append('_');
	if(state != null) {
		sortLocation.append(state.toUpperCase());
		displayLocation.append(state);
    }
	String regionName = campaignCardsResults.getValue("regionName", String.class);
	String allCampaigns = campaignCardsResults.getValue("allCampaigns", String.class);
    %><%=StringUtils.prepareCDATA(sortLocation.toString()) %>"><%=StringUtils.prepareHTML(displayLocation.toString()) %></td>
    <td data-sort-value="<%=StringUtils.prepareCDATA(regionName == null ? null : regionName.toUpperCase()) %>"><%=StringUtils.prepareHTML(regionName) %></td>
    <td data-sort-value="<%=StringUtils.prepareCDATA(allCampaigns == null ? null : allCampaigns.toUpperCase()) %>"><%=StringUtils.prepareHTML(allCampaigns) %></td>
</tr><%
}
if(campaignCardsResults.getRowCount() < 1) {
    %><tr class="non-data-row"><td colspan="6">No Card Found</td></tr><%
}%>
</tbody>
</table>
