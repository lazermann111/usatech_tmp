<%@page import="simple.text.StringUtils,simple.servlet.RequestUtils"%>
<jsp:useBean id="tabs" type="simple.results.Results" scope="request" />
<table cellspacing="0" cellpadding="2"><tr><%
Integer selectedTabId = RequestUtils.getAttribute(request,"selectedTab", Integer.class, false);
String url = null;
Boolean border = null;
while(tabs.next()) {
	int tabId = tabs.getValue("id", Integer.class);
    if((selectedTabId != null && tabId == selectedTabId) || ((selectedTabId == null || selectedTabId == 0) && url == null)) {
        url = tabs.getValue("href", String.class);
        border = RequestUtils.getAttribute(request,"border",Boolean.class, false);
        %><th class="selectedTab" title="<%=StringUtils.prepareCDATA(tabs.getValue("description", String.class))%>" onmouseover="window.status = '<%=StringUtils.prepareScript(tabs.getValue("description", String.class))%>'; return true;"
    onmouseout="window.status = ''"><%=StringUtils.prepareHTML(tabs.getValue("title", String.class))%></th><%
    } else {
        %><th class="unselectedTab">
        	<a href="tabs.i?usage=<%=StringUtils.prepareURLPart(RequestUtils.getAttribute(request, "usage", String.class, false))%>&amp;selectedTab=<%=tabId%>"
    title="<%=StringUtils.prepareCDATA(tabs.getValue("description", String.class))%>" onmouseover="window.status = '<%=StringUtils.prepareScript(tabs.getValue("description", String.class))%>'; return true;"
    onmouseout="window.status = ''"><%=StringUtils.prepareHTML(tabs.getValue("title", String.class))%></a></th><%
    }
}%></tr>
<tr><td class="tabContents" colspan="<%=tabs.getRow() - 1%>"><div id="<%=(border == null || border.booleanValue() ? "tcwb" : "tc")%>"><%
if(url != null) {
    if(url.startsWith("./")) url = url.substring(2);
    url += (url.indexOf('?') < 0 ? '?' : '&') + "unframed=true";
    %><jsp:include page="<%=StringUtils.prepareCDATA(url)%>" flush="true"/><%
} else {
    %>Please choose a tab<%
}%></div></td></tr>
</table>