<%@page import="java.text.SimpleDateFormat"%>
<%@page import="simple.text.StringUtils,simple.servlet.RequestUtils,simple.bean.ConvertUtils"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="simple.results.Results"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>

<%
	SimpleDateFormat campaignFormat = new java.text.SimpleDateFormat("MM/dd/yyyy");
	String campaignScope = ConvertUtils.getString(RequestUtils.getAttribute(request, "campaignScope", String.class, false), "");
	int selectedCampaignId = ConvertUtils.getInt(RequestUtils.getAttribute(request, "campaignId", true));
	String mode = ConvertUtils.getString(RequestUtils.getAttribute(request,"mode", String.class, false), "view");
	String backToManageCampaignLink=null;
	if(campaignScope.equals("[Global]")){
		backToManageCampaignLink="manage_campaign_internal.i";
	}else{
		backToManageCampaignLink="manage_campaign.i";
	}
	String selectedProduct="";
	String selectedCampaignProduct=""; 
	
	Map<String, Object> params = new HashMap<>();
	params.put("campaignId", selectedCampaignId);
	
%>
<form name="campaignProductsForm" id="campaignProductsForm">
<input type="hidden" id="redirectHome" name="redirectHome" value="true"/>
<input type="hidden" name="campaignScope" id="campaignScope" value="<%=StringUtils.prepareCDATA(campaignScope)%>"/>
<input type="hidden" name="campaignId" id="campaignId" value="<%=selectedCampaignId%>"/>
<input type="hidden" name="subName" id="subName" value=""/>
<input type="hidden" name="productDelCode" id="productDelCode" value="" />
<input type="hidden" name="action" id="action" value="show" />
<input type="hidden" name="codeToDeviceCheck" id="codeToDeviceCheck" value=""/>
<script type="text/javascript">

function backToManageCampaign(){
	location.href="<%=backToManageCampaignLink%>?campaignId=<%=selectedCampaignId%>";
}

function onAddProduct(){
	var code="";
	code = $('productCode').value;
	if (code ==''){
		alert("Please enter product code.");	
	}else{
			if ($('productName').value==''){
				$('productName').value=code;
			}
			$('action').value = "add";
			updateProducts();
	}	
}

function onRemoveProduct(){
	$('productDelCode').value = selectedCampaignProduct;
	if (!$('productDelCode').value == ''){
		$('action').value = "remove";
		updateProducts();
	}
}

function updateProducts(){
	new Request.HTML({ 
	   	url: "assign_products.html",
	   	data: $("campaignProductsForm"), 
	   	method: 'post',
	   	update : $("productList"),
	   	onComplete : function() {
	   		$("waitImage").style.display='none';
	   	},
		evalScripts: true
	}).send();
}

function showProducts(){
	$('action').value = "show";
	updateProducts();
}

function onSearchProduct(){
	
		$('subName').value="%" + $('searchCode').value + "%";
		$("message").set("html","");
		$("waitImage").style.display='block';
		new Request.HTML({ 
		   	url: "assign_product_search.html",
		   	data: $("campaignProductsForm"), 
		   	method: 'post',
		   	update : $("products"),
		   	onComplete : function() {
		   		$("waitImage").style.display='none';
		   	},
			evalScripts: true
		}).send();
		$('deviceIds').empty();
		$('productCode').value = "";
		$('productName').value = "";
}

function onProductSelect(){
	element = document.getElementById('products');
	productLine = element.options[element.selectedIndex].text;
	selectedProduct = productLine.split('=')[0];
	$('productCode').value = selectedProduct;
	$('codeToDeviceCheck').value = selectedProduct;
	onDeviceCheck();
}

function onCampaignProductSelect(){
	element = document.getElementById('productCodes');
	productLine = element.options[element.selectedIndex].text;
	selectedCampaignProduct = productLine.split('=')[0];
	$('codeToDeviceCheck').value = selectedCampaignProduct;
	onDeviceCheck();
}

function onDeviceCheck(){
$("message").set("html","");
	$("waitImage").style.display='block';
	new Request.HTML({ 
	   	url: "product_device_check.html",
	   	data: $("campaignProductsForm"), 
	   	method: 'post',
	   	update : $("deviceIds"),
	   	onComplete : function() {
	   		$("waitImage").style.display='none';
	   	},
		evalScripts: true
	}).send();
}

</script>
<div id="waitImage" style="display:none">Please wait...<img alt="Please wait" src="images/pleasewait.gif"/></div>
<div id="message"></div>
<table class="selectBody">
	<caption>Manage Campaign Products</caption>
	<tr>
		<td valign="top">
    	<div class="selectSection">
			<div class="sectionTitle" id="searchCardsTitle" >Assign Products to Campaign</div>
			<table class="subSectionTable">
			<tr>
			<td>
			<table>
			<tr>
				<td>
					Search for product, coil or UPC (in Column Maps):
					<input type="text" id="searchCode" name="searchCode"/>
					<input title="Search product" id="searchProduct" type="button" onclick="onSearchProduct();" value="Search" />
					<br/><b>Search Result Content(Vend Column=Coil=Code)</b>
					<div id="productSelect">
						<select size="16" name="products" id="products" onchange="onProductSelect();" style="width: 100%; height:150px;">
						</select>
					</div>
				</td>
 			</tr>
 			<tr>
    			<td>
    				Vend Column:
    				<input type="text" id="productCode" name="productCode" />
    				*Enter value or choose in search results
    			</td>
    		</tr>
    		<tr>
    			<td>
    				Product name for campaign:
    				<input type="text" id="productName" name="productName" />
					<input title="Add product to campaign" id="addProduct" type="button" onclick="onAddProduct();" value="Add product"/>
					<input title="Remove product from campaign" id="removeProduct" type="button" onclick="onRemoveProduct();" value="Remove product"/>
				</td>
			</tr>
			<tr>
				<td>
					<div id="productList">
						<div id="notification" style="color:red"></div>
						<select  size="16" name="productCodes" id="productCodes" onchange="onCampaignProductSelect();" style="width: 100%; height: 150px;">
						</select>
					</div>
					<div>**Vend column value for each product should be the same across all assigned devices.</div>
				</td>
			</tr>
			</table>
			</td>
			<td style="width: 10px;"></td>
			<td>
			<table>
			<tr>
				<td style="width: 300px"><span>Devices assign to campaign.<br /></span> 
										<span style="font-weight: bold"> NOTE:</span> 
										<span> Devices in</span> <span style="color:red"> RED </span> 
										<span> don't have selected vend column in column mapping or no column mapping at all:</span></td>
			</tr>
			<tr>
				<td>
					<div id="deviceList">
						<select  size="16" name="deviceIds" id="deviceIds"  class="showDevices" style="width: 300px; height: 400px;">
						</select>
					</div>
				</td>
			</tr>
			</table>
			</td>
			</tr>
			<tr>
				<td align="center">
				<input type="button" value="Back To Manage Campaign" onclick="backToManageCampaign();"/>
				</td>
			</tr>
			</table>
		</div>
    	</td>
    </tr>
</table>
	<script>
		showProducts();
		if ('<%=mode%>' == 'view'){
			$('searchProduct').disabled = true;
			$('addProduct').disabled = true;
			$('removeProduct').disabled = true;
		}
	</script>
</form>
