<%@page import="simple.text.StringUtils"%>
<jsp:useBean id="customerResults" type="simple.results.Results" scope="request" />
<select multiple="multiple" size="4" name="customerIds" id="customerIds"  style="width: 100%;">
<%
while(customerResults.next()) {
    %>
    <option value="<%=customerResults.getValue("customerId", int.class) %>"><%=StringUtils.prepareHTML(customerResults.getFormattedValue("customerName")) %></option>
    <%
}
%>
</select>

