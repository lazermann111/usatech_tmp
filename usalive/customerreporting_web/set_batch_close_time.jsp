<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.RequestUtils"%>
<%
	String batchCloseTime=RequestUtils.getAttribute(request, "batchCloseTime", String.class, false);
	if (batchCloseTime.startsWith("00:"))
		batchCloseTime = "12" + batchCloseTime.substring(2);
%>
<form name="batchCloseTimeForm" id="batchCloseTimeForm">
<input type="hidden" id="redirectHome" name="redirectHome" value="true"/>
<input type="hidden" name="session-token" value="<%=RequestUtils.getSessionToken(request)%>" />
<script type="text/javascript">
	
	window.addEvent('domready', function() { 
		new Form.Validator.Inline.Mask(document.batchCloseTimeForm);
	});
	
	function onSave(){
		if($("batchCloseTimeForm").validate()){
			
			new Request.HTML({ 
		    	url: "save_batch_close_time.i",
		    	data: $("batchCloseTimeForm"), 
		    	method: 'post',
				evalScripts: true
			}).send();
		}
	}
	
	function editBatchCloseTimeResult(status, errorMessage){
		if(status>0){
			$("result-message").set("html","<p class='status-info-success'>Successfully saved the batch close time.</p>");
		}else{
			$("result-message").set("html","<p class='status-info-failure'>"+errorMessage+"</p>");
		}
	}
</script>
<div id="result-message"></div>
<table>
<tbody>
<tr>
<td>
<div class="selectSection">
<div class="sectionTitle">Daily Batch Close Time</div>
<div class="sectionRow">
	<table>
		<tr>
			<td class="requirementNote" colspan=2>Please enter time between 12:00 AM and 6:00 AM in EST timezone.</td>
		</tr>
		<tr>
			<td>Batch Close Time: <input type="text" size="5" maxlength="5" name="batchCloseTime" id="batchCloseTime" value="<%=StringUtils.prepareCDATA(batchCloseTime)%>" data-validators="validate-regex:'^((12|0*[1-5]):([0-5][0-9]))|(0*6:00)$'" /></td>
			<td width="55%" style="text-align: left; vertical-align: top; padding-top: 5px;">AM</td>
		</tr>
		<tr>
			<td valign="bottom" align="center" colspan="3"><input type="button" id="saveButton" value="Save Changes" onclick="onSave();"/></td>
		</tr>
	</table>
</div>
</div>
</td>
</tr>
</tbody>
</table>
</form>
