<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.text.StringUtils"%>
<jsp:useBean id="terminalResults" type="simple.results.Results" scope="request" />
<div><%
String instructions = RequestUtils.getAttribute(request, "instructions", String.class, false);
if(instructions != null && (instructions=instructions.trim()).length() > 0) {%><div class="caption"><%=StringUtils.prepareHTML(instructions) %></div><%}
while(terminalResults.next()) {
	Integer[] columnIndexes = terminalResults.getValue("columnIndexes", Integer[].class);
    if(terminalResults.isGroupBeginning(0)) {
		%><table class="terminalChecklist">
        <thead><tr class="tableHeader"><%
        String[] columnLabels = terminalResults.getValue("columnLabels", String[].class);
        for(int i = 0; i < columnIndexes.length; i++) {
        	if(columnIndexes[i] != null) {
            %><th><a data-toggle="sort" data-sort-type="STRING"><%=StringUtils.prepareHTML(columnLabels[i])%></a></th><%
        	}
        } %></tr></thead><%
	}
    %><tr><%
    long terminalId = terminalResults.getValue("terminalId", long.class);
    for(int i = 0; i < columnIndexes.length; i++) {
        if(columnIndexes[i] != null) {
            %><td data-sort-value="<%=StringUtils.encodeForHTMLAttribute(terminalResults.getFormattedValue("data" + columnIndexes[i]))%>"><a href="terminal_details.i?terminalId=<%=terminalId%>" onmouseover="window.status = 'View Terminal Details'; return true;" onmouseout="window.status = ''">
            <%=StringUtils.prepareHTML(terminalResults.getFormattedValue("data" + columnIndexes[i]))%></a></td><%
        }
    }%></tr><%
    if(terminalResults.isGroupEnding(0)) {
    	%></table><%
    }
}
if(terminalResults.getRow() <= 1) { 
	%><span class="no-terminals">No devices found</span><%
} %></div>