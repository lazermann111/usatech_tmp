<jsp:useBean id="googleMapsApiKey" type="java.lang.String" scope="request" />
<jsp:useBean id="deviceLocation" type="com.usatech.usalive.device.dto.DeviceLocation" scope="request" />

<div>
<div id="map" style="height: 600px;"></div>
<script type="text/javascript">
    var map;
    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: <%=deviceLocation.getLatitude()%>, lng: <%=deviceLocation.getLongitude()%>},
            zoom: 13
        });

        var circle = new google.maps.Circle({
            strokeColor: '#FF0000',
            strokeOpacity: 0.7,
            strokeWidth: 2,
            fillColor: '#FFCC00',
            fillOpacity: 0.3,
            map: map,
            center: {lat: <%=deviceLocation.getLatitude()%>, lng: <%=deviceLocation.getLongitude()%>},
            radius: 1300
        });
    }
</script>
<script async="async" defer="defer"
        src="https://maps.googleapis.com/maps/api/js?key=<%=googleMapsApiKey%>&amp;callback=initMap">
</script>
</div>