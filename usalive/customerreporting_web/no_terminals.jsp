<%@page import="simple.servlet.RequestUtils,com.usatech.usalive.servlet.UsaliveUser,simple.servlet.SimpleServlet"%>
<%
UsaliveUser user = RequestUtils.getAttribute(request, SimpleServlet.ATTRIBUTE_USER, UsaliveUser.class, true);
/* we are going to hard code this for now (BSK 09-22-04)*/
String deviceName = "Device";
String usatBrand = "";
String agreeType = "License";
Long dealerId = RequestUtils.getAttribute(request, "dealerId", Long.class, false);
if(dealerId != null && dealerId == 302L) { /* MEI */
	deviceName = "MEI System";
	usatBrand = "USALive&#174;";
	agreeType = "Services";
}
%>
<table class="noTerminals"><%
if(user.getCustomerId() != 0) {
    boolean canActivate = true;
    if(user.isMissingLicense()) {
        canActivate = false;
        %><tr><td><ul class="required"><li><%
        if(user.hasPrivilege("3" /*Create Bank Acct*/)) {
            %>We have not received your signed <%=agreeType%> Agreement. If you have
     already faxed us this document with your signature, thank you - your account will be active soon. If not, please goto&#160;<a href="license_instructions.i" onmouseover="window.status = 'Print Your <%=agreeType%> Agreement'; return true;"
            onmouseout="window.status = ''" title="Print Your <%=agreeType%> Agreement"><%=agreeType%> Agreement</a> and follow the instructions.<%
        } else {
            %>We have not received a signed <%=agreeType%> Agreement for your company. Please 
        ask your <%=deviceName%> administrator to print and fax this document to USA Technologies.<%
        }%></li></ul></td></tr><%
    } 
    if(user.isMissingBankAccount()) {
        canActivate = false;
        %><tr><td><ul class="required"><li><%
        if(user.hasPrivilege("3" /*Create Bank Acct*/)) {
        %>You have not yet established a Bank Account for Direct Credit Payments from USA Technologies. Please goto&#160;<a href="new_bank_acct.i" onmouseover="window.status = 'Create a New Bank Account'; return true;"
            onmouseout="window.status = ''" title="Establish a Bank Account to Receive Payments from USA Technologies">New Bank Acct</a> to setup your Bank Account.<%
        } else {
        %>A Bank Account for Direct Credit Payments has not yet been established for your company. Please 
    ask your <%=deviceName%> administrator to log on to USALive Online and set up a Bank Account.<%
            }%></li></ul></td></tr><%
    }
    if(user.hasPrivilege("3" /*Create Bank Acct*/) && user.getPendingBankAccountCount() > 0) {
        if(user.getActiveBankAccountCount() == 0) canActivate = false;
        %><tr><td><ul class="required"><li>We have not received a signed Authorization For Electronic Funds Transfer Form for <%=user.getPendingBankAccountCount()%> of
        your bank accounts. If you have already faxed us this form with your signature, thank you - your account will be active soon. If not, please goto&#160;<a href="eft_auth_instructions.i" onmouseover="window.status = 'Print an EFT Authorization Form'; return true;"
                onmouseout="window.status = ''" title="Print an EFT Authorization Form">EFT Auth Form</a> and follow the instructions.</li></ul></td></tr><%
    } 
    if(user.getTerminalCount() == 0 && canActivate) {
        %><tr><td><ul class="required"><li><%
        if(user.hasPrivilege("4" /*Activate Terminal */)) { 
            %>Your devices have not been activated yet. Please goto&#160;<a href="new_terminal.i" onmouseover="window.status = 'Activate Device'; return true;"
                    onmouseout="window.status = ''" title="Activate Devices so you can begin Cashless Vending!">Activate Device</a> to activate your devices.<%
        } else {
            %>No active devices are assigned to you. Please ask your <%=deviceName%> administrator to assign devices to you.<%
        }%></li></ul></td></tr><%
    }
} else if(user.getTerminalCount() == 0) {%>
<tr><td><ul><li>No active devices are assigned to you.</li></ul></td></tr> 
<%}%></table>