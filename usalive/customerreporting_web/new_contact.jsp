<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="simple.servlet.RequestUtils"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<html>
<head>
<title>New Contact</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="copyright" content="Copyright 2002, USA Technologies, Inc." />
<meta name="author" content="Brian Krug" />
<link href="<%=StringUtils.prepareCDATA(request.getContextPath())%>/theme/Reports.css" type="text/css" rel="stylesheet" />
<jsp:useBean id="onloadMessage" class="java.lang.String" scope="request" />
<jsp:useBean id="onloadScript" class="java.lang.String" scope="request" />
<script language="javascript" type=""><%
if (!onloadMessage.equals("")) { 
    %>alert("<%=StringUtils.prepareScript(onloadMessage)%>");<%
}%><%=onloadScript%>
</script>
<script language="javascript" defer="defer" type="">
    function cancelChanges() {
       window.close();
    }

    function okPassword() {
        with(window.document.userForm) {
            if(password.value != confirm.value) {
                alert("The password and its confirmation do not match. Please re-type both.");
                password.focus();
                return false;
            }
            if(password.value.length < 5) {
                alert("The password must be at least 5 digits. Please choose another.");
                password.focus();
                return false;
            }
        }
        return true;
    }

    function validateForm() {
        with(window.document.userForm) {
            if(String.from(user_name.value).trim() == "") {
                alert("Please enter a Member ID which this contact will use to Login to USA Live Online.");
                return false;
            }
            if(String.from(user_name.value).trim().length < 5) {
                alert("Member ID's must be at least 5 characters long. Please enter a different Member ID.");
                return false;
            }
            if(String.from(first_name.value).trim() == "") {
                alert("Please enter the contact's first name.");
                return false;
            }
            if(String.from(last_name.value).trim() == "") {
                alert("Please enter the contact's last name.");
                return false;
            }
            if(String.from(email.value).trim() == "") {
                alert("Please enter the contact's email.");
                return false;
            }
            /*if(numbersOnly(telephone.value).length < 10) {
                alert("Please enter the contact's telephone number.");
                return false;
            }*/
            if(!okPassword()) return false;
        }
        return true;
    }

    function userChanged() {}
</script>
<style>
#uf1 {
	margin-top: 0px;
}
</style>
</head>
<body>
<jsp:useBean id="privResults" type="simple.results.Results" scope="request" />
<% UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);%>
<p class="instructions">Please enter the contact's information then click submit:</p>
<form id="uf1" autocomplete="off" name="userForm" action="<%=request.getContextPath()%>/i" method="post" onsubmit="return validateForm();" target="_self">
<input type="hidden" name="requestHandler" value="insert_contact" />
<input type="hidden" name="requestAction" value="insert" />
<input type="hidden" name="unframed" value="true" />
<table cellspacing="2" cellpadding="2" border="0">
    <tr class="tableHeader"><th colspan="2">Enter the Contact's Information</th></tr><%String tmp;%>
    <tr class="tableDataShade"><td>Member ID</td><td><input size="50" maxlength="20" type="text" name="user_name" value='<%=((tmp=StringUtils.prepareHTML(request.getParameter("user_name"))) == null ? "" : tmp)%>' onchange="userChanged()" /></td></tr>
    <tr class="tableDataShade"><td>First Name</td><td><input size="50" maxlength="50" type="text" name="first_name" value='<%=((tmp=StringUtils.prepareHTML(request.getParameter("first_name"))) == null ? "" : tmp)%>' onchange="userChanged()" /></td></tr>
    <tr class="tableDataShade"><td>Last Name</td><td><input size="50" maxlength="50" type="text" name="last_name" value='<%=((tmp=StringUtils.prepareHTML(request.getParameter("last_name"))) == null ? "" : tmp)%>' onchange="userChanged()" /></td></tr>
    <tr class="tableDataShade"><td>Password</td><td><input size="50" maxlength="20" type="password" name="password" value='<%=((tmp=StringUtils.prepareHTML(request.getParameter("password"))) == null ? "" : tmp)%>' onchange="userChanged()" /></td></tr>
    <tr class="tableDataShade"><td>Confirm Password</td><td><input size="50" maxlength="20" type="password" name="confirm" value='<%=((tmp=StringUtils.prepareHTML(request.getParameter("confirm"))) == null ? "" : tmp)%>' onchange="userChanged()" /></td></tr>
    <tr class="tableDataShade"><td>Email</td><td><input size="50" maxlength="255" type="text" name="email" value='<%=((tmp=StringUtils.prepareHTML(request.getParameter("email"))) == null ? "" : tmp)%>' onchange="userChanged()" /></td></tr>
    <tr class="tableDataShade"><td>Telephone</td><td><input size="50" maxlength="20" type="text" name="telephone" onpropertychange="enforceTelephoneFormat(this);" value='<%=((tmp=StringUtils.prepareHTML(request.getParameter("telephone"))) == null ? "" : tmp)%>' onchange="userChanged()" /></td></tr>
    <tr class="tableDataShade"><td>Fax</td><td><input size="50" maxlength="20" type="text" name="fax" onpropertychange="enforceTelephoneFormat(this);" value='<%=((tmp=StringUtils.prepareHTML(request.getParameter("fax"))) == null ? "" : tmp)%>' onchange="userChanged()" /></td></tr><%
    int[] privs = RequestUtils.getAttribute(request, "privs", int[].class, false);
    java.util.Arrays.sort(privs);
    while(privResults.next()) {
        %><tr class="tableDataShade"><td><%=StringUtils.prepareHTML(privResults.getFormattedValue("description"))%></td><td><input type="checkbox" name="privs" value='<%=StringUtils.encodeForHTMLAttribute(privResults.getFormattedValue("priv_id"))
        %>' <%if(java.util.Arrays.binarySearch(privs,((Number)privResults.getValue("priv_id")).intValue()) >= 0) { %>="<%if(java.util.Arrays.binarySearch(privs,((Number)privResults.getValue("priv_id")).intValue()) >= 0) { %>"checked ="checked"<%}%>="<%}%>"onchange="userChanged()" /></td></tr><%
    }%>
    <tr class="tableDataShade"><td colspan="2" align="center" valign="bottom"><input type="submit" name="submitButton" value="Save Changes" />
<!--<INPUT type=button onclick="window.opener.newUser(this.form);" value="Save Changes">--><input type="button" name="cancelButton" value="Cancel Changes" onclick="cancelChanges()" />
</td></tr></table></form></body></html>