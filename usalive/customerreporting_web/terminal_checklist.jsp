<%@page import="simple.text.StringUtils,simple.servlet.RequestUtils,java.util.Set, simple.results.Results"%>
<div><%
Results terminalResults = RequestUtils.getAttribute(request, "terminalResults", Results.class, true);
Set<?> selectedTerminals = RequestUtils.getAttribute(request, "selectedTerminals", Set.class, false);
while(terminalResults.next()) {
	Integer[] columnIndexes = terminalResults.getValue("columnIndexes", Integer[].class);
    if(terminalResults.isGroupBeginning(0)) {
		%><table border="0" cellPadding="2" cellSpacing="3" class="terminalChecklist">
        <thead><tr class="tableHeader"><th>&#160;</th><%
        String[] columnLabels = terminalResults.getValue("columnLabels", String[].class);
        for(int i = 0; i < columnIndexes.length; i++) {
        	if(columnIndexes[i] != null) {
            %><th><a data-toggle="sort" data-sort-type="STRING"><%=StringUtils.prepareHTML(columnLabels[i])%></a></th><%
        	}
        } %></tr></thead><%
	}
    int terminalId = terminalResults.getValue("terminalId", Integer.class);
    %><tr class="tchecklist">
		<td width="20">
			<input type="checkbox" onclick="terminalSelectionChanged(this)" value="<%=terminalId%>" name="terminals" id="terminals" <%
			if(selectedTerminals != null && selectedTerminals.contains(terminalId)) { %> checked="checked"<%} %>/>
		</td><%
    for(int i = 0; i < columnIndexes.length; i++) {
        if(columnIndexes[i] != null) {
            %><td <%if(i== 0){%>id="t<%=terminalId%>"<%} %> data-sort-value="<%=StringUtils.prepareCDATA(terminalResults.getFormattedValue("data" + columnIndexes[i]))%>"><%=StringUtils.prepareHTML(terminalResults.getFormattedValue("data" + columnIndexes[i]))%></td><%
        }
    }%></tr><%
    if(terminalResults.isGroupEnding(0)) {
    	%></table><%
    }
}
if(terminalResults.getRow() <= 1) { 
	%><span class="no-terminals">No devices found</span><%
} %></div>