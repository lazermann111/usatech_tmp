<%@page import="simple.text.StringUtils"%>
<jsp:useBean id="terminalResults" type="simple.results.Results" scope="request" />
<div><%
while(terminalResults.next()) {
	Integer[] columnIndexes = terminalResults.getValue("columnIndexes", Integer[].class);
    if(terminalResults.isGroupBeginning(0)) {
		%><table class="terminalChecklist">
        <thead><tr class="tableHeader"><th>View</th><th>Edit</th><%
        String[] columnLabels = terminalResults.getValue("columnLabels", String[].class);
        for(int i = 0; i < columnIndexes.length; i++) {
        	if(columnIndexes[i] != null) {
            %><th><a data-toggle="sort" data-sort-type="STRING"><%=StringUtils.prepareHTML(columnLabels[i])%></a></th><%
        	}
        } %></tr></thead><%
	}
    long terminalId = terminalResults.getValue("terminalId", Long.class);
    char allowEdit = terminalResults.getFormattedValue("allowEdit").toUpperCase().charAt(0);    		
    char changeEdit = terminalResults.getFormattedValue("changeEdit").toUpperCase().charAt(0);    		
    char customEdit = terminalResults.getFormattedValue("customEdit").toUpperCase().charAt(0);    		
    %><tr>
    <td>
    <input type="checkbox" name="view" <%
    if(changeEdit != '-' && allowEdit == customEdit) {
        %>value="<%=terminalId%>" onclick="handlePrivChange(this)" id="v<%=terminalId%>"<%
        if(allowEdit == 'Y') {%> disabled="disabled"<%}
    } else {
        %>disabled="disabled"<%
    } 
    if(allowEdit != '-') {%> checked="checked"<%} %>/></td>
    <td>
	<input type="checkbox" name="edit" <%
	if(changeEdit == 'Y' && allowEdit == customEdit) {
	   %>value="<%=terminalId%>" onclick="handlePrivChange(this)" id="e<%=terminalId%>"<%
	} else {
	   %>disabled="disabled"<%
	}
    if(allowEdit == 'Y') {%> checked="checked"<%} %>/></td>
    <%
    for(int i = 0; i < columnIndexes.length; i++) {
        if(columnIndexes[i] != null) {
            %><td data-sort-value="<%=StringUtils.encodeForHTMLAttribute(terminalResults.getFormattedValue("data" + columnIndexes[i]))%>"><a href="terminal_details.i?terminalId=<%=terminalId%>" onmouseover="window.status = 'View Terminal Details'; return true;" onmouseout="window.status = ''">
            <%=StringUtils.prepareHTML(terminalResults.getFormattedValue("data" + columnIndexes[i]))%></a></td><%
        }
    }%></tr><%
    if(terminalResults.isGroupEnding(0)) {
    	%></table><%
    }
}
if(terminalResults.getRow() <= 1) { 
	%><span class="no-terminals">No devices found</span><%
} %></div>