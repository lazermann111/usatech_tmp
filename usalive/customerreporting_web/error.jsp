<%@page import="simple.bean.ConvertUtils"%>
<%@page import="simple.text.StringUtils"%>
<%@page isErrorPage="true"%><%
if(exception == null) 
	exception = (Throwable)request.getAttribute("simple.servlet.SimpleAction.exception");
if(exception == null)
	exception = new Throwable("Stack Trace");
simple.io.Log.getLog("Error Page").warn("",exception);
//response.setStatus(response.SC_INTERNAL_SERVER_ERROR);
response.setHeader("Success", "false");
Object displayErrorMessage = request.getAttribute("displayErrorMessage");
if(displayErrorMessage == null)
	displayErrorMessage = "An error occurred on the server. We are working to resolve this. Please try again later.";
%><p style="font-family: Verdana; font-size: 10px; color: red"><b>Application Error:</b> <%=StringUtils.prepareHTML(ConvertUtils.getStringSafely(displayErrorMessage,"")) %></p>