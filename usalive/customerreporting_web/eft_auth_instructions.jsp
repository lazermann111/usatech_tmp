<%@page import="simple.servlet.RequestUtils"%>
<%long bankAcctId = RequestUtils.getAttribute(request, "bankAcctId", Long.class, true);%>
<span class="caption">EFT Auth Form</span>
<div class="spacer5"></div>
<table class="fillVerticalSpace">
<tr><td>
<div class="eftAuthInstructions">
Please print the Authorization
for Electronic Funds Transfer Form below, fill in any missing information, sign and date the form, and fax
the form back to USA Technologies at (610) 989-9695. You must sign and fax back to USA Technologies this form before 
payments can be made to this Bank Account for Device transactions. If you have any questions do not hesitate to call 
USA Technologies at (888) 561-4748. Thank you.
</div></td></tr>
<tr class="fillVerticalSpace"><td class="fillVerticalSpace">
<iframe class="fillEFTAuth" name="eftAuthFormFrame" src="eft_auth.i?unframed=true&amp;bankAcctId=<%=bankAcctId%>"></iframe>
</td></tr>
</table>


    