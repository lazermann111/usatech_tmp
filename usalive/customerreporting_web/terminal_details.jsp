<%@page import="com.usatech.layers.common.util.WebHelper"%>
<%@page import="simple.servlet.NotAuthorizedException"%>
<%@page import="com.usatech.report.ReportingUser.ActionType"%>
<%@page import="com.usatech.report.ReportingUser.ResourceType"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.util.Set"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="java.util.HashMap"%>
<%@page import="simple.db.DataLayerMgr"%>
<%@page import="simple.results.Results"%>
<%@page import="java.util.Map"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser,com.usatech.report.custom.GenerateUtils,simple.servlet.RequestUtils,simple.text.StringUtils"%>
<jsp:useBean id="machineResults" type="simple.results.Results" scope="request" />
<jsp:useBean id="custBankResults" type="simple.results.Results" scope="request" />
<jsp:useBean id="locationTypeResults" type="simple.results.Results" scope="request" />
<jsp:useBean id="productTypeResults" type="simple.results.Results" scope="request" />
<jsp:useBean id="contactResults" type="simple.results.Results" scope="request" />
<jsp:useBean id="timeZoneResults" type="simple.results.Results" scope="request" />
<jsp:useBean id="paymentScheduleResults" type="simple.results.Results" scope="request" />
<jsp:useBean id="businessTypeResults" type="simple.results.Results" scope="request" />
<jsp:useBean id="regionResults" type="simple.results.Results" scope="request" />
<jsp:useBean id="results" type="simple.results.Results" scope="request" />
<jsp:useBean id="customResults" type="simple.results.Results" scope="request" />
<jsp:useBean id="customComments" type="simple.results.Results" scope="request" />
<jsp:useBean id="customerLicenseResults" type="simple.results.Results" scope="request" />
<jsp:useBean id="turnOffInactivityDaysYNresults" type="simple.results.Results" scope="request" />
<jsp:useBean id="deviceLocation" type="com.usatech.usalive.device.dto.DeviceLocation" scope="request" />
<jsp:useBean id="googleMapsApiKey" type="java.lang.String" scope="request" />
<jsp:useBean id="locationImgMapUrl" type="java.lang.String" scope="request" />
<div><%
long terminalId = RequestUtils.getAttribute(request, "terminalId", Long.class, true);
if(!results.next()) 
	throw new javax.servlet.ServletException("Terminal was not found");
String eportSerialNums=StringUtils.encodeForHTMLAttribute(results.getFormattedValue("eportSerialNums"));
simple.text.TimeZoneFormat tzf = new simple.text.TimeZoneFormat();
%><%/* we are going to hard code this for now (BSK 09-24-04)*/
String deviceName = "Device";
boolean extra = true;
boolean isKiosk = false;
int deviceTypeId = results.getValue("deviceTypeId", int.class);
int deviceSubTypeId = results.getValue("deviceSubTypeId", int.class);
long deviceId = results.getValue("deviceId", long.class);
if(deviceTypeId == 6) { /* MEI */
	deviceName = "MEI System";
	extra = false;
} else if (deviceTypeId == 11 && deviceSubTypeId == 3) {
	isKiosk = true;
}
String licenseType = null;
if (customerLicenseResults != null && customerLicenseResults.next()) {
	licenseType = customerLicenseResults.getFormattedValue("licenseType");
}
UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
boolean allowEdit = (!user.isReadOnly() && "Y".equals(results.getValue("allowEdit", String.class)));
if(allowEdit) {%>
<script type="text/javascript" defer="defer">
//<![CDATA[
var machineDialog;
window.addEvent("domready", function() {
	new Form.Validator.Inline.Mask(document.terminalForm); 
	new Form.Validator.Inline.Mask(document.machineForm); 
	machineDialog = new Dialog({content: $(document.machineForm), destroyOnClose: false});
});
           
function newMachine() {
	machineDialog.show();
}

function cancelMachine() {
	machineDialog.hide();
}

function saveMachine() {
  	var form = $(document.machineForm);
    if(!form.validate())
    	return;
    var terminalForm = $(document.terminalForm);
    terminalForm.make.value = form.make.value;
    terminalForm.model.value = form.model.value;
    var op = terminalForm.machineId.options[terminalForm.machineId.length-1];
    if(op.value != 0) {
    	op = new Element("option", {value: "0"});
    	terminalForm.machineId.options.add(op);
    }
    op.set("text",form.make.value + ' - - ' + form.model.value);
    terminalForm.machineId.selectedIndex = terminalForm.machineId.options.length - 1;
    machineDialog.hide();
}
function handleSpecifyChanged(el, enable) {
	var el = $(el);
	if(el) {
	    el.disabled=!enable;
		if(!enable)
			App.revalidate(el);
	}
}
function sendAjaxGeneratePasscode(){
		new Request.HTML({ 
	    	url: "generate_passcode.i",
	    	async: true,
	    	data: {terminalId:'<%=terminalId%>', fragment:true}, 
			update: $("passcodeMessage")
		}).send();
		window.scrollTo(0, 0);
}
function handleMobileIndicatorChange(input) {
    input = $(input);
    var mobile = (input.checked == (input.value == 'Y'));
    var elements = input.form.terminalActivator;
    for(var i = 0; i < elements.length; i++) {
           elements[i].disabled = mobile;
           if(mobile)
        	   elements[i].checked = (elements[i].value == 'A');
           else if(elements[i].checked)
               handleActivatorChange(elements[i]);
    }
    $("terminalActivatorFieldset").disabled = mobile;
    var validator = input.form.get("validator");
    validator.resetField($("terminalActivatorFieldset"));  
    if(mobile) {
        setEntryListSignatureDisabled(false);   
    }
}
function handleActivatorChange(input) {
    input = $(input);
    var unattended = (input.checked == (input.value == 'C'));
    setEntryListSignatureDisabled(unattended);
}
function setEntryListSignatureDisabled(disabled) {
    var fieldset = $("entryListSignatureFieldset");
    fieldset.disabled = disabled;
    var elements =  fieldset.getElements("input[name=entryList]");
    for(var i = 0; i < elements.length; i++)
           elements[i].disabled = disabled;    
    var validator = fieldset.form.get("validator");
    validator.resetField(fieldset);    
}
function to2DigitStr(digit) {
    return digit > 9 ? ("" + digit) : ("0" + digit);
}
function setNetworkLocation(country, state, city, zip, geo, updated, updateQueued, updateResricted, errorCode) {
    var nlCountryEl = document.getElementById("nlCountry");
    var nlStateEl = document.getElementById("nlState");
    var nlCityEl = document.getElementById("nlCity");
    var nlZipEl = document.getElementById("nlZip");
    var nlGeoEl = document.getElementById("nlGeo");
    var nlUpdatedEl = document.getElementById("nlUpdated");
    var nlRefreshNetGeoDataButtonEl = document.getElementById("refreshNetGeoDataButton");
    var nlUpdateErrorEl = document.getElementById("updateError");

    nlCountryEl.innerHTML = country;
    nlStateEl.innerHTML = state;
    nlCityEl.innerHTML = city;
    nlZipEl.innerHTML = zip;
    nlGeoEl.innerHTML = geo;
    nlUpdatedEl.innerHTML = updated;
    if (updateQueued || updateResricted || errorCode != null) {
        nlRefreshNetGeoDataButtonEl.className = "hide";
    } else {
        nlRefreshNetGeoDataButtonEl.className = "";
    }
    if (errorCode != null || updateQueued) {
        nlUpdateErrorEl.className = "";
    } else {
        nlUpdateErrorEl.className = "hide";
    }
}

function setNetworkLocationAll(textForAllFields) {
    setNetworkLocation(textForAllFields, textForAllFields, textForAllFields, textForAllFields, textForAllFields, textForAllFields, true, false, null);
}

function updateStaticGooleMap(url) {
	var nlStaticGoogleMap = document.getElementById("staticGmapId");
	var nlGoogleMapLink = document.getElementById("googleMapLinkId");
	var isDisplayMap = url != null && url != "";
	nlGoogleMapLink.className = isDisplayMap ? "" : "hide";
	nlStaticGoogleMap.src =  isDisplayMap ?  url : "";
}

var getAjaxDeviceNetworkLocation = function() {
    setNetworkLocationAll("Loading..");
    new Request.JSON({
        url: "device_network_location.i",
        method: "post",
        async: true,
        data: {terminalId:'<%=terminalId%>', device_id: <%=deviceId%>},
        onSuccess: function(result) {
            var nlCountry = result.country ? result.country : "N/A";
            var nlState = result.state ? result.state : "N/A";
            var nlCity = result.city ? result.city : "N/A";
            var nlZip = result.zip ? result.zip : "N/A";
            var nlGeo = result.latitude && result.longitude ? (result.latitude + ", " + result.longitude) : "N/A";
            var nlUpdated = result.strUpdatedAt ? result.strUpdatedAt : "N/A";
            var nlUpdateQueued = result.requestedAt != null;
            var nlUpdateResricted = result.updateRestricted;
            var nlErrorCode = result.errorCode == "" ? null : result.errorCode;
			
            setNetworkLocation(nlCountry, nlState, nlCity, nlZip, nlGeo, nlUpdated, nlUpdateQueued, nlUpdateResricted, nlErrorCode);
            updateStaticGooleMap(result.gooleMapUrl);
            if (nlUpdateQueued) {
                setTimeout(getAjaxDeviceNetworkLocation, 30000);
            }
        },
        onFailure: function() {
            setNetworkLocationAll("N/A");
        }
    }).send();
    return true;
};

<%
	if (deviceLocation.getRequestedAt() != null) {
%>
	document.addEventListener("DOMContentLoaded", getAjaxDeviceNetworkLocation);
<% 
	}
%>

//]]>
</script><%}
String errorMessage = RequestUtils.getAttribute(request, "errorMessage", String.class, false);
String successMessage = RequestUtils.getAttribute(request, "successMessage", String.class, false);
if((errorMessage != null && errorMessage.trim().length() > 0) || (successMessage != null && successMessage.trim().length() > 0)) {%>
<table class="messages">
  <tr>
    <td>
      <ul class="message-list"><%if(errorMessage != null && errorMessage.trim().length() > 0) {%>
        <li class="message-error"><%=StringUtils.prepareHTML(errorMessage)%></li><%} if(successMessage != null && successMessage.trim().length() > 0) {%>
        <li class="message-success"><%=StringUtils.prepareHTML(successMessage)%></li><%}%>
      </ul>
    </td>
  </tr>
</table>
<%} %>
<div id="passcodeMessage" ></div>
<div id="editTerminalDetails">
<form name="terminalForm" action="update_terminal.i" method="post">
<input type="hidden" name="terminalId" value="<%=terminalId%>"/>
<input type="hidden" name="serial" value="<%=eportSerialNums%>"/>
<input type="hidden" name="session-token" value="<%=RequestUtils.getSessionToken(request)%>" />
<table>
  <tr>
    <td><table class="fields">
    <tr class="tableDataShade"><th colspan="2" class="terminalHdr">Terminal <%=StringUtils.prepareHTML(results.getFormattedValue("terminalNbr"))%>:</th></tr>
    <tr class="tableHeader"><th colspan="2"><%=deviceName%> Information</th></tr>
    <tr class="tableDataShade"><th>Device Serial Number</th><td><span class="data"><%=StringUtils.prepareHTML(results.getFormattedValue("eportSerialNums"))%></span></td>
    </tr>
    <tr class="tableDataShade"><th>Client</th><td style="vertical-align: middle;"><%if(allowEdit) { 
        String client = RequestUtils.getAttribute(request, "client", String.class, false);
        if(client == null)
        	client = results.getFormattedValue("client");
        %><input maxlength="50" type="text" name="client" id="client" value="<%=StringUtils.prepareCDATA(client)%>"/><%} else { %><%=StringUtils.prepareHTML(results.getFormattedValue("client"))%><%} %></td>
    </tr>
    <tr class="tableHeader"><th colspan="2">Credit Card Statement Information</th></tr>
    <tr class="tableDataShade"><th>Override Doing Business As</th><td style="vertical-align: middle;"><%if(allowEdit) { 
        String doingBusinessAs = RequestUtils.getAttribute(request, "doingBusinessAs", String.class, false);
        if(doingBusinessAs == null)
        	doingBusinessAs = results.getFormattedValue("doingBusinessAs");
        %> <%if(user.isInternal()||(user.getMasterUser()!=null&&user.getMasterUser().isInternal())){ %>
		<input  maxlength="21" type="text" name="doingBusinessAs" id="doingBusinessAs" value="<%=StringUtils.prepareCDATA(doingBusinessAs)%>"/>
		<%}else{ %>
        	<%=StringUtils.prepareHTML(results.getFormattedValue("doingBusinessAs"))%>
		<%}//close of isInternal
		}//close of allowEdit
        else { %><%=StringUtils.prepareHTML(results.getFormattedValue("doingBusinessAs"))%><%} %>, shown on credit card statements, overrides customer setup parameter</td></tr>
    <tr class="tableDataShade"><th>Override Customer Service Phone</th><td><%if(allowEdit) { 
        String customerServicePhone = RequestUtils.getAttribute(request, "customerServicePhone", String.class, false);
        if(customerServicePhone == null)
        	customerServicePhone = results.getFormattedValue("customerServicePhone");
        %><input size="50" maxlength="20"  type="text" name="customerServicePhone" edit-mask="Fixed.PhoneUs" data-validators="validate-phone" value="<%=StringUtils.prepareCDATA(customerServicePhone)%>"/><%} else { %><%=StringUtils.prepareHTML(results.getFormattedValue("customerServicePhone"))%><%} %></td></tr>
    <tr class="tableDataShade"><th>Override Customer Service Email</th><td><%if(allowEdit) { 
        String customerServiceEmail = RequestUtils.getAttribute(request, "customerServiceEmail", String.class, false);
        if(customerServiceEmail == null)
        	customerServiceEmail = results.getFormattedValue("customerServiceEmail");
        %><input size="50" maxlength="70" type="text" name="customerServiceEmail" data-validators="validate-email" value="<%=StringUtils.prepareCDATA(customerServiceEmail)%>"/><%} else { %><%=StringUtils.prepareHTML(results.getFormattedValue("customerServiceEmail"))%><%} %></td></tr>
    <tr class="tableSpacer"><td colspan="2"/></tr><tr class="tableHeader"><th colspan="2">Location Information</th></tr>
    <tr class="tableDataShade"><th>Region</th><td><%if(allowEdit) {%><select name="regionId"><%
    	Long regionId = ConvertUtils.convertSafely(Long.class, RequestUtils.getAttribute(request, "regionId", false), null);
        if(regionId == null)
          regionId = results.getValue("regionId", Long.class); %>
    	<option<%if(regionId == null) { %> selected="selected"<%} %> onselect="this.form.regionName.disabled=true"/><%
    	Long parentRegionId = null;
    	Long optionRegionId = null;
        while(regionResults.next()) {
        	optionRegionId = regionResults.getValue("regionId", Long.class);
        	Long newParentRegionId = regionResults.getValue("parentRegionId", Long.class);
        	if(parentRegionId != null && !ConvertUtils.areEqual(newParentRegionId, parentRegionId)) {
        		%></optgroup><%
        	}
            if(newParentRegionId != null && !ConvertUtils.areEqual(newParentRegionId, parentRegionId)) {
            	%><optgroup label="<%=StringUtils.prepareCDATA(regionResults.getFormattedValue("parentRegionName"))%>"><%
            }
            if(!ConvertUtils.areEqual(optionRegionId, parentRegionId) || (regionId != null && regionId.equals(optionRegionId))) {
        		  %><option value="<%=StringUtils.prepareCDATA(regionResults.getFormattedValue("regionId"))%>"<%if(regionId != null && regionId.equals(optionRegionId)) { %> selected="selected"<%} %> onselect="this.form.regionName.disabled=true"><%
                  %><%=StringUtils.prepareHTML(regionResults.getFormattedValue("regionName"))%></option><%     		
        	}
            parentRegionId = newParentRegionId;
        }
        if(parentRegionId != null) {
            %></optgroup><%
        } %>
        <option<%if(regionId != null && regionId == -1) { %> selected="selected"<%} %> value="-1" onselect="this.form.regionName.disabled=false;this.form.regionName.focus();">-- New Region --</option>
    </select>&#160; <input class="form-field" type="text" size="50" maxlength="50" name="regionName"<%if(regionId == null || regionId != -1) { %> disabled="disabled"<%} else { %> value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "regionName", String.class, false))%>"<%} %>/><%
    } else { 
    %><%=StringUtils.encodeForHTML(results.getFormattedValue("regionName"))%><%
    } %></td></tr>
    <tr class="tableDataShade"><th>Location Name<span class="required">*</span></th><td><%if(allowEdit) { 
        String location = RequestUtils.getAttribute(request, "location", String.class, false);
        if(location == null)
        	location = results.getFormattedValue("location");
        %><input size="50" maxlength="50" type="text" name="location" data-validators="required" value="<%=StringUtils.prepareCDATA(location)%>"/><%} else { %><%=StringUtils.prepareHTML(results.getFormattedValue("location"))%><%} %></td></tr>
    <tr class="tableDataShade"><th>Address<span class="required">*</span></th><td><%
    String postal = null;
    String country = null;
    String city = null;
    String state = null;
    if(allowEdit) {
    	postal = RequestUtils.getAttribute(request, "postal", String.class, false);
    	country = RequestUtils.getAttribute(request, "country", String.class, false);
    	city = RequestUtils.getAttribute(request, "city", String.class, false);
    	state = RequestUtils.getAttribute(request, "state", String.class, false);
    }    
    if(postal == null)
    	postal = results.getFormattedValue("postal");
    if(country == null)
        country = results.getFormattedValue("country");
    if(city == null)
        city = results.getFormattedValue("city");
    if(state == null)
        state = results.getFormattedValue("state");
    boolean postalVerified = false;
    if(!StringUtils.isBlank(postal) && !StringUtils.isBlank(country)) {
        Map<String,Object> params = new HashMap<String,Object>();
        params.put("postalCd", GenerateUtils.getPrimaryPostal(country, postal));
        params.put("countryCd", country);
        Results postalResults = DataLayerMgr.executeQuery("LOOKUP_POSTAL", params);
        if(postalResults.next()) {
            postalVerified = true;
            city = postalResults.getValue("city", String.class);
            state = postalResults.getValue("state", String.class); 
        }
    }
    if(allowEdit) {
    	String address1 = RequestUtils.getAttribute(request, "address1", String.class, false);
    	if(address1 == null)
    		address1 = results.getFormattedValue("address1");
    %>
    	<input size="50" maxlength="50" type="text" name="address1" data-validators="required" value="<%=StringUtils.prepareCDATA(address1)%>"/>
    	<%GenerateUtils.writePostalTableElement(pageContext, null, postal, city, state, country, postalVerified, GenerateUtils.getTerminalCountries()); %>
    <%} else { %>
    	<%=StringUtils.prepareHTML(results.getFormattedValue("address1"))%><br/>
        <%=StringUtils.encodeForHTML(city)%>,&#160;<%=StringUtils.encodeForHTML(state)%>&#160;<%=StringUtils.encodeForHTML(GenerateUtils.formatPostal(country, postal))%>&#160;<%=StringUtils.encodeForHTML(country)%>
    <%} %></td></tr>
    <tr class="tableDataShade"><th>Specific Location at this Address<%=isKiosk ? "" : "<span class=\"required\">*</span>"%></th><td><%if(allowEdit) { 
        String locationDetails = RequestUtils.getAttribute(request, "locationDetails", String.class, false);
        if(locationDetails == null)
        	locationDetails = results.getFormattedValue("locationDetails");
    %><input size="50" maxlength="50" type="text" name="locationDetails" <%=isKiosk ? "" : "data-validators=\"required\""%> value="<%=StringUtils.prepareCDATA(locationDetails)%>"/><%} else { %><%=StringUtils.prepareHTML(results.getFormattedValue("locationDetails"))%><%} %></td></tr>
    <tr class="tableDataShade"><th>Phone Number at this Location</th><td><%if(allowEdit) { 
        String telephone = RequestUtils.getAttribute(request, "telephone", String.class, false);
        if(telephone == null)
        	telephone = results.getFormattedValue("telephone");
    %><input size="50" maxlength="50" type="text" name="telephone" edit-mask="Fixed.PhoneUs" data-validators="validate-digits minLength:10" value="<%=StringUtils.prepareCDATA(telephone)%>"/><%} else { %><%=StringUtils.prepareHTML(results.getFormattedValue("telephone"))%><%} %></td></tr>
    <% if(extra){%><tr class="tableDataShade"><th>Location Type<span class="required">*</span></th><td><%if(allowEdit) { %><table><tr><td>
		<select name="locationTypeId" data-validators="required"><%
		Integer locationType = ConvertUtils.convertSafely(Integer.class, RequestUtils.getAttribute(request,"locationTypeId", false), null);
        if(locationType == null)
        	locationType = results.getValue("locationTypeId", Integer.class);
        boolean enableSpecify = false;
        boolean showSpecify = false;
        if(locationType == null) {
        %><option onselect="handleSpecifyChanged(this.form.locationTypeSpecify, false);"/><%
        }
        while(locationTypeResults.next()) {
        	int ltId = locationTypeResults.getValue("locationTypeId", Integer.class);
        	boolean specify = "Y".equalsIgnoreCase(locationTypeResults.getValue("specifyRequired", String.class));
            %><option value="<%=ltId%>"<%if(locationType != null && locationType == ltId) { enableSpecify = specify; %> selected="selected"<%} %>
            onselect="handleSpecifyChanged(this.form.locationTypeSpecify, <%=specify %>);"><%=StringUtils.prepareHTML(locationTypeResults.getFormattedValue("locationTypeName"))%>
            <%if(specify) { showSpecify = true; %> (specify)<%}%></option><%
        } %></select></td><td>
        <%if(showSpecify) { 
            String locationTypeSpecify = RequestUtils.getAttribute(request, "locationTypeSpecify", String.class, false);
            if(locationTypeSpecify == null)
            	locationTypeSpecify = results.getFormattedValue("locationTypeSpecify");
        %><input class="form-field" type="text" size="50" maxlength="50" name="locationTypeSpecify" data-validators="required"<%if(!enableSpecify) {%> disabled="disabled"<%}%> value="<%=StringUtils.prepareCDATA(locationTypeSpecify)%>"/><%} %></td></tr></table>
        <%} else { %><%=StringUtils.prepareHTML(results.getFormattedValue("locationTypeName") + (results.getValue("locationTypeSpecify") != null ? " - " + results.getFormattedValue("locationTypeSpecify") : ""))%><%} %></td></tr>
    <%}%><tr class="tableDataShade"><th>Location's Time Zone<span class="required">*</span></th><td><%if(allowEdit) { %><select name="timeZone" data-validators="required"><%
    	Integer timeZoneId = ConvertUtils.convertSafely(Integer.class, RequestUtils.getAttribute(request,"timeZone", false), null);
        if(timeZoneId == null)
            timeZoneId = results.getValue("timeZone", Integer.class);
        while(timeZoneResults.next()) {
        	int tzId = timeZoneResults.getValue("timeZoneId", Integer.class);
            %><option value="<%=tzId%>"<%if(timeZoneId != null && timeZoneId == tzId) { %> selected="selected"<%} %>>
            <%=StringUtils.prepareHTML(timeZoneResults.getFormattedValue("timeZoneName"))%>
            (<%=StringUtils.prepareHTML(tzf.format(timeZoneResults.getValue("timeZoneOffset")))%>)</option><%
        } %></select>
        <%} else { %><%=StringUtils.prepareHTML(results.getValue("timeZone") != null ? results.getFormattedValue("timeZoneName") + " (" + tzf.format(results.getValue("timeZoneOffset")) + ")" : "") %><%} %></td></tr>
<%customResults.setRow(Integer.MIN_VALUE);
  while(customResults.next()) { if(customResults.isGroupBeginning(0)) {%><tr class="tableSpacer"><td colspan="2"/></tr><tr class="tableHeader"><th colspan="2">Custom Field Information</th></tr><%}%>  
<%while(customComments.next()) {%>
  <tr class="tableDataShade"><td colspan="2" class="<%=StringUtils.prepareCDATA(customComments.getFormattedValue("commentClass"))%>">
  <%=StringUtils.prepareHTML(customComments.getFormattedValue("commentText"))%></td></tr><%}%> 
    <tr class="tableDataShade"> 
      <th><%=StringUtils.prepareHTML(customResults.getFormattedValue("fieldLabel"))%>
          <% String d = customResults.getFormattedValue("fieldRequired");
             if("Y".equalsIgnoreCase(d)) { %><span class="required">*</span><% } %></th>
      <td><%if(allowEdit) {
    	  String fieldName = customResults.getFormattedValue("fieldName");
    	  String fieldValue = RequestUtils.getAttribute(request, fieldName.toLowerCase(), String.class, false);
    	  if(fieldValue == null)
    		  fieldValue = results.getFormattedValue(fieldName);
      %><input size="50" maxlength="4000" type="text" name="<%=StringUtils.prepareCDATA(fieldName.toLowerCase())%>"<%if("Y".equalsIgnoreCase(d)) { %> data-validators="required"<%} %> value="<%=StringUtils.prepareCDATA(fieldValue)%>"/><%} else { %><%=StringUtils.prepareHTML(results.getFormattedValue(customResults.getFormattedValue("fieldName")))%><%} %></td>
    </tr><%}%>
        <tr class="tableHeader<%=deviceLocation.getDeviceId() == null ? " hide" : ""%>"><th colspan="2">Network Information</th></tr>
        <tr class="tableDataShade<%=deviceLocation.getDeviceId() == null ? " hide" : ""%>"><th>Country</th><td width="692"><span id="nlCountry">
        	<%=deviceLocation.getCountry() != null ? deviceLocation.getCountry() : "N/A" %>
        </span></td>
        <td rowspan="8">
            <a id="googleMapLinkId" class="<%=StringUtils.isBlank(locationImgMapUrl) ? "hide" : ""%>" href="/google_map.i?terminalId=<%=terminalId%>" target="_blank"><img id="staticGmapId" src="<%=locationImgMapUrl%>" style="position:relative;left:-450px;"/></a>
        </td>
        </tr>
        <tr class="tableDataShade<%=deviceLocation.getDeviceId() == null ? " hide" : ""%>"><th>State</th><td><span id="nlState">
        	<%=deviceLocation.getState() != null ? deviceLocation.getState() : "N/A" %>
        </span></td></tr>
        <tr class="tableDataShade<%=deviceLocation.getDeviceId() == null ? " hide" : ""%>"><th>City</th><td><span id="nlCity">
        	<%=deviceLocation.getCity() != null ? deviceLocation.getCity() : "N/A" %>
        </span></td></tr>
        <tr class="tableDataShade<%=deviceLocation.getDeviceId() == null ? " hide" : ""%>"><th>Zip</th><td><span id="nlZip">
        	<%=deviceLocation.getZip() != null ? deviceLocation.getZip() : "N/A" %>
        </span></td></tr>
        <tr class="tableDataShade<%=deviceLocation.getDeviceId() == null ? " hide" : ""%>"><th>Geo coordinates</th><td><span id="nlGeo">
        	<%=deviceLocation.getLatitude() != null ? deviceLocation.getLatitude() : "N/A" %>,
        	<%=deviceLocation.getLongitude() != null ? deviceLocation.getLongitude() : "N/A" %>
        </span></td></tr>
        <tr class="tableDataShade<%=deviceLocation.getDeviceId() == null ? " hide" : ""%>"><th>Update time</th><td><span id="nlUpdated">
        	<%=deviceLocation.getUpdatedAt() != null ? WebHelper.convertDateToLocalStr(deviceLocation.getUpdatedAt()) : "N/A" %>
        </span></td></tr>
        <tr class="tableDataShade<%=deviceLocation.getDeviceId() == null ? " hide" : ""%>" id="geoControlsRow">
            <th>&#0160;</th>
            <td>
                <div id="updateError" name="updateError"
                     class="<%=StringUtils.isBlank(deviceLocation.getErrorCode()) && deviceLocation.getRequestedAt() == null ? "hide" : "" %>">
                    <b>Request has been queued</b>
                </div>
                <input type="button" value="Refresh" id="refreshNetGeoDataButton"
                       class="<%=deviceLocation.getRequestedAt() == null &&
                                 !deviceLocation.isUpdateRestricted() &&
                                 StringUtils.isBlank(deviceLocation.getErrorCode()) ? "" : "hide" %>"
                       name="refreshNetGeoDataButton" onclick="getAjaxDeviceNetworkLocation();"/>
                <img src="images/spinner-rosetta.gif" id="refreshNetGeoDataSpinner" class="hide"/>
            </td>
        </tr>
    <tr class="tableSpacer"><td colspan="2"/></tr><tr class="tableHeader"><th colspan="2">Machine Information</th></tr>
    <tr class="tableDataShade"><th>Machine Asset Number<%=isKiosk ? "" : "<span class=\"required\">*</span>"%></th><td><%if(allowEdit) { 
    	String asset = RequestUtils.getAttribute(request, "asset", String.class, false);
        if(asset == null)
        	asset = results.getFormattedValue("asset");
    %><input size="50" maxlength="15" type="text" name="asset" <%=isKiosk ? "" : "data-validators=\"required\""%> value="<%=StringUtils.prepareCDATA(asset)%>"/><%} else { %><%=StringUtils.prepareHTML(results.getFormattedValue("asset"))%><%} %></td></tr>
    <tr class="tableDataShade"><th>Machine's Make and Model<span class="required">*</span></th>
        <td><%if(allowEdit) { %><table><tr><td><select name="machineId" data-validators="required"><%
        Integer machineId = ConvertUtils.convertSafely(Integer.class, RequestUtils.getAttribute(request,"machineId", false), null);
        if(machineId == null)
            machineId = results.getValue("machineId", Integer.class);
        if(machineId == null) {%>
        <option></option><%
        }
        while(machineResults.next()) {
          int mId = machineResults.getValue("machineId", Integer.class);
        %><option value="<%=mId%>"<%if(machineId != null && machineId == mId) { %> selected="selected"<%} %>>
        <%=StringUtils.prepareHTML(machineResults.getFormattedValue("machineMake")) %> - -
        <%=StringUtils.prepareHTML(machineResults.getFormattedValue("machineModel")) %>
        </option><%
        }%>
        </select>&#160;</td><td>
        <input type="button" value="New Make/Model" onclick="newMachine()" name="addMachineButton"/></td></tr></table><%
        } else { %><%=StringUtils.prepareHTML(results.getFormattedValue("make"))%><br/><%=StringUtils.prepareHTML(results.getFormattedValue("model"))%><%} %></td></tr>
    <tr class="tableDataShade"><th>Product Type<span class="required">*</span></th><td>
        <%if(allowEdit) { %><table><tr><td><select name="productTypeId" data-validators="required"><%
        Integer productType = ConvertUtils.convertSafely(Integer.class, RequestUtils.getAttribute(request,"productTypeId", false), null);
        if(productType == null)
            productType = results.getValue("productTypeId", Integer.class);
        boolean enableSpecify = false;
        boolean showSpecify = false;
        if(productType == null) {
        %><option onselect="handleSpecifyChanged(this.form.productTypeSpecify, false);"/><%
        }
        while(productTypeResults.next()) {
        	int ptId = productTypeResults.getValue("productTypeId", Integer.class);
        	boolean specify = "Y".equalsIgnoreCase(productTypeResults.getValue("specifyRequired", String.class));
            %><option value="<%=ptId%>"<%if(productType != null && productType == ptId) { enableSpecify = specify; %> selected="selected"<%} %>
            onselect="handleSpecifyChanged(this.form.productTypeSpecify, <%=specify %>);">
            <%=StringUtils.prepareHTML(productTypeResults.getFormattedValue("productTypeName"))%>
            <%if(specify) { showSpecify = true; %> (specify)<%}%></option><%
        } %></select>&#160;</td><td>
        <%if(showSpecify) {
        	String productTypeSpecify = RequestUtils.getAttribute(request, "productTypeSpecify", String.class, false);
        	if(productTypeSpecify == null)
        		productTypeSpecify = results.getFormattedValue("productTypeSpecify");
        	%><input class="form-field" type="text" size="50" maxlength="50" name="productTypeSpecify" data-validators="required"<%if(!enableSpecify) {%> disabled="disabled"<%}%> value="<%=StringUtils.prepareCDATA(productTypeSpecify)%>"/><%} %></td></tr></table>
        <%} else { %><%=StringUtils.prepareHTML(results.getFormattedValue("productTypeName") + (results.getValue("productTypeSpecify") != null ? " - " + results.getFormattedValue("productTypeSpecify") : ""))%><%} %></td></tr>
    <%if(isKiosk) { 
    	boolean posTypeEditable = results.getValue("posTypeEditable", Boolean.class);
        String posEnvironment = results.getValue("posEnvironment", String.class);
        if(StringUtils.isBlank(posEnvironment))
        	posEnvironment =  "?";
        String entryCapability = results.getValue("entryCapability", String.class);
        Boolean pinCapability = results.getValue("pinCapability", Boolean.class);
        Character terminalActivator;
        Character mobileIndicator;
        switch(posEnvironment.charAt(0)) {
        	case 'E':
        	case 'U':
            	mobileIndicator = 'N';
                terminalActivator = 'C';
                break;
            case 'C':
                mobileIndicator = 'Y';
                terminalActivator = 'A';
                break;
            case 'M':
            case 'A':
            	mobileIndicator = 'N';
                terminalActivator = 'A';
                break;
            default:
            	mobileIndicator = '?';
                terminalActivator = '?';
        }
        Set<Character> entryList;
        if(posTypeEditable) {
	        terminalActivator = ConvertUtils.convertSafely(Character.class, RequestUtils.getAttribute(request,"terminalActivator", false), terminalActivator);
	        mobileIndicator = ConvertUtils.convertSafely(Character.class, RequestUtils.getAttribute(request,"mobileIndicator", false), mobileIndicator);
	        entryList = ConvertUtils.convert(Set.class,  Character.class, RequestUtils.getAttribute(request,"entryList", false));   
        } else
        	entryList = null;
        if(entryList == null) {
        	entryList = new HashSet<Character>();
        	if(!StringUtils.isBlank(entryCapability)) {
        		for(int i = 0; i < entryCapability.length(); i++)
        		    entryList.add(entryCapability.charAt(i));
        	}
        	if(pinCapability != null && pinCapability.booleanValue())
        		entryList.add('M');
        }
        %>
    <tr class="tableDataShade"><th>Machine is Tablet / Phone<span class="required">*</span></th><td><%if(allowEdit && posTypeEditable) { %><fieldset class="no-border-grouper" data-validators="validate-reqchk-bynode">
        <label><input type="radio" name="mobileIndicator" value="Y"<%if(mobileIndicator == 'Y') {%> checked="checked"<%} %> onclick="handleMobileIndicatorChange(this)"/>Yes</label>
        <label><input type="radio" name="mobileIndicator" value="N"<%if(mobileIndicator == 'N') {%> checked="checked"<%} %> onclick="handleMobileIndicatorChange(this)"/>No</label>
        </fieldset><%
    } else {
        switch(mobileIndicator) {
            case 'Y':%>Yes<%break;
            case 'N':%>No<%break;
            default:%>Unknown<%
        }
    }%></td></tr>
    <tr class="tableDataShade"><th>Card Data Entered By<span class="required">*</span></th><td><%if(allowEdit && posTypeEditable) { %><fieldset class="no-border-grouper" data-validators="validate-reqchk-bynode" id="terminalActivatorFieldset"<%if(mobileIndicator == 'Y') {%> disabled="disabled"<%} %>>
        <label><input type="radio" name="terminalActivator" value="C"<%if(terminalActivator == 'C') {%> checked="checked"<%} %><%if(mobileIndicator == 'Y') {%> disabled="disabled"<%} %> onclick="handleActivatorChange(this)"/>Card Holder</label>
        <label><input type="radio" name="terminalActivator" value="A"<%if(terminalActivator == 'A') {%> checked="checked"<%} %><%if(mobileIndicator == 'Y') {%> disabled="disabled"<%} %> onclick="handleActivatorChange(this)"/>Attendant (Cashier)</label>
        </fieldset><%
    } else {
        switch(terminalActivator) {
        	case 'C':%>Card Holder<%break;
        	case 'A':%>Attendant (Cashier)<%break;
        	default:%>Unknown<%
        }
    }%></td></tr>
    <tr class="tableDataShade"><th>Card Entry Types <span class="note2"> &#8225;</span></th><td><%
    for(Character entry : entryList) {
    	if(entry != null && entry != 'X' && entry >= 'A' && entry <= 'Z') {
    		%><input type="hidden" name="entryList" value="<%=entry%>"/><%
    	}
    }
   	boolean first = true;
   	if(entryList.contains('M')) { 
   		first = false;%>Keypad or Keyboard<%
   	}
   	if(entryList.contains('S')) { 
   		if(first)
   			first = false;
   	    else {%>, <%}    	    
   	    %>Card Swipe Reader<%
   	}
   	if(entryList.contains('P')) { 
           if(first)
               first = false;
           else {%>, <%}           
           %>Proximity Reader (Contactless)<%
       }
   	if(entryList.contains('E')) { 
           if(first)
               first = false;
           else {%>, <%}           
           %>EMV<%
       }
    %></td></tr>
    <tr class="tableDataShade"><th>Signature Capture<span class="required">*</span></th><td><%if(allowEdit && posTypeEditable) { %><fieldset class="no-border-grouper" data-validators="validate-reqchk-bynode"<%if(mobileIndicator == 'N' && terminalActivator != 'A') {%> disabled="disabled"<%} %> id="entryListSignatureFieldset">
        <label><input type="radio" name="entryList" value="X"<%if(entryList.contains('X')) {%> checked="checked"<%} %>/>Electronic</label>
        <label><input type="radio" name="entryList" value=""<%if(!entryList.contains('X')) {%> checked="checked"<%} %>/>Manual</label>
        </fieldset><%
    } else {
    	if(entryList.contains('X')) {%>Yes<%} else {%>No<%}
    }%></td></tr><%
    }
    if(false) {%>
    <tr class="tableSpacer"><td colspan="2"/></tr><tr class="tableHeader"><th colspan="2">Transaction Settings</th></tr>
    <tr class="tableDataShade"><th>Items Per Transaction Mode<span class="required">*</span></th><td>
        <%if(allowEdit) { %>
        <select name="vends" data-validators="required"><%
        Integer vends = ConvertUtils.convertSafely(Integer.class, RequestUtils.getAttribute(request,"vends", false), null);
        if(vends == null)
            vends = results.getValue("vends", Integer.class);
        if(vends == null || vends < 1) {%>
        <option></option><%} %>
        <option value="1"<%if(vends != null && vends == 1) {%> selected="selected"<%} %>>Single Item</option>
        <option value="10"<%if(vends != null && vends > 1) {%> selected="selected"<%} %>>Multiple Items</option></select>
       	<%} else { %><%
        Integer vends = results.getValue("vends", Integer.class);
        if(vends != null && vends.intValue() > 1) {
            %>Multiple Items<%
        } else {
            %>Single Item<%
        }%><%} %></td></tr>
    <tr class="tableDataShade"><th>Credit Card Authorization<span class="required">*</span></th><td>
        <%if(allowEdit) { %>
        <select name="authMode" data-validators="required"><%
        Character authMode = ConvertUtils.convertSafely(Character.class, RequestUtils.getAttribute(request,"authMode", false), null);
        if(authMode == null)
            authMode = results.getValue("authMode", Character.class);
        if(authMode == null) {%>
        <option></option><%} %>
        <option value="N"<%if(authMode != null && authMode == 'N') {%> selected="selected"<%} %>>Credit Cards Not Accepted</option>
        <option value="L"<%if(authMode != null && authMode == 'L') {%> selected="selected"<%} %>>Local Authorization</option>
        <option value="F"<%if(authMode != null && authMode == 'F') {%> selected="selected"<%} %>>Full Authorization</option></select>
        <%} else { %><%
        String s = results.getFormattedValue("authMode");
        if("N".equalsIgnoreCase(s)) {
            %>Credit Cards Not Accepted<%
        } else if("L".equalsIgnoreCase(s)) {
            %>Local Authorization<%
        } else if("F".equalsIgnoreCase(s)) {
            %>Full Authorization<%
        }%><%} %></td></tr>
    <%if(extra) {%><tr class="tableDataShade"><th>Print a receipt?<span class="required">*</span></th><td>
        <%if(allowEdit) { %>
        <select name="receipt" data-validators="required"><%
        Character receipt = ConvertUtils.convertSafely(Character.class, RequestUtils.getAttribute(request,"receipt", false), null);
        if(receipt == null)
            receipt = results.getValue("receipt", Character.class);
        if(receipt == null) {%>
        <option></option><%} %>
        <option value="Y"<%if(receipt != null && receipt == 'Y') {%> selected="selected"<%} %>>Yes</option>
        <option value="N"<%if(receipt != null && receipt == 'N') {%> selected="selected"<%} %>>No</option>
        </select>
        <%} else { %><%
        if("Y".equalsIgnoreCase(results.getFormattedValue("receipt"))) {
            %>Yes<%
        } else {
            %>No<%
        }%><%} %></td></tr>
    <tr class="tableDataShade"><th>DEX Data Capture<span class="required">*</span></th><td><%
    if(allowEdit) { 
    	Character dexData = ConvertUtils.convertSafely(Character.class, RequestUtils.getAttribute(request,"dexData", false), null);
        if(dexData == null)
            dexData = results.getValue("dexData", Character.class); 
         %><fieldset class="no-border-grouper" data-validators="validate-reqchk-bynode">
        <label class="radioLabel"><input type="radio" name="dexData" value="N"<%if(dexData != null && dexData == 'N') {%> checked="checked"<%} %>/><b>None</b></label><br/>
        <label class="radioLabel"><input type="radio" name="dexData" value="D"<%if(dexData != null && dexData == 'D') {%> checked="checked"<%} %>/><b>Complete DEX File</b> ($4.00 per month) - includes download of complete DEX File for importing and web notification of alarms on delayed basis</label><br/><%
        /*%>
        <label class="radioLabel"><input type="radio" name="dexData" value="A"<%if(dexData != null && dexData == 'A') {%> checked="checked"<%} %>/><b>Alarm Notification + Complete DEX File</b> ($0.40 per alarm + $4.00 per month) - includes immediate email notification of each alarm, in addition to all benefits described above</label><br/><%
        */%></fieldset>
        <%} else { %><%
        String s = results.getFormattedValue("dexData");
        if("N".equalsIgnoreCase(s)) {
            %>None<%
        } else if("D".equalsIgnoreCase(s)) {
            %>Complete DEX File<%
        } else if("A".equalsIgnoreCase(s)) {
            %>Alarm Notification + Complete DEX File<%
        }%><%} %></td></tr>
    <%}
    }%><tr class="tableSpacer"><td colspan="2"/></tr><tr class="tableHeader"><th colspan="2">Payment Information</th></tr>
    <tr class="tableDataShade"><th>Owner</th><td><%=StringUtils.prepareHTML(results.getFormattedValue("customerName"))%></td></tr>
    <tr class="tableDataShade"><th>Bank Account to pay<span class="baNote">*</span></th><td>
    <%if(allowEdit) { %>
    <select name="custBankId"><%
    	Long bankAcctId = ConvertUtils.convertSafely(Long.class, RequestUtils.getAttribute(request,"custBankId", false), null);
        if(bankAcctId == null)
            bankAcctId = results.getValue("custBankId", Long.class);	
    	while(custBankResults.next()) {
    		long baId = custBankResults.getValue("custBankId", Long.class);
            %><option value="<%=baId%>"<%if(bankAcctId != null && baId == bankAcctId) {%> selected="selected"<%} %>>
            <%=user.isPartner() ?  StringUtils.prepareHTML(custBankResults.getFormattedValue("custName")) + " - " : ""%>
            <%=StringUtils.prepareHTML(custBankResults.getFormattedValue("bankName"))%>
            - #<%=StringUtils.encodeForHTML(custBankResults.getFormattedValue("bankAcctNum"))%>
            - <%=StringUtils.prepareHTML(custBankResults.getFormattedValue("accountTitle"))%>
              <%=custBankResults.getFormattedValue("status").equalsIgnoreCase("P") ? " (PENDING APPROVAL)" : "" %></option><%
        }
    	if(bankAcctId == null) {
        	%><option value="" selected="selected">-- None --</option><%
        }%></select>
    <%} else { %><%=StringUtils.encodeForHTML(results.getValue("custBankId") != null ? results.getFormattedValue("bankName") + " - #" + results.getFormattedValue("bankAcctNbr") + " - " + results.getFormattedValue("accountTitle") : "")%><%} %></td></tr>
    <tr class="tableDataShade"><th>Payment Schedule<span class="required">*</span></th><td>
    <%if(allowEdit) { %><select name="paymentScheduleId" data-validators="required"><%
        Integer paymentScheduleId = ConvertUtils.convertSafely(Integer.class, RequestUtils.getAttribute(request,"paymentScheduleId", false), null);
        if(paymentScheduleId == null)
            paymentScheduleId = results.getValue("paymentScheduleId", Integer.class);
        if(paymentScheduleId == null) {%>
        <option></option><%
        }
        while(paymentScheduleResults.next()) {
            int psId = paymentScheduleResults.getValue("paymentScheduleId", Integer.class);
        %><option value="<%=psId%>"<%if(paymentScheduleId != null && paymentScheduleId == psId) { %> selected="selected"<%} %>>
        <%=StringUtils.prepareHTML(paymentScheduleResults.getFormattedValue("paymentScheduleName")) %></option><%
        }%>
        </select>
        <span class="help-button"><a onclick="paySchedInstructsDialog.show();">&#160;</a></span>
        <script type="text/javascript">var paySchedInstructsDialog = new Dialog.Help({ helpKey: 'payment-schedule-instructions', destroyOnClose: false, height: 300 });</script>
        <%} else { %><%=StringUtils.prepareHTML(results.getFormattedValue("paymentScheduleName")) %><%} %></td></tr>
    <tr class="tableDataShade"><th>Business Type<span class="required">*</span></th><td> 
    <%if(user.isInternal()||(user.getMasterUser()!=null&&user.getMasterUser().isInternal())){ %>
    <%if(allowEdit) { %><select name="businessTypeId" data-validators="required"><%
        Integer businessTypeId = ConvertUtils.convertSafely(Integer.class, RequestUtils.getAttribute(request,"businessTypeId", false), null);
        if(businessTypeId == null)
        	businessTypeId = results.getValue("businessTypeId", Integer.class);
        if(businessTypeId == null) {%>
        <option></option><%
        }
        while(businessTypeResults.next()) {
            int btId = businessTypeResults.getValue("businessTypeId", Integer.class);
        %><option value="<%=btId%>"<%if(businessTypeId != null && businessTypeId == btId) { %> selected="selected"<%} %>>
        <%=StringUtils.prepareHTML(businessTypeResults.getFormattedValue("businessTypeName")) %></option><%
        }%>
        </select>
        <%} else { %><%=StringUtils.prepareHTML(results.getFormattedValue("businessTypeName")) %><%} %>
        <%}//end of check internal user
	else{ %>
		<%=StringUtils.prepareHTML(results.getFormattedValue("businessTypeName")) %>
	<%}%>
        </td></tr>    

	<%	boolean bQSFeeInactivityDaysShow = true;
		if (bQSFeeInactivityDaysShow) { %>
	<%	if (turnOffInactivityDaysYNresults.next() 
			&& turnOffInactivityDaysYNresults.getValue("turnOffInactivityDaysYN", String.class).equals("Y")) {
			Integer inactivityDays = results.getValue("eportQSFeeInactivityDays", Integer.class);
			String inactivityDaysStr = (inactivityDays == null ? "" : inactivityDays.toString());
	%>
	<tr class="tableDataShade"><th>Inactivity Days to Turn Off ePort Connect Fees</th>
		<td style="vertical-align: middle;">
		<%	if (allowEdit) {%>
			<input maxlength="50" type="text" name="qsFeeInactivityDays" id="qsFeeInactivityDays" value="<%=inactivityDaysStr %>" />
		<%	} else { %>
			<%=inactivityDaysStr %>
		<%	} %>
		</td>
	</tr>
	<%	} %>
	<%	} %>

    <%if(extra){%><tr class="tableSpacer"><td colspan="2"/></tr><tr class="tableHeader"><th colspan="2">Contact Information</th></tr>
    <tr class="tableDataShade"><th>Primary Contact<span class="required">*</span></th><td>
    <%if(allowEdit) { %>
    <select name="primaryContactId" data-validators="required"><%
	    Integer primaryContactId = ConvertUtils.convertSafely(Integer.class, RequestUtils.getAttribute(request,"primaryContactId", false), null);
        if(primaryContactId == null)
          primaryContactId = results.getValue("primaryContactId", Integer.class);
          while(contactResults.next()) {
          int userId = contactResults.getValue("userId", Integer.class);
            %><option value="<%=userId%>"<%if(primaryContactId != null && userId == primaryContactId) {%> selected="selected"<%} %>>
              <%=StringUtils.prepareHTML(contactResults.getFormattedValue("firstName"))%> 
              <%=StringUtils.prepareHTML(contactResults.getFormattedValue("lastName"))%>
            </option><%
        } %></select>
    <%} else { %><%=StringUtils.prepareHTML(results.getFormattedValue("primaryContactFirstName"))%> <%=StringUtils.prepareHTML(results.getFormattedValue("primaryContactLastName"))%><%} %></td></tr>
    <tr class="tableDataShade"><th>Secondary Contact</th><td>
    <%if(allowEdit) { %>
    <select name="secondaryContactId"><%
    	Integer secondaryContactId = ConvertUtils.convertSafely(Integer.class, RequestUtils.getAttribute(request,"secondaryContactId", false), null);
        if(secondaryContactId == null)
            secondaryContactId = results.getValue("secondaryContactId", Integer.class);
      contactResults.setRow(0);
      while(contactResults.next()) {
        	int userId = contactResults.getValue("userId", Integer.class);
            %><option value="<%=userId%>"<%if(secondaryContactId != null && userId == secondaryContactId) {%> selected="selected"<%} %>>
            	<%=StringUtils.prepareHTML(contactResults.getFormattedValue("firstName"))%> 
            	<%=StringUtils.prepareHTML(contactResults.getFormattedValue("lastName"))%>
            </option><%
        } %><option<%if(secondaryContactId == null) {%> selected="selected"<%} %>/></select>
    <%} else { %><%=StringUtils.prepareHTML(results.getFormattedValue("secondaryContactFirstName"))%> <%=StringUtils.prepareHTML(results.getFormattedValue("secondaryContactLastName"))%><%} %></td></tr><%
	}%>
<%if (deviceId > 0 && isKiosk) {%>
<tr class="tableSpacer"><td colspan="2"/></tr><tr class="tableHeader"><th colspan="2">Quick Connect Information</th></tr>
<tr class="tableDataShade"><th>Username</th><td><%=StringUtils.encodeForHTML(results.getFormattedValue("quick_connect_username"))%></td></tr>
<tr class="tableDataShade"><th>Password Generated</th><td><%=StringUtils.prepareHTML(results.getFormattedValue("last_password_generated_ts"))%></td></tr>
<tr class="tableDataShade"><th>Password Updated</th><td><%=StringUtils.prepareHTML(results.getFormattedValue("last_password_updated_ts"))%></td></tr>
<%}%>
</table></td></tr>
<tr><td><div class="spacer5"></div></td></tr>
<tr><td><span class="required">* Required Fields</span></td></tr>
<tr><td><span class="baNote">* If pending approval, no payments will be made for revenue generated by this device until the bank account is approved.</span></td></tr>
<tr><td><span class="note2">&#8225; Please contact Customer Service to change this</span></td></tr>
<tr><td><div class="spacer5"></div></td></tr>
<tr><td align="center">
<%if(allowEdit) { %>
<input type="submit" value="Save Changes" name="submitButton"/>
<%}%>
<% boolean canConfigDevice = false;
   try { 
	user.checkPermission(ResourceType.DEVICE_CONFIG, ActionType.EDIT, terminalId);
	canConfigDevice = true;
   } catch (NotAuthorizedException e) { }
   if(canConfigDevice && deviceId > 0 && (deviceTypeId == 0 || deviceTypeId == 1 || deviceTypeId == 6 || deviceTypeId == 13||WebHelper.isCraneDevice(eportSerialNums))) {%>
	<input type="button" value="Device Configuration" onclick="window.location = 'device_configuration.html?terminalId=<%=terminalId%>'"/>
<%}%>
<%if(allowEdit && eportSerialNums.startsWith("K3")) { %>
	<input type="button" value="Generate Passcode" name="generatePasscodeButton" onClick="sendAjaxGeneratePasscode();"/>
  <% if ("C".equals(licenseType) && (user.isCustomerService() || user.isReseller())) {%>
  <input type="button" value="Terminal Fees" onclick="window.location = 'terminal_fees.html?terminalId=<%=terminalId%>'"/>
  <% } %>
<% } %>
&#160;</td></tr>
</table>
<input type="hidden" name="make"/>
<input type="hidden" name="model"/>
<input type="hidden" name="addressId" value="<%=StringUtils.encodeForHTMLAttribute(results.getFormattedValue("addressId"))%>"/>
</form></div>
<div id="machineDetails" style="display: none;">
<form class="popupForm" name="machineForm" action="">
<table>
<tr class="tableDataShade"><th class="terminalHdr" colspan="2" align="left">Enter the Make and Model and click submit:</th></tr>
<tr class="tableDataShade"><td>Make</td><td><input type="text" size="40" maxlength="20" name="make" data-validators="required"/></td></tr>
<tr class="tableDataShade"><td>Model</td><td><input type="text" size="40" maxlength="20" name="model" data-validators="required"/></td></tr>
<tr class="center"><td colspan="2"><input type="button" value="Save Changes" onclick="saveMachine()"/><input type="button" value="Cancel" onclick="cancelMachine();"/>
</td></tr>
</table>
</form></div>
</div>