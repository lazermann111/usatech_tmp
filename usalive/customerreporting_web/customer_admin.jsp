<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="simple.text.StringUtils,simple.servlet.RequestUtils,simple.results.Results"%>
<%@page import="simple.servlet.ResponseUtils"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="simple.servlet.SimpleServlet"%>
<%
	Results customerResults = RequestUtils.getAttribute(request, "customerResults", Results.class, false);
    UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request);
    boolean updatable = user.hasPrivilege(ReportingPrivilege.PRIV_LOGIN_AS_CUST);
%>
<div>
	<div class="toggle-heading">
		<span class="caption">Customers</span>
	</div>
<form name="searchForm"	action="customer_admin_search.i" method="post" id="searchCustomerForm" onSubmit="searchCustomerName();">
<div class="message-header">Customer Name:<span title="Search customer name that contains: (input nothing for all customers)"><input name="customerName" id="customerName" type="text"/></span>
<input type="submit" value="Search" />
</div>
</form>
<form name="customerForm" action="be_user.i" method="post" id="customerAdminForm">
<%
InputForm inputForm = (InputForm)request.getAttribute(SimpleServlet.ATTRIBUTE_FORM);
ResponseUtils.writeXsfrFormProtection(pageContext);
%>
<script type="text/javascript" defer="defer">
	var selectedCustomerId;
	var selectedFirstName;
	var selectedLastName;<% if(updatable) {%>
	function addBVReporting(){
		new Request.HTML(
	   			 {url: "add_best_vendor_reporting.i", 
	   				 async: true,
	   				 data: {customerId: selectedCustomerId, fragment:true}, 
	   				 onSuccess: function(responseTree, responseElements, responseHTML, responseJavaScript){
	   					 if(responseHTML.contains('status-info-success')){
	   						$('removeBestVendorReporting').disabled=false;
		   		        	$('addBestVendorReporting').disabled=true;
		   		        	$('editUserId').getSelected().set('onselect', "setCustomer(1,"+selectedCustomerId+","+"'"+$("customer_name").getText()+"',"+"'"+$("user_name").getText()+"',"+"'"+selectedFirstName+"',"+"'"+selectedLastName+"',"+"'"+$("email").getText()+"');");
	   					 }
	   					$('message').set("html", responseHTML);
	   				 }}).send();
	}
	function removeBVReporting(){
		new Request.HTML(
				 {url: "remove_best_vendor_reporting.i", 
					 async: true,
					 data: {customerId: selectedCustomerId, fragment:true}, 
					 onSuccess: function(responseTree, responseElements, responseHTML, responseJavaScript){
						 if(responseHTML.contains('status-info-success')){
							 $('addBestVendorReporting').disabled=false;
				        	 $('removeBestVendorReporting').disabled=true;
				        	 $('editUserId').getSelected().set('onselect', "setCustomer(0,"+selectedCustomerId+","+"'"+$("customer_name").getText()+"',"+"'"+$("user_name").getText()+"',"+"'"+selectedFirstName+"',"+"'"+selectedLastName+"',"+"'"+$("email").getText()+"');");
		   				 }
						 $('message').set("html", responseHTML);
					 }}).send();
	}<% }%>
	function setCustomer(bestVendorParamCount, customerId, customerName, userName, firstName, lastName, email, licenseType) {
        $("customer_name").setText(customerName);
        $("user_name").setText(userName);
        $("full_name").setText(firstName + " " + lastName);
        $("email").setText(email);
        $("beButton").disabled = (!userName);
        if (licenseType === "C") {
        	if($('customerFees')){
           	 $('customerFees').disabled=false;
        	}
            <% if(user.isCustomerService()) { %>$("license_type").setText("Commissioned");<% } %>
        } else if (licenseType === "R") {
        	if($('customerFees')){
            	$('customerFees').disabled=true;
        	}
            <% if(user.isCustomerService()) { %>$("license_type").setText("Reseller");<% } %>
        } else if (licenseType === "S") {
        	if($('customerFees')){
            	$('customerFees').disabled=true;
        	}
            <% if(user.isCustomerService()) { %>$("license_type").setText("Standard");<% } %>
        }
        <% if(user.isReseller() || user.isCustomerService()) { %>
        if($('customerFees')){
        	$('customerFees').disabled = (licenseType!="C");
        }
        <% }
        if(updatable) {%>
        if(bestVendorParamCount>0){
        	$('removeBestVendorReporting').disabled=false;
        	$('addBestVendorReporting').disabled=true;
        }else{
        	$('removeBestVendorReporting').disabled=true;
        	if(customerId>0){
        		$('addBestVendorReporting').disabled=false;
        	}else{
        		$('addBestVendorReporting').disabled=true;
        	}
        }<% }%>
        $('message').setText("");
        selectedCustomerId=customerId;
        selectedFirstName=firstName;
        selectedLastName=lastName;
        if(customerId>0){
        	$('viewCustomerUserReport').set("href","view_customer_all_user_reports.html?customerId="+customerId);
        	$('viewCustomerUserReportDiv').set("style","display:block");
        }
    }
	
    window.addEvent('domready', function() { 
    	var select = $("editUserId");
    	if(select.selectedIndex < 0) {
			$("beButton").disabled = true;
			setCustomer(0,0,"","","","","");
		} else {
			$("beButton").disabled = false;
			select.fireEvent("change", new Event({type: "change", target: select, returnValue: true}));		    
		}
    	Form.attachOnSelect(select);
    });
    
    function searchCustomerName(){
    	$('customerName').value=$('customerName').value.trim();
    }    
</script>
<table class="selectBody">
	<tr><td valign="top" width="30%">
		<div class="selectSection">
			<div class="sectionTitle">Customers</div>
				<div class="sectionRow">
					<table>
        				<tr><td><select size="15" name="editUserId" id="editUserId" style="width: 100%;"><%
        while(customerResults!=null&&customerResults.next()) {%><option value="<%=StringUtils.prepareCDATA(customerResults.getFormattedValue("userId"))
        %>" onselect="setCustomer(<%=StringUtils.prepareScript(customerResults.getFormattedValue("bestVendorParamCount"))
        %>,<%=StringUtils.prepareScript(customerResults.getFormattedValue("customerId"))
        %>,'<%=StringUtils.prepareScript(customerResults.getFormattedValue("customerName"))
        %>','<%=StringUtils.prepareScript(customerResults.getFormattedValue("userName"))
        %>','<%=StringUtils.prepareScript(customerResults.getFormattedValue("firstName"))
        %>','<%=StringUtils.prepareScript(customerResults.getFormattedValue("lastName"))
        %>','<%=StringUtils.prepareScript(customerResults.getFormattedValue("email"))
        %>','<%=StringUtils.prepareScript(customerResults.getFormattedValue("licenseType"))%>');">
        <%=StringUtils.prepareCDATA(customerResults.getFormattedValue("customerName"))%>
        (<%=StringUtils.prepareCDATA(customerResults.getFormattedValue("firstName"))%> <%=StringUtils.prepareCDATA(customerResults.getFormattedValue("lastName"))%>)</option><%
		}%>
        </select>
        </td></tr>
        <tr><td align="center"><input id="beButton" type="submit" value="Login As"/></td></tr>
    </table>
    </div></div>
    </td>
    <td valign="top">
    <div class="selectSection">
			<div class="sectionTitle">Customer Information</div>		
    <table class="customerInfo">
        <tr><td>Customer Name</td><td><span id="customer_name" class="readonlyField"></span></td></tr>
        <tr><td>Primary Contact Member ID</td><td><span id="user_name" class="readonlyField"></span></td></tr>
        <tr><td>Primary Contact Name</td><td><span id="full_name" class="readonlyField"></span></td></tr>
        <tr><td>Primary Contact Email</td><td><span id="email" class="readonlyField"></span></td></tr>
        <% if(user.isCustomerService()) { %>
        <tr><td>License Type</td><td><span id="license_type" class="readonlyField"></span></td></tr>
        <% } %>
        <tr><td colspan="2" align="left">
        <% if(updatable) {%><input id="addBestVendorReporting" type="button" value="Add Best Vendor Reporting" disabled="disabled" onclick="addBVReporting();"/> <input id="removeBestVendorReporting" type="button" value="Remove Best Vendor Reporting" disabled="disabled" onclick="removeBVReporting()"/><%} %>
        <% if(user.isReseller() || user.isCustomerService()) { %>
        <input id="customerFees" type="button" value="Customer Fees" disabled="disabled" onclick="window.location = 'customer_fees.html?customerId=' + selectedCustomerId"/>
        <% } %>
        </td></tr>
        <tr><td colspan="2"><div id="message"></div></td></tr>
    </table>
    </div>
    </td></tr>
</table></form>
<div id="viewCustomerUserReportDiv" style="display:none"><a id="viewCustomerUserReport" href="">View Customer's User Reports</a></div>
</div>