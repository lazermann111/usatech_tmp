<%@page contentType="text/html"%>
<%@page import="simple.text.StringUtils"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML><HEAD><TITLE>USA Technologies, Inc.</TITLE>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1">
<BASE href="<%=StringUtils.prepareHTML(request.getRequestURL().toString())%>" />
<META name=copyright content="Copyright 2002, USA Technologies, Inc.">
<META name=author content="Brian Krug">
<STYLE>
  <jsp:include page="theme/Reports.css" flush="true"/> 
</STYLE>
</HEAD>
<BODY leftMargin=10px topMargin=10px>
<jsp:useBean id="innerPage" class="java.lang.String" scope="request" />
<jsp:include page="<%=StringUtils.prepareCDATA(innerPage)%>" flush="true"/>
</BODY></HTML>