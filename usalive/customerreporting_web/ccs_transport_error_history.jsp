<%@page
	import="simple.servlet.RequestUtils,simple.text.StringUtils,simple.results.Results"%>
<%
	Results ccsTransportErrorResults = RequestUtils.getAttribute(
			request, "ccsTransportErrorResults", Results.class, false);
	int rowNum = 1;
%>
<div>
	<table class="reportTitle">
		<tr>
			<td>
				<div class="title2">Transport Error History</div>
				<table class="reportTitle">
					<tr class="headerRow">
						<th><a data-toggle="sort" data-sort-type="STRING" title="">Report Name</a></th>
						<th><a data-toggle="sort" data-sort-type="STRING" title="">Transport Name</a></th>
						<th><a data-toggle="sort" data-sort-type="STRING" title="">Transport Type</a></th>
						<th>Transport Properties</th>
						<th>Error Reason</th>
						<th><a data-toggle="sort" data-sort-type="STRING" title="">Error Time</a></th>
						<th><a data-toggle="sort" data-sort-type="STRING" title="">Reattempt Time</a></th>
						<th><a data-toggle="sort" data-sort-type="STRING" title="">Error Type</a></th>
						<th><a data-toggle="sort" data-sort-type="NUMBER" title="">Retry Count</a></th>
					</tr>
					<%
						while (ccsTransportErrorResults != null
								&& ccsTransportErrorResults.next()) {
					%>
					<%
						if (rowNum % 2 == 0) {
					%>
					<tr class="evenRow">
						<%
							} else {
						%>


						<tr class="oddRow">
						<%
							}
						%><td
							data-sort-value="<%=StringUtils.prepareCDATA(ccsTransportErrorResults
						.getFormattedValue("reportName"))%>">
							<%=StringUtils.prepareCDATA(ccsTransportErrorResults
						.getFormattedValue("reportName"))%>
						</td>
						<td data-sort-value="<%=StringUtils.prepareCDATA(ccsTransportErrorResults
						.getFormattedValue("transportName"))%>"><%=StringUtils.prepareCDATA(ccsTransportErrorResults
						.getFormattedValue("transportName"))%></td>
						<td data-sort-value="<%=StringUtils.prepareCDATA(ccsTransportErrorResults
						.getFormattedValue("transportTypeName"))%>"><%=StringUtils.prepareCDATA(ccsTransportErrorResults
						.getFormattedValue("transportTypeName"))%></td>
						<td data-sort-value="<%=StringUtils.prepareCDATA(ccsTransportErrorResults
						.getFormattedValue("transportProp"))%>"><%=StringUtils.prepareCDATA(ccsTransportErrorResults
						.getFormattedValue("transportProp"))%></td>
						<td data-sort-value="<%=StringUtils.prepareCDATA(ccsTransportErrorResults
						.getFormattedValue("errorText"))%>"><%=StringUtils.prepareCDATA(ccsTransportErrorResults
						.getFormattedValue("errorText"))%></td>
						<td data-sort-value="<%=StringUtils.prepareCDATA(ccsTransportErrorResults
						.getFormattedValue("errorTs"))%>"><%=StringUtils.prepareCDATA(ccsTransportErrorResults
						.getFormattedValue("errorTs"))%></td>
						<td data-sort-value="<%=StringUtils.prepareCDATA(ccsTransportErrorResults
						.getFormattedValue("reattemptedTs"))%>"><%=StringUtils.prepareCDATA(ccsTransportErrorResults
						.getFormattedValue("reattemptedTs"))%></td>
						<td data-sort-value="<%=StringUtils.prepareCDATA(ccsTransportErrorResults
						.getFormattedValue("errorType"))%>"><%=StringUtils.prepareCDATA(ccsTransportErrorResults
						.getFormattedValue("errorType"))%></td>
						<%if(ccsTransportErrorResults.getValue("retryCount", int.class)>1){ %>
							<td data-sort-value="<%=StringUtils.prepareCDATA(ccsTransportErrorResults
						.getFormattedValue("retryCount"))%>"><a href="javascript:requestTransportErrorHistoryDetails(<%=StringUtils.prepareCDATA(ccsTransportErrorResults
						.getFormattedValue("errorId"))%>)"><%=StringUtils.prepareCDATA(ccsTransportErrorResults
						.getFormattedValue("retryCount"))%></a></td>
						<%}else{ %>
						<td data-sort-value="0"><%=StringUtils.prepareCDATA(ccsTransportErrorResults
						.getFormattedValue("retryCount"))%></td>
						<%} %>
					</tr>
					
						<%
												rowNum++;
												}
											%>
						<%
							if (rowNum == 1) {
						%>
						
					<tr>
							<td colspan="9">No transport error history found</td>
						</tr>
						<%
							}
						%>
				</table> &#160;
				<div class="copyright"></div>
			</td>
		</tr>
	</table>
</div>