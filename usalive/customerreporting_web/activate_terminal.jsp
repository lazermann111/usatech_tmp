<%@page import="simple.servlet.InputForm"%>
<%@page import="com.usatech.layers.common.fees.LicenseFees"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.util.Set"%>
<%@page import="simple.bean.ConvertUtils"%>
<%@page import="com.usatech.report.custom.GenerateUtils,simple.servlet.RequestUtils,simple.text.StringUtils"%>
<jsp:useBean id="machineResults" type="simple.results.Results" scope="request" />
<jsp:useBean id="custBankResults" type="simple.results.Results" scope="request" />
<jsp:useBean id="commissionBankResults" type="simple.results.Results" scope="request" />
<jsp:useBean id="locationTypeResults" type="simple.results.Results" scope="request" />
<jsp:useBean id="productTypeResults" type="simple.results.Results" scope="request" />
<jsp:useBean id="contactResults" type="simple.results.Results" scope="request" />
<jsp:useBean id="timeZoneResults" type="simple.results.Results" scope="request" />
<jsp:useBean id="paymentScheduleResults" type="simple.results.Results" scope="request" />
<jsp:useBean id="dealerResults" type="simple.results.Results" scope="request" />
<jsp:useBean id="regionResults" type="simple.results.Results" scope="request" />
<jsp:useBean id="customResults" type="simple.results.Results" scope="request" />
<jsp:useBean id="customComments" type="simple.results.Results" scope="request" />
<jsp:useBean id="businessTypeResults" type="simple.results.Results" scope="request" />
<div><%
String serial = RequestUtils.getAttribute(request, "serial", String.class, false);
UsaliveUser user = (UsaliveUser) RequestUtils.getUser(request);
InputForm form = RequestUtils.getInputForm(request);
Long dealerId = RequestUtils.getAttribute(request, "dealerId", Long.class, false);
simple.text.TimeZoneFormat tzf = new simple.text.TimeZoneFormat();
/* we are going to hard code this for now (BSK 09-24-04)*/
String deviceName = "Device";
boolean extra = true;
if(dealerId != null && dealerId == 302) { /* MEI */
  deviceName = "MEI System";
  extra = false;
}
%>
<% if (user.isReseller()) { %>
<script src="/scripts/fees.js"></script>
<% } %>
<script type="text/javascript" defer="defer">
//<![CDATA[
var machineDialog;
window.addEvent("domready", function() {
  new Form.Validator.Inline.Mask(document.terminalForm);
  new Form.Validator.Inline.Mask(document.machineForm);
  machineDialog = new Dialog({content: $(document.machineForm), destroyOnClose: false});
<% if(!StringUtils.isBlank(serial)) {%>
  handleDeviceChange($(document.forms["terminalForm"].elements["serial"]));
<%}%>
});
function newMachine() {
  machineDialog.show();
}
function cancelMachine() {
  machineDialog.hide();
}
function saveMachine() {
  var form = $(document.machineForm);
  if(!form.validate())
    return;
  var terminalForm = $(document.terminalForm);
  terminalForm.make.value = form.make.value;
  terminalForm.model.value = form.model.value;
  var machineKey = form.make.value + ' - - ' + form.model.value;
  var op = terminalForm.machine.options[terminalForm.machine.length-1];
  if(op.value != 0) {
    op = new Element("option", {value: machineKey});
    terminalForm.machine.options.add(op);
  }
  op.set("text", machineKey);
  op.set("value", machineKey);
  terminalForm.machine.selectedIndex = terminalForm.machine.options.length - 1;
  machineDialog.hide();
}
function handleSpecifyChanged(el, enable) {
  var el = $(el);
  if(el) {
    el.disabled=!enable;
    if(!enable)
      App.revalidate(el);
  }
}
function handleDeviceChange(input) {
  input = $(input);
  if(input.getValue() && /^[kK]3/.test(input.getValue())) {
    $("fieldsTable").getElements("tr.k3OnlyRow").reveal();
    $("fieldsTable").getElements("tr.k3HideRow").dissolve();
  } else { 
    $("fieldsTable").getElements("tr.k3OnlyRow").dissolve();
    $("fieldsTable").getElements("tr.k3HideRow").reveal();
  }
}
function handleMobileIndicatorChange(input) {
  input = $(input);
  var mobile = (input.checked == (input.value == 'Y'));
  var elements = input.form.terminalActivator;
  for(var i = 0; i < elements.length; i++) {
    elements[i].disabled = mobile;
    if(mobile)
      elements[i].checked = (elements[i].value == 'A');
    else if(elements[i].checked)
      handleActivatorChange(elements[i]);
  }
  $("terminalActivatorFieldset").disabled = mobile;
  var validator = input.form.get("validator");
  validator.resetField($("terminalActivatorFieldset"));
  $("entryListManualCheckbox").disabled = mobile;
  if(mobile) {
    setEntryListSignatureDisabled(false);
    $("entryListManualCheckbox").checked = true;
  }
}
function handleActivatorChange(input) {
  input = $(input);
  var unattended = (input.checked == (input.value == 'C'));
  setEntryListSignatureDisabled(unattended);
}
function setEntryListSignatureDisabled(disabled) {
  var fieldset = $("entryListSignatureFieldset");
  fieldset.disabled = disabled;
  var elements =  fieldset.getElements("input[name=entryList]");
  for(var i = 0; i < elements.length; i++)
    elements[i].disabled = disabled;
  var validator = fieldset.form.get("validator");
  validator.resetField(fieldset);
}
//]]>
</script><%
String errorMessage = RequestUtils.getAttribute(request, "errorMessage", String.class, false);
if(errorMessage != null && errorMessage.trim().length() > 0) {%>
<table class="messages">
  <tr>
    <td><ul class="message-list"><li class="message-error"><%=StringUtils.prepareHTML(errorMessage)%></li></ul></td>
  </tr>
</table>
<%} %>
<div id="editTerminalDetails">
<form name="terminalForm" action="activate_terminal.i" method="post">
<div class="instructions">Please enter the information below to activate your device:</div>
<table>
  <tr>
    <td><table class="fields" id="fieldsTable">
    <tr class="tableHeader"><th colspan="2">Device Information</th></tr>
    <tr class="tableDataShade"><th><%=deviceName%> Serial Number<span class="required">*</span></th><td><input size="50" maxlength="50" type="text" name="serial" data-validators="required" value="<%=StringUtils.prepareCDATA(serial)%>" onchange="handleDeviceChange(this)"/>
    &#160;<input type="button" name="copyButton" value="Copy Settings..." onclick="document.location.href='copy_terminal_settings.i?session-token=<%=StringUtils.prepareScript(RequestUtils.getSessionTokenIfPresent(request))%>'"/></td></tr>
<% if(!user.isPartner()) { %>
    <tr class="tableDataShade"><th>Program<span class="required">*</span></th>
    	<td><select name="dealerId" data-validators="required"><%
    	while(dealerResults.next()) {
            %><option value="<%=StringUtils.prepareCDATA(dealerResults.getFormattedValue("dealerId"))%>"<%if(dealerId != null && dealerId.equals(dealerResults.getValue("dealerId", Long.class))) { %> selected="selected"<%} %>><%=StringUtils.prepareHTML(dealerResults.getFormattedValue("dealerName"))%></option><%
        } %></select></td></tr>
<% } %>
    <tr class="tableHeader"><th colspan="2">Credit Card Statement Information</th></tr>
    <%if(user.isInternal()||(user.getMasterUser()!=null&&user.getMasterUser().isInternal())){ %>
    <tr class="tableDataShade"><th>Override Doing Business As</th><td style="vertical-align: middle;"><input maxlength="21" type="text" name="doingBusinessAs" id="doingBusinessAs" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "doingBusinessAs", String.class, false))%>"/>, shown on credit card statements, overrides customer setup parameter</td></tr>
    <%} %>
    <tr class="tableDataShade"><th>Override Customer Service Phone</th><td><input size="50" maxlength="20" type="text" name="customerServicePhone" id="customerServicePhone" edit-mask="Fixed.PhoneUs" data-validators="validate-digits minLength:10" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "customerServicePhone", String.class, false))%>"/></td></tr>
    <tr class="tableDataShade"><th>Override Customer Service Email</th><td><input size="50" maxlength="70" type="text" name="customerServiceEmail" id="customerServiceEmail" data-validators="validate-email" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "customerServiceEmail", String.class, false))%>"/></td></tr>    
    <tr class="tableSpacer"><td colspan="2"/></tr><tr class="tableHeader"><th colspan="2">Location Information</th></tr>
    <tr class="tableDataShade"><th>Region</th><td><select name="region"><%
      String region = RequestUtils.getAttribute(request, "region", String.class, false);
      String regionName = RequestUtils.getAttribute(request, "regionName", String.class, false); %>
      <option<%if(StringUtils.isBlank(region)) { %> selected="selected"<%} %> onselect="this.form.regionName.disabled=true"/><%
      while(regionResults.next()) {
        String regionVal = regionResults.getFormattedValue("regionName");
            %><option value="<%=StringUtils.prepareCDATA(regionVal)%>"<%if(regionVal.equals(region)) { %> selected="selected"<%} %> onselect="this.form.regionName.disabled=true"><%=StringUtils.prepareHTML(regionVal)%></option><%
        } %>
        <option value="" onselect="this.form.regionName.disabled=false;this.form.regionName.focus();">-- New Region --</option>
    </select>&#160; <input class="form-field" type="text" size="50" maxlength="50" name="regionName" <%if(StringUtils.isBlank(regionName)) { %> disabled="disabled"<%} %> value="<%=StringUtils.prepareCDATA(regionName)%>"/>
    </td></tr>
    <tr class="tableDataShade"><th>Location Name<span class="required">*</span></th><td><input size="50" maxlength="50" type="text" name="location" data-validators="required" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "location", String.class, false))%>"/></td></tr>
    <tr class="tableDataShade"><th>Address<span class="required">*</span></th><td>
    	<input size="50" maxlength="50" type="text" name="address1" data-validators="required" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "address1", String.class, false))%>"/>
    	<%GenerateUtils.writePostalTableElement(pageContext, null, RequestUtils.getAttribute(request, "postal", String.class, false), RequestUtils.getAttribute(request, "city", String.class, false), RequestUtils.getAttribute(request, "state", String.class, false), RequestUtils.getAttribute(request, "country", String.class, false), false, GenerateUtils.getTerminalCountries()); %>        
    </td></tr>
    <tr class="tableDataShade"><th>Specific Location at this Address<span class="required">*</span></th><td><input size="50" maxlength="50" type="text" name="locationDetails" data-validators="required" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "locationDetails", String.class, false))%>"/></td></tr>
    <tr class="tableDataShade"><th>Phone Number at this Location</th><td><input size="50" maxlength="50" type="text" name="telephone" edit-mask="Fixed.PhoneUs" data-validators="validate-digits minLength:10" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "telephone", String.class, false))%>"/></td></tr>
    <% if(extra){%><tr class="tableDataShade"><th>Location Type<span class="required">*</span></th><td>
		<table><tr><td><select name="locationType" onchange="this.options.item(this.selectedIndex).onclick();" data-validators="required"><%
        String locationType = RequestUtils.getAttribute(request, "locationType", String.class, false);
        boolean enableSpecify = false;
        boolean showSpecify = false;
        if(locationType == null) {
        %><option onselect="handleSpecifyChanged(this.form.locationTypeSpecify, false);"/><%
        }
        while(locationTypeResults.next()) {
        	String locationTypeName = locationTypeResults.getFormattedValue("locationTypeName");
        	boolean specify = "Y".equalsIgnoreCase(locationTypeResults.getValue("specifyRequired", String.class));
            %><option value="<%=StringUtils.prepareCDATA(locationTypeName)%>"<%if(locationType != null && locationType.equals(locationTypeName)) { enableSpecify = specify; %> selected="selected"<%} %>
            onclick="handleSpecifyChanged(this.form.locationTypeSpecify, <%=specify %>);">
            <%=StringUtils.prepareHTML(locationTypeName)%>
            <%if(specify) { showSpecify = true; %> (specify)<%}%></option><%
        } %></select>&#160;</td><td>
        <%if(showSpecify) { %><input class="form-field" type="text" size="50" maxlength="50" name="locationTypeSpecify" data-validators="required"<%if(!enableSpecify) {%> disabled="disabled"<%}%> value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "locationTypeSpecify", String.class, false))%>"/><%} %></td></tr></table>
        </td></tr><%}%>
        <tr class="tableDataShade"><th>Location's Time Zone<span class="required">*</span></th><td><select name="timeZone" data-validators="required"><%
    	Integer timeZoneId = RequestUtils.getAttribute(request, "timeZone", Integer.class, false);
        while(timeZoneResults.next()) {
        	int tzId = timeZoneResults.getValue("timeZoneId", Integer.class);
            %><option value="<%=tzId%>"<%if(timeZoneId != null && timeZoneId == tzId) { %> selected="selected"<%} %>>
            <%=StringUtils.prepareHTML(timeZoneResults.getFormattedValue("timeZoneName"))%>
            (<%=StringUtils.prepareHTML(tzf.format(timeZoneResults.getValue("timeZoneOffset")))%>)</option><%
        } %></select>
        </td></tr>
<%customResults.setRow(Integer.MIN_VALUE);
  while(customResults.next()) { if(customResults.isGroupBeginning(0)) {%><tr class="tableSpacer"><td colspan="2"/></tr><tr class="tableHeader"><th colspan="2">Custom Field Information</th></tr><%}%>  
<%while(customComments.next()) {%>
  <tr class="tableDataShade"><td colspan="2" class="<%=StringUtils.encodeForHTMLAttribute(customComments.getFormattedValue("commentClass"))%>">
  <br/><%=StringUtils.prepareHTML(customComments.getFormattedValue("commentText"))%><br/></td></tr><%}%> 
    <tr class="tableDataShade"> 
      <td><%=StringUtils.encodeForHTML(customResults.getFormattedValue("fieldLabel"))%>
          <% String d = customResults.getFormattedValue("fieldRequired");
             if("Y".equalsIgnoreCase(d)) { %><span class="required">*</span><% } %></td>
      <td><input size="50" maxlength="4000" type="text" name="<%=StringUtils.prepareCDATA(customResults.getFormattedValue("fieldName").toLowerCase())%>"<%if("Y".equalsIgnoreCase(d)) { %> data-validators="required"<%} %> value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, customResults.getFormattedValue("fieldName"), String.class, false))%>"/></td>
    </tr><%}%>  
    <tr class="tableSpacer"><td colspan="2"/></tr><tr class="tableHeader"><th colspan="2">Machine Information</th></tr>
    <tr class="tableDataShade"><th>Machine Asset Number<span class="required">*</span></th><td><input size="50" maxlength="15" type="text" name="asset" data-validators="required" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "asset", String.class, false))%>"/></td></tr>
    <tr class="tableDataShade"><th>Machine's Make and Model<span class="required">*</span></th>
        <td><select name="machine" data-validators="required"><%
        String machine = RequestUtils.getAttribute(request, "machine", String.class, false);
        if(machine == null) {%>
        <option/><%
        }
        while(machineResults.next()) {
        	String make = machineResults.getFormattedValue("machineMake");
        	String model = machineResults.getFormattedValue("machineModel");
        	String machineKey = make + " - - " + model;
        %><option value="<%=StringUtils.prepareCDATA(machineKey)%>"<%if(machineKey.equals(machine)) { %> selected="selected"<%} %>>
        <%=StringUtils.prepareHTML(machineKey) %>
        </option><%
        }%>
        </select>&#160;
        <input type="button" value="New Make/Model" onclick="newMachine()" name="addMachineButton"/></td></tr>
    <tr class="tableDataShade"><th>Product Type<span class="required">*</span></th><td>
        <table><tr><td><select name="productType" onchange="this.options.item(this.selectedIndex).onclick();" data-validators="required"><%
        String productType = RequestUtils.getAttribute(request, "productType", String.class, false);
        boolean enableSpecify = false;
        boolean showSpecify = false;
        if(productType == null) {
        %><option onselect="handleSpecifyChanged(this.form.productTypeSpecify, false);"/><%
        }
        while(productTypeResults.next()) {
        	String productTypeName = productTypeResults.getFormattedValue("productTypeName");
        	boolean specify = "Y".equalsIgnoreCase(productTypeResults.getValue("specifyRequired", String.class));
            %><option value="<%=StringUtils.prepareCDATA(productTypeName)%>"<%if(productType != null && productType.equals(productTypeName)) { enableSpecify = specify; %> selected="selected"<%} %>
            onclick='handleSpecifyChanged(this.form.productTypeSpecify, <%=specify %>);'>
            <%=StringUtils.prepareHTML(productTypeName)%>
            <%if(specify) { showSpecify = true; %> (specify)<%}%></option><%
        } %></select>&#160;</td><td>
        <%if(showSpecify) { %><input class="form-field" type="text" size="50" maxlength="50" name="productTypeSpecify" data-validators="required"<%if(!enableSpecify) {%> disabled="disabled"<%}%> value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "productTypeSpecify", String.class, false))%>"/><%} %></td></tr></table>
       </td></tr><%
       String posEnvironment = RequestUtils.getAttribute(request, "posEnvironment", String.class, false);
       if(StringUtils.isBlank(posEnvironment))
           posEnvironment =  "?";
       String entryCapability = RequestUtils.getAttribute(request, "entryCapability", String.class, false);
       Boolean pinCapability = RequestUtils.getAttribute(request, "pinCapability", Boolean.class, false);
       Character terminalActivator;
       Character mobileIndicator;
       switch(posEnvironment.charAt(0)) {
           case 'E':
           case 'U':
               mobileIndicator = 'N';
               terminalActivator = 'C';
               break;
           case 'C':
               mobileIndicator = 'Y';
               terminalActivator = 'A';
               break;
           case 'M':
           case 'A':
               mobileIndicator = 'N';
               terminalActivator = 'A';
               break;
           default:
               mobileIndicator = '?';
               terminalActivator = '?';
       }
       terminalActivator = ConvertUtils.convertSafely(Character.class, RequestUtils.getAttribute(request,"terminalActivator", false), terminalActivator);
       mobileIndicator = ConvertUtils.convertSafely(Character.class, RequestUtils.getAttribute(request,"mobileIndicator", false), mobileIndicator);
       Set<Character> entryList = ConvertUtils.convert(Set.class,  Character.class, RequestUtils.getAttribute(request,"entryList", false));   
       if(entryList == null) {
           entryList = new HashSet<Character>();
           switch(posEnvironment.charAt(0)) {
               case 'E':
               case 'C':
               case 'M':
                   entryList.add('M');
                   break;
           }
           if(!StringUtils.isBlank(entryCapability)) {
               for(int i = 0; i < entryCapability.length(); i++)
                   entryList.add(entryCapability.charAt(i));
           }
           if(pinCapability != null && pinCapability.booleanValue())
               entryList.add('M');
       }
       %>
    <tr class="tableDataShade k3OnlyRow hide"><th>Machine is Tablet / Phone<span class="required">*</span></th><td><fieldset class="no-border-grouper" data-validators="validate-reqchk-bynode">
        <label><input type="radio" name="mobileIndicator" value="Y"<%if(mobileIndicator == 'Y') {%> checked="checked"<%} %> onclick="handleMobileIndicatorChange(this)"/>Yes</label>
        <label><input type="radio" name="mobileIndicator" value="N"<%if(mobileIndicator == 'N') {%> checked="checked"<%} %> onclick="handleMobileIndicatorChange(this)"/>No</label>
        </fieldset></td></tr>
    <tr class="tableDataShade k3OnlyRow hide"><th>Card Data Entered By<span class="required">*</span></th><td><fieldset class="no-border-grouper" data-validators="validate-reqchk-bynode" id="terminalActivatorFieldset"<%if(mobileIndicator == 'Y') {%> disabled="disabled"<%} %>>
        <label><input type="radio" name="terminalActivator" value="C"<%if(terminalActivator == 'C' && mobileIndicator != 'Y') {%> checked="checked"<%} %><%if(mobileIndicator == 'Y') {%> disabled="disabled"<%} %> onclick="handleActivatorChange(this)"/>Card Holder</label>
        <label><input type="radio" name="terminalActivator" value="A"<%if(terminalActivator == 'A' || mobileIndicator == 'Y') {%> checked="checked"<%} %><%if(mobileIndicator == 'Y') {%> disabled="disabled"<%} %> onclick="handleActivatorChange(this)"/>Attendant (Cashier)</label>
        </fieldset></td></tr>
    <tr class="tableDataShade k3OnlyRow hide"><th>Card Entry Types<span class="required">*</span></th><td><fieldset class="no-border-grouper" data-validators="validate-reqchk-bynode">
        <label><input type="checkbox" name="entryList" value="M"<%if(entryList.contains('M') || mobileIndicator == 'Y') {%> checked="checked"<%} %><%if(mobileIndicator == 'Y') {%> disabled="disabled"<%} %> id="entryListManualCheckbox"/>Keypad or Keyboard</label>
        <label><input type="checkbox" name="entryList" value="S"<%if(entryList.contains('S')) {%> checked="checked"<%} %>/>Card Swipe Reader</label>
        <label><input type="checkbox" name="entryList" value="P"<%if(entryList.contains('P')) {%> checked="checked"<%} %>/>Proximity Reader (Contactless)</label>
        <label><input type="checkbox" name="entryList" value="E"<%if(entryList.contains('E')) {%> checked="checked"<%} %>/>EMV</label>
        </fieldset></td></tr>
    <tr class="tableDataShade k3OnlyRow hide"><th>Signature Capture<span class="required">*</span></th><td><fieldset class="no-border-grouper" data-validators="validate-reqchk-bynode"<%if(mobileIndicator == 'N' && terminalActivator != 'A') {%> disabled="disabled"<%} %> id="entryListSignatureFieldset">
        <label><input type="radio" name="entryList" value="X"<%if(entryList.contains('X')) {%> checked="checked"<%} %>/>Electronic</label>
        <label><input type="radio" name="entryList" value=""<%if(!entryList.contains('X')) {%> checked="checked"<%} %>/>Manual</label>
        </fieldset></td></tr>
<%
    if(false) { %>
    <tr class="tableSpacer"><td colspan="2"/></tr><tr class="tableHeader"><th colspan="2">Transaction Settings</th></tr>
    <tr class="tableDataShade"><th>Items Per Transaction Mode<span class="required">*</span></th><td>
        <select name="vends" data-validators="required"><%
        Integer vends = RequestUtils.getAttribute(request, "vends", Integer.class, false);
        if(vends == null || vends < 1) {%>
        <option></option><%} %>
        <option value="1"<%if(vends != null && vends == 1) {%> selected="selected"<%} %>>Single Item</option>
        <option value="10"<%if(vends != null && vends > 1) {%> selected="selected"<%} %>>Multiple Items</option></select>
    </td></tr>
    <tr class="tableDataShade"><th>Credit Card Authorization<span class="required">*</span></th><td>
        <select name="authMode" data-validators="required"><%
        Character authMode = RequestUtils.getAttribute(request, "authMode", Character.class, false);
        if(authMode == null) {%>
        <option></option><%} %>
        <option value="N"<%if(authMode != null && authMode == 'N') {%> selected="selected"<%} %>>Credit Cards Not Accepted</option>
        <option value="L"<%if(authMode != null && authMode == 'L') {%> selected="selected"<%} %>>Local Authorization</option>
        <option value="F"<%if(authMode != null && authMode == 'F') {%> selected="selected"<%} %>>Full Authorization</option></select>
    </td></tr>
    <%if(extra) {%><tr class="tableDataShade"><th>Print a receipt?<span class="required">*</span></th><td>
        <select name="receipt" data-validators="required"><%
        Character receipt = RequestUtils.getAttribute(request, "receipt", Character.class, false);
        if(receipt == null) {%>
        <option></option><%} %>
        <option value="Y"<%if(receipt != null && receipt == 'Y') {%> selected="selected"<%} %>>Yes</option>
        <option value="N"<%if(receipt != null && receipt == 'N') {%> selected="selected"<%} %>>No</option>
        </select>
    </td></tr>
    <tr class="tableDataShade"><th>DEX Data Capture<span class="required">*</span></th><td><%
    	Character dexData = RequestUtils.getAttribute(request, "dexData", Character.class, false);
    %><fieldset class="no-border-grouper" data-validators="validate-reqchk-bynode">
        <label class="radioLabel"><input type="radio" name="dexData" value="N"<%if(dexData != null && dexData == 'N') {%> checked="checked"<%} %>/><b>None</b></label><br/>
        <label class="radioLabel"><input type="radio" name="dexData" value="D"<%if(dexData != null && dexData == 'D') {%> checked="checked"<%} %>/><b>Complete DEX File</b> ($4.00 per month) - includes download of complete DEX File for importing and web notification of alarms on delayed basis</label><br/><%
        /*%>
        <label class="radioLabel"><input type="radio" name="dexData" value="A"<%if(dexData != null && dexData == 'A') {%> checked="checked"<%} %>/><b>Alarm Notification + Complete DEX File</b> ($0.40 per alarm + $4.00 per month) - includes immediate email notification of each alarm, in addition to all benefits described above</label><br/><%
        */%></fieldset>
    </td></tr><%} 
    }%>
    <tr class="tableSpacer"><td colspan="2"/></tr><tr class="tableHeader"><th colspan="2">Payment Information</th></tr>
    <tr class="tableDataShade"><th><%=user.isPartner() ? "Customer" : ""%> Bank Account To Pay
      <% if (user.isReseller()) { %><span class="required">*</span><% } else { %><span class="baNote">*</span><% } %></th><td>
    <select name="custBankId" <% if (user.isReseller()) { %>data-validators="required"<% } %>><%
    	Long bankAcctId = RequestUtils.getAttribute(request, "custBankId", Long.class, false);	
    	while(custBankResults.next()) {
    		long baId = custBankResults.getValue("custBankId", Long.class);
            %><option value="<%=baId%>"<%if(bankAcctId != null && baId == bankAcctId) {%> selected="selected"<%} %>>
            <%=user.isReseller() ?  StringUtils.prepareHTML(custBankResults.getFormattedValue("custName")) + " - " : ""%>
            <%=StringUtils.encodeForHTML(custBankResults.getFormattedValue("bankName"))%>
            - #<%=StringUtils.prepareHTML(custBankResults.getFormattedValue("bankAcctNum"))%>
            - <%=StringUtils.prepareHTML(custBankResults.getFormattedValue("accountTitle"))%>
              <%=custBankResults.getFormattedValue("status").equalsIgnoreCase("P") ? " (PENDING APPROVAL)" : "" %></option><%
        }
    	%><option value="">-- None --</option></select></td></tr>
    <tr class="tableDataShade"><th>Payment Schedule<span class="required">*</span></th><td>
    <select name="paymentScheduleId" data-validators="required"><%
        Integer paymentScheduleId = RequestUtils.getAttribute(request, "paymentScheduleId", Integer.class, false);
        while(paymentScheduleResults.next()) {
            int psId = paymentScheduleResults.getValue("paymentScheduleId", Integer.class);
        %><option value="<%=psId%>"<%if(paymentScheduleId != null && paymentScheduleId == psId) { %> selected="selected"<%} %>>
        <%=StringUtils.prepareHTML(paymentScheduleResults.getFormattedValue("paymentScheduleName")) %></option><%
        }%>
        </select>
        <span class="help-button"><a onclick="paySchedInstructsDialog.show();">&#160;</a></span>
        <script type="text/javascript">var paySchedInstructsDialog = new Dialog.Help({ helpKey: 'payment-schedule-instructions', destroyOnClose: false, height: 300 });</script>
        </td></tr>
    <%if(user.isInternal()||(user.getMasterUser()!=null&&user.getMasterUser().isInternal())){ %>
    <tr class="tableDataShade"><th>Business Type<span class="required">*</span></th><td>
    <select name="businessType" data-validators="required"><%
        String businessType = RequestUtils.getAttribute(request, "businessType", String.class, false);
        if(businessType == null) {%>
        <option></option><%
        }
        while(businessTypeResults.next()) {
        	String businessTypeName = businessTypeResults.getFormattedValue("businessTypeName");
        %><option value="<%=StringUtils.prepareCDATA(businessTypeName) %>"<%if(businessType != null && businessType.equals(businessTypeName)) { %> selected="selected"<%} %>>
        <%=StringUtils.prepareHTML(businessTypeName)%></option><%
        }%>
        </select>
        </td></tr>
    <%} %>
    <% if(extra) {%><tr class="tableSpacer"><td colspan="2"/></tr><tr class="tableHeader"><th colspan="2">Contact Information</th></tr>
    <tr class="tableDataShade"><th>Primary Contact<span class="required">*</span></th><td>
    <select name="primaryContactId" data-validators="required"><%
	    Integer primaryContactId = RequestUtils.getAttribute(request, "primaryContactId", Integer.class, false);
		while(contactResults.next()) {
        	int userId = contactResults.getValue("userId", Integer.class);
            %><option value="<%=userId%>"<%if(primaryContactId != null && userId == primaryContactId) {%> selected="selected"<%} %>>
            	<%=user.isPartner() ? StringUtils.prepareHTML(contactResults.getFormattedValue("custName")) + " - " : ""%>
            	<%=StringUtils.encodeForHTML(contactResults.getFormattedValue("firstName"))%> 
            	<%=StringUtils.prepareHTML(contactResults.getFormattedValue("lastName"))%>
            </option><%
        } %></select>
    </td></tr>
    <tr class="tableDataShade"><th>Secondary Contact</th><td>
    <select name="secondaryContactId"><option></option><%
    Integer secondaryContactId = RequestUtils.getAttribute(request, "secondaryContactId", Integer.class, false);
    contactResults.setRow(0);
    while(contactResults.next()) {
        	int userId = contactResults.getValue("userId", Integer.class);
            %><option value="<%=userId%>"<%if(secondaryContactId != null && userId == secondaryContactId) {%> selected="selected"<%} %>>
            	<%=user.isPartner() ? StringUtils.prepareHTML(contactResults.getFormattedValue("custName")) + " - " : ""%>
            	<%=StringUtils.prepareHTML(contactResults.getFormattedValue("firstName"))%> 
            	<%=StringUtils.prepareHTML(contactResults.getFormattedValue("lastName"))%>
            </option><%
        } %></select>
    </td></tr>
<% if (user.isPartner()) { %>
    <tr class="tableSpacer k3OnlyRow hide"><td colspan="2"/></tr><tr class="tableHeader k3OnlyRow hide"><th colspan="2">Quick Connect Information</th></tr>
    <tr class="tableDataShade k3OnlyRow hide"><th>Username<span class="required">*</span></th>
    <td><input size="20" maxlength="20" type="text" name="quickConnect" data-validators="required" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "quickConnect", String.class, false))%>"/></td></tr>
	<tr>
    <td colspan="3" class="spacer"></td>
  </tr>
<% }
   if (user.isReseller()) {
     LicenseFees fees = LicenseFees.loadByCustomerId(user.getCustomerId());
     fees.updateFromForm(form, false); %>
  <tr class="tableHeader k3OnlyRow hide">
    <th colspan="3" align="left">Fees</th>
  </tr>
  <tr class="tableDataShade k3OnlyRow hide">
    <td colspan="3">
      <% fees.writeHtmlTable(pageContext, true); %>
    </td>
  </tr>
  <tr>
    <td colspan="2 k3OnlyRow hide">
      <div class="spacer5"></div>
    </td>
  </tr>
  <tr class="tableHeader k3OnlyRow hide">
    <th colspan="2">Commission Payment Information</th>
  </tr>
  <tr class="tableDataShade k3OnlyRow hide">
    <th>Bank Account To Pay<span class="required">*</span></th>
    <td><select name="COMMISSION_BANK_ID"><%
Long selectedBankId = ConvertUtils.convertSafely(Long.class, RequestUtils.getAttribute(request,"COMMISSION_BANK_ID", false), null);
while(commissionBankResults.next()) {
  long bankId = commissionBankResults.getValue("custBankId", Long.class);%>
  <option value="<%=bankId%>"<%if(selectedBankId != null && selectedBankId == bankId) {%> selected="selected"<%} %>>
    <%=StringUtils.prepareHTML(commissionBankResults.getFormattedValue("bankName"))%>
    - #<%=StringUtils.prepareHTML(commissionBankResults.getFormattedValue("bankAcctNum"))%>
    - <%=StringUtils.prepareHTML(commissionBankResults.getFormattedValue("accountTitle"))%>
      <%=commissionBankResults.getFormattedValue("status").equalsIgnoreCase("P") ? " (PENDING APPROVAL)" : "" %></option><%
}%></select>
    </td>
  </tr>
<% } }%>
</table></td></tr>
<tr><td><span class="required">* Required Fields</span></td></tr>
<% if (!user.isReseller()) { %><tr><td><span class="baNote">* If blank, no payments will be made for revenue generated by this device</span></td></tr><% } %>
<tr><td class="instructions"><br/>Activation Terms &amp; Conditions</td></tr>
<tr><td>By activating the above device(s), I authorize USA Technologies, Inc. to commence billing of the monthly per device fee as
        previously agreed to in the Device License Agreement and the above fees related to DEX reporting services.</td>
</tr><tr><td align="center"><input type="submit" value="Accept" name="submitButton"/></td></tr>
</table>
<input type="hidden" name="make"/>
<input type="hidden" name="model"/>
<input type="hidden" name="addressId" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "address_id", String.class, false))%>"/>
</form></div>
<div id="machineDetails" style="display: none;">
<form class="popupForm" name="machineForm" action="">
<table>
<tr class="tableDataShade"><th class="terminalHdr" colspan="2" align="left">Enter the Make and Model and click submit:</th></tr>
<tr class="tableDataShade"><td>Make</td><td><input type="text" size="40" maxlength="20" name="make" data-validators="required"/></td></tr>
<tr class="tableDataShade"><td>Model</td><td><input type="text" size="40" maxlength="20" name="model" data-validators="required"/></td></tr>
<tr class="center"><td colspan="2"><input type="button" value="Save Changes" onclick="saveMachine()"/><input type="button" value="Cancel" onclick="cancelMachine();"/>
</td></tr>
</table>
</form></div>
</div>
