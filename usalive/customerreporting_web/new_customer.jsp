<%@page import="com.usatech.layers.common.fees.Fees.Fee"%>
<%@page import="com.usatech.layers.common.fees.*"%>
<%@page import="simple.servlet.InputForm"%>
<%@page import="com.usatech.usalive.servlet.UsaliveUser"%>
<%@page import="com.usatech.report.ReportingPrivilege"%>
<%@page import="com.usatech.report.custom.GenerateUtils,simple.servlet.RequestUtils,simple.text.StringUtils,simple.servlet.ResponseUtils,simple.text.MessageFormat,java.util.Collections"%>
<jsp:useBean id="licenses" type="simple.results.Results" scope="request" />
<% UsaliveUser user = (UsaliveUser)RequestUtils.getUser(request); 
   InputForm form = RequestUtils.getInputForm(request); %>
<form autocomplete="off" name="userForm" action="create_customer.i" method="post" onsubmit="removeSpace();App.updateClientTimestamp(this);">
<script src="/scripts/fees.js"></script>
<script type="text/javascript">
//<![CDATA[
	window.addEvent("domready", function() {
		new Form.Validator.Inline.Mask(document.userForm);
	});

	function removeSpace(){
		$('newUserName').value=$('newUserName').value.trim();
		$('newPassword').value=$('newPassword').value.trim();
		//$('newPasswordConfirm').value=$('newPasswordConfirm').value.trim();
		$('emailField').value=$('emailField').value.trim();
		$('customerNameField').value=$('customerNameField').value.trim(); 
	}
//]]>

	function onEmailChange(){
		$("newUserName").value = $("emailField").value;
	}
</script>
<div class="toggle-heading">
	<span class="caption">New Customer</span>
</div>
<p class="instructions">Please enter the information below to create a new customer and user account:</p>
<table class="fields">
  <tr class="tableHeader">
    <th colspan="3" align="left">User Information</th>
  </tr>
  <tr class="tableDataShade">
    <td>User Name (= Email)<span class="required">*</span></td>
    <td colspan="2"><input size="50" maxlength="255" type="text" id="newUserName" name="newUserName"  readonly="true" data-validators="required minLength:5" value='<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "newUserName", String.class, false))%>'/></td>
  </tr>
  <tr class="tableDataShade">
    <td>Enter a First Name<span class="required">*</span></td>
    <td colspan="2"><input size="50" maxlength="50" type="text" name="firstName" data-validators="required" value='<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "firstName", String.class, false))%>'/></td>
  </tr>
  <tr class="tableDataShade">
    <td>Enter a Last Name<span class="required">*</span></td>
    <td colspan="2"><input size="50" maxlength="50" type="text" name="lastName" data-validators="required" value='<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "lastName", String.class, false))%>'/></td>
  </tr>
  <tr class="tableDataShade" style="display:none">
    <td>Password<span class="required">*</span></td>
    <td><input size="50" maxlength="20" type="password" name="newPassword" id="newPassword" data-validators="required validate-password" value='<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "newPassword", String.class, false))%>'/></td>
    <td rowspan="2"><div class="requirementNote">Must be at least 8 characters and contain 1 uppercase letter, 1 lowercase letter, and 1 number or punctuation.</div></td>
  </tr>
  <tr class="tableDataShade">
    <td>Enter an Email<span class="required">*</span></td>
    <td colspan="2"><input size="50" maxlength="255" type="text" id="emailField" name="email" data-validators="required validate-email" onChange="onEmailChange();" value='<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "email", String.class, false))%>'/></td>
  </tr>
  <tr class="tableDataShade">
    <td>Enter a Phone<span class="required">*</span></td>
    <td colspan="2"><input size="50" maxlength="20" type="text" name="telephone" edit-mask="Fixed.PhoneUs" data-validators="required validate-digits minLength:10" value='<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "telephone", String.class, false))%>'/></td>
  </tr>
  <tr class="tableDataShade">
    <td>Enter a Fax</td>
    <td colspan="2"><input size="50" maxlength="20" type="text" name="fax" edit-mask="Fixed.PhoneUs" data-validators="validate-digits minLength:10" value='<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "fax", String.class, false))%>'/></td>
  </tr>
  <tr>
    <td colspan="3" class="spacer"></td>
  </tr>
  <tr class="tableHeader">
    <th colspan="3" align="left">Company Information</th>
  </tr>
  <tr class="tableDataShade">
    <td>Enter the Company Name<span class="required">*</span></td>
    <td colspan="2"><input size="50" maxlength="50" type="text" id="customerNameField" name="customerName" data-validators="required" value='<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "customerName", String.class, false))%>'/></td>
  </tr>
  <tr class="tableDataShade">
    <td>Enter the Company Billing Address<span class="required">*</span></td>
    <td colspan="2"><input size="50" maxlength="50" type="text" name="address1" data-validators="required" value='<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "address1", String.class, false))%>'/>
    <% GenerateUtils.writePostalTableElement(pageContext, null, RequestUtils.getAttribute(request, "postal", String.class, false), RequestUtils.getAttribute(request, "city", String.class, false), RequestUtils.getAttribute(request, "state", String.class, false), RequestUtils.getAttribute(request, "country", String.class, false), false, GenerateUtils.getCustomerCountries()); %>
    </td>
  </tr>
  <tr class="tableDataShade">
    <td>Sprout Vendor Id</td>
    <td><input size="20" maxlength="20" type="text" name="sproutVendorId" data-validators="validate-regex:'^([0-9])*$'" value='<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "sproutVendorId", String.class, false))%>'/></td>
    <td><div class="requirementNote">Leave blank if unknown</div></td>
    
  </tr>
  <tr>
    <td colspan="3" class="spacer"></td>
  </tr>
  <tr class="tableHeader">
    <th colspan="3" align="left">Credit Card Statement Information</th>
  </tr>
  <%if(user.isInternal()||(user.getMasterUser()!=null&&user.getMasterUser().isInternal())){ %>
  <tr class="tableDataShade">
    <td>Enter the Doing Business As</td>
    <td colspan="2"><input size="50" maxlength="21" type="text" id="customerDBAField" name="doingBusinessAs" value='<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "doingBusinessAs", String.class, false))%>'/></td>
  </tr>
  <%} %>
  <tr class="tableDataShade">
    <td>Enter the Customer Service Phone</td>
    <td colspan="1"><input size="50" maxlength="20" type="text" id="customerServicePhone" name="customerServicePhone" edit-mask="Fixed.PhoneUs" data-validators="validate-digits minLength:10" value='<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "customerServicePhone", String.class, false))%>'/></td>
    <td rowspan="2"><div class="requirementNote">Leave blank for USA Technologies customer service center.</div></td>
  </tr>
  <tr class="tableDataShade">
    <td>Enter the Customer Service Email</td>
    <td colspan="1"><input size="50" maxlength="70" type="text" id="customerServiceEmail" name="customerServiceEmail" data-validators="validate-email" value='<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "customerServiceEmail", String.class, false))%>'/></td>
  </tr>
  <% if (user.hasPrivilege(ReportingPrivilege.PRIV_CUSTOMER_SERVICE)) { %>
	<tr>
    <td colspan="3" class="spacer"></td>
  </tr>
  <tr class="tableHeader">
    <th colspan="3" align="left">License Information</th>
  </tr>
  <tr class="tableDataShade">
    <td>Select the License<span class="required">*</span></td>
    <td colspan="2"><% ResponseUtils.writeSelectElement(pageContext, "licenseId", licenses, new MessageFormat("{licenseId}"), new MessageFormat("{name}"), Collections.singletonMap("style", "width: 324px")); %></td>
  </tr>
  <% } %>
  <% if (user.isReseller()) {
     LicenseFees fees = LicenseFees.loadByCustomerId(user.getCustomerId());
     fees.updateFromForm(form, false); %>
	<tr>
    <td colspan="3" class="spacer"></td>
  </tr>
  <tr class="tableHeader">
    <th colspan="3" align="left">Fees</th>
  </tr>
  <tr class="tableDataShade">
    <td colspan="3">
     <% fees.writeHtmlTable(pageContext, true); %>
    </td>
  </tr>
<% } %>
  <tr>
    <td colspan="3" align="left"><span class="required">* Required Fields</span></td>
  </tr>
  <tr>
    <td colspan="3" class="spacer"></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><input type="submit" value="Submit"/></td>
  </tr>
</table>
</form>
