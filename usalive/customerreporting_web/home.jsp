<%@page import="java.util.regex.Pattern"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="simple.text.StringUtils"%>
<%@page import="simple.servlet.RequestUtils,com.usatech.usalive.servlet.UsaliveUser"%>
<jsp:useBean id="message" class="java.lang.String" scope="request" />
<jsp:useBean id="infoMessage" class="java.lang.String" scope="request" />
<%UsaliveUser user = RequestUtils.getAttribute(request, "simple.servlet.ServletUser", UsaliveUser.class, true, "session");%>
<div class="welcome">Welcome, <%=StringUtils.prepareCDATA(user.getFirstName()) %>!</div>
<table class="homepage" id="homeStat">
<tr><td align="center"><%
SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
Calendar cal = Calendar.getInstance();
String end = df.format(cal.getTime());
cal.add(Calendar.MONTH, -1);
String start = df.format(cal.getTime());
String sessionTokenEncoded = StringUtils.prepareScript(StringUtils.prepareURLPart(RequestUtils.getSessionTokenIfPresent(request)));
String userAgent = request.getHeader("user-agent");
int outputType;
if(userAgent != null && Pattern.compile("\\((Macintosh|iPhone|iPad|iPod); ").matcher(userAgent).find())
    outputType = 22;
else
	outputType = 27;
%>
<input type="button" onclick="document.location.href='activity_rollup_parameters.i?reportTitle=Sales+Rollup+Report&amp;outputType=<%=outputType%>&amp;params.beginDate=<%=start%>&amp;params.endDate=<%=end%>'" value="Sales Rollup Report"/>
<input type="button" onclick="document.location.href='run_report_async.i?folioId=1087&amp;save=false&amp;reportTitle=Device+Health+Report&amp;outputType=<%=outputType%>&amp;params.ShowAll=1&amp;session-token=<%=sessionTokenEncoded%>'" value="Full Device Health Report"/>
<input type="button" onclick="document.location.href='run_report_async.i?folioId=1087&amp;save=false&amp;reportTitle=Device+Exception+Report&amp;outputType=<%=outputType%>&amp;params.ShowAll=0&amp;session-token=<%=sessionTokenEncoded%>'" value="Device Exception Report"/>
<input type="button" onclick="document.location.href='run_report_async.i?folioId=453&amp;save=false&amp;reportTitle=Device+Details+Report&amp;outputType=<%=outputType%>&amp;session-token=<%=sessionTokenEncoded%>'" value="Device Details Report"/>
</td></tr>
<tr><td>
<div id="msg"><%=message%><br/><%=infoMessage%></div></td></tr>
</table>