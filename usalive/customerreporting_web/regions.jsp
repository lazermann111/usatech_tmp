<%@page import="simple.text.StringUtils"%>
<jsp:useBean id="terminalResults" type="simple.results.Results" scope="request" />
<div>
<%int i = 0;
while(terminalResults.next()) {
	if(terminalResults.isGroupBeginning(0)) {%>
		<script type="text/javascript">
	     var i;	
	     var allRegionNames = new Array();
	     var regionNameChanges=[];
	     var terminalChanges= new Array();
	     function addRegion() {  	    
			var newRegionBox = new Element("div", {class: "regionBox"});
			newRegionBox.adopt(new Element("p", {text: "Region:&#160;"});
			newRegionBox.adopt(new Element("input", {
				onchange: "regionNameChanged(this.id, this.value)", 
				type: "text", 
				size: "20", 
				maxlength: "50", 
				name: "region" + i, 
				id: "region" + i,
				value: "NEW REGION"});
			newRegionBox.adopt(new Element("input", {
				type: "hidden",
				name: "terminals" + i,
				id: "terminals" + i});

			newRegionBox.adopt(new Element("div", {id: "r" + i, class: "regionDiv"});
			newRegionBox.injectInside($("allRegions"));	
	      	addNewRegion('region'+i); 
	        i++;       
	     }
	     function regionNameChanged(regionId, regionValue){
	     	var regionOption = $('regionsSelect').getElements('option[value='+regionId+']');
	     	if(validRegionNameChange(regionValue)) {
	     		allRegionNames[regionId] = regionValue;
	     		regionOption.set("text",regionValue);
	     		regionNameChanges.include(regionId.substr(6));
	     	} else {
	     		$(regionId).value = allRegionNames[regionId];
	     	}
	     }
	     
	     function validRegionNameChange(regionValue){
	     	var selectMenu = $('regionsSelect');
	     	for(var j = 0; j < selectMenu.childNodes.length; j++) {
	     		if(selectMenu.childNodes[j].text == regionValue){
	     			alert("You cannot change region name to an existing region.");
	     			return false;
	     		}
	     	}
	     	return true;
	     }
	     function addNewRegion(regionName){
	     	var region1 = new Element('option');
	     	region1.value = regionName;
	     	region1.set("text","NEW REGION");
	     	region1.injectInside($('regionsSelect')); 
	     	allRegionNames[regionName]="NEW REGION";
	     }
	     
	     function selectTerminal(terminalId){	     	
	     	if($(terminalId).className == "terminalDiv") {
	     		$(terminalId).className = "terminalDivSel";
	     	} else {
	     		$(terminalId).className = "terminalDiv";
	     	}
	     }
	     
	     function moveTerminalsToRegion(){
	     	var regionNumber = $("regionsSelect").value.substr(6);
	     	var t = $("terminals" + regionNumber);
	    	
	    	var allTerminals=document.getElementsBySelector('.terminalDivSel');
	    	allTerminals.each(function(terminal){ 
	    	    terminalChanges[terminal.id] = regionNumber;
	    	    terminal.className = "terminalDiv";
	    	    terminal.injectInside($("r" + regionNumber));
	        });
	    	
	     }
	     function handleChange(){
	     	for(var terminal in terminalChanges) {
	     		if($type(terminalChanges[terminal]) == 'string'){
			     	var t = $("terminals" + terminalChanges[terminal]);
			     	if(t !=null) {
				     	if(t.value.length > 0){
				    		t.value += ",";
				    		t.value += terminal.substr(1);
				    	} else {
					        t.value = terminal.substr(1);
					    }
				    }
			    }
	     	}
	     	regionNameChanges.each(function(regionId){ 
	    		var t = $("terminals" + regionId);
		        var d = $("r" + regionId);
		        for(var j = 0; j < d.childNodes.length; j++) {
		        	if(d.childNodes[j].className == "terminalDiv" || d.childNodes[j].className == "terminalDivSel") {
		                	if(t.value.length > 0) {
	    						t.value += ",";
	    					} 	
		                    t.value += d.childNodes[j].getAttribute("id").substr(1);
		            }
		                 
		        }
	        });
	     }
	     
	     function setupDropDownRegions(){
	        var allRegions = document.getElements('input[name^=region]');
	        var regionsSelect = new Element('select');
	        regionsSelect.id = 'regionsSelect';
	        allRegions.each(function(aRegion){ 
	    	    var region1 = new Element('option');
		     	region1.value = aRegion.name;
		     	if(aRegion.value == ''){
		     		allRegionNames[aRegion.name] = 'UNASSIGNED';
		     		region1.set("text",'UNASSIGNED');
		     	}else{
		     		region1.set("text",aRegion.value);
		     		allRegionNames[aRegion.name] = aRegion.value;
		     	}
		     	
		     	region1.injectInside(regionsSelect);
	        });
	       
	     	regionsSelect.injectInside($("moveToRegion"));
	     }
	  	 window.addEvent('domready', setupDropDownRegions);
	</script>
	<style type="text/css">
.regionDiv {
	border: 2px inset white;
	position: relative;
	height: 100px;
	overflow: auto;
	padding: 3px;
	margin-bottom: 20px;
	cursor: default;
	width: 255px;
}

.regionDivSel {
	border: 2px inset white;
	position: relative;
	height: 100px;
	overflow: auto;
	padding: 3px;
	margin-bottom: 20px;
	cursor: default;
	background: #FFEEEE;
	width: 255px;
}

.terminalDiv {
	position: relative;
}

.terminalDivSel {
	position: relative;
	border: 1px solid yellow;
	background: #CCFFFF;
}

.regionBox {
	border: 2px solid #000060;
	position: relative;
	margin-bottom: 10px;
	margin-right: 10px;
	padding: 5px;
	float: left;
	width: 270px;
	height: 140px;
	overflow: hidden;
}

.regionBox P {
	color: #CC0000;
	margin-bottom: 4px;
	font-weight: bold;
}

.regionBox INPUT {
	color: #CC0000;
	font-weight: bold;
	top: 3px;
	position: relative;
}

.regionTitle {
	background: #DDDDDD;
}

.regionTitle TH {
	font-size: 20px;
}

.note {
	color: #8f8f8f;
	font-style: italic;
	font-weight: bold;
}
</style>
<form id="regionsForm" name="regionsForm" action="update_regions.i" method="POST" onsubmit="handleChange()">
<table><tr class="regionTitle"><th colspan="3">Regions</th></tr>
<tr><td>Using your mouse, select each terminal and click "Move terminals to the selected region" to move it into the region you select. Click the "Save Changes" button to make changes permanent.<br/>
<span class="note">Note: A region is only saved when it has one or more terminals assigned to it.</span></td>
<td>&#160;<input type="button" value="Add Region" onclick="addRegion()"/></td><td>&#160;<input type="submit" value="Save Changes"/></td>
</tr>
<tr><td colspan="3" id="moveToRegion">Move to Region:</td></tr>
<tr><td colspan="3">&#160;</td></tr>
<tr><td colspan="3" ><input type="button" value="Move terminals to the selected region" onclick="moveTerminalsToRegion()"/></td></tr>
<tr><td id="allRegions" colspan="3">
	<%
	}
	if(terminalResults.isGroupBeginning("regionName")) {
    	%><div class="regionBox"><p><%
        if(terminalResults.getValue("regionName") != null) { 
            %>Region:&#160;<input onchange="regionNameChanged(this.id, this.value)" id="region<%=i%>" type="text" size="25" maxlength="50" name="region<%=i%>" value="<%=StringUtils.encodeForHTMLAttribute(terminalResults.getFormattedValue("regionName"))%>"/><%
        } else {
            %>&lt;UNASSIGNED&gt;<input type="hidden" name="region<%=i%>" value=""/><%
        }%></p><input type="hidden" name="terminals<%=i%>" value="" id="terminals<%=i%>"><div id="r<%=i%>" class="regionDiv"><%
    }
    %><div id='t<%=terminalResults.getValue("terminalId", Number.class)%>' class="terminalDiv" onmousedown="selectTerminal(this.id)"><%
    Integer[] columnIndexes = terminalResults.getValue("columnIndexes", Integer[].class);
    for(int k = 0; k < columnIndexes.length; k++) {
        if(columnIndexes[k] != null) {
            if(k > 0) { %>&#160;&#160;&#160;<%}
            %><%=StringUtils.prepareHTML(terminalResults.getFormattedValue("data" + columnIndexes[i]))%><%
        }
    }
    %></div><%
    if(terminalResults.isGroupEnding("regionName")) {
        i++;%></div></div><%
        if(terminalResults.isGroupEnding(0)) {
        	%></table><script type="text/javascript" defer="defer">i = <%=i%>;</script></form><%
        }
    }
}
if(terminalResults.getRow() <= 1) { 
	%><span class="no-terminals">No terminals found</span><%
} %>
</div>
