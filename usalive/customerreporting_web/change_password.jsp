<%@page import="simple.servlet.RequestUtils,simple.text.StringUtils"%>
<form name="info" method="post" action="change_password.i" onsubmit="return validateForm();" autocomplete="off">
<script type="text/javascript">
	function validateForm() {
	    var passwordElement = $("password");
	    var password = passwordElement.value;
	    if(password != $("confirm").value) {
	        alert("The password and its confirmation do not match. Please re-type both.");
	        passwordElement.focus();
	        return false;
	    }
	    var errorMsg = "";
	    if(password.length &lt; 8) {
	    	errorMsg = "must be at least 8 digits";
	    }
	    if(!password.match(/[A-Z]/)) {
	    	if(errorMsg != "") errorMsg = errorMsg + " and ";          	
			errorMsg = errorMsg + "must contain at least 1 uppercase letter";
	    }
		if(!password.match(/[a-z]/)) {
		    if(errorMsg != "") errorMsg = errorMsg + " and ";          	
			errorMsg = errorMsg + "must contain at least 1 lowercase letter";
		}
		if(!password.match(/[!-@\[-^`{-~]/)) {
		    if(errorMsg != "") errorMsg = errorMsg + " and ";          	
			errorMsg = errorMsg + "must contain at least 1 number or 1 punctuation symbol";
		}
		if(errorMsg != "") {
	    	alert("The password " + errorMsg);
	    	passwordElement.focus();
            return false;
        }
        return true;
    }
</script>  
<input type="hidden" name="username" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "username", String.class, true))%>"/>
<input type="hidden" name="passcode" value="<%=StringUtils.prepareCDATA(RequestUtils.getAttribute(request, "passcode", String.class, true))%>"/>
<%
String errorMessage = RequestUtils.getAttribute(request, "onloadMessage", String.class, false);
if(errorMessage != null && errorMessage.trim().length() > 0) { 
%><div class="errorText"><%=StringUtils.prepareHTML(errorMessage)%></div><% 
}%><p class="loginInstructs">Please enter a new password for Member Id '<%=StringUtils.prepareHTML(RequestUtils.getAttribute(request, "username", String.class, true))%>':</p>
<table class="loginLayout">
    <tr><th>New Password:&#160;</th><td><input maxlength="20" type="password" name="editPassword" id="password"/></td>   
		<td rowspan="2"><div class="requirementNote">Must be at least 8 characters and contain 1 uppercase letter, 1 lowercase letter, and 1 number or punctuation.</div></td>
	</tr>
	<tr><th>Confirm Password:&#160;</th><td><input maxlength="20" type="password" name="editConfirm" id="confirm"/></td></tr>   
	 <tr class="loginButtonRow"><td colspan="3"><input type="submit" tabindex="0" value="Submit"/></td></tr>
</table>
</form>
