<%@page import="java.text.SimpleDateFormat"%>
<%@page import="simple.text.StringUtils,simple.servlet.RequestUtils,simple.bean.ConvertUtils"%>
<%
	SimpleDateFormat campaignFormat = new java.text.SimpleDateFormat("MM/dd/yyyy");
	String campaignScope = ConvertUtils.getString(RequestUtils.getAttribute(request, "campaignScope", String.class, false), "");
	String mode = ConvertUtils.getString(RequestUtils.getAttribute(request, "mode", false), "view");
	int selectedCampaignId = ConvertUtils.getInt(RequestUtils.getAttribute(request, "campaignId", true));
	char cardType = ConvertUtils.getChar(RequestUtils.getAttribute(request, "cardType", true));
	String backToManageCampaignLink=null;
	if(campaignScope.equals("[Global]")){
		backToManageCampaignLink="manage_campaign_internal.i";
	}else{
		backToManageCampaignLink="manage_campaign.i";
	}
%>
<jsp:useBean id="campaignResults" type="simple.results.Results" scope="request" />
<form name="campaignCardsForm" id="campaignCardsForm">
<input type="hidden" id="redirectHome" name="redirectHome" value="true"/>
<input type="hidden" name="campaignScope" id="campaignScope" value="<%=StringUtils.prepareCDATA(campaignScope)%>"/>
<input type="hidden" name="session-token" value="<%=RequestUtils.getSessionToken(request)%>" />
<script type="text/javascript">

function backToManageCampaign(){
	location.href="<%=backToManageCampaignLink%>?campaignId=<%=selectedCampaignId%>";
}
function checkStartEndCardId(startCardId, endCardId){
	if(startCardId){
		if(!endCardId){
			alert("Please enter Card Range End Card ID");
			return false;
		}else{
			if(startCardId > endCardId){
				alert("Please enter valid Card Range Start Card ID and Card Range End Card ID");
				return false;
			}else{
				return true;
			}
		}
	}else{
		if(endCardId){
			alert("Please enter Card Range Start Card ID");
			return false;
		}else{
			return true;
		}
	}
}
function onSearchCards(){
	$("message").set("html","");
	if($("campaignCardsForm").validate()){
		if(!checkStartEndCardId($("startCardId").value, $("endCardId").value)){
			return;
		}
		$("waitImage").style.display='block';
		new Request.HTML({ 
	    	url: "search_campaign_cards.i",
	    	data: $("campaignCardsForm"), 
	    	method: 'post',
	    	update : $("campaignCardsSection"),
	    	onComplete : function() {
	    		$("waitImage").style.display='none';
	    		$("campaignCardsSectionButton").style.display='block';
	    	},
			evalScripts: true
		}).send();
	}
}

function onSearchCardsByCampaign(){
	$("message").set("html","");
	if($("campaignCardsForm").validate()){
		if(!checkStartEndCardId($("startCardId").value, $("endCardId").value)){
			return;
		}
		$("waitImage").style.display='block';
		new Request.HTML({ 
	    	url: "search_campaign_cards_by_campaign.i",
	    	data: $("campaignCardsForm"), 
	    	method: 'post',
	    	update : $("campaignCardsSection"),
	    	onComplete : function() {
	    		$("waitImage").style.display='none';
	    		$("campaignCardsSectionButton").style.display='block';
	    	},
			evalScripts: true
		}).send();
	}
}

function onSearchCustomer(){
	$("message").set("html","");
	if($("campaignCardsForm").validate()){
		$("waitImage").style.display='block';
		new Request.HTML({ 
	    	url: "search_customer.i",
	    	data: $("campaignCardsForm"), 
	    	method: 'post',
	    	update : $("customerSelect"),
	    	onComplete : function() {
	    		$("waitImage").style.display='none';
	    	},
			evalScripts: true
		}).send();
	}
}

function selectAnotherCampaign(){
	$("message").set("html","");
	$("campaignCardsSectionButton").style.display='none';
	$("campaignCardsSection").set("html", "");
}

function onCancel(){
	$("message").set("html","");
	$('campaignCardsSection').getElements("input[type=checkbox])").each(function(el) {
		if($(el).get("campaignId") > 0){
			el.checked=true;
		}else{
			el.checked=false;
		}
	});
}

function onAddSelected(){
	$("message").set("html","");
	new Request.HTML({ 
    	url: "add_cards_to_campaign.i",
    	data: $("campaignCardsForm"), 
    	method: 'post',
		evalScripts: true
	}).send();
}

function onRemoveSelected(){
	$("message").set("html","");
	new Request.HTML({ 
    	url: "remove_cards_from_campaign.i",
    	data: $("campaignCardsForm"), 
    	method: 'post',
		evalScripts: true
	}).send();
}

function addOrRemoveSelectedResult(actionType, status, errorMsg){
	$("waitImage").style.display='none';
	if(status>0){
		if(actionType=="add"){
			$("message").set("html","<p class='status-info-success'>Successfully added the selected to the campaign.</p>");
			$('campaignCardsSection').getElements("input[type=checkbox])").each(function(el) {
				if(el.checked){
					$(el).set("campaignId", $("campaignId").value);
				}
			});
		}else{
			$("message").set("html","<p class='status-info-success'>Successfully removed the selected from the campaign.</p>");
			$('campaignCardsSection').getElements("input[type=checkbox])").each(function(el) {
				if(el.checked){
					$(el).set("campaignId", 0);
				}
			});
		}
	}else{
		if(actionType=="add"){
			$("message").set("html","<p class='status-info-failure'>Failed to add the cards to the campaign. "+errorMsg+" Please try again.</p>");
		}else{
			$("message").set("html","<p class='status-info-failure'>Failed to remove the cards from the campaign. "+errorMsg+" Please try again.</p>");
		}
	}
}

window.addEvent('domready', function() { 
	var select = $("campaignId");
	Form.attachOnSelect(select);
	new Form.Validator.Inline.Mask(document.campaignCardsForm);
});

function onSelectAllCheckbox(){
	$("message").set("html","");
	if($("selectAllCheckbox").checked){
		$('campaignCardsSection').getElements("input[type=checkbox])").each(function(el) {
			el.checked=true;
		});
	}else{
		$('campaignCardsSection').getElements("input[type=checkbox])").each(function(el) {
			el.checked=false;
		});
	}
}

function onSelectAllCustomerButton(){
	$('customerIds').getElements("option").each(function(el) {
		el.selected="selected";
	});
}

function onUnSelectAllCustomerButton(){
	$('customerIds').getElements("option").each(function(el) {
		el.selected="";
	});
}

</script>
<div id="waitImage" style="display:none">Please wait...<img alt="Please wait" src="images/pleasewait.gif"/></div>
<div id="message"></div>
<span class="caption">Manage Campaign Cards</span>
<table class="selectBody">
	<tr>
    <td valign="top">
    <div class="selectSection">
			<div class="sectionTitle" id="searchCardsTitle" >Search Cards</div>
				<div class="sectionRow">
	<input type="hidden" name="cardType" id="cardType" value="<%=cardType%>" />		
    <table class="customerInfo">
    	<tr><td width="20%">Campaign Name: </td><td><select name="campaignId" id="campaignId"><%
        while(campaignResults.next()) {%><option onselect="selectAnotherCampaign();" value="<%=StringUtils.prepareCDATA(campaignResults.getFormattedValue("campaignId"))%>" 
        <%if(selectedCampaignId==ConvertUtils.getInt(campaignResults.get("campaignId"))){%> selected="selected"<%}%>
        >
        <%=StringUtils.encodeForHTML(campaignResults.getFormattedValue("campaignName"))%>
       	</option><%
		}%>
        </select>
        </td></tr>
        <tr><td>Card Range Start Card ID:</td><td><input name="startCardId" id="startCardId" type="text" data-validators="validate-integer" /></td></tr>
        <tr><td>Card Range End Card ID:</td><td><input name="endCardId" id="endCardId" type="text" data-validators="validate-integer" /></td></tr>
        <% if(campaignScope.equals("[Global]")){ %>
        <tr><td></td></tr>
        <%} %>
    </table>
	<table>
    <tr>
    <td>
    	<input type="button" id="searchCardsButton" value="Search All Cards" onclick="onSearchCards();"/>
    	<input type="button" id="searchCardsButton" value="Search Assigned Cards" onclick="onSearchCardsByCampaign();"/>
    	<input type="button" value="Back To Manage Campaign" onclick="backToManageCampaign();"/>
    </td>
    </tr>
    </table>
    </div></div>
    </td>
    <% if(campaignScope.equals("[Global]")){ %>
    <td valign="top">
		<div class="selectSection">
			<div class="sectionTitle">Select customers to filter card search results</div>
				<div class="sectionRow">
					<table>
						<tr><td>Customer Name:<input name="customerName" id="customerName" type="text" /><input id="searchCustomer" type="button" onclick="onSearchCustomer();" value="Search Customer"/></td></tr>
        				<tr><td>
        					<div id="customerSelect">
        					<select multiple="true" size="4" name="customerIds" id="customerIds"  style="width: 100%;"></select>
        					</div>
        					</td>
      					</tr>
    				</table>
    				<table>
      					<tr>
      						<td><input type="button" id="selectAllCustomerButton" value="Select All Customer" onclick="onSelectAllCustomerButton();"/></td>
      						<td><input type="button" id="unselectAllCustomerButton" value="Unselect All Customer" onclick="onUnSelectAllCustomerButton();"/></td>
      					</tr>
    				</table>
    			</div>
    	</div>
    </td>
    <%} %>
    </tr>
</table>
<div id="campaignCardsSectionButton" style="display:none;">
	<input id="addSelect" type="button" onclick="onAddSelected();" value="Add selected" <% if (mode.equals("view")) {%> disabled="true"<%} %>/>
	<input id="cancelSelect" type="button" onclick="onCancel();" value="Select current campaign cards" <% if (mode.equals("view")) {%> disabled="true"<%} %>/>
	<input id="removeSelect" type="button" onclick="onRemoveSelected();" value="Remove selected" <% if (mode.equals("view")) {%> disabled="true"<%} %>/>
</div>
<div id="campaignCardsSection" class="cardSearchResult"></div>

</form>
