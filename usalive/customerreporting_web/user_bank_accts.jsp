<%@page import="simple.text.StringUtils,simple.servlet.RequestUtils"%>
<jsp:useBean id="bankAcctResults" type="simple.results.Results" scope="request" />
<%
long editUserId = RequestUtils.getAttribute(request, "editUserId", Long.class, true); 
String editUserName = RequestUtils.getAttribute(request, "editUsername", String.class, false); 
%>
<div class="toggle-heading">
	<span class="caption">User Bank Accounts</span>
</div>
<form name="criteria" method="post" action="update_user_bank_acct.i" onsubmit="submitForm(this); return false;" class="fillVerticalSpace">
<script type="text/javascript" defer="defer">
	var request;
	var privChanges = {};
	window.addEvent("domready", function() {
		request = new Request.HTML({
			url: "update_user_bank_acct.i",
			method: "post",
			async: false,
			update: $("messageInfo")
		});
		if(Browser.ie || Browser.opera) {
			var div = $("scrollParent1");
			var container = $("content");
			var updateDimensions = function(event) {
				div.setStyle("height", (container.getSize().y - div.getCoordinates(container).top) + "px");
			};
		
			window.addEvent("resize",updateDimensions);
			window.addEvent("scroll",updateDimensions);
			div.setStyle("height", (container.getSize().y - div.getParent("tr").getPrevious().getCoordinates(container).bottom) + "px");
		}
	});
	
	function submitForm(frm) {
		var query = "privProfileId=<%=editUserId%>&privUsername=<%=StringUtils.prepareURLPart(editUserName)%>&fragment=true";
		Object.each(privChanges, function(item, index) {
			if(item == "E")
				query += "&edit=" + index;
			else if(item == "V")
				query += "&view=" + index;
			else if(item == "-")
				query += "&none=" + index;
		});
		request.send({data: query});
		privChanges = {};		
	}
	function handlePrivChange(inp) {
		if(inp.type == "checkbox" && !inp.disabled) {
			var newPriv = null;
			if(inp.checked) {
				if(inp.name == "edit") {
					newPriv = "E";
					var inpv = $("v" + inp.value);
					inpv.checked = true;
					inpv.disabled = true;
				} else if(inp.name == "view")
					newPriv = "V";
			} else {
				newPriv = "-";
				if(inp.name == "edit") {
					var inpv = $("v" + inp.value);
					inpv.checked = false;
					inpv.disabled = false;
				}
			}
			if(newPriv != null)
				privChanges[inp.value] = newPriv;
		}
	}
	function toggleViewAll(button) {
        if(button.ison) {
            button.ison = false;
            button.value = "View All";
        } else {
            button.ison = true;
            button.value = "View None";
        }
        $(button.form).getElements('input[type=checkbox]').each(function(el) {
        	el.checked = button.ison;
        });
    }
</script>
	<table class="fillVerticalSpace">
	<tr><td colspan="2"><div id="messageInfo"></div></td></tr>
	<tr><td><p class="instruct"><%=StringUtils.prepareCDATA(RequestUtils.getTranslator(request).translate("user-bank-acct-list-prompt", "Please select for which Bank Accounts '" + editUserName + "' can view payment information:", editUserName)) %></p></td>
	<td><input type="submit" value="Update"/></td></tr>
	<tr class="fillVerticalSpace"><td>
		<div class="listEditPane" id="scrollParent1"><div class="listEditScroll">
		<table width="100%">
				<tr class="tableHeader">
					<th>View</th>
					<th>Bank Name</th>
					<th>Bank ID</th>
					<th>Account Number</th>
					<th>Customer Name</th>
				</tr>
				<% while(bankAcctResults.next()) { 
   				%><tr class="checklist">
					<td><input type="checkbox" value='<%=bankAcctResults.getValue("bankId", Long.class)%>' name="view"
						<% if(!"-".equalsIgnoreCase(bankAcctResults.getFormattedValue("allowEdit"))) {%>checked="checked"<%}%>
						onclick="handlePrivChange(this)"/>
					</td>
					<td><%=StringUtils.prepareCDATA(bankAcctResults.getFormattedValue("bankName"))%></td>
					<td><%=StringUtils.prepareCDATA(bankAcctResults.getFormattedValue("bankId"))%></td>
					<td><%=StringUtils.prepareCDATA(bankAcctResults.getFormattedValue("bankAcctNbr"))%></td>
					<td><%=StringUtils.prepareCDATA(bankAcctResults.getFormattedValue("customerName"))%></td>
				</tr>
				<% } %>
			</table>
			</div></div>
		</td>
		<td valign="top">
			<div><input type="button" value="View All" onclick="toggleViewAll(this)"/></div>
		</td>
	</tr>
	</table>
</form>