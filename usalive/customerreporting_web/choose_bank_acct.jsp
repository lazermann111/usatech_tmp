<%@page import="simple.text.StringUtils"%>
<jsp:useBean id="results" type="simple.results.Results" scope="request" />
<jsp:useBean id="instructs" class="java.lang.String" scope="request" />
<jsp:useBean id="forwardTo" class="java.lang.String" scope="request" />
<%
String prefix;
if(forwardTo.indexOf('?') >= 0)
	prefix = forwardTo + '&';
else
	prefix = forwardTo +'?';
%>
<span class="caption">EFT Auth Form</span>
<div>
<p class="instructions"><%=StringUtils.prepareHTML(instructs.trim().length() > 0 ? instructs : "Please select a bank account")%>:</p>
<table>
<tr class="tableHeader"><th>Bank Name</th><th>Account Number</th></tr><%
while(results.next()) {
    %><tr><td><a href='<%=prefix%>bankAcctId=<%=results.getValue("bankAcctId", Long.class)%>' onmouseover="window.status = 'Select Bank Account'; return true;" onmouseout="window.status = ''">
    <%=StringUtils.encodeForHTML(results.getFormattedValue("bankName"))%></a></td>
    <td><a href='<%=prefix%>bankAcctId=<%=results.getValue("bankAcctId", Long.class)%>' onmouseover="window.status = 'Select Bank Account'; return true;" onmouseout="window.status = ''">
    <%=StringUtils.prepareHTML(results.getFormattedValue("accountNumber"))%></a></td></tr><%
}%></table>
</div>