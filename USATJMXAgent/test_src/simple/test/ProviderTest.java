/**
 *
 */
package simple.test;

import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.util.Arrays;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.KeyManagerFactorySpi;

import com.sun.org.apache.bcel.internal.generic.ObjectType;
import com.sun.org.apache.bcel.internal.generic.Type;
import com.sun.org.apache.bcel.internal.util.BCELifier;

import simple.security.provider.SimpleProvider;
import simple.security.provider.SimpleProvider.ServiceSpec;
import simple.security.provider.ssl.DelegatingKeyManagerFactoryImpl;

/**
 * @author Brian S. Krug
 *
 */
public class ProviderTest {

	/**
	 * @param args
	 * @throws NoSuchAlgorithmException
	 */
	public static void main(String[] args) throws Exception {
		testProvider();
		/*
		inspectSelf();
		Class<?> testClass = testClassGen() ;
		System.out.println("Got class '" + testClass + "'");
		Object kmf = testClass.newInstance();
		System.out.println("Created Instance '" + kmf + "'");
		*/
	}
	protected static void inspectSelf() throws Exception {
		BCELifier._main(new String[] {"C:\\Documents and Settings\\bkrug\\My Documents\\SoftwareDevelopment\\Java Projects\\USATJMXAgent\\bin\\simple\\test\\ProviderTest.class"});
	}
	protected static void testProvider() throws Exception {
		ServiceSpec<?>[] SERVICE_SPECS = new ServiceSpec<?>[] {
				new ServiceSpec<KeyManagerFactorySpi>("KeyManagerFactory", "SunX509", KeyManagerFactorySpi.class, DelegatingKeyManagerFactoryImpl.class),
				new ServiceSpec<KeyManagerFactorySpi>("KeyManagerFactory", "NewSunX509", KeyManagerFactorySpi.class, DelegatingKeyManagerFactoryImpl.class),
				new ServiceSpec<KeyManagerFactorySpi>("KeyManagerFactory", "X509", KeyManagerFactorySpi.class, DelegatingKeyManagerFactoryImpl.class),
		};

		String specFile = "simple/security/provider/ServiceSpecs.lst";
		Security.insertProviderAt(SimpleProvider.createProvider("KMF2", specFile), 1);
		KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
		KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
		char[] password = "usatech".toCharArray();
		ks.load(new FileInputStream("C:\\Documents and Settings\\bkrug\\My Documents\\devapr01-keystore.ks"), password);
		kmf.init(ks, password);
		KeyManager[] kms = kmf.getKeyManagers();
		System.out.println("Found key managers: " + Arrays.toString(kms));
	}


	protected static final Type CLASS_TYPE = new ObjectType(Class.class.getName());

	/*protected static class DynamicClassLoader extends ClassLoader {
		public Class<?> defineClass(ClassGen classGen) {
			byte[] bytes = classGen.getJavaClass().getBytes();
			return defineClass(classGen.getClassName(), bytes, 0, bytes.length);
		}
	}
	protected static final DynamicClassLoader dynamicClassLoader = new DynamicClassLoader();*/

	protected static void preview() {
		Class<?> abc = KeyManagerFactorySpi.class;
		System.out.println("GOT CLASS: " + abc);
	}
}
