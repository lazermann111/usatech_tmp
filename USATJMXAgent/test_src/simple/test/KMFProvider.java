/**
 *
 */
package simple.test;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.net.ssl.KeyManagerFactorySpi;

import simple.security.provider.ssl.DelegatingKeyManagerFactoryImpl;
import sun.security.jca.GetInstance;
import sun.security.jca.Providers;

/**
 * @author Brian S. Krug
 *
 */
public class KMFProvider extends java.security.Provider {
	private static final long serialVersionUID = 1418403312833761937L;
	protected static final String serviceType = "KeyManagerFactory";

	public static class SunX509 extends DelegatingKeyManagerFactoryImpl {
		public SunX509() throws NoSuchAlgorithmException {
			super(getDelegate(KeyManagerFactorySpi.class, serviceType, "SunX509"));
		}
	}
	public static class X509 extends DelegatingKeyManagerFactoryImpl {
		public X509() throws NoSuchAlgorithmException {
			super(getDelegate(KeyManagerFactorySpi.class, serviceType, "X509"));
		}
	}
	public KMFProvider() {
		super("Simple", 1.0, "Simple Provider (KeyManagerFactory)");
		register();
	}

	protected void register() {
		put(serviceType + ".SunX509", SunX509.class.getName());
		put(serviceType + ".NewSunX509", X509.class.getName());
	}

	protected static <C> C getDelegate(Class<C> clazz, String type, String algorithm) throws NoSuchAlgorithmException, ClassCastException {
		List<java.security.Provider> providers = Providers.getProviderList().providers();
		int i = 0;
		for(;i < providers.size(); i++) {
			if(providers.get(i).getClass().equals(KMFProvider.class))
				break;
		}
		if(i >= providers.size())
			throw new NoSuchAlgorithmException("This provider is not in the provider list");
		for(i++; i < providers.size(); i++) {
			Service service = providers.get(i).getService(type, algorithm);
			if(service != null) {
				return clazz.cast(GetInstance.getInstance(service, clazz).impl);
			}
		}
		throw new NoSuchAlgorithmException("No other providers implementing '" + algorithm + "' were found after this one");
	}
}
