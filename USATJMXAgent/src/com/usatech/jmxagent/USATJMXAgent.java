package com.usatech.jmxagent;

import java.security.Security;

import simple.security.provider.SimpleProvider;

/**
 * Two fixed port is used for JMX RMI.
 * USAGE:
 * add switch -javaagent:usat-jmx-agent-1.0.0.jar to java command
 * 
 * System properties can be set:
 * jmx.remote.x.rmiRegistryPort
 * jmx.remote.x.rmiServerPort
 * 
 * The following can be configured to do user authentication.
 * jmx.remote.x.login.config
 * jmx.remote.x.access.file
 * 
 * @author yhe
 * @author bkrug
 * 
 */
public class USATJMXAgent {
	public static void premain(String args) throws Exception {
		Package p = USATJMXAgent.class.getPackage();
		if(p != null) {
			String version = p.getImplementationVersion();
			if(version != null)
				System.out.println("Starting up application with USAT JMX Agent v. " + version);
			else
				System.out.println("Starting up application with USAT JMX Agent (Version not provided)");
		} else
			System.out.println("Starting up application with USAT JMX Agent (Package not available)");
		System.out.println("Classpath: " + System.getProperty("java.class.path"));
		// first add SimpleProvider
		boolean enforceCipherSuitePreference = !"false".equalsIgnoreCase(SimpleProvider.getProperties().getProperty("enforceCipherSuitePreference"));
		if((args != null && (args = args.trim()).length() > 0) || enforceCipherSuitePreference) {
			SimpleProvider provider = SimpleProvider.createProvider("SimpleDelegates");
			if(args.length() > 0)
				provider.registerServiceSpecs(args);
			Security.insertProviderAt(provider, 1);
			if(enforceCipherSuitePreference)
				provider.enforceCipherSuitePreference();
			System.out.println("Installed SimpleProvider");
		}
    }
}
