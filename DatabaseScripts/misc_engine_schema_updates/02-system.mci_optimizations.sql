-- drop unnecessary triggers
DROP TRIGGER engine.TIA_MACHINE_CMD_INBOUND;
DROP TRIGGER engine.TUA_MACHINE_CMD_INBOUND;

-- Start of DDL Script for Trigger ENGINE.TRBI_MACHINE_CMD_INBOUND
-- Generated 14-Mar-2006 17:57:22 from ENGINE@USADBD02.USATECH.COM

-- Drop the old instance of TRBI_MACHINE_CMD_INBOUND
DROP TRIGGER engine.trbi_machine_cmd_inbound
/

CREATE OR REPLACE TRIGGER engine.trbi_machine_cmd_inbound
 BEFORE
  INSERT
 ON engine.machine_cmd_inbound
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
	IF (:NEW.inbound_id IS NULL) THEN
		SELECT seq_machine_cmd_inbound_id.NEXTVAL
		INTO :NEW.inbound_id
		FROM DUAL;
	END IF;

	IF (:NEW.inbound_date IS NULL) THEN
		:NEW.inbound_date := SYSDATE;
	END IF;

	:NEW.num_times_executed := 0;

	-- Execute_CD will be null unless it is populated with an 'S'
	IF (:NEW.execute_cd IS NULL) THEN
		:NEW.execute_cd := 'N';
	END IF;

	:NEW.original_execute_cd := :NEW.execute_cd;
	:NEW.insert_ts := SYSDATE;
	:NEW.logic_engine_id := 0;

	-- logic distributed design engine choice
	IF (:NEW.session_id IS NULL) THEN
		BEGIN
			SELECT MAX(session_id)
			INTO :NEW.session_id
			FROM net_layer_device_session
            WHERE device_name = :NEW.machine_id;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				:NEW.session_id := NULL;
		END;
	END IF;
    BEGIN
        SELECT sf_select_best_logic_engine(:NEW.session_id)
        INTO :NEW.logic_engine_id
        FROM dual;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            :NEW.logic_engine_id := 0;
    END;

	-- logic distributed design stats update
	UPDATE logic_engine_stats
	SET mci_last_row_cnt_activity_ts = SYSDATE,
	    mci_last_row_cnt = mci_last_row_cnt + 1,
	    mci_consecutive_growth_ts = SYSDATE,
		mci_consecutive_growth_cnt = mci_consecutive_growth_cnt + 1
	WHERE logic_engine_id = :NEW.logic_engine_id;
END;
/


-- End of DDL Script for Trigger ENGINE.TRBI_MACHINE_CMD_INBOUND

-- Start of DDL Script for Trigger ENGINE.TRBU_MACHINE_CMD_INBOUND
-- Generated 14-Mar-2006 17:57:22 from ENGINE@USADBD02.USATECH.COM

-- Drop the old instance of TRBU_MACHINE_CMD_INBOUND
DROP TRIGGER engine.trbu_machine_cmd_inbound
/

CREATE OR REPLACE TRIGGER engine.trbu_machine_cmd_inbound
 BEFORE
  UPDATE
 ON engine.machine_cmd_inbound
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   :NEW.update_ts := SYSDATE;
   :NEW.execute_date := SYSDATE;

   /* Only allow 10 tries to execute a command */
   IF :NEW.num_times_executed > 10 THEN
      :NEW.execute_cd := 'X';
   END IF;

	/* logic distributed design stats update */
	-- message being processed by engine: update engine activity stats
    IF ((:OLD.execute_cd = 'N' and :NEW.execute_cd IN ('N', 'Q', 'F'))
            OR (:OLD.execute_cd = 'Q' and :NEW.execute_cd IN ('Q', 'F'))
    		OR (:OLD.execute_cd = 'F' and :NEW.execute_cd = 'F')) THEN
		UPDATE logic_engine_stats
		SET engine_last_activity_ts = SYSDATE
		WHERE logic_engine_id = :OLD.logic_engine_id;
	
	-- message processed: reset growth and row counts; update engine activity stats
	ELSIF (:OLD.execute_cd IN ('N', 'F', 'Q') and :NEW.execute_cd IN ('Y', 'E')) THEN
		UPDATE logic_engine_stats
		SET mci_last_row_cnt_activity_ts = SYSDATE,
			mci_last_row_cnt = mci_last_row_cnt - 1,
			mci_consecutive_growth_cnt = 0,
			engine_last_activity_ts = SYSDATE,
			engine_session_processed_msgs = engine_session_processed_msgs + 1
		WHERE logic_engine_id = :OLD.logic_engine_id;
	
	-- purge rows: reset growth and row counts; no change to engine activity stats
    ELSIF (:OLD.execute_cd IN ('N', 'F', 'Q') AND :NEW.execute_cd NOT IN ('N', 'F', 'Q')) THEN
		UPDATE logic_engine_stats
		SET mci_last_row_cnt_activity_ts = SYSDATE,
			mci_last_row_cnt = mci_last_row_cnt - 1,
			mci_consecutive_growth_cnt = 0
		WHERE logic_engine_id = :OLD.logic_engine_id;

    END IF;

END;
/


-- End of DDL Script for Trigger ENGINE.TRBU_MACHINE_CMD_INBOUND
