-- Start of DDL Script for Trigger ENGINE.TUA_MACHINE_CMD_INBOUND
-- Generated 1/26/2006 5:02:46 PM from ENGINE@USADBD02.USATECH.COM

CREATE OR REPLACE TRIGGER engine.tua_machine_cmd_inbound
 AFTER
  UPDATE
 ON engine.machine_cmd_inbound
REFERENCING NEW AS NEW OLD AS OLD
BEGIN
	DECLARE
		n_logic_engine_id logic_engine.logic_engine_id%TYPE;
		n_cur_row_cnt NUMBER(6,0);
		CURSOR c_logic_engine_info IS
			SELECT le.logic_engine_id, NVL(COUNT(mci.logic_engine_id), 0)
			INTO n_logic_engine_id, n_cur_row_cnt
			FROM machine_cmd_inbound mci, logic_engine le
			WHERE le.logic_engine_id = mci.logic_engine_id(+)
			GROUP BY le.logic_engine_id
			ORDER BY le.logic_engine_id;
	BEGIN
		-- logic distributed design stats update (only mci row cnt)
		OPEN c_logic_engine_info;
		LOOP
			FETCH c_logic_engine_info INTO n_logic_engine_id, n_cur_row_cnt;
			EXIT WHEN c_logic_engine_info%NOTFOUND;

			UPDATE logic_engine_stats
			SET mci_last_row_cnt = n_cur_row_cnt
			WHERE logic_engine_id = n_logic_engine_id;
		END LOOP;
		CLOSE c_logic_engine_info;
	END;
END;
/


-- End of DDL Script for Trigger ENGINE.TUA_MACHINE_CMD_INBOUND


-- Start of DDL Script for Trigger ENGINE.TIA_MACHINE_CMD_INBOUND
-- Generated 6-Feb-2006 10:27:59 from ENGINE@USADBP.USATECH.COM

CREATE OR REPLACE TRIGGER engine.tia_machine_cmd_inbound
 AFTER
  INSERT
 ON engine.machine_cmd_inbound
REFERENCING NEW AS NEW OLD AS OLD
BEGIN
    DECLARE
        n_logic_engine_id logic_engine.logic_engine_id%TYPE;
        n_cur_row_cnt NUMBER(6,0);
        CURSOR c_logic_engine_info IS
			SELECT le.logic_engine_id, NVL(COUNT(mci.logic_engine_id), 0)
			INTO n_logic_engine_id, n_cur_row_cnt
			FROM machine_cmd_inbound mci, logic_engine le
			WHERE le.logic_engine_id = mci.logic_engine_id(+)
			GROUP BY le.logic_engine_id
			ORDER BY le.logic_engine_id;
    BEGIN
        -- logic distributed design stats update
        OPEN c_logic_engine_info;
        LOOP
            FETCH c_logic_engine_info INTO n_logic_engine_id, n_cur_row_cnt;
            EXIT WHEN c_logic_engine_info%NOTFOUND;

        	UPDATE logic_engine_stats
        	SET mci_last_row_cnt = n_cur_row_cnt,
                mci_consecutive_growth_ts = DECODE( --(d1 - d2) - ABS(d1 - d2) ==> 0=d1>=d2, !0=d1<d2
					(mci_last_row_cnt - n_cur_row_cnt) - ABS(mci_last_row_cnt - n_cur_row_cnt),
					0,
					mci_consecutive_growth_ts,	--mci_last_row_cnt >= n_cur_row_cnt
					SYSDATE --mci_last_row_cnt < n_cur_row_cnt
				)
                /*mci_consecutive_growth_cnt = DECODE(
                    mci_last_row_cnt - n_cur_row_cnt,
                    0,
                    mci_consecutive_growth_cnt,  --mci_last_row_cnt = n_cur_row_cnt
                    DECODE( --(d1 - d2) - ABS(d1 - d2) ==> 0=d1>=d2, !0=d1<d2
                        (mci_last_row_cnt - n_cur_row_cnt) - ABS(mci_last_row_cnt - n_cur_row_cnt),
                        0,
                        0,	--mci_last_row_cnt > n_cur_row_cnt
                        mci_consecutive_growth_cnt + 1	--mci_last_row_cnt < n_cur_row_cnt
                    )
                )*/
        	WHERE logic_engine_id = n_logic_engine_id;
        END LOOP;
        CLOSE c_logic_engine_info;
    END;
END;
/


-- End of DDL Script for Trigger ENGINE.TIA_MACHINE_CMD_INBOUND



-- ***** UNCOMMENT NEXT LINE AND RUN TO GET THE VALUE TO USE for the create sequence *****
--SELECT TAZDBA.MACHINE_COMMAND_PENDING_SEQ.nextval from dual;
CREATE SEQUENCE ENGINE.SEQ_MACHINE_CMD_PENDING_ID
    INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH ;
GRANT SELECT ON ENGINE.SEQ_MACHINE_CMD_PENDING_ID to web_user;

-- ***** UNCOMMENT NEXT LINE AND RUN TO GET THE VALUE TO USE for the create sequence *****
--SELECT TAZDBA.RERIX_MESSAGE_NUM_SEQ.nextval from dual;
CREATE SEQUENCE engine.RERIX_MESSAGE_NUM_SEQ
    INCREMENT BY 1 MINVALUE 1 MAXVALUE 254 START WITH 
    CACHE 10 CYCLE ORDER;
GRANT SELECT ON ENGINE.RERIX_MESSAGE_NUM_SEQ to web_user;



CREATE TABLE ENGINE.MACHINE_CMD_PENDING
  (
    MACHINE_COMMAND_PENDING_ID  VARCHAR2(12)     not null,
    MACHINE_ID                  VARCHAR2(22)     not null,
    MESSAGE_NUMBER              INTEGER,
    DATA_TYPE                   VARCHAR2(8)      not null,
    COMMAND                     VARCHAR2(4000),
    EXECUTE_DATE                DATE,
    EXECUTE_CD                  VARCHAR2(3)      default 'P' not null,
    EXECUTE_ORDER               INTEGER          default 999 not null,
    CREATED_BY                  VARCHAR2(30)     not null,
    CREATED_TS                  DATE             not null,
    LAST_UPDATED_BY             VARCHAR2(30)     not null,
    LAST_UPDATED_TS             DATE             not null
  )
  TABLESPACE ENGINE_DATA;

CREATE UNIQUE INDEX ENGINE.UDX_MACHINE_CMD_PENDING_1 ON engine.MACHINE_CMD_PENDING(MACHINE_COMMAND_PENDING_ID) TABLESPACE ENGINE_INDX;
CREATE INDEX ENGINE.IDX_MACHINE_CMD_PENDING_1 ON engine.MACHINE_CMD_PENDING(MACHINE_ID) TABLESPACE ENGINE_INDX;
GRANT SELECT, INSERT, UPDATE, DELETE ON ENGINE.MACHINE_CMD_PENDING to web_user;
GRANT SELECT, INSERT, UPDATE, DELETE ON ENGINE.MACHINE_CMD_PENDING to net_user;
GRANT SELECT, INSERT, UPDATE, DELETE ON ENGINE.MACHINE_CMD_PENDING to device;

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER engine.TAU_MACHINE_COMMAND_PENDING AFTER UPDATE ON engine.MACHINE_CMD_PENDING
BEGIN
    DELETE FROM MACHINE_COMMAND_PENDING WHERE execute_cd in ('A', 'C');
END;
/

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER engine.TRBI_MACHINE_CMD_PENDING BEFORE INSERT ON engine.MACHINE_CMD_PENDING
  FOR EACH ROW 
BEGIN
   if (:NEW.machine_command_pending_id is null) then
      SELECT SEQ_MACHINE_CMD_PENDING_ID.NEXTVAL
      INTO :NEW.machine_command_pending_id
      FROM dual ;
   end if;
	SELECT	sysdate,
			user,
			sysdate,
			user
	into	:new.created_ts,
			:new.created_by,
			:new.last_updated_ts,
			:new.last_updated_by
	FROM	dual;
END;
/

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER engine.TRBU_MACHINE_CMD_PENDING BEFORE UPDATE ON engine.MACHINE_CMD_PENDING
  FOR EACH ROW 
BEGIN
	SELECT	sysdate, user
	into	:new.last_updated_ts,
			:new.last_updated_by
	FROM	dual;
	IF(:new.EXECUTE_CD = 'S' OR :new.EXECUTE_CD = 'A') THEN
	   select sysdate into :new.EXECUTE_DATE from dual;
    END IF;
	IF(:new.EXECUTE_CD = 'A' OR :new.EXECUTE_CD = 'C') THEN
        INSERT INTO machine_command_pending_hist
        (
            MACHINE_COMMAND_PENDING_ID,
            MACHINE_ID,
            MESSAGE_NUMBER,
            DATA_TYPE,
            COMMAND,
            EXECUTE_DATE,
            EXECUTE_CD,
            EXECUTE_ORDER,
            CREATED_BY,
            CREATED_TS,
            LAST_UPDATED_BY,
            LAST_UPDATED_TS
        )
        VALUES
        (
            :new.MACHINE_COMMAND_PENDING_ID,
            :new.MACHINE_ID,
            :new.MESSAGE_NUMBER,
            :new.DATA_TYPE,
            :new.COMMAND,
            :new.EXECUTE_DATE,
            :new.EXECUTE_CD,
            :new.EXECUTE_ORDER,
            :new.CREATED_BY,
            :new.CREATED_TS,
            :new.LAST_UPDATED_BY,
            :new.LAST_UPDATED_TS
        );
    END IF;
END;
/

CREATE TABLE ENGINE.MACHINE_CMD_PENDING_HIST
  (
    MACHINE_COMMAND_PENDING_ID  VARCHAR2(12)     not null,
    MACHINE_ID                  VARCHAR2(22)     not null,
    MESSAGE_NUMBER              INTEGER,
    DATA_TYPE                   VARCHAR2(8)      not null,
    COMMAND                     VARCHAR2(4000),
    EXECUTE_DATE                DATE,
    EXECUTE_CD                  VARCHAR2(3)      default 'P' not null,
    EXECUTE_ORDER               INTEGER          default 999 not null,
    CREATED_BY                  VARCHAR2(30)     not null,
    CREATED_TS                  DATE             not null,
    LAST_UPDATED_BY             VARCHAR2(30)     not null,
    LAST_UPDATED_TS             DATE             not null
  )
  TABLESPACE ENGINE_DATA;
GRANT SELECT, INSERT, UPDATE, DELETE ON ENGINE.MACHINE_CMD_PENDING_HIST to web_user;
GRANT SELECT, INSERT, UPDATE, DELETE ON ENGINE.MACHINE_CMD_PENDING_HIST to net_user;




DROP PUBLIC SYNONYM MACHINE_COMMAND_PENDING;
CREATE PUBLIC SYNONYM MACHINE_COMMAND_PENDING FOR ENGINE.MACHINE_CMD_PENDING
/
DROP PUBLIC SYNONYM MACHINE_COMMAND_PENDING_HIST;
CREATE PUBLIC SYNONYM MACHINE_COMMAND_PENDING_HIST FOR ENGINE.MACHINE_CMD_PENDING_HIST
/
DROP PUBLIC SYNONYM SEQ_MACHINE_CMD_PENDING_ID;
CREATE PUBLIC SYNONYM SEQ_MACHINE_CMD_PENDING_ID FOR ENGINE.SEQ_MACHINE_CMD_PENDING_ID
/
DROP PUBLIC SYNONYM MACHINE_CMD_PENDING;
CREATE PUBLIC SYNONYM MACHINE_CMD_PENDING FOR ENGINE.MACHINE_CMD_PENDING
/
DROP PUBLIC SYNONYM MACHINE_CMD_PENDING_HIST;
CREATE PUBLIC SYNONYM MACHINE_CMD_PENDING_HIST FOR ENGINE.MACHINE_CMD_PENDING
/
DROP PUBLIC SYNONYM RERIX_MESSAGE_NUM_SEQ;
CREATE PUBLIC SYNONYM RERIX_MESSAGE_NUM_SEQ FOR ENGINE.RERIX_MESSAGE_NUM_SEQ
/




ALTER TABLE ENGINE.MACHINE_CMD_INBOUND ADD (CONSTRAINT FK_MCI_LOGIC_ENGINE_ID foreign key(LOGIC_ENGINE_ID) references ENGINE.LOGIC_ENGINE(LOGIC_ENGINE_ID));
ALTER TABLE ENGINE.MACHINE_CMD_OUTBOUND MODIFY  COMMAND    VARCHAR2(4000);
ALTER TABLE ENGINE.MACHINE_CMD_OUTBOUND ADD (SESSION_ID    NUMBER(20,0));
ALTER TABLE ENGINE.MACHINE_CMD_OUTBOUND_HIST ADD (SESSION_ID    NUMBER(20,0));
ALTER TABLE ENGINE.MACHINE_CMD_OUTBOUND_HIST MODIFY  COMMAND    VARCHAR2(4000);

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER engine.TRBU_MACHINE_CMD_OUTBOUND BEFORE UPDATE ON engine.MACHINE_CMD_OUTBOUND
  FOR EACH ROW 
BEGIN
	:NEW.execute_date := SYSDATE;
	/* Move Errors to Hist table */
	IF (SUBSTR (:NEW.execute_cd, 1, 1) = 'E') THEN
		INSERT INTO machine_cmd_outbound_hist(
			command_id,
			modem_id,
			command,
			command_date,
			execute_date,
			execute_cd,
			session_id
		) VALUES (
			:NEW.command_id,
			:NEW.modem_id,
			:NEW.command,
			:NEW.command_date,
			:NEW.execute_date,
			:NEW.execute_cd,
			:NEW.session_id
		);
		/* Change execute_cd to X */
		:NEW.execute_cd := 'X';
	END IF;
END;
/

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER engine.TRBI_MACHINE_CMD_OUTBOUND BEFORE INSERT ON engine.MACHINE_CMD_OUTBOUND
  FOR EACH ROW 
BEGIN
	IF (:New.command_id IS NULL) THEN
		SELECT seq_machine_cmd_outbound_id.NEXTVAL
		INTO :NEW.command_id
		FROM dual;
	END IF;
	:NEW.command_date := SYSDATE;
	/*Execute_CD will be null unless it is populated with an 'S'*/
	IF (:NEW.execute_cd is null) THEN
		:NEW.execute_cd := 'N';
	END IF;
END;
/

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER engine.TRUA_MACHINE_CMD_OUTBOUND AFTER UPDATE ON engine.MACHINE_CMD_OUTBOUND
  FOR EACH ROW 
BEGIN
	IF (:NEW.execute_cd = 'Y') THEN
		INSERT INTO machine_cmd_outbound_hist (
			command_id,
			modem_id,
			command,
			command_date,
			execute_date,
			execute_cd,
			session_id
		) VALUES (
			:NEW.command_id,
			:NEW.Modem_id,
			:NEW.command,
			:NEW.command_date,
			:NEW.execute_date,
			:NEW.execute_cd,
			:NEW.session_id
		);
	END IF;
END;
/

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER engine.TUA_MACHINE_CMD_OUTBOUND AFTER UPDATE ON engine.MACHINE_CMD_OUTBOUND
BEGIN
    /* Y's will be saved in Machine_command_hist - C's will be deleted - X's are Errors*/
    DELETE FROM machine_cmd_outbound WHERE execute_cd in  ('Y','C', 'X');
END;
/


-- copy old pending into new pending
INSERT INTO engine.machine_cmd_pending (
	SELECT * from tazdba.machine_command_pending
);
