CREATE TABLESPACE file_repo_data LOCATION '/opt/USAT/postgres/data/tblspace/file_repo_data';
CREATE ROLE admin WITH CREATEDB CREATEROLE;
GRANT CREATE ON TABLESPACE file_repo_data TO admin;
CREATE USER admin_1 PASSWORD 'ADMIN_1';
GRANT admin TO admin_1;
CREATE ROLE write_file_repo;
CREATE ROLE read_file_repo;
GRANT write_file_repo to admin;
CREATE TRUSTED LANGUAGE plpgsql;
