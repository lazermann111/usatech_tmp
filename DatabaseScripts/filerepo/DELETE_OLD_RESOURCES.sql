CREATE OR REPLACE FUNCTION FILE_REPO.DELETE_OLD_RESOURCE_BATCH(
    pn_minimum_days INTEGER,
    pn_max_rows INTEGER)
RETURNS INTEGER 
AS $$
DECLARE
    ln_rowcnt INTEGER;
BEGIN
	DELETE FROM FILE_REPO.RESOURCE WHERE RESOURCE_ID IN(
	   SELECT RESOURCE_ID FROM FILE_REPO.RESOURCE WHERE CREATED_UTC_TS < CURRENT_TIMESTAMP - (pn_minimum_days || ' days')::INTERVAL ORDER BY CREATED_UTC_TS LIMIT pn_max_rows
	);
	GET DIAGNOSTICS ln_rowcnt = ROW_COUNT;
    RETURN ln_rowcnt;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION FILE_REPO.DELETE_OLD_RESOURCES(
    pn_minimum_days INTEGER,
    pn_batch_size INTEGER)
RETURNS INTEGER 
AS $$
DECLARE
    ln_cnt INTEGER;
    ln_rowcnt INTEGER;
BEGIN
	ln_cnt := 0;
    LOOP
        SELECT FILE_REPO.DELETE_OLD_RESOURCE_BATCH(pn_minimum_days, pn_batch_size) INTO ln_rowcnt;
        EXIT WHEN ln_rowcnt <= 0;
        ln_cnt := ln_cnt + ln_rowcnt;
    END LOOP;
    RETURN ln_cnt;
END;
$$ LANGUAGE plpgsql;
