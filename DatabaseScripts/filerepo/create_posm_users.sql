CREATE USER posm_layer_1 PASSWORD 'POSM_LAYER_1';
GRANT write_file_repo TO posm_layer_1;

CREATE USER auth_layer_1 PASSWORD 'AUTH_LAYER_1';
GRANT write_file_repo TO auth_layer_1;
