CREATE USER app_layer_1 PASSWORD 'APP_LAYER_1';
CREATE USER net_layer_1 PASSWORD 'NET_LAYER_1';
CREATE USER auth_layer_1 PASSWORD 'AUTH_LAYER_1';
CREATE USER posm_layer_1 PASSWORD 'POSM_LAYER_1';

CREATE USER app_layer_2 PASSWORD 'APP_LAYER_1';
CREATE USER net_layer_2 PASSWORD 'NET_LAYER_1';
CREATE USER auth_layer_2 PASSWORD 'AUTH_LAYER_1';
CREATE USER posm_layer_2 PASSWORD 'POSM_LAYER_1';

CREATE USER app_layer_3 PASSWORD 'APP_LAYER_1';
CREATE USER net_layer_3 PASSWORD 'NET_LAYER_1';
CREATE USER auth_layer_3 PASSWORD 'AUTH_LAYER_1';
CREATE USER posm_layer_3 PASSWORD 'POSM_LAYER_1';

CREATE USER app_layer_4 PASSWORD 'APP_LAYER_1';
CREATE USER net_layer_4 PASSWORD 'NET_LAYER_1';
CREATE USER auth_layer_4 PASSWORD 'AUTH_LAYER_1';
CREATE USER posm_layer_4 PASSWORD 'POSM_LAYER_1';

CREATE USER monitor PASSWORD 'MONITOR';

GRANT use_mq TO app_layer_1;
GRANT use_mq TO net_layer_1;
GRANT use_mq TO auth_layer_1;
GRANT use_mq TO posm_layer_1;

GRANT use_mq TO app_layer_2;
GRANT use_mq TO net_layer_2;
GRANT use_mq TO auth_layer_2;
GRANT use_mq TO posm_layer_2;

GRANT use_mq TO app_layer_3;
GRANT use_mq TO net_layer_3;
GRANT use_mq TO auth_layer_3;
GRANT use_mq TO posm_layer_3;

GRANT use_mq TO app_layer_4;
GRANT use_mq TO net_layer_4;
GRANT use_mq TO auth_layer_4;
GRANT use_mq TO posm_layer_4;

GRANT read_mq TO monitor;

GRANT write_file_repo TO app_layer_2;
GRANT write_file_repo TO net_layer_2;
GRANT write_file_repo TO posm_layer_2;
GRANT write_file_repo TO auth_layer_2;

GRANT write_file_repo TO app_layer_3;
GRANT write_file_repo TO net_layer_3;
GRANT write_file_repo TO posm_layer_3;
GRANT write_file_repo TO auth_layer_3;

GRANT write_file_repo TO app_layer_4;
GRANT write_file_repo TO net_layer_4;
GRANT write_file_repo TO posm_layer_4;
GRANT write_file_repo TO auth_layer_4;
