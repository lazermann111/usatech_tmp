CREATE OR REPLACE PROCEDURE PSS.ADD_BLACKLISTING(
	pn_global_account_id PSS.CAB_BL.GLOBAL_ACCOUNT_ID%TYPE,
	pv_device_name PSS.CAB_BL.DEVICE_NAME%TYPE,
	pn_amount_min PSS.CAB_BL.AMOUNT_MIN%TYPE,
	pn_amount_max PSS.CAB_BL.AMOUNT_MAX%TYPE,
	pd_decline_until PSS.CAB_BL.DECLINE_UNTIL%TYPE,
	pv_denied_reason PSS.CAB_BL.DENIED_REASON%TYPE,
	pv_blacklist_reason PSS.CAB_BL.BL_REASON%TYPE,
	pn_blacklisting_id OUT NUMBER)
IS
	ln_whitelist_count NUMBER;
	ln_cab_bl_id PSS.CAB_BL.CAB_BL_ID%TYPE;
BEGIN
	IF pv_blacklist_reason != null && pv_blacklist_reason == 'RECENT_PROCESSOR_DECLINE' && DBADMIN.PKG_GLOBAL.GET_APP_SETTING('CONSUMER_ACCT_BLACKLIST_AFTER_DECLINE_ENABLED') != 'Y' THEN
		pn_blacklisting_id := NULL;
		RETURN;
	END IF;
	
	select count(1)
	into ln_whitelist_count
	from pss.consumer_acct_base
	where global_account_id = pn_global_account_id
	and nvl(whitelist_flag, 'N') = 'Y';
	
	IF ln_whitelist_count > 0 THEN
		pn_blacklisting_id := NULL;
		RETURN;
	END IF;
	
	ln_cab_bl_id := PSS.SEQ_CAB_BL.NEXTVAL;
	
	INSERT INTO PSS.CAB_BL(CAB_BL_ID, GLOBAL_ACCOUNT_ID, DECLINE_UNTIL, DENIED_REASON, DEVICE_NAME, AMOUNT_MIN, AMOUNT_MAX, BL_REASON)
	VALUES (ln_cab_bl_id, pn_global_account_id, pd_decline_until, pv_denied_reason, pv_device_name, pn_amount_min, pn_amount_max, pv_blacklist_reason);
	
	pn_blacklisting_id := ln_cab_bl_id;
END;
/

GRANT EXECUTE ON PSS.ADD_BLACKLISTING TO USAT_POSM_ROLE;
GRANT EXECUTE ON PSS.ADD_BLACKLISTING TO USAT_DMS_ROLE;
GRANT EXECUTE ON PSS.ADD_BLACKLISTING TO USAT_LOADER_ROLE;
