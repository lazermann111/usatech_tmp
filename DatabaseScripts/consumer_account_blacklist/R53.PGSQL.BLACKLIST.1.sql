DROP TABLE APP_CLUSTER.CONSUMER_ACCT_BLACKLIST;

CREATE TABLE IF NOT EXISTS APP_CLUSTER.CONSUMER_ACCT_BLACKLIST (
    CONSUMER_ACCT_BLACKLIST_ID SERIAL NOT NULL,
    GLOBAL_ACCOUNT_ID BIGINT NOT NULL,
    AUTHORITY_NAME CHARACTER VARYING(128) NOT NULL,
    DECLINE_UNTIL TIMESTAMP NULL,
    DENIED_REASON_NAME CHARACTER VARYING(128) NULL,
    CONSTRAINT PK_CONSUMER_ACCT_BLACKLIST PRIMARY KEY(CONSUMER_ACCT_BLACKLIST_ID))
INHERITS(APP_CLUSTER._);

DROP TRIGGER IF EXISTS TRBI_CONSUMER_ACCT_BLACKLIST ON APP_CLUSTER.CONSUMER_ACCT_BLACKLIST;
CREATE TRIGGER TRBI_CONSUMER_ACCT_BLACKLIST
BEFORE INSERT ON APP_CLUSTER.CONSUMER_ACCT_BLACKLIST
    FOR EACH ROW
    EXECUTE PROCEDURE APP_CLUSTER.FTRBI_AUDIT();

DROP TRIGGER IF EXISTS TRBU_CONSUMER_ACCT_BLACKLIST ON APP_CLUSTER.CONSUMER_ACCT_BLACKLIST;
CREATE TRIGGER TRBU_CONSUMER_ACCT_BLACKLIST
BEFORE UPDATE ON APP_CLUSTER.CONSUMER_ACCT_BLACKLIST
    FOR EACH ROW
    EXECUTE PROCEDURE APP_CLUSTER.FTRBU_AUDIT();

CREATE INDEX IX_CONSUMER_ACCT_BLACKLIST_AK ON APP_CLUSTER.CONSUMER_ACCT_BLACKLIST(GLOBAL_ACCOUNT_ID,EXPIRATION,AUTHORITY_NAME);

GRANT SELECT ON APP_CLUSTER.CONSUMER_ACCT_BLACKLIST TO read_app;
GRANT INSERT, UPDATE, DELETE, TRUNCATE ON APP_CLUSTER.CONSUMER_ACCT_BLACKLIST TO write_app;

CREATE OR REPLACE FUNCTION APP_CLUSTER.UPD_CONSUMER_ACCT_BLACKLIST (
    PN_GLOBAL_ACCOUNT_ID APP_CLUSTER.CONSUMER_ACCT_BLACKLIST.GLOBAL_ACCOUNT_ID%TYPE,
    PV_AUTHORITY_NAME APP_CLUSTER.CONSUMER_ACCT_BLACKLIST.AUTHORITY_NAME%TYPE,
    PV_DENIED_REASON_NAME APP_CLUSTER.CONSUMER_ACCT_BLACKLIST.DENIED_REASON_NAME%TYPE,
    PT_DECLINE_UNTIL APP_CLUSTER.CONSUMER_ACCT_BLACKLIST.DECLINE_UNTIL%TYPE)
RETURNS VOID
SECURITY DEFINER
AS $$
BEGIN
    LOOP
        UPDATE APP_CLUSTER.CONSUMER_ACCT_BLACKLIST
        SET DECLINE_UNTIL = PT_DECLINE_UNTIL, 
            DENIED_REASON_NAME = PV_DENIED_REASON_NAME
        WHERE GLOBAL_ACCOUNT_ID = PN_GLOBAL_ACCOUNT_ID
            AND AUTHORITY_NAME = PV_AUTHORITY_NAME;

        IF FOUND THEN
            RETURN;
        END IF;

        BEGIN
            INSERT INTO APP_CLUSTER.CONSUMER_ACCT_BLACKLIST(GLOBAL_ACCOUNT_ID, AUTHORITY_NAME, DECLINE_UNTIL, DENIED_REASON_NAME)
            VALUES(PN_GLOBAL_ACCOUNT_ID, PV_AUTHORITY_NAME, PT_DECLINE_UNTIL, PV_DENIED_REASON_NAME);
            RETURN;
        EXCEPTION WHEN unique_violation THEN
            -- loop
        END;
    END LOOP;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION APP_CLUSTER.UPD_CONSUMER_ACCT_BLACKLIST (
    PN_GLOBAL_ACCOUNT_ID APP_CLUSTER.CONSUMER_ACCT_BLACKLIST.GLOBAL_ACCOUNT_ID%TYPE,
    PV_AUTHORITY_NAME APP_CLUSTER.CONSUMER_ACCT_BLACKLIST.AUTHORITY_NAME%TYPE,
    PV_DENIED_REASON_NAME APP_CLUSTER.CONSUMER_ACCT_BLACKLIST.DENIED_REASON_NAME%TYPE,
    PT_DECLINE_UNTIL APP_CLUSTER.CONSUMER_ACCT_BLACKLIST.DECLINE_UNTIL%TYPE) TO read_app;
