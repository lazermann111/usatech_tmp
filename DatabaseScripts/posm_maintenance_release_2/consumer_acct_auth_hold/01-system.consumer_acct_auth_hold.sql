CREATE TABLE PSS.CONSUMER_ACCT_AUTH_HOLD
  (
    AUTH_ID           NUMBER(20,0),
    CONSUMER_ACCT_ID  NUMBER(20,0)   not null,
    CREATED_BY        VARCHAR2(30)   not null,
    CREATED_TS        DATE           not null,
    LAST_UPDATED_BY   VARCHAR2(30)   not null,
    LAST_UPDATED_TS   DATE           not null,
    CONSTRAINT PK_CONSUMER_ACCT_AUTH_HOLD primary key(AUTH_ID) USING INDEX TABLESPACE PSS_INDX,
    CONSTRAINT FK_CONSUMER_ACCT_AH_AUTH_ID foreign key(AUTH_ID) references AUTH(AUTH_ID),
    CONSTRAINT FK_CONSUMER_ACCT_AH_CA_ID foreign key(CONSUMER_ACCT_ID) references CONSUMER_ACCT(CONSUMER_ACCT_ID)
  )
  TABLESPACE PSS_DATA;

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER PSS.TRBI_CONSUMER_ACCT_AUTH_HOLD BEFORE INSERT ON PSS.CONSUMER_ACCT_AUTH_HOLD
  FOR EACH ROW 
BEGIN
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER PSS.TRBU_CONSUMER_ACCT_AUTH_HOLD BEFORE UPDATE ON PSS.CONSUMER_ACCT_AUTH_HOLD
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

COMMENT ON TABLE PSS.CONSUMER_ACCT_AUTH_HOLD is 'Associates a consumer account to authorization events that are currently holding locks on funds (sum of the approved amount in the auths).  Application may optionally consider the time of the auth when calculating total funds.';

CREATE PUBLIC SYNONYM CONSUMER_ACCT_AUTH_HOLD FOR PSS.CONSUMER_ACCT_AUTH_HOLD;
