grant select, insert, update, delete on engine.machine_cmd_inbound to web_user;
grant select, insert, update, delete on engine.machine_cmd_inbound_hist to web_user;
grant select, insert, update, delete on pss.consumer_acct_auth_hold to web_user;

insert into tran_state (tran_state_cd, tran_state_desc) values ('Z', 'Duplicate Tran');
