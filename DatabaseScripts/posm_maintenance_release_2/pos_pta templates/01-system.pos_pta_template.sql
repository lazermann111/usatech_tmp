-- pos activation/deactivation fields
ALTER TABLE PSS.POS ADD (
	POS_ACTIVATION_TS    DATE
);

-- modify trigger on PSS.POS
CREATE OR REPLACE TRIGGER PSS.TRBI_POS BEFORE INSERT ON PSS.POS
  FOR EACH ROW 
Begin
    IF :new.pos_id IS NULL THEN
      SELECT pss.seq_pos_id.nextval
        into :new.pos_id
        FROM dual;
    END IF;
 SELECT    sysdate,
           user,
           sysdate,
           user,
           nvl(:new.pos_active_yn_flag, 'Y'),
           nvl(:new.pos_activation_ts, sysdate)
      into :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by,
           :new.pos_active_yn_flag,
           :new.pos_activation_ts
      FROM dual;
End;
/

-- create tables
CREATE SEQUENCE PSS.SEQ_POS_PTA_TMPL_ID
    INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH 1;

CREATE SEQUENCE PSS.SEQ_POS_PTA_TMPL_ENTRY_ID
    INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH 1;

CREATE TABLE PSS.POS_PTA_TMPL
  (
    POS_PTA_TMPL_ID    NUMBER(20,0),
    POS_PTA_TMPL_NAME  VARCHAR2(60)    not null,
    POS_PTA_TMPL_DESC  VARCHAR2(255),
    CREATED_BY         VARCHAR2(30)    not null,
    CREATED_TS         DATE            not null,
    LAST_UPDATED_BY    VARCHAR2(30)    not null,
    LAST_UPDATED_TS    DATE            not null,
    CONSTRAINT PK_POS_PTA_TMPL primary key(POS_PTA_TMPL_ID) USING INDEX TABLESPACE PSS_INDX
  )
  TABLESPACE PSS_DATA;

CREATE PUBLIC SYNONYM POS_PTA_TMPL FOR PSS.POS_PTA_TMPL
/
REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER PSS.TRBI_POS_PTA_TMPL BEFORE INSERT ON PSS.POS_PTA_TMPL
  FOR EACH ROW 
BEGIN
	IF :NEW.POS_PTA_TMPL_ID IS NULL
	THEN
		SELECT PSS.SEQ_POS_PTA_TMPL_ID.NEXTVAL
		INTO :NEW.POS_PTA_TMPL_ID
		FROM DUAL;
	END IF;
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER PSS.TRBU_POS_PTA_TMPL BEFORE UPDATE ON PSS.POS_PTA_TMPL
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

COMMENT ON TABLE PSS.POS_PTA_TMPL is 'Stores point-of-sale templates for ease of creation of multiple logical pos_ptas.';

-- create tables
CREATE TABLE PSS.POS_PTA_TMPL_ENTRY
  (
    POS_PTA_TMPL_ENTRY_ID         NUMBER(20,0),
    POS_PTA_TMPL_ID               NUMBER(20,0)    not null,
    PAYMENT_SUBTYPE_ID            NUMBER(20,0)    not null,
    POS_PTA_ENCRYPT_KEY           VARCHAR2(192),
    POS_PTA_ACTIVATION_OSET_HR    INTEGER,
    POS_PTA_DEACTIVATION_OSET_HR  INTEGER,
    CREATED_BY                    VARCHAR2(30)    not null,
    CREATED_TS                    DATE            not null,
    LAST_UPDATED_BY               VARCHAR2(30)    not null,
    LAST_UPDATED_TS               DATE            not null,
    PAYMENT_SUBTYPE_KEY_ID        INTEGER,
    POS_PTA_PIN_REQ_YN_FLAG       char(1)         default 'N',
    POS_PTA_DEVICE_SERIAL_CD      VARCHAR2(60),
    POS_PTA_ENCRYPT_KEY2          VARCHAR2(192),
    AUTHORITY_PAYMENT_MASK_ID     NUMBER(20,0),
    POS_PTA_PRIORITY              INTEGER         not null,
    TERMINAL_ID                   NUMBER(20,0),
    MERCHANT_BANK_ACCT_ID         NUMBER(20,0),
    CURRENCY_CD                   VARCHAR2(3)     not null,
    CONSTRAINT PK_POS_PTA_TMPL_ENTRY primary key(POS_PTA_TMPL_ENTRY_ID) USING INDEX TABLESPACE PSS_INDX,
    CONSTRAINT FK_POS_PTA_TMPL_ENTRY_PPT foreign key(POS_PTA_TMPL_ID) references POS_PTA_TMPL(POS_PTA_TMPL_ID),
    CONSTRAINT FK_POS_PTA_TMPL_ENTRY_PS foreign key(PAYMENT_SUBTYPE_ID) references PAYMENT_SUBTYPE(PAYMENT_SUBTYPE_ID),
    CONSTRAINT FK_POS_PTA_TMPL_ENTRY_APM foreign key(AUTHORITY_PAYMENT_MASK_ID) references AUTHORITY_PAYMENT_MASK(AUTHORITY_PAYMENT_MASK_ID),
    CONSTRAINT FK_POS_PTA_TMPL_ENTRY_TERMINAL foreign key(TERMINAL_ID) references TERMINAL(TERMINAL_ID),
    CONSTRAINT FK_POS_PTA_TMPL_ENTRY_MBA foreign key(MERCHANT_BANK_ACCT_ID) references MERCHANT_BANK_ACCT(MERCHANT_BANK_ACCT_ID),
    CONSTRAINT FK_POS_PTA_TMPL_ENTRY_CURRENCY foreign key(CURRENCY_CD) references CURRENCY(CURRENCY_CD)
  )
  TABLESPACE PSS_DATA;

CREATE UNIQUE INDEX PSS.UDX_POS_PTA_TMPL_ENTRY_1 ON PSS.POS_PTA_TMPL_ENTRY(POS_PTA_TMPL_ID,PAYMENT_SUBTYPE_ID,PAYMENT_SUBTYPE_KEY_ID) TABLESPACE PSS_INDX;
CREATE UNIQUE INDEX PSS.UDX_POS_PTA_TMPL_ENTRY_2 ON PSS.POS_PTA_TMPL_ENTRY(POS_PTA_TMPL_ID,PAYMENT_SUBTYPE_ID,POS_PTA_PRIORITY) TABLESPACE PSS_INDX;

CREATE PUBLIC SYNONYM POS_PTA_TMPL_ENTRY FOR PSS.POS_PTA_TMPL_ENTRY
/

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER PSS.TRBI_POS_PTA_TMPL_ENTRY BEFORE INSERT ON PSS.POS_PTA_TMPL_ENTRY
  FOR EACH ROW 
BEGIN
	IF :NEW.POS_PTA_TMPL_ENTRY_ID IS NULL
	THEN
		SELECT PSS.SEQ_POS_PTA_TMPL_ENTRY_ID.NEXTVAL
		INTO :NEW.POS_PTA_TMPL_ENTRY_ID
		FROM DUAL;
	END IF;
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER PSS.TRBU_POS_PTA_TMPL_ENTRY BEFORE UPDATE ON PSS.POS_PTA_TMPL_ENTRY
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

COMMENT ON TABLE PSS.POS_PTA_TMPL_ENTRY is 'Represents an individual template for a pos_pta.';

-- add cols to device_type table
ALTER TABLE DEVICE.DEVICE_TYPE ADD (
	DEVICE_TYPE_SERIAL_CD_REGEX  VARCHAR2(255),
	POS_PTA_TMPL_ID              NUMBER(20,0)
);

-- create defaults for new columns in device_type table
-- 0                    	^E4[0-9]{6}$
-- 1                    	^G5[0-9]{6}$
-- 3                    	^$
-- 4                    	^((([0-9A-F]{8})((63){4}))|(S1[0-9A-F]{16}))$
-- 5                    	^[0-9]{12}$
-- 6                    	^M1[0-9]{6}$
-- 7                    	^$
-- 8                    	^$
-- 9                    	^E4[0-9]{6}$
-- 10                   	^$
-- 11                   	^K1[0-9A-F]{16}$
-- 12                   	^T2[0-9]{6}$
UPDATE DEVICE.DEVICE_TYPE
	SET DEVICE_TYPE_SERIAL_CD_REGEX = '^E4[0-9]{6}$'
	WHERE device_type_id = 0;	--G4;
UPDATE DEVICE.DEVICE_TYPE
	SET DEVICE_TYPE_SERIAL_CD_REGEX = '^G5[0-9]{6}$'
	WHERE device_type_id = 1;	--G5;
UPDATE DEVICE.DEVICE_TYPE
	SET DEVICE_TYPE_SERIAL_CD_REGEX = '^$'
	WHERE device_type_id = 3;	--Brick;
UPDATE DEVICE.DEVICE_TYPE
	SET DEVICE_TYPE_SERIAL_CD_REGEX = '^((([0-9A-F]{8})((63){4}))|(S1[0-9A-F]{16}))$'
	WHERE device_type_id = 4;	--Sony PictureStation;
UPDATE DEVICE.DEVICE_TYPE
	SET DEVICE_TYPE_SERIAL_CD_REGEX = '^[0-9]{12}$'
	WHERE device_type_id = 5;	--eSuds Room Controller;
UPDATE DEVICE.DEVICE_TYPE
	SET DEVICE_TYPE_SERIAL_CD_REGEX = '^M1[0-9]{6}$'
	WHERE device_type_id = 6;	--MEI Telemeter;

UPDATE DEVICE.DEVICE_TYPE
	SET DEVICE_TYPE_SERIAL_CD_REGEX = '^$'
	WHERE device_type_id = 7;	--EZ80 ePort Develoment Module;
UPDATE DEVICE.DEVICE_TYPE
	SET DEVICE_TYPE_SERIAL_CD_REGEX = '^$'
	WHERE device_type_id = 8;	--EZ80 ePort Production Module;
UPDATE DEVICE.DEVICE_TYPE
	SET DEVICE_TYPE_SERIAL_CD_REGEX = '^E4[0-9]{6}$'
	WHERE device_type_id = 9;	--Legacy G4;
UPDATE DEVICE.DEVICE_TYPE
	SET DEVICE_TYPE_SERIAL_CD_REGEX = '^$'
	WHERE device_type_id = 10;	--Transact;
UPDATE DEVICE.DEVICE_TYPE
	SET DEVICE_TYPE_SERIAL_CD_REGEX = '^K1[0-9A-F]{16}$'
	WHERE device_type_id = 11;	--Kiosk;
UPDATE DEVICE.DEVICE_TYPE
	SET DEVICE_TYPE_SERIAL_CD_REGEX = '^T2[0-9]{6}$'
	WHERE device_type_id = 12;	--T2;

ALTER TABLE DEVICE.DEVICE_TYPE MODIFY DEVICE_TYPE_SERIAL_CD_REGEX not null;

-- set defaults for pos activation (created_ date)
UPDATE PSS.POS SET POS_ACTIVATION_TS = CREATED_TS;

-- set column not null
ALTER TABLE PSS.POS MODIFY POS_ACTIVATION_TS not null;

