--logic engine tables
CREATE TABLE engine.logic_engine(
	logic_engine_id NUMBER(20,0),	--pk
	logic_engine_desc VARCHAR2(60),
	logic_engine_active_yn_flag CHAR(1) NOT NULL,
	created_by VARCHAR2(30),
	created_ts DATE,
	last_updated_by VARCHAR2(30),
	last_updated_ts DATE,
	CONSTRAINT pk_logic_engine PRIMARY KEY (logic_engine_id)
)
TABLESPACE engine_data;

CREATE TABLE engine.logic_engine_device_type_excl(
	logic_engine_id NUMBER(20,0), --fk of engine.logic_engine.logic_engine_id,
	device_type_id NUMBER(20,0), --fk of device.device_type.device_type_id
	created_by VARCHAR2(30),
	created_ts DATE,
	last_updated_by VARCHAR2(30),
	last_updated_ts DATE
)
TABLESPACE engine_data;

CREATE TABLE engine.logic_engine_stats(
	logic_engine_id NUMBER(20,0) NOT NULL,	--pk
	mci_last_row_cnt_activity_ts DATE NOT NULL,
	mci_last_row_cnt NUMBER(9,0) NOT NULL,
	mci_consecutive_growth_ts DATE NOT NULL,
	mci_consecutive_growth_cnt NUMBER(6,0) DEFAULT 0 NOT NULL,
	engine_last_activity_ts DATE NOT NULL,
	engine_session_processed_msgs NUMBER(20,0) DEFAULT 0 NOT NULL,
	created_by VARCHAR2(30),
	created_ts DATE,
	last_updated_by VARCHAR2(30),
	last_updated_ts DATE
)
TABLESPACE engine_data;

--network layer tables
CREATE TABLE engine.net_layer(
	net_layer_id NUMBER(20,0),	--pk
	net_layer_desc VARCHAR2(60),
	net_layer_active_yn_flag CHAR(1) NOT NULL,
	created_by VARCHAR2(30),
	created_ts DATE,
	last_updated_by VARCHAR2(30),
	last_updated_ts DATE,
	CONSTRAINT pk_net_layer PRIMARY KEY (net_layer_id)
)
TABLESPACE engine_data;

CREATE TABLE engine.net_layer_device_session(
	session_id NUMBER(20,0),	--pk
	net_layer_id NUMBER(20,0),	--fk of engine.net_layer.net_layer_id
	device_name VARCHAR2(60) NOT NULL,	--in future, fk of device.device.device_name (when sony in device table)
	session_active_yn_flag CHAR(1) DEFAULT 'Y' NOT NULL,	--when 'N', after update row trigger inserts to _hist table, after update stmt trigger deletes all 'N'
	logic_engine_id NUMBER(20,0),	--fk of engine.logic_engine.logic_engine_id (set by before insert stmt trigger)
	session_start_ts DATE DEFAULT SYSDATE NOT NULL,
	session_end_ts DATE,	--set by row update trigger when session_active_yn_flag set to 'N'
	session_client_ip_addr VARCHAR2(16),
	session_client_port NUMBER(5,0),
	session_inbound_msg_cnt NUMBER(6,0) DEFAULT 0,
	session_outbound_msg_cnt NUMBER(6,0) DEFAULT 0,
	session_inbound_byte_cnt NUMBER(9,0) DEFAULT 0,
	session_outbound_byte_cnt NUMBER(9,0) DEFAULT 0,
	created_by VARCHAR2(30),
	created_ts DATE,
	last_updated_by VARCHAR2(30),
	last_updated_ts DATE,
	CONSTRAINT pk_net_layer_device_session PRIMARY KEY (session_id)
)
TABLESPACE engine_data;

CREATE TABLE engine.net_layer_device_session_hist(
	session_id NUMBER(20,0),	--pk
	net_layer_id NUMBER(20,0),
	device_name VARCHAR2(60),
	session_active_yn_flag CHAR(1),
	logic_engine_id NUMBER(20,0),
	session_start_ts DATE,
	session_end_ts DATE,
	session_client_ip_addr VARCHAR2(16),
	session_client_port NUMBER(5,0),
	session_inbound_msg_cnt NUMBER(6,0) DEFAULT 0,
	session_outbound_msg_cnt NUMBER(6,0) DEFAULT 0,
	session_inbound_byte_cnt NUMBER(9,0) DEFAULT 0,
	session_outbound_byte_cnt NUMBER(9,0) DEFAULT 0,
	created_by VARCHAR2(30),
	created_ts DATE,
	last_updated_by VARCHAR2(30),
	last_updated_ts DATE,
	CONSTRAINT pk_net_layer_device_session_hi PRIMARY KEY (session_id)
)
TABLESPACE engine_data;

CREATE TABLE engine.machine_cmd_inbound(
	inbound_id				NUMBER(20) NOT NULL,
	machine_id				VARCHAR2(22),
	inbound_date			DATE,
	inbound_command			VARCHAR2(4000),
	timestamp				DATE,
	execute_date			DATE,
	execute_cd				VARCHAR2(3),
	inbound_msg_no			NUMBER(3,0),
	insert_ts				DATE,
	update_ts				DATE,
	original_execute_cd		VARCHAR2(3),
	num_times_executed		NUMBER,
	logic_engine_id 		NUMBER(20,0),
	session_id 				NUMBER(20,0),
	CONSTRAINT pk_machine_cmd_inbound PRIMARY KEY (inbound_id)
)
TABLESPACE engine_data;

CREATE TABLE engine.machine_cmd_inbound_hist(
	inbound_id			NUMBER(20) NOT NULL,
	machine_id			VARCHAR2(22),
	inbound_date		DATE,
	inbound_command		VARCHAR2(4000),
	timestamp			DATE,
	execute_date		DATE,
	execute_cd			VARCHAR2(3),
	inbound_msg_no		NUMBER(3,0),
	num_times_executed	NUMBER,
	logic_engine_id		NUMBER(20,0),
	session_id			NUMBER(20,0),
	CONSTRAINT pk_machine_cmd_inbound_hist PRIMARY KEY (inbound_id)
)
TABLESPACE engine_data;

CREATE TABLE engine.machine_cmd_outbound(
	command_id		NUMBER(20) NOT NULL,
	modem_id		VARCHAR2(20),
	command			VARCHAR2(2304),
	command_date	DATE,
	execute_date	DATE,
	execute_cd		VARCHAR2(3),
	CONSTRAINT pk_machine_cmd_outbound PRIMARY KEY (command_id)
)
TABLESPACE engine_data;

CREATE TABLE engine.machine_cmd_outbound_hist(
	command_id		NUMBER(20) NOT NULL,
    modem_id		VARCHAR2(20),
    command			VARCHAR2(2304),
    command_date	DATE,
    execute_date	DATE,
    execute_cd		VARCHAR2(3),
	CONSTRAINT pk_machine_cmd_outbound_hist PRIMARY KEY (command_id)
)
TABLESPACE engine_data;

-- additional indexes
CREATE INDEX engine.ix_mcih_machine_id ON engine.MACHINE_CMD_INBOUND_HIST(machine_id)
TABLESPACE engine_indx; 

CREATE INDEX engine.ix_mcih_inbound_date ON engine.MACHINE_CMD_INBOUND_HIST(inbound_date)
TABLESPACE engine_indx; 

CREATE INDEX engine.ix_mcoh_modem_id ON engine.MACHINE_CMD_OUTBOUND_HIST(modem_id)
TABLESPACE engine_indx; 

CREATE INDEX engine.ix_mcoh_execute_date ON engine.MACHINE_CMD_OUTBOUND_HIST(execute_date)
TABLESPACE engine_indx; 
