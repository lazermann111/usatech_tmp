-- public synonyms: tables
DROP PUBLIC SYNONYM machine_command_inbound;
CREATE PUBLIC SYNONYM machine_cmd_inbound FOR engine.machine_cmd_inbound;
CREATE PUBLIC SYNONYM machine_command_inbound FOR engine.machine_cmd_inbound;

DROP PUBLIC SYNONYM machine_command_inbound_hist;
CREATE PUBLIC SYNONYM machine_cmd_inbound_hist FOR engine.machine_cmd_inbound_hist;
CREATE PUBLIC SYNONYM machine_command_inbound_hist FOR engine.machine_cmd_inbound_hist;

DROP PUBLIC SYNONYM machine_command;
CREATE PUBLIC SYNONYM machine_cmd_outbound FOR engine.machine_cmd_outbound;
CREATE PUBLIC SYNONYM machine_command FOR engine.machine_cmd_outbound;

DROP PUBLIC SYNONYM machine_command_hist;
CREATE PUBLIC SYNONYM machine_cmd_outbound_hist FOR engine.machine_cmd_outbound_hist;
CREATE PUBLIC SYNONYM machine_command_hist FOR engine.machine_cmd_outbound_hist;

CREATE PUBLIC SYNONYM logic_engine FOR engine.logic_engine;
CREATE PUBLIC SYNONYM logic_engine_device_type_excl FOR engine.logic_engine_device_type_excl;
CREATE PUBLIC SYNONYM logic_engine_stats FOR engine.logic_engine_stats;
CREATE PUBLIC SYNONYM net_layer FOR engine.net_layer;
CREATE PUBLIC SYNONYM net_layer_device_session FOR engine.net_layer_device_session;
CREATE PUBLIC SYNONYM net_layer_device_session_hist FOR engine.net_layer_device_session_hist;

-- public synonyms: other objects
DROP PUBLIC SYNONYM sp_flush_outbound_cmd_queue;
CREATE PUBLIC SYNONYM sp_flush_outbound_cmd_queue FOR engine.sp_flush_outbound_cmd_queue;
CREATE PUBLIC SYNONYM sf_select_best_logic_engine FOR engine.sf_select_best_logic_engine;
CREATE PUBLIC SYNONYM seq_logic_engine_id FOR engine.seq_logic_engine_id;
--CREATE PUBLIC SYNONYM seq_machine_cmd_id FOR engine.seq_machine_cmd_id;
CREATE PUBLIC SYNONYM seq_machine_cmd_inbound_id FOR engine.seq_machine_cmd_inbound_id;
CREATE PUBLIC SYNONYM seq_machine_cmd_outbound_id FOR engine.seq_machine_cmd_outbound_id;
CREATE PUBLIC SYNONYM seq_net_layer_id FOR engine.seq_net_layer_id;
CREATE PUBLIC SYNONYM seq_net_layer_device_sess_id FOR engine.seq_net_layer_device_sess_id;
CREATE PUBLIC SYNONYM pkg_engine_maint FOR engine.pkg_engine_maint;


-- grants: web_user
GRANT SELECT, INSERT, UPDATE ON engine.logic_engine TO web_user;
GRANT SELECT, INSERT, UPDATE, DELETE ON engine.logic_engine_device_type_excl TO web_user;
GRANT SELECT ON engine.logic_engine_stats TO web_user;
GRANT SELECT, UPDATE ON engine.machine_cmd_inbound TO web_user;
GRANT SELECT ON engine.machine_cmd_inbound_hist TO web_user;
GRANT SELECT, INSERT, UPDATE ON engine.machine_cmd_outbound TO web_user;
GRANT SELECT ON engine.machine_cmd_outbound_hist TO web_user;
GRANT SELECT ON engine.net_layer TO web_user;
GRANT SELECT ON engine.net_layer_device_session TO web_user;
GRANT SELECT ON engine.net_layer_device_session_hist TO web_user;

GRANT EXECUTE ON engine.sp_flush_outbound_cmd_queue TO web_user;
GRANT EXECUTE ON engine.sf_select_best_logic_engine TO web_user;
GRANT SELECT ON engine.seq_logic_engine_id TO web_user;
--GRANT SELECT ON engine.seq_machine_cmd_id TO web_user;
GRANT SELECT ON engine.seq_machine_cmd_inbound_id TO web_user;
GRANT SELECT ON engine.seq_machine_cmd_outbound_id TO web_user;
GRANT SELECT ON engine.seq_net_layer_id TO web_user;
GRANT EXECUTE ON engine.pkg_engine_maint TO net_user;

-- grants: net_layer
GRANT SELECT ON device.device_type TO net_user;
GRANT SELECT, INSERT, UPDATE ON engine.machine_cmd_inbound TO net_user;
GRANT SELECT, INSERT, UPDATE ON engine.machine_cmd_outbound TO net_user;
GRANT SELECT ON engine.net_layer TO net_user;
GRANT SELECT, INSERT, UPDATE ON engine.net_layer_device_session TO net_user;

GRANT EXECUTE ON engine.sp_flush_outbound_cmd_queue TO net_user;
GRANT EXECUTE ON engine.sf_select_best_logic_engine TO net_user;
GRANT SELECT ON engine.seq_logic_engine_id TO net_user;
--GRANT SELECT ON engine.seq_machine_cmd_id TO net_user;
GRANT SELECT ON engine.seq_machine_cmd_inbound_id TO net_user;
GRANT SELECT ON engine.seq_machine_cmd_outbound_id TO net_user;
GRANT SELECT ON engine.seq_net_layer_id TO net_user;

--grants: net_layer (USANet3)
GRANT EXECUTE ON device.pkg_call_in_record_maint TO net_user;
GRANT SELECT, UPDATE ON device.device TO net_user;

--grants: net_layer (USANet2)
GRANT EXECUTE ON tazdba.rerix_machine_id TO net_user;
GRANT SELECT, INSERT ON tazdba.rerix_modem_to_serial TO net_user;
GRANT SELECT ON tazdba.rerix_initialization TO net_user;

--grants: net_layer (UnifiedLayer)
