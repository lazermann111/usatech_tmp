CREATE SEQUENCE engine.seq_net_layer_device_sess_id
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  NOCYCLE
  NOORDER
  NOCACHE
/

CREATE SEQUENCE engine.seq_logic_engine_id
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  NOCYCLE
  NOORDER
  NOCACHE
/

CREATE SEQUENCE engine.seq_net_layer_id
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  NOCYCLE
  NOORDER
  NOCACHE
/

BEGIN
	DECLARE
		seq_num NUMBER(20,0);
		sqlc varchar2(1000);
	BEGIN
        SELECT tazdba.machine_cmd_in_seq.NEXTVAL INTO seq_num FROM DUAL;
		sqlc := 'CREATE SEQUENCE engine.seq_machine_cmd_inbound_id
		  INCREMENT BY 1
		  START WITH '||seq_num||'
		  MINVALUE 1
		  MAXVALUE 999999999999999999999999999
		  NOCYCLE
		  NOORDER
		  NOCACHE';
        EXECUTE IMMEDIATE sqlc;
	END;
END;
/

BEGIN
	DECLARE
		seq_num NUMBER(20,0);
		sqlc varchar2(1000);
	BEGIN
        SELECT tazdba.machine_command_seq.NEXTVAL INTO seq_num FROM DUAL;
		sqlc := 'CREATE SEQUENCE engine.seq_machine_cmd_outbound_id
		  INCREMENT BY 1
		  START WITH '||seq_num||'
		  MINVALUE 1
		  MAXVALUE 999999999999999999999999999
		  NOCYCLE
		  NOORDER
		  NOCACHE';
        EXECUTE IMMEDIATE sqlc;
	END;
END;
/

-- Start of DDL Script for Procedure ENGINE.SP_FLUSH_OUTBOUND_CMD_QUEUE
-- Generated 15-Mar-2005 16:29:36 from ENGINE@USADBD03.USATECH.COM

CREATE OR REPLACE 
PROCEDURE engine.sp_flush_outbound_cmd_queue (

/*This procedure will clear all outbound messages for the specified machine*/

   pv_machine_id              IN       machine_cmd_outbound.modem_id%TYPE,
   pn_error_number            OUT      NUMBER,
   pv_error_message           OUT      VARCHAR2)
IS
   v_error_msg                  exception_data.additional_information%TYPE;
   cv_procedure_name   CONSTANT VARCHAR2 (30)                                := 'SP_FLUSH_OUTBOUND_CMD_QUEUE';
   cv_server_name      CONSTANT VARCHAR2 (30)                                := 'TAZ1';
BEGIN
   DELETE FROM machine_cmd_outbound
         WHERE modem_id = pv_machine_id;

   COMMIT;
EXCEPTION
   WHEN OTHERS THEN
      v_error_msg :=
                'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
      pkg_exception_processor.sp_log_exception (
         PKG_APP_EXEC_HIST_GLOBALS.unknown_error_id, v_error_msg, cv_server_name,
         cv_procedure_name);
      --pn_return_code := pkg_bottler_globals.unsuccessful_execution;
      pn_error_number := 1;
      pv_error_message := v_error_msg;
      ROLLBACK;
      RAISE;
END;
/



-- End of DDL Script for Procedure ENGINE.SP_FLUSH_OUTBOUND_CMD_QUEUE

-- Start of DDL Script for Procedure ENGINE.SP_MACHINE_COMMAND_IN_CLEANUP
-- Generated 8-Sep-2005 10:41:27 from ENGINE@USADBD03.USATECH.COM

CREATE OR REPLACE 
PROCEDURE engine.sp_machine_command_in_cleanup
AS 
BEGIN

     /* Move Errors to Hist table */
     INSERT INTO machine_command_inbound_hist
                  (inbound_id,
                   machine_id,
                   inbound_date,
                   inbound_command,
                   TIMESTAMP,
                   execute_date,
                   execute_cd,
                   inbound_msg_no,
                   num_times_executed)
     SELECT inbound_id,
                  machine_id,
                  inbound_date,
                  inbound_command,
                  TIMESTAMP,
                  execute_date,
                  execute_cd,
                  inbound_msg_no,
                  num_times_executed
     FROM machine_command_inbound
     WHERE SUBSTR(execute_cd, 1, 1) NOT IN ('N', 'F');

    /* Y's will be saved in Machine_command_hist - E's are Errors - P's are purged */
    DELETE FROM MACHINE_COMMAND_INBOUND
    WHERE execute_cd in ('Y', 'E', 'P')
    OR inbound_date < sysdate - (10 / (60 * 24));  --older than N minutes (10 in this case)

END;
/



-- End of DDL Script for Procedure ENGINE.SP_MACHINE_COMMAND_IN_CLEANUP

-- Start of DDL Script for Procedure ENGINE.SP_MACHINE_COMMAND_OUT_CLEANUP
-- Generated 8-Sep-2005 10:41:27 from ENGINE@USADBD03.USATECH.COM

CREATE OR REPLACE 
PROCEDURE engine.sp_machine_command_out_cleanup
AS 
BEGIN

    INSERT INTO machine_command_hist
    (command_id, modem_id, command, command_date, execute_date, execute_cd)
    SELECT command_id, Modem_id, command, command_date, execute_date, execute_cd
    FROM machine_command
    WHERE execute_cd IN ('Y', 'E');

    /* Y's will be saved in Machine_command_hist - C's will be deleted - E's are Errors*/
    DELETE FROM MACHINE_COMMAND WHERE execute_cd in  ('Y', 'C', 'E');
END;
/



-- End of DDL Script for Procedure ENGINE.SP_MACHINE_COMMAND_OUT_CLEANUP

-- Start of DDL Script for Function ENGINE.SF_SELECT_BEST_LOGIC_ENGINE
-- Generated 15-Mar-2005 16:29:49 from ENGINE@USADBD03.USATECH.COM

CREATE OR REPLACE 
FUNCTION engine.sf_select_best_logic_engine( 
    pn_net_layer_id IN engine.net_layer_device_session.session_id%type DEFAULT 0
) RETURN engine.logic_engine.logic_engine_id%type IS
    n_logic_engine_id   engine.logic_engine.logic_engine_id%type;
    n_engine_timeout_sec NUMBER(6,0) := 30;
    n_mci_consecutive_growth_max engine.logic_engine_stats.mci_consecutive_growth_cnt%TYPE := 25;
BEGIN
    BEGIN
		SELECT logic_engine_id
		INTO n_logic_engine_id
		FROM net_layer_device_session
		WHERE session_id = pn_net_layer_id;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			n_logic_engine_id := 0;
    END;
    BEGIN
		IF n_logic_engine_id = 0 THEN --find next best engine alternative (alive or dead)
			SELECT logic_engine_id
			INTO n_logic_engine_id
			FROM (
				SELECT rex.logic_engine_id, rex.logic_engine_active_yn_flag, MIN(resx.mci_consecutive_growth_cnt) grow_cnt
				FROM engine.logic_engine rex, engine.logic_engine_stats resx, engine.logic_engine_device_type_excl redtex
				WHERE rex.logic_engine_id = resx.logic_engine_id
				AND rex.logic_engine_id = redtex.logic_engine_id(+)
				AND rex.logic_engine_active_yn_flag = 'Y'
				AND NOT (	--check for inactive engines that were unable to set 'N' flag
					resx.engine_last_activity_ts + (n_engine_timeout_sec / 86400) < SYSDATE
					AND resx.mci_consecutive_growth_cnt > n_mci_consecutive_growth_max
				)
				GROUP BY rex.logic_engine_id, logic_engine_active_yn_flag
				ORDER BY logic_engine_active_yn_flag, grow_cnt, logic_engine_id
			) x
			WHERE ROWNUM = 1;
		END IF;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			n_logic_engine_id := 0;
	END;
	BEGIN
		IF n_logic_engine_id = 0 THEN --find next best engine alternative (alive or dead)
			SELECT logic_engine_id
			INTO n_logic_engine_id
			FROM (
				SELECT rex.logic_engine_id, rex.logic_engine_active_yn_flag, MIN(resx.mci_consecutive_growth_cnt) grow_cnt
				FROM logic_engine rex, engine.logic_engine_stats resx
				WHERE rex.logic_engine_id = resx.logic_engine_id
				AND rex.logic_engine_active_yn_flag = 'Y'
				GROUP BY rex.logic_engine_id, logic_engine_active_yn_flag
				ORDER BY logic_engine_active_yn_flag, grow_cnt, logic_engine_id
			) x
			WHERE ROWNUM = 1;
		END IF;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			n_logic_engine_id := 0;
	END;
	RETURN n_logic_engine_id;
END;
/



-- End of DDL Script for Function ENGINE.SF_SELECT_BEST_LOGIC_ENGINE

-- Start of DDL Script for Package ENGINE.PKG_ENGINE_MAINT
-- Generated 2-Jun-2005 12:53:49 from ENGINE@USADBD03.USATECH.COM

CREATE OR REPLACE 
PACKAGE engine.pkg_engine_maint IS

   PROCEDURE sp_start_net_layer
   (
      pn_stored_net_layer_id IN net_layer.net_layer_id%TYPE,
      ps_net_layer_desc IN net_layer.net_layer_desc%TYPE,
      pn_new_net_layer_id OUT net_layer.net_layer_id%TYPE
   );
   
   PROCEDURE sp_stop_net_layer
   (
      pn_stored_net_layer_id IN net_layer.net_layer_id%TYPE
   );
   
   PROCEDURE sp_start_device_session
   (
      pn_net_layer_id IN net_layer_device_session.net_layer_id%TYPE,
      ps_device_name IN net_layer_device_session.device_name%TYPE,
      ps_client_ip_addr IN net_layer_device_session.session_client_ip_addr%TYPE,
      ps_client_port IN net_layer_device_session.session_client_port%TYPE,
      pn_session_id OUT net_layer_device_session.session_id%TYPE
   );
   
   PROCEDURE sp_stop_device_session
   (
      pn_session_id IN net_layer_device_session.session_id%TYPE,
      pn_inbound_msg_cnt IN net_layer_device_session.session_inbound_msg_cnt%TYPE,
      pn_outbound_msg_cnt IN net_layer_device_session.session_outbound_msg_cnt%TYPE,
      pn_inbound_byte_cnt IN net_layer_device_session.session_inbound_byte_cnt%TYPE,
      pn_outbound_byte_cnt IN net_layer_device_session.session_outbound_byte_cnt%TYPE
   );


END;
/


CREATE OR REPLACE 
PACKAGE BODY        engine.pkg_engine_maint IS

   -- Logging contants
   cs_server_name         CONSTANT VARCHAR2 (30)                    := 'USADBP';
   cs_package_name        CONSTANT VARCHAR2 (30)                    := 'PKG_ENGINE_MAINT';
   -- Exception stuff
   vs_error_msg           exception_data.additional_information%TYPE;

   PROCEDURE sp_start_net_layer
   (
      pn_stored_net_layer_id IN net_layer.net_layer_id%TYPE,
      ps_net_layer_desc IN net_layer.net_layer_desc%TYPE,
      pn_new_net_layer_id OUT net_layer.net_layer_id%TYPE
   )
   AS
      cs_procedure_name             CONSTANT VARCHAR2 (50)                  := 'sp_start_net_layer';
      vn_net_layer_active_yn_flag   net_layer.net_layer_active_yn_flag%TYPE;

   BEGIN
      IF pn_stored_net_layer_id IS NULL OR pn_stored_net_layer_id <= 0 THEN
         SELECT SEQ_NET_LAYER_ID.NEXTVAL INTO pn_new_net_layer_id FROM dual;
         INSERT INTO net_layer(net_layer_id, net_layer_desc, net_layer_active_yn_flag) VALUES (pn_new_net_layer_id,ps_net_layer_desc, 'Y');

      ELSE
         SELECT net_layer.net_layer_active_yn_flag
         INTO vn_net_layer_active_yn_flag
         FROM net_layer
         WHERE net_layer.net_layer_id = pn_stored_net_layer_id;

         IF vn_net_layer_active_yn_flag = 'Y' THEN
            UPDATE net_layer
            SET net_layer_active_yn_flag = 'N'
            WHERE net_layer_id = pn_stored_net_layer_id;
         END IF;

         UPDATE net_layer
         SET net_layer_active_yn_flag = 'Y',
             net_layer_desc = ps_net_layer_desc
         WHERE net_layer.net_layer_id = pn_stored_net_layer_id;

         pn_new_net_layer_id := pn_stored_net_layer_id;
      END IF;

   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         SELECT SEQ_NET_LAYER_ID.NEXTVAL INTO pn_new_net_layer_id FROM dual;
         INSERT INTO net_layer(net_layer_id, net_layer_desc, net_layer_active_yn_flag) VALUES (pn_new_net_layer_id, ps_net_layer_desc, 'Y');

      WHEN OTHERS THEN
         vs_error_msg :=
               'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pkg_exception_processor.sp_log_exception
                                   (pkg_app_exec_hist_globals.unknown_error_id,
                                    vs_error_msg,
                                    cs_server_name,
                                    cs_package_name || '.' || cs_procedure_name
                                   );
    END;


   PROCEDURE sp_stop_net_layer
   (
      pn_stored_net_layer_id IN net_layer.net_layer_id%TYPE
   )
   AS
      cs_procedure_name             CONSTANT VARCHAR2 (50)                  := 'sp_stop_net_layer';

   BEGIN
      UPDATE net_layer
      SET net_layer.net_layer_active_yn_flag = 'N'
      WHERE net_layer.net_layer_id = pn_stored_net_layer_id;

   EXCEPTION
      WHEN OTHERS THEN
         vs_error_msg :=
               'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pkg_exception_processor.sp_log_exception
                                   (pkg_app_exec_hist_globals.unknown_error_id,
                                    vs_error_msg,
                                    cs_server_name,
                                    cs_package_name || '.' || cs_procedure_name
                                   );
   END;


   PROCEDURE sp_start_device_session
   (
      pn_net_layer_id IN net_layer_device_session.net_layer_id%TYPE,
      ps_device_name IN net_layer_device_session.device_name%TYPE,
      ps_client_ip_addr IN net_layer_device_session.session_client_ip_addr%TYPE,
      ps_client_port IN net_layer_device_session.session_client_port%TYPE,
      pn_session_id OUT net_layer_device_session.session_id%TYPE
   )
   AS
      cs_procedure_name             CONSTANT VARCHAR2 (50)                  := 'sp_start_device_session';

   BEGIN
      SELECT seq_net_layer_device_sess_id.NEXTVAL INTO pn_session_id FROM dual;
      INSERT INTO net_layer_device_session
      (
         session_id,
         net_layer_id,
         device_name,
         session_client_ip_addr,
         session_client_port
      )
      VALUES
      (
         pn_session_id,
         pn_net_layer_id,
         ps_device_name,
         ps_client_ip_addr,
         ps_client_port
      );
   END;


   PROCEDURE sp_stop_device_session
   (
      pn_session_id IN net_layer_device_session.session_id%TYPE,
      pn_inbound_msg_cnt IN net_layer_device_session.session_inbound_msg_cnt%TYPE,
      pn_outbound_msg_cnt IN net_layer_device_session.session_outbound_msg_cnt%TYPE,
      pn_inbound_byte_cnt IN net_layer_device_session.session_inbound_byte_cnt%TYPE,
      pn_outbound_byte_cnt IN net_layer_device_session.session_outbound_byte_cnt%TYPE
   )
   AS
      cs_procedure_name             CONSTANT VARCHAR2 (50)                  := 'sp_start_device_session';

   BEGIN
      UPDATE net_layer_device_session
      SET session_active_yn_flag = 'N',
      session_inbound_msg_cnt = pn_inbound_msg_cnt,
      session_outbound_msg_cnt = pn_outbound_msg_cnt,
      session_inbound_byte_cnt = pn_inbound_byte_cnt,
      session_outbound_byte_cnt = pn_outbound_byte_cnt
      WHERE session_id = pn_session_id;

   EXCEPTION
      WHEN OTHERS THEN
         vs_error_msg :=
               'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pkg_exception_processor.sp_log_exception
                                   (pkg_app_exec_hist_globals.unknown_error_id,
                                    vs_error_msg,
                                    cs_server_name,
                                    cs_package_name || '.' || cs_procedure_name
                                   );
   END;
END;
/


-- End of DDL Script for Package ENGINE.PKG_ENGINE_MAINT

-- Start of DDL Script for Trigger ENGINE.TIA_MACHINE_CMD_INBOUND
-- Generated 8-Sep-2005 10:34:15 from ENGINE@USADBD03.USATECH.COM

CREATE OR REPLACE TRIGGER engine.tia_machine_cmd_inbound
 AFTER
  INSERT
 ON engine.machine_cmd_inbound
REFERENCING NEW AS NEW OLD AS OLD
BEGIN
    DECLARE
        n_logic_engine_id logic_engine.logic_engine_id%TYPE;
        n_cur_row_cnt NUMBER(6,0);
        CURSOR c_logic_engine_info IS
       	    SELECT logic_engine_id, COUNT(logic_engine_id)
            INTO n_logic_engine_id, n_cur_row_cnt
            FROM machine_cmd_inbound
            GROUP BY logic_engine_id;
    BEGIN
        -- logic distributed design stats update
        OPEN c_logic_engine_info;
        LOOP
            FETCH c_logic_engine_info INTO n_logic_engine_id, n_cur_row_cnt;
            EXIT WHEN c_logic_engine_info%NOTFOUND;

        	UPDATE logic_engine_stats
        	SET mci_last_row_cnt = n_cur_row_cnt,
                mci_consecutive_growth_ts = DECODE( --(d1 - d2) - ABS(d1 - d2) ==> 0=d1>=d2, !0=d1<d2
					(mci_last_row_cnt - n_cur_row_cnt) - ABS(mci_last_row_cnt - n_cur_row_cnt),
					0,
					mci_consecutive_growth_ts,	--mci_last_row_cnt >= n_cur_row_cnt
					SYSDATE --mci_last_row_cnt < n_cur_row_cnt
				)
                /*mci_consecutive_growth_cnt = DECODE(
                    mci_last_row_cnt - n_cur_row_cnt,
                    0,
                    mci_consecutive_growth_cnt,  --mci_last_row_cnt = n_cur_row_cnt
                    DECODE( --(d1 - d2) - ABS(d1 - d2) ==> 0=d1>=d2, !0=d1<d2
                        (mci_last_row_cnt - n_cur_row_cnt) - ABS(mci_last_row_cnt - n_cur_row_cnt),
                        0,
                        0,	--mci_last_row_cnt > n_cur_row_cnt
                        mci_consecutive_growth_cnt + 1	--mci_last_row_cnt < n_cur_row_cnt
                    )
                )*/
        	WHERE logic_engine_id = n_logic_engine_id;
        END LOOP;
        CLOSE c_logic_engine_info;
    END;
END;
/


-- End of DDL Script for Trigger ENGINE.TIA_MACHINE_CMD_INBOUND

-- Start of DDL Script for Trigger ENGINE.TRAI_LOGIC_ENGINE
-- Generated 8-Sep-2005 10:34:15 from ENGINE@USADBD03.USATECH.COM

CREATE OR REPLACE TRIGGER engine.trai_logic_engine
 AFTER
  INSERT
 ON engine.logic_engine
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
	-- logic distributed design stats default values
	INSERT INTO logic_engine_stats (
		logic_engine_id,
		mci_last_row_cnt_activity_ts,
		mci_last_row_cnt,
		mci_consecutive_growth_ts,
		mci_consecutive_growth_cnt,
		engine_last_activity_ts,
		engine_session_processed_msgs
	)
	VALUES (
		:NEW.logic_engine_id,
		SYSDATE,
		0,
		SYSDATE,
		0,
		SYSDATE,
		0
	); 
END;
/


-- End of DDL Script for Trigger ENGINE.TRAI_LOGIC_ENGINE

-- Start of DDL Script for Trigger ENGINE.TRBI_LOGIC_ENGINE
-- Generated 8-Sep-2005 10:34:15 from ENGINE@USADBD03.USATECH.COM

CREATE OR REPLACE TRIGGER engine.trbi_logic_engine
 BEFORE
  INSERT
 ON engine.logic_engine
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   IF :NEW.logic_engine_id IS NULL
   THEN
      SELECT seq_logic_engine_id.NEXTVAL
        INTO :NEW.logic_engine_id
        FROM DUAL;
   END IF;

   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/


-- End of DDL Script for Trigger ENGINE.TRBI_LOGIC_ENGINE

-- Start of DDL Script for Trigger ENGINE.TRBI_LOGIC_ENGINE_DEVICE_TYPE_
-- Generated 8-Sep-2005 10:34:15 from ENGINE@USADBD03.USATECH.COM

CREATE OR REPLACE TRIGGER engine.trbi_logic_engine_device_type_
 BEFORE
  INSERT
 ON engine.logic_engine_device_type_excl
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/


-- End of DDL Script for Trigger ENGINE.TRBI_LOGIC_ENGINE_DEVICE_TYPE_

-- Start of DDL Script for Trigger ENGINE.TRBI_LOGIC_ENGINE_STATS
-- Generated 8-Sep-2005 10:34:15 from ENGINE@USADBD03.USATECH.COM

CREATE OR REPLACE TRIGGER engine.trbi_logic_engine_stats
 BEFORE
  INSERT
 ON engine.logic_engine_stats
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/


-- End of DDL Script for Trigger ENGINE.TRBI_LOGIC_ENGINE_STATS

-- Start of DDL Script for Trigger ENGINE.TRBI_MACHINE_CMD_INBOUND
-- Generated 8-Sep-2005 10:34:15 from ENGINE@USADBD03.USATECH.COM

CREATE OR REPLACE TRIGGER engine.trbi_machine_cmd_inbound
 BEFORE
  INSERT
 ON engine.machine_cmd_inbound
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
	IF (:NEW.inbound_id IS NULL) THEN
		SELECT seq_machine_cmd_inbound_id.NEXTVAL
		INTO :NEW.inbound_id
		FROM DUAL;
	END IF;

	IF (:NEW.inbound_date IS NULL) THEN
		:NEW.inbound_date := SYSDATE;
	END IF;

	:NEW.num_times_executed := 0;

	-- Execute_CD will be null unless it is populated with an 'S'
	IF (:NEW.execute_cd IS NULL) THEN
		:NEW.execute_cd := 'N';
	END IF;

	:NEW.original_execute_cd := :NEW.execute_cd;
	:NEW.insert_ts := SYSDATE;
	:NEW.logic_engine_id := 0;

	-- logic distributed design engine choice
	IF (:NEW.session_id IS NULL) THEN
		BEGIN
			SELECT MAX(session_id)
			INTO :NEW.session_id
			FROM net_layer_device_session
            WHERE device_name = :NEW.machine_id;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				:NEW.session_id := NULL;
		END;
	END IF;
    BEGIN
        SELECT sf_select_best_logic_engine(:NEW.session_id)
        INTO :NEW.logic_engine_id
        FROM dual;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            :NEW.logic_engine_id := 0;
    END;

	-- logic distributed design stats update
	UPDATE logic_engine_stats
	SET mci_last_row_cnt_activity_ts = SYSDATE,
		mci_consecutive_growth_cnt = mci_consecutive_growth_cnt + 1
	WHERE logic_engine_id = :NEW.logic_engine_id;
END;
/


-- End of DDL Script for Trigger ENGINE.TRBI_MACHINE_CMD_INBOUND

-- Start of DDL Script for Trigger ENGINE.TRBI_MACHINE_CMD_OUTBOUND
-- Generated 8-Sep-2005 10:34:15 from ENGINE@USADBD03.USATECH.COM

CREATE OR REPLACE TRIGGER engine.trbi_machine_cmd_outbound
 BEFORE
  INSERT
 ON engine.machine_cmd_outbound
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
	IF (:New.command_id IS NULL) THEN
		SELECT seq_machine_cmd_outbound_id.NEXTVAL
		INTO :NEW.command_id
		FROM dual;
	END IF;

	:NEW.command_date := SYSDATE;

	/*Execute_CD will be null unless it is populated with an 'S'*/
	IF (:NEW.execute_cd is null) THEN
		:NEW.execute_cd := 'N';
	END IF;
END;
/


-- End of DDL Script for Trigger ENGINE.TRBI_MACHINE_CMD_OUTBOUND

-- Start of DDL Script for Trigger ENGINE.TRBI_NET_LAYER
-- Generated 8-Sep-2005 10:34:15 from ENGINE@USADBD03.USATECH.COM

CREATE OR REPLACE TRIGGER engine.trbi_net_layer
 BEFORE
  INSERT
 ON engine.net_layer
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   IF :NEW.net_layer_id IS NULL
   THEN
      SELECT seq_net_layer_id.NEXTVAL
        INTO :NEW.net_layer_id
        FROM DUAL;
   END IF;

   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/


-- End of DDL Script for Trigger ENGINE.TRBI_NET_LAYER

-- Start of DDL Script for Trigger ENGINE.TRBI_NET_LAYER_DEVICE_SESSION
-- Generated 8-Sep-2005 10:34:15 from ENGINE@USADBD03.USATECH.COM

CREATE OR REPLACE TRIGGER engine.trbi_net_layer_device_session
 BEFORE
  INSERT
 ON engine.net_layer_device_session
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
	SELECT SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO :NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;

   IF :NEW.session_id IS NULL
   THEN
      SELECT seq_net_layer_device_sess_id.NEXTVAL
        INTO :NEW.session_id
        FROM DUAL;
   END IF;

	-- logic distributed design engine choice
	BEGIN
		SELECT sf_select_best_logic_engine()
		INTO :NEW.logic_engine_id
		FROM dual;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				:NEW.logic_engine_id := 0;
	END;
END;
/


-- End of DDL Script for Trigger ENGINE.TRBI_NET_LAYER_DEVICE_SESSION

-- Start of DDL Script for Trigger ENGINE.TRBU_LOGIC_ENGINE
-- Generated 8-Sep-2005 10:34:15 from ENGINE@USADBD03.USATECH.COM

CREATE OR REPLACE TRIGGER engine.trbu_logic_engine
 BEFORE
  UPDATE
 ON engine.logic_engine
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
     INTO :NEW.created_by,
          :NEW.created_ts,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;

	-- logic distributed design stats default values
	IF :NEW.logic_engine_active_yn_flag = 'Y' THEN
		UPDATE logic_engine_stats
		SET mci_last_row_cnt = 0,
            mci_last_row_cnt_activity_ts = SYSDATE,
			mci_consecutive_growth_ts = SYSDATE,
			mci_consecutive_growth_cnt = 0,
			engine_last_activity_ts = SYSDATE
		WHERE logic_engine_id = :OLD.logic_engine_id;
	END IF;
END;
/


-- End of DDL Script for Trigger ENGINE.TRBU_LOGIC_ENGINE

-- Start of DDL Script for Trigger ENGINE.TRBU_LOGIC_ENGINE_DEVICE_TYPE_
-- Generated 8-Sep-2005 10:34:15 from ENGINE@USADBD03.USATECH.COM

CREATE OR REPLACE TRIGGER engine.trbu_logic_engine_device_type_
 BEFORE
  UPDATE
 ON engine.logic_engine_device_type_excl
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
     INTO :NEW.created_by,
          :NEW.created_ts,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/


-- End of DDL Script for Trigger ENGINE.TRBU_LOGIC_ENGINE_DEVICE_TYPE_

-- Start of DDL Script for Trigger ENGINE.TRBU_LOGIC_ENGINE_STATS
-- Generated 8-Sep-2005 10:34:15 from ENGINE@USADBD03.USATECH.COM

CREATE OR REPLACE TRIGGER engine.trbu_logic_engine_stats
 BEFORE
  UPDATE
 ON engine.logic_engine_stats
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
     INTO :NEW.created_by,
          :NEW.created_ts,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/


-- End of DDL Script for Trigger ENGINE.TRBU_LOGIC_ENGINE_STATS

-- Start of DDL Script for Trigger ENGINE.TRBU_MACHINE_CMD_INBOUND
-- Generated 8-Sep-2005 10:34:15 from ENGINE@USADBD03.USATECH.COM

CREATE OR REPLACE TRIGGER engine.trbu_machine_cmd_inbound
 BEFORE
  UPDATE
 ON engine.machine_cmd_inbound
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   :NEW.update_ts := SYSDATE;
   :NEW.execute_date := SYSDATE;

   /* Only allow 10 tries to execute a command */
   IF :NEW.num_times_executed > 10 THEN
      :NEW.execute_cd := 'X';
   END IF;

	-- logic distributed design stats update
    IF (:NEW.execute_cd IS NOT NULL) THEN
		UPDATE logic_engine_stats
		SET mci_last_row_cnt_activity_ts = DECODE(    --update when row is scheduled to be deleted (by after update trigger)
                :NEW.execute_cd,
                'N',
                mci_last_row_cnt_activity_ts,
                'F',
                mci_last_row_cnt_activity_ts,
                SYSDATE
            ),
            mci_consecutive_growth_cnt = DECODE(  --update on states logic engine uses to indicate msg completion
				:NEW.execute_cd,
				'Y',
				0,
				'E',
				0,
				'P',
				0,
				mci_consecutive_growth_cnt
			),
			engine_last_activity_ts = DECODE(  --update on all states logic engine uses
				:NEW.execute_cd,
				'Y',
				SYSDATE,
				'E',
				SYSDATE,
				'P',
				SYSDATE,
				engine_last_activity_ts
			),
			engine_session_processed_msgs = DECODE(  --update on states logic engine uses to indicate successfully handled msgs
				:NEW.execute_cd,
				'Y',
				engine_session_processed_msgs + 1,
				'E',
				engine_session_processed_msgs + 1,
				engine_session_processed_msgs
			)
		WHERE logic_engine_id = :OLD.logic_engine_id;
    END IF;
END;
/


-- End of DDL Script for Trigger ENGINE.TRBU_MACHINE_CMD_INBOUND

-- Start of DDL Script for Trigger ENGINE.TRBU_MACHINE_CMD_OUTBOUND
-- Generated 8-Sep-2005 10:34:15 from ENGINE@USADBD03.USATECH.COM

CREATE OR REPLACE TRIGGER engine.trbu_machine_cmd_outbound
 BEFORE
   UPDATE OF execute_cd
 ON engine.machine_cmd_outbound
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
	:NEW.execute_date := SYSDATE;
END;
/


-- End of DDL Script for Trigger ENGINE.TRBU_MACHINE_CMD_OUTBOUND

-- Start of DDL Script for Trigger ENGINE.TRBU_NET_LAYER
-- Generated 8-Sep-2005 10:34:15 from ENGINE@USADBD03.USATECH.COM

CREATE OR REPLACE TRIGGER engine.trbu_net_layer
 BEFORE
  UPDATE
 ON engine.net_layer
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
     INTO :NEW.created_by,
          :NEW.created_ts,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;

    /* mark device session records inactive */
    IF (:NEW.net_layer_active_yn_flag = 'N') THEN
        UPDATE net_layer_device_session
        SET session_active_yn_flag = 'N'
        WHERE net_layer_id = :OLD.net_layer_id;
   END IF;

END;
/


-- End of DDL Script for Trigger ENGINE.TRBU_NET_LAYER

-- Start of DDL Script for Trigger ENGINE.TRBU_NET_LAYER_DEVICE_SESSION
-- Generated 8-Sep-2005 10:34:15 from ENGINE@USADBD03.USATECH.COM

CREATE OR REPLACE TRIGGER engine.trbu_net_layer_device_session
 BEFORE
  UPDATE
 ON engine.net_layer_device_session
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
     INTO :NEW.created_by,
          :NEW.created_ts,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;

	/* Copy completed sessions to Hist table */
	IF (:NEW.session_active_yn_flag = 'N') THEN
        IF :NEW.session_end_ts IS NULL THEN
            :NEW.session_end_ts := SYSDATE;
        END IF;
		INSERT INTO net_layer_device_session_hist(
			session_id,
			net_layer_id,
			device_name,
			session_active_yn_flag,
			logic_engine_id,
			session_start_ts,
			session_end_ts,
			session_client_ip_addr,
			session_client_port,
			session_inbound_msg_cnt,
			session_outbound_msg_cnt,
			session_inbound_byte_cnt,
			session_outbound_byte_cnt,
            created_by,
            created_ts,
            last_updated_ts,
            last_updated_by
		) VALUES (
			:NEW.session_id,
			:NEW.net_layer_id,
			:NEW.device_name,
			:NEW.session_active_yn_flag,
			:NEW.logic_engine_id,
			:NEW.session_start_ts,
			:NEW.session_end_ts,
			:NEW.session_client_ip_addr,
			:NEW.session_client_port,
			:NEW.session_inbound_msg_cnt,
			:NEW.session_outbound_msg_cnt,
			:NEW.session_inbound_byte_cnt,
			:NEW.session_outbound_byte_cnt,
            :NEW.created_by,
            :NEW.created_ts,
            :NEW.last_updated_ts,
            :NEW.last_updated_by
		);
	END IF;
END;
/


-- End of DDL Script for Trigger ENGINE.TRBU_NET_LAYER_DEVICE_SESSION

-- Start of DDL Script for Trigger ENGINE.TUA_MACHINE_CMD_INBOUND
-- Generated 8-Sep-2005 10:34:15 from ENGINE@USADBD03.USATECH.COM

CREATE OR REPLACE TRIGGER engine.tua_machine_cmd_inbound
 AFTER
  UPDATE
 ON engine.machine_cmd_inbound
REFERENCING NEW AS NEW OLD AS OLD
BEGIN
	DECLARE
		n_logic_engine_id logic_engine.logic_engine_id%TYPE;
		n_cur_row_cnt NUMBER(6,0);
		CURSOR c_logic_engine_info IS
			SELECT logic_engine_id, COUNT(logic_engine_id)
			INTO n_logic_engine_id, n_cur_row_cnt
			FROM machine_cmd_inbound
			GROUP BY logic_engine_id;
	BEGIN
		-- logic distributed design stats update (only mci row cnt)
		OPEN c_logic_engine_info;
		LOOP
			FETCH c_logic_engine_info INTO n_logic_engine_id, n_cur_row_cnt;
			EXIT WHEN c_logic_engine_info%NOTFOUND;

			UPDATE logic_engine_stats
			SET mci_last_row_cnt = n_cur_row_cnt
			WHERE logic_engine_id = n_logic_engine_id;
		END LOOP;
		CLOSE c_logic_engine_info;
	END;
END;
/


-- End of DDL Script for Trigger ENGINE.TUA_MACHINE_CMD_INBOUND

-- Start of DDL Script for Trigger ENGINE.TUA_NET_LAYER_DEVICE_SESSION
-- Generated 8-Sep-2005 10:34:15 from ENGINE@USADBD03.USATECH.COM

CREATE OR REPLACE TRIGGER engine.tua_net_layer_device_session
 AFTER
  UPDATE
 ON engine.net_layer_device_session
REFERENCING NEW AS NEW OLD AS OLD
BEGIN
	/* Purge net_layer_device_session of inactive sessions */
	DELETE FROM engine.net_layer_device_session WHERE session_active_yn_flag = 'N';
END;
/


-- End of DDL Script for Trigger ENGINE.TUA_NET_LAYER_DEVICE_SESSION
