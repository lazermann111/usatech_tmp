INSERT INTO engine.machine_cmd_inbound_hist (
	inbound_id,
	machine_id,
	inbound_date,
	inbound_command,
	timestamp,
	execute_date,
	execute_cd,
	inbound_msg_no,
	num_times_executed
) 
SELECT TO_NUMBER(inbound_id),
	machine_id,
	inbound_date,
	inbound_command,
	timestamp,
	execute_date,
	execute_cd,
	inbound_msg_no,
	num_times_executed
FROM tazdba.machine_command_inbound_hist
WHERE inbound_date >= SYSDATE - 90;

INSERT INTO machine_command_hist
SELECT TO_NUMBER(command_id),
	modem_id,
	command,
	command_date,
	execute_date,
	execute_cd
FROM tazdba.machine_command_hist
WHERE command_id NOT IN (
    SELECT command_id FROM (
        SELECT MAX(command_date) command_date, command_id 
        FROM tazdba.machine_command_hist 
        GROUP BY command_id 
        HAVING count(*) > 1
    )
)
AND command_date >= SYSDATE - 90;
