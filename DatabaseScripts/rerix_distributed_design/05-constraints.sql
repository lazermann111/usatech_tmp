ALTER TABLE engine.logic_engine_device_type_excl ADD CONSTRAINT fk_logic_engine_ledte FOREIGN KEY (logic_engine_id) REFERENCES engine.logic_engine(logic_engine_id);
ALTER TABLE engine.logic_engine_device_type_excl ADD CONSTRAINT fk_device_type_ledte FOREIGN KEY (device_type_id) REFERENCES device.device_type(device_type_id);

ALTER TABLE engine.logic_engine_stats ADD CONSTRAINT fk_logic_engine_les FOREIGN KEY (logic_engine_id) REFERENCES engine.logic_engine(logic_engine_id);

ALTER TABLE engine.net_layer_device_session ADD CONSTRAINT fk_net_layer_nlds FOREIGN KEY (net_layer_id) REFERENCES engine.net_layer(net_layer_id);
ALTER TABLE engine.net_layer_device_session ADD CONSTRAINT fk_logic_engine_nlds FOREIGN KEY (logic_engine_id) REFERENCES engine.logic_engine(logic_engine_id);
ALTER TABLE engine.net_layer_device_session_hist ADD CONSTRAINT fk_net_layer_nldsh FOREIGN KEY (net_layer_id) REFERENCES engine.net_layer(net_layer_id);
ALTER TABLE engine.net_layer_device_session_hist ADD CONSTRAINT fk_logic_engine_nldsh FOREIGN KEY (logic_engine_id) REFERENCES engine.logic_engine(logic_engine_id);

ALTER TABLE engine.machine_cmd_inbound ADD CONSTRAINT fk_logic_engine_mci FOREIGN KEY (logic_engine_id) REFERENCES engine.logic_engine(logic_engine_id);

ALTER TABLE engine.machine_cmd_inbound_hist ADD CONSTRAINT fk_logic_engine_mcih FOREIGN KEY (logic_engine_id) REFERENCES engine.logic_engine(logic_engine_id);
