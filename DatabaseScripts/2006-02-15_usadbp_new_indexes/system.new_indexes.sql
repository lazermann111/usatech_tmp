CREATE INDEX device.idx_file_transfer_name ON device.file_transfer
  (
    file_transfer_name              ASC
  )
  PCTFREE     10
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  device_indx
  STORAGE   (
    INITIAL     1048576
    NEXT        1048576
    PCTINCREASE 0
    MINEXTENTS  1
    MAXEXTENTS  2147483645
  )
/


CREATE INDEX pss.ix_tran_upload_ts ON pss.tran
  (
    tran_upload_ts                  ASC
  )
  PCTFREE     10
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  pss_indx
  STORAGE   (
    INITIAL     5242880
    NEXT        5242880
    PCTINCREASE 0
    MINEXTENTS  1
    MAXEXTENTS  2147483645
  )
/
