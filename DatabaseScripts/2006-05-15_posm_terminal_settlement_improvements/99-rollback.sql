-- Start of DDL Script for Trigger ENGINE.TRBU_MACHINE_CMD_INBOUND
-- Generated 5/23/2006 4:58:28 PM from ENGINE@USADBP.USATECH.COM

-- Drop the old instance of TRBU_MACHINE_CMD_INBOUND
DROP TRIGGER engine.trbu_machine_cmd_inbound
/

CREATE OR REPLACE TRIGGER engine.trbu_machine_cmd_inbound
 BEFORE
  UPDATE
 ON engine.machine_cmd_inbound
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   :NEW.update_ts := SYSDATE;
   :NEW.execute_date := SYSDATE;

   /* Only allow 10 tries to execute a command */
   IF :NEW.num_times_executed > 10 THEN
      :NEW.execute_cd := 'X';
   END IF;

	-- logic distributed design stats update
/*    IF (:NEW.execute_cd IS NOT NULL) THEN
		UPDATE logic_engine_stats
		SET mci_last_row_cnt_activity_ts = DECODE(    --update when row is scheduled to be deleted (by after update trigger)
                :NEW.execute_cd,
                'N',
                mci_last_row_cnt_activity_ts,
                'F',
                mci_last_row_cnt_activity_ts,
                SYSDATE
            ),
            mci_consecutive_growth_cnt = DECODE(  --update on states logic engine uses to indicate msg completion
				:NEW.execute_cd,
				'Y',
				0,
				'E',
				0,
				'P',
				0,
				mci_consecutive_growth_cnt
			),
			engine_last_activity_ts = DECODE(  --update on all states logic engine uses
				:NEW.execute_cd,
				'Y',
				SYSDATE,
				'E',
				SYSDATE,
				'P',
				SYSDATE,
				engine_last_activity_ts
			),
			engine_session_processed_msgs = DECODE(  --update on states logic engine uses to indicate successfully handled msgs
				:NEW.execute_cd,
				'Y',
				engine_session_processed_msgs + 1,
				'E',
				engine_session_processed_msgs + 1,
				engine_session_processed_msgs
			)
		WHERE logic_engine_id = :OLD.logic_engine_id;
    END IF;
*/
   /* Copy Processed messages or Errors (X) to Hist table and mark to be deleted */
   IF (:NEW.execute_cd NOT IN ('N', 'F')) THEN
      INSERT INTO engine.machine_cmd_inbound_hist
                  (inbound_id,
                   machine_id,
                   inbound_date,
                   inbound_command,
                   TIMESTAMP,
                   execute_date,
                   execute_cd,
                   inbound_msg_no,
                   num_times_executed,
                   logic_engine_id,
                   session_id)
           VALUES (:NEW.inbound_id,
                   :NEW.machine_id,
                   :NEW.inbound_date,
                   :NEW.inbound_command,
                   :NEW.TIMESTAMP,
                   :NEW.execute_date,
                   :NEW.execute_cd,
                   :NEW.inbound_msg_no,
                   :NEW.num_times_executed,
                   :NEW.logic_engine_id,
                   :NEW.session_id);
                   
      /* Mark message complete (to be deleted) */
      :NEW.execute_cd := 'C';
   END IF;
END;
/


-- End of DDL Script for Trigger ENGINE.TRBU_MACHINE_CMD_INBOUND
