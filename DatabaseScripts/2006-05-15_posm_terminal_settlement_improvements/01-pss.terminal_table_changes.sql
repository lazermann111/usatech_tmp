/* Formatted on 2006/05/15 17:24 (Formatter Plus v4.8.0) */
/* create new terminal properties */
ALTER TABLE pss.terminal ADD (terminal_min_batch_num       INTEGER);
ALTER TABLE pss.terminal ADD (terminal_min_batch_close_hr  INTEGER);
ALTER TABLE pss.terminal ADD (terminal_max_batch_close_hr  INTEGER);

/* update all existing terminals to some logical defaults (as all are FHMS) */
UPDATE terminal
   SET terminal_min_batch_num = 850,
       terminal_min_batch_close_hr = 24,
       terminal_max_batch_close_hr = 25;

/* set final constraints */
ALTER TABLE pss.terminal MODIFY terminal_min_batch_num NOT NULL;
ALTER TABLE pss.terminal MODIFY terminal_min_batch_close_hr NOT NULL;
ALTER TABLE pss.terminal MODIFY terminal_max_batch_close_hr NOT NULL;

/* modify size of refund table description field */
ALTER TABLE pss.refund MODIFY refund_desc VARCHAR2(4000);
ALTER TABLE pss.tran_line_item MODIFY tran_line_item_desc VARCHAR2(4000);
