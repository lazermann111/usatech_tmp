-- if update NOT ok
rollback;

delete from host h where exists (
	select count(1)
	from host_type ht, device_type_host_type dtht
	where device_type_host_type_cd = 'B'
	and ht.host_type_id = dtht.host_Type_id
	and ht.host_type_id = h.host_type_id
);

alter table device.device drop column def_base_host_type_id;
alter table device.device drop column def_base_host_equipment_id;
alter table device.device drop column def_prim_host_type_id;
alter table device.device drop column def_prim_host_equipment_id;
alter table device.host drop column host_equipment_id;

drop table device.host_type_host_equipment;
drop public synonym host_type_host_equipment;

drop table device.host_equipment;
drop public synonym host_equipment;
drop sequence device.seq_host_equipment_id;

drop table device.device_type_host_type;
drop public synonym device_type_host_type;

delete from device.host_type_host_group_type hthgt
where exists (
	select hgt.host_group_type_id
	from host_group_type hgt
	where hgt.host_group_type_cd = 'BAST_HOST'
);

delete from device.host_group_type where host_group_type_cd = 'BAST_HOST';

REM ----------------------------------------------------------------------
/* disable required constraints */
disable constraint device.host.FK_HOST_HOST_TYPE;
disable constraint device.host_type_host_group_type.FK_HOST_TYPE_ID;
disable constraint pss.tran_line_item.FK_TRAN_LINE_ITEM_HOST;

-- Start of DDL Script for Table DEVICE.HOST_TYPE
-- Generated 10-Apr-2006 10:02:17 from DEVICE@USADBD02.USATECH.COM

REM ----------------------------------------------------------------------
/* recreate host type table */
-- Drop the old instance of HOST_TYPE
DROP TABLE device.host_type
/

RENAME TABLE device.host_type_bak to host_type;

-- Drop the old synonym HOST_TYPE
DROP PUBLIC SYNONYM HOST_TYPE
/

-- Create synonym HOST_TYPE
CREATE PUBLIC SYNONYM host_type
  FOR device.host_type
/

-- Grants for Table
GRANT SELECT ON device.host_type TO public
WITH GRANT OPTION
/



-- Indexes for DEVICE.HOST_TYPE

CREATE INDEX device.inx_host_name$host_type_id ON device.host_type
  (
    host_type_desc                  ASC,
    host_type_id                    ASC
  )
  PCTFREE     10
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  device_data
  STORAGE   (
    INITIAL     5242880
    NEXT        1048576
    PCTINCREASE 0
    MINEXTENTS  1
    MAXEXTENTS  2147483645
  )
/

CREATE INDEX device.inx_host_type_id$host_name ON device.host_type
  (
    host_type_id                    ASC,
    host_type_desc                  ASC
  )
  PCTFREE     10
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  device_data
  STORAGE   (
    INITIAL     5242880
    NEXT        1048576
    PCTINCREASE 0
    MINEXTENTS  1
    MAXEXTENTS  2147483645
  )
/



-- Triggers for DEVICE.HOST_TYPE

CREATE OR REPLACE TRIGGER device.trbu_host_type
 BEFORE
  UPDATE
 ON device.host_type
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
Begin
SELECT
           :old.created_by,
           :old.created_ts,
           sysdate,
           user
      into
           :new.created_by,
           :new.created_ts,

           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;
End;
/

CREATE OR REPLACE TRIGGER device.trbi_host_type
 BEFORE
  INSERT
 ON device.host_type
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
Begin

    IF :new.HOST_TYPE_id IS NULL THEN

      SELECT seq_HOST_TYPE_id.nextval
        into :new.HOST_TYPE_id
        FROM dual;

    END IF;

 SELECT    sysdate,
           user,
           sysdate,
           user
      into :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;
End;
/


-- Comments for DEVICE.HOST_TYPE

COMMENT ON TABLE device.host_type IS 'Contains a listing of all the different types of hosts.'
/
COMMENT ON COLUMN device.host_type.host_default_complete_minut IS 'Will be removed.'
/
COMMENT ON COLUMN device.host_type.host_model_cd IS 'The model number/code of the host type.'
/
COMMENT ON COLUMN device.host_type.host_type_id IS 'Surrogate key generated by the system.  Unique identifier for each host type.  Not displayed to the end user and only for internal use.  Used to improve performance of queries.'
/
COMMENT ON COLUMN device.host_type.host_type_manufacturer IS 'The manufacturer of the particular host type.'
/

-- End of DDL Script for Table DEVICE.HOST_TYPE


REM ----------------------------------------------------------------------
/* recreate host table */
-- Drop the old instance of HOST_TYPE
DROP TABLE device.host
/

RENAME TABLE device.host_bak to host;

-- Drop the old synonym HOST
DROP PUBLIC SYNONYM HOST
/

-- Create synonym HOST
CREATE PUBLIC SYNONYM host
  FOR device.host
/

-- Grants for Table
GRANT SELECT ON device.host TO public
WITH GRANT OPTION
/



-- Indexes for DEVICE.HOST

CREATE INDEX device.if1_host ON device.host
  (
    host_type_id                    ASC
  )
  PCTFREE     10
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  device_data
  STORAGE   (
    INITIAL     5242880
    NEXT        1048576
    PCTINCREASE 0
    MINEXTENTS  1
    MAXEXTENTS  2147483645
  )
/



-- Constraints for DEVICE.HOST




-- Triggers for DEVICE.HOST

CREATE OR REPLACE TRIGGER device.trbu_host
 BEFORE
  UPDATE
 ON device.host
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   --if (instr(:new.host_status_flag, 2) <> 0 and
   --instr(:old.host_status_flag, 2) = 0) THEN

   --  :new.host_last_start_ts := sysdate;

   --end if;

   --if(:new.host_status_flag <> :old.host_status_flag) then
   --    :new.host_last_start_ts := sysdate;
   --end if;
   IF (    :NEW.host_status_cd = 2
       AND :OLD.host_status_cd <> 2) THEN
      :NEW.host_last_start_ts := SYSDATE;
   END IF;

   SELECT SYSDATE,
          USER
     INTO :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER device.trbi_host
 BEFORE
  INSERT
 ON device.host
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
Begin

    IF :new.HOST_id IS NULL THEN

      SELECT seq_HOST_id.nextval
        into :new.HOST_id
        FROM dual;

    END IF;

 SELECT    sysdate,
           user,
           sysdate,
           user
      into :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;
End;
/


-- Comments for DEVICE.HOST

COMMENT ON TABLE device.host IS 'A host is a logical unit that is monitored, controlled, and/or houses a device.  It is typically not owned or managed by USA Technologies.  Examples are laundry machines, vending machines, and fax machines.

'
/
COMMENT ON COLUMN device.host.device_id IS 'Surrogate key generated by the system.  Unique identifier for each device.  Not displayed to the end user and only for internal use.  Used to improve performance of queries.'
/
COMMENT ON COLUMN device.host.host_label_cd IS 'The text a label attached to the host.'
/
COMMENT ON COLUMN device.host.host_type_id IS 'Surrogate key generated by the system.  Unique identifier for each host type.  Not displayed to the end user and only for internal use.  Used to improve performance of queries.'
/

-- End of DDL Script for Table DEVICE.HOST

-- Foreign Key
ALTER TABLE device.host
ADD CONSTRAINT fk_host_host_type FOREIGN KEY (host_type_id)
REFERENCES DEVICE.host_type (host_type_id) ON DELETE SET NULL
/
ALTER TABLE device.host
ADD CONSTRAINT fk_host_device FOREIGN KEY (device_id)
REFERENCES DEVICE.device (device_id) ON DELETE SET NULL
/
-- End of DDL script for Foreign Key(s)


REM ----------------------------------------------------------------------
/* re-enable constraints */
enable constraint device.host.FK_HOST_HOST_TYPE;
enable constraint device.host_type_host_group_type.FK_HOST_TYPE_ID;
enable constraint pss.tran_line_item.FK_TRAN_LINE_ITEM_HOST;
