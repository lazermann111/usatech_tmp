/* Formatted on 2006/04/19 09:21 (Formatter Plus v4.8.0) */
GRANT SELECT ON device.seq_host_equipment_id TO web_user;
GRANT SELECT,INSERT,UPDATE ON device.host_equipment TO web_user;
GRANT SELECT,INSERT,UPDATE ON device.device_type_host_type TO web_user;
GRANT SELECT,INSERT,UPDATE ON device.host_type_host_equipment TO web_user;

GRANT SELECT,INSERT,UPDATE ON device.device_type_host_type TO pss;

GRANT SELECT on device.device_type to PSS;
