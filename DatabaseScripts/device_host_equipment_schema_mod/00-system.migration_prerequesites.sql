CREATE TABLE device.host_type_bak
    (host_type_id                   NUMBER(20,0) NOT NULL,
    host_type_desc                 VARCHAR2(60) NOT NULL,
    host_type_manufacturer         VARCHAR2(20) NOT NULL,
    host_model_cd                  VARCHAR2(10) NOT NULL,
    created_by                     VARCHAR2(30) NOT NULL,
    host_default_complete_minut    NUMBER(3,0) NOT NULL,
    created_ts                     DATE NOT NULL,
    last_updated_by                VARCHAR2(30) NOT NULL,
    last_updated_ts                DATE NOT NULL,
    host_type_cd                   VARCHAR2(2)
  ,
  CONSTRAINT PK_HOST_TYPE_bak
  PRIMARY KEY (host_type_id)
  USING INDEX)
  PCTFREE     10
  PCTUSED     40
  INITRANS    1
  MAXTRANS    255
  TABLESPACE  device_data
  STORAGE   (
    INITIAL     5242880
    NEXT        1048576
    PCTINCREASE 0
    MINEXTENTS  1
    MAXEXTENTS  2147483645
  )
/

insert into device.host_type_bak (
    select * from host_type
);


CREATE TABLE device.host_bak
    (host_id                        NUMBER(20,0) NOT NULL,
    host_last_start_ts             DATE,
    host_status_cd                 CHAR(1) DEFAULT 0

  NOT NULL,
    host_serial_cd                 VARCHAR2(20),
    host_est_complete_minut        NUMBER(3,0) NOT NULL,
    created_by                     VARCHAR2(30) NOT NULL,
    created_ts                     DATE NOT NULL,
    last_updated_by                VARCHAR2(30) NOT NULL,
    last_updated_ts                DATE NOT NULL,
    host_port_num                  NUMBER(2,0) NOT NULL,
    host_setting_updated_yn_flag   CHAR(1) NOT NULL,
    device_id                      NUMBER(20,0) NOT NULL,
    host_position_num              NUMBER(3,0) DEFAULT 0  NOT NULL,
    host_label_cd                  VARCHAR2(10),
    host_active_yn_flag            VARCHAR2(1),
    host_type_id                   NUMBER(20,0)
  ,
  CONSTRAINT PK_HOST_bak
  PRIMARY KEY (host_id)
  USING INDEX
  PCTFREE     10
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  device_data
  STORAGE   (
    INITIAL     5242880
    NEXT        1048576
    PCTINCREASE 0
    MINEXTENTS  1
    MAXEXTENTS  2147483645
  ))
  PCTFREE     10
  PCTUSED     40
  INITRANS    1
  MAXTRANS    255
  TABLESPACE  device_data
  STORAGE   (
    INITIAL     5242880
    NEXT        1048576
    PCTINCREASE 0
    MINEXTENTS  1
    MAXEXTENTS  2147483645
  )
/

insert into device.host_bak (
    select * from host
);

commit;
