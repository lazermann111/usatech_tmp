/* Formatted on 2006/04/20 18:22 (Formatter Plus v4.8.0) */
-- create base hosts for all devices of this type
INSERT INTO HOST
            (host_serial_cd, host_est_complete_minut, host_port_num,
             host_setting_updated_yn_flag, device_id, host_type_id,
             host_equipment_id)
   (SELECT device_serial_cd, 0, 0, 'N', device_id, dt.def_base_host_type_id,
           dt.def_base_host_equipment_id
      FROM device d, device_type dt
     WHERE d.device_type_id = dt.device_type_id);

-- create primary hosts for any devices of this type
INSERT INTO HOST
            (host_est_complete_minut, host_port_num,
             host_setting_updated_yn_flag, device_id, host_type_id,
             host_equipment_id)
   (SELECT 0, 1, 'N', device_id, dt.def_prim_host_type_id,
           dt.def_prim_host_equipment_id
      FROM device d, device_type dt
     WHERE d.device_type_id = dt.device_type_id
       AND dt.def_prim_host_type_id IS NOT NULL
       AND dt.def_prim_host_equipment_id IS NOT NULL
       AND d.device_id NOT IN (
              SELECT device_id
                FROM HOST h
               WHERE h.host_type_id = dt.def_prim_host_type_id
                 AND h.host_equipment_id = dt.def_prim_host_equipment_id));