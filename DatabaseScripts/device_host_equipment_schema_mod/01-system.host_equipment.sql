/* Formatted on 2006/04/10 10:44 (Formatter Plus v4.8.0) */
-- create host_equipment objects
CREATE SEQUENCE device.seq_host_equipment_id
    INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH 1;

CREATE TABLE DEVICE.HOST_EQUIPMENT
  (
    HOST_EQUIPMENT_ID     NUMBER(20,0),
    HOST_EQUIPMENT_MFGR   VARCHAR2(255)   not null,
    HOST_EQUIPMENT_MODEL  VARCHAR2(255)   not null,
    CREATED_BY            VARCHAR2(30)    not null,
    CREATED_TS            DATE            not null,
    LAST_UPDATED_BY       VARCHAR2(30)    not null,
    LAST_UPDATED_TS       DATE            not null,
    CONSTRAINT PK_HOST_EQUIPMENT primary key(HOST_EQUIPMENT_ID) USING INDEX TABLESPACE DEVICE_INDX,
    CONSTRAINT UK_HOST_EQUIPMENT_2 unique(HOST_EQUIPMENT_MFGR,HOST_EQUIPMENT_MODEL)
  )
  TABLESPACE DEVICE_DATA;

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER device.trbi_host_equipment
   BEFORE INSERT
   ON device.host_equipment
   FOR EACH ROW
BEGIN
   IF :NEW.host_equipment_id IS NULL
   THEN
      SELECT seq_host_equipment_id.NEXTVAL
        INTO :NEW.host_equipment_id
        FROM DUAL;
   END IF;

   SELECT SYSDATE, USER, SYSDATE,
          USER
     INTO :NEW.created_ts, :NEW.created_by, :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/
REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER device.trbu_host_equipment
   BEFORE UPDATE
   ON device.host_equipment
   FOR EACH ROW
BEGIN
   SELECT :OLD.created_by, :OLD.created_ts, SYSDATE,
          USER
     INTO :NEW.created_by, :NEW.created_ts, :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/
REM ----------------------------------------------------------------------

COMMENT ON TABLE device.host_equipment IS 'Represents host equipment, comprised of a manufacturer and model';

CREATE PUBLIC SYNONYM host_equipment FOR device.host_equipment;


-- create device_type_host_type objects
CREATE TABLE device.device_type_host_type
  (
    device_type_host_type_cd  CHAR(1)        NOT NULL,
    device_type_id            NUMBER(20,0)   NOT NULL,
    host_type_id              NUMBER(20,0)   NOT NULL,
    created_by                VARCHAR2(30)   NOT NULL,
    created_ts                DATE           NOT NULL,
    last_updated_by           VARCHAR2(30)   NOT NULL,
    last_updated_ts           DATE           NOT NULL,
    CONSTRAINT pk_device_type_host_type PRIMARY KEY(device_type_id,host_type_id) USING INDEX TABLESPACE device_indx,
    CONSTRAINT fk_device_type_htdt_id FOREIGN KEY(device_type_id) REFERENCES device_type(device_type_id),
    CONSTRAINT fk_device_type_ht_host_type_id FOREIGN KEY(host_type_id) REFERENCES host_type(host_type_id)
  )
  TABLESPACE device_data;

CREATE UNIQUE INDEX device.udx_device_type_host_type_1 ON device.device_type_host_type(device_type_host_type_cd,device_type_id) TABLESPACE device_indx;

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER device.trbi_device_type_host_type
   BEFORE INSERT
   ON device.device_type_host_type
   FOR EACH ROW
BEGIN
   SELECT SYSDATE, USER, SYSDATE,
          USER
     INTO :NEW.created_ts, :NEW.created_by, :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/
REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER device.trbu_device_type_host_type
   BEFORE UPDATE
   ON device.device_type_host_type
   FOR EACH ROW
BEGIN
   SELECT :OLD.created_by, :OLD.created_ts, SYSDATE,
          USER
     INTO :NEW.created_by, :NEW.created_ts, :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/
REM ----------------------------------------------------------------------

COMMENT ON TABLE device.device_type_host_type IS 'Represents code-based functional mapping of allowed host types for each device type';

CREATE PUBLIC SYNONYM device_type_host_type FOR device.device_type_host_type;


-- add host_type_host_equipment table
CREATE TABLE DEVICE.HOST_TYPE_HOST_EQUIPMENT
  (
    HOST_TYPE_ID       NUMBER(20,0),
    HOST_EQUIPMENT_ID  NUMBER(20,0),
    CREATED_BY         VARCHAR2(30)   not null,
    CREATED_TS         DATE           not null,
    LAST_UPDATED_BY    VARCHAR2(30)   not null,
    LAST_UPDATED_TS    DATE           not null,
    CONSTRAINT PK_HOST_TYPE_HOST_EQUIPMENT primary key(HOST_TYPE_ID,HOST_EQUIPMENT_ID) USING INDEX TABLESPACE DEVICE_INDX,
    CONSTRAINT FK_HOST_TYPE_HE_HOST_TYPE_ID foreign key(HOST_TYPE_ID) references HOST_TYPE(HOST_TYPE_ID),
    CONSTRAINT FK_HOST_TYPE_HE_HE_ID foreign key(HOST_EQUIPMENT_ID) references HOST_EQUIPMENT(HOST_EQUIPMENT_ID)
  )
  TABLESPACE DEVICE_DATA;

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER DEVICE.TRBI_HOST_TYPE_HOST_EQUIPMENT BEFORE INSERT ON DEVICE.HOST_TYPE_HOST_EQUIPMENT
  FOR EACH ROW 
BEGIN
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER DEVICE.TRBU_HOST_TYPE_HOST_EQUIPMENT BEFORE UPDATE ON DEVICE.HOST_TYPE_HOST_EQUIPMENT
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/


CREATE PUBLIC SYNONYM HOST_TYPE_HOST_EQUIPMENT FOR DEVICE.HOST_TYPE_HOST_EQUIPMENT;


-- add host_equipment_id column to HOST table
ALTER TABLE device.HOST ADD (
    host_equipment_id             NUMBER(20,0)
);
