/* Formatted on 2006/04/18 17:07 (Formatter Plus v4.8.0) */
/* data migration:	clean up host and host_type tables (eliminiating duplicates based on host_type_cd);
					create device_type_host_type records;
					insert default data into host_equipment table (unknown for all host_type_ids, esuds-related types)
*/

BEGIN
   DECLARE
      n_host_type_id                host_type.host_type_id%TYPE;
      v_host_type_cd                host_type.host_type_cd%TYPE;
      n_host_equipment_id           host_equipment.host_equipment_id%TYPE;
      n_unknown_host_equipment_id   host_equipment.host_equipment_id%TYPE;
      v_host_equipment_mfgr         host_equipment.host_equipment_mfgr%TYPE;
      v_host_equipment_model        host_equipment.host_equipment_model%TYPE;

      TYPE cv_type IS REF CURSOR;

      c_get_host_equipment_id       cv_type;

      CURSOR c_get_host_type_ids
      IS
         SELECT DISTINCT MAX (host_type_id) host_type_id, host_type_cd
                    FROM host_type
                GROUP BY host_type_cd
                ORDER BY host_type_cd;

      CURSOR c_get_seq_host_equipment_id
      IS
         SELECT seq_host_equipment_id.NEXTVAL
           FROM DUAL;

      CURSOR c_get_maytag_host_mode_cd
      IS
         SELECT DISTINCT DECODE (host_model_cd,
                                 '0', 'Unknown',
                                 host_model_cd
                                )
                    FROM HOST h, host_type ht
                   WHERE h.host_type_id = ht.host_type_id
                     AND host_type_cd IN ('W', 'D', 'S', 'U', 'G', 'H');
   BEGIN
      OPEN c_get_host_type_ids;

      -- create default (unknown) host_equipment
      OPEN c_get_seq_host_equipment_id;

      FETCH c_get_seq_host_equipment_id
       INTO n_unknown_host_equipment_id;

      CLOSE c_get_seq_host_equipment_id;

      INSERT INTO device.host_equipment
                  (host_equipment_id, host_equipment_mfgr,
                   host_equipment_model
                  )
           VALUES (n_unknown_host_equipment_id, 'Unknown',
                   'Unknown'
                  );

      -- process each host_type_cd
      LOOP
         FETCH c_get_host_type_ids
          INTO n_host_type_id, v_host_type_cd;

         EXIT WHEN c_get_host_type_ids%NOTFOUND;

		-- create unknown equipment association
		INSERT INTO device.host_type_host_equipment
					(host_type_id, host_equipment_id
					)
			 VALUES (n_host_type_id, n_unknown_host_equipment_id
					);

         -- host_equipment cases for this host_type
         IF v_host_type_cd IN
                            ('W', 'D', 'S', 'U', 'G', 'H') /* esuds Maytag */
         THEN
            v_host_equipment_mfgr := 'Maytag';

            -- maytag specific case of custom host_model_cd
            OPEN c_get_maytag_host_mode_cd;

            LOOP
               FETCH c_get_maytag_host_mode_cd
                INTO v_host_equipment_model;

               EXIT WHEN c_get_maytag_host_mode_cd%NOTFOUND;

               OPEN c_get_host_equipment_id
                FOR 'SELECT host_equipment_id
             	FROM host_equipment
         	WHERE host_equipment_mfgr = :v_host_equipment_mfgr
         	AND host_equipment_model = :v_host_equipment_model'
                USING v_host_equipment_mfgr, v_host_equipment_model;

               FETCH c_get_host_equipment_id
                INTO n_host_equipment_id;

               IF c_get_host_equipment_id%NOTFOUND
               THEN
                  OPEN c_get_seq_host_equipment_id;

                  FETCH c_get_seq_host_equipment_id
                   INTO n_host_equipment_id;

                  CLOSE c_get_seq_host_equipment_id;

                  INSERT INTO device.host_equipment
                              (host_equipment_id, host_equipment_mfgr,
                               host_equipment_model
                              )
                       VALUES (n_host_equipment_id, v_host_equipment_mfgr,
                               v_host_equipment_model
                              );
               END IF;

               CLOSE c_get_host_equipment_id;
            END LOOP;

            CLOSE c_get_maytag_host_mode_cd;

            -- create equipment association
            INSERT INTO device.host_type_host_equipment
                        (host_type_id, host_equipment_id
                        )
                 VALUES (n_host_type_id, n_host_equipment_id
                        );
         ELSIF v_host_type_cd IN ('A', 'R', 'I', 'J')  /* esuds Speed Queen */
         THEN
            v_host_equipment_mfgr := 'Speed Queen';
            v_host_equipment_model := 'Unknown';

            OPEN c_get_host_equipment_id
             FOR 'SELECT host_equipment_id
             	FROM host_equipment
         	WHERE host_equipment_mfgr = :v_host_equipment_mfgr
         	AND host_equipment_model = :v_host_equipment_model'
             USING v_host_equipment_mfgr, v_host_equipment_model;

            FETCH c_get_host_equipment_id
             INTO n_host_equipment_id;

            IF c_get_host_equipment_id%NOTFOUND
            THEN
               OPEN c_get_seq_host_equipment_id;

               FETCH c_get_seq_host_equipment_id
                INTO n_host_equipment_id;

               CLOSE c_get_seq_host_equipment_id;

               INSERT INTO device.host_equipment
                           (host_equipment_id, host_equipment_mfgr,
                            host_equipment_model
                           )
                    VALUES (n_host_equipment_id, v_host_equipment_mfgr,
                            v_host_equipment_model
                           );
            END IF;

            CLOSE c_get_host_equipment_id;

            -- create equipment association
            INSERT INTO device.host_type_host_equipment
                        (host_type_id, host_equipment_id
                        )
                 VALUES (n_host_type_id, n_host_equipment_id
                        );
         ELSIF v_host_type_cd IN ('T')                    /* T2 */
         THEN
            v_host_equipment_mfgr := 'USA Technologies, Inc.';
            v_host_equipment_model := 'Unknown';

            OPEN c_get_host_equipment_id
             FOR 'SELECT host_equipment_id
             	FROM host_equipment
         	WHERE host_equipment_mfgr = :v_host_equipment_mfgr
         	AND host_equipment_model = :v_host_equipment_model'
             USING v_host_equipment_mfgr, v_host_equipment_model;

            FETCH c_get_host_equipment_id
             INTO n_host_equipment_id;

            IF c_get_host_equipment_id%NOTFOUND
            THEN
               OPEN c_get_seq_host_equipment_id;

               FETCH c_get_seq_host_equipment_id
                INTO n_host_equipment_id;

               CLOSE c_get_seq_host_equipment_id;

               INSERT INTO device.host_equipment
                           (host_equipment_id, host_equipment_mfgr,
                            host_equipment_model
                           )
                    VALUES (n_host_equipment_id, v_host_equipment_mfgr,
                            v_host_equipment_model
                           );
            END IF;

            CLOSE c_get_host_equipment_id;

            -- create equipment association
            INSERT INTO device.host_type_host_equipment
                        (host_type_id, host_equipment_id
                        )
                 VALUES (n_host_type_id, n_host_equipment_id
                        );
         ELSE                                                      /* other */
            n_host_equipment_id := n_unknown_host_equipment_id;
         END IF;

         -- update host table with host_type_id and host_equipment_id (use special case for esuds)
         IF v_host_type_cd IN ('W', 'D', 'S', 'U', 'G', 'H')
         THEN
            UPDATE HOST h
               SET host_type_id = n_host_type_id,
                   host_equipment_id =
                      (SELECT DISTINCT host_equipment_id
                                  FROM host_type ht, host_equipment he
                                 WHERE ht.host_type_id = h.host_type_id
                                   AND he.host_equipment_mfgr = 'Maytag'
                                   AND DECODE (he.host_equipment_model,
                                               'Unknown', '0',
                                               he.host_equipment_model
                                              ) = ht.host_model_cd)
             WHERE host_type_id IN (SELECT host_type_id
                                      FROM host_type
                                     WHERE host_type_cd = v_host_type_cd);
         ELSE
            UPDATE HOST h
               SET host_type_id = n_host_type_id,
                   host_equipment_id = n_host_equipment_id
             WHERE host_type_id IN (SELECT host_type_id
                                      FROM host_type
                                     WHERE host_type_cd = v_host_type_cd);
         END IF;

         -- delete from host_type_host_group_type table given host_type_id
         DELETE FROM host_type_host_group_type
               WHERE host_type_id IN (SELECT host_type_id
                                        FROM host_type
                                       WHERE host_type_cd = v_host_type_cd);

/*          EXISTS (
                   SELECT COUNT (1)
                     FROM host_type ht
                    WHERE host_type_cd = v_host_type_cd
                      AND ht.host_type_id = h.host_type_id
                      AND ht.host_type_id != n_host_type_id);*/

         -- create device_type_host_type for this host_type
         INSERT INTO device_type_host_type
                     (device_type_id,
                      host_type_id, device_type_host_type_cd
                     )
              VALUES (CASE
                         WHEN v_host_type_cd IN
                                ('A',
                                 'D',
                                 'G',
                                 'H',
                                 'I',
                                 'J',
                                 'R',
                                 'S',
                                 'U',
                                 'W'
                                )
                            THEN 5
                         WHEN v_host_type_cd IN ('K')
                            THEN 11
                         WHEN v_host_type_cd IN ('T')
                            THEN 12
                         WHEN v_host_type_cd IN
                                             ('V') /* Legacy G4, G5, MEI handled later */
                            THEN 0
                      END,
                      n_host_type_id, v_host_type_cd
                     );

         -- (special case) create additional device_type_host_type records for legacy G4, G5, & MEI
         IF v_host_type_cd IN ('V')
         THEN
            INSERT INTO device_type_host_type
                        (device_type_id, host_type_id,
                         device_type_host_type_cd
                        )
                 VALUES (1, n_host_type_id,
                         v_host_type_cd
                        );

            INSERT INTO device_type_host_type
                        (device_type_id, host_type_id,
                         device_type_host_type_cd
                        )
                 VALUES (6, n_host_type_id,
                         v_host_type_cd
                        );

            INSERT INTO device_type_host_type
                        (device_type_id, host_type_id,
                         device_type_host_type_cd
                        )
                 VALUES (9, n_host_type_id,
                         v_host_type_cd
                        );
         END IF;
      END LOOP;

      CLOSE c_get_host_type_ids;
   END;
END;
/

-- clean up host_type table: purge unused (now considered duplicate) host_types
DELETE FROM host_type
      WHERE host_type_id NOT IN (
               SELECT a.host_type_id
                 FROM (SELECT DISTINCT MAX (host_type_id) host_type_id,
                                       host_type_cd
                                  FROM host_type hta
                              GROUP BY host_type_cd
                              ORDER BY host_type_cd) a);


-- drop columns HOST_TYPE_MANUFACTURER, HOST_MODEL_CD, HOST_TYPE_CD from HOST_TYPE
ALTER TABLE device.host_type DROP COLUMN host_type_manufacturer;
ALTER TABLE device.host_type DROP COLUMN host_model_cd;
ALTER TABLE device.host_type DROP COLUMN host_type_cd;


--- modify device_type table to include default base host id
ALTER TABLE device.device_type ADD (
    def_base_host_type_id        NUMBER(20,0),
    def_base_host_equipment_id   NUMBER(20,0),
    DEF_PRIM_HOST_TYPE_ID        NUMBER(20,0),
    DEF_PRIM_HOST_EQUIPMENT_ID   NUMBER(20,0),
    CONSTRAINT fk_device_type_host_type_id FOREIGN KEY(def_base_host_type_id) REFERENCES host_type(host_type_id),
    CONSTRAINT fk_device_type_host_equip_id FOREIGN KEY(def_base_host_equipment_id) REFERENCES host_equipment(host_equipment_id),
    CONSTRAINT FK_DEVICE_TYPE_HOST_TYPE_ID2 foreign key(DEF_PRIM_HOST_TYPE_ID) references HOST_TYPE(HOST_TYPE_ID),
    CONSTRAINT FK_DEVICE_TYPE_HOST_EQUIP_ID2 foreign key(DEF_PRIM_HOST_EQUIPMENT_ID) references HOST_EQUIPMENT(HOST_EQUIPMENT_ID)
);

-- create base hosts
CREATE TABLE device_host_types_tmp
  (
    device_type_id    NUMBER(20,0),
    device_type_desc  VARCHAR2(60)     NOT NULL,
    host_type_desc    VARCHAR2(60)     NOT NULL,
    host_equipment_mfgr VARCHAR2(60)   NOT NULL,
    host_equipment_model VARCHAR2(60)  NOT NULL,
    prim_host_type_cd    VARCHAR2(60),
    prim_host_equipment_mfgr VARCHAR2(60),
    prim_host_equipment_model VARCHAR2(60),
    CONSTRAINT pk_device_host_types_tmp PRIMARY KEY(device_type_id),
    CONSTRAINT fk_device_host_types_tmp_dt_id FOREIGN KEY(device_type_id) REFERENCES device.device_type(device_type_id)
  );
INSERT INTO device_host_types_tmp
   VALUES (0, 'G4 ePort', 'ePort G4', 'USA Technologies, Inc.',
           'Unknown', 'V', 'Unknown', 'Unknown');
INSERT INTO device_host_types_tmp
   VALUES (1, 'G5 ePort', 'ePort G5', 'USA Technologies, Inc.',
           'Unknown', 'V', 'Unknown', 'Unknown');
INSERT INTO device_host_types_tmp
     VALUES (3, 'ePort NG', 'ePort NG Brick', 'USA Technologies, Inc.',
             'Unknown', NULL, NULL, NULL);
INSERT INTO device_host_types_tmp
     VALUES (4, 'Legacy Kiosk (Sony DLL)', 'PictureStation Software', 'Sony',
             'Unknown', NULL, NULL, NULL);
INSERT INTO device_host_types_tmp
     VALUES (5, 'eSuds Room Controller', 'ePort NG Brick',
             'USA Technologies, Inc.', 'Unknown', NULL, NULL, NULL);
INSERT INTO device_host_types_tmp
   VALUES (6, 'MEI ePort', 'RDP', 'MEI', 'Unknown',
           'V', 'Unknown', 'Unknown');
INSERT INTO device_host_types_tmp
     VALUES (7, 'EZ80 ePort Development', 'eZ-APN Board',
             'USA Technologies, Inc.', 'Unknown', NULL, NULL, NULL);
INSERT INTO device_host_types_tmp
     VALUES (8, 'EZ80 ePort', 'eZ-APN Board', 'USA Technologies, Inc.',
             'Unknown', NULL, NULL, NULL);
INSERT INTO device_host_types_tmp
   VALUES (9, 'Legacy G4 ePort', 'ePort G4',
           'USA Technologies, Inc.', 'V', 'Unknown', 'Unknown',
           'Unknown');
INSERT INTO device_host_types_tmp
     VALUES (10, 'Transact', 'USALive Transact', 'USA Technologies, Inc.',
             'Unknown', NULL, NULL, NULL);
INSERT INTO device_host_types_tmp
     VALUES (11, 'Kiosk', 'Kiosk Software', 'USA Technologies, Inc.',
             'Unknown', NULL, NULL, NULL);
INSERT INTO device_host_types_tmp
     VALUES (12, 'T2', 'eZ-APN Board', 'USA Technologies, Inc.', 'Unknown',
             NULL, NULL, NULL);

BEGIN
   DECLARE
      n_host_group_type_id          host_group_type.host_group_type_id%TYPE;
      n_host_type_id                host_type.host_type_id%TYPE;
      n_device_type_id              device_type.device_type_id%TYPE;
      v_device_type_desc            device_type.device_type_desc%TYPE;
      v_host_type_desc              host_type.host_type_desc%TYPE;
      v_prim_host_type_cd           device_type_host_type.device_type_host_type_cd%TYPE;
      v_host_equipment_mfgr         host_equipment.host_equipment_mfgr%TYPE;
      v_host_equipment_model        host_equipment.host_equipment_model%TYPE;
      v_prim_host_equipment_mfgr    host_equipment.host_equipment_mfgr%TYPE;
      v_prim_host_equipment_model   host_equipment.host_equipment_model%TYPE;
      n_host_equipment_id           host_equipment.host_equipment_id%TYPE;

      TYPE cv_type IS REF CURSOR;

      c_get_host_equipment_id       cv_type;
      c_get_host_type_id            cv_type;

      CURSOR c_get_device_host_types_tmp
      IS
         SELECT device_type_id, device_type_desc, host_type_desc,
                host_equipment_mfgr, host_equipment_model,
                prim_host_type_cd, prim_host_equipment_mfgr,
                prim_host_equipment_model
           FROM device_host_types_tmp;

      CURSOR c_get_seq_host_equipment_id
      IS
         SELECT seq_host_equipment_id.NEXTVAL
           FROM DUAL;
   BEGIN
      -- create base host in host_group_type
      SELECT seq_host_group_type_id.NEXTVAL
        INTO n_host_group_type_id
        FROM DUAL;

      INSERT INTO host_group_type
                  (host_group_type_id, host_group_type_cd,
                   host_group_type_name,
                   host_group_type_desc
                  )
           VALUES (n_host_group_type_id, 'BASE_HOST',
                   'Base Host',
                   'Base hardware hosting associated device software'
                  );

      -- create base host_type records for each device type
      OPEN c_get_device_host_types_tmp;

      LOOP
         FETCH c_get_device_host_types_tmp
          INTO n_device_type_id, v_device_type_desc, v_host_type_desc,
               v_host_equipment_mfgr, v_host_equipment_model,
               v_prim_host_type_cd, v_prim_host_equipment_mfgr,
               v_prim_host_equipment_model;

         EXIT WHEN c_get_device_host_types_tmp%NOTFOUND;

         -- create new host type, if necessary
         OPEN c_get_host_type_id
          FOR 'SELECT host_type_id
             	FROM host_type
         	WHERE host_type_desc = :v_host_type_desc' USING v_host_type_desc;

         FETCH c_get_host_type_id
          INTO n_host_type_id;

         IF c_get_host_type_id%NOTFOUND
         THEN
            SELECT seq_host_type_id.NEXTVAL
              INTO n_host_type_id
              FROM DUAL;

            INSERT INTO host_type
                        (host_type_id, host_type_desc,
                         host_default_complete_minut
                        )
                 VALUES (n_host_type_id, v_host_type_desc,
                         0
                        );

            -- create host_type_host_group_type association
            INSERT INTO host_type_host_group_type
                        (host_type_id, host_group_type_id
                        )
                 VALUES (n_host_type_id, n_host_group_type_id
                        );
         END IF;

         -- set device_type default base host
         INSERT INTO device_type_host_type
                     (device_type_id, host_type_id, device_type_host_type_cd
                     )
              VALUES (n_device_type_id, n_host_type_id, 'B'
                     );

         -- create host equipment, unless it already exists
         OPEN c_get_host_equipment_id
          FOR 'SELECT host_equipment_id
             	FROM host_equipment
         	WHERE host_equipment_mfgr = :v_host_equipment_mfgr
         	AND host_equipment_model = :v_host_equipment_model'
          USING v_host_equipment_mfgr, v_host_equipment_model;

         FETCH c_get_host_equipment_id
          INTO n_host_equipment_id;

         IF c_get_host_equipment_id%NOTFOUND
         THEN
            OPEN c_get_seq_host_equipment_id;

            FETCH c_get_seq_host_equipment_id
             INTO n_host_equipment_id;

            CLOSE c_get_seq_host_equipment_id;

            INSERT INTO device.host_equipment
                        (host_equipment_id, host_equipment_mfgr,
                         host_equipment_model
                        )
                 VALUES (n_host_equipment_id, v_host_equipment_mfgr,
                         v_host_equipment_model
                        );
         END IF;

         CLOSE c_get_host_equipment_id;

         -- update device with base host default and new descriptions
         UPDATE device_type
            SET def_base_host_type_id = n_host_type_id,
                def_base_host_equipment_id = n_host_equipment_id,
                device_type_desc = v_device_type_desc
          WHERE device_type_id = n_device_type_id;

         IF v_prim_host_type_cd IS NOT NULL
         THEN
            UPDATE device_type
               SET def_prim_host_type_id =
                      (SELECT host_type_id
                         FROM device_type_host_type
                        WHERE device_type_host_type_cd = v_prim_host_type_cd
                          AND device_type_id = n_device_type_id),
                   def_prim_host_equipment_id =
                      (SELECT host_equipment_id
                         FROM host_equipment
                        WHERE host_equipment_mfgr = v_prim_host_equipment_mfgr
                          AND host_equipment_model =
                                                   v_prim_host_equipment_model)
             WHERE device_type_id = n_device_type_id;
         END IF;
      END LOOP;

      CLOSE c_get_device_host_types_tmp;
   END;
END;
/

DROP TABLE device_host_types_tmp;


-- set all null HOST table host_equipment_ids to unknown/unknown
UPDATE HOST
   SET host_equipment_id =
          (SELECT host_equipment_id
             FROM host_equipment
            WHERE host_equipment_mfgr = 'Unknown'
              AND host_equipment_model = 'Unknown')
 WHERE host_equipment_id IS NULL;


-- recreate esuds-specific host_type_host_group_type entries
INSERT INTO device.host_type_host_group_type
            (host_type_id, host_group_type_id)
   (SELECT host_type_id,
           DECODE (host_type_desc,
                   'Dryer', 2,
                   'Washer', 1,
                   'Stacked Dryer', 3,
                   'Stacked Washer/Dryer', 4,
                   NULL
                  ) host_group_type_id
      FROM device.host_type
     WHERE host_type_desc IN
                 ('Dryer', 'Washer', 'Stacked Dryer', 'Stacked Washer/Dryer'));

-- update descriptions of some host types
UPDATE host_type
   SET host_type_desc = 'Vending Machine'
 WHERE host_type_id IN (SELECT host_type_id
                          FROM device_type_host_type
                         WHERE device_type_host_type_cd IN ('V'));

UPDATE host_type
   SET host_type_desc = 'Gen1 Washer'
 WHERE host_type_id IN (SELECT host_type_id
                          FROM device_type_host_type
                         WHERE device_type_host_type_cd IN ('A'));

UPDATE host_type
   SET host_type_desc = 'Gen2 Washer'
 WHERE host_type_id IN (SELECT host_type_id
                          FROM device_type_host_type
                         WHERE device_type_host_type_cd IN ('W'));

UPDATE host_type
   SET host_type_desc = 'Gen1 Dryer'
 WHERE host_type_id IN (SELECT host_type_id
                          FROM device_type_host_type
                         WHERE device_type_host_type_cd IN ('R'));

UPDATE host_type
   SET host_type_desc = 'Gen2 Dryer'
 WHERE host_type_id IN (SELECT host_type_id
                          FROM device_type_host_type
                         WHERE device_type_host_type_cd IN ('D'));

UPDATE host_type
   SET host_type_desc = 'Gen1 Stack Dryer/Dryer (Top)'
 WHERE host_type_id IN (
                        SELECT host_type_id
                          FROM device_type_host_type
                         WHERE device_type_host_type_cd IN
                                                         ('G', 'I'));
UPDATE host_type
   SET host_type_desc = 'Gen1 Stack Dryer/Dryer (Bottom)'
 WHERE host_type_id IN (
                        SELECT host_type_id
                          FROM device_type_host_type
                         WHERE device_type_host_type_cd IN
                                                         ('H', 'J'));
                                                         
UPDATE host_type
   SET host_type_desc = 'Gen2 Stack Dryer/Dryer'
 WHERE host_type_id IN (SELECT host_type_id
                          FROM device_type_host_type
                         WHERE device_type_host_type_cd IN ('S'));

UPDATE host_type
   SET host_type_desc = 'Gen2 Stack Dryer/Washer'
 WHERE host_type_id IN (SELECT host_type_id
                          FROM device_type_host_type
                         WHERE device_type_host_type_cd IN ('U'));


-- special case for Kiosk and T2 -- migrate host records from old CDs (K, T) to new base hosts
-- kiosk first
UPDATE HOST h
   SET host_type_id =
               (SELECT host_type_id
                  FROM device_type_host_type
                 WHERE device_type_host_type_cd = 'B' AND device_type_id = 11)
 WHERE host_type_id =
               (SELECT host_type_id
                  FROM device_type_host_type
                 WHERE device_type_host_type_cd = 'K' AND device_type_id = 11);

DELETE FROM host_type_host_equipment
      WHERE host_type_id =
               (SELECT host_type_id
                  FROM device_type_host_type
                 WHERE device_type_host_type_cd = 'K' AND device_type_id = 11);

DELETE FROM device_type_host_type
      WHERE device_type_host_type_cd = 'K' AND device_type_id = 11;

DELETE FROM host_type
      WHERE host_type_desc = 'Kiosk';

-- now T2
UPDATE HOST h
   SET host_type_id =
               (SELECT host_type_id
                  FROM device_type_host_type
                 WHERE device_type_host_type_cd = 'B' AND device_type_id = 12)
 WHERE host_type_id =
               (SELECT host_type_id
                  FROM device_type_host_type
                 WHERE device_type_host_type_cd = 'T' AND device_type_id = 12);

DELETE FROM host_type_host_equipment
      WHERE host_type_id =
               (SELECT host_type_id
                  FROM device_type_host_type
                 WHERE device_type_host_type_cd = 'T' AND device_type_id = 12);

DELETE FROM device_type_host_type
      WHERE device_type_host_type_cd = 'T' AND device_type_id = 12;

DELETE FROM host_type
      WHERE host_type_desc = 'T2';


-- set final constraints on HOST table changes
ALTER TABLE device.HOST MODIFY host_equipment_id NOT NULL;


-- set final constraints on DEVICE_TYPE table changes
ALTER TABLE device.device_type MODIFY def_base_host_type_id NOT NULL;
ALTER TABLE device.device_type MODIFY def_base_host_equipment_id NOT NULL;
