CREATE OR REPLACE PACKAGE device.pkg_call_in_record
IS
   PROCEDURE sp_start (
        pn_session_id     IN   device_call_in_record.session_id%TYPE,
        ps_ev_number      IN   device.device_name%TYPE,
        ps_serial_number  IN   device.device_serial_cd%TYPE,
        pn_device_id      IN   device.device_id%TYPE
   );

   PROCEDURE sp_add_trans (
        pn_session_id     IN   device_call_in_record.session_id%TYPE,
        pc_card_type      IN   device_call_in_record.auth_card_type%TYPE,
        pn_trans_amount   IN   device_call_in_record.credit_trans_total%TYPE,
        pn_vend_count     IN   device_call_in_record.credit_vend_count%TYPE
   );

   PROCEDURE sp_add_auth (
        pn_session_id     IN   device_call_in_record.session_id%TYPE,
        pc_card_type      IN   device_call_in_record.auth_card_type%TYPE,
        pn_auth_amount    IN   device_call_in_record.auth_amount%TYPE,
        pc_approved_flag  IN   device_call_in_record.auth_approved_flag%TYPE
   );

   PROCEDURE sp_set_dex_received (
        pn_session_id     IN   device_call_in_record.session_id%TYPE,
        pn_dex_file_size  IN   device_call_in_record.dex_file_size%TYPE
   );

   PROCEDURE sp_set_initialized (
        pn_session_id     IN   device_call_in_record.session_id%TYPE
   );

   PROCEDURE sp_set_device_sent_config (
        pn_session_id     IN   device_call_in_record.session_id%TYPE
   );

   PROCEDURE sp_set_server_sent_config (
        pn_session_id     IN   device_call_in_record.session_id%TYPE
   );

   PROCEDURE sp_set_status (
        pn_session_id     IN   device_call_in_record.session_id%TYPE,
        pc_status_flag    IN   device_call_in_record.call_in_status%TYPE
   );

   PROCEDURE sp_set_type (
        pn_session_id     IN   device_call_in_record.session_id%TYPE,
        pc_type_flag      IN   device_call_in_record.call_in_type%TYPE
   );

   PROCEDURE sp_finish (
        pn_session_id              IN   device_call_in_record.session_id%TYPE,
        ps_network_layer           IN   device_call_in_record.network_layer%TYPE,
        ps_ip_address              IN   device_call_in_record.ip_address%TYPE,
        pc_call_in_status          IN   device_call_in_record.call_in_status%TYPE,        
        pn_inbound_message_count   IN	device_call_in_record.inbound_message_count%TYPE,
        pn_outbound_message_count  IN	device_call_in_record.outbound_message_count%TYPE,
        pn_inbound_byte_count      IN	device_call_in_record.inbound_byte_count%TYPE,
        pn_outbound_byte_count     IN	device_call_in_record.outbound_byte_count%TYPE
   );

   PROCEDURE sp_add_message (
        pn_session_id     IN   device_call_in_record.session_id%TYPE,
        pc_direction      IN   CHAR,
        pn_num_bytes      IN   device_call_in_record.inbound_byte_count%TYPE
   );
   
   FUNCTION fn_get_type (
        pn_session_id     IN   device_call_in_record.session_id%TYPE,
        pc_type_flag      IN   device_call_in_record.call_in_type%TYPE
   ) RETURN device_call_in_record.call_in_type%TYPE;
END;
/


CREATE OR REPLACE PACKAGE BODY device.pkg_call_in_record
IS
   -- Status constants
   CC_STATUS_INCOMPLETE   CONSTANT CHAR (1)  := 'I';
   CC_STATUS_COMPLETE     CONSTANT CHAR (1)  := 'S';
   CC_STATUS_FAILED       CONSTANT CHAR (1)  := 'U';
   CC_STATUS_UNKNOWN      CONSTANT CHAR (1)  := 'X';
   -- Call type contants
   CC_CALL_TYPE_UNKNOWN   CONSTANT CHAR (1)  := 'U';
   CC_CALL_TYPE_AUTH      CONSTANT CHAR (1)  := 'A';
   CC_CALL_TYPE_BATCH     CONSTANT CHAR (1)  := 'B';
   CC_CALL_TYPE_COMBO     CONSTANT CHAR (1)  := 'C';
   -- Approved constants
   CC_AUTH_APPROVED       CONSTANT CHAR (1)  := 'Y';
   CC_AUTH_DENIED         CONSTANT CHAR (1)  := 'N';
   CC_AUTH_FAILED         CONSTANT CHAR (1)  := 'F';
   -- Transaction type constants
   CC_CREDIT_TRANS        CONSTANT CHAR (1)  := 'C';
   CC_RFID_CREDIT_TRANS   CONSTANT CHAR (1)  := 'R';
   CC_PASSCARD_TRANS      CONSTANT CHAR (1)  := 'S';
   CC_RFID_PASSCARD_TRANS CONSTANT CHAR (1)  := 'P';
   CC_CASH_TRANS          CONSTANT CHAR (1)  := 'M';
   -- Message direction constants
   CC_INBOUND             CONSTANT CHAR (1)  := 'I';
   CC_OUTBOUND            CONSTANT CHAR (1)  := 'O';

   PROCEDURE sp_start (
        pn_session_id     IN   device_call_in_record.session_id%TYPE,
        ps_ev_number      IN   device.device_name%TYPE,
        ps_serial_number  IN   device.device_serial_cd%TYPE,
        pn_device_id      IN   device.device_id%TYPE
   )
   AS
        vs_location_name          location.location_name%TYPE;
        vs_customer_name          customer.customer_name%TYPE;
        vn_count                  NUMBER;
   BEGIN
        SELECT COUNT(1)
        INTO vn_count
        FROM device.device_call_in_record
        WHERE session_id = pn_session_id;
        
        IF vn_count > 0 THEN
            RETURN;
        END IF;
   
        -- If any extra in-progress records exists, mark them unsuccessful
        UPDATE device.device_call_in_record
        SET call_in_status = CC_STATUS_FAILED
        WHERE serial_number = ps_serial_number
            AND call_in_status = CC_STATUS_INCOMPLETE;
         
        SELECT l.location_name, c.customer_name
        INTO vs_location_name, vs_customer_name
        FROM pss.pos p, location.location l, location.customer c
        WHERE p.device_id = pn_device_id
            AND l.location_id = p.location_id
            AND c.customer_id = p.customer_id;
       
        INSERT INTO device.device_call_in_record(
            serial_number, call_in_start_ts, call_in_status,
            location_name, customer_name, session_id)
        VALUES (ps_serial_number, SYSDATE, CC_STATUS_INCOMPLETE,
            vs_location_name, vs_customer_name, pn_session_id);
   END;

   PROCEDURE sp_add_trans (
        pn_session_id     IN   device_call_in_record.session_id%TYPE,
        pc_card_type      IN   device_call_in_record.auth_card_type%TYPE,
        pn_trans_amount   IN   device_call_in_record.credit_trans_total%TYPE,
        pn_vend_count     IN   device_call_in_record.credit_vend_count%TYPE
   )
   AS
        vc_new_type_flag	device_call_in_record.call_in_type%TYPE;
   BEGIN
        vc_new_type_flag := fn_get_type(pn_session_id, CC_CALL_TYPE_BATCH);
   
      	IF pc_card_type IN (CC_CREDIT_TRANS, CC_RFID_CREDIT_TRANS) THEN
         	UPDATE device.device_call_in_record
            	SET credit_trans_count = credit_trans_count + 1,
                	credit_trans_total = credit_trans_total + pn_trans_amount,
                	credit_vend_count = credit_vend_count + pn_vend_count,
                	last_trans_in_ts = SYSDATE,
                	call_in_type = vc_new_type_flag
          	WHERE session_id = pn_session_id;
      	ELSIF pc_card_type = CC_CASH_TRANS THEN
         	UPDATE device.device_call_in_record
            	SET cash_trans_count = cash_trans_count + 1,
                	cash_trans_total = cash_trans_total + pn_trans_amount,
                	cash_vend_count = cash_vend_count + pn_vend_count,
                	last_trans_in_ts = SYSDATE,
                	call_in_type = vc_new_type_flag
          	WHERE session_id = pn_session_id;
      	ELSIF pc_card_type IN (CC_PASSCARD_TRANS, CC_RFID_PASSCARD_TRANS) THEN
         	UPDATE device.device_call_in_record
            	SET passcard_trans_count = passcard_trans_count + 1,
                	passcard_trans_total = passcard_trans_total + pn_trans_amount,
                	passcard_vend_count = passcard_vend_count + pn_vend_count,
                	last_trans_in_ts = SYSDATE,
                	call_in_type = vc_new_type_flag
          	WHERE session_id = pn_session_id;
      	END IF;
   END;

   PROCEDURE sp_add_auth (
        pn_session_id     IN   device_call_in_record.session_id%TYPE,
        pc_card_type      IN   device_call_in_record.auth_card_type%TYPE,
        pn_auth_amount    IN   device_call_in_record.auth_amount%TYPE,
        pc_approved_flag  IN   device_call_in_record.auth_approved_flag%TYPE
   )
   AS
        vc_new_type_flag	device_call_in_record.call_in_type%TYPE;
   BEGIN
        vc_new_type_flag := fn_get_type(pn_session_id, CC_CALL_TYPE_AUTH);
   	
      	UPDATE device.device_call_in_record
        SET auth_amount = pn_auth_amount,
             auth_approved_flag = pc_approved_flag,
             auth_card_type = pc_card_type,
             last_auth_in_ts = SYSDATE,
             call_in_status = CC_STATUS_COMPLETE,
             call_in_type = vc_new_type_flag
       	WHERE session_id = pn_session_id;
   END;

   PROCEDURE sp_set_dex_received (
        pn_session_id     IN   device_call_in_record.session_id%TYPE,
        pn_dex_file_size  IN   device_call_in_record.dex_file_size%TYPE
   )
   AS
        vc_new_type_flag	device_call_in_record.call_in_type%TYPE;
   BEGIN
        vc_new_type_flag := fn_get_type(pn_session_id, CC_CALL_TYPE_BATCH);
   	
      	UPDATE device.device_call_in_record
        SET dex_received_flag = 'Y',
             dex_file_size = pn_dex_file_size,
             call_in_type = vc_new_type_flag
       	WHERE session_id = pn_session_id;
   END;

   PROCEDURE sp_set_initialized (
        pn_session_id     IN   device_call_in_record.session_id%TYPE
   )
   AS
        vc_new_type_flag	device_call_in_record.call_in_type%TYPE;
   BEGIN
        vc_new_type_flag := fn_get_type(pn_session_id, CC_CALL_TYPE_BATCH);
   	
      	UPDATE device.device_call_in_record
      	SET device_initialized_flag = 'Y',
      		call_in_type = vc_new_type_flag
      	WHERE session_id = pn_session_id;
   END;

   PROCEDURE sp_set_device_sent_config (
        pn_session_id     IN   device_call_in_record.session_id%TYPE
   )
   AS
        vc_new_type_flag	device_call_in_record.call_in_type%TYPE;
   BEGIN
        vc_new_type_flag := fn_get_type(pn_session_id, CC_CALL_TYPE_BATCH);
   	
      	UPDATE device.device_call_in_record
        SET device_sent_config_flag = 'Y',
        	call_in_type = vc_new_type_flag
       	WHERE session_id = pn_session_id;
   END;

   PROCEDURE sp_set_server_sent_config (
        pn_session_id     IN   device_call_in_record.session_id%TYPE
   )
   AS
        vc_new_type_flag	device_call_in_record.call_in_type%TYPE;
   BEGIN
        vc_new_type_flag := fn_get_type(pn_session_id, CC_CALL_TYPE_BATCH);
   	
      	UPDATE device.device_call_in_record
        SET server_sent_config_flag = 'Y',
        	call_in_type = vc_new_type_flag
       	WHERE session_id = pn_session_id;
   END;

   PROCEDURE sp_set_status (
        pn_session_id     IN   device_call_in_record.session_id%TYPE,
        pc_status_flag    IN   device_call_in_record.call_in_status%TYPE
   )
   AS
   BEGIN
      -- if new_status is Success, overwrite all
      -- if new_status is Failed, overwrite all but success
      -- if new_status is Unknown, overwrite only incomplete
      -- if new_status is Incomplete, overwrite none
      IF pc_status_flag = CC_STATUS_COMPLETE THEN
      	  UPDATE device.device_call_in_record
          SET call_in_status = pc_status_flag
          WHERE session_id = pn_session_id;
      ELSIF pc_status_flag = CC_STATUS_FAILED THEN
          UPDATE device.device_call_in_record
          SET call_in_status =
                   DECODE (call_in_status,
                           CC_STATUS_COMPLETE, call_in_status,
                           pc_status_flag
                          )
          WHERE session_id = pn_session_id;
      ELSIF pc_status_flag = CC_STATUS_UNKNOWN THEN
         UPDATE device.device_call_in_record
         SET call_in_status =
                   DECODE (call_in_status,
                           CC_STATUS_INCOMPLETE, pc_status_flag,
                           call_in_status
                          )
         WHERE session_id = pn_session_id;
      END IF;
   END;

   PROCEDURE sp_set_type (
        pn_session_id     IN   device_call_in_record.session_id%TYPE,
        pc_type_flag      IN   device_call_in_record.call_in_type%TYPE
   )
   AS
      	vc_new_type_flag	device_call_in_record.call_in_type%TYPE;
   BEGIN
   	    vc_new_type_flag := fn_get_type(pn_session_id, pc_type_flag);
   
        UPDATE device.device_call_in_record
        SET call_in_type = vc_new_type_flag
        WHERE session_id = pn_session_id;
   END;

   PROCEDURE sp_finish (
        pn_session_id              IN   device_call_in_record.session_id%TYPE,
        ps_network_layer           IN   device_call_in_record.network_layer%TYPE,
        ps_ip_address              IN   device_call_in_record.ip_address%TYPE,
        pc_call_in_status          IN   device_call_in_record.call_in_status%TYPE,
        pn_inbound_message_count   IN	device_call_in_record.inbound_message_count%TYPE,
        pn_outbound_message_count  IN	device_call_in_record.outbound_message_count%TYPE,
        pn_inbound_byte_count      IN	device_call_in_record.inbound_byte_count%TYPE,
        pn_outbound_byte_count     IN	device_call_in_record.outbound_byte_count%TYPE
   )
   AS
   BEGIN
        -- if passed call_in_status is Failed, set it to Failed in DB
      	-- if DB call_in_status is Incomplete, update it to Failed
      	UPDATE device.device_call_in_record
        SET call_in_finish_ts = SYSDATE,
             call_in_status =        
             	DECODE (pc_call_in_status,
             		CC_STATUS_FAILED, CC_STATUS_FAILED,
                	DECODE (call_in_status,
                        	CC_STATUS_INCOMPLETE, CC_STATUS_FAILED,
                        	call_in_status
                       	)
                ),
             network_layer = ps_network_layer,
             ip_address = ps_ip_address,
             inbound_message_count = pn_inbound_message_count,
             outbound_message_count = pn_outbound_message_count,
             inbound_byte_count = pn_inbound_byte_count,
             outbound_byte_count = pn_outbound_byte_count
       	WHERE session_id = pn_session_id;
   END;

   PROCEDURE sp_add_message (
        pn_session_id     IN   device_call_in_record.session_id%TYPE,
        pc_direction      IN   CHAR,
        pn_num_bytes      IN   device_call_in_record.inbound_byte_count%TYPE
   )
   AS
   BEGIN
      	IF pc_direction = CC_INBOUND THEN
         	UPDATE device.device_call_in_record
            	SET inbound_message_count = inbound_message_count + 1,
                	inbound_byte_count = inbound_byte_count + pn_num_bytes
         	WHERE session_id = pn_session_id;
      	ELSIF pc_direction = CC_OUTBOUND THEN
         	UPDATE device.device_call_in_record
            	SET outbound_message_count = outbound_message_count + 1,
                	outbound_byte_count = outbound_byte_count + pn_num_bytes,
                	last_message_out_ts = SYSDATE
         	WHERE session_id = pn_session_id;
      	END IF;
   END;
   
   FUNCTION fn_get_type (
        pn_session_id     IN   device_call_in_record.session_id%TYPE,
        pc_type_flag      IN   device_call_in_record.call_in_type%TYPE
   ) RETURN device_call_in_record.call_in_type%TYPE
   AS
      	vc_current_type_flag	device_call_in_record.call_in_type%TYPE;
   BEGIN
      	SELECT call_in_type
      	INTO vc_current_type_flag
      	FROM device.device_call_in_record
      	WHERE session_id = pn_session_id;

      	IF vc_current_type_flag = CC_CALL_TYPE_AUTH AND pc_type_flag = CC_CALL_TYPE_BATCH THEN
         	RETURN CC_CALL_TYPE_COMBO;
      	ELSIF vc_current_type_flag = CC_CALL_TYPE_BATCH AND pc_type_flag = CC_CALL_TYPE_AUTH THEN
         	RETURN CC_CALL_TYPE_COMBO;
      	ELSE
         	RETURN pc_type_flag;
      	END IF;
   END;
END;
/

GRANT EXECUTE ON device.pkg_call_in_record TO web_user;
GRANT EXECUTE ON device.pkg_call_in_record TO USAT_WEB_ROLE;
