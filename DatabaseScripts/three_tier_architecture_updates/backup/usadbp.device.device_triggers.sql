-- Start of DDL Script for Trigger DEVICE.TRBI_DEVICE
-- Generated 8/9/2007 11:11:20 AM from DEVICE@USADBP

CREATE OR REPLACE TRIGGER trbi_device
 BEFORE
  INSERT
 ON device
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
Begin

    IF :new.DEVICE_id IS NULL THEN

      SELECT seq_DEVICE_id.nextval
        into :new.DEVICE_id
        FROM dual;

    END IF;

 SELECT    sysdate,
           user,
           sysdate,
           user,
           nvl(:new.device_active_yn_flag, 'Y')
      into :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by,
           :new.device_active_yn_flag
      FROM dual;
End;
/


-- End of DDL Script for Trigger DEVICE.TRBI_DEVICE


-- Start of DDL Script for Trigger DEVICE.TRBU_DEVICE
-- Generated 8/9/2007 11:11:52 AM from DEVICE@USADBP

CREATE OR REPLACE TRIGGER trbu_device
 BEFORE
  UPDATE
 ON device
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
Begin
SELECT
           :old.created_by,
           :old.created_ts,
           sysdate,
           user
      into
           :new.created_by,
           :new.created_ts,

           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;
End;
/


-- End of DDL Script for Trigger DEVICE.TRBU_DEVICE
