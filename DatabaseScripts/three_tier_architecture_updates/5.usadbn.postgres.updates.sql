CREATE CAST (VARCHAR AS BYTEA) WITHOUT FUNCTION;
CREATE CAST (TEXT AS BYTEA) WITHOUT FUNCTION;

CREATE OR REPLACE FUNCTION engine.sp_start_net_layer(pn_stored_net_layer_id integer, ps_net_layer_desc character varying, OUT pn_new_net_layer_id integer) RETURNS integer
    AS $$
DECLARE
       vn_net_layer_active_yn_flag engine.net_layer.net_layer_active_yn_flag%TYPE;
BEGIN
	IF pn_stored_net_layer_id IS NULL OR pn_stored_net_layer_id <= 0 THEN
	 	pn_new_net_layer_id := NEXTVAL('ENGINE.SEQ_NET_LAYER_ID');
	 	INSERT INTO engine.net_layer(net_layer_id, net_layer_desc, net_layer_active_yn_flag) VALUES (pn_new_net_layer_id,ps_net_layer_desc, 'Y');
	ELSE
		SELECT net_layer_active_yn_flag
		INTO vn_net_layer_active_yn_flag
		FROM engine.net_layer
		WHERE net_layer_id = pn_stored_net_layer_id;

		IF vn_net_layer_active_yn_flag = 'Y' THEN
			UPDATE engine.net_layer
			SET net_layer_active_yn_flag = 'N'
			WHERE net_layer_id = pn_stored_net_layer_id;
		END IF;

		UPDATE engine.net_layer
		SET net_layer_active_yn_flag = 'Y',
			net_layer_desc = ps_net_layer_desc
		WHERE net_layer_id = pn_stored_net_layer_id;

		pn_new_net_layer_id := pn_stored_net_layer_id;
	END IF;
	
	-- pass net layer start internal message to app layer
	INSERT INTO engine.machine_cmd_inbound(machine_id, inbound_command, inbound_msg_no, session_id, net_layer_id, logic_engine_id) 
	VALUES('NETLAYER', '0002' || ENCODE(CAST(ps_net_layer_desc AS BYTEA), 'hex'), 0, NEXTVAL('engine.seq_net_layer_device_session_id'), pn_new_net_layer_id, engine.sf_select_best_logic_engine(NULL));
END;
$$
    LANGUAGE plpgsql;
    
    
CREATE OR REPLACE FUNCTION engine.sp_stop_net_layer(pn_stored_net_layer_id integer) RETURNS void
    AS $$
BEGIN
	UPDATE engine.net_layer
	SET net_layer_active_yn_flag = 'N'
	WHERE net_layer_id = pn_stored_net_layer_id;
	
	-- pass net layer stop internal message to app layer
	INSERT INTO engine.machine_cmd_inbound(machine_id, inbound_command, inbound_msg_no, session_id, net_layer_id, logic_engine_id) 
	VALUES('NETLAYER', '0003', 0, NEXTVAL('engine.seq_net_layer_device_session_id'), pn_stored_net_layer_id, engine.sf_select_best_logic_engine(NULL));
END;
$$
    LANGUAGE plpgsql;
    
    
CREATE OR REPLACE FUNCTION engine.trbu_machine_cmd_outbound() RETURNS trigger
    AS $$
BEGIN
	NEW.execute_date := NOW();
	
	/* Move Errors to Hist table */
	IF SUBSTR(NEW.execute_cd, 1, 1) = 'E' AND SUBSTR(NEW.command, 3, 2) <> '01' THEN
		INSERT INTO engine.machine_cmd_outbound_hist(
			command_id,
			modem_id,
			command,
			command_date,
			execute_date,
			execute_cd,
			session_id,
			net_layer_id
		) VALUES (
			NEW.command_id,
			NEW.modem_id,
			NEW.command,
			NEW.command_date,
			NEW.execute_date,
			NEW.execute_cd,
			NEW.session_id,
			NEW.net_layer_id
		);
		/* Change execute_cd to X */
		NEW.execute_cd := 'X';
	END IF;

   	RETURN NEW;
END;
$$
    LANGUAGE plpgsql;    


CREATE OR REPLACE FUNCTION engine.trau_machine_cmd_outbound() RETURNS trigger
    AS $$
BEGIN
	IF NEW.execute_cd = 'Y' AND SUBSTR(NEW.command, 3, 2) <> '01' THEN
		INSERT INTO engine.machine_cmd_outbound_hist (
			command_id,
			modem_id,
			command,
			command_date,
			execute_date,
			execute_cd,
			session_id,
			net_layer_id
		) VALUES (
			NEW.command_id,
			NEW.Modem_id,
			NEW.command,
			NEW.command_date,
			NEW.execute_date,
			NEW.execute_cd,
			NEW.session_id,
			NEW.net_layer_id
		);
	END IF;

   	RETURN NULL;
END;
$$
    LANGUAGE plpgsql;
        
    
CREATE OR REPLACE FUNCTION engine.sp_start_device_session(pn_net_layer_id integer, ps_device_name character varying, ps_client_ip_addr character varying, ps_client_port integer, OUT pn_session_id bigint) RETURNS bigint
    AS $$
DECLARE
	--v_device_type_id    device.device_type_id%TYPE;
BEGIN
      	pn_session_id := NEXTVAL('engine.seq_net_layer_device_session_id');
        
        IF ps_device_name <> 'NETLAYER' THEN
		INSERT INTO engine.net_layer_device_session
		(
			 session_id,
			 net_layer_id,
			 device_name,
			 session_client_ip_addr,
			 session_client_port
		)
		VALUES
		(
			 pn_session_id,
			 pn_net_layer_id,
			 ps_device_name,
			 ps_client_ip_addr,
			 ps_client_port
		);
	END IF;		
/*      
	-- Flush the outbound queue here to clear out any old messages

	-- EV000 indicates the default EV which will not be in the database
	-- We want to flush the outbound queue in ALL cases for the default EV
	IF ps_device_name LIKE 'EV000%' THEN
		v_device_type_id := -1;
	ELSE
	    	SELECT device_type_id
	      	INTO v_device_type_id
	      	FROM device.device
	     	WHERE device_name = ps_device_name
	       		AND device_active_yn_flag = 'Y';
	       		
	       	v_device_type_id := COALESCE(v_device_type_id, -1);
	END IF;

	-- dont't flush these types...
	IF v_device_type_id IN (0, 1, 4, 5) THEN
		RETURN;
	END IF;

	UPDATE ENGINE.MACHINE_CMD_OUTBOUND
	SET EXECUTE_CD = 'E'
	WHERE MODEM_ID = ps_device_name;
*/
END;
$$
    LANGUAGE plpgsql;    
        

CREATE OR REPLACE FUNCTION engine.sp_stop_device_session (pn_session_id bigint, pn_inbound_msg_cnt integer, pn_outbound_msg_cnt integer, pn_inbound_byte_cnt integer, pn_outbound_byte_cnt integer, ps_device_name varchar, pn_msg_no integer, pn_net_layer_id integer, pn_logic_engine_id integer, ps_network_layer varchar, ps_client_ip_address varchar, pn_client_port integer, pc_call_in_status char) RETURNS void
    AS $$
BEGIN
    UPDATE engine.net_layer_device_session
    SET session_active_yn_flag = 'N',
        session_inbound_msg_cnt = pn_inbound_msg_cnt,
        session_outbound_msg_cnt = pn_outbound_msg_cnt,
        session_inbound_byte_cnt = pn_inbound_byte_cnt,
        session_outbound_byte_cnt = pn_outbound_byte_cnt
    WHERE session_id = pn_session_id;       
    
    -- pass device session end internal message to app layer
    INSERT INTO engine.machine_cmd_inbound(machine_id, inbound_command, inbound_msg_no, session_id, net_layer_id, logic_engine_id) 
    VALUES(ps_device_name, '0004' || ENCODE(CAST(ps_network_layer
                           || '|' || ps_client_ip_address || ':' || pn_client_port
                           || '|' || pc_call_in_status
                           || '|' || pn_inbound_msg_cnt
                           || '|' || pn_outbound_msg_cnt
                           || '|' || pn_inbound_byte_cnt
                           || '|' || pn_outbound_byte_cnt AS BYTEA), 'hex'),
                           pn_msg_no, pn_session_id, pn_net_layer_id, pn_logic_engine_id);
END;
$$
    LANGUAGE plpgsql;
    
    
CREATE OR REPLACE FUNCTION engine.trbi_machine_cmd_inbound_hist() RETURNS trigger
    AS $$
BEGIN
	-- Get rid of sensitive info to comply with PCI rules
	IF UPPER(SUBSTR(NEW.inbound_command, 1, 2)) IN ('5E', 'A0') THEN
		NEW.inbound_command :=  SUBSTR(NEW.inbound_command, 1, 12) || '=>' || LENGTH(NEW.inbound_command) - 12;
	ELSIF UPPER(SUBSTR(NEW.inbound_command, 1, 2)) IN ('2B', '9C', 'A3', '93') THEN
		NEW.inbound_command :=  SUBSTR(NEW.inbound_command, 1, 2) || '=>' || LENGTH(NEW.inbound_command) - 2;
	ELSIF UPPER(SUBSTR(NEW.inbound_command, 1, 4)) = '9A5F' THEN
		NEW.inbound_command :=  SUBSTR(NEW.inbound_command, 1, 4) || '=>' || LENGTH(NEW.inbound_command) - 4;
	END IF;

   	RETURN NEW;
END;
$$
    LANGUAGE plpgsql;

    
ALTER FUNCTION engine.trbi_machine_cmd_inbound_hist() OWNER TO net_user;
    
    
CREATE TRIGGER trbi_machine_cmd_inbound_hist
    BEFORE INSERT ON engine.machine_cmd_inbound_hist
    FOR EACH ROW
    EXECUTE PROCEDURE engine.trbi_machine_cmd_inbound_hist();
    
    
CREATE OR REPLACE FUNCTION engine.trbi_machine_cmd_outbound_hist() RETURNS trigger
    AS $$
BEGIN
	-- Get rid of sensitive info to comply with PCI rules
	IF UPPER(SUBSTR(NEW.command, 3, 2)) = '8F' THEN
		NEW.command :=  SUBSTR(NEW.command, 1, 4) || '=>' || LENGTH(NEW.command) - 4;
	END IF;

   	RETURN NEW;
END;
$$
    LANGUAGE plpgsql;

    
ALTER FUNCTION engine.trbi_machine_cmd_outbound_hist() OWNER TO net_user;
    
    
CREATE TRIGGER trbi_machine_cmd_outbound_hist
    BEFORE INSERT ON engine.machine_cmd_outbound_hist
    FOR EACH ROW
    EXECUTE PROCEDURE engine.trbi_machine_cmd_outbound_hist();
