TRUNCATE TABLE engine.machine_cmd_inbound_hist;
TRUNCATE TABLE engine.machine_cmd_outbound_hist;
TRUNCATE TABLE engine.net_layer_device_session_hist;

VACUUM ANALYZE engine.machine_cmd_inbound_hist;
VACUUM ANALYZE engine.machine_cmd_outbound_hist;
VACUUM ANALYZE engine.net_layer_device_session_hist;
