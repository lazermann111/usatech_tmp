ALTER TABLE device.device_call_in_record ADD session_id NUMBER(20, 0);

CREATE INDEX device.idx_call_in_record_session_id ON device.device_call_in_record(session_id) TABLESPACE device_indx;