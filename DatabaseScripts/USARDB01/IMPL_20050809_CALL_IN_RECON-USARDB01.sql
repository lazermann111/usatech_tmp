/* 
Description:


This is a migration of daily call in record to transaction reconciliation and device counters to call in record reconciliation tracking. It includes moving a package that contains stored procedures for data generation from USADBT01 to USARDB01. 

Created By:
J Bradley 

Date:
2005-08-09 

Grant Privileges (run as):
SYSTEM 

*/ 


-- database link to USADBP (add the correct password to the script prior to execution)
@PUBLIC.USADBP_TRACKING.WORLD_ddl.sql;

-- reconciliation package (contains stored procedures for data generation)
@REPORT.PKG_RECONCILIATION_ddl.sql;


