ALTER TABLE report.session_counter_except ADD
    (calls_after_prior_batch        NUMBER(20,0) DEFAULT 0,
    batch_session_count            NUMBER(20,0) DEFAULT 0)
/
