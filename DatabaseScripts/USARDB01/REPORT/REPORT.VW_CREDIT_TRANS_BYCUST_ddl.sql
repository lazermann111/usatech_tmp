-- Start View REPORT.VW_CREDIT_TRANS_BYCUST

CREATE OR REPLACE VIEW report.vw_credit_trans_bycust (
   business_unit,
   customer,
   currency,
   tranmonth,
   servermonth,
   credit_settled_paid,
   refund_settled_paid,
   credit_settled_unpaid,
   refund_settled_unpaid,
   credit_settled_total,
   refund_settled_total,
   credit_pending_paid,
   refund_pending_paid,
   credit_pending_unpaid,
   refund_pending_unpaid,
   credit_pending_total,
   refund_pending_total,
   credit_failed_paid,
   refund_failed_paid,
   credit_failed_unpaid,
   refund_failed_unpaid,
   credit_failed_total,
   refund_failed_total,
   credit_paid_total,
   refund_paid_total,
   credit_unpaid_total,
   refund_unpaid_total,
   credit_total,
   refund_total )
AS
SELECT
b.business_unit_name,
b.customer,
b.currency_name,
b.tran_month,
b.server_month,
SUM(CASE WHEN b.tran_state_id = 3 THEN b.credit_paid ELSE NULL END) credit_settled_paid,
SUM(CASE WHEN b.tran_state_id = 3 THEN b.refund_paid ELSE NULL END) refund_settled_paid,
SUM(CASE WHEN b.tran_state_id = 3 THEN b.credit_unpaid ELSE NULL END) credit_settled_unpaid,
SUM(CASE WHEN b.tran_state_id = 3 THEN b.refund_unpaid ELSE NULL END) refund_settled_unpaid,
SUM(CASE WHEN b.tran_state_id = 3 THEN b.credit ELSE NULL END) credit_settled_total,
SUM(CASE WHEN b.tran_state_id = 3 THEN b.refund ELSE NULL END) refund_settled_total,
SUM(CASE WHEN b.tran_state_id = 4 THEN b.credit_paid ELSE NULL END) credit_pending_paid,
SUM(CASE WHEN b.tran_state_id = 4 THEN b.refund_paid ELSE NULL END) refund_pending_paid,
SUM(CASE WHEN b.tran_state_id = 4 THEN b.credit_unpaid ELSE NULL END) credit_pending_unpaid,
SUM(CASE WHEN b.tran_state_id = 4 THEN b.refund_unpaid ELSE NULL END) refund_pending_unpaid,
SUM(CASE WHEN b.tran_state_id = 4 THEN b.credit ELSE NULL END) credit_pending_total,
SUM(CASE WHEN b.tran_state_id = 4 THEN b.refund ELSE NULL END) refund_pending_total,
SUM(CASE WHEN b.tran_state_id = 5 THEN b.credit_paid ELSE NULL END) credit_failed_paid,
SUM(CASE WHEN b.tran_state_id = 5 THEN b.refund_paid ELSE NULL END) refund_failed_paid,
SUM(CASE WHEN b.tran_state_id = 5 THEN b.credit_unpaid ELSE NULL END) credit_failed_unpaid,
SUM(CASE WHEN b.tran_state_id = 5 THEN b.refund_unpaid ELSE NULL END) refund_failed_unpaid,
SUM(CASE WHEN b.tran_state_id = 5 THEN b.credit ELSE NULL END) credit_failed_total,
SUM(CASE WHEN b.tran_state_id = 5 THEN b.refund ELSE NULL END) refund_failed_total,
SUM(b.credit_paid) credit_paid_total,
SUM(b.refund_paid) refund_paid_total,
SUM(b.credit_unpaid) credit_unpaid_total,
SUM(b.refund_unpaid) refund_unpaid_total,
SUM(b.credit) credit_total,
SUM(b.refund) refund_total
FROM (
    SELECT
    a.business_unit_name,
    a.customer,
    a.currency_name,
    a.tran_month,
    a.server_month,
    (CASE 
    	WHEN a.tran_state_id = 1 THEN 4
    	WHEN a.tran_state_id = 2 THEN 3
    	WHEN a.tran_state_id = 6 THEN 3
    	ELSE a.tran_state_id 
    END) tran_state_id,
    SUM(a.credit) credit,
    SUM(a.refund) refund,
    SUM(CASE WHEN a.tran_paid = 1 THEN a.credit ELSE NULL END) credit_paid,
    SUM(CASE WHEN a.tran_paid = 1 THEN a.refund ELSE NULL END) refund_paid,
    SUM(CASE WHEN a.tran_paid = 0 THEN a.credit ELSE NULL END) credit_unpaid,
    SUM(CASE WHEN a.tran_paid = 0 THEN a.refund ELSE NULL END) refund_unpaid
    FROM (
    	SELECT
    	tr.tran_id, 
		MAX(NVL(led.paid_date, MAX_DATE)) max_led_date,
    	bu.business_unit_name,
    	cu.currency_name,
    	REPLACE(cus.customer_name, ',', ' ') customer,
    	TO_CHAR(tr.close_date, 'Mon-yyyy') tran_month,
    	TO_CHAR (tr.server_date, 'Mon-yyyy') server_month,
    	ept.eport_serial_num serial,
    	(CASE WHEN tr.trans_type_id = 16 THEN tr.total_amount ELSE NULL END) credit,
    	(CASE WHEN tr.trans_type_id = 20 THEN tr.total_amount ELSE NULL END) refund,	
    	(CASE WHEN NVL(tr.settle_date, MAX_DATE) >= TRUNC(SYSDATE, 'MM') THEN 1 ELSE tr.settle_state_id END) tran_state_id,
    	(CASE WHEN NVL(led.paid_date, MAX_DATE) >= TRUNC(SYSDATE, 'MM') THEN 0 ELSE 1 END) tran_paid
    	FROM corp.ledger led, report.trans tr, report.eport ept, report.terminal ter, corp.customer cus, corp.business_unit bu, corp.currency cu
    	WHERE cus.customer_id(+) = ter.customer_id
    	AND ter.terminal_id(+) = tr.terminal_id
    	AND led.trans_id(+) = tr.tran_id
    	AND ept.eport_id = tr.eport_id
    	AND NVL(ter.business_unit_id, report.find_business_unit(tr.eport_id, NULL)) = bu.business_unit_id 
    	AND tr.server_date >= ADD_MONTHS(TRUNC(SYSDATE, 'MM'), -1)
    	AND tr.server_date < TRUNC(SYSDATE, 'MM')
    	AND tr.trans_type_id IN(16, 20)
    	AND tr.currency_id = cu.currency_id
    	GROUP BY tr.tran_id, bu.business_unit_name, cu.currency_name, 
		REPLACE(cus.customer_name, ',', ' '), TO_CHAR(tr.close_date, 'Mon-yyyy'),
		TO_CHAR (tr.server_date, 'Mon-yyyy'), ept.eport_serial_num,
    	(CASE WHEN tr.trans_type_id = 16 THEN tr.total_amount ELSE NULL END),
    	(CASE WHEN tr.trans_type_id = 20 THEN tr.total_amount ELSE NULL END),	
    	(CASE WHEN NVL(tr.settle_date, MAX_DATE) >= TRUNC(SYSDATE, 'MM') THEN 1 ELSE tr.settle_state_id END),
    	(CASE WHEN NVL(led.paid_date, MAX_DATE) >= TRUNC(SYSDATE, 'MM') THEN 0 ELSE 1 END)
    	
    ) a
    GROUP BY a.business_unit_name, a.customer, a.currency_name, a.tran_month, a.server_month,
    (CASE WHEN a.tran_state_id = 1 THEN 4 WHEN a.tran_state_id = 2 THEN 3 WHEN a.tran_state_id = 6 THEN 3 ELSE a.tran_state_id END)
) b
GROUP BY b.business_unit_name, b.customer, b.currency_name, b.tran_month, b.server_month
/


-- End of DDL Script for View REPORT.VW_CREDIT_TRANS_BYCUST

