-- Start of DDL Script for Table REPORT.SESSION_COUNTER_EXCEPT
-- Generated 14-Sep-2005 16:24:11 from REPORT@USADBD02

CREATE TABLE report.session_counter_except
    (session_counter_except_id      NUMBER(20,0) NOT NULL,
    device_call_in_record_id       NUMBER(20,0) NOT NULL,
    device_id                      NUMBER(20,0) NOT NULL,
    machine_id                     VARCHAR2(60) NOT NULL,
    device_serial_cd               VARCHAR2(20) NOT NULL,
    device_type                    VARCHAR2(60) NOT NULL,
    firmware_version               VARCHAR2(60) NOT NULL,
    server_date                    DATE NOT NULL,
    call_in_start_ts               DATE NOT NULL,
    call_in_finish_ts              DATE,
    session_count                  NUMBER(20,0) DEFAULT 0      NOT NULL,
    created_by                     VARCHAR2(30) NOT NULL,
    created_ts                     DATE NOT NULL,
    last_updated_by                VARCHAR2(30) NOT NULL,
    last_updated_ts                DATE NOT NULL)
  TABLESPACE  report_data
/




-- Indexes for REPORT.SESSION_COUNTER_EXCEPT

CREATE INDEX report.ix_session_counter_ex_1 ON report.session_counter_except
  (
    device_call_in_record_id        ASC
  )
  TABLESPACE  report_index
PARALLEL (DEGREE DEFAULT)
NOLOGGING
/

CREATE INDEX report.ix_session_counter_ex_2 ON report.session_counter_except
  (
    device_id                       ASC
  )
  TABLESPACE  report_index
PARALLEL (DEGREE DEFAULT)
NOLOGGING
/

CREATE INDEX report.ix_session_counter_ex_3 ON report.session_counter_except
  (
    machine_id                      ASC
  )
  TABLESPACE  report_index
PARALLEL (DEGREE DEFAULT)
NOLOGGING
/

CREATE INDEX report.ix_session_counter_ex_4 ON report.session_counter_except
  (
    device_serial_cd                ASC
  )
  TABLESPACE  report_index
PARALLEL (DEGREE DEFAULT)
NOLOGGING
/

CREATE INDEX report.ix_session_counter_ex_5 ON report.session_counter_except
  (
    server_date                     ASC
  )
  TABLESPACE  report_index
PARALLEL (DEGREE DEFAULT)
NOLOGGING
/



-- Constraints for REPORT.SESSION_COUNTER_EXCEPT

ALTER TABLE report.session_counter_except
ADD CONSTRAINT pk_session_counter_except_id PRIMARY KEY (
  session_counter_except_id)
USING INDEX
  TABLESPACE  report_data
/


-- Triggers for REPORT.SESSION_COUNTER_EXCEPT

CREATE OR REPLACE TRIGGER report.trbi_session_counter_ex
 BEFORE
  INSERT
 ON report.session_counter_except
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN

    IF :new.session_counter_except_id IS NULL THEN

      SELECT seq_session_counter_except_id.NEXTVAL
        INTO :new.session_counter_except_id
        FROM dual;

    END IF;

 SELECT    SYSDATE,
           USER,
           SYSDATE,
           USER
      INTO :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;
END;
/

CREATE OR REPLACE TRIGGER report.trbu_session_counter_ex
 BEFORE
  UPDATE
 ON report.session_counter_except
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN

    
  SELECT
           :old.created_by,
           :old.created_ts,
           SYSDATE,
           USER
      INTO
           :new.created_by,
           :new.created_ts,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;
END;
/


-- End of DDL Script for Table REPORT.SESSION_COUNTER_EXCEPT

