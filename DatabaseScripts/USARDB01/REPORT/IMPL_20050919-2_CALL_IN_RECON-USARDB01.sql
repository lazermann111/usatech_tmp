/* 
Description:


This is an update implementation for device counters to call in record reconciliation tracking. This will update the session counter tracking to add network layer and ip address information. It updates a table adding two new fields and updates a stored procedures (in a package) for exception reporting. 

Created By:
J Bradley 

Date:
2005-09-19 

Grant Privileges (run as):
REPORT 

*/ 



-- SESSION_COUNTER_EXCEPT - alter table adding two new fields
@REPORT.SESSION_COUNTER_EXCEPT_alter-2_ddl.sql;

-- reconciliation package - update stored proc to generate the data for the above table
@REPORT.PKG_RECONCILIATION_ddl.sql;


