/* 
Description:


This is an update implementation for device counters to call in record reconciliation tracking. This will implement session and byte counters for the reconciliaton. It includes a new stored procedures (added to a package) and a new table (and sequence) for exception reporting. 

Created By:
J Bradley 

Date:
2005-09-15 

Grant Privileges (run as):
REPORT 

*/ 



-- sequence for new table (see below)
@REPORT.SEQ_SESSION_COUNTER_EXCEPT_ID_ddl.sql;

-- SESSION_COUNTER_EXCEPT - new tabel for exception reporting
@REPORT.SESSION_COUNTER_EXCEPT_ddl.sql;

-- reconciliation package - add a new stored proc to generate the data for the above table
@REPORT.PKG_RECONCILIATION_ddl.sql;


