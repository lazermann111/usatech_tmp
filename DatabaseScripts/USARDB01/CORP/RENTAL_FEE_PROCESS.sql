/* *************************************************************************
* RENTAL PROGRAM FEE UPDATE
* 
* 3. UPDATE RENTAL ADMIN FEES
* 	THIS SCRIPT MUST BE RUN TO LOAD FEES INTO CORP.SERVICE_FEES
*	PER TERMINAL PROVIDED BY SPREADSHEET.
*
* MODIFICATIONS:	
* AUTH		DATE		BUG#	  	DESCRIPTION
* ------------------------------------------------------------------------
* EPC		02/16/2010	NA			Edit insert/update logic to look for a fee_id = 1
*									instead of an existing one with fee_id = 6 and frequency_id = 2.
*									Changed from using the REPORT.TERMINAL table to get the termination date,
*									to using REPORT.TERMINAL_EPORT table which has END_DATE field.
*									Added where predicate EPORT_SERIAL_NUM on check for end date.
*									Switched from EXPLICT CURSOR to IMPLICIT CURSOR to implictly open/close cursor.
*									Now check for a non-null LAST_PAYMENT date, if exists, just print a message, do not add fee.
* EPC		02/18/2010	NA			Removed check on LAST_PAYMENT date.		
*                         			Removed FEE_ID filter on looking for existing fees.
*                         			If needed, just set the END_DATE on current fee to ship_date if closing it out.
* EPC		02/19/2010	NA			Switched logic to only check for existing ledger entries; if exist, print message, else do insert.	
***************************************************************************/
SET SERVEROUTPUT ON
declare

  the_terminal_number VARCHAR2(30);
  the_fee_id NUMBER := 6;
  the_frequency_id NUMBER := 2;
  the_total_fees_to_process NUMBER := 0;
  the_end_date date;
  
  temp_last_payment date;
  temp_ledger_count NUMBER := 0;
  temp_start_date date;
  temp_terminal_id  NUMBER (21,2);
  temp_old_service_fee_id  NUMBER (21,2);
  
  the_counter NUMBER := 0;
  the_terminal_id_exists VARCHAR2(5);
  the_program_staus VARCHAR2(40);
  err_num NUMBER;
  err_msg VARCHAR2(200);
  
  CURSOR fee_info_cursor IS

		SELECT  EPORT_SERIAL_NUMBER, TERMINAL_NUMBER, SHIP_DATE,  PROCESS_FEE, SERVICE_FEE, ADMIN_FEE

		FROM TMP_fee_info_list

		order by EPORT_SERIAL_NUMBER

		FOR UPDATE;

BEGIN
	
	/*  Get the total number of fees we'll attempt to process */
	
    select count(1) into the_total_fees_to_process from TMP_fee_info_list;
	
	dbms_output.put_line('STARTING RENTAL PROGRAM FEE UPDATE - LOAD FEE DATA for '|| the_total_fees_to_process || ' terminals...');
	
    BEGIN
		
		/* Retrieve each row of the result FROM THE TEMP TABLE into PL/SQL variables: */
		
		FOR fee_info_rec IN fee_info_cursor LOOP
      
		
			
      /* Track the current terminal number, terminal id so we can report them in case of exception. */
      
      the_terminal_number := null;
      the_terminal_id_exists := null;
      temp_terminal_id := null;
      the_terminal_number := fee_info_rec.TERMINAL_NUMBER;
      
	  /* Get the termination date in case the terminal or eport device is no longer active. */
      	
      	begin
          the_terminal_id_exists := null;
	      	select  te.terminal_id, te.end_date into temp_terminal_id, the_end_date
          from report.terminal_eport te, report.eport e, report.terminal t 
          where e.eport_serial_num = fee_info_rec.EPORT_SERIAL_NUMBER  
          and e.eport_id = te.eport_id and 
          te.terminal_id = t.terminal_id
          and t.terminal_nbr = fee_info_rec.TERMINAL_NUMBER;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN 
            the_terminal_id_exists := 'false';
        end;
       
      if(the_terminal_id_exists = 'false') then
        dbms_output.put_line('### NO_DATA_FOUND for TERMINAL_NUMBER='||fee_info_rec.TERMINAL_NUMBER||', EPORT_SERIAL_NUMBER='||fee_info_rec.EPORT_SERIAL_NUMBER);
      else
        /* Check if this current terminal id exists in the fees table with the fee_id and frequency_id below*/
        begin
          temp_ledger_count := 0;
          select count(1) into temp_ledger_count from corp.ledger where service_fee_id in (select sf.service_fee_id from CORP.SERVICE_FEES sf where sf.terminal_id = temp_terminal_id);
        end;
        /* 	If the TERMINAL_ID does not exist for the FEE_ID and FREQUENCY_ID, then insert. 
          If it does exist, set the END_DATE on the the current/latest fee record, and insert a new fee record. 
          
          NOTE:
          The Current Eport Manager App fee related code takes the current/latest record's LAST_PAYMENT date when ending a fee record (essentially updating the end_date) 
          and uses this value as the LAST_PAYMENT of the newly inserted fee record.
          
          In order to use the ship date, we will slightly modify this logic, ONLY for the scenario where there is an existing CORP.SERVICE_FEE record:
            SET THE END DATE ON current/latest record's
            --------------------------------------------
              Any UPDATED Fee Records will use the currently fetched ship_date from the cursor as the END_DATE on the current/latest record in CORP.SERVICE_FEES.  This will effectively close it out.
              Any INSERTED records will use a LAST_PAYMENT and START_DATE = ship_date (from the temp table/cursor).
        */
      
        if temp_ledger_count > 0 then
          dbms_output.put_line('### SERVICE FEE(s) ALREADY PROCESSED IN LEDGER! : TERMINAL_NUM='||fee_info_rec.TERMINAL_NUMBER ||', TERMINAL_ID='||temp_terminal_id|| ' with ' || temp_ledger_count || ' service_fee entries in CORP.LEDGER.');
        else
        	begin
	            update CORP.SERVICE_FEES sf set sf.end_date = fee_info_rec.SHIP_DATE where sf.terminal_id = temp_terminal_id and sf.frequency_id = the_frequency_id;
	            if SQL%ROWCOUNT > 0 then 
	              dbms_output.put('UPDATED LAST CORP.SERVICE_FEES record! Closed out END_DATE on TERMINAL_ID='||temp_terminal_id||' AND ');
	            end if;
            end;
          
          insert into CORP.SERVICE_FEES  (terminal_id, fee_id, fee_amount, frequency_id, last_payment, start_date, end_date, service_fee_id, create_date, fee_percent) 
          values(temp_terminal_id, 
          the_fee_id, 
          fee_info_rec.ADMIN_FEE, 
          the_frequency_id, 
          fee_info_rec.SHIP_DATE, 
          fee_info_rec.SHIP_DATE, 
          --case when the_end_date is null then null else to_date(the_end_date, 'mm/dd/yyyy') end, 
          the_end_date, 
          corp.service_fee_seq.NEXTVAL, 
          SYSDATE, 
          null) ;
          dbms_output.put('ADDED CORP.SERVICE_FEES record for TERMINAL_NUM='||fee_info_rec.TERMINAL_NUMBER ||', TERMINAL_ID='||temp_terminal_id);
        if(the_end_date is not null) then
          dbms_output.put_line(' which was terminated on '||the_end_date);
        else
          dbms_output.put_line('');
        end if;
        the_counter := the_counter + 1;
      end if;
    end if;
      
	END LOOP;
	
	the_program_staus := 'SUCCESS ('||the_counter||' fee records loaded)' ;
	
  EXCEPTION
   WHEN OTHERS THEN
      err_num := SQLCODE;
      err_msg := SUBSTR(SQLERRM, 1, 100);
      dbms_output.put_line('### ALERT! EXCEPTION OCCURRED DURING PROCESSING OF RECORD # ' || the_counter || ', TERMINAL_NUM='||the_terminal_number||', TERMINAL_ID='||temp_terminal_id||'.');
      dbms_output.put_line('### ALERT! EXCEPTION WAS ERR_NUM: '||err_num ||', ERR_MSG: '||err_msg||'.');
      the_program_staus := 'FAILURE';
      rollback;
      
  END;
	
  dbms_output.put_line('COMPLETED RENTAL PROGRAM FEE UPDATE WITH ' ||the_program_staus||' - LOAD FEE DATA.');
  commit;

END;
/