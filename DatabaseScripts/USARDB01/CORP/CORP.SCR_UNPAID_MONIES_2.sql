

DROP TABLE corp.scr_unpaid_monies;

CREATE TABLE corp.scr_unpaid_monies
    (business_unit_id              NUMBER(20,0) NOT NULL,
    ledger_id                      NUMBER(20,0) NOT NULL,
    customer_bank_id               NUMBER(20,0) NOT NULL,
    entry_type                     CHAR(2) NOT NULL,
    amount                         NUMBER(21,8) DEFAULT 0  NOT NULL,
    gen_for_ts                     DATE NOT NULL)
  TABLESPACE  corp_data;


