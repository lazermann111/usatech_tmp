/* *************************************************************************
* RENTAL PROGRAM FEE UPDATE - PROCESS_FEES
* 
* 3. UPDATE RENTAL ADMIN PROCESS_FEE PERCENTAGES
* 	THIS SCRIPT MUST BE RUN TO LOAD FEES INTO CORP.PROCESS_FEES
*	PER TERMINAL PROVIDED BY SPREADSHEET.
*
* MODIFICATIONS:	
* AUTH		DATE		BUG#	  	DESCRIPTION
* ------------------------------------------------------------------------
* EPC		02/24/2010	NA			INIT
*
***************************************************************************/

SET SERVEROUTPUT ON
declare
the_process_fee_pct  NUMBER (21,4) := 0.0595;
the_min_fee_amt  NUMBER (21,2) := 0.05;
the_process_fee_amt  NUMBER (21) := 0;
the_trans_type_id  NUMBER (21) := 16;
the_total_fees_to_process NUMBER := 0;
temp_terminal_id  NUMBER (21);

the_service_fees_run_date VARCHAR2(20) := '22-FEB-10';

err_num NUMBER;
err_msg VARCHAR2(200);

the_counter NUMBER := 0;
    
BEGIN
  /* Start the process, display total number of terminal_ids to update. */
  
  select count(1) into the_total_fees_to_process 
  from 
    (select distinct terminal_id from corp.service_fees where fee_id = 6 and fee_amount = 9.95 and trunc(create_date) = the_service_fees_run_date);
  
  dbms_output.put_line('STARTING RENTAL PROGRAM PROCESS_FEE UPDATE - LOAD PROCESS_FEE DATA for '|| the_total_fees_to_process || ' terminals...');
  
  /* Start the FOR LOOP. */
  
  FOR process_fee_rec IN (select distinct terminal_id from corp.service_fees where fee_id = 6 and fee_amount = 9.95 and trunc(create_date) = the_service_fees_run_date) 
  
  LOOP
    temp_terminal_id := process_fee_rec.TERMINAL_ID;
    
    /* UPDATE counter. */
    
    the_counter := the_counter +1;
    
    BEGIN
      /* Update process_fee for terminal_id. */ 
      
      CORP.payments_PKG.PROCESS_FEES_UPD(process_fee_rec.TERMINAL_ID, the_trans_type_id, the_process_fee_pct, the_process_fee_amt, the_min_fee_amt, sysdate, null);
      
      dbms_output.put_line('PROCESS_FEE UPDATE COMPLETED FOR : COUNTER: '||the_counter|| ', TERMINAL_ID: ' ||process_fee_rec.TERMINAL_ID|| '.');
      
    EXCEPTION WHEN OTHERS THEN
      err_num := SQLCODE;
      err_msg := SUBSTR(SQLERRM, 1, 100);
      dbms_output.put_line('### ALERT!EXCEPTION OCCURRED DURING PROCESS_FEE UPDATE: COUNTER: '||the_counter|| ', TERMINAL_ID: ' ||process_fee_rec.TERMINAL_ID|| ', ERR_NUM: '||err_num ||', ERR_MSG: '||err_msg||'.');
    
    END;
  
  /* END LOOP. */ 
  
  END LOOP;
  
  /* END PROCESS. */ 
  dbms_output.put_line('COMPLETED RENTAL PROGRAM PROCESS_FEE UPDATE FOR ' ||the_counter|| ' OF ' ||the_total_fees_to_process|| ' terminals.');
  
  commit;
  
EXCEPTION WHEN OTHERS THEN
  err_num := SQLCODE;
  err_msg := SUBSTR(SQLERRM, 1, 100);
  dbms_output.put_line('### ALERT!EXCEPTION OCCURRED DURING PROCESS_FEE UPDATE: CURRENT TERMINAL_ID: ' ||temp_terminal_id|| ', ERR_NUM: '||err_num ||', ERR_MSG: '||err_msg||'.');

END;
/
    