/* Formatted on 4/19/2010 12:43:22 PM (QP5 v5.126.903.23003) */
CREATE OR REPLACE FORCE VIEW CORP.VW_PENDING_OR_LOCKED_PAYMENTS (
   EFT_ID,
   STATUS,
   BATCH_REF_NBR,
   DESCRIPTION,
   CUSTOMER_NAME,
   CUSTOMER_BANK_ID,
   CURRENCY_ID,
   BANK_ACCT_NBR,
   BANK_ROUTING_NBR,
   PAY_MIN_AMOUNT,
   PAY_CYCLE_ID,
   ACCOUNT_TITLE,
   PAYMENT_METHOD,
   BUSINESS_UNIT_NAME,
   BATCH_DATE,
   GROSS_AMOUNT,
   REFUND_AMOUNT,
   CHARGEBACK_AMOUNT,
   PROCESS_FEE_AMOUNT,
   SERVICE_FEE_AMOUNT,
   ADJUST_AMOUNT,
   FAILED_AMOUNT,
   NET_AMOUNT
)
AS
     SELECT   P.DOC_ID,
              DECODE (P.STATUS,
                      'L',
                      'Locked',
                      'O',
                      ''),
              D.REF_NBR,
              D.DESCRIPTION,
              C.CUSTOMER_NAME,
              D.CUSTOMER_BANK_ID,
              D.CURRENCY_ID,
              D.BANK_ACCT_NBR,
              D.BANK_ROUTING_NBR,
              CB.PAY_MIN_AMOUNT,
              CB.PAY_CYCLE_ID,
              CB.ACCOUNT_TITLE,
              DECODE (CB.IS_EFT, 'Y', 'Elec', 'Paper'),
              BU.BUSINESS_UNIT_NAME,
              NVL (MAX (BATCH_DATE), MIN (MIN_AS_ACCUM_DATE)),
              SUM(CASE
                     WHEN P.ENTRY_TYPE = 'CC' AND P.PAYABLE = 'Y' THEN P.AMOUNT
                     ELSE NULL
                  END)
                 CREDIT_AMOUNT,
              SUM(CASE
                     WHEN P.ENTRY_TYPE = 'RF' AND P.PAYABLE = 'Y' THEN P.AMOUNT
                     ELSE NULL
                  END)
                 REFUND_AMOUNT,
              SUM(CASE
                     WHEN P.ENTRY_TYPE = 'CB' AND P.PAYABLE = 'Y' THEN P.AMOUNT
                     ELSE NULL
                  END)
                 CHARGEBACK_AMOUNT,
              SUM(CASE
                     WHEN P.ENTRY_TYPE = 'PF' AND P.PAYABLE = 'Y' THEN P.AMOUNT
                     ELSE NULL
                  END)
                 PROCESS_FEE_AMOUNT,
              NULLIF (
                 NVL (
                    SUM(CASE
                           WHEN P.ENTRY_TYPE = 'SF' AND P.PAYABLE = 'Y'
                           THEN
                              P.AMOUNT
                           ELSE
                              NULL
                        END),
                    0
                 )
                 + LEAST (
                      NVL (
                         SUM(CASE
                                WHEN P.ENTRY_TYPE = 'NR' AND P.PAYABLE = 'Y'
                                THEN
                                   P.AMOUNT
                                ELSE
                                   NULL
                             END),
                         0
                      ),
                      0
                   ),
                 0
              )
                 SERVICE_FEE_AMOUNT,
              SUM(CASE
                     WHEN P.ENTRY_TYPE = 'AD' AND P.PAYABLE = 'Y' THEN P.AMOUNT
                     ELSE NULL
                  END)
                 ADJUST_AMOUNT,
              SUM(CASE
                     WHEN P.ENTRY_TYPE IN ('CC') AND P.PAYABLE = 'N'
                     THEN
                        -P.AMOUNT
                     ELSE
                        NULL
                  END)
                 FAILED_AMOUNT,
              SUM(CASE
                     WHEN P.PAYABLE = 'Y' AND P.ENTRY_TYPE <> 'NR'
                     THEN
                        P.AMOUNT
                     ELSE
                        NULL
                  END)
              + LEAST (
                   NVL (
                      SUM(CASE
                             WHEN P.ENTRY_TYPE = 'NR' AND P.PAYABLE = 'Y'
                             THEN
                                P.AMOUNT
                             ELSE
                                NULL
                          END),
                      0
                   ),
                   0
                )
                 NET_AMOUNT
       FROM   (SELECT   P.DOC_ID,
                        P.STATUS,
                        CASE
                           WHEN P.PAYMENT_SCHEDULE_ID = 5
                                AND P.ENTRY_TYPE IN ('CC', 'PF')
                           THEN
                              'AD'
                           --WHEN P.STATUS NOT IN('O', 'L') AND P.PAYMENT_SCHEDULE_ID = 7 AND P.ENTRY_TYPE IN('AD') THEN 'SF'
                        WHEN SF.FREQUENCY_ID = 6
                           THEN
                              'NR' --WHEN P.STATUS IN('O', 'L') AND SF.FREQUENCY_ID = 6 THEN 'NR'
                           ELSE
                              P.ENTRY_TYPE
                        END
                           ENTRY_TYPE,
                        CORP.PAYMENTS_PKG.ENTRY_PAYABLE (P.SETTLE_STATE_ID,
                                                         P.ENTRY_TYPE)
                           PAYABLE,
                        P.AMOUNT,
                        CASE
                           WHEN P.PAYMENT_SCHEDULE_ID NOT IN (1, 5, 7)
                           THEN
                              MAX_START_DATE
                           ELSE
                              NULL
                        END
                           BATCH_DATE,
                        CASE
                           WHEN P.PAYMENT_SCHEDULE_ID IN (1)
                           THEN
                              MIN_START_DATE
                           ELSE
                              NULL
                        END
                           MIN_AS_ACCUM_DATE
                 FROM      (  SELECT   /*+ USE_NL(L,B) */ D.DOC_ID,
                                       D.STATUS,
                                       B.PAYMENT_SCHEDULE_ID,
                                       L.ENTRY_TYPE,
                                       L.SETTLE_STATE_ID,
                                       L.SERVICE_FEE_ID,
                                       SUM (L.AMOUNT) AMOUNT,
                                       MAX (B.START_DATE) MAX_START_DATE,
                                       MIN (B.START_DATE) MIN_START_DATE
                                FROM         CORP.LEDGER L
                                          INNER JOIN
                                             CORP.BATCH B
                                          ON L.BATCH_ID = B.BATCH_ID
                                       INNER JOIN
                                          CORP.DOC D
                                       ON B.DOC_ID = D.DOC_ID
                               WHERE       L.DELETED = 'N'
                                       AND D.STATUS IN ('L', 'O')
                                       AND (D.STATUS = 'L'
                                            OR CORP.PAYMENTS_PKG.BATCH_CLOSABLE (
                                                 B.BATCH_ID,
                                                 B.PAYMENT_SCHEDULE_ID,
                                                 B.END_DATE,
                                                 B.BATCH_STATE_CD
                                              ) = 'Y')
                            GROUP BY   D.DOC_ID,
                                       D.STATUS,
                                       B.PAYMENT_SCHEDULE_ID,
                                       L.ENTRY_TYPE,
                                       L.SETTLE_STATE_ID,
                                       l.SERVICE_FEE_ID) P
                        LEFT JOIN
                           CORP.SERVICE_FEES SF
                        ON P.SERVICE_FEE_ID = SF.SERVICE_FEE_ID
                           AND SF.FREQUENCY_ID = 6) P,
              CORP.DOC D,
              CORP.CUSTOMER_BANK CB,
              CORP.CUSTOMER C,
              CORP.BUSINESS_UNIT BU
      WHERE       P.DOC_ID = D.DOC_ID
              AND D.CUSTOMER_BANK_ID = CB.CUSTOMER_BANK_ID
              AND CB.CUSTOMER_ID = C.CUSTOMER_ID
              AND D.BUSINESS_UNIT_ID = BU.BUSINESS_UNIT_ID(+)
   GROUP BY   C.CUSTOMER_NAME,
              D.BANK_ACCT_NBR,
              P.DOC_ID,
              P.STATUS,
              D.CURRENCY_ID,
              D.CUSTOMER_BANK_ID,
              D.BANK_ROUTING_NBR,
              D.REF_NBR,
              D.DESCRIPTION,
              CB.PAY_MIN_AMOUNT,
              DECODE (CB.IS_EFT, 'Y', 'Elec', 'Paper'),
              BU.BUSINESS_UNIT_NAME,
              CB.PAY_CYCLE_ID,
              CB.ACCOUNT_TITLE;


GRANT DELETE, INSERT, SELECT, UPDATE ON CORP.VW_PENDING_OR_LOCKED_PAYMENTS TO USATECH_UPD_TRANS;

GRANT DELETE, INSERT, SELECT, UPDATE ON CORP.VW_PENDING_OR_LOCKED_PAYMENTS TO USAT_ADMIN_ROLE;

GRANT SELECT ON CORP.VW_PENDING_OR_LOCKED_PAYMENTS TO USAT_DEV_READ_ONLY;

