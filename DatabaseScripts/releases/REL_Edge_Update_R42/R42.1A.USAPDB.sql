UPDATE REPORT.REPORT_PARAM
SET PARAM_VALUE = 'SELECT 
led.entry_type,
NVL(dt.business_unit_name, ''~ Terminal Orphan'') business_unit_name,
NVL(dt.monthly_payment, ''UNK'') monthly_payment,
cr.currency_code,
NVL(dt.customer_name, ''~ Terminal Orphan'') customer_name,
TO_CHAR(TRUNC(DECODE (led.entry_type,
	''CC'', tr.settle_date,
	led.ledger_date
), ''MONTH''), ''mm/dd/yyyy'') month_for,
COUNT(1) tran_count,
SUM(tr.total_amount) total_tran_amount,
SUM(led.amount) total_ledger_amount,
tr.trans_type_id,
tt.trans_type_name
FROM report.trans tr
JOIN corp.currency cr
ON cr.currency_id = tr.currency_id
JOIN report.trans_state ts
ON ts.state_id = tr.settle_state_id 
LEFT OUTER JOIN (
	SELECT
	t.terminal_id,
	c.customer_name,
	bu.business_unit_name,
	DECODE(t.payment_schedule_id, 4, ''Y'', ''N'') monthly_payment
	FROM report.terminal t
	JOIN corp.customer c
	ON c.customer_id = t.customer_id
	JOIN corp.business_unit bu
	ON bu.business_unit_id = t.business_unit_id
) dt
ON dt.terminal_id = tr.terminal_id
JOIN corp.ledger led
ON led.trans_id = tr.tran_id
AND led.deleted = ''N''
JOIN report.trans_type tt
ON tr.trans_type_id = tt.trans_type_id
WHERE tr.settle_state_id IN (2, 3) 
AND led.entry_type IN (''CC'', ''PF'', ''CB'', ''RF'') 
AND NVL(tr.settle_date, led.create_date) >= CAST(? AS DATE) AND NVL(tr.settle_date, led.create_date) < CAST(? AS DATE) + 1
GROUP BY
led.entry_type,
NVL(dt.business_unit_name, ''~ Terminal Orphan''),
NVL(dt.monthly_payment, ''UNK''),
cr.currency_code,
NVL(dt.customer_name, ''~ Terminal Orphan''),
TRUNC(DECODE (led.entry_type,
	''CC'', tr.settle_date,
	led.ledger_date
), ''MONTH''),
tr.trans_type_id,
tt.trans_type_name'
WHERE REPORT_ID = (SELECT REPORT_ID FROM REPORT.REPORTS WHERE REPORT_NAME = 'Transaction Processing Entry Summary')
AND PARAM_NAME = 'query';
COMMIT;

UPDATE REPORT.REPORT_PARAM
SET PARAM_VALUE = 'SELECT
e.eport_serial_num,
dt.device_type_desc,
tt.trans_type_name,
ts.state_label settle_state,
NVL2(tr.terminal_id, ''TB'', ''T'') orphan_type,
NVL(ssy.source_system_name, ''~ Legacy'') source_system_name,
NVL(cr.currency_code, ''USD'') currency_code,
TO_CHAR(TRUNC(tr.settle_date, ''MONTH''), ''mm/dd/yyyy'') month_for,
COUNT(1) tran_count,
SUM(tr.total_amount) total_amount
FROM report.trans tr
JOIN report.eport e
ON e.eport_id = tr.eport_id
JOIN report.device_type dt
ON dt.device_type_id = e.device_type_id
JOIN report.trans_type tt
ON tr.trans_type_id = tt.trans_type_id
JOIN report.trans_state ts
ON ts.state_id = tr.settle_state_id -- yes, this is non intuituve but correct
LEFT OUTER JOIN corp.currency cr
ON cr.currency_id = tr.currency_id
LEFT OUTER JOIN report.source_system ssy
ON ssy.source_system_cd = tr.source_system_cd
WHERE tr.settle_date < TRUNC(CAST(? AS DATE) + 1, ''MONTH'')
AND tt.payable_ind = ''Y''
AND tr.settle_state_id IN (2, 3)
AND DECODE(tr.customer_bank_id, NULL, 0) = 0
GROUP BY
e.eport_serial_num,
dt.device_type_desc,
tt.trans_type_name,
ts.state_label,
NVL2(tr.terminal_id, ''TB'', ''T''),
NVL(ssy.source_system_name, ''~ Legacy''),
NVL(cr.currency_code, ''USD''),
TRUNC(tr.settle_date, ''MONTH'')
ORDER BY 8, 2, 3, 4, 5, 6, 7, 1'
WHERE REPORT_ID = (SELECT REPORT_ID FROM REPORT.REPORTS WHERE REPORT_NAME = 'Orphan Device History')
AND PARAM_NAME = 'query';
COMMIT;
