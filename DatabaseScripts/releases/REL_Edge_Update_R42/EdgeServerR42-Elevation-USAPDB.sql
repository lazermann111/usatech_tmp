WHENEVER SQLERROR EXIT FAILURE COMMIT;

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/releases/REL_Edge_Update_R42/CANTEEN_REST.1.sql?rev=HEAD
GRANT SELECT ON PSS.SALE to USAT_RPT_GEN_ROLE;

GRANT SELECT ON REPORT.TRANS_TYPE to USAT_APP_LAYER_ROLE;
GRANT SELECT ON REPORT.TERMINAL_EPORT to USAT_APP_LAYER_ROLE;
GRANT SELECT ON REPORT.REPORTS to USAT_APP_LAYER_ROLE;
GRANT SELECT ON REPORT.USER_REPORT to USAT_APP_LAYER_ROLE;
GRANT SELECT ON REPORT.CCS_TRANSPORT to USAT_APP_LAYER_ROLE;
GRANT SELECT ON REPORT.USER_LOGIN to USAT_APP_LAYER_ROLE;
GRANT SELECT ON REPORT.VW_USER_TERMINAL to USAT_APP_LAYER_ROLE;
GRANT SELECT ON REPORT.ACTIVITY_REF to USAT_APP_LAYER_ROLE;


GRANT SELECT ON CORP.BATCH_TOTAL to REPORT;
GRANT SELECT ON ENGINE.APP_SETTING to REPORT;


INSERT INTO ENGINE.APP_SETTING (APP_SETTING_CD, APP_SETTING_VALUE, APP_SETTING_DESC)
VALUES('EFT_MAX_CC_COUNT', 1000, 'Maximum number of credit transactions that can be in a eft for reporting');

--delete from ENGINE.APP_SETTING where APP_SETTING_CD='EFT_MAX_CC_COUNT';

INSERT INTO CORP.PAY_CYCLE(PAY_CYCLE_ID, PAY_CYCLE_NAME)
VALUES(12, 'Daily or exceeding maximum number of credit transactions');

INSERT INTO REPORT.EXPORT_TYPE (EXPORT_TYPE_ID, NAME, DESCRIPTION, STATUS)
VALUES(10, 'SINGLE_TXN', 'Single Transaction', 'A');

INSERT INTO REPORT.GENERATOR (GENERATOR_ID, NAME)
VALUES(11, 'Credit XML');


--INSERT INTO REPORT.EXPORT_TYPE (EXPORT_TYPE_ID, NAME, DESCRIPTION, STATUS)
--VALUES(11, 'EFT_XML', 'EFT Xml', 'A');

INSERT INTO REPORT.GENERATOR (GENERATOR_ID, NAME)
VALUES(12, 'EFT XML');


INSERT INTO REPORT.CCS_TRANSPORT_TYPE values(9, 'REST', 'A', 9);
INSERT INTO REPORT.CCS_TRANSPORT_PROPERTY_TYPE
(CCS_TRANSPORT_PROPERTY_TYPE_ID,CCS_TRANSPORT_TYPE_ID,CCS_TPT_NAME,CCS_TPT_REQUIRED_FLAG, CCS_TPT_EDITOR)
VALUES( report.CCS_TRANSPORT_PROPERTY_SEQ.nextval, 9, 'URL', 'Y',null);
INSERT INTO REPORT.CCS_TRANSPORT_PROPERTY_TYPE
(CCS_TRANSPORT_PROPERTY_TYPE_ID,CCS_TRANSPORT_TYPE_ID,CCS_TPT_NAME,CCS_TPT_REQUIRED_FLAG, CCS_TPT_EDITOR)
VALUES( report.CCS_TRANSPORT_PROPERTY_SEQ.nextval, 9, 'Username', 'N','TEXT');
INSERT INTO REPORT.CCS_TRANSPORT_PROPERTY_TYPE
(CCS_TRANSPORT_PROPERTY_TYPE_ID,CCS_TRANSPORT_TYPE_ID,CCS_TPT_NAME,CCS_TPT_REQUIRED_FLAG, CCS_TPT_EDITOR)
VALUES( report.CCS_TRANSPORT_PROPERTY_SEQ.nextval, 9, 'Password', 'N','PASSWORD');

--drop table REPORT.TRANSPORT_REPORT_MAP;
--drop sequence REPORT.SEQ_TRANSPORT_REPORT_MAP_ID;

CREATE TABLE REPORT.TRANSPORT_REPORT_MAP(
	TRANSPORT_REPORT_MAP_ID NUMBER NOT NULL,
    CCS_TRANSPORT_TYPE_ID NUMBER NOT NULL,
    REPORT_ID NUMBER NOT NULL,
    CREATED_BY VARCHAR2(30) NOT NULL,
    CREATED_UTC_TS TIMESTAMP NOT NULL,
    LAST_UPDATED_BY VARCHAR2(30) NOT NULL,
    LAST_UPDATED_UTC_TS TIMESTAMP NOT NULL,
    CONSTRAINT PK_REPORT_TRANSPORT_MAP_ID  PRIMARY KEY(TRANSPORT_REPORT_MAP_ID)
) TABLESPACE REPORT_DATA;

CREATE SEQUENCE REPORT.SEQ_TRANSPORT_REPORT_MAP_ID MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE NOORDER NOCYCLE;

CREATE OR REPLACE TRIGGER REPORT.TRBI_TRANSPORT_REPORT_MAP BEFORE 
INSERT ON REPORT.TRANSPORT_REPORT_MAP FOR EACH ROW 
BEGIN
    IF :NEW.TRANSPORT_REPORT_MAP_ID is NULL THEN
	:NEW.TRANSPORT_REPORT_MAP_ID:= REPORT.SEQ_TRANSPORT_REPORT_MAP_ID.NEXTVAL;
    END IF;

    SELECT 
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER,
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER
    INTO 
        :NEW.CREATED_UTC_TS,
        :NEW.CREATED_BY,
        :NEW.LAST_UPDATED_UTC_TS,
        :NEW.LAST_UPDATED_BY
    FROM DUAL;
END;  
/

CREATE OR REPLACE TRIGGER REPORT.TRBU_TRANSPORT_REPORT_MAP BEFORE 
UPDATE ON REPORT.TRANSPORT_REPORT_MAP FOR EACH ROW 
BEGIN
    SELECT
        :OLD.CREATED_BY,
        :OLD.CREATED_UTC_TS,
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER
    INTO
        :NEW.CREATED_BY,
        :NEW.CREATED_UTC_TS,
        :NEW.LAST_UPDATED_UTC_TS,
        :NEW.LAST_UPDATED_BY
    FROM DUAL;
END; 
/

GRANT SELECT ON REPORT.TRANSPORT_REPORT_MAP TO USALIVE_APP_ROLE; 


 



-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/releases/REL_Edge_Update_R42/CANTEEN_REST.2.sql?rev=HEAD
SET DEFINE OFF;
DECLARE 
	l_user_id NUMBER;
BEGIN
  
select user_id into l_user_id from report.user_login ul cross join V$DATABASE d 
where user_name=(CASE WHEN d.name like 'USADEV%' THEN 'yhe@usatech.com'
WHEN d.name like 'ECC%' THEN 'aroyce@usatech.com'
ELSE 'compassgroup' END);
  
INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
VALUES( REPORT.REPORTS_SEQ.NEXTVAL, 'Credit xml', 11, 10, 'Credit xml', 'Xml that contains credit transaction data', 'N', l_user_id);

INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
VALUES( REPORT.REPORTS_SEQ.NEXTVAL, 'EFT xml', 12, 3, 'EFT xml', 'Xml that contains payment and transaction data', 'N', l_user_id);

COMMIT;

END;
/

INSERT INTO REPORT.TRANSPORT_REPORT_MAP(CCS_TRANSPORT_TYPE_ID, REPORT_ID)
SELECT 9, REPORT_ID FROM REPORT.REPORTS WHERE REPORT_NAME='Credit xml';

INSERT INTO REPORT.TRANSPORT_REPORT_MAP(CCS_TRANSPORT_TYPE_ID, REPORT_ID)
SELECT 9, REPORT_ID FROM REPORT.REPORTS WHERE REPORT_NAME='EFT xml';

COMMIT;



-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/DATA_IN_PKG.psk?rev=1.20
CREATE OR REPLACE PACKAGE REPORT.DATA_IN_PKG IS
--
-- Receives data from external systems
--
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------
-- BKRUG        10-18-04    NEW

  PROCEDURE ADD_TRAN_ITEM(
        l_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE,
        l_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE,
        l_column_num PURCHASE.MDB_NUMBER%TYPE,
        l_column_label PURCHASE.VEND_COLUMN%TYPE,
        l_quantity PURCHASE.AMOUNT%TYPE DEFAULT 1,
        l_price PURCHASE.PRICE%TYPE DEFAULT NULL,
        l_product_desc PURCHASE.DESCRIPTION%TYPE DEFAULT NULL,
        l_tran_line_item_type_id PURCHASE.TRAN_LINE_ITEM_TYPE_ID%TYPE DEFAULT NULL,
        l_apply_to_consumer_acct_id PURCHASE.APPLY_TO_CONSUMER_ACCT_ID%TYPE DEFAULT NULL);

  PROCEDURE UPDATE_DEVICE_INFO(
        l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
        l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE,
        l_device_type_id EPORT.DEVICE_TYPE_ID%TYPE/*,
        l_device_name EPORT.DEVICE_NAME%TYPE*/);

  PROCEDURE ADD_FILL(
       l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
       l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE,
       l_fill_date IN FILL.FILL_DATE%TYPE);
       
   -- R42 and above   
   PROCEDURE ADD_TRANSACTION(
        l_dup_flag OUT VARCHAR,
        l_report_tran_id OUT REPORT.TRANS.TRAN_ID%TYPE,
        l_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE,
        l_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE,
        l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
        l_start_date TRANS.START_DATE%TYPE,
        l_close_date TRANS.CLOSE_DATE%TYPE,
        l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE,
        l_total_amount TRANS.TOTAL_AMOUNT%TYPE,
        l_card_number TRANS.CARD_NUMBER%TYPE,
        l_received_date TRANS.SERVER_DATE%TYPE,
        l_settle_state_id TRANS.SETTLE_STATE_ID%TYPE,
        l_settle_date TRANS.SETTLE_DATE%TYPE,
        l_preauth_amount TRANS.PREAUTH_AMOUNT%TYPE,
        l_preauth_date   TRANS.PREAUTH_DATE%TYPE,
        l_approval_cd TRANS.CC_APPR_CODE%TYPE,
        l_merchant_num CORP.MERCHANT.MERCHANT_NBR%TYPE,
        l_description TRANS.DESCRIPTION%TYPE,
        l_orig_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE DEFAULT NULL,
        l_orig_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE DEFAULT NULL,
        l_currency_code CORP.CURRENCY.CURRENCY_CODE%TYPE,
        l_device_type EPORT.DEVICE_TYPE_ID%TYPE DEFAULT NULL,
        l_card_name REPORT.CARD_TYPE.CARD_NAME%TYPE DEFAULT NULL,
        l_consumer_acct_id TRANS.CONSUMER_ACCT_ID%TYPE DEFAULT NULL);
       
    -- R37 to R41
    PROCEDURE ADD_TRANSACTION(
        l_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE,
        l_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE,
        l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
        l_start_date TRANS.START_DATE%TYPE,
        l_close_date TRANS.CLOSE_DATE%TYPE,
        l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE,
        l_total_amount TRANS.TOTAL_AMOUNT%TYPE,
        l_card_number TRANS.CARD_NUMBER%TYPE,
        l_received_date TRANS.SERVER_DATE%TYPE,
        l_settle_state_id TRANS.SETTLE_STATE_ID%TYPE,
        l_settle_date TRANS.SETTLE_DATE%TYPE,
        l_preauth_amount TRANS.PREAUTH_AMOUNT%TYPE,
        l_preauth_date   TRANS.PREAUTH_DATE%TYPE,
        l_approval_cd TRANS.CC_APPR_CODE%TYPE,
        l_merchant_num CORP.MERCHANT.MERCHANT_NBR%TYPE,
        l_description TRANS.DESCRIPTION%TYPE,
        l_orig_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE DEFAULT NULL,
        l_orig_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE DEFAULT NULL,
        l_currency_code CORP.CURRENCY.CURRENCY_CODE%TYPE,
        l_device_type EPORT.DEVICE_TYPE_ID%TYPE DEFAULT NULL,
        l_card_name REPORT.CARD_TYPE.CARD_NAME%TYPE DEFAULT NULL,
        l_consumer_acct_id TRANS.CONSUMER_ACCT_ID%TYPE DEFAULT NULL);

  PROCEDURE UPDATE_SETTLE_INFO(
        l_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE,
        l_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE,
        l_settle_state_id TRANS.SETTLE_STATE_ID%TYPE,
        l_settle_date TRANS.SETTLE_DATE%TYPE,
        l_approval_cd TRANS.CC_APPR_CODE%TYPE,
        l_merchant_num CORP.MERCHANT.MERCHANT_NBR%TYPE,
        ln_amount REPORT.TRANS.TOTAL_AMOUNT%TYPE DEFAULT NULL,
        pn_override_trans_type_id REPORT.TRANS.TRANS_TYPE_ID%TYPE DEFAULT NULL);

  PROCEDURE UPDATE_BEX_LOCATION(
        l_serial_num EPORT.EPORT_SERIAL_NUM%TYPE,
        l_location_name LOCATION.LOCATION_NAME%TYPE,
        l_customer_abbr CORP.CUSTOMER.CUSTOMER_ALT_NAME%TYPE,
        l_effective_date DATE);

  FUNCTION GET_OR_CREATE_DEVICE(
        l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
        l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE,
        l_device_type EPORT.DEVICE_TYPE_ID%TYPE DEFAULT NULL,
        l_tries PLS_INTEGER DEFAULT 10)
     RETURN EPORT.EPORT_ID%TYPE;
END; -- Package spec
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/DATA_IN_PKG.pbk?rev=1.37
CREATE OR REPLACE PACKAGE BODY REPORT.DATA_IN_PKG IS
   FK_NOT_FOUND EXCEPTION;
   PRAGMA EXCEPTION_INIT(FK_NOT_FOUND, -2291);

    FUNCTION GET_OR_CREATE_DEVICE(
        l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
        l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE,
        l_device_type EPORT.DEVICE_TYPE_ID%TYPE DEFAULT NULL,
        l_tries PLS_INTEGER DEFAULT 10)
     RETURN EPORT.EPORT_ID%TYPE
    IS
        PRAGMA AUTONOMOUS_TRANSACTION;
        l_device_id EPORT.EPORT_ID%TYPE;
        l_use_device_type EPORT.DEVICE_TYPE_ID%TYPE;
    BEGIN
        SELECT EPORT_ID
          INTO l_device_id
          FROM EPORT
         WHERE EPORT_SERIAL_NUM = l_device_serial;
         -- NOTE: eventually the above should be source system dependent
         RETURN l_device_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            SELECT EPORT_SEQ.NEXTVAL INTO l_device_id FROM DUAL;
            IF l_device_type IS NOT NULL THEN
                l_use_device_type := l_device_type;
            ELSIF l_device_serial LIKE 'E4%' THEN
                l_use_device_type := 0; -- G4
			ELSIF l_device_serial LIKE 'EE%' THEN
                l_use_device_type := 13; -- Edge
            ELSIF l_device_serial LIKE 'G%' THEN
                l_use_device_type := 1; -- G5-G8
			ELSIF l_device_serial LIKE 'K%' THEN
                l_use_device_type := 11; -- Kiosk
            ELSIF l_device_serial LIKE 'M1%' THEN
                l_use_device_type := 6; -- MEI
            --ELSIF l_device_serial LIKE '10%' THEN -- esuds
            ELSIF l_device_serial LIKE '10%' THEN
                l_use_device_type := 3; -- Radisys Brick
            ELSE
                l_use_device_type := 10;
            END IF;
            BEGIN
                INSERT INTO EPORT(EPORT_ID, EPORT_SERIAL_NUM, ACTIVATION_DATE, DEVICE_TYPE_ID)
			  		 VALUES(l_device_id, l_device_serial, SYSDATE, l_use_device_type);
 		        COMMIT;
            EXCEPTION
                WHEN FK_NOT_FOUND THEN
                    ROLLBACK;
                    RAISE_APPLICATION_ERROR(-20889, 'Device Type ''' || l_use_device_type || ''' does not exist');
                WHEN DUP_VAL_ON_INDEX THEN
                    ROLLBACK;
                    IF l_tries > 0 THEN
                        RETURN GET_OR_CREATE_DEVICE(l_device_serial, l_source_system_cd, l_device_type, l_tries - 1);
                    ELSE
                        RAISE;
                    END IF;
                WHEN OTHERS THEN
                    ROLLBACK;
                    RAISE;
            END;
            RETURN l_device_id;
        WHEN OTHERS THEN
            RAISE;
    END;
  
    FUNCTION GET_OR_CREATE_MERCHANT(
        l_merchant_num CORP.MERCHANT.MERCHANT_NBR%TYPE,
        l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE)
     RETURN CORP.MERCHANT.MERCHANT_ID%TYPE
    IS
        PRAGMA AUTONOMOUS_TRANSACTION;
        l_merchant_id CORP.MERCHANT.MERCHANT_ID%TYPE;
    BEGIN
        SELECT MERCHANT_ID
          INTO l_merchant_id
          FROM CORP.MERCHANT
         WHERE MERCHANT_NBR = l_merchant_num;
         RETURN l_merchant_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            SELECT CORP.MERCHANT_SEQ.NEXTVAL INTO l_merchant_id FROM DUAL;
            INSERT INTO CORP.MERCHANT(MERCHANT_ID, MERCHANT_NBR, DESCRIPTION, UPD_BY)
			  		 VALUES(l_merchant_id, l_merchant_num, 'Created for source system "'||l_source_system_cd||'"', 0);
            COMMIT;
            RETURN l_merchant_id;
        WHEN OTHERS THEN
            ROLLBACK;
            RAISE;
    END;

  /* This allows external systems to add transactions. Credit, debit,
   * pass, access, or maintenance cards or refund, chargeback transaction
   * or cash should be added this way. Refunds
   * and chargebacks should always provide a valid original machine tran number.
   */
  -- R42 and above
  PROCEDURE ADD_TRANSACTION(
        l_dup_flag OUT VARCHAR,
        l_report_tran_id OUT REPORT.TRANS.TRAN_ID%TYPE,
        l_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE,
        l_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE,
        l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
        l_start_date TRANS.START_DATE%TYPE,
        l_close_date TRANS.CLOSE_DATE%TYPE,
        l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE,
        l_total_amount TRANS.TOTAL_AMOUNT%TYPE,
        l_card_number TRANS.CARD_NUMBER%TYPE,
        l_received_date TRANS.SERVER_DATE%TYPE,
        l_settle_state_id TRANS.SETTLE_STATE_ID%TYPE,
        l_settle_date TRANS.SETTLE_DATE%TYPE,
        l_preauth_amount TRANS.PREAUTH_AMOUNT%TYPE,
        l_preauth_date   TRANS.PREAUTH_DATE%TYPE,
        l_approval_cd TRANS.CC_APPR_CODE%TYPE,
        l_merchant_num CORP.MERCHANT.MERCHANT_NBR%TYPE,
        l_description TRANS.DESCRIPTION%TYPE,
        l_orig_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE DEFAULT NULL,
        l_orig_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE DEFAULT NULL,
        l_currency_code CORP.CURRENCY.CURRENCY_CODE%TYPE,
        l_device_type EPORT.DEVICE_TYPE_ID%TYPE DEFAULT NULL,
        l_card_name REPORT.CARD_TYPE.CARD_NAME%TYPE DEFAULT NULL,
        l_consumer_acct_id TRANS.CONSUMER_ACCT_ID%TYPE DEFAULT NULL)
    IS
        l_eport_id TRANS.EPORT_ID%TYPE;
        l_merchant_id TRANS.MERCHANT_ID%TYPE;
        l_orig_tran_id TRANS.ORIG_TRAN_ID%TYPE;
        l_real_amount TRANS.TOTAL_AMOUNT%TYPE;
        l_currency_id TRANS.CURRENCY_ID%TYPE;
		l_cardtype_authority_id TRANS.CARDTYPE_AUTHORITY_ID%TYPE := NULL;
		l_lock VARCHAR2(128) := GLOBALS_PKG.REQUEST_LOCK('REPORT.TRANS::' || l_machine_trans_no, 0);
		l_refund_ind REPORT.TRANS_TYPE.REFUND_IND%TYPE;
    BEGIN
        -- check for dup
        BEGIN
            SELECT TRAN_ID
              INTO l_report_tran_id
              FROM TRANS
             WHERE MACHINE_TRANS_NO = l_machine_trans_no
               AND SOURCE_SYSTEM_CD = l_source_system_cd;
            l_dup_flag:='Y';
            RETURN;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                NULL; -- CONTINUE, no dup found
            WHEN OTHERS THEN
                RAISE;
        END;
        l_dup_flag:='N';
        --get needed lookup values
        l_eport_id := GET_OR_CREATE_DEVICE(l_device_serial, l_source_system_cd, l_device_type);
        IF l_merchant_num IS NOT NULL THEN
            l_merchant_id := GET_OR_CREATE_MERCHANT(l_merchant_num, l_source_system_cd);
        END IF;
        
        IF l_orig_machine_trans_no IS NOT NULL THEN
            BEGIN
                SELECT TRAN_ID
                  INTO l_orig_tran_id
                  FROM TRANS
                 WHERE MACHINE_TRANS_NO = l_orig_machine_trans_no
                   AND SOURCE_SYSTEM_CD = NVL(l_orig_source_system_cd, l_source_system_cd);
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    RAISE_APPLICATION_ERROR(-20888, 'Could not find the original transaction with MACHINE_TRANS_NUM = "'||l_orig_machine_trans_no||'" from the source system "'||l_source_system_cd||'". Transaction was NOT added.');
                WHEN OTHERS THEN
                    RAISE;
            END;
        END IF;
        
        --get currency id from currency code
        IF l_currency_code IS NOT NULL THEN
        	BEGIN
        		SELECT currency_id
        		  INTO l_currency_id
        		  FROM CORP.currency
        		 WHERE currency_code = l_currency_code;
        	EXCEPTION
        		WHEN NO_DATA_FOUND THEN
                    RAISE_APPLICATION_ERROR(-20888, 'No currency is mapped in the'
						|| ' system for currency code ''' || l_currency_code
						|| '''. A transaction can not be added with no currency'
						|| ' identified. Transaction was NOT added.');
                WHEN OTHERS THEN
                    RAISE;
        	END;
        ELSE
        	RAISE_APPLICATION_ERROR(-20880, 'A transaction can not be entered '
				|| 'without a currency identified. Transaction was NOT added.');
        END IF;
		
		IF l_card_name IS NOT NULL THEN
			SELECT MAX(CA.CARDTYPE_AUTHORITY_ID)
			INTO l_cardtype_authority_id
			FROM REPORT.CARDTYPE_AUTHORITY CA
			JOIN REPORT.CARD_TYPE CT ON CA.CARDTYPE_ID = CT.CARD_TYPE_ID			
			WHERE CT.CARD_NAME = l_card_name;
			
			IF l_cardtype_authority_id IS NULL THEN
				SELECT MAX(CA.CARDTYPE_AUTHORITY_ID)
				INTO l_cardtype_authority_id
				FROM REPORT.CARDTYPE_AUTHORITY CA
				JOIN REPORT.CARD_TYPE CT ON CA.CARDTYPE_ID = CT.CARD_TYPE_ID			
				JOIN REPORT.TRANS_TYPE TT ON CT.CARD_NAME = TT.TRANS_TYPE_NAME
				WHERE TT.TRANS_TYPE_ID = l_trans_type_id;
			END IF;
		END IF;
		
		SELECT REFUND_IND
		INTO l_refund_ind
		FROM REPORT.TRANS_TYPE
		WHERE TRANS_TYPE_ID = l_trans_type_id;
        
        IF l_refund_ind = 'Y' THEN -- make it negative
            l_real_amount := -ABS(l_total_amount);
        ELSE
            l_real_amount := ABS(l_total_amount);
        END IF;
        SELECT TRANS_SEQ.NEXTVAL
          INTO l_report_tran_id
          FROM DUAL;
        INSERT INTO TRANS(
            TRAN_ID,
            MACHINE_TRANS_NO,
            SOURCE_SYSTEM_CD,
            EPORT_ID,
			CARDTYPE_AUTHORITY_ID,
            START_DATE,
            CLOSE_DATE,
            TRANS_TYPE_ID,
            TOTAL_AMOUNT,
            CARD_NUMBER,
            SERVER_DATE,
            SETTLE_STATE_ID,
            SETTLE_DATE,
            PREAUTH_AMOUNT,
            PREAUTH_DATE,
            CC_APPR_CODE,
            MERCHANT_ID,
            DESCRIPTION,
            ORIG_TRAN_ID,
            CURRENCY_ID,
            CONSUMER_ACCT_ID)
          SELECT
            l_report_tran_id,
            l_machine_trans_no,
            l_source_system_cd,
            l_eport_id,
			l_cardtype_authority_id,
            l_start_date,
            l_close_date,
            l_trans_type_id,
            l_real_amount,
            l_card_number,
            l_received_date,
            l_settle_state_id,
            l_settle_date,
            l_preauth_amount,
            l_preauth_date,
            l_approval_cd,
            l_merchant_id,
            l_description,
            l_orig_tran_id,
            l_currency_id,
            l_consumer_acct_id
          FROM DUAL;
    END;
    
    -- R37 to R41
    PROCEDURE ADD_TRANSACTION(
        l_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE,
        l_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE,
        l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
        l_start_date TRANS.START_DATE%TYPE,
        l_close_date TRANS.CLOSE_DATE%TYPE,
        l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE,
        l_total_amount TRANS.TOTAL_AMOUNT%TYPE,
        l_card_number TRANS.CARD_NUMBER%TYPE,
        l_received_date TRANS.SERVER_DATE%TYPE,
        l_settle_state_id TRANS.SETTLE_STATE_ID%TYPE,
        l_settle_date TRANS.SETTLE_DATE%TYPE,
        l_preauth_amount TRANS.PREAUTH_AMOUNT%TYPE,
        l_preauth_date   TRANS.PREAUTH_DATE%TYPE,
        l_approval_cd TRANS.CC_APPR_CODE%TYPE,
        l_merchant_num CORP.MERCHANT.MERCHANT_NBR%TYPE,
        l_description TRANS.DESCRIPTION%TYPE,
        l_orig_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE DEFAULT NULL,
        l_orig_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE DEFAULT NULL,
        l_currency_code CORP.CURRENCY.CURRENCY_CODE%TYPE,
        l_device_type EPORT.DEVICE_TYPE_ID%TYPE DEFAULT NULL,
        l_card_name REPORT.CARD_TYPE.CARD_NAME%TYPE DEFAULT NULL,
        l_consumer_acct_id TRANS.CONSUMER_ACCT_ID%TYPE DEFAULT NULL)
    IS
      l_report_tran_id TRANS.TRAN_ID%TYPE;
      l_dup_flag VARCHAR(1);
    BEGIN
        ADD_TRANSACTION(
            l_dup_flag,
            l_report_tran_id,
            l_machine_trans_no,
            l_source_system_cd,
            l_device_serial,
            l_start_date,
            l_close_date,
            l_trans_type_id,
            l_total_amount,
            l_card_number,
            l_received_date,
            l_settle_state_id,
            l_settle_date,
            l_preauth_amount,
            l_preauth_date,
            l_approval_cd,
            l_merchant_num,
            l_description,
            l_orig_machine_trans_no,
            l_orig_source_system_cd,
            l_currency_code,
            l_device_type,
            l_card_name,
            l_consumer_acct_id);
        IF l_dup_flag = 'Y' THEN
          RAISE_APPLICATION_ERROR(-20880, 'A transaction with MACHINE_TRANS_NUM = "'
                ||l_machine_trans_no||'" from the source system "'
                ||l_source_system_cd||'" already exists. It was received '
                ||'l_report_tran_id='||l_report_tran_id
                ||'. Transaction was NOT added.');
        END IF;
    END;
    
    PROCEDURE ADD_TRAN_ITEM(
        l_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE,
        l_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE,
        l_column_num PURCHASE.MDB_NUMBER%TYPE,
        l_column_label PURCHASE.VEND_COLUMN%TYPE,
        l_quantity PURCHASE.AMOUNT%TYPE DEFAULT 1,
        l_price PURCHASE.PRICE%TYPE DEFAULT NULL,
        l_product_desc PURCHASE.DESCRIPTION%TYPE DEFAULT NULL,
        l_tran_line_item_type_id PURCHASE.TRAN_LINE_ITEM_TYPE_ID%TYPE DEFAULT NULL,
        l_apply_to_consumer_acct_id PURCHASE.APPLY_TO_CONSUMER_ACCT_ID%TYPE DEFAULT NULL)
    IS
    BEGIN
        --FOR i IN 1..l_quantity LOOP
            INSERT INTO PURCHASE(
                PURCHASE_ID,
                TRAN_ID,
                TRAN_DATE,
                AMOUNT,
                PRICE,
                VEND_COLUMN,
                DESCRIPTION,
                MDB_NUMBER,
                TRAN_LINE_ITEM_TYPE_ID,
                APPLY_TO_CONSUMER_ACCT_ID)
              SELECT
                PURCHASE_SEQ.NEXTVAL,
                TRAN_ID,
                CLOSE_DATE,
                l_quantity,
                l_price,
                l_column_label,
                l_product_desc,
                l_column_num,
                l_tran_line_item_type_id,
                l_apply_to_consumer_acct_id
              FROM TRANS
             WHERE MACHINE_TRANS_NO = l_machine_trans_no
               AND SOURCE_SYSTEM_CD = l_source_system_cd;
            IF SQL%ROWCOUNT = 0 THEN
                RAISE_APPLICATION_ERROR(-20888, 'Could not find a transaction with MACHINE_TRANS_NUM = "'||l_machine_trans_no||'" from the source system "'||l_source_system_cd||'". Item was NOT added.');
            END IF;
--        END LOOP;
    END;
    
    /* This procedure allows external systems to update the settlement info
     * of a transaction.
     *
     */
    PROCEDURE UPDATE_SETTLE_INFO(
        l_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE,
        l_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE,
        l_settle_state_id TRANS.SETTLE_STATE_ID%TYPE,
        l_settle_date TRANS.SETTLE_DATE%TYPE,
        l_approval_cd TRANS.CC_APPR_CODE%TYPE,
        l_merchant_num CORP.MERCHANT.MERCHANT_NBR%TYPE,
        ln_amount REPORT.TRANS.TOTAL_AMOUNT%TYPE DEFAULT NULL,
        pn_override_trans_type_id REPORT.TRANS.TRANS_TYPE_ID%TYPE DEFAULT NULL)
    IS
        l_merchant_id TRANS.MERCHANT_ID%TYPE;
        ln_tran_id REPORT.TRANS.TRAN_ID%TYPE;
        lc_imported PSS.SALE.IMPORTED%TYPE;
        ln_old_amount REPORT.TRANS.TOTAL_AMOUNT%TYPE;
        ln_old_trans_type_id REPORT.TRANS.TRANS_TYPE_ID%TYPE;
    BEGIN
        IF l_merchant_num IS NOT NULL THEN
            l_merchant_id := GET_OR_CREATE_MERCHANT(l_merchant_num, l_source_system_cd);
        END IF;
        UPDATE TRANS T SET (SETTLE_STATE_ID, SETTLE_DATE, CC_APPR_CODE, MERCHANT_ID) =
            (SELECT NVL(l_settle_state_id, T.SETTLE_STATE_ID),
                    l_settle_date,
                    NVL(l_approval_cd, T.CC_APPR_CODE),
                    NVL(l_merchant_id, T.MERCHANT_ID)
              FROM DUAL)
          WHERE T.MACHINE_TRANS_NO = l_machine_trans_no
            AND T.SOURCE_SYSTEM_CD = l_source_system_cd
            RETURNING TRAN_ID, TOTAL_AMOUNT, TRANS_TYPE_ID
            INTO ln_tran_id, ln_old_amount, ln_old_trans_type_id;
        IF SQL%ROWCOUNT = 0 THEN
            IF l_settle_state_id = 2 AND ln_amount = 0 THEN
                SELECT NVL(MAX(IMPORTED), '-')
                  INTO lc_imported
                  FROM PSS.TRAN X
                  JOIN PSS.SALE S ON X.TRAN_ID = S.TRAN_ID
                 WHERE X.TRAN_GLOBAL_TRANS_CD = l_machine_trans_no;
                IF lc_imported = '-' THEN
                    RETURN; -- sale was cancelled before it was imported
                END IF;
            END IF;
            RAISE_APPLICATION_ERROR(-20888, 'Could not find a transaction with MACHINE_TRANS_NUM = "'||l_machine_trans_no||'" from the source system "'||l_source_system_cd||'". Settle info was NOT updated.');
        END IF;
        IF (ln_amount IS NOT NULL AND ln_amount != ln_old_amount) OR (pn_override_trans_type_id IS NOT NULL AND pn_override_trans_type_id != ln_old_trans_type_id) THEN
            UPDATE REPORT.TRANS T
               SET TOTAL_AMOUNT = NVL(ln_amount, T.TOTAL_AMOUNT),
                   TRANS_TYPE_ID = NVL(pn_override_trans_type_id, T.TRANS_TYPE_ID)
             WHERE TRAN_ID = ln_tran_id;
        END IF;
        REPORT.SYNC_PKG.RECEIVE_TRANS_SETTLEMENT(ln_tran_id, l_settle_state_id, l_settle_date, l_approval_cd);
    END;
    
    /* This procedure allows external systems to update the device info
     * of their devices
     */
    PROCEDURE UPDATE_DEVICE_INFO(
        l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
        l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE,
        l_device_type_id EPORT.DEVICE_TYPE_ID%TYPE/*,
        l_device_name EPORT.DEVICE_NAME%TYPE*/)
    IS
        l_device_id EPORT.EPORT_ID%TYPE;
    BEGIN
        l_device_id := GET_OR_CREATE_DEVICE(l_device_serial, l_source_system_cd);
        UPDATE EPORT E SET DEVICE_TYPE_ID = l_device_type_id
         WHERE E.EPORT_ID = l_device_id;
    END;
    
    PROCEDURE ADD_FILL(
       l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
       l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE,
       l_fill_date IN FILL.FILL_DATE%TYPE)
    IS
       l_prev_fill_date FILL.FILL_DATE%TYPE;
       l_eport_id FILL.EPORT_ID%TYPE;
    BEGIN
        l_eport_id := GET_OR_CREATE_DEVICE(l_device_serial, l_source_system_cd);
  	    INSERT INTO FILL (FILL_ID, EPORT_ID, FILL_DATE)
 	         VALUES(FILL_SEQ.NEXTVAL, l_eport_id, l_fill_date);
    END;
    /* need to change alerts to relate to devices not terminals
    PROCEDURE ADD_ALERT(
       l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
       l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE,
       l_alert_id ALERT.ALERT_ID%TYPE,
       l_alert_date TERMINAL_ALERT.ALERT_DATE%TYPE,
       l_details TERMINAL_ALERT.DETAILS%TYPE)
    IS
       l_eport_id FILL.EPORT_ID%TYPE;
    BEGIN
        l_eport_id := GET_OR_CREATE_DEVICE(l_device_serial, l_source_system_cd);
    	 SELECT TERMINAL_ALERT_SEQ.NEXTVAL INTO l_id FROM DUAL;
    	 INSERT INTO TERMINAL_ALERT (TERMINAL_ALERT_ID, ALERT_ID, TERMINAL_ID, ALERT_DATE, DETAILS, RESPONSE_SENT )
     		VALUES(l_id,l_alert_id,l_terminal_id,l_alert_date,l_details, l_sent);
    END;
    */
    /*
    Right now Legacy G4 and BEX do not have any additional refund information (PROBLEM_DATE is the TRAN_DATE, REFUND_STATUS is always 1)
    */
    
    PROCEDURE UPDATE_POS_INFO(
        l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
        l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE,
        l_location_name LOCATION.LOCATION_NAME%TYPE,
        l_customer_name CUSTOMER.CUSTOMER_NAME%TYPE,
        l_effective_date TERMINAL_EPORT.START_DATE%TYPE)
    IS
        l_device_id EPORT.EPORT_ID%TYPE;
    BEGIN
        l_device_id := GET_OR_CREATE_DEVICE(l_device_serial, l_source_system_cd);
        /*UPDATE EPORT E SET DEVICE_TYPE_ID = l_device_type_id
         WHERE E.EPORT_SERIAL_NUM = l_device_serial;*/
    END;

    /* Creates a customer, location and terminal, if necessary for the given device
     * This should only be used by BEX machines because they are currently not
     * configured in the Customer Reporting System.
     */
    PROCEDURE UPDATE_BEX_LOCATION(
        l_serial_num EPORT.EPORT_SERIAL_NUM%TYPE,
        l_location_name LOCATION.LOCATION_NAME%TYPE,
        l_customer_abbr CORP.CUSTOMER.CUSTOMER_ALT_NAME%TYPE,
        l_effective_date DATE)
    IS
        l_customer_id CORP.CUSTOMER.CUSTOMER_ID%TYPE;
        l_location_id REPORT.LOCATION.LOCATION_ID%TYPE;
        l_terminal_id REPORT.TERMINAL.TERMINAL_ID%TYPE;
        l_eport_id REPORT.EPORT.EPORT_ID%TYPE;
        l_terminal_nbr REPORT.TERMINAL.TERMINAL_NBR%TYPE;
        l_te_id REPORT.TERMINAL_EPORT.TERMINAL_EPORT_ID%TYPE;
        l_old_start_date REPORT.TERMINAL_EPORT.START_DATE%TYPE;
    BEGIN
        SELECT EPORT_ID
          INTO l_eport_id
          FROM EPORT
         WHERE EPORT_SERIAL_NUM = l_serial_num;

        SELECT MAX(CUSTOMER_ID)
          INTO l_customer_id
          FROM CORP.CUSTOMER
         WHERE CUSTOMER_ALT_NAME = l_customer_abbr;

        IF l_customer_id IS NULL THEN
            SELECT CORP.CUSTOMER_SEQ.NEXTVAL
              INTO l_customer_id
              FROM DUAL;

            INSERT INTO CORP.CUSTOMER(CUSTOMER_ID, CUSTOMER_NAME, CUSTOMER_ALT_NAME, STATUS, CREATE_BY)
	           VALUES(l_customer_id, l_customer_abbr, l_customer_abbr, 'A', 0);	
        END IF;

        SELECT MAX(LOCATION_ID)
          INTO l_location_id
          FROM LOCATION
         WHERE LOCATION_NAME = l_location_name
           AND EPORT_ID = l_eport_id;

        IF l_location_id IS NULL THEN
		  	 SELECT LOCATION_SEQ.NEXTVAL INTO l_location_id FROM DUAL;
	  		 INSERT INTO LOCATION(LOCATION_ID, TERMINAL_ID, LOCATION_NAME, CREATE_BY, STATUS, EPORT_ID)
	  		     VALUES(l_location_id, 0, l_location_name, 0, 'A', l_eport_id);
        END IF;

        SELECT MAX(TE.TERMINAL_ID)
          INTO l_terminal_id
          FROM TERMINAL_EPORT TE, TERMINAL T
         WHERE TE.TERMINAL_ID = T.TERMINAL_ID
           AND TE.EPORT_ID = l_eport_id
           AND T.CUSTOMER_ID = l_customer_id
           AND T.LOCATION_ID = l_location_id;

        IF l_terminal_id IS NULL THEN
            --customer_id or location_id changed, or its new
            SELECT MAX(T.TERMINAL_NBR)
              INTO l_terminal_nbr
              FROM TERMINAL_EPORT TE, TERMINAL T
             WHERE TE.TERMINAL_ID = T.TERMINAL_ID
               AND TE.EPORT_ID = l_eport_id;
            IF l_terminal_nbr IS NULL THEN
                l_terminal_nbr := l_serial_num;
            ELSIF l_terminal_nbr = l_serial_num THEN
                l_terminal_nbr := l_serial_num || '-1';
            ELSIF l_terminal_nbr LIKE l_serial_num || '-%' THEN
                l_terminal_nbr := l_serial_num || '-' || TO_CHAR(TO_NUMBER(SUBSTR(l_terminal_nbr, INSTR(l_terminal_nbr, '-', -1) + 1)) + 1);
            ELSE
                l_terminal_nbr := l_serial_num;
            END IF;
            SELECT TERMINAL_SEQ.NEXTVAL INTO l_terminal_id FROM DUAL;
            INSERT INTO TERMINAL(TERMINAL_ID, TERMINAL_NBR, TERMINAL_NAME, EPORT_ID, CUSTOMER_ID, LOCATION_ID, BUSINESS_UNIT_ID, PAYMENT_SCHEDULE_ID)
                VALUES(l_terminal_id, l_terminal_nbr, l_serial_num, l_eport_id, l_customer_id, l_location_id, 2, 4);
            TERMINAL_EPORT_UPD(l_terminal_id, l_eport_id, l_effective_date, NULL, NULL);
        ELSE
            -- ensure terminal_eport start date is early enough
            BEGIN
                SELECT TERMINAL_EPORT_ID, START_DATE
                  INTO l_te_id, l_old_start_date
                  FROM (SELECT TERMINAL_EPORT_ID, START_DATE
                          FROM TERMINAL_EPORT
                         WHERE TERMINAL_ID = l_terminal_id
                           AND EPORT_ID = l_eport_id
                           AND NVL(END_DATE, MAX_DATE) > l_effective_date
                           ORDER BY START_DATE ASC)
                 WHERE ROWNUM = 1;
                IF l_old_start_date > l_effective_date THEN -- we need to adjust
                    TERMINAL_EPORT_UPD(l_terminal_id, l_eport_id, l_effective_date, NULL, l_te_id);
                END IF;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN -- Need to add new entry
                    TERMINAL_EPORT_UPD(l_terminal_id, l_eport_id, l_effective_date, NULL, NULL);
                WHEN OTHERS THEN
                    RAISE;
            END;
        END IF;
    END;
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/CORP/PAYMENTS_PKG.psk?rev=1.21
CREATE OR REPLACE PACKAGE CORP.PAYMENTS_PKG IS
--
-- Holds all the procedures and functions related to EFTs and payments
--
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------
-- B Krug       09-16-04  NEW

   /*PROCEDURE AUTO_CREATE_EFTS;
   PROCEDURE CREATE_EFT(
        l_cust_bank_id CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        l_user_id IN EFT.CREATE_BY%TYPE,
        l_check_min CHAR DEFAULT 'N');

   PROCEDURE REFRESH_PENDING_REVENUE;
   */
   CHILD_RECORD_FOUND EXCEPTION;
   PRAGMA EXCEPTION_INIT(CHILD_RECORD_FOUND, -2292);

   TRANS_BATCH_CLOSED EXCEPTION;
   PRAGMA EXCEPTION_INIT(TRANS_BATCH_CLOSED, -20701);

   FUNCTION GET_UNPAID_TRANS_AND_FEES (
        l_as_of DATE
    ) RETURN GLOBALS_PKG.REF_CURSOR;
   /*
   FUNCTION FORMAT_EFT_REASON(
        l_eft_id EFT.EFT_ID%TYPE
     ) RETURN VARCHAR;
     */
   FUNCTION FORMAT_EFT_REASON(
        l_payment_sched_id BATCH.PAYMENT_SCHEDULE_ID%TYPE,
        l_min_pay_date BATCH.START_DATE%TYPE,
        l_max_pay_date BATCH.END_DATE%TYPE
     ) RETURN VARCHAR;
     
    FUNCTION BATCH_CLOSABLE(
        l_batch_id BATCH.BATCH_ID%TYPE)
     RETURN CHAR
     PARALLEL_ENABLE;
     
    FUNCTION BATCH_CLOSABLE(
        l_batch_id BATCH.BATCH_ID%TYPE,
        l_pay_sched BATCH.PAYMENT_SCHEDULE_ID%TYPE,
        l_end_date BATCH.END_DATE%TYPE,
        l_batch_state_cd BATCH.BATCH_STATE_CD%TYPE)
     RETURN CHAR
     PARALLEL_ENABLE;

   /*
   FUNCTION GET_NOT_PAYABLE_COUNT(
        l_terminal_id TERMINAL.TERMINAL_ID%TYPE,
        l_cust_bank_id CUSTOMER_BANK.CUSTOMER_BANK_id%TYPE,
        l_close_date LEDGER.CLOSE_DATE%TYPE)
     RETURN NUMBER
     PARALLEL_ENABLE;
*/

  PROCEDURE UPDATE_LEDGER(
        l_trans_id    LEDGER.TRANS_ID%TYPE,
        l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE,
        l_close_date    LEDGER.ENTRY_DATE%TYPE,
        l_settle_date LEDGER.LEDGER_DATE%TYPE,
        l_total_amount LEDGER.AMOUNT%TYPE,
        l_settle_state_id LEDGER.SETTLE_STATE_ID%TYPE,
        l_terminal_id   BATCH.TERMINAL_ID%TYPE,
        l_customer_bank_id  DOC.CUSTOMER_BANK_ID%TYPE,
        l_process_fee_id    LEDGER.PROCESS_FEE_ID%TYPE,
        l_currency_id       DOC.CURRENCY_ID%TYPE);

  PROCEDURE LOCK_DOC(
        l_doc_id DOC.DOC_ID%TYPE,
        l_user_id IN DOC.CREATE_BY%TYPE);

  FUNCTION GET_OR_CREATE_BATCH(
        l_terminal_id  BATCH.TERMINAL_ID%TYPE,
        l_cust_bank_id DOC.CUSTOMER_BANK_ID%TYPE,
        l_entry_date LEDGER.ENTRY_DATE%TYPE,
        l_currency_id DOC.CURRENCY_ID%TYPE,
        l_always_as_accum CHAR
    )
     RETURN BATCH.BATCH_ID%TYPE;
     
  PROCEDURE      DELAY_REFUND_BY_TRAN_ID (
      l_trans_id IN LEDGER.TRANS_ID%TYPE);
      
  PROCEDURE      DELAY_ENTRY (
      l_ledger_id IN LEDGER.LEDGER_ID%TYPE);
      
  PROCEDURE      DELETE_ENTRY (
      l_ledger_id IN LEDGER.LEDGER_ID%TYPE);
      
  PROCEDURE ADJUSTMENT_INS (
        l_ledger_id OUT NOCOPY LEDGER.LEDGER_ID%TYPE,
       	l_doc_id IN BATCH.DOC_ID%TYPE,
        l_terminal_id IN BATCH.TERMINAL_ID%TYPE,
    	l_reason IN LEDGER.DESCRIPTION%TYPE,
    	l_amt IN LEDGER.AMOUNT%TYPE,
    	l_user_id IN LEDGER.CREATE_BY%TYPE,
		lc_split_payment_flag IN CHAR,
		lc_split_payment_interval_cd IN CHAR,
		ln_split_number_of_payments IN INTEGER,
		ld_first_split_payment_date IN DATE
	);
	  
  PROCEDURE ADJUSTMENT_INS (
        l_ledger_id OUT NOCOPY LEDGER.LEDGER_ID%TYPE,
       	l_doc_id IN BATCH.DOC_ID%TYPE,
        l_terminal_id IN BATCH.TERMINAL_ID%TYPE,
    	l_reason IN LEDGER.DESCRIPTION%TYPE,
    	l_amt IN LEDGER.AMOUNT%TYPE,
    	l_user_id IN LEDGER.CREATE_BY%TYPE
	);
		
PROCEDURE ADJUSTMENT_INS (
	pn_customer_id CORP.CUSTOMER.CUSTOMER_ID%TYPE,
	pv_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
	pv_currency_cd CORP.CURRENCY.CURRENCY_CODE%TYPE,
	pv_reason CORP.LEDGER.DESCRIPTION%TYPE,
	pn_amount CORP.LEDGER.AMOUNT%TYPE,
	pn_doc_id OUT CORP.DOC.DOC_ID%TYPE,
	pn_ledger_id OUT CORP.LEDGER.LEDGER_ID%TYPE
);
    	
  PROCEDURE                             ADJUSTMENT_UPD
       (l_ledger_id IN LEDGER.LEDGER_ID%TYPE,
       	l_reason IN LEDGER.DESCRIPTION%TYPE,
       	l_amt IN LEDGER.AMOUNT%TYPE,
    	l_user_id IN LEDGER.CREATE_BY%TYPE);
    	
  PROCEDURE      APPROVE_PAYMENT (
      l_doc_id IN DOC.DOC_ID%TYPE,
      l_user_id IN DOC.APPROVE_BY%TYPE);
      
  PROCEDURE      MARK_DOC_PAID (
      l_doc_id IN DOC.DOC_ID%TYPE,
      l_user_id IN DOC.APPROVE_BY%TYPE);
      
  PROCEDURE PROCESS_FEES_UPD
       (l_terminal_id IN PROCESS_FEES.TERMINAL_ID%TYPE,
        l_trans_type_id IN PROCESS_FEES.TRANS_TYPE_ID%TYPE,
        l_fee_percent IN PROCESS_FEES.FEE_PERCENT%TYPE,
        l_fee_amount IN PROCESS_FEES.FEE_AMOUNT%TYPE,
        l_fee_minimum IN PROCESS_FEES.MIN_AMOUNT%TYPE,
        l_effective_date IN PROCESS_FEES.END_DATE%TYPE DEFAULT SYSDATE,
        l_override IN CHAR DEFAULT 'N');
        
  PROCEDURE SCAN_FOR_SERVICE_FEES;
  
  PROCEDURE SERVICE_FEES_UPD(
        l_terminal_id IN SERVICE_FEES.TERMINAL_ID%TYPE,
        l_fee_id IN SERVICE_FEES.FEE_ID%TYPE,
        l_freq_id IN SERVICE_FEES.FREQUENCY_ID%TYPE,
        l_fee_amt IN SERVICE_FEES.FEE_AMOUNT%TYPE,
        l_fee_perc IN SERVICE_FEES.FEE_PERCENT%TYPE,
        l_effective_date IN SERVICE_FEES.END_DATE%TYPE DEFAULT SYSDATE,
        l_end_date IN SERVICE_FEES.END_DATE%TYPE,
        l_override IN CHAR DEFAULT 'N',
        pn_grace_days IN NUMBER DEFAULT 60,
		pc_no_trigger_event_flag IN SERVICE_FEES.NO_TRIGGER_EVENT_FLAG%TYPE DEFAULT NULL);
        
  PROCEDURE UNLOCK_DOC (
      l_doc_id IN DOC.DOC_ID%TYPE);
      
  FUNCTION GET_NEW_BATCH(
        l_batch_id BATCH.BATCH_ID%TYPE,
        l_entry_date LEDGER.ENTRY_DATE%TYPE)
     RETURN BATCH.BATCH_ID%TYPE;
     
  FUNCTION ENTRY_PAYABLE(
        l_settle_state LEDGER.SETTLE_STATE_ID%TYPE,
        l_entry_type LEDGER.ENTRY_TYPE%TYPE)
     RETURN CHAR
     DETERMINISTIC
     PARALLEL_ENABLE;

  PROCEDURE UPDATE_FILL_BATCH(
        l_fill_id   FILL.FILL_ID%TYPE);
        
  PROCEDURE CHECK_TRANS_CLOSED(
        l_trans_id    CORP.LEDGER.TRANS_ID%TYPE)
  PARALLEL_ENABLE;

  PROCEDURE UPDATE_SETTLEMENT(
        l_trans_id    CORP.LEDGER.TRANS_ID%TYPE,
        l_settle_state_id CORP.LEDGER.SETTLE_STATE_ID%TYPE,
        l_settle_date   CORP.LEDGER.LEDGER_DATE%TYPE);
        
  PROCEDURE CHECK_TRANS_WITH_PF_CLOSED(
        l_process_fee_id    CORP.LEDGER.PROCESS_FEE_ID%TYPE)
  PARALLEL_ENABLE;
        
  PROCEDURE UPDATE_PROCESS_FEE_VALUES(
        l_process_fee_id CORP.LEDGER.PROCESS_FEE_ID%TYPE);
        
  PROCEDURE      UNAPPROVE_PAYMENT (
      l_doc_id IN DOC.DOC_ID%TYPE);
      
  FUNCTION GET_SERVICE_FEE_DATE_FMT(
        l_months FREQUENCY.MONTHS%TYPE,
        l_days FREQUENCY.DAYS%TYPE)
     RETURN VARCHAR
     DETERMINISTIC
     PARALLEL_ENABLE;
     
  FUNCTION GET_TRANS_ADJ_DESC(
        l_trans_id LEDGER.TRANS_ID%TYPE,
        l_entry_type LEDGER.ENTRY_TYPE%TYPE,
        l_entry_date LEDGER.ENTRY_DATE%TYPE,
        l_terminal_id BATCH.TERMINAL_ID%TYPE,
        l_customer_bank_id DOC.CUSTOMER_BANK_ID%TYPE)
     RETURN LEDGER.DESCRIPTION%TYPE
     PARALLEL_ENABLE;
     
  FUNCTION GET_OR_CREATE_DOC(
        l_cust_bank_id DOC.CUSTOMER_BANK_ID%TYPE,
        l_currency_id DOC.CURRENCY_ID%TYPE,
        l_business_unit DOC.BUSINESS_UNIT_ID%TYPE
    )
     RETURN DOC.DOC_ID%TYPE;
     
  PROCEDURE UPDATE_LEDGER(
              l_tran_id LEDGER.TRANS_ID%TYPE);
              
  PROCEDURE ADD_ACTIVATION_FEE(
        l_terminal_id CORP.BATCH.TERMINAL_ID%TYPE,
        l_fee_amt CORP.LEDGER.AMOUNT%TYPE,
        l_activation_date  CORP.LEDGER.ENTRY_DATE%TYPE);
        
  PROCEDURE UPDATE_EXPORT_BATCH(
        l_export_id   REPORT.EXPORT_BATCH.BATCH_ID%TYPE);
        
  PROCEDURE SWITCH_PAYMENT_SCHEDULE(
        l_tran_id CORP.LEDGER.TRANS_ID%TYPE,
        l_payment_schedule_id  CORP.BATCH.PAYMENT_SCHEDULE_ID%TYPE);
        
  PROCEDURE CHECK_FILL_BATCH_COMPLETE(
        l_batch_id BATCH.BATCH_ID%TYPE);

  PROCEDURE START_EFT_PROPAGATION;		
		
  PROCEDURE START_EFT_PROCESSING(
	PN_DOC_ID CORP.DOC.DOC_ID%TYPE);
		
  PROCEDURE PROCESS_EFT(
	PN_DOC_ID CORP.DOC.DOC_ID%TYPE);
	
  PROCEDURE COMPLETE_EFT_PROPAGATION;
  
  PROCEDURE SPLIT_DOC(
	l_doc_id CORP.DOC.DOC_ID%TYPE);
END; -- Package spec

/
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/CORP/PAYMENTS_PKG.pbk?rev=1.117
CREATE OR REPLACE PACKAGE BODY CORP.PAYMENTS_PKG IS
--
-- Holds all the procedures and functions related to EFTs and payments
--
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------
-- B Krug       09-16-04  NEW
-- B Krug       09-16-04  Moved CREATE_PAYMENT_FOR_ACCOUNT into this package to
--                        take advantage of other procs in this package
-- B Krug       10-05-04  Added Ledger sync procs (and batch-related stuff)
-- B Krug       01-31-08  Added Batch Confirmation Logic

    -- Returns 'Y' if an entry is payable (it's been settled)
    -- Returns 'N' if an entry is not payable
    -- Returns '?' if an entry has not been processed
    FUNCTION ENTRY_PAYABLE(
        l_settle_state LEDGER.SETTLE_STATE_ID%TYPE,
        l_entry_type LEDGER.ENTRY_TYPE%TYPE)
     RETURN CHAR
    IS
    BEGIN
        IF l_settle_state IN(2,3,6) THEN
            RETURN 'Y';
        /*ELSIF l_entry_type IN('CB','RF','SF') THEN
            RETURN 'Y';
        */ELSIF l_settle_state IN(5) THEN
            RETURN 'N';
        ELSE
            RETURN '?';
        END IF;
    END;
    
    -- Gets the batch_id or creates one if necessary
    FUNCTION GET_OR_CREATE_DOC(
        l_cust_bank_id DOC.CUSTOMER_BANK_ID%TYPE,
        l_currency_id DOC.CURRENCY_ID%TYPE,
        l_business_unit DOC.BUSINESS_UNIT_ID%TYPE
     )
     RETURN DOC.DOC_ID%TYPE
    IS
        l_doc_id DOC.DOC_ID%TYPE;
        l_batch_ref DOC.REF_NBR%TYPE;
        l_lock VARCHAR2(128);
    BEGIN
        -- lock this bank id until commit to ensure the the doc stays open until the ledger entry is set with it
        l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_cust_bank_id);
        SELECT MAX(D.DOC_ID) -- just to be safe in case there is more than one
          INTO l_doc_id
          FROM DOC D
         WHERE D.CUSTOMER_BANK_ID = l_cust_bank_id
           AND NVL(D.CURRENCY_ID, 0) = NVL(l_currency_id, 0)
           AND NVL(D.BUSINESS_UNIT_ID, 0) = NVL(l_business_unit, 0)
           AND D.STATUS = 'O';
        IF l_doc_id IS NULL THEN
            -- create new doc record
            SELECT EFT_SEQ.NEXTVAL, TO_CHAR(EFT_BATCH_SEQ.NEXTVAL, 'FM0000999999')
              INTO l_doc_id, l_batch_ref
              FROM DUAL;
            INSERT INTO DOC(
                DOC_ID,
                DOC_TYPE,
                REF_NBR,
                DESCRIPTION,
                CUSTOMER_BANK_ID,
                CURRENCY_ID,
                BUSINESS_UNIT_ID,
                BANK_ACCT_NBR,
                BANK_ROUTING_NBR,
                STATUS)
              SELECT
                l_doc_id,
                '--',
                l_batch_ref,
                NVL(EFT_PREFIX, 'USAT: ') || l_batch_ref,
                CUSTOMER_BANK_ID,
                DECODE(l_currency_id, 0, NULL, l_currency_id),
                DECODE(l_business_unit, 0, NULL, l_business_unit),
                BANK_ACCT_NBR,
                BANK_ROUTING_NBR,
                'O'
              FROM CUSTOMER_BANK
              WHERE CUSTOMER_BANK_ID = l_cust_bank_id;
        END IF;
        RETURN l_doc_id;
    END;

    -- Gets the batch_id or creates one if necessary
    FUNCTION GET_OR_CREATE_BATCH(
        l_terminal_id  BATCH.TERMINAL_ID%TYPE,
        l_cust_bank_id DOC.CUSTOMER_BANK_ID%TYPE,
        l_entry_date LEDGER.ENTRY_DATE%TYPE,
        l_currency_id DOC.CURRENCY_ID%TYPE,
        l_pay_sched TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
        l_business_unit CORP.BUSINESS_UNIT.BUSINESS_UNIT_ID%TYPE,
        l_always_as_accum CHAR
    )
     RETURN BATCH.BATCH_ID%TYPE
    IS
        l_batch_id BATCH.BATCH_ID%TYPE;
        l_doc_id DOC.DOC_ID%TYPE;
        l_start_date BATCH.START_DATE%TYPE;
        l_end_date BATCH.END_DATE%TYPE;
        l_batch_state_cd BATCH.BATCH_STATE_CD%TYPE;
        l_start_fill_id BATCH_FILL.START_FILL_ID%TYPE;
        l_end_fill_id BATCH_FILL.END_FILL_ID%TYPE;
        l_counters_must_show CHAR(1);
        l_lock VARCHAR2(128);
    BEGIN
        -- calculate batch (and create if necessary)
        -- Get an "Open" doc record
        l_doc_id := GET_OR_CREATE_DOC(l_cust_bank_id, l_currency_id, l_business_unit);
        --l_lock := GLOBALS_PKG.REQUEST_LOCK('BATCH.TERMINAL_ID',l_terminal_id); -- this may not be necessary since we lock the doc on customer_bank_id
        SELECT MAX(B.BATCH_ID) -- just to be safe in case there is more than one
          INTO l_batch_id
          FROM BATCH B
         WHERE B.DOC_ID = l_doc_id
           AND B.TERMINAL_ID = l_terminal_id
           AND B.PAYMENT_SCHEDULE_ID = l_pay_sched
           AND B.BATCH_STATE_CD IN('O', 'L') -- Open or Closeable only
           -- for "As Accumulated", batch start date does not matter
           AND (B.START_DATE <= l_entry_date OR l_pay_sched IN(1,5,8))
           AND (NVL(B.END_DATE, MAX_DATE) > l_entry_date OR (B.END_DATE = l_entry_date AND l_pay_sched IN(8)))
           AND (l_pay_sched NOT IN(8) OR DECODE(B.END_DATE, NULL, 0, 1) = DECODE(l_always_as_accum, 'E', 1, 0));
        IF l_batch_id IS NULL THEN
            -- calc start and end dates
            IF l_pay_sched IN(1, 5) THEN
                SELECT LEAST(NVL(MAX(B.END_DATE), l_entry_date), l_entry_date)
                  INTO l_start_date
                  FROM BATCH B, DOC D
                 WHERE B.DOC_ID = D.DOC_ID
                   AND B.TERMINAL_ID = l_terminal_id
                   AND D.CUSTOMER_BANK_ID = l_cust_bank_id
                   AND NVL(D.CURRENCY_ID, 0) = NVL(l_currency_id, 0)
                   AND B.PAYMENT_SCHEDULE_ID = l_pay_sched;
                l_batch_state_cd := 'L'; -- Closeable
                -- end date is null
            ELSIF l_pay_sched = 2 THEN
                SELECT DECODE(MAX(BCOT.BATCH_CONFIRM_OPTION_TYPE_ID), NULL, 'N', 'Y')
                  INTO l_counters_must_show
                  FROM REPORT.TERMINAL T
                  JOIN CORP.BATCH_CONFIRM BC ON T.CUSTOMER_ID = BC.CUSTOMER_ID
                  JOIN CORP.BATCH_CONFIRM_OPTION BCO
                    ON BC.BATCH_CONFIRM_ID = BCO.BATCH_CONFIRM_ID
                  JOIN CORP.BATCH_CONFIRM_OPTION_TYPE BCOT ON BCO.BATCH_CONFIRM_OPTION_TYPE_ID = BCOT.BATCH_CONFIRM_OPTION_TYPE_ID
                 WHERE BC.PAYMENT_SCHEDULE_ID = l_pay_sched 
                   AND BCOT.BATCH_CONFIRM_OPTION_TYPE_CD = 'COUNTER_MUST_SHOW'                  
                   AND T.TERMINAL_ID = l_terminal_id;
                       
                SELECT NVL(MAX(FILL_DATE), MIN_DATE), MAX(FILL_ID)
                  INTO l_start_date, l_start_fill_id
                  FROM (
                    SELECT f.FILL_DATE, f.FILL_ID
                      FROM REPORT.FILL f
                      JOIN REPORT.TERMINAL_EPORT te
                        ON f.EPORT_ID = te.EPORT_ID
                       AND f.FILL_DATE >= NVL(te.START_DATE, MIN_DATE)
                       AND f.FILL_DATE < NVL(te.END_DATE, MAX_DATE)
                     WHERE f.FILL_DATE <= l_entry_date
                       AND te.TERMINAL_ID = l_terminal_id
                       AND (F.COUNTERS_DISPLAYED_FLAG = 'Y' OR l_counters_must_show = 'N')
                     ORDER BY f.FILL_DATE DESC)
                 WHERE ROWNUM = 1 ;
                 SELECT MIN(FILL_DATE), MIN(FILL_ID)
                  INTO l_end_date, l_end_fill_id
                  FROM (
                    SELECT f.FILL_DATE, f.FILL_ID
                      FROM REPORT.FILL f
                      JOIN (SELECT CONNECT_BY_ROOT FILL_ID START_FILL_ID, FILL_ID END_FILL_ID, LEVEL - 1 DEPTH
                              FROM REPORT.FILL
                              START WITH FILL_ID = l_start_fill_id
                              CONNECT BY NOCYCLE PRIOR FILL_ID = PREV_FILL_ID
                              ) H ON F.FILL_ID = H.END_FILL_ID
                     JOIN REPORT.TERMINAL_EPORT te
                        ON f.EPORT_ID = te.EPORT_ID
                       AND f.FILL_DATE >= NVL(te.START_DATE, MIN_DATE)
                       AND f.FILL_DATE < NVL(te.END_DATE, MAX_DATE)
                     WHERE f.FILL_DATE > l_entry_date
                       AND te.TERMINAL_ID = l_terminal_id
                       AND (F.COUNTERS_DISPLAYED_FLAG = 'Y' OR l_counters_must_show = 'N')
                     ORDER BY f.FILL_DATE ASC)
                 WHERE ROWNUM = 1;
                
                l_batch_state_cd := 'O'; -- Open
            ELSIF l_pay_sched = 8 THEN
                SELECT LEAST(NVL(MAX(B.END_DATE), l_entry_date), l_entry_date)
                  INTO l_start_date
                  FROM BATCH B, DOC D
                 WHERE B.DOC_ID = D.DOC_ID
                   AND B.TERMINAL_ID = l_terminal_id
                   AND D.CUSTOMER_BANK_ID = l_cust_bank_id
                   AND NVL(D.CURRENCY_ID, 0) = NVL(l_currency_id, 0)
                   AND B.PAYMENT_SCHEDULE_ID = l_pay_sched;
                l_batch_state_cd := 'O'; -- Open
            ELSE
                SELECT TRUNC(l_entry_date - (OFFSET_HOURS / 24), INTERVAL) + (OFFSET_HOURS / 24),
                       ADD_MONTHS(TRUNC(l_entry_date - (OFFSET_HOURS / 24), INTERVAL) + DAYS, MONTHS) + (OFFSET_HOURS / 24)
                  INTO l_start_date, l_end_date
                  FROM PAYMENT_SCHEDULE
                 WHERE PAYMENT_SCHEDULE_ID = l_pay_sched;
                IF l_end_date <= l_entry_date THEN -- trouble, should not happen
                    l_end_date := l_entry_date + (1.0/(24*60*60));
                END IF;
                l_batch_state_cd := 'O'; -- Open
            END IF;

            -- create new batch record
            SELECT BATCH_SEQ.NEXTVAL
              INTO l_batch_id
              FROM DUAL;
            INSERT INTO BATCH(
                BATCH_ID,
                DOC_ID,
                TERMINAL_ID,
                PAYMENT_SCHEDULE_ID,
                START_DATE,
                END_DATE,
                BATCH_STATE_CD)
              VALUES(
                l_batch_id,
                l_doc_id,
                l_terminal_id,
                l_pay_sched,
                l_start_date,
                l_end_date,
                l_batch_state_cd);
            IF l_pay_sched = 2 THEN
                INSERT INTO CORP.BATCH_FILL(
                    BATCH_ID,
                    START_FILL_ID,
                    END_FILL_ID)
                  VALUES(
                    l_batch_id,
                    l_start_fill_id,
                    l_end_fill_id);
            END IF;
        END IF;
        RETURN l_batch_id;
    END;
    FUNCTION GET_OR_CREATE_BATCH(
        l_terminal_id  BATCH.TERMINAL_ID%TYPE,
        l_cust_bank_id DOC.CUSTOMER_BANK_ID%TYPE,
        l_entry_date LEDGER.ENTRY_DATE%TYPE,
        l_currency_id DOC.CURRENCY_ID%TYPE,
        l_always_as_accum CHAR
    )
     RETURN BATCH.BATCH_ID%TYPE
    IS
        l_business_unit CORP.BUSINESS_UNIT.BUSINESS_UNIT_ID%TYPE;
        l_pay_sched TERMINAL.PAYMENT_SCHEDULE_ID%TYPE;
    BEGIN
        -- calculate batch (and create if necessary)
        SELECT DECODE(l_always_as_accum,
                    'Y', 1,
                    'A', 5,
                    PAYMENT_SCHEDULE_ID),
               BUSINESS_UNIT_ID
          INTO l_pay_sched, l_business_unit
          FROM TERMINAL
         WHERE TERMINAL_ID = l_terminal_id;
        RETURN GET_OR_CREATE_BATCH(l_terminal_id,l_cust_bank_id,l_entry_date,l_currency_id,l_pay_sched, l_business_unit, l_always_as_accum);
    END;
    
    FUNCTION GET_NEW_BATCH(
        l_batch_id BATCH.BATCH_ID%TYPE,
        l_entry_date LEDGER.ENTRY_DATE%TYPE)
     RETURN BATCH.BATCH_ID%TYPE
    IS
        l_cb_id DOC.CUSTOMER_BANK_ID%TYPE;
        l_curr_id DOC.CURRENCY_ID%TYPE;
        l_term_id BATCH.TERMINAL_ID%TYPE;
    BEGIN

        SELECT D.CUSTOMER_BANK_ID, D.CURRENCY_ID, B.TERMINAL_ID
          INTO l_cb_id, l_curr_id, l_term_id
          FROM BATCH B, DOC D
          WHERE D.DOC_ID = B.DOC_ID
            AND B.BATCH_ID = l_batch_id;
        RETURN GET_OR_CREATE_BATCH(l_term_id, l_cb_id, l_entry_date, l_curr_id, 'Y');
    END;
    
    FUNCTION GET_DOC_STATUS(
        l_doc_id DOC.DOC_ID%TYPE)
     RETURN DOC.STATUS%TYPE
    IS
        l_status DOC.STATUS%TYPE;
    BEGIN
        SELECT STATUS INTO l_status FROM CORP.DOC WHERE DOC_ID = l_doc_id;
        RETURN l_status;
    END;

    FUNCTION FREEZE_DOC_STATUS_LEDGER_ID(
        l_ledger_id LEDGER.LEDGER_ID%TYPE)
     RETURN DOC.STATUS%TYPE
    IS
        l_doc_id DOC.DOC_ID%TYPE;
        l_customer_bank_id CORP.DOC.CUSTOMER_BANK_ID%TYPE;
        l_lock VARCHAR2(128);
    BEGIN
        SELECT D.DOC_ID, D.CUSTOMER_BANK_ID
          INTO l_doc_id, l_customer_bank_id
          FROM CORP.DOC D
         INNER JOIN CORP.BATCH B ON D.DOC_ID = B.DOC_ID
         INNER JOIN CORP.LEDGER L ON L.BATCH_ID = B.BATCH_ID
         WHERE L.LEDGER_ID = l_ledger_id;
        l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_customer_bank_id);
        RETURN GET_DOC_STATUS(l_doc_id);
    END;
    
    FUNCTION FREEZE_DOC_STATUS_DOC_ID(
        l_doc_id DOC.DOC_ID%TYPE)
     RETURN DOC.STATUS%TYPE
    IS
        l_customer_bank_id CORP.DOC.CUSTOMER_BANK_ID%TYPE;
        l_lock VARCHAR2(128);
    BEGIN
        SELECT D.CUSTOMER_BANK_ID
          INTO l_customer_bank_id
          FROM CORP.DOC D
         WHERE D.DOC_ID = l_doc_id;
        l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_customer_bank_id);
        RETURN GET_DOC_STATUS(l_doc_id);
    END;
    
    -- Returns 'Y' if the batch is either an "As Accumulated" or has no unsettled transactions
    FUNCTION BATCH_CLOSABLE(
        l_batch_id BATCH.BATCH_ID%TYPE,
        l_pay_sched BATCH.PAYMENT_SCHEDULE_ID%TYPE,
        l_end_date BATCH.END_DATE%TYPE,
        l_batch_state_cd BATCH.BATCH_STATE_CD%TYPE)
     RETURN CHAR
    IS
        l_cnt NUMBER;
    BEGIN
        IF l_batch_state_cd IN('L', 'F') THEN
            RETURN 'Y';
        ELSIF l_batch_state_cd IN('C', 'D') THEN
            IF l_pay_sched IN(2) THEN -- Fill To Fill
                SELECT COUNT(*)
                  INTO l_cnt
                  FROM LEDGER l
                 WHERE ENTRY_PAYABLE(L.SETTLE_STATE_ID, L.ENTRY_TYPE) = '?' -- not processed
                   AND L.DELETED = 'N'
                   AND L.BATCH_ID = l_batch_id;
                IF l_cnt = 0 THEN
                    RETURN 'Y';
                ELSE
                    RETURN 'N';
                END IF;
            ELSE
                RETURN 'Y';
            END IF;
        ELSIF l_batch_state_cd IN('O') AND l_pay_sched NOT IN(1,2,5,7,8) AND l_end_date <= SYSDATE THEN
            RETURN 'Y';
        ELSE
            RETURN 'N';
        END IF;
    END;

    -- Returns 'Y' if the batch is either an "As Accumulated" or has no unsettled transactions
    FUNCTION BATCH_CLOSABLE(
        l_batch_id BATCH.BATCH_ID%TYPE)
     RETURN CHAR
    IS
        l_pay_sched BATCH.PAYMENT_SCHEDULE_ID%TYPE;
        l_end_date BATCH.END_DATE%TYPE;
        l_batch_state_cd BATCH.BATCH_STATE_CD%TYPE;
    BEGIN
        SELECT PAYMENT_SCHEDULE_ID, END_DATE, BATCH_STATE_CD
          INTO l_pay_sched, l_end_date, l_batch_state_cd
          FROM BATCH
         WHERE BATCH_ID = l_batch_id;
        RETURN BATCH_CLOSABLE(l_batch_id, l_pay_sched, l_end_date, l_batch_state_cd);
    END;

    PROCEDURE CHECK_TRANS_WITH_PF_CLOSED(
        l_process_fee_id    CORP.LEDGER.PROCESS_FEE_ID%TYPE)
    IS
        CURSOR l_check_cur IS
           SELECT L.TRANS_ID
              FROM LEDGER L, BATCH B, DOC D
             WHERE L.PROCESS_FEE_ID = l_process_fee_id
               AND L.BATCH_ID = B.BATCH_ID
               AND B.DOC_ID = D.DOC_ID
             GROUP BY L.TRANS_ID
             HAVING SUM(DECODE(D.STATUS, 'O', 0, 'D', 0, 1)) > 0
                AND SUM(DECODE(D.STATUS, 'O', 1, 0)) = 0;
        l_batch_id BATCH.BATCH_ID%TYPE;
    BEGIN
        FOR l_check_rec IN l_check_cur LOOP
            SELECT MAX(B.BATCH_ID)
              INTO l_batch_id
              FROM LEDGER L, BATCH B, DOC D
             WHERE L.TRANS_ID = l_check_rec.TRANS_ID
               AND L.BATCH_ID = B.BATCH_ID
               AND B.DOC_ID = D.DOC_ID
               AND D.STATUS NOT IN('O', 'D');
            IF l_batch_id IS NOT NULL THEN
                RAISE_APPLICATION_ERROR(-20701, 'PROCESS FEE (process_fee_id='||TO_CHAR(l_process_fee_id)||' is used in a batch (id='||TO_CHAR(l_batch_id)||') that was already closed - cannot update ledger table!');
            END IF;
        END LOOP;
    END;

    -- Checks if the specified transaction is in a closed batch and if so raises an
    -- exception
    PROCEDURE CHECK_TRANS_CLOSED(
        l_trans_id    CORP.LEDGER.TRANS_ID%TYPE)
    IS
        l_closed_cnt PLS_INTEGER;
        l_open_cnt PLS_INTEGER;
        l_batch_id BATCH.BATCH_ID%TYPE;
    BEGIN
        -- get the batch
        SELECT SUM(DECODE(D.STATUS, 'O', 0, 'D', 0, 1)),
               SUM(DECODE(D.STATUS, 'O', 1, 0))
          INTO l_closed_cnt, l_open_cnt
          FROM LEDGER L, BATCH B, DOC D
         WHERE L.TRANS_ID = l_trans_id
           AND L.BATCH_ID = B.BATCH_ID
           AND B.DOC_ID = D.DOC_ID;
        IF l_open_cnt = 0 AND l_closed_cnt > 0 THEN
            SELECT MAX(B.BATCH_ID)
              INTO l_batch_id
              FROM LEDGER L, BATCH B, DOC D
             WHERE L.TRANS_ID = l_trans_id
               AND L.BATCH_ID = B.BATCH_ID
               AND B.DOC_ID = D.DOC_ID
               AND D.STATUS NOT IN('O', 'D');
            RAISE_APPLICATION_ERROR(-20701, 'TRANSACTION (tran_id='||TO_CHAR(l_trans_id)||' is in a batch (id='||TO_CHAR(l_batch_id)||') that was already closed - cannot update ledger table!');
        END IF;
    END;
    
    PROCEDURE CHECK_FILL_BATCH_COMPLETE(
        l_batch_id BATCH.BATCH_ID%TYPE)
    IS
        l_batch_amt NUMBER;
        l_batch_tran_cnt NUMBER;
        l_src_credit_amt NUMBER;
        l_src_credit_cnt NUMBER;
        l_first_tran_ts DATE;
        lv_source_system_cd REPORT.FILL.SOURCE_SYSTEM_CD%TYPE;
        ld_fill_date REPORT.FILL.FILL_DATE%TYPE;
    BEGIN
        SELECT F.SOURCE_SYSTEM_CD, F.FILL_DATE
          INTO lv_source_system_cd, ld_fill_date
          FROM REPORT.FILL F
          JOIN CORP.BATCH_FILL BF ON BF.END_FILL_ID = F.FILL_ID
         WHERE BF.BATCH_ID = l_batch_id;
        IF lv_source_system_cd IS NULL THEN -- this is a manual fill, consider it complete after one day
          IF ld_fill_date <= SYSDATE - 1 THEN
              UPDATE CORP.BATCH B
               SET B.BATCH_STATE_CD = (
                    SELECT DECODE(COUNT(*), 0, 'D', 'U')
                      FROM REPORT.TERMINAL t
                      JOIN CORP.BATCH_CONFIRM CN ON t.CUSTOMER_ID = cn.CUSTOMER_ID
                     WHERE b.TERMINAL_ID = t.TERMINAL_ID
                       AND b.PAYMENT_SCHEDULE_ID = cn.PAYMENT_SCHEDULE_ID),
                   B.BATCH_CLOSED_TS = SYSDATE
              WHERE B.BATCH_ID = l_batch_id;
          END IF;
          RETURN;
        END IF;
        
        SELECT SUM(l.AMOUNT), MIN(l.ENTRY_DATE)
          INTO l_batch_amt, l_first_tran_ts
     	  FROM CORP.LEDGER l
         WHERE l.ENTRY_TYPE = 'CC'
           AND l.DELETED = 'N'
           AND l.BATCH_ID = l_batch_id;
                   
        SELECT SUM(p.AMOUNT)
          INTO l_batch_tran_cnt
          FROM REPORT.PURCHASE P
          LEFT OUTER JOIN PSS.TRAN_LINE_ITEM_TYPE XIT ON P.TRAN_LINE_ITEM_TYPE_ID = XIT.TRAN_LINE_ITEM_TYPE_ID
         WHERE P.TRAN_ID IN(
            SELECT l.TRANS_ID 
              FROM CORP.LEDGER l
             WHERE l.ENTRY_TYPE = 'CC'
               AND l.DELETED = 'N'
               AND l.BATCH_ID = l_batch_id)
           AND (XIT.TRAN_LINE_ITEM_TYPE_GROUP_CD IS NULL OR XIT.TRAN_LINE_ITEM_TYPE_GROUP_CD IN('P', 'S'));
        
        BEGIN
            SELECT SUM(HF.CREDIT_AMOUNT), SUM(HF.CREDIT_COUNT)
              INTO l_src_credit_amt, l_src_credit_cnt
              FROM CORP.BATCH_FILL BF
              JOIN (SELECT CONNECT_BY_ROOT FILL_ID ANCESTOR_FILL_ID, F.*
                      FROM REPORT.FILL F
                     START WITH F.FILL_ID = (SELECT START_FILL_ID FROM CORP.BATCH_FILL WHERE BATCH_ID = l_batch_id)
                   CONNECT BY NOCYCLE PRIOR FILL_ID = PREV_FILL_ID) HF ON BF.START_FILL_ID = HF.ANCESTOR_FILL_ID AND BF.END_FILL_ID = HF.FILL_ID
             WHERE BF.BATCH_ID = l_batch_id 
            HAVING COUNT(*) = COUNT(HF.CREDIT_AMOUNT) 
               AND COUNT(*) =  COUNT(HF.CREDIT_COUNT);
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                DECLARE
                    CURSOR l_cur IS
                        SELECT e.EPORT_SERIAL_NUM DEVICE_SERIAL_CD, 
                               GREATEST(l_first_tran_ts, NVL(te.START_DATE, MIN_DATE)) START_DATE,
                               LEAST(B.END_DATE,NVL(te.END_DATE, MAX_DATE)) END_DATE 
                          FROM CORP.BATCH B
                          JOIN REPORT.TERMINAL_EPORT te ON b.TERMINAL_ID = te.TERMINAL_ID
            	          JOIN REPORT.EPORT E ON te.EPORT_ID = e.EPORT_ID
        		         WHERE B.BATCH_ID = l_batch_id;
        		BEGIN
                    l_src_credit_amt := 0;
                    l_src_credit_cnt := 0;
                    FOR l_rec IN l_cur LOOP
                        SELECT l_src_credit_amt + NVL(SUM(T.TRAN_LINE_ITEM_AMOUNT * T.TRAN_LINE_ITEM_QUANTITY), 0), 
                               l_src_credit_cnt + NVL(SUM(DECODE(T.TRAN_LINE_ITEM_TYPE_GROUP_CD, 'U', NULL, 'I', NULL, T.TRAN_LINE_ITEM_QUANTITY)), 0)
                	      INTO l_src_credit_amt, l_src_credit_cnt                         
                          FROM PSS.VW_REPORTING_TRAN_LINE_ITEM T
                          WHERE T.DEVICE_SERIAL_CD = l_rec.DEVICE_SERIAL_CD
                            AND T.CLIENT_PAYMENT_TYPE_CD IN('C', 'R')
                            AND T.TRAN_START_TS BETWEEN l_rec.START_DATE AND l_rec.END_DATE;
                    END LOOP; 
                END;       
            WHEN OTHERS THEN
                RAISE;
        END;

        IF l_batch_amt = l_src_credit_amt AND l_batch_tran_cnt = l_src_credit_cnt THEN
            UPDATE CORP.BATCH B
               SET B.BATCH_STATE_CD = (
                    SELECT DECODE(COUNT(*), 0, 'D', 'U')
                      FROM REPORT.TERMINAL t
                      JOIN CORP.BATCH_CONFIRM CN ON t.CUSTOMER_ID = cn.CUSTOMER_ID
                     WHERE b.TERMINAL_ID = t.TERMINAL_ID
                       AND b.PAYMENT_SCHEDULE_ID = cn.PAYMENT_SCHEDULE_ID),
                   B.BATCH_CLOSED_TS = SYSDATE
              WHERE B.BATCH_ID = l_batch_id;
        END IF;	
    END;
    
    PROCEDURE ADD_BATCH_ROUNDING_ENTRY(
        l_batch_id BATCH.BATCH_ID%TYPE)
    IS
        l_round_amt LEDGER.AMOUNT%TYPE;
    BEGIN
        -- calculate rounding amount
        SELECT ROUND(SUM(AMOUNT), 2) - SUM(AMOUNT)
          INTO l_round_amt
          FROM LEDGER L
         WHERE L.DELETED = 'N'
           AND ENTRY_PAYABLE(L.SETTLE_STATE_ID,L.ENTRY_TYPE) = 'Y'
           AND L.BATCH_ID = l_batch_id;

        -- add entry if necessary
        IF l_round_amt <> 0 THEN
            INSERT INTO LEDGER(
                LEDGER_ID,
                ENTRY_TYPE,
                TRANS_ID,
                PROCESS_FEE_ID,
                AMOUNT,
                ENTRY_DATE,
                BATCH_ID,
                SETTLE_STATE_ID,
                LEDGER_DATE,
                DESCRIPTION)
            SELECT
                LEDGER_SEQ.NEXTVAL,
                'SB',
                NULL,
                NULL,
                l_round_amt,
                SYSDATE,
                l_batch_id,
                2,
                SYSDATE,
                NULL
              FROM DUAL;
        END IF;
    END;

    PROCEDURE ADD_BATCH_ROUNDING_ENTRIES(
        l_doc_id DOC.DOC_ID%TYPE)
    IS
        CURSOR l_cur IS
            SELECT BATCH_ID
              FROM BATCH
             WHERE DOC_ID = l_doc_id;
    BEGIN
        FOR l_rec IN l_cur LOOP
            ADD_BATCH_ROUNDING_ENTRY(l_rec.BATCH_ID);
        END LOOP;
    END;

    PROCEDURE ADD_ROUNDING_ENTRY(
        l_doc_id DOC.DOC_ID%TYPE)
    IS
        l_round_amt LEDGER.AMOUNT%TYPE;
        l_batch_id BATCH.BATCH_ID%TYPE;
    BEGIN
        -- calculate rounding amount
        SELECT ROUND(SUM(AMOUNT), 2) - SUM(AMOUNT)
          INTO l_round_amt
          FROM LEDGER L, BATCH B
         WHERE L.DELETED = 'N'
           AND ENTRY_PAYABLE(L.SETTLE_STATE_ID,L.ENTRY_TYPE) = 'Y'
           AND L.BATCH_ID = B.BATCH_ID
           AND B.DOC_ID = l_doc_id;

        -- add entry if necessary
        IF l_round_amt <> 0 THEN
            SELECT MAX(BATCH_ID)
              INTO l_batch_id
              FROM BATCH
             WHERE DOC_ID = l_doc_id
               AND TERMINAL_ID IS NULL;
            IF l_batch_id IS NULL THEN
                -- create a batch and set the doc id
                SELECT BATCH_SEQ.NEXTVAL
                  INTO l_batch_id
                  FROM DUAL;
                INSERT INTO BATCH(BATCH_ID, DOC_ID, TERMINAL_ID, PAYMENT_SCHEDULE_ID, START_DATE, END_DATE, BATCH_STATE_CD)
                    SELECT l_batch_id, l_doc_id, NULL, 1, SYSDATE, SYSDATE, 'F'
                      FROM DOC
                     WHERE DOC_ID = l_doc_id;
            END IF;

            INSERT INTO LEDGER(
                LEDGER_ID,
                ENTRY_TYPE,
                TRANS_ID,
                PROCESS_FEE_ID,
                AMOUNT,
                ENTRY_DATE,
                BATCH_ID,
                SETTLE_STATE_ID,
                LEDGER_DATE,
                DESCRIPTION)
            SELECT
                LEDGER_SEQ.NEXTVAL,
                'SB',
                NULL,
                NULL,
                l_round_amt,
                SYSDATE,
                l_batch_id,
                2,
                SYSDATE,
                NULL
              FROM DUAL;
        END IF;
    END;
    /*
     * This procedure references REPORT.TRAN
    */
    PROCEDURE UPDATE_SETTLEMENT(
        l_trans_id    CORP.LEDGER.TRANS_ID%TYPE,
        l_settle_state_id CORP.LEDGER.SETTLE_STATE_ID%TYPE,
        l_settle_date   CORP.LEDGER.LEDGER_DATE%TYPE)
    IS
        CURSOR l_cur IS
            SELECT D.DOC_ID, B.BATCH_ID, D.CUSTOMER_BANK_ID
              FROM LEDGER L, BATCH B, DOC D
             WHERE L.TRANS_ID = l_trans_id
               AND L.BATCH_ID = B.BATCH_ID
               AND B.DOC_ID = D.DOC_ID
               AND D.STATUS = 'O';
        TYPE t_rec_list IS TABLE OF l_cur%ROWTYPE;
        l_recs t_rec_list;
        l_batch_ids NUMBER_TABLE := NUMBER_TABLE();
        l_lock VARCHAR2(128);
    BEGIN
        l_lock := GLOBALS_PKG.REQUEST_LOCK('LEDGER.TRANS_ID',l_trans_id);
        OPEN l_cur;
        FETCH l_cur BULK COLLECT INTO l_recs;
        CLOSE l_cur;

        IF l_recs.FIRST IS NOT NULL THEN
            FOR i IN l_recs.FIRST .. l_recs.LAST LOOP
                l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_recs(i).CUSTOMER_BANK_ID);
                IF GET_DOC_STATUS(l_recs(i).DOC_ID) = 'O' THEN
                   l_batch_ids.EXTEND;
                   l_batch_ids(l_batch_ids.LAST) := l_recs(i).BATCH_ID;
                ELSE
                    EXIT;
                END IF;
            END LOOP;
        END IF;
        IF l_recs.FIRST IS NOT NULL AND l_recs.LAST = l_batch_ids.LAST THEN -- we can do direct update Yippee!
            UPDATE LEDGER SET
                SETTLE_STATE_ID = l_settle_state_id,
                LEDGER_DATE = l_settle_date
             WHERE TRANS_ID = l_trans_id
               AND BATCH_ID MEMBER OF l_batch_ids;
        ELSE
           DECLARE
                l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE;
                l_close_date LEDGER.LEDGER_DATE%TYPE;
                l_total_amount LEDGER.AMOUNT%TYPE;
                l_terminal_id BATCH.TERMINAL_ID%TYPE;
                l_customer_bank_id DOC.CUSTOMER_BANK_ID%TYPE;
                l_process_fee_id LEDGER.PROCESS_FEE_ID%TYPE;
                l_currency_id DOC.CURRENCY_ID%TYPE;
            BEGIN
                SELECT TRANS_TYPE_ID, CLOSE_DATE, TOTAL_AMOUNT, TERMINAL_ID,
                       CUSTOMER_BANK_ID, PROCESS_FEE_ID, CURRENCY_ID
                  INTO l_trans_type_id, l_close_date, l_total_amount, l_terminal_id,
                       l_customer_bank_id, l_process_fee_id, l_currency_id
                  FROM TRANS
                 WHERE TRAN_ID = l_trans_id;

                UPDATE_LEDGER(l_trans_id, l_trans_type_id, l_close_date,
                    l_settle_date, l_total_amount, l_settle_state_id,
                    l_terminal_id, l_customer_bank_id,l_process_fee_id, l_currency_id);
            END;
        END IF;
    END;

    PROCEDURE UPDATE_PROCESS_FEE_VALUES(
        l_process_fee_id CORP.LEDGER.PROCESS_FEE_ID%TYPE)
    IS
        CURSOR l_tran_cur IS
           SELECT DISTINCT L.TRANS_ID, X.TRANS_TYPE_ID, X.CLOSE_DATE, X.TOTAL_AMOUNT,
                  X.TERMINAL_ID, X.CUSTOMER_BANK_ID, X.SETTLE_STATE_ID,
                  X.SETTLE_DATE, X.CURRENCY_ID
              FROM LEDGER L, REPORT.TRANS X
             WHERE L.PROCESS_FEE_ID = l_process_fee_id
               AND L.DELETED = 'N'
               AND L.ENTRY_TYPE = 'PF'
               AND L.TRANS_ID = X.TRAN_ID;
    BEGIN
        FOR l_tran_rec IN l_tran_cur LOOP
            UPDATE_LEDGER(
                l_tran_rec.TRANS_ID,
                l_tran_rec.trans_type_id,
                l_tran_rec.close_date,
                l_tran_rec.settle_date,
                l_tran_rec.total_amount,
                l_tran_rec.settle_state_id,
                l_tran_rec.terminal_id,
                l_tran_rec.customer_bank_id,
                l_process_fee_id,
                l_tran_rec.currency_id);
        END LOOP;
    END;

    -- Puts CC, RF, or CB record into Ledger and also any required PF record
    PROCEDURE INSERT_TRANS_TO_LEDGER(
        l_trans_id    LEDGER.TRANS_ID%TYPE,
        l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE,
        l_close_date    LEDGER.ENTRY_DATE%TYPE,
        l_settle_date LEDGER.LEDGER_DATE%TYPE,
        l_amount LEDGER.AMOUNT%TYPE,
        l_settle_state_id LEDGER.SETTLE_STATE_ID%TYPE,
        l_process_fee_id    LEDGER.PROCESS_FEE_ID%TYPE,
        l_batch_id LEDGER.BATCH_ID%TYPE)
    IS
        l_ledger_id LEDGER.LEDGER_ID%TYPE;
        l_real_amt LEDGER.AMOUNT%TYPE;
        l_pay_sched BATCH.PAYMENT_SCHEDULE_ID%TYPE;
        l_end_date BATCH.END_DATE%TYPE;
		l_payable_ind REPORT.TRANS_TYPE.PAYABLE_IND%TYPE;
		l_customer_debit_ind REPORT.TRANS_TYPE.CUSTOMER_DEBIT_IND%TYPE;
		l_entry_type REPORT.TRANS_TYPE.ENTRY_TYPE%TYPE;
		l_fill_batch_ind REPORT.TRANS_TYPE.FILL_BATCH_IND%TYPE;
    BEGIN
        IF NVL(l_amount, 0) = 0 THEN
            RETURN;
        END IF;
		
		SELECT PAYABLE_IND, CUSTOMER_DEBIT_IND, ENTRY_TYPE, FILL_BATCH_IND
		INTO l_payable_ind, l_customer_debit_ind, l_entry_type, l_fill_batch_ind
		FROM REPORT.TRANS_TYPE
		WHERE TRANS_TYPE_ID = l_trans_type_id;
        
		IF l_payable_ind = 'Y' THEN
            SELECT LEDGER_SEQ.NEXTVAL,
                   DECODE(l_customer_debit_ind, 'Y', -ABS(l_amount), l_amount)
              INTO l_ledger_id,
                   l_real_amt
              FROM DUAL;
            INSERT INTO LEDGER(
                LEDGER_ID,
                ENTRY_TYPE,
                TRANS_ID,
                PROCESS_FEE_ID,
                AMOUNT,
                ENTRY_DATE,
                BATCH_ID,
                SETTLE_STATE_ID,
                LEDGER_DATE)
            SELECT
                l_ledger_id,
                l_entry_type,
                l_trans_id,
                l_process_fee_id,
                l_real_amt,
                l_close_date,
                l_batch_id,
                l_settle_state_id,
                l_settle_date
              FROM DUAL;
            -- create net revenue fee on transaction, if any
            INSERT INTO LEDGER(
                LEDGER_ID,
                RELATED_LEDGER_ID,
                ENTRY_TYPE,
                TRANS_ID,
                PROCESS_FEE_ID,
                SERVICE_FEE_ID,
                AMOUNT,
                ENTRY_DATE,
                BATCH_ID,
                SETTLE_STATE_ID,
                LEDGER_DATE,
                DESCRIPTION)
            SELECT
                LEDGER_SEQ.NEXTVAL,
                l_ledger_id,
                'SF',
                l_trans_id,
                l_process_fee_id,
                SF.SERVICE_FEE_ID,
                -l_real_amt * SF.FEE_PERCENT,
                l_close_date,
                l_batch_id,
                l_settle_state_id,
                l_settle_date,
                F.FEE_NAME
            FROM SERVICE_FEES SF, FEES F, BATCH B
           WHERE SF.FEE_ID = F.FEE_ID
             AND SF.TERMINAL_ID = B.TERMINAL_ID
             AND B.BATCH_ID = l_batch_id
             AND l_close_date BETWEEN NVL(SF.START_DATE, MIN_DATE)
                  AND NVL(SF.END_DATE, MAX_DATE)
             AND SF.FREQUENCY_ID = 6;
        END IF;
        IF l_process_fee_id IS NOT NULL THEN
            BEGIN
                SELECT LEDGER_SEQ.NEXTVAL,
                       -GREATEST((ABS(l_amount) * pf.FEE_PERCENT + pf.FEE_AMOUNT), pf.MIN_AMOUNT)
                  INTO l_ledger_id,
                       l_real_amt
                  FROM PROCESS_FEES pf
                WHERE pf.PROCESS_FEE_ID = l_process_fee_id
                  AND (pf.FEE_PERCENT > 0 OR pf.FEE_AMOUNT > 0);
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                RETURN;
              WHEN OTHERS THEN
                RAISE;
            END;
            --also insert process fee
            INSERT INTO LEDGER(
                LEDGER_ID,
                ENTRY_TYPE,
                TRANS_ID,
                PROCESS_FEE_ID,
                AMOUNT,
                ENTRY_DATE,
                BATCH_ID,
                SETTLE_STATE_ID,
                LEDGER_DATE)
            VALUES(
                l_ledger_id,
                'PF',
                l_trans_id,
                l_process_fee_id,
                l_real_amt,
                l_close_date,
                l_batch_id,
                l_settle_state_id,
                l_settle_date);
            -- create net revenue fee on process fee, if any
            INSERT INTO LEDGER(
                LEDGER_ID,
                RELATED_LEDGER_ID,
                ENTRY_TYPE,
                TRANS_ID,
                PROCESS_FEE_ID,
                SERVICE_FEE_ID,
                AMOUNT,
                ENTRY_DATE,
                BATCH_ID,
                SETTLE_STATE_ID,
                LEDGER_DATE,
                DESCRIPTION)
            SELECT
                LEDGER_SEQ.NEXTVAL,
                l_ledger_id,
                'SF',
                l_trans_id,
                l_process_fee_id,
                SF.SERVICE_FEE_ID,
                -l_real_amt * SF.FEE_PERCENT,
                l_close_date,
                l_batch_id,
                l_settle_state_id,
                l_settle_date,
                F.FEE_NAME
            FROM SERVICE_FEES SF, FEES F, BATCH B
           WHERE SF.FEE_ID = F.FEE_ID
             AND SF.TERMINAL_ID = B.TERMINAL_ID
             AND B.BATCH_ID = l_batch_id
             AND l_close_date BETWEEN NVL(SF.START_DATE, MIN_DATE)
                  AND NVL(SF.END_DATE, MAX_DATE)
             AND SF.FREQUENCY_ID = 6;
        END IF;
        -- Now check batch complete
        IF l_fill_batch_ind = 'Y' THEN
            SELECT PAYMENT_SCHEDULE_ID, END_DATE
              INTO l_pay_sched, l_end_date
              FROM CORP.BATCH B
             WHERE B.BATCH_ID = l_batch_id;
            IF l_pay_sched IN(2) AND l_end_date IS NOT NULL THEN
               CHECK_FILL_BATCH_COMPLETE(l_batch_id);
            END IF;
        END IF;
    END;

    PROCEDURE INSERT_TRANS_TO_LEDGER(
        l_trans_id    LEDGER.TRANS_ID%TYPE,
        l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE,
        l_close_date    LEDGER.ENTRY_DATE%TYPE,
        l_settle_date LEDGER.LEDGER_DATE%TYPE,
        l_amount LEDGER.AMOUNT%TYPE,
        l_settle_state_id LEDGER.SETTLE_STATE_ID%TYPE,
        l_process_fee_id    LEDGER.PROCESS_FEE_ID%TYPE,
        l_terminal_id   BATCH.TERMINAL_ID%TYPE,
        l_customer_bank_id  DOC.CUSTOMER_BANK_ID%TYPE,
        l_currency_id       DOC.CURRENCY_ID%TYPE)
    IS
        l_batch_id LEDGER.BATCH_ID%TYPE;
        l_doc_status DOC.STATUS%TYPE;
        l_lock VARCHAR2(128);
		l_customer_debit_ind REPORT.TRANS_TYPE.CUSTOMER_DEBIT_IND%TYPE;
    BEGIN
		SELECT CUSTOMER_DEBIT_IND
		INTO l_customer_debit_ind
		FROM REPORT.TRANS_TYPE
		WHERE TRANS_TYPE_ID = l_trans_type_id;
	
        -- if refund or chargeback get the batch that the
        -- original trans is in, if open use it else use "as accumulated"
        IF l_customer_debit_ind = 'Y' THEN
            l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_customer_bank_id);
            l_lock := GLOBALS_PKG.REQUEST_LOCK('BATCH.TERMINAL_ID',l_terminal_id);
            BEGIN
                SELECT BATCH_ID, STATUS
                  INTO l_batch_id, l_doc_status
                  FROM (SELECT B.BATCH_ID, D.STATUS
                          FROM LEDGER L, BATCH B, TRANS T, DOC D
                         WHERE L.TRANS_ID = T.ORIG_TRAN_ID
                           AND T.TRAN_ID = l_trans_id
                           AND L.BATCH_ID = B.BATCH_ID
                           AND L.DELETED = 'N'
                           AND B.DOC_ID = D.DOC_ID
                           AND NVL(D.CURRENCY_ID, 0) = NVL(l_currency_id, 0)
                           AND L.ENTRY_TYPE = 'CC'
                           AND B.PAYMENT_SCHEDULE_ID NOT IN(8) -- As Exported requires each trans to be put in "open" batch
                         ORDER BY DECODE(D.STATUS, 'O', 0, 1), B.BATCH_ID DESC)
                 WHERE ROWNUM = 1;
                IF NVL(l_doc_status, ' ') <> 'O' THEN
                    l_batch_id := GET_OR_CREATE_BATCH(l_terminal_id, l_customer_bank_id, l_close_date, l_currency_id, 'Y');
                END IF;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN -- refund for trans not in ledger
                -- The following should be removed and the error re-enabled,
                -- once we enforce orig_tran_id for all refunds/ chargebacks
                l_batch_id := GET_OR_CREATE_BATCH(l_terminal_id, l_customer_bank_id, l_close_date, l_currency_id, 'Y');
                --RAISE_APPLICATION_ERROR(-20702, 'TRANSACTION (tran_id='||TO_CHAR(l_trans_id)||' is a refund or chargeback on a transaction NOT found in the ledger table!');
                WHEN OTHERS THEN
                    RAISE;
            END;
        ELSE
            l_batch_id := GET_OR_CREATE_BATCH(l_terminal_id, l_customer_bank_id, l_close_date, l_currency_id, 'N');
        END IF;
        INSERT_TRANS_TO_LEDGER(l_trans_id, l_trans_type_id, l_close_date, l_settle_date,
            l_amount, l_settle_state_id, l_process_fee_id, l_batch_id);
    END;
    
    -- Updates the ledger table with the transaction changes
    -- May fail if the transaction is already part of a document
    -- (i.e. - it has already been paid)
    PROCEDURE UPDATE_LEDGER(
        l_trans_id    LEDGER.TRANS_ID%TYPE,
        l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE,
        l_close_date    LEDGER.ENTRY_DATE%TYPE,
        l_settle_date LEDGER.LEDGER_DATE%TYPE,
        l_total_amount LEDGER.AMOUNT%TYPE,
        l_settle_state_id LEDGER.SETTLE_STATE_ID%TYPE,
        l_terminal_id   BATCH.TERMINAL_ID%TYPE,
        l_customer_bank_id  DOC.CUSTOMER_BANK_ID%TYPE,
        l_process_fee_id    LEDGER.PROCESS_FEE_ID%TYPE,
        l_currency_id       DOC.CURRENCY_ID%TYPE)
    IS
        l_batch_id BATCH.BATCH_ID%TYPE;
        l_closed_cnt PLS_INTEGER;
        l_lock VARCHAR2(128);
        l_prev_cb_ids NUMBER_TABLE;
		l_cash_ind REPORT.TRANS_TYPE.CASH_IND%TYPE;
		l_entry_type REPORT.TRANS_TYPE.ENTRY_TYPE%TYPE;
		l_payable_ind REPORT.TRANS_TYPE.PAYABLE_IND%TYPE;
    BEGIN
		SELECT CASH_IND, ENTRY_TYPE, PAYABLE_IND
		INTO l_cash_ind, l_entry_type, l_payable_ind
		FROM REPORT.TRANS_TYPE
		WHERE TRANS_TYPE_ID = l_trans_type_id;
		
        IF l_cash_ind = 'Y' THEN -- cash; ignore
            RETURN;
        END IF;
        l_lock := GLOBALS_PKG.REQUEST_LOCK('LEDGER.TRANS_ID',l_trans_id);
        -- get the batch
        SELECT MAX(B.BATCH_ID), SUM(DECODE(D.STATUS, 'O', 0, 'D', 0, 1)), -- are there ANY closed batches on this transaction?
               SET(CAST(COLLECT(CAST(D.CUSTOMER_BANK_ID AS NUMBER)) AS NUMBER_TABLE))
          INTO l_batch_id, l_closed_cnt, l_prev_cb_ids
          FROM LEDGER L, BATCH B, DOC D
         WHERE L.TRANS_ID = l_trans_id
           AND L.BATCH_ID = B.BATCH_ID
           AND L.DELETED = 'N'
           AND B.DOC_ID = D.DOC_ID;
        IF l_batch_id IS NULL THEN -- Create new batch for this ledger record
            -- If terminal is null then no need to do anything more
            -- Added: if cust bank is null don't do any more (we may need to change this in the future which would also entail making BATCH.CUSTOMER_BANK_ID nullable)
            IF l_terminal_id IS NOT NULL AND l_customer_bank_id IS NOT NULL THEN
                INSERT_TRANS_TO_LEDGER(l_trans_id, l_trans_type_id, l_close_date, l_settle_date,
                    l_total_amount, l_settle_state_id, l_process_fee_id, l_terminal_id, l_customer_bank_id, l_currency_id);
                RETURN; -- all done
            END IF;
        ELSIF l_closed_cnt = 0 THEN -- lock previous customer_bank_id's to ensure that none of the doc's closes
            FOR i IN l_prev_cb_ids.FIRST..l_prev_cb_ids.LAST LOOP
                l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_prev_cb_ids(i));
            END LOOP;
            -- double-check closed count
            SELECT SUM(DECODE(D.STATUS, 'O', 0, 'D', 0, 1))
              INTO l_closed_cnt
              FROM LEDGER L, BATCH B, DOC D
             WHERE L.TRANS_ID = l_trans_id
               AND L.BATCH_ID = B.BATCH_ID
               AND L.DELETED = 'N'
               AND B.DOC_ID = D.DOC_ID;
        END IF;
        IF l_closed_cnt > 0 THEN -- add adjustment for prev paid
            DECLARE
                CURSOR l_prev_cur IS
                    SELECT COUNT(*) PAID_CNT,
                           SUM(DECODE(ENTRY_PAYABLE(L.SETTLE_STATE_ID, L.ENTRY_TYPE), 'Y', AMOUNT, NULL)) PAID_AMT, B.TERMINAL_ID,
                           D.CUSTOMER_BANK_ID, D.CURRENCY_ID
                      FROM LEDGER L, BATCH B, DOC D
                     WHERE L.TRANS_ID = l_trans_id
                       AND L.BATCH_ID = B.BATCH_ID
                       AND B.DOC_ID = D.DOC_ID
                       AND L.DELETED = 'N'
                       AND L.RELATED_LEDGER_ID IS NULL -- Must exclude Net Revenue Fees
                     GROUP BY B.TERMINAL_ID, D.CUSTOMER_BANK_ID, D.CURRENCY_ID;

                l_adj_amt LEDGER.AMOUNT%TYPE;
                l_adjusted_new_cb BOOLEAN := FALSE;
                l_old_pf_amt LEDGER.AMOUNT%TYPE;
                l_old_amt LEDGER.AMOUNT%TYPE;
                l_desc LEDGER.DESCRIPTION%TYPE;
                l_new_payable CHAR(1);
                l_ledger_id LEDGER.LEDGER_ID%TYPE;
            BEGIN
                --figure out what change has occurred
                FOR l_prev_rec IN l_prev_cur LOOP
                    IF NVL(l_customer_bank_id, 0) = l_prev_rec.CUSTOMER_BANK_ID
                       AND NVL(l_currency_id, 0) = NVL(l_prev_rec.CURRENCY_ID, 0) THEN
                        IF NVL(l_prev_rec.PAID_CNT, 0) <> 0 AND NVL(l_prev_rec.PAID_AMT, 0) <> 0 THEN
                            --calc difference and add as adjustment
                            SELECT DECODE(l_prev_rec.TERMINAL_ID, l_terminal_id, ENTRY_PAYABLE(l_settle_state_id, l_entry_type), 'N')
                              INTO l_new_payable
                              FROM DUAL;
                            SELECT (CASE WHEN l_payable_ind = 'Y' THEN l_total_amount ELSE 0 END
                                   - NVL(SUM(GREATEST(ABS(l_total_amount) * pf.FEE_PERCENT + pf.FEE_AMOUNT, pf.MIN_AMOUNT)), 0))
                                   * DECODE(l_new_payable, 'Y', 1, 0)
                                   - l_prev_rec.PAID_AMT
                              INTO l_adj_amt
                              FROM PROCESS_FEES pf
                             WHERE pf.PROCESS_FEE_ID = l_process_fee_id;
                            IF l_adj_amt <> 0 THEN
                                IF l_new_payable = 'Y' THEN
                                    SELECT SUM(AMOUNT)
                                      INTO l_old_amt
                                      FROM LEDGER
                                     WHERE TRANS_ID = l_trans_id
                                       AND ENTRY_TYPE IN('CC')
                                       AND ENTRY_PAYABLE(SETTLE_STATE_ID, ENTRY_TYPE) = 'Y'
                                       AND DELETED = 'N';
                                    SELECT SUM(AMOUNT)
                                      INTO l_old_pf_amt
                                      FROM LEDGER
                                     WHERE TRANS_ID = l_trans_id
                                       AND ENTRY_TYPE IN('PF')
                                       AND ENTRY_PAYABLE(SETTLE_STATE_ID, ENTRY_TYPE) = 'Y'
                                       AND DELETED = 'N';
                                    IF NVL(l_old_amt, 0) = 0 AND l_payable_ind = 'Y' THEN
                                        l_desc := 'Correction for a previously ignored transaction';
                                    ELSIF l_old_amt <> l_total_amount AND l_payable_ind = 'Y' THEN
                                        l_desc := 'Correction for a value amount change on a transaction that has already been paid';
                                    ELSIF NVL(l_old_pf_amt, 0) = 0 THEN
                                        l_desc := 'Correction for a missing processing fee on a transaction that has already been paid';
                                    ELSIF NVL(l_terminal_id, 0) <> l_prev_rec.TERMINAL_ID THEN
                                        l_desc := 'Correction for a change of location on a transaction that has already been paid';
                                    ELSE
                                        l_desc := 'Correction for a change in processing fee on a transaction that has already been paid';
                                    END IF;
                                ELSIF NVL(l_terminal_id, 0) <> l_prev_rec.TERMINAL_ID THEN
                                    l_desc := 'Correction for a change of location on a transaction that has already been paid';
                                ELSE -- should not be paid now
                                    l_desc := 'Correction for a transaction that has already been paid but has since failed settlement';
                                END IF;
                                l_batch_id := GET_OR_CREATE_BATCH(l_prev_rec.TERMINAL_ID,
                                        l_customer_bank_id, l_close_date, l_currency_id, 'A');
                                -- create a negative adjustment based on this previous payment
                                SELECT LEDGER_SEQ.NEXTVAL
                                  INTO l_ledger_id
                                  FROM DUAL;
                                INSERT INTO LEDGER(
                                    LEDGER_ID,
                                    ENTRY_TYPE,
                                    TRANS_ID,
                                    PROCESS_FEE_ID,
                                    AMOUNT,
                                    ENTRY_DATE,
                                    BATCH_ID,
                                    SETTLE_STATE_ID,
                                    LEDGER_DATE,
                                    DESCRIPTION)
                                VALUES(
                                    l_ledger_id,
                                    'AD',
                                    l_trans_id,
                                    NULL,
                                    l_adj_amt,
                                    l_close_date,
                                    l_batch_id,
                                    2,
                                    l_settle_date,
                                    l_desc);
                                -- create net revenue adjustment, if any
                                INSERT INTO LEDGER(
                                    LEDGER_ID,
                                    RELATED_LEDGER_ID,
                                    ENTRY_TYPE,
                                    TRANS_ID,
                                    PROCESS_FEE_ID,
                                    SERVICE_FEE_ID,
                                    AMOUNT,
                                    ENTRY_DATE,
                                    BATCH_ID,
                                    SETTLE_STATE_ID,
                                    LEDGER_DATE,
                                    DESCRIPTION)
                                SELECT
                                    LEDGER_SEQ.NEXTVAL,
                                    l_ledger_id,
                                    'SF',
                                    l_trans_id,
                                    NULL,
                                    SF.SERVICE_FEE_ID,
                                    -l_adj_amt * SF.FEE_PERCENT,
                                    l_close_date,
                                    l_batch_id,
                                    2,
                                    l_settle_date,
                                    F.FEE_NAME
                                FROM SERVICE_FEES SF, FEES F
                               WHERE SF.FEE_ID = F.FEE_ID
                                 AND SF.TERMINAL_ID = l_prev_rec.TERMINAL_ID
                                 AND l_close_date BETWEEN NVL(SF.START_DATE, MIN_DATE)
                                      AND NVL(SF.END_DATE, MAX_DATE)
                                 AND SF.FREQUENCY_ID = 6;
                            END IF;
                            l_adjusted_new_cb := l_adjusted_new_cb OR l_adj_amt = 0 OR NVL(l_terminal_id, 0) = l_prev_rec.TERMINAL_ID; -- whether we adjust or not the new cb of the tran is appropriately dealt with
                        END IF;
                    --case: bank acct changed
                    ELSIF l_prev_rec.PAID_AMT <> 0 THEN
                        IF NVL(l_terminal_id, 0) <> l_prev_rec.TERMINAL_ID THEN
                            l_desc := 'Correction for a transaction that was assigned to the wrong location';
                        ELSIF NVL(l_currency_id, 0) <> NVL(l_prev_rec.CURRENCY_ID, 0) THEN
                            l_desc := 'Correction for a change in currency';
                        ELSE
                            l_desc := 'Correction for a transaction that was paid to the wrong bank account';
                        END IF;
                        -- deduct entire amt from old
                        l_batch_id := GET_OR_CREATE_BATCH(
                                l_prev_rec.TERMINAL_ID,
                                l_prev_rec.CUSTOMER_BANK_ID,
                                l_close_date,
                                l_prev_rec.CURRENCY_ID,
                                'A');
                        SELECT LEDGER_SEQ.NEXTVAL
                          INTO l_ledger_id
                          FROM DUAL;
                        INSERT INTO LEDGER(
                            LEDGER_ID,
                            ENTRY_TYPE,
                            TRANS_ID,
                            PROCESS_FEE_ID,
                            AMOUNT,
                            ENTRY_DATE,
                            BATCH_ID,
                            SETTLE_STATE_ID,
                            LEDGER_DATE,
                            DESCRIPTION)
                        VALUES(
                            l_ledger_id,
                            'AD',
                            l_trans_id,
                            NULL,
                            -l_prev_rec.PAID_AMT,
                            l_close_date,
                            l_batch_id,
                            l_settle_state_id,
                            l_settle_date,
                            l_desc);
                        -- create net revenue adjustment, if any
                        INSERT INTO LEDGER(
                            LEDGER_ID,
                            RELATED_LEDGER_ID,
                            ENTRY_TYPE,
                            TRANS_ID,
                            PROCESS_FEE_ID,
                            SERVICE_FEE_ID,
                            AMOUNT,
                            ENTRY_DATE,
                            BATCH_ID,
                            SETTLE_STATE_ID,
                            LEDGER_DATE,
                            DESCRIPTION)
                        SELECT
                            LEDGER_SEQ.NEXTVAL,
                            l_ledger_id,
                            'SF',
                            l_trans_id,
                            NULL,
                            SF.SERVICE_FEE_ID,
                            l_prev_rec.PAID_AMT * SF.FEE_PERCENT,
                            l_close_date,
                            l_batch_id,
                            l_settle_state_id,
                            l_settle_date,
                            F.FEE_NAME
                        FROM SERVICE_FEES SF, FEES F
                       WHERE SF.FEE_ID = F.FEE_ID
                         AND SF.TERMINAL_ID = l_prev_rec.TERMINAL_ID
                         AND l_close_date BETWEEN NVL(SF.START_DATE, MIN_DATE)
                              AND NVL(SF.END_DATE, MAX_DATE)
                         AND SF.FREQUENCY_ID = 6;
                    END IF;
                END LOOP;
                IF NOT l_adjusted_new_cb AND l_customer_bank_id IS NOT NULL THEN
                    l_batch_id := GET_OR_CREATE_BATCH(l_terminal_id,
                        l_customer_bank_id, l_close_date, l_currency_id, 'A');
                    INSERT_TRANS_TO_LEDGER(l_trans_id, l_trans_type_id,
                        l_close_date, l_settle_date, l_total_amount,
                        l_settle_state_id, l_process_fee_id, l_batch_id);
                END IF;
            END;
        ELSE -- an open batch exists
            -- To make it easy let's just delete what's there and re-insert
            --DELETE FROM LEDGER WHERE TRANS_ID = l_trans_id;
            UPDATE LEDGER SET DELETED = 'Y' WHERE TRANS_ID = l_trans_id;
            -- If terminal is null then no need to do anything more
            -- Added: if cust bank is null don't do any more (we may need to change this in the future which would also entail making BATCH.CUSTOMER_BANK_ID nullable)
            IF l_terminal_id IS NOT NULL AND l_customer_bank_id IS NOT NULL THEN
                INSERT_TRANS_TO_LEDGER(l_trans_id, l_trans_type_id, l_close_date, l_settle_date,
                    l_total_amount, l_settle_state_id, l_process_fee_id, l_terminal_id, l_customer_bank_id, l_currency_id);
            END IF;
        END IF;
    END;
    
    -- updates any fill-to-fill batch records upon the insertion of a record
    -- into the FILL table. Updates to the FILL table are NOT handled correctly
    -- by this procedure!
    PROCEDURE UPDATE_FILL_BATCH(
        l_fill_id   FILL.FILL_ID%TYPE)
    IS
        CURSOR l_batch_cur IS
            SELECT B.BATCH_ID, B.TERMINAL_ID, D.CUSTOMER_BANK_ID, D.CURRENCY_ID,
                   B.START_DATE, CASE WHEN BCOT.BATCH_CONFIRM_OPTION_TYPE_ID IS NULL THEN  'N' ELSE 'Y' END COUNTER_MUST_SHOW
              FROM CORP.BATCH B
              JOIN REPORT.TERMINAL_EPORT TE ON B.TERMINAL_ID = TE.TERMINAL_ID
              JOIN REPORT.FILL F ON TE.EPORT_ID = F.EPORT_ID
               AND F.FILL_DATE >= NVL(te.START_DATE, MIN_DATE)
               AND F.FILL_DATE < NVL(te.END_DATE, MAX_DATE)
              JOIN CORP.DOC D ON B.DOC_ID = D.DOC_ID
              JOIN REPORT.TERMINAL T ON B.TERMINAL_ID = T.TERMINAL_ID
              LEFT OUTER JOIN (CORP.BATCH_CONFIRM BC
              JOIN CORP.BATCH_CONFIRM_OPTION BCO
                ON BC.BATCH_CONFIRM_ID = BCO.BATCH_CONFIRM_ID
              JOIN CORP.BATCH_CONFIRM_OPTION_TYPE BCOT ON BCO.BATCH_CONFIRM_OPTION_TYPE_ID = BCOT.BATCH_CONFIRM_OPTION_TYPE_ID
               AND BCOT.BATCH_CONFIRM_OPTION_TYPE_CD = 'COUNTER_MUST_SHOW'
              ) ON BC.CUSTOMER_ID = T.CUSTOMER_ID
               AND BC.PAYMENT_SCHEDULE_ID = B.PAYMENT_SCHEDULE_ID
             WHERE B.START_DATE < F.FILL_DATE
               AND NVL(B.END_DATE, MAX_DATE) > F.FILL_DATE
               AND B.BATCH_STATE_CD = 'O'
               AND B.PAYMENT_SCHEDULE_ID = 2
               AND (F.COUNTERS_DISPLAYED_FLAG = 'Y' OR BCOT.BATCH_CONFIRM_OPTION_TYPE_ID IS NULL)
               AND F.FILL_ID = l_fill_id;
        l_end_date CORP.BATCH.END_DATE%TYPE;
        l_end_fill_id CORP.BATCH_FILL.END_FILL_ID%TYPE;
        l_lock VARCHAR2(128);
    BEGIN
        FOR l_batch_rec IN l_batch_cur LOOP
            SELECT MIN(FILL_DATE), MIN(FILL_ID)
                  INTO l_end_date, l_end_fill_id
                  FROM (
                    SELECT f.FILL_DATE, f.FILL_ID
                      FROM REPORT.FILL f, REPORT.TERMINAL_EPORT te
                     WHERE f.FILL_DATE > l_batch_rec.START_DATE
                       AND f.EPORT_ID = te.EPORT_ID
                       AND f.FILL_DATE >= NVL(te.START_DATE, MIN_DATE)
                       AND f.FILL_DATE < NVL(te.END_DATE, MAX_DATE)
                       AND te.TERMINAL_ID = l_batch_rec.TERMINAL_ID
                       AND (F.COUNTERS_DISPLAYED_FLAG = 'Y' OR l_batch_rec.COUNTER_MUST_SHOW = 'N')
                     ORDER BY f.FILL_DATE ASC)
                 WHERE ROWNUM = 1;
            l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_batch_rec.CUSTOMER_BANK_ID);
            -- it doesn't matter if the batch's doc has changed, because we assume the batch is in an open doc
            -- update batch record
            UPDATE CORP.BATCH B SET B.END_DATE = l_end_date
             WHERE B.BATCH_ID = l_batch_rec.BATCH_ID;
            UPDATE CORP.BATCH_FILL BF SET BF.END_FILL_ID = l_end_fill_id
             WHERE BF.BATCH_ID = l_batch_rec.BATCH_ID;

            -- also update ledger.batch_id of entries that are after fill_date
            DECLARE
                CURSOR l_cur IS
                  SELECT L.LEDGER_ID, L.ENTRY_DATE
                    FROM LEDGER L
                   WHERE L.BATCH_ID = l_batch_rec.BATCH_ID
                     AND L.ENTRY_DATE >= l_end_date;
                TYPE t_rec_list IS TABLE OF l_cur%ROWTYPE;
                l_recs t_rec_list;
                l_new_batch_id LEDGER.BATCH_ID%TYPE;
            BEGIN
                OPEN l_cur;
                FETCH l_cur BULK COLLECT INTO l_recs;
                CLOSE l_cur;

                IF l_recs.FIRST IS NOT NULL THEN
                    FOR i IN l_recs.FIRST .. l_recs.LAST LOOP
                        l_new_batch_id := GET_OR_CREATE_BATCH(
                            l_batch_rec.TERMINAL_ID,
                            l_batch_rec.CUSTOMER_BANK_ID,
                            l_recs(i).ENTRY_DATE,
                            l_batch_rec.CURRENCY_ID,
                            'N');
                        UPDATE LEDGER L
                           SET L.BATCH_ID = l_new_batch_id
                         WHERE L.LEDGER_ID = l_recs(i).LEDGER_ID;
                    END LOOP;
                END IF;
            END;
            GLOBALS_PKG.RELEASE_LOCK(l_lock);
            COMMIT;
            
            -- Now check batch complete
            CHECK_FILL_BATCH_COMPLETE(l_batch_rec.BATCH_ID);
            COMMIT;
         END LOOP;
    END;
    
    PROCEDURE UPDATE_EXPORT_BATCH(
        l_export_id   REPORT.EXPORT_BATCH.BATCH_ID%TYPE)
    IS
        CURSOR l_batch_cur IS
            SELECT DISTINCT B.BATCH_ID, B.TERMINAL_ID, D.CUSTOMER_BANK_ID, D.CURRENCY_ID, B.START_DATE, EX.TRAN_CREATE_DT_END NEW_END_DATE
              FROM CORP.BATCH B
             INNER JOIN CORP.DOC D ON B.DOC_ID = D.DOC_ID
             INNER JOIN CORP.CUSTOMER_BANK CB ON D.CUSTOMER_BANK_ID = CB.CUSTOMER_BANK_ID
             /*
             INNER JOIN REPORT.TERMINAL TERMINAL_ID ON B.TERMINAL_ID = T.TERMINAL_ID
             INNER JOIN FRONT.VW_LOCATION_HIERARCHY H ON T.LOCATION_ID = H.DESCENDANT_LOCATION_ID
             */
             INNER JOIN REPORT.EXPORT_BATCH EX ON CB.CUSTOMER_ID = EX.CUSTOMER_ID /* AND EX.LOCATION_ID = H.ANCESTOR_LOCATION_ID */
             WHERE B.BATCH_STATE_CD = 'O'
               AND B.END_DATE IS NULL
               AND B.PAYMENT_SCHEDULE_ID = 8
               AND EX.BATCH_ID = l_export_id;
        l_lock VARCHAR2(128);
    BEGIN
        FOR l_batch_rec IN l_batch_cur LOOP
            l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_batch_rec.CUSTOMER_BANK_ID);
            -- it doesn't matter if the batch's doc has changed, because we assume the batch is in an open doc
            -- update batch record
            UPDATE BATCH B
               SET B.END_DATE = l_batch_rec.new_end_date,
                   B.BATCH_STATE_CD = (SELECT DECODE(COUNT(*), 0, 'D', 'U')
                      FROM REPORT.TERMINAL t
                      JOIN CORP.BATCH_CONFIRM CN ON t.CUSTOMER_ID = cn.CUSTOMER_ID
                     WHERE b.TERMINAL_ID = t.TERMINAL_ID
                       AND b.PAYMENT_SCHEDULE_ID = cn.PAYMENT_SCHEDULE_ID),
                   B.BATCH_CLOSED_TS = SYSDATE
             WHERE B.BATCH_ID = l_batch_rec.BATCH_ID;

            -- also update ledger.batch_id of entries that are not exported
            DECLARE
                CURSOR l_cur IS
                  SELECT L.LEDGER_ID, L.ENTRY_DATE
                    FROM LEDGER L
                   WHERE L.BATCH_ID = l_batch_rec.BATCH_ID
                     AND (L.ENTRY_DATE > l_batch_rec.new_end_date
                         OR NOT EXISTS(SELECT 1 FROM REPORT.ACTIVITY_REF A
                              INNER JOIN REPORT.EXPORT_BATCH EB ON A.BATCH_ID = EB.BATCH_ID
                              WHERE L.TRANS_ID = A.TRAN_ID AND EB.STATUS IN('A','S')));
                TYPE t_rec_list IS TABLE OF l_cur%ROWTYPE;
                l_recs t_rec_list;
                l_new_batch_id LEDGER.BATCH_ID%TYPE;
            BEGIN
                OPEN l_cur;
                FETCH l_cur BULK COLLECT INTO l_recs;
                CLOSE l_cur;

                IF l_recs.FIRST IS NOT NULL THEN
                    FOR i IN l_recs.FIRST .. l_recs.LAST LOOP
                        l_new_batch_id := GET_OR_CREATE_BATCH(
                            l_batch_rec.TERMINAL_ID,
                            l_batch_rec.CUSTOMER_BANK_ID,
                            l_recs(i).ENTRY_DATE,
                            l_batch_rec.CURRENCY_ID,
                            'N');
                        UPDATE LEDGER L
                           SET L.BATCH_ID = l_new_batch_id
                         WHERE L.LEDGER_ID = l_recs(i).LEDGER_ID;
                    END LOOP;
                END IF;
            END;
         END LOOP;
    END;
/* not doing this yet
    PROCEDURE AUTO_CREATE_DOCS.
    IS
       CURSOR c_docs IS
             SELECT cb.customer_bank_id
              FROM (SELECT l.CUSTOMER_BANK_ID, l.TRANS_TYPE_ID, SUM(l.TOTAL_AMOUNT) TOTAL_AMOUNT,
                           SUM(l.TOTAL_AMOUNT * l.FEE_PERCENT + l.FEE_AMOUNT) FEE_TOTAL
                      FROM LEDGER l, TERMINAL t
                     WHERE l.PAID_STATUS = 'U'
                       AND l.TERMINAL_ID = t.TERMINAL_ID
                       AND t.PAYMENT_SCHEDULE_ID <> 1
                     GROUP BY l.CUSTOMER_BANK_ID, l.TRANS_TYPE_ID) lp,
                     CUSTOMER_BANK cb
              WHERE cb.CUSTOMER_BANK_ID = lp.CUSTOMER_BANK_ID
              GROUP BY cb.CUSTOMER_BANK_ID, cb.PAY_MIN_AMOUNT
              HAVING SUM(CASE TRANS_TYPE_ID
                      WHEN 16 THEN 1
                      WHEN 19 THEN 1
                      WHEN 20 THEN -1
                      WHEN 21 THEN -1
                      ELSE NULL
                   END * TOTAL_AMOUNT) - SUM (lp.FEE_TOTAL) > cb.PAY_MIN_AMOUNT;
    BEGIN
        -- determine each customer bank that whose total eft will exceed the min_eft_amt
        FOR r_docs IN c_docs LOOP
            BEGIN
                CREATE_DOC(r_efts.customer_bank_id, NULL, l_type,'Y');
                COMMIT;
            EXCEPTION
                WHEN OTHERS THEN
                    IF SQLCODE = -20910 THEN
                        ROLLBACK;
                    ELSE
                        RAISE;
                    END IF;
            END;
        END LOOP;
    END;

*/

    FUNCTION GET_UNPAID_TRANS_AND_FEES (
        l_as_of DATE
    ) RETURN GLOBALS_PKG.REF_CURSOR
    IS
        cur GLOBALS_PKG.REF_CURSOR;
    BEGIN
       OPEN cur FOR
            SELECT CUSTOMER_NAME, CB.CUSTOMER_ID, CB.CUSTOMER_BANK_ID,
                   BANK_ACCT_NBR, BANK_ROUTING_NBR, PAY_MIN_AMOUNT, CREDIT_AMOUNT,
                   REFUND_AMOUNT, CHARGEBACK_AMOUNT, PROCESS_FEE_AMOUNT,
                   SERVICE_FEE_AMOUNT, ADJUST_AMOUNT
                   FROM CUSTOMER C, CUSTOMER_BANK CB, (
                SELECT CUSTOMER_BANK_ID,
                       SUM(DECODE(ENTRY_TYPE, 'CC', AMOUNT, NULL)) CREDIT_AMOUNT,
                       SUM(DECODE(ENTRY_TYPE, 'RF', AMOUNT, NULL)) REFUND_AMOUNT,
                       SUM(DECODE(ENTRY_TYPE, 'CB', AMOUNT, NULL)) CHARGEBACK_AMOUNT,
                       SUM(DECODE(ENTRY_TYPE, 'PF', AMOUNT, NULL)) PROCESS_FEE_AMOUNT,
                       SUM(DECODE(ENTRY_TYPE, 'SF', AMOUNT, NULL)) SERVICE_FEE_AMOUNT,
                       SUM(DECODE(ENTRY_TYPE, 'AD', AMOUNT, NULL)) ADJUST_AMOUNT
                  FROM (
                    SELECT D.CUSTOMER_BANK_ID, L.ENTRY_TYPE, SUM(L.AMOUNT) AMOUNT
                      FROM LEDGER L, BATCH B, DOC D
                      WHERE L.BATCH_ID = B.BATCH_ID
                      AND L.DELETED = 'N'
                      AND PAYMENTS_PKG.ENTRY_PAYABLE(L.SETTLE_STATE_ID,L.ENTRY_TYPE) = 'Y'
                      AND B.DOC_ID = D.DOC_ID
                      AND NVL(L.PAID_DATE, MIN_DATE) < l_as_of
                      AND L.LEDGER_DATE < l_as_of
                      GROUP BY D.CUSTOMER_BANK_ID, L.ENTRY_TYPE) M
                  GROUP BY CUSTOMER_BANK_ID) A
            WHERE CB.CUSTOMER_BANK_ID = A.CUSTOMER_BANK_ID (+)
            AND CB.CUSTOMER_ID = C.CUSTOMER_ID
            ORDER BY UPPER(CUSTOMER_NAME), BANK_ACCT_NBR;
        RETURN cur;
    END;
    
    FUNCTION FORMAT_EFT_REASON(
        l_payment_sched_id BATCH.PAYMENT_SCHEDULE_ID%TYPE,
        l_min_pay_date BATCH.START_DATE%TYPE,
        l_max_pay_date BATCH.END_DATE%TYPE
     ) RETURN VARCHAR
    IS
        l_text VARCHAR(8000);
    BEGIN
        SELECT DESCRIPTION
          INTO l_text
          FROM PAYMENT_SCHEDULE
         WHERE PAYMENT_SCHEDULE_ID = l_payment_sched_id;
        IF l_min_pay_date = l_max_pay_date THEN
            l_text := l_text || ' on ' || TO_CHAR(l_min_pay_date, 'MM-DD-YYYY');
        ELSE
            l_text := l_text || ' from ' || TO_CHAR(l_min_pay_date, 'MM-DD-YYYY')
                || ' to ' || TO_CHAR(l_max_pay_date, 'MM-DD-YYYY');
        END IF;
        RETURN l_text;
    END;
    /*
    FUNCTION FORMAT_EFT_REASON(
        l_eft_id EFT.EFT_ID%TYPE
     ) RETURN VARCHAR
    IS
        l_text VARCHAR(8000) := '';
        CURSOR l_cur IS
            SELECT PAYMENT_SCHEDULE_ID, MIN(PAYMENT_SCHEDULE_DATE) MIN_PAY_DATE,
                   MAX(PAYMENT_SCHEDULE_DATE) MAX_PAY_DATE
            FROM PAYMENTS P
            WHERE P.EFT_ID = l_eft_id
            GROUP BY PAYMENT_SCHEDULE_ID;
    BEGIN
        FOR l_rec IN l_cur LOOP
            IF LENGTH(l_text) > 0 THEN
                l_text := l_text || ' and ';
            END IF;
            l_text := l_text || FORMAT_EFT_REASON(l_rec.PAYMENT_SCHEDULE_ID, l_rec.MIN_PAY_DATE, l_rec.MAX_PAY_DATE);
        END LOOP;
        RETURN l_text;
    END;
    */

    -- Puts ledger record into a new batch
    PROCEDURE      DELAY_ENTRY (
      l_ledger_id IN LEDGER.LEDGER_ID%TYPE
      )
    IS
      l_status DOC.STATUS%TYPE;
      l_batch_id LEDGER.BATCH_ID%TYPE;
      l_entry_date LEDGER.ENTRY_DATE%TYPE;
      l_new_batch_id LEDGER.BATCH_ID%TYPE;
    BEGIN
        l_status := FREEZE_DOC_STATUS_LEDGER_ID(l_ledger_id);
        IF l_status IN('O') THEN
    		 RAISE_APPLICATION_ERROR(-20101, 'The Document must be locked before this entry can be pulled out of it');
        ELSIF l_status NOT IN('L') THEN
             RAISE_APPLICATION_ERROR(-20142, 'This entry belongs to a document that is already approved.');
        END IF;
        -- put in new batch
        SELECT L.BATCH_ID, L.ENTRY_DATE
          INTO l_batch_id, l_entry_date
          FROM LEDGER L
         WHERE LEDGER_ID = l_ledger_id;
        l_new_batch_id := GET_NEW_BATCH(l_batch_id, l_entry_date);
        UPDATE LEDGER L
           SET BATCH_ID = l_new_batch_id
         WHERE LEDGER_ID = l_ledger_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
    		 RAISE_APPLICATION_ERROR(-20100, 'Entry not found');
    	WHEN OTHERS THEN
    		 RAISE;
    END;
    
    -- Marks ledger record as deleted
    PROCEDURE      DELETE_ENTRY (
      l_ledger_id IN LEDGER.LEDGER_ID%TYPE)
    IS
      l_status DOC.STATUS%TYPE;
    BEGIN
        l_status := FREEZE_DOC_STATUS_LEDGER_ID(l_ledger_id);
        IF l_status IN('O') THEN
    		 NULL; --RAISE_APPLICATION_ERROR(-20101, 'The Document must be locked before this entry can be pulled out of it');
        ELSIF l_status NOT IN('L') THEN
             RAISE_APPLICATION_ERROR(-20142, 'This entry belongs to a document that is already approved.');
        END IF;
        -- mark as deleted (remove net revenue fees also)
        UPDATE LEDGER L
           SET DELETED = 'Y'
         WHERE LEDGER_ID = l_ledger_id
            OR RELATED_LEDGER_ID = l_ledger_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
    		 RAISE_APPLICATION_ERROR(-20100, 'Entry not found');
    	WHEN OTHERS THEN
    		 RAISE;
    END;

    -- Puts refund into a new batch
    PROCEDURE      DELAY_REFUND_BY_TRAN_ID (
      l_trans_id IN LEDGER.TRANS_ID%TYPE)
    IS
      l_ledger_id LEDGER.LEDGER_ID%TYPE;
    BEGIN
    	 SELECT LEDGER_ID
           INTO l_ledger_id
           FROM LEDGER
          WHERE TRANS_ID = l_trans_id
            AND ENTRY_TYPE = 'RF';
          DELAY_ENTRY(l_ledger_id);
    END;
    
    PROCEDURE ADJUSTMENT_INS (
        l_ledger_id OUT NOCOPY LEDGER.LEDGER_ID%TYPE,
       	l_doc_id IN BATCH.DOC_ID%TYPE,
        l_terminal_id IN BATCH.TERMINAL_ID%TYPE,
    	l_reason IN LEDGER.DESCRIPTION%TYPE,
    	l_amt IN LEDGER.AMOUNT%TYPE,
    	l_user_id IN LEDGER.CREATE_BY%TYPE,
		lc_split_payment_flag IN CHAR,
		lc_split_payment_interval_cd IN CHAR,
		ln_split_number_of_payments IN INTEGER,
		ld_first_split_payment_date IN DATE
	)
    IS
        l_batch_id LEDGER.BATCH_ID%TYPE;
        l_curr_id DOC.CURRENCY_ID%TYPE;
        l_term_curr_id DOC.CURRENCY_ID%TYPE;
        l_fee_date LEDGER.ENTRY_DATE%TYPE;
        l_customer_bank_id CORP.DOC.CUSTOMER_BANK_ID%TYPE;
        l_status DOC.STATUS%TYPE;
        l_lock VARCHAR2(128);
		ln_count INTEGER;
		ln_split_payment_amt LEDGER.AMOUNT%TYPE;
		ld_payment_date BATCH.END_DATE%TYPE;
    BEGIN
        SELECT D.CUSTOMER_BANK_ID, D.CURRENCY_ID
          INTO l_customer_bank_id, l_curr_id
          FROM CORP.DOC D
         WHERE D.DOC_ID = l_doc_id;
		 
		IF l_terminal_id IS NOT NULL THEN
			SELECT FEE_CURRENCY_ID
			INTO l_term_curr_id
			FROM REPORT.TERMINAL
			WHERE TERMINAL_ID = l_terminal_id;
			
            -- ensure that currency matches
            IF NVL(l_curr_id, 0) <> l_term_curr_id THEN
                RAISE_APPLICATION_ERROR(-20020, 'Terminal uses a different currency than the doc''s currency. Adjustment not added');
            END IF;
		END IF;
		 
        l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_customer_bank_id);
        l_status := GET_DOC_STATUS(l_doc_id);
        IF l_status NOT IN('O', 'L') THEN
            RAISE_APPLICATION_ERROR(-20021, 'This doc must be open or locked to add adjustments. Adjustment not added');
    	END IF;
		
		IF lc_split_payment_flag = 'Y' AND lc_split_payment_interval_cd IN ('D', 'W', 'M') AND ln_split_number_of_payments BETWEEN 2 AND 999 AND TRUNC(ld_first_split_payment_date) >= TRUNC(SYSDATE) THEN
			ln_split_payment_amt := ROUND(l_amt / ln_split_number_of_payments, 2);
			ln_count := 0;
			ld_payment_date := TRUNC(ld_first_split_payment_date);
			LOOP
				ln_count := ln_count + 1;
				IF ln_count >= ln_split_number_of_payments THEN
					ln_split_payment_amt := l_amt - ln_split_payment_amt * (ln_count - 1);
				END IF;
				
				SELECT MAX(B.BATCH_ID)
				  INTO l_batch_id
				  FROM BATCH B
				 WHERE B.DOC_ID = l_doc_id
				   AND NVL(B.TERMINAL_ID, 0) = NVL(l_terminal_id, 0)
				   AND B.PAYMENT_SCHEDULE_ID = 9
				   AND B.END_DATE = ld_payment_date;
			
				IF l_batch_id IS NULL THEN
					-- create a batch and set the doc id
					SELECT BATCH_SEQ.NEXTVAL
					  INTO l_batch_id
					  FROM DUAL;
					INSERT INTO BATCH(
						BATCH_ID,
						DOC_ID,
						TERMINAL_ID,
						PAYMENT_SCHEDULE_ID,
						START_DATE,
						END_DATE,
						BATCH_STATE_CD)
					  VALUES(
						l_batch_id,
						l_doc_id,
						DECODE(l_terminal_id, 0, NULL, l_terminal_id),
						9,
						ld_payment_date,
						ld_payment_date,
						'O');
				END IF;
				SELECT LEDGER_SEQ.NEXTVAL
				  INTO l_ledger_id
				  FROM DUAL;
				INSERT INTO LEDGER(
					LEDGER_ID,
					ENTRY_TYPE,
					AMOUNT,
					ENTRY_DATE,
					BATCH_ID,
					SETTLE_STATE_ID,
					LEDGER_DATE,
					DESCRIPTION,
					CREATE_BY,
					REPORT_DATE)
				   VALUES (
					l_ledger_id,
					'AD',
					ln_split_payment_amt,
					ld_payment_date,
					l_batch_id,
					2 /*process-no require*/,
					ld_payment_date,
					l_reason,
					l_user_id,
					ld_payment_date);
				-- create net revenue fee also
				INSERT INTO LEDGER(
					LEDGER_ID,
					RELATED_LEDGER_ID,
					SERVICE_FEE_ID,
					ENTRY_TYPE,
					AMOUNT,
					ENTRY_DATE,
					BATCH_ID,
					SETTLE_STATE_ID,
					LEDGER_DATE,
					DESCRIPTION,
					REPORT_DATE)
				 SELECT
					LEDGER_SEQ.NEXTVAL,
					l_ledger_id,
					SF.SERVICE_FEE_ID,
					'SF',
					-ln_split_payment_amt * SF.FEE_PERCENT,
					ld_payment_date,
					l_batch_id,
					2 /*process-no require*/,
					ld_payment_date,
					F.FEE_NAME,
					ld_payment_date
				  FROM SERVICE_FEES SF, FEES F
				 WHERE SF.FEE_ID = F.FEE_ID
				   AND SF.TERMINAL_ID = l_terminal_id
				   AND ld_payment_date BETWEEN NVL(SF.START_DATE, MIN_DATE)
						  AND NVL(SF.END_DATE, MAX_DATE)
				   AND SF.FREQUENCY_ID = 6;
				   
				IF ln_count >= ln_split_number_of_payments THEN
					EXIT;
				END IF;
				
				IF lc_split_payment_interval_cd = 'D' THEN
					ld_payment_date := ld_payment_date + 1;
				ELSIF lc_split_payment_interval_cd = 'W' THEN
					ld_payment_date := ld_payment_date + 7;
				ELSIF lc_split_payment_interval_cd = 'M' THEN
					ld_payment_date := ADD_MONTHS(ld_payment_date, 1);
				END IF;
			END LOOP;
		ELSE
			SELECT MAX(B.BATCH_ID)
			  INTO l_batch_id
			  FROM BATCH B
			 WHERE B.DOC_ID = l_doc_id
			   AND NVL(B.TERMINAL_ID, 0) = NVL(l_terminal_id, 0)
			   AND B.PAYMENT_SCHEDULE_ID = 1;
		
			IF l_batch_id IS NULL THEN
				-- create a batch and set the doc id
				SELECT BATCH_SEQ.NEXTVAL
				  INTO l_batch_id
				  FROM DUAL;
				INSERT INTO BATCH(
					BATCH_ID,
					DOC_ID,
					TERMINAL_ID,
					PAYMENT_SCHEDULE_ID,
					START_DATE,
					END_DATE,
					BATCH_STATE_CD)
				  VALUES(
					l_batch_id,
					l_doc_id,
					DECODE(l_terminal_id, 0, NULL, l_terminal_id),
					1,
					SYSDATE,
					DECODE(l_status, 'L', SYSDATE, 'O', null),
					DECODE(l_status, 'L', 'F', 'O', 'L'));
			END IF;
			SELECT LEDGER_SEQ.NEXTVAL, SYSDATE
			  INTO l_ledger_id, l_fee_date
			  FROM DUAL;
			INSERT INTO LEDGER(
				LEDGER_ID,
				ENTRY_TYPE,
				AMOUNT,
				ENTRY_DATE,
				BATCH_ID,
				SETTLE_STATE_ID,
				LEDGER_DATE,
				DESCRIPTION,
				CREATE_BY)
			   VALUES (
				l_ledger_id,
				'AD',
				l_amt,
				l_fee_date,
				l_batch_id,
				2 /*process-no require*/,
				l_fee_date,
				l_reason,
				l_user_id);
			-- create net revenue fee also
			INSERT INTO LEDGER(
				LEDGER_ID,
				RELATED_LEDGER_ID,
				SERVICE_FEE_ID,
				ENTRY_TYPE,
				AMOUNT,
				ENTRY_DATE,
				BATCH_ID,
				SETTLE_STATE_ID,
				LEDGER_DATE,
				DESCRIPTION)
			 SELECT
				LEDGER_SEQ.NEXTVAL,
				l_ledger_id,
				SF.SERVICE_FEE_ID,
				'SF',
				-l_amt * SF.FEE_PERCENT,
				l_fee_date,
				l_batch_id,
				2 /*process-no require*/,
				l_fee_date,
				F.FEE_NAME
			  FROM SERVICE_FEES SF, FEES F
			 WHERE SF.FEE_ID = F.FEE_ID
			   AND SF.TERMINAL_ID = l_terminal_id
			   AND l_fee_date BETWEEN NVL(SF.START_DATE, MIN_DATE)
					  AND NVL(SF.END_DATE, MAX_DATE)
			   AND SF.FREQUENCY_ID = 6;
		END IF;
    END;
	
PROCEDURE ADJUSTMENT_INS (
		l_ledger_id OUT NOCOPY LEDGER.LEDGER_ID%TYPE,
       	l_doc_id IN BATCH.DOC_ID%TYPE,
        l_terminal_id IN BATCH.TERMINAL_ID%TYPE,
    	l_reason IN LEDGER.DESCRIPTION%TYPE,
    	l_amt IN LEDGER.AMOUNT%TYPE,
    	l_user_id IN LEDGER.CREATE_BY%TYPE
) IS
BEGIN
	ADJUSTMENT_INS(l_ledger_id, l_doc_id, l_terminal_id, l_reason, l_amt, l_user_id, 'N', NULL, NULL, NULL);
END;
	
PROCEDURE ADJUSTMENT_INS (
	pn_customer_id CORP.CUSTOMER.CUSTOMER_ID%TYPE,
	pv_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
	pv_currency_cd CORP.CURRENCY.CURRENCY_CODE%TYPE,
	pv_reason CORP.LEDGER.DESCRIPTION%TYPE,
	pn_amount CORP.LEDGER.AMOUNT%TYPE,
	pn_doc_id OUT CORP.DOC.DOC_ID%TYPE,
	pn_ledger_id OUT CORP.LEDGER.LEDGER_ID%TYPE
) IS
	ln_currency_id CORP.CURRENCY.CURRENCY_ID%TYPE;
	ln_customer_bank_id CORP.CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE;
	ln_business_unit_id CORP.BUSINESS_UNIT.BUSINESS_UNIT_ID%TYPE;
	ln_user_id REPORT.USER_LOGIN.USER_ID%TYPE;
BEGIN
	SELECT MAX(customer_bank_id) INTO ln_customer_bank_id
	FROM (
		SELECT cbt.customer_bank_id
		FROM corp.customer_bank_terminal cbt
		JOIN report.terminal t ON cbt.terminal_id = t.terminal_id
		WHERE t.customer_id = pn_customer_id AND NVL(cbt.start_date, MIN_DATE) <= SYSDATE AND NVL(cbt.end_date, MAX_DATE) > SYSDATE
		GROUP BY cbt.customer_bank_id
		ORDER BY COUNT(1) DESC
	) WHERE ROWNUM = 1;
	
	IF ln_customer_bank_id IS NULL THEN
		RAISE_APPLICATION_ERROR(-20703, 'Unable to find a customer bank account for this customer');
	END IF;
	
	SELECT MAX(business_unit_id) INTO ln_business_unit_id
	FROM (
		SELECT business_unit_id
		FROM report.terminal
		WHERE customer_id = pn_customer_id AND status != 'D'
		GROUP BY business_unit_id
		ORDER BY COUNT(1) DESC
	) WHERE ROWNUM = 1;
	
	IF ln_business_unit_id IS NULL THEN
		RAISE_APPLICATION_ERROR(-20704, 'Unable to find a terminal business unit for this customer');
	END IF;
	
	SELECT MAX(user_id) INTO ln_user_id
	FROM report.user_login
	WHERE user_name = pv_user_name;
	
	SELECT currency_id
	INTO ln_currency_id
	FROM corp.currency
	WHERE currency_code = pv_currency_cd;
	
	pn_doc_id := GET_OR_CREATE_DOC(ln_customer_bank_id, ln_currency_id, ln_business_unit_id);
	ADJUSTMENT_INS(pn_ledger_id, pn_doc_id, NULL, pv_reason, pn_amount, ln_user_id);
END;	
    
    PROCEDURE                             ADJUSTMENT_UPD
       (l_ledger_id IN LEDGER.LEDGER_ID%TYPE,
       	l_reason IN LEDGER.DESCRIPTION%TYPE,
       	l_amt IN LEDGER.AMOUNT%TYPE,
    	l_user_id IN LEDGER.CREATE_BY%TYPE)
    IS
        l_status CORP.DOC.STATUS%TYPE;
    BEGIN
  	    l_status := FREEZE_DOC_STATUS_LEDGER_ID(l_ledger_id);
        IF l_status NOT IN('O', 'L') THEN
            RAISE_APPLICATION_ERROR(-20021, 'This doc must be open or locked to update adjustments. Adjustment not updated');
    	END IF;
        UPDATE LEDGER SET DESCRIPTION = l_reason, AMOUNT = l_amt, CREATE_BY = l_user_id
          WHERE LEDGER_ID = l_ledger_id;
         -- update net revenue
         UPDATE LEDGER L SET AMOUNT = -l_amt * (
            SELECT SF.FEE_PERCENT
              FROM SERVICE_FEES SF
             WHERE L.SERVICE_FEE_ID = SF.SERVICE_FEE_ID
               AND SF.FREQUENCY_ID = 6)
          WHERE L.RELATED_LEDGER_ID = l_ledger_id
            AND L.ENTRY_TYPE = 'SF'
            AND EXISTS(SELECT 1
                FROM SERVICE_FEES SF
                WHERE L.SERVICE_FEE_ID = SF.SERVICE_FEE_ID
                AND SF.FREQUENCY_ID = 6
            );
    END;

    PROCEDURE LOCK_DOC(
        l_doc_id DOC.DOC_ID%TYPE,
        l_user_id IN DOC.CREATE_BY%TYPE)
    IS
    BEGIN
        IF FREEZE_DOC_STATUS_DOC_ID(l_doc_id) = 'O' THEN
            UPDATE DOC
               SET STATUS = 'L', UPDATE_DATE = SYSDATE
             WHERE DOC_ID = l_doc_id;

            --Close all closeable batches
            UPDATE BATCH B
               SET B.BATCH_STATE_CD = 'F',
                   B.BATCH_CLOSED_TS = SYSDATE,
                   B.END_DATE = NVL(B.END_DATE, SYSDATE)
             WHERE B.DOC_ID = l_doc_id
               AND B.BATCH_STATE_CD = 'L';

            UPDATE BATCH B
               SET B.BATCH_STATE_CD = (SELECT DECODE(COUNT(*), 0, 'D', 'U')
                      FROM REPORT.TERMINAL t
                      JOIN CORP.BATCH_CONFIRM CN ON t.CUSTOMER_ID = cn.CUSTOMER_ID
                     WHERE b.TERMINAL_ID = t.TERMINAL_ID
                       AND b.PAYMENT_SCHEDULE_ID = cn.PAYMENT_SCHEDULE_ID),
                   B.BATCH_CLOSED_TS = SYSDATE
             WHERE B.DOC_ID = l_doc_id
               AND B.BATCH_STATE_CD = 'O'
               AND B.PAYMENT_SCHEDULE_ID NOT IN(1,2,5,7,8)
               AND B.END_DATE <= SYSDATE;

            -- Remove unclosed batches
            DECLARE
                l_cnt PLS_INTEGER;
                l_cb_id DOC.CUSTOMER_BANK_ID%TYPE;
                l_cur_id DOC.CURRENCY_ID%TYPE;
                l_bu_id DOC.BUSINESS_UNIT_ID%TYPE;
                l_new_doc_id DOC.DOC_ID%TYPE;
            BEGIN
                SELECT COUNT(*)
                  INTO l_cnt
                  FROM BATCH B
                 WHERE B.DOC_ID = l_doc_id
                   AND BATCH_CLOSABLE(B.BATCH_ID, B.PAYMENT_SCHEDULE_ID, B.END_DATE, B.BATCH_STATE_CD) = 'N';
                IF l_cnt > 0 THEN
                    SELECT D.CUSTOMER_BANK_ID, D.CURRENCY_ID, D.BUSINESS_UNIT_ID
                      INTO l_cb_id, l_cur_id, l_bu_id
                      FROM DOC D
                    WHERE D.DOC_ID = l_doc_id;
                    l_new_doc_id := GET_OR_CREATE_DOC(l_cb_id, l_cur_id, l_bu_id);
                   UPDATE BATCH B
                       SET B.DOC_ID = l_new_doc_id
                     WHERE B.DOC_ID = l_doc_id
                       AND BATCH_CLOSABLE(B.BATCH_ID, B.PAYMENT_SCHEDULE_ID, B.END_DATE, B.BATCH_STATE_CD) = 'N';
                END IF;
            END;
            
            -- Remove any unsettled trans from as batches
            DECLARE
                CURSOR l_cur IS
                  SELECT L.LEDGER_ID, L.BATCH_ID, L.ENTRY_DATE
                    FROM LEDGER L
                   WHERE ENTRY_PAYABLE(L.SETTLE_STATE_ID, L.ENTRY_TYPE) = '?'
                     AND L.BATCH_ID IN(
                            SELECT B.BATCH_ID
                              FROM BATCH B
                             WHERE B.DOC_ID = l_doc_id);
                TYPE t_rec_list IS TABLE OF l_cur%ROWTYPE;
                l_recs t_rec_list;
                l_new_batch_id LEDGER.BATCH_ID%TYPE;
            BEGIN
                OPEN l_cur;
                FETCH l_cur BULK COLLECT INTO l_recs;
                CLOSE l_cur;

                IF l_recs.FIRST IS NOT NULL THEN
                    FOR i IN l_recs.FIRST .. l_recs.LAST LOOP
                        l_new_batch_id := GET_NEW_BATCH(l_recs(i).BATCH_ID, l_recs(i).ENTRY_DATE);
                        UPDATE LEDGER L
                           SET L.BATCH_ID = l_new_batch_id
                         WHERE L.LEDGER_ID = l_recs(i).LEDGER_ID;
                    END LOOP;
                END IF;
            END;

            -- add rounding entries
            ADD_BATCH_ROUNDING_ENTRIES(l_doc_id);
            ADD_ROUNDING_ENTRY(l_doc_id);
        END IF;
    END;

    PROCEDURE      APPROVE_PAYMENT (
      l_doc_id IN DOC.DOC_ID%TYPE,
      l_user_id IN DOC.APPROVE_BY%TYPE)
    IS
      l_total DOC.TOTAL_AMOUNT%TYPE;
    BEGIN
        IF FREEZE_DOC_STATUS_DOC_ID(l_doc_id) = 'L' THEN
            -- add Positive Net Revenue Adjustment, if necessary
            DECLARE
                l_nrf_amt LEDGER.AMOUNT%TYPE;
                l_fee_date LEDGER.ENTRY_DATE%TYPE;
                l_ledger_id LEDGER.LEDGER_ID%TYPE;
                l_batch_id BATCH.BATCH_ID%TYPE;
            BEGIN
                SELECT NVL(SUM(AMOUNT), 0), SYSDATE
                  INTO l_nrf_amt, l_fee_date
                  FROM LEDGER L, BATCH B, SERVICE_FEES SF
                 WHERE L.BATCH_ID = B.BATCH_ID
                   AND B.DOC_ID = l_doc_id
                   AND L.DELETED = 'N'
                   AND L.ENTRY_TYPE = 'SF'
                   AND L.SERVICE_FEE_ID = SF.SERVICE_FEE_ID
                   AND SF.FREQUENCY_ID = 6;
                 IF l_nrf_amt > 0 THEN
                    -- l_nrf_amt is positive (money to the customer), so back it out with an adjustment
                    SELECT MAX(B.BATCH_ID)
                      INTO l_batch_id
                      FROM BATCH B
                     WHERE B.DOC_ID = l_doc_id
                       AND B.PAYMENT_SCHEDULE_ID = 7;
                    IF l_batch_id IS NULL THEN
                	    SELECT BATCH_SEQ.NEXTVAL
                          INTO l_batch_id
                          FROM DUAL;
                        INSERT INTO BATCH(
                            BATCH_ID,
                            DOC_ID,
                            TERMINAL_ID,
                            PAYMENT_SCHEDULE_ID,
                            START_DATE,
                            END_DATE,
                            BATCH_STATE_CD)
                          VALUES(
                            l_batch_id,
                            l_doc_id,
                            NULL,
                            7,
                            l_fee_date,
                            l_fee_date,
                            'F');
                    END IF;
                	SELECT LEDGER_SEQ.NEXTVAL
                      INTO l_ledger_id
                      FROM DUAL;
                	INSERT INTO LEDGER(
                        LEDGER_ID,
                        ENTRY_TYPE,
                        AMOUNT,
                        ENTRY_DATE,
                        BATCH_ID,
                        SETTLE_STATE_ID,
                        LEDGER_DATE,
                        DESCRIPTION,
                        CREATE_BY)
                       VALUES (
                        l_ledger_id,
                        'AD',
                        -l_nrf_amt,
                        l_fee_date,
                        l_batch_id,
                        2 /*process-no require*/,
                        l_fee_date,
                        'Net Revenue Hurdle Not Met',
                        l_user_id);
                 END IF;
            END;
            ADD_ROUNDING_ENTRY(l_doc_id);
            SELECT NVL(SUM(AMOUNT), 0)
              INTO l_total
              FROM LEDGER L, BATCH B
             WHERE L.DELETED = 'N'
               AND ENTRY_PAYABLE(L.SETTLE_STATE_ID,L.ENTRY_TYPE) = 'Y'
               AND L.BATCH_ID = B.BATCH_ID
               AND B.DOC_ID = l_doc_id;

        	UPDATE DOC D
               SET APPROVE_BY = l_user_id,
                   UPDATE_DATE = SYSDATE,
                   STATUS = DECODE(l_total, 0, 'D', 'A'),
                   TOTAL_AMOUNT = l_total,
                   DOC_TYPE = (
                        SELECT DECODE(CB.IS_EFT, 'Y', 'E', 'P') -- electronic or paper
                               || CASE WHEN l_total > 0 THEN 'C' ELSE 'D' END -- credit or debit
                          FROM CUSTOMER_BANK CB
                         WHERE D.CUSTOMER_BANK_ID = CB.CUSTOMER_BANK_ID)
      	 		WHERE DOC_ID = l_doc_id;
          END IF;
    END;
    
    PROCEDURE      UNAPPROVE_PAYMENT (
      l_doc_id IN DOC.DOC_ID%TYPE)
    IS
    BEGIN
        UPDATE DOC D SET APPROVE_BY = NULL, UPDATE_DATE = SYSDATE, STATUS = 'L',
            TOTAL_AMOUNT = NULL, DOC_TYPE = '--'
  	 		WHERE DOC_ID = l_doc_id
              AND STATUS = 'A';
        IF SQL%FOUND THEN
            -- Remove adjustments for positive net revenue fees
            UPDATE LEDGER
               SET DELETED = 'Y'
             WHERE ENTRY_TYPE = 'AD'
               AND TRANS_ID IS NULL
               AND BATCH_ID IN(
                    SELECT BATCH_ID
                      FROM BATCH
                     WHERE DOC_ID = l_doc_id
                       AND PAYMENT_SCHEDULE_ID = 7);
        END IF;
    END;
    
    PROCEDURE      UPLOAD_TO_ORF (
      l_doc_id IN DOC.DOC_ID%TYPE)
    IS
    BEGIN
    	 INSERT INTO USAT_INVOICE_INTERFACE@FINANCIALS(
            VENDOR_NAME_I, --                                        VARCHAR2 (240)                        REQUIRED     vendor name
            INVOICE_NUMBER_I, --                                    VARCHAR2 (240)                       REQUIRED      invoice number
            INVOICE_DATE_I, --                                           DATE                                           OPTIONAL
            INVOICE_AMOUNT_I, --                                    NUMBER                                     REQUIRED      amount of invoice
            DISTRIBUTION_SET_NAME_I, --                     VARCHAR2 (240)                       REQUIRED
            GL_DATE_I, --                                                      DATE                                            OPTIONAL
            PAY_GROUP_LOOKUP_CODE_I, --                 VARCHAR2 (240)                        REQUIRED     pay group
            VENDOR_SITE_CODE_I, --                                VARCHAR2 (25)                          REQUIRED     site code
            INVOICE_CURRENCY_CODE_I, --                  VARCHAR2 (15)                           REQUIRED     invoice currency
            PAYMENT_METHOD_LOOKUP_CODE_I, --   VARCHAR2 (25)                         REQUIRED     payment method
            VENDOR_NUMBER_I) --                                    VARCHAR2 (200)                        OPTIONAL
        SELECT
            C.CUSTOMER_NAME,
            D.REF_NBR,
            SYSDATE,
            D.TOTAL_AMOUNT,
            BU.DISTRIBUTION_SET_CD,
            SYSDATE,
            (CASE WHEN CU.CURRENCY_CODE in ('CAD') THEN 'CAD' ELSE BU.PAY_GROUP_CD END)  "PAY_GROUP_CD",
            CB.CUSTOMER_BANK_ID,
            CU.CURRENCY_CODE,
            DECODE(CB.IS_EFT, 'Y', 'EFT', 'CHECK'),
            C.CUSTOMER_ID
        FROM CORP.CUSTOMER C, CORP.CUSTOMER_BANK CB, CORP.DOC D, CORP.BUSINESS_UNIT BU, CORP.CURRENCY CU
        WHERE D.DOC_ID = l_doc_id
        AND D.CUSTOMER_BANK_ID = CB.CUSTOMER_BANK_ID
        AND CB.CUSTOMER_ID = C.CUSTOMER_ID
        AND D.CURRENCY_ID = CU.CURRENCY_ID
        AND D.BUSINESS_UNIT_ID = BU.BUSINESS_UNIT_ID
		AND NVL(D.TOTAL_AMOUNT, 0) != 0;
    END;
    
    PROCEDURE      MARK_DOC_PAID (
      l_doc_id IN DOC.DOC_ID%TYPE,
      l_user_id IN DOC.APPROVE_BY%TYPE)
    IS
        l_paid_date DATE := SYSDATE;
    BEGIN
    	 UPDATE DOC SET UPDATE_DATE = l_paid_date, STATUS = 'P'
          WHERE DOC_ID = l_doc_id
            AND STATUS = 'A';
         IF SQL%FOUND THEN
             UPDATE LEDGER SET PAID_DATE = l_paid_date
              WHERE BATCH_ID IN(SELECT BATCH_ID FROM BATCH WHERE DOC_ID = l_doc_id);
             UPDATE BATCH SET PAID_DATE = l_paid_date
              WHERE DOC_ID = l_doc_id;
			 UPLOAD_TO_ORF(l_doc_id);
			 UPDATE DOC SET SENT_BY = l_user_id, SENT_DATE = l_paid_date
			 WHERE DOC_ID = l_doc_id;
         END IF;
    END;
    
    PROCEDURE PROCESS_FEES_UPD
       (l_terminal_id IN PROCESS_FEES.TERMINAL_ID%TYPE,
        l_trans_type_id IN PROCESS_FEES.TRANS_TYPE_ID%TYPE,
        l_fee_percent IN PROCESS_FEES.FEE_PERCENT%TYPE,
        l_fee_amount IN PROCESS_FEES.FEE_AMOUNT%TYPE,
        l_fee_minimum IN PROCESS_FEES.MIN_AMOUNT%TYPE,
        l_effective_date IN PROCESS_FEES.END_DATE%TYPE DEFAULT SYSDATE,
        l_override IN CHAR DEFAULT 'N')
    IS
		l_lock VARCHAR2(128);
		l_current_date PROCESS_FEES.END_DATE%TYPE := SYSDATE;
		l_fee_effective_date PROCESS_FEES.END_DATE%TYPE := NVL(l_effective_date, l_current_date);
		l_fee_override CHAR := NVL(l_override, 'N');
    BEGIN
    	 --CHECK IF WE already paid on transactions before the effective date
    	 IF l_fee_override <> 'Y' AND l_fee_effective_date < l_current_date THEN
    	 	 DECLARE
    		     l_tran_id LEDGER.TRANS_ID%TYPE;
    			BEGIN
    		 	 SELECT /*+ index(T USAT_IX_TRANS_TERMINAL_ID) */ MIN(L.TRANS_ID)
                   INTO l_tran_id
                   FROM LEDGER L, BATCH B, TRANS T, DOC D
                  WHERE T.TRAN_ID = L.TRANS_ID
                    AND T.TRANS_TYPE_ID = l_trans_type_id
                    AND T.TERMINAL_ID = l_terminal_id
                    AND T.CLOSE_DATE >= l_fee_effective_date
                    AND B.TERMINAL_ID = l_terminal_id
                    AND L.BATCH_ID = B.BATCH_ID
                    AND B.DOC_ID = D.DOC_ID
                    AND L.DELETED = 'N'
                    AND D.STATUS NOT IN('O', 'D');
    			 IF l_tran_id IS NOT NULL THEN  --BIG PROBLEM
    			 	 RAISE_APPLICATION_ERROR(-20011, 'Transaction #' || TO_CHAR(l_tran_id) || ', which ocurred on the terminal whose fees you are updating, has already been paid.');
    			 END IF;
    		 END;
    	 END IF;
		 
		 l_lock := GLOBALS_PKG.REQUEST_LOCK('TERMINAL.TERMINAL_ID', l_terminal_id);
		 
    	 UPDATE PROCESS_FEES SET END_DATE = l_fee_effective_date WHERE TERMINAL_ID = l_terminal_id
    	 	AND TRANS_TYPE_ID = l_trans_type_id AND NVL(END_DATE, l_fee_effective_date) >= l_fee_effective_date;
    	 BEGIN
            DELETE FROM PROCESS_FEES WHERE TERMINAL_ID = l_terminal_id
        	 	AND TRANS_TYPE_ID = l_trans_type_id AND NVL(START_DATE, MIN_DATE) >= NVL(END_DATE, MAX_DATE);	
         EXCEPTION
            WHEN CHILD_RECORD_FOUND THEN
                NULL;
            WHEN OTHERS THEN
                RAISE;
         END;
         IF NVL(l_fee_percent, 0) <> 0 OR NVL(l_fee_amount, 0) <> 0 THEN	
        	 INSERT INTO PROCESS_FEES(PROCESS_FEE_ID, TERMINAL_ID, TRANS_TYPE_ID, FEE_PERCENT, FEE_AMOUNT, MIN_AMOUNT, START_DATE)
         	    VALUES(PROCESS_FEE_SEQ.NEXTVAL, l_terminal_id, l_trans_type_id, l_fee_percent, l_fee_amount, l_fee_minimum, l_fee_effective_date);
 	     END IF;
    END;
    
    FUNCTION GET_SERVICE_FEE_DATE_FMT(
        l_months FREQUENCY.MONTHS%TYPE,
        l_days FREQUENCY.DAYS%TYPE)
     RETURN VARCHAR
    IS
    BEGIN
        IF l_months >= 12  AND MOD(l_months, 12.0) = 0 THEN
            RETURN 'FMYYYY';
        ELSIF l_months > 0 THEN
            RETURN 'FMMonth, YYYY';
        ELSIF l_days >= 1 AND MOD(l_days, 1.0) = 0 THEN
            RETURN 'FMMM/DD/YYYY';
        ELSE
            RETURN 'FMMM/DD/YYYY HH:MI AM';
        END IF;
    END;
    
    PROCEDURE SCAN_FOR_SERVICE_FEES
    IS
        CURSOR c_fee
        IS
            SELECT DISTINCT
                   SF.SERVICE_FEE_ID,
                   SF.TERMINAL_ID,
                   CBT.CUSTOMER_BANK_ID,
                   F.FEE_ID,
                   SF.FEE_AMOUNT,
                   Q.MONTHS,
                   Q.DAYS,
                   SF.LAST_PAYMENT,
                   SF.START_DATE,
                   LEAST(NVL(
                        CASE WHEN Q.MONTHS = 0 THEN SF.END_DATE + Q.DAYS
                             ELSE TRUNC(LAST_DAY(ADD_MONTHS(SF.END_DATE, Q.MONTHS)), 'DD')
                        END
                   , MAX_DATE), SYSDATE) EFFECTIVE_END_DATE,
                   F.FEE_NAME,
                   T.FEE_CURRENCY_ID,
                   DECODE(T.PAYMENT_SCHEDULE_ID, 2, 'Y', 8, 'Y', 'N') AS_ACCUM,
                   F.INITIATION_TYPE_CD,
                   SF.GRACE_PERIOD_DATE,
				   SF.TRIGGERING_DATE,
				   SF.NO_TRIGGER_EVENT_FLAG
              FROM CORP.SERVICE_FEES SF
              JOIN CORP.FREQUENCY Q ON SF.FREQUENCY_ID = Q.FREQUENCY_ID
              JOIN CORP.CUSTOMER_BANK_TERMINAL CBT ON SF.TERMINAL_ID = CBT.TERMINAL_ID AND SYSDATE >= NVL(CBT.START_DATE, MIN_DATE) AND SYSDATE < NVL(CBT.END_DATE, MAX_DATE)
              JOIN CORP.FEES F ON SF.FEE_ID = F.FEE_ID
              JOIN REPORT.TERMINAL T ON SF.TERMINAL_ID = T.TERMINAL_ID              
             WHERE (Q.DAYS > 0 OR Q.MONTHS > 0)
               AND CASE WHEN Q.MONTHS = 0 THEN NVL(SF.LAST_PAYMENT, MIN_DATE) + Q.DAYS
                             ELSE TRUNC(LAST_DAY(ADD_MONTHS(NVL(SF.LAST_PAYMENT, MIN_DATE), Q.MONTHS)), 'DD')
                        END < LEAST(NVL(
                        CASE WHEN Q.MONTHS = 0 THEN SF.END_DATE + Q.DAYS
                             ELSE TRUNC(LAST_DAY(ADD_MONTHS(SF.END_DATE, Q.MONTHS)), 'DD')
                        END
                   , MAX_DATE), SYSDATE)
               AND (SF.LAST_PAYMENT IS NOT NULL 
				OR SF.TRIGGERING_DATE IS NOT NULL AND (F.INITIATION_TYPE_CD = 'T'
					OR F.INITIATION_TYPE_CD = 'G' AND COALESCE(SF.NO_TRIGGER_EVENT_FLAG, 'N') != 'Y')
				OR SF.GRACE_PERIOD_DATE < SYSDATE)
             ORDER BY SF.LAST_PAYMENT NULLS LAST, CBT.CUSTOMER_BANK_ID, SF.SERVICE_FEE_ID;

        l_last_payment                     SERVICE_FEES.LAST_PAYMENT%TYPE;
        l_fmt VARCHAR2(50);
        l_ledger_id LEDGER.LEDGER_ID%TYPE;
        l_batch_id LEDGER.BATCH_ID%TYPE;
		l_last_run_complete_ts DATE := TO_DATE(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('SCAN_FOR_SERVICE_FEES_LAST_RUN_COMPLETE_TS'), 'MM/DD/YYYY HH24:MI:SS');
    BEGIN
		IF l_last_run_complete_ts >= TO_DATE(TO_CHAR(CURRENT_TIMESTAMP, 'MM/DD/YYYY'), 'MM/DD/YYYY') 
			OR DBADMIN.PKG_GLOBAL.GET_APP_SETTING('SCAN_FOR_SERVICE_FEES_ENABLED') = 'N' THEN
			RETURN;
		END IF;
		
		UPDATE ENGINE.APP_SETTING
   		SET APP_SETTING_VALUE = TO_CHAR(CURRENT_TIMESTAMP, 'MM/DD/YYYY HH24:MI:SS')
   		WHERE APP_SETTING_CD = 'SCAN_FOR_SERVICE_FEES_LAST_RUN_START_TS';
		COMMIT;
	
        FOR r_fee IN c_fee LOOP
            -- If last payment is null, Use first transaction as last payment date
            IF r_fee.LAST_PAYMENT IS NULL THEN
                IF r_fee.INITIATION_TYPE_CD = 'I' THEN
                    l_last_payment := r_fee.START_DATE;
                ELSIF r_fee.INITIATION_TYPE_CD IN('G', 'T') THEN
					 l_last_payment := r_fee.TRIGGERING_DATE;
                     IF r_fee.INITIATION_TYPE_CD = 'G' AND r_fee.GRACE_PERIOD_DATE < NVL(l_last_payment, SYSDATE) THEN 
                        l_last_payment := r_fee.GRACE_PERIOD_DATE;
						UPDATE CORP.SERVICE_FEES
                        SET TRIGGERING_DATE = l_last_payment
                        WHERE SERVICE_FEE_ID = r_fee.SERVICE_FEE_ID;
                     END IF;
                     IF l_last_payment IS NOT NULL THEN
                         IF r_fee.INITIATION_TYPE_CD = 'G' THEN
                             IF r_fee.MONTHS > 0 THEN
                                SELECT TRUNC(LAST_DAY(ADD_MONTHS(l_last_payment, -r_fee.MONTHS)), 'DD')
                                  INTO l_last_payment
                                  FROM DUAL;
                             ELSE
                                SELECT l_last_payment - r_fee.DAYS
                                  INTO l_last_payment
                                  FROM DUAL;
                             END IF;
                        END IF;
                    END IF;
                ELSE
                    l_last_payment := NULL;
                END IF;
            ELSE
                l_last_payment := r_fee.LAST_PAYMENT;
            END IF;
            IF l_last_payment IS NOT NULL THEN
              l_fmt := GET_SERVICE_FEE_DATE_FMT(r_fee.MONTHS, r_fee.DAYS);
              LOOP
                  IF r_fee.MONTHS > 0 THEN
                      SELECT TRUNC(LAST_DAY(ADD_MONTHS(l_last_payment, r_fee.MONTHS)), 'DD')
                        INTO l_last_payment
                        FROM DUAL;
                  ELSE
                      SELECT l_last_payment + r_fee.DAYS
                        INTO l_last_payment
                        FROM DUAL;
                  END IF;
                  EXIT WHEN l_last_payment >= r_fee.EFFECTIVE_END_DATE;
  
                  DECLARE
                     l_fee_amt CORP.LEDGER.AMOUNT%TYPE;
                     l_desc CORP.LEDGER.DESCRIPTION%TYPE;
                     l_hosts PLS_INTEGER;
                  BEGIN -- catch exception here
                      -- skip creation of fee if start date is after fee date
                      IF l_last_payment >= NVL(r_fee.start_date, l_last_payment) THEN
                          l_batch_id := GET_OR_CREATE_BATCH(
                                      r_fee.TERMINAL_ID,
                                      r_fee.CUSTOMER_BANK_ID,
                                      l_last_payment,
                                      r_fee.FEE_CURRENCY_ID,
                                      r_fee.AS_ACCUM);
                          SELECT LEDGER_SEQ.NEXTVAL
                            INTO l_ledger_id
                            FROM DUAL;
                          IF r_fee.fee_id = 9 THEN
                             SELECT MAX(DEVICE_HOST_COUNT(e.EPORT_SERIAL_NUM, l_last_payment))
                               INTO l_hosts
                               FROM REPORT.EPORT e, REPORT.TERMINAL_EPORT te
                              WHERE e.EPORT_ID = te.EPORT_ID
                                AND te.TERMINAL_ID = r_fee.TERMINAL_ID
                                AND l_last_payment BETWEEN NVL(te.START_DATE, MIN_DATE) AND NVL(te.END_DATE, MAX_DATE);
                             l_fee_amt := -ABS(r_fee.FEE_AMOUNT * l_hosts);
                             l_desc := r_fee.FEE_NAME || ' ('||TO_CHAR(l_hosts)||' hosts) ' || ' for ' || TO_CHAR(l_last_payment, l_fmt);
                          ELSE
                             l_fee_amt := -ABS(r_fee.FEE_AMOUNT);
                             l_desc := r_fee.FEE_NAME || ' for ' || TO_CHAR(l_last_payment, l_fmt);
                          END IF;
                          INSERT INTO LEDGER(
                              LEDGER_ID,
                              ENTRY_TYPE,
                              SERVICE_FEE_ID,
                              AMOUNT,
                              ENTRY_DATE,
                              BATCH_ID,
                              SETTLE_STATE_ID,
                              LEDGER_DATE,
                              DESCRIPTION)
                          VALUES (
                              l_ledger_id,
                              'SF',
                              r_fee.SERVICE_FEE_ID,
                              l_fee_amt,
                              l_last_payment,
                              l_batch_id,
                              2,
                              l_last_payment,
                              l_desc);
                          -- create net revenue fee on transaction, if any
                          INSERT INTO LEDGER(
                              LEDGER_ID,
                              RELATED_LEDGER_ID,
                              ENTRY_TYPE,
                              SERVICE_FEE_ID,
                              AMOUNT,
                              ENTRY_DATE,
                              BATCH_ID,
                              SETTLE_STATE_ID,
                              LEDGER_DATE,
                              DESCRIPTION)
                          SELECT
                              LEDGER_SEQ.NEXTVAL,
                              l_ledger_id,
                              'SF',
                              SF.SERVICE_FEE_ID,
                              -l_fee_amt * SF.FEE_PERCENT,
                              l_last_payment,
                              l_batch_id,
                              2,
                              l_last_payment,
                              F.FEE_NAME
                          FROM SERVICE_FEES SF, FEES F
                         WHERE SF.FEE_ID = F.FEE_ID
                           AND SF.TERMINAL_ID = r_fee.TERMINAL_ID
                           AND l_last_payment BETWEEN NVL(SF.START_DATE, MIN_DATE)
                                AND NVL(SF.END_DATE, MAX_DATE)
                           AND SF.FREQUENCY_ID = 6;
                      END IF;
  
                      UPDATE SERVICE_FEES
                         SET LAST_PAYMENT = l_last_payment,
                             FIRST_PAYMENT = CASE WHEN FIRST_PAYMENT IS NULL OR l_last_payment < FIRST_PAYMENT THEN l_last_payment ELSE FIRST_PAYMENT END
                       WHERE SERVICE_FEE_ID =  r_fee.SERVICE_FEE_ID;
  
                      COMMIT;             -- BECAREFUL OF CALLING THIS PROCEDURE SINCE IT HAS THIS COMMIT
                  EXCEPTION
                                   WHEN DUP_VAL_ON_INDEX THEN
                                           ROLLBACK;
                  END;
              END LOOP;
            END IF;
        END LOOP;
		
		UPDATE ENGINE.APP_SETTING
   		SET APP_SETTING_VALUE = TO_CHAR(CURRENT_TIMESTAMP, 'MM/DD/YYYY HH24:MI:SS')
   		WHERE APP_SETTING_CD = 'SCAN_FOR_SERVICE_FEES_LAST_RUN_COMPLETE_TS';
    END;

    PROCEDURE SERVICE_FEES_UPD(
        l_terminal_id IN SERVICE_FEES.TERMINAL_ID%TYPE,
        l_fee_id IN SERVICE_FEES.FEE_ID%TYPE,
        l_freq_id IN SERVICE_FEES.FREQUENCY_ID%TYPE,
        l_fee_amt IN SERVICE_FEES.FEE_AMOUNT%TYPE,
        l_fee_perc IN SERVICE_FEES.FEE_PERCENT%TYPE,
        l_effective_date IN SERVICE_FEES.END_DATE%TYPE DEFAULT SYSDATE,
        l_end_date IN SERVICE_FEES.END_DATE%TYPE,
        l_override IN CHAR DEFAULT 'N',
        pn_grace_days IN NUMBER DEFAULT 60,
		pc_no_trigger_event_flag IN SERVICE_FEES.NO_TRIGGER_EVENT_FLAG%TYPE DEFAULT NULL)
    IS
        l_last_payment SERVICE_FEES.LAST_PAYMENT%TYPE;
        l_sf_id SERVICE_FEES.SERVICE_FEE_ID%TYPE;
        l_batch_id LEDGER.BATCH_ID%TYPE;
        l_next_payment SERVICE_FEES.LAST_PAYMENT%TYPE;
        l_first_payment SERVICE_FEES.LAST_PAYMENT%TYPE;
		l_lock VARCHAR2(128);
		l_current_date SERVICE_FEES.END_DATE%TYPE := SYSDATE;
		l_fee_effective_date SERVICE_FEES.END_DATE%TYPE := NVL(l_effective_date, l_current_date);
		l_fee_override CHAR := NVL(l_override, 'N');
        l_orig_start_date SERVICE_FEES.START_DATE%TYPE;
        l_orig_end_date SERVICE_FEES.END_DATE%TYPE;
        l_orig_amount SERVICE_FEES.FEE_AMOUNT%TYPE;
        l_orig_perc SERVICE_FEES.FEE_PERCENT%TYPE; 
        lc_initiation_type_cd CORP.FEES.INITIATION_TYPE_CD%TYPE;
    BEGIN
		SELECT INITIATION_TYPE_CD
          INTO lc_initiation_type_cd
          FROM CORP.FEES
         WHERE FEE_ID = l_fee_id;
         
        l_lock := GLOBALS_PKG.REQUEST_LOCK('TERMINAL.TERMINAL_ID', l_terminal_id);
        
        BEGIN
            IF lc_initiation_type_cd = 'P' THEN
                --disallow change to net revenue fees that affect already paid entries
                SELECT MAX(ENTRY_DATE)
                  INTO l_last_payment
                  FROM LEDGER L, BATCH B, SERVICE_FEES SF, DOC D
                 WHERE B.TERMINAL_ID = l_terminal_id
                   AND SF.TERMINAL_ID = l_terminal_id
                   AND SF.FEE_ID = l_fee_id
                   AND SF.FREQUENCY_ID = l_freq_id
                   AND B.DOC_ID = D.DOC_ID
                   AND D.STATUS NOT IN('O', 'D')
                   AND B.BATCH_ID = L.BATCH_ID
                   AND L.SERVICE_FEE_ID = SF.SERVICE_FEE_ID
                   AND L.ENTRY_TYPE = 'SF'
                   AND L.DELETED = 'N';
                IF NVL(l_last_payment, l_fee_effective_date) > l_fee_effective_date THEN -- BIG PROBLEM
                    RAISE_APPLICATION_ERROR(-20012, 'This net revenue fee was charged to the customer after the specified effective date.');
                ELSE
                    l_last_payment := MIN_DATE;
                END IF;
            ELSE
                SELECT MAX(LAST_PAYMENT), 
                       NVL(MIN(FIRST_PAYMENT), MIN_DATE),
                       CASE WHEN Q.MONTHS = 0 THEN l_fee_effective_date + Q.DAYS
                           ELSE TRUNC(LAST_DAY(ADD_MONTHS(l_fee_effective_date, Q.MONTHS)), 'DD')
                      END
                  INTO l_last_payment, l_first_payment, l_next_payment
                  FROM SERVICE_FEES SF, CORP.FREQUENCY Q
                 WHERE SF.TERMINAL_ID = l_terminal_id
                   AND SF.FEE_ID = l_fee_id
                   AND SF.FREQUENCY_ID = l_freq_id
                   AND SF.FREQUENCY_ID = Q.FREQUENCY_ID
                 GROUP BY CASE WHEN Q.MONTHS = 0 THEN l_fee_effective_date + Q.DAYS
                           ELSE TRUNC(LAST_DAY(ADD_MONTHS(l_fee_effective_date, Q.MONTHS)), 'DD')
                      END;
                IF l_last_payment is not null AND l_last_payment > l_next_payment THEN
                    IF l_fee_override <> 'Y' THEN --BIG PROBLEM
                        RAISE_APPLICATION_ERROR(-20011, 'This service fee was charged to the customer after the specified effective date.');
                    END IF;
                END IF;
                IF l_last_payment is not null AND l_last_payment > l_end_date THEN
                    IF l_fee_override <> 'Y' THEN --BIG PROBLEM
                        RAISE_APPLICATION_ERROR(-20011, 'This service fee was charged to the customer after the specified end date.');
                    END IF;
                END IF;
            END IF;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
            	 NULL;
            WHEN OTHERS THEN
            	 RAISE;
        END;
        IF lc_initiation_type_cd = 'P' THEN
            --delete any old net revenue fees >= l_fee_effective_date
            DELETE FROM LEDGER L
             WHERE L.ENTRY_TYPE = 'SF'
               AND L.SERVICE_FEE_ID IN(
                 SELECT SF.SERVICE_FEE_ID
                   FROM SERVICE_FEES SF
                  WHERE SF.TERMINAL_ID = l_terminal_id
                    AND SF.FEE_ID = l_fee_id
                    AND SF.FREQUENCY_ID = l_freq_id
                    --AND NVL(SF.END_DATE, l_fee_effective_date) >= l_fee_effective_date
                    )
               AND L.ENTRY_DATE >= l_fee_effective_date
               AND NOT EXISTS(
                    SELECT 1
                    FROM BATCH B, DOC D
                    WHERE B.BATCH_ID = L.BATCH_ID
                    AND B.DOC_ID = D.DOC_ID
                    AND D.STATUS NOT IN('O', 'D'));
        END IF;
        
        SELECT MAX(START_DATE), MAX(END_DATE), MAX(FEE_AMOUNT), MAX(FEE_PERCENT), MAX(SERVICE_FEE_ID)
          INTO l_orig_start_date, l_orig_end_date, l_orig_amount, l_orig_perc, l_sf_id
          FROM (SELECT * FROM CORP.SERVICE_FEES SF
		 WHERE SF.TERMINAL_ID = l_terminal_id
           AND SF.FEE_ID = l_fee_id
           AND SF.FREQUENCY_ID = l_freq_id
         ORDER BY NVL(END_DATE, MAX_DATE) DESC)
        WHERE ROWNUM = 1;
        
        IF l_sf_id IS NOT NULL AND ((lc_initiation_type_cd = 'P' AND NVL(l_orig_perc, 0) = NVL(l_fee_perc, 0)) OR (lc_initiation_type_cd != 'P' AND NVL(l_orig_amount, 0) = NVL(l_fee_amt, 0))) AND l_fee_effective_date <= NVL(l_orig_start_date, MIN_DATE) AND NVL(l_end_date, MAX_DATE) >= NVL(l_orig_start_date, MIN_DATE) THEN
            -- first update other records that may be affected
            UPDATE SERVICE_FEES 
               SET END_DATE = l_fee_effective_date
             WHERE TERMINAL_ID = l_terminal_id
               AND FEE_ID = l_fee_id
               AND FREQUENCY_ID = l_freq_id
               AND NVL(END_DATE, l_fee_effective_date) >= l_fee_effective_date
               AND SERVICE_FEE_ID != l_sf_id;

            -- update existing record
            UPDATE SERVICE_FEES 
               SET END_DATE = l_end_date,
                   START_DATE = l_fee_effective_date,
                   GRACE_PERIOD_DATE = DECODE(lc_initiation_type_cd, 'G', l_fee_effective_date + pn_grace_days),
				   NO_TRIGGER_EVENT_FLAG = pc_no_trigger_event_flag
             WHERE SERVICE_FEE_ID = l_sf_id;           
        ELSE
            l_sf_id := NULL;
            UPDATE SERVICE_FEES SET END_DATE = l_fee_effective_date
             WHERE TERMINAL_ID = l_terminal_id
               AND FEE_ID = l_fee_id
               AND FREQUENCY_ID = l_freq_id
               AND NVL(END_DATE, l_fee_effective_date) >= l_fee_effective_date;
            IF NVL(l_fee_amt, 0) <> 0 OR NVL(l_fee_perc, 0) <> 0 THEN
                SELECT SERVICE_FEE_SEQ.NEXTVAL INTO l_sf_id FROM DUAL;
                INSERT INTO SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID,
                    FREQUENCY_ID, FEE_AMOUNT, FEE_PERCENT, START_DATE,
                    END_DATE, LAST_PAYMENT, GRACE_PERIOD_DATE, NO_TRIGGER_EVENT_FLAG)
                    SELECT l_sf_id, l_terminal_id, l_fee_id, l_freq_id, l_fee_amt,
                           l_fee_perc, l_fee_effective_date, l_end_date,
                           l_last_payment, DECODE(lc_initiation_type_cd, 'G', l_fee_effective_date + pn_grace_days),
						   pc_no_trigger_event_flag
                      FROM FREQUENCY F
                     WHERE F.FREQUENCY_ID = l_freq_id;
            END IF;	
        END IF;
		IF l_sf_id IS NOT NULL THEN
            IF lc_initiation_type_cd = 'P' THEN
                --add any new net revenue fees
                INSERT INTO LEDGER(
                    LEDGER_ID,
                    RELATED_LEDGER_ID,
                    ENTRY_TYPE,
                    TRANS_ID,
                    PROCESS_FEE_ID,
                    SERVICE_FEE_ID,
                    AMOUNT,
                    ENTRY_DATE,
                    BATCH_ID,
                    SETTLE_STATE_ID,
                    LEDGER_DATE,
                    DESCRIPTION)
                SELECT
                    LEDGER_SEQ.NEXTVAL,
                    L.LEDGER_ID,
                    'SF',
                    L.TRANS_ID,
                    L.PROCESS_FEE_ID,
                    l_sf_id,
                    -L.AMOUNT * l_fee_perc,
                    L.ENTRY_DATE,
                    L.BATCH_ID,
                    L.SETTLE_STATE_ID,
                    L.LEDGER_DATE,
                    F.FEE_NAME
                FROM LEDGER L, BATCH B, FEES F, DOC D
               WHERE F.FEE_ID = l_fee_id
                 AND L.BATCH_ID = B.BATCH_ID
                 AND B.TERMINAL_ID = l_terminal_id
                 AND L.ENTRY_DATE >= l_fee_effective_date
                 AND L.RELATED_LEDGER_ID IS NULL -- Avoid Percent of Percent Fee
                 AND L.DELETED = 'N'
                 AND B.DOC_ID = D.DOC_ID
                 AND D.STATUS IN('O')
                 AND L.ENTRY_TYPE IN('CC', 'PF', 'SF', 'AD', 'CB', 'RF');
            ELSIF l_fee_override = 'Y' AND l_last_payment IS NOT NULL THEN -- let's insert any missing (No need to do this for net rev fees)
                DECLARE
                    CURSOR l_cur IS
                        SELECT LEDGER_SEQ.NEXTVAL LEDGER_ID,
                               A.FEE_DATE,
                               CBT.CUSTOMER_BANK_ID,
                               T.FEE_CURRENCY_ID,
                               F.FEE_NAME || ' for ' || TO_CHAR(A.FEE_DATE, A.FMT) FEE_DESC,
                               'Y' AS_ACCUM
                    FROM (SELECT DECODE(Q.DAYS, 0, LAST_DAY(D.COLUMN_VALUE), D.COLUMN_VALUE + Q.DAYS - 1) FEE_DATE,
                                 GET_SERVICE_FEE_DATE_FMT(Q.MONTHS, Q.DAYS) FMT
                            FROM TABLE(CAST(CORP.GLOBALS_PKG.GET_DATE_LIST(GREATEST(l_fee_effective_date, l_first_payment),
                                 LEAST(NVL(l_last_payment, (SELECT  
                                   CASE WHEN Q.MONTHS = 0 THEN l_current_date - Q.DAYS
                                        ELSE TRUNC(LAST_DAY(ADD_MONTHS(l_current_date, -Q.MONTHS)), 'DD')
                                   END FROM CORP.FREQUENCY Q WHERE Q.FREQUENCY_ID = l_freq_id)), NVL(l_end_date, MAX_DATE)),
                                 l_freq_id, 0, 0) AS REPORT.DATE_LIST)) D,
                                 FREQUENCY Q
                            WHERE Q.FREQUENCY_ID = l_freq_id
                              AND (Q.MONTHS > 0 OR Q.DAYS > 0)) A,
                          CUSTOMER_BANK_TERMINAL CBT,
                          FEES F,
                          REPORT.TERMINAL T
                    WHERE NOT EXISTS(SELECT 1
                        FROM CORP.LEDGER L, CORP.BATCH B
                        WHERE L.BATCH_ID = B.BATCH_ID
                          AND B.TERMINAL_ID = l_terminal_id
                          AND L.ENTRY_TYPE = 'SF'
                          AND L.DELETED = 'N'
                          AND L.AMOUNT <> 0
                          AND L.ENTRY_DATE = A.FEE_DATE)
                      AND WITHIN1(A.FEE_DATE, CBT.START_DATE, CBT.END_DATE) = 1
                      AND CBT.TERMINAL_ID = l_terminal_id
                      AND F.FEE_ID = l_fee_id
                      AND T.TERMINAL_ID = l_terminal_id;
                BEGIN
                    FOR l_rec IN l_cur LOOP
                        l_batch_id := GET_OR_CREATE_BATCH(
                                    l_terminal_id,
                                    l_rec.CUSTOMER_BANK_ID,
                                    l_rec.FEE_DATE,
                                    l_rec.FEE_CURRENCY_ID,
                                    l_rec.AS_ACCUM);

                        INSERT INTO LEDGER(
                                LEDGER_ID,
                                ENTRY_TYPE,
                                SERVICE_FEE_ID,
                                AMOUNT,
                                ENTRY_DATE,
                                BATCH_ID,
                                SETTLE_STATE_ID,
                                LEDGER_DATE,
                                DESCRIPTION)
                            VALUES(
                                l_rec.LEDGER_ID,
                                'SF',
                                l_sf_id,
                                -ABS(l_fee_amt),
                                l_rec.FEE_DATE,
                                l_batch_id,
                                2,
                                l_rec.FEE_DATE,
                                l_rec.FEE_DESC);
                        -- create net revenue fee on service fee, if any
                        INSERT INTO LEDGER(
                            LEDGER_ID,
                            RELATED_LEDGER_ID,
                            ENTRY_TYPE,
                            SERVICE_FEE_ID,
                            AMOUNT,
                            ENTRY_DATE,
                            BATCH_ID,
                            SETTLE_STATE_ID,
                            LEDGER_DATE,
                            DESCRIPTION)
                        SELECT
                            LEDGER_SEQ.NEXTVAL,
                            l_rec.LEDGER_ID,
                            'SF',
                            SF.SERVICE_FEE_ID,
                            ABS(l_fee_amt) * SF.FEE_PERCENT,
                            l_rec.FEE_DATE,
                            l_batch_id,
                            2,
                            l_rec.FEE_DATE,
                            F.FEE_NAME
                        FROM SERVICE_FEES SF, FEES F
                       WHERE SF.FEE_ID = F.FEE_ID
                         AND SF.TERMINAL_ID = l_terminal_id
                         AND l_rec.FEE_DATE BETWEEN NVL(SF.START_DATE, MIN_DATE)
                              AND NVL(SF.END_DATE, MAX_DATE)
                         AND SF.FREQUENCY_ID = 6;
                        UPDATE CORP.SERVICE_FEES
                         SET LAST_PAYMENT = CASE WHEN LAST_PAYMENT IS NULL OR l_rec.FEE_DATE > LAST_PAYMENT THEN l_rec.FEE_DATE ELSE LAST_PAYMENT END,
                             FIRST_PAYMENT = CASE WHEN FIRST_PAYMENT IS NULL OR l_rec.FEE_DATE < FIRST_PAYMENT THEN l_rec.FEE_DATE ELSE FIRST_PAYMENT END
                       WHERE SERVICE_FEE_ID =  l_sf_id;
                    END LOOP;
                END;
            END IF;
        END IF;
    END;
    
    PROCEDURE UNLOCK_DOC (
      l_doc_id IN DOC.DOC_ID%TYPE)
    IS
        l_customer_bank_id CORP.DOC.CUSTOMER_BANK_ID%TYPE;
        l_lock VARCHAR2(128);
    BEGIN
    	IF FREEZE_DOC_STATUS_DOC_ID(l_doc_id) <> 'L' THEN
           RAISE_APPLICATION_ERROR(-20400, 'This document has already been approved; you can not unlock it.');
   	    END IF;
    	--Remove rounding entries
    	UPDATE LEDGER
           SET DELETED = 'Y'
         WHERE ENTRY_TYPE = 'SB'
           AND TRANS_ID IS NULL
           AND BATCH_ID IN(SELECT BATCH_ID FROM BATCH WHERE DOC_ID = l_doc_id);
           
        -- Blank out the end date of any as accum batches
        UPDATE BATCH B
           SET B.BATCH_STATE_CD = 'L',
               B.BATCH_CLOSED_TS = NULL,
               B.END_DATE = NULL
         WHERE B.DOC_ID = l_doc_id
           AND B.BATCH_STATE_CD = 'F'
           AND B.PAYMENT_SCHEDULE_ID IN(1,5);

        -- Change time-period batches to Open
        UPDATE BATCH B
           SET B.BATCH_STATE_CD = 'O',
               B.BATCH_CLOSED_TS = NULL
         WHERE B.DOC_ID = l_doc_id
           AND B.BATCH_STATE_CD IN('U', 'D')
           AND B.PAYMENT_SCHEDULE_ID NOT IN(1,2,5,7,8);

        -- first condense any docs that may match once they are re-opened
    	DECLARE
    	   l_new_doc_id DOC.DOC_ID%TYPE;
        BEGIN
    	    SELECT MAX(DNEW.DOC_ID)
              INTO l_new_doc_id
              FROM DOC DOLD, DOC DNEW
             WHERE DOLD.DOC_ID = l_doc_id
               AND DNEW.DOC_ID <> l_doc_id
               AND DOLD.CUSTOMER_BANK_ID = DNEW.CUSTOMER_BANK_ID
               AND NVL(DOLD.CURRENCY_ID, 0) = NVL(DNEW.CURRENCY_ID, 0)
               AND DNEW.STATUS = 'O';
            IF l_new_doc_id IS NOT NULL THEN
            	-- Update batches and delete old doc
            	UPDATE BATCH SET DOC_ID = l_new_doc_id WHERE DOC_ID = l_doc_id;
            	DELETE FROM DOC WHERE DOC_ID = l_doc_id;
            	
            	--now condense any batches that may match once they are moved
            	DECLARE
            	   CURSOR l_cur IS
                    	SELECT BNEW.BATCH_ID NEW_BATCH_ID,
                               BOLD.BATCH_ID OLD_BATCH_ID,
                               BOLD.END_DATE OLD_END_DATE,
                               BOLD.START_DATE OLD_START_DATE
                          FROM BATCH BOLD, BATCH BNEW
                         WHERE BOLD.DOC_ID = l_new_doc_id
                           AND BNEW.DOC_ID = l_new_doc_id
                           AND BOLD.PAYMENT_SCHEDULE_ID = BNEW.PAYMENT_SCHEDULE_ID
                           AND NVL(BOLD.TERMINAL_ID, 0) = NVL(BNEW.TERMINAL_ID, 0)
                           AND ((BOLD.BATCH_STATE_CD != 'C' AND BNEW.BATCH_STATE_CD = 'C')
                            OR ((BOLD.BATCH_STATE_CD != 'C' OR BNEW.BATCH_STATE_CD = 'C') AND BOLD.BATCH_ID > BNEW.BATCH_ID))
                           AND (BOLD.PAYMENT_SCHEDULE_ID IN(1,5)
                             OR BOLD.START_DATE = BNEW.START_DATE);
            	BEGIN
            	   FOR l_rec IN l_cur LOOP
            	       UPDATE LEDGER
            	          SET BATCH_ID = l_rec.NEW_BATCH_ID
                        WHERE BATCH_ID = l_rec.OLD_BATCH_ID;
                    LOOP
                      UPDATE CORP.BATCH_TOTAL
                      SET (LEDGER_AMOUNT, LEDGER_COUNT)= (select sum(LEDGER_AMOUNT) LEDGER_AMOUNT, sum(LEDGER_COUNT) LEDGER_COUNT
                      from (
                      select LEDGER_AMOUNT, LEDGER_COUNT
                      from corp.batch_total where batch_id= l_rec.OLD_BATCH_ID and ENTRY_TYPE='CA'
                      union
                      select LEDGER_AMOUNT, LEDGER_COUNT
                      from corp.batch_total where batch_id= l_rec.NEW_BATCH_ID and ENTRY_TYPE='CA'))
                      where batch_id=l_rec.NEW_BATCH_ID
                      and entry_type ='CA';
                      EXIT WHEN SQL%ROWCOUNT > 0;
                      BEGIN
                          INSERT INTO CORP.BATCH_TOTAL(BATCH_ID, ENTRY_TYPE, PAYABLE, LEDGER_AMOUNT, LEDGER_COUNT)                        
                          select l_rec.NEW_BATCH_ID,'CA','N', sum(LEDGER_AMOUNT) LEDGER_AMOUNT, sum(LEDGER_COUNT) LEDGER_COUNT
	                      from (
	                      select LEDGER_AMOUNT, LEDGER_COUNT
	                      from corp.batch_total where batch_id= l_rec.OLD_BATCH_ID and ENTRY_TYPE='CA'
	                      union
	                      select LEDGER_AMOUNT, LEDGER_COUNT
	                      from corp.batch_total where batch_id= l_rec.NEW_BATCH_ID and ENTRY_TYPE='CA')
                          EXIT;    
                      EXCEPTION
                          WHEN DUP_VAL_ON_INDEX THEN
                              NULL;
                      END;
                    END LOOP;  
                    LOOP
                     UPDATE CORP.BATCH_TOTAL
                      SET (LEDGER_AMOUNT, LEDGER_COUNT)= (select sum(LEDGER_AMOUNT) LEDGER_AMOUNT, sum(LEDGER_COUNT) LEDGER_COUNT
                      from (
                      select LEDGER_AMOUNT, LEDGER_COUNT
                      from corp.batch_total where batch_id= l_rec.OLD_BATCH_ID and ENTRY_TYPE='TT'
                      union
                      select LEDGER_AMOUNT, LEDGER_COUNT
                      from corp.batch_total where batch_id= l_rec.NEW_BATCH_ID and ENTRY_TYPE='TT'))
                      where batch_id=l_rec.NEW_BATCH_ID
                      and entry_type ='TT';
                      EXIT WHEN SQL%ROWCOUNT > 0;
                      BEGIN
                          INSERT INTO CORP.BATCH_TOTAL(BATCH_ID, ENTRY_TYPE, PAYABLE, LEDGER_AMOUNT, LEDGER_COUNT)                        
                          select l_rec.NEW_BATCH_ID,'TT','N', sum(LEDGER_AMOUNT) LEDGER_AMOUNT, sum(LEDGER_COUNT) LEDGER_COUNT
	                      from (
	                      select LEDGER_AMOUNT, LEDGER_COUNT
	                      from corp.batch_total where batch_id= l_rec.OLD_BATCH_ID and ENTRY_TYPE='TT'
	                      union
	                      select LEDGER_AMOUNT, LEDGER_COUNT
	                      from corp.batch_total where batch_id= l_rec.NEW_BATCH_ID and ENTRY_TYPE='TT')
                          EXIT;    
                      EXCEPTION
                          WHEN DUP_VAL_ON_INDEX THEN
                              NULL;
                      END;
                    END LOOP;  
                    UPDATE REPORT.ACTIVITY_REF SET PAYMENT_BATCH_ID=l_rec.NEW_BATCH_ID
                    WHERE PAYMENT_BATCH_ID=l_rec.OLD_BATCH_ID;
                       DELETE FROM BATCH_FILL WHERE BATCH_ID = l_rec.OLD_BATCH_ID;
                       DELETE FROM BATCH WHERE BATCH_ID = l_rec.OLD_BATCH_ID;
                       DELETE FROM BATCH_TOTAL WHERE BATCH_ID = l_rec.OLD_BATCH_ID;
                       
                       UPDATE BATCH
                          SET START_DATE = LEAST(START_DATE, l_rec.OLD_START_DATE),
                              END_DATE = GREATEST(END_DATE, l_rec.OLD_END_DATE)
                        WHERE BATCH_ID = l_rec.NEW_BATCH_ID;
            	   END LOOP;
         	   END;
            ELSE
                -- Update doc
            	UPDATE DOC SET STATUS = 'O' WHERE DOC_ID = l_doc_id;
            END IF;
    	END;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
    	   RAISE_APPLICATION_ERROR(-20401, 'This document does not exist.');
        WHEN OTHERS THEN
    	   RAISE;
    END;
    
    FUNCTION GET_TRANS_ADJ_DESC(
        l_trans_id LEDGER.TRANS_ID%TYPE,
        l_entry_type LEDGER.ENTRY_TYPE%TYPE,
        l_entry_date LEDGER.ENTRY_DATE%TYPE,
        l_terminal_id BATCH.TERMINAL_ID%TYPE,
        l_customer_bank_id DOC.CUSTOMER_BANK_ID%TYPE)
     RETURN LEDGER.DESCRIPTION%TYPE
    IS
        l_old_payable VARCHAR(4000); -- oracle raises an exception if you use anything with smaller length
        l_old_terminal_id BATCH.TERMINAL_ID%TYPE;
        l_old_customer_bank_id DOC.CUSTOMER_BANK_ID%TYPE;
        l_desc LEDGER.DESCRIPTION%TYPE;
    BEGIN
        -- figure out why this was added
        --possible reasons:
        --  1. previously denied
        --  2. changed location
        --  3. changed customer bank
        
        SELECT *
          INTO l_old_payable, l_old_terminal_id, l_old_customer_bank_id
          FROM (
            SELECT PAYMENTS_PKG.ENTRY_PAYABLE(L.SETTLE_STATE_ID, L.ENTRY_TYPE),
                   B.TERMINAL_ID, D.CUSTOMER_BANK_ID
              FROM LEDGER L, BATCH B, DOC D
             WHERE L.BATCH_ID = B.BATCH_ID
               AND L.DELETED = 'N'
               AND B.DOC_ID = D.DOC_ID
               AND D.STATUS NOT IN('O', 'D')
               AND L.TRANS_ID = l_trans_id
               AND L.ENTRY_TYPE = 'CC'
               AND B.PAYMENT_SCHEDULE_ID <> 5
             ORDER BY L.CREATE_DATE DESC) A
         WHERE ROWNUM = 1;
        IF l_old_payable = 'N' THEN
            IF l_entry_type = 'CC' THEN
                RETURN 'Payment of previously declined transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSIF l_entry_type = 'PF' THEN
                RETURN 'Process Fee for previously declined transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSE -- this should never occur
                RETURN 'Payment related to previously declined transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            END IF;
        ELSIF l_terminal_id <> l_old_terminal_id THEN
            IF l_entry_type = 'CC' THEN
                RETURN 'Payment of transaction previously assigned to a different location (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSIF l_entry_type = 'PF' THEN
                RETURN 'Process Fee for transaction previously assigned to a different location (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSE -- this should never occur
                RETURN 'Payment related to transaction previously assigned to a different location (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            END IF;
        ELSIF l_customer_bank_id <> l_old_customer_bank_id THEN
            IF l_entry_type = 'CC' THEN
                RETURN 'Payment of transaction previously assigned to a different bank account (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSIF l_entry_type = 'PF' THEN
                RETURN 'Process Fee for transaction previously assigned to a different bank account (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSE -- this should never occur
                RETURN 'Payment related to transaction previously assigned to a different bank account (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            END IF;
        ELSE
            IF l_entry_type = 'CC' THEN
                RETURN 'Payment of previously unpaid transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSIF l_entry_type = 'PF' THEN
                RETURN 'Process Fee for previously unpaid transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSE -- this should never occur
                RETURN 'Payment related to previously unpaid transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            END IF;
        END IF;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            IF l_entry_type = 'CC' THEN
                RETURN 'Payment of previously unpaid transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSIF l_entry_type = 'PF' THEN
                RETURN 'Process Fee for previously unpaid transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSE -- this should never occur
                RETURN 'Payment related to previously unpaid transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            END IF;
    END;
    
    PROCEDURE UPDATE_LEDGER(
              l_tran_id LEDGER.TRANS_ID%TYPE)
    IS
    	CURSOR l_cur IS
    		SELECT TRANS_TYPE_ID, CLOSE_DATE, SETTLE_DATE, TOTAL_AMOUNT, SETTLE_STATE_ID,
                   TERMINAL_ID, CUSTOMER_BANK_ID, PROCESS_FEE_ID, CURRENCY_ID
    		  FROM REPORT.TRANS
    		 WHERE TRAN_ID = l_tran_id;
    BEGIN
         FOR l_rec IN l_cur LOOP
    	     UPDATE_LEDGER(l_tran_id,l_rec.trans_type_id,l_rec.close_date,l_rec.settle_date,
                           l_rec.total_amount,l_rec.settle_state_id,l_rec.terminal_id,
                           l_rec.customer_bank_id,l_rec.process_fee_id,l_rec.currency_id);
   	     END LOOP;
    END;
    
    PROCEDURE ADD_ACTIVATION_FEE(
        l_terminal_id CORP.BATCH.TERMINAL_ID%TYPE,
        l_fee_amt CORP.LEDGER.AMOUNT%TYPE,
        l_activation_date  CORP.LEDGER.ENTRY_DATE%TYPE)
    IS
        l_batch_id CORP.BATCH.BATCH_ID%TYPE;
        l_service_fee_id CORP.LEDGER.SERVICE_FEE_ID%TYPE;
        l_desc CORP.LEDGER.DESCRIPTION%TYPE;
        l_ledger_id CORP.LEDGER.LEDGER_ID%TYPE;
        l_cust_bank_id CORP.DOC.CUSTOMER_BANK_ID%TYPE;
        l_fee_currency_id CORP.DOC.CURRENCY_ID%TYPE;
		l_lock VARCHAR2(128);
    BEGIN
        SELECT cbt.CUSTOMER_BANK_ID, t.FEE_CURRENCY_ID,
               'Activation Fee for Terminal ' || t.TERMINAL_NBR
          INTO l_cust_bank_id, l_fee_currency_id, l_desc
          FROM REPORT.TERMINAL t, CORP.CUSTOMER_BANK_TERMINAL cbt
         WHERE t.TERMINAL_ID = l_terminal_id
           AND t.TERMINAL_ID = cbt.TERMINAL_ID
           AND l_activation_date >= NVL(cbt.START_DATE, MIN_DATE)
           AND l_activation_date < NVL(cbt.END_DATE, MAX_DATE);
		   
		l_lock := GLOBALS_PKG.REQUEST_LOCK('TERMINAL.TERMINAL_ID', l_terminal_id);
		   
        l_batch_id := GET_OR_CREATE_BATCH(l_terminal_id, l_cust_bank_id, l_activation_date, l_fee_currency_id, 'N');
        SELECT SERVICE_FEE_SEQ.NEXTVAL
          INTO l_service_fee_id
          FROM DUAL;
        INSERT INTO SERVICE_FEES(
            SERVICE_FEE_ID,
            TERMINAL_ID,
            FEE_ID,
            FEE_AMOUNT,
            FREQUENCY_ID,
            LAST_PAYMENT,
            START_DATE)
       	  VALUES(
       	    l_service_fee_id,
       	    l_terminal_id,
       	    10,
       	    l_fee_amt,
       	    7,
       	    l_activation_date,
       	    l_activation_date);
       	    
        SELECT LEDGER_SEQ.NEXTVAL
          INTO l_ledger_id
          FROM DUAL;
        INSERT INTO LEDGER(
            LEDGER_ID,
            ENTRY_TYPE,
            SERVICE_FEE_ID,
            AMOUNT,
            ENTRY_DATE,
            BATCH_ID,
            SETTLE_STATE_ID,
            LEDGER_DATE,
            DESCRIPTION)
        VALUES (
            l_ledger_id,
            'SF',
            l_service_fee_id,
            -l_fee_amt,
            l_activation_date,
            l_batch_id,
            2,
            l_activation_date,
            l_desc);
    END;
    
    PROCEDURE SWITCH_PAYMENT_SCHEDULE(
        l_tran_id CORP.LEDGER.TRANS_ID%TYPE,
        l_payment_schedule_id  CORP.BATCH.PAYMENT_SCHEDULE_ID%TYPE)
    IS
        l_lock VARCHAR(128);
        l_adj_amt CORP.LEDGER.AMOUNT%TYPE;
        l_batch_id CORP.LEDGER.BATCH_ID%TYPE;
       	CURSOR l_cur IS
    		SELECT X.TRANS_TYPE_ID, X.CLOSE_DATE, X.SETTLE_DATE, X.TOTAL_AMOUNT, X.SETTLE_STATE_ID,
                   X.TERMINAL_ID, X.CUSTOMER_BANK_ID, X.PROCESS_FEE_ID, X.CURRENCY_ID, T.BUSINESS_UNIT_ID
    		  FROM REPORT.TRANS X
  		     INNER JOIN REPORT.TERMINAL T ON X.TERMINAL_ID = T.TERMINAL_ID
    		 WHERE TRAN_ID = l_tran_id;
    BEGIN
        FOR l_rec IN l_cur LOOP
            IF NVL(l_rec.terminal_id, 0) <> 0 AND NVL(l_rec.customer_bank_id, 0) <> 0 THEN
                l_lock := GLOBALS_PKG.REQUEST_LOCK('LEDGER.TRANS_ID',l_tran_id);
                -- Remove any open entries in different pay sched
                UPDATE CORP.LEDGER L SET DELETED = 'Y'
                 WHERE L.TRANS_ID = l_tran_id
                   AND L.DELETED = 'N'
                   AND EXISTS(
                      SELECT 1
                        FROM CORP.BATCH B
                       INNER JOIN CORP.DOC D ON B.DOC_ID = D.DOC_ID
                       WHERE B.PAYMENT_SCHEDULE_ID <> l_payment_schedule_id
                         AND B.BATCH_STATE_CD IN('O', 'L')
                         AND D.STATUS IN('O')
                         AND L.BATCH_ID = B.BATCH_ID);

                -- Make adjustment for old
                SELECT SUM(L.AMOUNT)
                  INTO l_adj_amt
                  FROM CORP.LEDGER L
                 INNER JOIN CORP.BATCH B ON L.BATCH_ID = B.BATCH_ID
                 INNER JOIN CORP.DOC D ON B.DOC_ID = D.DOC_ID
                 WHERE L.TRANS_ID = l_tran_id
                   AND L.DELETED = 'N'
                   AND D.STATUS NOT IN('D')
                   AND B.PAYMENT_SCHEDULE_ID <> l_payment_schedule_id
                   AND ENTRY_PAYABLE(l.settle_state_id,l.entry_type) = 'Y';

                IF NVL(l_adj_amt, 0) <> 0 THEN
                    l_batch_id := GET_OR_CREATE_BATCH(l_rec.terminal_id,
                                    l_rec.customer_bank_id, l_rec.close_date, l_rec.currency_id, 'A');
                    INSERT INTO LEDGER(
                        LEDGER_ID,
                        ENTRY_TYPE,
                        TRANS_ID,
                        PROCESS_FEE_ID,
                        AMOUNT,
                        ENTRY_DATE,
                        BATCH_ID,
                        SETTLE_STATE_ID,
                        LEDGER_DATE,
                        DESCRIPTION)
                    SELECT
                        LEDGER_SEQ.NEXTVAL,
                        'AD',
                        l_tran_id,
                        NULL,
                        -l_adj_amt,
                        l_rec.close_date,
                        l_batch_id,
                        l_rec.SETTLE_STATE_ID,
                        NVL(l_rec.SETTLE_DATE, SYSDATE),
                        'Payment schedule switched to ' || DESCRIPTION
                      FROM CORP.PAYMENT_SCHEDULE
                     WHERE PAYMENT_SCHEDULE_ID = l_payment_schedule_id;
                END IF;
                -- insert as new
                l_batch_id := GET_OR_CREATE_BATCH(l_rec.terminal_id, l_rec.customer_bank_id, l_rec.close_date, l_rec.currency_id, l_payment_schedule_id, l_rec.BUSINESS_UNIT_ID, 'N');
                INSERT_TRANS_TO_LEDGER(l_tran_id, l_rec.trans_type_id, l_rec.close_date, l_rec.settle_date,
                     l_rec.total_amount, l_rec.settle_state_id, l_rec.process_fee_id, l_batch_id);
            END IF;
        END LOOP;
    END;
	
	PROCEDURE CHECK_EFT_PROCESS_COMPLETION
	IS
		LD_EFT_PROPAGATION_START_TS DATE := TO_DATE(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EFT_PROPAGATION_LAST_RUN_START_TS'), 'MM/DD/YYYY HH24:MI:SS');
		LD_EFT_PROPAGATION_COMPLETE_TS DATE := TO_DATE(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EFT_PROPAGATION_LAST_RUN_COMPLETE_TS'), 'MM/DD/YYYY HH24:MI:SS');
		LV_EMAIL_FROM ENGINE.APP_SETTING.APP_SETTING_VALUE%TYPE := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EMAIL_FROM_ADDRESS');
		LV_EMAIL_TO ENGINE.APP_SETTING.APP_SETTING_VALUE%TYPE := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EFT_PROCESSING_EMAIL_TO');
		LV_EMAIL_CONTENT ENGINE.OB_EMAIL_QUEUE.OB_EMAIL_CONTENT%TYPE := 'Automatic EFT Processing is complete. Number of processed EFT(s) by currency:' || CHR(13) || CHR(10);
		LC_SEND_EMAIL CHAR(1) := 'N';
		LN_COUNT INTEGER;
	
		CURSOR L_CUR IS
			SELECT C.CURRENCY_NAME, COUNT(1) EFT_COUNT
			FROM CORP.DOC D
			JOIN CORP.CURRENCY C ON D.CURRENCY_ID = C.CURRENCY_ID
			WHERE D.AUTO_PROCESS_START_TS >= LD_EFT_PROPAGATION_START_TS
				AND D.STATUS != 'D'
			GROUP BY C.CURRENCY_NAME
			ORDER BY COUNT(1) DESC;
	BEGIN
		IF LD_EFT_PROPAGATION_COMPLETE_TS < TO_DATE(TO_CHAR(CURRENT_TIMESTAMP, 'MM/DD/YYYY'), 'MM/DD/YYYY') THEN
			RETURN;
		END IF;
	
		SELECT COUNT(1)
		INTO LN_COUNT
		FROM CORP.DOC
		WHERE AUTO_PROCESS_START_TS >= LD_EFT_PROPAGATION_START_TS
			AND AUTO_PROCESS_END_TS IS NULL;
			
		IF LN_COUNT > 0 THEN
			RETURN;
		END IF;
	
		FOR L_REC IN L_CUR LOOP
			IF LC_SEND_EMAIL = 'N' THEN
				LC_SEND_EMAIL := 'Y';
			END IF;
		
			LV_EMAIL_CONTENT := LV_EMAIL_CONTENT || CHR(13) || CHR(10) || L_REC.CURRENCY_NAME || ': ' || L_REC.EFT_COUNT;
		END LOOP;
		
		IF LC_SEND_EMAIL = 'Y' THEN
			LV_EMAIL_CONTENT := LV_EMAIL_CONTENT || CHR(13) || CHR(10) || CHR(13) || CHR(10) 
				|| DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DMS_URL')
				|| 'processedEFT.i?eft_from_date=' || TO_CHAR(CURRENT_TIMESTAMP, 'MM/DD/YYYY') || '&' || 'eft_from_time=00:00:00&' || 'eft_to_date='
				|| TO_CHAR(CURRENT_TIMESTAMP, 'MM/DD/YYYY') || '&' || 'eft_to_time=' || TO_CHAR(CURRENT_TIMESTAMP, 'HH24:MI:SS')
				|| '&' || 'auto_processed=Y&' || 'data_format=HTML&' || 'action=List+EFTs';
		
			INSERT INTO ENGINE.OB_EMAIL_QUEUE(OB_EMAIL_FROM_EMAIL_ADDR, OB_EMAIL_FROM_NAME, OB_EMAIL_MSG, OB_EMAIL_SUBJECT, OB_EMAIL_TO_EMAIL_ADDR, OB_EMAIL_TO_NAME, OB_EMAIL_CONTENT)
			SELECT LV_EMAIL_FROM, LV_EMAIL_FROM, ' ', 'EFT Processing', LV_EMAIL_TO, LV_EMAIL_TO, LV_EMAIL_CONTENT
			FROM DUAL
			WHERE NOT EXISTS (
				SELECT 1 FROM ENGINE.OB_EMAIL_QUEUE
				WHERE CREATED_TS >= LD_EFT_PROPAGATION_START_TS
					AND OB_EMAIL_SUBJECT = 'EFT Processing'
					AND OB_EMAIL_CONTENT LIKE 'Automatic EFT Processing is complete.%'
			);
		END IF;
		
		UPDATE ENGINE.APP_SETTING
   		SET APP_SETTING_VALUE = TO_CHAR(CURRENT_TIMESTAMP, 'MM/DD/YYYY HH24:MI:SS')
   		WHERE APP_SETTING_CD = 'EFT_PROCESSING_LAST_RUN_COMPLETE_TS';
	END;
	
	PROCEDURE START_EFT_PROPAGATION
	IS
	BEGIN
		UPDATE ENGINE.APP_SETTING
   		SET APP_SETTING_VALUE = TO_CHAR(CURRENT_TIMESTAMP, 'MM/DD/YYYY HH24:MI:SS')
   		WHERE APP_SETTING_CD = 'EFT_PROPAGATION_LAST_RUN_START_TS';
	END;
	
	PROCEDURE START_EFT_PROCESSING(
		PN_DOC_ID CORP.DOC.DOC_ID%TYPE)
	IS
	BEGIN
		UPDATE CORP.DOC
		SET AUTO_PROCESS_START_TS = CURRENT_TIMESTAMP
		WHERE DOC_ID = PN_DOC_ID;
	END;
	
	PROCEDURE PROCESS_EFT(
		PN_DOC_ID CORP.DOC.DOC_ID%TYPE)
	IS
    l_eft_cc_count NUMBER:=0;
    l_eft_max_cc_count NUMBER:=0;
	BEGIN
    select to_number(APP_SETTING_VALUE) into l_eft_max_cc_count from ENGINE.APP_SETTING where APP_SETTING_CD='EFT_MAX_CC_COUNT';
    select sum(ledger_count) into l_eft_cc_count from corp.customer_bank cb join corp.doc d on cb.customer_bank_id=d.customer_bank_id 
		join corp.batch b on b.doc_id=d.doc_id
		join corp.batch_total bt on bt.batch_id=b.batch_id and bt.entry_type='CC' and bt.payable='Y'
		where d.doc_id = PN_DOC_ID
    and cb.PAY_CYCLE_ID = 12;
    IF l_eft_cc_count >l_eft_max_cc_count THEN
      CORP.PAYMENTS_PKG.SPLIT_DOC(PN_DOC_ID);
    ELSE
      IF GET_DOC_STATUS(PN_DOC_ID) NOT IN ('P', 'S') THEN
        CORP.PAYMENTS_PKG.LOCK_DOC(PN_DOC_ID, NULL);
        CORP.PAYMENTS_PKG.APPROVE_PAYMENT(PN_DOC_ID, NULL);			
        CORP.PAYMENTS_PKG.MARK_DOC_PAID(PN_DOC_ID, NULL);
      END IF;
      
      UPDATE CORP.DOC
      SET AUTO_PROCESS_END_TS = CURRENT_TIMESTAMP
      WHERE DOC_ID = PN_DOC_ID;
      
      CHECK_EFT_PROCESS_COMPLETION;
    END IF;
		
	END;
	
	PROCEDURE COMPLETE_EFT_PROPAGATION
	IS
	BEGIN
		UPDATE ENGINE.APP_SETTING
   		SET APP_SETTING_VALUE = TO_CHAR(CURRENT_TIMESTAMP, 'MM/DD/YYYY HH24:MI:SS')
   		WHERE APP_SETTING_CD = 'EFT_PROPAGATION_LAST_RUN_COMPLETE_TS';
		
		CHECK_EFT_PROCESS_COMPLETION;
	END;
	
  PROCEDURE           SPLIT_DOC(
	l_doc_id CORP.DOC.DOC_ID%TYPE)
	IS
		l_min_amount NUMBER:=25;
		l_eft_max_cc_count NUMBER;
		l_cc_count NUMBER:=0;
		l_total_amount NUMBER;
		l_cb_id CORP.DOC.CUSTOMER_BANK_ID%TYPE;
	    l_cur_id CORP.DOC.CURRENCY_ID%TYPE;
	    l_bu_id CORP.DOC.BUSINESS_UNIT_ID%TYPE;
	    l_new_doc_id CORP.DOC.DOC_ID%TYPE;
	BEGIN
		IF GET_DOC_STATUS(l_doc_id) NOT IN ('P', 'S') THEN
	  CORP.PAYMENTS_PKG.LOCK_DOC(l_doc_id, NULL);
	  
	  SELECT D.CUSTOMER_BANK_ID, D.CURRENCY_ID, D.BUSINESS_UNIT_ID
	  INTO l_cb_id, l_cur_id, l_bu_id
	  FROM DOC D
	  WHERE D.DOC_ID = l_doc_id;
	   
	  select to_number(APP_SETTING_VALUE) into l_eft_max_cc_count 
	  from ENGINE.APP_SETTING where APP_SETTING_CD='EFT_MAX_CC_COUNT';
	                   
	  WHILE true LOOP
	  	  select sum(total_ledger_amount) into l_total_amount from corp.batch where doc_id=l_doc_id;
	      --dbms_output.put_line('before compare l_total_amount='||l_total_amount||' l_min_amount='||l_min_amount);
	  	  IF l_total_amount>= l_min_amount THEN
	  	  	  l_total_amount:=0;
	          l_cc_count:=0;
	          l_new_doc_id := CORP.PAYMENTS_PKG.GET_OR_CREATE_DOC(l_cb_id, l_cur_id, l_bu_id);
	           --dbms_output.put_line('before positive update doc_id='||l_new_doc_id);
	          FOR c_pos IN (select b.batch_id, b.total_ledger_amount, bt.ledger_count 
	          from corp.batch b join corp.batch_total bt on b.batch_id= bt.batch_id and bt.payable='Y' and bt.entry_type='CC' 
	          where b.doc_id=l_doc_id and b.total_ledger_amount>=0 order by b.batch_id) LOOP
	            IF l_cc_count+c_pos.ledger_count <= l_eft_max_cc_count or l_cc_count=0 THEN
	              l_cc_count:=l_cc_count+c_pos.ledger_count;
	              l_total_amount:=l_total_amount+c_pos.total_ledger_amount;
	              update corp.batch set doc_id=l_new_doc_id where batch_id=c_pos.batch_id;
	              --dbms_output.put_line('positive update:'||c_pos.batch_id||' to doc_id='||l_new_doc_id);
	            ELSE
	              EXIT;
	            END IF;
	          END LOOP;
	      
	          FOR c_neg IN (select b.batch_id, b.total_ledger_amount, nvl(bt.ledger_count,0) ledger_count
	          from corp.batch b left outer join corp.batch_total bt on b.batch_id= bt.batch_id and bt.payable='Y' and bt.entry_type='CC' 
	          where b.doc_id=l_doc_id and b.total_ledger_amount<=0 order by b.batch_id) LOOP
	              IF l_total_amount+c_neg.total_ledger_amount >= l_min_amount and l_cc_count+c_neg.ledger_count <= l_eft_max_cc_count THEN
	                l_cc_count:=l_cc_count+c_neg.ledger_count;
		              l_total_amount:=l_total_amount+c_neg.total_ledger_amount;
	                update corp.batch set doc_id=l_new_doc_id where batch_id=c_neg.batch_id;
	                --dbms_output.put_line('negative update:'||c_neg.batch_id||' to doc_id='||l_new_doc_id);
	              ELSE
	               EXIT;
	              END IF;
	          END LOOP;
	          --dbms_output.put_line('after negative doc_id='||l_new_doc_id);
	          IF l_total_amount > 0 THEN
	            CORP.PAYMENTS_PKG.LOCK_DOC(l_new_doc_id, NULL);
	            CORP.PAYMENTS_PKG.APPROVE_PAYMENT(l_new_doc_id, NULL);			
	            CORP.PAYMENTS_PKG.MARK_DOC_PAID(l_new_doc_id, NULL);
	            COMMIT;
	          ELSE
	            CORP.PAYMENTS_PKG.UNLOCK_DOC(l_doc_id);
	            EXIT;
	          END IF;         
	      ELSE
	        --dbms_output.put_line('unlock doc_id='||l_doc_id);
	        CORP.PAYMENTS_PKG.UNLOCK_DOC(l_doc_id);
	        EXIT;
	      END IF;
	  END LOOP;
	  END IF;
	END;
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/PKG_APP_USER.psk?rev=1.12
CREATE OR REPLACE PACKAGE REPORT.PKG_APP_USER AS
    PROCEDURE LOGIN_INTERNAL_USER(
        pn_login_user_id OUT REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_login_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pv_login_first_name REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_login_last_name REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_login_email REPORT.USER_LOGIN.EMAIL%TYPE,
        pv_login_time_zone_guid IN OUT REPORT.USER_LOGIN.TIME_ZONE_GUID%TYPE,
        pn_profile_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_profile_user_name OUT REPORT.USER_LOGIN.USER_NAME%TYPE,
        pn_profile_user_type OUT REPORT.USER_LOGIN.USER_TYPE%TYPE,
        pv_profile_first_name OUT REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_profile_last_name OUT REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_profile_email OUT REPORT.USER_LOGIN.EMAIL%TYPE,
        pv_profile_telephone OUT REPORT.USER_LOGIN.TELEPHONE%TYPE,
        pv_profile_fax OUT REPORT.USER_LOGIN.FAX%TYPE,
        pn_profile_customer_id OUT REPORT.USER_LOGIN.CUSTOMER_ID%TYPE,
        pv_profile_customer_name OUT CORP.CUSTOMER.CUSTOMER_NAME%TYPE,
        pt_profile_user_privileges OUT NUMBER_TABLE,
        pn_profile_active_bank_accts OUT PLS_INTEGER,
        pn_profile_pending_bank_accts OUT PLS_INTEGER,
        pn_profile_terminal_count OUT PLS_INTEGER,
        pv_profile_license_nbr OUT CORP.LICENSE_NBR.LICENSE_NBR%TYPE);
        
    --USALive 1.8.0 and above    
    PROCEDURE CHECK_EXTERNAL_USER(
        pn_login_user_id OUT REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_login_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pb_login_password_hash OUT REPORT.USER_LOGIN.PASSWORD_HASH%TYPE,
        pb_login_password_salt OUT REPORT.USER_LOGIN.PASSWORD_SALT%TYPE,
        pn_login_failure_count OUT REPORT.USER_LOGIN.LOGIN_FAILURE_COUNT%TYPE,
        pn_profile_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_profile_user_name OUT REPORT.USER_LOGIN.USER_NAME%TYPE,
        pn_profile_user_type OUT REPORT.USER_LOGIN.USER_TYPE%TYPE,
        pv_profile_first_name OUT REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_profile_last_name OUT REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_profile_email OUT REPORT.USER_LOGIN.EMAIL%TYPE,
        pv_profile_telephone OUT REPORT.USER_LOGIN.TELEPHONE%TYPE,
        pv_profile_fax OUT REPORT.USER_LOGIN.FAX%TYPE,
        pn_profile_customer_id OUT REPORT.USER_LOGIN.CUSTOMER_ID%TYPE,
        pv_profile_customer_name OUT CORP.CUSTOMER.CUSTOMER_NAME%TYPE,
        pt_profile_user_privileges OUT NUMBER_TABLE,
        pn_profile_active_bank_accts OUT PLS_INTEGER,
        pn_profile_pending_bank_accts OUT PLS_INTEGER,
        pn_missing_bank_acct_flag OUT VARCHAR2,
        pn_profile_terminal_count OUT PLS_INTEGER,
        pv_profile_license_nbr OUT CORP.LICENSE_NBR.LICENSE_NBR%TYPE);
        
    --USALive 1.8.0 
    PROCEDURE LOGIN_AS_USER(
        pn_login_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_profile_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_profile_user_name OUT REPORT.USER_LOGIN.USER_NAME%TYPE,
        pn_profile_user_type OUT REPORT.USER_LOGIN.USER_TYPE%TYPE,
        pv_profile_first_name OUT REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_profile_last_name OUT REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_profile_email OUT REPORT.USER_LOGIN.EMAIL%TYPE,
        pv_profile_telephone OUT REPORT.USER_LOGIN.TELEPHONE%TYPE,
        pv_profile_fax OUT REPORT.USER_LOGIN.FAX%TYPE,
        pn_profile_customer_id OUT REPORT.USER_LOGIN.CUSTOMER_ID%TYPE,
        pv_profile_customer_name OUT CORP.CUSTOMER.CUSTOMER_NAME%TYPE,
        pt_profile_user_privileges OUT NUMBER_TABLE,
        pn_profile_active_bank_accts OUT PLS_INTEGER,
        pn_profile_pending_bank_accts OUT PLS_INTEGER,
        pn_missing_bank_acct_flag OUT VARCHAR2,
        pn_profile_terminal_count OUT PLS_INTEGER,
        pv_profile_license_nbr OUT CORP.LICENSE_NBR.LICENSE_NBR%TYPE,
        pv_profile_readonly OUT VARCHAR2);
        
    PROCEDURE RECORD_FAILED_EXTERNAL_LOGIN(
        pn_login_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_login_failure_count OUT REPORT.USER_LOGIN.LOGIN_FAILURE_COUNT%TYPE);
        
    PROCEDURE RECORD_EXTERNAL_LOGIN(
        pn_login_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_login_time_zone_guid IN OUT REPORT.USER_LOGIN.TIME_ZONE_GUID%TYPE);
        
    FUNCTION CREATE_PASSCODE(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_passcode_type_id REPORT.PASSCODE_TYPE.PASSCODE_TYPE_ID%TYPE,
        pd_expiration_dt REPORT.USER_PASSCODE.EXPIRATION_TS%TYPE)
    RETURN REPORT.USER_PASSCODE.PASSCODE%TYPE;
    
    PROCEDURE RESET_PASSWORD(
        pv_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pv_passcode OUT REPORT.USER_PASSCODE.PASSCODE%TYPE,
        pv_email OUT REPORT.USER_LOGIN.EMAIL%TYPE);
    
    PROCEDURE CANCEL_PASSWORD_RESET(
        pv_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pv_passcode REPORT.USER_PASSCODE.PASSCODE%TYPE);
        
    PROCEDURE CHANGE_PASSWORD_BY_PASSCODE(
        pv_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pv_passcode REPORT.USER_PASSCODE.PASSCODE%TYPE,
        pb_password_hash REPORT.USER_LOGIN.PASSWORD_HASH%TYPE,
        pb_password_salt REPORT.USER_LOGIN.PASSWORD_SALT%TYPE); 
    
    FUNCTION CREATE_USER(
        pn_user_type REPORT.USER_LOGIN.USER_TYPE%TYPE,
        pv_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pv_first_name REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_last_name REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_email REPORT.USER_LOGIN.EMAIL%TYPE,
        pv_time_zone_guid REPORT.USER_LOGIN.TIME_ZONE_GUID%TYPE,
        pn_admin_id REPORT.USER_LOGIN.ADMIN_ID%TYPE,
        pn_customer_id REPORT.USER_LOGIN.CUSTOMER_ID%TYPE,
        pv_telephone REPORT.USER_LOGIN.TELEPHONE%TYPE,
        pv_fax REPORT.USER_LOGIN.FAX%TYPE,
        pb_password_hash REPORT.USER_LOGIN.PASSWORD_HASH%TYPE,
        pb_password_salt REPORT.USER_LOGIN.PASSWORD_SALT%TYPE,
        pt_user_privileges NUMBER_TABLE)
    RETURN REPORT.USER_LOGIN.USER_ID%TYPE;

    PROCEDURE DELETE_USER(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE);
            
    PROCEDURE UPDATE_USER(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_user_type REPORT.USER_LOGIN.USER_TYPE%TYPE,
        pv_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pv_first_name REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_last_name REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_email REPORT.USER_LOGIN.EMAIL%TYPE,
        pn_admin_id REPORT.USER_LOGIN.ADMIN_ID%TYPE,
        pn_customer_id REPORT.USER_LOGIN.CUSTOMER_ID%TYPE,
        pv_telephone REPORT.USER_LOGIN.TELEPHONE%TYPE,
        pv_fax REPORT.USER_LOGIN.FAX%TYPE,
        pb_password_hash REPORT.USER_LOGIN.PASSWORD_HASH%TYPE,
        pb_password_salt REPORT.USER_LOGIN.PASSWORD_SALT%TYPE,
        pt_user_privileges NUMBER_TABLE,
        pn_updating_user_id REPORT.USER_LOGIN.USER_ID%TYPE);
    
    PROCEDURE CREATE_CUSTOMER(
        pn_user_id OUT CORP.CUSTOMER.USER_ID%TYPE,
        pn_cust_id OUT CORP.CUSTOMER.CUSTOMER_ID%TYPE,
        pv_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pv_first_name REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_last_name REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_email REPORT.USER_LOGIN.EMAIL%TYPE,
        pv_time_zone_guid REPORT.USER_LOGIN.TIME_ZONE_GUID%TYPE,
        pb_password_hash REPORT.USER_LOGIN.PASSWORD_HASH%TYPE,
        pb_password_salt REPORT.USER_LOGIN.PASSWORD_SALT%TYPE,
        pv_cust_name IN CORP.CUSTOMER.CUSTOMER_NAME%TYPE,
        pv_addr1 IN CORP.CUSTOMER_ADDR.ADDRESS1%TYPE,
        pv_city IN CORP.CUSTOMER_ADDR.CITY%TYPE,
        pv_state_cd IN CORP.CUSTOMER_ADDR.STATE%TYPE,
        pv_postal IN CORP.CUSTOMER_ADDR.ZIP%TYPE,
        pv_country_cd IN CORP.CUSTOMER_ADDR.COUNTRY_CD%TYPE,       
        pv_telephone REPORT.USER_LOGIN.TELEPHONE%TYPE,
        pv_fax REPORT.USER_LOGIN.FAX%TYPE,
        pn_dealer_id IN CORP.CUSTOMER.DEALER_ID%TYPE,
        pv_tax_id_nbr IN CORP.CUSTOMER.TAX_ID_NBR%TYPE,
        pv_doing_business_as IN CORP.CUSTOMER.DOING_BUSINESS_AS%TYPE DEFAULT NULL);
        
    PROCEDURE CREATE_CUSTOM_REPORT_LINK(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_label WEB_CONTENT.WEB_LINK.WEB_LINK_LABEL%TYPE,
        pv_url WEB_CONTENT.WEB_LINK.WEB_LINK_URL%TYPE,
        pv_desc WEB_CONTENT.WEB_LINK.WEB_LINK_DESC%TYPE,        
        pn_order WEB_CONTENT.WEB_LINK.WEB_LINK_ORDER%TYPE,
        pv_group WEB_CONTENT.WEB_LINK.WEB_LINK_GROUP%TYPE DEFAULT 'User Defined');
    
    PROCEDURE DELETE_USER_LINK(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_link_id WEB_CONTENT.WEB_LINK.WEB_LINK_ID%TYPE);
    
     FUNCTION GET_USER_PREFERENCE(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_pref_id REPORT.PREFERENCE.PREFERENCE_ID%TYPE)
        RETURN REPORT.USER_PREFERENCE.PREFERENCE_VALUE%TYPE;
    
     FUNCTION GET_HEALTH_CODE(
        pn_measured_date DATE,
        pn_min_days NUMBER,
        pn_max_days NUMBER)
        RETURN CHAR;
        
     FUNCTION GET_HEALTH_CODE_BY_PREF(
        pn_date DATE,
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_pref_id REPORT.PREFERENCE.PREFERENCE_ID%TYPE)
        RETURN CHAR;
        
    PROCEDURE UPSERT_USER_PREFERENCE(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_preference_id REPORT.PREFERENCE.PREFERENCE_ID%TYPE,
        pv_preference_value REPORT.USER_PREFERENCE.PREFERENCE_VALUE%TYPE);
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/PKG_APP_USER.pbk?rev=1.19
CREATE OR REPLACE PACKAGE BODY REPORT.PKG_APP_USER AS
    PROCEDURE POPULATE_USER(
        pn_profile_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pc_internal_flag CHAR,
        pv_profile_user_name OUT REPORT.USER_LOGIN.USER_NAME%TYPE,
        pn_profile_user_type OUT REPORT.USER_LOGIN.USER_TYPE%TYPE,
        pv_profile_first_name OUT REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_profile_last_name OUT REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_profile_email OUT REPORT.USER_LOGIN.EMAIL%TYPE,
        pv_profile_telephone OUT REPORT.USER_LOGIN.TELEPHONE%TYPE,
        pv_profile_fax OUT REPORT.USER_LOGIN.FAX%TYPE,
        pn_profile_customer_id OUT REPORT.USER_LOGIN.CUSTOMER_ID%TYPE,
        pv_profile_customer_name OUT CORP.CUSTOMER.CUSTOMER_NAME%TYPE,
        pt_profile_user_privileges OUT NUMBER_TABLE,
        pn_profile_active_bank_accts OUT PLS_INTEGER,
        pn_profile_pending_bank_accts OUT PLS_INTEGER,
        pn_customer_active_bank_accts OUT PLS_INTEGER,
        pn_profile_terminal_count OUT PLS_INTEGER,
        pv_profile_license_nbr OUT CORP.LICENSE_NBR.LICENSE_NBR%TYPE)
    AS
        lc_internal_flag CHAR(1);
    BEGIN
        SELECT U.USER_NAME, U.USER_TYPE, U.FIRST_NAME, U.LAST_NAME, U.EMAIL, U.TELEPHONE, U.FAX, NVL(C.CUSTOMER_ID, 0), C.CUSTOMER_NAME
          INTO pv_profile_user_name, pn_profile_user_type, pv_profile_first_name, pv_profile_last_name, pv_profile_email, pv_profile_telephone, pv_profile_fax, pn_profile_customer_id, pv_profile_customer_name 
          FROM REPORT.USER_LOGIN U
          LEFT OUTER JOIN CORP.CUSTOMER C ON U.CUSTOMER_ID = C.CUSTOMER_ID AND C.STATUS != 'D'
         WHERE U.USER_ID = pn_profile_user_id
           AND U.STATUS = 'A';
        
        -- get counts and privs
        IF pc_internal_flag != '?' THEN
            lc_internal_flag := pc_internal_flag;
        ELSIF pv_profile_user_name LIKE '%@usatech.com' AND pn_profile_user_type != 8 THEN
            lc_internal_flag := 'Y';
        ELSE
            lc_internal_flag := 'N';
        END IF;
        
        SELECT UP.PRIV_ID
          BULK COLLECT INTO pt_profile_user_privileges
          FROM REPORT.USER_PRIVS UP 
          JOIN REPORT.PRIV P on UP.PRIV_ID=P.PRIV_ID
         WHERE UP.USER_ID = pn_profile_user_id
           AND (P.INTERNAL_EXTERNAL_FLAG = 'B' OR (lc_internal_flag = 'Y' AND P.INTERNAL_EXTERNAL_FLAG = 'I') OR (lc_internal_flag = 'N' AND P.INTERNAL_EXTERNAL_FLAG IN('E', 'P')));
                   
        SELECT COUNT(*)
          INTO pn_profile_terminal_count
          FROM REPORT.VW_USER_TERMINAL UT
         WHERE UT.USER_ID = pn_profile_user_id;
         
        SELECT COUNT(DISTINCT DECODE(UCB.STATUS, 'A', UCB.CUSTOMER_BANK_ID)) ACTIVE_BANK_ACCT_COUNT, COUNT(DISTINCT DECODE(UCB.STATUS, 'P', UCB.CUSTOMER_BANK_ID)) PENDING_BANK_ACCT_COUNT
          INTO pn_profile_active_bank_accts, pn_profile_pending_bank_accts
          FROM REPORT.VW_USER_CUSTOMER_BANK UCB
	     WHERE UCB.USER_ID = pn_profile_user_id;
         
        IF pn_profile_user_type = 8 THEN
            SELECT MAX(LICENSE_NBR)
              INTO pv_profile_license_nbr
              FROM (SELECT CL.LICENSE_NBR
                      FROM CORP.CUSTOMER_LICENSE CL
                     WHERE CUSTOMER_ID = pn_profile_customer_id
                     ORDER BY CL.RECEIVED DESC)
             WHERE ROWNUM = 1;
            SELECT COUNT(*)
              INTO pn_customer_active_bank_accts
              FROM CORP.CUSTOMER_BANK
             WHERE CUSTOMER_ID = pn_profile_customer_id
               AND STATUS = 'A';
        ELSE
            pn_profile_pending_bank_accts := 0;
            pn_customer_active_bank_accts := 0;
        END IF; 
    END;

    PROCEDURE LOGIN_INTERNAL_USER(
        pn_login_user_id OUT REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_login_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pv_login_first_name REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_login_last_name REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_login_email REPORT.USER_LOGIN.EMAIL%TYPE,
        pv_login_time_zone_guid IN OUT REPORT.USER_LOGIN.TIME_ZONE_GUID%TYPE,
        pn_profile_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_profile_user_name OUT REPORT.USER_LOGIN.USER_NAME%TYPE,
        pn_profile_user_type OUT REPORT.USER_LOGIN.USER_TYPE%TYPE,
        pv_profile_first_name OUT REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_profile_last_name OUT REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_profile_email OUT REPORT.USER_LOGIN.EMAIL%TYPE,
        pv_profile_telephone OUT REPORT.USER_LOGIN.TELEPHONE%TYPE,
        pv_profile_fax OUT REPORT.USER_LOGIN.FAX%TYPE,
        pn_profile_customer_id OUT REPORT.USER_LOGIN.CUSTOMER_ID%TYPE,
        pv_profile_customer_name OUT CORP.CUSTOMER.CUSTOMER_NAME%TYPE,
        pt_profile_user_privileges OUT NUMBER_TABLE,
        pn_profile_active_bank_accts OUT PLS_INTEGER,
        pn_profile_pending_bank_accts OUT PLS_INTEGER,
        pn_profile_terminal_count OUT PLS_INTEGER,
        pv_profile_license_nbr OUT CORP.LICENSE_NBR.LICENSE_NBR%TYPE)
    AS
        ln_profile_user_id REPORT.USER_LOGIN.USER_ID%TYPE;
        ln_customer_active_bank_accts PLS_INTEGER;
    BEGIN
        UPDATE REPORT.USER_LOGIN
           SET LAST_LOGIN_TS = SYSDATE,
               FIRST_NAME = NVL(pv_login_first_name, FIRST_NAME),
               LAST_NAME = NVL(pv_login_last_name, LAST_NAME),
               EMAIL = NVL(pv_login_email, EMAIL),
               TIME_ZONE_GUID = NVL(pv_login_time_zone_guid, TIME_ZONE_GUID),
               STATUS = 'A'
         WHERE USER_NAME = pv_login_user_name
           AND USER_TYPE != 8
         RETURNING USER_ID, TIME_ZONE_GUID
              INTO pn_login_user_id, pv_login_time_zone_guid;
        IF SQL%NOTFOUND THEN
            pn_login_user_id := CREATE_USER(9, pv_login_user_name, pv_login_first_name, pv_login_last_name, pv_login_email, pv_login_time_zone_guid,  NULL, 0, NULL, NULL, NULL, NULL, NULL); 
        END IF;
        IF pn_profile_user_id IS NULL THEN
            ln_profile_user_id := pn_login_user_id;
        ELSIF REPORT.CAN_ADMIN_USER(pn_profile_user_id, pn_login_user_id)  = 'N' THEN
            IF REPORT.CHECK_PRIV(pn_login_user_id,10)='Y' THEN
              ln_profile_user_id := pn_profile_user_id;
            ELSE
              RAISE_APPLICATION_ERROR(-20100, 'User ''' || pv_login_user_name || ''' may not log in as user id ' || pn_profile_user_id); 
            END IF;
        ELSE
            ln_profile_user_id := pn_profile_user_id;
        END IF;
        POPULATE_USER(
            ln_profile_user_id,
            'Y',
            pv_profile_user_name,
            pn_profile_user_type,
            pv_profile_first_name,
            pv_profile_last_name,
            pv_profile_email,
            pv_profile_telephone,
            pv_profile_fax,
            pn_profile_customer_id,
            pv_profile_customer_name,
            pt_profile_user_privileges,
            pn_profile_active_bank_accts,
            pn_profile_pending_bank_accts,
            ln_customer_active_bank_accts,
            pn_profile_terminal_count,
            pv_profile_license_nbr);
    END;

    PROCEDURE CHECK_EXTERNAL_USER(
        pn_login_user_id OUT REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_login_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pb_login_password_hash OUT REPORT.USER_LOGIN.PASSWORD_HASH%TYPE,
        pb_login_password_salt OUT REPORT.USER_LOGIN.PASSWORD_SALT%TYPE,
        pn_login_failure_count OUT REPORT.USER_LOGIN.LOGIN_FAILURE_COUNT%TYPE,
        pn_profile_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_profile_user_name OUT REPORT.USER_LOGIN.USER_NAME%TYPE,
        pn_profile_user_type OUT REPORT.USER_LOGIN.USER_TYPE%TYPE,
        pv_profile_first_name OUT REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_profile_last_name OUT REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_profile_email OUT REPORT.USER_LOGIN.EMAIL%TYPE,
        pv_profile_telephone OUT REPORT.USER_LOGIN.TELEPHONE%TYPE,
        pv_profile_fax OUT REPORT.USER_LOGIN.FAX%TYPE,
        pn_profile_customer_id OUT REPORT.USER_LOGIN.CUSTOMER_ID%TYPE,
        pv_profile_customer_name OUT CORP.CUSTOMER.CUSTOMER_NAME%TYPE,
        pt_profile_user_privileges OUT NUMBER_TABLE,
        pn_profile_active_bank_accts OUT PLS_INTEGER,
        pn_profile_pending_bank_accts OUT PLS_INTEGER,
        pn_missing_bank_acct_flag OUT VARCHAR2,
        pn_profile_terminal_count OUT PLS_INTEGER,
        pv_profile_license_nbr OUT CORP.LICENSE_NBR.LICENSE_NBR%TYPE)
    AS
        ln_profile_user_id REPORT.USER_LOGIN.USER_ID%TYPE;
        ln_customer_active_bank_accts PLS_INTEGER;
    BEGIN
        SELECT U.USER_ID, U.PASSWORD_HASH, U.PASSWORD_SALT, U.LOGIN_FAILURE_COUNT
          INTO pn_login_user_id, pb_login_password_hash, pb_login_password_salt, pn_login_failure_count
          FROM REPORT.USER_LOGIN U
         WHERE U.USER_NAME = pv_login_user_name
           AND U.STATUS = 'A'
           AND U.USER_TYPE = 8;
        IF pn_profile_user_id IS NULL THEN
            ln_profile_user_id := pn_login_user_id;
        ELSIF REPORT.CAN_ADMIN_USER(pn_profile_user_id, pn_login_user_id)  = 'N' THEN
            RAISE_APPLICATION_ERROR(-20100, 'User ''' || pv_login_user_name || ''' may not log in as user id ' || pn_profile_user_id); 
        ELSE
            ln_profile_user_id := pn_profile_user_id;
        END IF;
        POPULATE_USER(
            ln_profile_user_id,
            'N',
            pv_profile_user_name,
            pn_profile_user_type,
            pv_profile_first_name,
            pv_profile_last_name,
            pv_profile_email,
            pv_profile_telephone,
            pv_profile_fax,
            pn_profile_customer_id,
            pv_profile_customer_name,
            pt_profile_user_privileges,
            pn_profile_active_bank_accts,
            pn_profile_pending_bank_accts,
            ln_customer_active_bank_accts,
            pn_profile_terminal_count,
            pv_profile_license_nbr);
        IF ln_customer_active_bank_accts > 0 THEN
            pn_missing_bank_acct_flag := 'N';
        ELSE
            pn_missing_bank_acct_flag := 'Y';
        END IF;
    END;
    
    
    PROCEDURE LOGIN_AS_USER(
        pn_login_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_profile_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_profile_user_name OUT REPORT.USER_LOGIN.USER_NAME%TYPE,
        pn_profile_user_type OUT REPORT.USER_LOGIN.USER_TYPE%TYPE,
        pv_profile_first_name OUT REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_profile_last_name OUT REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_profile_email OUT REPORT.USER_LOGIN.EMAIL%TYPE,
        pv_profile_telephone OUT REPORT.USER_LOGIN.TELEPHONE%TYPE,
        pv_profile_fax OUT REPORT.USER_LOGIN.FAX%TYPE,
        pn_profile_customer_id OUT REPORT.USER_LOGIN.CUSTOMER_ID%TYPE,
        pv_profile_customer_name OUT CORP.CUSTOMER.CUSTOMER_NAME%TYPE,
        pt_profile_user_privileges OUT NUMBER_TABLE,
        pn_profile_active_bank_accts OUT PLS_INTEGER,
        pn_profile_pending_bank_accts OUT PLS_INTEGER,
        pn_missing_bank_acct_flag OUT VARCHAR2,
        pn_profile_terminal_count OUT PLS_INTEGER,
        pv_profile_license_nbr OUT CORP.LICENSE_NBR.LICENSE_NBR%TYPE,
        pv_profile_readonly OUT VARCHAR2)
    AS
        lv_permit CHAR(1);
        ln_customer_active_bank_accts PLS_INTEGER;
    BEGIN
        lv_permit := REPORT.CAN_ADMIN_USER(pn_profile_user_id, pn_login_user_id);
        IF lv_permit = 'N' AND REPORT.CHECK_PRIV(pn_login_user_id,10)!='Y' THEN
            RAISE_APPLICATION_ERROR(-20100, 'User id' || pn_login_user_id || ' may not log in as user id ' || pn_profile_user_id); 
        END IF;
        POPULATE_USER(
            pn_profile_user_id,
            '?',
            pv_profile_user_name,
            pn_profile_user_type,
            pv_profile_first_name,
            pv_profile_last_name,
            pv_profile_email,
            pv_profile_telephone,
            pv_profile_fax,
            pn_profile_customer_id,
            pv_profile_customer_name,
            pt_profile_user_privileges,
            pn_profile_active_bank_accts,
            pn_profile_pending_bank_accts,
            ln_customer_active_bank_accts,
            pn_profile_terminal_count,
            pv_profile_license_nbr); 
        IF lv_permit = 'Y' THEN
            pv_profile_readonly := 'N';
        ELSE
            pv_profile_readonly := 'Y';
        END IF;
        IF ln_customer_active_bank_accts > 0 THEN
            pn_missing_bank_acct_flag := 'N';
        ELSE
            pn_missing_bank_acct_flag := 'Y';
        END IF;
    END;
    
    PROCEDURE RECORD_FAILED_EXTERNAL_LOGIN(
        pn_login_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_login_failure_count OUT REPORT.USER_LOGIN.LOGIN_FAILURE_COUNT%TYPE)
    AS
    BEGIN
        UPDATE REPORT.USER_LOGIN
           SET LOGIN_FAILURE_COUNT = LOGIN_FAILURE_COUNT + 1
         WHERE USER_ID = pn_login_user_id
          RETURNING LOGIN_FAILURE_COUNT 
          INTO pn_login_failure_count;
    END;
        
    PROCEDURE RECORD_EXTERNAL_LOGIN(
        pn_login_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_login_time_zone_guid IN OUT REPORT.USER_LOGIN.TIME_ZONE_GUID%TYPE)
    AS
    BEGIN
        UPDATE REPORT.USER_LOGIN
           SET LAST_LOGIN_TS = SYSDATE,
               TIME_ZONE_GUID = NVL(pv_login_time_zone_guid, TIME_ZONE_GUID),
               LOGIN_FAILURE_COUNT = 0
         WHERE USER_ID = pn_login_user_id
          RETURNING TIME_ZONE_GUID
          INTO pv_login_time_zone_guid;
    END;
    
    FUNCTION CREATE_PASSCODE(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_passcode_type_id REPORT.PASSCODE_TYPE.PASSCODE_TYPE_ID%TYPE,
        pd_expiration_dt REPORT.USER_PASSCODE.EXPIRATION_TS%TYPE)
    RETURN REPORT.USER_PASSCODE.PASSCODE%TYPE
    IS
        lv_passcode REPORT.USER_PASSCODE.PASSCODE%TYPE;
    BEGIN
        lv_passcode := DBMS_RANDOM.STRING('A', 30);
        INSERT INTO REPORT.USER_PASSCODE(USER_PASSCODE_ID, USER_ID, PASSCODE, PASSCODE_TYPE_ID, EXPIRATION_TS)
            VALUES(REPORT.SEQ_USER_PASSCODE_ID.NEXTVAL, pn_user_id, lv_passcode, pn_passcode_type_id, pd_expiration_dt);
        RETURN lv_passcode;
    EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
            RETURN CREATE_PASSCODE(pn_user_id, pn_passcode_type_id, pd_expiration_dt);
    END;
    
    PROCEDURE RESET_PASSWORD(
        pv_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pv_passcode OUT REPORT.USER_PASSCODE.PASSCODE%TYPE,
        pv_email OUT REPORT.USER_LOGIN.EMAIL%TYPE)
    IS
        ln_user_id REPORT.USER_LOGIN.USER_ID%TYPE;
        ld_expiration_dt REPORT.USER_PASSCODE.EXPIRATION_TS%TYPE;
    BEGIN
        SELECT SYSDATE + DURATION_DAYS
          INTO ld_expiration_dt
          FROM REPORT.PASSCODE_TYPE
         WHERE PASSCODE_TYPE_ID = 1;
        UPDATE REPORT.USER_LOGIN
           SET PASSWORD_RESET_FLAG = 'Y'
         WHERE USER_NAME = pv_user_name
         RETURNING USER_ID, EMAIL INTO ln_user_id, pv_email;
        IF ln_user_id IS NULL OR pv_email IS NULL THEN
            RAISE_APPLICATION_ERROR(-20100, 'User ''' || pv_user_name || ''' is not configured for password reset because an email is not registerd'); 
        END IF;
        pv_passcode := CREATE_PASSCODE(ln_user_id, 1, ld_expiration_dt);
    END;
    
    PROCEDURE CANCEL_PASSWORD_RESET(
        pv_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pv_passcode REPORT.USER_PASSCODE.PASSCODE%TYPE)
    IS
        ln_user_id REPORT.USER_LOGIN.USER_ID%TYPE;
        lc_password_reset_flag REPORT.USER_LOGIN.PASSWORD_RESET_FLAG%TYPE;
    BEGIN
        SELECT USER_ID, PASSWORD_RESET_FLAG
          INTO ln_user_id, lc_password_reset_flag
          FROM REPORT.USER_LOGIN
         WHERE USER_NAME = pv_user_name;
        IF lc_password_reset_flag = 'N' THEN
            RAISE_APPLICATION_ERROR(-20400, 'Password has already been cancelled');
        END IF;
        UPDATE REPORT.USER_PASSCODE
           SET EXPIRATION_TS = SYSDATE
         WHERE PASSCODE = pv_passcode
           AND USER_ID = ln_user_id;
        IF SQL%NOTFOUND THEN
             RAISE_APPLICATION_ERROR(-20401, 'Invalid passcode');
        END IF;
        UPDATE REPORT.USER_LOGIN
           SET PASSWORD_RESET_FLAG = 'N'
         WHERE USER_ID = ln_user_id;        
    END;
    
    PROCEDURE CHANGE_PASSWORD_BY_PASSCODE(
        pv_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pv_passcode REPORT.USER_PASSCODE.PASSCODE%TYPE,
        pb_password_hash REPORT.USER_LOGIN.PASSWORD_HASH%TYPE,
        pb_password_salt REPORT.USER_LOGIN.PASSWORD_SALT%TYPE)
    IS
        ln_user_id REPORT.USER_LOGIN.USER_ID%TYPE;
        lc_password_reset_flag REPORT.USER_LOGIN.PASSWORD_RESET_FLAG%TYPE;
    BEGIN
        SELECT USER_ID, PASSWORD_RESET_FLAG
          INTO ln_user_id, lc_password_reset_flag
          FROM REPORT.USER_LOGIN
         WHERE USER_NAME = pv_user_name;
        IF lc_password_reset_flag = 'N' THEN
            RAISE_APPLICATION_ERROR(-20400, 'Password has already been changed');
        END IF;
        UPDATE REPORT.USER_PASSCODE
           SET EXPIRATION_TS = SYSDATE
         WHERE PASSCODE = pv_passcode
           AND USER_ID = ln_user_id;
        IF SQL%NOTFOUND THEN
             RAISE_APPLICATION_ERROR(-20401, 'Invalid passcode');
        END IF;
        UPDATE REPORT.USER_LOGIN
           SET PASSWORD_RESET_FLAG = 'N',
               PASSWORD_HASH = pb_password_hash,
               PASSWORD_SALT = pb_password_salt
         WHERE USER_ID = ln_user_id;        
    END; 
    
    FUNCTION CREATE_USER(
        pn_user_type REPORT.USER_LOGIN.USER_TYPE%TYPE,
        pv_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pv_first_name REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_last_name REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_email REPORT.USER_LOGIN.EMAIL%TYPE,
        pv_time_zone_guid REPORT.USER_LOGIN.TIME_ZONE_GUID%TYPE,
        pn_admin_id REPORT.USER_LOGIN.ADMIN_ID%TYPE,
        pn_customer_id REPORT.USER_LOGIN.CUSTOMER_ID%TYPE,
        pv_telephone REPORT.USER_LOGIN.TELEPHONE%TYPE,
        pv_fax REPORT.USER_LOGIN.FAX%TYPE,
        pb_password_hash REPORT.USER_LOGIN.PASSWORD_HASH%TYPE,
        pb_password_salt REPORT.USER_LOGIN.PASSWORD_SALT%TYPE,
        pt_user_privileges NUMBER_TABLE)
    RETURN REPORT.USER_LOGIN.USER_ID%TYPE
    IS
		ln_user_id REPORT.USER_LOGIN.USER_ID%TYPE;
	BEGIN
		SELECT REPORT.USER_LOGIN_SEQ.NEXTVAL 
		  INTO ln_user_id
		  FROM DUAL;
	 	INSERT INTO REPORT.USER_LOGIN(
            USER_ID, 
            USER_TYPE, 
            USER_NAME, 
            FIRST_NAME, 
            LAST_NAME, 
            EMAIL, 
            TIME_ZONE_GUID,
            ADMIN_ID, 
            CUSTOMER_ID, 
            TELEPHONE, 
            FAX, 
            PASSWORD_HASH, 
            PASSWORD_SALT,
            LAST_LOGIN_TS)
          VALUES(
            ln_user_id, 
            pn_user_type,
            pv_user_name,
            pv_first_name,
            pv_last_name,
            pv_email,
            pv_time_zone_guid,
            NVL(pn_admin_id, 0),
            NVL(pn_customer_id, 0),
            pv_telephone,
            pv_fax,
            pb_password_hash,
            pb_password_salt,
            SYSDATE);
        IF pt_user_privileges IS NOT NULL THEN
            IF NVL(pn_admin_id, 0) = 0 THEN
                INSERT INTO REPORT.USER_PRIVS(USER_ID, PRIV_ID)
                  SELECT ln_user_id, P.PRIV_ID
                    FROM REPORT.PRIV P
                   WHERE P.PRIV_ID MEMBER OF pt_user_privileges
                     AND (pn_user_type != 8 OR P.INTERNAL_EXTERNAL_FLAG != 'I');
            ELSE
                INSERT INTO REPORT.USER_PRIVS(USER_ID, PRIV_ID)
                  SELECT ln_user_id, P.PRIV_ID
                    FROM REPORT.PRIV P
                    JOIN REPORT.USER_PRIVS UP ON UP.PRIV_ID = P.PRIV_ID
                    JOIN REPORT.USER_LOGIN U ON UP.USER_ID = U.USER_ID
                   WHERE P.PRIV_ID MEMBER OF pt_user_privileges
                     AND UP.USER_ID = pn_admin_id 
                     AND (U.USER_TYPE != 8 OR P.INTERNAL_EXTERNAL_FLAG IN('B', 'E'))
                     AND (pn_user_type != 8 OR P.INTERNAL_EXTERNAL_FLAG != 'I');
            END IF;
        END IF;
        INSERT INTO REPORT.USER_DISPLAY(USER_ID, DISPLAY_ID, SEQ)
            SELECT ln_user_id, 1, 1 FROM DUAL
            UNION ALL
            SELECT ln_user_id, 2, 2 FROM DUAL;
        INSERT INTO REPORT.REPORT_REQUEST_ORDER (USER_ID, PROFILE_MAX_REQUEST_ORDER,USER_MAX_REQUEST_ORDER)
        VALUES(ln_user_id, 0, 0);
		RETURN ln_user_id;
	END;

    PROCEDURE DELETE_USER(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE)
    IS
    BEGIN
        UPDATE REPORT.USER_LOGIN 
           SET STATUS = 'D' 
         WHERE USER_ID = pn_user_id;
        DELETE FROM REPORT.USER_PRIVS 
         WHERE USER_ID = pn_user_id;
    END;
            
    PROCEDURE UPDATE_USER(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_user_type REPORT.USER_LOGIN.USER_TYPE%TYPE,
        pv_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pv_first_name REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_last_name REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_email REPORT.USER_LOGIN.EMAIL%TYPE,
        pn_admin_id REPORT.USER_LOGIN.ADMIN_ID%TYPE,
        pn_customer_id REPORT.USER_LOGIN.CUSTOMER_ID%TYPE,
        pv_telephone REPORT.USER_LOGIN.TELEPHONE%TYPE,
        pv_fax REPORT.USER_LOGIN.FAX%TYPE,
        pb_password_hash REPORT.USER_LOGIN.PASSWORD_HASH%TYPE,
        pb_password_salt REPORT.USER_LOGIN.PASSWORD_SALT%TYPE,
        pt_user_privileges NUMBER_TABLE,
        pn_updating_user_id REPORT.USER_LOGIN.USER_ID%TYPE)
    IS
        ln_user_type REPORT.USER_LOGIN.USER_TYPE%TYPE;
	BEGIN
		UPDATE REPORT.USER_LOGIN
          SET USER_TYPE = NVL(pn_user_type, USER_TYPE), 
            USER_NAME = NVL(pv_user_name, USER_NAME), 
            FIRST_NAME = NVL(pv_first_name, FIRST_NAME), 
            LAST_NAME = NVL(pv_last_name, LAST_NAME), 
            EMAIL = NVL(pv_email, EMAIL), 
            ADMIN_ID = NVL(pn_admin_id, ADMIN_ID), 
            CUSTOMER_ID = NVL(pn_customer_id, CUSTOMER_ID), 
            TELEPHONE = NVL(pv_telephone, TELEPHONE), 
            FAX = NVL(pv_fax, FAX), 
            PASSWORD_HASH = NVL(pb_password_hash, PASSWORD_HASH), 
            PASSWORD_SALT = NVL(pb_password_salt, PASSWORD_SALT),
            PASSWORD_RESET_FLAG = DECODE(PASSWORD_HASH, NULL, PASSWORD_RESET_FLAG, 'N')
         WHERE USER_ID = pn_user_id
         RETURNING USER_TYPE INTO ln_user_type;
        IF pn_updating_user_id != pn_user_id AND pt_user_privileges IS NOT NULL THEN
            DELETE
              FROM REPORT.USER_PRIVS
             WHERE USER_ID = pn_user_id
               AND PRIV_ID IN(
                   SELECT UP.PRIV_ID 
                     FROM REPORT.USER_PRIVS UP
                     JOIN REPORT.USER_LOGIN U ON UP.USER_ID = U.USER_ID
                     JOIN REPORT.PRIV P ON UP.PRIV_ID = P.PRIV_ID
                    WHERE UP.USER_ID = pn_updating_user_id 
                      AND (U.USER_TYPE != 8 OR P.INTERNAL_EXTERNAL_FLAG IN('B', 'E')));
            INSERT INTO REPORT.USER_PRIVS(USER_ID, PRIV_ID)
              SELECT pn_user_id, P.PRIV_ID
                FROM REPORT.PRIV P
                JOIN REPORT.USER_PRIVS UP ON UP.PRIV_ID = P.PRIV_ID
                JOIN REPORT.USER_LOGIN U ON UP.USER_ID = U.USER_ID
               WHERE P.PRIV_ID MEMBER OF pt_user_privileges
                 AND UP.USER_ID = pn_updating_user_id 
                 AND (U.USER_TYPE != 8 OR P.INTERNAL_EXTERNAL_FLAG IN('B', 'E'))
                 AND (ln_user_type != 8 OR P.INTERNAL_EXTERNAL_FLAG != 'I');
        END IF;
	END;
    
    PROCEDURE CREATE_CUSTOMER(
        pn_user_id OUT CORP.CUSTOMER.USER_ID%TYPE,
        pn_cust_id OUT CORP.CUSTOMER.CUSTOMER_ID%TYPE,
        pv_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pv_first_name REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_last_name REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_email REPORT.USER_LOGIN.EMAIL%TYPE,
        pv_time_zone_guid REPORT.USER_LOGIN.TIME_ZONE_GUID%TYPE,
        pb_password_hash REPORT.USER_LOGIN.PASSWORD_HASH%TYPE,
        pb_password_salt REPORT.USER_LOGIN.PASSWORD_SALT%TYPE,
        pv_cust_name IN CORP.CUSTOMER.CUSTOMER_NAME%TYPE,
        pv_addr1 IN CORP.CUSTOMER_ADDR.ADDRESS1%TYPE,
        pv_city IN CORP.CUSTOMER_ADDR.CITY%TYPE,
        pv_state_cd IN CORP.CUSTOMER_ADDR.STATE%TYPE,
        pv_postal IN CORP.CUSTOMER_ADDR.ZIP%TYPE,
        pv_country_cd IN CORP.CUSTOMER_ADDR.COUNTRY_CD%TYPE,       
        pv_telephone REPORT.USER_LOGIN.TELEPHONE%TYPE,
        pv_fax REPORT.USER_LOGIN.FAX%TYPE,
        pn_dealer_id IN CORP.CUSTOMER.DEALER_ID%TYPE,
        pv_tax_id_nbr IN CORP.CUSTOMER.TAX_ID_NBR%TYPE,
        pv_doing_business_as IN CORP.CUSTOMER.DOING_BUSINESS_AS%TYPE DEFAULT NULL)
    IS
        ln_addr_id CORP.CUSTOMER_ADDR.ADDRESS_ID%TYPE;
        lv_lic_nbr CORP.LICENSE_NBR.LICENSE_NBR%TYPE;
        ln_lic_id CORP.LICENSE_NBR.LICENSE_ID%TYPE;
        l_user_privs NUMBER_TABLE;
    BEGIN
        SELECT LICENSE_ID 
          INTO ln_lic_id 
          FROM CORP.VW_DEALER_LICENSE 
         WHERE DEALER_ID = pn_dealer_id;
        SELECT CORP.CUSTOMER_SEQ.NEXTVAL, CORP.CUSTOMER_ADDR_SEQ.NEXTVAL 
          INTO pn_cust_id, ln_addr_id 
          FROM DUAL;
        SELECT PRIV_ID
          BULK COLLECT INTO l_user_privs
        FROM REPORT.PRIV WHERE CUSTOMER_MASTER_USER_DEFAULT='Y';

        pn_user_id := CREATE_USER(8, pv_user_name, pv_first_name, pv_last_name, pv_email, pv_time_zone_guid, 0, pn_cust_id, pv_telephone, pv_fax, pb_password_hash, pb_password_salt, l_user_privs);
        INSERT INTO CORP.CUSTOMER(CUSTOMER_ID, CUSTOMER_NAME, USER_ID, CREATE_BY, DEALER_ID, TAX_ID_NBR, DOING_BUSINESS_AS)
             VALUES(pn_cust_id, pv_cust_name, pn_user_id, pn_user_id, pn_dealer_id, pv_tax_id_nbr, pv_doing_business_as);
        INSERT INTO CORP.CUSTOMER_ADDR(ADDRESS_ID, CUSTOMER_ID, ADDR_TYPE, NAME, ADDRESS1, CITY, STATE, ZIP, COUNTRY_CD)
             VALUES (ln_addr_id, pn_cust_id, 2, pv_first_name || ' ' || pv_last_name, pv_addr1, pv_city, pv_state_cd, pv_postal, pv_country_cd);
        CORP.CREATE_LICENSE(ln_lic_id, pn_cust_id, lv_lic_nbr);
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
             RAISE_APPLICATION_ERROR(-20100, 'Could not find license agreement for this dealer');
        WHEN OTHERS THEN
             RAISE;
    END;
    
    PROCEDURE CREATE_CUSTOM_REPORT_LINK(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_label WEB_CONTENT.WEB_LINK.WEB_LINK_LABEL%TYPE,
        pv_url WEB_CONTENT.WEB_LINK.WEB_LINK_URL%TYPE,
        pv_desc WEB_CONTENT.WEB_LINK.WEB_LINK_DESC%TYPE,        
        pn_order WEB_CONTENT.WEB_LINK.WEB_LINK_ORDER%TYPE,
        pv_group WEB_CONTENT.WEB_LINK.WEB_LINK_GROUP%TYPE DEFAULT 'User Defined')
    IS
        ln_link_id WEB_CONTENT.WEB_LINK.WEB_LINK_ID%TYPE;
    BEGIN
        SELECT WEB_CONTENT.SEQ_WEB_LINK_ID.NEXTVAL
          INTO ln_link_id
          FROM DUAL;
        INSERT INTO WEB_CONTENT.WEB_LINK(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_ORDER, WEB_LINK_GROUP, WEB_LINK_USAGE)
            VALUES(ln_link_id, pv_label, pv_url, pv_desc, pn_order, pv_group, '-');
        INSERT INTO REPORT.USER_LINK(USER_ID, LINK_ID, INCLUDE, USAGE)
            VALUES(pn_user_id, ln_link_id, 'Y', 'U');
    END;
        
    PROCEDURE DELETE_USER_LINK(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_link_id WEB_CONTENT.WEB_LINK.WEB_LINK_ID%TYPE)
    IS
    BEGIN
        DELETE FROM REPORT.USER_LINK
         WHERE USER_ID = pn_user_id 
           AND LINK_ID = pn_link_id;
        IF SQL%NOTFOUND THEN
            RAISE NO_DATA_FOUND;
        END IF;
        DELETE FROM WEB_CONTENT.WEB_LINK
         WHERE WEB_LINK_ID = pn_link_id
           AND NOT EXISTS(SELECT 1 FROM REPORT.USER_LINK WHERE LINK_ID = pn_link_id);
    END;
    
    FUNCTION GET_USER_PREFERENCE(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_pref_id REPORT.PREFERENCE.PREFERENCE_ID%TYPE)
        RETURN REPORT.USER_PREFERENCE.PREFERENCE_VALUE%TYPE
    IS
        ln_value REPORT.USER_PREFERENCE.PREFERENCE_VALUE%TYPE;
    BEGIN
        SELECT DECODE(UP.PREFERENCE_ID, NULL, P.PREFERENCE_DEFAULT, UP.PREFERENCE_VALUE)
          INTO ln_value
          FROM REPORT.PREFERENCE P
          LEFT OUTER JOIN REPORT.USER_PREFERENCE UP ON P.PREFERENCE_ID = UP.PREFERENCE_ID AND UP.USER_ID = pn_user_id
         WHERE P.PREFERENCE_ID = pn_pref_id;
        RETURN ln_value;
    END;
    
    FUNCTION GET_HEALTH_CODE(
        pn_measured_date DATE,
        pn_min_days NUMBER,
        pn_max_days NUMBER)
        RETURN CHAR
    IS
    BEGIN
        IF pn_measured_date IS NULL THEN
            RETURN 'D'; -- Never accessed
        END IF;
        IF pn_min_days IS NULL OR pn_min_days <= 0 THEN
            RETURN '-'; -- Not measured
        ELSIF pn_measured_date < SYSDATE - pn_max_days THEN
            RETURN 'O'; -- Old
        ELSIF pn_measured_date < SYSDATE - pn_min_days THEN
            RETURN 'Y'; -- Not healthy
        ELSE
            RETURN 'A'; -- Healthy
        END IF;
    END;
    
    FUNCTION GET_HEALTH_CODE_BY_PREF(
        pn_date DATE,
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_pref_id REPORT.PREFERENCE.PREFERENCE_ID%TYPE)
        RETURN CHAR
    IS
        ln_pref_days NUMBER;
    BEGIN
        ln_pref_days := TO_NUMBER_OR_NULL(GET_USER_PREFERENCE(pn_user_id, pn_pref_id));
        RETURN GET_HEALTH_CODE(pn_date, ln_pref_days, 1 + 2 * ln_pref_days);
    END;
    
    PROCEDURE UPSERT_USER_PREFERENCE(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_preference_id REPORT.PREFERENCE.PREFERENCE_ID%TYPE,
        pv_preference_value REPORT.USER_PREFERENCE.PREFERENCE_VALUE%TYPE)
    IS
    BEGIN
        UPDATE REPORT.USER_PREFERENCE 
           SET PREFERENCE_VALUE = pv_preference_value 
         WHERE USER_ID = pn_user_id 
           AND PREFERENCE_ID = pn_preference_id;
        IF SQL%ROWCOUNT < 1 THEN
             BEGIN
                INSERT INTO REPORT.USER_PREFERENCE(
                    USER_ID,
                    PREFERENCE_ID,
                    PREFERENCE_VALUE
                ) VALUES(
                    pn_user_id,
                    pn_preference_id,
                    pv_preference_value
                ); 
            EXCEPTION
                WHEN DUP_VAL_ON_INDEX THEN
                    UPSERT_USER_PREFERENCE(pn_user_id, pn_preference_id, pv_preference_value);
            END;
        END IF;
    END;
END;
/

