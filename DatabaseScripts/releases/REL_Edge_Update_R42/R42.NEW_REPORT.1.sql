GRANT SELECT ON PSS.AUTH to USALIVE_APP_ROLE;
GRANT SELECT ON PSS.TRAN to USALIVE_APP_ROLE;
GRANT SELECT ON PSS.CONSUMER_ACCT_SUB_TYPE to USALIVE_APP_ROLE;
GRANT SELECT ON PSS.CONSUMER_CREDENTIAL to USALIVE_APP_ROLE;
GRANT SELECT ON PSS.CONSUMER_ACCT_REPLENISH to USALIVE_APP_ROLE;
GRANT SELECT ON PSS.REPLENISH_TYPE to USALIVE_APP_ROLE;


INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
VALUES( REPORT.REPORTS_SEQ.NEXTVAL, 'MORE Accounts', 6, 0, 'MORE Accounts', 'Report that shows detailed more card accounts information', 'N', 0);
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( REPORT.REPORTS_SEQ.CURRVAL, 'query', 'SELECT CUS.CUSTOMER_NAME "Operator Name", R.REGION_NAME "Region Name", C.CONSUMER_FNAME "First Name", C.CONSUMER_LNAME "Last Name", CA.CONSUMER_ACCT_IDENTIFIER "Card Id", 
CA.CONSUMER_ACCT_BALANCE "Account Balance", CA.CONSUMER_ACCT_CD "Card Number", C.CONSUMER_EMAIL_ADDR1 "Email Address", C.CONSUMER_CELL_PHONE_NUM "Mobile Phone Number",
CA.CONSUMER_ACCT_ACTIVE_YN_FLAG "Account Enabled", DBADMIN.UTC_TO_LOCAL_DATE(CC.CREATED_UTC_TS) "Registration Time", CC.CREDENTIAL_ACTIVE_FLAG "Account Activated", 
CC.ACTIVATED_TS "Activation Time", CAS.CONSUMER_ACCT_SUB_TYPE_DESC "Account Sub Type", CA.CONSUMER_ACCT_PROMO_TOTAL "Promo Total",
CA.CONSUMER_ACCT_REPLENISH_TOTAL "Replenish Total", CA.REPLENISH_BONUS_TOTAL "Replenish Bonus Total", 
CA.LOYALTY_DISCOUNT_TOTAL "Loyalty Discount Total", CA.CASH_BACK_TOTAL "Cashback Total", 
RT.REPLENISH_TYPE_DESC "Replenish Type", CAR.REPLENISH_AMOUNT "Replenish Amount", CAR.REPLENISH_THRESHHOLD "Replenish Threshold", 
CAR.REPLENISH_CARD_MASKED "Replenish Card", CAR.LAST_REPLENISH_TRAN_TS "Last Replenish Time"
FROM PSS.CONSUMER C
JOIN PSS.CONSUMER_ACCT CA ON C.CONSUMER_ID = CA.CONSUMER_ID
JOIN PSS.CONSUMER_ACCT_SUB_TYPE CAS ON CA.CONSUMER_ACCT_SUB_TYPE_ID = CAS.CONSUMER_ACCT_SUB_TYPE_ID
JOIN PSS.CONSUMER_CREDENTIAL CC ON C.CONSUMER_ID = CC.CONSUMER_ID
JOIN CORP.CUSTOMER CUS ON CA.CORP_CUSTOMER_ID = CUS.CUSTOMER_ID
LEFT OUTER JOIN REPORT.REGION R ON C.REGION_ID = R.REGION_ID
LEFT OUTER JOIN PSS.CONSUMER_ACCT_REPLENISH CAR ON CA.CONSUMER_ACCT_REPLENISH_ID = CAR.CONSUMER_ACCT_REPLENISH_ID
LEFT OUTER JOIN PSS.REPLENISH_TYPE RT ON CAR.REPLENISH_TYPE_ID = RT.REPLENISH_TYPE_ID
WHERE CA.CONSUMER_ACCT_TYPE_ID = 3
AND CUS.CUSTOMER_ID=?
ORDER BY CUS.CUSTOMER_NAME, CA.CONSUMER_ACCT_BALANCE, C.CONSUMER_FNAME, C.CONSUMER_LNAME');

insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'params.CustomerId','user.customerId');
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'paramNames','CustomerId');
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'paramTypes','NUMBER');
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'isSQLFolio','true');

insert into web_content.web_link values(web_content.seq_web_link_id.nextval, 'MORE Accounts', './run_sql_folio_report_async.i?basicReportId='||REPORT.REPORTS_SEQ.CURRVAL||'&' ||'reportTitle=MORE+Accounts','MORE Accounts','Daily Operations','W', 0);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 1);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 31);


INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
VALUES( REPORT.REPORTS_SEQ.NEXTVAL, 'Declined MORE Authorizations', 6, 0, 'Declined MORE Authorizations', 'Report that shows detailed more card declined authorization information', 'N', 0);
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( REPORT.REPORTS_SEQ.CURRVAL, 'query', 'SELECT CUS.CUSTOMER_NAME "Operator Name", R.REGION_NAME "Region Name", T.TRAN_START_TS "Transaction Time", A.AUTH_RESP_CD "Auth Response Code", 
A.AUTH_RESP_DESC "Auth Response Message", C.CONSUMER_FNAME "First Name", C.CONSUMER_LNAME "Last Name", CA.CONSUMER_ACCT_IDENTIFIER "Card Id",
CA.CONSUMER_ACCT_BALANCE "Account Balance", CA.CONSUMER_ACCT_CD "Card Number", C.CONSUMER_EMAIL_ADDR1 "Email Address", C.CONSUMER_CELL_PHONE_NUM "Mobile Phone Number",
CA.CONSUMER_ACCT_ACTIVE_YN_FLAG "Account Enabled", DBADMIN.UTC_TO_LOCAL_DATE(CC.CREATED_UTC_TS) "Registration Time", CC.CREDENTIAL_ACTIVE_FLAG "Account Activated", 
CC.ACTIVATED_TS "Activation Time", CAS.CONSUMER_ACCT_SUB_TYPE_DESC "Account Sub Type"
FROM PSS.TRAN T
JOIN PSS.AUTH A ON T.TRAN_ID = A.TRAN_ID AND A.AUTH_TYPE_CD = ''N''
JOIN PSS.CONSUMER_ACCT CA ON T.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
JOIN PSS.CONSUMER C ON CA.CONSUMER_ID = C.CONSUMER_ID
JOIN PSS.CONSUMER_ACCT_SUB_TYPE CAS ON CA.CONSUMER_ACCT_SUB_TYPE_ID = CAS.CONSUMER_ACCT_SUB_TYPE_ID
JOIN PSS.CONSUMER_CREDENTIAL CC ON C.CONSUMER_ID = CC.CONSUMER_ID
JOIN CORP.CUSTOMER CUS ON CA.CORP_CUSTOMER_ID = CUS.CUSTOMER_ID
LEFT OUTER JOIN REPORT.REGION R ON C.REGION_ID = R.REGION_ID
WHERE T.TRAN_START_TS >= ? AND T.TRAN_START_TS < ? AND T.TRAN_STATE_CD IN (''5'', ''7'') AND CA.CONSUMER_ACCT_TYPE_ID = 3
AND CUS.CUSTOMER_ID = ?
ORDER BY CUS.CUSTOMER_NAME, R.REGION_NAME, T.TRAN_START_TS DESC, C.CONSUMER_FNAME, C.CONSUMER_LNAME');

insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'paramNames','StartDate,EndDate,CustomerId');
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'paramTypes','TIMESTAMP,TIMESTAMP,NUMBER');
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'params.StartDate','{b}');
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'params.EndDate','{e}');
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'params.CustomerId','user.customerId');
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'isSQLFolio','true');

insert into web_content.web_link values(web_content.seq_web_link_id.nextval, 'Declined MORE Authorizations', './select_date_range_frame_sqlfolio.i?basicReportId='||REPORT.REPORTS_SEQ.CURRVAL||'&' ||'reportTitle=Declined+MORE+Authorizations','MORE Accounts','Daily Operations','W', 0);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 1);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 31);
commit;

