SET DEFINE OFF;
DECLARE 
	l_user_id NUMBER;
BEGIN
  
select user_id into l_user_id from report.user_login ul cross join V$DATABASE d 
where user_name=(CASE WHEN d.name like 'USADEV%' THEN 'yhe@usatech.com'
WHEN d.name like 'ECC%' THEN 'aroyce@usatech.com'
ELSE 'compassgroup' END);
  
INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
VALUES( REPORT.REPORTS_SEQ.NEXTVAL, 'Credit xml', 11, 10, 'Credit xml', 'Xml that contains credit transaction data', 'N', l_user_id);

INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
VALUES( REPORT.REPORTS_SEQ.NEXTVAL, 'EFT xml', 12, 3, 'EFT xml', 'Xml that contains payment and transaction data', 'N', l_user_id);

COMMIT;

END;
/

INSERT INTO REPORT.TRANSPORT_REPORT_MAP(CCS_TRANSPORT_TYPE_ID, REPORT_ID)
SELECT 9, REPORT_ID FROM REPORT.REPORTS WHERE REPORT_NAME='Credit xml';

INSERT INTO REPORT.TRANSPORT_REPORT_MAP(CCS_TRANSPORT_TYPE_ID, REPORT_ID)
SELECT 9, REPORT_ID FROM REPORT.REPORTS WHERE REPORT_NAME='EFT xml';

COMMIT;


