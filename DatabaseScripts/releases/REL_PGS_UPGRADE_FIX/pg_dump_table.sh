#!/bin/bash
tableName=$1
startTime=$(date +"%s")
echo "[`date`] startTime $startTime dump $tableName"
`pg_dump -d main -p 5432 -v -Fc -t main.$tableName -f /backup/pg_dump/$tableName.dump`
endTime=$(date +"%s")
echo "[`date`] endTime $endTimei dump $tableName"
diff=$(($endTime-$startTime))
echo "$(($diff / 60)) minutes and $(($diff % 60)) seconds for the dump $tableName"

