#!/bin/bash
tableName=$1
startTime=$(date +"%s")
echo "[`date`] startTime $startTime restore $tableName"
`pg_restore -d main -Fc -v /backup/pg_dump/$tableName.dump`
endTime=$(date +"%s")
echo "[`date`] endTime $endTimei restore $tableName"
diff=$(($endTime-$startTime))
echo "$(($diff / 60)) minutes and $(($diff % 60)) seconds for the restore $tableName"

