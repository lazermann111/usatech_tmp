#!/bin/bash
startTime=$(date +"%s")
echo "[`date`] startTime $startTime"
allTable=`psql -At -d main -p 15432 -c "select tablename from pg_tables where schemaname='main' and tablename not like '%app_request%' and tablename not like '%06_23%' and tablename not like '%06_24%' order by tablename"`
for item in ${allTable[*]}
do 
        startTime=$(date +"%s")
        echo "[`date`] startTime $startTime dump $item"
        `pg_dump -d main -p 15432 -v -Fc -t main.$item -f /backup/pg_dump/$item.dump`
        endTime=$(date +"%s")
        echo "[`date`] endTime $endTimei dump $item"
        diff=$(($endTime-$startTime))
        echo "$(($diff / 60)) minutes and $(($diff % 60)) seconds for $item"
done
endTime=$(date +"%s")
echo "[`date`] endTime $endTime"
diff=$(($endTime-$startTime))
echo "$(($diff / 60)) minutes and $(($diff % 60)) seconds for the dump"


