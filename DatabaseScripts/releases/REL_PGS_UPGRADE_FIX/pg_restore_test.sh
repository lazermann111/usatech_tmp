#!/bin/bash
startTotalTime=$(date +"%s")
startTime=$(date +"%s")
echo "[`date`] startTime $startTime"
allTable=`ls -l /backup/pg_dump/_device_*.dump |awk '{print $9}'`
for item in ${allTable[*]}
do 
	startTime=$(date +"%s")
	echo "[`date`] startTime $startTime restore $item"
	echo "table is |$item|"
	`pg_restore -d main -Fc -v $item`
	endTime=$(date +"%s")
	echo "[`date`] endTime $endTimei restore $item"
	diff=$(($endTime-$startTime))
	echo "$(($diff / 60)) minutes and $(($diff % 60)) seconds for $item"
done
echo "[`date`] done for device_message and device_session"
echo "[`date`] pg_restore done for device_message and device_session"| mail -s "pg_restore done for device_message and device_session" "yhe@usatech.com"
allTable=`ls -lt /backup/pg_dump/_app_*.dump |awk '{print $9}'`
for item in ${allTable[*]}
do 
	startTime=$(date +"%s")
	echo "[`date`] startTime $startTime restore $item"
	echo "table is |$item|"
	`pg_restore -d main -Fc -v $item`
	endTime=$(date +"%s")
	echo "[`date`] endTime $endTimei restore $item"
	diff=$(($endTime-$startTime))
	echo "$(($diff / 60)) minutes and $(($diff % 60)) seconds for $item"
done
echo "[`date`] pg_restore done for app_request"| mail -s "pg_restore done for app_request" "yhe@usatech.com"
endTime=$(date +"%s")
echo "[`date`] endTime $endTime"
diff=$(($endTime-$startTotalTime))
echo "$(($diff / 60)) minutes and $(($diff % 60)) seconds for the restore"

