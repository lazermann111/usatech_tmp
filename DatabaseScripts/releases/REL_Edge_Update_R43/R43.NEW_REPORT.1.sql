GRANT SELECT ON PSS.AUTH to USALIVE_APP_ROLE;
GRANT SELECT ON PSS.TRAN to USALIVE_APP_ROLE;
GRANT SELECT ON PSS.CONSUMER_ACCT_SUB_TYPE to USALIVE_APP_ROLE;
GRANT SELECT ON PSS.CONSUMER_CREDENTIAL to USALIVE_APP_ROLE;
GRANT SELECT ON PSS.CONSUMER_ACCT_REPLENISH to USALIVE_APP_ROLE;
GRANT SELECT ON PSS.REPLENISH_TYPE to USALIVE_APP_ROLE;


ALTER SESSION SET CURRENT_SCHEMA = REPORT;    
DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := -1;
BEGIN
	SELECT MAX(REPORT_ID) INTO LN_REPORT_ID
	FROM REPORT.REPORTS WHERE REPORT_NAME = 'MORE Accounts';
	
	IF LN_REPORT_ID > 0 THEN
		RETURN;
	END IF;

	SELECT REPORT.REPORTS_SEQ.NEXTVAL INTO LN_REPORT_ID FROM DUAL;
	
INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
VALUES( LN_REPORT_ID, 'MORE Accounts', 6, 0, 'MORE Accounts', 'Report that shows detailed more card accounts information', 'N', 0);
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( LN_REPORT_ID, 'query', 'SELECT CUS.CUSTOMER_NAME "Operator Name", R.REGION_NAME "Region Name", C.CONSUMER_FNAME "First Name", C.CONSUMER_LNAME "Last Name", CA.CONSUMER_ACCT_IDENTIFIER "Card Id", 
CA.CONSUMER_ACCT_BALANCE "Account Balance", CA.CONSUMER_ACCT_CD "Card Number", C.CONSUMER_EMAIL_ADDR1 "Email Address", C.CONSUMER_CELL_PHONE_NUM "Mobile Phone Number",
CA.CONSUMER_ACCT_ACTIVE_YN_FLAG "Account Enabled", DBADMIN.UTC_TO_LOCAL_DATE(CC.CREATED_UTC_TS) "Registration Time", CC.CREDENTIAL_ACTIVE_FLAG "Account Activated", 
CC.ACTIVATED_TS "Activation Time", CAS.CONSUMER_ACCT_SUB_TYPE_DESC "Account Sub Type", CA.CONSUMER_ACCT_PROMO_TOTAL "Promo Total",
CA.CONSUMER_ACCT_REPLENISH_TOTAL "Replenish Total", CA.REPLENISH_BONUS_TOTAL "Replenish Bonus Total", 
CA.LOYALTY_DISCOUNT_TOTAL "Loyalty Discount Total", CA.CASH_BACK_TOTAL "Cashback Total", 
RT.REPLENISH_TYPE_DESC "Replenish Type", CAR.REPLENISH_AMOUNT "Replenish Amount", CAR.REPLENISH_THRESHHOLD "Replenish Threshold", 
CAR.REPLENISH_CARD_MASKED "Replenish Card", CAR.LAST_REPLENISH_TRAN_TS "Last Replenish Time",
CA.CONSUMER_ACCT_REPLENISH_TOTAL+CA.CONSUMER_ACCT_PROMO_TOTAL-CA.CONSUMER_ACCT_BALANCE "Total Tran Amount",
CA.TRAN_COUNT_TOTAL "Total Tran Count"
FROM PSS.CONSUMER C
JOIN PSS.CONSUMER_ACCT CA ON C.CONSUMER_ID = CA.CONSUMER_ID
JOIN PSS.CONSUMER_ACCT_SUB_TYPE CAS ON CA.CONSUMER_ACCT_SUB_TYPE_ID = CAS.CONSUMER_ACCT_SUB_TYPE_ID
JOIN PSS.CONSUMER_CREDENTIAL CC ON C.CONSUMER_ID = CC.CONSUMER_ID
JOIN CORP.CUSTOMER CUS ON CA.CORP_CUSTOMER_ID = CUS.CUSTOMER_ID
LEFT OUTER JOIN REPORT.REGION R ON C.REGION_ID = R.REGION_ID
LEFT OUTER JOIN PSS.CONSUMER_ACCT_REPLENISH CAR ON CA.CONSUMER_ACCT_REPLENISH_ID = CAR.CONSUMER_ACCT_REPLENISH_ID
LEFT OUTER JOIN PSS.REPLENISH_TYPE RT ON CAR.REPLENISH_TYPE_ID = RT.REPLENISH_TYPE_ID
WHERE CA.CONSUMER_ACCT_TYPE_ID = 3 AND (? = 0 or CUS.CUSTOMER_ID = ?)
ORDER BY CUS.CUSTOMER_NAME, CA.CONSUMER_ACCT_BALANCE, C.CONSUMER_FNAME, C.CONSUMER_LNAME');

insert into report.report_param values(LN_REPORT_ID, 'params.CustomerId','user.customerId');
insert into report.report_param values(LN_REPORT_ID, 'paramNames','CustomerId,CustomerId');
insert into report.report_param values(LN_REPORT_ID, 'paramTypes','NUMBER,NUMBER');
insert into report.report_param values(LN_REPORT_ID, 'isSQLFolio','true');

insert into web_content.web_link values(web_content.seq_web_link_id.nextval, 'MORE Accounts', './run_sql_folio_report_async.i?basicReportId='||LN_REPORT_ID||'&' ||'reportTitle=MORE+Accounts','MORE Accounts','Daily Operations','W', 0);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 1);
commit;

END;
/

DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := -1;
BEGIN
	SELECT MAX(REPORT_ID) INTO LN_REPORT_ID
	FROM REPORT.REPORTS WHERE REPORT_NAME = 'Declined MORE Authorizations';
	
	IF LN_REPORT_ID > 0 THEN
		RETURN;
	END IF;

	SELECT REPORT.REPORTS_SEQ.NEXTVAL INTO LN_REPORT_ID FROM DUAL;
	
INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
VALUES( LN_REPORT_ID, 'Declined MORE Authorizations - {b} to {e}', 6, 0, 'Declined MORE Authorizations', 'Report that shows detailed more card declined authorization information', 'N', 0);
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( LN_REPORT_ID, 'query', 'SELECT CUS.CUSTOMER_NAME "Operator Name", R.REGION_NAME "Region Name", T.TRAN_START_TS "Transaction Time", A.AUTH_RESP_CD "Auth Response Code", 
A.AUTH_RESP_DESC "Auth Response Message", C.CONSUMER_FNAME "First Name", C.CONSUMER_LNAME "Last Name", CA.CONSUMER_ACCT_IDENTIFIER "Card Id",
CA.CONSUMER_ACCT_BALANCE "Account Balance", CA.CONSUMER_ACCT_CD "Card Number", C.CONSUMER_EMAIL_ADDR1 "Email Address", C.CONSUMER_CELL_PHONE_NUM "Mobile Phone Number",
CA.CONSUMER_ACCT_ACTIVE_YN_FLAG "Account Enabled", DBADMIN.UTC_TO_LOCAL_DATE(CC.CREATED_UTC_TS) "Registration Time", CC.CREDENTIAL_ACTIVE_FLAG "Account Activated", 
CC.ACTIVATED_TS "Activation Time", CAS.CONSUMER_ACCT_SUB_TYPE_DESC "Account Sub Type"
FROM PSS.TRAN T
JOIN PSS.AUTH A ON T.TRAN_ID = A.TRAN_ID AND A.AUTH_TYPE_CD = ''N''
JOIN PSS.CONSUMER_ACCT CA ON T.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
JOIN PSS.CONSUMER C ON CA.CONSUMER_ID = C.CONSUMER_ID
JOIN PSS.CONSUMER_ACCT_SUB_TYPE CAS ON CA.CONSUMER_ACCT_SUB_TYPE_ID = CAS.CONSUMER_ACCT_SUB_TYPE_ID
JOIN PSS.CONSUMER_CREDENTIAL CC ON C.CONSUMER_ID = CC.CONSUMER_ID
JOIN CORP.CUSTOMER CUS ON CA.CORP_CUSTOMER_ID = CUS.CUSTOMER_ID
LEFT OUTER JOIN REPORT.REGION R ON C.REGION_ID = R.REGION_ID
WHERE T.TRAN_START_TS >= ? AND T.TRAN_START_TS < ? AND T.TRAN_STATE_CD IN (''5'', ''7'') AND CA.CONSUMER_ACCT_TYPE_ID = 3
AND (? = 0 or CUS.CUSTOMER_ID = ?)
ORDER BY CUS.CUSTOMER_NAME, R.REGION_NAME, T.TRAN_START_TS DESC, C.CONSUMER_FNAME, C.CONSUMER_LNAME');

insert into report.report_param values(LN_REPORT_ID, 'paramNames','StartDate,EndDate,CustomerId,CustomerId');
insert into report.report_param values(LN_REPORT_ID, 'paramTypes','TIMESTAMP,TIMESTAMP,NUMBER,NUMBER');
insert into report.report_param values(LN_REPORT_ID, 'params.StartDate','{b}');
insert into report.report_param values(LN_REPORT_ID, 'params.EndDate','{e}');
insert into report.report_param values(LN_REPORT_ID, 'params.CustomerId','user.customerId');
insert into report.report_param values(LN_REPORT_ID, 'isSQLFolio','true');

insert into web_content.web_link values(web_content.seq_web_link_id.nextval, 'Declined MORE Authorizations', './select_date_range_frame_sqlfolio.i?basicReportId='||LN_REPORT_ID||'&' ||'reportTitle=Declined+MORE+Authorizations','MORE Accounts','Daily Operations','W', 0);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 1);
commit;

END;
/

DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE;
BEGIN
	SELECT MAX(REPORT_ID) INTO LN_REPORT_ID
	FROM REPORT.REPORTS WHERE REPORT_NAME = 'Softcard Discount Transactions';
	
	IF LN_REPORT_ID > 0 THEN
		RETURN;
	END IF;

	SELECT REPORT.REPORTS_SEQ.NEXTVAL INTO LN_REPORT_ID FROM DUAL;	

	INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID, LATENCY)
	SELECT LN_REPORT_ID, 'Softcard Discount Transactions - {b} to {e}', 6, 0, 'Softcard Discount Transactions', 'Softcard Discount Transactions report', 'N', USER_ID, 1
	FROM REPORT.USER_LOGIN
	WHERE USER_NAME = 'USATMaster';

	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE)
	VALUES(LN_REPORT_ID, 'query', 'SELECT
	TO_CHAR(X.SETTLE_DATE, ''MM/DD/YYYY HH24:MI:SS'') "Settle Date", X.TRAN_ID "Tran Id", X.TOTAL_AMOUNT "Tran Amount", P.PRICE * P.AMOUNT "Reward Amount", CUR.CURRENCY_CODE "Currency",
	P.VEND_COLUMN "Tran Line Item", TT.TRANS_TYPE_NAME "Tran Type", CT.CARD_NAME "Card Type", X.CARD_NUMBER "Card Number", E.EPORT_SERIAL_NUM "Device Serial #", T.TERMINAL_NBR "Terminal #", C.CUSTOMER_NAME "Customer", L.LOCATION_NAME "Location"
	FROM REPORT.TRANS X
	JOIN REPORT.TRANS_TYPE TT ON X.TRANS_TYPE_ID = TT.TRANS_TYPE_ID
	JOIN REPORT.EPORT E ON X.EPORT_ID = E.EPORT_ID
	JOIN REPORT.PURCHASE P ON X.TRAN_ID = P.TRAN_ID
	LEFT OUTER JOIN REPORT.TERMINAL T ON X.TERMINAL_ID = T.TERMINAL_ID
	LEFT OUTER JOIN CORP.CUSTOMER C ON T.CUSTOMER_ID = C.CUSTOMER_ID
	LEFT OUTER JOIN REPORT.LOCATION L ON T.LOCATION_ID = L.LOCATION_ID
	LEFT OUTER JOIN CORP.CURRENCY CUR ON X.CURRENCY_ID = CUR.CURRENCY_ID
	LEFT OUTER JOIN REPORT.CARDTYPE_AUTHORITY CA ON X.CARDTYPE_AUTHORITY_ID = CA.CARDTYPE_AUTHORITY_ID
	LEFT OUTER JOIN REPORT.CARD_TYPE CT ON CA.CARDTYPE_ID = CT.CARD_TYPE_ID
	WHERE X.SETTLE_DATE >= CAST(? AS DATE) AND X.SETTLE_DATE < CAST(? AS DATE) + 1 AND X.SETTLE_STATE_ID = 3 AND P.TRAN_LINE_ITEM_TYPE_ID = 209
	ORDER BY X.SETTLE_DATE, X.TRAN_ID');

	INSERT INTO report.report_param VALUES(LN_REPORT_ID, 'paramNames', 'StartDate,EndDate');
	INSERT INTO report.report_param VALUES(LN_REPORT_ID, 'paramTypes', 'TIMESTAMP,TIMESTAMP');
	INSERT INTO report.report_param VALUES(LN_REPORT_ID, 'params.StartDate', '{b}');
	INSERT INTO report.report_param VALUES(LN_REPORT_ID, 'params.EndDate', '{e}');
	INSERT INTO report.report_param VALUES(LN_REPORT_ID, 'isSQLFolio', 'true');

	INSERT INTO web_content.web_link(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER) VALUES(web_content.seq_web_link_id.nextval, 'Softcard Discount Transactions', './select_date_range_frame_sqlfolio.i?basicReportId=' || LN_REPORT_ID || '&' || 'reportTitle=Softcard+Discount+Transactions', 'Softcard Discount Transactions', 'Accounting', 'W', 0);
	INSERT INTO web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) SELECT web_link_id, 1 FROM web_content.web_link WHERE web_link_label = 'Softcard Discount Transactions';
	INSERT INTO web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) SELECT web_link_id, 14 FROM web_content.web_link WHERE web_link_label = 'Softcard Discount Transactions';
	
	COMMIT;
	
END;
/

DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE;
BEGIN
	SELECT MAX(REPORT_ID) INTO LN_REPORT_ID
	FROM REPORT.REPORTS WHERE REPORT_NAME = 'Sprout Devices';
	
	IF LN_REPORT_ID > 0 THEN
		RETURN;
	END IF;

	SELECT REPORT.REPORTS_SEQ.NEXTVAL INTO LN_REPORT_ID FROM DUAL;	

	INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID, LATENCY)
	VALUES(LN_REPORT_ID, 'Sprout Devices', 6, 0, 'Sprout Devices', 'Report for devices with enabled Sprout payment type', 'N', 0, 1);

	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE)
	VALUES(LN_REPORT_ID, 'query', 'select c.customer_name "Customer", d.device_serial_cd "Device", pp.pos_pta_device_serial_cd "Sprout POS ID", t.asset_nbr "Asset #", 
	l.location_name "Location"
	from device.device d
	join pss.pos p on d.device_id = p.device_id
	join pss.pos_pta pp on p.pos_id = pp.pos_id and pp.pos_pta_activation_ts <= sysdate and (pp.pos_pta_deactivation_ts is null or pp.pos_pta_deactivation_ts > sysdate)
	join pss.payment_subtype ps on pp.payment_subtype_id = ps.payment_subtype_id and ps.payment_subtype_class = ''Sprout''
	left outer join report.eport e on d.device_serial_cd = e.eport_serial_num
	left outer join report.vw_terminal_eport te on e.eport_id = te.eport_id
	left outer join report.terminal t on te.terminal_id = t.terminal_id
	left outer join corp.customer c on t.customer_id = c.customer_id
	left outer join report.location l on t.location_id = l.location_id
	where d.device_active_yn_flag = ''Y'' and (? = 0 or c.customer_id = ?)
	order by c.customer_name, e.eport_serial_num');

	INSERT INTO report.report_param VALUES(LN_REPORT_ID, 'params.CustomerId', 'user.customerId');
	INSERT INTO report.report_param VALUES(LN_REPORT_ID, 'paramNames', 'CustomerId,CustomerId');
	INSERT INTO report.report_param VALUES(LN_REPORT_ID, 'paramTypes', 'NUMBER,NUMBER');
	INSERT INTO report.report_param VALUES(LN_REPORT_ID, 'isSQLFolio', 'true');

	INSERT INTO web_content.web_link(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER) VALUES(web_content.seq_web_link_id.nextval, 'Sprout Devices', './run_sql_folio_report_async.i?basicReportId=' || LN_REPORT_ID || '&' || 'reportTitle=Sprout+Devices', 'Sprout Devices', 'Daily Operations', 'W', 0);
	INSERT INTO web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) SELECT web_link_id, 1 FROM web_content.web_link WHERE web_link_label = 'Sprout Devices';
	
	COMMIT;
	
END;
/

update WEB_CONTENT.web_link set web_link_label='ePort Vending Equipment Compatibility Chart(Excel)' where web_link_label ='ePort Vending Equipment Compatibility Chart';
commit;