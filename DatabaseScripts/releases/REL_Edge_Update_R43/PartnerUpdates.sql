ALTER TABLE CORP.CUSTOMER
ADD (PARENT_CUSTOMER_ID NUMBER);

INSERT INTO REPORT.PRIV (PRIV_ID, DESCRIPTION, INTERNAL_EXTERNAL_FLAG, CUSTOMER_MASTER_USER_DEFAULT)
VALUES ('29', 'Partner', 'B', 'N');

insert into report.user_privs(user_id, priv_id)
select up.user_id, 29
from report.user_privs up
where up.priv_id = 16
and not exists (
  select 1
  from report.user_privs up2
  where up.user_id = up2.user_id
  and up2.priv_id = 29);

INSERT INTO WEB_CONTENT.WEB_LINK(web_link_id, web_link_label, web_link_url, web_link_desc, web_link_group, web_link_usage, web_link_order) 
VALUES (266, 'New Customer', './new_customer.i', 'Create a New Customer', 'Administration', 'M', 172);

INSERT INTO WEB_CONTENT.REQUIREMENT(requirement_id, requirement_name, requirement_description, requirement_class, requirement_param_class, requirement_param_value) 
VALUES (40, 'REQ_PARTNER', null, 'com.usatech.usalive.link.HasPrivilegeRequirement', 'java.lang.String', 'PRIV_PARTNER');

INSERT INTO WEB_CONTENT.REQUIREMENT(requirement_id, requirement_name, requirement_description, requirement_class, requirement_param_class, requirement_param_value) 
VALUES (41, 'REQ_NOT_CUSTOMER_SERVICE', null, 'com.usatech.usalive.link.LacksPrivilegeRequirement', 'java.lang.String', 'PRIV_CUSTOMER_SERVICE');

INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT(web_link_id, requirement_id) 
VALUES (266, 1);

INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT(web_link_id, requirement_id) 
VALUES (266, 40);

INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT(web_link_id, requirement_id) 
VALUES (266, 41);

GRANT SELECT ON CORP.LICENSE TO USALIVE_APP_ROLE;
GRANT SELECT ON CORP.VW_DEALER_LICENSE TO USALIVE_APP_ROLE;
GRANT SELECT ON CORP.VW_CURRENT_LICENSE TO USALIVE_APP_ROLE;
GRANT SELECT ON CORP.LICENSE_NBR TO USALIVE_APP_ROLE;
GRANT SELECT ON CORP.VW_LICENSE TO USALIVE_APP_ROLE;

GRANT EXECUTE ON CORP.RECEIVED_CUSTOM_LICENSE TO USALIVE_APP_ROLE;
GRANT EXECUTE ON CORP.RECEIVED_LICENSE TO USALIVE_APP_ROLE;

GRANT SELECT ON CORP.LICENSE TO REPORT;
GRANT EXECUTE ON CORP.RECEIVED_CUSTOM_LICENSE TO REPORT;

INSERT INTO WEB_CONTENT.WEB_LINK(web_link_id, web_link_label, web_link_url, web_link_desc, web_link_group, web_link_usage, web_link_order) 
VALUES (267, 'Customers', './customer_admin_search.i', 'Customers', 'Administration', 'M', 170);

INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT(web_link_id, requirement_id) 
VALUES (267, 1);

INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT(web_link_id, requirement_id) 
VALUES (267, 40);

INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT(web_link_id, requirement_id) 
VALUES (267, 41);

ALTER TABLE CORP.LICENSE ADD (
	LICENSE_TYPE CHAR(1 BYTE) DEFAULT 'S' NOT NULL ENABLE,
	PARENT_LICENSE_ID NUMBER
);

ALTER TABLE CORP.PROCESS_FEES ADD (
	ORIGINAL_LICENSE_ID NUMBER
);

ALTER TABLE CORP.SERVICE_FEES ADD (
	ORIGINAL_LICENSE_ID NUMBER
);

GRANT EXECUTE ON REPORT.GET_OR_CREATE_EPORT TO USALIVE_APP_ROLE;
GRANT EXECUTE ON CORP.DEALER_EPORT_UPD TO USALIVE_APP_ROLE;
GRANT EXECUTE ON REPORT.ACCEPT_NEW_TERMINAL TO USALIVE_APP_ROLE;
