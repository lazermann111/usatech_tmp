ALTER TABLE CORP.CUSTOMER
ADD (POS_PTA_TMPL_ID NUMBER(20,0));

GRANT SELECT ON DEVICE.CREDENTIAL TO USALIVE_APP_ROLE;
GRANT SELECT ON ENGINE.APP_SETTING TO USALIVE_APP_ROLE;
GRANT SELECT ON PSS.POS_PTA_TMPL TO USALIVE_APP_ROLE;
GRANT SELECT ON PSS.POS_PTA_TMPL_ENTRY TO USALIVE_APP_ROLE;

insert into engine.app_setting(app_setting_cd, app_setting_value, app_setting_desc)
select 'USALIVE_WEBSERVICE_K3_DEFAULT_POS_PTA_TMPL_ID', ppt.pos_pta_tmpl_id, 'Default payment template for K3 type devices created via the USALive webservice'
from pss.pos_pta_tmpl ppt
where ppt.pos_pta_tmpl_name = 'CREDIT SETUP EDGE: EPORT MOBILE Devices';

COMMIT;

GRANT EXECUTE ON PSS.PKG_POS_PTA TO USALIVE_APP_ROLE;

