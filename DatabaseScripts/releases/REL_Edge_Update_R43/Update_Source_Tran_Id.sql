DECLARE
    ln_min_tran_id PSS.TRAN.TRAN_ID%TYPE;
    ln_max_tran_id PSS.TRAN.TRAN_ID%TYPE;
    l_n PLS_INTEGER := 0;
    CURSOR l_cur(cn_min_id PSS.TRAN.TRAN_ID%TYPE, cn_max_id PSS.TRAN.TRAN_ID%TYPE) IS
        SELECT RX.TRAN_ID TARGET_TRAN_ID, X.TRAN_ID SOURCE_TRAN_ID
          FROM REPORT.TRANS RX
          JOIN PSS.TRAN X ON RX.MACHINE_TRANS_NO = X.TRAN_GLOBAL_TRANS_CD AND RX.SOURCE_SYSTEM_CD = 'PSS'
         WHERE RX.SOURCE_TRAN_ID IS NULL
           AND X.TRAN_ID BETWEEN cn_min_id AND cn_max_id;
    TYPE rec_list IS TABLE OF l_cur%ROWTYPE;
    l_list rec_list; 
BEGIN 
    SELECT 2527060000 --MIN(TRAN_ID)
      INTO ln_min_tran_id
      FROM DUAL; --PSS.TRAN;    
    SELECT MAX(TRAN_ID)
      INTO ln_max_tran_id
      FROM PSS.TRAN;    
    WHILE ln_max_tran_id >= ln_min_tran_id LOOP
        OPEN l_cur(ln_min_tran_id, ln_min_tran_id + 9999);
        FETCH l_cur BULK COLLECT INTO l_list;
        CLOSE l_cur;
        FORALL i IN INDICES OF l_list
          UPDATE REPORT.TRANS
             SET SOURCE_TRAN_ID = l_list(i).SOURCE_TRAN_ID
           WHERE TRAN_ID = l_list(i).TARGET_TRAN_ID;
        COMMIT;
        ln_min_tran_id := ln_min_tran_id + 10000;
        l_n := l_n + 1;
        IF l_n = 10 THEN
          DBMS_OUTPUT.PUT_LINE('UPDATED TRANS ' || (ln_min_tran_id - 100000) || ' TO ' || (ln_min_tran_id - 1));
          l_n := 0;
        END IF;
    END LOOP;
    DBMS_OUTPUT.PUT_LINE('UPDATED TRANS ' || (ln_min_tran_id - 10000 * l_n) || ' TO ' || (ln_min_tran_id - 1));
END;
/

CREATE UNIQUE INDEX REPORT.UIX_TRANS_SOURCE_TRAN_ID ON REPORT.TRANS(SOURCE_TRAN_ID) ONLINE TABLESPACE REPORT_INDX;

