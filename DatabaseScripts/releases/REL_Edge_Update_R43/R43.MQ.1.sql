CREATE OR REPLACE FUNCTION MQ.REMOVE_SHADOW_MESSAGES_MISSING(
    pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
    pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
    pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
    pa_exclude_priorities SMALLINT[],
    pn_last_posted_time MQ.QUEUE_SHADOW_DELETION.LAST_POSTED_TIME%TYPE,
    pa_exclude_message_ids VARCHAR(100)[])
    RETURNS INTEGER
    SECURITY DEFINER
AS $$
DECLARE
    lv_queue_table MQ.QUEUE.QUEUE_TABLE%TYPE;
    ln_count INTEGER;
BEGIN
    lv_queue_table := MQ.GET_QUEUE_TABLE(pv_data_source_name, pv_queue_name, pv_subscription_name);
    BEGIN
        EXECUTE 'DELETE FROM MQ.' || lv_queue_table 
             || ' WHERE AVAILABLE_TIME <= $1'
             || ' AND ($2 IS NULL OR MESSAGE_ID != ALL($2))'
             || ' AND ($3 IS NULL OR PRIORITY != ALL($3))'
             || ' AND POSTED_TIME < $4'
          USING pn_last_posted_time, pa_exclude_message_ids, pa_exclude_priorities, pn_last_posted_time;
        GET DIAGNOSTICS ln_count = ROW_COUNT;
    EXCEPTION
        WHEN undefined_table THEN
            ln_count := 0;
    END; 
    RETURN ln_count;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MQ.REMOVE_SHADOW_MESSAGES_MISSING(
    pv_data_source_name MQ.QUEUE.DATA_SOURCE_NAME%TYPE,
    pv_queue_name MQ.QUEUE.QUEUE_NAME%TYPE,
    pv_subscription_name MQ.QUEUE.SUBSCRIPTION_NAME%TYPE,
    pa_exclude_priorities SMALLINT[],
    pn_last_posted_time MQ.QUEUE_SHADOW_DELETION.LAST_POSTED_TIME%TYPE,
    pa_exclude_message_ids VARCHAR(100)[]) TO use_mq;
