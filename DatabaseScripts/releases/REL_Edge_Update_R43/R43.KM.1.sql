ALTER TABLE KM.TOKEN
 ADD INITIATING_DEVICE_TRAN_CD BIGINT DEFAULT 0 NOT NULL;

CREATE OR REPLACE FUNCTION KM.UPSERT_TOKEN(
    pn_global_account_id KM.ACCOUNT.GLOBAL_ACCOUNT_ID%TYPE,
    pn_key_id KM.TOKEN.KEY_ID%TYPE,
    pb_encrypted_token KM.TOKEN.ENCRYPTED_TOKEN%TYPE,
    pba_encrypted_data KM.TOKEN.ENCRYPTED_DATA%TYPE,
    pd_expiration_utc_ts KM.TOKEN.EXPIRATION_UTC_TS%TYPE,
    pv_initiating_device_name KM.TOKEN.INITIATING_DEVICE_NAME%TYPE,
    pn_initiating_device_tran_cd KM.TOKEN.INITIATING_DEVICE_TRAN_CD%TYPE)
    RETURNS BOOLEAN
    SECURITY DEFINER
AS $$
DECLARE
    lv_old_device_name KM.TOKEN.INITIATING_DEVICE_NAME%TYPE;
    lv_old_device_tran_cd KM.TOKEN.INITIATING_DEVICE_TRAN_CD%TYPE;
BEGIN
    FOR i IN 1..10 LOOP
        BEGIN
            INSERT INTO KM.TOKEN(GLOBAL_ACCOUNT_ID, KEY_ID, ENCRYPTED_TOKEN, ENCRYPTED_DATA, EXPIRATION_UTC_TS, INITIATING_DEVICE_NAME, INITIATING_DEVICE_TRAN_CD)
                VALUES(pn_global_account_id, pn_key_id, pb_encrypted_token, pba_encrypted_data, pd_expiration_utc_ts, pv_initiating_device_name, pn_initiating_device_tran_cd);
            RETURN TRUE;
        EXCEPTION
            WHEN unique_violation THEN
                UPDATE KM.TOKEN
                   SET KEY_ID = pn_key_id,
                       ENCRYPTED_TOKEN = pb_encrypted_token, 
                       ENCRYPTED_DATA = pba_encrypted_data, 
                       EXPIRATION_UTC_TS = pd_expiration_utc_ts,
                       INITIATING_DEVICE_TRAN_CD = pn_initiating_device_tran_cd
                 WHERE GLOBAL_ACCOUNT_ID = pn_global_account_id
                   AND INITIATING_DEVICE_NAME = pv_initiating_device_name
                   AND INITIATING_DEVICE_TRAN_CD <= pn_initiating_device_tran_cd;
                IF FOUND THEN
                    RETURN TRUE;
                ELSE
                    SELECT INITIATING_DEVICE_TRAN_CD
                      INTO lv_old_device_tran_cd
                      FROM KM.TOKEN
                     WHERE GLOBAL_ACCOUNT_ID = pn_global_account_id
                       AND INITIATING_DEVICE_NAME = pv_initiating_device_name
                       AND INITIATING_DEVICE_TRAN_CD > pn_initiating_device_tran_cd;
                    IF FOUND THEN
                        RETURN FALSE;
                    END IF;
                    SELECT INITIATING_DEVICE_NAME
                      INTO lv_old_device_name
                      FROM KM.TOKEN
                     WHERE GLOBAL_ACCOUNT_ID = pn_global_account_id
                       AND ENCRYPTED_TOKEN = pb_encrypted_token
                       AND INITIATING_DEVICE_NAME != pv_initiating_device_name;
                    IF FOUND THEN
                        RAISE unique_violation USING MESSAGE = 'This same token was already created for this account but by a different device';
                    END IF;
                END IF;
        END;    
    END LOOP;
    RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION KM.UPSERT_TOKEN(
    pn_global_account_id KM.TOKEN.GLOBAL_ACCOUNT_ID%TYPE,
    pn_key_id KM.TOKEN.KEY_ID%TYPE,
    pb_encrypted_token KM.TOKEN.ENCRYPTED_TOKEN%TYPE,
    pba_encrypted_data KM.TOKEN.ENCRYPTED_DATA%TYPE,
    pd_expiration_utc_ts KM.TOKEN.EXPIRATION_UTC_TS%TYPE,
    pv_initiating_device_name KM.TOKEN.INITIATING_DEVICE_NAME%TYPE,
    pn_initiating_device_tran_cd KM.TOKEN.INITIATING_DEVICE_TRAN_CD%TYPE)
    TO use_km;
    