set serveroutput on
DECLARE
  		l_trans_id REPORT.TRANS.TRAN_ID%TYPE:=1000009862832;
        l_user_id REPORT.USER_LOGIN.USER_ID%TYPE:=7;
        l_amount REPORT.TRANS.TOTAL_AMOUNT%TYPE:=2.4;
        l_reason_id CORP.REFUND_REASON.REFUND_REASON_ID%TYPE:=9;
        l_comment CORP.REFUND.COMMENT_TEXT%TYPE:='Refund for replenishment';
        l_override CORP.REFUND.MANAGER_OVERRIDE_FLAG%TYPE:='N';
        l_has_more CHAR:='N';
        l_return_val INT;
        l_remaining_amount REPORT.TRANS.total_amount%TYPE;
        l_refund_trans_id REPORT.TRANS.TRAN_ID%TYPE;
        l_machine_trans_no REPORT.TRANS.MACHINE_TRANS_NO%TYPE;
        l_refund_id CORP.REFUND.REFUND_ID%TYPE;
        l_refund_desc REPORT.TRANS.DESCRIPTION%TYPE;
        l_prev_total REPORT.TRANS.TOTAL_AMOUNT%TYPE;
        l_trans_total REPORT.TRANS.TOTAL_AMOUNT%TYPE;
        l_num PLS_INTEGER;
        l_lock VARCHAR2(128);
        l_update CHAR:='N';
        l_refund_amount REPORT.TRANS.TOTAL_AMOUNT%TYPE;
        l_upload_date REPORT.TRANS.SERVER_DATE%TYPE;
        l_max_refund_days INTEGER;
        lc_refundable_ind REPORT.TRANS_TYPE.REFUNDABLE_IND%TYPE;
        lc_payable_ind REPORT.TRANS_TYPE.PAYABLE_IND%TYPE;
        lc_replenish_ind VARCHAR2(1);
        ln_trans_type_id REPORT.TRANS_TYPE.TRANS_TYPE_ID%TYPE;
        lv_trans_type REPORT.TRANS_TYPE.TRANS_TYPE_NAME%TYPE;
        ln_source_tran_id REPORT.TRANS.SOURCE_TRAN_ID%TYPE;
        lv_currency_cd CORP.CURRENCY.CURRENCY_CODE%TYPE;
        lv_orig_machine_trans_no REPORT.TRANS.MACHINE_TRANS_NO%TYPE;       

BEGIN
    dbms_output.put_line('Refund for replenishment report.trans.tran_id='||l_trans_id);
		l_return_val := -1; --'Normal Failure'
        l_lock := GLOBALS_PKG.REQUEST_LOCK('REPORT.TRANS.TRAN_ID', l_trans_id);
        l_lock := GLOBALS_PKG.REQUEST_LOCK('CORP.REFUND.TRAN_ID', l_trans_id);
        BEGIN
            SELECT NVL(SUM(T.TOTAL_AMOUNT), 0), NVL(SUM(DECODE(T.TRANS_TYPE_ID, 21, 0, 1)), 0) + 1
              INTO l_prev_total, l_num
              FROM REPORT.TRANS T
              JOIN REPORT.TRANS_TYPE TT ON T.TRANS_TYPE_ID = TT.TRANS_TYPE_ID
             WHERE T.ORIG_TRAN_ID = l_trans_id
               AND TT.REFUND_IND = 'Y';
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                l_prev_total := 0;
                l_num := 1;
            WHEN OTHERS THEN
                RAISE;
        END;
        SELECT
            REPORT.TRANS_SEQ.NEXTVAL,
            CORP.SEQ_REFUND_ID.NEXTVAL,
            'RF:' || SUBSTR(x.MACHINE_TRANS_NO,
                INSTR(x.MACHINE_TRANS_NO, ':') + 1, 100) || ':R'
                || TO_CHAR(l_num, 'FM9999999990'),
            x.MACHINE_TRANS_NO,
            rr.REFUND_REASON,
            x.TOTAL_AMOUNT,
            X.SERVER_DATE,
            NVL(TO_NUMBER_OR_NULL(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('MAX_REFUND_DAYS')), 90),
            TT.REFUNDABLE_IND,
            TT.PAYABLE_IND,
            CASE WHEN TT.TRANS_TYPE_ID IN(24,25,27,28) THEN 'Y' ELSE 'N' END,
            TT.TRANS_TYPE_NAME,
            X.TRANS_TYPE_ID,
            X.SOURCE_TRAN_ID,
            CUR.CURRENCY_CODE
          INTO
            l_refund_trans_id,
            l_refund_id,
            l_machine_trans_no,
            lv_orig_machine_trans_no,
            l_refund_desc,
            l_trans_total,
            l_upload_date,
            l_max_refund_days,
            lc_refundable_ind,
            lc_payable_ind,
            lc_replenish_ind,
            lv_trans_type,
            ln_trans_type_id,
            ln_source_tran_id,
            lv_currency_cd
          FROM REPORT.TRANS x
         CROSS JOIN CORP.REFUND_REASON rr
          JOIN REPORT.TRANS_TYPE TT ON X.TRANS_TYPE_ID = TT.TRANS_TYPE_ID
          LEFT OUTER JOIN CORP.CURRENCY CUR ON X.CURRENCY_ID = CUR.CURRENCY_ID
         WHERE x.TRAN_ID = l_trans_id
           AND RR.REFUND_REASON_ID = l_reason_id;
        dbms_output.put_line('lc_refundable_ind='||lc_refundable_ind);
        dbms_output.put_line('lc_replenish_ind='||lc_replenish_ind);
        
        IF l_upload_date < SYSDATE - l_max_refund_days THEN
            RAISE_APPLICATION_ERROR(-20801, 'Transaction '||TO_CHAR(l_trans_id)||' was uploaded '||TO_CHAR(l_upload_date, 'MM/DD/YYYY')||' and is no longer eligible for a refund');
        ELSIF lc_replenish_ind = 'Y' THEN
            IF ((ABS(l_prev_total) + ABS(l_amount)) > l_trans_total) THEN
                RAISE_APPLICATION_ERROR(-20804, 'You may not refund a replenish transaction for more than it''s total amount. Refund must be $' ||TO_CHAR(l_trans_total - ABS(l_prev_total), 'FM9,999,990.00') || ' or less.');
            END IF;
            -- check balance on prepaid card
            -- check replenish bonus and adjust acct balance and eft as necessary
            DECLARE
                ln_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE;
                ln_consumer_acct_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_TYPE_ID%TYPE;
                ln_consumer_acct_balance PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE;
                ln_replenish_balance PSS.CONSUMER_ACCT.CONSUMER_ACCT_REPLEN_BALANCE%TYPE;
                ln_promo_balance PSS.CONSUMER_ACCT.CONSUMER_ACCT_PROMO_BALANCE%TYPE;
                ln_consumer_acct_sub_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_SUB_TYPE_ID%TYPE;
                ln_corp_customer_id PSS.CONSUMER_ACCT.CORP_CUSTOMER_ID%TYPE;
                lv_consumer_acct_cd PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD%TYPE;
                lv_consumer_acct_identifier PSS.CONSUMER_ACCT.CONSUMER_ACCT_IDENTIFIER%TYPE;
                ln_auth_hold_amt PSS.AUTH.AUTH_AMT%TYPE;
                ln_replenish_bonus NUMBER;
                lv_user_name VARCHAR2(4000);
                ln_doc_id CORP.DOC.DOC_ID%TYPE;
                ln_ledger_id CORP.LEDGER.LEDGER_ID%TYPE; 
                ln_replenish_change NUMBER;
                ln_promo_change NUMBER;
                ln_extra NUMBER;
            BEGIN
                SELECT P.APPLY_TO_CONSUMER_ACCT_ID
                  INTO ln_consumer_acct_id
                  FROM REPORT.PURCHASE P
                 WHERE P.TRAN_ID = l_trans_id
                   AND P.TRAN_LINE_ITEM_TYPE_ID = 550;
                SELECT CA.CONSUMER_ACCT_TYPE_ID, CA.CONSUMER_ACCT_BALANCE, CA.CONSUMER_ACCT_REPLEN_BALANCE, 
                       CA.CONSUMER_ACCT_PROMO_BALANCE, CA.CONSUMER_ACCT_SUB_TYPE_ID, CA.CORP_CUSTOMER_ID, CA.CONSUMER_ACCT_CD, CA.CONSUMER_ACCT_IDENTIFIER
                  INTO ln_consumer_acct_type_id, ln_consumer_acct_balance, ln_replenish_balance, ln_promo_balance, 
                       ln_consumer_acct_sub_type_id, ln_corp_customer_id, lv_consumer_acct_cd, lv_consumer_acct_identifier
                  FROM PSS.CONSUMER_ACCT CA
                 WHERE CA.CONSUMER_ACCT_ID = ln_consumer_acct_id
                   FOR UPDATE;
                SELECT COALESCE(SUM(A.AUTH_AMT_APPROVED), 0)
                  INTO ln_auth_hold_amt
                  FROM PSS.CONSUMER_ACCT_AUTH_HOLD CAAH
                  JOIN PSS.AUTH A ON CAAH.AUTH_ID = A.AUTH_ID
                 WHERE CAAH.CONSUMER_ACCT_ID = ln_consumer_acct_id
                   AND CAAH.EXPIRATION_TS > SYSDATE
                   AND CAAH.CLEARED_YN_FLAG = 'N';    
                IF ln_source_tran_id IS NULL THEN
                    SELECT TRAN_ID
                      INTO ln_source_tran_id
                      FROM PSS.TRAN
                     WHERE TRAN_GLOBAL_TRANS_CD = lv_orig_machine_trans_no;
                END IF;
                SELECT COALESCE(SUM(xi.TRAN_LINE_ITEM_QUANTITY * xi.TRAN_LINE_ITEM_AMOUNT) * l_amount / l_trans_total, 0)
                  INTO ln_replenish_bonus
                  FROM PSS.TRAN X
                  JOIN PSS.TRAN_LINE_ITEM XI ON X.TRAN_ID = XI.TRAN_ID
                 WHERE X.PARENT_TRAN_ID = ln_source_tran_id
                   AND XI.TRAN_LINE_ITEM_TYPE_ID = 555;
                IF ln_consumer_acct_balance - ln_auth_hold_amt < l_amount + ln_replenish_bonus THEN
                    RAISE_APPLICATION_ERROR(-20803, 'Prepaid account ' || TO_CHAR(ln_consumer_acct_id) || ' only has an available balance of $' 
                        ||TO_CHAR(ln_consumer_acct_balance - ln_auth_hold_amt - ln_replenish_bonus, 'FM9,999,990.00') || ' (including an auth hold of $'
                        ||TO_CHAR(ln_auth_hold_amt, 'FM9,999,990.00') || ' and a replenish bonus of $' || TO_CHAR(ln_replenish_bonus, 'FM9,999,990.00') 
                        || '). The refund amount must be equal to or less than this.');              
                END IF;
                l_update := 'Y';
                l_refund_amount:=l_amount;
                l_remaining_amount:=0;  
                l_return_val:=0;
                dbms_output.put_line('ln_consumer_acct_id='||ln_consumer_acct_id);
                
                IF ln_promo_balance < ln_replenish_bonus THEN
                    ln_promo_change := ln_promo_balance;
                    ln_extra := ln_replenish_bonus - ln_promo_balance;
                ELSE
                    ln_promo_change := ln_replenish_bonus;
                    ln_extra := 0;
                END IF;
                ln_replenish_change := l_amount + ln_extra - ln_consumer_acct_balance + ln_promo_balance + ln_replenish_balance;
                IF ln_replenish_change < 0 THEN
                    ln_replenish_change := 0;
                END IF;
                
                FOR l_rec in (select REPLENISH_BONUS_TOTAL, CONSUMER_ACCT_BALANCE, CONSUMER_ACCT_REPLEN_BALANCE,CONSUMER_ACCT_REPLENISH_TOTAL, CONSUMER_ACCT_PROMO_BALANCE,CONSUMER_ACCT_PROMO_TOTAL from PSS.CONSUMER_ACCT WHERE CONSUMER_ACCT_ID = ln_consumer_acct_id ) LOOP
                  dbms_output.put_line('-------------------------------------');
                	dbms_output.put_line('before REPLENISH_BONUS_TOTAL='||l_rec.REPLENISH_BONUS_TOTAL);
                	dbms_output.put_line('before CONSUMER_ACCT_BALANCE='||l_rec.CONSUMER_ACCT_BALANCE);
                	dbms_output.put_line('before CONSUMER_ACCT_REPLEN_BALANCE='||l_rec.CONSUMER_ACCT_REPLEN_BALANCE);
                	dbms_output.put_line('before CONSUMER_ACCT_REPLENISH_TOTAL='||l_rec.CONSUMER_ACCT_REPLENISH_TOTAL);
                	dbms_output.put_line('before CONSUMER_ACCT_PROMO_BALANCE='||l_rec.CONSUMER_ACCT_PROMO_BALANCE);
                	dbms_output.put_line('before CONSUMER_ACCT_PROMO_TOTAL='||l_rec.CONSUMER_ACCT_PROMO_TOTAL);
                END LOOP;
				
                UPDATE PSS.CONSUMER_ACCT
                   SET REPLENISH_BONUS_TOTAL = NVL(REPLENISH_BONUS_TOTAL, 0) - ln_replenish_bonus,
                       CONSUMER_ACCT_BALANCE = CONSUMER_ACCT_BALANCE - ln_replenish_bonus - l_amount,
                       CONSUMER_ACCT_REPLEN_BALANCE = CONSUMER_ACCT_REPLEN_BALANCE - ln_replenish_change,
                       CONSUMER_ACCT_REPLENISH_TOTAL = CONSUMER_ACCT_REPLENISH_TOTAL - l_amount,
                       CONSUMER_ACCT_PROMO_BALANCE =CONSUMER_ACCT_PROMO_BALANCE - ln_promo_change,
                       CONSUMER_ACCT_PROMO_TOTAL = CONSUMER_ACCT_PROMO_TOTAL - ln_replenish_bonus
                 WHERE CONSUMER_ACCT_ID = ln_consumer_acct_id;
                 
                FOR l_rec in (select REPLENISH_BONUS_TOTAL, CONSUMER_ACCT_BALANCE, CONSUMER_ACCT_REPLEN_BALANCE,CONSUMER_ACCT_REPLENISH_TOTAL, CONSUMER_ACCT_PROMO_BALANCE,CONSUMER_ACCT_PROMO_TOTAL from PSS.CONSUMER_ACCT WHERE CONSUMER_ACCT_ID = ln_consumer_acct_id ) LOOP
                  dbms_output.put_line('-------------------------------------');
                	dbms_output.put_line('after REPLENISH_BONUS_TOTAL='||l_rec.REPLENISH_BONUS_TOTAL);
                	dbms_output.put_line('after CONSUMER_ACCT_BALANCE='||l_rec.CONSUMER_ACCT_BALANCE);
                	dbms_output.put_line('after CONSUMER_ACCT_REPLEN_BALANCE='||l_rec.CONSUMER_ACCT_REPLEN_BALANCE);
                	dbms_output.put_line('after CONSUMER_ACCT_REPLENISH_TOTAL='||l_rec.CONSUMER_ACCT_REPLENISH_TOTAL);
                	dbms_output.put_line('after CONSUMER_ACCT_PROMO_BALANCE='||l_rec.CONSUMER_ACCT_PROMO_BALANCE);
                	dbms_output.put_line('after CONSUMER_ACCT_PROMO_TOTAL='||l_rec.CONSUMER_ACCT_PROMO_TOTAL);
                END LOOP;
                dbms_output.put_line('-------------------------------------');
                dbms_output.put_line('ln_replenish_bonus='||ln_replenish_bonus);
                dbms_output.put_line('ln_consumer_acct_sub_type_id='||ln_consumer_acct_sub_type_id);
                dbms_output.put_line('ln_corp_customer_id='||ln_corp_customer_id);

                IF ln_replenish_bonus > 0 AND ln_consumer_acct_sub_type_id = 1 AND ln_corp_customer_id IS NOT NULL AND ln_corp_customer_id != 0 THEN
                    SELECT NVL(MAX(USER_NAME), 'Refund Replenishment Processing')
                      INTO lv_user_name
                      FROM REPORT.USER_LOGIN
                     WHERE USER_ID = l_user_id;
                    CORP.PAYMENTS_PKG.ADJUSTMENT_INS(ln_corp_customer_id, lv_user_name, lv_currency_cd, 
                        'Refund of Replenish Bonus, card # ' || lv_consumer_acct_cd || ', card ID ' || lv_consumer_acct_identifier,
                        ln_replenish_bonus, ln_doc_id, ln_ledger_id);
                    dbms_output.put_line('CORP.PAYMENTS_PKG.ADJUSTMENT_INS ln_replenish_bonus='||ln_replenish_bonus||' ln_doc_id='||ln_doc_id||' ln_ledger_id='||ln_ledger_id);
                END IF;               
           END;
        ELSIF ((ABS(l_prev_total) + ABS(l_amount)) <= l_trans_total) THEN
            l_update:='Y';
            l_refund_amount:=l_amount;
            l_remaining_amount:=0;  
            l_return_val:=0;
        ELSIF l_has_more = 'Y' THEN
            IF l_trans_total-ABS(l_prev_total) > 0 THEN
                l_update:='Y';
                l_refund_amount:=l_trans_total-ABS(l_prev_total);
                l_remaining_amount:=l_amount-l_refund_amount;
                l_return_val:=2;--'This transaction is refunded the diff'
            ELSE
                l_remaining_amount:=l_amount;
                l_return_val:=3; --'This transaction is already refunded'
            END IF;
        ELSIF l_override = 'Y' THEN 
            l_update:='Y';
            l_refund_amount:=l_amount;
            l_remaining_amount:=0;
            l_return_val := 1; --'Override Success'
        ELSE 
            l_remaining_amount:=l_amount;
            l_return_val := -2; --'Override Failure'
        END IF;
		dbms_output.put_line('l_return_val='||l_return_val||' l_remaining_amount='||l_remaining_amount);
        IF l_update = 'Y' THEN
            DECLARE
                l_date DATE := SYSDATE;
            BEGIN
            INSERT INTO REPORT.TRANS(tran_id, card_number, total_amount, cc_appr_code,
                start_date, close_date, server_date, settle_state_id, eport_id,
                trans_type_id, orig_tran_id, terminal_id, merchant_id,
                source_system_cd, machine_trans_no, customer_bank_id,
                process_fee_id, create_date, description, currency_id, consumer_acct_id)
                (SELECT
                    l_refund_trans_id, orig.card_number, -ABS(l_refund_amount), 'PENDING',
                    l_date, l_date, l_date, 1, orig.eport_id, 20, l_trans_id,
                    orig.terminal_id, orig.merchant_id, 'RA', l_machine_trans_no,
                    orig.customer_bank_id, orig.process_fee_id, l_date,
                    l_refund_desc, orig.currency_id, orig.consumer_acct_id
                FROM REPORT.TRANS orig WHERE orig.tran_id = l_trans_id);

            INSERT INTO CORP.REFUND(refund_id, creator_user_id, tran_id, reason_id,
                comment_text, processed_flag, manager_override_flag, close_date) VALUES
                (l_refund_id, l_user_id, l_refund_trans_id, l_reason_id, l_comment,
                'N', l_override, l_date);
            END;
      END IF;
END;