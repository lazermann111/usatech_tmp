DECLARE
  l_min_consumer_acct_id NUMBER;
  l_max_consumer_acct_id NUMBER;
BEGIN
	select min(consumer_acct_id), max(consumer_acct_id) into l_min_consumer_acct_id, l_max_consumer_acct_id from PSS.CONSUMER_ACCT where CONSUMER_ACCT_TYPE_ID=3;
    while l_min_consumer_acct_id <= l_max_consumer_acct_id loop
		UPDATE PSS.CONSUMER_ACCT CA SET (TRAN_COUNT_TOTAL)=(
			SELECT COUNT(AR.TRAN_ID) FROM report.activity_ref AR 
			WHERE AR.CONSUMER_ACCT_ID= CA.CONSUMER_ACCT_ID)
    	where consumer_acct_id between l_min_consumer_acct_id and l_min_consumer_acct_id+1000-1
    	and CONSUMER_ACCT_TYPE_ID=3;
		commit;
		l_min_consumer_acct_id:=l_min_consumer_acct_id+1000;
	end loop;
END;
/