WHENEVER SQLERROR EXIT FAILURE COMMIT;

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/PSS/PKG_TRAN.psk?rev=1.66
CREATE OR REPLACE PACKAGE PSS.PKG_TRAN IS

PROCEDURE SP_CREATE_REFUND (
    pv_global_trans_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
    pv_orig_global_trans_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
    pn_refund_utc_ts_ms IN NUMBER,
    pn_orig_upload_utc_ts_ms IN NUMBER,
    pn_refund_amt PSS.REFUND.REFUND_AMT%TYPE,
    pn_refund_desc PSS.REFUND.REFUND_DESC%TYPE,
    pn_refund_issue_by PSS.REFUND.REFUND_ISSUE_BY%TYPE,
    pn_refund_type_cd PSS.REFUND.REFUND_TYPE_CD%TYPE,
    pc_entry_method_cd PSS.REFUND.ACCT_ENTRY_METHOD_CD%TYPE,
    pc_already_inserted_flag OUT VARCHAR2,
    pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
    pn_orig_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
    pn_minor_currency_factor OUT PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
    pn_payment_subtype_key_id OUT PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE,
    pv_payment_subtype_class OUT PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE
);

PROCEDURE SP_CLOSE_CONSUMER_ACCT (
	pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
	pv_refund_issue_by PSS.REFUND.REFUND_ISSUE_BY%TYPE,
	pn_refund_amt OUT PSS.REFUND.REFUND_AMT%TYPE,
	pn_eft_credit_amt OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
	pn_doc_id OUT CORP.DOC.DOC_ID%TYPE
);

-- R33+ signature
PROCEDURE sp_create_sale(
    pc_global_event_cd_prefix IN CHAR,
    pv_device_name IN device.device_name%TYPE,
    pv_device_tran_cd IN pss.tran.tran_device_tran_cd%TYPE,
    pn_device_batch_id IN pss.sale.device_batch_id%TYPE,
    pc_sale_type_cd IN pss.sale.sale_type_cd%TYPE,
    pn_sale_utc_ts_ms IN NUMBER,
    pn_sale_utc_offset_min IN pss.sale.sale_utc_offset_min%TYPE,
    pv_tran_device_result_type_cd IN pss.tran.tran_device_result_type_cd%TYPE,
    pn_sale_result_id IN pss.sale.sale_result_id%TYPE,
    pn_sale_amount IN NUMBER,
    pc_receipt_result_cd IN pss.sale.receipt_result_cd%TYPE,
    pc_tran_batch_type_cd IN pss.tran_line_item_batch_type.tran_line_item_batch_type_cd%TYPE,
    pv_hash_type_cd IN pss.sale.hash_type_cd%TYPE,
    pv_tran_line_item_hash IN pss.sale.tran_line_item_hash%TYPE,
    pv_global_session_cd IN VARCHAR2,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_tran_id OUT pss.tran.tran_id%TYPE,
    pv_tran_state_cd OUT pss.tran.tran_state_cd%TYPE,
    pc_void_allowed IN PSS.SALE.VOID_ALLOWED%TYPE DEFAULT 'N'
);

PROCEDURE sp_create_tran_line_item
(
    pn_tran_id IN pss.tran_line_item.tran_id%TYPE,
    pn_host_port_num IN host.host_port_num%TYPE,
    pn_tli_type_id IN pss.tran_line_item.tran_line_item_type_id%TYPE,
    pn_tli_quantity IN pss.tran_line_item.tran_line_item_quantity%TYPE,    
    pn_tli_amount IN NUMBER,
    pn_tli_tax IN NUMBER,
    pv_tli_desc IN pss.tran_line_item.tran_line_item_desc%TYPE,    
    pn_tli_utc_ts_ms IN NUMBER,
    pn_tli_utc_offset_min IN NUMBER,
    pv_tli_position_cd IN pss.tran_line_item.tran_line_item_position_cd%TYPE,
    pc_tran_batch_type_cd IN pss.tran_line_item.tran_line_item_batch_type_cd%TYPE,
    pn_sale_result_id pss.tran_line_item.sale_result_id%TYPE DEFAULT NULL,
    pn_host_position_num host.host_position_num%TYPE DEFAULT 0,
	pn_sale_amount pss.sale.sale_amount%TYPE DEFAULT 0
);

-- R33+ signature
PROCEDURE sp_finalize_sale
(
    pn_tran_id IN pss.tran.tran_id%TYPE,
    pv_global_session_cd IN VARCHAR2,
    pv_tran_device_result_type_cd IN pss.tran.tran_device_result_type_cd%TYPE,
    pn_sale_result_id IN pss.sale.sale_result_id%TYPE,
    pn_sale_amount IN NUMBER,
    pn_sale_tax IN NUMBER,
    pc_tran_batch_type_cd IN pss.tran_line_item.tran_line_item_batch_type_cd%TYPE,
    pn_sale_utc_ts_ms IN NUMBER,
    pn_sale_duration_sec IN NUMBER,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pc_sale_type_cd pss.sale.sale_type_cd%TYPE DEFAULT NULL,
	pc_tran_import_needed OUT VARCHAR2,
	pc_session_update_needed OUT VARCHAR2,
	pc_client_payment_type_cd OUT VARCHAR2,
	pn_minor_currency_factor OUT PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
	pn_tli_count OUT NUMBER
);

PROCEDURE CREATE_REPLENISHMENT(
    pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
    pn_auth_id PSS.AUTH.AUTH_ID%TYPE,
    pn_replenish_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
    pv_tran_device_result_type_cd PSS.TRAN.TRAN_DEVICE_RESULT_TYPE_CD%TYPE,
    pn_device_batch_id PSS.SALE.DEVICE_BATCH_ID%TYPE,
    pc_receipt_result_cd PSS.SALE.RECEIPT_RESULT_CD%TYPE,
    pc_auth_only CHAR,
    pc_tran_state_cd OUT VARCHAR2,
    pc_client_payment_type_cd OUT VARCHAR2,
    pn_replenish_amount OUT PSS.AUTH.AUTH_AMT_APPROVED%TYPE,
    pn_replenish_balance_amount OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE);
    
PROCEDURE UPDATE_AUTHORIZATION(
    pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
    pn_auth_id PSS.AUTH.AUTH_ID%TYPE,
    pv_card_key PSS.AUTH.CARD_KEY%TYPE,
    pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE);

-- R37+ signature
PROCEDURE SP_CREATE_AUTH(
   pv_global_event_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
   pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
   pn_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE,
   pv_device_event_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE,
   pc_invalid_device_event_cd CHAR,
   pn_tran_start_time NUMBER,
   pn_auth_utc_ms NUMBER,
   pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
   pc_entry_method CHAR,
   pv_masked_card_number PSS.TRAN.TRAN_RECEIVED_RAW_ACCT_DATA%TYPE,
   pn_consumer_acct_id PSS.TRAN.CONSUMER_ACCT_ID%TYPE,
   pd_auth_ts PSS.AUTH.AUTH_TS%TYPE,
   pv_authority_resp_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
   pv_authority_resp_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
   pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
   pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
   pt_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
   pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
   pn_trace_number PSS.AUTH.TRACE_NUMBER%TYPE,
   pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
   pn_auth_amt PSS.AUTH.AUTH_AMT%TYPE,
   pn_balance_amt PSS.AUTH.AUTH_BALANCE_AMT%TYPE,
   pn_requested_amt PSS.AUTH.AUTH_AUTHORITY_AMT_RQST%TYPE,
   pn_received_amt PSS.AUTH.AUTH_AUTHORITY_AMT_RCVD%TYPE,
   pn_add_auth_hold_days NUMBER,
   pc_auth_hold_used PSS.AUTH.AUTH_HOLD_USED%TYPE,
   pv_global_session_cd VARCHAR2,
   pc_ignore_dup CHAR,
   pn_auth_action_id PSS.AUTH.AUTH_ACTION_ID%TYPE,
   pn_auth_action_bitmap PSS.AUTH.AUTH_ACTION_BITMAP%TYPE,
   pc_sent_to_device CHAR,
   pc_pass_thru CHAR,
   pv_card_key PSS.AUTH.CARD_KEY%TYPE,
   pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
   pc_tran_state_cd OUT VARCHAR2,
   pc_tran_import_needed OUT VARCHAR2,
   pc_session_update_needed OUT VARCHAR2,
   pv_sale_global_session_cd OUT VARCHAR2,
   pn_sale_session_start_time OUT PSS.SALE.SALE_SESSION_START_TIME%TYPE,
   pc_client_payment_type_cd OUT VARCHAR2,
   pn_sale_amount OUT pss.sale.sale_amount%TYPE,
   pn_tli_count OUT NUMBER,
   pn_auth_id OUT PSS.AUTH.AUTH_ID%TYPE) ;
   
-- R33-R36 signature
PROCEDURE SP_CREATE_AUTH(
   pv_global_event_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
   pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
   pn_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE,
   pv_device_event_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE,
   pc_invalid_device_event_cd CHAR,
   pn_tran_start_time NUMBER,
   pn_auth_utc_ms NUMBER,
   pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
   pc_entry_method CHAR,
   pc_payment_type CHAR,
   pv_masked_card_number PSS.TRAN.TRAN_RECEIVED_RAW_ACCT_DATA%TYPE,
   pv_pan_sha1 VARCHAR2,
   pr_consumer_acct_cd_hash PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD_HASH%TYPE,
   pn_consumer_acct_id PSS.TRAN.CONSUMER_ACCT_ID%TYPE,
   pd_auth_ts PSS.AUTH.AUTH_TS%TYPE,
   pv_authority_resp_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
   pv_authority_resp_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
   pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
   pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
   pt_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
   pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
   pn_trace_number PSS.AUTH.TRACE_NUMBER%TYPE,
   pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
   pn_auth_amt PSS.AUTH.AUTH_AMT%TYPE,
   pn_balance_amt PSS.AUTH.AUTH_BALANCE_AMT%TYPE,
   pn_requested_amt PSS.AUTH.AUTH_AUTHORITY_AMT_RQST%TYPE,
   pn_received_amt PSS.AUTH.AUTH_AUTHORITY_AMT_RCVD%TYPE,
   pn_add_auth_hold_days NUMBER,
   pc_auth_hold_used PSS.AUTH.AUTH_HOLD_USED%TYPE,
   pv_global_session_cd VARCHAR2,
   pc_ignore_dup CHAR,
   pn_auth_action_id PSS.AUTH.AUTH_ACTION_ID%TYPE,
   pn_auth_action_bitmap PSS.AUTH.AUTH_ACTION_BITMAP%TYPE,
   pc_sent_to_device CHAR,
   pc_pass_thru CHAR,
   pv_card_key PSS.AUTH.CARD_KEY%TYPE,
   pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
   pc_tran_state_cd OUT VARCHAR2,
   pc_tran_import_needed OUT VARCHAR2,
   pc_session_update_needed OUT VARCHAR2,
   pv_sale_global_session_cd OUT VARCHAR2,
   pn_sale_session_start_time OUT PSS.SALE.SALE_SESSION_START_TIME%TYPE,
   pc_client_payment_type_cd OUT VARCHAR2,
   pn_sale_amount OUT pss.sale.sale_amount%TYPE,
   pn_tli_count OUT NUMBER
);

-- R37 and above
PROCEDURE SP_INSERT_AUTH_STATS(
   pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
   pn_auth_id PSS.AUTH.AUTH_ID%TYPE,
   pn_request_time NUMBER,
   pn_applayer_start_time NUMBER,
   pn_authority_start_time NUMBER,
   pn_authority_end_time NUMBER,
   pn_applayer_end_time NUMBER,
   pn_response_time NUMBER
);

-- R36 and below
PROCEDURE SP_INSERT_AUTH_STATS(
   pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
   pn_request_time NUMBER,
   pn_applayer_start_time NUMBER,
   pn_authority_start_time NUMBER,
   pn_authority_end_time NUMBER,
   pn_applayer_end_time NUMBER,
   pn_response_time NUMBER
);

-- R36- Signature
PROCEDURE SP_PERMIT_CONSUMER_ACCT(
    pn_pos_pta_id PSS.POS_PTA.POS_PTA_ID%TYPE,
    pv_consumer_acct_cd PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD%TYPE,
    pt_auth_ts PSS.CONSUMER_ACCT.CONSUMER_ACCT_ACTIVATION_TS%TYPE,
    pv_currency_cd PSS.CURRENCY.CURRENCY_CD%TYPE,	
    pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
    pn_action_id OUT PSS.PERMISSION_ACTION.ACTION_ID%TYPE,
    pn_action_code OUT NUMBER,
    pn_action_bitmap OUT NUMBER,
	pr_consumer_acct_cd_hash IN PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD_HASH%TYPE DEFAULT NULL
);

-- R37+ Signature
PROCEDURE PERMIT_CONSUMER_ACCT(
	pn_pos_pta_id PSS.POS_PTA.POS_PTA_ID%TYPE,
	pn_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
	pt_auth_ts PSS.CONSUMER_ACCT.CONSUMER_ACCT_ACTIVATION_TS%TYPE,
	pl_consumer_acct_type_ids NUMBER_TABLE,
	pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
	pn_consumer_id OUT PSS.CONSUMER_ACCT.CONSUMER_ID%TYPE,
    pn_consumer_acct_type_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_TYPE_ID%TYPE,
    pv_consumer_acct_type_label OUT PSS.CONSUMER_ACCT_TYPE.CONSUMER_ACCT_TYPE_LABEL%TYPE,
    pb_consumer_acct_raw_hash OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_RAW_HASH%TYPE, 
    pb_consumer_acct_raw_salt OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_RAW_SALT%TYPE, 
    pb_security_cd_hash OUT PSS.CONSUMER_ACCT.SECURITY_CD_HASH%TYPE, 
    pb_security_cd_salt OUT PSS.CONSUMER_ACCT.SECURITY_CD_SALT%TYPE,
	pn_action_id OUT PSS.PERMISSION_ACTION.ACTION_ID%TYPE,
	pn_action_code OUT NUMBER,
	pn_action_bitmap OUT NUMBER);

    -- R37+ signature
    PROCEDURE SP_CREATE_LOCAL_AUTH_SALE(
       pc_global_event_cd_prefix IN CHAR,
       pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
       pn_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE,
       pv_device_tran_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE,
       pn_sale_utc_ts_ms NUMBER,
       pn_sale_utc_offset_min IN pss.sale.sale_utc_offset_min%TYPE,
       pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
       pc_entry_method CHAR,
       pc_payment_type CHAR,
       pv_track_data PSS.TRAN.TRAN_RECEIVED_RAW_ACCT_DATA%TYPE,
       pn_consumer_acct_id PSS.TRAN.CONSUMER_ACCT_ID%TYPE,
       pd_auth_ts PSS.AUTH.AUTH_TS%TYPE,
       pn_trace_number PSS.AUTH.TRACE_NUMBER%TYPE,
       pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
       pn_auth_amt PSS.AUTH.AUTH_AMT%TYPE,
       pn_device_batch_id IN pss.sale.device_batch_id%TYPE,
       pc_sale_type_cd IN pss.sale.sale_type_cd%TYPE,
       pv_tran_device_result_type_cd IN pss.tran.tran_device_result_type_cd%TYPE,
       pn_sale_result_id IN pss.sale.sale_result_id%TYPE,
       pv_hash_type_cd IN pss.sale.hash_type_cd%TYPE,
       pv_tran_line_item_hash IN pss.sale.tran_line_item_hash%TYPE,
       pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
       pn_result_cd OUT NUMBER,
       pv_error_message OUT VARCHAR2
    );

    -- R33-R36 signature
    PROCEDURE SP_CREATE_LOCAL_AUTH_SALE(
       pc_global_event_cd_prefix IN CHAR,
       pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
       pn_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE,
       pv_device_tran_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE,
       pn_sale_utc_ts_ms NUMBER,
       pn_sale_utc_offset_min IN pss.sale.sale_utc_offset_min%TYPE,
       pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
       pc_entry_method CHAR,
       pc_payment_type CHAR,
       pv_track_data PSS.TRAN.TRAN_RECEIVED_RAW_ACCT_DATA%TYPE,
       pv_pan_sha1 VARCHAR2,
       pn_consumer_acct_id PSS.TRAN.CONSUMER_ACCT_ID%TYPE,
       pd_auth_ts PSS.AUTH.AUTH_TS%TYPE,
       pn_trace_number PSS.AUTH.TRACE_NUMBER%TYPE,
       pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
       pn_auth_amt PSS.AUTH.AUTH_AMT%TYPE,
       pn_device_batch_id IN pss.sale.device_batch_id%TYPE,
       pc_sale_type_cd IN pss.sale.sale_type_cd%TYPE,
       pv_tran_device_result_type_cd IN pss.tran.tran_device_result_type_cd%TYPE,
       pn_sale_result_id IN pss.sale.sale_result_id%TYPE,
       pv_hash_type_cd IN pss.sale.hash_type_cd%TYPE,
       pv_tran_line_item_hash IN pss.sale.tran_line_item_hash%TYPE,
       pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
       pn_result_cd OUT NUMBER,
       pv_error_message OUT VARCHAR2
    );
    
    --R37 and above
    PROCEDURE DEBIT_CONSUMER_ACCT(
	   pn_amount PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
	   pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
	   pc_redelivery_flag CHAR,
       pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
	   pc_auth_result_cd OUT VARCHAR2,
       pn_debitted_amount OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
	   pn_new_balance OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
       pc_auto_replenish_flag OUT VARCHAR2,
       pn_used_cash_back_balance OUT PSS.CONSUMER_ACCT.TOWARD_CASH_BACK_BALANCE%TYPE,
	   pn_cash_back_amount OUT PSS.SALE.SALE_AMOUNT%TYPE);  

    --R36 and below
    PROCEDURE DEBIT_CONSUMER_ACCT(
	   pn_amount PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
	   pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
	   pc_redelivery_flag CHAR,
       pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
	   pn_debitted_amount OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
	   pn_new_balance OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
       pc_auto_replenish_flag OUT VARCHAR2,
       pn_used_cash_back_balance OUT PSS.CONSUMER_ACCT.TOWARD_CASH_BACK_BALANCE%TYPE,
	   pn_cash_back_amount OUT PSS.SALE.SALE_AMOUNT%TYPE);  
    
    --R37 and above
    PROCEDURE CREDIT_CONSUMER_ACCT(
	   pn_amount PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
	   pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
	   pc_redelivery_flag CHAR,
       pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
	   pc_auth_result_cd OUT VARCHAR2,
       pn_creditted_amount OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
	   pn_new_balance OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE);

    --R36 and below
    PROCEDURE CREDIT_CONSUMER_ACCT(
	   pn_amount PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
	   pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
	   pc_redelivery_flag CHAR,
       pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
	   pn_creditted_amount OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
	   pn_new_balance OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE);
    
    -- For R37 and above
	PROCEDURE CHECK_REPLENISH_CONSUMER_ACCT(
	   pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
	   pv_replenish_device_serial_cd OUT DEVICE.DEVICE_SERIAL_CD%TYPE,
	   pc_auto_replenish_flag OUT VARCHAR2,
	   pn_replenish_id OUT PSS.CONSUMER_ACCT_REPLENISH.CONSUMER_ACCT_REPLENISH_ID%TYPE,
	   pn_replenish_amount OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_AMOUNT%TYPE,
	   pv_replenish_card_key OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_CARD_KEY%TYPE,
	   pn_global_account_id OUT PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
	   pn_consumer_id OUT PSS.CONSUMER_ACCT.CONSUMER_ID%TYPE,
	   pn_replenish_next_master_id OUT NUMBER,
	   pv_replenish_device_name OUT DEVICE.DEVICE_NAME%TYPE,
       pn_max_denied_count PLS_INTEGER DEFAULT -1);
       
    -- For R36 and below   
    PROCEDURE CHECK_REPLENISH_CONSUMER_ACCT(
	   pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
	   pv_replenish_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
       pc_auto_replenish_flag OUT VARCHAR2,
       pn_replenish_id OUT PSS.CONSUMER_ACCT_REPLENISH.CONSUMER_ACCT_REPLENISH_ID%TYPE,
       pn_replenish_amount OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_AMOUNT%TYPE,
       pv_replenish_card_key OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_CARD_KEY%TYPE,
       pb_consumer_acct_cd_hash OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD_HASH%TYPE,
       pn_replenish_next_master_id OUT NUMBER,
       pv_replenish_device_name OUT DEVICE.DEVICE_NAME%TYPE,
       pn_max_denied_count PLS_INTEGER DEFAULT -1);
    
    PROCEDURE SETUP_REPLENISH(
        pn_replenish_id IN OUT PSS.CONSUMER_ACCT_REPLENISH.CONSUMER_ACCT_REPLENISH_ID%TYPE,
        pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
        pn_replenish_type_id PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_TYPE_ID%TYPE,
        pv_replenish_card_masked PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_CARD_MASKED%TYPE,
        pv_replenish_device_serial_cd IN OUT DEVICE.DEVICE_SERIAL_CD%TYPE,
        pn_replenish_threshhold PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_THRESHHOLD%TYPE,
        pn_replenish_amount IN OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_AMOUNT%TYPE,
        pc_replenish_flag OUT VARCHAR2,
        pn_replenish_next_master_id OUT NUMBER,
        pv_replenish_device_name OUT DEVICE.DEVICE_NAME%TYPE);
    
    PROCEDURE UPDATE_PENDING_REPLENISH(
        pn_replenish_id PSS.CONSUMER_ACCT_REPLENISH.CONSUMER_ACCT_REPLENISH_ID%TYPE,
        pn_replenish_next_master_id NUMBER,
        pc_submitted_flag PSS.CONSUMER_ACCT_PEND_REPLENISH.SUBMITTED_FLAG%TYPE,
        pc_initial_replenish_flag CHAR,
        pn_denied_count OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_DENIED_COUNT%TYPE,
        pv_replenish_card_masked OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_CARD_MASKED%TYPE);
    
    -- R33 Signature
    PROCEDURE LOCK_AUTH_HOLD(
     	pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
		pn_ignore_auth_id PSS.AUTH.AUTH_ID%TYPE,
		pn_auth_hold_total OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE);
	
    -- R37+ Signature
    PROCEDURE AUTHORIZE_ISIS_PROMO(
		pn_global_account_id IN OUT PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
		pv_instance PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_INSTANCE%TYPE,
		pv_consumer_acct_cd PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD%TYPE,
		pv_consumer_identifier PSS.CONSUMER.CONSUMER_IDENTIFIER%TYPE,
		pv_currency_cd PSS.CURRENCY.CURRENCY_CD%TYPE,
		pv_auth_result_cd OUT VARCHAR2,
		pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
		pn_consumer_id OUT PSS.CONSUMER.CONSUMER_ID%TYPE,
		pn_trans_to_free_tran OUT NUMBER);
        
    -- R36 and below Signature     
	PROCEDURE AUTHORIZE_ISIS_PROMO(
		pv_consumer_acct_cd PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD%TYPE,
		pr_consumer_acct_cd_hash PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD_HASH%TYPE,
		pv_consumer_identifier PSS.CONSUMER.CONSUMER_IDENTIFIER%TYPE,
		pv_currency_cd PSS.CURRENCY.CURRENCY_CD%TYPE,
		pv_auth_result_cd OUT VARCHAR2,
		pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
		pn_consumer_id OUT PSS.CONSUMER.CONSUMER_ID%TYPE,
		pn_trans_to_free_tran OUT NUMBER);
		
	PROCEDURE REFUND_ISIS_PROMO(
		pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
		pn_result_cd OUT NUMBER,
		pv_error_message OUT VARCHAR2);
        
    PROCEDURE GET_OR_CREATE_CONSUMER_ACCT(
		pn_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
		pn_global_account_instance PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_INSTANCE%TYPE,
		pn_pos_pta_id PSS.POS_PTA.POS_PTA_ID%TYPE,
		pv_currency_cd PSS.CONSUMER_ACCT_BASE.CURRENCY_CD%TYPE,
        pd_auth_ts DATE,
        pv_truncated_card_num PSS.CONSUMER_ACCT_BASE.TRUNCATED_CARD_NUMBER%TYPE,
        pn_consumer_acct_id OUT PSS.CONSUMER_ACCT_BASE.CONSUMER_ACCT_ID%TYPE);
    
    PROCEDURE RESOLVE_ACCOUNT_CONFLICT(
        pn_old_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
		pn_new_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
		pn_global_account_instance PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_INSTANCE%TYPE);
            
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/PSS/PKG_TRAN.pbk?rev=1.197
CREATE OR REPLACE PACKAGE BODY PSS.PKG_TRAN IS
    
FUNCTION sf_find_host_id(
	pn_device_id DEVICE.DEVICE_ID%TYPE,
	pn_host_port_num IN HOST.HOST_PORT_NUM%TYPE,
	pn_host_position_num HOST.HOST_POSITION_NUM%TYPE)
  RETURN HOST.HOST_ID%TYPE
IS
	ln_host_id HOST.HOST_ID%TYPE;
BEGIN
	SELECT MAX(H.HOST_ID)
	  INTO ln_host_id
	  FROM DEVICE.HOST H
	 WHERE H.DEVICE_ID = pn_device_id
	   AND H.HOST_PORT_NUM = pn_host_port_num
	   AND H.HOST_POSITION_NUM = pn_host_position_num;

	IF ln_host_id IS NULL THEN
		-- Use base host
		SELECT MAX(H.HOST_ID)
		  INTO ln_host_id
		  FROM DEVICE.HOST H
		 WHERE H.DEVICE_ID = pn_device_id
		   AND H.HOST_PORT_NUM = 0;
	END IF;
    
	RETURN ln_host_id;
END;

PROCEDURE SP_CREATE_REFUND (
	pv_global_trans_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
	pv_orig_global_trans_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
	pn_refund_utc_ts_ms IN NUMBER,
	pn_orig_upload_utc_ts_ms IN NUMBER,
	pn_refund_amt PSS.REFUND.REFUND_AMT%TYPE,
	pn_refund_desc PSS.REFUND.REFUND_DESC%TYPE,
	pn_refund_issue_by PSS.REFUND.REFUND_ISSUE_BY%TYPE,
	pn_refund_type_cd PSS.REFUND.REFUND_TYPE_CD%TYPE,
	pc_entry_method_cd PSS.REFUND.ACCT_ENTRY_METHOD_CD%TYPE,
	pc_already_inserted_flag OUT VARCHAR2,
	pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
	pn_orig_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
	pn_minor_currency_factor OUT PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
	pn_payment_subtype_key_id OUT PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE,
	pv_payment_subtype_class OUT PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE)
IS
	ld_orig_tran_upload_ts PSS.TRAN.TRAN_UPLOAD_TS%TYPE;
	ld_refund_ts PSS.TRAN.TRAN_START_TS%TYPE;
	lv_last_lock_utc_ts VARCHAR2(128);
	ln_cnt PLS_INTEGER;
	ln_orig_tran_id PSS.TRAN.TRAN_ID%TYPE;
	lv_lock_string VARCHAR2(100);
	ln_start INTEGER;
	ln_end INTEGER;
    ln_consumer_acct_sub_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_SUB_TYPE_ID%TYPE;
    ln_override_trans_type_id PSS.REFUND.OVERRIDE_TRANS_TYPE_ID%TYPE;   
BEGIN
	ln_start := INSTR(pv_global_trans_cd, ':', 1, 1) + 1;
	ln_end := INSTR(pv_global_trans_cd, ':', 1, 3);
	IF ln_end <= 0 THEN
		ln_end := LENGTH(pv_global_trans_cd) + 1;
	END IF;
	lv_lock_string := SUBSTR(pv_global_trans_cd, ln_start,  ln_end -  ln_start);
	ld_orig_tran_upload_ts := DBADMIN.UTC_TO_LOCAL_DATE(DBADMIN.MILLIS_TO_TIMESTAMP(pn_orig_upload_utc_ts_ms));
	ld_refund_ts := DBADMIN.UTC_TO_LOCAL_DATE(DBADMIN.MILLIS_TO_TIMESTAMP(pn_refund_utc_ts_ms));
	lv_last_lock_utc_ts := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', lv_lock_string);
	-- check if refund already exists
	BEGIN
		SELECT X.TRAN_ID, c.MINOR_CURRENCY_FACTOR, pta.PAYMENT_SUBTYPE_KEY_ID, PST.PAYMENT_SUBTYPE_CLASS, 'Y'
		  INTO pn_tran_id, pn_minor_currency_factor, pn_payment_subtype_key_id, pv_payment_subtype_class, pc_already_inserted_flag
		  FROM PSS.TRAN X
		  JOIN PSS.POS_PTA PTA ON PTA.POS_PTA_ID = X.POS_PTA_ID
		  JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
		  JOIN PSS.PAYMENT_SUBTYPE PST ON PTA.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
		 WHERE X.TRAN_GLOBAL_TRANS_CD = pv_global_trans_cd;
		RETURN;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			pc_already_inserted_flag := 'N';
	END;
	-- Find original transaction
	BEGIN
		SELECT X.TRAN_ID, PSS.SEQ_TRAN_ID.NEXTVAL, c.MINOR_CURRENCY_FACTOR, pta.PAYMENT_SUBTYPE_KEY_ID, PST.PAYMENT_SUBTYPE_CLASS, CA.CONSUMER_ACCT_SUB_TYPE_ID
		  INTO pn_orig_tran_id, pn_tran_id, pn_minor_currency_factor, pn_payment_subtype_key_id, pv_payment_subtype_class, ln_consumer_acct_sub_type_id
		  FROM PSS.TRAN X
		  JOIN PSS.POS_PTA PTA ON PTA.POS_PTA_ID = X.POS_PTA_ID
		  JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
		  JOIN PSS.PAYMENT_SUBTYPE PST ON PTA.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
          LEFT OUTER JOIN PSS.CONSUMER_ACCT CA ON X.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
		 WHERE X.TRAN_GLOBAL_TRANS_CD = pv_orig_global_trans_cd
		   AND X.TRAN_UPLOAD_TS = ld_orig_tran_upload_ts;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			 RAISE_APPLICATION_ERROR(-20381, 'Original Transaction ''' || pv_orig_global_trans_cd || ''', uploaded at ' || TO_CHAR(ld_orig_tran_upload_ts, 'MM/DD/YYYY HH24:MI:SS') || ' not found');
	END;
    
	INSERT INTO PSS.TRAN (
			TRAN_ID,
			PARENT_TRAN_ID,
			TRAN_START_TS,
			TRAN_END_TS,
			TRAN_UPLOAD_TS,
			TRAN_GLOBAL_TRANS_CD,
			TRAN_STATE_CD,
			CONSUMER_ACCT_ID,
			TRAN_DEVICE_TRAN_CD,
			POS_PTA_ID,
			TRAN_DEVICE_RESULT_TYPE_CD,
			TRAN_RECEIVED_RAW_ACCT_DATA,
			PAYMENT_SUBTYPE_KEY_ID,
			PAYMENT_SUBTYPE_CLASS,
			CLIENT_PAYMENT_TYPE_CD,
			DEVICE_NAME
			)
	 SELECT pn_tran_id,
			pn_orig_tran_id,
			ld_refund_ts,
			ld_refund_ts,
			NULL, /* Must be NULL so that PSSUpdater will not pick it up */
			pv_global_trans_cd,
			'8',
			O.CONSUMER_ACCT_ID,
			SUBSTR(pv_global_trans_cd, INSTR(pv_global_trans_cd, ':', 1, 2) + 1, LENGTH(pv_global_trans_cd)),
			O.POS_PTA_ID,
			O.TRAN_DEVICE_RESULT_TYPE_CD,
			O.TRAN_RECEIVED_RAW_ACCT_DATA,
			pp.PAYMENT_SUBTYPE_KEY_ID,
			ps.PAYMENT_SUBTYPE_CLASS,
			ps.CLIENT_PAYMENT_TYPE_CD,
			d.DEVICE_NAME
	FROM PSS.TRAN O
	JOIN pss.pos_pta pp ON O.pos_pta_id = pp.pos_pta_id
	JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
	JOIN pss.pos p ON pp.pos_id = p.pos_id
	JOIN device.device d ON p.device_id = d.device_id
	WHERE O.TRAN_ID = pn_orig_tran_id;
    
    IF ln_consumer_acct_sub_type_id IS NOT NULL THEN
        IF ln_consumer_acct_sub_type_id = 2 THEN
            ln_override_trans_type_id := 31;
        ELSE
            ln_override_trans_type_id := 20;
        END IF;
    END IF;
    
	INSERT INTO PSS.REFUND (
			TRAN_ID,
			REFUND_AMT,
			REFUND_DESC,
			REFUND_ISSUE_TS,
			REFUND_ISSUE_BY,
			REFUND_TYPE_CD,
			REFUND_STATE_ID,
			ACCT_ENTRY_METHOD_CD,
            OVERRIDE_TRANS_TYPE_ID
		) VALUES (
			pn_tran_id,
			-ABS(pn_refund_amt),
			pn_refund_desc,
			ld_refund_ts,
			pn_refund_issue_by,
			pn_refund_type_cd,
			6,
			pc_entry_method_cd,
            ln_override_trans_type_id);
END;

PROCEDURE SP_CLOSE_CONSUMER_ACCT (
	pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
	pv_refund_issue_by PSS.REFUND.REFUND_ISSUE_BY%TYPE,
	pn_refund_amt OUT PSS.REFUND.REFUND_AMT%TYPE,
	pn_eft_credit_amt OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
	pn_doc_id OUT CORP.DOC.DOC_ID%TYPE
)
IS
	ln_consumer_acct_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_TYPE_ID%TYPE;
	ln_consumer_acct_sub_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_SUB_TYPE_ID%TYPE;
	ln_corp_customer_id PSS.CONSUMER_ACCT.CORP_CUSTOMER_ID%TYPE;
	lv_currency_cd PSS.CONSUMER_ACCT.CURRENCY_CD%TYPE;
	lv_consumer_acct_cd PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD%TYPE;
	lv_consumer_acct_identifier PSS.CONSUMER_ACCT.CONSUMER_ACCT_IDENTIFIER%TYPE;
	ln_refund_amt PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE;	
	ln_eft_credit_amt PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE;	
	ln_tran_id PSS.TRAN.TRAN_ID%TYPE;
	ln_orig_tran_id PSS.TRAN.PARENT_TRAN_ID%TYPE;
	ln_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE;
	ld_refund_ts PSS.TRAN.TRAN_START_TS%TYPE := SYSDATE;
	lv_replenish_card_masked PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD%TYPE;
	lv_tran_device_tran_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE;
	ln_ledger_id CORP.LEDGER.LEDGER_ID%TYPE;
BEGIN
	pn_refund_amt := 0;
	pn_eft_credit_amt := 0;
	
	UPDATE PSS.CONSUMER_ACCT
	   SET CONSUMER_ACCT_ACTIVE_YN_FLAG = 'N'
	 WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id
	  RETURNING CONSUMER_ACCT_TYPE_ID, CONSUMER_ACCT_REPLEN_BALANCE, CONSUMER_ACCT_PROMO_BALANCE,
		CORP_CUSTOMER_ID, CURRENCY_CD, CONSUMER_ACCT_CD, CONSUMER_ACCT_IDENTIFIER, CONSUMER_ACCT_SUB_TYPE_ID
	  INTO ln_consumer_acct_type_id, ln_refund_amt, ln_eft_credit_amt, ln_corp_customer_id, lv_currency_cd, lv_consumer_acct_cd, lv_consumer_acct_identifier, ln_consumer_acct_sub_type_id;
    
	UPDATE PSS.CONSUMER_ACCT
	   SET CONSUMER_ACCT_BALANCE = 0,
		   CONSUMER_ACCT_REPLEN_BALANCE = 0,
		   CONSUMER_ACCT_PROMO_BALANCE = 0
	 WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id;
	 
	IF ln_consumer_acct_type_id != 3 OR ln_consumer_acct_sub_type_id != 1 THEN
		RETURN;
	END IF;
	
	IF ln_refund_amt > 0 THEN
		--TODO: Handle when tran is no longer in table (b/c of retention policy)
		SELECT LAST_REPLENISH_TRAN_ID, REPLENISH_POS_PTA_ID, REPLENISH_CARD_MASKED
		  INTO ln_orig_tran_id, ln_pos_pta_id, lv_replenish_card_masked
		  FROM (SELECT LAST_REPLENISH_TRAN_ID, REPLENISH_POS_PTA_ID, REPLENISH_CARD_MASKED
				  FROM PSS.CONSUMER_ACCT_REPLENISH
				 WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id
				 ORDER BY LAST_REPLENISH_TRAN_TS DESC, PRIORITY, LAST_REPLENISH_TRAN_ID DESC)
		 WHERE ROWNUM = 1;
     
		SELECT PSS.SEQ_TRAN_ID.NEXTVAL
		INTO ln_tran_id
		FROM DUAL;
		
		lv_tran_device_tran_cd := DBADMIN.DATE_TO_MILLIS(ld_refund_ts) / 1000;
	
		INSERT INTO PSS.TRAN (
					TRAN_ID,
					PARENT_TRAN_ID,
					TRAN_START_TS,
					TRAN_END_TS,
					TRAN_UPLOAD_TS,
					TRAN_GLOBAL_TRANS_CD,
					TRAN_STATE_CD,
					TRAN_DEVICE_TRAN_CD,
					POS_PTA_ID,
					TRAN_DEVICE_RESULT_TYPE_CD,
					TRAN_RECEIVED_RAW_ACCT_DATA,
					PAYMENT_SUBTYPE_KEY_ID,
					PAYMENT_SUBTYPE_CLASS,
					CLIENT_PAYMENT_TYPE_CD,
					DEVICE_NAME
					)
			 SELECT ln_tran_id,
					ln_orig_tran_id,
					ld_refund_ts,
					ld_refund_ts,
					NULL,
					'RF:' || d.device_name || ':' || lv_tran_device_tran_cd || ':R1',
					PKG_CONST.TRAN_STATE__BATCH,
					lv_tran_device_tran_cd,
					ln_pos_pta_id,
					PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_RCPT,
					lv_replenish_card_masked,
					pp.PAYMENT_SUBTYPE_KEY_ID,
					ps.PAYMENT_SUBTYPE_CLASS,
					ps.CLIENT_PAYMENT_TYPE_CD,
					d.DEVICE_NAME
			FROM pss.pos_pta pp
			JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
			JOIN pss.pos p ON pp.pos_id = p.pos_id
			JOIN device.device d ON p.device_id = d.device_id
			WHERE pp.pos_pta_id = ln_pos_pta_id;
			
		INSERT INTO PSS.TRAN_LINE_ITEM (
			TRAN_ID,
			TRAN_LINE_ITEM_AMOUNT,
			TRAN_LINE_ITEM_POSITION_CD,
			TRAN_LINE_ITEM_TAX,
			TRAN_LINE_ITEM_TYPE_ID,
			TRAN_LINE_ITEM_QUANTITY,
			TRAN_LINE_ITEM_DESC,
			TRAN_LINE_ITEM_BATCH_TYPE_CD,
			SALE_RESULT_ID,
			APPLY_TO_CONSUMER_ACCT_ID
		) VALUES (
			ln_tran_id,
			-ln_refund_amt,
			'0',
			0,
			500,
			1,
			'Prepaid account closure',
			PKG_CONST.TRAN_BATCH_TYPE__ACTUAL,
			PKG_CONST.SALE_RES__SUCCESS,
			pn_consumer_acct_id
		);
			
		INSERT INTO PSS.REFUND (
			TRAN_ID,
			REFUND_AMT,
			REFUND_DESC,
			REFUND_ISSUE_TS,
			REFUND_ISSUE_BY,
			REFUND_TYPE_CD,
			REFUND_STATE_ID,
			ACCT_ENTRY_METHOD_CD
		) VALUES (
			ln_tran_id,
			-ln_refund_amt,
			'Prepaid account closure',
			ld_refund_ts,
			pv_refund_issue_by,
			'G',
			6,
			2);
			
		pn_refund_amt := ln_refund_amt;
	END IF;
	
	IF ln_eft_credit_amt > 0 THEN
		CORP.PAYMENTS_PKG.ADJUSTMENT_INS(ln_corp_customer_id, pv_refund_issue_by, lv_currency_cd,
			'Promo credit for prepaid account closure, card # ' || lv_consumer_acct_cd || ', card ID ' || lv_consumer_acct_identifier,
			ln_eft_credit_amt, pn_doc_id, ln_ledger_id);
		pn_eft_credit_amt := ln_eft_credit_amt;
	END IF;
END;

PROCEDURE PROCESS_ISIS_TRAN (
	pn_tran_id PSS.TRAN.TRAN_ID%TYPE	
)
IS
	ln_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE;
	lv_isis_promo_status_cd PSS.PROMOTION.STATUS_CD%TYPE;
	ln_tran_diff PSS.CONSUMER_PROMOTION.TRAN_COUNT%TYPE;
	lv_tran_info PSS.TRAN.TRAN_INFO%TYPE;
	lv_payment_subtype_class PSS.TRAN.PAYMENT_SUBTYPE_CLASS%TYPE; 
	lv_auth_resp_cd PSS.AUTH.AUTH_RESP_CD%TYPE;
	ln_sale_amount PSS.SALE.SALE_AMOUNT%TYPE;
	ln_isis_promo_pos_pta_id PSS.POS_PTA.POS_PTA_ID%TYPE;
	ld_sysdate DATE := SYSDATE;
BEGIN
	SELECT NVL(MAX(STATUS_CD), 'D')
	INTO lv_isis_promo_status_cd
	FROM PSS.PROMOTION 
	WHERE PROMOTION_ID = 1;
	
	IF lv_isis_promo_status_cd != 'A' THEN
		RETURN;
	END IF;
	
	SELECT MAX(CA.CONSUMER_ID), MAX(S.SALE_AMOUNT), NVL(MAX(T.TRAN_INFO), '-'), NVL(MAX(T.PAYMENT_SUBTYPE_CLASS), '-'), NVL(MAX(A.AUTH_RESP_CD), '-'), NVL(MAX(PP2.POS_PTA_ID), 0)
	INTO ln_consumer_id, ln_sale_amount, lv_tran_info, lv_payment_subtype_class, lv_auth_resp_cd, ln_isis_promo_pos_pta_id
	FROM PSS.TRAN T
	JOIN PSS.SALE S ON T.TRAN_ID = S.TRAN_ID
	JOIN PSS.CONSUMER_ACCT CA ON T.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID	
	JOIN PSS.POS_PTA PP ON T.POS_PTA_ID = PP.POS_PTA_ID
	JOIN PSS.PAYMENT_SUBTYPE PS ON PP.PAYMENT_SUBTYPE_ID = PS.PAYMENT_SUBTYPE_ID
	LEFT OUTER JOIN PSS.AUTH A ON T.TRAN_ID = A.TRAN_ID AND A.AUTH_TYPE_CD = 'N' AND A.AUTH_STATE_ID IN (2, 5)
	LEFT OUTER JOIN (PSS.POS_PTA PP2
		JOIN PSS.PAYMENT_SUBTYPE PS2 ON PP2.PAYMENT_SUBTYPE_ID = PS2.PAYMENT_SUBTYPE_ID AND PS2.PAYMENT_SUBTYPE_CLASS = 'Isis'
	) ON PP.POS_ID = PP2.POS_ID AND PP2.POS_PTA_ACTIVATION_TS < ld_sysdate AND (PP2.POS_PTA_DEACTIVATION_TS IS NULL OR PP2.POS_PTA_DEACTIVATION_TS > ld_sysdate)
		AND NVL(PP.AUTHORITY_PAYMENT_MASK_ID, PS.AUTHORITY_PAYMENT_MASK_ID) = NVL(PP2.AUTHORITY_PAYMENT_MASK_ID, PS2.AUTHORITY_PAYMENT_MASK_ID)
		AND (T.PAYMENT_SUBTYPE_CLASS = 'Isis' OR PP.POS_PTA_ID != PP2.POS_PTA_ID)
	WHERE T.TRAN_ID = pn_tran_id;
	
	IF ln_consumer_id IS NULL OR ln_sale_amount IS NULL OR lv_tran_info LIKE '%Isis loyalty updated%' OR ln_isis_promo_pos_pta_id < 1 THEN
		RETURN;
	END IF;
	
	IF ln_sale_amount > 0 THEN
		IF lv_payment_subtype_class != 'Isis' THEN
			LOOP
				UPDATE PSS.CONSUMER_PROMOTION
				SET TRAN_COUNT = TRAN_COUNT + 1
				WHERE CONSUMER_ID = ln_consumer_id AND PROMOTION_ID = 1;
				
				IF SQL%FOUND THEN
					EXIT;
				END IF;
				
				BEGIN
					INSERT INTO PSS.CONSUMER_PROMOTION(CONSUMER_ID, PROMOTION_ID, TRAN_COUNT)
					VALUES(ln_consumer_id, 1, 1);
					EXIT;
				EXCEPTION
					WHEN DUP_VAL_ON_INDEX THEN
						NULL;
				END;
			END LOOP;
		END IF;
	ELSE
		IF lv_payment_subtype_class = 'Isis' AND lv_auth_resp_cd = 'ISIS_PROMO' THEN
			UPDATE PSS.CONSUMER_PROMOTION
			SET TRAN_COUNT = CASE WHEN TRAN_COUNT - 1 >= 0 THEN TRAN_COUNT - 1 ELSE 0 END,
				PROMO_TRAN_COUNT = CASE WHEN PROMO_TRAN_COUNT - 1 >= 0 THEN PROMO_TRAN_COUNT - 1 ELSE 0 END
			WHERE CONSUMER_ID = ln_consumer_id AND PROMOTION_ID = 1;
		END IF;
	END IF;
	
	UPDATE PSS.TRAN
	SET TRAN_INFO = SUBSTR('Isis loyalty updated' || DECODE(TRAN_INFO, NULL, '', ';' || TRAN_INFO), 1, 1000)
	WHERE TRAN_ID = pn_tran_id AND NVL(TRAN_INFO, '-') NOT LIKE '%Isis loyalty updated%';
END;

-- R33+ signature
PROCEDURE sp_create_sale(
	pc_global_event_cd_prefix IN CHAR,
	pv_device_name IN device.device_name%TYPE,
	pv_device_tran_cd IN pss.tran.tran_device_tran_cd%TYPE,
	pn_device_batch_id IN pss.sale.device_batch_id%TYPE,
	pc_sale_type_cd IN pss.sale.sale_type_cd%TYPE,
	pn_sale_utc_ts_ms IN NUMBER,
	pn_sale_utc_offset_min IN pss.sale.sale_utc_offset_min%TYPE,
	pv_tran_device_result_type_cd IN pss.tran.tran_device_result_type_cd%TYPE,
	pn_sale_result_id IN pss.sale.sale_result_id%TYPE,
	pn_sale_amount IN NUMBER,
	pc_receipt_result_cd IN pss.sale.receipt_result_cd%TYPE,
	pc_tran_batch_type_cd IN pss.tran_line_item_batch_type.tran_line_item_batch_type_cd%TYPE,
	pv_hash_type_cd IN pss.sale.hash_type_cd%TYPE,
	pv_tran_line_item_hash IN pss.sale.tran_line_item_hash%TYPE,
	pv_global_session_cd IN VARCHAR2,
	pn_result_cd OUT NUMBER,
	pv_error_message OUT VARCHAR2,
	pn_tran_id OUT pss.tran.tran_id%TYPE,
	pv_tran_state_cd OUT pss.tran.tran_state_cd%TYPE,
	pc_void_allowed IN PSS.SALE.VOID_ALLOWED%TYPE DEFAULT 'N')
IS
/*
	Returned result codes:
		RESULT__SUCCESS
		RESULT__FAILURE
		RESULT__INVALID_PARAMETER
		RESULT__DUPLICATE
*/
	lv_global_trans_cd pss.tran.tran_global_trans_cd%TYPE;
	ld_tran_upload_ts pss.tran.tran_upload_ts%TYPE;
	lt_sale_start_utc_ts pss.sale.sale_start_utc_ts%TYPE;
	ld_tran_start_ts pss.tran.tran_start_ts%TYPE;
	ld_original_tran_start_ts pss.tran.tran_start_ts%TYPE;
	ld_tran_server_ts DATE;
	ln_device_id device.device_id%TYPE;
	ln_pos_pta_id pss.tran.pos_pta_id%TYPE;
	ld_current_ts DATE := SYSDATE;
	lc_client_payment_type_cd pss.client_payment_type.client_payment_type_cd%TYPE;
	ln_tran_exists NUMBER := PKG_CONST.BOOLEAN__FALSE;
	ln_tli_hash_match NUMBER;
	ln_insert_tran NUMBER := PKG_CONST.BOOLEAN__FALSE;
	ln_original_tran_id pss.tran.tran_id%TYPE;
	ln_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE;
	lc_sale_type_cd pss.sale.sale_type_cd%TYPE;
	lv_last_lock_utc_ts VARCHAR2(128);
	lc_auth_hold_used PSS.TRAN.AUTH_HOLD_USED%TYPE;
	lv_orig_tran_state_cd pss.tran.tran_state_cd%TYPE;
	ln_consumer_acct_id pss.tran.consumer_acct_id%TYPE;
	ln_auth_amt_approved pss.auth.auth_amt_approved%TYPE;
	ln_auth_amt_allowed pss.auth.auth_amt_approved%TYPE;
	ln_sale_over_auth_amt_percent NUMBER;
	lv_email_from_address engine.app_setting.app_setting_value%TYPE;
	lv_email_to_address engine.app_setting.app_setting_value%TYPE;
	lv_error pss.tran.tran_info%TYPE;
	lc_original_void_allowed PSS.SALE.VOID_ALLOWED%TYPE;
	lv_card_key PSS.AUTH.CARD_KEY%TYPE;
	ln_original_sale_amount PSS.SALE.SALE_AMOUNT%TYPE;  
	lc_entry_method_cd PSS.AUTH.ACCT_ENTRY_METHOD_CD%TYPE;
	lc_calc_tran_state_cd CHAR(1) := 'Y';
	ln_host_id PSS.TRAN_LINE_ITEM.HOST_ID%TYPE;
	ln_new_host_count PLS_INTEGER;
	lc_orig_term_capture_flag CHAR(1);
	ln_count PLS_INTEGER;	
	ln_remaining_refund_amt PSS.REFUND.REFUND_AMT%TYPE;
BEGIN
	pn_result_cd := PKG_CONST.RESULT__SUCCESS;
	pv_error_message := PKG_CONST.ERROR__NO_ERROR;
	pn_tran_id := 0;

	IF pc_global_event_cd_prefix NOT IN (PKG_CONST.EVENT_CODE_PREFIX__APP_LAYER,
		PKG_CONST.EVENT_CODE_PREFIX__LEGACY) THEN
		pn_result_cd := PKG_CONST.RESULT__INVALID_PARAMETER;
		pv_error_message := 'Invalid pc_global_event_cd_prefix: ' || pc_global_event_cd_prefix;
		RETURN;
	END IF;

	lv_global_trans_cd := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(pc_global_event_cd_prefix, pv_device_name, pv_device_tran_cd);
    
	lt_sale_start_utc_ts := CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_sale_utc_ts_ms) AS DATE);
	ld_tran_start_ts := CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_sale_utc_ts_ms + pn_sale_utc_offset_min * 60 * 1000) AS DATE);
	ld_tran_server_ts := CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_sale_utc_ts_ms) - SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) + CURRENT_TIMESTAMP AS DATE);

	lv_last_lock_utc_ts := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', pv_device_name || ':' || pv_device_tran_cd);

	BEGIN
		SELECT tran_id, tran_state_cd, tran_start_ts, tran_upload_ts, PKG_CONST.BOOLEAN__TRUE, tli_hash_match, 
			   pos_pta_id, sale_type_cd, auth_hold_used, consumer_acct_id, VOID_ALLOWED, 
			   SALE_AMOUNT
		INTO pn_tran_id, pv_tran_state_cd, ld_original_tran_start_ts, ld_tran_upload_ts, ln_tran_exists, ln_tli_hash_match, 
			 ln_pos_pta_id, lc_sale_type_cd, lc_auth_hold_used, ln_consumer_acct_id, lc_original_void_allowed, 
			 ln_original_sale_amount
		FROM
		(
			SELECT /*+ INDEX(t IDX_DEVICE_TRAN_CD) */ t.tran_id, t.tran_state_cd, t.tran_start_ts, t.tran_upload_ts,
				CASE WHEN s.hash_type_cd = pv_hash_type_cd
					AND s.tran_line_item_hash = pv_tran_line_item_hash
					AND s.sale_type_cd = pc_sale_type_cd THEN PKG_CONST.BOOLEAN__TRUE
				ELSE PKG_CONST.BOOLEAN__FALSE END AS tli_hash_match,
				t.pos_pta_id, s.sale_type_cd, NVL(t.auth_hold_used, 'N') auth_hold_used, t.consumer_acct_id, 
				s.VOID_ALLOWED, s.SALE_AMOUNT
			FROM pss.tran t
			LEFT OUTER JOIN pss.sale s ON t.tran_id = s.tran_id
			WHERE t.tran_device_tran_cd = pv_device_tran_cd 
			  AND (t.tran_global_trans_cd = lv_global_trans_cd OR t.tran_global_trans_cd LIKE lv_global_trans_cd || ':%')
			ORDER BY CASE WHEN s.sale_type_cd = pc_sale_type_cd THEN 1 
						  WHEN s.sale_type_cd IN('A', 'I') AND pc_sale_type_cd IN('A', 'I') THEN 2 
						  WHEN s.sale_type_cd IS NULL AND pc_sale_type_cd IN('A', 'I') THEN 3 
						  ELSE 4 END,
				CASE WHEN t.tran_global_trans_cd = lv_global_trans_cd THEN 1 ELSE 2 END,
				tli_hash_match DESC, t.tran_start_ts, t.created_ts
		)
		WHERE ROWNUM = 1;
    
		ln_original_tran_id := pn_tran_id;
		lv_orig_tran_state_cd := pv_tran_state_cd;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			ln_insert_tran := PKG_CONST.BOOLEAN__TRUE;
	END;

	-- Handle each case
	IF ln_tran_exists = PKG_CONST.BOOLEAN__TRUE THEN
		IF ln_tli_hash_match = PKG_CONST.BOOLEAN__TRUE THEN
			UPDATE pss.sale
			   SET duplicate_count = duplicate_count + 1
			 WHERE tran_id = pn_tran_id;
    
			pv_tran_state_cd := PKG_CONST.TRAN_STATE__DUPLICATE;
			pn_result_cd := PKG_CONST.RESULT__DUPLICATE;
			pv_error_message := 'Duplicate sale, original tran_id: ' || pn_tran_id;
			RETURN;
		ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__CASH THEN
			lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CASH;
			pv_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE;
			ln_insert_tran := PKG_CONST.BOOLEAN__TRUE;
		ELSIF lc_sale_type_cd = PKG_CONST.SALE_TYPE__CASH THEN
			ln_insert_tran := PKG_CONST.BOOLEAN__TRUE;
			IF pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED THEN
				lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CREDIT;
				pv_tran_state_cd := PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND;
			ELSIF pn_sale_result_id != 0 /* Success */ AND NVL(pn_sale_amount, 0) = 0 THEN
				lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CREDIT;
				pv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED;
			ELSE
				lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CREDIT;
				pv_tran_state_cd := PKG_CONST.TRAN_STATE__SALE_NO_AUTH;
			END IF;
		ELSIF lc_sale_type_cd = PKG_CONST.SALE_TYPE__ACTUAL AND pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED THEN
			RETURN; -- ignore this as we have already processd the actual sale
		ELSIF lc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED AND pc_sale_type_cd = PKG_CONST.SALE_TYPE__ACTUAL THEN
			NULL; -- just update the transaction  
		ELSIF lc_sale_type_cd IS NOT NULL THEN 
			lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CREDIT;
			IF NVL(ln_original_sale_amount, 0) = 0 AND pc_void_allowed = 'Y' THEN
				RETURN; -- we have already processed the cancel of this charged sale
			ELSIF (pv_tran_device_result_type_cd IN (PKG_CONST.TRAN_DEV_RES__CANCELLED,
				PKG_CONST.TRAN_DEV_RES__AUTH_FAILURE,
				PKG_CONST.TRAN_DEV_RES__FAILURE,
				PKG_CONST.TRAN_DEV_RES__TIMEOUT) OR pn_sale_amount <= 0) 
				AND lc_original_void_allowed = 'Y' 
				AND NVL(ln_original_sale_amount, 0) > 0
				AND pv_tran_state_cd IN(
					PKG_CONST.TRAN_STATE__SALE_NO_AUTH, 
					PKG_CONST.TRAN_STATE__BATCH, 
					PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND, 
					PKG_CONST.TRAN_STATE__BATCH_INTENDED,
					PKG_CONST.TRAN_STATE__PROCESSED_TRAN, 
					PKG_CONST.TRAN_STATE__PROCESSED_TRAN_INT,
					PKG_CONST.TRAN_STATE__PROCESSING_TRAN,
					PKG_CONST.TRAN_STATE__INCOMPLETE,
					PKG_CONST.TRAN_STATE__INCOMPLETE_ERROR,
					PKG_CONST.TRAN_STATE__PROCESSING_STTLMT,
					PKG_CONST.TRAN_STATE__SETTLEMENT,
					PKG_CONST.TRAN_STATE__COMPLETE,
					PKG_CONST.TRAN_STATE__STLMT_INCOMPLETE,
					PKG_CONST.TRAN_STATE__STLMT_ERROR,
					PKG_CONST.TRAN_STATE__PROCESSING_BATCH,
					PKG_CONST.TRAN_STATE__PRCSNG_TRAN_RETRY) THEN -- cancel of a charge sale
                
				-- Lock tran row by updating it
				UPDATE PSS.TRAN
				   SET TRAN_STATE_CD = pv_tran_state_cd
				 WHERE TRAN_ID = pn_tran_id
				   AND TRAN_STATE_CD = pv_tran_state_cd;
				IF SQL%NOTFOUND THEN
					RAISE_APPLICATION_ERROR(-20120, 'Tran State Cd changed while processing tran ' || pn_tran_id || '; please retry');
				END IF;
				-- figure out what to do based on current transaction state
				IF pv_tran_state_cd IN(
					PKG_CONST.TRAN_STATE__SALE_NO_AUTH, 
					PKG_CONST.TRAN_STATE__BATCH, 
					PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND, 
					PKG_CONST.TRAN_STATE__BATCH_INTENDED) THEN 
					lc_calc_tran_state_cd := 'N'; -- easy case: update tran and line items and leave state as is
				ELSE
					IF pv_tran_state_cd IN(PKG_CONST.TRAN_STATE__PROCESSED_TRAN) THEN
						SELECT MAX(TB.TERMINAL_CAPTURE_FLAG) 
						  INTO lc_orig_term_capture_flag
						  FROM PSS.TERMINAL_BATCH TB
						  JOIN PSS.AUTH A ON A.TERMINAL_BATCH_ID = TB.TERMINAL_BATCH_ID
						 WHERE TB.TERMINAL_BATCH_CLOSE_TS IS NULL 
						  AND A.TRAN_ID = pn_tran_id;
					END IF;
					IF pv_tran_state_cd IN(PKG_CONST.TRAN_STATE__PROCESSED_TRAN) AND lc_orig_term_capture_flag = 'Y' THEN
						IF lc_auth_hold_used = 'Y' THEN
							UPDATE PSS.AUTH
							   SET AUTH_TYPE_CD = 'C', 
								   AUTH_AMT = 0 
							 WHERE TRAN_ID = pn_tran_id 
							   AND AUTH_TYPE_CD = 'U';
							lc_calc_tran_state_cd := 'N'; -- easy case: update tran and line items and leave state as is
						ELSE --Remove from batch and cancel
							UPDATE PSS.AUTH A
							   SET TERMINAL_BATCH_ID = NULL
							 WHERE A.TRAN_ID = pn_tran_id
							   AND A.TERMINAL_BATCH_ID IS NOT NULL
							   AND (SELECT TB.TERMINAL_BATCH_CLOSE_TS FROM PSS.TERMINAL_BATCH TB WHERE TB.TERMINAL_BATCH_ID = A.TERMINAL_BATCH_ID) IS NULL;
							pv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED; 
							lc_calc_tran_state_cd := 'N';
						END IF;
					ELSE -- create a refund and return
						ln_original_tran_id := pn_tran_id;
						pv_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH;
						SELECT NVL(SUM(R.REFUND_AMT), 0) - ABS(ln_original_sale_amount), MAX(T.TRAN_ID)
						  INTO ln_remaining_refund_amt, pn_tran_id 
						  FROM PSS.TRAN T
						  JOIN PSS.REFUND R ON T.TRAN_ID = R.TRAN_ID
						 WHERE T.PARENT_TRAN_ID = ln_original_tran_id;
						IF ln_remaining_refund_amt < 0 THEN
							SELECT pss.seq_tran_id.NEXTVAL INTO pn_tran_id FROM DUAL; 
							INSERT INTO PSS.TRAN (
									TRAN_ID,
									PARENT_TRAN_ID,
									TRAN_START_TS,
									TRAN_END_TS,
									TRAN_UPLOAD_TS,
									TRAN_GLOBAL_TRANS_CD,
									TRAN_STATE_CD,
									CONSUMER_ACCT_ID,
									TRAN_DEVICE_TRAN_CD,
									POS_PTA_ID,
									TRAN_DEVICE_RESULT_TYPE_CD,
									TRAN_RECEIVED_RAW_ACCT_DATA,
									PAYMENT_SUBTYPE_KEY_ID,
									PAYMENT_SUBTYPE_CLASS,
									CLIENT_PAYMENT_TYPE_CD,
									DEVICE_NAME)
							 SELECT pn_tran_id,
									ln_original_tran_id,
									ld_tran_start_ts,
									ld_tran_start_ts,
									ld_current_ts, /* Must NOT be NULL so that it will be imported */
									'RF' || SUBSTR(O.TRAN_GLOBAL_TRANS_CD, INSTR(O.TRAN_GLOBAL_TRANS_CD, ':'), 56) || ':1', 
									pv_tran_state_cd,
									ln_consumer_acct_id,
									pv_device_tran_cd,
									O.POS_PTA_ID,
									pv_tran_device_result_type_cd,
									O.TRAN_RECEIVED_RAW_ACCT_DATA,
									pp.PAYMENT_SUBTYPE_KEY_ID,
									ps.PAYMENT_SUBTYPE_CLASS,
									ps.CLIENT_PAYMENT_TYPE_CD,
									d.DEVICE_NAME
							FROM PSS.TRAN O
							JOIN pss.pos_pta pp ON O.pos_pta_id = pp.pos_pta_id
							JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
							JOIN pss.pos p ON pp.pos_id = p.pos_id
							JOIN device.device d ON p.device_id = d.device_id
							WHERE O.TRAN_ID = ln_original_tran_id;
							SELECT MAX(ACCT_ENTRY_METHOD_CD)
							  INTO lc_entry_method_cd
							  FROM (SELECT ACCT_ENTRY_METHOD_CD
									  FROM PSS.AUTH
									 WHERE TRAN_ID = ln_original_tran_id
									 ORDER BY DECODE(AUTH_TYPE_CD, 'N', 1, 5), AUTH_RESULT_CD DESC, AUTH_TS, AUTH_ID)
							 WHERE ROWNUM = 1;
							SELECT p.DEVICE_ID
							  INTO ln_device_id
							  FROM PSS.POS_PTA PTA
							  JOIN PSS.POS P ON PTA.POS_ID = P.POS_ID
							 WHERE PTA.POS_PTA_ID = ln_pos_pta_id;
            
							INSERT INTO PSS.REFUND (
									TRAN_ID,
									REFUND_AMT,
									REFUND_DESC,
									REFUND_ISSUE_TS,
									REFUND_ISSUE_BY,
									REFUND_TYPE_CD,
									REFUND_STATE_ID,
									ACCT_ENTRY_METHOD_CD
								) VALUES (
									pn_tran_id,
									ln_remaining_refund_amt,
									'Void of Charged Sale',
									ld_current_ts,
									'PSS',
									'V',
									6,
									lc_entry_method_cd);
							-- Insert line item
							ln_host_id := sf_find_host_id(ln_device_id, 0, 0);
							IF ln_host_id IS NULL THEN
								-- create default hosts
								pkg_device_configuration.sp_create_default_hosts(ln_device_id, ln_new_host_count, pn_result_cd, pv_error_message);
								IF pn_result_cd != PKG_CONST.RESULT__SUCCESS THEN
									RETURN;
								END IF;
								ln_host_id := sf_find_host_id(ln_device_id, 0, 0);
							END IF;
							INSERT INTO pss.tran_line_item (
								tran_line_item_id,
								tran_id,
								tran_line_item_amount,
								tran_line_item_position_cd,
								tran_line_item_tax,
								tran_line_item_type_id,
								tran_line_item_quantity,
								tran_line_item_desc,
								host_id,
								tran_line_item_batch_type_cd,
								tran_line_item_ts,
								sale_result_id
							) VALUES (
								PSS.SEQ_TLI_ID.NEXTVAL,
								pn_tran_id,
								ln_remaining_refund_amt,
								NULL,
								NULL,
								312, /*Cancellation Adjustment */
								1,
								'Void of Charged Sale',
								ln_host_id,
								'A',
								ld_current_ts,
								0);
						END IF;       
						pn_result_cd := PKG_CONST.RESULT__SALE_VOIDED;
						pv_error_message := 'Refund issued for canceled transaction already in-process or settled, refund tran_id: ' || pn_tran_id || ', original tran_id: ' || ln_original_tran_id;
						RETURN;
					END IF;
				END IF;
			ELSE
				SELECT CASE WHEN (lc_auth_hold_used = 'Y' OR NVL(SUM(DECODE(TLI.TRAN_LINE_ITEM_BATCH_TYPE_CD, 'I', TLI.TRAN_LINE_ITEM_AMOUNT * TLI.TRAN_LINE_ITEM_QUANTITY)), 0) > 0) AND NVL(SUM(DECODE(TLI.TRAN_LINE_ITEM_BATCH_TYPE_CD, 'A', TLI.TRAN_LINE_ITEM_AMOUNT * TLI.TRAN_LINE_ITEM_QUANTITY)), 0) = 0 
							THEN PKG_CONST.TRAN_STATE__COMPLETE_ERROR
							ELSE PKG_CONST.TRAN_STATE__DUPLICATE
					   END
				  INTO pv_tran_state_cd
				  FROM PSS.TRAN_LINE_ITEM TLI
				 WHERE TLI.TRAN_ID = pn_tran_id;
				ln_insert_tran := PKG_CONST.BOOLEAN__TRUE;          
				IF pv_tran_state_cd = PKG_CONST.TRAN_STATE__DUPLICATE THEN
					pn_result_cd := PKG_CONST.RESULT__DUPLICATE;
					pv_error_message := 'Duplicate sale with different line items, original tran_id: ' || ln_original_tran_id;
				END IF;
			END IF;
		ELSE
			NULL; -- just update the transaction  
		END IF;
	END IF;
	
	ln_device_id := PKG_DEVICE_CONFIGURATION.GET_DEVICE_ID_BY_NAME(pv_device_name, ld_tran_server_ts);
    
	IF ln_insert_tran = PKG_CONST.BOOLEAN__TRUE THEN
		IF lc_client_payment_type_cd IS NULL THEN
			IF pc_sale_type_cd = PKG_CONST.SALE_TYPE__CASH THEN
				lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CASH;
				pv_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE;
			ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED THEN
				lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CREDIT;
				pv_tran_state_cd := PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND;
			ELSIF pn_sale_result_id != 0 /* Success */ AND NVL(pn_sale_amount, 0) = 0 THEN
				lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CREDIT;
				pv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED;
			ELSE
				lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CREDIT;
				pv_tran_state_cd := PKG_CONST.TRAN_STATE__SALE_NO_AUTH;
			END IF;
		END IF;
		SELECT pss.seq_tran_id.NEXTVAL INTO pn_tran_id FROM DUAL;

		IF ln_tran_exists = PKG_CONST.BOOLEAN__TRUE THEN
			lv_global_trans_cd := lv_global_trans_cd || ':' || pn_tran_id;
		END IF;

		PKG_POS_PTA.SP_GET_OR_CREATE_POS_PTA(ln_device_id, pv_device_name, lc_client_payment_type_cd, lt_sale_start_utc_ts, pn_result_cd, pv_error_message, ln_pos_pta_id);
		
		SELECT c.MINOR_CURRENCY_FACTOR
		INTO ln_minor_currency_factor
		FROM PSS.POS_PTA PTA
		JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
		WHERE PTA.POS_PTA_ID = ln_pos_pta_id;

		INSERT INTO pss.tran (
			tran_id,
			tran_start_ts,
			tran_end_ts,
			tran_upload_ts,
			tran_state_cd,
			tran_device_tran_cd,
			pos_pta_id,
			tran_global_trans_cd,
			tran_device_result_type_cd,
			payment_subtype_key_id,
			payment_subtype_class,
			client_payment_type_cd,
			device_name
		) SELECT
			pn_tran_id,
			ld_tran_start_ts,
			ld_tran_start_ts,
			ld_current_ts,
			pv_tran_state_cd,
			pv_device_tran_cd,
			ln_pos_pta_id,
			lv_global_trans_cd,
			pv_tran_device_result_type_cd,
			pp.payment_subtype_key_id,
			ps.payment_subtype_class,
			ps.client_payment_type_cd,
			pv_device_name
		FROM pss.pos_pta pp
		JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
		WHERE pp.pos_pta_id = ln_pos_pta_id;
		IF INSTR(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('MASTER_ID_UPDATE_SALE_TYPES'), pc_sale_type_cd) > 0 AND TO_NUMBER_OR_NULL(pv_device_tran_cd) < DBADMIN.DATE_TO_MILLIS(SYSDATE + 365) / 1000 THEN
			UPDATE DEVICE.DEVICE_SETTING 
			   SET DEVICE_SETTING_VALUE = pv_device_tran_cd
			 WHERE DEVICE_ID = ln_device_id
			   AND DEVICE_SETTING_PARAMETER_CD = '60' -- Master Id
			   AND TO_NUMBER_OR_NULL(NVL(DEVICE_SETTING_VALUE, '0')) < TO_NUMBER_OR_NULL(pv_device_tran_cd);
		END IF;
	ELSE -- logic to determine pv_tran_state_cd
		SELECT c.MINOR_CURRENCY_FACTOR
		INTO ln_minor_currency_factor
		FROM PSS.POS_PTA PTA
		JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
		WHERE PTA.POS_PTA_ID = ln_pos_pta_id;
	
		IF lc_calc_tran_state_cd = 'Y' THEN
			IF pv_tran_device_result_type_cd IN (PKG_CONST.TRAN_DEV_RES__CANCELLED,
					PKG_CONST.TRAN_DEV_RES__AUTH_FAILURE,
					PKG_CONST.TRAN_DEV_RES__FAILURE,
					PKG_CONST.TRAN_DEV_RES__TIMEOUT) OR pn_sale_amount <= 0 THEN
				IF pv_tran_state_cd IN (
						PKG_CONST.TRAN_STATE__AUTH_SUCCESS,
						PKG_CONST.TRAN_STATE__AUTH_SUCCESS_COND,
						PKG_CONST.TRAN_STATE__BATCH_INTENDED,
						PKG_CONST.TRAN_STATE__PROCESSED_TRAN_INT) THEN
					IF lc_auth_hold_used = 'Y' OR pv_tran_state_cd = PKG_CONST.TRAN_STATE__PROCESSED_TRAN_INT THEN
						IF pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED THEN
							pv_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH_INTENDED;
						ELSE
							pv_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH;
						END IF;
					ELSE
						pv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED;
					END IF;
				ELSIF pv_tran_state_cd IN (
						PKG_CONST.TRAN_STATE__AUTH_EXPIRED,
						PKG_CONST.TRAN_STATE__AUTH_COND_EXPIRED) THEN
					pv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED; -- Reversal not available since auth is expired
				ELSIF pv_tran_state_cd IN (
						PKG_CONST.TRAN_STATE__AUTH_DECLINE,
						PKG_CONST.TRAN_STATE__AUTH_FAILURE) THEN
					pv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED; -- We already determined that that no reversal is needed
				ELSIF pv_tran_state_cd IN (
						PKG_CONST.TRAN_STATE__AUTH_REVERSED,
						PKG_CONST.TRAN_STATE__AUTH_PEND_RVRSL,
						PKG_CONST.TRAN_STATE__COMPLETE_ERROR,
						PKG_CONST.TRAN_STATE__PROCESSED_TRAN,
						PKG_CONST.TRAN_STATE__PROCESSING_TRAN,
						PKG_CONST.TRAN_STATE__INCOMPLETE,
						PKG_CONST.TRAN_STATE__INCOMPLETE_ERROR,
						PKG_CONST.TRAN_STATE__PROCESSING_STTLMT,
						PKG_CONST.TRAN_STATE__STLMT_INCOMPLETE,
						PKG_CONST.TRAN_STATE__STLMT_ERROR) THEN
				   -- don't change it
				   NULL;
				ELSE                
					pv_error_message := 'Bad tran state for a cancelled cashless sale, tran_id: ' || pn_tran_id || ', tran_state_cd: ' || pv_tran_state_cd;
					pv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED;
				END IF;
			ELSIF pv_tran_state_cd IN (
					PKG_CONST.TRAN_STATE__AUTH_EXPIRED,
					PKG_CONST.TRAN_STATE__AUTH_COND_EXPIRED)
				 OR (pv_tran_state_cd IN (
					PKG_CONST.TRAN_STATE__AUTH_SUCCESS,
					PKG_CONST.TRAN_STATE__AUTH_SUCCESS_COND)
					AND ld_original_tran_start_ts < ld_current_ts - 8) THEN
				pv_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE_ERROR;
			ELSIF pv_tran_state_cd IN (
					PKG_CONST.TRAN_STATE__AUTH_DECLINE,
					PKG_CONST.TRAN_STATE__AUTH_FAILURE,
					PKG_CONST.TRAN_STATE__AUTH_REVERSED,
					PKG_CONST.TRAN_STATE__AUTH_PEND_RVRSL,
					PKG_CONST.TRAN_STATE__INTENDED_ERROR) THEN
				pv_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE_ERROR;
				pv_error_message := 'Received a cashless sale for an unsuccessful auth, tran_id: ' || pn_tran_id || ', tran_state_cd: ' || pv_tran_state_cd;
			ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED
				AND pv_tran_device_result_type_cd IN (
					PKG_CONST.TRAN_DEV_RES__INCOMPLETE,
					PKG_CONST.TRAN_DEV_RES__SUCCESS,
					PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_PRNTR,
					PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_RCPT,
					PKG_CONST.TRAN_DEV_RES__SUCCESS_RCPT_ERR) THEN
				IF pv_tran_state_cd IN (PKG_CONST.TRAN_STATE__AUTH_SUCCESS, PKG_CONST.TRAN_STATE__AUTH_SUCCESS_COND) THEN
					-- normal case
					pv_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH_INTENDED;
					-- insert sale record
				ELSE
					-- sale actual uploaded
					-- don't change tran_state_cd
					-- don't update sale record
					pn_result_cd := PKG_CONST.RESULT__SUCCESS;
					pv_error_message := 'Actual uploaded before intended';
					RETURN;
				END IF;
			-- we must let POSM processed cancelled sales too to do auth reversal
			ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__ACTUAL
				AND pv_tran_state_cd IN (
					PKG_CONST.TRAN_STATE__AUTH_SUCCESS,
					PKG_CONST.TRAN_STATE__AUTH_SUCCESS_COND,
					PKG_CONST.TRAN_STATE__PROCESSING_BATCH,
					PKG_CONST.TRAN_STATE__PRCSNG_BATCH_INTD,
					PKG_CONST.TRAN_STATE__PRCSNG_BATCH_LOCAL,
					PKG_CONST.TRAN_STATE__PROCESSED_TRAN_INT,
					PKG_CONST.TRAN_STATE__BATCH_INTENDED)
				AND pv_tran_device_result_type_cd IN (
					PKG_CONST.TRAN_DEV_RES__AUTH_FAILURE,
					PKG_CONST.TRAN_DEV_RES__CANCELLED,
					PKG_CONST.TRAN_DEV_RES__FAILURE,
					PKG_CONST.TRAN_DEV_RES__INCOMPLETE,
					PKG_CONST.TRAN_DEV_RES__SUCCESS,
					PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_PRNTR,
					PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_RCPT,
					PKG_CONST.TRAN_DEV_RES__SUCCESS_RCPT_ERR,
					PKG_CONST.TRAN_DEV_RES__TIMEOUT) THEN
				IF pv_tran_device_result_type_cd IN (
					PKG_CONST.TRAN_DEV_RES__SUCCESS,
					PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_PRNTR,
					PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_RCPT,
					PKG_CONST.TRAN_DEV_RES__SUCCESS_RCPT_ERR) THEN

					SELECT NVL(MAX(auth_amt_approved), 0)
					INTO ln_auth_amt_approved
					FROM PSS.AUTH
					WHERE TRAN_ID = pn_tran_id AND auth_type_cd = 'N' AND auth_state_id IN (2, 5);
					
					pv_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH;
					IF pn_sale_amount > 0 AND ln_auth_amt_approved > 0 AND pn_sale_amount / ln_minor_currency_factor > ln_auth_amt_approved THEN
						SELECT GREATEST(ln_auth_amt_approved, NVL(NVL(MAX(DBADMIN.TO_NUMBER_OR_NULL(DS_MAA.DEVICE_SETTING_VALUE) / 100), MAX(DBADMIN.TO_NUMBER_OR_NULL(DS_AUTH_AMT.DEVICE_SETTING_VALUE) / DECODE(D.DEVICE_TYPE_ID, 13, 100, 1))), 0))
						INTO ln_auth_amt_allowed
						FROM DEVICE.DEVICE D
						LEFT OUTER JOIN DEVICE.DEVICE_SETTING DS_MAA ON D.DEVICE_ID = DS_MAA.DEVICE_ID AND DS_MAA.DEVICE_SETTING_PARAMETER_CD = 'MAX_AUTH_AMOUNT'
						LEFT OUTER JOIN DEVICE.DEVICE_SETTING DS_AUTH_AMT ON D.DEVICE_ID = DS_AUTH_AMT.DEVICE_ID
							AND DS_AUTH_AMT.DEVICE_SETTING_PARAMETER_CD = DECODE(D.DEVICE_TYPE_ID, 13, '1200', 1, '195', 11, 'AUTHORIZATION_AMOUNT', 0, '195')
						WHERE D.DEVICE_ID = ln_device_id;
					
						ln_sale_over_auth_amt_percent := NVL(DBADMIN.TO_NUMBER_OR_NULL(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('ALLOWED_SALE_AMT_OVER_AUTH_AMT_PERCENT')), 100);
						IF pn_sale_amount / ln_minor_currency_factor > ln_auth_amt_allowed + ln_auth_amt_allowed * ln_sale_over_auth_amt_percent / 100 THEN
							pv_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE_ERROR;
							lv_error := 'Error: Sale amount ' || TO_CHAR(pn_sale_amount / ln_minor_currency_factor, 'FM9,999,999,990.00') || ' exceeds allowed auth amount ' || TO_CHAR(ln_auth_amt_allowed, 'FM9,999,999,990.00') || ' by more than ' || ln_sale_over_auth_amt_percent || '%';
							UPDATE PSS.TRAN
							SET TRAN_INFO = SUBSTR(TRAN_INFO || DECODE(TRAN_INFO, NULL, '', ', ') || lv_error, 1, 1000)
							WHERE TRAN_ID = pn_tran_id AND (TRAN_INFO IS NULL OR INSTR(TRAN_INFO, lv_error) = 0);
                            
							UPDATE DEVICE.DEVICE
							SET DEVICE_ACTIVE_YN_FLAG = 'N'
							WHERE DEVICE_NAME = pv_device_name AND DEVICE_ACTIVE_YN_FLAG = 'Y';
                            
							lv_email_from_address := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EMAIL_FROM_ADDRESS');
							lv_email_to_address := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EMAIL_TO_ADDRESS_CUSTOMER_SERVICE');
							INSERT INTO ENGINE.OB_EMAIL_QUEUE(OB_EMAIL_FROM_EMAIL_ADDR, OB_EMAIL_FROM_NAME, OB_EMAIL_TO_EMAIL_ADDR, OB_EMAIL_TO_NAME, OB_EMAIL_SUBJECT, OB_EMAIL_MSG)
							VALUES(lv_email_from_address, lv_email_from_address, lv_email_to_address, lv_email_to_address, 'Invalid sale amount', lv_error || ', device: ' || pv_device_name || ', transaction ID: ' || pn_tran_id || '. Device has been disabled.');
						END IF;
					END IF;
				ELSE
					pv_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH;
				END IF;
			ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__ACTUAL
				AND pv_tran_state_cd = PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND THEN
				pv_tran_state_cd := PKG_CONST.TRAN_STATE__SALE_NO_AUTH;
			ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__ACTUAL
				AND pv_tran_state_cd = PKG_CONST.TRAN_STATE__PROCESSING_TRAN THEN
				NULL;-- don't change tran_state_cd
			ELSIF pv_tran_state_cd != PKG_CONST.TRAN_STATE__SALE_NO_AUTH THEN
				 pv_error_message := 'Unusual tran state for a cashless sale, tran_id: ' || pn_tran_id || ', tran_state_cd: ' || pv_tran_state_cd;
			END IF;
		END IF;
        
		UPDATE pss.tran
		SET tran_state_cd = DECODE(TRAN_STATE_CD, lv_orig_tran_state_cd, pv_tran_state_cd, TRAN_STATE_CD), -- it might have changed if POSMLayer is processing it
			tran_end_ts = tran_start_ts,
			tran_upload_ts = ld_current_ts,
			tran_device_result_type_cd = pv_tran_device_result_type_cd
		WHERE tran_id = pn_tran_id
		RETURNING client_payment_type_cd INTO lc_client_payment_type_cd;

		SELECT /*+ INDEX(tli IF1_TRAN_LINE_ITEM) */ COUNT(1)
		INTO ln_count
		FROM pss.tran_line_item tli
		WHERE tran_id = pn_tran_id
			AND tran_line_item_batch_type_cd = pc_tran_batch_type_cd;
		
		IF ln_count > 0 THEN
			DELETE /*+ INDEX(tli IF1_TRAN_LINE_ITEM) */ FROM pss.tran_line_item tli
			WHERE tran_id = pn_tran_id
				AND tran_line_item_batch_type_cd = pc_tran_batch_type_cd;
		END IF;
	END IF;    

	UPDATE pss.sale
	SET device_batch_id = pn_device_batch_id,
		sale_type_cd = pc_sale_type_cd,
		sale_start_utc_ts = lt_sale_start_utc_ts,
		sale_end_utc_ts = lt_sale_start_utc_ts,
		sale_utc_offset_min = pn_sale_utc_offset_min,
		sale_result_id = pn_sale_result_id,
		sale_amount = pn_sale_amount / ln_minor_currency_factor,
		receipt_result_cd = pc_receipt_result_cd,
		hash_type_cd = pv_hash_type_cd,
		tran_line_item_hash = pv_tran_line_item_hash,
		sale_global_session_cd = pv_global_session_cd,
		imported = CASE WHEN pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED OR pv_tran_state_cd IN('F', 'G', 'Z') THEN '?' WHEN pn_sale_result_id = 0 THEN 'N' ELSE '-' END,
		VOID_ALLOWED = NVL(pc_void_allowed, 'N')
	WHERE tran_id = pn_tran_id;

	IF SQL%NOTFOUND THEN
		INSERT INTO pss.sale (
			tran_id,
			device_batch_id,
			sale_type_cd,
			sale_start_utc_ts,
			sale_end_utc_ts,
			sale_utc_offset_min,
			sale_result_id,
			sale_amount,
			receipt_result_cd,
			hash_type_cd,
			tran_line_item_hash,
			sale_global_session_cd,
			imported,
			VOID_ALLOWED
		) VALUES (
			pn_tran_id,
			pn_device_batch_id,
			pc_sale_type_cd,
			lt_sale_start_utc_ts,
			lt_sale_start_utc_ts,
			pn_sale_utc_offset_min,
			pn_sale_result_id,
			pn_sale_amount / ln_minor_currency_factor,
			pc_receipt_result_cd,
			pv_hash_type_cd,
			pv_tran_line_item_hash,
			pv_global_session_cd,
			CASE WHEN pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED OR pv_tran_state_cd IN('F', 'G', 'Z') THEN '?' WHEN pn_sale_result_id = 0 THEN 'N' ELSE '-' END,
			NVL(pc_void_allowed, 'N')
		);
		
		IF lc_client_payment_type_cd IN (PKG_CONST.CLNT_PMNT_TYPE__ISIS_CREDIT, PKG_CONST.CLNT_PMNT_TYPE__ISIS_SPECIAL)
			AND ln_consumer_acct_id > 0 AND pc_sale_type_cd = PKG_CONST.SALE_TYPE__ACTUAL THEN
			PROCESS_ISIS_TRAN(pn_tran_id);
		END IF;
	END IF;
	
	IF pn_sale_result_id != 0 AND ln_consumer_acct_id IS NOT NULL AND lc_auth_hold_used = 'N' THEN
		UPDATE pss.last_device_action
		SET device_action_utc_ts = device_action_utc_ts - INTERVAL '1' YEAR
		WHERE device_name = pv_device_name
			AND consumer_acct_id = ln_consumer_acct_id;
	END IF;
END;

	PROCEDURE ADD_REPLENISH_BONUSES(
		pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
		pn_apply_to_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
		pn_amount PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
		pd_auth_ts PSS.AUTH.AUTH_TS%TYPE)
	IS
		ln_used_replenish_balance PSS.CONSUMER_ACCT.TOWARD_REPLENISH_BONUS_BALANCE%TYPE;
        ln_bonus_amount PSS.SALE.SALE_AMOUNT%TYPE;
        ln_bonus_percent REPORT.CAMPAIGN.DISCOUNT_PERCENT%TYPE;
        ln_bonus_threshhold REPORT.CAMPAIGN.THRESHOLD_AMOUNT%TYPE;
        ln_bonus_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE;
	BEGIN
        SELECT MAX(DISCOUNT_PERCENT), MAX(THRESHOLD_AMOUNT), MAX(CAMPAIGN_ID)
          INTO ln_bonus_percent, ln_bonus_threshhold, ln_bonus_campaign_id
          FROM (SELECT C.DISCOUNT_PERCENT, C.THRESHOLD_AMOUNT, C.CAMPAIGN_ID
          FROM REPORT.CAMPAIGN C
          JOIN PSS.CAMPAIGN_CONSUMER_ACCT CCA on C.CAMPAIGN_ID = CCA.CAMPAIGN_ID
         WHERE CCA.CONSUMER_ACCT_ID = pn_apply_to_consumer_acct_id
           AND pd_auth_ts BETWEEN NVL(C.START_DATE, MIN_DATE) AND NVL(C.END_DATE, MAX_DATE)
           AND C.CAMPAIGN_TYPE_ID = 2 /* Replenish reward - Bonus */
           AND C.DISCOUNT_PERCENT > 0
           AND C.DISCOUNT_PERCENT < 1
           AND (TRIM(C.RECUR_SCHEDULE) IS NULL OR REPORT.MATCH_CAMPAIGN_RECUR_SCHEDULE(C.RECUR_SCHEDULE, pd_auth_ts) = 'Y')
         ORDER BY C.DISCOUNT_PERCENT DESC, C.CAMPAIGN_ID DESC)
         WHERE ROWNUM = 1;
        IF ln_bonus_campaign_id IS NOT NULL AND ln_bonus_percent > 0 THEN
            DECLARE
                ln_bonus_balance PSS.CONSUMER_ACCT.TOWARD_CASH_BACK_BALANCE%TYPE;
                lv_currency_cd PSS.CONSUMER_ACCT.CURRENCY_CD%TYPE;
                ln_corp_customer_id PSS.CONSUMER_ACCT.CORP_CUSTOMER_ID%TYPE;
                lv_consumer_acct_cd PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD%TYPE;
                lv_consumer_acct_identifier PSS.CONSUMER_ACCT.CONSUMER_ACCT_IDENTIFIER%TYPE;
                lv_bonus_device_name DEVICE.DEVICE_NAME%TYPE;
                ln_bonus_next_master_id NUMBER;
                ln_result_cd NUMBER;
                lv_error_message VARCHAR2(4000);
                ln_bonus_tran_id PSS.TRAN.TRAN_ID%TYPE;
                lv_bonus_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
                ln_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE;
                lc_currency_symbol CORP.CURRENCY.CURRENCY_SYMBOL%TYPE;
                ln_host_id HOST.HOST_ID%TYPE;
                ld_bonus_ts DATE;
                ld_bonus_time NUMBER;
                lv_desc PSS.TRAN_LINE_ITEM.TRAN_LINE_ITEM_DESC%TYPE;
                ln_doc_id CORP.DOC.DOC_ID%TYPE;
                ln_ledger_id CORP.LEDGER.LEDGER_ID%TYPE; 
                ln_consumer_acct_sub_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_SUB_TYPE_ID%TYPE;
            BEGIN
                UPDATE PSS.CONSUMER_ACCT
                   SET TOWARD_REPLENISH_BONUS_BALANCE = NVL(TOWARD_REPLENISH_BONUS_BALANCE, 0) + pn_amount
                 WHERE CONSUMER_ACCT_ID = pn_apply_to_consumer_acct_id
                 RETURNING TOWARD_REPLENISH_BONUS_BALANCE, CURRENCY_CD, CORP_CUSTOMER_ID, CONSUMER_ACCT_CD, CONSUMER_ACCT_IDENTIFIER, CONSUMER_ACCT_SUB_TYPE_ID
                  INTO ln_bonus_balance, lv_currency_cd, ln_corp_customer_id, lv_consumer_acct_cd, lv_consumer_acct_identifier, ln_consumer_acct_sub_type_id;
                IF ln_bonus_balance >= ln_bonus_threshhold THEN 
                    ln_used_replenish_balance := TRUNC(ln_bonus_balance / ln_bonus_threshhold) * ln_bonus_threshhold;
                    ln_bonus_amount := ROUND(ln_used_replenish_balance * ln_bonus_percent, 2);
                    UPDATE PSS.CONSUMER_ACCT
                       SET TOWARD_REPLENISH_BONUS_BALANCE = TOWARD_REPLENISH_BONUS_BALANCE - ln_used_replenish_balance,
                           REPLENISH_BONUS_TOTAL = NVL(REPLENISH_BONUS_TOTAL, 0) + ln_bonus_amount,
                           CONSUMER_ACCT_BALANCE = CONSUMER_ACCT_BALANCE + ln_bonus_amount,
                           CONSUMER_ACCT_PROMO_BALANCE = CONSUMER_ACCT_PROMO_BALANCE + ln_bonus_amount,
                           CONSUMER_ACCT_PROMO_TOTAL = CONSUMER_ACCT_PROMO_TOTAL + ln_bonus_amount
                     WHERE CONSUMER_ACCT_ID = pn_apply_to_consumer_acct_id;                         
                    -- add trans to virtual terminal
                    SELECT C.MINOR_CURRENCY_FACTOR, NVL(CC.CURRENCY_SYMBOL, '$'), DBADMIN.TIMESTAMP_TO_MILLIS(SYS_EXTRACT_UTC(SYSTIMESTAMP)), SYSDATE
                      INTO ln_minor_currency_factor, lc_currency_symbol, ld_bonus_time, ld_bonus_ts
                      FROM PSS.CURRENCY C
                      LEFT OUTER JOIN CORP.CURRENCY CC ON C.CURRENCY_CD = CC.CURRENCY_CODE
                     WHERE C.CURRENCY_CD = lv_currency_cd;
                     
                    PKG_DEVICE_CONFIGURATION.NEXT_MASTER_ID_BY_SERIAL('V1-' || CASE WHEN ln_consumer_acct_sub_type_id = 2 THEN ln_corp_customer_id ELSE 1 END || '-' || lv_currency_cd, lv_bonus_device_name, ln_bonus_next_master_id);
                    SP_CREATE_SALE('A', lv_bonus_device_name, ln_bonus_next_master_id,  0, 'C', ld_bonus_time, 
                        DBADMIN.PKG_UTL.TIME_ZONE_DIFF_MIN(PKG_CONST.DB_TIME_ZONE, PKG_CONST.GMT_TIME_ZONE), 'S', 0, ln_bonus_amount * ln_minor_currency_factor, 'U', 'A', 'SHA1', 
                        DBADMIN.HASH_CARD('Replenish Bonus on ' || pn_apply_to_consumer_acct_id || ' of ' || ln_bonus_amount),
                        NULL, ln_result_cd, lv_error_message, ln_bonus_tran_id, lv_bonus_tran_state_cd);
                    IF ln_result_cd != PKG_CONST.RESULT__SUCCESS THEN
                        RAISE_APPLICATION_ERROR(-20118, 'Could not create replenish bonus transaction: ' || lv_error_message);
                    END IF;
                    SELECT HOST_ID, 'Replenish Bonus for ' || lc_currency_symbol || TO_NUMBER(ln_used_replenish_balance, 'FM9,999,999.00') || ' in replenishments'
                      INTO ln_host_id, lv_desc
                      FROM (SELECT H.HOST_ID
                              FROM DEVICE.HOST H
                              JOIN DEVICE.DEVICE D ON H.DEVICE_ID = D.DEVICE_ID
                             WHERE D.DEVICE_ACTIVE_YN_FLAG = 'Y'
                               AND H.HOST_PORT_NUM IN(0,1)
                               AND D.DEVICE_NAME = lv_bonus_device_name
                               AND H.HOST_ACTIVE_YN_FLAG = 'Y'
                             ORDER BY H.HOST_PORT_NUM DESC, H.HOST_ID DESC) 
                     WHERE ROWNUM = 1;
                    INSERT INTO PSS.TRAN_LINE_ITEM(
                        TRAN_ID,
                        TRAN_LINE_ITEM_AMOUNT,
                        TRAN_LINE_ITEM_POSITION_CD,
                        TRAN_LINE_ITEM_TAX,
                        TRAN_LINE_ITEM_TYPE_ID,
                        TRAN_LINE_ITEM_QUANTITY,
                        TRAN_LINE_ITEM_DESC,
                        HOST_ID,
                        TRAN_LINE_ITEM_BATCH_TYPE_CD,
                        TRAN_LINE_ITEM_TS,
                        SALE_RESULT_ID,
                        APPLY_TO_CONSUMER_ACCT_ID,
                        CAMPAIGN_ID)
                    SELECT
                        ln_bonus_tran_id,
                        ln_used_replenish_balance,
                        NULL,
                        NULL,
                        555,
                        ln_bonus_percent,
                        lv_desc,
                        ln_host_id,
                        'A',
                        ld_bonus_ts,
                        0,
                        pn_apply_to_consumer_acct_id,
                        ln_bonus_campaign_id
                    FROM DUAL;
                    UPDATE PSS.TRAN
                       SET PARENT_TRAN_ID = pn_tran_id
                     WHERE TRAN_ID = ln_bonus_tran_id;
                    IF ln_consumer_acct_sub_type_id = 1 THEN
                        -- add adjustment to ledger
                        CORP.PAYMENTS_PKG.ADJUSTMENT_INS(ln_corp_customer_id, 'Replenish Bonus Processing', lv_currency_cd, lv_desc || ', card # ' || lv_consumer_acct_cd || ', card ID ' || lv_consumer_acct_identifier,
                            -ln_bonus_amount, ln_doc_id, ln_ledger_id);
                    END IF;
                END IF; 
            END;
        END IF;  
    END;
    
    PROCEDURE FINISH_REPLENISH_SETUP(
		pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
		pn_apply_to_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
		pv_card_key PSS.AUTH.CARD_KEY%TYPE,
		pd_auth_ts PSS.AUTH.AUTH_TS%TYPE,
		pv_masked_card_number PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_CARD_MASKED%TYPE,
		pn_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE)
	IS
        ln_replenish_id PSS.CONSUMER_ACCT_REPLENISH.CONSUMER_ACCT_REPLENISH_ID%TYPE;
		ln_capr_row_id ROWID;   
    BEGIN
		SELECT MAX(CAR.CONSUMER_ACCT_REPLENISH_ID), MAX(CAPR.ROWID)
		  INTO ln_replenish_id, ln_capr_row_id
		  FROM PSS.CONSUMER_ACCT_REPLENISH CAR 
		  JOIN PSS.CONSUMER_ACCT_PEND_REPLENISH CAPR ON CAR.CONSUMER_ACCT_REPLENISH_ID = CAPR.CONSUMER_ACCT_REPLENISH_ID
		  JOIN PSS.TRAN X ON CAPR.DEVICE_NAME = X.DEVICE_NAME AND CAPR.DEVICE_TRAN_CD = X.TRAN_DEVICE_TRAN_CD
		 WHERE X.TRAN_ID = pn_tran_id
		   AND CAR.CONSUMER_ACCT_ID = pn_apply_to_consumer_acct_id;
		IF ln_replenish_id IS NOT NULL THEN
			UPDATE PSS.CONSUMER_ACCT_REPLENISH CAR
			   SET REPLENISH_CARD_KEY = CASE WHEN pv_card_key IS NOT NULL AND pd_auth_ts > NVL(LAST_REPLENISH_TRAN_TS, MIN_DATE) THEN pv_card_key ELSE REPLENISH_CARD_KEY END,
				   REPLENISH_CARD_MASKED = CASE WHEN pv_card_key IS NOT NULL AND pd_auth_ts > NVL(LAST_REPLENISH_TRAN_TS, MIN_DATE) THEN DBADMIN.MASK_CREDIT_CARD(pv_masked_card_number) ELSE REPLENISH_CARD_MASKED END,                   
				   REPLENISH_POS_PTA_ID = CASE WHEN pv_card_key IS NOT NULL AND pd_auth_ts > NVL(LAST_REPLENISH_TRAN_TS, MIN_DATE) THEN pn_pos_pta_id ELSE REPLENISH_POS_PTA_ID END,
				   LAST_REPLENISH_TRAN_TS = CASE WHEN pv_card_key IS NOT NULL AND pd_auth_ts > NVL(LAST_REPLENISH_TRAN_TS, MIN_DATE) THEN pd_auth_ts ELSE LAST_REPLENISH_TRAN_TS END,
				   LAST_REPLENISH_TRAN_ID = CASE WHEN pv_card_key IS NOT NULL AND pd_auth_ts > NVL(LAST_REPLENISH_TRAN_TS, MIN_DATE) THEN pn_tran_id ELSE LAST_REPLENISH_TRAN_ID END
			 WHERE CONSUMER_ACCT_ID = pn_apply_to_consumer_acct_id
			   AND CONSUMER_ACCT_REPLENISH_ID = ln_replenish_id;
			DELETE 
			  FROM PSS.CONSUMER_ACCT_PEND_REPLENISH 
			 WHERE ROWID = ln_capr_row_id
			   AND CONSUMER_ACCT_REPLENISH_ID = ln_replenish_id;
		END IF;
	END;

    PROCEDURE REPLENISH_CONSUMER_ACCT(
		pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
		pv_tli_desc PSS.TRAN_LINE_ITEM.TRAN_LINE_ITEM_DESC%TYPE,
		pn_amount PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
		pn_tli_type_id PSS.TRAN_LINE_ITEM.TRAN_LINE_ITEM_TYPE_ID%TYPE,
		pv_card_key PSS.AUTH.CARD_KEY%TYPE,
		pd_auth_ts PSS.AUTH.AUTH_TS%TYPE,
		pv_masked_card_number PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_CARD_MASKED%TYPE,
		pn_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE,
		pn_apply_to_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE)
	IS
        ln_consumer_acct_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_TYPE_ID%TYPE;
    BEGIN
        UPDATE PSS.CONSUMER_ACCT
		   SET CONSUMER_ACCT_BALANCE = CONSUMER_ACCT_BALANCE + pn_amount,
			   CONSUMER_ACCT_REPLEN_BALANCE = CONSUMER_ACCT_REPLEN_BALANCE + pn_amount,
			   CONSUMER_ACCT_REPLENISH_TOTAL = CONSUMER_ACCT_REPLENISH_TOTAL + pn_amount
		 WHERE CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y'
		   AND CONSUMER_ACCT_TYPE_ID = 3
		   AND CONSUMER_ACCT_CD_HASH = HEXTORAW(pv_tli_desc)
		  RETURNING CONSUMER_ACCT_ID, CONSUMER_ACCT_TYPE_ID 
		  INTO pn_apply_to_consumer_acct_id, ln_consumer_acct_type_id;
        IF ln_consumer_acct_type_id IN(3) THEN			
            ADD_REPLENISH_BONUSES(
                pn_tran_id,
                pn_apply_to_consumer_acct_id,
                pn_amount,
                pd_auth_ts);
        END IF;
        FINISH_REPLENISH_SETUP(
            pn_tran_id,
            pn_apply_to_consumer_acct_id,
            pv_card_key,
            pd_auth_ts,
            pv_masked_card_number,
            pn_pos_pta_id);
    END;
    
PROCEDURE sp_create_tran_line_item
(
	pn_tran_id IN pss.tran_line_item.tran_id%TYPE,
	pn_host_port_num IN host.host_port_num%TYPE,
	pn_tli_type_id IN pss.tran_line_item.tran_line_item_type_id%TYPE,
	pn_tli_quantity IN pss.tran_line_item.tran_line_item_quantity%TYPE,
	pn_tli_amount IN NUMBER,
	pn_tli_tax IN NUMBER,
	pv_tli_desc IN pss.tran_line_item.tran_line_item_desc%TYPE,
	pn_tli_utc_ts_ms IN NUMBER,
	pn_tli_utc_offset_min IN NUMBER,
	pv_tli_position_cd IN pss.tran_line_item.tran_line_item_position_cd%TYPE,
	pc_tran_batch_type_cd IN pss.tran_line_item.tran_line_item_batch_type_cd%TYPE,
	pn_sale_result_id pss.tran_line_item.sale_result_id%TYPE DEFAULT NULL,
	pn_host_position_num host.host_position_num%TYPE DEFAULT 0,
	pn_sale_amount pss.sale.sale_amount%TYPE DEFAULT 0
)
IS
	ln_host_id pss.tran_line_item.host_id%TYPE;
	ln_device_id host.device_id%TYPE;
	ln_new_host_count NUMBER;
	ln_result_cd NUMBER;
	lv_error_message VARCHAR2(255);
	ln_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE;
	ln_tran_line_item_id pss.tran_line_item.tran_line_item_id%TYPE;
	ln_tli_desc pss.tran_line_item.tran_line_item_desc%TYPE;
	ln_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE;
	lc_tli_type_group_cd PSS.TRAN_LINE_ITEM_TYPE.TRAN_LINE_ITEM_TYPE_GROUP_CD%TYPE;
	lv_device_name DEVICE.DEVICE_NAME%TYPE;
	ln_convenience_fee_amount PSS.SALE.SALE_AMOUNT%TYPE;
	lv_email_from_address engine.app_setting.app_setting_value%TYPE;
	lv_email_to_address engine.app_setting.app_setting_value%TYPE;
	lv_error pss.tran.tran_info%TYPE;
	ln_tli_type_id PSS.TRAN_LINE_ITEM.TRAN_LINE_ITEM_TYPE_ID%TYPE := pn_tli_type_id;
	ln_apply_to_consumer_acct_id PSS.TRAN_LINE_ITEM.APPLY_TO_CONSUMER_ACCT_ID%TYPE;
	lv_card_key PSS.AUTH.CARD_KEY%TYPE;
	ld_auth_ts PSS.AUTH.AUTH_TS%TYPE;
	lv_masked_card_number PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_CARD_MASKED%TYPE;
	ln_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE;
BEGIN
	SELECT POS.DEVICE_ID, c.MINOR_CURRENCY_FACTOR, D.DEVICE_TYPE_ID, D.DEVICE_NAME
	  INTO ln_device_id, ln_minor_currency_factor, ln_device_type_id, lv_device_name
	  FROM PSS.POS POS
	  JOIN DEVICE.DEVICE D ON POS.DEVICE_ID = D.DEVICE_ID
	  JOIN PSS.POS_PTA PTA ON POS.POS_ID = PTA.POS_ID
	  JOIN PSS.TRAN X ON PTA.POS_PTA_ID = X.POS_PTA_ID
	  JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
	WHERE X.TRAN_ID = pn_tran_id;

	ln_host_id := sf_find_host_id(ln_device_id, pn_host_port_num, pn_host_position_num);
	IF ln_host_id IS NULL THEN
		-- create default hosts
		pkg_device_configuration.sp_create_default_hosts(ln_device_id, ln_new_host_count, ln_result_cd, lv_error_message);
		IF ln_result_cd != PKG_CONST.RESULT__SUCCESS THEN
			RETURN;
		END IF;
		ln_host_id := sf_find_host_id(ln_device_id, pn_host_port_num, pn_host_position_num);
	END IF;

	SELECT PSS.SEQ_TLI_ID.NEXTVAL
		INTO ln_tran_line_item_id
		FROM DUAL;

	-- For Kiosk type, use tran_line_item_type to find description
	IF ln_device_type_id = 11 THEN
		SELECT tran_line_item_type_desc || ' ' || SUBSTR(TRIM(pv_tli_desc), 1, 3999 - LENGTH(tran_line_item_type_desc))
		  INTO ln_tli_desc
		  FROM pss.tran_line_item_type
		 WHERE tran_line_item_type_id = ln_tli_type_id;
	ELSIF ln_device_type_id = 5 THEN -- eSuds
		SELECT TLIT.TRAN_LINE_ITEM_TYPE_DESC || ', ' || CASE WHEN DTHT.DEVICE_TYPE_HOST_TYPE_CD IN('S', 'U', 'G', 'H', 'I', 'J') THEN DECODE(H.HOST_POSITION_NUM, 0, 'Bottom ', 1, 'Top ') END
				|| GT.HOST_GROUP_TYPE_NAME || ' ' || H.HOST_LABEL_CD
		  INTO ln_tli_desc
		  FROM PSS.TRAN_LINE_ITEM_TYPE tlit
		 CROSS JOIN DEVICE.HOST H
		  JOIN DEVICE.DEVICE_TYPE_HOST_TYPE dtht ON DTHT.HOST_TYPE_ID = H.HOST_TYPE_ID AND DTHT.DEVICE_TYPE_ID = 5
		  LEFT OUTER JOIN (DEVICE.HOST_TYPE_HOST_GROUP_TYPE HTGT
		  JOIN DEVICE.HOST_GROUP_TYPE GT ON HTGT.HOST_GROUP_TYPE_ID = GT.HOST_GROUP_TYPE_ID)
			ON H.HOST_TYPE_ID = HTGT.HOST_TYPE_ID
		 WHERE tlit.TRAN_LINE_ITEM_TYPE_ID = ln_tli_type_id
		   AND H.HOST_ID = ln_host_id;
	ELSE
		ln_tli_desc := pv_tli_desc;
	END IF;

	INSERT INTO PSS.TRAN_LINE_ITEM (
		TRAN_LINE_ITEM_ID,
		TRAN_ID,
		TRAN_LINE_ITEM_AMOUNT,
		TRAN_LINE_ITEM_POSITION_CD,
		TRAN_LINE_ITEM_TAX,
		TRAN_LINE_ITEM_TYPE_ID,
		TRAN_LINE_ITEM_QUANTITY,
		TRAN_LINE_ITEM_DESC,
		HOST_ID,
		TRAN_LINE_ITEM_BATCH_TYPE_CD,
		TRAN_LINE_ITEM_TS,
		SALE_RESULT_ID,
		APPLY_TO_CONSUMER_ACCT_ID)
	SELECT
		ln_tran_line_item_id,
		pn_tran_id,
		pn_tli_amount * CASE tran_line_item_type_sign_pn
			WHEN 'N' THEN -1
			ELSE 1
		END / ln_minor_currency_factor,
		pv_tli_position_cd,
		pn_tli_tax * CASE tran_line_item_type_sign_pn
			WHEN 'N' THEN -1
			ELSE 1
		END / ln_minor_currency_factor,
		ln_tli_type_id,
		pn_tli_quantity,
		ln_tli_desc,
		ln_host_id,
		pc_tran_batch_type_cd,
		CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_tli_utc_ts_ms + pn_tli_utc_offset_min * 60 * 1000) AS DATE),
		pn_sale_result_id,
		ln_apply_to_consumer_acct_id
	FROM tran_line_item_type
	WHERE tran_line_item_type_id = ln_tli_type_id;

	-- For all device actual batch type only
	IF ln_host_id IS NOT NULL AND pc_tran_batch_type_cd = PKG_CONST.TRAN_BATCH_TYPE__ACTUAL THEN
		SELECT tran_line_item_type_group_cd
		INTO lc_tli_type_group_cd
		FROM pss.tran_line_item_type
		WHERE tran_line_item_type_id = ln_tli_type_id;
	
		IF lc_tli_type_group_cd IN ('P', 'S') THEN
			UPDATE PSS.TRAN_LINE_ITEM_RECENT
			SET tran_line_item_id = ln_tran_line_item_id,
				fkp_tran_id = pn_tran_id
			WHERE host_id = ln_host_id
				AND tran_line_item_type_id = ln_tli_type_id;
				
			IF SQL%NOTFOUND THEN
				BEGIN
					INSERT INTO PSS.TRAN_LINE_ITEM_RECENT (
						TRAN_LINE_ITEM_ID,
						HOST_ID,
						FKP_TRAN_ID,
						TRAN_LINE_ITEM_TYPE_ID
					) VALUES (
						ln_tran_line_item_id,
						ln_host_id,
						pn_tran_id,
						ln_tli_type_id
					);
				EXCEPTION
					WHEN DUP_VAL_ON_INDEX THEN
						UPDATE PSS.TRAN_LINE_ITEM_RECENT
						SET tran_line_item_id = ln_tran_line_item_id,
							fkp_tran_id = pn_tran_id
						WHERE host_id = ln_host_id
							AND tran_line_item_type_id = ln_tli_type_id;
				END;
			END IF;
		END IF;
	END IF;
	
	IF ln_tli_type_id = PKG_CONST.TLI__CONVENIENCE_FEE AND pc_tran_batch_type_cd = PKG_CONST.TRAN_BATCH_TYPE__ACTUAL AND pn_sale_amount > 0 THEN
		ln_convenience_fee_amount := (NVL(pn_tli_amount, 0) + NVL(pn_tli_tax, 0)) * NVL(pn_tli_quantity, 0);
		IF ln_convenience_fee_amount > pn_sale_amount - ln_convenience_fee_amount 
			AND ln_convenience_fee_amount > NVL(DBADMIN.TO_NUMBER_OR_NULL(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('MIN_INVALID_CONVENIENCE_FEE_AMT_PENNIES')), 100) THEN
			UPDATE PSS.TRAN
			SET tran_state_cd = PKG_CONST.TRAN_STATE__COMPLETE_ERROR
			WHERE TRAN_ID = pn_tran_id;

			lv_error := 'Error: Two-Tier Pricing amount ' || TO_CHAR(ln_convenience_fee_amount / ln_minor_currency_factor, 'FM9,999,999,990.00') || ' exceeds sale amount without Two-Tier Pricing ' || TO_CHAR((pn_sale_amount - ln_convenience_fee_amount) / ln_minor_currency_factor, 'FM9,999,999,990.00');
			UPDATE PSS.TRAN
			SET TRAN_INFO = SUBSTR(TRAN_INFO || DECODE(TRAN_INFO, NULL, '', ', ') || lv_error, 1, 1000)
			WHERE TRAN_ID = pn_tran_id AND (TRAN_INFO IS NULL OR INSTR(TRAN_INFO, lv_error) = 0);
			
			UPDATE DEVICE.DEVICE
			SET DEVICE_ACTIVE_YN_FLAG = 'N'
			WHERE DEVICE_NAME = lv_device_name AND DEVICE_ACTIVE_YN_FLAG = 'Y';
			
			lv_email_from_address := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EMAIL_FROM_ADDRESS');
			lv_email_to_address := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EMAIL_TO_ADDRESS_CUSTOMER_SERVICE');
			INSERT INTO ENGINE.OB_EMAIL_QUEUE(OB_EMAIL_FROM_EMAIL_ADDR, OB_EMAIL_FROM_NAME, OB_EMAIL_TO_EMAIL_ADDR, OB_EMAIL_TO_NAME, OB_EMAIL_SUBJECT, OB_EMAIL_MSG)
			VALUES(lv_email_from_address, lv_email_from_address, lv_email_to_address, lv_email_to_address, 'Invalid Two-Tier Pricing amount', lv_error || ', device: ' || lv_device_name || ', transaction ID: ' || pn_tran_id || '. Device has been disabled.');		
		END IF;
	END IF;
END;

-- R33+ signature
PROCEDURE sp_finalize_sale
(
	pn_tran_id IN pss.tran.tran_id%TYPE,
	pv_global_session_cd IN VARCHAR2,
	pv_tran_device_result_type_cd IN pss.tran.tran_device_result_type_cd%TYPE,
	pn_sale_result_id IN pss.sale.sale_result_id%TYPE,
	pn_sale_amount IN NUMBER,
	pn_sale_tax IN NUMBER,
	pc_tran_batch_type_cd IN pss.tran_line_item.tran_line_item_batch_type_cd%TYPE,
	pn_sale_utc_ts_ms IN NUMBER,
	pn_sale_duration_sec IN NUMBER,
	pn_result_cd OUT NUMBER,
	pv_error_message OUT VARCHAR2,
	pc_sale_type_cd pss.sale.sale_type_cd%TYPE DEFAULT NULL,
	pc_tran_import_needed OUT VARCHAR2,
	pc_session_update_needed OUT VARCHAR2,
	pc_client_payment_type_cd OUT VARCHAR2,
	pn_minor_currency_factor OUT PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
	pn_tli_count OUT NUMBER
)
IS
/*
	Returned result codes:
		RESULT__SUCCESS
		RESULT__FAILURE
		RESULT__HOST_NOT_FOUND
*/
	ln_tli_total pss.tran_line_item.tran_line_item_amount%TYPE;
	ln_adj_amt pss.tran_line_item.tran_line_item_amount%TYPE;
	ln_adj_tli pss.tran_line_item.tran_line_item_type_id%TYPE := -1;
	ln_base_host_id pss.tran_line_item.host_id%TYPE;
	lc_tli_batch_type_cd pss.tran_line_item.tran_line_item_batch_type_cd%TYPE;
	ln_new_host_count NUMBER;
	ln_device_id DEVICE.DEVICE_ID%TYPE;
	ln_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE;
	lc_tran_state_cd pss.tran.tran_state_cd%TYPE;
	ln_discount_percent report.campaign.discount_percent%TYPE;
	ln_discount_amount NUMBER;
	ln_sale_amount NUMBER := NVL(pn_sale_amount, 0);
	ln_pos_pta_id pss.tran.pos_pta_id%TYPE;
	ld_tran_start_ts pss.tran.tran_start_ts%TYPE;
	lv_device_tran_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE;
	lv_device_name DEVICE.DEVICE_NAME%TYPE;
	ln_consumer_acct_id PSS.TRAN.CONSUMER_ACCT_ID%TYPE;
    ln_campaign_id PSS.TRAN_LINE_ITEM.CAMPAIGN_ID%TYPE;
BEGIN
	pn_result_cd := PKG_CONST.RESULT__FAILURE;
	pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;
	pc_session_update_needed := 'N';
	
	IF pn_sale_duration_sec > 0 AND pc_tran_batch_type_cd = PKG_CONST.TRAN_BATCH_TYPE__ACTUAL THEN
		UPDATE pss.tran
		SET tran_end_ts = tran_start_ts + pn_sale_duration_sec / 86400
		WHERE tran_id = pn_tran_id;

		UPDATE pss.sale
		SET sale_end_utc_ts = sale_start_utc_ts + pn_sale_duration_sec / 86400
		WHERE tran_id = pn_tran_id;
	END IF;    

	SELECT POS.DEVICE_ID, X.POS_PTA_ID, c.MINOR_CURRENCY_FACTOR, PST.CLIENT_PAYMENT_TYPE_CD, D.DEVICE_TYPE_ID, DECODE(S.IMPORTED, 'N', 'Y', NULL, 'Y', 'N'), X.TRAN_STATE_CD, 
		   X.TRAN_START_TS, X.TRAN_DEVICE_TRAN_CD, D.DEVICE_NAME, X.CONSUMER_ACCT_ID
	  INTO ln_device_id, ln_pos_pta_id, pn_minor_currency_factor, pc_client_payment_type_cd, ln_device_type_id, pc_tran_import_needed, lc_tran_state_cd, 
		   ld_tran_start_ts, lv_device_tran_cd, lv_device_name, ln_consumer_acct_id
	  FROM PSS.POS POS
	  JOIN DEVICE.DEVICE D ON POS.DEVICE_ID = D.DEVICE_ID
	  JOIN PSS.POS_PTA PTA ON POS.POS_ID = PTA.POS_ID
	  JOIN PSS.TRAN X ON PTA.POS_PTA_ID = X.POS_PTA_ID
	  JOIN PSS.SALE S ON X.TRAN_ID = S.TRAN_ID
	  JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
	  JOIN PSS.PAYMENT_SUBTYPE PST ON PTA.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
	  JOIN LOCATION.LOCATION L ON POS.LOCATION_ID = L.LOCATION_ID
	  JOIN LOCATION.TIME_ZONE TZ ON L.LOCATION_TIME_ZONE_CD = TZ.TIME_ZONE_CD
	WHERE X.TRAN_ID = pn_tran_id;
	
	-- use the base host for adjustments
	SELECT MAX(H.HOST_ID)
	INTO ln_base_host_id
	FROM DEVICE.HOST H
	WHERE H.DEVICE_ID = ln_device_id
	AND H.HOST_PORT_NUM = 0;
	IF ln_base_host_id IS NULL THEN
		pkg_device_configuration.sp_create_default_hosts(ln_device_id, ln_new_host_count, pn_result_cd, pv_error_message);
		IF pn_result_cd != PKG_CONST.RESULT__SUCCESS THEN
			RETURN;
		END IF;
		SELECT H.HOST_ID
		INTO ln_base_host_id
		FROM DEVICE.HOST H
		WHERE H.DEVICE_ID = ln_device_id
		AND H.HOST_PORT_NUM = 0;
	END IF;
	
	SELECT MAX(DISCOUNT_PERCENT), MAX(CAMPAIGN_ID)
      INTO ln_discount_percent, ln_campaign_id
	  FROM (
    SELECT C.DISCOUNT_PERCENT, CPP.PRIORITY, C.CAMPAIGN_ID
	  FROM PSS.CAMPAIGN_POS_PTA CPP
	  JOIN REPORT.CAMPAIGN C ON CPP.CAMPAIGN_ID = C.CAMPAIGN_ID
	  JOIN PSS.CAMPAIGN_CONSUMER_ACCT CCA on CPP.CAMPAIGN_ID = CCA.CAMPAIGN_ID
	 WHERE CCA.CONSUMER_ACCT_ID = ln_consumer_acct_id
	   AND CPP.POS_PTA_ID = ln_pos_pta_id
	   AND ld_tran_start_ts BETWEEN NVL(CPP.START_DATE, MIN_DATE) AND NVL(CPP.END_DATE, MAX_DATE)
	   AND C.CAMPAIGN_TYPE_ID = 1
	   AND C.DISCOUNT_PERCENT > 0
	   AND C.DISCOUNT_PERCENT < 1
	   AND (TRIM(C.RECUR_SCHEDULE) IS NULL OR REPORT.MATCH_CAMPAIGN_RECUR_SCHEDULE(C.RECUR_SCHEDULE, ld_tran_start_ts) = 'Y')
     ORDER BY CPP.PRIORITY, C.DISCOUNT_PERCENT DESC, C.CAMPAIGN_ID DESC)
     WHERE ROWNUM = 1 ;
	
	IF ln_discount_percent IS NOT NULL AND ln_sale_amount > 0 THEN
		ln_discount_amount := ROUND(ln_sale_amount * ln_discount_percent);
		IF ln_discount_amount > 0 THEN
			IF ln_discount_amount > ln_sale_amount THEN
				ln_discount_amount := ln_sale_amount;
			END IF;
			ln_sale_amount := ln_sale_amount - ln_discount_amount;
			
			UPDATE pss.sale
			SET sale_amount = ln_sale_amount / pn_minor_currency_factor
			WHERE tran_id = pn_tran_id;
			
			-- create Loyalty Discount line item
			INSERT INTO PSS.TRAN_LINE_ITEM (
				TRAN_ID,
				HOST_ID,
				TRAN_LINE_ITEM_TYPE_ID,
				TRAN_LINE_ITEM_AMOUNT,
				TRAN_LINE_ITEM_QUANTITY,
				TRAN_LINE_ITEM_DESC,
				TRAN_LINE_ITEM_BATCH_TYPE_CD,
				TRAN_LINE_ITEM_TAX,
                CAMPAIGN_ID)
			SELECT
				pn_tran_id,
				ln_base_host_id,
				tran_line_item_type_id,
				-ln_discount_amount / pn_minor_currency_factor,
				1,
				tran_line_item_type_desc || ' ' || ln_discount_percent * 100 || '%',
				pc_tran_batch_type_cd,
				0,
                ln_campaign_id
			FROM pss.tran_line_item_type
			WHERE tran_line_item_type_id = 204;
		END IF;
	END IF;
	
	SELECT /*+ INDEX(TLI IF1_TRAN_LINE_ITEM) */ NVL(SUM((NVL(TRAN_LINE_ITEM_AMOUNT, 0) + NVL(TRAN_LINE_ITEM_TAX, 0)) * NVL(TRAN_LINE_ITEM_QUANTITY, 0)), 0),
		   COUNT(1)
	  INTO ln_tli_total, pn_tli_count
	  FROM PSS.TRAN_LINE_ITEM TLI
	 WHERE TRAN_ID = pn_tran_id
	   AND TRAN_LINE_ITEM_BATCH_TYPE_CD = pc_tran_batch_type_cd;

	IF pv_tran_device_result_type_cd IN (PKG_CONST.TRAN_DEV_RES__CANCELLED,
			PKG_CONST.TRAN_DEV_RES__AUTH_FAILURE,
			PKG_CONST.TRAN_DEV_RES__FAILURE,
			PKG_CONST.TRAN_DEV_RES__TIMEOUT)
		OR NVL(pn_sale_result_id, -1) != PKG_CONST.SALE_RES__SUCCESS
		OR ln_sale_amount = 0 THEN
		IF ln_tli_total != 0 THEN
			ln_adj_tli := PKG_CONST.TLI__CANCELLATION_ADJMT;
			ln_adj_amt := -ln_tli_total;
		END IF;
	ELSE
		ln_adj_amt := ln_sale_amount / pn_minor_currency_factor - ln_tli_total;
		IF ln_adj_amt > 0 THEN
			ln_adj_tli := PKG_CONST.TLI__POS_DISCREPANCY_ADJMT;
		ELSIF ln_adj_amt < 0 THEN
			ln_adj_tli := PKG_CONST.TLI__NEG_DISCREPANCY_ADJMT;
		END IF;
	END IF;
    
	IF ln_device_type_id IN (0, 1) AND pc_sale_type_cd != PKG_CONST.SALE_TYPE__CASH THEN
		IF pn_tli_count > 0 AND ln_adj_amt != 0 THEN
			IF ln_adj_amt > 0 THEN
				-- create Transaction Amount Summary record
				INSERT INTO pss.tran_line_item (
					tran_id,
					host_id,
					tran_line_item_type_id,
					tran_line_item_amount,
					tran_line_item_quantity,
					tran_line_item_desc,
					tran_line_item_batch_type_cd,
					tran_line_item_tax)
				VALUES(
					pn_tran_id,
					ln_base_host_id,
					201,
					ln_adj_amt,
					1,
					'Transaction Amount Summary',
					pc_tran_batch_type_cd,
					pn_sale_tax);
			ELSE
				INSERT INTO pss.tran_line_item (
					tran_id,
					host_id,
					tran_line_item_type_id,
					tran_line_item_amount,
					tran_line_item_quantity,
					tran_line_item_desc,
					tran_line_item_batch_type_cd,
					tran_line_item_tax)
				SELECT
					pn_tran_id,
					ln_base_host_id,
					tran_line_item_type_id,
					ln_adj_amt,
					1,
					tran_line_item_type_desc,
					pc_tran_batch_type_cd,
					pn_sale_tax
				FROM pss.tran_line_item_type
				WHERE tran_line_item_type_id = PKG_CONST.TLI__NEG_DISCREPANCY_ADJMT;
			END IF;
		END IF;
   ELSE
		IF ln_adj_tli > -1 THEN
			INSERT INTO pss.tran_line_item (
				tran_id,
				host_id,
				tran_line_item_type_id,
				tran_line_item_amount,
				tran_line_item_quantity,
				tran_line_item_desc,
				tran_line_item_batch_type_cd,
				tran_line_item_tax)
			SELECT
				pn_tran_id,
				ln_base_host_id,
				ln_adj_tli,
				ln_adj_amt,
				1,
				tran_line_item_type_desc,
				pc_tran_batch_type_cd,
				pn_sale_tax
			FROM pss.tran_line_item_type
			WHERE tran_line_item_type_id = ln_adj_tli;
		END IF;            
	END IF;

	IF pn_tli_count = 0 AND pc_tran_import_needed = 'Y' THEN
		UPDATE PSS.SALE
		   SET IMPORTED = '-'
		 WHERE TRAN_ID = pn_tran_id
		   AND IMPORTED NOT IN('-', 'Y');
	END IF;
    
	IF lc_tran_state_cd NOT IN (PKG_CONST.TRAN_STATE__SALE_NO_AUTH, PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND, PKG_CONST.TRAN_STATE__SALE_NO_AUTH_ERROR) THEN
		pc_session_update_needed := 'Y';
	END IF;

	pn_result_cd := PKG_CONST.RESULT__SUCCESS;
	pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;

PROCEDURE CREATE_REPLENISHMENT(
    pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
    pn_auth_id PSS.AUTH.AUTH_ID%TYPE,
    pn_replenish_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
    pv_tran_device_result_type_cd PSS.TRAN.TRAN_DEVICE_RESULT_TYPE_CD%TYPE,
    pn_device_batch_id PSS.SALE.DEVICE_BATCH_ID%TYPE,
    pc_receipt_result_cd PSS.SALE.RECEIPT_RESULT_CD%TYPE,
    pc_auth_only CHAR,
    pc_tran_state_cd OUT VARCHAR2,
    pc_client_payment_type_cd OUT VARCHAR2,
    pn_replenish_amount OUT PSS.AUTH.AUTH_AMT_APPROVED%TYPE,
    pn_replenish_balance_amount OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE)
IS
    ln_consumer_acct_id PSS.TRAN.CONSUMER_ACCT_ID%TYPE;
    ln_host_id PSS.TRAN_LINE_ITEM.HOST_ID%TYPE;
    ln_tran_line_item_id PSS.TRAN_LINE_ITEM.TRAN_LINE_ITEM_ID%TYPE;
    ln_result_cd NUMBER;
    ln_new_host_count NUMBER;
    lv_error_message VARCHAR2(4000);
    ln_device_id PSS.POS.DEVICE_ID%TYPE;
    ln_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE;
    ld_sale_utc_ts PSS.SALE.SALE_START_UTC_TS%TYPE;
    ld_sale_local_date PSS.TRAN.TRAN_START_TS%TYPE;
    lv_global_session_cd PSS.SALE.SALE_GLOBAL_SESSION_CD%TYPE;
    lv_device_name PSS.TRAN.DEVICE_NAME%TYPE;
    lv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE;
    lv_device_tran_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE;
    ld_auth_ts PSS.AUTH.AUTH_TS%TYPE;
    ln_consumer_acct_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_TYPE_ID%TYPE;
    ln_consumer_acct_sub_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_SUB_TYPE_ID%TYPE;
    lc_payment_type_cd PSS.CLIENT_PAYMENT_TYPE.PAYMENT_TYPE_CD%TYPE;
    lc_auth_hold_used PSS.AUTH.AUTH_HOLD_USED%TYPE;
    lc_is_cash CHAR(1);
    ln_corp_customer_id PSS.CONSUMER_ACCT.CORP_CUSTOMER_ID%TYPE;
    ln_consumer_acct_identifier PSS.CONSUMER_ACCT.CONSUMER_ACCT_IDENTIFIER%TYPE;                       
    lv_currency_cd PSS.POS_PTA.CURRENCY_CD%TYPE;
    ln_doc_id CORP.DOC.DOC_ID%TYPE;
    ln_ledger_id CORP.LEDGER.LEDGER_ID%TYPE;
    ln_trans_type_id PSS.PAYMENT_SUBTYPE.TRANS_TYPE_ID%TYPE;
BEGIN
    SELECT DECODE(pc_auth_only, 'Y', 0, A.AUTH_AMT_APPROVED), A.AUTH_TS, A.AUTH_HOLD_USED
      INTO pn_replenish_amount, ld_auth_ts, lc_auth_hold_used
      FROM PSS.AUTH A
     WHERE A.AUTH_ID = pn_auth_id;
    
    UPDATE PSS.TRAN X
       SET TRAN_STATE_CD = DECODE(pc_auth_only, 'Y', DECODE(lc_auth_hold_used, 'Y', '8', 'C'), (SELECT DECODE(CPT.PAYMENT_TYPE_CD, 'M', 'D', '8') FROM PSS.CLIENT_PAYMENT_TYPE CPT WHERE CPT.CLIENT_PAYMENT_TYPE_CD = X.CLIENT_PAYMENT_TYPE_CD)),
           TRAN_END_TS = TRAN_START_TS,
           TRAN_UPLOAD_TS = SYSDATE,
           TRAN_DEVICE_RESULT_TYPE_CD = pv_tran_device_result_type_cd
     WHERE TRAN_ID = pn_tran_id
     RETURNING CLIENT_PAYMENT_TYPE_CD, POS_PTA_ID, TRAN_START_TS, AUTH_GLOBAL_SESSION_CD, DEVICE_NAME, TRAN_DEVICE_TRAN_CD, TRAN_STATE_CD
      INTO pc_client_payment_type_cd, ln_pos_pta_id, ld_sale_local_date, lv_global_session_cd, lv_device_name, lv_device_tran_cd, pc_tran_state_cd;
    
    SELECT DECODE(PAYMENT_TYPE_CD, 'M', 'Y', 'N') 
      INTO lc_is_cash
      FROM PSS.CLIENT_PAYMENT_TYPE
     WHERE CLIENT_PAYMENT_TYPE_CD = pc_client_payment_type_cd;
    
    ld_sale_utc_ts := TO_TIMESTAMP(ld_auth_ts) AT TIME ZONE 'GMT';
    INSERT INTO PSS.SALE (
			TRAN_ID,
			DEVICE_BATCH_ID,
			SALE_TYPE_CD,
			SALE_START_UTC_TS,
			SALE_END_UTC_TS,
			SALE_UTC_OFFSET_MIN,
			SALE_RESULT_ID,
			SALE_AMOUNT,
			RECEIPT_RESULT_CD,
			HASH_TYPE_CD,
			TRAN_LINE_ITEM_HASH,
			SALE_GLOBAL_SESSION_CD,
			IMPORTED,
			VOID_ALLOWED) 
     SELECT pn_tran_id,
			pn_device_batch_id,
			DECODE(lc_is_cash, 'Y', 'C', 'A'),
			ld_sale_utc_ts,
			ld_sale_utc_ts,
			(ld_sale_local_date - CAST(ld_sale_utc_ts AS DATE)) * 24 * 60,
			DECODE(pc_auth_only, 'Y', 1, 0),
			pn_replenish_amount,
			pc_receipt_result_cd,
			'SHA1',
			'00',
			lv_global_session_cd,
			'N',
			'Y'
       FROM DUAL;
		
    IF pc_client_payment_type_cd IN (PKG_CONST.CLNT_PMNT_TYPE__ISIS_CREDIT, PKG_CONST.CLNT_PMNT_TYPE__ISIS_SPECIAL) AND ln_consumer_acct_id > 0 THEN
        PROCESS_ISIS_TRAN(pn_tran_id);
    END IF;
		
    SELECT D.DEVICE_ID, D.DEVICE_SERIAL_CD, PP.CURRENCY_CD, PST.TRANS_TYPE_ID
      INTO ln_device_id, lv_device_serial_cd, lv_currency_cd, ln_trans_type_id
      FROM DEVICE.DEVICE D
      JOIN PSS.POS P ON D.DEVICE_ID = P.DEVICE_ID
      JOIN PSS.POS_PTA PP ON PP.POS_ID = P.POS_ID
      JOIN PSS.PAYMENT_SUBTYPE PST ON PP.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
     WHERE PP.POS_PTA_ID = ln_pos_pta_id;
     
    ln_host_id := SF_FIND_HOST_ID(ln_device_id, 0, 0);
	IF ln_host_id IS NULL THEN
		-- create default hosts
		PKG_DEVICE_CONFIGURATION.SP_CREATE_DEFAULT_HOSTS(ln_device_id, ln_new_host_count, ln_result_cd, lv_error_message);
		IF ln_result_cd != PKG_CONST.RESULT__SUCCESS THEN
			RETURN;
		END IF;
		ln_host_id := SF_FIND_HOST_ID(ln_device_id, 0, 0);
	END IF;

	SELECT PSS.SEQ_TLI_ID.NEXTVAL
		INTO ln_tran_line_item_id
		FROM DUAL;
                  
    INSERT INTO PSS.TRAN_LINE_ITEM (
		TRAN_LINE_ITEM_ID,
		TRAN_ID,
		TRAN_LINE_ITEM_AMOUNT,
		TRAN_LINE_ITEM_POSITION_CD,
		TRAN_LINE_ITEM_TAX,
		TRAN_LINE_ITEM_TYPE_ID,
		TRAN_LINE_ITEM_QUANTITY,
		TRAN_LINE_ITEM_DESC,
		HOST_ID,
		TRAN_LINE_ITEM_BATCH_TYPE_CD,
		TRAN_LINE_ITEM_TS,
		SALE_RESULT_ID,
		APPLY_TO_CONSUMER_ACCT_ID)
	SELECT
		ln_tran_line_item_id,
		pn_tran_id,
		pn_replenish_amount,
		NULL,
		0,
		550,
		1,
		'Replenishment of card id ' || CAB.GLOBAL_ACCOUNT_ID,
		ln_host_id,
		'A',
		ld_sale_local_date,
		0,
		pn_replenish_consumer_acct_id
	FROM PSS.CONSUMER_ACCT_BASE CAB
	WHERE CONSUMER_ACCT_ID = pn_replenish_consumer_acct_id;

    UPDATE PSS.CONSUMER_ACCT
	   SET CONSUMER_ACCT_BALANCE = CONSUMER_ACCT_BALANCE + pn_replenish_amount,
		   CONSUMER_ACCT_REPLEN_BALANCE = CONSUMER_ACCT_REPLEN_BALANCE + pn_replenish_amount,
		   CONSUMER_ACCT_REPLENISH_TOTAL = CONSUMER_ACCT_REPLENISH_TOTAL + pn_replenish_amount
	 WHERE CONSUMER_ACCT_ID = pn_replenish_consumer_acct_id
      RETURNING CONSUMER_ACCT_TYPE_ID, CONSUMER_ACCT_SUB_TYPE_ID, CONSUMER_ACCT_BALANCE, CORP_CUSTOMER_ID, CONSUMER_ACCT_IDENTIFIER
      INTO ln_consumer_acct_type_id, ln_consumer_acct_sub_type_id, pn_replenish_balance_amount, ln_corp_customer_id, ln_consumer_acct_identifier;
   
    IF ln_consumer_acct_type_id IN(3) THEN
        IF lc_is_cash = 'Y' AND ln_consumer_acct_sub_type_id = 1 THEN
            IF ln_consumer_acct_identifier IS NULL THEN
                SELECT GLOBAL_ACCOUNT_ID
                  INTO ln_consumer_acct_identifier
                  FROM PSS.CONSUMER_ACCT_BASE
                 WHERE CONSUMER_ACCT_ID = pn_replenish_consumer_acct_id;
            END IF;
            -- add adjustment to ledger
            CORP.PAYMENTS_PKG.ADJUSTMENT_INS(ln_corp_customer_id, 'Cash Replenishment', lv_currency_cd, 'Cash Replenishment of card id ' || ln_consumer_acct_identifier,
                -pn_replenish_amount, ln_doc_id, ln_ledger_id);
		ELSIF ln_consumer_acct_sub_type_id = 2 THEN
		    UPDATE PSS.AUTH
		       SET OVERRIDE_TRANS_TYPE_ID = (
		              SELECT TT.OPERATOR_TRANS_TYPE_ID
		                FROM REPORT.TRANS_TYPE TT
		               WHERE TT.TRANS_TYPE_ID = ln_trans_type_id)
             WHERE AUTH_ID = pn_auth_id;
		END IF;
        ADD_REPLENISH_BONUSES(
            pn_tran_id,
            pn_replenish_consumer_acct_id,
            pn_replenish_amount,
            ld_auth_ts);
    END IF;
END;

PROCEDURE UPDATE_AUTHORIZATION(
    pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
    pn_auth_id PSS.AUTH.AUTH_ID%TYPE,
    pv_card_key PSS.AUTH.CARD_KEY%TYPE,
    pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE)
IS
    ld_auth_ts PSS.AUTH.AUTH_TS%TYPE;
    lv_masked_card_number PSS.TRAN.TRAN_RECEIVED_RAW_ACCT_DATA%TYPE;
    ln_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE;
    lv_device_name PSS.TRAN.DEVICE_NAME%TYPE;
    lv_device_tran_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE;
    ln_apply_to_consumer_acct_id PSS.TRAN_LINE_ITEM.APPLY_TO_CONSUMER_ACCT_ID%TYPE;
BEGIN
    UPDATE PSS.AUTH
       SET CARD_KEY = CASE WHEN CARD_KEY IS NOT NULL THEN CARD_KEY ELSE pv_card_key END
     WHERE AUTH_ID = pn_auth_id
       AND TRAN_ID = pn_tran_id
       AND pv_card_key IS NOT NULL
     RETURNING AUTH_TS INTO ld_auth_ts;
       
    UPDATE PSS.TRAN
       SET CONSUMER_ACCT_ID = CASE WHEN CONSUMER_ACCT_ID IS NOT NULL THEN CONSUMER_ACCT_ID ELSE pn_consumer_acct_id END
     WHERE TRAN_ID = pn_tran_id
     RETURNING TRAN_RECEIVED_RAW_ACCT_DATA, POS_PTA_ID, DEVICE_NAME, TRAN_DEVICE_TRAN_CD 
          INTO lv_masked_card_number, ln_pos_pta_id, lv_device_name, lv_device_tran_cd;
    
    IF pv_card_key IS NOT NULL THEN
        SELECT MAX(APPLY_TO_CONSUMER_ACCT_ID)
          INTO ln_apply_to_consumer_acct_id
          FROM PSS.TRAN_LINE_ITEM
         WHERE TRAN_ID = pn_tran_id
           AND TRAN_LINE_ITEM_TYPE_ID = 550;
        IF ln_apply_to_consumer_acct_id IS NOT NULL THEN
            FINISH_REPLENISH_SETUP(
                pn_tran_id,
                ln_apply_to_consumer_acct_id,
                pv_card_key,
                ld_auth_ts,
                lv_masked_card_number,
                ln_pos_pta_id);    
        END IF;
    END IF;
    DELETE FROM PSS.CONSUMER_ACCT_PEND_REPLENISH
         WHERE DEVICE_NAME = lv_device_name
           AND DEVICE_TRAN_CD = TO_NUMBER_OR_NULL(lv_device_tran_cd);
END;

-- R37+ signature
PROCEDURE SP_CREATE_AUTH(
   pv_global_event_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
   pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
   pn_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE,
   pv_device_event_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE,
   pc_invalid_device_event_cd CHAR,
   pn_tran_start_time NUMBER,
   pn_auth_utc_ms NUMBER,
   pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
   pc_entry_method CHAR,
   pv_masked_card_number PSS.TRAN.TRAN_RECEIVED_RAW_ACCT_DATA%TYPE,
   pn_consumer_acct_id PSS.TRAN.CONSUMER_ACCT_ID%TYPE,
   pd_auth_ts PSS.AUTH.AUTH_TS%TYPE,
   pv_authority_resp_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
   pv_authority_resp_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
   pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
   pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
   pt_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
   pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
   pn_trace_number PSS.AUTH.TRACE_NUMBER%TYPE,
   pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
   pn_auth_amt PSS.AUTH.AUTH_AMT%TYPE,
   pn_balance_amt PSS.AUTH.AUTH_BALANCE_AMT%TYPE,
   pn_requested_amt PSS.AUTH.AUTH_AUTHORITY_AMT_RQST%TYPE,
   pn_received_amt PSS.AUTH.AUTH_AUTHORITY_AMT_RCVD%TYPE,
   pn_add_auth_hold_days NUMBER,
   pc_auth_hold_used PSS.AUTH.AUTH_HOLD_USED%TYPE,
   pv_global_session_cd VARCHAR2,
   pc_ignore_dup CHAR,
   pn_auth_action_id PSS.AUTH.AUTH_ACTION_ID%TYPE,
   pn_auth_action_bitmap PSS.AUTH.AUTH_ACTION_BITMAP%TYPE,
   pc_sent_to_device CHAR,
   pc_pass_thru CHAR,
   pv_card_key PSS.AUTH.CARD_KEY%TYPE,
   pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
   pc_tran_state_cd OUT VARCHAR2,
   pc_tran_import_needed OUT VARCHAR2,
   pc_session_update_needed OUT VARCHAR2,
   pv_sale_global_session_cd OUT VARCHAR2,
   pn_sale_session_start_time OUT PSS.SALE.SALE_SESSION_START_TIME%TYPE,
   pc_client_payment_type_cd OUT VARCHAR2,
   pn_sale_amount OUT pss.sale.sale_amount%TYPE,
   pn_tli_count OUT NUMBER,
   pn_auth_id OUT PSS.AUTH.AUTH_ID%TYPE) 
IS
   lc_sale_type_cd PSS.SALE.SALE_TYPE_CD%TYPE;
   ln_sale_result_id PSS.SALE.SALE_RESULT_ID%TYPE;
   ld_orig_trace_number PSS.AUTH.TRACE_NUMBER%TYPE;
   lv_orig_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE;
   lv_last_lock_utc_ts VARCHAR2(128);
   lc_invalid_device_event_cd CHAR := pc_invalid_device_event_cd;
   ld_tran_start_ts PSS.TRAN.TRAN_START_TS%TYPE := CAST(MILLIS_TO_TIMESTAMP(pn_tran_start_time) AS DATE);
   lc_imported PSS.SALE.IMPORTED%TYPE;
   ln_auth_amt_approved pss.auth.auth_amt_approved%TYPE;
   ln_auth_amt_allowed pss.auth.auth_amt_approved%TYPE;
   ln_sale_over_auth_amt_percent NUMBER;
   lv_email_from_address engine.app_setting.app_setting_value%TYPE;
   lv_email_to_address engine.app_setting.app_setting_value%TYPE;
   lv_error pss.tran.tran_info%TYPE;
   ln_device_id device.device_id%TYPE;
   lc_payment_subtype_key_id pss.pos_pta.payment_subtype_key_id%TYPE;
   lc_payment_subtype_class pss.payment_subtype.payment_subtype_class%TYPE;
   lc_previous_tran_state_cd pss.tran.tran_state_cd%TYPE;
   lt_auth_utc_ts pss.consumer_acct_device.last_used_utc_ts%TYPE;
   ln_override_trans_type_id PSS.AUTH.OVERRIDE_TRANS_TYPE_ID%TYPE;
   ln_consumer_acct_sub_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_SUB_TYPE_ID%TYPE;
BEGIN
	pn_sale_amount := 0;
	pc_session_update_needed := 'N';

	SELECT DECODE(pc_auth_result_cd, 'Y', pn_auth_amt, 'P', pn_received_amt) / pn_minor_currency_factor
	INTO ln_auth_amt_approved
	FROM DUAL;
	pc_tran_import_needed := 'N';
	lv_last_lock_utc_ts := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', pv_device_name || ':' || pv_device_event_cd);
	
	BEGIN
		SELECT tran_id, sale_type_cd, tran_state_cd, trace_number, sale_result_id, imported, sale_global_session_cd, sale_session_start_time, AUTH_RESULT_CD
		INTO pn_tran_id, lc_sale_type_cd, pc_tran_state_cd, ld_orig_trace_number, ln_sale_result_id, lc_imported, pv_sale_global_session_cd, pn_sale_session_start_time, lv_orig_auth_result_cd 
		FROM
		(
			SELECT /*+ INDEX(t IDX_DEVICE_TRAN_CD) */ t.tran_id, s.sale_type_cd, t.tran_state_cd, a.trace_number, s.sale_result_id, s.imported, s.sale_global_session_cd, s.sale_session_start_time, a.AUTH_RESULT_CD
			FROM pss.tran t
			LEFT OUTER JOIN pss.sale s ON t.tran_id = s.tran_id
			LEFT OUTER JOIN pss.auth a ON t.tran_id = a.tran_id AND a.auth_type_cd = 'N'
			WHERE t.tran_device_tran_cd = pv_device_event_cd 
			  AND (t.tran_global_trans_cd = pv_global_event_cd OR t.tran_global_trans_cd LIKE pv_global_event_cd || ':%')
			ORDER BY CASE WHEN a.trace_number = pn_trace_number THEN 1 ELSE 2 END,
				CASE WHEN t.AUTH_GLOBAL_SESSION_CD = pv_global_session_cd THEN 1 ELSE 2 END,
				CASE WHEN a.AUTH_RESULT_CD IN('Y', 'P') THEN 1 ELSE 2 END,
				CASE WHEN s.sale_type_cd IN('A', 'I') THEN 1 ELSE 2 END, 
				CASE WHEN t.tran_global_trans_cd = pv_global_event_cd THEN 1 ELSE 2 END,
				t.tran_start_ts, t.created_ts, a.created_ts
		)
		WHERE ROWNUM = 1;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			pn_tran_id := NULL;
	END;
    
	IF pn_tran_id IS NOT NULL AND lc_sale_type_cd = PKG_CONST.SALE_TYPE__CASH THEN
		pn_tran_id := NULL; -- this is not a match so insert it
		lc_invalid_device_event_cd := 'Y'; -- add ':' || tran_id to tran_global_trans_cd
	END IF;
    
	IF pn_tran_id IS NOT NULL THEN
		lc_previous_tran_state_cd := pc_tran_state_cd;
		IF ld_orig_trace_number = pn_trace_number THEN
			IF lc_imported NOT IN('Y', '-') AND lc_sale_type_cd IN(PKG_CONST.SALE_TYPE__ACTUAL, PKG_CONST.SALE_TYPE__CASH) AND ln_sale_result_id = 0 AND pc_tran_state_cd NOT IN('F', 'G', 'Z') THEN
				pc_tran_import_needed := 'Y';
			ELSE
				pc_tran_import_needed := 'N';
			END IF;                    
			RETURN; -- already inserted; exit
		ELSIF pc_auth_result_cd IN('Y', 'P') AND lv_orig_auth_result_cd  IN('Y', 'P') THEN -- Device sent dup tran id in two different auths
			pc_tran_state_cd := PKG_CONST.TRAN_STATE__DUPLICATE;
			lc_invalid_device_event_cd := 'Y';
		ELSIF pc_pass_thru = 'N' THEN -- This allows saving pass-thru auths
			IF pc_tran_state_cd IN(PKG_CONST.TRAN_STATE__SALE_NO_AUTH, PKG_CONST.TRAN_STATE__SALE_NO_AUTH_ERROR, PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND)
				OR (pc_tran_state_cd = PKG_CONST.TRAN_STATE__CLIENT_CANCELLED AND ln_sale_result_id != 0 /*Not 'Success'*/) THEN
				
				IF pc_auth_result_cd IN('Y', 'P') THEN
					IF lc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED THEN
						pc_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH_INTENDED;
					ELSE
						BEGIN
							SELECT S.SALE_AMOUNT, P.DEVICE_ID
							INTO pn_sale_amount, ln_device_id
							FROM PSS.SALE S
							JOIN PSS.TRAN T ON S.TRAN_ID = T.TRAN_ID
							JOIN PSS.POS_PTA PP ON T.POS_PTA_ID = PP.POS_PTA_ID
							JOIN PSS.POS P ON PP.POS_ID = P.POS_ID
							WHERE S.TRAN_ID = pn_tran_id;
						EXCEPTION
							WHEN NO_DATA_FOUND THEN
								NULL;
						END;
						pc_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH;
						IF pn_sale_amount > 0 AND ln_auth_amt_approved > 0 AND pn_sale_amount > ln_auth_amt_approved THEN
							SELECT GREATEST(ln_auth_amt_approved, NVL(NVL(MAX(DBADMIN.TO_NUMBER_OR_NULL(DS_MAA.DEVICE_SETTING_VALUE) / 100), MAX(DBADMIN.TO_NUMBER_OR_NULL(DS_AUTH_AMT.DEVICE_SETTING_VALUE) / DECODE(D.DEVICE_TYPE_ID, 13, 100, 1))), 0))
							INTO ln_auth_amt_allowed
							FROM DEVICE.DEVICE D
							LEFT OUTER JOIN DEVICE.DEVICE_SETTING DS_MAA ON D.DEVICE_ID = DS_MAA.DEVICE_ID AND DS_MAA.DEVICE_SETTING_PARAMETER_CD = 'MAX_AUTH_AMOUNT'
							LEFT OUTER JOIN DEVICE.DEVICE_SETTING DS_AUTH_AMT ON D.DEVICE_ID = DS_AUTH_AMT.DEVICE_ID
								AND DS_AUTH_AMT.DEVICE_SETTING_PARAMETER_CD = DECODE(D.DEVICE_TYPE_ID, 13, '1200', 1, '195', 11, 'AUTHORIZATION_AMOUNT', 0, '195')
							WHERE D.DEVICE_ID = ln_device_id;
						
							ln_sale_over_auth_amt_percent := NVL(DBADMIN.TO_NUMBER_OR_NULL(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('ALLOWED_SALE_AMT_OVER_AUTH_AMT_PERCENT')), 100);
							IF pn_sale_amount > ln_auth_amt_allowed + ln_auth_amt_allowed * ln_sale_over_auth_amt_percent / 100 THEN
								pc_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE_ERROR;
								lv_error := 'Error: Sale amount ' || TO_CHAR(pn_sale_amount, 'FM9,999,999,990.00') || ' exceeds allowed auth amount ' || TO_CHAR(ln_auth_amt_allowed, 'FM9,999,999,990.00') || ' by more than ' || ln_sale_over_auth_amt_percent || '%';
								UPDATE PSS.TRAN
								SET TRAN_INFO = SUBSTR(TRAN_INFO || DECODE(TRAN_INFO, NULL, '', ', ') || lv_error, 1, 1000)
								WHERE TRAN_ID = pn_tran_id AND (TRAN_INFO IS NULL OR INSTR(TRAN_INFO, lv_error) = 0);
								
								UPDATE DEVICE.DEVICE
								SET DEVICE_ACTIVE_YN_FLAG = 'N'
								WHERE DEVICE_NAME = pv_device_name AND DEVICE_ACTIVE_YN_FLAG = 'Y';
								
								lv_email_from_address := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EMAIL_FROM_ADDRESS');
								lv_email_to_address := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EMAIL_TO_ADDRESS_CUSTOMER_SERVICE');
								INSERT INTO ENGINE.OB_EMAIL_QUEUE(OB_EMAIL_FROM_EMAIL_ADDR, OB_EMAIL_FROM_NAME, OB_EMAIL_TO_EMAIL_ADDR, OB_EMAIL_TO_NAME, OB_EMAIL_SUBJECT, OB_EMAIL_MSG)
								VALUES(lv_email_from_address, lv_email_from_address, lv_email_to_address, lv_email_to_address, 'Invalid sale amount', lv_error || ', device: ' || pv_device_name || ', transaction ID: ' || pn_tran_id || '. Device has been disabled.');
							END IF;
						END IF;
					END IF;
				ELSE
					IF lc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED THEN
						pc_tran_state_cd := PKG_CONST.TRAN_STATE__INTENDED_ERROR;
					ELSE
						pc_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE_ERROR;
					END IF;
				END IF;
				
				SELECT pp.PAYMENT_SUBTYPE_KEY_ID, ps.PAYMENT_SUBTYPE_CLASS, ps.CLIENT_PAYMENT_TYPE_CD
				INTO lc_payment_subtype_key_id, lc_payment_subtype_class, pc_client_payment_type_cd
				FROM pss.pos_pta pp
				JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
				WHERE pp.pos_pta_id = pn_pos_pta_id;
				
				UPDATE PSS.TRAN
				   SET (TRAN_START_TS,
						TRAN_END_TS,
						TRAN_STATE_CD,
						TRAN_RECEIVED_RAW_ACCT_DATA,
						POS_PTA_ID,
						CONSUMER_ACCT_ID,
						AUTH_GLOBAL_SESSION_CD,
						PAYMENT_SUBTYPE_KEY_ID,
						PAYMENT_SUBTYPE_CLASS,
						CLIENT_PAYMENT_TYPE_CD,						
						AUTH_HOLD_USED,
						DEVICE_NAME) =
					(SELECT
						ld_tran_start_ts,  /* TRAN_START_TS */
						TRAN_END_TS - TRAN_START_TS + ld_tran_start_ts,  /* TRAN_END_TS */
						pc_tran_state_cd,  /* TRAN_STATE_CD */
						pv_masked_card_number,  /* TRAN_RECEIVED_RAW_ACCT_DATA */
						pn_pos_pta_id, /* POS_PTA_ID */
						pn_consumer_acct_id  /* CONSUMER_ACCT_ID */,
						pv_global_session_cd,
						lc_payment_subtype_key_id,
						lc_payment_subtype_class,
						pc_client_payment_type_cd,
						pc_auth_hold_used,
						pv_device_name
					FROM dual
					) WHERE TRAN_ID = pn_tran_id;
					
				IF lc_previous_tran_state_cd IN (PKG_CONST.TRAN_STATE__SALE_NO_AUTH, PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND, PKG_CONST.TRAN_STATE__SALE_NO_AUTH_ERROR) THEN
					SELECT /*+ INDEX(TLI IF1_TRAN_LINE_ITEM) */ COUNT(1)
					INTO pn_tli_count
					FROM PSS.TRAN_LINE_ITEM TLI
					WHERE TRAN_ID = pn_tran_id
						AND TRAN_LINE_ITEM_BATCH_TYPE_CD = lc_sale_type_cd
						AND TRAN_LINE_ITEM_TYPE_ID NOT IN (PKG_CONST.TLI__CANCELLATION_ADJMT, PKG_CONST.TLI__POS_DISCREPANCY_ADJMT, PKG_CONST.TLI__NEG_DISCREPANCY_ADJMT, 201);
					
					IF pv_sale_global_session_cd IS NOT NULL THEN
						pc_session_update_needed := 'Y';
						pn_sale_amount := pn_sale_amount * pn_minor_currency_factor;
					END IF;
				END IF;
			ELSE
				pc_tran_state_cd := PKG_CONST.TRAN_STATE__DUPLICATE;
				lc_invalid_device_event_cd := 'Y';
			END IF;
		END IF;
	ELSE
		IF lc_invalid_device_event_cd = 'N' AND INSTR(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('MASTER_ID_UPDATE_SALE_TYPES'), 'N') > 0 AND TO_NUMBER_OR_NULL(pv_device_event_cd) < DBADMIN.DATE_TO_MILLIS(SYSDATE + 365) / 1000 THEN
			SELECT P.DEVICE_ID
			  INTO ln_device_id
			  FROM PSS.POS_PTA PP
			  JOIN PSS.POS P ON PP.POS_ID = P.POS_ID
			 WHERE PP.POS_PTA_ID = pn_pos_pta_id;
			UPDATE DEVICE.DEVICE_SETTING 
			   SET DEVICE_SETTING_VALUE = pv_device_event_cd
			 WHERE DEVICE_ID = ln_device_id
			   AND DEVICE_SETTING_PARAMETER_CD = '60' -- Master Id
			   AND TO_NUMBER_OR_NULL(NVL(DEVICE_SETTING_VALUE, '0')) < TO_NUMBER_OR_NULL(pv_device_event_cd);
		END IF;
		IF pc_auth_result_cd IN('Y', 'P') AND pc_sent_to_device = 'N' AND pc_auth_hold_used = 'Y' THEN
			pc_tran_state_cd := 'W'; -- Pending Reversal
		ELSIF pc_auth_result_cd IN('F') AND pc_auth_hold_used = 'Y' THEN
			pc_tran_state_cd := 'W'; -- Pending Reversal
		ELSIF pc_auth_result_cd IN('Y') THEN
			pc_tran_state_cd := '6'; -- Auth Success
		ELSIF pc_auth_result_cd IN('P') THEN
			pc_tran_state_cd := '0'; -- Auth Success Conditional
		ELSIF pc_auth_result_cd IN('N', 'O', 'R') THEN
			pc_tran_state_cd := '7'; -- Auth Decline
		ELSIF pc_auth_result_cd IN('F') THEN
			pc_tran_state_cd := '5'; -- Auth Failure
		END IF;
	END IF;
		
	IF pn_tran_id IS NULL OR pc_tran_state_cd = PKG_CONST.TRAN_STATE__DUPLICATE THEN
		SELECT PSS.SEQ_TRAN_ID.NEXTVAL
		  INTO pn_tran_id
		  FROM DUAL;    
		INSERT INTO PSS.TRAN (
			TRAN_ID,
			TRAN_START_TS,
			TRAN_END_TS,
			TRAN_STATE_CD,
			TRAN_DEVICE_TRAN_CD,
			TRAN_RECEIVED_RAW_ACCT_DATA,
			POS_PTA_ID,
			TRAN_GLOBAL_TRANS_CD,
			CONSUMER_ACCT_ID,
			AUTH_GLOBAL_SESSION_CD,
			PAYMENT_SUBTYPE_KEY_ID,
			PAYMENT_SUBTYPE_CLASS,
			CLIENT_PAYMENT_TYPE_CD,
			AUTH_HOLD_USED,
			DEVICE_NAME)
		SELECT
			pn_tran_id, /* TRAN_ID */
			ld_tran_start_ts,  /* TRAN_START_TS */
			ld_tran_start_ts,  /* TRAN_END_TS */
			pc_tran_state_cd,  /* TRAN_STATE_CD */
			pv_device_event_cd,  /* TRAN_DEVICE_TRAN_CD */
			pv_masked_card_number,  /* TRAN_RECEIVED_RAW_ACCT_DATA */
			pn_pos_pta_id, /* POS_PTA_ID */
			DECODE(lc_invalid_device_event_cd, 'Y', pv_global_event_cd || ':' || pn_tran_id, pv_global_event_cd), /* TRAN_GLOBAL_TRANS_CD */
			pn_consumer_acct_id  /* CONSUMER_ACCT_ID */,
			pv_global_session_cd,
			pp.payment_subtype_key_id,
			ps.payment_subtype_class,
			ps.client_payment_type_cd,
			pc_auth_hold_used,
			pv_device_name
		FROM pss.pos_pta pp
		JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
		WHERE pp.pos_pta_id = pn_pos_pta_id;
	END IF;

    IF pn_consumer_acct_id IS NOT NULL THEN
        SELECT MAX(CONSUMER_ACCT_SUB_TYPE_ID)
          INTO ln_consumer_acct_sub_type_id
          FROM PSS.CONSUMER_ACCT
         WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id;
        IF ln_consumer_acct_sub_type_id IS NOT NULL THEN
            SELECT DECODE(ln_consumer_acct_sub_type_id, 2, TT.OPERATOR_TRANS_TYPE_ID, PST.TRANS_TYPE_ID)
              INTO ln_override_trans_type_id
              FROM PSS.POS_PTA PP
              JOIN PSS.PAYMENT_SUBTYPE PST ON PP.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
			  JOIN REPORT.TRANS_TYPE TT ON PST.TRANS_TYPE_ID = TT.TRANS_TYPE_ID
             WHERE PP.POS_PTA_ID = pn_pos_pta_id;
        END IF;
    END IF;
	SELECT PSS.SEQ_AUTH_ID.NEXTVAL
	  INTO pn_auth_id
	  FROM DUAL;
	INSERT INTO PSS.AUTH (
		AUTH_ID,
		TRAN_ID,
		AUTH_STATE_ID,
		AUTH_TYPE_CD,
		ACCT_ENTRY_METHOD_CD,
		AUTH_TS,
		AUTH_RESULT_CD,
		AUTH_RESP_CD,
		AUTH_RESP_DESC,
		AUTH_AUTHORITY_TRAN_CD,
		AUTH_AUTHORITY_REF_CD,
		AUTH_AUTHORITY_TS,
		AUTH_AUTHORITY_MISC_DATA,
		AUTH_AMT,
		AUTH_AMT_APPROVED,
		AUTH_AUTHORITY_AMT_RQST,
		AUTH_AUTHORITY_AMT_RCVD,
		AUTH_BALANCE_AMT,
		TRACE_NUMBER,
		AUTH_ACTION_ID,
		AUTH_ACTION_BITMAP,
		AUTH_HOLD_USED,
		CARD_KEY,
        OVERRIDE_TRANS_TYPE_ID)
	 VALUES(
		pn_auth_id, /* AUTH_ID */
		pn_tran_id, /* TRAN_ID */
		DECODE(pc_auth_result_cd, 'Y', 2, 'N', 3, 'P', 5, 'O', 7, 'F', 4, 'R', 7), /* AUTH_STATE_ID */
		'N', /* AUTH_TYPE_CD */
		DECODE(pc_entry_method, 'B', 7, 'C', 6, 'M', 2, 'S', 3, 'I', 8, 1), /* ACCT_ENTRY_METHOD_CD */
		pd_auth_ts, /* AUTH_TS */
		pc_auth_result_cd, /* AUTH_RESULT_CD */
		pv_authority_resp_cd, /* AUTH_RESP_CD */
		pv_authority_resp_desc, /* AUTH_RESP_DESC */
		pv_authority_tran_cd, /* AUTH_AUTHORITY_TRAN_CD */
		pv_authority_ref_cd, /* AUTH_AUTHORITY_REF_CD */
		pt_authority_ts, /* AUTH_AUTHORITY_TS */
		pv_authority_misc_data, /* AUTH_AUTHORITY_MISC_DATA */
		NVL(pn_auth_amt / pn_minor_currency_factor, 0), /* AUTH_AMT */
		ln_auth_amt_approved, /* AUTH_AMT_APPROVED */
		pn_requested_amt / pn_minor_currency_factor, /* AUTH_AUTHORITY_AMT_RQST */
		pn_received_amt / pn_minor_currency_factor,  /* AUTH_AUTHORITY_AMT_RCVD */
		pn_balance_amt / pn_minor_currency_factor, /* AUTH_BALANCE_AMT */
		pn_trace_number, /* TRACE_NUMBER */
		pn_auth_action_id,
		pn_auth_action_bitmap,
		pc_auth_hold_used,
		pv_card_key,
        ln_override_trans_type_id);

	IF NVL(pn_add_auth_hold_days, 0) > 0 THEN
		INSERT INTO PSS.CONSUMER_ACCT_AUTH_HOLD (CONSUMER_ACCT_ID, AUTH_ID, TRAN_ID, EXPIRATION_TS)
		  VALUES(pn_consumer_acct_id, pn_auth_id, pn_tran_id, SYSDATE + pn_add_auth_hold_days);
	END IF;
	
	IF lc_imported NOT IN('Y', '-') AND lc_sale_type_cd IN(PKG_CONST.SALE_TYPE__ACTUAL, PKG_CONST.SALE_TYPE__CASH) AND ln_sale_result_id = 0 AND pc_tran_state_cd NOT IN('F', 'G', 'Z') THEN
		pc_tran_import_needed := 'Y';
	ELSE
		pc_tran_import_needed := 'N';
	END IF;
	
	IF pc_auth_result_cd = 'F' AND pv_authority_resp_cd = 'INVALID_AUTH_AMOUNT' AND pn_auth_amt > 0 THEN
	     INSERT INTO ENGINE.MACHINE_CMD_PENDING(MACHINE_ID, DATA_TYPE, COMMAND, EXECUTE_ORDER)
             SELECT *
               FROM (
             SELECT DEVICE_NAME MACHINE_ID, DECODE(DEVICE_TYPE_ID, 13, 'CB', '88') DATA_TYPE, 
                    DECODE(DEVICE_TYPE_ID, 13, '00000E' || TO_CHAR(6 + LENGTH(TO_CHAR(MAX_AUTH_AMOUNT)), 'FM000X') || '313230303D' || RAWTOHEX(TO_CHAR(MAX_AUTH_AMOUNT)) || '0A', '420000006100000002') COMMAND, 20 EXECUTE_ORDER
               FROM (SELECT D.DEVICE_NAME, D.DEVICE_TYPE_ID, NVL(NVL(DBADMIN.TO_NUMBER_OR_NULL(DD.MAX_AUTH_AMOUNT), DBADMIN.TO_NUMBER_OR_NULL(DD.AUTH_AMOUNT) * DECODE(D.DEVICE_TYPE_ID, 13, 1, 100) * 3), 0) MAX_AUTH_AMOUNT
               FROM PSS.POS_PTA PP
               JOIN PSS.POS P ON PP.POS_ID = P.POS_ID
               JOIN DEVICE.DEVICE D ON P.DEVICE_ID = D.DEVICE_ID
               JOIN DEVICE.DEVICE_DATA DD ON D.DEVICE_NAME = DD.DEVICE_NAME
              WHERE PP.POS_PTA_ID = pn_pos_pta_id
                AND D.DEVICE_TYPE_ID IN(0,1,13))
              WHERE MAX_AUTH_AMOUNT < pn_auth_amt
                AND MAX_AUTH_AMOUNT > 0) N
              WHERE NOT EXISTS(
                    SELECT 1 
                      FROM ENGINE.MACHINE_CMD_PENDING O
                    WHERE O.MACHINE_ID = N.MACHINE_ID
                      AND O.DATA_TYPE = N.DATA_TYPE
                      AND O.COMMAND = N.COMMAND);
	END IF;
	
	IF pn_consumer_acct_id IS NOT NULL THEN
		lt_auth_utc_ts := DBADMIN.MILLIS_TO_TIMESTAMP(pn_auth_utc_ms);
	
		FOR i IN 1..2 LOOP
			UPDATE PSS.CONSUMER_ACCT_DEVICE
			SET USED_COUNT = USED_COUNT + 1,
				LAST_USED_UTC_TS = CASE WHEN LAST_USED_UTC_TS IS NULL OR lt_auth_utc_ts > LAST_USED_UTC_TS THEN lt_auth_utc_ts ELSE LAST_USED_UTC_TS END
			WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id AND DEVICE_NAME = pv_device_name;
			
			IF SQL%FOUND THEN
				EXIT;
			ELSE
				BEGIN
					INSERT INTO PSS.CONSUMER_ACCT_DEVICE(CONSUMER_ACCT_ID, DEVICE_NAME, USED_COUNT, LAST_USED_UTC_TS)
					VALUES(pn_consumer_acct_id, pv_device_name, 1, lt_auth_utc_ts);
					EXIT;
				EXCEPTION
					WHEN DUP_VAL_ON_INDEX THEN
						NULL;
				END;
			END IF;
		END LOOP;
	END IF;
	
	IF pc_client_payment_type_cd IN (PKG_CONST.CLNT_PMNT_TYPE__ISIS_CREDIT, PKG_CONST.CLNT_PMNT_TYPE__ISIS_SPECIAL)
		AND pn_consumer_acct_id > 0 AND lc_previous_tran_state_cd = PKG_CONST.TRAN_STATE__SALE_NO_AUTH THEN
		PROCESS_ISIS_TRAN(pn_tran_id);
	END IF;
	
	IF REGEXP_LIKE(pv_authority_misc_data, 'ConsumerID=|OfferID=') THEN
		UPDATE REPORT.TRANS
		SET DESCRIPTION = pv_authority_misc_data
		WHERE MACHINE_TRANS_NO = pv_global_event_cd AND SOURCE_SYSTEM_CD = 'PSS' AND DESCRIPTION IS NULL;
	END IF;
END;

-- R33 - R36 signature
PROCEDURE SP_CREATE_AUTH(
   pv_global_event_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
   pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
   pn_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE,
   pv_device_event_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE,
   pc_invalid_device_event_cd CHAR,
   pn_tran_start_time NUMBER,
   pn_auth_utc_ms NUMBER,
   pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
   pc_entry_method CHAR,
   pc_payment_type CHAR,
   pv_masked_card_number PSS.TRAN.TRAN_RECEIVED_RAW_ACCT_DATA%TYPE,
   pv_pan_sha1 VARCHAR2,
   pr_consumer_acct_cd_hash PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD_HASH%TYPE,
   pn_consumer_acct_id PSS.TRAN.CONSUMER_ACCT_ID%TYPE,
   pd_auth_ts PSS.AUTH.AUTH_TS%TYPE,
   pv_authority_resp_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
   pv_authority_resp_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
   pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
   pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
   pt_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
   pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
   pn_trace_number PSS.AUTH.TRACE_NUMBER%TYPE,
   pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
   pn_auth_amt PSS.AUTH.AUTH_AMT%TYPE,
   pn_balance_amt PSS.AUTH.AUTH_BALANCE_AMT%TYPE,
   pn_requested_amt PSS.AUTH.AUTH_AUTHORITY_AMT_RQST%TYPE,
   pn_received_amt PSS.AUTH.AUTH_AUTHORITY_AMT_RCVD%TYPE,
   pn_add_auth_hold_days NUMBER,
   pc_auth_hold_used PSS.AUTH.AUTH_HOLD_USED%TYPE,
   pv_global_session_cd VARCHAR2,
   pc_ignore_dup CHAR,
   pn_auth_action_id PSS.AUTH.AUTH_ACTION_ID%TYPE,
   pn_auth_action_bitmap PSS.AUTH.AUTH_ACTION_BITMAP%TYPE,
   pc_sent_to_device CHAR,
   pc_pass_thru CHAR,
   pv_card_key PSS.AUTH.CARD_KEY%TYPE,
   pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
   pc_tran_state_cd OUT VARCHAR2,
   pc_tran_import_needed OUT VARCHAR2,
   pc_session_update_needed OUT VARCHAR2,
   pv_sale_global_session_cd OUT VARCHAR2,
   pn_sale_session_start_time OUT PSS.SALE.SALE_SESSION_START_TIME%TYPE,
   pc_client_payment_type_cd OUT VARCHAR2,
   pn_sale_amount OUT pss.sale.sale_amount%TYPE,
   pn_tli_count OUT NUMBER) 
IS
    ln_auth_id PSS.AUTH.AUTH_ID%TYPE; 
    ln_consumer_acct_id PSS.TRAN.CONSUMER_ACCT_ID%TYPE := pn_consumer_acct_id;
BEGIN
	IF ln_consumer_acct_id IS NULL AND pr_consumer_acct_cd_hash IS NOT NULL THEN
		SELECT MAX(CONSUMER_ACCT_ID)
		INTO ln_consumer_acct_id
		FROM PSS.CONSUMER_ACCT
		WHERE CONSUMER_ACCT_CD_HASH = pr_consumer_acct_cd_hash
			AND CONSUMER_ACCT_CD = pv_masked_card_number
			AND CONSUMER_ACCT_TYPE_ID = 5;
	END IF;
    
    SP_CREATE_AUTH(
       pv_global_event_cd,
       pv_device_name,
       pn_pos_pta_id,
       pv_device_event_cd,
       pc_invalid_device_event_cd,
       pn_tran_start_time,
       pn_auth_utc_ms,
       pc_auth_result_cd,
       pc_entry_method,
       pv_masked_card_number,
       ln_consumer_acct_id,
       pd_auth_ts,
       pv_authority_resp_cd,
       pv_authority_resp_desc,
       pv_authority_tran_cd,
       pv_authority_ref_cd,
       pt_authority_ts,
       pv_authority_misc_data,
       pn_trace_number,
       pn_minor_currency_factor,
       pn_auth_amt,
       pn_balance_amt,
       pn_requested_amt,
       pn_received_amt,
       pn_add_auth_hold_days,
       pc_auth_hold_used,
       pv_global_session_cd,
       pc_ignore_dup,
       pn_auth_action_id,
       pn_auth_action_bitmap,
       pc_sent_to_device,
       pc_pass_thru,
       pv_card_key,
       pn_tran_id,
       pc_tran_state_cd,
       pc_tran_import_needed,
       pc_session_update_needed,
       pv_sale_global_session_cd,
       pn_sale_session_start_time,
       pc_client_payment_type_cd,
       pn_sale_amount,
       pn_tli_count,
       ln_auth_id);
END;

-- R37 and above
PROCEDURE SP_INSERT_AUTH_STATS(
   pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
   pn_auth_id PSS.AUTH.AUTH_ID%TYPE,
   pn_request_time NUMBER,
   pn_applayer_start_time NUMBER,
   pn_authority_start_time NUMBER,
   pn_authority_end_time NUMBER,
   pn_applayer_end_time NUMBER,
   pn_response_time NUMBER
) IS
BEGIN
	INSERT INTO PSS.TRAN_STAT(TRAN_ID, AUTH_ID, TRAN_STAT_TYPE_ID, TRAN_STAT_VALUE)
	  SELECT pn_tran_id, pn_auth_id, TRAN_STAT_TYPE_ID, TRAN_STAT_VALUE FROM (
		SELECT 2 /* live auth "POSM" time*/ TRAN_STAT_TYPE_ID, (pn_applayer_end_time - pn_applayer_start_time) / 1000 TRAN_STAT_VALUE FROM DUAL WHERE pn_applayer_start_time IS NOT NULL AND pn_applayer_end_time IS NOT NULL
		UNION ALL SELECT 4 /* live auth network time*/, (pn_response_time - pn_request_time) / 1000 FROM DUAL WHERE pn_request_time IS NOT NULL AND pn_response_time IS NOT NULL
		UNION ALL SELECT 1 /* live auth gateway time*/, (pn_authority_end_time - pn_authority_start_time) / 1000 FROM DUAL WHERE pn_authority_start_time IS NOT NULL AND pn_authority_end_time IS NOT NULL) a
	 WHERE NOT EXISTS(SELECT 1 FROM PSS.TRAN_STAT TS WHERE pn_tran_id = TS.TRAN_ID AND NVL(pn_auth_id, 0) = NVL(TS.AUTH_ID, 0) AND A.TRAN_STAT_TYPE_ID = TS.TRAN_STAT_TYPE_ID);    
END;

-- R36 and below
PROCEDURE SP_INSERT_AUTH_STATS(
   pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
   pn_request_time NUMBER,
   pn_applayer_start_time NUMBER,
   pn_authority_start_time NUMBER,
   pn_authority_end_time NUMBER,
   pn_applayer_end_time NUMBER,
   pn_response_time NUMBER
) IS
BEGIN
    SP_INSERT_AUTH_STATS(
       pn_tran_id,
       NULL,
       pn_request_time,
       pn_applayer_start_time,
       pn_authority_start_time,
       pn_authority_end_time,
       pn_applayer_end_time,
       pn_response_time);
END;

-- ? - R36 Signature
PROCEDURE SP_PERMIT_CONSUMER_ACCT(
	pn_pos_pta_id PSS.POS_PTA.POS_PTA_ID%TYPE,
	pv_consumer_acct_cd PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD%TYPE,
	pt_auth_ts PSS.CONSUMER_ACCT.CONSUMER_ACCT_ACTIVATION_TS%TYPE,
	pv_currency_cd PSS.CURRENCY.CURRENCY_CD%TYPE,
	pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
	pn_action_id OUT PSS.PERMISSION_ACTION.ACTION_ID%TYPE,
	pn_action_code OUT NUMBER,
	pn_action_bitmap OUT NUMBER,
	pr_consumer_acct_cd_hash IN PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD_HASH%TYPE DEFAULT NULL
)
IS
	lc_store_action CHAR(1);
	lv_device_name DEVICE.DEVICE_NAME%TYPE;
	ln_device_id DEVICE.DEVICE_ID%TYPE;
	ln_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE;
BEGIN
	SELECT CONSUMER_ACCT_ID, DEVICE_ID, DEVICE_TYPE_ID, DEVICE_NAME
	  INTO pn_consumer_acct_id, ln_device_id, ln_device_type_id, lv_device_name
	  FROM (
		 SELECT CA.CONSUMER_ACCT_ID, D.DEVICE_ID, D.DEVICE_TYPE_ID, D.DEVICE_NAME, VLH.ANCESTOR_LOCATION_ID, MAX(CA.CONSUMER_ACCT_ISSUE_NUM) MAX_ISSUE_NUM, VLH.DEPTH
		   FROM PSS.POS_PTA PTA
		   JOIN PSS.POS POS ON PTA.POS_ID = POS.POS_ID
		   JOIN DEVICE.DEVICE D ON D.DEVICE_ID = POS.DEVICE_ID
		   JOIN LOCATION.VW_LOCATION_HIERARCHY VLH ON VLH.DESCENDENT_LOCATION_ID = POS.LOCATION_ID
		   JOIN PSS.CONSUMER_ACCT CA ON VLH.ANCESTOR_LOCATION_ID = CA.LOCATION_ID
		  WHERE PTA.POS_PTA_ID = pn_pos_pta_id
			AND (pr_consumer_acct_cd_hash IS NOT NULL AND CA.CONSUMER_ACCT_CD_HASH = pr_consumer_acct_cd_hash OR CA.CONSUMER_ACCT_CD = pv_consumer_acct_cd)
			AND CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y'
			AND NVL(CA.CONSUMER_ACCT_ACTIVATION_TS, MIN_DATE) <= pt_auth_ts
			AND NVL(CA.CONSUMER_ACCT_DEACTIVATION_TS, MAX_DATE) > pt_auth_ts
			AND CA.CURRENCY_CD = pv_currency_cd
			GROUP BY CA.CONSUMER_ACCT_ID, VLH.ANCESTOR_LOCATION_ID, VLH.DEPTH, D.DEVICE_ID, D.DEVICE_TYPE_ID, D.DEVICE_NAME
			ORDER BY VLH.DEPTH, MAX_ISSUE_NUM DESC    /* DEPTH IS ASCENDING, AS IT IS THE DIFFERENCE BETWEEN LOCATION AND ANCESTOR */
	) WHERE ROWNUM = 1;

	SELECT A.ACTION_ID, DTA.DEVICE_TYPE_ACTION_CD,
		   DECODE(A.ACTION_PARAM_TYPE_CD, 'B', SUM(POWER(2, AP.PROTOCOL_BIT_INDEX))) PROTOCOL_BITMAP,
		   DECODE(A.ACTION_CLEAR_PARAMETER_CD, NULL, 'N', 'Y') STORE_LAST_ACTION
	  INTO pn_action_id, pn_action_code, pn_action_bitmap, lc_store_action
	  FROM (SELECT * FROM (
		 SELECT CAP.PERMISSION_ACTION_ID, DTA.DEVICE_TYPE_ACTION_CD PROTOCOL_ACTION_CD,
				CASE WHEN LDA.DEVICE_ACTION_UTC_TS IS NOT NULL
						  AND CURRENT_TIMESTAMP < LDA.DEVICE_ACTION_UTC_TS
						  + NUMTODSINTERVAL(COALESCE(TO_NUMBER_OR_NULL(DS_T.DEVICE_SETTING_VALUE),
						  TO_NUMBER_OR_NULL(cts.CONFIG_TEMPLATE_SETTING_VALUE), 3600), 'SECOND') THEN 10
					 ELSE DTA.ACTION_ID END ACTION_ID
		   FROM PSS.CONSUMER_ACCT_PERMISSION CAP
		   JOIN PSS.PERMISSION_ACTION PA ON CAP.PERMISSION_ACTION_ID = PA.PERMISSION_ACTION_ID
		   JOIN DEVICE.DEVICE_TYPE_ACTION DTA ON DTA.ACTION_ID = PA.ACTION_ID
		   JOIN DEVICE.DEVICE_TYPE DT ON DTA.DEVICE_TYPE_ID = DT.DEVICE_TYPE_ID
		   LEFT OUTER JOIN PSS.LAST_DEVICE_ACTION LDA
			 ON LDA.DEVICE_NAME = lv_device_name
			AND CAP.CONSUMER_ACCT_ID = LDA.CONSUMER_ACCT_ID
			AND PA.ACTION_ID = LDA.DEVICE_ACTION_ID
		   JOIN DEVICE.ACTION A ON PA.ACTION_ID = A.ACTION_ID
		   LEFT OUTER JOIN DEVICE.DEVICE_SETTING DS_T ON ln_device_id = DS_T.DEVICE_ID AND DS_T.DEVICE_SETTING_PARAMETER_CD = A.ACTION_CLEAR_PARAMETER_CD
		   LEFT OUTER JOIN DEVICE.DEVICE_SETTING ds_v ON ln_device_id = ds_v.DEVICE_ID AND ds_v.DEVICE_SETTING_PARAMETER_CD = 'Property List Version'
		   LEFT OUTER JOIN DEVICE.CONFIG_TEMPLATE ct
			 ON ct.CONFIG_TEMPLATE_NAME = DECODE(DBADMIN.PKG_UTL.COMPARE(DT.DEVICE_TYPE_ID, 13),
				-1, DT.DEFAULT_CONFIG_TEMPLATE_NAME, DT.DEFAULT_CONFIG_TEMPLATE_NAME || ds_v.DEVICE_SETTING_VALUE)
		   LEFT OUTER JOIN DEVICE.CONFIG_TEMPLATE_SETTING cts
			 ON cts.CONFIG_TEMPLATE_ID = ct.CONFIG_TEMPLATE_ID
			AND cts.DEVICE_SETTING_PARAMETER_CD = A.ACTION_CLEAR_PARAMETER_CD
		  WHERE CAP.CONSUMER_ACCT_ID = pn_consumer_acct_id
			AND DTA.DEVICE_TYPE_ID = ln_device_type_id
		  ORDER BY CAP.CONSUMER_ACCT_PERMISSION_ORDER
	  ) WHERE ROWNUM = 1) O
	  LEFT OUTER JOIN (PSS.PERMISSION_ACTION_PARAM PAP
	  JOIN DEVICE.ACTION_PARAM AP ON PAP.ACTION_PARAM_ID = AP.ACTION_PARAM_ID)
		ON O.PERMISSION_ACTION_ID = PAP.PERMISSION_ACTION_ID
	  JOIN DEVICE.ACTION A ON O.ACTION_ID = A.ACTION_ID
	  JOIN DEVICE.DEVICE_TYPE_ACTION DTA ON DTA.ACTION_ID = O.ACTION_ID
	 WHERE DTA.DEVICE_TYPE_ID = ln_device_type_id
	  GROUP BY A.ACTION_ID, DTA.DEVICE_TYPE_ACTION_CD, A.ACTION_CLEAR_PARAMETER_CD, A.ACTION_PARAM_TYPE_CD;
	IF lc_store_action = 'Y' THEN
		MERGE INTO PSS.LAST_DEVICE_ACTION O
		 USING (
			  SELECT lv_device_name DEVICE_NAME,
					 pn_consumer_acct_id CONSUMER_ACCT_ID,
					 pn_action_id DEVICE_ACTION_ID,
					 CURRENT_TIMESTAMP DEVICE_ACTION_UTC_TS
				FROM DUAL) N
			  ON (O.DEVICE_NAME = N.DEVICE_NAME)
			  WHEN MATCHED THEN
			   UPDATE
				  SET O.CONSUMER_ACCT_ID = N.CONSUMER_ACCT_ID,
					  O.DEVICE_ACTION_ID = N.DEVICE_ACTION_ID,
					  O.DEVICE_ACTION_UTC_TS = N.DEVICE_ACTION_UTC_TS
			  WHEN NOT MATCHED THEN
			   INSERT (O.DEVICE_NAME,
					   O.CONSUMER_ACCT_ID,
					   O.DEVICE_ACTION_ID,
					   O.DEVICE_ACTION_UTC_TS)
				VALUES(N.DEVICE_NAME,
					   N.CONSUMER_ACCT_ID,
					   N.DEVICE_ACTION_ID,
					   N.DEVICE_ACTION_UTC_TS
				);
	END IF;
EXCEPTION
	WHEN NO_DATA_FOUND THEN
		RETURN;
END;

-- R37+ Signature
PROCEDURE PERMIT_CONSUMER_ACCT(
	pn_pos_pta_id PSS.POS_PTA.POS_PTA_ID%TYPE,
	pn_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
	pt_auth_ts PSS.CONSUMER_ACCT.CONSUMER_ACCT_ACTIVATION_TS%TYPE,
	pl_consumer_acct_type_ids NUMBER_TABLE,
	pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
	pn_consumer_id OUT PSS.CONSUMER_ACCT.CONSUMER_ID%TYPE,
    pn_consumer_acct_type_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_TYPE_ID%TYPE,
    pv_consumer_acct_type_label OUT PSS.CONSUMER_ACCT_TYPE.CONSUMER_ACCT_TYPE_LABEL%TYPE,
    pb_consumer_acct_raw_hash OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_RAW_HASH%TYPE, 
    pb_consumer_acct_raw_salt OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_RAW_SALT%TYPE, 
    pb_security_cd_hash OUT PSS.CONSUMER_ACCT.SECURITY_CD_HASH%TYPE, 
    pb_security_cd_salt OUT PSS.CONSUMER_ACCT.SECURITY_CD_SALT%TYPE,
	pn_action_id OUT PSS.PERMISSION_ACTION.ACTION_ID%TYPE,
	pn_action_code OUT NUMBER,
	pn_action_bitmap OUT NUMBER)
IS
	lc_store_action CHAR(1);
	lv_device_name DEVICE.DEVICE_NAME%TYPE;
	ln_device_id DEVICE.DEVICE_ID%TYPE;
	ln_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE;
BEGIN
	SELECT CONSUMER_ACCT_ID, CONSUMER_ID, CONSUMER_ACCT_TYPE_ID, CONSUMER_ACCT_TYPE_LABEL, CONSUMER_ACCT_RAW_HASH, CONSUMER_ACCT_RAW_SALT, SECURITY_CD_HASH, SECURITY_CD_SALT, DEVICE_ID, DEVICE_TYPE_ID, DEVICE_NAME
	  INTO pn_consumer_acct_id, pn_consumer_id, pn_consumer_acct_type_id, pv_consumer_acct_type_label, pb_consumer_acct_raw_hash, pb_consumer_acct_raw_salt, pb_security_cd_hash, pb_security_cd_salt, ln_device_id, ln_device_type_id, lv_device_name
	  FROM (
		 SELECT CA.CONSUMER_ACCT_ID, CA.CONSUMER_ID, CA.CONSUMER_ACCT_TYPE_ID, CAT.CONSUMER_ACCT_TYPE_LABEL, CA.CONSUMER_ACCT_RAW_HASH, CA.CONSUMER_ACCT_RAW_SALT, CA.SECURITY_CD_HASH, CA.SECURITY_CD_SALT, D.DEVICE_ID, D.DEVICE_TYPE_ID, D.DEVICE_NAME, VLH.ANCESTOR_LOCATION_ID, MAX(CA.CONSUMER_ACCT_ISSUE_NUM) MAX_ISSUE_NUM, VLH.DEPTH
		   FROM PSS.POS_PTA PTA
		   JOIN PSS.POS POS ON PTA.POS_ID = POS.POS_ID
		   JOIN DEVICE.DEVICE D ON D.DEVICE_ID = POS.DEVICE_ID
		   JOIN LOCATION.VW_LOCATION_HIERARCHY VLH ON VLH.DESCENDENT_LOCATION_ID = POS.LOCATION_ID
		   JOIN PSS.CONSUMER_ACCT CA ON VLH.ANCESTOR_LOCATION_ID = CA.LOCATION_ID
           JOIN PSS.CONSUMER_ACCT_TYPE CAT ON CAT.CONSUMER_ACCT_TYPE_ID = CA.CONSUMER_ACCT_TYPE_ID
           JOIN PSS.CONSUMER_ACCT_BASE CAB ON CAB.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
		  WHERE PTA.POS_PTA_ID = pn_pos_pta_id
			AND CAB.GLOBAL_ACCOUNT_ID = pn_global_account_id
			AND CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y'
			AND NVL(CA.CONSUMER_ACCT_ACTIVATION_TS, MIN_DATE) <= pt_auth_ts
			AND NVL(CA.CONSUMER_ACCT_DEACTIVATION_TS, MAX_DATE) > pt_auth_ts
            AND CA.CONSUMER_ACCT_TYPE_ID MEMBER OF pl_consumer_acct_type_ids
			GROUP BY CA.CONSUMER_ACCT_ID, CA.CONSUMER_ID, CA.CONSUMER_ACCT_TYPE_ID, CAT.CONSUMER_ACCT_TYPE_LABEL, CA.CONSUMER_ACCT_RAW_HASH, CA.CONSUMER_ACCT_RAW_SALT, CA.SECURITY_CD_HASH, CA.SECURITY_CD_SALT, D.DEVICE_ID, D.DEVICE_TYPE_ID, D.DEVICE_NAME, VLH.ANCESTOR_LOCATION_ID, VLH.DEPTH
			ORDER BY VLH.DEPTH, MAX_ISSUE_NUM DESC    /* DEPTH IS ASCENDING, AS IT IS THE DIFFERENCE BETWEEN LOCATION AND ANCESTOR */
	) WHERE ROWNUM = 1;

	SELECT A.ACTION_ID, DTA.DEVICE_TYPE_ACTION_CD,
		   DECODE(A.ACTION_PARAM_TYPE_CD, 'B', SUM(POWER(2, AP.PROTOCOL_BIT_INDEX))) PROTOCOL_BITMAP,
		   DECODE(A.ACTION_CLEAR_PARAMETER_CD, NULL, 'N', 'Y') STORE_LAST_ACTION
	  INTO pn_action_id, pn_action_code, pn_action_bitmap, lc_store_action
	  FROM (SELECT * FROM (
		 SELECT CAP.PERMISSION_ACTION_ID, DTA.DEVICE_TYPE_ACTION_CD PROTOCOL_ACTION_CD,
				CASE WHEN LDA.DEVICE_ACTION_UTC_TS IS NOT NULL
						  AND CURRENT_TIMESTAMP < LDA.DEVICE_ACTION_UTC_TS
						  + NUMTODSINTERVAL(COALESCE(TO_NUMBER_OR_NULL(DS_T.DEVICE_SETTING_VALUE),
						  TO_NUMBER_OR_NULL(cts.CONFIG_TEMPLATE_SETTING_VALUE), 3600), 'SECOND') THEN 10
					 ELSE DTA.ACTION_ID END ACTION_ID
		   FROM PSS.CONSUMER_ACCT_PERMISSION CAP
		   JOIN PSS.PERMISSION_ACTION PA ON CAP.PERMISSION_ACTION_ID = PA.PERMISSION_ACTION_ID
		   JOIN DEVICE.DEVICE_TYPE_ACTION DTA ON DTA.ACTION_ID = PA.ACTION_ID
		   JOIN DEVICE.DEVICE_TYPE DT ON DTA.DEVICE_TYPE_ID = DT.DEVICE_TYPE_ID
		   LEFT OUTER JOIN PSS.LAST_DEVICE_ACTION LDA
			 ON LDA.DEVICE_NAME = lv_device_name
			AND CAP.CONSUMER_ACCT_ID = LDA.CONSUMER_ACCT_ID
			AND PA.ACTION_ID = LDA.DEVICE_ACTION_ID
		   JOIN DEVICE.ACTION A ON PA.ACTION_ID = A.ACTION_ID
		   LEFT OUTER JOIN DEVICE.DEVICE_SETTING DS_T ON ln_device_id = DS_T.DEVICE_ID AND DS_T.DEVICE_SETTING_PARAMETER_CD = A.ACTION_CLEAR_PARAMETER_CD
		   LEFT OUTER JOIN DEVICE.DEVICE_SETTING ds_v ON ln_device_id = ds_v.DEVICE_ID AND ds_v.DEVICE_SETTING_PARAMETER_CD = 'Property List Version'
		   LEFT OUTER JOIN DEVICE.CONFIG_TEMPLATE ct
			 ON ct.CONFIG_TEMPLATE_NAME = DECODE(DBADMIN.PKG_UTL.COMPARE(DT.DEVICE_TYPE_ID, 13),
				-1, DT.DEFAULT_CONFIG_TEMPLATE_NAME, DT.DEFAULT_CONFIG_TEMPLATE_NAME || ds_v.DEVICE_SETTING_VALUE)
		   LEFT OUTER JOIN DEVICE.CONFIG_TEMPLATE_SETTING cts
			 ON cts.CONFIG_TEMPLATE_ID = ct.CONFIG_TEMPLATE_ID
			AND cts.DEVICE_SETTING_PARAMETER_CD = A.ACTION_CLEAR_PARAMETER_CD
		  WHERE CAP.CONSUMER_ACCT_ID = pn_consumer_acct_id
			AND DTA.DEVICE_TYPE_ID = ln_device_type_id
		  ORDER BY CAP.CONSUMER_ACCT_PERMISSION_ORDER
	  ) WHERE ROWNUM = 1) O
	  LEFT OUTER JOIN (PSS.PERMISSION_ACTION_PARAM PAP
	  JOIN DEVICE.ACTION_PARAM AP ON PAP.ACTION_PARAM_ID = AP.ACTION_PARAM_ID)
		ON O.PERMISSION_ACTION_ID = PAP.PERMISSION_ACTION_ID
	  JOIN DEVICE.ACTION A ON O.ACTION_ID = A.ACTION_ID
	  JOIN DEVICE.DEVICE_TYPE_ACTION DTA ON DTA.ACTION_ID = O.ACTION_ID
	 WHERE DTA.DEVICE_TYPE_ID = ln_device_type_id
	  GROUP BY A.ACTION_ID, DTA.DEVICE_TYPE_ACTION_CD, A.ACTION_CLEAR_PARAMETER_CD, A.ACTION_PARAM_TYPE_CD;
	IF lc_store_action = 'Y' THEN
		MERGE INTO PSS.LAST_DEVICE_ACTION O
		 USING (
			  SELECT lv_device_name DEVICE_NAME,
					 pn_consumer_acct_id CONSUMER_ACCT_ID,
					 pn_action_id DEVICE_ACTION_ID,
					 CURRENT_TIMESTAMP DEVICE_ACTION_UTC_TS
				FROM DUAL) N
			  ON (O.DEVICE_NAME = N.DEVICE_NAME)
			  WHEN MATCHED THEN
			   UPDATE
				  SET O.CONSUMER_ACCT_ID = N.CONSUMER_ACCT_ID,
					  O.DEVICE_ACTION_ID = N.DEVICE_ACTION_ID,
					  O.DEVICE_ACTION_UTC_TS = N.DEVICE_ACTION_UTC_TS
			  WHEN NOT MATCHED THEN
			   INSERT (O.DEVICE_NAME,
					   O.CONSUMER_ACCT_ID,
					   O.DEVICE_ACTION_ID,
					   O.DEVICE_ACTION_UTC_TS)
				VALUES(N.DEVICE_NAME,
					   N.CONSUMER_ACCT_ID,
					   N.DEVICE_ACTION_ID,
					   N.DEVICE_ACTION_UTC_TS
				);
	END IF;
EXCEPTION
	WHEN NO_DATA_FOUND THEN
		RETURN;
END;

    -- R37+ signature
    PROCEDURE SP_CREATE_LOCAL_AUTH_SALE(
       pc_global_event_cd_prefix IN CHAR,
       pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
       pn_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE,
       pv_device_tran_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE,
       pn_sale_utc_ts_ms NUMBER,
       pn_sale_utc_offset_min IN pss.sale.sale_utc_offset_min%TYPE,
       pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
       pc_entry_method CHAR,
       pc_payment_type CHAR,
       pv_track_data PSS.TRAN.TRAN_RECEIVED_RAW_ACCT_DATA%TYPE,
       pn_consumer_acct_id PSS.TRAN.CONSUMER_ACCT_ID%TYPE,
       pd_auth_ts PSS.AUTH.AUTH_TS%TYPE,
       pn_trace_number PSS.AUTH.TRACE_NUMBER%TYPE,
       pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
       pn_auth_amt PSS.AUTH.AUTH_AMT%TYPE,
       pn_device_batch_id IN pss.sale.device_batch_id%TYPE,
       pc_sale_type_cd IN pss.sale.sale_type_cd%TYPE,
       pv_tran_device_result_type_cd IN pss.tran.tran_device_result_type_cd%TYPE,
       pn_sale_result_id IN pss.sale.sale_result_id%TYPE,
       pv_hash_type_cd IN pss.sale.hash_type_cd%TYPE,
       pv_tran_line_item_hash IN pss.sale.tran_line_item_hash%TYPE,
       pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
       pn_result_cd OUT NUMBER,
       pv_error_message OUT VARCHAR2
    ) IS
       ln_auth_id PSS.AUTH.AUTH_ID%TYPE;
       lv_global_trans_cd pss.tran.tran_global_trans_cd%TYPE;
       lv_global_trans_cd_app_layer pss.tran.tran_global_trans_cd%TYPE;
       lv_global_trans_cd_legacy pss.tran.tran_global_trans_cd%TYPE;
       ld_tran_upload_ts pss.tran.tran_upload_ts%TYPE;
       ln_tran_exists NUMBER := PKG_CONST.BOOLEAN__FALSE;
       ln_tli_hash_match NUMBER;
       ld_current_ts DATE := SYSDATE;
       ld_tran_start_ts pss.tran.tran_start_ts%TYPE;
       lt_sale_start_utc_ts pss.sale.sale_start_utc_ts%TYPE;
       lv_last_lock_utc_ts VARCHAR2(128);
       ln_insert_tran NUMBER := PKG_CONST.BOOLEAN__FALSE;
       lv_tran_state_cd pss.tran.tran_state_cd%TYPE := PKG_CONST.TRAN_STATE__COMPLETE_ERROR;
    BEGIN
        IF pc_global_event_cd_prefix NOT IN (PKG_CONST.EVENT_CODE_PREFIX__APP_LAYER,
            PKG_CONST.EVENT_CODE_PREFIX__LEGACY) THEN
            pn_result_cd := PKG_CONST.RESULT__INVALID_PARAMETER;
            pv_error_message := 'Invalid pc_global_event_cd_prefix: ' || pc_global_event_cd_prefix;
            RETURN;
        END IF;
    
        lv_global_trans_cd := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(pc_global_event_cd_prefix, pv_device_name, pv_device_tran_cd);
        lv_global_trans_cd_app_layer := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(PKG_CONST.EVENT_CODE_PREFIX__APP_LAYER, pv_device_name, pv_device_tran_cd);
        lv_global_trans_cd_legacy := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(PKG_CONST.EVENT_CODE_PREFIX__LEGACY, pv_device_name, pv_device_tran_cd);
        ld_tran_start_ts := CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_sale_utc_ts_ms + pn_sale_utc_offset_min * 60 * 1000) AS DATE);
        lt_sale_start_utc_ts := CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_sale_utc_ts_ms) AS DATE);
        lv_last_lock_utc_ts := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', pv_device_name || ':' || pv_device_tran_cd);
        BEGIN
            SELECT tran_id, tran_upload_ts, PKG_CONST.BOOLEAN__TRUE, tli_hash_match
            INTO pn_tran_id, ld_tran_upload_ts, ln_tran_exists, ln_tli_hash_match
            FROM
            (
                SELECT /*+ INDEX(t IDX_DEVICE_TRAN_CD) */ t.tran_id, t.tran_upload_ts,
                    CASE WHEN s.hash_type_cd = pv_hash_type_cd
                        AND s.tran_line_item_hash = pv_tran_line_item_hash
                        AND s.sale_type_cd = pc_sale_type_cd THEN PKG_CONST.BOOLEAN__TRUE
                    ELSE PKG_CONST.BOOLEAN__FALSE END AS tli_hash_match
                FROM pss.tran t
                LEFT OUTER JOIN pss.sale s ON t.tran_id = s.tran_id
                WHERE t.tran_device_tran_cd = pv_device_tran_cd AND (
                    t.tran_global_trans_cd IN (lv_global_trans_cd_app_layer, lv_global_trans_cd_legacy)
                    OR t.tran_global_trans_cd LIKE lv_global_trans_cd_app_layer || ':%'
                    OR t.tran_global_trans_cd LIKE lv_global_trans_cd_legacy || ':%'
                )
                ORDER BY CASE WHEN t.tran_global_trans_cd IN (lv_global_trans_cd_app_layer, lv_global_trans_cd_legacy) THEN 1 ELSE 2 END,
                    CASE WHEN s.sale_type_cd = pc_sale_type_cd THEN 1 ELSE 2 END,
                    tli_hash_match DESC, t.tran_start_ts, t.created_ts
            )
            WHERE ROWNUM = 1;
    
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                ln_insert_tran := PKG_CONST.BOOLEAN__TRUE;
        END;
    
        IF ln_tran_exists = PKG_CONST.BOOLEAN__TRUE AND ld_tran_upload_ts IS NOT NULL THEN
            IF ln_tli_hash_match = PKG_CONST.BOOLEAN__TRUE THEN
                UPDATE pss.sale
                SET duplicate_count = duplicate_count + 1
                WHERE tran_id = pn_tran_id;
    
                pn_result_cd := PKG_CONST.RESULT__DUPLICATE;
                pv_error_message := 'Duplicate sale, original tran_id: ' || pn_tran_id;
                pn_tran_id := 0;
                RETURN;
            END IF;
        
            ln_insert_tran := PKG_CONST.BOOLEAN__TRUE;
        END IF;
    
        IF ln_insert_tran = PKG_CONST.BOOLEAN__TRUE THEN
            IF pc_sale_type_cd = PKG_CONST.SALE_TYPE__CASH THEN
                lv_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE;
            ELSIF pv_tran_device_result_type_cd IN (PKG_CONST.TRAN_DEV_RES__CANCELLED,
                    PKG_CONST.TRAN_DEV_RES__AUTH_FAILURE,
                    PKG_CONST.TRAN_DEV_RES__FAILURE,
                    PKG_CONST.TRAN_DEV_RES__TIMEOUT) OR pn_auth_amt <= 0 THEN
                lv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED;
            END IF;
            
            SELECT PSS.SEQ_TRAN_ID.NEXTVAL
            INTO pn_tran_id
            FROM DUAL;
    
            IF ln_tran_exists = PKG_CONST.BOOLEAN__TRUE THEN
                lv_global_trans_cd := lv_global_trans_cd || ':' || pn_tran_id;
            END IF;		
    
            INSERT INTO PSS.TRAN (
                TRAN_ID,
                TRAN_START_TS,
                TRAN_END_TS,
                TRAN_UPLOAD_TS,
                TRAN_STATE_CD,
                TRAN_DEVICE_TRAN_CD,
                TRAN_RECEIVED_RAW_ACCT_DATA,
                POS_PTA_ID,
                TRAN_GLOBAL_TRANS_CD,
                CONSUMER_ACCT_ID,
                TRAN_DEVICE_RESULT_TYPE_CD,
                PAYMENT_SUBTYPE_KEY_ID,
                PAYMENT_SUBTYPE_CLASS,
                CLIENT_PAYMENT_TYPE_CD,
                DEVICE_NAME)
            SELECT
                pn_tran_id, /* TRAN_ID */
                ld_tran_start_ts,  /* TRAN_START_TS */
                ld_tran_start_ts, /* TRAN_END_TS */
                ld_current_ts,
                lv_tran_state_cd,  /* TRAN_STATE_CD */
                pv_device_tran_cd,  /* TRAN_DEVICE_TRAN_CD */
                pv_track_data,  /* TRAN_RECEIVED_RAW_ACCT_DATA */
                pn_pos_pta_id, /* POS_PTA_ID */
                lv_global_trans_cd, /* TRAN_GLOBAL_TRANS_CD */
                pn_consumer_acct_id,  /* CONSUMER_ACCT_ID */
                pv_tran_device_result_type_cd,
                pp.payment_subtype_key_id,
                ps.payment_subtype_class,
                ps.client_payment_type_cd,
                pv_device_name
            FROM pss.pos_pta pp
            JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
            WHERE pp.pos_pta_id = pn_pos_pta_id;
    
            IF pc_sale_type_cd != PKG_CONST.SALE_TYPE__CASH THEN
            SELECT PSS.SEQ_AUTH_ID.NEXTVAL
              INTO ln_auth_id
              FROM DUAL;
            INSERT INTO PSS.AUTH (
                AUTH_ID,
                TRAN_ID,
                AUTH_STATE_ID,
                AUTH_TYPE_CD,
                AUTH_PARSED_ACCT_DATA,
                ACCT_ENTRY_METHOD_CD,
                AUTH_TS,
                AUTH_RESULT_CD,
                AUTH_RESP_CD,
                AUTH_RESP_DESC,
                AUTH_AUTHORITY_TS,
                AUTH_AMT,
                AUTH_AUTHORITY_AMT_RQST,
                TRACE_NUMBER)
             VALUES(
                ln_auth_id, /* AUTH_ID */
                pn_tran_id, /* TRAN_ID */
                DECODE(pc_auth_result_cd, 'Y', 2, 'N', 3, 'P', 5, 'O', 7, 'F', 4, 'R', 7), /* AUTH_STATE_ID */
                'L', /* AUTH_TYPE_CD */
                pv_track_data, /* AUTH_PARSED_ACCT_DATA */
                DECODE(pc_entry_method, 'B', 7, 'C', 6, 'M', 2, 'S', 3, 'I', 8, 1), /* ACCT_ENTRY_METHOD_CD */
                pd_auth_ts, /* AUTH_TS */
                pc_auth_result_cd, /* AUTH_RESULT_CD */
                'LOCAL', /* AUTH_RESP_CD */
                'Local authorization not accepted', /* AUTH_RESP_DESC */
                pd_auth_ts, /* AUTH_AUTHORITY_TS */
                NVL(pn_auth_amt / pn_minor_currency_factor, 0), /* AUTH_AMT */
                pn_auth_amt / pn_minor_currency_factor, /* AUTH_AUTHORITY_AMT_RQST */
                pn_trace_number /* TRACE_NUMBER */
                );
            END IF;
    
            INSERT INTO pss.sale (
                tran_id,
                device_batch_id,
                sale_type_cd,
                sale_start_utc_ts,
                sale_end_utc_ts,
                sale_utc_offset_min,
                sale_result_id,
                sale_amount,
                receipt_result_cd,
                hash_type_cd,
                tran_line_item_hash
            ) VALUES (
                pn_tran_id,
                pn_device_batch_id,
                pc_sale_type_cd,
                lt_sale_start_utc_ts,
                lt_sale_start_utc_ts,
                pn_sale_utc_offset_min,
                pn_sale_result_id,
                pn_auth_amt / pn_minor_currency_factor,
                'U',
                pv_hash_type_cd,
                pv_tran_line_item_hash
            );
        END IF;
    
        pn_result_cd := PKG_CONST.RESULT__SUCCESS;
        pv_error_message := PKG_CONST.ERROR__NO_ERROR;
    END;

    -- R33 - R36 signature
    PROCEDURE SP_CREATE_LOCAL_AUTH_SALE(
       pc_global_event_cd_prefix IN CHAR,
       pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
       pn_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE,
       pv_device_tran_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE,
       pn_sale_utc_ts_ms NUMBER,
       pn_sale_utc_offset_min IN pss.sale.sale_utc_offset_min%TYPE,
       pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
       pc_entry_method CHAR,
       pc_payment_type CHAR,
       pv_track_data PSS.TRAN.TRAN_RECEIVED_RAW_ACCT_DATA%TYPE,
       pv_pan_sha1 VARCHAR2,
       pn_consumer_acct_id PSS.TRAN.CONSUMER_ACCT_ID%TYPE,
       pd_auth_ts PSS.AUTH.AUTH_TS%TYPE,
       pn_trace_number PSS.AUTH.TRACE_NUMBER%TYPE,
       pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
       pn_auth_amt PSS.AUTH.AUTH_AMT%TYPE,
       pn_device_batch_id IN pss.sale.device_batch_id%TYPE,
       pc_sale_type_cd IN pss.sale.sale_type_cd%TYPE,
       pv_tran_device_result_type_cd IN pss.tran.tran_device_result_type_cd%TYPE,
       pn_sale_result_id IN pss.sale.sale_result_id%TYPE,
       pv_hash_type_cd IN pss.sale.hash_type_cd%TYPE,
       pv_tran_line_item_hash IN pss.sale.tran_line_item_hash%TYPE,
       pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
       pn_result_cd OUT NUMBER,
       pv_error_message OUT VARCHAR2
    ) IS
    BEGIN
        SP_CREATE_LOCAL_AUTH_SALE(
           pc_global_event_cd_prefix,
           pv_device_name,
           pn_pos_pta_id,
           pv_device_tran_cd,
           pn_sale_utc_ts_ms,
           pn_sale_utc_offset_min,
           pc_auth_result_cd,
           pc_entry_method,
           pc_payment_type,
           pv_track_data,
           pn_consumer_acct_id,
           pd_auth_ts,
           pn_trace_number,
           pn_minor_currency_factor,
           pn_auth_amt,
           pn_device_batch_id,
           pc_sale_type_cd,
           pv_tran_device_result_type_cd,
           pn_sale_result_id,
           pv_hash_type_cd,
           pv_tran_line_item_hash,
           pn_tran_id,
           pn_result_cd,
           pv_error_message);
    END;

	PROCEDURE SETUP_REPLENISH_CONSUMER_ACCT(
	   pn_replenish_id PSS.CONSUMER_ACCT_PEND_REPLENISH.CONSUMER_ACCT_REPLENISH_ID%TYPE,
	   pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
	   lc_always_auth_flag CHAR,
	   pv_replenish_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
	   pn_balance PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
	   pn_replenish_threshhold PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_THRESHHOLD%TYPE,
	   pn_replenish_amount IN OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_AMOUNT%TYPE,
	   pc_replenish_flag OUT VARCHAR2,
	   pn_replenish_next_master_id OUT NUMBER,
	   pv_replenish_device_name OUT DEVICE.DEVICE_NAME%TYPE)
	IS
	   ln_balance PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE;
	   ln_pending_amount PSS.CONSUMER_ACCT_PEND_REPLENISH.AMOUNT%TYPE;
	   ln_auth_hold_total PSS.AUTH.AUTH_AMT_APPROVED%TYPE;
	BEGIN
		SELECT NVL(SUM(A.AUTH_AMT_APPROVED), 0)
		  INTO ln_auth_hold_total
		  FROM PSS.CONSUMER_ACCT_AUTH_HOLD CAAH
		  JOIN PSS.AUTH A ON CAAH.AUTH_ID = A.AUTH_ID
		 WHERE CAAH.CONSUMER_ACCT_ID = pn_consumer_acct_id
		   AND CAAH.EXPIRATION_TS > SYSDATE
		   AND CAAH.CLEARED_YN_FLAG = 'N';
		SELECT NVL(SUM(AMOUNT), 0)
		  INTO ln_pending_amount
		  FROM PSS.CONSUMER_ACCT_PEND_REPLENISH
		 WHERE CONSUMER_ACCT_REPLENISH_ID = pn_replenish_id
		   AND EXPIRATION_TS > SYSDATE
		   AND (SUBMITTED_FLAG = 'Y' OR (SUBMITTED_FLAG = '?' AND CREATED_UTC_TS > SYS_EXTRACT_UTC(SYSTIMESTAMP) - (1/24/60)));
		ln_balance := pn_balance + ln_pending_amount - ln_auth_hold_total;   
		IF lc_always_auth_flag = 'Y' OR pn_replenish_threshhold IS NULL OR ln_balance < pn_replenish_threshhold THEN
			IF pn_replenish_threshhold IS NOT NULL THEN
				IF pn_replenish_amount + ln_balance < pn_replenish_threshhold THEN
					pn_replenish_amount := pn_replenish_threshhold - ln_balance;
				ELSIF ln_balance >= pn_replenish_threshhold AND lc_always_auth_flag = 'Y' THEN
					pn_replenish_amount := 0;
				END IF;
			END IF;
			PKG_DEVICE_CONFIGURATION.NEXT_MASTER_ID_BY_SERIAL(pv_replenish_device_serial_cd, pv_replenish_device_name, pn_replenish_next_master_id);
			INSERT INTO PSS.CONSUMER_ACCT_PEND_REPLENISH(CONSUMER_ACCT_REPLENISH_ID, DEVICE_NAME, DEVICE_TRAN_CD, AMOUNT, EXPIRATION_TS)
				VALUES(pn_replenish_id, pv_replenish_device_name, pn_replenish_next_master_id, pn_replenish_amount, SYSDATE + 7);
			pc_replenish_flag := 'Y';
		ELSE
			pc_replenish_flag := 'N';       
		END IF;
	END;
    
	-- R37 and above
    PROCEDURE DEBIT_CONSUMER_ACCT(
	   pn_amount PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
	   pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
	   pc_redelivery_flag CHAR,
	   pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
	   pc_auth_result_cd OUT VARCHAR2,
	   pn_debitted_amount OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
	   pn_new_balance OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
	   pc_auto_replenish_flag OUT VARCHAR2,
       pn_used_cash_back_balance OUT PSS.CONSUMER_ACCT.TOWARD_CASH_BACK_BALANCE%TYPE,
	   pn_cash_back_amount OUT PSS.SALE.SALE_AMOUNT%TYPE)
	IS
	   ln_auth_id PSS.AUTH.AUTH_ID%TYPE;
	   ln_auth_amount PSS.AUTH.AUTH_AMT_APPROVED%TYPE;
	   ln_auth_hold_used PSS.AUTH.AUTH_HOLD_USED%TYPE;
	   ld_tran_start_ts PSS.TRAN.TRAN_START_TS%TYPE;
	   ln_loyalty_discount PSS.CONSUMER_ACCT.LOYALTY_DISCOUNT_TOTAL%TYPE;
	   ln_consumer_acct_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_TYPE_ID%TYPE;
       ln_consumer_acct_sub_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_SUB_TYPE_ID%TYPE;
	   ln_cash_back_percent REPORT.CAMPAIGN.DISCOUNT_PERCENT%TYPE;
	   ln_cash_back_threshhold REPORT.CAMPAIGN.THRESHOLD_AMOUNT%TYPE;
	   ln_cash_back_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE;
       ln_trans_type_id PSS.PAYMENT_SUBTYPE.TRANS_TYPE_ID%TYPE;
	   ln_operator_trans_type_id REPORT.TRANS_TYPE.OPERATOR_TRANS_TYPE_ID%TYPE;
       lv_lock VARCHAR2(128);
	BEGIN
		SELECT ca.CONSUMER_ACCT_ID, A.AUTH_HOLD_USED, A.AUTH_ID, X.TRAN_START_TS, A.AUTH_AMT_APPROVED, NVL(SUM(ABS(XI.TRAN_LINE_ITEM_AMOUNT * XI.TRAN_LINE_ITEM_QUANTITY)), 0), 
			   CASE WHEN CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y' AND CA.CONSUMER_ACCT_TYPE_ID = 3 THEN 'Y' ELSE 'N' END, CA.CONSUMER_ACCT_TYPE_ID, PST.TRANS_TYPE_ID, TT.OPERATOR_TRANS_TYPE_ID
		  INTO pn_consumer_acct_id, ln_auth_hold_used, ln_auth_id, ld_tran_start_ts, ln_auth_amount, ln_loyalty_discount, pc_auto_replenish_flag, ln_consumer_acct_type_id, ln_trans_type_id, ln_operator_trans_type_id
		  FROM PSS.CONSUMER_ACCT ca
		  JOIN PSS.TRAN X ON X.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
		  JOIN PSS.AUTH A ON X.TRAN_ID = A.TRAN_ID AND A.AUTH_TYPE_CD = 'N' AND A.AUTH_RESULT_CD IN('Y', 'P')
          JOIN PSS.POS_PTA PP ON X.POS_PTA_ID = PP.POS_PTA_ID
          JOIN PSS.PAYMENT_SUBTYPE PST ON PP.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
		  JOIN REPORT.TRANS_TYPE TT ON PST.TRANS_TYPE_ID = TT.TRANS_TYPE_ID
          LEFT OUTER JOIN PSS.TRAN_LINE_ITEM XI ON X.TRAN_ID = XI.TRAN_ID AND XI.TRAN_LINE_ITEM_TYPE_ID = 204
		 WHERE X.TRAN_ID = pn_tran_id
		 GROUP BY ca.CONSUMER_ACCT_ID, A.AUTH_HOLD_USED, A.AUTH_ID, X.TRAN_START_TS, A.AUTH_AMT_APPROVED, CA.CONSUMER_ACCT_ACTIVE_YN_FLAG, CA.CONSUMER_ACCT_TYPE_ID, PST.TRANS_TYPE_ID, TT.OPERATOR_TRANS_TYPE_ID;
		IF pc_auto_replenish_flag = 'Y' THEN
			SELECT NVL(MAX(REPLENISH_FLAG), 'N')
			  INTO pc_auto_replenish_flag
			  FROM (
				SELECT CASE WHEN CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y' 
							 AND CA.CONSUMER_ACCT_TYPE_ID = 3 
							 AND CAR.REPLENISH_THRESHHOLD > 0
							 AND CAR.REPLENISH_AMOUNT > 0 
							 AND CAR.REPLENISH_CARD_KEY IS NOT NULL THEN 'Y' ELSE 'N' END REPLENISH_FLAG
				  FROM PSS.CONSUMER_ACCT CA
				  JOIN PSS.CONSUMER_ACCT_REPLENISH CAR ON CA.CONSUMER_ACCT_ID = CAR.CONSUMER_ACCT_ID
				 WHERE ca.CONSUMER_ACCT_ID = pn_consumer_acct_id
				   AND CAR.REPLENISH_TYPE_ID = 1
				 ORDER BY CAR.PRIORITY, CAR.CONSUMER_ACCT_REPLENISH_ID)
			 WHERE ROWNUM = 1;    
		END IF;
		IF ln_auth_hold_used = 'Y' THEN
			IF ln_consumer_acct_type_id IN(3) THEN
				SELECT MAX(DISCOUNT_PERCENT), MAX(THRESHOLD_AMOUNT), MAX(CAMPAIGN_ID)
				  INTO ln_cash_back_percent, ln_cash_back_threshhold, ln_cash_back_campaign_id
				  FROM (SELECT C.DISCOUNT_PERCENT, C.THRESHOLD_AMOUNT, C.CAMPAIGN_ID
				  FROM REPORT.CAMPAIGN C
				  JOIN PSS.CAMPAIGN_CONSUMER_ACCT CCA on C.CAMPAIGN_ID = CCA.CAMPAIGN_ID
				 WHERE CCA.CONSUMER_ACCT_ID = pn_consumer_acct_id
				   AND ld_tran_start_ts BETWEEN NVL(C.START_DATE, MIN_DATE) AND NVL(C.END_DATE, MAX_DATE)
				   AND C.CAMPAIGN_TYPE_ID = 3 /* Spend reward - Cash back */
				   AND C.DISCOUNT_PERCENT > 0
				   AND C.DISCOUNT_PERCENT < 1
				   AND (TRIM(C.RECUR_SCHEDULE) IS NULL OR REPORT.MATCH_CAMPAIGN_RECUR_SCHEDULE(C.RECUR_SCHEDULE, ld_tran_start_ts) = 'Y')
				 ORDER BY C.DISCOUNT_PERCENT DESC, C.CAMPAIGN_ID DESC)
				 WHERE ROWNUM = 1;
			END IF;
			lv_lock := PKG_GLOBAL.REQUEST_LOCK('PSS.CONSUMER_ACCT', pn_consumer_acct_id);
			DELETE 
			  FROM PSS.CONSUMER_ACCT_AUTH_HOLD
			 WHERE AUTH_ID = ln_auth_id
			   AND EXPIRATION_TS > SYSDATE 
			   AND CLEARED_YN_FLAG = 'N';
			IF SQL%FOUND THEN
				-- debit CONSUMER_ACCT_PROMO_BALANCE first and then CONSUMER_ACCT_REPLEN_BALANCE
				UPDATE PSS.CONSUMER_ACCT
				   SET CONSUMER_ACCT_BALANCE = CONSUMER_ACCT_BALANCE - pn_amount,
					   LOYALTY_DISCOUNT_TOTAL = NVL(LOYALTY_DISCOUNT_TOTAL, 0) + ln_loyalty_discount,
					   CONSUMER_ACCT_PROMO_BALANCE = CONSUMER_ACCT_PROMO_BALANCE -
							CASE WHEN CONSUMER_ACCT_PROMO_BALANCE > 0 AND CONSUMER_ACCT_PROMO_BALANCE >= pn_amount THEN pn_amount ELSE CONSUMER_ACCT_PROMO_BALANCE END,
					   CONSUMER_ACCT_REPLEN_BALANCE = CONSUMER_ACCT_REPLEN_BALANCE - 
							CASE WHEN CONSUMER_ACCT_PROMO_BALANCE > 0 AND CONSUMER_ACCT_PROMO_BALANCE >= pn_amount THEN 0
								WHEN CONSUMER_ACCT_REPLEN_BALANCE > 0 AND CONSUMER_ACCT_REPLEN_BALANCE >= pn_amount - CONSUMER_ACCT_PROMO_BALANCE THEN pn_amount - CONSUMER_ACCT_PROMO_BALANCE
								ELSE CONSUMER_ACCT_REPLEN_BALANCE END
				 WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id
				  RETURNING CONSUMER_ACCT_BALANCE, CONSUMER_ACCT_SUB_TYPE_ID 
                  INTO pn_new_balance, ln_consumer_acct_sub_type_id;
				IF pn_new_balance < 0 THEN
					IF ln_auth_amount < pn_amount THEN
						RAISE_APPLICATION_ERROR(-20113, 'Sale amount (' || pn_amount || ') is greater than auth amount (' || ln_auth_amount || ') for transaction ' || pn_tran_id);
					END IF;
					RAISE_APPLICATION_ERROR(-20112, 'Insufficient Funds (' || pn_new_balance || ')'); 
				END IF;
                pc_auth_result_cd := 'Y';
                UPDATE PSS.AUTH SA 
                   SET AUTH_STATE_ID = 2,
                       AUTH_RESULT_CD = pc_auth_result_cd,
                       OVERRIDE_TRANS_TYPE_ID = CASE WHEN ln_consumer_acct_sub_type_id = 2 THEN ln_operator_trans_type_id ELSE ln_trans_type_id END
                 WHERE SA.TRAN_ID = pn_tran_id
                   AND SA.AUTH_TYPE_CD IN('U', 'C')
                   AND SA.AUTH_STATE_ID = 6;
                IF SQL%ROWCOUNT != 1 THEN
                    RAISE_APPLICATION_ERROR(-20116, 'Auth Hold Inconsistent State for transaction ' || pn_tran_id);
                END IF;
				IF ln_cash_back_campaign_id IS NOT NULL AND ln_cash_back_percent > 0 THEN
                    DECLARE
                        ln_cash_back_balance PSS.CONSUMER_ACCT.TOWARD_CASH_BACK_BALANCE%TYPE;
						lv_cash_back_device_name DEVICE.DEVICE_NAME%TYPE;
                        ln_cash_back_next_master_id NUMBER;
                        lv_currency_cd PSS.CONSUMER_ACCT.CURRENCY_CD%TYPE;
                        ln_result_cd NUMBER;
                        lv_error_message VARCHAR2(4000);
                        ln_cash_back_tran_id PSS.TRAN.TRAN_ID%TYPE;
                        lv_cash_back_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
                        ln_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE;
                        lc_currency_symbol CORP.CURRENCY.CURRENCY_SYMBOL%TYPE;
                        ln_host_id HOST.HOST_ID%TYPE;
                        ld_cash_back_ts DATE;
                        ld_cash_back_time NUMBER;
                        ln_corp_customer_id PSS.CONSUMER_ACCT.CORP_CUSTOMER_ID%TYPE;
                        lv_consumer_acct_cd PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD%TYPE;
                        lv_consumer_acct_identifier PSS.CONSUMER_ACCT.CONSUMER_ACCT_IDENTIFIER%TYPE;
                        lv_desc PSS.TRAN_LINE_ITEM.TRAN_LINE_ITEM_DESC%TYPE;
                        ln_doc_id CORP.DOC.DOC_ID%TYPE;
                        ln_ledger_id CORP.LEDGER.LEDGER_ID%TYPE;                        
                    BEGIN
					    -- update consumer acct
                        UPDATE PSS.CONSUMER_ACCT
                           SET TOWARD_CASH_BACK_BALANCE = NVL(TOWARD_CASH_BACK_BALANCE, 0) + pn_amount
                         WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id
                         RETURNING TOWARD_CASH_BACK_BALANCE, CURRENCY_CD, CORP_CUSTOMER_ID, CONSUMER_ACCT_CD, CONSUMER_ACCT_IDENTIFIER
                          INTO ln_cash_back_balance, lv_currency_cd, ln_corp_customer_id, lv_consumer_acct_cd, lv_consumer_acct_identifier;
                        IF ln_cash_back_balance >= ln_cash_back_threshhold THEN 
                            pn_used_cash_back_balance := TRUNC(ln_cash_back_balance / ln_cash_back_threshhold) * ln_cash_back_threshhold;
                            pn_cash_back_amount := ROUND(pn_used_cash_back_balance * ln_cash_back_percent, 2);
                            UPDATE PSS.CONSUMER_ACCT
                               SET TOWARD_CASH_BACK_BALANCE = TOWARD_CASH_BACK_BALANCE - pn_used_cash_back_balance,
                                   CASH_BACK_TOTAL = NVL(CASH_BACK_TOTAL, 0) + pn_cash_back_amount,
                                   CONSUMER_ACCT_BALANCE = CONSUMER_ACCT_BALANCE + pn_cash_back_amount,
								   CONSUMER_ACCT_PROMO_BALANCE = CONSUMER_ACCT_PROMO_BALANCE + pn_cash_back_amount,
                                   CONSUMER_ACCT_PROMO_TOTAL = CONSUMER_ACCT_PROMO_TOTAL + pn_cash_back_amount
                             WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id;                         
                            -- add trans to virtual terminal
                            SELECT C.MINOR_CURRENCY_FACTOR, NVL(CC.CURRENCY_SYMBOL, '$'), DBADMIN.TIMESTAMP_TO_MILLIS(SYS_EXTRACT_UTC(SYSTIMESTAMP)), SYSDATE
                              INTO ln_minor_currency_factor, lc_currency_symbol, ld_cash_back_time, ld_cash_back_ts
                              FROM PSS.CURRENCY C
                              LEFT OUTER JOIN CORP.CURRENCY CC ON C.CURRENCY_CD = CC.CURRENCY_CODE
                             WHERE C.CURRENCY_CD = lv_currency_cd;
                             
							PKG_DEVICE_CONFIGURATION.NEXT_MASTER_ID_BY_SERIAL('V1-' || ln_corp_customer_id || '-' || lv_currency_cd, lv_cash_back_device_name, ln_cash_back_next_master_id);
                            SP_CREATE_SALE('A', lv_cash_back_device_name, ln_cash_back_next_master_id,  0, 'C', ld_cash_back_time, 
                                DBADMIN.PKG_UTL.TIME_ZONE_DIFF_MIN(PKG_CONST.DB_TIME_ZONE, PKG_CONST.GMT_TIME_ZONE), 'S', 0, pn_cash_back_amount * ln_minor_currency_factor, 'U', 'A', 'SHA1', 
                                DBADMIN.HASH_CARD('Bonus Cash on ' || pn_consumer_acct_id || ' of ' || pn_cash_back_amount),
                                NULL, ln_result_cd, lv_error_message, ln_cash_back_tran_id, lv_cash_back_tran_state_cd);
                            IF ln_result_cd != PKG_CONST.RESULT__SUCCESS THEN
                                RAISE_APPLICATION_ERROR(-20118, 'Could not create cash back transaction: ' || lv_error_message);
                            END IF;
                            SELECT HOST_ID, 'Bonus Cash for ' || lc_currency_symbol || TO_NUMBER(pn_used_cash_back_balance, 'FM9,999,999.00') || ' in purchases'
                              INTO ln_host_id, lv_desc
                              FROM (SELECT H.HOST_ID
                                      FROM DEVICE.HOST H
                                      JOIN DEVICE.DEVICE D ON H.DEVICE_ID = D.DEVICE_ID
                                     WHERE D.DEVICE_ACTIVE_YN_FLAG = 'Y'
                                       AND H.HOST_PORT_NUM IN(0,1)
                                       AND D.DEVICE_NAME = lv_cash_back_device_name
                                       AND H.HOST_ACTIVE_YN_FLAG = 'Y'
                                     ORDER BY H.HOST_PORT_NUM DESC, H.HOST_ID DESC) 
                             WHERE ROWNUM = 1;
                            INSERT INTO PSS.TRAN_LINE_ITEM(
                                TRAN_ID,
                                TRAN_LINE_ITEM_AMOUNT,
                                TRAN_LINE_ITEM_POSITION_CD,
                                TRAN_LINE_ITEM_TAX,
                                TRAN_LINE_ITEM_TYPE_ID,
                                TRAN_LINE_ITEM_QUANTITY,
                                TRAN_LINE_ITEM_DESC,
                                HOST_ID,
                                TRAN_LINE_ITEM_BATCH_TYPE_CD,
                                TRAN_LINE_ITEM_TS,
                                SALE_RESULT_ID,
                                APPLY_TO_CONSUMER_ACCT_ID,
                                CAMPAIGN_ID)
                            SELECT
                                ln_cash_back_tran_id,
                                pn_used_cash_back_balance,
                                NULL,
                                NULL,
                                554,
                                ln_cash_back_percent,
                                lv_desc,
                                ln_host_id,
                                'A',
                                ld_cash_back_ts,
                                0,
                                pn_consumer_acct_id,
                                ln_cash_back_campaign_id
                            FROM DUAL;
                            UPDATE PSS.TRAN
                               SET PARENT_TRAN_ID = pn_tran_id
                             WHERE TRAN_ID = ln_cash_back_tran_id;
							IF ln_consumer_acct_sub_type_id = 1 THEN
								-- add adjustment to ledger
								CORP.PAYMENTS_PKG.ADJUSTMENT_INS(ln_corp_customer_id, 'Bonus Cash Processing', lv_currency_cd, lv_desc || ', card # ' || lv_consumer_acct_cd || ', card ID ' || lv_consumer_acct_identifier,
									-pn_cash_back_amount, ln_doc_id, ln_ledger_id);
							END IF;
                        END IF; 
                    END;
				END IF;
			ELSE
                SELECT MAX(AUTH_RESULT_CD)
                  INTO pc_auth_result_cd
                  FROM PSS.AUTH SA 
                 WHERE SA.TRAN_ID = pn_tran_id
                   AND SA.AUTH_TYPE_CD IN('U', 'C')
                   AND SA.AUTH_STATE_ID = 2;
                IF pc_auth_result_cd IS NULL THEN
                    DECLARE
                        ld_expiration_ts PSS.CONSUMER_ACCT_AUTH_HOLD.EXPIRATION_TS%TYPE;
                        lc_cleared_flag PSS.CONSUMER_ACCT_AUTH_HOLD.CLEARED_YN_FLAG%TYPE;
                    BEGIN
                        SELECT EXPIRATION_TS, CLEARED_YN_FLAG
                          INTO ld_expiration_ts, lc_cleared_flag
                          FROM PSS.CONSUMER_ACCT_AUTH_HOLD
                         WHERE AUTH_ID = ln_auth_id;
                        IF lc_cleared_flag != 'N' THEN
                            RAISE_APPLICATION_ERROR(-20114, 'Auth Hold Was Cleared for transaction ' || pn_tran_id);
                        ELSIF ld_expiration_ts <= SYSDATE THEN
                            RAISE_APPLICATION_ERROR(-20115, 'Auth Hold Has Expired for transaction ' || pn_tran_id);
                        ELSE
                            RAISE_APPLICATION_ERROR(-20116, 'Auth Hold Inconsistent State for transaction ' || pn_tran_id);
                        END IF;
                    EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                            RAISE_APPLICATION_ERROR(-20111, 'Auth Hold Not Found for transaction ' || pn_tran_id);
                    END;
                END IF;
			END IF;
			pn_debitted_amount := pn_amount;
		ELSE
			pn_debitted_amount := 0;
			pc_auto_replenish_flag := 'N';
		END IF;
	END;
    
    -- R36 and below
    PROCEDURE DEBIT_CONSUMER_ACCT(
	   pn_amount PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
	   pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
	   pc_redelivery_flag CHAR,
	   pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
	   pn_debitted_amount OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
	   pn_new_balance OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
	   pc_auto_replenish_flag OUT VARCHAR2,
       pn_used_cash_back_balance OUT PSS.CONSUMER_ACCT.TOWARD_CASH_BACK_BALANCE%TYPE,
	   pn_cash_back_amount OUT PSS.SALE.SALE_AMOUNT%TYPE)
	IS
        ln_auth_result_cd VARCHAR2(1);	   
	BEGIN
        DEBIT_CONSUMER_ACCT(
           pn_amount,
           pn_tran_id,
           pc_redelivery_flag,
           pn_consumer_acct_id,
           ln_auth_result_cd,
           pn_debitted_amount,
           pn_new_balance,
           pc_auto_replenish_flag,
           pn_used_cash_back_balance,
           pn_cash_back_amount);
    END;
    
    -- R37 and above
    PROCEDURE CREDIT_CONSUMER_ACCT(
	   pn_amount PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
	   pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
	   pc_redelivery_flag CHAR,
	   pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
	   pc_auth_result_cd OUT VARCHAR2,
	   pn_creditted_amount OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
	   pn_new_balance OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE)
	IS
	   ln_refund_id PSS.REFUND.REFUND_ID%TYPE;
	   ln_refund_state_id PSS.REFUND.REFUND_STATE_ID%TYPE;
	   ln_consumer_acct_sub_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_SUB_TYPE_ID%TYPE;
       ln_override_trans_type_id PSS.REFUND.OVERRIDE_TRANS_TYPE_ID%TYPE;
	   lv_lock VARCHAR2(128);
	BEGIN
		SELECT ca.CONSUMER_ACCT_ID, R.REFUND_ID, R.REFUND_STATE_ID, CA.CONSUMER_ACCT_SUB_TYPE_ID
		  INTO pn_consumer_acct_id, ln_refund_id, ln_refund_state_id, ln_consumer_acct_sub_type_id
		  FROM PSS.CONSUMER_ACCT ca
		  JOIN PSS.TRAN X ON X.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
		  JOIN PSS.REFUND R ON X.TRAN_ID = R.TRAN_ID
		 WHERE X.TRAN_ID = pn_tran_id;
        
		IF ln_refund_state_id IN(2,3,4,6) THEN
            pc_auth_result_cd := 'Y';
            IF ln_consumer_acct_sub_type_id IS NOT NULL THEN
                IF ln_consumer_acct_sub_type_id = 2 THEN
                    ln_override_trans_type_id := 31;
                ELSE
                    ln_override_trans_type_id := 20;
                END IF;
            END IF;                 
			lv_lock := PKG_GLOBAL.REQUEST_LOCK('PSS.CONSUMER_ACCT', pn_consumer_acct_id);
			UPDATE PSS.REFUND
			   SET REFUND_STATE_ID = 1,
                   OVERRIDE_TRANS_TYPE_ID = ln_override_trans_type_id
			 WHERE REFUND_ID = ln_refund_id
			   AND REFUND_STATE_ID = ln_refund_state_id;
			IF SQL%FOUND THEN
				UPDATE PSS.CONSUMER_ACCT
				   SET CONSUMER_ACCT_BALANCE = CONSUMER_ACCT_BALANCE + pn_amount,
					   CONSUMER_ACCT_REPLEN_BALANCE = CONSUMER_ACCT_REPLEN_BALANCE + pn_amount,
                       TOWARD_CASH_BACK_BALANCE = CASE WHEN TOWARD_CASH_BACK_BALANCE IS NOT NULL AND CONSUMER_ACCT_TYPE_ID = 3 THEN TOWARD_CASH_BACK_BALANCE - pn_amount ELSE TOWARD_CASH_BACK_BALANCE END
				 WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id
                   AND NVL(CONSUMER_ACCT_SUB_TYPE_ID, 0) = NVL(ln_consumer_acct_sub_type_id, 0)
				 RETURNING CONSUMER_ACCT_BALANCE INTO pn_new_balance;
                IF NOT SQL%FOUND THEN
                    RAISE_APPLICATION_ERROR(-20116, 'Auth Hold Inconsistent State for transaction ' || pn_tran_id);
                END IF;
			ELSE
				RAISE_APPLICATION_ERROR(-20116, 'Auth Hold Inconsistent State for transaction ' || pn_tran_id);
			END IF;
			pn_creditted_amount := pn_amount;
		ELSIF pc_redelivery_flag != 'Y' THEN
			RAISE_APPLICATION_ERROR(-20111, 'Pending refund not found for transaction ' || pn_tran_id);
		ELSE
			pc_auth_result_cd := 'Y';
            pn_creditted_amount := pn_amount;
		END IF;
	END;
    
    -- R36 and below
    PROCEDURE CREDIT_CONSUMER_ACCT(
	    pn_amount PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
	    pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
	    pc_redelivery_flag CHAR,
	    pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
	    pn_creditted_amount OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
	    pn_new_balance OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE)
	IS
        ln_auth_result_cd VARCHAR2(1);  
	BEGIN
        CREDIT_CONSUMER_ACCT(pn_amount,
            pn_tran_id,
            pc_redelivery_flag,
            pn_consumer_acct_id,
            ln_auth_result_cd,
            pn_creditted_amount,
            pn_new_balance);
    END;  

    -- For R37 and above
	PROCEDURE CHECK_REPLENISH_CONSUMER_ACCT(
	   pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
	   pv_replenish_device_serial_cd OUT DEVICE.DEVICE_SERIAL_CD%TYPE,
	   pc_auto_replenish_flag OUT VARCHAR2,
	   pn_replenish_id OUT PSS.CONSUMER_ACCT_REPLENISH.CONSUMER_ACCT_REPLENISH_ID%TYPE,
	   pn_replenish_amount OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_AMOUNT%TYPE,
	   pv_replenish_card_key OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_CARD_KEY%TYPE,
	   pn_global_account_id OUT PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
	   pn_consumer_id OUT PSS.CONSUMER_ACCT.CONSUMER_ID%TYPE,
	   pn_replenish_next_master_id OUT NUMBER,
	   pv_replenish_device_name OUT DEVICE.DEVICE_NAME%TYPE,
       pn_max_denied_count PLS_INTEGER DEFAULT -1)
	IS
	   ln_replenish_threshhold PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_THRESHHOLD%TYPE;
	   lv_lock VARCHAR2(128);
	   CURSOR l_cur IS
			SELECT CAR.CONSUMER_ACCT_REPLENISH_ID, CAR.REPLENISH_TYPE_ID,
				   DECODE(CAR.REPLENISH_REQUESTED_FLAG, 'Y', NULL, CAR.REPLENISH_THRESHHOLD) REPLENISH_THRESHHOLD, 
				   CAR.REPLENISH_AMOUNT, CAR.REPLENISH_CARD_KEY, CA.CONSUMER_ACCT_BALANCE, CA.CURRENCY_CD,
				   'V1-' || CASE
						WHEN CA.CONSUMER_ACCT_SUB_TYPE_ID = 2 THEN CA.CORP_CUSTOMER_ID
						ELSE 1 
					END || '-' || CA.CURRENCY_CD REPLENISH_DEVICE_SERIAL_CD,
                    CA.CONSUMER_ID,
                    CAB.GLOBAL_ACCOUNT_ID
			  FROM PSS.CONSUMER_ACCT CA
              JOIN PSS.CONSUMER_ACCT_BASE CAB ON CA.CONSUMER_ACCT_ID = CAB.CONSUMER_ACCT_ID
			  JOIN PSS.CONSUMER_ACCT_REPLENISH CAR ON CA.CONSUMER_ACCT_ID = CAR.CONSUMER_ACCT_ID
			 WHERE ca.CONSUMER_ACCT_ID = pn_consumer_acct_id
			   AND (CAR.REPLENISH_TYPE_ID = 1 OR CAR.REPLENISH_REQUESTED_FLAG = 'Y')
               AND (pn_max_denied_count < 1 OR CAR.REPLENISH_DENIED_COUNT < pn_max_denied_count OR CAR.REPLENISH_REQUESTED_FLAG = 'Y')
               AND CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y' 
               AND CA.CONSUMER_ACCT_TYPE_ID = 3 
               AND (CAR.REPLENISH_THRESHHOLD > 0 OR CAR.REPLENISH_REQUESTED_FLAG = 'Y')
               AND CAR.REPLENISH_AMOUNT > 0 
               AND CAR.REPLENISH_CARD_KEY IS NOT NULL
			 ORDER BY CAR.PRIORITY, CAR.CONSUMER_ACCT_REPLENISH_ID;
	BEGIN
		lv_lock := PKG_GLOBAL.REQUEST_LOCK('PSS.CONSUMER_ACCT', pn_consumer_acct_id);
		pc_auto_replenish_flag := 'N';
		FOR l_rec IN l_cur LOOP
            UPDATE PSS.CONSUMER_ACCT_REPLENISH
               SET REPLENISH_REQUESTED_FLAG = 'N'
             WHERE CONSUMER_ACCT_REPLENISH_ID = l_rec.CONSUMER_ACCT_REPLENISH_ID
               AND REPLENISH_REQUESTED_FLAG = 'Y';
            IF SQL%FOUND THEN
                ln_replenish_threshhold := NULL; -- ignore threshhold
            ELSIF l_rec.REPLENISH_TYPE_ID != 1 THEN
                pc_auto_replenish_flag := 'N';
                CONTINUE;
            ELSE
                ln_replenish_threshhold := l_rec.REPLENISH_THRESHHOLD;
            END IF;
            pn_replenish_amount := l_rec.REPLENISH_AMOUNT;
            pn_global_account_id := l_rec.GLOBAL_ACCOUNT_ID;
            pn_consumer_id := l_rec.CONSUMER_ID;
            
            SETUP_REPLENISH_CONSUMER_ACCT(
                   l_rec.CONSUMER_ACCT_REPLENISH_ID,
                   pn_consumer_acct_id,
                   'N',
                   l_rec.REPLENISH_DEVICE_SERIAL_CD,
                   l_rec.CONSUMER_ACCT_BALANCE,
                   ln_replenish_threshhold,
                   pn_replenish_amount,
                   pc_auto_replenish_flag,
                   pn_replenish_next_master_id,
                   pv_replenish_device_name);
            IF pc_auto_replenish_flag = 'Y' THEN
                pn_replenish_id := l_rec.CONSUMER_ACCT_REPLENISH_ID;
                pv_replenish_card_key := l_rec.REPLENISH_CARD_KEY;
                pv_replenish_device_serial_cd := l_rec.REPLENISH_DEVICE_SERIAL_CD;
                RETURN;
            END IF;
		END LOOP;
	END;
    
    -- Not needed after R37
    PROCEDURE CHECK_REPLENISH_CONSUMER_ACCT(
	   pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
	   pv_replenish_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
	   pc_auto_replenish_flag OUT VARCHAR2,
	   pn_replenish_id OUT PSS.CONSUMER_ACCT_REPLENISH.CONSUMER_ACCT_REPLENISH_ID%TYPE,
	   pn_replenish_amount OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_AMOUNT%TYPE,
	   pv_replenish_card_key OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_CARD_KEY%TYPE,
	   pb_consumer_acct_cd_hash OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD_HASH%TYPE,
	   pn_replenish_next_master_id OUT NUMBER,
	   pv_replenish_device_name OUT DEVICE.DEVICE_NAME%TYPE,
       pn_max_denied_count PLS_INTEGER DEFAULT -1)
	IS
	   ln_balance PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE;
	   ln_replenish_threshhold PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_THRESHHOLD%TYPE;
	   lv_currency_cd PSS.CONSUMER_ACCT.CURRENCY_CD%TYPE;
	   ln_replenish_type_id PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_TYPE_ID%TYPE;
	   lv_lock VARCHAR2(128);
	   lv_replenish_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE;
	   CURSOR l_cur IS
			SELECT CAR.CONSUMER_ACCT_REPLENISH_ID, CAR.REPLENISH_TYPE_ID, CA.CONSUMER_ACCT_CD_HASH, 
				   DECODE(CAR.REPLENISH_REQUESTED_FLAG, 'Y', NULL, CAR.REPLENISH_THRESHHOLD) REPLENISH_THRESHHOLD, 
				   CAR.REPLENISH_AMOUNT, CAR.REPLENISH_CARD_KEY, CA.CONSUMER_ACCT_BALANCE, CA.CURRENCY_CD,
				   CASE
						WHEN CA.CONSUMER_ACCT_SUB_TYPE_ID = 2 THEN 'V1-' || CA.CORP_CUSTOMER_ID
						ELSE pv_replenish_device_serial_cd 
					END || '-' || CA.CURRENCY_CD REPLENISH_DEVICE_SERIAL_CD
			  FROM PSS.CONSUMER_ACCT CA
			  JOIN PSS.CONSUMER_ACCT_REPLENISH CAR ON CA.CONSUMER_ACCT_ID = CAR.CONSUMER_ACCT_ID
			 WHERE ca.CONSUMER_ACCT_ID = pn_consumer_acct_id
			   AND (CAR.REPLENISH_TYPE_ID = 1 OR CAR.REPLENISH_REQUESTED_FLAG = 'Y')
               AND (pn_max_denied_count < 1 OR CAR.REPLENISH_DENIED_COUNT < pn_max_denied_count OR CAR.REPLENISH_REQUESTED_FLAG = 'Y')
               AND CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y' 
               AND CA.CONSUMER_ACCT_TYPE_ID = 3 
               AND (CAR.REPLENISH_THRESHHOLD > 0 OR CAR.REPLENISH_REQUESTED_FLAG = 'Y')
               AND CAR.REPLENISH_AMOUNT > 0 
               AND CAR.REPLENISH_CARD_KEY IS NOT NULL
			 ORDER BY CAR.PRIORITY, CAR.CONSUMER_ACCT_REPLENISH_ID;
	BEGIN
		lv_lock := PKG_GLOBAL.REQUEST_LOCK('PSS.CONSUMER_ACCT', pn_consumer_acct_id);
		pc_auto_replenish_flag := 'N';
		FOR l_rec IN l_cur LOOP
            UPDATE PSS.CONSUMER_ACCT_REPLENISH
               SET REPLENISH_REQUESTED_FLAG = 'N'
             WHERE CONSUMER_ACCT_REPLENISH_ID = l_rec.CONSUMER_ACCT_REPLENISH_ID
               AND REPLENISH_REQUESTED_FLAG = 'Y';
            IF SQL%FOUND THEN
                ln_replenish_threshhold := NULL; -- ignore threshhold
            ELSIF l_rec.REPLENISH_TYPE_ID != 1 THEN
                pc_auto_replenish_flag := 'N';
                CONTINUE;
            ELSE
                ln_replenish_threshhold := l_rec.REPLENISH_THRESHHOLD;
            END IF;
            pn_replenish_amount := l_rec.REPLENISH_AMOUNT;
            SETUP_REPLENISH_CONSUMER_ACCT(
                   l_rec.CONSUMER_ACCT_REPLENISH_ID,
                   pn_consumer_acct_id,
                   'N',
                   l_rec.REPLENISH_DEVICE_SERIAL_CD,
                   l_rec.CONSUMER_ACCT_BALANCE,
                   ln_replenish_threshhold,
                   pn_replenish_amount,
                   pc_auto_replenish_flag,
                   pn_replenish_next_master_id,
                   pv_replenish_device_name);
            IF pc_auto_replenish_flag = 'Y' THEN
                pn_replenish_id := l_rec.CONSUMER_ACCT_REPLENISH_ID;
                pv_replenish_card_key := l_rec.REPLENISH_CARD_KEY;
                pb_consumer_acct_cd_hash := l_rec.CONSUMER_ACCT_CD_HASH;
                RETURN;
            END IF;
		END LOOP;
	END;
    
	-- R33 Signature
	PROCEDURE LOCK_AUTH_HOLD(
		pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
		pn_ignore_auth_id PSS.AUTH.AUTH_ID%TYPE,
		pn_auth_hold_total OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE)
	IS
		lv_lock VARCHAR2(128);
	BEGIN
		lv_lock := PKG_GLOBAL.REQUEST_LOCK('PSS.CONSUMER_ACCT', pn_consumer_acct_id);
		SELECT NVL(SUM(A.AUTH_AMT_APPROVED), 0)
		  INTO pn_auth_hold_total
		  FROM PSS.CONSUMER_ACCT_AUTH_HOLD CAAH
		  JOIN PSS.AUTH A ON CAAH.AUTH_ID = A.AUTH_ID
		 WHERE CAAH.CONSUMER_ACCT_ID = pn_consumer_acct_id
		   AND CAAH.EXPIRATION_TS > SYSDATE
		   AND CAAH.CLEARED_YN_FLAG = 'N'
		   AND (pn_ignore_auth_id IS NULL OR A.AUTH_ID != pn_ignore_auth_id);
	END;
    
	PROCEDURE SETUP_REPLENISH(
		pn_replenish_id IN OUT PSS.CONSUMER_ACCT_REPLENISH.CONSUMER_ACCT_REPLENISH_ID%TYPE,
		pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
		pn_replenish_type_id PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_TYPE_ID%TYPE,
		pv_replenish_card_masked PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_CARD_MASKED%TYPE,
		pv_replenish_device_serial_cd IN OUT DEVICE.DEVICE_SERIAL_CD%TYPE, -- From R37 on we ignore the provided device_serial_cd
		pn_replenish_threshhold PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_THRESHHOLD%TYPE,
		pn_replenish_amount IN OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_AMOUNT%TYPE,
		pc_replenish_flag OUT VARCHAR2,
		pn_replenish_next_master_id OUT NUMBER,
		pv_replenish_device_name OUT DEVICE.DEVICE_NAME%TYPE)
	IS
		lv_lock VARCHAR2(128);
		ln_balance PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE;
		ln_priority PSS.CONSUMER_ACCT_REPLENISH.PRIORITY%TYPE;
		lc_always_auth_flag CHAR(1);
	BEGIN
		lv_lock := PKG_GLOBAL.REQUEST_LOCK('PSS.CONSUMER_ACCT', pn_consumer_acct_id);
		SELECT CONSUMER_ACCT_BALANCE, 'V1-' || CASE 
				WHEN CONSUMER_ACCT_SUB_TYPE_ID = 2 THEN CORP_CUSTOMER_ID
				ELSE 1 
			END || '-' || CURRENCY_CD
		  INTO ln_balance, pv_replenish_device_serial_cd
		  FROM PSS.CONSUMER_ACCT
		 WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id;       
		IF pn_replenish_id IS NULL THEN
			SELECT PSS.SEQ_CONSUMER_ACCT_REPLENISH_ID.NEXTVAL, PRIORITY
			  INTO pn_replenish_id, ln_priority
			  FROM (SELECT NVL(MAX(PRIORITY), 0) + 1 PRIORITY
			  FROM PSS.CONSUMER_ACCT_REPLENISH
			 WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id);
			INSERT INTO PSS.CONSUMER_ACCT_REPLENISH(CONSUMER_ACCT_REPLENISH_ID, CONSUMER_ACCT_ID, PRIORITY, REPLENISH_TYPE_ID, REPLENISH_CARD_MASKED, REPLENISH_AMOUNT, REPLENISH_THRESHHOLD)
				VALUES(pn_replenish_id, pn_consumer_acct_id, ln_priority, pn_replenish_type_id, pv_replenish_card_masked, pn_replenish_amount, pn_replenish_threshhold);
			lc_always_auth_flag := 'Y';
		ELSE
			UPDATE PSS.CONSUMER_ACCT_REPLENISH CAR
			   SET CAR.REPLENISH_THRESHHOLD = pn_replenish_threshhold,
				   CAR.REPLENISH_AMOUNT = pn_replenish_amount,
				   CAR.REPLENISH_TYPE_ID = pn_replenish_type_id
			 WHERE CAR.CONSUMER_ACCT_REPLENISH_ID = pn_replenish_id
			   AND CAR.CONSUMER_ACCT_ID = pn_consumer_acct_id;
			IF NOT SQL%FOUND THEN
				RAISE_APPLICATION_ERROR(-20119, 'ReplenishId ' || pn_replenish_id || ' does not belong to consumer acct ' || pn_consumer_acct_id);
			END IF;
			lc_always_auth_flag := 'N';
		END IF;
		SETUP_REPLENISH_CONSUMER_ACCT(
		   pn_replenish_id,
		   pn_consumer_acct_id,
		   lc_always_auth_flag,
		   pv_replenish_device_serial_cd,
		   ln_balance,
		   pn_replenish_threshhold,
		   pn_replenish_amount,
		   pc_replenish_flag,
		   pn_replenish_next_master_id,
		   pv_replenish_device_name);
		IF pc_replenish_flag = 'N' THEN
			PKG_DEVICE_CONFIGURATION.NEXT_MASTER_ID_BY_SERIAL(pv_replenish_device_serial_cd, pv_replenish_device_name, pn_replenish_next_master_id);
			pn_replenish_amount := 0;
		END IF;
	END;   
    
	PROCEDURE UPDATE_PENDING_REPLENISH(
		pn_replenish_id PSS.CONSUMER_ACCT_REPLENISH.CONSUMER_ACCT_REPLENISH_ID%TYPE,
		pn_replenish_next_master_id NUMBER,
		pc_submitted_flag PSS.CONSUMER_ACCT_PEND_REPLENISH.SUBMITTED_FLAG%TYPE,
		pc_initial_replenish_flag CHAR,
        pn_denied_count OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_DENIED_COUNT%TYPE,
        pv_replenish_card_masked OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_CARD_MASKED%TYPE)
	IS
		ln_priority PSS.CONSUMER_ACCT_REPLENISH.PRIORITY%TYPE;
		ln_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE;
	BEGIN
		IF pc_initial_replenish_flag = 'Y' THEN
            IF pc_submitted_flag NOT IN('Y', 'P') THEN
                DELETE
                  FROM PSS.CONSUMER_ACCT_PEND_REPLENISH
                 WHERE CONSUMER_ACCT_REPLENISH_ID = pn_replenish_id
                   AND DEVICE_TRAN_CD = pn_replenish_next_master_id;      
                DELETE
                  FROM PSS.CONSUMER_ACCT_REPLENISH
                 WHERE CONSUMER_ACCT_REPLENISH_ID = pn_replenish_id
                RETURNING PRIORITY, CONSUMER_ACCT_ID, REPLENISH_CARD_MASKED 
                  INTO ln_priority, ln_consumer_acct_id, pv_replenish_card_masked;
                IF ln_priority IS NOT NULL AND ln_priority > 0 THEN
                    UPDATE PSS.CONSUMER_ACCT_REPLENISH
                       SET PRIORITY = PRIORITY - 1
                     WHERE CONSUMER_ACCT_ID = ln_consumer_acct_id
                       AND PRIORITY IN(SELECT PRIORITY FROM PSS.CONSUMER_ACCT_REPLENISH WHERE CONSUMER_ACCT_ID = ln_consumer_acct_id CONNECT BY PRIOR PRIORITY = PRIORITY - 1 START WITH PRIORITY = ln_priority + 1);
                END IF;
                pn_denied_count := 1;
            ELSE
                UPDATE PSS.CONSUMER_ACCT_REPLENISH CAR
                   SET PRIORITY = PRIORITY + 1
                 WHERE CONSUMER_ACCT_ID = (SELECT CONSUMER_ACCT_ID FROM PSS.CONSUMER_ACCT_REPLENISH WHERE CONSUMER_ACCT_REPLENISH_ID = pn_replenish_id)
                   AND PRIORITY IN(SELECT CAR0.PRIORITY FROM PSS.CONSUMER_ACCT_REPLENISH CAR0 WHERE CAR0.CONSUMER_ACCT_ID = CAR.CONSUMER_ACCT_ID CONNECT BY PRIOR CAR0.PRIORITY = CAR0.PRIORITY - 1 START WITH CAR0.PRIORITY = 1);
                UPDATE PSS.CONSUMER_ACCT_REPLENISH
                   SET PRIORITY = 1
                 WHERE CONSUMER_ACCT_REPLENISH_ID = pn_replenish_id
                  RETURNING REPLENISH_CARD_MASKED
                  INTO pv_replenish_card_masked;
                UPDATE PSS.CONSUMER_ACCT_PEND_REPLENISH
                   SET SUBMITTED_FLAG = pc_submitted_flag
                 WHERE CONSUMER_ACCT_REPLENISH_ID = pn_replenish_id
                   AND DEVICE_TRAN_CD = pn_replenish_next_master_id; 
                pn_denied_count := 0;
            END IF;
        ELSE
			UPDATE PSS.CONSUMER_ACCT_PEND_REPLENISH
			   SET SUBMITTED_FLAG = pc_submitted_flag
			 WHERE CONSUMER_ACCT_REPLENISH_ID = pn_replenish_id
			   AND DEVICE_TRAN_CD = pn_replenish_next_master_id;
            UPDATE PSS.CONSUMER_ACCT_REPLENISH
               SET REPLENISH_DENIED_COUNT = DECODE(pc_submitted_flag, 'Y', 0, 'P', 0, REPLENISH_DENIED_COUNT + 1)
             WHERE CONSUMER_ACCT_REPLENISH_ID = pn_replenish_id
              RETURNING REPLENISH_DENIED_COUNT, REPLENISH_CARD_MASKED 
              INTO pn_denied_count, pv_replenish_card_masked;
		END IF;
	END;
	
    PROCEDURE CREATE_ISIS_CONSUMER(
        pn_consumer_id OUT PSS.CONSUMER.CONSUMER_ID%TYPE,
        pv_consumer_identifier PSS.CONSUMER.CONSUMER_IDENTIFIER%TYPE)
    IS
    BEGIN
        SELECT PSS.SEQ_CONSUMER_ID.NEXTVAL 
          INTO pn_consumer_id 
          FROM DUAL;
		INSERT INTO PSS.CONSUMER(CONSUMER_ID, CONSUMER_EMAIL_ADDR1, CONSUMER_TYPE_ID, CONSUMER_IDENTIFIER)
            VALUES(pn_consumer_id, 'isis_' || pn_consumer_id || '@usatech.com', 7, pv_consumer_identifier);
    END;
    
    -- R37+ Signature
    PROCEDURE AUTHORIZE_ISIS_PROMO(
		pn_global_account_id IN OUT PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
		pv_instance PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_INSTANCE%TYPE,
		pv_consumer_acct_cd PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD%TYPE,
		pv_consumer_identifier PSS.CONSUMER.CONSUMER_IDENTIFIER%TYPE,
		pv_currency_cd PSS.CURRENCY.CURRENCY_CD%TYPE,
		pv_auth_result_cd OUT VARCHAR2,
		pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
		pn_consumer_id OUT PSS.CONSUMER.CONSUMER_ID%TYPE,
		pn_trans_to_free_tran OUT NUMBER)
	IS
		ln_old_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE;
	BEGIN
		pv_auth_result_cd := 'N';
		pn_consumer_id := NULL;
		
		IF pv_consumer_identifier IS NOT NULL THEN
			SELECT MAX(consumer_id)
			  INTO pn_consumer_id
			  FROM PSS.CONSUMER
			  WHERE CONSUMER_IDENTIFIER = pv_consumer_identifier;
			
			IF pn_consumer_id IS NULL THEN
				BEGIN
					CREATE_ISIS_CONSUMER(pn_consumer_id, pv_consumer_identifier);
				EXCEPTION
					WHEN DUP_VAL_ON_INDEX THEN
						SELECT MAX(consumer_id)
						  INTO pn_consumer_id
						  FROM PSS.CONSUMER
                         WHERE CONSUMER_IDENTIFIER = pv_consumer_identifier;
				END;
			END IF;
		END IF;
	
        IF pn_global_account_id IS NULL THEN
            SELECT PSS.SEQ_GLOBAL_ACCOUNT_ID_BASE.NEXTVAL
              INTO pn_global_account_id 
              FROM DUAL;      
        ELSE
            SELECT MAX(CAB.CONSUMER_ACCT_ID), MAX(CA.CONSUMER_ID)
              INTO pn_consumer_acct_id, ln_old_consumer_id
              FROM PSS.CONSUMER_ACCT_BASE CAB
              LEFT OUTER JOIN PSS.CONSUMER_ACCT CA ON CAB.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
             WHERE CAB.GLOBAL_ACCOUNT_ID = pn_global_account_id
               AND CAB.CURRENCY_CD = pv_currency_cd;
 		END IF;
        IF pn_consumer_acct_id IS NULL THEN
            SELECT PSS.SEQ_CONSUMER_ACCT_ID.NEXTVAL
              INTO pn_consumer_acct_id
              FROM DUAL;
            IF pn_consumer_id IS NULL THEN
                CREATE_ISIS_CONSUMER(pn_consumer_id, pv_consumer_identifier);
            END IF;
            INSERT INTO PSS.CONSUMER_ACCT_BASE(CONSUMER_ACCT_ID, GLOBAL_ACCOUNT_ID, GLOBAL_ACCOUNT_INSTANCE, TRUNCATED_CARD_NUMBER, PAYMENT_SUBTYPE_ID, CURRENCY_CD)
                VALUES(pn_consumer_acct_id, pn_global_account_id, 0, DBADMIN.MASK_CREDIT_CARD(pv_consumer_acct_cd), 1, pv_currency_cd);
            INSERT INTO PSS.CONSUMER_ACCT(CONSUMER_ACCT_ID, CONSUMER_ACCT_CD, CONSUMER_ACCT_ACTIVE_YN_FLAG, CONSUMER_ACCT_BALANCE, CONSUMER_ID, LOCATION_ID, 
                    CONSUMER_ACCT_ISSUE_NUM, PAYMENT_SUBTYPE_ID, CURRENCY_CD, CONSUMER_ACCT_TYPE_ID) 
                VALUES(pn_consumer_acct_id, pv_consumer_acct_cd, 'Y', 0, pn_consumer_id, 1, 1, 1, pv_currency_cd, 5);
        ELSIF pn_consumer_id IS NULL THEN
            pn_consumer_id := ln_old_consumer_id;
        ELSIF ln_old_consumer_id IS NULL THEN
            INSERT INTO PSS.CONSUMER_ACCT(CONSUMER_ACCT_ID, CONSUMER_ACCT_CD, CONSUMER_ACCT_ACTIVE_YN_FLAG, CONSUMER_ACCT_BALANCE, CONSUMER_ID, LOCATION_ID, 
                    CONSUMER_ACCT_ISSUE_NUM, PAYMENT_SUBTYPE_ID, CURRENCY_CD, CONSUMER_ACCT_TYPE_ID) 
                VALUES(pn_consumer_acct_id, pv_consumer_acct_cd, 'Y', 0, pn_consumer_id, 1, 1, 1, pv_currency_cd, 5);
        ELSIF pn_consumer_id != ln_old_consumer_id THEN
            UPDATE PSS.CONSUMER_ACCT
               SET CONSUMER_ID = pn_consumer_id
             WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id;
        END IF;
	
		UPDATE PSS.CONSUMER_PROMOTION
		   SET TRAN_COUNT = tran_count + 1,
			   PROMO_TRAN_COUNT = promo_tran_count + 1
		 WHERE CONSUMER_ID = pn_consumer_id
           AND PROMOTION_ID = 1
           AND (TRAN_COUNT + 1) / (PROMO_TRAN_COUNT + 1) >= 5
		RETURNING 'Y' INTO pv_auth_result_cd;
			
		LOOP
			BEGIN
				SELECT 4 - MOD(TRAN_COUNT + 1, 5)
				  INTO pn_trans_to_free_tran
				  FROM PSS.CONSUMER_PROMOTION
				 WHERE CONSUMER_ID = pn_consumer_id
                   AND PROMOTION_ID = 1;
				EXIT;
			EXCEPTION
				WHEN NO_DATA_FOUND THEN
					BEGIN
						INSERT INTO PSS.CONSUMER_PROMOTION(CONSUMER_ID, PROMOTION_ID)
                            VALUES(pn_consumer_id, 1);
					EXCEPTION
						WHEN DUP_VAL_ON_INDEX THEN
							NULL;
					END;
			END;
		END LOOP;
	END;

    -- R36 and below Signature
	PROCEDURE AUTHORIZE_ISIS_PROMO(
		pv_consumer_acct_cd PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD%TYPE,
		pr_consumer_acct_cd_hash PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD_HASH%TYPE,
		pv_consumer_identifier PSS.CONSUMER.CONSUMER_IDENTIFIER%TYPE,
		pv_currency_cd PSS.CURRENCY.CURRENCY_CD%TYPE,
		pv_auth_result_cd OUT VARCHAR2,
		pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
		pn_consumer_id OUT PSS.CONSUMER.CONSUMER_ID%TYPE,
		pn_trans_to_free_tran OUT NUMBER)
	IS
		ln_old_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE;
        ln_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE;
	BEGIN
		pv_auth_result_cd := 'N';
		pn_consumer_id := NULL;
		
		IF pv_consumer_identifier IS NOT NULL THEN
			SELECT MAX(consumer_id)
			INTO pn_consumer_id
			FROM pss.consumer
			WHERE consumer_identifier = pv_consumer_identifier;
			
			IF pn_consumer_id IS NULL THEN
				SELECT pss.seq_consumer_id.NEXTVAL INTO pn_consumer_id FROM dual;
				BEGIN
					INSERT INTO pss.consumer(consumer_id, consumer_email_addr1, consumer_type_id, consumer_identifier)
					VALUES(pn_consumer_id, 'isis_' || pn_consumer_id || '@usatech.com', 7, pv_consumer_identifier);
				EXCEPTION
					WHEN DUP_VAL_ON_INDEX THEN
						SELECT MAX(consumer_id)
						INTO pn_consumer_id
						FROM pss.consumer
						WHERE consumer_identifier = pv_consumer_identifier;
				END;
			END IF;
		END IF;
	
		SELECT MAX(ca.consumer_acct_id), MAX(ca.consumer_id)
		INTO pn_consumer_acct_id, ln_old_consumer_id
		FROM pss.consumer_acct ca
		JOIN pss.consumer c ON ca.consumer_id = c.consumer_id
		WHERE ca.consumer_acct_cd_hash = pr_consumer_acct_cd_hash AND ca.consumer_acct_cd = pv_consumer_acct_cd;
		
		IF pn_consumer_acct_id IS NULL THEN
			IF pn_consumer_id IS NULL THEN
				SELECT pss.seq_consumer_id.NEXTVAL INTO pn_consumer_id FROM dual;
				INSERT INTO pss.consumer(consumer_id, consumer_email_addr1, consumer_type_id)
				VALUES(pn_consumer_id, 'isis_' || pn_consumer_id || '@usatech.com', 7);
			END IF;
		
			SELECT pss.seq_consumer_acct_id.NEXTVAL, PSS.SEQ_GLOBAL_ACCOUNT_ID_BASE.NEXTVAL
              INTO pn_consumer_acct_id, ln_global_account_id
              FROM dual;
			BEGIN
				INSERT INTO PSS.CONSUMER_ACCT_BASE(CONSUMER_ACCT_ID, GLOBAL_ACCOUNT_ID, GLOBAL_ACCOUNT_INSTANCE, TRUNCATED_CARD_NUMBER, PAYMENT_SUBTYPE_ID, CURRENCY_CD)
                    VALUES(pn_consumer_acct_id, ln_global_account_id, 0, DBADMIN.MASK_CREDIT_CARD(pv_consumer_acct_cd), 1, pv_currency_cd);
                INSERT INTO pss.consumer_acct(consumer_acct_id, consumer_acct_cd, consumer_acct_active_yn_flag, 
					consumer_acct_balance, consumer_id, location_id, consumer_acct_issue_num, payment_subtype_id,					
					currency_cd, consumer_acct_type_id, consumer_acct_cd_hash) 
				VALUES(pn_consumer_acct_id, pv_consumer_acct_cd, 'Y', 0, pn_consumer_id, 1, 1, 1, pv_currency_cd, 5, pr_consumer_acct_cd_hash);
			EXCEPTION
				WHEN DUP_VAL_ON_INDEX THEN
					SELECT MAX(consumer_acct_id), MAX(consumer_id)
					INTO pn_consumer_acct_id, pn_consumer_id
					FROM pss.consumer_acct
					WHERE consumer_acct_cd_hash = pr_consumer_acct_cd_hash AND consumer_acct_cd = pv_consumer_acct_cd;
			END;
		ELSE
			IF pn_consumer_id IS NULL THEN
				pn_consumer_id := ln_old_consumer_id;
			ELSIF pn_consumer_id != ln_old_consumer_id THEN
				UPDATE pss.consumer_acct
				SET consumer_id = pn_consumer_id
				WHERE consumer_acct_id = pn_consumer_acct_id;
			END IF;
		END IF;
	
		UPDATE pss.consumer_promotion
		SET tran_count = tran_count + 1,
			promo_tran_count = promo_tran_count + 1
		WHERE consumer_id = pn_consumer_id
			AND promotion_id = 1
			AND (tran_count + 1) / (promo_tran_count + 1) >= 5
		RETURNING 'Y' INTO pv_auth_result_cd;
			
		LOOP
			BEGIN
				SELECT 4 - MOD(tran_count + 1, 5)
				INTO pn_trans_to_free_tran
				FROM pss.consumer_promotion
				WHERE consumer_id = pn_consumer_id
					AND promotion_id = 1;
				EXIT;
			EXCEPTION
				WHEN NO_DATA_FOUND THEN
					BEGIN
						INSERT INTO pss.consumer_promotion(consumer_id, promotion_id)
						VALUES(pn_consumer_id, 1);
					EXCEPTION
						WHEN DUP_VAL_ON_INDEX THEN
							NULL;
					END;
			END;
		END LOOP;
	END;
	
	PROCEDURE REFUND_ISIS_PROMO(
		pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
		pn_result_cd OUT NUMBER,
		pv_error_message OUT VARCHAR2)
	IS
		ln_refund_state_id PSS.REFUND.REFUND_STATE_ID%TYPE;
		ln_parent_tran_id PSS.TRAN.PARENT_TRAN_ID%TYPE;
		ln_refund_count NUMBER;
		ln_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE;
	BEGIN
		pn_result_cd := PKG_CONST.RESULT__SUCCESS;
		pv_error_message := PKG_CONST.ERROR__NO_ERROR;
		
		SELECT PARENT_TRAN_ID
		INTO ln_parent_tran_id
		FROM PSS.TRAN
		WHERE TRAN_ID = pn_tran_id;
		
		SELECT COUNT(1)
		INTO ln_refund_count
		FROM PSS.REFUND R
		JOIN PSS.TRAN T ON R.TRAN_ID = T.TRAN_ID
		WHERE T.PARENT_TRAN_ID = ln_parent_tran_id AND R.REFUND_STATE_ID IN(1);
		
		IF ln_refund_count > 0 THEN
			RETURN;
		END IF;
		
		SELECT CA.CONSUMER_ID
		INTO ln_consumer_id
		FROM PSS.TRAN T
		JOIN PSS.CONSUMER_ACCT CA ON T.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
		WHERE T.TRAN_ID = pn_tran_id;
		
		UPDATE PSS.CONSUMER_PROMOTION
		SET TRAN_COUNT = CASE WHEN TRAN_COUNT - 1 >= 0 THEN TRAN_COUNT - 1 ELSE 0 END,
			PROMO_TRAN_COUNT = CASE WHEN PROMO_TRAN_COUNT - 1 >= 0 THEN PROMO_TRAN_COUNT - 1 ELSE 0 END
		WHERE CONSUMER_ID = ln_consumer_id AND PROMOTION_ID = 1;
	END;
    
    PROCEDURE GET_OR_CREATE_CONSUMER_ACCT(
		pn_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
		pn_global_account_instance PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_INSTANCE%TYPE,
		pn_pos_pta_id PSS.POS_PTA.POS_PTA_ID%TYPE,
		pv_currency_cd PSS.CONSUMER_ACCT_BASE.CURRENCY_CD%TYPE,
        pd_auth_ts DATE,
        pv_truncated_card_num PSS.CONSUMER_ACCT_BASE.TRUNCATED_CARD_NUMBER%TYPE,
        pn_consumer_acct_id OUT PSS.CONSUMER_ACCT_BASE.CONSUMER_ACCT_ID%TYPE)
	IS
    BEGIN
        SELECT CONSUMER_ACCT_ID 
          INTO pn_consumer_acct_id
          FROM (		
	         SELECT CAB.CONSUMER_ACCT_ID  
	           FROM (
		        SELECT CAB.CONSUMER_ACCT_ID,
				       CASE WHEN CA.CONSUMER_ACCT_ACTIVE_YN_FLAG IS NULL THEN 5 WHEN CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y' AND NVL(CA.CONSUMER_ACCT_ACTIVATION_TS, MIN_DATE) <= pd_auth_ts AND NVL(CA.CONSUMER_ACCT_DEACTIVATION_TS, MAX_DATE) > pd_auth_ts THEN 1 ELSE 10 END CA_PRIORITY,
				       CAB.PAYMENT_SUBTYPE_ID,
				       CAB.LOCATION_ID,
				       CASE WHEN PTA.PAYMENT_SUBTYPE_ID = CAB.PAYMENT_SUBTYPE_ID THEN 1 ELSE 10 END PST_PRIORITY,
				       POS.LOCATION_ID POS_LOCATION_ID,
				       CA.CONSUMER_ACCT_ISSUE_NUM,
				       CASE WHEN CAB.LOCATION_ID IS NULL THEN NULL ELSE (SELECT VLH.DEPTH FROM LOCATION.VW_LOCATION_HIERARCHY VLH WHERE VLH.DESCENDENT_LOCATION_ID = POS.LOCATION_ID AND VLH.ANCESTOR_LOCATION_ID = CAB.LOCATION_ID) END DEPTH
				  FROM PSS.CONSUMER_ACCT_BASE CAB
				  LEFT OUTER JOIN PSS.CONSUMER_ACCT CA ON CAB.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
				 CROSS JOIN PSS.POS_PTA PTA
				  JOIN PSS.POS POS ON PTA.POS_ID = POS.POS_ID          
				 WHERE CAB.GLOBAL_ACCOUNT_ID = pn_global_account_id
				   AND PTA.POS_PTA_ID = pn_pos_pta_id 
				   AND CAB.CURRENCY_CD = pv_currency_cd) CAB
				ORDER BY CAB.CA_PRIORITY, CAB.PST_PRIORITY, CAB.DEPTH NULLS LAST, CAB.CONSUMER_ACCT_ISSUE_NUM DESC
        ) WHERE ROWNUM = 1;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            SELECT PSS.SEQ_CONSUMER_ACCT_ID.NEXTVAL
              INTO pn_consumer_acct_id
              FROM DUAL;
            INSERT INTO PSS.CONSUMER_ACCT_BASE(CONSUMER_ACCT_ID, GLOBAL_ACCOUNT_ID, GLOBAL_ACCOUNT_INSTANCE, TRUNCATED_CARD_NUMBER, PAYMENT_SUBTYPE_ID, CURRENCY_CD) 
                SELECT pn_consumer_acct_id, pn_global_account_id, pn_global_account_instance, pv_truncated_card_num, PTA.PAYMENT_SUBTYPE_ID, pv_currency_cd
                  FROM PSS.POS_PTA PTA
                 WHERE PTA.POS_PTA_ID = pn_pos_pta_id;
    END;
    
    PROCEDURE RESOLVE_ACCOUNT_CONFLICT(
        pn_old_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
		pn_new_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
		pn_global_account_instance PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_INSTANCE%TYPE)
    IS
    BEGIN
        UPDATE PSS.CONSUMER_ACCT_BASE
           SET GLOBAL_ACCOUNT_ID = pn_new_global_account_id, GLOBAL_ACCOUNT_INSTANCE = pn_global_account_instance
         WHERE GLOBAL_ACCOUNT_ID = pn_old_global_account_id;
    EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
            -- we must merge rows
            DECLARE
                CURSOR l_cur IS
                    SELECT CAB_NEW.CONSUMER_ACCT_ID NEW_CONSUMER_ACCT_ID, CAB_OLD.CONSUMER_ACCT_ID OLD_CONSUMER_ACCT_ID
                      FROM PSS.CONSUMER_ACCT_BASE CAB_NEW
                      JOIN PSS.CONSUMER_ACCT_BASE CAB_OLD ON CAB_NEW.PAYMENT_SUBTYPE_ID = CAB_OLD.PAYMENT_SUBTYPE_ID 
                       AND CAB_NEW.CURRENCY_CD = CAB_OLD.CURRENCY_CD
                       AND NVL(CAB_NEW.LOCATION_ID, 0) = NVL(CAB_OLD.LOCATION_ID, 0)
                     WHERE CAB_OLD.GLOBAL_ACCOUNT_ID = pn_old_global_account_id
                       AND CAB_NEW.GLOBAL_ACCOUNT_ID = pn_new_global_account_id;
            BEGIN
                FOR l_rec IN l_cur LOOP
                    UPDATE PSS.TRAN X
                       SET CONSUMER_ACCT_ID = l_rec.NEW_CONSUMER_ACCT_ID
                     WHERE CONSUMER_ACCT_ID = l_rec.OLD_CONSUMER_ACCT_ID;
                    DELETE 
                      FROM PSS.CONSUMER_ACCT_BASE
                     WHERE CONSUMER_ACCT_ID = l_rec.OLD_CONSUMER_ACCT_ID;
                END LOOP;
            END;            
    END;
       
END;
/

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/releases/REL_Edge_Update_R37/R37.USAPDB.4.sql?rev=HEAD
update web_content.literal set literal_value='We at USA Technologies, Inc. are continually striving to enhance our customers'' power to run their business effectively. The USALive website is loaded with features to help you do just that. For the best website experience, please use the latest major version of your browser. Now new in Version 1.13: <ul><li>Customers may make a secure credit card payment for an order by following the Complete Order link.</li><li>All cards are assigned a unique Card Id not related to the card account number in any way.  Card Id has been added to numerous reports.</li><li>Using the Card Id allows repeat card usage to be easily identified and tracked.</li><li>New Transaction Types have been added to many reports to support new Operator Serviced MORE prepaid cards.</li><li>Report output format icons have been added to report links to streamline report generation in various formats.</li><li>Report generation speed enhancements.</li><li>Minor bug fixes.</li></ul>' where literal_key='home-page-message' and subdomain_id is null;
commit;

-- check trans
/*
SELECT *
  FROM PSS.TRAN 
 WHERE CONSUMER_ACCT_ID IS NULL
   AND TRAN_RECEIVED_RAW_ACCT_DATA IS NOT NULL
   AND (TRAN_PARSED_ACCT_NUM_ENCR IS NOT NULL OR REGEXP_LIKE(TRAN_RECEIVED_RAW_ACCT_DATA, '^[^A-Za-z0-9*@]*[0-9]{4,}([=^].*)?[^A-Za-z0-9*@]*$'));

SELECT TRAN_RECEIVED_RAW_ACCT_DATA, COUNT(*)
  FROM PSS.TRAN 
 WHERE CONSUMER_ACCT_ID IS NULL
   AND TRAN_RECEIVED_RAW_ACCT_DATA IS NOT NULL
   AND REGEXP_LIKE(TRAN_RECEIVED_RAW_ACCT_DATA, '[0-9]{4,}')
   AND NOT REGEXP_LIKE(TRAN_RECEIVED_RAW_ACCT_DATA, '[0-9]{2,}[*]{1,}[0-9]{4,}')
   AND (TRAN_PARSED_ACCT_NUM IS NOT NULL OR
               TRAN_PARSED_ACCT_NUM_HASH IS NOT NULL OR
               TRAN_PARSED_ACCT_NUM_ENCR IS NOT NULL)
   GROUP BY TRAN_RECEIVED_RAW_ACCT_DATA;
   
SELECT *
  FROM PSS.TRAN 
 WHERE CONSUMER_ACCT_ID IS NULL
   AND TRAN_RECEIVED_RAW_ACCT_DATA IS NOT NULL
   AND REGEXP_LIKE(TRAN_RECEIVED_RAW_ACCT_DATA, '[0-9]{4,}')
   AND NOT REGEXP_LIKE(TRAN_RECEIVED_RAW_ACCT_DATA, '[0-9]{2,}[*]{1,}[0-9]{4,}')
   AND (       TRAN_PARSED_ACCT_NUM_HASH IS NOT NULL OR
               TRAN_PARSED_ACCT_NUM_ENCR IS NOT NULL);
SELECT *
  FROM PSS.TRAN
  WHERE (TRAN_PARSED_ACCT_NAME IS NOT NULL OR
               TRAN_PARSED_ACCT_EXP_DATE IS NOT NULL OR
               TRAN_PARSED_ACCT_NUM IS NOT NULL OR
               TRAN_PARSED_ACCT_NUM_HASH IS NOT NULL OR
               TRAN_PARSED_ACCT_NUM_ENCR IS NOT NULL OR
               TRAN_REPORTABLE_ACCT_NUM IS NOT NULL OR
               TRAN_LEGACY_TRANS_NO IS NOT NULL OR
               TRAN_ACCOUNT_PIN IS NOT NULL OR
               TRAN_DESC IS NOT NULL);
*/     
DECLARE
    ln_start PSS.TRAN.TRAN_ID%TYPE;
    ln_end PSS.TRAN.TRAN_ID%TYPE;
    ln_interval PLS_INTEGER := 100000;
    ln_cnt PLS_INTEGER;
BEGIN
    SELECT MIN(TRAN_ID), MAX(TRAN_ID)
      INTO ln_start, ln_end
      FROM PSS.TRAN;
    WHILE ln_start <= ln_end LOOP
        UPDATE PSS.TRAN
           SET TRAN_PARSED_ACCT_NAME = NULL,
               TRAN_PARSED_ACCT_EXP_DATE = NULL,
               TRAN_PARSED_ACCT_NUM = NULL,
               TRAN_PARSED_ACCT_NUM_HASH = NULL,
               TRAN_PARSED_ACCT_NUM_ENCR = NULL,
               TRAN_REPORTABLE_ACCT_NUM = NULL,
               TRAN_LEGACY_TRANS_NO = NULL,
               TRAN_ACCOUNT_PIN = NULL,
               TRAN_DESC = NULL
         WHERE (TRAN_PARSED_ACCT_NAME IS NOT NULL OR
               TRAN_PARSED_ACCT_EXP_DATE IS NOT NULL OR
               TRAN_PARSED_ACCT_NUM IS NOT NULL OR
               TRAN_PARSED_ACCT_NUM_HASH IS NOT NULL OR
               TRAN_PARSED_ACCT_NUM_ENCR IS NOT NULL OR
               TRAN_REPORTABLE_ACCT_NUM IS NOT NULL OR
               TRAN_LEGACY_TRANS_NO IS NOT NULL OR
               TRAN_ACCOUNT_PIN IS NOT NULL OR
               TRAN_DESC IS NOT NULL)
           AND TRAN_ID BETWEEN ln_start AND ln_start + ln_interval - 1;
        ln_cnt := SQL%ROWCOUNT;
        COMMIT;
        DBMS_OUTPUT.PUT_LINE('Updated ' || ln_cnt || ' rows between ' || ln_start || ' AND ' || (ln_start + ln_interval - 1));
        ln_start := ln_start + ln_interval;
    END LOOP;
END;
/
       
ALTER TABLE PSS.TRAN SET UNUSED(TRAN_PARSED_ACCT_NAME, TRAN_PARSED_ACCT_EXP_DATE, TRAN_PARSED_ACCT_NUM, TRAN_PARSED_ACCT_NUM_HASH, TRAN_PARSED_ACCT_NUM_ENCR ,TRAN_REPORTABLE_ACCT_NUM, TRAN_LEGACY_TRANS_NO, TRAN_ACCOUNT_PIN, TRAN_DESC);


