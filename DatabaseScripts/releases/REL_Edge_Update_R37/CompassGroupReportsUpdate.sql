INSERT INTO FOLIO_CONF.FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) 
    SELECT FP.FOLIO_PILLAR_ID, 1, 553, 'ASC', 0, 0 
      FROM FOLIO_CONF.FOLIO_PILLAR FP
     WHERE FP.FOLIO_ID IN(1552,1554)
       AND FP.PILLAR_LABEL = 'LITERAL:Card Id'
       AND NOT EXISTS(SELECT 1 FROM FOLIO_CONF.FOLIO_PILLAR_FIELD FPF WHERE FP.FOLIO_PILLAR_ID = FPF.FOLIO_PILLAR_ID);

UPDATE REPORT.REPORT_PARAM 
   SET PARAM_VALUE = DECODE(PARAM_VALUE, '536', '1552', '556', '1554', PARAM_VALUE)
 WHERE PARAM_NAME = 'folioId'
   AND PARAM_VALUE IN('536', '556');
   
COMMIT;

/*
       
SELECT * FROM FOLIO_CONF.FOLIO WHERE FOLIO_NAME LIKE 'Compass%';
536 => 1552
556 => 1554

SELECT * 
  FROM REPORT.REPORTS R
  JOIN REPORT.REPORT_PARAM RP ON R.REPORT_ID = RP.REPORT_ID
 WHERE RP.PARAM_NAME = 'folioId'
   AND RP.PARAM_VALUE IN('536', '556');

SELECT * 
  FROM REPORT.REPORTS R
  JOIN REPORT.REPORT_PARAM RP ON R.REPORT_ID = RP.REPORT_ID
 WHERE R.REPORT_ID IN(144, 145);
 
SELECT * 
  FROM REPORT.REPORT_PARAM R
 WHERE R.REPORT_ID IN(144, 145);
 
 select * from report.user_login u
 join report.user_report ur on u.user_id = ur.user_id
 --join report.ccs_transport_property tp on ur.ccs_transport_id = tp.ccs_transport_id
 where ur.report_id in(144,145)
 and u.user_id = 2061;
 
 UPDATE report.user_report set status = 'A', upd_dt = to_date('01/01/2013', 'MM/DD/YYYY') where user_report_id in(2854,2855);
 
 select * from corp.doc d
 join report.user_customer_bank ucb on d.customer_bank_id = ucb.customer_bank_Id
 where d.sent_date > to_date('01/01/2013', 'MM/DD/YYYY')
 and ucb.user_id = 2061;
 
 update corp.doc
   set status = 'P'
   where status = 'S'
   and doc_id in(66377, 122010, 248261);
   
 select distinct eb.* 
 from report.export_batch eb
 join report.terminal t on eb.customer_id = t.customer_id
 join report.user_terminal ut on t.terminal_id = ut.terminal_id
 where eb.EXPORT_SENT > to_date('01/01/2013', 'MM/DD/YYYY')
 and ut.user_id = 2061;
 
 update report.export_batch 
  set status = 'A'
   where status = 'S'
   and batch_id IN(
 1788488,
1788492,
1787248,
1788678);

*/

