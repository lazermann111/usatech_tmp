update web_content.literal set literal_value='We at USA Technologies, Inc. are continually striving to enhance our customers'' power to run their business effectively. The USALive website is loaded with features to help you do just that. For the best website experience, please use the latest major version of your browser. Now new in Version 1.13: <ul><li>Customers may make a secure credit card payment for an order by following the Complete Order link.</li><li>All cards are assigned a unique Card Id not related to the card account number in any way.  Card Id has been added to numerous reports.</li><li>Using the Card Id allows repeat card usage to be easily identified and tracked.</li><li>New Transaction Types have been added to many reports to support new Operator Serviced MORE prepaid cards.</li><li>Report output format icons have been added to report links to streamline report generation in various formats.</li><li>Report generation speed enhancements.</li><li>Minor bug fixes.</li></ul>' where literal_key='home-page-message' and subdomain_id is null;
commit;

-- check trans
/*
SELECT *
  FROM PSS.TRAN 
 WHERE CONSUMER_ACCT_ID IS NULL
   AND TRAN_RECEIVED_RAW_ACCT_DATA IS NOT NULL
   AND (TRAN_PARSED_ACCT_NUM_ENCR IS NOT NULL OR REGEXP_LIKE(TRAN_RECEIVED_RAW_ACCT_DATA, '^[^A-Za-z0-9*@]*[0-9]{4,}([=^].*)?[^A-Za-z0-9*@]*$'));

SELECT TRAN_RECEIVED_RAW_ACCT_DATA, COUNT(*)
  FROM PSS.TRAN 
 WHERE CONSUMER_ACCT_ID IS NULL
   AND TRAN_RECEIVED_RAW_ACCT_DATA IS NOT NULL
   AND REGEXP_LIKE(TRAN_RECEIVED_RAW_ACCT_DATA, '[0-9]{4,}')
   AND NOT REGEXP_LIKE(TRAN_RECEIVED_RAW_ACCT_DATA, '[0-9]{2,}[*]{1,}[0-9]{4,}')
   AND (TRAN_PARSED_ACCT_NUM IS NOT NULL OR
               TRAN_PARSED_ACCT_NUM_HASH IS NOT NULL OR
               TRAN_PARSED_ACCT_NUM_ENCR IS NOT NULL)
   GROUP BY TRAN_RECEIVED_RAW_ACCT_DATA;
   
SELECT *
  FROM PSS.TRAN 
 WHERE CONSUMER_ACCT_ID IS NULL
   AND TRAN_RECEIVED_RAW_ACCT_DATA IS NOT NULL
   AND REGEXP_LIKE(TRAN_RECEIVED_RAW_ACCT_DATA, '[0-9]{4,}')
   AND NOT REGEXP_LIKE(TRAN_RECEIVED_RAW_ACCT_DATA, '[0-9]{2,}[*]{1,}[0-9]{4,}')
   AND (       TRAN_PARSED_ACCT_NUM_HASH IS NOT NULL OR
               TRAN_PARSED_ACCT_NUM_ENCR IS NOT NULL);
SELECT *
  FROM PSS.TRAN
  WHERE (TRAN_PARSED_ACCT_NAME IS NOT NULL OR
               TRAN_PARSED_ACCT_EXP_DATE IS NOT NULL OR
               TRAN_PARSED_ACCT_NUM IS NOT NULL OR
               TRAN_PARSED_ACCT_NUM_HASH IS NOT NULL OR
               TRAN_PARSED_ACCT_NUM_ENCR IS NOT NULL OR
               TRAN_REPORTABLE_ACCT_NUM IS NOT NULL OR
               TRAN_LEGACY_TRANS_NO IS NOT NULL OR
               TRAN_ACCOUNT_PIN IS NOT NULL OR
               TRAN_DESC IS NOT NULL);
*/     
DECLARE
    ln_start PSS.TRAN.TRAN_ID%TYPE;
    ln_end PSS.TRAN.TRAN_ID%TYPE;
    ln_interval PLS_INTEGER := 100000;
    ln_cnt PLS_INTEGER;
BEGIN
    SELECT MIN(TRAN_ID), MAX(TRAN_ID)
      INTO ln_start, ln_end
      FROM PSS.TRAN;
    WHILE ln_start <= ln_end LOOP
        UPDATE PSS.TRAN
           SET TRAN_PARSED_ACCT_NAME = NULL,
               TRAN_PARSED_ACCT_EXP_DATE = NULL,
               TRAN_PARSED_ACCT_NUM = NULL,
               TRAN_PARSED_ACCT_NUM_HASH = NULL,
               TRAN_PARSED_ACCT_NUM_ENCR = NULL,
               TRAN_REPORTABLE_ACCT_NUM = NULL,
               TRAN_LEGACY_TRANS_NO = NULL,
               TRAN_ACCOUNT_PIN = NULL,
               TRAN_DESC = NULL
         WHERE (TRAN_PARSED_ACCT_NAME IS NOT NULL OR
               TRAN_PARSED_ACCT_EXP_DATE IS NOT NULL OR
               TRAN_PARSED_ACCT_NUM IS NOT NULL OR
               TRAN_PARSED_ACCT_NUM_HASH IS NOT NULL OR
               TRAN_PARSED_ACCT_NUM_ENCR IS NOT NULL OR
               TRAN_REPORTABLE_ACCT_NUM IS NOT NULL OR
               TRAN_LEGACY_TRANS_NO IS NOT NULL OR
               TRAN_ACCOUNT_PIN IS NOT NULL OR
               TRAN_DESC IS NOT NULL)
           AND TRAN_ID BETWEEN ln_start AND ln_start + ln_interval - 1;
        ln_cnt := SQL%ROWCOUNT;
        COMMIT;
        DBMS_OUTPUT.PUT_LINE('Updated ' || ln_cnt || ' rows between ' || ln_start || ' AND ' || (ln_start + ln_interval - 1));
        ln_start := ln_start + ln_interval;
    END LOOP;
END;
/
       
ALTER TABLE PSS.TRAN SET UNUSED(TRAN_PARSED_ACCT_NAME, TRAN_PARSED_ACCT_EXP_DATE, TRAN_PARSED_ACCT_NUM, TRAN_PARSED_ACCT_NUM_HASH, TRAN_PARSED_ACCT_NUM_ENCR ,TRAN_REPORTABLE_ACCT_NUM, TRAN_LEGACY_TRANS_NO, TRAN_ACCOUNT_PIN, TRAN_DESC);

