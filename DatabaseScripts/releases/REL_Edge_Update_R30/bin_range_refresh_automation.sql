insert into ENGINE.APP_SETTING (app_setting_cd,app_setting_value, app_setting_desc)
values('IND_DB_FILE_MOD_TIME','1329512520000','The modification time string for ind_db_ardef.txt');

insert into ENGINE.APP_SETTING (app_setting_cd,app_setting_value, app_setting_desc)
values('IND_DB_FILE_MOD_TIME_PREAPPROVE','1329512520000','The modification time string for ind_db_ardef.txt that needs manual approval.');

insert into ENGINE.APP_SETTING (app_setting_cd,app_setting_value, app_setting_desc)
values('IND_DB_FILE_LAST_UPLOAD_DIFF_PERCENTAGE','0','The last uploaded ind_db_ardef.txt percentage difference from the previous.');
commit;

GRANT SELECT, UPDATE ON ENGINE.APP_SETTING TO USAT_POSM_ROLE;