CREATE OR REPLACE PACKAGE BODY PSS.PKG_PREPAID
IS
    -- R36 and below
    PROCEDURE CREATE_PREPAID_CONSUMER(
        pb_card_hash PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD_HASH%TYPE,
        pv_security_code PSS.CONSUMER_ACCT.SECURITY_CD_HASH%TYPE,
        pv_email PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pv_first PSS.CONSUMER.CONSUMER_FNAME%TYPE,
        pv_last PSS.CONSUMER.CONSUMER_LNAME%TYPE,
        pv_addr1 PSS.CONSUMER.CONSUMER_ADDR1%TYPE,
        pv_city PSS.CONSUMER.CONSUMER_CITY%TYPE,
        pv_state PSS.CONSUMER.CONSUMER_STATE_CD%TYPE,
        pv_postal PSS.CONSUMER.CONSUMER_POSTAL_CD%TYPE,
        pv_country PSS.CONSUMER.CONSUMER_COUNTRY_CD%TYPE,
        pb_credential_hash PSS.CONSUMER_CREDENTIAL.CREDENTIAL_HASH%TYPE,
        pb_credential_salt PSS.CONSUMER_CREDENTIAL.CREDENTIAL_SALT%TYPE,
        pv_credential_alg PSS.CONSUMER_CREDENTIAL.CREDENTIAL_ALG%TYPE,
        pn_consumer_id OUT PSS.CONSUMER.CONSUMER_ID%TYPE,
        pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
        pv_passcode OUT PSS.CONSUMER_PASSCODE.PASSCODE%TYPE)
    IS
        ln_cnt PLS_INTEGER;
        lc_flag CHAR(1);
        lv_lock VARCHAR2(28);		
    BEGIN
        lv_lock := DBADMIN.PKG_GLOBAL.REQUEST_LOCK('PSS.CONSUMER_ACCT', RAWTOHEX(pb_card_hash)); 
        SELECT COUNT(*), MAX(DECODE(C.CONSUMER_TYPE_ID, 4, CA.CONSUMER_ACCT_ID)), 
               NVL(MAX(CASE 
                    WHEN C.CONSUMER_TYPE_ID = 5 AND TRIM(UPPER(C.CONSUMER_EMAIL_ADDR1)) = TRIM(UPPER(pv_email)) AND CC.CREDENTIAL_ACTIVE_FLAG = 'Y' THEN 'Y' -- Registered and matches and not new
                    WHEN C.CONSUMER_TYPE_ID = 5 AND TRIM(UPPER(C.CONSUMER_EMAIL_ADDR1)) = TRIM(UPPER(pv_email)) AND CC.CREDENTIAL_ACTIVE_FLAG = 'N' THEN 'A' -- Registered and matches and new - so send email
                    END), 'N'),
               MAX(CASE 
                    WHEN TRIM(UPPER(C.CONSUMER_EMAIL_ADDR1)) = TRIM(UPPER(pv_email)) THEN C.CONSUMER_ID -- Registered and matches and new - so send email
                    END)
          INTO ln_cnt, pn_consumer_acct_id, lc_flag, pn_consumer_id
          FROM PSS.CONSUMER_ACCT CA
          JOIN PSS.CONSUMER C ON CA.CONSUMER_ID = C.CONSUMER_ID
          LEFT OUTER JOIN PSS.CONSUMER_CREDENTIAL CC ON C.CONSUMER_ID = CC.CONSUMER_ID AND CC.RESOURCE_KEY = 'PREPAID_WEBSITE'
         WHERE CA.CONSUMER_ACCT_CD_HASH = pb_card_hash           
           AND CA.CONSUMER_ACCT_FMT_ID = 2
           AND CA.CONSUMER_ACCT_TYPE_ID = 3
		   AND DBADMIN.PKG_UTL.EQL(CA.SECURITY_CD_HASH, DBADMIN.GET_HASH(UTL_RAW.CAST_TO_RAW(pv_security_code), CA.SECURITY_CD_SALT, 'SHA-256/1000')) = 'Y';
        IF ln_cnt = 0 THEN
            RAISE_APPLICATION_ERROR(-20300, 'Did not find an account for card hash ' || RAWTOHEX(pb_card_hash));
        ELSIF pn_consumer_acct_id IS NULL THEN
            IF lc_flag = 'A' THEN
                DELETE 
                  FROM PSS.CONSUMER_CREDENTIAL
                 WHERE CONSUMER_ID = pn_consumer_id
                   AND RESOURCE_KEY = 'PREPAID_WEBSITE';
                INSERT INTO PSS.CONSUMER_CREDENTIAL(CONSUMER_ID, CREDENTIAL_HASH, CREDENTIAL_SALT, CREDENTIAL_ALG, RESOURCE_KEY)
                    VALUES(pn_consumer_id, pb_credential_hash, pb_credential_salt, pv_credential_alg, 'PREPAID_WEBSITE');
                pv_passcode := PSS.PKG_CONSUMER_MAINT.CREATE_CONSUMER_PASSCODE(pn_consumer_id, 'REGISTER_PREPAID');
            ELSIF lc_flag = 'Y' THEN
                RAISE_APPLICATION_ERROR(-20301, 'Account already registered to this email');
            ELSE
                RAISE_APPLICATION_ERROR(-20302, 'Account already registered to another email');
            END IF;
        ELSE
            IF pv_state IS NOT NULL THEN
                SELECT COUNT(*)
                  INTO ln_cnt
                  FROM LOCATION.STATE
                 WHERE STATE_CD = pv_state;
                IF ln_cnt = 0 THEN
                    RAISE_APPLICATION_ERROR(-20309, 'Invalid state ' || pv_state || ' provided');
                END IF;
            END IF;
            SELECT PSS.SEQ_CONSUMER_ID.NEXTVAL
              INTO pn_consumer_id
              FROM DUAL;
            BEGIN
                INSERT INTO PSS.CONSUMER(
                    CONSUMER_ID,
                    CONSUMER_EMAIL_ADDR1,
                    CONSUMER_FNAME,
                    CONSUMER_LNAME,
                    CONSUMER_ADDR1,
                    CONSUMER_CITY,
                    CONSUMER_STATE_CD,
                    CONSUMER_POSTAL_CD,
                    CONSUMER_COUNTRY_CD,
                    CONSUMER_TYPE_ID)
                VALUES(
                    pn_consumer_id,
                    pv_email,
                    pv_first,
                    pv_last,
                    pv_addr1,
                    pv_city,
                    pv_state,
                    pv_postal,
                    pv_country,
                    5);
            EXCEPTION
                WHEN DUP_VAL_ON_INDEX THEN
                    RAISE_APPLICATION_ERROR(-20303, 'This email is already registered with another card.');
            END;
            UPDATE PSS.CONSUMER_ACCT
               SET CONSUMER_ID = pn_consumer_id
             WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id;
            INSERT INTO PSS.CONSUMER_CREDENTIAL(CONSUMER_ID, CREDENTIAL_HASH, CREDENTIAL_SALT, CREDENTIAL_ALG, RESOURCE_KEY)
                VALUES(pn_consumer_id, pb_credential_hash, pb_credential_salt, pv_credential_alg, 'PREPAID_WEBSITE');
            pv_passcode := PSS.PKG_CONSUMER_MAINT.CREATE_CONSUMER_PASSCODE(pn_consumer_id, 'REGISTER_PREPAID');        
        END IF;
    END;
    
    -- R37 and above
    PROCEDURE CREATE_PREPAID_CONSUMER(
        pn_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
        pv_security_code PSS.CONSUMER_ACCT.SECURITY_CD_HASH%TYPE,
        pv_email PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pv_first PSS.CONSUMER.CONSUMER_FNAME%TYPE,
        pv_last PSS.CONSUMER.CONSUMER_LNAME%TYPE,
        pv_addr1 PSS.CONSUMER.CONSUMER_ADDR1%TYPE,
        pv_city PSS.CONSUMER.CONSUMER_CITY%TYPE,
        pv_state PSS.CONSUMER.CONSUMER_STATE_CD%TYPE,
        pv_postal PSS.CONSUMER.CONSUMER_POSTAL_CD%TYPE,
        pv_country PSS.CONSUMER.CONSUMER_COUNTRY_CD%TYPE,
        pb_credential_hash PSS.CONSUMER_CREDENTIAL.CREDENTIAL_HASH%TYPE,
        pb_credential_salt PSS.CONSUMER_CREDENTIAL.CREDENTIAL_SALT%TYPE,
        pv_credential_alg PSS.CONSUMER_CREDENTIAL.CREDENTIAL_ALG%TYPE,
        pn_consumer_id OUT PSS.CONSUMER.CONSUMER_ID%TYPE,
        pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
        pv_passcode OUT PSS.CONSUMER_PASSCODE.PASSCODE%TYPE)
    IS
        ln_cnt PLS_INTEGER;
        lc_flag CHAR(1);
        lv_lock VARCHAR2(28);
		ln_consumer_type_id PSS.CONSUMER.CONSUMER_TYPE_ID%TYPE;
    BEGIN
        lv_lock := DBADMIN.PKG_GLOBAL.REQUEST_LOCK('PSS.CONSUMER_ACCT', pn_global_account_id); 
        SELECT COUNT(*), MAX(CA.CONSUMER_ACCT_ID), 
               NVL(MAX(CASE 
                    WHEN C.CONSUMER_TYPE_ID = 5 AND TRIM(UPPER(C.CONSUMER_EMAIL_ADDR1)) = TRIM(UPPER(pv_email)) AND CC.CREDENTIAL_ACTIVE_FLAG = 'Y' THEN 'Y' -- Registered and matches and not new
                    WHEN C.CONSUMER_TYPE_ID = 5 AND TRIM(UPPER(C.CONSUMER_EMAIL_ADDR1)) = TRIM(UPPER(pv_email)) AND CC.CREDENTIAL_ACTIVE_FLAG = 'N' THEN 'A' -- Registered and matches and new - so send email
                    END), 'N'),
               MAX(CASE 
                    WHEN TRIM(UPPER(C.CONSUMER_EMAIL_ADDR1)) = TRIM(UPPER(pv_email)) THEN C.CONSUMER_ID -- Registered and matches and new - so send email
                    END),
				MAX(C.CONSUMER_TYPE_ID)
          INTO ln_cnt, pn_consumer_acct_id, lc_flag, pn_consumer_id, ln_consumer_type_id
          FROM PSS.CONSUMER_ACCT CA
          JOIN PSS.CONSUMER_ACCT_BASE CAB ON CA.CONSUMER_ACCT_ID = CAB.CONSUMER_ACCT_ID
          JOIN PSS.CONSUMER C ON CA.CONSUMER_ID = C.CONSUMER_ID
          LEFT OUTER JOIN PSS.CONSUMER_CREDENTIAL CC ON C.CONSUMER_ID = CC.CONSUMER_ID AND CC.RESOURCE_KEY = 'PREPAID_WEBSITE'
         WHERE CAB.GLOBAL_ACCOUNT_ID = pn_global_account_id           
           AND CA.CONSUMER_ACCT_FMT_ID = 2
           AND CA.CONSUMER_ACCT_TYPE_ID = 3
           AND CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y'
		   AND DBADMIN.PKG_UTL.EQL(CA.SECURITY_CD_HASH, DBADMIN.GET_HASH(UTL_RAW.CAST_TO_RAW(pv_security_code), CA.SECURITY_CD_SALT, 'SHA-256/1000')) = 'Y';
        IF ln_cnt = 0 THEN
            RAISE_APPLICATION_ERROR(-20300, 'Did not find an account for card id ' || pn_global_account_id);
        ELSIF ln_consumer_type_id != 4 THEN
            IF lc_flag = 'A' THEN
                DELETE 
                  FROM PSS.CONSUMER_CREDENTIAL
                 WHERE CONSUMER_ID = pn_consumer_id
                   AND RESOURCE_KEY = 'PREPAID_WEBSITE';
                INSERT INTO PSS.CONSUMER_CREDENTIAL(CONSUMER_ID, CREDENTIAL_HASH, CREDENTIAL_SALT, CREDENTIAL_ALG, RESOURCE_KEY)
                    VALUES(pn_consumer_id, pb_credential_hash, pb_credential_salt, pv_credential_alg, 'PREPAID_WEBSITE');
                pv_passcode := PSS.PKG_CONSUMER_MAINT.CREATE_CONSUMER_PASSCODE(pn_consumer_id, 'REGISTER_PREPAID');
            ELSIF lc_flag = 'Y' THEN
                RAISE_APPLICATION_ERROR(-20301, 'Account already registered to this email');
            ELSE
                RAISE_APPLICATION_ERROR(-20302, 'Account already registered to another email');
            END IF;
        ELSE
            IF pv_state IS NOT NULL THEN
                SELECT COUNT(*)
                  INTO ln_cnt
                  FROM LOCATION.STATE
                 WHERE STATE_CD = pv_state;
                IF ln_cnt = 0 THEN
                    RAISE_APPLICATION_ERROR(-20309, 'Invalid state ' || pv_state || ' provided');
                END IF;
            END IF;
            SELECT PSS.SEQ_CONSUMER_ID.NEXTVAL
              INTO pn_consumer_id
              FROM DUAL;
            BEGIN
                INSERT INTO PSS.CONSUMER(
                    CONSUMER_ID,
                    CONSUMER_EMAIL_ADDR1,
                    CONSUMER_FNAME,
                    CONSUMER_LNAME,
                    CONSUMER_ADDR1,
                    CONSUMER_CITY,
                    CONSUMER_STATE_CD,
                    CONSUMER_POSTAL_CD,
                    CONSUMER_COUNTRY_CD,
                    CONSUMER_TYPE_ID)
                VALUES(
                    pn_consumer_id,
                    pv_email,
                    pv_first,
                    pv_last,
                    pv_addr1,
                    pv_city,
                    pv_state,
                    pv_postal,
                    pv_country,
                    5);
            EXCEPTION
                WHEN DUP_VAL_ON_INDEX THEN
                    RAISE_APPLICATION_ERROR(-20303, 'This email is already registered with another card.');
            END;
            UPDATE PSS.CONSUMER_ACCT
               SET CONSUMER_ID = pn_consumer_id
             WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id;
            INSERT INTO PSS.CONSUMER_CREDENTIAL(CONSUMER_ID, CREDENTIAL_HASH, CREDENTIAL_SALT, CREDENTIAL_ALG, RESOURCE_KEY)
                VALUES(pn_consumer_id, pb_credential_hash, pb_credential_salt, pv_credential_alg, 'PREPAID_WEBSITE');
            pv_passcode := PSS.PKG_CONSUMER_MAINT.CREATE_CONSUMER_PASSCODE(pn_consumer_id, 'REGISTER_PREPAID');        
        END IF;
    END;
    
    -- R36 and below
    PROCEDURE ADD_PREPAID_ACCT(
        pv_email PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pb_card_hash PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD_HASH%TYPE,
        pv_security_code PSS.CONSUMER_ACCT.SECURITY_CD_HASH%TYPE,
        pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE)
    IS
        ln_cnt PLS_INTEGER;
        lc_flag CHAR(1);
        lv_lock VARCHAR2(28);
        ln_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE;
    BEGIN
        lv_lock := DBADMIN.PKG_GLOBAL.REQUEST_LOCK('PSS.CONSUMER_ACCT', RAWTOHEX(pb_card_hash));
        SELECT C.CONSUMER_ID, CC.CREDENTIAL_ACTIVE_FLAG
          INTO ln_consumer_id, lc_flag
          FROM PSS.CONSUMER C
          LEFT OUTER JOIN PSS.CONSUMER_CREDENTIAL CC ON C.CONSUMER_ID = CC.CONSUMER_ID AND CC.RESOURCE_KEY = 'PREPAID_WEBSITE'
         WHERE TRIM(UPPER(C.CONSUMER_EMAIL_ADDR1)) = TRIM(UPPER(pv_email))
           AND C.CONSUMER_TYPE_ID = 5;      
        IF lc_flag != 'Y' THEN
            RAISE_APPLICATION_ERROR(-20304, 'Account not active');
        END IF;   
        SELECT COUNT(*), MAX(DECODE(C.CONSUMER_TYPE_ID, 4, CA.CONSUMER_ACCT_ID)), MAX(DECODE(C.CONSUMER_ID, ln_consumer_id, 'Y', 'N'))
          INTO ln_cnt, pn_consumer_acct_id, lc_flag
          FROM PSS.CONSUMER_ACCT CA
          JOIN PSS.CONSUMER C ON CA.CONSUMER_ID = C.CONSUMER_ID
         WHERE CA.CONSUMER_ACCT_CD_HASH = pb_card_hash           
           AND CA.CONSUMER_ACCT_FMT_ID = 2
           AND CA.CONSUMER_ACCT_TYPE_ID = 3
		   AND DBADMIN.PKG_UTL.EQL(CA.SECURITY_CD_HASH, DBADMIN.GET_HASH(UTL_RAW.CAST_TO_RAW(pv_security_code), CA.SECURITY_CD_SALT, 'SHA-256/1000')) = 'Y';
        IF ln_cnt = 0 THEN
            RAISE_APPLICATION_ERROR(-20300, 'Did not find an account for card hash ' || RAWTOHEX(pb_card_hash));
        ELSIF pn_consumer_acct_id IS NULL THEN
            IF lc_flag = 'Y' THEN
                RAISE_APPLICATION_ERROR(-20301, 'Account already registered to this email');
            ELSE
                RAISE_APPLICATION_ERROR(-20302, 'Account already registered to another email');
            END IF;
        ELSE
            UPDATE PSS.CONSUMER_ACCT
               SET CONSUMER_ID = ln_consumer_id
             WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id;
        END IF;
    END;

    -- R37 and above
    PROCEDURE ADD_PREPAID_ACCT(
        pv_email PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pn_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
        pv_security_code PSS.CONSUMER_ACCT.SECURITY_CD_HASH%TYPE,
        pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE)
    IS
        ln_cnt PLS_INTEGER;
        lc_flag CHAR(1);
        lv_lock VARCHAR2(28);
        ln_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE;
    BEGIN
        lv_lock := DBADMIN.PKG_GLOBAL.REQUEST_LOCK('PSS.CONSUMER_ACCT', pn_global_account_id);
        SELECT C.CONSUMER_ID, CC.CREDENTIAL_ACTIVE_FLAG
          INTO ln_consumer_id, lc_flag
          FROM PSS.CONSUMER C
          LEFT OUTER JOIN PSS.CONSUMER_CREDENTIAL CC ON C.CONSUMER_ID = CC.CONSUMER_ID AND CC.RESOURCE_KEY = 'PREPAID_WEBSITE'
         WHERE TRIM(UPPER(C.CONSUMER_EMAIL_ADDR1)) = TRIM(UPPER(pv_email))
           AND C.CONSUMER_TYPE_ID = 5;      
        IF lc_flag != 'Y' THEN
            RAISE_APPLICATION_ERROR(-20304, 'Account not active');
        END IF;   
        SELECT COUNT(*), MAX(DECODE(C.CONSUMER_TYPE_ID, 4, CA.CONSUMER_ACCT_ID)), MAX(DECODE(C.CONSUMER_ID, ln_consumer_id, 'Y', 'N'))
          INTO ln_cnt, pn_consumer_acct_id, lc_flag
          FROM PSS.CONSUMER_ACCT CA
          JOIN PSS.CONSUMER_ACCT_BASE CAB ON CA.CONSUMER_ACCT_ID = CAB.CONSUMER_ACCT_ID
          JOIN PSS.CONSUMER C ON CA.CONSUMER_ID = C.CONSUMER_ID
         WHERE CAB.GLOBAL_ACCOUNT_ID = pn_global_account_id           
           AND CA.CONSUMER_ACCT_FMT_ID = 2
           AND CA.CONSUMER_ACCT_TYPE_ID = 3
		   AND CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y'
		   AND DBADMIN.PKG_UTL.EQL(CA.SECURITY_CD_HASH, DBADMIN.GET_HASH(UTL_RAW.CAST_TO_RAW(pv_security_code), CA.SECURITY_CD_SALT, 'SHA-256/1000')) = 'Y';
        IF ln_cnt = 0 THEN
            RAISE_APPLICATION_ERROR(-20300, 'Did not find an account for card id ' || pn_global_account_id);
        ELSIF pn_consumer_acct_id IS NULL THEN
            IF lc_flag = 'Y' THEN
                RAISE_APPLICATION_ERROR(-20301, 'Account already registered to this email');
            ELSE
                RAISE_APPLICATION_ERROR(-20302, 'Account already registered to another email');
            END IF;
        ELSE
            UPDATE PSS.CONSUMER_ACCT
               SET CONSUMER_ID = ln_consumer_id
             WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id;
        END IF;
    END;
    
    PROCEDURE UPDATE_PREPAID_CONSUMER(
        pv_old_email PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pv_email PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pv_first PSS.CONSUMER.CONSUMER_FNAME%TYPE,
        pv_last PSS.CONSUMER.CONSUMER_LNAME%TYPE,
        pv_addr1 PSS.CONSUMER.CONSUMER_ADDR1%TYPE,
        pv_city PSS.CONSUMER.CONSUMER_CITY%TYPE,
        pv_state PSS.CONSUMER.CONSUMER_STATE_CD%TYPE,
        pv_postal PSS.CONSUMER.CONSUMER_POSTAL_CD%TYPE,
        pv_country PSS.CONSUMER.CONSUMER_COUNTRY_CD%TYPE,
        pb_credential_hash PSS.CONSUMER_CREDENTIAL.CREDENTIAL_HASH%TYPE,
        pb_credential_salt PSS.CONSUMER_CREDENTIAL.CREDENTIAL_SALT%TYPE,
        pv_credential_alg PSS.CONSUMER_CREDENTIAL.CREDENTIAL_ALG%TYPE,
        pn_consumer_id OUT PSS.CONSUMER.CONSUMER_ID%TYPE,
        pv_passcode OUT PSS.CONSUMER_PASSCODE.PASSCODE%TYPE)
    IS
        ln_cnt PLS_INTEGER;
        lc_flag CHAR(1);
    BEGIN
        IF pv_state IS NOT NULL THEN
            SELECT COUNT(*)
              INTO ln_cnt
              FROM LOCATION.STATE
             WHERE STATE_CD = pv_state;
            IF ln_cnt = 0 THEN
                RAISE_APPLICATION_ERROR(-20309, 'Invalid state ' || pv_state || ' provided');
            END IF;
        END IF;
        UPDATE PSS.CONSUMER
           SET CONSUMER_EMAIL_ADDR1 = TRIM(pv_email),
               CONSUMER_FNAME = pv_first,
               CONSUMER_LNAME = pv_last,
               CONSUMER_ADDR1 = pv_addr1,
               CONSUMER_CITY = pv_city,
               CONSUMER_STATE_CD = pv_state,
               CONSUMER_POSTAL_CD = pv_postal,
               CONSUMER_COUNTRY_CD = pv_country
         WHERE TRIM(UPPER(CONSUMER_EMAIL_ADDR1)) = TRIM(UPPER(pv_old_email))
           AND CONSUMER_TYPE_ID = 5
          RETURNING CONSUMER_ID
          INTO pn_consumer_id;
         
        IF pb_credential_hash IS NOT NULL THEN
            UPDATE PSS.CONSUMER_CREDENTIAL
               SET CREDENTIAL_HASH = pb_credential_hash, 
                   CREDENTIAL_SALT = pb_credential_salt,
                   CREDENTIAL_ALG = pv_credential_alg,
                   CREDENTIAL_ACTIVE_FLAG = 'Y'
             WHERE CONSUMER_ID = pn_consumer_id
               AND RESOURCE_KEY = 'PREPAID_WEBSITE';
            IF TRIM(UPPER(pv_email)) != TRIM(UPPER(pv_old_email)) THEN
                pv_passcode := PSS.PKG_CONSUMER_MAINT.CREATE_CONSUMER_PASSCODE(pn_consumer_id, 'REGISTER_PREPAID');     
            END IF;
        ELSIF TRIM(UPPER(pv_email)) != TRIM(UPPER(pv_old_email)) THEN
            UPDATE PSS.CONSUMER_CREDENTIAL
               SET CREDENTIAL_ACTIVE_FLAG = 'Y'
             WHERE CONSUMER_ID = pn_consumer_id
               AND RESOURCE_KEY = 'PREPAID_WEBSITE';
            pv_passcode := PSS.PKG_CONSUMER_MAINT.CREATE_CONSUMER_PASSCODE(pn_consumer_id, 'REGISTER_PREPAID');
        END IF;
    EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
            RAISE_APPLICATION_ERROR(-20303, 'This email is already registered with another card.');
    END;
    
    PROCEDURE REGISTER_PREPAID_CONSUMER(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pv_passcode PSS.CONSUMER_PASSCODE.PASSCODE%TYPE,
        pv_email OUT PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pv_first OUT PSS.CONSUMER.CONSUMER_FNAME%TYPE,
        pv_last OUT PSS.CONSUMER.CONSUMER_LNAME%TYPE)
    IS
        ld_exp_ts PSS.CONSUMER_PASSCODE.EXPIRATION_TS%TYPE;
        ln_consumer_passcode_id PSS.CONSUMER_PASSCODE.CONSUMER_PASSCODE_ID%TYPE;
        lc_active_flag PSS.CONSUMER_CREDENTIAL.CREDENTIAL_ACTIVE_FLAG%TYPE;
    BEGIN
        SELECT C.CONSUMER_EMAIL_ADDR1,
               C.CONSUMER_FNAME, 
               C.CONSUMER_LNAME,
               CP.EXPIRATION_TS,
               CP.CONSUMER_PASSCODE_ID,
               CC.CREDENTIAL_ACTIVE_FLAG
          INTO pv_email, pv_first, pv_last, ld_exp_ts, ln_consumer_passcode_id, lc_active_flag
          FROM PSS.CONSUMER C
          LEFT OUTER JOIN (PSS.CONSUMER_PASSCODE CP
          JOIN PSS.PASSCODE_TYPE PT ON CP.PASSCODE_TYPE_ID = PT.PASSCODE_TYPE_ID AND PT.PASSCODE_TYPE_CD = 'REGISTER_PREPAID' AND CP.PASSCODE = pv_passcode) 
            ON C.CONSUMER_ID = CP.CONSUMER_ID
          LEFT OUTER JOIN PSS.CONSUMER_CREDENTIAL CC ON C.CONSUMER_ID = CC.CONSUMER_ID AND CC.RESOURCE_KEY = 'PREPAID_WEBSITE'
         WHERE C.CONSUMER_ID = pn_consumer_id;
        IF lc_active_flag IS NULL THEN
            RAISE_APPLICATION_ERROR(-20323, 'Credentials not found for consumer ' || pn_consumer_id);
        ELSIF lc_active_flag = 'Y' THEN
            RAISE_APPLICATION_ERROR(-20324, 'Consumer ' || pn_consumer_id || ' is already registered');
        ELSIF ln_consumer_passcode_id IS NULL THEN
            RAISE_APPLICATION_ERROR(-20321, 'Passcode ' || pv_passcode || ' does not exist for consumer ' || pn_consumer_id);
        ELSIF ld_exp_ts < SYSDATE THEN
            RAISE_APPLICATION_ERROR(-20322, 'Passcode ' || pv_passcode || ' is expired for consumer ' || pn_consumer_id || ' as of ' || TO_CHAR(ld_exp_ts, 'MM/DD/YYYY HH24:MI:SS'));
        ELSE
            UPDATE PSS.CONSUMER_CREDENTIAL
               SET CREDENTIAL_ACTIVE_FLAG = 'Y'
             WHERE CONSUMER_ID = pn_consumer_id
               AND RESOURCE_KEY = 'PREPAID_WEBSITE'
               AND CREDENTIAL_ACTIVE_FLAG IN('A', 'N');
            UPDATE PSS.CONSUMER_PASSCODE
               SET EXPIRATION_TS = SYSDATE
             WHERE CONSUMER_PASSCODE_ID = ln_consumer_passcode_id;
        END IF;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RAISE_APPLICATION_ERROR(-20320, 'Consumer ' || pn_consumer_id || ' does not exist');
    END;
    
    PROCEDURE CREATE_PASSCODE_FOR_PASSWORD(
        pv_email PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pn_consumer_id OUT PSS.CONSUMER.CONSUMER_ID%TYPE,
        pv_first OUT PSS.CONSUMER.CONSUMER_FNAME%TYPE,
        pv_last OUT PSS.CONSUMER.CONSUMER_LNAME%TYPE,
        pv_passcode OUT PSS.CONSUMER_PASSCODE.PASSCODE%TYPE)
    IS
    BEGIN
        SELECT C.CONSUMER_ID,
               C.CONSUMER_FNAME, 
               C.CONSUMER_LNAME
          INTO pn_consumer_id, pv_first, pv_last
          FROM PSS.CONSUMER C
         WHERE TRIM(UPPER(CONSUMER_EMAIL_ADDR1)) = TRIM(UPPER(pv_email))
           AND CONSUMER_TYPE_ID = 5;
        pv_passcode := PSS.PKG_CONSUMER_MAINT.CREATE_CONSUMER_PASSCODE(pn_consumer_id, 'RESET_PREPAID_PASS');     
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RAISE_APPLICATION_ERROR(-20320, 'Consumer ' || pn_consumer_id || ' does not exist');
    END;
    
    PROCEDURE UPDATE_PREPAID_PASSWORD(
        pv_email PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pb_credential_hash PSS.CONSUMER_CREDENTIAL.CREDENTIAL_HASH%TYPE,
        pb_credential_salt PSS.CONSUMER_CREDENTIAL.CREDENTIAL_SALT%TYPE,
        pv_credential_alg PSS.CONSUMER_CREDENTIAL.CREDENTIAL_ALG%TYPE)
    IS
        ln_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE;
    BEGIN
        SELECT CONSUMER_ID
          INTO ln_consumer_id
          FROM PSS.CONSUMER
         WHERE TRIM(UPPER(CONSUMER_EMAIL_ADDR1)) = TRIM(UPPER(pv_email))
           AND CONSUMER_TYPE_ID = 5;
        UPDATE PSS.CONSUMER_CREDENTIAL
           SET CREDENTIAL_HASH = pb_credential_hash, 
               CREDENTIAL_SALT = pb_credential_salt,
               CREDENTIAL_ALG = pv_credential_alg
         WHERE CONSUMER_ID = ln_consumer_id
           AND RESOURCE_KEY = 'PREPAID_WEBSITE';
    END;
    
    PROCEDURE UPDATE_PREPAID_PASSWORD(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pv_passcode PSS.CONSUMER_PASSCODE.PASSCODE%TYPE,
        pb_credential_hash PSS.CONSUMER_CREDENTIAL.CREDENTIAL_HASH%TYPE,
        pb_credential_salt PSS.CONSUMER_CREDENTIAL.CREDENTIAL_SALT%TYPE,
        pv_credential_alg PSS.CONSUMER_CREDENTIAL.CREDENTIAL_ALG%TYPE)
    IS
        ld_exp_ts PSS.CONSUMER_PASSCODE.EXPIRATION_TS%TYPE;
        ln_consumer_passcode_id PSS.CONSUMER_PASSCODE.CONSUMER_PASSCODE_ID%TYPE;
    BEGIN
        SELECT MAX(CP.EXPIRATION_TS),
               MAX(CP.CONSUMER_PASSCODE_ID)
          INTO ld_exp_ts, ln_consumer_passcode_id
          FROM PSS.CONSUMER_PASSCODE CP
          JOIN PSS.PASSCODE_TYPE PT ON CP.PASSCODE_TYPE_ID = PT.PASSCODE_TYPE_ID AND PT.PASSCODE_TYPE_CD = 'RESET_PREPAID_PASS' AND CP.PASSCODE = pv_passcode
         WHERE CP.CONSUMER_ID = pn_consumer_id;
        IF ln_consumer_passcode_id IS NULL THEN
            RAISE_APPLICATION_ERROR(-20321, 'Passcode ' || pv_passcode || ' does not exist for consumer ' || pn_consumer_id);
        ELSIF ld_exp_ts < SYSDATE THEN
            RAISE_APPLICATION_ERROR(-20322, 'Passcode ' || pv_passcode || ' is expired for consumer ' || pn_consumer_id || ' as of ' || TO_CHAR(ld_exp_ts, 'MM/DD/YYYY HH24:MI:SS'));
        END IF;
        
        UPDATE PSS.CONSUMER_CREDENTIAL
           SET CREDENTIAL_HASH = pb_credential_hash, 
               CREDENTIAL_SALT = pb_credential_salt,
               CREDENTIAL_ALG = pv_credential_alg
         WHERE CONSUMER_ID = pn_consumer_id
           AND RESOURCE_KEY = 'PREPAID_WEBSITE';
           
        UPDATE PSS.CONSUMER_PASSCODE
           SET EXPIRATION_TS = LEAST(SYSDATE, NVL(EXPIRATION_TS, MAX_DATE))
         WHERE PASSCODE = pv_passcode
           AND CONSUMER_ID = pn_consumer_id;
    END;
    
    PROCEDURE REREGISTER_PREPAID_CONSUMER(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pv_email OUT PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pv_first OUT PSS.CONSUMER.CONSUMER_FNAME%TYPE,
        pv_last OUT PSS.CONSUMER.CONSUMER_LNAME%TYPE,
        pv_passcode OUT PSS.CONSUMER_PASSCODE.PASSCODE%TYPE)
    IS
    BEGIN
        SELECT C.CONSUMER_EMAIL_ADDR1,
               C.CONSUMER_FNAME, 
               C.CONSUMER_LNAME
          INTO pv_email, pv_first, pv_last
          FROM PSS.CONSUMER C
         WHERE C.CONSUMER_ID = pn_consumer_id;
        pv_passcode := PSS.PKG_CONSUMER_MAINT.CREATE_CONSUMER_PASSCODE(pn_consumer_id, 'REGISTER_PREPAID');
    END;
   
    PROCEDURE CANCEL_PASSCODE(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pv_passcode PSS.CONSUMER_PASSCODE.PASSCODE%TYPE)
    IS
        ld_exp_ts PSS.CONSUMER_PASSCODE.EXPIRATION_TS%TYPE;
        ln_consumer_passcode_id PSS.CONSUMER_PASSCODE.CONSUMER_PASSCODE_ID%TYPE;
    BEGIN
        UPDATE PSS.CONSUMER_PASSCODE
           SET EXPIRATION_TS = LEAST(SYSDATE, NVL(EXPIRATION_TS, MAX_DATE))
         WHERE PASSCODE = pv_passcode
           AND CONSUMER_ID = pn_consumer_id;
        IF SQL%NOTFOUND THEN
            RAISE_APPLICATION_ERROR(-20321, 'Passcode ' || pv_passcode || ' does not exist for consumer ' || pn_consumer_id);
        END IF;
    END;
    
    PROCEDURE UPDATE_CONSUMER_SETTING(
        pv_email PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pn_param_id PSS.CONSUMER_SETTING.CONSUMER_SETTING_PARAM_ID%TYPE,
        pv_param_value PSS.CONSUMER_SETTING.CONSUMER_SETTING_VALUE%TYPE)
    IS
        ln_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE;
        lv_param_value_actual PSS.CONSUMER_SETTING.CONSUMER_SETTING_VALUE%TYPE;
    BEGIN
        SELECT CONSUMER_ID
          INTO ln_consumer_id
          FROM PSS.CONSUMER
         WHERE TRIM(UPPER(CONSUMER_EMAIL_ADDR1)) = TRIM(UPPER(pv_email))
           AND CONSUMER_TYPE_ID = 5;
        IF pv_param_value IS NULL THEN
            SELECT DEFAULT_VALUE
              INTO lv_param_value_actual
              FROM PSS.CONSUMER_SETTING_PARAM
             WHERE CONSUMER_SETTING_PARAM_ID = pn_param_id;
        ELSE
            lv_param_value_actual := pv_param_value;
        END IF;
        LOOP
            UPDATE PSS.CONSUMER_SETTING
               SET CONSUMER_SETTING_VALUE = lv_param_value_actual
             WHERE CONSUMER_ID = ln_consumer_id
               AND CONSUMER_SETTING_PARAM_ID = pn_param_id;
            EXIT WHEN SQL%FOUND;
            BEGIN
                INSERT INTO PSS.CONSUMER_SETTING(CONSUMER_ID, CONSUMER_SETTING_PARAM_ID, CONSUMER_SETTING_VALUE)
                    VALUES(ln_consumer_id, pn_param_id, lv_param_value_actual);
                EXIT;
            EXCEPTION
                WHEN DUP_VAL_ON_INDEX THEN
                    NULL;
            END;
        END LOOP;
    END;
END;
/