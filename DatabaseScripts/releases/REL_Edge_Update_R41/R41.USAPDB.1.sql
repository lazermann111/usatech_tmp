INSERT INTO FOLIO_CONF.JOIN_FILTER(JOIN_FILTER_ID, FROM_TABLE, TO_TABLE, JOIN_EXPRESSION, JOIN_TYPE, JOIN_CARDINALITY)
  VALUES(77, 'REPORT.TRANS', 'REPORT.TRANS_STATE', 'REPORT.TRANS.SETTLE_STATE_ID = REPORT.TRANS_STATE.STATE_ID', 'INNER', '>');
  
INSERT INTO FOLIO_CONF.FIELD(FIELD_ID,FIELD_LABEL,FIELD_DESC,DISPLAY_EXPRESSION,SORT_EXPRESSION,DISPLAY_FORMAT,DISPLAY_SQL_TYPE,SORT_SQL_TYPE,IMPORTANCE,FIELD_CATEGORY_ID) 
  VALUES (9384,'Settle State (Trans Table)',null,'REPORT.TRANS_STATE.STATE_LABEL','UPPER(REPORT.TRANS_STATE.STATE_LABEL)',null,'VARCHAR','VARCHAR',50,11);
  
INSERT INTO  FOLIO_CONF.FIELD(FIELD_ID,FIELD_LABEL,FIELD_DESC,DISPLAY_EXPRESSION,SORT_EXPRESSION,DISPLAY_FORMAT,DISPLAY_SQL_TYPE,SORT_SQL_TYPE,IMPORTANCE,FIELD_CATEGORY_ID) 
  VALUES (9386,'Settle State Id (Trans Table)',null,'REPORT.TRANS.SETTLE_STATE_ID','REPORT.TRANS.SETTLE_STATE_ID',null,'NUMERIC','NUMERIC',50,12);
  
INSERT INTO FOLIO_CONF.FIELD_PRIV(USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
  SELECT * FROM (
   SELECT UG.COLUMN_VALUE USER_GROUP_ID, 9384 FIELD_ID, NULL FILTER_GROUP_ID
     FROM TABLE(NUMBER_TABLE(1,2,3,4,5,8)) UG
   UNION ALL
   SELECT UG.COLUMN_VALUE, 9386, CASE WHEN UG.COLUMN_VALUE < 4 THEN NULL ELSE 1 END
     FROM TABLE(NUMBER_TABLE(1,2,3,4,5,8)) UG) A
  WHERE NOT EXISTS(SELECT 1 FROM FOLIO_CONF.FIELD_PRIV FG WHERE FG.USER_GROUP_ID = A.USER_GROUP_ID AND FG.FIELD_ID = A.FIELD_ID);
  
COMMIT;

ALTER TABLE CORP.CUSTOMER ADD BRANDING_SUBDIRECTORY VARCHAR2(60);
ALTER TABLE WEB_CONTENT.SUBDOMAIN ADD APP_CD VARCHAR2(50);

UPDATE WEB_CONTENT.SUBDOMAIN
   SET APP_CD = CASE 
        WHEN SUBDOMAIN_DESCRIPTION LIKE 'USALive %' THEN 'USALive'
        WHEN SUBDOMAIN_DESCRIPTION LIKE 'Sony %' THEN 'Sony'
        WHEN SUBDOMAIN_DESCRIPTION LIKE 'eSuds %' THEN 'eSuds'
        WHEN SUBDOMAIN_DESCRIPTION LIKE 'GetMore %' THEN 'Prepaid'
        WHEN SUBDOMAIN_DESCRIPTION LIKE '%USALive %' THEN 'USALive'
        WHEN SUBDOMAIN_DESCRIPTION LIKE '%GetMore %' THEN 'Prepaid'
        WHEN SUBDOMAIN_DESCRIPTION LIKE 'USALive %' THEN 'USALive'
        END
 WHERE APP_CD IS NULL;
   
UPDATE CORP.CUSTOMER
   SET BRANDING_SUBDIRECTORY = '/pepi'
 WHERE CUSTOMER_NAME = 'Pepi Food Services';

INSERT INTO WEB_CONTENT.LITERAL(SUBDOMAIN_ID, LITERAL_KEY, LITERAL_VALUE)
  SELECT SUBDOMAIN_ID, LITERAL_KEY, LITERAL_VALUE
    FROM (
      SELECT SD.SUBDOMAIN_ID, L.LITERAL_KEY, L.LITERAL_VALUE
        FROM WEB_CONTENT.SUBDOMAIN SD
        JOIN (
        SELECT '' LITERAL_KEY, '' LITERAL_VALUE, '' SUBDIRECTORY
          FROM DUAL WHERE 1=0
        UNION ALL
        SELECT 'campaign-blast-email-from-name', 'Promotions by {0}', ''
          FROM DUAL
        UNION ALL
        SELECT 'campaign-blast-email-from-name', 'Promotions by Pepi Food Services', '/pepi'
          FROM DUAL
        UNION ALL
        SELECT 'campaign-blast-email-subject-1', 'We are running a promotion!', ''
          FROM DUAL
        UNION ALL
        SELECT 'campaign-blast-email-subject-1', 'We are running a Pepi promotion!', '/pepi'
          FROM DUAL
        UNION ALL
        SELECT 'campaign-blast-email-subject-2', 'We are running a promotion!', ''
          FROM DUAL
        UNION ALL
        SELECT 'campaign-blast-email-subject-2', 'We are running a Pepi promotion!', '/pepi'
          FROM DUAL
        UNION ALL
        SELECT 'campaign-blast-email-subject-3', 'We are running a promotion!', ''
          FROM DUAL
        UNION ALL
        SELECT 'campaign-blast-email-subject-3', 'We are running a Pepi promotion!', '/pepi'
          FROM DUAL
        UNION ALL
        SELECT 'prepaid-register-email-extra-html', '<p style="font-size: 21px; color: #333333; margin-top: 30px; line-height: 23px; padding: 20px 0 20px 0; background-color: #f6b332;">Don''''t forget, you get a {0,PERCENT} bonus every<br />time you reload with ${1,NUMBER,#,##0} or <span style="font-weight: bold; font-style: italic; font-family: Georgia, sans-serif; color: #2992d0; font-size: 23px;">more</span>.</p>', '/pepi'
          FROM DUAL      
        ) L ON NVL(REGEXP_SUBSTR(SD.SUBDOMAIN_URL, '([^/]+)(/.+)?', 1,1,'',2), '!') = NVL(L.SUBDIRECTORY, '!')
       WHERE SD.APP_CD = 'Prepaid'
    ) N
   WHERE NOT EXISTS(SELECT 1 FROM WEB_CONTENT.LITERAL O WHERE O.SUBDOMAIN_ID = N.SUBDOMAIN_ID AND O.LITERAL_KEY = N.LITERAL_KEY);

UPDATE WEB_CONTENT.LITERAL L
   SET LITERAL_VALUE = '{0,NULL,{0},{1}}@{2}'
 WHERE L.LITERAL_KEY like '%-email-from-addr';
 
UPDATE WEB_CONTENT.LITERAL L
   SET LITERAL_VALUE = REGEXP_REPLACE(LITERAL_VALUE, '( by )(USA Technologies|Pepi Food Services)', '\1{0}')
 WHERE L.LITERAL_KEY like '%-email-from-name';
 
COMMIT;

UPDATE WEB_CONTENT.LITERAL SET LITERAL_VALUE = 'You earned MORE!' WHERE LITERAL_VALUE = 'You earned a MORE!';
UPDATE WEB_CONTENT.LITERAL SET LITERAL_VALUE = REPLACE(LITERAL_VALUE, 'Prepaid', 'MORE') WHERE LITERAL_VALUE LIKE 'Prepaid%';
COMMIT;

INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, LITERAL_VALUE, SUBDOMAIN_ID)
SELECT L.LITERAL_KEY, L.LITERAL_VALUE, S.SUBDOMAIN_ID
FROM (
SELECT '' LITERAL_KEY, '' LITERAL_VALUE FROM DUAL WHERE 0=1
UNION ALL SELECT 'prepaid-home-description', 'Start getting <span class="more-text">more</span> with every purchase at participating self-service retail locations like vending machines, kiosks and much, much <span class="more-text">more</span> with this ultra-convenient Prepaid &' || 'amp; Loyalty program. Simply use your card at participating machines to see the benefits add up!' FROM DUAL
UNION ALL SELECT 'prepaid-home-text1', 'Earn an additional 10% in bonus cash every time you replenish your card with $20 or <span class="more-text">more</span>!' FROM DUAL
UNION ALL SELECT 'prepaid-home-text2', 'Access exclusive offers and promotions at all of your favorite Pepi locations on campus: vending, dining rooms and kiosks!' FROM DUAL
UNION ALL SELECT 'prepaid-home-text3', 'Find out about new products and special promotions being offered in all of your favorite locations!' FROM DUAL
UNION ALL SELECT 'prepaid-replenishment-note', 'Don''t forget, you get a 10% bonus every time you replenish with $20 or <span class="more-text">more</span>.' FROM DUAL
UNION ALL SELECT 'prepaid-company-name', 'The Pepi Companies' FROM DUAL
UNION ALL SELECT 'prepaid-company-phone-number', '800-356-4068' FROM DUAL
UNION ALL SELECT 'prepaid-company-email-address', 'pepiservice@pepifoods.com' FROM DUAL
UNION ALL SELECT 'prepaid-company-address-line1', '165 Technology Drive' FROM DUAL
UNION ALL SELECT 'prepaid-company-address-line2', 'Dothan, AL 36303 USA' FROM DUAL
UNION ALL SELECT 'prepaid-company-website-url', 'http://www.pepifoods.com/' FROM DUAL
UNION ALL SELECT 'prepaid-more-color', '#2992d0' FROM DUAL
) L
CROSS JOIN WEB_CONTENT.SUBDOMAIN S
WHERE S.SUBDOMAIN_URL like '%/pepi'
AND NOT EXISTS(SELECT 1 FROM  WEB_CONTENT.LITERAL L0 WHERE L0.LITERAL_KEY = L.LITERAL_KEY AND L0.SUBDOMAIN_ID = S.SUBDOMAIN_ID AND L0.LOCALE_CD IS NULL);
COMMIT;

UPDATE WEB_CONTENT.LITERAL SET LITERAL_VALUE = '<p style="font-size: 21px; color: #333333; margin-top: 30px; line-height: 23px; padding: 20px 0 20px 0; background-color: #f6b332;">Don''t forget, you get a 10% bonus every<br />time you replenish with $20 or <span style="font-weight: bold; font-style: italic; font-family: Georgia, sans-serif; color: #2992d0; font-size: 23px;">more</span>.</p>' WHERE LITERAL_KEY = 'prepaid-register-email-extra-html';
COMMIT;

ALTER TABLE PSS.CONSUMER_ACCT ADD ALLOW_NEGATIVE_BALANCE VARCHAR2(1);

GRANT SELECT ON REPORT.PURCHASE TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.TRANS TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.TRANS_STATE TO USAT_DMS_ROLE;