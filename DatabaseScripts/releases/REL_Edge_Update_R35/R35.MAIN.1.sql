GRANT WRITE_MAIN TO USALIVE_1;
GRANT WRITE_MAIN TO USALIVE_2;
GRANT WRITE_MAIN TO USALIVE_3;
GRANT WRITE_MAIN TO USALIVE_4;

INSERT INTO MAIN.OBJECT_TYPE(OBJECT_TYPE_CD, OBJECT_TYPE_NAME) SELECT 'user', 'User' WHERE NOT EXISTS (SELECT 1 FROM MAIN.OBJECT_TYPE WHERE OBJECT_TYPE_CD = 'user');
COMMIT;

DO $$
BEGIN
	ALTER TABLE MAIN._APP_REQUEST ADD USER_ID BIGINT;
EXCEPTION
	WHEN duplicate_column THEN
		NULL;
END$$;

CREATE OR REPLACE FUNCTION MAIN.FRIIUD_APP_REQUEST()
	RETURNS TRIGGER
	SECURITY DEFINER
AS $$
DECLARE
	lv_old_partition_table VARCHAR(4000);
	lv_new_partition_table VARCHAR(4000);
	lv_new_suffix VARCHAR(4000);
	lv_check_exp VARCHAR(4000);
	lv_sql TEXT;
	lv_filter TEXT;
	ln_cnt INTEGER;
BEGIN
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		SELECT COALESCE(TO_CHAR(NEW.REQUEST_TS, 'YYYY_MM'),'') INTO lv_new_suffix;
		SELECT MAIN.CONSTRUCT_NAME('_APP_REQUEST', lv_new_suffix) INTO lv_new_partition_table;
	END IF;
	IF TG_OP IN('DELETE', 'UPDATE') THEN
		SELECT MAIN.CONSTRUCT_NAME('_APP_REQUEST', COALESCE(TO_CHAR(OLD.REQUEST_TS, 'YYYY_MM'), '')) INTO lv_old_partition_table;
	END IF;
	IF lv_new_partition_table IS NOT NULL AND lv_old_partition_table IS NOT NULL AND lv_new_partition_table = lv_old_partition_table THEN
		lv_sql := 'UPDATE MAIN.' || lv_new_partition_table || ' SET ';
		lv_filter := '';
		IF OLD.APP_REQUEST_ID != NEW.APP_REQUEST_ID THEN
			lv_sql := lv_sql || 'APP_REQUEST_ID = $2.APP_REQUEST_ID, ';
		END IF;
		IF OLD.APP_CD != NEW.APP_CD THEN
			lv_sql := lv_sql || 'APP_CD = $2.APP_CD, ';
			lv_filter := lv_filter || ' AND (APP_CD = $1.APP_CD OR APP_CD = $2.APP_CD)';
		END IF;
		IF OLD.REQUEST_URL != NEW.REQUEST_URL THEN
			lv_sql := lv_sql || 'REQUEST_URL = $2.REQUEST_URL, ';
			lv_filter := lv_filter || ' AND (REQUEST_URL = $1.REQUEST_URL OR REQUEST_URL = $2.REQUEST_URL)';
		END IF;
		IF OLD.REQUEST_TS != NEW.REQUEST_TS THEN
			lv_sql := lv_sql || 'REQUEST_TS = $2.REQUEST_TS, ';
			lv_filter := lv_filter || ' AND (REQUEST_TS = $1.REQUEST_TS OR REQUEST_TS = $2.REQUEST_TS)';
		END IF;
		IF OLD.REMOTE_ADDR IS DISTINCT FROM NEW.REMOTE_ADDR THEN
			lv_sql := lv_sql || 'REMOTE_ADDR = $2.REMOTE_ADDR, ';
			lv_filter := lv_filter || ' AND (REMOTE_ADDR IS NOT DISTINCT FROM $1.REMOTE_ADDR OR REMOTE_ADDR IS NOT DISTINCT FROM $2.REMOTE_ADDR)';
		END IF;
		IF OLD.REMOTE_PORT IS DISTINCT FROM NEW.REMOTE_PORT THEN
			lv_sql := lv_sql || 'REMOTE_PORT = $2.REMOTE_PORT, ';
			lv_filter := lv_filter || ' AND (REMOTE_PORT IS NOT DISTINCT FROM $1.REMOTE_PORT OR REMOTE_PORT IS NOT DISTINCT FROM $2.REMOTE_PORT)';
		END IF;
		IF OLD.REFERER IS DISTINCT FROM NEW.REFERER THEN
			lv_sql := lv_sql || 'REFERER = $2.REFERER, ';
			lv_filter := lv_filter || ' AND (REFERER IS NOT DISTINCT FROM $1.REFERER OR REFERER IS NOT DISTINCT FROM $2.REFERER)';
		END IF;
		IF OLD.HTTP_USER_AGENT IS DISTINCT FROM NEW.HTTP_USER_AGENT THEN
			lv_sql := lv_sql || 'HTTP_USER_AGENT = $2.HTTP_USER_AGENT, ';
			lv_filter := lv_filter || ' AND (HTTP_USER_AGENT IS NOT DISTINCT FROM $1.HTTP_USER_AGENT OR HTTP_USER_AGENT IS NOT DISTINCT FROM $2.HTTP_USER_AGENT)';
		END IF;
		IF OLD.SESSION_ID != NEW.SESSION_ID THEN
			lv_sql := lv_sql || 'SESSION_ID = $2.SESSION_ID, ';
			lv_filter := lv_filter || ' AND (SESSION_ID = $1.SESSION_ID OR SESSION_ID = $2.SESSION_ID)';
		END IF;
		IF OLD.USERNAME IS DISTINCT FROM NEW.USERNAME THEN
			lv_sql := lv_sql || 'USERNAME = $2.USERNAME, ';
			lv_filter := lv_filter || ' AND (USERNAME IS NOT DISTINCT FROM $1.USERNAME OR USERNAME IS NOT DISTINCT FROM $2.USERNAME)';
		END IF;
		IF OLD.REQUEST_HEADERS IS DISTINCT FROM NEW.REQUEST_HEADERS THEN
			lv_sql := lv_sql || 'REQUEST_HEADERS = $2.REQUEST_HEADERS, ';
			lv_filter := lv_filter || ' AND (REQUEST_HEADERS IS NOT DISTINCT FROM $1.REQUEST_HEADERS OR REQUEST_HEADERS IS NOT DISTINCT FROM $2.REQUEST_HEADERS)';
		END IF;
		IF OLD.REQUEST_PARAMETERS IS DISTINCT FROM NEW.REQUEST_PARAMETERS THEN
			lv_sql := lv_sql || 'REQUEST_PARAMETERS = $2.REQUEST_PARAMETERS, ';
			lv_filter := lv_filter || ' AND (REQUEST_PARAMETERS IS NOT DISTINCT FROM $1.REQUEST_PARAMETERS OR REQUEST_PARAMETERS IS NOT DISTINCT FROM $2.REQUEST_PARAMETERS)';
		END IF;
		IF OLD.PROCESS_TIME_MS != NEW.PROCESS_TIME_MS THEN
			lv_sql := lv_sql || 'PROCESS_TIME_MS = $2.PROCESS_TIME_MS, ';
			lv_filter := lv_filter || ' AND (PROCESS_TIME_MS = $1.PROCESS_TIME_MS OR PROCESS_TIME_MS = $2.PROCESS_TIME_MS)';
		END IF;
		IF OLD.EXCEPTION_FLAG != NEW.EXCEPTION_FLAG THEN
			lv_sql := lv_sql || 'EXCEPTION_FLAG = $2.EXCEPTION_FLAG, ';
			lv_filter := lv_filter || ' AND (EXCEPTION_FLAG = $1.EXCEPTION_FLAG OR EXCEPTION_FLAG = $2.EXCEPTION_FLAG)';
		END IF;
		IF OLD.EXCEPTION_TEXT IS DISTINCT FROM NEW.EXCEPTION_TEXT THEN
			lv_sql := lv_sql || 'EXCEPTION_TEXT = $2.EXCEPTION_TEXT, ';
			lv_filter := lv_filter || ' AND (EXCEPTION_TEXT IS NOT DISTINCT FROM $1.EXCEPTION_TEXT OR EXCEPTION_TEXT IS NOT DISTINCT FROM $2.EXCEPTION_TEXT)';
		END IF;
		IF OLD.REQUEST_HANDLER IS DISTINCT FROM NEW.REQUEST_HANDLER THEN
			lv_sql := lv_sql || 'REQUEST_HANDLER = $2.REQUEST_HANDLER, ';
			lv_filter := lv_filter || ' AND (REQUEST_HANDLER IS NOT DISTINCT FROM $1.REQUEST_HANDLER OR REQUEST_HANDLER IS NOT DISTINCT FROM $2.REQUEST_HANDLER)';
		END IF;
		IF OLD.FOLIO_ID IS DISTINCT FROM NEW.FOLIO_ID THEN
			lv_sql := lv_sql || 'FOLIO_ID = $2.FOLIO_ID, ';
			lv_filter := lv_filter || ' AND (FOLIO_ID IS NOT DISTINCT FROM $1.FOLIO_ID OR FOLIO_ID IS NOT DISTINCT FROM $2.FOLIO_ID)';
		END IF;
		IF OLD.REPORT_ID IS DISTINCT FROM NEW.REPORT_ID THEN
			lv_sql := lv_sql || 'REPORT_ID = $2.REPORT_ID, ';
			lv_filter := lv_filter || ' AND (REPORT_ID IS NOT DISTINCT FROM $1.REPORT_ID OR REPORT_ID IS NOT DISTINCT FROM $2.REPORT_ID)';
		END IF;
		IF OLD.PROFILE_ID IS DISTINCT FROM NEW.PROFILE_ID THEN
			lv_sql := lv_sql || 'PROFILE_ID = $2.PROFILE_ID, ';
			lv_filter := lv_filter || ' AND (PROFILE_ID IS NOT DISTINCT FROM $1.PROFILE_ID OR PROFILE_ID IS NOT DISTINCT FROM $2.PROFILE_ID)';
		END IF;
		IF OLD.ACTION_NAME IS DISTINCT FROM NEW.ACTION_NAME THEN
			lv_sql := lv_sql || 'ACTION_NAME = $2.ACTION_NAME, ';
			lv_filter := lv_filter || ' AND (ACTION_NAME IS NOT DISTINCT FROM $1.ACTION_NAME OR ACTION_NAME IS NOT DISTINCT FROM $2.ACTION_NAME)';
		END IF;
		IF OLD.OBJECT_TYPE_CD IS DISTINCT FROM NEW.OBJECT_TYPE_CD THEN
			lv_sql := lv_sql || 'OBJECT_TYPE_CD = $2.OBJECT_TYPE_CD, ';
			lv_filter := lv_filter || ' AND (OBJECT_TYPE_CD IS NOT DISTINCT FROM $1.OBJECT_TYPE_CD OR OBJECT_TYPE_CD IS NOT DISTINCT FROM $2.OBJECT_TYPE_CD)';
		END IF;
		IF OLD.OBJECT_CD IS DISTINCT FROM NEW.OBJECT_CD THEN
			lv_sql := lv_sql || 'OBJECT_CD = $2.OBJECT_CD, ';
			lv_filter := lv_filter || ' AND (OBJECT_CD IS NOT DISTINCT FROM $1.OBJECT_CD OR OBJECT_CD IS NOT DISTINCT FROM $2.OBJECT_CD)';
		END IF;
		IF OLD.USER_ID IS DISTINCT FROM NEW.USER_ID THEN
			lv_sql := lv_sql || 'USER_ID = $2.USER_ID, ';
			lv_filter := lv_filter || ' AND (USER_ID IS NOT DISTINCT FROM $1.USER_ID OR USER_ID IS NOT DISTINCT FROM $2.USER_ID)';
		END IF;
		lv_sql := LEFT(lv_sql, -2) || ' WHERE APP_REQUEST_ID = $1.APP_REQUEST_ID' || lv_filter;
		EXECUTE lv_sql USING OLD, NEW;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
		RETURN NEW;
	END IF;
	IF lv_new_partition_table IS NOT NULL THEN
		LOOP
			BEGIN
				EXECUTE 'INSERT INTO MAIN.' || lv_new_partition_table || ' SELECT $1.*' USING NEW;
				EXIT;
			EXCEPTION
			WHEN undefined_table THEN
				SELECT 'CONSTRAINT CK_' || lv_new_partition_table || ' CHECK(' ||'REQUEST_TS >= TIMESTAMP'''||TO_CHAR(NEW.REQUEST_TS, 'YYYY-MM-01')||''' AND REQUEST_TS < TIMESTAMP'''||TO_CHAR(NEW.REQUEST_TS + '1 month', 'YYYY-MM-01')||''''||'), ' INTO lv_check_exp;
				RAISE NOTICE 'Creating new partition table %', lv_new_partition_table;
				BEGIN
					EXECUTE 'CREATE TABLE MAIN.' || lv_new_partition_table || '(' || lv_check_exp ||'CONSTRAINT PK_' || lv_new_partition_table || ' PRIMARY KEY(APP_REQUEST_ID)) INHERITS(MAIN._APP_REQUEST)';
				EXCEPTION
					WHEN duplicate_table THEN
						CONTINUE;
				END;
				EXECUTE 'GRANT ALL ON TABLE MAIN.' || lv_new_partition_table || ' TO ' || CURRENT_USER;
				PERFORM MAIN.COPY_INDEXES('_APP_REQUEST', lv_new_suffix);
			END;
		END LOOP;
	END IF;
	IF lv_old_partition_table IS NOT NULL THEN
		EXECUTE 'DELETE FROM MAIN.' || lv_old_partition_table || ' WHERE APP_REQUEST_ID = $1.APP_REQUEST_ID' USING OLD;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		RETURN NEW;
	ELSE
		RETURN OLD;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE VIEW MAIN.APP_REQUEST
	AS SELECT * FROM MAIN._APP_REQUEST;

ALTER VIEW MAIN.APP_REQUEST ALTER APP_REQUEST_ID SET DEFAULT nextval('MAIN._app_request_app_request_id_seq'::regclass);
ALTER VIEW MAIN.APP_REQUEST ALTER REQUEST_TS SET DEFAULT CURRENT_TIMESTAMP;

GRANT SELECT ON MAIN.APP_REQUEST TO read_main;
GRANT INSERT, UPDATE, DELETE ON MAIN.APP_REQUEST TO write_main;
REVOKE INSERT, UPDATE, DELETE, TRUNCATE ON MAIN._APP_REQUEST FROM postgres;

DROP TRIGGER IF EXISTS TRIIUD_APP_REQUEST ON MAIN.APP_REQUEST;
CREATE TRIGGER TRIIUD_APP_REQUEST
INSTEAD OF INSERT OR UPDATE OR DELETE ON MAIN.APP_REQUEST
	FOR EACH ROW
	EXECUTE PROCEDURE MAIN.FRIIUD_APP_REQUEST();


CREATE OR REPLACE FUNCTION MAIN.UPSERT_APP_REQUEST(
	p_app_request_id BIGINT, 
	p_app_cd VARCHAR(50), 
	p_request_url TEXT, 
	p_request_ts TIMESTAMP WITH TIME ZONE, 
	p_remote_addr VARCHAR(15), 
	p_remote_port INTEGER, 
	p_referer TEXT, 
	p_http_user_agent TEXT, 
	p_session_id VARCHAR(100), 
	p_username VARCHAR(100), 
	p_request_headers TEXT, 
	p_request_parameters TEXT, 
	p_process_time_ms INTEGER, 
	p_exception_flag CHAR(1), 
	p_exception_text TEXT, 
	p_request_handler VARCHAR(100), 
	p_folio_id INTEGER, 
	p_report_id INTEGER, 
	p_profile_id INTEGER, 
	p_action_name VARCHAR(100), 
	p_object_type_cd VARCHAR(50), 
	p_object_cd VARCHAR(200), 
	p_user_id BIGINT,
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
BEGIN
	LOOP
		BEGIN
			UPDATE MAIN.APP_REQUEST
			   SET APP_CD = p_app_cd,
			       REQUEST_URL = p_request_url,
			       REQUEST_TS = COALESCE(p_request_ts, REQUEST_TS),
			       REMOTE_ADDR = p_remote_addr,
			       REMOTE_PORT = p_remote_port,
			       REFERER = p_referer,
			       HTTP_USER_AGENT = p_http_user_agent,
			       SESSION_ID = p_session_id,
			       USERNAME = p_username,
			       REQUEST_HEADERS = p_request_headers,
			       REQUEST_PARAMETERS = p_request_parameters,
			       PROCESS_TIME_MS = p_process_time_ms,
			       EXCEPTION_FLAG = p_exception_flag,
			       EXCEPTION_TEXT = p_exception_text,
			       REQUEST_HANDLER = p_request_handler,
			       FOLIO_ID = p_folio_id,
			       REPORT_ID = p_report_id,
			       PROFILE_ID = p_profile_id,
			       ACTION_NAME = p_action_name,
			       OBJECT_TYPE_CD = p_object_type_cd,
			       OBJECT_CD = p_object_cd,
			       USER_ID = p_user_id
			 WHERE APP_REQUEST_ID = p_app_request_id;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				INSERT INTO MAIN.APP_REQUEST(APP_REQUEST_ID, APP_CD, REQUEST_URL, REQUEST_TS, REMOTE_ADDR, REMOTE_PORT, REFERER, HTTP_USER_AGENT, SESSION_ID, USERNAME, REQUEST_HEADERS, REQUEST_PARAMETERS, PROCESS_TIME_MS, EXCEPTION_FLAG, EXCEPTION_TEXT, REQUEST_HANDLER, FOLIO_ID, REPORT_ID, PROFILE_ID, ACTION_NAME, OBJECT_TYPE_CD, OBJECT_CD, USER_ID)
				 VALUES(p_app_request_id, p_app_cd, p_request_url, COALESCE(p_request_ts, CURRENT_TIMESTAMP), p_remote_addr, p_remote_port, p_referer, p_http_user_agent, p_session_id, p_username, p_request_headers, p_request_parameters, p_process_time_ms, p_exception_flag, p_exception_text, p_request_handler, p_folio_id, p_report_id, p_profile_id, p_action_name, p_object_type_cd, p_object_cd, p_user_id);
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			END IF;
			EXIT;
		EXCEPTION
			WHEN unique_violation THEN
				NULL;
			WHEN serialization_failure THEN
				NULL;
		END;
	END LOOP;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MAIN.UPSERT_APP_REQUEST(
	p_app_request_id BIGINT, 
	p_app_cd VARCHAR(50), 
	p_request_url TEXT, 
	p_request_ts TIMESTAMP WITH TIME ZONE, 
	p_remote_addr VARCHAR(15), 
	p_remote_port INTEGER, 
	p_referer TEXT, 
	p_http_user_agent TEXT, 
	p_session_id VARCHAR(100), 
	p_username VARCHAR(100), 
	p_request_headers TEXT, 
	p_request_parameters TEXT, 
	p_process_time_ms INTEGER, 
	p_exception_flag CHAR(1), 
	p_exception_text TEXT, 
	p_request_handler VARCHAR(100), 
	p_folio_id INTEGER, 
	p_report_id INTEGER, 
	p_profile_id INTEGER, 
	p_action_name VARCHAR(100), 
	p_object_type_cd VARCHAR(50), 
	p_object_cd VARCHAR(200), 
	p_user_id BIGINT,
	p_row_count OUT INTEGER) TO write_main;
