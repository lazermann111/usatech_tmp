ALTER TABLE main._device_session ADD COLUMN dex_status character varying(128);




-- View: main.device_session

-- DROP VIEW main.device_session;

CREATE OR REPLACE VIEW main.device_session AS
 SELECT _device_session.created_utc_ts,
    _device_session.created_by,
    _device_session.last_updated_utc_ts,
    _device_session.last_updated_by,
    _device_session.global_session_cd,
    _device_session.call_in_start_ts,
    _device_session.device_name,
    _device_session.net_layer_id,
    _device_session.device_serial_cd,
    _device_session.device_type_id,
    _device_session.session_state_id,
    _device_session.call_in_finish_ts,
    _device_session.call_in_status,
    _device_session.client_ip_address,
    _device_session.client_port,
    _device_session.inbound_message_count,
    _device_session.outbound_message_count,
    _device_session.inbound_byte_count,
    _device_session.outbound_byte_count,
    _device_session.device_initialized_flag,
    _device_session.device_sent_config_flag,
    _device_session.server_sent_config_flag,
    _device_session.call_in_type,
    _device_session.auth_amount,
    _device_session.auth_approved_flag,
    _device_session.auth_card_type,
    _device_session.dex_file_size,
    _device_session.dex_received_flag,
    _device_session.terminal_id,
    _device_session.customer_id,
    _device_session.location_name,
    _device_session.customer_name,
    _device_session.credit_trans_count,
    _device_session.credit_trans_total,
    _device_session.credit_vend_count,
    _device_session.cash_trans_count,
    _device_session.cash_trans_total,
    _device_session.cash_vend_count,
    _device_session.passcard_trans_count,
    _device_session.passcard_trans_total,
    _device_session.passcard_vend_count,
    _device_session.last_auth_in_ts,
    _device_session.last_trans_in_ts,
    _device_session.firmware_version,
    _device_session.comm_method_cd,
    _device_session.comments,
    _device_session.dex_status
   FROM main._device_session;

ALTER TABLE main.device_session
  OWNER TO admin_1;
GRANT ALL ON TABLE main.device_session TO admin_1;
GRANT SELECT ON TABLE main.device_session TO read_main;
GRANT UPDATE, INSERT, DELETE ON TABLE main.device_session TO write_main;
ALTER TABLE main.device_session ALTER COLUMN created_by SET DEFAULT "current_user"();
ALTER TABLE main.device_session ALTER COLUMN last_updated_by SET DEFAULT "current_user"();
ALTER TABLE main.device_session ALTER COLUMN call_in_start_ts SET DEFAULT now();
ALTER TABLE main.device_session ALTER COLUMN session_state_id SET DEFAULT 1;
ALTER TABLE main.device_session ALTER COLUMN call_in_status SET DEFAULT 'I'::bpchar;
ALTER TABLE main.device_session ALTER COLUMN call_in_type SET DEFAULT 'U'::bpchar;


-- Trigger: triiud_device_session on main.device_session

DROP TRIGGER triiud_device_session ON main.device_session;

CREATE TRIGGER triiud_device_session
  INSTEAD OF INSERT OR UPDATE OR DELETE
  ON main.device_session
  FOR EACH ROW
  EXECUTE PROCEDURE main.friiud_device_session();

-- Function: main.update_session_end(character varying, character varying, character varying, integer, integer, integer, integer, integer, timestamp with time zone, timestamp with time zone, character, character, character, bigint, numeric, character, character, timestamp with time zone, character, character, character varying)

--DROP FUNCTION main.update_session_end(character varying, character varying, character varying, integer, integer, integer, integer, integer, timestamp with time zone, timestamp with time zone, character, character, character, bigint, numeric, character, character, timestamp with time zone, character, character, character varying);

CREATE OR REPLACE FUNCTION main.update_session_end(
    pv_global_session_cd character varying,
    pv_device_name character varying,
    pv_device_serial_cd character varying,
    pn_device_type_id integer,
    pn_bytes_received integer,
    pn_bytes_sent integer,
    pn_messages_received integer,
    pn_messages_sent integer,
    pt_call_in_start_ts timestamp with time zone,
    pt_call_in_finish_ts timestamp with time zone,
    pc_initialized character,
    pc_device_sent_config character,
    pc_server_sent_config character,
    pn_dex_file_size bigint,
    pn_auth_amt numeric,
    pc_auth_result_cd character,
    pc_payment_type character,
    pd_auth_ts timestamp with time zone,
    pc_call_in_type character,
    pc_call_in_status character,
    pv_firmware_version character varying,
    pv_dex_status character varying)
  RETURNS void AS
$BODY$
BEGIN
	-- R34+ version
	LOOP
        BEGIN
            UPDATE MAIN.DEVICE_SESSION
	           SET DEVICE_NAME = pv_device_name,
				   DEVICE_SERIAL_CD = CASE WHEN DEVICE_SERIAL_CD IS NULL THEN pv_device_serial_cd ELSE DEVICE_SERIAL_CD END,
	               DEVICE_TYPE_ID = CASE WHEN pn_device_type_id = -1 THEN DEVICE_TYPE_ID WHEN DEVICE_TYPE_ID IS NULL THEN pn_device_type_id ELSE DEVICE_TYPE_ID END,
	               SESSION_STATE_ID = CASE pc_call_in_status WHEN 'S' THEN 2 WHEN 'M' THEN 2 ELSE 3 END,
	               INBOUND_BYTE_COUNT = pn_bytes_received,
	               OUTBOUND_BYTE_COUNT = pn_bytes_sent,
	               INBOUND_MESSAGE_COUNT = pn_messages_received,
	               OUTBOUND_MESSAGE_COUNT = pn_messages_sent,
	               CALL_IN_FINISH_TS = pt_call_in_finish_ts,
	               DEVICE_INITIALIZED_FLAG = pc_initialized,
	               DEVICE_SENT_CONFIG_FLAG = pc_device_sent_config,
	               SERVER_SENT_CONFIG_FLAG = pc_server_sent_config,
	               DEX_FILE_SIZE = pn_dex_file_size,
	               DEX_RECEIVED_FLAG = CASE WHEN pn_dex_file_size IS NULL THEN NULL::CHAR WHEN pn_dex_file_size <= 0 THEN 'N' ELSE 'Y' END,
	               AUTH_AMOUNT = pn_auth_amt,
	               AUTH_APPROVED_FLAG = CASE pc_auth_result_cd WHEN NULL THEN NULL::CHAR WHEN 'Y' THEN 'Y' WHEN 'N' THEN 'N' WHEN 'P' THEN 'Y' WHEN 'O' THEN 'N' WHEN 'F' THEN 'N' END,
	               AUTH_CARD_TYPE = pc_payment_type,
	               LAST_AUTH_IN_TS = pd_auth_ts,
	               CALL_IN_TYPE = pc_call_in_type,
	               CALL_IN_STATUS = pc_call_in_status,
	               FIRMWARE_VERSION = pv_firmware_version,
	               DEX_STATUS = pv_dex_status
	         WHERE GLOBAL_SESSION_CD = pv_global_session_cd;
	        EXIT WHEN FOUND;
            INSERT INTO MAIN.DEVICE_SESSION(
                GLOBAL_SESSION_CD,
                DEVICE_NAME,
                CALL_IN_START_TS,
                DEVICE_SERIAL_CD,
                DEVICE_TYPE_ID,
                SESSION_STATE_ID,
                INBOUND_BYTE_COUNT,
                OUTBOUND_BYTE_COUNT,
                INBOUND_MESSAGE_COUNT,
                OUTBOUND_MESSAGE_COUNT,
                CALL_IN_FINISH_TS,
			    DEVICE_INITIALIZED_FLAG,
			    DEVICE_SENT_CONFIG_FLAG,
			    SERVER_SENT_CONFIG_FLAG,
			    DEX_FILE_SIZE,
			    DEX_RECEIVED_FLAG,
			    AUTH_AMOUNT,
			    AUTH_APPROVED_FLAG,
			    AUTH_CARD_TYPE,
			    LAST_AUTH_IN_TS,
			    CALL_IN_TYPE,
			    CALL_IN_STATUS,
			    FIRMWARE_VERSION,
			    DEX_STATUS
            ) VALUES (
                pv_global_session_cd,
                pv_device_name,
                COALESCE(pt_call_in_start_ts, pt_call_in_finish_ts),
                pv_device_serial_cd,
                CASE WHEN pn_device_type_id = -1 THEN NULL ELSE pn_device_type_id END,
                CASE pc_call_in_status WHEN 'S' THEN 2 WHEN 'M' THEN 2 ELSE 3 END,
                pn_bytes_received,
                pn_bytes_sent,
                pn_messages_received,
                pn_messages_sent,
                pt_call_in_finish_ts,
                pc_initialized,
                pc_device_sent_config,
                pc_server_sent_config,
                pn_dex_file_size,
                CASE WHEN pn_dex_file_size IS NULL THEN NULL::CHAR WHEN pn_dex_file_size <= 0 THEN 'N' ELSE 'Y' END,
                pn_auth_amt,
                CASE pc_auth_result_cd WHEN NULL THEN NULL::CHAR WHEN 'Y' THEN 'Y' WHEN 'N' THEN 'N' WHEN 'P' THEN 'Y' WHEN 'O' THEN 'N' WHEN 'F' THEN 'N' END,
                pc_payment_type,
                pd_auth_ts,
                pc_call_in_type,
                pc_call_in_status,
		pv_firmware_version,
		pv_dex_status
            );
            EXIT;
        EXCEPTION
            WHEN unique_violation THEN
                NULL;
            WHEN serialization_failure THEN
                NULL;
        END;
    END LOOP;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION main.update_session_end(character varying, character varying, character varying, integer, integer, integer, integer, integer, timestamp with time zone, timestamp with time zone, character, character, character, bigint, numeric, character, character, timestamp with time zone, character, character, character varying)
  OWNER TO admin_1;
GRANT EXECUTE ON FUNCTION main.update_session_end(character varying, character varying, character varying, integer, integer, integer, integer, integer, timestamp with time zone, timestamp with time zone, character, character, character, bigint, numeric, character, character, timestamp with time zone, character, character, character varying) TO admin_1;
GRANT EXECUTE ON FUNCTION main.update_session_end(character varying, character varying, character varying, integer, integer, integer, integer, integer, timestamp with time zone, timestamp with time zone, character, character, character, bigint, numeric, character, character, timestamp with time zone, character, character, character varying) TO public;
GRANT EXECUTE ON FUNCTION main.update_session_end(character varying, character varying, character varying, integer, integer, integer, integer, integer, timestamp with time zone, timestamp with time zone, character, character, character, bigint, numeric, character, character, timestamp with time zone, character, character, character varying) TO write_main;



-- Function: main.friiud_device_session()
-- DROP FUNCTION main.friiud_device_session();

CREATE OR REPLACE FUNCTION main.friiud_device_session()
  RETURNS trigger AS
$BODY$
DECLARE
	lv_old_partition_table VARCHAR(4000);
	lv_new_partition_table VARCHAR(4000);
	lv_new_suffix VARCHAR(4000);
	lv_check_exp VARCHAR(4000);
	lv_sql TEXT;
	lv_filter TEXT;
	ln_cnt INTEGER;
BEGIN
	IF TG_OP = 'INSERT' THEN
			NEW.CREATED_UTC_TS := STATEMENT_TIMESTAMP() AT TIME ZONE 'UTC';
			NEW.CREATED_BY := CURRENT_USER;
			NEW.LAST_UPDATED_UTC_TS := STATEMENT_TIMESTAMP() AT TIME ZONE 'UTC';
			NEW.LAST_UPDATED_BY := CURRENT_USER;
	ELSIF TG_OP = 'UPDATE' THEN
			NEW.CREATED_UTC_TS := OLD.CREATED_UTC_TS;
			NEW.CREATED_BY := OLD.CREATED_BY;
			NEW.LAST_UPDATED_UTC_TS := STATEMENT_TIMESTAMP() AT TIME ZONE 'UTC';
			NEW.LAST_UPDATED_BY := CURRENT_USER;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		SELECT COALESCE(TO_CHAR(NEW.CALL_IN_START_TS, 'YYYY_MM_DD'),'') INTO lv_new_suffix;
		SELECT MAIN.CONSTRUCT_NAME('_DEVICE_SESSION', lv_new_suffix) INTO lv_new_partition_table;
	END IF;
	IF TG_OP IN('DELETE', 'UPDATE') THEN
		SELECT MAIN.CONSTRUCT_NAME('_DEVICE_SESSION', COALESCE(TO_CHAR(OLD.CALL_IN_START_TS, 'YYYY_MM_DD'), '')) INTO lv_old_partition_table;
	END IF;
	IF lv_new_partition_table IS NOT NULL AND lv_old_partition_table IS NOT NULL AND lv_new_partition_table = lv_old_partition_table THEN
		lv_sql := 'UPDATE MAIN.' || lv_new_partition_table || ' SET ';
		lv_filter := '';
		IF OLD.GLOBAL_SESSION_CD != NEW.GLOBAL_SESSION_CD THEN
			lv_sql := lv_sql || 'GLOBAL_SESSION_CD = $2.GLOBAL_SESSION_CD, ';
		END IF;
		IF OLD.CALL_IN_START_TS != NEW.CALL_IN_START_TS THEN
			lv_sql := lv_sql || 'CALL_IN_START_TS = $2.CALL_IN_START_TS, ';
			lv_filter := lv_filter || ' AND (CALL_IN_START_TS = $1.CALL_IN_START_TS OR CALL_IN_START_TS = $2.CALL_IN_START_TS)';
		END IF;
		IF OLD.DEVICE_NAME != NEW.DEVICE_NAME THEN
			lv_sql := lv_sql || 'DEVICE_NAME = $2.DEVICE_NAME, ';
			lv_filter := lv_filter || ' AND (DEVICE_NAME = $1.DEVICE_NAME OR DEVICE_NAME = $2.DEVICE_NAME)';
		END IF;
		IF OLD.NET_LAYER_ID IS DISTINCT FROM NEW.NET_LAYER_ID THEN
			lv_sql := lv_sql || 'NET_LAYER_ID = $2.NET_LAYER_ID, ';
			lv_filter := lv_filter || ' AND (NET_LAYER_ID IS NOT DISTINCT FROM $1.NET_LAYER_ID OR NET_LAYER_ID IS NOT DISTINCT FROM $2.NET_LAYER_ID)';
		END IF;
		IF OLD.DEVICE_SERIAL_CD IS DISTINCT FROM NEW.DEVICE_SERIAL_CD THEN
			lv_sql := lv_sql || 'DEVICE_SERIAL_CD = $2.DEVICE_SERIAL_CD, ';
			lv_filter := lv_filter || ' AND (DEVICE_SERIAL_CD IS NOT DISTINCT FROM $1.DEVICE_SERIAL_CD OR DEVICE_SERIAL_CD IS NOT DISTINCT FROM $2.DEVICE_SERIAL_CD)';
		END IF;
		IF OLD.DEVICE_TYPE_ID IS DISTINCT FROM NEW.DEVICE_TYPE_ID THEN
			lv_sql := lv_sql || 'DEVICE_TYPE_ID = $2.DEVICE_TYPE_ID, ';
			lv_filter := lv_filter || ' AND (DEVICE_TYPE_ID IS NOT DISTINCT FROM $1.DEVICE_TYPE_ID OR DEVICE_TYPE_ID IS NOT DISTINCT FROM $2.DEVICE_TYPE_ID)';
		END IF;
		IF OLD.SESSION_STATE_ID != NEW.SESSION_STATE_ID THEN
			lv_sql := lv_sql || 'SESSION_STATE_ID = $2.SESSION_STATE_ID, ';
			lv_filter := lv_filter || ' AND (SESSION_STATE_ID = $1.SESSION_STATE_ID OR SESSION_STATE_ID = $2.SESSION_STATE_ID)';
		END IF;
		IF OLD.CALL_IN_FINISH_TS IS DISTINCT FROM NEW.CALL_IN_FINISH_TS THEN
			lv_sql := lv_sql || 'CALL_IN_FINISH_TS = $2.CALL_IN_FINISH_TS, ';
			lv_filter := lv_filter || ' AND (CALL_IN_FINISH_TS IS NOT DISTINCT FROM $1.CALL_IN_FINISH_TS OR CALL_IN_FINISH_TS IS NOT DISTINCT FROM $2.CALL_IN_FINISH_TS)';
		END IF;
		IF OLD.CALL_IN_STATUS != NEW.CALL_IN_STATUS THEN
			lv_sql := lv_sql || 'CALL_IN_STATUS = $2.CALL_IN_STATUS, ';
			lv_filter := lv_filter || ' AND (CALL_IN_STATUS = $1.CALL_IN_STATUS OR CALL_IN_STATUS = $2.CALL_IN_STATUS)';
		END IF;
		IF OLD.CLIENT_IP_ADDRESS IS DISTINCT FROM NEW.CLIENT_IP_ADDRESS THEN
			lv_sql := lv_sql || 'CLIENT_IP_ADDRESS = $2.CLIENT_IP_ADDRESS, ';
			lv_filter := lv_filter || ' AND (CLIENT_IP_ADDRESS IS NOT DISTINCT FROM $1.CLIENT_IP_ADDRESS OR CLIENT_IP_ADDRESS IS NOT DISTINCT FROM $2.CLIENT_IP_ADDRESS)';
		END IF;
		IF OLD.CLIENT_PORT IS DISTINCT FROM NEW.CLIENT_PORT THEN
			lv_sql := lv_sql || 'CLIENT_PORT = $2.CLIENT_PORT, ';
			lv_filter := lv_filter || ' AND (CLIENT_PORT IS NOT DISTINCT FROM $1.CLIENT_PORT OR CLIENT_PORT IS NOT DISTINCT FROM $2.CLIENT_PORT)';
		END IF;
		IF OLD.INBOUND_MESSAGE_COUNT IS DISTINCT FROM NEW.INBOUND_MESSAGE_COUNT THEN
			lv_sql := lv_sql || 'INBOUND_MESSAGE_COUNT = $2.INBOUND_MESSAGE_COUNT, ';
			lv_filter := lv_filter || ' AND (INBOUND_MESSAGE_COUNT IS NOT DISTINCT FROM $1.INBOUND_MESSAGE_COUNT OR INBOUND_MESSAGE_COUNT IS NOT DISTINCT FROM $2.INBOUND_MESSAGE_COUNT)';
		END IF;
		IF OLD.OUTBOUND_MESSAGE_COUNT IS DISTINCT FROM NEW.OUTBOUND_MESSAGE_COUNT THEN
			lv_sql := lv_sql || 'OUTBOUND_MESSAGE_COUNT = $2.OUTBOUND_MESSAGE_COUNT, ';
			lv_filter := lv_filter || ' AND (OUTBOUND_MESSAGE_COUNT IS NOT DISTINCT FROM $1.OUTBOUND_MESSAGE_COUNT OR OUTBOUND_MESSAGE_COUNT IS NOT DISTINCT FROM $2.OUTBOUND_MESSAGE_COUNT)';
		END IF;
		IF OLD.INBOUND_BYTE_COUNT IS DISTINCT FROM NEW.INBOUND_BYTE_COUNT THEN
			lv_sql := lv_sql || 'INBOUND_BYTE_COUNT = $2.INBOUND_BYTE_COUNT, ';
			lv_filter := lv_filter || ' AND (INBOUND_BYTE_COUNT IS NOT DISTINCT FROM $1.INBOUND_BYTE_COUNT OR INBOUND_BYTE_COUNT IS NOT DISTINCT FROM $2.INBOUND_BYTE_COUNT)';
		END IF;
		IF OLD.OUTBOUND_BYTE_COUNT IS DISTINCT FROM NEW.OUTBOUND_BYTE_COUNT THEN
			lv_sql := lv_sql || 'OUTBOUND_BYTE_COUNT = $2.OUTBOUND_BYTE_COUNT, ';
			lv_filter := lv_filter || ' AND (OUTBOUND_BYTE_COUNT IS NOT DISTINCT FROM $1.OUTBOUND_BYTE_COUNT OR OUTBOUND_BYTE_COUNT IS NOT DISTINCT FROM $2.OUTBOUND_BYTE_COUNT)';
		END IF;
		IF OLD.DEVICE_INITIALIZED_FLAG IS DISTINCT FROM NEW.DEVICE_INITIALIZED_FLAG THEN
			lv_sql := lv_sql || 'DEVICE_INITIALIZED_FLAG = $2.DEVICE_INITIALIZED_FLAG, ';
			lv_filter := lv_filter || ' AND (DEVICE_INITIALIZED_FLAG IS NOT DISTINCT FROM $1.DEVICE_INITIALIZED_FLAG OR DEVICE_INITIALIZED_FLAG IS NOT DISTINCT FROM $2.DEVICE_INITIALIZED_FLAG)';
		END IF;
		IF OLD.DEVICE_SENT_CONFIG_FLAG IS DISTINCT FROM NEW.DEVICE_SENT_CONFIG_FLAG THEN
			lv_sql := lv_sql || 'DEVICE_SENT_CONFIG_FLAG = $2.DEVICE_SENT_CONFIG_FLAG, ';
			lv_filter := lv_filter || ' AND (DEVICE_SENT_CONFIG_FLAG IS NOT DISTINCT FROM $1.DEVICE_SENT_CONFIG_FLAG OR DEVICE_SENT_CONFIG_FLAG IS NOT DISTINCT FROM $2.DEVICE_SENT_CONFIG_FLAG)';
		END IF;
		IF OLD.SERVER_SENT_CONFIG_FLAG IS DISTINCT FROM NEW.SERVER_SENT_CONFIG_FLAG THEN
			lv_sql := lv_sql || 'SERVER_SENT_CONFIG_FLAG = $2.SERVER_SENT_CONFIG_FLAG, ';
			lv_filter := lv_filter || ' AND (SERVER_SENT_CONFIG_FLAG IS NOT DISTINCT FROM $1.SERVER_SENT_CONFIG_FLAG OR SERVER_SENT_CONFIG_FLAG IS NOT DISTINCT FROM $2.SERVER_SENT_CONFIG_FLAG)';
		END IF;
		IF OLD.CALL_IN_TYPE IS DISTINCT FROM NEW.CALL_IN_TYPE THEN
			lv_sql := lv_sql || 'CALL_IN_TYPE = $2.CALL_IN_TYPE, ';
			lv_filter := lv_filter || ' AND (CALL_IN_TYPE IS NOT DISTINCT FROM $1.CALL_IN_TYPE OR CALL_IN_TYPE IS NOT DISTINCT FROM $2.CALL_IN_TYPE)';
		END IF;
		IF OLD.AUTH_AMOUNT IS DISTINCT FROM NEW.AUTH_AMOUNT THEN
			lv_sql := lv_sql || 'AUTH_AMOUNT = $2.AUTH_AMOUNT, ';
			lv_filter := lv_filter || ' AND (AUTH_AMOUNT IS NOT DISTINCT FROM $1.AUTH_AMOUNT OR AUTH_AMOUNT IS NOT DISTINCT FROM $2.AUTH_AMOUNT)';
		END IF;
		IF OLD.AUTH_APPROVED_FLAG IS DISTINCT FROM NEW.AUTH_APPROVED_FLAG THEN
			lv_sql := lv_sql || 'AUTH_APPROVED_FLAG = $2.AUTH_APPROVED_FLAG, ';
			lv_filter := lv_filter || ' AND (AUTH_APPROVED_FLAG IS NOT DISTINCT FROM $1.AUTH_APPROVED_FLAG OR AUTH_APPROVED_FLAG IS NOT DISTINCT FROM $2.AUTH_APPROVED_FLAG)';
		END IF;
		IF OLD.AUTH_CARD_TYPE IS DISTINCT FROM NEW.AUTH_CARD_TYPE THEN
			lv_sql := lv_sql || 'AUTH_CARD_TYPE = $2.AUTH_CARD_TYPE, ';
			lv_filter := lv_filter || ' AND (AUTH_CARD_TYPE IS NOT DISTINCT FROM $1.AUTH_CARD_TYPE OR AUTH_CARD_TYPE IS NOT DISTINCT FROM $2.AUTH_CARD_TYPE)';
		END IF;
		IF OLD.DEX_FILE_SIZE IS DISTINCT FROM NEW.DEX_FILE_SIZE THEN
			lv_sql := lv_sql || 'DEX_FILE_SIZE = $2.DEX_FILE_SIZE, ';
			lv_filter := lv_filter || ' AND (DEX_FILE_SIZE IS NOT DISTINCT FROM $1.DEX_FILE_SIZE OR DEX_FILE_SIZE IS NOT DISTINCT FROM $2.DEX_FILE_SIZE)';
		END IF;
		IF OLD.DEX_RECEIVED_FLAG IS DISTINCT FROM NEW.DEX_RECEIVED_FLAG THEN
			lv_sql := lv_sql || 'DEX_RECEIVED_FLAG = $2.DEX_RECEIVED_FLAG, ';
			lv_filter := lv_filter || ' AND (DEX_RECEIVED_FLAG IS NOT DISTINCT FROM $1.DEX_RECEIVED_FLAG OR DEX_RECEIVED_FLAG IS NOT DISTINCT FROM $2.DEX_RECEIVED_FLAG)';
		END IF;
		IF OLD.TERMINAL_ID IS DISTINCT FROM NEW.TERMINAL_ID THEN
			lv_sql := lv_sql || 'TERMINAL_ID = $2.TERMINAL_ID, ';
			lv_filter := lv_filter || ' AND (TERMINAL_ID IS NOT DISTINCT FROM $1.TERMINAL_ID OR TERMINAL_ID IS NOT DISTINCT FROM $2.TERMINAL_ID)';
		END IF;
		IF OLD.CUSTOMER_ID IS DISTINCT FROM NEW.CUSTOMER_ID THEN
			lv_sql := lv_sql || 'CUSTOMER_ID = $2.CUSTOMER_ID, ';
			lv_filter := lv_filter || ' AND (CUSTOMER_ID IS NOT DISTINCT FROM $1.CUSTOMER_ID OR CUSTOMER_ID IS NOT DISTINCT FROM $2.CUSTOMER_ID)';
		END IF;
		IF OLD.LOCATION_NAME IS DISTINCT FROM NEW.LOCATION_NAME THEN
			lv_sql := lv_sql || 'LOCATION_NAME = $2.LOCATION_NAME, ';
			lv_filter := lv_filter || ' AND (LOCATION_NAME IS NOT DISTINCT FROM $1.LOCATION_NAME OR LOCATION_NAME IS NOT DISTINCT FROM $2.LOCATION_NAME)';
		END IF;
		IF OLD.CUSTOMER_NAME IS DISTINCT FROM NEW.CUSTOMER_NAME THEN
			lv_sql := lv_sql || 'CUSTOMER_NAME = $2.CUSTOMER_NAME, ';
			lv_filter := lv_filter || ' AND (CUSTOMER_NAME IS NOT DISTINCT FROM $1.CUSTOMER_NAME OR CUSTOMER_NAME IS NOT DISTINCT FROM $2.CUSTOMER_NAME)';
		END IF;
		IF OLD.CREDIT_TRANS_COUNT IS DISTINCT FROM NEW.CREDIT_TRANS_COUNT THEN
			lv_sql := lv_sql || 'CREDIT_TRANS_COUNT = $2.CREDIT_TRANS_COUNT, ';
			lv_filter := lv_filter || ' AND (CREDIT_TRANS_COUNT IS NOT DISTINCT FROM $1.CREDIT_TRANS_COUNT OR CREDIT_TRANS_COUNT IS NOT DISTINCT FROM $2.CREDIT_TRANS_COUNT)';
		END IF;
		IF OLD.CREDIT_TRANS_TOTAL IS DISTINCT FROM NEW.CREDIT_TRANS_TOTAL THEN
			lv_sql := lv_sql || 'CREDIT_TRANS_TOTAL = $2.CREDIT_TRANS_TOTAL, ';
			lv_filter := lv_filter || ' AND (CREDIT_TRANS_TOTAL IS NOT DISTINCT FROM $1.CREDIT_TRANS_TOTAL OR CREDIT_TRANS_TOTAL IS NOT DISTINCT FROM $2.CREDIT_TRANS_TOTAL)';
		END IF;
		IF OLD.CREDIT_VEND_COUNT IS DISTINCT FROM NEW.CREDIT_VEND_COUNT THEN
			lv_sql := lv_sql || 'CREDIT_VEND_COUNT = $2.CREDIT_VEND_COUNT, ';
			lv_filter := lv_filter || ' AND (CREDIT_VEND_COUNT IS NOT DISTINCT FROM $1.CREDIT_VEND_COUNT OR CREDIT_VEND_COUNT IS NOT DISTINCT FROM $2.CREDIT_VEND_COUNT)';
		END IF;
		IF OLD.CASH_TRANS_COUNT IS DISTINCT FROM NEW.CASH_TRANS_COUNT THEN
			lv_sql := lv_sql || 'CASH_TRANS_COUNT = $2.CASH_TRANS_COUNT, ';
			lv_filter := lv_filter || ' AND (CASH_TRANS_COUNT IS NOT DISTINCT FROM $1.CASH_TRANS_COUNT OR CASH_TRANS_COUNT IS NOT DISTINCT FROM $2.CASH_TRANS_COUNT)';
		END IF;
		IF OLD.CASH_TRANS_TOTAL IS DISTINCT FROM NEW.CASH_TRANS_TOTAL THEN
			lv_sql := lv_sql || 'CASH_TRANS_TOTAL = $2.CASH_TRANS_TOTAL, ';
			lv_filter := lv_filter || ' AND (CASH_TRANS_TOTAL IS NOT DISTINCT FROM $1.CASH_TRANS_TOTAL OR CASH_TRANS_TOTAL IS NOT DISTINCT FROM $2.CASH_TRANS_TOTAL)';
		END IF;
		IF OLD.CASH_VEND_COUNT IS DISTINCT FROM NEW.CASH_VEND_COUNT THEN
			lv_sql := lv_sql || 'CASH_VEND_COUNT = $2.CASH_VEND_COUNT, ';
			lv_filter := lv_filter || ' AND (CASH_VEND_COUNT IS NOT DISTINCT FROM $1.CASH_VEND_COUNT OR CASH_VEND_COUNT IS NOT DISTINCT FROM $2.CASH_VEND_COUNT)';
		END IF;
		IF OLD.PASSCARD_TRANS_COUNT IS DISTINCT FROM NEW.PASSCARD_TRANS_COUNT THEN
			lv_sql := lv_sql || 'PASSCARD_TRANS_COUNT = $2.PASSCARD_TRANS_COUNT, ';
			lv_filter := lv_filter || ' AND (PASSCARD_TRANS_COUNT IS NOT DISTINCT FROM $1.PASSCARD_TRANS_COUNT OR PASSCARD_TRANS_COUNT IS NOT DISTINCT FROM $2.PASSCARD_TRANS_COUNT)';
		END IF;
		IF OLD.PASSCARD_TRANS_TOTAL IS DISTINCT FROM NEW.PASSCARD_TRANS_TOTAL THEN
			lv_sql := lv_sql || 'PASSCARD_TRANS_TOTAL = $2.PASSCARD_TRANS_TOTAL, ';
			lv_filter := lv_filter || ' AND (PASSCARD_TRANS_TOTAL IS NOT DISTINCT FROM $1.PASSCARD_TRANS_TOTAL OR PASSCARD_TRANS_TOTAL IS NOT DISTINCT FROM $2.PASSCARD_TRANS_TOTAL)';
		END IF;
		IF OLD.PASSCARD_VEND_COUNT IS DISTINCT FROM NEW.PASSCARD_VEND_COUNT THEN
			lv_sql := lv_sql || 'PASSCARD_VEND_COUNT = $2.PASSCARD_VEND_COUNT, ';
			lv_filter := lv_filter || ' AND (PASSCARD_VEND_COUNT IS NOT DISTINCT FROM $1.PASSCARD_VEND_COUNT OR PASSCARD_VEND_COUNT IS NOT DISTINCT FROM $2.PASSCARD_VEND_COUNT)';
		END IF;
		IF OLD.LAST_AUTH_IN_TS IS DISTINCT FROM NEW.LAST_AUTH_IN_TS THEN
			lv_sql := lv_sql || 'LAST_AUTH_IN_TS = $2.LAST_AUTH_IN_TS, ';
			lv_filter := lv_filter || ' AND (LAST_AUTH_IN_TS IS NOT DISTINCT FROM $1.LAST_AUTH_IN_TS OR LAST_AUTH_IN_TS IS NOT DISTINCT FROM $2.LAST_AUTH_IN_TS)';
		END IF;
		IF OLD.LAST_TRANS_IN_TS IS DISTINCT FROM NEW.LAST_TRANS_IN_TS THEN
			lv_sql := lv_sql || 'LAST_TRANS_IN_TS = $2.LAST_TRANS_IN_TS, ';
			lv_filter := lv_filter || ' AND (LAST_TRANS_IN_TS IS NOT DISTINCT FROM $1.LAST_TRANS_IN_TS OR LAST_TRANS_IN_TS IS NOT DISTINCT FROM $2.LAST_TRANS_IN_TS)';
		END IF;
		IF OLD.FIRMWARE_VERSION IS DISTINCT FROM NEW.FIRMWARE_VERSION THEN
			lv_sql := lv_sql || 'FIRMWARE_VERSION = $2.FIRMWARE_VERSION, ';
			lv_filter := lv_filter || ' AND (FIRMWARE_VERSION IS NOT DISTINCT FROM $1.FIRMWARE_VERSION OR FIRMWARE_VERSION IS NOT DISTINCT FROM $2.FIRMWARE_VERSION)';
		END IF;
		IF OLD.COMM_METHOD_CD IS DISTINCT FROM NEW.COMM_METHOD_CD THEN
			lv_sql := lv_sql || 'COMM_METHOD_CD = $2.COMM_METHOD_CD, ';
			lv_filter := lv_filter || ' AND (COMM_METHOD_CD IS NOT DISTINCT FROM $1.COMM_METHOD_CD OR COMM_METHOD_CD IS NOT DISTINCT FROM $2.COMM_METHOD_CD)';
		END IF;
		IF OLD.COMMENTS IS DISTINCT FROM NEW.COMMENTS THEN
            lv_sql := lv_sql || 'COMMENTS = $2.COMMENTS, ';
            lv_filter := lv_filter || ' AND (COMMENTS IS NOT DISTINCT FROM $1.COMMENTS OR COMMENTS IS NOT DISTINCT FROM $2.COMMENTS)';
        END IF;
		IF OLD.DEX_STATUS IS DISTINCT FROM NEW.DEX_STATUS THEN
            lv_sql := lv_sql || 'DEX_STATUS = $2.DEX_STATUS, ';
            lv_filter := lv_filter || ' AND (DEX_STATUS IS NOT DISTINCT FROM $1.DEX_STATUS OR DEX_STATUS IS NOT DISTINCT FROM $2.DEX_STATUS)';
        END IF;

        lv_sql := lv_sql || 'LAST_UPDATED_UTC_TS = $2.LAST_UPDATED_UTC_TS, LAST_UPDATED_BY = $2.LAST_UPDATED_BY, ';
		lv_sql := LEFT(lv_sql, -2) || ' WHERE GLOBAL_SESSION_CD = $1.GLOBAL_SESSION_CD' || lv_filter;
		EXECUTE lv_sql USING OLD, NEW;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
		RETURN NEW;
	END IF;
	IF lv_new_partition_table IS NOT NULL THEN
		LOOP
			BEGIN
				EXECUTE 'INSERT INTO MAIN.' || lv_new_partition_table || ' SELECT $1.*' USING NEW;
				EXIT;
			EXCEPTION
			WHEN undefined_table THEN
				SELECT 'CONSTRAINT CK_' || lv_new_partition_table || ' CHECK(' ||'CALL_IN_START_TS >= ''' || TO_CHAR(NEW.CALL_IN_START_TS, 'YYYY-MM-DD') || '''::TIMESTAMP WITH TIME ZONE AND CALL_IN_START_TS < ''' || TO_CHAR(NEW.CALL_IN_START_TS + '1 DAY'::INTERVAL, 'YYYY-MM-DD') || '''::TIMESTAMP WITH TIME ZONE'||'), ' INTO lv_check_exp;
				RAISE NOTICE 'Creating new partition table %', lv_new_partition_table;
				BEGIN
					EXECUTE 'CREATE TABLE MAIN.' || lv_new_partition_table || '(' || lv_check_exp ||'CONSTRAINT PK_' || lv_new_partition_table || ' PRIMARY KEY(GLOBAL_SESSION_CD)) INHERITS(MAIN._DEVICE_SESSION)';
				EXCEPTION
					WHEN duplicate_table THEN
						CONTINUE;
				END;
				EXECUTE 'GRANT ALL ON TABLE MAIN.' || lv_new_partition_table || ' TO ' || CURRENT_USER;
				PERFORM MAIN.COPY_INDEXES('_DEVICE_SESSION', lv_new_suffix);
				EXECUTE 'ALTER TABLE MAIN.' || lv_new_partition_table || ' ADD FOREIGN KEY(NET_LAYER_ID) REFERENCES MAIN.NET_LAYER(NET_LAYER_ID)';
				EXECUTE 'ALTER TABLE MAIN.' || lv_new_partition_table || ' ADD FOREIGN KEY(DEVICE_TYPE_ID) REFERENCES MAIN.DEVICE_TYPE(DEVICE_TYPE_ID)';
				EXECUTE 'ALTER TABLE MAIN.' || lv_new_partition_table || ' ADD FOREIGN KEY(SESSION_STATE_ID) REFERENCES MAIN.SESSION_STATE(SESSION_STATE_ID)';
				EXECUTE 'ALTER TABLE MAIN.' || lv_new_partition_table || ' ADD FOREIGN KEY(COMM_METHOD_CD) REFERENCES MAIN.COMM_METHOD(COMM_METHOD_CD)';
			END;
		END LOOP;
	END IF;
	IF lv_old_partition_table IS NOT NULL THEN
		EXECUTE 'DELETE FROM MAIN.' || lv_old_partition_table || ' WHERE GLOBAL_SESSION_CD = $1.GLOBAL_SESSION_CD' USING OLD;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		RETURN NEW;
	ELSE
		RETURN OLD;
	END IF;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;

INSERT INTO MAIN.OBJECT_TYPE(OBJECT_TYPE_CD, OBJECT_TYPE_NAME)
SELECT 'ach', 'ACH' WHERE NOT EXISTS (SELECT 1 FROM MAIN.OBJECT_TYPE WHERE OBJECT_TYPE_CD = 'ach');

INSERT INTO MAIN.OBJECT_TYPE(OBJECT_TYPE_CD, OBJECT_TYPE_NAME)
SELECT 'affiliate', 'Sales Affiliate' WHERE NOT EXISTS (SELECT 1 FROM MAIN.OBJECT_TYPE WHERE OBJECT_TYPE_CD = 'affiliate');

INSERT INTO MAIN.OBJECT_TYPE(OBJECT_TYPE_CD, OBJECT_TYPE_NAME)
SELECT 'category', 'Sales Category' WHERE NOT EXISTS (SELECT 1 FROM MAIN.OBJECT_TYPE WHERE OBJECT_TYPE_CD = 'category');

INSERT INTO MAIN.OBJECT_TYPE(OBJECT_TYPE_CD, OBJECT_TYPE_NAME)
SELECT 'pricingTier', 'Pricing Tier' WHERE NOT EXISTS (SELECT 1 FROM MAIN.OBJECT_TYPE WHERE OBJECT_TYPE_CD = 'pricingTier');

INSERT INTO MAIN.OBJECT_TYPE(OBJECT_TYPE_CD, OBJECT_TYPE_NAME)
SELECT 'riskAlertId', 'Risk Alert' WHERE NOT EXISTS (SELECT 1 FROM MAIN.OBJECT_TYPE WHERE OBJECT_TYPE_CD = 'riskAlertId');

INSERT INTO MAIN.OBJECT_TYPE(OBJECT_TYPE_CD, OBJECT_TYPE_NAME)
SELECT 'salesRep', 'Sales Rep' WHERE NOT EXISTS (SELECT 1 FROM MAIN.OBJECT_TYPE WHERE OBJECT_TYPE_CD = 'salesRep');

INSERT INTO MAIN.OBJECT_TYPE(OBJECT_TYPE_CD, OBJECT_TYPE_NAME)
SELECT 'transaction', 'Transaction' WHERE NOT EXISTS (SELECT 1 FROM MAIN.OBJECT_TYPE WHERE OBJECT_TYPE_CD = 'transaction');

COMMIT;
