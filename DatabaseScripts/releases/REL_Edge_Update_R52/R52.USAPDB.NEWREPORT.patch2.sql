ALTER SESSION SET CURRENT_SCHEMA = REPORT;    
DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := -1;
BEGIN
	SELECT NVL(MAX(REPORT_ID),-1) INTO LN_REPORT_ID
	FROM REPORT.REPORTS WHERE REPORT_NAME = 'Activity By Fill Date And Region';
	
	IF LN_REPORT_ID > 0 THEN
		RETURN;
	END IF;

	SELECT max(report_id)+1 INTO LN_REPORT_ID FROM REPORT.REPORTS;
	
	INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
	VALUES( LN_REPORT_ID, 'Activity By Fill Date And Region {b} to {e}', 6, 0, 'Activity By Fill Date And Region', 'Report shows transactions amount by currency,region,device,location,asset nbr and terminal during the fill begin date and fill end date group by region.', 'E', 0);

	insert into report.report_param (report_id,param_name,param_value) values(LN_REPORT_ID, 'folioId','1982');
	insert into report.report_param (report_id,param_name,param_value) values(LN_REPORT_ID, 'outputType','27');
	insert into report.report_param (report_id,param_name,param_value) values(LN_REPORT_ID, 'params.beginDate','{b}');
	insert into report.report_param (report_id,param_name,param_value) values(LN_REPORT_ID, 'params.endDate','{e}');
	insert into report.report_param (report_id,param_name,param_value) values(LN_REPORT_ID, 'dateFormat','MM-dd-yyyy');
	commit;

END;
/

DECLARE
	LN_WEB_LINK_ID web_content.web_link.WEB_LINK_ID%TYPE;
BEGIN
	
	SELECT MAX(web_link_id) INTO LN_WEB_LINK_ID
	FROM web_content.web_link WHERE web_link_label='Activity By Fill Date And Region';

	IF LN_WEB_LINK_ID > 0 THEN
		RETURN;
	END IF;
	
	select max(web_link_id)+1 into LN_WEB_LINK_ID from web_content.web_link;
		
	insert into web_content.web_link(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER) values(LN_WEB_LINK_ID, 'Activity By Fill Date And Region', './select_date_range_frame.i?folioId=1982'||'&'||'startParamName=params.beginDate'||'&'||'StartDate={*DAY-1}'||'&'||'endParamName=params.endDate'||'&'||'EndDate={*DAY-1}', 'Activity By Fill Date And Region', 'Daily Operations', 'W', 0);
	insert into web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) select web_link_id, 1 from web_content.web_link where web_link_label = 'Activity By Fill Date And Region';

	COMMIT;
END;
/

ALTER SESSION SET CURRENT_SCHEMA = REPORT;    
DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := -1;
BEGIN
	SELECT NVL(MAX(REPORT_ID),-1) INTO LN_REPORT_ID
	FROM REPORT.REPORTS WHERE REPORT_NAME = 'Activity By Fill Date And Region-Credit';
	
	IF LN_REPORT_ID > 0 THEN
		RETURN;
	END IF;

	SELECT max(report_id)+1 INTO LN_REPORT_ID FROM REPORT.REPORTS;
	
	INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
	VALUES( LN_REPORT_ID, 'Activity By Fill Date And Region-Credit {b} to {e}', 6, 0, 'Activity By Fill Date And Region-Credit', 'Report shows credit and credit contactless transactions amount by currency,region,device,location,asset nbr and terminal during the fill begin date and fill end date group by region.', 'E', 0);

	insert into report.report_param (report_id,param_name,param_value) values(LN_REPORT_ID, 'folioId','1984');
	insert into report.report_param (report_id,param_name,param_value) values(LN_REPORT_ID, 'outputType','27');
	insert into report.report_param (report_id,param_name,param_value) values(LN_REPORT_ID, 'params.beginDate','{b}');
	insert into report.report_param (report_id,param_name,param_value) values(LN_REPORT_ID, 'params.endDate','{e}');
	insert into report.report_param (report_id,param_name,param_value) values(LN_REPORT_ID, 'dateFormat','MM-dd-yyyy');
	commit;

END;
/

DECLARE
	LN_WEB_LINK_ID web_content.web_link.WEB_LINK_ID%TYPE;
BEGIN
	
	SELECT MAX(web_link_id) INTO LN_WEB_LINK_ID
	FROM web_content.web_link WHERE web_link_label='Activity By Fill Date And Region-Credit';

	IF LN_WEB_LINK_ID > 0 THEN
		RETURN;
	END IF;
	
	select max(web_link_id)+1 into LN_WEB_LINK_ID from web_content.web_link;
		
	insert into web_content.web_link(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER) values(LN_WEB_LINK_ID, 'Activity By Fill Date And Region-Credit', './select_date_range_frame.i?folioId=1984'||'&'||'startParamName=params.beginDate'||'&'||'StartDate={*DAY-1}'||'&'||'endParamName=params.endDate'||'&'||'EndDate={*DAY-1}', 'Activity By Fill Date And Region-Credit', 'Daily Operations', 'W', 0);
	insert into web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) select web_link_id, 1 from web_content.web_link where web_link_label = 'Activity By Fill Date And Region-Credit';

	COMMIT;
END;
/