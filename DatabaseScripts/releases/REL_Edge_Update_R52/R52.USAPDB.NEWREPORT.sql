DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := -1;
	LV_REPORT_NAME VARCHAR(50) := 'All MORE Accounts';
	LN_WEB_LINK_ID web_content.web_link.WEB_LINK_ID%TYPE;
BEGIN
	
	SELECT MAX(REPORT_ID) INTO LN_REPORT_ID
	FROM REPORT.REPORTS WHERE REPORT_NAME = LV_REPORT_NAME;

	IF LN_REPORT_ID > 0 THEN
		RETURN;
	ELSE
		LN_REPORT_ID:=441;
	END IF;
		
	INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
	VALUES( LN_REPORT_ID, 'All MORE Accounts', 6, 0, 'All MORE Accounts', 'Report that shows detailed registered or unregistered more card accounts information', 'U', 0);
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
	VALUES( LN_REPORT_ID, 'query', 'SELECT CUS.CUSTOMER_NAME "Operator Name", R.REGION_NAME "Region Name", C.CONSUMER_FNAME "First Name", C.CONSUMER_LNAME "Last Name", CA.CONSUMER_ACCT_IDENTIFIER "Card Id",
CA.CONSUMER_ACCT_BALANCE "Account Balance", CA.NICK_NAME "Card Nickname", CA.CONSUMER_ACCT_CD "Card Number", C.CONSUMER_EMAIL_ADDR1 "Email Address", C.CONSUMER_CELL_PHONE_NUM "Mobile Phone Number",
CA.CONSUMER_ACCT_ACTIVE_YN_FLAG "Account Enabled", DBADMIN.UTC_TO_LOCAL_DATE(CC.CREATED_UTC_TS) "Registration Time", CC.CREDENTIAL_ACTIVE_FLAG "Account Activated",
CC.ACTIVATED_TS "Activation Time", CAS.CONSUMER_ACCT_SUB_TYPE_DESC "Account Sub Type", CA.CONSUMER_ACCT_PROMO_TOTAL "Promo Total",
CA.CONSUMER_ACCT_REPLENISH_TOTAL "Replenish Total", CA.REPLENISH_BONUS_TOTAL "Replenish Bonus Total",
CA.LOYALTY_DISCOUNT_TOTAL "Loyalty Discount Total", CA.CASH_BACK_TOTAL "Cashback Total",
RT.REPLENISH_TYPE_DESC "Replenish Type", CAR.REPLENISH_AMOUNT "Replenish Amount", CAR.REPLENISH_THRESHHOLD "Replenish Threshold",
CAR.REPLENISH_CARD_MASKED "Replenish Card", CAR.LAST_REPLENISH_TRAN_TS "Last Replenish Time",
CA.CONSUMER_ACCT_REPLENISH_TOTAL+CA.CONSUMER_ACCT_PROMO_TOTAL-CA.CONSUMER_ACCT_BALANCE "Total Tran Amount",
CA.TRAN_COUNT_TOTAL "Total Tran Count"
FROM PSS.CONSUMER C
JOIN PSS.CONSUMER_ACCT CA ON C.CONSUMER_ID = CA.CONSUMER_ID
JOIN PSS.CONSUMER_ACCT_SUB_TYPE CAS ON CA.CONSUMER_ACCT_SUB_TYPE_ID = CAS.CONSUMER_ACCT_SUB_TYPE_ID
LEFT OUTER JOIN PSS.CONSUMER_CREDENTIAL CC ON C.CONSUMER_ID = CC.CONSUMER_ID
JOIN CORP.CUSTOMER CUS ON CA.CORP_CUSTOMER_ID = CUS.CUSTOMER_ID
LEFT OUTER JOIN REPORT.REGION R ON C.REGION_ID = R.REGION_ID
LEFT OUTER JOIN PSS.CONSUMER_ACCT_REPLENISH CAR ON CA.CONSUMER_ACCT_REPLENISH_ID = CAR.CONSUMER_ACCT_REPLENISH_ID
LEFT OUTER JOIN PSS.REPLENISH_TYPE RT ON CAR.REPLENISH_TYPE_ID = RT.REPLENISH_TYPE_ID
JOIN REPORT.VW_USER_CONSUMER_ACCT VUCA ON VUCA.CONSUMER_ACCT_ID=CA.CONSUMER_ACCT_ID
JOIN REPORT.USER_LOGIN UL ON UL.USER_ID=VUCA.USER_ID
WHERE CA.CONSUMER_ACCT_TYPE_ID = 3
AND UL.USER_ID = ?
ORDER BY CUS.CUSTOMER_NAME, CC.CREATED_UTC_TS,CA.CONSUMER_ACCT_BALANCE, C.CONSUMER_FNAME, C.CONSUMER_LNAME');
	
	insert into report.report_param (report_id,param_name,param_value) values(LN_REPORT_ID, 'params.userId','{u}');
	insert into report.report_param (report_id,param_name,param_value) values(LN_REPORT_ID, 'paramNames','userId');
	insert into report.report_param (report_id,param_name,param_value) values(LN_REPORT_ID, 'paramTypes','NUMERIC');
	insert into report.report_param (report_id,param_name,param_value) values(LN_REPORT_ID, 'isSQLFolio','true');
	
	insert into web_content.web_link values(web_content.seq_web_link_id.nextval, 'All MORE Accounts', './run_sql_folio_report_async.i?basicReportId='||LN_REPORT_ID,'All MORE Accounts','Daily Operations','P', 0);
	insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 1);
	insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 45);
	commit;
END;
/

update report.report_param set param_value='SELECT
lts.entry_type,
bu.business_unit_name,
DECODE(bat.payment_schedule_id, 4, ''Y'', ''N'') monthly_payment,
cu.currency_code,
c.customer_name,
ee.eport_num,
lts.fee_name,
DECODE(d.status,
	''S'', ''Y'',
	''P'', ''Y'',
	''N''
) in_eft,
TO_CHAR(TRUNC(lts.entry_date, ''MONTH''),''mm/dd/yyyy'') month_for,
COUNT(1) entry_count,
SUM (lts.amount) total_entry_amount,
lts.description,
t.terminal_nbr,
t.sales_order_number
FROM (
	SELECT
	led.batch_id,
	led.amount,
	led.entry_type,
	led.settle_state_id,
	led.create_date,
	led.entry_date,
	led.description,
	sfi.fee_name
	FROM corp.ledger led
	LEFT OUTER JOIN report.trans tr
	ON tr.tran_id = led.trans_id
	LEFT OUTER JOIN (
		SELECT
		sf.service_fee_id,
		f.fee_name
		FROM corp.service_fees sf
		JOIN corp.fees f
		ON f.fee_id = sf.fee_id
	) sfi
	ON sfi.service_fee_id = led.service_fee_id
	WHERE led.entry_type IN (''SF'', ''AD'', ''SB'')
	AND led.deleted = ''N''
	AND led.settle_state_id IN (2, 3)
	AND (led.report_date >= CAST(? AS DATE) AND led.report_date < CAST(? AS DATE) + 1
	OR led.create_date >= CAST(? AS DATE) AND led.create_date < CAST(? AS DATE) + 1 AND led.report_date IS NULL)
)lts
JOIN corp.batch bat
ON bat.batch_id = lts.batch_id
JOIN corp.doc d
ON d.doc_id = bat.doc_id
JOIN corp.customer_bank cb
ON cb.customer_bank_id = d.customer_bank_id
JOIN corp.customer c
ON c.customer_id = cb.customer_id
JOIN corp.business_unit bu
ON bu.business_unit_id = d.business_unit_id
JOIN corp.currency cu
ON cu.currency_id = d.currency_id
JOIN report.trans_state ts
ON ts.state_id = lts.settle_state_id
LEFT outer JOIN (SELECT DISTINCT TERMINAL_ID, FIRST_VALUE(eport_serial_num) OVER (PARTITION BY TERMINAL_ID ORDER BY start_date DESC) Eport_num
from report.terminal_eport te, report.eport e where te.eport_id=e.eport_id) ee
ON bat.terminal_id=ee.terminal_id
LEFT OUTER JOIN report.terminal t ON bat.terminal_id = t.terminal_id
GROUP BY
lts.entry_type,
bu.business_unit_name,
DECODE(bat.payment_schedule_id, 4, ''Y'', ''N''),
cu.currency_code,
c.customer_name,
lts.fee_name,
DECODE(d.status,
	''S'', ''Y'',
	''P'', ''Y'',
	''N''
),
TRUNC(lts.entry_date, ''MONTH''),
lts.description
,ee.Eport_num,
t.terminal_nbr,
t.sales_order_number'
where param_name='query' and report_id =(select report_id from report.reports where report_name = 'All Other Fee Entries With Device');
commit;

create or replace FUNCTION REPORT.GET_CASHLESS(pd_tran_date IN REPORT.TRANS_STAT_BY_DAY.TRAN_DATE%TYPE,
pn_terminal_id IN REPORT.TRANS_STAT_BY_DAY.TERMINAL_ID%TYPE,
pn_eport_id IN REPORT.TRANS_STAT_BY_DAY.EPORT_ID%TYPE)
   RETURN NUMBER
IS
   v_amount   REPORT.TRANS_STAT_BY_DAY.TRAN_AMOUNT%TYPE;
BEGIN
   select coalesce(sum(tran_amount),0) into v_amount
   FROM report.trans_stat_by_day tsbd 
   where tsbd.tran_date=trunc(pd_tran_date)
   and tsbd.trans_type_id in (select trans_type_id from report.trans_type where CASHLESS_DEVICE_TRAN_IND='Y') 
   and tsbd.terminal_id=pn_terminal_id and tsbd.eport_id=pn_eport_id;

   RETURN v_amount;
END;
/

GRANT EXECUTE on REPORT.GET_CASHLESS to USAT_DEV_READ_ONLY;
GRANT EXECUTE on REPORT.GET_CASHLESS to USALIVE_APP_ROLE;

DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := -1;
	LV_REPORT_NAME VARCHAR(50) := 'Device Health with Cashless Sales';
	LN_WEB_LINK_ID web_content.web_link.WEB_LINK_ID%TYPE;
	l_env VARCHAR2(3); 
	l_user_id NUMBER;
	l_query VARCHAR2(4000);
BEGIN
	
	SELECT MAX(REPORT_ID) INTO LN_REPORT_ID
	FROM REPORT.REPORTS WHERE REPORT_NAME = LV_REPORT_NAME;

	IF LN_REPORT_ID > 0 THEN
		RETURN;
	ELSE
		LN_REPORT_ID:=453;
	END IF;
	
	l_query:='SELECT 
	c.CUSTOMER_NAME "Customer",
	r.REGION_NAME "Region",
	t.ASSET_NBR "Asset #",
	l.LOCATION_NAME "Location",
	e.EPORT_SERIAL_NUM "Device",
	floor((sysdate-e.LASTDIALIN_DATE)) "# Days Since Last Call-in",
	REPORT.GET_CASHLESS(sysdate-1,t.terminal_id,e.eport_id) "1 Day Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-2,t.terminal_id,e.eport_id) "2 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-3,t.terminal_id,e.eport_id) "3 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-4,t.terminal_id,e.eport_id) "4 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-5,t.terminal_id,e.eport_id) "5 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-6,t.terminal_id,e.eport_id) "6 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-7,t.terminal_id,e.eport_id) "7 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-8,t.terminal_id,e.eport_id) "8 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-9,t.terminal_id,e.eport_id) "9 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-10,t.terminal_id,e.eport_id) "10 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-11,t.terminal_id,e.eport_id) "11 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-12,t.terminal_id,e.eport_id) "12 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-13,t.terminal_id,e.eport_id) "13 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-14,t.terminal_id,e.eport_id) "14 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-15,t.terminal_id,e.eport_id) "15 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-16,t.terminal_id,e.eport_id) "16 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-17,t.terminal_id,e.eport_id) "17 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-18,t.terminal_id,e.eport_id) "18 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-19,t.terminal_id,e.eport_id) "19 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-20,t.terminal_id,e.eport_id) "20 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-21,t.terminal_id,e.eport_id) "21 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-22,t.terminal_id,e.eport_id) "22 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-23,t.terminal_id,e.eport_id) "23 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-24,t.terminal_id,e.eport_id) "24 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-25,t.terminal_id,e.eport_id) "25 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-26,t.terminal_id,e.eport_id) "26 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-27,t.terminal_id,e.eport_id) "27 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-28,t.terminal_id,e.eport_id) "28 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-29,t.terminal_id,e.eport_id) "29 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-30,t.terminal_id,e.eport_id) "30 Days Ago Cashless Amount"
	FROM CORP.CUSTOMER c
	    JOIN REPORT.TERMINAL t ON t.CUSTOMER_ID = c.CUSTOMER_ID AND t.STATUS != ''D''
	    JOIN REPORT.TERMINAL_EPORT te ON t.TERMINAL_ID = te.TERMINAL_ID
	    JOIN REPORT.EPORT e ON e.EPORT_ID = te.EPORT_ID
	    JOIN REPORT.LOCATION l ON t.LOCATION_ID = l.LOCATION_ID
	    JOIN REPORT.VW_USER_TERMINAL vut ON t.TERMINAL_ID = vut.TERMINAL_ID AND vut.TERMINAL_ID != 0
	    JOIN REPORT.USER_LOGIN ul ON ul.USER_ID = vut.USER_ID
	    LEFT OUTER JOIN (REPORT.TERMINAL_REGION tr
	    JOIN REPORT.REGION r ON tr.REGION_ID = r.REGION_ID) ON t.TERMINAL_ID = tr.TERMINAL_ID
	WHERE te.END_DATE IS NULL
	  AND vut.USER_ID =?';
  select NVL(MAX(DECODE(substr(subdomain_url, length('hotchoice') + 1, length(subdomain_url) - length('hotchoice.usatech.com')), '-ecc', 'ECC', '-dev', 'DEV', '-int', 'INT', null, 'PRD')), 'DEV')
  into l_env
  from WEB_CONTENT.subdomain where subdomain_url like 'hotchoice%.usatech.com'
  and subdomain_url not IN('hotchoice-PRD.usatech.com','hotchoice-USA.usatech.com') ;
  
  IF l_env = 'PRD' THEN
    insert into web_content.web_link values(web_content.seq_web_link_id.nextval, 'Device Health with Cashless Sales', './run_device_health_with_cashless_sales.i?basicReportId=453','Device Health with Cashless Sales','Daily Operations','-', 0);
    insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 1);
    FOR l_cur in (select user_id from report.user_login where customer_id=54887 and status='A' ) LOOP
		INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
		VALUES( LN_REPORT_ID, 'Device Health with Cashless Sales', 6, 0, 'Device Health with Cashless Sales', 'Report that shows device last call-in and last 30 days of cashless amount', 'U', l_cur.user_id);
		INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
		VALUES( LN_REPORT_ID, 'query', l_query);
		
		insert into report.report_param (report_id,param_name,param_value) values(LN_REPORT_ID, 'params.userId','{u}');
		insert into report.report_param (report_id,param_name,param_value) values(LN_REPORT_ID, 'paramNames','userId');
		insert into report.report_param (report_id,param_name,param_value) values(LN_REPORT_ID, 'paramTypes','NUMERIC');
		insert into report.report_param (report_id,param_name,param_value) values(LN_REPORT_ID, 'isSQLFolio','true');
		insert into report.report_param (report_id,param_name,param_value) values(LN_REPORT_ID, 'header', 'true');
		insert into report.report_param (report_id,param_name,param_value) values(LN_REPORT_ID, 'outputType', '27');
		LN_REPORT_ID:=LN_REPORT_ID+1;
		insert into report.user_link values(l_cur.user_id,web_content.seq_web_link_id.currval,'Y','W');
	END LOOP;
	
  ELSE
  	select user_id into l_user_id from report.user_login where user_name='aroyce@usatech.com';
  	
  	INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
	VALUES( LN_REPORT_ID, 'Device Health with Cashless Sales', 6, 0, 'Device Health with Cashless Sales', 'Report that shows device last call-in and last 30 days of cashless amount', 'U', l_user_id);
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
	VALUES( LN_REPORT_ID, 'query', l_query);
	
	insert into report.report_param (report_id,param_name,param_value) values(LN_REPORT_ID, 'params.userId','{u}');
	insert into report.report_param (report_id,param_name,param_value) values(LN_REPORT_ID, 'paramNames','userId');
	insert into report.report_param (report_id,param_name,param_value) values(LN_REPORT_ID, 'paramTypes','NUMERIC');
	insert into report.report_param (report_id,param_name,param_value) values(LN_REPORT_ID, 'isSQLFolio','true');
	insert into report.report_param(report_id, param_name, param_value) values(LN_REPORT_ID, 'header', 'true');
	insert into report.report_param(report_id, param_name, param_value) values(LN_REPORT_ID, 'outputType', '27');
	
	insert into web_content.web_link values(web_content.seq_web_link_id.nextval, 'Device Health with Cashless Sales', './run_device_health_with_cashless_sales.i?basicReportId='||LN_REPORT_ID,'Device Health With Cashless Sales','Daily Operations','-', 0);
	insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 1);
	insert into report.user_link values(l_user_id,web_content.seq_web_link_id.currval,'Y','W');
  END IF;
		
	
	commit;
END;
/


