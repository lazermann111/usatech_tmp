INSERT INTO REPORT.REPORTS(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
  SELECT 468, 'Multiple Devices Per Terminal', 6, 0, 'Multiple Devices Per Terminal', 'List of terminals with multiple device records', 'N', USER_ID
	FROM REPORT.USER_LOGIN 
   WHERE USER_NAME = 'USATMaster';

INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE)
SELECT 468, PARAM_NAME, PARAM_VALUE
  FROM (SELECT '' PARAM_NAME, '' PARAM_VALUE FROM DUAL WHERE 1=0
  UNION ALL SELECT 'query', 'select c.customer_name, t.terminal_nbr, e.eport_serial_num,
to_char(d.last_activity_ts, ''mm/dd/yyyy hh24:mi:ss'') last_activity_ts
from report.terminal t
join corp.customer c on t.customer_id = c.customer_id
join report.vw_terminal_eport te on t.terminal_id = te.terminal_id
join report.eport e on te.eport_id = e.eport_id
left outer join (device.device d
join device.device_last_active dla on d.device_id = dla.device_id
) on e.eport_serial_num = d.device_serial_cd
where t.status != ''D'' and t.terminal_id in (
select terminal_id from report.vw_terminal_eport
group by terminal_id having count(1) > 1
) order by t.terminal_nbr, d.last_activity_ts desc nulls last' FROM DUAL
UNION ALL SELECT 'header', 'true' FROM DUAL
UNION ALL SELECT 'isSQLFolio', 'true' FROM DUAL
UNION ALL SELECT 'outputType', '21' FROM DUAL 
  ) P
 WHERE NOT EXISTS(
 SELECT 1
	FROM REPORT.REPORT_PARAM RP0 
   WHERE RP0.REPORT_ID = 468
	 AND RP0.PARAM_NAME = P.PARAM_NAME);

INSERT INTO REPORT.USER_REPORT (USER_REPORT_ID, REPORT_ID, USER_ID, STATUS, CCS_TRANSPORT_ID, FREQUENCY_ID, LATENCY, UPD_DT)
SELECT REPORT.USER_REPORT_SEQ.NEXTVAL, REPORT_ID, USER_ID, STATUS, CCS_TRANSPORT_ID, FREQUENCY_ID, LATENCY, SYSDATE - 13
  FROM (SELECT 468 REPORT_ID, U.USER_ID, 'A' STATUS, T.CCS_TRANSPORT_ID, 1 FREQUENCY_ID, 1 LATENCY
  FROM REPORT.USER_LOGIN U
  CROSS JOIN REPORT.CCS_TRANSPORT T
 WHERE U.USER_NAME = 'USATMaster'
   AND T.CCS_TRANSPORT_NAME = 'Customer Service') R
 WHERE NOT EXISTS(SELECT 1
  FROM REPORT.USER_REPORT UR0
 WHERE UR0.REPORT_ID = R.REPORT_ID
   AND UR0.USER_ID = R.USER_ID);

COMMIT;

INSERT INTO REPORT.REPORTS(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
VALUES(469, 'Apriva Device Payment Activations - {b} to {e}', 6, 0, 'Apriva Device Payment Activations', 'List of devices with activated Apriva payment types', 'N', 0);

INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE)
SELECT 469, PARAM_NAME, PARAM_VALUE
  FROM (SELECT '' PARAM_NAME, '' PARAM_VALUE FROM DUAL WHERE 1=0
  UNION ALL SELECT 'query', 'select distinct c.customer_name, d.device_serial_cd,
first_value(ps.payment_subtype_name) over (partition by d.device_serial_cd order by pp.pos_pta_activation_ts, lower(ps.payment_subtype_name)) payment_type,
to_char(first_value(pp.pos_pta_activation_ts) over (partition by d.device_serial_cd order by pp.pos_pta_activation_ts, lower(ps.payment_subtype_name)), ''mm/dd/yyyy hh24:mi:ss'') payment_activation_ts
from pss.payment_subtype ps
join pss.pos_pta pp on ps.payment_subtype_id = pp.payment_subtype_id
join pss.pos p on pp.pos_id = p.pos_id
join device.device d on p.device_id = d.device_id
left outer join (report.eport e
join report.vw_terminal_eport te on e.eport_id = te.eport_id
join report.terminal t on te.terminal_id = t.terminal_id
join corp.customer c on t.customer_id = c.customer_id
) on d.device_serial_cd = e.eport_serial_num
where ps.payment_subtype_class = ''Apriva''
and pp.pos_pta_activation_ts >= cast(? as date) and pp.pos_pta_activation_ts < cast(? as date) + 1
order by lower(c.customer_name), d.device_serial_cd' FROM DUAL
UNION ALL SELECT 'header', 'true' FROM DUAL
UNION ALL SELECT 'isSQLFolio', 'true' FROM DUAL
UNION ALL SELECT 'outputType', '21' FROM DUAL
UNION ALL SELECT 'dateFormat', 'MM/dd/yyyy' FROM DUAL
UNION ALL SELECT 'params.StartDate', '{b}' FROM DUAL
UNION ALL SELECT 'params.EndDate', '{e}' FROM DUAL
UNION ALL SELECT 'paramNames', 'StartDate,EndDate' FROM DUAL
UNION ALL SELECT 'paramTypes', 'TIMESTAMP,TIMESTAMP' FROM DUAL
  ) P
 WHERE NOT EXISTS(
 SELECT 1
	FROM REPORT.REPORT_PARAM RP0 
   WHERE RP0.REPORT_ID = 469
	 AND RP0.PARAM_NAME = P.PARAM_NAME);

INSERT INTO web_content.web_link(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER) VALUES(web_content.seq_web_link_id.nextval, 'Apriva Device Payment Activations', './select_date_range_frame_sqlfolio.i?basicReportId=' || 469 || '&' || 'outputType=22', 'Apriva Device Payment Activations report', 'Accounting', 'W', 0);
INSERT INTO web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) SELECT web_link_id, 1 FROM web_content.web_link WHERE web_link_label = 'Apriva Device Payment Activations';
INSERT INTO web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) SELECT web_link_id, 14 FROM web_content.web_link WHERE web_link_label = 'Apriva Device Payment Activations';	 
	 
COMMIT;
