DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := -1;
	LV_REPORT_NAME VARCHAR(50) := 'Device Health with Cashless Sales';
	LN_WEB_LINK_ID web_content.web_link.WEB_LINK_ID%TYPE;
	l_env VARCHAR2(3); 
	l_user_id NUMBER;
	l_query VARCHAR2(4000);
BEGIN
	
	SELECT MAX(REPORT_ID) INTO LN_REPORT_ID
	FROM REPORT.REPORTS WHERE REPORT_NAME = LV_REPORT_NAME;

	IF LN_REPORT_ID > 0 THEN
		RETURN;
	ELSE
		LN_REPORT_ID:=457;
	END IF;
	
	l_query:='SELECT 
	c.CUSTOMER_NAME "Customer",
	r.REGION_NAME "Region",
	t.ASSET_NBR "Asset #",
	l.LOCATION_NAME "Location",
	e.EPORT_SERIAL_NUM "Device",
	floor((sysdate-e.LASTDIALIN_DATE)) "# Days Since Last Call-in",
	REPORT.GET_CASHLESS(sysdate-1,t.terminal_id,e.eport_id) "1 Day Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-2,t.terminal_id,e.eport_id) "2 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-3,t.terminal_id,e.eport_id) "3 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-4,t.terminal_id,e.eport_id) "4 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-5,t.terminal_id,e.eport_id) "5 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-6,t.terminal_id,e.eport_id) "6 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-7,t.terminal_id,e.eport_id) "7 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-8,t.terminal_id,e.eport_id) "8 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-9,t.terminal_id,e.eport_id) "9 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-10,t.terminal_id,e.eport_id) "10 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-11,t.terminal_id,e.eport_id) "11 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-12,t.terminal_id,e.eport_id) "12 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-13,t.terminal_id,e.eport_id) "13 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-14,t.terminal_id,e.eport_id) "14 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-15,t.terminal_id,e.eport_id) "15 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-16,t.terminal_id,e.eport_id) "16 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-17,t.terminal_id,e.eport_id) "17 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-18,t.terminal_id,e.eport_id) "18 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-19,t.terminal_id,e.eport_id) "19 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-20,t.terminal_id,e.eport_id) "20 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-21,t.terminal_id,e.eport_id) "21 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-22,t.terminal_id,e.eport_id) "22 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-23,t.terminal_id,e.eport_id) "23 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-24,t.terminal_id,e.eport_id) "24 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-25,t.terminal_id,e.eport_id) "25 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-26,t.terminal_id,e.eport_id) "26 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-27,t.terminal_id,e.eport_id) "27 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-28,t.terminal_id,e.eport_id) "28 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-29,t.terminal_id,e.eport_id) "29 Days Ago Cashless Amount",
	REPORT.GET_CASHLESS(sysdate-30,t.terminal_id,e.eport_id) "30 Days Ago Cashless Amount"
	FROM CORP.CUSTOMER c
	    JOIN REPORT.TERMINAL t ON t.CUSTOMER_ID = c.CUSTOMER_ID AND t.STATUS != ''D''
	    JOIN REPORT.TERMINAL_EPORT te ON t.TERMINAL_ID = te.TERMINAL_ID
	    JOIN REPORT.EPORT e ON e.EPORT_ID = te.EPORT_ID
	    JOIN REPORT.LOCATION l ON t.LOCATION_ID = l.LOCATION_ID
	    JOIN REPORT.VW_USER_TERMINAL vut ON t.TERMINAL_ID = vut.TERMINAL_ID AND vut.TERMINAL_ID != 0
	    JOIN REPORT.USER_LOGIN ul ON ul.USER_ID = vut.USER_ID
	    LEFT OUTER JOIN (REPORT.TERMINAL_REGION tr
	    JOIN REPORT.REGION r ON tr.REGION_ID = r.REGION_ID) ON t.TERMINAL_ID = tr.TERMINAL_ID
	WHERE te.END_DATE IS NULL
	  AND vut.USER_ID =?';
  select NVL(MAX(DECODE(substr(subdomain_url, length('hotchoice') + 1, length(subdomain_url) - length('hotchoice.usatech.com')), '-ecc', 'ECC', '-dev', 'DEV', '-int', 'INT', null, 'PRD')), 'DEV')
  into l_env
  from WEB_CONTENT.subdomain where subdomain_url like 'hotchoice%.usatech.com'
  and subdomain_url not IN('hotchoice-PRD.usatech.com','hotchoice-USA.usatech.com') ;
  
  IF l_env = 'PRD' THEN
    insert into web_content.web_link values(web_content.seq_web_link_id.nextval, 'Device Health with Cashless Sales', './run_device_health_with_cashless_sales.i?basicReportId=453','Device Health with Cashless Sales','Daily Operations','-', 0);
    insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 1);
    FOR l_cur in (select user_id from report.user_login where customer_id=54887 and status='A' ) LOOP
		INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
		VALUES( LN_REPORT_ID, 'Device Health with Cashless Sales', 6, 0, 'Device Health with Cashless Sales', 'Report that shows device last call-in and last 30 days of cashless amount', 'U', l_cur.user_id);
		INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
		VALUES( LN_REPORT_ID, 'query', l_query);
		
		insert into report.report_param (report_id,param_name,param_value) values(LN_REPORT_ID, 'params.userId','{u}');
		insert into report.report_param (report_id,param_name,param_value) values(LN_REPORT_ID, 'paramNames','userId');
		insert into report.report_param (report_id,param_name,param_value) values(LN_REPORT_ID, 'paramTypes','NUMERIC');
		insert into report.report_param (report_id,param_name,param_value) values(LN_REPORT_ID, 'isSQLFolio','true');
		insert into report.report_param (report_id,param_name,param_value) values(LN_REPORT_ID, 'header', 'true');
		insert into report.report_param (report_id,param_name,param_value) values(LN_REPORT_ID, 'outputType', '27');
		LN_REPORT_ID:=LN_REPORT_ID+1;
		insert into report.user_link values(l_cur.user_id,web_content.seq_web_link_id.currval,'Y','W');
	END LOOP;
	
  ELSE
  	select user_id into l_user_id from report.user_login where user_name='aroyce@usatech.com';
  	
  	INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
	VALUES( LN_REPORT_ID, 'Device Health with Cashless Sales', 6, 0, 'Device Health with Cashless Sales', 'Report that shows device last call-in and last 30 days of cashless amount', 'U', l_user_id);
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
	VALUES( LN_REPORT_ID, 'query', l_query);
	
	insert into report.report_param (report_id,param_name,param_value) values(LN_REPORT_ID, 'params.userId','{u}');
	insert into report.report_param (report_id,param_name,param_value) values(LN_REPORT_ID, 'paramNames','userId');
	insert into report.report_param (report_id,param_name,param_value) values(LN_REPORT_ID, 'paramTypes','NUMERIC');
	insert into report.report_param (report_id,param_name,param_value) values(LN_REPORT_ID, 'isSQLFolio','true');
	insert into report.report_param(report_id, param_name, param_value) values(LN_REPORT_ID, 'header', 'true');
	insert into report.report_param(report_id, param_name, param_value) values(LN_REPORT_ID, 'outputType', '27');
	
	insert into web_content.web_link values(web_content.seq_web_link_id.nextval, 'Device Health with Cashless Sales', './run_device_health_with_cashless_sales.i?basicReportId='||LN_REPORT_ID,'Device Health With Cashless Sales','Daily Operations','-', 0);
	insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 1);
	insert into report.user_link values(l_user_id,web_content.seq_web_link_id.currval,'Y','W');
  END IF;
		
	
	commit;
END;
/