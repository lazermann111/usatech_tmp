-- This SQL script grants access to two stored procedures
-- required by the Transaction Details report in USALive.

grant execute on report.GET_TRANS_DETAILS to USALIVE_APP_ROLE;
grant execute on report.MASK_CARD to USALIVE_APP_ROLE;
