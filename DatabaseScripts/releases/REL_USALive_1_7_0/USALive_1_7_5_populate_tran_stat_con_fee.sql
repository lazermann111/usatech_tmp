DECLARE
  ld_min_date DATE := TO_DATE('11/14/2011 00:00:00', 'MM/DD/YYYY HH24:MI:SS');
  ld_max_date DATE := sysdate+1;
BEGIN
    WHILE ld_min_date < ld_max_date LOOP
      update report.trans_stat_by_day t set convenience_fee=
      (select sum(a.convenience_fee) convenience_fee 
      from report.activity_ref a  
      where a.convenience_fee >0 and t.terminal_id=a.terminal_id and t.eport_id=a.eport_id and t.tran_date=trunc(a.tran_date, 'DD') and t.trans_type_id=a.trans_type_id
      and a.tran_date >= ld_min_date and a.tran_date < ld_min_date + 1)
	  where t.tran_date >= ld_min_date and t.tran_date < ld_min_date + 1;
      commit;
      ld_min_date:=ld_min_date+1;
    END LOOP;
END;
