-- This SQL script grants access to the new customer management package
-- required by USA Live 1.7

GRANT EXECUTE ON REPORT.PKG_CUSTOMER_MANAGEMENT TO USALIVE_APP_ROLE;
GRANT EXECUTE ON REPORT.PKG_APP_USER TO USALIVE_APP_ROLE;