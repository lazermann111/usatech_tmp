CREATE ROLE USAT_RPT_GEN_ROLE;

GRANT select on usat_custom.file_trnsfr_config to USAT_RPT_GEN_ROLE;
GRANT insert on usat_custom.file_content to USAT_RPT_GEN_ROLE;

GRANT USAT_RPT_GEN_ROLE TO RPT_GEN_APP;

-- No CONNECT grant