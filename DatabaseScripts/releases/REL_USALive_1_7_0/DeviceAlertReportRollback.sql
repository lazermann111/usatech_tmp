-- New report 'Device Alerts'

set serveroutput on format wrapped;

delete from report.reports where report_name = 'Device Alert' OR REPORT_NAME = 'Device Alerts';
delete from REPORT.GENERATOR where generator_id = 9;
DELETE FROM REPORT.EXPORT_TYPE WHERE EXPORT_TYPE_ID = 6;

REVOKE EXECUTE ON REPORT.CCS_PKG FROM USAT_APP_LAYER_ROLE;