INSERT INTO REPORT.GENERATOR (GENERATOR_ID, NAME, CMDLINE, CLASSNAME) values ( 8, 'RDC (BestVendor)', null, null );

     INSERT
INTO REPORT.EXPORT_TYPE
  (
    EXPORT_TYPE_ID,
    NAME,
    DESCRIPTION,
    STATUS,
    CREATE_DATE,
    UPD_DATE,
    UPD_BY
  )
  VALUES
  (
    5,
    'RDC',
    'Best Vendor RDC Batch',
    'A',
    SYSDATE,
    SYSDATE,
    'phorsfield'
  );

COMMIT;

declare
 newval NUMBER;
 dropcheck NUMBER;
 tgtschema VARCHAR(20) := 'REPORT';
 seqname VARCHAR(20) := 'REPORTS_SEQ';
 srcmaxtable VARCHAR(40) := 'REPORT.REPORTS';
 srcmaxcol VARCHAR(40) := 'REPORT_ID';
 newseq VARCHAR(150);
BEGIN 
  execute immediate 'select MAX(' || srcmaxcol || ')+1 FROM ' || srcmaxtable into newval;

  newseq := 'CREATE SEQUENCE ' || tgtschema || '.' || seqname || ' MINVALUE 0 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH ' || newval || ' NOCACHE NOORDER NOCYCLE';

  DBMS_OUTPUT.PUT_LINE(newseq);

  select count(*) into dropcheck
  from dba_objects
  where object_type = 'SEQUENCE'
  and object_name = seqname
  and owner = tgtschema;

  if dropcheck > 0 THEN 
    DBMS_OUTPUT.PUT_LINE('Dropping existing sequence');
    execute immediate 'drop sequence ' || tgtschema || '.' || seqname ;
  END If;
 
  DBMS_OUTPUT.PUT_LINE('Creating new sequence starting at ' || newval);
  execute immediate newseq;
  DBMS_OUTPUT.PUT_LINE('Completed');
  DBMS_OUTPUT.NEW_LINE();
END; 
/

     INSERT
INTO REPORT.REPORTS
  (
    REPORT_ID,
    TITLE,
    GENERATOR_ID,
    UPD_DT,
    BATCH_TYPE_ID,
    REPORT_NAME,
    DESCRIPTION,
    USAGE,
    USER_ID,
    CATEGORY
  )
  VALUES
  (
    REPORT.REPORTS_SEQ.NEXTVAL,
    'rdc_{x}.dex',
    8,
    SYSDATE,
    5,
    'RDC Report',
    'Best Vendor Fusion RDC Individual DEX Report Transmission',
    'D',
    0,
    ''
  );

INSERT
INTO REPORT.CCS_TRANSPORT_TYPE
  (
    CCS_TRANSPORT_TYPE_ID,
    CCS_TRANSPORT_TYPE_NAME,
    CCS_TRANSPORT_TYPE_STATUS,
    DISPLAY_ORDER
  )
  VALUES
  (
    8,
    'BV Fusion RDC SOAP',
    'I',
    10
  );

INSERT
INTO REPORT.CCS_TRANSPORT
  (
    CCS_TRANSPORT_ID,
    CCS_TRANSPORT_TYPE_ID,
    CCS_TRANSPORT_NAME,
    CREATED_BY,
    CREATED_TS,
    LAST_UPDATED_BY,
    LAST_UPDATED_TS,
    CCS_TRANSPORT_STATUS_CD
  )
  VALUES
  (
    REPORT.CCS_TRANSPORT_SEQ.NEXTVAL,
    8,
    'Best Vendor RDC Test Transport',
    'phorsfield',
    SYSDATE,
    'phorsfield',
    SYSDATE,
    'N'
  );

COMMIT;
-- Add property type and property for the above transport

declare
 newval NUMBER;
 dropcheck NUMBER;
 tgtschema VARCHAR(20) := 'REPORT';
 seqname VARCHAR(20) := 'CCS_TPT_SEQ';
 srcmaxtable VARCHAR(40) := 'REPORT.CCS_TRANSPORT_PROPERTY_TYPE';
 srcmaxcol VARCHAR(40) := 'CCS_TRANSPORT_PROPERTY_TYPE_ID';
 newseq VARCHAR(150);
BEGIN 
  execute immediate 'select MAX(' || srcmaxcol || ')+1 FROM ' || srcmaxtable into newval;

  newseq := 'CREATE SEQUENCE ' || tgtschema || '.' || seqname || ' MINVALUE 0 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH ' || newval || ' NOCACHE NOORDER NOCYCLE';

  DBMS_OUTPUT.PUT_LINE(newseq);

  select count(*) into dropcheck
  from dba_objects
  where object_type = 'SEQUENCE'
  and object_name = seqname
  and owner = tgtschema;

  if dropcheck > 0 THEN 
    DBMS_OUTPUT.PUT_LINE('Dropping existing sequence');
    execute immediate 'drop sequence ' || tgtschema || '.' || seqname ;
  END If;
 
  DBMS_OUTPUT.PUT_LINE('Creating new sequence starting at ' || newval);
  execute immediate newseq;
  DBMS_OUTPUT.PUT_LINE('Completed');
  DBMS_OUTPUT.NEW_LINE();
END; 
/
insert into report.ccs_transport_property_type 
  (CCS_TRANSPORT_PROPERTY_TYPE_ID, CCS_transport_type_id, ccs_tpt_name, ccs_tpt_required_flag) 
  values (REPORT.CCS_TPT_SEQ.NEXTVAL, 8, 'URL', 'Y');

INSERT
INTO REPORT.CCS_TRANSPORT_PROPERTY
  (
    CCS_TRANSPORT_PROPERTY_ID,
    CCS_TRANSPORT_ID,
    CCS_TRANSPORT_PROPERTY_TYPE_ID,
    CCS_TRANSPORT_PROPERTY_VALUE,
    CREATED_BY,
    CREATED_TS,
    LAST_UPDATED_BY,
    LAST_UPDATED_TS
  )
  select 
    REPORT.CCS_TRANSPORT_PROPERTY_SEQ.NEXTVAL,
    t.ccs_transport_id,
    tpt.ccs_transport_property_type_id,
    'http://localhost:8080/axis/services/wsMainSoap',
    'phorsfield',
    SYSDATE,
    'phorsfield',
    SYSDATE
    from report.ccs_transport_property_type tpt, report.ccs_transport t
    where tpt.ccs_tpt_name = 'URL' and tpt.ccs_transport_type_id = '8'
    and t.ccs_transport_name = 'Best Vendor RDC Test Transport'
  ;
  
-- Official URL : http://208.82.3.241:85/ws_RDC.asmx 
-- Debug URL : http://localhost:8080/axis/services/wsMainSoap

-- Report generation parameters, copy from DEX report

insert into report.report_param 
			(report_id, param_name, param_value) 
	select 	r.report_id, rp.param_name, rp.param_value 
	from 	report.reports r, REPORT.report_param rp 
	where 	r.report_name = 'RDC Report' 
		and rp.REPORT_ID = 1;

-- If we move to a bulk RDC messaging system, then we need to establish a timed report
-- Timed reports have the frequency field set. Batch reports have it set to null
-- For now, we are using pre-created batch reports where each batch is one dexId.

-- Remove timed reports for RDC previously added:
delete from REPORT.user_report r where r.frequency_id is not null and exists (select c.ccs_transport_id from report.ccs_transport c where c.ccs_transport_type_id = 8 and r.ccs_transport_id = c.ccs_transport_id );

commit;
