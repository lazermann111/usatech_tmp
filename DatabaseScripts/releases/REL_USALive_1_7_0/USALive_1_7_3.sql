GRANT SELECT ON CORP.CUSTOMER_BANK TO USAT_RPT_REQ_ROLE;
GRANT SELECT ON REPORT.USER_PRIVS TO USAT_RPT_REQ_ROLE;
GRANT SELECT ON REPORT.USER_CUSTOMER_BANK TO USAT_RPT_REQ_ROLE;
GRANT SELECT ON REPORT.USER_TERMINAL TO USAT_RPT_REQ_ROLE;

GRANT UPDATE ON G4OP.DEX_FILE TO USAT_RPT_REQ_ROLE;
GRANT UPDATE ON CORP.DOC TO USAT_RPT_REQ_ROLE;
GRANT UPDATE ON REPORT.EXPORT_BATCH TO USAT_RPT_REQ_ROLE;

UPDATE CORP.FREQUENCY SET INTERVAL = 'DY', DAYS = 7
WHERE DATE_FIELD = 'WEEK' AND INTERVAL  = ' ';
COMMIT;

ALTER TABLE CORP.FREQUENCY MODIFY (DAYS NUMBER(38,23));
UPDATE CORP.FREQUENCY SET DAYS = (1/24)
WHERE DATE_FIELD = 'HOUR' AND INTERVAL  = 'HH';
COMMIT;
  
UPDATE CORP.DOC SET STATUS = 'S' WHERE STATUS = 'P' AND SENT_DATE < SYSDATE - 14;
COMMIT;

DECLARE
CURSOR l_cur(ln_min_id NUMBER, ln_max_id NUMBER) IS
    SELECT D.DEX_FILE_ID, TE.TERMINAL_ID, D.DEX_DATE, D.CREATE_DT 
          FROM G4OP.DEX_FILE D
          JOIN REPORT.TERMINAL_EPORT TE ON D.EPORT_ID = TE.EPORT_ID
           AND D.DEX_DATE >= NVL(TE.START_DATE, MIN_DATE) AND D.DEX_DATE < NVL(TE.END_DATE, MAX_DATE)                   
         WHERE D.SENT = 'N'
           AND D.DEX_FILE_ID BETWEEN ln_min_id AND ln_max_id
          ORDER BY D.DEX_FILE_ID;
ln_start_id NUMBER;
ln_end_id NUMBER;
ln_interval PLS_INTEGER := 100000;
ln_cnt PLS_INTEGER;
ln_updated NUMBER := 0;
BEGIN
    SELECT MIN(DEX_FILE_ID), MAX(DEX_FILE_ID)
      INTO ln_start_id, ln_end_id
      FROM G4OP.DEX_FILE;
      
    WHILE ln_start_id < ln_end_id LOOP
        DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'MM/DD/YYYY HH24:MI:SS') || ' Processing DEX_FILE_IDs between ' || ln_start_id || ' and ' || (ln_start_id + ln_interval));
        ln_updated := 0;
        FOR l_rec in l_cur(ln_start_id, ln_start_id + ln_interval - 1) LOOP
            IF l_rec.CREATE_DT < SYSDATE - 10 THEN
                ln_cnt := 0;
            ELSE
                SELECT COUNT(*)
                  INTO ln_cnt
                  FROM (SELECT DISTINCT UR.USER_REPORT_ID, UR.USER_ID, UR.UPD_DT,
                           UR.REPORT_ID, R.GENERATOR_ID, R.TITLE, R.REPORT_NAME
                      FROM REPORT.REPORTS R
                      JOIN REPORT.USER_REPORT UR ON R.REPORT_ID = UR.REPORT_ID
                      JOIN REPORT.CCS_TRANSPORT T ON T.CCS_TRANSPORT_ID = UR.CCS_TRANSPORT_ID AND T.CCS_TRANSPORT_STATUS_CD != 'E'
                      JOIN REPORT.USER_LOGIN U ON UR.USER_ID = U.USER_ID
                      JOIN REPORT.VW_USER_TERMINAL UT ON U.USER_ID = UT.USER_ID
                     WHERE UR.STATUS = 'A' 
                       AND U.STATUS = 'A'
                       AND R.BATCH_TYPE_ID = 2
                       AND l_rec.DEX_DATE > UR.UPD_DT  
                       AND UT.TERMINAL_ID = l_rec.TERMINAL_ID) RPT
                   LEFT OUTER JOIN REPORT.REPORT_SENT RS ON RS.BATCH_ID = l_rec.DEX_FILE_ID
                   AND RS.USER_REPORT_ID = RPT.USER_REPORT_ID
                   AND RS.REPORT_SENT_STATE_ID != 4
                  WHERE RS.REPORT_SENT_ID IS NULL;
            END IF;
            IF ln_cnt = 0 THEN
                UPDATE G4OP.DEX_FILE
                   SET SENT = 'Y'
                 WHERE DEX_FILE_ID = l_rec.DEX_FILE_ID
                   AND SENT = 'N';
                ln_updated := ln_updated + SQL%ROWCOUNT;
                COMMIT;
            END IF;
        END LOOP;
        DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'MM/DD/YYYY HH24:MI:SS') || ' Updated ' || ln_updated || ' DEX_FILE_IDs between ' || ln_start_id || ' and ' || (ln_start_id + ln_interval));
        ln_start_id := ln_start_id + ln_interval;
    END LOOP;
END;
/

DECLARE
CURSOR l_cur(ln_min_id NUMBER, ln_max_id NUMBER) IS
    SELECT EB.BATCH_ID, EB.CUSTOMER_ID, EB.CRD_DATE 
          FROM REPORT.EXPORT_BATCH EB
         WHERE EB.EXPORT_TYPE = 1
           AND EB.STATUS = 'A'
           AND EB.BATCH_ID BETWEEN ln_min_id AND ln_max_id
          ORDER BY EB.BATCH_ID;
ln_start_id NUMBER;
ln_end_id NUMBER;
ln_interval PLS_INTEGER := 100000;
ln_cnt PLS_INTEGER;
ln_updated NUMBER := 0;
BEGIN
    SELECT MIN(BATCH_ID), MAX(BATCH_ID)
      INTO ln_start_id, ln_end_id
      FROM REPORT.EXPORT_BATCH;
      
    WHILE ln_start_id < ln_end_id LOOP
        DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'MM/DD/YYYY HH24:MI:SS') || ' Processing EXPORT_BATCH_IDs between ' || ln_start_id || ' and ' || (ln_start_id + ln_interval));
        ln_updated := 0;
        FOR l_rec in l_cur(ln_start_id, ln_start_id + ln_interval - 1) LOOP
            IF l_rec.CRD_DATE < SYSDATE - 10 THEN
                ln_cnt := 0;
            ELSE
                SELECT COUNT(*)
                  INTO ln_cnt
                  FROM (
                    SELECT UR.USER_REPORT_ID, UR.USER_ID, UR.UPD_DT,
                           UR.REPORT_ID, R.GENERATOR_ID, R.TITLE, R.REPORT_NAME
                      FROM REPORT.REPORTS R
                      JOIN REPORT.USER_REPORT UR ON R.REPORT_ID = UR.REPORT_ID
                      JOIN REPORT.CCS_TRANSPORT T ON T.CCS_TRANSPORT_ID = UR.CCS_TRANSPORT_ID AND T.CCS_TRANSPORT_STATUS_CD != 'E'
                      JOIN REPORT.USER_LOGIN U ON UR.USER_ID = U.USER_ID
                      JOIN REPORT.USER_PRIVS UP ON U.USER_ID = UP.USER_ID AND UP.PRIV_ID IN(7, 6)
                     WHERE UR.STATUS = 'A' 
                       AND U.STATUS = 'A'
                       AND R.BATCH_TYPE_ID = 1
                       AND l_rec.CRD_DATE > UR.UPD_DT
                       AND (U.CUSTOMER_ID = l_rec.CUSTOMER_ID OR (U.CUSTOMER_ID = 0 AND U.USER_TYPE != 8))
                    UNION 
                    SELECT UR.USER_REPORT_ID, UR.USER_ID, UR.UPD_DT,
                           UR.REPORT_ID, R.GENERATOR_ID, R.TITLE, R.REPORT_NAME
                      FROM REPORT.REPORTS R
                      JOIN REPORT.USER_REPORT UR ON R.REPORT_ID = UR.REPORT_ID
                      JOIN REPORT.CCS_TRANSPORT T ON T.CCS_TRANSPORT_ID = UR.CCS_TRANSPORT_ID AND T.CCS_TRANSPORT_STATUS_CD != 'E'
                      JOIN REPORT.USER_LOGIN U ON UR.USER_ID = U.USER_ID
                      JOIN REPORT.USER_TERMINAL UT ON U.USER_ID = UT.USER_ID
                      JOIN REPORT.TERMINAL T ON UT.TERMINAL_ID = T.TERMINAL_ID
                     WHERE UR.STATUS = 'A' 
                       AND U.STATUS = 'A'
                       AND T.STATUS != 'D'
                       AND R.BATCH_TYPE_ID = 1
                       AND l_rec.CRD_DATE > UR.UPD_DT  
                       AND T.CUSTOMER_ID = l_rec.CUSTOMER_ID) RPT
                   LEFT OUTER JOIN REPORT.REPORT_SENT RS ON RS.BATCH_ID = l_rec.BATCH_ID
                   AND RS.USER_REPORT_ID = RPT.USER_REPORT_ID
                   AND RS.REPORT_SENT_STATE_ID != 4
                  WHERE RS.REPORT_SENT_ID IS NULL;
            END IF;
            IF ln_cnt = 0 THEN
                UPDATE REPORT.EXPORT_BATCH
                   SET STATUS = 'S',
                       EXPORT_SENT = SYSDATE
                 WHERE BATCH_ID = l_rec.BATCH_ID
                   AND STATUS = 'A';
                ln_updated := ln_updated + SQL%ROWCOUNT;
                COMMIT;
            END IF;
        END LOOP;
        DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'MM/DD/YYYY HH24:MI:SS') || ' Updated ' || ln_updated || ' EXPORT_BATCH_IDs between ' || ln_start_id || ' and ' || (ln_start_id + ln_interval));
        ln_start_id := ln_start_id + ln_interval;
    END LOOP;
END;
/

MERGE INTO CORP.APP_SETTING T
USING (SELECT a.COLUMN_VALUE || '_REPORT_REQUEST' || b.SUFFIX APP_SETTING_CD, REPLACE(b.SETTING_DESC, '<TYPE>', INITCAP(REPLACE(a.COLUMN_VALUE, '_', ' '))) APP_SETTING_DESC, b.SETTING_VALUE APP_SETTING_VALUE
    FROM TABLE(VARCHAR2_TABLE(
    'RESEND', 'DAILY_EXPORT', 'DEX', 'PAYMENT', 'TIMED', 'PENDING_PAYMENT')) a, 
    (SELECT '_LOCK' SUFFIX, NULL SETTING_VALUE, 'Record for locking <TYPE> Report Request' SETTING_DESC FROM DUAL
    UNION ALL SELECT '_MAX_DURATION_SEC', '120', 'Number of seconds for generating the <TYPE> Report Request by report requester.' FROM DUAL) b) S
ON (T.APP_SETTING_CD = S.APP_SETTING_CD)
WHEN NOT MATCHED THEN INSERT(APP_SETTING_CD, APP_SETTING_DESC, APP_SETTING_VALUE)
    VALUES(S.APP_SETTING_CD, S.APP_SETTING_DESC, S.APP_SETTING_VALUE);

COMMIT;
