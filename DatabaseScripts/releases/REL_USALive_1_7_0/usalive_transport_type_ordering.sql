alter table REPORT.CCS_TRANSPORT_TYPE add ( CCS_TRANSPORT_TYPE_STATUS CHAR(1) );
alter table REPORT.CCS_TRANSPORT_TYPE add ( DISPLAY_ORDER number(3,0) );
update REPORT.CCS_TRANSPORT_TYPE T set DISPLAY_ORDER = ( 
  select DISPLAY_ORDER from ( 
    SELECT ROWNUM AS DISPLAY_ORDER,CCS_TRANSPORT_TYPE_ID FROM (
      select CCS_TRANSPORT_TYPE_ID from REPORT.CCS_TRANSPORT_TYPE W order by UPPER(W.CCS_TRANSPORT_TYPE_NAME)
    ) 
  ) P where P.CCS_TRANSPORT_TYPE_ID = T.CCS_TRANSPORT_TYPE_ID
);
update REPORT.CCS_TRANSPORT_TYPE set CCS_TRANSPORT_TYPE_STATUS = 'A';
update REPORT.CCS_TRANSPORT_TYPE set CCS_TRANSPORT_TYPE_STATUS = 'I' where CCS_TRANSPORT_TYPE_NAME = 'BV Fusion RDC SOAP';
commit;
alter table REPORT.CCS_TRANSPORT_TYPE modify ( CCS_TRANSPORT_TYPE_STATUS CHAR(1) not null );
alter table REPORT.CCS_TRANSPORT_TYPE modify ( DISPLAY_ORDER number(3,0) not null );