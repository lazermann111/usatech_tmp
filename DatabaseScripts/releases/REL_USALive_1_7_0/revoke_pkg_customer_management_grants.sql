-- This SQL script revokes access to the new customer management package
-- which was added in USA Live 1.7

revoke execute on report.PKG_CUSTOMER_MANAGEMENT from USALIVE_APP_ROLE;
