DECLARE
    l_host_subdomain_id NUMBER;
    l_subdomain_id NUMBER;
    l_literal_id NUMBER;
    l_env VARCHAR2(3); 
	l_hotchoice_ip VARCHAR2(50) ;
	l_verizon_ip VARCHAR2(50) ;
	l_subdomain_desc VARCHAR2(100) ;
BEGIN
	
  select DECODE(substr(subdomain_url, length('hotchoice') + 1, length(subdomain_url) - length('hotchoice.usatech.com')), '-ecc', 'ECC', '-dev', 'DEV', '-int', 'INT',null, 'PRD'),
  subdomain_id, subdomain_description into l_env, l_host_subdomain_id, l_subdomain_desc
  from WEB_CONTENT.subdomain where subdomain_url like 'hotchoice%.usatech.com'
  and subdomain_url not IN('hotchoice-PRD.usatech.com','hotchoice-USA.usatech.com') ;
  
  IF l_env = 'INT' THEN
  	l_hotchoice_ip:='10.0.0.243';
  	l_verizon_ip:='10.0.0.30';
  ELSIF l_env = 'ECC' THEN
  	l_hotchoice_ip:='192.168.4.102';
  	l_verizon_ip:='192.168.4.104';
  ELSIF l_env = 'PRD' THEN
  	l_hotchoice_ip:='192.168.79.199';
  	l_verizon_ip:='192.168.79.200';
  ELSE
  	-- dev
  	l_hotchoice_ip:='10.0.0.32';
  	l_verizon_ip:='10.0.0.29';
  END IF;
	-- hotchoice site
  select web_content.seq_subdomain_id.nextval into l_subdomain_id from dual;
  
  insert into WEB_CONTENT.subdomain (subdomain_id, subdomain_url, subdomain_description) values(l_subdomain_id, l_hotchoice_ip, l_subdomain_desc);

  insert into WEB_CONTENT.literal (literal_id, subdomain_id, literal_key, literal_value) 
  select web_content.seq_literal_id.nextval, l_subdomain_id, literal_key, literal_value from WEB_CONTENT.literal where subdomain_id=l_host_subdomain_id;
  
  -- verizon site
  
  select  subdomain_id, subdomain_description into l_host_subdomain_id, l_subdomain_desc
  from WEB_CONTENT.subdomain where subdomain_url like 'verizon%.usatech.com'
  and subdomain_url not IN('verizon-PRD.usatech.com','verizon-USA.usatech.com') ;
  
  select web_content.seq_subdomain_id.nextval into l_subdomain_id from dual;
  
  insert into WEB_CONTENT.subdomain (subdomain_id, subdomain_url, subdomain_description) values(l_subdomain_id, l_verizon_ip, l_subdomain_desc);

  insert into WEB_CONTENT.literal (literal_id, subdomain_id, literal_key, literal_value) 
  select web_content.seq_literal_id.nextval, l_subdomain_id, literal_key, literal_value from WEB_CONTENT.literal where subdomain_id=l_host_subdomain_id;

  commit;
END;
