-- Modifies tables like REPORT.ALERT to support device alerts
CREATE TABLE report.alert_source
(
    alert_source_id                         NUMBER                          NOT NULL
  , alert_source_name                       VARCHAR2 (50)                   NOT NULL
  , alert_source_desc                    	VARCHAR2 (256)                  
  , status                          		CHAR     (1)                    DEFAULT 'A'  NOT NULL
  , created_by                      		VARCHAR2 (30)                   DEFAULT USER    NOT NULL
  , created_ts                      		DATE                            DEFAULT SYSDATE    NOT NULL
  , CONSTRAINT PK_ALERT_SOURCE PRIMARY KEY(ALERT_SOURCE_ID)
) 
TABLESPACE 			REPORT_DATA02
;

comment on table report.alert_source IS 'Defines the possible sources of alerts';

GRANT SELECT ON REPORT.ALERT_SOURCE TO USALIVE_APP_ROLE;

-- Matches AlertSourceCode.java
insert into report.alert_source (alert_source_id, alert_source_name, alert_source_desc) VALUES ( 1, 'DEX', 'DEX Alerts' );
insert into report.alert_source (alert_source_id, alert_source_name, alert_source_desc) VALUES ( 2, 'DEVICE', 'Device Events' );

commit;

alter table report.alert add ( 
	alert_source_id NUMBER(3,0),
	event_type_id NUMBER(20,0)
);

comment on column report.alert.alert_source_id IS 'Alert processing subsystem (DEX, DEVICE)';
comment on column report.alert.event_type_id IS 'Nullable, original reported alert type ';

alter table report.alert add constraint FK_ALERT_SOURCE  FOREIGN KEY (ALERT_SOURCE_ID)
        REFERENCES REPORT.ALERT_SOURCE(ALERT_SOURCE_ID);

update report.alert set alert_source_id = 1, event_type_id = alert_id where alert_source_id is null;

commit;

alter table report.alert modify ( alert_source_id NUMBER(3,0) not null );
alter table report.alert modify ( event_type_id NUMBER(20,0) not null );

alter table report.terminal_alert add (
	reference_id NUMBER(20,0),
	component_id NUMBER(20,0),
	notification_date DATE
);

comment on column report.terminal_alert.notification_date IS 'Date at which the the network received the alert.';
comment on column report.terminal_alert.component_id IS 'Nullable, original subcomponent identifier';
comment on column report.terminal_alert.reference_id IS 'Nullable, original reported alert identifier (EVENT_ID, DEX_FILE_ID)';


declare
 newval NUMBER;
 dropcheck NUMBER;
 tgtschema VARCHAR(20) := 'REPORT';
 seqname VARCHAR(20) := 'SEQ_ALERT_ID';
 srcmaxtable VARCHAR(40) := 'REPORT.ALERT';
 srcmaxcol VARCHAR(40) := 'ALERT_ID';
 newseq VARCHAR(150);
BEGIN 
  execute immediate 'select MAX(' || srcmaxcol || ')+1 FROM ' || srcmaxtable into newval;

  newseq := 'CREATE SEQUENCE ' || tgtschema || '.' || seqname || ' MINVALUE 0 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH ' || newval || ' NOCACHE NOORDER NOCYCLE';

  DBMS_OUTPUT.PUT_LINE(newseq);

  select count(*) into dropcheck
  from dba_objects
  where object_type = 'SEQUENCE'
  and object_name = seqname
  and owner = tgtschema;

  if dropcheck > 0 THEN 
    DBMS_OUTPUT.PUT_LINE('Dropping existing sequence');
    execute immediate 'drop sequence ' || tgtschema || '.' || seqname ;
  END If;
 
  DBMS_OUTPUT.PUT_LINE('Creating new sequence starting at ' || newval);
  execute immediate newseq;
  DBMS_OUTPUT.PUT_LINE('Completed');
  DBMS_OUTPUT.NEW_LINE();
END; 
/
insert into REPORT.ALERT (ALERT_ID, ALERT_SOURCE_ID, EVENT_TYPE_ID,ALERT_NAME, DESCRIPTION, STATUS) values ( REPORT.SEQ_ALERT_ID.NEXTVAL, 2, 	1	,'Batch','Scheduled device batch session','I');
insert into REPORT.ALERT (ALERT_ID, ALERT_SOURCE_ID, EVENT_TYPE_ID,ALERT_NAME, DESCRIPTION, STATUS) values ( REPORT.SEQ_ALERT_ID.NEXTVAL, 2, 	2	,'Fill','Products have been replenished','I');
insert into REPORT.ALERT (ALERT_ID, ALERT_SOURCE_ID, EVENT_TYPE_ID,ALERT_NAME, DESCRIPTION, STATUS) values ( REPORT.SEQ_ALERT_ID.NEXTVAL, 2, 	3	,'Scheduled','Scheduled settlement','I');
insert into REPORT.ALERT (ALERT_ID, ALERT_SOURCE_ID, EVENT_TYPE_ID,ALERT_NAME, DESCRIPTION, STATUS) values ( REPORT.SEQ_ALERT_ID.NEXTVAL, 2, 	4	,'Error','Generic error','I');
insert into REPORT.ALERT (ALERT_ID, ALERT_SOURCE_ID, EVENT_TYPE_ID,ALERT_NAME, DESCRIPTION, STATUS) values ( REPORT.SEQ_ALERT_ID.NEXTVAL, 2, 	5	,'Forced','Forced settlement','I');
insert into REPORT.ALERT (ALERT_ID, ALERT_SOURCE_ID, EVENT_TYPE_ID,ALERT_NAME, DESCRIPTION, STATUS) values ( REPORT.SEQ_ALERT_ID.NEXTVAL, 2, 	99	,'Invalid','Events that should be ignored','I');
insert into REPORT.ALERT (ALERT_ID, ALERT_SOURCE_ID, EVENT_TYPE_ID,ALERT_NAME, DESCRIPTION, STATUS) values ( REPORT.SEQ_ALERT_ID.NEXTVAL, 2, 	7	,'Machine Alert','Notifications about conditions on the machine such as column jam or compressor failure or power outage','A');
insert into REPORT.ALERT (ALERT_ID, ALERT_SOURCE_ID, EVENT_TYPE_ID,ALERT_NAME, DESCRIPTION, STATUS) values ( REPORT.SEQ_ALERT_ID.NEXTVAL, 2, 	8	,'Device Alert','Notifications about conditions on the device such as re-boot, or other debug notifications','A');
insert into REPORT.ALERT (ALERT_ID, ALERT_SOURCE_ID, EVENT_TYPE_ID,ALERT_NAME, DESCRIPTION, STATUS) values ( REPORT.SEQ_ALERT_ID.NEXTVAL, 2, 	6	,'Initial','First event after a brand new initialization with no counters','I');
insert into REPORT.ALERT (ALERT_ID, ALERT_SOURCE_ID, EVENT_TYPE_ID,ALERT_NAME, DESCRIPTION, STATUS) values ( REPORT.SEQ_ALERT_ID.NEXTVAL, 2, 	9	,'Coin Changer Alert: bad tube sensor','Device alert generated by MDB','A');
insert into REPORT.ALERT (ALERT_ID, ALERT_SOURCE_ID, EVENT_TYPE_ID,ALERT_NAME, DESCRIPTION, STATUS) values ( REPORT.SEQ_ALERT_ID.NEXTVAL, 2, 	10	,'Coin Changer Alert: changer unplugged','Device alert generated by MDB','A');
insert into REPORT.ALERT (ALERT_ID, ALERT_SOURCE_ID, EVENT_TYPE_ID,ALERT_NAME, DESCRIPTION, STATUS) values ( REPORT.SEQ_ALERT_ID.NEXTVAL, 2, 	11	,'Coin Changer Alert: tube jam','Device alert generated by MDB','A');
insert into REPORT.ALERT (ALERT_ID, ALERT_SOURCE_ID, EVENT_TYPE_ID,ALERT_NAME, DESCRIPTION, STATUS) values ( REPORT.SEQ_ALERT_ID.NEXTVAL, 2, 	12	,'Coin Changer Alert: corrupt ROM','Device alert generated by MDB','A');
insert into REPORT.ALERT (ALERT_ID, ALERT_SOURCE_ID, EVENT_TYPE_ID,ALERT_NAME, DESCRIPTION, STATUS) values ( REPORT.SEQ_ALERT_ID.NEXTVAL, 2, 	13	,'Coin Changer Alert: coin routing error','Device alert generated by MDB','A');
insert into REPORT.ALERT (ALERT_ID, ALERT_SOURCE_ID, EVENT_TYPE_ID,ALERT_NAME, DESCRIPTION, STATUS) values ( REPORT.SEQ_ALERT_ID.NEXTVAL, 2, 	14	,'Coin Changer Alert: coin jam','Device alert generated by MDB','A');
insert into REPORT.ALERT (ALERT_ID, ALERT_SOURCE_ID, EVENT_TYPE_ID,ALERT_NAME, DESCRIPTION, STATUS) values ( REPORT.SEQ_ALERT_ID.NEXTVAL, 2, 	15	,'Coin Changer Alert: credit stolen','Device alert generated by MDB','A');
insert into REPORT.ALERT (ALERT_ID, ALERT_SOURCE_ID, EVENT_TYPE_ID,ALERT_NAME, DESCRIPTION, STATUS) values ( REPORT.SEQ_ALERT_ID.NEXTVAL, 2, 	16	,'Bill Acceptor Alert: bad motor','Device alert generated by MDB','A');
insert into REPORT.ALERT (ALERT_ID, ALERT_SOURCE_ID, EVENT_TYPE_ID,ALERT_NAME, DESCRIPTION, STATUS) values ( REPORT.SEQ_ALERT_ID.NEXTVAL, 2, 	17	,'Bill Acceptor Alert: bad sensor','Device alert generated by MDB','A');
insert into REPORT.ALERT (ALERT_ID, ALERT_SOURCE_ID, EVENT_TYPE_ID,ALERT_NAME, DESCRIPTION, STATUS) values ( REPORT.SEQ_ALERT_ID.NEXTVAL, 2, 	18	,'Bill Acceptor Alert: corrupt ROM','Device alert generated by MDB','A');
insert into REPORT.ALERT (ALERT_ID, ALERT_SOURCE_ID, EVENT_TYPE_ID,ALERT_NAME, DESCRIPTION, STATUS) values ( REPORT.SEQ_ALERT_ID.NEXTVAL, 2, 	19	,'Bill Acceptor Alert: jammed','Device alert generated by MDB','A');
insert into REPORT.ALERT (ALERT_ID, ALERT_SOURCE_ID, EVENT_TYPE_ID,ALERT_NAME, DESCRIPTION, STATUS) values ( REPORT.SEQ_ALERT_ID.NEXTVAL, 2, 	20	,'Bill Acceptor Alert: credit stolen','Device alert generated by MDB','A');
insert into REPORT.ALERT (ALERT_ID, ALERT_SOURCE_ID, EVENT_TYPE_ID,ALERT_NAME, DESCRIPTION, STATUS) values ( REPORT.SEQ_ALERT_ID.NEXTVAL, 2, 	21	,'Bill Acceptor Alert: stacker out of position','Device alert generated by MDB','A');
insert into REPORT.ALERT (ALERT_ID, ALERT_SOURCE_ID, EVENT_TYPE_ID,ALERT_NAME, DESCRIPTION, STATUS) values ( REPORT.SEQ_ALERT_ID.NEXTVAL, 2, 	22	,'Bill Acceptor Alert: disabled','Device alert generated by MDB','A');

commit;
