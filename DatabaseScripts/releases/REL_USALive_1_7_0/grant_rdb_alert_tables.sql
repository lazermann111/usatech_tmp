-- Grant the app layer the ability to insert alerts

GRANT EXECUTE ON REPORT.CCS_PKG TO USAT_APP_LAYER_ROLE;