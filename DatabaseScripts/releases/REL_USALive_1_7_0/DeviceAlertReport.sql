-- New report 'Device Alerts'
INSERT INTO REPORT.EXPORT_TYPE ( EXPORT_TYPE_ID, NAME, DESCRIPTION, STATUS) 
values (6 , 'IMMED', 'No batching, immediate transmission', 'A');

INSERT INTO REPORT.GENERATOR (GENERATOR_ID, NAME, CMDLINE, CLASSNAME) values ( 9, 'Alert', null, null );

declare
 newval NUMBER;
 dropcheck NUMBER;
 tgtschema VARCHAR(20) := 'REPORT';
 seqname VARCHAR(20) := 'REPORTS_SEQ';
 srcmaxtable VARCHAR(40) := 'REPORT.REPORTS';
 srcmaxcol VARCHAR(40) := 'REPORT_ID';
 newseq VARCHAR(150);
BEGIN 
  execute immediate 'select MAX(' || srcmaxcol || ')+1 FROM ' || srcmaxtable into newval;

  newseq := 'CREATE SEQUENCE ' || tgtschema || '.' || seqname || ' MINVALUE 0 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH ' || newval || ' NOCACHE NOORDER NOCYCLE';

  DBMS_OUTPUT.PUT_LINE(newseq);

  select count(*) into dropcheck
  from dba_objects
  where object_type = 'SEQUENCE'
  and object_name = seqname
  and owner = tgtschema;

  if dropcheck > 0 THEN 
    DBMS_OUTPUT.PUT_LINE('Dropping existing sequence');
    execute immediate 'drop sequence ' || tgtschema || '.' || seqname ;
  END If;
 
  DBMS_OUTPUT.PUT_LINE('Creating new sequence starting at ' || newval);
  execute immediate newseq;
  DBMS_OUTPUT.PUT_LINE('Completed');
  DBMS_OUTPUT.NEW_LINE();
END; 
/
INSERT INTO REPORT.REPORTS
  (
    REPORT_ID,
    TITLE,
    GENERATOR_ID,
    UPD_DT,
    BATCH_TYPE_ID,
    REPORT_NAME,
    DESCRIPTION,
    USAGE,
    USER_ID,
    CATEGORY
  )
  VALUES
  (
    REPORT.REPORTS_SEQ.NEXTVAL,
    'Device Alert from {s}',
    9,
    SYSDATE,
    6,
    'Device Alerts',
    'Alerts will be sent upon detection',
    'E',
    0,
    ''
  );
  
UPDATE report.reports set TITLE = 'Device Alert from {s}', BATCH_TYPE_ID = 6, REPORT_NAME = 'Device Alerts' where REPORT_NAME = 'Device Alert' OR REPORT_NAME = 'Device Alerts';

COMMIT;

GRANT EXECUTE ON REPORT.CCS_PKG TO USAT_APP_LAYER_ROLE;


