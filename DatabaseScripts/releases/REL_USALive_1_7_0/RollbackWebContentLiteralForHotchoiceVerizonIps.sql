DECLARE
  	l_subdomain_id NUMBER;
  	l_env VARCHAR2(3); 
    l_hotchoice_ip VARCHAR2(50) ;
	l_verizon_ip VARCHAR2(50) ;
BEGIN
	
  select DECODE(substr(subdomain_url, length('hotchoice') + 1, length(subdomain_url) - length('hotchoice.usatech.com')), '-ecc', 'ECC', '-dev', 'DEV', '-int', 'INT',null, 'PRD')
  into l_env
  from WEB_CONTENT.subdomain where subdomain_url like 'hotchoice%.usatech.com'
  and subdomain_url not IN('hotchoice-PRD.usatech.com','hotchoice-USA.usatech.com') ;
  
  IF l_env = 'INT' THEN
  	l_hotchoice_ip:='10.0.0.243';
  	l_verizon_ip:='10.0.0.30';
  ELSIF l_env = 'ECC' THEN
  	l_hotchoice_ip:='192.168.4.102';
  	l_verizon_ip:='192.168.4.104';
  ELSIF l_env = 'PRD' THEN
  	l_hotchoice_ip:='192.168.79.199';
  	l_verizon_ip:='192.168.79.200';
  ELSE
  	-- dev
  	l_hotchoice_ip:='10.0.0.32';
  	l_verizon_ip:='10.0.0.29';
  END IF;
	-- hotchoice site
  
  select subdomain_id into l_subdomain_id from WEB_CONTENT.subdomain where subdomain_url=l_hotchoice_ip;
  
  delete from WEB_CONTENT.literal where subdomain_id=l_subdomain_id;
  
  delete from WEB_CONTENT.subdomain where subdomain_id=l_subdomain_id;
  

  -- verizon site
  select subdomain_id into l_subdomain_id from WEB_CONTENT.subdomain where subdomain_url=l_verizon_ip;  
  
  delete from WEB_CONTENT.literal where subdomain_id=l_subdomain_id;
  
  delete from WEB_CONTENT.subdomain where subdomain_id=l_subdomain_id;
  
  commit;

END;