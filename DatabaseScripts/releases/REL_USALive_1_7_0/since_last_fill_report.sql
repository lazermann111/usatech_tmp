-- This SQL script sets an appropriate link from the 
-- canned reports pages in USALive.

UPDATE WEB_CONTENT.WEB_LINK 
	SET WEB_LINK_URL = ( 
		SELECT './run_report.i?folioId=' || FOLIO_ID 
		FROM FOLIO_CONF.FOLIO 
		WHERE  FOLIO_NAME = 'Since Last Fill Transaction Summary'
	) WHERE WEB_LINK_LABEL = 'Since Last Fill';
	
commit;