DECLARE
  l_min_tran_id NUMBER;
  l_max_tran_id NUMBER;
BEGIN
	select min(tran_id), max(tran_id) into l_min_tran_id, l_max_tran_id from report.purchase where vend_column='Convenience Fee';
    while l_min_tran_id <= l_max_tran_id loop
    	update report.activity_ref af set convenience_fee=(
			select sum(NVL(p.AMOUNT * NVL(p.PRICE, 0), 0))
			from report.purchase p where p.vend_column='Convenience Fee' and af.tran_id = p.tran_id)
		where exists (
    		select p.tran_id
			from report.purchase p where p.vend_column='Convenience Fee' and af.tran_id = p.tran_id)
    	and tran_id between l_min_tran_id and l_min_tran_id+1000-1;
		commit;
		l_min_tran_id:=l_min_tran_id+1000;
	end loop;
END;