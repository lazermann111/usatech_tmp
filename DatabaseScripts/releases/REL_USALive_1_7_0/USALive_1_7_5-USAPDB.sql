WHENEVER SQLERROR EXIT FAILURE COMMIT;

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/releases/REL_USALive_1_7_0/USALive_1_7_5.sql?rev=HEAD
GRANT UPDATE ON REPORT.REPORT_REQUEST TO USAT_RPT_REQ_ROLE;

MERGE INTO REPORT.REPORT_REQUEST_STATUS T
USING (SELECT 4 REPORT_REQUEST_STATUS_ID, 'New' DESCRIPTION FROM DUAL
   UNION ALL SELECT 0, 'Requested' FROM DUAL) S
ON (T.REPORT_REQUEST_STATUS_ID = S.REPORT_REQUEST_STATUS_ID)
WHEN NOT MATCHED THEN INSERT(REPORT_REQUEST_STATUS_ID, DESCRIPTION)
    VALUES(S.REPORT_REQUEST_STATUS_ID, S.DESCRIPTION)
WHEN MATCHED THEN UPDATE SET T.DESCRIPTION = S.DESCRIPTION
   WHERE T.DESCRIPTION != S.DESCRIPTION;

UPDATE FOLIO_CONF.FILTER_PARAM SET PARAM_EDITOR = 'TEXT:0-'
 WHERE FILTER_ID IN(SELECT F.FILTER_ID 
            FROM FOLIO_CONF.FILTER F 
            JOIN FOLIO_CONF.VW_FILTER_GROUP_HIERARCHY H ON F.FILTER_GROUP_ID = H.DESCENDENT_FILTER_GROUP_ID
            JOIN FOLIO_CONF.FOLIO FO ON FO.FILTER_GROUP_ID = H.ANCESTOR_FILTER_GROUP_ID
           WHERE FO.FOLIO_ID = 493)
   AND PARAM_INDEX = 1;
   
COMMIT;

-- Payment Details Report(Payment Batches) folio 260 uses ACTIVITY_REF.CONVENIENCE_FEE
ALTER TABLE REPORT.ACTIVITY_REF
ADD CONVENIENCE_FEE NUMBER(15,2);

-- Sales Rollup Report folio 969 uses TRANS_STAT_BY_DAY.CONVENIENCE_FEE
ALTER TABLE REPORT.TRANS_STAT_BY_DAY
ADD CONVENIENCE_FEE NUMBER(15,2);
-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/CORP/PAYMENTS_PKG.pbk?rev=1.76
CREATE OR REPLACE PACKAGE BODY CORP.PAYMENTS_PKG IS
--
-- Holds all the procedures and functions related to EFTs and payments
--
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------
-- B Krug       09-16-04  NEW
-- B Krug       09-16-04  Moved CREATE_PAYMENT_FOR_ACCOUNT into this package to
--                        take advantage of other procs in this package
-- B Krug       10-05-04  Added Ledger sync procs (and batch-related stuff)
-- B Krug       01-31-08  Added Batch Confirmation Logic

    -- Returns 'Y' if an entry is payable (it's been settled)
    -- Returns 'N' if an entry is not payable
    -- Returns '?' if an entry has not been processed
    FUNCTION ENTRY_PAYABLE(
        l_settle_state LEDGER.SETTLE_STATE_ID%TYPE,
        l_entry_type LEDGER.ENTRY_TYPE%TYPE)
     RETURN CHAR
    IS
    BEGIN
        IF l_settle_state IN(2,3,6) THEN
            RETURN 'Y';
        /*ELSIF l_entry_type IN('CB','RF','SF') THEN
            RETURN 'Y';
        */ELSIF l_settle_state IN(5) THEN
            RETURN 'N';
        ELSE
            RETURN '?';
        END IF;
    END;
    
    -- Gets the batch_id or creates one if necessary
    FUNCTION GET_OR_CREATE_DOC(
        l_cust_bank_id DOC.CUSTOMER_BANK_ID%TYPE,
        l_currency_id DOC.CURRENCY_ID%TYPE,
        l_business_unit DOC.BUSINESS_UNIT_ID%TYPE
     )
     RETURN DOC.DOC_ID%TYPE
    IS
        l_doc_id DOC.DOC_ID%TYPE;
        l_batch_ref DOC.REF_NBR%TYPE;
        l_lock VARCHAR2(128);
    BEGIN
        -- lock this bank id until commit to ensure the the doc stays open until the ledger entry is set with it
        l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_cust_bank_id);
        SELECT MAX(D.DOC_ID) -- just to be safe in case there is more than one
          INTO l_doc_id
          FROM DOC D
         WHERE D.CUSTOMER_BANK_ID = l_cust_bank_id
           AND NVL(D.CURRENCY_ID, 0) = NVL(l_currency_id, 0)
           AND NVL(D.BUSINESS_UNIT_ID, 0) = NVL(l_business_unit, 0)
           AND D.STATUS = 'O';
        IF l_doc_id IS NULL THEN
            -- create new doc record
            SELECT EFT_SEQ.NEXTVAL, TO_CHAR(EFT_BATCH_SEQ.NEXTVAL, 'FM0000999999')
              INTO l_doc_id, l_batch_ref
              FROM DUAL;
            INSERT INTO DOC(
                DOC_ID,
                DOC_TYPE,
                REF_NBR,
                DESCRIPTION,
                CUSTOMER_BANK_ID,
                CURRENCY_ID,
                BUSINESS_UNIT_ID,
                BANK_ACCT_NBR,
                BANK_ROUTING_NBR,
                STATUS)
              SELECT
                l_doc_id,
                '--',
                l_batch_ref,
                NVL(EFT_PREFIX, 'USAT: ') || l_batch_ref,
                CUSTOMER_BANK_ID,
                DECODE(l_currency_id, 0, NULL, l_currency_id),
                DECODE(l_business_unit, 0, NULL, l_business_unit),
                BANK_ACCT_NBR,
                BANK_ROUTING_NBR,
                'O'
              FROM CUSTOMER_BANK
              WHERE CUSTOMER_BANK_ID = l_cust_bank_id;
        END IF;
        RETURN l_doc_id;
    END;

    -- Gets the batch_id or creates one if necessary
    FUNCTION GET_OR_CREATE_BATCH(
        l_terminal_id  BATCH.TERMINAL_ID%TYPE,
        l_cust_bank_id DOC.CUSTOMER_BANK_ID%TYPE,
        l_entry_date LEDGER.ENTRY_DATE%TYPE,
        l_currency_id DOC.CURRENCY_ID%TYPE,
        l_pay_sched TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
        l_business_unit CORP.BUSINESS_UNIT.BUSINESS_UNIT_ID%TYPE,
        l_always_as_accum CHAR
    )
     RETURN BATCH.BATCH_ID%TYPE
    IS
        l_batch_id BATCH.BATCH_ID%TYPE;
        l_doc_id DOC.DOC_ID%TYPE;
        l_start_date BATCH.START_DATE%TYPE;
        l_end_date BATCH.END_DATE%TYPE;
        l_batch_state_cd BATCH.BATCH_STATE_CD%TYPE;
        l_start_fill_id BATCH_FILL.START_FILL_ID%TYPE;
        l_end_fill_id BATCH_FILL.END_FILL_ID%TYPE;
        l_counters_must_show CHAR(1);
        l_lock VARCHAR2(128);
    BEGIN
        -- calculate batch (and create if necessary)
        -- Get an "Open" doc record
        l_doc_id := GET_OR_CREATE_DOC(l_cust_bank_id, l_currency_id, l_business_unit);
        --l_lock := GLOBALS_PKG.REQUEST_LOCK('BATCH.TERMINAL_ID',l_terminal_id); -- this may not be necessary since we lock the doc on customer_bank_id
        SELECT MAX(B.BATCH_ID) -- just to be safe in case there is more than one
          INTO l_batch_id
          FROM BATCH B
         WHERE B.DOC_ID = l_doc_id
           AND B.TERMINAL_ID = l_terminal_id
           AND B.PAYMENT_SCHEDULE_ID = l_pay_sched
           AND B.BATCH_STATE_CD IN('O', 'L') -- Open or Closeable only
           -- for "As Accumulated", batch start date does not matter
           AND (B.START_DATE <= l_entry_date OR l_pay_sched IN(1,5,8))
           AND (NVL(B.END_DATE, MAX_DATE) > l_entry_date OR (B.END_DATE = l_entry_date AND l_pay_sched IN(8)))
           AND (l_pay_sched NOT IN(8) OR DECODE(B.END_DATE, NULL, 0, 1) = DECODE(l_always_as_accum, 'E', 1, 0));
        IF l_batch_id IS NULL THEN
            -- calc start and end dates
            IF l_pay_sched IN(1, 5) THEN
                SELECT LEAST(NVL(MAX(B.END_DATE), l_entry_date), l_entry_date)
                  INTO l_start_date
                  FROM BATCH B, DOC D
                 WHERE B.DOC_ID = D.DOC_ID
                   AND B.TERMINAL_ID = l_terminal_id
                   AND D.CUSTOMER_BANK_ID = l_cust_bank_id
                   AND NVL(D.CURRENCY_ID, 0) = NVL(l_currency_id, 0)
                   AND B.PAYMENT_SCHEDULE_ID = l_pay_sched;
                l_batch_state_cd := 'L'; -- Closeable
                -- end date is null
            ELSIF l_pay_sched = 2 THEN
                SELECT DECODE(MAX(BCOT.BATCH_CONFIRM_OPTION_TYPE_ID), NULL, 'N', 'Y')
                  INTO l_counters_must_show
                  FROM REPORT.TERMINAL T
                  JOIN CORP.BATCH_CONFIRM BC ON T.CUSTOMER_ID = BC.CUSTOMER_ID
                  JOIN CORP.BATCH_CONFIRM_OPTION BCO
                    ON BC.BATCH_CONFIRM_ID = BCO.BATCH_CONFIRM_ID
                  JOIN CORP.BATCH_CONFIRM_OPTION_TYPE BCOT ON BCO.BATCH_CONFIRM_OPTION_TYPE_ID = BCOT.BATCH_CONFIRM_OPTION_TYPE_ID
                 WHERE BC.PAYMENT_SCHEDULE_ID = l_pay_sched 
                   AND BCOT.BATCH_CONFIRM_OPTION_TYPE_CD = 'COUNTER_MUST_SHOW'                  
                   AND T.TERMINAL_ID = l_terminal_id;
                       
                SELECT NVL(MAX(FILL_DATE), MIN_DATE), MAX(FILL_ID)
                  INTO l_start_date, l_start_fill_id
                  FROM (
                    SELECT f.FILL_DATE, f.FILL_ID
                      FROM REPORT.FILL f
                      JOIN REPORT.TERMINAL_EPORT te
                        ON f.EPORT_ID = te.EPORT_ID
                       AND f.FILL_DATE >= NVL(te.START_DATE, MIN_DATE)
                       AND f.FILL_DATE < NVL(te.END_DATE, MAX_DATE)
                     WHERE f.FILL_DATE <= l_entry_date
                       AND te.TERMINAL_ID = l_terminal_id
                       AND (F.COUNTERS_DISPLAYED_FLAG = 'Y' OR l_counters_must_show = 'N')
                     ORDER BY f.FILL_DATE DESC)
                 WHERE ROWNUM = 1 ;
                 SELECT MIN(FILL_DATE), MIN(FILL_ID)
                  INTO l_end_date, l_end_fill_id
                  FROM (
                    SELECT f.FILL_DATE, f.FILL_ID
                      FROM REPORT.FILL f
                      JOIN (SELECT CONNECT_BY_ROOT FILL_ID START_FILL_ID, FILL_ID END_FILL_ID, LEVEL - 1 DEPTH
                              FROM REPORT.FILL
                              START WITH FILL_ID = l_start_fill_id
                              CONNECT BY NOCYCLE PRIOR FILL_ID = PREV_FILL_ID
                              ) H ON F.FILL_ID = H.END_FILL_ID
                     JOIN REPORT.TERMINAL_EPORT te
                        ON f.EPORT_ID = te.EPORT_ID
                       AND f.FILL_DATE >= NVL(te.START_DATE, MIN_DATE)
                       AND f.FILL_DATE < NVL(te.END_DATE, MAX_DATE)
                     WHERE f.FILL_DATE > l_entry_date
                       AND te.TERMINAL_ID = l_terminal_id
                       AND (F.COUNTERS_DISPLAYED_FLAG = 'Y' OR l_counters_must_show = 'N')
                     ORDER BY f.FILL_DATE ASC)
                 WHERE ROWNUM = 1;
                
                l_batch_state_cd := 'O'; -- Open
            ELSIF l_pay_sched = 8 THEN
                SELECT LEAST(NVL(MAX(B.END_DATE), l_entry_date), l_entry_date)
                  INTO l_start_date
                  FROM BATCH B, DOC D
                 WHERE B.DOC_ID = D.DOC_ID
                   AND B.TERMINAL_ID = l_terminal_id
                   AND D.CUSTOMER_BANK_ID = l_cust_bank_id
                   AND NVL(D.CURRENCY_ID, 0) = NVL(l_currency_id, 0)
                   AND B.PAYMENT_SCHEDULE_ID = l_pay_sched;
                l_batch_state_cd := 'O'; -- Open
            ELSE
                SELECT TRUNC(l_entry_date - (OFFSET_HOURS / 24), INTERVAL) + (OFFSET_HOURS / 24),
                       ADD_MONTHS(TRUNC(l_entry_date - (OFFSET_HOURS / 24), INTERVAL) + DAYS, MONTHS) + (OFFSET_HOURS / 24)
                  INTO l_start_date, l_end_date
                  FROM PAYMENT_SCHEDULE
                 WHERE PAYMENT_SCHEDULE_ID = l_pay_sched;
                IF l_end_date <= l_entry_date THEN -- trouble, should not happen
                    l_end_date := l_entry_date + (1.0/(24*60*60));
                END IF;
                l_batch_state_cd := 'O'; -- Open
            END IF;

            -- create new batch record
            SELECT BATCH_SEQ.NEXTVAL
              INTO l_batch_id
              FROM DUAL;
            INSERT INTO BATCH(
                BATCH_ID,
                DOC_ID,
                TERMINAL_ID,
                PAYMENT_SCHEDULE_ID,
                START_DATE,
                END_DATE,
                BATCH_STATE_CD)
              VALUES(
                l_batch_id,
                l_doc_id,
                l_terminal_id,
                l_pay_sched,
                l_start_date,
                l_end_date,
                l_batch_state_cd);
            IF l_pay_sched = 2 THEN
                INSERT INTO CORP.BATCH_FILL(
                    BATCH_ID,
                    START_FILL_ID,
                    END_FILL_ID)
                  VALUES(
                    l_batch_id,
                    l_start_fill_id,
                    l_end_fill_id);
            END IF;
        END IF;
        RETURN l_batch_id;
    END;
    FUNCTION GET_OR_CREATE_BATCH(
        l_terminal_id  BATCH.TERMINAL_ID%TYPE,
        l_cust_bank_id DOC.CUSTOMER_BANK_ID%TYPE,
        l_entry_date LEDGER.ENTRY_DATE%TYPE,
        l_currency_id DOC.CURRENCY_ID%TYPE,
        l_always_as_accum CHAR
    )
     RETURN BATCH.BATCH_ID%TYPE
    IS
        l_business_unit CORP.BUSINESS_UNIT.BUSINESS_UNIT_ID%TYPE;
        l_pay_sched TERMINAL.PAYMENT_SCHEDULE_ID%TYPE;
    BEGIN
        -- calculate batch (and create if necessary)
        SELECT DECODE(l_always_as_accum,
                    'Y', 1,
                    'A', 5,
                    PAYMENT_SCHEDULE_ID),
               BUSINESS_UNIT_ID
          INTO l_pay_sched, l_business_unit
          FROM TERMINAL
         WHERE TERMINAL_ID = l_terminal_id;
        RETURN GET_OR_CREATE_BATCH(l_terminal_id,l_cust_bank_id,l_entry_date,l_currency_id,l_pay_sched, l_business_unit, l_always_as_accum);
    END;
    
    FUNCTION GET_NEW_BATCH(
        l_batch_id BATCH.BATCH_ID%TYPE,
        l_entry_date LEDGER.ENTRY_DATE%TYPE)
     RETURN BATCH.BATCH_ID%TYPE
    IS
        l_cb_id DOC.CUSTOMER_BANK_ID%TYPE;
        l_curr_id DOC.CURRENCY_ID%TYPE;
        l_term_id BATCH.TERMINAL_ID%TYPE;
    BEGIN

        SELECT D.CUSTOMER_BANK_ID, D.CURRENCY_ID, B.TERMINAL_ID
          INTO l_cb_id, l_curr_id, l_term_id
          FROM BATCH B, DOC D
          WHERE D.DOC_ID = B.DOC_ID
            AND B.BATCH_ID = l_batch_id;
        RETURN GET_OR_CREATE_BATCH(l_term_id, l_cb_id, l_entry_date, l_curr_id, 'Y');
    END;
    
    FUNCTION GET_DOC_STATUS(
        l_doc_id DOC.DOC_ID%TYPE)
     RETURN DOC.STATUS%TYPE
    IS
        l_status DOC.STATUS%TYPE;
    BEGIN
        SELECT STATUS INTO l_status FROM CORP.DOC WHERE DOC_ID = l_doc_id;
        RETURN l_status;
    END;

    FUNCTION FREEZE_DOC_STATUS_LEDGER_ID(
        l_ledger_id LEDGER.LEDGER_ID%TYPE)
     RETURN DOC.STATUS%TYPE
    IS
        l_doc_id DOC.DOC_ID%TYPE;
        l_customer_bank_id CORP.DOC.CUSTOMER_BANK_ID%TYPE;
        l_lock VARCHAR2(128);
    BEGIN
        SELECT D.DOC_ID, D.CUSTOMER_BANK_ID
          INTO l_doc_id, l_customer_bank_id
          FROM CORP.DOC D
         INNER JOIN CORP.BATCH B ON D.DOC_ID = B.DOC_ID
         INNER JOIN CORP.LEDGER L ON L.BATCH_ID = B.BATCH_ID
         WHERE L.LEDGER_ID = l_ledger_id;
        l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_customer_bank_id);
        RETURN GET_DOC_STATUS(l_doc_id);
    END;
    
    FUNCTION FREEZE_DOC_STATUS_DOC_ID(
        l_doc_id DOC.DOC_ID%TYPE)
     RETURN DOC.STATUS%TYPE
    IS
        l_customer_bank_id CORP.DOC.CUSTOMER_BANK_ID%TYPE;
        l_lock VARCHAR2(128);
    BEGIN
        SELECT D.CUSTOMER_BANK_ID
          INTO l_customer_bank_id
          FROM CORP.DOC D
         WHERE D.DOC_ID = l_doc_id;
        l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_customer_bank_id);
        RETURN GET_DOC_STATUS(l_doc_id);
    END;
    
    -- Returns 'Y' if the batch is either an "As Accumulated" or has no unsettled transactions
    FUNCTION BATCH_CLOSABLE(
        l_batch_id BATCH.BATCH_ID%TYPE,
        l_pay_sched BATCH.PAYMENT_SCHEDULE_ID%TYPE,
        l_end_date BATCH.END_DATE%TYPE,
        l_batch_state_cd BATCH.BATCH_STATE_CD%TYPE)
     RETURN CHAR
    IS
        l_cnt NUMBER;
    BEGIN
        IF l_batch_state_cd IN('L', 'F') THEN
            RETURN 'Y';
        ELSIF l_batch_state_cd IN('C', 'D') THEN
            IF l_pay_sched IN(2) THEN -- Fill To Fill
                SELECT COUNT(*)
                  INTO l_cnt
                  FROM LEDGER l
                 WHERE ENTRY_PAYABLE(L.SETTLE_STATE_ID, L.ENTRY_TYPE) = '?' -- not processed
                   AND L.DELETED = 'N'
                   AND L.BATCH_ID = l_batch_id;
                IF l_cnt = 0 THEN
                    RETURN 'Y';
                ELSE
                    RETURN 'N';
                END IF;
            ELSE
                RETURN 'Y';
            END IF;
        ELSIF l_batch_state_cd IN('O') AND l_pay_sched NOT IN(1,2,5,7,8) AND l_end_date <= SYSDATE THEN
            RETURN 'Y';
        ELSE
            RETURN 'N';
        END IF;
    END;

    -- Returns 'Y' if the batch is either an "As Accumulated" or has no unsettled transactions
    FUNCTION BATCH_CLOSABLE(
        l_batch_id BATCH.BATCH_ID%TYPE)
     RETURN CHAR
    IS
        l_pay_sched BATCH.PAYMENT_SCHEDULE_ID%TYPE;
        l_end_date BATCH.END_DATE%TYPE;
        l_batch_state_cd BATCH.BATCH_STATE_CD%TYPE;
    BEGIN
        SELECT PAYMENT_SCHEDULE_ID, END_DATE, BATCH_STATE_CD
          INTO l_pay_sched, l_end_date, l_batch_state_cd
          FROM BATCH
         WHERE BATCH_ID = l_batch_id;
        RETURN BATCH_CLOSABLE(l_batch_id, l_pay_sched, l_end_date, l_batch_state_cd);
    END;

    PROCEDURE CHECK_TRANS_WITH_PF_CLOSED(
        l_process_fee_id    CORP.LEDGER.PROCESS_FEE_ID%TYPE)
    IS
        CURSOR l_check_cur IS
           SELECT L.TRANS_ID
              FROM LEDGER L, BATCH B, DOC D
             WHERE L.PROCESS_FEE_ID = l_process_fee_id
               AND L.BATCH_ID = B.BATCH_ID
               AND B.DOC_ID = D.DOC_ID
             GROUP BY L.TRANS_ID
             HAVING SUM(DECODE(D.STATUS, 'O', 0, 'D', 0, 1)) > 0
                AND SUM(DECODE(D.STATUS, 'O', 1, 0)) = 0;
        l_batch_id BATCH.BATCH_ID%TYPE;
    BEGIN
        FOR l_check_rec IN l_check_cur LOOP
            SELECT MAX(B.BATCH_ID)
              INTO l_batch_id
              FROM LEDGER L, BATCH B, DOC D
             WHERE L.TRANS_ID = l_check_rec.TRANS_ID
               AND L.BATCH_ID = B.BATCH_ID
               AND B.DOC_ID = D.DOC_ID
               AND D.STATUS NOT IN('O', 'D');
            IF l_batch_id IS NOT NULL THEN
                RAISE_APPLICATION_ERROR(-20701, 'PROCESS FEE (process_fee_id='||TO_CHAR(l_process_fee_id)||' is used in a batch (id='||TO_CHAR(l_batch_id)||') that was already closed - cannot update ledger table!');
            END IF;
        END LOOP;
    END;

    -- Checks if the specified transaction is in a closed batch and if so raises an
    -- exception
    PROCEDURE CHECK_TRANS_CLOSED(
        l_trans_id    CORP.LEDGER.TRANS_ID%TYPE)
    IS
        l_closed_cnt PLS_INTEGER;
        l_open_cnt PLS_INTEGER;
        l_batch_id BATCH.BATCH_ID%TYPE;
    BEGIN
        -- get the batch
        SELECT SUM(DECODE(D.STATUS, 'O', 0, 'D', 0, 1)),
               SUM(DECODE(D.STATUS, 'O', 1, 0))
          INTO l_closed_cnt, l_open_cnt
          FROM LEDGER L, BATCH B, DOC D
         WHERE L.TRANS_ID = l_trans_id
           AND L.BATCH_ID = B.BATCH_ID
           AND B.DOC_ID = D.DOC_ID;
        IF l_open_cnt = 0 AND l_closed_cnt > 0 THEN
            SELECT MAX(B.BATCH_ID)
              INTO l_batch_id
              FROM LEDGER L, BATCH B, DOC D
             WHERE L.TRANS_ID = l_trans_id
               AND L.BATCH_ID = B.BATCH_ID
               AND B.DOC_ID = D.DOC_ID
               AND D.STATUS NOT IN('O', 'D');
            RAISE_APPLICATION_ERROR(-20701, 'TRANSACTION (tran_id='||TO_CHAR(l_trans_id)||' is in a batch (id='||TO_CHAR(l_batch_id)||') that was already closed - cannot update ledger table!');
        END IF;
    END;
    
    PROCEDURE CHECK_FILL_BATCH_COMPLETE(
        l_batch_id BATCH.BATCH_ID%TYPE)
    IS
        l_batch_amt NUMBER;
        l_batch_tran_cnt NUMBER;
        l_src_credit_amt NUMBER;
        l_src_credit_cnt NUMBER;
        l_prev_fill_id REPORT.FILL.FILL_ID%TYPE;
        l_start_fill_id REPORT.FILL.FILL_ID%TYPE;
        l_first_tran_ts DATE;
        l_last_tran_ts DATE;
    BEGIN
        SELECT SUM(l.AMOUNT), MIN(l.ENTRY_DATE)
          INTO l_batch_amt, l_first_tran_ts
     	  FROM CORP.LEDGER l
         WHERE l.ENTRY_TYPE = 'CC'
           AND l.DELETED = 'N'
           AND l.BATCH_ID = l_batch_id;
                   
        SELECT SUM(p.AMOUNT)
          INTO l_batch_tran_cnt
          FROM REPORT.PURCHASE P
         WHERE P.TRAN_ID IN(
            SELECT l.TRANS_ID 
              FROM CORP.LEDGER l
             WHERE l.ENTRY_TYPE = 'CC'
               AND l.DELETED = 'N'
               AND l.BATCH_ID = l_batch_id);
        
        BEGIN
            SELECT SUM(HF.CREDIT_AMOUNT), SUM(HF.CREDIT_COUNT)
              INTO l_src_credit_amt, l_src_credit_cnt
              FROM CORP.BATCH_FILL BF
              JOIN (SELECT CONNECT_BY_ROOT FILL_ID ANCESTOR_FILL_ID, F.*
                      FROM REPORT.FILL F
                     START WITH F.FILL_ID = (SELECT START_FILL_ID FROM CORP.BATCH_FILL WHERE BATCH_ID = l_batch_id)
                   CONNECT BY NOCYCLE PRIOR FILL_ID = PREV_FILL_ID) HF ON BF.START_FILL_ID = HF.ANCESTOR_FILL_ID AND BF.END_FILL_ID = HF.FILL_ID
             WHERE BF.BATCH_ID = l_batch_id 
            HAVING COUNT(*) = COUNT(HF.CREDIT_AMOUNT) 
               AND COUNT(*) =  COUNT(HF.CREDIT_COUNT);
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                DECLARE
                    CURSOR l_cur IS
                        SELECT e.EPORT_SERIAL_NUM DEVICE_SERIAL_CD, 
                               GREATEST(l_first_tran_ts, NVL(te.START_DATE, MIN_DATE)) START_DATE,
                               LEAST(B.END_DATE,NVL(te.END_DATE, MAX_DATE)) END_DATE 
                          FROM CORP.BATCH B
                          JOIN REPORT.TERMINAL_EPORT te ON b.TERMINAL_ID = te.TERMINAL_ID
            	          JOIN REPORT.EPORT E ON te.EPORT_ID = e.EPORT_ID
        		         WHERE B.BATCH_ID = l_batch_id;
        		BEGIN
                    l_src_credit_amt := 0;
                    l_src_credit_cnt := 0;
                    FOR l_rec IN l_cur LOOP
                        SELECT l_src_credit_amt + NVL(SUM(T.TRAN_LINE_ITEM_AMOUNT * T.TRAN_LINE_ITEM_QUANTITY), 0), 
                               l_src_credit_cnt + NVL(SUM(DECODE(T.TRAN_LINE_ITEM_TYPE_GROUP_CD, 'U', NULL, 'I', NULL, T.TRAN_LINE_ITEM_QUANTITY)), 0)
                	      INTO l_src_credit_amt, l_src_credit_cnt                         
                          FROM PSS.VW_REPORTING_TRAN_LINE_ITEM@USADBP_PSS T
                          WHERE T.DEVICE_SERIAL_CD = l_rec.DEVICE_SERIAL_CD
                            AND T.CLIENT_PAYMENT_TYPE_CD IN('C', 'R')
                            AND T.TRAN_START_TS BETWEEN l_rec.START_DATE AND l_rec.END_DATE;
                    END LOOP; 
                END;       
            WHEN OTHERS THEN
                RAISE;
        END;

        IF l_batch_amt = l_src_credit_amt AND l_batch_tran_cnt = l_src_credit_cnt THEN
            UPDATE CORP.BATCH B
               SET B.BATCH_STATE_CD = (
                    SELECT DECODE(COUNT(*), 0, 'D', 'U')
                      FROM REPORT.TERMINAL t
                      JOIN CORP.BATCH_CONFIRM CN ON t.CUSTOMER_ID = cn.CUSTOMER_ID
                     WHERE b.TERMINAL_ID = t.TERMINAL_ID
                       AND b.PAYMENT_SCHEDULE_ID = cn.PAYMENT_SCHEDULE_ID),
                   B.BATCH_CLOSED_TS = SYSDATE
              WHERE B.BATCH_ID = l_batch_id;
        END IF;	
    END;
    
    PROCEDURE ADD_BATCH_ROUNDING_ENTRY(
        l_batch_id BATCH.BATCH_ID%TYPE)
    IS
        l_round_amt LEDGER.AMOUNT%TYPE;
    BEGIN
        -- calculate rounding amount
        SELECT ROUND(SUM(AMOUNT), 2) - SUM(AMOUNT)
          INTO l_round_amt
          FROM LEDGER L
         WHERE L.DELETED = 'N'
           AND ENTRY_PAYABLE(L.SETTLE_STATE_ID,L.ENTRY_TYPE) = 'Y'
           AND L.BATCH_ID = l_batch_id;

        -- add entry if necessary
        IF l_round_amt <> 0 THEN
            INSERT INTO LEDGER(
                LEDGER_ID,
                ENTRY_TYPE,
                TRANS_ID,
                PROCESS_FEE_ID,
                AMOUNT,
                ENTRY_DATE,
                BATCH_ID,
                SETTLE_STATE_ID,
                LEDGER_DATE,
                DESCRIPTION)
            SELECT
                LEDGER_SEQ.NEXTVAL,
                'SB',
                NULL,
                NULL,
                l_round_amt,
                SYSDATE,
                l_batch_id,
                2,
                SYSDATE,
                NULL
              FROM DUAL;
        END IF;
    END;

    PROCEDURE ADD_BATCH_ROUNDING_ENTRIES(
        l_doc_id DOC.DOC_ID%TYPE)
    IS
        CURSOR l_cur IS
            SELECT BATCH_ID
              FROM BATCH
             WHERE DOC_ID = l_doc_id;
    BEGIN
        FOR l_rec IN l_cur LOOP
            ADD_BATCH_ROUNDING_ENTRY(l_rec.BATCH_ID);
        END LOOP;
    END;

    PROCEDURE ADD_ROUNDING_ENTRY(
        l_doc_id DOC.DOC_ID%TYPE)
    IS
        l_round_amt LEDGER.AMOUNT%TYPE;
        l_batch_id BATCH.BATCH_ID%TYPE;
    BEGIN
        -- calculate rounding amount
        SELECT ROUND(SUM(AMOUNT), 2) - SUM(AMOUNT)
          INTO l_round_amt
          FROM LEDGER L, BATCH B
         WHERE L.DELETED = 'N'
           AND ENTRY_PAYABLE(L.SETTLE_STATE_ID,L.ENTRY_TYPE) = 'Y'
           AND L.BATCH_ID = B.BATCH_ID
           AND B.DOC_ID = l_doc_id;

        -- add entry if necessary
        IF l_round_amt <> 0 THEN
            SELECT MAX(BATCH_ID)
              INTO l_batch_id
              FROM BATCH
             WHERE DOC_ID = l_doc_id
               AND TERMINAL_ID IS NULL;
            IF l_batch_id IS NULL THEN
                -- create a batch and set the doc id
                SELECT BATCH_SEQ.NEXTVAL
                  INTO l_batch_id
                  FROM DUAL;
                INSERT INTO BATCH(BATCH_ID, DOC_ID, TERMINAL_ID, PAYMENT_SCHEDULE_ID, START_DATE, END_DATE, BATCH_STATE_CD)
                    SELECT l_batch_id, l_doc_id, NULL, 1, SYSDATE, SYSDATE, 'F'
                      FROM DOC
                     WHERE DOC_ID = l_doc_id;
            END IF;

            INSERT INTO LEDGER(
                LEDGER_ID,
                ENTRY_TYPE,
                TRANS_ID,
                PROCESS_FEE_ID,
                AMOUNT,
                ENTRY_DATE,
                BATCH_ID,
                SETTLE_STATE_ID,
                LEDGER_DATE,
                DESCRIPTION)
            SELECT
                LEDGER_SEQ.NEXTVAL,
                'SB',
                NULL,
                NULL,
                l_round_amt,
                SYSDATE,
                l_batch_id,
                2,
                SYSDATE,
                NULL
              FROM DUAL;
        END IF;
    END;
    /*
     * This procedure references REPORT.TRAN
    */
    PROCEDURE UPDATE_SETTLEMENT(
        l_trans_id    CORP.LEDGER.TRANS_ID%TYPE,
        l_settle_state_id CORP.LEDGER.SETTLE_STATE_ID%TYPE,
        l_settle_date   CORP.LEDGER.LEDGER_DATE%TYPE)
    IS
        CURSOR l_cur IS
            SELECT D.DOC_ID, B.BATCH_ID, D.CUSTOMER_BANK_ID
              FROM LEDGER L, BATCH B, DOC D
             WHERE L.TRANS_ID = l_trans_id
               AND L.BATCH_ID = B.BATCH_ID
               AND B.DOC_ID = D.DOC_ID
               AND D.STATUS = 'O';
        TYPE t_rec_list IS TABLE OF l_cur%ROWTYPE;
        l_recs t_rec_list;
        l_batch_ids NUMBER_TABLE := NUMBER_TABLE();
        l_lock VARCHAR2(128);
    BEGIN
        l_lock := GLOBALS_PKG.REQUEST_LOCK('LEDGER.TRANS_ID',l_trans_id);
        OPEN l_cur;
        FETCH l_cur BULK COLLECT INTO l_recs;
        CLOSE l_cur;

        IF l_recs.FIRST IS NOT NULL THEN
            FOR i IN l_recs.FIRST .. l_recs.LAST LOOP
                l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_recs(i).CUSTOMER_BANK_ID);
                IF GET_DOC_STATUS(l_recs(i).DOC_ID) = 'O' THEN
                   l_batch_ids.EXTEND;
                   l_batch_ids(l_batch_ids.LAST) := l_recs(i).BATCH_ID;
                ELSE
                    EXIT;
                END IF;
            END LOOP;
        END IF;
        IF l_recs.FIRST IS NOT NULL AND l_recs.LAST = l_batch_ids.LAST THEN -- we can do direct update Yippee!
            UPDATE LEDGER SET
                SETTLE_STATE_ID = l_settle_state_id,
                LEDGER_DATE = l_settle_date
             WHERE TRANS_ID = l_trans_id
               AND BATCH_ID MEMBER OF l_batch_ids;
        ELSE
           DECLARE
                l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE;
                l_close_date LEDGER.LEDGER_DATE%TYPE;
                l_total_amount LEDGER.AMOUNT%TYPE;
                l_terminal_id BATCH.TERMINAL_ID%TYPE;
                l_customer_bank_id DOC.CUSTOMER_BANK_ID%TYPE;
                l_process_fee_id LEDGER.PROCESS_FEE_ID%TYPE;
                l_currency_id DOC.CURRENCY_ID%TYPE;
            BEGIN
                SELECT TRANS_TYPE_ID, CLOSE_DATE, TOTAL_AMOUNT, TERMINAL_ID,
                       CUSTOMER_BANK_ID, PROCESS_FEE_ID, CURRENCY_ID
                  INTO l_trans_type_id, l_close_date, l_total_amount, l_terminal_id,
                       l_customer_bank_id, l_process_fee_id, l_currency_id
                  FROM TRANS
                 WHERE TRAN_ID = l_trans_id;

                UPDATE_LEDGER(l_trans_id, l_trans_type_id, l_close_date,
                    l_settle_date, l_total_amount, l_settle_state_id,
                    l_terminal_id, l_customer_bank_id,l_process_fee_id, l_currency_id);
            END;
        END IF;
    END;

    PROCEDURE UPDATE_PROCESS_FEE_VALUES(
        l_process_fee_id CORP.LEDGER.PROCESS_FEE_ID%TYPE)
    IS
        CURSOR l_tran_cur IS
           SELECT DISTINCT L.TRANS_ID, X.TRANS_TYPE_ID, X.CLOSE_DATE, X.TOTAL_AMOUNT,
                  X.TERMINAL_ID, X.CUSTOMER_BANK_ID, X.SETTLE_STATE_ID,
                  X.SETTLE_DATE, X.CURRENCY_ID
              FROM LEDGER L, REPORT.TRANS X
             WHERE L.PROCESS_FEE_ID = l_process_fee_id
               AND L.DELETED = 'N'
               AND L.ENTRY_TYPE = 'PF'
               AND L.TRANS_ID = X.TRAN_ID;
    BEGIN
        FOR l_tran_rec IN l_tran_cur LOOP
            UPDATE_LEDGER(
                l_tran_rec.TRANS_ID,
                l_tran_rec.trans_type_id,
                l_tran_rec.close_date,
                l_tran_rec.settle_date,
                l_tran_rec.total_amount,
                l_tran_rec.settle_state_id,
                l_tran_rec.terminal_id,
                l_tran_rec.customer_bank_id,
                l_process_fee_id,
                l_tran_rec.currency_id);
        END LOOP;
    END;

    -- Puts CC, RF, or CB record into Ledger and also any required PF record
    PROCEDURE INSERT_TRANS_TO_LEDGER(
        l_trans_id    LEDGER.TRANS_ID%TYPE,
        l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE,
        l_close_date    LEDGER.ENTRY_DATE%TYPE,
        l_settle_date LEDGER.LEDGER_DATE%TYPE,
        l_amount LEDGER.AMOUNT%TYPE,
        l_settle_state_id LEDGER.SETTLE_STATE_ID%TYPE,
        l_process_fee_id    LEDGER.PROCESS_FEE_ID%TYPE,
        l_batch_id LEDGER.BATCH_ID%TYPE)
    IS
        l_ledger_id LEDGER.LEDGER_ID%TYPE;
        l_real_amt LEDGER.AMOUNT%TYPE;
        l_pay_sched BATCH.PAYMENT_SCHEDULE_ID%TYPE;
        l_end_date BATCH.END_DATE%TYPE;
    BEGIN
        IF NVL(l_amount, 0) = 0 THEN
            RETURN;
        END IF;
        IF l_trans_type_id IN(16,19,20,21) THEN
            SELECT LEDGER_SEQ.NEXTVAL,
                   DECODE(l_trans_type_id, 20, -ABS(l_amount), 21, -ABS(l_amount), l_amount)
              INTO l_ledger_id,
                   l_real_amt
              FROM DUAL;
            INSERT INTO LEDGER(
                LEDGER_ID,
                ENTRY_TYPE,
                TRANS_ID,
                PROCESS_FEE_ID,
                AMOUNT,
                ENTRY_DATE,
                BATCH_ID,
                SETTLE_STATE_ID,
                LEDGER_DATE)
            SELECT
                l_ledger_id,
                DECODE(l_trans_type_id, 16, 'CC', 19, 'CC', 20, 'RF', 21, 'CB'),--, 'AU'), -- Audit trail of other transactions
                l_trans_id,
                l_process_fee_id,
                l_real_amt,
                l_close_date,
                l_batch_id,
                l_settle_state_id,
                l_settle_date
              FROM DUAL;
            -- create net revenue fee on transaction, if any
            INSERT INTO LEDGER(
                LEDGER_ID,
                RELATED_LEDGER_ID,
                ENTRY_TYPE,
                TRANS_ID,
                PROCESS_FEE_ID,
                SERVICE_FEE_ID,
                AMOUNT,
                ENTRY_DATE,
                BATCH_ID,
                SETTLE_STATE_ID,
                LEDGER_DATE,
                DESCRIPTION)
            SELECT
                LEDGER_SEQ.NEXTVAL,
                l_ledger_id,
                'SF',
                l_trans_id,
                l_process_fee_id,
                SF.SERVICE_FEE_ID,
                -l_real_amt * SF.FEE_PERCENT,
                l_close_date,
                l_batch_id,
                l_settle_state_id,
                l_settle_date,
                F.FEE_NAME
            FROM SERVICE_FEES SF, FEES F, BATCH B
           WHERE SF.FEE_ID = F.FEE_ID
             AND SF.TERMINAL_ID = B.TERMINAL_ID
             AND B.BATCH_ID = l_batch_id
             AND l_close_date BETWEEN NVL(SF.START_DATE, MIN_DATE)
                  AND NVL(SF.END_DATE, MAX_DATE)
             AND SF.FREQUENCY_ID = 6;
        END IF;
        IF l_process_fee_id IS NOT NULL THEN
            BEGIN
                SELECT LEDGER_SEQ.NEXTVAL,
                       -GREATEST((ABS(l_amount) * pf.FEE_PERCENT + pf.FEE_AMOUNT), pf.MIN_AMOUNT)
                  INTO l_ledger_id,
                       l_real_amt
                  FROM PROCESS_FEES pf
                WHERE pf.PROCESS_FEE_ID = l_process_fee_id
                  AND (pf.FEE_PERCENT > 0 OR pf.FEE_AMOUNT > 0);
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                RETURN;
              WHEN OTHERS THEN
                RAISE;
            END;
            --also insert process fee
            INSERT INTO LEDGER(
                LEDGER_ID,
                ENTRY_TYPE,
                TRANS_ID,
                PROCESS_FEE_ID,
                AMOUNT,
                ENTRY_DATE,
                BATCH_ID,
                SETTLE_STATE_ID,
                LEDGER_DATE)
            VALUES(
                l_ledger_id,
                'PF',
                l_trans_id,
                l_process_fee_id,
                l_real_amt,
                l_close_date,
                l_batch_id,
                l_settle_state_id,
                l_settle_date);
            -- create net revenue fee on process fee, if any
            INSERT INTO LEDGER(
                LEDGER_ID,
                RELATED_LEDGER_ID,
                ENTRY_TYPE,
                TRANS_ID,
                PROCESS_FEE_ID,
                SERVICE_FEE_ID,
                AMOUNT,
                ENTRY_DATE,
                BATCH_ID,
                SETTLE_STATE_ID,
                LEDGER_DATE,
                DESCRIPTION)
            SELECT
                LEDGER_SEQ.NEXTVAL,
                l_ledger_id,
                'SF',
                l_trans_id,
                l_process_fee_id,
                SF.SERVICE_FEE_ID,
                -l_real_amt * SF.FEE_PERCENT,
                l_close_date,
                l_batch_id,
                l_settle_state_id,
                l_settle_date,
                F.FEE_NAME
            FROM SERVICE_FEES SF, FEES F, BATCH B
           WHERE SF.FEE_ID = F.FEE_ID
             AND SF.TERMINAL_ID = B.TERMINAL_ID
             AND B.BATCH_ID = l_batch_id
             AND l_close_date BETWEEN NVL(SF.START_DATE, MIN_DATE)
                  AND NVL(SF.END_DATE, MAX_DATE)
             AND SF.FREQUENCY_ID = 6;
        END IF;
        -- Now check batch complete
        IF l_trans_type_id IN(16,19) THEN
            SELECT PAYMENT_SCHEDULE_ID, END_DATE
              INTO l_pay_sched, l_end_date
              FROM CORP.BATCH B
             WHERE B.BATCH_ID = l_batch_id;
            IF l_pay_sched IN(2) AND l_end_date IS NOT NULL THEN
               CHECK_FILL_BATCH_COMPLETE(l_batch_id);
            END IF;
        END IF;
    END;

    PROCEDURE INSERT_TRANS_TO_LEDGER(
        l_trans_id    LEDGER.TRANS_ID%TYPE,
        l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE,
        l_close_date    LEDGER.ENTRY_DATE%TYPE,
        l_settle_date LEDGER.LEDGER_DATE%TYPE,
        l_amount LEDGER.AMOUNT%TYPE,
        l_settle_state_id LEDGER.SETTLE_STATE_ID%TYPE,
        l_process_fee_id    LEDGER.PROCESS_FEE_ID%TYPE,
        l_terminal_id   BATCH.TERMINAL_ID%TYPE,
        l_customer_bank_id  DOC.CUSTOMER_BANK_ID%TYPE,
        l_currency_id       DOC.CURRENCY_ID%TYPE)
    IS
        l_batch_id LEDGER.BATCH_ID%TYPE;
        l_doc_status DOC.STATUS%TYPE;
        l_lock VARCHAR2(128);
    BEGIN
        -- if refund or chargeback get the batch that the
        -- original trans is in, if open use it else use "as accumulated"
        IF l_trans_type_id IN(20, 21) THEN
            l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_customer_bank_id);
            l_lock := GLOBALS_PKG.REQUEST_LOCK('BATCH.TERMINAL_ID',l_terminal_id);
            BEGIN
                SELECT BATCH_ID, STATUS
                  INTO l_batch_id, l_doc_status
                  FROM (SELECT B.BATCH_ID, D.STATUS
                          FROM LEDGER L, BATCH B, TRANS T, DOC D
                         WHERE L.TRANS_ID = T.ORIG_TRAN_ID
                           AND T.TRAN_ID = l_trans_id
                           AND L.BATCH_ID = B.BATCH_ID
                           AND L.DELETED = 'N'
                           AND B.DOC_ID = D.DOC_ID
                           AND NVL(D.CURRENCY_ID, 0) = NVL(l_currency_id, 0)
                           AND L.ENTRY_TYPE = 'CC'
                           AND B.PAYMENT_SCHEDULE_ID NOT IN(8) -- As Exported requires each trans to be put in "open" batch
                         ORDER BY DECODE(D.STATUS, 'O', 0, 1), B.BATCH_ID DESC)
                 WHERE ROWNUM = 1;
                IF NVL(l_doc_status, ' ') <> 'O' THEN
                    l_batch_id := GET_OR_CREATE_BATCH(l_terminal_id, l_customer_bank_id, l_close_date, l_currency_id, 'Y');
                END IF;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN -- refund for trans not in ledger
                -- The following should be removed and the error re-enabled,
                -- once we enforce orig_tran_id for all refunds/ chargebacks
                l_batch_id := GET_OR_CREATE_BATCH(l_terminal_id, l_customer_bank_id, l_close_date, l_currency_id, 'Y');
                --RAISE_APPLICATION_ERROR(-20702, 'TRANSACTION (tran_id='||TO_CHAR(l_trans_id)||' is a refund or chargeback on a transaction NOT found in the ledger table!');
                WHEN OTHERS THEN
                    RAISE;
            END;
        ELSE
            l_batch_id := GET_OR_CREATE_BATCH(l_terminal_id, l_customer_bank_id, l_close_date, l_currency_id, 'N');
        END IF;
        INSERT_TRANS_TO_LEDGER(l_trans_id, l_trans_type_id, l_close_date, l_settle_date,
            l_amount, l_settle_state_id, l_process_fee_id, l_batch_id);
    END;
    
    -- Updates the ledger table with the transaction changes
    -- May fail if the transaction is already part of a document
    -- (i.e. - it has already been paid)
    PROCEDURE UPDATE_LEDGER(
        l_trans_id    LEDGER.TRANS_ID%TYPE,
        l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE,
        l_close_date    LEDGER.ENTRY_DATE%TYPE,
        l_settle_date LEDGER.LEDGER_DATE%TYPE,
        l_total_amount LEDGER.AMOUNT%TYPE,
        l_settle_state_id LEDGER.SETTLE_STATE_ID%TYPE,
        l_terminal_id   BATCH.TERMINAL_ID%TYPE,
        l_customer_bank_id  DOC.CUSTOMER_BANK_ID%TYPE,
        l_process_fee_id    LEDGER.PROCESS_FEE_ID%TYPE,
        l_currency_id       DOC.CURRENCY_ID%TYPE)
    IS
        l_batch_id BATCH.BATCH_ID%TYPE;
        l_closed_cnt PLS_INTEGER;
        l_lock VARCHAR2(128);
        l_prev_cb_ids NUMBER_TABLE;
    BEGIN
        IF l_trans_type_id IN(22) THEN -- cash; ignore
            RETURN;
        END IF;
        l_lock := GLOBALS_PKG.REQUEST_LOCK('LEDGER.TRANS_ID',l_trans_id);
        -- get the batch
        SELECT MAX(B.BATCH_ID), SUM(DECODE(D.STATUS, 'O', 0, 'D', 0, 1)), -- are there ANY closed batches on this transaction?
               CAST(COLLECT(CAST(D.CUSTOMER_BANK_ID AS NUMBER)) AS NUMBER_TABLE)
          INTO l_batch_id, l_closed_cnt, l_prev_cb_ids
          FROM LEDGER L, BATCH B, DOC D
         WHERE L.TRANS_ID = l_trans_id
           AND L.BATCH_ID = B.BATCH_ID
           AND L.DELETED = 'N'
           AND B.DOC_ID = D.DOC_ID;
        IF l_batch_id IS NULL THEN -- Create new batch for this ledger record
            -- If terminal is null then no need to do anything more
            -- Added: if cust bank is null don't do any more (we may need to change this in the future which would also entail making BATCH.CUSTOMER_BANK_ID nullable)
            IF l_terminal_id IS NOT NULL AND l_customer_bank_id IS NOT NULL THEN
                INSERT_TRANS_TO_LEDGER(l_trans_id, l_trans_type_id, l_close_date, l_settle_date,
                    l_total_amount, l_settle_state_id, l_process_fee_id, l_terminal_id, l_customer_bank_id, l_currency_id);
                RETURN; -- all done
            END IF;
        ELSIF l_closed_cnt = 0 THEN -- lock previous customer_bank_id's to ensure that none of the doc's closes
            FOR i IN l_prev_cb_ids.FIRST..l_prev_cb_ids.LAST LOOP
                l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_prev_cb_ids(i));
            END LOOP;
            -- double-check closed count
            SELECT SUM(DECODE(D.STATUS, 'O', 0, 'D', 0, 1))
              INTO l_closed_cnt
              FROM LEDGER L, BATCH B, DOC D
             WHERE L.TRANS_ID = l_trans_id
               AND L.BATCH_ID = B.BATCH_ID
               AND L.DELETED = 'N'
               AND B.DOC_ID = D.DOC_ID;
        END IF;
        IF l_closed_cnt > 0 THEN -- add adjustment for prev paid
            DECLARE
                CURSOR l_prev_cur IS
                    SELECT COUNT(*) PAID_CNT,
                           SUM(DECODE(ENTRY_PAYABLE(L.SETTLE_STATE_ID, L.ENTRY_TYPE), 'Y', AMOUNT, NULL)) PAID_AMT, B.TERMINAL_ID,
                           D.CUSTOMER_BANK_ID, D.CURRENCY_ID
                      FROM LEDGER L, BATCH B, DOC D
                     WHERE L.TRANS_ID = l_trans_id
                       AND L.BATCH_ID = B.BATCH_ID
                       AND B.DOC_ID = D.DOC_ID
                       AND L.DELETED = 'N'
                       AND L.RELATED_LEDGER_ID IS NULL -- Must exclude Net Revenue Fees
                     GROUP BY B.TERMINAL_ID, D.CUSTOMER_BANK_ID, D.CURRENCY_ID;

                l_adj_amt LEDGER.AMOUNT%TYPE;
                l_adjusted_new_cb BOOLEAN := FALSE;
                l_old_pf_amt LEDGER.AMOUNT%TYPE;
                l_old_amt LEDGER.AMOUNT%TYPE;
                l_desc LEDGER.DESCRIPTION%TYPE;
                l_new_payable CHAR(1);
                l_ledger_id LEDGER.LEDGER_ID%TYPE;
            BEGIN
                --figure out what change has occurred
                FOR l_prev_rec IN l_prev_cur LOOP
                    IF NVL(l_customer_bank_id, 0) = l_prev_rec.CUSTOMER_BANK_ID
                       AND NVL(l_currency_id, 0) = NVL(l_prev_rec.CURRENCY_ID, 0) THEN
                        IF NVL(l_prev_rec.PAID_CNT, 0) <> 0 AND NVL(l_prev_rec.PAID_AMT, 0) <> 0 THEN
                            --calc difference and add as adjustment
                            SELECT ENTRY_PAYABLE(l_settle_state_id, DECODE(l_trans_type_id, 16, 'CC', 19, 'CC', 20, 'RF', 21, 'CB'))
                              INTO l_new_payable
                              FROM DUAL;
                            SELECT (CASE WHEN l_trans_type_id IN(16,19,20,21) THEN l_total_amount ELSE 0 END
                                   - NVL(SUM(GREATEST(ABS(l_total_amount) * pf.FEE_PERCENT + pf.FEE_AMOUNT, pf.MIN_AMOUNT)), 0))
                                   * DECODE(l_new_payable, 'Y', 1, 0)
                                   - l_prev_rec.PAID_AMT
                              INTO l_adj_amt
                              FROM PROCESS_FEES pf
                             WHERE pf.PROCESS_FEE_ID = l_process_fee_id;
                            IF l_adj_amt <> 0 THEN
                                IF l_new_payable = 'Y' THEN
                                    SELECT SUM(AMOUNT)
                                      INTO l_old_amt
                                      FROM LEDGER
                                     WHERE TRANS_ID = l_trans_id
                                       AND ENTRY_TYPE IN('CC')
                                       AND ENTRY_PAYABLE(SETTLE_STATE_ID, ENTRY_TYPE) = 'Y'
                                       AND DELETED = 'N';
                                    SELECT SUM(AMOUNT)
                                      INTO l_old_pf_amt
                                      FROM LEDGER
                                     WHERE TRANS_ID = l_trans_id
                                       AND ENTRY_TYPE IN('PF')
                                       AND ENTRY_PAYABLE(SETTLE_STATE_ID, ENTRY_TYPE) = 'Y'
                                       AND DELETED = 'N';
                                    IF NVL(l_old_amt, 0) = 0 AND l_trans_type_id IN(16,19,20,21) THEN
                                        l_desc := 'Correction for a previously ignored transaction';
                                    ELSIF l_old_amt <> l_total_amount AND l_trans_type_id IN(16,19,20,21) THEN
                                        l_desc := 'Correction for a value amount change on a transaction that has already been paid';
                                    ELSIF NVL(l_old_pf_amt, 0) = 0 THEN
                                        l_desc := 'Correction for a missing processing fee on a transaction that has already been paid';
                                    ELSIF NVL(l_terminal_id, 0) <> l_prev_rec.TERMINAL_ID THEN
                                        l_desc := 'Correction for a change of location on a transaction that has already been paid';
                                    ELSE
                                        l_desc := 'Correction for a change in processing fee on a transaction that has already been paid';
                                    END IF;
                                ELSE -- should not be paid now
                                    l_desc := 'Correction for a transaction that has already been paid but has since failed settlement';
                                END IF;
                                l_batch_id := GET_OR_CREATE_BATCH(l_terminal_id,
                                        l_customer_bank_id, l_close_date, l_currency_id, 'A');
                                -- create a negative adjustment based on this previous payment
                                SELECT LEDGER_SEQ.NEXTVAL
                                  INTO l_ledger_id
                                  FROM DUAL;
                                INSERT INTO LEDGER(
                                    LEDGER_ID,
                                    ENTRY_TYPE,
                                    TRANS_ID,
                                    PROCESS_FEE_ID,
                                    AMOUNT,
                                    ENTRY_DATE,
                                    BATCH_ID,
                                    SETTLE_STATE_ID,
                                    LEDGER_DATE,
                                    DESCRIPTION)
                                VALUES(
                                    l_ledger_id,
                                    'AD',
                                    l_trans_id,
                                    NULL,
                                    l_adj_amt,
                                    l_close_date,
                                    l_batch_id,
                                    2,
                                    l_settle_date,
                                    l_desc);
                                -- create net revenue adjustment, if any
                                INSERT INTO LEDGER(
                                    LEDGER_ID,
                                    RELATED_LEDGER_ID,
                                    ENTRY_TYPE,
                                    TRANS_ID,
                                    PROCESS_FEE_ID,
                                    SERVICE_FEE_ID,
                                    AMOUNT,
                                    ENTRY_DATE,
                                    BATCH_ID,
                                    SETTLE_STATE_ID,
                                    LEDGER_DATE,
                                    DESCRIPTION)
                                SELECT
                                    LEDGER_SEQ.NEXTVAL,
                                    l_ledger_id,
                                    'SF',
                                    l_trans_id,
                                    NULL,
                                    SF.SERVICE_FEE_ID,
                                    -l_adj_amt * SF.FEE_PERCENT,
                                    l_close_date,
                                    l_batch_id,
                                    2,
                                    l_settle_date,
                                    F.FEE_NAME
                                FROM SERVICE_FEES SF, FEES F
                               WHERE SF.FEE_ID = F.FEE_ID
                                 AND SF.TERMINAL_ID = l_terminal_id
                                 AND l_close_date BETWEEN NVL(SF.START_DATE, MIN_DATE)
                                      AND NVL(SF.END_DATE, MAX_DATE)
                                 AND SF.FREQUENCY_ID = 6;
                            END IF;
                            l_adjusted_new_cb := TRUE; -- whether we adjust or not the new cb of the tran is appropriately dealt with
                        END IF;
                    --case: bank acct changed
                    ELSIF l_prev_rec.PAID_AMT <> 0 THEN
                        IF NVL(l_terminal_id, 0) <> l_prev_rec.TERMINAL_ID THEN
                            l_desc := 'Correction for a transaction that was assigned to the wrong location';
                        ELSIF NVL(l_currency_id, 0) <> NVL(l_prev_rec.CURRENCY_ID, 0) THEN
                            l_desc := 'Correction for a change in currency';
                        ELSE
                            l_desc := 'Correction for a transaction that was paid to the wrong bank account';
                        END IF;
                        -- deduct entire amt from old
                        l_batch_id := GET_OR_CREATE_BATCH(
                                l_prev_rec.TERMINAL_ID,
                                l_prev_rec.CUSTOMER_BANK_ID,
                                l_close_date,
                                l_prev_rec.CURRENCY_ID,
                                'A');
                        SELECT LEDGER_SEQ.NEXTVAL
                          INTO l_ledger_id
                          FROM DUAL;
                        INSERT INTO LEDGER(
                            LEDGER_ID,
                            ENTRY_TYPE,
                            TRANS_ID,
                            PROCESS_FEE_ID,
                            AMOUNT,
                            ENTRY_DATE,
                            BATCH_ID,
                            SETTLE_STATE_ID,
                            LEDGER_DATE,
                            DESCRIPTION)
                        VALUES(
                            l_ledger_id,
                            'AD',
                            l_trans_id,
                            NULL,
                            -l_prev_rec.PAID_AMT,
                            l_close_date,
                            l_batch_id,
                            l_settle_state_id,
                            l_settle_date,
                            l_desc);
                        -- create net revenue adjustment, if any
                        INSERT INTO LEDGER(
                            LEDGER_ID,
                            RELATED_LEDGER_ID,
                            ENTRY_TYPE,
                            TRANS_ID,
                            PROCESS_FEE_ID,
                            SERVICE_FEE_ID,
                            AMOUNT,
                            ENTRY_DATE,
                            BATCH_ID,
                            SETTLE_STATE_ID,
                            LEDGER_DATE,
                            DESCRIPTION)
                        SELECT
                            LEDGER_SEQ.NEXTVAL,
                            l_ledger_id,
                            'SF',
                            l_trans_id,
                            NULL,
                            SF.SERVICE_FEE_ID,
                            l_prev_rec.PAID_AMT * SF.FEE_PERCENT,
                            l_close_date,
                            l_batch_id,
                            l_settle_state_id,
                            l_settle_date,
                            F.FEE_NAME
                        FROM SERVICE_FEES SF, FEES F
                       WHERE SF.FEE_ID = F.FEE_ID
                         AND SF.TERMINAL_ID = l_prev_rec.TERMINAL_ID
                         AND l_close_date BETWEEN NVL(SF.START_DATE, MIN_DATE)
                              AND NVL(SF.END_DATE, MAX_DATE)
                         AND SF.FREQUENCY_ID = 6;
                    END IF;
                END LOOP;
                IF NOT l_adjusted_new_cb AND l_customer_bank_id IS NOT NULL THEN
                    l_batch_id := GET_OR_CREATE_BATCH(l_terminal_id,
                        l_customer_bank_id, l_close_date, l_currency_id, 'A');
                    INSERT_TRANS_TO_LEDGER(l_trans_id, l_trans_type_id,
                        l_close_date, l_settle_date, l_total_amount,
                        l_settle_state_id, l_process_fee_id, l_batch_id);
                END IF;
            END;
        ELSE -- an open batch exists
            -- To make it easy let's just delete what's there and re-insert
            --DELETE FROM LEDGER WHERE TRANS_ID = l_trans_id;
            UPDATE LEDGER SET DELETED = 'Y' WHERE TRANS_ID = l_trans_id;
            -- If terminal is null then no need to do anything more
            -- Added: if cust bank is null don't do any more (we may need to change this in the future which would also entail making BATCH.CUSTOMER_BANK_ID nullable)
            IF l_terminal_id IS NOT NULL AND l_customer_bank_id IS NOT NULL THEN
                INSERT_TRANS_TO_LEDGER(l_trans_id, l_trans_type_id, l_close_date, l_settle_date,
                    l_total_amount, l_settle_state_id, l_process_fee_id, l_terminal_id, l_customer_bank_id, l_currency_id);
            END IF;
        END IF;
    END;
    
    -- updates any fill-to-fill batch records upon the insertion of a record
    -- into the FILL table. Updates to the FILL table are NOT handled correctly
    -- by this procedure!
    PROCEDURE UPDATE_FILL_BATCH(
        l_fill_id   FILL.FILL_ID%TYPE)
    IS
        CURSOR l_batch_cur IS
            SELECT B.BATCH_ID, B.TERMINAL_ID, D.CUSTOMER_BANK_ID, D.CURRENCY_ID,
                   B.START_DATE, CASE WHEN BCOT.BATCH_CONFIRM_OPTION_TYPE_ID IS NULL THEN  'N' ELSE 'Y' END COUNTER_MUST_SHOW
              FROM CORP.BATCH B
              JOIN REPORT.TERMINAL_EPORT TE ON B.TERMINAL_ID = TE.TERMINAL_ID
              JOIN REPORT.FILL F ON TE.EPORT_ID = F.EPORT_ID
               AND F.FILL_DATE >= NVL(te.START_DATE, MIN_DATE)
               AND F.FILL_DATE < NVL(te.END_DATE, MAX_DATE)
              JOIN CORP.DOC D ON B.DOC_ID = D.DOC_ID
              JOIN REPORT.TERMINAL T ON B.TERMINAL_ID = T.TERMINAL_ID
              LEFT OUTER JOIN (CORP.BATCH_CONFIRM BC
              JOIN CORP.BATCH_CONFIRM_OPTION BCO
                ON BC.BATCH_CONFIRM_ID = BCO.BATCH_CONFIRM_ID
              JOIN CORP.BATCH_CONFIRM_OPTION_TYPE BCOT ON BCO.BATCH_CONFIRM_OPTION_TYPE_ID = BCOT.BATCH_CONFIRM_OPTION_TYPE_ID
               AND BCOT.BATCH_CONFIRM_OPTION_TYPE_CD = 'COUNTER_MUST_SHOW'
              ) ON BC.CUSTOMER_ID = T.CUSTOMER_ID
               AND BC.PAYMENT_SCHEDULE_ID = B.PAYMENT_SCHEDULE_ID
             WHERE B.START_DATE < F.FILL_DATE
               AND NVL(B.END_DATE, MAX_DATE) > F.FILL_DATE
               AND B.BATCH_STATE_CD = 'O'
               AND B.PAYMENT_SCHEDULE_ID = 2
               AND (F.COUNTERS_DISPLAYED_FLAG = 'Y' OR BCOT.BATCH_CONFIRM_OPTION_TYPE_ID IS NULL)
               AND F.FILL_ID = l_fill_id;
        l_end_date CORP.BATCH.END_DATE%TYPE;
        l_end_fill_id CORP.BATCH_FILL.END_FILL_ID%TYPE;
        l_lock VARCHAR2(128);
    BEGIN
        FOR l_batch_rec IN l_batch_cur LOOP
            SELECT MIN(FILL_DATE), MIN(FILL_ID)
                  INTO l_end_date, l_end_fill_id
                  FROM (
                    SELECT f.FILL_DATE, f.FILL_ID
                      FROM REPORT.FILL f, REPORT.TERMINAL_EPORT te
                     WHERE f.FILL_DATE > l_batch_rec.START_DATE
                       AND f.EPORT_ID = te.EPORT_ID
                       AND f.FILL_DATE >= NVL(te.START_DATE, MIN_DATE)
                       AND f.FILL_DATE < NVL(te.END_DATE, MAX_DATE)
                       AND te.TERMINAL_ID = l_batch_rec.TERMINAL_ID
                       AND (F.COUNTERS_DISPLAYED_FLAG = 'Y' OR l_batch_rec.COUNTER_MUST_SHOW = 'N')
                     ORDER BY f.FILL_DATE ASC)
                 WHERE ROWNUM = 1;
            l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_batch_rec.CUSTOMER_BANK_ID);
            -- it doesn't matter if the batch's doc has changed, because we assume the batch is in an open doc
            -- update batch record
            UPDATE CORP.BATCH B SET B.END_DATE = l_end_date
             WHERE B.BATCH_ID = l_batch_rec.BATCH_ID;
            UPDATE CORP.BATCH_FILL BF SET BF.END_FILL_ID = l_end_fill_id
             WHERE BF.BATCH_ID = l_batch_rec.BATCH_ID;

            -- also update ledger.batch_id of entries that are after fill_date
            DECLARE
                CURSOR l_cur IS
                  SELECT L.LEDGER_ID, L.ENTRY_DATE
                    FROM LEDGER L
                   WHERE L.BATCH_ID = l_batch_rec.BATCH_ID
                     AND L.ENTRY_DATE >= l_end_date;
                TYPE t_rec_list IS TABLE OF l_cur%ROWTYPE;
                l_recs t_rec_list;
                l_new_batch_id LEDGER.BATCH_ID%TYPE;
            BEGIN
                OPEN l_cur;
                FETCH l_cur BULK COLLECT INTO l_recs;
                CLOSE l_cur;

                IF l_recs.FIRST IS NOT NULL THEN
                    FOR i IN l_recs.FIRST .. l_recs.LAST LOOP
                        l_new_batch_id := GET_OR_CREATE_BATCH(
                            l_batch_rec.TERMINAL_ID,
                            l_batch_rec.CUSTOMER_BANK_ID,
                            l_recs(i).ENTRY_DATE,
                            l_batch_rec.CURRENCY_ID,
                            'N');
                        UPDATE LEDGER L
                           SET L.BATCH_ID = l_new_batch_id
                         WHERE L.LEDGER_ID = l_recs(i).LEDGER_ID;
                    END LOOP;
                END IF;
            END;
            GLOBALS_PKG.RELEASE_LOCK(l_lock);
            COMMIT;
            
            -- Now check batch complete
            CHECK_FILL_BATCH_COMPLETE(l_batch_rec.BATCH_ID);
            COMMIT;
         END LOOP;
    END;
    
    PROCEDURE UPDATE_EXPORT_BATCH(
        l_export_id   REPORT.EXPORT_BATCH.BATCH_ID%TYPE)
    IS
        CURSOR l_batch_cur IS
            SELECT DISTINCT B.BATCH_ID, B.TERMINAL_ID, D.CUSTOMER_BANK_ID, D.CURRENCY_ID, B.START_DATE, EX.TRAN_CREATE_DT_END NEW_END_DATE
              FROM CORP.BATCH B
             INNER JOIN CORP.DOC D ON B.DOC_ID = D.DOC_ID
             INNER JOIN CORP.CUSTOMER_BANK CB ON D.CUSTOMER_BANK_ID = CB.CUSTOMER_BANK_ID
             /*
             INNER JOIN REPORT.TERMINAL TERMINAL_ID ON B.TERMINAL_ID = T.TERMINAL_ID
             INNER JOIN FRONT.VW_LOCATION_HIERARCHY H ON T.LOCATION_ID = H.DESCENDANT_LOCATION_ID
             */
             INNER JOIN REPORT.EXPORT_BATCH EX ON CB.CUSTOMER_ID = EX.CUSTOMER_ID /* AND EX.LOCATION_ID = H.ANCESTOR_LOCATION_ID */
             WHERE B.BATCH_STATE_CD = 'O'
               AND B.END_DATE IS NULL
               AND B.PAYMENT_SCHEDULE_ID = 8
               AND EX.BATCH_ID = l_export_id;
        l_lock VARCHAR2(128);
    BEGIN
        FOR l_batch_rec IN l_batch_cur LOOP
            l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_batch_rec.CUSTOMER_BANK_ID);
            -- it doesn't matter if the batch's doc has changed, because we assume the batch is in an open doc
            -- update batch record
            UPDATE BATCH B
               SET B.END_DATE = l_batch_rec.new_end_date,
                   B.BATCH_STATE_CD = (SELECT DECODE(COUNT(*), 0, 'D', 'U')
                      FROM REPORT.TERMINAL t
                      JOIN CORP.BATCH_CONFIRM CN ON t.CUSTOMER_ID = cn.CUSTOMER_ID
                     WHERE b.TERMINAL_ID = t.TERMINAL_ID
                       AND b.PAYMENT_SCHEDULE_ID = cn.PAYMENT_SCHEDULE_ID),
                   B.BATCH_CLOSED_TS = SYSDATE
             WHERE B.BATCH_ID = l_batch_rec.BATCH_ID;

            -- also update ledger.batch_id of entries that are not exported
            DECLARE
                CURSOR l_cur IS
                  SELECT L.LEDGER_ID, L.ENTRY_DATE
                    FROM LEDGER L
                   WHERE L.BATCH_ID = l_batch_rec.BATCH_ID
                     AND (L.ENTRY_DATE > l_batch_rec.new_end_date
                         OR NOT EXISTS(SELECT 1 FROM REPORT.ACTIVITY_REF A
                              INNER JOIN REPORT.EXPORT_BATCH EB ON A.BATCH_ID = EB.BATCH_ID
                              WHERE L.TRANS_ID = A.TRAN_ID AND EB.STATUS IN('A','S')));
                TYPE t_rec_list IS TABLE OF l_cur%ROWTYPE;
                l_recs t_rec_list;
                l_new_batch_id LEDGER.BATCH_ID%TYPE;
            BEGIN
                OPEN l_cur;
                FETCH l_cur BULK COLLECT INTO l_recs;
                CLOSE l_cur;

                IF l_recs.FIRST IS NOT NULL THEN
                    FOR i IN l_recs.FIRST .. l_recs.LAST LOOP
                        l_new_batch_id := GET_OR_CREATE_BATCH(
                            l_batch_rec.TERMINAL_ID,
                            l_batch_rec.CUSTOMER_BANK_ID,
                            l_recs(i).ENTRY_DATE,
                            l_batch_rec.CURRENCY_ID,
                            'N');
                        UPDATE LEDGER L
                           SET L.BATCH_ID = l_new_batch_id
                         WHERE L.LEDGER_ID = l_recs(i).LEDGER_ID;
                    END LOOP;
                END IF;
            END;
         END LOOP;
    END;
/* not doing this yet
    PROCEDURE AUTO_CREATE_DOCS.
    IS
       CURSOR c_docs IS
             SELECT cb.customer_bank_id
              FROM (SELECT l.CUSTOMER_BANK_ID, l.TRANS_TYPE_ID, SUM(l.TOTAL_AMOUNT) TOTAL_AMOUNT,
                           SUM(l.TOTAL_AMOUNT * l.FEE_PERCENT + l.FEE_AMOUNT) FEE_TOTAL
                      FROM LEDGER l, TERMINAL t
                     WHERE l.PAID_STATUS = 'U'
                       AND l.TERMINAL_ID = t.TERMINAL_ID
                       AND t.PAYMENT_SCHEDULE_ID <> 1
                     GROUP BY l.CUSTOMER_BANK_ID, l.TRANS_TYPE_ID) lp,
                     CUSTOMER_BANK cb
              WHERE cb.CUSTOMER_BANK_ID = lp.CUSTOMER_BANK_ID
              GROUP BY cb.CUSTOMER_BANK_ID, cb.PAY_MIN_AMOUNT
              HAVING SUM(CASE TRANS_TYPE_ID
                      WHEN 16 THEN 1
                      WHEN 19 THEN 1
                      WHEN 20 THEN -1
                      WHEN 21 THEN -1
                      ELSE NULL
                   END * TOTAL_AMOUNT) - SUM (lp.FEE_TOTAL) > cb.PAY_MIN_AMOUNT;
    BEGIN
        -- determine each customer bank that whose total eft will exceed the min_eft_amt
        FOR r_docs IN c_docs LOOP
            BEGIN
                CREATE_DOC(r_efts.customer_bank_id, NULL, l_type,'Y');
                COMMIT;
            EXCEPTION
                WHEN OTHERS THEN
                    IF SQLCODE = -20910 THEN
                        ROLLBACK;
                    ELSE
                        RAISE;
                    END IF;
            END;
        END LOOP;
    END;

*/

    FUNCTION GET_UNPAID_TRANS_AND_FEES (
        l_as_of DATE
    ) RETURN GLOBALS_PKG.REF_CURSOR
    IS
        cur GLOBALS_PKG.REF_CURSOR;
    BEGIN
       OPEN cur FOR
            SELECT CUSTOMER_NAME, CB.CUSTOMER_ID, CB.CUSTOMER_BANK_ID,
                   BANK_ACCT_NBR, BANK_ROUTING_NBR, PAY_MIN_AMOUNT, CREDIT_AMOUNT,
                   REFUND_AMOUNT, CHARGEBACK_AMOUNT, PROCESS_FEE_AMOUNT,
                   SERVICE_FEE_AMOUNT, ADJUST_AMOUNT
                   FROM CUSTOMER C, CUSTOMER_BANK CB, (
                SELECT CUSTOMER_BANK_ID,
                       SUM(DECODE(ENTRY_TYPE, 'CC', AMOUNT, NULL)) CREDIT_AMOUNT,
                       SUM(DECODE(ENTRY_TYPE, 'RF', AMOUNT, NULL)) REFUND_AMOUNT,
                       SUM(DECODE(ENTRY_TYPE, 'CB', AMOUNT, NULL)) CHARGEBACK_AMOUNT,
                       SUM(DECODE(ENTRY_TYPE, 'PF', AMOUNT, NULL)) PROCESS_FEE_AMOUNT,
                       SUM(DECODE(ENTRY_TYPE, 'SF', AMOUNT, NULL)) SERVICE_FEE_AMOUNT,
                       SUM(DECODE(ENTRY_TYPE, 'AD', AMOUNT, NULL)) ADJUST_AMOUNT
                  FROM (
                    SELECT D.CUSTOMER_BANK_ID, L.ENTRY_TYPE, SUM(L.AMOUNT) AMOUNT
                      FROM LEDGER L, BATCH B, DOC D
                      WHERE L.BATCH_ID = B.BATCH_ID
                      AND L.DELETED = 'N'
                      AND PAYMENTS_PKG.ENTRY_PAYABLE(L.SETTLE_STATE_ID,L.ENTRY_TYPE) = 'Y'
                      AND B.DOC_ID = D.DOC_ID
                      AND NVL(L.PAID_DATE, MIN_DATE) < l_as_of
                      AND L.LEDGER_DATE < l_as_of
                      GROUP BY D.CUSTOMER_BANK_ID, L.ENTRY_TYPE) M
                  GROUP BY CUSTOMER_BANK_ID) A
            WHERE CB.CUSTOMER_BANK_ID = A.CUSTOMER_BANK_ID (+)
            AND CB.CUSTOMER_ID = C.CUSTOMER_ID
            ORDER BY UPPER(CUSTOMER_NAME), BANK_ACCT_NBR;
        RETURN cur;
    END;
    
    FUNCTION FORMAT_EFT_REASON(
        l_payment_sched_id BATCH.PAYMENT_SCHEDULE_ID%TYPE,
        l_min_pay_date BATCH.START_DATE%TYPE,
        l_max_pay_date BATCH.END_DATE%TYPE
     ) RETURN VARCHAR
    IS
        l_text VARCHAR(8000);
    BEGIN
        SELECT DESCRIPTION
          INTO l_text
          FROM PAYMENT_SCHEDULE
         WHERE PAYMENT_SCHEDULE_ID = l_payment_sched_id;
        IF l_min_pay_date = l_max_pay_date THEN
            l_text := l_text || ' on ' || TO_CHAR(l_min_pay_date, 'MM-DD-YYYY');
        ELSE
            l_text := l_text || ' from ' || TO_CHAR(l_min_pay_date, 'MM-DD-YYYY')
                || ' to ' || TO_CHAR(l_max_pay_date, 'MM-DD-YYYY');
        END IF;
        RETURN l_text;
    END;
    /*
    FUNCTION FORMAT_EFT_REASON(
        l_eft_id EFT.EFT_ID%TYPE
     ) RETURN VARCHAR
    IS
        l_text VARCHAR(8000) := '';
        CURSOR l_cur IS
            SELECT PAYMENT_SCHEDULE_ID, MIN(PAYMENT_SCHEDULE_DATE) MIN_PAY_DATE,
                   MAX(PAYMENT_SCHEDULE_DATE) MAX_PAY_DATE
            FROM PAYMENTS P
            WHERE P.EFT_ID = l_eft_id
            GROUP BY PAYMENT_SCHEDULE_ID;
    BEGIN
        FOR l_rec IN l_cur LOOP
            IF LENGTH(l_text) > 0 THEN
                l_text := l_text || ' and ';
            END IF;
            l_text := l_text || FORMAT_EFT_REASON(l_rec.PAYMENT_SCHEDULE_ID, l_rec.MIN_PAY_DATE, l_rec.MAX_PAY_DATE);
        END LOOP;
        RETURN l_text;
    END;
    */

    -- Puts ledger record into a new batch
    PROCEDURE      DELAY_ENTRY (
      l_ledger_id IN LEDGER.LEDGER_ID%TYPE
      )
    IS
      l_status DOC.STATUS%TYPE;
      l_batch_id LEDGER.BATCH_ID%TYPE;
      l_entry_date LEDGER.ENTRY_DATE%TYPE;
      l_new_batch_id LEDGER.BATCH_ID%TYPE;
    BEGIN
        l_status := FREEZE_DOC_STATUS_LEDGER_ID(l_ledger_id);
        IF l_status IN('O') THEN
    		 RAISE_APPLICATION_ERROR(-20101, 'The Document must be locked before this entry can be pulled out of it');
        ELSIF l_status NOT IN('L') THEN
             RAISE_APPLICATION_ERROR(-20142, 'This entry belongs to a document that is already approved.');
        END IF;
        -- put in new batch
        SELECT L.BATCH_ID, L.ENTRY_DATE
          INTO l_batch_id, l_entry_date
          FROM LEDGER L
         WHERE LEDGER_ID = l_ledger_id;
        l_new_batch_id := GET_NEW_BATCH(l_batch_id, l_entry_date);
        UPDATE LEDGER L
           SET BATCH_ID = l_new_batch_id
         WHERE LEDGER_ID = l_ledger_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
    		 RAISE_APPLICATION_ERROR(-20100, 'Entry not found');
    	WHEN OTHERS THEN
    		 RAISE;
    END;
    
    -- Marks ledger record as deleted
    PROCEDURE      DELETE_ENTRY (
      l_ledger_id IN LEDGER.LEDGER_ID%TYPE)
    IS
      l_status DOC.STATUS%TYPE;
    BEGIN
        l_status := FREEZE_DOC_STATUS_LEDGER_ID(l_ledger_id);
        IF l_status IN('O') THEN
    		 NULL; --RAISE_APPLICATION_ERROR(-20101, 'The Document must be locked before this entry can be pulled out of it');
        ELSIF l_status NOT IN('L') THEN
             RAISE_APPLICATION_ERROR(-20142, 'This entry belongs to a document that is already approved.');
        END IF;
        -- mark as deleted (remove net revenue fees also)
        UPDATE LEDGER L
           SET DELETED = 'Y'
         WHERE LEDGER_ID = l_ledger_id
            OR RELATED_LEDGER_ID = l_ledger_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
    		 RAISE_APPLICATION_ERROR(-20100, 'Entry not found');
    	WHEN OTHERS THEN
    		 RAISE;
    END;

    -- Puts refund into a new batch
    PROCEDURE      DELAY_REFUND_BY_TRAN_ID (
      l_trans_id IN LEDGER.TRANS_ID%TYPE)
    IS
      l_ledger_id LEDGER.LEDGER_ID%TYPE;
    BEGIN
    	 SELECT LEDGER_ID
           INTO l_ledger_id
           FROM LEDGER
          WHERE TRANS_ID = l_trans_id
            AND ENTRY_TYPE = 'RF';
          DELAY_ENTRY(l_ledger_id);
    END;
    
    PROCEDURE                             ADJUSTMENT_INS
       (l_ledger_id OUT NOCOPY LEDGER.LEDGER_ID%TYPE,
       	l_doc_id IN BATCH.DOC_ID%TYPE,
        l_terminal_id IN BATCH.TERMINAL_ID%TYPE,
    	l_reason IN LEDGER.DESCRIPTION%TYPE,
    	l_amt IN LEDGER.AMOUNT%TYPE,
    	l_user_id IN LEDGER.CREATE_BY%TYPE)
    IS
        l_batch_id LEDGER.BATCH_ID%TYPE;
        l_curr_id DOC.CURRENCY_ID%TYPE;
        l_term_curr_id DOC.CURRENCY_ID%TYPE;
        l_fee_date LEDGER.ENTRY_DATE%TYPE;
        l_customer_bank_id CORP.DOC.CUSTOMER_BANK_ID%TYPE;
        l_status DOC.STATUS%TYPE;
        l_lock VARCHAR2(128);
    BEGIN
        SELECT D.CUSTOMER_BANK_ID, D.CURRENCY_ID
          INTO l_customer_bank_id, l_curr_id
          FROM CORP.DOC D
         WHERE D.DOC_ID = l_doc_id;
		 
		IF l_terminal_id IS NOT NULL THEN
			SELECT FEE_CURRENCY_ID
			INTO l_term_curr_id
			FROM REPORT.TERMINAL
			WHERE TERMINAL_ID = l_terminal_id;
			
            -- ensure that currency matches
            IF NVL(l_curr_id, 0) <> l_term_curr_id THEN
                RAISE_APPLICATION_ERROR(-20020, 'Terminal uses a different currency than the doc''s currency. Adjustment not added');
            END IF;
		END IF;
		 
        l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_customer_bank_id);
        l_status := GET_DOC_STATUS(l_doc_id);
        IF l_status NOT IN('O', 'L') THEN
            RAISE_APPLICATION_ERROR(-20021, 'This doc must be open or locked to add adjustments. Adjustment not added');
    	END IF;
		
		SELECT MAX(B.BATCH_ID)
		  INTO l_batch_id
		  FROM BATCH B
		 WHERE B.DOC_ID = l_doc_id
		   AND NVL(B.TERMINAL_ID, 0) = NVL(l_terminal_id, 0)
		   AND B.PAYMENT_SCHEDULE_ID = 1;
    
        IF l_batch_id IS NULL THEN
            -- create a batch and set the doc id
            SELECT BATCH_SEQ.NEXTVAL
              INTO l_batch_id
              FROM DUAL;
            INSERT INTO BATCH(
                BATCH_ID,
                DOC_ID,
                TERMINAL_ID,
                PAYMENT_SCHEDULE_ID,
                START_DATE,
                END_DATE,
                BATCH_STATE_CD)
              VALUES(
                l_batch_id,
                l_doc_id,
                DECODE(l_terminal_id, 0, NULL, l_terminal_id),
                1,
                SYSDATE,
                SYSDATE,
                DECODE(l_status, 'L', 'F', 'O', 'L'));
        END IF;
    	SELECT LEDGER_SEQ.NEXTVAL, SYSDATE
          INTO l_ledger_id, l_fee_date
          FROM DUAL;
    	INSERT INTO LEDGER(
            LEDGER_ID,
            ENTRY_TYPE,
            AMOUNT,
            ENTRY_DATE,
            BATCH_ID,
            SETTLE_STATE_ID,
            LEDGER_DATE,
            DESCRIPTION,
            CREATE_BY)
           VALUES (
            l_ledger_id,
            'AD',
            l_amt,
            l_fee_date,
            l_batch_id,
            2 /*process-no require*/,
            l_fee_date,
            l_reason,
            l_user_id);
        -- create net revenue fee also
    	INSERT INTO LEDGER(
            LEDGER_ID,
            RELATED_LEDGER_ID,
            SERVICE_FEE_ID,
            ENTRY_TYPE,
            AMOUNT,
            ENTRY_DATE,
            BATCH_ID,
            SETTLE_STATE_ID,
            LEDGER_DATE,
            DESCRIPTION)
         SELECT
            LEDGER_SEQ.NEXTVAL,
            l_ledger_id,
            SF.SERVICE_FEE_ID,
            'SF',
            -l_amt * SF.FEE_PERCENT,
            l_fee_date,
            l_batch_id,
            2 /*process-no require*/,
            l_fee_date,
            F.FEE_NAME
          FROM SERVICE_FEES SF, FEES F
         WHERE SF.FEE_ID = F.FEE_ID
           AND SF.TERMINAL_ID = l_terminal_id
           AND l_fee_date BETWEEN NVL(SF.START_DATE, MIN_DATE)
                  AND NVL(SF.END_DATE, MAX_DATE)
           AND SF.FREQUENCY_ID = 6;
    END;
    
    PROCEDURE                             ADJUSTMENT_UPD
       (l_ledger_id IN LEDGER.LEDGER_ID%TYPE,
       	l_reason IN LEDGER.DESCRIPTION%TYPE,
       	l_amt IN LEDGER.AMOUNT%TYPE,
    	l_user_id IN LEDGER.CREATE_BY%TYPE)
    IS
        l_status CORP.DOC.STATUS%TYPE;
    BEGIN
  	    l_status := FREEZE_DOC_STATUS_LEDGER_ID(l_ledger_id);
        IF l_status NOT IN('O', 'L') THEN
            RAISE_APPLICATION_ERROR(-20021, 'This doc must be open or locked to update adjustments. Adjustment not updated');
    	END IF;
        UPDATE LEDGER SET DESCRIPTION = l_reason, AMOUNT = l_amt, CREATE_BY = l_user_id
          WHERE LEDGER_ID = l_ledger_id;
         -- update net revenue
         UPDATE LEDGER L SET AMOUNT = -l_amt * (
            SELECT SF.FEE_PERCENT
              FROM SERVICE_FEES SF
             WHERE L.SERVICE_FEE_ID = SF.SERVICE_FEE_ID
               AND SF.FREQUENCY_ID = 6)
          WHERE L.RELATED_LEDGER_ID = l_ledger_id
            AND L.ENTRY_TYPE = 'SF'
            AND EXISTS(SELECT 1
                FROM SERVICE_FEES SF
                WHERE L.SERVICE_FEE_ID = SF.SERVICE_FEE_ID
                AND SF.FREQUENCY_ID = 6
            );
    END;

    PROCEDURE LOCK_DOC(
        l_doc_id DOC.DOC_ID%TYPE,
        l_user_id IN DOC.CREATE_BY%TYPE)
    IS
    BEGIN
        IF FREEZE_DOC_STATUS_DOC_ID(l_doc_id) = 'O' THEN
            UPDATE DOC
               SET STATUS = 'L', UPDATE_DATE = SYSDATE
             WHERE DOC_ID = l_doc_id;

            --Close all closeable batches
            UPDATE BATCH B
               SET B.BATCH_STATE_CD = 'F',
                   B.BATCH_CLOSED_TS = SYSDATE,
                   B.END_DATE = NVL(B.END_DATE, SYSDATE)
             WHERE B.DOC_ID = l_doc_id
               AND B.BATCH_STATE_CD = 'L';

            UPDATE BATCH B
               SET B.BATCH_STATE_CD = (SELECT DECODE(COUNT(*), 0, 'D', 'U')
                      FROM REPORT.TERMINAL t
                      JOIN CORP.BATCH_CONFIRM CN ON t.CUSTOMER_ID = cn.CUSTOMER_ID
                     WHERE b.TERMINAL_ID = t.TERMINAL_ID
                       AND b.PAYMENT_SCHEDULE_ID = cn.PAYMENT_SCHEDULE_ID),
                   B.BATCH_CLOSED_TS = SYSDATE
             WHERE B.DOC_ID = l_doc_id
               AND B.BATCH_STATE_CD = 'O'
               AND B.PAYMENT_SCHEDULE_ID NOT IN(1,2,5,7,8)
               AND B.END_DATE <= SYSDATE;

            -- Remove unclosed batches
            DECLARE
                l_cnt PLS_INTEGER;
                l_cb_id DOC.CUSTOMER_BANK_ID%TYPE;
                l_cur_id DOC.CURRENCY_ID%TYPE;
                l_bu_id DOC.BUSINESS_UNIT_ID%TYPE;
                l_new_doc_id DOC.DOC_ID%TYPE;
            BEGIN
                SELECT COUNT(*)
                  INTO l_cnt
                  FROM BATCH B
                 WHERE B.DOC_ID = l_doc_id
                   AND BATCH_CLOSABLE(B.BATCH_ID, B.PAYMENT_SCHEDULE_ID, B.END_DATE, B.BATCH_STATE_CD) = 'N';
                IF l_cnt > 0 THEN
                    SELECT D.CUSTOMER_BANK_ID, D.CURRENCY_ID, D.BUSINESS_UNIT_ID
                      INTO l_cb_id, l_cur_id, l_bu_id
                      FROM DOC D
                    WHERE D.DOC_ID = l_doc_id;
                    l_new_doc_id := GET_OR_CREATE_DOC(l_cb_id, l_cur_id, l_bu_id);
                   UPDATE BATCH B
                       SET B.DOC_ID = l_new_doc_id
                     WHERE B.DOC_ID = l_doc_id
                       AND BATCH_CLOSABLE(B.BATCH_ID, B.PAYMENT_SCHEDULE_ID, B.END_DATE, B.BATCH_STATE_CD) = 'N';
                END IF;
            END;
            
            -- Remove any unsettled trans from as batches
            DECLARE
                CURSOR l_cur IS
                  SELECT L.LEDGER_ID, L.BATCH_ID, L.ENTRY_DATE
                    FROM LEDGER L
                   WHERE ENTRY_PAYABLE(L.SETTLE_STATE_ID, L.ENTRY_TYPE) = '?'
                     AND L.BATCH_ID IN(
                            SELECT B.BATCH_ID
                              FROM BATCH B
                             WHERE B.DOC_ID = l_doc_id);
                TYPE t_rec_list IS TABLE OF l_cur%ROWTYPE;
                l_recs t_rec_list;
                l_new_batch_id LEDGER.BATCH_ID%TYPE;
            BEGIN
                OPEN l_cur;
                FETCH l_cur BULK COLLECT INTO l_recs;
                CLOSE l_cur;

                IF l_recs.FIRST IS NOT NULL THEN
                    FOR i IN l_recs.FIRST .. l_recs.LAST LOOP
                        l_new_batch_id := GET_NEW_BATCH(l_recs(i).BATCH_ID, l_recs(i).ENTRY_DATE);
                        UPDATE LEDGER L
                           SET L.BATCH_ID = l_new_batch_id
                         WHERE L.LEDGER_ID = l_recs(i).LEDGER_ID;
                    END LOOP;
                END IF;
            END;

            -- add rounding entries
            ADD_BATCH_ROUNDING_ENTRIES(l_doc_id);
            ADD_ROUNDING_ENTRY(l_doc_id);
        END IF;
    END;

    PROCEDURE      APPROVE_PAYMENT (
      l_doc_id IN DOC.DOC_ID%TYPE,
      l_user_id IN DOC.APPROVE_BY%TYPE)
    IS
      l_total DOC.TOTAL_AMOUNT%TYPE;
    BEGIN
        IF FREEZE_DOC_STATUS_DOC_ID(l_doc_id) = 'L' THEN
            -- add Positive Net Revenue Adjustment, if necessary
            DECLARE
                l_nrf_amt LEDGER.AMOUNT%TYPE;
                l_fee_date LEDGER.ENTRY_DATE%TYPE;
                l_ledger_id LEDGER.LEDGER_ID%TYPE;
                l_batch_id BATCH.BATCH_ID%TYPE;
            BEGIN
                SELECT NVL(SUM(AMOUNT), 0), SYSDATE
                  INTO l_nrf_amt, l_fee_date
                  FROM LEDGER L, BATCH B, SERVICE_FEES SF
                 WHERE L.BATCH_ID = B.BATCH_ID
                   AND B.DOC_ID = l_doc_id
                   AND L.DELETED = 'N'
                   AND L.ENTRY_TYPE = 'SF'
                   AND L.SERVICE_FEE_ID = SF.SERVICE_FEE_ID
                   AND SF.FREQUENCY_ID = 6;
                 IF l_nrf_amt > 0 THEN
                    -- l_nrf_amt is positive (money to the customer), so back it out with an adjustment
                    SELECT MAX(B.BATCH_ID)
                      INTO l_batch_id
                      FROM BATCH B
                     WHERE B.DOC_ID = l_doc_id
                       AND B.PAYMENT_SCHEDULE_ID = 7;
                    IF l_batch_id IS NULL THEN
                	    SELECT BATCH_SEQ.NEXTVAL
                          INTO l_batch_id
                          FROM DUAL;
                        INSERT INTO BATCH(
                            BATCH_ID,
                            DOC_ID,
                            TERMINAL_ID,
                            PAYMENT_SCHEDULE_ID,
                            START_DATE,
                            END_DATE,
                            BATCH_STATE_CD)
                          VALUES(
                            l_batch_id,
                            l_doc_id,
                            NULL,
                            7,
                            l_fee_date,
                            l_fee_date,
                            'F');
                    END IF;
                	SELECT LEDGER_SEQ.NEXTVAL
                      INTO l_ledger_id
                      FROM DUAL;
                	INSERT INTO LEDGER(
                        LEDGER_ID,
                        ENTRY_TYPE,
                        AMOUNT,
                        ENTRY_DATE,
                        BATCH_ID,
                        SETTLE_STATE_ID,
                        LEDGER_DATE,
                        DESCRIPTION,
                        CREATE_BY)
                       VALUES (
                        l_ledger_id,
                        'AD',
                        -l_nrf_amt,
                        l_fee_date,
                        l_batch_id,
                        2 /*process-no require*/,
                        l_fee_date,
                        'Net Revenue Hurdle Not Met',
                        l_user_id);
                 END IF;
            END;
            ADD_ROUNDING_ENTRY(l_doc_id);
            SELECT SUM(AMOUNT)
              INTO l_total
              FROM LEDGER L, BATCH B
             WHERE L.DELETED = 'N'
               AND ENTRY_PAYABLE(L.SETTLE_STATE_ID,L.ENTRY_TYPE) = 'Y'
               AND L.BATCH_ID = B.BATCH_ID
               AND B.DOC_ID = l_doc_id;

        	UPDATE DOC D
               SET APPROVE_BY = l_user_id,
                   UPDATE_DATE = SYSDATE,
                   STATUS = 'A',
                   TOTAL_AMOUNT = l_total,
                   DOC_TYPE = (
                        SELECT DECODE(CB.IS_EFT, 'Y', 'E', 'P') -- electronic or paper
                               || CASE WHEN l_total > 0 THEN 'C' ELSE 'D' END -- credit or debit
                          FROM CUSTOMER_BANK CB
                         WHERE D.CUSTOMER_BANK_ID = CB.CUSTOMER_BANK_ID)
      	 		WHERE DOC_ID = l_doc_id;
          END IF;
    END;
    
    PROCEDURE      UNAPPROVE_PAYMENT (
      l_doc_id IN DOC.DOC_ID%TYPE)
    IS
    BEGIN
        UPDATE DOC D SET APPROVE_BY = NULL, UPDATE_DATE = SYSDATE, STATUS = 'L',
            TOTAL_AMOUNT = NULL, DOC_TYPE = '--'
  	 		WHERE DOC_ID = l_doc_id
              AND STATUS = 'A';
        IF SQL%FOUND THEN
            -- Remove adjustments for positive net revenue fees
            UPDATE LEDGER
               SET DELETED = 'Y'
             WHERE ENTRY_TYPE = 'AD'
               AND TRANS_ID IS NULL
               AND BATCH_ID IN(
                    SELECT BATCH_ID
                      FROM BATCH
                     WHERE DOC_ID = l_doc_id
                       AND PAYMENT_SCHEDULE_ID = 7);
        END IF;
    END;
    
    PROCEDURE      UPLOAD_TO_ORF (
      l_doc_id IN DOC.DOC_ID%TYPE)
    IS
    BEGIN
    	 INSERT INTO USAT_INVOICE_INTERFACE@FINANCIALS(
            VENDOR_NAME_I, --                                        VARCHAR2 (240)                        REQUIRED     vendor name
            INVOICE_NUMBER_I, --                                    VARCHAR2 (240)                       REQUIRED      invoice number
            INVOICE_DATE_I, --                                           DATE                                           OPTIONAL
            INVOICE_AMOUNT_I, --                                    NUMBER                                     REQUIRED      amount of invoice
            DISTRIBUTION_SET_NAME_I, --                     VARCHAR2 (240)                       REQUIRED
            GL_DATE_I, --                                                      DATE                                            OPTIONAL
            PAY_GROUP_LOOKUP_CODE_I, --                 VARCHAR2 (240)                        REQUIRED     pay group
            VENDOR_SITE_CODE_I, --                                VARCHAR2 (25)                          REQUIRED     site code
            INVOICE_CURRENCY_CODE_I, --                  VARCHAR2 (15)                           REQUIRED     invoice currency
            PAYMENT_METHOD_LOOKUP_CODE_I, --   VARCHAR2 (25)                         REQUIRED     payment method
            VENDOR_NUMBER_I) --                                    VARCHAR2 (200)                        OPTIONAL
        SELECT
            C.CUSTOMER_NAME,
            D.REF_NBR,
            D.SENT_DATE,
            D.TOTAL_AMOUNT,
            BU.DISTRIBUTION_SET_CD,
            NVL((SELECT MAX(B.END_DATE-1)
                  FROM CORP.BATCH B
                 WHERE D.DOC_ID = B.DOC_ID
                   AND B.PAYMENT_SCHEDULE_ID IN(3,4,6)), D.SENT_DATE),
            BU.PAY_GROUP_CD,
            CB.CUSTOMER_BANK_ID,
            CU.CURRENCY_CODE,
            DECODE(CB.IS_EFT, 'Y', 'EFT', 'CHECK'),
            C.CUSTOMER_ID
        FROM CORP.CUSTOMER C, CORP.CUSTOMER_BANK CB, CORP.DOC D, CORP.BUSINESS_UNIT BU, CORP.CURRENCY CU
        WHERE D.DOC_ID = l_doc_id
        AND D.CUSTOMER_BANK_ID = CB.CUSTOMER_BANK_ID
        AND CB.CUSTOMER_ID = C.CUSTOMER_ID
        AND D.CURRENCY_ID = CU.CURRENCY_ID
        AND D.BUSINESS_UNIT_ID = BU.BUSINESS_UNIT_ID;
    END;
    
    PROCEDURE      MARK_DOC_PAID (
      l_doc_id IN DOC.DOC_ID%TYPE,
      l_user_id IN DOC.APPROVE_BY%TYPE)
    IS
        l_paid_date DATE := SYSDATE;
        l_total CORP.DOC.TOTAL_AMOUNT%TYPE;
    BEGIN
    	 UPDATE DOC SET SENT_BY = l_user_id, UPDATE_DATE = l_paid_date, STATUS = 'P',
                SENT_DATE = l_paid_date, TOTAL_AMOUNT = NVL(l_total, TOTAL_AMOUNT)
          WHERE DOC_ID = l_doc_id
            AND STATUS = 'A';
         IF SQL%FOUND THEN
             UPDATE LEDGER SET PAID_DATE = l_paid_date
              WHERE BATCH_ID IN(SELECT BATCH_ID FROM BATCH WHERE DOC_ID = l_doc_id);
             UPDATE BATCH SET PAID_DATE = l_paid_date
              WHERE DOC_ID = l_doc_id;
             UPLOAD_TO_ORF(l_doc_id);
         END IF;
    END;
    
    PROCEDURE PROCESS_FEES_UPD
       (l_terminal_id IN PROCESS_FEES.TERMINAL_ID%TYPE,
        l_trans_type_id IN PROCESS_FEES.TRANS_TYPE_ID%TYPE,
        l_fee_percent IN PROCESS_FEES.FEE_PERCENT%TYPE,
        l_fee_amount IN PROCESS_FEES.FEE_AMOUNT%TYPE,
        l_fee_minimum IN PROCESS_FEES.MIN_AMOUNT%TYPE,
        l_effective_date IN PROCESS_FEES.END_DATE%TYPE DEFAULT SYSDATE,
        l_override IN CHAR DEFAULT 'N')
    IS
		l_lock VARCHAR2(128);
		l_current_date PROCESS_FEES.END_DATE%TYPE := SYSDATE;
		l_fee_effective_date PROCESS_FEES.END_DATE%TYPE := NVL(l_effective_date, l_current_date);
		l_fee_override CHAR := NVL(l_override, 'N');
    BEGIN
    	 --CHECK IF WE already paid on transactions before the effective date
    	 IF l_fee_override <> 'Y' AND l_fee_effective_date < l_current_date THEN
    	 	 DECLARE
    		     l_tran_id LEDGER.TRANS_ID%TYPE;
    			BEGIN
    		 	 SELECT /*+ index(T USAT_IX_TRANS_TERMINAL_ID) */ MIN(L.TRANS_ID)
                   INTO l_tran_id
                   FROM LEDGER L, BATCH B, TRANS T, DOC D
                  WHERE T.TRAN_ID = L.TRANS_ID
                    AND T.TRANS_TYPE_ID = l_trans_type_id
                    AND T.TERMINAL_ID = l_terminal_id
                    AND T.CLOSE_DATE >= l_fee_effective_date
                    AND B.TERMINAL_ID = l_terminal_id
                    AND L.BATCH_ID = B.BATCH_ID
                    AND B.DOC_ID = D.DOC_ID
                    AND L.DELETED = 'N'
                    AND D.STATUS NOT IN('O', 'D');
    			 IF l_tran_id IS NOT NULL THEN  --BIG PROBLEM
    			 	 RAISE_APPLICATION_ERROR(-20011, 'Transaction #' || TO_CHAR(l_tran_id) || ', which ocurred on the terminal whose fees you are updating, has already been paid.');
    			 END IF;
    		 END;
    	 END IF;
		 
		 l_lock := GLOBALS_PKG.REQUEST_LOCK('TERMINAL.TERMINAL_ID', l_terminal_id);
		 
    	 UPDATE PROCESS_FEES SET END_DATE = l_fee_effective_date WHERE TERMINAL_ID = l_terminal_id
    	 	AND TRANS_TYPE_ID = l_trans_type_id AND NVL(END_DATE, l_fee_effective_date) >= l_fee_effective_date;
    	 BEGIN
            DELETE FROM PROCESS_FEES WHERE TERMINAL_ID = l_terminal_id
        	 	AND TRANS_TYPE_ID = l_trans_type_id AND NVL(START_DATE, MIN_DATE) >= NVL(END_DATE, MAX_DATE);	
         EXCEPTION
            WHEN CHILD_RECORD_FOUND THEN
                NULL;
            WHEN OTHERS THEN
                RAISE;
         END;
         IF NVL(l_fee_percent, 0) <> 0 OR NVL(l_fee_amount, 0) <> 0 THEN	
        	 INSERT INTO PROCESS_FEES(PROCESS_FEE_ID, TERMINAL_ID, TRANS_TYPE_ID, FEE_PERCENT, FEE_AMOUNT, MIN_AMOUNT, START_DATE)
         	    VALUES(PROCESS_FEE_SEQ.NEXTVAL, l_terminal_id, l_trans_type_id, l_fee_percent, l_fee_amount, l_fee_minimum, l_fee_effective_date);
 	     END IF;
    END;
    
    FUNCTION GET_SERVICE_FEE_DATE_FMT(
        l_months FREQUENCY.MONTHS%TYPE,
        l_days FREQUENCY.DAYS%TYPE)
     RETURN VARCHAR
    IS
    BEGIN
        IF l_months >= 12  AND MOD(l_months, 12.0) = 0 THEN
            RETURN 'FMYYYY';
        ELSIF l_months > 0 THEN
            RETURN 'FMMonth, YYYY';
        ELSIF l_days >= 1 AND MOD(l_days, 1.0) = 0 THEN
            RETURN 'FMMM/DD/YYYY';
        ELSE
            RETURN 'FMMM/DD/YYYY HH:MI AM';
        END IF;
    END;
    
    PROCEDURE SCAN_FOR_SERVICE_FEES
    IS
        CURSOR c_fee
        IS
            SELECT SF.SERVICE_FEE_ID,
                   SF.TERMINAL_ID,
                   CBT.CUSTOMER_BANK_ID,
                   F.FEE_ID,
                   SF.FEE_AMOUNT,
                   Q.MONTHS,
                   Q.DAYS,
                   SF.LAST_PAYMENT,
                   SF.START_DATE,
                   LEAST(NVL(
                        CASE WHEN Q.MONTHS = 0 THEN SF.END_DATE + Q.DAYS
                             ELSE TRUNC(LAST_DAY(ADD_MONTHS(SF.END_DATE, Q.MONTHS)), 'DD')
                        END
                   , MAX_DATE), SYSDATE) EFFECTIVE_END_DATE,
                   F.FEE_NAME,
                   T.FEE_CURRENCY_ID,
                   DECODE(T.PAYMENT_SCHEDULE_ID, 2, 'Y', 8, 'Y', 'N') AS_ACCUM
              FROM CORP.SERVICE_FEES SF, CORP.FREQUENCY Q, CORP.CUSTOMER_BANK_TERMINAL CBT, CORP.FEES F, REPORT.TERMINAL T
             WHERE SF.FREQUENCY_ID = Q.FREQUENCY_ID
               AND SF.TERMINAL_ID = CBT.TERMINAL_ID
               AND SF.FEE_ID = F.FEE_ID
               AND CORP.WITHIN1(SYSDATE, CBT.START_DATE, CBT.END_DATE) = 1
               AND (Q.DAYS > 0 OR Q.MONTHS > 0)
               AND CASE WHEN Q.MONTHS = 0 THEN NVL(SF.LAST_PAYMENT, MIN_DATE) + Q.DAYS
                             ELSE TRUNC(LAST_DAY(ADD_MONTHS(NVL(SF.LAST_PAYMENT, MIN_DATE), Q.MONTHS)), 'DD')
                        END < LEAST(NVL(
                        CASE WHEN Q.MONTHS = 0 THEN SF.END_DATE + Q.DAYS
                             ELSE TRUNC(LAST_DAY(ADD_MONTHS(SF.END_DATE, Q.MONTHS)), 'DD')
                        END
                   , MAX_DATE), SYSDATE)
               AND SF.TERMINAL_ID = T.TERMINAL_ID;

        l_last_payment                     SERVICE_FEES.LAST_PAYMENT%TYPE;
        l_fmt VARCHAR2(50);
        l_ledger_id LEDGER.LEDGER_ID%TYPE;
        l_batch_id LEDGER.BATCH_ID%TYPE;
    BEGIN
        FOR r_fee IN c_fee LOOP
            -- If last payment is null, Use first transaction as last payment date
            IF r_fee.LAST_PAYMENT IS NULL THEN
              SELECT MIN(x.CLOSE_DATE)
                INTO l_last_payment
                FROM REPORT.TRANS x
              WHERE x.TERMINAL_ID = r_fee.TERMINAL_ID
                AND x.CLOSE_DATE >= NVL(r_fee.START_DATE, MIN_DATE);
              IF l_last_payment IS NOT NULL THEN
                 UPDATE SERVICE_FEES
                    SET LAST_PAYMENT = l_last_payment
                  WHERE SERVICE_FEE_ID =  r_fee.SERVICE_FEE_ID;
              END IF;
            ELSE
              l_last_payment := r_fee.LAST_PAYMENT;
            END IF;
            IF l_last_payment IS NOT NULL THEN
              l_fmt := GET_SERVICE_FEE_DATE_FMT(r_fee.MONTHS, r_fee.DAYS);
              LOOP
                  IF r_fee.MONTHS > 0 THEN
                      SELECT TRUNC(LAST_DAY(ADD_MONTHS(l_last_payment, r_fee.MONTHS)), 'DD')
                        INTO l_last_payment
                        FROM DUAL;
                  ELSE
                      SELECT l_last_payment + r_fee.DAYS
                        INTO l_last_payment
                        FROM DUAL;
                  END IF;
                  EXIT WHEN l_last_payment >= r_fee.EFFECTIVE_END_DATE;
  
                  DECLARE
                     l_fee_amt CORP.LEDGER.AMOUNT%TYPE;
                     l_desc CORP.LEDGER.DESCRIPTION%TYPE;
                     l_hosts PLS_INTEGER;
                  BEGIN -- catch exception here
                      -- skip creation of fee if start date is after fee date
                      IF l_last_payment >= NVL(r_fee.start_date, l_last_payment) THEN
                          l_batch_id := GET_OR_CREATE_BATCH(
                                      r_fee.TERMINAL_ID,
                                      r_fee.CUSTOMER_BANK_ID,
                                      l_last_payment,
                                      r_fee.FEE_CURRENCY_ID,
                                      r_fee.AS_ACCUM);
                          SELECT LEDGER_SEQ.NEXTVAL
                            INTO l_ledger_id
                            FROM DUAL;
                          IF r_fee.fee_id = 9 THEN
                             SELECT MAX(REAL_TIME.DEVICE_HOST_COUNT(e.EPORT_SERIAL_NUM, l_last_payment))
                               INTO l_hosts
                               FROM REPORT.EPORT e, REPORT.TERMINAL_EPORT te
                              WHERE e.EPORT_ID = te.EPORT_ID
                                AND te.TERMINAL_ID = r_fee.TERMINAL_ID
                                AND l_last_payment BETWEEN NVL(te.START_DATE, MIN_DATE) AND NVL(te.END_DATE, MAX_DATE);
                             l_fee_amt := -ABS(r_fee.FEE_AMOUNT * l_hosts);
                             l_desc := r_fee.FEE_NAME || ' ('||TO_CHAR(l_hosts)||' hosts) ' || ' for ' || TO_CHAR(l_last_payment, l_fmt);
                          ELSE
                             l_fee_amt := -ABS(r_fee.FEE_AMOUNT);
                             l_desc := r_fee.FEE_NAME || ' for ' || TO_CHAR(l_last_payment, l_fmt);
                          END IF;
                          INSERT INTO LEDGER(
                              LEDGER_ID,
                              ENTRY_TYPE,
                              SERVICE_FEE_ID,
                              AMOUNT,
                              ENTRY_DATE,
                              BATCH_ID,
                              SETTLE_STATE_ID,
                              LEDGER_DATE,
                              DESCRIPTION)
                          VALUES (
                              l_ledger_id,
                              'SF',
                              r_fee.SERVICE_FEE_ID,
                              l_fee_amt,
                              l_last_payment,
                              l_batch_id,
                              2,
                              l_last_payment,
                              l_desc);
                          -- create net revenue fee on transaction, if any
                          INSERT INTO LEDGER(
                              LEDGER_ID,
                              RELATED_LEDGER_ID,
                              ENTRY_TYPE,
                              SERVICE_FEE_ID,
                              AMOUNT,
                              ENTRY_DATE,
                              BATCH_ID,
                              SETTLE_STATE_ID,
                              LEDGER_DATE,
                              DESCRIPTION)
                          SELECT
                              LEDGER_SEQ.NEXTVAL,
                              l_ledger_id,
                              'SF',
                              SF.SERVICE_FEE_ID,
                              -l_fee_amt * SF.FEE_PERCENT,
                              l_last_payment,
                              l_batch_id,
                              2,
                              l_last_payment,
                              F.FEE_NAME
                          FROM SERVICE_FEES SF, FEES F
                         WHERE SF.FEE_ID = F.FEE_ID
                           AND SF.TERMINAL_ID = r_fee.TERMINAL_ID
                           AND l_last_payment BETWEEN NVL(SF.START_DATE, MIN_DATE)
                                AND NVL(SF.END_DATE, MAX_DATE)
                           AND SF.FREQUENCY_ID = 6;
                      END IF;
  
                      UPDATE SERVICE_FEES
                         SET LAST_PAYMENT = l_last_payment
                       WHERE SERVICE_FEE_ID =  r_fee.SERVICE_FEE_ID;
  
                      COMMIT;             -- BECAREFUL OF CALLING THIS PROCEDURE SINCE IT HAS THIS COMMIT
                  EXCEPTION
                                   WHEN DUP_VAL_ON_INDEX THEN
                                           ROLLBACK;
                  END;
              END LOOP;
            END IF;
        END LOOP;
    END;

    PROCEDURE SERVICE_FEES_UPD(
        l_terminal_id IN SERVICE_FEES.TERMINAL_ID%TYPE,
        l_fee_id IN SERVICE_FEES.FEE_ID%TYPE,
        l_freq_id IN SERVICE_FEES.FREQUENCY_ID%TYPE,
        l_fee_amt IN SERVICE_FEES.FEE_AMOUNT%TYPE,
        l_fee_perc IN SERVICE_FEES.FEE_PERCENT%TYPE,
        l_effective_date IN SERVICE_FEES.END_DATE%TYPE DEFAULT SYSDATE,
        l_end_date IN SERVICE_FEES.END_DATE%TYPE,
        l_override IN CHAR DEFAULT 'N')
    IS
        l_last_payment SERVICE_FEES.LAST_PAYMENT%TYPE;
        l_sf_id SERVICE_FEES.SERVICE_FEE_ID%TYPE;
        l_batch_id LEDGER.BATCH_ID%TYPE;
        l_next_payment SERVICE_FEES.LAST_PAYMENT%TYPE;
		l_lock VARCHAR2(128);
		l_current_date SERVICE_FEES.END_DATE%TYPE := SYSDATE;
		l_fee_effective_date SERVICE_FEES.END_DATE%TYPE := NVL(l_effective_date, l_current_date);
		l_fee_override CHAR := NVL(l_override, 'N');
    BEGIN
		l_lock := GLOBALS_PKG.REQUEST_LOCK('TERMINAL.TERMINAL_ID', l_terminal_id);
	
        BEGIN
            IF l_freq_id = 6 THEN
                --disallow change to net revenue fees that affect already paid entries
                SELECT MAX(ENTRY_DATE)
                  INTO l_last_payment
                  FROM LEDGER L, BATCH B, SERVICE_FEES SF, DOC D
                 WHERE B.TERMINAL_ID = l_terminal_id
                   AND SF.TERMINAL_ID = l_terminal_id
                   AND SF.FEE_ID = l_fee_id
                   AND SF.FREQUENCY_ID = l_freq_id
                   AND B.DOC_ID = D.DOC_ID
                   AND D.STATUS NOT IN('O', 'D')
                   AND B.BATCH_ID = L.BATCH_ID
                   AND L.SERVICE_FEE_ID = SF.SERVICE_FEE_ID
                   AND L.ENTRY_TYPE = 'SF'
                   AND L.DELETED = 'N';
                IF NVL(l_last_payment, l_fee_effective_date) > l_fee_effective_date THEN -- BIG PROBLEM
                    RAISE_APPLICATION_ERROR(-20012, 'This net revenue fee was charged to the customer after the specified effective date.');
                ELSE
                    l_last_payment := MIN_DATE;
                END IF;
            ELSIF l_fee_id NOT IN(7) THEN
                SELECT MAX(LAST_PAYMENT), 
                      CASE WHEN Q.MONTHS = 0 THEN l_fee_effective_date + Q.DAYS
                           ELSE TRUNC(LAST_DAY(ADD_MONTHS(l_fee_effective_date, Q.MONTHS)), 'DD')
                      END
                  INTO l_last_payment, l_next_payment
                  FROM SERVICE_FEES SF, CORP.FREQUENCY Q
                 WHERE SF.TERMINAL_ID = l_terminal_id
                   AND SF.FEE_ID = l_fee_id
                   AND SF.FREQUENCY_ID = l_freq_id
                   AND SF.FREQUENCY_ID = Q.FREQUENCY_ID
                 GROUP BY Q.MONTHS, Q.DAYS;
                IF l_last_payment is not null AND l_last_payment > l_next_payment THEN
                    IF l_fee_override <> 'Y' THEN --BIG PROBLEM
                        RAISE_APPLICATION_ERROR(-20011, 'This service fee was charged to the customer after the specified effective date.');
                    END IF;
                END IF;
            END IF;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
            	 NULL;
            WHEN OTHERS THEN
            	 RAISE;
        END;
        IF l_freq_id = 6 THEN
            --delete any old net revenue fees >= l_fee_effective_date
            DELETE FROM LEDGER L
             WHERE L.ENTRY_TYPE = 'SF'
               AND L.SERVICE_FEE_ID IN(
                 SELECT SF.SERVICE_FEE_ID
                   FROM SERVICE_FEES SF
                  WHERE SF.TERMINAL_ID = l_terminal_id
                    AND SF.FEE_ID = l_fee_id
                    AND SF.FREQUENCY_ID = l_freq_id
                    --AND NVL(SF.END_DATE, l_fee_effective_date) >= l_fee_effective_date
                    )
               AND L.ENTRY_DATE >= l_fee_effective_date
               AND NOT EXISTS(
                    SELECT 1
                    FROM BATCH B, DOC D
                    WHERE B.BATCH_ID = L.BATCH_ID
                    AND B.DOC_ID = D.DOC_ID
                    AND D.STATUS NOT IN('O', 'D'));
        END IF;
        IF l_fee_id NOT IN(7) THEN
            UPDATE SERVICE_FEES SET END_DATE = l_fee_effective_date
             WHERE TERMINAL_ID = l_terminal_id
               AND FEE_ID = l_fee_id
               AND FREQUENCY_ID = l_freq_id
               AND NVL(END_DATE, l_fee_effective_date) >= l_fee_effective_date;
        END IF;
        IF NVL(l_fee_amt, 0) <> 0 OR NVL(l_fee_perc, 0) <> 0 THEN
            SELECT SERVICE_FEE_SEQ.NEXTVAL INTO l_sf_id FROM DUAL;
            INSERT INTO SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID,
                FREQUENCY_ID, FEE_AMOUNT, FEE_PERCENT, START_DATE,
                END_DATE, LAST_PAYMENT)
                SELECT l_sf_id, l_terminal_id, l_fee_id, l_freq_id, l_fee_amt,
                       l_fee_perc, l_fee_effective_date, l_end_date,
                       l_last_payment
                  FROM FREQUENCY F
                 WHERE F.FREQUENCY_ID = l_freq_id;
            IF l_freq_id = 6 THEN
                --add any new net revenue fees
                INSERT INTO LEDGER(
                    LEDGER_ID,
                    RELATED_LEDGER_ID,
                    ENTRY_TYPE,
                    TRANS_ID,
                    PROCESS_FEE_ID,
                    SERVICE_FEE_ID,
                    AMOUNT,
                    ENTRY_DATE,
                    BATCH_ID,
                    SETTLE_STATE_ID,
                    LEDGER_DATE,
                    DESCRIPTION)
                SELECT
                    LEDGER_SEQ.NEXTVAL,
                    L.LEDGER_ID,
                    'SF',
                    L.TRANS_ID,
                    L.PROCESS_FEE_ID,
                    l_sf_id,
                    -L.AMOUNT * l_fee_perc,
                    L.ENTRY_DATE,
                    L.BATCH_ID,
                    L.SETTLE_STATE_ID,
                    L.LEDGER_DATE,
                    F.FEE_NAME
                FROM LEDGER L, BATCH B, FEES F, DOC D
               WHERE F.FEE_ID = l_fee_id
                 AND L.BATCH_ID = B.BATCH_ID
                 AND B.TERMINAL_ID = l_terminal_id
                 AND L.ENTRY_DATE >= l_fee_effective_date
                 AND L.RELATED_LEDGER_ID IS NULL -- Avoid Percent of Percent Fee
                 AND L.DELETED = 'N'
                 AND B.DOC_ID = D.DOC_ID
                 AND D.STATUS IN('O')
                 AND L.ENTRY_TYPE IN('CC', 'PF', 'SF', 'AD', 'CB', 'RF');
            ELSIF l_fee_override = 'Y' AND l_last_payment IS NOT NULL THEN -- let's insert any missing (No need to do this for net rev fees)
                DECLARE
                    CURSOR l_cur IS
                        SELECT LEDGER_SEQ.NEXTVAL LEDGER_ID,
                               A.FEE_DATE,
                               CBT.CUSTOMER_BANK_ID,
                               T.FEE_CURRENCY_ID,
                               F.FEE_NAME || ' for ' || TO_CHAR(A.FEE_DATE, A.FMT) FEE_DESC,
                               'Y' AS_ACCUM
                    FROM (SELECT DECODE(Q.DAYS, 0, LAST_DAY(D.COLUMN_VALUE), D.COLUMN_VALUE + Q.DAYS - 1) FEE_DATE,
                                 GET_SERVICE_FEE_DATE_FMT(Q.MONTHS, Q.DAYS) FMT
                            FROM TABLE(CAST(CORP.GLOBALS_PKG.GET_DATE_LIST(l_fee_effective_date,
                                 LEAST(NVL(l_last_payment, (SELECT  
                                   CASE WHEN Q.MONTHS = 0 THEN l_current_date - Q.DAYS
                                        ELSE TRUNC(LAST_DAY(ADD_MONTHS(l_current_date, -Q.MONTHS)), 'DD')
                                   END FROM CORP.FREQUENCY Q WHERE Q.FREQUENCY_ID = l_freq_id)), NVL(l_end_date, MAX_DATE)),
                                 l_freq_id, 0, 0) AS REPORT.DATE_LIST)) D,
                                 FREQUENCY Q
                            WHERE Q.FREQUENCY_ID = l_freq_id
                              AND (Q.MONTHS > 0 OR Q.DAYS > 0)) A,
                          CUSTOMER_BANK_TERMINAL CBT,
                          FEES F,
                          REPORT.TERMINAL T
                    WHERE NOT EXISTS(SELECT 1
                        FROM CORP.LEDGER L, CORP.BATCH B
                        WHERE L.BATCH_ID = B.BATCH_ID
                          AND B.TERMINAL_ID = l_terminal_id
                          AND L.ENTRY_TYPE = 'SF'
                          AND L.DELETED = 'N'
                          AND L.AMOUNT <> 0
                          AND L.ENTRY_DATE = A.FEE_DATE)
                      AND WITHIN1(A.FEE_DATE, CBT.START_DATE, CBT.END_DATE) = 1
                      AND CBT.TERMINAL_ID = l_terminal_id
                      AND F.FEE_ID = l_fee_id
                      AND T.TERMINAL_ID = l_terminal_id;
                BEGIN
                    FOR l_rec IN l_cur LOOP
                        l_batch_id := GET_OR_CREATE_BATCH(
                                    l_terminal_id,
                                    l_rec.CUSTOMER_BANK_ID,
                                    l_rec.FEE_DATE,
                                    l_rec.FEE_CURRENCY_ID,
                                    l_rec.AS_ACCUM);

                        INSERT INTO LEDGER(
                                LEDGER_ID,
                                ENTRY_TYPE,
                                SERVICE_FEE_ID,
                                AMOUNT,
                                ENTRY_DATE,
                                BATCH_ID,
                                SETTLE_STATE_ID,
                                LEDGER_DATE,
                                DESCRIPTION)
                            VALUES(
                                l_rec.LEDGER_ID,
                                'SF',
                                l_sf_id,
                                -ABS(l_fee_amt),
                                l_rec.FEE_DATE,
                                l_batch_id,
                                2,
                                l_rec.FEE_DATE,
                                l_rec.FEE_DESC);
                        -- create net revenue fee on service fee, if any
                        INSERT INTO LEDGER(
                            LEDGER_ID,
                            RELATED_LEDGER_ID,
                            ENTRY_TYPE,
                            SERVICE_FEE_ID,
                            AMOUNT,
                            ENTRY_DATE,
                            BATCH_ID,
                            SETTLE_STATE_ID,
                            LEDGER_DATE,
                            DESCRIPTION)
                        SELECT
                            LEDGER_SEQ.NEXTVAL,
                            l_rec.LEDGER_ID,
                            'SF',
                            SF.SERVICE_FEE_ID,
                            ABS(l_fee_amt) * SF.FEE_PERCENT,
                            l_rec.FEE_DATE,
                            l_batch_id,
                            2,
                            l_rec.FEE_DATE,
                            F.FEE_NAME
                        FROM SERVICE_FEES SF, FEES F
                       WHERE SF.FEE_ID = F.FEE_ID
                         AND SF.TERMINAL_ID = l_terminal_id
                         AND l_rec.FEE_DATE BETWEEN NVL(SF.START_DATE, MIN_DATE)
                              AND NVL(SF.END_DATE, MAX_DATE)
                         AND SF.FREQUENCY_ID = 6;
                    END LOOP;
                    -- TODO: Set LAST_PAYMENT if necessary
                END;
            END IF;
        END IF;
    END;
    
    PROCEDURE UNLOCK_DOC (
      l_doc_id IN DOC.DOC_ID%TYPE)
    IS
        l_customer_bank_id CORP.DOC.CUSTOMER_BANK_ID%TYPE;
        l_lock VARCHAR2(128);
    BEGIN
    	IF FREEZE_DOC_STATUS_DOC_ID(l_doc_id) <> 'L' THEN
           RAISE_APPLICATION_ERROR(-20400, 'This document has already been approved; you can not unlock it.');
   	    END IF;
    	--Remove rounding entries
    	UPDATE LEDGER
           SET DELETED = 'Y'
         WHERE ENTRY_TYPE = 'SB'
           AND TRANS_ID IS NULL
           AND BATCH_ID IN(SELECT BATCH_ID FROM BATCH WHERE DOC_ID = l_doc_id);
           
        -- Blank out the end date of any as accum batches
        UPDATE BATCH B
           SET B.BATCH_STATE_CD = 'L',
               B.BATCH_CLOSED_TS = NULL,
               B.END_DATE = NULL
         WHERE B.DOC_ID = l_doc_id
           AND B.BATCH_STATE_CD = 'F'
           AND B.PAYMENT_SCHEDULE_ID IN(1,5);

        -- Change time-period batches to Open
        UPDATE BATCH B
           SET B.BATCH_STATE_CD = 'O',
               B.BATCH_CLOSED_TS = NULL
         WHERE B.DOC_ID = l_doc_id
           AND B.BATCH_STATE_CD IN('U', 'D')
           AND B.PAYMENT_SCHEDULE_ID NOT IN(1,2,5,7,8);

        -- first condense any docs that may match once they are re-opened
    	DECLARE
    	   l_new_doc_id DOC.DOC_ID%TYPE;
        BEGIN
    	    SELECT MAX(DNEW.DOC_ID)
              INTO l_new_doc_id
              FROM DOC DOLD, DOC DNEW
             WHERE DOLD.DOC_ID = l_doc_id
               AND DNEW.DOC_ID <> l_doc_id
               AND DOLD.CUSTOMER_BANK_ID = DNEW.CUSTOMER_BANK_ID
               AND NVL(DOLD.CURRENCY_ID, 0) = NVL(DNEW.CURRENCY_ID, 0)
               AND DNEW.STATUS = 'O';
            IF l_new_doc_id IS NOT NULL THEN
            	-- Update batches and delete old doc
            	UPDATE BATCH SET DOC_ID = l_new_doc_id WHERE DOC_ID = l_doc_id;
            	DELETE FROM DOC WHERE DOC_ID = l_doc_id;
            	
            	--now condense any batches that may match once they are moved
            	DECLARE
            	   CURSOR l_cur IS
                    	SELECT BNEW.BATCH_ID NEW_BATCH_ID,
                               BOLD.BATCH_ID OLD_BATCH_ID,
                               BOLD.END_DATE OLD_END_DATE,
                               BOLD.START_DATE OLD_START_DATE
                          FROM BATCH BOLD, BATCH BNEW
                         WHERE BOLD.DOC_ID = l_doc_id
                           AND BNEW.DOC_ID = l_doc_id
                           AND BOLD.PAYMENT_SCHEDULE_ID = BNEW.PAYMENT_SCHEDULE_ID
                           AND NVL(BOLD.TERMINAL_ID, 0) = NVL(BNEW.TERMINAL_ID, 0)
                           AND BOLD.BATCH_ID > BNEW.BATCH_ID
                           AND (BOLD.PAYMENT_SCHEDULE_ID IN(1,5)
                             OR BOLD.START_DATE = BNEW.START_DATE);
            	BEGIN
            	   FOR l_rec IN l_cur LOOP
            	       UPDATE LEDGER
            	          SET BATCH_ID = l_rec.NEW_BATCH_ID
                        WHERE BATCH_ID = l_rec.OLD_BATCH_ID;
                        
                       DELETE FROM BATCH WHERE BATCH_ID = l_rec.OLD_BATCH_ID;
                       
                       UPDATE BATCH
                          SET START_DATE = LEAST(START_DATE, l_rec.OLD_START_DATE),
                              END_DATE = GREATEST(END_DATE, l_rec.OLD_END_DATE)
                        WHERE BATCH_ID = l_rec.NEW_BATCH_ID;
            	   END LOOP;
         	   END;
            ELSE
                -- Update doc
            	UPDATE DOC SET STATUS = 'O' WHERE DOC_ID = l_doc_id;
            END IF;
    	END;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
    	   RAISE_APPLICATION_ERROR(-20401, 'This document does not exist.');
        WHEN OTHERS THEN
    	   RAISE;
    END;
    
    FUNCTION GET_TRANS_ADJ_DESC(
        l_trans_id LEDGER.TRANS_ID%TYPE,
        l_entry_type LEDGER.ENTRY_TYPE%TYPE,
        l_entry_date LEDGER.ENTRY_DATE%TYPE,
        l_terminal_id BATCH.TERMINAL_ID%TYPE,
        l_customer_bank_id DOC.CUSTOMER_BANK_ID%TYPE)
     RETURN LEDGER.DESCRIPTION%TYPE
    IS
        l_old_payable VARCHAR(4000); -- oracle raises an exception if you use anything with smaller length
        l_old_terminal_id BATCH.TERMINAL_ID%TYPE;
        l_old_customer_bank_id DOC.CUSTOMER_BANK_ID%TYPE;
        l_desc LEDGER.DESCRIPTION%TYPE;
    BEGIN
        -- figure out why this was added
        --possible reasons:
        --  1. previously denied
        --  2. changed location
        --  3. changed customer bank
        
        SELECT *
          INTO l_old_payable, l_old_terminal_id, l_old_customer_bank_id
          FROM (
            SELECT PAYMENTS_PKG.ENTRY_PAYABLE(L.SETTLE_STATE_ID, L.ENTRY_TYPE),
                   B.TERMINAL_ID, D.CUSTOMER_BANK_ID
              FROM LEDGER L, BATCH B, DOC D
             WHERE L.BATCH_ID = B.BATCH_ID
               AND L.DELETED = 'N'
               AND B.DOC_ID = D.DOC_ID
               AND D.STATUS NOT IN('O', 'D')
               AND L.TRANS_ID = l_trans_id
               AND L.ENTRY_TYPE = 'CC'
               AND B.PAYMENT_SCHEDULE_ID <> 5
             ORDER BY L.CREATE_DATE DESC) A
         WHERE ROWNUM = 1;
        IF l_old_payable = 'N' THEN
            IF l_entry_type = 'CC' THEN
                RETURN 'Payment of previously declined transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSIF l_entry_type = 'PF' THEN
                RETURN 'Process Fee for previously declined transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSE -- this should never occur
                RETURN 'Payment related to previously declined transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            END IF;
        ELSIF l_terminal_id <> l_old_terminal_id THEN
            IF l_entry_type = 'CC' THEN
                RETURN 'Payment of transaction previously assigned to a different location (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSIF l_entry_type = 'PF' THEN
                RETURN 'Process Fee for transaction previously assigned to a different location (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSE -- this should never occur
                RETURN 'Payment related to transaction previously assigned to a different location (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            END IF;
        ELSIF l_customer_bank_id <> l_old_customer_bank_id THEN
            IF l_entry_type = 'CC' THEN
                RETURN 'Payment of transaction previously assigned to a different bank account (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSIF l_entry_type = 'PF' THEN
                RETURN 'Process Fee for transaction previously assigned to a different bank account (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSE -- this should never occur
                RETURN 'Payment related to transaction previously assigned to a different bank account (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            END IF;
        ELSE
            IF l_entry_type = 'CC' THEN
                RETURN 'Payment of previously unpaid transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSIF l_entry_type = 'PF' THEN
                RETURN 'Process Fee for previously unpaid transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSE -- this should never occur
                RETURN 'Payment related to previously unpaid transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            END IF;
        END IF;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            IF l_entry_type = 'CC' THEN
                RETURN 'Payment of previously unpaid transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSIF l_entry_type = 'PF' THEN
                RETURN 'Process Fee for previously unpaid transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSE -- this should never occur
                RETURN 'Payment related to previously unpaid transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            END IF;
    END;
    
    PROCEDURE UPDATE_LEDGER(
              l_tran_id LEDGER.TRANS_ID%TYPE)
    IS
    	CURSOR l_cur IS
    		SELECT TRANS_TYPE_ID, CLOSE_DATE, SETTLE_DATE, TOTAL_AMOUNT, SETTLE_STATE_ID,
                   TERMINAL_ID, CUSTOMER_BANK_ID, PROCESS_FEE_ID, CURRENCY_ID
    		  FROM REPORT.TRANS
    		 WHERE TRAN_ID = l_tran_id;
    BEGIN
         FOR l_rec IN l_cur LOOP
    	     UPDATE_LEDGER(l_tran_id,l_rec.trans_type_id,l_rec.close_date,l_rec.settle_date,
                           l_rec.total_amount,l_rec.settle_state_id,l_rec.terminal_id,
                           l_rec.customer_bank_id,l_rec.process_fee_id,l_rec.currency_id);
   	     END LOOP;
    END;
    
    PROCEDURE ADD_ACTIVATION_FEE(
        l_terminal_id CORP.BATCH.TERMINAL_ID%TYPE,
        l_fee_amt CORP.LEDGER.AMOUNT%TYPE,
        l_activation_date  CORP.LEDGER.ENTRY_DATE%TYPE)
    IS
        l_batch_id CORP.BATCH.BATCH_ID%TYPE;
        l_service_fee_id CORP.LEDGER.SERVICE_FEE_ID%TYPE;
        l_desc CORP.LEDGER.DESCRIPTION%TYPE;
        l_ledger_id CORP.LEDGER.LEDGER_ID%TYPE;
        l_cust_bank_id CORP.DOC.CUSTOMER_BANK_ID%TYPE;
        l_fee_currency_id CORP.DOC.CURRENCY_ID%TYPE;
		l_lock VARCHAR2(128);
    BEGIN
        SELECT cbt.CUSTOMER_BANK_ID, t.FEE_CURRENCY_ID,
               'Activation Fee for Terminal ' || t.TERMINAL_NBR
          INTO l_cust_bank_id, l_fee_currency_id, l_desc
          FROM REPORT.TERMINAL t, CORP.CUSTOMER_BANK_TERMINAL cbt
         WHERE t.TERMINAL_ID = l_terminal_id
           AND t.TERMINAL_ID = cbt.TERMINAL_ID
           AND l_activation_date >= NVL(cbt.START_DATE, MIN_DATE)
           AND l_activation_date < NVL(cbt.END_DATE, MAX_DATE);
		   
		l_lock := GLOBALS_PKG.REQUEST_LOCK('TERMINAL.TERMINAL_ID', l_terminal_id);
		   
        l_batch_id := GET_OR_CREATE_BATCH(l_terminal_id, l_cust_bank_id, l_activation_date, l_fee_currency_id, 'N');
        SELECT SERVICE_FEE_SEQ.NEXTVAL
          INTO l_service_fee_id
          FROM DUAL;
        INSERT INTO SERVICE_FEES(
            SERVICE_FEE_ID,
            TERMINAL_ID,
            FEE_ID,
            FEE_AMOUNT,
            FREQUENCY_ID,
            LAST_PAYMENT,
            START_DATE)
       	  VALUES(
       	    l_service_fee_id,
       	    l_terminal_id,
       	    10,
       	    l_fee_amt,
       	    7,
       	    l_activation_date,
       	    l_activation_date);
       	    
        SELECT LEDGER_SEQ.NEXTVAL
          INTO l_ledger_id
          FROM DUAL;
        INSERT INTO LEDGER(
            LEDGER_ID,
            ENTRY_TYPE,
            SERVICE_FEE_ID,
            AMOUNT,
            ENTRY_DATE,
            BATCH_ID,
            SETTLE_STATE_ID,
            LEDGER_DATE,
            DESCRIPTION)
        VALUES (
            l_ledger_id,
            'SF',
            l_service_fee_id,
            -l_fee_amt,
            l_activation_date,
            l_batch_id,
            2,
            l_activation_date,
            l_desc);
    END;
    
    PROCEDURE SWITCH_PAYMENT_SCHEDULE(
        l_tran_id CORP.LEDGER.TRANS_ID%TYPE,
        l_payment_schedule_id  CORP.BATCH.PAYMENT_SCHEDULE_ID%TYPE)
    IS
        l_lock VARCHAR(128);
        l_adj_amt CORP.LEDGER.AMOUNT%TYPE;
        l_batch_id CORP.LEDGER.BATCH_ID%TYPE;
       	CURSOR l_cur IS
    		SELECT X.TRANS_TYPE_ID, X.CLOSE_DATE, X.SETTLE_DATE, X.TOTAL_AMOUNT, X.SETTLE_STATE_ID,
                   X.TERMINAL_ID, X.CUSTOMER_BANK_ID, X.PROCESS_FEE_ID, X.CURRENCY_ID, T.BUSINESS_UNIT_ID
    		  FROM REPORT.TRANS X
  		     INNER JOIN REPORT.TERMINAL T ON X.TERMINAL_ID = T.TERMINAL_ID
    		 WHERE TRAN_ID = l_tran_id;
    BEGIN
        FOR l_rec IN l_cur LOOP
            IF NVL(l_rec.terminal_id, 0) <> 0 AND NVL(l_rec.customer_bank_id, 0) <> 0 THEN
                l_lock := GLOBALS_PKG.REQUEST_LOCK('LEDGER.TRANS_ID',l_tran_id);
                -- Remove any open entries in different pay sched
                UPDATE CORP.LEDGER L SET DELETED = 'Y'
                 WHERE L.TRANS_ID = l_tran_id
                   AND L.DELETED = 'N'
                   AND EXISTS(
                      SELECT 1
                        FROM CORP.BATCH B
                       INNER JOIN CORP.DOC D ON B.DOC_ID = D.DOC_ID
                       WHERE B.PAYMENT_SCHEDULE_ID <> l_payment_schedule_id
                         AND B.BATCH_STATE_CD IN('O', 'L')
                         AND D.STATUS IN('O')
                         AND L.BATCH_ID = B.BATCH_ID);

                -- Make adjustment for old
                SELECT SUM(L.AMOUNT)
                  INTO l_adj_amt
                  FROM CORP.LEDGER L
                 INNER JOIN CORP.BATCH B ON L.BATCH_ID = B.BATCH_ID
                 INNER JOIN CORP.DOC D ON B.DOC_ID = D.DOC_ID
                 WHERE L.TRANS_ID = l_tran_id
                   AND L.DELETED = 'N'
                   AND D.STATUS NOT IN('D')
                   AND B.PAYMENT_SCHEDULE_ID <> l_payment_schedule_id
                   AND ENTRY_PAYABLE(l.settle_state_id,l.entry_type) = 'Y';

                IF NVL(l_adj_amt, 0) <> 0 THEN
                    l_batch_id := GET_OR_CREATE_BATCH(l_rec.terminal_id,
                                    l_rec.customer_bank_id, l_rec.close_date, l_rec.currency_id, 'A');
                    INSERT INTO LEDGER(
                        LEDGER_ID,
                        ENTRY_TYPE,
                        TRANS_ID,
                        PROCESS_FEE_ID,
                        AMOUNT,
                        ENTRY_DATE,
                        BATCH_ID,
                        SETTLE_STATE_ID,
                        LEDGER_DATE,
                        DESCRIPTION)
                    SELECT
                        LEDGER_SEQ.NEXTVAL,
                        'AD',
                        l_tran_id,
                        NULL,
                        -l_adj_amt,
                        l_rec.close_date,
                        l_batch_id,
                        l_rec.SETTLE_STATE_ID,
                        NVL(l_rec.SETTLE_DATE, SYSDATE),
                        'Payment schedule switched to ' || DESCRIPTION
                      FROM CORP.PAYMENT_SCHEDULE
                     WHERE PAYMENT_SCHEDULE_ID = l_payment_schedule_id;
                END IF;
                -- insert as new
                l_batch_id := GET_OR_CREATE_BATCH(l_rec.terminal_id, l_rec.customer_bank_id, l_rec.close_date, l_rec.currency_id, l_payment_schedule_id, l_rec.BUSINESS_UNIT_ID, 'N');
                INSERT_TRANS_TO_LEDGER(l_tran_id, l_rec.trans_type_id, l_rec.close_date, l_rec.settle_date,
                     l_rec.total_amount, l_rec.settle_state_id, l_rec.process_fee_id, l_batch_id);
            END IF;
        END LOOP;
    END;
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/PKG_CUSTOMER_MANAGEMENT.pbk?rev=1.18
CREATE OR REPLACE PACKAGE BODY REPORT.PKG_CUSTOMER_MANAGEMENT AS
    PROCEDURE INTERNAL_CREATE_TERMINAL(
       pn_terminal_id OUT REPORT.TERMINAL.TERMINAL_ID%TYPE,
       pv_device_serial_cd IN REPORT.EPORT.EPORT_SERIAL_NUM%TYPE,
       pn_dealer_id IN CORP.DEALER.DEALER_ID%TYPE,
       pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
       pn_machine_id IN REPORT.TERMINAL.MACHINE_ID%TYPE,
       pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
       pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
       pn_region_id IN REPORT.REGION.REGION_ID%TYPE,
       pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
       pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
       pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
       pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
       pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
       pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
       pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
       pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
       pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
       pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
       pn_pc_id IN REPORT.TERMINAL.PRIMARY_CONTACT_ID%TYPE,
       pn_sc_id IN REPORT.TERMINAL.SECONDARY_CONTACT_ID%TYPE,
       pn_location_type_id IN REPORT.LOCATION.LOCATION_TYPE_ID%TYPE,
       pv_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
       pn_product_type_id IN REPORT.TERMINAL.PRODUCT_TYPE_ID%TYPE,
       pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
       pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
       pn_avg_amt IN REPORT.TERMINAL.AVG_TRANS_AMT%TYPE,
       pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
       pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
       pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
       pn_vends REPORT.TERMINAL.VENDS%TYPE,
       pv_term_var_1 IN REPORT.TERMINAL.TERM_VAR_1%TYPE,
       pv_term_var_2 IN REPORT.TERMINAL.TERM_VAR_2%TYPE,
       pv_term_var_3 IN REPORT.TERMINAL.TERM_VAR_3%TYPE,
       pv_term_var_4 IN REPORT.TERMINAL.TERM_VAR_4%TYPE,
       pv_term_var_5 IN REPORT.TERMINAL.TERM_VAR_5%TYPE,
       pv_term_var_6 IN REPORT.TERMINAL.TERM_VAR_6%TYPE,
       pv_term_var_7 IN REPORT.TERMINAL.TERM_VAR_7%TYPE,
       pv_term_var_8 IN REPORT.TERMINAL.TERM_VAR_8%TYPE,
	   pd_activate_date IN REPORT.TERMINAL_EPORT.START_DATE%TYPE)
    IS
      l_eport_id REPORT.EPORT.EPORT_ID%TYPE;
      l_dev_type_id REPORT.EPORT.DEVICE_TYPE_ID%TYPE;
      l_location_id REPORT.TERMINAL.LOCATION_ID%TYPE;
      l_terminal_nbr REPORT.TERMINAL.TERMINAL_NBR%TYPE;
      l_customer_id REPORT.TERMINAL.CUSTOMER_ID%TYPE;
      l_license_id NUMBER;
      l_addr_id REPORT.TERMINAL_ADDR.ADDRESS_ID%TYPE;
      l_activate_date DATE := NVL(pd_activate_date, SYSDATE);
      l_last_deactivate_date DATE;
      l_start_date DATE;
      l_cnt NUMBER;
      l_business_unit_id CORP.BUSINESS_UNIT.BUSINESS_UNIT_ID%TYPE;
	  ln_new_pay_sched_id REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE;
      l_lock VARCHAR2(128);
	  ln_user_id REPORT.USER_LOGIN.USER_ID%TYPE := pn_user_id;
	  ln_pc_id REPORT.TERMINAL.PRIMARY_CONTACT_ID%TYPE := pn_pc_id;
    BEGIN
         BEGIN
              SELECT TERMINAL_SEQ.NEXTVAL, EPORT_ID, DEVICE_TYPE_ID, TERMINAL_ADDR_SEQ.NEXTVAL
                 INTO pn_terminal_id, l_eport_id, l_dev_type_id, l_addr_id FROM EPORT WHERE EPORT_SERIAL_NUM = pv_device_serial_cd;
         EXCEPTION
              WHEN NO_DATA_FOUND THEN
                   RAISE_APPLICATION_ERROR(-20201, 'Invalid eport serial number');
              WHEN OTHERS THEN
                   RAISE;
         END;
		 
		 l_lock := GLOBALS_PKG.REQUEST_LOCK('EPORT.EPORT_ID', l_eport_id);		 
         -- CHECK THAT EPORT IS INACTIVE
             SELECT NVL(MAX(NVL(END_DATE, MAX_DATE)), MIN_DATE)
               INTO l_last_deactivate_date
               FROM REPORT.TERMINAL_EPORT 
               WHERE EPORT_ID = l_eport_id;
             /*
         SELECT COUNT(TERMINAL_ID) 
               INTO l_cnt 
               FROM TERMINAL_EPORT 
               WHERE EPORT_ID = l_eport_id 
               AND NVL(END_DATE, MAX_DATE) > l_activate_date;
               */
         IF l_last_deactivate_date > l_activate_date THEN
              SELECT COUNT(TERMINAL_ID) INTO l_cnt FROM USER_TERMINAL WHERE USER_ID = ln_user_id AND
                 TERMINAL_ID IN(SELECT TERMINAL_ID FROM TERMINAL_EPORT WHERE EPORT_ID = l_eport_id AND NVL(END_DATE, MAX_DATE) > l_activate_date);
             IF l_cnt > 0 THEN --USER CAN VIEW TERMINAL
                  RAISE_APPLICATION_ERROR(-20205, 'Eport has already been activated');
             ELSE -- THIS MAY INDICATE THAT SOMEONE ACTIVATED THE WRONG TERMINAL
                  RAISE_APPLICATION_ERROR(-20206, 'Eport already in use');
             END IF;
         END IF;
         --VERIFY DEALER
         SELECT COUNT(DEALER_ID) INTO l_cnt FROM CORP.DEALER_EPORT WHERE DEALER_ID = pn_dealer_id AND EPORT_ID = l_eport_id;
         IF l_cnt = 0 THEN
              RAISE_APPLICATION_ERROR(-20202, 'Invalid dealer specified');
         END IF;
         --CREATE TERMINAL_NBR
         SELECT 'T0'|| TO_CHAR(TERMINAL_NBR_SEQ.NEXTVAL, 'FM99000000') INTO l_terminal_nbr FROM DUAL;
         
         --INSERT INTO TERMINAL, LOCATION, TERMINAL_ADDR
         IF pn_customer_bank_id <> 0 THEN
             BEGIN
                  SELECT CUSTOMER_ID INTO l_customer_id FROM CORP.CUSTOMER_BANK WHERE CUSTOMER_BANK_ID = pn_customer_bank_id;
             EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                       RAISE_APPLICATION_ERROR(-20203, 'Invalid customer bank');
                  WHEN OTHERS THEN
                       RAISE;
             END;
         ELSE
              BEGIN
                  SELECT CUSTOMER_ID INTO l_customer_id FROM USER_LOGIN WHERE USER_ID = ln_pc_id AND CUSTOMER_ID <> 0;
             EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                       RAISE_APPLICATION_ERROR(-20203, 'Invalid primary contact');
                  WHEN OTHERS THEN
                       RAISE;
             END;
         END IF;
		 
		 IF ln_user_id = 0 THEN
			SELECT MAX(USER_ID)
			INTO ln_user_id
			FROM CORP.CUSTOMER
			WHERE CUSTOMER_ID = l_customer_id;
			
			ln_pc_id := ln_user_id;
		 END IF;
		 
         BEGIN
              SELECT LICENSE_ID INTO l_license_id FROM (SELECT LICENSE_ID FROM CORP.CUSTOMER_LICENSE CL, CORP.LICENSE_NBR LN
                  WHERE CL.LICENSE_NBR = LN.LICENSE_NBR AND CL.CUSTOMER_ID = l_customer_id ORDER BY RECEIVED DESC) WHERE ROWNUM = 1;
         EXCEPTION
              WHEN NO_DATA_FOUND THEN
                   RAISE_APPLICATION_ERROR(-20204, 'No license agreement found for this customer');
              WHEN OTHERS THEN
                   RAISE;
         END;
         BEGIN
              SELECT LICENSE_ID 
                INTO l_license_id 
                FROM (SELECT LICENSE_ID 
                        FROM CORP.DEALER_LICENSE 
                       WHERE DEALER_ID = pn_dealer_id
                         AND SYSDATE >= NVL(START_DATE, MIN_DATE)
                         AND SYSDATE < NVL(END_DATE, MAX_DATE)
                       ORDER BY START_DATE DESC, LICENSE_ID DESC)
               WHERE ROWNUM = 1;
         EXCEPTION
              WHEN NO_DATA_FOUND THEN
                   RAISE_APPLICATION_ERROR(-20204, 'No license agreement found for this dealer');
              WHEN OTHERS THEN
                   RAISE;
         END;    
         INSERT INTO TERMINAL_ADDR(ADDRESS_ID, CUSTOMER_ID, ADDRESS1, CITY, STATE, ZIP, COUNTRY_CD)
             VALUES(l_addr_id, l_customer_id, pv_address1, pv_city, pv_state_cd, pv_postal, pv_country_cd);
         BEGIN
             SELECT LOCATION_ID INTO l_location_id FROM LOCATION WHERE EQL(LOCATION_NAME, pv_location_name) = -1 AND EPORT_ID =  l_eport_id AND TERMINAL_ID = 0;
             UPDATE LOCATION SET (DESCRIPTION, ADDRESS_ID, UPD_DATE, TERMINAL_ID, LOCATION_TYPE_ID, LOCATION_TYPE_SPECIFY) =
                   (SELECT pv_location_details, l_addr_id, SYSDATE, pn_terminal_id, pn_location_type_id,pv_location_type_specific FROM DUAL) WHERE LOCATION_ID = l_location_id;
         EXCEPTION
              WHEN NO_DATA_FOUND THEN
                  SELECT LOCATION_SEQ.NEXTVAL INTO l_location_id FROM DUAL;
                   INSERT INTO LOCATION(LOCATION_ID, LOCATION_NAME, DESCRIPTION, ADDRESS_ID, CREATE_BY, TERMINAL_ID, EPORT_ID, LOCATION_TYPE_ID, LOCATION_TYPE_SPECIFY)
                          VALUES(l_location_id, pv_location_name, pv_location_details, l_addr_id, ln_user_id, pn_terminal_id, l_eport_id, pn_location_type_id, pv_location_type_specific);
              WHEN OTHERS THEN
                   RAISE;
         END;
         l_business_unit_id := FIND_BUSINESS_UNIT(l_eport_id, l_customer_id);
         SELECT MAX(PAYMENT_SCHEDULE_ID)
          INTO ln_new_pay_sched_id
          FROM CORP.PAYMENT_SCHEDULE
         WHERE PAYMENT_SCHEDULE_ID = pn_pay_sched_id
           AND SELECTABLE = 'Y';
         IF l_business_unit_id IN(2,3,5) OR ln_new_pay_sched_id IS NULL THEN
            SELECT DEFAULT_PAYMENT_SCHEDULE_ID
              INTO ln_new_pay_sched_id
              FROM CORP.BUSINESS_UNIT
             WHERE BUSINESS_UNIT_ID = l_business_unit_id;
         END IF;
                 
         INSERT INTO TERMINAL(
                TERMINAL_ID,
                TERMINAL_NBR,
                TERMINAL_NAME,
                EPORT_ID,
                CUSTOMER_ID,
                LOCATION_ID,
                ASSET_NBR,
                MACHINE_ID,
                TELEPHONE,
                PREFIX,
                PRODUCT_TYPE_ID,
                PRODUCT_TYPE_SPECIFY,
                PRIMARY_CONTACT_ID,
                SECONDARY_CONTACT_ID,
                AUTHORIZATION_MODE,
                AVG_TRANS_AMT,
                TIME_ZONE_ID,
                DEX_DATA,
                RECEIPT,
                VENDS,
                TERM_VAR_1,
                TERM_VAR_2,
                TERM_VAR_3,
                TERM_VAR_4,
                TERM_VAR_5,
                TERM_VAR_6,
                TERM_VAR_7,
                TERM_VAR_8,
                BUSINESS_UNIT_ID,
                PAYMENT_SCHEDULE_ID)
             SELECT
                pn_terminal_id,
                l_terminal_nbr,
                l_terminal_nbr,
                l_eport_id,
                l_customer_id,
                l_location_id,
                pv_asset_nbr,
                pn_machine_id,
                pv_telephone,
                pv_prefix,
                pn_product_type_id,
                pn_product_type_specific,
                ln_pc_id,
                pn_sc_id,
                pc_auth_mode,
                NVL(pn_avg_amt, 0),
                pn_time_zone_id,
                pc_dex_data,
                pc_receipt,
                pn_vends,
                pv_term_var_1,
                pv_term_var_2,
                pv_term_var_3,
                pv_term_var_4,
                pv_term_var_5,
                pv_term_var_6,
                pv_term_var_7,
                pv_term_var_8,
                l_business_unit_id,
                ln_new_pay_sched_id
              FROM DUAL;
              
            IF pn_region_id != 0 THEN
                INSERT INTO REPORT.TERMINAL_REGION(TERMINAL_ID, REGION_ID)
                  VALUES(pn_terminal_id, pn_region_id); 
            END IF;
            
            -- Find start date
            SELECT GREATEST(l_last_deactivate_date, NVL(MIN(x.CLOSE_DATE), MIN_DATE), l_activate_date - 30)
              INTO l_start_date
              FROM REPORT.TRANS x
             WHERE x.EPORT_ID = l_eport_id
               AND x.CLOSE_DATE >= l_last_deactivate_date
               AND x.CLOSE_DATE > l_activate_date - 30; 
              
         --INSERT INTO TERMINAL_EPORT
         /* A much faster way - BSK 04/10/2007
         TERMINAL_EPORT_UPD(pn_terminal_id, l_eport_id, l_start_date, NULL, NULL, 'Y');*/
         INSERT INTO REPORT.TERMINAL_EPORT(EPORT_ID, TERMINAL_ID, START_DATE, END_DATE)
            VALUES(l_eport_id, pn_terminal_id, l_start_date, NULL);
        
         --UPDATE DEALER_EPORT
         UPDATE CORP.DEALER_EPORT SET ACTIVATE_DATE = l_activate_date WHERE DEALER_ID = pn_dealer_id AND EPORT_ID = l_eport_id;
         --INSERT INTO USER_TERMINAL
         INSERT INTO USER_TERMINAL(USER_ID, TERMINAL_ID, ALLOW_EDIT)
         /* A much faster way - BSK 04/10/2007
              SELECT USER_ID, pn_terminal_id, 'Y' FROM USER_LOGIN WHERE CAN_ADMIN_USER(ln_user_id, USER_ID) = 'Y'; */
             SELECT USER_ID, pn_terminal_id, 'Y'
               FROM REPORT.USER_LOGIN u
               CONNECT BY PRIOR u.admin_id = u.user_id
               START WITH u.user_id = ln_user_id
             UNION
             SELECT a.USER_ID, pn_terminal_id, 'Y'
               FROM REPORT.USER_LOGIN a
              INNER JOIN REPORT.USER_PRIVS up
                 ON a.USER_ID = up.user_id
              WHERE a.CUSTOMER_ID = l_customer_id
                AND up.priv_id IN(5,8);
    
         --INSERT INTO CORP.CUSTOMER_BANK_TERMINAL
         IF pn_customer_bank_id <> 0 THEN
             INSERT INTO CORP.CUSTOMER_BANK_TERMINAL(TERMINAL_ID, CUSTOMER_BANK_ID, START_DATE)
               VALUES(pn_terminal_id, pn_customer_bank_id, NULL); --l_start_date); : Use NULL (or MIN_DATE) instead
         END IF;
         --INSERT INTO SERVICE_FEES
         -- frequency interval so that first active day has a service fee (BSK 09-14-04)
         -- made start dates NULL (or MIN_DATE)
         INSERT INTO CORP.SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID, FEE_AMOUNT, FREQUENCY_ID, LAST_PAYMENT, START_DATE, FEE_PERCENT)
              SELECT CORP.SERVICE_FEE_SEQ.NEXTVAL, pn_terminal_id, lsf.FEE_ID,
                     DECODE(lsf.FEE_ID, 8, 0, lsf.AMOUNT), lsf.FREQUENCY_ID,
                     DECODE(lsf.START_IMMEDIATELY_FLAG, 'Y', l_start_date), l_start_date,
                     DECODE(lsf.FEE_ID, 8, lsf.AMOUNT, NULL)
                FROM CORP.LICENSE_SERVICE_FEES lsf, CORP.FREQUENCY f
               WHERE lsf.FREQUENCY_ID = f.FREQUENCY_ID
                 AND lsf.LICENSE_ID = l_license_id
                 AND (pc_dex_data IN('A', 'D') OR lsf.FEE_ID != 4);
         IF pc_dex_data IN('A', 'D') THEN
             INSERT INTO CORP.SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID, FEE_AMOUNT, FREQUENCY_ID, LAST_PAYMENT, START_DATE)
                 SELECT CORP.SERVICE_FEE_SEQ.NEXTVAL, pn_terminal_id, 4,
                       4.00, f.FREQUENCY_ID, NULL, l_start_date
                  FROM CORP.FREQUENCY f
                 WHERE f.FREQUENCY_ID = 2
                     AND NOT EXISTS(SELECT 1 
                                      FROM CORP.LICENSE_SERVICE_FEES lsf 
                                     WHERE lsf.LICENSE_ID = l_license_id
                                       AND lsf.FEE_ID = 4);
         END IF;
         --INSERT INTO PROCESS_FEES (Added FEE_AMOUNT - BSK 09-17-04)
         INSERT INTO CORP.PROCESS_FEES(PROCESS_FEE_ID, TERMINAL_ID, TRANS_TYPE_ID, FEE_PERCENT, FEE_AMOUNT, MIN_AMOUNT, START_DATE)
             SELECT CORP.PROCESS_FEE_SEQ.NEXTVAL, pn_terminal_id, TRANS_TYPE_ID, FEE_PERCENT, FEE_AMOUNT, MIN_AMOUNT, NULL
             FROM CORP.LICENSE_PROCESS_FEES WHERE LICENSE_ID = l_license_id;
    END;

    PROCEDURE INTERNAL_UPDATE_TERMINAL(
       pn_terminal_id IN REPORT.TERMINAL.TERMINAL_ID%TYPE,
       pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
       pn_machine_id IN REPORT.TERMINAL.MACHINE_ID%TYPE,
       pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
       pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
       pn_region_id IN REPORT.REGION.REGION_ID%TYPE,
       pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
       pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
       pn_address_id REPORT.TERMINAL_ADDR.ADDRESS_ID%TYPE,
       pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
       pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
       pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
       pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
       pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
       pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
       pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
       pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
       pn_pc_id IN REPORT.TERMINAL.PRIMARY_CONTACT_ID%TYPE,
       pn_sc_id IN REPORT.TERMINAL.SECONDARY_CONTACT_ID%TYPE,
       pn_location_type_id IN REPORT.LOCATION.LOCATION_TYPE_ID%TYPE,
       pn_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
       pn_product_type_id IN REPORT.TERMINAL.PRODUCT_TYPE_ID%TYPE,
       pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
       pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
       pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
       pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
       pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
       pn_vends REPORT.TERMINAL.VENDS%TYPE,
       pv_term_var_1 REPORT.TERMINAL.term_var_1%TYPE,
       pv_term_var_2 REPORT.TERMINAL.term_var_2%TYPE,
       pv_term_var_3 REPORT.TERMINAL.term_var_3%TYPE,
       pv_term_var_4 REPORT.TERMINAL.term_var_4%TYPE,
       pv_term_var_5 REPORT.TERMINAL.term_var_5%TYPE,
       pv_term_var_6 REPORT.TERMINAL.term_var_6%TYPE,
       pv_term_var_7 REPORT.TERMINAL.term_var_7%TYPE,
       pv_term_var_8 REPORT.TERMINAL.term_var_8%TYPE)    
    IS
      pv_location_name_id REPORT.TERMINAL.LOCATION_ID%TYPE;
      l_customer_id REPORT.TERMINAL.CUSTOMER_ID%TYPE;
      l_old_cb_id NUMBER;
      l_old_lname REPORT.LOCATION.LOCATION_NAME%TYPE;
      l_old_ldesc REPORT.LOCATION.DESCRIPTION%TYPE;
      l_new_address_id REPORT.TERMINAL_ADDR.ADDRESS_ID%TYPE;
      l_old_address_id REPORT.LOCATION.ADDRESS_ID%TYPE;
      l_old_dex REPORT.TERMINAL.DEX_DATA%TYPE;
      isnew BOOLEAN;
      l_date DATE := SYSDATE;
	  l_lock VARCHAR2(128);
      ln_business_unit_id REPORT.TERMINAL.BUSINESS_UNIT_ID%TYPE;
      ln_old_pay_sched_id REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE;
      ln_old_region_id REPORT.REGION.REGION_ID%TYPE;
	  ln_new_pay_sched_id REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE;
    BEGIN
		 l_lock := GLOBALS_PKG.REQUEST_LOCK('TERMINAL.TERMINAL_ID', pn_terminal_id);	
         -- if a new location is named than create it
         SELECT T.LOCATION_ID, T.CUSTOMER_ID, L.LOCATION_NAME, L.DESCRIPTION, L.ADDRESS_ID, CB.CUSTOMER_BANK_ID, T.DEX_DATA, T.BUSINESS_UNIT_ID, T.PAYMENT_SCHEDULE_ID, NVL(TR.REGION_ID, 0)
           INTO pv_location_name_id, l_customer_id, l_old_lname, l_old_ldesc, l_old_address_id, l_old_cb_id, l_old_dex, ln_business_unit_id, ln_old_pay_sched_id, ln_old_region_id
           FROM REPORT.TERMINAL T
           LEFT OUTER JOIN REPORT.LOCATION L ON T.LOCATION_ID = L.LOCATION_ID
           LEFT OUTER JOIN CORP.VW_CURRENT_BANK_ACCT CB ON T.TERMINAL_ID = CB.TERMINAL_ID
           LEFT OUTER JOIN REPORT.TERMINAL_REGION TR ON T.TERMINAL_ID = TR.TERMINAL_ID
          WHERE T.TERMINAL_ID = pn_terminal_id;
         IF EQL(l_old_lname, pv_location_name) <> -1 THEN  -- location has changed
             UPDATE REPORT.LOCATION SET status = 'D' WHERE LOCATION_ID = pv_location_name_id;
            INSERT INTO REPORT.TERMINAL_CHANGES (TERMINAL_ID, ATTRIBUTE, OLD_VALUE, NEW_VALUE)
                   VALUES(pn_terminal_id, 'LOCATION',l_old_lname, pv_location_name);
            BEGIN
               SELECT LOCATION_ID INTO pv_location_name_id FROM REPORT.LOCATION WHERE LOCATION_NAME = pv_location_name AND TERMINAL_ID = pn_terminal_id;
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                     isnew := true;
               WHEN OTHERS THEN
                  RAISE;
            END;
         END IF;
         --RECORD CHANGES (SO USALIVE CAN BE UPDATED)
         DECLARE
             l_z TERMINAL.TIME_ZONE_ID%TYPE;
             l_cnt NUMBER;
         BEGIN
              SELECT TIME_ZONE_ID
                INTO l_z 
                FROM REPORT.TERMINAL 
               WHERE TERMINAL_ID = pn_terminal_id;
             IF EQL(l_z, pn_time_zone_id) <> -1 THEN
                 UPDATE REPORT.TERMINAL_CHANGES SET NEW_VALUE = pn_time_zone_id WHERE TERMINAL_ID = pn_terminal_id
                   AND ATTRIBUTE = 'TIME_ZONE' RETURNING 1 INTO l_cnt;
                IF NVL(l_cnt, 0) = 0 THEN
                    INSERT INTO REPORT.TERMINAL_CHANGES (TERMINAL_ID, ATTRIBUTE, OLD_VALUE, NEW_VALUE)
                   VALUES(pn_terminal_id, 'TIME_ZONE',l_z, pn_time_zone_id);
                END IF;
             END IF;
         END;
         -- UPDATE ADDRESS
         IF pn_address_id IS NULL THEN    -- none specified
            l_new_address_id := NULL;
         ELSIF pn_address_id = 0 THEN    --IS NEW
            SELECT REPORT.TERMINAL_ADDR_SEQ.NEXTVAL INTO l_new_address_id FROM DUAL;
            INSERT INTO REPORT.TERMINAL_ADDR(ADDRESS_ID, CUSTOMER_ID, ADDRESS1, CITY, STATE, ZIP, COUNTRY_CD)
                   VALUES(l_new_address_id, l_customer_id, pv_address1, pv_city, pv_state_cd, pv_postal, pv_country_cd);
         ELSE
             UPDATE REPORT.TERMINAL_ADDR SET ADDRESS1 = pv_address1, CITY = pv_city, STATE = pv_state_cd, ZIP = pv_postal, COUNTRY_CD = pv_country_cd
                WHERE ADDRESS_ID = pn_address_id AND (EQL(ADDRESS1, pv_address1) = 0
                OR EQL(CITY, pv_city) = 0 OR EQL(STATE, pv_state_cd) = 0 OR EQL(ZIP, pv_postal) = 0 OR EQL(COUNTRY_CD, pv_country_cd) = 0);
            l_new_address_id := pn_address_id;
         END IF;
         IF isnew THEN
             SELECT REPORT.LOCATION_SEQ.NEXTVAL INTO pv_location_name_id FROM DUAL;
             INSERT INTO REPORT.LOCATION(LOCATION_ID, LOCATION_NAME, DESCRIPTION, ADDRESS_ID, CREATE_BY, TERMINAL_ID, LOCATION_TYPE_ID, LOCATION_TYPE_SPECIFY)
                   VALUES(pv_location_name_id,pv_location_name,pv_location_details,l_new_address_id,pn_user_id,pn_terminal_id,pn_location_type_id,pn_location_type_specific);
         ELSE
             UPDATE REPORT.LOCATION L SET (DESCRIPTION, ADDRESS_ID, UPD_DATE, LOCATION_TYPE_ID, LOCATION_TYPE_SPECIFY) =
                   (SELECT pv_location_details,l_new_address_id,l_date,NVL(pn_location_type_id, L.LOCATION_TYPE_ID),DECODE(pn_location_type_id, NULL, L.LOCATION_TYPE_SPECIFY, pn_location_type_specific) FROM DUAL) WHERE location_id = pv_location_name_id;
         END IF;
  
         IF pn_pay_sched_id IS NULL THEN
            ln_new_pay_sched_id := ln_old_pay_sched_id;
         ELSIF pn_pay_sched_id != ln_old_pay_sched_id THEN
            SELECT MAX(PAYMENT_SCHEDULE_ID)
              INTO ln_new_pay_sched_id
              FROM CORP.PAYMENT_SCHEDULE
             WHERE PAYMENT_SCHEDULE_ID = pn_pay_sched_id
               AND SELECTABLE = 'Y';
            IF ln_new_pay_sched_id IS NULL THEN
                RAISE_APPLICATION_ERROR(-20217, 'Invalid payment schedule requested');
            ELSIF ln_business_unit_id IN(2,3,5) THEN
                RAISE_APPLICATION_ERROR(-20217, 'Not permitted to change payment schedule as requested');
            END IF;
         ELSE
            ln_new_pay_sched_id := ln_old_pay_sched_id;
         END IF;
         UPDATE REPORT.TERMINAL T SET (LOCATION_ID, ASSET_NBR, MACHINE_ID, TELEPHONE, PREFIX, PRODUCT_TYPE_ID, PRODUCT_TYPE_SPECIFY,
             PRIMARY_CONTACT_ID, SECONDARY_CONTACT_ID, AUTHORIZATION_MODE, TIME_ZONE_ID, DEX_DATA, RECEIPT, VENDS, PAYMENT_SCHEDULE_ID, STATUS,
             TERM_VAR_1, TERM_VAR_2, TERM_VAR_3, TERM_VAR_4, TERM_VAR_5, TERM_VAR_6, TERM_VAR_7, TERM_VAR_8) =
            (SELECT pv_location_name_id, pv_asset_nbr, pn_machine_id, pv_telephone, pv_prefix, pn_product_type_id, pn_product_type_specific,
             pn_pc_id, pn_sc_id, pc_auth_mode, pn_time_zone_id, NVL(pc_dex_data, T.DEX_DATA), NVL(pc_receipt, T.RECEIPT), pn_vends, ln_new_pay_sched_id, 'U',
             pv_term_var_1, pv_term_var_2, pv_term_var_3, pv_term_var_4, pv_term_var_5, pv_term_var_6, pv_term_var_7, pv_term_var_8 FROM DUAL) 
             WHERE TERMINAL_ID = pn_terminal_id;
         IF NVL(pn_region_id, 0) = ln_old_region_id THEN
            NULL;
         ELSIF ln_old_region_id = 0 THEN
            INSERT INTO REPORT.TERMINAL_REGION(TERMINAL_ID, REGION_ID)
                  VALUES(pn_terminal_id, pn_region_id); 
         ELSIF NVL(pn_region_id, 0) = 0 THEN
            DELETE FROM  REPORT.TERMINAL_REGION
             WHERE TERMINAL_ID = pn_terminal_id;
         ELSE
            UPDATE REPORT.TERMINAL_REGION
               SET REGION_ID = pn_region_id
             WHERE TERMINAL_ID = pn_terminal_id; 
         END IF;
         
         --UPDATE BANK ACCOUNT
         IF EQL(l_old_cb_id, pn_customer_bank_id) = 0 THEN
             DECLARE
                 l_cb_date DATE;
            BEGIN
                UPDATE CORP.CUSTOMER_BANK_TERMINAL SET END_DATE = CASE WHEN END_DATE < l_date THEN END_DATE ELSE l_date END
                 WHERE TERMINAL_ID = pn_terminal_id
                 RETURNING MAX(CASE WHEN END_DATE < l_date THEN END_DATE ELSE l_date END) INTO l_cb_date;
                IF NVL(pn_customer_bank_id, 0) <> 0 THEN
                       INSERT INTO CORP.CUSTOMER_BANK_TERMINAL(CUSTOMER_BANK_TERMINAL_ID, TERMINAL_ID, CUSTOMER_BANK_ID, START_DATE)
                        VALUES(CORP.CUSTOMER_BANK_TERMINAL_SEQ.NEXTVAL, pn_terminal_id, pn_customer_bank_id, l_cb_date);
                END IF;
            END;
         END IF;
         /*
         IF NVL(l_old_cb_id, 0) = 0 AND NVL(pn_customer_bank_id, 0) <> 0 THEN
             DECLARE
                 l_cb_date DATE;
            BEGIN
                 SELECT NVL(MIN(CLOSE_DATE), l_date) INTO l_cb_date FROM TRANS WHERE TERMINAL_ID = pn_terminal_id AND CLOSE_DATE >=
                     (SELECT NVL(MAX(END_DATE), CLOSE_DATE) FROM CORP.CUSTOMER_BANK_TERMINAL
                     WHERE TERMINAL_ID = pn_terminal_id AND END_DATE > NVL(START_DATE, END_DATE));
                 CORP.TERMINAL_REASSIGN_BANK_ACCOUNT(pn_terminal_id, pn_customer_bank_id, l_cb_date);
            END;
         ELSIF EQL(l_old_cb_id, pn_customer_bank_id) = 0 THEN
             CORP.TERMINAL_REASSIGN_BANK_ACCOUNT(pn_terminal_id, pn_customer_bank_id, l_date);
         END IF;
         */
         IF l_old_dex NOT IN('D','A') AND pc_dex_data IN('D','A') THEN
             DECLARE
                ln_service_fee_id CORP.SERVICE_FEES.SERVICE_FEE_ID%TYPE;
                lc_copy CHAR(1);         
             BEGIN
                SELECT SERVICE_FEE_ID, DECODE(END_DATE, NULL, 'N', 'Y')
                  INTO ln_service_fee_id, lc_copy
                  FROM (SELECT SERVICE_FEE_ID, END_DATE
                          FROM CORP.SERVICE_FEES
                         WHERE TERMINAL_ID = pn_terminal_id
                           AND FEE_ID = 4
                         ORDER BY NVL(END_DATE, MAX_DATE) DESC, LAST_PAYMENT DESC, SERVICE_FEE_ID DESC)
                 WHERE ROWNUM = 1;
                IF lc_copy = 'Y' THEN
                INSERT INTO CORP.SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID, FEE_AMOUNT, FREQUENCY_ID, LAST_PAYMENT, START_DATE)
                     SELECT CORP.SERVICE_FEE_SEQ.NEXTVAL, pn_terminal_id, 4, FEE_AMOUNT, FREQUENCY_ID, LAST_PAYMENT, GREATEST(l_date, END_DATE) 
                       FROM CORP.SERVICE_FEES
                      WHERE SERVICE_FEE_ID = ln_service_fee_id;
                END IF;          
             EXCEPTION
                WHEN NO_DATA_FOUND THEN
                     INSERT INTO CORP.SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID, FEE_AMOUNT, FREQUENCY_ID, LAST_PAYMENT, START_DATE)
                         SELECT CORP.SERVICE_FEE_SEQ.NEXTVAL, pn_terminal_id, 4, 4, 2, l_date, l_date FROM DUAL;
             END;
         ELSIF pc_dex_data NOT IN('D','A') AND l_old_dex IN('D','A') THEN
              UPDATE CORP.SERVICE_FEES SET END_DATE = l_date 
               WHERE TERMINAL_ID = pn_terminal_id
                 AND NVL(END_DATE, l_date) >= l_date
                 AND FEE_ID = 4;
         END IF;
    EXCEPTION
         WHEN NO_DATA_FOUND THEN
              RAISE_APPLICATION_ERROR(-20100, 'No such terminal found');
         WHEN OTHERS THEN
             ROLLBACK;
             RAISE;
    END;
    
    PROCEDURE CREATE_TERMINAL(
        pn_terminal_id OUT REPORT.TERMINAL.TERMINAL_ID%TYPE,
        pv_device_serial_cd IN REPORT.EPORT.EPORT_SERIAL_NUM%TYPE,
        pn_dealer_id IN CORP.DEALER.DEALER_ID%TYPE,
        pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
        pn_machine_id IN REPORT.TERMINAL.MACHINE_ID%TYPE,
        pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
        pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
        pn_region_id IN REPORT.REGION.REGION_ID%TYPE,
        pv_region_name IN REPORT.REGION.REGION_NAME%TYPE,
        pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
        pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
        pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
        pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
        pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
        pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
        pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
        pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
        pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_pc_id IN REPORT.TERMINAL.PRIMARY_CONTACT_ID%TYPE,
        pn_sc_id IN REPORT.TERMINAL.SECONDARY_CONTACT_ID%TYPE,
        pn_location_type_id IN REPORT.LOCATION.LOCATION_TYPE_ID%TYPE,
        pv_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
        pn_product_type_id IN REPORT.TERMINAL.PRODUCT_TYPE_ID%TYPE,
        pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
        pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
        pn_avg_amt IN REPORT.TERMINAL.AVG_TRANS_AMT%TYPE,
        pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
        pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
        pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
        pn_vends REPORT.TERMINAL.VENDS%TYPE,
        pv_term_var_1 IN REPORT.TERMINAL.TERM_VAR_1%TYPE,
        pv_term_var_2 IN REPORT.TERMINAL.TERM_VAR_2%TYPE,
        pv_term_var_3 IN REPORT.TERMINAL.TERM_VAR_3%TYPE,
        pv_term_var_4 IN REPORT.TERMINAL.TERM_VAR_4%TYPE,
        pv_term_var_5 IN REPORT.TERMINAL.TERM_VAR_5%TYPE,
        pv_term_var_6 IN REPORT.TERMINAL.TERM_VAR_6%TYPE,
        pv_term_var_7 IN REPORT.TERMINAL.TERM_VAR_7%TYPE,
        pv_term_var_8 IN REPORT.TERMINAL.TERM_VAR_8%TYPE,
		pd_activate_date IN REPORT.TERMINAL_EPORT.START_DATE%TYPE DEFAULT SYSDATE)
    IS
        ln_cnt PLS_INTEGER;
        lv_city REPORT.TERMINAL_ADDR.CITY%TYPE;
        lv_state_cd REPORT.TERMINAL_ADDR.STATE%TYPE;     
        ln_new_region_id REPORT.REGION.REGION_ID%TYPE;        
    BEGIN
        IF pv_state_cd IS NULL OR pv_city IS NULL THEN
            SELECT MAX(CITY), MAX(STATE_CD), MAX(CNT)
              INTO lv_city, lv_state_cd, ln_cnt
              FROM (SELECT P.CITY, S.STATE_CD, 1 CNT
                  FROM FRONT.POSTAL P 
                  JOIN CORP.STATE S ON P.STATE_ID = S.STATE_ID
                WHERE P.POSTAL_CD = pv_postal
                  AND S.COUNTRY_CD = pv_country_cd
                ORDER BY P.POSTAL_ID ASC)
             WHERE ROWNUM = 1;
        ELSE
            SELECT COUNT(*), pv_city, pv_state_cd
              INTO ln_cnt, lv_city, lv_state_cd 
              FROM CORP.STATE 
             WHERE STATE_CD = pv_state_cd
               AND COUNTRY_CD = pv_country_cd; 
        END IF; 
        IF ln_cnt = 0 THEN
            RAISE_APPLICATION_ERROR(-20220, 'Invalid state/country');
            RETURN;
        END IF;
        IF pn_region_id != -1 THEN
            ln_new_region_id := pn_region_id;
        ELSIF TRIM(pv_region_name) IS NULL THEN
            ln_new_region_id := 0;
        ELSE
            REPORT.ADD_REGION(ln_new_region_id, pv_region_name);
        END IF;
        INTERNAL_CREATE_TERMINAL(
            pn_terminal_id,
            pv_device_serial_cd,
            pn_dealer_id,
            pv_asset_nbr,
            pn_machine_id,
            pv_telephone,
            pv_prefix,
            ln_new_region_id,
            pv_location_name,
            pv_location_details,
            pv_address1,
            lv_city,
            lv_state_cd,
            pv_postal,
            pv_country_cd,
            pn_customer_bank_id,
            pn_pay_sched_id,
            pn_user_id,
            pn_pc_id,
            pn_sc_id,
            pn_location_type_id,
            pv_location_type_specific,
            pn_product_type_id,
            pn_product_type_specific,
            pc_auth_mode,
            pn_avg_amt,
            pn_time_zone_id,
            pc_dex_data,
            pc_receipt,
            pn_vends,
            pv_term_var_1,
            pv_term_var_2,
            pv_term_var_3,
            pv_term_var_4,
            pv_term_var_5,
            pv_term_var_6,
            pv_term_var_7,
            pv_term_var_8,
			pd_activate_date);
    END;

    PROCEDURE UPDATE_TERMINAL(
       pn_terminal_id IN REPORT.TERMINAL.TERMINAL_ID%TYPE,
       pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
       pn_machine_id IN REPORT.TERMINAL.MACHINE_ID%TYPE,
       pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
       pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
       pn_region_id IN REPORT.REGION.REGION_ID%TYPE,
       pv_region_name IN REPORT.REGION.REGION_NAME%TYPE,
       pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
       pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
       pn_address_id REPORT.TERMINAL_ADDR.ADDRESS_ID%TYPE,
       pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
       pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
       pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
       pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
       pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
       pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
       pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
       pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
       pn_pc_id IN REPORT.TERMINAL.PRIMARY_CONTACT_ID%TYPE,
       pn_sc_id IN REPORT.TERMINAL.SECONDARY_CONTACT_ID%TYPE,
       pn_location_type_id IN REPORT.LOCATION.LOCATION_TYPE_ID%TYPE,
       pn_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
       pn_product_type_id IN REPORT.TERMINAL.PRODUCT_TYPE_ID%TYPE,
       pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
       pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
       pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
       pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
       pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
       pn_vends REPORT.TERMINAL.VENDS%TYPE,
       pv_term_var_1 REPORT.TERMINAL.term_var_1%TYPE,
       pv_term_var_2 REPORT.TERMINAL.term_var_2%TYPE,
       pv_term_var_3 REPORT.TERMINAL.term_var_3%TYPE,
       pv_term_var_4 REPORT.TERMINAL.term_var_4%TYPE,
       pv_term_var_5 REPORT.TERMINAL.term_var_5%TYPE,
       pv_term_var_6 REPORT.TERMINAL.term_var_6%TYPE,
       pv_term_var_7 REPORT.TERMINAL.term_var_7%TYPE,
       pv_term_var_8 REPORT.TERMINAL.term_var_8%TYPE)    
    IS
        ln_cnt PLS_INTEGER;
        lv_city REPORT.TERMINAL_ADDR.CITY%TYPE;
        lv_state_cd REPORT.TERMINAL_ADDR.STATE%TYPE;   
        ln_new_region_id REPORT.REGION.REGION_ID%TYPE;        
    BEGIN
        IF pv_state_cd IS NULL OR pv_city IS NULL THEN
            SELECT MAX(CITY), MAX(STATE_CD), MAX(CNT)
              INTO lv_city, lv_state_cd, ln_cnt
              FROM (SELECT P.CITY, S.STATE_CD, 1 CNT
                  FROM FRONT.POSTAL P 
                  JOIN CORP.STATE S ON P.STATE_ID = S.STATE_ID
                WHERE P.POSTAL_CD = pv_postal
                  AND S.COUNTRY_CD = pv_country_cd
                ORDER BY P.POSTAL_ID ASC)
             WHERE ROWNUM = 1;
        ELSE
            SELECT COUNT(*), pv_city, pv_state_cd
              INTO ln_cnt, lv_city, lv_state_cd
              FROM CORP.STATE 
             WHERE STATE_CD = pv_state_cd
               AND COUNTRY_CD = pv_country_cd; 
        END IF;
        IF ln_cnt = 0 THEN
            RAISE_APPLICATION_ERROR(-20220, 'Invalid state/country');
        ELSE
            IF pn_region_id != -1 THEN
                ln_new_region_id := pn_region_id;
            ELSIF TRIM(pv_region_name) IS NULL THEN
                ln_new_region_id := 0;
            ELSE
                REPORT.ADD_REGION(ln_new_region_id, pv_region_name);
            END IF;
            INTERNAL_UPDATE_TERMINAL(
               pn_terminal_id,
               pv_asset_nbr,
               pn_machine_id,
               pv_telephone,
               pv_prefix,
               ln_new_region_id,
               pv_location_name,
               pv_location_details,
               pn_address_id,
               pv_address1,
               lv_city,
               lv_state_cd,
               pv_postal,
               pv_country_cd,
               pn_customer_bank_id,
               pn_pay_sched_id,
               pn_user_id,
               pn_pc_id,
               pn_sc_id,
               pn_location_type_id,
               pn_location_type_specific,
               pn_product_type_id,
               pn_product_type_specific,
               pc_auth_mode,
               pn_time_zone_id,
               pc_dex_data,
               pc_receipt,
               pn_vends,
               pv_term_var_1,
               pv_term_var_2,
               pv_term_var_3,
               pv_term_var_4,
               pv_term_var_5,
               pv_term_var_6,
               pv_term_var_7,
               pv_term_var_8);
        END IF;
    END;
        
    PROCEDURE CREATE_TERMINAL_MASS(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
		pn_terminal_id OUT REPORT.TERMINAL.TERMINAL_ID%TYPE,
        pv_device_serial_cd IN REPORT.EPORT.EPORT_SERIAL_NUM%TYPE,
        pn_dealer_id IN CORP.DEALER.DEALER_ID%TYPE,
        pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
        pv_machine_make REPORT.MACHINE.MAKE%TYPE,
		pv_machine_model REPORT.MACHINE.MODEL%TYPE,
        pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
        pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
        pv_region_name IN REPORT.REGION.REGION_NAME%TYPE,
        pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
        pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
        pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
        pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
        pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
        pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
        pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
        pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
        pv_location_type_name REPORT.LOCATION_TYPE.LOCATION_TYPE_NAME%TYPE,
        pn_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
        pv_product_type_name REPORT.PRODUCT_TYPE.PRODUCT_TYPE_NAME%TYPE,
        pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
        pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
        pn_avg_amt IN REPORT.TERMINAL.AVG_TRANS_AMT%TYPE,
        pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
        pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
        pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
        pn_vends REPORT.TERMINAL.VENDS%TYPE,
        pv_term_var_1 REPORT.TERMINAL.term_var_1%TYPE,
        pv_term_var_2 REPORT.TERMINAL.term_var_2%TYPE,
        pv_term_var_3 REPORT.TERMINAL.term_var_3%TYPE,
        pv_term_var_4 REPORT.TERMINAL.term_var_4%TYPE,
        pv_term_var_5 REPORT.TERMINAL.term_var_5%TYPE,
        pv_term_var_6 REPORT.TERMINAL.term_var_6%TYPE,
        pv_term_var_7 REPORT.TERMINAL.term_var_7%TYPE,
        pv_term_var_8 REPORT.TERMINAL.term_var_8%TYPE,
		pd_activate_date IN REPORT.TERMINAL_EPORT.START_DATE%TYPE DEFAULT SYSDATE)
    IS
        ln_machine_id REPORT.MACHINE.MACHINE_ID%TYPE;
        ln_product_type_id REPORT.PRODUCT_TYPE.PRODUCT_TYPE_ID%TYPE;
        ln_location_type_id REPORT.LOCATION_TYPE.LOCATION_TYPE_ID%TYPE;	
        ln_cnt PLS_INTEGER;
        lv_city REPORT.TERMINAL_ADDR.CITY%TYPE;
        lv_state_cd REPORT.TERMINAL_ADDR.STATE%TYPE;     
        ln_region_id REPORT.REGION.REGION_ID%TYPE;        
    BEGIN
        BEGIN
            SELECT MAX(MACHINE_ID)
              INTO ln_machine_id
              FROM REPORT.MACHINE
             WHERE UPPER(MAKE) = UPPER(pv_machine_make)
               AND UPPER(MODEL) = UPPER(pv_machine_model)
               AND STATUS IN('A','I');
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                REPORT.MACHINE_INS(ln_machine_id, pv_machine_make, pv_machine_model, pn_user_id);
        END;
        BEGIN
            IF pv_location_type_name IS NOT NULL AND LENGTH(TRIM(pv_location_type_name)) > 0 THEN
                SELECT LOCATION_TYPE_ID
                  INTO ln_location_type_id
                  FROM REPORT.LOCATION_TYPE
                 WHERE UPPER(LOCATION_TYPE_NAME) = UPPER(pv_location_type_name)
                   AND STATUS IN('A','I');
            ELSE
                ln_location_type_id := 0;
            END IF;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                RAISE_APPLICATION_ERROR(-20211, 'Invalid location type name');
        END;
        BEGIN
            SELECT PRODUCT_TYPE_ID
              INTO ln_product_type_id
              FROM REPORT.PRODUCT_TYPE
             WHERE UPPER(PRODUCT_TYPE_NAME) = UPPER(pv_product_type_name)
               AND STATUS IN('A','I');
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                RAISE_APPLICATION_ERROR(-20212, 'Invalid product type name');
        END;

         IF pv_state_cd IS NULL OR pv_city IS NULL THEN
            SELECT MAX(CITY), MAX(STATE_CD), MAX(CNT)
              INTO lv_city, lv_state_cd, ln_cnt
              FROM (SELECT P.CITY, S.STATE_CD, 1 CNT
                  FROM FRONT.POSTAL P 
                  JOIN CORP.STATE S ON P.STATE_ID = S.STATE_ID
                WHERE P.POSTAL_CD = pv_postal
                  AND S.COUNTRY_CD = pv_country_cd
                ORDER BY P.POSTAL_ID ASC)
             WHERE ROWNUM = 1;
        ELSE
            SELECT COUNT(*), pv_city, pv_state_cd
              INTO ln_cnt, lv_city, lv_state_cd 
              FROM CORP.STATE 
             WHERE STATE_CD = pv_state_cd
               AND COUNTRY_CD = pv_country_cd; 
        END IF; 
        IF ln_cnt = 0 THEN
            RAISE_APPLICATION_ERROR(-20220, 'Invalid state/country');
            RETURN;
        END IF;
        IF TRIM(pv_region_name) IS NOT NULL THEN
            REPORT.ADD_REGION(ln_region_id, pv_region_name);
        ELSE
            ln_region_id := 0;
        END IF;
        INTERNAL_CREATE_TERMINAL(
               pn_terminal_id,
               pv_device_serial_cd,
               pn_dealer_id,
               pv_asset_nbr,
               ln_machine_id,
               pv_telephone,
               pv_prefix,
               ln_region_id,
               pv_location_name,
               pv_location_details,
               pv_address1,
               lv_city,
               lv_state_cd,
               pv_postal,
               pv_country_cd,
               pn_customer_bank_id,
               pn_pay_sched_id,
               pn_user_id,
               pn_user_id,
               NULL,
               ln_location_type_id,
               pn_location_type_specific,
               ln_product_type_id,
               pn_product_type_specific,
               pc_auth_mode,
               pn_avg_amt,
               pn_time_zone_id,
               pc_dex_data,
               pc_receipt,
               pn_vends,
               pv_term_var_1,
               pv_term_var_2,
               pv_term_var_3,
               pv_term_var_4,
               pv_term_var_5,
               pv_term_var_6,
               pv_term_var_7,
               pv_term_var_8,
			   pd_activate_date);
    END;
    
    PROCEDURE UPDATE_TERMINAL_MASS(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
		pn_terminal_id IN REPORT.TERMINAL.TERMINAL_ID%TYPE,
        pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
        pv_machine_make REPORT.MACHINE.MAKE%TYPE,
		pv_machine_model REPORT.MACHINE.MODEL%TYPE,
        pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
        pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
        pv_region_name IN REPORT.REGION.REGION_NAME%TYPE,
        pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
        pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
        pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
        pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
        pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
        pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
        pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
        pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
        pn_location_type_id REPORT.LOCATION_TYPE.LOCATION_TYPE_ID%TYPE,
		pv_location_type_name REPORT.LOCATION_TYPE.LOCATION_TYPE_NAME%TYPE,
        pn_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
        pv_product_type_name REPORT.PRODUCT_TYPE.PRODUCT_TYPE_NAME%TYPE,
        pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
        pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
        pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
        pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
        pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
        pn_vends REPORT.TERMINAL.VENDS%TYPE,
        pv_term_var_1 REPORT.TERMINAL.term_var_1%TYPE,
        pv_term_var_2 REPORT.TERMINAL.term_var_2%TYPE,
        pv_term_var_3 REPORT.TERMINAL.term_var_3%TYPE,
        pv_term_var_4 REPORT.TERMINAL.term_var_4%TYPE,
        pv_term_var_5 REPORT.TERMINAL.term_var_5%TYPE,
        pv_term_var_6 REPORT.TERMINAL.term_var_6%TYPE,
        pv_term_var_7 REPORT.TERMINAL.term_var_7%TYPE,
        pv_term_var_8 REPORT.TERMINAL.term_var_8%TYPE)
    IS
        ln_machine_id REPORT.MACHINE.MACHINE_ID%TYPE;
        ln_product_type_id REPORT.PRODUCT_TYPE.PRODUCT_TYPE_ID%TYPE;
        ln_location_type_id REPORT.LOCATION_TYPE.LOCATION_TYPE_ID%TYPE;	
        ln_cnt PLS_INTEGER;
        lv_city REPORT.TERMINAL_ADDR.CITY%TYPE;
        lv_state_cd REPORT.TERMINAL_ADDR.STATE%TYPE;  
        ln_region_id REPORT.REGION.REGION_ID%TYPE;        
    BEGIN
        BEGIN
            SELECT MAX(MACHINE_ID)
              INTO ln_machine_id
              FROM REPORT.MACHINE
             WHERE UPPER(MAKE) = UPPER(pv_machine_make)
               AND UPPER(MODEL) = UPPER(pv_machine_model)
               AND STATUS IN('A','I');
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                REPORT.MACHINE_INS(ln_machine_id, pv_machine_make, pv_machine_model, pn_user_id);
        END;
        BEGIN
            IF pv_location_type_name IS NOT NULL AND LENGTH(TRIM(pv_location_type_name)) > 0 THEN
                SELECT LOCATION_TYPE_ID
                  INTO ln_location_type_id
                  FROM REPORT.LOCATION_TYPE
                 WHERE UPPER(LOCATION_TYPE_NAME) = UPPER(pv_location_type_name)
                   AND STATUS IN('A','I');
            ELSIF pn_location_type_id IS NULL THEN
                ln_location_type_id := 0;
            ELSE
                ln_location_type_id := pn_location_type_id;
            END IF;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                RAISE_APPLICATION_ERROR(-20211, 'Invalid location type name');
        END;
        BEGIN
            SELECT PRODUCT_TYPE_ID
              INTO ln_product_type_id
              FROM REPORT.PRODUCT_TYPE
             WHERE UPPER(PRODUCT_TYPE_NAME) = UPPER(pv_product_type_name)
               AND STATUS IN('A','I');
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                RAISE_APPLICATION_ERROR(-20212, 'Invalid product type name');
        END;

         IF pv_state_cd IS NULL OR pv_city IS NULL THEN
            SELECT MAX(CITY), MAX(STATE_CD), MAX(CNT)
              INTO lv_city, lv_state_cd, ln_cnt
              FROM (SELECT P.CITY, S.STATE_CD, 1 CNT
                  FROM FRONT.POSTAL P 
                  JOIN CORP.STATE S ON P.STATE_ID = S.STATE_ID
                WHERE P.POSTAL_CD = pv_postal
                  AND S.COUNTRY_CD = pv_country_cd
                ORDER BY P.POSTAL_ID ASC)
             WHERE ROWNUM = 1;
        ELSE
            SELECT COUNT(*), pv_city, pv_state_cd
              INTO ln_cnt, lv_city, lv_state_cd
              FROM CORP.STATE 
             WHERE STATE_CD = pv_state_cd
               AND COUNTRY_CD = pv_country_cd; 
        END IF; 
        IF ln_cnt = 0 THEN
            RAISE_APPLICATION_ERROR(-20220, 'Invalid state/country');
            RETURN;
        END IF;
        IF TRIM(pv_region_name) IS NOT NULL THEN
            REPORT.ADD_REGION(ln_region_id, pv_region_name);
        ELSE
            ln_region_id := 0;
        END IF;
        INTERNAL_UPDATE_TERMINAL(
               pn_terminal_id,
               pv_asset_nbr,
               ln_machine_id,
               pv_telephone,
               pv_prefix,
               ln_region_id,
               pv_location_name,
               pv_location_details,
               0,  /* address id (0=create a new address)*/
               pv_address1,
               lv_city,
               lv_state_cd,
               pv_postal,
               pv_country_cd,
               pn_customer_bank_id,
               pn_pay_sched_id,
               pn_user_id,
               pn_user_id,
               NULL,
               ln_location_type_id,
               pn_location_type_specific,
               ln_product_type_id,
               pn_product_type_specific,
               pc_auth_mode,
               pn_time_zone_id,
               pc_dex_data,
               pc_receipt,
               pn_vends,
               pv_term_var_1,
               pv_term_var_2,
               pv_term_var_3,
               pv_term_var_4,
               pv_term_var_5,
               pv_term_var_6,
               pv_term_var_7,
               pv_term_var_8);
    END;

    PROCEDURE CREATE_CUSTOMER_BANK(
        pn_customer_bank_id OUT CORP.CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        pn_customer_id IN CORP.CUSTOMER_BANK.CUSTOMER_ID%TYPE,
        pn_user_id IN CORP.CUSTOMER.USER_ID%TYPE,
        pv_bank_name IN CORP.CUSTOMER_BANK.BANK_NAME%TYPE,
        pv_bank_address1 IN CORP.CUSTOMER_BANK.BANK_ADDRESS%TYPE,
        pv_bank_city IN CORP.CUSTOMER_BANK.BANK_CITY%TYPE,
        pv_bank_state_cd IN CORP.CUSTOMER_BANK.BANK_STATE%TYPE,
        pv_bank_postal IN CORP.CUSTOMER_BANK.BANK_ZIP%TYPE,
        pv_bank_country_cd IN CORP.CUSTOMER_BANK.BANK_COUNTRY_CD%TYPE,
        pv_account_title IN CORP.CUSTOMER_BANK.ACCOUNT_TITLE%TYPE,
        pv_account_type IN CORP.CUSTOMER_BANK.ACCOUNT_TYPE%TYPE,
        pv_bank_routing_nbr IN CORP.CUSTOMER_BANK.BANK_ROUTING_NBR%TYPE,
        pv_bank_acct_nbr IN CORP.CUSTOMER_BANK.BANK_ACCT_NBR%TYPE,
        pv_contact_name IN CORP.CUSTOMER_BANK.CONTACT_NAME%TYPE,
        pv_contact_title IN CORP.CUSTOMER_BANK.CONTACT_TITLE%TYPE,
        pv_contact_telephone IN CORP.CUSTOMER_BANK.CONTACT_TELEPHONE%TYPE,
        pv_contact_fax IN CORP.CUSTOMER_BANK.CONTACT_FAX%TYPE)
    IS
    BEGIN
        SELECT CORP.CUSTOMER_BANK_SEQ.NEXTVAL INTO pn_customer_bank_id FROM DUAL;
        INSERT INTO CORP.CUSTOMER_BANK(CUSTOMER_BANK_ID, CUSTOMER_ID, BANK_NAME, BANK_ADDRESS, BANK_CITY, BANK_STATE, BANK_ZIP, BANK_COUNTRY_CD, CREATE_BY,
            ACCOUNT_TITLE, ACCOUNT_TYPE, BANK_ROUTING_NBR,	BANK_ACCT_NBR, CONTACT_NAME, CONTACT_TITLE, CONTACT_TELEPHONE, CONTACT_FAX)
            VALUES(pn_customer_bank_id, pn_customer_id, pv_bank_name, pv_bank_address1, pv_bank_city, pv_bank_state_cd, pv_bank_postal, pv_bank_country_cd, pn_user_id,
                   pv_account_title, pv_account_type, pv_bank_routing_nbr, pv_bank_acct_nbr, pv_contact_name, pv_contact_title, pv_contact_telephone, pv_contact_fax);
        INSERT INTO CORP.USER_CUSTOMER_BANK (CUSTOMER_BANK_ID, USER_ID, ALLOW_EDIT)
            VALUES(pn_customer_bank_id, pn_user_id, 'Y');
    END;
    
    PROCEDURE UPDATE_REGION_NAME(
        pn_region_id REPORT.REGION.REGION_ID%TYPE,
        pv_region_name REPORT.REGION.REGION_NAME%TYPE,
        pn_customer_id REPORT.TERMINAL.CUSTOMER_ID%TYPE)
    IS
        ln_new_region_id REPORT.REGION.REGION_ID%TYPE;
    BEGIN
        IF pv_region_name IS NULL THEN
            DELETE FROM REPORT.TERMINAL_REGION
             WHERE REGION_ID = pn_region_id
               AND TERMINAL_ID IN(SELECT TERMINAL_ID FROM REPORT.TERMINAL WHERE CUSTOMER_ID = pn_customer_id);
        ELSE
            REPORT.ADD_REGION(ln_new_region_id, pv_region_name);       
            IF pn_region_id = 0 THEN
                INSERT INTO REPORT.TERMINAL_REGION(TERMINAL_ID, REGION_ID)
                  SELECT T.TERMINAL_ID, ln_new_region_id
                    FROM REPORT.TERMINAL T
                   WHERE T.CUSTOMER_ID = pn_customer_id
                     AND NOT EXISTS(SELECT 1 FROM REPORT.TERMINAL_REGION TR WHERE T.TERMINAL_ID = TR.TERMINAL_ID);
            ELSE
                UPDATE REPORT.TERMINAL_REGION
                   SET REGION_ID = ln_new_region_id
                 WHERE REGION_ID = pn_region_id
                   AND TERMINAL_ID IN(SELECT TERMINAL_ID FROM REPORT.TERMINAL WHERE CUSTOMER_ID = pn_customer_id);
            END IF;
        END IF;
    END;        
END;
/
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/PKG_REPORT.pbk?rev=1.8
CREATE OR REPLACE PACKAGE BODY REPORT.PKG_REPORT AS   
    -- USALive 1.7 Signature
    PROCEDURE GET_OR_CREATE_REPORT_REQUEST(
        pn_profile_id REPORT.REPORT_REQUEST.PROFILE_ID%TYPE,
        pn_generator_id REPORT.REPORT_REQUEST.GENERATOR_ID%TYPE,
        pn_report_and_params_hash REPORT.REPORT_REQUEST.REPORT_AND_PARAMS_HASH%TYPE,
        pl_report_xml REPORT.REPORT_REQUEST.REPORT_XML%TYPE,
        pl_report_params REPORT.REPORT_REQUEST.REPORT_PARAMS%TYPE,
        pv_base_url REPORT.REPORT_REQUEST.BASE_URL%TYPE,
        pv_time_zone_guid REPORT.REPORT_REQUEST.TIME_ZONE_GUID%TYPE,
        pv_locale_cd REPORT.REPORT_REQUEST.LOCALE_CD%TYPE,
        pv_run_report_action REPORT.REPORT_REQUEST.RUN_REPORT_ACTION%TYPE,
        pn_report_request_type_id REPORT.REPORT_REQUEST.REPORT_REQUEST_TYPE_ID%TYPE,
        pn_refresh_stored_file_id REPORT.REPORT_REQUEST.STORED_FILE_ID%TYPE,
        pv_report_name REPORT.REPORT_REQUEST.REPORT_NAME%TYPE,
        pc_compressed_flag REPORT.REPORT_REQUEST.COMPRESSED_FLAG%TYPE,
        pn_request_id OUT REPORT.REPORT_REQUEST.REPORT_REQUEST_ID%TYPE,
        pn_stored_file_id OUT REPORT.REPORT_REQUEST.STORED_FILE_ID%TYPE,
        pv_stored_file_name OUT REPORT.STORED_FILE.STORED_FILE_NAME%TYPE,
        pv_stored_file_key OUT REPORT.STORED_FILE.STORED_FILE_KEY%TYPE,
        pv_stored_file_contenttype OUT REPORT.STORED_FILE.STORED_FILE_CONTENTTYPE%TYPE,
        pv_stored_file_passcode OUT REPORT.STORED_FILE.STORED_FILE_PASSCODE%TYPE,
        pn_stored_file_expiration OUT NUMBER,
        pn_reusing_request_flag OUT VARCHAR2)
    IS
        lv_lock VARCHAR2(128);
        lv_orig_report_name REPORT.REPORT_REQUEST.REPORT_NAME%TYPE;
        ln_status_id REPORT.REPORT_REQUEST.REPORT_REQUEST_STATUS_ID%TYPE;
    BEGIN
        lv_lock := GLOBALS_PKG.REQUEST_LOCK('REPORT_REQUEST' || CHR(0) || pn_profile_id || CHR(0) || pn_generator_id, pn_report_and_params_hash);
        SELECT MAX(REPORT_REQUEST_ID), MAX(STORED_FILE_ID), MAX(REPORT_REQUEST_STATUS_ID)
          INTO pn_request_id, pn_stored_file_id, ln_status_id
          FROM (SELECT REPORT_REQUEST_ID, STORED_FILE_ID, REPORT_REQUEST_STATUS_ID
                  FROM REPORT.REPORT_REQUEST
                 WHERE REPORT_AND_PARAMS_HASH = pn_report_and_params_hash
                   AND PROFILE_ID = pn_profile_id
                   AND DBMS_LOB.COMPARE(REPORT_PARAMS, pl_report_params) = 0
                   AND DBMS_LOB.COMPARE(REPORT_XML, pl_report_xml) = 0
                   AND REPORT_REQUEST_STATUS_ID IN(0, 1, 4)
                   AND GENERATOR_ID = pn_generator_id
                   AND COMPRESSED_FLAG = pc_compressed_flag
                   AND ((pv_base_url IS NULL AND BASE_URL IS NULL) OR pv_base_url = BASE_URL)
                   AND ((pv_time_zone_guid IS NULL AND TIME_ZONE_GUID IS NULL) OR pv_time_zone_guid = TIME_ZONE_GUID)
                   AND ((pv_locale_cd IS NULL AND LOCALE_CD IS NULL) OR pv_locale_cd = LOCALE_CD)
                   AND ((pv_run_report_action IS NULL AND RUN_REPORT_ACTION IS NULL) OR pv_run_report_action = RUN_REPORT_ACTION)               
                 ORDER BY GENERATED_TS DESC NULLS LAST, DECODE(REPORT_REQUEST_STATUS_ID, 1, 1, 0, 2, 4, 3, 4), REPORT_REQUEST_ID DESC)
         WHERE ROWNUM = 1 ;
        IF pn_request_id IS NULL THEN
            SELECT REPORT.SEQ_REPORT_REQUEST_ID.NEXTVAL, 'N'
              INTO pn_request_id, pn_reusing_request_flag
              FROM DUAL;
            INSERT INTO REPORT.REPORT_REQUEST(
                REPORT_REQUEST_ID, 
                PROFILE_ID, 
                GENERATOR_ID, 
                REPORT_AND_PARAMS_HASH, 
                BASE_URL, 
                TIME_ZONE_GUID,
                LOCALE_CD,
                RUN_REPORT_ACTION, 
                REPORT_XML, 
                REPORT_PARAMS,
                REPORT_REQUEST_TYPE_ID,
                REPORT_NAME,
                COMPRESSED_FLAG,
                REPORT_REQUEST_STATUS_ID)
              VALUES(
                pn_request_id, 
                pn_profile_id, 
                pn_generator_id, 
                pn_report_and_params_hash, 
                pv_base_url, 
                pv_time_zone_guid,
                pv_locale_cd, 
                pv_run_report_action,
                pl_report_xml, 
                pl_report_params,
                pn_report_request_type_id,
                pv_report_name,
                pc_compressed_flag,
                4 /*New*/);
        ELSIF pn_stored_file_id IS NOT NULL THEN
        	IF pn_stored_file_id != pn_refresh_stored_file_id THEN
	            SELECT MAX(STORED_FILE_NAME), MAX(STORED_FILE_KEY), MAX(STORED_FILE_CONTENTTYPE), MAX(STORED_FILE_PASSCODE), DBADMIN.TIMESTAMP_TO_MILLIS(MAX(STORED_FILE_EXPIRATION_TS)), 'Y'
	              INTO pv_stored_file_name, pv_stored_file_key, pv_stored_file_contenttype, pv_stored_file_passcode, pn_stored_file_expiration, pn_reusing_request_flag
	              FROM REPORT.STORED_FILE
	             WHERE STORED_FILE_ID = pn_stored_file_id
	               AND STORED_FILE_EXPIRATION_TS > CURRENT_TIMESTAMP;
        	END IF;
        	IF pn_stored_file_id = pn_refresh_stored_file_id OR pv_stored_file_key IS NULL THEN
	            UPDATE REPORT.REPORT_REQUEST
	               SET REPORT_REQUEST_STATUS_ID = 2
	             WHERE REPORT_REQUEST_ID = pn_request_id
	             RETURNING REPORT_NAME INTO lv_orig_report_name;
	            SELECT REPORT.SEQ_REPORT_REQUEST_ID.NEXTVAL, 'N'
	              INTO pn_request_id, pn_reusing_request_flag
	              FROM DUAL;
	            INSERT INTO REPORT.REPORT_REQUEST(
	                REPORT_REQUEST_ID, 
	                PROFILE_ID, 
	                GENERATOR_ID, 
	                REPORT_AND_PARAMS_HASH, 
	                BASE_URL, 
                    TIME_ZONE_GUID,
	                LOCALE_CD, 
	                RUN_REPORT_ACTION,
	                REPORT_XML, 
	                REPORT_PARAMS,
	                REPORT_REQUEST_TYPE_ID,
	                REPORT_NAME,
                    COMPRESSED_FLAG,
                    REPORT_REQUEST_STATUS_ID)
	              VALUES(
	                pn_request_id, 
	                pn_profile_id, 
	                pn_generator_id, 
	                pn_report_and_params_hash, 
	                pv_base_url, 
                    pv_time_zone_guid,
	                pv_locale_cd, 
	                pv_run_report_action,
	                pl_report_xml, 
	                pl_report_params,
	                pn_report_request_type_id,
	                NVL(pv_report_name, lv_orig_report_name),
                    pc_compressed_flag,
                    4 /*New*/);
	            pn_stored_file_id := NULL;
           	END IF;
        ELSIF ln_status_id = 4 THEN
            pn_reusing_request_flag := 'N'; /* So that it is requested through QueueLayer again */
        ELSE
            pn_reusing_request_flag := 'Y';
        END IF;
    END;
    
    -- USALive 1.6 Signature
    PROCEDURE GET_OR_CREATE_REPORT_REQUEST(
        pn_profile_id REPORT.REPORT_REQUEST.PROFILE_ID%TYPE,
        pn_generator_id REPORT.REPORT_REQUEST.GENERATOR_ID%TYPE,
        pn_report_and_params_hash REPORT.REPORT_REQUEST.REPORT_AND_PARAMS_HASH%TYPE,
        pl_report_xml REPORT.REPORT_REQUEST.REPORT_XML%TYPE,
        pl_report_params REPORT.REPORT_REQUEST.REPORT_PARAMS%TYPE,
        pv_base_url REPORT.REPORT_REQUEST.BASE_URL%TYPE,
        pv_locale_cd REPORT.REPORT_REQUEST.LOCALE_CD%TYPE,
        pv_run_report_action REPORT.REPORT_REQUEST.RUN_REPORT_ACTION%TYPE,
        pn_report_request_type_id REPORT.REPORT_REQUEST.REPORT_REQUEST_TYPE_ID%TYPE,
        pn_refresh_stored_file_id REPORT.REPORT_REQUEST.STORED_FILE_ID%TYPE,
        pv_report_name REPORT.REPORT_REQUEST.REPORT_NAME%TYPE,
        pn_request_id OUT REPORT.REPORT_REQUEST.REPORT_REQUEST_ID%TYPE,
        pn_stored_file_id OUT REPORT.REPORT_REQUEST.STORED_FILE_ID%TYPE,
        pv_stored_file_name OUT REPORT.STORED_FILE.STORED_FILE_NAME%TYPE,
        pv_stored_file_key OUT REPORT.STORED_FILE.STORED_FILE_KEY%TYPE,
        pv_stored_file_contenttype OUT REPORT.STORED_FILE.STORED_FILE_CONTENTTYPE%TYPE,
        pv_stored_file_passcode OUT REPORT.STORED_FILE.STORED_FILE_PASSCODE%TYPE,
        pn_reusing_request_flag OUT VARCHAR2)
    IS
        ln_stored_file_expiration NUMBER;
    BEGIN
        GET_OR_CREATE_REPORT_REQUEST(
            pn_profile_id,
            pn_generator_id,
            pn_report_and_params_hash,
            pl_report_xml,
            pl_report_params,
            pv_base_url,
            NULL,
            pv_locale_cd,
            pv_run_report_action,
            pn_report_request_type_id,
            pn_refresh_stored_file_id,
            pv_report_name,
            'N',
            pn_request_id,
            pn_stored_file_id,
            pv_stored_file_name,
            pv_stored_file_key,
            pv_stored_file_contenttype,
            pv_stored_file_passcode,
            ln_stored_file_expiration,
            pn_reusing_request_flag);
    END;
    
    -- RG 3.5 Signature
    PROCEDURE GET_OR_CREATE_REPORT_REQUEST(
        pn_profile_id REPORT.REPORT_REQUEST.PROFILE_ID%TYPE,
        pn_generator_id REPORT.REPORT_REQUEST.GENERATOR_ID%TYPE,
        pn_report_and_params_hash REPORT.REPORT_REQUEST.REPORT_AND_PARAMS_HASH%TYPE,
        pl_report_xml REPORT.REPORT_REQUEST.REPORT_XML%TYPE,
        pl_report_params REPORT.REPORT_REQUEST.REPORT_PARAMS%TYPE,
        pv_base_url REPORT.REPORT_REQUEST.BASE_URL%TYPE,
        pv_locale_cd REPORT.REPORT_REQUEST.LOCALE_CD%TYPE,
        pv_run_report_action REPORT.REPORT_REQUEST.RUN_REPORT_ACTION%TYPE,
        pn_report_request_type_id REPORT.REPORT_REQUEST.REPORT_REQUEST_TYPE_ID%TYPE,
        pn_refresh_stored_file_id REPORT.REPORT_REQUEST.STORED_FILE_ID%TYPE,
        pn_request_id OUT REPORT.REPORT_REQUEST.REPORT_REQUEST_ID%TYPE,
        pn_stored_file_id OUT REPORT.REPORT_REQUEST.STORED_FILE_ID%TYPE,
        pv_stored_file_name OUT REPORT.STORED_FILE.STORED_FILE_NAME%TYPE,
        pv_stored_file_key OUT REPORT.STORED_FILE.STORED_FILE_KEY%TYPE,
        pv_stored_file_contenttype OUT REPORT.STORED_FILE.STORED_FILE_CONTENTTYPE%TYPE,
        pv_stored_file_passcode OUT REPORT.STORED_FILE.STORED_FILE_PASSCODE%TYPE,
        pn_reusing_request_flag OUT VARCHAR2)
    IS
        ln_stored_file_expiration NUMBER;  
    BEGIN
        GET_OR_CREATE_REPORT_REQUEST(
            pn_profile_id,
            pn_generator_id,
            pn_report_and_params_hash,
            pl_report_xml,
            pl_report_params,
            pv_base_url,
            NULL,
            pv_locale_cd,
            pv_run_report_action,
            pn_report_request_type_id,
            pn_refresh_stored_file_id,
            NULL,
           'N',
            pn_request_id,
            pn_stored_file_id,
            pv_stored_file_name,
            pv_stored_file_key,
            pv_stored_file_contenttype,
            pv_stored_file_passcode,
            ln_stored_file_expiration,
            pn_reusing_request_flag);
    END;
    
    -- USALive 1.7 Signature
    PROCEDURE REFRESH_REPORT_REQUEST(
        pn_request_id REPORT.REPORT_REQUEST.REPORT_REQUEST_ID%TYPE,
        pn_refresh_stored_file_id REPORT.REPORT_REQUEST.STORED_FILE_ID%TYPE,
        pn_report_request_type_id REPORT.REPORT_REQUEST.REPORT_REQUEST_TYPE_ID%TYPE,
        pn_profile_id REPORT.REPORT_REQUEST.PROFILE_ID%TYPE,
        pn_generator_id OUT REPORT.REPORT_REQUEST.GENERATOR_ID%TYPE,
        pl_report_xml OUT REPORT.REPORT_REQUEST.REPORT_XML%TYPE,
        pl_report_params OUT REPORT.REPORT_REQUEST.REPORT_PARAMS%TYPE,
        pv_base_url OUT REPORT.REPORT_REQUEST.BASE_URL%TYPE,
        pv_time_zone_guid OUT REPORT.REPORT_REQUEST.TIME_ZONE_GUID%TYPE,
        pv_locale_cd OUT REPORT.REPORT_REQUEST.LOCALE_CD%TYPE,
        pv_run_report_action OUT REPORT.REPORT_REQUEST.RUN_REPORT_ACTION%TYPE,
        pc_compressed_flag OUT VARCHAR2, --REPORT.REPORT_REQUEST.COMPRESSED_FLAG%TYPE,
        pn_new_request_id OUT REPORT.REPORT_REQUEST.REPORT_REQUEST_ID%TYPE,
        pn_stored_file_id OUT REPORT.REPORT_REQUEST.STORED_FILE_ID%TYPE,
        pv_stored_file_name OUT REPORT.STORED_FILE.STORED_FILE_NAME%TYPE,
        pv_stored_file_key OUT REPORT.STORED_FILE.STORED_FILE_KEY%TYPE,
        pv_stored_file_contenttype OUT REPORT.STORED_FILE.STORED_FILE_CONTENTTYPE%TYPE,
        pv_stored_file_passcode OUT REPORT.STORED_FILE.STORED_FILE_PASSCODE%TYPE,
        pn_stored_file_expiration OUT NUMBER,
        pn_reusing_request_flag OUT VARCHAR2)
    IS
        ln_report_and_params_hash REPORT.REPORT_REQUEST.REPORT_AND_PARAMS_HASH%TYPE;
        ld_generated_ts REPORT.REPORT_REQUEST.GENERATED_TS%TYPE;
        lv_lock VARCHAR2(128);
        lv_orig_report_name REPORT.REPORT_REQUEST.REPORT_NAME%TYPE;
    BEGIN
        UPDATE REPORT.REPORT_REQUEST
           SET REPORT_REQUEST_STATUS_ID = 2
         WHERE REPORT_REQUEST_ID = pn_request_id
           AND (STORED_FILE_ID = pn_refresh_stored_file_id OR (STORED_FILE_ID IS NULL AND pn_refresh_stored_file_id IS NULL))
           AND PROFILE_ID = pn_profile_id
          RETURNING GENERATOR_ID, REPORT_AND_PARAMS_HASH, REPORT_XML, REPORT_PARAMS, BASE_URL, TIME_ZONE_GUID, LOCALE_CD, RUN_REPORT_ACTION, GENERATED_TS, REPORT_NAME, COMPRESSED_FLAG
          INTO pn_generator_id, ln_report_and_params_hash, pl_report_xml, pl_report_params, pv_base_url, pv_time_zone_guid, pv_locale_cd, pv_run_report_action, ld_generated_ts, lv_orig_report_name, pc_compressed_flag;
        lv_lock := GLOBALS_PKG.REQUEST_LOCK('REPORT_REQUEST' || CHR(0) || pn_profile_id || CHR(0) || pn_generator_id, ln_report_and_params_hash);        
        SELECT MAX(REPORT_REQUEST_ID), MAX(STORED_FILE_ID)
          INTO pn_new_request_id, pn_stored_file_id 
          FROM (SELECT REPORT_REQUEST_ID, STORED_FILE_ID
                  FROM REPORT.REPORT_REQUEST
                 WHERE REPORT_AND_PARAMS_HASH = ln_report_and_params_hash
                   AND PROFILE_ID = pn_profile_id
                   AND DBMS_LOB.COMPARE(REPORT_PARAMS, pl_report_params) = 0
                   AND DBMS_LOB.COMPARE(REPORT_XML, pl_report_xml) = 0
                   AND REPORT_REQUEST_STATUS_ID IN(0, 1)
                   AND GENERATOR_ID = pn_generator_id
                   AND COMPRESSED_FLAG = pc_compressed_flag
                   AND ((pv_base_url IS NULL AND BASE_URL IS NULL) OR pv_base_url = BASE_URL)
                   AND ((pv_locale_cd IS NULL AND LOCALE_CD IS NULL) OR pv_locale_cd = LOCALE_CD)
                   AND ((pv_run_report_action IS NULL AND RUN_REPORT_ACTION IS NULL) OR pv_run_report_action = RUN_REPORT_ACTION)               
                   AND (STORED_FILE_ID != pn_refresh_stored_file_id OR (STORED_FILE_ID IS NOT NULL AND pn_refresh_stored_file_id IS NULL))
                   AND NVL(GENERATED_TS, CREATED_TS) > NVL(ld_generated_ts, MIN_DATE)
                 ORDER BY GENERATED_TS DESC NULLS LAST, REPORT_REQUEST_ID DESC)
         WHERE ROWNUM = 1;
        IF pn_new_request_id IS NULL THEN
            SELECT REPORT.SEQ_REPORT_REQUEST_ID.NEXTVAL, 'N'
              INTO pn_new_request_id, pn_reusing_request_flag
              FROM DUAL;
            INSERT INTO REPORT.REPORT_REQUEST(
                REPORT_REQUEST_ID, 
                PROFILE_ID, 
                GENERATOR_ID, 
                REPORT_AND_PARAMS_HASH, 
                BASE_URL, 
                TIME_ZONE_GUID,
                LOCALE_CD,
                RUN_REPORT_ACTION, 
                REPORT_XML, 
                REPORT_PARAMS,
                REPORT_REQUEST_TYPE_ID,
                REPORT_NAME,
                COMPRESSED_FLAG)
              VALUES(
                pn_new_request_id, 
                pn_profile_id, 
                pn_generator_id, 
                ln_report_and_params_hash, 
                pv_base_url, 
                pv_time_zone_guid,
                pv_locale_cd, 
                pv_run_report_action,
                pl_report_xml, 
                pl_report_params,
                pn_report_request_type_id,
                lv_orig_report_name,
                pc_compressed_flag);
        ELSIF pn_stored_file_id IS NOT NULL THEN
            SELECT STORED_FILE_NAME, STORED_FILE_KEY, STORED_FILE_CONTENTTYPE, STORED_FILE_PASSCODE, DBADMIN.TIMESTAMP_TO_MILLIS(STORED_FILE_EXPIRATION_TS), 'Y'
              INTO pv_stored_file_name, pv_stored_file_key, pv_stored_file_contenttype, pv_stored_file_passcode, pn_stored_file_expiration, pn_reusing_request_flag
              FROM REPORT.STORED_FILE
             WHERE STORED_FILE_ID = pn_stored_file_id;
        ELSE
            pn_reusing_request_flag := 'Y';
        END IF;
    END;
    
     -- USALive 1.6 Signature
    PROCEDURE REFRESH_REPORT_REQUEST(
        pn_request_id REPORT.REPORT_REQUEST.REPORT_REQUEST_ID%TYPE,
        pn_refresh_stored_file_id REPORT.REPORT_REQUEST.STORED_FILE_ID%TYPE,
        pn_report_request_type_id REPORT.REPORT_REQUEST.REPORT_REQUEST_TYPE_ID%TYPE,
        pn_profile_id REPORT.REPORT_REQUEST.PROFILE_ID%TYPE,
        pn_generator_id OUT REPORT.REPORT_REQUEST.GENERATOR_ID%TYPE,
        pl_report_xml OUT REPORT.REPORT_REQUEST.REPORT_XML%TYPE,
        pl_report_params OUT REPORT.REPORT_REQUEST.REPORT_PARAMS%TYPE,
        pv_base_url OUT REPORT.REPORT_REQUEST.BASE_URL%TYPE,
        pv_locale_cd OUT REPORT.REPORT_REQUEST.LOCALE_CD%TYPE,
        pv_run_report_action OUT REPORT.REPORT_REQUEST.RUN_REPORT_ACTION%TYPE,
        pn_new_request_id OUT REPORT.REPORT_REQUEST.REPORT_REQUEST_ID%TYPE,
        pn_stored_file_id OUT REPORT.REPORT_REQUEST.STORED_FILE_ID%TYPE,
        pv_stored_file_name OUT REPORT.STORED_FILE.STORED_FILE_NAME%TYPE,
        pv_stored_file_key OUT REPORT.STORED_FILE.STORED_FILE_KEY%TYPE,
        pv_stored_file_contenttype OUT REPORT.STORED_FILE.STORED_FILE_CONTENTTYPE%TYPE,
        pv_stored_file_passcode OUT REPORT.STORED_FILE.STORED_FILE_PASSCODE%TYPE,
        pn_reusing_request_flag OUT VARCHAR2)
    IS
        ln_stored_file_expiration NUMBER; 
        ln_time_zone_guid REPORT.REPORT_REQUEST.TIME_ZONE_GUID%TYPE;
        lc_compressed_flag REPORT.REPORT_REQUEST.COMPRESSED_FLAG%TYPE;
    BEGIN
        REFRESH_REPORT_REQUEST(
            pn_request_id,
            pn_refresh_stored_file_id,
            pn_report_request_type_id,
            pn_profile_id,
            pn_generator_id,
            pl_report_xml,
            pl_report_params,
            pv_base_url,
            ln_time_zone_guid,
            pv_locale_cd,
            pv_run_report_action,
            lc_compressed_flag,
            pn_new_request_id,
            pn_stored_file_id,
            pv_stored_file_name,
            pv_stored_file_key,
            pv_stored_file_contenttype,
            pv_stored_file_passcode,
            ln_stored_file_expiration,
            pn_reusing_request_flag);
    END;
    
    PROCEDURE ADD_USER_REQUEST(
      pn_user_id REPORT.REPORT_REQUEST_USER.USER_ID%TYPE,
      pn_request_id REPORT.REPORT_REQUEST.REPORT_REQUEST_ID%TYPE)
    IS
    BEGIN
        MERGE INTO REPORT.REPORT_REQUEST_USER TGT USING (
            SELECT pn_user_id USER_ID, pn_request_id REPORT_REQUEST_ID
              FROM DUAL) src
                ON (SRC.USER_ID = TGT.USER_ID AND SRC.REPORT_REQUEST_ID = TGT.REPORT_REQUEST_ID AND TGT.REPORT_REQUEST_USER_STATUS_ID != -1)
            WHEN NOT MATCHED THEN
            INSERT(REPORT_REQUEST_USER_ID, USER_ID, REPORT_REQUEST_ID)
               VALUES(REPORT.SEQ_REPORT_REQUEST_USER_ID.NEXTVAL, pn_user_id, pn_request_id);
    END;
    
    PROCEDURE CANCEL_REPORT_REQUEST(
        pn_user_id REPORT.REPORT_REQUEST_USER.USER_ID%TYPE,
        pn_request_id REPORT.REPORT_REQUEST_USER.REPORT_REQUEST_ID%TYPE,
        pn_cancel_needed OUT PLS_INTEGER)
    IS
        ln_pending PLS_INTEGER;
        ln_profile_id REPORT.REPORT_REQUEST.PROFILE_ID%TYPE;
        ln_generator_id REPORT.REPORT_REQUEST.GENERATOR_ID%TYPE;
        ln_report_and_params_hash REPORT.REPORT_REQUEST.REPORT_AND_PARAMS_HASH%TYPE;
        lv_lock VARCHAR2(128);
    BEGIN
        pn_cancel_needed := -1;
        UPDATE REPORT.REPORT_REQUEST_USER 
           SET REPORT_REQUEST_USER_STATUS_ID = -1
         WHERE USER_ID = pn_user_id
           AND REPORT_REQUEST_ID = pn_request_id
           AND REPORT_REQUEST_USER_STATUS_ID = 0
           AND 1 != (SELECT REPORT_REQUEST_STATUS_ID FROM REPORT.REPORT_REQUEST WHERE REPORT_REQUEST_ID = pn_request_id);
        IF SQL%ROWCOUNT > 0 THEN
            SELECT PROFILE_ID, GENERATOR_ID, REPORT_AND_PARAMS_HASH
              INTO ln_profile_id, ln_generator_id, ln_report_and_params_hash
              FROM REPORT.REPORT_REQUEST
             WHERE REPORT_REQUEST_ID = pn_request_id;
            lv_lock := GLOBALS_PKG.REQUEST_LOCK('REPORT_REQUEST' || CHR(0) || ln_profile_id || CHR(0) || ln_generator_id, ln_report_and_params_hash);
            UPDATE REPORT.REPORT_REQUEST 
               SET REPORT_REQUEST_STATUS_ID = -1
             WHERE REPORT_REQUEST_ID = pn_request_id
               AND NOT EXISTS(
                    SELECT 1 
                      FROM REPORT.REPORT_REQUEST_USER 
                     WHERE REPORT_REQUEST_ID = pn_request_id 
                       AND REPORT_REQUEST_USER_STATUS_ID = 0)
               AND REPORT_REQUEST_STATUS_ID = 0; 
            pn_cancel_needed := SQL%ROWCOUNT;
        END IF;
    END;
    
    PROCEDURE SAVE_GENERATED_REPORT(
      pv_file_name REPORT.STORED_FILE.STORED_FILE_NAME%TYPE,
      pv_file_key REPORT.STORED_FILE.STORED_FILE_KEY%TYPE,
      pn_file_length REPORT.STORED_FILE.STORED_FILE_LENGTH%TYPE,
      pv_file_contenttype REPORT.STORED_FILE.STORED_FILE_CONTENTTYPE%TYPE,
      pv_description REPORT.STORED_FILE.STORED_FILE_DESCRIPTION%TYPE,
      pd_expiration_ts REPORT.STORED_FILE.STORED_FILE_EXPIRATION_TS%TYPE,
      pv_file_passcode REPORT.STORED_FILE.STORED_FILE_PASSCODE%TYPE,
      pd_generate_ts REPORT.REPORT_REQUEST.GENERATED_TS%TYPE,
      pn_report_sent_id REPORT.REPORT_SENT.REPORT_SENT_ID%TYPE,
      pn_request_id REPORT.REPORT_REQUEST.REPORT_REQUEST_ID%TYPE,
      pn_file_id OUT REPORT.STORED_FILE.STORED_FILE_ID%TYPE)
    IS     
    BEGIN
        SELECT REPORT.SEQ_STORED_FILE_ID.NEXTVAL 
          INTO pn_file_id 
          FROM DUAL;
        INSERT INTO REPORT.STORED_FILE(
            STORED_FILE_ID, 
            STORED_FILE_NAME, 
            STORED_FILE_KEY, 
            STORED_FILE_LENGTH, 
            STORED_FILE_CONTENTTYPE, 
            STORED_FILE_DESCRIPTION,
            STORED_FILE_PASSCODE,
            STORED_FILE_EXPIRATION_TS)
         VALUES(
            pn_file_id,
            pv_file_name,
            pv_file_key,
            pn_file_length,
            pv_file_contenttype,
            pv_description,
            pv_file_passcode, 
            pd_expiration_ts);
        IF pn_report_sent_id IS NOT NULL THEN
            UPDATE REPORT.REPORT_SENT 
               SET REPORT_SENT_STATE_ID = 2, 
                   DETAILS = 'GENERATED'
             WHERE REPORT_SENT_ID = pn_report_sent_id
               AND REPORT_REQUEST_ID = pn_request_id;
        END IF;     
        IF pn_request_id IS NOT NULL THEN
            UPDATE REPORT.REPORT_REQUEST 
               SET STORED_FILE_ID = pn_file_id, 
                   REPORT_REQUEST_STATUS_ID = 1,
                   GENERATED_TS = pd_generate_ts
             WHERE REPORT_REQUEST_ID = pn_request_id;
        END IF;          
    END;
    
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/DW_PKG.pbk?rev=1.26
CREATE OR REPLACE PACKAGE BODY REPORT.DW_PKG IS
    FUNCTION GET_OR_CREATE_EXPORT_BATCH(
        l_customer_id CORP.CUSTOMER.CUSTOMER_ID%TYPE)
     RETURN ACTIVITY_REF.BATCH_ID%TYPE
    IS
    PRAGMA AUTONOMOUS_TRANSACTION;
        l_export_id ACTIVITY_REF.BATCH_ID%TYPE;
        l_lock VARCHAR2(128);
    BEGIN
        l_lock := GLOBALS_PKG.REQUEST_LOCK('EXPORT.CUSTOMER_ID',l_customer_id);
        SELECT eb.batch_id
          INTO l_export_id
          FROM REPORT.EXPORT_BATCH eb
         WHERE eb.CUSTOMER_ID = l_customer_id
           AND eb.EXPORT_TYPE = 1
           AND eb.STATUS = 'O';
        COMMIT;
        RETURN l_export_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            SELECT REPORT.EXPORT_BATCH_SEQ.NEXTVAL
              INTO l_export_id
              FROM DUAL;
            
            INSERT INTO REPORT.EXPORT_BATCH(
                   BATCH_ID,
                   CUSTOMER_ID,
                   EXPORT_TYPE,
                   STATUS)
               SELECT
                   l_export_id,
                   l_customer_id,
                   1,
                   'O'
                 FROM DUAL
                WHERE NOT EXISTS(
                    SELECT 1
                      FROM REPORT.EXPORT_BATCH eb
                     WHERE eb.CUSTOMER_ID = l_customer_id
                       AND eb.EXPORT_TYPE = 1
                       AND eb.STATUS = 'O');
            COMMIT;
            IF SQL%FOUND THEN
                RETURN l_export_id;
            ELSE
                RETURN GET_OR_CREATE_EXPORT_BATCH(l_customer_id);
            END IF;
        WHEN OTHERS THEN
            RAISE;
    END;
    
    PROCEDURE CLOSE_EXPORT_BATCH(
        l_export_id REPORT.EXPORT_BATCH.BATCH_ID%TYPE,
        l_customer_id REPORT.EXPORT_BATCH.CUSTOMER_ID%TYPE)
    IS
        l_lock VARCHAR2(128);
    BEGIN
        l_lock := GLOBALS_PKG.REQUEST_LOCK('EXPORT.CUSTOMER_ID',l_customer_id);
        UPDATE REPORT.EXPORT_BATCH EB SET (
                STATUS,
                TRAN_CREATE_DT_BEG,
                TRAN_CREATE_DT_END,
                TOT_TRAN_ROWS,
                TOT_TRAN_AMOUNT
              ) = (SELECT
                'A',
                MIN(A.TRAN_DATE),
                MAX(A.TRAN_DATE),
                COUNT(*),
                SUM(A.TOTAL_AMOUNT)
              FROM REPORT.ACTIVITY_REF A
             WHERE A.BATCH_ID = eb.BATCH_ID)
         WHERE EB.BATCH_ID = l_export_id
           AND EB.STATUS = 'O';
         CORP.PAYMENTS_PKG.UPDATE_EXPORT_BATCH(l_export_id);
    END;
    
    PROCEDURE CLOSE_EXPORT_BATCHES
    IS
        CURSOR l_cur IS
            SELECT BATCH_ID, CUSTOMER_ID
              FROM REPORT.EXPORT_BATCH
            WHERE STATUS = 'O'
              AND CRD_DATE < TRUNC(SYSDATE - (5/24)) + (5/24);
    BEGIN
        FOR l_rec IN l_cur LOOP
            CLOSE_EXPORT_BATCH(l_rec.BATCH_ID, l_rec.CUSTOMER_ID);
            COMMIT;
        END LOOP;
    END;
    
    PROCEDURE CLOSE_EXPORT_BATCH_FOR(
        l_customer_id CORP.CUSTOMER.CUSTOMER_ID%TYPE)
    IS
        CURSOR l_cur IS
            SELECT BATCH_ID
              FROM REPORT.EXPORT_BATCH
            WHERE STATUS = 'O'
              AND CUSTOMER_ID = l_customer_id;
    BEGIN
        FOR l_rec IN l_cur LOOP
            CLOSE_EXPORT_BATCH(l_rec.BATCH_ID, l_customer_id);
            COMMIT;
        END LOOP;
    END;
    
    PROCEDURE UPDATE_SETTLEMENT(
        l_trans_id TRANS.TRAN_ID%TYPE,
        l_settle_state_id TRANS.SETTLE_STATE_ID%TYPE,
        l_settle_date TRANS.SETTLE_DATE%TYPE,
        l_appr_cd TRANS.CC_APPR_CODE%TYPE)
    IS
    BEGIN
        UPDATE ACTIVITY_REF SET CC_APPR_CODE = l_appr_cd,
            SETTLE_STATE = (SELECT STATE_LABEL FROM TRANS_STATE S
                WHERE l_settle_state_id = S.STATE_ID)
         WHERE TRAN_ID = l_trans_id;
    END;

PROCEDURE UPDATE_TRAN_INFO(
        l_trans_id TRANS.TRAN_ID%TYPE)
    IS
        l_ref_nbr ACTIVITY_REF.REF_NBR%TYPE;
        l_export_id ACTIVITY_REF.BATCH_ID%TYPE;
        l_create_dt ACTIVITY_REF.CREATE_DT%TYPE;
        l_update_dt ACTIVITY_REF.UPDATE_DT%TYPE;
        l_qty ACTIVITY_REF.QUANTITY%TYPE;
        l_terminal_id ACTIVITY_REF.TERMINAL_ID%TYPE;
        l_eport_id ACTIVITY_REF.EPORT_ID%TYPE;
        l_tran_date ACTIVITY_REF.TRAN_DATE%TYPE;
        l_trans_type_id ACTIVITY_REF.TRANS_TYPE_ID%TYPE;
        l_total_amount ACTIVITY_REF.TOTAL_AMOUNT%TYPE;
        l_currency_id ACTIVITY_REF.CURRENCY_ID%TYPE;
        l_customer_id CORP.CUSTOMER.CUSTOMER_ID%TYPE;
        l_convenience_fee ACTIVITY_REF.CONVENIENCE_FEE%TYPE;
        ln_cnt PLS_INTEGER;
        CURSOR l_cur IS
         SELECT
            R.REGION_ID,
            R.REGION_NAME,
            L.LOCATION_NAME,
            L.LOCATION_ID,
            T.TERMINAL_ID,
            TERM.TERMINAL_NAME,
            TERM.TERMINAL_NBR,
            T.TRANS_TYPE_ID,
            TT.TRANS_TYPE_NAME,
            S.STATE_LABEL,
            T.CARD_NUMBER,
            T.CC_APPR_CODE,
            T.CLOSE_DATE,
            T.TOTAL_AMOUNT,
            T.ORIG_TRAN_ID,
            T.EPORT_ID,
            T.DESCRIPTION,
            T.CURRENCY_ID,
			T.CARDTYPE_AUTHORITY_ID,
            TERM.CUSTOMER_ID,
            P.AMOUNT*NVL(P.PRICE,0) CONVENIENCE_FEE
          FROM TRANS_TYPE TT, TRANS T, TERMINAL TERM,
            REGION R, TERMINAL_REGION TR, LOCATION L,
            TRANS_STATE S, REPORT.PURCHASE P
          WHERE TR.REGION_ID = R.REGION_ID (+)
            AND TERM.TERMINAL_ID = TR.TERMINAL_ID (+)
            AND TERM.LOCATION_ID = L.LOCATION_ID (+)
            AND T.TERMINAL_ID = TERM.TERMINAL_ID (+)
            AND T.TRANS_TYPE_ID = TT.TRANS_TYPE_ID
            AND T.TRAN_ID = l_trans_id
            AND T.SETTLE_STATE_ID = S.STATE_ID
			AND T.TRAN_ID = P.TRAN_ID (+) AND P.VEND_COLUMN (+) ='Convenience Fee';
    BEGIN
        DELETE FROM REPORT.ACTIVITY_REF
         WHERE TRAN_ID = L_TRANS_ID
         RETURNING REF_NBR, BATCH_ID, CREATE_DT, SYSDATE, TERMINAL_ID, EPORT_ID, TRAN_DATE, TRANS_TYPE_ID, TOTAL_AMOUNT, QUANTITY, CURRENCY_ID, CONVENIENCE_FEE
              INTO l_ref_nbr, l_export_id, l_create_dt, l_update_dt, l_terminal_id, l_eport_id, l_tran_date, l_trans_type_id, l_total_amount, l_qty, l_currency_id, l_convenience_fee;
        -- if exists, then update the trans_stat_by_day
        IF NVL(l_qty, 0) != 0 AND l_create_dt is NOT NULL THEN
          UPDATE REPORT.TRANS_STAT_BY_DAY 
             SET TRAN_COUNT = NVL(tran_count, 0) - 1,
                 VEND_COUNT = NVL(VEND_COUNT, 0) - l_qty,
                 TRAN_AMOUNT = NVL(TRAN_AMOUNT, 0) - NVL(l_total_amount, 0),
                 CONVENIENCE_FEE = NVL(CONVENIENCE_FEE, 0) - NVL(l_convenience_fee, 0)
           WHERE TERMINAL_ID = l_terminal_id
             AND EPORT_ID = l_eport_id
             AND TRAN_DATE = trunc(l_tran_date, 'DD')
             AND TRANS_TYPE_ID = l_trans_type_id
             AND CURRENCY_ID = l_currency_id;
        END IF;
        COMMIT;
        FOR l_rec IN l_cur LOOP
            IF NVL(l_export_id, 0) = 0 THEN
                IF l_rec.customer_id IS NULL THEN
                    l_export_id := 0;
                ELSE
                    l_export_id := GET_OR_CREATE_EXPORT_BATCH(l_rec.customer_id);
                END IF;
            END IF;
            IF l_ref_nbr IS NULL THEN
                SELECT REF_NBR_SEQ.NEXTVAL
                  INTO l_ref_nbr 
                  FROM DUAL;
            END IF;
            SELECT SUM(AMOUNT)
              INTO l_qty
              FROM REPORT.PURCHASE
             WHERE TRAN_ID = l_trans_id
               AND (MDB_NUMBER IS NOT NULL OR VEND_COLUMN != 'Convenience Fee');

            INSERT INTO ACTIVITY_REF(
                REGION_ID,
                REGION_NAME,
                LOCATION_NAME,
                LOCATION_ID,
                TERMINAL_ID,
                TERMINAL_NAME,
                TERMINAL_NBR,
                TRAN_ID,
                TRANS_TYPE_ID,
                TRANS_TYPE_NAME,
                SETTLE_STATE,
                CARD_NUMBER,
                CARD_COMPANY,
                CC_APPR_CODE,
                TRAN_DATE,
                FILL_DATE,
                TOTAL_AMOUNT,
                ORIG_TRAN_ID,
                BATCH_ID,
                VEND_COLUMN,
                QUANTITY,
                REF_NBR,
                EPORT_ID,
                DESCRIPTION,
                CREATE_DT,
                UPDATE_DT,
                CURRENCY_ID,
				CONVENIENCE_FEE)
            SELECT
                l_rec.REGION_ID,
                l_rec.REGION_NAME,
                NVL(l_rec.LOCATION_NAME, 'Unknown'),
                NVL(l_rec.LOCATION_ID, 0),
                l_rec.TERMINAL_ID,
                l_rec.TERMINAL_NAME,
                l_rec.TERMINAL_NBR,
                l_trans_id,
                l_rec.TRANS_TYPE_ID,
                l_rec.TRANS_TYPE_NAME,
                l_rec.STATE_LABEL,
                l_rec.CARD_NUMBER,
                REPORT.CARD_NAME(l_rec.CARDTYPE_AUTHORITY_ID, l_rec.TRANS_TYPE_ID, l_rec.CARD_NUMBER),
                l_rec.CC_APPR_CODE,
                l_rec.CLOSE_DATE TRAN_DATE,
                GET_FILL_DATE(l_trans_id) FILL_DATE,
                l_rec.TOTAL_AMOUNT,
                l_rec.ORIG_TRAN_ID,
                l_export_id,
                VEND_COLUMN_STRING(l_trans_id),
                l_qty,
                l_ref_nbr,
                l_rec.EPORT_ID,
                l_rec.DESCRIPTION,
                NVL(l_create_dt, SYSDATE),
                l_update_dt,
                l_rec.currency_id,
				l_rec.convenience_fee
              FROM DUAL;
            -- update trans_stat_by_day
            IF NVL(l_qty, 0) != 0 THEN
                LOOP
                    UPDATE REPORT.TRANS_STAT_BY_DAY
                       SET TRAN_COUNT = NVL(TRAN_COUNT, 0) + 1,
                           VEND_COUNT = NVL(VEND_COUNT, 0) + l_qty,
                           TRAN_AMOUNT = NVL(TRAN_AMOUNT, 0) + NVL(l_rec.TOTAL_AMOUNT, 0),
                           CONVENIENCE_FEE = NVL(CONVENIENCE_FEE, 0) + NVL(l_rec.CONVENIENCE_FEE, 0)
                     WHERE TERMINAL_ID = l_rec.TERMINAL_ID
                       AND EPORT_ID = l_rec.EPORT_ID
                       AND TRAN_DATE = trunc(l_rec.CLOSE_DATE, 'DD')
                       AND TRANS_TYPE_ID = l_rec.TRANS_TYPE_ID
                       AND CURRENCY_ID = l_rec.CURRENCY_ID;
                    EXIT WHEN SQL%ROWCOUNT > 0;
                    BEGIN
                        INSERT INTO REPORT.TRANS_STAT_BY_DAY(
                            TERMINAL_ID,
                            EPORT_ID,
                            TRAN_DATE, 
                            TRANS_TYPE_ID,
                            TRAN_COUNT,
                            VEND_COUNT, 
                            TRAN_AMOUNT,
                            CURRENCY_ID, 
                            CONVENIENCE_FEE) 
                        VALUES( 
                            l_rec.TERMINAL_ID,
                            l_rec.EPORT_ID,
                            trunc(l_rec.CLOSE_DATE, 'DD'),
                            l_rec.TRANS_TYPE_ID, 
                            1,
                            l_qty,
                            NVL(l_rec.TOTAL_AMOUNT, 0),
                            l_rec.CURRENCY_ID,
                            l_rec.CONVENIENCE_FEE); 
                        EXIT;    
                    EXCEPTION
                        WHEN DUP_VAL_ON_INDEX THEN
                            NULL;
                    END;
                END LOOP;  
            END IF;
            COMMIT;
            IF NVL(l_qty, 0) != 0 THEN
                --logic to update report.eport column for condition report
                IF l_rec.TRANS_TYPE_ID IN (16, 19) THEN
                  -- credit, debit
                  UPDATE REPORT.EPORT SET
                   FIRST_TRAN_DATE=LEAST(NVL(FIRST_TRAN_DATE, max_date), l_rec.CLOSE_DATE),
                   FIRST_CREDIT_TRAN_DATE=LEAST(NVL(FIRST_CREDIT_TRAN_DATE, max_date), l_rec.CLOSE_DATE),
                   LAST_TRAN_DATE=GREATEST(NVL(LAST_TRAN_DATE, min_date), l_rec.CLOSE_DATE),
                   LAST_CREDIT_TRAN_DATE=GREATEST(NVL(LAST_CREDIT_TRAN_DATE, min_date), l_rec.CLOSE_DATE)
                  WHERE EPORT_ID=l_rec.EPORT_ID;
                ELSIF l_rec.TRANS_TYPE_ID = 22 THEN
                  -- cash
                  UPDATE REPORT.EPORT SET
                   FIRST_TRAN_DATE=LEAST(NVL(FIRST_TRAN_DATE, max_date), l_rec.CLOSE_DATE),
                   FIRST_CASH_TRAN_DATE=LEAST(NVL(FIRST_CASH_TRAN_DATE, max_date), l_rec.CLOSE_DATE),
                   LAST_TRAN_DATE=GREATEST(NVL(LAST_TRAN_DATE, min_date), l_rec.CLOSE_DATE),
                   LAST_CASH_TRAN_DATE=GREATEST(NVL(LAST_CASH_TRAN_DATE, min_date), l_rec.CLOSE_DATE)
                  WHERE EPORT_ID=l_rec.EPORT_ID;
                ELSE
                  UPDATE REPORT.EPORT SET
                   FIRST_TRAN_DATE=LEAST(NVL(FIRST_TRAN_DATE, max_date), l_rec.CLOSE_DATE),
                   LAST_TRAN_DATE=GREATEST(NVL(LAST_TRAN_DATE, min_date), l_rec.CLOSE_DATE)
                  WHERE EPORT_ID=l_rec.EPORT_ID;
                END IF;                 
            END IF;
        END LOOP;
    END;
    
    PROCEDURE UPDATE_TERMINAL(
        l_terminal_id TERMINAL.TERMINAL_ID%TYPE)
    IS
    BEGIN
        UPDATE ACTIVITY_REF A SET (
            region_id,
            region_name,
            location_name,
            location_id,
            terminal_name,
            terminal_nbr) = (
        SELECT
            R.REGION_ID,
            R.REGION_NAME,
            NVL(L.LOCATION_NAME, 'Unknown'),
            NVL(L.LOCATION_ID, 0),
            TERMINAL_NAME,
            TERM.TERMINAL_NBR
          FROM TERMINAL TERM,  REGION R, TERMINAL_REGION TR, LOCATION L
          WHERE TR.REGION_ID = R.REGION_ID (+)
            AND TERM.TERMINAL_ID = TR.TERMINAL_ID (+)
            AND TERM.LOCATION_ID = L.LOCATION_ID (+)
            AND TERM.TERMINAL_ID = A.TERMINAL_ID)
        WHERE A.TERMINAL_ID = l_terminal_id
          AND A.TRAN_DATE > SYSDATE - 90;
    END;

    PROCEDURE UPDATE_FILL_DATE(
        l_fill_id FILL.FILL_ID%TYPE)
    IS
        l_fill_date FILL.FILL_DATE%TYPE;
        l_prev_fill_date FILL.FILL_DATE%TYPE;
        l_eport_id FILL.EPORT_ID%TYPE;
    BEGIN
        SELECT FILL_DATE, EPORT_ID
          INTO l_fill_date, l_eport_id
          FROM FILL
         WHERE FILL_ID = l_fill_id;
         
         -- add update last fill date for condition report
         UPDATE REPORT.EPORT
         SET LAST_FILL_DATE=l_fill_date
         WHERE EPORT_ID=l_eport_id
         AND NVL(LAST_FILL_DATE, min_date) < l_fill_date;

          SELECT MAX(FILL_DATE)
          INTO l_prev_fill_date
          FROM FILL
         WHERE FILL_DATE < l_fill_date
           AND EPORT_ID = l_eport_id;
           
        -- Added to capture first fills (03-05-2007 BSK)
        IF l_prev_fill_date IS NULL THEN
            SELECT MAX(START_DATE)
              INTO l_prev_fill_date
              FROM REPORT.TERMINAL_EPORT
             WHERE START_DATE <= l_fill_date
               AND EPORT_ID = l_eport_id;
        END IF;
        
        -- Ignore first fill period (added June 8th for performance reasons - BSK)
        IF l_prev_fill_date IS NOT NULL THEN
            UPDATE ACTIVITY_REF SET FILL_DATE = GET_FILL_DATE(TRAN_ID)
             WHERE EPORT_ID = l_eport_id AND TRAN_DATE BETWEEN l_prev_fill_date AND l_fill_date;
          END IF;
    END;
    
    PROCEDURE UPDATE_FILL_INFO(
            l_fill_id   FILL.FILL_ID%TYPE) 
    IS
        l_export_batch_id FILL.BATCH_ID%TYPE;
        l_customer_id TERMINAL.CUSTOMER_ID%TYPE;
        l_terminal_id FILL.TERMINAL_ID%TYPE;
    
    BEGIN
        SELECT T.CUSTOMER_ID, T.TERMINAL_ID INTO l_customer_id, l_terminal_id
        FROM REPORT.TERMINAL T 
        JOIN REPORT.TERMINAL_EPORT TE ON T.TERMINAL_ID=TE.TERMINAL_ID
        JOIN REPORT.FILL F ON F.EPORT_ID=TE.EPORT_ID
        WHERE F.FILL_ID=l_fill_id
        AND F.FILL_DATE >= NVL(TE.START_DATE, MIN_DATE) AND F.FILL_DATE < NVL(TE.END_DATE, MAX_DATE);
        
        l_export_batch_id := GET_OR_CREATE_EXPORT_BATCH(l_customer_id);
        UPDATE REPORT.FILL SET BATCH_ID = l_export_batch_id, TERMINAL_ID = l_terminal_id,
        REF_NBR = NVL(REF_NBR, REF_NBR_SEQ.nextval)
        WHERE FILL_ID = l_fill_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN;  
    END;
    
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/releases/REL_USALive_1_7_0/USALive_1_7_5_populate_activity_ref_con_fee.sql?rev=HEAD
DECLARE
  l_min_tran_id NUMBER;
  l_max_tran_id NUMBER;
BEGIN
	select min(tran_id), max(tran_id) into l_min_tran_id, l_max_tran_id from report.purchase where vend_column='Convenience Fee';
    while l_min_tran_id <= l_max_tran_id loop
    	update report.activity_ref af set convenience_fee=(
			select sum(NVL(p.AMOUNT * NVL(p.PRICE, 0), 0))
			from report.purchase p where p.vend_column='Convenience Fee' and af.tran_id = p.tran_id)
		where exists (
    		select p.tran_id
			from report.purchase p where p.vend_column='Convenience Fee' and af.tran_id = p.tran_id)
    	and tran_id between l_min_tran_id and l_min_tran_id+1000-1;
		commit;
		l_min_tran_id:=l_min_tran_id+1000;
	end loop;
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/releases/REL_USALive_1_7_0/USALive_1_7_5_populate_tran_stat_con_fee.sql?rev=HEAD
DECLARE
  ld_min_date DATE := TO_DATE('11/14/2011 00:00:00', 'MM/DD/YYYY HH24:MI:SS');
  ld_max_date DATE := sysdate+1;
BEGIN
    WHILE ld_min_date < ld_max_date LOOP
      update report.trans_stat_by_day t set convenience_fee=
      (select sum(a.convenience_fee) convenience_fee 
      from report.activity_ref a  
      where a.convenience_fee >0 and t.terminal_id=a.terminal_id and t.eport_id=a.eport_id and t.tran_date=trunc(a.tran_date, 'DD') and t.trans_type_id=a.trans_type_id
      and a.tran_date >= ld_min_date and a.tran_date < ld_min_date + 1)
	  where t.tran_date >= ld_min_date and t.tran_date < ld_min_date + 1;
      commit;
      ld_min_date:=ld_min_date+1;
    END LOOP;
END;

/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/FOLIO_CONF/folio_exports/folio_payment_batches_field_update.sql?rev=HEAD
-- convenience fee under Payments field category
INSERT INTO FOLIO_CONF.FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
 VALUES(2706, 'Convenience Fee','','REPORT.ACTIVITY_REF[CONVENIENCE_FEE].CONVENIENCE_FEE','REPORT.ACTIVITY_REF[CONVENIENCE_FEE].CONVENIENCE_FEE','CURRENCY','DECIMAL','DECIMAL',50,63,'Y');                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          

INSERT INTO FOLIO_CONF.FIELD_PRIV (USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID) VALUES(1, 2706, NULL);
INSERT INTO FOLIO_CONF.FIELD_PRIV (USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID) VALUES(2, 2706, NULL);
INSERT INTO FOLIO_CONF.FIELD_PRIV (USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID) VALUES(3, 2706, NULL);
INSERT INTO FOLIO_CONF.FIELD_PRIV (USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID) VALUES(4, 2706, 1);
INSERT INTO FOLIO_CONF.FIELD_PRIV (USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID) VALUES(5, 2706, 1);
INSERT INTO FOLIO_CONF.FIELD_PRIV (USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID) VALUES(8, 2706, 1);

-- new 
insert into folio_conf.join_filter (join_filter_id, from_table,to_table, join_expression)
values(-16, 'CORP.LEDGER', 'REPORT.ACTIVITY_REF[CONVENIENCE_FEE]', 'DECODE(CORP.LEDGER.ENTRY_TYPE, ''PF'', CORP.LEDGER.TRANS_ID) = REPORT.ACTIVITY_REF[CONVENIENCE_FEE].TRAN_ID (+)');

-- delete from FOLIO_CONF.FIELD_PRIV where FIELD_ID=2706;
-- delete from FOLIO_CONF.FIELD where FIELD_ID=2706;
-- delete from folio_conf.join_filter where join_filter_id=-16;
commit;

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/FOLIO_CONF/folio_exports/folio_payment_batches.sql?rev=HEAD
SET DEFINE OFF;
ALTER SESSION SET CURRENT_SCHEMA = FOLIO_CONF;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
DECLARE                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
  l_ids NUMBER_TABLE := NUMBER_TABLE(0, 0);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
BEGIN                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
FOLIO_CONF.FOLIO_PKG.DELETE_FOLIO(260);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
SELECT 260 INTO l_ids(1) FROM DUAL;
INSERT INTO FOLIO(FOLIO_ID, FOLIO_NAME, FOLIO_TITLE, FOLIO_SUBTITLE, DEFAULT_OUTPUT_TYPE_ID, OWNER_USER_ID, DEFAULT_CHART_TYPE_ID, MAX_ROWS_PER_SECTION, MAX_ROWS) (SELECT l_ids(1), 'Payment Batches', '', '', 22, 7, NULL, -1, -1 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 1, 1, 'LITERAL:Device', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 606, 'ASC', 0, 0 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 2, 1, 'LITERAL:Terminal', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 20, 'ASC', 0, 0 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 3, 1, 'LITERAL:Location', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 16, 'ASC', 0, 0 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 4, 1, 'LITERAL:Asset Nbr', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 761, 'ASC', 0, 0 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 5, 1, 'LITERAL:# of Trans', '', '', 'MESSAGE:{0,NUMBER}', 'MESSAGE:run_report_async.i?folioId=286&params.PaymentBatchId={1}&params.DocId={2}', 'LITERAL:Click to view transaction details', 'MESSAGE:{0}', '', 0, '', '', 'NUMBER' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                            
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 604, 'ASC', 6, 0 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 2, 724, 'ASC', 10, 0 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 3, 555, 'ASC', 0, 0 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 6, 1, 'LITERAL:Gross Credit', '', '', 'CURRENCY', '', '', '', '', 0, '', '', 'NUMBER' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 105, 'ASC', 1, 0 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 7, 1, 'LITERAL:Failed Credit', '', '', 'CURRENCY', '', '', '', '', 0, '', '', 'NUMBER' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 592, 'ASC', 1, 0 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 8, 1, 'LITERAL:Net Credit', '', '', 'CURRENCY', '', '', '', '', 0, '', '', 'NUMBER' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 594, 'ASC', 1, 0 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 9, 1, 'LITERAL:Process Fees', '', '', 'CURRENCY', 'LITERAL:javascript:scrollTo(''folio_282'');', '', '', '', 0, '', '', 'NUMBER' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 108, 'ASC', 1, 0 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 10, 1, 'LITERAL:Service Fees', '', '', 'CURRENCY', 'LITERAL:javascript:scrollTo(''folio_288'');', '', '', '', 0, '', '', 'NUMBER' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 109, 'ASC', 1, 0 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 11, 1, 'LITERAL:Refunds/ Chgbcks', '', '', 'CURRENCY', 'LITERAL:javascript:scrollTo(''folio_284'');', '', '', '', 0, '', '', 'NUMBER' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 596, 'ASC', 1, 0 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 12, 1, 'LITERAL:Adjustments', '', '', 'CURRENCY', 'LITERAL:javascript:scrollTo(''folio_290'');', '', '', '', 0, '', '', 'NUMBER' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 110, 'ASC', 1, 0 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 13, 1, 'LITERAL:Net Amount', '', '', 'CURRENCY', '', '', '', '', 0, '', '', 'NUMBER' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 623, 'ASC', 1, 0 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 14, 1, 'LITERAL:Convenience Fee (Included in Net Credit)', '', '', 'CURRENCY', '', '', '', '', 0, '', '', 'NUMBER' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 2706, 'ASC', 1, 0 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 15, 1, 'LITERAL:Time Period', '', '', 'MESSAGE:{0,DATE,MM/dd/yyyy hh:mm a} - {1,DATE,MM/dd/yyyy hh:mm a}', '', '', 'MESSAGE:{0,DATE,MM/dd/yyyy HH:mm}', '', 2, '', '', 'DATE' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 598, 'ASC', 0, 0 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 2, 600, 'ASC', 0, 0 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 16, 1, 'LITERAL:Schedule', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 602, 'ASC', 0, 0 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
INSERT INTO FOLIO_DIRECTIVE(FOLIO_DIRECTIVE_ID, FOLIO_ID, DIRECTIVE_ID, DIRECTIVE_VALUE) (SELECT SEQ_FOLIO_DIRECTIVE_ID.NEXTVAL, l_ids(1), 14, '2.0' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
INSERT INTO FOLIO_DIRECTIVE(FOLIO_DIRECTIVE_ID, FOLIO_ID, DIRECTIVE_ID, DIRECTIVE_VALUE) (SELECT SEQ_FOLIO_DIRECTIVE_ID.NEXTVAL, l_ids(1), 2, 'false' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
l_ids.EXTEND; SELECT SEQ_FILTER_GROUP_ID.NEXTVAL INTO l_ids(l_ids.LAST) FROM DUAL;
INSERT INTO FILTER_GROUP(PARENT_GROUP_ID, FILTER_GROUP_ID, SEPARATOR) (SELECT NULL, l_ids(l_ids.LAST), 'AND' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
l_ids.EXTEND; SELECT SEQ_FILTER_ID.NEXTVAL INTO l_ids(l_ids.LAST) FROM DUAL;
INSERT INTO FILTER(FILTER_ID, FIELD_ID, FILTER_OPERATOR_ID, AGGREGATE_TYPE_ID, FILTER_GROUP_ID) (SELECT l_ids(l_ids.LAST), 555, 1, 0, l_ids(l_ids.LAST-1) FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
INSERT INTO FILTER_PARAM(FILTER_ID, PARAM_INDEX, PARAM_NAME, PARAM_PROMPT, PARAM_VALUE, PARAM_LABEL, PARAM_SQL_TYPE, PARAM_EDITOR) (SELECT l_ids(l_ids.LAST), 1, 'DocId', 'Enter the value for DocId', '', 'DocId', 'NUMERIC', 'NUMBER' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
l_ids.TRIM;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
l_ids.EXTEND; SELECT SEQ_FILTER_ID.NEXTVAL INTO l_ids(l_ids.LAST) FROM DUAL;
INSERT INTO FILTER(FILTER_ID, FIELD_ID, FILTER_OPERATOR_ID, AGGREGATE_TYPE_ID, FILTER_GROUP_ID) (SELECT l_ids(l_ids.LAST), 629, 1, 0, l_ids(l_ids.LAST-1) FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
INSERT INTO FILTER_PARAM(FILTER_ID, PARAM_INDEX, PARAM_NAME, PARAM_PROMPT, PARAM_VALUE, PARAM_LABEL, PARAM_SQL_TYPE, PARAM_EDITOR) (SELECT l_ids(l_ids.LAST), 1, '', '', 'Y', '', 'VARCHAR', 'TEXT' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
l_ids.TRIM;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
UPDATE FOLIO SET FILTER_GROUP_ID = l_ids(l_ids.LAST) WHERE FOLIO_ID = l_ids(1);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
l_ids.TRIM;       
commit;
END;  

/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/FOLIO_CONF/folio_exports/folio_sales_rollup_field_update2.sql?rev=HEAD
INSERT INTO FOLIO_CONF.FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
 VALUES(2704, 'Convenience Fee','','NVL(REPORT.TRANS_STAT_BY_DAY.CONVENIENCE_FEE,0)','NVL(REPORT.TRANS_STAT_BY_DAY.CONVENIENCE_FEE,0)','','DECIMAL(15,2)','DECIMAL(15,2)',50,497,'Y');
 
 
INSERT INTO FOLIO_CONF.FIELD_PRIV (USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID) VALUES(1, 2704, NULL);
INSERT INTO FOLIO_CONF.FIELD_PRIV (USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID) VALUES(2, 2704, NULL);
INSERT INTO FOLIO_CONF.FIELD_PRIV (USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID) VALUES(3, 2704, NULL);
INSERT INTO FOLIO_CONF.FIELD_PRIV (USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID) VALUES(4, 2704, 1);
INSERT INTO FOLIO_CONF.FIELD_PRIV (USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID) VALUES(5, 2704, 1);
INSERT INTO FOLIO_CONF.FIELD_PRIV (USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID) VALUES(8, 2704, 1);

commit;

--rollback
--delete from FOLIO_CONF.FIELD_PRIV where field_id=2704;


 
 


-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/FOLIO_CONF/folio_exports/folio_sales_rollup.sql?rev=HEAD
SET DEFINE OFF;
ALTER SESSION SET CURRENT_SCHEMA = FOLIO_CONF;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
DECLARE                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
  l_ids NUMBER_TABLE := NUMBER_TABLE(0, 0);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
BEGIN                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
FOLIO_CONF.FOLIO_PKG.DELETE_FOLIO(969);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
SELECT 969 INTO l_ids(1) FROM DUAL;
INSERT INTO FOLIO(FOLIO_ID, FOLIO_NAME, FOLIO_TITLE, FOLIO_SUBTITLE, DEFAULT_OUTPUT_TYPE_ID, OWNER_USER_ID, DEFAULT_CHART_TYPE_ID, MAX_ROWS_PER_SECTION, MAX_ROWS) (SELECT l_ids(1), 'Sales Rollup', 'LITERAL:Sales Rollup', 'MESSAGE:From {params.beginDate,DATE, MM/dd/yyyy} to {params.endDate,DATE, MM/dd/yyyy}', 22, 932, NULL, -1, -1 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 1, 1, 'LITERAL:Customer', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 2563, 'ASC', 0, 0 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 2, 1, 'LITERAL:Region', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 2565, 'ASC', 0, 0 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 3, 1, 'LITERAL:Location', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 2567, 'ASC', 0, 0 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 4, 1, 'LITERAL:Location Type', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 2569, 'ASC', 0, 0 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 5, 1, 'LITERAL:Serial #', '', '', 'MESSAGE:{0}', 'MESSAGE:run_report_async.i?folioId=971&params.beginDate={params.beginDate, DATE,MM/dd/yyyy}&params.endDate={params.endDate,DATE,MM/dd/yyyy}&params.terminalId={1}&params.eportId={2}&params.currencyId={3}', '', '', '', 2, '', '', 'STRING' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 60, 'ASC', 0, 0 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 2, 2581, 'ASC', 0, 0 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 3, 2583, 'ASC', 0, 0 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 4, 2599, 'ASC', 0, 0 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 6, 1, 'LITERAL:Asset #', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 2561, 'ASC', 0, 0 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 7, 1, 'LITERAL:Make', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 2571, 'ASC', 0, 0 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 8, 1, 'LITERAL:Model', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 2573, 'ASC', 0, 0 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 9, 1, 'LITERAL:City', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 2575, 'ASC', 0, 0 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 10, 1, 'LITERAL:State', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 2577, 'ASC', 0, 0 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 11, 1, 'LITERAL:Product Type', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 2579, 'ASC', 0, 0 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 12, 1, 'LITERAL:Trans Type Name', '', '', '', '', '', '', '', 1, '', '', 'STRING' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 2551, 'ASC', 0, 0 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 13, 1, 'LITERAL:Tran Count', '', '', '', '', '', '', '', 0, '', '', 'NUMBER' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 2555, 'ASC', 1, 0 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 14, 1, 'LITERAL:Vend Count', '', '', '', '', '', '', '', 0, '', '', 'NUMBER' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 2557, 'ASC', 1, 0 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 15, 1, 'LITERAL:Amount', '', '', 'MESSAGE:{1}{0, NUMBER,#,###,###,###,##0.00;(#,###,###,###,##0.00)}', '', '', 'MESSAGE:{0, NUMBER,#,###,###,###,##0.00;(#,###,###,###,##0.00)}', '', 0, 'MESSAGE:{0, PERCENT}', '', 'NUMBER' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                 
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 2559, 'ASC', 1, 0 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 2, 181, 'ASC', 0, 0 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 16, 1, 'LITERAL:Currency Code', '', '', '', '', '', '', '', 3, '', '', 'STRING' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 182, 'ASC', 0, 0 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 17, 1, 'LITERAL:Convenience Fee (Included in Net Credit)', 'con', '', 'MESSAGE:{1}{0, NUMBER,#,###,###,###,##0.00;(#,###,###,###,##0.00)}', '', '', 'MESSAGE:{0, NUMBER,#,###,###,###,##0.00;(#,###,###,###,##0.00)}', '', -3, '', '', 'NUMBER' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                               
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 2704, 'ASC', 1, 0 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 2, 181, 'ASC', 0, 0 FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
INSERT INTO FOLIO_DIRECTIVE(FOLIO_DIRECTIVE_ID, FOLIO_ID, DIRECTIVE_ID, DIRECTIVE_VALUE) (SELECT SEQ_FOLIO_DIRECTIVE_ID.NEXTVAL, l_ids(1), 14, '2.0' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
l_ids.EXTEND; SELECT SEQ_FILTER_GROUP_ID.NEXTVAL INTO l_ids(l_ids.LAST) FROM DUAL;
INSERT INTO FILTER_GROUP(PARENT_GROUP_ID, FILTER_GROUP_ID, SEPARATOR) (SELECT NULL, l_ids(l_ids.LAST), 'AND' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
l_ids.EXTEND; SELECT SEQ_FILTER_ID.NEXTVAL INTO l_ids(l_ids.LAST) FROM DUAL;
INSERT INTO FILTER(FILTER_ID, FIELD_ID, FILTER_OPERATOR_ID, AGGREGATE_TYPE_ID, FILTER_GROUP_ID) (SELECT l_ids(l_ids.LAST), 2553, 28, 0, l_ids(l_ids.LAST-1) FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
INSERT INTO FILTER_PARAM(FILTER_ID, PARAM_INDEX, PARAM_NAME, PARAM_PROMPT, PARAM_VALUE, PARAM_LABEL, PARAM_SQL_TYPE, PARAM_EDITOR) (SELECT l_ids(l_ids.LAST), 1, 'beginDate', 'Enter the value for beginDate', '{*MONTH}', 'beginDate', 'DATE', 'DATE:MM/dd/yyyy' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
INSERT INTO FILTER_PARAM(FILTER_ID, PARAM_INDEX, PARAM_NAME, PARAM_PROMPT, PARAM_VALUE, PARAM_LABEL, PARAM_SQL_TYPE, PARAM_EDITOR) (SELECT l_ids(l_ids.LAST), 2, 'endDate', 'Enter the value for endDate', '{*DAY}', 'endDate', 'DATE', 'DATE:MM/dd/yyyy' FROM DUAL);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
l_ids.TRIM;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
UPDATE FOLIO SET FILTER_GROUP_ID = l_ids(l_ids.LAST) WHERE FOLIO_ID = l_ids(1);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
l_ids.TRIM;   
commit;
END;    

/

