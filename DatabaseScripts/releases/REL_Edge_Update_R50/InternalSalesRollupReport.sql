DECLARE
	LN_WEB_LINK_ID web_content.web_link.WEB_LINK_ID%TYPE;
BEGIN
	
	SELECT MAX(web_link_id) INTO LN_WEB_LINK_ID
	FROM web_content.web_link WHERE web_link_label='Sales Rollup Internal';

	IF LN_WEB_LINK_ID > 0 THEN
		RETURN;
	END IF;
	
	select max(web_link_id)+1 into LN_WEB_LINK_ID from web_content.web_link;
		
	insert into web_content.web_link(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER) values(LN_WEB_LINK_ID, 'Sales Rollup Internal', './select_date_range_frame.i?folioId=1854'||'&'||'outputType=27'||'&'||'startParamName=params.beginDate'||'&'||'StartDate={*DAY-1}'||'&'||'endParamName=params.endDate'||'&'||'EndDate={*DAY-1}', 'Sales Rollup Internal', 'Daily Operations', 'W', 0);
	insert into web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) select web_link_id, 1 from web_content.web_link where web_link_label = 'Sales Rollup Internal';
	insert into web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) select web_link_id, 14 from web_content.web_link where web_link_label = 'Sales Rollup Internal';

	COMMIT;
END;
/