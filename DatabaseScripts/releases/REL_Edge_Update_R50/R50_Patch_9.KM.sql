CREATE OR REPLACE FUNCTION KM.UPSERT_ACCOUNT_ID(
    pn_old_global_account_id OUT KM.ACCOUNT.GLOBAL_ACCOUNT_ID%TYPE,
    pn_old_instance OUT KM.ACCOUNT.SOURCE_INSTANCE%TYPE,
    pn_changed OUT BOOLEAN,
    pn_global_account_id KM.ACCOUNT.GLOBAL_ACCOUNT_ID%TYPE,
    pb_encrypted_account_cd KM.ACCOUNT.ENCRYPTED_ACCOUNT_CD%TYPE,
    pb_encrypted_crc KM.ACCOUNT.ENCRYPTED_CRC%TYPE,
    pn_instance KM.ACCOUNT.SOURCE_INSTANCE%TYPE)
    SECURITY DEFINER
AS $$
DECLARE
    ln_old_encrypted_crc KM.ACCOUNT.ENCRYPTED_CRC%TYPE;
BEGIN
    FOR i IN 1..10 LOOP
        BEGIN
            INSERT INTO KM.ACCOUNT(ENCRYPTED_ACCOUNT_CD, GLOBAL_ACCOUNT_ID, SOURCE_INSTANCE, ENCRYPTED_CRC)
                VALUES(pb_encrypted_account_cd, pn_global_account_id, pn_instance, pb_encrypted_crc);
            pn_changed := TRUE;
            RETURN;
        EXCEPTION
            WHEN unique_violation THEN  
            -- Allow multiple rows (one for each instance) so that even it we override it we can still get the info with the old global_account_id
            --      this will also solve the issue of a TOKEN record without an ACCOUNT record
                SELECT GLOBAL_ACCOUNT_ID, SOURCE_INSTANCE, ENCRYPTED_CRC
                  INTO pn_old_global_account_id, pn_old_instance, ln_old_encrypted_crc
                  FROM KM.ACCOUNT
                 WHERE ENCRYPTED_ACCOUNT_CD = pb_encrypted_account_cd
                 ORDER BY SOURCE_INSTANCE;
                 IF pn_old_global_account_id IS NULL THEN
                    -- Didn't find the row which means either it was deleted out from under us or the global_account_id/source_instance already exists but not with this encrypted_account_cd
                    UPDATE KM.ACCOUNT
                       SET ENCRYPTED_ACCOUNT_CD = pb_encrypted_account_cd,
                           ENCRYPTED_CRC = COALESCE(pb_encrypted_crc, ENCRYPTED_CRC)
                     WHERE GLOBAL_ACCOUNT_ID = pn_global_account_id
                       AND SOURCE_INSTANCE = pn_instance;
                    IF NOT FOUND THEN
                        CONTINUE; -- Try again
                    END IF;
                    pn_changed := TRUE;
                    pn_old_global_account_id := pn_global_account_id;
                    pn_old_instance := pn_instance;
                    RETURN;
                ELSIF pn_old_instance = 0 AND pn_instance = 0 AND pn_old_global_account_id < pn_global_account_id THEN
                    -- Global Account Id is Updated
                    UPDATE KM.ACCOUNT
                       SET GLOBAL_ACCOUNT_ID = pn_global_account_id,
                           ENCRYPTED_CRC = COALESCE(pb_encrypted_crc, ENCRYPTED_CRC)
                     WHERE ENCRYPTED_ACCOUNT_CD = pb_encrypted_account_cd
                       AND SOURCE_INSTANCE = pn_instance
                       AND GLOBAL_ACCOUNT_ID = pn_old_global_account_id;
                    IF NOT FOUND THEN
                        CONTINUE; -- Try again
                    END IF;
                    pn_changed := TRUE;
                    RETURN;
                ELSIF pn_old_instance < pn_instance OR (pn_old_instance = pn_instance AND pn_old_global_account_id >= pn_global_account_id 
                        AND (pb_encrypted_crc IS NULL OR pb_encrypted_crc IS NOT DISTINCT FROM ln_old_encrypted_crc))                   
                        THEN
                    pn_changed := FALSE;
                    RETURN;
                END IF;
                IF pb_encrypted_crc IS NOT NULL THEN
                    UPDATE KM.ACCOUNT
                       SET ENCRYPTED_CRC = pb_encrypted_crc
                     WHERE ENCRYPTED_ACCOUNT_CD = pb_encrypted_account_cd
                       AND SOURCE_INSTANCE = pn_instance
                       AND GLOBAL_ACCOUNT_ID = pn_global_account_id;
                END IF;
                pn_changed := TRUE;
                RETURN;
        END;    
    END LOOP;
    RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;
