create table yhe.tmp_apriva_heartland_03282016 as
select * from PSS.PAYMENT_SUBTYPE where PAYMENT_SUBTYPE_NAME = 'Apriva - Heartland Campus';

insert into PSS.AUTHORITY_PAYMENT_MASK(AUTHORITY_PAYMENT_MASK_ID,AUTHORITY_PAYMENT_MASK_NAME,AUTHORITY_PAYMENT_MASK_DESC,AUTHORITY_PAYMENT_MASK_REGEX,AUTHORITY_PAYMENT_MASK_BREF, STATUS_CD)
select 2000754,'Generic Special Card Track 2 (optional after=)','Generic Special Card Track 2 (optional after=)','^;?(?!(?:4[0-9]{15}|5[1-5][0-9]{14}|3[47][0-9]{13}|(?:30[0-5][0-9]|3095|35[2-8][0-9]|36|3[8-9][0-9]{2}|6011|622[1-9]|62[4-6][0-9]|628[2-8]|64[4-9][0-9]|65[0-9]{2})[0-9]{12}|639621[0-9]{10,13}|627722[0-9]{10})(?:=[0-9]+|$))([0-9]{5,}(?:=[0-9]*)?)(?:\?([\x00-\xFF])?)?$','1:1','A'
from dual where not exists (select 1 from PSS.AUTHORITY_PAYMENT_MASK where AUTHORITY_PAYMENT_MASK_ID=2000754);

update PSS.PAYMENT_SUBTYPE set authority_payment_mask_id=2000754 where PAYMENT_SUBTYPE_NAME = 'Apriva - Heartland Campus';
commit;