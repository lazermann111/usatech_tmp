create or replace
FUNCTION      PSS.GET_PROMOTION_CAMPAIGN_ID (
    l_pos_pta_id PSS.POS_PTA.POS_PTA_ID%TYPE,
    ld_tran_start_ts PSS.TRAN.TRAN_START_TS%TYPE)
  RETURN  NUMBER
  IS
    l_campaign_id NUMBER;
BEGIN
    SELECT MAX(CAMPAIGN_ID) into l_campaign_id
          FROM (
        SELECT C.CAMPAIGN_ID
          FROM PSS.CAMPAIGN_POS_PTA CPP
          JOIN REPORT.CAMPAIGN C ON CPP.CAMPAIGN_ID = C.CAMPAIGN_ID
          JOIN PSS.POS_PTA PTA ON CPP.POS_PTA_ID = PTA.POS_PTA_ID
          JOIN PSS.CURRENCY cur ON NVL(pta.CURRENCY_CD, 'USD') = cur.CURRENCY_CD
          JOIN PSS.POS P ON P.POS_ID = PTA.POS_ID
          JOIN LOCATION.LOCATION L ON NVL(P.LOCATION_ID, 1) = L.LOCATION_ID
          JOIN LOCATION.TIME_ZONE TZ ON L.LOCATION_TIME_ZONE_CD = TZ.TIME_ZONE_CD
         WHERE CPP.POS_PTA_ID = l_pos_pta_id
           AND ld_tran_start_ts BETWEEN NVL(CPP.START_DATE, MIN_DATE) AND NVL(CPP.END_DATE, MAX_DATE)
           AND C.CAMPAIGN_TYPE_ID = 4 
         ORDER BY CPP.PRIORITY, C.NTH_VEND_FREE_NUM, C.CAMPAIGN_ID DESC)
         WHERE ROWNUM = 1;
   RETURN l_campaign_id;
END; 
/

GRANT EXECUTE ON PSS.GET_PROMOTION_CAMPAIGN_ID to USAT_DEV_READ_ONLY;
GRANT EXECUTE ON PSS.GET_PROMOTION_CAMPAIGN_ID to USALIVE_APP_ROLE;

create or replace
FUNCTION      REPORT.CONVERT_NUMBER_TO_WORD (pn_number NUMBER)
  RETURN  VARCHAR
  IS
  l_return VARCHAR2(4000);
BEGIN
   select pn_number||substr( to_char( to_date( abs( decode( mod( pn_number, 10 ), 0, 4, mod( pn_number , 10 )  ) ), 'yyyy' ), 'yth' ), 2 ) 
   into l_return from dual;
   RETURN l_return;
END; 
/

GRANT EXECUTE ON REPORT.CONVERT_NUMBER_TO_WORD to USAT_DEV_READ_ONLY;
GRANT EXECUTE ON REPORT.CONVERT_NUMBER_TO_WORD to USALIVE_APP_ROLE;

DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE;
	LV_REPORT_NAME VARCHAR(50) := 'Promotion Devices';
BEGIN
	SELECT MAX(REPORT_ID) INTO LN_REPORT_ID
	FROM REPORT.REPORTS WHERE REPORT_NAME = LV_REPORT_NAME;
	
	IF LN_REPORT_ID > 0 THEN
		RETURN;
	ELSE
		LN_REPORT_ID:=429;
	END IF;


	INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
	SELECT LN_REPORT_ID, 'Promotion Devices', 6, 0, 'Promotion Devices', 'Promotion Devices report', 'U', 0
	FROM DUAL;

	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
	VALUES(LN_REPORT_ID, 'query', 'select CUSTOMER_NAME "Customer", 
LOCATION_NAME "Location",
EPORT_SERIAL_NUM "Device",
TERMINAL_NBR "Terminal",
ADDRESS1 "Address",
CITY "City",
STATE "State",
ZIP "Zip",
listagg(campaign, '','' ) WITHIN GROUP (ORDER BY campaign) "Campaign",
lastActivityTime "Last Activity Time"
from 
(SELECT
    distinct
		C.CUSTOMER_NAME , L.LOCATION_NAME, E.EPORT_SERIAL_NUM, T.TERMINAL_NBR , TA.ADDRESS1 , TA.CITY , TA.STATE , TA.ZIP , 
    CAM.CAMPAIGN_NAME||'' - ''||REPORT.CONVERT_NUMBER_TO_WORD(CAM.NTH_VEND_FREE_NUM)||'' purchase free up to $''||CAM.FREE_VEND_MAX_AMOUNT Campaign,
    TO_CHAR(D.LAST_ACTIVITY_TS, ''MM/DD/YYYY HH24:MI:SS'') lastActivityTime
		FROM DEVICE.DEVICE D
		JOIN PSS.POS P ON D.DEVICE_ID = P.DEVICE_ID
		JOIN PSS.POS_PTA PP ON P.POS_ID = PP.POS_ID AND PP.POS_PTA_ACTIVATION_TS < SYSDATE AND (PP.POS_PTA_DEACTIVATION_TS IS NULL OR 
PP.POS_PTA_DEACTIVATION_TS > SYSDATE)
		JOIN PSS.PAYMENT_SUBTYPE PS ON PP.PAYMENT_SUBTYPE_ID = PS.PAYMENT_SUBTYPE_ID AND PS.PAYMENT_SUBTYPE_CLASS = ''Promotion'' 
    join PSS.CAMPAIGN_POS_PTA CPP on cpp.pos_pta_id=pp.pos_pta_id and sysdate >= cpp.start_date and sysdate <cpp.end_date
    JOIN REPORT.CAMPAIGN CAM on CAM.CAMPAIGN_ID=CPP.CAMPAIGN_ID and campaign_type_id=4
		JOIN REPORT.EPORT E ON D.DEVICE_SERIAL_CD = E.EPORT_SERIAL_NUM
		JOIN REPORT.VW_TERMINAL_EPORT TE ON E.EPORT_ID = TE.EPORT_ID
		JOIN REPORT.TERMINAL T ON TE.TERMINAL_ID = T.TERMINAL_ID
		JOIN REPORT.VW_USER_TERMINAL VUT ON T.TERMINAL_ID = VUT.TERMINAL_ID AND VUT.TERMINAL_ID != 0
		LEFT OUTER JOIN CORP.CUSTOMER C ON T.CUSTOMER_ID = C.CUSTOMER_ID
		LEFT OUTER JOIN REPORT.LOCATION L ON T.LOCATION_ID = L.LOCATION_ID
		LEFT OUTER JOIN REPORT.TERMINAL_ADDR TA ON L.ADDRESS_ID = TA.ADDRESS_ID
		WHERE vut.user_id= ?
		AND D.DEVICE_ACTIVE_YN_FLAG = ''Y''
		ORDER BY C.CUSTOMER_NAME, L.LOCATION_NAME, E.EPORT_SERIAL_NUM
    )
    group by CUSTOMER_NAME, LOCATION_NAME, EPORT_SERIAL_NUM, TERMINAL_NBR, ADDRESS1, CITY, STATE, ZIP,lastActivityTime
');
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'header', 'true');
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'isSQLFolio', 'true');
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'paramNames', 'userId');
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'paramTypes', 'NUMERIC');
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'params.userId', '{u}');
		
	insert into web_content.web_link(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER) values(web_content.seq_web_link_id.nextval, 'Promotion Devices', './run_sql_folio_report_async.i?basicReportId=' || LN_REPORT_ID, 'Promotion Devices report', 'Daily Operations', 'P', 0);
	insert into web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) select web_link_id, 1 from web_content.web_link where web_link_label = 'Promotion Devices';
	insert into web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) select web_link_id, 45 from web_content.web_link where web_link_label = 'Promotion Devices';
	
	COMMIT;
END;
/

DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := -1;
	LV_REPORT_NAME VARCHAR(50) := 'Promotion Purchase Free Transactions';
BEGIN
	
	SELECT MAX(REPORT_ID) INTO LN_REPORT_ID
	FROM REPORT.REPORTS WHERE REPORT_NAME = LV_REPORT_NAME;

	IF LN_REPORT_ID > 0 THEN
		RETURN;
	ELSE
		LN_REPORT_ID:=430;
	END IF;
		
	INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID, LATENCY)
	SELECT LN_REPORT_ID, 'Promotion Purchase Free Transactions - {b} to {e}', 6, 0, 'Promotion Purchase Free Transactions', 'Promotion Purchase Free Transactions report', 'U', 0, 1
	FROM DUAL;

	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
	VALUES(LN_REPORT_ID, 'query', 'SELECT TO_CHAR(X.SETTLE_DATE, ''MM/DD/YYYY HH24:MI:SS'') "Settle Date", 
E.EPORT_SERIAL_NUM "Device", TA.CITY "City", TA.STATE "State", X.TOTAL_AMOUNT "Redemption Amount",
CUR.CURRENCY_CODE "Currency", TA.ADDRESS1 "Address", TA.ZIP "Zip", X.TRAN_ID "Transaction Id", TO_CHAR(X.CLOSE_DATE, ''MM/DD/YYYY HH24:MI:SS'') "Transaction Date", CT.CARD_NAME "Card Type",
X.CARD_NUMBER "Card Number", T.TERMINAL_NBR "Terminal", C.CUSTOMER_NAME "Customer", L.LOCATION_NAME "Location", CAB.GLOBAL_ACCOUNT_ID "Card Id"
FROM REPORT.TRANS X
JOIN REPORT.TRANS_TYPE TT ON X.TRANS_TYPE_ID=TT.TRANS_TYPE_ID
JOIN REPORT.EPORT E ON X.EPORT_ID = E.EPORT_ID
JOIN REPORT.TERMINAL T ON X.TERMINAL_ID = T.TERMINAL_ID
JOIN REPORT.VW_USER_TERMINAL VUT ON T.TERMINAL_ID = VUT.TERMINAL_ID AND VUT.TERMINAL_ID != 0
LEFT OUTER JOIN CORP.CUSTOMER C ON T.CUSTOMER_ID = C.CUSTOMER_ID
LEFT OUTER JOIN REPORT.LOCATION L ON T.LOCATION_ID = L.LOCATION_ID
LEFT OUTER JOIN REPORT.TERMINAL_ADDR TA ON L.ADDRESS_ID = TA.ADDRESS_ID
LEFT OUTER JOIN CORP.CURRENCY CUR ON X.CURRENCY_ID = CUR.CURRENCY_ID
LEFT OUTER JOIN REPORT.CARDTYPE_AUTHORITY CA ON X.CARDTYPE_AUTHORITY_ID = CA.CARDTYPE_AUTHORITY_ID
LEFT OUTER JOIN REPORT.CARD_TYPE CT ON CA.CARDTYPE_ID = CT.CARD_TYPE_ID
LEFT OUTER JOIN PSS.CONSUMER_ACCT CA ON X.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
LEFT OUTER JOIN PSS.CONSUMER CON ON CA.CONSUMER_ID = CON.CONSUMER_ID
LEFT OUTER JOIN PSS.CONSUMER_ACCT_BASE CAB ON X.CONSUMER_ACCT_ID = CAB.CONSUMER_ACCT_ID
WHERE vut.user_id= ?
AND X.SETTLE_DATE >= CAST(? AS DATE) AND X.SETTLE_DATE < CAST(? AS DATE) + 1 AND TT.FREE_IND =''Y'' AND X.SETTLE_STATE_ID = 3
ORDER BY X.SETTLE_DATE, X.TRAN_ID
');
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'header', 'true');
	insert into report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) values(LN_REPORT_ID, 'dateFormat', 'MM/dd/yyyy');
	insert into report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) values(LN_REPORT_ID, 'params.StartDate', '{b}');
	insert into report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) values(LN_REPORT_ID, 'params.EndDate', '{e}');
	insert into report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) values(LN_REPORT_ID, 'paramNames', 'userId,StartDate,EndDate');
	insert into report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) values(LN_REPORT_ID, 'paramTypes', 'NUMERIC,TIMESTAMP,TIMESTAMP');
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'isSQLFolio', 'true');
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'params.userId', '{u}');
		
	insert into web_content.web_link(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER) values(web_content.seq_web_link_id.nextval, 'Promotion Purchase Free Transactions', './select_date_range_frame_sqlfolio.i?basicReportId=' || LN_REPORT_ID, 'Promotion Purchase Free Transactions report', 'Daily Operations', 'P', 0);
	insert into web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) select web_link_id, 1 from web_content.web_link where web_link_label = 'Promotion Purchase Free Transactions';
	insert into web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) select web_link_id, 45 from web_content.web_link where web_link_label = 'Promotion Purchase Free Transactions';

	COMMIT;
END;
/

DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := -1;
	LV_REPORT_NAME VARCHAR(50) := 'Promotion Purchase Free Summary';
BEGIN
	
	SELECT MAX(REPORT_ID) INTO LN_REPORT_ID
	FROM REPORT.REPORTS WHERE REPORT_NAME = LV_REPORT_NAME;

	IF LN_REPORT_ID > 0 THEN
		RETURN;
	ELSE
		LN_REPORT_ID:=431;
	END IF;
		
	INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID, LATENCY)
	SELECT LN_REPORT_ID, 'Promotion Purchase Free Summary - {b} to {e}', 6, 0, 'Promotion Purchase Free Summary', 'Promotion Purchase Free Summary report', 'U', 0, 1
	FROM DUAL;

	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
	VALUES(LN_REPORT_ID, 'query', 'SELECT C.CAMPAIGN_NAME "Campaign",SUM(X.TRAN_COUNT) "Transaction Count", SUM(X.TRAN_AMOUNT) "Total Amount", CUR.CURRENCY_CODE "Currency"
		FROM REPORT.TRANS_STAT_BY_DAY X JOIN REPORT.CAMPAIGN C on X.CAMPAIGN_ID=C.CAMPAIGN_ID
        JOIN REPORT.VW_USER_TERMINAL VUT on VUT.USER_ID=? AND VUT.TERMINAL_ID=X.TERMINAL_ID
		JOIN REPORT.TRANS_TYPE TT on X.TRANS_TYPE_ID=TT.TRANS_TYPE_ID
		LEFT OUTER JOIN CORP.CURRENCY CUR ON X.CURRENCY_ID = CUR.CURRENCY_ID
    	WHERE X.TRAN_DATE >= CAST(? AS DATE) AND X.TRAN_DATE < CAST(? AS DATE) + 1 AND TT.FREE_IND =''Y''
		GROUP BY C.CAMPAIGN_NAME, CUR.CURRENCY_CODE
		ORDER BY 1, 2, 3, 4
');
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'header', 'true');
	insert into report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) values(LN_REPORT_ID, 'dateFormat', 'MM/dd/yyyy');
	insert into report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) values(LN_REPORT_ID, 'params.StartDate', '{b}');
	insert into report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) values(LN_REPORT_ID, 'params.EndDate', '{e}');
	insert into report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) values(LN_REPORT_ID, 'paramNames', 'userId,StartDate,EndDate');
	insert into report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) values(LN_REPORT_ID, 'paramTypes', 'NUMERIC,TIMESTAMP,TIMESTAMP');
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'isSQLFolio', 'true');
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'params.userId', '{u}');
		
	insert into web_content.web_link(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER) values(web_content.seq_web_link_id.nextval, 'Promotion Purchase Free Summary', './select_date_range_frame_sqlfolio.i?basicReportId=' || LN_REPORT_ID, 'Promotion Purchase Free Summary report', 'Daily Operations', 'P', 0);
	insert into web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) select web_link_id, 1 from web_content.web_link where web_link_label = 'Promotion Purchase Free Summary';
	insert into web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) select web_link_id, 45 from web_content.web_link where web_link_label = 'Promotion Purchase Free Summary';

	COMMIT;
END;
/

DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := -1;
	LV_REPORT_NAME VARCHAR(50) := 'Promotion Purchase Free Summary by Device';
BEGIN
	
	SELECT MAX(REPORT_ID) INTO LN_REPORT_ID
	FROM REPORT.REPORTS WHERE REPORT_NAME = LV_REPORT_NAME;

	IF LN_REPORT_ID > 0 THEN
		RETURN;
	ELSE
		LN_REPORT_ID:=432;
	END IF;
		
	INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID, LATENCY)
	SELECT LN_REPORT_ID, 'Promotion Purchase Free Summary by Device - {b} to {e}', 6, 0, 'Promotion Purchase Free Summary by Device', 'Promotion Purchase Free Summary by Device report', 'U', 0, 1
	FROM DUAL;

	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
	VALUES(LN_REPORT_ID, 'query', 'SELECT C.CAMPAIGN_NAME "Campaign",E.EPORT_SERIAL_NUM "Device",
		SUM(X.TRAN_COUNT) "Transaction Count", SUM(X.TRAN_AMOUNT) "Total Amount", CUR.CURRENCY_CODE "Currency"
		FROM REPORT.TRANS_STAT_BY_DAY X JOIN REPORT.CAMPAIGN C on X.CAMPAIGN_ID=C.CAMPAIGN_ID
		JOIN REPORT.VW_USER_TERMINAL VUT on VUT.USER_ID=? AND VUT.TERMINAL_ID=X.TERMINAL_ID
		JOIN REPORT.TRANS_TYPE TT on X.TRANS_TYPE_ID=TT.TRANS_TYPE_ID
    	JOIN REPORT.EPORT E ON X.EPORT_ID = E.EPORT_ID
		LEFT OUTER JOIN CORP.CURRENCY CUR ON X.CURRENCY_ID = CUR.CURRENCY_ID
    	WHERE X.TRAN_DATE >= CAST(? AS DATE) AND X.TRAN_DATE < CAST(? AS DATE) + 1 AND TT.FREE_IND =''Y''
		GROUP BY C.CAMPAIGN_NAME, E.EPORT_SERIAL_NUM,CUR.CURRENCY_CODE
		ORDER BY 1, 2
');
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'header', 'true');
	insert into report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) values(LN_REPORT_ID, 'dateFormat', 'MM/dd/yyyy');
	insert into report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) values(LN_REPORT_ID, 'params.StartDate', '{b}');
	insert into report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) values(LN_REPORT_ID, 'params.EndDate', '{e}');
	insert into report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) values(LN_REPORT_ID, 'paramNames', 'userId,StartDate,EndDate');
	insert into report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) values(LN_REPORT_ID, 'paramTypes', 'NUMERIC,TIMESTAMP,TIMESTAMP');
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'isSQLFolio', 'true');
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'params.userId', '{u}');
		
	insert into web_content.web_link(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER) values(web_content.seq_web_link_id.nextval, 'Promotion Purchase Free Summary by Device', './select_date_range_frame_sqlfolio.i?basicReportId=' || LN_REPORT_ID, 'Promotion Purchase Free Summary by Device report', 'Daily Operations', 'P', 0);
	insert into web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) select web_link_id, 1 from web_content.web_link where web_link_label = 'Promotion Purchase Free Summary by Device';
	insert into web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) select web_link_id, 45 from web_content.web_link where web_link_label = 'Promotion Purchase Free Summary by Device';

	COMMIT;
END;
/

DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE;	
	LN_USER_ID REPORT.USER_LOGIN.USER_ID%TYPE;
	LN_USER_REPORT_ID REPORT.USER_REPORT.USER_REPORT_ID%TYPE;
	LN_CCS_TRANSPORT_ID REPORT.CCS_TRANSPORT.CCS_TRANSPORT_ID%TYPE;
	LV_EMAIL_ADDRESSES REPORT.CCS_TRANSPORT_PROPERTY.CCS_TRANSPORT_PROPERTY_VALUE%TYPE := 'Tier1@usatech.com, Tier2@usatech.com, SalesTeam@usatech.com';
	LN_WEB_LINK_ID WEB_CONTENT.WEB_LINK.WEB_LINK_ID%TYPE;
	LV_DB_NAME VARCHAR2(30);
BEGIN
	SELECT MAX(REPORT_ID) INTO LN_REPORT_ID
	FROM REPORT.REPORTS WHERE REPORT_NAME = 'Customers with Many Inactive Devices';
	
	IF LN_REPORT_ID > 0 THEN
		RETURN;
	ELSE
		LN_REPORT_ID := 434;
	END IF;
	
	SELECT USER_ID INTO LN_USER_ID FROM REPORT.USER_LOGIN WHERE USER_NAME = 'USATMaster';	

	INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
	VALUES(LN_REPORT_ID, 'Customers with Many Inactive Devices', 6, 0, 'Customers with Many Inactive Devices', 'Customers with Many Inactive Devices report', 'N', LN_USER_ID);

	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE)
	VALUES(LN_REPORT_ID, 'query', 'select c.customer_name, x.previously_active_devices, x.currently_active_devices,
ul.user_name, ul.first_name, ul.last_name, ul.email, ul.telephone, c.customer_id
from corp.customer c join (
select c.customer_id,
sum(case when e.lastdialin_date between sysdate - 60 and sysdate - 30 then 1 else 0 end) previously_active_devices,
sum(case when e.lastdialin_date > sysdate - 30 then 1 else 0 end) currently_active_devices
from report.terminal t 
join report.vw_terminal_eport te on t.terminal_id = te.terminal_id
join report.eport e on te.eport_id = e.eport_id
join corp.customer c on t.customer_id = c.customer_id
where c.status != ''D'' and t.status != ''D''
group by c.customer_id ) x on c.customer_id = x.customer_id
join report.user_login ul on c.user_id = ul.user_id
where x.previously_active_devices > 5 and x.currently_active_devices / x.previously_active_devices < 0.2
order by upper(c.customer_name)');
		
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'header', 'true');
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'isSQLFolio', 'true');
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'outputType', '21');
	
	SELECT MAX(WEB_LINK_ID) + 1 INTO LN_WEB_LINK_ID FROM WEB_CONTENT.WEB_LINK;
	
	insert into web_content.web_link(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER) values(LN_WEB_LINK_ID, 'Customers with Many Inactive Devices', './run_sql_folio_report_async.i?basicReportId=' || LN_REPORT_ID || '&' || 'outputType=22', 'Customers with Many Inactive Devices report', 'Accounting', 'W', 0);
	insert into web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) VALUES(LN_WEB_LINK_ID, 1);
	insert into web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) VALUES(LN_WEB_LINK_ID, 14);
	
	SELECT REPLACE(GLOBAL_NAME, '.WORLD', '') INTO LV_DB_NAME FROM GLOBAL_NAME;
	
	IF LV_DB_NAME = 'USAPDB' THEN
		SELECT REPORT.CCS_TRANSPORT_SEQ.NEXTVAL INTO LN_CCS_TRANSPORT_ID FROM DUAL;
		
		INSERT INTO REPORT.CCS_TRANSPORT(CCS_TRANSPORT_ID, CCS_TRANSPORT_TYPE_ID, CCS_TRANSPORT_NAME, USER_ID)
		SELECT LN_CCS_TRANSPORT_ID, 1, 'Customers with Many Inactive Devices', LN_USER_ID
		FROM DUAL;
		
		INSERT INTO REPORT.CCS_TRANSPORT_PROPERTY(CCS_TRANSPORT_PROPERTY_ID, CCS_TRANSPORT_ID, CCS_TRANSPORT_PROPERTY_TYPE_ID, CCS_TRANSPORT_PROPERTY_VALUE)
		SELECT REPORT.CCS_TRANSPORT_PROPERTY_SEQ.NEXTVAL, LN_CCS_TRANSPORT_ID, CCS_TRANSPORT_PROPERTY_TYPE_ID, LV_EMAIL_ADDRESSES
		FROM REPORT.CCS_TRANSPORT_PROPERTY_TYPE WHERE CCS_TPT_NAME = 'Email';
		
		INSERT INTO REPORT.CCS_TRANSPORT_PROPERTY(CCS_TRANSPORT_PROPERTY_ID, CCS_TRANSPORT_ID, CCS_TRANSPORT_PROPERTY_TYPE_ID, CCS_TRANSPORT_PROPERTY_VALUE)
		SELECT REPORT.CCS_TRANSPORT_PROPERTY_SEQ.NEXTVAL, LN_CCS_TRANSPORT_ID, CCS_TRANSPORT_PROPERTY_TYPE_ID, NULL
		FROM REPORT.CCS_TRANSPORT_PROPERTY_TYPE WHERE CCS_TPT_NAME = 'CC';
		
		SELECT REPORT.USER_REPORT_SEQ.NEXTVAL INTO LN_USER_REPORT_ID FROM DUAL;
		
		INSERT INTO REPORT.USER_REPORT (USER_REPORT_ID, REPORT_ID, USER_ID, CCS_TRANSPORT_ID, FREQUENCY_ID, LATENCY, STATUS)
		SELECT LN_USER_REPORT_ID, LN_REPORT_ID, LN_USER_ID, LN_CCS_TRANSPORT_ID, 2, 0.375, 'A'
		FROM DUAL;
		
		UPDATE REPORT.USER_REPORT SET UPD_DT = ADD_MONTHS(TRUNC(SYSDATE, 'MM'), -1) WHERE USER_REPORT_ID = LN_USER_REPORT_ID;
	END IF;
	
	COMMIT;
END;
/

ALTER TABLE report.reports
  MODIFY DESCRIPTION VARCHAR2(4000 BYTE);
  
update report.reports 
set description='Shows detail information on each terminal with the following columns: Customer, Region, Device, Location, Terminal, Asset #, Address, City, State, Zip, Location Details, Location Type, Machine Make, Machine Model, Product Type, Timezone, Vends Per Swipe, Authorization Mode, DEX Data Capture, Terminal Start Date, Activation Submitted Date, Business Type, Communication Method, Firmware Version'
where report_name='Terminal Details (CSV)';

update report.reports 
set description='Shows detail information on each terminal with the following columns: Customer, Region, Device, Location, Terminal, Asset #, Address, City, State, Zip, Location Details, Location Type, Machine Make, Machine Model, Product Type, Timezone, Vends Per Swipe, Authorization Mode, DEX Data Capture, Terminal Start Date, Activation Submitted Date, Business Type, Communication Method, Firmware Version'
where report_name='Terminal Details (Excel)';

update report.reports 
set description='Customer, Region, Device, Location, Terminal, Asset #, Address, City, State, Zip, Location Details, Location Type, Machine Make, Machine Model, Product Type, Timezone, Vends Per Swipe, Authorization Mode, DEX Data Capture, Terminal Start Date, Activation Submitted Date, Custom Fields (1-8), Communication Method, Firmware Version'
where report_name='Terminal Details (CSV) (Custom)';

update report.reports 
set description='Customer, Region, Device, Location, Terminal, Asset #, Address, City, State, Zip, Location Details, Location Type, Machine Make, Machine Model, Product Type, Timezone, Vends Per Swipe, Authorization Mode, DEX Data Capture, Terminal Start Date, Activation Submitted Date, Custom Fields (1-8), Communication Method, Firmware Version'
where report_name='Terminal Details (Excel) (Custom)';
commit;

