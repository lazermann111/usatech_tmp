DECLARE
    CURSOR l_cur IS
      SELECT P.POS_ID, PP.PAYMENT_SUBTYPE_KEY_ID, D.DEVICE_NAME, T.MERCHANT_ID
        FROM DEVICE.DEVICE D
        JOIN PSS.POS P ON D.DEVICE_ID = P.DEVICE_ID
        JOIN PSS.POS_PTA PP ON P.POS_ID = PP.POS_ID
        JOIN PSS.PAYMENT_SUBTYPE PS ON PP.PAYMENT_SUBTYPE_ID = PS.PAYMENT_SUBTYPE_ID
          and ps.payment_subtype_class = 'Apriva'
        JOIN pss.terminal t ON t.terminal_id = pp.payment_subtype_key_id
        JOIN pss.merchant m ON t.merchant_id = m.merchant_id
        JOIN authority.authority A ON m.authority_id = A.authority_id
          AND A.authority_name = 'Apriva'
        where PP.POS_PTA_ACTIVATION_TS < SYSDATE
        AND NVL(PP.POS_PTA_DEACTIVATION_TS, MAX_DATE) > SYSDATE
        --AND T.TERMINAL_CD = '0'
        AND t.terminal_id in (2029470, 2032739, 2032771)
        AND D.DEVICE_ACTIVE_YN_FLAG = 'Y'
      ORDER BY d.device_name;
    ln_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE;
    ln_cnt PLS_INTEGER := 0;
BEGIN
    FOR l_rec IN l_cur LOOP
        -- create or get new terminal
        IF ln_terminal_id IS NULL OR ln_cnt <= 0 THEN
            SELECT PSS.SEQ_TERMINAL_ID.NEXTVAL, 10
              INTO ln_terminal_id, ln_cnt 
              FROM DUAL;
            INSERT INTO PSS.TERMINAL(
                TERMINAL_ID,
                TERMINAL_CD, 
                TERMINAL_NEXT_BATCH_NUM,
                MERCHANT_ID,
                TERMINAL_DESC,
                TERMINAL_STATE_ID,
                TERMINAL_MAX_BATCH_NUM,
                TERMINAL_BATCH_MAX_TRAN,
                TERMINAL_BATCH_CYCLE_NUM,
                TERMINAL_MIN_BATCH_NUM,
                TERMINAL_MIN_BATCH_CLOSE_HR,
                TERMINAL_MAX_BATCH_CLOSE_HR,
                TERMINAL_BATCH_MIN_TRAN,
                TERMINAL_GROUP_ID)
              SELECT 
                ln_terminal_id,
                l_rec.DEVICE_NAME,
                1,
                MERCHANT_ID,
                TERMINAL_DESC || ' for ' || l_rec.DEVICE_NAME,
                1,
                TERMINAL_MAX_BATCH_NUM,
                TERMINAL_BATCH_MAX_TRAN,
                TERMINAL_BATCH_CYCLE_NUM,
                TERMINAL_MIN_BATCH_NUM,
                TERMINAL_MIN_BATCH_CLOSE_HR,
                TERMINAL_MAX_BATCH_CLOSE_HR,
                TERMINAL_BATCH_MIN_TRAN,
                TERMINAL_GROUP_ID
              FROM PSS.TERMINAL
             WHERE TERMINAL_ID = l_rec.PAYMENT_SUBTYPE_KEY_ID;
        END IF;
        
        -- set payment_subtype_key_id
        UPDATE PSS.POS_PTA PP
           SET PAYMENT_SUBTYPE_KEY_ID = ln_terminal_id
         WHERE PAYMENT_SUBTYPE_KEY_ID = l_rec.PAYMENT_SUBTYPE_KEY_ID
           AND POS_ID = l_rec.POS_ID
           AND (SELECT PS.PAYMENT_SUBTYPE_CLASS FROM PSS.PAYMENT_SUBTYPE PS WHERE PS.PAYMENT_SUBTYPE_ID = PP.PAYMENT_SUBTYPE_ID) = 'Apriva';
           
        COMMIT;
        ln_cnt := ln_cnt - 1;
    END LOOP;
END;
/
