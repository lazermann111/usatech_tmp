DECLARE
  PROCEDURE CREATE_TANDEM_MERCHANT (
    pv_tdid PSS.MERCHANT.AUTHORITY_ASSIGNED_NUMBER%TYPE,
    pv_merchant_name PSS.MERCHANT.MERCHANT_NAME%TYPE,
    pv_dba PSS.MERCHANT.DOING_BUSINESS_AS%TYPE,
    pv_merchant_cd PSS.MERCHANT.MERCHANT_CD%TYPE,
    pn_mcc PSS.MERCHANT.MCC%TYPE,
    pv_merchant_bus_name PSS.MERCHANT.MERCHANT_BUS_NAME%TYPE)
  IS
    ln_exists_count NUMBER;
    ln_new_merchant_id NUMBER;
    ln_new_terminal_id NUMBER;
    ln_new_pos_pta_tmpl_id NUMBER;
  BEGIN
    SELECT COUNT(1)
    INTO ln_exists_count
    FROM PSS.MERCHANT 
    WHERE MERCHANT_CD = pv_merchant_cd
    AND AUTHORITY_ID = (select authority_id from authority.authority where authority_name = 'Tandem');
    
    IF ln_exists_count > 0 THEN
      dbms_output.put_line('Merchant account already exists: '||pv_merchant_cd);
      RETURN;
    END IF;
    
    ln_new_merchant_id := PSS.SEQ_merchant_id.nextval;
    dbms_output.put_line('ln_new_merchant_id='||ln_new_merchant_id);
    
    INSERT INTO PSS.MERCHANT(MERCHANT_ID, MERCHANT_CD, MERCHANT_NAME, MERCHANT_DESC, MERCHANT_BUS_NAME, AUTHORITY_ID, DOING_BUSINESS_AS, MCC, AUTHORITY_ASSIGNED_NUMBER)
    SELECT ln_new_merchant_id, pv_merchant_cd, 'SWAP TEST: ' || pv_merchant_name, pv_merchant_name, pv_merchant_bus_name, AUTHORITY_ID, pv_dba, pn_mcc, pv_tdid
    FROM AUTHORITY.AUTHORITY M
    WHERE AUTHORITY_NAME = 'Tandem';
    
    ln_new_terminal_id := PSS.SEQ_terminal_id.nextval;
    dbms_output.put_line('ln_new_terminal_id='||ln_new_terminal_id);

    INSERT INTO PSS.TERMINAL(TERMINAL_ID, TERMINAL_CD, TERMINAL_NEXT_BATCH_NUM, MERCHANT_ID, TERMINAL_DESC, TERMINAL_STATE_ID, TERMINAL_MAX_BATCH_NUM, TERMINAL_BATCH_MAX_TRAN, TERMINAL_BATCH_CYCLE_NUM, TERMINAL_MIN_BATCH_NUM, TERMINAL_MIN_BATCH_CLOSE_HR, TERMINAL_MAX_BATCH_CLOSE_HR, TERMINAL_BATCH_MIN_TRAN)
    VALUES (ln_new_terminal_id, '001', 1, ln_new_merchant_id, 'SWAP TEST: ' || pv_merchant_name || ' Terminal', 1, 999, 1, 1, 1, 0, 4, 1);

    ln_new_pos_pta_tmpl_id := PSS.SEQ_POS_PTA_TMPL_ID.nextval;
    dbms_output.put_line('ln_new_pos_pta_tmpl_id='||ln_new_pos_pta_tmpl_id);
    
    INSERT INTO PSS.POS_PTA_TMPL(pos_pta_tmpl_id, pos_pta_tmpl_name, pos_pta_tmpl_desc, status_cd)
    VALUES (ln_new_pos_pta_tmpl_id, 'SWAP TEST: ' || pv_merchant_name, pv_merchant_name, 'A');
    
    insert into pss.pos_pta_tmpl_entry(pos_pta_tmpl_id, payment_subtype_id, pos_pta_encrypt_key, pos_pta_activation_oset_hr, pos_pta_deactivation_oset_hr, payment_subtype_key_id, pos_pta_pin_req_yn_flag, pos_pta_device_serial_cd, pos_pta_encrypt_key2, authority_payment_mask_id, pos_pta_priority, terminal_id, merchant_bank_acct_id, currency_cd, pos_pta_passthru_allow_yn_flag, pos_pta_pref_auth_amt, pos_pta_pref_auth_amt_max, pos_pta_disable_debit_denial, no_convenience_fee)
    select ln_new_pos_pta_tmpl_id, te.payment_subtype_id, te.pos_pta_encrypt_key, te.pos_pta_activation_oset_hr, te.pos_pta_deactivation_oset_hr, case when ps.payment_subtype_class = 'Tandem' then ln_new_terminal_id else te.payment_subtype_key_id end, te.pos_pta_pin_req_yn_flag, te.pos_pta_device_serial_cd, te.pos_pta_encrypt_key2, te.authority_payment_mask_id, te.pos_pta_priority, te.terminal_id, te.merchant_bank_acct_id, te.currency_cd, te.pos_pta_passthru_allow_yn_flag, te.pos_pta_pref_auth_amt, te.pos_pta_pref_auth_amt_max, te.pos_pta_disable_debit_denial, te.no_convenience_fee
    from pss.pos_pta_tmpl_entry te
    join pss.payment_subtype ps on ps.payment_subtype_id = te.payment_subtype_id
    where pos_pta_tmpl_id = 2005924; -- usadev04 = 693

  END;
BEGIN
  CREATE_TANDEM_MERCHANT('265028', 'USA Technologies Vending 01', 'Soda Snack Vending', '612000000000', 5814, 'USA Technologies Vending');
  CREATE_TANDEM_MERCHANT('265029', 'USA Technologies Vending 02', 'Soda Snack Vending', '641000000000', 5814, 'USA Technologies Vending');
  CREATE_TANDEM_MERCHANT('265030', 'USA Technologies Vending 03', 'Soda Snack Vending', '645200000000', 5814, 'USA Technologies Vending');
  CREATE_TANDEM_MERCHANT('265032', 'USA Technologies Vending 04', 'Soda Snack Vending', '677500000000', 5814, 'USA Technologies Vending');
  CREATE_TANDEM_MERCHANT('265034', 'USA Technologies Vending 05', 'Soda Snack Vending', '032258065000', 5814, 'USA Technologies Vending');
  CREATE_TANDEM_MERCHANT('265035', 'USA Technologies Vending 06', 'Soda Snack Vending', '741000000000', 5814, 'USA Technologies Vending');
  CREATE_TANDEM_MERCHANT('265037', 'USA Technologies Vending 07', 'Soda Snack Vending', '774000000000', 5814, 'USA Technologies Vending');
  CREATE_TANDEM_MERCHANT('265038', 'USA Technologies Vending 08', 'Soda Snack Vending', '129000000000', 5814, 'USA Technologies Vending');
  CREATE_TANDEM_MERCHANT('265039', 'USA Technologies Vending 09', 'Soda Snack Vending', '230000002200', 5814, 'USA Technologies Vending');
  CREATE_TANDEM_MERCHANT('265040', 'USA Technologies Vending 10', 'Soda Snack Vending', '064999999000', 5814, 'USA Technologies Vending');
  CREATE_TANDEM_MERCHANT('265043', 'USA Technologies Vending 11', 'Soda Snack Vending', '807000000000', 5814, 'USA Technologies Vending');
  CREATE_TANDEM_MERCHANT('265044', 'USA Technologies Vending 12', 'Soda Snack Vending', '840000000000', 5814, 'USA Technologies Vending');
  CREATE_TANDEM_MERCHANT('265046', 'USA Technologies Vending 13', 'Soda Snack Vending', '871000000000', 5814, 'USA Technologies Vending');
  CREATE_TANDEM_MERCHANT('265048', 'USA Technologies Vending 14', 'Soda Snack Vending', '920000000000', 5814, 'USA Technologies Vending');
  CREATE_TANDEM_MERCHANT('265049', 'USA Technologies Vending 15', 'Soda Snack Vending', '889000000000', 5814, 'USA Technologies Vending');
  CREATE_TANDEM_MERCHANT('265050', 'USA Technologies Vending 16', 'Soda Snack Vending', '968000000000', 5814, 'USA Technologies Vending');
  CREATE_TANDEM_MERCHANT('265051', 'USA Technologies Vending 17', 'Soda Snack Vending', '193000000000', 5814, 'USA Technologies Vending');
  CREATE_TANDEM_MERCHANT('265053', 'USA Technologies Vending 18', 'Soda Snack Vending', '199999999000', 5814, 'USA Technologies Vending');
  CREATE_TANDEM_MERCHANT('265054', 'USA Technologies Vending 19', 'Soda Snack Vending', '483000000000', 5814, 'USA Technologies Vending');
  CREATE_TANDEM_MERCHANT('265055', 'USA Technologies Vending 20', 'Soda Snack Vending', '290300000000', 5814, 'USA Technologies Vending');
  CREATE_TANDEM_MERCHANT('265057', 'USA Technologies Vending 21', 'Soda Snack Vending', '548000000000', 5814, 'USA Technologies Vending');
  CREATE_TANDEM_MERCHANT('265059', 'USA Technologies Vending 22', 'Soda Snack Vending', '333000000500', 5814, 'USA Technologies Vending');
  CREATE_TANDEM_MERCHANT('265060', 'USA Technologies Vending 23', 'Soda Snack Vending', '371000000000', 5814, 'USA Technologies Vending');
  CREATE_TANDEM_MERCHANT('265061', 'USA Technologies Vending 24', 'Soda Snack Vending', '417000134000', 5814, 'USA Technologies Vending');
  CREATE_TANDEM_MERCHANT('265063', 'USA Technologies Vending 25', 'Soda Snack Vending', '451320000000', 5814, 'USA Technologies Vending');
  CREATE_TANDEM_MERCHANT('265064', 'USA Technologies Vending 26', 'Soda Snack Vending', '800900000000', 5814, 'USA Technologies Vending');
  CREATE_TANDEM_MERCHANT('265065', 'USA Technologies Vending 27', 'Soda Snack Vending', '570100200000', 5814, 'USA Technologies Vending');
  CREATE_TANDEM_MERCHANT('265068', 'USA Technologies Vending 28', 'Soda Snack Vending', '936000000500', 5814, 'USA Technologies Vending');
  CREATE_TANDEM_MERCHANT('265070', 'USA Technologies Amusement 01', 'Amusement Vending', '089000000000', 7994, 'USA Technologies Amusement');
  CREATE_TANDEM_MERCHANT('265073', 'USA Tech Laundry-Setomatics 1', 'Laundry Service', '225000000000', 7211, 'USA Tech Laundry-Setomatics');
  COMMIT;
END;
/
