DELETE FROM REPORT.REPORT_PARAM
  WHERE REPORT_ID = 171 
    AND PARAM_NAME = 'for';
    
UPDATE REPORT.REPORTS
   SET TITLE = REPLACE(TITLE, '{e}', '{d}')
 WHERE TITLE LIKE '%{e}%'
   AND BATCH_TYPE_ID = 0
   AND REPORT_ID IN(
      6,
      11,
      134,
      147,
      157,
      168,
      169,
      184,
      188,
      199,
      333,
      337,
      339,
      340);

COMMIT;

UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET DESCRIPTION = 'Firmware 6.07 or later: Time in military format and device local time zone when the ePort will read DEX and send the DEX file to the server. Valid values: 0000 - 2359 or set to FFFF to disable. Examples: 2235 is 10:35 PM, 0915 is 9:15 AM.', CUSTOMER_EDITABLE = 'Y'
WHERE DEVICE_TYPE_ID = 0 AND DEVICE_SETTING_PARAMETER_CD IN ('358');

UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET DESCRIPTION = 'Firmware 6.07 or later: Number of minutes the ePort will repeatedly add to the "DEX Read and Send Time" value to derive the time of the recurring event to read DEX and upload the DEX file to the server.', CUSTOMER_EDITABLE = 'Y'
WHERE DEVICE_TYPE_ID = 0 AND DEVICE_SETTING_PARAMETER_CD IN ('244');

COMMIT;

-- insert missing device.gprs_device records and fix device.gprs_device record discrepancies
DECLARE
	CURSOR L_CUR IS
	SELECT dbadmin.to_number_or_null(h.host_serial_cd) ICCID, H.LAST_UPDATED_TS, H.DEVICE_ID, HE.HOST_EQUIPMENT_MFGR, HS_FW.HOST_SETTING_VALUE FIRMWARE_VERSION,
			H.HOST_LABEL_CD, HS_RSSI.HOST_SETTING_VALUE RSSI, HS_RSSI.LAST_UPDATED_TS RSSI_TS	
	FROM DEVICE.HOST H
	LEFT OUTER JOIN DEVICE.HOST_EQUIPMENT HE ON H.HOST_EQUIPMENT_ID = HE.HOST_EQUIPMENT_ID
	LEFT OUTER JOIN DEVICE.HOST_SETTING HS_FW ON HS_FW.HOST_ID = H.HOST_ID AND HS_FW.HOST_SETTING_PARAMETER = 'Firmware Version'
	LEFT OUTER JOIN DEVICE.HOST_SETTING HS_RSSI ON HS_RSSI.HOST_ID = H.HOST_ID AND HS_RSSI.HOST_SETTING_PARAMETER = 'CSQ'
	WHERE H.HOST_ID IN (
	select distinct first_value(h.host_id) over (partition by h.host_serial_cd order by h.last_updated_ts desc) host_id
	from device.host h
	join device.device d on h.device_id = d.device_id
	where h.host_id in (	
	select h.host_id
	from device.host h
	join device.device d on h.device_id = d.device_id
	join device.device_last_active dla on d.device_serial_cd = dla.device_serial_cd and d.device_id = dla.device_id
	join device.host_type ht on h.host_type_id = ht.host_type_id
	left outer join device.gprs_device gd on gd.device_id = d.device_id and gd.iccid = dbadmin.to_number_or_null(h.host_serial_cd) and gd.gprs_device_state_id = 5
	left outer join device.gprs_device gd2 on gd2.iccid = dbadmin.to_number_or_null(h.host_serial_cd) and gd2.gprs_device_state_id = 5 and gd2.device_id != d.device_id
	and gd2.assigned_by = 'APP_LAYER'
	where (ht.host_type_desc like 'GPRS%' or ht.host_type_desc like 'HSPA%') and regexp_like(h.host_serial_cd, '^[0-9]{19,22}$') 
	and h.host_active_yn_flag = 'Y' and d.last_activity_ts is not null and gd.gprs_device_id is null and (gd2.gprs_device_id is null or h.last_updated_ts > gd2.assigned_ts)
	))
	order by h.last_updated_ts desc;
BEGIN
	FOR L_REC IN L_CUR LOOP
		UPDATE DEVICE.GPRS_DEVICE
		SET DEVICE_ID = NULL,
			GPRS_DEVICE_STATE_ID = DECODE(GPRS_DEVICE_STATE_ID, 5, 4, GPRS_DEVICE_STATE_ID)
		WHERE DEVICE_ID = L_REC.DEVICE_ID
			AND ICCID != L_REC.ICCID;
			
		UPDATE DEVICE.GPRS_DEVICE
		   SET DEVICE_ID = L_REC.DEVICE_ID, DEVICE_TYPE_NAME = L_REC.HOST_EQUIPMENT_MFGR, DEVICE_FIRMWARE_NAME = L_REC.FIRMWARE_VERSION, IMEI = L_REC.HOST_LABEL_CD, 
		       GPRS_DEVICE_STATE_ID = 5, 
		       ASSIGNED_BY = 'APP_LAYER', 
		   	   ASSIGNED_TS = L_REC.LAST_UPDATED_TS
		 WHERE ICCID = L_REC.ICCID;
		 
		 IF SQL%NOTFOUND THEN
		BEGIN
		 		INSERT INTO DEVICE.GPRS_DEVICE(DEVICE_ID, DEVICE_TYPE_NAME, DEVICE_FIRMWARE_NAME, IMEI, GPRS_DEVICE_STATE_ID, ASSIGNED_BY, ASSIGNED_TS, ICCID)
		 		VALUES(L_REC.DEVICE_ID, L_REC.HOST_EQUIPMENT_MFGR, L_REC.FIRMWARE_VERSION, L_REC.HOST_LABEL_CD, 5, 'APP_LAYER', L_REC.LAST_UPDATED_TS, L_REC.ICCID);
		EXCEPTION
			WHEN DUP_VAL_ON_INDEX THEN
				NULL;
		END;
		 END IF;
	
		COMMIT;
	END LOOP;	
END;
/

-- GPRS - AT&T
DECLARE 
	CURSOR L_CUR IS
	select d.device_id, h.last_updated_ts
	from device.device d
	join device.device_last_active dla on d.device_serial_cd = dla.device_serial_cd and d.device_id = dla.device_id
	join device.host h on d.device_id = h.device_id
	join device.host_type ht on h.host_type_id = ht.host_type_id
	where h.host_active_yn_flag = 'Y' and ht.comm_method_cd is not null and regexp_like(h.host_serial_cd, '^(8901|8931)[0-9]{15,18}$') 
	and d.comm_method_cd != 'G'
	order by d.last_activity_ts desc;
BEGIN
	FOR L_REC IN L_CUR LOOP
		UPDATE DEVICE.DEVICE 
		SET COMM_METHOD_CD = 'G', COMM_METHOD_TS = L_REC.LAST_UPDATED_TS
		WHERE DEVICE_ID = L_REC.DEVICE_ID;
		COMMIT;
	END LOOP;
END;
/

-- HSPA Eseye is on G9 only
DECLARE 
	CURSOR L_CUR IS
	select d.device_id, h.last_updated_ts
	from device.device d
	join device.device_last_active dla on d.device_serial_cd = dla.device_serial_cd and d.device_id = dla.device_id
	join device.host h on d.device_id = h.device_id
	join device.host_type ht on h.host_type_id = ht.host_type_id
	where h.host_active_yn_flag = 'Y' and ht.comm_method_cd is not null and regexp_like(h.host_serial_cd, '^8944[0-9]{15,18}$') and d.device_serial_cd like 'VJ%'
	and d.comm_method_cd != 'Y'
	order by d.last_activity_ts desc;
BEGIN
	FOR L_REC IN L_CUR LOOP
		UPDATE DEVICE.DEVICE 
		SET COMM_METHOD_CD = 'Y', COMM_METHOD_TS = L_REC.LAST_UPDATED_TS
		WHERE DEVICE_ID = L_REC.DEVICE_ID;
		COMMIT;
	END LOOP;
END;
/

-- GPRS Eseye is used by non-G9 devices
DECLARE 
	CURSOR L_CUR IS
	select d.device_id, h.last_updated_ts
	from device.device d
	join device.device_last_active dla on d.device_serial_cd = dla.device_serial_cd and d.device_id = dla.device_id
	join device.host h on d.device_id = h.device_id
	join device.host_type ht on h.host_type_id = ht.host_type_id
	where h.host_active_yn_flag = 'Y' and ht.comm_method_cd is not null and regexp_like(h.host_serial_cd, '^8944[0-9]{15,18}$') and d.device_serial_cd not like 'VJ%'
	and d.comm_method_cd != 'S'
	order by d.last_activity_ts desc;
BEGIN
	FOR L_REC IN L_CUR LOOP
		UPDATE DEVICE.DEVICE 
		SET COMM_METHOD_CD = 'S', COMM_METHOD_TS = L_REC.LAST_UPDATED_TS
		WHERE DEVICE_ID = L_REC.DEVICE_ID;
		COMMIT;
	END LOOP;
END;
/

-- GPRS - Rogers
DECLARE 
	CURSOR L_CUR IS
	select d.device_id, h.last_updated_ts
	from device.device d
	join device.device_last_active dla on d.device_serial_cd = dla.device_serial_cd and d.device_id = dla.device_id
	join device.host h on d.device_id = h.device_id
	join device.host_type ht on h.host_type_id = ht.host_type_id
	where h.host_active_yn_flag = 'Y' and ht.comm_method_cd is not null and regexp_like(h.host_serial_cd, '^8930[0-9]{15,18}$')
	and d.comm_method_cd != 'R'
	order by d.last_activity_ts desc;
BEGIN
	FOR L_REC IN L_CUR LOOP
		UPDATE DEVICE.DEVICE 
		SET COMM_METHOD_CD = 'R', COMM_METHOD_TS = L_REC.LAST_UPDATED_TS
		WHERE DEVICE_ID = L_REC.DEVICE_ID;
		COMMIT;
	END LOOP;
END;
/
