DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE;
	LV_REPORT_NAME VARCHAR(50) := 'ATT 2G Devices';
BEGIN
	SELECT MAX(REPORT_ID) INTO LN_REPORT_ID
	FROM REPORT.REPORTS WHERE REPORT_NAME = LV_REPORT_NAME;
	
	IF LN_REPORT_ID > 0 THEN
		RETURN;
	ELSE
		LN_REPORT_ID:=435;
	END IF;


	INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
	SELECT LN_REPORT_ID, 'ATT 2G Devices', 6, 0, 'ATT 2G Devices', 'ATT 2G Devices report', 'U', 0
	FROM DUAL;

	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
	VALUES(LN_REPORT_ID, 'query', 'select distinct D.DEVICE_SERIAL_CD "Device Serial Number" ,
		T.region_name "Region Name", 
		T.LOCATION_NAME "Location", 
    T.ASSET_NBR "Asset Nbr",
    CASE WHEN SF.FEE_ID = 6 THEN ''Y'' ELSE CASE WHEN rd.DEVICE_ID is not null THEN ''Y'' ELSE ''N'' END END "Is Rental"
		FROM REPORT.VW_TERMINAL T
				  JOIN REPORT.VW_TERMINAL_EPORT TE ON T.TERMINAL_ID = TE.TERMINAL_ID
		         JOIN REPORT.EPORT E ON TE.EPORT_ID = E.EPORT_ID
		         JOIN DEVICE.DEVICE D ON E.EPORT_SERIAL_NUM = D.DEVICE_SERIAL_CD AND D.DEVICE_ACTIVE_YN_FLAG = ''Y'' AND D.COMM_METHOD_CD=''G''
		      JOIN REPORT.VW_USER_TERMINAL UT ON T.TERMINAL_ID = UT.TERMINAL_ID AND UT.USER_ID = ?
          left outer JOIN corp.service_fees sf on T.TERMINAL_ID=SF.TERMINAL_ID and SF.FEE_ID=6 and NVL(sf.end_date, MAX_DATE)>sysdate
          left outer join (select distinct D.DEVICE_SERIAL_CD,
		D.device_id,
		T.terminal_id, 
		T.region_name, 
		T.LOCATION_NAME, T.ASSET_NBR,
		''Y'' from REPORT.VW_TERMINAL T
		JOIN REPORT.VW_TERMINAL_EPORT TE ON T.TERMINAL_ID = TE.TERMINAL_ID
		JOIN REPORT.EPORT E ON TE.EPORT_ID = E.EPORT_ID
		JOIN DEVICE.DEVICE D ON E.EPORT_SERIAL_NUM = D.DEVICE_SERIAL_CD AND D.DEVICE_ACTIVE_YN_FLAG = ''Y''
		join corp.process_fees pf on pf.terminal_id=t.terminal_id
		left outer JOIN corp.service_fees sf on pf.TERMINAL_ID=SF.TERMINAL_ID and SF.FEE_ID=6 and NVL(sf.end_date, MAX_DATE)>sysdate
		JOIN REPORT.VW_USER_TERMINAL UT ON T.TERMINAL_ID = UT.TERMINAL_ID AND UT.USER_ID = ?
		where pf.trans_type_id=16 and pf.fee_percent >=0.1
		and sf.fee_id is null) rd on d.device_id=rd.device_id
        WHERE D.DEVICE_SERIAL_CD LIKE ''EE1000%'' OR D.DEVICE_SERIAL_CD LIKE ''G81%'' or D.DEVICE_SERIAL_CD LIKE ''G82%''
		ORDER BY D.DEVICE_SERIAL_CD
');
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'header', 'true');
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'isSQLFolio', 'true');
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'paramNames', 'userId,userId');
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'paramTypes', 'NUMERIC,NUMERIC');
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'params.userId', '{u}');
		
	insert into web_content.web_link(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER) values(web_content.seq_web_link_id.nextval, 'ATT 2G Devices', './run_sql_folio_report_async.i?basicReportId=' || LN_REPORT_ID, 'ATT 2G Devices report', 'Daily Operations', 'W', 0);
	insert into web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) select web_link_id, 1 from web_content.web_link where web_link_label = 'ATT 2G Devices';
	insert into web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) select web_link_id, 45 from web_content.web_link where web_link_label = 'ATT 2G Devices';
	
	COMMIT;
END;
/