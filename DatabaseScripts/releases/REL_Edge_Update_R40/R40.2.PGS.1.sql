CREATE OR REPLACE FUNCTION MAIN.COPY_INDEXES(pv_source_table VARCHAR, pv_suffix VARCHAR)
    RETURNS SMALLINT
    SECURITY DEFINER
AS $$
DECLARE
    lv_sql TEXT;
    ln_cnt SMALLINT := 0;
BEGIN
    FOR lv_sql IN SELECT 'CREATE '||CASE WHEN x.indisunique THEN 'UNIQUE ' ELSE '' END ||'INDEX ' || MAIN.CONSTRUCT_NAME(CAST(i.relname AS VARCHAR), pv_suffix) || ' ON '
            || CASE WHEN n.nspname IS NOT NULL AND n.nspname != 'pg_catalog' THEN n.nspname || '.' ELSE '' END || MAIN.CONSTRUCT_NAME(CAST(c.relname AS VARCHAR), pv_suffix) || CASE WHEN a.amname IS NOT NULL AND a.amname != 'btree' THEN ' USING ' || a.amname || ' ' ELSE '' END || '('
            || (SELECT ARRAY_TO_STRING(ARRAY_AGG(t.col_desc), cast(', ' as text))
                  FROM (SELECT CASE WHEN b.attname IS NOT NULL THEN b.attname || CASE WHEN x.indoption[s.s-1] & 3 = 3 THEN ' DESC'  ELSE '' END WHEN x.indexprs IS NOT NULL THEN PG_GET_EXPR(x.indexprs, x.indrelid) ELSE '' END 
            || CASE WHEN o.collname IS NOT NULL AND o.collname != 'default' THEN ' COLLATE ' || o.collname ELSE '' END
            || CASE WHEN p.opcname IS NOT NULL AND y.typname || '_ops' != p.opcname THEN ' ' || p.opcname ELSE '' END col_desc
            FROM (
            SELECT generate_series(1,x.indnatts) s) s
            LEFT JOIN pg_attribute b on x.indrelid = b.attrelid and x.indkey[s.s-1] = b.attnum
            LEFT JOIN pg_type y on x.indclass[s.s-1] = y.oid
            LEFT JOIN pg_collation o on x.indcollation[s.s-1] = o.oid
            LEFT JOIN pg_opclass p on x.indclass[s.s-1] = p.oid AND NOT(p.opcintype = b.atttypid AND p.opcdefault)
            ORDER BY s.s) t)
            ||')' || CASE WHEN t.spcname IS NOT NULL THEN ' TABLESPACE ' || t.spcname ELSE '' END
            || CASE WHEN x.indpred IS NOT NULL THEN ' WHERE ' || x.indpred ELSE '' END
          FROM pg_index x 
          JOIN pg_class i ON i.oid = x.indexrelid 
          JOIN pg_class c ON c.oid = x.indrelid 
          LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
          LEFT JOIN pg_tablespace t ON t.oid = i.reltablespace
          LEFT JOIN pg_am a ON i.relam = a.oid
          LEFT JOIN (pg_index ox JOIN pg_class oi ON oi.oid = ox.indexrelid JOIN pg_class oc ON oc.oid = ox.indrelid) ON MAIN.CONSTRUCT_NAME(CAST(i.relname AS VARCHAR), pv_suffix) = oi.relname AND oc.relname = MAIN.CONSTRUCT_NAME(CAST(c.relname AS VARCHAR), pv_suffix)
        WHERE c.relkind = 'r'
          AND i.relkind = 'i'
          AND c.relname = LOWER(pv_source_table)
          AND n.nspname = 'main' 
          AND NOT x.indisprimary
          AND oi.oid IS NULL
    LOOP
        EXECUTE lv_sql;
        ln_cnt := ln_cnt + 1;
    END LOOP;
   RETURN ln_cnt;     
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MAIN.COPY_INDEXES_TO_CHILDREN(pv_source_table VARCHAR)
    RETURNS SMALLINT
    SECURITY DEFINER
AS $$
DECLARE
    ln_cnt SMALLINT := 0;
BEGIN
	SELECT SUM(MAIN.COPY_INDEXES(pv_source_table, 
		   CASE 
			   WHEN LOWER(LEFT(c.relname, LENGTH(p.relname) + 1)) = LOWER(p.relname) || '_' THEN SUBSTR(c.relname, LENGTH(p.relname) + 2)
			   WHEN c.relname ~ '.+$\d{8}_.+' THEN (REGEXP_MATCHES(c.relname, '.+$\d{8}_(.+)'))[0]
		   END))
	  INTO ln_cnt
	  FROM pg_class p
	  JOIN pg_inherits h ON p.oid = h.inhparent
	  JOIN pg_class c ON h.inhrelid = c.oid
	  LEFT JOIN pg_namespace n ON n.oid = c.relnamespace         	  
	 WHERE p.relkind = 'r'
       AND p.relname = LOWER(pv_source_table)
       AND n.nspname = 'main';  	   
    RETURN ln_cnt;     
END;
$$ LANGUAGE plpgsql;

CREATE INDEX ix_device_session_device_name_call_in_start_ts
  ON main._device_session(device_name, call_in_start_ts);

SELECT MAIN.COPY_INDEXES_TO_CHILDREN('_device_session');
