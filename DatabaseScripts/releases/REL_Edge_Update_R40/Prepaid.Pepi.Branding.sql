INSERT INTO WEB_CONTENT.SUBDOMAIN(SUBDOMAIN_URL, SUBDOMAIN_DESCRIPTION)
  SELECT SUBDOMAIN_URL || '/pepi', 'Pepi ' || SUBDOMAIN_DESCRIPTION
  FROM WEB_CONTENT.SUBDOMAIN 
  WHERE SUBDOMAIN_DESCRIPTION LIKE 'GetMore%'
    AND NOT EXISTS(SELECT 1 FROM WEB_CONTENT.SUBDOMAIN 
  WHERE SUBDOMAIN_DESCRIPTION LIKE 'Pepi GetMore%');
  
INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, LITERAL_VALUE, SUBDOMAIN_ID)
    SELECT L.LITERAL_KEY, L.LITERAL_VALUE, S.SUBDOMAIN_ID
      FROM (
    SELECT '' LITERAL_KEY, '' LITERAL_VALUE FROM DUAL WHERE 0=1
    UNION ALL SELECT 'prepaid-title', 'The Pepi Companies -' FROM DUAL
    UNION ALL SELECT 'prepaid-footer-image-url', '/images/logo-pepi-footer200x80.png' FROM DUAL
    UNION ALL SELECT 'prepaid-footer-image-title', 'the pepi companies' FROM DUAL
    UNION ALL SELECT 'prepaid-top-image-url', '/images/icon-logo_pepi-white.png' FROM DUAL
    UNION ALL SELECT 'prepaid-top-image-welcome-url', '/images/icon-logo_pepi.png' FROM DUAL
    UNION ALL SELECT 'prepaid-home-image-url', '/images/home-logo_simplymore-simplybetter.png' FROM DUAL
    UNION ALL SELECT 'prepaid-home-image-title', 'Simply more. Simply better.' FROM DUAL
    UNION ALL SELECT 'prepaid-cash-icon-url', '/images/icon-cash60x50_pepi.png' FROM DUAL
    UNION ALL SELECT 'prepaid-sale-icon-url', '/images/icon-sale60x50_pepi.png' FROM DUAL
    UNION ALL SELECT 'prepaid-new-icon-url', '/images/icon-new60x50_pepi.png' FROM DUAL
    UNION ALL SELECT 'prepaid-new-user-image', '/images/logo-pepi-signup150x60.png' FROM DUAL
    UNION ALL SELECT 'prepaid-new-user-image-title', 'the pepi companies' FROM DUAL
    UNION ALL SELECT 'prepaid-stylesheets', '/css/normalize.css;/css/main.css;/css/pepi.css' FROM DUAL) L
    CROSS JOIN WEB_CONTENT.SUBDOMAIN S
 WHERE S.SUBDOMAIN_URL like '%/pepi'
   AND NOT EXISTS(SELECT 1 FROM  WEB_CONTENT.LITERAL L0 WHERE L0.LITERAL_KEY = L.LITERAL_KEY AND L0.SUBDOMAIN_ID = S.SUBDOMAIN_ID AND L0.LOCALE_CD IS NULL);

COMMIT;
 