delete from folio_conf.join_filter where join_filter_id=62;

insert into folio_conf.join_filter (join_filter_id, from_table,to_table, join_expression, join_type, active_flag, join_cardinality) 
values(62, 'REPORT.PURCHASE', 'PSS.TRAN_LINE_ITEM_TYPE', 'REPORT.PURCHASE.TRAN_LINE_ITEM_TYPE_ID=PSS.TRAN_LINE_ITEM_TYPE.TRAN_LINE_ITEM_TYPE_ID', 'INNER', 'Y', '=');

insert into folio_conf.join_filter (join_filter_id, from_table,to_table, join_expression, join_type, active_flag, join_cardinality) 
values(63,'REPORT.PURCHASE', null, '(PSS.TRAN_LINE_ITEM_TYPE.TRAN_LINE_ITEM_TYPE_GROUP_CD<>''U'' OR PSS.TRAN_LINE_ITEM_TYPE.TRAN_LINE_ITEM_TYPE_GROUP_CD IS NULL)', 'INNER', 'Y', '=');

commit;

--folio 244, 1021, 821