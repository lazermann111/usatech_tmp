GRANT SELECT ON CORP.CUSTOMER_ADDR TO DEVICE;
GRANT EXECUTE ON CORP.NEXT_PAY_DAY TO USALIVE_APP_ROLE;
GRANT SELECT ON CORP.BATCH_TOTAL TO USALIVE_APP_ROLE;
GRANT SELECT ON CORP.CUSTOMER TO USAT_PREPAID_APP_ROLE;
GRANT SELECT ON PSS.TRAN_LINE_ITEM_TYPE TO REPORT;
GRANT SELECT, REFERENCES ON PSS.CONSUMER_ACCT_BASE TO CORP;
GRANT SELECT, REFERENCES ON DEVICE.DEVICE TO CORP;
GRANT SELECT, REFERENCES ON REPORT.TIME_ZONE TO CORP;
GRANT EXECUTE ON REPORT.GET_OR_CREATE_EPORT TO CORP;
GRANT EXECUTE ON REPORT.PKG_CUSTOMER_MANAGEMENT TO CORP;
GRANT EXECUTE ON PKG_DEVICE_CONFIGURATION TO CORP;
GRANT SELECT ON PSS.CONSUMER_ACCT_BASE TO USALIVE_APP_ROLE;
GRANT SELECT ON DEVICE.DEVICE_LAST_ACTIVE TO USALIVE_APP_ROLE;
GRANT SELECT ON DEVICE.DEVICE_SUB_TYPE TO USALIVE_APP_ROLE;
GRANT SELECT ON CORP.CUSTOMER TO USAT_POSM_ROLE;

INSERT INTO PSS.AUTH_RESULT(AUTH_RESULT_CD, AUTH_RESULT_DESC)
    SELECT * FROM(
    SELECT 'C' AUTH_RESULT_CD, 'CVV Mis-Match' AUTH_RESULT_DESC FROM DUAL
    UNION ALL
    SELECT 'V', 'AVS Mis-Match' FROM DUAL
    UNION ALL
    SELECT 'M', 'CVV and AVS Mis-Match' FROM DUAL) N
    WHERE NOT EXISTS(SELECT 1 FROM PSS.AUTH_RESULT WHERE AUTH_RESULT_CD = N.AUTH_RESULT_CD);

COMMIT;

INSERT INTO PSS.POS_PTA_TMPL(POS_PTA_TMPL_NAME, POS_PTA_TMPL_DESC)
SELECT 'DEFAULT: Back Office Virtual Initialization - US', 'Default template for Back Office Virtual in US during initialization' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'DEFAULT: Back Office Virtual Initialization - US');

INSERT INTO PSS.POS_PTA_TMPL_ENTRY(POS_PTA_TMPL_ID, PAYMENT_SUBTYPE_ID, POS_PTA_ACTIVATION_OSET_HR, PAYMENT_SUBTYPE_KEY_ID, POS_PTA_PRIORITY, CURRENCY_CD, AUTHORITY_PAYMENT_MASK_ID)
SELECT TM.POS_PTA_TMPL_ID, PST.PAYMENT_SUBTYPE_ID, 0, T.TERMINAL_ID, PST.PRIORITY, 'USD', PST.AUTHORITY_PAYMENT_MASK_ID
  FROM PSS.POS_PTA_TMPL TM
 CROSS JOIN V$DATABASE D
 CROSS JOIN (
     SELECT PST.*, ROWNUM PRIORITY 
       FROM (SELECT * FROM PSS.PAYMENT_SUBTYPE
      WHERE CLIENT_PAYMENT_TYPE_CD IN('Q')
        AND PAYMENT_SUBTYPE_CLASS IN('Authority::ISO8583::Elavon', 'Authority::NOP')  --XXX: Can you use a MORE card?
      ORDER BY DECODE(SUBSTR(PAYMENT_SUBTYPE_NAME, 1, 4), 'Visa', 1, 'Mast', 2, 'Amer', 3, 'Disc', 4, 5), PAYMENT_SUBTYPE_ID) PST) PST
  JOIN (PSS.TERMINAL T
  JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
  JOIN AUTHORITY.AUTHORITY A ON M.AUTHORITY_ID = A.AUTHORITY_ID) 
    ON T.STATUS_CD = 'A'
   AND M.STATUS_CD = 'A'
   AND T.TERMINAL_CD = DECODE(PST.PAYMENT_SUBTYPE_CLASS,
        'Authority::ISO8583::Elavon', CASE 
            WHEN D.NAME LIKE 'USADEV%' THEN '9998012015809000' 
            WHEN D.NAME LIKE 'ECC%' THEN '9998014592748000' 
            ELSE '999----------000' END, 
        'Authority::NOP', '0') -- adjust for environment
   AND M.MERCHANT_CD = DECODE(PST.PAYMENT_SUBTYPE_CLASS,
        'Authority::ISO8583::Elavon', '1060', 
        'Authority::NOP', 'Error Bin') -- adjust for environment 
   AND A.AUTHORITY_NAME = DECODE(PST.PAYMENT_SUBTYPE_CLASS, 'Authority::ISO8583::Elavon', 'Elavon ISO8583', 'Authority::NOP', 'Error Bin') 
 WHERE TM.POS_PTA_TMPL_NAME = 'DEFAULT: Back Office Virtual Initialization - US'
   AND NOT EXISTS(SELECT 1 FROM PSS.POS_PTA_TMPL_ENTRY E WHERE E.POS_PTA_TMPL_ID = TM.POS_PTA_TMPL_ID AND E.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID);

INSERT INTO PSS.POS_PTA_TMPL(POS_PTA_TMPL_NAME, POS_PTA_TMPL_DESC)
SELECT 'DEFAULT: Back Office Virtual Initialization - Canada', 'Default template for Back Office Virtual in Canada during initialization' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'DEFAULT: Back Office Virtual Initialization - Canada');

INSERT INTO PSS.POS_PTA_TMPL_ENTRY(POS_PTA_TMPL_ID, PAYMENT_SUBTYPE_ID, POS_PTA_ACTIVATION_OSET_HR, PAYMENT_SUBTYPE_KEY_ID, POS_PTA_PRIORITY, CURRENCY_CD, AUTHORITY_PAYMENT_MASK_ID)
SELECT TM.POS_PTA_TMPL_ID, PST.PAYMENT_SUBTYPE_ID, 0, T.TERMINAL_ID, PST.PRIORITY, 'CAD', PST.AUTHORITY_PAYMENT_MASK_ID
  FROM PSS.POS_PTA_TMPL TM
 CROSS JOIN V$DATABASE D
 CROSS JOIN (
     SELECT PST.*, ROWNUM PRIORITY 
       FROM (SELECT * FROM PSS.PAYMENT_SUBTYPE
      WHERE CLIENT_PAYMENT_TYPE_CD IN('Q')
        AND PAYMENT_SUBTYPE_CLASS IN('Authority::ISO8583::FHMS::Paymentech', 'Authority::NOP')  --XXX: Can you use a MORE card?
      ORDER BY DECODE(SUBSTR(PAYMENT_SUBTYPE_NAME, 1, 4), 'Visa', 1, 'Mast', 2, 'Amer', 3, 'Disc', 4, 5), PAYMENT_SUBTYPE_ID) PST) PST
  JOIN (PSS.TERMINAL T
  JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
  JOIN AUTHORITY.AUTHORITY A ON M.AUTHORITY_ID = A.AUTHORITY_ID) 
    ON T.STATUS_CD = 'A'
   AND M.STATUS_CD = 'A'
   AND M.MERCHANT_CD = DECODE(PST.PAYMENT_SUBTYPE_CLASS,
        'Authority::ISO8583::FHMS::Paymentech', CASE 
            WHEN D.NAME LIKE 'USADEV%' THEN '700000008124' 
            WHEN D.NAME LIKE 'ECC%' THEN '700000008124' 
            ELSE '12400000----' END, 
        'Authority::NOP', '0') -- adjust for environment
   AND A.AUTHORITY_NAME = DECODE(PST.PAYMENT_SUBTYPE_CLASS, 'Authority::ISO8583::FHMS::Paymentech', 'Paymentech', 'Authority::NOP', 'Error Bin') 
 WHERE TM.POS_PTA_TMPL_NAME = 'DEFAULT: Back Office Virtual Initialization - Canada'
   AND NOT EXISTS(SELECT 1 FROM PSS.POS_PTA_TMPL_ENTRY E WHERE E.POS_PTA_TMPL_ID = TM.POS_PTA_TMPL_ID AND E.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID);

UPDATE DEVICE.DEVICE_TYPE
   SET DEVICE_TYPE_SERIAL_CD_REGEX = '^V[1-2]-.*$'
 WHERE DEVICE_TYPE_ID = 14
   AND DEVICE_TYPE_SERIAL_CD_REGEX = '^V1-.*$';

INSERT INTO DEVICE.DEVICE_SUB_TYPE(DEVICE_TYPE_ID,DEVICE_SUB_TYPE_ID,DEVICE_SUB_TYPE_DESC,DEVICE_SERIAL_CD_REGEX,POS_PTA_TMPL_ID,DEF_BASE_HOST_TYPE_ID,DEF_BASE_HOST_EQUIPMENT_ID,MASTER_ID_ALWAYS_INC,REQUIRE_PREREGISTER) 
    SELECT 14, 3,'Back Office Virtual - USD','^V2-[0-9][0-9]*-USD$', T.POS_PTA_TMPL_ID, 1,HE.HOST_EQUIPMENT_ID,'N','Y' 
      FROM PSS.POS_PTA_TMPL T, DEVICE.HOST_EQUIPMENT HE
     WHERE T.POS_PTA_TMPL_NAME = 'DEFAULT: Back Office Virtual Initialization - US'
       AND HE.HOST_EQUIPMENT_MFGR = 'Unknown' AND HE.HOST_EQUIPMENT_MODEL = 'Unknown'
       AND NOT EXISTS(SELECT 1 FROM DEVICE.DEVICE_SUB_TYPE WHERE DEVICE_TYPE_ID = 14 AND DEVICE_SUB_TYPE_ID = 3)
    UNION ALL
    SELECT 14, 4,'Back Office Virtual - CAD','^V2-[0-9][0-9]*-CAD$', T.POS_PTA_TMPL_ID, 1,HE.HOST_EQUIPMENT_ID,'N','Y' 
      FROM PSS.POS_PTA_TMPL T, DEVICE.HOST_EQUIPMENT HE
     WHERE POS_PTA_TMPL_NAME = 'DEFAULT: Back Office Virtual Initialization - Canada'
       AND HE.HOST_EQUIPMENT_MFGR = 'Unknown' AND HE.HOST_EQUIPMENT_MODEL = 'Unknown'
       AND NOT EXISTS(SELECT 1 FROM DEVICE.DEVICE_SUB_TYPE WHERE DEVICE_TYPE_ID = 14 AND DEVICE_SUB_TYPE_ID = 4);

COMMIT;

CREATE SEQUENCE CORP.SEQ_PAYOR_ID;

CREATE TABLE CORP.PAYOR(
    PAYOR_ID NUMBER(20) NOT NULL,
    PAYOR_NAME VARCHAR2(50) NOT NULL,
    PAYOR_EMAIL VARCHAR2(60),
    PAY_TO_DEVICE_ID NUMBER(20) NOT NULL,
    GLOBAL_ACCOUNT_ID NUMBER(19) NOT NULL,
    TRUNCATED_CARD_NUMBER VARCHAR2(50) NOT NULL,
    TOKEN_CD RAW(32) NOT NULL,
    LAST_CHARGE_AMOUNT NUMBER(14,4),
    LAST_CHARGE_TS DATE,
    MAX_CHARGE_AMOUNT NUMBER(14,4),
    MIN_INTERVAL_MONTHS NUMBER(4),
    MIN_INTERVAL_DAYS NUMBER(4),  
    CREATED_BY_ID NUMBER,
    CREATED_BY VARCHAR2(30) NOT NULL,
    CREATED_TS DATE NOT NULL,
    LAST_UPDATED_BY VARCHAR2(30) NOT NULL,
    LAST_UPDATED_TS DATE NOT NULL,
    CONSTRAINT PK_PAYOR PRIMARY KEY(PAYOR_ID),
    CONSTRAINT FK_PAYOR_DEVICE_ID FOREIGN KEY (PAY_TO_DEVICE_ID) REFERENCES DEVICE.DEVICE(DEVICE_ID),
    CONSTRAINT FK_PAYOR_CREATED_BY_ID FOREIGN KEY (CREATED_BY_ID) REFERENCES REPORT.USER_LOGIN(USER_ID)
) TABLESPACE CORP_DATA;

CREATE OR REPLACE TRIGGER CORP.TRBI_PAYOR BEFORE INSERT ON CORP.PAYOR
  FOR EACH ROW
BEGIN
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER CORP.TRBU_PAYOR BEFORE UPDATE ON CORP.PAYOR
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

CREATE UNIQUE INDEX CORP.IX_PAYOR_COMBO_1 ON CORP.PAYOR(GLOBAL_ACCOUNT_ID, PAY_TO_DEVICE_ID) TABLESPACE CORP_INDX;

CREATE INDEX CORP.IX_PAYOR_PAY_TO_DEVICE_ID ON CORP.PAYOR(PAY_TO_DEVICE_ID) TABLESPACE CORP_INDX;

GRANT SELECT ON CORP.PAYOR TO USALIVE_APP_ROLE, USAT_DEV_READ_ONLY;

GRANT SELECT ON CORP.SEQ_PAYOR_ID TO USALIVE_APP_ROLE;
GRANT INSERT, UPDATE, DELETE ON CORP.PAYOR TO USALIVE_APP_ROLE;

GRANT SELECT, UPDATE ON CORP.PAYOR TO PSS;

INSERT INTO ENGINE.APP_SETTING(APP_SETTING_CD, APP_SETTING_VALUE, APP_SETTING_DESC)
SELECT 'STANDARD_CREDIT_PROCESS_FEE_PERCENT', 0.0545, 'Standard credit process fee' FROM DUAL WHERE NOT EXISTS(
SELECT 1 FROM ENGINE.APP_SETTING WHERE APP_SETTING_CD = 'STANDARD_CREDIT_PROCESS_FEE_PERCENT'
);
COMMIT;

BEGIN
	INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
	SELECT REPORT.REPORTS_SEQ.NEXTVAL, 'High Process Fee Transaction Summary With Device - {b} to {e}', 1, 0, 'High Process Fee Transaction Summary With Device', 'Transaction summary with device for higher than standard process fees.', 'N', user_id
	FROM report.reports WHERE report_name = 'Transaction Processing Entry Summary' 
	AND NOT EXISTS (SELECT 1 FROM REPORT.REPORTS WHERE REPORT_NAME = 'High Process Fee Transaction Summary With Device');

	IF SQL%FOUND THEN
		INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
		VALUES( REPORT.REPORTS_SEQ.CURRVAL, 'query', 'SELECT 
				led.entry_type,
				bu.business_unit_name,
				DECODE(t.payment_schedule_id, 4, ''Y'', ''N'') monthly_payment,
				cr.currency_code,
				c.customer_name,
				TO_CHAR(TRUNC(tr.settle_date, ''MONTH''), ''mm/dd/yyyy'') month_for,
				e.eport_serial_num,
				pf.fee_percent * 100 fee_percent,
				COUNT(1) tran_count,
				SUM(tr.total_amount) total_tran_amount,
				SUM(led.amount) total_ledger_amount
				FROM report.trans tr
				JOIN corp.currency cr
				ON cr.currency_id = tr.currency_id
				JOIN report.trans_state ts
				ON ts.state_id = tr.settle_state_id 
				JOIN report.terminal t ON tr.terminal_id = t.terminal_id
				JOIN corp.customer c ON c.customer_id = t.customer_id
				JOIN corp.business_unit bu ON bu.business_unit_id = t.business_unit_id
				JOIN corp.ledger led
				ON led.trans_id = tr.tran_id
				AND led.deleted = ''N''
				JOIN report.eport e on tr.eport_id = e.eport_id
				JOIN corp.process_fees pf ON t.terminal_id = pf.terminal_id
				WHERE tr.trans_type_id IN (14, 16, 19, 20, 21) 
				AND tr.settle_state_id IN (2, 3) 
				AND led.entry_type IN (''CC'', ''PF'', ''CB'', ''RF'') 
				AND tr.settle_date >= CAST(? AS DATE) AND tr.settle_date < CAST(? AS DATE) + 1
				AND pf.trans_type_id = 16 AND (pf.start_date IS NULL OR pf.start_date < SYSDATE) AND (pf.end_date IS NULL OR pf.end_date > SYSDATE)
				AND pf.fee_percent > TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING(''STANDARD_CREDIT_PROCESS_FEE_PERCENT''))
				GROUP BY
				led.entry_type,
				bu.business_unit_name,
				DECODE(t.payment_schedule_id, 4, ''Y'', ''N''),
				cr.currency_code,
				c.customer_name,
				TRUNC(tr.settle_date, ''MONTH''),
				e.eport_serial_num,
				pf.fee_percent
				ORDER BY c.customer_name, e.eport_serial_num, led.entry_type');
		INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
		VALUES( REPORT.REPORTS_SEQ.CURRVAL, 'header', 'true');
		insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'dateFormat','MM/dd/yyyy');
		insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'params.StartDate','{b}');
		insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'params.EndDate','{e}');
		insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'paramNames','StartDate,EndDate');
		insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'paramTypes','TIMESTAMP,TIMESTAMP');
			
		insert into web_content.web_link values(web_content.seq_web_link_id.nextval, 'High Process Fee Transaction Summary With Device', './select_date_range_frame_nonfolio.i?basicReportId='||REPORT.REPORTS_SEQ.CURRVAL,'Transaction summary with device for higher than standard process fees.','Accounting','W', 0);
		insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 1);
		insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 14);
			
		insert into report.user_report (user_report_id, report_id, user_id, status, ccs_transport_id, frequency_id, latency, upd_dt)
		select report.user_report_seq.nextval, (select report_id from report.reports where report_name = 'High Process Fee Transaction Summary With Device'), 
			ur.user_id, 'A', ur.ccs_transport_id, ur.frequency_id, ur.latency, to_date('09-30-2012 00:00:00','MM-DD-YYYY HH24:MI:SS')
		from report.reports r join report.user_report ur on r.report_id = ur.report_id
		where report_name = 'Transaction Processing Entry Summary';
	END IF;
		
	COMMIT;
END;
/

begin
	insert into web_content.web_link(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER) 
	select web_content.seq_web_link_id.nextval, 'Back Office', 'back_office.html', 'Charge your customers for your products and services', 'General', 'M', 111 from dual
	where not exists (select 1 from web_content.web_link where web_link_label = 'Back Office');

	if sql%found then
		insert into web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) select web_link_id, 1 from web_content.web_link where web_link_label = 'Back Office';
		insert into web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) select web_link_id, 4 from web_content.web_link where web_link_label = 'Back Office';
	end if;

	commit;
end;
/

COMMENT ON COLUMN REPORT.PRIV.INTERNAL_EXTERNAL_FLAG IS 'Valid Values: B - Any user may use and grant this privilege if they have it; I - Only Internal users may use and grant this privilege if they have it; E - Only External users may use this privilege and any user may grant this privilege if they have it; P - Only External users may use this privilege and only Internal users may grant this privilege if they have it';  
INSERT INTO REPORT.PRIV(PRIV_ID, DESCRIPTION, INTERNAL_EXTERNAL_FLAG, CUSTOMER_MASTER_USER_DEFAULT)
    SELECT 26, 'Back Office', 'P', 'N'
      FROM DUAL
     WHERE NOT EXISTS(SELECT 1 FROM REPORT.PRIV WHERE PRIV_ID = 26);

INSERT INTO WEB_CONTENT.REQUIREMENT(REQUIREMENT_ID, REQUIREMENT_NAME, REQUIREMENT_CLASS, REQUIREMENT_PARAM_CLASS, REQUIREMENT_PARAM_VALUE)
    SELECT 38, 'REQ_BACK_OFFICE', 'com.usatech.usalive.link.HasPrivilegeRequirement', 'java.lang.Integer', '26'
      FROM DUAL
     WHERE NOT EXISTS(SELECT 1 FROM WEB_CONTENT.REQUIREMENT WHERE REQUIREMENT_ID = 38);
     
INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT(WEB_LINK_ID, REQUIREMENT_ID)
    SELECT WEB_LINK_ID, 38
      FROM WEB_CONTENT.WEB_LINK WL
     WHERE WEB_LINK_LABEL = 'Back Office'
       AND NOT EXISTS(SELECT 1 FROM WEB_CONTENT.WEB_LINK_REQUIREMENT WLR WHERE WLR.WEB_LINK_ID = WL.WEB_LINK_ID AND WLR.REQUIREMENT_ID = 38);
	   
COMMIT;

INSERT INTO FOLIO_CONF.CHART_TYPE(CHART_TYPE_ID, CHART_TYPE_CD, CHART_TYPE_NAME, CHART_TYPE_DESCRIPTION)
    SELECT 24, 'XYStackedBarChart', 'XY Stacked Bar Chart', 'XY Stacked Bar Chart'
      FROM DUAL
     WHERE NOT EXISTS(SELECT 1 FROM FOLIO_CONF.CHART_TYPE WHERE CHART_TYPE_ID = 24);

COMMIT;

ALTER TABLE REPORT.TERMINAL ADD DOING_BUSINESS_AS VARCHAR2(21);
ALTER TABLE CORP.CUSTOMER ADD DOING_BUSINESS_AS VARCHAR2(21);
ALTER TABLE PSS.MERCHANT ADD DOING_BUSINESS_AS VARCHAR2(21);
UPDATE PSS.MERCHANT SET DOING_BUSINESS_AS = SUBSTR(MERCHANT_NAME, 1, 21);
COMMIT;
ALTER TABLE PSS.MERCHANT MODIFY DOING_BUSINESS_AS NOT NULL;

UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_LABEL = 'Preferences and Setup' WHERE WEB_LINK_LABEL = 'Preferences';
COMMIT;

CREATE TABLE CORP.CUSTOMER_DEALER(
    CUSTOMER_ID NUMBER NOT NULL,
    DEALER_ID NUMBER NOT NULL,
    CREATED_BY VARCHAR2(30) NOT NULL,
    CREATED_TS DATE NOT NULL,
    LAST_UPDATED_BY VARCHAR2(30) NOT NULL,
    LAST_UPDATED_TS DATE NOT NULL,
    CONSTRAINT PK_CUSTOMER_DEALER PRIMARY KEY(CUSTOMER_ID, DEALER_ID),
    CONSTRAINT FK_CUSTOMER_DEALER_CUSTOMER_ID FOREIGN KEY (CUSTOMER_ID) REFERENCES CORP.CUSTOMER(CUSTOMER_ID),
    CONSTRAINT FK_CUSTOMER_DEALER_DEALER_ID FOREIGN KEY (DEALER_ID) REFERENCES CORP.DEALER(DEALER_ID)
) TABLESPACE CORP_DATA;

CREATE OR REPLACE TRIGGER CORP.TRBI_CUSTOMER_DEALER BEFORE INSERT ON CORP.CUSTOMER_DEALER
  FOR EACH ROW
BEGIN
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER CORP.TRBU_CUSTOMER_DEALER BEFORE UPDATE ON CORP.CUSTOMER_DEALER
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

GRANT SELECT ON CORP.CUSTOMER_DEALER TO USALIVE_APP_ROLE, USAT_DEV_READ_ONLY, USAT_DMS_ROLE;
GRANT INSERT, UPDATE, DELETE ON CORP.CUSTOMER_DEALER TO USAT_DMS_ROLE;

UPDATE REPORT.REPORT_PARAM
SET PARAM_VALUE = CASE PARAM_NAME
		WHEN 'paramNames' THEN 'StartDate,EndDate,StartDate,EndDate'
		WHEN 'paramTypes' THEN 'TIMESTAMP,TIMESTAMP,TIMESTAMP,TIMESTAMP'
		WHEN 'query' THEN 'SELECT
lts.entry_type,
bu.business_unit_name,
DECODE(bat.payment_schedule_id, 4, ''Y'', ''N'') monthly_payment,
cu.currency_code,
c.customer_name,
lts.fee_name,
DECODE(d.status,
	''S'', ''Y'',
	''P'', ''Y'',
	''N''
) in_eft,
TO_CHAR(TRUNC(lts.entry_date, ''MONTH''),''mm/dd/yyyy'') month_for,
COUNT(1) entry_count,
SUM (lts.amount) total_entry_amount,
lts.description
FROM (
	SELECT
	led.batch_id,
	led.amount,
	led.entry_type,
	led.settle_state_id,
	led.create_date,
	led.entry_date,
	led.description,
	sfi.fee_name
	FROM corp.ledger led
	LEFT OUTER JOIN report.trans tr
	ON tr.tran_id = led.trans_id
	LEFT OUTER JOIN (
		SELECT
		sf.service_fee_id,
		f.fee_name
		FROM corp.service_fees sf
		JOIN corp.fees f
		ON f.fee_id = sf.fee_id
	) sfi
	ON sfi.service_fee_id = led.service_fee_id
	WHERE led.entry_type IN (''SF'', ''AD'', ''SB'')
	AND led.deleted = ''N''
	AND led.settle_state_id IN (2, 3)
	AND (led.report_date >= CAST(? AS DATE) AND led.report_date < CAST(? AS DATE) + 1
	OR led.create_date >= CAST(? AS DATE) AND led.create_date < CAST(? AS DATE) + 1 AND led.report_date IS NULL)
)lts
JOIN corp.batch bat
ON bat.batch_id = lts.batch_id
JOIN corp.doc d
ON d.doc_id = bat.doc_id
JOIN corp.customer_bank cb
ON cb.customer_bank_id = d.customer_bank_id
JOIN corp.customer c
ON c.customer_id = cb.customer_id
JOIN corp.business_unit bu
ON bu.business_unit_id = d.business_unit_id
JOIN corp.currency cu
ON cu.currency_id = d.currency_id
JOIN report.trans_state ts
ON ts.state_id = lts.settle_state_id -- yes, this is non intuitive but correct
GROUP BY
lts.entry_type,
bu.business_unit_name,
DECODE(bat.payment_schedule_id, 4, ''Y'', ''N''),
cu.currency_code,
c.customer_name,
lts.fee_name,
DECODE(d.status,
	''S'', ''Y'',
	''P'', ''Y'',
	''N''
),
TRUNC(lts.entry_date, ''MONTH''),
lts.description' END
WHERE REPORT_ID = (SELECT REPORT_ID FROM REPORT.REPORTS WHERE REPORT_NAME = 'All Other Fee Entries') AND PARAM_NAME IN ('paramNames', 'paramTypes', 'query');

UPDATE REPORT.REPORT_PARAM
SET PARAM_VALUE = CASE PARAM_NAME
		WHEN 'paramNames' THEN 'StartDate,EndDate,StartDate,EndDate'
		WHEN 'paramTypes' THEN 'TIMESTAMP,TIMESTAMP,TIMESTAMP,TIMESTAMP'
		WHEN 'query' THEN 'SELECT
lts.entry_type,
bu.business_unit_name,
DECODE(bat.payment_schedule_id, 4, ''Y'', ''N'') monthly_payment,
cu.currency_code,
c.customer_name,
ee.eport_num,
lts.fee_name,
DECODE(d.status,
	''S'', ''Y'',
	''P'', ''Y'',
	''N''
) in_eft,
TO_CHAR(TRUNC(lts.entry_date, ''MONTH''),''mm/dd/yyyy'') month_for,
COUNT(1) entry_count,
SUM (lts.amount) total_entry_amount,
lts.description,
t.terminal_nbr
FROM (
	SELECT
	led.batch_id,
	led.amount,
	led.entry_type,
	led.settle_state_id,
	led.create_date,
	led.entry_date,
	led.description,
	sfi.fee_name
	FROM corp.ledger led
	LEFT OUTER JOIN report.trans tr
	ON tr.tran_id = led.trans_id
	LEFT OUTER JOIN (
		SELECT
		sf.service_fee_id,
		f.fee_name
		FROM corp.service_fees sf
		JOIN corp.fees f
		ON f.fee_id = sf.fee_id
	) sfi
	ON sfi.service_fee_id = led.service_fee_id
	WHERE led.entry_type IN (''SF'', ''AD'', ''SB'')
	AND led.deleted = ''N''
	AND led.settle_state_id IN (2, 3)
	AND (led.report_date >= CAST(? AS DATE) AND led.report_date < CAST(? AS DATE) + 1
	OR led.create_date >= CAST(? AS DATE) AND led.create_date < CAST(? AS DATE) + 1 AND led.report_date IS NULL)
)lts
JOIN corp.batch bat
ON bat.batch_id = lts.batch_id
JOIN corp.doc d
ON d.doc_id = bat.doc_id
JOIN corp.customer_bank cb
ON cb.customer_bank_id = d.customer_bank_id
JOIN corp.customer c
ON c.customer_id = cb.customer_id
JOIN corp.business_unit bu
ON bu.business_unit_id = d.business_unit_id
JOIN corp.currency cu
ON cu.currency_id = d.currency_id
JOIN report.trans_state ts
ON ts.state_id = lts.settle_state_id
LEFT outer JOIN (SELECT DISTINCT TERMINAL_ID, FIRST_VALUE(eport_serial_num) OVER (PARTITION BY TERMINAL_ID ORDER BY start_date DESC) Eport_num
from report.terminal_eport te, report.eport e where te.eport_id=e.eport_id) ee
ON bat.terminal_id=ee.terminal_id
LEFT OUTER JOIN report.terminal t ON bat.terminal_id = t.terminal_id
GROUP BY
lts.entry_type,
bu.business_unit_name,
DECODE(bat.payment_schedule_id, 4, ''Y'', ''N''),
cu.currency_code,
c.customer_name,
lts.fee_name,
DECODE(d.status,
	''S'', ''Y'',
	''P'', ''Y'',
	''N''
),
TRUNC(lts.entry_date, ''MONTH''),
lts.description
,ee.Eport_num,
t.terminal_nbr' END
WHERE REPORT_ID = (SELECT REPORT_ID FROM REPORT.REPORTS WHERE REPORT_NAME = 'All Other Fee Entries With Device') AND PARAM_NAME IN ('paramNames', 'paramTypes', 'query');

COMMIT;

UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET ACTIVE = 'N' WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1010';
UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET DESCRIPTION = 'This number represents how fast the device will attempt to parse a new MDB message packet received from the VMC. Too fast and the device will be wasting CPU time. Too slow will delay the response timing. Parse Delay Time = This number x 0.2ms. The value of 12 = 2.4ms. MDB response delay is 0.3ms + this value * 0.2ms.' WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1011';
COMMIT;

ALTER TABLE CORP.SERVICE_FEES ADD NO_TRIGGER_EVENT_FLAG VARCHAR2(1);
ALTER TABLE REPORT.TERMINAL ADD (SALES_ORDER_NUMBER VARCHAR2(50), PURCHASE_ORDER_NUMBER VARCHAR2(50));