WHENEVER SQLERROR EXIT FAILURE COMMIT;

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/releases/REL_Edge_Update_R40/Prepaid.Pepi.Branding.sql?rev=HEAD
INSERT INTO WEB_CONTENT.SUBDOMAIN(SUBDOMAIN_URL, SUBDOMAIN_DESCRIPTION)
  SELECT SUBDOMAIN_URL || '/pepi', 'Pepi ' || SUBDOMAIN_DESCRIPTION
  FROM WEB_CONTENT.SUBDOMAIN 
  WHERE SUBDOMAIN_DESCRIPTION LIKE 'GetMore%'
    AND NOT EXISTS(SELECT 1 FROM WEB_CONTENT.SUBDOMAIN 
  WHERE SUBDOMAIN_DESCRIPTION LIKE 'Pepi GetMore%');
  
INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, LITERAL_VALUE, SUBDOMAIN_ID)
    SELECT L.LITERAL_KEY, L.LITERAL_VALUE, S.SUBDOMAIN_ID
      FROM (
    SELECT '' LITERAL_KEY, '' LITERAL_VALUE FROM DUAL WHERE 0=1
    UNION ALL SELECT 'prepaid-title', 'The Pepi Companies -' FROM DUAL
    UNION ALL SELECT 'prepaid-footer-image-url', '/images/logo-pepi-footer200x80.png' FROM DUAL
    UNION ALL SELECT 'prepaid-footer-image-title', 'the pepi companies' FROM DUAL
    UNION ALL SELECT 'prepaid-top-image-url', '/images/icon-logo_pepi-white.png' FROM DUAL
    UNION ALL SELECT 'prepaid-top-image-welcome-url', '/images/icon-logo_pepi.png' FROM DUAL
    UNION ALL SELECT 'prepaid-home-image-url', '/images/home-logo_simplymore-simplybetter.png' FROM DUAL
    UNION ALL SELECT 'prepaid-home-image-title', 'Simply more. Simply better.' FROM DUAL
    UNION ALL SELECT 'prepaid-cash-icon-url', '/images/icon-cash60x50_pepi.png' FROM DUAL
    UNION ALL SELECT 'prepaid-sale-icon-url', '/images/icon-sale60x50_pepi.png' FROM DUAL
    UNION ALL SELECT 'prepaid-new-icon-url', '/images/icon-new60x50_pepi.png' FROM DUAL
    UNION ALL SELECT 'prepaid-new-user-image', '/images/logo-pepi-signup150x60.png' FROM DUAL
    UNION ALL SELECT 'prepaid-new-user-image-title', 'the pepi companies' FROM DUAL
    UNION ALL SELECT 'prepaid-stylesheets', '/css/normalize.css;/css/main.css;/css/pepi.css' FROM DUAL) L
    CROSS JOIN WEB_CONTENT.SUBDOMAIN S
 WHERE S.SUBDOMAIN_URL like '%/pepi'
   AND NOT EXISTS(SELECT 1 FROM  WEB_CONTENT.LITERAL L0 WHERE L0.LITERAL_KEY = L.LITERAL_KEY AND L0.SUBDOMAIN_ID = S.SUBDOMAIN_ID AND L0.LOCALE_CD IS NULL);

COMMIT;
 
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/CORP/NEXT_PAY_DAY.fnc?rev=HEAD
CREATE OR REPLACE FUNCTION CORP.NEXT_PAY_DAY(
    pd_date DATE,
    pn_pay_cycle_id NUMBER)
    RETURN DATE
    DETERMINISTIC
    PARALLEL_ENABLE
IS
    ld_tmp DATE;
BEGIN
    IF pn_pay_cycle_id = 1 THEN
        RETURN NEXT_DAY(TRUNC(pd_date), 'MONDAY');
    ELSIF pn_pay_cycle_id = 2 THEN
        RETURN NEXT_DAY(TRUNC(pd_date), 'THURSDAY');
    ELSIF pn_pay_cycle_id = 3 THEN
        RETURN NEXT_DAY(TRUNC(pd_date), 'FRIDAY');
    ELSIF pn_pay_cycle_id = 4 THEN
        ld_tmp := NEXT_DAY(TRUNC(pd_date, 'MONTH'), 'FRIDAY');
        IF ld_tmp > TRUNC(pd_date) + 1 THEN
            RETURN ld_tmp;
        ELSE
            RETURN NEXT_DAY(ADD_MONTHS(TRUNC(pd_date, 'MONTH'), 1), 'FRIDAY');
        END IF;
    ELSIF pn_pay_cycle_id = 5 THEN
        IF EXTRACT(DAY FROM pd_date) >= 15 THEN
            RETURN ADD_MONTHS(TRUNC(pd_date, 'MONTH'), 1) + 14;
        ELSE
            RETURN TRUNC(pd_date, 'MONTH') + 14;
        END IF;
    ELSIF pn_pay_cycle_id = 6 THEN
        IF EXTRACT(DAY FROM pd_date) >= 3 THEN
            RETURN ADD_MONTHS(TRUNC(pd_date, 'MONTH'), 1) + 2;
        ELSE
            RETURN TRUNC(pd_date, 'MONTH') + 2;
        END IF;
    ELSIF pn_pay_cycle_id = 7 THEN
        RETURN LEAST(NEXT_DAY(TRUNC(pd_date), 'WEDNESDAY'), NEXT_DAY(TRUNC(pd_date), 'FRIDAY'));
    ELSIF pn_pay_cycle_id = 8 THEN
        RETURN TRUNC(pd_date) + 1;
    ELSIF pn_pay_cycle_id = 9 THEN
        RETURN NEXT_DAY(TRUNC(pd_date), 'TUESDAY');
    ELSIF pn_pay_cycle_id = 10 THEN
        RETURN NEXT_DAY(TRUNC(pd_date), 'WEDNESDAY');
    ELSE
        RETURN NULL;
    END IF;
END;
/
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/releases/REL_Edge_Update_R40/R40.USAPDB.1.sql?rev=HEAD
GRANT SELECT ON CORP.CUSTOMER_ADDR TO DEVICE;
GRANT EXECUTE ON CORP.NEXT_PAY_DAY TO USALIVE_APP_ROLE;
GRANT SELECT ON CORP.BATCH_TOTAL TO USALIVE_APP_ROLE;
GRANT SELECT ON CORP.CUSTOMER TO USAT_PREPAID_APP_ROLE;
GRANT SELECT ON PSS.TRAN_LINE_ITEM_TYPE TO REPORT;
GRANT SELECT, REFERENCES ON PSS.CONSUMER_ACCT_BASE TO CORP;
GRANT SELECT, REFERENCES ON DEVICE.DEVICE TO CORP;
GRANT SELECT, REFERENCES ON REPORT.TIME_ZONE TO CORP;
GRANT EXECUTE ON REPORT.GET_OR_CREATE_EPORT TO CORP;
GRANT EXECUTE ON REPORT.PKG_CUSTOMER_MANAGEMENT TO CORP;
GRANT EXECUTE ON PKG_DEVICE_CONFIGURATION TO CORP;
GRANT SELECT ON PSS.CONSUMER_ACCT_BASE TO USALIVE_APP_ROLE;
GRANT SELECT ON DEVICE.DEVICE_LAST_ACTIVE TO USALIVE_APP_ROLE;
GRANT SELECT ON DEVICE.DEVICE_SUB_TYPE TO USALIVE_APP_ROLE;
GRANT SELECT ON CORP.CUSTOMER TO USAT_POSM_ROLE;

INSERT INTO PSS.TRAN_LINE_ITEM_TYPE(TRAN_LINE_ITEM_TYPE_ID, TRAN_LINE_ITEM_TYPE_DESC, TRAN_LINE_ITEM_TYPE_GROUP_CD, TRAN_LINE_ITEM_TYPE_SIGN_PN)
    SELECT 207, 'Attendant', 'I', 'P' FROM DUAL
     WHERE NOT EXISTS(SELECT 1 FROM PSS.TRAN_LINE_ITEM_TYPE WHERE TRAN_LINE_ITEM_TYPE_ID = 207);

INSERT INTO PSS.AUTH_RESULT(AUTH_RESULT_CD, AUTH_RESULT_DESC)
    SELECT * FROM(
    SELECT 'C' AUTH_RESULT_CD, 'CVV Mis-Match' AUTH_RESULT_DESC FROM DUAL
    UNION ALL
    SELECT 'V', 'AVS Mis-Match' FROM DUAL
    UNION ALL
    SELECT 'M', 'CVV and AVS Mis-Match' FROM DUAL) N
    WHERE NOT EXISTS(SELECT 1 FROM PSS.AUTH_RESULT WHERE AUTH_RESULT_CD = N.AUTH_RESULT_CD);

COMMIT;

INSERT INTO PSS.POS_PTA_TMPL(POS_PTA_TMPL_NAME, POS_PTA_TMPL_DESC)
SELECT 'DEFAULT: Back Office Virtual Initialization - US', 'Default template for Back Office Virtual in US during initialization' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'DEFAULT: Back Office Virtual Initialization - US');

INSERT INTO PSS.POS_PTA_TMPL_ENTRY(POS_PTA_TMPL_ID, PAYMENT_SUBTYPE_ID, POS_PTA_ACTIVATION_OSET_HR, PAYMENT_SUBTYPE_KEY_ID, POS_PTA_PRIORITY, CURRENCY_CD, AUTHORITY_PAYMENT_MASK_ID, POS_PTA_DISABLE_DEBIT_DENIAL)
SELECT TM.POS_PTA_TMPL_ID, PST.PAYMENT_SUBTYPE_ID, 0, T.TERMINAL_ID, PST.PRIORITY, 'USD', PST.AUTHORITY_PAYMENT_MASK_ID, 'Y'
  FROM PSS.POS_PTA_TMPL TM
 CROSS JOIN V$DATABASE D
 CROSS JOIN (
     SELECT PST.*, ROWNUM PRIORITY 
       FROM (SELECT * FROM PSS.PAYMENT_SUBTYPE
      WHERE CLIENT_PAYMENT_TYPE_CD IN('Q')
        AND PAYMENT_SUBTYPE_CLASS IN('Authority::ISO8583::Elavon', 'Authority::NOP')  --XXX: Can you use a MORE card?
      ORDER BY DECODE(SUBSTR(PAYMENT_SUBTYPE_NAME, 1, 4), 'Visa', 1, 'Mast', 2, 'Amer', 3, 'Disc', 4, 5), PAYMENT_SUBTYPE_ID) PST) PST
  JOIN (PSS.TERMINAL T
  JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
  JOIN AUTHORITY.AUTHORITY A ON M.AUTHORITY_ID = A.AUTHORITY_ID) 
    ON T.STATUS_CD = 'A'
   AND M.STATUS_CD = 'A'
   AND T.TERMINAL_CD = DECODE(PST.PAYMENT_SUBTYPE_CLASS,
        'Authority::ISO8583::Elavon', CASE 
            WHEN D.NAME LIKE 'USADEV%' THEN '9998012015809000' 
            WHEN D.NAME LIKE 'ECC%' THEN '9998014592748000' 
            ELSE '999----------000' END, 
        'Authority::NOP', '0') -- adjust for environment
   AND M.MERCHANT_CD = DECODE(PST.PAYMENT_SUBTYPE_CLASS,
        'Authority::ISO8583::Elavon', '1060', 
        'Authority::NOP', '0') -- adjust for environment 
   AND A.AUTHORITY_NAME = DECODE(PST.PAYMENT_SUBTYPE_CLASS, 'Authority::ISO8583::Elavon', 'Elavon ISO8583', 'Authority::NOP', 'Error Bin') 
 WHERE TM.POS_PTA_TMPL_NAME = 'DEFAULT: Back Office Virtual Initialization - US'
   AND NOT EXISTS(SELECT 1 FROM PSS.POS_PTA_TMPL_ENTRY E WHERE E.POS_PTA_TMPL_ID = TM.POS_PTA_TMPL_ID AND E.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID);

INSERT INTO PSS.POS_PTA_TMPL(POS_PTA_TMPL_NAME, POS_PTA_TMPL_DESC)
SELECT 'DEFAULT: Back Office Virtual Initialization - Canada', 'Default template for Back Office Virtual in Canada during initialization' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'DEFAULT: Back Office Virtual Initialization - Canada');

INSERT INTO PSS.POS_PTA_TMPL_ENTRY(POS_PTA_TMPL_ID, PAYMENT_SUBTYPE_ID, POS_PTA_ACTIVATION_OSET_HR, PAYMENT_SUBTYPE_KEY_ID, POS_PTA_PRIORITY, CURRENCY_CD, AUTHORITY_PAYMENT_MASK_ID, POS_PTA_DISABLE_DEBIT_DENIAL)
SELECT TM.POS_PTA_TMPL_ID, PST.PAYMENT_SUBTYPE_ID, 0, T.TERMINAL_ID, PST.PRIORITY, 'CAD', PST.AUTHORITY_PAYMENT_MASK_ID, 'Y'
  FROM PSS.POS_PTA_TMPL TM
 CROSS JOIN V$DATABASE D
 CROSS JOIN (
     SELECT PST.*, ROWNUM PRIORITY 
       FROM (SELECT * FROM PSS.PAYMENT_SUBTYPE
      WHERE CLIENT_PAYMENT_TYPE_CD IN('Q')
        AND PAYMENT_SUBTYPE_CLASS IN('Authority::ISO8583::FHMS::Paymentech', 'Authority::NOP')  --XXX: Can you use a MORE card?
      ORDER BY DECODE(SUBSTR(PAYMENT_SUBTYPE_NAME, 1, 4), 'Visa', 1, 'Mast', 2, 'Amer', 3, 'Disc', 4, 5), PAYMENT_SUBTYPE_ID) PST) PST
  JOIN (PSS.TERMINAL T
  JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
  JOIN AUTHORITY.AUTHORITY A ON M.AUTHORITY_ID = A.AUTHORITY_ID) 
    ON T.STATUS_CD = 'A'
   AND M.STATUS_CD = 'A'
   AND M.MERCHANT_CD = DECODE(PST.PAYMENT_SUBTYPE_CLASS,
        'Authority::ISO8583::FHMS::Paymentech', CASE 
            WHEN D.NAME LIKE 'USADEV%' THEN '700000008124' 
            WHEN D.NAME LIKE 'ECC%' THEN '700000008124' 
            ELSE '12400000----' END, 
        'Authority::NOP', '0') -- adjust for environment
   AND A.AUTHORITY_NAME = DECODE(PST.PAYMENT_SUBTYPE_CLASS, 'Authority::ISO8583::FHMS::Paymentech', 'Paymentech', 'Authority::NOP', 'Error Bin') 
 WHERE TM.POS_PTA_TMPL_NAME = 'DEFAULT: Back Office Virtual Initialization - Canada'
   AND NOT EXISTS(SELECT 1 FROM PSS.POS_PTA_TMPL_ENTRY E WHERE E.POS_PTA_TMPL_ID = TM.POS_PTA_TMPL_ID AND E.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID);

UPDATE DEVICE.DEVICE_TYPE
   SET DEVICE_TYPE_SERIAL_CD_REGEX = '^V[1-2]-.*$'
 WHERE DEVICE_TYPE_ID = 14
   AND DEVICE_TYPE_SERIAL_CD_REGEX = '^V1-.*$';

INSERT INTO DEVICE.DEVICE_SUB_TYPE(DEVICE_TYPE_ID,DEVICE_SUB_TYPE_ID,DEVICE_SUB_TYPE_DESC,DEVICE_SERIAL_CD_REGEX,POS_PTA_TMPL_ID,DEF_BASE_HOST_TYPE_ID,DEF_BASE_HOST_EQUIPMENT_ID,MASTER_ID_ALWAYS_INC,REQUIRE_PREREGISTER) 
    SELECT 14, 3,'Back Office Virtual - USD','^V2-[0-9][0-9]*-USD$', T.POS_PTA_TMPL_ID, 1,HE.HOST_EQUIPMENT_ID,'N','Y' 
      FROM PSS.POS_PTA_TMPL T, DEVICE.HOST_EQUIPMENT HE
     WHERE T.POS_PTA_TMPL_NAME = 'DEFAULT: Back Office Virtual Initialization - US'
       AND HE.HOST_EQUIPMENT_MFGR = 'Unknown' AND HE.HOST_EQUIPMENT_MODEL = 'Unknown'
       AND NOT EXISTS(SELECT 1 FROM DEVICE.DEVICE_SUB_TYPE WHERE DEVICE_TYPE_ID = 14 AND DEVICE_SUB_TYPE_ID = 3)
    UNION ALL
    SELECT 14, 4,'Back Office Virtual - CAD','^V2-[0-9][0-9]*-CAD$', T.POS_PTA_TMPL_ID, 1,HE.HOST_EQUIPMENT_ID,'N','Y' 
      FROM PSS.POS_PTA_TMPL T, DEVICE.HOST_EQUIPMENT HE
     WHERE POS_PTA_TMPL_NAME = 'DEFAULT: Back Office Virtual Initialization - Canada'
       AND HE.HOST_EQUIPMENT_MFGR = 'Unknown' AND HE.HOST_EQUIPMENT_MODEL = 'Unknown'
       AND NOT EXISTS(SELECT 1 FROM DEVICE.DEVICE_SUB_TYPE WHERE DEVICE_TYPE_ID = 14 AND DEVICE_SUB_TYPE_ID = 4);

COMMIT;

CREATE SEQUENCE CORP.SEQ_PAYOR_ID;

CREATE TABLE CORP.PAYOR(
    PAYOR_ID NUMBER(20) NOT NULL,
    PAYOR_NAME VARCHAR2(50) NOT NULL,
    PAYOR_EMAIL VARCHAR2(60),
    PAY_TO_DEVICE_ID NUMBER(20) NOT NULL,
    GLOBAL_ACCOUNT_ID NUMBER(19) NOT NULL,
    TRUNCATED_CARD_NUMBER VARCHAR2(50) NOT NULL,
    TOKEN_CD RAW(32) NOT NULL,
    LAST_CHARGE_AMOUNT NUMBER(14,4),
    LAST_CHARGE_TS DATE,
    MAX_CHARGE_AMOUNT NUMBER(14,4),
    MIN_INTERVAL_MONTHS NUMBER(4),
    MIN_INTERVAL_DAYS NUMBER(4),  
    CREATED_BY_ID NUMBER,
    CREATED_BY VARCHAR2(30) NOT NULL,
    CREATED_TS DATE NOT NULL,
    LAST_UPDATED_BY VARCHAR2(30) NOT NULL,
    LAST_UPDATED_TS DATE NOT NULL,
    CONSTRAINT PK_PAYOR PRIMARY KEY(PAYOR_ID),
    CONSTRAINT FK_PAYOR_DEVICE_ID FOREIGN KEY (PAY_TO_DEVICE_ID) REFERENCES DEVICE.DEVICE(DEVICE_ID),
    CONSTRAINT FK_PAYOR_CREATED_BY_ID FOREIGN KEY (CREATED_BY_ID) REFERENCES REPORT.USER_LOGIN(USER_ID)
) TABLESPACE CORP_DATA;

CREATE OR REPLACE TRIGGER CORP.TRBI_PAYOR BEFORE INSERT ON CORP.PAYOR
  FOR EACH ROW
BEGIN
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER CORP.TRBU_PAYOR BEFORE UPDATE ON CORP.PAYOR
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

CREATE INDEX CORP.IX_PAYOR_COMBO_1 ON CORP.PAYOR(GLOBAL_ACCOUNT_ID, PAY_TO_DEVICE_ID) TABLESPACE CORP_INDX;

CREATE INDEX CORP.IX_PAYOR_PAY_TO_DEVICE_ID ON CORP.PAYOR(PAY_TO_DEVICE_ID) TABLESPACE CORP_INDX;

GRANT SELECT ON CORP.PAYOR TO USALIVE_APP_ROLE, USAT_DEV_READ_ONLY;

GRANT SELECT ON CORP.SEQ_PAYOR_ID TO USALIVE_APP_ROLE;
GRANT INSERT, UPDATE, DELETE ON CORP.PAYOR TO USALIVE_APP_ROLE;

GRANT SELECT, UPDATE ON CORP.PAYOR TO PSS;

INSERT INTO ENGINE.APP_SETTING(APP_SETTING_CD, APP_SETTING_VALUE, APP_SETTING_DESC)
SELECT 'STANDARD_CREDIT_PROCESS_FEE_PERCENT', 0.0545, 'Standard credit process fee' FROM DUAL WHERE NOT EXISTS(
SELECT 1 FROM ENGINE.APP_SETTING WHERE APP_SETTING_CD = 'STANDARD_CREDIT_PROCESS_FEE_PERCENT'
);
COMMIT;

BEGIN
	INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
	SELECT REPORT.REPORTS_SEQ.NEXTVAL, 'High Process Fee Transaction Summary With Device - {b} to {e}', 1, 0, 'High Process Fee Transaction Summary With Device', 'Transaction summary with device for higher than standard process fees.', 'N', user_id
	FROM report.reports WHERE report_name = 'Transaction Processing Entry Summary' 
	AND NOT EXISTS (SELECT 1 FROM REPORT.REPORTS WHERE REPORT_NAME = 'High Process Fee Transaction Summary With Device');

	IF SQL%FOUND THEN
		INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
		VALUES( REPORT.REPORTS_SEQ.CURRVAL, 'query', 'SELECT 
				led.entry_type,
				bu.business_unit_name,
				DECODE(t.payment_schedule_id, 4, ''Y'', ''N'') monthly_payment,
				cr.currency_code,
				c.customer_name,
				TO_CHAR(TRUNC(tr.settle_date, ''MONTH''), ''mm/dd/yyyy'') month_for,
				e.eport_serial_num,
				pf.fee_percent * 100 fee_percent,
				COUNT(1) tran_count,
				SUM(tr.total_amount) total_tran_amount,
				SUM(led.amount) total_ledger_amount
				FROM report.trans tr
				JOIN corp.currency cr
				ON cr.currency_id = tr.currency_id
				JOIN report.trans_state ts
				ON ts.state_id = tr.settle_state_id 
				JOIN report.terminal t ON tr.terminal_id = t.terminal_id
				JOIN corp.customer c ON c.customer_id = t.customer_id
				JOIN corp.business_unit bu ON bu.business_unit_id = t.business_unit_id
				JOIN corp.ledger led
				ON led.trans_id = tr.tran_id
				AND led.deleted = ''N''
				JOIN report.eport e on tr.eport_id = e.eport_id
				JOIN corp.process_fees pf ON t.terminal_id = pf.terminal_id
				WHERE tr.trans_type_id IN (14, 16, 19, 20, 21) 
				AND tr.settle_state_id IN (2, 3) 
				AND led.entry_type IN (''CC'', ''PF'', ''CB'', ''RF'') 
				AND tr.settle_date >= CAST(? AS DATE) AND tr.settle_date < CAST(? AS DATE) + 1
				AND pf.trans_type_id = 16 AND (pf.start_date IS NULL OR pf.start_date < SYSDATE) AND (pf.end_date IS NULL OR pf.end_date > SYSDATE)
				AND pf.fee_percent > TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING(''STANDARD_CREDIT_PROCESS_FEE_PERCENT''))
				GROUP BY
				led.entry_type,
				bu.business_unit_name,
				DECODE(t.payment_schedule_id, 4, ''Y'', ''N''),
				cr.currency_code,
				c.customer_name,
				TRUNC(tr.settle_date, ''MONTH''),
				e.eport_serial_num,
				pf.fee_percent
				ORDER BY c.customer_name, e.eport_serial_num, led.entry_type');
		INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
		VALUES( REPORT.REPORTS_SEQ.CURRVAL, 'header', 'true');
		insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'dateFormat','MM/dd/yyyy');
		insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'params.StartDate','{b}');
		insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'params.EndDate','{e}');
		insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'paramNames','StartDate,EndDate');
		insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'paramTypes','TIMESTAMP,TIMESTAMP');
			
		insert into web_content.web_link values(web_content.seq_web_link_id.nextval, 'High Process Fee Transaction Summary With Device', './select_date_range_frame_nonfolio.i?basicReportId='||REPORT.REPORTS_SEQ.CURRVAL,'Transaction summary with device for higher than standard process fees.','Accounting','W', 0);
		insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 1);
		insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 14);
			
		insert into report.user_report (user_report_id, report_id, user_id, status, ccs_transport_id, frequency_id, latency, upd_dt)
		select report.user_report_seq.nextval, (select report_id from report.reports where report_name = 'High Process Fee Transaction Summary With Device'), 
			ur.user_id, 'A', ur.ccs_transport_id, ur.frequency_id, ur.latency, to_date('09-30-2012 00:00:00','MM-DD-YYYY HH24:MI:SS')
		from report.reports r join report.user_report ur on r.report_id = ur.report_id
		where report_name = 'Transaction Processing Entry Summary';
	END IF;
		
	COMMIT;
END;
/

begin
	insert into web_content.web_link(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER) 
	select web_content.seq_web_link_id.nextval, 'Back Office', 'back_office.html', 'Charge your customers for your products and services', 'General', 'M', 111 from dual
	where not exists (select 1 from web_content.web_link where web_link_label = 'Back Office');

	if sql%found then
		insert into web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) select web_link_id, 1 from web_content.web_link where web_link_label = 'Back Office';
		insert into web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) select web_link_id, 4 from web_content.web_link where web_link_label = 'Back Office';
	end if;

	commit;
end;
/

COMMENT ON COLUMN REPORT.PRIV.INTERNAL_EXTERNAL_FLAG IS 'Valid Values: B - Any user may use and grant this privilege if they have it; I - Only Internal users may use and grant this privilege if they have it; E - Only External users may use this privilege and any user may grant this privilege if they have it; P - Only External users may use this privilege and only Internal users may grant this privilege if they have it';  
INSERT INTO REPORT.PRIV(PRIV_ID, DESCRIPTION, INTERNAL_EXTERNAL_FLAG, CUSTOMER_MASTER_USER_DEFAULT)
    SELECT 26, 'Back Office', 'P', 'N'
      FROM DUAL
     WHERE NOT EXISTS(SELECT 1 FROM REPORT.PRIV WHERE PRIV_ID = 26);

INSERT INTO WEB_CONTENT.REQUIREMENT(REQUIREMENT_ID, REQUIREMENT_NAME, REQUIREMENT_CLASS, REQUIREMENT_PARAM_CLASS, REQUIREMENT_PARAM_VALUE)
    SELECT 38, 'REQ_BACK_OFFICE', 'com.usatech.usalive.link.HasPrivilegeRequirement', 'java.lang.Integer', '26'
      FROM DUAL
     WHERE NOT EXISTS(SELECT 1 FROM WEB_CONTENT.REQUIREMENT WHERE REQUIREMENT_ID = 38);
     
INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT(WEB_LINK_ID, REQUIREMENT_ID)
    SELECT WEB_LINK_ID, 38
      FROM WEB_CONTENT.WEB_LINK WL
     WHERE WEB_LINK_LABEL = 'Back Office'
       AND NOT EXISTS(SELECT 1 FROM WEB_CONTENT.WEB_LINK_REQUIREMENT WLR WHERE WLR.WEB_LINK_ID = WL.WEB_LINK_ID AND WLR.REQUIREMENT_ID = 38);
	   
COMMIT;

INSERT INTO FOLIO_CONF.CHART_TYPE(CHART_TYPE_ID, CHART_TYPE_CD, CHART_TYPE_NAME, CHART_TYPE_DESCRIPTION)
    SELECT 24, 'XYStackedBarChart', 'XY Stacked Bar Chart', 'XY Stacked Bar Chart'
      FROM DUAL
     WHERE NOT EXISTS(SELECT 1 FROM FOLIO_CONF.CHART_TYPE WHERE CHART_TYPE_ID = 24);

COMMIT;

ALTER TABLE REPORT.TERMINAL ADD DOING_BUSINESS_AS VARCHAR2(21);
ALTER TABLE CORP.CUSTOMER ADD DOING_BUSINESS_AS VARCHAR2(21);
ALTER TABLE PSS.MERCHANT ADD DOING_BUSINESS_AS VARCHAR2(21);
UPDATE PSS.MERCHANT SET DOING_BUSINESS_AS = SUBSTR(MERCHANT_NAME, 1, 21);
COMMIT;
ALTER TABLE PSS.MERCHANT MODIFY DOING_BUSINESS_AS NOT NULL;

UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_LABEL = 'Preferences and Setup' WHERE WEB_LINK_LABEL = 'Preferences';
COMMIT;

CREATE TABLE CORP.CUSTOMER_DEALER(
    CUSTOMER_ID NUMBER NOT NULL,
    DEALER_ID NUMBER NOT NULL,
    CREATED_BY VARCHAR2(30) NOT NULL,
    CREATED_TS DATE NOT NULL,
    LAST_UPDATED_BY VARCHAR2(30) NOT NULL,
    LAST_UPDATED_TS DATE NOT NULL,
    CONSTRAINT PK_CUSTOMER_DEALER PRIMARY KEY(CUSTOMER_ID, DEALER_ID),
    CONSTRAINT FK_CUSTOMER_DEALER_CUSTOMER_ID FOREIGN KEY (CUSTOMER_ID) REFERENCES CORP.CUSTOMER(CUSTOMER_ID),
    CONSTRAINT FK_CUSTOMER_DEALER_DEALER_ID FOREIGN KEY (DEALER_ID) REFERENCES CORP.DEALER(DEALER_ID)
) TABLESPACE CORP_DATA;

CREATE OR REPLACE TRIGGER CORP.TRBI_CUSTOMER_DEALER BEFORE INSERT ON CORP.CUSTOMER_DEALER
  FOR EACH ROW
BEGIN
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER CORP.TRBU_CUSTOMER_DEALER BEFORE UPDATE ON CORP.CUSTOMER_DEALER
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

GRANT SELECT ON CORP.CUSTOMER_DEALER TO USALIVE_APP_ROLE, USAT_DEV_READ_ONLY, USAT_DMS_ROLE;
GRANT INSERT, UPDATE, DELETE ON CORP.CUSTOMER_DEALER TO USAT_DMS_ROLE;

UPDATE REPORT.REPORT_PARAM
SET PARAM_VALUE = CASE PARAM_NAME
		WHEN 'paramNames' THEN 'StartDate,EndDate,StartDate,EndDate'
		WHEN 'paramTypes' THEN 'TIMESTAMP,TIMESTAMP,TIMESTAMP,TIMESTAMP'
		WHEN 'query' THEN 'SELECT
lts.entry_type,
bu.business_unit_name,
DECODE(bat.payment_schedule_id, 4, ''Y'', ''N'') monthly_payment,
cu.currency_code,
c.customer_name,
lts.fee_name,
DECODE(d.status,
	''S'', ''Y'',
	''P'', ''Y'',
	''N''
) in_eft,
TO_CHAR(TRUNC(lts.entry_date, ''MONTH''),''mm/dd/yyyy'') month_for,
COUNT(1) entry_count,
SUM (lts.amount) total_entry_amount,
lts.description
FROM (
	SELECT
	led.batch_id,
	led.amount,
	led.entry_type,
	led.settle_state_id,
	led.create_date,
	led.entry_date,
	led.description,
	sfi.fee_name
	FROM corp.ledger led
	LEFT OUTER JOIN report.trans tr
	ON tr.tran_id = led.trans_id
	LEFT OUTER JOIN (
		SELECT
		sf.service_fee_id,
		f.fee_name
		FROM corp.service_fees sf
		JOIN corp.fees f
		ON f.fee_id = sf.fee_id
	) sfi
	ON sfi.service_fee_id = led.service_fee_id
	WHERE led.entry_type IN (''SF'', ''AD'', ''SB'')
	AND led.deleted = ''N''
	AND led.settle_state_id IN (2, 3)
	AND (led.report_date >= CAST(? AS DATE) AND led.report_date < CAST(? AS DATE) + 1
	OR led.create_date >= CAST(? AS DATE) AND led.create_date < CAST(? AS DATE) + 1 AND led.report_date IS NULL)
)lts
JOIN corp.batch bat
ON bat.batch_id = lts.batch_id
JOIN corp.doc d
ON d.doc_id = bat.doc_id
JOIN corp.customer_bank cb
ON cb.customer_bank_id = d.customer_bank_id
JOIN corp.customer c
ON c.customer_id = cb.customer_id
JOIN corp.business_unit bu
ON bu.business_unit_id = d.business_unit_id
JOIN corp.currency cu
ON cu.currency_id = d.currency_id
JOIN report.trans_state ts
ON ts.state_id = lts.settle_state_id -- yes, this is non intuitive but correct
GROUP BY
lts.entry_type,
bu.business_unit_name,
DECODE(bat.payment_schedule_id, 4, ''Y'', ''N''),
cu.currency_code,
c.customer_name,
lts.fee_name,
DECODE(d.status,
	''S'', ''Y'',
	''P'', ''Y'',
	''N''
),
TRUNC(lts.entry_date, ''MONTH''),
lts.description' END
WHERE REPORT_ID = (SELECT REPORT_ID FROM REPORT.REPORTS WHERE REPORT_NAME = 'All Other Fee Entries') AND PARAM_NAME IN ('paramNames', 'paramTypes', 'query');

UPDATE REPORT.REPORT_PARAM
SET PARAM_VALUE = CASE PARAM_NAME
		WHEN 'paramNames' THEN 'StartDate,EndDate,StartDate,EndDate'
		WHEN 'paramTypes' THEN 'TIMESTAMP,TIMESTAMP,TIMESTAMP,TIMESTAMP'
		WHEN 'query' THEN 'SELECT
lts.entry_type,
bu.business_unit_name,
DECODE(bat.payment_schedule_id, 4, ''Y'', ''N'') monthly_payment,
cu.currency_code,
c.customer_name,
ee.eport_num,
lts.fee_name,
DECODE(d.status,
	''S'', ''Y'',
	''P'', ''Y'',
	''N''
) in_eft,
TO_CHAR(TRUNC(lts.entry_date, ''MONTH''),''mm/dd/yyyy'') month_for,
COUNT(1) entry_count,
SUM (lts.amount) total_entry_amount,
lts.description,
t.terminal_nbr
FROM (
	SELECT
	led.batch_id,
	led.amount,
	led.entry_type,
	led.settle_state_id,
	led.create_date,
	led.entry_date,
	led.description,
	sfi.fee_name
	FROM corp.ledger led
	LEFT OUTER JOIN report.trans tr
	ON tr.tran_id = led.trans_id
	LEFT OUTER JOIN (
		SELECT
		sf.service_fee_id,
		f.fee_name
		FROM corp.service_fees sf
		JOIN corp.fees f
		ON f.fee_id = sf.fee_id
	) sfi
	ON sfi.service_fee_id = led.service_fee_id
	WHERE led.entry_type IN (''SF'', ''AD'', ''SB'')
	AND led.deleted = ''N''
	AND led.settle_state_id IN (2, 3)
	AND (led.report_date >= CAST(? AS DATE) AND led.report_date < CAST(? AS DATE) + 1
	OR led.create_date >= CAST(? AS DATE) AND led.create_date < CAST(? AS DATE) + 1 AND led.report_date IS NULL)
)lts
JOIN corp.batch bat
ON bat.batch_id = lts.batch_id
JOIN corp.doc d
ON d.doc_id = bat.doc_id
JOIN corp.customer_bank cb
ON cb.customer_bank_id = d.customer_bank_id
JOIN corp.customer c
ON c.customer_id = cb.customer_id
JOIN corp.business_unit bu
ON bu.business_unit_id = d.business_unit_id
JOIN corp.currency cu
ON cu.currency_id = d.currency_id
JOIN report.trans_state ts
ON ts.state_id = lts.settle_state_id
LEFT outer JOIN (SELECT DISTINCT TERMINAL_ID, FIRST_VALUE(eport_serial_num) OVER (PARTITION BY TERMINAL_ID ORDER BY start_date DESC) Eport_num
from report.terminal_eport te, report.eport e where te.eport_id=e.eport_id) ee
ON bat.terminal_id=ee.terminal_id
LEFT OUTER JOIN report.terminal t ON bat.terminal_id = t.terminal_id
GROUP BY
lts.entry_type,
bu.business_unit_name,
DECODE(bat.payment_schedule_id, 4, ''Y'', ''N''),
cu.currency_code,
c.customer_name,
lts.fee_name,
DECODE(d.status,
	''S'', ''Y'',
	''P'', ''Y'',
	''N''
),
TRUNC(lts.entry_date, ''MONTH''),
lts.description
,ee.Eport_num,
t.terminal_nbr' END
WHERE REPORT_ID = (SELECT REPORT_ID FROM REPORT.REPORTS WHERE REPORT_NAME = 'All Other Fee Entries With Device') AND PARAM_NAME IN ('paramNames', 'paramTypes', 'query');

COMMIT;

UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET ACTIVE = 'N' WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1010';
UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET DESCRIPTION = 'This number represents how fast the device will attempt to parse a new MDB message packet received from the VMC. Too fast and the device will be wasting CPU time. Too slow will delay the response timing. Parse Delay Time = This number x 0.2ms. The value of 12 = 2.4ms. MDB response delay is 0.3ms + this value * 0.2ms.' WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1011';
COMMIT;

ALTER TABLE CORP.SERVICE_FEES ADD NO_TRIGGER_EVENT_FLAG VARCHAR2(1);
ALTER TABLE REPORT.TERMINAL ADD (SALES_ORDER_NUMBER VARCHAR2(50), PURCHASE_ORDER_NUMBER VARCHAR2(50));
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/CORP/REFUND_PKG.pbk?rev=1.18
CREATE OR REPLACE PACKAGE BODY CORP.REFUND_PKG IS
-- Purpose: Briefly explain the functionality of the package body
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------
-- Noah S.     6/15/05 Creation

   /*
   Function REFUND_PKG.issue_refund

   Creates entries in REPORT.Refund and REPORT.Trans to represent
   the issued refund.

   Return Values:
         0: Success (Normal)
         1: Successful Manager Override
        -1: Failure (General)
        -2: Failed Manager Override
   */
   FUNCTION issue_refund(
        l_trans_id REPORT.TRANS.TRAN_ID%TYPE,
        l_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        l_amount REPORT.TRANS.TOTAL_AMOUNT%TYPE,
        l_reason_id CORP.REFUND_REASON.REFUND_REASON_ID%TYPE,
        l_comment CORP.REFUND.COMMENT_TEXT%TYPE,
        l_override CORP.REFUND.MANAGER_OVERRIDE_FLAG%TYPE
   ) RETURN INT IS
        l_refund_trans_id REPORT.TRANS.TRAN_ID%TYPE;
        l_machine_trans_no REPORT.TRANS.MACHINE_TRANS_NO%TYPE;
        l_refund_id CORP.REFUND.REFUND_ID%TYPE;
        l_refund_desc REPORT.TRANS.DESCRIPTION%TYPE;
        l_prev_total REPORT.TRANS.TOTAL_AMOUNT%TYPE;
        l_trans_total REPORT.TRANS.TOTAL_AMOUNT%TYPE;
        l_return_val INT;
        l_num PLS_INTEGER;
        l_upload_date REPORT.TRANS.SERVER_DATE%TYPE;
        l_max_refund_days INTEGER;
   BEGIN
        l_return_val := -1; --'Normal Failure'
        LOCK TABLE CORP.REFUND IN EXCLUSIVE MODE;
        BEGIN
            SELECT NVL(SUM(T.TOTAL_AMOUNT), 0), NVL(SUM(DECODE(T.TRANS_TYPE_ID, 21, 0, 1)), 0) + 1
              INTO l_prev_total, l_num
              FROM REPORT.TRANS T
			  JOIN REPORT.TRANS_TYPE TT ON T.TRANS_TYPE_ID = TT.TRANS_TYPE_ID
             WHERE T.ORIG_TRAN_ID = l_trans_id
               AND TT.REFUND_IND = 'Y';
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                l_prev_total := 0;
                l_num := 1;
            WHEN OTHERS THEN
                RAISE;
        END;
        SELECT
            REPORT.TRANS_SEQ.NEXTVAL,
            CORP.SEQ_REFUND_ID.NEXTVAL,
            'RF:' || SUBSTR(x.MACHINE_TRANS_NO,
                INSTR(x.MACHINE_TRANS_NO, ':') + 1, 100) || ':R'
                || TO_CHAR(l_num, 'FM9999999990'),
            rr.REFUND_REASON,
            x.TOTAL_AMOUNT,
            X.SERVER_DATE,
            NVL(TO_NUMBER_OR_NULL(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('MAX_REFUND_DAYS')), 90)
          INTO
            l_refund_trans_id,
            l_refund_id,
            l_machine_trans_no,
            l_refund_desc,
            l_trans_total,
            l_upload_date,
            l_max_refund_days
          FROM REPORT.TRANS x, CORP.REFUND_REASON rr
         WHERE x.TRAN_ID = l_trans_id
           AND rr.refund_reason_id = l_reason_id;

        IF l_upload_date < SYSDATE - l_max_refund_days THEN
            RAISE_APPLICATION_ERROR(-20801, 'Transaction '||TO_CHAR(l_trans_id)||' was uploaded '||TO_CHAR(l_upload_date, 'MM/DD/YYYY')||' and is no longer eligible for a refund');
        ELSIF (((ABS(l_prev_total) + ABS(l_amount)) <= l_trans_total) OR (l_override = 'Y')) THEN
            DECLARE
                l_date DATE := SYSDATE;
            BEGIN
            INSERT INTO REPORT.TRANS(tran_id, card_number, total_amount, cc_appr_code,
                start_date, close_date, server_date, settle_state_id, eport_id,
                trans_type_id, orig_tran_id, terminal_id, merchant_id,
                source_system_cd, machine_trans_no, customer_bank_id,
                process_fee_id, create_date, description, currency_id, consumer_acct_id)
                (SELECT
                    l_refund_trans_id, orig.card_number, -ABS(l_amount), 'PENDING',
                    l_date, l_date, l_date, 1, orig.eport_id, 20, l_trans_id,
                    orig.terminal_id, orig.merchant_id, 'RA', l_machine_trans_no,
                    orig.customer_bank_id, orig.process_fee_id, l_date,
                    l_refund_desc, orig.currency_id, orig.consumer_acct_id
                FROM REPORT.TRANS orig WHERE orig.tran_id = l_trans_id);

            INSERT INTO CORP.REFUND(refund_id, creator_user_id, tran_id, reason_id,
                comment_text, processed_flag, manager_override_flag, close_date) VALUES
                (l_refund_id, l_user_id, l_refund_trans_id, l_reason_id, l_comment,
                'N', l_override, l_date);
            END;
            l_return_val := 0; --'Normal Success'
        END IF;

        IF ((ABS(l_prev_total) + ABS(l_amount)) > l_trans_total) THEN
            IF (l_override = 'Y') THEN l_return_val := 1; --'Override Success'
            ELSE l_return_val := -2; --'Override Failure'
            END IF;
        END IF;
        RETURN l_return_val;
   END;

   /*
   Function REFUND_PKG.issue_chargeback

   Creates entries in CORP.Chargeback and REPORT.Trans to represent
   the issued chargeback.

   Return Values:
         0: Success
        -1: Failure
   */
   FUNCTION issue_chargeback(
        l_trans_id REPORT.TRANS.TRAN_ID%TYPE,
        l_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        l_amount REPORT.TRANS.TOTAL_AMOUNT%TYPE,
        l_fee REPORT.TRANS.TOTAL_AMOUNT%TYPE,
        l_comment CORP.REFUND.COMMENT_TEXT%TYPE,
        l_override CORP.REFUND.MANAGER_OVERRIDE_FLAG%TYPE
   ) RETURN INT IS
        l_chargeback_trans_id REPORT.TRANS.TRAN_ID%TYPE;
        l_machine_trans_no REPORT.TRANS.MACHINE_TRANS_NO%TYPE;
        l_chargeback_id CORP.CHARGEBACK.CHARGEBACK_ID%TYPE;
        l_return_val INT;
        l_card_assoc_id CORP.CARD_ASSOC.CARD_ASSOC_ID%TYPE;
        l_card_assoc_name CORP.CARD_ASSOC.CARD_ASSOC_NAME%TYPE;
        l_prev_total REPORT.TRANS.TOTAL_AMOUNT%TYPE;
        l_trans_total REPORT.TRANS.TOTAL_AMOUNT%TYPE;
        l_total_chargeback_amt REPORT.TRANS.TOTAL_AMOUNT%TYPE;
        l_num PLS_INTEGER;
        l_date DATE := SYSDATE;
   BEGIN
        l_return_val := -1; --'Normal Failure'
        l_total_chargeback_amt := l_amount + NVL(l_fee,0);
        LOCK TABLE CORP.CHARGEBACK IN EXCLUSIVE MODE;

        BEGIN
            SELECT NVL(SUM(T.TOTAL_AMOUNT), 0), NVL(SUM(DECODE(T.TRANS_TYPE_ID, 21, 1, 0)), 0) + 1
              INTO l_prev_total, l_num
              FROM REPORT.TRANS T
			  JOIN REPORT.TRANS_TYPE TT ON T.TRANS_TYPE_ID = TT.TRANS_TYPE_ID
             WHERE T.ORIG_TRAN_ID = l_trans_id
               AND TT.REFUND_IND = 'Y';
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                l_prev_total := 0;
                l_num := 1;
            WHEN OTHERS THEN
                RAISE;
        END;
        SELECT
            REPORT.TRANS_SEQ.NEXTVAL,
            CORP.SEQ_CHARGEBACK_ID.NEXTVAL,
            'RF:' || SUBSTR(x.MACHINE_TRANS_NO,
                INSTR(x.MACHINE_TRANS_NO, ':') + 1, 100) || ':C'
                || TO_CHAR(l_num, 'FM9999999990'),
            x.TOTAL_AMOUNT
          INTO
            l_chargeback_trans_id,
            l_chargeback_id,
            l_machine_trans_no,
            l_trans_total
          FROM REPORT.TRANS x
         WHERE x.TRAN_ID = l_trans_id;

        IF (((ABS(l_prev_total) + ABS(l_total_chargeback_amt)) <= l_trans_total) OR (l_override = 'Y')) THEN
        	INSERT INTO REPORT.TRANS(
            	tran_id, 
            	card_number, 
            	total_amount, 
            	cc_appr_code,
                start_date, 
                close_date, 
                server_date, 
                settle_state_id, 
                eport_id,
                trans_type_id, 
                orig_tran_id, 
                terminal_id, 
                merchant_id,
                source_system_cd, 
                machine_trans_no, 
                customer_bank_id,
                process_fee_id, 
                create_date, 
                settle_date, 
                description, 
                currency_id,
                consumer_acct_id)
			SELECT
            	l_chargeback_trans_id, 
            	orig.CARD_NUMBER, 
            	-ABS(l_total_chargeback_amt), 
            	'SETTLED',
                l_date, 
                l_date, 
                l_date, 
                3, 
                orig.EPORT_ID, 
                21, 
                l_trans_id,
                orig.TERMINAL_ID, 
                orig.MERCHANT_ID, 
                'RA', 
                l_machine_trans_no,
                orig.CUSTOMER_BANK_ID, 
                orig.PROCESS_FEE_ID, 
                l_date, 
                l_date,
				REPORT.CARD_NAME(orig.CARDTYPE_AUTHORITY_ID, orig.TRANS_TYPE_ID, orig.CARD_NUMBER) || ' issued chargeback ' || 
                	orig.CC_APPR_CODE || ' ' || TO_CHAR(orig.start_date, 'MM/DD/yyyy') || 
                	' Tran #: ' || orig.TRAN_ID || ' Amt: ' || curr.CURRENCY_SYMBOL || 
                	TO_CHAR(l_amount,'FM9,999,990.00') || ' ' || curr.CURRENCY_CODE || ' Fee: ' 
                	|| curr.CURRENCY_SYMBOL || TO_CHAR(NVL(l_fee,0),'FM9,999,990.00') 
                	|| ' ' || curr.CURRENCY_CODE,
                orig.CURRENCY_ID,
                orig.CONSUMER_ACCT_ID
			FROM REPORT.TRANS orig 
                INNER JOIN CORP.CURRENCY curr ON orig.CURRENCY_ID = curr.CURRENCY_ID
                WHERE orig.TRAN_ID = l_trans_id;

            INSERT INTO CORP.CHARGEBACK(
            	chargeback_id, 
            	creator_user_id, 
            	tran_id, 
            	card_assoc_id, 
            	comment_text, 
            	manager_override_flag)
            SELECT
           		l_chargeback_id, 
           		l_user_id, 
           		l_chargeback_trans_id, 
           		(SELECT CA.CARD_ASSOC_ID 
				   FROM CORP.CARD_ASSOC CA, REPORT.TRANS X
				  WHERE CA.CARD_ASSOC_NAME = REPORT.CARD_COMPANY(x.CARD_NUMBER)
				    AND x.TRAN_ID = l_trans_id), 
           		l_comment, 
           		l_override
           	FROM DUAL;
            l_return_val := 0; --'Normal Success'
        END IF;
        
        IF ((ABS(l_prev_total) + ABS(l_total_chargeback_amt)) > l_trans_total) THEN
            IF (l_override = 'Y') THEN l_return_val := 1; --'Override Success'
            ELSE l_return_val := -2; --'Override Failure'
            END IF;
        END IF;

        RETURN l_return_val;
   END;
   
   PROCEDURE issue_refund_with_remains(
        l_trans_id REPORT.TRANS.TRAN_ID%TYPE,
        l_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        l_amount REPORT.TRANS.TOTAL_AMOUNT%TYPE,
        l_reason_id CORP.REFUND_REASON.REFUND_REASON_ID%TYPE,
        l_comment CORP.REFUND.COMMENT_TEXT%TYPE,
        l_override CORP.REFUND.MANAGER_OVERRIDE_FLAG%TYPE,
        l_has_more CHAR,
        l_return_val OUT INT,
        l_remaining_amount OUT REPORT.TRANS.total_amount%TYPE
   )IS
        l_refund_trans_id REPORT.TRANS.TRAN_ID%TYPE;
        l_machine_trans_no REPORT.TRANS.MACHINE_TRANS_NO%TYPE;
        l_refund_id CORP.REFUND.REFUND_ID%TYPE;
        l_refund_desc REPORT.TRANS.DESCRIPTION%TYPE;
        l_prev_total REPORT.TRANS.TOTAL_AMOUNT%TYPE;
        l_trans_total REPORT.TRANS.TOTAL_AMOUNT%TYPE;
        l_num PLS_INTEGER;
        l_lock VARCHAR2(128);
        l_update CHAR:='N';
        l_refund_amount REPORT.TRANS.TOTAL_AMOUNT%TYPE;
        l_upload_date REPORT.TRANS.SERVER_DATE%TYPE;
        l_max_refund_days INTEGER;
   BEGIN
        l_return_val := -1; --'Normal Failure'
        l_lock := GLOBALS_PKG.REQUEST_LOCK('REPORT.TRANS.TRAN_ID', l_trans_id);
        l_lock := GLOBALS_PKG.REQUEST_LOCK('CORP.REFUND.TRAN_ID', l_trans_id);
        BEGIN
            SELECT NVL(SUM(T.TOTAL_AMOUNT), 0), NVL(SUM(DECODE(T.TRANS_TYPE_ID, 21, 0, 1)), 0) + 1
              INTO l_prev_total, l_num
              FROM REPORT.TRANS T
			  JOIN REPORT.TRANS_TYPE TT ON T.TRANS_TYPE_ID = TT.TRANS_TYPE_ID
             WHERE T.ORIG_TRAN_ID = l_trans_id
               AND TT.REFUND_IND = 'Y';
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                l_prev_total := 0;
                l_num := 1;
            WHEN OTHERS THEN
                RAISE;
        END;
        SELECT
            REPORT.TRANS_SEQ.NEXTVAL,
            CORP.SEQ_REFUND_ID.NEXTVAL,
            'RF:' || SUBSTR(x.MACHINE_TRANS_NO,
                INSTR(x.MACHINE_TRANS_NO, ':') + 1, 100) || ':R'
                || TO_CHAR(l_num, 'FM9999999990'),
            rr.REFUND_REASON,
            x.TOTAL_AMOUNT,
            X.SERVER_DATE,
            NVL(TO_NUMBER_OR_NULL(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('MAX_REFUND_DAYS')), 90)
          INTO
            l_refund_trans_id,
            l_refund_id,
            l_machine_trans_no,
            l_refund_desc,
            l_trans_total,
            l_upload_date,
            l_max_refund_days
          FROM REPORT.TRANS x, CORP.REFUND_REASON rr
         WHERE x.TRAN_ID = l_trans_id
           AND rr.refund_reason_id = l_reason_id;
	
        IF l_upload_date < SYSDATE - l_max_refund_days THEN
            RAISE_APPLICATION_ERROR(-20801, 'Transaction '||TO_CHAR(l_trans_id)||' was uploaded '||TO_CHAR(l_upload_date, 'MM/DD/YYYY')||' and is no longer eligible for a refund');
        ELSIF ((ABS(l_prev_total) + ABS(l_amount)) <= l_trans_total) THEN
          l_update:='Y';
          l_refund_amount:=l_amount;
          l_remaining_amount:=0;  
          l_return_val:=0;
        ELSIF l_has_more = 'Y' THEN
          IF l_trans_total-ABS(l_prev_total) > 0 THEN
            l_update:='Y';
            l_refund_amount:=l_trans_total-ABS(l_prev_total);
            l_remaining_amount:=l_amount-l_refund_amount;
            l_return_val:=2;--'This transaction is refunded the diff'
          ELSE
            l_remaining_amount:=l_amount;
            l_return_val:=3; --'This transaction is already refunded'
          END IF;
        ELSE
          IF (l_override = 'Y') THEN 
            l_update:='Y';
            l_refund_amount:=l_amount;
            l_remaining_amount:=0;
            l_return_val := 1; --'Override Success'
          ELSE 
            l_remaining_amount:=l_amount;
            l_return_val := -2; --'Override Failure'
          END IF;		
        END IF;

        IF l_update = 'Y' THEN
            DECLARE
                l_date DATE := SYSDATE;
            BEGIN
            INSERT INTO REPORT.TRANS(tran_id, card_number, total_amount, cc_appr_code,
                start_date, close_date, server_date, settle_state_id, eport_id,
                trans_type_id, orig_tran_id, terminal_id, merchant_id,
                source_system_cd, machine_trans_no, customer_bank_id,
                process_fee_id, create_date, description, currency_id, consumer_acct_id)
                (SELECT
                    l_refund_trans_id, orig.card_number, -ABS(l_refund_amount), 'PENDING',
                    l_date, l_date, l_date, 1, orig.eport_id, 20, l_trans_id,
                    orig.terminal_id, orig.merchant_id, 'RA', l_machine_trans_no,
                    orig.customer_bank_id, orig.process_fee_id, l_date,
                    l_refund_desc, orig.currency_id, orig.consumer_acct_id
                FROM REPORT.TRANS orig WHERE orig.tran_id = l_trans_id);

            INSERT INTO CORP.REFUND(refund_id, creator_user_id, tran_id, reason_id,
                comment_text, processed_flag, manager_override_flag, close_date) VALUES
                (l_refund_id, l_user_id, l_refund_trans_id, l_reason_id, l_comment,
                'N', l_override, l_date);
            END;
      END IF;
   END;
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/CORP/PAYMENTS_PKG.psk?rev=1.20
CREATE OR REPLACE PACKAGE CORP.PAYMENTS_PKG IS
--
-- Holds all the procedures and functions related to EFTs and payments
--
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------
-- B Krug       09-16-04  NEW

   /*PROCEDURE AUTO_CREATE_EFTS;
   PROCEDURE CREATE_EFT(
        l_cust_bank_id CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        l_user_id IN EFT.CREATE_BY%TYPE,
        l_check_min CHAR DEFAULT 'N');

   PROCEDURE REFRESH_PENDING_REVENUE;
   */
   CHILD_RECORD_FOUND EXCEPTION;
   PRAGMA EXCEPTION_INIT(CHILD_RECORD_FOUND, -2292);

   TRANS_BATCH_CLOSED EXCEPTION;
   PRAGMA EXCEPTION_INIT(TRANS_BATCH_CLOSED, -20701);

   FUNCTION GET_UNPAID_TRANS_AND_FEES (
        l_as_of DATE
    ) RETURN GLOBALS_PKG.REF_CURSOR;
   /*
   FUNCTION FORMAT_EFT_REASON(
        l_eft_id EFT.EFT_ID%TYPE
     ) RETURN VARCHAR;
     */
   FUNCTION FORMAT_EFT_REASON(
        l_payment_sched_id BATCH.PAYMENT_SCHEDULE_ID%TYPE,
        l_min_pay_date BATCH.START_DATE%TYPE,
        l_max_pay_date BATCH.END_DATE%TYPE
     ) RETURN VARCHAR;
     
    FUNCTION BATCH_CLOSABLE(
        l_batch_id BATCH.BATCH_ID%TYPE)
     RETURN CHAR
     PARALLEL_ENABLE;
     
    FUNCTION BATCH_CLOSABLE(
        l_batch_id BATCH.BATCH_ID%TYPE,
        l_pay_sched BATCH.PAYMENT_SCHEDULE_ID%TYPE,
        l_end_date BATCH.END_DATE%TYPE,
        l_batch_state_cd BATCH.BATCH_STATE_CD%TYPE)
     RETURN CHAR
     PARALLEL_ENABLE;

   /*
   FUNCTION GET_NOT_PAYABLE_COUNT(
        l_terminal_id TERMINAL.TERMINAL_ID%TYPE,
        l_cust_bank_id CUSTOMER_BANK.CUSTOMER_BANK_id%TYPE,
        l_close_date LEDGER.CLOSE_DATE%TYPE)
     RETURN NUMBER
     PARALLEL_ENABLE;
*/

  PROCEDURE UPDATE_LEDGER(
        l_trans_id    LEDGER.TRANS_ID%TYPE,
        l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE,
        l_close_date    LEDGER.ENTRY_DATE%TYPE,
        l_settle_date LEDGER.LEDGER_DATE%TYPE,
        l_total_amount LEDGER.AMOUNT%TYPE,
        l_settle_state_id LEDGER.SETTLE_STATE_ID%TYPE,
        l_terminal_id   BATCH.TERMINAL_ID%TYPE,
        l_customer_bank_id  DOC.CUSTOMER_BANK_ID%TYPE,
        l_process_fee_id    LEDGER.PROCESS_FEE_ID%TYPE,
        l_currency_id       DOC.CURRENCY_ID%TYPE);

  PROCEDURE LOCK_DOC(
        l_doc_id DOC.DOC_ID%TYPE,
        l_user_id IN DOC.CREATE_BY%TYPE);

  FUNCTION GET_OR_CREATE_BATCH(
        l_terminal_id  BATCH.TERMINAL_ID%TYPE,
        l_cust_bank_id DOC.CUSTOMER_BANK_ID%TYPE,
        l_entry_date LEDGER.ENTRY_DATE%TYPE,
        l_currency_id DOC.CURRENCY_ID%TYPE,
        l_always_as_accum CHAR
    )
     RETURN BATCH.BATCH_ID%TYPE;
     
  PROCEDURE      DELAY_REFUND_BY_TRAN_ID (
      l_trans_id IN LEDGER.TRANS_ID%TYPE);
      
  PROCEDURE      DELAY_ENTRY (
      l_ledger_id IN LEDGER.LEDGER_ID%TYPE);
      
  PROCEDURE      DELETE_ENTRY (
      l_ledger_id IN LEDGER.LEDGER_ID%TYPE);
      
  PROCEDURE ADJUSTMENT_INS (
        l_ledger_id OUT NOCOPY LEDGER.LEDGER_ID%TYPE,
       	l_doc_id IN BATCH.DOC_ID%TYPE,
        l_terminal_id IN BATCH.TERMINAL_ID%TYPE,
    	l_reason IN LEDGER.DESCRIPTION%TYPE,
    	l_amt IN LEDGER.AMOUNT%TYPE,
    	l_user_id IN LEDGER.CREATE_BY%TYPE,
		lc_split_payment_flag IN CHAR,
		lc_split_payment_interval_cd IN CHAR,
		ln_split_number_of_payments IN INTEGER,
		ld_first_split_payment_date IN DATE
	);
	  
  PROCEDURE ADJUSTMENT_INS (
        l_ledger_id OUT NOCOPY LEDGER.LEDGER_ID%TYPE,
       	l_doc_id IN BATCH.DOC_ID%TYPE,
        l_terminal_id IN BATCH.TERMINAL_ID%TYPE,
    	l_reason IN LEDGER.DESCRIPTION%TYPE,
    	l_amt IN LEDGER.AMOUNT%TYPE,
    	l_user_id IN LEDGER.CREATE_BY%TYPE
	);
		
PROCEDURE ADJUSTMENT_INS (
	pn_customer_id CORP.CUSTOMER.CUSTOMER_ID%TYPE,
	pv_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
	pv_currency_cd CORP.CURRENCY.CURRENCY_CODE%TYPE,
	pv_reason CORP.LEDGER.DESCRIPTION%TYPE,
	pn_amount CORP.LEDGER.AMOUNT%TYPE,
	pn_doc_id OUT CORP.DOC.DOC_ID%TYPE,
	pn_ledger_id OUT CORP.LEDGER.LEDGER_ID%TYPE
);
    	
  PROCEDURE                             ADJUSTMENT_UPD
       (l_ledger_id IN LEDGER.LEDGER_ID%TYPE,
       	l_reason IN LEDGER.DESCRIPTION%TYPE,
       	l_amt IN LEDGER.AMOUNT%TYPE,
    	l_user_id IN LEDGER.CREATE_BY%TYPE);
    	
  PROCEDURE      APPROVE_PAYMENT (
      l_doc_id IN DOC.DOC_ID%TYPE,
      l_user_id IN DOC.APPROVE_BY%TYPE);
      
  PROCEDURE      MARK_DOC_PAID (
      l_doc_id IN DOC.DOC_ID%TYPE,
      l_user_id IN DOC.APPROVE_BY%TYPE);
      
  PROCEDURE PROCESS_FEES_UPD
       (l_terminal_id IN PROCESS_FEES.TERMINAL_ID%TYPE,
        l_trans_type_id IN PROCESS_FEES.TRANS_TYPE_ID%TYPE,
        l_fee_percent IN PROCESS_FEES.FEE_PERCENT%TYPE,
        l_fee_amount IN PROCESS_FEES.FEE_AMOUNT%TYPE,
        l_fee_minimum IN PROCESS_FEES.MIN_AMOUNT%TYPE,
        l_effective_date IN PROCESS_FEES.END_DATE%TYPE DEFAULT SYSDATE,
        l_override IN CHAR DEFAULT 'N');
        
  PROCEDURE SCAN_FOR_SERVICE_FEES;
  
  PROCEDURE SERVICE_FEES_UPD(
        l_terminal_id IN SERVICE_FEES.TERMINAL_ID%TYPE,
        l_fee_id IN SERVICE_FEES.FEE_ID%TYPE,
        l_freq_id IN SERVICE_FEES.FREQUENCY_ID%TYPE,
        l_fee_amt IN SERVICE_FEES.FEE_AMOUNT%TYPE,
        l_fee_perc IN SERVICE_FEES.FEE_PERCENT%TYPE,
        l_effective_date IN SERVICE_FEES.END_DATE%TYPE DEFAULT SYSDATE,
        l_end_date IN SERVICE_FEES.END_DATE%TYPE,
        l_override IN CHAR DEFAULT 'N',
        pn_grace_days IN NUMBER DEFAULT 60,
		pc_no_trigger_event_flag IN SERVICE_FEES.NO_TRIGGER_EVENT_FLAG%TYPE DEFAULT NULL);
        
  PROCEDURE UNLOCK_DOC (
      l_doc_id IN DOC.DOC_ID%TYPE);
      
  FUNCTION GET_NEW_BATCH(
        l_batch_id BATCH.BATCH_ID%TYPE,
        l_entry_date LEDGER.ENTRY_DATE%TYPE)
     RETURN BATCH.BATCH_ID%TYPE;
     
  FUNCTION ENTRY_PAYABLE(
        l_settle_state LEDGER.SETTLE_STATE_ID%TYPE,
        l_entry_type LEDGER.ENTRY_TYPE%TYPE)
     RETURN CHAR
     DETERMINISTIC
     PARALLEL_ENABLE;

  PROCEDURE UPDATE_FILL_BATCH(
        l_fill_id   FILL.FILL_ID%TYPE);
        
  PROCEDURE CHECK_TRANS_CLOSED(
        l_trans_id    CORP.LEDGER.TRANS_ID%TYPE)
  PARALLEL_ENABLE;

  PROCEDURE UPDATE_SETTLEMENT(
        l_trans_id    CORP.LEDGER.TRANS_ID%TYPE,
        l_settle_state_id CORP.LEDGER.SETTLE_STATE_ID%TYPE,
        l_settle_date   CORP.LEDGER.LEDGER_DATE%TYPE);
        
  PROCEDURE CHECK_TRANS_WITH_PF_CLOSED(
        l_process_fee_id    CORP.LEDGER.PROCESS_FEE_ID%TYPE)
  PARALLEL_ENABLE;
        
  PROCEDURE UPDATE_PROCESS_FEE_VALUES(
        l_process_fee_id CORP.LEDGER.PROCESS_FEE_ID%TYPE);
        
  PROCEDURE      UNAPPROVE_PAYMENT (
      l_doc_id IN DOC.DOC_ID%TYPE);
      
  FUNCTION GET_SERVICE_FEE_DATE_FMT(
        l_months FREQUENCY.MONTHS%TYPE,
        l_days FREQUENCY.DAYS%TYPE)
     RETURN VARCHAR
     DETERMINISTIC
     PARALLEL_ENABLE;
     
  FUNCTION GET_TRANS_ADJ_DESC(
        l_trans_id LEDGER.TRANS_ID%TYPE,
        l_entry_type LEDGER.ENTRY_TYPE%TYPE,
        l_entry_date LEDGER.ENTRY_DATE%TYPE,
        l_terminal_id BATCH.TERMINAL_ID%TYPE,
        l_customer_bank_id DOC.CUSTOMER_BANK_ID%TYPE)
     RETURN LEDGER.DESCRIPTION%TYPE
     PARALLEL_ENABLE;
     
  FUNCTION GET_OR_CREATE_DOC(
        l_cust_bank_id DOC.CUSTOMER_BANK_ID%TYPE,
        l_currency_id DOC.CURRENCY_ID%TYPE,
        l_business_unit DOC.BUSINESS_UNIT_ID%TYPE
    )
     RETURN DOC.DOC_ID%TYPE;
     
  PROCEDURE UPDATE_LEDGER(
              l_tran_id LEDGER.TRANS_ID%TYPE);
              
  PROCEDURE ADD_ACTIVATION_FEE(
        l_terminal_id CORP.BATCH.TERMINAL_ID%TYPE,
        l_fee_amt CORP.LEDGER.AMOUNT%TYPE,
        l_activation_date  CORP.LEDGER.ENTRY_DATE%TYPE);
        
  PROCEDURE UPDATE_EXPORT_BATCH(
        l_export_id   REPORT.EXPORT_BATCH.BATCH_ID%TYPE);
        
  PROCEDURE SWITCH_PAYMENT_SCHEDULE(
        l_tran_id CORP.LEDGER.TRANS_ID%TYPE,
        l_payment_schedule_id  CORP.BATCH.PAYMENT_SCHEDULE_ID%TYPE);
        
  PROCEDURE CHECK_FILL_BATCH_COMPLETE(
        l_batch_id BATCH.BATCH_ID%TYPE);

  PROCEDURE START_EFT_PROPAGATION;		
		
  PROCEDURE START_EFT_PROCESSING(
	PN_DOC_ID CORP.DOC.DOC_ID%TYPE);
		
  PROCEDURE PROCESS_EFT(
	PN_DOC_ID CORP.DOC.DOC_ID%TYPE);
	
  PROCEDURE COMPLETE_EFT_PROPAGATION;
END; -- Package spec

/
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/CORP/PAYMENTS_PKG.pbk?rev=1.114
CREATE OR REPLACE PACKAGE BODY CORP.PAYMENTS_PKG IS
--
-- Holds all the procedures and functions related to EFTs and payments
--
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------
-- B Krug       09-16-04  NEW
-- B Krug       09-16-04  Moved CREATE_PAYMENT_FOR_ACCOUNT into this package to
--                        take advantage of other procs in this package
-- B Krug       10-05-04  Added Ledger sync procs (and batch-related stuff)
-- B Krug       01-31-08  Added Batch Confirmation Logic

    -- Returns 'Y' if an entry is payable (it's been settled)
    -- Returns 'N' if an entry is not payable
    -- Returns '?' if an entry has not been processed
    FUNCTION ENTRY_PAYABLE(
        l_settle_state LEDGER.SETTLE_STATE_ID%TYPE,
        l_entry_type LEDGER.ENTRY_TYPE%TYPE)
     RETURN CHAR
    IS
    BEGIN
        IF l_settle_state IN(2,3,6) THEN
            RETURN 'Y';
        /*ELSIF l_entry_type IN('CB','RF','SF') THEN
            RETURN 'Y';
        */ELSIF l_settle_state IN(5) THEN
            RETURN 'N';
        ELSE
            RETURN '?';
        END IF;
    END;
    
    -- Gets the batch_id or creates one if necessary
    FUNCTION GET_OR_CREATE_DOC(
        l_cust_bank_id DOC.CUSTOMER_BANK_ID%TYPE,
        l_currency_id DOC.CURRENCY_ID%TYPE,
        l_business_unit DOC.BUSINESS_UNIT_ID%TYPE
     )
     RETURN DOC.DOC_ID%TYPE
    IS
        l_doc_id DOC.DOC_ID%TYPE;
        l_batch_ref DOC.REF_NBR%TYPE;
        l_lock VARCHAR2(128);
    BEGIN
        -- lock this bank id until commit to ensure the the doc stays open until the ledger entry is set with it
        l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_cust_bank_id);
        SELECT MAX(D.DOC_ID) -- just to be safe in case there is more than one
          INTO l_doc_id
          FROM DOC D
         WHERE D.CUSTOMER_BANK_ID = l_cust_bank_id
           AND NVL(D.CURRENCY_ID, 0) = NVL(l_currency_id, 0)
           AND NVL(D.BUSINESS_UNIT_ID, 0) = NVL(l_business_unit, 0)
           AND D.STATUS = 'O';
        IF l_doc_id IS NULL THEN
            -- create new doc record
            SELECT EFT_SEQ.NEXTVAL, TO_CHAR(EFT_BATCH_SEQ.NEXTVAL, 'FM0000999999')
              INTO l_doc_id, l_batch_ref
              FROM DUAL;
            INSERT INTO DOC(
                DOC_ID,
                DOC_TYPE,
                REF_NBR,
                DESCRIPTION,
                CUSTOMER_BANK_ID,
                CURRENCY_ID,
                BUSINESS_UNIT_ID,
                BANK_ACCT_NBR,
                BANK_ROUTING_NBR,
                STATUS)
              SELECT
                l_doc_id,
                '--',
                l_batch_ref,
                NVL(EFT_PREFIX, 'USAT: ') || l_batch_ref,
                CUSTOMER_BANK_ID,
                DECODE(l_currency_id, 0, NULL, l_currency_id),
                DECODE(l_business_unit, 0, NULL, l_business_unit),
                BANK_ACCT_NBR,
                BANK_ROUTING_NBR,
                'O'
              FROM CUSTOMER_BANK
              WHERE CUSTOMER_BANK_ID = l_cust_bank_id;
        END IF;
        RETURN l_doc_id;
    END;

    -- Gets the batch_id or creates one if necessary
    FUNCTION GET_OR_CREATE_BATCH(
        l_terminal_id  BATCH.TERMINAL_ID%TYPE,
        l_cust_bank_id DOC.CUSTOMER_BANK_ID%TYPE,
        l_entry_date LEDGER.ENTRY_DATE%TYPE,
        l_currency_id DOC.CURRENCY_ID%TYPE,
        l_pay_sched TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
        l_business_unit CORP.BUSINESS_UNIT.BUSINESS_UNIT_ID%TYPE,
        l_always_as_accum CHAR
    )
     RETURN BATCH.BATCH_ID%TYPE
    IS
        l_batch_id BATCH.BATCH_ID%TYPE;
        l_doc_id DOC.DOC_ID%TYPE;
        l_start_date BATCH.START_DATE%TYPE;
        l_end_date BATCH.END_DATE%TYPE;
        l_batch_state_cd BATCH.BATCH_STATE_CD%TYPE;
        l_start_fill_id BATCH_FILL.START_FILL_ID%TYPE;
        l_end_fill_id BATCH_FILL.END_FILL_ID%TYPE;
        l_counters_must_show CHAR(1);
        l_lock VARCHAR2(128);
    BEGIN
        -- calculate batch (and create if necessary)
        -- Get an "Open" doc record
        l_doc_id := GET_OR_CREATE_DOC(l_cust_bank_id, l_currency_id, l_business_unit);
        --l_lock := GLOBALS_PKG.REQUEST_LOCK('BATCH.TERMINAL_ID',l_terminal_id); -- this may not be necessary since we lock the doc on customer_bank_id
        SELECT MAX(B.BATCH_ID) -- just to be safe in case there is more than one
          INTO l_batch_id
          FROM BATCH B
         WHERE B.DOC_ID = l_doc_id
           AND B.TERMINAL_ID = l_terminal_id
           AND B.PAYMENT_SCHEDULE_ID = l_pay_sched
           AND B.BATCH_STATE_CD IN('O', 'L') -- Open or Closeable only
           -- for "As Accumulated", batch start date does not matter
           AND (B.START_DATE <= l_entry_date OR l_pay_sched IN(1,5,8))
           AND (NVL(B.END_DATE, MAX_DATE) > l_entry_date OR (B.END_DATE = l_entry_date AND l_pay_sched IN(8)))
           AND (l_pay_sched NOT IN(8) OR DECODE(B.END_DATE, NULL, 0, 1) = DECODE(l_always_as_accum, 'E', 1, 0));
        IF l_batch_id IS NULL THEN
            -- calc start and end dates
            IF l_pay_sched IN(1, 5) THEN
                SELECT LEAST(NVL(MAX(B.END_DATE), l_entry_date), l_entry_date)
                  INTO l_start_date
                  FROM BATCH B, DOC D
                 WHERE B.DOC_ID = D.DOC_ID
                   AND B.TERMINAL_ID = l_terminal_id
                   AND D.CUSTOMER_BANK_ID = l_cust_bank_id
                   AND NVL(D.CURRENCY_ID, 0) = NVL(l_currency_id, 0)
                   AND B.PAYMENT_SCHEDULE_ID = l_pay_sched;
                l_batch_state_cd := 'L'; -- Closeable
                -- end date is null
            ELSIF l_pay_sched = 2 THEN
                SELECT DECODE(MAX(BCOT.BATCH_CONFIRM_OPTION_TYPE_ID), NULL, 'N', 'Y')
                  INTO l_counters_must_show
                  FROM REPORT.TERMINAL T
                  JOIN CORP.BATCH_CONFIRM BC ON T.CUSTOMER_ID = BC.CUSTOMER_ID
                  JOIN CORP.BATCH_CONFIRM_OPTION BCO
                    ON BC.BATCH_CONFIRM_ID = BCO.BATCH_CONFIRM_ID
                  JOIN CORP.BATCH_CONFIRM_OPTION_TYPE BCOT ON BCO.BATCH_CONFIRM_OPTION_TYPE_ID = BCOT.BATCH_CONFIRM_OPTION_TYPE_ID
                 WHERE BC.PAYMENT_SCHEDULE_ID = l_pay_sched 
                   AND BCOT.BATCH_CONFIRM_OPTION_TYPE_CD = 'COUNTER_MUST_SHOW'                  
                   AND T.TERMINAL_ID = l_terminal_id;
                       
                SELECT NVL(MAX(FILL_DATE), MIN_DATE), MAX(FILL_ID)
                  INTO l_start_date, l_start_fill_id
                  FROM (
                    SELECT f.FILL_DATE, f.FILL_ID
                      FROM REPORT.FILL f
                      JOIN REPORT.TERMINAL_EPORT te
                        ON f.EPORT_ID = te.EPORT_ID
                       AND f.FILL_DATE >= NVL(te.START_DATE, MIN_DATE)
                       AND f.FILL_DATE < NVL(te.END_DATE, MAX_DATE)
                     WHERE f.FILL_DATE <= l_entry_date
                       AND te.TERMINAL_ID = l_terminal_id
                       AND (F.COUNTERS_DISPLAYED_FLAG = 'Y' OR l_counters_must_show = 'N')
                     ORDER BY f.FILL_DATE DESC)
                 WHERE ROWNUM = 1 ;
                 SELECT MIN(FILL_DATE), MIN(FILL_ID)
                  INTO l_end_date, l_end_fill_id
                  FROM (
                    SELECT f.FILL_DATE, f.FILL_ID
                      FROM REPORT.FILL f
                      JOIN (SELECT CONNECT_BY_ROOT FILL_ID START_FILL_ID, FILL_ID END_FILL_ID, LEVEL - 1 DEPTH
                              FROM REPORT.FILL
                              START WITH FILL_ID = l_start_fill_id
                              CONNECT BY NOCYCLE PRIOR FILL_ID = PREV_FILL_ID
                              ) H ON F.FILL_ID = H.END_FILL_ID
                     JOIN REPORT.TERMINAL_EPORT te
                        ON f.EPORT_ID = te.EPORT_ID
                       AND f.FILL_DATE >= NVL(te.START_DATE, MIN_DATE)
                       AND f.FILL_DATE < NVL(te.END_DATE, MAX_DATE)
                     WHERE f.FILL_DATE > l_entry_date
                       AND te.TERMINAL_ID = l_terminal_id
                       AND (F.COUNTERS_DISPLAYED_FLAG = 'Y' OR l_counters_must_show = 'N')
                     ORDER BY f.FILL_DATE ASC)
                 WHERE ROWNUM = 1;
                
                l_batch_state_cd := 'O'; -- Open
            ELSIF l_pay_sched = 8 THEN
                SELECT LEAST(NVL(MAX(B.END_DATE), l_entry_date), l_entry_date)
                  INTO l_start_date
                  FROM BATCH B, DOC D
                 WHERE B.DOC_ID = D.DOC_ID
                   AND B.TERMINAL_ID = l_terminal_id
                   AND D.CUSTOMER_BANK_ID = l_cust_bank_id
                   AND NVL(D.CURRENCY_ID, 0) = NVL(l_currency_id, 0)
                   AND B.PAYMENT_SCHEDULE_ID = l_pay_sched;
                l_batch_state_cd := 'O'; -- Open
            ELSE
                SELECT TRUNC(l_entry_date - (OFFSET_HOURS / 24), INTERVAL) + (OFFSET_HOURS / 24),
                       ADD_MONTHS(TRUNC(l_entry_date - (OFFSET_HOURS / 24), INTERVAL) + DAYS, MONTHS) + (OFFSET_HOURS / 24)
                  INTO l_start_date, l_end_date
                  FROM PAYMENT_SCHEDULE
                 WHERE PAYMENT_SCHEDULE_ID = l_pay_sched;
                IF l_end_date <= l_entry_date THEN -- trouble, should not happen
                    l_end_date := l_entry_date + (1.0/(24*60*60));
                END IF;
                l_batch_state_cd := 'O'; -- Open
            END IF;

            -- create new batch record
            SELECT BATCH_SEQ.NEXTVAL
              INTO l_batch_id
              FROM DUAL;
            INSERT INTO BATCH(
                BATCH_ID,
                DOC_ID,
                TERMINAL_ID,
                PAYMENT_SCHEDULE_ID,
                START_DATE,
                END_DATE,
                BATCH_STATE_CD)
              VALUES(
                l_batch_id,
                l_doc_id,
                l_terminal_id,
                l_pay_sched,
                l_start_date,
                l_end_date,
                l_batch_state_cd);
            IF l_pay_sched = 2 THEN
                INSERT INTO CORP.BATCH_FILL(
                    BATCH_ID,
                    START_FILL_ID,
                    END_FILL_ID)
                  VALUES(
                    l_batch_id,
                    l_start_fill_id,
                    l_end_fill_id);
            END IF;
        END IF;
        RETURN l_batch_id;
    END;
    FUNCTION GET_OR_CREATE_BATCH(
        l_terminal_id  BATCH.TERMINAL_ID%TYPE,
        l_cust_bank_id DOC.CUSTOMER_BANK_ID%TYPE,
        l_entry_date LEDGER.ENTRY_DATE%TYPE,
        l_currency_id DOC.CURRENCY_ID%TYPE,
        l_always_as_accum CHAR
    )
     RETURN BATCH.BATCH_ID%TYPE
    IS
        l_business_unit CORP.BUSINESS_UNIT.BUSINESS_UNIT_ID%TYPE;
        l_pay_sched TERMINAL.PAYMENT_SCHEDULE_ID%TYPE;
    BEGIN
        -- calculate batch (and create if necessary)
        SELECT DECODE(l_always_as_accum,
                    'Y', 1,
                    'A', 5,
                    PAYMENT_SCHEDULE_ID),
               BUSINESS_UNIT_ID
          INTO l_pay_sched, l_business_unit
          FROM TERMINAL
         WHERE TERMINAL_ID = l_terminal_id;
        RETURN GET_OR_CREATE_BATCH(l_terminal_id,l_cust_bank_id,l_entry_date,l_currency_id,l_pay_sched, l_business_unit, l_always_as_accum);
    END;
    
    FUNCTION GET_NEW_BATCH(
        l_batch_id BATCH.BATCH_ID%TYPE,
        l_entry_date LEDGER.ENTRY_DATE%TYPE)
     RETURN BATCH.BATCH_ID%TYPE
    IS
        l_cb_id DOC.CUSTOMER_BANK_ID%TYPE;
        l_curr_id DOC.CURRENCY_ID%TYPE;
        l_term_id BATCH.TERMINAL_ID%TYPE;
    BEGIN

        SELECT D.CUSTOMER_BANK_ID, D.CURRENCY_ID, B.TERMINAL_ID
          INTO l_cb_id, l_curr_id, l_term_id
          FROM BATCH B, DOC D
          WHERE D.DOC_ID = B.DOC_ID
            AND B.BATCH_ID = l_batch_id;
        RETURN GET_OR_CREATE_BATCH(l_term_id, l_cb_id, l_entry_date, l_curr_id, 'Y');
    END;
    
    FUNCTION GET_DOC_STATUS(
        l_doc_id DOC.DOC_ID%TYPE)
     RETURN DOC.STATUS%TYPE
    IS
        l_status DOC.STATUS%TYPE;
    BEGIN
        SELECT STATUS INTO l_status FROM CORP.DOC WHERE DOC_ID = l_doc_id;
        RETURN l_status;
    END;

    FUNCTION FREEZE_DOC_STATUS_LEDGER_ID(
        l_ledger_id LEDGER.LEDGER_ID%TYPE)
     RETURN DOC.STATUS%TYPE
    IS
        l_doc_id DOC.DOC_ID%TYPE;
        l_customer_bank_id CORP.DOC.CUSTOMER_BANK_ID%TYPE;
        l_lock VARCHAR2(128);
    BEGIN
        SELECT D.DOC_ID, D.CUSTOMER_BANK_ID
          INTO l_doc_id, l_customer_bank_id
          FROM CORP.DOC D
         INNER JOIN CORP.BATCH B ON D.DOC_ID = B.DOC_ID
         INNER JOIN CORP.LEDGER L ON L.BATCH_ID = B.BATCH_ID
         WHERE L.LEDGER_ID = l_ledger_id;
        l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_customer_bank_id);
        RETURN GET_DOC_STATUS(l_doc_id);
    END;
    
    FUNCTION FREEZE_DOC_STATUS_DOC_ID(
        l_doc_id DOC.DOC_ID%TYPE)
     RETURN DOC.STATUS%TYPE
    IS
        l_customer_bank_id CORP.DOC.CUSTOMER_BANK_ID%TYPE;
        l_lock VARCHAR2(128);
    BEGIN
        SELECT D.CUSTOMER_BANK_ID
          INTO l_customer_bank_id
          FROM CORP.DOC D
         WHERE D.DOC_ID = l_doc_id;
        l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_customer_bank_id);
        RETURN GET_DOC_STATUS(l_doc_id);
    END;
    
    -- Returns 'Y' if the batch is either an "As Accumulated" or has no unsettled transactions
    FUNCTION BATCH_CLOSABLE(
        l_batch_id BATCH.BATCH_ID%TYPE,
        l_pay_sched BATCH.PAYMENT_SCHEDULE_ID%TYPE,
        l_end_date BATCH.END_DATE%TYPE,
        l_batch_state_cd BATCH.BATCH_STATE_CD%TYPE)
     RETURN CHAR
    IS
        l_cnt NUMBER;
    BEGIN
        IF l_batch_state_cd IN('L', 'F') THEN
            RETURN 'Y';
        ELSIF l_batch_state_cd IN('C', 'D') THEN
            IF l_pay_sched IN(2) THEN -- Fill To Fill
                SELECT COUNT(*)
                  INTO l_cnt
                  FROM LEDGER l
                 WHERE ENTRY_PAYABLE(L.SETTLE_STATE_ID, L.ENTRY_TYPE) = '?' -- not processed
                   AND L.DELETED = 'N'
                   AND L.BATCH_ID = l_batch_id;
                IF l_cnt = 0 THEN
                    RETURN 'Y';
                ELSE
                    RETURN 'N';
                END IF;
            ELSE
                RETURN 'Y';
            END IF;
        ELSIF l_batch_state_cd IN('O') AND l_pay_sched NOT IN(1,2,5,7,8) AND l_end_date <= SYSDATE THEN
            RETURN 'Y';
        ELSE
            RETURN 'N';
        END IF;
    END;

    -- Returns 'Y' if the batch is either an "As Accumulated" or has no unsettled transactions
    FUNCTION BATCH_CLOSABLE(
        l_batch_id BATCH.BATCH_ID%TYPE)
     RETURN CHAR
    IS
        l_pay_sched BATCH.PAYMENT_SCHEDULE_ID%TYPE;
        l_end_date BATCH.END_DATE%TYPE;
        l_batch_state_cd BATCH.BATCH_STATE_CD%TYPE;
    BEGIN
        SELECT PAYMENT_SCHEDULE_ID, END_DATE, BATCH_STATE_CD
          INTO l_pay_sched, l_end_date, l_batch_state_cd
          FROM BATCH
         WHERE BATCH_ID = l_batch_id;
        RETURN BATCH_CLOSABLE(l_batch_id, l_pay_sched, l_end_date, l_batch_state_cd);
    END;

    PROCEDURE CHECK_TRANS_WITH_PF_CLOSED(
        l_process_fee_id    CORP.LEDGER.PROCESS_FEE_ID%TYPE)
    IS
        CURSOR l_check_cur IS
           SELECT L.TRANS_ID
              FROM LEDGER L, BATCH B, DOC D
             WHERE L.PROCESS_FEE_ID = l_process_fee_id
               AND L.BATCH_ID = B.BATCH_ID
               AND B.DOC_ID = D.DOC_ID
             GROUP BY L.TRANS_ID
             HAVING SUM(DECODE(D.STATUS, 'O', 0, 'D', 0, 1)) > 0
                AND SUM(DECODE(D.STATUS, 'O', 1, 0)) = 0;
        l_batch_id BATCH.BATCH_ID%TYPE;
    BEGIN
        FOR l_check_rec IN l_check_cur LOOP
            SELECT MAX(B.BATCH_ID)
              INTO l_batch_id
              FROM LEDGER L, BATCH B, DOC D
             WHERE L.TRANS_ID = l_check_rec.TRANS_ID
               AND L.BATCH_ID = B.BATCH_ID
               AND B.DOC_ID = D.DOC_ID
               AND D.STATUS NOT IN('O', 'D');
            IF l_batch_id IS NOT NULL THEN
                RAISE_APPLICATION_ERROR(-20701, 'PROCESS FEE (process_fee_id='||TO_CHAR(l_process_fee_id)||' is used in a batch (id='||TO_CHAR(l_batch_id)||') that was already closed - cannot update ledger table!');
            END IF;
        END LOOP;
    END;

    -- Checks if the specified transaction is in a closed batch and if so raises an
    -- exception
    PROCEDURE CHECK_TRANS_CLOSED(
        l_trans_id    CORP.LEDGER.TRANS_ID%TYPE)
    IS
        l_closed_cnt PLS_INTEGER;
        l_open_cnt PLS_INTEGER;
        l_batch_id BATCH.BATCH_ID%TYPE;
    BEGIN
        -- get the batch
        SELECT SUM(DECODE(D.STATUS, 'O', 0, 'D', 0, 1)),
               SUM(DECODE(D.STATUS, 'O', 1, 0))
          INTO l_closed_cnt, l_open_cnt
          FROM LEDGER L, BATCH B, DOC D
         WHERE L.TRANS_ID = l_trans_id
           AND L.BATCH_ID = B.BATCH_ID
           AND B.DOC_ID = D.DOC_ID;
        IF l_open_cnt = 0 AND l_closed_cnt > 0 THEN
            SELECT MAX(B.BATCH_ID)
              INTO l_batch_id
              FROM LEDGER L, BATCH B, DOC D
             WHERE L.TRANS_ID = l_trans_id
               AND L.BATCH_ID = B.BATCH_ID
               AND B.DOC_ID = D.DOC_ID
               AND D.STATUS NOT IN('O', 'D');
            RAISE_APPLICATION_ERROR(-20701, 'TRANSACTION (tran_id='||TO_CHAR(l_trans_id)||' is in a batch (id='||TO_CHAR(l_batch_id)||') that was already closed - cannot update ledger table!');
        END IF;
    END;
    
    PROCEDURE CHECK_FILL_BATCH_COMPLETE(
        l_batch_id BATCH.BATCH_ID%TYPE)
    IS
        l_batch_amt NUMBER;
        l_batch_tran_cnt NUMBER;
        l_src_credit_amt NUMBER;
        l_src_credit_cnt NUMBER;
        l_prev_fill_id REPORT.FILL.FILL_ID%TYPE;
        l_start_fill_id REPORT.FILL.FILL_ID%TYPE;
        l_first_tran_ts DATE;
        l_last_tran_ts DATE;
    BEGIN
        SELECT SUM(l.AMOUNT), MIN(l.ENTRY_DATE)
          INTO l_batch_amt, l_first_tran_ts
     	  FROM CORP.LEDGER l
         WHERE l.ENTRY_TYPE = 'CC'
           AND l.DELETED = 'N'
           AND l.BATCH_ID = l_batch_id;
                   
        SELECT SUM(p.AMOUNT)
          INTO l_batch_tran_cnt
          FROM REPORT.PURCHASE P
          LEFT OUTER JOIN PSS.TRAN_LINE_ITEM_TYPE XIT ON P.TRAN_LINE_ITEM_TYPE_ID = XIT.TRAN_LINE_ITEM_TYPE_ID
         WHERE P.TRAN_ID IN(
            SELECT l.TRANS_ID 
              FROM CORP.LEDGER l
             WHERE l.ENTRY_TYPE = 'CC'
               AND l.DELETED = 'N'
               AND l.BATCH_ID = l_batch_id)
           AND (XIT.TRAN_LINE_ITEM_TYPE_GROUP_CD IS NULL OR XIT.TRAN_LINE_ITEM_TYPE_GROUP_CD IN('P', 'S'));
        
        BEGIN
            SELECT SUM(HF.CREDIT_AMOUNT), SUM(HF.CREDIT_COUNT)
              INTO l_src_credit_amt, l_src_credit_cnt
              FROM CORP.BATCH_FILL BF
              JOIN (SELECT CONNECT_BY_ROOT FILL_ID ANCESTOR_FILL_ID, F.*
                      FROM REPORT.FILL F
                     START WITH F.FILL_ID = (SELECT START_FILL_ID FROM CORP.BATCH_FILL WHERE BATCH_ID = l_batch_id)
                   CONNECT BY NOCYCLE PRIOR FILL_ID = PREV_FILL_ID) HF ON BF.START_FILL_ID = HF.ANCESTOR_FILL_ID AND BF.END_FILL_ID = HF.FILL_ID
             WHERE BF.BATCH_ID = l_batch_id 
            HAVING COUNT(*) = COUNT(HF.CREDIT_AMOUNT) 
               AND COUNT(*) =  COUNT(HF.CREDIT_COUNT);
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                DECLARE
                    CURSOR l_cur IS
                        SELECT e.EPORT_SERIAL_NUM DEVICE_SERIAL_CD, 
                               GREATEST(l_first_tran_ts, NVL(te.START_DATE, MIN_DATE)) START_DATE,
                               LEAST(B.END_DATE,NVL(te.END_DATE, MAX_DATE)) END_DATE 
                          FROM CORP.BATCH B
                          JOIN REPORT.TERMINAL_EPORT te ON b.TERMINAL_ID = te.TERMINAL_ID
            	          JOIN REPORT.EPORT E ON te.EPORT_ID = e.EPORT_ID
        		         WHERE B.BATCH_ID = l_batch_id;
        		BEGIN
                    l_src_credit_amt := 0;
                    l_src_credit_cnt := 0;
                    FOR l_rec IN l_cur LOOP
                        SELECT l_src_credit_amt + NVL(SUM(T.TRAN_LINE_ITEM_AMOUNT * T.TRAN_LINE_ITEM_QUANTITY), 0), 
                               l_src_credit_cnt + NVL(SUM(DECODE(T.TRAN_LINE_ITEM_TYPE_GROUP_CD, 'U', NULL, 'I', NULL, T.TRAN_LINE_ITEM_QUANTITY)), 0)
                	      INTO l_src_credit_amt, l_src_credit_cnt                         
                          FROM PSS.VW_REPORTING_TRAN_LINE_ITEM T
                          WHERE T.DEVICE_SERIAL_CD = l_rec.DEVICE_SERIAL_CD
                            AND T.CLIENT_PAYMENT_TYPE_CD IN('C', 'R')
                            AND T.TRAN_START_TS BETWEEN l_rec.START_DATE AND l_rec.END_DATE;
                    END LOOP; 
                END;       
            WHEN OTHERS THEN
                RAISE;
        END;

        IF l_batch_amt = l_src_credit_amt AND l_batch_tran_cnt = l_src_credit_cnt THEN
            UPDATE CORP.BATCH B
               SET B.BATCH_STATE_CD = (
                    SELECT DECODE(COUNT(*), 0, 'D', 'U')
                      FROM REPORT.TERMINAL t
                      JOIN CORP.BATCH_CONFIRM CN ON t.CUSTOMER_ID = cn.CUSTOMER_ID
                     WHERE b.TERMINAL_ID = t.TERMINAL_ID
                       AND b.PAYMENT_SCHEDULE_ID = cn.PAYMENT_SCHEDULE_ID),
                   B.BATCH_CLOSED_TS = SYSDATE
              WHERE B.BATCH_ID = l_batch_id;
        END IF;	
    END;
    
    PROCEDURE ADD_BATCH_ROUNDING_ENTRY(
        l_batch_id BATCH.BATCH_ID%TYPE)
    IS
        l_round_amt LEDGER.AMOUNT%TYPE;
    BEGIN
        -- calculate rounding amount
        SELECT ROUND(SUM(AMOUNT), 2) - SUM(AMOUNT)
          INTO l_round_amt
          FROM LEDGER L
         WHERE L.DELETED = 'N'
           AND ENTRY_PAYABLE(L.SETTLE_STATE_ID,L.ENTRY_TYPE) = 'Y'
           AND L.BATCH_ID = l_batch_id;

        -- add entry if necessary
        IF l_round_amt <> 0 THEN
            INSERT INTO LEDGER(
                LEDGER_ID,
                ENTRY_TYPE,
                TRANS_ID,
                PROCESS_FEE_ID,
                AMOUNT,
                ENTRY_DATE,
                BATCH_ID,
                SETTLE_STATE_ID,
                LEDGER_DATE,
                DESCRIPTION)
            SELECT
                LEDGER_SEQ.NEXTVAL,
                'SB',
                NULL,
                NULL,
                l_round_amt,
                SYSDATE,
                l_batch_id,
                2,
                SYSDATE,
                NULL
              FROM DUAL;
        END IF;
    END;

    PROCEDURE ADD_BATCH_ROUNDING_ENTRIES(
        l_doc_id DOC.DOC_ID%TYPE)
    IS
        CURSOR l_cur IS
            SELECT BATCH_ID
              FROM BATCH
             WHERE DOC_ID = l_doc_id;
    BEGIN
        FOR l_rec IN l_cur LOOP
            ADD_BATCH_ROUNDING_ENTRY(l_rec.BATCH_ID);
        END LOOP;
    END;

    PROCEDURE ADD_ROUNDING_ENTRY(
        l_doc_id DOC.DOC_ID%TYPE)
    IS
        l_round_amt LEDGER.AMOUNT%TYPE;
        l_batch_id BATCH.BATCH_ID%TYPE;
    BEGIN
        -- calculate rounding amount
        SELECT ROUND(SUM(AMOUNT), 2) - SUM(AMOUNT)
          INTO l_round_amt
          FROM LEDGER L, BATCH B
         WHERE L.DELETED = 'N'
           AND ENTRY_PAYABLE(L.SETTLE_STATE_ID,L.ENTRY_TYPE) = 'Y'
           AND L.BATCH_ID = B.BATCH_ID
           AND B.DOC_ID = l_doc_id;

        -- add entry if necessary
        IF l_round_amt <> 0 THEN
            SELECT MAX(BATCH_ID)
              INTO l_batch_id
              FROM BATCH
             WHERE DOC_ID = l_doc_id
               AND TERMINAL_ID IS NULL;
            IF l_batch_id IS NULL THEN
                -- create a batch and set the doc id
                SELECT BATCH_SEQ.NEXTVAL
                  INTO l_batch_id
                  FROM DUAL;
                INSERT INTO BATCH(BATCH_ID, DOC_ID, TERMINAL_ID, PAYMENT_SCHEDULE_ID, START_DATE, END_DATE, BATCH_STATE_CD)
                    SELECT l_batch_id, l_doc_id, NULL, 1, SYSDATE, SYSDATE, 'F'
                      FROM DOC
                     WHERE DOC_ID = l_doc_id;
            END IF;

            INSERT INTO LEDGER(
                LEDGER_ID,
                ENTRY_TYPE,
                TRANS_ID,
                PROCESS_FEE_ID,
                AMOUNT,
                ENTRY_DATE,
                BATCH_ID,
                SETTLE_STATE_ID,
                LEDGER_DATE,
                DESCRIPTION)
            SELECT
                LEDGER_SEQ.NEXTVAL,
                'SB',
                NULL,
                NULL,
                l_round_amt,
                SYSDATE,
                l_batch_id,
                2,
                SYSDATE,
                NULL
              FROM DUAL;
        END IF;
    END;
    /*
     * This procedure references REPORT.TRAN
    */
    PROCEDURE UPDATE_SETTLEMENT(
        l_trans_id    CORP.LEDGER.TRANS_ID%TYPE,
        l_settle_state_id CORP.LEDGER.SETTLE_STATE_ID%TYPE,
        l_settle_date   CORP.LEDGER.LEDGER_DATE%TYPE)
    IS
        CURSOR l_cur IS
            SELECT D.DOC_ID, B.BATCH_ID, D.CUSTOMER_BANK_ID
              FROM LEDGER L, BATCH B, DOC D
             WHERE L.TRANS_ID = l_trans_id
               AND L.BATCH_ID = B.BATCH_ID
               AND B.DOC_ID = D.DOC_ID
               AND D.STATUS = 'O';
        TYPE t_rec_list IS TABLE OF l_cur%ROWTYPE;
        l_recs t_rec_list;
        l_batch_ids NUMBER_TABLE := NUMBER_TABLE();
        l_lock VARCHAR2(128);
    BEGIN
        l_lock := GLOBALS_PKG.REQUEST_LOCK('LEDGER.TRANS_ID',l_trans_id);
        OPEN l_cur;
        FETCH l_cur BULK COLLECT INTO l_recs;
        CLOSE l_cur;

        IF l_recs.FIRST IS NOT NULL THEN
            FOR i IN l_recs.FIRST .. l_recs.LAST LOOP
                l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_recs(i).CUSTOMER_BANK_ID);
                IF GET_DOC_STATUS(l_recs(i).DOC_ID) = 'O' THEN
                   l_batch_ids.EXTEND;
                   l_batch_ids(l_batch_ids.LAST) := l_recs(i).BATCH_ID;
                ELSE
                    EXIT;
                END IF;
            END LOOP;
        END IF;
        IF l_recs.FIRST IS NOT NULL AND l_recs.LAST = l_batch_ids.LAST THEN -- we can do direct update Yippee!
            UPDATE LEDGER SET
                SETTLE_STATE_ID = l_settle_state_id,
                LEDGER_DATE = l_settle_date
             WHERE TRANS_ID = l_trans_id
               AND BATCH_ID MEMBER OF l_batch_ids;
        ELSE
           DECLARE
                l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE;
                l_close_date LEDGER.LEDGER_DATE%TYPE;
                l_total_amount LEDGER.AMOUNT%TYPE;
                l_terminal_id BATCH.TERMINAL_ID%TYPE;
                l_customer_bank_id DOC.CUSTOMER_BANK_ID%TYPE;
                l_process_fee_id LEDGER.PROCESS_FEE_ID%TYPE;
                l_currency_id DOC.CURRENCY_ID%TYPE;
            BEGIN
                SELECT TRANS_TYPE_ID, CLOSE_DATE, TOTAL_AMOUNT, TERMINAL_ID,
                       CUSTOMER_BANK_ID, PROCESS_FEE_ID, CURRENCY_ID
                  INTO l_trans_type_id, l_close_date, l_total_amount, l_terminal_id,
                       l_customer_bank_id, l_process_fee_id, l_currency_id
                  FROM TRANS
                 WHERE TRAN_ID = l_trans_id;

                UPDATE_LEDGER(l_trans_id, l_trans_type_id, l_close_date,
                    l_settle_date, l_total_amount, l_settle_state_id,
                    l_terminal_id, l_customer_bank_id,l_process_fee_id, l_currency_id);
            END;
        END IF;
    END;

    PROCEDURE UPDATE_PROCESS_FEE_VALUES(
        l_process_fee_id CORP.LEDGER.PROCESS_FEE_ID%TYPE)
    IS
        CURSOR l_tran_cur IS
           SELECT DISTINCT L.TRANS_ID, X.TRANS_TYPE_ID, X.CLOSE_DATE, X.TOTAL_AMOUNT,
                  X.TERMINAL_ID, X.CUSTOMER_BANK_ID, X.SETTLE_STATE_ID,
                  X.SETTLE_DATE, X.CURRENCY_ID
              FROM LEDGER L, REPORT.TRANS X
             WHERE L.PROCESS_FEE_ID = l_process_fee_id
               AND L.DELETED = 'N'
               AND L.ENTRY_TYPE = 'PF'
               AND L.TRANS_ID = X.TRAN_ID;
    BEGIN
        FOR l_tran_rec IN l_tran_cur LOOP
            UPDATE_LEDGER(
                l_tran_rec.TRANS_ID,
                l_tran_rec.trans_type_id,
                l_tran_rec.close_date,
                l_tran_rec.settle_date,
                l_tran_rec.total_amount,
                l_tran_rec.settle_state_id,
                l_tran_rec.terminal_id,
                l_tran_rec.customer_bank_id,
                l_process_fee_id,
                l_tran_rec.currency_id);
        END LOOP;
    END;

    -- Puts CC, RF, or CB record into Ledger and also any required PF record
    PROCEDURE INSERT_TRANS_TO_LEDGER(
        l_trans_id    LEDGER.TRANS_ID%TYPE,
        l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE,
        l_close_date    LEDGER.ENTRY_DATE%TYPE,
        l_settle_date LEDGER.LEDGER_DATE%TYPE,
        l_amount LEDGER.AMOUNT%TYPE,
        l_settle_state_id LEDGER.SETTLE_STATE_ID%TYPE,
        l_process_fee_id    LEDGER.PROCESS_FEE_ID%TYPE,
        l_batch_id LEDGER.BATCH_ID%TYPE)
    IS
        l_ledger_id LEDGER.LEDGER_ID%TYPE;
        l_real_amt LEDGER.AMOUNT%TYPE;
        l_pay_sched BATCH.PAYMENT_SCHEDULE_ID%TYPE;
        l_end_date BATCH.END_DATE%TYPE;
		l_payable_ind REPORT.TRANS_TYPE.PAYABLE_IND%TYPE;
		l_customer_debit_ind REPORT.TRANS_TYPE.CUSTOMER_DEBIT_IND%TYPE;
		l_entry_type REPORT.TRANS_TYPE.ENTRY_TYPE%TYPE;
		l_fill_batch_ind REPORT.TRANS_TYPE.FILL_BATCH_IND%TYPE;
    BEGIN
        IF NVL(l_amount, 0) = 0 THEN
            RETURN;
        END IF;
		
		SELECT PAYABLE_IND, CUSTOMER_DEBIT_IND, ENTRY_TYPE, FILL_BATCH_IND
		INTO l_payable_ind, l_customer_debit_ind, l_entry_type, l_fill_batch_ind
		FROM REPORT.TRANS_TYPE
		WHERE TRANS_TYPE_ID = l_trans_type_id;
        
		IF l_payable_ind = 'Y' THEN
            SELECT LEDGER_SEQ.NEXTVAL,
                   DECODE(l_customer_debit_ind, 'Y', -ABS(l_amount), l_amount)
              INTO l_ledger_id,
                   l_real_amt
              FROM DUAL;
            INSERT INTO LEDGER(
                LEDGER_ID,
                ENTRY_TYPE,
                TRANS_ID,
                PROCESS_FEE_ID,
                AMOUNT,
                ENTRY_DATE,
                BATCH_ID,
                SETTLE_STATE_ID,
                LEDGER_DATE)
            SELECT
                l_ledger_id,
                l_entry_type,
                l_trans_id,
                l_process_fee_id,
                l_real_amt,
                l_close_date,
                l_batch_id,
                l_settle_state_id,
                l_settle_date
              FROM DUAL;
            -- create net revenue fee on transaction, if any
            INSERT INTO LEDGER(
                LEDGER_ID,
                RELATED_LEDGER_ID,
                ENTRY_TYPE,
                TRANS_ID,
                PROCESS_FEE_ID,
                SERVICE_FEE_ID,
                AMOUNT,
                ENTRY_DATE,
                BATCH_ID,
                SETTLE_STATE_ID,
                LEDGER_DATE,
                DESCRIPTION)
            SELECT
                LEDGER_SEQ.NEXTVAL,
                l_ledger_id,
                'SF',
                l_trans_id,
                l_process_fee_id,
                SF.SERVICE_FEE_ID,
                -l_real_amt * SF.FEE_PERCENT,
                l_close_date,
                l_batch_id,
                l_settle_state_id,
                l_settle_date,
                F.FEE_NAME
            FROM SERVICE_FEES SF, FEES F, BATCH B
           WHERE SF.FEE_ID = F.FEE_ID
             AND SF.TERMINAL_ID = B.TERMINAL_ID
             AND B.BATCH_ID = l_batch_id
             AND l_close_date BETWEEN NVL(SF.START_DATE, MIN_DATE)
                  AND NVL(SF.END_DATE, MAX_DATE)
             AND SF.FREQUENCY_ID = 6;
        END IF;
        IF l_process_fee_id IS NOT NULL THEN
            BEGIN
                SELECT LEDGER_SEQ.NEXTVAL,
                       -GREATEST((ABS(l_amount) * pf.FEE_PERCENT + pf.FEE_AMOUNT), pf.MIN_AMOUNT)
                  INTO l_ledger_id,
                       l_real_amt
                  FROM PROCESS_FEES pf
                WHERE pf.PROCESS_FEE_ID = l_process_fee_id
                  AND (pf.FEE_PERCENT > 0 OR pf.FEE_AMOUNT > 0);
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                RETURN;
              WHEN OTHERS THEN
                RAISE;
            END;
            --also insert process fee
            INSERT INTO LEDGER(
                LEDGER_ID,
                ENTRY_TYPE,
                TRANS_ID,
                PROCESS_FEE_ID,
                AMOUNT,
                ENTRY_DATE,
                BATCH_ID,
                SETTLE_STATE_ID,
                LEDGER_DATE)
            VALUES(
                l_ledger_id,
                'PF',
                l_trans_id,
                l_process_fee_id,
                l_real_amt,
                l_close_date,
                l_batch_id,
                l_settle_state_id,
                l_settle_date);
            -- create net revenue fee on process fee, if any
            INSERT INTO LEDGER(
                LEDGER_ID,
                RELATED_LEDGER_ID,
                ENTRY_TYPE,
                TRANS_ID,
                PROCESS_FEE_ID,
                SERVICE_FEE_ID,
                AMOUNT,
                ENTRY_DATE,
                BATCH_ID,
                SETTLE_STATE_ID,
                LEDGER_DATE,
                DESCRIPTION)
            SELECT
                LEDGER_SEQ.NEXTVAL,
                l_ledger_id,
                'SF',
                l_trans_id,
                l_process_fee_id,
                SF.SERVICE_FEE_ID,
                -l_real_amt * SF.FEE_PERCENT,
                l_close_date,
                l_batch_id,
                l_settle_state_id,
                l_settle_date,
                F.FEE_NAME
            FROM SERVICE_FEES SF, FEES F, BATCH B
           WHERE SF.FEE_ID = F.FEE_ID
             AND SF.TERMINAL_ID = B.TERMINAL_ID
             AND B.BATCH_ID = l_batch_id
             AND l_close_date BETWEEN NVL(SF.START_DATE, MIN_DATE)
                  AND NVL(SF.END_DATE, MAX_DATE)
             AND SF.FREQUENCY_ID = 6;
        END IF;
        -- Now check batch complete
        IF l_fill_batch_ind = 'Y' THEN
            SELECT PAYMENT_SCHEDULE_ID, END_DATE
              INTO l_pay_sched, l_end_date
              FROM CORP.BATCH B
             WHERE B.BATCH_ID = l_batch_id;
            IF l_pay_sched IN(2) AND l_end_date IS NOT NULL THEN
               CHECK_FILL_BATCH_COMPLETE(l_batch_id);
            END IF;
        END IF;
    END;

    PROCEDURE INSERT_TRANS_TO_LEDGER(
        l_trans_id    LEDGER.TRANS_ID%TYPE,
        l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE,
        l_close_date    LEDGER.ENTRY_DATE%TYPE,
        l_settle_date LEDGER.LEDGER_DATE%TYPE,
        l_amount LEDGER.AMOUNT%TYPE,
        l_settle_state_id LEDGER.SETTLE_STATE_ID%TYPE,
        l_process_fee_id    LEDGER.PROCESS_FEE_ID%TYPE,
        l_terminal_id   BATCH.TERMINAL_ID%TYPE,
        l_customer_bank_id  DOC.CUSTOMER_BANK_ID%TYPE,
        l_currency_id       DOC.CURRENCY_ID%TYPE)
    IS
        l_batch_id LEDGER.BATCH_ID%TYPE;
        l_doc_status DOC.STATUS%TYPE;
        l_lock VARCHAR2(128);
		l_customer_debit_ind REPORT.TRANS_TYPE.CUSTOMER_DEBIT_IND%TYPE;
    BEGIN
		SELECT CUSTOMER_DEBIT_IND
		INTO l_customer_debit_ind
		FROM REPORT.TRANS_TYPE
		WHERE TRANS_TYPE_ID = l_trans_type_id;
	
        -- if refund or chargeback get the batch that the
        -- original trans is in, if open use it else use "as accumulated"
        IF l_customer_debit_ind = 'Y' THEN
            l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_customer_bank_id);
            l_lock := GLOBALS_PKG.REQUEST_LOCK('BATCH.TERMINAL_ID',l_terminal_id);
            BEGIN
                SELECT BATCH_ID, STATUS
                  INTO l_batch_id, l_doc_status
                  FROM (SELECT B.BATCH_ID, D.STATUS
                          FROM LEDGER L, BATCH B, TRANS T, DOC D
                         WHERE L.TRANS_ID = T.ORIG_TRAN_ID
                           AND T.TRAN_ID = l_trans_id
                           AND L.BATCH_ID = B.BATCH_ID
                           AND L.DELETED = 'N'
                           AND B.DOC_ID = D.DOC_ID
                           AND NVL(D.CURRENCY_ID, 0) = NVL(l_currency_id, 0)
                           AND L.ENTRY_TYPE = 'CC'
                           AND B.PAYMENT_SCHEDULE_ID NOT IN(8) -- As Exported requires each trans to be put in "open" batch
                         ORDER BY DECODE(D.STATUS, 'O', 0, 1), B.BATCH_ID DESC)
                 WHERE ROWNUM = 1;
                IF NVL(l_doc_status, ' ') <> 'O' THEN
                    l_batch_id := GET_OR_CREATE_BATCH(l_terminal_id, l_customer_bank_id, l_close_date, l_currency_id, 'Y');
                END IF;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN -- refund for trans not in ledger
                -- The following should be removed and the error re-enabled,
                -- once we enforce orig_tran_id for all refunds/ chargebacks
                l_batch_id := GET_OR_CREATE_BATCH(l_terminal_id, l_customer_bank_id, l_close_date, l_currency_id, 'Y');
                --RAISE_APPLICATION_ERROR(-20702, 'TRANSACTION (tran_id='||TO_CHAR(l_trans_id)||' is a refund or chargeback on a transaction NOT found in the ledger table!');
                WHEN OTHERS THEN
                    RAISE;
            END;
        ELSE
            l_batch_id := GET_OR_CREATE_BATCH(l_terminal_id, l_customer_bank_id, l_close_date, l_currency_id, 'N');
        END IF;
        INSERT_TRANS_TO_LEDGER(l_trans_id, l_trans_type_id, l_close_date, l_settle_date,
            l_amount, l_settle_state_id, l_process_fee_id, l_batch_id);
    END;
    
    -- Updates the ledger table with the transaction changes
    -- May fail if the transaction is already part of a document
    -- (i.e. - it has already been paid)
    PROCEDURE UPDATE_LEDGER(
        l_trans_id    LEDGER.TRANS_ID%TYPE,
        l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE,
        l_close_date    LEDGER.ENTRY_DATE%TYPE,
        l_settle_date LEDGER.LEDGER_DATE%TYPE,
        l_total_amount LEDGER.AMOUNT%TYPE,
        l_settle_state_id LEDGER.SETTLE_STATE_ID%TYPE,
        l_terminal_id   BATCH.TERMINAL_ID%TYPE,
        l_customer_bank_id  DOC.CUSTOMER_BANK_ID%TYPE,
        l_process_fee_id    LEDGER.PROCESS_FEE_ID%TYPE,
        l_currency_id       DOC.CURRENCY_ID%TYPE)
    IS
        l_batch_id BATCH.BATCH_ID%TYPE;
        l_closed_cnt PLS_INTEGER;
        l_lock VARCHAR2(128);
        l_prev_cb_ids NUMBER_TABLE;
		l_cash_ind REPORT.TRANS_TYPE.CASH_IND%TYPE;
		l_entry_type REPORT.TRANS_TYPE.ENTRY_TYPE%TYPE;
		l_payable_ind REPORT.TRANS_TYPE.PAYABLE_IND%TYPE;
    BEGIN
		SELECT CASH_IND, ENTRY_TYPE, PAYABLE_IND
		INTO l_cash_ind, l_entry_type, l_payable_ind
		FROM REPORT.TRANS_TYPE
		WHERE TRANS_TYPE_ID = l_trans_type_id;
		
        IF l_cash_ind = 'Y' THEN -- cash; ignore
            RETURN;
        END IF;
        l_lock := GLOBALS_PKG.REQUEST_LOCK('LEDGER.TRANS_ID',l_trans_id);
        -- get the batch
        SELECT MAX(B.BATCH_ID), SUM(DECODE(D.STATUS, 'O', 0, 'D', 0, 1)), -- are there ANY closed batches on this transaction?
               SET(CAST(COLLECT(CAST(D.CUSTOMER_BANK_ID AS NUMBER)) AS NUMBER_TABLE))
          INTO l_batch_id, l_closed_cnt, l_prev_cb_ids
          FROM LEDGER L, BATCH B, DOC D
         WHERE L.TRANS_ID = l_trans_id
           AND L.BATCH_ID = B.BATCH_ID
           AND L.DELETED = 'N'
           AND B.DOC_ID = D.DOC_ID;
        IF l_batch_id IS NULL THEN -- Create new batch for this ledger record
            -- If terminal is null then no need to do anything more
            -- Added: if cust bank is null don't do any more (we may need to change this in the future which would also entail making BATCH.CUSTOMER_BANK_ID nullable)
            IF l_terminal_id IS NOT NULL AND l_customer_bank_id IS NOT NULL THEN
                INSERT_TRANS_TO_LEDGER(l_trans_id, l_trans_type_id, l_close_date, l_settle_date,
                    l_total_amount, l_settle_state_id, l_process_fee_id, l_terminal_id, l_customer_bank_id, l_currency_id);
                RETURN; -- all done
            END IF;
        ELSIF l_closed_cnt = 0 THEN -- lock previous customer_bank_id's to ensure that none of the doc's closes
            FOR i IN l_prev_cb_ids.FIRST..l_prev_cb_ids.LAST LOOP
                l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_prev_cb_ids(i));
            END LOOP;
            -- double-check closed count
            SELECT SUM(DECODE(D.STATUS, 'O', 0, 'D', 0, 1))
              INTO l_closed_cnt
              FROM LEDGER L, BATCH B, DOC D
             WHERE L.TRANS_ID = l_trans_id
               AND L.BATCH_ID = B.BATCH_ID
               AND L.DELETED = 'N'
               AND B.DOC_ID = D.DOC_ID;
        END IF;
        IF l_closed_cnt > 0 THEN -- add adjustment for prev paid
            DECLARE
                CURSOR l_prev_cur IS
                    SELECT COUNT(*) PAID_CNT,
                           SUM(DECODE(ENTRY_PAYABLE(L.SETTLE_STATE_ID, L.ENTRY_TYPE), 'Y', AMOUNT, NULL)) PAID_AMT, B.TERMINAL_ID,
                           D.CUSTOMER_BANK_ID, D.CURRENCY_ID
                      FROM LEDGER L, BATCH B, DOC D
                     WHERE L.TRANS_ID = l_trans_id
                       AND L.BATCH_ID = B.BATCH_ID
                       AND B.DOC_ID = D.DOC_ID
                       AND L.DELETED = 'N'
                       AND L.RELATED_LEDGER_ID IS NULL -- Must exclude Net Revenue Fees
                     GROUP BY B.TERMINAL_ID, D.CUSTOMER_BANK_ID, D.CURRENCY_ID;

                l_adj_amt LEDGER.AMOUNT%TYPE;
                l_adjusted_new_cb BOOLEAN := FALSE;
                l_old_pf_amt LEDGER.AMOUNT%TYPE;
                l_old_amt LEDGER.AMOUNT%TYPE;
                l_desc LEDGER.DESCRIPTION%TYPE;
                l_new_payable CHAR(1);
                l_ledger_id LEDGER.LEDGER_ID%TYPE;
            BEGIN
                --figure out what change has occurred
                FOR l_prev_rec IN l_prev_cur LOOP
                    IF NVL(l_customer_bank_id, 0) = l_prev_rec.CUSTOMER_BANK_ID
                       AND NVL(l_currency_id, 0) = NVL(l_prev_rec.CURRENCY_ID, 0) THEN
                        IF NVL(l_prev_rec.PAID_CNT, 0) <> 0 AND NVL(l_prev_rec.PAID_AMT, 0) <> 0 THEN
                            --calc difference and add as adjustment
                            SELECT DECODE(l_prev_rec.TERMINAL_ID, l_terminal_id, ENTRY_PAYABLE(l_settle_state_id, l_entry_type), 'N')
                              INTO l_new_payable
                              FROM DUAL;
                            SELECT (CASE WHEN l_payable_ind = 'Y' THEN l_total_amount ELSE 0 END
                                   - NVL(SUM(GREATEST(ABS(l_total_amount) * pf.FEE_PERCENT + pf.FEE_AMOUNT, pf.MIN_AMOUNT)), 0))
                                   * DECODE(l_new_payable, 'Y', 1, 0)
                                   - l_prev_rec.PAID_AMT
                              INTO l_adj_amt
                              FROM PROCESS_FEES pf
                             WHERE pf.PROCESS_FEE_ID = l_process_fee_id;
                            IF l_adj_amt <> 0 THEN
                                IF l_new_payable = 'Y' THEN
                                    SELECT SUM(AMOUNT)
                                      INTO l_old_amt
                                      FROM LEDGER
                                     WHERE TRANS_ID = l_trans_id
                                       AND ENTRY_TYPE IN('CC')
                                       AND ENTRY_PAYABLE(SETTLE_STATE_ID, ENTRY_TYPE) = 'Y'
                                       AND DELETED = 'N';
                                    SELECT SUM(AMOUNT)
                                      INTO l_old_pf_amt
                                      FROM LEDGER
                                     WHERE TRANS_ID = l_trans_id
                                       AND ENTRY_TYPE IN('PF')
                                       AND ENTRY_PAYABLE(SETTLE_STATE_ID, ENTRY_TYPE) = 'Y'
                                       AND DELETED = 'N';
                                    IF NVL(l_old_amt, 0) = 0 AND l_payable_ind = 'Y' THEN
                                        l_desc := 'Correction for a previously ignored transaction';
                                    ELSIF l_old_amt <> l_total_amount AND l_payable_ind = 'Y' THEN
                                        l_desc := 'Correction for a value amount change on a transaction that has already been paid';
                                    ELSIF NVL(l_old_pf_amt, 0) = 0 THEN
                                        l_desc := 'Correction for a missing processing fee on a transaction that has already been paid';
                                    ELSIF NVL(l_terminal_id, 0) <> l_prev_rec.TERMINAL_ID THEN
                                        l_desc := 'Correction for a change of location on a transaction that has already been paid';
                                    ELSE
                                        l_desc := 'Correction for a change in processing fee on a transaction that has already been paid';
                                    END IF;
                                ELSIF NVL(l_terminal_id, 0) <> l_prev_rec.TERMINAL_ID THEN
                                    l_desc := 'Correction for a change of location on a transaction that has already been paid';
                                ELSE -- should not be paid now
                                    l_desc := 'Correction for a transaction that has already been paid but has since failed settlement';
                                END IF;
                                l_batch_id := GET_OR_CREATE_BATCH(l_prev_rec.TERMINAL_ID,
                                        l_customer_bank_id, l_close_date, l_currency_id, 'A');
                                -- create a negative adjustment based on this previous payment
                                SELECT LEDGER_SEQ.NEXTVAL
                                  INTO l_ledger_id
                                  FROM DUAL;
                                INSERT INTO LEDGER(
                                    LEDGER_ID,
                                    ENTRY_TYPE,
                                    TRANS_ID,
                                    PROCESS_FEE_ID,
                                    AMOUNT,
                                    ENTRY_DATE,
                                    BATCH_ID,
                                    SETTLE_STATE_ID,
                                    LEDGER_DATE,
                                    DESCRIPTION)
                                VALUES(
                                    l_ledger_id,
                                    'AD',
                                    l_trans_id,
                                    NULL,
                                    l_adj_amt,
                                    l_close_date,
                                    l_batch_id,
                                    2,
                                    l_settle_date,
                                    l_desc);
                                -- create net revenue adjustment, if any
                                INSERT INTO LEDGER(
                                    LEDGER_ID,
                                    RELATED_LEDGER_ID,
                                    ENTRY_TYPE,
                                    TRANS_ID,
                                    PROCESS_FEE_ID,
                                    SERVICE_FEE_ID,
                                    AMOUNT,
                                    ENTRY_DATE,
                                    BATCH_ID,
                                    SETTLE_STATE_ID,
                                    LEDGER_DATE,
                                    DESCRIPTION)
                                SELECT
                                    LEDGER_SEQ.NEXTVAL,
                                    l_ledger_id,
                                    'SF',
                                    l_trans_id,
                                    NULL,
                                    SF.SERVICE_FEE_ID,
                                    -l_adj_amt * SF.FEE_PERCENT,
                                    l_close_date,
                                    l_batch_id,
                                    2,
                                    l_settle_date,
                                    F.FEE_NAME
                                FROM SERVICE_FEES SF, FEES F
                               WHERE SF.FEE_ID = F.FEE_ID
                                 AND SF.TERMINAL_ID = l_prev_rec.TERMINAL_ID
                                 AND l_close_date BETWEEN NVL(SF.START_DATE, MIN_DATE)
                                      AND NVL(SF.END_DATE, MAX_DATE)
                                 AND SF.FREQUENCY_ID = 6;
                            END IF;
                            l_adjusted_new_cb := l_adjusted_new_cb OR l_adj_amt = 0 OR NVL(l_terminal_id, 0) = l_prev_rec.TERMINAL_ID; -- whether we adjust or not the new cb of the tran is appropriately dealt with
                        END IF;
                    --case: bank acct changed
                    ELSIF l_prev_rec.PAID_AMT <> 0 THEN
                        IF NVL(l_terminal_id, 0) <> l_prev_rec.TERMINAL_ID THEN
                            l_desc := 'Correction for a transaction that was assigned to the wrong location';
                        ELSIF NVL(l_currency_id, 0) <> NVL(l_prev_rec.CURRENCY_ID, 0) THEN
                            l_desc := 'Correction for a change in currency';
                        ELSE
                            l_desc := 'Correction for a transaction that was paid to the wrong bank account';
                        END IF;
                        -- deduct entire amt from old
                        l_batch_id := GET_OR_CREATE_BATCH(
                                l_prev_rec.TERMINAL_ID,
                                l_prev_rec.CUSTOMER_BANK_ID,
                                l_close_date,
                                l_prev_rec.CURRENCY_ID,
                                'A');
                        SELECT LEDGER_SEQ.NEXTVAL
                          INTO l_ledger_id
                          FROM DUAL;
                        INSERT INTO LEDGER(
                            LEDGER_ID,
                            ENTRY_TYPE,
                            TRANS_ID,
                            PROCESS_FEE_ID,
                            AMOUNT,
                            ENTRY_DATE,
                            BATCH_ID,
                            SETTLE_STATE_ID,
                            LEDGER_DATE,
                            DESCRIPTION)
                        VALUES(
                            l_ledger_id,
                            'AD',
                            l_trans_id,
                            NULL,
                            -l_prev_rec.PAID_AMT,
                            l_close_date,
                            l_batch_id,
                            l_settle_state_id,
                            l_settle_date,
                            l_desc);
                        -- create net revenue adjustment, if any
                        INSERT INTO LEDGER(
                            LEDGER_ID,
                            RELATED_LEDGER_ID,
                            ENTRY_TYPE,
                            TRANS_ID,
                            PROCESS_FEE_ID,
                            SERVICE_FEE_ID,
                            AMOUNT,
                            ENTRY_DATE,
                            BATCH_ID,
                            SETTLE_STATE_ID,
                            LEDGER_DATE,
                            DESCRIPTION)
                        SELECT
                            LEDGER_SEQ.NEXTVAL,
                            l_ledger_id,
                            'SF',
                            l_trans_id,
                            NULL,
                            SF.SERVICE_FEE_ID,
                            l_prev_rec.PAID_AMT * SF.FEE_PERCENT,
                            l_close_date,
                            l_batch_id,
                            l_settle_state_id,
                            l_settle_date,
                            F.FEE_NAME
                        FROM SERVICE_FEES SF, FEES F
                       WHERE SF.FEE_ID = F.FEE_ID
                         AND SF.TERMINAL_ID = l_prev_rec.TERMINAL_ID
                         AND l_close_date BETWEEN NVL(SF.START_DATE, MIN_DATE)
                              AND NVL(SF.END_DATE, MAX_DATE)
                         AND SF.FREQUENCY_ID = 6;
                    END IF;
                END LOOP;
                IF NOT l_adjusted_new_cb AND l_customer_bank_id IS NOT NULL THEN
                    l_batch_id := GET_OR_CREATE_BATCH(l_terminal_id,
                        l_customer_bank_id, l_close_date, l_currency_id, 'A');
                    INSERT_TRANS_TO_LEDGER(l_trans_id, l_trans_type_id,
                        l_close_date, l_settle_date, l_total_amount,
                        l_settle_state_id, l_process_fee_id, l_batch_id);
                END IF;
            END;
        ELSE -- an open batch exists
            -- To make it easy let's just delete what's there and re-insert
            --DELETE FROM LEDGER WHERE TRANS_ID = l_trans_id;
            UPDATE LEDGER SET DELETED = 'Y' WHERE TRANS_ID = l_trans_id;
            -- If terminal is null then no need to do anything more
            -- Added: if cust bank is null don't do any more (we may need to change this in the future which would also entail making BATCH.CUSTOMER_BANK_ID nullable)
            IF l_terminal_id IS NOT NULL AND l_customer_bank_id IS NOT NULL THEN
                INSERT_TRANS_TO_LEDGER(l_trans_id, l_trans_type_id, l_close_date, l_settle_date,
                    l_total_amount, l_settle_state_id, l_process_fee_id, l_terminal_id, l_customer_bank_id, l_currency_id);
            END IF;
        END IF;
    END;
    
    -- updates any fill-to-fill batch records upon the insertion of a record
    -- into the FILL table. Updates to the FILL table are NOT handled correctly
    -- by this procedure!
    PROCEDURE UPDATE_FILL_BATCH(
        l_fill_id   FILL.FILL_ID%TYPE)
    IS
        CURSOR l_batch_cur IS
            SELECT B.BATCH_ID, B.TERMINAL_ID, D.CUSTOMER_BANK_ID, D.CURRENCY_ID,
                   B.START_DATE, CASE WHEN BCOT.BATCH_CONFIRM_OPTION_TYPE_ID IS NULL THEN  'N' ELSE 'Y' END COUNTER_MUST_SHOW
              FROM CORP.BATCH B
              JOIN REPORT.TERMINAL_EPORT TE ON B.TERMINAL_ID = TE.TERMINAL_ID
              JOIN REPORT.FILL F ON TE.EPORT_ID = F.EPORT_ID
               AND F.FILL_DATE >= NVL(te.START_DATE, MIN_DATE)
               AND F.FILL_DATE < NVL(te.END_DATE, MAX_DATE)
              JOIN CORP.DOC D ON B.DOC_ID = D.DOC_ID
              JOIN REPORT.TERMINAL T ON B.TERMINAL_ID = T.TERMINAL_ID
              LEFT OUTER JOIN (CORP.BATCH_CONFIRM BC
              JOIN CORP.BATCH_CONFIRM_OPTION BCO
                ON BC.BATCH_CONFIRM_ID = BCO.BATCH_CONFIRM_ID
              JOIN CORP.BATCH_CONFIRM_OPTION_TYPE BCOT ON BCO.BATCH_CONFIRM_OPTION_TYPE_ID = BCOT.BATCH_CONFIRM_OPTION_TYPE_ID
               AND BCOT.BATCH_CONFIRM_OPTION_TYPE_CD = 'COUNTER_MUST_SHOW'
              ) ON BC.CUSTOMER_ID = T.CUSTOMER_ID
               AND BC.PAYMENT_SCHEDULE_ID = B.PAYMENT_SCHEDULE_ID
             WHERE B.START_DATE < F.FILL_DATE
               AND NVL(B.END_DATE, MAX_DATE) > F.FILL_DATE
               AND B.BATCH_STATE_CD = 'O'
               AND B.PAYMENT_SCHEDULE_ID = 2
               AND (F.COUNTERS_DISPLAYED_FLAG = 'Y' OR BCOT.BATCH_CONFIRM_OPTION_TYPE_ID IS NULL)
               AND F.FILL_ID = l_fill_id;
        l_end_date CORP.BATCH.END_DATE%TYPE;
        l_end_fill_id CORP.BATCH_FILL.END_FILL_ID%TYPE;
        l_lock VARCHAR2(128);
    BEGIN
        FOR l_batch_rec IN l_batch_cur LOOP
            SELECT MIN(FILL_DATE), MIN(FILL_ID)
                  INTO l_end_date, l_end_fill_id
                  FROM (
                    SELECT f.FILL_DATE, f.FILL_ID
                      FROM REPORT.FILL f, REPORT.TERMINAL_EPORT te
                     WHERE f.FILL_DATE > l_batch_rec.START_DATE
                       AND f.EPORT_ID = te.EPORT_ID
                       AND f.FILL_DATE >= NVL(te.START_DATE, MIN_DATE)
                       AND f.FILL_DATE < NVL(te.END_DATE, MAX_DATE)
                       AND te.TERMINAL_ID = l_batch_rec.TERMINAL_ID
                       AND (F.COUNTERS_DISPLAYED_FLAG = 'Y' OR l_batch_rec.COUNTER_MUST_SHOW = 'N')
                     ORDER BY f.FILL_DATE ASC)
                 WHERE ROWNUM = 1;
            l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_batch_rec.CUSTOMER_BANK_ID);
            -- it doesn't matter if the batch's doc has changed, because we assume the batch is in an open doc
            -- update batch record
            UPDATE CORP.BATCH B SET B.END_DATE = l_end_date
             WHERE B.BATCH_ID = l_batch_rec.BATCH_ID;
            UPDATE CORP.BATCH_FILL BF SET BF.END_FILL_ID = l_end_fill_id
             WHERE BF.BATCH_ID = l_batch_rec.BATCH_ID;

            -- also update ledger.batch_id of entries that are after fill_date
            DECLARE
                CURSOR l_cur IS
                  SELECT L.LEDGER_ID, L.ENTRY_DATE
                    FROM LEDGER L
                   WHERE L.BATCH_ID = l_batch_rec.BATCH_ID
                     AND L.ENTRY_DATE >= l_end_date;
                TYPE t_rec_list IS TABLE OF l_cur%ROWTYPE;
                l_recs t_rec_list;
                l_new_batch_id LEDGER.BATCH_ID%TYPE;
            BEGIN
                OPEN l_cur;
                FETCH l_cur BULK COLLECT INTO l_recs;
                CLOSE l_cur;

                IF l_recs.FIRST IS NOT NULL THEN
                    FOR i IN l_recs.FIRST .. l_recs.LAST LOOP
                        l_new_batch_id := GET_OR_CREATE_BATCH(
                            l_batch_rec.TERMINAL_ID,
                            l_batch_rec.CUSTOMER_BANK_ID,
                            l_recs(i).ENTRY_DATE,
                            l_batch_rec.CURRENCY_ID,
                            'N');
                        UPDATE LEDGER L
                           SET L.BATCH_ID = l_new_batch_id
                         WHERE L.LEDGER_ID = l_recs(i).LEDGER_ID;
                    END LOOP;
                END IF;
            END;
            GLOBALS_PKG.RELEASE_LOCK(l_lock);
            COMMIT;
            
            -- Now check batch complete
            CHECK_FILL_BATCH_COMPLETE(l_batch_rec.BATCH_ID);
            COMMIT;
         END LOOP;
    END;
    
    PROCEDURE UPDATE_EXPORT_BATCH(
        l_export_id   REPORT.EXPORT_BATCH.BATCH_ID%TYPE)
    IS
        CURSOR l_batch_cur IS
            SELECT DISTINCT B.BATCH_ID, B.TERMINAL_ID, D.CUSTOMER_BANK_ID, D.CURRENCY_ID, B.START_DATE, EX.TRAN_CREATE_DT_END NEW_END_DATE
              FROM CORP.BATCH B
             INNER JOIN CORP.DOC D ON B.DOC_ID = D.DOC_ID
             INNER JOIN CORP.CUSTOMER_BANK CB ON D.CUSTOMER_BANK_ID = CB.CUSTOMER_BANK_ID
             /*
             INNER JOIN REPORT.TERMINAL TERMINAL_ID ON B.TERMINAL_ID = T.TERMINAL_ID
             INNER JOIN FRONT.VW_LOCATION_HIERARCHY H ON T.LOCATION_ID = H.DESCENDANT_LOCATION_ID
             */
             INNER JOIN REPORT.EXPORT_BATCH EX ON CB.CUSTOMER_ID = EX.CUSTOMER_ID /* AND EX.LOCATION_ID = H.ANCESTOR_LOCATION_ID */
             WHERE B.BATCH_STATE_CD = 'O'
               AND B.END_DATE IS NULL
               AND B.PAYMENT_SCHEDULE_ID = 8
               AND EX.BATCH_ID = l_export_id;
        l_lock VARCHAR2(128);
    BEGIN
        FOR l_batch_rec IN l_batch_cur LOOP
            l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_batch_rec.CUSTOMER_BANK_ID);
            -- it doesn't matter if the batch's doc has changed, because we assume the batch is in an open doc
            -- update batch record
            UPDATE BATCH B
               SET B.END_DATE = l_batch_rec.new_end_date,
                   B.BATCH_STATE_CD = (SELECT DECODE(COUNT(*), 0, 'D', 'U')
                      FROM REPORT.TERMINAL t
                      JOIN CORP.BATCH_CONFIRM CN ON t.CUSTOMER_ID = cn.CUSTOMER_ID
                     WHERE b.TERMINAL_ID = t.TERMINAL_ID
                       AND b.PAYMENT_SCHEDULE_ID = cn.PAYMENT_SCHEDULE_ID),
                   B.BATCH_CLOSED_TS = SYSDATE
             WHERE B.BATCH_ID = l_batch_rec.BATCH_ID;

            -- also update ledger.batch_id of entries that are not exported
            DECLARE
                CURSOR l_cur IS
                  SELECT L.LEDGER_ID, L.ENTRY_DATE
                    FROM LEDGER L
                   WHERE L.BATCH_ID = l_batch_rec.BATCH_ID
                     AND (L.ENTRY_DATE > l_batch_rec.new_end_date
                         OR NOT EXISTS(SELECT 1 FROM REPORT.ACTIVITY_REF A
                              INNER JOIN REPORT.EXPORT_BATCH EB ON A.BATCH_ID = EB.BATCH_ID
                              WHERE L.TRANS_ID = A.TRAN_ID AND EB.STATUS IN('A','S')));
                TYPE t_rec_list IS TABLE OF l_cur%ROWTYPE;
                l_recs t_rec_list;
                l_new_batch_id LEDGER.BATCH_ID%TYPE;
            BEGIN
                OPEN l_cur;
                FETCH l_cur BULK COLLECT INTO l_recs;
                CLOSE l_cur;

                IF l_recs.FIRST IS NOT NULL THEN
                    FOR i IN l_recs.FIRST .. l_recs.LAST LOOP
                        l_new_batch_id := GET_OR_CREATE_BATCH(
                            l_batch_rec.TERMINAL_ID,
                            l_batch_rec.CUSTOMER_BANK_ID,
                            l_recs(i).ENTRY_DATE,
                            l_batch_rec.CURRENCY_ID,
                            'N');
                        UPDATE LEDGER L
                           SET L.BATCH_ID = l_new_batch_id
                         WHERE L.LEDGER_ID = l_recs(i).LEDGER_ID;
                    END LOOP;
                END IF;
            END;
         END LOOP;
    END;
/* not doing this yet
    PROCEDURE AUTO_CREATE_DOCS.
    IS
       CURSOR c_docs IS
             SELECT cb.customer_bank_id
              FROM (SELECT l.CUSTOMER_BANK_ID, l.TRANS_TYPE_ID, SUM(l.TOTAL_AMOUNT) TOTAL_AMOUNT,
                           SUM(l.TOTAL_AMOUNT * l.FEE_PERCENT + l.FEE_AMOUNT) FEE_TOTAL
                      FROM LEDGER l, TERMINAL t
                     WHERE l.PAID_STATUS = 'U'
                       AND l.TERMINAL_ID = t.TERMINAL_ID
                       AND t.PAYMENT_SCHEDULE_ID <> 1
                     GROUP BY l.CUSTOMER_BANK_ID, l.TRANS_TYPE_ID) lp,
                     CUSTOMER_BANK cb
              WHERE cb.CUSTOMER_BANK_ID = lp.CUSTOMER_BANK_ID
              GROUP BY cb.CUSTOMER_BANK_ID, cb.PAY_MIN_AMOUNT
              HAVING SUM(CASE TRANS_TYPE_ID
                      WHEN 16 THEN 1
                      WHEN 19 THEN 1
                      WHEN 20 THEN -1
                      WHEN 21 THEN -1
                      ELSE NULL
                   END * TOTAL_AMOUNT) - SUM (lp.FEE_TOTAL) > cb.PAY_MIN_AMOUNT;
    BEGIN
        -- determine each customer bank that whose total eft will exceed the min_eft_amt
        FOR r_docs IN c_docs LOOP
            BEGIN
                CREATE_DOC(r_efts.customer_bank_id, NULL, l_type,'Y');
                COMMIT;
            EXCEPTION
                WHEN OTHERS THEN
                    IF SQLCODE = -20910 THEN
                        ROLLBACK;
                    ELSE
                        RAISE;
                    END IF;
            END;
        END LOOP;
    END;

*/

    FUNCTION GET_UNPAID_TRANS_AND_FEES (
        l_as_of DATE
    ) RETURN GLOBALS_PKG.REF_CURSOR
    IS
        cur GLOBALS_PKG.REF_CURSOR;
    BEGIN
       OPEN cur FOR
            SELECT CUSTOMER_NAME, CB.CUSTOMER_ID, CB.CUSTOMER_BANK_ID,
                   BANK_ACCT_NBR, BANK_ROUTING_NBR, PAY_MIN_AMOUNT, CREDIT_AMOUNT,
                   REFUND_AMOUNT, CHARGEBACK_AMOUNT, PROCESS_FEE_AMOUNT,
                   SERVICE_FEE_AMOUNT, ADJUST_AMOUNT
                   FROM CUSTOMER C, CUSTOMER_BANK CB, (
                SELECT CUSTOMER_BANK_ID,
                       SUM(DECODE(ENTRY_TYPE, 'CC', AMOUNT, NULL)) CREDIT_AMOUNT,
                       SUM(DECODE(ENTRY_TYPE, 'RF', AMOUNT, NULL)) REFUND_AMOUNT,
                       SUM(DECODE(ENTRY_TYPE, 'CB', AMOUNT, NULL)) CHARGEBACK_AMOUNT,
                       SUM(DECODE(ENTRY_TYPE, 'PF', AMOUNT, NULL)) PROCESS_FEE_AMOUNT,
                       SUM(DECODE(ENTRY_TYPE, 'SF', AMOUNT, NULL)) SERVICE_FEE_AMOUNT,
                       SUM(DECODE(ENTRY_TYPE, 'AD', AMOUNT, NULL)) ADJUST_AMOUNT
                  FROM (
                    SELECT D.CUSTOMER_BANK_ID, L.ENTRY_TYPE, SUM(L.AMOUNT) AMOUNT
                      FROM LEDGER L, BATCH B, DOC D
                      WHERE L.BATCH_ID = B.BATCH_ID
                      AND L.DELETED = 'N'
                      AND PAYMENTS_PKG.ENTRY_PAYABLE(L.SETTLE_STATE_ID,L.ENTRY_TYPE) = 'Y'
                      AND B.DOC_ID = D.DOC_ID
                      AND NVL(L.PAID_DATE, MIN_DATE) < l_as_of
                      AND L.LEDGER_DATE < l_as_of
                      GROUP BY D.CUSTOMER_BANK_ID, L.ENTRY_TYPE) M
                  GROUP BY CUSTOMER_BANK_ID) A
            WHERE CB.CUSTOMER_BANK_ID = A.CUSTOMER_BANK_ID (+)
            AND CB.CUSTOMER_ID = C.CUSTOMER_ID
            ORDER BY UPPER(CUSTOMER_NAME), BANK_ACCT_NBR;
        RETURN cur;
    END;
    
    FUNCTION FORMAT_EFT_REASON(
        l_payment_sched_id BATCH.PAYMENT_SCHEDULE_ID%TYPE,
        l_min_pay_date BATCH.START_DATE%TYPE,
        l_max_pay_date BATCH.END_DATE%TYPE
     ) RETURN VARCHAR
    IS
        l_text VARCHAR(8000);
    BEGIN
        SELECT DESCRIPTION
          INTO l_text
          FROM PAYMENT_SCHEDULE
         WHERE PAYMENT_SCHEDULE_ID = l_payment_sched_id;
        IF l_min_pay_date = l_max_pay_date THEN
            l_text := l_text || ' on ' || TO_CHAR(l_min_pay_date, 'MM-DD-YYYY');
        ELSE
            l_text := l_text || ' from ' || TO_CHAR(l_min_pay_date, 'MM-DD-YYYY')
                || ' to ' || TO_CHAR(l_max_pay_date, 'MM-DD-YYYY');
        END IF;
        RETURN l_text;
    END;
    /*
    FUNCTION FORMAT_EFT_REASON(
        l_eft_id EFT.EFT_ID%TYPE
     ) RETURN VARCHAR
    IS
        l_text VARCHAR(8000) := '';
        CURSOR l_cur IS
            SELECT PAYMENT_SCHEDULE_ID, MIN(PAYMENT_SCHEDULE_DATE) MIN_PAY_DATE,
                   MAX(PAYMENT_SCHEDULE_DATE) MAX_PAY_DATE
            FROM PAYMENTS P
            WHERE P.EFT_ID = l_eft_id
            GROUP BY PAYMENT_SCHEDULE_ID;
    BEGIN
        FOR l_rec IN l_cur LOOP
            IF LENGTH(l_text) > 0 THEN
                l_text := l_text || ' and ';
            END IF;
            l_text := l_text || FORMAT_EFT_REASON(l_rec.PAYMENT_SCHEDULE_ID, l_rec.MIN_PAY_DATE, l_rec.MAX_PAY_DATE);
        END LOOP;
        RETURN l_text;
    END;
    */

    -- Puts ledger record into a new batch
    PROCEDURE      DELAY_ENTRY (
      l_ledger_id IN LEDGER.LEDGER_ID%TYPE
      )
    IS
      l_status DOC.STATUS%TYPE;
      l_batch_id LEDGER.BATCH_ID%TYPE;
      l_entry_date LEDGER.ENTRY_DATE%TYPE;
      l_new_batch_id LEDGER.BATCH_ID%TYPE;
    BEGIN
        l_status := FREEZE_DOC_STATUS_LEDGER_ID(l_ledger_id);
        IF l_status IN('O') THEN
    		 RAISE_APPLICATION_ERROR(-20101, 'The Document must be locked before this entry can be pulled out of it');
        ELSIF l_status NOT IN('L') THEN
             RAISE_APPLICATION_ERROR(-20142, 'This entry belongs to a document that is already approved.');
        END IF;
        -- put in new batch
        SELECT L.BATCH_ID, L.ENTRY_DATE
          INTO l_batch_id, l_entry_date
          FROM LEDGER L
         WHERE LEDGER_ID = l_ledger_id;
        l_new_batch_id := GET_NEW_BATCH(l_batch_id, l_entry_date);
        UPDATE LEDGER L
           SET BATCH_ID = l_new_batch_id
         WHERE LEDGER_ID = l_ledger_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
    		 RAISE_APPLICATION_ERROR(-20100, 'Entry not found');
    	WHEN OTHERS THEN
    		 RAISE;
    END;
    
    -- Marks ledger record as deleted
    PROCEDURE      DELETE_ENTRY (
      l_ledger_id IN LEDGER.LEDGER_ID%TYPE)
    IS
      l_status DOC.STATUS%TYPE;
    BEGIN
        l_status := FREEZE_DOC_STATUS_LEDGER_ID(l_ledger_id);
        IF l_status IN('O') THEN
    		 NULL; --RAISE_APPLICATION_ERROR(-20101, 'The Document must be locked before this entry can be pulled out of it');
        ELSIF l_status NOT IN('L') THEN
             RAISE_APPLICATION_ERROR(-20142, 'This entry belongs to a document that is already approved.');
        END IF;
        -- mark as deleted (remove net revenue fees also)
        UPDATE LEDGER L
           SET DELETED = 'Y'
         WHERE LEDGER_ID = l_ledger_id
            OR RELATED_LEDGER_ID = l_ledger_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
    		 RAISE_APPLICATION_ERROR(-20100, 'Entry not found');
    	WHEN OTHERS THEN
    		 RAISE;
    END;

    -- Puts refund into a new batch
    PROCEDURE      DELAY_REFUND_BY_TRAN_ID (
      l_trans_id IN LEDGER.TRANS_ID%TYPE)
    IS
      l_ledger_id LEDGER.LEDGER_ID%TYPE;
    BEGIN
    	 SELECT LEDGER_ID
           INTO l_ledger_id
           FROM LEDGER
          WHERE TRANS_ID = l_trans_id
            AND ENTRY_TYPE = 'RF';
          DELAY_ENTRY(l_ledger_id);
    END;
    
    PROCEDURE ADJUSTMENT_INS (
        l_ledger_id OUT NOCOPY LEDGER.LEDGER_ID%TYPE,
       	l_doc_id IN BATCH.DOC_ID%TYPE,
        l_terminal_id IN BATCH.TERMINAL_ID%TYPE,
    	l_reason IN LEDGER.DESCRIPTION%TYPE,
    	l_amt IN LEDGER.AMOUNT%TYPE,
    	l_user_id IN LEDGER.CREATE_BY%TYPE,
		lc_split_payment_flag IN CHAR,
		lc_split_payment_interval_cd IN CHAR,
		ln_split_number_of_payments IN INTEGER,
		ld_first_split_payment_date IN DATE
	)
    IS
        l_batch_id LEDGER.BATCH_ID%TYPE;
        l_curr_id DOC.CURRENCY_ID%TYPE;
        l_term_curr_id DOC.CURRENCY_ID%TYPE;
        l_fee_date LEDGER.ENTRY_DATE%TYPE;
        l_customer_bank_id CORP.DOC.CUSTOMER_BANK_ID%TYPE;
        l_status DOC.STATUS%TYPE;
        l_lock VARCHAR2(128);
		ln_count INTEGER;
		ln_split_payment_amt LEDGER.AMOUNT%TYPE;
		ld_payment_date BATCH.END_DATE%TYPE;
    BEGIN
        SELECT D.CUSTOMER_BANK_ID, D.CURRENCY_ID
          INTO l_customer_bank_id, l_curr_id
          FROM CORP.DOC D
         WHERE D.DOC_ID = l_doc_id;
		 
		IF l_terminal_id IS NOT NULL THEN
			SELECT FEE_CURRENCY_ID
			INTO l_term_curr_id
			FROM REPORT.TERMINAL
			WHERE TERMINAL_ID = l_terminal_id;
			
            -- ensure that currency matches
            IF NVL(l_curr_id, 0) <> l_term_curr_id THEN
                RAISE_APPLICATION_ERROR(-20020, 'Terminal uses a different currency than the doc''s currency. Adjustment not added');
            END IF;
		END IF;
		 
        l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_customer_bank_id);
        l_status := GET_DOC_STATUS(l_doc_id);
        IF l_status NOT IN('O', 'L') THEN
            RAISE_APPLICATION_ERROR(-20021, 'This doc must be open or locked to add adjustments. Adjustment not added');
    	END IF;
		
		IF lc_split_payment_flag = 'Y' AND lc_split_payment_interval_cd IN ('D', 'W', 'M') AND ln_split_number_of_payments BETWEEN 2 AND 999 AND TRUNC(ld_first_split_payment_date) >= TRUNC(SYSDATE) THEN
			ln_split_payment_amt := ROUND(l_amt / ln_split_number_of_payments, 2);
			ln_count := 0;
			ld_payment_date := TRUNC(ld_first_split_payment_date);
			LOOP
				ln_count := ln_count + 1;
				IF ln_count >= ln_split_number_of_payments THEN
					ln_split_payment_amt := l_amt - ln_split_payment_amt * (ln_count - 1);
				END IF;
				
				SELECT MAX(B.BATCH_ID)
				  INTO l_batch_id
				  FROM BATCH B
				 WHERE B.DOC_ID = l_doc_id
				   AND NVL(B.TERMINAL_ID, 0) = NVL(l_terminal_id, 0)
				   AND B.PAYMENT_SCHEDULE_ID = 9
				   AND B.END_DATE = ld_payment_date;
			
				IF l_batch_id IS NULL THEN
					-- create a batch and set the doc id
					SELECT BATCH_SEQ.NEXTVAL
					  INTO l_batch_id
					  FROM DUAL;
					INSERT INTO BATCH(
						BATCH_ID,
						DOC_ID,
						TERMINAL_ID,
						PAYMENT_SCHEDULE_ID,
						START_DATE,
						END_DATE,
						BATCH_STATE_CD)
					  VALUES(
						l_batch_id,
						l_doc_id,
						DECODE(l_terminal_id, 0, NULL, l_terminal_id),
						9,
						ld_payment_date,
						ld_payment_date,
						'O');
				END IF;
				SELECT LEDGER_SEQ.NEXTVAL
				  INTO l_ledger_id
				  FROM DUAL;
				INSERT INTO LEDGER(
					LEDGER_ID,
					ENTRY_TYPE,
					AMOUNT,
					ENTRY_DATE,
					BATCH_ID,
					SETTLE_STATE_ID,
					LEDGER_DATE,
					DESCRIPTION,
					CREATE_BY,
					REPORT_DATE)
				   VALUES (
					l_ledger_id,
					'AD',
					ln_split_payment_amt,
					ld_payment_date,
					l_batch_id,
					2 /*process-no require*/,
					ld_payment_date,
					l_reason,
					l_user_id,
					ld_payment_date);
				-- create net revenue fee also
				INSERT INTO LEDGER(
					LEDGER_ID,
					RELATED_LEDGER_ID,
					SERVICE_FEE_ID,
					ENTRY_TYPE,
					AMOUNT,
					ENTRY_DATE,
					BATCH_ID,
					SETTLE_STATE_ID,
					LEDGER_DATE,
					DESCRIPTION,
					REPORT_DATE)
				 SELECT
					LEDGER_SEQ.NEXTVAL,
					l_ledger_id,
					SF.SERVICE_FEE_ID,
					'SF',
					-ln_split_payment_amt * SF.FEE_PERCENT,
					ld_payment_date,
					l_batch_id,
					2 /*process-no require*/,
					ld_payment_date,
					F.FEE_NAME,
					ld_payment_date
				  FROM SERVICE_FEES SF, FEES F
				 WHERE SF.FEE_ID = F.FEE_ID
				   AND SF.TERMINAL_ID = l_terminal_id
				   AND ld_payment_date BETWEEN NVL(SF.START_DATE, MIN_DATE)
						  AND NVL(SF.END_DATE, MAX_DATE)
				   AND SF.FREQUENCY_ID = 6;
				   
				IF ln_count >= ln_split_number_of_payments THEN
					EXIT;
				END IF;
				
				IF lc_split_payment_interval_cd = 'D' THEN
					ld_payment_date := ld_payment_date + 1;
				ELSIF lc_split_payment_interval_cd = 'W' THEN
					ld_payment_date := ld_payment_date + 7;
				ELSIF lc_split_payment_interval_cd = 'M' THEN
					ld_payment_date := ADD_MONTHS(ld_payment_date, 1);
				END IF;
			END LOOP;
		ELSE
			SELECT MAX(B.BATCH_ID)
			  INTO l_batch_id
			  FROM BATCH B
			 WHERE B.DOC_ID = l_doc_id
			   AND NVL(B.TERMINAL_ID, 0) = NVL(l_terminal_id, 0)
			   AND B.PAYMENT_SCHEDULE_ID = 1;
		
			IF l_batch_id IS NULL THEN
				-- create a batch and set the doc id
				SELECT BATCH_SEQ.NEXTVAL
				  INTO l_batch_id
				  FROM DUAL;
				INSERT INTO BATCH(
					BATCH_ID,
					DOC_ID,
					TERMINAL_ID,
					PAYMENT_SCHEDULE_ID,
					START_DATE,
					END_DATE,
					BATCH_STATE_CD)
				  VALUES(
					l_batch_id,
					l_doc_id,
					DECODE(l_terminal_id, 0, NULL, l_terminal_id),
					1,
					SYSDATE,
					DECODE(l_status, 'L', SYSDATE, 'O', null),
					DECODE(l_status, 'L', 'F', 'O', 'L'));
			END IF;
			SELECT LEDGER_SEQ.NEXTVAL, SYSDATE
			  INTO l_ledger_id, l_fee_date
			  FROM DUAL;
			INSERT INTO LEDGER(
				LEDGER_ID,
				ENTRY_TYPE,
				AMOUNT,
				ENTRY_DATE,
				BATCH_ID,
				SETTLE_STATE_ID,
				LEDGER_DATE,
				DESCRIPTION,
				CREATE_BY)
			   VALUES (
				l_ledger_id,
				'AD',
				l_amt,
				l_fee_date,
				l_batch_id,
				2 /*process-no require*/,
				l_fee_date,
				l_reason,
				l_user_id);
			-- create net revenue fee also
			INSERT INTO LEDGER(
				LEDGER_ID,
				RELATED_LEDGER_ID,
				SERVICE_FEE_ID,
				ENTRY_TYPE,
				AMOUNT,
				ENTRY_DATE,
				BATCH_ID,
				SETTLE_STATE_ID,
				LEDGER_DATE,
				DESCRIPTION)
			 SELECT
				LEDGER_SEQ.NEXTVAL,
				l_ledger_id,
				SF.SERVICE_FEE_ID,
				'SF',
				-l_amt * SF.FEE_PERCENT,
				l_fee_date,
				l_batch_id,
				2 /*process-no require*/,
				l_fee_date,
				F.FEE_NAME
			  FROM SERVICE_FEES SF, FEES F
			 WHERE SF.FEE_ID = F.FEE_ID
			   AND SF.TERMINAL_ID = l_terminal_id
			   AND l_fee_date BETWEEN NVL(SF.START_DATE, MIN_DATE)
					  AND NVL(SF.END_DATE, MAX_DATE)
			   AND SF.FREQUENCY_ID = 6;
		END IF;
    END;
	
PROCEDURE ADJUSTMENT_INS (
		l_ledger_id OUT NOCOPY LEDGER.LEDGER_ID%TYPE,
       	l_doc_id IN BATCH.DOC_ID%TYPE,
        l_terminal_id IN BATCH.TERMINAL_ID%TYPE,
    	l_reason IN LEDGER.DESCRIPTION%TYPE,
    	l_amt IN LEDGER.AMOUNT%TYPE,
    	l_user_id IN LEDGER.CREATE_BY%TYPE
) IS
BEGIN
	ADJUSTMENT_INS(l_ledger_id, l_doc_id, l_terminal_id, l_reason, l_amt, l_user_id, 'N', NULL, NULL, NULL);
END;
	
PROCEDURE ADJUSTMENT_INS (
	pn_customer_id CORP.CUSTOMER.CUSTOMER_ID%TYPE,
	pv_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
	pv_currency_cd CORP.CURRENCY.CURRENCY_CODE%TYPE,
	pv_reason CORP.LEDGER.DESCRIPTION%TYPE,
	pn_amount CORP.LEDGER.AMOUNT%TYPE,
	pn_doc_id OUT CORP.DOC.DOC_ID%TYPE,
	pn_ledger_id OUT CORP.LEDGER.LEDGER_ID%TYPE
) IS
	ln_currency_id CORP.CURRENCY.CURRENCY_ID%TYPE;
	ln_customer_bank_id CORP.CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE;
	ln_business_unit_id CORP.BUSINESS_UNIT.BUSINESS_UNIT_ID%TYPE;
	ln_user_id REPORT.USER_LOGIN.USER_ID%TYPE;
BEGIN
	SELECT MAX(customer_bank_id) INTO ln_customer_bank_id
	FROM (
		SELECT cbt.customer_bank_id
		FROM corp.customer_bank_terminal cbt
		JOIN report.terminal t ON cbt.terminal_id = t.terminal_id
		WHERE t.customer_id = pn_customer_id AND NVL(cbt.start_date, MIN_DATE) <= SYSDATE AND NVL(cbt.end_date, MAX_DATE) > SYSDATE
		GROUP BY cbt.customer_bank_id
		ORDER BY COUNT(1) DESC
	) WHERE ROWNUM = 1;
	
	IF ln_customer_bank_id IS NULL THEN
		RAISE_APPLICATION_ERROR(-20703, 'Unable to find a customer bank account for this customer');
	END IF;
	
	SELECT MAX(business_unit_id) INTO ln_business_unit_id
	FROM (
		SELECT business_unit_id
		FROM report.terminal
		WHERE customer_id = pn_customer_id AND status != 'D'
		GROUP BY business_unit_id
		ORDER BY COUNT(1) DESC
	) WHERE ROWNUM = 1;
	
	IF ln_business_unit_id IS NULL THEN
		RAISE_APPLICATION_ERROR(-20704, 'Unable to find a terminal business unit for this customer');
	END IF;
	
	SELECT MAX(user_id) INTO ln_user_id
	FROM report.user_login
	WHERE user_name = pv_user_name;
	
	SELECT currency_id
	INTO ln_currency_id
	FROM corp.currency
	WHERE currency_code = pv_currency_cd;
	
	pn_doc_id := GET_OR_CREATE_DOC(ln_customer_bank_id, ln_currency_id, ln_business_unit_id);
	ADJUSTMENT_INS(pn_ledger_id, pn_doc_id, NULL, pv_reason, pn_amount, ln_user_id);
END;	
    
    PROCEDURE                             ADJUSTMENT_UPD
       (l_ledger_id IN LEDGER.LEDGER_ID%TYPE,
       	l_reason IN LEDGER.DESCRIPTION%TYPE,
       	l_amt IN LEDGER.AMOUNT%TYPE,
    	l_user_id IN LEDGER.CREATE_BY%TYPE)
    IS
        l_status CORP.DOC.STATUS%TYPE;
    BEGIN
  	    l_status := FREEZE_DOC_STATUS_LEDGER_ID(l_ledger_id);
        IF l_status NOT IN('O', 'L') THEN
            RAISE_APPLICATION_ERROR(-20021, 'This doc must be open or locked to update adjustments. Adjustment not updated');
    	END IF;
        UPDATE LEDGER SET DESCRIPTION = l_reason, AMOUNT = l_amt, CREATE_BY = l_user_id
          WHERE LEDGER_ID = l_ledger_id;
         -- update net revenue
         UPDATE LEDGER L SET AMOUNT = -l_amt * (
            SELECT SF.FEE_PERCENT
              FROM SERVICE_FEES SF
             WHERE L.SERVICE_FEE_ID = SF.SERVICE_FEE_ID
               AND SF.FREQUENCY_ID = 6)
          WHERE L.RELATED_LEDGER_ID = l_ledger_id
            AND L.ENTRY_TYPE = 'SF'
            AND EXISTS(SELECT 1
                FROM SERVICE_FEES SF
                WHERE L.SERVICE_FEE_ID = SF.SERVICE_FEE_ID
                AND SF.FREQUENCY_ID = 6
            );
    END;

    PROCEDURE LOCK_DOC(
        l_doc_id DOC.DOC_ID%TYPE,
        l_user_id IN DOC.CREATE_BY%TYPE)
    IS
    BEGIN
        IF FREEZE_DOC_STATUS_DOC_ID(l_doc_id) = 'O' THEN
            UPDATE DOC
               SET STATUS = 'L', UPDATE_DATE = SYSDATE
             WHERE DOC_ID = l_doc_id;

            --Close all closeable batches
            UPDATE BATCH B
               SET B.BATCH_STATE_CD = 'F',
                   B.BATCH_CLOSED_TS = SYSDATE,
                   B.END_DATE = NVL(B.END_DATE, SYSDATE)
             WHERE B.DOC_ID = l_doc_id
               AND B.BATCH_STATE_CD = 'L';

            UPDATE BATCH B
               SET B.BATCH_STATE_CD = (SELECT DECODE(COUNT(*), 0, 'D', 'U')
                      FROM REPORT.TERMINAL t
                      JOIN CORP.BATCH_CONFIRM CN ON t.CUSTOMER_ID = cn.CUSTOMER_ID
                     WHERE b.TERMINAL_ID = t.TERMINAL_ID
                       AND b.PAYMENT_SCHEDULE_ID = cn.PAYMENT_SCHEDULE_ID),
                   B.BATCH_CLOSED_TS = SYSDATE
             WHERE B.DOC_ID = l_doc_id
               AND B.BATCH_STATE_CD = 'O'
               AND B.PAYMENT_SCHEDULE_ID NOT IN(1,2,5,7,8)
               AND B.END_DATE <= SYSDATE;

            -- Remove unclosed batches
            DECLARE
                l_cnt PLS_INTEGER;
                l_cb_id DOC.CUSTOMER_BANK_ID%TYPE;
                l_cur_id DOC.CURRENCY_ID%TYPE;
                l_bu_id DOC.BUSINESS_UNIT_ID%TYPE;
                l_new_doc_id DOC.DOC_ID%TYPE;
            BEGIN
                SELECT COUNT(*)
                  INTO l_cnt
                  FROM BATCH B
                 WHERE B.DOC_ID = l_doc_id
                   AND BATCH_CLOSABLE(B.BATCH_ID, B.PAYMENT_SCHEDULE_ID, B.END_DATE, B.BATCH_STATE_CD) = 'N';
                IF l_cnt > 0 THEN
                    SELECT D.CUSTOMER_BANK_ID, D.CURRENCY_ID, D.BUSINESS_UNIT_ID
                      INTO l_cb_id, l_cur_id, l_bu_id
                      FROM DOC D
                    WHERE D.DOC_ID = l_doc_id;
                    l_new_doc_id := GET_OR_CREATE_DOC(l_cb_id, l_cur_id, l_bu_id);
                   UPDATE BATCH B
                       SET B.DOC_ID = l_new_doc_id
                     WHERE B.DOC_ID = l_doc_id
                       AND BATCH_CLOSABLE(B.BATCH_ID, B.PAYMENT_SCHEDULE_ID, B.END_DATE, B.BATCH_STATE_CD) = 'N';
                END IF;
            END;
            
            -- Remove any unsettled trans from as batches
            DECLARE
                CURSOR l_cur IS
                  SELECT L.LEDGER_ID, L.BATCH_ID, L.ENTRY_DATE
                    FROM LEDGER L
                   WHERE ENTRY_PAYABLE(L.SETTLE_STATE_ID, L.ENTRY_TYPE) = '?'
                     AND L.BATCH_ID IN(
                            SELECT B.BATCH_ID
                              FROM BATCH B
                             WHERE B.DOC_ID = l_doc_id);
                TYPE t_rec_list IS TABLE OF l_cur%ROWTYPE;
                l_recs t_rec_list;
                l_new_batch_id LEDGER.BATCH_ID%TYPE;
            BEGIN
                OPEN l_cur;
                FETCH l_cur BULK COLLECT INTO l_recs;
                CLOSE l_cur;

                IF l_recs.FIRST IS NOT NULL THEN
                    FOR i IN l_recs.FIRST .. l_recs.LAST LOOP
                        l_new_batch_id := GET_NEW_BATCH(l_recs(i).BATCH_ID, l_recs(i).ENTRY_DATE);
                        UPDATE LEDGER L
                           SET L.BATCH_ID = l_new_batch_id
                         WHERE L.LEDGER_ID = l_recs(i).LEDGER_ID;
                    END LOOP;
                END IF;
            END;

            -- add rounding entries
            ADD_BATCH_ROUNDING_ENTRIES(l_doc_id);
            ADD_ROUNDING_ENTRY(l_doc_id);
        END IF;
    END;

    PROCEDURE      APPROVE_PAYMENT (
      l_doc_id IN DOC.DOC_ID%TYPE,
      l_user_id IN DOC.APPROVE_BY%TYPE)
    IS
      l_total DOC.TOTAL_AMOUNT%TYPE;
    BEGIN
        IF FREEZE_DOC_STATUS_DOC_ID(l_doc_id) = 'L' THEN
            -- add Positive Net Revenue Adjustment, if necessary
            DECLARE
                l_nrf_amt LEDGER.AMOUNT%TYPE;
                l_fee_date LEDGER.ENTRY_DATE%TYPE;
                l_ledger_id LEDGER.LEDGER_ID%TYPE;
                l_batch_id BATCH.BATCH_ID%TYPE;
            BEGIN
                SELECT NVL(SUM(AMOUNT), 0), SYSDATE
                  INTO l_nrf_amt, l_fee_date
                  FROM LEDGER L, BATCH B, SERVICE_FEES SF
                 WHERE L.BATCH_ID = B.BATCH_ID
                   AND B.DOC_ID = l_doc_id
                   AND L.DELETED = 'N'
                   AND L.ENTRY_TYPE = 'SF'
                   AND L.SERVICE_FEE_ID = SF.SERVICE_FEE_ID
                   AND SF.FREQUENCY_ID = 6;
                 IF l_nrf_amt > 0 THEN
                    -- l_nrf_amt is positive (money to the customer), so back it out with an adjustment
                    SELECT MAX(B.BATCH_ID)
                      INTO l_batch_id
                      FROM BATCH B
                     WHERE B.DOC_ID = l_doc_id
                       AND B.PAYMENT_SCHEDULE_ID = 7;
                    IF l_batch_id IS NULL THEN
                	    SELECT BATCH_SEQ.NEXTVAL
                          INTO l_batch_id
                          FROM DUAL;
                        INSERT INTO BATCH(
                            BATCH_ID,
                            DOC_ID,
                            TERMINAL_ID,
                            PAYMENT_SCHEDULE_ID,
                            START_DATE,
                            END_DATE,
                            BATCH_STATE_CD)
                          VALUES(
                            l_batch_id,
                            l_doc_id,
                            NULL,
                            7,
                            l_fee_date,
                            l_fee_date,
                            'F');
                    END IF;
                	SELECT LEDGER_SEQ.NEXTVAL
                      INTO l_ledger_id
                      FROM DUAL;
                	INSERT INTO LEDGER(
                        LEDGER_ID,
                        ENTRY_TYPE,
                        AMOUNT,
                        ENTRY_DATE,
                        BATCH_ID,
                        SETTLE_STATE_ID,
                        LEDGER_DATE,
                        DESCRIPTION,
                        CREATE_BY)
                       VALUES (
                        l_ledger_id,
                        'AD',
                        -l_nrf_amt,
                        l_fee_date,
                        l_batch_id,
                        2 /*process-no require*/,
                        l_fee_date,
                        'Net Revenue Hurdle Not Met',
                        l_user_id);
                 END IF;
            END;
            ADD_ROUNDING_ENTRY(l_doc_id);
            SELECT NVL(SUM(AMOUNT), 0)
              INTO l_total
              FROM LEDGER L, BATCH B
             WHERE L.DELETED = 'N'
               AND ENTRY_PAYABLE(L.SETTLE_STATE_ID,L.ENTRY_TYPE) = 'Y'
               AND L.BATCH_ID = B.BATCH_ID
               AND B.DOC_ID = l_doc_id;

        	UPDATE DOC D
               SET APPROVE_BY = l_user_id,
                   UPDATE_DATE = SYSDATE,
                   STATUS = DECODE(l_total, 0, 'D', 'A'),
                   TOTAL_AMOUNT = l_total,
                   DOC_TYPE = (
                        SELECT DECODE(CB.IS_EFT, 'Y', 'E', 'P') -- electronic or paper
                               || CASE WHEN l_total > 0 THEN 'C' ELSE 'D' END -- credit or debit
                          FROM CUSTOMER_BANK CB
                         WHERE D.CUSTOMER_BANK_ID = CB.CUSTOMER_BANK_ID)
      	 		WHERE DOC_ID = l_doc_id;
          END IF;
    END;
    
    PROCEDURE      UNAPPROVE_PAYMENT (
      l_doc_id IN DOC.DOC_ID%TYPE)
    IS
    BEGIN
        UPDATE DOC D SET APPROVE_BY = NULL, UPDATE_DATE = SYSDATE, STATUS = 'L',
            TOTAL_AMOUNT = NULL, DOC_TYPE = '--'
  	 		WHERE DOC_ID = l_doc_id
              AND STATUS = 'A';
        IF SQL%FOUND THEN
            -- Remove adjustments for positive net revenue fees
            UPDATE LEDGER
               SET DELETED = 'Y'
             WHERE ENTRY_TYPE = 'AD'
               AND TRANS_ID IS NULL
               AND BATCH_ID IN(
                    SELECT BATCH_ID
                      FROM BATCH
                     WHERE DOC_ID = l_doc_id
                       AND PAYMENT_SCHEDULE_ID = 7);
        END IF;
    END;
    
    PROCEDURE      UPLOAD_TO_ORF (
      l_doc_id IN DOC.DOC_ID%TYPE)
    IS
    BEGIN
    	 INSERT INTO USAT_INVOICE_INTERFACE@FINANCIALS(
            VENDOR_NAME_I, --                                        VARCHAR2 (240)                        REQUIRED     vendor name
            INVOICE_NUMBER_I, --                                    VARCHAR2 (240)                       REQUIRED      invoice number
            INVOICE_DATE_I, --                                           DATE                                           OPTIONAL
            INVOICE_AMOUNT_I, --                                    NUMBER                                     REQUIRED      amount of invoice
            DISTRIBUTION_SET_NAME_I, --                     VARCHAR2 (240)                       REQUIRED
            GL_DATE_I, --                                                      DATE                                            OPTIONAL
            PAY_GROUP_LOOKUP_CODE_I, --                 VARCHAR2 (240)                        REQUIRED     pay group
            VENDOR_SITE_CODE_I, --                                VARCHAR2 (25)                          REQUIRED     site code
            INVOICE_CURRENCY_CODE_I, --                  VARCHAR2 (15)                           REQUIRED     invoice currency
            PAYMENT_METHOD_LOOKUP_CODE_I, --   VARCHAR2 (25)                         REQUIRED     payment method
            VENDOR_NUMBER_I) --                                    VARCHAR2 (200)                        OPTIONAL
        SELECT
            C.CUSTOMER_NAME,
            D.REF_NBR,
            SYSDATE,
            D.TOTAL_AMOUNT,
            BU.DISTRIBUTION_SET_CD,
            SYSDATE,
            BU.PAY_GROUP_CD,
            CB.CUSTOMER_BANK_ID,
            CU.CURRENCY_CODE,
            DECODE(CB.IS_EFT, 'Y', 'EFT', 'CHECK'),
            C.CUSTOMER_ID
        FROM CORP.CUSTOMER C, CORP.CUSTOMER_BANK CB, CORP.DOC D, CORP.BUSINESS_UNIT BU, CORP.CURRENCY CU
        WHERE D.DOC_ID = l_doc_id
        AND D.CUSTOMER_BANK_ID = CB.CUSTOMER_BANK_ID
        AND CB.CUSTOMER_ID = C.CUSTOMER_ID
        AND D.CURRENCY_ID = CU.CURRENCY_ID
        AND D.BUSINESS_UNIT_ID = BU.BUSINESS_UNIT_ID
		AND NVL(D.TOTAL_AMOUNT, 0) != 0;
    END;
    
    PROCEDURE      MARK_DOC_PAID (
      l_doc_id IN DOC.DOC_ID%TYPE,
      l_user_id IN DOC.APPROVE_BY%TYPE)
    IS
        l_paid_date DATE := SYSDATE;
    BEGIN
    	 UPDATE DOC SET UPDATE_DATE = l_paid_date, STATUS = 'P'
          WHERE DOC_ID = l_doc_id
            AND STATUS = 'A';
         IF SQL%FOUND THEN
             UPDATE LEDGER SET PAID_DATE = l_paid_date
              WHERE BATCH_ID IN(SELECT BATCH_ID FROM BATCH WHERE DOC_ID = l_doc_id);
             UPDATE BATCH SET PAID_DATE = l_paid_date
              WHERE DOC_ID = l_doc_id;
			 UPLOAD_TO_ORF(l_doc_id);
			 UPDATE DOC SET SENT_BY = l_user_id, SENT_DATE = l_paid_date
			 WHERE DOC_ID = l_doc_id;
         END IF;
    END;
    
    PROCEDURE PROCESS_FEES_UPD
       (l_terminal_id IN PROCESS_FEES.TERMINAL_ID%TYPE,
        l_trans_type_id IN PROCESS_FEES.TRANS_TYPE_ID%TYPE,
        l_fee_percent IN PROCESS_FEES.FEE_PERCENT%TYPE,
        l_fee_amount IN PROCESS_FEES.FEE_AMOUNT%TYPE,
        l_fee_minimum IN PROCESS_FEES.MIN_AMOUNT%TYPE,
        l_effective_date IN PROCESS_FEES.END_DATE%TYPE DEFAULT SYSDATE,
        l_override IN CHAR DEFAULT 'N')
    IS
		l_lock VARCHAR2(128);
		l_current_date PROCESS_FEES.END_DATE%TYPE := SYSDATE;
		l_fee_effective_date PROCESS_FEES.END_DATE%TYPE := NVL(l_effective_date, l_current_date);
		l_fee_override CHAR := NVL(l_override, 'N');
    BEGIN
    	 --CHECK IF WE already paid on transactions before the effective date
    	 IF l_fee_override <> 'Y' AND l_fee_effective_date < l_current_date THEN
    	 	 DECLARE
    		     l_tran_id LEDGER.TRANS_ID%TYPE;
    			BEGIN
    		 	 SELECT /*+ index(T USAT_IX_TRANS_TERMINAL_ID) */ MIN(L.TRANS_ID)
                   INTO l_tran_id
                   FROM LEDGER L, BATCH B, TRANS T, DOC D
                  WHERE T.TRAN_ID = L.TRANS_ID
                    AND T.TRANS_TYPE_ID = l_trans_type_id
                    AND T.TERMINAL_ID = l_terminal_id
                    AND T.CLOSE_DATE >= l_fee_effective_date
                    AND B.TERMINAL_ID = l_terminal_id
                    AND L.BATCH_ID = B.BATCH_ID
                    AND B.DOC_ID = D.DOC_ID
                    AND L.DELETED = 'N'
                    AND D.STATUS NOT IN('O', 'D');
    			 IF l_tran_id IS NOT NULL THEN  --BIG PROBLEM
    			 	 RAISE_APPLICATION_ERROR(-20011, 'Transaction #' || TO_CHAR(l_tran_id) || ', which ocurred on the terminal whose fees you are updating, has already been paid.');
    			 END IF;
    		 END;
    	 END IF;
		 
		 l_lock := GLOBALS_PKG.REQUEST_LOCK('TERMINAL.TERMINAL_ID', l_terminal_id);
		 
    	 UPDATE PROCESS_FEES SET END_DATE = l_fee_effective_date WHERE TERMINAL_ID = l_terminal_id
    	 	AND TRANS_TYPE_ID = l_trans_type_id AND NVL(END_DATE, l_fee_effective_date) >= l_fee_effective_date;
    	 BEGIN
            DELETE FROM PROCESS_FEES WHERE TERMINAL_ID = l_terminal_id
        	 	AND TRANS_TYPE_ID = l_trans_type_id AND NVL(START_DATE, MIN_DATE) >= NVL(END_DATE, MAX_DATE);	
         EXCEPTION
            WHEN CHILD_RECORD_FOUND THEN
                NULL;
            WHEN OTHERS THEN
                RAISE;
         END;
         IF NVL(l_fee_percent, 0) <> 0 OR NVL(l_fee_amount, 0) <> 0 THEN	
        	 INSERT INTO PROCESS_FEES(PROCESS_FEE_ID, TERMINAL_ID, TRANS_TYPE_ID, FEE_PERCENT, FEE_AMOUNT, MIN_AMOUNT, START_DATE)
         	    VALUES(PROCESS_FEE_SEQ.NEXTVAL, l_terminal_id, l_trans_type_id, l_fee_percent, l_fee_amount, l_fee_minimum, l_fee_effective_date);
 	     END IF;
    END;
    
    FUNCTION GET_SERVICE_FEE_DATE_FMT(
        l_months FREQUENCY.MONTHS%TYPE,
        l_days FREQUENCY.DAYS%TYPE)
     RETURN VARCHAR
    IS
    BEGIN
        IF l_months >= 12  AND MOD(l_months, 12.0) = 0 THEN
            RETURN 'FMYYYY';
        ELSIF l_months > 0 THEN
            RETURN 'FMMonth, YYYY';
        ELSIF l_days >= 1 AND MOD(l_days, 1.0) = 0 THEN
            RETURN 'FMMM/DD/YYYY';
        ELSE
            RETURN 'FMMM/DD/YYYY HH:MI AM';
        END IF;
    END;
    
    PROCEDURE SCAN_FOR_SERVICE_FEES
    IS
        CURSOR c_fee
        IS
            SELECT DISTINCT
                   SF.SERVICE_FEE_ID,
                   SF.TERMINAL_ID,
                   CBT.CUSTOMER_BANK_ID,
                   F.FEE_ID,
                   SF.FEE_AMOUNT,
                   Q.MONTHS,
                   Q.DAYS,
                   SF.LAST_PAYMENT,
                   SF.START_DATE,
                   LEAST(NVL(
                        CASE WHEN Q.MONTHS = 0 THEN SF.END_DATE + Q.DAYS
                             ELSE TRUNC(LAST_DAY(ADD_MONTHS(SF.END_DATE, Q.MONTHS)), 'DD')
                        END
                   , MAX_DATE), SYSDATE) EFFECTIVE_END_DATE,
                   F.FEE_NAME,
                   T.FEE_CURRENCY_ID,
                   DECODE(T.PAYMENT_SCHEDULE_ID, 2, 'Y', 8, 'Y', 'N') AS_ACCUM,
                   F.INITIATION_TYPE_CD,
                   SF.GRACE_PERIOD_DATE,
				   SF.TRIGGERING_DATE,
				   SF.NO_TRIGGER_EVENT_FLAG
              FROM CORP.SERVICE_FEES SF
              JOIN CORP.FREQUENCY Q ON SF.FREQUENCY_ID = Q.FREQUENCY_ID
              JOIN CORP.CUSTOMER_BANK_TERMINAL CBT ON SF.TERMINAL_ID = CBT.TERMINAL_ID AND SYSDATE >= NVL(CBT.START_DATE, MIN_DATE) AND SYSDATE < NVL(CBT.END_DATE, MAX_DATE)
              JOIN CORP.FEES F ON SF.FEE_ID = F.FEE_ID
              JOIN REPORT.TERMINAL T ON SF.TERMINAL_ID = T.TERMINAL_ID              
             WHERE (Q.DAYS > 0 OR Q.MONTHS > 0)
               AND CASE WHEN Q.MONTHS = 0 THEN NVL(SF.LAST_PAYMENT, MIN_DATE) + Q.DAYS
                             ELSE TRUNC(LAST_DAY(ADD_MONTHS(NVL(SF.LAST_PAYMENT, MIN_DATE), Q.MONTHS)), 'DD')
                        END < LEAST(NVL(
                        CASE WHEN Q.MONTHS = 0 THEN SF.END_DATE + Q.DAYS
                             ELSE TRUNC(LAST_DAY(ADD_MONTHS(SF.END_DATE, Q.MONTHS)), 'DD')
                        END
                   , MAX_DATE), SYSDATE)
               AND (SF.LAST_PAYMENT IS NOT NULL 
				OR SF.TRIGGERING_DATE IS NOT NULL AND (F.INITIATION_TYPE_CD = 'T'
					OR F.INITIATION_TYPE_CD = 'G' AND COALESCE(SF.NO_TRIGGER_EVENT_FLAG, 'N') != 'Y')
				OR SF.GRACE_PERIOD_DATE < SYSDATE)
             ORDER BY SF.LAST_PAYMENT NULLS LAST, CBT.CUSTOMER_BANK_ID, SF.SERVICE_FEE_ID;

        l_last_payment                     SERVICE_FEES.LAST_PAYMENT%TYPE;
        l_fmt VARCHAR2(50);
        l_ledger_id LEDGER.LEDGER_ID%TYPE;
        l_batch_id LEDGER.BATCH_ID%TYPE;
		l_last_run_complete_ts DATE := TO_DATE(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('SCAN_FOR_SERVICE_FEES_LAST_RUN_COMPLETE_TS'), 'MM/DD/YYYY HH24:MI:SS');
    BEGIN
		IF l_last_run_complete_ts >= TO_DATE(TO_CHAR(CURRENT_TIMESTAMP, 'MM/DD/YYYY'), 'MM/DD/YYYY') 
			OR DBADMIN.PKG_GLOBAL.GET_APP_SETTING('SCAN_FOR_SERVICE_FEES_ENABLED') = 'N' THEN
			RETURN;
		END IF;
		
		UPDATE ENGINE.APP_SETTING
   		SET APP_SETTING_VALUE = TO_CHAR(CURRENT_TIMESTAMP, 'MM/DD/YYYY HH24:MI:SS')
   		WHERE APP_SETTING_CD = 'SCAN_FOR_SERVICE_FEES_LAST_RUN_START_TS';
		COMMIT;
	
        FOR r_fee IN c_fee LOOP
            -- If last payment is null, Use first transaction as last payment date
            IF r_fee.LAST_PAYMENT IS NULL THEN
                IF r_fee.INITIATION_TYPE_CD = 'I' THEN
                    l_last_payment := r_fee.START_DATE;
                ELSIF r_fee.INITIATION_TYPE_CD IN('G', 'T') THEN
					 l_last_payment := r_fee.TRIGGERING_DATE;
                     IF r_fee.INITIATION_TYPE_CD = 'G' AND r_fee.GRACE_PERIOD_DATE < NVL(l_last_payment, SYSDATE) THEN 
                        l_last_payment := r_fee.GRACE_PERIOD_DATE;
						UPDATE CORP.SERVICE_FEES
                        SET TRIGGERING_DATE = l_last_payment
                        WHERE SERVICE_FEE_ID = r_fee.SERVICE_FEE_ID;
                     END IF;
                     IF l_last_payment IS NOT NULL THEN
                         IF r_fee.INITIATION_TYPE_CD = 'G' THEN
                             IF r_fee.MONTHS > 0 THEN
                                SELECT TRUNC(LAST_DAY(ADD_MONTHS(l_last_payment, -r_fee.MONTHS)), 'DD')
                                  INTO l_last_payment
                                  FROM DUAL;
                             ELSE
                                SELECT l_last_payment - r_fee.DAYS
                                  INTO l_last_payment
                                  FROM DUAL;
                             END IF;
                        END IF;
                    END IF;
                ELSE
                    l_last_payment := NULL;
                END IF;
            ELSE
                l_last_payment := r_fee.LAST_PAYMENT;
            END IF;
            IF l_last_payment IS NOT NULL THEN
              l_fmt := GET_SERVICE_FEE_DATE_FMT(r_fee.MONTHS, r_fee.DAYS);
              LOOP
                  IF r_fee.MONTHS > 0 THEN
                      SELECT TRUNC(LAST_DAY(ADD_MONTHS(l_last_payment, r_fee.MONTHS)), 'DD')
                        INTO l_last_payment
                        FROM DUAL;
                  ELSE
                      SELECT l_last_payment + r_fee.DAYS
                        INTO l_last_payment
                        FROM DUAL;
                  END IF;
                  EXIT WHEN l_last_payment >= r_fee.EFFECTIVE_END_DATE;
  
                  DECLARE
                     l_fee_amt CORP.LEDGER.AMOUNT%TYPE;
                     l_desc CORP.LEDGER.DESCRIPTION%TYPE;
                     l_hosts PLS_INTEGER;
                  BEGIN -- catch exception here
                      -- skip creation of fee if start date is after fee date
                      IF l_last_payment >= NVL(r_fee.start_date, l_last_payment) THEN
                          l_batch_id := GET_OR_CREATE_BATCH(
                                      r_fee.TERMINAL_ID,
                                      r_fee.CUSTOMER_BANK_ID,
                                      l_last_payment,
                                      r_fee.FEE_CURRENCY_ID,
                                      r_fee.AS_ACCUM);
                          SELECT LEDGER_SEQ.NEXTVAL
                            INTO l_ledger_id
                            FROM DUAL;
                          IF r_fee.fee_id = 9 THEN
                             SELECT MAX(DEVICE_HOST_COUNT(e.EPORT_SERIAL_NUM, l_last_payment))
                               INTO l_hosts
                               FROM REPORT.EPORT e, REPORT.TERMINAL_EPORT te
                              WHERE e.EPORT_ID = te.EPORT_ID
                                AND te.TERMINAL_ID = r_fee.TERMINAL_ID
                                AND l_last_payment BETWEEN NVL(te.START_DATE, MIN_DATE) AND NVL(te.END_DATE, MAX_DATE);
                             l_fee_amt := -ABS(r_fee.FEE_AMOUNT * l_hosts);
                             l_desc := r_fee.FEE_NAME || ' ('||TO_CHAR(l_hosts)||' hosts) ' || ' for ' || TO_CHAR(l_last_payment, l_fmt);
                          ELSE
                             l_fee_amt := -ABS(r_fee.FEE_AMOUNT);
                             l_desc := r_fee.FEE_NAME || ' for ' || TO_CHAR(l_last_payment, l_fmt);
                          END IF;
                          INSERT INTO LEDGER(
                              LEDGER_ID,
                              ENTRY_TYPE,
                              SERVICE_FEE_ID,
                              AMOUNT,
                              ENTRY_DATE,
                              BATCH_ID,
                              SETTLE_STATE_ID,
                              LEDGER_DATE,
                              DESCRIPTION)
                          VALUES (
                              l_ledger_id,
                              'SF',
                              r_fee.SERVICE_FEE_ID,
                              l_fee_amt,
                              l_last_payment,
                              l_batch_id,
                              2,
                              l_last_payment,
                              l_desc);
                          -- create net revenue fee on transaction, if any
                          INSERT INTO LEDGER(
                              LEDGER_ID,
                              RELATED_LEDGER_ID,
                              ENTRY_TYPE,
                              SERVICE_FEE_ID,
                              AMOUNT,
                              ENTRY_DATE,
                              BATCH_ID,
                              SETTLE_STATE_ID,
                              LEDGER_DATE,
                              DESCRIPTION)
                          SELECT
                              LEDGER_SEQ.NEXTVAL,
                              l_ledger_id,
                              'SF',
                              SF.SERVICE_FEE_ID,
                              -l_fee_amt * SF.FEE_PERCENT,
                              l_last_payment,
                              l_batch_id,
                              2,
                              l_last_payment,
                              F.FEE_NAME
                          FROM SERVICE_FEES SF, FEES F
                         WHERE SF.FEE_ID = F.FEE_ID
                           AND SF.TERMINAL_ID = r_fee.TERMINAL_ID
                           AND l_last_payment BETWEEN NVL(SF.START_DATE, MIN_DATE)
                                AND NVL(SF.END_DATE, MAX_DATE)
                           AND SF.FREQUENCY_ID = 6;
                      END IF;
  
                      UPDATE SERVICE_FEES
                         SET LAST_PAYMENT = l_last_payment,
                             FIRST_PAYMENT = CASE WHEN FIRST_PAYMENT IS NULL OR l_last_payment < FIRST_PAYMENT THEN l_last_payment ELSE FIRST_PAYMENT END
                       WHERE SERVICE_FEE_ID =  r_fee.SERVICE_FEE_ID;
  
                      COMMIT;             -- BECAREFUL OF CALLING THIS PROCEDURE SINCE IT HAS THIS COMMIT
                  EXCEPTION
                                   WHEN DUP_VAL_ON_INDEX THEN
                                           ROLLBACK;
                  END;
              END LOOP;
            END IF;
        END LOOP;
		
		UPDATE ENGINE.APP_SETTING
   		SET APP_SETTING_VALUE = TO_CHAR(CURRENT_TIMESTAMP, 'MM/DD/YYYY HH24:MI:SS')
   		WHERE APP_SETTING_CD = 'SCAN_FOR_SERVICE_FEES_LAST_RUN_COMPLETE_TS';
    END;

    PROCEDURE SERVICE_FEES_UPD(
        l_terminal_id IN SERVICE_FEES.TERMINAL_ID%TYPE,
        l_fee_id IN SERVICE_FEES.FEE_ID%TYPE,
        l_freq_id IN SERVICE_FEES.FREQUENCY_ID%TYPE,
        l_fee_amt IN SERVICE_FEES.FEE_AMOUNT%TYPE,
        l_fee_perc IN SERVICE_FEES.FEE_PERCENT%TYPE,
        l_effective_date IN SERVICE_FEES.END_DATE%TYPE DEFAULT SYSDATE,
        l_end_date IN SERVICE_FEES.END_DATE%TYPE,
        l_override IN CHAR DEFAULT 'N',
        pn_grace_days IN NUMBER DEFAULT 60,
		pc_no_trigger_event_flag IN SERVICE_FEES.NO_TRIGGER_EVENT_FLAG%TYPE DEFAULT NULL)
    IS
        l_last_payment SERVICE_FEES.LAST_PAYMENT%TYPE;
        l_sf_id SERVICE_FEES.SERVICE_FEE_ID%TYPE;
        l_batch_id LEDGER.BATCH_ID%TYPE;
        l_next_payment SERVICE_FEES.LAST_PAYMENT%TYPE;
        l_first_payment SERVICE_FEES.LAST_PAYMENT%TYPE;
		l_lock VARCHAR2(128);
		l_current_date SERVICE_FEES.END_DATE%TYPE := SYSDATE;
		l_fee_effective_date SERVICE_FEES.END_DATE%TYPE := NVL(l_effective_date, l_current_date);
		l_fee_override CHAR := NVL(l_override, 'N');
        l_orig_start_date SERVICE_FEES.START_DATE%TYPE;
        l_orig_end_date SERVICE_FEES.END_DATE%TYPE;
        l_orig_amount SERVICE_FEES.FEE_AMOUNT%TYPE;
        l_orig_perc SERVICE_FEES.FEE_PERCENT%TYPE; 
        lc_initiation_type_cd CORP.FEES.INITIATION_TYPE_CD%TYPE;
    BEGIN
		SELECT INITIATION_TYPE_CD
          INTO lc_initiation_type_cd
          FROM CORP.FEES
         WHERE FEE_ID = l_fee_id;
         
        l_lock := GLOBALS_PKG.REQUEST_LOCK('TERMINAL.TERMINAL_ID', l_terminal_id);
        
        BEGIN
            IF lc_initiation_type_cd = 'P' THEN
                --disallow change to net revenue fees that affect already paid entries
                SELECT MAX(ENTRY_DATE)
                  INTO l_last_payment
                  FROM LEDGER L, BATCH B, SERVICE_FEES SF, DOC D
                 WHERE B.TERMINAL_ID = l_terminal_id
                   AND SF.TERMINAL_ID = l_terminal_id
                   AND SF.FEE_ID = l_fee_id
                   AND SF.FREQUENCY_ID = l_freq_id
                   AND B.DOC_ID = D.DOC_ID
                   AND D.STATUS NOT IN('O', 'D')
                   AND B.BATCH_ID = L.BATCH_ID
                   AND L.SERVICE_FEE_ID = SF.SERVICE_FEE_ID
                   AND L.ENTRY_TYPE = 'SF'
                   AND L.DELETED = 'N';
                IF NVL(l_last_payment, l_fee_effective_date) > l_fee_effective_date THEN -- BIG PROBLEM
                    RAISE_APPLICATION_ERROR(-20012, 'This net revenue fee was charged to the customer after the specified effective date.');
                ELSE
                    l_last_payment := MIN_DATE;
                END IF;
            ELSE
                SELECT MAX(LAST_PAYMENT), 
                       NVL(MIN(FIRST_PAYMENT), MIN_DATE),
                       CASE WHEN Q.MONTHS = 0 THEN l_fee_effective_date + Q.DAYS
                           ELSE TRUNC(LAST_DAY(ADD_MONTHS(l_fee_effective_date, Q.MONTHS)), 'DD')
                      END
                  INTO l_last_payment, l_first_payment, l_next_payment
                  FROM SERVICE_FEES SF, CORP.FREQUENCY Q
                 WHERE SF.TERMINAL_ID = l_terminal_id
                   AND SF.FEE_ID = l_fee_id
                   AND SF.FREQUENCY_ID = l_freq_id
                   AND SF.FREQUENCY_ID = Q.FREQUENCY_ID
                 GROUP BY CASE WHEN Q.MONTHS = 0 THEN l_fee_effective_date + Q.DAYS
                           ELSE TRUNC(LAST_DAY(ADD_MONTHS(l_fee_effective_date, Q.MONTHS)), 'DD')
                      END;
                IF l_last_payment is not null AND l_last_payment > l_next_payment THEN
                    IF l_fee_override <> 'Y' THEN --BIG PROBLEM
                        RAISE_APPLICATION_ERROR(-20011, 'This service fee was charged to the customer after the specified effective date.');
                    END IF;
                END IF;
                IF l_last_payment is not null AND l_last_payment > l_end_date THEN
                    IF l_fee_override <> 'Y' THEN --BIG PROBLEM
                        RAISE_APPLICATION_ERROR(-20011, 'This service fee was charged to the customer after the specified end date.');
                    END IF;
                END IF;
            END IF;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
            	 NULL;
            WHEN OTHERS THEN
            	 RAISE;
        END;
        IF lc_initiation_type_cd = 'P' THEN
            --delete any old net revenue fees >= l_fee_effective_date
            DELETE FROM LEDGER L
             WHERE L.ENTRY_TYPE = 'SF'
               AND L.SERVICE_FEE_ID IN(
                 SELECT SF.SERVICE_FEE_ID
                   FROM SERVICE_FEES SF
                  WHERE SF.TERMINAL_ID = l_terminal_id
                    AND SF.FEE_ID = l_fee_id
                    AND SF.FREQUENCY_ID = l_freq_id
                    --AND NVL(SF.END_DATE, l_fee_effective_date) >= l_fee_effective_date
                    )
               AND L.ENTRY_DATE >= l_fee_effective_date
               AND NOT EXISTS(
                    SELECT 1
                    FROM BATCH B, DOC D
                    WHERE B.BATCH_ID = L.BATCH_ID
                    AND B.DOC_ID = D.DOC_ID
                    AND D.STATUS NOT IN('O', 'D'));
        END IF;
        
        SELECT MAX(START_DATE), MAX(END_DATE), MAX(FEE_AMOUNT), MAX(FEE_PERCENT), MAX(SERVICE_FEE_ID)
          INTO l_orig_start_date, l_orig_end_date, l_orig_amount, l_orig_perc, l_sf_id
          FROM (SELECT * FROM CORP.SERVICE_FEES SF
		 WHERE SF.TERMINAL_ID = l_terminal_id
           AND SF.FEE_ID = l_fee_id
           AND SF.FREQUENCY_ID = l_freq_id
         ORDER BY NVL(END_DATE, MAX_DATE) DESC)
        WHERE ROWNUM = 1;
        
        IF l_sf_id IS NOT NULL AND ((lc_initiation_type_cd = 'P' AND NVL(l_orig_perc, 0) = NVL(l_fee_perc, 0)) OR (lc_initiation_type_cd != 'P' AND NVL(l_orig_amount, 0) = NVL(l_fee_amt, 0))) AND l_fee_effective_date <= NVL(l_orig_start_date, MIN_DATE) AND NVL(l_end_date, MAX_DATE) >= NVL(l_orig_start_date, MIN_DATE) THEN
            -- first update other records that may be affected
            UPDATE SERVICE_FEES 
               SET END_DATE = l_fee_effective_date
             WHERE TERMINAL_ID = l_terminal_id
               AND FEE_ID = l_fee_id
               AND FREQUENCY_ID = l_freq_id
               AND NVL(END_DATE, l_fee_effective_date) >= l_fee_effective_date
               AND SERVICE_FEE_ID != l_sf_id;

            -- update existing record
            UPDATE SERVICE_FEES 
               SET END_DATE = l_end_date,
                   START_DATE = l_fee_effective_date,
                   GRACE_PERIOD_DATE = DECODE(lc_initiation_type_cd, 'G', l_fee_effective_date + pn_grace_days),
				   NO_TRIGGER_EVENT_FLAG = pc_no_trigger_event_flag
             WHERE SERVICE_FEE_ID = l_sf_id;           
        ELSE
            l_sf_id := NULL;
            UPDATE SERVICE_FEES SET END_DATE = l_fee_effective_date
             WHERE TERMINAL_ID = l_terminal_id
               AND FEE_ID = l_fee_id
               AND FREQUENCY_ID = l_freq_id
               AND NVL(END_DATE, l_fee_effective_date) >= l_fee_effective_date;
            IF NVL(l_fee_amt, 0) <> 0 OR NVL(l_fee_perc, 0) <> 0 THEN
                SELECT SERVICE_FEE_SEQ.NEXTVAL INTO l_sf_id FROM DUAL;
                INSERT INTO SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID,
                    FREQUENCY_ID, FEE_AMOUNT, FEE_PERCENT, START_DATE,
                    END_DATE, LAST_PAYMENT, GRACE_PERIOD_DATE, NO_TRIGGER_EVENT_FLAG)
                    SELECT l_sf_id, l_terminal_id, l_fee_id, l_freq_id, l_fee_amt,
                           l_fee_perc, l_fee_effective_date, l_end_date,
                           l_last_payment, DECODE(lc_initiation_type_cd, 'G', l_fee_effective_date + pn_grace_days),
						   pc_no_trigger_event_flag
                      FROM FREQUENCY F
                     WHERE F.FREQUENCY_ID = l_freq_id;
            END IF;	
        END IF;
		IF l_sf_id IS NOT NULL THEN
            IF lc_initiation_type_cd = 'P' THEN
                --add any new net revenue fees
                INSERT INTO LEDGER(
                    LEDGER_ID,
                    RELATED_LEDGER_ID,
                    ENTRY_TYPE,
                    TRANS_ID,
                    PROCESS_FEE_ID,
                    SERVICE_FEE_ID,
                    AMOUNT,
                    ENTRY_DATE,
                    BATCH_ID,
                    SETTLE_STATE_ID,
                    LEDGER_DATE,
                    DESCRIPTION)
                SELECT
                    LEDGER_SEQ.NEXTVAL,
                    L.LEDGER_ID,
                    'SF',
                    L.TRANS_ID,
                    L.PROCESS_FEE_ID,
                    l_sf_id,
                    -L.AMOUNT * l_fee_perc,
                    L.ENTRY_DATE,
                    L.BATCH_ID,
                    L.SETTLE_STATE_ID,
                    L.LEDGER_DATE,
                    F.FEE_NAME
                FROM LEDGER L, BATCH B, FEES F, DOC D
               WHERE F.FEE_ID = l_fee_id
                 AND L.BATCH_ID = B.BATCH_ID
                 AND B.TERMINAL_ID = l_terminal_id
                 AND L.ENTRY_DATE >= l_fee_effective_date
                 AND L.RELATED_LEDGER_ID IS NULL -- Avoid Percent of Percent Fee
                 AND L.DELETED = 'N'
                 AND B.DOC_ID = D.DOC_ID
                 AND D.STATUS IN('O')
                 AND L.ENTRY_TYPE IN('CC', 'PF', 'SF', 'AD', 'CB', 'RF');
            ELSIF l_fee_override = 'Y' AND l_last_payment IS NOT NULL THEN -- let's insert any missing (No need to do this for net rev fees)
                DECLARE
                    CURSOR l_cur IS
                        SELECT LEDGER_SEQ.NEXTVAL LEDGER_ID,
                               A.FEE_DATE,
                               CBT.CUSTOMER_BANK_ID,
                               T.FEE_CURRENCY_ID,
                               F.FEE_NAME || ' for ' || TO_CHAR(A.FEE_DATE, A.FMT) FEE_DESC,
                               'Y' AS_ACCUM
                    FROM (SELECT DECODE(Q.DAYS, 0, LAST_DAY(D.COLUMN_VALUE), D.COLUMN_VALUE + Q.DAYS - 1) FEE_DATE,
                                 GET_SERVICE_FEE_DATE_FMT(Q.MONTHS, Q.DAYS) FMT
                            FROM TABLE(CAST(CORP.GLOBALS_PKG.GET_DATE_LIST(GREATEST(l_fee_effective_date, l_first_payment),
                                 LEAST(NVL(l_last_payment, (SELECT  
                                   CASE WHEN Q.MONTHS = 0 THEN l_current_date - Q.DAYS
                                        ELSE TRUNC(LAST_DAY(ADD_MONTHS(l_current_date, -Q.MONTHS)), 'DD')
                                   END FROM CORP.FREQUENCY Q WHERE Q.FREQUENCY_ID = l_freq_id)), NVL(l_end_date, MAX_DATE)),
                                 l_freq_id, 0, 0) AS REPORT.DATE_LIST)) D,
                                 FREQUENCY Q
                            WHERE Q.FREQUENCY_ID = l_freq_id
                              AND (Q.MONTHS > 0 OR Q.DAYS > 0)) A,
                          CUSTOMER_BANK_TERMINAL CBT,
                          FEES F,
                          REPORT.TERMINAL T
                    WHERE NOT EXISTS(SELECT 1
                        FROM CORP.LEDGER L, CORP.BATCH B
                        WHERE L.BATCH_ID = B.BATCH_ID
                          AND B.TERMINAL_ID = l_terminal_id
                          AND L.ENTRY_TYPE = 'SF'
                          AND L.DELETED = 'N'
                          AND L.AMOUNT <> 0
                          AND L.ENTRY_DATE = A.FEE_DATE)
                      AND WITHIN1(A.FEE_DATE, CBT.START_DATE, CBT.END_DATE) = 1
                      AND CBT.TERMINAL_ID = l_terminal_id
                      AND F.FEE_ID = l_fee_id
                      AND T.TERMINAL_ID = l_terminal_id;
                BEGIN
                    FOR l_rec IN l_cur LOOP
                        l_batch_id := GET_OR_CREATE_BATCH(
                                    l_terminal_id,
                                    l_rec.CUSTOMER_BANK_ID,
                                    l_rec.FEE_DATE,
                                    l_rec.FEE_CURRENCY_ID,
                                    l_rec.AS_ACCUM);

                        INSERT INTO LEDGER(
                                LEDGER_ID,
                                ENTRY_TYPE,
                                SERVICE_FEE_ID,
                                AMOUNT,
                                ENTRY_DATE,
                                BATCH_ID,
                                SETTLE_STATE_ID,
                                LEDGER_DATE,
                                DESCRIPTION)
                            VALUES(
                                l_rec.LEDGER_ID,
                                'SF',
                                l_sf_id,
                                -ABS(l_fee_amt),
                                l_rec.FEE_DATE,
                                l_batch_id,
                                2,
                                l_rec.FEE_DATE,
                                l_rec.FEE_DESC);
                        -- create net revenue fee on service fee, if any
                        INSERT INTO LEDGER(
                            LEDGER_ID,
                            RELATED_LEDGER_ID,
                            ENTRY_TYPE,
                            SERVICE_FEE_ID,
                            AMOUNT,
                            ENTRY_DATE,
                            BATCH_ID,
                            SETTLE_STATE_ID,
                            LEDGER_DATE,
                            DESCRIPTION)
                        SELECT
                            LEDGER_SEQ.NEXTVAL,
                            l_rec.LEDGER_ID,
                            'SF',
                            SF.SERVICE_FEE_ID,
                            ABS(l_fee_amt) * SF.FEE_PERCENT,
                            l_rec.FEE_DATE,
                            l_batch_id,
                            2,
                            l_rec.FEE_DATE,
                            F.FEE_NAME
                        FROM SERVICE_FEES SF, FEES F
                       WHERE SF.FEE_ID = F.FEE_ID
                         AND SF.TERMINAL_ID = l_terminal_id
                         AND l_rec.FEE_DATE BETWEEN NVL(SF.START_DATE, MIN_DATE)
                              AND NVL(SF.END_DATE, MAX_DATE)
                         AND SF.FREQUENCY_ID = 6;
                        UPDATE CORP.SERVICE_FEES
                         SET LAST_PAYMENT = CASE WHEN LAST_PAYMENT IS NULL OR l_rec.FEE_DATE > LAST_PAYMENT THEN l_rec.FEE_DATE ELSE LAST_PAYMENT END,
                             FIRST_PAYMENT = CASE WHEN FIRST_PAYMENT IS NULL OR l_rec.FEE_DATE < FIRST_PAYMENT THEN l_rec.FEE_DATE ELSE FIRST_PAYMENT END
                       WHERE SERVICE_FEE_ID =  l_sf_id;
                    END LOOP;
                END;
            END IF;
        END IF;
    END;
    
    PROCEDURE UNLOCK_DOC (
      l_doc_id IN DOC.DOC_ID%TYPE)
    IS
        l_customer_bank_id CORP.DOC.CUSTOMER_BANK_ID%TYPE;
        l_lock VARCHAR2(128);
    BEGIN
    	IF FREEZE_DOC_STATUS_DOC_ID(l_doc_id) <> 'L' THEN
           RAISE_APPLICATION_ERROR(-20400, 'This document has already been approved; you can not unlock it.');
   	    END IF;
    	--Remove rounding entries
    	UPDATE LEDGER
           SET DELETED = 'Y'
         WHERE ENTRY_TYPE = 'SB'
           AND TRANS_ID IS NULL
           AND BATCH_ID IN(SELECT BATCH_ID FROM BATCH WHERE DOC_ID = l_doc_id);
           
        -- Blank out the end date of any as accum batches
        UPDATE BATCH B
           SET B.BATCH_STATE_CD = 'L',
               B.BATCH_CLOSED_TS = NULL,
               B.END_DATE = NULL
         WHERE B.DOC_ID = l_doc_id
           AND B.BATCH_STATE_CD = 'F'
           AND B.PAYMENT_SCHEDULE_ID IN(1,5);

        -- Change time-period batches to Open
        UPDATE BATCH B
           SET B.BATCH_STATE_CD = 'O',
               B.BATCH_CLOSED_TS = NULL
         WHERE B.DOC_ID = l_doc_id
           AND B.BATCH_STATE_CD IN('U', 'D')
           AND B.PAYMENT_SCHEDULE_ID NOT IN(1,2,5,7,8);

        -- first condense any docs that may match once they are re-opened
    	DECLARE
    	   l_new_doc_id DOC.DOC_ID%TYPE;
        BEGIN
    	    SELECT MAX(DNEW.DOC_ID)
              INTO l_new_doc_id
              FROM DOC DOLD, DOC DNEW
             WHERE DOLD.DOC_ID = l_doc_id
               AND DNEW.DOC_ID <> l_doc_id
               AND DOLD.CUSTOMER_BANK_ID = DNEW.CUSTOMER_BANK_ID
               AND NVL(DOLD.CURRENCY_ID, 0) = NVL(DNEW.CURRENCY_ID, 0)
               AND DNEW.STATUS = 'O';
            IF l_new_doc_id IS NOT NULL THEN
            	-- Update batches and delete old doc
            	UPDATE BATCH SET DOC_ID = l_new_doc_id WHERE DOC_ID = l_doc_id;
            	DELETE FROM DOC WHERE DOC_ID = l_doc_id;
            	
            	--now condense any batches that may match once they are moved
            	DECLARE
            	   CURSOR l_cur IS
                    	SELECT BNEW.BATCH_ID NEW_BATCH_ID,
                               BOLD.BATCH_ID OLD_BATCH_ID,
                               BOLD.END_DATE OLD_END_DATE,
                               BOLD.START_DATE OLD_START_DATE
                          FROM BATCH BOLD, BATCH BNEW
                         WHERE BOLD.DOC_ID = l_new_doc_id
                           AND BNEW.DOC_ID = l_new_doc_id
                           AND BOLD.PAYMENT_SCHEDULE_ID = BNEW.PAYMENT_SCHEDULE_ID
                           AND NVL(BOLD.TERMINAL_ID, 0) = NVL(BNEW.TERMINAL_ID, 0)
                           AND ((BOLD.BATCH_STATE_CD != 'C' AND BNEW.BATCH_STATE_CD = 'C')
                            OR ((BOLD.BATCH_STATE_CD != 'C' OR BNEW.BATCH_STATE_CD = 'C') AND BOLD.BATCH_ID > BNEW.BATCH_ID))
                           AND (BOLD.PAYMENT_SCHEDULE_ID IN(1,5)
                             OR BOLD.START_DATE = BNEW.START_DATE);
            	BEGIN
            	   FOR l_rec IN l_cur LOOP
            	       UPDATE LEDGER
            	          SET BATCH_ID = l_rec.NEW_BATCH_ID
                        WHERE BATCH_ID = l_rec.OLD_BATCH_ID;
                    LOOP
                      UPDATE CORP.BATCH_TOTAL
                      SET (LEDGER_AMOUNT, LEDGER_COUNT)= (select sum(LEDGER_AMOUNT) LEDGER_AMOUNT, sum(LEDGER_COUNT) LEDGER_COUNT
                      from (
                      select LEDGER_AMOUNT, LEDGER_COUNT
                      from corp.batch_total where batch_id= l_rec.OLD_BATCH_ID and ENTRY_TYPE='CA'
                      union
                      select LEDGER_AMOUNT, LEDGER_COUNT
                      from corp.batch_total where batch_id= l_rec.NEW_BATCH_ID and ENTRY_TYPE='CA'))
                      where batch_id=l_rec.NEW_BATCH_ID
                      and entry_type ='CA';
                      EXIT WHEN SQL%ROWCOUNT > 0;
                      BEGIN
                          INSERT INTO CORP.BATCH_TOTAL(BATCH_ID, ENTRY_TYPE, PAYABLE, LEDGER_AMOUNT, LEDGER_COUNT)                        
                          select l_rec.NEW_BATCH_ID,'CA','N', sum(LEDGER_AMOUNT) LEDGER_AMOUNT, sum(LEDGER_COUNT) LEDGER_COUNT
	                      from (
	                      select LEDGER_AMOUNT, LEDGER_COUNT
	                      from corp.batch_total where batch_id= l_rec.OLD_BATCH_ID and ENTRY_TYPE='CA'
	                      union
	                      select LEDGER_AMOUNT, LEDGER_COUNT
	                      from corp.batch_total where batch_id= l_rec.NEW_BATCH_ID and ENTRY_TYPE='CA')
                          EXIT;    
                      EXCEPTION
                          WHEN DUP_VAL_ON_INDEX THEN
                              NULL;
                      END;
                    END LOOP;  
                    LOOP
                     UPDATE CORP.BATCH_TOTAL
                      SET (LEDGER_AMOUNT, LEDGER_COUNT)= (select sum(LEDGER_AMOUNT) LEDGER_AMOUNT, sum(LEDGER_COUNT) LEDGER_COUNT
                      from (
                      select LEDGER_AMOUNT, LEDGER_COUNT
                      from corp.batch_total where batch_id= l_rec.OLD_BATCH_ID and ENTRY_TYPE='TT'
                      union
                      select LEDGER_AMOUNT, LEDGER_COUNT
                      from corp.batch_total where batch_id= l_rec.NEW_BATCH_ID and ENTRY_TYPE='TT'))
                      where batch_id=l_rec.NEW_BATCH_ID
                      and entry_type ='TT';
                      EXIT WHEN SQL%ROWCOUNT > 0;
                      BEGIN
                          INSERT INTO CORP.BATCH_TOTAL(BATCH_ID, ENTRY_TYPE, PAYABLE, LEDGER_AMOUNT, LEDGER_COUNT)                        
                          select l_rec.NEW_BATCH_ID,'TT','N', sum(LEDGER_AMOUNT) LEDGER_AMOUNT, sum(LEDGER_COUNT) LEDGER_COUNT
	                      from (
	                      select LEDGER_AMOUNT, LEDGER_COUNT
	                      from corp.batch_total where batch_id= l_rec.OLD_BATCH_ID and ENTRY_TYPE='TT'
	                      union
	                      select LEDGER_AMOUNT, LEDGER_COUNT
	                      from corp.batch_total where batch_id= l_rec.NEW_BATCH_ID and ENTRY_TYPE='TT')
                          EXIT;    
                      EXCEPTION
                          WHEN DUP_VAL_ON_INDEX THEN
                              NULL;
                      END;
                    END LOOP;  
                    UPDATE REPORT.ACTIVITY_REF SET PAYMENT_BATCH_ID=l_rec.NEW_BATCH_ID
                    WHERE PAYMENT_BATCH_ID=l_rec.OLD_BATCH_ID;
                       DELETE FROM BATCH_FILL WHERE BATCH_ID = l_rec.OLD_BATCH_ID;
                       DELETE FROM BATCH WHERE BATCH_ID = l_rec.OLD_BATCH_ID;
                       DELETE FROM BATCH_TOTAL WHERE BATCH_ID = l_rec.OLD_BATCH_ID;
                       
                       UPDATE BATCH
                          SET START_DATE = LEAST(START_DATE, l_rec.OLD_START_DATE),
                              END_DATE = GREATEST(END_DATE, l_rec.OLD_END_DATE)
                        WHERE BATCH_ID = l_rec.NEW_BATCH_ID;
            	   END LOOP;
         	   END;
            ELSE
                -- Update doc
            	UPDATE DOC SET STATUS = 'O' WHERE DOC_ID = l_doc_id;
            END IF;
    	END;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
    	   RAISE_APPLICATION_ERROR(-20401, 'This document does not exist.');
        WHEN OTHERS THEN
    	   RAISE;
    END;
    
    FUNCTION GET_TRANS_ADJ_DESC(
        l_trans_id LEDGER.TRANS_ID%TYPE,
        l_entry_type LEDGER.ENTRY_TYPE%TYPE,
        l_entry_date LEDGER.ENTRY_DATE%TYPE,
        l_terminal_id BATCH.TERMINAL_ID%TYPE,
        l_customer_bank_id DOC.CUSTOMER_BANK_ID%TYPE)
     RETURN LEDGER.DESCRIPTION%TYPE
    IS
        l_old_payable VARCHAR(4000); -- oracle raises an exception if you use anything with smaller length
        l_old_terminal_id BATCH.TERMINAL_ID%TYPE;
        l_old_customer_bank_id DOC.CUSTOMER_BANK_ID%TYPE;
        l_desc LEDGER.DESCRIPTION%TYPE;
    BEGIN
        -- figure out why this was added
        --possible reasons:
        --  1. previously denied
        --  2. changed location
        --  3. changed customer bank
        
        SELECT *
          INTO l_old_payable, l_old_terminal_id, l_old_customer_bank_id
          FROM (
            SELECT PAYMENTS_PKG.ENTRY_PAYABLE(L.SETTLE_STATE_ID, L.ENTRY_TYPE),
                   B.TERMINAL_ID, D.CUSTOMER_BANK_ID
              FROM LEDGER L, BATCH B, DOC D
             WHERE L.BATCH_ID = B.BATCH_ID
               AND L.DELETED = 'N'
               AND B.DOC_ID = D.DOC_ID
               AND D.STATUS NOT IN('O', 'D')
               AND L.TRANS_ID = l_trans_id
               AND L.ENTRY_TYPE = 'CC'
               AND B.PAYMENT_SCHEDULE_ID <> 5
             ORDER BY L.CREATE_DATE DESC) A
         WHERE ROWNUM = 1;
        IF l_old_payable = 'N' THEN
            IF l_entry_type = 'CC' THEN
                RETURN 'Payment of previously declined transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSIF l_entry_type = 'PF' THEN
                RETURN 'Process Fee for previously declined transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSE -- this should never occur
                RETURN 'Payment related to previously declined transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            END IF;
        ELSIF l_terminal_id <> l_old_terminal_id THEN
            IF l_entry_type = 'CC' THEN
                RETURN 'Payment of transaction previously assigned to a different location (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSIF l_entry_type = 'PF' THEN
                RETURN 'Process Fee for transaction previously assigned to a different location (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSE -- this should never occur
                RETURN 'Payment related to transaction previously assigned to a different location (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            END IF;
        ELSIF l_customer_bank_id <> l_old_customer_bank_id THEN
            IF l_entry_type = 'CC' THEN
                RETURN 'Payment of transaction previously assigned to a different bank account (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSIF l_entry_type = 'PF' THEN
                RETURN 'Process Fee for transaction previously assigned to a different bank account (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSE -- this should never occur
                RETURN 'Payment related to transaction previously assigned to a different bank account (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            END IF;
        ELSE
            IF l_entry_type = 'CC' THEN
                RETURN 'Payment of previously unpaid transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSIF l_entry_type = 'PF' THEN
                RETURN 'Process Fee for previously unpaid transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSE -- this should never occur
                RETURN 'Payment related to previously unpaid transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            END IF;
        END IF;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            IF l_entry_type = 'CC' THEN
                RETURN 'Payment of previously unpaid transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSIF l_entry_type = 'PF' THEN
                RETURN 'Process Fee for previously unpaid transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSE -- this should never occur
                RETURN 'Payment related to previously unpaid transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            END IF;
    END;
    
    PROCEDURE UPDATE_LEDGER(
              l_tran_id LEDGER.TRANS_ID%TYPE)
    IS
    	CURSOR l_cur IS
    		SELECT TRANS_TYPE_ID, CLOSE_DATE, SETTLE_DATE, TOTAL_AMOUNT, SETTLE_STATE_ID,
                   TERMINAL_ID, CUSTOMER_BANK_ID, PROCESS_FEE_ID, CURRENCY_ID
    		  FROM REPORT.TRANS
    		 WHERE TRAN_ID = l_tran_id;
    BEGIN
         FOR l_rec IN l_cur LOOP
    	     UPDATE_LEDGER(l_tran_id,l_rec.trans_type_id,l_rec.close_date,l_rec.settle_date,
                           l_rec.total_amount,l_rec.settle_state_id,l_rec.terminal_id,
                           l_rec.customer_bank_id,l_rec.process_fee_id,l_rec.currency_id);
   	     END LOOP;
    END;
    
    PROCEDURE ADD_ACTIVATION_FEE(
        l_terminal_id CORP.BATCH.TERMINAL_ID%TYPE,
        l_fee_amt CORP.LEDGER.AMOUNT%TYPE,
        l_activation_date  CORP.LEDGER.ENTRY_DATE%TYPE)
    IS
        l_batch_id CORP.BATCH.BATCH_ID%TYPE;
        l_service_fee_id CORP.LEDGER.SERVICE_FEE_ID%TYPE;
        l_desc CORP.LEDGER.DESCRIPTION%TYPE;
        l_ledger_id CORP.LEDGER.LEDGER_ID%TYPE;
        l_cust_bank_id CORP.DOC.CUSTOMER_BANK_ID%TYPE;
        l_fee_currency_id CORP.DOC.CURRENCY_ID%TYPE;
		l_lock VARCHAR2(128);
    BEGIN
        SELECT cbt.CUSTOMER_BANK_ID, t.FEE_CURRENCY_ID,
               'Activation Fee for Terminal ' || t.TERMINAL_NBR
          INTO l_cust_bank_id, l_fee_currency_id, l_desc
          FROM REPORT.TERMINAL t, CORP.CUSTOMER_BANK_TERMINAL cbt
         WHERE t.TERMINAL_ID = l_terminal_id
           AND t.TERMINAL_ID = cbt.TERMINAL_ID
           AND l_activation_date >= NVL(cbt.START_DATE, MIN_DATE)
           AND l_activation_date < NVL(cbt.END_DATE, MAX_DATE);
		   
		l_lock := GLOBALS_PKG.REQUEST_LOCK('TERMINAL.TERMINAL_ID', l_terminal_id);
		   
        l_batch_id := GET_OR_CREATE_BATCH(l_terminal_id, l_cust_bank_id, l_activation_date, l_fee_currency_id, 'N');
        SELECT SERVICE_FEE_SEQ.NEXTVAL
          INTO l_service_fee_id
          FROM DUAL;
        INSERT INTO SERVICE_FEES(
            SERVICE_FEE_ID,
            TERMINAL_ID,
            FEE_ID,
            FEE_AMOUNT,
            FREQUENCY_ID,
            LAST_PAYMENT,
            START_DATE)
       	  VALUES(
       	    l_service_fee_id,
       	    l_terminal_id,
       	    10,
       	    l_fee_amt,
       	    7,
       	    l_activation_date,
       	    l_activation_date);
       	    
        SELECT LEDGER_SEQ.NEXTVAL
          INTO l_ledger_id
          FROM DUAL;
        INSERT INTO LEDGER(
            LEDGER_ID,
            ENTRY_TYPE,
            SERVICE_FEE_ID,
            AMOUNT,
            ENTRY_DATE,
            BATCH_ID,
            SETTLE_STATE_ID,
            LEDGER_DATE,
            DESCRIPTION)
        VALUES (
            l_ledger_id,
            'SF',
            l_service_fee_id,
            -l_fee_amt,
            l_activation_date,
            l_batch_id,
            2,
            l_activation_date,
            l_desc);
    END;
    
    PROCEDURE SWITCH_PAYMENT_SCHEDULE(
        l_tran_id CORP.LEDGER.TRANS_ID%TYPE,
        l_payment_schedule_id  CORP.BATCH.PAYMENT_SCHEDULE_ID%TYPE)
    IS
        l_lock VARCHAR(128);
        l_adj_amt CORP.LEDGER.AMOUNT%TYPE;
        l_batch_id CORP.LEDGER.BATCH_ID%TYPE;
       	CURSOR l_cur IS
    		SELECT X.TRANS_TYPE_ID, X.CLOSE_DATE, X.SETTLE_DATE, X.TOTAL_AMOUNT, X.SETTLE_STATE_ID,
                   X.TERMINAL_ID, X.CUSTOMER_BANK_ID, X.PROCESS_FEE_ID, X.CURRENCY_ID, T.BUSINESS_UNIT_ID
    		  FROM REPORT.TRANS X
  		     INNER JOIN REPORT.TERMINAL T ON X.TERMINAL_ID = T.TERMINAL_ID
    		 WHERE TRAN_ID = l_tran_id;
    BEGIN
        FOR l_rec IN l_cur LOOP
            IF NVL(l_rec.terminal_id, 0) <> 0 AND NVL(l_rec.customer_bank_id, 0) <> 0 THEN
                l_lock := GLOBALS_PKG.REQUEST_LOCK('LEDGER.TRANS_ID',l_tran_id);
                -- Remove any open entries in different pay sched
                UPDATE CORP.LEDGER L SET DELETED = 'Y'
                 WHERE L.TRANS_ID = l_tran_id
                   AND L.DELETED = 'N'
                   AND EXISTS(
                      SELECT 1
                        FROM CORP.BATCH B
                       INNER JOIN CORP.DOC D ON B.DOC_ID = D.DOC_ID
                       WHERE B.PAYMENT_SCHEDULE_ID <> l_payment_schedule_id
                         AND B.BATCH_STATE_CD IN('O', 'L')
                         AND D.STATUS IN('O')
                         AND L.BATCH_ID = B.BATCH_ID);

                -- Make adjustment for old
                SELECT SUM(L.AMOUNT)
                  INTO l_adj_amt
                  FROM CORP.LEDGER L
                 INNER JOIN CORP.BATCH B ON L.BATCH_ID = B.BATCH_ID
                 INNER JOIN CORP.DOC D ON B.DOC_ID = D.DOC_ID
                 WHERE L.TRANS_ID = l_tran_id
                   AND L.DELETED = 'N'
                   AND D.STATUS NOT IN('D')
                   AND B.PAYMENT_SCHEDULE_ID <> l_payment_schedule_id
                   AND ENTRY_PAYABLE(l.settle_state_id,l.entry_type) = 'Y';

                IF NVL(l_adj_amt, 0) <> 0 THEN
                    l_batch_id := GET_OR_CREATE_BATCH(l_rec.terminal_id,
                                    l_rec.customer_bank_id, l_rec.close_date, l_rec.currency_id, 'A');
                    INSERT INTO LEDGER(
                        LEDGER_ID,
                        ENTRY_TYPE,
                        TRANS_ID,
                        PROCESS_FEE_ID,
                        AMOUNT,
                        ENTRY_DATE,
                        BATCH_ID,
                        SETTLE_STATE_ID,
                        LEDGER_DATE,
                        DESCRIPTION)
                    SELECT
                        LEDGER_SEQ.NEXTVAL,
                        'AD',
                        l_tran_id,
                        NULL,
                        -l_adj_amt,
                        l_rec.close_date,
                        l_batch_id,
                        l_rec.SETTLE_STATE_ID,
                        NVL(l_rec.SETTLE_DATE, SYSDATE),
                        'Payment schedule switched to ' || DESCRIPTION
                      FROM CORP.PAYMENT_SCHEDULE
                     WHERE PAYMENT_SCHEDULE_ID = l_payment_schedule_id;
                END IF;
                -- insert as new
                l_batch_id := GET_OR_CREATE_BATCH(l_rec.terminal_id, l_rec.customer_bank_id, l_rec.close_date, l_rec.currency_id, l_payment_schedule_id, l_rec.BUSINESS_UNIT_ID, 'N');
                INSERT_TRANS_TO_LEDGER(l_tran_id, l_rec.trans_type_id, l_rec.close_date, l_rec.settle_date,
                     l_rec.total_amount, l_rec.settle_state_id, l_rec.process_fee_id, l_batch_id);
            END IF;
        END LOOP;
    END;
	
	PROCEDURE CHECK_EFT_PROCESS_COMPLETION
	IS
		LD_EFT_PROPAGATION_START_TS DATE := TO_DATE(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EFT_PROPAGATION_LAST_RUN_START_TS'), 'MM/DD/YYYY HH24:MI:SS');
		LD_EFT_PROPAGATION_COMPLETE_TS DATE := TO_DATE(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EFT_PROPAGATION_LAST_RUN_COMPLETE_TS'), 'MM/DD/YYYY HH24:MI:SS');
		LV_EMAIL_FROM ENGINE.APP_SETTING.APP_SETTING_VALUE%TYPE := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EMAIL_FROM_ADDRESS');
		LV_EMAIL_TO ENGINE.APP_SETTING.APP_SETTING_VALUE%TYPE := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EFT_PROCESSING_EMAIL_TO');
		LV_EMAIL_CONTENT ENGINE.OB_EMAIL_QUEUE.OB_EMAIL_CONTENT%TYPE := 'Automatic EFT Processing is complete. Number of processed EFT(s) by currency:' || CHR(13) || CHR(10);
		LC_SEND_EMAIL CHAR(1) := 'N';
		LN_COUNT INTEGER;
	
		CURSOR L_CUR IS
			SELECT C.CURRENCY_NAME, COUNT(1) EFT_COUNT
			FROM CORP.DOC D
			JOIN CORP.CURRENCY C ON D.CURRENCY_ID = C.CURRENCY_ID
			WHERE D.AUTO_PROCESS_START_TS >= LD_EFT_PROPAGATION_START_TS
				AND D.STATUS != 'D'
			GROUP BY C.CURRENCY_NAME
			ORDER BY COUNT(1) DESC;
	BEGIN
		IF LD_EFT_PROPAGATION_COMPLETE_TS < TO_DATE(TO_CHAR(CURRENT_TIMESTAMP, 'MM/DD/YYYY'), 'MM/DD/YYYY') THEN
			RETURN;
		END IF;
	
		SELECT COUNT(1)
		INTO LN_COUNT
		FROM CORP.DOC
		WHERE AUTO_PROCESS_START_TS >= LD_EFT_PROPAGATION_START_TS
			AND AUTO_PROCESS_END_TS IS NULL;
			
		IF LN_COUNT > 0 THEN
			RETURN;
		END IF;
	
		FOR L_REC IN L_CUR LOOP
			IF LC_SEND_EMAIL = 'N' THEN
				LC_SEND_EMAIL := 'Y';
			END IF;
		
			LV_EMAIL_CONTENT := LV_EMAIL_CONTENT || CHR(13) || CHR(10) || L_REC.CURRENCY_NAME || ': ' || L_REC.EFT_COUNT;
		END LOOP;
		
		IF LC_SEND_EMAIL = 'Y' THEN
			LV_EMAIL_CONTENT := LV_EMAIL_CONTENT || CHR(13) || CHR(10) || CHR(13) || CHR(10) 
				|| DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DMS_URL')
				|| 'processedEFT.i?eft_from_date=' || TO_CHAR(CURRENT_TIMESTAMP, 'MM/DD/YYYY') || '&' || 'eft_from_time=00:00:00&' || 'eft_to_date='
				|| TO_CHAR(CURRENT_TIMESTAMP, 'MM/DD/YYYY') || '&' || 'eft_to_time=' || TO_CHAR(CURRENT_TIMESTAMP, 'HH24:MI:SS')
				|| '&' || 'auto_processed=Y&' || 'data_format=HTML&' || 'action=List+EFTs';
		
			INSERT INTO ENGINE.OB_EMAIL_QUEUE(OB_EMAIL_FROM_EMAIL_ADDR, OB_EMAIL_FROM_NAME, OB_EMAIL_MSG, OB_EMAIL_SUBJECT, OB_EMAIL_TO_EMAIL_ADDR, OB_EMAIL_TO_NAME, OB_EMAIL_CONTENT)
			SELECT LV_EMAIL_FROM, LV_EMAIL_FROM, ' ', 'EFT Processing', LV_EMAIL_TO, LV_EMAIL_TO, LV_EMAIL_CONTENT
			FROM DUAL
			WHERE NOT EXISTS (
				SELECT 1 FROM ENGINE.OB_EMAIL_QUEUE
				WHERE CREATED_TS >= LD_EFT_PROPAGATION_START_TS
					AND OB_EMAIL_SUBJECT = 'EFT Processing'
					AND OB_EMAIL_CONTENT LIKE 'Automatic EFT Processing is complete.%'
			);
		END IF;
		
		UPDATE ENGINE.APP_SETTING
   		SET APP_SETTING_VALUE = TO_CHAR(CURRENT_TIMESTAMP, 'MM/DD/YYYY HH24:MI:SS')
   		WHERE APP_SETTING_CD = 'EFT_PROCESSING_LAST_RUN_COMPLETE_TS';
	END;
	
	PROCEDURE START_EFT_PROPAGATION
	IS
	BEGIN
		UPDATE ENGINE.APP_SETTING
   		SET APP_SETTING_VALUE = TO_CHAR(CURRENT_TIMESTAMP, 'MM/DD/YYYY HH24:MI:SS')
   		WHERE APP_SETTING_CD = 'EFT_PROPAGATION_LAST_RUN_START_TS';
	END;
	
	PROCEDURE START_EFT_PROCESSING(
		PN_DOC_ID CORP.DOC.DOC_ID%TYPE)
	IS
	BEGIN
		UPDATE CORP.DOC
		SET AUTO_PROCESS_START_TS = CURRENT_TIMESTAMP
		WHERE DOC_ID = PN_DOC_ID;
	END;
	
	PROCEDURE PROCESS_EFT(
		PN_DOC_ID CORP.DOC.DOC_ID%TYPE)
	IS
	BEGIN
		IF GET_DOC_STATUS(PN_DOC_ID) NOT IN ('P', 'S') THEN
			CORP.PAYMENTS_PKG.LOCK_DOC(PN_DOC_ID, NULL);
			CORP.PAYMENTS_PKG.APPROVE_PAYMENT(PN_DOC_ID, NULL);			
			CORP.PAYMENTS_PKG.MARK_DOC_PAID(PN_DOC_ID, NULL);
		END IF;
		
		UPDATE CORP.DOC
		SET AUTO_PROCESS_END_TS = CURRENT_TIMESTAMP
		WHERE DOC_ID = PN_DOC_ID;
		
		CHECK_EFT_PROCESS_COMPLETION;
	END;
	
	PROCEDURE COMPLETE_EFT_PROPAGATION
	IS
	BEGIN
		UPDATE ENGINE.APP_SETTING
   		SET APP_SETTING_VALUE = TO_CHAR(CURRENT_TIMESTAMP, 'MM/DD/YYYY HH24:MI:SS')
   		WHERE APP_SETTING_CD = 'EFT_PROPAGATION_LAST_RUN_COMPLETE_TS';
		
		CHECK_EFT_PROCESS_COMPLETION;
	END;
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/ENGINE/TRBI_MACHINE_CMD_PENDING.trg?rev=1.10
CREATE OR REPLACE TRIGGER engine.trbi_machine_cmd_pending
BEFORE INSERT ON engine.machine_cmd_pending
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
DECLARE
	ln_device_id DEVICE.DEVICE_ID%TYPE;
	ln_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE;
	lv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE;
	lc_comm_method_cd DEVICE.COMM_METHOD_CD%TYPE;
	lv_comm_method_name COMM_METHOD.COMM_METHOD_NAME%TYPE;
	lv_firmware_version DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
	lv_diag_app_version DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
	ln_file_transfer_id FILE_TRANSFER.FILE_TRANSFER_ID%TYPE;
	ln_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE;
	ln_file_transfer_type_cd FILE_TRANSFER_TYPE.FILE_TRANSFER_TYPE_CD%TYPE;
	lv_file_transfer_type_name FILE_TRANSFER_TYPE.FILE_TRANSFER_TYPE_NAME%TYPE;
	lv_app_setting_value ENGINE.APP_SETTING.APP_SETTING_VALUE%TYPE;
	lv_new_firmware_version VARCHAR2(20);
BEGIN
	IF :NEW.DATA_TYPE IN('7C', 'A4', 'C7', 'C8') AND :NEW.DEVICE_FILE_TRANSFER_ID IS NULL THEN
		:NEW.DEVICE_FILE_TRANSFER_ID := DBADMIN.TO_NUMBER_OR_NULL(:NEW.COMMAND);
	END IF;

	IF :NEW.DATA_TYPE IN('A4', 'C7', 'C8') THEN
		SELECT MAX(D.DEVICE_ID), MAX(D.DEVICE_TYPE_ID), MAX(D.DEVICE_SERIAL_CD), MAX(D.COMM_METHOD_CD), MAX(CM.COMM_METHOD_NAME), MAX(D.FIRMWARE_VERSION), MAX(DD.DIAG_APP_VERSION)
		INTO ln_device_id, ln_device_type_id, lv_device_serial_cd, lc_comm_method_cd, lv_comm_method_name, lv_firmware_version, lv_diag_app_version
		FROM DEVICE.DEVICE_LAST_ACTIVE DLA
		JOIN DEVICE.DEVICE D ON DLA.DEVICE_ID = D.DEVICE_ID
		LEFT OUTER JOIN DEVICE.DEVICE_DATA DD ON DLA.DEVICE_NAME = DD.DEVICE_NAME
		LEFT OUTER JOIN DEVICE.COMM_METHOD CM ON D.COMM_METHOD_CD = CM.COMM_METHOD_CD
		WHERE DLA.DEVICE_NAME = :NEW.MACHINE_ID;
		
		IF ln_device_type_id IN (1, 13) THEN
			SELECT FT.FILE_TRANSFER_ID, FT.FILE_TRANSFER_NAME, FTT.FILE_TRANSFER_TYPE_CD, FTT.FILE_TRANSFER_TYPE_NAME
			INTO ln_file_transfer_id, ln_file_transfer_name, ln_file_transfer_type_cd, lv_file_transfer_type_name
			FROM DEVICE.DEVICE_FILE_TRANSFER DFT
			JOIN DEVICE.FILE_TRANSFER FT ON DFT.FILE_TRANSFER_ID = FT.FILE_TRANSFER_ID
			JOIN DEVICE.FILE_TRANSFER_TYPE FTT ON FT.FILE_TRANSFER_TYPE_CD = FTT.FILE_TRANSFER_TYPE_CD
			WHERE DFT.DEVICE_FILE_TRANSFER_ID = :NEW.DEVICE_FILE_TRANSFER_ID;
			
			IF ln_device_type_id = 1 THEN
				IF ln_file_transfer_type_cd NOT IN (5, 24, 25, 27, 28) THEN
					RAISE_APPLICATION_ERROR(-20208, lv_file_transfer_type_name || ' internal file download is not supported by Gx');
				END IF;
				
				lv_firmware_version := SUBSTR(lv_firmware_version, 9);
				lv_diag_app_version := REGEXP_REPLACE(lv_diag_app_version, 'Diag |Diagnostic = N/A', '');
				
				lv_app_setting_value := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('GX_MIN_FIRMWARE_VERSION_FOR_INTERNAL_FILE_TRANSFERS');
				IF lv_firmware_version IS NOT NULL AND DBADMIN.VERSION_COMPARE(lv_firmware_version, lv_app_setting_value) < 0 THEN
					RAISE_APPLICATION_ERROR(-20208, 'Invalid Gx firmware version: ' || lv_firmware_version || ', ' || lv_app_setting_value || ' or greater is required for internal file downloads');
				END IF;
		
				IF ln_file_transfer_type_cd = 5 THEN --Application Software Upgrade
					lv_app_setting_value := DBADMIN.PKG_GLOBAL.GET_APP_SETTING(CASE lc_comm_method_cd WHEN 'C' THEN 'GX_CDMA' ELSE 'GX' END || '_MIN_DIAGNOSTIC_VERSION_FOR_INTERNAL_FILE_TRANSFER_UPGRADES');
					IF lv_diag_app_version IS NOT NULL AND DBADMIN.VERSION_COMPARE(lv_diag_app_version, lv_app_setting_value) < 0 THEN
						RAISE_APPLICATION_ERROR(-20208, 'Invalid ' || lv_comm_method_name || ' Gx diagnostic app version: ' || lv_diag_app_version || ', ' || lv_app_setting_value || ' or greater is required for ' || lv_file_transfer_type_name);
					END IF;
					
					lv_app_setting_value := DBADMIN.PKG_GLOBAL.GET_APP_SETTING(CASE lc_comm_method_cd WHEN 'C' THEN 'GX_CDMA' ELSE 'GX' END || '_MIN_FIRMWARE_VERSION_FOR_INTERNAL_FILE_TRANSFER_UPGRADES');
					IF lv_firmware_version IS NOT NULL AND DBADMIN.VERSION_COMPARE(lv_firmware_version, lv_app_setting_value) < 0 THEN
						RAISE_APPLICATION_ERROR(-20208, 'Invalid ' || lv_comm_method_name || ' Gx firmware version: ' || lv_firmware_version || ', ' || lv_app_setting_value || ' or greater is required for ' || lv_file_transfer_type_name);
					END IF;
					
					SELECT COALESCE(UTL_RAW.CAST_TO_VARCHAR2(DBMS_LOB.SUBSTR(FILE_TRANSFER_CONTENT, 19, 82)), '-')
					INTO lv_new_firmware_version
					FROM DEVICE.FILE_TRANSFER
					WHERE FILE_TRANSFER_ID = ln_file_transfer_id;
					
					IF lv_new_firmware_version NOT LIKE 'USA-%' AND lv_new_firmware_version NOT LIKE 'Diag%' AND lv_new_firmware_version NOT LIKE 'PTest%' THEN
						RAISE_APPLICATION_ERROR(-20208, ln_file_transfer_name || ' is incorrect ' || lv_file_transfer_type_name || ' file download for Gx');
					END IF;
				ELSIF ln_file_transfer_type_cd IN (27, 28) THEN --Card Reader Application Firmware, Isis SmartTap Configuration
					lv_app_setting_value := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('GX_MIN_FIRMWARE_VERSION_FOR_CARD_READER_UPGRADES');
					IF lv_firmware_version IS NOT NULL AND DBADMIN.VERSION_COMPARE(lv_firmware_version, lv_app_setting_value) < 0 THEN
						RAISE_APPLICATION_ERROR(-20208, 'Invalid Gx firmware version: ' || lv_firmware_version || ', ' || lv_app_setting_value || ' or greater is required for ' || lv_file_transfer_type_name);
					END IF;
				END IF;
			ELSIF ln_device_type_id = 13 THEN
				IF lv_device_serial_cd LIKE 'VJ%' THEN
					IF ln_file_transfer_type_cd NOT IN (5, 19, 28) THEN
						RAISE_APPLICATION_ERROR(-20208, lv_file_transfer_type_name || ' file download is not supported by G9');
					END IF;
				ELSIF lv_device_serial_cd LIKE 'EE%' THEN
					IF ln_file_transfer_type_cd NOT IN (5, 19) THEN
						RAISE_APPLICATION_ERROR(-20208, lv_file_transfer_type_name || ' file download is not supported by Edge');
					END IF;
				END IF;
				
				IF ln_file_transfer_type_cd = 5 THEN --Application Software Upgrade
					SELECT COALESCE(UTL_RAW.CAST_TO_VARCHAR2(DBMS_LOB.SUBSTR(FILE_TRANSFER_CONTENT, 10, 93)), '-')
					INTO lv_new_firmware_version
					FROM DEVICE.FILE_TRANSFER
					WHERE FILE_TRANSFER_ID = ln_file_transfer_id;
					
					IF lv_device_serial_cd LIKE 'VJ%' THEN
						IF lv_new_firmware_version NOT LIKE '2.02.%' THEN
							RAISE_APPLICATION_ERROR(-20208, ln_file_transfer_name || ' is incorrect ' || lv_file_transfer_type_name || ' file download for ' || lv_comm_method_name || ' G9');
						END IF;
						IF lc_comm_method_cd = 'G' THEN
							IF DBADMIN.VERSION_COMPARE(lv_new_firmware_version, '2.02.007') < 0 THEN
								RAISE_APPLICATION_ERROR(-20208, 'Firmware version 2.02.007 or greater is required for ' || lv_comm_method_name || ' G9');
							END IF;
						END IF;
					ELSIF lv_device_serial_cd LIKE 'EE%' THEN
						IF lc_comm_method_cd = 'C' THEN
							IF lv_new_firmware_version NOT LIKE '1.02.%' THEN
								RAISE_APPLICATION_ERROR(-20208, ln_file_transfer_name || ' is incorrect ' || lv_file_transfer_type_name || ' file download for ' || lv_comm_method_name || ' Edge');
							END IF;
						ELSIF lc_comm_method_cd = 'G' THEN
							IF lv_new_firmware_version NOT LIKE '1.00.%' AND lv_new_firmware_version NOT LIKE '1.01.%' THEN
								RAISE_APPLICATION_ERROR(-20208, ln_file_transfer_name || ' is incorrect ' || lv_file_transfer_type_name || ' file download for ' || lv_comm_method_name || ' Edge');
							END IF;
						END IF;
					END IF;
				END IF;
			END IF;
		END IF;
	END IF;

   if (:NEW.machine_command_pending_id is null) then
      SELECT SEQ_MACHINE_CMD_PENDING_ID.NEXTVAL
      INTO :NEW.machine_command_pending_id
      FROM dual ;
   end if;
	SELECT	sysdate,
			user,
			sysdate,
			user
	into	:new.created_ts,
			:new.created_by,
			:new.last_updated_ts,
			:new.last_updated_by
	FROM	dual;
END;
/
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/PKG_APP_USER.pbk?rev=1.18
CREATE OR REPLACE PACKAGE BODY REPORT.PKG_APP_USER AS
    PROCEDURE POPULATE_USER(
        pn_profile_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pc_internal_flag CHAR,
        pv_profile_user_name OUT REPORT.USER_LOGIN.USER_NAME%TYPE,
        pn_profile_user_type OUT REPORT.USER_LOGIN.USER_TYPE%TYPE,
        pv_profile_first_name OUT REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_profile_last_name OUT REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_profile_email OUT REPORT.USER_LOGIN.EMAIL%TYPE,
        pv_profile_telephone OUT REPORT.USER_LOGIN.TELEPHONE%TYPE,
        pv_profile_fax OUT REPORT.USER_LOGIN.FAX%TYPE,
        pn_profile_customer_id OUT REPORT.USER_LOGIN.CUSTOMER_ID%TYPE,
        pv_profile_customer_name OUT CORP.CUSTOMER.CUSTOMER_NAME%TYPE,
        pt_profile_user_privileges OUT NUMBER_TABLE,
        pn_profile_active_bank_accts OUT PLS_INTEGER,
        pn_profile_pending_bank_accts OUT PLS_INTEGER,
        pn_customer_active_bank_accts OUT PLS_INTEGER,
        pn_profile_terminal_count OUT PLS_INTEGER,
        pv_profile_license_nbr OUT CORP.LICENSE_NBR.LICENSE_NBR%TYPE)
    AS
        lc_internal_flag CHAR(1);
    BEGIN
        SELECT U.USER_NAME, U.USER_TYPE, U.FIRST_NAME, U.LAST_NAME, U.EMAIL, U.TELEPHONE, U.FAX, NVL(C.CUSTOMER_ID, 0), C.CUSTOMER_NAME
          INTO pv_profile_user_name, pn_profile_user_type, pv_profile_first_name, pv_profile_last_name, pv_profile_email, pv_profile_telephone, pv_profile_fax, pn_profile_customer_id, pv_profile_customer_name 
          FROM REPORT.USER_LOGIN U
          LEFT OUTER JOIN CORP.CUSTOMER C ON U.CUSTOMER_ID = C.CUSTOMER_ID AND C.STATUS != 'D'
         WHERE U.USER_ID = pn_profile_user_id
           AND U.STATUS = 'A';
        
        -- get counts and privs
        IF pc_internal_flag != '?' THEN
            lc_internal_flag := pc_internal_flag;
        ELSIF pv_profile_user_name LIKE '%@usatech.com' AND pn_profile_user_type != 8 THEN
            lc_internal_flag := 'Y';
        ELSE
            lc_internal_flag := 'N';
        END IF;
        
        SELECT UP.PRIV_ID
          BULK COLLECT INTO pt_profile_user_privileges
          FROM REPORT.USER_PRIVS UP 
          JOIN REPORT.PRIV P on UP.PRIV_ID=P.PRIV_ID
         WHERE UP.USER_ID = pn_profile_user_id
           AND (P.INTERNAL_EXTERNAL_FLAG = 'B' OR (lc_internal_flag = 'Y' AND P.INTERNAL_EXTERNAL_FLAG = 'I') OR (lc_internal_flag = 'N' AND P.INTERNAL_EXTERNAL_FLAG IN('E', 'P')));
                   
        SELECT COUNT(*)
          INTO pn_profile_terminal_count
          FROM REPORT.VW_USER_TERMINAL UT
         WHERE UT.USER_ID = pn_profile_user_id;
         
        SELECT COUNT(DISTINCT DECODE(UCB.STATUS, 'A', UCB.CUSTOMER_BANK_ID)) ACTIVE_BANK_ACCT_COUNT, COUNT(DISTINCT DECODE(UCB.STATUS, 'P', UCB.CUSTOMER_BANK_ID)) PENDING_BANK_ACCT_COUNT
          INTO pn_profile_active_bank_accts, pn_profile_pending_bank_accts
          FROM REPORT.VW_USER_CUSTOMER_BANK UCB
	     WHERE UCB.USER_ID = pn_profile_user_id;
         
        IF pn_profile_user_type = 8 THEN
            SELECT MAX(LICENSE_NBR)
              INTO pv_profile_license_nbr
              FROM (SELECT CL.LICENSE_NBR
                      FROM CORP.CUSTOMER_LICENSE CL
                     WHERE CUSTOMER_ID = pn_profile_customer_id
                     ORDER BY CL.RECEIVED DESC)
             WHERE ROWNUM = 1;
            SELECT COUNT(*)
              INTO pn_customer_active_bank_accts
              FROM CORP.CUSTOMER_BANK
             WHERE CUSTOMER_ID = pn_profile_customer_id
               AND STATUS = 'A';
        ELSE
            pn_profile_pending_bank_accts := 0;
            pn_customer_active_bank_accts := 0;
        END IF; 
    END;

    PROCEDURE LOGIN_INTERNAL_USER(
        pn_login_user_id OUT REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_login_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pv_login_first_name REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_login_last_name REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_login_email REPORT.USER_LOGIN.EMAIL%TYPE,
        pv_login_time_zone_guid IN OUT REPORT.USER_LOGIN.TIME_ZONE_GUID%TYPE,
        pn_profile_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_profile_user_name OUT REPORT.USER_LOGIN.USER_NAME%TYPE,
        pn_profile_user_type OUT REPORT.USER_LOGIN.USER_TYPE%TYPE,
        pv_profile_first_name OUT REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_profile_last_name OUT REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_profile_email OUT REPORT.USER_LOGIN.EMAIL%TYPE,
        pv_profile_telephone OUT REPORT.USER_LOGIN.TELEPHONE%TYPE,
        pv_profile_fax OUT REPORT.USER_LOGIN.FAX%TYPE,
        pn_profile_customer_id OUT REPORT.USER_LOGIN.CUSTOMER_ID%TYPE,
        pv_profile_customer_name OUT CORP.CUSTOMER.CUSTOMER_NAME%TYPE,
        pt_profile_user_privileges OUT NUMBER_TABLE,
        pn_profile_active_bank_accts OUT PLS_INTEGER,
        pn_profile_pending_bank_accts OUT PLS_INTEGER,
        pn_profile_terminal_count OUT PLS_INTEGER,
        pv_profile_license_nbr OUT CORP.LICENSE_NBR.LICENSE_NBR%TYPE)
    AS
        ln_profile_user_id REPORT.USER_LOGIN.USER_ID%TYPE;
        ln_customer_active_bank_accts PLS_INTEGER;
    BEGIN
        UPDATE REPORT.USER_LOGIN
           SET LAST_LOGIN_TS = SYSDATE,
               FIRST_NAME = NVL(pv_login_first_name, FIRST_NAME),
               LAST_NAME = NVL(pv_login_last_name, LAST_NAME),
               EMAIL = NVL(pv_login_email, EMAIL),
               TIME_ZONE_GUID = NVL(pv_login_time_zone_guid, TIME_ZONE_GUID),
               STATUS = 'A'
         WHERE USER_NAME = pv_login_user_name
           AND USER_TYPE != 8
         RETURNING USER_ID, TIME_ZONE_GUID
              INTO pn_login_user_id, pv_login_time_zone_guid;
        IF SQL%NOTFOUND THEN
            pn_login_user_id := CREATE_USER(9, pv_login_user_name, pv_login_first_name, pv_login_last_name, pv_login_email, pv_login_time_zone_guid,  NULL, 0, NULL, NULL, NULL, NULL, NULL); 
        END IF;
        IF pn_profile_user_id IS NULL THEN
            ln_profile_user_id := pn_login_user_id;
        ELSIF REPORT.CAN_ADMIN_USER(pn_profile_user_id, pn_login_user_id)  = 'N' THEN
            IF REPORT.CHECK_PRIV(pn_login_user_id,10)='Y' THEN
              ln_profile_user_id := pn_profile_user_id;
            ELSE
              RAISE_APPLICATION_ERROR(-20100, 'User ''' || pv_login_user_name || ''' may not log in as user id ' || pn_profile_user_id); 
            END IF;
        ELSE
            ln_profile_user_id := pn_profile_user_id;
        END IF;
        POPULATE_USER(
            ln_profile_user_id,
            'Y',
            pv_profile_user_name,
            pn_profile_user_type,
            pv_profile_first_name,
            pv_profile_last_name,
            pv_profile_email,
            pv_profile_telephone,
            pv_profile_fax,
            pn_profile_customer_id,
            pv_profile_customer_name,
            pt_profile_user_privileges,
            pn_profile_active_bank_accts,
            pn_profile_pending_bank_accts,
            ln_customer_active_bank_accts,
            pn_profile_terminal_count,
            pv_profile_license_nbr);
    END;

    PROCEDURE CHECK_EXTERNAL_USER(
        pn_login_user_id OUT REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_login_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pb_login_password_hash OUT REPORT.USER_LOGIN.PASSWORD_HASH%TYPE,
        pb_login_password_salt OUT REPORT.USER_LOGIN.PASSWORD_SALT%TYPE,
        pn_login_failure_count OUT REPORT.USER_LOGIN.LOGIN_FAILURE_COUNT%TYPE,
        pn_profile_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_profile_user_name OUT REPORT.USER_LOGIN.USER_NAME%TYPE,
        pn_profile_user_type OUT REPORT.USER_LOGIN.USER_TYPE%TYPE,
        pv_profile_first_name OUT REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_profile_last_name OUT REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_profile_email OUT REPORT.USER_LOGIN.EMAIL%TYPE,
        pv_profile_telephone OUT REPORT.USER_LOGIN.TELEPHONE%TYPE,
        pv_profile_fax OUT REPORT.USER_LOGIN.FAX%TYPE,
        pn_profile_customer_id OUT REPORT.USER_LOGIN.CUSTOMER_ID%TYPE,
        pv_profile_customer_name OUT CORP.CUSTOMER.CUSTOMER_NAME%TYPE,
        pt_profile_user_privileges OUT NUMBER_TABLE,
        pn_profile_active_bank_accts OUT PLS_INTEGER,
        pn_profile_pending_bank_accts OUT PLS_INTEGER,
        pn_missing_bank_acct_flag OUT VARCHAR2,
        pn_profile_terminal_count OUT PLS_INTEGER,
        pv_profile_license_nbr OUT CORP.LICENSE_NBR.LICENSE_NBR%TYPE)
    AS
        ln_profile_user_id REPORT.USER_LOGIN.USER_ID%TYPE;
        ln_customer_active_bank_accts PLS_INTEGER;
    BEGIN
        SELECT U.USER_ID, U.PASSWORD_HASH, U.PASSWORD_SALT, U.LOGIN_FAILURE_COUNT
          INTO pn_login_user_id, pb_login_password_hash, pb_login_password_salt, pn_login_failure_count
          FROM REPORT.USER_LOGIN U
         WHERE U.USER_NAME = pv_login_user_name
           AND U.STATUS = 'A'
           AND U.USER_TYPE = 8;
        IF pn_profile_user_id IS NULL THEN
            ln_profile_user_id := pn_login_user_id;
        ELSIF REPORT.CAN_ADMIN_USER(pn_profile_user_id, pn_login_user_id)  = 'N' THEN
            RAISE_APPLICATION_ERROR(-20100, 'User ''' || pv_login_user_name || ''' may not log in as user id ' || pn_profile_user_id); 
        ELSE
            ln_profile_user_id := pn_profile_user_id;
        END IF;
        POPULATE_USER(
            ln_profile_user_id,
            'N',
            pv_profile_user_name,
            pn_profile_user_type,
            pv_profile_first_name,
            pv_profile_last_name,
            pv_profile_email,
            pv_profile_telephone,
            pv_profile_fax,
            pn_profile_customer_id,
            pv_profile_customer_name,
            pt_profile_user_privileges,
            pn_profile_active_bank_accts,
            pn_profile_pending_bank_accts,
            ln_customer_active_bank_accts,
            pn_profile_terminal_count,
            pv_profile_license_nbr);
        IF ln_customer_active_bank_accts > 0 THEN
            pn_missing_bank_acct_flag := 'N';
        ELSE
            pn_missing_bank_acct_flag := 'Y';
        END IF;
    END;
    
    
    PROCEDURE LOGIN_AS_USER(
        pn_login_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_profile_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_profile_user_name OUT REPORT.USER_LOGIN.USER_NAME%TYPE,
        pn_profile_user_type OUT REPORT.USER_LOGIN.USER_TYPE%TYPE,
        pv_profile_first_name OUT REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_profile_last_name OUT REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_profile_email OUT REPORT.USER_LOGIN.EMAIL%TYPE,
        pv_profile_telephone OUT REPORT.USER_LOGIN.TELEPHONE%TYPE,
        pv_profile_fax OUT REPORT.USER_LOGIN.FAX%TYPE,
        pn_profile_customer_id OUT REPORT.USER_LOGIN.CUSTOMER_ID%TYPE,
        pv_profile_customer_name OUT CORP.CUSTOMER.CUSTOMER_NAME%TYPE,
        pt_profile_user_privileges OUT NUMBER_TABLE,
        pn_profile_active_bank_accts OUT PLS_INTEGER,
        pn_profile_pending_bank_accts OUT PLS_INTEGER,
        pn_missing_bank_acct_flag OUT VARCHAR2,
        pn_profile_terminal_count OUT PLS_INTEGER,
        pv_profile_license_nbr OUT CORP.LICENSE_NBR.LICENSE_NBR%TYPE,
        pv_profile_readonly OUT VARCHAR2)
    AS
        lv_permit CHAR(1);
        ln_customer_active_bank_accts PLS_INTEGER;
    BEGIN
        lv_permit := REPORT.CAN_ADMIN_USER(pn_profile_user_id, pn_login_user_id);
        IF lv_permit = 'N' AND REPORT.CHECK_PRIV(pn_login_user_id,10)!='Y' THEN
            RAISE_APPLICATION_ERROR(-20100, 'User id' || pn_login_user_id || ' may not log in as user id ' || pn_profile_user_id); 
        END IF;
        POPULATE_USER(
            pn_profile_user_id,
            '?',
            pv_profile_user_name,
            pn_profile_user_type,
            pv_profile_first_name,
            pv_profile_last_name,
            pv_profile_email,
            pv_profile_telephone,
            pv_profile_fax,
            pn_profile_customer_id,
            pv_profile_customer_name,
            pt_profile_user_privileges,
            pn_profile_active_bank_accts,
            pn_profile_pending_bank_accts,
            ln_customer_active_bank_accts,
            pn_profile_terminal_count,
            pv_profile_license_nbr); 
        IF lv_permit = 'Y' THEN
            pv_profile_readonly := 'N';
        ELSE
            pv_profile_readonly := 'Y';
        END IF;
        IF ln_customer_active_bank_accts > 0 THEN
            pn_missing_bank_acct_flag := 'N';
        ELSE
            pn_missing_bank_acct_flag := 'Y';
        END IF;
    END;
    
    PROCEDURE RECORD_FAILED_EXTERNAL_LOGIN(
        pn_login_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_login_failure_count OUT REPORT.USER_LOGIN.LOGIN_FAILURE_COUNT%TYPE)
    AS
    BEGIN
        UPDATE REPORT.USER_LOGIN
           SET LOGIN_FAILURE_COUNT = LOGIN_FAILURE_COUNT + 1
         WHERE USER_ID = pn_login_user_id
          RETURNING LOGIN_FAILURE_COUNT 
          INTO pn_login_failure_count;
    END;
        
    PROCEDURE RECORD_EXTERNAL_LOGIN(
        pn_login_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_login_time_zone_guid IN OUT REPORT.USER_LOGIN.TIME_ZONE_GUID%TYPE)
    AS
    BEGIN
        UPDATE REPORT.USER_LOGIN
           SET LAST_LOGIN_TS = SYSDATE,
               TIME_ZONE_GUID = NVL(pv_login_time_zone_guid, TIME_ZONE_GUID),
               LOGIN_FAILURE_COUNT = 0
         WHERE USER_ID = pn_login_user_id
          RETURNING TIME_ZONE_GUID
          INTO pv_login_time_zone_guid;
    END;
    
    FUNCTION CREATE_PASSCODE(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_passcode_type_id REPORT.PASSCODE_TYPE.PASSCODE_TYPE_ID%TYPE,
        pd_expiration_dt REPORT.USER_PASSCODE.EXPIRATION_TS%TYPE)
    RETURN REPORT.USER_PASSCODE.PASSCODE%TYPE
    IS
        lv_passcode REPORT.USER_PASSCODE.PASSCODE%TYPE;
    BEGIN
        lv_passcode := DBMS_RANDOM.STRING('A', 30);
        INSERT INTO REPORT.USER_PASSCODE(USER_PASSCODE_ID, USER_ID, PASSCODE, PASSCODE_TYPE_ID, EXPIRATION_TS)
            VALUES(REPORT.SEQ_USER_PASSCODE_ID.NEXTVAL, pn_user_id, lv_passcode, pn_passcode_type_id, pd_expiration_dt);
        RETURN lv_passcode;
    EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
            RETURN CREATE_PASSCODE(pn_user_id, pn_passcode_type_id, pd_expiration_dt);
    END;
    
    PROCEDURE RESET_PASSWORD(
        pv_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pv_passcode OUT REPORT.USER_PASSCODE.PASSCODE%TYPE,
        pv_email OUT REPORT.USER_LOGIN.EMAIL%TYPE)
    IS
        ln_user_id REPORT.USER_LOGIN.USER_ID%TYPE;
        ld_expiration_dt REPORT.USER_PASSCODE.EXPIRATION_TS%TYPE;
    BEGIN
        SELECT SYSDATE + DURATION_DAYS
          INTO ld_expiration_dt
          FROM REPORT.PASSCODE_TYPE
         WHERE PASSCODE_TYPE_ID = 1;
        UPDATE REPORT.USER_LOGIN
           SET PASSWORD_RESET_FLAG = 'Y'
         WHERE USER_NAME = pv_user_name
         RETURNING USER_ID, EMAIL INTO ln_user_id, pv_email;
        IF ln_user_id IS NULL OR pv_email IS NULL THEN
            RAISE_APPLICATION_ERROR(-20100, 'User ''' || pv_user_name || ''' is not configured for password reset because an email is not registerd'); 
        END IF;
        pv_passcode := CREATE_PASSCODE(ln_user_id, 1, ld_expiration_dt);
    END;
    
    PROCEDURE CANCEL_PASSWORD_RESET(
        pv_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pv_passcode REPORT.USER_PASSCODE.PASSCODE%TYPE)
    IS
        ln_user_id REPORT.USER_LOGIN.USER_ID%TYPE;
        lc_password_reset_flag REPORT.USER_LOGIN.PASSWORD_RESET_FLAG%TYPE;
    BEGIN
        SELECT USER_ID, PASSWORD_RESET_FLAG
          INTO ln_user_id, lc_password_reset_flag
          FROM REPORT.USER_LOGIN
         WHERE USER_NAME = pv_user_name;
        IF lc_password_reset_flag = 'N' THEN
            RAISE_APPLICATION_ERROR(-20400, 'Password has already been cancelled');
        END IF;
        UPDATE REPORT.USER_PASSCODE
           SET EXPIRATION_TS = SYSDATE
         WHERE PASSCODE = pv_passcode
           AND USER_ID = ln_user_id;
        IF SQL%NOTFOUND THEN
             RAISE_APPLICATION_ERROR(-20401, 'Invalid passcode');
        END IF;
        UPDATE REPORT.USER_LOGIN
           SET PASSWORD_RESET_FLAG = 'N'
         WHERE USER_ID = ln_user_id;        
    END;
    
    PROCEDURE CHANGE_PASSWORD_BY_PASSCODE(
        pv_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pv_passcode REPORT.USER_PASSCODE.PASSCODE%TYPE,
        pb_password_hash REPORT.USER_LOGIN.PASSWORD_HASH%TYPE,
        pb_password_salt REPORT.USER_LOGIN.PASSWORD_SALT%TYPE)
    IS
        ln_user_id REPORT.USER_LOGIN.USER_ID%TYPE;
        lc_password_reset_flag REPORT.USER_LOGIN.PASSWORD_RESET_FLAG%TYPE;
    BEGIN
        SELECT USER_ID, PASSWORD_RESET_FLAG
          INTO ln_user_id, lc_password_reset_flag
          FROM REPORT.USER_LOGIN
         WHERE USER_NAME = pv_user_name;
        IF lc_password_reset_flag = 'N' THEN
            RAISE_APPLICATION_ERROR(-20400, 'Password has already been changed');
        END IF;
        UPDATE REPORT.USER_PASSCODE
           SET EXPIRATION_TS = SYSDATE
         WHERE PASSCODE = pv_passcode
           AND USER_ID = ln_user_id;
        IF SQL%NOTFOUND THEN
             RAISE_APPLICATION_ERROR(-20401, 'Invalid passcode');
        END IF;
        UPDATE REPORT.USER_LOGIN
           SET PASSWORD_RESET_FLAG = 'N',
               PASSWORD_HASH = pb_password_hash,
               PASSWORD_SALT = pb_password_salt
         WHERE USER_ID = ln_user_id;        
    END; 
    
    FUNCTION CREATE_USER(
        pn_user_type REPORT.USER_LOGIN.USER_TYPE%TYPE,
        pv_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pv_first_name REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_last_name REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_email REPORT.USER_LOGIN.EMAIL%TYPE,
        pv_time_zone_guid REPORT.USER_LOGIN.TIME_ZONE_GUID%TYPE,
        pn_admin_id REPORT.USER_LOGIN.ADMIN_ID%TYPE,
        pn_customer_id REPORT.USER_LOGIN.CUSTOMER_ID%TYPE,
        pv_telephone REPORT.USER_LOGIN.TELEPHONE%TYPE,
        pv_fax REPORT.USER_LOGIN.FAX%TYPE,
        pb_password_hash REPORT.USER_LOGIN.PASSWORD_HASH%TYPE,
        pb_password_salt REPORT.USER_LOGIN.PASSWORD_SALT%TYPE,
        pt_user_privileges NUMBER_TABLE)
    RETURN REPORT.USER_LOGIN.USER_ID%TYPE
    IS
		ln_user_id REPORT.USER_LOGIN.USER_ID%TYPE;
	BEGIN
		SELECT REPORT.USER_LOGIN_SEQ.NEXTVAL 
		  INTO ln_user_id
		  FROM DUAL;
	 	INSERT INTO REPORT.USER_LOGIN(
            USER_ID, 
            USER_TYPE, 
            USER_NAME, 
            FIRST_NAME, 
            LAST_NAME, 
            EMAIL, 
            TIME_ZONE_GUID,
            ADMIN_ID, 
            CUSTOMER_ID, 
            TELEPHONE, 
            FAX, 
            PASSWORD_HASH, 
            PASSWORD_SALT,
            LAST_LOGIN_TS)
          VALUES(
            ln_user_id, 
            pn_user_type,
            pv_user_name,
            pv_first_name,
            pv_last_name,
            pv_email,
            pv_time_zone_guid,
            NVL(pn_admin_id, 0),
            NVL(pn_customer_id, 0),
            pv_telephone,
            pv_fax,
            pb_password_hash,
            pb_password_salt,
            SYSDATE);
        IF pt_user_privileges IS NOT NULL THEN
            IF NVL(pn_admin_id, 0) = 0 THEN
                INSERT INTO REPORT.USER_PRIVS(USER_ID, PRIV_ID)
                  SELECT ln_user_id, P.PRIV_ID
                    FROM REPORT.PRIV P
                   WHERE P.PRIV_ID MEMBER OF pt_user_privileges
                     AND (pn_user_type != 8 OR P.INTERNAL_EXTERNAL_FLAG != 'I');
            ELSE
                INSERT INTO REPORT.USER_PRIVS(USER_ID, PRIV_ID)
                  SELECT ln_user_id, P.PRIV_ID
                    FROM REPORT.PRIV P
                    JOIN REPORT.USER_PRIVS UP ON UP.PRIV_ID = P.PRIV_ID
                    JOIN REPORT.USER_LOGIN U ON UP.USER_ID = U.USER_ID
                   WHERE P.PRIV_ID MEMBER OF pt_user_privileges
                     AND UP.USER_ID = pn_admin_id 
                     AND (U.USER_TYPE != 8 OR P.INTERNAL_EXTERNAL_FLAG IN('B', 'E'))
                     AND (pn_user_type != 8 OR P.INTERNAL_EXTERNAL_FLAG != 'I');
            END IF;
        END IF;
        INSERT INTO REPORT.USER_DISPLAY(USER_ID, DISPLAY_ID, SEQ)
            SELECT ln_user_id, 1, 1 FROM DUAL
            UNION ALL
            SELECT ln_user_id, 2, 2 FROM DUAL;
        INSERT INTO REPORT.REPORT_REQUEST_ORDER (USER_ID, PROFILE_MAX_REQUEST_ORDER,USER_MAX_REQUEST_ORDER)
        VALUES(ln_user_id, 0, 0);
		RETURN ln_user_id;
	END;

    PROCEDURE DELETE_USER(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE)
    IS
    BEGIN
        UPDATE REPORT.USER_LOGIN 
           SET STATUS = 'D' 
         WHERE USER_ID = pn_user_id;
        DELETE FROM REPORT.USER_PRIVS 
         WHERE USER_ID = pn_user_id;
    END;
            
    PROCEDURE UPDATE_USER(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_user_type REPORT.USER_LOGIN.USER_TYPE%TYPE,
        pv_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pv_first_name REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_last_name REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_email REPORT.USER_LOGIN.EMAIL%TYPE,
        pn_admin_id REPORT.USER_LOGIN.ADMIN_ID%TYPE,
        pn_customer_id REPORT.USER_LOGIN.CUSTOMER_ID%TYPE,
        pv_telephone REPORT.USER_LOGIN.TELEPHONE%TYPE,
        pv_fax REPORT.USER_LOGIN.FAX%TYPE,
        pb_password_hash REPORT.USER_LOGIN.PASSWORD_HASH%TYPE,
        pb_password_salt REPORT.USER_LOGIN.PASSWORD_SALT%TYPE,
        pt_user_privileges NUMBER_TABLE,
        pn_updating_user_id REPORT.USER_LOGIN.USER_ID%TYPE)
    IS
        ln_user_type REPORT.USER_LOGIN.USER_TYPE%TYPE;
	BEGIN
		UPDATE REPORT.USER_LOGIN
          SET USER_TYPE = NVL(pn_user_type, USER_TYPE), 
            USER_NAME = NVL(pv_user_name, USER_NAME), 
            FIRST_NAME = NVL(pv_first_name, FIRST_NAME), 
            LAST_NAME = NVL(pv_last_name, LAST_NAME), 
            EMAIL = NVL(pv_email, EMAIL), 
            ADMIN_ID = NVL(pn_admin_id, ADMIN_ID), 
            CUSTOMER_ID = NVL(pn_customer_id, CUSTOMER_ID), 
            TELEPHONE = NVL(pv_telephone, TELEPHONE), 
            FAX = NVL(pv_fax, FAX), 
            PASSWORD_HASH = NVL(pb_password_hash, PASSWORD_HASH), 
            PASSWORD_SALT = NVL(pb_password_salt, PASSWORD_SALT),
            PASSWORD_RESET_FLAG = DECODE(PASSWORD_HASH, NULL, PASSWORD_RESET_FLAG, 'N')
         WHERE USER_ID = pn_user_id
         RETURNING USER_TYPE INTO ln_user_type;
        IF pn_updating_user_id != pn_user_id AND pt_user_privileges IS NOT NULL THEN
            DELETE
              FROM REPORT.USER_PRIVS
             WHERE USER_ID = pn_user_id
               AND PRIV_ID IN(
                   SELECT UP.PRIV_ID 
                     FROM REPORT.USER_PRIVS UP
                     JOIN REPORT.USER_LOGIN U ON UP.USER_ID = U.USER_ID
                     JOIN REPORT.PRIV P ON UP.PRIV_ID = P.PRIV_ID
                    WHERE UP.USER_ID = pn_updating_user_id 
                      AND (U.USER_TYPE != 8 OR P.INTERNAL_EXTERNAL_FLAG IN('B', 'E')));
            INSERT INTO REPORT.USER_PRIVS(USER_ID, PRIV_ID)
              SELECT pn_user_id, P.PRIV_ID
                FROM REPORT.PRIV P
                JOIN REPORT.USER_PRIVS UP ON UP.PRIV_ID = P.PRIV_ID
                JOIN REPORT.USER_LOGIN U ON UP.USER_ID = U.USER_ID
               WHERE P.PRIV_ID MEMBER OF pt_user_privileges
                 AND UP.USER_ID = pn_updating_user_id 
                 AND (U.USER_TYPE != 8 OR P.INTERNAL_EXTERNAL_FLAG IN('B', 'E'))
                 AND (ln_user_type != 8 OR P.INTERNAL_EXTERNAL_FLAG != 'I');
        END IF;
	END;
    
    PROCEDURE CREATE_CUSTOMER(
        pn_user_id OUT CORP.CUSTOMER.USER_ID%TYPE,
        pn_cust_id OUT CORP.CUSTOMER.CUSTOMER_ID%TYPE,
        pv_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pv_first_name REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_last_name REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_email REPORT.USER_LOGIN.EMAIL%TYPE,
        pv_time_zone_guid REPORT.USER_LOGIN.TIME_ZONE_GUID%TYPE,
        pb_password_hash REPORT.USER_LOGIN.PASSWORD_HASH%TYPE,
        pb_password_salt REPORT.USER_LOGIN.PASSWORD_SALT%TYPE,
        pv_cust_name IN CORP.CUSTOMER.CUSTOMER_NAME%TYPE,
        pv_addr1 IN CORP.CUSTOMER_ADDR.ADDRESS1%TYPE,
        pv_city IN CORP.CUSTOMER_ADDR.CITY%TYPE,
        pv_state_cd IN CORP.CUSTOMER_ADDR.STATE%TYPE,
        pv_postal IN CORP.CUSTOMER_ADDR.ZIP%TYPE,
        pv_country_cd IN CORP.CUSTOMER_ADDR.COUNTRY_CD%TYPE,       
        pv_telephone REPORT.USER_LOGIN.TELEPHONE%TYPE,
        pv_fax REPORT.USER_LOGIN.FAX%TYPE,
        pn_dealer_id IN CORP.CUSTOMER.DEALER_ID%TYPE,
        pv_tax_id_nbr IN CORP.CUSTOMER.TAX_ID_NBR%TYPE)
    IS
        ln_addr_id CORP.CUSTOMER_ADDR.ADDRESS_ID%TYPE;
        lv_lic_nbr CORP.LICENSE_NBR.LICENSE_NBR%TYPE;
        ln_lic_id CORP.LICENSE_NBR.LICENSE_ID%TYPE;
        l_user_privs NUMBER_TABLE;
    BEGIN
        SELECT LICENSE_ID 
          INTO ln_lic_id 
          FROM CORP.VW_DEALER_LICENSE 
         WHERE DEALER_ID = pn_dealer_id;
        SELECT CORP.CUSTOMER_SEQ.NEXTVAL, CORP.CUSTOMER_ADDR_SEQ.NEXTVAL 
          INTO pn_cust_id, ln_addr_id 
          FROM DUAL;
        SELECT PRIV_ID
          BULK COLLECT INTO l_user_privs
        FROM REPORT.PRIV WHERE CUSTOMER_MASTER_USER_DEFAULT='Y';

        pn_user_id := CREATE_USER(8, pv_user_name, pv_first_name, pv_last_name, pv_email, pv_time_zone_guid, 0, pn_cust_id, pv_telephone, pv_fax, pb_password_hash, pb_password_salt, l_user_privs);
        INSERT INTO CORP.CUSTOMER(CUSTOMER_ID, CUSTOMER_NAME, USER_ID, CREATE_BY, DEALER_ID, TAX_ID_NBR)
             VALUES(pn_cust_id, pv_cust_name, pn_user_id, pn_user_id, pn_dealer_id, pv_tax_id_nbr);
        INSERT INTO CORP.CUSTOMER_ADDR(ADDRESS_ID, CUSTOMER_ID, ADDR_TYPE, NAME, ADDRESS1, CITY, STATE, ZIP, COUNTRY_CD)
             VALUES (ln_addr_id, pn_cust_id, 2, pv_first_name || ' ' || pv_last_name, pv_addr1, pv_city, pv_state_cd, pv_postal, pv_country_cd);
        CORP.CREATE_LICENSE(ln_lic_id, pn_cust_id, lv_lic_nbr);
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
             RAISE_APPLICATION_ERROR(-20100, 'Could not find license agreement for this dealer');
        WHEN OTHERS THEN
             RAISE;
    END;
    
    PROCEDURE CREATE_CUSTOM_REPORT_LINK(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_label WEB_CONTENT.WEB_LINK.WEB_LINK_LABEL%TYPE,
        pv_url WEB_CONTENT.WEB_LINK.WEB_LINK_URL%TYPE,
        pv_desc WEB_CONTENT.WEB_LINK.WEB_LINK_DESC%TYPE,        
        pn_order WEB_CONTENT.WEB_LINK.WEB_LINK_ORDER%TYPE,
        pv_group WEB_CONTENT.WEB_LINK.WEB_LINK_GROUP%TYPE DEFAULT 'User Defined')
    IS
        ln_link_id WEB_CONTENT.WEB_LINK.WEB_LINK_ID%TYPE;
    BEGIN
        SELECT WEB_CONTENT.SEQ_WEB_LINK_ID.NEXTVAL
          INTO ln_link_id
          FROM DUAL;
        INSERT INTO WEB_CONTENT.WEB_LINK(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_ORDER, WEB_LINK_GROUP, WEB_LINK_USAGE)
            VALUES(ln_link_id, pv_label, pv_url, pv_desc, pn_order, pv_group, '-');
        INSERT INTO REPORT.USER_LINK(USER_ID, LINK_ID, INCLUDE, USAGE)
            VALUES(pn_user_id, ln_link_id, 'Y', 'U');
    END;
        
    PROCEDURE DELETE_USER_LINK(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_link_id WEB_CONTENT.WEB_LINK.WEB_LINK_ID%TYPE)
    IS
    BEGIN
        DELETE FROM REPORT.USER_LINK
         WHERE USER_ID = pn_user_id 
           AND LINK_ID = pn_link_id;
        IF SQL%NOTFOUND THEN
            RAISE NO_DATA_FOUND;
        END IF;
        DELETE FROM WEB_CONTENT.WEB_LINK
         WHERE WEB_LINK_ID = pn_link_id
           AND NOT EXISTS(SELECT 1 FROM REPORT.USER_LINK WHERE LINK_ID = pn_link_id);
    END;
    
    FUNCTION GET_USER_PREFERENCE(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_pref_id REPORT.PREFERENCE.PREFERENCE_ID%TYPE)
        RETURN REPORT.USER_PREFERENCE.PREFERENCE_VALUE%TYPE
    IS
        ln_value REPORT.USER_PREFERENCE.PREFERENCE_VALUE%TYPE;
    BEGIN
        SELECT DECODE(UP.PREFERENCE_ID, NULL, P.PREFERENCE_DEFAULT, UP.PREFERENCE_VALUE)
          INTO ln_value
          FROM REPORT.PREFERENCE P
          LEFT OUTER JOIN REPORT.USER_PREFERENCE UP ON P.PREFERENCE_ID = UP.PREFERENCE_ID AND UP.USER_ID = pn_user_id
         WHERE P.PREFERENCE_ID = pn_pref_id;
        RETURN ln_value;
    END;
    
    FUNCTION GET_HEALTH_CODE(
        pn_measured_date DATE,
        pn_min_days NUMBER,
        pn_max_days NUMBER)
        RETURN CHAR
    IS
    BEGIN
        IF pn_measured_date IS NULL THEN
            RETURN 'D'; -- Never accessed
        END IF;
        IF pn_min_days IS NULL OR pn_min_days <= 0 THEN
            RETURN '-'; -- Not measured
        ELSIF pn_measured_date < SYSDATE - pn_max_days THEN
            RETURN 'O'; -- Old
        ELSIF pn_measured_date < SYSDATE - pn_min_days THEN
            RETURN 'Y'; -- Not healthy
        ELSE
            RETURN 'A'; -- Healthy
        END IF;
    END;
    
    FUNCTION GET_HEALTH_CODE_BY_PREF(
        pn_date DATE,
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_pref_id REPORT.PREFERENCE.PREFERENCE_ID%TYPE)
        RETURN CHAR
    IS
        ln_pref_days NUMBER;
    BEGIN
        ln_pref_days := TO_NUMBER_OR_NULL(GET_USER_PREFERENCE(pn_user_id, pn_pref_id));
        RETURN GET_HEALTH_CODE(pn_date, ln_pref_days, 1 + 2 * ln_pref_days);
    END;
    
    PROCEDURE UPSERT_USER_PREFERENCE(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_preference_id REPORT.PREFERENCE.PREFERENCE_ID%TYPE,
        pv_preference_value REPORT.USER_PREFERENCE.PREFERENCE_VALUE%TYPE)
    IS
    BEGIN
        UPDATE REPORT.USER_PREFERENCE 
           SET PREFERENCE_VALUE = pv_preference_value 
         WHERE USER_ID = pn_user_id 
           AND PREFERENCE_ID = pn_preference_id;
        IF SQL%ROWCOUNT < 1 THEN
             BEGIN
                INSERT INTO REPORT.USER_PREFERENCE(
                    USER_ID,
                    PREFERENCE_ID,
                    PREFERENCE_VALUE
                ) VALUES(
                    pn_user_id,
                    pn_preference_id,
                    pv_preference_value
                ); 
            EXCEPTION
                WHEN DUP_VAL_ON_INDEX THEN
                    UPSERT_USER_PREFERENCE(pn_user_id, pn_preference_id, pv_preference_value);
            END;
        END IF;
    END;
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/DW_PKG.pbk?rev=1.42
CREATE OR REPLACE PACKAGE BODY REPORT.DW_PKG IS
    FUNCTION GET_OR_CREATE_EXPORT_BATCH(
        l_customer_id CORP.CUSTOMER.CUSTOMER_ID%TYPE)
     RETURN ACTIVITY_REF.BATCH_ID%TYPE
    IS
    PRAGMA AUTONOMOUS_TRANSACTION;
        l_export_id ACTIVITY_REF.BATCH_ID%TYPE;
        l_lock VARCHAR2(128);
    BEGIN
        l_lock := GLOBALS_PKG.REQUEST_LOCK('EXPORT.CUSTOMER_ID',l_customer_id);
        SELECT eb.batch_id
          INTO l_export_id
          FROM REPORT.EXPORT_BATCH eb
         WHERE eb.CUSTOMER_ID = l_customer_id
           AND eb.EXPORT_TYPE = 1
           AND eb.STATUS = 'O';
        COMMIT;
        RETURN l_export_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            SELECT REPORT.EXPORT_BATCH_SEQ.NEXTVAL
              INTO l_export_id
              FROM DUAL;
            
            INSERT INTO REPORT.EXPORT_BATCH(
                   BATCH_ID,
                   CUSTOMER_ID,
                   EXPORT_TYPE,
                   STATUS)
               SELECT
                   l_export_id,
                   l_customer_id,
                   1,
                   'O'
                 FROM DUAL
                WHERE NOT EXISTS(
                    SELECT 1
                      FROM REPORT.EXPORT_BATCH eb
                     WHERE eb.CUSTOMER_ID = l_customer_id
                       AND eb.EXPORT_TYPE = 1
                       AND eb.STATUS = 'O');
            COMMIT;
            IF SQL%FOUND THEN
                RETURN l_export_id;
            ELSE
                RETURN GET_OR_CREATE_EXPORT_BATCH(l_customer_id);
            END IF;
        WHEN OTHERS THEN
            RAISE;
    END;
    
    PROCEDURE CLOSE_EXPORT_BATCH(
        l_export_id REPORT.EXPORT_BATCH.BATCH_ID%TYPE,
        l_customer_id REPORT.EXPORT_BATCH.CUSTOMER_ID%TYPE)
    IS
        l_lock VARCHAR2(128);
    BEGIN
        l_lock := GLOBALS_PKG.REQUEST_LOCK('EXPORT.CUSTOMER_ID',l_customer_id);
        UPDATE REPORT.EXPORT_BATCH EB SET (
                STATUS,
                TRAN_CREATE_DT_BEG,
                TRAN_CREATE_DT_END,
                TOT_TRAN_ROWS,
                TOT_TRAN_AMOUNT
              ) = (SELECT
                'A',
                MIN(A.TRAN_DATE),
                MAX(A.TRAN_DATE),
                COUNT(*),
                SUM(A.TOTAL_AMOUNT)
              FROM REPORT.ACTIVITY_REF A
             WHERE A.BATCH_ID = eb.BATCH_ID)
         WHERE EB.BATCH_ID = l_export_id
           AND EB.STATUS = 'O';
         CORP.PAYMENTS_PKG.UPDATE_EXPORT_BATCH(l_export_id);
    END;
    
    PROCEDURE CLOSE_EXPORT_BATCHES
    IS
        CURSOR l_cur IS
             SELECT EB.BATCH_ID, EB.CUSTOMER_ID
              FROM REPORT.EXPORT_BATCH EB
              JOIN (SELECT CUSTOMER_ID, 
              CASE WHEN BATCH_CLOSE_DATE > SYSDATE THEN BATCH_CLOSE_DATE - 1 ELSE BATCH_CLOSE_DATE END BATCH_CLOSE_DATE 
              FROM (SELECT CUSTOMER_ID, TO_DATE(TO_CHAR(SYSDATE, 'MM/DD/YYYY')||' '||NVL(BATCH_CLOSE_TIME,'05:00'), 'MM/DD/YYYY HH24:MI') BATCH_CLOSE_DATE 
              FROM CORP.CUSTOMER)) C ON EB.CUSTOMER_ID = C.CUSTOMER_ID
            WHERE EB.STATUS = 'O'
              AND EB.CRD_DATE < C.BATCH_CLOSE_DATE
              AND SYSDATE > C.BATCH_CLOSE_DATE;
    BEGIN
        FOR l_rec IN l_cur LOOP
            CLOSE_EXPORT_BATCH(l_rec.BATCH_ID, l_rec.CUSTOMER_ID);
            COMMIT;
        END LOOP;
    END;
    
    PROCEDURE CLOSE_EXPORT_BATCH_FOR(
        l_customer_id CORP.CUSTOMER.CUSTOMER_ID%TYPE)
    IS
        CURSOR l_cur IS
            SELECT BATCH_ID
              FROM REPORT.EXPORT_BATCH
            WHERE STATUS = 'O'
              AND CUSTOMER_ID = l_customer_id;
    BEGIN
        FOR l_rec IN l_cur LOOP
            CLOSE_EXPORT_BATCH(l_rec.BATCH_ID, l_customer_id);
            COMMIT;
        END LOOP;
    END;
    
    PROCEDURE UPDATE_SETTLEMENT(
        l_trans_id TRANS.TRAN_ID%TYPE,
        l_settle_state_id TRANS.SETTLE_STATE_ID%TYPE,
        l_settle_date TRANS.SETTLE_DATE%TYPE,
        l_appr_cd TRANS.CC_APPR_CODE%TYPE)
    IS
    BEGIN
        UPDATE ACTIVITY_REF SET CC_APPR_CODE = l_appr_cd,
            SETTLE_STATE = (SELECT STATE_LABEL FROM TRANS_STATE S
                WHERE l_settle_state_id = S.STATE_ID)
         WHERE TRAN_ID = l_trans_id;
    END;

PROCEDURE UPDATE_TRAN_INFO(
        l_trans_id TRANS.TRAN_ID%TYPE)
    IS
        l_ref_nbr ACTIVITY_REF.REF_NBR%TYPE;
        l_export_id ACTIVITY_REF.BATCH_ID%TYPE;
        l_create_dt ACTIVITY_REF.CREATE_DT%TYPE;
        l_update_dt ACTIVITY_REF.UPDATE_DT%TYPE;
        l_qty ACTIVITY_REF.QUANTITY%TYPE;
        l_terminal_id ACTIVITY_REF.TERMINAL_ID%TYPE;
        l_eport_id ACTIVITY_REF.EPORT_ID%TYPE;
        l_tran_date ACTIVITY_REF.TRAN_DATE%TYPE;
        l_trans_type_id ACTIVITY_REF.TRANS_TYPE_ID%TYPE;
        l_total_amount ACTIVITY_REF.TOTAL_AMOUNT%TYPE;
        l_currency_id ACTIVITY_REF.CURRENCY_ID%TYPE;
        l_customer_id CORP.CUSTOMER.CUSTOMER_ID%TYPE;
        l_convenience_fee ACTIVITY_REF.CONVENIENCE_FEE%TYPE;
        l_loyalty_discount ACTIVITY_REF.LOYALTY_DISCOUNT%TYPE;
        l_consumer_acct_id ACTIVITY_REF.CONSUMER_ACCT_ID%TYPE;
		lv_payment_entry_method_desc PSS.PAYMENT_ENTRY_METHOD.PAYMENT_ENTRY_METHOD_DESC%TYPE;
		lv_payment_subtype_class PSS.TRAN.PAYMENT_SUBTYPE_CLASS%TYPE;
		lv_card_company REPORT.ACTIVITY_REF.CARD_COMPANY%TYPE;
    l_payment_batch_id CORP.BATCH.BATCH_ID%TYPE;
    l_cust_bank_id CORP.DOC.CUSTOMER_BANK_ID%TYPE;
        ln_cnt PLS_INTEGER;
        CURSOR l_cur IS
         SELECT
            R.REGION_ID,
            R.REGION_NAME,
            L.LOCATION_NAME,
            L.LOCATION_ID,
            T.TERMINAL_ID,
            TERM.TERMINAL_NAME,
            TERM.TERMINAL_NBR,
            T.TRANS_TYPE_ID,
            TT.TRANS_TYPE_NAME,
            S.STATE_LABEL,
            T.CARD_NUMBER,
            T.CC_APPR_CODE,
            T.CLOSE_DATE,
            T.TOTAL_AMOUNT,
            T.ORIG_TRAN_ID,
            T.EPORT_ID,
            T.DESCRIPTION,
            T.CURRENCY_ID,
            T.CARDTYPE_AUTHORITY_ID,
            TERM.CUSTOMER_ID,
            SUM(P.AMOUNT*NVL(P.PRICE,0)) CONVENIENCE_FEE,
            SUM(PL.AMOUNT*NVL(PL.PRICE,0)) LOYALTY_DISCOUNT,
            T.CONSUMER_ACCT_ID,
			TT.CREDIT_IND,
			TT.CASH_IND
          FROM REPORT.TRANS T join REPORT.TRANS_TYPE TT on T.TRANS_TYPE_ID = TT.TRANS_TYPE_ID
          JOIN REPORT.TRANS_STATE S on T.SETTLE_STATE_ID = S.STATE_ID
          LEFT OUTER JOIN REPORT.PURCHASE P on T.TRAN_ID = P.TRAN_ID and (P.TRAN_LINE_ITEM_TYPE_ID = 203 OR (TRAN_LINE_ITEM_TYPE_ID IS NULL AND P.VEND_COLUMN IN ('Two-Tier Pricing', 'Convenience Fee')))
          LEFT OUTER JOIN REPORT.PURCHASE PL on T.TRAN_ID = PL.TRAN_ID and PL.tran_line_item_type_id =204
          LEFT OUTER JOIN REPORT.TERMINAL TERM on T.TERMINAL_ID = TERM.TERMINAL_ID
          LEFT OUTER JOIN REPORT.LOCATION L on TERM.LOCATION_ID = L.LOCATION_ID
          LEFT OUTER JOIN REPORT.TERMINAL_REGION TR on TERM.TERMINAL_ID = TR.TERMINAL_ID
          LEFT OUTER JOIN REPORT.REGION R on TR.REGION_ID = R.REGION_ID
          WHERE T.TRAN_ID = l_trans_id
          GROUP BY
            R.REGION_ID,
            R.REGION_NAME,
            L.LOCATION_NAME,
            L.LOCATION_ID,
            T.TERMINAL_ID,
            TERM.TERMINAL_NAME,
            TERM.TERMINAL_NBR,
            T.TRANS_TYPE_ID,
            TT.TRANS_TYPE_NAME,
            S.STATE_LABEL,
            T.CARD_NUMBER,
            T.CC_APPR_CODE,
            T.CLOSE_DATE,
            T.TOTAL_AMOUNT,
            T.ORIG_TRAN_ID,
            T.EPORT_ID,
            T.DESCRIPTION,
            T.CURRENCY_ID,
            T.CARDTYPE_AUTHORITY_ID,
            TERM.CUSTOMER_ID,
            T.CONSUMER_ACCT_ID,
            TT.CREDIT_IND,
            TT.CASH_IND;
    BEGIN
        DELETE FROM REPORT.ACTIVITY_REF
         WHERE TRAN_ID = L_TRANS_ID
         RETURNING REF_NBR, BATCH_ID, CREATE_DT, SYSDATE, TERMINAL_ID, EPORT_ID, TRAN_DATE, TRANS_TYPE_ID, TOTAL_AMOUNT, QUANTITY, CURRENCY_ID, CONVENIENCE_FEE, LOYALTY_DISCOUNT, CONSUMER_ACCT_ID, PAYMENT_BATCH_ID
              INTO l_ref_nbr, l_export_id, l_create_dt, l_update_dt, l_terminal_id, l_eport_id, l_tran_date, l_trans_type_id, l_total_amount, l_qty, l_currency_id, l_convenience_fee, l_loyalty_discount,l_consumer_acct_id, l_payment_batch_id;       
        
        -- if exists, then update the trans_stat_by_day
        IF NVL(l_qty, 0) != 0 AND l_create_dt is NOT NULL THEN
          UPDATE REPORT.TRANS_STAT_BY_DAY 
             SET TRAN_COUNT = NVL(tran_count, 0) - 1,
                 VEND_COUNT = NVL(VEND_COUNT, 0) - l_qty,
                 TRAN_AMOUNT = NVL(TRAN_AMOUNT, 0) - NVL(l_total_amount, 0),
                 CONVENIENCE_FEE = NVL(CONVENIENCE_FEE, 0) - NVL(l_convenience_fee, 0),
                 LOYALTY_DISCOUNT = NVL(LOYALTY_DISCOUNT, 0) - NVL(l_loyalty_discount, 0)
           WHERE TERMINAL_ID = l_terminal_id
             AND EPORT_ID = l_eport_id
             AND TRAN_DATE = trunc(l_tran_date, 'DD')
             AND TRANS_TYPE_ID = l_trans_type_id
             AND CURRENCY_ID = l_currency_id;
             
             IF l_trans_type_id = 22 THEN               
                UPDATE CORP.BATCH_TOTAL
                SET LEDGER_AMOUNT = LEDGER_AMOUNT - l_total_amount,
                  LEDGER_COUNT = LEDGER_COUNT - 1
                WHERE BATCH_ID = l_payment_batch_id
                  AND ENTRY_TYPE = 'CA' ;
             END IF;  
             IF l_trans_type_id = 16 AND l_convenience_fee>0 THEN               
                UPDATE CORP.BATCH_TOTAL
                SET LEDGER_AMOUNT = LEDGER_AMOUNT - l_convenience_fee,
                  LEDGER_COUNT = LEDGER_COUNT - 1
                WHERE BATCH_ID = l_payment_batch_id
                  AND ENTRY_TYPE = 'TT' ;
             END IF;    
        END IF;
        COMMIT;
        FOR l_rec IN l_cur LOOP
            IF NVL(l_export_id, 0) = 0 THEN
                IF l_rec.customer_id IS NULL THEN
                    l_export_id := 0;
                ELSE
                    l_export_id := GET_OR_CREATE_EXPORT_BATCH(l_rec.customer_id);
                END IF;
            END IF;
            IF l_ref_nbr IS NULL THEN
                SELECT REF_NBR_SEQ.NEXTVAL
                  INTO l_ref_nbr 
                  FROM DUAL;
            END IF;
            SELECT NVL(SUM(P.AMOUNT), 0)
              INTO l_qty
              FROM REPORT.PURCHASE P
              LEFT OUTER JOIN PSS.TRAN_LINE_ITEM_TYPE IT ON P.TRAN_LINE_ITEM_TYPE_ID = IT.TRAN_LINE_ITEM_TYPE_ID
             WHERE P.TRAN_ID = l_trans_id
              AND (IT.TRAN_LINE_ITEM_TYPE_GROUP_CD IN('S', 'P')
               OR (P.TRAN_LINE_ITEM_TYPE_ID IS NULL AND P.VEND_COLUMN NOT IN ('Two-Tier Pricing', 'Convenience Fee')));
			   
			SELECT MAX(PEM.PAYMENT_ENTRY_METHOD_DESC), MAX(X.PAYMENT_SUBTYPE_CLASS)
			INTO lv_payment_entry_method_desc, lv_payment_subtype_class
			FROM REPORT.TRANS T
			JOIN PSS.TRAN X ON T.MACHINE_TRANS_NO = X.TRAN_GLOBAL_TRANS_CD
			JOIN PSS.CLIENT_PAYMENT_TYPE CPT ON X.CLIENT_PAYMENT_TYPE_CD = CPT.CLIENT_PAYMENT_TYPE_CD
			LEFT OUTER JOIN PSS.PAYMENT_ENTRY_METHOD PEM ON CPT.PAYMENT_ENTRY_METHOD_CD = PEM.PAYMENT_ENTRY_METHOD_CD AND PEM.PAYMENT_ENTRY_METHOD_CD NOT IN ('M', 'S')
			WHERE T.TRAN_ID = l_trans_id;
			
			lv_card_company := REPORT.CARD_NAME(l_rec.CARDTYPE_AUTHORITY_ID, l_rec.TRANS_TYPE_ID, l_rec.CARD_NUMBER);
			IF lv_payment_subtype_class = 'Demo' THEN
				lv_card_company := lv_card_company || ' Demo';
			END IF;
			IF lv_payment_entry_method_desc IS NOT NULL THEN
				lv_card_company := lv_card_company || ' (' || lv_payment_entry_method_desc || ')';
			END IF;
        IF l_rec.TRANS_TYPE_ID = 22  OR (l_rec.TRANS_TYPE_ID = 16 AND l_rec.convenience_fee>0) THEN
          select customer_bank_id into l_cust_bank_id from report.trans where tran_id=L_TRANS_ID;
          l_payment_batch_id:=CORP.PAYMENTS_PKG.GET_OR_CREATE_BATCH(l_rec.TERMINAL_ID,l_cust_bank_id,l_rec.CLOSE_DATE,l_rec.currency_id,'N');
        END IF;
            INSERT INTO ACTIVITY_REF(
                REGION_ID,
                REGION_NAME,
                LOCATION_NAME,
                LOCATION_ID,
                TERMINAL_ID,
                TERMINAL_NAME,
                TERMINAL_NBR,
                TRAN_ID,
                TRANS_TYPE_ID,
                TRANS_TYPE_NAME,
                SETTLE_STATE,
                CARD_NUMBER,
                CARD_COMPANY,
                CC_APPR_CODE,
                TRAN_DATE,
                FILL_DATE,
                TOTAL_AMOUNT,
                ORIG_TRAN_ID,
                BATCH_ID,
                VEND_COLUMN,
                QUANTITY,
                REF_NBR,
                EPORT_ID,
                DESCRIPTION,
                CREATE_DT,
                UPDATE_DT,
                CURRENCY_ID,
                CONVENIENCE_FEE,
                LOYALTY_DISCOUNT,
                CONSUMER_ACCT_ID,
                PAYMENT_BATCH_ID
                )
            SELECT
                l_rec.REGION_ID,
                l_rec.REGION_NAME,
                NVL(l_rec.LOCATION_NAME, 'Unknown'),
                NVL(l_rec.LOCATION_ID, 0),
                l_rec.TERMINAL_ID,
                l_rec.TERMINAL_NAME,
                l_rec.TERMINAL_NBR,
                l_trans_id,
                l_rec.TRANS_TYPE_ID,
                l_rec.TRANS_TYPE_NAME,
                l_rec.STATE_LABEL,
                l_rec.CARD_NUMBER,
                lv_card_company,
                l_rec.CC_APPR_CODE,
                l_rec.CLOSE_DATE TRAN_DATE,
                GET_FILL_DATE(l_trans_id) FILL_DATE,
                l_rec.TOTAL_AMOUNT,
                l_rec.ORIG_TRAN_ID,
                l_export_id,
                VEND_COLUMN_STRING(l_trans_id),
                l_qty,
                l_ref_nbr,
                l_rec.EPORT_ID,
                l_rec.DESCRIPTION,
                NVL(l_create_dt, SYSDATE),
                l_update_dt,
                l_rec.currency_id,
                l_rec.convenience_fee,
                l_rec.loyalty_discount,
                l_rec.consumer_acct_id,
                l_payment_batch_id
              FROM DUAL;
            -- update trans_stat_by_day
            IF l_qty != 0  OR l_rec.TOTAL_AMOUNT != 0 THEN
                LOOP
                    UPDATE REPORT.TRANS_STAT_BY_DAY
                       SET TRAN_COUNT = NVL(TRAN_COUNT, 0) + 1,
                           VEND_COUNT = NVL(VEND_COUNT, 0) + l_qty,
                           TRAN_AMOUNT = NVL(TRAN_AMOUNT, 0) + NVL(l_rec.TOTAL_AMOUNT, 0),
                           CONVENIENCE_FEE = NVL(CONVENIENCE_FEE, 0) + NVL(l_rec.CONVENIENCE_FEE, 0),
                           LOYALTY_DISCOUNT = NVL(LOYALTY_DISCOUNT, 0) + NVL(l_rec.loyalty_discount, 0)
                     WHERE TERMINAL_ID = l_rec.TERMINAL_ID
                       AND EPORT_ID = l_rec.EPORT_ID
                       AND TRAN_DATE = trunc(l_rec.CLOSE_DATE, 'DD')
                       AND TRANS_TYPE_ID = l_rec.TRANS_TYPE_ID
                       AND CURRENCY_ID = l_rec.CURRENCY_ID;
                    EXIT WHEN SQL%ROWCOUNT > 0;
                    BEGIN
                        INSERT INTO REPORT.TRANS_STAT_BY_DAY(
                            TERMINAL_ID,
                            EPORT_ID,
                            TRAN_DATE, 
                            TRANS_TYPE_ID,
                            TRAN_COUNT,
                            VEND_COUNT, 
                            TRAN_AMOUNT,
                            CURRENCY_ID, 
                            CONVENIENCE_FEE,
                            LOYALTY_DISCOUNT) 
                        VALUES( 
                            l_rec.TERMINAL_ID,
                            l_rec.EPORT_ID,
                            trunc(l_rec.CLOSE_DATE, 'DD'),
                            l_rec.TRANS_TYPE_ID, 
                            1,
                            l_qty,
                            NVL(l_rec.TOTAL_AMOUNT, 0),
                            l_rec.CURRENCY_ID,
                            l_rec.CONVENIENCE_FEE,
                            l_rec.loyalty_discount); 
                        EXIT;    
                    EXCEPTION
                        WHEN DUP_VAL_ON_INDEX THEN
                            NULL;
                    END;
                END LOOP;  
            END IF;
            COMMIT;
            
            -- update corp.batch_total
            IF l_qty != 0  OR l_rec.TOTAL_AMOUNT != 0 THEN
               IF l_rec.TRANS_TYPE_ID = 22 THEN            
                LOOP
                    UPDATE CORP.BATCH_TOTAL
                    SET LEDGER_AMOUNT = LEDGER_AMOUNT + l_rec.TOTAL_AMOUNT,
                      LEDGER_COUNT = LEDGER_COUNT + 1
                    WHERE BATCH_ID = l_payment_batch_id
                      AND ENTRY_TYPE = 'CA' ;
                    EXIT WHEN SQL%ROWCOUNT > 0;
                    BEGIN
                        INSERT INTO CORP.BATCH_TOTAL(BATCH_ID, ENTRY_TYPE, PAYABLE, LEDGER_AMOUNT, LEDGER_COUNT)
                        VALUES(l_payment_batch_id,'CA','N', l_rec.TOTAL_AMOUNT, 1); 
                        EXIT;    
                    EXCEPTION
                        WHEN DUP_VAL_ON_INDEX THEN
                            NULL;
                    END;
                END LOOP; 
                END IF;
                IF l_rec.TRANS_TYPE_ID = 16 AND l_rec.convenience_fee>0 THEN            
                LOOP
                    UPDATE CORP.BATCH_TOTAL
                    SET LEDGER_AMOUNT = LEDGER_AMOUNT + l_rec.convenience_fee,
                      LEDGER_COUNT = LEDGER_COUNT + 1
                    WHERE BATCH_ID = l_payment_batch_id
                      AND ENTRY_TYPE ='TT' ;
                    EXIT WHEN SQL%ROWCOUNT > 0;
                    BEGIN
                        INSERT INTO CORP.BATCH_TOTAL(BATCH_ID, ENTRY_TYPE, PAYABLE, LEDGER_AMOUNT, LEDGER_COUNT)
                        VALUES(l_payment_batch_id,'TT','N', l_rec.convenience_fee, 1); 
                        EXIT;    
                    EXCEPTION
                        WHEN DUP_VAL_ON_INDEX THEN
                            NULL;
                    END;
                END LOOP; 
                END IF;
            END IF;
            COMMIT;         
            IF l_qty != 0  OR l_rec.TOTAL_AMOUNT != 0 THEN
                --logic to update report.eport column for condition report
                IF l_rec.CREDIT_IND = 'Y' THEN
                  -- credit
                  UPDATE REPORT.EPORT SET
                   FIRST_TRAN_DATE=LEAST(NVL(FIRST_TRAN_DATE, max_date), l_rec.CLOSE_DATE),
                   FIRST_CREDIT_TRAN_DATE=LEAST(NVL(FIRST_CREDIT_TRAN_DATE, max_date), l_rec.CLOSE_DATE),
                   LAST_TRAN_DATE=GREATEST(NVL(LAST_TRAN_DATE, min_date), l_rec.CLOSE_DATE),
                   LAST_CREDIT_TRAN_DATE=GREATEST(NVL(LAST_CREDIT_TRAN_DATE, min_date), l_rec.CLOSE_DATE)
                  WHERE EPORT_ID=l_rec.EPORT_ID;
                ELSIF l_rec.CASH_IND = 'Y' THEN
                  -- cash
                  UPDATE REPORT.EPORT SET
                   FIRST_TRAN_DATE=LEAST(NVL(FIRST_TRAN_DATE, max_date), l_rec.CLOSE_DATE),
                   FIRST_CASH_TRAN_DATE=LEAST(NVL(FIRST_CASH_TRAN_DATE, max_date), l_rec.CLOSE_DATE),
                   LAST_TRAN_DATE=GREATEST(NVL(LAST_TRAN_DATE, min_date), l_rec.CLOSE_DATE),
                   LAST_CASH_TRAN_DATE=GREATEST(NVL(LAST_CASH_TRAN_DATE, min_date), l_rec.CLOSE_DATE)
                  WHERE EPORT_ID=l_rec.EPORT_ID;
                ELSE
                  UPDATE REPORT.EPORT SET
                   FIRST_TRAN_DATE=LEAST(NVL(FIRST_TRAN_DATE, max_date), l_rec.CLOSE_DATE),
                   LAST_TRAN_DATE=GREATEST(NVL(LAST_TRAN_DATE, min_date), l_rec.CLOSE_DATE)
                  WHERE EPORT_ID=l_rec.EPORT_ID;
                END IF;                 
            END IF;
        END LOOP;
    END;
 
    
    PROCEDURE UPDATE_TERMINAL(
        l_terminal_id TERMINAL.TERMINAL_ID%TYPE)
    IS
    BEGIN
        UPDATE ACTIVITY_REF A SET (
            region_id,
            region_name,
            location_name,
            location_id,
            terminal_name,
            terminal_nbr) = (
        SELECT
            R.REGION_ID,
            R.REGION_NAME,
            NVL(L.LOCATION_NAME, 'Unknown'),
            NVL(L.LOCATION_ID, 0),
            TERMINAL_NAME,
            TERM.TERMINAL_NBR
          FROM TERMINAL TERM,  REGION R, TERMINAL_REGION TR, LOCATION L
          WHERE TR.REGION_ID = R.REGION_ID (+)
            AND TERM.TERMINAL_ID = TR.TERMINAL_ID (+)
            AND TERM.LOCATION_ID = L.LOCATION_ID (+)
            AND TERM.TERMINAL_ID = A.TERMINAL_ID)
        WHERE A.TERMINAL_ID = l_terminal_id
          AND A.TRAN_DATE > SYSDATE - 90;
    END;

    PROCEDURE UPDATE_FILL_DATE(
        l_fill_id FILL.FILL_ID%TYPE)
    IS
        l_fill_date FILL.FILL_DATE%TYPE;
        l_prev_fill_date FILL.FILL_DATE%TYPE;
        l_eport_id FILL.EPORT_ID%TYPE;
    BEGIN
        SELECT FILL_DATE, EPORT_ID
          INTO l_fill_date, l_eport_id
          FROM FILL
         WHERE FILL_ID = l_fill_id;
         
         -- add update last fill date for condition report
         UPDATE REPORT.EPORT
         SET LAST_FILL_DATE=l_fill_date
         WHERE EPORT_ID=l_eport_id
         AND NVL(LAST_FILL_DATE, min_date) < l_fill_date;

          SELECT MAX(FILL_DATE)
          INTO l_prev_fill_date
          FROM FILL
         WHERE FILL_DATE < l_fill_date
           AND EPORT_ID = l_eport_id;
           
        -- Added to capture first fills (03-05-2007 BSK)
        IF l_prev_fill_date IS NULL THEN
            SELECT MAX(START_DATE)
              INTO l_prev_fill_date
              FROM REPORT.TERMINAL_EPORT
             WHERE START_DATE <= l_fill_date
               AND EPORT_ID = l_eport_id;
        END IF;
        
        -- Ignore first fill period (added June 8th for performance reasons - BSK)
        IF l_prev_fill_date IS NOT NULL THEN
            UPDATE ACTIVITY_REF SET FILL_DATE = GET_FILL_DATE(TRAN_ID)
             WHERE EPORT_ID = l_eport_id AND TRAN_DATE BETWEEN l_prev_fill_date AND l_fill_date;
          END IF;
    END;
    
    PROCEDURE UPDATE_FILL_INFO(
            l_fill_id   FILL.FILL_ID%TYPE) 
    IS
        l_export_batch_id FILL.BATCH_ID%TYPE;
        l_customer_id TERMINAL.CUSTOMER_ID%TYPE;
        l_terminal_id FILL.TERMINAL_ID%TYPE;
    
    BEGIN
        SELECT T.CUSTOMER_ID, T.TERMINAL_ID INTO l_customer_id, l_terminal_id
        FROM REPORT.TERMINAL T 
        JOIN REPORT.TERMINAL_EPORT TE ON T.TERMINAL_ID=TE.TERMINAL_ID
        JOIN REPORT.FILL F ON F.EPORT_ID=TE.EPORT_ID
        WHERE F.FILL_ID=l_fill_id
        AND F.FILL_DATE >= NVL(TE.START_DATE, MIN_DATE) AND F.FILL_DATE < NVL(TE.END_DATE, MAX_DATE);
        
        l_export_batch_id := GET_OR_CREATE_EXPORT_BATCH(l_customer_id);
        UPDATE REPORT.FILL SET BATCH_ID = l_export_batch_id, TERMINAL_ID = l_terminal_id,
        REF_NBR = NVL(REF_NBR, REF_NBR_SEQ.nextval)
        WHERE FILL_ID = l_fill_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN;  
    END;
    
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/VEND_COLUMN_STRING.fnc?rev=1.15
CREATE OR REPLACE FUNCTION REPORT.VEND_COLUMN_STRING(ATRAN_ID INT)
RETURN VARCHAR2 
IS
BEGIN
  DECLARE
    ln_device_type_id EPORT.DEVICE_TYPE_ID%TYPE;
    lc_currency_symbol CORP.CURRENCY.CURRENCY_SYMBOL%TYPE;
    lc_no_price CHAR(1);
    str ACTIVITY_REF.VEND_COLUMN%TYPE := '';
    tmp VARCHAR2(4000);
    
    CURSOR l_cur IS
         SELECT NVL(MAPPED_COLUMN, NVL(DESCRIPTION, NVL(VEND_COLUMN, '#' || MDB_NUMBER))) LABEL, AMOUNT QTY, DECODE(lc_no_price, 'N', PRICE) PRICE
         FROM REPORT.PURCHASE P
         LEFT OUTER JOIN PSS.TRAN_LINE_ITEM_TYPE IT ON P.TRAN_LINE_ITEM_TYPE_ID = IT.TRAN_LINE_ITEM_TYPE_ID
         WHERE TRAN_ID = atran_id
           AND (IT.TRAN_LINE_ITEM_TYPE_GROUP_CD IS NULL OR IT.TRAN_LINE_ITEM_TYPE_GROUP_CD NOT IN('U'))
         ORDER BY PURCHASE_ID;
  BEGIN
    SELECT E.DEVICE_TYPE_ID, NVL(C.CURRENCY_SYMBOL, '$'), CASE WHEN E.DEVICE_TYPE_ID = 0 OR (E.DEVICE_TYPE_ID = 1 AND SUM(P.AMOUNT * P.PRICE) != T.TOTAL_AMOUNT) THEN 'Y' ELSE 'N' END
    INTO ln_device_type_id, lc_currency_symbol, lc_no_price
    FROM REPORT.TRANS T
    JOIN REPORT.EPORT E ON T.EPORT_ID = E.EPORT_ID
    LEFT OUTER JOIN CORP.CURRENCY C ON T.CURRENCY_ID = C.CURRENCY_ID
    LEFT OUTER JOIN REPORT.PURCHASE P ON E.DEVICE_TYPE_ID = 1 AND P.TRAN_ID = T.TRAN_ID
    WHERE T.TRAN_ID = atran_id
    GROUP BY E.DEVICE_TYPE_ID, NVL(C.CURRENCY_SYMBOL, '$'), T.TOTAL_AMOUNT;
  
    FOR l_rec IN l_cur LOOP
      IF LENGTH(str) > 0 THEN
        str := str || ', ';
      END IF;
      IF l_rec.QTY != 1 OR l_rec.PRICE IS NOT NULL THEN 
          tmp := l_rec.LABEL || '(';
          IF l_rec.QTY != 1 THEN
            tmp := tmp || TO_CHAR(l_rec.QTY);
            IF l_rec.PRICE IS NOT NULL THEN
                 tmp := tmp || ' * ';
            END IF;
          END IF;
          IF l_rec.PRICE IS NOT NULL THEN
            IF l_rec.PRICE < 0 THEN
                tmp := tmp || '-';
            END IF;
            tmp := tmp || lc_currency_symbol || TRIM(TO_CHAR(ABS(l_rec.PRICE), '9,999,999,999,990.00'));
          END IF;
          tmp := tmp || ')';
      ELSE
        tmp := l_rec.LABEL;
      END IF;         
      IF LENGTH(str) + LENGTH(tmp) > 3995 THEN
        str := str || '...';
        EXIT;
      END IF;
      str := str || tmp;
    END LOOP;
    RETURN str;
  END;
END;
/
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/DEVICE/PKG_DEVICE_CONFIGURATION.pbk?rev=1.145
CREATE OR REPLACE PACKAGE BODY DEVICE.PKG_DEVICE_CONFIGURATION IS
    
PROCEDURE SP_GET_FILE_TRANSFER_BLOB
(
    pn_file_transfer_id file_transfer.file_transfer_id%TYPE,
    pbl_file_transfer_content OUT file_transfer.file_transfer_content%TYPE
)
IS
BEGIN
    SELECT file_transfer_content
	INTO pbl_file_transfer_content
    FROM device.file_transfer
    WHERE file_transfer_id = pn_file_transfer_id;
END;

PROCEDURE SP_GET_MAP_CONFIG_FILE
(
    pn_device_id device.device_id%TYPE,
    pv_file_content_hex OUT VARCHAR2
)
IS
	ln_device_id DEVICE.DEVICE_ID%TYPE;
BEGIN
	SELECT MAX(dla.device_id)
	INTO ln_device_id
	FROM device.device d
	JOIN device.device_last_active dla ON d.device_name = dla.device_name
	WHERE d.device_id = pn_device_id;
		
    pv_file_content_hex := '';
	FOR rec IN (
		SELECT DS.DEVICE_SETTING_VALUE
		FROM DEVICE.DEVICE_SETTING DS
		JOIN DEVICE.DEVICE D ON DS.DEVICE_ID = D.DEVICE_ID
		JOIN DEVICE.CONFIG_TEMPLATE_SETTING CTS ON DECODE(D.DEVICE_TYPE_ID, PKG_CONST.DEVICE_TYPE__GX, PKG_CONST.DEVICE_TYPE__G4, D.DEVICE_TYPE_ID) = CTS.DEVICE_TYPE_ID
			AND DS.DEVICE_SETTING_PARAMETER_CD = CTS.DEVICE_SETTING_PARAMETER_CD
			AND CTS.FIELD_OFFSET BETWEEN 0 AND 511
		WHERE DS.DEVICE_ID = ln_device_id
		ORDER BY CTS.FIELD_OFFSET
	) 
	LOOP
		pv_file_content_hex := pv_file_content_hex || rec.DEVICE_SETTING_VALUE;
	END LOOP;
END;

PROCEDURE SP_UPSERT_KIOSK_CONFIG_UPDATE
(
	pv_device_name DEVICE.DEVICE_NAME%TYPE,
	pn_device_id DEVICE.DEVICE_ID%TYPE,
	pv_kiosk_config_name ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE
)
IS
	ln_file_transfer_id FILE_TRANSFER.FILE_TRANSFER_ID%TYPE;
	lb_file_transfer_content FILE_TRANSFER.FILE_TRANSFER_CONTENT%TYPE;
	ln_device_file_transfer_id DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_ID%TYPE;
	ln_required_fields NUMBER;
	ln_pending_file_transfer NUMBER;
	lv_line VARCHAR2(300);
	ln_device_id DEVICE.DEVICE_ID%TYPE;
BEGIN
	SELECT MAX(dla.device_id)
	INTO ln_device_id
	FROM device.device d
	JOIN device.device_last_active dla ON d.device_name = dla.device_name
	WHERE d.device_id = pn_device_id;
	
	SELECT COUNT(1) INTO ln_required_fields 
	FROM DEVICE.DEVICE_SETTING
	WHERE DEVICE_ID = ln_device_id
	AND DEVICE_SETTING_PARAMETER_CD IN ('SSN', 'VMC', 'EncKey')
	AND DECODE(DEVICE_SETTING_VALUE, NULL, 0, LENGTH(DEVICE_SETTING_VALUE)) > 6;

	IF ln_required_fields = 3 THEN
		BEGIN
			SELECT FILE_TRANSFER_ID
			INTO ln_file_transfer_id
			FROM (SELECT /*+index(FT IDX_FILE_TRANSFER_NAME)*/ FILE_TRANSFER_ID 
			FROM DEVICE.FILE_TRANSFER FT
			WHERE FILE_TRANSFER_NAME = pv_device_name || '-CFG'
				AND FILE_TRANSFER_TYPE_CD = PKG_CONST.FILE_TYPE__CONFIG
			ORDER BY CREATED_TS)
			WHERE ROWNUM = 1;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				SELECT seq_file_transfer_id.nextval INTO ln_file_transfer_id FROM DUAL;
				INSERT INTO DEVICE.FILE_TRANSFER(FILE_TRANSFER_ID, FILE_TRANSFER_NAME, FILE_TRANSFER_TYPE_CD)
				VALUES(ln_file_transfer_id, pv_device_name || '-CFG', PKG_CONST.FILE_TYPE__CONFIG);
		END;
		
		UPDATE DEVICE.FILE_TRANSFER
		SET FILE_TRANSFER_CONTENT = EMPTY_BLOB()
		WHERE FILE_TRANSFER_ID = ln_file_transfer_id;
		
		SELECT FILE_TRANSFER_CONTENT
		INTO lb_file_transfer_content
		FROM DEVICE.FILE_TRANSFER
		WHERE FILE_TRANSFER_ID = ln_file_transfer_id
		FOR UPDATE;
		
		FOR rec IN (
			SELECT DS.DEVICE_SETTING_PARAMETER_CD, DS.DEVICE_SETTING_VALUE
			FROM DEVICE.DEVICE_SETTING DS
			JOIN DEVICE.DEVICE_SETTING_PARAMETER DSP ON DS.DEVICE_SETTING_PARAMETER_CD = DSP.DEVICE_SETTING_PARAMETER_CD
			LEFT OUTER JOIN DEVICE.SETTING_PARAMETER_TYPE SPT ON DSP.SETTING_PARAMETER_TYPE_ID = SPT.SETTING_PARAMETER_TYPE_ID
			WHERE DS.DEVICE_ID = ln_device_id AND (SPT.SETTING_PARAMETER_TYPE_DESC IS NULL OR SPT.SETTING_PARAMETER_TYPE_DESC != 'SERVER_ONLY_DEVICE_SETTING')
			ORDER BY DS.FILE_ORDER
		) 
		LOOP
			lv_line := rec.device_setting_parameter_cd || '=' || rec.device_setting_value || PKG_CONST.ASCII__LF;
			dbms_lob.writeappend(lb_file_transfer_content, LENGTH(lv_line), UTL_RAW.CAST_TO_RAW(lv_line));
		END LOOP;
	
		UPDATE ENGINE.MACHINE_CMD_PENDING
		SET EXECUTE_CD = 'A'
		WHERE MACHINE_ID = pv_device_name
		AND DATA_TYPE = '9B'
		AND COMMAND = UPPER(pv_kiosk_config_name);
		
		SELECT COUNT(1) 
		INTO ln_pending_file_transfer
		FROM DEVICE.DEVICE_FILE_TRANSFER DFT, ENGINE.MACHINE_CMD_PENDING MCP 
		WHERE DFT.DEVICE_ID = ln_device_id
		AND DFT.FILE_TRANSFER_ID = ln_file_transfer_id
		AND DFT.DEVICE_FILE_TRANSFER_STATUS_CD = 0 
		AND DFT.DEVICE_FILE_TRANSFER_DIRECT = 'O' 
		AND MCP.MACHINE_ID = pv_device_name
		AND MCP.DATA_TYPE = 'A4'
		AND DFT.DEVICE_FILE_TRANSFER_ID = MCP.DEVICE_FILE_TRANSFER_ID;
			
		IF ln_pending_file_transfer = 0 THEN
			SELECT SEQ_DEVICE_FILE_TRANSFER_ID.NEXTVAL INTO ln_device_file_transfer_id FROM DUAL;
			INSERT INTO DEVICE.DEVICE_FILE_TRANSFER(
				DEVICE_FILE_TRANSFER_ID, 
				DEVICE_ID, 
				FILE_TRANSFER_ID, 
				DEVICE_FILE_TRANSFER_DIRECT, 
				DEVICE_FILE_TRANSFER_STATUS_CD, 
				DEVICE_FILE_TRANSFER_PKT_SIZE 
			) 
			VALUES(
				ln_device_file_transfer_id,
				ln_device_id, 
				ln_file_transfer_id, 
				'O',
				0,
				1024);
				
			INSERT INTO ENGINE.MACHINE_CMD_PENDING(
				MACHINE_ID, 
				DATA_TYPE, 
				DEVICE_FILE_TRANSFER_ID, 
				EXECUTE_CD, 
				EXECUTE_ORDER
			) 
			VALUES(
				pv_device_name,
				'A4',
				ln_device_file_transfer_id,
				'P',
				1
			);
		END IF;
	END IF;
END;

-- IS_DEVICE_SETTING_IN_CONFIG is deprecated in R30
FUNCTION IS_DEVICE_SETTING_IN_CONFIG(
	pn_device_id IN device.device_id%TYPE,
	pv_device_setting_parameter_cd IN device_setting.device_setting_parameter_cd%TYPE,
	pv_device_setting_value IN device_setting.device_setting_value%TYPE
)
RETURN VARCHAR
IS
BEGIN
    RETURN 'Y';
END;

	FUNCTION GET_DEVICE_SETTING(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pv_device_setting_parameter_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE
	) RETURN DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE
	IS
		lv_device_setting_value DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
	BEGIN
		SELECT DS.DEVICE_SETTING_VALUE
		INTO lv_device_setting_value
		FROM DEVICE.DEVICE D
		JOIN DEVICE.DEVICE_LAST_ACTIVE DLA ON D.DEVICE_NAME = DLA.DEVICE_NAME
		JOIN DEVICE.DEVICE_SETTING DS ON DLA.DEVICE_ID = DS.DEVICE_ID
		WHERE D.DEVICE_ID = pn_device_id
			AND DS.DEVICE_SETTING_PARAMETER_CD = pv_device_setting_parameter_cd;
		
		RETURN lv_device_setting_value;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			RETURN NULL;
	END;

PROCEDURE SP_UPDATE_DEVICE_SETTING(
	pn_device_id IN device.device_id%TYPE,
	pv_device_setting_parameter_cd IN device_setting.device_setting_parameter_cd%TYPE,
	pv_device_setting_value IN device_setting.device_setting_value%TYPE,
    pc_only_greater_flag IN CHAR DEFAULT 'N'
)
IS
	ln_exists NUMBER;
BEGIN
	SP_UPSERT_DEVICE_SETTING(pn_device_id, pv_device_setting_parameter_cd, pv_device_setting_value, ln_exists);
END;

	PROCEDURE SP_UPSERT_DEVICE_SETTING(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pv_device_setting_parameter_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_device_setting_value DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pn_file_order DEVICE_SETTING.FILE_ORDER%TYPE,
		pn_exists OUT NUMBER,
        pc_only_greater_flag IN CHAR DEFAULT 'N'
	)
	IS
		ln_count NUMBER;
		ln_device_id DEVICE.DEVICE_ID%TYPE;
	BEGIN
		SELECT MAX(dla.device_id)
		INTO ln_device_id
		FROM device.device d
		JOIN device.device_last_active dla ON d.device_name = dla.device_name
		WHERE d.device_id = pn_device_id;
	
		SELECT COUNT(1) INTO ln_count
		FROM device.device_setting_parameter
		WHERE device_setting_parameter_cd = pv_device_setting_parameter_cd;
			
		IF ln_count = 0 THEN
			BEGIN
				INSERT INTO device.device_setting_parameter(device_setting_parameter_cd)
				VALUES(pv_device_setting_parameter_cd);
			EXCEPTION
	        	WHEN DUP_VAL_ON_INDEX THEN
	        		NULL;
	        END;	
		END IF;
		
		UPDATE device.device_setting
		   SET device_setting_value = 
                CASE 
                    WHEN pc_only_greater_flag != 'Y' OR TO_NUMBER_OR_NULL(pv_device_setting_value) > TO_NUMBER_OR_NULL(NVL(device_setting_value, '0')) THEN pv_device_setting_value
                    ELSE device_setting_value 
                END
		 WHERE device_id = ln_device_id
           AND device_setting_parameter_cd = pv_device_setting_parameter_cd;
		
		IF SQL%NOTFOUND THEN
			pn_exists := 0;
			BEGIN
				INSERT INTO device.device_setting(device_id, device_setting_parameter_cd, device_setting_value, file_order)
				VALUES(ln_device_id, pv_device_setting_parameter_cd, pv_device_setting_value, pn_file_order);
			EXCEPTION
	        	WHEN DUP_VAL_ON_INDEX THEN
	        		SP_UPSERT_DEVICE_SETTING(ln_device_id, pv_device_setting_parameter_cd, pv_device_setting_value, pn_file_order, pn_exists, pc_only_greater_flag);
	        END;
		ELSE
			pn_exists := 1;
		END IF;
    END;
	
PROCEDURE SP_UPSERT_DEVICE_SETTING(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pv_device_setting_parameter_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_device_setting_value DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,		
		pn_exists OUT NUMBER
	)
	IS
	BEGIN
		SP_UPSERT_DEVICE_SETTING(pn_device_id, pv_device_setting_parameter_cd, pv_device_setting_value, NULL, pn_exists);
	END;

PROCEDURE SP_UPSERT_CFG_TEMPLATE_SETTING(
	pn_config_template_id CONFIG_TEMPLATE_SETTING.CONFIG_TEMPLATE_ID%TYPE,
	pv_device_setting_parameter_cd CONFIG_TEMPLATE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
	pv_config_template_setting_val CONFIG_TEMPLATE_SETTING.CONFIG_TEMPLATE_SETTING_VALUE%TYPE,
	pn_exists OUT NUMBER
) 
IS
	ln_count NUMBER;
BEGIN
	SELECT COUNT(1) INTO ln_count
	FROM device.device_setting_parameter
	WHERE device_setting_parameter_cd = pv_device_setting_parameter_cd;
		
	IF ln_count = 0 THEN
		BEGIN
			INSERT INTO device.device_setting_parameter(device_setting_parameter_cd)
			VALUES(pv_device_setting_parameter_cd);
		EXCEPTION
			WHEN DUP_VAL_ON_INDEX THEN
				NULL;
		END;	
	END IF;
	
	UPDATE device.config_template_setting
	SET config_template_setting_value = pv_config_template_setting_val
	WHERE config_template_id = pn_config_template_id
		AND device_setting_parameter_cd = pv_device_setting_parameter_cd;
	
	IF SQL%NOTFOUND THEN
		pn_exists := 0;
		BEGIN
			INSERT INTO device.config_template_setting(config_template_id, device_setting_parameter_cd, config_template_setting_value)
			VALUES(pn_config_template_id, pv_device_setting_parameter_cd, pv_config_template_setting_val);
		EXCEPTION
			WHEN DUP_VAL_ON_INDEX THEN
				SP_UPSERT_CFG_TEMPLATE_SETTING(pn_config_template_id, pv_device_setting_parameter_cd, pv_config_template_setting_val, pn_exists);
		END;
	ELSE
		pn_exists := 1;
	END IF;
END;
	
PROCEDURE SP_UPDATE_DEVICE_SETTINGS
(
    pn_device_id IN device.device_id%TYPE,
    pn_file_transfer_id IN file_transfer.file_transfer_id%TYPE,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_setting_count OUT NUMBER
)
IS
    ll_file_transfer_content file_transfer.file_transfer_content%TYPE;
    ln_pos NUMBER := 1;
    ln_content_size NUMBER := 2000;
    ln_content_pos NUMBER := 1;
    ls_content VARCHAR2(4000);
    ls_record VARCHAR2(4000);
    ls_param device_setting.device_setting_parameter_cd%TYPE;
    ls_value VARCHAR2(4000);
    ln_return NUMBER := 0;
    ln_file_transfer_type_cd file_transfer.file_transfer_type_cd%TYPE;
	lv_file_transfer_name file_transfer.file_transfer_name%TYPE;
	ln_file_order device_setting.file_order%TYPE := 0;
	ln_config_template_id CONFIG_TEMPLATE.CONFIG_TEMPLATE_ID%TYPE;
	ln_device_id DEVICE.DEVICE_ID%TYPE;
BEGIN
    pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;
	
	SELECT MAX(dla.device_id)
	INTO ln_device_id
	FROM device.device d
	JOIN device.device_last_active dla ON d.device_name = dla.device_name
	WHERE d.device_id = pn_device_id;

    SELECT file_transfer_type_cd, file_transfer_content, file_transfer_name
    INTO ln_file_transfer_type_cd, ll_file_transfer_content, lv_file_transfer_name
    FROM device.file_transfer
    WHERE file_transfer_id = pn_file_transfer_id;

    IF ln_file_transfer_type_cd = PKG_CONST.FILE_TYPE__CONFIG THEN
        DELETE FROM device.device_setting
        WHERE device_id = ln_device_id
            AND device_setting_parameter_cd IN (
                SELECT device_setting_parameter_cd
                FROM device.device_setting_parameter
                WHERE device_setting_ui_configurable = 'Y');
    ELSE
		SELECT config_template_id
		INTO ln_config_template_id
		FROM device.config_template
		WHERE config_template_name = lv_file_transfer_name;
	
        DELETE FROM device.config_template_setting
        WHERE config_template_id = ln_config_template_id;
    END IF;

    LOOP
        ls_content := ls_content || utl_raw.cast_to_varchar2(HEXTORAW(DBMS_LOB.SUBSTR(ll_file_transfer_content, ln_content_size, ln_content_pos)));
        ln_content_pos := ln_content_pos + ln_content_size;

        ln_pos := NVL(INSTR(ls_content, PKG_CONST.ASCII__LF), 0);
        IF ln_pos = 0 OR NVL(INSTR(ls_content, '='), 0) = 0 THEN
            ln_return := 1;
        END IF;

        LOOP
            ln_pos := NVL(INSTR(ls_content, PKG_CONST.ASCII__LF), 0);
            IF ln_pos = 0 AND ln_return = 0 THEN
                EXIT;
            END IF;

            IF ln_return = 0 THEN
                ls_record := SUBSTR(ls_content, 1, ln_pos - 1);
                ls_content := SUBSTR(ls_content, ln_pos + 1);
            ELSE
                ls_record := ls_content;
            END IF;

            ln_pos := NVL(INSTR(ls_record, '='), 0);
            IF ln_pos = 0 AND ln_return = 0 THEN
                EXIT;
            END IF;

            ls_param := TRIM(SUBSTR(ls_record, 1, ln_pos - 1));
            ls_value := REPLACE(TRIM(SUBSTR(ls_record, ln_pos + 1, LENGTH(ls_record) - ln_pos)), CHR(13), '');
			IF LENGTH(ls_value) > 200 THEN
				ls_value := NULL;
			END IF;

            IF NVL(LENGTH(ls_param), 0) > 0 THEN
				BEGIN
					INSERT INTO device.device_setting_parameter(device_setting_parameter_cd)
					SELECT ls_param FROM dual
					WHERE NOT EXISTS (
						SELECT 1 FROM device.device_setting_parameter
						WHERE device_setting_parameter_cd = ls_param);
				EXCEPTION
					WHEN DUP_VAL_ON_INDEX THEN
						NULL;
				END;

				ln_file_order := ln_file_order + 1;
				
				BEGIN
					IF ln_file_transfer_type_cd = PKG_CONST.FILE_TYPE__CONFIG THEN
						INSERT INTO device.device_setting(device_id, device_setting_parameter_cd, device_setting_value, file_order)
						VALUES(ln_device_id, ls_param, ls_value, ln_file_order);
					ELSE
						INSERT INTO device.config_template_setting(config_template_id, device_setting_parameter_cd, config_template_setting_value, file_order)
						VALUES(ln_config_template_id, ls_param, ls_value, ln_file_order);
					END IF;
				EXCEPTION
					WHEN DUP_VAL_ON_INDEX THEN
						NULL;
				END;					
            END IF;

            IF ln_return = 1 THEN
                IF ln_file_transfer_type_cd = PKG_CONST.FILE_TYPE__CONFIG THEN
                    SELECT COUNT(1)
                    INTO pn_setting_count
                    FROM device.device_setting
                    WHERE device_id = ln_device_id
                        AND device_setting_parameter_cd IN (
                            SELECT device_setting_parameter_cd
                            FROM device.device_setting_parameter
                            WHERE device_setting_ui_configurable = 'Y');
                ELSE
                    SELECT COUNT(1)
                    INTO pn_setting_count
                    FROM device.config_template_setting
                    WHERE config_template_id = ln_config_template_id
                        AND device_setting_parameter_cd IN (
                            SELECT device_setting_parameter_cd
                            FROM device.device_setting_parameter
                            WHERE device_setting_ui_configurable = 'Y');
                END IF;

                pn_result_cd := PKG_CONST.RESULT__SUCCESS;
                pv_error_message := PKG_CONST.ERROR__NO_ERROR;
                RETURN;
            END IF;
        END LOOP;

    END LOOP;
END;

PROCEDURE SP_UPDATE_CFG_TMPL_SETTINGS
(
    pn_file_transfer_id IN file_transfer.file_transfer_id%TYPE,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_setting_count OUT NUMBER
)
IS
BEGIN
    SP_UPDATE_DEVICE_SETTINGS(0, pn_file_transfer_id, pn_result_cd, pv_error_message, pn_setting_count);
END;

PROCEDURE SP_GET_HOST_BY_PORT_NUM
(
    pv_device_name IN device.device_name%TYPE,
    pn_host_port_num IN host.host_port_num%TYPE,
    pt_utc_ts IN TIMESTAMP,
    pn_host_id OUT host.host_id%TYPE
)
IS
BEGIN
    SELECT host_id INTO pn_host_id FROM (
        SELECT host_id
        FROM device.host h
        INNER JOIN device.device d ON h.device_id = d.device_id
        WHERE d.device_name = pv_device_name
            AND h.host_port_num = pn_host_port_num
            AND SYS_EXTRACT_UTC(CAST(h.created_ts AS TIMESTAMP)) <= pt_utc_ts
        ORDER BY h.created_ts DESC
    ) WHERE ROWNUM = 1;
END;

PROCEDURE SP_GET_HOST
(
    pn_device_id IN device.device_id%TYPE,
    pv_device_name IN device.device_name%TYPE,
    pn_host_port_num IN host.host_port_num%TYPE,
    pt_utc_ts IN TIMESTAMP,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_host_id OUT host.host_id%TYPE
)
IS
    ln_new_host_count NUMBER;
	ln_device_id DEVICE.DEVICE_ID%TYPE;
BEGIN
	pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;

	SELECT MAX(dla.device_id)
	INTO ln_device_id
	FROM device.device d
	JOIN device.device_last_active dla ON d.device_name = dla.device_name
	WHERE d.device_id = pn_device_id;

    BEGIN
        sp_get_host_by_port_num(pv_device_name, pn_host_port_num, pt_utc_ts, pn_host_id);
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            BEGIN
                -- fail over to the base host
                sp_get_host_by_port_num(pv_device_name, 0, pt_utc_ts, pn_host_id);
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    BEGIN
                        sp_create_default_hosts(ln_device_id, ln_new_host_count, pn_result_cd, pv_error_message);
                        IF pn_result_cd != PKG_CONST.RESULT__SUCCESS THEN
                            RETURN;
                        END IF;

                        sp_get_host_by_port_num(pv_device_name, 0, SYS_EXTRACT_UTC(SYSTIMESTAMP), pn_host_id);
                    EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                            pn_result_cd := PKG_CONST.RESULT__HOST_NOT_FOUND;
                            pv_error_message := 'Unable to find host for device_name: ' || pv_device_name || ', host_port_num: ' || pn_host_port_num;
                            RETURN;
                    END;
            END;
    END;

    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;
            
PROCEDURE SP_CREATE_DEFAULT_HOSTS
(
    pn_device_id                            IN  device.device_id%TYPE,
    pn_new_host_count                       OUT NUMBER,
    pn_result_cd                            OUT NUMBER,
    pv_error_message                        OUT VARCHAR2
)
IS
    ln_base_host_count                      NUMBER;
    ln_other_host_count                     NUMBER;
    lv_device_serial_cd                     device.device_serial_cd%TYPE;
    ln_device_type_id                       device.device_type_id%TYPE;
    ln_def_base_host_type_id                device_type.def_base_host_type_id%TYPE;
	ln_def_base_host_equipment_id           device_type.def_base_host_equipment_id%TYPE;
	ln_def_prim_host_type_id                device_type.def_prim_host_type_id%TYPE;
	ln_def_prim_host_equipment_id           device_type.def_prim_host_equipment_id%TYPE;
	ln_device_id DEVICE.DEVICE_ID%TYPE;
BEGIN
    pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;
    pn_new_host_count := 0;
	
	SELECT MAX(dla.device_id)
	INTO ln_device_id
	FROM device.device d
	JOIN device.device_last_active dla ON d.device_name = dla.device_name
	WHERE d.device_id = pn_device_id;	

    SELECT NVL(SUM(CASE WHEN HTGT.HOST_TYPE_ID IS NOT NULL OR H.HOST_PORT_NUM = 0 THEN 1 ELSE 0 END), 0), NVL(SUM(CASE WHEN HTGT.HOST_TYPE_ID IS NOT NULL OR H.HOST_PORT_NUM = 0 THEN 0 ELSE 1 END), 0)
      INTO ln_base_host_count, ln_other_host_count
      FROM DEVICE.HOST H
      LEFT OUTER JOIN (DEVICE.HOST_TYPE_HOST_GROUP_TYPE HTGT   
      JOIN DEVICE.HOST_GROUP_TYPE GT ON HTGT.HOST_GROUP_TYPE_ID = GT.HOST_GROUP_TYPE_ID) 
        ON H.HOST_TYPE_ID = HTGT.HOST_TYPE_ID
       AND GT.HOST_GROUP_TYPE_CD = 'BASE_HOST'
     WHERE H.DEVICE_ID = H.DEVICE_ID
       AND H.DEVICE_ID = ln_device_id;

    -- if exists both a base and another host, then there's nothing to do here
    IF ln_base_host_count > 0 AND ln_other_host_count > 0 THEN
        pn_result_cd := PKG_CONST.RESULT__SUCCESS;
        pv_error_message := PKG_CONST.ERROR__NO_ERROR;
        RETURN;
    END IF;

    SELECT
        D.DEVICE_SERIAL_CD,
        D.DEVICE_TYPE_ID,
        DECODE(DST.DEVICE_SUB_TYPE_ID, NULL, DT.DEF_BASE_HOST_TYPE_ID, DST.DEF_BASE_HOST_TYPE_ID),
        DECODE(DST.DEVICE_SUB_TYPE_ID, NULL, DT.DEF_BASE_HOST_EQUIPMENT_ID, DST.DEF_BASE_HOST_EQUIPMENT_ID),
        DECODE(DST.DEVICE_SUB_TYPE_ID, NULL, DT.DEF_PRIM_HOST_TYPE_ID, DST.DEF_PRIM_HOST_TYPE_ID),
        DECODE(DST.DEVICE_SUB_TYPE_ID, NULL, DT.DEF_PRIM_HOST_EQUIPMENT_ID, DST.DEF_PRIM_HOST_EQUIPMENT_ID)
    INTO
        lv_device_serial_cd,
        ln_device_type_id,
        ln_def_base_host_type_id,
        ln_def_base_host_equipment_id,
        ln_def_prim_host_type_id,
        ln_def_prim_host_equipment_id
    FROM DEVICE.DEVICE D
    JOIN DEVICE.DEVICE_TYPE DT ON D.DEVICE_TYPE_ID = DT.DEVICE_TYPE_ID
    LEFT OUTER JOIN DEVICE.DEVICE_SUB_TYPE DST ON D.DEVICE_TYPE_ID = DST.DEVICE_TYPE_ID AND D.DEVICE_SUB_TYPE_ID = DST.DEVICE_SUB_TYPE_ID
    WHERE D.DEVICE_ID = ln_device_id;

    -- Create base host if it doesn't exist
    IF ln_base_host_count = 0 AND ln_def_base_host_type_id IS NOT NULL THEN
        INSERT INTO device.host
        (
            host_status_cd,
            host_serial_cd,
            host_est_complete_minut,
            host_port_num,
            host_setting_updated_yn_flag,
            device_id,
            host_position_num,
            host_active_yn_flag,
            host_type_id,
            host_equipment_id
        )
        VALUES
        (
            0,
            lv_device_serial_cd,
            0,
            0,
            'N',
            ln_device_id,
            0,
            'Y',
            ln_def_base_host_type_id,
            ln_def_base_host_equipment_id
        );

        pn_new_host_count := pn_new_host_count + 1;
    END IF;

    -- Create primary host if no other hosts exist
    IF ln_other_host_count = 0 AND ln_def_prim_host_type_id IS NOT NULL THEN
        INSERT INTO device.host
        (
            host_status_cd,
            host_est_complete_minut,
            host_port_num,
            host_setting_updated_yn_flag,
            device_id,
            host_position_num,
            host_active_yn_flag,
            host_type_id,
            host_equipment_id
        )
        VALUES
        (
            0,
            0,
            1,
            'N',
            ln_device_id,
            0,
            'Y',
            ln_def_prim_host_type_id,
            ln_def_prim_host_equipment_id
        );

        pn_new_host_count := pn_new_host_count + 1;
    END IF;

    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;

    FUNCTION GET_OR_CREATE_HOST_EQUIPMENT(
        pv_host_equipment_mfgr IN HOST_EQUIPMENT.HOST_EQUIPMENT_MFGR%TYPE,
    	pv_host_equipment_model IN HOST_EQUIPMENT.HOST_EQUIPMENT_MODEL%TYPE,
        pn_existing_cnt OUT PLS_INTEGER)
        RETURN HOST_EQUIPMENT.HOST_EQUIPMENT_ID%TYPE
    IS
        ln_host_equipment_id HOST_EQUIPMENT.HOST_EQUIPMENT_ID%TYPE;
    BEGIN
        SELECT MAX(HOST_EQUIPMENT_ID), 1
          INTO ln_host_equipment_id, pn_existing_cnt
          FROM DEVICE.HOST_EQUIPMENT
         WHERE HOST_EQUIPMENT_MFGR = pv_host_equipment_mfgr
           AND HOST_EQUIPMENT_MODEL = pv_host_equipment_model;
        IF ln_host_equipment_id IS NULL THEN
            SELECT SEQ_HOST_EQUIPMENT_ID.NEXTVAL, 0
              INTO ln_host_equipment_id, pn_existing_cnt
              FROM DUAL;
            INSERT INTO DEVICE.HOST_EQUIPMENT(HOST_EQUIPMENT_ID, HOST_EQUIPMENT_MFGR, HOST_EQUIPMENT_MODEL)
                 VALUES(ln_host_equipment_id, pv_host_equipment_mfgr, pv_host_equipment_model);
        END IF;
        RETURN ln_host_equipment_id;
    EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
            RETURN GET_OR_CREATE_HOST_EQUIPMENT(pv_host_equipment_mfgr, pv_host_equipment_model, pn_existing_cnt);
    END;
        
    PROCEDURE UPSERT_HOST(
        pn_device_id IN DEVICE.DEVICE_ID%TYPE,
        pn_host_port_num IN HOST.HOST_PORT_NUM%TYPE,
        pn_host_position_num IN HOST.HOST_POSITION_NUM%TYPE,
        pn_host_type_id IN HOST.HOST_TYPE_ID%TYPE,
        pv_host_serial_cd IN HOST.HOST_SERIAL_CD%TYPE,
    	pv_host_label_cd IN HOST.HOST_LABEL_CD%TYPE,
    	pn_host_equipment_id HOST_EQUIPMENT.HOST_EQUIPMENT_ID%TYPE,
        pn_host_est_complete_minut IN HOST.HOST_EST_COMPLETE_MINUT%TYPE,
        pn_host_id OUT HOST.HOST_ID%TYPE,
        pn_existing_cnt OUT PLS_INTEGER)
    IS
		ln_device_id DEVICE.DEVICE_ID%TYPE;
    BEGIN
		SELECT MAX(dla.device_id)
		INTO ln_device_id
		FROM device.device d
		JOIN device.device_last_active dla ON d.device_name = dla.device_name
		WHERE d.device_id = pn_device_id;	
	
        UPDATE DEVICE.HOST
           SET (HOST_TYPE_ID, HOST_SERIAL_CD, HOST_LABEL_CD, HOST_EQUIPMENT_ID, HOST_EST_COMPLETE_MINUT, HOST_ACTIVE_YN_FLAG) =
               (SELECT pn_host_type_id, pv_host_serial_cd, pv_host_label_cd, pn_host_equipment_id, pn_host_est_complete_minut, 'Y' FROM DUAL)
         WHERE DEVICE_ID = ln_device_id
           AND HOST_PORT_NUM = pn_host_port_num
           AND HOST_POSITION_NUM = pn_host_position_num
           RETURNING HOST_ID, 1 INTO pn_host_id, pn_existing_cnt;
        IF pn_host_id IS NULL THEN
            SELECT SEQ_HOST_ID.NEXTVAL, 0
              INTO pn_host_id, pn_existing_cnt
              FROM DUAL;
            INSERT INTO DEVICE.HOST(HOST_ID, DEVICE_ID, HOST_PORT_NUM, HOST_POSITION_NUM, HOST_TYPE_ID, HOST_SERIAL_CD, HOST_LABEL_CD, HOST_EQUIPMENT_ID, HOST_EST_COMPLETE_MINUT, HOST_SETTING_UPDATED_YN_FLAG, HOST_ACTIVE_YN_FLAG)
                 VALUES(pn_host_id, ln_device_id, pn_host_port_num, pn_host_position_num, pn_host_type_id, pv_host_serial_cd, pv_host_label_cd, pn_host_equipment_id, pn_host_est_complete_minut, 'N', 'Y');	        		
       END IF;
    EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
            UPSERT_HOST(pn_device_id, pn_host_port_num, pn_host_position_num, pn_host_type_id, pv_host_serial_cd, pv_host_label_cd, pn_host_equipment_id, pn_host_est_complete_minut, pn_host_id, pn_existing_cnt);
    END;
    
    PROCEDURE UPSERT_HOST(
        pn_device_id IN DEVICE.DEVICE_ID%TYPE,
        pn_host_port_num IN HOST.HOST_PORT_NUM%TYPE,
        pn_host_position_num IN HOST.HOST_POSITION_NUM%TYPE,
        pn_host_type_id IN HOST.HOST_TYPE_ID%TYPE,
        pv_host_serial_cd IN HOST.HOST_SERIAL_CD%TYPE,
    	pv_host_label_cd IN HOST.HOST_LABEL_CD%TYPE,
    	pv_host_equipment_mfgr IN HOST_EQUIPMENT.HOST_EQUIPMENT_MFGR%TYPE,
    	pv_host_equipment_model IN HOST_EQUIPMENT.HOST_EQUIPMENT_MODEL%TYPE,
        pn_host_est_complete_minut IN HOST.HOST_EST_COMPLETE_MINUT%TYPE,
        pn_host_id OUT HOST.HOST_ID%TYPE,
        pn_existing_cnt OUT PLS_INTEGER)
    IS
        ln_host_equipment_existing_cnt PLS_INTEGER;
    BEGIN
        UPSERT_HOST(pn_device_id, pn_host_port_num, pn_host_position_num, pn_host_type_id, pv_host_serial_cd, pv_host_label_cd, GET_OR_CREATE_HOST_EQUIPMENT(pv_host_equipment_mfgr, pv_host_equipment_model, ln_host_equipment_existing_cnt), pn_host_est_complete_minut, pn_host_id, pn_existing_cnt);
    END;

    PROCEDURE UPSERT_HOST_SETTING(
		pn_host_id HOST.HOST_ID%TYPE,
		pv_host_setting_parameter_cd HOST_SETTING.HOST_SETTING_PARAMETER%TYPE,
		pv_host_setting_value HOST_SETTING.HOST_SETTING_VALUE%TYPE)
    IS
    BEGIN
       UPDATE DEVICE.HOST_SETTING
           SET HOST_SETTING_VALUE = pv_host_setting_value
         WHERE HOST_ID = pn_host_id 
           AND HOST_SETTING_PARAMETER = pv_host_setting_parameter_cd;
	    IF SQL%NOTFOUND THEN
            INSERT INTO DEVICE.HOST_SETTING(HOST_ID, HOST_SETTING_PARAMETER, HOST_SETTING_VALUE)
                VALUES(pn_host_id, pv_host_setting_parameter_cd, pv_host_setting_value);
        END IF;
    EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
            UPSERT_HOST_SETTING(pn_host_id, pv_host_setting_parameter_cd, pv_host_setting_value);
    END;
    
    PROCEDURE UPSERT_HOST_SETTING(
		pn_host_id HOST.HOST_ID%TYPE,
		pv_host_setting_parameter_cd HOST_SETTING.HOST_SETTING_PARAMETER%TYPE,
		pv_host_setting_value HOST_SETTING.HOST_SETTING_VALUE%TYPE,
        pv_old_host_setting_value OUT HOST_SETTING.HOST_SETTING_VALUE%TYPE)
    IS
    BEGIN
        SELECT MAX(HOST_SETTING_VALUE)
          INTO pv_old_host_setting_value
          FROM DEVICE.HOST_SETTING
         WHERE HOST_ID = pn_host_id 
           AND HOST_SETTING_PARAMETER = pv_host_setting_parameter_cd;         
        UPSERT_HOST_SETTING(pn_host_id, pv_host_setting_parameter_cd, pv_host_setting_value);
    END;
    
     PROCEDURE UPDATE_COMM_STATS(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pn_rssi NUMBER,
		pn_ber NUMBER,
        pd_update_ts GPRS_DEVICE.RSSI_TS%TYPE,
        pv_modem_info GPRS_DEVICE.MODEM_INFO%TYPE)
    IS
        ln_host_id HOST.HOST_ID%TYPE;
		ln_device_id DEVICE.DEVICE_ID%TYPE;	
        lv_device_name DEVICE.DEVICE_NAME%TYPE;	
    BEGIN
		SELECT MAX(dla.device_id), MAX(D.DEVICE_NAME)
		INTO ln_device_id, lv_device_name
		FROM device.device d
		JOIN device.device_last_active dla ON d.device_name = dla.device_name
		WHERE d.device_id = pn_device_id;
	
        UPDATE DEVICE.DEVICE_DATA
           SET RSSI = pn_rssi,
               RSSI_TS = pd_update_ts
         WHERE DEVICE_NAME = lv_device_name
           AND RSSI_TS < pd_update_ts;
	
        IF SQL%NOTFOUND THEN
            BEGIN
                INSERT INTO DEVICE.DEVICE_DATA(DEVICE_NAME, RSSI, RSSI_TS)
                    VALUES(lv_device_name, pn_rssi, pd_update_ts);
            EXCEPTION
                WHEN DUP_VAL_ON_INDEX THEN
                    NULL;
            END;
        END IF;      
        ENGINE.PKG_DATA_SYNC.SP_CREATE_DATA_SYNC('DEVICE_INFO', 'DEVICE.DEVICE_SETTING', 'U', pn_device_id, lv_device_name, NULL);
        
		UPDATE DEVICE.GPRS_DEVICE GD
		SET DEVICE_ID = NULL,
			GPRS_DEVICE_STATE_ID = DECODE(GPRS_DEVICE_STATE_ID, 5, 4, GPRS_DEVICE_STATE_ID)
		WHERE DEVICE_ID = ln_device_id
			AND ICCID NOT IN(SELECT TO_NUMBER_OR_NULL(HOST_SERIAL_CD) FROM DEVICE.HOST WHERE DEVICE_ID = GD.DEVICE_ID AND HOST_TYPE_ID = 202);
	
		UPDATE DEVICE.GPRS_DEVICE GD
		   SET RSSI = pn_rssi || ',' || pn_ber, 
               RSSI_TS = pd_update_ts, 
               LAST_FILE_TRANSFER_ID = NULL, 
               MODEM_INFO_RECEIVED_TS = SYSDATE, 
               MODEM_INFO = pv_modem_info,
		   	   DEVICE_ID = ln_device_id, 
               GPRS_DEVICE_STATE_ID = 5,
		       ASSIGNED_BY = DECODE(DEVICE_ID, ln_device_id, ASSIGNED_BY, 'APP_LAYER'), 
		   	   ASSIGNED_TS = DECODE(DEVICE_ID, ln_device_id, ASSIGNED_TS, SYSDATE)
		 WHERE ICCID IN(SELECT TO_NUMBER_OR_NULL(HOST_SERIAL_CD) FROM DEVICE.HOST WHERE DEVICE_ID = ln_device_id AND HOST_TYPE_ID = 202)
		   AND (RSSI_TS IS NULL OR RSSI_TS < pd_update_ts);
        SELECT MAX(HOST_ID)
          INTO ln_host_id
          FROM DEVICE.HOST
         WHERE DEVICE_ID = ln_device_id
           AND HOST_TYPE_ID IN(202,204);
        IF ln_host_id IS NOT NULL THEN
            UPSERT_HOST_SETTING(ln_host_id,'CSQ',pn_rssi || ',' || pn_ber);
        END IF;
	END;

	-- SP_CONSTRUCT_PROPERTIES_FILE is deprecated in R30
    PROCEDURE SP_CONSTRUCT_PROPERTIES_FILE(
        pn_device_id IN device.device_id%TYPE,
        pl_file OUT CLOB)
    IS
    BEGIN
        NULL;
    END;

	-- SP_UPDATE_DEVICE_CONFIG_FILE is deprecated in R30
    PROCEDURE SP_UPDATE_DEVICE_CONFIG_FILE(
        pn_device_id DEVICE.DEVICE_ID%TYPE,
        pl_config_file LONG)
    IS
    BEGIN
		NULL;
    END;

    PROCEDURE SP_INITIALIZE_CONFIG_FILE(
        pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
        pn_new_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
        pn_new_property_list_version NUMBER,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pn_updated OUT NUMBER,
        pn_old_property_list_version OUT NUMBER)
    IS
        ln_device_id DEVICE.DEVICE_ID%TYPE;
        lv_default_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE;
        lv_old_default_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE;
		lv_last_lock_utc_ts VARCHAR2(128);
     BEGIN
		lv_last_lock_utc_ts := PKG_GLOBAL.REQUEST_LOCK('DEVICE.DEVICE', pv_device_name);
	 
        SELECT MAX(DEVICE_ID)
          INTO ln_device_id
          FROM (SELECT DEVICE_ID
             FROM DEVICE.DEVICE
            WHERE DEVICE_NAME = pv_device_name
            ORDER BY DEVICE_ACTIVE_YN_FLAG DESC, CREATED_TS DESC)
         WHERE ROWNUM = 1;

		IF ln_device_id IS NULL THEN
			RETURN;
		END IF;
		
		SELECT DECODE(DBADMIN.PKG_UTL.COMPARE(pn_new_device_type_id, 13), 
			-1, default_config_template_name, default_config_template_name || pn_new_property_list_version)
		INTO lv_default_name
		FROM device.device_type
		WHERE device_type_id = pn_new_device_type_id;
		
		INSERT INTO DEVICE.DEVICE_SETTING(DEVICE_ID, DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_VALUE)
		SELECT ln_device_id, CTS.DEVICE_SETTING_PARAMETER_CD, CTS.CONFIG_TEMPLATE_SETTING_VALUE
		FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS
		JOIN DEVICE.CONFIG_TEMPLATE CT ON CTS.CONFIG_TEMPLATE_ID = CT.CONFIG_TEMPLATE_ID
		WHERE CT.CONFIG_TEMPLATE_NAME = lv_default_name AND NOT EXISTS (
			SELECT 1 FROM DEVICE.DEVICE_SETTING
			WHERE DEVICE_ID = ln_device_id
				AND DEVICE_SETTING_PARAMETER_CD = CTS.DEVICE_SETTING_PARAMETER_CD
		);
		
		IF pn_new_device_type_id IN (0, 1) THEN
			-- set Call Home Now Flag to N
			UPDATE DEVICE.DEVICE_SETTING
			SET DEVICE_SETTING_VALUE = '4E'
			WHERE DEVICE_ID = ln_device_id
				AND DEVICE_SETTING_PARAMETER_CD = '210'
				AND DBADMIN.PKG_UTL.EQL(DEVICE_SETTING_VALUE, '4E') = 'N';
		END IF;
	 
		IF pn_new_device_type_id IN (0, 1, 6) THEN
			RETURN;
		END IF;

		SELECT MAX(TO_NUMBER(ds.DEVICE_SETTING_VALUE))
		  INTO pn_old_property_list_version
		  FROM DEVICE.DEVICE_SETTING ds
		 WHERE ds.DEVICE_ID = ln_device_id
		   AND ds.DEVICE_SETTING_PARAMETER_CD = PKG_CONST.DSP__PROPERTY_LIST_VERSION
		   AND REGEXP_LIKE(ds.DEVICE_SETTING_VALUE, '^[0-9]+$');
		IF pn_new_property_list_version IS NOT NULL AND pn_new_property_list_version <> NVL(pn_old_property_list_version, -1) THEN
			SP_UPDATE_DEVICE_SETTING(ln_device_id, PKG_CONST.DSP__PROPERTY_LIST_VERSION, pn_new_property_list_version);
			
		   -- Update all values that equal the default in the old version and where the default in the new version is differnt
		   lv_old_default_name := 'DEFAULT-CFG-' || pn_new_device_type_id || '-' || pn_old_property_list_version;
		   MERGE INTO DEVICE.DEVICE_SETTING O
			 USING (
				   SELECT ln_device_id DEVICE_ID, cts.DEVICE_SETTING_PARAMETER_CD SETTING_NAME, cts.CONFIG_TEMPLATE_SETTING_VALUE SETTING_VALUE
					FROM DEVICE.CONFIG_TEMPLATE ct
					JOIN DEVICE.CONFIG_TEMPLATE_SETTING cts
					  ON cts.CONFIG_TEMPLATE_ID = ct.CONFIG_TEMPLATE_ID
					JOIN DEVICE.CONFIG_TEMPLATE_SETTING cts0
					  ON cts0.DEVICE_SETTING_PARAMETER_CD = cts.DEVICE_SETTING_PARAMETER_CD
					JOIN DEVICE.CONFIG_TEMPLATE ct0
					  ON cts0.CONFIG_TEMPLATE_ID = ct0.CONFIG_TEMPLATE_ID
					JOIN DEVICE.DEVICE_SETTING ds
					  ON cts.DEVICE_SETTING_PARAMETER_CD = ds.DEVICE_SETTING_PARAMETER_CD
				   WHERE cts.CONFIG_TEMPLATE_SETTING_VALUE != cts0.CONFIG_TEMPLATE_SETTING_VALUE
					 AND cts0.CONFIG_TEMPLATE_SETTING_VALUE = ds.DEVICE_SETTING_VALUE
					 AND cts.DEVICE_SETTING_PARAMETER_CD NOT IN('50','51','52','60','61','62','63','64','80','81','70','100','101','102','103','104','105','106','107','108','200','201','202','203','204','205','206','207','208','300','301')
					 AND TO_NUMBER_OR_NULL(cts.DEVICE_SETTING_PARAMETER_CD) IS NOT NULL
					 AND ct.CONFIG_TEMPLATE_NAME = lv_default_name
					 AND ct0.CONFIG_TEMPLATE_NAME = lv_old_default_name
					 AND ds.DEVICE_ID = ln_device_id) N
				  ON (O.DEVICE_ID = N.DEVICE_ID AND O.DEVICE_SETTING_PARAMETER_CD = N.SETTING_NAME)
				  WHEN MATCHED THEN
				   UPDATE
					  SET O.DEVICE_SETTING_VALUE = N.SETTING_VALUE
				  WHEN NOT MATCHED THEN
				   INSERT (O.DEVICE_ID,
						   O.DEVICE_SETTING_PARAMETER_CD,
						   O.DEVICE_SETTING_VALUE)
					VALUES(N.DEVICE_ID,
						   N.SETTING_NAME,
						   N.SETTING_VALUE
					);
		END IF;

        pn_updated := PKG_CONST.BOOLEAN__TRUE;
    END;

    PROCEDURE SP_INITIALIZE_CONFIG_FILE(
        pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
        pn_new_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
        pn_new_property_list_version NUMBER,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pn_updated OUT NUMBER)
    IS
        ln_old_property_list_version NUMBER;
    BEGIN
        SP_INITIALIZE_CONFIG_FILE(pv_device_name, pn_new_device_type_id, pn_new_property_list_version, pn_file_transfer_id, pn_updated, ln_old_property_list_version);
    END;

    PROCEDURE ADD_PENDING_FILE_TRANSFER(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_id FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pc_data_type ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pn_file_transfer_group DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
        pn_file_transfer_packet_size DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
        pn_execute_order ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE
        )
    IS
        ln_device_file_transfer_id ENGINE.MACHINE_CMD_PENDING.DEVICE_FILE_TRANSFER_ID%TYPE;
    BEGIN
      ADD_PENDING_FILE_TRANSFER(
        pv_device_name,pv_file_transfer_name,pn_file_transfer_type_id,
        pc_data_type,pn_file_transfer_group,pn_file_transfer_packet_size,pn_execute_order,pn_command_id, pn_file_transfer_id, ln_device_file_transfer_id);
        
    END;
    
    PROCEDURE ADD_PENDING_FILE_TRANSFER(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_id FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pc_data_type ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pn_file_transfer_group DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
        pn_file_transfer_packet_size DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
        pn_execute_order ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pn_device_file_transfer_id OUT ENGINE.MACHINE_CMD_PENDING.DEVICE_FILE_TRANSFER_ID%TYPE
        )
    IS
        ln_device_id DEVICE_FILE_TRANSFER.DEVICE_ID%TYPE;
    BEGIN
        SELECT SEQ_FILE_TRANSFER_ID.NEXTVAL, SEQ_DEVICE_FILE_TRANSFER_ID.NEXTVAL, ENGINE.SEQ_MACHINE_CMD_PENDING_ID.NEXTVAL, DEVICE_ID
          INTO pn_file_transfer_id, pn_device_file_transfer_id, pn_command_id, ln_device_id
          FROM (SELECT DEVICE_ID
          FROM DEVICE.DEVICE
         WHERE DEVICE_NAME = pv_device_name
         ORDER BY DEVICE_ACTIVE_YN_FLAG DESC, DEVICE_ID DESC)
         WHERE ROWNUM = 1;
        INSERT INTO DEVICE.FILE_TRANSFER(FILE_TRANSFER_ID, FILE_TRANSFER_TYPE_CD, FILE_TRANSFER_NAME)
           VALUES(pn_file_transfer_id, pn_file_transfer_type_id, pv_file_transfer_name);
        INSERT INTO DEVICE.DEVICE_FILE_TRANSFER(DEVICE_FILE_TRANSFER_ID, DEVICE_ID, FILE_TRANSFER_ID, DEVICE_FILE_TRANSFER_DIRECT, DEVICE_FILE_TRANSFER_STATUS_CD, DEVICE_FILE_TRANSFER_GROUP_NUM, DEVICE_FILE_TRANSFER_PKT_SIZE)
            VALUES(pn_device_file_transfer_id, ln_device_id, pn_file_transfer_id, 'O', 0, pn_file_transfer_group, pn_file_transfer_packet_size);
        INSERT INTO ENGINE.MACHINE_CMD_PENDING(MACHINE_COMMAND_PENDING_ID, MACHINE_ID, DATA_TYPE, DEVICE_FILE_TRANSFER_ID, EXECUTE_CD, EXECUTE_ORDER, EXECUTE_DATE)
           VALUES(pn_command_id, pv_device_name, pc_data_type, pn_device_file_transfer_id, 'S', NVL(pn_execute_order, 999), SYSDATE);
    END;

	-- R30+
    PROCEDURE REQUEST_OUTBOUND_FILE_TRANSFER(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_id FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pc_data_type ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pn_file_transfer_group DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
        pn_file_transfer_packet_size DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
        pl_file_transfer_content OUT FILE_TRANSFER.FILE_TRANSFER_CONTENT%TYPE,
        pd_file_transfer_create_ts OUT FILE_TRANSFER.CREATED_TS%TYPE,
        pn_mcp_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
		pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE)
    IS
        l_dft_id DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_ID%TYPE;
        l_device_id DEVICE_FILE_TRANSFER.DEVICE_ID%TYPE;
    BEGIN
        SELECT FILE_TRANSFER_ID, CREATED_TS
        INTO pn_file_transfer_id, pd_file_transfer_create_ts
        FROM (
            SELECT FILE_TRANSFER_ID, CREATED_TS
            FROM DEVICE.FILE_TRANSFER
            WHERE FILE_TRANSFER_NAME = pv_file_transfer_name
                AND FILE_TRANSFER_TYPE_CD = pn_file_transfer_type_id
            ORDER BY CREATED_TS
        ) WHERE ROWNUM = 1;
        SELECT MAX(dft.DEVICE_FILE_TRANSFER_ID)
          INTO l_dft_id
          FROM DEVICE.DEVICE_FILE_TRANSFER dft
          JOIN DEVICE.DEVICE d ON dft.DEVICE_ID = d.DEVICE_ID
         WHERE d.DEVICE_NAME = pv_device_name
           AND dft.FILE_TRANSFER_ID = pn_file_transfer_id
           AND dft.DEVICE_FILE_TRANSFER_DIRECT = 'O'
           AND dft.DEVICE_FILE_TRANSFER_STATUS_CD = 1;
       IF l_dft_id IS NOT NULL THEN -- already exists, find pending command
         SELECT MAX(MACHINE_COMMAND_PENDING_ID) -- avoid NO_DATA_FOUND
           INTO pn_mcp_id
           FROM ENGINE.MACHINE_CMD_PENDING
          WHERE MACHINE_ID = pv_device_name
            AND DEVICE_FILE_TRANSFER_ID = l_dft_id;
        ELSE
            SELECT SEQ_DEVICE_FILE_TRANSFER_ID.NEXTVAL, DEVICE_ID
              INTO l_dft_id, l_device_id
              FROM (SELECT DEVICE_ID
              FROM DEVICE.DEVICE
             WHERE DEVICE_NAME = pv_device_name
             ORDER BY DEVICE_ACTIVE_YN_FLAG DESC, DEVICE_ID DESC)
             WHERE ROWNUM = 1;
            INSERT INTO DEVICE.DEVICE_FILE_TRANSFER(DEVICE_FILE_TRANSFER_ID, DEVICE_ID, FILE_TRANSFER_ID, DEVICE_FILE_TRANSFER_DIRECT, DEVICE_FILE_TRANSFER_STATUS_CD, DEVICE_FILE_TRANSFER_GROUP_NUM, DEVICE_FILE_TRANSFER_PKT_SIZE)
              VALUES(l_dft_id, l_device_id, pn_file_transfer_id, 'O', 0, pn_file_transfer_group, pn_file_transfer_packet_size);
        END IF;
        IF pn_mcp_id IS NULL THEN
            SELECT ENGINE.SEQ_MACHINE_CMD_PENDING_ID.NEXTVAL
              INTO pn_mcp_id
              FROM DUAL;
            INSERT INTO ENGINE.MACHINE_CMD_PENDING(MACHINE_COMMAND_PENDING_ID, MACHINE_ID, DATA_TYPE, DEVICE_FILE_TRANSFER_ID, EXECUTE_CD, EXECUTE_ORDER, EXECUTE_DATE)
              VALUES(pn_mcp_id, pv_device_name, pc_data_type, l_dft_id, 'S', 999, SYSDATE);
        END IF;
        SP_GET_FILE_TRANSFER_BLOB(pn_file_transfer_id, pl_file_transfer_content);
    EXCEPTION
       WHEN NO_DATA_FOUND THEN
               NULL; -- let variables be null
    END;
	
	-- R29
	PROCEDURE REQUEST_OUTBOUND_FILE_TRANSFER(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_id FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pc_data_type ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pn_file_transfer_group DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
        pn_file_transfer_packet_size DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
        pl_file_transfer_content OUT FILE_TRANSFER.FILE_TRANSFER_CONTENT%TYPE,
        pd_file_transfer_create_ts OUT FILE_TRANSFER.CREATED_TS%TYPE,
        pn_mcp_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE)
	IS
		ln_file_transfer_id FILE_TRANSFER.FILE_TRANSFER_ID%TYPE;
	BEGIN
		REQUEST_OUTBOUND_FILE_TRANSFER(
        pv_device_name,
        pv_file_transfer_name,
        pn_file_transfer_type_id,
        pc_data_type,
        pn_file_transfer_group,
        pn_file_transfer_packet_size,
        pl_file_transfer_content,
        pd_file_transfer_create_ts,
        pn_mcp_id,
		ln_file_transfer_id);
	END;
	
	-- R33+ signature
    PROCEDURE SP_RECORD_FILE_TRANSFER(
        pv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_cd FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pv_event_global_trans_cd VARCHAR2,
        pc_overwrite_flag CHAR,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pd_file_transfer_ts DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_TS%TYPE DEFAULT SYSDATE,
        pv_global_session_cd VARCHAR2
    )
    IS
        ln_device_file_transfer_id DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_ID%TYPE;
        ln_device_id DEVICE_FILE_TRANSFER.DEVICE_ID%TYPE;
    BEGIN
		ln_device_id := GET_DEVICE_ID_BY_NAME(pv_device_name, pd_file_transfer_ts);
		
		-- duplicate detection of already stored files re-delivered by QueueLayer
		SELECT /*+INDEX(DFT IDX_DEV_FILE_XFER_GLOB_SESS_CD)*/ MAX(DEVICE_FILE_TRANSFER_ID), MAX(FILE_TRANSFER_ID)
		INTO ln_device_file_transfer_id, pn_file_transfer_id
		FROM DEVICE.DEVICE_FILE_TRANSFER DFT
		WHERE GLOBAL_SESSION_CD = pv_global_session_cd
			AND DEVICE_ID = ln_device_id
			AND DEVICE_FILE_TRANSFER_TS = pd_file_transfer_ts;
		IF ln_device_file_transfer_id IS NOT NULL THEN
			RETURN;
		END IF;
		
        BEGIN
            SELECT FILE_TRANSFER_ID
              INTO pn_file_transfer_id
              FROM (SELECT /*+ index(FT INX_FILE_TRANSFER_TYPE_NAME) index(DFT IDX_DEVICE_FILE_TRANSFER_ID) */ ft.FILE_TRANSFER_ID
              FROM DEVICE.FILE_TRANSFER ft
              LEFT JOIN DEVICE.DEVICE_FILE_TRANSFER dft on ft.FILE_TRANSFER_ID = dft.FILE_TRANSFER_ID
             WHERE ft.FILE_TRANSFER_NAME = pv_file_transfer_name
               AND ft.FILE_TRANSFER_TYPE_CD = pn_file_transfer_type_cd
               AND NVL(dft.DEVICE_FILE_TRANSFER_DIRECT, 'I') = 'I'
               AND NVL(dft.DEVICE_FILE_TRANSFER_STATUS_CD, 1) = 1
               AND pc_overwrite_flag = 'Y'
             ORDER BY ft.FILE_TRANSFER_ID DESC)
             WHERE ROWNUM = 1;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                SELECT SEQ_FILE_TRANSFER_ID.NEXTVAL
                  INTO pn_file_transfer_id
                  FROM DUAL;
                INSERT INTO DEVICE.FILE_TRANSFER (
                    FILE_TRANSFER_ID,
                    FILE_TRANSFER_NAME,
                    FILE_TRANSFER_TYPE_CD)
                  VALUES(
                    pn_file_transfer_id,
                    pv_file_transfer_name,
                    pn_file_transfer_type_cd);
        END;
        SELECT SEQ_DEVICE_FILE_TRANSFER_ID.NEXTVAL
          INTO ln_device_file_transfer_id
          FROM DUAL;
        INSERT INTO DEVICE.DEVICE_FILE_TRANSFER(
            DEVICE_FILE_TRANSFER_ID,
            DEVICE_ID,
            FILE_TRANSFER_ID,
            DEVICE_FILE_TRANSFER_DIRECT,
            DEVICE_FILE_TRANSFER_STATUS_CD,
            DEVICE_FILE_TRANSFER_TS,
            GLOBAL_SESSION_CD)
         VALUES(
            ln_device_file_transfer_id,
            ln_device_id,
            pn_file_transfer_id,
            'I',
            1,
            pd_file_transfer_ts,
            pv_global_session_cd);
        IF pv_event_global_trans_cd IS NOT NULL THEN
            INSERT INTO DEVICE.DEVICE_FILE_TRANSFER_EVENT(EVENT_ID, DEVICE_FILE_TRANSFER_ID)
              SELECT EVENT_ID, ln_device_file_transfer_id
                FROM DEVICE.EVENT
                WHERE EVENT_GLOBAL_TRANS_CD = pv_event_global_trans_cd;
        END IF;
		
		-- mark any pending file transfer requests for the same file name as complete
		UPDATE engine.machine_cmd_pending
		SET execute_cd = 'A', global_session_cd = pv_global_session_cd
		WHERE machine_id = pv_device_name
			AND data_type = '9B'
			AND command = UPPER(RAWTOHEX(pv_file_transfer_name));
    END;

	FUNCTION GET_NORMALIZED_TIME(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
		pv_normalizer_start_min_cd ENGINE.APP_SETTING.APP_SETTING_CD%TYPE,
		pv_normalizer_end_min_cd ENGINE.APP_SETTING.APP_SETTING_CD%TYPE,
		pv_window_start_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_window_hours_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_device_time_zone_guid VARCHAR2
	) RETURN VARCHAR2
	IS
		ln_call_in_start_min NUMBER;
		ln_call_in_end_min NUMBER;
		ln_call_in_window_min NUMBER;
		ln_call_in_time_min NUMBER;
		ln_call_in_window_start NUMBER;
		ln_call_in_window_hours NUMBER;
		ln_seed_4 NUMBER := NVL(DBADMIN.TO_NUMBER_OR_NULL(SUBSTR(pv_device_serial_cd, -4)), 0);
		ln_convert_time_zone NUMBER := PKG_CONST.BOOLEAN__TRUE;
	BEGIN
		ln_call_in_window_start := ROUND(NVL(DBADMIN.TO_NUMBER_OR_NULL(GET_DEVICE_SETTING(pn_device_id, pv_window_start_cd)), -1));
		ln_call_in_window_hours := ROUND(NVL(DBADMIN.TO_NUMBER_OR_NULL(GET_DEVICE_SETTING(pn_device_id, pv_window_hours_cd)), 0));
		IF ln_call_in_window_start BETWEEN 0 AND 2359 AND ln_call_in_window_hours BETWEEN 1 AND 23 THEN
			ln_call_in_start_min := FLOOR(ln_call_in_window_start / 100) * 60 + MOD(ln_call_in_window_start, 100);
			ln_call_in_window_min := ln_call_in_window_hours * 60;
			ln_convert_time_zone := PKG_CONST.BOOLEAN__FALSE;
		ELSIF pv_normalizer_start_min_cd IS NULL OR pv_normalizer_end_min_cd IS NULL THEN
			RETURN NULL;
		ELSE
			ln_call_in_start_min := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING(pv_normalizer_start_min_cd));
			ln_call_in_end_min := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING(pv_normalizer_end_min_cd));
			
			IF ln_call_in_start_min < 0 OR ln_call_in_start_min = ln_call_in_end_min THEN
				RETURN NULL;
			END IF;

			ln_call_in_window_min := MOD(ln_call_in_end_min - ln_call_in_start_min, 1440);
			IF ln_call_in_window_min < 0 THEN
				ln_call_in_window_min := ln_call_in_window_min + 1440;
			END IF;
		END IF;
			
		-- use last 4 digits of device serial number to calculate its call-in time
		ln_call_in_time_min := MOD(ln_call_in_start_min + ROUND(ln_call_in_window_min * ln_seed_4 / 9999), 1440);
		
		IF ln_convert_time_zone = PKG_CONST.BOOLEAN__TRUE THEN
			-- convert call-in time from server time zone to device local time zone
			ln_call_in_time_min := MOD(ln_call_in_time_min + DBADMIN.PKG_UTL.TIME_ZONE_DIFF_MIN(pv_device_time_zone_guid, PKG_CONST.DB_TIME_ZONE), 1440);
			IF ln_call_in_time_min < 0 THEN
				ln_call_in_time_min := ln_call_in_time_min + 1440;
			END IF;
		END IF;
		
		RETURN LPAD(FLOOR(ln_call_in_time_min / 60), 2, '0') || LPAD(MOD(ln_call_in_time_min, 60), 2, '0');			
	END;
	
	FUNCTION GET_NORMALIZED_OFFSET(
		pv_normalized_time VARCHAR2,
		pv_device_time_zone_guid VARCHAR2
	) RETURN NUMBER
	IS
		ln_normalized_time_min NUMBER;
		ln_normalized_utc_time_min NUMBER;
	BEGIN
		ln_normalized_time_min := TO_NUMBER(SUBSTR(pv_normalized_time, 1, 2)) * 60 + TO_NUMBER(SUBSTR(pv_normalized_time, 3, 2));
		ln_normalized_utc_time_min := MOD(ln_normalized_time_min + DBADMIN.PKG_UTL.TIME_ZONE_DIFF_MIN(PKG_CONST.GMT_TIME_ZONE, pv_device_time_zone_guid), 1440);
		IF ln_normalized_utc_time_min < 0 THEN
			ln_normalized_utc_time_min := ln_normalized_utc_time_min + 1440;
		END IF;
		RETURN ln_normalized_utc_time_min * 60;
	END;

    PROCEDURE NORMALIZE_SCHEDULE(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
		pv_schedule_type DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_normalizer_start_min_cd ENGINE.APP_SETTING.APP_SETTING_CD%TYPE,
		pv_normalizer_end_min_cd ENGINE.APP_SETTING.APP_SETTING_CD%TYPE,
		pv_window_start_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_window_hours_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_device_time_zone_guid VARCHAR2,
		pn_result_cd IN OUT NUMBER,
		pv_schedule IN OUT DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE
	)
    IS
		lv_current_schedule DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
		ln_current_interval NUMBER;
		ln_min_allowed_interval NUMBER;
		lc_reoccurrence_type CHAR(1);
		ln_exists NUMBER;
		lv_normalized_time VARCHAR2(4) := GET_NORMALIZED_TIME(pn_device_id, pv_device_serial_cd, pv_normalizer_start_min_cd, pv_normalizer_end_min_cd, pv_window_start_cd, pv_window_hours_cd, pv_device_time_zone_guid);
		ln_normalized_offset NUMBER := GET_NORMALIZED_OFFSET(lv_normalized_time, pv_device_time_zone_guid);
		lv_schedule DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE := NULL;
    BEGIN
		lv_current_schedule := NVL(GET_DEVICE_SETTING(pn_device_id, pv_schedule_type), '0');
		IF lv_current_schedule = '0' THEN
			IF pv_schedule_type = PKG_CONST.DSP__NON_ACTIV_CALL_IN_SCHED THEN
				lv_schedule := PKG_CONST.REOCCURRENCE_TYPE__INTERVAL || PKG_CONST.SCHEDULE__FS || ln_normalized_offset || PKG_CONST.SCHEDULE__FS || DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EDGE_NON_ACTIVATED_CALL_IN_INTERVAL_SEC');
			ELSIF pv_schedule_type IN (PKG_CONST.DSP__ACTIVATED_CALL_IN_SCHED, PKG_CONST.DSP__SETTLEMENT_SCHEDULE) THEN
				IF lv_normalized_time IS NOT NULL THEN
					lv_schedule := PKG_CONST.REOCCURRENCE_TYPE__WEEKLY || PKG_CONST.SCHEDULE__FS || lv_normalized_time || PKG_CONST.SCHEDULE__FS || PKG_CONST.SCHEDULE__SUNDAY;
				END IF;
			END IF;
		ELSE
			lc_reoccurrence_type := SUBSTR(lv_current_schedule, 1, 1);
			IF lc_reoccurrence_type = PKG_CONST.REOCCURRENCE_TYPE__INTERVAL THEN
				ln_current_interval := NVL(DBADMIN.TO_NUMBER_OR_NULL(SUBSTR(lv_current_schedule, INSTR(lv_current_schedule, PKG_CONST.SCHEDULE__FS, -1, 1) + 1)), -1);
				ln_min_allowed_interval := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('MIN_INTERVAL_SCHEDULE_SEC'));
				IF ln_current_interval < ln_min_allowed_interval THEN
					ln_current_interval := ln_min_allowed_interval;
				END IF;
				lv_schedule := lc_reoccurrence_type || PKG_CONST.SCHEDULE__FS || ln_normalized_offset || PKG_CONST.SCHEDULE__FS || ln_current_interval;
			ELSIF lc_reoccurrence_type IN (PKG_CONST.REOCCURRENCE_TYPE__MONTHLY, PKG_CONST.REOCCURRENCE_TYPE__WEEKLY) THEN
				IF lv_normalized_time IS NOT NULL THEN
					lv_schedule := lc_reoccurrence_type || PKG_CONST.SCHEDULE__FS || lv_normalized_time || PKG_CONST.SCHEDULE__FS || SUBSTR(lv_current_schedule, INSTR(lv_current_schedule, PKG_CONST.SCHEDULE__FS, -1, 1) + 1);
				END IF;
			ELSE
				IF lv_normalized_time IS NOT NULL THEN
					lv_schedule := PKG_CONST.REOCCURRENCE_TYPE__DAILY || PKG_CONST.SCHEDULE__FS || lv_normalized_time;
				END IF;
			END IF;
		END IF;
		IF lv_schedule IS NOT NULL AND lv_schedule != lv_current_schedule THEN
			SP_UPSERT_DEVICE_SETTING(pn_device_id, pv_schedule_type, lv_schedule, ln_exists);
			pn_result_cd := 1;
			pv_schedule := lv_schedule;
		END IF;		
    END;
  
  PROCEDURE SP_NORMALIZE_CALL_IN_TIME(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pn_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE,
		pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
		pv_device_time_zone_guid VARCHAR2,
		pn_result_cd OUT NUMBER,
		pv_activated_call_in_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_non_activ_call_in_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_settlement_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_dex_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE
	)
	IS
    ln_command_id       ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE;
    lv_data_type        ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE;
    ll_command          ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE;
  BEGIN
    SP_NORMALIZE_CALL_IN_TIME(
      pn_device_id, 
      pn_device_type_id, 
      pv_device_serial_cd, 
      pv_device_time_zone_guid, 
      pn_result_cd, 
      pv_activated_call_in_schedule, 
      pv_non_activ_call_in_schedule, 
      pv_settlement_schedule, 
      pv_dex_schedule,
      ln_command_id, 
      lv_data_type, 
      ll_command);
  END; -- SP_NORMALIZE_CALL_IN_TIME stub
  
  /**
    * r29 Call-in normalization version
    */ 
	PROCEDURE SP_NORMALIZE_CALL_IN_TIME(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pn_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE,
		pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
		pv_device_time_zone_guid VARCHAR2,
		pn_result_cd OUT NUMBER,
		pv_activated_call_in_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_non_activ_call_in_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_settlement_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_dex_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
		pv_data_type OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
		pl_command OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE
	)
	IS
		lv_return_msg VARCHAR2(2048);
		ln_exists NUMBER;
		ln_command_id ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE;
		lv_data_type ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE;
		ll_command ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE;
		ln_result_cd NUMBER;
		lv_current_value DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
		lv_new_value DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
		ln_current_value NUMBER;
		ln_threshold NUMBER;
		ln_device_id DEVICE.DEVICE_ID%TYPE;
	BEGIN
		-- normalize device call-in time to avoid load peaks
		pn_result_cd := 0;
		pv_activated_call_in_schedule := PKG_CONST.ASCII__NUL;
		pv_non_activ_call_in_schedule := PKG_CONST.ASCII__NUL;
		pv_settlement_schedule := PKG_CONST.ASCII__NUL;
		pv_dex_schedule := PKG_CONST.ASCII__NUL;
		
		SELECT MAX(dla.device_id)
		INTO ln_device_id
		FROM device.device d
		JOIN device.device_last_active dla ON d.device_name = dla.device_name
		WHERE d.device_id = pn_device_id;
		
		IF pn_device_type_id IN (PKG_CONST.DEVICE_TYPE__G4, PKG_CONST.DEVICE_TYPE__GX) THEN
			pv_activated_call_in_schedule := GET_NORMALIZED_TIME(ln_device_id, pv_device_serial_cd, 'GX_CALL_IN_NORMALIZER_START_MIN', 'GX_CALL_IN_NORMALIZER_END_MIN',
				'CALL_IN_TIME_WINDOW_START', 'CALL_IN_TIME_WINDOW_HOURS', pv_device_time_zone_guid);
            IF pv_activated_call_in_schedule IS NOT NULL THEN
				IF pv_activated_call_in_schedule BETWEEN '0000' AND '0004' THEN
					pv_activated_call_in_schedule := '001' || SUBSTR(pv_activated_call_in_schedule, 4, 1);
				ELSIF pv_activated_call_in_schedule BETWEEN '2356' AND '2359' THEN
					pv_activated_call_in_schedule := '234' || SUBSTR(pv_activated_call_in_schedule, 4, 1);
				END IF;
				SP_UPDATE_CONFIG_AND_RETURN(ln_device_id, 172, pv_activated_call_in_schedule, 'H', pn_device_type_id, 0, pn_result_cd, lv_return_msg, pn_command_id, pv_data_type, pl_command);
			END IF;
			
			lv_current_value := GET_DEVICE_SETTING(ln_device_id, '358');
			pv_dex_schedule := GET_NORMALIZED_TIME(ln_device_id, pv_device_serial_cd,
				CASE WHEN lv_current_value = 'FFFF' THEN NULL ELSE 'GX_CALL_IN_NORMALIZER_START_MIN' END,
				CASE WHEN lv_current_value = 'FFFF' THEN NULL ELSE 'GX_CALL_IN_NORMALIZER_END_MIN' END,
				'CALL_IN_TIME_WINDOW_START_DEX', 'CALL_IN_TIME_WINDOW_HOURS_DEX', pv_device_time_zone_guid);
			IF pv_dex_schedule IS NOT NULL THEN
				IF pn_command_id IS NULL THEN
					SP_UPDATE_CONFIG_AND_RETURN(ln_device_id, 358, pv_dex_schedule, 'H', pn_device_type_id, 0, pn_result_cd, lv_return_msg, pn_command_id, pv_data_type, pl_command);
				ELSE
					SP_UPDATE_CONFIG_AND_RETURN(ln_device_id, 358, pv_dex_schedule, 'H', pn_device_type_id, 0, ln_result_cd, lv_return_msg, ln_command_id, lv_data_type, ll_command);
				END IF;
			END IF;
			
			--DEX Read and Send Interval Minutes
			lv_current_value := GET_DEVICE_SETTING(ln_device_id, '244');
			IF lv_current_value != '0000' THEN
				ln_current_value := NVL(DBADMIN.TO_NUMBER_OR_NULL(lv_current_value), 0);
				ln_threshold := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('MIN_DEX_READ_AND_SEND_INTERVAL_MIN'));
				IF ln_current_value < ln_threshold THEN
					lv_new_value := LPAD(ln_threshold, 4, '0');
					IF pn_command_id IS NULL THEN
						SP_UPDATE_CONFIG_AND_RETURN(ln_device_id, 244, lv_new_value, 'H', pn_device_type_id, 0, pn_result_cd, lv_return_msg, pn_command_id, pv_data_type, pl_command);
					ELSE
						SP_UPDATE_CONFIG_AND_RETURN(ln_device_id, 244, lv_new_value, 'H', pn_device_type_id, 0, ln_result_cd, lv_return_msg, ln_command_id, lv_data_type, ll_command);
					END IF;
				END IF;
			END IF;
		ELSIF pn_device_type_id = PKG_CONST.DEVICE_TYPE__MEI THEN
			pv_activated_call_in_schedule := GET_NORMALIZED_TIME(ln_device_id, pv_device_serial_cd, 'MEI_CALL_IN_NORMALIZER_START_MIN', 'MEI_CALL_IN_NORMALIZER_END_MIN',
				'CALL_IN_TIME_WINDOW_START', 'CALL_IN_TIME_WINDOW_HOURS', pv_device_time_zone_guid);
            IF pv_activated_call_in_schedule IS NOT NULL THEN
				lv_new_value := LPAD(TO_NUMBER(SUBSTR(pv_activated_call_in_schedule, 1, 2)) * 60 + TO_NUMBER(SUBSTR(pv_activated_call_in_schedule, 3, 2)), 4, '0');
				SP_UPDATE_CONFIG_AND_RETURN(ln_device_id, 17, lv_new_value, 'H', pn_device_type_id, 0, pn_result_cd, lv_return_msg, pn_command_id, pv_data_type, pl_command);
			END IF;			
		ELSIF pn_device_type_id = PKG_CONST.DEVICE_TYPE__EDGE THEN
			NORMALIZE_SCHEDULE(ln_device_id, pv_device_serial_cd, PKG_CONST.DSP__ACTIVATED_CALL_IN_SCHED, 'EDGE_CALL_IN_NORMALIZER_START_MIN', 'EDGE_CALL_IN_NORMALIZER_END_MIN', 
				'CALL_IN_TIME_WINDOW_START_ACTIVATED', 'CALL_IN_TIME_WINDOW_HOURS_ACTIVATED', pv_device_time_zone_guid, pn_result_cd, pv_activated_call_in_schedule);
			NORMALIZE_SCHEDULE(ln_device_id, pv_device_serial_cd, PKG_CONST.DSP__NON_ACTIV_CALL_IN_SCHED, 'EDGE_CALL_IN_NORMALIZER_START_MIN', 'EDGE_CALL_IN_NORMALIZER_END_MIN', 
				'CALL_IN_TIME_WINDOW_START_NON_ACTIVATED', 'CALL_IN_TIME_WINDOW_HOURS_NON_ACTIVATED', pv_device_time_zone_guid, pn_result_cd, pv_non_activ_call_in_schedule);
			NORMALIZE_SCHEDULE(ln_device_id, pv_device_serial_cd, PKG_CONST.DSP__SETTLEMENT_SCHEDULE, 'EDGE_CALL_IN_NORMALIZER_START_MIN', 'EDGE_CALL_IN_NORMALIZER_END_MIN', 
				'CALL_IN_TIME_WINDOW_START_SETTLEMENT', 'CALL_IN_TIME_WINDOW_HOURS_SETTLEMENT', pv_device_time_zone_guid, pn_result_cd, pv_settlement_schedule);

			IF DBADMIN.PKG_GLOBAL.GET_APP_SETTING('NORMALIZE_EDGE_DEX_SCHEDULE_FLAG') = 'Y'
				AND NVL(GET_DEVICE_SETTING(ln_device_id, PKG_CONST.DSP__VMC_INTERFACE_TYPE), PKG_CONST.VMC_INTERFACE__STANDARD_MDB) = PKG_CONST.VMC_INTERFACE__STANDARD_MDB THEN
				NORMALIZE_SCHEDULE(ln_device_id, pv_device_serial_cd, PKG_CONST.DSP__DEX_SCHEDULE, 'EDGE_CALL_IN_NORMALIZER_START_MIN', 'EDGE_CALL_IN_NORMALIZER_END_MIN', 
					'CALL_IN_TIME_WINDOW_START_DEX', 'CALL_IN_TIME_WINDOW_HOURS_DEX', pv_device_time_zone_guid, pn_result_cd, pv_dex_schedule);
			END IF;
		END IF;
		
		UPDATE DEVICE.DEVICE
		SET CALL_IN_TIME_NORMALIZED_TS = SYSDATE
		WHERE DEVICE_ID = ln_device_id;
	END;
         
  /**
    * Part of the r29 version of SP_NEXT_PENDING_COMMAND
    * This non-public procedure adds commands for ESUDS Diagnostics, if existing diagnostics are beyond 16 hours old.
    */
    PROCEDURE SP_NPC_HELPER_ESUDS_DIAG(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pv_data_type OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pv_session_attributes IN OUT VARCHAR2, -- contains character 'E' if ESUDS diagnostics has been requested previously
        pn_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
        pn_commands_inserted OUT NUMBER,
        pl_command OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE
    )
    IS
        ld_oldest_diag DATE;
    BEGIN
		IF pn_device_type_id IN (PKG_CONST.DEVICE_TYPE__ESUDS) AND INSTR(NVL(pv_session_attributes,'-'),'E') = 0 THEN        
            pv_session_attributes := NVL(pv_session_attributes,'') || 'E';       
            -- if any records for the given device are old, (no records = old records)       
            SELECT MIN(MOST_RECENT_PORT_DIAG_DATE) 
              INTO ld_oldest_diag
              FROM (
                  SELECT d.device_id, h.host_port_num, MAX(NVL(hds.host_diag_last_reported_ts, MIN_DATE)) AS MOST_RECENT_PORT_DIAG_DATE
                    FROM device d  
                    LEFT OUTER JOIN host h ON (d.device_id = h.device_id) 
                    LEFT OUTER JOIN host_diag_status hds ON (hds.host_id = h.host_id) 
                    WHERE d.device_type_id = PKG_CONST.DEVICE_TYPE__ESUDS
                    AND d.device_name = pv_device_name
                    AND d.device_active_yn_flag = 'Y'
                    AND NVL(h.host_active_yn_flag,'Y') = 'Y'
                    AND h.host_port_num <> 0
                    GROUP BY d.device_id, h.host_port_num            
              );
    
            IF ld_oldest_diag < SYSDATE - 1 THEN           
                pv_data_type := '9A61';
                pl_command := '';
                UPSERT_PENDING_COMMAND(pv_device_name, pv_data_type, pl_command, 'S', 999, pn_commands_inserted, pn_command_id);                                   
            END IF;
		END IF;  
    END; -- SP_NPC_HELPER_ESUDS_DIAGNOSTICS

  /**
    * Part of the r29 version of SP_NEXT_PENDING_COMMAND
    * This non-public procedure adds commands for GPRS configuration files, if needed.
    */
    PROCEDURE SP_NPC_HELPER_MODEM(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pv_data_type OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pv_session_attributes IN OUT VARCHAR2, -- contains character 'C' if configuration test has been requested previously, R if configuration requested       
        pn_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
        pn_commands_inserted OUT NUMBER,
        pl_command OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE
    )
    IS
        ld_last_update_ts DATE;
    BEGIN
        -- For Gx and MEI, test to see if modem data is outdated by 7 days. If so, send the appropriate message (both happen to be 87)	
        -- Some G4's only send garbage for modem info, so don't keep requesting it for G4 (PKG_CONST.DEVICE_TYPE__G4)
    	IF pn_device_type_id IN (PKG_CONST.DEVICE_TYPE__GX, PKG_CONST.DEVICE_TYPE__MEI) AND INSTR(NVL(pv_session_attributes,'-'),'C') = 0 THEN             
            pv_session_attributes := NVL(pv_session_attributes,'') || 'C';
            SELECT NVL(MAX(H.LAST_UPDATED_TS), MIN_DATE)
              INTO ld_last_update_ts
              FROM DEVICE.DEVICE D
              JOIN DEVICE.HOST H ON D.DEVICE_ID = H.DEVICE_ID
             WHERE D.DEVICE_NAME = pv_device_name
               AND D.DEVICE_ACTIVE_YN_FLAG = 'Y'
               AND H.HOST_PORT_NUM = 2;
            IF ld_last_update_ts < SYSDATE - NVL(TO_NUMBER_OR_NULL(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DEVICE_MODEM_INFO_REFRESH_HR')) / 24, 7) THEN				
                pv_data_type := '87';					
                IF (pn_device_type_id = PKG_CONST.DEVICE_TYPE__MEI) THEN 
                    pl_command := '4200000000000000FF';							
                ELSIF (pn_device_type_id = PKG_CONST.DEVICE_TYPE__GX OR pn_device_type_id = PKG_CONST.DEVICE_TYPE__G4) THEN 
                    pl_command := '4400802C0000000190';
                END IF;
				pv_session_attributes := NVL(pv_session_attributes,'') || 'R';
                UPSERT_PENDING_COMMAND(pv_device_name, pv_data_type, pl_command, 'S', 999, pn_commands_inserted, pn_command_id);
            END IF;
        END IF;
    END;

  /**
    * Part of the r29 version of SP_NEXT_PENDING_COMMAND
    * This non-public procedure adds commands for G4 configuration files, if existing ones are stale.
    */
    PROCEDURE SP_NPC_HELPER_STALE_CONFIG(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pv_data_type OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pv_session_attributes IN OUT VARCHAR2, -- contains character 'G' if GX configuration has been requested previously
        pn_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
        pn_commands_inserted OUT NUMBER,
        pl_command OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE
    )
    IS
        ld_last_config_ts DATE;
    BEGIN
        -- For MEI/Gx test age of configuration data
		IF pn_device_type_id IN (PKG_CONST.DEVICE_TYPE__GX, PKG_CONST.DEVICE_TYPE__MEI, PKG_CONST.DEVICE_TYPE__G4) AND INSTR(NVL(pv_session_attributes,'-'),'G') = 0 THEN              
	        pv_session_attributes := NVL(pv_session_attributes,'') || 'G';	        
	        IF NOT(pn_device_type_id = PKG_CONST.DEVICE_TYPE__MEI AND INSTR(NVL(pv_session_attributes,'-'), 'R') > 0 ) THEN	        	 
	        	-- Now check last configuration received timestamp 	        	
		        SELECT NVL(D.RECEIVED_CONFIG_FILE_TS, MIN_DATE)
		          INTO ld_last_config_ts
		          FROM DEVICE.DEVICE D 
		         WHERE D.DEVICE_NAME = pv_device_name 
		           AND D.DEVICE_ACTIVE_YN_FLAG = 'Y';
                IF ld_last_config_ts < SYSDATE - NVL(TO_NUMBER_OR_NULL(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DEVICE_CONFIG_REFRESH_HR')) / 24, 30) THEN				
                    pv_data_type := '87';
					-- same command for all devices handled by this procedure					
					pl_command := '4200000000000000FF';		
					pv_session_attributes := NVL(pv_session_attributes,'') || 'R';	
					UPSERT_PENDING_COMMAND(pv_device_name, pv_data_type, pl_command, 'S', 999, pn_commands_inserted, pn_command_id);	                						
				END IF; -- actual test	
			END IF; -- 'R' flag			
    	END IF; -- 'G' flag    	
    END; -- SP_NPC_HELPER_STALE_CONFIG
    
  /**
    * Part of the r29 version of SP_NEXT_PENDING_COMMAND
    * This non-public procedure adds commands to set the Gx verbosity flag 
    * on a non-remotely updatable device.
    */
    PROCEDURE SP_NPC_HELPER_PHILLY_COKE(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pv_data_type OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pv_session_attributes IN OUT VARCHAR2, -- contains character 'P' if ESUDS diagnostics has been requested previously
        pn_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
        pv_firmware_version IN DEVICE.FIRMWARE_VERSION%TYPE,
        pn_commands_inserted OUT NUMBER,
        pl_command OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE
    )
    IS
        l_is_philly_coke NUMBER;
    BEGIN
        -- For Gx test age of configuration data
        IF pn_device_type_id IN (PKG_CONST.DEVICE_TYPE__GX) AND INSTR(NVL(pv_session_attributes,'-'),'P') = 0 AND REGEXP_LIKE(pv_firmware_version, 'USA-G5[0-9] ?[vV]4\.2\.(0|1[A-S]).*') THEN             
            pv_session_attributes := NVL(pv_session_attributes,'') || 'P';      
            pv_data_type := '88';
            pl_command := '4400807FE001';
            UPSERT_PENDING_COMMAND(pv_device_name, pv_data_type, pl_command, 'S', 2, pn_commands_inserted, pn_command_id);
		END IF;   
    END; -- SP_NPC_HELPER_PHILLY_COKE

  /**
    * r29 version of SP_NEXT_PENDING_COMMAND
    * This is the feed of commands from the UI tools to the App Layer and thus the devices
    */
    PROCEDURE SP_NEXT_PENDING_COMMAND(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pv_data_type OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pr_command_bytes OUT RAW,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pv_file_transfer_name OUT FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_id OUT FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pl_file_transfer_content OUT FILE_TRANSFER.FILE_TRANSFER_CONTENT%TYPE,
        pn_file_transfer_group_num OUT DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
        pn_file_transfer_pkt_size OUT DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
        pd_file_transfer_created_ts OUT DEVICE_FILE_TRANSFER.CREATED_TS%TYPE,
        pn_max_execute_order ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE DEFAULT 999,
        pn_priority_command_id ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE DEFAULT NULL,
        pv_session_attributes IN OUT VARCHAR2, -- contains character 'C' if configuration has been requested previously
		pv_global_session_cd VARCHAR2 DEFAULT NULL,
		pn_update_status NUMBER DEFAULT 0,
        pc_v4_messages CHAR DEFAULT '?'
        )
    IS
        ll_command ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE;
		ln_device_id DEVICE.DEVICE_ID%TYPE;
        ln_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE;
        lv_firmware_version DEVICE.FIRMWARE_VERSION%TYPE;
		lv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE;
		ld_call_in_time_normalized_ts DEVICE.CALL_IN_TIME_NORMALIZED_TS%TYPE;
		lv_device_time_zone_guid VARCHAR2(60);
		ln_result_cd NUMBER;
		lv_activated_call_in_schedule DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
		lv_non_activ_call_in_schedule DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
		lv_settlement_schedule DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
		lv_dex_schedule DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
		ln_command_id ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE;
		ln_file_transfer_id FILE_TRANSFER.FILE_TRANSFER_ID%TYPE;
		l_addr NUMBER;
        l_len NUMBER;
        l_pos NUMBER;
        l_commands_inserted NUMBER;
		ln_sending_file_cnt NUMBER;
		ln_sending_file_limit NUMBER := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DOWNLOAD_LIMIT_APP_UPGRADE'));
		ln_max_file_download_attempts NUMBER(3,0) := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('MAX_FILE_DOWNLOAD_ATTEMPTS'));
		lv_file_content_hex VARCHAR2(1024);
		lv_data_type ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE;
		lv_command ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE;
		lv_last_pending_cmd_session_cd VARCHAR2(100);
		lc_upgrade_throttling_enabled COMM_METHOD.UPGRADE_THROTTLING_ENABLED%TYPE;
		ln_upgrade_cap_bytes COMM_METHOD.UPGRADE_CAP_BYTES%TYPE;
		ln_billing_period_start COMM_METHOD.BILLING_PERIOD_START%TYPE;
		ld_billing_period_start_ts DATE;
		ln_average_bytes_transferred DATA_TRANSFER.BYTES_RECEIVED%TYPE := 0;
		ln_active_billing_devices DATA_TRANSFER.DEVICE_COUNT%TYPE;
		lc_comm_method_cd DEVICE.COMM_METHOD_CD%TYPE;
		ln_fw_upg_max_step_attempts NUMBER;
		ln_current_fw_upg_step_attempt DEVICE_FIRMWARE_UPGRADE.CURRENT_FW_UPG_STEP_ATTEMPT%TYPE;
		ln_device_file_transfer_id DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_ID%TYPE := 0;
		ln_device_fw_upg_id DEVICE_FIRMWARE_UPGRADE.DEVICE_FIRMWARE_UPGRADE_ID%TYPE;
		ln_firmware_upgrade_id FIRMWARE_UPGRADE.FIRMWARE_UPGRADE_ID%TYPE;
		ln_complete_fw_upg_step_id DEVICE_FIRMWARE_UPGRADE.COMPLETE_FW_UPG_STEP_ID%TYPE;
		lv_error_message DEVICE_FIRMWARE_UPGRADE.ERROR_MESSAGE%TYPE;
		ln_step_id FIRMWARE_UPGRADE_STEP.FIRMWARE_UPGRADE_STEP_ID%TYPE;
		ln_step_number FIRMWARE_UPGRADE_STEP.STEP_NUMBER%TYPE;
		lv_step_name FIRMWARE_UPGRADE_STEP.FIRMWARE_UPGRADE_STEP_NAME%TYPE;
		ln_file_id FILE_TRANSFER.FILE_TRANSFER_ID%TYPE;
		lv_peek_command ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE;
		ln_file_size NUMBER;
		ln_packet_size NUMBER;
		lv_branch_param_cd FIRMWARE_UPGRADE_STEP.BRANCH_PARAM_CD%TYPE;
		lv_branch_value_regex FIRMWARE_UPGRADE_STEP.BRANCH_VALUE_REGEX%TYPE;
		lv_target_param_cd FIRMWARE_UPGRADE_STEP.TARGET_PARAM_CD%TYPE;
		lv_target_version FIRMWARE_UPGRADE_STEP.TARGET_VERSION%TYPE;
		lv_target_value_regex FIRMWARE_UPGRADE_STEP.TARGET_VALUE_REGEX%TYPE;
		lv_current_version DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
		lv_current_value VARCHAR2(255);
		lv_app_setting_value ENGINE.APP_SETTING.APP_SETTING_VALUE%TYPE;
		ln_requirement_met NUMBER;
		lv_new_firmware_version VARCHAR2(20);
		ln_cancel_command NUMBER(1,0);
		ln_loop_count NUMBER := 0;
		ld_sysdate DATE := SYSDATE;
    BEGIN
		SELECT D.DEVICE_ID, D.DEVICE_TYPE_ID, D.DEVICE_SERIAL_CD, D.CALL_IN_TIME_NORMALIZED_TS, TZ.TIME_ZONE_GUID, D.FIRMWARE_VERSION,
			D.COMM_METHOD_CD, CM.UPGRADE_THROTTLING_ENABLED, CM.UPGRADE_CAP_BYTES, CM.BILLING_PERIOD_START
		INTO ln_device_id, ln_device_type_id, lv_device_serial_cd, ld_call_in_time_normalized_ts, lv_device_time_zone_guid, lv_firmware_version,
			lc_comm_method_cd, lc_upgrade_throttling_enabled, ln_upgrade_cap_bytes, ln_billing_period_start
		FROM DEVICE.DEVICE D
		JOIN PSS.POS P ON D.DEVICE_ID = P.DEVICE_ID
		JOIN LOCATION.LOCATION L ON P.LOCATION_ID = L.LOCATION_ID
		JOIN LOCATION.TIME_ZONE TZ ON L.LOCATION_TIME_ZONE_CD = TZ.TIME_ZONE_CD
		LEFT OUTER JOIN DEVICE.COMM_METHOD CM ON D.COMM_METHOD_CD = CM.COMM_METHOD_CD
		WHERE D.DEVICE_NAME = pv_device_name
			AND D.DEVICE_ACTIVE_YN_FLAG = 'Y';
	
		IF lv_device_serial_cd LIKE 'K3%' AND pn_update_status = PKG_CONST.UPDATE_STATUS__NORMAL AND pv_global_session_cd IS NOT NULL THEN
			BEGIN
				SELECT TO_CHAR(LAST_PENDING_COMMAND_ID), LAST_PENDING_CMD_SESSION_CD
				INTO ln_command_id, lv_last_pending_cmd_session_cd
				FROM DEVICE.DEVICE_INFO
				WHERE DEVICE_NAME = pv_device_name;
				
				IF ln_command_id IS NOT NULL THEN
					UPDATE ENGINE.MACHINE_CMD_PENDING
					SET EXECUTE_CD = 'A', GLOBAL_SESSION_CD = pv_global_session_cd
					WHERE MACHINE_COMMAND_PENDING_ID = ln_command_id
                      AND REGEXP_LIKE(DATA_TYPE, DECODE(pc_v4_messages, '?', '^[0-9A-Fa-f]{2,4}$', 'N', '^[0-9A-Ba-b][0-9A-Fa-f]{1,3}$', 'Y', '^C[0-9A-Fa-f]{1,3}$'))
					RETURNING DATA_TYPE, COMMAND, DEVICE_FILE_TRANSFER_ID INTO lv_data_type, lv_command, ln_device_file_transfer_id;
					
					IF ln_device_file_transfer_id > 0 THEN		  		
						UPDATE DEVICE.DEVICE_FILE_TRANSFER
						SET DEVICE_FILE_TRANSFER_STATUS_CD = 1,
							DEVICE_FILE_TRANSFER_TS = SYSDATE,
							GLOBAL_SESSION_CD = lv_last_pending_cmd_session_cd
						WHERE DEVICE_FILE_TRANSFER_ID = ln_device_file_transfer_id; 
					END IF;
					
					UPDATE DEVICE.DEVICE_INFO
					SET LAST_PENDING_COMMAND_ID = NULL,
						LAST_PENDING_CMD_SESSION_CD = NULL
					WHERE DEVICE_NAME = pv_device_name;
				END IF;
			EXCEPTION
				WHEN NO_DATA_FOUND THEN
					NULL;
			END;
		END IF;
		
		LOOP
			pn_command_id := NULL;
			pv_data_type := NULL;
			ll_command := NULL;
			ln_device_file_transfer_id := 0;
			ln_device_fw_upg_id := 0;
		
			ln_loop_count := ln_loop_count + 1;
			IF ln_loop_count > 100 THEN
				-- make sure we don't enter an infinite loop
				EXIT;
			END IF;
	
			-- throttle the number of concurrent app upgrade file downloads
			SELECT COUNT(1) INTO ln_sending_file_cnt
			FROM engine.machine_cmd_pending mcp
			WHERE mcp.execute_cd = 'S' AND mcp.execute_date > SYSDATE - 15/1440 AND (
				mcp.device_file_transfer_id > 0 AND EXISTS (
					SELECT 1 FROM device.device_file_transfer dft
					JOIN device.file_transfer ft ON dft.file_transfer_id = ft.file_transfer_id
					WHERE dft.device_file_transfer_id = mcp.device_file_transfer_id
						AND ft.file_transfer_type_cd IN (PKG_CONST.FILE_TYPE__APP_UPGRADE, PKG_CONST.FILE_TYPE__CARD_READER_APP)
				)
				OR mcp.data_type = 'FW_UPG' AND EXISTS (
					SELECT 1 FROM DEVICE.DEVICE_FIRMWARE_UPGRADE DFU
					WHERE DFU.DEVICE_FIRMWARE_UPGRADE_ID = mcp.device_firmware_upgrade_id
				)
			);
				
			IF lc_upgrade_throttling_enabled = 'Y' AND ln_sending_file_cnt < ln_sending_file_limit THEN
				ld_billing_period_start_ts := TRUNC(ld_sysdate, 'MONTH') + ln_billing_period_start - 1;
				IF ld_billing_period_start_ts > ld_sysdate THEN
					ld_billing_period_start_ts := ADD_MONTHS(TRUNC(ld_sysdate, 'MONTH'), -1) + ln_billing_period_start - 1;
				END IF;
				
				SELECT NVL(MAX(DEVICE_COUNT), 0)
				INTO ln_active_billing_devices
				FROM DEVICE.DATA_TRANSFER
				WHERE DATA_TRANSFER_TS >= ld_billing_period_start_ts
					AND COMM_METHOD_CD = lc_comm_method_cd;
					
				IF ln_active_billing_devices > 0 THEN
					SELECT (SUM(BYTES_RECEIVED) + SUM(BYTES_SENT)) / ln_active_billing_devices
					INTO ln_average_bytes_transferred
					FROM DEVICE.DATA_TRANSFER
					WHERE DATA_TRANSFER_TS >= ld_billing_period_start_ts
						AND COMM_METHOD_CD = lc_comm_method_cd;
				END IF;
			END IF;
		
			IF ln_sending_file_cnt >= ln_sending_file_limit OR lc_upgrade_throttling_enabled = 'Y' AND ln_average_bytes_transferred >= ln_upgrade_cap_bytes THEN
				-- mark commands Capped or Throttled
				UPDATE ENGINE.MACHINE_CMD_PENDING MCP
				SET EXECUTE_CD = CASE WHEN lc_upgrade_throttling_enabled = 'Y' AND ln_average_bytes_transferred >= ln_upgrade_cap_bytes THEN 'E'
					ELSE 'T' END,
					EXECUTE_DATE = SYSDATE, GLOBAL_SESSION_CD = pv_global_session_cd
				WHERE MCP.MACHINE_ID = pv_device_name AND EXECUTE_CD IN ('P', 'S') AND MCP.DEVICE_FILE_TRANSFER_ID > 0 AND (
					MCP.DATA_TYPE != 'C7' AND EXISTS (
						SELECT 1 FROM DEVICE.DEVICE_FILE_TRANSFER DFT
						JOIN DEVICE.FILE_TRANSFER FT ON DFT.FILE_TRANSFER_ID = FT.FILE_TRANSFER_ID
						WHERE DFT.DEVICE_FILE_TRANSFER_ID = MCP.DEVICE_FILE_TRANSFER_ID
							AND FT.FILE_TRANSFER_TYPE_CD IN (PKG_CONST.FILE_TYPE__APP_UPGRADE, PKG_CONST.FILE_TYPE__CARD_READER_APP)
					)
					OR MCP.DATA_TYPE = 'FW_UPG' AND EXISTS (
						SELECT 1 FROM DEVICE.DEVICE_FIRMWARE_UPGRADE DFU
						WHERE DFU.DEVICE_FIRMWARE_UPGRADE_ID = MCP.DEVICE_FIRMWARE_UPGRADE_ID
					)
				);
			ELSE
				-- mark commands Pending again
				UPDATE ENGINE.MACHINE_CMD_PENDING MCP
				SET EXECUTE_CD = 'P', EXECUTE_DATE = SYSDATE, GLOBAL_SESSION_CD = pv_global_session_cd
				WHERE MCP.MACHINE_ID = pv_device_name AND EXECUTE_CD IN ('E', 'T') AND MCP.DEVICE_FILE_TRANSFER_ID > 0 AND (
					MCP.DATA_TYPE != 'C7' AND EXISTS (
						SELECT 1 FROM DEVICE.DEVICE_FILE_TRANSFER DFT
						JOIN DEVICE.FILE_TRANSFER FT ON DFT.FILE_TRANSFER_ID = FT.FILE_TRANSFER_ID
						WHERE DFT.DEVICE_FILE_TRANSFER_ID = MCP.DEVICE_FILE_TRANSFER_ID
							AND FT.FILE_TRANSFER_TYPE_CD IN (PKG_CONST.FILE_TYPE__APP_UPGRADE, PKG_CONST.FILE_TYPE__CARD_READER_APP)
					)
					OR MCP.DATA_TYPE = 'FW_UPG' AND EXISTS (
						SELECT 1 FROM DEVICE.DEVICE_FIRMWARE_UPGRADE DFU
						WHERE DFU.DEVICE_FIRMWARE_UPGRADE_ID = MCP.DEVICE_FIRMWARE_UPGRADE_ID
					)
				);
			END IF;
			
			IF ln_max_file_download_attempts > 0 THEN
				-- cancel file downloads that exceeded MAX_FILE_DOWNLOAD_ATTEMPTS
				UPDATE ENGINE.MACHINE_CMD_PENDING
				SET EXECUTE_CD = 'C'
				WHERE MACHINE_ID = pv_device_name
					AND EXECUTE_CD IN('S', 'P')
					AND DEVICE_FILE_TRANSFER_ID IS NOT NULL
					AND ATTEMPT_COUNT >= ln_max_file_download_attempts;
			END IF;
			
			UPDATE ENGINE.MACHINE_CMD_PENDING MCP
			   SET EXECUTE_CD = 'S', EXECUTE_DATE = SYSDATE, GLOBAL_SESSION_CD = pv_global_session_cd, ATTEMPT_COUNT = COALESCE(ATTEMPT_COUNT, 0) + 1
			 WHERE MCP.MACHINE_ID = pv_device_name
			   AND MCP.EXECUTE_ORDER <= pn_max_execute_order
			   AND MCP.EXECUTE_CD IN('S', 'P')
			   AND (REGEXP_LIKE(DATA_TYPE, DECODE(pc_v4_messages, '?', '^[0-9A-Fa-f]{2,4}$', 'N', '^[0-9A-Ba-b][0-9A-Fa-f]{1,3}$', 'Y', '^C[0-9A-Fa-f]{1,3}$'))
					OR DATA_TYPE = 'FW_UPG' AND NOT EXISTS (
						SELECT 1 FROM DEVICE.DEVICE_FIRMWARE_UPGRADE DFU
						WHERE DFU.DEVICE_FIRMWARE_UPGRADE_ID = MCP.DEVICE_FIRMWARE_UPGRADE_ID
							AND DFU.LAST_GLOBAL_SESSION_CD = pv_global_session_cd
					)
			   )
			   AND NOT EXISTS(
							SELECT 1
							  FROM ENGINE.MACHINE_CMD_PENDING MCP2
							 WHERE MCP.MACHINE_ID = MCP2.MACHINE_ID
							   AND MCP2.EXECUTE_CD IN('S', 'P')
							   AND (REGEXP_LIKE(DATA_TYPE, DECODE(pc_v4_messages, '?', '^[0-9A-Fa-f]{2,4}$', 'N', '^[0-9A-Ba-b][0-9A-Fa-f]{1,3}$', 'Y', '^C[0-9A-Fa-f]{1,3}$'))
									OR DATA_TYPE = 'FW_UPG' AND NOT EXISTS (
										SELECT 1 FROM DEVICE.DEVICE_FIRMWARE_UPGRADE DFU
										WHERE DFU.DEVICE_FIRMWARE_UPGRADE_ID = MCP2.DEVICE_FIRMWARE_UPGRADE_ID
											AND DFU.LAST_GLOBAL_SESSION_CD = pv_global_session_cd
									)
							   )
							   AND MCP.MACHINE_COMMAND_PENDING_ID != MCP2.MACHINE_COMMAND_PENDING_ID
							   AND MCP.EXECUTE_ORDER >= MCP2.EXECUTE_ORDER
							   AND (MCP.EXECUTE_ORDER > MCP2.EXECUTE_ORDER
								   OR MCP2.MACHINE_COMMAND_PENDING_ID = pn_priority_command_id
								   OR (MCP.MACHINE_COMMAND_PENDING_ID != NVL(pn_priority_command_id, 0)
										AND MCP.CREATED_TS > MCP2.CREATED_TS
										OR (MCP.CREATED_TS = MCP2.CREATED_TS AND MCP.MACHINE_COMMAND_PENDING_ID > MCP2.MACHINE_COMMAND_PENDING_ID))))
				RETURNING MACHINE_COMMAND_PENDING_ID, DATA_TYPE, COMMAND, DEVICE_FILE_TRANSFER_ID, DEVICE_FIRMWARE_UPGRADE_ID
				INTO pn_command_id, pv_data_type, ll_command, ln_device_file_transfer_id, ln_device_fw_upg_id;
				
			IF pn_command_id IS NULL THEN
				IF ln_device_type_id IN (PKG_CONST.DEVICE_TYPE__G4, PKG_CONST.DEVICE_TYPE__GX, PKG_CONST.DEVICE_TYPE__EDGE, PKG_CONST.DEVICE_TYPE__MEI)
					AND NVL(ld_call_in_time_normalized_ts, MIN_DATE) < SYSDATE - TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DEVICE_CALL_IN_NORMALIZATION_INTERVAL_HR')) / 24 THEN
					SP_NORMALIZE_CALL_IN_TIME(ln_device_id, ln_device_type_id, lv_device_serial_cd, lv_device_time_zone_guid, ln_result_cd, lv_activated_call_in_schedule, lv_non_activ_call_in_schedule, lv_settlement_schedule, lv_dex_schedule, pn_command_id, pv_data_type, ll_command);
					IF ln_result_cd = 1 THEN
						IF ln_device_type_id = PKG_CONST.DEVICE_TYPE__EDGE THEN
							pv_data_type := 'C7';
							ADD_PENDING_FILE_TRANSFER(pv_device_name, pv_device_name || '-CFG-' || DBADMIN.TIMESTAMP_TO_MILLIS(SYSTIMESTAMP), 
								PKG_CONST.FILE_TYPE__PROPERTY_LIST, pv_data_type, 0, 1024, 1, ln_command_id, ln_file_transfer_id, ln_device_file_transfer_id);
							pn_command_id := ln_command_id;
							
							UPDATE DEVICE.FILE_TRANSFER
							SET FILE_TRANSFER_CONTENT = RAWTOHEX(DECODE(lv_settlement_schedule, PKG_CONST.ASCII__NUL, NULL, PKG_CONST.DSP__SETTLEMENT_SCHEDULE || '=' || lv_settlement_schedule || PKG_CONST.ASCII__LF)
							  || DECODE(lv_non_activ_call_in_schedule, PKG_CONST.ASCII__NUL, NULL, PKG_CONST.DSP__NON_ACTIV_CALL_IN_SCHED || '=' || lv_non_activ_call_in_schedule || PKG_CONST.ASCII__LF)
							  || DECODE(lv_activated_call_in_schedule, PKG_CONST.ASCII__NUL, NULL, PKG_CONST.DSP__ACTIVATED_CALL_IN_SCHED || '=' || lv_activated_call_in_schedule || PKG_CONST.ASCII__LF)
							  || DECODE(lv_dex_schedule, PKG_CONST.ASCII__NUL, NULL, PKG_CONST.DSP__DEX_SCHEDULE || '=' || lv_dex_schedule || PKG_CONST.ASCII__LF))
							WHERE FILE_TRANSFER_ID = ln_file_transfer_id;
						ELSE
							UPDATE ENGINE.MACHINE_CMD_PENDING MCP
							SET EXECUTE_CD = 'S', EXECUTE_DATE = SYSDATE, GLOBAL_SESSION_CD = pv_global_session_cd, ATTEMPT_COUNT = COALESCE(ATTEMPT_COUNT, 0) + 1
							WHERE MACHINE_COMMAND_PENDING_ID = pn_command_id;
						END IF;
					END IF;
				END IF;
				IF pn_command_id IS NULL THEN
					SP_NPC_HELPER_MODEM(pv_device_name, pn_command_id, pv_data_type, pv_session_attributes, ln_device_type_id, l_commands_inserted, ll_command); 
					IF pn_command_id IS NULL THEN
						SP_NPC_HELPER_STALE_CONFIG(pv_device_name, pn_command_id, pv_data_type, pv_session_attributes, ln_device_type_id, l_commands_inserted, ll_command);
						IF pn_command_id IS NULL THEN
							SP_NPC_HELPER_ESUDS_DIAG(pv_device_name, pn_command_id, pv_data_type, pv_session_attributes, ln_device_type_id, l_commands_inserted, ll_command);
							IF pn_command_id IS NULL THEN
								SP_NPC_HELPER_PHILLY_COKE(pv_device_name, pn_command_id, pv_data_type, pv_session_attributes, ln_device_type_id, lv_firmware_version, l_commands_inserted, ll_command);
							END IF;
						END IF;
					END IF;
				END IF;
			END IF;
			
			IF pn_command_id IS NULL THEN
				EXIT;
			END IF;
							
			IF ln_device_file_transfer_id > 0 OR pv_data_type = 'FW_UPG' THEN
				IF pv_data_type = 'FW_UPG' OR ln_device_fw_upg_id > 0 THEN
					IF pv_data_type = 'FW_UPG' THEN
						-- cancel existing firmware and bezel app upgrades
						UPDATE ENGINE.MACHINE_CMD_PENDING MCP
						SET EXECUTE_CD = 'C', GLOBAL_SESSION_CD = pv_global_session_cd
						WHERE MACHINE_ID = pv_device_name AND DEVICE_FILE_TRANSFER_ID > 0 AND EXECUTE_ORDER > 0 AND EXISTS (
							SELECT 1 FROM device.device_file_transfer dft
							JOIN device.file_transfer ft ON dft.file_transfer_id = ft.file_transfer_id
							WHERE dft.device_file_transfer_id = mcp.device_file_transfer_id
								AND ft.file_transfer_type_cd IN (PKG_CONST.FILE_TYPE__APP_UPGRADE, PKG_CONST.FILE_TYPE__CARD_READER_APP)
						);
					END IF;
				
					SELECT NVL(MAX(DFU.FIRMWARE_UPGRADE_ID), 0), NVL(MAX(DFU.COMPLETE_FW_UPG_STEP_ID), 0),
						NVL(MAX(DFU.CURRENT_FW_UPG_STEP_ID), 0), NVL(MAX(FUS.FILE_TRANSFER_ID), 0), NVL(MAX(DBMS_LOB.GETLENGTH(FT.FILE_TRANSFER_CONTENT)), 0), NVL(MAX(FUS.STEP_NUMBER), 0),
						MAX(FUS.TARGET_PARAM_CD), MAX(FUS.TARGET_VERSION), MAX(FUS.TARGET_VALUE_REGEX),
						MAX(FUS.FIRMWARE_UPGRADE_STEP_NAME), MAX(FUS.BRANCH_PARAM_CD), MAX(FUS.BRANCH_VALUE_REGEX)
					INTO ln_firmware_upgrade_id, ln_complete_fw_upg_step_id, 
						ln_step_id, ln_file_id, ln_file_size, ln_step_number, lv_target_param_cd, lv_target_version, lv_target_value_regex,
						lv_step_name, lv_branch_param_cd, lv_branch_value_regex
					FROM DEVICE.DEVICE_FIRMWARE_UPGRADE DFU
					LEFT OUTER JOIN DEVICE.FIRMWARE_UPGRADE_STEP FUS ON DFU.CURRENT_FW_UPG_STEP_ID = FUS.FIRMWARE_UPGRADE_STEP_ID AND FUS.STATUS_CD = 'A'
					LEFT OUTER JOIN DEVICE.FILE_TRANSFER FT ON FUS.FILE_TRANSFER_ID = FT.FILE_TRANSFER_ID
					WHERE DFU.DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id AND DFU.DEVICE_FW_UPG_STATUS_CD = 'I';
					
					IF pv_data_type = 'FW_UPG' THEN
						IF ln_firmware_upgrade_id < 1 THEN
							-- device firmware upgrade is no longer incomplete, cancel command
							UPDATE ENGINE.MACHINE_CMD_PENDING
							SET EXECUTE_CD = 'C'
							WHERE MACHINE_COMMAND_PENDING_ID = pn_command_id;
							CONTINUE;
						END IF;
						
						IF ln_step_id = 0 OR ln_step_id = ln_complete_fw_upg_step_id THEN
							-- no current step or current step is complete, find next step
							SELECT NVL(MAX(FIRMWARE_UPGRADE_STEP_ID), 0), NVL(MAX(FILE_TRANSFER_ID), 0), NVL(MAX(DBMS_LOB.GETLENGTH(FILE_TRANSFER_CONTENT)), 0),
								MAX(STEP_NUMBER), MAX(TARGET_PARAM_CD), MAX(TARGET_VERSION), MAX(TARGET_VALUE_REGEX),
								MAX(FIRMWARE_UPGRADE_STEP_NAME), MAX(BRANCH_PARAM_CD), MAX(BRANCH_VALUE_REGEX)
							INTO ln_step_id, ln_file_id, ln_file_size, ln_step_number, lv_target_param_cd, lv_target_version, lv_target_value_regex,
								lv_step_name, lv_branch_param_cd, lv_branch_value_regex
							FROM (
								SELECT FUS.FIRMWARE_UPGRADE_STEP_ID, FUS.FILE_TRANSFER_ID, FT.FILE_TRANSFER_CONTENT, FUS.STEP_NUMBER, FUS.TARGET_PARAM_CD, FUS.TARGET_VERSION,
									FUS.TARGET_VALUE_REGEX, FUS.FIRMWARE_UPGRADE_STEP_NAME, FUS.BRANCH_PARAM_CD, FUS.BRANCH_VALUE_REGEX
								FROM DEVICE.FIRMWARE_UPGRADE_STEP FUS
								JOIN DEVICE.FILE_TRANSFER FT ON FUS.FILE_TRANSFER_ID = FT.FILE_TRANSFER_ID
								WHERE FUS.FIRMWARE_UPGRADE_ID = ln_firmware_upgrade_id AND FUS.STATUS_CD = 'A' AND FUS.STEP_NUMBER > ln_step_number
								ORDER BY FUS.STEP_NUMBER
							) WHERE ROWNUM = 1;
							
							IF ln_step_id > 0 THEN
								UPDATE DEVICE.DEVICE_FIRMWARE_UPGRADE
								SET CURRENT_FW_UPG_STEP_ATTEMPT = 0
								WHERE DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id AND CURRENT_FW_UPG_STEP_ATTEMPT != 0;
							END IF;
						END IF;
					END IF;
					
					IF ln_step_id > 0 THEN
						IF ln_file_size <= 0 THEN
							lv_error_message := 'Size of file ID ' || ln_file_id || ' is ' || ln_file_size || ' bytes';
							UPDATE DEVICE.DEVICE_FIRMWARE_UPGRADE
							SET DEVICE_FW_UPG_STATUS_CD = 'F', DEVICE_FW_UPG_STATUS_TS = SYSDATE, 
								ERROR_MESSAGE = SUBSTR(CASE WHEN ERROR_MESSAGE LIKE '%' || lv_error_message || '%' THEN ERROR_MESSAGE
									WHEN ERROR_MESSAGE IS NULL THEN lv_error_message
									ELSE ERROR_MESSAGE || '; ' || lv_error_message END, 1, 4000)
							WHERE DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id;
							CONTINUE;
						END IF;
						
						IF pv_data_type = 'FW_UPG' THEN
							IF ln_device_type_id = PKG_CONST.DEVICE_TYPE__GX THEN
								lv_current_version := REGEXP_REPLACE(GET_DEVICE_SETTING(ln_device_id, 'Diagnostic App Rev'), 'Diag |Diagnostic = N/A', '');
								lv_app_setting_value := DBADMIN.PKG_GLOBAL.GET_APP_SETTING(CASE lc_comm_method_cd WHEN 'C' THEN 'GX_CDMA' ELSE 'GX' END || '_MIN_DIAGNOSTIC_VERSION_FOR_INTERNAL_FILE_TRANSFER_UPGRADES');
								IF REGEXP_LIKE(lv_current_version, '[0-9]') AND DBADMIN.VERSION_COMPARE(lv_current_version, lv_app_setting_value) < 0 THEN
									lv_error_message := 'Minimum required Gx diagnostic version for internal file transfers is ' || lv_app_setting_value;
									UPDATE DEVICE.DEVICE_FIRMWARE_UPGRADE
									SET DEVICE_FW_UPG_STATUS_CD = 'F', DEVICE_FW_UPG_STATUS_TS = SYSDATE,
										ERROR_MESSAGE = SUBSTR(CASE WHEN ERROR_MESSAGE LIKE '%' || lv_error_message || '%' THEN ERROR_MESSAGE
											WHEN ERROR_MESSAGE IS NULL THEN lv_error_message
											ELSE ERROR_MESSAGE || '; ' || lv_error_message END, 1, 4000)
									WHERE DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id;
									CONTINUE;
								END IF;
								
								lv_current_version := SUBSTR(GET_DEVICE_SETTING(ln_device_id, 'Firmware Version'), 9);
								lv_app_setting_value := DBADMIN.PKG_GLOBAL.GET_APP_SETTING(CASE lc_comm_method_cd WHEN 'C' THEN 'GX_CDMA' ELSE 'GX' END || '_MIN_FIRMWARE_VERSION_FOR_INTERNAL_FILE_TRANSFER_UPGRADES');
								IF REGEXP_LIKE(lv_current_version, '[0-9]') AND DBADMIN.VERSION_COMPARE(lv_current_version, lv_app_setting_value) < 0 THEN
									lv_error_message := 'Minimum required Gx firmware version for internal file transfers is ' || lv_app_setting_value;
									UPDATE DEVICE.DEVICE_FIRMWARE_UPGRADE
									SET DEVICE_FW_UPG_STATUS_CD = 'F', DEVICE_FW_UPG_STATUS_TS = SYSDATE,
										ERROR_MESSAGE = SUBSTR(CASE WHEN ERROR_MESSAGE LIKE '%' || lv_error_message || '%' THEN ERROR_MESSAGE
											WHEN ERROR_MESSAGE IS NULL THEN lv_error_message
											ELSE ERROR_MESSAGE || '; ' || lv_error_message END, 1, 4000)
									WHERE DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id;
									CONTINUE;
								END IF;
							END IF;
						
							UPDATE DEVICE.DEVICE_FIRMWARE_UPGRADE
							SET CURRENT_FW_UPG_STEP_ID = ln_step_id,
								CURRENT_FW_UPG_STEP_TS = SYSDATE,
								CURRENT_FW_UPG_STEP_ATTEMPT = CURRENT_FW_UPG_STEP_ATTEMPT + 1
							WHERE DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id
							RETURNING CURRENT_FW_UPG_STEP_ATTEMPT INTO ln_current_fw_upg_step_attempt;
						
							ln_fw_upg_max_step_attempts := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DEVICE_FIRMWARE_UPGRADE_MAX_STEP_ATTEMPTS'));
							IF ln_current_fw_upg_step_attempt > ln_fw_upg_max_step_attempts THEN
								lv_error_message := 'Exceeded maximum attempts ' || ln_fw_upg_max_step_attempts || ' for step # ' || ln_step_number || ': ' || lv_step_name;
								UPDATE DEVICE.DEVICE_FIRMWARE_UPGRADE
								SET DEVICE_FW_UPG_STATUS_CD = 'F', DEVICE_FW_UPG_STATUS_TS = SYSDATE,
									ERROR_MESSAGE = SUBSTR(CASE WHEN ERROR_MESSAGE LIKE '%' || lv_error_message || '%' THEN ERROR_MESSAGE
										WHEN ERROR_MESSAGE IS NULL THEN lv_error_message
										ELSE ERROR_MESSAGE || '; ' || lv_error_message END, 1, 4000)
								WHERE DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id;
								CONTINUE;
							END IF;
						
							IF lv_branch_param_cd IS NOT NULL AND lv_branch_value_regex IS NOT NULL THEN
								lv_current_value := NULL;
								IF lv_branch_param_cd = 'Bezel Mfgr' THEN
									SELECT MAX(he.host_equipment_mfgr)
									INTO lv_current_value
									FROM device.host h
									JOIN device.host_equipment he ON h.host_equipment_id = he.host_equipment_id
									WHERE h.device_id = ln_device_id AND h.host_type_id = 400;
								END IF;
								
								IF lv_current_value IS NULL THEN
									-- no current value, exit out of firmware upgrade until next session
									lv_error_message := lv_branch_param_cd || ' is NULL';
									UPDATE DEVICE.DEVICE_FIRMWARE_UPGRADE
									SET LAST_GLOBAL_SESSION_CD = pv_global_session_cd,
										ERROR_MESSAGE = SUBSTR(CASE WHEN ERROR_MESSAGE LIKE '%' || lv_error_message || '%' THEN ERROR_MESSAGE
											WHEN ERROR_MESSAGE IS NULL THEN lv_error_message
											ELSE ERROR_MESSAGE || '; ' || lv_error_message END, 1, 4000)
									WHERE DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id;
									CONTINUE;
								END IF;
								
								IF lv_branch_param_cd = 'Bezel Mfgr' AND lv_current_value = 'USAT' THEN
									-- this could be because the bezel firmware upgrade went wrong, try to re-send the bezel firmware file
									SELECT NVL(MAX(FILE_TRANSFER_ID), ln_file_id)
									INTO ln_file_id
									FROM (
										SELECT FT.FILE_TRANSFER_ID
										FROM ENGINE.MACHINE_CMD_PENDING_HIST MCPH
										JOIN DEVICE.DEVICE_FILE_TRANSFER DFT ON MCPH.DEVICE_FILE_TRANSFER_ID = DFT.DEVICE_FILE_TRANSFER_ID
										JOIN DEVICE.FILE_TRANSFER FT ON DFT.FILE_TRANSFER_ID = FT.FILE_TRANSFER_ID
										WHERE MACHINE_ID = pv_device_name AND MCPH.DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id
											AND DFT.DEVICE_FILE_TRANSFER_STATUS_CD = 1 AND FT.FILE_TRANSFER_TYPE_CD = PKG_CONST.FILE_TYPE__CARD_READER_APP
										ORDER BY DFT.DEVICE_FILE_TRANSFER_TS
									) WHERE ROWNUM = 1;
								ELSIF NOT REGEXP_LIKE(lv_current_value, lv_branch_value_regex) THEN
									-- current value doesn't match the branch regex, mark step as complete
									UPDATE DEVICE.DEVICE_FIRMWARE_UPGRADE
									SET COMPLETE_FW_UPG_STEP_ID = ln_step_id,
										COMPLETE_FW_UPG_STEP_TS = SYSDATE
									WHERE DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id;
									CONTINUE;
								END IF;
							END IF;
						END IF;
						
						lv_peek_command := NULL;
						IF lv_target_param_cd IS NOT NULL AND (REGEXP_LIKE(lv_target_version, '[0-9]') OR lv_target_value_regex IS NOT NULL) THEN
							IF lv_target_param_cd = 'Bezel App Rev' THEN
								SELECT MAX(hs.host_setting_value)
								INTO lv_current_value
								FROM device.host h
								JOIN device.host_setting hs ON h.host_id = hs.host_id AND hs.host_setting_parameter = 'Application Version'
								WHERE h.device_id = ln_device_id AND h.host_type_id = 400;
							ELSIF lv_target_param_cd = 'Bezel Mfgr' THEN
								SELECT MAX(he.host_equipment_mfgr)
								INTO lv_current_value
								FROM device.host h
								JOIN device.host_equipment he ON h.host_equipment_id = he.host_equipment_id
								WHERE h.device_id = ln_device_id AND h.host_type_id = 400;
							ELSE
								lv_current_value := GET_DEVICE_SETTING(ln_device_id, lv_target_param_cd);
							END IF;
														
							IF lv_target_param_cd = 'Bezel App Rev' THEN
								lv_peek_command := '4400807EB20000004C';
								lv_current_version := REGEXP_REPLACE(lv_current_value, 'EC5 GR ', '');
							ELSIF lv_target_param_cd = 'Bootloader Rev' THEN
								lv_peek_command := '4400807E9000000021';
								lv_current_version := REGEXP_REPLACE(lv_current_value, 'BtLdr |Bootloader = N/A', '');
							ELSIF lv_target_param_cd = 'Diagnostic App Rev' THEN
								lv_peek_command := '4400807E9000000021';
								lv_current_version := REGEXP_REPLACE(lv_current_value, 'Diag |Diagnostic = N/A', '');
							ELSIF lv_target_param_cd = 'Firmware Version' THEN
								lv_peek_command := '4400007F200000000F';
								lv_current_version := SUBSTR(lv_current_value, 9);
							ELSIF lv_target_param_cd = 'PTest Rev' THEN
								lv_current_version := REGEXP_REPLACE(lv_current_value, 'PTest |PTEST      = N/A', '');
							ELSE
								lv_current_version := lv_current_value;
							END IF;
							
							ln_requirement_met := 0;
							IF REGEXP_LIKE(lv_target_version, '[0-9]') AND lv_target_value_regex IS NOT NULL THEN
								IF DBADMIN.VERSION_COMPARE(lv_current_version, lv_target_version) >= 0 AND REGEXP_LIKE(lv_current_value, lv_target_value_regex) THEN
									ln_requirement_met := 1;
								END IF;
							ELSIF REGEXP_LIKE(lv_target_version, '[0-9]') THEN
								IF DBADMIN.VERSION_COMPARE(lv_current_version, lv_target_version) >= 0 THEN
									ln_requirement_met := 1;
								END IF;
							ELSIF lv_target_value_regex IS NOT NULL THEN								
								IF REGEXP_LIKE(lv_current_value, lv_target_value_regex) THEN
									ln_requirement_met := 1;
								END IF;
							END IF;
							
							IF ln_requirement_met = 1 THEN
								-- target version requirement met, mark step as complete
								UPDATE DEVICE.DEVICE_FIRMWARE_UPGRADE
								SET COMPLETE_FW_UPG_STEP_ID = ln_step_id,
									COMPLETE_FW_UPG_STEP_TS = SYSDATE
								WHERE DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id;
								
								IF pv_data_type != 'FW_UPG' AND ln_device_file_transfer_id > 0 THEN
									-- cancel current pending file transfer if target requirement is met
									UPDATE ENGINE.MACHINE_CMD_PENDING
									SET EXECUTE_CD = 'C'
									WHERE MACHINE_COMMAND_PENDING_ID = pn_command_id;
								END IF;
								
								CONTINUE;
							END IF;
						END IF;
						
						IF pv_data_type = 'FW_UPG' THEN
							IF ln_device_type_id = PKG_CONST.DEVICE_TYPE__GX THEN
								ln_packet_size := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('GX_PACKET_SIZE'));
							ELSIF ln_device_type_id = PKG_CONST.DEVICE_TYPE__KIOSK AND lv_device_serial_cd LIKE 'K3%' THEN
								ln_packet_size := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EPORT_CONNECT_PACKET_SIZE'));
							ELSE
								ln_packet_size := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DEFAULT_PACKET_SIZE'));
							END IF;
							
							IF ln_device_type_id IN (PKG_CONST.DEVICE_TYPE__GX, PKG_CONST.DEVICE_TYPE__KIOSK) THEN
								pv_data_type := 'A4';
							ELSIF ln_device_type_id = PKG_CONST.DEVICE_TYPE__EDGE THEN
								IF ln_file_size > ln_packet_size THEN
									pv_data_type := 'C8';
								ELSE
									pv_data_type := 'C7';
								END IF;
							ELSE
								pv_data_type := '7C';
							END IF;
						
							SELECT SEQ_DEVICE_FILE_TRANSFER_ID.NEXTVAL INTO ln_device_file_transfer_id FROM DUAL;
						
							INSERT INTO device.device_file_transfer(device_file_transfer_id, device_id, file_transfer_id, device_file_transfer_direct, device_file_transfer_status_cd, device_file_transfer_pkt_size)
							VALUES(ln_device_file_transfer_id, ln_device_id, ln_file_id, 'O', 0, ln_packet_size);
							
							SELECT ENGINE.SEQ_MACHINE_CMD_PENDING_ID.NEXTVAL INTO pn_command_id FROM DUAL;
							
							INSERT INTO engine.machine_cmd_pending (machine_command_pending_id, machine_id, data_type, execute_cd, execute_order, execute_date, global_session_cd, device_file_transfer_id, device_firmware_upgrade_id) 
							VALUES(pn_command_id, pv_device_name, pv_data_type, 'S', -2, SYSDATE, pv_global_session_cd, ln_device_file_transfer_id, ln_device_fw_upg_id);
							
							IF ln_device_type_id != PKG_CONST.DEVICE_TYPE__GX OR lv_target_param_cd IS NULL OR lv_target_version IS NULL AND lv_target_value_regex IS NULL THEN
								-- applayer will mark step as complete when it receives file download ack from device
								UPDATE DEVICE.DEVICE_FIRMWARE_UPGRADE
								SET CURRENT_PENDING_COMMAND_ID = pn_command_id
								WHERE DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id;
							END IF;
							
							IF lv_peek_command IS NOT NULL AND ln_device_type_id = PKG_CONST.DEVICE_TYPE__GX THEN
								INSERT INTO engine.machine_cmd_pending (machine_id, data_type, command, execute_cd, execute_order, device_firmware_upgrade_id) 
								VALUES(pv_device_name, '87', lv_peek_command, 'P', -1, ln_device_fw_upg_id);
							END IF;
						END IF;
					ELSIF pv_data_type = 'FW_UPG' THEN
						UPDATE DEVICE.DEVICE_FIRMWARE_UPGRADE
						SET DEVICE_FW_UPG_STATUS_CD = 'A', DEVICE_FW_UPG_STATUS_TS = SYSDATE
						WHERE DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id;
						
						UPDATE ENGINE.MACHINE_CMD_PENDING
						SET EXECUTE_CD = 'A'
						WHERE MACHINE_COMMAND_PENDING_ID = pn_command_id;
						
						CONTINUE;
					END IF;
				END IF;
				
				IF ln_device_file_transfer_id > 0 THEN
					SELECT MAX(ft.FILE_TRANSFER_ID), MAX(ft.FILE_TRANSFER_NAME), MAX(ft.FILE_TRANSFER_TYPE_CD),
						   MAX(dft.DEVICE_FILE_TRANSFER_GROUP_NUM), MAX(dft.DEVICE_FILE_TRANSFER_PKT_SIZE), MAX(dft.CREATED_TS)
					  INTO pn_file_transfer_id, pv_file_transfer_name, pn_file_transfer_type_id,
						   pn_file_transfer_group_num, pn_file_transfer_pkt_size, pd_file_transfer_created_ts
					  FROM DEVICE.DEVICE_FILE_TRANSFER dft
					  JOIN DEVICE.FILE_TRANSFER ft on dft.FILE_TRANSFER_ID = ft.FILE_TRANSFER_ID
					 WHERE DEVICE_FILE_TRANSFER_ID = ln_device_file_transfer_id;
					 
					IF pn_file_transfer_id IS NULL THEN
						-- device file transfer no longer exists, cancel command
						UPDATE ENGINE.MACHINE_CMD_PENDING
						SET EXECUTE_CD = 'C'
						WHERE MACHINE_COMMAND_PENDING_ID = pn_command_id;
						CONTINUE;
					END IF;
										
					IF pn_file_transfer_type_id = PKG_CONST.FILE_TYPE__APP_UPGRADE THEN
						ln_cancel_command := 0;
						IF ln_device_type_id = PKG_CONST.DEVICE_TYPE__GX THEN
							SELECT COALESCE(UTL_RAW.CAST_TO_VARCHAR2(DBMS_LOB.SUBSTR(FILE_TRANSFER_CONTENT, 19, 82)), '-')
							INTO lv_new_firmware_version
							FROM DEVICE.FILE_TRANSFER
							WHERE FILE_TRANSFER_ID = pn_file_transfer_id;						
						
							IF lv_new_firmware_version NOT LIKE 'USA-%' AND lv_new_firmware_version NOT LIKE 'Diag%' AND lv_new_firmware_version NOT LIKE 'PTest%' THEN
								ln_cancel_command := 1;
							END IF;
						ELSIF ln_device_type_id = PKG_CONST.DEVICE_TYPE__EDGE THEN
							SELECT COALESCE(UTL_RAW.CAST_TO_VARCHAR2(DBMS_LOB.SUBSTR(FILE_TRANSFER_CONTENT, 10, 93)), '-')
							INTO lv_new_firmware_version
							FROM DEVICE.FILE_TRANSFER
							WHERE FILE_TRANSFER_ID = pn_file_transfer_id;
							
							IF lv_device_serial_cd LIKE 'VJ%' THEN
								IF lv_new_firmware_version NOT LIKE '2.02.%' THEN
									ln_cancel_command := 1;
								END IF;
								IF lc_comm_method_cd = 'G' THEN
									IF DBADMIN.VERSION_COMPARE(lv_new_firmware_version, '2.02.007') < 0 THEN
										ln_cancel_command := 1;
									END IF;
								END IF;
							ELSIF lv_device_serial_cd LIKE 'EE%' THEN
								IF lc_comm_method_cd = 'C' THEN
									IF lv_new_firmware_version NOT LIKE '1.02.%' THEN
										ln_cancel_command := 1;
									END IF;
								ELSIF lc_comm_method_cd = 'G' THEN
									IF lv_new_firmware_version NOT LIKE '1.00.%' AND lv_new_firmware_version NOT LIKE '1.01.%' THEN
										ln_cancel_command := 1;
									END IF;
								END IF;
							END IF;
						END IF;
						
						IF ln_cancel_command = 1 THEN
							-- incorrect file download, cancel command
							UPDATE ENGINE.MACHINE_CMD_PENDING
							SET EXECUTE_CD = 'C'
							WHERE MACHINE_COMMAND_PENDING_ID = pn_command_id;
							CONTINUE;
						END IF;
					END IF;

					SP_GET_FILE_TRANSFER_BLOB(pn_file_transfer_id, pl_file_transfer_content);
				ELSE
					CONTINUE;
				END IF;
			ELSIF pv_data_type = '88' THEN
				l_addr := to_number(substr(ll_command, 3, 8), 'XXXXXXXX');
				IF substr(ll_command, 1, 2) = '42' THEN -- EEROM: get data from device setting
					l_len := to_number(substr(ll_command, 11, 8), 'XXXXXXXX') * 2;
		
					IF ln_device_type_id in (PKG_CONST.DEVICE_TYPE__G4, PKG_CONST.DEVICE_TYPE__GX) THEN
						  l_pos := l_addr * 2;
					ELSE
						  l_pos := l_addr;
					END IF;
		
					l_pos := l_pos * 2 + 1;
		
					SP_GET_MAP_CONFIG_FILE(ln_device_id, lv_file_content_hex);
					pr_command_bytes := UTL_RAW.CONCAT(hextoraw(substr(ll_command, 1, 10)), SUBSTR(lv_file_content_hex, l_pos, l_len));
				ELSE
					pr_command_bytes := HEXTORAW(ll_command);
				END IF;
			ELSE
				pr_command_bytes := HEXTORAW(ll_command);
			END IF;
			
			EXIT;
		END LOOP;
		
		IF pn_command_id IS NOT NULL THEN
			IF lv_device_serial_cd LIKE 'K3%' THEN
				UPDATE DEVICE.DEVICE_INFO
				SET LAST_PENDING_COMMAND_ID = TO_NUMBER(pn_command_id),
					LAST_PENDING_CMD_SESSION_CD = pv_global_session_cd
				WHERE DEVICE_NAME = pv_device_name;
				
				IF SQL%NOTFOUND THEN
					BEGIN
						INSERT INTO DEVICE.DEVICE_INFO(DEVICE_NAME, LAST_PENDING_COMMAND_ID, LAST_PENDING_CMD_SESSION_CD)
						VALUES(pv_device_name, TO_NUMBER(pn_command_id), pv_global_session_cd);
					EXCEPTION
						WHEN DUP_VAL_ON_INDEX THEN
							UPDATE DEVICE.DEVICE_INFO
							SET LAST_PENDING_COMMAND_ID = TO_NUMBER(pn_command_id),
								LAST_PENDING_CMD_SESSION_CD = pv_global_session_cd
							WHERE DEVICE_NAME = pv_device_name;
					END;
				END IF;
			END IF;
		END IF;
    END;
    
    /**
      * r29+ version that doesn't return command id but does return row count
      */
    PROCEDURE UPSERT_PENDING_COMMAND(
        pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
        pv_date_type IN ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pv_command IN ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE,
        pv_execute_order IN ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE DEFAULT 999,
        pn_rows_inserted OUT PLS_INTEGER)
    IS
        ln_command_id ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE;
    BEGIN
      UPSERT_PENDING_COMMAND(pv_device_name,pv_date_type,pv_command,'P',pv_execute_order,pn_rows_inserted,ln_command_id);
    END;
    
    /** r29+ version
      * Inserts a pending command into the table, and returns a command id,
      * IF it does not already exist.
      */      
    PROCEDURE UPSERT_PENDING_COMMAND(
        pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
        pv_data_type IN ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pv_command IN ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE,
        pv_execute_cd IN ENGINE.MACHINE_CMD_PENDING.EXECUTE_CD%TYPE,
        pv_execute_order IN ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE DEFAULT 999,
        pn_rows_inserted OUT PLS_INTEGER,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE)
    IS
	  ld_execute_date ENGINE.MACHINE_CMD_PENDING.EXECUTE_DATE%TYPE;
	  ln_cnt NUMBER;
    BEGIN
		SELECT COUNT(1)
		INTO ln_cnt
		FROM ENGINE.MACHINE_CMD_PENDING
		WHERE MACHINE_ID = pv_device_name
			AND DATA_TYPE = pv_data_type
			AND DBADMIN.PKG_UTL.EQL(COMMAND, pv_command) = 'Y';
	
		IF ln_cnt = 0 THEN
			SELECT DECODE(pv_execute_cd, 'S', SYSDATE, NULL) INTO ld_execute_date FROM DUAL;
        
			-- This isn't atomic so could result in duplicate pending commands but it's not critical to avoid duplicates only nice
			INSERT INTO ENGINE.MACHINE_CMD_PENDING(MACHINE_COMMAND_PENDING_ID, MACHINE_ID, DATA_TYPE, COMMAND, EXECUTE_CD, EXECUTE_DATE, EXECUTE_ORDER)
			VALUES(SEQ_MACHINE_CMD_PENDING_ID.NEXTVAL, pv_device_name, pv_data_type, pv_command, pv_execute_cd, ld_execute_date, pv_execute_order)
			RETURNING MACHINE_COMMAND_PENDING_ID INTO pn_command_id;
        
			pn_rows_inserted := SQL%ROWCOUNT;
		ELSE
			pn_rows_inserted := 0;
		END IF;
    END;
    
    PROCEDURE CONFIG_POKE
    (pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
     pv_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE)
    IS
        l_command ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE;
        l_file_len NUMBER;
        l_num_parts NUMBER;
        l_pos NUMBER;
        l_poke_size NUMBER:=200;
        l_len NUMBER;
		ln_rows_inserted PLS_INTEGER;
        ln_command_id ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE;
    BEGIN
        IF pv_device_type_id in (0, 1) THEN
            UPDATE ENGINE.MACHINE_CMD_PENDING
            SET EXECUTE_CD='C'
            WHERE MACHINE_ID = pv_device_name
            AND DATA_TYPE = '88'
            AND EXECUTE_ORDER > 2
            AND (EXECUTE_CD = 'P' OR (EXECUTE_CD = 'S' AND EXECUTE_DATE < (SYSDATE-(90/86400))));

            UPSERT_PENDING_COMMAND(pv_device_name, '88', '420000000000000088', 'P', -100, ln_rows_inserted, ln_command_id);
            UPSERT_PENDING_COMMAND(pv_device_name, '88', '42000000470000008E', 'P', -99, ln_rows_inserted, ln_command_id);
            UPSERT_PENDING_COMMAND(pv_device_name, '88', '42000000B20000009C', 'P', -98, ln_rows_inserted, ln_command_id);
        END IF;
    END;
    
    FUNCTION GET_DEVICE_ID_BY_NAME(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pd_effective_date DEVICE.CREATED_TS%TYPE
    )
    RETURN DEVICE.DEVICE_ID%TYPE
    IS
        ln_device_id DEVICE.DEVICE_ID%TYPE;
    BEGIN
        SELECT MAX(DEVICE_ID)
          INTO ln_device_id
          FROM DEVICE.DEVICE
         WHERE DEVICE_ACTIVE_YN_FLAG = 'Y'
           AND DEVICE_NAME = pv_device_name
           AND CREATED_TS <= pd_effective_date;
        IF ln_device_id IS NULL THEN
            SELECT MAX(DEVICE_ID)
              INTO ln_device_id
              FROM (SELECT DEVICE_ID, CREATED_TS, NVL(LEAD(CREATED_TS) OVER (PARTITION BY DEVICE_NAME ORDER BY CREATED_TS), MAX_DATE) NEXT_CREATED_TS
                      FROM DEVICE.DEVICE
                     WHERE DEVICE_NAME = pv_device_name)
             WHERE CREATED_TS <= pd_effective_date
               AND NEXT_CREATED_TS > pd_effective_date;
            IF ln_device_id IS NULL THEN
                SELECT MAX(DEVICE_ID)
                  INTO ln_device_id
                  FROM (SELECT DEVICE_ID
                          FROM DEVICE.DEVICE
                         WHERE DEVICE_NAME = pv_device_name
                           AND CREATED_TS > pd_effective_date
                         ORDER BY CREATED_TS ASC)
                 WHERE ROWNUM = 1;
            END IF;
        END IF;
        RETURN ln_device_id;        
    END;
    
    FUNCTION GET_DEVICE_ID_BY_SERIAL(
        pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
        pd_effective_date DEVICE.CREATED_TS%TYPE
    )
    RETURN DEVICE.DEVICE_ID%TYPE
    IS
        ln_device_id DEVICE.DEVICE_ID%TYPE;
    BEGIN
        SELECT MAX(DEVICE_ID)
          INTO ln_device_id
          FROM DEVICE.DEVICE
         WHERE DEVICE_ACTIVE_YN_FLAG = 'Y'
           AND DEVICE_SERIAL_CD = pv_device_serial_cd
           AND CREATED_TS <= pd_effective_date;
        IF ln_device_id IS NULL THEN
            SELECT MAX(DEVICE_ID)
              INTO ln_device_id
              FROM (SELECT DEVICE_ID, CREATED_TS, NVL(LEAD(CREATED_TS) OVER (PARTITION BY DEVICE_SERIAL_CD ORDER BY CREATED_TS), MAX_DATE) NEXT_CREATED_TS
                      FROM DEVICE.DEVICE
                     WHERE DEVICE_SERIAL_CD = pv_device_serial_cd)
             WHERE CREATED_TS <= pd_effective_date
               AND NEXT_CREATED_TS > pd_effective_date;
        END IF;
        RETURN ln_device_id;        
    END;
	
	PROCEDURE CLONE_GX_CONFIG (
		pn_source_device_id IN DEVICE.DEVICE_ID%TYPE,
		pn_target_device_id IN DEVICE.DEVICE_ID%TYPE,
		pn_part1_changed_count OUT NUMBER,
		pn_part2_changed_count OUT NUMBER,
		pn_part3_changed_count OUT NUMBER,
		pn_counters_changed_count OUT NUMBER
	)
	IS
	BEGIN
		/*
		 * 	# we are sending very specific sections of the config with the offsets below, not the whole thing for Gx
		 *	#	0 	- 135
		 *	#	142 - 283
		 *	#	356 - 511
		 */	
		MERGE INTO DEVICE.DEVICE_SETTING O
		USING (
			SELECT DS_T.DEVICE_ID, DS_T.DEVICE_SETTING_PARAMETER_CD, DS_S.DEVICE_SETTING_VALUE
			FROM DEVICE.DEVICE_SETTING DS_T
			JOIN DEVICE.DEVICE_SETTING DS_S ON DS_T.DEVICE_SETTING_PARAMETER_CD = DS_S.DEVICE_SETTING_PARAMETER_CD
			WHERE DS_T.DEVICE_ID = pn_target_device_id
				AND DS_S.DEVICE_ID = pn_source_device_id
				AND DS_S.DEVICE_SETTING_PARAMETER_CD IN (
					SELECT DEVICE_SETTING_PARAMETER_CD
					FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS
					WHERE DEVICE_TYPE_ID = 0
						AND FIELD_OFFSET BETWEEN 0 AND 135
				) AND DBADMIN.PKG_UTL.EQL(DS_S.DEVICE_SETTING_VALUE, DS_T.DEVICE_SETTING_VALUE) = 'N') N
		ON (O.DEVICE_ID = N.DEVICE_ID AND O.DEVICE_SETTING_PARAMETER_CD = N.DEVICE_SETTING_PARAMETER_CD)
		WHEN MATCHED THEN
			UPDATE
				SET O.DEVICE_SETTING_VALUE = N.DEVICE_SETTING_VALUE;
		pn_part1_changed_count := SQL%ROWCOUNT;
		
		MERGE INTO DEVICE.DEVICE_SETTING O
		USING (
			SELECT DS_T.DEVICE_ID, DS_T.DEVICE_SETTING_PARAMETER_CD, DS_S.DEVICE_SETTING_VALUE
			FROM DEVICE.DEVICE_SETTING DS_T
			JOIN DEVICE.DEVICE_SETTING DS_S ON DS_T.DEVICE_SETTING_PARAMETER_CD = DS_S.DEVICE_SETTING_PARAMETER_CD
			WHERE DS_T.DEVICE_ID = pn_target_device_id
				AND DS_S.DEVICE_ID = pn_source_device_id
				AND DS_S.DEVICE_SETTING_PARAMETER_CD IN (
					SELECT DEVICE_SETTING_PARAMETER_CD
					FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS
					WHERE DEVICE_TYPE_ID = 0
						AND FIELD_OFFSET BETWEEN 142 AND 283
				) AND DBADMIN.PKG_UTL.EQL(DS_S.DEVICE_SETTING_VALUE, DS_T.DEVICE_SETTING_VALUE) = 'N') N
		ON (O.DEVICE_ID = N.DEVICE_ID AND O.DEVICE_SETTING_PARAMETER_CD = N.DEVICE_SETTING_PARAMETER_CD)
		WHEN MATCHED THEN
			UPDATE
				SET O.DEVICE_SETTING_VALUE = N.DEVICE_SETTING_VALUE;
		pn_part2_changed_count := SQL%ROWCOUNT;
		
		MERGE INTO DEVICE.DEVICE_SETTING O
		USING (
			SELECT DS_T.DEVICE_ID, DS_T.DEVICE_SETTING_PARAMETER_CD, DS_S.DEVICE_SETTING_VALUE
			FROM DEVICE.DEVICE_SETTING DS_T
			JOIN DEVICE.DEVICE_SETTING DS_S ON DS_T.DEVICE_SETTING_PARAMETER_CD = DS_S.DEVICE_SETTING_PARAMETER_CD
			WHERE DS_T.DEVICE_ID = pn_target_device_id
				AND DS_S.DEVICE_ID = pn_source_device_id
				AND DS_S.DEVICE_SETTING_PARAMETER_CD IN (
					SELECT DEVICE_SETTING_PARAMETER_CD
					FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS
					WHERE DEVICE_TYPE_ID = 0
						AND FIELD_OFFSET BETWEEN 356 AND 511
				) AND DBADMIN.PKG_UTL.EQL(DS_S.DEVICE_SETTING_VALUE, DS_T.DEVICE_SETTING_VALUE) = 'N') N
		ON (O.DEVICE_ID = N.DEVICE_ID AND O.DEVICE_SETTING_PARAMETER_CD = N.DEVICE_SETTING_PARAMETER_CD)
		WHEN MATCHED THEN
			UPDATE
				SET O.DEVICE_SETTING_VALUE = N.DEVICE_SETTING_VALUE;
		pn_part3_changed_count := SQL%ROWCOUNT;
		
		UPDATE DEVICE.DEVICE_SETTING
		SET DEVICE_SETTING_VALUE = '00000000'
		WHERE DEVICE_ID = pn_target_device_id
			AND DEVICE_SETTING_PARAMETER_CD IN (
				SELECT DEVICE_SETTING_PARAMETER_CD
				FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS
				WHERE DEVICE_TYPE_ID = 0
					AND FIELD_OFFSET BETWEEN 320 AND 352
			) AND DEVICE_SETTING_VALUE != '00000000';
		pn_counters_changed_count := SQL%ROWCOUNT;
	END;
    
    PROCEDURE INITIALIZE_DEVICE(
        pv_device_serial_cd IN DEVICE.DEVICE_SERIAL_CD%TYPE,
        pn_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
        pc_preregistering IN CHAR, 
        pn_device_id OUT DEVICE.DEVICE_ID%TYPE,
        pv_device_name OUT DEVICE.DEVICE_NAME%TYPE,
        pv_device_type_desc OUT DEVICE_TYPE.DEVICE_TYPE_DESC%TYPE,
        pn_device_sub_type_id OUT DEVICE.DEVICE_SUB_TYPE_ID%TYPE,
        pc_prev_device_active_yn_flag OUT VARCHAR,
        pc_master_id_always_inc OUT VARCHAR,
        pn_key_gen_time OUT NUMBER,
        pc_legacy_safe_key OUT VARCHAR,
        pv_time_zone_guid OUT TIME_ZONE.TIME_ZONE_GUID%TYPE,
        pn_new_host_count OUT NUMBER,
        pn_result_cd OUT NUMBER,
        pv_error_message OUT VARCHAR2)
    IS
        ln_pos_pta_tmpl_id DEVICE_TYPE.POS_PTA_TMPL_ID%TYPE;
        lc_require_preregister DEVICE_TYPE.REQUIRE_PREREGISTER%TYPE;
        ln_prev_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE;
        ln_prev_device_sub_type_id  DEVICE.DEVICE_SUB_TYPE_ID%TYPE;
    BEGIN
        BEGIN
            SELECT DEVICE_SUB_TYPE_ID, DEVICE_SUB_TYPE_DESC, POS_PTA_TMPL_ID, REQUIRE_PREREGISTER, MASTER_ID_ALWAYS_INC
             INTO pn_device_sub_type_id, pv_device_type_desc, ln_pos_pta_tmpl_id, lc_require_preregister, pc_master_id_always_inc
             FROM 
                (SELECT 1 PRIORITY, DEVICE_SUB_TYPE_ID, DEVICE_SUB_TYPE_DESC, POS_PTA_TMPL_ID, REQUIRE_PREREGISTER, MASTER_ID_ALWAYS_INC
                  FROM DEVICE.DEVICE_SUB_TYPE
                 WHERE DEVICE_TYPE_ID = pn_device_type_id
                   AND REGEXP_LIKE(pv_device_serial_cd, DEVICE_SERIAL_CD_REGEX)
                UNION ALL 
                SELECT 2, NULL, DEVICE_TYPE_DESC, POS_PTA_TMPL_ID, REQUIRE_PREREGISTER, MASTER_ID_ALWAYS_INC
                  FROM DEVICE.DEVICE_TYPE
                 WHERE DEVICE_TYPE_ID = pn_device_type_id
                   AND REGEXP_LIKE(pv_device_serial_cd, DEVICE_TYPE_SERIAL_CD_REGEX)
             ORDER BY PRIORITY)
            WHERE ROWNUM = 1;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                pn_result_cd := PKG_CONST.RESULT__FAILURE;
                pv_error_message := 'No match for device type and serial cd';
                RETURN;
        END;
        BEGIN
            SELECT DEVICE_ID, DEVICE_NAME, DEVICE_TYPE_ID, DEVICE_SUB_TYPE_ID, CASE WHEN REGEXP_LIKE(ENCRYPTION_KEY, '^[0-9]{16}$') THEN 'Y' ELSE 'N' END, DEVICE_ACTIVE_YN_FLAG, DATE_TO_MILLIS(DEVICE_ENCR_KEY_GEN_TS)
              INTO pn_device_id, pv_device_name, ln_prev_device_type_id, ln_prev_device_sub_type_id, pc_legacy_safe_key, pc_prev_device_active_yn_flag, pn_key_gen_time
              FROM (SELECT * 
                      FROM DEVICE.DEVICE
                     WHERE DEVICE_SERIAL_CD = pv_device_serial_cd
                     ORDER BY DEVICE_ACTIVE_YN_FLAG DESC, DEVICE_ID DESC)
              WHERE ROWNUM = 1;
            IF ln_prev_device_type_id != pn_device_type_id OR NVL(ln_prev_device_sub_type_id,0) != NVL(pn_device_sub_type_id, 0) THEN
                UPDATE DEVICE.DEVICE
                   SET DEVICE_TYPE_ID = pn_device_type_id,
                       DEVICE_SUB_TYPE_ID = pn_device_sub_type_id
                 WHERE DEVICE_ID = pn_device_id;
            END IF;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                IF pc_preregistering != 'Y' AND lc_require_preregister = 'Y' THEN
                    pn_result_cd := PKG_CONST.RESULT__FAILURE;
                    pv_error_message := 'Device type ''' || pv_device_type_desc ||''' requires pre-registration and this device is not registered';
                    RETURN;
                END IF;
                SELECT SEQ_DEVICE_ID.NEXTVAL, SF_GENERATE_DEVICE_NAME(pn_device_type_id, pn_device_sub_type_id)
                  INTO pn_device_id, pv_device_name
                  FROM DUAL;
                INSERT INTO DEVICE.DEVICE(DEVICE_ID, DEVICE_NAME, DEVICE_TYPE_ID, DEVICE_SUB_TYPE_ID, DEVICE_SERIAL_CD, DEVICE_ACTIVE_YN_FLAG)
                    VALUES(pn_device_id, pv_device_name, pn_device_type_id, pn_device_sub_type_id, pv_device_serial_cd, 'Y');
        END;
        DECLARE
            ln_pos_id PSS.POS.POS_ID%TYPE;
        BEGIN
            SELECT P.POS_ID, TZ.TIME_ZONE_GUID
              INTO ln_pos_id, pv_time_zone_guid
              FROM PSS.POS P
              JOIN LOCATION.LOCATION L ON P.LOCATION_ID = L.LOCATION_ID
              JOIN LOCATION.TIME_ZONE TZ ON L.LOCATION_TIME_ZONE_CD = TZ.TIME_ZONE_CD
             WHERE P.DEVICE_ID = pn_device_id;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                SELECT PSS.SEQ_POS_ID.NEXTVAL
                  INTO ln_pos_id
                  FROM DUAL; 
                INSERT INTO PSS.POS(POS_ID, DEVICE_ID) 
                    VALUES(ln_pos_id, pn_device_id);
                SELECT TZ.TIME_ZONE_GUID
                  INTO pv_time_zone_guid
                  FROM PSS.POS P
                  JOIN LOCATION.LOCATION L ON P.LOCATION_ID = L.LOCATION_ID
                  JOIN LOCATION.TIME_ZONE TZ ON L.LOCATION_TIME_ZONE_CD = TZ.TIME_ZONE_CD
                 WHERE P.POS_ID = ln_pos_id;
	    END;
        SP_CREATE_DEFAULT_HOSTS(pn_device_id, pn_new_host_count, pn_result_cd, pv_error_message);
        IF pn_result_cd != PKG_CONST.RESULT__SUCCESS THEN
            RETURN;
        END IF;
        PSS.PKG_POS_PTA.SP_IMPORT_POS_PTA_TEMPLATE(pn_device_id, ln_pos_pta_tmpl_id, 'S', 'AE', 'N', pn_result_cd, pv_error_message);
        IF pn_result_cd != PKG_CONST.RESULT__SUCCESS THEN
            RETURN;
        END IF;
        pn_result_cd := PKG_CONST.RESULT__SUCCESS;
        pv_error_message := PKG_CONST.ERROR__NO_ERROR;
    END;
    
    PROCEDURE NEXT_MASTER_ID_BY_SERIAL(
        pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
        pv_device_name OUT DEVICE.DEVICE_NAME%TYPE,
        pn_master_id OUT NUMBER)
    IS
        ln_device_id DEVICE.DEVICE_ID%TYPE;
    BEGIN
        -- increment and set masterId
        SELECT DEVICE_ID, DEVICE_NAME
          INTO ln_device_id, pv_device_name
          FROM DEVICE.DEVICE
         WHERE DEVICE_SERIAL_CD = pv_device_serial_cd
           AND DEVICE_ACTIVE_YN_FLAG = 'Y';
        LOOP
            UPDATE DEVICE.DEVICE_SETTING
               SET DEVICE_SETTING_VALUE = GREATEST(DATE_TO_MILLIS(SYSDATE) / 1000, NVL(TO_NUMBER_OR_NULL(DEVICE_SETTING_VALUE), 0)) + 1
             WHERE DEVICE_SETTING_PARAMETER_CD = 'VIRTUAL_MASTER_ID'
               AND DEVICE_ID = ln_device_id
             RETURNING TO_NUMBER_OR_NULL(DEVICE_SETTING_VALUE)
              INTO pn_master_id;
            IF pn_master_id IS NOT NULL THEN
                RETURN;
            END IF;
            pn_master_id := DATE_TO_MILLIS(SYSDATE) / 1000;
            BEGIN
                INSERT INTO DEVICE.DEVICE_SETTING(DEVICE_ID, DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_VALUE)
                    VALUES(ln_device_id, 'VIRTUAL_MASTER_ID', pn_master_id);
                RETURN;
            EXCEPTION
                WHEN DUP_VAL_ON_INDEX THEN
                    pn_master_id := NULL;
            END;
        END LOOP;    
    END;
	
	PROCEDURE GET_OR_CREATE_PREPAID_DEVICE(
		pn_customer_id IN PSS.POS.CUSTOMER_ID%TYPE,
		pn_location_id IN PSS.POS.LOCATION_ID%TYPE,
		pn_corp_customer_id IN PSS.CONSUMER_ACCT.CORP_CUSTOMER_ID%TYPE,
		pv_currency_cd IN PSS.CONSUMER_ACCT.CURRENCY_CD%TYPE,
		pn_device_id OUT DEVICE.DEVICE_ID%TYPE,
		pv_device_serial_cd OUT DEVICE.DEVICE_SERIAL_CD%TYPE,
		pv_device_name OUT DEVICE.DEVICE_NAME%TYPE
	)
	IS
        lv_device_type_desc DEVICE_TYPE.DEVICE_TYPE_DESC%TYPE;
        ln_device_sub_type_id DEVICE.DEVICE_SUB_TYPE_ID%TYPE;
        lc_prev_device_active_yn_flag VARCHAR(1);
        lc_master_id_always_inc VARCHAR(1);
        ln_key_gen_time NUMBER;
        lc_legacy_safe_key VARCHAR(1);
        lv_time_zone_guid TIME_ZONE.TIME_ZONE_GUID%TYPE;
        ln_new_host_count NUMBER;
        ln_result_cd NUMBER;
        lv_error_message VARCHAR2(1000);
		lv_lock VARCHAR2(128);
		ln_terminal_id REPORT.TERMINAL.TERMINAL_ID%TYPE;
		ln_dealer_id CORP.DEALER.DEALER_ID%TYPE;
		ln_eport_id REPORT.EPORT.EPORT_ID%TYPE;
		ln_customer_bank_id CORP.CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE;
        lv_customer_address1 CORP.CUSTOMER_ADDR.ADDRESS1%TYPE;                                                                                                                                                               
        lv_customer_city CORP.CUSTOMER_ADDR.CITY%TYPE;                                                                                                                                                                              
        lv_customer_state_cd CORP.CUSTOMER_ADDR.STATE%TYPE;                                                                                                                                                                              
        lv_customer_postal CORP.CUSTOMER_ADDR.ZIP%TYPE;                                                                                                                                                                              
        lv_customer_country_cd CORP.CUSTOMER_ADDR.COUNTRY_CD%TYPE;                                                                                                                                                                              				
	BEGIN		
		pv_device_serial_cd := 'V1-' || pn_corp_customer_id || '-' || pv_currency_cd;
		
		SELECT MAX(D.DEVICE_ID), MAX(D.DEVICE_NAME)
		INTO pn_device_id, pv_device_name
		FROM DEVICE.DEVICE D
		JOIN REPORT.EPORT E ON D.DEVICE_SERIAL_CD = E.EPORT_SERIAL_NUM
		JOIN REPORT.VW_TERMINAL_EPORT TE ON E.EPORT_ID = TE.EPORT_ID
		JOIN REPORT.TERMINAL T ON TE.TERMINAL_ID = T.TERMINAL_ID
		WHERE D.DEVICE_SERIAL_CD = pv_device_serial_cd 
			AND D.DEVICE_ACTIVE_YN_FLAG = 'Y'
			AND T.CUSTOMER_ID = pn_corp_customer_id;
		
		IF pn_device_id > 0 THEN
			RETURN;
		END IF;
	
		lv_lock := PKG_GLOBAL.REQUEST_LOCK('DEVICE.DEVICE', pv_device_serial_cd);
	
		INITIALIZE_DEVICE(pv_device_serial_cd, 14, 'Y',
			pn_device_id,
			pv_device_name,
			lv_device_type_desc,
			ln_device_sub_type_id,
			lc_prev_device_active_yn_flag,
			lc_master_id_always_inc,
			ln_key_gen_time,
			lc_legacy_safe_key,
			lv_time_zone_guid,
			ln_new_host_count,
			ln_result_cd,
			lv_error_message		
		);
		
		UPDATE PSS.POS
		SET CUSTOMER_ID = pn_customer_id, LOCATION_ID = pn_location_id
		WHERE DEVICE_ID = pn_device_id;
		
		IF pn_device_id > 0 THEN
			SELECT MAX(DEALER_ID)
			INTO ln_dealer_id
			FROM (
				SELECT DEALER_ID
				FROM CORP.DEALER_LICENSE
				WHERE LICENSE_ID IN (
					SELECT MAX(LICENSE_ID)
					FROM (
						SELECT L.LICENSE_ID
						FROM CORP.CUSTOMER_LICENSE CL
						JOIN CORP.LICENSE_NBR LN ON CL.LICENSE_NBR = LN.LICENSE_NBR
						JOIN CORP.LICENSE L ON LN.LICENSE_ID = L.LICENSE_ID
						WHERE CL.CUSTOMER_ID = pn_corp_customer_id AND L.STATUS = 'A'
						ORDER BY CL.RECEIVED DESC
					) WHERE ROWNUM = 1
				)
				AND SYSDATE >= NVL(START_DATE, MIN_DATE)
				AND SYSDATE < NVL(END_DATE, MAX_DATE)
				ORDER BY START_DATE DESC, DEALER_ID DESC
			) WHERE ROWNUM = 1;
			
			IF ln_dealer_id IS NULL THEN
				SELECT DEALER_ID
				INTO ln_dealer_id
				FROM (
					SELECT DEALER_ID
					FROM CORP.DEALER
					WHERE DEALER_NAME = 'USA Technologies'
					ORDER BY DEALER_ID
				) WHERE ROWNUM = 1;
			END IF;
			
			SELECT CUSTOMER_BANK_ID
			INTO ln_customer_bank_id
			FROM (
				SELECT CB.CUSTOMER_BANK_ID
				FROM CORP.CUSTOMER_BANK CB
				LEFT OUTER JOIN CORP.CUSTOMER_BANK_TERMINAL CBT ON CB.CUSTOMER_BANK_ID = CBT.CUSTOMER_BANK_ID
					AND SYSDATE >= NVL(CBT.START_DATE, MIN_DATE)
					AND SYSDATE < NVL(CBT.END_DATE, MAX_DATE)
				LEFT OUTER JOIN REPORT.TERMINAL T ON CBT.TERMINAL_ID = T.TERMINAL_ID AND T.STATUS != 'D'
				WHERE CB.CUSTOMER_ID = pn_corp_customer_id
				GROUP BY CB.CUSTOMER_BANK_ID
				ORDER BY COUNT(1) DESC, CB.CUSTOMER_BANK_ID
			) WHERE ROWNUM = 1;
		
            SELECT MAX(ADDRESS1), NVL(MAX(CITY), 'Malvern'), NVL(MAX(STATE), 'PA'), NVL(MAX(ZIP), '19355'), NVL(MAX(COUNTRY_CD), 'US')                                                                                                                                                                     
              INTO lv_customer_address1, lv_customer_city, lv_customer_state_cd, lv_customer_postal, lv_customer_country_cd 
              FROM CORP.CUSTOMER_ADDR
             WHERE CUSTOMER_ID = pn_corp_customer_id;
             
			REPORT.GET_OR_CREATE_EPORT(ln_eport_id, pv_device_serial_cd, 14);
			CORP.DEALER_EPORT_UPD(ln_dealer_id, ln_eport_id);
			REPORT.PKG_CUSTOMER_MANAGEMENT.CREATE_TERMINAL_MASS(
				0, /* pn_user_id */
				ln_terminal_id, /* pn_terminal_id */
				pv_device_serial_cd, /* pv_device_serial_cd */
				ln_dealer_id, /* pn_dealer_id */
				'TBD', /* pv_asset_nbr */
				'TBD', /* pv_machine_make */
				'To Be Determined', /* pv_machine_model */
				NULL, /* pv_telephone */
				NULL, /* pv_prefix */
				NULL, /* pv_region_name */
				'TBD', /* pv_location_name */
				NULL, /* pv_location_details */
				lv_customer_address1, /* pv_address1 */
				lv_customer_city, /* pv_city */
				lv_customer_state_cd, /* pv_state_cd */
				lv_customer_postal, /* pv_postal */
				lv_customer_country_cd, /* pv_country_cd */
				ln_customer_bank_id, /* pn_customer_bank_id */
				1, /* pn_pay_sched_id */
				'- Not Assigned -', /* pv_location_type_name */
				NULL, /* pn_location_type_specific */
				'- Not Assigned -', /* pv_product_type_name */
				NULL, /* pn_product_type_specific */
				'?', /* pc_auth_mode */
				0, /* pn_avg_amt */
				0, /* pn_time_zone_id */
				'?', /* pc_dex_data */
				'?', /* pc_receipt */
				0, /* pn_vends */
				NULL, /* pv_term_var_1 */
				NULL, /* pv_term_var_2 */
				NULL, /* pv_term_var_3 */
				NULL, /* pv_term_var_4 */
				NULL, /* pv_term_var_5 */
				NULL, /* pv_term_var_6 */
				NULL, /* pv_term_var_7 */
				NULL, /* pv_term_var_8 */
				TRUNC(SYSDATE - 30), /* pd_activate_date */
				0, /* pn_fee_grace_days */
				'N', /* pc_keep_existing_data */
				'N' /* pc_override_payment_schedule */
			);
			
			UPDATE REPORT.TERMINAL
			SET STATUS = 'A'
			WHERE TERMINAL_ID = ln_terminal_id AND STATUS != 'A';
		END IF;
	END;
    
    PROCEDURE UPDATE_APP_VERSION(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pn_protocol_version NUMBER,
        pv_app_type DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
        pv_app_version DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE)
    IS
        ln_device_id DEVICE.DEVICE_ID%TYPE;
    BEGIN
        SELECT DEVICE_ID
          INTO ln_device_id
          FROM DEVICE.DEVICE_LAST_ACTIVE
         WHERE DEVICE_NAME = pv_device_name;
        INSERT INTO DEVICE.DEVICE_SETTING(DEVICE_ID, DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_VALUE)
            SELECT ln_device_id, A.NAME, A.VALUE
              FROM (SELECT 'App Type' NAME, pv_app_type VALUE FROM DUAL
              UNION ALL SELECT 'App Version', pv_app_version FROM DUAL
              UNION ALL SELECT 'Property List Version', TO_CHAR(pn_protocol_version) FROM DUAL) A
              LEFT OUTER JOIN DEVICE.DEVICE_SETTING O ON A.NAME = O.DEVICE_SETTING_PARAMETER_CD AND O.DEVICE_ID = ln_device_id
             WHERE O.DEVICE_SETTING_PARAMETER_CD IS NULL;
        IF SQL%ROWCOUNT != 3 THEN
            UPDATE DEVICE.DEVICE_SETTING
               SET DEVICE_SETTING_VALUE = DECODE(DEVICE_SETTING_PARAMETER_CD, 'App Type', pv_app_type, 'App Version', pv_app_version, 'Property List Version', TO_CHAR(pn_protocol_version))
             WHERE DEVICE_ID = ln_device_id
               AND DEVICE_SETTING_PARAMETER_CD IN('App Type', 'App Version','Property List Version')
               AND DEVICE_SETTING_VALUE != DECODE(DEVICE_SETTING_PARAMETER_CD, 'App Type', pv_app_type, 'App Version', pv_app_version, 'Property List Version', TO_CHAR(pn_protocol_version));
        END IF;
    END;
    
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/PSS/PKG_SETTLEMENT.pbk?rev=1.77
CREATE OR REPLACE PACKAGE BODY PSS.PKG_SETTLEMENT AS
    FUNCTION AFTER_SETTLE_TRAN_STATE_CD(
        pc_auth_type_cd PSS.AUTH.AUTH_TYPE_CD%TYPE,
        pc_refund_type_cd PSS.REFUND.REFUND_TYPE_CD%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pc_device_result_type_cd PSS.TRAN.TRAN_DEVICE_RESULT_TYPE_CD%TYPE)
    RETURN PSS.TRAN.TRAN_STATE_CD%TYPE
    PARALLEL_ENABLE DETERMINISTIC
    IS       
    BEGIN
        IF pc_auth_result_cd = 'Y' THEN
            IF pc_auth_type_cd IS NOT NULL THEN
                IF pc_auth_type_cd IN('C', 'V', 'E', 'I') THEN
                    IF pc_device_result_type_cd IS NULL THEN
                        RETURN 'V'; -- processed reversal
                    ELSE
                        RETURN 'C'; -- Cancelled 
                    END IF;
                ELSE
                    RETURN 'D'; -- Complete
                END IF;                    
            ELSIF pc_refund_type_cd IS NOT NULL THEN
                IF pc_refund_type_cd IN('C') THEN
                    RETURN 'C'; -- Cancelled 
                ELSE
                    RETURN 'D'; -- Complete
                END IF;
            ELSE
                RAISE_APPLICATION_ERROR(-20560, 'Both auth type cd and refund type cd are null');
            END IF;
        ELSIF pc_auth_result_cd = 'P' THEN
            RETURN 'Q'; -- settlement processed
        ELSIF pc_auth_result_cd = 'N' THEN
            RETURN 'N'; -- PROCESSED_SERVER_SETTLEMENT_INCOMPLETE
        ELSIF pc_auth_result_cd = 'F' THEN
            RETURN 'R'; -- PROCESSED_SERVER_SETTLEMENT_ERROR
        ELSIF pc_auth_result_cd IN('O', 'R') THEN
            IF pc_auth_type_cd IS NOT NULL THEN
                IF pc_auth_type_cd IN('C', 'V', 'E', 'I') THEN
                    IF pc_device_result_type_cd IS NULL THEN
                        RETURN 'V'; -- processed reversal
                    ELSE
                        RETURN 'C'; -- Cancelled 
                    END IF;
                ELSE
                    RETURN 'E'; -- Complete - Error
                END IF;                    
            ELSIF pc_refund_type_cd IS NOT NULL THEN
                IF pc_refund_type_cd IN('C') THEN
                    RETURN 'C'; -- Cancelled 
                ELSE
                    RETURN 'E'; -- Complete - Error
                END IF;
            ELSE
                RAISE_APPLICATION_ERROR(-20560, 'Both auth type cd and refund type cd are null');
            END IF;
        END IF;
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    FUNCTION CREATE_TERMINAL_BATCH(
        pn_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE)
    RETURN PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE
    IS
        ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
    BEGIN
        SELECT PSS.SEQ_TERMINAL_BATCH_ID.NEXTVAL
          INTO ln_terminal_batch_id
          FROM DUAL;
        INSERT INTO PSS.TERMINAL_BATCH (
                TERMINAL_BATCH_ID,
                TERMINAL_ID,
                TERMINAL_BATCH_NUM,
                TERMINAL_BATCH_OPEN_TS,
                TERMINAL_BATCH_CYCLE_NUM,
                TERMINAL_CAPTURE_FLAG) 
         SELECT ln_terminal_batch_id, 
                pn_terminal_id, 
                T.TERMINAL_NEXT_BATCH_NUM,
                SYSDATE,
                T.TERMINAL_BATCH_CYCLE_NUM,
                A.TERMINAL_CAPTURE_FLAG
           FROM PSS.TERMINAL T
           JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
           JOIN AUTHORITY.AUTHORITY A ON M.AUTHORITY_ID = A.AUTHORITY_ID
          WHERE TERMINAL_ID = pn_terminal_id;
          
         UPDATE PSS.TERMINAL
            SET TERMINAL_NEXT_BATCH_NUM = CASE 
                    WHEN TERMINAL_NEXT_BATCH_NUM >= TERMINAL_MAX_BATCH_NUM THEN NVL(TERMINAL_MIN_BATCH_NUM, 1) /* reset batch num */ 
                    ELSE TERMINAL_NEXT_BATCH_NUM + 1 /* increment batch num */
                END,
                TERMINAL_BATCH_CYCLE_NUM = CASE 
                    WHEN TERMINAL_NEXT_BATCH_NUM >= TERMINAL_MAX_BATCH_NUM THEN TERMINAL_BATCH_CYCLE_NUM + 1 /* next cycle */ 
                    ELSE TERMINAL_BATCH_CYCLE_NUM /* same cycle */
                END
            WHERE TERMINAL_ID = pn_terminal_id;
        RETURN ln_terminal_batch_id;
    END;
            
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    FUNCTION GET_AVAILABLE_TERMINAL_BATCH(
        pn_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE,
        pn_allowed_trans OUT PLS_INTEGER,
        pb_create_if_needed BOOLEAN)
    RETURN PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE
    IS
        ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
        lc_is_closed CHAR(1);
        ln_max_tran PSS.TERMINAL.TERMINAL_BATCH_MAX_TRAN%TYPE; 
        ln_terminal_state_id PSS.TERMINAL.TERMINAL_STATE_ID%TYPE;
        lc_terminal_capture_flag PSS.TERMINAL_BATCH.TERMINAL_CAPTURE_FLAG%TYPE;
        ln_attempts PLS_INTEGER;
    BEGIN
        -- get last terminal batch record and verify that it is open       
        SELECT /*+ FIRST_ROWS */ MAX(TERMINAL_BATCH_ID), MAX(DECODE(TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y')), MAX(TERMINAL_BATCH_MAX_TRAN), MAX(TERMINAL_STATE_ID), MAX(NVL(TERMINAL_CAPTURE_FLAG, 'N'))
          INTO ln_terminal_batch_id, lc_is_closed, ln_max_tran, ln_terminal_state_id, lc_terminal_capture_flag
          FROM (SELECT TB.TERMINAL_BATCH_ID,
                       TB.TERMINAL_BATCH_NUM, 
                       TB.TERMINAL_BATCH_CYCLE_NUM, 
                       TB.TERMINAL_BATCH_CLOSE_TS,
                       COALESCE(T.TERMINAL_BATCH_MAX_TRAN, AU.AUTHORITY_BATCH_MAX_TRAN, 999) TERMINAL_BATCH_MAX_TRAN,
                       T.TERMINAL_STATE_ID,
                       TB.TERMINAL_CAPTURE_FLAG
                  FROM PSS.TERMINAL T
                  JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
                  JOIN AUTHORITY.AUTHORITY AU ON M.AUTHORITY_ID = AU.AUTHORITY_ID 
                  LEFT OUTER JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID                   
                 WHERE T.TERMINAL_ID = pn_terminal_id
                 ORDER BY TB.TERMINAL_BATCH_CYCLE_NUM DESC,
                          TB.TERMINAL_BATCH_NUM DESC)
         WHERE ROWNUM = 1;
        pn_allowed_trans := ln_max_tran;
        IF ln_terminal_state_id NOT IN(3) THEN
            RAISE_APPLICATION_ERROR(-20559, 'Terminal ' || pn_terminal_id || ' is not locked and a new terminal batch can not be created for it');
        ELSIF ln_terminal_batch_id IS NULL THEN
            IF pb_create_if_needed THEN
                ln_terminal_batch_id := CREATE_TERMINAL_BATCH(pn_terminal_id);
            END IF;
        ELSIF lc_is_closed  = 'Y' THEN
            IF pb_create_if_needed THEN
                ln_terminal_batch_id := CREATE_TERMINAL_BATCH(pn_terminal_id);
            END IF;
        ELSE
            -- do we need a new batch?
            SELECT ln_max_tran - COUNT(DISTINCT TRAN_ID)
              INTO pn_allowed_trans
              FROM (SELECT A.TRAN_ID
                      FROM PSS.AUTH A
                     WHERE A.TERMINAL_BATCH_ID = ln_terminal_batch_id
                    UNION ALL
                    SELECT R.TRAN_ID
                      FROM PSS.REFUND R
                     WHERE R.TERMINAL_BATCH_ID = ln_terminal_batch_id);
            IF lc_terminal_capture_flag = 'Y' THEN              
                IF pn_allowed_trans > 0 THEN
                    SELECT COUNT(*)
                      INTO ln_attempts
                      FROM PSS.SETTLEMENT_BATCH
                     WHERE TERMINAL_BATCH_ID = ln_terminal_batch_id;
                END IF;
                IF ln_attempts > 0 OR pn_allowed_trans <= 0 THEN
                    -- Create new terminal batch
                    IF pb_create_if_needed THEN
                        ln_terminal_batch_id := CREATE_TERMINAL_BATCH(pn_terminal_id);
                    END IF;
                    pn_allowed_trans := ln_max_tran;
                END IF;
            END IF;
        END IF;
        
        RETURN ln_terminal_batch_id;
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE GET_PENDING_SETTLEMENTS(
        pn_payment_subtype_key_id PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE, 
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pc_ignore_minimums_flag CHAR,
        pn_max_settlements PLS_INTEGER, 
        pn_pending_terminal_batch_ids OUT NUMBER_TABLE)
    IS
    BEGIN
         -- The following should never occur:
        /*
        -- Find retry settlements
        SELECT TERMINAL_BATCH_ID
          BULK COLLECT INTO pn_pending_terminal_batch_ids
          FROM (SELECT TB.TERMINAL_BATCH_ID
                  FROM PSS.SETTLEMENT_BATCH SB 
                  JOIN PSS.TERMINAL_BATCH TB ON TB.TERMINAL_BATCH_ID = SB.TERMINAL_BATCH_ID
                 WHERE TB.TERMINAL_ID = pn_payment_subtype_key_id 
                   AND SB.SETTLEMENT_BATCH_STATE_ID = 4 
                 ORDER BY TB.TERMINAL_BATCH_OPEN_TS ASC)
         WHERE ROWNUM <= pn_max_settlements;
     
        IF pn_pending_terminal_batch_ids.COUNT > 0 THEN
            RETURN;
        END IF;
        */
        -- Find new open batches
        SELECT TERMINAL_BATCH_ID
          BULK COLLECT INTO pn_pending_terminal_batch_ids
          FROM (
            SELECT C.TERMINAL_BATCH_ID
              FROM PSS.MERCHANT M
              JOIN AUTHORITY.AUTHORITY AU ON M.AUTHORITY_ID = AU.AUTHORITY_ID
              JOIN (SELECT TERMINAL_ID, TERMINAL_BATCH_ID, MERCHANT_ID, TERMINAL_BATCH_OPEN_TS, 
                           TERMINAL_BATCH_MIN_TRAN, TERMINAL_BATCH_MAX_TRAN, TERMINAL_MIN_BATCH_CLOSE_HR, TERMINAL_MAX_BATCH_CLOSE_HR, 
                           SUM(CASE WHEN TRAN_STATE_CD IN('T') THEN 1 ELSE 0 END) NUM_TRAN
                      FROM (SELECT T.TERMINAL_ID, 
                                   TB.TERMINAL_BATCH_ID,
                                   T.MERCHANT_ID,
                                   X.TRAN_ID,
                                   X.TRAN_STATE_CD,
                                   T.TERMINAL_BATCH_MIN_TRAN,
                                   T.TERMINAL_BATCH_MAX_TRAN,
                                   T.TERMINAL_MIN_BATCH_CLOSE_HR,
                                   T.TERMINAL_MAX_BATCH_CLOSE_HR,
                                   TB.TERMINAL_BATCH_OPEN_TS
                              FROM PSS.TERMINAL T
                              JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
                              JOIN PSS.AUTH A ON TB.TERMINAL_BATCH_ID = A.TERMINAL_BATCH_ID
                              JOIN PSS.TRAN X ON A.TRAN_ID = X.TRAN_ID 
                             WHERE TB.TERMINAL_BATCH_OPEN_TS IS NOT NULL
                               AND DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y') = 'N'
                               AND T.TERMINAL_ID = pn_payment_subtype_key_id
                             UNION 
                            SELECT T.TERMINAL_ID, 
                                   TB.TERMINAL_BATCH_ID,
                                   T.MERCHANT_ID,
                                   X.TRAN_ID,
                                   X.TRAN_STATE_CD,
                                   T.TERMINAL_BATCH_MIN_TRAN,
                                   T.TERMINAL_BATCH_MAX_TRAN,
                                   T.TERMINAL_MIN_BATCH_CLOSE_HR,
                                   T.TERMINAL_MAX_BATCH_CLOSE_HR,
                                   TB.TERMINAL_BATCH_OPEN_TS
                              FROM PSS.TERMINAL T
                              JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
                              JOIN PSS.REFUND R ON TB.TERMINAL_BATCH_ID = R.TERMINAL_BATCH_ID
                              JOIN PSS.TRAN X ON R.TRAN_ID = X.TRAN_ID 
                             WHERE TB.TERMINAL_BATCH_OPEN_TS IS NOT NULL
                               AND DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y') = 'N'
                               AND T.TERMINAL_ID = pn_payment_subtype_key_id) 
                     GROUP BY TERMINAL_ID, TERMINAL_BATCH_ID, MERCHANT_ID, TERMINAL_BATCH_OPEN_TS, TERMINAL_BATCH_MIN_TRAN, TERMINAL_BATCH_MAX_TRAN, TERMINAL_MIN_BATCH_CLOSE_HR, TERMINAL_MAX_BATCH_CLOSE_HR
                     HAVING COUNT(TRAN_STATE_CD) = SUM(CASE WHEN TRAN_STATE_CD IN('T', 'E') THEN 1 ELSE 0 END)) C  ON C.MERCHANT_ID = M.MERCHANT_ID 
             WHERE C.NUM_TRAN > 0
               AND (pc_ignore_minimums_flag = 'Y'
                OR C.NUM_TRAN >= COALESCE(C.TERMINAL_BATCH_MAX_TRAN, AU.AUTHORITY_BATCH_MAX_TRAN, 999) 
                OR SYSDATE >= C.TERMINAL_BATCH_OPEN_TS + COALESCE(C.TERMINAL_MAX_BATCH_CLOSE_HR/24, AU.AUTHORITY_MAX_BATCH_CLOSE_HR/24, 1)
                OR (SYSDATE >= C.TERMINAL_BATCH_OPEN_TS + COALESCE(C.TERMINAL_MIN_BATCH_CLOSE_HR/24, AU.AUTHORITY_MIN_BATCH_CLOSE_HR/24, 0)
                    AND C.NUM_TRAN >= COALESCE(C.TERMINAL_BATCH_MIN_TRAN, AU.AUTHORITY_BATCH_MIN_TRAN, 25)))
             ORDER BY c.TERMINAL_BATCH_OPEN_TS ASC)
         WHERE ROWNUM <= pn_max_settlements;
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE GET_RETRY_SETTLEMENTS(
        pn_payment_subtype_key_id PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE, 
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_settlement_retry_interval NUMBER,
        pn_max_settlements PLS_INTEGER, 
        pn_pending_terminal_batch_ids OUT NUMBER_TABLE)
    IS
    BEGIN
        -- Find retyable batches
        SELECT TERMINAL_BATCH_ID
          BULK COLLECT INTO pn_pending_terminal_batch_ids
          FROM (
            SELECT C.TERMINAL_BATCH_ID
              FROM (SELECT TERMINAL_ID, TERMINAL_BATCH_ID, TERMINAL_BATCH_OPEN_TS, 
                           SUM(CASE WHEN TRAN_STATE_CD IN('R', 'N') THEN 1 ELSE 0 END) NUM_TRAN
                      FROM (SELECT T.TERMINAL_ID, 
                                   TB.TERMINAL_BATCH_ID,
                                   X.TRAN_ID,
                                   X.TRAN_STATE_CD,
                                   TB.TERMINAL_BATCH_OPEN_TS
                              FROM PSS.TERMINAL T
                              JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
                              JOIN PSS.AUTH A ON TB.TERMINAL_BATCH_ID = A.TERMINAL_BATCH_ID
                              JOIN PSS.TRAN X ON A.TRAN_ID = X.TRAN_ID 
                             WHERE TB.TERMINAL_BATCH_OPEN_TS IS NOT NULL
                               AND DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y') = 'N'
                               AND T.TERMINAL_ID = pn_payment_subtype_key_id
                             UNION 
                            SELECT T.TERMINAL_ID, 
                                   TB.TERMINAL_BATCH_ID,
                                   X.TRAN_ID,
                                   X.TRAN_STATE_CD,
                                   TB.TERMINAL_BATCH_OPEN_TS
                              FROM PSS.TERMINAL T
                              JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
                              JOIN PSS.REFUND R ON TB.TERMINAL_BATCH_ID = R.TERMINAL_BATCH_ID
                              JOIN PSS.TRAN X ON R.TRAN_ID = X.TRAN_ID 
                             WHERE TB.TERMINAL_BATCH_OPEN_TS IS NOT NULL
                               AND DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y') = 'N'
                               AND T.TERMINAL_ID = pn_payment_subtype_key_id) 
                     GROUP BY TERMINAL_ID, TERMINAL_BATCH_ID, TERMINAL_BATCH_OPEN_TS
                     HAVING COUNT(TRAN_STATE_CD) = SUM(CASE WHEN TRAN_STATE_CD IN('R', 'N', 'E') THEN 1 ELSE 0 END)) C
             WHERE C.NUM_TRAN > 0
               AND SYSDATE >= pn_settlement_retry_interval + (
                        SELECT NVL(MAX(LA.SETTLEMENT_BATCH_START_TS), MIN_DATE)
                          FROM PSS.SETTLEMENT_BATCH LA
                          WHERE C.TERMINAL_BATCH_ID = LA.TERMINAL_BATCH_ID)
             ORDER BY c.TERMINAL_BATCH_OPEN_TS ASC)
         WHERE ROWNUM <= pn_max_settlements;
    END;

/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE FORCE_SETTLE_BATCH(
        pn_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE,
        pv_force_reason PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_RESP_DESC%TYPE)
    IS
        ln_settlement_batch_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE;
        ln_settlement_batch_state_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_STATE_ID%TYPE;
        ln_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE;
        ln_terminal_state_id PSS.TERMINAL.TERMINAL_STATE_ID%TYPE;
        ln_is_open PLS_INTEGER;
        lv_msg VARCHAR2(4000);
    BEGIN
        -- check terminal state
        SELECT T.TERMINAL_ID, T.TERMINAL_STATE_ID
          INTO ln_terminal_id, ln_terminal_state_id
          FROM PSS.TERMINAL T
          JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
         WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id;
        IF ln_terminal_state_id NOT IN(3,5) THEN
            SELECT 'Terminal ' || ln_terminal_id || ' is currently ' || DECODE(ln_terminal_state_id, 1, 'not locked', 2, 'disabled', 5, 'busy with retry', 'unavailable')
              INTO lv_msg
              FROM DUAL;
            RAISE_APPLICATION_ERROR(-20560, lv_msg);
        END IF;
        
        -- Check status of most recent settlement batch to ensure it is failue or decline
        SELECT MAX(SETTLEMENT_BATCH_ID), NVL(MAX(SETTLEMENT_BATCH_STATE_ID), 0)
          INTO ln_settlement_batch_id, ln_settlement_batch_state_id
          FROM (
            SELECT SETTLEMENT_BATCH_ID, SETTLEMENT_BATCH_STATE_ID
              FROM PSS.SETTLEMENT_BATCH
             WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id
             ORDER BY SETTLEMENT_BATCH_START_TS DESC, SETTLEMENT_BATCH_ID DESC)
         WHERE ROWNUM = 1;
        IF ln_settlement_batch_state_id NOT IN(2, 3) THEN
            SELECT 'Terminal Batch ' || pn_terminal_batch_id || DECODE(ln_settlement_batch_state_id, 0, ' has not yet been tried', 1, ' was successfully settled already', 4, ' is awaiting a retry', 7, 'was partially settled already', ' is not ready for forced settlement')
              INTO lv_msg
              FROM DUAL;
            RAISE_APPLICATION_ERROR(-20561, lv_msg);
        END IF;
        
        -- create new settlement batch
        SELECT PSS.SEQ_SETTLEMENT_BATCH_ID.NEXTVAL
          INTO ln_settlement_batch_id
          FROM DUAL;            
        
        INSERT INTO PSS.SETTLEMENT_BATCH(
            SETTLEMENT_BATCH_ID,
            SETTLEMENT_BATCH_STATE_ID,
            SETTLEMENT_BATCH_START_TS,
            TERMINAL_BATCH_ID,
            SETTLEMENT_BATCH_RESP_CD,
            SETTLEMENT_BATCH_RESP_DESC,
            SETTLEMENT_BATCH_REF_CD,
            SETTLEMENT_BATCH_END_TS
        ) VALUES (
            ln_settlement_batch_id,
            1,
            SYSDATE,
            pn_terminal_batch_id,
            0,
            pv_force_reason, 
            'FORCE_SETTLEMENT',
            SYSDATE);
        -- add all trans and update their tran state cd
        INSERT INTO PSS.TRAN_SETTLEMENT_BATCH(
            SETTLEMENT_BATCH_ID,
            AUTH_ID,
            TRAN_ID,
            TRAN_SETTLEMENT_B_AMT)
          SELECT DISTINCT 
                 ln_settlement_batch_id,
                 FIRST_VALUE(A.AUTH_ID) OVER (PARTITION BY T.TRAN_ID ORDER BY A.CREATED_TS DESC, A.AUTH_ID DESC) AUTH_ID, 
                 T.TRAN_ID, 
                 FIRST_VALUE(A.AUTH_AMT) OVER (PARTITION BY T.TRAN_ID ORDER BY A.CREATED_TS DESC, A.AUTH_ID DESC) AUTH_AMT
            FROM PSS.TRAN T
            JOIN PSS.AUTH A ON T.TRAN_ID = A.TRAN_ID
           WHERE A.TERMINAL_BATCH_ID = pn_terminal_batch_id
             AND A.AUTH_STATE_ID IN(2,6);
        INSERT INTO PSS.REFUND_SETTLEMENT_BATCH(
            SETTLEMENT_BATCH_ID,
            REFUND_ID,
            TRAN_ID,
            REFUND_SETTLEMENT_B_AMT)
          SELECT DISTINCT 
                 ln_settlement_batch_id,
                 FIRST_VALUE(R.REFUND_ID) OVER (PARTITION BY T.TRAN_ID ORDER BY R.CREATED_TS DESC, R.REFUND_ID DESC) AUTH_ID, 
                 T.TRAN_ID, 
                 FIRST_VALUE(R.REFUND_AMT) OVER (PARTITION BY T.TRAN_ID ORDER BY R.CREATED_TS DESC, R.REFUND_ID DESC) REFUND_AMT
            FROM PSS.TRAN T
            JOIN PSS.REFUND R ON T.TRAN_ID = R.TRAN_ID
           WHERE R.TERMINAL_BATCH_ID = pn_terminal_batch_id
             AND R.REFUND_STATE_ID IN(1);
        UPDATE PSS.TRAN 
           SET TRAN_STATE_CD = 'D'
         WHERE TRAN_STATE_CD IN('R', 'N', 'Q', 'T')
           AND TRAN_ID IN(
            SELECT A.TRAN_ID
              FROM PSS.AUTH A 
             WHERE A.TERMINAL_BATCH_ID = pn_terminal_batch_id
               AND A.AUTH_STATE_ID IN(2,6)
            UNION ALL
            SELECT R.TRAN_ID
              FROM PSS.REFUND R
             WHERE R.TERMINAL_BATCH_ID = pn_terminal_batch_id
               AND R.REFUND_STATE_ID IN(1));
                   
        -- update terminal batch
        UPDATE PSS.TERMINAL_BATCH
           SET TERMINAL_BATCH_CLOSE_TS = SYSDATE
         WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id;
    END;

/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE FORCE_TRAN(
        pn_payment_subtype_key_id PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE, 
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pv_force_reason PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_RESP_DESC%TYPE)
    IS
        lc_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
        ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
        ln_sale_auth_id PSS.AUTH.AUTH_ID%TYPE; 
        ln_force_auth_id PSS.AUTH.AUTH_ID%TYPE;
        ln_force_refund_id PSS.REFUND.REFUND_ID%TYPE;
        ln_allowed_trans PLS_INTEGER;
        ln_settlement_batch_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE;
        ln_auth_amt PSS.AUTH.AUTH_AMT%TYPE;
    BEGIN
        -- check terminal state
        SELECT T.TRAN_STATE_CD
          INTO lc_tran_state_cd
          FROM PSS.TRAN T
         WHERE T.TRAN_ID = pn_tran_id;
        IF lc_tran_state_cd NOT IN('I', 'J') THEN
            RAISE_APPLICATION_ERROR(-20562, 'Tran ' || pn_tran_id || ' is not in INCOMPLETE or INCOMPLETE_ERROR state and cannot be forced');
        END IF;
        
        UPDATE PSS.TRAN
           SET TRAN_STATE_CD = CASE WHEN pv_payment_subtype_class LIKE 'Authority::ISO8583%' THEN 'T' ELSE 'D' END
         WHERE TRAN_ID = pn_tran_id
           AND TRAN_STATE_CD = lc_tran_state_cd; -- make sure it didn't change
        
        IF SQL%ROWCOUNT < 1 THEN
            FORCE_TRAN(pn_payment_subtype_key_id, pv_payment_subtype_class, pn_tran_id, pv_force_reason);
        ELSE
            -- add to batch if necessary
            SELECT MAX(AUTH_ID), MAX(AUTH_AMT)
              INTO ln_sale_auth_id, ln_auth_amt
              FROM (
                SELECT AUTH_ID, AUTH_TS, AUTH_AMT
                  FROM PSS.AUTH
                 WHERE TRAN_ID = pn_tran_id
                   AND AUTH_TYPE_CD IN('U','S','O','A','D','C','E','V','I')
                 ORDER BY AUTH_TS DESC, AUTH_ID DESC)
             WHERE ROWNUM = 1;
            IF ln_sale_auth_id IS NULL THEN
                SELECT MAX(REFUND_ID), MAX(REFUND_AMT)
                  INTO ln_force_refund_id, ln_auth_amt
                  FROM (
                    SELECT CREATED_TS, REFUND_ID, REFUND_AMT
                      FROM PSS.REFUND
                     WHERE TRAN_ID = pn_tran_id
                     ORDER BY CREATED_TS DESC, REFUND_ID DESC)
                 WHERE ROWNUM = 1;
            END IF;             
            IF pv_payment_subtype_class LIKE 'Authority::ISO8583%' THEN
                IF ln_sale_auth_id IS NOT NULL THEN
                    SELECT MAX(TERMINAL_BATCH_ID)
                      INTO ln_terminal_batch_id
                      FROM PSS.AUTH
                     WHERE TRAN_ID = pn_tran_id;
                ELSIF ln_force_refund_id IS NOT NULL THEN
                    SELECT MAX(TERMINAL_BATCH_ID)
                      INTO ln_terminal_batch_id
                      FROM PSS.REFUND
                     WHERE TRAN_ID = pn_tran_id;
                END IF;
                IF ln_terminal_batch_id IS NULL THEN
                    ln_terminal_batch_id := GET_AVAILABLE_TERMINAL_BATCH(pn_payment_subtype_key_id, ln_allowed_trans, TRUE);
                    IF ln_allowed_trans <= 0 THEN
                        RAISE_APPLICATION_ERROR(-20556, 'Terminal Batch ' || ln_terminal_batch_id || ' is full for terminal ' || pn_payment_subtype_key_id || '; Can not add tran ' || pn_tran_id || ' to it');
                    END IF;
                END IF;
            ELSE
                -- create settlement batch record
                SELECT PSS.SEQ_SETTLEMENT_BATCH_ID.NEXTVAL
                  INTO ln_settlement_batch_id
                  FROM DUAL;
                INSERT INTO PSS.SETTLEMENT_BATCH(
                    SETTLEMENT_BATCH_ID,
                    SETTLEMENT_BATCH_STATE_ID,
                    SETTLEMENT_BATCH_START_TS,
                    SETTLEMENT_BATCH_END_TS,
                    SETTLEMENT_BATCH_RESP_CD,
                    SETTLEMENT_BATCH_RESP_DESC
                ) VALUES (
                    ln_settlement_batch_id,
                    1,
                    SYSDATE,
                    SYSDATE,
                    'MANUAL',
                    pv_force_reason);
            END IF;
            IF ln_sale_auth_id IS NOT NULL THEN  
                SELECT PSS.SEQ_AUTH_ID.NEXTVAL
                  INTO ln_force_auth_id
                  FROM DUAL;
                INSERT INTO PSS.AUTH (
                        AUTH_ID,
                        TRAN_ID,
                        AUTH_TYPE_CD,
                        AUTH_STATE_ID,
                        AUTH_PARSED_ACCT_DATA,
                        ACCT_ENTRY_METHOD_CD,
                        AUTH_AMT,
                        AUTH_TS,
                        TERMINAL_BATCH_ID,
                        TRACE_NUMBER,
                        AUTH_RESULT_CD,
                        AUTH_RESP_CD,
                        AUTH_RESP_DESC,
                        AUTH_AMT_APPROVED)
                 SELECT ln_force_auth_id,
                        pn_tran_id,
                        A.AUTH_TYPE_CD,
                        2,
                        a.AUTH_PARSED_ACCT_DATA,
                        a.ACCT_ENTRY_METHOD_CD,
                        a.AUTH_AMT,	    
                        SYSDATE,
                        ln_terminal_batch_id,
                        a.TRACE_NUMBER,
                        'Y',
                        'MANUAL',
                        pv_force_reason,
                        a.AUTH_AMT
                   FROM PSS.AUTH a
                  WHERE a.AUTH_ID = ln_sale_auth_id;
                IF ln_settlement_batch_id IS NOT NULL THEN
                    INSERT INTO PSS.TRAN_SETTLEMENT_BATCH(
                        SETTLEMENT_BATCH_ID,
                        AUTH_ID,
                        TRAN_ID,
                        TRAN_SETTLEMENT_B_AMT
                    ) VALUES (
                        ln_settlement_batch_id,
                        ln_force_auth_id,
                        pn_tran_id,
                        ln_auth_amt
                    );               
                END IF;
            ELSIF ln_force_refund_id IS NOT NULL THEN
                UPDATE PSS.REFUND
                   SET REFUND_STATE_ID = 1,
                       REFUND_RESP_CD = 'MANUAL',
                       REFUND_RESP_DESC = pv_force_reason,
                       REFUND_AUTHORITY_TS = SYSDATE,
                       TERMINAL_BATCH_ID = NVL(ln_terminal_batch_id, TERMINAL_BATCH_ID)
                 WHERE REFUND_ID = ln_force_refund_id;
                IF ln_settlement_batch_id IS NOT NULL THEN
                    INSERT INTO PSS.REFUND_SETTLEMENT_BATCH(
                        SETTLEMENT_BATCH_ID,
                        REFUND_ID,
                        TRAN_ID,
                        REFUND_SETTLEMENT_B_AMT
                    ) VALUES (
                        ln_settlement_batch_id,
                        ln_force_refund_id,
                        pn_tran_id,
                        ln_auth_amt
                    );               
                END IF;
            END IF;
        END IF;
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE ERROR_TRAN(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pv_force_reason PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_RESP_DESC%TYPE)
    IS
        lc_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
        lc_terminal_capture_flag PSS.TERMINAL_BATCH.TERMINAL_CAPTURE_FLAG%TYPE;
    BEGIN
        -- check terminal state
        SELECT TRAN_STATE_CD
          INTO lc_tran_state_cd
          FROM PSS.TRAN
         WHERE TRAN_ID = pn_tran_id;
        IF lc_tran_state_cd IN('T','N') THEN
            SELECT MIN(TB.TERMINAL_CAPTURE_FLAG)
              INTO lc_terminal_capture_flag
              FROM PSS.TERMINAL_BATCH TB
              JOIN (SELECT A.TERMINAL_BATCH_ID FROM PSS.AUTH A WHERE A.AUTH_TYPE_CD NOT IN('N', 'L') AND A.TRAN_ID = pn_tran_id
                    UNION ALL
                    SELECT R.TERMINAL_BATCH_ID FROM PSS.REFUND R WHERE R.TRAN_ID = pn_tran_id) X ON TB.TERMINAL_BATCH_ID = X.TERMINAL_BATCH_ID;
            IF lc_terminal_capture_flag != 'Y' THEN
                RAISE_APPLICATION_ERROR(-20562, 'Tran ' || pn_tran_id || ' is not in INCOMPLETE or INCOMPLETE_ERROR state and it is in a Host-Capture batch and cannot be errored');
            END IF;
        ELSIF lc_tran_state_cd NOT IN('I', 'J') THEN
            RAISE_APPLICATION_ERROR(-20562, 'Tran ' || pn_tran_id || ' is not in INCOMPLETE or INCOMPLETE_ERROR state and cannot be errored');
        END IF;
        
        UPDATE PSS.TRAN
           SET TRAN_STATE_CD = 'E'
         WHERE TRAN_ID = pn_tran_id
           AND TRAN_STATE_CD = lc_tran_state_cd; -- make sure it didn't change
           
        IF SQL%ROWCOUNT < 1 THEN
            ERROR_TRAN(pn_tran_id, pv_force_reason);
        END IF;
    END;
         
    PROCEDURE MARK_ADMIN_CMD_EXECUTED(
        pn_admin_cmd_id PSS.ADMIN_CMD.ADMIN_CMD_ID%TYPE)
    IS
    BEGIN
        UPDATE PSS.ADMIN_CMD
           SET ADMIN_CMD_STATE_ID = 2,
               CLOSED_UTC_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP)
         WHERE ADMIN_CMD_ID = pn_admin_cmd_id;
    END;

    PROCEDURE MARK_ADMIN_CMD_ERRORED(
        pn_admin_cmd_id PSS.ADMIN_CMD.ADMIN_CMD_ID%TYPE,
        pv_error_msg PSS.ADMIN_CMD.ERROR_MSG%TYPE)
    IS
    BEGIN
        UPDATE PSS.ADMIN_CMD
           SET ADMIN_CMD_STATE_ID = 4,
               CLOSED_UTC_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP),
               ERROR_MSG = pv_error_msg
         WHERE ADMIN_CMD_ID = pn_admin_cmd_id;
    END;
    
    PROCEDURE GET_ACTIONS_FROM_ADMIN_CMDS(
        pn_payment_subtype_key_id PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE, 
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_max_settlements PLS_INTEGER, 
        pn_max_trans PLS_INTEGER,
        pc_allow_add_to_batch CHAR,
        pc_retry_only CHAR,
        pc_settle_processing_enabled CHAR,
        pc_tran_processing_enabled CHAR,
        pn_pending_terminal_batch_ids OUT NUMBER_TABLE,
        pn_pending_tran_ids OUT NUMBER_TABLE)
    IS
        CURSOR l_cur IS
            SELECT ADMIN_CMD_ID, ADMIN_CMD_TYPE_ID
              FROM PSS.ADMIN_CMD
             WHERE PAYMENT_SUBTYPE_KEY_ID = pn_payment_subtype_key_id
               AND PAYMENT_SUBTYPE_CLASS =  pv_payment_subtype_class
               AND ADMIN_CMD_STATE_ID = 1
               AND (pc_settle_processing_enabled = 'Y' OR ADMIN_CMD_TYPE_ID NOT IN(1,2,3,6,8))
               AND (pc_tran_processing_enabled = 'Y' OR ADMIN_CMD_TYPE_ID NOT IN(4,5,7))
               AND (pc_retry_only != 'Y' OR ADMIN_CMD_TYPE_ID IN(2, 3, 4, 5, 6, 7))
               AND (pc_allow_add_to_batch = 'Y' OR pv_payment_subtype_class NOT LIKE 'Authority::ISO8583%' OR ADMIN_CMD_TYPE_ID NOT IN(5))
             ORDER BY PRIORITY ASC, CREATED_UTC_TS ASC;
    BEGIN
        -- get the first command and translate it into a terminal batch or tran to process
        FOR l_rec IN l_cur LOOP
            IF l_rec.ADMIN_CMD_TYPE_ID IN(1,2,3,6,5,8) AND pv_payment_subtype_class NOT LIKE 'Authority::ISO8583%' THEN
                MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Settlement commands are not allowed for Payment Subtype Class ''' || pv_payment_subtype_class || '''');
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 1 THEN -- Settle All
                GET_PENDING_SETTLEMENTS(
                    pn_payment_subtype_key_id, 
                    pv_payment_subtype_class, 
                    'Y', 
                    pn_max_settlements,
                    pn_pending_terminal_batch_ids);
                IF pn_pending_terminal_batch_ids IS NULL OR pn_pending_terminal_batch_ids.COUNT < pn_max_settlements THEN
                    -- Mark command executed
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                END IF;
                IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
                    RETURN; -- found something to process so exit
                END IF;
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 2 THEN -- Retry All
                GET_RETRY_SETTLEMENTS(
                    pn_payment_subtype_key_id, 
                    pv_payment_subtype_class,
                    0,
                    pn_max_settlements, 
                    pn_pending_terminal_batch_ids);
                IF pn_pending_terminal_batch_ids IS NULL OR pn_pending_terminal_batch_ids.COUNT < pn_max_settlements THEN
                    -- Mark command executed
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                END IF;
                IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
                    RETURN; -- found something to process so exit
                END IF;   
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 3 THEN -- Retry Batch
                SELECT TERMINAL_BATCH_ID
                  BULK COLLECT INTO pn_pending_terminal_batch_ids
                  FROM (
                    SELECT C.TERMINAL_BATCH_ID
                      FROM (SELECT TERMINAL_ID, TERMINAL_BATCH_ID, TERMINAL_BATCH_OPEN_TS, 
                                   SUM(CASE WHEN TRAN_STATE_CD IN('R','N') THEN 1 ELSE 0 END) NUM_TRAN
                              FROM (SELECT T.TERMINAL_ID, 
                                           TB.TERMINAL_BATCH_ID,
                                           X.TRAN_ID,
                                           X.TRAN_STATE_CD,
                                           TB.TERMINAL_BATCH_OPEN_TS
                                      FROM PSS.TERMINAL T
                                      JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
                                      JOIN PSS.AUTH A ON TB.TERMINAL_BATCH_ID = A.TERMINAL_BATCH_ID
                                      JOIN PSS.TRAN X ON A.TRAN_ID = X.TRAN_ID 
                                     WHERE TB.TERMINAL_BATCH_OPEN_TS IS NOT NULL
                                       AND DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y') = 'N'
                                       AND T.TERMINAL_ID = pn_payment_subtype_key_id
                                     UNION 
                                    SELECT T.TERMINAL_ID, 
                                           TB.TERMINAL_BATCH_ID,
                                           X.TRAN_ID,
                                           X.TRAN_STATE_CD,
                                           TB.TERMINAL_BATCH_OPEN_TS
                                      FROM PSS.TERMINAL T
                                      JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
                                      JOIN PSS.REFUND R ON TB.TERMINAL_BATCH_ID = R.TERMINAL_BATCH_ID
                                      JOIN PSS.TRAN X ON R.TRAN_ID = X.TRAN_ID 
                                     WHERE TB.TERMINAL_BATCH_OPEN_TS IS NOT NULL
                                       AND DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y') = 'N'
                                       AND T.TERMINAL_ID = pn_payment_subtype_key_id) 
                             GROUP BY TERMINAL_ID, TERMINAL_BATCH_ID, TERMINAL_BATCH_OPEN_TS
                             HAVING COUNT(TRAN_STATE_CD) = SUM(CASE WHEN TRAN_STATE_CD IN('R', 'N', 'E') THEN 1 ELSE 0 END)) C
                      JOIN PSS.ADMIN_CMD_PARAM ACP ON C.TERMINAL_BATCH_ID = TO_NUMBER_OR_NULL(ACP.PARAM_VALUE)
                     WHERE C.NUM_TRAN > 0
                       AND ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'TERMINAL_BATCH_ID'
                   ORDER BY c.TERMINAL_BATCH_OPEN_TS ASC)
                 WHERE ROWNUM <= pn_max_settlements;
                IF pn_pending_terminal_batch_ids IS NULL OR pn_pending_terminal_batch_ids.COUNT < pn_max_settlements THEN
                    -- Mark command executed
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                END IF;
                IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
                    RETURN; -- found something to process so exit
                END IF;   
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 4 THEN -- Retry Tran
                SELECT TRAN_ID
                  BULK COLLECT INTO pn_pending_tran_ids
                  FROM (SELECT T.TRAN_ID 
                          FROM PSS.TRAN T
                          LEFT OUTER JOIN PSS.AUTH A   ON T.TRAN_ID = A.TRAN_ID
                          LEFT OUTER JOIN PSS.REFUND R ON T.TRAN_ID = R.TRAN_ID
                          JOIN PSS.ADMIN_CMD_PARAM ACP ON T.TRAN_ID = TO_NUMBER_OR_NULL(ACP.PARAM_VALUE)
                         WHERE T.TRAN_STATE_CD IN('I', 'J') /* transaction_incomplete, transaction_incomplete_error */
                           AND T.PAYMENT_SUBTYPE_KEY_ID = pn_payment_subtype_key_id 
                           AND T.PAYMENT_SUBTYPE_CLASS  = pv_payment_subtype_class
                           AND ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                           AND ACP.PARAM_NAME = 'TRAN_ID'
                           AND (pc_allow_add_to_batch = 'Y' OR NVL(A.TERMINAL_BATCH_ID, R.TERMINAL_BATCH_ID) IS NOT NULL)
                         GROUP BY T.TRAN_ID
                         ORDER BY T.TRAN_ID)
                 WHERE ROWNUM <= pn_max_trans;
                IF pn_pending_tran_ids IS NULL OR pn_pending_tran_ids.COUNT < pn_max_trans THEN
                    -- Mark command executed
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                END IF;
                IF pn_pending_tran_ids IS NOT NULL AND pn_pending_tran_ids.COUNT > 0 THEN 
                    RETURN; -- found something to process so exit
                END IF;
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 5 THEN -- Force Tran
                 DECLARE
                    ln_tran_id PSS.TRAN.TRAN_ID%TYPE;
                    lv_force_reason PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_RESP_DESC%TYPE;
                BEGIN
                    SELECT TO_NUMBER_OR_NULL(ACP.PARAM_VALUE)
                      INTO ln_tran_id
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'TRAN_ID';
                    SELECT ACP.PARAM_VALUE
                      INTO lv_force_reason
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'FORCE_REASON';    
                    FORCE_TRAN(pn_payment_subtype_key_id, pv_payment_subtype_class, ln_tran_id, lv_force_reason);
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Parameter ''TRAN_ID'' not found');
                    WHEN WRONG_TRAN_STATE THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, SQLERRM);
                    WHEN OTHERS THEN
                        IF SQLCODE IN(1, 20556) THEN
                            MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, SQLERRM);
                        ELSE
                            RAISE;
                        END IF;
                END;
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 6 THEN -- Force Batch
                DECLARE
                    ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
                    lv_force_reason PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_RESP_DESC%TYPE;
                BEGIN
                    SELECT TO_NUMBER_OR_NULL(ACP.PARAM_VALUE)
                      INTO ln_terminal_batch_id
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'TERMINAL_BATCH_ID';
                    SELECT ACP.PARAM_VALUE
                      INTO lv_force_reason
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'FORCE_REASON';    
                    FORCE_SETTLE_BATCH(ln_terminal_batch_id, lv_force_reason);
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Parameter ''TERMINAL_BATCH_ID'' or ''FORCE_REASON'' not found');
                    WHEN WRONG_SETTLE_BATCH_STATE THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, SQLERRM);
                    WHEN OTHERS THEN
                        IF SQLCODE = 1 THEN
                            MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, SQLERRM);
                        ELSE
                            RAISE;
                        END IF;
                END;
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 7 THEN -- Error Tran
                 DECLARE
                    ln_tran_id PSS.TRAN.TRAN_ID%TYPE;
                    lv_error_reason PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_RESP_DESC%TYPE;
                BEGIN
                    SELECT TO_NUMBER_OR_NULL(ACP.PARAM_VALUE)
                      INTO ln_tran_id
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'TRAN_ID';
                    SELECT ACP.PARAM_VALUE
                      INTO lv_error_reason
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'ERROR_REASON';    
                    ERROR_TRAN(ln_tran_id, lv_error_reason);
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Parameter ''TRAN_ID'' not found');
                    WHEN WRONG_TRAN_STATE THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, SQLERRM);
                    WHEN OTHERS THEN
                        IF SQLCODE = 1 THEN
                            MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, SQLERRM);
                        ELSE
                            RAISE;
                        END IF;
                END;
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 8 THEN -- Settle Batch
                DECLARE
                    ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
                    lc_terminal_batch_closed CHAR;
                    ln_num_tran PLS_INTEGER;
                    ln_count_tran PLS_INTEGER;
                    ln_okay_tran PLS_INTEGER;          
                BEGIN
                    SELECT MAX(TO_NUMBER_OR_NULL(ACP.PARAM_VALUE))
                      INTO ln_terminal_batch_id
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'TERMINAL_BATCH_ID';
                    IF ln_terminal_batch_id IS NULL THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Parameter ''TERMINAL_BATCH_ID'' not found');
                    ELSE
                        SELECT DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y'), 
                           SUM(CASE WHEN X.TRAN_STATE_CD IN('T') THEN 1 ELSE 0 END) NUM_TRAN,
                           COUNT(X.TRAN_STATE_CD) COUNT_TRAN,
                           SUM(CASE WHEN X.TRAN_STATE_CD IN('T', 'E') THEN 1 ELSE 0 END) OKAY_TRAN
                          INTO lc_terminal_batch_closed,
                               ln_num_tran,
                               ln_count_tran,
                               ln_okay_tran
                          FROM PSS.TERMINAL_BATCH TB
                          JOIN (SELECT A.TRAN_ID, A.TERMINAL_BATCH_ID
                                  FROM PSS.AUTH A
                                UNION
                                SELECT R.TRAN_ID, R.TERMINAL_BATCH_ID
                                  FROM PSS.REFUND R) A ON TB.TERMINAL_BATCH_ID = A.TERMINAL_BATCH_ID
                          JOIN PSS.TRAN X ON A.TRAN_ID = X.TRAN_ID
                         WHERE TB.TERMINAL_BATCH_ID = ln_terminal_batch_id 
                         GROUP BY DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y');
                        IF lc_terminal_batch_closed != 'N' THEN
                            MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Terminal Batch '|| ln_terminal_batch_id ||' is already closed');
                        ELSIF ln_okay_tran != ln_count_tran THEN
                            NULL; -- Not ready for settlement yet
                        ELSIF ln_num_tran = 0 THEN
                            MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID); -- No non-error transaction, so mark as complete and continue
                        ELSE
                            SELECT ln_terminal_batch_id
                              BULK COLLECT INTO pn_pending_terminal_batch_ids
                              FROM DUAL;
                            -- Mark command executed                          
                            MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                            RETURN; -- found something to process so exit
                        END IF;
                    END IF;
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Terminal Batch ' ||ln_terminal_batch_id||' not found');
                END;
            ELSE
                -- Unknown cmd type
                NULL;
            END IF;
        END LOOP;
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE GET_PENDING_ACTIONS(
        pn_payment_subtype_key_id PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE, 
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_settlement_retry_interval NUMBER,
        pn_tran_retry_interval NUMBER,
        pn_max_settlements PLS_INTEGER, 
        pn_max_trans PLS_INTEGER,
        pn_terminal_state_id OUT PSS.TERMINAL.TERMINAL_STATE_ID%TYPE,
        pn_pending_terminal_batch_ids OUT NUMBER_TABLE,
        pn_pending_tran_ids OUT NUMBER_TABLE,
        pn_terminal_batch_id OUT PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE,
        pc_call_again_flag OUT VARCHAR2)
    IS
        ln_batch_max_trans PSS.TERMINAL.TERMINAL_BATCH_MAX_TRAN%TYPE;
        ln_allowed_trans PLS_INTEGER;
        lc_settle_processing_enabled CHAR(1);
        lc_tran_processing_enabled CHAR(1);
        lv_terminal_global_token_cd PSS.TERMINAL.GLOBAL_TOKEN_CD%TYPE;
        lc_terminal_capture_flag PSS.TERMINAL_BATCH.TERMINAL_CAPTURE_FLAG%TYPE;
        ln_cnt PLS_INTEGER;
    BEGIN
        SELECT NVL(MAX(POSM_SETTING_VALUE), 'N')
          INTO lc_tran_processing_enabled
          FROM PSS.POSM_SETTING
         WHERE POSM_SETTING_NAME = 'TRAN_PROCESSING_ENABLED';
        
        -- Get terminal state id
        IF pv_payment_subtype_class  LIKE 'Authority::ISO8583%' THEN -- this is same as in APP_LAYER.VW_PAYMENT_SUBTYPE_DETAIL
            SELECT NVL(MAX(POSM_SETTING_VALUE), 'N')
              INTO lc_settle_processing_enabled
              FROM PSS.POSM_SETTING
             WHERE POSM_SETTING_NAME = 'SETTLE_PROCESSING_ENABLED';
        
            SELECT T.TERMINAL_STATE_ID, COALESCE(T.TERMINAL_BATCH_MAX_TRAN, AU.AUTHORITY_BATCH_MAX_TRAN, 999), AU.TERMINAL_CAPTURE_FLAG
              INTO pn_terminal_state_id, ln_batch_max_trans, lc_terminal_capture_flag
              FROM PSS.TERMINAL T
              JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
              JOIN AUTHORITY.AUTHORITY AU ON M.AUTHORITY_ID = AU.AUTHORITY_ID                       
             WHERE T.TERMINAL_ID = pn_payment_subtype_key_id;
            IF pn_terminal_state_id = 5 THEN
                -- see if there are retry admin commands
                GET_ACTIONS_FROM_ADMIN_CMDS(
                    pn_payment_subtype_key_id, 
                    pv_payment_subtype_class, 
                    pn_max_settlements, 
                    0,
                    'N', -- pc_allow_add_to_batch
                    'Y', -- pc_retry_only
                    lc_settle_processing_enabled,
                    lc_tran_processing_enabled,
                    pn_pending_terminal_batch_ids,
                    pn_pending_tran_ids);
                IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
                    RETURN; -- Early Exit if we found something to process
                END IF;
                IF lc_settle_processing_enabled = 'Y' THEN
                    -- Only settlement retry is available
                    -- Find retyable batches
                    GET_RETRY_SETTLEMENTS(
                        pn_payment_subtype_key_id, 
                        pv_payment_subtype_class,
                        pn_settlement_retry_interval,
                        pn_max_settlements, 
                        pn_pending_terminal_batch_ids);
                    IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
                        RETURN; -- Early Exit if we found something to process
                    END IF;  
                    -- Make sure terminal should be in state 5 (that there is an unclosed batch)
                    SELECT COUNT(*)
                      INTO ln_cnt
                      FROM PSS.TERMINAL_BATCH TB
                     WHERE TB.TERMINAL_BATCH_OPEN_TS IS NOT NULL
                       AND DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y') = 'N'
                       AND TB.TERMINAL_ID = pn_payment_subtype_key_id;
                    IF ln_cnt = 0 THEN
                        UPDATE PSS.TERMINAL 
                           SET TERMINAL_STATE_ID = 3
                         WHERE TERMINAL_ID = pn_payment_subtype_key_id;
                    END IF;
                END IF;
                RETURN; -- if terminal is in state 5, we don't process anything else
            ELSIF pn_terminal_state_id != 3 THEN
                RETURN; -- Terminal not locked for processing
            END IF;
        ELSE
            lv_terminal_global_token_cd := GET_TERMINAL_GLOBAL_TOKEN(pv_payment_subtype_class, pn_payment_subtype_key_id);
            IF lv_terminal_global_token_cd IS NULL THEN
                pn_terminal_state_id := 1;
                RETURN;  -- Terminal not locked for processing
            ELSE
                pn_terminal_state_id := 3;
                lc_settle_processing_enabled := 'N';
            END IF;
        END IF;
        
        IF lc_tran_processing_enabled != 'Y' AND lc_settle_processing_enabled != 'Y' THEN
            RETURN; -- we are not doing any processing right now
        END IF;
        
        -- limit num of trans if need be
        IF ln_batch_max_trans IS NOT NULL THEN
            pn_terminal_batch_id := GET_AVAILABLE_TERMINAL_BATCH(pn_payment_subtype_key_id, ln_batch_max_trans, TRUE);
            IF ln_batch_max_trans < pn_max_trans THEN
                ln_allowed_trans := ln_batch_max_trans;
            ELSE
                ln_allowed_trans := pn_max_trans;
            END IF;
        ELSE
            ln_allowed_trans := pn_max_trans;
        END IF;
        
        -- Check pending commands and process them first
        IF ln_allowed_trans <= 0 THEN            
            GET_ACTIONS_FROM_ADMIN_CMDS(
                pn_payment_subtype_key_id, 
                pv_payment_subtype_class, 
                pn_max_settlements, 
                pn_max_trans,
                'N', -- pc_allow_add_to_batch
                'N', -- pc_retry_only
                lc_settle_processing_enabled,
                lc_tran_processing_enabled,
                pn_pending_terminal_batch_ids,
                pn_pending_tran_ids);
        ELSE
            GET_ACTIONS_FROM_ADMIN_CMDS(
                pn_payment_subtype_key_id, 
                pv_payment_subtype_class, 
                pn_max_settlements, 
                ln_allowed_trans,
                'Y', -- pc_allow_add_to_batch
                'N', -- pc_retry_only
                lc_settle_processing_enabled,
                lc_tran_processing_enabled,
                pn_pending_terminal_batch_ids,
                pn_pending_tran_ids);
        END IF;
        IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
            RETURN; -- Early Exit if we found something to process
        ELSIF pn_pending_tran_ids IS NOT NULL AND pn_pending_tran_ids.COUNT > 0 THEN
            RETURN; -- Early Exit if we found something to process
        END IF;
        
        -- Check for Settlements
        IF lc_settle_processing_enabled = 'Y' THEN
            GET_PENDING_SETTLEMENTS(
                pn_payment_subtype_key_id, 
                pv_payment_subtype_class, 
                'N', 
                pn_max_settlements,
                pn_pending_terminal_batch_ids);
            IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
                RETURN; -- Early Exit if we found something to process
            END IF;
            GET_RETRY_SETTLEMENTS(
                pn_payment_subtype_key_id, 
                pv_payment_subtype_class,
                pn_settlement_retry_interval,
                pn_max_settlements, 
                pn_pending_terminal_batch_ids);
            IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
                RETURN; -- Early Exit if we found something to process
            END IF;
        END IF;
        
        IF lc_tran_processing_enabled != 'Y' THEN
            RETURN; -- we are not doing any tran processing right now
        END IF;
        IF lc_terminal_capture_flag = 'Y' AND pn_terminal_batch_id IS NOT NULL THEN
            SELECT TRAN_ID
              BULK COLLECT INTO pn_pending_tran_ids
              FROM (SELECT /*+ index(T IDX_TRAN_STATE_CD) */ T.TRAN_ID, T.TRAN_UPLOAD_TS
                      FROM PSS.TRAN T
                     WHERE T.TRAN_STATE_CD IN ('8', 'A', 'W') /* processed_server_batch, processing_server_tran, processed_server_auth_pending_reversal */
                       AND T.PAYMENT_SUBTYPE_KEY_ID = pn_payment_subtype_key_id
                       AND T.PAYMENT_SUBTYPE_CLASS  = pv_payment_subtype_class
                     ORDER BY T.TRAN_UPLOAD_TS ASC, T.TRAN_ID ASC)
             WHERE ROWNUM <= ln_allowed_trans;
            IF pn_pending_tran_ids IS NULL OR pn_pending_tran_ids.COUNT = 0 THEN
                RETURN; -- early exit
            END IF;
        
            -- prepare transactions
            -- NOTE: update tran first so as to lock it
            UPDATE PSS.TRAN X
			   SET TRAN_STATE_CD = CASE 
                    WHEN X.AUTH_HOLD_USED = 'Y' THEN 'T' 
                    WHEN 0 != (
                        SELECT NVL(SUM(TLI.TRAN_LINE_ITEM_AMOUNT * TLI.TRAN_LINE_ITEM_QUANTITY), 0) 
                          FROM PSS.TRAN_LINE_ITEM TLI 
                         WHERE TLI.TRAN_ID = X.TRAN_ID 
                           AND TLI.TRAN_LINE_ITEM_BATCH_TYPE_CD = 'A'
                    ) THEN 'T' 
                    ELSE 'C' END
			 WHERE X.TRAN_ID IN(SELECT COLUMN_VALUE FROM TABLE(pn_pending_tran_ids)); -- NOTE: using MEMBER OF causes a FTS			
            INSERT INTO PSS.AUTH (
                        AUTH_ID,
                        TRAN_ID,
                        AUTH_TYPE_CD,
                        AUTH_STATE_ID,
                        AUTH_PARSED_ACCT_DATA,
                        ACCT_ENTRY_METHOD_CD,
                        AUTH_AMT,
                        AUTH_TS,
                        TERMINAL_BATCH_ID,
                        TRACE_NUMBER)
                 SELECT PSS.SEQ_AUTH_ID.NEXTVAL,
                        TRAN_ID,
                        CASE 
                            WHEN 0 != SALE_AMOUNT THEN 'U' 
                            ELSE 'C' END,
                        6,
                        AUTH_PARSED_ACCT_DATA,
                        ACCT_ENTRY_METHOD_CD,
                        SALE_AMOUNT,	    
                        SYSDATE,
                        pn_terminal_batch_id,
                        TRACE_NUMBER
                   FROM (SELECT /*+INDEX(X PK_TRAN)*/
                        A.TRAN_ID,
                        NVL(S.SALE_AMOUNT, 0) SALE_AMOUNT,
                        MAX(a.AUTH_PARSED_ACCT_DATA) AUTH_PARSED_ACCT_DATA,
                        MAX(a.ACCT_ENTRY_METHOD_CD) ACCT_ENTRY_METHOD_CD,
                        MAX(a.TRACE_NUMBER) TRACE_NUMBER
                   FROM PSS.AUTH a
                   JOIN PSS.TRAN X ON A.TRAN_ID = X.TRAN_ID
                   LEFT OUTER JOIN PSS.SALE S ON S.TRAN_ID = A.TRAN_ID AND S.SALE_TYPE_CD = 'A'
                  WHERE X.TRAN_STATE_CD = 'T'
                    AND A.AUTH_TYPE_CD IN('L', 'N') AND (A.AUTH_RESULT_CD IN('Y', 'P', 'V', 'C', 'M') OR (A.AUTH_RESULT_CD = 'F' AND NVL(S.SALE_AMOUNT, 0) = 0))
                    AND A.TRAN_ID IN(SELECT COLUMN_VALUE FROM TABLE(pn_pending_tran_ids))  -- NOTE: using MEMBER OF causes a FTS
                    GROUP BY A.TRAN_ID, NVL(S.SALE_AMOUNT, 0));
            UPDATE PSS.REFUND
               SET TERMINAL_BATCH_ID = pn_terminal_batch_id,
                   REFUND_STATE_ID = 6
             WHERE TRAN_ID IN(SELECT COLUMN_VALUE FROM TABLE(pn_pending_tran_ids)); 
            IF pn_pending_tran_ids.COUNT >= ln_batch_max_trans THEN
                pn_pending_terminal_batch_ids := NUMBER_TABLE(pn_terminal_batch_id);
            ELSIF pn_pending_tran_ids.COUNT >= pn_max_trans THEN
                pc_call_again_flag := 'Y';
            END IF;
            pn_pending_tran_ids := NULL;
        ELSE
        -- Find retry trans
        IF ln_allowed_trans <= 0 THEN
             -- We can only process trans already in a batch
             SELECT DISTINCT TRAN_ID
              BULK COLLECT INTO pn_pending_tran_ids
              FROM (SELECT /*+ USE_NL(T,A,R) index(T IDX_TRAN_STATE_CD) index(A IDX_AUTH_TRAN_ID) */ T.TRAN_ID, T.TRAN_UPLOAD_TS
                      FROM PSS.TRAN T
                      LEFT OUTER JOIN PSS.AUTH A   ON T.TRAN_ID = A.TRAN_ID
                      LEFT OUTER JOIN PSS.REFUND R ON T.TRAN_ID = R.TRAN_ID
                     WHERE T.TRAN_STATE_CD = 'I' /* transaction_incomplete */
                       AND T.PAYMENT_SUBTYPE_KEY_ID = pn_payment_subtype_key_id 
                       AND T.PAYMENT_SUBTYPE_CLASS  = pv_payment_subtype_class
                       AND NVL(A.TERMINAL_BATCH_ID, R.TERMINAL_BATCH_ID) IS NOT NULL
                     GROUP BY T.TRAN_ID, T.TRAN_UPLOAD_TS
                    HAVING SYSDATE >= pn_tran_retry_interval + GREATEST(NVL(MAX(A.AUTH_TS), MIN_DATE), NVL(MAX(R.LAST_UPDATED_TS), MIN_DATE))
                     ORDER BY T.TRAN_UPLOAD_TS ASC, T.TRAN_ID ASC)
             WHERE ROWNUM <= pn_max_trans;
                pn_terminal_batch_id := NULL;
            RETURN; -- we can't process trans right now
        END IF;
        
        SELECT DISTINCT TRAN_ID
          BULK COLLECT INTO pn_pending_tran_ids
          FROM (SELECT /*+ USE_NL(T,A,R) index(T IDX_TRAN_STATE_CD) index(A IDX_AUTH_TRAN_ID) */ T.TRAN_ID, T.TRAN_UPLOAD_TS
                  FROM PSS.TRAN T
                  LEFT OUTER JOIN PSS.AUTH A   ON T.TRAN_ID = A.TRAN_ID
                  LEFT OUTER JOIN PSS.REFUND R ON T.TRAN_ID = R.TRAN_ID
                 WHERE T.TRAN_STATE_CD = 'I' /* transaction_incomplete */
                   AND T.PAYMENT_SUBTYPE_KEY_ID = pn_payment_subtype_key_id 
                   AND T.PAYMENT_SUBTYPE_CLASS  = pv_payment_subtype_class
                 GROUP BY T.TRAN_ID, T.TRAN_UPLOAD_TS
                HAVING SYSDATE >= pn_tran_retry_interval + GREATEST(NVL(MAX(A.AUTH_TS), MIN_DATE), NVL(MAX(R.LAST_UPDATED_TS), MIN_DATE))
                 ORDER BY T.TRAN_UPLOAD_TS ASC, T.TRAN_ID ASC)
         WHERE ROWNUM <= ln_allowed_trans;
        IF pn_pending_tran_ids.COUNT > 0 THEN
            RETURN; -- Early Exit if we found something to process
        END IF;
          
        -- get new pending trans
        SELECT TRAN_ID
          BULK COLLECT INTO pn_pending_tran_ids
          FROM (SELECT /*+ index(T IDX_TRAN_STATE_CD) */ T.TRAN_ID, T.TRAN_UPLOAD_TS
                  FROM PSS.TRAN T
                 WHERE T.TRAN_STATE_CD IN ('8', '9', 'A', 'W') /* processed_server_batch, processed_server_batch_intended, processing_server_tran, processed_server_auth_pending_reversal */
                   AND (T.TRAN_STATE_CD != '9' OR T.AUTH_HOLD_USED != 'Y') /* Filters out the Intended Transactions which have an Auth Hold */
                   AND T.PAYMENT_SUBTYPE_KEY_ID = pn_payment_subtype_key_id
                   AND T.PAYMENT_SUBTYPE_CLASS  = pv_payment_subtype_class
                 ORDER BY T.TRAN_UPLOAD_TS ASC, T.TRAN_ID ASC)
         WHERE ROWNUM <= ln_allowed_trans;        
        END IF;
    END;
    
    PROCEDURE SET_UNIQUE_SECONDS(
        pn_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE,
        pn_authority_id PSS.TERMINAL_BATCH.AUTHORITY_ID%TYPE,
        pn_unique_seconds IN OUT PSS.TERMINAL_BATCH.UNIQUE_SECONDS%TYPE)
    IS
    BEGIN
        UPDATE PSS.TERMINAL_BATCH
           SET UNIQUE_SECONDS = pn_unique_seconds,
               AUTHORITY_ID = pn_authority_id
         WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id;
    EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
            pn_unique_seconds := pn_unique_seconds + 1;
            SET_UNIQUE_SECONDS(pn_terminal_batch_id, pn_authority_id, pn_unique_seconds);
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE GET_OR_CREATE_SETTLEMENT_BATCH(
        pn_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE,
        pn_settlement_batch_id OUT PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_prior_attempts OUT PLS_INTEGER,
        pc_upload_needed_flag OUT VARCHAR2)
    IS
        lc_terminal_capture_flag PSS.TERMINAL_BATCH.TERMINAL_CAPTURE_FLAG%TYPE;
        ln_terminal_id PSS.TERMINAL_BATCH.TERMINAL_ID%TYPE;
        ln_unique_seconds PSS.TERMINAL_BATCH.UNIQUE_SECONDS%TYPE;
        ln_authority_id PSS.TERMINAL_BATCH.AUTHORITY_ID%TYPE;
    BEGIN
        -- check for an incomplete settlement_batch
        SELECT MAX(SB.SETTLEMENT_BATCH_ID), TB.TERMINAL_ID, TB.TERMINAL_CAPTURE_FLAG, TB.UNIQUE_SECONDS
          INTO pn_settlement_batch_id, ln_terminal_id, lc_terminal_capture_flag, ln_unique_seconds
          FROM PSS.TERMINAL_BATCH TB
          LEFT OUTER JOIN PSS.SETTLEMENT_BATCH SB ON TB.TERMINAL_BATCH_ID = SB.TERMINAL_BATCH_ID AND SB.SETTLEMENT_BATCH_STATE_ID = 4
         WHERE TB.TERMINAL_BATCH_ID = pn_terminal_batch_id
         GROUP BY TB.TERMINAL_ID, TB.TERMINAL_CAPTURE_FLAG, TB.UNIQUE_SECONDS;
        IF pn_settlement_batch_id IS NULL THEN
            SELECT PSS.SEQ_SETTLEMENT_BATCH_ID.NEXTVAL
              INTO pn_settlement_batch_id
              FROM DUAL;            
            
            INSERT INTO PSS.SETTLEMENT_BATCH(
                SETTLEMENT_BATCH_ID,
                SETTLEMENT_BATCH_STATE_ID,
                SETTLEMENT_BATCH_START_TS,
                TERMINAL_BATCH_ID
            ) VALUES (
                pn_settlement_batch_id,
                4,
                SYSDATE,
                pn_terminal_batch_id);
            /* Do this after transaction is uploaded (SALEd) with the Authority)    
            -- add all trans and update their tran state cd
            INSERT INTO PSS.TRAN_SETTLEMENT_BATCH(
                SETTLEMENT_BATCH_ID,
                AUTH_ID,
                TRAN_ID,
                TRAN_SETTLEMENT_B_AMT)
              SELECT DISTINCT 
                     pn_settlement_batch_id,
                     FIRST_VALUE(A.AUTH_ID) OVER (PARTITION BY T.TRAN_ID ORDER BY A.CREATED_TS DESC, A.AUTH_ID DESC) AUTH_ID, 
                     T.TRAN_ID, 
                     FIRST_VALUE(A.AUTH_AMT) OVER (PARTITION BY T.TRAN_ID ORDER BY A.CREATED_TS DESC, A.AUTH_ID DESC) AUTH_AMT
                FROM PSS.TRAN T
                JOIN PSS.AUTH A ON T.TRAN_ID = A.TRAN_ID
               WHERE A.TERMINAL_BATCH_ID = pn_terminal_batch_id
                 AND (A.AUTH_STATE_ID = 2
                      OR (A.AUTH_STATE_ID = 4 AND A.AUTH_TYPE_CD = 'N' AND A.AUTH_HOLD_USED = 'Y'));
            INSERT INTO PSS.REFUND_SETTLEMENT_BATCH(
                SETTLEMENT_BATCH_ID,
                REFUND_ID,
                TRAN_ID,
                REFUND_SETTLEMENT_B_AMT)
              SELECT DISTINCT 
                     pn_settlement_batch_id,
                     FIRST_VALUE(R.REFUND_ID) OVER (PARTITION BY T.TRAN_ID ORDER BY R.CREATED_TS DESC, R.REFUND_ID DESC) AUTH_ID, 
                     T.TRAN_ID, 
                     FIRST_VALUE(R.REFUND_AMT) OVER (PARTITION BY T.TRAN_ID ORDER BY R.CREATED_TS DESC, R.REFUND_ID DESC) REFUND_AMT
                FROM PSS.TRAN T
                JOIN PSS.REFUND R ON T.TRAN_ID = R.TRAN_ID
               WHERE R.TERMINAL_BATCH_ID = pn_terminal_batch_id
                 AND R.REFUND_STATE_ID IN(1);
            */
        END IF;
        UPDATE PSS.TRAN 
           SET TRAN_STATE_CD = 'S'
         WHERE TRAN_STATE_CD IN('R', 'N', 'T')
           AND TRAN_ID IN(
            SELECT A.TRAN_ID
              FROM PSS.AUTH A 
             WHERE A.TERMINAL_BATCH_ID = pn_terminal_batch_id
               AND (A.AUTH_STATE_ID IN(2, 5, 6)
                  OR (A.AUTH_STATE_ID = 4 AND A.AUTH_TYPE_CD = 'N' AND A.AUTH_HOLD_USED = 'Y'))
            UNION ALL
            SELECT R.TRAN_ID
              FROM PSS.REFUND R
             WHERE R.TERMINAL_BATCH_ID = pn_terminal_batch_id
               AND R.REFUND_STATE_ID IN(1, 6));
        IF lc_terminal_capture_flag != 'Y' THEN
            UPDATE PSS.TERMINAL
               SET TERMINAL_STATE_ID = 5
             WHERE TERMINAL_ID = ln_terminal_id;
        END IF;
        SELECT COUNT(*)
          INTO pn_prior_attempts
    	  FROM PSS.SETTLEMENT_BATCH  
    	 WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id 
    	   AND SETTLEMENT_BATCH_STATE_ID IN(2, 3);
        IF pn_prior_attempts > 0 THEN
            SELECT DECODE(SETTLEMENT_BATCH_STATE_ID, 2, 'N', 3, 'Y')
              INTO pc_upload_needed_flag
              FROM (
                SELECT SETTLEMENT_BATCH_END_TS, SETTLEMENT_BATCH_ID, SETTLEMENT_BATCH_STATE_ID
                  FROM PSS.SETTLEMENT_BATCH  
                 WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id 
                   AND SETTLEMENT_BATCH_STATE_ID IN(2, 3)           
                 ORDER BY SETTLEMENT_BATCH_END_TS DESC, SETTLEMENT_BATCH_ID DESC)
             WHERE ROWNUM = 1;
        ELSE
            pc_upload_needed_flag := 'N';
        END IF;
        IF lc_terminal_capture_flag = 'Y' AND ln_unique_seconds IS NULL THEN
            SELECT ROUND(DATE_TO_MILLIS(SYSDATE) / 1000), M.AUTHORITY_ID
              INTO ln_unique_seconds, ln_authority_id
              FROM PSS.TERMINAL T
              JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
             WHERE T.TERMINAL_ID = ln_terminal_id;
            SET_UNIQUE_SECONDS(pn_terminal_batch_id, ln_authority_id, ln_unique_seconds);
        END IF;
    END;
    /*
    FUNCTION IS_LAST_TERMINAL_BATCH_OPEN(
        pn_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE)
    RETURN PLS_INTEGER
    IS
        ln_is_open PLS_INTEGER;
    BEGIN
        SELECT DECODE(TERMINAL_BATCH_CLOSE_TS, NULL, 1, 0)
          INTO ln_is_open
          FROM (SELECT TERMINAL_BATCH_NUM, 
                       TERMINAL_BATCH_CYCLE_NUM, 
                       TERMINAL_BATCH_CLOSE_TS
                  FROM PSS.TERMINAL_BATCH
                 WHERE TERMINAL_ID = pn_terminal_id
                 ORDER BY TERMINAL_BATCH_CYCLE_NUM DESC,
                          TERMINAL_BATCH_NUM DESC)
         WHERE ROWNUM = 1;
        RETURN ln_is_open;
    END;
    */
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE UPDATE_PROCESSED_BATCH(
        pn_settlement_batch_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pv_authority_response_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
        pv_authority_response_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
        pd_batch_closed_ts PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_END_TS%TYPE)
    IS
        ln_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE;
        ln_settlement_batch_state_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_STATE_ID%TYPE;
        ln_terminal_capture_flag PSS.TERMINAL_BATCH.TERMINAL_CAPTURE_FLAG%TYPE;
    BEGIN
        SELECT SB.SETTLEMENT_BATCH_STATE_ID, TB.TERMINAL_CAPTURE_FLAG
          INTO ln_settlement_batch_state_id, ln_terminal_capture_flag
          FROM PSS.SETTLEMENT_BATCH SB
          JOIN PSS.TERMINAL_BATCH TB ON TB.TERMINAL_BATCH_ID = SB.TERMINAL_BATCH_ID
         WHERE SB.SETTLEMENT_BATCH_ID = pn_settlement_batch_id;
        IF ln_settlement_batch_state_id = 1 OR (ln_settlement_batch_state_id IN(1,2,3,7) AND pc_auth_result_cd IN('P')) THEN
            RAISE_APPLICATION_ERROR(-20100, 'Settlement batch ' || pn_settlement_batch_id || ' was already processed');
        END IF;
        -- update the settlement batch
        UPDATE PSS.SETTLEMENT_BATCH
           SET SETTLEMENT_BATCH_STATE_ID = DECODE(pc_auth_result_cd, 'Y', 1, 'P', 7, 'N', 2, 'F', 3, 'O', 2, 'R', 2),
               SETTLEMENT_BATCH_RESP_CD = pv_authority_response_cd,
               SETTLEMENT_BATCH_RESP_DESC = pv_authority_response_desc,
               SETTLEMENT_BATCH_REF_CD = pv_authority_ref_cd,
               SETTLEMENT_BATCH_END_TS = pd_batch_closed_ts
         WHERE SETTLEMENT_BATCH_ID = pn_settlement_batch_id;
            
        -- add tran settlement record for each tran in batch
        INSERT INTO PSS.TRAN_SETTLEMENT_BATCH(
               SETTLEMENT_BATCH_ID,
               AUTH_ID,
               TRAN_ID,
               TRAN_SETTLEMENT_B_AMT,
               TRAN_SETTLEMENT_B_RESP_CD,
               TRAN_SETTLEMENT_B_RESP_DESC)
        SELECT pn_settlement_batch_id,
               SA.AUTH_ID,
               X.TRAN_ID,
               DECODE(pc_auth_result_cd, 'Y', SA.AUTH_AMT_APPROVED),
               pv_authority_response_cd,
               pv_authority_response_desc
          FROM PSS.AUTH SA 
          JOIN PSS.TRAN X ON SA.TRAN_ID = X.TRAN_ID
         WHERE SA.TERMINAL_BATCH_ID = pn_terminal_batch_id
           AND SA.AUTH_TYPE_CD IN('U','S','O','A','D','C','E','V','I', 'N')
           AND ((SA.AUTH_STATE_ID IN(2, 5) AND SA.AUTH_TYPE_CD != 'N')
                OR (SA.AUTH_STATE_ID IN(6) AND ln_terminal_capture_flag = 'Y' AND SA.AUTH_TYPE_CD != 'N')
                OR (SA.AUTH_STATE_ID = 4 AND SA.AUTH_TYPE_CD = 'N' AND SA.AUTH_HOLD_USED = 'Y'))
           AND X.TRAN_STATE_CD IN('S');
        -- update the tran_state_cd of all sales in the batch
        UPDATE PSS.TRAN X
           SET TRAN_STATE_CD = (SELECT AFTER_SETTLE_TRAN_STATE_CD(sa.AUTH_TYPE_CD, NULL, pc_auth_result_cd, X.TRAN_DEVICE_RESULT_TYPE_CD)
            FROM PSS.AUTH SA 
           WHERE SA.TRAN_ID = X.TRAN_ID 
		     AND SA.TERMINAL_BATCH_ID = pn_terminal_batch_id
             AND SA.AUTH_TYPE_CD IN('U','S','O','A','D','C','E','V','I', 'N')
             AND ((SA.AUTH_STATE_ID IN(2, 5) AND SA.AUTH_TYPE_CD != 'N')
                OR (SA.AUTH_STATE_ID IN(6) AND ln_terminal_capture_flag = 'Y' AND SA.AUTH_TYPE_CD != 'N')
                OR (SA.AUTH_STATE_ID = 4 AND SA.AUTH_TYPE_CD = 'N' AND SA.AUTH_HOLD_USED = 'Y')))
         WHERE X.TRAN_ID IN(SELECT SA.TRAN_ID 
          FROM PSS.AUTH SA 
         WHERE SA.TERMINAL_BATCH_ID = pn_terminal_batch_id
           AND SA.AUTH_TYPE_CD IN('U','S','O','A','D','C','E','V','I', 'N')
           AND ((SA.AUTH_STATE_ID IN(2, 5) AND SA.AUTH_TYPE_CD != 'N')
                OR (SA.AUTH_STATE_ID IN(6) AND ln_terminal_capture_flag = 'Y' AND SA.AUTH_TYPE_CD != 'N')
                OR (SA.AUTH_STATE_ID = 4 AND SA.AUTH_TYPE_CD = 'N' AND SA.AUTH_HOLD_USED = 'Y')))
           AND X.TRAN_STATE_CD IN('S');
       
        -- add tran settlement record for each tran in batch
        INSERT INTO PSS.REFUND_SETTLEMENT_BATCH(
               SETTLEMENT_BATCH_ID,
               REFUND_ID,
               TRAN_ID,
               REFUND_SETTLEMENT_B_AMT,
               REFUND_SETTLEMENT_B_RESP_CD,
               REFUND_SETTLEMENT_B_RESP_DESC)
        SELECT pn_settlement_batch_id,
               R.REFUND_ID,
               X.TRAN_ID,
               DECODE(pc_auth_result_cd, 'Y', R.REFUND_AMT),
               pv_authority_response_cd,
               pv_authority_response_desc
          FROM PSS.REFUND R  
          JOIN PSS.TRAN X ON R.TRAN_ID = X.TRAN_ID
         WHERE R.TERMINAL_BATCH_ID = pn_terminal_batch_id
           AND (R.REFUND_STATE_ID IN(1) OR (ln_terminal_capture_flag = 'Y' AND R.REFUND_STATE_ID IN(6)))
           AND X.TRAN_STATE_CD IN('S');
        -- Update refunds in batch
        UPDATE PSS.TRAN X
           SET TRAN_STATE_CD =           
           (SELECT AFTER_SETTLE_TRAN_STATE_CD(NULL, R.REFUND_TYPE_CD, pc_auth_result_cd, X.TRAN_DEVICE_RESULT_TYPE_CD)               
            FROM PSS.REFUND R
           WHERE R.TRAN_ID = X.TRAN_ID 
             AND (R.REFUND_STATE_ID IN(1) OR (ln_terminal_capture_flag = 'Y' AND R.REFUND_STATE_ID IN(6))))
         WHERE X.TRAN_ID IN(SELECT R.TRAN_ID 
          FROM PSS.REFUND R 
         WHERE R.TERMINAL_BATCH_ID = pn_terminal_batch_id
           AND (R.REFUND_STATE_ID IN(1) OR (ln_terminal_capture_flag = 'Y' AND R.REFUND_STATE_ID IN(6))))
           AND X.TRAN_STATE_CD IN('S');
        
        -- if settlement successful or permanently declined, close old terminal batch and create new terminal batch
        IF pn_terminal_batch_id IS NOT NULL AND pc_auth_result_cd IN('Y', 'P', 'O', 'R') THEN
            UPDATE PSS.TERMINAL_BATCH
               SET TERMINAL_BATCH_CLOSE_TS = pd_batch_closed_ts
             WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id
             RETURNING TERMINAL_ID INTO ln_terminal_id;
            UPDATE PSS.TERMINAL
               SET TERMINAL_STATE_ID = 3
             WHERE TERMINAL_ID = ln_terminal_id;
        END IF;
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE PREPARE_PENDING_TRAN(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pn_sale_auth_id OUT PSS.AUTH.AUTH_ID%TYPE,
        pn_refund_id OUT PSS.REFUND.REFUND_ID%TYPE,
        pc_sale_phase_cd OUT VARCHAR2,
        pn_prior_attempts OUT PLS_INTEGER,
        pn_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE)
    IS
        lv_tran_lock_cd VARCHAR2(128);
        lv_lock_handle VARCHAR2(128);
        ln_actual_amt PSS.AUTH.AUTH_AMT%TYPE;
        ln_intended_amt PSS.AUTH.AUTH_AMT%TYPE;
        lc_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
        lc_auth_type_cd PSS.AUTH.AUTH_TYPE_CD%TYPE;
        ln_previous_saled_amt PSS.AUTH.AUTH_AMT%TYPE;
        lc_auth_hold_used PSS.AUTH.AUTH_HOLD_USED%TYPE;
        ln_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE;
        lc_terminal_capture_flag PSS.TERMINAL_BATCH.TERMINAL_CAPTURE_FLAG%TYPE;
        ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
        ln_is_open PLS_INTEGER;   
        ln_auth_auth_id PSS.AUTH.AUTH_ID%TYPE;
        ln_allowed_trans PLS_INTEGER;
        lc_new_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
        ln_device_result_type_cd PSS.TRAN.TRAN_DEVICE_RESULT_TYPE_CD%TYPE;
    BEGIN
        SELECT DEVICE_NAME || ':' || TRAN_DEVICE_TRAN_CD, 
               TRAN_STATE_CD,
               NVL(AUTH_HOLD_USED, 'N'),
               TERMINAL_ID,
               NVL(TERMINAL_CAPTURE_FLAG, 'N'),
               AUTH_AUTH_ID,
               SALE_AUTH_ID,
               REFUND_ID,
               TRAN_DEVICE_RESULT_TYPE_CD
          INTO lv_tran_lock_cd,
               lc_tran_state_cd,
               lc_auth_hold_used,
               ln_terminal_id,
               lc_terminal_capture_flag,
               ln_auth_auth_id,
               pn_sale_auth_id,
               pn_refund_id,
               ln_device_result_type_cd
          FROM (SELECT T.DEVICE_NAME, 
                       T.TRAN_DEVICE_TRAN_CD, 
                       A.AUTH_ID AUTH_AUTH_ID,
                       SA.AUTH_ID SALE_AUTH_ID,
                       R.REFUND_ID, 
                       T.TRAN_STATE_CD,
                       T.AUTH_HOLD_USED,
                       TE.TERMINAL_ID,
                       AU.TERMINAL_CAPTURE_FLAG,
                       T.TRAN_DEVICE_RESULT_TYPE_CD
                  FROM PSS.TRAN T
				  LEFT OUTER JOIN PSS.AUTH RA  ON T.TRAN_ID = RA.TRAN_ID AND RA.AUTH_TYPE_CD IN ('C', 'E')
                  LEFT OUTER JOIN PSS.AUTH A   ON T.TRAN_ID = A.TRAN_ID AND A.AUTH_TYPE_CD IN('L', 'N') AND (A.AUTH_RESULT_CD IN('Y', 'P', 'V', 'C', 'M') OR T.TRAN_STATE_CD IN('W') OR RA.AUTH_ID IS NOT NULL)
                  LEFT OUTER JOIN PSS.REFUND R ON T.TRAN_ID = R.TRAN_ID
                  LEFT OUTER JOIN PSS.AUTH SA  ON T.TRAN_ID = SA.TRAN_ID AND SA.AUTH_TYPE_CD IN('U','S','O','A','D','C','E','V','I') AND SA.AUTH_STATE_ID = 6
                  LEFT OUTER JOIN (PSS.TERMINAL TE
                        JOIN PSS.MERCHANT M ON TE.MERCHANT_ID = M.MERCHANT_ID
                        JOIN AUTHORITY.AUTHORITY AU ON M.AUTHORITY_ID = AU.AUTHORITY_ID
                  ) ON T.PAYMENT_SUBTYPE_CLASS NOT IN ('Aramark', 'Authority::NOP', 'BlackBoard', 'Cash') AND T.PAYMENT_SUBTYPE_CLASS NOT LIKE 'Internal%' AND T.PAYMENT_SUBTYPE_KEY_ID = TE.TERMINAL_ID
                 WHERE T.TRAN_ID = pn_tran_id
                 ORDER BY A.AUTH_RESULT_CD DESC, A.AUTH_TS DESC, A.AUTH_ID DESC, SA.AUTH_RESULT_CD DESC, SA.AUTH_TS DESC, SA.AUTH_ID DESC, R.CREATED_TS DESC, R.REFUND_ID DESC, RA.AUTH_RESULT_CD DESC, RA.AUTH_TS DESC, RA.AUTH_ID DESC)
         WHERE ROWNUM = 1;
        IF NOT(lc_tran_state_cd IN('A', '8', 'I', 'W')
            OR (lc_tran_state_cd IN('9') AND lc_auth_hold_used != 'Y' AND lc_terminal_capture_flag != 'Y')) THEN
            pc_sale_phase_cd := '-'; -- Do nothing
            pn_prior_attempts := -1;
            RETURN;
        END IF;
        -- Thus, "lc_tran_state_cd IN('A', '8', 'I', 'W') OR (lc_tran_state_cd IN('9') AND lc_auth_hold_used != 'Y' AND lc_terminal_capture_flag != 'Y')"
		lc_new_tran_state_cd := lc_tran_state_cd;
        -- Lock transaction
        lv_lock_handle := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', lv_tran_lock_cd);
        IF ln_auth_auth_id IS NOT NULL THEN -- it's a sale or a reversal
            IF lc_terminal_capture_flag = 'Y' AND lc_tran_state_cd != '9' THEN
                -- get the open terminal batch
                IF pn_terminal_batch_id IS NULL THEN
                	ln_terminal_batch_id := GET_AVAILABLE_TERMINAL_BATCH(ln_terminal_id, ln_allowed_trans, TRUE);
                ELSE
                    ln_terminal_batch_id := pn_terminal_batch_id;
                END IF;
                SELECT NVL(SUM(DECODE(TLI.TRAN_LINE_ITEM_BATCH_TYPE_CD, 'A', TLI.TRAN_LINE_ITEM_AMOUNT * TLI.TRAN_LINE_ITEM_QUANTITY)), 0) ACTUAL_AMT
                  INTO ln_actual_amt
                  FROM PSS.TRAN_LINE_ITEM TLI
                 WHERE TLI.TRAN_ID = pn_tran_id;
                pc_sale_phase_cd := '-'; -- No further processing
                IF ln_actual_amt != 0 THEN
                    lc_auth_type_cd := 'U'; -- pre-auth sale
                    lc_new_tran_state_cd := 'T'; -- Processed Server Batch
                ELSIF lc_auth_hold_used = 'Y' THEN
                     lc_auth_type_cd := 'C'; -- Auth Reversal
                     lc_new_tran_state_cd := 'T'; -- Processed Server Batch
                ELSE
                    -- do not create sale auth record
                    lc_new_tran_state_cd := 'C'; -- client cancelled
                END IF;
            ELSIF lc_tran_state_cd = 'W' THEN
                pc_sale_phase_cd := 'R'; -- Reversal
                ln_actual_amt := 0;
                lc_auth_type_cd := 'C'; -- Auth reversal
                lc_new_tran_state_cd := 'A'; -- processing tran
            ELSE
                SELECT NVL(SUM(DECODE(TLI.TRAN_LINE_ITEM_BATCH_TYPE_CD, 'A', TLI.TRAN_LINE_ITEM_AMOUNT * TLI.TRAN_LINE_ITEM_QUANTITY)), 0) ACTUAL_AMT,
                       NVL(SUM(DECODE(TLI.TRAN_LINE_ITEM_BATCH_TYPE_CD, 'I', TLI.TRAN_LINE_ITEM_AMOUNT * TLI.TRAN_LINE_ITEM_QUANTITY)), 0) INTENDED_AMT,
                       T.TRAN_STATE_CD -- get this again now that transaction is locked
                  INTO ln_actual_amt, ln_intended_amt, lc_tran_state_cd
                  FROM PSS.TRAN_LINE_ITEM TLI
                 RIGHT OUTER JOIN PSS.TRAN T ON T.TRAN_ID = TLI.TRAN_ID
                 WHERE T.TRAN_ID = pn_tran_id 
                 GROUP BY T.TRAN_STATE_CD;
                IF lc_tran_state_cd = '9' THEN
                    IF ln_actual_amt > 0 THEN -- sanity check
                        RAISE_APPLICATION_ERROR(-20555, 'Invalid tran_state_cd for tran ' || pn_tran_id || ': Intended batch with Actual Amount');
                    ELSIF ln_intended_amt > 0 THEN
                        pc_sale_phase_cd := 'I'; -- needs processing of intended sale
                        ln_actual_amt := NULL; -- Use Intended Amt
                        lc_auth_type_cd := 'S'; -- network sale
                        lc_new_tran_state_cd := 'A'; -- processing tran
                    ELSE -- we can assume lc_auth_hold_used != 'Y' or it would not have gotten here
                        pc_sale_phase_cd := '-'; -- no processing needed
                        -- do not create sale auth record
                        lc_new_tran_state_cd := 'U'; -- processed_server_tran_intended
                    END IF;             
                ELSE
                    SELECT NVL(MAX(AUTH_AMT), 0)
                      INTO ln_previous_saled_amt
                      FROM (SELECT /*+ index(AT IDX_AUTH_TRAN_ID) */ AT.AUTH_AMT
                              FROM PSS.AUTH AT
                             WHERE AT.TRAN_ID = pn_tran_id
                               AND AT.AUTH_TYPE_CD IN('U', 'S')
                               AND AT.AUTH_STATE_ID IN(2)
                             ORDER BY AT.AUTH_TS DESC, AT.AUTH_ID DESC)
                     WHERE ROWNUM = 1;
                    IF ln_previous_saled_amt != ln_actual_amt THEN
                        pc_sale_phase_cd := 'A'; -- needs processing of actual sale
                        IF ln_actual_amt = 0 THEN -- had previous amount, but now cancelled
                            lc_auth_type_cd := 'E'; -- Sale Reversal
                        ELSIF ln_previous_saled_amt > 0 THEN
                            lc_auth_type_cd := 'D'; -- Sale Adjustment
                        ELSIF lc_auth_hold_used = 'Y' THEN                    
                            lc_auth_type_cd := 'U'; -- Pre-Authed Sale
                        ELSE
                            lc_auth_type_cd := 'S'; -- Sale
                        END IF;
                        lc_new_tran_state_cd := 'A'; -- processing tran
                    ELSIF ln_actual_amt = 0 THEN -- previous amount = actual amount = 0
                        IF ln_intended_amt > 0 AND lc_tran_state_cd != '8' THEN
                            -- This may have been an intended sale that failed, in which case we need to retry the intended amt
                            SELECT SALE_TYPE_CD
                              INTO pc_sale_phase_cd
                              FROM PSS.SALE
                             WHERE TRAN_ID = pn_tran_id;
                            IF pc_sale_phase_cd = 'I' THEN -- needs re-processing of intended sale
                                ln_actual_amt := NULL; -- Use Intended Amt
                                lc_auth_type_cd := 'S'; -- network sale
                                lc_new_tran_state_cd := 'A'; -- processing tran
                            ELSIF lc_auth_hold_used = 'Y' THEN
                                pc_sale_phase_cd := 'A'; -- Actual
                                lc_auth_type_cd := 'C'; -- Auth reversal
                                lc_new_tran_state_cd := 'A'; -- processing tran
                            ELSE
                                pc_sale_phase_cd := '-'; -- no processing needed
                                -- do not create sale auth record
                                lc_new_tran_state_cd := 'C'; -- client cancelled
                            END IF;
                        ELSIF lc_auth_hold_used = 'Y' THEN
                            IF lc_tran_state_cd IN('I', 'A') AND ln_device_result_type_cd IS NULL THEN
                                pc_sale_phase_cd := 'R'; -- Reversal
                            ELSE
                                pc_sale_phase_cd := 'A'; -- Actual
                            END IF;
                            lc_auth_type_cd := 'C'; -- Auth reversal
                            lc_new_tran_state_cd := 'A'; -- processing tran
                        ELSE
                            pc_sale_phase_cd := '-'; -- no processing needed
                            -- do not create sale auth record
                            lc_new_tran_state_cd := 'C'; -- client cancelled
                        END IF;
                    ELSE
                        pc_sale_phase_cd := '-'; -- no processing needed
                        -- do not create sale auth record
                        lc_new_tran_state_cd := 'D'; -- tran complete
                    END IF;
                END IF;
                IF pc_sale_phase_cd != '-' THEN
                    SELECT COUNT(*)
                      INTO pn_prior_attempts
                      FROM PSS.AUTH A
                     WHERE A.TRAN_ID = pn_tran_id
                       AND A.AUTH_TYPE_CD IN ('S','I','U','V')
                       AND A.AUTH_STATE_ID IN(3, 4);  	
                END IF;
            END IF;
            IF pn_sale_auth_id IS NULL AND lc_auth_type_cd IS NOT NULL THEN
                SELECT PSS.SEQ_AUTH_ID.NEXTVAL
                  INTO pn_sale_auth_id
                  FROM DUAL;
                INSERT INTO PSS.AUTH (
                        AUTH_ID,
                        TRAN_ID,
                        AUTH_TYPE_CD,
                        AUTH_STATE_ID,
                        AUTH_PARSED_ACCT_DATA,
                        ACCT_ENTRY_METHOD_CD,
                        AUTH_AMT,
                        AUTH_TS,
                        TERMINAL_BATCH_ID,
                        TRACE_NUMBER)
                 SELECT pn_sale_auth_id,
                        pn_tran_id,
                        lc_auth_type_cd,
                        6,
                        a.AUTH_PARSED_ACCT_DATA,
                        a.ACCT_ENTRY_METHOD_CD,
                        NVL(ln_actual_amt, ln_intended_amt),	    
                        SYSDATE,
                        ln_terminal_batch_id,
                        a.TRACE_NUMBER
                   FROM PSS.AUTH a
                  WHERE a.AUTH_ID = ln_auth_auth_id;
            END IF;
        ELSIF pn_refund_id IS NOT NULL THEN -- it's a refund
            IF lc_terminal_capture_flag = 'Y' THEN
                IF pn_terminal_batch_id IS NULL THEN
                	ln_terminal_batch_id := GET_AVAILABLE_TERMINAL_BATCH(ln_terminal_id, ln_allowed_trans, TRUE);
                ELSE
                    ln_terminal_batch_id := pn_terminal_batch_id;
                END IF;
                -- stick it in the open terminal batch
                UPDATE PSS.REFUND
                   SET TERMINAL_BATCH_ID = ln_terminal_batch_id,
                       REFUND_STATE_ID = 6
                 WHERE REFUND_ID = pn_refund_id;
                pc_sale_phase_cd := '-'; -- No further processing
                lc_new_tran_state_cd := 'T'; -- Processed Server Batch
            ELSE
                -- We could check the actual amount and the previous refunded amount for sanity, but that seems overkill. 
                -- Just assume actual amount > 0 and previous refunded amount = 0
                -- Also, assume REFUND_STATE_ID IN(6, 2, 3)
                pc_sale_phase_cd := 'A'; -- needs processing of actual sale
                lc_new_tran_state_cd := 'A'; -- processing tran
                SELECT COUNT(*)
                  INTO pn_prior_attempts
                  FROM PSS.REFUND R
                 WHERE R.TRAN_ID = pn_tran_id
                   AND R.REFUND_TYPE_CD IN ('G','C','V')
                   AND R.REFUND_STATE_ID IN(2, 3);
            END IF;
        ELSE -- neither authId nor refundId is found, set to error
            pc_sale_phase_cd := '-'; -- No further processing
            lc_new_tran_state_cd := 'E'; -- Complete Error
        END IF;
		IF lc_new_tran_state_cd != lc_tran_state_cd THEN
			UPDATE PSS.TRAN 
			   SET TRAN_STATE_CD = lc_new_tran_state_cd
			 WHERE TRAN_ID = pn_tran_id;
		END IF;
    END;
    
    -- R37 and above signature
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE UPDATE_PROCESSED_TRAN(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pn_sale_auth_id PSS.AUTH.AUTH_ID%TYPE,
        pn_refund_id PSS.REFUND.REFUND_ID%TYPE,
        pc_settled_flag CHAR,
        pc_sale_phase_cd CHAR,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
        pn_auth_approved_amt PSS.AUTH.AUTH_AMT_APPROVED%TYPE,
        pv_authority_response_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
        pv_authority_response_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
        pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
        pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
        pd_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
        pc_ignore_reprocess_flag CHAR,
        pv_tran_global_trans_cd OUT PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
        pv_auth_authority_tran_cd OUT PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
		pc_settle_update_needed OUT VARCHAR2,
		pv_source_system_cd OUT VARCHAR2,
        pc_auth_type_cd OUT VARCHAR2,
        pn_override_trans_type_id OUT PSS.PAYMENT_SUBTYPE.TRANS_TYPE_ID%TYPE)
    IS
        lv_tran_lock_cd VARCHAR2(128);
        lv_lock_handle VARCHAR2(128);
        ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
        ln_settlement_batch_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE;
        lc_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
        ln_payment_subtype_key_id PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE;
        lv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE;
        ln_allowed_trans PLS_INTEGER;
        ln_auth_auth_id PSS.AUTH.AUTH_ID%TYPE;
        lc_auth_hold_used PSS.AUTH.AUTH_HOLD_USED%TYPE;
    BEGIN
		pc_settle_update_needed := 'Y';
        SELECT T.DEVICE_NAME || ':' || T.TRAN_DEVICE_TRAN_CD, T.PAYMENT_SUBTYPE_CLASS, T.PAYMENT_SUBTYPE_KEY_ID, T.TRAN_STATE_CD, T.TRAN_GLOBAL_TRANS_CD, AA.AUTH_AUTHORITY_TRAN_CD, AA.AUTH_ID, AA.AUTH_HOLD_USED, NVL(RT.SOURCE_SYSTEM_CD, 'PSS')
          INTO lv_tran_lock_cd, lv_payment_subtype_class, ln_payment_subtype_key_id, lc_tran_state_cd, pv_tran_global_trans_cd, pv_auth_authority_tran_cd, ln_auth_auth_id, lc_auth_hold_used, pv_source_system_cd
          FROM PSS.TRAN T
          LEFT OUTER JOIN PSS.AUTH AA ON T.TRAN_ID = AA.TRAN_ID AND AA.AUTH_TYPE_CD = 'N' AND AA.AUTH_STATE_ID IN(2,5)
		  LEFT OUTER JOIN REPORT.TRANS RT ON T.TRAN_GLOBAL_TRANS_CD = RT.MACHINE_TRANS_NO
         WHERE T.TRAN_ID = pn_tran_id;
        IF lc_tran_state_cd != 'A' THEN
            IF pc_ignore_reprocess_flag = 'Y' THEN
                IF pn_sale_auth_id IS NOT NULL THEN
                    SELECT AUTH_TYPE_CD, OVERRIDE_TRANS_TYPE_ID 
                      INTO pc_auth_type_cd, pn_override_trans_type_id
                      FROM PSS.AUTH 
                     WHERE AUTH_ID = pn_sale_auth_id;
                ELSIF pn_refund_id IS NOT NULL THEN
                    SELECT REFUND_TYPE_CD, OVERRIDE_TRANS_TYPE_ID 
                      INTO pc_auth_type_cd, pn_override_trans_type_id
                      FROM PSS.REFUND
                     WHERE REFUND_ID = pn_refund_id;               
                END IF;
                RETURN;
            ELSE
                RAISE_APPLICATION_ERROR(-20554, 'Invalid tran_state_cd for tran ' || pn_tran_id || ' when updating processed tran');      
            END IF;
        END IF;
        IF pc_settled_flag = 'Y' THEN
            -- create settlement batch record
            SELECT PSS.SEQ_SETTLEMENT_BATCH_ID.NEXTVAL
              INTO ln_settlement_batch_id
              FROM DUAL;
            INSERT INTO PSS.SETTLEMENT_BATCH(
                SETTLEMENT_BATCH_ID,
                SETTLEMENT_BATCH_STATE_ID,
                SETTLEMENT_BATCH_START_TS,
                SETTLEMENT_BATCH_END_TS,
                SETTLEMENT_BATCH_RESP_CD,
                SETTLEMENT_BATCH_RESP_DESC,
                SETTLEMENT_BATCH_REF_CD
            ) VALUES (
                ln_settlement_batch_id,
                DECODE(pc_auth_result_cd, 'Y', 1, 'P', 1, 'N', 2, 'O', 2, 'F', 3, 'R', 2),
                pd_authority_ts,
                pd_authority_ts,
                pv_authority_response_cd,
                pv_authority_response_desc,
                pv_authority_ref_cd);
        ELSIF pc_auth_result_cd IN('Y', 'P') AND (lv_payment_subtype_class  LIKE 'Authority::ISO8583%') THEN
            -- If this is a retry of a failure, it may already have a terminal_batch assigned
            IF pn_sale_auth_id IS NOT NULL THEN
                SELECT MAX(TERMINAL_BATCH_ID)
                  INTO ln_terminal_batch_id
                  FROM PSS.AUTH
                 WHERE TRAN_ID = pn_tran_id;
            ELSIF pn_refund_id IS NOT NULL THEN
                SELECT MAX(TERMINAL_BATCH_ID)
                  INTO ln_terminal_batch_id
                  FROM PSS.REFUND
                 WHERE TRAN_ID = pn_tran_id;
            END IF;
            IF ln_terminal_batch_id IS NULL THEN
                ln_terminal_batch_id := GET_AVAILABLE_TERMINAL_BATCH(ln_payment_subtype_key_id, ln_allowed_trans, TRUE);
                IF ln_allowed_trans <= 0 THEN
                    RAISE_APPLICATION_ERROR(-20556, 'Terminal Batch ' || ln_terminal_batch_id || ' is full for terminal ' || ln_payment_subtype_key_id || '; Can not add tran ' || pn_tran_id || ' to it');
                END IF;
            END IF;
        END IF;
        -- Lock transaction
        lv_lock_handle := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', lv_tran_lock_cd);
        IF pn_sale_auth_id IS NOT NULL THEN
            UPDATE PSS.AUTH 
               SET (
			    AUTH_STATE_ID,
                AUTH_RESULT_CD,
                AUTH_RESP_CD,
                AUTH_RESP_DESC,
                AUTH_AUTHORITY_TRAN_CD,
                AUTH_AUTHORITY_REF_CD,
                AUTH_AUTHORITY_TS,
                TERMINAL_BATCH_ID,
                AUTH_AMT_APPROVED,
                AUTH_AUTHORITY_MISC_DATA) =
            (SELECT 
                DECODE(pc_auth_result_cd, 'Y', 2, 'P', 5, 'N', 3, 'O', 7, 'F', 4, 'R', 7),
                pc_auth_result_cd,
                pv_authority_response_cd,
                pv_authority_response_desc,
                pv_authority_tran_cd,
                pv_authority_ref_cd,
                pd_authority_ts,
                ln_terminal_batch_id,
                pn_auth_approved_amt / pn_minor_currency_factor,
                pv_authority_misc_data
             FROM DUAL)
             WHERE AUTH_ID = pn_sale_auth_id
             RETURNING AUTH_TYPE_CD, OVERRIDE_TRANS_TYPE_ID INTO pc_auth_type_cd, pn_override_trans_type_id;
            IF lc_auth_hold_used = 'Y' AND pc_auth_result_cd IN('O', 'R') THEN
                DELETE 
                  FROM PSS.CONSUMER_ACCT_AUTH_HOLD
                 WHERE AUTH_ID = ln_auth_auth_id;
            END IF;
			IF ln_settlement_batch_id IS NOT NULL THEN
                INSERT INTO PSS.TRAN_SETTLEMENT_BATCH(
                    SETTLEMENT_BATCH_ID,
                    AUTH_ID,
                    TRAN_ID,
                    TRAN_SETTLEMENT_B_AMT,
                    TRAN_SETTLEMENT_B_RESP_CD,
                    TRAN_SETTLEMENT_B_RESP_DESC
                ) VALUES (
                    ln_settlement_batch_id,
                    pn_sale_auth_id,
                    pn_tran_id,
                    pn_auth_approved_amt / pn_minor_currency_factor,
                    pv_authority_response_cd,
                    pv_authority_response_desc
                );               
            END IF;
        ELSIF pn_refund_id IS NOT NULL THEN
           UPDATE PSS.REFUND
               SET REFUND_STATE_ID = DECODE(pc_auth_result_cd, 'Y', 1, 'P', 1, 'N', 2, 'O', 2, 'F', 3, 'R', 2),
                REFUND_RESP_CD = NVL(pv_authority_response_cd, REFUND_RESP_CD),
                REFUND_RESP_DESC = NVL(pv_authority_response_desc, REFUND_RESP_DESC),
                REFUND_AUTHORITY_TRAN_CD = NVL(pv_authority_tran_cd, REFUND_AUTHORITY_TRAN_CD),
                REFUND_AUTHORITY_REF_CD = NVL(pv_authority_ref_cd, REFUND_AUTHORITY_REF_CD),
                REFUND_AUTHORITY_TS = NVL(pd_authority_ts, REFUND_AUTHORITY_TS),
                TERMINAL_BATCH_ID = NVL(ln_terminal_batch_id, TERMINAL_BATCH_ID),
                REFUND_AUTHORITY_MISC_DATA = NVL(pv_authority_misc_data, REFUND_AUTHORITY_MISC_DATA)
            WHERE REFUND_ID = pn_refund_id
            RETURNING REFUND_TYPE_CD, OVERRIDE_TRANS_TYPE_ID INTO pc_auth_type_cd, pn_override_trans_type_id;
            IF ln_settlement_batch_id IS NOT NULL THEN
                INSERT INTO PSS.REFUND_SETTLEMENT_BATCH(
                    SETTLEMENT_BATCH_ID,
                    REFUND_ID,
                    TRAN_ID,
                    REFUND_SETTLEMENT_B_AMT
                ) VALUES (
                    ln_settlement_batch_id,
                    pn_refund_id,
                    pn_tran_id,
                    pn_auth_approved_amt / pn_minor_currency_factor
                );               
            END IF;
        END IF;
        
        -- update tran_state_cd
        IF pc_auth_result_cd IN('F') THEN
            lc_tran_state_cd := 'J'; -- Incomplete Error: Manual intervention needed
        ELSIF pc_auth_result_cd IN('O', 'R') THEN
            IF pc_sale_phase_cd = 'R' THEN -- it was an auth reversal
                lc_tran_state_cd := 'V';
            ELSIF pc_auth_type_cd IN('C', 'E') THEN -- sale reversal or auth reversal (indicates actual amount = 0)
                lc_tran_state_cd := 'C'; -- Client Cancelled
            ELSIF pc_sale_phase_cd = 'I' THEN
                -- check If actuals have since been uploaded
                SELECT DECODE(SALE_TYPE_CD, 'I', 'K', 'A', '8')
                  INTO lc_tran_state_cd
                  FROM PSS.SALE
                WHERE TRAN_ID = pn_tran_id;         
            ELSE
                lc_tran_state_cd := 'E'; -- Complete Error: don't retry
            END IF;
        ELSIF pc_auth_result_cd IN('N') THEN
            lc_tran_state_cd := 'I'; -- Incomplete: will retry
        ELSIF pc_auth_result_cd IN('Y', 'P') THEN
            IF pc_sale_phase_cd = 'R' THEN -- it was an auth reversal
                IF pc_settled_flag = 'Y' THEN
                    lc_tran_state_cd := 'V';
                ELSE
                     lc_tran_state_cd := 'T';
                END IF;
            ELSIF pc_sale_phase_cd = 'I' THEN
                -- check If actuals have since been uploaded
                SELECT DECODE(SALE_TYPE_CD, 'I', 'U', 'A', '8')
                  INTO lc_tran_state_cd
                  FROM PSS.SALE
                WHERE TRAN_ID = pn_tran_id;
            ELSIF pc_settled_flag = 'Y' THEN
                IF pc_auth_type_cd IN('C', 'E') THEN -- sale reversal or auth reversal (indicates actual amount = 0)
                    lc_tran_state_cd := 'C'; -- Client Cancelled
                ELSE
                    lc_tran_state_cd := 'D'; -- Complete
                END IF;
            ELSE
                lc_tran_state_cd := 'T'; -- Processed Server Batch
            END IF;
        ELSE
            RAISE_APPLICATION_ERROR(-20557, 'Invalid auth result cd "' ||  pc_auth_result_cd + '" for tran ' || pn_tran_id);
        END IF;
        UPDATE PSS.TRAN
           SET TRAN_STATE_CD = lc_tran_state_cd
         WHERE TRAN_ID = pn_tran_id;
    END;      

    -- R36 and below
    PROCEDURE UPDATE_PROCESSED_TRAN(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pn_sale_auth_id PSS.AUTH.AUTH_ID%TYPE,
        pn_refund_id PSS.REFUND.REFUND_ID%TYPE,
        pc_settled_flag CHAR,
        pc_sale_phase_cd CHAR,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
        pn_auth_approved_amt PSS.AUTH.AUTH_AMT_APPROVED%TYPE,
        pv_authority_response_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
        pv_authority_response_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
        pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
        pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
        pd_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
        pc_ignore_reprocess_flag CHAR,
        pv_tran_global_trans_cd OUT PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
        pv_auth_authority_tran_cd OUT PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
		pc_settle_update_needed OUT VARCHAR2,
		pv_source_system_cd OUT VARCHAR2,
        pc_auth_type_cd OUT VARCHAR2)
    IS
        ln_override_trans_type_id PSS.PAYMENT_SUBTYPE.TRANS_TYPE_ID%TYPE;
    BEGIN
        UPDATE_PROCESSED_TRAN(
            pn_tran_id,
            pn_sale_auth_id,
            pn_refund_id,
            pc_settled_flag,
            pc_sale_phase_cd,
            pc_auth_result_cd,
            pn_minor_currency_factor,
            pn_auth_approved_amt,
            pv_authority_response_cd,
            pv_authority_response_desc,
            pv_authority_tran_cd,
            pv_authority_ref_cd,
            pv_authority_misc_data,
            pd_authority_ts,
            pc_ignore_reprocess_flag,
            pv_tran_global_trans_cd,
            pv_auth_authority_tran_cd,
            pc_settle_update_needed,
            pv_source_system_cd,
            pc_auth_type_cd,
            ln_override_trans_type_id);
    END;
    
    FUNCTION GET_TERMINAL_GLOBAL_TOKEN(
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_payment_subtype_key_id NUMBER)
    RETURN VARCHAR2
    IS
        lv_global_token PSS.TERMINAL.GLOBAL_TOKEN_CD%TYPE;
    BEGIN
        IF pv_payment_subtype_class IN('Authority::NOP', 'Cash') THEN
            RAISE_APPLICATION_ERROR(-20865, 'Payment subtype class ''' || pv_payment_subtype_class || ''' does not have terminals');
        ELSIF pv_payment_subtype_class = 'Aramark' THEN
            SELECT GLOBAL_TOKEN_CD
              INTO lv_global_token
              FROM PSS.ARAMARK_PAYMENT_TYPE
             WHERE ARAMARK_PAYMENT_TYPE_ID = pn_payment_subtype_key_id;
        ELSIF pv_payment_subtype_class = 'BlackBoard' THEN
            SELECT GLOBAL_TOKEN_CD
              INTO lv_global_token
              FROM PSS.BLACKBRD_TENDER
             WHERE BLACKBRD_TENDER_ID = pn_payment_subtype_key_id;
        ELSIF pv_payment_subtype_class LIKE 'Internal%' THEN
            SELECT GLOBAL_TOKEN_CD
              INTO lv_global_token
              FROM PSS.INTERNAL_PAYMENT_TYPE
             WHERE INTERNAL_PAYMENT_TYPE_ID = pn_payment_subtype_key_id;
        ELSE
            SELECT GLOBAL_TOKEN_CD
              INTO lv_global_token
              FROM PSS.TERMINAL
             WHERE TERMINAL_ID = pn_payment_subtype_key_id;
        END IF;
        RETURN lv_global_token;
    END;
    
    PROCEDURE UPDATE_TERMINAL_GLOBAL_TOKEN(
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_payment_subtype_key_id NUMBER,
        pv_old_global_token PSS.TERMINAL.GLOBAL_TOKEN_CD%TYPE,
        pv_new_global_token PSS.TERMINAL.GLOBAL_TOKEN_CD%TYPE,
        pn_update_count OUT PLS_INTEGER,
        pc_current_must_match CHAR DEFAULT 'N')
    IS
		ln_term_lock_max_duration_sec NUMBER := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('POSM_TERMINAL_LOCK_MAX_DURATION_SEC'));
		ld_sysdate DATE := SYSDATE;
		ln_forced_unlock_count NUMBER := 0;
    BEGIN
        IF pv_new_global_token IS NULL THEN
            RAISE_APPLICATION_ERROR(-20864, 'Global Token Code may not be null');
        ELSIF pv_payment_subtype_class IN('Authority::NOP', 'Cash') THEN
            RAISE_APPLICATION_ERROR(-20865, 'Payment subtype class ''' || pv_payment_subtype_class || ''' does not have terminals');
        ELSIF pv_payment_subtype_class = 'Aramark' THEN
            UPDATE PSS.ARAMARK_PAYMENT_TYPE
               SET GLOBAL_TOKEN_CD = pv_new_global_token
             WHERE ARAMARK_PAYMENT_TYPE_ID = pn_payment_subtype_key_id
               AND ((pc_current_must_match = 'N' AND GLOBAL_TOKEN_CD IS NULL) OR GLOBAL_TOKEN_CD = pv_old_global_token);
            pn_update_count := SQL%ROWCOUNT;
			IF pn_update_count = 0 THEN
				UPDATE PSS.ARAMARK_PAYMENT_TYPE
				   SET GLOBAL_TOKEN_CD = pv_new_global_token
				 WHERE ARAMARK_PAYMENT_TYPE_ID = pn_payment_subtype_key_id
					AND GLOBAL_TOKEN_CD IS NOT NULL
					AND LAST_UPDATED_TS < ld_sysdate - ln_term_lock_max_duration_sec/86400;
				pn_update_count := SQL%ROWCOUNT;
				ln_forced_unlock_count := pn_update_count;
			END IF;
        ELSIF pv_payment_subtype_class = 'BlackBoard' THEN
            UPDATE PSS.BLACKBRD_TENDER
               SET GLOBAL_TOKEN_CD = pv_new_global_token
             WHERE BLACKBRD_TENDER_ID = pn_payment_subtype_key_id
               AND ((pc_current_must_match = 'N' AND GLOBAL_TOKEN_CD IS NULL) OR GLOBAL_TOKEN_CD = pv_old_global_token);
            pn_update_count := SQL%ROWCOUNT;
			IF pn_update_count = 0 THEN
				UPDATE PSS.BLACKBRD_TENDER
				   SET GLOBAL_TOKEN_CD = pv_new_global_token
				 WHERE BLACKBRD_TENDER_ID = pn_payment_subtype_key_id
					AND GLOBAL_TOKEN_CD IS NOT NULL
					AND LAST_UPDATED_TS < ld_sysdate - ln_term_lock_max_duration_sec/86400;
				pn_update_count := SQL%ROWCOUNT;
				ln_forced_unlock_count := pn_update_count;
			END IF;
        ELSIF pv_payment_subtype_class LIKE 'Internal%' THEN
            UPDATE PSS.INTERNAL_PAYMENT_TYPE
               SET GLOBAL_TOKEN_CD = pv_new_global_token
             WHERE INTERNAL_PAYMENT_TYPE_ID = pn_payment_subtype_key_id
               AND ((pc_current_must_match = 'N' AND GLOBAL_TOKEN_CD IS NULL) OR GLOBAL_TOKEN_CD = pv_old_global_token);
            pn_update_count := SQL%ROWCOUNT;
			IF pn_update_count = 0 THEN
				UPDATE PSS.INTERNAL_PAYMENT_TYPE
				   SET GLOBAL_TOKEN_CD = pv_new_global_token
				 WHERE INTERNAL_PAYMENT_TYPE_ID = pn_payment_subtype_key_id
					AND GLOBAL_TOKEN_CD IS NOT NULL
					AND LAST_UPDATED_TS < ld_sysdate - ln_term_lock_max_duration_sec/86400;
				pn_update_count := SQL%ROWCOUNT;
				ln_forced_unlock_count := pn_update_count;
			END IF;
        ELSE
            UPDATE PSS.TERMINAL
               SET GLOBAL_TOKEN_CD = pv_new_global_token,
                   TERMINAL_STATE_ID =  DECODE(TERMINAL_STATE_ID, 5, 5, 3) -- terminal is busy or settlement retry now
             WHERE TERMINAL_ID = pn_payment_subtype_key_id
               AND ((pc_current_must_match = 'N' AND GLOBAL_TOKEN_CD IS NULL AND TERMINAL_STATE_ID IN(1, 5))
                OR (GLOBAL_TOKEN_CD = pv_old_global_token AND TERMINAL_STATE_ID IN(3, 5)));
            pn_update_count := SQL%ROWCOUNT;
			IF pn_update_count = 0 THEN
				UPDATE PSS.TERMINAL
				   SET GLOBAL_TOKEN_CD = pv_new_global_token,
					   TERMINAL_STATE_ID =  DECODE(TERMINAL_STATE_ID, 5, 5, 3) -- terminal is busy or settlement retry now
				 WHERE TERMINAL_ID = pn_payment_subtype_key_id
					AND GLOBAL_TOKEN_CD IS NOT NULL
					AND LAST_UPDATED_TS < ld_sysdate - ln_term_lock_max_duration_sec/86400;
				pn_update_count := SQL%ROWCOUNT;
				ln_forced_unlock_count := pn_update_count;
			END IF;
        END IF;
		
		IF ln_forced_unlock_count > 0 THEN
			pkg_exception_processor.sp_log_exception (
				pkg_app_exec_hist_globals.unknown_error_id,
				'POSM terminal ' || pn_payment_subtype_key_id || '(' || pv_payment_subtype_class || ') was locked for more than ' || ln_term_lock_max_duration_sec || ' seconds, forcefully unlocked',
				NULL,
				'PSS.PKG_SETTLEMENT.UPDATE_TERMINAL_GLOBAL_TOKEN'
			);
		END IF;
    END;
    
    PROCEDURE CLEAR_TERMINAL_GLOBAL_TOKEN(
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_payment_subtype_key_id NUMBER,
        pv_global_token PSS.TERMINAL.GLOBAL_TOKEN_CD%TYPE,
        pn_update_count OUT PLS_INTEGER)
    IS
    BEGIN
        IF pv_payment_subtype_class IN('Authority::NOP', 'Cash') THEN
            RAISE_APPLICATION_ERROR(-20865, 'Payment subtype class ''' || pv_payment_subtype_class || ''' does not have terminals');
        ELSIF pv_payment_subtype_class = 'Aramark' THEN
            UPDATE PSS.ARAMARK_PAYMENT_TYPE
               SET GLOBAL_TOKEN_CD = NULL,
                   LAST_RELEASED_UTC_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP)
             WHERE ARAMARK_PAYMENT_TYPE_ID = pn_payment_subtype_key_id
               AND GLOBAL_TOKEN_CD = pv_global_token;
            pn_update_count := SQL%ROWCOUNT;
        ELSIF pv_payment_subtype_class = 'BlackBoard' THEN
            UPDATE PSS.BLACKBRD_TENDER
               SET GLOBAL_TOKEN_CD = NULL,
                   LAST_RELEASED_UTC_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP)
             WHERE BLACKBRD_TENDER_ID = pn_payment_subtype_key_id
               AND GLOBAL_TOKEN_CD = pv_global_token;
            pn_update_count := SQL%ROWCOUNT;
        ELSIF pv_payment_subtype_class LIKE 'Internal%' THEN
            UPDATE PSS.INTERNAL_PAYMENT_TYPE
               SET GLOBAL_TOKEN_CD = NULL,
                   LAST_RELEASED_UTC_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP)
             WHERE INTERNAL_PAYMENT_TYPE_ID = pn_payment_subtype_key_id
               AND GLOBAL_TOKEN_CD = pv_global_token;
            pn_update_count := SQL%ROWCOUNT;
        ELSE
            UPDATE PSS.TERMINAL
               SET GLOBAL_TOKEN_CD = NULL,
                   TERMINAL_STATE_ID = DECODE(TERMINAL_STATE_ID, 5, 5, 1), -- terminal is active or settlement retry now
                   LAST_RELEASED_UTC_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP)
             WHERE TERMINAL_ID = pn_payment_subtype_key_id
               AND GLOBAL_TOKEN_CD = pv_global_token;
            pn_update_count := SQL%ROWCOUNT;
        END IF;
    END;

    FUNCTION ADD_ADMIN_CMD(
        pv_payment_subtype_class PSS.ADMIN_CMD.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_payment_subtype_key_id PSS.ADMIN_CMD.PAYMENT_SUBTYPE_KEY_ID%TYPE,
        pn_admin_cmd_type_id PSS.ADMIN_CMD.ADMIN_CMD_TYPE_ID%TYPE,
        pv_requested_by PSS.ADMIN_CMD.REQUESTED_BY%TYPE,
        pn_priority PSS.ADMIN_CMD.PRIORITY%TYPE)
    RETURN PSS.ADMIN_CMD.ADMIN_CMD_ID%TYPE
    IS
        ln_admin_cmd_id PSS.ADMIN_CMD.ADMIN_CMD_ID%TYPE;
    BEGIN
        SELECT PSS.SEQ_ADMIN_CMD_ID.NEXTVAL
          INTO ln_admin_cmd_id
          FROM DUAL;
        INSERT INTO PSS.ADMIN_CMD (
                ADMIN_CMD_ID,
                ADMIN_CMD_TYPE_ID,
                PAYMENT_SUBTYPE_KEY_ID,
                PAYMENT_SUBTYPE_CLASS,
                REQUESTED_BY,
                PRIORITY) 
         VALUES(ln_admin_cmd_id, 
                pn_admin_cmd_type_id, 
                pn_payment_subtype_key_id,
                pv_payment_subtype_class,
                pv_requested_by,
                pn_priority);
         RETURN ln_admin_cmd_id;
    END;
    
    FUNCTION ADD_ADMIN_CMD_PARAM(
        pn_admin_cmd_id PSS.ADMIN_CMD_PARAM.ADMIN_CMD_ID%TYPE,
        pv_param_name PSS.ADMIN_CMD_PARAM.PARAM_NAME%TYPE,
        pv_param_value PSS.ADMIN_CMD_PARAM.PARAM_VALUE%TYPE)
    RETURN PSS.ADMIN_CMD_PARAM.ADMIN_CMD_PARAM_ID%TYPE
    IS
        ln_admin_cmd_param_id PSS.ADMIN_CMD_PARAM.ADMIN_CMD_PARAM_ID%TYPE;
    BEGIN
        SELECT PSS.SEQ_ADMIN_CMD_PARAM_ID.NEXTVAL
          INTO ln_admin_cmd_param_id
          FROM DUAL;
        INSERT INTO PSS.ADMIN_CMD_PARAM (
                ADMIN_CMD_PARAM_ID,
                ADMIN_CMD_ID,
                PARAM_NAME,
                PARAM_VALUE) 
         VALUES(ln_admin_cmd_param_id, 
                pn_admin_cmd_id,
                pv_param_name, 
                pv_param_value);
         RETURN ln_admin_cmd_param_id;
    END;   
    
    PROCEDURE LOCK_PARTIAL_SETTLE_POLLING(
        pv_process_id PSS.POSM_SETTING.POSM_SETTING_VALUE%TYPE,
        pv_success_flag OUT VARCHAR2,
        pv_locked_by_process_id OUT PSS.POSM_SETTING.POSM_SETTING_VALUE%TYPE)
    IS
		ln_polling_max_duration_sec NUMBER := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('POSM_FEEDBACK_POLLING_MAX_DURATION_SEC'));
		lt_last_updated_utc_ts PSS.POSM_SETTING.LAST_UPDATED_UTC_TS%TYPE;
    BEGIN
        SELECT POSM_SETTING_VALUE, 'N', LAST_UPDATED_UTC_TS
          INTO pv_locked_by_process_id, pv_success_flag, lt_last_updated_utc_ts
          FROM PSS.POSM_SETTING
         WHERE POSM_SETTING_NAME = 'PARTIAL_SETTLEMENT_POLLING_LOCK';
		 
		IF lt_last_updated_utc_ts < SYS_EXTRACT_UTC(SYSTIMESTAMP) - ln_polling_max_duration_sec/86400 THEN
			pkg_exception_processor.sp_log_exception (
				pkg_app_exec_hist_globals.unknown_error_id,
				'POSM feedback polling has been locked by ' || pv_locked_by_process_id || ' since ' || lt_last_updated_utc_ts || ' UTC, unlocking',
				NULL,
				'PSS.PKG_SETTLEMENT.LOCK_PARTIAL_SETTLE_POLLING'
			);
			UNLOCK_PARTIAL_SETTLE_POLLING(pv_locked_by_process_id);
		END IF;
		
        SELECT POSM_SETTING_VALUE, 'N'
        INTO pv_locked_by_process_id, pv_success_flag
        FROM PSS.POSM_SETTING
        WHERE POSM_SETTING_NAME = 'PARTIAL_SETTLEMENT_POLLING_LOCK';
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            BEGIN
                INSERT INTO PSS.POSM_SETTING(POSM_SETTING_NAME, POSM_SETTING_VALUE)
                  VALUES('PARTIAL_SETTLEMENT_POLLING_LOCK', pv_process_id);
                pv_success_flag := 'Y';
            EXCEPTION
                WHEN DUP_VAL_ON_INDEX THEN
                    LOCK_PARTIAL_SETTLE_POLLING(pv_process_id, pv_success_flag, pv_locked_by_process_id);
            END;
    END;
    
    PROCEDURE UNLOCK_PARTIAL_SETTLE_POLLING(
        pv_process_id PSS.POSM_SETTING.POSM_SETTING_VALUE%TYPE)
    IS
    BEGIN
        DELETE 
          FROM PSS.POSM_SETTING
         WHERE POSM_SETTING_NAME = 'PARTIAL_SETTLEMENT_POLLING_LOCK'
           AND POSM_SETTING_VALUE = pv_process_id;
        IF SQL%ROWCOUNT != 1 THEN
            RAISE NO_DATA_FOUND;
        END IF;
    END;
    
    PROCEDURE UPDATE_BATCH_FEEDBACK_REF_CD(
        pv_settlement_batch_ref_cd PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_REF_CD%TYPE,
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pd_auth_authority_ts PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_END_TS%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pv_authority_resp_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pn_settlement_batch_id OUT PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_terminal_batch_id OUT PSS.SETTLEMENT_BATCH.TERMINAL_BATCH_ID%TYPE)
    IS
        ln_settlement_batch_state_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_STATE_ID%TYPE;
    BEGIN
        SELECT SETTLEMENT_BATCH_ID, TERMINAL_BATCH_ID, SETTLEMENT_BATCH_STATE_ID
          INTO pn_settlement_batch_id, pn_terminal_batch_id, ln_settlement_batch_state_id
          FROM (SELECT SB.SETTLEMENT_BATCH_ID, SB.TERMINAL_BATCH_ID, SB.SETTLEMENT_BATCH_STATE_ID
          FROM PSS.SETTLEMENT_BATCH SB
          JOIN PSS.TERMINAL_BATCH TB ON SB.TERMINAL_BATCH_ID = TB.TERMINAL_BATCH_ID
          JOIN PSS.TERMINAL T ON TB.TERMINAL_ID = T.TERMINAL_ID
          JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
          JOIN AUTHORITY.AUTHORITY A ON M.AUTHORITY_ID = A.AUTHORITY_ID
          JOIN AUTHORITY.AUTHORITY_TYPE AUT ON A.AUTHORITY_TYPE_ID = AUT.AUTHORITY_TYPE_ID
          JOIN AUTHORITY.HANDLER H ON H.HANDLER_ID = AUT.HANDLER_ID
         WHERE SB.SETTLEMENT_BATCH_REF_CD = pv_settlement_batch_ref_cd
           AND H.HANDLER_CLASS = pv_payment_subtype_class
           AND TB.TERMINAL_BATCH_CLOSE_TS IS NOT NULL
         ORDER BY DECODE(SB.SETTLEMENT_BATCH_STATE_ID, 7, 1, 5), TB.TERMINAL_BATCH_CYCLE_NUM DESC, SB.SETTLEMENT_BATCH_ID DESC)
         WHERE ROWNUM = 1; 
        
        IF ln_settlement_batch_state_id = 1 OR (ln_settlement_batch_state_id IN(1,2,3,7) AND pc_auth_result_cd IN('P')) THEN
            RETURN;
        END IF;
           
        UPDATE PSS.SETTLEMENT_BATCH SB
           SET SB.SETTLEMENT_BATCH_STATE_ID = DECODE(pc_auth_result_cd, 'Y', 1, 'P', 7, 'N', 2, 'F', 3, 'O', 2, 'R', 2),
               SB.SETTLEMENT_BATCH_END_TS = NVL(pd_auth_authority_ts, SYSDATE),
               SB.SETTLEMENT_BATCH_RESP_DESC = DECODE(TRIM(pv_authority_resp_desc), NULL, SETTLEMENT_BATCH_RESP_DESC, SUBSTR(SETTLEMENT_BATCH_RESP_DESC || ' => ' || TRIM(pv_authority_resp_desc) , 1, 2000))
         WHERE SB.SETTLEMENT_BATCH_ID = pn_settlement_batch_id;                 
    END;
    
    PROCEDURE UPDATE_BATCH_FEEDBACK(
        pv_merchant_cd PSS.MERCHANT.MERCHANT_CD%TYPE,
        pv_terminal_cd PSS.TERMINAL.TERMINAL_CD%TYPE,
        pv_terminal_batch_num PSS.TERMINAL_BATCH.TERMINAL_BATCH_NUM%TYPE,
        pd_auth_authority_ts PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_END_TS%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pv_authority_resp_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pn_settlement_batch_id OUT PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_terminal_batch_id OUT PSS.SETTLEMENT_BATCH.TERMINAL_BATCH_ID%TYPE)
    IS
        ln_settlement_batch_state_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_STATE_ID%TYPE;
    BEGIN
        SELECT SETTLEMENT_BATCH_ID, TERMINAL_BATCH_ID, SETTLEMENT_BATCH_STATE_ID
          INTO pn_settlement_batch_id, pn_terminal_batch_id, ln_settlement_batch_state_id
          FROM (SELECT SB.SETTLEMENT_BATCH_ID, SB.TERMINAL_BATCH_ID, SB.SETTLEMENT_BATCH_STATE_ID
          FROM PSS.SETTLEMENT_BATCH SB
          JOIN PSS.TERMINAL_BATCH TB ON SB.TERMINAL_BATCH_ID = TB.TERMINAL_BATCH_ID
          JOIN PSS.TERMINAL T ON TB.TERMINAL_ID = T.TERMINAL_ID
          JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
         WHERE M.MERCHANT_CD = pv_merchant_cd
           AND T.TERMINAL_CD = pv_terminal_cd
           AND TB.TERMINAL_BATCH_NUM = pv_terminal_batch_num
           AND TB.TERMINAL_BATCH_CLOSE_TS IS NOT NULL
         ORDER BY DECODE(SB.SETTLEMENT_BATCH_STATE_ID, 7, 1, 5), TB.TERMINAL_BATCH_CYCLE_NUM DESC, SB.SETTLEMENT_BATCH_ID DESC)
         WHERE ROWNUM = 1; 
           
        IF ln_settlement_batch_state_id = 1 OR (ln_settlement_batch_state_id IN(1,2,3,7) AND pc_auth_result_cd IN('P')) THEN
            RETURN;
        END IF;
        
        UPDATE PSS.SETTLEMENT_BATCH SB
           SET SB.SETTLEMENT_BATCH_STATE_ID = DECODE(pc_auth_result_cd, 'Y', 1, 'P', 7, 'N', 2, 'F', 3, 'O', 2, 'R', 2),
               SB.SETTLEMENT_BATCH_END_TS = NVL(pd_auth_authority_ts, SYSDATE),
               SB.SETTLEMENT_BATCH_RESP_DESC = DECODE(TRIM(pv_authority_resp_desc), NULL, SETTLEMENT_BATCH_RESP_DESC, SUBSTR(SETTLEMENT_BATCH_RESP_DESC || ' => ' || TRIM(pv_authority_resp_desc) , 1, 2000))
         WHERE SB.SETTLEMENT_BATCH_ID = pn_settlement_batch_id;                 
    END;
   
    -- R37 and above signature 
    PROCEDURE UPDATE_TRAN_FEEDBACK(
        pn_terminal_batch_id PSS.SETTLEMENT_BATCH.TERMINAL_BATCH_ID%TYPE,
        pn_settlement_batch_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_sale_auth_id PSS.AUTH.AUTH_ID%TYPE,
        pn_refund_id PSS.REFUND.REFUND_ID%TYPE,
        pc_tran_type_cd CHAR,
        pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
        pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pv_authority_response_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
        pv_authority_response_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
        pd_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
        pn_approved_amt PSS.AUTH.AUTH_AMT_APPROVED%TYPE,
        pn_sale_amt PSS.AUTH.AUTH_AMT%TYPE,
        pc_major_currency_used CHAR,
        pv_tran_global_trans_cd OUT PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
		pc_settle_update_needed OUT VARCHAR2,
		pc_auth_type_cd OUT VARCHAR2,
        pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
        pn_override_trans_type_id OUT PSS.PAYMENT_SUBTYPE.TRANS_TYPE_ID%TYPE)
    IS
        ln_original_auth_id PSS.AUTH.AUTH_ID%TYPE;
        ln_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE;
        lc_entry_method_cd PSS.AUTH.ACCT_ENTRY_METHOD_CD%TYPE;
        lv_trace_number PSS.AUTH.TRACE_NUMBER%TYPE;
        ln_auth_id PSS.AUTH.AUTH_ID%TYPE;
        ln_refund_id PSS.REFUND.REFUND_ID%TYPE;
        lc_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
    BEGIN
		pc_settle_update_needed := 'Y';
        IF pn_sale_auth_id IS NOT NULL THEN
            ln_auth_id := pn_sale_auth_id;
        ELSIF pn_refund_id IS NOT NULL THEN
            ln_refund_id := pn_refund_id;
        ELSIF pc_tran_type_cd = '+' THEN
            SELECT AUTH_ID
              INTO ln_auth_id
              FROM (SELECT SA.AUTH_ID
              FROM PSS.AUTH A
              JOIN PSS.AUTH SA ON A.TRAN_ID = SA.TRAN_ID
             WHERE SA.TERMINAL_BATCH_ID = pn_terminal_batch_id
               AND A.AUTH_AUTHORITY_TRAN_CD = pv_authority_tran_cd 
               AND A.AUTH_AUTHORITY_REF_CD = pv_authority_ref_cd
               AND SA.AUTH_TYPE_CD IN('U','S','O','A','D','C','E','V','I')
               AND A.AUTH_TYPE_CD IN('L','N')
             ORDER BY SA.AUTH_TS DESC, SA.AUTH_ID DESC)
             WHERE ROWNUM = 1;
        ELSIF pc_tran_type_cd = '-' THEN
            SELECT REFUND_ID
              INTO ln_refund_id
              FROM (SELECT R.REFUND_ID, R.TRAN_ID
                      FROM PSS.REFUND R
                     WHERE R.TERMINAL_BATCH_ID = pn_terminal_batch_id
                       AND R.REFUND_AUTHORITY_TRAN_CD = pv_authority_tran_cd 
                       AND R.REFUND_AUTHORITY_REF_CD = pv_authority_ref_cd
                       AND R.REFUND_STATE_ID IN(6, 1)
                     ORDER BY R.CREATED_TS DESC, R.REFUND_ID DESC)
             WHERE ROWNUM = 1;
        ELSE
            RAISE_APPLICATION_ERROR(-20558, 'Invalid tran type cd "' ||  pc_tran_type_cd + '"');
        END IF;
        
        IF ln_auth_id IS NOT NULL THEN
            IF pc_major_currency_used = 'Y' THEN
                ln_minor_currency_factor := 1;
            ELSE
                SELECT C.MINOR_CURRENCY_FACTOR
                  INTO ln_minor_currency_factor
                  FROM PSS.CURRENCY C
                  JOIN PSS.POS_PTA PTA ON C.CURRENCY_CD = NVL(pta.CURRENCY_CD, 'USD')
                  JOIN PSS.TRAN X ON PTA.POS_PTA_ID = X.POS_PTA_ID
                  JOIN PSS.AUTH A ON A.TRAN_ID = X.TRAN_ID
                 WHERE A.AUTH_ID = ln_auth_id;
            END IF;
            UPDATE PSS.AUTH 
               SET AUTH_STATE_ID = DECODE(pc_auth_result_cd, 'Y', 2, 'P', 5, 'N', 3, 'O', 7, 'F', 4, 'R', 7),
                   AUTH_RESULT_CD = pc_auth_result_cd,
                   AUTH_RESP_CD = NVL(pv_authority_response_cd, AUTH_RESP_CD),
                   AUTH_RESP_DESC = NVL(pv_authority_response_desc, AUTH_RESP_DESC),
                   AUTH_AUTHORITY_TRAN_CD = NVL(pv_authority_tran_cd, AUTH_AUTHORITY_TRAN_CD),
                   AUTH_AUTHORITY_REF_CD = NVL(pv_authority_ref_cd, AUTH_AUTHORITY_REF_CD),
                   AUTH_AUTHORITY_TS = NVL(pd_authority_ts, AUTH_AUTHORITY_TS),
                   AUTH_AMT_APPROVED = NVL(pn_approved_amt / ln_minor_currency_factor, AUTH_AMT_APPROVED),
                   AUTH_AUTHORITY_MISC_DATA = NVL(pv_authority_misc_data, AUTH_AUTHORITY_MISC_DATA)
             WHERE AUTH_ID = ln_auth_id
             RETURNING TRAN_ID, AUTH_TYPE_CD, OVERRIDE_TRANS_TYPE_ID INTO pn_tran_id, pc_auth_type_cd, pn_override_trans_type_id;
			IF pn_settlement_batch_id IS NOT NULL THEN
                UPDATE PSS.TRAN_SETTLEMENT_BATCH
                   SET TRAN_SETTLEMENT_B_AMT = NVL(pn_approved_amt / ln_minor_currency_factor, TRAN_SETTLEMENT_B_AMT),
                       TRAN_SETTLEMENT_B_RESP_CD = NVL(pv_authority_response_cd, TRAN_SETTLEMENT_B_RESP_CD),
                       TRAN_SETTLEMENT_B_RESP_DESC = NVL(pv_authority_response_desc, TRAN_SETTLEMENT_B_RESP_DESC)
                 WHERE SETTLEMENT_BATCH_ID = pn_settlement_batch_id
                   AND AUTH_ID = ln_auth_id;               
            END IF;
        ELSIF ln_refund_id IS NOT NULL THEN
            IF pc_major_currency_used = 'Y' THEN
                ln_minor_currency_factor := 1;
            ELSE
                SELECT C.MINOR_CURRENCY_FACTOR
                  INTO ln_minor_currency_factor
                  FROM PSS.CURRENCY C
                  JOIN PSS.POS_PTA PTA ON C.CURRENCY_CD = NVL(pta.CURRENCY_CD, 'USD')
                  JOIN PSS.TRAN X ON PTA.POS_PTA_ID = X.POS_PTA_ID
                  JOIN PSS.REFUND R ON X.TRAN_ID = R.TRAN_ID
                 WHERE R.REFUND_ID = ln_refund_id;
            END IF;          
            UPDATE PSS.REFUND
               SET REFUND_STATE_ID = DECODE(pc_auth_result_cd, 'Y', 1, 'P', 1, 'N', 2, 'O', 2, 'F', 3, 'R', 2),
                REFUND_RESP_CD = NVL(pv_authority_response_cd, REFUND_RESP_CD),
                REFUND_RESP_DESC = NVL(pv_authority_response_desc, REFUND_RESP_DESC),
                REFUND_AUTHORITY_TS = NVL(pd_authority_ts, REFUND_AUTHORITY_TS),
                REFUND_AUTHORITY_MISC_DATA = NVL(pv_authority_misc_data, REFUND_AUTHORITY_MISC_DATA)
             WHERE REFUND_ID = ln_refund_id
             RETURNING TRAN_ID, REFUND_TYPE_CD, OVERRIDE_TRANS_TYPE_ID INTO pn_tran_id, pc_auth_type_cd, pn_override_trans_type_id;
            IF pn_settlement_batch_id IS NOT NULL THEN 
                UPDATE PSS.REFUND_SETTLEMENT_BATCH
                   SET REFUND_SETTLEMENT_B_AMT = NVL(pn_approved_amt / ln_minor_currency_factor, REFUND_SETTLEMENT_B_AMT),
                       REFUND_SETTLEMENT_B_RESP_CD = NVL(pv_authority_response_cd, REFUND_SETTLEMENT_B_RESP_CD),
                       REFUND_SETTLEMENT_B_RESP_DESC = NVL(pv_authority_response_desc, REFUND_SETTLEMENT_B_RESP_DESC)
                 WHERE SETTLEMENT_BATCH_ID = pn_settlement_batch_id
                   AND REFUND_ID = ln_refund_id;   
            END IF;
        END IF;
        
        -- update tran_state_cd
        UPDATE PSS.TRAN
           SET TRAN_STATE_CD = AFTER_SETTLE_TRAN_STATE_CD(DECODE(ln_auth_id, NULL, NULL, pc_auth_type_cd), DECODE(ln_auth_id, NULL,pc_auth_type_cd), pc_auth_result_cd, TRAN_DEVICE_RESULT_TYPE_CD)
         WHERE TRAN_ID = pn_tran_id
         RETURNING TRAN_GLOBAL_TRANS_CD INTO pv_tran_global_trans_cd;
    END;
    
    -- R36 and below
    PROCEDURE UPDATE_TRAN_FEEDBACK(
        pn_terminal_batch_id PSS.SETTLEMENT_BATCH.TERMINAL_BATCH_ID%TYPE,
        pn_settlement_batch_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_sale_auth_id PSS.AUTH.AUTH_ID%TYPE,
        pn_refund_id PSS.REFUND.REFUND_ID%TYPE,
        pc_tran_type_cd CHAR,
        pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
        pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pv_authority_response_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
        pv_authority_response_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
        pd_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
        pn_approved_amt PSS.AUTH.AUTH_AMT_APPROVED%TYPE,
        pn_sale_amt PSS.AUTH.AUTH_AMT%TYPE,
        pc_major_currency_used CHAR,
        pv_tran_global_trans_cd OUT PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
		pc_settle_update_needed OUT VARCHAR2,
		pc_auth_type_cd OUT VARCHAR2,
        pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE)
    IS
        ln_override_trans_type_id PSS.PAYMENT_SUBTYPE.TRANS_TYPE_ID%TYPE;
    BEGIN
        UPDATE_TRAN_FEEDBACK(
            pn_terminal_batch_id,
            pn_settlement_batch_id,
            pn_sale_auth_id,
            pn_refund_id,
            pc_tran_type_cd,
            pv_authority_tran_cd,
            pv_authority_ref_cd,
            pc_auth_result_cd,
            pv_authority_response_cd,
            pv_authority_response_desc,
            pv_authority_misc_data,
            pd_authority_ts,
            pn_approved_amt,
            pn_sale_amt,
            pc_major_currency_used,
            pv_tran_global_trans_cd,
            pc_settle_update_needed,
            pc_auth_type_cd ,
            pn_tran_id,
            ln_override_trans_type_id);
    END;
    
END PKG_SETTLEMENT;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/PSS/PKG_TRAN.pbk?rev=1.212
CREATE OR REPLACE PACKAGE BODY PSS.PKG_TRAN IS
    
FUNCTION sf_find_host_id(
    pn_device_id DEVICE.DEVICE_ID%TYPE,
    pn_host_port_num IN HOST.HOST_PORT_NUM%TYPE,
    pn_host_position_num HOST.HOST_POSITION_NUM%TYPE)
  RETURN HOST.HOST_ID%TYPE
IS
    ln_host_id HOST.HOST_ID%TYPE;
BEGIN
    SELECT MAX(H.HOST_ID)
      INTO ln_host_id
      FROM DEVICE.HOST H
     WHERE H.DEVICE_ID = pn_device_id
       AND H.HOST_PORT_NUM = pn_host_port_num
       AND H.HOST_POSITION_NUM = pn_host_position_num;

    IF ln_host_id IS NULL THEN
        -- Use base host
        SELECT MAX(H.HOST_ID)
          INTO ln_host_id
          FROM DEVICE.HOST H
         WHERE H.DEVICE_ID = pn_device_id
           AND H.HOST_PORT_NUM = 0;
    END IF;
    
    RETURN ln_host_id;
END;

PROCEDURE SP_CREATE_REFUND (
    pv_global_trans_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
    pv_orig_global_trans_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
    pn_refund_utc_ts_ms IN NUMBER,
    pn_orig_upload_utc_ts_ms IN NUMBER,
    pn_refund_amt PSS.REFUND.REFUND_AMT%TYPE,
    pn_refund_desc PSS.REFUND.REFUND_DESC%TYPE,
    pn_refund_issue_by PSS.REFUND.REFUND_ISSUE_BY%TYPE,
    pn_refund_type_cd PSS.REFUND.REFUND_TYPE_CD%TYPE,
    pc_entry_method_cd PSS.REFUND.ACCT_ENTRY_METHOD_CD%TYPE,
    pc_already_inserted_flag OUT VARCHAR2,
    pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
    pn_orig_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
    pn_minor_currency_factor OUT PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
    pn_payment_subtype_key_id OUT PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE,
    pv_payment_subtype_class OUT PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE)
IS
    ld_orig_tran_upload_ts PSS.TRAN.TRAN_UPLOAD_TS%TYPE;
    ld_refund_ts PSS.TRAN.TRAN_START_TS%TYPE;
    lv_last_lock_utc_ts VARCHAR2(128);
    ln_cnt PLS_INTEGER;
    ln_orig_tran_id PSS.TRAN.TRAN_ID%TYPE;
    lv_lock_string VARCHAR2(100);
    ln_start INTEGER;
    ln_end INTEGER;
    ln_consumer_acct_sub_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_SUB_TYPE_ID%TYPE;
    ln_override_trans_type_id PSS.REFUND.OVERRIDE_TRANS_TYPE_ID%TYPE;   
BEGIN
    ln_start := INSTR(pv_global_trans_cd, ':', 1, 1) + 1;
    ln_end := INSTR(pv_global_trans_cd, ':', 1, 3);
    IF ln_end <= 0 THEN
        ln_end := LENGTH(pv_global_trans_cd) + 1;
    END IF;
    lv_lock_string := SUBSTR(pv_global_trans_cd, ln_start,  ln_end -  ln_start);
    ld_orig_tran_upload_ts := DBADMIN.UTC_TO_LOCAL_DATE(DBADMIN.MILLIS_TO_TIMESTAMP(pn_orig_upload_utc_ts_ms));
    ld_refund_ts := DBADMIN.UTC_TO_LOCAL_DATE(DBADMIN.MILLIS_TO_TIMESTAMP(pn_refund_utc_ts_ms));
    lv_last_lock_utc_ts := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', lv_lock_string);
    -- check if refund already exists
    BEGIN
        SELECT X.TRAN_ID, c.MINOR_CURRENCY_FACTOR, pta.PAYMENT_SUBTYPE_KEY_ID, PST.PAYMENT_SUBTYPE_CLASS, 'Y'
          INTO pn_tran_id, pn_minor_currency_factor, pn_payment_subtype_key_id, pv_payment_subtype_class, pc_already_inserted_flag
          FROM PSS.TRAN X
          JOIN PSS.POS_PTA PTA ON PTA.POS_PTA_ID = X.POS_PTA_ID
          JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
          JOIN PSS.PAYMENT_SUBTYPE PST ON PTA.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
         WHERE X.TRAN_GLOBAL_TRANS_CD = pv_global_trans_cd;
        RETURN;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            pc_already_inserted_flag := 'N';
    END;
    -- Find original transaction
    BEGIN
        SELECT X.TRAN_ID, PSS.SEQ_TRAN_ID.NEXTVAL, c.MINOR_CURRENCY_FACTOR, pta.PAYMENT_SUBTYPE_KEY_ID, PST.PAYMENT_SUBTYPE_CLASS, CA.CONSUMER_ACCT_SUB_TYPE_ID
          INTO pn_orig_tran_id, pn_tran_id, pn_minor_currency_factor, pn_payment_subtype_key_id, pv_payment_subtype_class, ln_consumer_acct_sub_type_id
          FROM PSS.TRAN X
          JOIN PSS.POS_PTA PTA ON PTA.POS_PTA_ID = X.POS_PTA_ID
          JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
          JOIN PSS.PAYMENT_SUBTYPE PST ON PTA.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
          LEFT OUTER JOIN PSS.CONSUMER_ACCT CA ON X.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
         WHERE X.TRAN_GLOBAL_TRANS_CD = pv_orig_global_trans_cd
           AND X.TRAN_UPLOAD_TS = ld_orig_tran_upload_ts;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
             RAISE_APPLICATION_ERROR(-20381, 'Original Transaction ''' || pv_orig_global_trans_cd || ''', uploaded at ' || TO_CHAR(ld_orig_tran_upload_ts, 'MM/DD/YYYY HH24:MI:SS') || ' not found');
    END;
    
    INSERT INTO PSS.TRAN (
            TRAN_ID,
            PARENT_TRAN_ID,
            TRAN_START_TS,
            TRAN_END_TS,
            TRAN_UPLOAD_TS,
            TRAN_GLOBAL_TRANS_CD,
            TRAN_STATE_CD,
            CONSUMER_ACCT_ID,
            TRAN_DEVICE_TRAN_CD,
            POS_PTA_ID,
            TRAN_DEVICE_RESULT_TYPE_CD,
            TRAN_RECEIVED_RAW_ACCT_DATA,
            PAYMENT_SUBTYPE_KEY_ID,
            PAYMENT_SUBTYPE_CLASS,
            CLIENT_PAYMENT_TYPE_CD,
            DEVICE_NAME
            )
     SELECT pn_tran_id,
            pn_orig_tran_id,
            ld_refund_ts,
            ld_refund_ts,
            NULL, /* Must be NULL so that PSSUpdater will not pick it up */
            pv_global_trans_cd,
            '8',
            O.CONSUMER_ACCT_ID,
            SUBSTR(pv_global_trans_cd, INSTR(pv_global_trans_cd, ':', 1, 2) + 1, LENGTH(pv_global_trans_cd)),
            O.POS_PTA_ID,
            O.TRAN_DEVICE_RESULT_TYPE_CD,
            O.TRAN_RECEIVED_RAW_ACCT_DATA,
            pp.PAYMENT_SUBTYPE_KEY_ID,
            ps.PAYMENT_SUBTYPE_CLASS,
            ps.CLIENT_PAYMENT_TYPE_CD,
            d.DEVICE_NAME
    FROM PSS.TRAN O
    JOIN pss.pos_pta pp ON O.pos_pta_id = pp.pos_pta_id
    JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
    JOIN pss.pos p ON pp.pos_id = p.pos_id
    JOIN device.device d ON p.device_id = d.device_id
    WHERE O.TRAN_ID = pn_orig_tran_id;
    
    IF ln_consumer_acct_sub_type_id IS NOT NULL THEN
        IF ln_consumer_acct_sub_type_id = 2 THEN
            ln_override_trans_type_id := 31;
        ELSE
            ln_override_trans_type_id := 20;
        END IF;
    END IF;
    
    INSERT INTO PSS.REFUND (
            TRAN_ID,
            REFUND_AMT,
            REFUND_DESC,
            REFUND_ISSUE_TS,
            REFUND_ISSUE_BY,
            REFUND_TYPE_CD,
            REFUND_STATE_ID,
            ACCT_ENTRY_METHOD_CD,
            OVERRIDE_TRANS_TYPE_ID
        ) VALUES (
            pn_tran_id,
            -ABS(pn_refund_amt),
            pn_refund_desc,
            ld_refund_ts,
            pn_refund_issue_by,
            pn_refund_type_cd,
            6,
            pc_entry_method_cd,
            ln_override_trans_type_id);
END;

PROCEDURE SP_CLOSE_CONSUMER_ACCT (
    pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
    pv_refund_issue_by PSS.REFUND.REFUND_ISSUE_BY%TYPE,
    pn_refund_amt OUT PSS.REFUND.REFUND_AMT%TYPE,
    pn_eft_credit_amt OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
    pn_doc_id OUT CORP.DOC.DOC_ID%TYPE
)
IS
    ln_consumer_acct_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_TYPE_ID%TYPE;
    ln_consumer_acct_sub_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_SUB_TYPE_ID%TYPE;
    ln_corp_customer_id PSS.CONSUMER_ACCT.CORP_CUSTOMER_ID%TYPE;
    lv_currency_cd PSS.CONSUMER_ACCT.CURRENCY_CD%TYPE;
    lv_consumer_acct_cd PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD%TYPE;
    lv_consumer_acct_identifier PSS.CONSUMER_ACCT.CONSUMER_ACCT_IDENTIFIER%TYPE;
    ln_refund_amt PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE; 
    ln_eft_credit_amt PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE; 
    ln_tran_id PSS.TRAN.TRAN_ID%TYPE;
    ln_orig_tran_id PSS.TRAN.PARENT_TRAN_ID%TYPE;
    ln_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE;
    ld_refund_ts PSS.TRAN.TRAN_START_TS%TYPE := SYSDATE;
    lv_replenish_card_masked PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD%TYPE;
    lv_tran_device_tran_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE;
    ln_ledger_id CORP.LEDGER.LEDGER_ID%TYPE;
BEGIN
    pn_refund_amt := 0;
    pn_eft_credit_amt := 0;
    
    UPDATE PSS.CONSUMER_ACCT
       SET CONSUMER_ACCT_ACTIVE_YN_FLAG = 'N'
     WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id
      RETURNING CONSUMER_ACCT_TYPE_ID, CONSUMER_ACCT_REPLEN_BALANCE, CONSUMER_ACCT_PROMO_BALANCE,
        CORP_CUSTOMER_ID, CURRENCY_CD, CONSUMER_ACCT_CD, CONSUMER_ACCT_IDENTIFIER, CONSUMER_ACCT_SUB_TYPE_ID
      INTO ln_consumer_acct_type_id, ln_refund_amt, ln_eft_credit_amt, ln_corp_customer_id, lv_currency_cd, lv_consumer_acct_cd, lv_consumer_acct_identifier, ln_consumer_acct_sub_type_id;
    
    UPDATE PSS.CONSUMER_ACCT
       SET CONSUMER_ACCT_BALANCE = 0,
           CONSUMER_ACCT_REPLEN_BALANCE = 0,
           CONSUMER_ACCT_PROMO_BALANCE = 0
     WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id;
     
    IF ln_consumer_acct_type_id != 3 OR ln_consumer_acct_sub_type_id != 1 THEN
        RETURN;
    END IF;
    
    IF ln_refund_amt > 0 THEN
        --TODO: Handle when tran is no longer in table (b/c of retention policy)
        SELECT LAST_REPLENISH_TRAN_ID, REPLENISH_POS_PTA_ID, REPLENISH_CARD_MASKED
          INTO ln_orig_tran_id, ln_pos_pta_id, lv_replenish_card_masked
          FROM (SELECT LAST_REPLENISH_TRAN_ID, REPLENISH_POS_PTA_ID, REPLENISH_CARD_MASKED
                  FROM PSS.CONSUMER_ACCT_REPLENISH
                 WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id
                 ORDER BY LAST_REPLENISH_TRAN_TS DESC, PRIORITY, LAST_REPLENISH_TRAN_ID DESC)
         WHERE ROWNUM = 1;
     
        SELECT PSS.SEQ_TRAN_ID.NEXTVAL
        INTO ln_tran_id
        FROM DUAL;
        
        lv_tran_device_tran_cd := DBADMIN.DATE_TO_MILLIS(ld_refund_ts) / 1000;
    
        INSERT INTO PSS.TRAN (
                    TRAN_ID,
                    PARENT_TRAN_ID,
                    TRAN_START_TS,
                    TRAN_END_TS,
                    TRAN_UPLOAD_TS,
                    TRAN_GLOBAL_TRANS_CD,
                    TRAN_STATE_CD,
                    TRAN_DEVICE_TRAN_CD,
                    POS_PTA_ID,
                    TRAN_DEVICE_RESULT_TYPE_CD,
                    TRAN_RECEIVED_RAW_ACCT_DATA,
                    PAYMENT_SUBTYPE_KEY_ID,
                    PAYMENT_SUBTYPE_CLASS,
                    CLIENT_PAYMENT_TYPE_CD,
                    DEVICE_NAME
                    )
             SELECT ln_tran_id,
                    ln_orig_tran_id,
                    ld_refund_ts,
                    ld_refund_ts,
                    NULL,
                    'RF:' || d.device_name || ':' || lv_tran_device_tran_cd || ':R1',
                    PKG_CONST.TRAN_STATE__BATCH,
                    lv_tran_device_tran_cd,
                    ln_pos_pta_id,
                    PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_RCPT,
                    lv_replenish_card_masked,
                    pp.PAYMENT_SUBTYPE_KEY_ID,
                    ps.PAYMENT_SUBTYPE_CLASS,
                    ps.CLIENT_PAYMENT_TYPE_CD,
                    d.DEVICE_NAME
            FROM pss.pos_pta pp
            JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
            JOIN pss.pos p ON pp.pos_id = p.pos_id
            JOIN device.device d ON p.device_id = d.device_id
            WHERE pp.pos_pta_id = ln_pos_pta_id;
            
        INSERT INTO PSS.TRAN_LINE_ITEM (
            TRAN_ID,
            TRAN_LINE_ITEM_AMOUNT,
            TRAN_LINE_ITEM_POSITION_CD,
            TRAN_LINE_ITEM_TAX,
            TRAN_LINE_ITEM_TYPE_ID,
            TRAN_LINE_ITEM_QUANTITY,
            TRAN_LINE_ITEM_DESC,
            TRAN_LINE_ITEM_BATCH_TYPE_CD,
            SALE_RESULT_ID,
            APPLY_TO_CONSUMER_ACCT_ID
        ) VALUES (
            ln_tran_id,
            -ln_refund_amt,
            '0',
            0,
            500,
            1,
            'Prepaid account closure',
            PKG_CONST.TRAN_BATCH_TYPE__ACTUAL,
            PKG_CONST.SALE_RES__SUCCESS,
            pn_consumer_acct_id
        );
            
        INSERT INTO PSS.REFUND (
            TRAN_ID,
            REFUND_AMT,
            REFUND_DESC,
            REFUND_ISSUE_TS,
            REFUND_ISSUE_BY,
            REFUND_TYPE_CD,
            REFUND_STATE_ID,
            ACCT_ENTRY_METHOD_CD
        ) VALUES (
            ln_tran_id,
            -ln_refund_amt,
            'Prepaid account closure',
            ld_refund_ts,
            pv_refund_issue_by,
            'G',
            6,
            2);
            
        pn_refund_amt := ln_refund_amt;
    END IF;
    
    IF ln_eft_credit_amt > 0 THEN
        CORP.PAYMENTS_PKG.ADJUSTMENT_INS(ln_corp_customer_id, pv_refund_issue_by, lv_currency_cd,
            'Promo credit for prepaid account closure, card # ' || lv_consumer_acct_cd || ', card ID ' || lv_consumer_acct_identifier,
            ln_eft_credit_amt, pn_doc_id, ln_ledger_id);
        pn_eft_credit_amt := ln_eft_credit_amt;
    END IF;
END;

PROCEDURE PROCESS_ISIS_TRAN (
    pn_tran_id PSS.TRAN.TRAN_ID%TYPE    
)
IS
    ln_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE;
    lv_isis_promo_status_cd PSS.PROMOTION.STATUS_CD%TYPE;
    ln_tran_diff PSS.CONSUMER_PROMOTION.TRAN_COUNT%TYPE;
    lv_tran_info PSS.TRAN.TRAN_INFO%TYPE;
    lv_payment_subtype_class PSS.TRAN.PAYMENT_SUBTYPE_CLASS%TYPE; 
    lv_auth_resp_cd PSS.AUTH.AUTH_RESP_CD%TYPE;
    ln_sale_amount PSS.SALE.SALE_AMOUNT%TYPE;
    ln_isis_promo_pos_pta_id PSS.POS_PTA.POS_PTA_ID%TYPE;
    ld_sysdate DATE := SYSDATE;
BEGIN
    SELECT NVL(MAX(STATUS_CD), 'D')
    INTO lv_isis_promo_status_cd
    FROM PSS.PROMOTION 
    WHERE PROMOTION_ID = 1;
    
    IF lv_isis_promo_status_cd != 'A' THEN
        RETURN;
    END IF;
    
    SELECT MAX(CA.CONSUMER_ID), MAX(S.SALE_AMOUNT), NVL(MAX(T.TRAN_INFO), '-'), NVL(MAX(T.PAYMENT_SUBTYPE_CLASS), '-'), NVL(MAX(A.AUTH_RESP_CD), '-'), NVL(MAX(PP2.POS_PTA_ID), 0)
    INTO ln_consumer_id, ln_sale_amount, lv_tran_info, lv_payment_subtype_class, lv_auth_resp_cd, ln_isis_promo_pos_pta_id
    FROM PSS.TRAN T
    JOIN PSS.SALE S ON T.TRAN_ID = S.TRAN_ID
    JOIN PSS.CONSUMER_ACCT CA ON T.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID   
    JOIN PSS.POS_PTA PP ON T.POS_PTA_ID = PP.POS_PTA_ID
    JOIN PSS.PAYMENT_SUBTYPE PS ON PP.PAYMENT_SUBTYPE_ID = PS.PAYMENT_SUBTYPE_ID
    LEFT OUTER JOIN PSS.AUTH A ON T.TRAN_ID = A.TRAN_ID AND A.AUTH_TYPE_CD = 'N' AND A.AUTH_STATE_ID IN (2, 5)
    LEFT OUTER JOIN (PSS.POS_PTA PP2
        JOIN PSS.PAYMENT_SUBTYPE PS2 ON PP2.PAYMENT_SUBTYPE_ID = PS2.PAYMENT_SUBTYPE_ID AND PS2.PAYMENT_SUBTYPE_CLASS = 'Isis'
    ) ON PP.POS_ID = PP2.POS_ID AND PP2.POS_PTA_ACTIVATION_TS < ld_sysdate AND (PP2.POS_PTA_DEACTIVATION_TS IS NULL OR PP2.POS_PTA_DEACTIVATION_TS > ld_sysdate)
        AND NVL(PP.AUTHORITY_PAYMENT_MASK_ID, PS.AUTHORITY_PAYMENT_MASK_ID) = NVL(PP2.AUTHORITY_PAYMENT_MASK_ID, PS2.AUTHORITY_PAYMENT_MASK_ID)
        AND (T.PAYMENT_SUBTYPE_CLASS = 'Isis' OR PP.POS_PTA_ID != PP2.POS_PTA_ID)
    WHERE T.TRAN_ID = pn_tran_id;
    
    IF ln_consumer_id IS NULL OR ln_sale_amount IS NULL OR lv_tran_info LIKE '%Isis loyalty updated%' OR ln_isis_promo_pos_pta_id < 1 THEN
        RETURN;
    END IF;
    
    IF ln_sale_amount > 0 THEN
        IF lv_payment_subtype_class != 'Isis' THEN
            LOOP
                UPDATE PSS.CONSUMER_PROMOTION
                SET TRAN_COUNT = TRAN_COUNT + 1
                WHERE CONSUMER_ID = ln_consumer_id AND PROMOTION_ID = 1;
                
                IF SQL%FOUND THEN
                    EXIT;
                END IF;
                
                BEGIN
                    INSERT INTO PSS.CONSUMER_PROMOTION(CONSUMER_ID, PROMOTION_ID, TRAN_COUNT)
                    VALUES(ln_consumer_id, 1, 1);
                    EXIT;
                EXCEPTION
                    WHEN DUP_VAL_ON_INDEX THEN
                        NULL;
                END;
            END LOOP;
        END IF;
    ELSE
        IF lv_payment_subtype_class = 'Isis' AND lv_auth_resp_cd = 'ISIS_PROMO' THEN
            UPDATE PSS.CONSUMER_PROMOTION
            SET TRAN_COUNT = CASE WHEN TRAN_COUNT - 1 >= 0 THEN TRAN_COUNT - 1 ELSE 0 END,
                PROMO_TRAN_COUNT = CASE WHEN PROMO_TRAN_COUNT - 1 >= 0 THEN PROMO_TRAN_COUNT - 1 ELSE 0 END
            WHERE CONSUMER_ID = ln_consumer_id AND PROMOTION_ID = 1;
        END IF;
    END IF;
    
    UPDATE PSS.TRAN
    SET TRAN_INFO = SUBSTR('Isis loyalty updated' || DECODE(TRAN_INFO, NULL, '', ';' || TRAN_INFO), 1, 1000)
    WHERE TRAN_ID = pn_tran_id AND NVL(TRAN_INFO, '-') NOT LIKE '%Isis loyalty updated%';
END;

-- R33+ signature
PROCEDURE sp_create_sale(
    pc_global_event_cd_prefix IN CHAR,
    pv_device_name IN device.device_name%TYPE,
    pv_device_tran_cd IN pss.tran.tran_device_tran_cd%TYPE,
    pn_device_batch_id IN pss.sale.device_batch_id%TYPE,
    pc_sale_type_cd IN pss.sale.sale_type_cd%TYPE,
    pn_sale_utc_ts_ms IN NUMBER,
    pn_sale_utc_offset_min IN pss.sale.sale_utc_offset_min%TYPE,
    pv_tran_device_result_type_cd IN pss.tran.tran_device_result_type_cd%TYPE,
    pn_sale_result_id IN pss.sale.sale_result_id%TYPE,
    pn_sale_amount IN NUMBER,
    pc_receipt_result_cd IN pss.sale.receipt_result_cd%TYPE,
    pc_tran_batch_type_cd IN pss.tran_line_item_batch_type.tran_line_item_batch_type_cd%TYPE,
    pv_hash_type_cd IN pss.sale.hash_type_cd%TYPE,
    pv_tran_line_item_hash IN pss.sale.tran_line_item_hash%TYPE,
    pv_global_session_cd IN VARCHAR2,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_tran_id OUT pss.tran.tran_id%TYPE,
    pv_tran_state_cd OUT pss.tran.tran_state_cd%TYPE,
    pc_void_allowed IN PSS.SALE.VOID_ALLOWED%TYPE DEFAULT 'N')
IS
/*
    Returned result codes:
        RESULT__SUCCESS
        RESULT__FAILURE
        RESULT__INVALID_PARAMETER
        RESULT__DUPLICATE
*/
    lv_global_trans_cd pss.tran.tran_global_trans_cd%TYPE;
    ld_tran_upload_ts pss.tran.tran_upload_ts%TYPE;
    lt_sale_start_utc_ts pss.sale.sale_start_utc_ts%TYPE;
    ld_tran_start_ts pss.tran.tran_start_ts%TYPE;
    ld_original_tran_start_ts pss.tran.tran_start_ts%TYPE;
    ld_tran_server_ts DATE;
    ln_device_id device.device_id%TYPE;
    ln_pos_pta_id pss.tran.pos_pta_id%TYPE;
    ld_current_ts DATE := SYSDATE;
    lc_client_payment_type_cd pss.client_payment_type.client_payment_type_cd%TYPE;
    ln_tran_exists NUMBER := PKG_CONST.BOOLEAN__FALSE;
    ln_tli_hash_match NUMBER;
    ln_insert_tran NUMBER := PKG_CONST.BOOLEAN__FALSE;
    ln_original_tran_id pss.tran.tran_id%TYPE;
    ln_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE;
    lc_sale_type_cd pss.sale.sale_type_cd%TYPE;
    lv_last_lock_utc_ts VARCHAR2(128);
    lc_auth_hold_used PSS.TRAN.AUTH_HOLD_USED%TYPE;
    lv_orig_tran_state_cd pss.tran.tran_state_cd%TYPE;
    ln_consumer_acct_id pss.tran.consumer_acct_id%TYPE;
    ln_auth_amt_approved pss.auth.auth_amt_approved%TYPE;
    ln_auth_amt_allowed pss.auth.auth_amt_approved%TYPE;
    ln_sale_over_auth_amt_percent NUMBER;
    lv_email_from_address engine.app_setting.app_setting_value%TYPE;
    lv_email_to_address engine.app_setting.app_setting_value%TYPE;
    lv_error pss.tran.tran_info%TYPE;
    lc_original_void_allowed PSS.SALE.VOID_ALLOWED%TYPE;
    lv_card_key PSS.AUTH.CARD_KEY%TYPE;
    ln_original_sale_amount PSS.SALE.SALE_AMOUNT%TYPE;  
    lc_entry_method_cd PSS.AUTH.ACCT_ENTRY_METHOD_CD%TYPE;
    lc_calc_tran_state_cd CHAR(1) := 'Y';
    ln_host_id PSS.TRAN_LINE_ITEM.HOST_ID%TYPE;
    ln_new_host_count PLS_INTEGER;
    lc_orig_term_capture_flag CHAR(1);
    ln_count PLS_INTEGER;   
    ln_remaining_refund_amt PSS.REFUND.REFUND_AMT%TYPE;
BEGIN
    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
    pn_tran_id := 0;

    IF pc_global_event_cd_prefix NOT IN (PKG_CONST.EVENT_CODE_PREFIX__APP_LAYER,
        PKG_CONST.EVENT_CODE_PREFIX__LEGACY) THEN
        pn_result_cd := PKG_CONST.RESULT__INVALID_PARAMETER;
        pv_error_message := 'Invalid pc_global_event_cd_prefix: ' || pc_global_event_cd_prefix;
        RETURN;
    END IF;

    lv_global_trans_cd := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(pc_global_event_cd_prefix, pv_device_name, pv_device_tran_cd);
    
    lt_sale_start_utc_ts := CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_sale_utc_ts_ms) AS DATE);
    ld_tran_start_ts := CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_sale_utc_ts_ms + pn_sale_utc_offset_min * 60 * 1000) AS DATE);
    ld_tran_server_ts := CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_sale_utc_ts_ms) - SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) + CURRENT_TIMESTAMP AS DATE);

    lv_last_lock_utc_ts := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', pv_device_name || ':' || pv_device_tran_cd);

    BEGIN
        SELECT tran_id, tran_state_cd, tran_start_ts, tran_upload_ts, PKG_CONST.BOOLEAN__TRUE, tli_hash_match, 
               pos_pta_id, sale_type_cd, auth_hold_used, consumer_acct_id, VOID_ALLOWED, 
               SALE_AMOUNT
        INTO pn_tran_id, pv_tran_state_cd, ld_original_tran_start_ts, ld_tran_upload_ts, ln_tran_exists, ln_tli_hash_match, 
             ln_pos_pta_id, lc_sale_type_cd, lc_auth_hold_used, ln_consumer_acct_id, lc_original_void_allowed, 
             ln_original_sale_amount
        FROM
        (
            SELECT /*+ INDEX(t IDX_DEVICE_TRAN_CD) */ t.tran_id, t.tran_state_cd, t.tran_start_ts, t.tran_upload_ts,
                CASE WHEN s.hash_type_cd = pv_hash_type_cd
                    AND s.tran_line_item_hash = pv_tran_line_item_hash
                    AND s.sale_type_cd = pc_sale_type_cd THEN PKG_CONST.BOOLEAN__TRUE
                ELSE PKG_CONST.BOOLEAN__FALSE END AS tli_hash_match,
                t.pos_pta_id, s.sale_type_cd, NVL(t.auth_hold_used, 'N') auth_hold_used, t.consumer_acct_id, 
                s.VOID_ALLOWED, s.SALE_AMOUNT
            FROM pss.tran t
            LEFT OUTER JOIN pss.sale s ON t.tran_id = s.tran_id
            LEFT OUTER JOIN pss.auth aa ON t.tran_id = aa.tran_id AND aa.auth_type_cd = 'N'
            WHERE t.tran_device_tran_cd = pv_device_tran_cd 
              AND (t.tran_global_trans_cd = lv_global_trans_cd OR t.tran_global_trans_cd LIKE lv_global_trans_cd || ':%')
              AND aa.auth_action_id IS NULL
            ORDER BY CASE WHEN s.sale_type_cd = pc_sale_type_cd THEN 1 
                          WHEN s.sale_type_cd IN('A', 'I') AND pc_sale_type_cd IN('A', 'I') THEN 2 
                          WHEN s.sale_type_cd IS NULL AND pc_sale_type_cd IN('A', 'I') THEN 3 
                          ELSE 4 END,
                CASE WHEN t.tran_global_trans_cd = lv_global_trans_cd THEN 1 ELSE 2 END,
                tli_hash_match DESC, t.tran_start_ts, t.created_ts
        )
        WHERE ROWNUM = 1;
    
        ln_original_tran_id := pn_tran_id;
        lv_orig_tran_state_cd := pv_tran_state_cd;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            ln_insert_tran := PKG_CONST.BOOLEAN__TRUE;
    END;

    -- Handle each case
    IF ln_tran_exists = PKG_CONST.BOOLEAN__TRUE THEN
        IF ln_tli_hash_match = PKG_CONST.BOOLEAN__TRUE AND ln_original_sale_amount = pn_sale_amount THEN
            UPDATE pss.sale
               SET duplicate_count = duplicate_count + 1
             WHERE tran_id = pn_tran_id;
    
            pv_tran_state_cd := PKG_CONST.TRAN_STATE__DUPLICATE;
            pn_result_cd := PKG_CONST.RESULT__DUPLICATE;
            pv_error_message := 'Duplicate sale, original tran_id: ' || pn_tran_id;
            RETURN;
        ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__CASH THEN
            lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CASH;
            pv_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE;
            ln_insert_tran := PKG_CONST.BOOLEAN__TRUE;
        ELSIF lc_sale_type_cd = PKG_CONST.SALE_TYPE__CASH THEN
            ln_insert_tran := PKG_CONST.BOOLEAN__TRUE;
            IF pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED THEN
                lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CREDIT;
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND;
            ELSIF pn_sale_result_id != 0 /* Success */ AND NVL(pn_sale_amount, 0) = 0 THEN
                lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CREDIT;
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED;
            ELSE
                lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CREDIT;
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__SALE_NO_AUTH;
            END IF;
        ELSIF lc_sale_type_cd = PKG_CONST.SALE_TYPE__ACTUAL AND pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED THEN
            RETURN; -- ignore this as we have already processd the actual sale
        ELSIF lc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED AND pc_sale_type_cd = PKG_CONST.SALE_TYPE__ACTUAL THEN
            NULL; -- just update the transaction  
        ELSIF lc_sale_type_cd IS NOT NULL THEN 
            lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CREDIT;
            IF NVL(ln_original_sale_amount, 0) = 0 AND pc_void_allowed = 'Y' THEN
                RETURN; -- we have already processed the cancel of this charged sale
            ELSIF (pv_tran_device_result_type_cd IN (PKG_CONST.TRAN_DEV_RES__CANCELLED,
                PKG_CONST.TRAN_DEV_RES__AUTH_FAILURE,
                PKG_CONST.TRAN_DEV_RES__FAILURE,
                PKG_CONST.TRAN_DEV_RES__TIMEOUT) OR pn_sale_amount <= 0) 
                AND lc_original_void_allowed = 'Y' 
                AND NVL(ln_original_sale_amount, 0) > 0
                AND pv_tran_state_cd IN(
                    PKG_CONST.TRAN_STATE__SALE_NO_AUTH, 
                    PKG_CONST.TRAN_STATE__BATCH, 
                    PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND, 
                    PKG_CONST.TRAN_STATE__BATCH_INTENDED,
                    PKG_CONST.TRAN_STATE__PROCESSED_TRAN, 
                    PKG_CONST.TRAN_STATE__PROCESSED_TRAN_INT,
                    PKG_CONST.TRAN_STATE__PROCESSING_TRAN,
                    PKG_CONST.TRAN_STATE__INCOMPLETE,
                    PKG_CONST.TRAN_STATE__INCOMPLETE_ERROR,
                    PKG_CONST.TRAN_STATE__PROCESSING_STTLMT,
                    PKG_CONST.TRAN_STATE__SETTLEMENT,
                    PKG_CONST.TRAN_STATE__COMPLETE,
                    PKG_CONST.TRAN_STATE__STLMT_INCOMPLETE,
                    PKG_CONST.TRAN_STATE__STLMT_ERROR,
                    PKG_CONST.TRAN_STATE__PROCESSING_BATCH,
                    PKG_CONST.TRAN_STATE__PRCSNG_TRAN_RETRY) THEN -- cancel of a charge sale
                
                -- Lock tran row by updating it
                UPDATE PSS.TRAN
                   SET TRAN_STATE_CD = pv_tran_state_cd
                 WHERE TRAN_ID = pn_tran_id
                   AND TRAN_STATE_CD = pv_tran_state_cd;
                IF SQL%NOTFOUND THEN
                    RAISE_APPLICATION_ERROR(-20120, 'Tran State Cd changed while processing tran ' || pn_tran_id || '; please retry');
                END IF;
                -- figure out what to do based on current transaction state
                IF pv_tran_state_cd IN(
                    PKG_CONST.TRAN_STATE__SALE_NO_AUTH, 
                    PKG_CONST.TRAN_STATE__BATCH, 
                    PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND, 
                    PKG_CONST.TRAN_STATE__BATCH_INTENDED) THEN 
                    lc_calc_tran_state_cd := 'N'; -- easy case: update tran and line items and leave state as is
                ELSE
                    IF pv_tran_state_cd IN(PKG_CONST.TRAN_STATE__PROCESSED_TRAN) THEN
                        SELECT MAX(TB.TERMINAL_CAPTURE_FLAG) 
                          INTO lc_orig_term_capture_flag
                          FROM PSS.TERMINAL_BATCH TB
                          JOIN PSS.AUTH A ON A.TERMINAL_BATCH_ID = TB.TERMINAL_BATCH_ID
                         WHERE TB.TERMINAL_BATCH_CLOSE_TS IS NULL 
                          AND A.TRAN_ID = pn_tran_id;
                    END IF;
                    IF pv_tran_state_cd IN(PKG_CONST.TRAN_STATE__PROCESSED_TRAN) AND lc_orig_term_capture_flag = 'Y' THEN
                        IF lc_auth_hold_used = 'Y' THEN
                            UPDATE PSS.AUTH
                               SET AUTH_TYPE_CD = 'C', 
                                   AUTH_AMT = 0 
                             WHERE TRAN_ID = pn_tran_id 
                               AND AUTH_TYPE_CD = 'U';
                            lc_calc_tran_state_cd := 'N'; -- easy case: update tran and line items and leave state as is
                        ELSE --Remove from batch and cancel
                            UPDATE PSS.AUTH A
                               SET TERMINAL_BATCH_ID = NULL
                             WHERE A.TRAN_ID = pn_tran_id
                               AND A.TERMINAL_BATCH_ID IS NOT NULL
                               AND (SELECT TB.TERMINAL_BATCH_CLOSE_TS FROM PSS.TERMINAL_BATCH TB WHERE TB.TERMINAL_BATCH_ID = A.TERMINAL_BATCH_ID) IS NULL;
                            pv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED; 
                            lc_calc_tran_state_cd := 'N';
                        END IF;
                    ELSE -- create a refund and return
                        ln_original_tran_id := pn_tran_id;
                        pv_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH;
                        SELECT NVL(SUM(R.REFUND_AMT), 0) - ABS(ln_original_sale_amount), MAX(T.TRAN_ID)
                          INTO ln_remaining_refund_amt, pn_tran_id 
                          FROM PSS.TRAN T
                          JOIN PSS.REFUND R ON T.TRAN_ID = R.TRAN_ID
                         WHERE T.PARENT_TRAN_ID = ln_original_tran_id;
                        IF ln_remaining_refund_amt < 0 THEN
                            SELECT pss.seq_tran_id.NEXTVAL INTO pn_tran_id FROM DUAL; 
                            INSERT INTO PSS.TRAN (
                                    TRAN_ID,
                                    PARENT_TRAN_ID,
                                    TRAN_START_TS,
                                    TRAN_END_TS,
                                    TRAN_UPLOAD_TS,
                                    TRAN_GLOBAL_TRANS_CD,
                                    TRAN_STATE_CD,
                                    CONSUMER_ACCT_ID,
                                    TRAN_DEVICE_TRAN_CD,
                                    POS_PTA_ID,
                                    TRAN_DEVICE_RESULT_TYPE_CD,
                                    TRAN_RECEIVED_RAW_ACCT_DATA,
                                    PAYMENT_SUBTYPE_KEY_ID,
                                    PAYMENT_SUBTYPE_CLASS,
                                    CLIENT_PAYMENT_TYPE_CD,
                                    DEVICE_NAME)
                             SELECT pn_tran_id,
                                    ln_original_tran_id,
                                    ld_tran_start_ts,
                                    ld_tran_start_ts,
                                    ld_current_ts, /* Must NOT be NULL so that it will be imported */
                                    'RF' || SUBSTR(O.TRAN_GLOBAL_TRANS_CD, INSTR(O.TRAN_GLOBAL_TRANS_CD, ':'), 56) || ':1', 
                                    pv_tran_state_cd,
                                    ln_consumer_acct_id,
                                    pv_device_tran_cd,
                                    O.POS_PTA_ID,
                                    pv_tran_device_result_type_cd,
                                    O.TRAN_RECEIVED_RAW_ACCT_DATA,
                                    pp.PAYMENT_SUBTYPE_KEY_ID,
                                    ps.PAYMENT_SUBTYPE_CLASS,
                                    ps.CLIENT_PAYMENT_TYPE_CD,
                                    d.DEVICE_NAME
                            FROM PSS.TRAN O
                            JOIN pss.pos_pta pp ON O.pos_pta_id = pp.pos_pta_id
                            JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
                            JOIN pss.pos p ON pp.pos_id = p.pos_id
                            JOIN device.device d ON p.device_id = d.device_id
                            WHERE O.TRAN_ID = ln_original_tran_id;
                            SELECT MAX(ACCT_ENTRY_METHOD_CD)
                              INTO lc_entry_method_cd
                              FROM (SELECT ACCT_ENTRY_METHOD_CD
                                      FROM PSS.AUTH
                                     WHERE TRAN_ID = ln_original_tran_id
                                     ORDER BY DECODE(AUTH_TYPE_CD, 'N', 1, 5), AUTH_RESULT_CD DESC, AUTH_TS, AUTH_ID)
                             WHERE ROWNUM = 1;
                            SELECT p.DEVICE_ID
                              INTO ln_device_id
                              FROM PSS.POS_PTA PTA
                              JOIN PSS.POS P ON PTA.POS_ID = P.POS_ID
                             WHERE PTA.POS_PTA_ID = ln_pos_pta_id;
            
                            INSERT INTO PSS.REFUND (
                                    TRAN_ID,
                                    REFUND_AMT,
                                    REFUND_DESC,
                                    REFUND_ISSUE_TS,
                                    REFUND_ISSUE_BY,
                                    REFUND_TYPE_CD,
                                    REFUND_STATE_ID,
                                    ACCT_ENTRY_METHOD_CD
                                ) VALUES (
                                    pn_tran_id,
                                    ln_remaining_refund_amt,
                                    'Void of Charged Sale',
                                    ld_current_ts,
                                    'PSS',
                                    'V',
                                    6,
                                    lc_entry_method_cd);
                            -- Insert line item
                            ln_host_id := sf_find_host_id(ln_device_id, 0, 0);
                            IF ln_host_id IS NULL THEN
                                -- create default hosts
                                pkg_device_configuration.sp_create_default_hosts(ln_device_id, ln_new_host_count, pn_result_cd, pv_error_message);
                                IF pn_result_cd != PKG_CONST.RESULT__SUCCESS THEN
                                    RETURN;
                                END IF;
                                ln_host_id := sf_find_host_id(ln_device_id, 0, 0);
                            END IF;
                            INSERT INTO pss.tran_line_item (
                                tran_line_item_id,
                                tran_id,
                                tran_line_item_amount,
                                tran_line_item_position_cd,
                                tran_line_item_tax,
                                tran_line_item_type_id,
                                tran_line_item_quantity,
                                tran_line_item_desc,
                                host_id,
                                tran_line_item_batch_type_cd,
                                tran_line_item_ts,
                                sale_result_id
                            ) VALUES (
                                PSS.SEQ_TLI_ID.NEXTVAL,
                                pn_tran_id,
                                ln_remaining_refund_amt,
                                NULL,
                                NULL,
                                312, /*Cancellation Adjustment */
                                1,
                                'Void of Charged Sale',
                                ln_host_id,
                                'A',
                                ld_current_ts,
                                0);
                        END IF;       
                        pn_result_cd := PKG_CONST.RESULT__SALE_VOIDED;
                        pv_error_message := 'Refund issued for canceled transaction already in-process or settled, refund tran_id: ' || pn_tran_id || ', original tran_id: ' || ln_original_tran_id;
                        RETURN;
                    END IF;
                END IF;
            ELSE
                SELECT CASE WHEN (lc_auth_hold_used = 'Y' OR NVL(SUM(DECODE(TLI.TRAN_LINE_ITEM_BATCH_TYPE_CD, 'I', TLI.TRAN_LINE_ITEM_AMOUNT * TLI.TRAN_LINE_ITEM_QUANTITY)), 0) > 0) AND NVL(SUM(DECODE(TLI.TRAN_LINE_ITEM_BATCH_TYPE_CD, 'A', TLI.TRAN_LINE_ITEM_AMOUNT * TLI.TRAN_LINE_ITEM_QUANTITY)), 0) = 0 
                            THEN PKG_CONST.TRAN_STATE__COMPLETE_ERROR
                            ELSE PKG_CONST.TRAN_STATE__DUPLICATE
                       END
                  INTO pv_tran_state_cd
                  FROM PSS.TRAN_LINE_ITEM TLI
                 WHERE TLI.TRAN_ID = pn_tran_id;
                ln_insert_tran := PKG_CONST.BOOLEAN__TRUE;          
                IF pv_tran_state_cd = PKG_CONST.TRAN_STATE__DUPLICATE THEN
                    pn_result_cd := PKG_CONST.RESULT__DUPLICATE;
                    pv_error_message := 'Duplicate sale with different line items, original tran_id: ' || ln_original_tran_id;
                END IF;
            END IF;
        ELSE
            NULL; -- just update the transaction  
        END IF;
    END IF;
    
    ln_device_id := PKG_DEVICE_CONFIGURATION.GET_DEVICE_ID_BY_NAME(pv_device_name, ld_tran_server_ts);
    
    IF ln_insert_tran = PKG_CONST.BOOLEAN__TRUE THEN
        IF lc_client_payment_type_cd IS NULL THEN
            IF pc_sale_type_cd = PKG_CONST.SALE_TYPE__CASH THEN
                lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CASH;
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE;
            ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED THEN
                lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CREDIT;
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND;
            ELSIF pn_sale_result_id != 0 /* Success */ AND NVL(pn_sale_amount, 0) = 0 THEN
                lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CREDIT;
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED;
            ELSE
                lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CREDIT;
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__SALE_NO_AUTH;
            END IF;
        END IF;
        SELECT pss.seq_tran_id.NEXTVAL INTO pn_tran_id FROM DUAL;

        IF ln_tran_exists = PKG_CONST.BOOLEAN__TRUE THEN
            lv_global_trans_cd := lv_global_trans_cd || ':' || pn_tran_id;
        END IF;

        IF pc_sale_type_cd = PKG_CONST.SALE_TYPE__CASH THEN
            PKG_POS_PTA.SP_GET_OR_CREATE_POS_PTA(ln_device_id, pv_device_name, lc_client_payment_type_cd, lt_sale_start_utc_ts, pn_result_cd, pv_error_message, ln_pos_pta_id);
        ELSE
            PKG_POS_PTA.SP_GET_OR_CREATE_ERR_POS_PTA(ln_device_id, pv_device_name, lc_client_payment_type_cd, lt_sale_start_utc_ts, pn_result_cd, pv_error_message, ln_pos_pta_id);
        END IF;
        
        SELECT c.MINOR_CURRENCY_FACTOR
        INTO ln_minor_currency_factor
        FROM PSS.POS_PTA PTA
        JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
        WHERE PTA.POS_PTA_ID = ln_pos_pta_id;

        INSERT INTO pss.tran (
            tran_id,
            tran_start_ts,
            tran_end_ts,
            tran_upload_ts,
            tran_state_cd,
            tran_device_tran_cd,
            pos_pta_id,
            tran_global_trans_cd,
            tran_device_result_type_cd,
            payment_subtype_key_id,
            payment_subtype_class,
            client_payment_type_cd,
            device_name
        ) SELECT
            pn_tran_id,
            ld_tran_start_ts,
            ld_tran_start_ts,
            ld_current_ts,
            pv_tran_state_cd,
            pv_device_tran_cd,
            ln_pos_pta_id,
            lv_global_trans_cd,
            pv_tran_device_result_type_cd,
            pp.payment_subtype_key_id,
            ps.payment_subtype_class,
            ps.client_payment_type_cd,
            pv_device_name
        FROM pss.pos_pta pp
        JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
        WHERE pp.pos_pta_id = ln_pos_pta_id;
        IF INSTR(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('MASTER_ID_UPDATE_SALE_TYPES'), pc_sale_type_cd) > 0 AND TO_NUMBER_OR_NULL(pv_device_tran_cd) < DBADMIN.DATE_TO_MILLIS(SYSDATE + 365) / 1000 THEN
            UPDATE DEVICE.DEVICE_SETTING 
               SET DEVICE_SETTING_VALUE = pv_device_tran_cd
             WHERE DEVICE_ID = ln_device_id
               AND DEVICE_SETTING_PARAMETER_CD = '60' -- Master Id
               AND TO_NUMBER_OR_NULL(NVL(DEVICE_SETTING_VALUE, '0')) < TO_NUMBER_OR_NULL(pv_device_tran_cd);
        END IF;
    ELSE -- logic to determine pv_tran_state_cd
        SELECT c.MINOR_CURRENCY_FACTOR
        INTO ln_minor_currency_factor
        FROM PSS.POS_PTA PTA
        JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
        WHERE PTA.POS_PTA_ID = ln_pos_pta_id;
    
        IF lc_calc_tran_state_cd = 'Y' THEN
            IF pv_tran_device_result_type_cd IN (PKG_CONST.TRAN_DEV_RES__CANCELLED,
                    PKG_CONST.TRAN_DEV_RES__AUTH_FAILURE,
                    PKG_CONST.TRAN_DEV_RES__FAILURE,
                    PKG_CONST.TRAN_DEV_RES__TIMEOUT) OR pn_sale_amount <= 0 THEN
                IF pv_tran_state_cd IN (
                        PKG_CONST.TRAN_STATE__AUTH_SUCCESS,
                        PKG_CONST.TRAN_STATE__AUTH_SUCCESS_COND,
                        PKG_CONST.TRAN_STATE__BATCH_INTENDED,
                        PKG_CONST.TRAN_STATE__PROCESSED_TRAN_INT) THEN
                    IF lc_auth_hold_used = 'Y' OR pv_tran_state_cd = PKG_CONST.TRAN_STATE__PROCESSED_TRAN_INT THEN
                        IF pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED THEN
                            pv_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH_INTENDED;
                        ELSE
                            pv_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH;
                        END IF;
                    ELSE
                        pv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED;
                    END IF;
                ELSIF pv_tran_state_cd IN (
                        PKG_CONST.TRAN_STATE__AUTH_EXPIRED,
                        PKG_CONST.TRAN_STATE__AUTH_COND_EXPIRED) THEN
                    pv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED; -- Reversal not available since auth is expired
                ELSIF pv_tran_state_cd IN (
                        PKG_CONST.TRAN_STATE__AUTH_DECLINE,
                        PKG_CONST.TRAN_STATE__AUTH_FAILURE) THEN
                    pv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED; -- We already determined that that no reversal is needed
                ELSIF pv_tran_state_cd IN (
                        PKG_CONST.TRAN_STATE__AUTH_REVERSED,
                        PKG_CONST.TRAN_STATE__AUTH_PEND_RVRSL,
                        PKG_CONST.TRAN_STATE__COMPLETE_ERROR,
                        PKG_CONST.TRAN_STATE__PROCESSED_TRAN,
                        PKG_CONST.TRAN_STATE__PROCESSING_TRAN,
                        PKG_CONST.TRAN_STATE__INCOMPLETE,
                        PKG_CONST.TRAN_STATE__INCOMPLETE_ERROR,
                        PKG_CONST.TRAN_STATE__PROCESSING_STTLMT,
                        PKG_CONST.TRAN_STATE__STLMT_INCOMPLETE,
                        PKG_CONST.TRAN_STATE__STLMT_ERROR) THEN
                   -- don't change it
                   NULL;
                ELSE                
                    pv_error_message := 'Bad tran state for a cancelled cashless sale, tran_id: ' || pn_tran_id || ', tran_state_cd: ' || pv_tran_state_cd;
                    pv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED;
                END IF;
            ELSIF pv_tran_state_cd IN (
                    PKG_CONST.TRAN_STATE__AUTH_EXPIRED,
                    PKG_CONST.TRAN_STATE__AUTH_COND_EXPIRED)
                 OR (pv_tran_state_cd IN (
                    PKG_CONST.TRAN_STATE__AUTH_SUCCESS,
                    PKG_CONST.TRAN_STATE__AUTH_SUCCESS_COND)
                    AND ld_original_tran_start_ts < ld_current_ts - 8) THEN
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE_ERROR;
            ELSIF pv_tran_state_cd IN (
                    PKG_CONST.TRAN_STATE__AUTH_DECLINE,
                    PKG_CONST.TRAN_STATE__AUTH_FAILURE,
                    PKG_CONST.TRAN_STATE__AUTH_REVERSED,
                    PKG_CONST.TRAN_STATE__AUTH_PEND_RVRSL,
                    PKG_CONST.TRAN_STATE__INTENDED_ERROR) THEN
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE_ERROR;
                pv_error_message := 'Received a cashless sale for an unsuccessful auth, tran_id: ' || pn_tran_id || ', tran_state_cd: ' || pv_tran_state_cd;
            ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED
                AND pv_tran_device_result_type_cd IN (
                    PKG_CONST.TRAN_DEV_RES__INCOMPLETE,
                    PKG_CONST.TRAN_DEV_RES__SUCCESS,
                    PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_PRNTR,
                    PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_RCPT,
                    PKG_CONST.TRAN_DEV_RES__SUCCESS_RCPT_ERR) THEN
                IF pv_tran_state_cd IN (PKG_CONST.TRAN_STATE__AUTH_SUCCESS, PKG_CONST.TRAN_STATE__AUTH_SUCCESS_COND) THEN
                    -- normal case
                    pv_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH_INTENDED;
                    -- insert sale record
                ELSE
                    -- sale actual uploaded
                    -- don't change tran_state_cd
                    -- don't update sale record
                    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
                    pv_error_message := 'Actual uploaded before intended';
                    RETURN;
                END IF;
            -- we must let POSM processed cancelled sales too to do auth reversal
            ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__ACTUAL
                AND pv_tran_state_cd IN (
                    PKG_CONST.TRAN_STATE__AUTH_SUCCESS,
                    PKG_CONST.TRAN_STATE__AUTH_SUCCESS_COND,
                    PKG_CONST.TRAN_STATE__PROCESSING_BATCH,
                    PKG_CONST.TRAN_STATE__PRCSNG_BATCH_INTD,
                    PKG_CONST.TRAN_STATE__PRCSNG_BATCH_LOCAL,
                    PKG_CONST.TRAN_STATE__PROCESSED_TRAN_INT,
                    PKG_CONST.TRAN_STATE__BATCH_INTENDED)
                AND pv_tran_device_result_type_cd IN (
                    PKG_CONST.TRAN_DEV_RES__AUTH_FAILURE,
                    PKG_CONST.TRAN_DEV_RES__CANCELLED,
                    PKG_CONST.TRAN_DEV_RES__FAILURE,
                    PKG_CONST.TRAN_DEV_RES__INCOMPLETE,
                    PKG_CONST.TRAN_DEV_RES__SUCCESS,
                    PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_PRNTR,
                    PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_RCPT,
                    PKG_CONST.TRAN_DEV_RES__SUCCESS_RCPT_ERR,
                    PKG_CONST.TRAN_DEV_RES__TIMEOUT) THEN
                IF pv_tran_device_result_type_cd IN (
                    PKG_CONST.TRAN_DEV_RES__SUCCESS,
                    PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_PRNTR,
                    PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_RCPT,
                    PKG_CONST.TRAN_DEV_RES__SUCCESS_RCPT_ERR) THEN

                    SELECT NVL(MAX(auth_amt_approved), 0)
                    INTO ln_auth_amt_approved
                    FROM PSS.AUTH
                    WHERE TRAN_ID = pn_tran_id AND auth_type_cd = 'N' AND auth_state_id IN (2, 5);
                    
                    pv_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH;
                    IF pn_sale_amount > 0 AND ln_auth_amt_approved > 0 AND pn_sale_amount / ln_minor_currency_factor > ln_auth_amt_approved THEN
                        SELECT GREATEST(ln_auth_amt_approved, NVL(NVL(MAX(DBADMIN.TO_NUMBER_OR_NULL(DS_MAA.DEVICE_SETTING_VALUE) / 100), MAX(DBADMIN.TO_NUMBER_OR_NULL(DS_AUTH_AMT.DEVICE_SETTING_VALUE) / DECODE(D.DEVICE_TYPE_ID, 13, 100, 1))), 0))
                        INTO ln_auth_amt_allowed
                        FROM DEVICE.DEVICE D
                        LEFT OUTER JOIN DEVICE.DEVICE_SETTING DS_MAA ON D.DEVICE_ID = DS_MAA.DEVICE_ID AND DS_MAA.DEVICE_SETTING_PARAMETER_CD = 'MAX_AUTH_AMOUNT'
                        LEFT OUTER JOIN DEVICE.DEVICE_SETTING DS_AUTH_AMT ON D.DEVICE_ID = DS_AUTH_AMT.DEVICE_ID
                            AND DS_AUTH_AMT.DEVICE_SETTING_PARAMETER_CD = DECODE(D.DEVICE_TYPE_ID, 13, '1200', 1, '195', 11, 'AUTHORIZATION_AMOUNT', 0, '195')
                        WHERE D.DEVICE_ID = ln_device_id;
                    
                        ln_sale_over_auth_amt_percent := NVL(DBADMIN.TO_NUMBER_OR_NULL(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('ALLOWED_SALE_AMT_OVER_AUTH_AMT_PERCENT')), 100);
                        IF pn_sale_amount / ln_minor_currency_factor > ln_auth_amt_allowed + ln_auth_amt_allowed * ln_sale_over_auth_amt_percent / 100 THEN
                            pv_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE_ERROR;
                            lv_error := 'Error: Sale amount ' || TO_CHAR(pn_sale_amount / ln_minor_currency_factor, 'FM9,999,999,990.00') || ' exceeds allowed auth amount ' || TO_CHAR(ln_auth_amt_allowed, 'FM9,999,999,990.00') || ' by more than ' || ln_sale_over_auth_amt_percent || '%';
                            UPDATE PSS.TRAN
                            SET TRAN_INFO = SUBSTR(TRAN_INFO || DECODE(TRAN_INFO, NULL, '', ', ') || lv_error, 1, 1000)
                            WHERE TRAN_ID = pn_tran_id AND (TRAN_INFO IS NULL OR INSTR(TRAN_INFO, lv_error) = 0);
                            
                            UPDATE DEVICE.DEVICE
                            SET DEVICE_ACTIVE_YN_FLAG = 'N'
                            WHERE DEVICE_NAME = pv_device_name AND DEVICE_ACTIVE_YN_FLAG = 'Y';
                            
                            lv_email_from_address := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EMAIL_FROM_ADDRESS');
                            lv_email_to_address := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EMAIL_TO_ADDRESS_CUSTOMER_SERVICE');
                            INSERT INTO ENGINE.OB_EMAIL_QUEUE(OB_EMAIL_FROM_EMAIL_ADDR, OB_EMAIL_FROM_NAME, OB_EMAIL_TO_EMAIL_ADDR, OB_EMAIL_TO_NAME, OB_EMAIL_SUBJECT, OB_EMAIL_MSG)
                            VALUES(lv_email_from_address, lv_email_from_address, lv_email_to_address, lv_email_to_address, 'Invalid sale amount', lv_error || ', device: ' || pv_device_name || ', transaction ID: ' || pn_tran_id || '. Device has been disabled.');
                        END IF;
                    END IF;
                ELSE
                    pv_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH;
                END IF;
            ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__ACTUAL
                AND pv_tran_state_cd = PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND THEN
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__SALE_NO_AUTH;
            ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__ACTUAL
                AND pv_tran_state_cd = PKG_CONST.TRAN_STATE__PROCESSING_TRAN THEN
                NULL;-- don't change tran_state_cd
            ELSIF pv_tran_state_cd != PKG_CONST.TRAN_STATE__SALE_NO_AUTH THEN
                 pv_error_message := 'Unusual tran state for a cashless sale, tran_id: ' || pn_tran_id || ', tran_state_cd: ' || pv_tran_state_cd;
            END IF;
        END IF;
        
        UPDATE pss.tran
        SET tran_state_cd = DECODE(TRAN_STATE_CD, lv_orig_tran_state_cd, pv_tran_state_cd, TRAN_STATE_CD), -- it might have changed if POSMLayer is processing it
            tran_end_ts = tran_start_ts,
            tran_upload_ts = ld_current_ts,
            tran_device_result_type_cd = pv_tran_device_result_type_cd
        WHERE tran_id = pn_tran_id
        RETURNING client_payment_type_cd INTO lc_client_payment_type_cd;

        SELECT /*+ INDEX(tli IF1_TRAN_LINE_ITEM) */ COUNT(1)
        INTO ln_count
        FROM pss.tran_line_item tli
        WHERE tran_id = pn_tran_id
            AND tran_line_item_batch_type_cd = pc_tran_batch_type_cd;
        
        IF ln_count > 0 THEN
            DELETE /*+ INDEX(tli IF1_TRAN_LINE_ITEM) */ FROM pss.tran_line_item tli
            WHERE tran_id = pn_tran_id
                AND tran_line_item_batch_type_cd = pc_tran_batch_type_cd;
        END IF;
    END IF;    

    UPDATE pss.sale
    SET device_batch_id = pn_device_batch_id,
        sale_type_cd = pc_sale_type_cd,
        sale_start_utc_ts = lt_sale_start_utc_ts,
        sale_end_utc_ts = lt_sale_start_utc_ts,
        sale_utc_offset_min = pn_sale_utc_offset_min,
        sale_result_id = pn_sale_result_id,
        sale_amount = pn_sale_amount / ln_minor_currency_factor,
        receipt_result_cd = pc_receipt_result_cd,
        hash_type_cd = pv_hash_type_cd,
        tran_line_item_hash = pv_tran_line_item_hash,
        sale_global_session_cd = pv_global_session_cd,
        imported = CASE WHEN pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED OR pv_tran_state_cd IN('F', 'G', 'Z') THEN '?' WHEN pn_sale_result_id = 0 THEN 'N' ELSE '-' END,
        VOID_ALLOWED = NVL(pc_void_allowed, 'N')
    WHERE tran_id = pn_tran_id;

    IF SQL%NOTFOUND THEN
        INSERT INTO pss.sale (
            tran_id,
            device_batch_id,
            sale_type_cd,
            sale_start_utc_ts,
            sale_end_utc_ts,
            sale_utc_offset_min,
            sale_result_id,
            sale_amount,
            receipt_result_cd,
            hash_type_cd,
            tran_line_item_hash,
            sale_global_session_cd,
            imported,
            VOID_ALLOWED
        ) VALUES (
            pn_tran_id,
            pn_device_batch_id,
            pc_sale_type_cd,
            lt_sale_start_utc_ts,
            lt_sale_start_utc_ts,
            pn_sale_utc_offset_min,
            pn_sale_result_id,
            pn_sale_amount / ln_minor_currency_factor,
            pc_receipt_result_cd,
            pv_hash_type_cd,
            pv_tran_line_item_hash,
            pv_global_session_cd,
            CASE WHEN pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED OR pv_tran_state_cd IN('F', 'G', 'Z') THEN '?' WHEN pn_sale_result_id = 0 THEN 'N' ELSE '-' END,
            NVL(pc_void_allowed, 'N')
        );
        
        IF lc_client_payment_type_cd IN (PKG_CONST.CLNT_PMNT_TYPE__ISIS_CREDIT, PKG_CONST.CLNT_PMNT_TYPE__ISIS_SPECIAL)
            AND ln_consumer_acct_id > 0 AND pc_sale_type_cd = PKG_CONST.SALE_TYPE__ACTUAL THEN
            PROCESS_ISIS_TRAN(pn_tran_id);
        END IF;
    END IF;
    
    IF pn_sale_result_id != 0 AND ln_consumer_acct_id IS NOT NULL AND lc_auth_hold_used = 'N' THEN
        UPDATE pss.last_device_action
        SET device_action_utc_ts = device_action_utc_ts - INTERVAL '1' YEAR
        WHERE device_name = pv_device_name
            AND consumer_acct_id = ln_consumer_acct_id;
    END IF;
END;

    PROCEDURE ADD_REPLENISH_BONUSES(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pn_apply_to_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
        pn_amount PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
        pd_auth_ts PSS.AUTH.AUTH_TS%TYPE)
    IS
        ln_used_replenish_balance PSS.CONSUMER_ACCT.TOWARD_REPLENISH_BONUS_BALANCE%TYPE;
        ln_bonus_amount PSS.SALE.SALE_AMOUNT%TYPE;
        ln_bonus_percent REPORT.CAMPAIGN.DISCOUNT_PERCENT%TYPE;
        ln_bonus_threshhold REPORT.CAMPAIGN.THRESHOLD_AMOUNT%TYPE;
        ln_bonus_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE;
    BEGIN
        SELECT MAX(DISCOUNT_PERCENT), MAX(THRESHOLD_AMOUNT), MAX(CAMPAIGN_ID)
          INTO ln_bonus_percent, ln_bonus_threshhold, ln_bonus_campaign_id
          FROM (SELECT C.DISCOUNT_PERCENT, C.THRESHOLD_AMOUNT, C.CAMPAIGN_ID
          FROM REPORT.CAMPAIGN C
          JOIN PSS.CAMPAIGN_CONSUMER_ACCT CCA on C.CAMPAIGN_ID = CCA.CAMPAIGN_ID
         WHERE CCA.CONSUMER_ACCT_ID = pn_apply_to_consumer_acct_id
           AND pd_auth_ts BETWEEN NVL(C.START_DATE, MIN_DATE) AND NVL(C.END_DATE, MAX_DATE)
           AND C.CAMPAIGN_TYPE_ID = 2 /* Replenish reward - Bonus */
           AND C.DISCOUNT_PERCENT > 0
           AND C.DISCOUNT_PERCENT < 1
           AND (TRIM(C.RECUR_SCHEDULE) IS NULL OR REPORT.MATCH_CAMPAIGN_RECUR_SCHEDULE(C.RECUR_SCHEDULE, pd_auth_ts) = 'Y')
         ORDER BY C.DISCOUNT_PERCENT DESC, C.CAMPAIGN_ID DESC)
         WHERE ROWNUM = 1;
        IF ln_bonus_campaign_id IS NOT NULL AND ln_bonus_percent > 0 THEN
            DECLARE
                ln_bonus_balance PSS.CONSUMER_ACCT.TOWARD_CASH_BACK_BALANCE%TYPE;
                lv_currency_cd PSS.CONSUMER_ACCT.CURRENCY_CD%TYPE;
                ln_corp_customer_id PSS.CONSUMER_ACCT.CORP_CUSTOMER_ID%TYPE;
                lv_consumer_acct_cd PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD%TYPE;
                lv_consumer_acct_identifier PSS.CONSUMER_ACCT.CONSUMER_ACCT_IDENTIFIER%TYPE;
                lv_bonus_device_name DEVICE.DEVICE_NAME%TYPE;
                ln_bonus_next_master_id NUMBER;
                ln_result_cd NUMBER;
                lv_error_message VARCHAR2(4000);
                ln_bonus_tran_id PSS.TRAN.TRAN_ID%TYPE;
                lv_bonus_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
                ln_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE;
                lc_currency_symbol CORP.CURRENCY.CURRENCY_SYMBOL%TYPE;
                ln_host_id HOST.HOST_ID%TYPE;
                ld_bonus_ts DATE;
                ld_bonus_time NUMBER;
                lv_desc PSS.TRAN_LINE_ITEM.TRAN_LINE_ITEM_DESC%TYPE;
                ln_doc_id CORP.DOC.DOC_ID%TYPE;
                ln_ledger_id CORP.LEDGER.LEDGER_ID%TYPE; 
                ln_consumer_acct_sub_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_SUB_TYPE_ID%TYPE;
            BEGIN
                UPDATE PSS.CONSUMER_ACCT
                   SET TOWARD_REPLENISH_BONUS_BALANCE = NVL(TOWARD_REPLENISH_BONUS_BALANCE, 0) + pn_amount
                 WHERE CONSUMER_ACCT_ID = pn_apply_to_consumer_acct_id
                 RETURNING TOWARD_REPLENISH_BONUS_BALANCE, CURRENCY_CD, CORP_CUSTOMER_ID, CONSUMER_ACCT_CD, CONSUMER_ACCT_IDENTIFIER, CONSUMER_ACCT_SUB_TYPE_ID
                  INTO ln_bonus_balance, lv_currency_cd, ln_corp_customer_id, lv_consumer_acct_cd, lv_consumer_acct_identifier, ln_consumer_acct_sub_type_id;
                IF ln_bonus_balance >= ln_bonus_threshhold THEN 
                    ln_used_replenish_balance := TRUNC(ln_bonus_balance / ln_bonus_threshhold) * ln_bonus_threshhold;
                    ln_bonus_amount := ROUND(ln_used_replenish_balance * ln_bonus_percent, 2);
                    UPDATE PSS.CONSUMER_ACCT
                       SET TOWARD_REPLENISH_BONUS_BALANCE = TOWARD_REPLENISH_BONUS_BALANCE - ln_used_replenish_balance,
                           REPLENISH_BONUS_TOTAL = NVL(REPLENISH_BONUS_TOTAL, 0) + ln_bonus_amount,
                           CONSUMER_ACCT_BALANCE = CONSUMER_ACCT_BALANCE + ln_bonus_amount,
                           CONSUMER_ACCT_PROMO_BALANCE = CONSUMER_ACCT_PROMO_BALANCE + ln_bonus_amount,
                           CONSUMER_ACCT_PROMO_TOTAL = CONSUMER_ACCT_PROMO_TOTAL + ln_bonus_amount
                     WHERE CONSUMER_ACCT_ID = pn_apply_to_consumer_acct_id;                         
                    -- add trans to virtual terminal
                    SELECT C.MINOR_CURRENCY_FACTOR, NVL(CC.CURRENCY_SYMBOL, '$'), DBADMIN.TIMESTAMP_TO_MILLIS(SYS_EXTRACT_UTC(SYSTIMESTAMP)), SYSDATE
                      INTO ln_minor_currency_factor, lc_currency_symbol, ld_bonus_time, ld_bonus_ts
                      FROM PSS.CURRENCY C
                      LEFT OUTER JOIN CORP.CURRENCY CC ON C.CURRENCY_CD = CC.CURRENCY_CODE
                     WHERE C.CURRENCY_CD = lv_currency_cd;
                     
                    PKG_DEVICE_CONFIGURATION.NEXT_MASTER_ID_BY_SERIAL('V1-' || CASE WHEN ln_consumer_acct_sub_type_id = 2 THEN ln_corp_customer_id ELSE 1 END || '-' || lv_currency_cd, lv_bonus_device_name, ln_bonus_next_master_id);
                    SP_CREATE_SALE('A', lv_bonus_device_name, ln_bonus_next_master_id,  0, 'C', ld_bonus_time, 
                        DBADMIN.PKG_UTL.TIME_ZONE_DIFF_MIN(PKG_CONST.DB_TIME_ZONE, PKG_CONST.GMT_TIME_ZONE), 'S', 0, ln_bonus_amount * ln_minor_currency_factor, 'U', 'A', 'SHA1', 
                        DBADMIN.HASH_CARD('Replenish Bonus on ' || pn_apply_to_consumer_acct_id || ' of ' || ln_bonus_amount),
                        NULL, ln_result_cd, lv_error_message, ln_bonus_tran_id, lv_bonus_tran_state_cd);
                    IF ln_result_cd != PKG_CONST.RESULT__SUCCESS THEN
                        RAISE_APPLICATION_ERROR(-20118, 'Could not create replenish bonus transaction: ' || lv_error_message);
                    END IF;
                    SELECT HOST_ID, 'Replenish Bonus for ' || lc_currency_symbol || TO_NUMBER(ln_used_replenish_balance, 'FM9,999,999.00') || ' in replenishments'
                      INTO ln_host_id, lv_desc
                      FROM (SELECT H.HOST_ID
                              FROM DEVICE.HOST H
                              JOIN DEVICE.DEVICE D ON H.DEVICE_ID = D.DEVICE_ID
                             WHERE D.DEVICE_ACTIVE_YN_FLAG = 'Y'
                               AND H.HOST_PORT_NUM IN(0,1)
                               AND D.DEVICE_NAME = lv_bonus_device_name
                               AND H.HOST_ACTIVE_YN_FLAG = 'Y'
                             ORDER BY H.HOST_PORT_NUM DESC, H.HOST_ID DESC) 
                     WHERE ROWNUM = 1;
                    INSERT INTO PSS.TRAN_LINE_ITEM(
                        TRAN_ID,
                        TRAN_LINE_ITEM_AMOUNT,
                        TRAN_LINE_ITEM_POSITION_CD,
                        TRAN_LINE_ITEM_TAX,
                        TRAN_LINE_ITEM_TYPE_ID,
                        TRAN_LINE_ITEM_QUANTITY,
                        TRAN_LINE_ITEM_DESC,
                        HOST_ID,
                        TRAN_LINE_ITEM_BATCH_TYPE_CD,
                        TRAN_LINE_ITEM_TS,
                        SALE_RESULT_ID,
                        APPLY_TO_CONSUMER_ACCT_ID,
                        CAMPAIGN_ID)
                    SELECT
                        ln_bonus_tran_id,
                        ln_used_replenish_balance,
                        NULL,
                        NULL,
                        555,
                        ln_bonus_percent,
                        lv_desc,
                        ln_host_id,
                        'A',
                        ld_bonus_ts,
                        0,
                        pn_apply_to_consumer_acct_id,
                        ln_bonus_campaign_id
                    FROM DUAL;
                    UPDATE PSS.TRAN
                       SET PARENT_TRAN_ID = pn_tran_id
                     WHERE TRAN_ID = ln_bonus_tran_id;
                    IF ln_consumer_acct_sub_type_id = 1 THEN
                        -- add adjustment to ledger
                        CORP.PAYMENTS_PKG.ADJUSTMENT_INS(ln_corp_customer_id, 'Replenish Bonus Processing', lv_currency_cd, lv_desc || ', card # ' || lv_consumer_acct_cd || ', card ID ' || lv_consumer_acct_identifier,
                            -ln_bonus_amount, ln_doc_id, ln_ledger_id);
                    END IF;
                END IF; 
            END;
        END IF;  
    END;
    
    PROCEDURE FINISH_REPLENISH_SETUP(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pn_apply_to_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
        pv_card_key PSS.AUTH.CARD_KEY%TYPE,
        pd_auth_ts PSS.AUTH.AUTH_TS%TYPE,
        pv_masked_card_number PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_CARD_MASKED%TYPE,
        pn_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE)
    IS
        ln_replenish_id PSS.CONSUMER_ACCT_REPLENISH.CONSUMER_ACCT_REPLENISH_ID%TYPE;
        ln_capr_row_id ROWID;   
    BEGIN
        SELECT MAX(CAR.CONSUMER_ACCT_REPLENISH_ID), MAX(CAPR.ROWID)
          INTO ln_replenish_id, ln_capr_row_id
          FROM PSS.CONSUMER_ACCT_REPLENISH CAR 
          JOIN PSS.CONSUMER_ACCT_PEND_REPLENISH CAPR ON CAR.CONSUMER_ACCT_REPLENISH_ID = CAPR.CONSUMER_ACCT_REPLENISH_ID
          JOIN PSS.TRAN X ON CAPR.DEVICE_NAME = X.DEVICE_NAME AND CAPR.DEVICE_TRAN_CD = X.TRAN_DEVICE_TRAN_CD
         WHERE X.TRAN_ID = pn_tran_id
           AND CAR.CONSUMER_ACCT_ID = pn_apply_to_consumer_acct_id;
        IF ln_replenish_id IS NOT NULL THEN
            UPDATE PSS.CONSUMER_ACCT_REPLENISH CAR
               SET REPLENISH_CARD_KEY = CASE WHEN pv_card_key IS NOT NULL AND pd_auth_ts > NVL(LAST_REPLENISH_TRAN_TS, MIN_DATE) THEN pv_card_key ELSE REPLENISH_CARD_KEY END,
                   REPLENISH_CARD_MASKED = CASE WHEN pv_card_key IS NOT NULL AND pd_auth_ts > NVL(LAST_REPLENISH_TRAN_TS, MIN_DATE) THEN DBADMIN.MASK_CREDIT_CARD(pv_masked_card_number) ELSE REPLENISH_CARD_MASKED END,                   
                   REPLENISH_POS_PTA_ID = CASE WHEN pv_card_key IS NOT NULL AND pd_auth_ts > NVL(LAST_REPLENISH_TRAN_TS, MIN_DATE) THEN pn_pos_pta_id ELSE REPLENISH_POS_PTA_ID END,
                   LAST_REPLENISH_TRAN_TS = CASE WHEN pv_card_key IS NOT NULL AND pd_auth_ts > NVL(LAST_REPLENISH_TRAN_TS, MIN_DATE) THEN pd_auth_ts ELSE LAST_REPLENISH_TRAN_TS END,
                   LAST_REPLENISH_TRAN_ID = CASE WHEN pv_card_key IS NOT NULL AND pd_auth_ts > NVL(LAST_REPLENISH_TRAN_TS, MIN_DATE) THEN pn_tran_id ELSE LAST_REPLENISH_TRAN_ID END
             WHERE CONSUMER_ACCT_ID = pn_apply_to_consumer_acct_id
               AND CONSUMER_ACCT_REPLENISH_ID = ln_replenish_id;
            DELETE 
              FROM PSS.CONSUMER_ACCT_PEND_REPLENISH 
             WHERE ROWID = ln_capr_row_id
               AND CONSUMER_ACCT_REPLENISH_ID = ln_replenish_id;
        END IF;
    END;

    PROCEDURE REPLENISH_CONSUMER_ACCT(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pv_tli_desc PSS.TRAN_LINE_ITEM.TRAN_LINE_ITEM_DESC%TYPE,
        pn_amount PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
        pn_tli_type_id PSS.TRAN_LINE_ITEM.TRAN_LINE_ITEM_TYPE_ID%TYPE,
        pv_card_key PSS.AUTH.CARD_KEY%TYPE,
        pd_auth_ts PSS.AUTH.AUTH_TS%TYPE,
        pv_masked_card_number PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_CARD_MASKED%TYPE,
        pn_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE,
        pn_apply_to_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE)
    IS
        ln_consumer_acct_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_TYPE_ID%TYPE;
    BEGIN
        UPDATE PSS.CONSUMER_ACCT
           SET CONSUMER_ACCT_BALANCE = CONSUMER_ACCT_BALANCE + pn_amount,
               CONSUMER_ACCT_REPLEN_BALANCE = CONSUMER_ACCT_REPLEN_BALANCE + pn_amount,
               CONSUMER_ACCT_REPLENISH_TOTAL = CONSUMER_ACCT_REPLENISH_TOTAL + pn_amount
         WHERE CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y'
           AND CONSUMER_ACCT_TYPE_ID = 3
           AND CONSUMER_ACCT_CD_HASH = HEXTORAW(pv_tli_desc)
          RETURNING CONSUMER_ACCT_ID, CONSUMER_ACCT_TYPE_ID 
          INTO pn_apply_to_consumer_acct_id, ln_consumer_acct_type_id;
        IF ln_consumer_acct_type_id IN(3) THEN          
            ADD_REPLENISH_BONUSES(
                pn_tran_id,
                pn_apply_to_consumer_acct_id,
                pn_amount,
                pd_auth_ts);
        END IF;
        FINISH_REPLENISH_SETUP(
            pn_tran_id,
            pn_apply_to_consumer_acct_id,
            pv_card_key,
            pd_auth_ts,
            pv_masked_card_number,
            pn_pos_pta_id);
    END;
    
PROCEDURE sp_create_tran_line_item
(
    pn_tran_id IN pss.tran_line_item.tran_id%TYPE,
    pn_host_port_num IN host.host_port_num%TYPE,
    pn_tli_type_id IN pss.tran_line_item.tran_line_item_type_id%TYPE,
    pn_tli_quantity IN pss.tran_line_item.tran_line_item_quantity%TYPE,
    pn_tli_amount IN NUMBER,
    pn_tli_tax IN NUMBER,
    pv_tli_desc IN pss.tran_line_item.tran_line_item_desc%TYPE,
    pn_tli_utc_ts_ms IN NUMBER,
    pn_tli_utc_offset_min IN NUMBER,
    pv_tli_position_cd IN pss.tran_line_item.tran_line_item_position_cd%TYPE,
    pc_tran_batch_type_cd IN pss.tran_line_item.tran_line_item_batch_type_cd%TYPE,
    pn_sale_result_id pss.tran_line_item.sale_result_id%TYPE DEFAULT NULL,
    pn_host_position_num host.host_position_num%TYPE DEFAULT 0,
    pn_sale_amount pss.sale.sale_amount%TYPE DEFAULT 0
)
IS
    ln_host_id pss.tran_line_item.host_id%TYPE;
    ln_device_id host.device_id%TYPE;
    ln_new_host_count NUMBER;
    ln_result_cd NUMBER;
    lv_error_message VARCHAR2(255);
    ln_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE;
    ln_tran_line_item_id pss.tran_line_item.tran_line_item_id%TYPE;
    ln_tli_desc pss.tran_line_item.tran_line_item_desc%TYPE;
    ln_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE;
    lc_tli_type_group_cd PSS.TRAN_LINE_ITEM_TYPE.TRAN_LINE_ITEM_TYPE_GROUP_CD%TYPE;
    lv_device_name DEVICE.DEVICE_NAME%TYPE;
    ln_convenience_fee_amount PSS.SALE.SALE_AMOUNT%TYPE;
    lv_email_from_address engine.app_setting.app_setting_value%TYPE;
    lv_email_to_address engine.app_setting.app_setting_value%TYPE;
    lv_error pss.tran.tran_info%TYPE;
    ln_tli_type_id PSS.TRAN_LINE_ITEM.TRAN_LINE_ITEM_TYPE_ID%TYPE := pn_tli_type_id;
    ln_apply_to_consumer_acct_id PSS.TRAN_LINE_ITEM.APPLY_TO_CONSUMER_ACCT_ID%TYPE;
    lv_card_key PSS.AUTH.CARD_KEY%TYPE;
    ld_auth_ts PSS.AUTH.AUTH_TS%TYPE;
    lv_masked_card_number PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_CARD_MASKED%TYPE;
    ln_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE;
BEGIN
    SELECT POS.DEVICE_ID, c.MINOR_CURRENCY_FACTOR, D.DEVICE_TYPE_ID, D.DEVICE_NAME
      INTO ln_device_id, ln_minor_currency_factor, ln_device_type_id, lv_device_name
      FROM PSS.POS POS
      JOIN DEVICE.DEVICE D ON POS.DEVICE_ID = D.DEVICE_ID
      JOIN PSS.POS_PTA PTA ON POS.POS_ID = PTA.POS_ID
      JOIN PSS.TRAN X ON PTA.POS_PTA_ID = X.POS_PTA_ID
      JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
    WHERE X.TRAN_ID = pn_tran_id;

    ln_host_id := sf_find_host_id(ln_device_id, pn_host_port_num, pn_host_position_num);
    IF ln_host_id IS NULL THEN
        -- create default hosts
        pkg_device_configuration.sp_create_default_hosts(ln_device_id, ln_new_host_count, ln_result_cd, lv_error_message);
        IF ln_result_cd != PKG_CONST.RESULT__SUCCESS THEN
            RETURN;
        END IF;
        ln_host_id := sf_find_host_id(ln_device_id, pn_host_port_num, pn_host_position_num);
    END IF;

    SELECT PSS.SEQ_TLI_ID.NEXTVAL
        INTO ln_tran_line_item_id
        FROM DUAL;

    -- For Kiosk type, use tran_line_item_type to find description
    IF ln_device_type_id = 11 THEN
        SELECT tran_line_item_type_desc || ' ' || SUBSTR(TRIM(pv_tli_desc), 1, 3999 - LENGTH(tran_line_item_type_desc))
          INTO ln_tli_desc
          FROM pss.tran_line_item_type
         WHERE tran_line_item_type_id = ln_tli_type_id;
    ELSIF ln_device_type_id = 5 THEN -- eSuds
        SELECT TLIT.TRAN_LINE_ITEM_TYPE_DESC || ', ' || CASE WHEN DTHT.DEVICE_TYPE_HOST_TYPE_CD IN('S', 'U', 'G', 'H', 'I', 'J') THEN DECODE(H.HOST_POSITION_NUM, 0, 'Bottom ', 1, 'Top ') END
                || GT.HOST_GROUP_TYPE_NAME || ' ' || H.HOST_LABEL_CD
          INTO ln_tli_desc
          FROM PSS.TRAN_LINE_ITEM_TYPE tlit
         CROSS JOIN DEVICE.HOST H
          JOIN DEVICE.DEVICE_TYPE_HOST_TYPE dtht ON DTHT.HOST_TYPE_ID = H.HOST_TYPE_ID AND DTHT.DEVICE_TYPE_ID = 5
          LEFT OUTER JOIN (DEVICE.HOST_TYPE_HOST_GROUP_TYPE HTGT
          JOIN DEVICE.HOST_GROUP_TYPE GT ON HTGT.HOST_GROUP_TYPE_ID = GT.HOST_GROUP_TYPE_ID)
            ON H.HOST_TYPE_ID = HTGT.HOST_TYPE_ID
         WHERE tlit.TRAN_LINE_ITEM_TYPE_ID = ln_tli_type_id
           AND H.HOST_ID = ln_host_id;
    ELSE
        ln_tli_desc := pv_tli_desc;
    END IF;

    INSERT INTO PSS.TRAN_LINE_ITEM (
        TRAN_LINE_ITEM_ID,
        TRAN_ID,
        TRAN_LINE_ITEM_AMOUNT,
        TRAN_LINE_ITEM_POSITION_CD,
        TRAN_LINE_ITEM_TAX,
        TRAN_LINE_ITEM_TYPE_ID,
        TRAN_LINE_ITEM_QUANTITY,
        TRAN_LINE_ITEM_DESC,
        HOST_ID,
        TRAN_LINE_ITEM_BATCH_TYPE_CD,
        TRAN_LINE_ITEM_TS,
        SALE_RESULT_ID,
        APPLY_TO_CONSUMER_ACCT_ID)
    SELECT
        ln_tran_line_item_id,
        pn_tran_id,
        pn_tli_amount * CASE tran_line_item_type_sign_pn
            WHEN 'N' THEN -1
            ELSE 1
        END / ln_minor_currency_factor,
        pv_tli_position_cd,
        pn_tli_tax * CASE tran_line_item_type_sign_pn
            WHEN 'N' THEN -1
            ELSE 1
        END / ln_minor_currency_factor,
        ln_tli_type_id,
        pn_tli_quantity,
        ln_tli_desc,
        ln_host_id,
        pc_tran_batch_type_cd,
        CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_tli_utc_ts_ms + pn_tli_utc_offset_min * 60 * 1000) AS DATE),
        pn_sale_result_id,
        ln_apply_to_consumer_acct_id
    FROM tran_line_item_type
    WHERE tran_line_item_type_id = ln_tli_type_id;

    -- For all device actual batch type only
    IF ln_host_id IS NOT NULL AND pc_tran_batch_type_cd = PKG_CONST.TRAN_BATCH_TYPE__ACTUAL THEN
        SELECT tran_line_item_type_group_cd
        INTO lc_tli_type_group_cd
        FROM pss.tran_line_item_type
        WHERE tran_line_item_type_id = ln_tli_type_id;
    
        IF lc_tli_type_group_cd IN ('P', 'S') THEN
            UPDATE PSS.TRAN_LINE_ITEM_RECENT
            SET tran_line_item_id = ln_tran_line_item_id,
                fkp_tran_id = pn_tran_id
            WHERE host_id = ln_host_id
                AND tran_line_item_type_id = ln_tli_type_id;
                
            IF SQL%NOTFOUND THEN
                BEGIN
                    INSERT INTO PSS.TRAN_LINE_ITEM_RECENT (
                        TRAN_LINE_ITEM_ID,
                        HOST_ID,
                        FKP_TRAN_ID,
                        TRAN_LINE_ITEM_TYPE_ID
                    ) VALUES (
                        ln_tran_line_item_id,
                        ln_host_id,
                        pn_tran_id,
                        ln_tli_type_id
                    );
                EXCEPTION
                    WHEN DUP_VAL_ON_INDEX THEN
                        UPDATE PSS.TRAN_LINE_ITEM_RECENT
                        SET tran_line_item_id = ln_tran_line_item_id,
                            fkp_tran_id = pn_tran_id
                        WHERE host_id = ln_host_id
                            AND tran_line_item_type_id = ln_tli_type_id;
                END;
            END IF;
        END IF;
    END IF;
    
    IF ln_tli_type_id = PKG_CONST.TLI__CONVENIENCE_FEE AND pc_tran_batch_type_cd = PKG_CONST.TRAN_BATCH_TYPE__ACTUAL AND pn_sale_amount > 0 THEN
        ln_convenience_fee_amount := (NVL(pn_tli_amount, 0) + NVL(pn_tli_tax, 0)) * NVL(pn_tli_quantity, 0);
        IF ln_convenience_fee_amount > pn_sale_amount - ln_convenience_fee_amount 
            AND ln_convenience_fee_amount > NVL(DBADMIN.TO_NUMBER_OR_NULL(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('MIN_INVALID_CONVENIENCE_FEE_AMT_PENNIES')), 100) THEN
            UPDATE PSS.TRAN
            SET tran_state_cd = PKG_CONST.TRAN_STATE__COMPLETE_ERROR
            WHERE TRAN_ID = pn_tran_id;

            lv_error := 'Error: Two-Tier Pricing amount ' || TO_CHAR(ln_convenience_fee_amount / ln_minor_currency_factor, 'FM9,999,999,990.00') || ' exceeds sale amount without Two-Tier Pricing ' || TO_CHAR((pn_sale_amount - ln_convenience_fee_amount) / ln_minor_currency_factor, 'FM9,999,999,990.00');
            UPDATE PSS.TRAN
            SET TRAN_INFO = SUBSTR(TRAN_INFO || DECODE(TRAN_INFO, NULL, '', ', ') || lv_error, 1, 1000)
            WHERE TRAN_ID = pn_tran_id AND (TRAN_INFO IS NULL OR INSTR(TRAN_INFO, lv_error) = 0);
            
            UPDATE DEVICE.DEVICE
            SET DEVICE_ACTIVE_YN_FLAG = 'N'
            WHERE DEVICE_NAME = lv_device_name AND DEVICE_ACTIVE_YN_FLAG = 'Y';
            
            lv_email_from_address := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EMAIL_FROM_ADDRESS');
            lv_email_to_address := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EMAIL_TO_ADDRESS_CUSTOMER_SERVICE');
            INSERT INTO ENGINE.OB_EMAIL_QUEUE(OB_EMAIL_FROM_EMAIL_ADDR, OB_EMAIL_FROM_NAME, OB_EMAIL_TO_EMAIL_ADDR, OB_EMAIL_TO_NAME, OB_EMAIL_SUBJECT, OB_EMAIL_MSG)
            VALUES(lv_email_from_address, lv_email_from_address, lv_email_to_address, lv_email_to_address, 'Invalid Two-Tier Pricing amount', lv_error || ', device: ' || lv_device_name || ', transaction ID: ' || pn_tran_id || '. Device has been disabled.');       
        END IF;
    END IF;
END;

-- R33+ signature
PROCEDURE sp_finalize_sale
(
    pn_tran_id IN pss.tran.tran_id%TYPE,
    pv_global_session_cd IN VARCHAR2,
    pv_tran_device_result_type_cd IN pss.tran.tran_device_result_type_cd%TYPE,
    pn_sale_result_id IN pss.sale.sale_result_id%TYPE,
    pn_sale_amount IN NUMBER,
    pn_sale_tax IN NUMBER,
    pc_tran_batch_type_cd IN pss.tran_line_item.tran_line_item_batch_type_cd%TYPE,
    pn_sale_utc_ts_ms IN NUMBER,
    pn_sale_duration_sec IN NUMBER,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pc_sale_type_cd pss.sale.sale_type_cd%TYPE DEFAULT NULL,
    pc_tran_import_needed OUT VARCHAR2,
    pc_session_update_needed OUT VARCHAR2,
    pc_client_payment_type_cd OUT VARCHAR2,
    pn_minor_currency_factor OUT PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
    pn_tli_count OUT NUMBER
)
IS
/*
    Returned result codes:
        RESULT__SUCCESS
        RESULT__FAILURE
        RESULT__HOST_NOT_FOUND
*/
    ln_tli_total pss.tran_line_item.tran_line_item_amount%TYPE;
    ln_adj_amt pss.tran_line_item.tran_line_item_amount%TYPE;
    ln_adj_tli pss.tran_line_item.tran_line_item_type_id%TYPE := -1;
    ln_base_host_id pss.tran_line_item.host_id%TYPE;
    lc_tli_batch_type_cd pss.tran_line_item.tran_line_item_batch_type_cd%TYPE;
    ln_new_host_count NUMBER;
    ln_device_id DEVICE.DEVICE_ID%TYPE;
    ln_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE;
    lc_tran_state_cd pss.tran.tran_state_cd%TYPE;
    ln_discount_percent report.campaign.discount_percent%TYPE;
    ln_discount_amount NUMBER;
    ln_sale_amount NUMBER := NVL(pn_sale_amount, 0);
    ln_pos_pta_id pss.tran.pos_pta_id%TYPE;
    ld_tran_start_ts pss.tran.tran_start_ts%TYPE;
    lv_device_tran_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE;
    lv_device_name DEVICE.DEVICE_NAME%TYPE;
    ln_consumer_acct_id PSS.TRAN.CONSUMER_ACCT_ID%TYPE;
    ln_campaign_id PSS.TRAN_LINE_ITEM.CAMPAIGN_ID%TYPE;
    lc_backoffice_virtual_flag CHAR(1);
BEGIN
    pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;
    pc_session_update_needed := 'N';
    
    IF pn_sale_duration_sec > 0 AND pc_tran_batch_type_cd = PKG_CONST.TRAN_BATCH_TYPE__ACTUAL THEN
        UPDATE pss.tran
        SET tran_end_ts = tran_start_ts + pn_sale_duration_sec / 86400
        WHERE tran_id = pn_tran_id;

        UPDATE pss.sale
        SET sale_end_utc_ts = sale_start_utc_ts + pn_sale_duration_sec / 86400
        WHERE tran_id = pn_tran_id;
    END IF;    

    SELECT POS.DEVICE_ID, X.POS_PTA_ID, c.MINOR_CURRENCY_FACTOR, PST.CLIENT_PAYMENT_TYPE_CD, D.DEVICE_TYPE_ID, DECODE(S.IMPORTED, 'N', 'Y', NULL, 'Y', 'N'), X.TRAN_STATE_CD, 
           X.TRAN_START_TS, X.TRAN_DEVICE_TRAN_CD, D.DEVICE_NAME, X.CONSUMER_ACCT_ID, CASE WHEN D.DEVICE_TYPE_ID = 14 AND D.DEVICE_SUB_TYPE_ID IN(3, 4) THEN 'Y' ELSE 'N' END
      INTO ln_device_id, ln_pos_pta_id, pn_minor_currency_factor, pc_client_payment_type_cd, ln_device_type_id, pc_tran_import_needed, lc_tran_state_cd, 
           ld_tran_start_ts, lv_device_tran_cd, lv_device_name, ln_consumer_acct_id, lc_backoffice_virtual_flag
      FROM PSS.POS POS
      JOIN DEVICE.DEVICE D ON POS.DEVICE_ID = D.DEVICE_ID
      JOIN PSS.POS_PTA PTA ON POS.POS_ID = PTA.POS_ID
      JOIN PSS.TRAN X ON PTA.POS_PTA_ID = X.POS_PTA_ID
      JOIN PSS.SALE S ON X.TRAN_ID = S.TRAN_ID
      JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
      JOIN PSS.PAYMENT_SUBTYPE PST ON PTA.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
      JOIN LOCATION.LOCATION L ON POS.LOCATION_ID = L.LOCATION_ID
      JOIN LOCATION.TIME_ZONE TZ ON L.LOCATION_TIME_ZONE_CD = TZ.TIME_ZONE_CD
    WHERE X.TRAN_ID = pn_tran_id;
    
    -- use the base host for adjustments
    SELECT MAX(H.HOST_ID)
    INTO ln_base_host_id
    FROM DEVICE.HOST H
    WHERE H.DEVICE_ID = ln_device_id
    AND H.HOST_PORT_NUM = 0;
    IF ln_base_host_id IS NULL THEN
        pkg_device_configuration.sp_create_default_hosts(ln_device_id, ln_new_host_count, pn_result_cd, pv_error_message);
        IF pn_result_cd != PKG_CONST.RESULT__SUCCESS THEN
            RETURN;
        END IF;
        SELECT H.HOST_ID
        INTO ln_base_host_id
        FROM DEVICE.HOST H
        WHERE H.DEVICE_ID = ln_device_id
        AND H.HOST_PORT_NUM = 0;
    END IF;
    
    SELECT MAX(DISCOUNT_PERCENT), MAX(CAMPAIGN_ID)
      INTO ln_discount_percent, ln_campaign_id
      FROM (
    SELECT C.DISCOUNT_PERCENT, CPP.PRIORITY, C.CAMPAIGN_ID
      FROM PSS.CAMPAIGN_POS_PTA CPP
      JOIN REPORT.CAMPAIGN C ON CPP.CAMPAIGN_ID = C.CAMPAIGN_ID
      JOIN PSS.CAMPAIGN_CONSUMER_ACCT CCA on CPP.CAMPAIGN_ID = CCA.CAMPAIGN_ID
     WHERE CCA.CONSUMER_ACCT_ID = ln_consumer_acct_id
       AND CPP.POS_PTA_ID = ln_pos_pta_id
       AND ld_tran_start_ts BETWEEN NVL(CPP.START_DATE, MIN_DATE) AND NVL(CPP.END_DATE, MAX_DATE)
       AND C.CAMPAIGN_TYPE_ID = 1
       AND C.DISCOUNT_PERCENT > 0
       AND C.DISCOUNT_PERCENT < 1
       AND (TRIM(C.RECUR_SCHEDULE) IS NULL OR REPORT.MATCH_CAMPAIGN_RECUR_SCHEDULE(C.RECUR_SCHEDULE, ld_tran_start_ts) = 'Y')
     ORDER BY CPP.PRIORITY, C.DISCOUNT_PERCENT DESC, C.CAMPAIGN_ID DESC)
     WHERE ROWNUM = 1 ;
    
    IF ln_discount_percent IS NOT NULL AND ln_sale_amount > 0 THEN
        ln_discount_amount := ROUND(ln_sale_amount * ln_discount_percent);
        IF ln_discount_amount > 0 THEN
            IF ln_discount_amount > ln_sale_amount THEN
                ln_discount_amount := ln_sale_amount;
            END IF;
            ln_sale_amount := ln_sale_amount - ln_discount_amount;
            
            UPDATE pss.sale
            SET sale_amount = ln_sale_amount / pn_minor_currency_factor
            WHERE tran_id = pn_tran_id;
            
            -- create Loyalty Discount line item
            INSERT INTO PSS.TRAN_LINE_ITEM (
                TRAN_ID,
                HOST_ID,
                TRAN_LINE_ITEM_TYPE_ID,
                TRAN_LINE_ITEM_AMOUNT,
                TRAN_LINE_ITEM_QUANTITY,
                TRAN_LINE_ITEM_DESC,
                TRAN_LINE_ITEM_BATCH_TYPE_CD,
                TRAN_LINE_ITEM_TAX,
                CAMPAIGN_ID)
            SELECT
                pn_tran_id,
                ln_base_host_id,
                tran_line_item_type_id,
                -ln_discount_amount / pn_minor_currency_factor,
                1,
                tran_line_item_type_desc || ' ' || ln_discount_percent * 100 || '%',
                pc_tran_batch_type_cd,
                0,
                ln_campaign_id
            FROM pss.tran_line_item_type
            WHERE tran_line_item_type_id = 204;
        END IF;
    END IF;
    
    SELECT /*+ INDEX(TLI IF1_TRAN_LINE_ITEM) */ NVL(SUM((NVL(TRAN_LINE_ITEM_AMOUNT, 0) + NVL(TRAN_LINE_ITEM_TAX, 0)) * NVL(TRAN_LINE_ITEM_QUANTITY, 0)), 0),
           COUNT(1)
      INTO ln_tli_total, pn_tli_count
      FROM PSS.TRAN_LINE_ITEM TLI
     WHERE TRAN_ID = pn_tran_id
       AND TRAN_LINE_ITEM_BATCH_TYPE_CD = pc_tran_batch_type_cd;

    IF pv_tran_device_result_type_cd IN (PKG_CONST.TRAN_DEV_RES__CANCELLED,
            PKG_CONST.TRAN_DEV_RES__AUTH_FAILURE,
            PKG_CONST.TRAN_DEV_RES__FAILURE,
            PKG_CONST.TRAN_DEV_RES__TIMEOUT)
        OR NVL(pn_sale_result_id, -1) != PKG_CONST.SALE_RES__SUCCESS
        OR ln_sale_amount = 0 THEN
        IF ln_tli_total != 0 THEN
            ln_adj_tli := PKG_CONST.TLI__CANCELLATION_ADJMT;
            ln_adj_amt := -ln_tli_total;
        END IF;
    ELSE
        ln_adj_amt := ln_sale_amount / pn_minor_currency_factor - ln_tli_total;
        IF ln_adj_amt > 0 THEN
            ln_adj_tli := PKG_CONST.TLI__POS_DISCREPANCY_ADJMT;
        ELSIF ln_adj_amt < 0 THEN
            ln_adj_tli := PKG_CONST.TLI__NEG_DISCREPANCY_ADJMT;
        END IF;
    END IF;
    
    IF ln_device_type_id IN (0, 1) AND pc_sale_type_cd != PKG_CONST.SALE_TYPE__CASH THEN
        IF pn_tli_count > 0 AND ln_adj_amt != 0 THEN
            IF ln_adj_amt > 0 THEN
                -- create Transaction Amount Summary record
                INSERT INTO pss.tran_line_item (
                    tran_id,
                    host_id,
                    tran_line_item_type_id,
                    tran_line_item_amount,
                    tran_line_item_quantity,
                    tran_line_item_desc,
                    tran_line_item_batch_type_cd,
                    tran_line_item_tax)
                VALUES(
                    pn_tran_id,
                    ln_base_host_id,
                    201,
                    ln_adj_amt,
                    1,
                    'Transaction Amount Summary',
                    pc_tran_batch_type_cd,
                    pn_sale_tax);
            ELSE
                INSERT INTO pss.tran_line_item (
                    tran_id,
                    host_id,
                    tran_line_item_type_id,
                    tran_line_item_amount,
                    tran_line_item_quantity,
                    tran_line_item_desc,
                    tran_line_item_batch_type_cd,
                    tran_line_item_tax)
                SELECT
                    pn_tran_id,
                    ln_base_host_id,
                    tran_line_item_type_id,
                    ln_adj_amt,
                    1,
                    tran_line_item_type_desc,
                    pc_tran_batch_type_cd,
                    pn_sale_tax
                FROM pss.tran_line_item_type
                WHERE tran_line_item_type_id = PKG_CONST.TLI__NEG_DISCREPANCY_ADJMT;
            END IF;
        END IF;
   ELSE
        IF ln_adj_tli > -1 THEN
            INSERT INTO pss.tran_line_item (
                tran_id,
                host_id,
                tran_line_item_type_id,
                tran_line_item_amount,
                tran_line_item_quantity,
                tran_line_item_desc,
                tran_line_item_batch_type_cd,
                tran_line_item_tax)
            SELECT
                pn_tran_id,
                ln_base_host_id,
                ln_adj_tli,
                ln_adj_amt,
                1,
                tran_line_item_type_desc,
                pc_tran_batch_type_cd,
                pn_sale_tax
            FROM pss.tran_line_item_type
            WHERE tran_line_item_type_id = ln_adj_tli;
        END IF;            
    END IF;

    IF pn_tli_count = 0 AND pc_tran_import_needed = 'Y' THEN
        UPDATE PSS.SALE
           SET IMPORTED = '-'
         WHERE TRAN_ID = pn_tran_id
           AND IMPORTED NOT IN('-', 'Y');
    END IF;
    
    IF lc_tran_state_cd NOT IN (PKG_CONST.TRAN_STATE__SALE_NO_AUTH, PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND, PKG_CONST.TRAN_STATE__SALE_NO_AUTH_ERROR) THEN
        --if backoffice virtual device, update PAYOR.LAST_CHARGE_*
        IF pn_sale_amount > 0 AND ln_consumer_acct_id IS NOT NULL AND lc_backoffice_virtual_flag = 'Y' THEN
            UPDATE CORP.PAYOR
               SET LAST_CHARGE_AMOUNT = ln_sale_amount / pn_minor_currency_factor, LAST_CHARGE_TS = ld_tran_start_ts
             WHERE PAY_TO_DEVICE_ID = ln_device_id
               AND GLOBAL_ACCOUNT_ID = (SELECT GLOBAL_ACCOUNT_ID FROM PSS.CONSUMER_ACCT_BASE WHERE CONSUMER_ACCT_ID = ln_consumer_acct_id)
               AND NVL(LAST_CHARGE_TS, MIN_DATE) < ld_tran_start_ts;
        END IF;                                
        pc_session_update_needed := 'Y';
    END IF;

    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;

PROCEDURE CREATE_REPLENISHMENT(
    pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
    pn_auth_id PSS.AUTH.AUTH_ID%TYPE,
    pn_replenish_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
    pv_tran_device_result_type_cd PSS.TRAN.TRAN_DEVICE_RESULT_TYPE_CD%TYPE,
    pn_device_batch_id PSS.SALE.DEVICE_BATCH_ID%TYPE,
    pc_receipt_result_cd PSS.SALE.RECEIPT_RESULT_CD%TYPE,
    pc_auth_only CHAR,
    pc_tran_state_cd OUT VARCHAR2,
    pc_client_payment_type_cd OUT VARCHAR2,
    pn_replenish_amount OUT PSS.AUTH.AUTH_AMT_APPROVED%TYPE,
    pn_replenish_balance_amount OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE)
IS
    ln_consumer_acct_id PSS.TRAN.CONSUMER_ACCT_ID%TYPE;
    ln_host_id PSS.TRAN_LINE_ITEM.HOST_ID%TYPE;
    ln_tran_line_item_id PSS.TRAN_LINE_ITEM.TRAN_LINE_ITEM_ID%TYPE;
    ln_result_cd NUMBER;
    ln_new_host_count NUMBER;
    lv_error_message VARCHAR2(4000);
    ln_device_id PSS.POS.DEVICE_ID%TYPE;
    ln_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE;
    ld_sale_utc_ts PSS.SALE.SALE_START_UTC_TS%TYPE;
    ld_sale_local_date PSS.TRAN.TRAN_START_TS%TYPE;
    lv_global_session_cd PSS.SALE.SALE_GLOBAL_SESSION_CD%TYPE;
    lv_device_name PSS.TRAN.DEVICE_NAME%TYPE;
    lv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE;
    lv_device_tran_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE;
    ld_auth_ts PSS.AUTH.AUTH_TS%TYPE;
    ln_consumer_acct_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_TYPE_ID%TYPE;
    ln_consumer_acct_sub_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_SUB_TYPE_ID%TYPE;
    lc_payment_type_cd PSS.CLIENT_PAYMENT_TYPE.PAYMENT_TYPE_CD%TYPE;
    lc_auth_hold_used PSS.AUTH.AUTH_HOLD_USED%TYPE;
    lc_is_cash CHAR(1);
    ln_corp_customer_id PSS.CONSUMER_ACCT.CORP_CUSTOMER_ID%TYPE;
    ln_consumer_acct_identifier PSS.CONSUMER_ACCT.CONSUMER_ACCT_IDENTIFIER%TYPE;                       
    lv_currency_cd PSS.POS_PTA.CURRENCY_CD%TYPE;
    ln_doc_id CORP.DOC.DOC_ID%TYPE;
    ln_ledger_id CORP.LEDGER.LEDGER_ID%TYPE;
    ln_trans_type_id PSS.PAYMENT_SUBTYPE.TRANS_TYPE_ID%TYPE;
BEGIN
    SELECT DECODE(pc_auth_only, 'Y', 0, A.AUTH_AMT_APPROVED), A.AUTH_TS, A.AUTH_HOLD_USED
      INTO pn_replenish_amount, ld_auth_ts, lc_auth_hold_used
      FROM PSS.AUTH A
     WHERE A.AUTH_ID = pn_auth_id;
    
    UPDATE PSS.TRAN X
       SET TRAN_STATE_CD = DECODE(pc_auth_only, 'Y', DECODE(lc_auth_hold_used, 'Y', '8', 'C'), (SELECT DECODE(CPT.PAYMENT_TYPE_CD, 'M', 'D', '8') FROM PSS.CLIENT_PAYMENT_TYPE CPT WHERE CPT.CLIENT_PAYMENT_TYPE_CD = X.CLIENT_PAYMENT_TYPE_CD)),
           TRAN_END_TS = TRAN_START_TS,
           TRAN_UPLOAD_TS = SYSDATE,
           TRAN_DEVICE_RESULT_TYPE_CD = pv_tran_device_result_type_cd
     WHERE TRAN_ID = pn_tran_id
     RETURNING CLIENT_PAYMENT_TYPE_CD, POS_PTA_ID, TRAN_START_TS, AUTH_GLOBAL_SESSION_CD, DEVICE_NAME, TRAN_DEVICE_TRAN_CD, TRAN_STATE_CD
      INTO pc_client_payment_type_cd, ln_pos_pta_id, ld_sale_local_date, lv_global_session_cd, lv_device_name, lv_device_tran_cd, pc_tran_state_cd;
    
    SELECT DECODE(PAYMENT_TYPE_CD, 'M', 'Y', 'N') 
      INTO lc_is_cash
      FROM PSS.CLIENT_PAYMENT_TYPE
     WHERE CLIENT_PAYMENT_TYPE_CD = pc_client_payment_type_cd;
    
    ld_sale_utc_ts := TO_TIMESTAMP(ld_auth_ts) AT TIME ZONE 'GMT';
    INSERT INTO PSS.SALE (
            TRAN_ID,
            DEVICE_BATCH_ID,
            SALE_TYPE_CD,
            SALE_START_UTC_TS,
            SALE_END_UTC_TS,
            SALE_UTC_OFFSET_MIN,
            SALE_RESULT_ID,
            SALE_AMOUNT,
            RECEIPT_RESULT_CD,
            HASH_TYPE_CD,
            TRAN_LINE_ITEM_HASH,
            SALE_GLOBAL_SESSION_CD,
            IMPORTED,
            VOID_ALLOWED) 
     SELECT pn_tran_id,
            pn_device_batch_id,
            DECODE(lc_is_cash, 'Y', 'C', 'A'),
            ld_sale_utc_ts,
            ld_sale_utc_ts,
            (ld_sale_local_date - CAST(ld_sale_utc_ts AS DATE)) * 24 * 60,
            DECODE(pc_auth_only, 'Y', 1, 0),
            pn_replenish_amount,
            pc_receipt_result_cd,
            'SHA1',
            '00',
            lv_global_session_cd,
            'N',
            'Y'
       FROM DUAL;
        
    IF pc_client_payment_type_cd IN (PKG_CONST.CLNT_PMNT_TYPE__ISIS_CREDIT, PKG_CONST.CLNT_PMNT_TYPE__ISIS_SPECIAL) AND ln_consumer_acct_id > 0 THEN
        PROCESS_ISIS_TRAN(pn_tran_id);
    END IF;
        
    SELECT D.DEVICE_ID, D.DEVICE_SERIAL_CD, PP.CURRENCY_CD, PST.TRANS_TYPE_ID
      INTO ln_device_id, lv_device_serial_cd, lv_currency_cd, ln_trans_type_id
      FROM DEVICE.DEVICE D
      JOIN PSS.POS P ON D.DEVICE_ID = P.DEVICE_ID
      JOIN PSS.POS_PTA PP ON PP.POS_ID = P.POS_ID
      JOIN PSS.PAYMENT_SUBTYPE PST ON PP.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
     WHERE PP.POS_PTA_ID = ln_pos_pta_id;
     
    ln_host_id := SF_FIND_HOST_ID(ln_device_id, 0, 0);
    IF ln_host_id IS NULL THEN
        -- create default hosts
        PKG_DEVICE_CONFIGURATION.SP_CREATE_DEFAULT_HOSTS(ln_device_id, ln_new_host_count, ln_result_cd, lv_error_message);
        IF ln_result_cd != PKG_CONST.RESULT__SUCCESS THEN
            RETURN;
        END IF;
        ln_host_id := SF_FIND_HOST_ID(ln_device_id, 0, 0);
    END IF;

    SELECT PSS.SEQ_TLI_ID.NEXTVAL
        INTO ln_tran_line_item_id
        FROM DUAL;
                  
    INSERT INTO PSS.TRAN_LINE_ITEM (
        TRAN_LINE_ITEM_ID,
        TRAN_ID,
        TRAN_LINE_ITEM_AMOUNT,
        TRAN_LINE_ITEM_POSITION_CD,
        TRAN_LINE_ITEM_TAX,
        TRAN_LINE_ITEM_TYPE_ID,
        TRAN_LINE_ITEM_QUANTITY,
        TRAN_LINE_ITEM_DESC,
        HOST_ID,
        TRAN_LINE_ITEM_BATCH_TYPE_CD,
        TRAN_LINE_ITEM_TS,
        SALE_RESULT_ID,
        APPLY_TO_CONSUMER_ACCT_ID)
    SELECT
        ln_tran_line_item_id,
        pn_tran_id,
        pn_replenish_amount,
        NULL,
        0,
        550,
        1,
        'Replenishment of card id ' || CAB.GLOBAL_ACCOUNT_ID,
        ln_host_id,
        'A',
        ld_sale_local_date,
        0,
        pn_replenish_consumer_acct_id
    FROM PSS.CONSUMER_ACCT_BASE CAB
    WHERE CONSUMER_ACCT_ID = pn_replenish_consumer_acct_id;

    UPDATE PSS.CONSUMER_ACCT
       SET CONSUMER_ACCT_BALANCE = CONSUMER_ACCT_BALANCE + pn_replenish_amount,
           CONSUMER_ACCT_REPLEN_BALANCE = CONSUMER_ACCT_REPLEN_BALANCE + pn_replenish_amount,
           CONSUMER_ACCT_REPLENISH_TOTAL = CONSUMER_ACCT_REPLENISH_TOTAL + pn_replenish_amount
     WHERE CONSUMER_ACCT_ID = pn_replenish_consumer_acct_id
      RETURNING CONSUMER_ACCT_TYPE_ID, CONSUMER_ACCT_SUB_TYPE_ID, CONSUMER_ACCT_BALANCE, CORP_CUSTOMER_ID, CONSUMER_ACCT_IDENTIFIER
      INTO ln_consumer_acct_type_id, ln_consumer_acct_sub_type_id, pn_replenish_balance_amount, ln_corp_customer_id, ln_consumer_acct_identifier;
   
    IF ln_consumer_acct_type_id IN(3) THEN
        IF lc_is_cash = 'Y' AND ln_consumer_acct_sub_type_id = 1 THEN
            IF ln_consumer_acct_identifier IS NULL THEN
                SELECT GLOBAL_ACCOUNT_ID
                  INTO ln_consumer_acct_identifier
                  FROM PSS.CONSUMER_ACCT_BASE
                 WHERE CONSUMER_ACCT_ID = pn_replenish_consumer_acct_id;
            END IF;
            -- add adjustment to ledger
            CORP.PAYMENTS_PKG.ADJUSTMENT_INS(ln_corp_customer_id, 'Cash Replenishment', lv_currency_cd, 'Cash Replenishment of card id ' || ln_consumer_acct_identifier,
                -pn_replenish_amount, ln_doc_id, ln_ledger_id);
        ELSIF ln_consumer_acct_sub_type_id = 2 THEN
            UPDATE PSS.AUTH
               SET OVERRIDE_TRANS_TYPE_ID = (
                      SELECT TT.OPERATOR_TRANS_TYPE_ID
                        FROM REPORT.TRANS_TYPE TT
                       WHERE TT.TRANS_TYPE_ID = ln_trans_type_id)
             WHERE AUTH_ID = pn_auth_id;
        END IF;
        ADD_REPLENISH_BONUSES(
            pn_tran_id,
            pn_replenish_consumer_acct_id,
            pn_replenish_amount,
            ld_auth_ts);
    END IF;
END;

PROCEDURE UPDATE_AUTHORIZATION(
    pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
    pn_auth_id PSS.AUTH.AUTH_ID%TYPE,
    pv_card_key PSS.AUTH.CARD_KEY%TYPE,
    pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE)
IS
    ld_auth_ts PSS.AUTH.AUTH_TS%TYPE;
    lv_masked_card_number PSS.TRAN.TRAN_RECEIVED_RAW_ACCT_DATA%TYPE;
    ln_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE;
    lv_device_name PSS.TRAN.DEVICE_NAME%TYPE;
    lv_device_tran_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE;
    ln_apply_to_consumer_acct_id PSS.TRAN_LINE_ITEM.APPLY_TO_CONSUMER_ACCT_ID%TYPE;
BEGIN
    UPDATE PSS.AUTH
       SET CARD_KEY = CASE WHEN CARD_KEY IS NOT NULL THEN CARD_KEY ELSE pv_card_key END
     WHERE AUTH_ID = pn_auth_id
       AND TRAN_ID = pn_tran_id
       AND pv_card_key IS NOT NULL
     RETURNING AUTH_TS INTO ld_auth_ts;
       
    UPDATE PSS.TRAN
       SET CONSUMER_ACCT_ID = CASE WHEN CONSUMER_ACCT_ID IS NOT NULL THEN CONSUMER_ACCT_ID ELSE pn_consumer_acct_id END
     WHERE TRAN_ID = pn_tran_id
     RETURNING TRAN_RECEIVED_RAW_ACCT_DATA, POS_PTA_ID, DEVICE_NAME, TRAN_DEVICE_TRAN_CD 
          INTO lv_masked_card_number, ln_pos_pta_id, lv_device_name, lv_device_tran_cd;
    
    IF pv_card_key IS NOT NULL THEN
        SELECT MAX(APPLY_TO_CONSUMER_ACCT_ID)
          INTO ln_apply_to_consumer_acct_id
          FROM PSS.TRAN_LINE_ITEM
         WHERE TRAN_ID = pn_tran_id
           AND TRAN_LINE_ITEM_TYPE_ID = 550;
        IF ln_apply_to_consumer_acct_id IS NOT NULL THEN
            FINISH_REPLENISH_SETUP(
                pn_tran_id,
                ln_apply_to_consumer_acct_id,
                pv_card_key,
                ld_auth_ts,
                lv_masked_card_number,
                ln_pos_pta_id);    
        END IF;
    END IF;
    DELETE FROM PSS.CONSUMER_ACCT_PEND_REPLENISH
         WHERE DEVICE_NAME = lv_device_name
           AND DEVICE_TRAN_CD = TO_NUMBER_OR_NULL(lv_device_tran_cd);
END;

-- R37+ signature
PROCEDURE SP_CREATE_AUTH(
   pv_global_event_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
   pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
   pn_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE,
   pv_device_event_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE,
   pc_invalid_device_event_cd CHAR,
   pn_tran_start_time NUMBER,
   pn_auth_utc_ms NUMBER,
   pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
   pc_entry_method CHAR,
   pv_masked_card_number PSS.TRAN.TRAN_RECEIVED_RAW_ACCT_DATA%TYPE,
   pn_consumer_acct_id PSS.TRAN.CONSUMER_ACCT_ID%TYPE,
   pd_auth_ts PSS.AUTH.AUTH_TS%TYPE,
   pv_authority_resp_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
   pv_authority_resp_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
   pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
   pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
   pt_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
   pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
   pn_trace_number PSS.AUTH.TRACE_NUMBER%TYPE,
   pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
   pn_auth_amt PSS.AUTH.AUTH_AMT%TYPE,
   pn_balance_amt PSS.AUTH.AUTH_BALANCE_AMT%TYPE,
   pn_requested_amt PSS.AUTH.AUTH_AUTHORITY_AMT_RQST%TYPE,
   pn_received_amt PSS.AUTH.AUTH_AUTHORITY_AMT_RCVD%TYPE,
   pn_add_auth_hold_days NUMBER,
   pc_auth_hold_used PSS.AUTH.AUTH_HOLD_USED%TYPE,
   pv_global_session_cd VARCHAR2,
   pc_ignore_dup CHAR,
   pn_auth_action_id PSS.AUTH.AUTH_ACTION_ID%TYPE,
   pn_auth_action_bitmap PSS.AUTH.AUTH_ACTION_BITMAP%TYPE,
   pc_sent_to_device CHAR,
   pc_pass_thru CHAR,
   pv_card_key PSS.AUTH.CARD_KEY%TYPE,
   pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
   pc_tran_state_cd OUT VARCHAR2,
   pc_tran_import_needed OUT VARCHAR2,
   pc_session_update_needed OUT VARCHAR2,
   pv_sale_global_session_cd OUT VARCHAR2,
   pn_sale_session_start_time OUT PSS.SALE.SALE_SESSION_START_TIME%TYPE,
   pc_client_payment_type_cd OUT VARCHAR2,
   pn_sale_amount OUT pss.sale.sale_amount%TYPE,
   pn_tli_count OUT NUMBER,
   pn_auth_id OUT PSS.AUTH.AUTH_ID%TYPE) 
IS
   lc_sale_type_cd PSS.SALE.SALE_TYPE_CD%TYPE;
   ln_sale_result_id PSS.SALE.SALE_RESULT_ID%TYPE;
   ld_orig_trace_number PSS.AUTH.TRACE_NUMBER%TYPE;
   lv_orig_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE;
   lv_last_lock_utc_ts VARCHAR2(128);
   lc_invalid_device_event_cd CHAR := pc_invalid_device_event_cd;
   ld_tran_start_ts PSS.TRAN.TRAN_START_TS%TYPE := CAST(MILLIS_TO_TIMESTAMP(pn_tran_start_time) AS DATE);
   lc_imported PSS.SALE.IMPORTED%TYPE;
   ln_auth_amt_approved pss.auth.auth_amt_approved%TYPE;
   ln_auth_amt_allowed pss.auth.auth_amt_approved%TYPE;
   ln_sale_over_auth_amt_percent NUMBER;
   lv_email_from_address engine.app_setting.app_setting_value%TYPE;
   lv_email_to_address engine.app_setting.app_setting_value%TYPE;
   lv_error pss.tran.tran_info%TYPE;
   ln_device_id device.device_id%TYPE;
   lc_payment_subtype_key_id pss.pos_pta.payment_subtype_key_id%TYPE;
   lc_payment_subtype_class pss.payment_subtype.payment_subtype_class%TYPE;
   lc_previous_tran_state_cd pss.tran.tran_state_cd%TYPE;
   lt_auth_utc_ts pss.consumer_acct_device.last_used_utc_ts%TYPE;
   ln_override_trans_type_id PSS.AUTH.OVERRIDE_TRANS_TYPE_ID%TYPE;
   ln_consumer_acct_sub_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_SUB_TYPE_ID%TYPE;
   lc_backoffice_virtual_flag CHAR(1);
BEGIN
    pn_sale_amount := 0;
    pc_session_update_needed := 'N';

    IF pc_auth_result_cd = 'Y' THEN
        ln_auth_amt_approved := pn_auth_amt / pn_minor_currency_factor;
    ELSIF pc_auth_result_cd = 'P' THEN
        ln_auth_amt_approved := pn_received_amt / pn_minor_currency_factor;
    ELSIF pc_auth_result_cd IN('V', 'C', 'M') THEN
        ln_auth_amt_approved :=  NVL(pn_received_amt, pn_auth_amt) / pn_minor_currency_factor;
    END IF;

    pc_tran_import_needed := 'N';
    lv_last_lock_utc_ts := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', pv_device_name || ':' || pv_device_event_cd);
    
    BEGIN
        SELECT tran_id, sale_type_cd, tran_state_cd, trace_number, sale_result_id, imported, sale_global_session_cd, sale_session_start_time, AUTH_RESULT_CD
        INTO pn_tran_id, lc_sale_type_cd, pc_tran_state_cd, ld_orig_trace_number, ln_sale_result_id, lc_imported, pv_sale_global_session_cd, pn_sale_session_start_time, lv_orig_auth_result_cd 
        FROM
        (
            SELECT /*+ INDEX(t IDX_DEVICE_TRAN_CD) */ t.tran_id, s.sale_type_cd, t.tran_state_cd, a.trace_number, s.sale_result_id, s.imported, s.sale_global_session_cd, s.sale_session_start_time, a.AUTH_RESULT_CD
            FROM pss.tran t
            LEFT OUTER JOIN pss.sale s ON t.tran_id = s.tran_id
            LEFT OUTER JOIN pss.auth a ON t.tran_id = a.tran_id AND a.auth_type_cd = 'N'
            WHERE t.tran_device_tran_cd = pv_device_event_cd 
              AND (t.tran_global_trans_cd = pv_global_event_cd OR t.tran_global_trans_cd LIKE pv_global_event_cd || ':%')
            ORDER BY CASE WHEN a.trace_number = pn_trace_number THEN 1 ELSE 2 END,
                CASE WHEN t.AUTH_GLOBAL_SESSION_CD = pv_global_session_cd THEN 1 ELSE 2 END,
                CASE WHEN a.AUTH_RESULT_CD IN('Y', 'P', 'V', 'C', 'M') THEN 1 ELSE 2 END,
                CASE WHEN s.sale_type_cd IN('A', 'I') THEN 1 ELSE 2 END, 
                CASE WHEN t.tran_global_trans_cd = pv_global_event_cd THEN 1 ELSE 2 END,
                t.tran_start_ts, t.created_ts, a.created_ts
        )
        WHERE ROWNUM = 1;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            pn_tran_id := NULL;
    END;
    
    IF pn_tran_id IS NOT NULL AND lc_sale_type_cd IS NOT NULL AND (lc_sale_type_cd = PKG_CONST.SALE_TYPE__CASH OR pn_auth_action_id IS NOT NULL) THEN
        pn_tran_id := NULL; -- this is not a match so insert it
        lc_invalid_device_event_cd := 'Y'; -- add ':' || tran_id to tran_global_trans_cd
    END IF;
    
    IF pn_tran_id IS NOT NULL THEN
        lc_previous_tran_state_cd := pc_tran_state_cd;
        IF ld_orig_trace_number = pn_trace_number THEN
            IF lc_imported NOT IN('Y', '-') AND lc_sale_type_cd IN(PKG_CONST.SALE_TYPE__ACTUAL, PKG_CONST.SALE_TYPE__CASH) AND ln_sale_result_id = 0 AND pc_tran_state_cd NOT IN('F', 'G', 'Z') THEN
                pc_tran_import_needed := 'Y';
            ELSE
                pc_tran_import_needed := 'N';
            END IF;                    
            RETURN; -- already inserted; exit
        ELSIF pc_auth_result_cd IN('Y', 'P', 'V', 'C', 'M') AND lv_orig_auth_result_cd  IN('Y', 'P', 'V', 'C', 'M') THEN -- Device sent dup tran id in two different auths
            pc_tran_state_cd := PKG_CONST.TRAN_STATE__DUPLICATE;
            lc_invalid_device_event_cd := 'Y';
        ELSIF pc_pass_thru = 'N' THEN -- This allows saving pass-thru auths
            IF pc_tran_state_cd IN(PKG_CONST.TRAN_STATE__SALE_NO_AUTH, PKG_CONST.TRAN_STATE__SALE_NO_AUTH_ERROR, PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND)
                OR (pc_tran_state_cd = PKG_CONST.TRAN_STATE__CLIENT_CANCELLED AND ln_sale_result_id != 0 /*Not 'Success'*/) THEN
                
                IF pc_auth_result_cd IN('Y', 'P', 'V', 'C', 'M') THEN
                    IF lc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED THEN
                        pc_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH_INTENDED;
                    ELSE
                        BEGIN
                            SELECT S.SALE_AMOUNT, P.DEVICE_ID
                            INTO pn_sale_amount, ln_device_id
                            FROM PSS.SALE S
                            JOIN PSS.TRAN T ON S.TRAN_ID = T.TRAN_ID
                            JOIN PSS.POS_PTA PP ON T.POS_PTA_ID = PP.POS_PTA_ID
                            JOIN PSS.POS P ON PP.POS_ID = P.POS_ID
                            WHERE S.TRAN_ID = pn_tran_id;
                        EXCEPTION
                            WHEN NO_DATA_FOUND THEN
                                NULL;
                        END;
                        pc_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH;
                        IF pn_sale_amount > 0 AND ln_auth_amt_approved > 0 AND pn_sale_amount > ln_auth_amt_approved THEN
                            SELECT GREATEST(ln_auth_amt_approved, NVL(NVL(MAX(DBADMIN.TO_NUMBER_OR_NULL(DS_MAA.DEVICE_SETTING_VALUE) / 100), MAX(DBADMIN.TO_NUMBER_OR_NULL(DS_AUTH_AMT.DEVICE_SETTING_VALUE) / DECODE(D.DEVICE_TYPE_ID, 13, 100, 1))), 0))
                            INTO ln_auth_amt_allowed
                            FROM DEVICE.DEVICE D
                            LEFT OUTER JOIN DEVICE.DEVICE_SETTING DS_MAA ON D.DEVICE_ID = DS_MAA.DEVICE_ID AND DS_MAA.DEVICE_SETTING_PARAMETER_CD = 'MAX_AUTH_AMOUNT'
                            LEFT OUTER JOIN DEVICE.DEVICE_SETTING DS_AUTH_AMT ON D.DEVICE_ID = DS_AUTH_AMT.DEVICE_ID
                                AND DS_AUTH_AMT.DEVICE_SETTING_PARAMETER_CD = DECODE(D.DEVICE_TYPE_ID, 13, '1200', 1, '195', 11, 'AUTHORIZATION_AMOUNT', 0, '195')
                            WHERE D.DEVICE_ID = ln_device_id;
                        
                            ln_sale_over_auth_amt_percent := NVL(DBADMIN.TO_NUMBER_OR_NULL(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('ALLOWED_SALE_AMT_OVER_AUTH_AMT_PERCENT')), 100);
                            IF pn_sale_amount > ln_auth_amt_allowed + ln_auth_amt_allowed * ln_sale_over_auth_amt_percent / 100 THEN
                                pc_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE_ERROR;
                                lv_error := 'Error: Sale amount ' || TO_CHAR(pn_sale_amount, 'FM9,999,999,990.00') || ' exceeds allowed auth amount ' || TO_CHAR(ln_auth_amt_allowed, 'FM9,999,999,990.00') || ' by more than ' || ln_sale_over_auth_amt_percent || '%';
                                UPDATE PSS.TRAN
                                SET TRAN_INFO = SUBSTR(TRAN_INFO || DECODE(TRAN_INFO, NULL, '', ', ') || lv_error, 1, 1000)
                                WHERE TRAN_ID = pn_tran_id AND (TRAN_INFO IS NULL OR INSTR(TRAN_INFO, lv_error) = 0);
                                
                                UPDATE DEVICE.DEVICE
                                SET DEVICE_ACTIVE_YN_FLAG = 'N'
                                WHERE DEVICE_NAME = pv_device_name AND DEVICE_ACTIVE_YN_FLAG = 'Y';
                                
                                lv_email_from_address := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EMAIL_FROM_ADDRESS');
                                lv_email_to_address := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EMAIL_TO_ADDRESS_CUSTOMER_SERVICE');
                                INSERT INTO ENGINE.OB_EMAIL_QUEUE(OB_EMAIL_FROM_EMAIL_ADDR, OB_EMAIL_FROM_NAME, OB_EMAIL_TO_EMAIL_ADDR, OB_EMAIL_TO_NAME, OB_EMAIL_SUBJECT, OB_EMAIL_MSG)
                                VALUES(lv_email_from_address, lv_email_from_address, lv_email_to_address, lv_email_to_address, 'Invalid sale amount', lv_error || ', device: ' || pv_device_name || ', transaction ID: ' || pn_tran_id || '. Device has been disabled.');
                            END IF;
                        END IF;
                    END IF;
                ELSE
                    IF lc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED THEN
                        pc_tran_state_cd := PKG_CONST.TRAN_STATE__INTENDED_ERROR;
                    ELSE
                        pc_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE_ERROR;
                    END IF;
                END IF;
                
                SELECT pp.PAYMENT_SUBTYPE_KEY_ID, ps.PAYMENT_SUBTYPE_CLASS, ps.CLIENT_PAYMENT_TYPE_CD, P.DEVICE_ID, CASE WHEN D.DEVICE_TYPE_ID = 14 AND D.DEVICE_SUB_TYPE_ID IN(3, 4) THEN 'Y' ELSE 'N' END
                  INTO lc_payment_subtype_key_id, lc_payment_subtype_class, pc_client_payment_type_cd, ln_device_id, lc_backoffice_virtual_flag
                  FROM PSS.POS_PTA PP
                  JOIN PSS.POS P ON PP.POS_ID = P.POS_ID
                  JOIN DEVICE.DEVICE D ON P.DEVICE_ID = D.DEVICE_ID
                  JOIN PSS.PAYMENT_SUBTYPE PS ON PP.PAYMENT_SUBTYPE_ID = PS.PAYMENT_SUBTYPE_ID
                 WHERE PP.POS_PTA_ID = pn_pos_pta_id;
                
                UPDATE PSS.TRAN
                   SET (TRAN_START_TS,
                        TRAN_END_TS,
                        TRAN_STATE_CD,
                        TRAN_RECEIVED_RAW_ACCT_DATA,
                        POS_PTA_ID,
                        CONSUMER_ACCT_ID,
                        AUTH_GLOBAL_SESSION_CD,
                        PAYMENT_SUBTYPE_KEY_ID,
                        PAYMENT_SUBTYPE_CLASS,
                        CLIENT_PAYMENT_TYPE_CD,                     
                        AUTH_HOLD_USED,
                        DEVICE_NAME) =
                    (SELECT
                        ld_tran_start_ts,  /* TRAN_START_TS */
                        TRAN_END_TS - TRAN_START_TS + ld_tran_start_ts,  /* TRAN_END_TS */
                        pc_tran_state_cd,  /* TRAN_STATE_CD */
                        pv_masked_card_number,  /* TRAN_RECEIVED_RAW_ACCT_DATA */
                        pn_pos_pta_id, /* POS_PTA_ID */
                        pn_consumer_acct_id  /* CONSUMER_ACCT_ID */,
                        pv_global_session_cd,
                        lc_payment_subtype_key_id,
                        lc_payment_subtype_class,
                        pc_client_payment_type_cd,
                        pc_auth_hold_used,
                        pv_device_name
                    FROM dual
                    ) WHERE TRAN_ID = pn_tran_id;
                    
                IF lc_previous_tran_state_cd IN (PKG_CONST.TRAN_STATE__SALE_NO_AUTH, PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND, PKG_CONST.TRAN_STATE__SALE_NO_AUTH_ERROR) THEN
                    --if backoffice virtual device, update PAYOR.LAST_CHARGE_*
                    IF pn_sale_amount > 0 AND pn_consumer_acct_id IS NOT NULL AND lc_backoffice_virtual_flag = 'Y' THEN
                        UPDATE CORP.PAYOR
                           SET LAST_CHARGE_AMOUNT = pn_sale_amount, LAST_CHARGE_TS = ld_tran_start_ts
                         WHERE PAY_TO_DEVICE_ID = ln_device_id
                           AND GLOBAL_ACCOUNT_ID = (SELECT GLOBAL_ACCOUNT_ID FROM PSS.CONSUMER_ACCT_BASE WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id)
                           AND NVL(LAST_CHARGE_TS, MIN_DATE) < ld_tran_start_ts;
                    END IF;
                    SELECT /*+ INDEX(TLI IF1_TRAN_LINE_ITEM) */ COUNT(1)
                    INTO pn_tli_count
                    FROM PSS.TRAN_LINE_ITEM TLI
                    WHERE TRAN_ID = pn_tran_id
                        AND TRAN_LINE_ITEM_BATCH_TYPE_CD = lc_sale_type_cd
                        AND TRAN_LINE_ITEM_TYPE_ID NOT IN (PKG_CONST.TLI__CANCELLATION_ADJMT, PKG_CONST.TLI__POS_DISCREPANCY_ADJMT, PKG_CONST.TLI__NEG_DISCREPANCY_ADJMT, 201);
                    
                    IF pv_sale_global_session_cd IS NOT NULL THEN
                        pc_session_update_needed := 'Y';
                        pn_sale_amount := pn_sale_amount * pn_minor_currency_factor;
                    END IF;
                END IF;
            ELSE
                pc_tran_state_cd := PKG_CONST.TRAN_STATE__DUPLICATE;
                lc_invalid_device_event_cd := 'Y';
            END IF;
        END IF;
    ELSE
        IF lc_invalid_device_event_cd = 'N' AND INSTR(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('MASTER_ID_UPDATE_SALE_TYPES'), 'N') > 0 AND TO_NUMBER_OR_NULL(pv_device_event_cd) < DBADMIN.DATE_TO_MILLIS(SYSDATE + 365) / 1000 THEN
            SELECT P.DEVICE_ID
              INTO ln_device_id
              FROM PSS.POS_PTA PP
              JOIN PSS.POS P ON PP.POS_ID = P.POS_ID
             WHERE PP.POS_PTA_ID = pn_pos_pta_id;
            UPDATE DEVICE.DEVICE_SETTING 
               SET DEVICE_SETTING_VALUE = pv_device_event_cd
             WHERE DEVICE_ID = ln_device_id
               AND DEVICE_SETTING_PARAMETER_CD = '60' -- Master Id
               AND TO_NUMBER_OR_NULL(NVL(DEVICE_SETTING_VALUE, '0')) < TO_NUMBER_OR_NULL(pv_device_event_cd);
        END IF;
        IF pc_auth_result_cd IN('Y', 'P', 'V', 'C', 'M') AND pc_sent_to_device = 'N' AND pc_auth_hold_used = 'Y' THEN
            pc_tran_state_cd := 'W'; -- Pending Reversal
        ELSIF pc_auth_result_cd IN('F') AND pc_auth_hold_used = 'Y' THEN
            pc_tran_state_cd := 'W'; -- Pending Reversal
        ELSIF pc_auth_result_cd IN('Y', 'V', 'C', 'M') THEN
            pc_tran_state_cd := '6'; -- Auth Success
        ELSIF pc_auth_result_cd IN('P') THEN
            pc_tran_state_cd := '0'; -- Auth Success Conditional
        ELSIF pc_auth_result_cd IN('N', 'O', 'R') THEN
            pc_tran_state_cd := '7'; -- Auth Decline
        ELSIF pc_auth_result_cd IN('F') THEN
            pc_tran_state_cd := '5'; -- Auth Failure
        END IF;
    END IF;
        
    IF pn_tran_id IS NULL OR pc_tran_state_cd = PKG_CONST.TRAN_STATE__DUPLICATE THEN
        SELECT PSS.SEQ_TRAN_ID.NEXTVAL
          INTO pn_tran_id
          FROM DUAL;    
        INSERT INTO PSS.TRAN (
            TRAN_ID,
            TRAN_START_TS,
            TRAN_END_TS,
            TRAN_STATE_CD,
            TRAN_DEVICE_TRAN_CD,
            TRAN_RECEIVED_RAW_ACCT_DATA,
            POS_PTA_ID,
            TRAN_GLOBAL_TRANS_CD,
            CONSUMER_ACCT_ID,
            AUTH_GLOBAL_SESSION_CD,
            PAYMENT_SUBTYPE_KEY_ID,
            PAYMENT_SUBTYPE_CLASS,
            CLIENT_PAYMENT_TYPE_CD,
            AUTH_HOLD_USED,
            DEVICE_NAME)
        SELECT
            pn_tran_id, /* TRAN_ID */
            ld_tran_start_ts,  /* TRAN_START_TS */
            ld_tran_start_ts,  /* TRAN_END_TS */
            pc_tran_state_cd,  /* TRAN_STATE_CD */
            pv_device_event_cd,  /* TRAN_DEVICE_TRAN_CD */
            pv_masked_card_number,  /* TRAN_RECEIVED_RAW_ACCT_DATA */
            pn_pos_pta_id, /* POS_PTA_ID */
            DECODE(lc_invalid_device_event_cd, 'Y', pv_global_event_cd || ':' || pn_tran_id, pv_global_event_cd), /* TRAN_GLOBAL_TRANS_CD */
            pn_consumer_acct_id  /* CONSUMER_ACCT_ID */,
            pv_global_session_cd,
            pp.payment_subtype_key_id,
            ps.payment_subtype_class,
            ps.client_payment_type_cd,
            pc_auth_hold_used,
            pv_device_name
        FROM pss.pos_pta pp
        JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
        WHERE pp.pos_pta_id = pn_pos_pta_id;
    END IF;

    IF pn_consumer_acct_id IS NOT NULL THEN
        SELECT MAX(CONSUMER_ACCT_SUB_TYPE_ID)
          INTO ln_consumer_acct_sub_type_id
          FROM PSS.CONSUMER_ACCT
         WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id;
        IF ln_consumer_acct_sub_type_id IS NOT NULL THEN
            SELECT DECODE(ln_consumer_acct_sub_type_id, 2, TT.OPERATOR_TRANS_TYPE_ID, PST.TRANS_TYPE_ID)
              INTO ln_override_trans_type_id
              FROM PSS.POS_PTA PP
              JOIN PSS.PAYMENT_SUBTYPE PST ON PP.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
              JOIN REPORT.TRANS_TYPE TT ON PST.TRANS_TYPE_ID = TT.TRANS_TYPE_ID
             WHERE PP.POS_PTA_ID = pn_pos_pta_id;
        END IF;
    END IF;
    SELECT PSS.SEQ_AUTH_ID.NEXTVAL
      INTO pn_auth_id
      FROM DUAL;
    INSERT INTO PSS.AUTH (
        AUTH_ID,
        TRAN_ID,
        AUTH_STATE_ID,
        AUTH_TYPE_CD,
        ACCT_ENTRY_METHOD_CD,
        AUTH_TS,
        AUTH_RESULT_CD,
        AUTH_RESP_CD,
        AUTH_RESP_DESC,
        AUTH_AUTHORITY_TRAN_CD,
        AUTH_AUTHORITY_REF_CD,
        AUTH_AUTHORITY_TS,
        AUTH_AUTHORITY_MISC_DATA,
        AUTH_AMT,
        AUTH_AMT_APPROVED,
        AUTH_AUTHORITY_AMT_RQST,
        AUTH_AUTHORITY_AMT_RCVD,
        AUTH_BALANCE_AMT,
        TRACE_NUMBER,
        AUTH_ACTION_ID,
        AUTH_ACTION_BITMAP,
        AUTH_HOLD_USED,
        CARD_KEY,
        OVERRIDE_TRANS_TYPE_ID)
     VALUES(
        pn_auth_id, /* AUTH_ID */
        pn_tran_id, /* TRAN_ID */
        DECODE(pc_auth_result_cd, 'Y', 2, 'N', 3, 'P', 5, 'O', 7, 'F', 4, 'R', 7, 'V', 2, 'C', 2, 'M', 2), /* AUTH_STATE_ID */
        'N', /* AUTH_TYPE_CD */
        DECODE(pc_entry_method, 'B', 7, 'C', 6, 'M', 2, 'S', 3, 'I', 8, 1), /* ACCT_ENTRY_METHOD_CD */
        pd_auth_ts, /* AUTH_TS */
        pc_auth_result_cd, /* AUTH_RESULT_CD */
        pv_authority_resp_cd, /* AUTH_RESP_CD */
        pv_authority_resp_desc, /* AUTH_RESP_DESC */
        pv_authority_tran_cd, /* AUTH_AUTHORITY_TRAN_CD */
        pv_authority_ref_cd, /* AUTH_AUTHORITY_REF_CD */
        pt_authority_ts, /* AUTH_AUTHORITY_TS */
        pv_authority_misc_data, /* AUTH_AUTHORITY_MISC_DATA */
        NVL(pn_auth_amt / pn_minor_currency_factor, 0), /* AUTH_AMT */
        ln_auth_amt_approved, /* AUTH_AMT_APPROVED */
        pn_requested_amt / pn_minor_currency_factor, /* AUTH_AUTHORITY_AMT_RQST */
        pn_received_amt / pn_minor_currency_factor,  /* AUTH_AUTHORITY_AMT_RCVD */
        pn_balance_amt / pn_minor_currency_factor, /* AUTH_BALANCE_AMT */
        pn_trace_number, /* TRACE_NUMBER */
        pn_auth_action_id,
        pn_auth_action_bitmap,
        pc_auth_hold_used,
        pv_card_key,
        ln_override_trans_type_id);

    IF NVL(pn_add_auth_hold_days, 0) > 0 THEN
        INSERT INTO PSS.CONSUMER_ACCT_AUTH_HOLD (CONSUMER_ACCT_ID, AUTH_ID, TRAN_ID, EXPIRATION_TS)
          VALUES(pn_consumer_acct_id, pn_auth_id, pn_tran_id, SYSDATE + pn_add_auth_hold_days);
    END IF;
    
    IF lc_imported NOT IN('Y', '-') AND lc_sale_type_cd IN(PKG_CONST.SALE_TYPE__ACTUAL, PKG_CONST.SALE_TYPE__CASH) AND ln_sale_result_id = 0 AND pc_tran_state_cd NOT IN('F', 'G', 'Z') THEN
        pc_tran_import_needed := 'Y';
    ELSE
        pc_tran_import_needed := 'N';
    END IF;
    
    IF pc_auth_result_cd = 'F' AND pv_authority_resp_cd = 'INVALID_AUTH_AMOUNT' AND pn_auth_amt > 0 THEN
         INSERT INTO ENGINE.MACHINE_CMD_PENDING(MACHINE_ID, DATA_TYPE, COMMAND, EXECUTE_ORDER)
             SELECT *
               FROM (
             SELECT DEVICE_NAME MACHINE_ID, DECODE(DEVICE_TYPE_ID, 13, 'CB', '88') DATA_TYPE, 
                    DECODE(DEVICE_TYPE_ID, 13, '00000E' || TO_CHAR(6 + LENGTH(TO_CHAR(AUTH_AMOUNT)), 'FM000X') || '313230303D' || RAWTOHEX(TO_CHAR(AUTH_AMOUNT)) || '0A', '420000006100000002') COMMAND, 20 EXECUTE_ORDER
               FROM (SELECT D.DEVICE_NAME, D.DEVICE_TYPE_ID, NVL(NVL(DBADMIN.TO_NUMBER_OR_NULL(DD.MAX_AUTH_AMOUNT), DBADMIN.TO_NUMBER_OR_NULL(DD.AUTH_AMOUNT) * DECODE(D.DEVICE_TYPE_ID, 13, 1, 100) * 3), 0) MAX_AUTH_AMOUNT,
                    DBADMIN.TO_NUMBER_OR_NULL(DD.AUTH_AMOUNT) AUTH_AMOUNT
               FROM PSS.POS_PTA PP
               JOIN PSS.POS P ON PP.POS_ID = P.POS_ID
               JOIN DEVICE.DEVICE D ON P.DEVICE_ID = D.DEVICE_ID
               JOIN DEVICE.DEVICE_DATA DD ON D.DEVICE_NAME = DD.DEVICE_NAME
              WHERE PP.POS_PTA_ID = pn_pos_pta_id
                AND D.DEVICE_TYPE_ID IN(0,1,13)
                AND DBADMIN.TO_NUMBER_OR_NULL(DD.AUTH_AMOUNT) > 0)
              WHERE MAX_AUTH_AMOUNT < pn_auth_amt
                AND MAX_AUTH_AMOUNT > 0) N
              WHERE NOT EXISTS(
                    SELECT 1 
                      FROM ENGINE.MACHINE_CMD_PENDING O
                    WHERE O.MACHINE_ID = N.MACHINE_ID
                      AND O.DATA_TYPE = N.DATA_TYPE
                      AND O.COMMAND = N.COMMAND);
    END IF;
    
    IF pn_consumer_acct_id IS NOT NULL AND (pc_pass_thru = 'N' OR pc_auth_result_cd IN('Y', 'P', 'V', 'C', 'M')) THEN
        lt_auth_utc_ts := DBADMIN.MILLIS_TO_TIMESTAMP(pn_auth_utc_ms);
    
        FOR i IN 1..2 LOOP
            UPDATE PSS.CONSUMER_ACCT_DEVICE
            SET USED_COUNT = USED_COUNT + 1,
                LAST_USED_UTC_TS = CASE WHEN LAST_USED_UTC_TS IS NULL OR lt_auth_utc_ts > LAST_USED_UTC_TS THEN lt_auth_utc_ts ELSE LAST_USED_UTC_TS END
            WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id AND DEVICE_NAME = pv_device_name;
            
            IF SQL%FOUND THEN
                EXIT;
            ELSE
                BEGIN
                    INSERT INTO PSS.CONSUMER_ACCT_DEVICE(CONSUMER_ACCT_ID, DEVICE_NAME, USED_COUNT, LAST_USED_UTC_TS)
                    VALUES(pn_consumer_acct_id, pv_device_name, 1, lt_auth_utc_ts);
                    EXIT;
                EXCEPTION
                    WHEN DUP_VAL_ON_INDEX THEN
                        NULL;
                END;
            END IF;
        END LOOP;
    END IF;
    
    IF pc_client_payment_type_cd IN (PKG_CONST.CLNT_PMNT_TYPE__ISIS_CREDIT, PKG_CONST.CLNT_PMNT_TYPE__ISIS_SPECIAL)
        AND pn_consumer_acct_id > 0 AND lc_previous_tran_state_cd = PKG_CONST.TRAN_STATE__SALE_NO_AUTH THEN
        PROCESS_ISIS_TRAN(pn_tran_id);
    END IF;
    
    IF REGEXP_LIKE(pv_authority_misc_data, 'ConsumerID=|OfferID=') THEN
        UPDATE REPORT.TRANS
        SET DESCRIPTION = pv_authority_misc_data
        WHERE MACHINE_TRANS_NO = pv_global_event_cd AND SOURCE_SYSTEM_CD = 'PSS' AND DESCRIPTION IS NULL;
    END IF;
END;

-- R33 - R36 signature
PROCEDURE SP_CREATE_AUTH(
   pv_global_event_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
   pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
   pn_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE,
   pv_device_event_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE,
   pc_invalid_device_event_cd CHAR,
   pn_tran_start_time NUMBER,
   pn_auth_utc_ms NUMBER,
   pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
   pc_entry_method CHAR,
   pc_payment_type CHAR,
   pv_masked_card_number PSS.TRAN.TRAN_RECEIVED_RAW_ACCT_DATA%TYPE,
   pv_pan_sha1 VARCHAR2,
   pr_consumer_acct_cd_hash PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD_HASH%TYPE,
   pn_consumer_acct_id PSS.TRAN.CONSUMER_ACCT_ID%TYPE,
   pd_auth_ts PSS.AUTH.AUTH_TS%TYPE,
   pv_authority_resp_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
   pv_authority_resp_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
   pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
   pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
   pt_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
   pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
   pn_trace_number PSS.AUTH.TRACE_NUMBER%TYPE,
   pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
   pn_auth_amt PSS.AUTH.AUTH_AMT%TYPE,
   pn_balance_amt PSS.AUTH.AUTH_BALANCE_AMT%TYPE,
   pn_requested_amt PSS.AUTH.AUTH_AUTHORITY_AMT_RQST%TYPE,
   pn_received_amt PSS.AUTH.AUTH_AUTHORITY_AMT_RCVD%TYPE,
   pn_add_auth_hold_days NUMBER,
   pc_auth_hold_used PSS.AUTH.AUTH_HOLD_USED%TYPE,
   pv_global_session_cd VARCHAR2,
   pc_ignore_dup CHAR,
   pn_auth_action_id PSS.AUTH.AUTH_ACTION_ID%TYPE,
   pn_auth_action_bitmap PSS.AUTH.AUTH_ACTION_BITMAP%TYPE,
   pc_sent_to_device CHAR,
   pc_pass_thru CHAR,
   pv_card_key PSS.AUTH.CARD_KEY%TYPE,
   pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
   pc_tran_state_cd OUT VARCHAR2,
   pc_tran_import_needed OUT VARCHAR2,
   pc_session_update_needed OUT VARCHAR2,
   pv_sale_global_session_cd OUT VARCHAR2,
   pn_sale_session_start_time OUT PSS.SALE.SALE_SESSION_START_TIME%TYPE,
   pc_client_payment_type_cd OUT VARCHAR2,
   pn_sale_amount OUT pss.sale.sale_amount%TYPE,
   pn_tli_count OUT NUMBER) 
IS
    ln_auth_id PSS.AUTH.AUTH_ID%TYPE; 
    ln_consumer_acct_id PSS.TRAN.CONSUMER_ACCT_ID%TYPE := pn_consumer_acct_id;
BEGIN
    IF ln_consumer_acct_id IS NULL AND pr_consumer_acct_cd_hash IS NOT NULL THEN
        SELECT MAX(CONSUMER_ACCT_ID)
        INTO ln_consumer_acct_id
        FROM PSS.CONSUMER_ACCT
        WHERE CONSUMER_ACCT_CD_HASH = pr_consumer_acct_cd_hash
            AND CONSUMER_ACCT_CD = pv_masked_card_number
            AND CONSUMER_ACCT_TYPE_ID = 5;
    END IF;
    
    SP_CREATE_AUTH(
       pv_global_event_cd,
       pv_device_name,
       pn_pos_pta_id,
       pv_device_event_cd,
       pc_invalid_device_event_cd,
       pn_tran_start_time,
       pn_auth_utc_ms,
       pc_auth_result_cd,
       pc_entry_method,
       pv_masked_card_number,
       ln_consumer_acct_id,
       pd_auth_ts,
       pv_authority_resp_cd,
       pv_authority_resp_desc,
       pv_authority_tran_cd,
       pv_authority_ref_cd,
       pt_authority_ts,
       pv_authority_misc_data,
       pn_trace_number,
       pn_minor_currency_factor,
       pn_auth_amt,
       pn_balance_amt,
       pn_requested_amt,
       pn_received_amt,
       pn_add_auth_hold_days,
       pc_auth_hold_used,
       pv_global_session_cd,
       pc_ignore_dup,
       pn_auth_action_id,
       pn_auth_action_bitmap,
       pc_sent_to_device,
       pc_pass_thru,
       pv_card_key,
       pn_tran_id,
       pc_tran_state_cd,
       pc_tran_import_needed,
       pc_session_update_needed,
       pv_sale_global_session_cd,
       pn_sale_session_start_time,
       pc_client_payment_type_cd,
       pn_sale_amount,
       pn_tli_count,
       ln_auth_id);
END;

-- R37 and above
PROCEDURE SP_INSERT_AUTH_STATS(
   pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
   pn_auth_id PSS.AUTH.AUTH_ID%TYPE,
   pn_request_time NUMBER,
   pn_applayer_start_time NUMBER,
   pn_authority_start_time NUMBER,
   pn_authority_end_time NUMBER,
   pn_applayer_end_time NUMBER,
   pn_response_time NUMBER
) IS
BEGIN
    INSERT INTO PSS.TRAN_STAT(TRAN_ID, AUTH_ID, TRAN_STAT_TYPE_ID, TRAN_STAT_VALUE)
      SELECT pn_tran_id, pn_auth_id, TRAN_STAT_TYPE_ID, TRAN_STAT_VALUE FROM (
        SELECT 2 /* live auth "POSM" time*/ TRAN_STAT_TYPE_ID, (pn_applayer_end_time - pn_applayer_start_time) / 1000 TRAN_STAT_VALUE FROM DUAL WHERE pn_applayer_start_time IS NOT NULL AND pn_applayer_end_time IS NOT NULL
        UNION ALL SELECT 4 /* live auth network time*/, (pn_response_time - pn_request_time) / 1000 FROM DUAL WHERE pn_request_time IS NOT NULL AND pn_response_time IS NOT NULL
        UNION ALL SELECT 1 /* live auth gateway time*/, (pn_authority_end_time - pn_authority_start_time) / 1000 FROM DUAL WHERE pn_authority_start_time IS NOT NULL AND pn_authority_end_time IS NOT NULL) a
     WHERE NOT EXISTS(SELECT 1 FROM PSS.TRAN_STAT TS WHERE pn_tran_id = TS.TRAN_ID AND NVL(pn_auth_id, 0) = NVL(TS.AUTH_ID, 0) AND A.TRAN_STAT_TYPE_ID = TS.TRAN_STAT_TYPE_ID);    
END;

-- R36 and below
PROCEDURE SP_INSERT_AUTH_STATS(
   pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
   pn_request_time NUMBER,
   pn_applayer_start_time NUMBER,
   pn_authority_start_time NUMBER,
   pn_authority_end_time NUMBER,
   pn_applayer_end_time NUMBER,
   pn_response_time NUMBER
) IS
BEGIN
    SP_INSERT_AUTH_STATS(
       pn_tran_id,
       NULL,
       pn_request_time,
       pn_applayer_start_time,
       pn_authority_start_time,
       pn_authority_end_time,
       pn_applayer_end_time,
       pn_response_time);
END;

-- ? - R36 Signature
PROCEDURE SP_PERMIT_CONSUMER_ACCT(
    pn_pos_pta_id PSS.POS_PTA.POS_PTA_ID%TYPE,
    pv_consumer_acct_cd PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD%TYPE,
    pt_auth_ts PSS.CONSUMER_ACCT.CONSUMER_ACCT_ACTIVATION_TS%TYPE,
    pv_currency_cd PSS.CURRENCY.CURRENCY_CD%TYPE,
    pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
    pn_action_id OUT PSS.PERMISSION_ACTION.ACTION_ID%TYPE,
    pn_action_code OUT NUMBER,
    pn_action_bitmap OUT NUMBER,
    pr_consumer_acct_cd_hash IN PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD_HASH%TYPE DEFAULT NULL
)
IS
    lc_store_action CHAR(1);
    lv_device_name DEVICE.DEVICE_NAME%TYPE;
    ln_device_id DEVICE.DEVICE_ID%TYPE;
    ln_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE;
BEGIN
    SELECT CONSUMER_ACCT_ID, DEVICE_ID, DEVICE_TYPE_ID, DEVICE_NAME
      INTO pn_consumer_acct_id, ln_device_id, ln_device_type_id, lv_device_name
      FROM (
         SELECT CA.CONSUMER_ACCT_ID, D.DEVICE_ID, D.DEVICE_TYPE_ID, D.DEVICE_NAME, VLH.ANCESTOR_LOCATION_ID, MAX(CA.CONSUMER_ACCT_ISSUE_NUM) MAX_ISSUE_NUM, VLH.DEPTH
           FROM PSS.POS_PTA PTA
           JOIN PSS.POS POS ON PTA.POS_ID = POS.POS_ID
           JOIN DEVICE.DEVICE D ON D.DEVICE_ID = POS.DEVICE_ID
           JOIN LOCATION.VW_LOCATION_HIERARCHY VLH ON VLH.DESCENDENT_LOCATION_ID = POS.LOCATION_ID
           JOIN PSS.CONSUMER_ACCT CA ON VLH.ANCESTOR_LOCATION_ID = CA.LOCATION_ID
          WHERE PTA.POS_PTA_ID = pn_pos_pta_id
            AND (pr_consumer_acct_cd_hash IS NOT NULL AND CA.CONSUMER_ACCT_CD_HASH = pr_consumer_acct_cd_hash OR CA.CONSUMER_ACCT_CD = pv_consumer_acct_cd)
            AND CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y'
            AND NVL(CA.CONSUMER_ACCT_ACTIVATION_TS, MIN_DATE) <= pt_auth_ts
            AND NVL(CA.CONSUMER_ACCT_DEACTIVATION_TS, MAX_DATE) > pt_auth_ts
            AND CA.CURRENCY_CD = pv_currency_cd
            GROUP BY CA.CONSUMER_ACCT_ID, VLH.ANCESTOR_LOCATION_ID, VLH.DEPTH, D.DEVICE_ID, D.DEVICE_TYPE_ID, D.DEVICE_NAME
            ORDER BY VLH.DEPTH, MAX_ISSUE_NUM DESC    /* DEPTH IS ASCENDING, AS IT IS THE DIFFERENCE BETWEEN LOCATION AND ANCESTOR */
    ) WHERE ROWNUM = 1;

    SELECT A.ACTION_ID, DTA.DEVICE_TYPE_ACTION_CD,
           DECODE(A.ACTION_PARAM_TYPE_CD, 'B', SUM(POWER(2, AP.PROTOCOL_BIT_INDEX))) PROTOCOL_BITMAP,
           DECODE(A.ACTION_CLEAR_PARAMETER_CD, NULL, 'N', 'Y') STORE_LAST_ACTION
      INTO pn_action_id, pn_action_code, pn_action_bitmap, lc_store_action
      FROM (SELECT * FROM (
         SELECT CAP.PERMISSION_ACTION_ID, DTA.DEVICE_TYPE_ACTION_CD PROTOCOL_ACTION_CD,
                CASE WHEN LDA.DEVICE_ACTION_UTC_TS IS NOT NULL
                          AND CURRENT_TIMESTAMP < LDA.DEVICE_ACTION_UTC_TS
                          + NUMTODSINTERVAL(COALESCE(TO_NUMBER_OR_NULL(DS_T.DEVICE_SETTING_VALUE),
                          TO_NUMBER_OR_NULL(cts.CONFIG_TEMPLATE_SETTING_VALUE), 3600), 'SECOND') THEN 10
                     ELSE DTA.ACTION_ID END ACTION_ID
           FROM PSS.CONSUMER_ACCT_PERMISSION CAP
           JOIN PSS.PERMISSION_ACTION PA ON CAP.PERMISSION_ACTION_ID = PA.PERMISSION_ACTION_ID
           JOIN DEVICE.DEVICE_TYPE_ACTION DTA ON DTA.ACTION_ID = PA.ACTION_ID
           JOIN DEVICE.DEVICE_TYPE DT ON DTA.DEVICE_TYPE_ID = DT.DEVICE_TYPE_ID
           LEFT OUTER JOIN PSS.LAST_DEVICE_ACTION LDA
             ON LDA.DEVICE_NAME = lv_device_name
            AND CAP.CONSUMER_ACCT_ID = LDA.CONSUMER_ACCT_ID
            AND PA.ACTION_ID = LDA.DEVICE_ACTION_ID
           JOIN DEVICE.ACTION A ON PA.ACTION_ID = A.ACTION_ID
           LEFT OUTER JOIN DEVICE.DEVICE_SETTING DS_T ON ln_device_id = DS_T.DEVICE_ID AND DS_T.DEVICE_SETTING_PARAMETER_CD = A.ACTION_CLEAR_PARAMETER_CD
           LEFT OUTER JOIN DEVICE.DEVICE_SETTING ds_v ON ln_device_id = ds_v.DEVICE_ID AND ds_v.DEVICE_SETTING_PARAMETER_CD = 'Property List Version'
           LEFT OUTER JOIN DEVICE.CONFIG_TEMPLATE ct
             ON ct.CONFIG_TEMPLATE_NAME = DECODE(DBADMIN.PKG_UTL.COMPARE(DT.DEVICE_TYPE_ID, 13),
                -1, DT.DEFAULT_CONFIG_TEMPLATE_NAME, DT.DEFAULT_CONFIG_TEMPLATE_NAME || ds_v.DEVICE_SETTING_VALUE)
           LEFT OUTER JOIN DEVICE.CONFIG_TEMPLATE_SETTING cts
             ON cts.CONFIG_TEMPLATE_ID = ct.CONFIG_TEMPLATE_ID
            AND cts.DEVICE_SETTING_PARAMETER_CD = A.ACTION_CLEAR_PARAMETER_CD
          WHERE CAP.CONSUMER_ACCT_ID = pn_consumer_acct_id
            AND DTA.DEVICE_TYPE_ID = ln_device_type_id
          ORDER BY CAP.CONSUMER_ACCT_PERMISSION_ORDER
      ) WHERE ROWNUM = 1) O
      LEFT OUTER JOIN (PSS.PERMISSION_ACTION_PARAM PAP
      JOIN DEVICE.ACTION_PARAM AP ON PAP.ACTION_PARAM_ID = AP.ACTION_PARAM_ID)
        ON O.PERMISSION_ACTION_ID = PAP.PERMISSION_ACTION_ID
      JOIN DEVICE.ACTION A ON O.ACTION_ID = A.ACTION_ID
      JOIN DEVICE.DEVICE_TYPE_ACTION DTA ON DTA.ACTION_ID = O.ACTION_ID
     WHERE DTA.DEVICE_TYPE_ID = ln_device_type_id
      GROUP BY A.ACTION_ID, DTA.DEVICE_TYPE_ACTION_CD, A.ACTION_CLEAR_PARAMETER_CD, A.ACTION_PARAM_TYPE_CD;
    IF lc_store_action = 'Y' THEN
        MERGE INTO PSS.LAST_DEVICE_ACTION O
         USING (
              SELECT lv_device_name DEVICE_NAME,
                     pn_consumer_acct_id CONSUMER_ACCT_ID,
                     pn_action_id DEVICE_ACTION_ID,
                     CURRENT_TIMESTAMP DEVICE_ACTION_UTC_TS
                FROM DUAL) N
              ON (O.DEVICE_NAME = N.DEVICE_NAME)
              WHEN MATCHED THEN
               UPDATE
                  SET O.CONSUMER_ACCT_ID = N.CONSUMER_ACCT_ID,
                      O.DEVICE_ACTION_ID = N.DEVICE_ACTION_ID,
                      O.DEVICE_ACTION_UTC_TS = N.DEVICE_ACTION_UTC_TS
              WHEN NOT MATCHED THEN
               INSERT (O.DEVICE_NAME,
                       O.CONSUMER_ACCT_ID,
                       O.DEVICE_ACTION_ID,
                       O.DEVICE_ACTION_UTC_TS)
                VALUES(N.DEVICE_NAME,
                       N.CONSUMER_ACCT_ID,
                       N.DEVICE_ACTION_ID,
                       N.DEVICE_ACTION_UTC_TS
                );
    END IF;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN;
END;

-- R37+ Signature
PROCEDURE PERMIT_CONSUMER_ACCT(
    pn_pos_pta_id PSS.POS_PTA.POS_PTA_ID%TYPE,
    pn_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
    pt_auth_ts PSS.CONSUMER_ACCT.CONSUMER_ACCT_ACTIVATION_TS%TYPE,
    pl_consumer_acct_type_ids NUMBER_TABLE,
    pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
    pn_consumer_id OUT PSS.CONSUMER_ACCT.CONSUMER_ID%TYPE,
    pn_consumer_acct_type_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_TYPE_ID%TYPE,
    pv_consumer_acct_type_label OUT PSS.CONSUMER_ACCT_TYPE.CONSUMER_ACCT_TYPE_LABEL%TYPE,
    pb_consumer_acct_raw_hash OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_RAW_HASH%TYPE, 
    pb_consumer_acct_raw_salt OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_RAW_SALT%TYPE, 
    pb_security_cd_hash OUT PSS.CONSUMER_ACCT.SECURITY_CD_HASH%TYPE, 
    pb_security_cd_salt OUT PSS.CONSUMER_ACCT.SECURITY_CD_SALT%TYPE,
    pn_action_id OUT PSS.PERMISSION_ACTION.ACTION_ID%TYPE,
    pn_action_code OUT NUMBER,
    pn_action_bitmap OUT NUMBER)
IS
    lc_store_action CHAR(1);
    lv_device_name DEVICE.DEVICE_NAME%TYPE;
    ln_device_id DEVICE.DEVICE_ID%TYPE;
    ln_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE;
BEGIN
    SELECT CONSUMER_ACCT_ID, CONSUMER_ID, CONSUMER_ACCT_TYPE_ID, CONSUMER_ACCT_TYPE_LABEL, CONSUMER_ACCT_RAW_HASH, CONSUMER_ACCT_RAW_SALT, SECURITY_CD_HASH, SECURITY_CD_SALT, DEVICE_ID, DEVICE_TYPE_ID, DEVICE_NAME
      INTO pn_consumer_acct_id, pn_consumer_id, pn_consumer_acct_type_id, pv_consumer_acct_type_label, pb_consumer_acct_raw_hash, pb_consumer_acct_raw_salt, pb_security_cd_hash, pb_security_cd_salt, ln_device_id, ln_device_type_id, lv_device_name
      FROM (
         SELECT CA.CONSUMER_ACCT_ID, CA.CONSUMER_ID, CA.CONSUMER_ACCT_TYPE_ID, CAT.CONSUMER_ACCT_TYPE_LABEL, CA.CONSUMER_ACCT_RAW_HASH, CA.CONSUMER_ACCT_RAW_SALT, CA.SECURITY_CD_HASH, CA.SECURITY_CD_SALT, D.DEVICE_ID, D.DEVICE_TYPE_ID, D.DEVICE_NAME, VLH.ANCESTOR_LOCATION_ID, MAX(CA.CONSUMER_ACCT_ISSUE_NUM) MAX_ISSUE_NUM, VLH.DEPTH
           FROM PSS.POS_PTA PTA
           JOIN PSS.POS POS ON PTA.POS_ID = POS.POS_ID
           JOIN DEVICE.DEVICE D ON D.DEVICE_ID = POS.DEVICE_ID
           JOIN LOCATION.VW_LOCATION_HIERARCHY VLH ON VLH.DESCENDENT_LOCATION_ID = POS.LOCATION_ID
           JOIN PSS.CONSUMER_ACCT CA ON VLH.ANCESTOR_LOCATION_ID = CA.LOCATION_ID
           JOIN PSS.CONSUMER_ACCT_TYPE CAT ON CAT.CONSUMER_ACCT_TYPE_ID = CA.CONSUMER_ACCT_TYPE_ID
           JOIN PSS.CONSUMER_ACCT_BASE CAB ON CAB.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
          WHERE PTA.POS_PTA_ID = pn_pos_pta_id
            AND CAB.GLOBAL_ACCOUNT_ID = pn_global_account_id
            AND CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y'
            AND NVL(CA.CONSUMER_ACCT_ACTIVATION_TS, MIN_DATE) <= pt_auth_ts
            AND NVL(CA.CONSUMER_ACCT_DEACTIVATION_TS, MAX_DATE) > pt_auth_ts
            AND CA.CONSUMER_ACCT_TYPE_ID MEMBER OF pl_consumer_acct_type_ids
            GROUP BY CA.CONSUMER_ACCT_ID, CA.CONSUMER_ID, CA.CONSUMER_ACCT_TYPE_ID, CAT.CONSUMER_ACCT_TYPE_LABEL, CA.CONSUMER_ACCT_RAW_HASH, CA.CONSUMER_ACCT_RAW_SALT, CA.SECURITY_CD_HASH, CA.SECURITY_CD_SALT, D.DEVICE_ID, D.DEVICE_TYPE_ID, D.DEVICE_NAME, VLH.ANCESTOR_LOCATION_ID, VLH.DEPTH
            ORDER BY VLH.DEPTH, MAX_ISSUE_NUM DESC    /* DEPTH IS ASCENDING, AS IT IS THE DIFFERENCE BETWEEN LOCATION AND ANCESTOR */
    ) WHERE ROWNUM = 1;

    SELECT A.ACTION_ID, DTA.DEVICE_TYPE_ACTION_CD,
           DECODE(A.ACTION_PARAM_TYPE_CD, 'B', SUM(POWER(2, AP.PROTOCOL_BIT_INDEX))) PROTOCOL_BITMAP,
           DECODE(A.ACTION_CLEAR_PARAMETER_CD, NULL, 'N', 'Y') STORE_LAST_ACTION
      INTO pn_action_id, pn_action_code, pn_action_bitmap, lc_store_action
      FROM (SELECT * FROM (
         SELECT CAP.PERMISSION_ACTION_ID, DTA.DEVICE_TYPE_ACTION_CD PROTOCOL_ACTION_CD,
                CASE WHEN LDA.DEVICE_ACTION_UTC_TS IS NOT NULL
                          AND CURRENT_TIMESTAMP < LDA.DEVICE_ACTION_UTC_TS
                          + NUMTODSINTERVAL(COALESCE(TO_NUMBER_OR_NULL(DS_T.DEVICE_SETTING_VALUE),
                          TO_NUMBER_OR_NULL(cts.CONFIG_TEMPLATE_SETTING_VALUE), 3600), 'SECOND') THEN 10
                     ELSE DTA.ACTION_ID END ACTION_ID
           FROM PSS.CONSUMER_ACCT_PERMISSION CAP
           JOIN PSS.PERMISSION_ACTION PA ON CAP.PERMISSION_ACTION_ID = PA.PERMISSION_ACTION_ID
           JOIN DEVICE.DEVICE_TYPE_ACTION DTA ON DTA.ACTION_ID = PA.ACTION_ID
           JOIN DEVICE.DEVICE_TYPE DT ON DTA.DEVICE_TYPE_ID = DT.DEVICE_TYPE_ID
           LEFT OUTER JOIN PSS.LAST_DEVICE_ACTION LDA
             ON LDA.DEVICE_NAME = lv_device_name
            AND CAP.CONSUMER_ACCT_ID = LDA.CONSUMER_ACCT_ID
            AND PA.ACTION_ID = LDA.DEVICE_ACTION_ID
           JOIN DEVICE.ACTION A ON PA.ACTION_ID = A.ACTION_ID
           LEFT OUTER JOIN DEVICE.DEVICE_SETTING DS_T ON ln_device_id = DS_T.DEVICE_ID AND DS_T.DEVICE_SETTING_PARAMETER_CD = A.ACTION_CLEAR_PARAMETER_CD
           LEFT OUTER JOIN DEVICE.DEVICE_SETTING ds_v ON ln_device_id = ds_v.DEVICE_ID AND ds_v.DEVICE_SETTING_PARAMETER_CD = 'Property List Version'
           LEFT OUTER JOIN DEVICE.CONFIG_TEMPLATE ct
             ON ct.CONFIG_TEMPLATE_NAME = DECODE(DBADMIN.PKG_UTL.COMPARE(DT.DEVICE_TYPE_ID, 13),
                -1, DT.DEFAULT_CONFIG_TEMPLATE_NAME, DT.DEFAULT_CONFIG_TEMPLATE_NAME || ds_v.DEVICE_SETTING_VALUE)
           LEFT OUTER JOIN DEVICE.CONFIG_TEMPLATE_SETTING cts
             ON cts.CONFIG_TEMPLATE_ID = ct.CONFIG_TEMPLATE_ID
            AND cts.DEVICE_SETTING_PARAMETER_CD = A.ACTION_CLEAR_PARAMETER_CD
          WHERE CAP.CONSUMER_ACCT_ID = pn_consumer_acct_id
            AND DTA.DEVICE_TYPE_ID = ln_device_type_id
          ORDER BY CAP.CONSUMER_ACCT_PERMISSION_ORDER
      ) WHERE ROWNUM = 1) O
      LEFT OUTER JOIN (PSS.PERMISSION_ACTION_PARAM PAP
      JOIN DEVICE.ACTION_PARAM AP ON PAP.ACTION_PARAM_ID = AP.ACTION_PARAM_ID)
        ON O.PERMISSION_ACTION_ID = PAP.PERMISSION_ACTION_ID
      JOIN DEVICE.ACTION A ON O.ACTION_ID = A.ACTION_ID
      JOIN DEVICE.DEVICE_TYPE_ACTION DTA ON DTA.ACTION_ID = O.ACTION_ID
     WHERE DTA.DEVICE_TYPE_ID = ln_device_type_id
      GROUP BY A.ACTION_ID, DTA.DEVICE_TYPE_ACTION_CD, A.ACTION_CLEAR_PARAMETER_CD, A.ACTION_PARAM_TYPE_CD;
    IF lc_store_action = 'Y' THEN
        MERGE INTO PSS.LAST_DEVICE_ACTION O
         USING (
              SELECT lv_device_name DEVICE_NAME,
                     pn_consumer_acct_id CONSUMER_ACCT_ID,
                     pn_action_id DEVICE_ACTION_ID,
                     CURRENT_TIMESTAMP DEVICE_ACTION_UTC_TS
                FROM DUAL) N
              ON (O.DEVICE_NAME = N.DEVICE_NAME)
              WHEN MATCHED THEN
               UPDATE
                  SET O.CONSUMER_ACCT_ID = N.CONSUMER_ACCT_ID,
                      O.DEVICE_ACTION_ID = N.DEVICE_ACTION_ID,
                      O.DEVICE_ACTION_UTC_TS = N.DEVICE_ACTION_UTC_TS
              WHEN NOT MATCHED THEN
               INSERT (O.DEVICE_NAME,
                       O.CONSUMER_ACCT_ID,
                       O.DEVICE_ACTION_ID,
                       O.DEVICE_ACTION_UTC_TS)
                VALUES(N.DEVICE_NAME,
                       N.CONSUMER_ACCT_ID,
                       N.DEVICE_ACTION_ID,
                       N.DEVICE_ACTION_UTC_TS
                );
    END IF;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN;
END;

    -- R37+ signature
    PROCEDURE SP_CREATE_LOCAL_AUTH_SALE(
       pc_global_event_cd_prefix IN CHAR,
       pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
       pn_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE,
       pv_device_tran_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE,
       pn_sale_utc_ts_ms NUMBER,
       pn_sale_utc_offset_min IN pss.sale.sale_utc_offset_min%TYPE,
       pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
       pc_entry_method CHAR,
       pc_payment_type CHAR,
       pv_track_data PSS.TRAN.TRAN_RECEIVED_RAW_ACCT_DATA%TYPE,
       pn_consumer_acct_id PSS.TRAN.CONSUMER_ACCT_ID%TYPE,
       pd_auth_ts PSS.AUTH.AUTH_TS%TYPE,
       pn_trace_number PSS.AUTH.TRACE_NUMBER%TYPE,
       pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
       pn_auth_amt PSS.AUTH.AUTH_AMT%TYPE,
       pn_device_batch_id IN pss.sale.device_batch_id%TYPE,
       pc_sale_type_cd IN pss.sale.sale_type_cd%TYPE,
       pv_tran_device_result_type_cd IN pss.tran.tran_device_result_type_cd%TYPE,
       pn_sale_result_id IN pss.sale.sale_result_id%TYPE,
       pv_hash_type_cd IN pss.sale.hash_type_cd%TYPE,
       pv_tran_line_item_hash IN pss.sale.tran_line_item_hash%TYPE,
       pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
       pn_result_cd OUT NUMBER,
       pv_error_message OUT VARCHAR2
    ) IS
       ln_auth_id PSS.AUTH.AUTH_ID%TYPE;
       lv_global_trans_cd pss.tran.tran_global_trans_cd%TYPE;
       lv_global_trans_cd_app_layer pss.tran.tran_global_trans_cd%TYPE;
       lv_global_trans_cd_legacy pss.tran.tran_global_trans_cd%TYPE;
       ld_tran_upload_ts pss.tran.tran_upload_ts%TYPE;
       ln_tran_exists NUMBER := PKG_CONST.BOOLEAN__FALSE;
       ln_tli_hash_match NUMBER;
       ld_current_ts DATE := SYSDATE;
       ld_tran_start_ts pss.tran.tran_start_ts%TYPE;
       lt_sale_start_utc_ts pss.sale.sale_start_utc_ts%TYPE;
       lv_last_lock_utc_ts VARCHAR2(128);
       ln_insert_tran NUMBER := PKG_CONST.BOOLEAN__FALSE;
       lv_tran_state_cd pss.tran.tran_state_cd%TYPE := PKG_CONST.TRAN_STATE__COMPLETE_ERROR;
    BEGIN
        IF pc_global_event_cd_prefix NOT IN (PKG_CONST.EVENT_CODE_PREFIX__APP_LAYER,
            PKG_CONST.EVENT_CODE_PREFIX__LEGACY) THEN
            pn_result_cd := PKG_CONST.RESULT__INVALID_PARAMETER;
            pv_error_message := 'Invalid pc_global_event_cd_prefix: ' || pc_global_event_cd_prefix;
            RETURN;
        END IF;
    
        lv_global_trans_cd := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(pc_global_event_cd_prefix, pv_device_name, pv_device_tran_cd);
        lv_global_trans_cd_app_layer := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(PKG_CONST.EVENT_CODE_PREFIX__APP_LAYER, pv_device_name, pv_device_tran_cd);
        lv_global_trans_cd_legacy := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(PKG_CONST.EVENT_CODE_PREFIX__LEGACY, pv_device_name, pv_device_tran_cd);
        ld_tran_start_ts := CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_sale_utc_ts_ms + pn_sale_utc_offset_min * 60 * 1000) AS DATE);
        lt_sale_start_utc_ts := CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_sale_utc_ts_ms) AS DATE);
        lv_last_lock_utc_ts := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', pv_device_name || ':' || pv_device_tran_cd);
        BEGIN
            SELECT tran_id, tran_upload_ts, PKG_CONST.BOOLEAN__TRUE, tli_hash_match
            INTO pn_tran_id, ld_tran_upload_ts, ln_tran_exists, ln_tli_hash_match
            FROM
            (
                SELECT /*+ INDEX(t IDX_DEVICE_TRAN_CD) */ t.tran_id, t.tran_upload_ts,
                    CASE WHEN s.hash_type_cd = pv_hash_type_cd
                        AND s.tran_line_item_hash = pv_tran_line_item_hash
                        AND s.sale_type_cd = pc_sale_type_cd THEN PKG_CONST.BOOLEAN__TRUE
                    ELSE PKG_CONST.BOOLEAN__FALSE END AS tli_hash_match
                FROM pss.tran t
                LEFT OUTER JOIN pss.sale s ON t.tran_id = s.tran_id
                WHERE t.tran_device_tran_cd = pv_device_tran_cd AND (
                    t.tran_global_trans_cd IN (lv_global_trans_cd_app_layer, lv_global_trans_cd_legacy)
                    OR t.tran_global_trans_cd LIKE lv_global_trans_cd_app_layer || ':%'
                    OR t.tran_global_trans_cd LIKE lv_global_trans_cd_legacy || ':%'
                )
                ORDER BY CASE WHEN t.tran_global_trans_cd IN (lv_global_trans_cd_app_layer, lv_global_trans_cd_legacy) THEN 1 ELSE 2 END,
                    CASE WHEN s.sale_type_cd = pc_sale_type_cd THEN 1 ELSE 2 END,
                    tli_hash_match DESC, t.tran_start_ts, t.created_ts
            )
            WHERE ROWNUM = 1;
    
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                ln_insert_tran := PKG_CONST.BOOLEAN__TRUE;
        END;
    
        IF ln_tran_exists = PKG_CONST.BOOLEAN__TRUE AND ld_tran_upload_ts IS NOT NULL THEN
            IF ln_tli_hash_match = PKG_CONST.BOOLEAN__TRUE THEN
                UPDATE pss.sale
                SET duplicate_count = duplicate_count + 1
                WHERE tran_id = pn_tran_id;
    
                pn_result_cd := PKG_CONST.RESULT__DUPLICATE;
                pv_error_message := 'Duplicate sale, original tran_id: ' || pn_tran_id;
                pn_tran_id := 0;
                RETURN;
            END IF;
        
            ln_insert_tran := PKG_CONST.BOOLEAN__TRUE;
        END IF;
    
        IF ln_insert_tran = PKG_CONST.BOOLEAN__TRUE THEN
            IF pc_sale_type_cd = PKG_CONST.SALE_TYPE__CASH THEN
                lv_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE;
            ELSIF pv_tran_device_result_type_cd IN (PKG_CONST.TRAN_DEV_RES__CANCELLED,
                    PKG_CONST.TRAN_DEV_RES__AUTH_FAILURE,
                    PKG_CONST.TRAN_DEV_RES__FAILURE,
                    PKG_CONST.TRAN_DEV_RES__TIMEOUT) OR pn_auth_amt <= 0 THEN
                lv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED;
            END IF;
            
            SELECT PSS.SEQ_TRAN_ID.NEXTVAL
            INTO pn_tran_id
            FROM DUAL;
    
            IF ln_tran_exists = PKG_CONST.BOOLEAN__TRUE THEN
                lv_global_trans_cd := lv_global_trans_cd || ':' || pn_tran_id;
            END IF;     
    
            INSERT INTO PSS.TRAN (
                TRAN_ID,
                TRAN_START_TS,
                TRAN_END_TS,
                TRAN_UPLOAD_TS,
                TRAN_STATE_CD,
                TRAN_DEVICE_TRAN_CD,
                TRAN_RECEIVED_RAW_ACCT_DATA,
                POS_PTA_ID,
                TRAN_GLOBAL_TRANS_CD,
                CONSUMER_ACCT_ID,
                TRAN_DEVICE_RESULT_TYPE_CD,
                PAYMENT_SUBTYPE_KEY_ID,
                PAYMENT_SUBTYPE_CLASS,
                CLIENT_PAYMENT_TYPE_CD,
                DEVICE_NAME)
            SELECT
                pn_tran_id, /* TRAN_ID */
                ld_tran_start_ts,  /* TRAN_START_TS */
                ld_tran_start_ts, /* TRAN_END_TS */
                ld_current_ts,
                lv_tran_state_cd,  /* TRAN_STATE_CD */
                pv_device_tran_cd,  /* TRAN_DEVICE_TRAN_CD */
                pv_track_data,  /* TRAN_RECEIVED_RAW_ACCT_DATA */
                pn_pos_pta_id, /* POS_PTA_ID */
                lv_global_trans_cd, /* TRAN_GLOBAL_TRANS_CD */
                pn_consumer_acct_id,  /* CONSUMER_ACCT_ID */
                pv_tran_device_result_type_cd,
                pp.payment_subtype_key_id,
                ps.payment_subtype_class,
                ps.client_payment_type_cd,
                pv_device_name
            FROM pss.pos_pta pp
            JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
            WHERE pp.pos_pta_id = pn_pos_pta_id;
    
            IF pc_sale_type_cd != PKG_CONST.SALE_TYPE__CASH THEN
            SELECT PSS.SEQ_AUTH_ID.NEXTVAL
              INTO ln_auth_id
              FROM DUAL;
            INSERT INTO PSS.AUTH (
                AUTH_ID,
                TRAN_ID,
                AUTH_STATE_ID,
                AUTH_TYPE_CD,
                AUTH_PARSED_ACCT_DATA,
                ACCT_ENTRY_METHOD_CD,
                AUTH_TS,
                AUTH_RESULT_CD,
                AUTH_RESP_CD,
                AUTH_RESP_DESC,
                AUTH_AUTHORITY_TS,
                AUTH_AMT,
                AUTH_AUTHORITY_AMT_RQST,
                TRACE_NUMBER)
             VALUES(
                ln_auth_id, /* AUTH_ID */
                pn_tran_id, /* TRAN_ID */
                DECODE(pc_auth_result_cd, 'Y', 2, 'N', 3, 'P', 5, 'O', 7, 'F', 4, 'R', 7, 'V', 2, 'C', 2, 'M', 2), /* AUTH_STATE_ID */
                'L', /* AUTH_TYPE_CD */
                pv_track_data, /* AUTH_PARSED_ACCT_DATA */
                DECODE(pc_entry_method, 'B', 7, 'C', 6, 'M', 2, 'S', 3, 'I', 8, 1), /* ACCT_ENTRY_METHOD_CD */
                pd_auth_ts, /* AUTH_TS */
                pc_auth_result_cd, /* AUTH_RESULT_CD */
                'LOCAL', /* AUTH_RESP_CD */
                'Local authorization not accepted', /* AUTH_RESP_DESC */
                pd_auth_ts, /* AUTH_AUTHORITY_TS */
                NVL(pn_auth_amt / pn_minor_currency_factor, 0), /* AUTH_AMT */
                pn_auth_amt / pn_minor_currency_factor, /* AUTH_AUTHORITY_AMT_RQST */
                pn_trace_number /* TRACE_NUMBER */
                );
            END IF;
    
            INSERT INTO pss.sale (
                tran_id,
                device_batch_id,
                sale_type_cd,
                sale_start_utc_ts,
                sale_end_utc_ts,
                sale_utc_offset_min,
                sale_result_id,
                sale_amount,
                receipt_result_cd,
                hash_type_cd,
                tran_line_item_hash
            ) VALUES (
                pn_tran_id,
                pn_device_batch_id,
                pc_sale_type_cd,
                lt_sale_start_utc_ts,
                lt_sale_start_utc_ts,
                pn_sale_utc_offset_min,
                pn_sale_result_id,
                pn_auth_amt / pn_minor_currency_factor,
                'U',
                pv_hash_type_cd,
                pv_tran_line_item_hash
            );
        END IF;
    
        pn_result_cd := PKG_CONST.RESULT__SUCCESS;
        pv_error_message := PKG_CONST.ERROR__NO_ERROR;
    END;

    -- R33 - R36 signature
    PROCEDURE SP_CREATE_LOCAL_AUTH_SALE(
       pc_global_event_cd_prefix IN CHAR,
       pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
       pn_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE,
       pv_device_tran_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE,
       pn_sale_utc_ts_ms NUMBER,
       pn_sale_utc_offset_min IN pss.sale.sale_utc_offset_min%TYPE,
       pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
       pc_entry_method CHAR,
       pc_payment_type CHAR,
       pv_track_data PSS.TRAN.TRAN_RECEIVED_RAW_ACCT_DATA%TYPE,
       pv_pan_sha1 VARCHAR2,
       pn_consumer_acct_id PSS.TRAN.CONSUMER_ACCT_ID%TYPE,
       pd_auth_ts PSS.AUTH.AUTH_TS%TYPE,
       pn_trace_number PSS.AUTH.TRACE_NUMBER%TYPE,
       pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
       pn_auth_amt PSS.AUTH.AUTH_AMT%TYPE,
       pn_device_batch_id IN pss.sale.device_batch_id%TYPE,
       pc_sale_type_cd IN pss.sale.sale_type_cd%TYPE,
       pv_tran_device_result_type_cd IN pss.tran.tran_device_result_type_cd%TYPE,
       pn_sale_result_id IN pss.sale.sale_result_id%TYPE,
       pv_hash_type_cd IN pss.sale.hash_type_cd%TYPE,
       pv_tran_line_item_hash IN pss.sale.tran_line_item_hash%TYPE,
       pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
       pn_result_cd OUT NUMBER,
       pv_error_message OUT VARCHAR2
    ) IS
    BEGIN
        SP_CREATE_LOCAL_AUTH_SALE(
           pc_global_event_cd_prefix,
           pv_device_name,
           pn_pos_pta_id,
           pv_device_tran_cd,
           pn_sale_utc_ts_ms,
           pn_sale_utc_offset_min,
           pc_auth_result_cd,
           pc_entry_method,
           pc_payment_type,
           pv_track_data,
           pn_consumer_acct_id,
           pd_auth_ts,
           pn_trace_number,
           pn_minor_currency_factor,
           pn_auth_amt,
           pn_device_batch_id,
           pc_sale_type_cd,
           pv_tran_device_result_type_cd,
           pn_sale_result_id,
           pv_hash_type_cd,
           pv_tran_line_item_hash,
           pn_tran_id,
           pn_result_cd,
           pv_error_message);
    END;

    PROCEDURE SETUP_REPLENISH_CONSUMER_ACCT(
       pn_replenish_id PSS.CONSUMER_ACCT_PEND_REPLENISH.CONSUMER_ACCT_REPLENISH_ID%TYPE,
       pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
       lc_always_auth_flag CHAR,
       pv_replenish_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
       pn_balance PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
       pn_replenish_threshhold PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_THRESHHOLD%TYPE,
       pn_replenish_amount IN OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_AMOUNT%TYPE,
       pc_replenish_flag OUT VARCHAR2,
       pn_replenish_next_master_id OUT NUMBER,
       pv_replenish_device_name OUT DEVICE.DEVICE_NAME%TYPE)
    IS
       ln_balance PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE;
       ln_pending_amount PSS.CONSUMER_ACCT_PEND_REPLENISH.AMOUNT%TYPE;
       ln_auth_hold_total PSS.AUTH.AUTH_AMT_APPROVED%TYPE;
    BEGIN
        SELECT NVL(SUM(A.AUTH_AMT_APPROVED), 0)
          INTO ln_auth_hold_total
          FROM PSS.CONSUMER_ACCT_AUTH_HOLD CAAH
          JOIN PSS.AUTH A ON CAAH.AUTH_ID = A.AUTH_ID
         WHERE CAAH.CONSUMER_ACCT_ID = pn_consumer_acct_id
           AND CAAH.EXPIRATION_TS > SYSDATE
           AND CAAH.CLEARED_YN_FLAG = 'N';
        SELECT NVL(SUM(AMOUNT), 0)
          INTO ln_pending_amount
          FROM PSS.CONSUMER_ACCT_PEND_REPLENISH
         WHERE CONSUMER_ACCT_REPLENISH_ID = pn_replenish_id
           AND EXPIRATION_TS > SYSDATE
           AND (SUBMITTED_FLAG = 'Y' OR (SUBMITTED_FLAG = '?' AND CREATED_UTC_TS > SYS_EXTRACT_UTC(SYSTIMESTAMP) - (1/24/60)));
        ln_balance := pn_balance + ln_pending_amount - ln_auth_hold_total;   
        IF lc_always_auth_flag = 'Y' OR pn_replenish_threshhold IS NULL OR ln_balance < pn_replenish_threshhold THEN
            IF pn_replenish_threshhold IS NOT NULL THEN
                IF pn_replenish_amount + ln_balance < pn_replenish_threshhold THEN
                    pn_replenish_amount := pn_replenish_threshhold - ln_balance;
                ELSIF ln_balance >= pn_replenish_threshhold AND lc_always_auth_flag = 'Y' THEN
                    pn_replenish_amount := 0;
                END IF;
            END IF;
            PKG_DEVICE_CONFIGURATION.NEXT_MASTER_ID_BY_SERIAL(pv_replenish_device_serial_cd, pv_replenish_device_name, pn_replenish_next_master_id);
            INSERT INTO PSS.CONSUMER_ACCT_PEND_REPLENISH(CONSUMER_ACCT_REPLENISH_ID, DEVICE_NAME, DEVICE_TRAN_CD, AMOUNT, EXPIRATION_TS)
                VALUES(pn_replenish_id, pv_replenish_device_name, pn_replenish_next_master_id, pn_replenish_amount, SYSDATE + 7);
            pc_replenish_flag := 'Y';
        ELSE
            pc_replenish_flag := 'N';       
        END IF;
    END;
    
    -- R37 and above
    PROCEDURE DEBIT_CONSUMER_ACCT(
       pn_amount PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
       pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
       pc_redelivery_flag CHAR,
       pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
       pc_auth_result_cd OUT VARCHAR2,
       pn_debitted_amount OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
       pn_new_balance OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
       pc_auto_replenish_flag OUT VARCHAR2,
       pn_used_cash_back_balance OUT PSS.CONSUMER_ACCT.TOWARD_CASH_BACK_BALANCE%TYPE,
       pn_cash_back_amount OUT PSS.SALE.SALE_AMOUNT%TYPE)
    IS
       ln_auth_id PSS.AUTH.AUTH_ID%TYPE;
       ln_auth_amount PSS.AUTH.AUTH_AMT_APPROVED%TYPE;
       ln_auth_hold_used PSS.AUTH.AUTH_HOLD_USED%TYPE;
       ld_tran_start_ts PSS.TRAN.TRAN_START_TS%TYPE;
       ln_loyalty_discount PSS.CONSUMER_ACCT.LOYALTY_DISCOUNT_TOTAL%TYPE;
       ln_consumer_acct_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_TYPE_ID%TYPE;
       ln_consumer_acct_sub_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_SUB_TYPE_ID%TYPE;
       ln_cash_back_percent REPORT.CAMPAIGN.DISCOUNT_PERCENT%TYPE;
       ln_cash_back_threshhold REPORT.CAMPAIGN.THRESHOLD_AMOUNT%TYPE;
       ln_cash_back_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE;
       ln_trans_type_id PSS.PAYMENT_SUBTYPE.TRANS_TYPE_ID%TYPE;
       ln_operator_trans_type_id REPORT.TRANS_TYPE.OPERATOR_TRANS_TYPE_ID%TYPE;
       lv_lock VARCHAR2(128);
    BEGIN
        SELECT ca.CONSUMER_ACCT_ID, A.AUTH_HOLD_USED, A.AUTH_ID, X.TRAN_START_TS, A.AUTH_AMT_APPROVED, NVL(SUM(ABS(XI.TRAN_LINE_ITEM_AMOUNT * XI.TRAN_LINE_ITEM_QUANTITY)), 0), 
               CASE WHEN CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y' AND CA.CONSUMER_ACCT_TYPE_ID = 3 THEN 'Y' ELSE 'N' END, CA.CONSUMER_ACCT_TYPE_ID, PST.TRANS_TYPE_ID, TT.OPERATOR_TRANS_TYPE_ID
          INTO pn_consumer_acct_id, ln_auth_hold_used, ln_auth_id, ld_tran_start_ts, ln_auth_amount, ln_loyalty_discount, pc_auto_replenish_flag, ln_consumer_acct_type_id, ln_trans_type_id, ln_operator_trans_type_id
          FROM PSS.CONSUMER_ACCT ca
          JOIN PSS.TRAN X ON X.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
          JOIN PSS.AUTH A ON X.TRAN_ID = A.TRAN_ID AND A.AUTH_TYPE_CD = 'N' AND A.AUTH_RESULT_CD IN('Y', 'P', 'V', 'C', 'M')
          JOIN PSS.POS_PTA PP ON X.POS_PTA_ID = PP.POS_PTA_ID
          JOIN PSS.PAYMENT_SUBTYPE PST ON PP.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
          JOIN REPORT.TRANS_TYPE TT ON PST.TRANS_TYPE_ID = TT.TRANS_TYPE_ID
          LEFT OUTER JOIN PSS.TRAN_LINE_ITEM XI ON X.TRAN_ID = XI.TRAN_ID AND XI.TRAN_LINE_ITEM_TYPE_ID = 204
         WHERE X.TRAN_ID = pn_tran_id
         GROUP BY ca.CONSUMER_ACCT_ID, A.AUTH_HOLD_USED, A.AUTH_ID, X.TRAN_START_TS, A.AUTH_AMT_APPROVED, CA.CONSUMER_ACCT_ACTIVE_YN_FLAG, CA.CONSUMER_ACCT_TYPE_ID, PST.TRANS_TYPE_ID, TT.OPERATOR_TRANS_TYPE_ID;
        IF pc_auto_replenish_flag = 'Y' THEN
            SELECT NVL(MAX(REPLENISH_FLAG), 'N')
              INTO pc_auto_replenish_flag
              FROM (
                SELECT CASE WHEN CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y' 
                             AND CA.CONSUMER_ACCT_TYPE_ID = 3 
                             AND CAR.REPLENISH_THRESHHOLD > 0
                             AND CAR.REPLENISH_AMOUNT > 0 
                             AND CAR.REPLENISH_CARD_KEY IS NOT NULL THEN 'Y' ELSE 'N' END REPLENISH_FLAG
                  FROM PSS.CONSUMER_ACCT CA
                  JOIN PSS.CONSUMER_ACCT_REPLENISH CAR ON CA.CONSUMER_ACCT_ID = CAR.CONSUMER_ACCT_ID
                 WHERE ca.CONSUMER_ACCT_ID = pn_consumer_acct_id
                   AND CAR.REPLENISH_TYPE_ID = 1
                 ORDER BY CAR.PRIORITY, CAR.CONSUMER_ACCT_REPLENISH_ID)
             WHERE ROWNUM = 1;    
        END IF;
        IF ln_auth_hold_used = 'Y' THEN
            IF ln_consumer_acct_type_id IN(3) THEN
                SELECT MAX(DISCOUNT_PERCENT), MAX(THRESHOLD_AMOUNT), MAX(CAMPAIGN_ID)
                  INTO ln_cash_back_percent, ln_cash_back_threshhold, ln_cash_back_campaign_id
                  FROM (SELECT C.DISCOUNT_PERCENT, C.THRESHOLD_AMOUNT, C.CAMPAIGN_ID
                  FROM REPORT.CAMPAIGN C
                  JOIN PSS.CAMPAIGN_CONSUMER_ACCT CCA on C.CAMPAIGN_ID = CCA.CAMPAIGN_ID
                 WHERE CCA.CONSUMER_ACCT_ID = pn_consumer_acct_id
                   AND ld_tran_start_ts BETWEEN NVL(C.START_DATE, MIN_DATE) AND NVL(C.END_DATE, MAX_DATE)
                   AND C.CAMPAIGN_TYPE_ID = 3 /* Spend reward - Cash back */
                   AND C.DISCOUNT_PERCENT > 0
                   AND C.DISCOUNT_PERCENT < 1
                   AND (TRIM(C.RECUR_SCHEDULE) IS NULL OR REPORT.MATCH_CAMPAIGN_RECUR_SCHEDULE(C.RECUR_SCHEDULE, ld_tran_start_ts) = 'Y')
                 ORDER BY C.DISCOUNT_PERCENT DESC, C.CAMPAIGN_ID DESC)
                 WHERE ROWNUM = 1;
            END IF;
            lv_lock := PKG_GLOBAL.REQUEST_LOCK('PSS.CONSUMER_ACCT', pn_consumer_acct_id);
            DELETE 
              FROM PSS.CONSUMER_ACCT_AUTH_HOLD
             WHERE AUTH_ID = ln_auth_id
               AND EXPIRATION_TS > SYSDATE 
               AND CLEARED_YN_FLAG = 'N';
            IF SQL%FOUND THEN
                -- debit CONSUMER_ACCT_PROMO_BALANCE first and then CONSUMER_ACCT_REPLEN_BALANCE
                UPDATE PSS.CONSUMER_ACCT
                   SET CONSUMER_ACCT_BALANCE = CONSUMER_ACCT_BALANCE - pn_amount,
                       LOYALTY_DISCOUNT_TOTAL = NVL(LOYALTY_DISCOUNT_TOTAL, 0) + ln_loyalty_discount,
                       CONSUMER_ACCT_PROMO_BALANCE = CONSUMER_ACCT_PROMO_BALANCE -
                            CASE WHEN CONSUMER_ACCT_PROMO_BALANCE > 0 AND CONSUMER_ACCT_PROMO_BALANCE >= pn_amount THEN pn_amount ELSE CONSUMER_ACCT_PROMO_BALANCE END,
                       CONSUMER_ACCT_REPLEN_BALANCE = CONSUMER_ACCT_REPLEN_BALANCE - 
                            CASE WHEN CONSUMER_ACCT_PROMO_BALANCE > 0 AND CONSUMER_ACCT_PROMO_BALANCE >= pn_amount THEN 0
                                WHEN CONSUMER_ACCT_REPLEN_BALANCE > 0 AND CONSUMER_ACCT_REPLEN_BALANCE >= pn_amount - CONSUMER_ACCT_PROMO_BALANCE THEN pn_amount - CONSUMER_ACCT_PROMO_BALANCE
                                ELSE CONSUMER_ACCT_REPLEN_BALANCE END
                 WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id
                  RETURNING CONSUMER_ACCT_BALANCE, CONSUMER_ACCT_SUB_TYPE_ID 
                  INTO pn_new_balance, ln_consumer_acct_sub_type_id;
                IF pn_new_balance < 0 THEN
                    IF ln_auth_amount < pn_amount THEN
                        RAISE_APPLICATION_ERROR(-20113, 'Sale amount (' || pn_amount || ') is greater than auth amount (' || ln_auth_amount || ') for transaction ' || pn_tran_id);
                    END IF;
                    RAISE_APPLICATION_ERROR(-20112, 'Insufficient Funds (' || pn_new_balance || ')'); 
                END IF;
                pc_auth_result_cd := 'Y';
                UPDATE PSS.AUTH SA 
                   SET AUTH_STATE_ID = 2,
                       AUTH_RESULT_CD = pc_auth_result_cd,
                       OVERRIDE_TRANS_TYPE_ID = CASE WHEN ln_consumer_acct_sub_type_id = 2 THEN ln_operator_trans_type_id ELSE ln_trans_type_id END
                 WHERE SA.TRAN_ID = pn_tran_id
                   AND SA.AUTH_TYPE_CD IN('U', 'C')
                   AND SA.AUTH_STATE_ID = 6;
                IF SQL%ROWCOUNT != 1 THEN
                    RAISE_APPLICATION_ERROR(-20116, 'Auth Hold Inconsistent State for transaction ' || pn_tran_id);
                END IF;
                IF ln_cash_back_campaign_id IS NOT NULL AND ln_cash_back_percent > 0 THEN
                    DECLARE
                        ln_cash_back_balance PSS.CONSUMER_ACCT.TOWARD_CASH_BACK_BALANCE%TYPE;
                        lv_cash_back_device_name DEVICE.DEVICE_NAME%TYPE;
                        ln_cash_back_next_master_id NUMBER;
                        lv_currency_cd PSS.CONSUMER_ACCT.CURRENCY_CD%TYPE;
                        ln_result_cd NUMBER;
                        lv_error_message VARCHAR2(4000);
                        ln_cash_back_tran_id PSS.TRAN.TRAN_ID%TYPE;
                        lv_cash_back_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
                        ln_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE;
                        lc_currency_symbol CORP.CURRENCY.CURRENCY_SYMBOL%TYPE;
                        ln_host_id HOST.HOST_ID%TYPE;
                        ld_cash_back_ts DATE;
                        ld_cash_back_time NUMBER;
                        ln_corp_customer_id PSS.CONSUMER_ACCT.CORP_CUSTOMER_ID%TYPE;
                        lv_consumer_acct_cd PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD%TYPE;
                        lv_consumer_acct_identifier PSS.CONSUMER_ACCT.CONSUMER_ACCT_IDENTIFIER%TYPE;
                        lv_desc PSS.TRAN_LINE_ITEM.TRAN_LINE_ITEM_DESC%TYPE;
                        ln_doc_id CORP.DOC.DOC_ID%TYPE;
                        ln_ledger_id CORP.LEDGER.LEDGER_ID%TYPE;                        
                    BEGIN
                        -- update consumer acct
                        UPDATE PSS.CONSUMER_ACCT
                           SET TOWARD_CASH_BACK_BALANCE = NVL(TOWARD_CASH_BACK_BALANCE, 0) + pn_amount
                         WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id
                         RETURNING TOWARD_CASH_BACK_BALANCE, CURRENCY_CD, CORP_CUSTOMER_ID, CONSUMER_ACCT_CD, CONSUMER_ACCT_IDENTIFIER
                          INTO ln_cash_back_balance, lv_currency_cd, ln_corp_customer_id, lv_consumer_acct_cd, lv_consumer_acct_identifier;
                        IF ln_cash_back_balance >= ln_cash_back_threshhold THEN 
                            pn_used_cash_back_balance := TRUNC(ln_cash_back_balance / ln_cash_back_threshhold) * ln_cash_back_threshhold;
                            pn_cash_back_amount := ROUND(pn_used_cash_back_balance * ln_cash_back_percent, 2);
                            UPDATE PSS.CONSUMER_ACCT
                               SET TOWARD_CASH_BACK_BALANCE = TOWARD_CASH_BACK_BALANCE - pn_used_cash_back_balance,
                                   CASH_BACK_TOTAL = NVL(CASH_BACK_TOTAL, 0) + pn_cash_back_amount,
                                   CONSUMER_ACCT_BALANCE = CONSUMER_ACCT_BALANCE + pn_cash_back_amount,
                                   CONSUMER_ACCT_PROMO_BALANCE = CONSUMER_ACCT_PROMO_BALANCE + pn_cash_back_amount,
                                   CONSUMER_ACCT_PROMO_TOTAL = CONSUMER_ACCT_PROMO_TOTAL + pn_cash_back_amount
                             WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id;                         
                            -- add trans to virtual terminal
                            SELECT C.MINOR_CURRENCY_FACTOR, NVL(CC.CURRENCY_SYMBOL, '$'), DBADMIN.TIMESTAMP_TO_MILLIS(SYS_EXTRACT_UTC(SYSTIMESTAMP)), SYSDATE
                              INTO ln_minor_currency_factor, lc_currency_symbol, ld_cash_back_time, ld_cash_back_ts
                              FROM PSS.CURRENCY C
                              LEFT OUTER JOIN CORP.CURRENCY CC ON C.CURRENCY_CD = CC.CURRENCY_CODE
                             WHERE C.CURRENCY_CD = lv_currency_cd;
                             
                            PKG_DEVICE_CONFIGURATION.NEXT_MASTER_ID_BY_SERIAL('V1-' || ln_corp_customer_id || '-' || lv_currency_cd, lv_cash_back_device_name, ln_cash_back_next_master_id);
                            SP_CREATE_SALE('A', lv_cash_back_device_name, ln_cash_back_next_master_id,  0, 'C', ld_cash_back_time, 
                                DBADMIN.PKG_UTL.TIME_ZONE_DIFF_MIN(PKG_CONST.DB_TIME_ZONE, PKG_CONST.GMT_TIME_ZONE), 'S', 0, pn_cash_back_amount * ln_minor_currency_factor, 'U', 'A', 'SHA1', 
                                DBADMIN.HASH_CARD('Bonus Cash on ' || pn_consumer_acct_id || ' of ' || pn_cash_back_amount),
                                NULL, ln_result_cd, lv_error_message, ln_cash_back_tran_id, lv_cash_back_tran_state_cd);
                            IF ln_result_cd != PKG_CONST.RESULT__SUCCESS THEN
                                RAISE_APPLICATION_ERROR(-20118, 'Could not create cash back transaction: ' || lv_error_message);
                            END IF;
                            SELECT HOST_ID, 'Bonus Cash for ' || lc_currency_symbol || TO_NUMBER(pn_used_cash_back_balance, 'FM9,999,999.00') || ' in purchases'
                              INTO ln_host_id, lv_desc
                              FROM (SELECT H.HOST_ID
                                      FROM DEVICE.HOST H
                                      JOIN DEVICE.DEVICE D ON H.DEVICE_ID = D.DEVICE_ID
                                     WHERE D.DEVICE_ACTIVE_YN_FLAG = 'Y'
                                       AND H.HOST_PORT_NUM IN(0,1)
                                       AND D.DEVICE_NAME = lv_cash_back_device_name
                                       AND H.HOST_ACTIVE_YN_FLAG = 'Y'
                                     ORDER BY H.HOST_PORT_NUM DESC, H.HOST_ID DESC) 
                             WHERE ROWNUM = 1;
                            INSERT INTO PSS.TRAN_LINE_ITEM(
                                TRAN_ID,
                                TRAN_LINE_ITEM_AMOUNT,
                                TRAN_LINE_ITEM_POSITION_CD,
                                TRAN_LINE_ITEM_TAX,
                                TRAN_LINE_ITEM_TYPE_ID,
                                TRAN_LINE_ITEM_QUANTITY,
                                TRAN_LINE_ITEM_DESC,
                                HOST_ID,
                                TRAN_LINE_ITEM_BATCH_TYPE_CD,
                                TRAN_LINE_ITEM_TS,
                                SALE_RESULT_ID,
                                APPLY_TO_CONSUMER_ACCT_ID,
                                CAMPAIGN_ID)
                            SELECT
                                ln_cash_back_tran_id,
                                pn_used_cash_back_balance,
                                NULL,
                                NULL,
                                554,
                                ln_cash_back_percent,
                                lv_desc,
                                ln_host_id,
                                'A',
                                ld_cash_back_ts,
                                0,
                                pn_consumer_acct_id,
                                ln_cash_back_campaign_id
                            FROM DUAL;
                            UPDATE PSS.TRAN
                               SET PARENT_TRAN_ID = pn_tran_id
                             WHERE TRAN_ID = ln_cash_back_tran_id;
                            IF ln_consumer_acct_sub_type_id = 1 THEN
                                -- add adjustment to ledger
                                CORP.PAYMENTS_PKG.ADJUSTMENT_INS(ln_corp_customer_id, 'Bonus Cash Processing', lv_currency_cd, lv_desc || ', card # ' || lv_consumer_acct_cd || ', card ID ' || lv_consumer_acct_identifier,
                                    -pn_cash_back_amount, ln_doc_id, ln_ledger_id);
                            END IF;
                        END IF; 
                    END;
                END IF;
            ELSE
                SELECT MAX(AUTH_RESULT_CD)
                  INTO pc_auth_result_cd
                  FROM PSS.AUTH SA 
                 WHERE SA.TRAN_ID = pn_tran_id
                   AND SA.AUTH_TYPE_CD IN('U', 'C')
                   AND SA.AUTH_STATE_ID = 2;
                IF pc_auth_result_cd IS NULL THEN
                    DECLARE
                        ld_expiration_ts PSS.CONSUMER_ACCT_AUTH_HOLD.EXPIRATION_TS%TYPE;
                        lc_cleared_flag PSS.CONSUMER_ACCT_AUTH_HOLD.CLEARED_YN_FLAG%TYPE;
                    BEGIN
                        SELECT EXPIRATION_TS, CLEARED_YN_FLAG
                          INTO ld_expiration_ts, lc_cleared_flag
                          FROM PSS.CONSUMER_ACCT_AUTH_HOLD
                         WHERE AUTH_ID = ln_auth_id;
                        IF lc_cleared_flag != 'N' THEN
                            RAISE_APPLICATION_ERROR(-20114, 'Auth Hold Was Cleared for transaction ' || pn_tran_id);
                        ELSIF ld_expiration_ts <= SYSDATE THEN
                            RAISE_APPLICATION_ERROR(-20115, 'Auth Hold Has Expired for transaction ' || pn_tran_id);
                        ELSE
                            RAISE_APPLICATION_ERROR(-20116, 'Auth Hold Inconsistent State for transaction ' || pn_tran_id);
                        END IF;
                    EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                            RAISE_APPLICATION_ERROR(-20111, 'Auth Hold Not Found for transaction ' || pn_tran_id);
                    END;
                END IF;
            END IF;
            pn_debitted_amount := pn_amount;
        ELSE
            pn_debitted_amount := 0;
            pc_auto_replenish_flag := 'N';
        END IF;
    END;
    
    -- R36 and below
    PROCEDURE DEBIT_CONSUMER_ACCT(
       pn_amount PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
       pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
       pc_redelivery_flag CHAR,
       pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
       pn_debitted_amount OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
       pn_new_balance OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
       pc_auto_replenish_flag OUT VARCHAR2,
       pn_used_cash_back_balance OUT PSS.CONSUMER_ACCT.TOWARD_CASH_BACK_BALANCE%TYPE,
       pn_cash_back_amount OUT PSS.SALE.SALE_AMOUNT%TYPE)
    IS
        ln_auth_result_cd VARCHAR2(1);     
    BEGIN
        DEBIT_CONSUMER_ACCT(
           pn_amount,
           pn_tran_id,
           pc_redelivery_flag,
           pn_consumer_acct_id,
           ln_auth_result_cd,
           pn_debitted_amount,
           pn_new_balance,
           pc_auto_replenish_flag,
           pn_used_cash_back_balance,
           pn_cash_back_amount);
    END;
    
    -- R37 and above
    PROCEDURE CREDIT_CONSUMER_ACCT(
       pn_amount PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
       pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
       pc_redelivery_flag CHAR,
       pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
       pc_auth_result_cd OUT VARCHAR2,
       pn_creditted_amount OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
       pn_new_balance OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE)
    IS
       ln_refund_id PSS.REFUND.REFUND_ID%TYPE;
       ln_refund_state_id PSS.REFUND.REFUND_STATE_ID%TYPE;
       ln_consumer_acct_sub_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_SUB_TYPE_ID%TYPE;
       ln_override_trans_type_id PSS.REFUND.OVERRIDE_TRANS_TYPE_ID%TYPE;
       lv_lock VARCHAR2(128);
    BEGIN
        SELECT ca.CONSUMER_ACCT_ID, R.REFUND_ID, R.REFUND_STATE_ID, CA.CONSUMER_ACCT_SUB_TYPE_ID
          INTO pn_consumer_acct_id, ln_refund_id, ln_refund_state_id, ln_consumer_acct_sub_type_id
          FROM PSS.CONSUMER_ACCT ca
          JOIN PSS.TRAN X ON X.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
          JOIN PSS.REFUND R ON X.TRAN_ID = R.TRAN_ID
         WHERE X.TRAN_ID = pn_tran_id;
        
        IF ln_refund_state_id IN(2,3,4,6) THEN
            pc_auth_result_cd := 'Y';
            IF ln_consumer_acct_sub_type_id IS NOT NULL THEN
                IF ln_consumer_acct_sub_type_id = 2 THEN
                    ln_override_trans_type_id := 31;
                ELSE
                    ln_override_trans_type_id := 20;
                END IF;
            END IF;                 
            lv_lock := PKG_GLOBAL.REQUEST_LOCK('PSS.CONSUMER_ACCT', pn_consumer_acct_id);
            UPDATE PSS.REFUND
               SET REFUND_STATE_ID = 1,
                   OVERRIDE_TRANS_TYPE_ID = ln_override_trans_type_id
             WHERE REFUND_ID = ln_refund_id
               AND REFUND_STATE_ID = ln_refund_state_id;
            IF SQL%FOUND THEN
                UPDATE PSS.CONSUMER_ACCT
                   SET CONSUMER_ACCT_BALANCE = CONSUMER_ACCT_BALANCE + pn_amount,
                       CONSUMER_ACCT_REPLEN_BALANCE = CONSUMER_ACCT_REPLEN_BALANCE + pn_amount,
                       TOWARD_CASH_BACK_BALANCE = CASE WHEN TOWARD_CASH_BACK_BALANCE IS NOT NULL AND CONSUMER_ACCT_TYPE_ID = 3 THEN TOWARD_CASH_BACK_BALANCE - pn_amount ELSE TOWARD_CASH_BACK_BALANCE END
                 WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id
                   AND NVL(CONSUMER_ACCT_SUB_TYPE_ID, 0) = NVL(ln_consumer_acct_sub_type_id, 0)
                 RETURNING CONSUMER_ACCT_BALANCE INTO pn_new_balance;
                IF NOT SQL%FOUND THEN
                    RAISE_APPLICATION_ERROR(-20116, 'Auth Hold Inconsistent State for transaction ' || pn_tran_id);
                END IF;
            ELSE
                RAISE_APPLICATION_ERROR(-20116, 'Auth Hold Inconsistent State for transaction ' || pn_tran_id);
            END IF;
            pn_creditted_amount := pn_amount;
        ELSIF pc_redelivery_flag != 'Y' THEN
            RAISE_APPLICATION_ERROR(-20111, 'Pending refund not found for transaction ' || pn_tran_id);
        ELSE
            pc_auth_result_cd := 'Y';
            pn_creditted_amount := pn_amount;
        END IF;
    END;
    
    -- R36 and below
    PROCEDURE CREDIT_CONSUMER_ACCT(
        pn_amount PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pc_redelivery_flag CHAR,
        pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
        pn_creditted_amount OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
        pn_new_balance OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE)
    IS
        ln_auth_result_cd VARCHAR2(1);  
    BEGIN
        CREDIT_CONSUMER_ACCT(pn_amount,
            pn_tran_id,
            pc_redelivery_flag,
            pn_consumer_acct_id,
            ln_auth_result_cd,
            pn_creditted_amount,
            pn_new_balance);
    END;  

    -- For R37 and above
    PROCEDURE CHECK_REPLENISH_CONSUMER_ACCT(
       pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
       pv_replenish_device_serial_cd OUT DEVICE.DEVICE_SERIAL_CD%TYPE,
       pc_auto_replenish_flag OUT VARCHAR2,
       pn_replenish_id OUT PSS.CONSUMER_ACCT_REPLENISH.CONSUMER_ACCT_REPLENISH_ID%TYPE,
       pn_replenish_amount OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_AMOUNT%TYPE,
       pv_replenish_card_key OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_CARD_KEY%TYPE,
       pn_global_account_id OUT PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
       pn_consumer_id OUT PSS.CONSUMER_ACCT.CONSUMER_ID%TYPE,
       pn_replenish_next_master_id OUT NUMBER,
       pv_replenish_device_name OUT DEVICE.DEVICE_NAME%TYPE,
       pn_max_denied_count PLS_INTEGER DEFAULT -1)
    IS
       ln_replenish_threshhold PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_THRESHHOLD%TYPE;
       lv_lock VARCHAR2(128);
       CURSOR l_cur IS
            SELECT CAR.CONSUMER_ACCT_REPLENISH_ID, CAR.REPLENISH_TYPE_ID,
                   DECODE(CAR.REPLENISH_REQUESTED_FLAG, 'Y', NULL, CAR.REPLENISH_THRESHHOLD) REPLENISH_THRESHHOLD, 
                   CAR.REPLENISH_AMOUNT, CAR.REPLENISH_CARD_KEY, CA.CONSUMER_ACCT_BALANCE, CA.CURRENCY_CD,
                   'V1-' || CASE
                        WHEN CA.CONSUMER_ACCT_SUB_TYPE_ID = 2 THEN CA.CORP_CUSTOMER_ID
                        ELSE 1 
                    END || '-' || CA.CURRENCY_CD REPLENISH_DEVICE_SERIAL_CD,
                    CA.CONSUMER_ID,
                    CAB.GLOBAL_ACCOUNT_ID
              FROM PSS.CONSUMER_ACCT CA
              JOIN PSS.CONSUMER_ACCT_BASE CAB ON CA.CONSUMER_ACCT_ID = CAB.CONSUMER_ACCT_ID
              JOIN PSS.CONSUMER_ACCT_REPLENISH CAR ON CA.CONSUMER_ACCT_ID = CAR.CONSUMER_ACCT_ID
             WHERE ca.CONSUMER_ACCT_ID = pn_consumer_acct_id
               AND (CAR.REPLENISH_TYPE_ID = 1 OR CAR.REPLENISH_REQUESTED_FLAG = 'Y')
               AND (pn_max_denied_count < 1 OR CAR.REPLENISH_DENIED_COUNT < pn_max_denied_count OR CAR.REPLENISH_REQUESTED_FLAG = 'Y')
               AND CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y' 
               AND CA.CONSUMER_ACCT_TYPE_ID = 3 
               AND (CAR.REPLENISH_THRESHHOLD > 0 OR CAR.REPLENISH_REQUESTED_FLAG = 'Y')
               AND CAR.REPLENISH_AMOUNT > 0 
               AND CAR.REPLENISH_CARD_KEY IS NOT NULL
             ORDER BY CAR.PRIORITY, CAR.CONSUMER_ACCT_REPLENISH_ID;
    BEGIN
        lv_lock := PKG_GLOBAL.REQUEST_LOCK('PSS.CONSUMER_ACCT', pn_consumer_acct_id);
        pc_auto_replenish_flag := 'N';
        FOR l_rec IN l_cur LOOP
            UPDATE PSS.CONSUMER_ACCT_REPLENISH
               SET REPLENISH_REQUESTED_FLAG = 'N'
             WHERE CONSUMER_ACCT_REPLENISH_ID = l_rec.CONSUMER_ACCT_REPLENISH_ID
               AND REPLENISH_REQUESTED_FLAG = 'Y';
            IF SQL%FOUND THEN
                ln_replenish_threshhold := NULL; -- ignore threshhold
            ELSIF l_rec.REPLENISH_TYPE_ID != 1 THEN
                pc_auto_replenish_flag := 'N';
                CONTINUE;
            ELSE
                ln_replenish_threshhold := l_rec.REPLENISH_THRESHHOLD;
            END IF;
            pn_replenish_amount := l_rec.REPLENISH_AMOUNT;
            pn_global_account_id := l_rec.GLOBAL_ACCOUNT_ID;
            pn_consumer_id := l_rec.CONSUMER_ID;
            
            SETUP_REPLENISH_CONSUMER_ACCT(
                   l_rec.CONSUMER_ACCT_REPLENISH_ID,
                   pn_consumer_acct_id,
                   'N',
                   l_rec.REPLENISH_DEVICE_SERIAL_CD,
                   l_rec.CONSUMER_ACCT_BALANCE,
                   ln_replenish_threshhold,
                   pn_replenish_amount,
                   pc_auto_replenish_flag,
                   pn_replenish_next_master_id,
                   pv_replenish_device_name);
            IF pc_auto_replenish_flag = 'Y' THEN
                pn_replenish_id := l_rec.CONSUMER_ACCT_REPLENISH_ID;
                pv_replenish_card_key := l_rec.REPLENISH_CARD_KEY;
                pv_replenish_device_serial_cd := l_rec.REPLENISH_DEVICE_SERIAL_CD;
                RETURN;
            END IF;
        END LOOP;
    END;
    
    -- Not needed after R37
    PROCEDURE CHECK_REPLENISH_CONSUMER_ACCT(
       pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
       pv_replenish_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
       pc_auto_replenish_flag OUT VARCHAR2,
       pn_replenish_id OUT PSS.CONSUMER_ACCT_REPLENISH.CONSUMER_ACCT_REPLENISH_ID%TYPE,
       pn_replenish_amount OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_AMOUNT%TYPE,
       pv_replenish_card_key OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_CARD_KEY%TYPE,
       pb_consumer_acct_cd_hash OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD_HASH%TYPE,
       pn_replenish_next_master_id OUT NUMBER,
       pv_replenish_device_name OUT DEVICE.DEVICE_NAME%TYPE,
       pn_max_denied_count PLS_INTEGER DEFAULT -1)
    IS
       ln_balance PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE;
       ln_replenish_threshhold PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_THRESHHOLD%TYPE;
       lv_currency_cd PSS.CONSUMER_ACCT.CURRENCY_CD%TYPE;
       ln_replenish_type_id PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_TYPE_ID%TYPE;
       lv_lock VARCHAR2(128);
       lv_replenish_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE;
       CURSOR l_cur IS
            SELECT CAR.CONSUMER_ACCT_REPLENISH_ID, CAR.REPLENISH_TYPE_ID, CA.CONSUMER_ACCT_CD_HASH, 
                   DECODE(CAR.REPLENISH_REQUESTED_FLAG, 'Y', NULL, CAR.REPLENISH_THRESHHOLD) REPLENISH_THRESHHOLD, 
                   CAR.REPLENISH_AMOUNT, CAR.REPLENISH_CARD_KEY, CA.CONSUMER_ACCT_BALANCE, CA.CURRENCY_CD,
                   CASE
                        WHEN CA.CONSUMER_ACCT_SUB_TYPE_ID = 2 THEN 'V1-' || CA.CORP_CUSTOMER_ID
                        ELSE pv_replenish_device_serial_cd 
                    END || '-' || CA.CURRENCY_CD REPLENISH_DEVICE_SERIAL_CD
              FROM PSS.CONSUMER_ACCT CA
              JOIN PSS.CONSUMER_ACCT_REPLENISH CAR ON CA.CONSUMER_ACCT_ID = CAR.CONSUMER_ACCT_ID
             WHERE ca.CONSUMER_ACCT_ID = pn_consumer_acct_id
               AND (CAR.REPLENISH_TYPE_ID = 1 OR CAR.REPLENISH_REQUESTED_FLAG = 'Y')
               AND (pn_max_denied_count < 1 OR CAR.REPLENISH_DENIED_COUNT < pn_max_denied_count OR CAR.REPLENISH_REQUESTED_FLAG = 'Y')
               AND CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y' 
               AND CA.CONSUMER_ACCT_TYPE_ID = 3 
               AND (CAR.REPLENISH_THRESHHOLD > 0 OR CAR.REPLENISH_REQUESTED_FLAG = 'Y')
               AND CAR.REPLENISH_AMOUNT > 0 
               AND CAR.REPLENISH_CARD_KEY IS NOT NULL
             ORDER BY CAR.PRIORITY, CAR.CONSUMER_ACCT_REPLENISH_ID;
    BEGIN
        lv_lock := PKG_GLOBAL.REQUEST_LOCK('PSS.CONSUMER_ACCT', pn_consumer_acct_id);
        pc_auto_replenish_flag := 'N';
        FOR l_rec IN l_cur LOOP
            UPDATE PSS.CONSUMER_ACCT_REPLENISH
               SET REPLENISH_REQUESTED_FLAG = 'N'
             WHERE CONSUMER_ACCT_REPLENISH_ID = l_rec.CONSUMER_ACCT_REPLENISH_ID
               AND REPLENISH_REQUESTED_FLAG = 'Y';
            IF SQL%FOUND THEN
                ln_replenish_threshhold := NULL; -- ignore threshhold
            ELSIF l_rec.REPLENISH_TYPE_ID != 1 THEN
                pc_auto_replenish_flag := 'N';
                CONTINUE;
            ELSE
                ln_replenish_threshhold := l_rec.REPLENISH_THRESHHOLD;
            END IF;
            pn_replenish_amount := l_rec.REPLENISH_AMOUNT;
            SETUP_REPLENISH_CONSUMER_ACCT(
                   l_rec.CONSUMER_ACCT_REPLENISH_ID,
                   pn_consumer_acct_id,
                   'N',
                   l_rec.REPLENISH_DEVICE_SERIAL_CD,
                   l_rec.CONSUMER_ACCT_BALANCE,
                   ln_replenish_threshhold,
                   pn_replenish_amount,
                   pc_auto_replenish_flag,
                   pn_replenish_next_master_id,
                   pv_replenish_device_name);
            IF pc_auto_replenish_flag = 'Y' THEN
                pn_replenish_id := l_rec.CONSUMER_ACCT_REPLENISH_ID;
                pv_replenish_card_key := l_rec.REPLENISH_CARD_KEY;
                pb_consumer_acct_cd_hash := l_rec.CONSUMER_ACCT_CD_HASH;
                RETURN;
            END IF;
        END LOOP;
    END;
    
    -- R33 Signature
    PROCEDURE LOCK_AUTH_HOLD(
        pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
        pn_ignore_auth_id PSS.AUTH.AUTH_ID%TYPE,
        pn_auth_hold_total OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE)
    IS
        lv_lock VARCHAR2(128);
    BEGIN
        lv_lock := PKG_GLOBAL.REQUEST_LOCK('PSS.CONSUMER_ACCT', pn_consumer_acct_id);
        SELECT NVL(SUM(A.AUTH_AMT_APPROVED), 0)
          INTO pn_auth_hold_total
          FROM PSS.CONSUMER_ACCT_AUTH_HOLD CAAH
          JOIN PSS.AUTH A ON CAAH.AUTH_ID = A.AUTH_ID
         WHERE CAAH.CONSUMER_ACCT_ID = pn_consumer_acct_id
           AND CAAH.EXPIRATION_TS > SYSDATE
           AND CAAH.CLEARED_YN_FLAG = 'N'
           AND (pn_ignore_auth_id IS NULL OR A.AUTH_ID != pn_ignore_auth_id);
    END;
    
    PROCEDURE SETUP_REPLENISH(
        pn_replenish_id IN OUT PSS.CONSUMER_ACCT_REPLENISH.CONSUMER_ACCT_REPLENISH_ID%TYPE,
        pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
        pn_replenish_type_id PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_TYPE_ID%TYPE,
        pv_replenish_card_masked PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_CARD_MASKED%TYPE,
        pv_replenish_device_serial_cd IN OUT DEVICE.DEVICE_SERIAL_CD%TYPE, -- From R37 on we ignore the provided device_serial_cd
        pn_replenish_threshhold PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_THRESHHOLD%TYPE,
        pn_replenish_amount IN OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_AMOUNT%TYPE,
        pc_replenish_flag OUT VARCHAR2,
        pn_replenish_next_master_id OUT NUMBER,
        pv_replenish_device_name OUT DEVICE.DEVICE_NAME%TYPE)
    IS
        lv_lock VARCHAR2(128);
        ln_balance PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE;
        ln_priority PSS.CONSUMER_ACCT_REPLENISH.PRIORITY%TYPE;
        lc_always_auth_flag CHAR(1);
    BEGIN
        lv_lock := PKG_GLOBAL.REQUEST_LOCK('PSS.CONSUMER_ACCT', pn_consumer_acct_id);
        SELECT CONSUMER_ACCT_BALANCE, 'V1-' || CASE 
                WHEN CONSUMER_ACCT_SUB_TYPE_ID = 2 THEN CORP_CUSTOMER_ID
                ELSE 1 
            END || '-' || CURRENCY_CD
          INTO ln_balance, pv_replenish_device_serial_cd
          FROM PSS.CONSUMER_ACCT
         WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id;       
        IF pn_replenish_id IS NULL THEN
            SELECT PSS.SEQ_CONSUMER_ACCT_REPLENISH_ID.NEXTVAL, PRIORITY
              INTO pn_replenish_id, ln_priority
              FROM (SELECT NVL(MAX(PRIORITY), 0) + 1 PRIORITY
              FROM PSS.CONSUMER_ACCT_REPLENISH
             WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id);
            INSERT INTO PSS.CONSUMER_ACCT_REPLENISH(CONSUMER_ACCT_REPLENISH_ID, CONSUMER_ACCT_ID, PRIORITY, REPLENISH_TYPE_ID, REPLENISH_CARD_MASKED, REPLENISH_AMOUNT, REPLENISH_THRESHHOLD)
                VALUES(pn_replenish_id, pn_consumer_acct_id, ln_priority, pn_replenish_type_id, pv_replenish_card_masked, pn_replenish_amount, pn_replenish_threshhold);
            lc_always_auth_flag := 'Y';
        ELSE
            UPDATE PSS.CONSUMER_ACCT_REPLENISH CAR
               SET CAR.REPLENISH_THRESHHOLD = pn_replenish_threshhold,
                   CAR.REPLENISH_AMOUNT = pn_replenish_amount,
                   CAR.REPLENISH_TYPE_ID = pn_replenish_type_id
             WHERE CAR.CONSUMER_ACCT_REPLENISH_ID = pn_replenish_id
               AND CAR.CONSUMER_ACCT_ID = pn_consumer_acct_id;
            IF NOT SQL%FOUND THEN
                RAISE_APPLICATION_ERROR(-20119, 'ReplenishId ' || pn_replenish_id || ' does not belong to consumer acct ' || pn_consumer_acct_id);
            END IF;
            lc_always_auth_flag := 'N';
        END IF;
        SETUP_REPLENISH_CONSUMER_ACCT(
           pn_replenish_id,
           pn_consumer_acct_id,
           lc_always_auth_flag,
           pv_replenish_device_serial_cd,
           ln_balance,
           pn_replenish_threshhold,
           pn_replenish_amount,
           pc_replenish_flag,
           pn_replenish_next_master_id,
           pv_replenish_device_name);
        IF pc_replenish_flag = 'N' THEN
            PKG_DEVICE_CONFIGURATION.NEXT_MASTER_ID_BY_SERIAL(pv_replenish_device_serial_cd, pv_replenish_device_name, pn_replenish_next_master_id);
            pn_replenish_amount := 0;
        END IF;
    END;   
    
    PROCEDURE UPDATE_PENDING_REPLENISH(
        pn_replenish_id PSS.CONSUMER_ACCT_REPLENISH.CONSUMER_ACCT_REPLENISH_ID%TYPE,
        pn_replenish_next_master_id NUMBER,
        pc_submitted_flag PSS.CONSUMER_ACCT_PEND_REPLENISH.SUBMITTED_FLAG%TYPE,
        pc_initial_replenish_flag CHAR,
        pn_denied_count OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_DENIED_COUNT%TYPE,
        pv_replenish_card_masked OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_CARD_MASKED%TYPE)
    IS
        ln_priority PSS.CONSUMER_ACCT_REPLENISH.PRIORITY%TYPE;
        ln_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE;
    BEGIN
        IF pc_initial_replenish_flag = 'Y' THEN
            IF pc_submitted_flag NOT IN('Y', 'P') THEN
                DELETE
                  FROM PSS.CONSUMER_ACCT_PEND_REPLENISH
                 WHERE CONSUMER_ACCT_REPLENISH_ID = pn_replenish_id
                   AND DEVICE_TRAN_CD = pn_replenish_next_master_id;      
                DELETE
                  FROM PSS.CONSUMER_ACCT_REPLENISH
                 WHERE CONSUMER_ACCT_REPLENISH_ID = pn_replenish_id
                RETURNING PRIORITY, CONSUMER_ACCT_ID, REPLENISH_CARD_MASKED 
                  INTO ln_priority, ln_consumer_acct_id, pv_replenish_card_masked;
                IF ln_priority IS NOT NULL AND ln_priority > 0 THEN
                    UPDATE PSS.CONSUMER_ACCT_REPLENISH
                       SET PRIORITY = PRIORITY - 1
                     WHERE CONSUMER_ACCT_ID = ln_consumer_acct_id
                       AND PRIORITY IN(SELECT PRIORITY FROM PSS.CONSUMER_ACCT_REPLENISH WHERE CONSUMER_ACCT_ID = ln_consumer_acct_id CONNECT BY PRIOR PRIORITY = PRIORITY - 1 START WITH PRIORITY = ln_priority + 1);
                END IF;
                pn_denied_count := 1;
            ELSE
                UPDATE PSS.CONSUMER_ACCT_REPLENISH CAR
                   SET PRIORITY = PRIORITY + 1
                 WHERE CONSUMER_ACCT_ID = (SELECT CONSUMER_ACCT_ID FROM PSS.CONSUMER_ACCT_REPLENISH WHERE CONSUMER_ACCT_REPLENISH_ID = pn_replenish_id)
                   AND PRIORITY IN(SELECT CAR0.PRIORITY FROM PSS.CONSUMER_ACCT_REPLENISH CAR0 WHERE CAR0.CONSUMER_ACCT_ID = CAR.CONSUMER_ACCT_ID CONNECT BY PRIOR CAR0.PRIORITY = CAR0.PRIORITY - 1 START WITH CAR0.PRIORITY = 1);
                UPDATE PSS.CONSUMER_ACCT_REPLENISH
                   SET PRIORITY = 1
                 WHERE CONSUMER_ACCT_REPLENISH_ID = pn_replenish_id
                  RETURNING REPLENISH_CARD_MASKED
                  INTO pv_replenish_card_masked;
                UPDATE PSS.CONSUMER_ACCT_PEND_REPLENISH
                   SET SUBMITTED_FLAG = pc_submitted_flag
                 WHERE CONSUMER_ACCT_REPLENISH_ID = pn_replenish_id
                   AND DEVICE_TRAN_CD = pn_replenish_next_master_id; 
                pn_denied_count := 0;
            END IF;
        ELSE
            UPDATE PSS.CONSUMER_ACCT_PEND_REPLENISH
               SET SUBMITTED_FLAG = pc_submitted_flag
             WHERE CONSUMER_ACCT_REPLENISH_ID = pn_replenish_id
               AND DEVICE_TRAN_CD = pn_replenish_next_master_id;
            UPDATE PSS.CONSUMER_ACCT_REPLENISH
               SET REPLENISH_DENIED_COUNT = DECODE(pc_submitted_flag, 'Y', 0, 'P', 0, REPLENISH_DENIED_COUNT + 1)
             WHERE CONSUMER_ACCT_REPLENISH_ID = pn_replenish_id
              RETURNING REPLENISH_DENIED_COUNT, REPLENISH_CARD_MASKED 
              INTO pn_denied_count, pv_replenish_card_masked;
        END IF;
    END;
    
    PROCEDURE CREATE_ISIS_CONSUMER(
        pn_consumer_id OUT PSS.CONSUMER.CONSUMER_ID%TYPE,
        pv_consumer_identifier PSS.CONSUMER.CONSUMER_IDENTIFIER%TYPE)
    IS
    BEGIN
        SELECT PSS.SEQ_CONSUMER_ID.NEXTVAL 
          INTO pn_consumer_id 
          FROM DUAL;
        INSERT INTO PSS.CONSUMER(CONSUMER_ID, CONSUMER_EMAIL_ADDR1, CONSUMER_TYPE_ID, CONSUMER_IDENTIFIER)
            VALUES(pn_consumer_id, 'isis_' || pn_consumer_id || '@usatech.com', 7, pv_consumer_identifier);
    END;
    
    -- R37+ Signature
    PROCEDURE AUTHORIZE_ISIS_PROMO(
        pn_global_account_id IN OUT PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
        pv_instance PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_INSTANCE%TYPE,
        pv_consumer_acct_cd PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD%TYPE,
        pv_consumer_identifier PSS.CONSUMER.CONSUMER_IDENTIFIER%TYPE,
        pv_currency_cd PSS.CURRENCY.CURRENCY_CD%TYPE,
        pv_auth_result_cd OUT VARCHAR2,
        pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
        pn_consumer_id OUT PSS.CONSUMER.CONSUMER_ID%TYPE,
        pn_trans_to_free_tran OUT NUMBER)
    IS
        ln_old_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE;
    BEGIN
        pv_auth_result_cd := 'N';
        pn_consumer_id := NULL;
        
        IF pv_consumer_identifier IS NOT NULL THEN
            SELECT MAX(consumer_id)
              INTO pn_consumer_id
              FROM PSS.CONSUMER
              WHERE CONSUMER_IDENTIFIER = pv_consumer_identifier;
            
            IF pn_consumer_id IS NULL THEN
                BEGIN
                    CREATE_ISIS_CONSUMER(pn_consumer_id, pv_consumer_identifier);
                EXCEPTION
                    WHEN DUP_VAL_ON_INDEX THEN
                        SELECT MAX(consumer_id)
                          INTO pn_consumer_id
                          FROM PSS.CONSUMER
                         WHERE CONSUMER_IDENTIFIER = pv_consumer_identifier;
                END;
            END IF;
        END IF;
    
        IF pn_global_account_id IS NOT NULL THEN
            SELECT MAX(CAB.CONSUMER_ACCT_ID), MAX(CA.CONSUMER_ID)
              INTO pn_consumer_acct_id, ln_old_consumer_id
              FROM PSS.CONSUMER_ACCT_BASE CAB
              LEFT OUTER JOIN PSS.CONSUMER_ACCT CA ON CAB.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
             WHERE CAB.GLOBAL_ACCOUNT_ID = pn_global_account_id
               AND CAB.CURRENCY_CD = pv_currency_cd;
            
            IF pn_consumer_id IS NULL THEN
                pn_consumer_id := ln_old_consumer_id;
        END IF;
        END IF;
        
        IF pn_consumer_acct_id IS NULL OR ln_old_consumer_id IS NULL THEN
        IF pn_consumer_acct_id IS NULL THEN
                IF pn_global_account_id IS NULL THEN
                    SELECT PSS.SEQ_GLOBAL_ACCOUNT_ID_BASE.NEXTVAL
                      INTO pn_global_account_id 
                      FROM DUAL; 
                END IF;
                
            SELECT PSS.SEQ_CONSUMER_ACCT_ID.NEXTVAL
              INTO pn_consumer_acct_id
              FROM DUAL;
                  
                INSERT INTO PSS.CONSUMER_ACCT_BASE(CONSUMER_ACCT_ID, GLOBAL_ACCOUNT_ID, GLOBAL_ACCOUNT_INSTANCE, TRUNCATED_CARD_NUMBER, PAYMENT_SUBTYPE_ID, CURRENCY_CD)
                    VALUES(pn_consumer_acct_id, pn_global_account_id, 0, DBADMIN.MASK_CREDIT_CARD(pv_consumer_acct_cd), 1, pv_currency_cd);
            END IF;
            IF ln_old_consumer_id IS NULL THEN
            IF pn_consumer_id IS NULL THEN
                CREATE_ISIS_CONSUMER(pn_consumer_id, pv_consumer_identifier);
            END IF;
            INSERT INTO PSS.CONSUMER_ACCT(CONSUMER_ACCT_ID, CONSUMER_ACCT_CD, CONSUMER_ACCT_ACTIVE_YN_FLAG, CONSUMER_ACCT_BALANCE, CONSUMER_ID, LOCATION_ID, 
                    CONSUMER_ACCT_ISSUE_NUM, PAYMENT_SUBTYPE_ID, CURRENCY_CD, CONSUMER_ACCT_TYPE_ID) 
                    VALUES(pn_consumer_acct_id, DBADMIN.MASK_CREDIT_CARD(pv_consumer_acct_cd), 'Y', 0, pn_consumer_id, 1, 1, 1, pv_currency_cd, 5);
            END IF;
        ELSIF pn_consumer_id != ln_old_consumer_id THEN
            UPDATE PSS.CONSUMER_ACCT
               SET CONSUMER_ID = pn_consumer_id
             WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id;
        END IF;
    
        UPDATE PSS.CONSUMER_PROMOTION
           SET TRAN_COUNT = tran_count + 1,
               PROMO_TRAN_COUNT = promo_tran_count + 1
         WHERE CONSUMER_ID = pn_consumer_id
           AND PROMOTION_ID = 1
           AND (TRAN_COUNT + 1) / (PROMO_TRAN_COUNT + 1) >= 5
        RETURNING 'Y' INTO pv_auth_result_cd;
            
        LOOP
            BEGIN
                SELECT 4 - MOD(TRAN_COUNT + 1, 5)
                  INTO pn_trans_to_free_tran
                  FROM PSS.CONSUMER_PROMOTION
                 WHERE CONSUMER_ID = pn_consumer_id
                   AND PROMOTION_ID = 1;
                EXIT;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    BEGIN
                        INSERT INTO PSS.CONSUMER_PROMOTION(CONSUMER_ID, PROMOTION_ID)
                            VALUES(pn_consumer_id, 1);
                    EXCEPTION
                        WHEN DUP_VAL_ON_INDEX THEN
                            NULL;
                    END;
            END;
        END LOOP;
    END;

    -- R36 and below Signature
    PROCEDURE AUTHORIZE_ISIS_PROMO(
        pv_consumer_acct_cd PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD%TYPE,
        pr_consumer_acct_cd_hash PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD_HASH%TYPE,
        pv_consumer_identifier PSS.CONSUMER.CONSUMER_IDENTIFIER%TYPE,
        pv_currency_cd PSS.CURRENCY.CURRENCY_CD%TYPE,
        pv_auth_result_cd OUT VARCHAR2,
        pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
        pn_consumer_id OUT PSS.CONSUMER.CONSUMER_ID%TYPE,
        pn_trans_to_free_tran OUT NUMBER)
    IS
        ln_old_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE;
        ln_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE;
    BEGIN
        pv_auth_result_cd := 'N';
        pn_consumer_id := NULL;
        
        IF pv_consumer_identifier IS NOT NULL THEN
            SELECT MAX(consumer_id)
            INTO pn_consumer_id
            FROM pss.consumer
            WHERE consumer_identifier = pv_consumer_identifier;
            
            IF pn_consumer_id IS NULL THEN
                SELECT pss.seq_consumer_id.NEXTVAL INTO pn_consumer_id FROM dual;
                BEGIN
                    INSERT INTO pss.consumer(consumer_id, consumer_email_addr1, consumer_type_id, consumer_identifier)
                    VALUES(pn_consumer_id, 'isis_' || pn_consumer_id || '@usatech.com', 7, pv_consumer_identifier);
                EXCEPTION
                    WHEN DUP_VAL_ON_INDEX THEN
                        SELECT MAX(consumer_id)
                        INTO pn_consumer_id
                        FROM pss.consumer
                        WHERE consumer_identifier = pv_consumer_identifier;
                END;
            END IF;
        END IF;
    
        SELECT MAX(ca.consumer_acct_id), MAX(ca.consumer_id)
        INTO pn_consumer_acct_id, ln_old_consumer_id
        FROM pss.consumer_acct ca
        JOIN pss.consumer c ON ca.consumer_id = c.consumer_id
        WHERE ca.consumer_acct_cd_hash = pr_consumer_acct_cd_hash AND ca.consumer_acct_cd = pv_consumer_acct_cd;
        
        IF pn_consumer_acct_id IS NULL THEN
            IF pn_consumer_id IS NULL THEN
                SELECT pss.seq_consumer_id.NEXTVAL INTO pn_consumer_id FROM dual;
                INSERT INTO pss.consumer(consumer_id, consumer_email_addr1, consumer_type_id)
                VALUES(pn_consumer_id, 'isis_' || pn_consumer_id || '@usatech.com', 7);
            END IF;
        
            SELECT pss.seq_consumer_acct_id.NEXTVAL, PSS.SEQ_GLOBAL_ACCOUNT_ID_BASE.NEXTVAL
              INTO pn_consumer_acct_id, ln_global_account_id
              FROM dual;
            BEGIN
                INSERT INTO PSS.CONSUMER_ACCT_BASE(CONSUMER_ACCT_ID, GLOBAL_ACCOUNT_ID, GLOBAL_ACCOUNT_INSTANCE, TRUNCATED_CARD_NUMBER, PAYMENT_SUBTYPE_ID, CURRENCY_CD)
                    VALUES(pn_consumer_acct_id, ln_global_account_id, 0, DBADMIN.MASK_CREDIT_CARD(pv_consumer_acct_cd), 1, pv_currency_cd);
                INSERT INTO pss.consumer_acct(consumer_acct_id, consumer_acct_cd, consumer_acct_active_yn_flag, 
                    consumer_acct_balance, consumer_id, location_id, consumer_acct_issue_num, payment_subtype_id,                   
                    currency_cd, consumer_acct_type_id, consumer_acct_cd_hash) 
                VALUES(pn_consumer_acct_id, pv_consumer_acct_cd, 'Y', 0, pn_consumer_id, 1, 1, 1, pv_currency_cd, 5, pr_consumer_acct_cd_hash);
            EXCEPTION
                WHEN DUP_VAL_ON_INDEX THEN
                    SELECT MAX(consumer_acct_id), MAX(consumer_id)
                    INTO pn_consumer_acct_id, pn_consumer_id
                    FROM pss.consumer_acct
                    WHERE consumer_acct_cd_hash = pr_consumer_acct_cd_hash AND consumer_acct_cd = pv_consumer_acct_cd;
            END;
        ELSE
            IF pn_consumer_id IS NULL THEN
                pn_consumer_id := ln_old_consumer_id;
            ELSIF pn_consumer_id != ln_old_consumer_id THEN
                UPDATE pss.consumer_acct
                SET consumer_id = pn_consumer_id
                WHERE consumer_acct_id = pn_consumer_acct_id;
            END IF;
        END IF;
    
        UPDATE pss.consumer_promotion
        SET tran_count = tran_count + 1,
            promo_tran_count = promo_tran_count + 1
        WHERE consumer_id = pn_consumer_id
            AND promotion_id = 1
            AND (tran_count + 1) / (promo_tran_count + 1) >= 5
        RETURNING 'Y' INTO pv_auth_result_cd;
            
        LOOP
            BEGIN
                SELECT 4 - MOD(tran_count + 1, 5)
                INTO pn_trans_to_free_tran
                FROM pss.consumer_promotion
                WHERE consumer_id = pn_consumer_id
                    AND promotion_id = 1;
                EXIT;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    BEGIN
                        INSERT INTO pss.consumer_promotion(consumer_id, promotion_id)
                        VALUES(pn_consumer_id, 1);
                    EXCEPTION
                        WHEN DUP_VAL_ON_INDEX THEN
                            NULL;
                    END;
            END;
        END LOOP;
    END;
    
    PROCEDURE REFUND_ISIS_PROMO(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pn_result_cd OUT NUMBER,
        pv_error_message OUT VARCHAR2)
    IS
        ln_refund_state_id PSS.REFUND.REFUND_STATE_ID%TYPE;
        ln_parent_tran_id PSS.TRAN.PARENT_TRAN_ID%TYPE;
        ln_refund_count NUMBER;
        ln_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE;
    BEGIN
        pn_result_cd := PKG_CONST.RESULT__SUCCESS;
        pv_error_message := PKG_CONST.ERROR__NO_ERROR;
        
        SELECT PARENT_TRAN_ID
        INTO ln_parent_tran_id
        FROM PSS.TRAN
        WHERE TRAN_ID = pn_tran_id;
        
        SELECT COUNT(1)
        INTO ln_refund_count
        FROM PSS.REFUND R
        JOIN PSS.TRAN T ON R.TRAN_ID = T.TRAN_ID
        WHERE T.PARENT_TRAN_ID = ln_parent_tran_id AND R.REFUND_STATE_ID IN(1);
        
        IF ln_refund_count > 0 THEN
            RETURN;
        END IF;
        
        SELECT CA.CONSUMER_ID
        INTO ln_consumer_id
        FROM PSS.TRAN T
        JOIN PSS.CONSUMER_ACCT CA ON T.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
        WHERE T.TRAN_ID = pn_tran_id;
        
        UPDATE PSS.CONSUMER_PROMOTION
        SET TRAN_COUNT = CASE WHEN TRAN_COUNT - 1 >= 0 THEN TRAN_COUNT - 1 ELSE 0 END,
            PROMO_TRAN_COUNT = CASE WHEN PROMO_TRAN_COUNT - 1 >= 0 THEN PROMO_TRAN_COUNT - 1 ELSE 0 END
        WHERE CONSUMER_ID = ln_consumer_id AND PROMOTION_ID = 1;
    END;
    
    PROCEDURE GET_OR_CREATE_CONSUMER_ACCT(
        pn_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
        pn_global_account_instance PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_INSTANCE%TYPE,
        pn_pos_pta_id PSS.POS_PTA.POS_PTA_ID%TYPE,
        pv_currency_cd PSS.CONSUMER_ACCT_BASE.CURRENCY_CD%TYPE,
        pd_auth_ts DATE,
        pv_truncated_card_num PSS.CONSUMER_ACCT_BASE.TRUNCATED_CARD_NUMBER%TYPE,
        pn_consumer_acct_id OUT PSS.CONSUMER_ACCT_BASE.CONSUMER_ACCT_ID%TYPE)
    IS
        ln_new_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE;
        ln_new_global_account_instance PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_INSTANCE%TYPE;
    BEGIN
        SELECT MAX(CONSUMER_ACCT_ID)
          INTO pn_consumer_acct_id
          FROM (        
             SELECT CAB.CONSUMER_ACCT_ID  
               FROM (
                SELECT CAB.CONSUMER_ACCT_ID,
                       CASE WHEN CA.CONSUMER_ACCT_ACTIVE_YN_FLAG IS NULL THEN 5 WHEN CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y' AND NVL(CA.CONSUMER_ACCT_ACTIVATION_TS, MIN_DATE) <= pd_auth_ts AND NVL(CA.CONSUMER_ACCT_DEACTIVATION_TS, MAX_DATE) > pd_auth_ts THEN 1 ELSE 10 END CA_PRIORITY,
                       CAB.PAYMENT_SUBTYPE_ID,
                       CAB.LOCATION_ID,
                       CASE WHEN PTA.PAYMENT_SUBTYPE_ID = CAB.PAYMENT_SUBTYPE_ID THEN 1 ELSE 10 END PST_PRIORITY,
                       POS.LOCATION_ID POS_LOCATION_ID,
                       CA.CONSUMER_ACCT_ISSUE_NUM,
                       CASE WHEN CAB.LOCATION_ID IS NULL THEN NULL ELSE (SELECT VLH.DEPTH FROM LOCATION.VW_LOCATION_HIERARCHY VLH WHERE VLH.DESCENDENT_LOCATION_ID = POS.LOCATION_ID AND VLH.ANCESTOR_LOCATION_ID = CAB.LOCATION_ID) END DEPTH
                  FROM PSS.CONSUMER_ACCT_BASE CAB
                  LEFT OUTER JOIN PSS.CONSUMER_ACCT CA ON CAB.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
                 CROSS JOIN PSS.POS_PTA PTA
                  JOIN PSS.POS POS ON PTA.POS_ID = POS.POS_ID          
                 WHERE CAB.GLOBAL_ACCOUNT_ID = pn_global_account_id
                   AND PTA.POS_PTA_ID = pn_pos_pta_id 
                   AND CAB.CURRENCY_CD = pv_currency_cd) CAB
                ORDER BY CAB.CA_PRIORITY, CAB.PST_PRIORITY, CAB.DEPTH NULLS LAST, CAB.CONSUMER_ACCT_ISSUE_NUM DESC
        ) WHERE ROWNUM = 1;
        IF pn_consumer_acct_id IS NULL THEN
           SELECT MAX(NEW_GLOBAL_ACCOUNT_ID), MAX(NEW_GLOBAL_ACCOUNT_INSTANCE)
              INTO ln_new_global_account_id, ln_new_global_account_instance
              FROM PSS.GLOBAL_ACCOUNT_OVERWRITE
             WHERE OLD_GLOBAL_ACCOUNT_ID = pn_global_account_id;
           IF ln_new_global_account_id IS NOT NULL AND ln_new_global_account_id != pn_global_account_id THEN   
                GET_OR_CREATE_CONSUMER_ACCT(ln_new_global_account_id, ln_new_global_account_instance, pn_pos_pta_id, pv_currency_cd, pd_auth_ts, pv_truncated_card_num, pn_consumer_acct_id);
           ELSE
                SELECT PSS.SEQ_CONSUMER_ACCT_ID.NEXTVAL
                  INTO pn_consumer_acct_id
                  FROM DUAL;
                INSERT INTO PSS.CONSUMER_ACCT_BASE(CONSUMER_ACCT_ID, GLOBAL_ACCOUNT_ID, GLOBAL_ACCOUNT_INSTANCE, TRUNCATED_CARD_NUMBER, PAYMENT_SUBTYPE_ID, CURRENCY_CD) 
                    SELECT pn_consumer_acct_id, pn_global_account_id, NVL(pn_global_account_instance, MOD(pn_global_account_id, 10)), SUBSTR(pv_truncated_card_num, 1, 50), PTA.PAYMENT_SUBTYPE_ID, pv_currency_cd
                      FROM PSS.POS_PTA PTA
                     WHERE PTA.POS_PTA_ID = pn_pos_pta_id;
            END IF;
        END IF;
    END;
    
    PROCEDURE RESOLVE_ACCOUNT_CONFLICT(
        pn_old_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
        pn_new_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
        pn_global_account_instance PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_INSTANCE%TYPE)
    IS
        CURSOR l_old_cur IS
            SELECT CAB.CONSUMER_ACCT_ID, CA.CONSUMER_ID, CAB.CURRENCY_CD, CAB.LOCATION_ID, CAB.PAYMENT_SUBTYPE_ID
              FROM PSS.CONSUMER_ACCT_BASE CAB
              LEFT OUTER JOIN PSS.CONSUMER_ACCT CA ON CAB.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
             WHERE CAB.GLOBAL_ACCOUNT_ID = pn_old_global_account_id;
        ln_newest_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE;
        ln_new_consumer_acct_id PSS.CONSUMER_ACCT_BASE.CONSUMER_ACCT_ID%TYPE;
        ln_new_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE;
        lv_new_consumer_identifier PSS.CONSUMER.CONSUMER_IDENTIFIER%TYPE;
        lv_old_consumer_identifier PSS.CONSUMER.CONSUMER_IDENTIFIER%TYPE;
    BEGIN
        BEGIN
            INSERT INTO PSS.GLOBAL_ACCOUNT_OVERWRITE(OLD_GLOBAL_ACCOUNT_ID, NEW_GLOBAL_ACCOUNT_ID, NEW_GLOBAL_ACCOUNT_INSTANCE)
                VALUES(pn_old_global_account_id, pn_new_global_account_id, pn_global_account_instance);
        EXCEPTION
            WHEN DUP_VAL_ON_INDEX THEN
                NULL;
        END;
        SELECT NVL(MAX(NEW_GLOBAL_ACCOUNT_ID), pn_new_global_account_id)
          INTO ln_newest_global_account_id
          FROM (
                SELECT NEW_GLOBAL_ACCOUNT_ID,
                       CONNECT_BY_ISLEAF ISLEAF
                  FROM PSS.GLOBAL_ACCOUNT_OVERWRITE
                 START WITH OLD_GLOBAL_ACCOUNT_ID = pn_new_global_account_id
                 CONNECT BY NOCYCLE PRIOR NEW_GLOBAL_ACCOUNT_ID = OLD_GLOBAL_ACCOUNT_ID)
          WHERE ISLEAF = 1;
        FOR l_old_rec IN l_old_cur LOOP
            -- Find best new CA
            SELECT CONSUMER_ACCT_ID, CONSUMER_ID, CONSUMER_IDENTIFIER
              INTO ln_new_consumer_acct_id, ln_new_consumer_id, lv_new_consumer_identifier
              FROM (
            SELECT CAB.CONSUMER_ACCT_ID, CAB.CONSUMER_ID, CAB.CONSUMER_IDENTIFIER
              FROM (
                SELECT CAB.CONSUMER_ACCT_ID, 
                       CASE WHEN CA.CONSUMER_ACCT_ACTIVE_YN_FLAG IS NULL THEN 5 WHEN CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y' AND NVL(CA.CONSUMER_ACCT_ACTIVATION_TS, MIN_DATE) <= SYSDATE AND NVL(CA.CONSUMER_ACCT_DEACTIVATION_TS, MAX_DATE) > SYSDATE THEN 1 ELSE 10 END CA_PRIORITY,
                       CASE WHEN l_old_rec.PAYMENT_SUBTYPE_ID = CAB.PAYMENT_SUBTYPE_ID THEN 1 ELSE 10 END PST_PRIORITY,
                       CASE WHEN NVL(l_old_rec.LOCATION_ID, 0) = NVL(CAB.LOCATION_ID, 0) THEN 1 ELSE 10 END LOCATION_PRIORITY,
                       CA.CONSUMER_ACCT_ISSUE_NUM,
                       C.CONSUMER_ID,
                       C.CONSUMER_IDENTIFIER
                  FROM PSS.CONSUMER_ACCT_BASE CAB
                  LEFT OUTER JOIN (PSS.CONSUMER_ACCT CA 
                  JOIN PSS.CONSUMER C ON CA.CONSUMER_ID = C.CONSUMER_ID) ON CAB.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
                 WHERE CAB.GLOBAL_ACCOUNT_ID = ln_newest_global_account_id
                   AND CAB.CURRENCY_CD = l_old_rec.CURRENCY_CD) CAB
             ORDER BY CAB.CA_PRIORITY, CAB.PST_PRIORITY, CAB.LOCATION_PRIORITY, CAB.CONSUMER_ACCT_ISSUE_NUM DESC, CAB.CONSUMER_IDENTIFIER NULLS LAST
             ) WHERE ROWNUM = 1;
            
            IF l_old_rec.CONSUMER_ID IS NOT NULL THEN
                IF ln_new_consumer_id IS NULL THEN
                    UPDATE PSS.CONSUMER_ACCT 
                       SET CONSUMER_ACCT_ID = ln_new_consumer_acct_id
                     WHERE CONSUMER_ACCT_ID = l_old_rec.CONSUMER_ACCT_ID;            
                ELSE
                    DELETE 
                      FROM PSS.CONSUMER_ACCT
                     WHERE CONSUMER_ACCT_ID = l_old_rec.CONSUMER_ACCT_ID;  
                    IF ln_new_consumer_id != l_old_rec.CONSUMER_ID THEN 
	                    UPDATE PSS.CONSUMER_ACCT 
                           SET CONSUMER_ID = ln_new_consumer_id
                         WHERE CONSUMER_ID = l_old_rec.CONSUMER_ID;
                         
                        UPDATE PSS.CONSUMER_PROMOTION CP
	                       SET (TRAN_COUNT, PROMO_TRAN_COUNT) = 
	                           (SELECT NVL(SUM(CP0.TRAN_COUNT), 0) + CP.TRAN_COUNT, NVL(SUM(CP0.PROMO_TRAN_COUNT), 0) + CP.PROMO_TRAN_COUNT
	                              FROM PSS.CONSUMER_PROMOTION CP0
	                             WHERE CP0.CONSUMER_ID = l_old_rec.CONSUMER_ID)
	                     WHERE CP.CONSUMER_ID = ln_new_consumer_id;
	                    
	                    DELETE FROM PSS.CONSUMER_PROMOTION
	                     WHERE CONSUMER_ID = l_old_rec.CONSUMER_ID;
	                    
	                    DELETE FROM PSS.CONSUMER
	                     WHERE CONSUMER_ID = l_old_rec.CONSUMER_ID
	                     RETURNING CONSUMER_IDENTIFIER INTO lv_old_consumer_identifier;  
	                    
	                    IF lv_new_consumer_identifier IS NULL AND lv_old_consumer_identifier IS NOT NULL THEN
	                        UPDATE PSS.CONSUMER
	                          SET CONSUMER_IDENTIFIER = lv_old_consumer_identifier
	                        WHERE CONSUMER_ID = ln_new_consumer_id
	                          AND CONSUMER_IDENTIFIER IS NULL;
	                    END IF;
	               END IF;
                END IF; 
            END IF;

            UPDATE PSS.CONSUMER_ACCT_DEVICE CAD
               SET (USED_COUNT, LAST_USED_UTC_TS) = 
                   (SELECT NVL(SUM(CAD0.USED_COUNT), 0) + CAD.USED_COUNT, NULLIF(GREATEST(NVL(MAX(CAD0.LAST_USED_UTC_TS), MIN_DATE), NVL(CAD.LAST_USED_UTC_TS, MIN_DATE)), MIN_DATE)
                      FROM PSS.CONSUMER_ACCT_DEVICE CAD0
                     WHERE CAD0.CONSUMER_ACCT_ID = l_old_rec.CONSUMER_ACCT_ID)
             WHERE CAD.CONSUMER_ACCT_ID = ln_new_consumer_acct_id;
            
            IF SQL%ROWCOUNT > 0 THEN
                DELETE FROM PSS.CONSUMER_ACCT_DEVICE 
                 WHERE CONSUMER_ACCT_ID = l_old_rec.CONSUMER_ACCT_ID;
            ELSE
                UPDATE PSS.CONSUMER_ACCT_DEVICE
                   SET CONSUMER_ACCT_ID = ln_new_consumer_acct_id
                 WHERE CONSUMER_ACCT_ID = l_old_rec.CONSUMER_ACCT_ID;
            END IF;
             
            UPDATE PSS.TRAN
               SET CONSUMER_ACCT_ID = ln_new_consumer_acct_id
             WHERE CONSUMER_ACCT_ID = l_old_rec.CONSUMER_ACCT_ID;
            
            UPDATE REPORT.TRANS
               SET CONSUMER_ACCT_ID = ln_new_consumer_acct_id
             WHERE CONSUMER_ACCT_ID = l_old_rec.CONSUMER_ACCT_ID;
            
            UPDATE REPORT.ACTIVITY_REF
               SET CONSUMER_ACCT_ID = ln_new_consumer_acct_id
             WHERE CONSUMER_ACCT_ID = l_old_rec.CONSUMER_ACCT_ID;
                   
            DELETE 
              FROM PSS.CONSUMER_ACCT_BASE
             WHERE CONSUMER_ACCT_ID = l_old_rec.CONSUMER_ACCT_ID;  

             COMMIT;
         END LOOP;
    END;
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/CORP/GET_BACKOFFICE_DEVICE.prc?rev=HEAD
CREATE OR REPLACE PROCEDURE CORP.GET_BACKOFFICE_DEVICE(
    pn_customer_bank_id CORP.CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
    pn_device_id OUT NUMBER,
    pv_device_serial_cd OUT VARCHAR2,
    pv_bank_country_cd OUT CORP.CUSTOMER_BANK.BANK_COUNTRY_CD%TYPE)
IS
    lv_device_name VARCHAR2(60);
    lv_device_type_desc VARCHAR2(60);
    ln_device_sub_type_id NUMBER(3);
    lc_prev_device_active_yn_flag VARCHAR(1);
    lc_master_id_always_inc VARCHAR(1);
    ln_key_gen_time NUMBER;
    lc_legacy_safe_key VARCHAR(1);
    lv_time_zone_guid REPORT.TIME_ZONE.TIME_ZONE_GUID%TYPE; 
    ln_new_host_count NUMBER;
    ln_result_cd NUMBER;
    lv_error_message VARCHAR2(1000);
    ln_customer_id CORP.CUSTOMER.CUSTOMER_ID%TYPE;
    ln_terminal_id REPORT.TERMINAL.TERMINAL_ID%TYPE;
    ln_dealer_id CORP.DEALER.DEALER_ID%TYPE;
    ln_eport_id REPORT.EPORT.EPORT_ID%TYPE;
    lv_customer_address1 CORP.CUSTOMER_ADDR.ADDRESS1%TYPE;                                                                                                                                                               
    lv_customer_city CORP.CUSTOMER_ADDR.CITY%TYPE;                                                                                                                                                                              
    lv_customer_state_cd CORP.CUSTOMER_ADDR.STATE%TYPE;                                                                                                                                                                              
    lv_customer_postal CORP.CUSTOMER_ADDR.ZIP%TYPE;                                                                                                                                                                              
    lv_customer_country_cd CORP.CUSTOMER_ADDR.COUNTRY_CD%TYPE;
    lv_location_name REPORT.LOCATION.LOCATION_NAME%TYPE;
BEGIN
    SELECT D.DEVICE_ID, 'V2-' || CB.CUSTOMER_BANK_ID || '-' || CU.CURRENCY_CODE, CB.BANK_COUNTRY_CD, CB.CUSTOMER_ID, SUBSTR('Back Office - ' || CB.BANK_NAME || ' - #' ||CB.BANK_ACCT_NBR, 1, 50)
      INTO pn_device_id, pv_device_serial_cd, pv_bank_country_cd, ln_customer_id, lv_location_name
      FROM CORP.CUSTOMER_BANK CB
      JOIN CORP.COUNTRY CO ON CB.BANK_COUNTRY_CD = CO.COUNTRY_CD
      JOIN CORP.CURRENCY CU ON CO.CURRENCY_ID = CU.CURRENCY_ID
      LEFT OUTER JOIN DEVICE.DEVICE D ON D.DEVICE_SERIAL_CD = 'V2-' || CB.CUSTOMER_BANK_ID || '-' || CU.CURRENCY_CODE
     WHERE CB.CUSTOMER_BANK_ID = pn_customer_bank_id
       AND CB.STATUS = 'A'
       AND CO.ALLOW_BANKS = 'Y';
     
    IF pn_device_id IS NOT NULL THEN
        RETURN;
    END IF;
    
    -- insert device
    PKG_DEVICE_CONFIGURATION.INITIALIZE_DEVICE(pv_device_serial_cd, 14, 'Y',
        pn_device_id,
        lv_device_name,
        lv_device_type_desc,
        ln_device_sub_type_id,
        lc_prev_device_active_yn_flag,
        lc_master_id_always_inc,
        ln_key_gen_time,
        lc_legacy_safe_key,
        lv_time_zone_guid,
        ln_new_host_count,
        ln_result_cd,
        lv_error_message		
    );
    
	IF pn_device_id <= 0 THEN
        RAISE_APPLICATION_ERROR(-20010, lv_error_message);
    END IF;
    
    SELECT MAX(DEALER_ID)
    INTO ln_dealer_id
    FROM (
        SELECT DEALER_ID
        FROM CORP.DEALER_LICENSE
        WHERE LICENSE_ID IN (
            SELECT MAX(LICENSE_ID)
            FROM (
                SELECT L.LICENSE_ID
                  FROM CORP.CUSTOMER_LICENSE CL
                  JOIN CORP.LICENSE_NBR LN ON CL.LICENSE_NBR = LN.LICENSE_NBR
                  JOIN CORP.LICENSE L ON LN.LICENSE_ID = L.LICENSE_ID
                 WHERE CL.CUSTOMER_ID = ln_customer_id
                   AND L.STATUS = 'A'
                 ORDER BY CL.RECEIVED DESC
            ) WHERE ROWNUM = 1
        )
        AND SYSDATE >= NVL(START_DATE, MIN_DATE)
        AND SYSDATE < NVL(END_DATE, MAX_DATE)
        ORDER BY START_DATE DESC, DEALER_ID DESC
    ) WHERE ROWNUM = 1;
    
    IF ln_dealer_id IS NULL THEN
        SELECT DEALER_ID
        INTO ln_dealer_id
        FROM (
            SELECT DEALER_ID
            FROM CORP.DEALER
            WHERE DEALER_NAME = 'USA Technologies'
            ORDER BY DEALER_ID
        ) WHERE ROWNUM = 1;
    END IF;

    SELECT MAX(ADDRESS1), NVL(MAX(CITY), 'Malvern'), NVL(MAX(STATE), 'PA'), NVL(MAX(ZIP), '19355'), NVL(MAX(COUNTRY_CD), 'US')                                                                                                                                                                     
      INTO lv_customer_address1, lv_customer_city, lv_customer_state_cd, lv_customer_postal, lv_customer_country_cd 
      FROM CORP.CUSTOMER_ADDR
     WHERE CUSTOMER_ID = ln_customer_id;
     
    REPORT.GET_OR_CREATE_EPORT(ln_eport_id, pv_device_serial_cd, 14);
    CORP.DEALER_EPORT_UPD(ln_dealer_id, ln_eport_id);
    REPORT.PKG_CUSTOMER_MANAGEMENT.CREATE_TERMINAL_MASS(
        0, /* pn_user_id */
        ln_terminal_id, /* pn_terminal_id */
        pv_device_serial_cd, /* pv_device_serial_cd */
        ln_dealer_id, /* pn_dealer_id */
        'TBD', /* pv_asset_nbr */
        'TBD', /* pv_machine_make */
        'To Be Determined', /* pv_machine_model */
        NULL, /* pv_telephone */
        NULL, /* pv_prefix */
        NULL, /* pv_region_name */
        lv_location_name, /* pv_location_name */
        NULL, /* pv_location_details */
        lv_customer_address1, /* pv_address1 */
        lv_customer_city, /* pv_city */
        lv_customer_state_cd, /* pv_state_cd */
        lv_customer_postal, /* pv_postal */
        lv_customer_country_cd, /* pv_country_cd */
        pn_customer_bank_id, /* pn_customer_bank_id */
        1, /* pn_pay_sched_id */
        '- Not Assigned -', /* pv_location_type_name */
        NULL, /* pn_location_type_specific */
        '- Not Assigned -', /* pv_product_type_name */
        NULL, /* pn_product_type_specific */
        '?', /* pc_auth_mode */
        0, /* pn_avg_amt */
        0, /* pn_time_zone_id */
        '?', /* pc_dex_data */
        '?', /* pc_receipt */
        0, /* pn_vends */
        NULL, /* pv_term_var_1 */
        NULL, /* pv_term_var_2 */
        NULL, /* pv_term_var_3 */
        NULL, /* pv_term_var_4 */
        NULL, /* pv_term_var_5 */
        NULL, /* pv_term_var_6 */
        NULL, /* pv_term_var_7 */
        NULL, /* pv_term_var_8 */
        TRUNC(SYSDATE - 30), /* pd_activate_date */
        0, /* pn_fee_grace_days */
        'N', /* pc_keep_existing_data */
        'N' /* pc_override_payment_schedule */
    );
    
    UPDATE REPORT.TERMINAL
       SET STATUS = 'A'
     WHERE TERMINAL_ID = ln_terminal_id 
       AND STATUS != 'A';
END;
/
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/DBADMIN/RESTRAIN.fnc?rev=HEAD
CREATE OR REPLACE FUNCTION DBADMIN.RESTRAIN(
    pd_date DATE,
    pd_min_date DATE,
    pd_max_date DATE)
    RETURN DATE
    DETERMINISTIC
    PARALLEL_ENABLE
IS
BEGIN
    IF pd_date IS NULL THEN
        RETURN pd_date;
    ELSIF pd_min_date IS NOT NULL AND pd_date < pd_min_date THEN
        RETURN pd_min_date;
    ELSIF pd_max_date IS NOT NULL AND pd_date > pd_max_date THEN
        RETURN pd_max_date;
    ELSE
        RETURN pd_date;
    END IF;
END;
/
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/CLASSIFY_HEALTH.fnc?rev=HEAD
CREATE OR REPLACE FUNCTION REPORT.CLASSIFY_HEALTH(
    pd_last_comm_date DATE,
    pd_last_cashless_date DATE,
    pd_last_cash_date DATE,
    pd_last_dex_date DATE,
    pn_terminal_fee_status CHAR,
    pn_rental_fee_status CHAR,    
    pn_no_comm_days_min PLS_INTEGER,
    pn_no_comm_days_max PLS_INTEGER,
    pn_no_cashless_days_min PLS_INTEGER,
    pn_no_cashless_days_max PLS_INTEGER,
    pn_no_cash_days_min PLS_INTEGER,
    pn_no_cash_days_max PLS_INTEGER,
    pn_no_dex_days_min PLS_INTEGER,
    pn_no_dex_days_max PLS_INTEGER)
    RETURN VARCHAR2
    DETERMINISTIC
    PARALLEL_ENABLE
IS
    lv_classification VARCHAR2(20);
BEGIN
    SELECT CASE 
            WHEN TERMINAL_FEE_STATUS != 'A' AND RENTAL_FEE_STATUS != 'A' AND (CALL_IN_HEALTH = 'D' OR (CASHLESS_HEALTH = 'D' AND CASH_HEALTH = 'D' AND DEX_HEALTH = 'D')) THEN 'INVENTORY'
            WHEN CALL_IN_HEALTH = 'Y' THEN 'NO_COMM'
            WHEN CALL_IN_HEALTH = 'A' AND CASHLESS_HEALTH = 'Y' THEN 'NO_CASHLESS'
            WHEN CALL_IN_HEALTH IN('D', 'O') AND RENTAL_FEE_STATUS = 'A' THEN 'INACTIVE_RENTAL'
            WHEN CALL_IN_HEALTH IN('D', 'O') AND TERMINAL_FEE_STATUS = 'A' THEN 'INACTIVE_OWNED'
            WHEN CALL_IN_HEALTH IN('D', 'O') THEN 'INACTIVE'
            ELSE 'ACTIVE'
          END
     INTO lv_classification
     FROM (
    SELECT REPORT.PKG_APP_USER.GET_HEALTH_CODE(pd_last_comm_date, pn_no_comm_days_min, pn_no_comm_days_max) CALL_IN_HEALTH,
           REPORT.PKG_APP_USER.GET_HEALTH_CODE(pd_last_cashless_date, pn_no_cashless_days_min, pn_no_cashless_days_max) CASHLESS_HEALTH,
           REPORT.PKG_APP_USER.GET_HEALTH_CODE(pd_last_cash_date, pn_no_cash_days_min, pn_no_cash_days_max) CASH_HEALTH,
           --REPORT.PKG_APP_USER.GET_HEALTH_CODE(pd_last_fill_date, pn_no_fill_days_min, pn_no_fill_days_max) FILL_HEALTH,
           REPORT.PKG_APP_USER.GET_HEALTH_CODE(pd_last_dex_date, pn_no_dex_days_min, pn_no_dex_days_max) DEX_HEALTH,
           pn_terminal_fee_status TERMINAL_FEE_STATUS,
           pn_rental_fee_status RENTAL_FEE_STATUS
      FROM DUAL);
    RETURN lv_classification;
END;
/
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/PKG_CUSTOMER_MANAGEMENT.psk?rev=1.22
CREATE OR REPLACE PACKAGE REPORT.PKG_CUSTOMER_MANAGEMENT AS
    PROCEDURE CREATE_TERMINAL(
        pn_terminal_id OUT REPORT.TERMINAL.TERMINAL_ID%TYPE,
        pv_device_serial_cd IN REPORT.EPORT.EPORT_SERIAL_NUM%TYPE,
        pn_dealer_id IN CORP.DEALER.DEALER_ID%TYPE,
        pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
        pn_machine_id IN REPORT.TERMINAL.MACHINE_ID%TYPE,
        pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
        pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
        pn_region_id IN REPORT.REGION.REGION_ID%TYPE,
        pv_region_name IN REPORT.REGION.REGION_NAME%TYPE,
        pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
        pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
        pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
        pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
        pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
        pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
        pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
        pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
        pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_pc_id IN REPORT.TERMINAL.PRIMARY_CONTACT_ID%TYPE,
        pn_sc_id IN REPORT.TERMINAL.SECONDARY_CONTACT_ID%TYPE,
        pn_location_type_id IN REPORT.LOCATION.LOCATION_TYPE_ID%TYPE,
        pv_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
        pn_product_type_id IN REPORT.TERMINAL.PRODUCT_TYPE_ID%TYPE,
        pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
        pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
        pn_avg_amt IN REPORT.TERMINAL.AVG_TRANS_AMT%TYPE,
        pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
        pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
        pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
        pn_vends REPORT.TERMINAL.VENDS%TYPE,
        pv_term_var_1 IN REPORT.TERMINAL.TERM_VAR_1%TYPE,
        pv_term_var_2 IN REPORT.TERMINAL.TERM_VAR_2%TYPE,
        pv_term_var_3 IN REPORT.TERMINAL.TERM_VAR_3%TYPE,
        pv_term_var_4 IN REPORT.TERMINAL.TERM_VAR_4%TYPE,
        pv_term_var_5 IN REPORT.TERMINAL.TERM_VAR_5%TYPE,
        pv_term_var_6 IN REPORT.TERMINAL.TERM_VAR_6%TYPE,
        pv_term_var_7 IN REPORT.TERMINAL.TERM_VAR_7%TYPE,
        pv_term_var_8 IN REPORT.TERMINAL.TERM_VAR_8%TYPE,
		pd_activate_date IN REPORT.TERMINAL_EPORT.START_DATE%TYPE DEFAULT SYSDATE - 30,
        pn_fee_grace_days IN NUMBER DEFAULT 60,
		pv_doing_business_as IN REPORT.TERMINAL.DOING_BUSINESS_AS%TYPE DEFAULT NULL,
		pc_fee_no_trigger_event_flag IN CORP.SERVICE_FEES.NO_TRIGGER_EVENT_FLAG%TYPE DEFAULT NULL,
		pv_sales_order_number IN REPORT.TERMINAL.SALES_ORDER_NUMBER%TYPE DEFAULT NULL,
		pv_purchase_order_number IN REPORT.TERMINAL.PURCHASE_ORDER_NUMBER%TYPE DEFAULT NULL);
    
    PROCEDURE CREATE_TERMINAL_MASS(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
		pn_terminal_id OUT REPORT.TERMINAL.TERMINAL_ID%TYPE,
        pv_device_serial_cd IN REPORT.EPORT.EPORT_SERIAL_NUM%TYPE,
        pn_dealer_id IN CORP.DEALER.DEALER_ID%TYPE,
        pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
        pv_machine_make REPORT.MACHINE.MAKE%TYPE,
		pv_machine_model REPORT.MACHINE.MODEL%TYPE,
        pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
        pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
        pv_region_name IN REPORT.REGION.REGION_NAME%TYPE,
        pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
        pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
        pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
        pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
        pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
        pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
        pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
        pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
        pv_location_type_name REPORT.LOCATION_TYPE.LOCATION_TYPE_NAME%TYPE,
        pn_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
        pv_product_type_name REPORT.PRODUCT_TYPE.PRODUCT_TYPE_NAME%TYPE,
        pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
        pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
        pn_avg_amt IN REPORT.TERMINAL.AVG_TRANS_AMT%TYPE,
        pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
        pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
        pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
        pn_vends REPORT.TERMINAL.VENDS%TYPE,
        pv_term_var_1 REPORT.TERMINAL.term_var_1%TYPE,
        pv_term_var_2 REPORT.TERMINAL.term_var_2%TYPE,
        pv_term_var_3 REPORT.TERMINAL.term_var_3%TYPE,
        pv_term_var_4 REPORT.TERMINAL.term_var_4%TYPE,
        pv_term_var_5 REPORT.TERMINAL.term_var_5%TYPE,
        pv_term_var_6 REPORT.TERMINAL.term_var_6%TYPE,
        pv_term_var_7 REPORT.TERMINAL.term_var_7%TYPE,
        pv_term_var_8 REPORT.TERMINAL.term_var_8%TYPE,
		pd_activate_date IN REPORT.TERMINAL_EPORT.START_DATE%TYPE DEFAULT SYSDATE - 30,
        pn_fee_grace_days IN NUMBER DEFAULT 60,
		pc_keep_existing_data IN CHAR DEFAULT 'N',
		pc_override_payment_schedule IN CHAR DEFAULT 'N',
		pv_doing_business_as IN REPORT.TERMINAL.DOING_BUSINESS_AS%TYPE DEFAULT NULL,
		pc_fee_no_trigger_event_flag IN CORP.SERVICE_FEES.NO_TRIGGER_EVENT_FLAG%TYPE DEFAULT NULL,
		pv_sales_order_number IN REPORT.TERMINAL.SALES_ORDER_NUMBER%TYPE DEFAULT NULL,
		pv_purchase_order_number IN REPORT.TERMINAL.PURCHASE_ORDER_NUMBER%TYPE DEFAULT NULL);
        
    PROCEDURE UPDATE_TERMINAL(
       pn_terminal_id IN REPORT.TERMINAL.TERMINAL_ID%TYPE,
       pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
       pn_machine_id IN REPORT.TERMINAL.MACHINE_ID%TYPE,
       pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
       pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
       pn_region_id IN REPORT.REGION.REGION_ID%TYPE,
       pv_region_name IN REPORT.REGION.REGION_NAME%TYPE,
       pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
       pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
       pn_address_id REPORT.TERMINAL_ADDR.ADDRESS_ID%TYPE,
       pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
       pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
       pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
       pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
       pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
       pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
       pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
       pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
       pn_pc_id IN REPORT.TERMINAL.PRIMARY_CONTACT_ID%TYPE,
       pn_sc_id IN REPORT.TERMINAL.SECONDARY_CONTACT_ID%TYPE,
       pn_location_type_id IN REPORT.LOCATION.LOCATION_TYPE_ID%TYPE,
       pn_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
       pn_product_type_id IN REPORT.TERMINAL.PRODUCT_TYPE_ID%TYPE,
       pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
       pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
       pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
       pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
       pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
       pn_vends REPORT.TERMINAL.VENDS%TYPE,
       pv_term_var_1 REPORT.TERMINAL.term_var_1%TYPE,
       pv_term_var_2 REPORT.TERMINAL.term_var_2%TYPE,
       pv_term_var_3 REPORT.TERMINAL.term_var_3%TYPE,
       pv_term_var_4 REPORT.TERMINAL.term_var_4%TYPE,
       pv_term_var_5 REPORT.TERMINAL.term_var_5%TYPE,
       pv_term_var_6 REPORT.TERMINAL.term_var_6%TYPE,
       pv_term_var_7 REPORT.TERMINAL.term_var_7%TYPE,
       pv_term_var_8 REPORT.TERMINAL.term_var_8%TYPE,
	   pv_doing_business_as IN REPORT.TERMINAL.DOING_BUSINESS_AS%TYPE DEFAULT NULL);
       
   PROCEDURE UPDATE_TERMINAL_MASS(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
		pn_terminal_id IN REPORT.TERMINAL.TERMINAL_ID%TYPE,
        pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
        pv_machine_make REPORT.MACHINE.MAKE%TYPE,
		pv_machine_model REPORT.MACHINE.MODEL%TYPE,
        pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
        pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
        pv_region_name IN REPORT.REGION.REGION_NAME%TYPE,
        pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
        pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
        pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
        pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
        pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
        pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
        pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
        pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
        pn_location_type_id REPORT.LOCATION_TYPE.LOCATION_TYPE_ID%TYPE,
		pv_location_type_name REPORT.LOCATION_TYPE.LOCATION_TYPE_NAME%TYPE,
        pn_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
        pv_product_type_name REPORT.PRODUCT_TYPE.PRODUCT_TYPE_NAME%TYPE,
        pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
        pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
        pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
        pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
        pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
        pn_vends REPORT.TERMINAL.VENDS%TYPE,
        pv_term_var_1 REPORT.TERMINAL.term_var_1%TYPE,
        pv_term_var_2 REPORT.TERMINAL.term_var_2%TYPE,
        pv_term_var_3 REPORT.TERMINAL.term_var_3%TYPE,
        pv_term_var_4 REPORT.TERMINAL.term_var_4%TYPE,
        pv_term_var_5 REPORT.TERMINAL.term_var_5%TYPE,
        pv_term_var_6 REPORT.TERMINAL.term_var_6%TYPE,
        pv_term_var_7 REPORT.TERMINAL.term_var_7%TYPE,
        pv_term_var_8 REPORT.TERMINAL.term_var_8%TYPE,
		pv_doing_business_as IN REPORT.TERMINAL.DOING_BUSINESS_AS%TYPE DEFAULT NULL);
     
    -- pre usalive 1.9 signature   
    PROCEDURE CREATE_CUSTOMER_BANK(
        pn_customer_bank_id OUT CORP.CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        pn_customer_id IN CORP.CUSTOMER_BANK.CUSTOMER_ID%TYPE,
        pn_user_id IN CORP.CUSTOMER.USER_ID%TYPE,
        pv_bank_name IN CORP.CUSTOMER_BANK.BANK_NAME%TYPE,
        pv_bank_address1 IN CORP.CUSTOMER_BANK.BANK_ADDRESS%TYPE,
        pv_bank_city IN CORP.CUSTOMER_BANK.BANK_CITY%TYPE,
        pv_bank_state_cd IN CORP.CUSTOMER_BANK.BANK_STATE%TYPE,
        pv_bank_postal IN CORP.CUSTOMER_BANK.BANK_ZIP%TYPE,
        pv_bank_country_cd IN CORP.CUSTOMER_BANK.BANK_COUNTRY_CD%TYPE,
        pv_account_title IN CORP.CUSTOMER_BANK.ACCOUNT_TITLE%TYPE,
        pv_account_type IN CORP.CUSTOMER_BANK.ACCOUNT_TYPE%TYPE,
        pv_bank_routing_nbr IN CORP.CUSTOMER_BANK.BANK_ROUTING_NBR%TYPE,
        pv_bank_acct_nbr IN CORP.CUSTOMER_BANK.BANK_ACCT_NBR%TYPE,
        pv_contact_name IN CORP.CUSTOMER_BANK.CONTACT_NAME%TYPE,
        pv_contact_title IN CORP.CUSTOMER_BANK.CONTACT_TITLE%TYPE,
        pv_contact_telephone IN CORP.CUSTOMER_BANK.CONTACT_TELEPHONE%TYPE,
        pv_contact_fax IN CORP.CUSTOMER_BANK.CONTACT_FAX%TYPE);
    
    -- usalive 1.9 signature    
    PROCEDURE CREATE_CUSTOMER_BANK(
        pn_customer_bank_id OUT CORP.CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        pn_customer_id IN CORP.CUSTOMER_BANK.CUSTOMER_ID%TYPE,
        pn_user_id IN CORP.CUSTOMER.USER_ID%TYPE,
        pv_bank_name IN CORP.CUSTOMER_BANK.BANK_NAME%TYPE,
        pv_bank_address1 IN CORP.CUSTOMER_BANK.BANK_ADDRESS%TYPE,
        pv_bank_city IN CORP.CUSTOMER_BANK.BANK_CITY%TYPE,
        pv_bank_state_cd IN CORP.CUSTOMER_BANK.BANK_STATE%TYPE,
        pv_bank_postal IN CORP.CUSTOMER_BANK.BANK_ZIP%TYPE,
        pv_bank_country_cd IN CORP.CUSTOMER_BANK.BANK_COUNTRY_CD%TYPE,
        pv_account_title IN CORP.CUSTOMER_BANK.ACCOUNT_TITLE%TYPE,
        pv_account_type IN CORP.CUSTOMER_BANK.ACCOUNT_TYPE%TYPE,
        pv_bank_routing_nbr IN CORP.CUSTOMER_BANK.BANK_ROUTING_NBR%TYPE,
        pv_bank_acct_nbr IN CORP.CUSTOMER_BANK.BANK_ACCT_NBR%TYPE,
        pv_contact_name IN CORP.CUSTOMER_BANK.CONTACT_NAME%TYPE,
        pv_contact_title IN CORP.CUSTOMER_BANK.CONTACT_TITLE%TYPE,
        pv_contact_telephone IN CORP.CUSTOMER_BANK.CONTACT_TELEPHONE%TYPE,
        pv_contact_fax IN CORP.CUSTOMER_BANK.CONTACT_FAX%TYPE,
        pv_swift_code IN CORP.CUSTOMER_BANK.SWIFT_CODE%TYPE);
        
    PROCEDURE UPDATE_REGION_NAME(
        pn_region_id REPORT.REGION.REGION_ID%TYPE,
        pv_region_name REPORT.REGION.REGION_NAME%TYPE,
        pn_customer_id REPORT.TERMINAL.CUSTOMER_ID%TYPE);
        
   PROCEDURE CREATE_CAMPAIGN(
        pv_campaign_name REPORT.CAMPAIGN.CAMPAIGN_NAME%TYPE,
        pv_campaign_desc REPORT.CAMPAIGN.CAMPAIGN_DESC%TYPE,
        pn_campaign_type_id REPORT.CAMPAIGN.CAMPAIGN_TYPE_ID%TYPE,
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pd_start_date REPORT.CAMPAIGN.START_DATE%TYPE,
        pd_end_date REPORT.CAMPAIGN.END_DATE%TYPE,
        pn_discount_percent REPORT.CAMPAIGN.DISCOUNT_PERCENT%TYPE,
        pn_recur_schedule REPORT.CAMPAIGN.RECUR_SCHEDULE%TYPE,
        pn_threshold_amount REPORT.CAMPAIGN.THRESHOLD_AMOUNT%TYPE,
        pn_campaign_id OUT REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE
        );
        
   PROCEDURE EDIT_CAMPAIGN(
        pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
        pv_campaign_name REPORT.CAMPAIGN.CAMPAIGN_NAME%TYPE,
        pv_campaign_desc REPORT.CAMPAIGN.CAMPAIGN_DESC%TYPE,
        pn_campaign_type_id REPORT.CAMPAIGN.CAMPAIGN_TYPE_ID%TYPE,
        pd_start_date REPORT.CAMPAIGN.START_DATE%TYPE,
        pd_end_date REPORT.CAMPAIGN.END_DATE%TYPE,
        pn_discount_percent REPORT.CAMPAIGN.DISCOUNT_PERCENT%TYPE,
        pn_recur_schedule REPORT.CAMPAIGN.RECUR_SCHEDULE%TYPE,
        pn_threshold_amount REPORT.CAMPAIGN.THRESHOLD_AMOUNT%TYPE
        );
        
  PROCEDURE DELETE_CAMPAIGN(
        pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE
        );
   
   PROCEDURE ADD_CAMP_POS_PTA_BY_POS(
		pn_pos_pta_id PSS.CAMPAIGN_POS_PTA.POS_PTA_ID%TYPE,
    pn_pos_id PSS.POS.POS_ID%TYPE,
    pc_delete_flag CHAR);
    
   PROCEDURE ADD_CAM_POS_PTA_BY_REG(
		pn_terminal_id REPORT.TERMINAL.TERMINAL_ID%TYPE,
    pn_region_id REPORT.TERMINAL_REGION.REGION_ID%TYPE);
    
    PROCEDURE ADD_CAM_POS_PTA_BY_TER_EPORT(
    pn_terminal_id REPORT.TERMINAL_EPORT.TERMINAL_ID%TYPE,
		pn_eport_id REPORT.TERMINAL_EPORT.EPORT_ID%TYPE,
		pd_start_date REPORT.TERMINAL_EPORT.START_DATE%TYPE,
    pd_end_date REPORT.TERMINAL_EPORT.END_DATE%TYPE,
    pn_terminal_eport_id REPORT.TERMINAL_EPORT.TERMINAL_EPORT_ID%TYPE,
    pc_delete_only_flag CHAR DEFAULT 'N');
    
    PROCEDURE DELETE_CAMPAIGN_ASSIGNMENT(
		pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE);
    
    PROCEDURE EDIT_CAMPAIGN_CUSTOMER(
		pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
    pn_customer_id REPORT.CAMPAIGN_ASSIGNMENT.CUSTOMER_ID%TYPE,
    pv_add CHAR);
    
    PROCEDURE EDIT_CAMPAIGN_REGION(
    pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
		pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
    pn_customer_id REPORT.CAMPAIGN_ASSIGNMENT.CUSTOMER_ID%TYPE,
    pn_region_id NUMBER_TABLE,
    pv_add CHAR);
    
    PROCEDURE EDIT_CAMPAIGN_TERMINAL(
    pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
		pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
    pn_terminal_id NUMBER_TABLE,
    pv_add CHAR);
    
    PROCEDURE ADD_CAM_POS_PTA_BY_CAMPAIGN(
		pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE);
    
    PROCEDURE ADD_CARDS_TO_CAMPAIGN(
		pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
    pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
    pn_card_id NUMBER_TABLE);
    
    PROCEDURE REMOVE_CARDS_FROM_CAMPAIGN(
    pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
    pn_card_id NUMBER_TABLE);
    
    --begin pre R36, can be removed afterwards   
    FUNCTION                 FIND_CAMPAIGN (
    l_terminal_id REPORT.TERMINAL_EPORT.TERMINAL_ID%TYPE,
    l_region_id REPORT.TERMINAL_REGION.REGION_ID%TYPE,
    l_customer_id REPORT.TERMINAL.CUSTOMER_ID%TYPE)
    RETURN REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE;
    
    FUNCTION                 FIND_CAMPAIGN (
    l_terminal_id REPORT.TERMINAL_EPORT.TERMINAL_ID%TYPE)
    RETURN REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE;
    
    PROCEDURE ADD_CAM_POS_PTA_BY_TER(
		pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
		pn_terminal_id REPORT.TERMINAL.TERMINAL_ID%TYPE);
    
    PROCEDURE ADD_CAM_POS_PTA_BY_TER_EPORT(
    pn_terminal_id REPORT.TERMINAL_EPORT.TERMINAL_ID%TYPE,
		pn_eport_id REPORT.TERMINAL_EPORT.EPORT_ID%TYPE,
		pd_start_date REPORT.TERMINAL_EPORT.START_DATE%TYPE,
    pd_end_date REPORT.TERMINAL_EPORT.END_DATE%TYPE);
    
    PROCEDURE ADD_CAMPAIGN_CUSTOMER(
		pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
    pn_customer_id NUMBER_TABLE);
    
    PROCEDURE ADD_CAMPAIGN_TERMINAL(
    pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
		pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
    pn_terminal_id NUMBER_TABLE,
    pn_assgined_terminal_id OUT NUMBER_TABLE);
    
    PROCEDURE CREATE_CAMPAIGN(
        pv_campaign_name REPORT.CAMPAIGN.CAMPAIGN_NAME%TYPE,
        pv_campaign_desc REPORT.CAMPAIGN.CAMPAIGN_DESC%TYPE,
        pn_campaign_type_id REPORT.CAMPAIGN.CAMPAIGN_TYPE_ID%TYPE,
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pd_start_date REPORT.CAMPAIGN.START_DATE%TYPE,
        pd_end_date REPORT.CAMPAIGN.END_DATE%TYPE,
        pn_discount_percent REPORT.CAMPAIGN.DISCOUNT_PERCENT%TYPE,
        pn_campaign_id OUT REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE
        );
        
   PROCEDURE EDIT_CAMPAIGN(
        pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
        pv_campaign_name REPORT.CAMPAIGN.CAMPAIGN_NAME%TYPE,
        pv_campaign_desc REPORT.CAMPAIGN.CAMPAIGN_DESC%TYPE,
        pn_campaign_type_id REPORT.CAMPAIGN.CAMPAIGN_TYPE_ID%TYPE,
        pd_start_date REPORT.CAMPAIGN.START_DATE%TYPE,
        pd_end_date REPORT.CAMPAIGN.END_DATE%TYPE,
        pn_discount_percent REPORT.CAMPAIGN.DISCOUNT_PERCENT%TYPE
        );
    -- end of pre R36, can be removed afterwards
    
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/PKG_CUSTOMER_MANAGEMENT.pbk?rev=1.48
CREATE OR REPLACE PACKAGE BODY REPORT.PKG_CUSTOMER_MANAGEMENT AS
    PROCEDURE INTERNAL_CREATE_TERMINAL(
       pn_terminal_id OUT REPORT.TERMINAL.TERMINAL_ID%TYPE,
       pv_device_serial_cd IN REPORT.EPORT.EPORT_SERIAL_NUM%TYPE,
       pn_dealer_id IN CORP.DEALER.DEALER_ID%TYPE,
       pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
       pn_machine_id IN REPORT.TERMINAL.MACHINE_ID%TYPE,
       pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
       pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
       pn_region_id IN REPORT.REGION.REGION_ID%TYPE,
       pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
       pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
       pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
       pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
       pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
       pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
       pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
       pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
       pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
       pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
       pn_pc_id IN REPORT.TERMINAL.PRIMARY_CONTACT_ID%TYPE,
       pn_sc_id IN REPORT.TERMINAL.SECONDARY_CONTACT_ID%TYPE,
       pn_location_type_id IN REPORT.LOCATION.LOCATION_TYPE_ID%TYPE,
       pv_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
       pn_product_type_id IN REPORT.TERMINAL.PRODUCT_TYPE_ID%TYPE,
       pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
       pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
       pn_avg_amt IN REPORT.TERMINAL.AVG_TRANS_AMT%TYPE,
       pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
       pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
       pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
       pn_vends REPORT.TERMINAL.VENDS%TYPE,
       pv_term_var_1 IN REPORT.TERMINAL.TERM_VAR_1%TYPE,
       pv_term_var_2 IN REPORT.TERMINAL.TERM_VAR_2%TYPE,
       pv_term_var_3 IN REPORT.TERMINAL.TERM_VAR_3%TYPE,
       pv_term_var_4 IN REPORT.TERMINAL.TERM_VAR_4%TYPE,
       pv_term_var_5 IN REPORT.TERMINAL.TERM_VAR_5%TYPE,
       pv_term_var_6 IN REPORT.TERMINAL.TERM_VAR_6%TYPE,
       pv_term_var_7 IN REPORT.TERMINAL.TERM_VAR_7%TYPE,
       pv_term_var_8 IN REPORT.TERMINAL.TERM_VAR_8%TYPE,
	   pd_activate_date IN REPORT.TERMINAL_EPORT.START_DATE%TYPE,
       pn_fee_grace_days IN NUMBER,
	   pc_keep_existing_data IN CHAR DEFAULT 'N',
	   pc_override_payment_schedule IN CHAR DEFAULT 'N',
	   pv_doing_business_as IN REPORT.TERMINAL.DOING_BUSINESS_AS%TYPE DEFAULT NULL,
	   pc_fee_no_trigger_event_flag IN CORP.SERVICE_FEES.NO_TRIGGER_EVENT_FLAG%TYPE DEFAULT NULL,
	   pv_sales_order_number IN REPORT.TERMINAL.SALES_ORDER_NUMBER%TYPE DEFAULT NULL,
	   pv_purchase_order_number IN REPORT.TERMINAL.PURCHASE_ORDER_NUMBER%TYPE DEFAULT NULL)
    IS
      l_eport_id REPORT.EPORT.EPORT_ID%TYPE;
      l_dev_type_id REPORT.EPORT.DEVICE_TYPE_ID%TYPE;
      l_location_id REPORT.TERMINAL.LOCATION_ID%TYPE;
      l_terminal_nbr REPORT.TERMINAL.TERMINAL_NBR%TYPE;
      l_customer_id REPORT.TERMINAL.CUSTOMER_ID%TYPE;
      l_license_id NUMBER;
      l_addr_id REPORT.TERMINAL_ADDR.ADDRESS_ID%TYPE;
      l_last_deactivate_date DATE;
      l_start_date DATE;
      l_cnt NUMBER;
      l_business_unit_id CORP.BUSINESS_UNIT.BUSINESS_UNIT_ID%TYPE;
	  ln_new_pay_sched_id REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE;
      l_lock VARCHAR2(128);
	  ln_user_id REPORT.USER_LOGIN.USER_ID%TYPE := pn_user_id;
	  ln_pc_id REPORT.TERMINAL.PRIMARY_CONTACT_ID%TYPE := pn_pc_id;
	  ln_currency_id CORP.COUNTRY.CURRENCY_ID%TYPE;
	  ln_old_terminal_id REPORT.TERMINAL.TERMINAL_ID%TYPE;
	  lc_keep_existing_data CHAR(1) := pc_keep_existing_data;
	  lv_service_fee_ind REPORT.DEVICE_TYPE.SERVICE_FEE_IND%TYPE;
	  lv_process_fee_ind REPORT.DEVICE_TYPE.PROCESS_FEE_IND%TYPE;
    BEGIN
		IF lc_keep_existing_data NOT IN ('Y', 'N') THEN
			RAISE_APPLICATION_ERROR(-20207, 'Invalid pc_keep_existing_data value: ' || lc_keep_existing_data);
		END IF;
		
		IF pc_override_payment_schedule NOT IN ('Y', 'N') THEN
			RAISE_APPLICATION_ERROR(-20207, 'Invalid pc_override_payment_schedule value: ' || pc_override_payment_schedule);
		END IF;
	
         BEGIN
              SELECT TERMINAL_SEQ.NEXTVAL, E.EPORT_ID, E.DEVICE_TYPE_ID, TERMINAL_ADDR_SEQ.NEXTVAL, DT.SERVICE_FEE_IND, DT.PROCESS_FEE_IND
              INTO pn_terminal_id, l_eport_id, l_dev_type_id, l_addr_id, lv_service_fee_ind, lv_process_fee_ind
			  FROM REPORT.EPORT E
			  JOIN REPORT.DEVICE_TYPE DT ON E.DEVICE_TYPE_ID = DT.DEVICE_TYPE_ID
			  WHERE E.EPORT_SERIAL_NUM = pv_device_serial_cd;
         EXCEPTION
              WHEN NO_DATA_FOUND THEN
                   RAISE_APPLICATION_ERROR(-20201, 'Invalid eport serial number');
              WHEN OTHERS THEN
                   RAISE;
         END;
		 
		 l_lock := GLOBALS_PKG.REQUEST_LOCK('EPORT.EPORT_ID', l_eport_id);		 
         -- CHECK THAT EPORT IS INACTIVE
         SELECT NVL(MAX(NVL(END_DATE, MAX_DATE)), MIN_DATE)
           INTO l_last_deactivate_date
           FROM REPORT.TERMINAL_EPORT 
           WHERE EPORT_ID = l_eport_id;
		   
		SELECT MAX(TERMINAL_ID)
		INTO ln_old_terminal_id
		FROM (
			SELECT TERMINAL_ID
			FROM REPORT.TERMINAL_EPORT
			WHERE EPORT_ID = l_eport_id
			ORDER BY NVL(END_DATE, MAX_DATE) DESC, CREATE_DT DESC
		) WHERE ROWNUM = 1;
		 
		IF lc_keep_existing_data = 'Y' AND ln_old_terminal_id IS NULL THEN
			lc_keep_existing_data := 'N';
		END IF;
         
         IF l_last_deactivate_date > SYSDATE THEN
              SELECT COUNT(TERMINAL_ID) 
                INTO l_cnt 
                FROM USER_TERMINAL 
               WHERE USER_ID = ln_user_id 
                 AND TERMINAL_ID IN(SELECT TERMINAL_ID FROM TERMINAL_EPORT WHERE EPORT_ID = l_eport_id AND NVL(END_DATE, MAX_DATE) > SYSDATE);
             IF l_cnt > 0 THEN --USER CAN VIEW TERMINAL
                  RAISE_APPLICATION_ERROR(-20205, 'Eport has already been activated');
             ELSE -- THIS MAY INDICATE THAT SOMEONE ACTIVATED THE WRONG TERMINAL
                  RAISE_APPLICATION_ERROR(-20206, 'Eport already in use');
             END IF;
         ELSIF l_last_deactivate_date > pd_activate_date THEN
            l_start_date := l_last_deactivate_date;
         ELSE
            l_start_date := pd_activate_date;
         END IF;
         --VERIFY DEALER
         SELECT COUNT(DEALER_ID) INTO l_cnt FROM CORP.DEALER_EPORT WHERE DEALER_ID = pn_dealer_id AND EPORT_ID = l_eport_id;
         IF l_cnt = 0 THEN
              RAISE_APPLICATION_ERROR(-20202, 'Invalid dealer specified');
         END IF;
         --CREATE TERMINAL_NBR
         SELECT 'T0'|| TO_CHAR(TERMINAL_NBR_SEQ.NEXTVAL, 'FM99000000') INTO l_terminal_nbr FROM DUAL;
         
         --INSERT INTO TERMINAL, LOCATION, TERMINAL_ADDR
         IF pn_customer_bank_id <> 0 THEN
             BEGIN
                  SELECT CUSTOMER_ID INTO l_customer_id FROM CORP.CUSTOMER_BANK WHERE CUSTOMER_BANK_ID = pn_customer_bank_id;
             EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                       RAISE_APPLICATION_ERROR(-20203, 'Invalid customer bank');
                  WHEN OTHERS THEN
                       RAISE;
             END;
         ELSE
              BEGIN
                  SELECT CUSTOMER_ID INTO l_customer_id FROM USER_LOGIN WHERE USER_ID = ln_pc_id AND CUSTOMER_ID <> 0;
             EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                       RAISE_APPLICATION_ERROR(-20203, 'Invalid primary contact');
                  WHEN OTHERS THEN
                       RAISE;
             END;
         END IF;
		 
		 IF ln_user_id = 0 THEN
			SELECT MAX(USER_ID)
			INTO ln_user_id
			FROM CORP.CUSTOMER
			WHERE CUSTOMER_ID = l_customer_id;
			
			ln_pc_id := ln_user_id;
		 END IF;
		 
         BEGIN
              SELECT LICENSE_ID INTO l_license_id FROM (SELECT LICENSE_ID FROM CORP.CUSTOMER_LICENSE CL, CORP.LICENSE_NBR LN
                  WHERE CL.LICENSE_NBR = LN.LICENSE_NBR AND CL.CUSTOMER_ID = l_customer_id ORDER BY RECEIVED DESC) WHERE ROWNUM = 1;
         EXCEPTION
              WHEN NO_DATA_FOUND THEN
                   RAISE_APPLICATION_ERROR(-20204, 'No license agreement found for this customer');
              WHEN OTHERS THEN
                   RAISE;
         END;
         BEGIN
              SELECT LICENSE_ID 
                INTO l_license_id 
                FROM (SELECT LICENSE_ID 
                        FROM CORP.DEALER_LICENSE 
                       WHERE DEALER_ID = pn_dealer_id
                         AND SYSDATE >= NVL(START_DATE, MIN_DATE)
                         AND SYSDATE < NVL(END_DATE, MAX_DATE)
                       ORDER BY START_DATE DESC, LICENSE_ID DESC)
               WHERE ROWNUM = 1;
         EXCEPTION
              WHEN NO_DATA_FOUND THEN
                   RAISE_APPLICATION_ERROR(-20204, 'No license agreement found for this dealer');
              WHEN OTHERS THEN
                   RAISE;
         END;
		 
		 IF lc_keep_existing_data = 'N' OR pc_override_payment_schedule = 'Y' THEN
			 l_business_unit_id := FIND_BUSINESS_UNIT(l_eport_id, l_customer_id);
			 SELECT MAX(PAYMENT_SCHEDULE_ID)
			  INTO ln_new_pay_sched_id
			  FROM CORP.PAYMENT_SCHEDULE
			 WHERE PAYMENT_SCHEDULE_ID = pn_pay_sched_id
			   AND SELECTABLE = 'Y';
			 IF l_business_unit_id IN(2,3,5) OR ln_new_pay_sched_id IS NULL THEN
				SELECT DEFAULT_PAYMENT_SCHEDULE_ID
				  INTO ln_new_pay_sched_id
				  FROM CORP.BUSINESS_UNIT
				 WHERE BUSINESS_UNIT_ID = l_business_unit_id;
			 END IF;
		 END IF;
		 
		 IF lc_keep_existing_data = 'N' THEN
			 INSERT INTO TERMINAL_ADDR(ADDRESS_ID, CUSTOMER_ID, ADDRESS1, CITY, STATE, ZIP, COUNTRY_CD)
				 VALUES(l_addr_id, l_customer_id, pv_address1, pv_city, pv_state_cd, pv_postal, pv_country_cd);
			 BEGIN
				 SELECT LOCATION_ID INTO l_location_id FROM LOCATION WHERE EQL(LOCATION_NAME, pv_location_name) = -1 AND EPORT_ID =  l_eport_id AND TERMINAL_ID = 0;
				 UPDATE LOCATION SET (DESCRIPTION, ADDRESS_ID, UPD_DATE, TERMINAL_ID, LOCATION_TYPE_ID, LOCATION_TYPE_SPECIFY) =
					   (SELECT pv_location_details, l_addr_id, SYSDATE, pn_terminal_id, pn_location_type_id,pv_location_type_specific FROM DUAL) WHERE LOCATION_ID = l_location_id;
			 EXCEPTION
				  WHEN NO_DATA_FOUND THEN
					  SELECT LOCATION_SEQ.NEXTVAL INTO l_location_id FROM DUAL;
					   INSERT INTO LOCATION(LOCATION_ID, LOCATION_NAME, DESCRIPTION, ADDRESS_ID, CREATE_BY, TERMINAL_ID, EPORT_ID, LOCATION_TYPE_ID, LOCATION_TYPE_SPECIFY)
							  VALUES(l_location_id, pv_location_name, pv_location_details, l_addr_id, ln_user_id, pn_terminal_id, l_eport_id, pn_location_type_id, pv_location_type_specific);
				  WHEN OTHERS THEN
					   RAISE;
			 END;
			 
			 SELECT NVL(CURRENCY_ID, 1)
			 INTO ln_currency_id
			 FROM CORP.COUNTRY
			 WHERE COUNTRY_CD = pv_country_cd;
		 
			 INSERT INTO TERMINAL(
				TERMINAL_ID,
				TERMINAL_NBR,
				TERMINAL_NAME,
				EPORT_ID,
				CUSTOMER_ID,
				LOCATION_ID,
				ASSET_NBR,
				MACHINE_ID,
				TELEPHONE,
				PREFIX,
				PRODUCT_TYPE_ID,
				PRODUCT_TYPE_SPECIFY,
				PRIMARY_CONTACT_ID,
				SECONDARY_CONTACT_ID,
				AUTHORIZATION_MODE,
				AVG_TRANS_AMT,
				TIME_ZONE_ID,
				DEX_DATA,
				RECEIPT,
				VENDS,
				TERM_VAR_1,
				TERM_VAR_2,
				TERM_VAR_3,
				TERM_VAR_4,
				TERM_VAR_5,
				TERM_VAR_6,
				TERM_VAR_7,
				TERM_VAR_8,
				BUSINESS_UNIT_ID,
				PAYMENT_SCHEDULE_ID,
				FEE_CURRENCY_ID,
				DOING_BUSINESS_AS,
				SALES_ORDER_NUMBER,
				PURCHASE_ORDER_NUMBER)
			 SELECT
				pn_terminal_id,
				l_terminal_nbr,
				l_terminal_nbr,
				l_eport_id,
				l_customer_id,
				l_location_id,
				pv_asset_nbr,
				pn_machine_id,
				pv_telephone,
				pv_prefix,
				NVL(pn_product_type_id, 0),
				pn_product_type_specific,
				ln_pc_id,
				pn_sc_id,
				pc_auth_mode,
				NVL(pn_avg_amt, 0),
				pn_time_zone_id,
				pc_dex_data,
				pc_receipt,
				pn_vends,
				pv_term_var_1,
				pv_term_var_2,
				pv_term_var_3,
				pv_term_var_4,
				pv_term_var_5,
				pv_term_var_6,
				pv_term_var_7,
				pv_term_var_8,
				l_business_unit_id,
				ln_new_pay_sched_id,
				ln_currency_id,
				pv_doing_business_as,
				pv_sales_order_number,
				pv_purchase_order_number
			  FROM DUAL;
		 ELSE
			INSERT INTO TERMINAL(
				TERMINAL_ID,
				TERMINAL_NBR,
				TERMINAL_NAME,
				EPORT_ID,
				CUSTOMER_ID,
				LOCATION_ID,
				ASSET_NBR,
				MACHINE_ID,
				TELEPHONE,
				PREFIX,
				PRODUCT_TYPE_ID,
				PRODUCT_TYPE_SPECIFY,
				PRIMARY_CONTACT_ID,
				SECONDARY_CONTACT_ID,
				AUTHORIZATION_MODE,
				AVG_TRANS_AMT,
				TIME_ZONE_ID,
				DEX_DATA,
				RECEIPT,
				VENDS,
				TERM_VAR_1,
				TERM_VAR_2,
				TERM_VAR_3,
				TERM_VAR_4,
				TERM_VAR_5,
				TERM_VAR_6,
				TERM_VAR_7,
				TERM_VAR_8,
				BUSINESS_UNIT_ID,
				PAYMENT_SCHEDULE_ID,
				FEE_CURRENCY_ID,
				DOING_BUSINESS_AS,
				SALES_ORDER_NUMBER,
				PURCHASE_ORDER_NUMBER)
			SELECT
				pn_terminal_id,
				l_terminal_nbr,
				l_terminal_nbr,
				l_eport_id,
				l_customer_id,
				LOCATION_ID,
				ASSET_NBR,
				MACHINE_ID,
				TELEPHONE,
				PREFIX,
				PRODUCT_TYPE_ID,
				PRODUCT_TYPE_SPECIFY,
				PRIMARY_CONTACT_ID,
				SECONDARY_CONTACT_ID,
				AUTHORIZATION_MODE,
				AVG_TRANS_AMT,
				TIME_ZONE_ID,
				DEX_DATA,
				RECEIPT,
				VENDS,
				TERM_VAR_1,
				TERM_VAR_2,
				TERM_VAR_3,
				TERM_VAR_4,
				TERM_VAR_5,
				TERM_VAR_6,
				TERM_VAR_7,
				TERM_VAR_8,
				BUSINESS_UNIT_ID,
				DECODE(pc_override_payment_schedule, 'N', PAYMENT_SCHEDULE_ID, ln_new_pay_sched_id),
				FEE_CURRENCY_ID,
				DOING_BUSINESS_AS,
				SALES_ORDER_NUMBER,
				PURCHASE_ORDER_NUMBER
			FROM REPORT.TERMINAL
			WHERE TERMINAL_ID = ln_old_terminal_id;
		 END IF;
              
		IF lc_keep_existing_data = 'N' THEN
			IF pn_region_id != 0 THEN
				INSERT INTO REPORT.TERMINAL_REGION(TERMINAL_ID, REGION_ID)
				  VALUES(pn_terminal_id, pn_region_id); 
			END IF;
		ELSE
			INSERT INTO REPORT.TERMINAL_REGION(TERMINAL_ID, REGION_ID)
			SELECT pn_terminal_id, REGION_ID
			FROM REPORT.TERMINAL_REGION
			WHERE TERMINAL_ID = ln_old_terminal_id;
		END IF;
            
         INSERT INTO REPORT.TERMINAL_EPORT(EPORT_ID, TERMINAL_ID, START_DATE, END_DATE)
            VALUES(l_eport_id, pn_terminal_id, l_start_date, NULL);
        
         --UPDATE DEALER_EPORT
         UPDATE CORP.DEALER_EPORT SET ACTIVATE_DATE = l_start_date WHERE DEALER_ID = pn_dealer_id AND EPORT_ID = l_eport_id;
         --INSERT INTO USER_TERMINAL
         INSERT INTO USER_TERMINAL(USER_ID, TERMINAL_ID, ALLOW_EDIT)
         /* A much faster way - BSK 04/10/2007
              SELECT USER_ID, pn_terminal_id, 'Y' FROM USER_LOGIN WHERE CAN_ADMIN_USER(ln_user_id, USER_ID) = 'Y'; */
             SELECT USER_ID, pn_terminal_id, 'Y'
               FROM REPORT.USER_LOGIN u
               CONNECT BY PRIOR u.admin_id = u.user_id
               START WITH u.user_id = ln_user_id
             UNION
             SELECT a.USER_ID, pn_terminal_id, 'Y'
               FROM REPORT.USER_LOGIN a
              INNER JOIN REPORT.USER_PRIVS up
                 ON a.USER_ID = up.user_id
              WHERE a.CUSTOMER_ID = l_customer_id
                AND up.priv_id IN(5,8);
    
         --INSERT INTO CORP.CUSTOMER_BANK_TERMINAL
         IF pn_customer_bank_id <> 0 THEN
             INSERT INTO CORP.CUSTOMER_BANK_TERMINAL(TERMINAL_ID, CUSTOMER_BANK_ID, START_DATE)
               VALUES(pn_terminal_id, pn_customer_bank_id, NULL); --l_start_date); : Use NULL (or MIN_DATE) instead
         END IF;
		 
		 IF lv_service_fee_ind = 'Y' THEN
			 --INSERT INTO SERVICE_FEES
			 -- frequency interval so that first active day has a service fee (BSK 09-14-04)
			 -- made start dates NULL (or MIN_DATE)
			 INSERT INTO CORP.SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID, FEE_AMOUNT, FREQUENCY_ID, LAST_PAYMENT, START_DATE, FEE_PERCENT, GRACE_PERIOD_DATE, NO_TRIGGER_EVENT_FLAG)
				  SELECT CORP.SERVICE_FEE_SEQ.NEXTVAL, pn_terminal_id, lsf.FEE_ID,
						 DECODE(lsf.FEE_ID, 8, 0, lsf.AMOUNT), lsf.FREQUENCY_ID,
						 CASE 
							WHEN F.INITIATION_TYPE_CD = 'G' AND l_start_date + pn_fee_grace_days > SYSDATE THEN NULL
							WHEN lsf.START_IMMEDIATELY_FLAG = 'Y' THEN l_start_date
							ELSE NULL
						 END,
						 l_start_date,
						 DECODE(lsf.FEE_ID, 8, lsf.AMOUNT, NULL),
						 DECODE(F.INITIATION_TYPE_CD, 'G', l_start_date + pn_fee_grace_days),
						 pc_fee_no_trigger_event_flag
					FROM CORP.LICENSE_SERVICE_FEES lsf
					JOIN CORP.FEES F ON lsf.FEE_ID = F.FEE_ID
				   WHERE lsf.LICENSE_ID = l_license_id
					 AND (pc_dex_data IN('A', 'D') OR lsf.FEE_ID != 4);
			 IF pc_dex_data IN('A', 'D') THEN
				 INSERT INTO CORP.SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID, FEE_AMOUNT, FREQUENCY_ID, START_DATE)
					 SELECT CORP.SERVICE_FEE_SEQ.NEXTVAL, pn_terminal_id, 4,
						   4.00, f.FREQUENCY_ID, l_start_date
					  FROM CORP.FREQUENCY f
					 WHERE f.FREQUENCY_ID = 2
						 AND NOT EXISTS(SELECT 1 
										  FROM CORP.LICENSE_SERVICE_FEES lsf 
										 WHERE lsf.LICENSE_ID = l_license_id
										   AND lsf.FEE_ID = 4);
			 END IF;
		 END IF;
		 
		 IF lv_process_fee_ind = 'Y' THEN
			--INSERT INTO PROCESS_FEES (Added FEE_AMOUNT - BSK 09-17-04)
			INSERT INTO CORP.PROCESS_FEES(PROCESS_FEE_ID, TERMINAL_ID, TRANS_TYPE_ID, FEE_PERCENT, FEE_AMOUNT, MIN_AMOUNT, START_DATE)
			SELECT CORP.PROCESS_FEE_SEQ.NEXTVAL, pn_terminal_id, TRANS_TYPE_ID, FEE_PERCENT, FEE_AMOUNT, MIN_AMOUNT, NULL
			FROM CORP.LICENSE_PROCESS_FEES WHERE LICENSE_ID = l_license_id;
		 END IF;
    END;

    PROCEDURE INTERNAL_UPDATE_TERMINAL(
       pn_terminal_id IN REPORT.TERMINAL.TERMINAL_ID%TYPE,
       pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
       pn_machine_id IN REPORT.TERMINAL.MACHINE_ID%TYPE,
       pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
       pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
       pn_region_id IN REPORT.REGION.REGION_ID%TYPE,
       pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
       pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
       pn_address_id REPORT.TERMINAL_ADDR.ADDRESS_ID%TYPE,
       pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
       pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
       pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
       pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
       pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
       pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
       pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
       pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
       pn_pc_id IN REPORT.TERMINAL.PRIMARY_CONTACT_ID%TYPE,
       pn_sc_id IN REPORT.TERMINAL.SECONDARY_CONTACT_ID%TYPE,
       pn_location_type_id IN REPORT.LOCATION.LOCATION_TYPE_ID%TYPE,
       pn_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
       pn_product_type_id IN REPORT.TERMINAL.PRODUCT_TYPE_ID%TYPE,
       pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
       pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
       pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
       pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
       pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
       pn_vends REPORT.TERMINAL.VENDS%TYPE,
       pv_term_var_1 REPORT.TERMINAL.term_var_1%TYPE,
       pv_term_var_2 REPORT.TERMINAL.term_var_2%TYPE,
       pv_term_var_3 REPORT.TERMINAL.term_var_3%TYPE,
       pv_term_var_4 REPORT.TERMINAL.term_var_4%TYPE,
       pv_term_var_5 REPORT.TERMINAL.term_var_5%TYPE,
       pv_term_var_6 REPORT.TERMINAL.term_var_6%TYPE,
       pv_term_var_7 REPORT.TERMINAL.term_var_7%TYPE,
       pv_term_var_8 REPORT.TERMINAL.term_var_8%TYPE,
	   pv_doing_business_as IN REPORT.TERMINAL.DOING_BUSINESS_AS%TYPE DEFAULT NULL)
    IS
      pv_location_name_id REPORT.TERMINAL.LOCATION_ID%TYPE;
      l_customer_id REPORT.TERMINAL.CUSTOMER_ID%TYPE;
      l_old_cb_id NUMBER;
      l_old_lname REPORT.LOCATION.LOCATION_NAME%TYPE;
      l_old_ldesc REPORT.LOCATION.DESCRIPTION%TYPE;
      l_new_address_id REPORT.TERMINAL_ADDR.ADDRESS_ID%TYPE;
      l_old_address_id REPORT.LOCATION.ADDRESS_ID%TYPE;
      l_old_dex REPORT.TERMINAL.DEX_DATA%TYPE;
      isnew BOOLEAN;
      l_date DATE := SYSDATE;
	  l_lock VARCHAR2(128);
      ln_business_unit_id REPORT.TERMINAL.BUSINESS_UNIT_ID%TYPE;
      ln_old_pay_sched_id REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE;
      ln_old_region_id REPORT.REGION.REGION_ID%TYPE;
	  ln_new_pay_sched_id REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE;
	  ln_currency_id CORP.COUNTRY.CURRENCY_ID%TYPE;
    BEGIN
		 l_lock := GLOBALS_PKG.REQUEST_LOCK('TERMINAL.TERMINAL_ID', pn_terminal_id);	
         -- if a new location is named than create it
         SELECT T.LOCATION_ID, T.CUSTOMER_ID, L.LOCATION_NAME, L.DESCRIPTION, L.ADDRESS_ID, CB.CUSTOMER_BANK_ID, T.DEX_DATA, T.BUSINESS_UNIT_ID, T.PAYMENT_SCHEDULE_ID, NVL(TR.REGION_ID, 0)
           INTO pv_location_name_id, l_customer_id, l_old_lname, l_old_ldesc, l_old_address_id, l_old_cb_id, l_old_dex, ln_business_unit_id, ln_old_pay_sched_id, ln_old_region_id
           FROM REPORT.TERMINAL T
           LEFT OUTER JOIN REPORT.LOCATION L ON T.LOCATION_ID = L.LOCATION_ID
           LEFT OUTER JOIN CORP.VW_CURRENT_BANK_ACCT CB ON T.TERMINAL_ID = CB.TERMINAL_ID
           LEFT OUTER JOIN REPORT.TERMINAL_REGION TR ON T.TERMINAL_ID = TR.TERMINAL_ID
          WHERE T.TERMINAL_ID = pn_terminal_id;
         IF EQL(l_old_lname, pv_location_name) <> -1 THEN  -- location has changed
             UPDATE REPORT.LOCATION SET status = 'D' WHERE LOCATION_ID = pv_location_name_id;
            INSERT INTO REPORT.TERMINAL_CHANGES (TERMINAL_ID, ATTRIBUTE, OLD_VALUE, NEW_VALUE)
                   VALUES(pn_terminal_id, 'LOCATION',l_old_lname, pv_location_name);
            BEGIN
               SELECT LOCATION_ID INTO pv_location_name_id FROM REPORT.LOCATION WHERE LOCATION_NAME = pv_location_name AND TERMINAL_ID = pn_terminal_id;
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                     isnew := true;
               WHEN OTHERS THEN
                  RAISE;
            END;
         END IF;
         --RECORD CHANGES (SO USALIVE CAN BE UPDATED)
         DECLARE
             l_z TERMINAL.TIME_ZONE_ID%TYPE;
             l_cnt NUMBER;
         BEGIN
              SELECT TIME_ZONE_ID
                INTO l_z 
                FROM REPORT.TERMINAL 
               WHERE TERMINAL_ID = pn_terminal_id;
             IF EQL(l_z, pn_time_zone_id) <> -1 THEN
                 UPDATE REPORT.TERMINAL_CHANGES SET NEW_VALUE = pn_time_zone_id WHERE TERMINAL_ID = pn_terminal_id
                   AND ATTRIBUTE = 'TIME_ZONE' RETURNING 1 INTO l_cnt;
                IF NVL(l_cnt, 0) = 0 THEN
                    INSERT INTO REPORT.TERMINAL_CHANGES (TERMINAL_ID, ATTRIBUTE, OLD_VALUE, NEW_VALUE)
                   VALUES(pn_terminal_id, 'TIME_ZONE',l_z, pn_time_zone_id);
                END IF;
             END IF;
         END;
         -- UPDATE ADDRESS
         IF pn_address_id IS NULL THEN    -- none specified
            l_new_address_id := NULL;
         ELSIF pn_address_id = 0 THEN    --IS NEW
            SELECT REPORT.TERMINAL_ADDR_SEQ.NEXTVAL INTO l_new_address_id FROM DUAL;
            INSERT INTO REPORT.TERMINAL_ADDR(ADDRESS_ID, CUSTOMER_ID, ADDRESS1, CITY, STATE, ZIP, COUNTRY_CD)
                   VALUES(l_new_address_id, l_customer_id, pv_address1, pv_city, pv_state_cd, pv_postal, pv_country_cd);
         ELSE
             UPDATE REPORT.TERMINAL_ADDR SET ADDRESS1 = pv_address1, CITY = pv_city, STATE = pv_state_cd, ZIP = pv_postal, COUNTRY_CD = pv_country_cd
                WHERE ADDRESS_ID = pn_address_id AND (EQL(ADDRESS1, pv_address1) = 0
                OR EQL(CITY, pv_city) = 0 OR EQL(STATE, pv_state_cd) = 0 OR EQL(ZIP, pv_postal) = 0 OR EQL(COUNTRY_CD, pv_country_cd) = 0);
            l_new_address_id := pn_address_id;
         END IF;
         IF isnew THEN
             SELECT REPORT.LOCATION_SEQ.NEXTVAL INTO pv_location_name_id FROM DUAL;
             INSERT INTO REPORT.LOCATION(LOCATION_ID, LOCATION_NAME, DESCRIPTION, ADDRESS_ID, CREATE_BY, TERMINAL_ID, LOCATION_TYPE_ID, LOCATION_TYPE_SPECIFY)
                   VALUES(pv_location_name_id,pv_location_name,pv_location_details,l_new_address_id,pn_user_id,pn_terminal_id,pn_location_type_id,pn_location_type_specific);
         ELSE
             UPDATE REPORT.LOCATION L SET (DESCRIPTION, ADDRESS_ID, UPD_DATE, LOCATION_TYPE_ID, LOCATION_TYPE_SPECIFY) =
                   (SELECT pv_location_details,l_new_address_id,l_date,NVL(pn_location_type_id, L.LOCATION_TYPE_ID),DECODE(pn_location_type_id, NULL, L.LOCATION_TYPE_SPECIFY, pn_location_type_specific) FROM DUAL) WHERE location_id = pv_location_name_id;
         END IF;
  
         IF pn_pay_sched_id IS NULL THEN
            ln_new_pay_sched_id := ln_old_pay_sched_id;
         ELSIF pn_pay_sched_id != ln_old_pay_sched_id THEN
            SELECT MAX(PAYMENT_SCHEDULE_ID)
              INTO ln_new_pay_sched_id
              FROM CORP.PAYMENT_SCHEDULE
             WHERE PAYMENT_SCHEDULE_ID = pn_pay_sched_id
               AND SELECTABLE = 'Y';
            IF ln_new_pay_sched_id IS NULL THEN
                RAISE_APPLICATION_ERROR(-20217, 'Invalid payment schedule requested');
            ELSIF ln_business_unit_id IN(2,3,5) THEN
                RAISE_APPLICATION_ERROR(-20217, 'Not permitted to change payment schedule as requested');
            END IF;
         ELSE
            ln_new_pay_sched_id := ln_old_pay_sched_id;
         END IF;
		 
		 SELECT NVL(CURRENCY_ID, 1)
		 INTO ln_currency_id
		 FROM CORP.COUNTRY
		 WHERE COUNTRY_CD = pv_country_cd;
		 
         UPDATE REPORT.TERMINAL T SET (LOCATION_ID, ASSET_NBR, MACHINE_ID, TELEPHONE, PREFIX, PRODUCT_TYPE_ID, PRODUCT_TYPE_SPECIFY,
             PRIMARY_CONTACT_ID, SECONDARY_CONTACT_ID, AUTHORIZATION_MODE, TIME_ZONE_ID, DEX_DATA, RECEIPT, VENDS, PAYMENT_SCHEDULE_ID, STATUS,
             TERM_VAR_1, TERM_VAR_2, TERM_VAR_3, TERM_VAR_4, TERM_VAR_5, TERM_VAR_6, TERM_VAR_7, TERM_VAR_8, FEE_CURRENCY_ID, DOING_BUSINESS_AS) =
            (SELECT pv_location_name_id, pv_asset_nbr, pn_machine_id, pv_telephone, pv_prefix, NVL(pn_product_type_id, 0), pn_product_type_specific,
             pn_pc_id, pn_sc_id, pc_auth_mode, pn_time_zone_id, NVL(pc_dex_data, T.DEX_DATA), NVL(pc_receipt, T.RECEIPT), pn_vends, ln_new_pay_sched_id, 'U',
             pv_term_var_1, pv_term_var_2, pv_term_var_3, pv_term_var_4, pv_term_var_5, pv_term_var_6, pv_term_var_7, pv_term_var_8, ln_currency_id, pv_doing_business_as
			 FROM DUAL) 
             WHERE TERMINAL_ID = pn_terminal_id;
         IF NVL(pn_region_id, 0) = ln_old_region_id THEN
            NULL;
         ELSIF ln_old_region_id = 0 THEN
            INSERT INTO REPORT.TERMINAL_REGION(TERMINAL_ID, REGION_ID)
                  VALUES(pn_terminal_id, pn_region_id); 
         ELSIF NVL(pn_region_id, 0) = 0 THEN
            DELETE FROM  REPORT.TERMINAL_REGION
             WHERE TERMINAL_ID = pn_terminal_id;
         ELSE
            UPDATE REPORT.TERMINAL_REGION
               SET REGION_ID = pn_region_id
             WHERE TERMINAL_ID = pn_terminal_id; 
         END IF;
         
         --UPDATE BANK ACCOUNT
         IF EQL(l_old_cb_id, pn_customer_bank_id) = 0 THEN
             DECLARE
                 l_cb_date DATE;
            BEGIN
                UPDATE CORP.CUSTOMER_BANK_TERMINAL SET END_DATE = CASE WHEN END_DATE < l_date THEN END_DATE ELSE l_date END
                 WHERE TERMINAL_ID = pn_terminal_id
                 RETURNING MAX(CASE WHEN END_DATE < l_date THEN END_DATE ELSE l_date END) INTO l_cb_date;
                IF NVL(pn_customer_bank_id, 0) <> 0 THEN
                       INSERT INTO CORP.CUSTOMER_BANK_TERMINAL(CUSTOMER_BANK_TERMINAL_ID, TERMINAL_ID, CUSTOMER_BANK_ID, START_DATE)
                        VALUES(CORP.CUSTOMER_BANK_TERMINAL_SEQ.NEXTVAL, pn_terminal_id, pn_customer_bank_id, l_cb_date);
                END IF;
            END;
         END IF;
         /*
         IF NVL(l_old_cb_id, 0) = 0 AND NVL(pn_customer_bank_id, 0) <> 0 THEN
             DECLARE
                 l_cb_date DATE;
            BEGIN
                 SELECT NVL(MIN(CLOSE_DATE), l_date) INTO l_cb_date FROM TRANS WHERE TERMINAL_ID = pn_terminal_id AND CLOSE_DATE >=
                     (SELECT NVL(MAX(END_DATE), CLOSE_DATE) FROM CORP.CUSTOMER_BANK_TERMINAL
                     WHERE TERMINAL_ID = pn_terminal_id AND END_DATE > NVL(START_DATE, END_DATE));
                 CORP.TERMINAL_REASSIGN_BANK_ACCOUNT(pn_terminal_id, pn_customer_bank_id, l_cb_date);
            END;
         ELSIF EQL(l_old_cb_id, pn_customer_bank_id) = 0 THEN
             CORP.TERMINAL_REASSIGN_BANK_ACCOUNT(pn_terminal_id, pn_customer_bank_id, l_date);
         END IF;
         */
         IF l_old_dex NOT IN('D','A') AND pc_dex_data IN('D','A') THEN
             DECLARE
                ln_service_fee_id CORP.SERVICE_FEES.SERVICE_FEE_ID%TYPE;
                lc_copy CHAR(1);         
             BEGIN
                SELECT SERVICE_FEE_ID, DECODE(END_DATE, NULL, 'N', 'Y')
                  INTO ln_service_fee_id, lc_copy
                  FROM (SELECT SERVICE_FEE_ID, END_DATE
                          FROM CORP.SERVICE_FEES
                         WHERE TERMINAL_ID = pn_terminal_id
                           AND FEE_ID = 4
                         ORDER BY NVL(END_DATE, MAX_DATE) DESC, LAST_PAYMENT DESC, SERVICE_FEE_ID DESC)
                 WHERE ROWNUM = 1;
                IF lc_copy = 'Y' THEN
                INSERT INTO CORP.SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID, FEE_AMOUNT, FREQUENCY_ID, LAST_PAYMENT, START_DATE)
                     SELECT CORP.SERVICE_FEE_SEQ.NEXTVAL, pn_terminal_id, 4, FEE_AMOUNT, FREQUENCY_ID, LAST_PAYMENT, GREATEST(l_date, END_DATE) 
                       FROM CORP.SERVICE_FEES
                      WHERE SERVICE_FEE_ID = ln_service_fee_id;
                END IF;          
             EXCEPTION
                WHEN NO_DATA_FOUND THEN
                     INSERT INTO CORP.SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID, FEE_AMOUNT, FREQUENCY_ID, LAST_PAYMENT, START_DATE)
                         SELECT CORP.SERVICE_FEE_SEQ.NEXTVAL, pn_terminal_id, 4, 4, 2, l_date, l_date FROM DUAL;
             END;
         ELSIF pc_dex_data NOT IN('D','A') AND l_old_dex IN('D','A') THEN
              UPDATE CORP.SERVICE_FEES SET END_DATE = l_date 
               WHERE TERMINAL_ID = pn_terminal_id
                 AND NVL(END_DATE, l_date) >= l_date
                 AND FEE_ID = 4;
         END IF;
    EXCEPTION
         WHEN NO_DATA_FOUND THEN
              RAISE_APPLICATION_ERROR(-20100, 'No such terminal found');
         WHEN OTHERS THEN
             ROLLBACK;
             RAISE;
    END;
    
    PROCEDURE CREATE_TERMINAL(
        pn_terminal_id OUT REPORT.TERMINAL.TERMINAL_ID%TYPE,
        pv_device_serial_cd IN REPORT.EPORT.EPORT_SERIAL_NUM%TYPE,
        pn_dealer_id IN CORP.DEALER.DEALER_ID%TYPE,
        pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
        pn_machine_id IN REPORT.TERMINAL.MACHINE_ID%TYPE,
        pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
        pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
        pn_region_id IN REPORT.REGION.REGION_ID%TYPE,
        pv_region_name IN REPORT.REGION.REGION_NAME%TYPE,
        pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
        pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
        pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
        pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
        pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
        pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
        pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
        pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
        pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_pc_id IN REPORT.TERMINAL.PRIMARY_CONTACT_ID%TYPE,
        pn_sc_id IN REPORT.TERMINAL.SECONDARY_CONTACT_ID%TYPE,
        pn_location_type_id IN REPORT.LOCATION.LOCATION_TYPE_ID%TYPE,
        pv_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
        pn_product_type_id IN REPORT.TERMINAL.PRODUCT_TYPE_ID%TYPE,
        pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
        pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
        pn_avg_amt IN REPORT.TERMINAL.AVG_TRANS_AMT%TYPE,
        pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
        pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
        pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
        pn_vends REPORT.TERMINAL.VENDS%TYPE,
        pv_term_var_1 IN REPORT.TERMINAL.TERM_VAR_1%TYPE,
        pv_term_var_2 IN REPORT.TERMINAL.TERM_VAR_2%TYPE,
        pv_term_var_3 IN REPORT.TERMINAL.TERM_VAR_3%TYPE,
        pv_term_var_4 IN REPORT.TERMINAL.TERM_VAR_4%TYPE,
        pv_term_var_5 IN REPORT.TERMINAL.TERM_VAR_5%TYPE,
        pv_term_var_6 IN REPORT.TERMINAL.TERM_VAR_6%TYPE,
        pv_term_var_7 IN REPORT.TERMINAL.TERM_VAR_7%TYPE,
        pv_term_var_8 IN REPORT.TERMINAL.TERM_VAR_8%TYPE,
		pd_activate_date IN REPORT.TERMINAL_EPORT.START_DATE%TYPE DEFAULT SYSDATE - 30,
        pn_fee_grace_days IN NUMBER DEFAULT 60,
		pv_doing_business_as IN REPORT.TERMINAL.DOING_BUSINESS_AS%TYPE DEFAULT NULL,
		pc_fee_no_trigger_event_flag IN CORP.SERVICE_FEES.NO_TRIGGER_EVENT_FLAG%TYPE DEFAULT NULL,
		pv_sales_order_number IN REPORT.TERMINAL.SALES_ORDER_NUMBER%TYPE DEFAULT NULL,
		pv_purchase_order_number IN REPORT.TERMINAL.PURCHASE_ORDER_NUMBER%TYPE DEFAULT NULL)
    IS
        ln_cnt PLS_INTEGER;
        lv_city REPORT.TERMINAL_ADDR.CITY%TYPE;
        lv_state_cd REPORT.TERMINAL_ADDR.STATE%TYPE;     
        ln_new_region_id REPORT.REGION.REGION_ID%TYPE;        
    BEGIN
        IF pv_state_cd IS NULL OR pv_city IS NULL THEN
            SELECT MAX(CITY), MAX(STATE_CD), MAX(CNT)
              INTO lv_city, lv_state_cd, ln_cnt
              FROM (SELECT P.CITY, S.STATE_CD, 1 CNT
                  FROM FRONT.POSTAL P 
                  JOIN CORP.STATE S ON P.STATE_ID = S.STATE_ID
                WHERE P.POSTAL_CD = pv_postal
                  AND S.COUNTRY_CD = pv_country_cd
                ORDER BY P.POSTAL_ID ASC)
             WHERE ROWNUM = 1;
        ELSE
            SELECT COUNT(*), pv_city, pv_state_cd
              INTO ln_cnt, lv_city, lv_state_cd 
              FROM CORP.STATE 
             WHERE STATE_CD = pv_state_cd
               AND COUNTRY_CD = pv_country_cd; 
        END IF; 
        IF ln_cnt = 0 THEN
            RAISE_APPLICATION_ERROR(-20220, 'Invalid state/country');
            RETURN;
        END IF;
        IF pn_region_id != -1 THEN
            ln_new_region_id := pn_region_id;
        ELSIF TRIM(pv_region_name) IS NULL THEN
            ln_new_region_id := 0;
        ELSE
            REPORT.ADD_REGION(ln_new_region_id, pv_region_name);
        END IF;
        INTERNAL_CREATE_TERMINAL(
            pn_terminal_id,
            pv_device_serial_cd,
            pn_dealer_id,
            pv_asset_nbr,
            pn_machine_id,
            pv_telephone,
            pv_prefix,
            ln_new_region_id,
            pv_location_name,
            pv_location_details,
            pv_address1,
            lv_city,
            lv_state_cd,
            pv_postal,
            pv_country_cd,
            pn_customer_bank_id,
            pn_pay_sched_id,
            pn_user_id,
            pn_pc_id,
            pn_sc_id,
            pn_location_type_id,
            pv_location_type_specific,
            pn_product_type_id,
            pn_product_type_specific,
            pc_auth_mode,
            pn_avg_amt,
            pn_time_zone_id,
            pc_dex_data,
            pc_receipt,
            pn_vends,
            pv_term_var_1,
            pv_term_var_2,
            pv_term_var_3,
            pv_term_var_4,
            pv_term_var_5,
            pv_term_var_6,
            pv_term_var_7,
            pv_term_var_8,
			pd_activate_date,
            pn_fee_grace_days,
			pv_doing_business_as,
			pc_fee_no_trigger_event_flag,
			pv_sales_order_number,
			pv_purchase_order_number);
    END;

    PROCEDURE UPDATE_TERMINAL(
       pn_terminal_id IN REPORT.TERMINAL.TERMINAL_ID%TYPE,
       pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
       pn_machine_id IN REPORT.TERMINAL.MACHINE_ID%TYPE,
       pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
       pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
       pn_region_id IN REPORT.REGION.REGION_ID%TYPE,
       pv_region_name IN REPORT.REGION.REGION_NAME%TYPE,
       pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
       pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
       pn_address_id REPORT.TERMINAL_ADDR.ADDRESS_ID%TYPE,
       pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
       pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
       pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
       pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
       pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
       pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
       pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
       pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
       pn_pc_id IN REPORT.TERMINAL.PRIMARY_CONTACT_ID%TYPE,
       pn_sc_id IN REPORT.TERMINAL.SECONDARY_CONTACT_ID%TYPE,
       pn_location_type_id IN REPORT.LOCATION.LOCATION_TYPE_ID%TYPE,
       pn_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
       pn_product_type_id IN REPORT.TERMINAL.PRODUCT_TYPE_ID%TYPE,
       pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
       pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
       pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
       pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
       pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
       pn_vends REPORT.TERMINAL.VENDS%TYPE,
       pv_term_var_1 REPORT.TERMINAL.term_var_1%TYPE,
       pv_term_var_2 REPORT.TERMINAL.term_var_2%TYPE,
       pv_term_var_3 REPORT.TERMINAL.term_var_3%TYPE,
       pv_term_var_4 REPORT.TERMINAL.term_var_4%TYPE,
       pv_term_var_5 REPORT.TERMINAL.term_var_5%TYPE,
       pv_term_var_6 REPORT.TERMINAL.term_var_6%TYPE,
       pv_term_var_7 REPORT.TERMINAL.term_var_7%TYPE,
       pv_term_var_8 REPORT.TERMINAL.term_var_8%TYPE,
	   pv_doing_business_as IN REPORT.TERMINAL.DOING_BUSINESS_AS%TYPE DEFAULT NULL)
    IS
        ln_cnt PLS_INTEGER;
        lv_city REPORT.TERMINAL_ADDR.CITY%TYPE;
        lv_state_cd REPORT.TERMINAL_ADDR.STATE%TYPE;   
        ln_new_region_id REPORT.REGION.REGION_ID%TYPE;        
    BEGIN
        IF pv_state_cd IS NULL OR pv_city IS NULL THEN
            SELECT MAX(CITY), MAX(STATE_CD), MAX(CNT)
              INTO lv_city, lv_state_cd, ln_cnt
              FROM (SELECT P.CITY, S.STATE_CD, 1 CNT
                  FROM FRONT.POSTAL P 
                  JOIN CORP.STATE S ON P.STATE_ID = S.STATE_ID
                WHERE P.POSTAL_CD = pv_postal
                  AND S.COUNTRY_CD = pv_country_cd
                ORDER BY P.POSTAL_ID ASC)
             WHERE ROWNUM = 1;
        ELSE
            SELECT COUNT(*), pv_city, pv_state_cd
              INTO ln_cnt, lv_city, lv_state_cd
              FROM CORP.STATE 
             WHERE STATE_CD = pv_state_cd
               AND COUNTRY_CD = pv_country_cd; 
        END IF;
        IF ln_cnt = 0 THEN
            RAISE_APPLICATION_ERROR(-20220, 'Invalid state/country');
        ELSE
            IF pn_region_id != -1 THEN
                ln_new_region_id := pn_region_id;
            ELSIF TRIM(pv_region_name) IS NULL THEN
                ln_new_region_id := 0;
            ELSE
                REPORT.ADD_REGION(ln_new_region_id, pv_region_name);
            END IF;
            INTERNAL_UPDATE_TERMINAL(
               pn_terminal_id,
               pv_asset_nbr,
               pn_machine_id,
               pv_telephone,
               pv_prefix,
               ln_new_region_id,
               pv_location_name,
               pv_location_details,
               pn_address_id,
               pv_address1,
               lv_city,
               lv_state_cd,
               pv_postal,
               pv_country_cd,
               pn_customer_bank_id,
               pn_pay_sched_id,
               pn_user_id,
               pn_pc_id,
               pn_sc_id,
               pn_location_type_id,
               pn_location_type_specific,
               pn_product_type_id,
               pn_product_type_specific,
               pc_auth_mode,
               pn_time_zone_id,
               pc_dex_data,
               pc_receipt,
               pn_vends,
               pv_term_var_1,
               pv_term_var_2,
               pv_term_var_3,
               pv_term_var_4,
               pv_term_var_5,
               pv_term_var_6,
               pv_term_var_7,
               pv_term_var_8,
			   pv_doing_business_as);
        END IF;
    END;
        
    PROCEDURE CREATE_TERMINAL_MASS(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
		pn_terminal_id OUT REPORT.TERMINAL.TERMINAL_ID%TYPE,
        pv_device_serial_cd IN REPORT.EPORT.EPORT_SERIAL_NUM%TYPE,
        pn_dealer_id IN CORP.DEALER.DEALER_ID%TYPE,
        pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
        pv_machine_make REPORT.MACHINE.MAKE%TYPE,
		pv_machine_model REPORT.MACHINE.MODEL%TYPE,
        pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
        pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
        pv_region_name IN REPORT.REGION.REGION_NAME%TYPE,
        pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
        pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
        pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
        pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
        pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
        pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
        pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
        pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
        pv_location_type_name REPORT.LOCATION_TYPE.LOCATION_TYPE_NAME%TYPE,
        pn_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
        pv_product_type_name REPORT.PRODUCT_TYPE.PRODUCT_TYPE_NAME%TYPE,
        pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
        pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
        pn_avg_amt IN REPORT.TERMINAL.AVG_TRANS_AMT%TYPE,
        pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
        pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
        pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
        pn_vends REPORT.TERMINAL.VENDS%TYPE,
        pv_term_var_1 REPORT.TERMINAL.term_var_1%TYPE,
        pv_term_var_2 REPORT.TERMINAL.term_var_2%TYPE,
        pv_term_var_3 REPORT.TERMINAL.term_var_3%TYPE,
        pv_term_var_4 REPORT.TERMINAL.term_var_4%TYPE,
        pv_term_var_5 REPORT.TERMINAL.term_var_5%TYPE,
        pv_term_var_6 REPORT.TERMINAL.term_var_6%TYPE,
        pv_term_var_7 REPORT.TERMINAL.term_var_7%TYPE,
        pv_term_var_8 REPORT.TERMINAL.term_var_8%TYPE,
		pd_activate_date IN REPORT.TERMINAL_EPORT.START_DATE%TYPE DEFAULT SYSDATE - 30,
        pn_fee_grace_days IN NUMBER DEFAULT 60,
		pc_keep_existing_data IN CHAR DEFAULT 'N',
		pc_override_payment_schedule IN CHAR DEFAULT 'N',
		pv_doing_business_as IN REPORT.TERMINAL.DOING_BUSINESS_AS%TYPE DEFAULT NULL,
		pc_fee_no_trigger_event_flag IN CORP.SERVICE_FEES.NO_TRIGGER_EVENT_FLAG%TYPE DEFAULT NULL,
		pv_sales_order_number IN REPORT.TERMINAL.SALES_ORDER_NUMBER%TYPE DEFAULT NULL,
		pv_purchase_order_number IN REPORT.TERMINAL.PURCHASE_ORDER_NUMBER%TYPE DEFAULT NULL)
    IS
        ln_machine_id REPORT.MACHINE.MACHINE_ID%TYPE;
        ln_product_type_id REPORT.PRODUCT_TYPE.PRODUCT_TYPE_ID%TYPE;
        ln_location_type_id REPORT.LOCATION_TYPE.LOCATION_TYPE_ID%TYPE;	
        ln_cnt PLS_INTEGER;
        lv_city REPORT.TERMINAL_ADDR.CITY%TYPE;
        lv_state_cd REPORT.TERMINAL_ADDR.STATE%TYPE;     
        ln_region_id REPORT.REGION.REGION_ID%TYPE;        
    BEGIN
		IF pc_keep_existing_data = 'N' THEN
			BEGIN
				SELECT MAX(MACHINE_ID)
				  INTO ln_machine_id
				  FROM REPORT.MACHINE
				 WHERE UPPER(MAKE) = UPPER(pv_machine_make)
				   AND UPPER(MODEL) = UPPER(pv_machine_model)
				   AND STATUS IN('A','I');
			EXCEPTION
				WHEN NO_DATA_FOUND THEN
					REPORT.MACHINE_INS(ln_machine_id, pv_machine_make, pv_machine_model, pn_user_id);
			END;
			BEGIN
				IF pv_location_type_name IS NOT NULL AND LENGTH(TRIM(pv_location_type_name)) > 0 THEN
					SELECT LOCATION_TYPE_ID
					  INTO ln_location_type_id
					  FROM REPORT.LOCATION_TYPE
					 WHERE UPPER(LOCATION_TYPE_NAME) = UPPER(pv_location_type_name)
					   AND STATUS IN('A','I');
				ELSE
					ln_location_type_id := 0;
				END IF;
			EXCEPTION
				WHEN NO_DATA_FOUND THEN
					RAISE_APPLICATION_ERROR(-20211, 'Invalid location type name');
			END;
			BEGIN
				SELECT PRODUCT_TYPE_ID
				  INTO ln_product_type_id
				  FROM REPORT.PRODUCT_TYPE
				 WHERE UPPER(PRODUCT_TYPE_NAME) = UPPER(pv_product_type_name)
				   AND STATUS IN('A','I');
			EXCEPTION
				WHEN NO_DATA_FOUND THEN
					RAISE_APPLICATION_ERROR(-20212, 'Invalid product type name');
			END;

			 IF pv_state_cd IS NULL OR pv_city IS NULL THEN
				SELECT MAX(CITY), MAX(STATE_CD), MAX(CNT)
				  INTO lv_city, lv_state_cd, ln_cnt
				  FROM (SELECT P.CITY, S.STATE_CD, 1 CNT
					  FROM FRONT.POSTAL P 
					  JOIN CORP.STATE S ON P.STATE_ID = S.STATE_ID
					WHERE P.POSTAL_CD = pv_postal
					  AND S.COUNTRY_CD = pv_country_cd
					ORDER BY P.POSTAL_ID ASC)
				 WHERE ROWNUM = 1;
			ELSE
				SELECT COUNT(*), pv_city, pv_state_cd
				  INTO ln_cnt, lv_city, lv_state_cd 
				  FROM CORP.STATE 
				 WHERE STATE_CD = pv_state_cd
				   AND COUNTRY_CD = pv_country_cd; 
			END IF; 
			IF ln_cnt = 0 THEN
				RAISE_APPLICATION_ERROR(-20220, 'Invalid state/country');
				RETURN;
			END IF;
			IF TRIM(pv_region_name) IS NOT NULL THEN
				REPORT.ADD_REGION(ln_region_id, pv_region_name);
			ELSE
				ln_region_id := 0;
			END IF;
		END IF;
        INTERNAL_CREATE_TERMINAL(
               pn_terminal_id,
               pv_device_serial_cd,
               pn_dealer_id,
               pv_asset_nbr,
               ln_machine_id,
               pv_telephone,
               pv_prefix,
               ln_region_id,
               pv_location_name,
               pv_location_details,
               pv_address1,
               lv_city,
               lv_state_cd,
               pv_postal,
               pv_country_cd,
               pn_customer_bank_id,
               pn_pay_sched_id,
               pn_user_id,
               pn_user_id,
               NULL,
               ln_location_type_id,
               pn_location_type_specific,
               ln_product_type_id,
               pn_product_type_specific,
               pc_auth_mode,
               pn_avg_amt,
               pn_time_zone_id,
               pc_dex_data,
               pc_receipt,
               pn_vends,
               pv_term_var_1,
               pv_term_var_2,
               pv_term_var_3,
               pv_term_var_4,
               pv_term_var_5,
               pv_term_var_6,
               pv_term_var_7,
               pv_term_var_8,
			   pd_activate_date,
               pn_fee_grace_days,
			   pc_keep_existing_data,
			   pc_override_payment_schedule,
			   pv_doing_business_as,
			   pc_fee_no_trigger_event_flag,
			   pv_sales_order_number,
			   pv_purchase_order_number);
    END;
    
    PROCEDURE UPDATE_TERMINAL_MASS(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
		pn_terminal_id IN REPORT.TERMINAL.TERMINAL_ID%TYPE,
        pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
        pv_machine_make REPORT.MACHINE.MAKE%TYPE,
		pv_machine_model REPORT.MACHINE.MODEL%TYPE,
        pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
        pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
        pv_region_name IN REPORT.REGION.REGION_NAME%TYPE,
        pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
        pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
        pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
        pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
        pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
        pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
        pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
        pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
        pn_location_type_id REPORT.LOCATION_TYPE.LOCATION_TYPE_ID%TYPE,
		pv_location_type_name REPORT.LOCATION_TYPE.LOCATION_TYPE_NAME%TYPE,
        pn_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
        pv_product_type_name REPORT.PRODUCT_TYPE.PRODUCT_TYPE_NAME%TYPE,
        pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
        pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
        pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
        pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
        pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
        pn_vends REPORT.TERMINAL.VENDS%TYPE,
        pv_term_var_1 REPORT.TERMINAL.term_var_1%TYPE,
        pv_term_var_2 REPORT.TERMINAL.term_var_2%TYPE,
        pv_term_var_3 REPORT.TERMINAL.term_var_3%TYPE,
        pv_term_var_4 REPORT.TERMINAL.term_var_4%TYPE,
        pv_term_var_5 REPORT.TERMINAL.term_var_5%TYPE,
        pv_term_var_6 REPORT.TERMINAL.term_var_6%TYPE,
        pv_term_var_7 REPORT.TERMINAL.term_var_7%TYPE,
        pv_term_var_8 REPORT.TERMINAL.term_var_8%TYPE,
		pv_doing_business_as IN REPORT.TERMINAL.DOING_BUSINESS_AS%TYPE DEFAULT NULL)
    IS
        ln_machine_id REPORT.MACHINE.MACHINE_ID%TYPE;
        ln_product_type_id REPORT.PRODUCT_TYPE.PRODUCT_TYPE_ID%TYPE;
        ln_location_type_id REPORT.LOCATION_TYPE.LOCATION_TYPE_ID%TYPE;	
        ln_cnt PLS_INTEGER;
        lv_city REPORT.TERMINAL_ADDR.CITY%TYPE;
        lv_state_cd REPORT.TERMINAL_ADDR.STATE%TYPE;  
        ln_region_id REPORT.REGION.REGION_ID%TYPE;        
    BEGIN
        BEGIN
            SELECT MAX(MACHINE_ID)
              INTO ln_machine_id
              FROM REPORT.MACHINE
             WHERE UPPER(MAKE) = UPPER(pv_machine_make)
               AND UPPER(MODEL) = UPPER(pv_machine_model)
               AND STATUS IN('A','I');
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                REPORT.MACHINE_INS(ln_machine_id, pv_machine_make, pv_machine_model, pn_user_id);
        END;
        BEGIN
            IF pv_location_type_name IS NOT NULL AND LENGTH(TRIM(pv_location_type_name)) > 0 THEN
                SELECT LOCATION_TYPE_ID
                  INTO ln_location_type_id
                  FROM REPORT.LOCATION_TYPE
                 WHERE UPPER(LOCATION_TYPE_NAME) = UPPER(pv_location_type_name)
                   AND STATUS IN('A','I');
            ELSIF pn_location_type_id IS NULL THEN
                ln_location_type_id := 0;
            ELSE
                ln_location_type_id := pn_location_type_id;
            END IF;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                RAISE_APPLICATION_ERROR(-20211, 'Invalid location type name');
        END;
        BEGIN
            SELECT PRODUCT_TYPE_ID
              INTO ln_product_type_id
              FROM REPORT.PRODUCT_TYPE
             WHERE UPPER(PRODUCT_TYPE_NAME) = UPPER(pv_product_type_name)
               AND STATUS IN('A','I');
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                RAISE_APPLICATION_ERROR(-20212, 'Invalid product type name');
        END;

         IF pv_state_cd IS NULL OR pv_city IS NULL THEN
            SELECT MAX(CITY), MAX(STATE_CD), MAX(CNT)
              INTO lv_city, lv_state_cd, ln_cnt
              FROM (SELECT P.CITY, S.STATE_CD, 1 CNT
                  FROM FRONT.POSTAL P 
                  JOIN CORP.STATE S ON P.STATE_ID = S.STATE_ID
                WHERE P.POSTAL_CD = pv_postal
                  AND S.COUNTRY_CD = pv_country_cd
                ORDER BY P.POSTAL_ID ASC)
             WHERE ROWNUM = 1;
        ELSE
            SELECT COUNT(*), pv_city, pv_state_cd
              INTO ln_cnt, lv_city, lv_state_cd
              FROM CORP.STATE 
             WHERE STATE_CD = pv_state_cd
               AND COUNTRY_CD = pv_country_cd; 
        END IF; 
        IF ln_cnt = 0 THEN
            RAISE_APPLICATION_ERROR(-20220, 'Invalid state/country');
            RETURN;
        END IF;
        IF TRIM(pv_region_name) IS NOT NULL THEN
            REPORT.ADD_REGION(ln_region_id, pv_region_name);
        ELSE
            ln_region_id := 0;
        END IF;
        INTERNAL_UPDATE_TERMINAL(
               pn_terminal_id,
               pv_asset_nbr,
               ln_machine_id,
               pv_telephone,
               pv_prefix,
               ln_region_id,
               pv_location_name,
               pv_location_details,
               0,  /* address id (0=create a new address)*/
               pv_address1,
               lv_city,
               lv_state_cd,
               pv_postal,
               pv_country_cd,
               pn_customer_bank_id,
               pn_pay_sched_id,
               pn_user_id,
               pn_user_id,
               NULL,
               ln_location_type_id,
               pn_location_type_specific,
               ln_product_type_id,
               pn_product_type_specific,
               pc_auth_mode,
               pn_time_zone_id,
               pc_dex_data,
               pc_receipt,
               pn_vends,
               pv_term_var_1,
               pv_term_var_2,
               pv_term_var_3,
               pv_term_var_4,
               pv_term_var_5,
               pv_term_var_6,
               pv_term_var_7,
               pv_term_var_8,
			   pv_doing_business_as);
    END;
    -- pre usalive 1.9 signature
    PROCEDURE CREATE_CUSTOMER_BANK(
        pn_customer_bank_id OUT CORP.CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        pn_customer_id IN CORP.CUSTOMER_BANK.CUSTOMER_ID%TYPE,
        pn_user_id IN CORP.CUSTOMER.USER_ID%TYPE,
        pv_bank_name IN CORP.CUSTOMER_BANK.BANK_NAME%TYPE,
        pv_bank_address1 IN CORP.CUSTOMER_BANK.BANK_ADDRESS%TYPE,
        pv_bank_city IN CORP.CUSTOMER_BANK.BANK_CITY%TYPE,
        pv_bank_state_cd IN CORP.CUSTOMER_BANK.BANK_STATE%TYPE,
        pv_bank_postal IN CORP.CUSTOMER_BANK.BANK_ZIP%TYPE,
        pv_bank_country_cd IN CORP.CUSTOMER_BANK.BANK_COUNTRY_CD%TYPE,
        pv_account_title IN CORP.CUSTOMER_BANK.ACCOUNT_TITLE%TYPE,
        pv_account_type IN CORP.CUSTOMER_BANK.ACCOUNT_TYPE%TYPE,
        pv_bank_routing_nbr IN CORP.CUSTOMER_BANK.BANK_ROUTING_NBR%TYPE,
        pv_bank_acct_nbr IN CORP.CUSTOMER_BANK.BANK_ACCT_NBR%TYPE,
        pv_contact_name IN CORP.CUSTOMER_BANK.CONTACT_NAME%TYPE,
        pv_contact_title IN CORP.CUSTOMER_BANK.CONTACT_TITLE%TYPE,
        pv_contact_telephone IN CORP.CUSTOMER_BANK.CONTACT_TELEPHONE%TYPE,
        pv_contact_fax IN CORP.CUSTOMER_BANK.CONTACT_FAX%TYPE)
    IS
    BEGIN
        CREATE_CUSTOMER_BANK(pn_customer_bank_id,pn_customer_id,pn_user_id,pv_bank_name,pv_bank_address1,pv_bank_city,
        pv_bank_state_cd,pv_bank_postal,pv_bank_country_cd,pv_account_title,pv_account_type,pv_bank_routing_nbr,
        pv_bank_acct_nbr,pv_contact_name,pv_contact_title,pv_contact_telephone,pv_contact_fax,null);
    END;
    
    -- usalive 1.9 signature
    PROCEDURE CREATE_CUSTOMER_BANK(
        pn_customer_bank_id OUT CORP.CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        pn_customer_id IN CORP.CUSTOMER_BANK.CUSTOMER_ID%TYPE,
        pn_user_id IN CORP.CUSTOMER.USER_ID%TYPE,
        pv_bank_name IN CORP.CUSTOMER_BANK.BANK_NAME%TYPE,
        pv_bank_address1 IN CORP.CUSTOMER_BANK.BANK_ADDRESS%TYPE,
        pv_bank_city IN CORP.CUSTOMER_BANK.BANK_CITY%TYPE,
        pv_bank_state_cd IN CORP.CUSTOMER_BANK.BANK_STATE%TYPE,
        pv_bank_postal IN CORP.CUSTOMER_BANK.BANK_ZIP%TYPE,
        pv_bank_country_cd IN CORP.CUSTOMER_BANK.BANK_COUNTRY_CD%TYPE,
        pv_account_title IN CORP.CUSTOMER_BANK.ACCOUNT_TITLE%TYPE,
        pv_account_type IN CORP.CUSTOMER_BANK.ACCOUNT_TYPE%TYPE,
        pv_bank_routing_nbr IN CORP.CUSTOMER_BANK.BANK_ROUTING_NBR%TYPE,
        pv_bank_acct_nbr IN CORP.CUSTOMER_BANK.BANK_ACCT_NBR%TYPE,
        pv_contact_name IN CORP.CUSTOMER_BANK.CONTACT_NAME%TYPE,
        pv_contact_title IN CORP.CUSTOMER_BANK.CONTACT_TITLE%TYPE,
        pv_contact_telephone IN CORP.CUSTOMER_BANK.CONTACT_TELEPHONE%TYPE,
        pv_contact_fax IN CORP.CUSTOMER_BANK.CONTACT_FAX%TYPE,
        pv_swift_code IN CORP.CUSTOMER_BANK.SWIFT_CODE%TYPE)
    IS
    BEGIN
        SELECT CORP.CUSTOMER_BANK_SEQ.NEXTVAL INTO pn_customer_bank_id FROM DUAL;
        INSERT INTO CORP.CUSTOMER_BANK(CUSTOMER_BANK_ID, CUSTOMER_ID, BANK_NAME, BANK_ADDRESS, BANK_CITY, BANK_STATE, BANK_ZIP, BANK_COUNTRY_CD, CREATE_BY,
            ACCOUNT_TITLE, ACCOUNT_TYPE, BANK_ROUTING_NBR,	BANK_ACCT_NBR, CONTACT_NAME, CONTACT_TITLE, CONTACT_TELEPHONE, CONTACT_FAX, SWIFT_CODE)
            VALUES(pn_customer_bank_id, pn_customer_id, pv_bank_name, pv_bank_address1, pv_bank_city, pv_bank_state_cd, pv_bank_postal, pv_bank_country_cd, pn_user_id,
                   pv_account_title, pv_account_type, pv_bank_routing_nbr, pv_bank_acct_nbr, pv_contact_name, pv_contact_title, pv_contact_telephone, pv_contact_fax, pv_swift_code);
        INSERT INTO CORP.USER_CUSTOMER_BANK (CUSTOMER_BANK_ID, USER_ID, ALLOW_EDIT)
            VALUES(pn_customer_bank_id, pn_user_id, 'Y');
    END;
    
    PROCEDURE UPDATE_REGION_NAME(
        pn_region_id REPORT.REGION.REGION_ID%TYPE,
        pv_region_name REPORT.REGION.REGION_NAME%TYPE,
        pn_customer_id REPORT.TERMINAL.CUSTOMER_ID%TYPE)
    IS
        ln_new_region_id REPORT.REGION.REGION_ID%TYPE;
    BEGIN
        IF pv_region_name IS NULL THEN
            DELETE FROM REPORT.TERMINAL_REGION
             WHERE REGION_ID = pn_region_id
               AND TERMINAL_ID IN(SELECT TERMINAL_ID FROM REPORT.TERMINAL WHERE CUSTOMER_ID = pn_customer_id);
        ELSE
            REPORT.ADD_REGION(ln_new_region_id, pv_region_name);       
            IF pn_region_id = 0 THEN
                INSERT INTO REPORT.TERMINAL_REGION(TERMINAL_ID, REGION_ID)
                  SELECT T.TERMINAL_ID, ln_new_region_id
                    FROM REPORT.TERMINAL T
                   WHERE T.CUSTOMER_ID = pn_customer_id
                     AND NOT EXISTS(SELECT 1 FROM REPORT.TERMINAL_REGION TR WHERE T.TERMINAL_ID = TR.TERMINAL_ID);
            ELSE
                UPDATE REPORT.TERMINAL_REGION
                   SET REGION_ID = ln_new_region_id
                 WHERE REGION_ID = pn_region_id
                   AND TERMINAL_ID IN(SELECT TERMINAL_ID FROM REPORT.TERMINAL WHERE CUSTOMER_ID = pn_customer_id);
            END IF;
        END IF;
    END;    
    
    PROCEDURE CREATE_CAMPAIGN(
        pv_campaign_name REPORT.CAMPAIGN.CAMPAIGN_NAME%TYPE,
        pv_campaign_desc REPORT.CAMPAIGN.CAMPAIGN_DESC%TYPE,
        pn_campaign_type_id REPORT.CAMPAIGN.CAMPAIGN_TYPE_ID%TYPE,
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pd_start_date REPORT.CAMPAIGN.START_DATE%TYPE,
        pd_end_date REPORT.CAMPAIGN.END_DATE%TYPE,
        pn_discount_percent REPORT.CAMPAIGN.DISCOUNT_PERCENT%TYPE,
        pn_recur_schedule REPORT.CAMPAIGN.RECUR_SCHEDULE%TYPE,
        pn_threshold_amount REPORT.CAMPAIGN.THRESHOLD_AMOUNT%TYPE,
        pn_campaign_id OUT REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE
        )
     IS
      pn_customer_id REPORT.CAMPAIGN.CUSTOMER_ID%TYPE;
    BEGIN
        select report.seq_campaign_id.nextval into pn_campaign_id from dual;
        select decode(customer_id, 0,null,customer_id) into pn_customer_id from report.user_login where user_id=pn_user_id;
        insert into report.campaign 
        (campaign_id, CAMPAIGN_NAME, CAMPAIGN_DESC, CAMPAIGN_TYPE_ID, customer_id, START_DATE, END_DATE, DISCOUNT_PERCENT, RECUR_SCHEDULE, THRESHOLD_AMOUNT) 
        values(pn_campaign_id, pv_campaign_name, pv_campaign_desc, pn_campaign_type_id, pn_customer_id, pd_start_date, pd_end_date, pn_discount_percent/100, pn_recur_schedule,pn_threshold_amount);
    END;
    
    PROCEDURE EDIT_CAMPAIGN(
        pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
        pv_campaign_name REPORT.CAMPAIGN.CAMPAIGN_NAME%TYPE,
        pv_campaign_desc REPORT.CAMPAIGN.CAMPAIGN_DESC%TYPE,
        pn_campaign_type_id REPORT.CAMPAIGN.CAMPAIGN_TYPE_ID%TYPE,
        pd_start_date REPORT.CAMPAIGN.START_DATE%TYPE,
        pd_end_date REPORT.CAMPAIGN.END_DATE%TYPE,
        pn_discount_percent REPORT.CAMPAIGN.DISCOUNT_PERCENT%TYPE,
        pn_recur_schedule REPORT.CAMPAIGN.RECUR_SCHEDULE%TYPE,
        pn_threshold_amount REPORT.CAMPAIGN.THRESHOLD_AMOUNT%TYPE
        )
     IS
    BEGIN
        update report.campaign set campaign_name=pv_campaign_name,
        campaign_desc=pv_campaign_desc,
        campaign_type_id=pn_campaign_type_id,
        start_date=pd_start_date,
        end_date=pd_end_date,
        discount_percent=pn_discount_percent/100,
        recur_schedule=pn_recur_schedule,
        threshold_amount=pn_threshold_amount
        where campaign_id=pn_campaign_id;      
        IF pn_campaign_type_id != 1 THEN
          DELETE_CAMPAIGN_ASSIGNMENT(pn_campaign_id);
        END IF;
    END;
    
    PROCEDURE DELETE_CAMPAIGN(
        pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE
        )
     IS
    BEGIN
        DELETE_CAMPAIGN_ASSIGNMENT(pn_campaign_id);
        delete from pss.campaign_consumer_acct where campaign_id=pn_campaign_id;
        update report.campaign set status='D' where campaign_id=pn_campaign_id; 
    END;
    
    PROCEDURE ADD_CAMP_POS_PTA_BY_POS(
    pn_pos_pta_id PSS.CAMPAIGN_POS_PTA.POS_PTA_ID%TYPE,
		pn_pos_id PSS.POS.POS_ID%TYPE,
    pc_delete_flag CHAR)
    IS
      l_lock VARCHAR2(128);
    BEGIN 
      l_lock := GLOBALS_PKG.REQUEST_LOCK('PSS.CAMPAIGN_POS_PTA',pn_pos_pta_id);
      
      IF pc_delete_flag = 'Y' THEN
        DELETE FROM PSS.CAMPAIGN_POS_PTA WHERE pos_pta_id = pn_pos_pta_id;
      END IF;
      
      INSERT INTO PSS.CAMPAIGN_POS_PTA(CAMPAIGN_POS_PTA_ID, CAMPAIGN_ID, POS_PTA_ID, START_DATE, END_DATE, PRIORITY, TERMINAL_EPORT_ID)
      select PSS.CAMPAIGN_POS_PTA_SEQ.nextval, ca.campaign_id, pn_pos_pta_id, 
      greatest(NVL(c.START_DATE, MIN_DATE), NVL(te.START_DATE, MIN_DATE)) as start_date, 
      least(NVL(c.END_DATE, MAX_DATE),NVL(te.END_DATE, MAX_DATE)) as end_date,
        CASE WHEN ca.TERMINAL_ID is not null THEN 10 WHEN ca.region_id is not null THEN 20 ELSE 30 END as priority, te.terminal_eport_id
        from report.terminal t join report.terminal_eport te on t.terminal_id=te.terminal_id 
        left outer join report.terminal_region tr on tr.terminal_id =t.terminal_id
        join report.campaign_assignment ca on (t.terminal_id=ca.terminal_id 
        or (ca.customer_id=t.customer_id and ca.region_id=tr.region_id ) 
        or (ca.customer_id=t.customer_id and ca.region_id is null ))     
        join report.campaign c on ca.campaign_id=c.campaign_id 
        join report.eport e ON te.eport_id = e.eport_id
        join device.device d on d.device_serial_cd=e.eport_serial_num and device_active_yn_flag='Y'
        join pss.pos p on p.device_id=d.device_id and p.pos_id=pn_pos_id
        WHERE greatest(NVL(c.START_DATE, MIN_DATE), NVL(te.START_DATE, MIN_DATE))<least(NVL(c.END_DATE, MAX_DATE),NVL(te.END_DATE, MAX_DATE));
    END;
    
    PROCEDURE ADD_CAM_POS_PTA_BY_REG(
		pn_terminal_id REPORT.TERMINAL.TERMINAL_ID%TYPE,
    pn_region_id REPORT.TERMINAL_REGION.REGION_ID%TYPE)
    IS
      l_lock VARCHAR2(128);
      l_pos_pta_ids NUMBER_TABLE;
      l_count NUMBER;
    BEGIN 
      l_pos_pta_ids:=NUMBER_TABLE();
      FOR l_cur in (select pt.pos_pta_id, greatest(NVL(c.START_DATE, MIN_DATE), NVL(te.START_DATE, MIN_DATE)) as start_date, 
        least(NVL(c.END_DATE, MAX_DATE),NVL(te.END_DATE, MAX_DATE)) as end_date, ca.campaign_id, te.terminal_eport_id
        from report.terminal t join report.terminal_eport te on t.terminal_id=te.terminal_id and t.terminal_id=pn_terminal_id
        left outer join report.campaign_assignment ca on ((ca.customer_id=t.customer_id and ca.region_id=pn_region_id ))     
        left outer join report.campaign c on ca.campaign_id=c.campaign_id 
        join report.eport e ON te.eport_id = e.eport_id
        join device.device d on d.device_serial_cd=e.eport_serial_num and device_active_yn_flag='Y'
        join pss.pos p on p.device_id=d.device_id
        join pss.pos_pta pt on p.pos_id = pt.pos_id
        join pss.payment_subtype sb on pt.payment_subtype_id=sb.payment_subtype_id and sb.PAYMENT_SUBTYPE_CLASS='Internal::Prepaid'
        )
      LOOP
          l_count:=0;
          select count(1) into l_count from table(l_pos_pta_ids) where column_value=l_cur.pos_pta_id;
          IF l_count = 0 THEN
            l_lock := GLOBALS_PKG.REQUEST_LOCK('PSS.CAMPAIGN_POS_PTA',l_cur.pos_pta_id);
            DELETE FROM PSS.CAMPAIGN_POS_PTA WHERE pos_pta_id = l_cur.pos_pta_id and priority=20 and terminal_eport_id=l_cur.terminal_eport_id;
          END IF;
          l_pos_pta_ids.extend;
          l_pos_pta_ids(l_pos_pta_ids.last):=l_cur.pos_pta_id;

        IF l_cur.campaign_id is not null and l_cur.start_date <l_cur.end_date THEN
          INSERT INTO PSS.CAMPAIGN_POS_PTA(CAMPAIGN_POS_PTA_ID, CAMPAIGN_ID, POS_PTA_ID, START_DATE, END_DATE, PRIORITY, TERMINAL_EPORT_ID)
          VALUES(PSS.CAMPAIGN_POS_PTA_SEQ.nextval, l_cur.campaign_id, l_cur.pos_pta_id, l_cur.start_date, l_cur.end_date, 20, l_cur.terminal_eport_id );
         END IF;
       END LOOP;
      EXCEPTION
            WHEN NO_DATA_FOUND THEN
              RETURN;
    END;
    
    PROCEDURE ADD_CAM_POS_PTA_BY_TER_EPORT(
    pn_terminal_id REPORT.TERMINAL_EPORT.TERMINAL_ID%TYPE,
		pn_eport_id REPORT.TERMINAL_EPORT.EPORT_ID%TYPE,
		pd_start_date REPORT.TERMINAL_EPORT.START_DATE%TYPE,
    pd_end_date REPORT.TERMINAL_EPORT.END_DATE%TYPE,
    pn_terminal_eport_id REPORT.TERMINAL_EPORT.TERMINAL_EPORT_ID%TYPE,
    pc_delete_only_flag CHAR DEFAULT 'N')
    IS
      l_lock VARCHAR2(128);
      l_pos_pta_ids NUMBER_TABLE;
      l_count NUMBER;
    BEGIN   
      IF pc_delete_only_flag = 'Y' THEN
        DELETE FROM PSS.CAMPAIGN_POS_PTA WHERE terminal_eport_id=pn_terminal_eport_id;
      ELSE
        l_pos_pta_ids:=NUMBER_TABLE();
        FOR l_cur in (select pt.pos_pta_id, greatest(NVL(c.START_DATE, MIN_DATE), NVL(pd_start_date, MIN_DATE)) as start_date, least(NVL(c.END_DATE, MAX_DATE),NVL(pd_end_date, MAX_DATE)) as end_date,
          CASE WHEN ca.TERMINAL_ID is not null THEN 10 WHEN ca.region_id is not null THEN 20 ELSE 30 END as priority, ca.campaign_id
          from report.terminal t  
          left outer join report.terminal_region tr on tr.terminal_id =t.terminal_id
          left outer join report.campaign_assignment ca on (t.terminal_id=ca.terminal_id 
          or (ca.customer_id=t.customer_id and ca.region_id=tr.region_id ) 
          or (ca.customer_id=t.customer_id and ca.region_id is null ))     
          left outer join report.campaign c on ca.campaign_id=c.campaign_id 
          join report.eport e ON e.eport_id=pn_eport_id
          join device.device d on d.device_serial_cd=e.eport_serial_num and device_active_yn_flag='Y'
          join pss.pos p on p.device_id=d.device_id
          join pss.pos_pta pt on p.pos_id = pt.pos_id
          join pss.payment_subtype sb on pt.payment_subtype_id=sb.payment_subtype_id and sb.PAYMENT_SUBTYPE_CLASS='Internal::Prepaid'
          WHERE t.terminal_id=pn_terminal_id)
        LOOP     
            l_count:=0;
            select count(1) into l_count from table(l_pos_pta_ids) where column_value=l_cur.pos_pta_id;
            IF l_count = 0 THEN
               l_lock := GLOBALS_PKG.REQUEST_LOCK('PSS.CAMPAIGN_POS_PTA',l_cur.pos_pta_id);
               DELETE FROM PSS.CAMPAIGN_POS_PTA WHERE pos_pta_id = l_cur.pos_pta_id and terminal_eport_id=pn_terminal_eport_id;
            END IF;
            l_pos_pta_ids.extend;
            l_pos_pta_ids(l_pos_pta_ids.last):=l_cur.pos_pta_id;
             
          IF l_cur.campaign_id is not null and l_cur.start_date < l_cur.end_date THEN
            INSERT INTO PSS.CAMPAIGN_POS_PTA(CAMPAIGN_POS_PTA_ID, CAMPAIGN_ID, POS_PTA_ID, START_DATE, END_DATE, PRIORITY, TERMINAL_EPORT_ID)
            VALUES(PSS.CAMPAIGN_POS_PTA_SEQ.nextval, l_cur.campaign_id, l_cur.pos_pta_id, l_cur.start_date, l_cur.end_date, l_cur.priority, pn_terminal_eport_id);
          END IF;
        END LOOP;
      END IF;
      EXCEPTION
            WHEN NO_DATA_FOUND THEN
              RETURN;
    END;
    
    PROCEDURE DELETE_CAMPAIGN_ASSIGNMENT(
		pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE)
    IS
    BEGIN
      delete from report.CAMPAIGN_ASSIGNMENT where campaign_id = pn_campaign_id;
      delete from PSS.campaign_pos_pta where campaign_id=pn_campaign_id;
    END;
    
    PROCEDURE EDIT_CAMPAIGN_CUSTOMER(
		pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
    pn_customer_id REPORT.CAMPAIGN_ASSIGNMENT.CUSTOMER_ID%TYPE,
    pv_add CHAR)
    IS
    BEGIN    
      delete from report.campaign_assignment 
      where campaign_id=pn_campaign_id and customer_id = pn_customer_id and region_id is null;
      IF pv_add = 'Y' THEN 
        insert into report.campaign_assignment(campaign_assignment_id, campaign_id, customer_id)
        values(REPORT.CAMPAIGN_ASSIGNMENT_SEQ.nextval,pn_campaign_id,pn_customer_id);
      END IF;
    END;
    
    PROCEDURE EDIT_CAMPAIGN_REGION(
    pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
		pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
    pn_customer_id REPORT.CAMPAIGN_ASSIGNMENT.CUSTOMER_ID%TYPE,
    pn_region_id NUMBER_TABLE,
    pv_add CHAR)
    IS
    BEGIN    
      delete from report.campaign_assignment 
      where campaign_id=pn_campaign_id and customer_id = pn_customer_id and region_id member of pn_region_id;
      
      IF pv_add = 'Y' THEN
        insert into report.campaign_assignment(campaign_assignment_id, campaign_id, customer_id, region_id)
        select REPORT.CAMPAIGN_ASSIGNMENT_SEQ.nextval, pn_campaign_id, pn_customer_id, column_value customer_id from table(pn_region_id);
      END IF;
    END;
    
    PROCEDURE EDIT_CAMPAIGN_TERMINAL(
    pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
		pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
    pn_terminal_id NUMBER_TABLE,
    pv_add CHAR)
    IS
    BEGIN
      delete from report.campaign_assignment where campaign_id=pn_campaign_id and terminal_id member of pn_terminal_id;
      IF pv_add = 'Y' THEN
        insert into report.campaign_assignment(campaign_assignment_id, campaign_id, terminal_id)
        select REPORT.CAMPAIGN_ASSIGNMENT_SEQ.nextval, pn_campaign_id, column_value terminal_id from table(pn_terminal_id);
      END IF;
    END;
    
    PROCEDURE ADD_CAM_POS_PTA_BY_CAMPAIGN(
		pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE)
    IS
    BEGIN
      DELETE from PSS.CAMPAIGN_POS_PTA where CAMPAIGN_ID=pn_campaign_id;
      FOR l_cur in (
      select pt.pos_pta_id, greatest(NVL(c.START_DATE, MIN_DATE), NVL(te.START_DATE, MIN_DATE)) as start_date, least(NVL(c.END_DATE, MAX_DATE),NVL(te.END_DATE, MAX_DATE)) as end_date,
        CASE WHEN ca.TERMINAL_ID is not null THEN 10 WHEN ca.region_id is not null THEN 20 ELSE 30 END as priority, te.terminal_eport_id
        from report.terminal t join report.terminal_eport te on t.terminal_id=te.terminal_id
        left outer join report.terminal_region tr on tr.terminal_id =t.terminal_id
        join report.campaign_assignment ca on (t.terminal_id=ca.terminal_id 
        or (ca.customer_id=t.customer_id and ca.region_id=tr.region_id ) 
        or (ca.customer_id=t.customer_id and ca.region_id is null ))     
        join report.campaign c on ca.campaign_id=c.campaign_id and ca.campaign_id=pn_campaign_id
        join report.eport e ON te.eport_id = e.eport_id
        join device.device d on d.device_serial_cd=e.eport_serial_num and device_active_yn_flag='Y'
        join pss.pos p on p.device_id=d.device_id
        join pss.pos_pta pt on p.pos_id = pt.pos_id
        join pss.payment_subtype sb on pt.payment_subtype_id=sb.payment_subtype_id and sb.PAYMENT_SUBTYPE_CLASS='Internal::Prepaid'
        WHERE greatest(NVL(c.START_DATE, MIN_DATE), NVL(te.START_DATE, MIN_DATE))<least(NVL(c.END_DATE, MAX_DATE),NVL(te.END_DATE, MAX_DATE)))
      LOOP
        INSERT INTO PSS.CAMPAIGN_POS_PTA(CAMPAIGN_POS_PTA_ID, CAMPAIGN_ID, POS_PTA_ID, START_DATE, END_DATE, PRIORITY, TERMINAL_EPORT_ID)
        VALUES(PSS.CAMPAIGN_POS_PTA_SEQ.nextval, pn_campaign_id, l_cur.pos_pta_id, l_cur.start_date, l_cur.end_date, l_cur.priority, l_cur.terminal_eport_id);
      END LOOP;
    END;
    
    PROCEDURE ADD_CARDS_TO_CAMPAIGN(
		pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
    pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
    pn_card_id NUMBER_TABLE)
    IS
    BEGIN 
      insert into pss.campaign_consumer_acct(CONSUMER_ACCT_ID,CAMPAIGN_ID)
      SELECT CONSUMER_ACCT_ID,pn_campaign_id
      FROM (SELECT CA.CONSUMER_ACCT_ID
        FROM PSS.CONSUMER_ACCT CA 
        WHERE CA.CORP_CUSTOMER_ID in (SELECT DISTINCT T.CUSTOMER_ID
		 FROM REPORT.VW_USER_TERMINAL UT
		 INNER JOIN REPORT.VW_TERMINAL T ON UT.TERMINAL_ID = T.TERMINAL_ID     	
		 WHERE UT.USER_ID = pn_user_id) and CONSUMER_ACCT_ID member of pn_card_id
        minus
        SELECT CONSUMER_ACCT_ID from pss.campaign_consumer_acct where campaign_id=pn_campaign_id);
        
    END;
    
    PROCEDURE REMOVE_CARDS_FROM_CAMPAIGN(
    pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
    pn_card_id NUMBER_TABLE)
    IS
    BEGIN 
      delete from pss.campaign_consumer_acct where campaign_id=pn_campaign_id
      and CONSUMER_ACCT_ID member of pn_card_id;
    END;
    
    --begin pre R36, can be removed afterwards   
    FUNCTION                 FIND_CAMPAIGN (
        l_terminal_id REPORT.TERMINAL_EPORT.TERMINAL_ID%TYPE,
        l_region_id REPORT.TERMINAL_REGION.REGION_ID%TYPE,
        l_customer_id REPORT.TERMINAL.CUSTOMER_ID%TYPE)
    RETURN REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE IS
        l_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE;
        l_count NUMBER;
    BEGIN
        select max(campaign_id) into l_campaign_id from report.campaign_assignment where terminal_id=l_terminal_id;
        IF l_campaign_id is NULL THEN
          select max(campaign_id) into l_campaign_id from report.campaign_assignment where customer_id=l_customer_id and region_id=l_region_id;
          IF l_campaign_id is NULL THEN
            select max(campaign_id) into l_campaign_id from report.campaign_assignment where customer_id=l_customer_id and region_id is null;
          END IF;
        END IF;	
      RETURN l_campaign_id;  
    END;
    
    FUNCTION                 FIND_CAMPAIGN (
        l_terminal_id REPORT.TERMINAL_EPORT.TERMINAL_ID%TYPE)
    RETURN REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE IS
        l_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE;
        l_region_id REPORT.TERMINAL_REGION.REGION_ID%TYPE;
        l_customer_id REPORT.CAMPAIGN.CUSTOMER_ID%TYPE;
        l_count NUMBER;
    BEGIN
        select t.customer_id, tr.region_id into l_customer_id, l_region_id 
        from report.terminal t left outer join report.terminal_region tr on t.terminal_id=tr.terminal_id
        where t.terminal_id=l_terminal_id;
        l_campaign_id:=FIND_CAMPAIGN(l_terminal_id, l_region_id, l_customer_id);
      RETURN l_campaign_id;  
    END;
    
    PROCEDURE ADD_CAM_POS_PTA_BY_TER(
		pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
		pn_terminal_id REPORT.TERMINAL.TERMINAL_ID%TYPE)
    IS
      l_start_date DATE;
      l_end_date DATE;
      l_lock VARCHAR2(128);
    BEGIN 
      FOR l_cur in (select pt.pos_pta_id as l_pos_pta_id, NVL(te.START_DATE, MIN_DATE) as l_start_date, NVL(te.END_DATE, MAX_DATE) as l_end_date
        from report.terminal t join report.terminal_eport te on t.terminal_id=te.terminal_id
        join report.eport e ON te.eport_id = e.eport_id
        join device.device d on d.device_serial_cd=e.eport_serial_num and device_active_yn_flag='Y'
        join pss.pos p on p.device_id=d.device_id
        join pss.pos_pta pt on p.pos_id = pt.pos_id
        join pss.payment_subtype sb on pt.payment_subtype_id=sb.payment_subtype_id and sb.PAYMENT_SUBTYPE_CLASS='Internal::Prepaid'
        WHERE SYSDATE >= NVL(te.START_DATE, MIN_DATE)
        AND SYSDATE    < NVL(te.END_DATE, MAX_DATE) and t.terminal_id=pn_terminal_id)
      LOOP
        l_lock := GLOBALS_PKG.REQUEST_LOCK('PSS.CAMPAIGN_POS_PTA',l_cur.l_pos_pta_id);
        DELETE FROM PSS.CAMPAIGN_POS_PTA WHERE pos_pta_id = l_cur.l_pos_pta_id;
        
        IF pn_campaign_id is not null THEN
          select greatest(NVL(START_DATE, MIN_DATE), l_cur.l_start_date), least(NVL(END_DATE, MAX_DATE), l_cur.l_end_date) into l_start_date, l_end_date 
          from report.campaign where campaign_id=pn_campaign_id;
          INSERT INTO PSS.CAMPAIGN_POS_PTA(CAMPAIGN_POS_PTA_ID, CAMPAIGN_ID, POS_PTA_ID, START_DATE, END_DATE)
          VALUES(PSS.CAMPAIGN_POS_PTA_SEQ.nextval, pn_campaign_id, l_cur.l_pos_pta_id, l_start_date, l_end_date );
         END IF;
       END LOOP;
      EXCEPTION
            WHEN NO_DATA_FOUND THEN
              RETURN;
    END;
    
    PROCEDURE ADD_CAM_POS_PTA_BY_TER_EPORT(
    pn_terminal_id REPORT.TERMINAL_EPORT.TERMINAL_ID%TYPE,
		pn_eport_id REPORT.TERMINAL_EPORT.EPORT_ID%TYPE,
		pd_start_date REPORT.TERMINAL_EPORT.START_DATE%TYPE,
    pd_end_date REPORT.TERMINAL_EPORT.END_DATE%TYPE)
    IS
      l_campaign_id NUMBER;
      l_start_date DATE;
      l_end_date DATE;
      l_lock VARCHAR2(128);
    BEGIN
      l_campaign_id:=find_campaign(pn_terminal_id);
      
      FOR l_cur in (select pt.pos_pta_id as l_pos_pta_id, NVL(pd_start_date, MIN_DATE) as l_start_date, NVL(pd_end_date, MAX_DATE) as l_end_date
        from device.device d 
        join report.eport e on d.device_serial_cd=e.eport_serial_num and device_active_yn_flag='Y'
        join pss.pos p on p.device_id=d.device_id
        join pss.pos_pta pt on p.pos_id = pt.pos_id
        join pss.payment_subtype sb on pt.payment_subtype_id=sb.payment_subtype_id and sb.PAYMENT_SUBTYPE_CLASS='Internal::Prepaid'
        WHERE SYSDATE >= NVL(pd_start_date, MIN_DATE)
        AND SYSDATE    < NVL(pd_end_date, MAX_DATE) and e.eport_id=pn_eport_id)
      LOOP
        
        l_lock := GLOBALS_PKG.REQUEST_LOCK('PSS.CAMPAIGN_POS_PTA',l_cur.l_pos_pta_id);
        DELETE FROM PSS.CAMPAIGN_POS_PTA WHERE pos_pta_id = l_cur.l_pos_pta_id;
        
        IF l_campaign_id is not null THEN
          select greatest(NVL(START_DATE, MIN_DATE), l_cur.l_start_date), least(NVL(END_DATE, MAX_DATE), l_cur.l_end_date) into l_start_date, l_end_date 
          from report.campaign where campaign_id=l_campaign_id;
          INSERT INTO PSS.CAMPAIGN_POS_PTA(CAMPAIGN_POS_PTA_ID, CAMPAIGN_ID, POS_PTA_ID, START_DATE, END_DATE)
          VALUES(PSS.CAMPAIGN_POS_PTA_SEQ.nextval, l_campaign_id, l_cur.l_pos_pta_id, l_start_date, l_end_date );
        END IF;
      END LOOP;
      EXCEPTION
            WHEN NO_DATA_FOUND THEN
              RETURN;
    END;
    
    PROCEDURE ADD_CAMPAIGN_CUSTOMER(
		pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
    pn_customer_id NUMBER_TABLE)
    IS
    BEGIN    
      delete from report.campaign_assignment 
      where customer_id member of pn_customer_id and region_id is null;
      
      insert into report.campaign_assignment(campaign_assignment_id, campaign_id, customer_id)
      select REPORT.CAMPAIGN_ASSIGNMENT_SEQ.nextval, pn_campaign_id, column_value customer_id from table(pn_customer_id);
    END;
    
    PROCEDURE ADD_CAMPAIGN_TERMINAL(
    pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
		pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
    pn_terminal_id NUMBER_TABLE,
    pn_assgined_terminal_id OUT NUMBER_TABLE)
    IS
    BEGIN
      select terminal_id bulk collect into pn_assgined_terminal_id from (
        select column_value terminal_id from table(pn_terminal_id) p join REPORT.VW_USER_TERMINAL UT on p.column_value=ut.terminal_id and ut.user_id = pn_user_id
        minus
        select t.terminal_id from report.terminal t join report.campaign_assignment ca on ca.campaign_id=pn_campaign_id and t.customer_id=ca.customer_id and ca.region_id is null
        minus
        select t.terminal_id from report.terminal t join report.terminal_region tr on t.terminal_id=tr.terminal_id 
        join report.campaign_assignment ca on ca.campaign_id=pn_campaign_id and t.customer_id=ca.customer_id and tr.region_id=ca.region_id
      );
      
      delete from report.campaign_assignment where terminal_id member of pn_assgined_terminal_id;

      insert into report.campaign_assignment(campaign_assignment_id, campaign_id, terminal_id)
      select REPORT.CAMPAIGN_ASSIGNMENT_SEQ.nextval, pn_campaign_id, column_value terminal_id from table(pn_assgined_terminal_id);
    END;
    
    PROCEDURE CREATE_CAMPAIGN(
        pv_campaign_name REPORT.CAMPAIGN.CAMPAIGN_NAME%TYPE,
        pv_campaign_desc REPORT.CAMPAIGN.CAMPAIGN_DESC%TYPE,
        pn_campaign_type_id REPORT.CAMPAIGN.CAMPAIGN_TYPE_ID%TYPE,
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pd_start_date REPORT.CAMPAIGN.START_DATE%TYPE,
        pd_end_date REPORT.CAMPAIGN.END_DATE%TYPE,
        pn_discount_percent REPORT.CAMPAIGN.DISCOUNT_PERCENT%TYPE,
        pn_campaign_id OUT REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE
        )
    IS
    BEGIN
       CREATE_CAMPAIGN(pv_campaign_name,pv_campaign_desc,pn_campaign_type_id,pn_user_id,pd_start_date,pd_end_date,pn_discount_percent, null, null, pn_campaign_id);
    END;
        
   PROCEDURE EDIT_CAMPAIGN(
        pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
        pv_campaign_name REPORT.CAMPAIGN.CAMPAIGN_NAME%TYPE,
        pv_campaign_desc REPORT.CAMPAIGN.CAMPAIGN_DESC%TYPE,
        pn_campaign_type_id REPORT.CAMPAIGN.CAMPAIGN_TYPE_ID%TYPE,
        pd_start_date REPORT.CAMPAIGN.START_DATE%TYPE,
        pd_end_date REPORT.CAMPAIGN.END_DATE%TYPE,
        pn_discount_percent REPORT.CAMPAIGN.DISCOUNT_PERCENT%TYPE
        )
     IS
    BEGIN
      EDIT_CAMPAIGN(pn_campaign_id, pv_campaign_name,pv_campaign_desc,pn_campaign_type_id,pd_start_date,pd_end_date,pn_discount_percent, null,null);
    END;
     -- end of pre R36, can be removed afterwards
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/releases/REL_Edge_Update_R40/R40.USAPDB.2.sql?rev=HEAD
UPDATE REPORT.TERMINAL_ADDR TA
   SET (ADDRESS1, CITY, STATE, ZIP, COUNTRY_CD) =
       (SELECT CA.ADDRESS1, CA.CITY, CA.STATE, CA.ZIP, CA.COUNTRY_CD
          FROM CORP.CUSTOMER_ADDR CA
          JOIN REPORT.TERMINAL T ON CA.CUSTOMER_ID = T.CUSTOMER_ID
          JOIN REPORT.LOCATION TL ON T.LOCATION_ID = TL.LOCATION_ID
         WHERE TL.ADDRESS_ID = TA.ADDRESS_ID
           AND CA.STATUS = 'A')
 WHERE NULLIF(TA.ADDRESS1, '100 Deerfield Lane') IS NULL
   AND NULLIF(TA.CITY, 'Malvern') IS NULL
   AND NULLIF(TA.STATE, 'PA') IS NULL
   AND NULLIF(TA.ZIP, '19355') IS NULL
   AND NULLIF(TA.COUNTRY_CD, 'US') IS NULL
   AND TA.ADDRESS_ID IN(SELECT TL.ADDRESS_ID
       FROM DEVICE.DEVICE D
       JOIN REPORT.EPORT E ON D.DEVICE_SERIAL_CD = E.EPORT_SERIAL_NUM
       JOIN REPORT.TERMINAL_EPORT TE ON E.EPORT_ID = TE.EPORT_ID AND SYSDATE >= NVL(TE.START_DATE, MIN_DATE) AND SYSDATE < NVL(TE.END_DATE, MAX_DATE)
       JOIN REPORT.TERMINAL T ON TE.TERMINAL_ID = T.TERMINAL_ID
       JOIN REPORT.LOCATION TL ON T.LOCATION_ID = TL.LOCATION_ID
       JOIN CORP.CUSTOMER_ADDR CA ON CA.CUSTOMER_ID = T.CUSTOMER_ID           
      WHERE D.DEVICE_TYPE_ID = 14
        AND D.DEVICE_SUB_TYPE_ID IN(1,2)
        AND CA.COUNTRY_CD IS NOT NULL
        AND CA.ZIP IS NOT NULL
        AND CA.STATE IS NOT NULL
        AND CA.STATUS = 'A');
      
COMMIT;

GRANT EXECUTE ON DBADMIN.RESTRAIN TO REPORT;
GRANT EXECUTE ON DBADMIN.RESTRAIN TO USALIVE_APP_ROLE;
GRANT EXECUTE ON REPORT.CLASSIFY_HEALTH TO USALIVE_APP_ROLE;
GRANT EXECUTE ON CORP.GET_BACKOFFICE_DEVICE TO USALIVE_APP_ROLE;

INSERT INTO FOLIO_CONF.FIELD(FIELD_ID,FIELD_LABEL,FIELD_DESC,DISPLAY_EXPRESSION,SORT_EXPRESSION,DISPLAY_FORMAT,DISPLAY_SQL_TYPE,SORT_SQL_TYPE,IMPORTANCE,FIELD_CATEGORY_ID,ACTIVE_FLAG) 
    VALUES(9292,'Health Classification',null,'REPORT.CLASSIFY_HEALTH(MAX(DBADMIN.RESTRAIN(REPORT.EPORT.LASTDIALIN_DATE, REPORT.TERMINAL_EPORT.START_DATE, REPORT.TERMINAL_EPORT.END_DATE)), MAX(DBADMIN.RESTRAIN(REPORT.EPORT.LAST_CREDIT_TRAN_DATE, REPORT.TERMINAL_EPORT.START_DATE, REPORT.TERMINAL_EPORT.END_DATE)), MAX(DBADMIN.RESTRAIN(REPORT.EPORT.LAST_CASH_TRAN_DATE, REPORT.TERMINAL_EPORT.START_DATE, REPORT.TERMINAL_EPORT.END_DATE)), MAX(DBADMIN.RESTRAIN(REPORT.EPORT.LAST_DEX_DATE, REPORT.TERMINAL_EPORT.START_DATE, REPORT.TERMINAL_EPORT.END_DATE)), CASE WHEN MAX(CORP.SERVICE_FEES[TERMINAL_FEE].SERVICE_FEE_ID) IS NULL THEN ''N'' WHEN MAX(CORP.SERVICE_FEES[TERMINAL_FEE].LAST_PAYMENT) IS NULL THEN ''I'' ELSE ''A'' END, CASE WHEN MAX(CORP.SERVICE_FEES[RENTAL_FEE].SERVICE_FEE_ID) IS NULL THEN ''N'' WHEN MAX(CORP.SERVICE_FEES[RENTAL_FEE].LAST_PAYMENT) IS NULL THEN ''I'' ELSE ''A'' END, :NoCommDaysMin::NUMBER, :NoCommDaysMax::NUMBER, :NoCashlessDaysMin::NUMBER, :NoCashlessDaysMax::NUMBER, :NoCashDaysMin::NUMBER, :NoCashDaysMax::NUMBER, :NoDEXDaysMin::NUMBER, :NoDEXDaysMax::NUMBER)','REPORT.CLASSIFY_HEALTH(MAX(DBADMIN.RESTRAIN(REPORT.EPORT.LASTDIALIN_DATE, REPORT.TERMINAL_EPORT.START_DATE, REPORT.TERMINAL_EPORT.END_DATE)), MAX(DBADMIN.RESTRAIN(REPORT.EPORT.LAST_CREDIT_TRAN_DATE, REPORT.TERMINAL_EPORT.START_DATE, REPORT.TERMINAL_EPORT.END_DATE)), MAX(DBADMIN.RESTRAIN(REPORT.EPORT.LAST_CASH_TRAN_DATE, REPORT.TERMINAL_EPORT.START_DATE, REPORT.TERMINAL_EPORT.END_DATE)), MAX(DBADMIN.RESTRAIN(REPORT.EPORT.LAST_DEX_DATE, REPORT.TERMINAL_EPORT.START_DATE, REPORT.TERMINAL_EPORT.END_DATE)), CASE WHEN MAX(CORP.SERVICE_FEES[TERMINAL_FEE].SERVICE_FEE_ID) IS NULL THEN ''N'' WHEN MAX(CORP.SERVICE_FEES[TERMINAL_FEE].LAST_PAYMENT) IS NULL THEN ''I'' ELSE ''A'' END, CASE WHEN MAX(CORP.SERVICE_FEES[RENTAL_FEE].SERVICE_FEE_ID) IS NULL THEN ''N'' WHEN MAX(CORP.SERVICE_FEES[RENTAL_FEE].LAST_PAYMENT) IS NULL THEN ''I'' ELSE ''A'' END, :NoCommDaysMin::NUMBER, :NoCommDaysMax::NUMBER, :NoCashlessDaysMin::NUMBER, :NoCashlessDaysMax::NUMBER, :NoCashDaysMin::NUMBER, :NoCashDaysMax::NUMBER, :NoDEXDaysMin::NUMBER, :NoDEXDaysMax::NUMBER)',null,'VARCHAR','VARCHAR',50,477,'Y');

INSERT INTO FOLIO_CONF.FIELD(FIELD_ID,FIELD_LABEL,FIELD_DESC,DISPLAY_EXPRESSION,SORT_EXPRESSION,DISPLAY_FORMAT,DISPLAY_SQL_TYPE,SORT_SQL_TYPE,IMPORTANCE,FIELD_CATEGORY_ID,ACTIVE_FLAG) 
    VALUES(9293,'Last Terminal Fee Date',null,'CORP.SERVICE_FEES[TERMINAL_FEE].LAST_PAYMENT', 'CORP.SERVICE_FEES[TERMINAL_FEE].LAST_PAYMENT',null,'TIMESTAMP','TIMESTAMP',50,477,'Y');
INSERT INTO FOLIO_CONF.FIELD(FIELD_ID,FIELD_LABEL,FIELD_DESC,DISPLAY_EXPRESSION,SORT_EXPRESSION,DISPLAY_FORMAT,DISPLAY_SQL_TYPE,SORT_SQL_TYPE,IMPORTANCE,FIELD_CATEGORY_ID,ACTIVE_FLAG) 
    VALUES(9294,'Terminal Fee Amount',null,'CORP.SERVICE_FEES[TERMINAL_FEE].FEE_AMOUNT', 'CORP.SERVICE_FEES[TERMINAL_FEE].FEE_AMOUNT',null,'NUMERIC','NUMERIC',50,477,'Y');
INSERT INTO FOLIO_CONF.FIELD(FIELD_ID,FIELD_LABEL,FIELD_DESC,DISPLAY_EXPRESSION,SORT_EXPRESSION,DISPLAY_FORMAT,DISPLAY_SQL_TYPE,SORT_SQL_TYPE,IMPORTANCE,FIELD_CATEGORY_ID,ACTIVE_FLAG) 
    VALUES(9295,'Last Rental Fee Date',null,'CORP.SERVICE_FEES[RENTAL_FEE].LAST_PAYMENT', 'CORP.SERVICE_FEES[RENTAL_FEE].LAST_PAYMENT',null,'TIMESTAMP','TIMESTAMP',50,477,'Y');
INSERT INTO FOLIO_CONF.FIELD(FIELD_ID,FIELD_LABEL,FIELD_DESC,DISPLAY_EXPRESSION,SORT_EXPRESSION,DISPLAY_FORMAT,DISPLAY_SQL_TYPE,SORT_SQL_TYPE,IMPORTANCE,FIELD_CATEGORY_ID,ACTIVE_FLAG) 
    VALUES(9296,'Rental Fee Amount',null,'CORP.SERVICE_FEES[RENTAL_FEE].FEE_AMOUNT', 'CORP.SERVICE_FEES[RENTAL_FEE].FEE_AMOUNT',null,'NUMERIC','NUMERIC',50,477,'Y');
INSERT INTO FOLIO_CONF.FIELD(FIELD_ID,FIELD_LABEL,FIELD_DESC,DISPLAY_EXPRESSION,SORT_EXPRESSION,DISPLAY_FORMAT,DISPLAY_SQL_TYPE,SORT_SQL_TYPE,IMPORTANCE,FIELD_CATEGORY_ID,ACTIVE_FLAG) 
    VALUES(9374,'Days Ago Upload Date',null,'SYSDATE - REPORT.TRANS.SERVER_DATE','SYSDATE - REPORT.TRANS.SERVER_DATE',null,'NUMERIC','NUMERIC',50,8,'Y');

INSERT INTO FOLIO_CONF.FIELD_PRIV(FIELD_ID, USER_GROUP_ID, FILTER_GROUP_ID)
    SELECT F.FIELD_ID, P.USER_GROUP_ID, P.FILTER_GROUP_ID
      FROM FOLIO_CONF.FIELD F
      CROSS JOIN (SELECT 0 USER_GROUP_ID, 0 FILTER_GROUP_ID FROM DUAL WHERE 1=0
      UNION ALL SELECT 1, NULL FROM DUAL
      UNION ALL SELECT 2, NULL FROM DUAL
      UNION ALL SELECT 3, NULL FROM DUAL
      UNION ALL SELECT 4, 1 FROM DUAL
      UNION ALL SELECT 5, 1 FROM DUAL
      UNION ALL SELECT 8, 1 FROM DUAL) P    
     WHERE (F.FIELD_ID BETWEEN 9292 AND 9296 OR F.FIELD_ID IN(9374))
       AND NOT EXISTS(SELECT 1 FROM FOLIO_CONF.FIELD_PRIV FP WHERE F.FIELD_ID = FP.FIELD_ID AND P.USER_GROUP_ID = FP.USER_GROUP_ID);

DELETE FROM FOLIO_CONF.JOIN_FILTER WHERE JOIN_FILTER_ID IN(2260, 2261, 2262);
INSERT INTO FOLIO_CONF.JOIN_FILTER(JOIN_FILTER_ID, FROM_TABLE, TO_TABLE, JOIN_EXPRESSION, JOIN_TYPE, JOIN_CARDINALITY)
    VALUES(2260, 'CORP.SERVICE_FEES[TERMINAL_FEE]', NULL, 'CORP.SERVICE_FEES[TERMINAL_FEE].FEE_ID = 1 AND NVL(CORP.SERVICE_FEES[TERMINAL_FEE].START_DATE, MIN_DATE) <= SYSDATE AND NVL(CORP.SERVICE_FEES[TERMINAL_FEE].END_DATE, MAX_DATE) > SYSDATE AND CORP.SERVICE_FEES[TERMINAL_FEE].FEE_AMOUNT > 0', 'RIGHT', '=');
INSERT INTO FOLIO_CONF.JOIN_FILTER(JOIN_FILTER_ID, FROM_TABLE, TO_TABLE, JOIN_EXPRESSION, JOIN_TYPE, JOIN_CARDINALITY)
    VALUES(2261, 'CORP.SERVICE_FEES[RENTAL_FEE]', NULL, 'CORP.SERVICE_FEES[RENTAL_FEE].FEE_ID = 6 AND NVL(CORP.SERVICE_FEES[RENTAL_FEE].START_DATE, MIN_DATE) <= SYSDATE AND NVL(CORP.SERVICE_FEES[RENTAL_FEE].END_DATE, MAX_DATE) > SYSDATE AND CORP.SERVICE_FEES[RENTAL_FEE].FEE_AMOUNT > 0', 'RIGHT', '=');
INSERT INTO FOLIO_CONF.JOIN_FILTER(JOIN_FILTER_ID, FROM_TABLE, TO_TABLE, JOIN_EXPRESSION, JOIN_TYPE, JOIN_CARDINALITY)
    VALUES(2262, 'REPORT.TERMINAL', NULL, 'REPORT.TERMINAL.STATUS != ''D''', 'INNER', '=');
        
UPDATE FOLIO_CONF.JOIN_FILTER 
  SET JOIN_TYPE = 'RIGHT'
 WHERE FROM_TABLE = 'CORP.SERVICE_FEES'
   AND TO_TABLE = 'REPORT.TERMINAL'
   AND JOIN_TYPE = 'INNER';

COMMIT;

ALTER TABLE FOLIO_CONF.FOLIO
    MODIFY(FOLIO_TITLE VARCHAR2(4000), FOLIO_SUBTITLE VARCHAR2(4000));

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/FOLIO_CONF/folio_exports/folio_transaction_totals_graph.sql?rev=HEAD
SET DEFINE OFF;
ALTER SESSION SET CURRENT_SCHEMA = FOLIO_CONF;
DECLARE
  l_ids NUMBER_TABLE := NUMBER_TABLE(0, 0);
BEGIN
FOLIO_CONF.FOLIO_PKG.DELETE_FOLIO(667);
SELECT 667 INTO l_ids(1) FROM DUAL;
INSERT INTO FOLIO(FOLIO_ID, FOLIO_NAME, FOLIO_TITLE, FOLIO_SUBTITLE, DEFAULT_OUTPUT_TYPE_ID, OWNER_USER_ID, DEFAULT_CHART_TYPE_ID, MAX_ROWS_PER_SECTION, MAX_ROWS) (SELECT l_ids(1), 'Transaction Totals Graph', 'LITERAL:Transaction Totals Graph', 'MESSAGE:From {params.StartDate, DATE, MM/dd/yyyy} to {params.EndDate, DATE, MM/dd/yyyy}', 28, 929, 24, -1, -1 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 1, 5, 'LITERAL:Day', '', '', 'DATE:MM/dd/yyyy', '', '', '', '', 2, '', '', 'DATE' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 811, 'ASC', 0, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 2, 5, 'LITERAL:Amount', '', '', 'CURRENCY', '', '', '', '', 0, '', '', 'NUMBER' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 51, 'ASC', 1, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 3, 4, 'LITERAL:Type', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 22, 'ASC', 0, 0 FROM DUAL);
INSERT INTO FOLIO_DIRECTIVE(FOLIO_DIRECTIVE_ID, FOLIO_ID, DIRECTIVE_ID, DIRECTIVE_VALUE) (SELECT SEQ_FOLIO_DIRECTIVE_ID.NEXTVAL, l_ids(1), 151, 'INDEX(ar IX_AR_TERMINAL_TRAN_DATE)' FROM DUAL);
INSERT INTO FOLIO_DIRECTIVE(FOLIO_DIRECTIVE_ID, FOLIO_ID, DIRECTIVE_ID, DIRECTIVE_VALUE) (SELECT SEQ_FOLIO_DIRECTIVE_ID.NEXTVAL, l_ids(1), 14, '2.0' FROM DUAL);
l_ids.EXTEND; SELECT SEQ_FILTER_GROUP_ID.NEXTVAL INTO l_ids(l_ids.LAST) FROM DUAL;
INSERT INTO FILTER_GROUP(PARENT_GROUP_ID, FILTER_GROUP_ID, SEPARATOR) (SELECT NULL, l_ids(l_ids.LAST), 'AND' FROM DUAL);
l_ids.EXTEND; SELECT SEQ_FILTER_ID.NEXTVAL INTO l_ids(l_ids.LAST) FROM DUAL;
INSERT INTO FILTER(FILTER_ID, FIELD_ID, FILTER_OPERATOR_ID, AGGREGATE_TYPE_ID, FILTER_GROUP_ID) (SELECT l_ids(l_ids.LAST), 440, 19, 0, l_ids(l_ids.LAST-1) FROM DUAL);
INSERT INTO FILTER_PARAM(FILTER_ID, PARAM_INDEX, PARAM_NAME, PARAM_PROMPT, PARAM_VALUE, PARAM_LABEL, PARAM_SQL_TYPE, PARAM_EDITOR) (SELECT l_ids(l_ids.LAST), 1, 'StartDate', 'Enter the value for startDate', '', 'StartDate', 'DATE', 'DATE' FROM DUAL);
INSERT INTO FILTER_PARAM(FILTER_ID, PARAM_INDEX, PARAM_NAME, PARAM_PROMPT, PARAM_VALUE, PARAM_LABEL, PARAM_SQL_TYPE, PARAM_EDITOR) (SELECT l_ids(l_ids.LAST), 2, 'EndDate', 'Enter the value for endDate', '', 'EndDate', 'DATE', 'DATE' FROM DUAL);
l_ids.TRIM;
l_ids.EXTEND; SELECT SEQ_FILTER_ID.NEXTVAL INTO l_ids(l_ids.LAST) FROM DUAL;
INSERT INTO FILTER(FILTER_ID, FIELD_ID, FILTER_OPERATOR_ID, AGGREGATE_TYPE_ID, FILTER_GROUP_ID) (SELECT l_ids(l_ids.LAST), 2021, 27, 0, l_ids(l_ids.LAST-1) FROM DUAL);
INSERT INTO FILTER_PARAM(FILTER_ID, PARAM_INDEX, PARAM_NAME, PARAM_PROMPT, PARAM_VALUE, PARAM_LABEL, PARAM_SQL_TYPE, PARAM_EDITOR) (SELECT l_ids(l_ids.LAST), 1, 'TransTypeId', 'Enter the value for transTypeId', '', 'TransTypeId', 'ARRAY:DECIMAL(22)', 'TEXT:;^(\d+([.]\d+)?[,])*\d+([.]\d+)?$' FROM DUAL);
l_ids.TRIM;
UPDATE FOLIO SET FILTER_GROUP_ID = l_ids(l_ids.LAST) WHERE FOLIO_ID = l_ids(1);
l_ids.TRIM;
END;
/
COMMIT;

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/FOLIO_CONF/folio_exports/folio_device_health_report_v3.sql?rev=HEAD
SET DEFINE OFF;
ALTER SESSION SET CURRENT_SCHEMA = FOLIO_CONF;
DECLARE
  l_ids NUMBER_TABLE := NUMBER_TABLE(0, 0);
BEGIN
FOLIO_CONF.FOLIO_PKG.DELETE_FOLIO(1584);
SELECT 1584 INTO l_ids(1) FROM DUAL;
INSERT INTO FOLIO(FOLIO_ID, FOLIO_NAME, FOLIO_TITLE, FOLIO_SUBTITLE, DEFAULT_OUTPUT_TYPE_ID, OWNER_USER_ID, DEFAULT_CHART_TYPE_ID, MAX_ROWS_PER_SECTION, MAX_ROWS) (SELECT l_ids(1), 'Device Health Report v3', 'LITERAL:Device Health Report', 'MESSAGE:{params.ShowAll,MATCH,1=All Devices;.*=''{params.HealthClassification,MATCH,NO_COMM=Service Required - No Communication in {params.NoCommDaysMin} to {params.NoCommDaysMax} Days;NO_CASHLESS=Service Required - No Cashless in {params.NoCashlessDaysMin} to {params.NoCashlessDaysMax} Days;REASSIGN_NEEDED=Inactive Devices Needing Redeployment;DEACTIVATION_NEEDED=Inactive Devices Needing Deactivation;INACTIVE=Other Inactive Devices;INVENTORY=Inventory (Never Called In);ACTIVE=Active Devices;}''}', 22, 932, NULL, -1, -1 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 1, 1, 'LITERAL:Customer', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 101, 'ASC', 0, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 2, 1, 'LITERAL:Region', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 15, 'ASC', 0, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 3, 1, 'LITERAL:Asset #', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 761, 'ASC', 0, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 4, 1, 'LITERAL:Location', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 16, 'ASC', 0, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 5, 1, 'LITERAL:Device', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 60, 'ASC', 10, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 6, 1, 'LITERAL:Last Fee', '', '', 'MESSAGE:{0,NULL,''{1,CURRENCY} on {0,DATE,MM/dd/yyyy} (Owned)''}{0,NULL,''{2,NULL,; }''}{2,NULL,''{3,CURRENCY} on {2,DATE,MM/dd/yyyy} (Rental)''}', '', '', 'MESSAGE:{0,NULL,''{2,NULL,B,O}'',''{2,NULL,R,N}''}_{0,DATE,yyyyMMdd}_{2,DATE,yyyyMMdd}_{0,NULL,''{1,NUMBER,000000.00}''}_{2,NULL,''{3,NUMBER,000000.00}''}', '', 2, '', '', 'STRING' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 9293, 'ASC', 0, 0 FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 2, 9294, 'ASC', 0, 0 FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 3, 9295, 'ASC', 0, 0 FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 4, 9296, 'ASC', 0, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 7, 1, 'LITERAL:# Days Since Last Communication', '', '', 'NUMBER:#######0', '', '', '', '', 2, '', 'LITERAL:text-align: center', 'NUMBER' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 2401, 'ASC', 4, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 8, 1, 'LITERAL:Date of Last Communication', '', '', 'DATE:MM/dd/yyyy hh:mm:ss a', '', '', 'DATE:MM/dd/yyyy hh:mm:ss a', '', 2, '', '', 'DATE' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 2393, 'ASC', 3, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 9, 1, 'LITERAL:# Days Since Last Cashless Tran', '', '', 'NUMBER:#######0', '', '', '', '', 2, '', 'LITERAL:text-align: center', 'NUMBER' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 2403, 'ASC', 4, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 10, 1, 'LITERAL:Last Cashless Tran Date', '', '', 'DATE:MM/dd/yyyy hh:mm:ss a', '', '', 'DATE:MM/dd/yyyy hh:mm:ss a', '', 2, '', '', 'DATE' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 2395, 'ASC', 3, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 11, 1, 'LITERAL:# Days Since Last Cash Tran', '', '', 'NUMBER:#######0', '', '', '', '', 2, '', 'LITERAL:text-align: center', 'NUMBER' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 2683, 'ASC', 4, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 12, 1, 'LITERAL:Last Cash Tran Date', '', '', 'DATE:MM/dd/yyyy hh:mm:ss a', '', '', 'DATE:MM/dd/yyyy hh:mm:ss a', '', 2, '', '', 'DATE' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 2397, 'ASC', 3, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 13, 1, 'LITERAL:# Days Since Last Fill', '', '', 'NUMBER:#######0', '', '', '', '', 2, '', '', 'NUMBER' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 2762, 'ASC', 4, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 14, 1, 'LITERAL:Last Fill Date', '', '', 'DATE:MM/dd/yyyy hh:mm:ss a', '', '', '', '', 2, '', '', 'DATE' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 2449, 'ASC', 3, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 15, 1, 'LITERAL:# Days Since Last DEX', '', '', 'NUMBER:#######0', '', '', '', '', 2, '', '', 'NUMBER' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 2764, 'ASC', 4, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 16, 1, 'LITERAL:Last DEX File Date', '', '', 'DATE:MM/dd/yyyy hh:mm:ss a', '', '', 'DATE:MM/dd/yyyy hh:mm:ss a', '', 2, '', '', 'DATE' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 2782, 'ASC', 3, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 17, 1, 'LITERAL:Status', '', '', 'MATCH:ACTIVE=Okay;NO_COMM=No Communication;NO_CASHLESS=No Credit Sales;INACTIVE_OWNED=Inactive (Owned);INACTIVE_RENTAL=Inactive (Rental);INACTIVE=Inactive (Other);INVENTORY=Inventory;', '', '', 'MATCH:ACTIVE=Okay;NO_COMM=No Communication;NO_CASHLESS=No Credit Sales;INACTIVE_OWNED=Inactive (Owned);INACTIVE_RENTAL=Inactive (Rental);INACTIVE=Inactive (Other);INVENTORY=Inventory;', '', 2, '', '', 'STRING' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 9292, 'ASC', 0, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 18, 1, '', '', '', 'MESSAGE:{,MATCH,NO_COMM=Service Required - No Communication in {params.NoCommDaysMin} to {params.NoCommDaysMax} Days;NO_CASHLESS=Service Required - No Cashless in {params.NoCashlessDaysMin} to {params.NoCashlessDaysMax} Days;INACTIVE_OWNED=Inactive (Owned);INACTIVE_RENTAL=Inactive (Rental);INACTIVE=Inactive (Other);INVENTORY=Inventory (Never Called In);ACTIVE=Active Devices;}', '', '', '', '', 3, '', 'LITERAL:text-align: center; font-size: 1.2em;', 'STRING' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 9292, 'ASC', 0, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 19, 6, '', '', '', '', '', '', '', '', 2, '', '', 'NUMBER' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 18, 'ASC', 0, 0 FROM DUAL);
INSERT INTO FOLIO_DIRECTIVE(FOLIO_DIRECTIVE_ID, FOLIO_ID, DIRECTIVE_ID, DIRECTIVE_VALUE) (SELECT SEQ_FOLIO_DIRECTIVE_ID.NEXTVAL, l_ids(1), 14, '2.0' FROM DUAL);
l_ids.EXTEND; SELECT SEQ_FILTER_GROUP_ID.NEXTVAL INTO l_ids(l_ids.LAST) FROM DUAL;
INSERT INTO FILTER_GROUP(PARENT_GROUP_ID, FILTER_GROUP_ID, SEPARATOR) (SELECT NULL, l_ids(l_ids.LAST), 'AND' FROM DUAL);
l_ids.EXTEND; SELECT SEQ_FILTER_ID.NEXTVAL INTO l_ids(l_ids.LAST) FROM DUAL;
INSERT INTO FILTER(FILTER_ID, FIELD_ID, FILTER_OPERATOR_ID, AGGREGATE_TYPE_ID, FILTER_GROUP_ID) (SELECT l_ids(l_ids.LAST), 321, 2, 0, l_ids(l_ids.LAST-1) FROM DUAL);
INSERT INTO FILTER_PARAM(FILTER_ID, PARAM_INDEX, PARAM_NAME, PARAM_PROMPT, PARAM_VALUE, PARAM_LABEL, PARAM_SQL_TYPE, PARAM_EDITOR) (SELECT l_ids(l_ids.LAST), 1, 'deviceTypeId', 'Enter the value for Device Type Id', '14', 'Device Type Id', 'NUMERIC', 'NUMBER' FROM DUAL);
l_ids.TRIM;
l_ids.EXTEND; SELECT SEQ_FILTER_GROUP_ID.NEXTVAL INTO l_ids(l_ids.LAST) FROM DUAL;
INSERT INTO FILTER_GROUP(PARENT_GROUP_ID, FILTER_GROUP_ID, SEPARATOR) (SELECT l_ids(l_ids.LAST-1), l_ids(l_ids.LAST), 'OR' FROM DUAL);
l_ids.EXTEND; SELECT SEQ_FILTER_ID.NEXTVAL INTO l_ids(l_ids.LAST) FROM DUAL;
INSERT INTO FILTER(FILTER_ID, FIELD_ID, FILTER_OPERATOR_ID, AGGREGATE_TYPE_ID, FILTER_GROUP_ID) (SELECT l_ids(l_ids.LAST), 9292, 1, 0, l_ids(l_ids.LAST-1) FROM DUAL);
INSERT INTO FILTER_PARAM(FILTER_ID, PARAM_INDEX, PARAM_NAME, PARAM_PROMPT, PARAM_VALUE, PARAM_LABEL, PARAM_SQL_TYPE, PARAM_EDITOR) (SELECT l_ids(l_ids.LAST), 1, 'HealthClassification', 'Enter the value for Health Classification', '', 'Health Classification', 'VARCHAR', 'TEXT' FROM DUAL);
l_ids.TRIM;
l_ids.EXTEND; SELECT SEQ_FILTER_ID.NEXTVAL INTO l_ids(l_ids.LAST) FROM DUAL;
INSERT INTO FILTER(FILTER_ID, FIELD_ID, FILTER_OPERATOR_ID, AGGREGATE_TYPE_ID, FILTER_GROUP_ID) (SELECT l_ids(l_ids.LAST), 242, 1, 0, l_ids(l_ids.LAST-1) FROM DUAL);
INSERT INTO FILTER_PARAM(FILTER_ID, PARAM_INDEX, PARAM_NAME, PARAM_PROMPT, PARAM_VALUE, PARAM_LABEL, PARAM_SQL_TYPE, PARAM_EDITOR) (SELECT l_ids(l_ids.LAST), 1, 'ShowAll', 'Enter the value for Include All Constant', '0', 'Include All Constant', 'NUMERIC', 'CHECKBOX:+0=No;1=Yes' FROM DUAL);
l_ids.TRIM;
l_ids.TRIM;
UPDATE FOLIO SET FILTER_GROUP_ID = l_ids(l_ids.LAST) WHERE FOLIO_ID = l_ids(1);
l_ids.TRIM;
END;
/
COMMIT;

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/FOLIO_CONF/folio_exports/folio_refund_search_transactions.sql?rev=1.12
SET DEFINE OFF;
ALTER SESSION SET CURRENT_SCHEMA = FOLIO_CONF;
DECLARE
  l_ids NUMBER_TABLE := NUMBER_TABLE(0, 0);
BEGIN
FOLIO_CONF.FOLIO_PKG.DELETE_FOLIO(81);
SELECT 81 INTO l_ids(1) FROM DUAL;
INSERT INTO FOLIO(FOLIO_ID, FOLIO_NAME, FOLIO_TITLE, FOLIO_SUBTITLE, DEFAULT_OUTPUT_TYPE_ID, OWNER_USER_ID, DEFAULT_CHART_TYPE_ID, MAX_ROWS_PER_SECTION, MAX_ROWS) (SELECT l_ids(1), 'Refund Search Transactions', '', '', 22, 2, NULL, -1, 1000 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 1, 6, 'LITERAL:Tran Id', '', '', '', 'MESSAGE:{1,CHOICE,-1#javascript: createRefund({0},"refund");|90<}', '', '', '', 2, '', '', 'NUMBER' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 444, 'ASC', 0, 0 FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 2, 9374, 'ASC', 0, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 2, 7, '', '', '', 'MESSAGE:{1,CHOICE,-1#tranIds|90<}', '', '', 'MESSAGE:{0}', '', 2, '', '', 'STRING' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 444, 'ASC', 0, 0 FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 2, 9374, 'ASC', 0, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 3, 1, 'LITERAL:Transaction Id', '', '', '', '', '', '', '', 2, '', '', 'NUMBER' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 444, 'ASC', 0, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 4, 1, 'LITERAL:Device', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 60, 'ASC', 0, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 5, 1, 'LITERAL:Customer', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 2447, 'ASC', 0, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 6, 1, 'LITERAL:Location', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 16, 'ASC', 0, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 7, 1, 'LITERAL:Amount', '', '', 'MESSAGE:{0} {1,NUMBER,#,##0.00} {2}', '', '', 'MESSAGE:{1}', '', 2, '', '', 'NUMBER' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 181, 'ASC', 0, 0 FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 2, 324, 'ASC', 1, 0 FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 3, 182, 'ASC', 0, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 8, 1, 'LITERAL:Transaction Date', '', '', 'DATE:MM/dd/yyyy hh:mm:ss a', '', '', 'DATE:MM/dd/yyyy hh:mm:ss a', '', 2, '', '', 'DATE' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 426, 'ASC', 0, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 9, 1, 'LITERAL:AP Code', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 789, 'ASC', 0, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 10, 1, 'LITERAL:Card Number', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 787, 'ASC', 0, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 11, 1, 'LITERAL:Card Id', '', '', '', '', '', '', '', 2, '', '', 'NUMBER' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 553, 'ASC', 0, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 12, 1, 'LITERAL:EV Number', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 521, 'ASC', 0, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 13, 1, 'LITERAL:Device Tran Code', '', '', '', '', '', '', '', 2, '', '', 'STRING' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 492, 'ASC', 0, 0 FROM DUAL);
SELECT SEQ_FOLIO_PILLAR_ID.NEXTVAL INTO l_ids(2) FROM DUAL;
INSERT INTO FOLIO_PILLAR(FOLIO_PILLAR_ID, FOLIO_ID, PILLAR_INDEX, PILLAR_TYPE_ID, PILLAR_LABEL, PILLAR_DESC, PILLAR_WIDTH, DISPLAY_FORMAT, ACTION_FORMAT, HELP_FORMAT, SORT_FORMAT, CSS_STYLE, GROUPING_LEVEL, PERCENT_FORMAT, STYLE_FORMAT, SORT_TYPE) (SELECT l_ids(2), l_ids(1), 14, 8, '', '', '', 'LITERAL:Verify Full Card', 'MESSAGE:javascript:verifyCardId(''''{,PREPARE,SCRIPT}'''');', 'LITERAL:Click check match of full card number', '', '', 2, '', '', 'STRING' FROM DUAL);
INSERT INTO FOLIO_PILLAR_FIELD(FOLIO_PILLAR_ID, FIELD_INDEX, FIELD_ID, SORT_ORDER, AGGREGATE_TYPE_ID, SORT_INDEX) (SELECT l_ids(2), 1, 553, 'ASC', 0, 0 FROM DUAL);
INSERT INTO FOLIO_DIRECTIVE(FOLIO_DIRECTIVE_ID, FOLIO_ID, DIRECTIVE_ID, DIRECTIVE_VALUE) (SELECT SEQ_FOLIO_DIRECTIVE_ID.NEXTVAL, l_ids(1), 8, 'false' FROM DUAL);
INSERT INTO FOLIO_DIRECTIVE(FOLIO_DIRECTIVE_ID, FOLIO_ID, DIRECTIVE_ID, DIRECTIVE_VALUE) (SELECT SEQ_FOLIO_DIRECTIVE_ID.NEXTVAL, l_ids(1), 14, '2.0' FROM DUAL);
INSERT INTO FOLIO_DIRECTIVE(FOLIO_DIRECTIVE_ID, FOLIO_ID, DIRECTIVE_ID, DIRECTIVE_VALUE) (SELECT SEQ_FOLIO_DIRECTIVE_ID.NEXTVAL, l_ids(1), 7, 'false' FROM DUAL);
l_ids.EXTEND; SELECT SEQ_FILTER_GROUP_ID.NEXTVAL INTO l_ids(l_ids.LAST) FROM DUAL;
INSERT INTO FILTER_GROUP(PARENT_GROUP_ID, FILTER_GROUP_ID, SEPARATOR) (SELECT NULL, l_ids(l_ids.LAST), 'AND' FROM DUAL);
l_ids.EXTEND; SELECT SEQ_FILTER_ID.NEXTVAL INTO l_ids(l_ids.LAST) FROM DUAL;
INSERT INTO FILTER(FILTER_ID, FIELD_ID, FILTER_OPERATOR_ID, AGGREGATE_TYPE_ID, FILTER_GROUP_ID) (SELECT l_ids(l_ids.LAST), 426, 22, 0, l_ids(l_ids.LAST-1) FROM DUAL);
INSERT INTO FILTER_PARAM(FILTER_ID, PARAM_INDEX, PARAM_NAME, PARAM_PROMPT, PARAM_VALUE, PARAM_LABEL, PARAM_SQL_TYPE, PARAM_EDITOR) (SELECT l_ids(l_ids.LAST), 1, 'startDate', 'Enter the value for StartDate', '', 'startDate', 'DATE', 'DATE' FROM DUAL);
INSERT INTO FILTER_PARAM(FILTER_ID, PARAM_INDEX, PARAM_NAME, PARAM_PROMPT, PARAM_VALUE, PARAM_LABEL, PARAM_SQL_TYPE, PARAM_EDITOR) (SELECT l_ids(l_ids.LAST), 2, 'endDate', 'Enter the value for EndDate', '', 'endDate', 'DATE', 'DATE' FROM DUAL);
l_ids.TRIM;
l_ids.EXTEND; SELECT SEQ_FILTER_ID.NEXTVAL INTO l_ids(l_ids.LAST) FROM DUAL;
INSERT INTO FILTER(FILTER_ID, FIELD_ID, FILTER_OPERATOR_ID, AGGREGATE_TYPE_ID, FILTER_GROUP_ID) (SELECT l_ids(l_ids.LAST), 161, 1, 0, l_ids(l_ids.LAST-1) FROM DUAL);
INSERT INTO FILTER_PARAM(FILTER_ID, PARAM_INDEX, PARAM_NAME, PARAM_PROMPT, PARAM_VALUE, PARAM_LABEL, PARAM_SQL_TYPE, PARAM_EDITOR) (SELECT l_ids(l_ids.LAST), 1, 'currencyId', 'Enter the value for currencyId', '', 'currencyId', 'NUMERIC', 'NUMBER' FROM DUAL);
l_ids.TRIM;
l_ids.EXTEND; SELECT SEQ_FILTER_ID.NEXTVAL INTO l_ids(l_ids.LAST) FROM DUAL;
INSERT INTO FILTER(FILTER_ID, FIELD_ID, FILTER_OPERATOR_ID, AGGREGATE_TYPE_ID, FILTER_GROUP_ID) (SELECT l_ids(l_ids.LAST), 9224, 1, 0, l_ids(l_ids.LAST-1) FROM DUAL);
INSERT INTO FILTER_PARAM(FILTER_ID, PARAM_INDEX, PARAM_NAME, PARAM_PROMPT, PARAM_VALUE, PARAM_LABEL, PARAM_SQL_TYPE, PARAM_EDITOR) (SELECT l_ids(l_ids.LAST), 1, '', '', 'Y', '', 'VARCHAR', 'TEXT' FROM DUAL);
l_ids.TRIM;
l_ids.EXTEND; SELECT SEQ_FILTER_GROUP_ID.NEXTVAL INTO l_ids(l_ids.LAST) FROM DUAL;
INSERT INTO FILTER_GROUP(PARENT_GROUP_ID, FILTER_GROUP_ID, SEPARATOR) (SELECT l_ids(l_ids.LAST-1), l_ids(l_ids.LAST), 'OR' FROM DUAL);
l_ids.EXTEND; SELECT SEQ_FILTER_ID.NEXTVAL INTO l_ids(l_ids.LAST) FROM DUAL;
INSERT INTO FILTER(FILTER_ID, FIELD_ID, FILTER_OPERATOR_ID, AGGREGATE_TYPE_ID, FILTER_GROUP_ID) (SELECT l_ids(l_ids.LAST), 324, 1, 1, l_ids(l_ids.LAST-1) FROM DUAL);
INSERT INTO FILTER_PARAM(FILTER_ID, PARAM_INDEX, PARAM_NAME, PARAM_PROMPT, PARAM_VALUE, PARAM_LABEL, PARAM_SQL_TYPE, PARAM_EDITOR) (SELECT l_ids(l_ids.LAST), 1, 'searchAmt', 'Enter the value for searchAmt', '', 'searchAmt', 'NUMERIC', 'NUMBER' FROM DUAL);
l_ids.TRIM;
l_ids.EXTEND; SELECT SEQ_FILTER_ID.NEXTVAL INTO l_ids(l_ids.LAST) FROM DUAL;
INSERT INTO FILTER(FILTER_ID, FIELD_ID, FILTER_OPERATOR_ID, AGGREGATE_TYPE_ID, FILTER_GROUP_ID) (SELECT l_ids(l_ids.LAST), 324, 24, 1, l_ids(l_ids.LAST-1) FROM DUAL);
INSERT INTO FILTER_PARAM(FILTER_ID, PARAM_INDEX, PARAM_NAME, PARAM_PROMPT, PARAM_VALUE, PARAM_LABEL, PARAM_SQL_TYPE, PARAM_EDITOR) (SELECT l_ids(l_ids.LAST), 1, 'searchAmt', 'Enter the value for searchAmt', '', 'searchAmt', 'VARCHAR', 'NUMBER' FROM DUAL);
l_ids.TRIM;
l_ids.TRIM;
UPDATE FOLIO SET FILTER_GROUP_ID = l_ids(l_ids.LAST) WHERE FOLIO_ID = l_ids(1);
l_ids.TRIM;
END;
/
COMMIT;

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/releases/REL_Edge_Update_R40/R40.USAPDB.3.sql?rev=HEAD
INSERT INTO REPORT.PRIV(PRIV_ID, DESCRIPTION, INTERNAL_EXTERNAL_FLAG, CUSTOMER_MASTER_USER_DEFAULT)
    SELECT 25, 'Advanced Dashboard', 'E', 'N'
      FROM DUAL
     WHERE NOT EXISTS(SELECT 1 FROM REPORT.PRIV WHERE PRIV_ID = 25);

INSERT INTO WEB_CONTENT.REQUIREMENT(REQUIREMENT_ID, REQUIREMENT_NAME, REQUIREMENT_CLASS, REQUIREMENT_PARAM_CLASS, REQUIREMENT_PARAM_VALUE)
    SELECT 36, 'REQ_ADVANCED_DASHBOARD', 'com.usatech.usalive.link.HasPrivilegeRequirement', 'java.lang.Integer', '25'
      FROM DUAL
     WHERE NOT EXISTS(SELECT 1 FROM WEB_CONTENT.REQUIREMENT WHERE REQUIREMENT_ID = 36)
    UNION ALL
    SELECT 37, 'REQ_NOT_ADVANCED_DASHBOARD', 'com.usatech.usalive.link.LacksPrivilegeRequirement', 'java.lang.Integer', '25'
      FROM DUAL
     WHERE NOT EXISTS(SELECT 1 FROM WEB_CONTENT.REQUIREMENT WHERE REQUIREMENT_ID = 37);
     
INSERT INTO WEB_CONTENT.WEB_LINK(WEB_LINK_ID,WEB_LINK_LABEL,WEB_LINK_URL,WEB_LINK_DESC,WEB_LINK_GROUP,WEB_LINK_USAGE,WEB_LINK_ORDER) 
    SELECT 265, 'Dashboard', './dashboard.html','Dashboard','General', 'M', 112
      FROM DUAL
     WHERE NOT EXISTS(SELECT 1 FROM WEB_CONTENT.WEB_LINK WHERE WEB_LINK_ID = 265);

INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT(WEB_LINK_ID, REQUIREMENT_ID)
    SELECT WEB_LINK_ID, 36
      FROM WEB_CONTENT.WEB_LINK WL
     WHERE WEB_LINK_USAGE = 'M' 
       AND WEB_LINK_URL LIKE '%dashboard.html'
       AND NOT EXISTS(SELECT 1 FROM WEB_CONTENT.WEB_LINK_REQUIREMENT WLR WHERE WLR.WEB_LINK_ID = WL.WEB_LINK_ID AND WLR.REQUIREMENT_ID = 36);

INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT(WEB_LINK_ID, REQUIREMENT_ID)
    SELECT WEB_LINK_ID, 37
      FROM WEB_CONTENT.WEB_LINK WL
     WHERE WEB_LINK_USAGE = 'M' 
       AND WEB_LINK_URL LIKE '%home_stat.i'
       AND NOT EXISTS(SELECT 1 FROM WEB_CONTENT.WEB_LINK_REQUIREMENT WLR WHERE WLR.WEB_LINK_ID = WL.WEB_LINK_ID AND WLR.REQUIREMENT_ID = 37);

INSERT INTO REPORT.USER_PRIVS(USER_ID, PRIV_ID)
    SELECT U.USER_ID, 25
      FROM REPORT.USER_LOGIN U
      CROSS JOIN V$DATABASE D 
     WHERE ((D.NAME LIKE 'USADEV%' AND U.USER_NAME IN('aroyce@usatech.com', 'mwhitelock@usatech.com', 'bkrug@usatech.com', 'dkouznetsov@usatech.com', 'yhe@usatech.com'))
        OR (D.NAME LIKE 'ECC%' AND U.USER_NAME IN('aroyce@usatech.com', 'mwhitelock@usatech.com'))
        OR U.USER_NAME IN('bhebert@usatech.com', 'aseymour@usatech.com'))
       AND U.USER_ID NOT IN(SELECT USER_ID FROM REPORT.USER_PRIVS WHERE PRIV_ID = 25);

INSERT INTO REPORT.USER_PRIVS(USER_ID, PRIV_ID)
    SELECT U.USER_ID, 26
      FROM REPORT.USER_LOGIN U
      CROSS JOIN V$DATABASE D 
     WHERE ((D.NAME LIKE 'USADEV%' AND U.USER_NAME IN('aroyce@usatech.com', 'mwhitelock@usatech.com', 'bkrug@usatech.com', 'dkouznetsov@usatech.com', 'yhe@usatech.com'))
        OR (D.NAME LIKE 'ECC%' AND U.USER_NAME IN('aroyce@usatech.com', 'mwhitelock@usatech.com'))
        OR U.USER_NAME IN('sscheiderman@usatech.com', 'aseymour@usatech.com'))
       AND U.USER_ID NOT IN(SELECT USER_ID FROM REPORT.USER_PRIVS WHERE PRIV_ID = 26);
        
COMMIT;   

