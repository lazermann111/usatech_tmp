insert into web_content.web_link(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER) 
values(web_content.seq_web_link_id.nextval, 'User Privileges', 'user_privileges.html', 'Shows user privileges', 'Accounting', 'W', 0);
insert into web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) select web_link_id, 1 from web_content.web_link where web_link_label = 'User Privileges';
insert into web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) select web_link_id, 26 from web_content.web_link where web_link_label = 'User Privileges';
COMMIT;

ALTER TABLE CORP.PAYOR ADD(STATUS_CD VARCHAR2(1) DEFAULT 'A' NOT NULL);
ALTER TABLE CORP.PAYOR ADD CONSTRAINT FK_PAYOR_STATUS_CD FOREIGN KEY(STATUS_CD) REFERENCES PSS.STATUS(STATUS_CD);

