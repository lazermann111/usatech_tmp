--set serveroutput on
DECLARE
  CURSOR l_batch IS
   select batch_id from corp.batch where doc_id in (
	select doc_id from corp.doc where AUTO_PROCESS_START_TS>cast('01-May-2014' as date) and status='D');
  l_count NUMBER:=0;

BEGIN
  
    FOR l_rec IN l_batch LOOP      
      	select /*+index (a IX_AR_PAYMENT_BATCH_ID) */ count(*) into l_count from report.activity_ref a where create_dt>cast('01-May-2014' as date) and payment_batch_id=l_rec.batch_id;
      	--dbms_output.put_line('for batchId:'||l_rec.batch_id||' count='||l_count);
      	IF l_count >0 THEN
      		update /*+index (a IX_AR_PAYMENT_BATCH_ID) */ report.activity_ref a set payment_batch_id=null where payment_batch_id=l_rec.batch_id and create_dt>cast('01-May-2014' as date);
        	commit;
      	END IF;
    END LOOP;

END;
/