ALTER TABLE CORP.LEDGER ADD REPORT_DATE DATE;
CREATE INDEX CORP.IDX_LEDGER_REPORT_DATE ON CORP.LEDGER(REPORT_DATE) TABLESPACE CORP_INDX LOCAL ONLINE;
