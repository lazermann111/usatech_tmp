/*
SELECT *
      FROM REPORT.REGION R
      JOIN REPORT.TERMINAL_REGION TR ON R.REGION_ID = TR.REGION_ID
      JOIN REPORT.TERMINAL T ON TR.TERMINAL_ID = T.TERMINAL_ID
      LEFT JOIN REPORT.REGION RN ON (R.REGION_NAME = RN.REGION_NAME OR (R.REGION_NAME IS NULL AND RN.REGION_NAME IS NULL)) AND RN.CUSTOMER_ID = T.CUSTOMER_ID
     WHERE R.CUSTOMER_ID != T.CUSTOMER_ID
     ORDER BY T.CUSTOMER_ID, R.REGION_NAME;
     
SELECT * 
      FROM REPORT.REGION R
      JOIN REPORT.TERMINAL_REGION TR ON R.REGION_ID = TR.REGION_ID
      JOIN REPORT.TERMINAL T ON TR.TERMINAL_ID = T.TERMINAL_ID
     WHERE T.TERMINAL_ID = :terminal_id
     ORDER BY T.CUSTOMER_ID, R.REGION_NAME;
     --629049	53	Bay Springs	32571		
     select * from corp.customer where customer_id in(3,53);
     select * from report.region where region_id = 32571;

 SELECT MAX(RN.REGION_ID), RO.PARENT_REGION_ID, RO.REGION_NAME
         -- INTO ln_new_region_id, ln_old_parent_region_id, lv_region_name
          FROM REPORT.REGION RO 
          LEFT JOIN REPORT.REGION RN ON RN.CUSTOMER_ID = :pn_new_customer_id AND (RO.REGION_NAME = RN.REGION_NAME OR (RO.REGION_NAME IS NULL AND RN.REGION_NAME IS NULL))
         WHERE RO.REGION_ID = :pn_old_region_id
         GROUP BY RO.PARENT_REGION_ID, RO.REGION_NAME;
         UPDATE REPORT.TERMINAL_REGION
           set REGION_ID = 32571
          WHERE TERMINAL_ID = 629049;
629049	53	Bay Springs	32571
629044	53	CANTEEN 4/1/2014	74537
629050	53	CANTEEN 4/1/2014	74537
629048	53	CANTEEN 4/1/2014	74537
629047	53	CANTEEN 4/1/2014	74537      
*/
DECLARE
    CURSOR l_cur IS
    select * from (
    SELECT TR.TERMINAL_ID, T.CUSTOMER_ID, R.REGION_NAME, TR.REGION_ID, RN.REGION_ID NEW_REGION_ID, R.PARENT_REGION_ID
      FROM REPORT.REGION R
      JOIN REPORT.TERMINAL_REGION TR ON R.REGION_ID = TR.REGION_ID
      JOIN REPORT.TERMINAL T ON TR.TERMINAL_ID = T.TERMINAL_ID
      LEFT JOIN REPORT.REGION RN ON (R.REGION_NAME = RN.REGION_NAME OR (R.REGION_NAME IS NULL AND RN.REGION_NAME IS NULL)) AND RN.CUSTOMER_ID = T.CUSTOMER_ID
     WHERE R.CUSTOMER_ID != T.CUSTOMER_ID
     --and  R.PARENT_REGION_ID is null
     ORDER BY T.CUSTOMER_ID, R.REGION_NAME)
     --where rownum < 1000
     ;
    ln_new_region_id REPORT.REGION.REGION_ID%TYPE;
    ln_new_parent_region_id REPORT.REGION.REGION_ID%TYPE;
    ln_prev_customer_id CORP.CUSTOMER.CUSTOMER_ID%TYPE;
    lv_prev_region_name REPORT.REGION.REGION_NAME%TYPE;
    FUNCTION GET_OR_CREATE_REGION_CLONE(
        pn_old_region_id REPORT.REGION.REGION_ID%TYPE, 
        pn_new_customer_id CORP.CUSTOMER.CUSTOMER_ID%TYPE)
        RETURN REPORT.REGION.REGION_ID%TYPE
    IS
        ln_new_region_id REPORT.REGION.REGION_ID%TYPE;
        ln_old_parent_region_id REPORT.REGION.REGION_ID%TYPE;
        ln_new_parent_region_id REPORT.REGION.REGION_ID%TYPE;
        lv_region_name REPORT.REGION.REGION_NAME%TYPE;
    BEGIN
        SELECT MAX(RN.REGION_ID), RO.PARENT_REGION_ID, RO.REGION_NAME
          INTO ln_new_region_id, ln_old_parent_region_id, lv_region_name
          FROM REPORT.REGION RO 
          LEFT JOIN REPORT.REGION RN ON RN.CUSTOMER_ID = pn_new_customer_id AND (RO.REGION_NAME = RN.REGION_NAME OR (RO.REGION_NAME IS NULL AND RN.REGION_NAME IS NULL))
         WHERE RO.REGION_ID = pn_old_region_id
         GROUP BY RO.PARENT_REGION_ID, RO.REGION_NAME;
        IF ln_new_region_id IS NOT NULL THEN
            RETURN ln_new_region_id;
        END IF;
        IF ln_old_parent_region_id IS NOT NULL THEN
            ln_new_parent_region_id := GET_OR_CREATE_REGION_CLONE(ln_old_parent_region_id, pn_new_customer_id);
        END IF;
        INSERT INTO REPORT.REGION(REGION_ID, REGION_NAME, CUSTOMER_ID, PARENT_REGION_ID)
             VALUES(REPORT.REGION_SEQ.NEXTVAL, lv_region_name, pn_new_customer_id, ln_new_parent_region_id)
             RETURNING REGION_ID INTO ln_new_region_id;
        RETURN ln_new_region_id;
    END;    
BEGIN
    FOR l_rec IN l_cur LOOP
        IF l_rec.NEW_REGION_ID IS NOT NULL THEN
            ln_new_region_id := l_rec.NEW_REGION_ID;
        ELSIF ln_new_region_id IS NULL OR NOT(ln_prev_customer_id = l_rec.CUSTOMER_ID AND (lv_prev_region_name = l_rec.REGION_NAME OR (lv_prev_region_name IS NULL AND l_rec.REGION_NAME IS NULL))) THEN
            ln_new_region_id := GET_OR_CREATE_REGION_CLONE(l_rec.REGION_ID, l_rec.CUSTOMER_ID);
        END IF;
        UPDATE REPORT.TERMINAL_REGION
           SET REGION_ID = ln_new_region_id
         WHERE TERMINAL_ID = l_rec.TERMINAL_ID
           AND REGION_ID = l_rec.REGION_ID;
        COMMIT;
        lv_prev_region_name := l_rec.REGION_NAME;
        ln_prev_customer_id := l_rec.CUSTOMER_ID;
    END LOOP;
END;
/
