update report.report_param set param_value='SELECT  
	    UPPER(e.EPORT_SERIAL_NUM) "Device Serial Num",
	    ar.REF_NBR "Ref Nbr",
	    tt.TRANS_TYPE_CODE "Trans Type Code",
	    REPORT.MASK_CARD(UPPER(ar.CARD_NUMBER), ar.TRANS_TYPE_ID) "Masked Card Number",
	    ar.TOTAL_AMOUNT "Total Amount",
	    ar.VEND_COLUMN "Vend Column",
	    ar.quantity "Quantity",
	    TO_CHAR(ar.TRAN_DATE, ''MM/dd/yyyy'') "Tran Date",
	    TO_CHAR(ar.TRAN_DATE, ''HH24:MI:SS'') "Tran Time",
	    cab.GLOBAL_ACCOUNT_ID "Card Id"
	FROM PSS.CONSUMER_ACCT_BASE cab
	    RIGHT OUTER JOIN (REPORT.ACTIVITY_REF ar
	    JOIN REPORT.EPORT e ON ar.EPORT_ID = e.EPORT_ID
	    JOIN REPORT.TRANS_TYPE tt ON ar.TRANS_TYPE_ID=tt.TRANS_TYPE_ID
	    ) ON ar.CONSUMER_ACCT_ID = cab.CONSUMER_ACCT_ID
	WHERE ar.TRAN_ID = {x}'
where report_id =(select report_id from report.reports where report_name='Single Transaction Data Export(CSV)')
and param_name='query';
commit;