INSERT INTO PSS.AUTHORITY_PAYMENT_MASK(AUTHORITY_PAYMENT_MASK_NAME, AUTHORITY_PAYMENT_MASK_DESC, AUTHORITY_PAYMENT_MASK_REGEX, AUTHORITY_PAYMENT_MASK_BREF, CARD_NAME)
SELECT 'Sprout Prepaid Card Manual Entry', 'Sprout Prepaid Card Manual Entry', '^(627722[0-9]{10})\|([0-9]{4})$', '1:1|2:5', 'Sprout Prepaid Card'
FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM PSS.AUTHORITY_PAYMENT_MASK WHERE AUTHORITY_PAYMENT_MASK_NAME = 'Sprout Prepaid Card Manual Entry');

INSERT INTO pss.payment_subtype(payment_subtype_name, payment_subtype_class, payment_subtype_key_name, client_payment_type_cd, payment_subtype_table_name, payment_subtype_key_desc_name, authority_payment_mask_id, status_cd, trans_type_id, card_type_label)
SELECT 'Sprout Prepaid Card Manual Entry', 'Sprout', 'TERMINAL_ID', 'T', 'TERMINAL', 'TERMINAL_DESC', AUTHORITY_PAYMENT_MASK_ID, 'A', 32, 'Sprout'
FROM PSS.AUTHORITY_PAYMENT_MASK
WHERE AUTHORITY_PAYMENT_MASK_NAME	= 'Sprout Prepaid Card Manual Entry'
AND NOT EXISTS(SELECT 1 FROM pss.payment_subtype WHERE payment_subtype_name = 'Sprout Prepaid Card Manual Entry');

INSERT INTO PSS.POS_PTA_TMPL(POS_PTA_TMPL_NAME, POS_PTA_TMPL_DESC)
SELECT 'SPECIAL MERGE: Sprout Prepaid Card Manual Entry', 'Setup to accept Sprout Prepaid Card Manual Entry' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'SPECIAL MERGE: Sprout Prepaid Card Manual Entry');

INSERT INTO PSS.POS_PTA_TMPL_ENTRY(POS_PTA_TMPL_ID, PAYMENT_SUBTYPE_ID, POS_PTA_ACTIVATION_OSET_HR, PAYMENT_SUBTYPE_KEY_ID, POS_PTA_PRIORITY, CURRENCY_CD)
SELECT (SELECT POS_PTA_TMPL_ID FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'SPECIAL MERGE: Sprout Prepaid Card Manual Entry'),
	(SELECT PAYMENT_SUBTYPE_ID FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'Sprout Prepaid Card Manual Entry'),
	0, (SELECT TERMINAL_ID FROM PSS.TERMINAL WHERE TERMINAL_DESC = 'Sprout Terminal'),
	1, 'USD' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM PSS.POS_PTA_TMPL_ENTRY 
	WHERE POS_PTA_TMPL_ID = (SELECT POS_PTA_TMPL_ID FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'SPECIAL MERGE: Sprout Prepaid Card Manual Entry')
		AND PAYMENT_SUBTYPE_ID = (SELECT PAYMENT_SUBTYPE_ID FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'Sprout Prepaid Card Manual Entry')
);
commit;

insert into pss.consumer_acct_type (consumer_acct_type_id, consumer_acct_type_desc, trans_type_id, consumer_acct_type_label, auth_hold_ind)
select 6,'Sprout Prepaid Account', 32,'Sprout Prepaid','N' from dual
where not exists(select 1 from pss.consumer_acct_type where consumer_acct_type_id=6);
commit;

insert into pss.consumer_type (consumer_type_id, consumer_type_desc)
select 8,'Sprout' from dual
where not exists(select 1 from pss.consumer_type where consumer_type_id=8);
commit;

insert into pss.tran_line_item_type
(tran_line_item_type_id, tran_line_item_type_desc, tran_line_item_type_sign_pn, tran_line_item_type_group_cd,mdb_number_ind,type_desc_as_product_ind)
values(556,'Sprout Replenishment','P','S','N','Y');

insert into pss.tran_line_item_type
(tran_line_item_type_id, tran_line_item_type_desc, tran_line_item_type_sign_pn, tran_line_item_type_group_cd,mdb_number_ind,type_desc_as_product_ind)
values(557,'Sprout Bonus Cash','P','S','N','Y');
commit;

insert into pss.tran_line_item_type
(tran_line_item_type_id, tran_line_item_type_desc, tran_line_item_type_sign_pn, tran_line_item_type_group_cd,mdb_number_ind,type_desc_as_product_ind)
values(558,'Sprout Replenish Refund','P','S','N','Y');
commit;

insert into pss.tran_line_item_type
(tran_line_item_type_id, tran_line_item_type_desc, tran_line_item_type_sign_pn, tran_line_item_type_group_cd,mdb_number_ind,type_desc_as_product_ind)
values(559,'Sprout Replenish Bonus Refund','P','S','N','Y');

insert into pss.tran_line_item_type
(tran_line_item_type_id, tran_line_item_type_desc, tran_line_item_type_sign_pn, tran_line_item_type_group_cd,mdb_number_ind,type_desc_as_product_ind)
values(560,'Sprout Replenishment Bonus','P','S','N','Y');
commit;


insert into pss.refund_type 
select 'S', 'Sprout Replenish/Bonus Cash'
from dual where not exists(select 1 from pss.refund_type where refund_type_cd='S');

insert into corp.refund_processed_flag
select 'S', 'Waiting for sprout replenish refund'
from dual where not exists(select 1 from corp.refund_processed_flag where processed_flag='S');
commit;

-- NOTE: the following will allow sprout tran to show up in refund editor in usalive
--update report.trans_type set refundable_ind='Y' where trans_type_id=32;
--commit;