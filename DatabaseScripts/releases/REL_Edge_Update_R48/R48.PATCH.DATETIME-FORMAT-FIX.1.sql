UPDATE REPORT.REPORT_PARAM SET PARAM_VALUE = 'select 
		c.customer_name "Customer",
		r.region_name "Region",
		e.eport_serial_num "Device",
		l.location_name "Location",
		t.terminal_nbr "Terminal",
		t.asset_nbr "Asset #",
		ta.address1 "Address",
		ta.city "City",
		ta.state "State",
		ta.zip "Zip",
		l.description "Location Details",
		lt.location_type_name "Location Type",
		m.make "Machine Make",
		m.model "Machine Model",
		pt.product_type_name "Product Type",
		tz.abbrev "Timezone",
		t.vends "Vends / Swipe",
		decode(t.authorization_mode, ''N'', ''Credit Cards Not Accepted'', ''L'', ''Local Authorization'', ''F'', ''Full Authorization'', ''\0'') "Authorization Mode",
		decode(t.dex_data, ''N'', ''None'', ''D'', ''Complete DEX File'', ''A'', ''Alarm Notification + Complete DEX File'', ''\0'') "DEX Data Capture",
		to_char(te.start_date, ''MM/DD/YYYY HH24:MI:SS'') "Terminal Start Date",
		to_char(t.create_date, ''MM/DD/YYYY HH24:MI:SS'') "Activation Submitted Date",
		bt.business_type_name "Business Type",
		decode(sr.sales_rep_id, null, null, sr.first_name || '' '' || sr.last_name) "Sales Rep", 
		sa.name "Affiliation",
		spt.name "Pricing Tier",
		sc.name "Sales Category"
	from 
		corp.customer c
		join report.terminal t on t.customer_id = c.customer_id 
			and t.status != ''D''
		join report.terminal_eport te on t.terminal_id = te.terminal_id
		join report.eport e_1 on e_1.eport_id = te.eport_id
		join report.eport e on e.eport_id = e_1.eport_id 
			and te.eport_id = e.eport_id 
			and sysdate between nvl(te.start_date, min_date) and nvl(te.end_date, max_date)
		join report.location l on t.location_id = l.location_id
		join report.time_zone tz on t.time_zone_id = tz.time_zone_id
		join report.vw_user_terminal vut on t.terminal_id = vut.terminal_id and vut.terminal_id != 0
		left outer join report.location_type lt on l.location_type_id = lt.location_type_id
		left outer join report.terminal_addr ta on l.address_id = ta.address_id
		left outer join report.product_type pt on t.product_type_id = pt.product_type_id
		left outer join report.machine m on t.machine_id = m.machine_id
		left outer join report.business_type bt on t.business_type_id = bt.business_type_id
		left outer join (report.terminal_region tr join report.region r on tr.region_id = r.region_id) on t.terminal_id = tr.terminal_id
		left outer join report.vw_current_device_sales_rep sr on e.eport_serial_num = sr.device_serial_cd
		left outer join report.sales_affiliate sa on c.sales_affiliate_id = sa.sales_affiliate_id
		left outer join report.sales_pricing_tier spt on c.sales_pricing_tier_id = spt.sales_pricing_tier_id
		left outer join report.sales_category sc on c.sales_category_id = sc.sales_category_id
	where 
		vut.user_id = ?
	order by 
		upper(c.customer_name) asc,
		upper(r.region_name) asc,
		upper(e.eport_serial_num) asc'
WHERE REPORT_ID = (SELECT REPORT_ID FROM REPORT.REPORTS WHERE REPORT_NAME = 'Device Sales Details') AND PARAM_NAME = 'query';

UPDATE REPORT.REPORT_PARAM SET PARAM_VALUE = 'select 
		c.customer_name "Customer",
		r.region_name "Region",
		e.eport_serial_num "Device",
		l.location_name "Location",
		t.terminal_nbr "Terminal",
		t.asset_nbr "Asset #",
		ta.address1 "Address",
		ta.city "City",
		ta.state "State",
		ta.zip "Zip",
		l.description "Location Details",
		lt.location_type_name "Location Type",
		m.make "Machine Make",
		m.model "Machine Model",
		pt.product_type_name "Product Type",
		tz.abbrev "Timezone",
		t.vends "Vends / Swipe",
		decode(t.authorization_mode, ''N'', ''Credit Cards Not Accepted'', ''L'', ''Local Authorization'', ''F'', ''Full Authorization'', ''\0'') "Authorization Mode",
		decode(t.dex_data, ''N'', ''None'', ''D'', ''Complete DEX File'', ''A'', ''Alarm Notification + Complete DEX File'', ''\0'') "DEX Data Capture",
		to_char(te.start_date, ''MM/DD/YYYY HH24:MI:SS'') "Terminal Start Date",
		to_char(t.create_date, ''MM/DD/YYYY HH24:MI:SS'') "Activation Submitted Date",
		to_char(te.end_date, ''MM/DD/YYYY HH24:MI:SS'') "Deactivation Date",
		bt.business_type_name "Business Type",
		decode(sr.sales_rep_id, null, null, sr.first_name || '' '' || sr.last_name) "Sales Rep", 
		sa.name "Affiliation",
		spt.name "Pricing Tier",
		sc.name "Sales Category"
	from 
		corp.customer c
		join report.terminal t on t.customer_id = c.customer_id 
			and t.status != ''D''
		join report.terminal_eport te on t.terminal_id = te.terminal_id
		join report.eport e_1 on e_1.eport_id = te.eport_id
		join report.eport e on e.eport_id = e_1.eport_id 
			and te.eport_id = e.eport_id 
			and te.end_date >= CAST(? AS DATE) AND te.end_date < CAST(? AS DATE) + 1
		join report.location l on t.location_id = l.location_id
		join report.time_zone tz on t.time_zone_id = tz.time_zone_id
		join report.vw_user_terminal vut on t.terminal_id = vut.terminal_id and vut.terminal_id != 0
		left outer join report.location_type lt on l.location_type_id = lt.location_type_id
		left outer join report.terminal_addr ta on l.address_id = ta.address_id
		left outer join report.product_type pt on t.product_type_id = pt.product_type_id
		left outer join report.machine m on t.machine_id = m.machine_id
		left outer join report.business_type bt on t.business_type_id = bt.business_type_id
		left outer join (report.terminal_region tr join report.region r on tr.region_id = r.region_id) on t.terminal_id = tr.terminal_id
		left outer join report.vw_current_device_sales_rep sr on e.eport_serial_num = sr.device_serial_cd
		left outer join report.sales_affiliate sa on c.sales_affiliate_id = sa.sales_affiliate_id
		left outer join report.sales_pricing_tier spt on c.sales_pricing_tier_id = spt.sales_pricing_tier_id
		left outer join report.sales_category sc on c.sales_category_id = sc.sales_category_id
	where 
		vut.user_id = ?
	order by 
		upper(c.customer_name) asc,
		upper(r.region_name) asc,
		upper(e.eport_serial_num) asc'
WHERE REPORT_ID = (SELECT REPORT_ID FROM REPORT.REPORTS WHERE REPORT_NAME = 'Device Deactivations') AND PARAM_NAME = 'query';

