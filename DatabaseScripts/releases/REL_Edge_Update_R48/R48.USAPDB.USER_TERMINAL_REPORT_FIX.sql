-- yhe.tmp_user_terminal_customer has the missed, this query finds all user reports affected
select ul.user_name, ul.user_type, r.report_name, from_tz( cast(ur.last_sent_utc_ts as timestamp), 'GMT' ) at time zone 'US/Eastern', ur.* 
from report.user_report ur join report.reports r on ur.report_id=r.report_id 
join report.user_login ul on ur.user_id=ul.user_id
where ur.user_id in (
select distinct user_id from yhe.TMP_USER_TERMINAL_CUSTOMER )
and ur.status='A' 
and from_tz( cast(ur.last_sent_utc_ts as timestamp), 'GMT' ) at time zone 'US/Eastern'>TO_DATE('2015/11/02 09:30:00', 'YYYY/MM/DD HH24:MI:SS')
order by ur.user_id

-- affected sent report
select * from report.report_sent where user_report_id in (select ur.user_report_id
from report.user_report ur join report.reports r on ur.report_id=r.report_id 
join report.user_login ul on ur.user_id=ul.user_id
where ur.user_id in (
select distinct user_id from yhe.TMP_USER_TERMINAL_CUSTOMER )
and ur.status='A' 
and from_tz( cast(ur.last_sent_utc_ts as timestamp), 'GMT' ) at time zone 'US/Eastern'>TO_DATE('2015/11/02 09:30:00', 'YYYY/MM/DD HH24:MI:SS')
) and created_ts>TO_DATE('2015/11/02 09:30:00', 'YYYY/MM/DD HH24:MI:SS')

select * from report.resend_reports

-- create resend for the affected reports
insert into report.resend_reports (user_report_id, batch_id, begin_date, end_date, resend_export_id, export_type)
select rs.user_report_id, rs.batch_id, rs.begin_date, rs.end_date, report.seq_resend_report_id.nextval, export_type from report.report_sent rs where user_report_id in (select ur.user_report_id
from report.user_report ur join report.reports r on ur.report_id=r.report_id 
join report.user_login ul on ur.user_id=ul.user_id
where ur.user_id in (
select distinct user_id from yhe.TMP_USER_TERMINAL_CUSTOMER )
and ur.status='A' 
and from_tz( cast(ur.last_sent_utc_ts as timestamp), 'GMT' ) at time zone 'US/Eastern'>TO_DATE('2015/11/02 09:30:00', 'YYYY/MM/DD HH24:MI:SS')
) and created_ts>TO_DATE('2015/11/02 09:30:00', 'YYYY/MM/DD HH24:MI:SS');
commit;