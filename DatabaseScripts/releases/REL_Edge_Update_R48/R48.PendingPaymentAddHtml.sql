WHENEVER SQLERROR EXIT FAILURE COMMIT;

ALTER SESSION SET CURRENT_SCHEMA = REPORT;    
DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := -1;
BEGIN
	SELECT MAX(REPORT_ID) INTO LN_REPORT_ID
	FROM REPORT.REPORTS WHERE REPORT_NAME = 'Pending Payment Summary (Html)';
	
	IF LN_REPORT_ID > 0 THEN
		RETURN;
	END IF;

	SELECT REPORT.REPORTS_SEQ.NEXTVAL INTO LN_REPORT_ID FROM DUAL;
	
	INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
	select LN_REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME||' (Html)', DESCRIPTION, USAGE, USER_ID
	from report.reports where report_name='Pending Payment Summary';
	
	insert into report.report_param values(LN_REPORT_ID, 'dateFormat','MM-dd-yyyy');
	insert into report.report_param values(LN_REPORT_ID, 'params.customerBankId','{x}');
	insert into report.report_param values(LN_REPORT_ID, 'reportId','5');
	
	update report.user_report set report_id=LN_REPORT_ID 
	where created_by='SYSTEM' and report_id=(select report_id from report.reports where report_name='Pending Payment Summary')
	and trunc(created_utc_ts,'DD') = TO_DATE('2015/11/02', 'yyyy/mm/dd');
	commit;
	
END;
/
update report.reports set report_name='Pending Payment Summary (Excel)' where report_name='Pending Payment Summary';
commit;