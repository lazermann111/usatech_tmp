DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE;
	LV_REPORT_NAME VARCHAR(50) := 'Device Sales Details';
BEGIN
	SELECT MAX(REPORT_ID) INTO LN_REPORT_ID
	FROM REPORT.REPORTS WHERE REPORT_NAME = LV_REPORT_NAME;
	
	IF LN_REPORT_ID > 0 THEN
		RETURN;
	END IF;

	SELECT REPORT.REPORTS_SEQ.NEXTVAL INTO LN_REPORT_ID FROM DUAL;	

	INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
	VALUES(LN_REPORT_ID, LV_REPORT_NAME, 6, 0, LV_REPORT_NAME, 'All active devices with sales rep, affiliation, and pricing tier', 'N', 0);

	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE)
	VALUES(LN_REPORT_ID, 'query', 'select 
		c.customer_name "Customer",
		r.region_name "Region",
		e.eport_serial_num "Device",
		l.location_name "Location",
		t.terminal_nbr "Terminal",
		t.asset_nbr "Asset #",
		ta.address1 "Address",
		ta.city "City",
		ta.state "State",
		ta.zip "Zip",
		l.description "Location Details",
		lt.location_type_name "Location Type",
		m.make "Machine Make",
		m.model "Machine Model",
		pt.product_type_name "Product Type",
		tz.abbrev "Timezone",
		t.vends "Vends / Swipe",
		decode(t.authorization_mode, ''N'', ''Credit Cards Not Accepted'', ''L'', ''Local Authorization'', ''F'', ''Full Authorization'', ''\0'') "Authorization Mode",
		decode(t.dex_data, ''N'', ''None'', ''D'', ''Complete DEX File'', ''A'', ''Alarm Notification + Complete DEX File'', ''\0'') "DEX Data Capture",
		to_char(te.start_date, ''MM/dd/yyyy HH:mm:ss'') "Terminal Start Date",
		to_char(t.create_date, ''MM/dd/yyyy HH:mm:ss'') "Activation Submitted Date",
		bt.business_type_name "Business Type",
		decode(sr.sales_rep_id, null, null, sr.first_name || '' '' || sr.last_name) "Sales Rep", 
		sa.name "Affiliation",
		spt.name "Pricing Tier",
		sc.name "Sales Category"
	from 
		corp.customer c
		join report.terminal t on t.customer_id = c.customer_id 
			and t.status != ''D''
		join report.terminal_eport te on t.terminal_id = te.terminal_id
		join report.eport e_1 on e_1.eport_id = te.eport_id
		join report.eport e on e.eport_id = e_1.eport_id 
			and te.eport_id = e.eport_id 
			and sysdate between nvl(te.start_date, min_date) and nvl(te.end_date, max_date)
		join report.location l on t.location_id = l.location_id
		join report.time_zone tz on t.time_zone_id = tz.time_zone_id
		join report.vw_user_terminal vut on t.terminal_id = vut.terminal_id and vut.terminal_id != 0
		left outer join report.location_type lt on l.location_type_id = lt.location_type_id
		left outer join report.terminal_addr ta on l.address_id = ta.address_id
		left outer join report.product_type pt on t.product_type_id = pt.product_type_id
		left outer join report.machine m on t.machine_id = m.machine_id
		left outer join report.business_type bt on t.business_type_id = bt.business_type_id
		left outer join (report.terminal_region tr join report.region r on tr.region_id = r.region_id) on t.terminal_id = tr.terminal_id
		left outer join report.vw_current_device_sales_rep sr on e.eport_serial_num = sr.device_serial_cd
		left outer join report.sales_affiliate sa on c.sales_affiliate_id = sa.sales_affiliate_id
		left outer join report.sales_pricing_tier spt on c.sales_pricing_tier_id = spt.sales_pricing_tier_id
		left outer join report.sales_category sc on c.sales_category_id = sc.sales_category_id
	where 
		vut.user_id = ?
	order by 
		upper(c.customer_name) asc,
		upper(r.region_name) asc,
		upper(e.eport_serial_num) asc');

	INSERT INTO report.report_param VALUES(LN_REPORT_ID, 'paramNames', 'userId');
	INSERT INTO report.report_param VALUES(LN_REPORT_ID, 'paramTypes', 'NUMERIC');
	INSERT INTO report.report_param VALUES(LN_REPORT_ID, 'params.userId', '{u}');
	INSERT INTO report.report_param VALUES(LN_REPORT_ID, 'isSQLFolio', 'true');

	INSERT INTO web_content.web_link(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER) VALUES(web_content.seq_web_link_id.nextval, LV_REPORT_NAME, './run_sql_folio_report_async.i?basicReportId=' || LN_REPORT_ID, LV_REPORT_NAME, 'Accounting', 'W', 0);
	INSERT INTO web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) SELECT web_link_id, 1 FROM web_content.web_link WHERE web_link_label = LV_REPORT_NAME;
	INSERT INTO web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) SELECT web_link_id, 14 FROM web_content.web_link WHERE web_link_label = LV_REPORT_NAME;
	
	COMMIT;
END;
/

DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE;
	LV_REPORT_NAME VARCHAR(50) := 'Device Deactivations';
BEGIN
	SELECT MAX(REPORT_ID) INTO LN_REPORT_ID
	FROM REPORT.REPORTS WHERE REPORT_NAME = LV_REPORT_NAME;
	
	IF LN_REPORT_ID > 0 THEN
		RETURN;
	END IF;

	SELECT REPORT.REPORTS_SEQ.NEXTVAL INTO LN_REPORT_ID FROM DUAL;	

	INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
	VALUES(LN_REPORT_ID, LV_REPORT_NAME, 6, 0, LV_REPORT_NAME, 'All devices deactivated withing the timeframe', 'N', 0);

	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE)
	VALUES(LN_REPORT_ID, 'query', 'select 
		c.customer_name "Customer",
		r.region_name "Region",
		e.eport_serial_num "Device",
		l.location_name "Location",
		t.terminal_nbr "Terminal",
		t.asset_nbr "Asset #",
		ta.address1 "Address",
		ta.city "City",
		ta.state "State",
		ta.zip "Zip",
		l.description "Location Details",
		lt.location_type_name "Location Type",
		m.make "Machine Make",
		m.model "Machine Model",
		pt.product_type_name "Product Type",
		tz.abbrev "Timezone",
		t.vends "Vends / Swipe",
		decode(t.authorization_mode, ''N'', ''Credit Cards Not Accepted'', ''L'', ''Local Authorization'', ''F'', ''Full Authorization'', ''\0'') "Authorization Mode",
		decode(t.dex_data, ''N'', ''None'', ''D'', ''Complete DEX File'', ''A'', ''Alarm Notification + Complete DEX File'', ''\0'') "DEX Data Capture",
		to_char(te.start_date, ''MM/dd/yyyy HH:mm:ss'') "Terminal Start Date",
		to_char(t.create_date, ''MM/dd/yyyy HH:mm:ss'') "Activation Submitted Date",
		to_char(te.end_date, ''MM/dd/yyyy HH:mm:ss'') "Deactivation Date",
		bt.business_type_name "Business Type",
		decode(sr.sales_rep_id, null, null, sr.first_name || '' '' || sr.last_name) "Sales Rep", 
		sa.name "Affiliation",
		spt.name "Pricing Tier",
		sc.name "Sales Category"
	from 
		corp.customer c
		join report.terminal t on t.customer_id = c.customer_id 
			and t.status != ''D''
		join report.terminal_eport te on t.terminal_id = te.terminal_id
		join report.eport e_1 on e_1.eport_id = te.eport_id
		join report.eport e on e.eport_id = e_1.eport_id 
			and te.eport_id = e.eport_id 
			and te.end_date >= CAST(? AS DATE) AND te.end_date < CAST(? AS DATE) + 1
		join report.location l on t.location_id = l.location_id
		join report.time_zone tz on t.time_zone_id = tz.time_zone_id
		join report.vw_user_terminal vut on t.terminal_id = vut.terminal_id and vut.terminal_id != 0
		left outer join report.location_type lt on l.location_type_id = lt.location_type_id
		left outer join report.terminal_addr ta on l.address_id = ta.address_id
		left outer join report.product_type pt on t.product_type_id = pt.product_type_id
		left outer join report.machine m on t.machine_id = m.machine_id
		left outer join report.business_type bt on t.business_type_id = bt.business_type_id
		left outer join (report.terminal_region tr join report.region r on tr.region_id = r.region_id) on t.terminal_id = tr.terminal_id
		left outer join report.vw_current_device_sales_rep sr on e.eport_serial_num = sr.device_serial_cd
		left outer join report.sales_affiliate sa on c.sales_affiliate_id = sa.sales_affiliate_id
		left outer join report.sales_pricing_tier spt on c.sales_pricing_tier_id = spt.sales_pricing_tier_id
		left outer join report.sales_category sc on c.sales_category_id = sc.sales_category_id
	where 
		vut.user_id = ?
	order by 
		upper(c.customer_name) asc,
		upper(r.region_name) asc,
		upper(e.eport_serial_num) asc');

	INSERT INTO report.report_param VALUES(LN_REPORT_ID, 'dateFormat', 'MM/dd/yyyy');
	INSERT INTO report.report_param VALUES(LN_REPORT_ID, 'params.StartDate', '{b}');
	INSERT INTO report.report_param VALUES(LN_REPORT_ID, 'params.EndDate', '{e}');
	INSERT INTO report.report_param VALUES(LN_REPORT_ID, 'params.userId', '{u}');
	INSERT INTO report.report_param VALUES(LN_REPORT_ID, 'paramNames', 'StartDate,EndDate,userId');
	INSERT INTO report.report_param VALUES(LN_REPORT_ID, 'paramTypes', 'TIMESTAMP,TIMESTAMP,NUMERIC');
	INSERT INTO report.report_param VALUES(LN_REPORT_ID, 'isSQLFolio', 'true');

	INSERT INTO web_content.web_link(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER) VALUES(web_content.seq_web_link_id.nextval, LV_REPORT_NAME, './select_date_range_frame_sqlfolio.i?basicReportId=' || LN_REPORT_ID, LV_REPORT_NAME, 'Accounting', 'W', 0);
	INSERT INTO web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) SELECT web_link_id, 1 FROM web_content.web_link WHERE web_link_label = LV_REPORT_NAME;
	INSERT INTO web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) SELECT web_link_id, 14 FROM web_content.web_link WHERE web_link_label = LV_REPORT_NAME;
	
	COMMIT;
END;
/
