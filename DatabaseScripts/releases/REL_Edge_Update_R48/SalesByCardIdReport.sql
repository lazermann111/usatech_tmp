DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := -1;
BEGIN
	SELECT MAX(REPORT_ID) INTO LN_REPORT_ID
	FROM REPORT.REPORTS WHERE REPORT_NAME = 'Sales by Card Id';
	
	IF LN_REPORT_ID > 0 THEN
		RETURN;
	END IF;

	SELECT REPORT.REPORTS_SEQ.NEXTVAL INTO LN_REPORT_ID FROM DUAL;
	
	INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
	VALUES( LN_REPORT_ID, 'Sales by Card Id - {b} to {e}', 6, 0, 'Sales by Card Id', 'Report with sales grouped by Card Id. Includes: Trans Type Name, Currency Code, Card Id, Card Number, Total Amount, # of Trans, Trans Type Id.', 'E', 0);

	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
	VALUES( LN_REPORT_ID, 'query', 'SELECT /*+ INDEX(X IDX_TRANS_STL_DT_EPORT_APPR_CD) */
TT.TRANS_TYPE_NAME "Trans Type Name",
C.CURRENCY_CODE "Currency Code",
CAB.GLOBAL_ACCOUNT_ID "Card Id",
X.CARD_NUMBER "Card Number",
SUM(X.TOTAL_AMOUNT) "Total Amount",
COUNT(1) "# of Trans",
X.TRANS_TYPE_ID "Trans Type Id"
FROM REPORT.TRANS X
JOIN CORP.CURRENCY C ON X.CURRENCY_ID = C.CURRENCY_ID
JOIN REPORT.VW_USER_TERMINAL UT ON X.TERMINAL_ID = UT.TERMINAL_ID
JOIN REPORT.TERMINAL_EPORT TE ON X.TERMINAL_ID = TE.TERMINAL_ID AND X.EPORT_ID = TE.EPORT_ID
JOIN REPORT.TRANS_TYPE TT ON X.TRANS_TYPE_ID = TT.TRANS_TYPE_ID
LEFT OUTER JOIN PSS.CONSUMER_ACCT_BASE CAB ON X.CONSUMER_ACCT_ID = CAB.CONSUMER_ACCT_ID
WHERE X.SETTLE_DATE >= CAST(? AS DATE) AND X.SETTLE_DATE < CAST(? AS DATE) + 1 AND UT.USER_ID = ?
AND X.CLOSE_DATE >= COALESCE(TE.START_DATE, MIN_DATE) AND X.CLOSE_DATE < COALESCE(TE.END_DATE, MAX_DATE)
GROUP BY TT.TRANS_TYPE_NAME, C.CURRENCY_CODE, CAB.GLOBAL_ACCOUNT_ID, X.CARD_NUMBER, X.TRANS_TYPE_ID
ORDER BY TT.TRANS_TYPE_NAME, C.CURRENCY_CODE, CAB.GLOBAL_ACCOUNT_ID');

	insert into report.report_param values(LN_REPORT_ID, 'paramNames','StartDate,EndDate,UserId');
	insert into report.report_param values(LN_REPORT_ID, 'paramTypes','TIMESTAMP,TIMESTAMP,NUMBER');
	insert into report.report_param values(LN_REPORT_ID, 'params.StartDate','{b}');
	insert into report.report_param values(LN_REPORT_ID, 'params.EndDate','{e}');
	insert into report.report_param values(LN_REPORT_ID, 'params.UserId','{u}');
	insert into report.report_param values(LN_REPORT_ID, 'isSQLFolio','true');
	insert into report.report_param values(LN_REPORT_ID, 'dateFormat','MM/dd/yyyy');
	INSERT INTO report.report_param VALUES(LN_REPORT_ID, 'header', 'true');
	INSERT INTO report.report_param VALUES(LN_REPORT_ID, 'outputType', '21');

	INSERT INTO web_content.web_link
	SELECT MAX(web_link_id) + 1, 'Sales by Card Id', './select_date_range_frame_sqlfolio.i?basicReportId=' || LN_REPORT_ID || '&' || 'outputType=22', 'Sales by Card Id', 'Daily Operations', 'W', 0
	FROM web_content.web_link;
	
	INSERT INTO web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) SELECT web_link_id, 1 FROM web_content.web_link WHERE web_link_label = 'Sales by Card Id';
	
	COMMIT;
END;
/