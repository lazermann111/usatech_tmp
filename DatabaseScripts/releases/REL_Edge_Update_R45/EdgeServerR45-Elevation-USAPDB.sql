WHENEVER SQLERROR EXIT FAILURE COMMIT;

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/CORP/VW_CUSTOMER_HIERARCHY.vws?rev=HEAD
CREATE OR REPLACE FORCE VIEW CORP.VW_CUSTOMER_HIERARCHY(
    ANCESTOR_CUSTOMER_ID
  , DESCENDENT_CUSTOMER_ID
  , DEPTH
  , ISROOT
  , ISLEAF
  , HIERARCHY_PATH
  , PATH_SEPARATOR
  , HIERARCHY_ACTIVE_YN_FLAG
)
AS
 SELECT CONNECT_BY_ROOT CUSTOMER_ID ANCESTOR_CUSTOMER_ID,
        CUSTOMER_ID DESCENDENT_CUSTOMER_ID,
        LEVEL - 1 DEPTH,
        DECODE(CONNECT_BY_ROOT PARENT_CUSTOMER_ID, NULL, 1, 0),
        CONNECT_BY_ISLEAF ISLEAF,
        REPLACE(SYS_CONNECT_BY_PATH(CUSTOMER_NAME, ''), '', CHR(0)),
        CHR(0),
        DECODE(REGEXP_INSTR(SYS_CONNECT_BY_PATH(STATUS, ''), '[^AP]'), 0, 'Y', 'N')
    FROM CORP.CUSTOMER
        CONNECT BY NOCYCLE PRIOR CUSTOMER_ID = PARENT_CUSTOMER_ID
;
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/releases/REL_Edge_Update_R45/R45.USAPDB.0.sql?rev=HEAD
GRANT SELECT ON CORP.VW_CUSTOMER_HIERARCHY TO REPORT WITH GRANT OPTION;

DROP SEQUENCE FRONT.SEQ_SMS_GATEWAY_ID;

GRANT UPDATE ON PSS.CONSUMER to USAT_PREPAID_APP_ROLE;

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/VW_USER_CONSUMER_ACCT.vws?rev=HEAD
CREATE OR REPLACE FORCE VIEW REPORT.VW_USER_CONSUMER_ACCT (USER_ID, CONSUMER_ACCT_ID, ALLOW_EDIT) AS 
  SELECT USER_ID, CONSUMER_ACCT_ID, MAX(ALLOW_EDIT)
FROM (
    SELECT U.USER_ID, CA.CONSUMER_ACCT_ID, CHECK_PRIV(U.USER_ID, 31) ALLOW_EDIT
      FROM USER_LOGIN U JOIN CORP.CUSTOMER C on (U.CUSTOMER_ID = 0 OR U.CUSTOMER_ID = C.CUSTOMER_ID)
      JOIN PSS.CONSUMER_ACCT CA on CA.CORP_CUSTOMER_ID=C.CUSTOMER_ID
    WHERE CHECK_PRIV(U.USER_ID, 31)='Y'
    UNION ALL
    SELECT U.USER_ID, CA.CONSUMER_ACCT_ID, CHECK_PRIV(U.USER_ID, 31) ALLOW_EDIT
      FROM USER_LOGIN U JOIN CORP.CUSTOMER C on (U.USER_TYPE!=8 AND (CHECK_PRIV(U.USER_ID, 8) ='Y' OR CHECK_PRIV(U.USER_ID, 16) ='Y'))
      JOIN PSS.CONSUMER_ACCT CA on CA.CORP_CUSTOMER_ID=C.CUSTOMER_ID
    WHERE CHECK_PRIV(U.USER_ID, 31)='Y'
    )
GROUP BY USER_ID, CONSUMER_ACCT_ID
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/VW_ADMIN_USER.vws?rev=HEAD
CREATE OR REPLACE VIEW REPORT.VW_ADMIN_USER
AS
SELECT ADMIN_ID, USER_ID, MAX(ALLOWED) ALLOWED
FROM (
SELECT USER_ID ADMIN_ID,
       USER_ID,
       'Y' ALLOWED
  FROM REPORT.USER_LOGIN
 WHERE STATUS = 'A'
UNION ALL
SELECT CONNECT_BY_ROOT ADMIN_ID,
        USER_ID,
        'Y'
    FROM REPORT.USER_LOGIN
   WHERE STATUS = 'A'
   AND CONNECT_BY_ROOT ADMIN_ID != 0
   CONNECT BY NOCYCLE PRIOR USER_ID = ADMIN_ID
UNION ALL
SELECT A.USER_ID,
        U.USER_ID,
        CASE WHEN AP.PRIV_ID = 10 THEN 'U' WHEN AP.PRIV_ID = 21 THEN 'R' ELSE 'Y' END
  FROM REPORT.USER_LOGIN A
  JOIN REPORT.USER_PRIVS AP ON A.USER_ID = AP.USER_ID
  JOIN REPORT.USER_LOGIN U ON AP.PRIV_ID = 5 OR (AP.PRIV_ID IN(8, 10, 21) AND U.USER_TYPE = 8 AND U.CUSTOMER_ID > 0)
  WHERE A.STATUS = 'A'
    AND A.USER_TYPE != 8
    AND U.STATUS = 'A'
UNION ALL
 SELECT A.USER_ID,
        U.USER_ID,
        CASE WHEN AP.PRIV_ID = 21 THEN 'R' ELSE 'Y' END
  FROM REPORT.USER_LOGIN A
  JOIN REPORT.USER_PRIVS AP ON A.USER_ID = AP.USER_ID
  JOIN REPORT.USER_LOGIN U ON AP.PRIV_ID IN(5, 8, 21) AND A.CUSTOMER_ID = U.CUSTOMER_ID AND A.USER_ID != U.USER_ID
  WHERE A.STATUS = 'A'
    AND A.USER_TYPE = 8 
    AND A.CUSTOMER_ID > 0
    AND U.STATUS = 'A'
UNION ALL
 SELECT A.USER_ID,
        U.USER_ID,
       'Y'
  FROM REPORT.USER_LOGIN A
  JOIN REPORT.USER_PRIVS AP ON A.USER_ID = AP.USER_ID AND AP.PRIV_ID IN(29)
  JOIN CORP.VW_CUSTOMER_HIERARCHY C ON A.CUSTOMER_ID = C.ANCESTOR_CUSTOMER_ID
  JOIN REPORT.USER_LOGIN U ON C.DESCENDENT_CUSTOMER_ID = U.CUSTOMER_ID AND A.USER_ID != U.USER_ID
  WHERE A.STATUS = 'A'
    AND A.USER_TYPE = 8 
    AND A.CUSTOMER_ID > 0
    AND U.STATUS = 'A'
    AND C.HIERARCHY_ACTIVE_YN_FLAG = 'Y')
GROUP BY ADMIN_ID, USER_ID
;
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/DEVICE/UPDATE_GPRS_FOR_DEVICE.pls?rev=HEAD
CREATE OR REPLACE PROCEDURE DEVICE.UPDATE_GPRS_FOR_DEVICE(
    pn_host_id HOST.HOST_ID%TYPE,
    pv_component_info GPRS_DEVICE.MODEM_INFO%TYPE,
    pn_local_time NUMBER)
IS
    lc_comm_method_cd HOST_TYPE.COMM_METHOD_CD%TYPE;
    ln_ccid GPRS_DEVICE.ICCID%TYPE;
    ln_device_id GPRS_DEVICE.DEVICE_ID%TYPE;
    ln_prev_device_id	GPRS_DEVICE.DEVICE_ID%TYPE;
    lv_device_type_name GPRS_DEVICE.DEVICE_TYPE_NAME%TYPE;
    lv_device_firmware_name GPRS_DEVICE.DEVICE_FIRMWARE_NAME%TYPE;
    ln_imei GPRS_DEVICE.IMEI%TYPE;
    lv_rssi GPRS_DEVICE.RSSI%TYPE;
    ld_rssi_ts GPRS_DEVICE.RSSI_TS%TYPE := CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_local_time) AS DATE);
BEGIN
    -- does device have a gprs modem?
    SELECT H.DEVICE_ID, CASE WHEN REGEXP_LIKE(H.HOST_SERIAL_CD, '[0-9]{20}') THEN DBADMIN.TO_NUMBER_OR_NULL(H.HOST_SERIAL_CD) END CCID, 
           HE.HOST_EQUIPMENT_MFGR DEVICE_TYPE_NAME, HS_FV.HOST_SETTING_VALUE DEVICE_FIRMWARE_NAME, 
           DBADMIN.TO_NUMBER_OR_NULL(H.HOST_LABEL_CD) IMEI, HS_CSQ.HOST_SETTING_VALUE RSSI, HT.COMM_METHOD_CD
      INTO ln_device_id, ln_ccid, lv_device_type_name, lv_device_firmware_name, ln_imei, lv_rssi, lc_comm_method_cd  
      FROM DEVICE.HOST H
      JOIN DEVICE.HOST_EQUIPMENT HE ON H.HOST_EQUIPMENT_ID = HE.HOST_EQUIPMENT_ID
      LEFT OUTER JOIN DEVICE.HOST_SETTING HS_FV ON H.HOST_ID = HS_FV.HOST_ID AND HS_FV.HOST_SETTING_PARAMETER = 'Firmware Version'
      LEFT OUTER JOIN DEVICE.HOST_SETTING HS_CSQ ON H.HOST_ID = HS_CSQ.HOST_ID AND HS_CSQ.HOST_SETTING_PARAMETER = 'CSQ'
      JOIN DEVICE.HOST_TYPE HT ON H.HOST_TYPE_ID = HT.HOST_TYPE_ID
     WHERE H.HOST_ID = pn_host_id;
    
    IF lc_comm_method_cd IS NULL THEN
        RETURN;
    END IF;
    
    IF lc_comm_method_cd IN('G', 'S', 'R', 'A', 'Y', 'O', '5', 'I') THEN
        IF ln_ccid IS NULL THEN
            RETURN;
        END IF;
        UPDATE DEVICE.GPRS_DEVICE
        SET DEVICE_ID = NULL,
          GPRS_DEVICE_STATE_ID = DECODE(GPRS_DEVICE_STATE_ID, 5, 4, GPRS_DEVICE_STATE_ID)
        WHERE DEVICE_ID = ln_device_id
          AND ICCID != ln_ccid;		
		
        BEGIN
            SELECT DEVICE_ID 
              INTO ln_prev_device_id 
              FROM DEVICE.GPRS_DEVICE 
             WHERE ICCID = ln_ccid;
        
            IF ln_prev_device_id IS NULL OR ln_prev_device_id != ln_device_id THEN
              UPDATE DEVICE.GPRS_DEVICE
              SET DEVICE_ID = ln_device_id,
              GPRS_DEVICE_STATE_ID = 5,
              ASSIGNED_BY = 'APP_LAYER',
              ASSIGNED_TS = SYSDATE,
              DEVICE_TYPE_NAME = lv_device_type_name,
              DEVICE_FIRMWARE_NAME = lv_device_firmware_name,
              IMEI = ln_imei,
              RSSI = lv_rssi,
              RSSI_TS = ld_rssi_ts,
              LAST_FILE_TRANSFER_ID = NULL,
              MODEM_INFO_RECEIVED_TS = SYSDATE,
              MODEM_INFO = pv_component_info
              WHERE ICCID = ln_ccid
                AND (RSSI_TS IS NULL OR RSSI_TS < ld_rssi_ts);
            ELSE
              UPDATE DEVICE.GPRS_DEVICE
              SET DEVICE_TYPE_NAME = lv_device_type_name,
              DEVICE_FIRMWARE_NAME = lv_device_firmware_name,
              IMEI = ln_imei,
              RSSI = lv_rssi,
              RSSI_TS = ld_rssi_ts,
              LAST_FILE_TRANSFER_ID = NULL,
              MODEM_INFO_RECEIVED_TS = SYSDATE,
              MODEM_INFO = pv_component_info
              WHERE ICCID = ln_ccid
                AND (RSSI_TS IS NULL OR RSSI_TS < ld_rssi_ts);
            END IF;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
              BEGIN
                  INSERT INTO DEVICE.GPRS_DEVICE(DEVICE_ID, DEVICE_TYPE_NAME, DEVICE_FIRMWARE_NAME, IMEI, RSSI, RSSI_TS, GPRS_DEVICE_STATE_ID, ASSIGNED_BY, ASSIGNED_TS, 
                    ICCID, MODEM_INFO_RECEIVED_TS, MODEM_INFO)
                  VALUES(ln_device_id, lv_device_type_name, lv_device_firmware_name, ln_imei, lv_rssi, ld_rssi_ts, 5, 'APP_LAYER', SYSDATE, 
                    ln_ccid, SYSDATE, pv_component_info);
                EXCEPTION
                  WHEN DUP_VAL_ON_INDEX THEN
                    NULL;
                END;
        END;
    ELSE
        UPDATE DEVICE.GPRS_DEVICE
           SET DEVICE_ID = NULL,
               DEVICE_TYPE_NAME = NULL,
               DEVICE_FIRMWARE_NAME = NULL, 
               IMEI = NULL,
               ASSIGNED_BY = NULL,
               ASSIGNED_TS = NULL,
               GPRS_DEVICE_STATE_ID = DECODE(GPRS_DEVICE_STATE_ID, 5, 4, GPRS_DEVICE_STATE_ID)	   
         WHERE DEVICE_ID = ln_device_id;
    END IF;
END;
/
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/releases/REL_Edge_Update_R45/R45.USAPDB.1.sql?rev=HEAD
SET DEFINE OFF;

INSERT INTO WEB_CONTENT.REQUIREMENT(REQUIREMENT_ID,REQUIREMENT_NAME,REQUIREMENT_DESCRIPTION,REQUIREMENT_CLASS,REQUIREMENT_PARAM_CLASS,REQUIREMENT_PARAM_VALUE) 
    SELECT 43,'REQ_MANAGE_PREPAID_CONSUMERS',NULL,'com.usatech.usalive.link.HasPrivilegeRequirement','java.lang.String','PRIV_MANAGE_PREPAID_CONSUMERS'
      FROM DUAL
      WHERE NOT EXISTS(SELECT 1 FROM WEB_CONTENT.REQUIREMENT WHERE REQUIREMENT_ID = 43); 
      
INSERT INTO WEB_CONTENT.WEB_LINK (WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER)
    SELECT WEB_CONTENT.SEQ_WEB_LINK_ID.NEXTVAL, 'Mass Card Update', './consumer_card_mass_manage.html','Consumer Card Mass Update','Administration','M', 177
      FROM DUAL
     WHERE NOT EXISTS(SELECT 1 FROM WEB_CONTENT.WEB_LINK WHERE WEB_LINK_LABEL = 'Mass Card Update' AND WEB_LINK_USAGE = 'M');
INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT (WEB_LINK_ID, REQUIREMENT_ID)
     SELECT WL.WEB_LINK_ID, R.COLUMN_VALUE
       FROM TABLE(NUMBER_TABLE(1, 43)) R, WEB_CONTENT.WEB_LINK WL
     WHERE WL.WEB_LINK_LABEL = 'Mass Card Update' 
       AND WL.WEB_LINK_USAGE = 'M'
       AND NOT EXISTS(SELECT 1 FROM WEB_CONTENT.WEB_LINK_REQUIREMENT WLR WHERE WLR.WEB_LINK_ID = WL.WEB_LINK_ID AND WLR.REQUIREMENT_ID = R.COLUMN_VALUE);
       
COMMIT;

ALTER TABLE PSS.CONSUMER_ACCT 
ADD NICK_NAME VARCHAR2(60);

CREATE UNIQUE INDEX REPORT.AK_CONSUMER_ACCT_NICK_NAME ON PSS.CONSUMER_ACCT(CASE WHEN NICK_NAME IS NOT NULL THEN TO_CHAR(CONSUMER_ID)||NICK_NAME END) TABLESPACE PSS_INDX ONLINE;

-- delete from report.reports where report_name='Single Transaction Data Export(CSV)';
ALTER SESSION SET CURRENT_SCHEMA = REPORT;    
DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := -1;
BEGIN
	SELECT MAX(REPORT_ID) INTO LN_REPORT_ID
	FROM REPORT.REPORTS WHERE REPORT_NAME = 'Single Transaction Data Export(CSV)';
	
	IF LN_REPORT_ID > 0 THEN
		RETURN;
	END IF;

	SELECT REPORT.REPORTS_SEQ.NEXTVAL INTO LN_REPORT_ID FROM DUAL;
	
	INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
	VALUES(LN_REPORT_ID, 'TRANS-{x}-{d}', 1, 10, 'Single Transaction Data Export(CSV)', 'Single Transaction data in csv format. Includes: Terminal Number, Transaction Reference Number, Transaction Type (Credit, Cash, Pass, etc), Card Number, Total Amount, Vended Columns, Number of Products Vended, Timestamp of the Transaction , Card Id', 'E', 0);
	
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
	VALUES( LN_REPORT_ID, 'query', 'SELECT  
	    UPPER(e.EPORT_SERIAL_NUM) "Device Serial Num",
	    ar.REF_NBR "Ref Nbr",
	    tt.TRANS_TYPE_CODE "Trans Type Code",
	    REPORT.MASK_CARD(UPPER(ar.CARD_NUMBER), ar.TRANS_TYPE_ID) "Masked Card Number",
	    ar.TOTAL_AMOUNT "Total Amount",
	    ar.VEND_COLUMN "Vend Column",
	    ar.quantity "Quantity",
	    TO_CHAR(ar.TRAN_DATE, ''MM/dd/yyyy'') "Tran Date",
	    TO_CHAR(ar.TRAN_DATE, ''HH:mm:ss'') "Tran Time",
	    cab.GLOBAL_ACCOUNT_ID "Card Id"
	FROM PSS.CONSUMER_ACCT_BASE cab
	    RIGHT OUTER JOIN (REPORT.ACTIVITY_REF ar
	    JOIN REPORT.EPORT e ON ar.EPORT_ID = e.EPORT_ID
	    JOIN REPORT.TRANS_TYPE tt ON ar.TRANS_TYPE_ID=tt.TRANS_TYPE_ID
	    ) ON ar.CONSUMER_ACCT_ID = cab.CONSUMER_ACCT_ID
	WHERE ar.TRAN_ID = {x}');

	commit;
END;
/

-- refund
GRANT UPDATE ON CORP.REFUND TO USALIVE_APP_ROLE;

update report.priv set INTERNAL_EXTERNAL_FLAG='B', customer_master_user_default='Y' where priv_id=12;
commit;

insert into web_content.web_link values(web_content.seq_web_link_id.nextval, 'Issue Refunds', './refund_search.i?folioId=81','Find Transaction and Issue Refunds','Administration','M', 178);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 1);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 4);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 15);
commit;

INSERT INTO REPORT.PRIV(PRIV_ID, DESCRIPTION, INTERNAL_EXTERNAL_FLAG, CUSTOMER_MASTER_USER_DEFAULT)
    SELECT 32, 'Admin Refund', 'B', 'Y'
      FROM DUAL
     WHERE NOT EXISTS(SELECT 1 FROM REPORT.PRIV WHERE PRIV_ID = 32);
     
COMMIT;

insert into web_content.requirement( requirement_id, requirement_name, requirement_class, requirement_param_class, requirement_param_value)
values(44, 'REQ_ADMIN_REFUND', 'com.usatech.usalive.link.HasPrivilegeRequirement', 'java.lang.String', 'PRIV_ADMIN_REFUND');

insert into web_content.web_link values(web_content.seq_web_link_id.nextval, 'Admin Refunds', './refund_admin.html','View, approve or cancel pending refunds','Administration','M', 179);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 1);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 44);
commit;

INSERT INTO REPORT.USER_PRIVS(USER_ID, PRIV_ID)
  	     SELECT U.USER_ID, 12
  	       FROM CORP.CUSTOMER C
  	       JOIN REPORT.USER_LOGIN U ON C.USER_ID = U.USER_ID
  	       LEFT OUTER JOIN REPORT.USER_PRIVS UP ON U.USER_ID = UP.USER_ID AND UP.PRIV_ID = 12
  	      WHERE C.STATUS = 'A'
  	        AND U.STATUS = 'A'
  	        AND UP.USER_ID IS NULL;
  	        
INSERT INTO REPORT.USER_PRIVS(USER_ID, PRIV_ID)
  	     SELECT U.USER_ID, 32
  	       FROM CORP.CUSTOMER C
  	       JOIN REPORT.USER_LOGIN U ON C.USER_ID = U.USER_ID
  	       LEFT OUTER JOIN REPORT.USER_PRIVS UP ON U.USER_ID = UP.USER_ID AND UP.PRIV_ID = 32
  	      WHERE C.STATUS = 'A'
  	        AND U.STATUS = 'A'
  	        AND UP.USER_ID IS NULL;

INSERT INTO REPORT.USER_PRIVS(USER_ID, PRIV_ID)
    SELECT U.USER_ID, 32
      FROM REPORT.USER_LOGIN U
      JOIN REPORT.USER_PRIVS UP0 on U.USER_ID = UP0.USER_ID
      LEFT OUTER JOIN REPORT.USER_PRIVS UP ON U.USER_ID = UP.USER_ID AND UP.PRIV_ID = 32
     WHERE U.STATUS = 'A'
       AND U.USER_TYPE != 8
       AND U.CUSTOMER_ID = 0
       AND UP0.PRIV_ID = 5
       AND UP.USER_ID IS NULL;
commit;

insert into web_content.web_link_requirement
select web_link_id, 14 from web_content.web_link where web_link_label='Issue Refunds' and web_link_group='Customer Service';
commit;

-- drop table CORP.REFUND_PROCESSED_FLAG;
CREATE TABLE CORP.REFUND_PROCESSED_FLAG
(
    PROCESSED_FLAG                          CHAR     (1)                    NOT NULL
  , DESCRIPTION                     VARCHAR2 (100)                  NOT NULL,
  CONSTRAINT PK_REFUND_PROCESSED_FLAG PRIMARY KEY(PROCESSED_FLAG)
) TABLESPACE CORP_DATA;

GRANT SELECT ON CORP.REFUND_PROCESSED_FLAG TO USALIVE_APP_ROLE;

insert into CORP.REFUND_PROCESSED_FLAG values('N', 'Not processed');
insert into CORP.REFUND_PROCESSED_FLAG values('Y', 'Processed');
insert into CORP.REFUND_PROCESSED_FLAG values('P', 'Pending');
insert into CORP.REFUND_PROCESSED_FLAG values('E', 'N/A');
insert into CORP.REFUND_PROCESSED_FLAG values('I', 'N/A');
insert into CORP.REFUND_PROCESSED_FLAG values('W', 'Waiting');
insert into CORP.REFUND_PROCESSED_FLAG values('C', 'Canceled');
insert into CORP.REFUND_PROCESSED_FLAG values('A', 'Approved');
commit;

ALTER TABLE CORP.REFUND
ADD CONSTRAINT FK_REFUND_PROCESSED_FLAG
  FOREIGN KEY (PROCESSED_FLAG)
  REFERENCES CORP.REFUND_PROCESSED_FLAG(PROCESSED_FLAG);

ALTER TABLE CORP.REFUND
DROP CONSTRAINT CK_REFUND_PROCESSED;

INSERT INTO REPORT.PREFERENCE(PREFERENCE_ID, PREFERENCE_LABEL, PREFERENCE_CATEGORY, PREFERENCE_DESC, PREFERENCE_EDITOR, PREFERENCE_LEVEL, PREFERENCE_DEFAULT)
	VALUES(106, 'Notify me when a refund is created', 'Refund', 'Whether to email the user when a refund is created', 'CHAR(1)', 1, 'Y');

COMMIT;

-- donation REPORT.VW_USER_CONSUMER_ACCT 
GRANT SELECT ON PSS.CONSUMER_ACCT TO REPORT WITH GRANT OPTION;
GRANT SELECT ON REPORT.VW_USER_CONSUMER_ACCT TO USALIVE_APP_ROLE;
GRANT SELECT ON REPORT.VW_USER_CONSUMER_ACCT TO USAT_RPT_GEN_ROLE;
GRANT SELECT ON REPORT.VW_USER_CONSUMER_ACCT TO USAT_DEV_READ_ONLY;

GRANT SELECT ON PSS.TRAN_LINE_ITEM to REPORT;
GRANT SELECT ON PSS.CONSUMER_SETTING to REPORT;
GRANT SELECT ON PSS.CONSUMER_SETTING_PARAM to REPORT;

ALTER TABLE REPORT.CAMPAIGN
ADD DONATION_PERCENT NUMBER(8,8);

INSERT INTO PSS.CONSUMER_SETTING_PARAM(CONSUMER_SETTING_PARAM_ID,DISPLAY_CONFIGURABLE_CD,DISPLAY_LABEL,DISPLAY_GROUP,EDITOR,CONVERTER,CONSUMER_COMMUNICATION_TYPE, DEFAULT_VALUE) 
    VALUES(9, 'P', 'Donate % of replenishment bonus to charity', 'Preferences', 'CHECKBOX:?Y=', null, 'DONATE_CHARITY','N');
COMMIT;

CREATE TABLE REPORT.PREPAID_DONATION(
	REPORT_TRAN_ID NUMBER(20,0) NOT NULL,
    CONSUMER_ACCT_ID NUMBER(20,0) NOT NULL,
    BASE_AMOUNT NUMBER(8,2) NOT NULL,
    DONATION_AMOUNT NUMBER(8,2) NOT NULL,
    CREATED_BY VARCHAR2(30) NOT NULL,
    CREATED_UTC_TS TIMESTAMP NOT NULL,
    LAST_UPDATED_BY VARCHAR2(30) NOT NULL,
    LAST_UPDATED_UTC_TS TIMESTAMP NOT NULL,
    CONSTRAINT PK_PD_REPORT_TRAN_ID  PRIMARY KEY(REPORT_TRAN_ID)
) TABLESPACE REPORT_DATA;

ALTER TABLE REPORT.PREPAID_DONATION
ADD CONSTRAINT FK_PD_REPORT_TRAN_ID
  FOREIGN KEY (REPORT_TRAN_ID)
  REFERENCES REPORT.TRANS(TRAN_ID);


CREATE OR REPLACE TRIGGER REPORT.TRBI_PREPAID_DONATION BEFORE 
INSERT ON REPORT.PREPAID_DONATION FOR EACH ROW 
BEGIN
    SELECT 
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER,
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER
    INTO 
        :NEW.CREATED_UTC_TS,
        :NEW.CREATED_BY,
        :NEW.LAST_UPDATED_UTC_TS,
        :NEW.LAST_UPDATED_BY
    FROM DUAL;
END;  
/

CREATE OR REPLACE TRIGGER REPORT.TRBU_PREPAID_DONATION BEFORE 
UPDATE ON REPORT.PREPAID_DONATION FOR EACH ROW 
BEGIN
    SELECT
        :OLD.CREATED_BY,
        :OLD.CREATED_UTC_TS,
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER
    INTO
        :NEW.CREATED_BY,
        :NEW.CREATED_UTC_TS,
        :NEW.LAST_UPDATED_UTC_TS,
        :NEW.LAST_UPDATED_BY
    FROM DUAL;
END; 
/

GRANT SELECT ON REPORT.PREPAID_DONATION TO USALIVE_APP_ROLE;
GRANT SELECT ON REPORT.PREPAID_DONATION TO USAT_RPT_GEN_ROLE;
GRANT SELECT ON REPORT.PREPAID_DONATION TO USAT_DEV_READ_ONLY;

ALTER SESSION SET CURRENT_SCHEMA = REPORT;    
DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := -1;
BEGIN
	SELECT MAX(REPORT_ID) INTO LN_REPORT_ID
	FROM REPORT.REPORTS WHERE REPORT_NAME = 'MORE Donation';
	
	IF LN_REPORT_ID > 0 THEN
		RETURN;
	END IF;

	SELECT REPORT.REPORTS_SEQ.NEXTVAL INTO LN_REPORT_ID FROM DUAL;
	
	INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
	VALUES( LN_REPORT_ID, 'MORE Donation - {b} to {e}', 6, 0, 'MORE Donation', 'Report that shows donation total for MORE consumer accounts grouped by region', 'N', 0);
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
	VALUES( LN_REPORT_ID, 'query', 'SELECT CUS.CUSTOMER_NAME "Operator Name", 
	CASE WHEN R.REGION_NAME IS NULL THEN ''NO REGION'' ELSE R.REGION_NAME END "Region Name",
	SUM(PD.BASE_AMOUNT) "Bonus Total",
	SUM(PD.DONATION_AMOUNT) "Donation Total"
	FROM REPORT.PREPAID_DONATION PD
	JOIN PSS.CONSUMER_ACCT CA ON PD.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
	JOIN PSS.CONSUMER C ON CA.CONSUMER_ID = C.CONSUMER_ID
	JOIN CORP.CUSTOMER CUS ON CA.CORP_CUSTOMER_ID = CUS.CUSTOMER_ID
	LEFT OUTER JOIN REPORT.REGION R ON C.REGION_ID = R.REGION_ID
    JOIN REPORT.VW_USER_CONSUMER_ACCT VUCA ON VUCA.CONSUMER_ACCT_ID=CA.CONSUMER_ACCT_ID
    JOIN REPORT.USER_LOGIN UL ON UL.USER_ID=VUCA.USER_ID 
	WHERE PD.CREATED_UTC_TS >= SYS_EXTRACT_UTC(CAST(? AS TIMESTAMP)) AND PD.CREATED_UTC_TS < SYS_EXTRACT_UTC(CAST(? + 1 AS TIMESTAMP))
	AND CA.CONSUMER_ACCT_TYPE_ID = 3
    AND UL.USER_ID = ?
	GROUP BY CUS.CUSTOMER_NAME, R.REGION_NAME');
	
	insert into report.report_param values(LN_REPORT_ID, 'paramNames','StartDate,EndDate,userId');
	insert into report.report_param values(LN_REPORT_ID, 'paramTypes','TIMESTAMP,TIMESTAMP,NUMBER');
	insert into report.report_param values(LN_REPORT_ID, 'params.StartDate','{b}');
	insert into report.report_param values(LN_REPORT_ID, 'params.EndDate','{e}');
	insert into report.report_param values(LN_REPORT_ID, 'params.userId','{u}');
	insert into report.report_param values(LN_REPORT_ID, 'isSQLFolio','true');
	insert into report.report_param values(LN_REPORT_ID, 'dateFormat','MM/dd/yyyy');
	
	insert into web_content.web_link values(web_content.seq_web_link_id.nextval, 'MORE Donation', './select_date_range_frame_sqlfolio.i?basicReportId='||LN_REPORT_ID,'MORE Donation','Daily Operations','W', 0);
	insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 1);
	commit;

END;
/
-- update Pepi replenish bonus campaign donation percent to 10
update report.campaign set donation_percent=0.1
where customer_id =(select customer_id from corp.customer where customer_name ='Pepi Food Services')
and campaign_type_id=2;
commit;

delete from web_content.web_link_requirement where web_link_id=(select web_link_id from web_content.web_link where web_link_label='MORE Accounts');
delete from web_content.web_link where web_link_id=(select web_link_id from web_content.web_link where web_link_label='MORE Accounts');
delete from report.report_param where report_id=(select report_id from report.reports where report_name='MORE Accounts');
delete from report.reports where report_id=(select report_id from report.reports where report_name='MORE Accounts');
commit;

ALTER SESSION SET CURRENT_SCHEMA = REPORT;    
DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := -1;
BEGIN

	SELECT REPORT.REPORTS_SEQ.NEXTVAL INTO LN_REPORT_ID FROM DUAL;
	
INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
VALUES( LN_REPORT_ID, 'MORE Accounts', 6, 0, 'MORE Accounts', 'Report that shows detailed more card accounts information', 'N', 0);
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( LN_REPORT_ID, 'query', 'SELECT CUS.CUSTOMER_NAME "Operator Name", R.REGION_NAME "Region Name", C.CONSUMER_FNAME "First Name", C.CONSUMER_LNAME "Last Name", CA.CONSUMER_ACCT_IDENTIFIER "Card Id", 
CA.CONSUMER_ACCT_BALANCE "Account Balance", CA.NICK_NAME "Card Nickname", CA.CONSUMER_ACCT_CD "Card Number", C.CONSUMER_EMAIL_ADDR1 "Email Address", C.CONSUMER_CELL_PHONE_NUM "Mobile Phone Number",
CA.CONSUMER_ACCT_ACTIVE_YN_FLAG "Account Enabled", DBADMIN.UTC_TO_LOCAL_DATE(CC.CREATED_UTC_TS) "Registration Time", CC.CREDENTIAL_ACTIVE_FLAG "Account Activated", 
CC.ACTIVATED_TS "Activation Time", CAS.CONSUMER_ACCT_SUB_TYPE_DESC "Account Sub Type", CA.CONSUMER_ACCT_PROMO_TOTAL "Promo Total",
CA.CONSUMER_ACCT_REPLENISH_TOTAL "Replenish Total", CA.REPLENISH_BONUS_TOTAL "Replenish Bonus Total", 
CA.LOYALTY_DISCOUNT_TOTAL "Loyalty Discount Total", CA.CASH_BACK_TOTAL "Cashback Total", 
RT.REPLENISH_TYPE_DESC "Replenish Type", CAR.REPLENISH_AMOUNT "Replenish Amount", CAR.REPLENISH_THRESHHOLD "Replenish Threshold", 
CAR.REPLENISH_CARD_MASKED "Replenish Card", CAR.LAST_REPLENISH_TRAN_TS "Last Replenish Time",
CA.CONSUMER_ACCT_REPLENISH_TOTAL+CA.CONSUMER_ACCT_PROMO_TOTAL-CA.CONSUMER_ACCT_BALANCE "Total Tran Amount",
CA.TRAN_COUNT_TOTAL "Total Tran Count"
FROM PSS.CONSUMER C
JOIN PSS.CONSUMER_ACCT CA ON C.CONSUMER_ID = CA.CONSUMER_ID
JOIN PSS.CONSUMER_ACCT_SUB_TYPE CAS ON CA.CONSUMER_ACCT_SUB_TYPE_ID = CAS.CONSUMER_ACCT_SUB_TYPE_ID
JOIN PSS.CONSUMER_CREDENTIAL CC ON C.CONSUMER_ID = CC.CONSUMER_ID
JOIN CORP.CUSTOMER CUS ON CA.CORP_CUSTOMER_ID = CUS.CUSTOMER_ID
LEFT OUTER JOIN REPORT.REGION R ON C.REGION_ID = R.REGION_ID
LEFT OUTER JOIN PSS.CONSUMER_ACCT_REPLENISH CAR ON CA.CONSUMER_ACCT_REPLENISH_ID = CAR.CONSUMER_ACCT_REPLENISH_ID
LEFT OUTER JOIN PSS.REPLENISH_TYPE RT ON CAR.REPLENISH_TYPE_ID = RT.REPLENISH_TYPE_ID
JOIN REPORT.VW_USER_CONSUMER_ACCT VUCA ON VUCA.CONSUMER_ACCT_ID=CA.CONSUMER_ACCT_ID
JOIN REPORT.USER_LOGIN UL ON UL.USER_ID=VUCA.USER_ID 
WHERE CA.CONSUMER_ACCT_TYPE_ID = 3 
AND UL.USER_ID = ?
ORDER BY CUS.CUSTOMER_NAME, CA.CONSUMER_ACCT_BALANCE, C.CONSUMER_FNAME, C.CONSUMER_LNAME');

insert into report.report_param values(LN_REPORT_ID, 'params.userId','{u}');
insert into report.report_param values(LN_REPORT_ID, 'paramNames','userId');
insert into report.report_param values(LN_REPORT_ID, 'paramTypes','NUMERIC');
insert into report.report_param values(LN_REPORT_ID, 'isSQLFolio','true');

insert into web_content.web_link values(web_content.seq_web_link_id.nextval, 'MORE Accounts', './run_sql_folio_report_async.i?basicReportId='||LN_REPORT_ID,'MORE Accounts','Daily Operations','W', 0);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 1);
commit;

END;
/

delete from web_content.web_link_requirement where web_link_id=(select web_link_id from web_content.web_link where web_link_label='Declined MORE Authorizations');
delete from web_content.web_link where web_link_id=(select web_link_id from web_content.web_link where web_link_label='Declined MORE Authorizations');
delete from report.report_param where report_id=(select report_id from report.reports where report_name='Declined MORE Authorizations');
delete from report.reports where report_id=(select report_id from report.reports where report_name='Declined MORE Authorizations');
commit;

DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := -1;
BEGIN

	SELECT REPORT.REPORTS_SEQ.NEXTVAL INTO LN_REPORT_ID FROM DUAL;
	
INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
VALUES( LN_REPORT_ID, 'Declined MORE Authorizations - {b} to {e}', 6, 0, 'Declined MORE Authorizations', 'Report that shows detailed more card declined authorization information', 'N', 0);
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( LN_REPORT_ID, 'query', 'SELECT CUS.CUSTOMER_NAME "Operator Name", R.REGION_NAME "Region Name", T.TRAN_START_TS "Transaction Time", A.AUTH_RESP_CD "Auth Response Code", 
A.AUTH_RESP_DESC "Auth Response Message", C.CONSUMER_FNAME "First Name", C.CONSUMER_LNAME "Last Name", CA.CONSUMER_ACCT_IDENTIFIER "Card Id",
CA.CONSUMER_ACCT_BALANCE "Account Balance", CA.CONSUMER_ACCT_CD "Card Number", C.CONSUMER_EMAIL_ADDR1 "Email Address", C.CONSUMER_CELL_PHONE_NUM "Mobile Phone Number",
CA.CONSUMER_ACCT_ACTIVE_YN_FLAG "Account Enabled", DBADMIN.UTC_TO_LOCAL_DATE(CC.CREATED_UTC_TS) "Registration Time", CC.CREDENTIAL_ACTIVE_FLAG "Account Activated", 
CC.ACTIVATED_TS "Activation Time", CAS.CONSUMER_ACCT_SUB_TYPE_DESC "Account Sub Type"
FROM PSS.TRAN T
JOIN PSS.AUTH A ON T.TRAN_ID = A.TRAN_ID AND A.AUTH_TYPE_CD = ''N''
JOIN PSS.CONSUMER_ACCT CA ON T.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
JOIN PSS.CONSUMER C ON CA.CONSUMER_ID = C.CONSUMER_ID
JOIN PSS.CONSUMER_ACCT_SUB_TYPE CAS ON CA.CONSUMER_ACCT_SUB_TYPE_ID = CAS.CONSUMER_ACCT_SUB_TYPE_ID
JOIN PSS.CONSUMER_CREDENTIAL CC ON C.CONSUMER_ID = CC.CONSUMER_ID
JOIN CORP.CUSTOMER CUS ON CA.CORP_CUSTOMER_ID = CUS.CUSTOMER_ID
LEFT OUTER JOIN REPORT.REGION R ON C.REGION_ID = R.REGION_ID
JOIN REPORT.VW_USER_CONSUMER_ACCT VUCA ON VUCA.CONSUMER_ACCT_ID=CA.CONSUMER_ACCT_ID
JOIN REPORT.USER_LOGIN UL ON UL.USER_ID=VUCA.USER_ID 
WHERE T.TRAN_START_TS >= CAST(? AS DATE) AND T.TRAN_START_TS < CAST(? AS DATE) + 1 AND T.TRAN_STATE_CD IN (''5'', ''7'') AND CA.CONSUMER_ACCT_TYPE_ID = 3
AND UL.USER_ID = ?
ORDER BY CUS.CUSTOMER_NAME, R.REGION_NAME, T.TRAN_START_TS DESC, C.CONSUMER_FNAME, C.CONSUMER_LNAME');

insert into report.report_param values(LN_REPORT_ID, 'paramNames','StartDate,EndDate,userId');
insert into report.report_param values(LN_REPORT_ID, 'paramTypes','TIMESTAMP,TIMESTAMP,NUMBER');
insert into report.report_param values(LN_REPORT_ID, 'params.StartDate','{b}');
insert into report.report_param values(LN_REPORT_ID, 'params.EndDate','{e}');
insert into report.report_param values(LN_REPORT_ID, 'params.userId','{u}');
insert into report.report_param values(LN_REPORT_ID, 'isSQLFolio','true');
insert into report.report_param values(LN_REPORT_ID, 'dateFormat', 'MM/dd/yyyy');

insert into web_content.web_link values(web_content.seq_web_link_id.nextval, 'Declined MORE Authorizations', './select_date_range_frame_sqlfolio.i?basicReportId='||LN_REPORT_ID,'MORE Accounts','Daily Operations','W', 0);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 1);
commit;

END;
/
 
UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET CUSTOMER_EDITABLE = 'Y' WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD IN ('1501', '1502', '1505');
COMMIT;

UPDATE PSS.AUTHORITY_PAYMENT_MASK
   SET AUTHORITY_PAYMENT_MASK_BREF = '1:1|2:3|3:4|5:12'
 WHERE AUTHORITY_PAYMENT_MASK_BREF = '1:1|2:3|3:4|4:5|5:12';
  
COMMIT;

DECLARE
	ln_report_id REPORT.REPORTS.REPORT_ID%TYPE := -1;
BEGIN
	SELECT MAX(REPORT_ID) 
    INTO ln_report_id
	  FROM REPORT.REPORTS 
   WHERE REPORT_NAME = 'Inactive MORE Accounts';
	
	IF ln_report_id IS NULL THEN
			SELECT REPORT.REPORTS_SEQ.NEXTVAL 
        INTO ln_report_id 
        FROM DUAL;
      INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
          VALUES( ln_report_id, 'Inactive MORE Accounts', 6, 0, 'Inactive MORE Accounts', 'Report that shows inactive MORE card information', 'N', 0);
  ELSE
      DELETE FROM report.report_param WHERE REPORT_ID = ln_report_id;
	END IF;

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( ln_report_id, 'query', 'SELECT * FROM (
SELECT CUS.CUSTOMER_NAME "Operator Name", R.REGION_NAME "Region Name", C.CONSUMER_FNAME "First Name", C.CONSUMER_LNAME "Last Name", CA.CONSUMER_ACCT_IDENTIFIER "Card Id", 
CA.CONSUMER_ACCT_BALANCE "Account Balance", CA.NICK_NAME "Card Nickname", CA.CONSUMER_ACCT_CD "Card Number", C.CONSUMER_EMAIL_ADDR1 "Email Address", C.CONSUMER_CELL_PHONE_NUM "Mobile Phone Number",
CA.CONSUMER_ACCT_ACTIVE_YN_FLAG "Account Enabled", DBADMIN.UTC_TO_LOCAL_DATE(CC.CREATED_UTC_TS) "Registration Time", CC.CREDENTIAL_ACTIVE_FLAG "Account Activated", 
CC.ACTIVATED_TS "Activation Time", CAS.CONSUMER_ACCT_SUB_TYPE_DESC "Account Sub Type", CA.CONSUMER_ACCT_PROMO_TOTAL "Promo Total",
CA.CONSUMER_ACCT_REPLENISH_TOTAL "Replenish Total", CA.REPLENISH_BONUS_TOTAL "Replenish Bonus Total", 
CA.LOYALTY_DISCOUNT_TOTAL "Loyalty Discount Total", CA.CASH_BACK_TOTAL "Cashback Total", 
RT.REPLENISH_TYPE_DESC "Replenish Type", CAR.REPLENISH_AMOUNT "Replenish Amount", CAR.REPLENISH_THRESHHOLD "Replenish Threshold", 
CAR.REPLENISH_CARD_MASKED "Replenish Card", CAR.LAST_REPLENISH_TRAN_TS "Last Replenish Time",
CA.CONSUMER_ACCT_REPLENISH_TOTAL+CA.CONSUMER_ACCT_PROMO_TOTAL-CA.CONSUMER_ACCT_BALANCE "Total Tran Amount",
CA.TRAN_COUNT_TOTAL "Total Tran Count",
(SELECT DBADMIN.UTC_TO_LOCAL_DATE(MAX(CAD.LAST_USED_UTC_TS)) FROM PSS.CONSUMER_ACCT_DEVICE CAD WHERE CAD.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID) "Last Activity Time"
FROM PSS.CONSUMER C
JOIN PSS.CONSUMER_ACCT CA ON C.CONSUMER_ID = CA.CONSUMER_ID
JOIN PSS.CONSUMER_ACCT_SUB_TYPE CAS ON CA.CONSUMER_ACCT_SUB_TYPE_ID = CAS.CONSUMER_ACCT_SUB_TYPE_ID
JOIN PSS.CONSUMER_CREDENTIAL CC ON C.CONSUMER_ID = CC.CONSUMER_ID
JOIN CORP.CUSTOMER CUS ON CA.CORP_CUSTOMER_ID = CUS.CUSTOMER_ID
LEFT OUTER JOIN REPORT.REGION R ON C.REGION_ID = R.REGION_ID
LEFT OUTER JOIN PSS.CONSUMER_ACCT_REPLENISH CAR ON CA.CONSUMER_ACCT_REPLENISH_ID = CAR.CONSUMER_ACCT_REPLENISH_ID
LEFT OUTER JOIN PSS.REPLENISH_TYPE RT ON CAR.REPLENISH_TYPE_ID = RT.REPLENISH_TYPE_ID
JOIN REPORT.VW_USER_CONSUMER_ACCT VUCA ON VUCA.CONSUMER_ACCT_ID=CA.CONSUMER_ACCT_ID
JOIN REPORT.USER_LOGIN UL ON UL.USER_ID=VUCA.USER_ID 
WHERE CA.CONSUMER_ACCT_TYPE_ID = 3
AND UL.USER_ID = ? 
) WHERE ("Last Activity Time" < CAST(? AS DATE) OR ("Last Activity Time" IS NULL AND ? = ''Y''))
ORDER BY "Operator Name", "Last Activity Time", "Account Balance", "First Name", "Last Name"');

INSERT INTO REPORT.REPORT_PARAM VALUES(ln_report_id, 'paramNames','userId,lastActivityDate,includeNeverActive');
INSERT INTO REPORT.REPORT_PARAM VALUES(ln_report_id, 'paramTypes','NUMERIC,TIMESTAMP,CHAR');
INSERT INTO REPORT.REPORT_PARAM VALUES(ln_report_id, 'isSQLFolio','true');
INSERT INTO REPORT.REPORT_PARAM VALUES(ln_report_id, 'params.userId','{u}');
INSERT INTO REPORT.REPORT_PARAM VALUES(ln_report_id, 'params.lastActivityDate','{b}');
INSERT INTO REPORT.REPORT_PARAM VALUES(ln_report_id, 'params.includeNeverActive','N');

COMMIT;
END;
/

DELETE FROM REPORT.REPORT_PARAM 
WHERE REPORT_ID IN(SELECT REPORT_ID FROM REPORT.REPORTS WHERE REPORT_NAME IN('MORE Accounts', 'Sprout Devices'))
AND PARAM_NAME = 'params.CustomerId';


UPDATE REPORT.REPORT_PARAM
   SET PARAM_VALUE = 
'select c.customer_name "Customer", d.device_serial_cd "Device", pp.pos_pta_device_serial_cd "Sprout POS ID", t.asset_nbr "Asset #",  
	l.location_name "Location" 
	from device.device d 
	join pss.pos p on d.device_id = p.device_id 
	join pss.pos_pta pp on p.pos_id = pp.pos_id and pp.pos_pta_activation_ts <= sysdate and (pp.pos_pta_deactivation_ts is null or pp.pos_pta_deactivation_ts > sysdate) 
	join pss.payment_subtype ps on pp.payment_subtype_id = ps.payment_subtype_id and ps.payment_subtype_class = ''Sprout'' 
	left outer join report.eport e on d.device_serial_cd = e.eport_serial_num 
	left outer join report.vw_terminal_eport te on e.eport_id = te.eport_id 
	left outer join report.terminal t on te.terminal_id = t.terminal_id 
	left outer join corp.customer c on t.customer_id = c.customer_id 
	left outer join report.location l on t.location_id = l.location_id
  JOIN REPORT.VW_USER_TERMINAL UT ON T.TERMINAL_ID = UT.TERMINAL_ID
	where d.device_active_yn_flag = ''Y'' and UT.USER_ID = ? 
	order by c.customer_name, e.eport_serial_num'
 WHERE REPORT_ID IN(SELECT REPORT_ID FROM REPORT.REPORTS WHERE REPORT_NAME = 'Sprout Devices')
   AND PARAM_NAME = 'query';

UPDATE REPORT.REPORT_PARAM
   SET PARAM_VALUE = 'userId'
 WHERE REPORT_ID IN(SELECT REPORT_ID FROM REPORT.REPORTS WHERE REPORT_NAME = 'Sprout Devices')
   AND PARAM_NAME = 'paramNames';   

UPDATE REPORT.REPORT_PARAM
   SET PARAM_VALUE = 'NUMERIC'
 WHERE REPORT_ID IN(SELECT REPORT_ID FROM REPORT.REPORTS WHERE REPORT_NAME = 'Sprout Devices')
   AND PARAM_NAME = 'paramTypes';  

INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE)
  SELECT REPORT_ID, 'params.userId','{u}'
    FROM REPORT.REPORTS R
   WHERE REPORT_NAME = 'Sprout Devices'
     AND NOT EXISTS(SELECT 1 FROM REPORT.REPORT_PARAM RP WHERE RP.REPORT_ID = R.REPORT_ID AND RP.PARAM_NAME = 'params.userId');

COMMIT;

INSERT INTO WEB_CONTENT.WEB_LINK(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER)
    SELECT WEB_CONTENT.SEQ_WEB_LINK_ID.NEXTVAL, 'Inactive MORE Accounts', './selection-date.html?basicReportId='||R.REPORT_ID||'&dateName=Last+Activity+Date','Inactive MORE Accounts','Daily Operations','W', 0
      FROM REPORT.REPORTS R
     WHERE R.REPORT_NAME = 'Inactive MORE Accounts'
       AND NOT EXISTS(SELECT 1 FROM WEB_CONTENT.WEB_LINK WHERE WEB_LINK_LABEL = 'Inactive MORE Accounts' AND WEB_LINK_USAGE = 'W');
INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT 
    SELECT WEB_LINK_ID, 1
      FROM WEB_CONTENT.WEB_LINK WL
     WHERE WEB_LINK_LABEL = 'Inactive MORE Accounts' AND WEB_LINK_USAGE = 'W'
       AND NOT EXISTS(SELECT 1 FROM WEB_CONTENT.WEB_LINK_REQUIREMENT  WHERE WEB_LINK_ID = WL.WEB_LINK_ID AND REQUIREMENT_ID = 1);
   
COMMIT;

UPDATE REPORT.REPORT_PARAM
   SET PARAM_VALUE = 'SELECT
	TO_CHAR(X.SETTLE_DATE, ''MM/DD/YYYY HH24:MI:SS'') "Settle Date", X.TRAN_ID "Tran Id", X.TOTAL_AMOUNT "Tran Amount", P.PRICE * P.AMOUNT "Reward Amount", CUR.CURRENCY_CODE "Currency",
	P.VEND_COLUMN "Tran Line Item", TT.TRANS_TYPE_NAME "Tran Type", CT.CARD_NAME "Card Type", X.CARD_NUMBER "Card Number", E.EPORT_SERIAL_NUM "Device Serial #", T.TERMINAL_NBR "Terminal #", C.CUSTOMER_NAME "Customer", L.LOCATION_NAME "Location"
	FROM REPORT.TRANS X
	JOIN REPORT.TRANS_TYPE TT ON X.TRANS_TYPE_ID = TT.TRANS_TYPE_ID
	JOIN REPORT.EPORT E ON X.EPORT_ID = E.EPORT_ID
	JOIN REPORT.PURCHASE P ON X.TRAN_ID = P.TRAN_ID
	LEFT OUTER JOIN REPORT.TERMINAL T ON X.TERMINAL_ID = T.TERMINAL_ID
	LEFT OUTER JOIN CORP.CUSTOMER C ON T.CUSTOMER_ID = C.CUSTOMER_ID
	LEFT OUTER JOIN REPORT.LOCATION L ON T.LOCATION_ID = L.LOCATION_ID
	LEFT OUTER JOIN CORP.CURRENCY CUR ON X.CURRENCY_ID = CUR.CURRENCY_ID
	LEFT OUTER JOIN REPORT.CARDTYPE_AUTHORITY CA ON X.CARDTYPE_AUTHORITY_ID = CA.CARDTYPE_AUTHORITY_ID
	LEFT OUTER JOIN REPORT.CARD_TYPE CT ON CA.CARDTYPE_ID = CT.CARD_TYPE_ID
  JOIN REPORT.VW_USER_TERMINAL UT ON X.TERMINAL_ID = UT.TERMINAL_ID
 WHERE X.SETTLE_DATE >= CAST(? AS DATE) AND X.SETTLE_DATE < CAST(? AS DATE) + 1 AND X.SETTLE_STATE_ID = 3 AND P.TRAN_LINE_ITEM_TYPE_ID = 209
   AND UT.USER_ID = ?
	ORDER BY X.SETTLE_DATE, X.TRAN_ID'
 WHERE REPORT_ID IN(SELECT REPORT_ID FROM REPORT.REPORTS WHERE REPORT_NAME = 'Softcard Discount Transactions')
   AND PARAM_NAME = 'query';

UPDATE REPORT.REPORT_PARAM
   SET PARAM_VALUE = 'beginDate,endDate,userId'
 WHERE REPORT_ID IN(SELECT REPORT_ID FROM REPORT.REPORTS WHERE REPORT_NAME = 'Softcard Discount Transactions')
   AND PARAM_NAME = 'paramNames';
 
UPDATE REPORT.REPORT_PARAM
   SET PARAM_VALUE = 'TIMESTAMP,TIMESTAMP,NUMERIC'
 WHERE REPORT_ID IN(SELECT REPORT_ID FROM REPORT.REPORTS WHERE REPORT_NAME = 'Softcard Discount Transactions')
   AND PARAM_NAME = 'paramTypes';

UPDATE REPORT.REPORT_PARAM
   SET PARAM_NAME = 'params.beginDate'
 WHERE REPORT_ID IN(SELECT REPORT_ID FROM REPORT.REPORTS WHERE REPORT_NAME = 'Softcard Discount Transactions')
   AND PARAM_NAME = 'params.StartDate';

UPDATE REPORT.REPORT_PARAM
   SET PARAM_NAME = 'params.endDate'
 WHERE REPORT_ID IN(SELECT REPORT_ID FROM REPORT.REPORTS WHERE REPORT_NAME = 'Softcard Discount Transactions')
   AND PARAM_NAME = 'params.EndDate';

INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE)
  SELECT REPORT_ID, 'params.userId','{u}'
    FROM REPORT.REPORTS R
   WHERE REPORT_NAME = 'Softcard Discount Transactions'
     AND NOT EXISTS(SELECT 1 FROM REPORT.REPORT_PARAM RP WHERE RP.REPORT_ID = R.REPORT_ID AND RP.PARAM_NAME = 'params.userId');

INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE)
  SELECT REPORT_ID, 'dateFormat', 'MM/dd/yyyy'
    FROM REPORT.REPORTS R
   WHERE REPORT_NAME = 'Softcard Discount Transactions'
     AND NOT EXISTS(SELECT 1 FROM REPORT.REPORT_PARAM RP WHERE RP.REPORT_ID = R.REPORT_ID AND RP.PARAM_NAME = 'dateFormat');

COMMIT;  

UPDATE REPORT.REPORT_PARAM
   SET PARAM_VALUE = 'SELECT DISTINCT
	C.CUSTOMER_NAME "Customer", L.LOCATION_NAME "Location", E.EPORT_SERIAL_NUM "Device", A.AUTHORITY_NAME "Authority", M.MERCHANT_NAME "Merchant Name",
	M.MERCHANT_CD "Merchant ID", TX.TERMINAL_CD "Terminal ID", COALESCE(T.DOING_BUSINESS_AS, C.DOING_BUSINESS_AS) "Doing Business As",
	TO_CHAR(D.LAST_ACTIVITY_TS, ''MM/DD/YYYY HH24:MI:SS'') "Last Activity"
	FROM REPORT.VW_TERMINAL_EPORT TE
	JOIN REPORT.EPORT E ON TE.EPORT_ID = E.EPORT_ID
	JOIN REPORT.TERMINAL T ON TE.TERMINAL_ID = T.TERMINAL_ID
	JOIN CORP.CUSTOMER C ON T.CUSTOMER_ID = C.CUSTOMER_ID
	JOIN REPORT.LOCATION L ON T.LOCATION_ID = L.LOCATION_ID
	JOIN DEVICE.DEVICE_LAST_ACTIVE DLA ON E.EPORT_SERIAL_NUM = DLA.DEVICE_SERIAL_CD
	JOIN DEVICE.DEVICE D ON DLA.DEVICE_ID = D.DEVICE_ID
	JOIN PSS.POS P ON D.DEVICE_ID = P.DEVICE_ID
	JOIN PSS.POS_PTA PP ON P.POS_ID = PP.POS_ID
	JOIN PSS.PAYMENT_SUBTYPE PS ON PP.PAYMENT_SUBTYPE_ID = PS.PAYMENT_SUBTYPE_ID
	JOIN PSS.TERMINAL TX ON PP.PAYMENT_SUBTYPE_KEY_ID = TX.TERMINAL_ID
	JOIN PSS.MERCHANT M ON TX.MERCHANT_ID = M.MERCHANT_ID
	JOIN AUTHORITY.AUTHORITY A ON M.AUTHORITY_ID = A.AUTHORITY_ID
  JOIN REPORT.VW_USER_TERMINAL UT ON T.TERMINAL_ID = UT.TERMINAL_ID
	WHERE PS.PAYMENT_SUBTYPE_TABLE_NAME = ''TERMINAL'' AND PS.PAYMENT_SUBTYPE_CLASS != ''Authority::NOP''
	AND PP.POS_PTA_ACTIVATION_TS < SYSDATE AND (PP.POS_PTA_DEACTIVATION_TS IS NULL OR PP.POS_PTA_DEACTIVATION_TS > SYSDATE)
  AND UT.USER_ID = ?
	ORDER BY C.CUSTOMER_NAME, L.LOCATION_NAME, E.EPORT_SERIAL_NUM'
 WHERE REPORT_ID IN(SELECT REPORT_ID FROM REPORT.REPORTS WHERE REPORT_NAME = 'Device MID Configuration')
   AND PARAM_NAME = 'query';

UPDATE REPORT.REPORT_PARAM
   SET PARAM_VALUE = 'userId'
 WHERE REPORT_ID IN(SELECT REPORT_ID FROM REPORT.REPORTS WHERE REPORT_NAME = 'Device MID Configuration')
   AND PARAM_NAME = 'paramNames';

INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE)
  SELECT REPORT_ID, 'paramTypes', 'NUMERIC'
    FROM REPORT.REPORTS R
   WHERE REPORT_NAME = 'Device MID Configuration'
     AND NOT EXISTS(SELECT 1 FROM REPORT.REPORT_PARAM RP WHERE RP.REPORT_ID = R.REPORT_ID AND RP.PARAM_NAME = 'paramTypes');

INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE)
  SELECT REPORT_ID, 'params.userId','{u}'
    FROM REPORT.REPORTS R
   WHERE REPORT_NAME = 'Device MID Configuration'
     AND NOT EXISTS(SELECT 1 FROM REPORT.REPORT_PARAM RP WHERE RP.REPORT_ID = R.REPORT_ID AND RP.PARAM_NAME = 'params.userId');

COMMIT;  

GRANT SELECT ON FRONT.TIME_ZONE TO USAT_DMS_ROLE;

INSERT INTO FOLIO_CONF.FIELD (FIELD_ID,FIELD_LABEL,FIELD_DESC,DISPLAY_EXPRESSION,SORT_EXPRESSION,DISPLAY_FORMAT,DISPLAY_SQL_TYPE,SORT_SQL_TYPE,IMPORTANCE,FIELD_CATEGORY_ID,CREATED_BY_USER_ID,LAST_UPDATED_BY_USER_ID,ACTIVE_FLAG) 
    VALUES (2814,'DEX File Name',NULL,'G4OP.DEX_FILE.FILE_NAME','UPPER(G4OP.DEX_FILE.FILE_NAME)',NULL,'VARCHAR','VARCHAR',50,518,7,7,'Y');

INSERT INTO FOLIO_CONF.FIELD_PRIV(FIELD_ID, FILTER_GROUP_ID, USER_GROUP_ID)
    SELECT 2814, NULL, USER_GROUP_ID
      FROM FOLIO_CONF.USER_GROUP UG
     WHERE USER_GROUP_ID NOT IN(SELECT USER_GROUP_ID FROM FOLIO_CONF.FIELD_PRIV WHERE FIELD_ID = 2814)
       AND USER_GROUP_ID IN(1,2,3);

INSERT INTO FOLIO_CONF.FIELD_PRIV(FIELD_ID, FILTER_GROUP_ID, USER_GROUP_ID)
    SELECT 2814, 1, USER_GROUP_ID
      FROM FOLIO_CONF.USER_GROUP UG
     WHERE USER_GROUP_ID NOT IN(SELECT USER_GROUP_ID FROM FOLIO_CONF.FIELD_PRIV WHERE FIELD_ID = 2814)
       AND USER_GROUP_ID IN(4,5,8);

COMMIT;

ALTER TABLE DEVICE.DEVICE ADD(LAST_CLIENT_IP_ADDRESS VARCHAR2(40));

GRANT UPDATE ON PSS.CONSUMER to USALIVE_APP_ROLE;

GRANT SELECT ON REPORT.TIME_ZONE TO DEVICE;

ALTER TABLE PSS.CONSUMER_ACCT_REPLENISH
ADD (EXPIRATION_DATE DATE,
EXP_NOTIFY_DATE DATE);

INSERT INTO PSS.CONSUMER_SETTING_PARAM(CONSUMER_SETTING_PARAM_ID,DISPLAY_CONFIGURABLE_CD,DISPLAY_LABEL,DISPLAY_GROUP,EDITOR,CONVERTER,CONSUMER_COMMUNICATION_TYPE, DEFAULT_VALUE) 
    VALUES(10 ,'P', 'Contact Me When Replenish Credit Card is about to expire', 'Preferences', 'CHECKBOX:?Y=', null, 'CARD_EXPIRATION','N');
COMMIT;

GRANT SELECT ON PSS.CONSUMER_ACCT_REPLENISH to USAT_RPT_REQ_ROLE;
GRANT SELECT ON PSS.CONSUMER_ACCT to USAT_RPT_REQ_ROLE;
GRANT UPDATE ON PSS.CONSUMER_ACCT_REPLENISH to USAT_RPT_REQ_ROLE;

DROP PROCEDURE REPORT.USER_LOGIN_DEL; 

CREATE OR REPLACE FORCE VIEW CORP.VW_ORF_JPMC_EFT_EXCEPTIONS AS
SELECT * FROM apps.usat_jpmc_returns_corrections@financials;

GRANT SELECT ON CORP.VW_ORF_JPMC_EFT_EXCEPTIONS TO USAT_RPT_GEN_ROLE;

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := -1;
BEGIN
	SELECT MAX(REPORT_ID) INTO LN_REPORT_ID
	FROM REPORT.REPORTS WHERE REPORT_NAME = 'Payment Exceptions';
	
	IF LN_REPORT_ID > 0 THEN
		RETURN;
	END IF;

	SELECT REPORT.REPORTS_SEQ.NEXTVAL INTO LN_REPORT_ID FROM DUAL;
	
INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
VALUES( LN_REPORT_ID, 'Payment Exceptions - {b} to {e}', 6, 0, 'Payment Exceptions', 'Report that shows bank account rejections and other payment exceptions', 'E', 0);
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( LN_REPORT_ID, 'query', 'select ojee.pymt_bank_name "Bank Name", substr(ojee.pymt_bank_account_num, -4) "Account # Last 4", 
ojee.payment_reference_number "Payment Reference #", c.customer_name "Customer", d.sent_date "Payment Report Date", d.ref_nbr "Reference #", 
d.total_amount "Total Amount", ojee.jpmc_return_date "Bank Return Date", ojee.jpmc_return_comments "Bank Return Comments"
from corp.vw_orf_jpmc_eft_exceptions ojee
join corp.doc d on ojee.invoice_num = d.ref_nbr
join corp.customer_bank cb on d.customer_bank_id = cb.customer_bank_id
join corp.customer c on cb.customer_id = c.customer_id
join report.vw_user_customer_bank ucb on cb.customer_bank_id = ucb.customer_bank_id
where ojee.jpmc_return_date >= CAST(? AS DATE) and ojee.jpmc_return_date < CAST(? AS DATE) + 1 and ucb.user_id = ? and ojee.jpmc_return_code like ''R%'' and ojee.amount_to_be_paid > 0
order by ojee.jpmc_return_date desc');

insert into report.report_param values(LN_REPORT_ID, 'paramNames','StartDate,EndDate,UserId');
insert into report.report_param values(LN_REPORT_ID, 'paramTypes','TIMESTAMP,TIMESTAMP,NUMBER');
insert into report.report_param values(LN_REPORT_ID, 'params.StartDate','{b}');
insert into report.report_param values(LN_REPORT_ID, 'params.EndDate','{e}');
insert into report.report_param values(LN_REPORT_ID, 'params.UserId','{u}');
insert into report.report_param values(LN_REPORT_ID, 'isSQLFolio','true');
insert into report.report_param values(LN_REPORT_ID, 'dateFormat','MM/dd/yyyy');

insert into web_content.web_link values(web_content.seq_web_link_id.nextval, 'Payment Exceptions', './select_date_range_frame_sqlfolio.i?basicReportId='||LN_REPORT_ID, 'Payment Exceptions', 'Daily Operations', 'W', 0);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 1);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 6);
commit;

END;
/

UPDATE REPORT.REPORTS SET USAGE = 'E' WHERE REPORT_NAME = 'Declined MORE Authorizations';
COMMIT;

CREATE INDEX PSS.IDX_CONSUMER_CRED_ACTIV_TS ON PSS.CONSUMER_CREDENTIAL(ACTIVATED_TS) TABLESPACE PSS_INDX ONLINE;

DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := -1;
BEGIN
	SELECT MAX(REPORT_ID) INTO LN_REPORT_ID
	FROM REPORT.REPORTS WHERE REPORT_NAME = 'MORE Activations';
	
	IF LN_REPORT_ID > 0 THEN
		RETURN;
	END IF;

	SELECT REPORT.REPORTS_SEQ.NEXTVAL INTO LN_REPORT_ID FROM DUAL;
	
INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
VALUES( LN_REPORT_ID, 'MORE Activations - {b} to {e}', 6, 0, 'MORE Activations', 'Report that shows new MORE account activations', 'E', 0);
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( LN_REPORT_ID, 'query', 'SELECT CUS.CUSTOMER_NAME "Operator Name", R.REGION_NAME "Region Name", C.CONSUMER_FNAME "First Name", C.CONSUMER_LNAME "Last Name", CA.CONSUMER_ACCT_IDENTIFIER "Card Id", 
CA.CONSUMER_ACCT_BALANCE "Account Balance", CA.NICK_NAME "Card Nickname", CA.CONSUMER_ACCT_CD "Card Number", C.CONSUMER_EMAIL_ADDR1 "Email Address", C.CONSUMER_CELL_PHONE_NUM "Mobile Phone Number",
CA.CONSUMER_ACCT_ACTIVE_YN_FLAG "Account Enabled", DBADMIN.UTC_TO_LOCAL_DATE(CC.CREATED_UTC_TS) "Registration Time", CC.CREDENTIAL_ACTIVE_FLAG "Account Activated", 
CC.ACTIVATED_TS "Activation Time", CAS.CONSUMER_ACCT_SUB_TYPE_DESC "Account Sub Type", CA.CONSUMER_ACCT_PROMO_TOTAL "Promo Total",
CA.CONSUMER_ACCT_REPLENISH_TOTAL "Replenish Total", CA.REPLENISH_BONUS_TOTAL "Replenish Bonus Total", 
CA.LOYALTY_DISCOUNT_TOTAL "Loyalty Discount Total", CA.CASH_BACK_TOTAL "Cashback Total", 
RT.REPLENISH_TYPE_DESC "Replenish Type", CAR.REPLENISH_AMOUNT "Replenish Amount", CAR.REPLENISH_THRESHHOLD "Replenish Threshold", 
CAR.REPLENISH_CARD_MASKED "Replenish Card", CAR.LAST_REPLENISH_TRAN_TS "Last Replenish Time",
CA.CONSUMER_ACCT_REPLENISH_TOTAL+CA.CONSUMER_ACCT_PROMO_TOTAL-CA.CONSUMER_ACCT_BALANCE "Total Tran Amount",
CA.TRAN_COUNT_TOTAL "Total Tran Count"
FROM PSS.CONSUMER C
JOIN PSS.CONSUMER_ACCT CA ON C.CONSUMER_ID = CA.CONSUMER_ID
JOIN PSS.CONSUMER_ACCT_SUB_TYPE CAS ON CA.CONSUMER_ACCT_SUB_TYPE_ID = CAS.CONSUMER_ACCT_SUB_TYPE_ID
JOIN PSS.CONSUMER_CREDENTIAL CC ON C.CONSUMER_ID = CC.CONSUMER_ID
JOIN CORP.CUSTOMER CUS ON CA.CORP_CUSTOMER_ID = CUS.CUSTOMER_ID
LEFT OUTER JOIN REPORT.REGION R ON C.REGION_ID = R.REGION_ID
LEFT OUTER JOIN PSS.CONSUMER_ACCT_REPLENISH CAR ON CA.CONSUMER_ACCT_REPLENISH_ID = CAR.CONSUMER_ACCT_REPLENISH_ID
LEFT OUTER JOIN PSS.REPLENISH_TYPE RT ON CAR.REPLENISH_TYPE_ID = RT.REPLENISH_TYPE_ID
JOIN REPORT.VW_USER_CONSUMER_ACCT VUCA ON VUCA.CONSUMER_ACCT_ID=CA.CONSUMER_ACCT_ID
JOIN REPORT.USER_LOGIN UL ON UL.USER_ID = VUCA.USER_ID 
WHERE CA.CONSUMER_ACCT_TYPE_ID = 3 AND CC.ACTIVATED_TS >= CAST(? AS DATE) AND CC.ACTIVATED_TS < CAST(? AS DATE) + 1
AND UL.USER_ID = ?
ORDER BY CUS.CUSTOMER_NAME, C.CONSUMER_FNAME, C.CONSUMER_LNAME');

insert into report.report_param values(LN_REPORT_ID, 'paramNames','StartDate,EndDate,UserId');
insert into report.report_param values(LN_REPORT_ID, 'paramTypes','TIMESTAMP,TIMESTAMP,NUMBER');
insert into report.report_param values(LN_REPORT_ID, 'params.StartDate','{b}');
insert into report.report_param values(LN_REPORT_ID, 'params.EndDate','{e}');
insert into report.report_param values(LN_REPORT_ID, 'params.UserId','{u}');
insert into report.report_param values(LN_REPORT_ID, 'isSQLFolio','true');
insert into report.report_param values(LN_REPORT_ID, 'dateFormat','MM/dd/yyyy');

insert into web_content.web_link values(web_content.seq_web_link_id.nextval, 'MORE Activations', './select_date_range_frame_sqlfolio.i?basicReportId='||LN_REPORT_ID||'&' ||'reportTitle=MORE+Activations', 'MORE Activations', 'Daily Operations', 'W', 0);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 1);
commit;

END;
/

UPDATE WEB_CONTENT.WEB_LINK
   SET WEB_LINK_URL = REGEXP_REPLACE(WEB_LINK_URL, '((\?)|&)reportTitle=[^&]*', '\2')
 WHERE WEB_LINK_URL LIKE '%reportTitle=%'
   AND WEB_LINK_USAGE = 'W';
   
COMMIT;

GRANT SELECT ON REPORT.VW_ADMIN_USER TO USALIVE_APP_ROLE;
GRANT EXECUTE ON DEVICE.UPDATE_GPRS_FOR_DEVICE TO USAT_APP_LAYER_ROLE;
CREATE PUBLIC SYNONYM UPDATE_GPRS_FOR_DEVICE FOR DEVICE.UPDATE_GPRS_FOR_DEVICE;

INSERT INTO DEVICE.HOST_TYPE(HOST_TYPE_ID, HOST_TYPE_DESC, HOST_DEFAULT_COMPLETE_MINUT)
    SELECT 100, 'ePort Beacon Module', 0
      FROM DUAL
     WHERE NOT EXISTS(SELECT 1 FROM DEVICE.HOST_TYPE WHERE HOST_TYPE_ID = 100);
     
COMMIT;

GRANT SELECT ON ENGINE.APP_SETTING TO USAT_PREPAID_APP_ROLE;

insert into engine.app_setting(app_setting_cd, app_setting_value, app_setting_desc)
values('MORE_ALLOW_SIMPLE_SIGNUP', 'N', 'Setting to display simple signup option or not on MORE website.''Y'' will allow simple signup, ''N'' will only support regular signup');
commit;

INSERT INTO DEVICE.BEZEL_MFGR(HOST_EQUIPMENT_MFGR, ENTRY_CAPABILITY_CDS)
   SELECT 'MEI 4-in-1+', 'SPE'
     FROM DUAL 
    WHERE NOT EXISTS(SELECT 1 FROM DEVICE.BEZEL_MFGR WHERE HOST_EQUIPMENT_MFGR = 'MEI 4-in-1+');
            
COMMIT;

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/PKG_APP_USER.psk?rev=1.15
CREATE OR REPLACE PACKAGE REPORT.PKG_APP_USER AS
    PROCEDURE LOGIN_INTERNAL_USER(
        pn_login_user_id OUT REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_login_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pv_login_first_name REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_login_last_name REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_login_email REPORT.USER_LOGIN.EMAIL%TYPE,
        pv_login_time_zone_guid IN OUT REPORT.USER_LOGIN.TIME_ZONE_GUID%TYPE,
        pn_profile_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_profile_user_name OUT REPORT.USER_LOGIN.USER_NAME%TYPE,
        pn_profile_user_type OUT REPORT.USER_LOGIN.USER_TYPE%TYPE,
        pv_profile_first_name OUT REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_profile_last_name OUT REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_profile_email OUT REPORT.USER_LOGIN.EMAIL%TYPE,
        pv_profile_telephone OUT REPORT.USER_LOGIN.TELEPHONE%TYPE,
        pv_profile_fax OUT REPORT.USER_LOGIN.FAX%TYPE,
        pn_profile_customer_id OUT REPORT.USER_LOGIN.CUSTOMER_ID%TYPE,
        pv_profile_customer_name OUT CORP.CUSTOMER.CUSTOMER_NAME%TYPE,
        pt_profile_user_privileges OUT NUMBER_TABLE,
        pn_profile_active_bank_accts OUT PLS_INTEGER,
        pn_profile_pending_bank_accts OUT PLS_INTEGER,
        pn_profile_terminal_count OUT PLS_INTEGER,
        pv_profile_license_nbr OUT CORP.LICENSE_NBR.LICENSE_NBR%TYPE);
        
    -- R45
    PROCEDURE LOGIN_INTERNAL_USER(
        pn_login_user_id OUT REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_login_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pv_login_first_name REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_login_last_name REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_login_email REPORT.USER_LOGIN.EMAIL%TYPE,
        pv_login_time_zone_guid IN OUT REPORT.USER_LOGIN.TIME_ZONE_GUID%TYPE,
        pn_profile_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_profile_user_name OUT REPORT.USER_LOGIN.USER_NAME%TYPE,
        pn_profile_user_type OUT REPORT.USER_LOGIN.USER_TYPE%TYPE,
        pv_profile_first_name OUT REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_profile_last_name OUT REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_profile_email OUT REPORT.USER_LOGIN.EMAIL%TYPE,
        pv_profile_telephone OUT REPORT.USER_LOGIN.TELEPHONE%TYPE,
        pv_profile_fax OUT REPORT.USER_LOGIN.FAX%TYPE,
        pn_profile_customer_id OUT REPORT.USER_LOGIN.CUSTOMER_ID%TYPE,
        pv_profile_customer_name OUT CORP.CUSTOMER.CUSTOMER_NAME%TYPE,
        pt_profile_user_privileges OUT NUMBER_TABLE,
        pn_profile_active_bank_accts OUT PLS_INTEGER,
        pn_profile_pending_bank_accts OUT PLS_INTEGER,
        pn_profile_terminal_count OUT PLS_INTEGER,
        pv_profile_license_nbr OUT CORP.LICENSE_NBR.LICENSE_NBR%TYPE,
        pc_license_type OUT VARCHAR2);
        
    --USALive 1.8.0 and above    
    PROCEDURE CHECK_EXTERNAL_USER(
        pn_login_user_id OUT REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_login_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pb_login_password_hash OUT REPORT.USER_LOGIN.PASSWORD_HASH%TYPE,
        pb_login_password_salt OUT REPORT.USER_LOGIN.PASSWORD_SALT%TYPE,
        pn_login_failure_count OUT REPORT.USER_LOGIN.LOGIN_FAILURE_COUNT%TYPE,
        pn_profile_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_profile_user_name OUT REPORT.USER_LOGIN.USER_NAME%TYPE,
        pn_profile_user_type OUT REPORT.USER_LOGIN.USER_TYPE%TYPE,
        pv_profile_first_name OUT REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_profile_last_name OUT REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_profile_email OUT REPORT.USER_LOGIN.EMAIL%TYPE,
        pv_profile_telephone OUT REPORT.USER_LOGIN.TELEPHONE%TYPE,
        pv_profile_fax OUT REPORT.USER_LOGIN.FAX%TYPE,
        pn_profile_customer_id OUT REPORT.USER_LOGIN.CUSTOMER_ID%TYPE,
        pv_profile_customer_name OUT CORP.CUSTOMER.CUSTOMER_NAME%TYPE,
        pt_profile_user_privileges OUT NUMBER_TABLE,
        pn_profile_active_bank_accts OUT PLS_INTEGER,
        pn_profile_pending_bank_accts OUT PLS_INTEGER,
        pn_missing_bank_acct_flag OUT VARCHAR2,
        pn_profile_terminal_count OUT PLS_INTEGER,
        pv_profile_license_nbr OUT CORP.LICENSE_NBR.LICENSE_NBR%TYPE);
        
    -- R45
    PROCEDURE CHECK_EXTERNAL_USER(
        pn_login_user_id OUT REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_login_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pb_login_password_hash OUT REPORT.USER_LOGIN.PASSWORD_HASH%TYPE,
        pb_login_password_salt OUT REPORT.USER_LOGIN.PASSWORD_SALT%TYPE,
        pn_login_failure_count OUT REPORT.USER_LOGIN.LOGIN_FAILURE_COUNT%TYPE,
        pn_profile_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_profile_user_name OUT REPORT.USER_LOGIN.USER_NAME%TYPE,
        pn_profile_user_type OUT REPORT.USER_LOGIN.USER_TYPE%TYPE,
        pv_profile_first_name OUT REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_profile_last_name OUT REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_profile_email OUT REPORT.USER_LOGIN.EMAIL%TYPE,
        pv_profile_telephone OUT REPORT.USER_LOGIN.TELEPHONE%TYPE,
        pv_profile_fax OUT REPORT.USER_LOGIN.FAX%TYPE,
        pn_profile_customer_id OUT REPORT.USER_LOGIN.CUSTOMER_ID%TYPE,
        pv_profile_customer_name OUT CORP.CUSTOMER.CUSTOMER_NAME%TYPE,
        pt_profile_user_privileges OUT NUMBER_TABLE,
        pn_profile_active_bank_accts OUT PLS_INTEGER,
        pn_profile_pending_bank_accts OUT PLS_INTEGER,
        pn_missing_bank_acct_flag OUT VARCHAR2,
        pn_profile_terminal_count OUT PLS_INTEGER,
        pv_profile_license_nbr OUT CORP.LICENSE_NBR.LICENSE_NBR%TYPE,
        pc_license_type OUT VARCHAR2);
        
    --USALive 1.8.0 
    PROCEDURE LOGIN_AS_USER(
        pn_login_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_profile_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_profile_user_name OUT REPORT.USER_LOGIN.USER_NAME%TYPE,
        pn_profile_user_type OUT REPORT.USER_LOGIN.USER_TYPE%TYPE,
        pv_profile_first_name OUT REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_profile_last_name OUT REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_profile_email OUT REPORT.USER_LOGIN.EMAIL%TYPE,
        pv_profile_telephone OUT REPORT.USER_LOGIN.TELEPHONE%TYPE,
        pv_profile_fax OUT REPORT.USER_LOGIN.FAX%TYPE,
        pn_profile_customer_id OUT REPORT.USER_LOGIN.CUSTOMER_ID%TYPE,
        pv_profile_customer_name OUT CORP.CUSTOMER.CUSTOMER_NAME%TYPE,
        pt_profile_user_privileges OUT NUMBER_TABLE,
        pn_profile_active_bank_accts OUT PLS_INTEGER,
        pn_profile_pending_bank_accts OUT PLS_INTEGER,
        pn_missing_bank_acct_flag OUT VARCHAR2,
        pn_profile_terminal_count OUT PLS_INTEGER,
        pv_profile_license_nbr OUT CORP.LICENSE_NBR.LICENSE_NBR%TYPE,
        pv_profile_readonly OUT VARCHAR2);
        
    -- R45
    PROCEDURE LOGIN_AS_USER(
        pn_login_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_profile_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_profile_user_name OUT REPORT.USER_LOGIN.USER_NAME%TYPE,
        pn_profile_user_type OUT REPORT.USER_LOGIN.USER_TYPE%TYPE,
        pv_profile_first_name OUT REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_profile_last_name OUT REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_profile_email OUT REPORT.USER_LOGIN.EMAIL%TYPE,
        pv_profile_telephone OUT REPORT.USER_LOGIN.TELEPHONE%TYPE,
        pv_profile_fax OUT REPORT.USER_LOGIN.FAX%TYPE,
        pn_profile_customer_id OUT REPORT.USER_LOGIN.CUSTOMER_ID%TYPE,
        pv_profile_customer_name OUT CORP.CUSTOMER.CUSTOMER_NAME%TYPE,
        pt_profile_user_privileges OUT NUMBER_TABLE,
        pn_profile_active_bank_accts OUT PLS_INTEGER,
        pn_profile_pending_bank_accts OUT PLS_INTEGER,
        pn_missing_bank_acct_flag OUT VARCHAR2,
        pn_profile_terminal_count OUT PLS_INTEGER,
        pv_profile_license_nbr OUT CORP.LICENSE_NBR.LICENSE_NBR%TYPE,
        pv_profile_readonly OUT VARCHAR2,
        pc_license_type OUT VARCHAR2);
        
    PROCEDURE RECORD_FAILED_EXTERNAL_LOGIN(
        pn_login_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_login_failure_count OUT REPORT.USER_LOGIN.LOGIN_FAILURE_COUNT%TYPE);
        
    PROCEDURE RECORD_EXTERNAL_LOGIN(
        pn_login_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_login_time_zone_guid IN OUT REPORT.USER_LOGIN.TIME_ZONE_GUID%TYPE);
        
    FUNCTION CREATE_PASSCODE(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_passcode_type_id REPORT.PASSCODE_TYPE.PASSCODE_TYPE_ID%TYPE,
        pd_expiration_dt REPORT.USER_PASSCODE.EXPIRATION_TS%TYPE)
    RETURN REPORT.USER_PASSCODE.PASSCODE%TYPE;
    
    PROCEDURE RESET_PASSWORD(
        pv_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pv_passcode OUT REPORT.USER_PASSCODE.PASSCODE%TYPE,
        pv_email OUT REPORT.USER_LOGIN.EMAIL%TYPE);
    
    PROCEDURE CANCEL_PASSWORD_RESET(
        pv_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pv_passcode REPORT.USER_PASSCODE.PASSCODE%TYPE);
        
    PROCEDURE CHANGE_PASSWORD_BY_PASSCODE(
        pv_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pv_passcode REPORT.USER_PASSCODE.PASSCODE%TYPE,
        pb_password_hash REPORT.USER_LOGIN.PASSWORD_HASH%TYPE,
        pb_password_salt REPORT.USER_LOGIN.PASSWORD_SALT%TYPE); 
    
    FUNCTION CREATE_USER(
        pn_user_type REPORT.USER_LOGIN.USER_TYPE%TYPE,
        pv_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pv_first_name REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_last_name REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_email REPORT.USER_LOGIN.EMAIL%TYPE,
        pv_time_zone_guid REPORT.USER_LOGIN.TIME_ZONE_GUID%TYPE,
        pn_admin_id REPORT.USER_LOGIN.ADMIN_ID%TYPE,
        pn_customer_id REPORT.USER_LOGIN.CUSTOMER_ID%TYPE,
        pv_telephone REPORT.USER_LOGIN.TELEPHONE%TYPE,
        pv_fax REPORT.USER_LOGIN.FAX%TYPE,
        pb_password_hash REPORT.USER_LOGIN.PASSWORD_HASH%TYPE,
        pb_password_salt REPORT.USER_LOGIN.PASSWORD_SALT%TYPE,
        pt_user_privileges NUMBER_TABLE)
    RETURN REPORT.USER_LOGIN.USER_ID%TYPE;

    PROCEDURE DELETE_USER(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE);
            
    PROCEDURE UPDATE_USER(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_user_type REPORT.USER_LOGIN.USER_TYPE%TYPE,
        pv_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pv_first_name REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_last_name REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_email REPORT.USER_LOGIN.EMAIL%TYPE,
        pn_admin_id REPORT.USER_LOGIN.ADMIN_ID%TYPE,
        pn_customer_id REPORT.USER_LOGIN.CUSTOMER_ID%TYPE,
        pv_telephone REPORT.USER_LOGIN.TELEPHONE%TYPE,
        pv_fax REPORT.USER_LOGIN.FAX%TYPE,
        pb_password_hash REPORT.USER_LOGIN.PASSWORD_HASH%TYPE,
        pb_password_salt REPORT.USER_LOGIN.PASSWORD_SALT%TYPE,
        pt_user_privileges NUMBER_TABLE,
        pn_updating_user_id REPORT.USER_LOGIN.USER_ID%TYPE);
    
    PROCEDURE CREATE_CUSTOMER(
        pn_user_id OUT CORP.CUSTOMER.USER_ID%TYPE,
        pn_cust_id OUT CORP.CUSTOMER.CUSTOMER_ID%TYPE,
        pv_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pv_first_name REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_last_name REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_email REPORT.USER_LOGIN.EMAIL%TYPE,
        pv_time_zone_guid REPORT.USER_LOGIN.TIME_ZONE_GUID%TYPE,
        pb_password_hash REPORT.USER_LOGIN.PASSWORD_HASH%TYPE,
        pb_password_salt REPORT.USER_LOGIN.PASSWORD_SALT%TYPE,
        pv_cust_name IN CORP.CUSTOMER.CUSTOMER_NAME%TYPE,
        pv_addr1 IN CORP.CUSTOMER_ADDR.ADDRESS1%TYPE,
        pv_city IN CORP.CUSTOMER_ADDR.CITY%TYPE,
        pv_state_cd IN CORP.CUSTOMER_ADDR.STATE%TYPE,
        pv_postal IN CORP.CUSTOMER_ADDR.ZIP%TYPE,
        pv_country_cd IN CORP.CUSTOMER_ADDR.COUNTRY_CD%TYPE,       
        pv_telephone REPORT.USER_LOGIN.TELEPHONE%TYPE,
        pv_fax REPORT.USER_LOGIN.FAX%TYPE,
        pn_dealer_id IN CORP.CUSTOMER.DEALER_ID%TYPE,
        pv_tax_id_nbr IN CORP.CUSTOMER.TAX_ID_NBR%TYPE,
        pv_doing_business_as IN CORP.CUSTOMER.DOING_BUSINESS_AS%TYPE DEFAULT NULL,
        pv_license_id IN CORP.LICENSE.LICENSE_ID%TYPE DEFAULT NULL,
        pv_parent_customer_id IN CORP.CUSTOMER.PARENT_CUSTOMER_ID%TYPE DEFAULT NULL);
        
    PROCEDURE CREATE_CUSTOM_REPORT_LINK(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_label WEB_CONTENT.WEB_LINK.WEB_LINK_LABEL%TYPE,
        pv_url WEB_CONTENT.WEB_LINK.WEB_LINK_URL%TYPE,
        pv_desc WEB_CONTENT.WEB_LINK.WEB_LINK_DESC%TYPE,        
        pn_order WEB_CONTENT.WEB_LINK.WEB_LINK_ORDER%TYPE,
        pv_group WEB_CONTENT.WEB_LINK.WEB_LINK_GROUP%TYPE DEFAULT 'User Defined');
    
    PROCEDURE DELETE_USER_LINK(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_link_id WEB_CONTENT.WEB_LINK.WEB_LINK_ID%TYPE);
    
     FUNCTION GET_USER_PREFERENCE(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_pref_id REPORT.PREFERENCE.PREFERENCE_ID%TYPE)
        RETURN REPORT.USER_PREFERENCE.PREFERENCE_VALUE%TYPE;
    
     FUNCTION GET_HEALTH_CODE(
        pn_measured_date DATE,
        pn_min_days NUMBER,
        pn_max_days NUMBER)
        RETURN CHAR;
        
     FUNCTION GET_HEALTH_CODE_BY_PREF(
        pn_date DATE,
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_pref_id REPORT.PREFERENCE.PREFERENCE_ID%TYPE)
        RETURN CHAR;
        
    PROCEDURE UPSERT_USER_PREFERENCE(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_preference_id REPORT.PREFERENCE.PREFERENCE_ID%TYPE,
        pv_preference_value REPORT.USER_PREFERENCE.PREFERENCE_VALUE%TYPE);
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/PKG_APP_USER.pbk?rev=1.28
CREATE OR REPLACE PACKAGE BODY REPORT.PKG_APP_USER AS
    PROCEDURE POPULATE_USER(
        pn_profile_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pc_internal_flag CHAR,
        pv_profile_user_name OUT REPORT.USER_LOGIN.USER_NAME%TYPE,
        pn_profile_user_type OUT REPORT.USER_LOGIN.USER_TYPE%TYPE,
        pv_profile_first_name OUT REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_profile_last_name OUT REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_profile_email OUT REPORT.USER_LOGIN.EMAIL%TYPE,
        pv_profile_telephone OUT REPORT.USER_LOGIN.TELEPHONE%TYPE,
        pv_profile_fax OUT REPORT.USER_LOGIN.FAX%TYPE,
        pn_profile_customer_id OUT REPORT.USER_LOGIN.CUSTOMER_ID%TYPE,
        pv_profile_customer_name OUT CORP.CUSTOMER.CUSTOMER_NAME%TYPE,
        pt_profile_user_privileges OUT NUMBER_TABLE,
        pn_profile_active_bank_accts OUT PLS_INTEGER,
        pn_profile_pending_bank_accts OUT PLS_INTEGER,
        pn_customer_active_bank_accts OUT PLS_INTEGER,
        pn_profile_terminal_count OUT PLS_INTEGER,
        pv_profile_license_nbr OUT CORP.LICENSE_NBR.LICENSE_NBR%TYPE,
        pc_license_type OUT VARCHAR2)
    AS
        lc_internal_flag CHAR(1);
    BEGIN
        SELECT U.USER_NAME, U.USER_TYPE, U.FIRST_NAME, U.LAST_NAME, U.EMAIL, U.TELEPHONE, U.FAX, NVL(C.CUSTOMER_ID, 0), C.CUSTOMER_NAME
          INTO pv_profile_user_name, pn_profile_user_type, pv_profile_first_name, pv_profile_last_name, pv_profile_email, pv_profile_telephone, pv_profile_fax, pn_profile_customer_id, pv_profile_customer_name 
          FROM REPORT.USER_LOGIN U
          LEFT OUTER JOIN CORP.CUSTOMER C ON U.CUSTOMER_ID = C.CUSTOMER_ID AND C.STATUS != 'D'
         WHERE U.USER_ID = pn_profile_user_id
           AND U.STATUS = 'A';
        
        -- get counts and privs
        IF pc_internal_flag != '?' THEN
            lc_internal_flag := pc_internal_flag;
        ELSIF pv_profile_user_name LIKE '%@usatech.com' AND pn_profile_user_type != 8 THEN
            lc_internal_flag := 'Y';
        ELSE
            lc_internal_flag := 'N';
        END IF;
        
        SELECT UP.PRIV_ID
          BULK COLLECT INTO pt_profile_user_privileges
          FROM REPORT.USER_PRIVS UP 
          JOIN REPORT.PRIV P on UP.PRIV_ID=P.PRIV_ID
         WHERE UP.USER_ID = pn_profile_user_id
           AND (P.INTERNAL_EXTERNAL_FLAG = 'B' OR (lc_internal_flag = 'Y' AND P.INTERNAL_EXTERNAL_FLAG = 'I') OR (lc_internal_flag = 'N' AND P.INTERNAL_EXTERNAL_FLAG IN('E', 'P')));
                   
        SELECT COUNT(*)
          INTO pn_profile_terminal_count
          FROM REPORT.VW_USER_TERMINAL UT
         WHERE UT.USER_ID = pn_profile_user_id;
         
        SELECT COUNT(DISTINCT DECODE(UCB.STATUS, 'A', UCB.CUSTOMER_BANK_ID)) ACTIVE_BANK_ACCT_COUNT, COUNT(DISTINCT DECODE(UCB.STATUS, 'P', UCB.CUSTOMER_BANK_ID)) PENDING_BANK_ACCT_COUNT
          INTO pn_profile_active_bank_accts, pn_profile_pending_bank_accts
          FROM REPORT.VW_USER_CUSTOMER_BANK UCB
	     WHERE UCB.USER_ID = pn_profile_user_id;
         
        IF pn_profile_user_type = 8 THEN
          BEGIN
            SELECT NBR.LICENSE_NBR, L.LICENSE_TYPE
              INTO pv_profile_license_nbr, pc_license_type
              FROM (SELECT MAX(LICENSE_NBR) LICENSE_NBR
                FROM (SELECT CL.LICENSE_NBR
                  FROM CORP.CUSTOMER_LICENSE CL
                  WHERE CUSTOMER_ID = pn_profile_customer_id
                  ORDER BY CL.RECEIVED DESC)
                WHERE ROWNUM = 1) CURRENT_LICENSE
            JOIN CORP.LICENSE_NBR NBR ON NBR.LICENSE_NBR = CURRENT_LICENSE.LICENSE_NBR
            JOIN CORP.LICENSE L ON L.LICENSE_ID = NBR.LICENSE_ID;
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              pv_profile_license_nbr := 0;
              pc_license_type := 'S';
          END;
          SELECT COUNT(*)
            INTO pn_customer_active_bank_accts
            FROM CORP.CUSTOMER_BANK
           WHERE CUSTOMER_ID = pn_profile_customer_id
             AND STATUS = 'A';
        ELSE
            pn_profile_pending_bank_accts := 0;
            pn_customer_active_bank_accts := 0;
            pc_license_type := 'S';
        END IF; 
    END;

    PROCEDURE LOGIN_INTERNAL_USER(
        pn_login_user_id OUT REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_login_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pv_login_first_name REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_login_last_name REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_login_email REPORT.USER_LOGIN.EMAIL%TYPE,
        pv_login_time_zone_guid IN OUT REPORT.USER_LOGIN.TIME_ZONE_GUID%TYPE,
        pn_profile_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_profile_user_name OUT REPORT.USER_LOGIN.USER_NAME%TYPE,
        pn_profile_user_type OUT REPORT.USER_LOGIN.USER_TYPE%TYPE,
        pv_profile_first_name OUT REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_profile_last_name OUT REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_profile_email OUT REPORT.USER_LOGIN.EMAIL%TYPE,
        pv_profile_telephone OUT REPORT.USER_LOGIN.TELEPHONE%TYPE,
        pv_profile_fax OUT REPORT.USER_LOGIN.FAX%TYPE,
        pn_profile_customer_id OUT REPORT.USER_LOGIN.CUSTOMER_ID%TYPE,
        pv_profile_customer_name OUT CORP.CUSTOMER.CUSTOMER_NAME%TYPE,
        pt_profile_user_privileges OUT NUMBER_TABLE,
        pn_profile_active_bank_accts OUT PLS_INTEGER,
        pn_profile_pending_bank_accts OUT PLS_INTEGER,
        pn_profile_terminal_count OUT PLS_INTEGER,
        pv_profile_license_nbr OUT CORP.LICENSE_NBR.LICENSE_NBR%TYPE)
    AS
        pc_license_type VARCHAR2(1);
    BEGIN
        LOGIN_INTERNAL_USER(pn_login_user_id, pv_login_user_name, pv_login_first_name, 
            pv_login_last_name, pv_login_email, pv_login_time_zone_guid, pn_profile_user_id,
            pv_profile_user_name, pn_profile_user_type, pv_profile_first_name, pv_profile_last_name, 
            pv_profile_email, pv_profile_telephone, pv_profile_fax, pn_profile_customer_id, 
            pv_profile_customer_name, pt_profile_user_privileges, pn_profile_active_bank_accts, 
            pn_profile_pending_bank_accts, pn_profile_terminal_count, pv_profile_license_nbr, pc_license_type);
    END;
    
    -- R45
    PROCEDURE LOGIN_INTERNAL_USER(
        pn_login_user_id OUT REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_login_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pv_login_first_name REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_login_last_name REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_login_email REPORT.USER_LOGIN.EMAIL%TYPE,
        pv_login_time_zone_guid IN OUT REPORT.USER_LOGIN.TIME_ZONE_GUID%TYPE,
        pn_profile_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_profile_user_name OUT REPORT.USER_LOGIN.USER_NAME%TYPE,
        pn_profile_user_type OUT REPORT.USER_LOGIN.USER_TYPE%TYPE,
        pv_profile_first_name OUT REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_profile_last_name OUT REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_profile_email OUT REPORT.USER_LOGIN.EMAIL%TYPE,
        pv_profile_telephone OUT REPORT.USER_LOGIN.TELEPHONE%TYPE,
        pv_profile_fax OUT REPORT.USER_LOGIN.FAX%TYPE,
        pn_profile_customer_id OUT REPORT.USER_LOGIN.CUSTOMER_ID%TYPE,
        pv_profile_customer_name OUT CORP.CUSTOMER.CUSTOMER_NAME%TYPE,
        pt_profile_user_privileges OUT NUMBER_TABLE,
        pn_profile_active_bank_accts OUT PLS_INTEGER,
        pn_profile_pending_bank_accts OUT PLS_INTEGER,
        pn_profile_terminal_count OUT PLS_INTEGER,
        pv_profile_license_nbr OUT CORP.LICENSE_NBR.LICENSE_NBR%TYPE,
        pc_license_type OUT VARCHAR2)
    AS
        ln_profile_user_id REPORT.USER_LOGIN.USER_ID%TYPE;
        ln_customer_active_bank_accts PLS_INTEGER;
    BEGIN
        UPDATE REPORT.USER_LOGIN
           SET LAST_LOGIN_TS = SYSDATE,
               FIRST_NAME = NVL(pv_login_first_name, FIRST_NAME),
               LAST_NAME = NVL(pv_login_last_name, LAST_NAME),
               EMAIL = NVL(pv_login_email, EMAIL),
               TIME_ZONE_GUID = NVL(pv_login_time_zone_guid, TIME_ZONE_GUID),
               STATUS = 'A'
         WHERE USER_NAME = pv_login_user_name
           AND USER_TYPE != 8
         RETURNING USER_ID, TIME_ZONE_GUID
              INTO pn_login_user_id, pv_login_time_zone_guid;
        IF SQL%NOTFOUND THEN
            pn_login_user_id := CREATE_USER(9, pv_login_user_name, pv_login_first_name, pv_login_last_name, pv_login_email, pv_login_time_zone_guid,  NULL, 0, NULL, NULL, NULL, NULL, NULL); 
        END IF;
        IF pn_profile_user_id IS NULL THEN
            ln_profile_user_id := pn_login_user_id;
        ELSIF REPORT.CAN_ADMIN_USER(pn_profile_user_id, pn_login_user_id)  = 'N' THEN
            IF REPORT.CHECK_PRIV(pn_login_user_id,10)='Y' THEN
              ln_profile_user_id := pn_profile_user_id;
            ELSE
              RAISE_APPLICATION_ERROR(-20100, 'User ''' || pv_login_user_name || ''' may not log in as user id ' || pn_profile_user_id); 
            END IF;
        ELSE
            ln_profile_user_id := pn_profile_user_id;
        END IF;
        POPULATE_USER(
            ln_profile_user_id,
            CASE WHEN ln_profile_user_id = pn_login_user_id THEN 'Y' ELSE '?' END,
            pv_profile_user_name,
            pn_profile_user_type,
            pv_profile_first_name,
            pv_profile_last_name,
            pv_profile_email,
            pv_profile_telephone,
            pv_profile_fax,
            pn_profile_customer_id,
            pv_profile_customer_name,
            pt_profile_user_privileges,
            pn_profile_active_bank_accts,
            pn_profile_pending_bank_accts,
            ln_customer_active_bank_accts,
            pn_profile_terminal_count,
            pv_profile_license_nbr,
            pc_license_type);
    END;
    
    PROCEDURE CHECK_EXTERNAL_USER(
        pn_login_user_id OUT REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_login_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pb_login_password_hash OUT REPORT.USER_LOGIN.PASSWORD_HASH%TYPE,
        pb_login_password_salt OUT REPORT.USER_LOGIN.PASSWORD_SALT%TYPE,
        pn_login_failure_count OUT REPORT.USER_LOGIN.LOGIN_FAILURE_COUNT%TYPE,
        pn_profile_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_profile_user_name OUT REPORT.USER_LOGIN.USER_NAME%TYPE,
        pn_profile_user_type OUT REPORT.USER_LOGIN.USER_TYPE%TYPE,
        pv_profile_first_name OUT REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_profile_last_name OUT REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_profile_email OUT REPORT.USER_LOGIN.EMAIL%TYPE,
        pv_profile_telephone OUT REPORT.USER_LOGIN.TELEPHONE%TYPE,
        pv_profile_fax OUT REPORT.USER_LOGIN.FAX%TYPE,
        pn_profile_customer_id OUT REPORT.USER_LOGIN.CUSTOMER_ID%TYPE,
        pv_profile_customer_name OUT CORP.CUSTOMER.CUSTOMER_NAME%TYPE,
        pt_profile_user_privileges OUT NUMBER_TABLE,
        pn_profile_active_bank_accts OUT PLS_INTEGER,
        pn_profile_pending_bank_accts OUT PLS_INTEGER,
        pn_missing_bank_acct_flag OUT VARCHAR2,
        pn_profile_terminal_count OUT PLS_INTEGER,
        pv_profile_license_nbr OUT CORP.LICENSE_NBR.LICENSE_NBR%TYPE)
    AS
        pc_license_type VARCHAR2(1);
    BEGIN
        CHECK_EXTERNAL_USER(pn_login_user_id, pv_login_user_name, pb_login_password_hash,
            pb_login_password_salt, pn_login_failure_count, pn_profile_user_id, pv_profile_user_name,
            pn_profile_user_type, pv_profile_first_name, pv_profile_last_name, pv_profile_email,
            pv_profile_telephone, pv_profile_fax, pn_profile_customer_id, pv_profile_customer_name,
            pt_profile_user_privileges, pn_profile_active_bank_accts, pn_profile_pending_bank_accts,
            pn_missing_bank_acct_flag, pn_profile_terminal_count, pv_profile_license_nbr, pc_license_type);
    END;
    
    -- R45
    PROCEDURE CHECK_EXTERNAL_USER(
        pn_login_user_id OUT REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_login_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pb_login_password_hash OUT REPORT.USER_LOGIN.PASSWORD_HASH%TYPE,
        pb_login_password_salt OUT REPORT.USER_LOGIN.PASSWORD_SALT%TYPE,
        pn_login_failure_count OUT REPORT.USER_LOGIN.LOGIN_FAILURE_COUNT%TYPE,
        pn_profile_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_profile_user_name OUT REPORT.USER_LOGIN.USER_NAME%TYPE,
        pn_profile_user_type OUT REPORT.USER_LOGIN.USER_TYPE%TYPE,
        pv_profile_first_name OUT REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_profile_last_name OUT REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_profile_email OUT REPORT.USER_LOGIN.EMAIL%TYPE,
        pv_profile_telephone OUT REPORT.USER_LOGIN.TELEPHONE%TYPE,
        pv_profile_fax OUT REPORT.USER_LOGIN.FAX%TYPE,
        pn_profile_customer_id OUT REPORT.USER_LOGIN.CUSTOMER_ID%TYPE,
        pv_profile_customer_name OUT CORP.CUSTOMER.CUSTOMER_NAME%TYPE,
        pt_profile_user_privileges OUT NUMBER_TABLE,
        pn_profile_active_bank_accts OUT PLS_INTEGER,
        pn_profile_pending_bank_accts OUT PLS_INTEGER,
        pn_missing_bank_acct_flag OUT VARCHAR2,
        pn_profile_terminal_count OUT PLS_INTEGER,
        pv_profile_license_nbr OUT CORP.LICENSE_NBR.LICENSE_NBR%TYPE,
        pc_license_type OUT VARCHAR2)
    AS
        ln_profile_user_id REPORT.USER_LOGIN.USER_ID%TYPE;
        ln_customer_active_bank_accts PLS_INTEGER;
    BEGIN
        SELECT U.USER_ID, U.PASSWORD_HASH, U.PASSWORD_SALT, U.LOGIN_FAILURE_COUNT
          INTO pn_login_user_id, pb_login_password_hash, pb_login_password_salt, pn_login_failure_count
          FROM REPORT.USER_LOGIN U
         WHERE U.USER_NAME = pv_login_user_name
           AND U.STATUS = 'A'
           AND U.USER_TYPE = 8;
        IF pn_profile_user_id IS NULL THEN
            ln_profile_user_id := pn_login_user_id;
        ELSIF REPORT.CAN_ADMIN_USER(pn_profile_user_id, pn_login_user_id)  = 'N' THEN
            RAISE_APPLICATION_ERROR(-20100, 'User ''' || pv_login_user_name || ''' may not log in as user id ' || pn_profile_user_id); 
        ELSE
            ln_profile_user_id := pn_profile_user_id;
        END IF;
        POPULATE_USER(
            ln_profile_user_id,
            'N',
            pv_profile_user_name,
            pn_profile_user_type,
            pv_profile_first_name,
            pv_profile_last_name,
            pv_profile_email,
            pv_profile_telephone,
            pv_profile_fax,
            pn_profile_customer_id,
            pv_profile_customer_name,
            pt_profile_user_privileges,
            pn_profile_active_bank_accts,
            pn_profile_pending_bank_accts,
            ln_customer_active_bank_accts,
            pn_profile_terminal_count,
            pv_profile_license_nbr,
            pc_license_type);
        IF ln_customer_active_bank_accts > 0 THEN
            pn_missing_bank_acct_flag := 'N';
        ELSE
            pn_missing_bank_acct_flag := 'Y';
        END IF;
    END;
    
    PROCEDURE LOGIN_AS_USER(
        pn_login_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_profile_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_profile_user_name OUT REPORT.USER_LOGIN.USER_NAME%TYPE,
        pn_profile_user_type OUT REPORT.USER_LOGIN.USER_TYPE%TYPE,
        pv_profile_first_name OUT REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_profile_last_name OUT REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_profile_email OUT REPORT.USER_LOGIN.EMAIL%TYPE,
        pv_profile_telephone OUT REPORT.USER_LOGIN.TELEPHONE%TYPE,
        pv_profile_fax OUT REPORT.USER_LOGIN.FAX%TYPE,
        pn_profile_customer_id OUT REPORT.USER_LOGIN.CUSTOMER_ID%TYPE,
        pv_profile_customer_name OUT CORP.CUSTOMER.CUSTOMER_NAME%TYPE,
        pt_profile_user_privileges OUT NUMBER_TABLE,
        pn_profile_active_bank_accts OUT PLS_INTEGER,
        pn_profile_pending_bank_accts OUT PLS_INTEGER,
        pn_missing_bank_acct_flag OUT VARCHAR2,
        pn_profile_terminal_count OUT PLS_INTEGER,
        pv_profile_license_nbr OUT CORP.LICENSE_NBR.LICENSE_NBR%TYPE,
        pv_profile_readonly OUT VARCHAR2)
    AS
        pc_license_type VARCHAR2(1);
    BEGIN
        LOGIN_AS_USER(pn_login_user_id, pn_profile_user_id, pv_profile_user_name, 
            pn_profile_user_type, pv_profile_first_name, pv_profile_last_name, pv_profile_email, 
            pv_profile_telephone, pv_profile_fax, pn_profile_customer_id, pv_profile_customer_name, 
            pt_profile_user_privileges, pn_profile_active_bank_accts, pn_profile_pending_bank_accts, 
            pn_missing_bank_acct_flag, pn_profile_terminal_count, pv_profile_license_nbr, 
            pv_profile_readonly, pc_license_type);
    END;
    
    -- R45
    PROCEDURE LOGIN_AS_USER(
        pn_login_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_profile_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_profile_user_name OUT REPORT.USER_LOGIN.USER_NAME%TYPE,
        pn_profile_user_type OUT REPORT.USER_LOGIN.USER_TYPE%TYPE,
        pv_profile_first_name OUT REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_profile_last_name OUT REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_profile_email OUT REPORT.USER_LOGIN.EMAIL%TYPE,
        pv_profile_telephone OUT REPORT.USER_LOGIN.TELEPHONE%TYPE,
        pv_profile_fax OUT REPORT.USER_LOGIN.FAX%TYPE,
        pn_profile_customer_id OUT REPORT.USER_LOGIN.CUSTOMER_ID%TYPE,
        pv_profile_customer_name OUT CORP.CUSTOMER.CUSTOMER_NAME%TYPE,
        pt_profile_user_privileges OUT NUMBER_TABLE,
        pn_profile_active_bank_accts OUT PLS_INTEGER,
        pn_profile_pending_bank_accts OUT PLS_INTEGER,
        pn_missing_bank_acct_flag OUT VARCHAR2,
        pn_profile_terminal_count OUT PLS_INTEGER,
        pv_profile_license_nbr OUT CORP.LICENSE_NBR.LICENSE_NBR%TYPE,
        pv_profile_readonly OUT VARCHAR2,
        pc_license_type OUT VARCHAR2)
    AS
        lv_permit CHAR(1);
        ln_customer_active_bank_accts PLS_INTEGER;
    BEGIN
        lv_permit := REPORT.CAN_ADMIN_USER(pn_profile_user_id, pn_login_user_id);
        IF lv_permit = 'N' AND REPORT.CHECK_PRIV(pn_login_user_id,10)!='Y' THEN
            RAISE_APPLICATION_ERROR(-20100, 'User id' || pn_login_user_id || ' may not log in as user id ' || pn_profile_user_id); 
        END IF;
        POPULATE_USER(
            pn_profile_user_id,
            '?',
            pv_profile_user_name,
            pn_profile_user_type,
            pv_profile_first_name,
            pv_profile_last_name,
            pv_profile_email,
            pv_profile_telephone,
            pv_profile_fax,
            pn_profile_customer_id,
            pv_profile_customer_name,
            pt_profile_user_privileges,
            pn_profile_active_bank_accts,
            pn_profile_pending_bank_accts,
            ln_customer_active_bank_accts,
            pn_profile_terminal_count,
            pv_profile_license_nbr,
            pc_license_type); 
        IF lv_permit = 'Y' THEN
            pv_profile_readonly := 'N';
        ELSE
            pv_profile_readonly := 'Y';
        END IF;
        IF ln_customer_active_bank_accts > 0 THEN
            pn_missing_bank_acct_flag := 'N';
        ELSE
            pn_missing_bank_acct_flag := 'Y';
        END IF;
    END;
    
    PROCEDURE RECORD_FAILED_EXTERNAL_LOGIN(
        pn_login_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_login_failure_count OUT REPORT.USER_LOGIN.LOGIN_FAILURE_COUNT%TYPE)
    AS
    BEGIN
        UPDATE REPORT.USER_LOGIN
           SET LOGIN_FAILURE_COUNT = LOGIN_FAILURE_COUNT + 1
         WHERE USER_ID = pn_login_user_id
          RETURNING LOGIN_FAILURE_COUNT 
          INTO pn_login_failure_count;
    END;
        
    PROCEDURE RECORD_EXTERNAL_LOGIN(
        pn_login_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_login_time_zone_guid IN OUT REPORT.USER_LOGIN.TIME_ZONE_GUID%TYPE)
    AS
    BEGIN
        UPDATE REPORT.USER_LOGIN
           SET LAST_LOGIN_TS = SYSDATE,
               TIME_ZONE_GUID = NVL(pv_login_time_zone_guid, TIME_ZONE_GUID),
               LOGIN_FAILURE_COUNT = 0
         WHERE USER_ID = pn_login_user_id
          RETURNING TIME_ZONE_GUID
          INTO pv_login_time_zone_guid;
    END;
    
    FUNCTION CREATE_PASSCODE(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_passcode_type_id REPORT.PASSCODE_TYPE.PASSCODE_TYPE_ID%TYPE,
        pd_expiration_dt REPORT.USER_PASSCODE.EXPIRATION_TS%TYPE)
    RETURN REPORT.USER_PASSCODE.PASSCODE%TYPE
    IS
        lv_passcode REPORT.USER_PASSCODE.PASSCODE%TYPE;
    BEGIN
        lv_passcode := DBMS_RANDOM.STRING('A', 30);
        INSERT INTO REPORT.USER_PASSCODE(USER_PASSCODE_ID, USER_ID, PASSCODE, PASSCODE_TYPE_ID, EXPIRATION_TS)
            VALUES(REPORT.SEQ_USER_PASSCODE_ID.NEXTVAL, pn_user_id, lv_passcode, pn_passcode_type_id, pd_expiration_dt);
        RETURN lv_passcode;
    EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
            RETURN CREATE_PASSCODE(pn_user_id, pn_passcode_type_id, pd_expiration_dt);
    END;
    
    PROCEDURE RESET_PASSWORD(
        pv_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pv_passcode OUT REPORT.USER_PASSCODE.PASSCODE%TYPE,
        pv_email OUT REPORT.USER_LOGIN.EMAIL%TYPE)
    IS
        ln_user_id REPORT.USER_LOGIN.USER_ID%TYPE;
        ld_expiration_dt REPORT.USER_PASSCODE.EXPIRATION_TS%TYPE;
    BEGIN
        SELECT SYSDATE + DURATION_DAYS
          INTO ld_expiration_dt
          FROM REPORT.PASSCODE_TYPE
         WHERE PASSCODE_TYPE_ID = 1;
        UPDATE REPORT.USER_LOGIN
           SET PASSWORD_RESET_FLAG = 'Y'
         WHERE USER_NAME = pv_user_name
         RETURNING USER_ID, EMAIL INTO ln_user_id, pv_email;
        IF ln_user_id IS NULL OR pv_email IS NULL THEN
            RAISE_APPLICATION_ERROR(-20100, 'User ''' || pv_user_name || ''' is not configured for password reset because an email is not registerd'); 
        END IF;
        pv_passcode := CREATE_PASSCODE(ln_user_id, 1, ld_expiration_dt);
    END;
    
    PROCEDURE CANCEL_PASSWORD_RESET(
        pv_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pv_passcode REPORT.USER_PASSCODE.PASSCODE%TYPE)
    IS
        ln_user_id REPORT.USER_LOGIN.USER_ID%TYPE;
        lc_password_reset_flag REPORT.USER_LOGIN.PASSWORD_RESET_FLAG%TYPE;
    BEGIN
        SELECT USER_ID, PASSWORD_RESET_FLAG
          INTO ln_user_id, lc_password_reset_flag
          FROM REPORT.USER_LOGIN
         WHERE USER_NAME = pv_user_name;
        IF lc_password_reset_flag = 'N' THEN
            RAISE_APPLICATION_ERROR(-20400, 'Password has already been cancelled');
        END IF;
        UPDATE REPORT.USER_PASSCODE
           SET EXPIRATION_TS = SYSDATE
         WHERE PASSCODE = pv_passcode
           AND USER_ID = ln_user_id;
        IF SQL%NOTFOUND THEN
             RAISE_APPLICATION_ERROR(-20401, 'Invalid passcode');
        END IF;
        UPDATE REPORT.USER_LOGIN
           SET PASSWORD_RESET_FLAG = 'N'
         WHERE USER_ID = ln_user_id;        
    END;
    
    PROCEDURE CHANGE_PASSWORD_BY_PASSCODE(
        pv_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pv_passcode REPORT.USER_PASSCODE.PASSCODE%TYPE,
        pb_password_hash REPORT.USER_LOGIN.PASSWORD_HASH%TYPE,
        pb_password_salt REPORT.USER_LOGIN.PASSWORD_SALT%TYPE)
    IS
        ln_user_id REPORT.USER_LOGIN.USER_ID%TYPE;
        lc_password_reset_flag REPORT.USER_LOGIN.PASSWORD_RESET_FLAG%TYPE;
    BEGIN
        SELECT USER_ID, PASSWORD_RESET_FLAG
          INTO ln_user_id, lc_password_reset_flag
          FROM REPORT.USER_LOGIN
         WHERE USER_NAME = pv_user_name;
        IF lc_password_reset_flag = 'N' THEN
            RAISE_APPLICATION_ERROR(-20400, 'Password has already been changed');
        END IF;
        UPDATE REPORT.USER_PASSCODE
           SET EXPIRATION_TS = SYSDATE
         WHERE PASSCODE = pv_passcode
           AND USER_ID = ln_user_id;
        IF SQL%NOTFOUND THEN
             RAISE_APPLICATION_ERROR(-20401, 'Invalid passcode');
        END IF;
        UPDATE REPORT.USER_LOGIN
           SET PASSWORD_RESET_FLAG = 'N',
               PASSWORD_HASH = pb_password_hash,
               PASSWORD_SALT = pb_password_salt
         WHERE USER_ID = ln_user_id;        
    END; 
    
    FUNCTION CREATE_USER(
        pn_user_type REPORT.USER_LOGIN.USER_TYPE%TYPE,
        pv_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pv_first_name REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_last_name REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_email REPORT.USER_LOGIN.EMAIL%TYPE,
        pv_time_zone_guid REPORT.USER_LOGIN.TIME_ZONE_GUID%TYPE,
        pn_admin_id REPORT.USER_LOGIN.ADMIN_ID%TYPE,
        pn_customer_id REPORT.USER_LOGIN.CUSTOMER_ID%TYPE,
        pv_telephone REPORT.USER_LOGIN.TELEPHONE%TYPE,
        pv_fax REPORT.USER_LOGIN.FAX%TYPE,
        pb_password_hash REPORT.USER_LOGIN.PASSWORD_HASH%TYPE,
        pb_password_salt REPORT.USER_LOGIN.PASSWORD_SALT%TYPE,
        pt_user_privileges NUMBER_TABLE)
    RETURN REPORT.USER_LOGIN.USER_ID%TYPE
    IS
		ln_user_id REPORT.USER_LOGIN.USER_ID%TYPE;
	BEGIN
		SELECT REPORT.USER_LOGIN_SEQ.NEXTVAL 
		  INTO ln_user_id
		  FROM DUAL;
	 	INSERT INTO REPORT.USER_LOGIN(
            USER_ID, 
            USER_TYPE, 
            USER_NAME, 
            FIRST_NAME, 
            LAST_NAME, 
            EMAIL, 
            TIME_ZONE_GUID,
            ADMIN_ID, 
            CUSTOMER_ID, 
            TELEPHONE, 
            FAX, 
            PASSWORD_HASH, 
            PASSWORD_SALT,
            LAST_LOGIN_TS)
          VALUES(
            ln_user_id, 
            pn_user_type,
            pv_user_name,
            pv_first_name,
            pv_last_name,
            pv_email,
            pv_time_zone_guid,
            NVL(pn_admin_id, 0),
            NVL(pn_customer_id, 0),
            pv_telephone,
            pv_fax,
            pb_password_hash,
            pb_password_salt,
            SYSDATE);
        IF pt_user_privileges IS NOT NULL THEN
            IF NVL(pn_admin_id, 0) = 0 THEN
                INSERT INTO REPORT.USER_PRIVS(USER_ID, PRIV_ID)
                  SELECT ln_user_id, P.PRIV_ID
                    FROM REPORT.PRIV P
                   WHERE P.PRIV_ID MEMBER OF pt_user_privileges
                     AND (pn_user_type != 8 OR P.INTERNAL_EXTERNAL_FLAG != 'I');
            ELSE
                INSERT INTO REPORT.USER_PRIVS(USER_ID, PRIV_ID)
                  SELECT ln_user_id, P.PRIV_ID
                    FROM REPORT.PRIV P
                    JOIN REPORT.USER_PRIVS UP ON UP.PRIV_ID = P.PRIV_ID
                    JOIN REPORT.USER_LOGIN U ON UP.USER_ID = U.USER_ID
                   WHERE P.PRIV_ID MEMBER OF pt_user_privileges
                     AND UP.USER_ID = pn_admin_id 
                     AND (U.USER_TYPE != 8 OR P.INTERNAL_EXTERNAL_FLAG IN('B', 'E'))
                     AND (pn_user_type != 8 OR P.INTERNAL_EXTERNAL_FLAG != 'I');
            END IF;
        END IF;
        IF pn_admin_id IS NULL OR pn_admin_id = 0 THEN
          INSERT INTO REPORT.USER_DISPLAY(USER_ID, DISPLAY_ID, SEQ)
              SELECT ln_user_id, 1, 1 FROM DUAL
              UNION ALL
              SELECT ln_user_id, 2, 2 FROM DUAL;
        ELSE
           INSERT INTO REPORT.USER_DISPLAY(USER_ID, DISPLAY_ID, SEQ)
           SELECT ln_user_id, DISPLAY_ID, SEQ FROM REPORT.USER_DISPLAY WHERE USER_ID=pn_admin_id;
           INSERT INTO REPORT.USER_PREFERENCE (USER_ID, PREFERENCE_ID, PREFERENCE_VALUE)
           SELECT ln_user_id, PREFERENCE_ID, PREFERENCE_VALUE FROM REPORT.USER_PREFERENCE WHERE USER_ID=pn_admin_id;
        END IF;
        INSERT INTO REPORT.REPORT_REQUEST_ORDER (USER_ID, PROFILE_MAX_REQUEST_ORDER,USER_MAX_REQUEST_ORDER)
        VALUES(ln_user_id, 0, 0);
		RETURN ln_user_id;
	END;

    PROCEDURE DELETE_USER(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE)
    IS
    BEGIN
        UPDATE REPORT.USER_LOGIN 
           SET STATUS = 'D' 
         WHERE USER_ID = pn_user_id;
        DELETE FROM REPORT.USER_PRIVS 
         WHERE USER_ID = pn_user_id;
        DELETE FROM REPORT.USER_DISPLAY WHERE USER_ID = pn_user_id;
        DELETE FROM REPORT.USER_PREFERENCE WHERE USER_ID = pn_user_id;
    END;
            
    PROCEDURE UPDATE_USER(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_user_type REPORT.USER_LOGIN.USER_TYPE%TYPE,
        pv_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pv_first_name REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_last_name REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_email REPORT.USER_LOGIN.EMAIL%TYPE,
        pn_admin_id REPORT.USER_LOGIN.ADMIN_ID%TYPE,
        pn_customer_id REPORT.USER_LOGIN.CUSTOMER_ID%TYPE,
        pv_telephone REPORT.USER_LOGIN.TELEPHONE%TYPE,
        pv_fax REPORT.USER_LOGIN.FAX%TYPE,
        pb_password_hash REPORT.USER_LOGIN.PASSWORD_HASH%TYPE,
        pb_password_salt REPORT.USER_LOGIN.PASSWORD_SALT%TYPE,
        pt_user_privileges NUMBER_TABLE,
        pn_updating_user_id REPORT.USER_LOGIN.USER_ID%TYPE)
    IS
        ln_user_type REPORT.USER_LOGIN.USER_TYPE%TYPE;
	BEGIN
		UPDATE REPORT.USER_LOGIN
          SET USER_TYPE = NVL(pn_user_type, USER_TYPE), 
            USER_NAME = NVL(pv_user_name, USER_NAME), 
            FIRST_NAME = NVL(pv_first_name, FIRST_NAME), 
            LAST_NAME = NVL(pv_last_name, LAST_NAME), 
            EMAIL = NVL(pv_email, EMAIL), 
            ADMIN_ID = NVL(pn_admin_id, ADMIN_ID), 
            CUSTOMER_ID = NVL(pn_customer_id, CUSTOMER_ID), 
            TELEPHONE = NVL(pv_telephone, TELEPHONE), 
            FAX = NVL(pv_fax, FAX), 
            PASSWORD_HASH = NVL(pb_password_hash, PASSWORD_HASH), 
            PASSWORD_SALT = NVL(pb_password_salt, PASSWORD_SALT),
            PASSWORD_RESET_FLAG = DECODE(PASSWORD_HASH, NULL, PASSWORD_RESET_FLAG, 'N')
         WHERE USER_ID = pn_user_id
         RETURNING USER_TYPE INTO ln_user_type;
        IF pn_updating_user_id != pn_user_id AND pt_user_privileges IS NOT NULL THEN
            DELETE
              FROM REPORT.USER_PRIVS
             WHERE USER_ID = pn_user_id
               AND PRIV_ID IN(
                   SELECT UP.PRIV_ID 
                     FROM REPORT.USER_PRIVS UP
                     JOIN REPORT.USER_LOGIN U ON UP.USER_ID = U.USER_ID
                     JOIN REPORT.PRIV P ON UP.PRIV_ID = P.PRIV_ID
                    WHERE UP.USER_ID = pn_updating_user_id 
                      AND (U.USER_TYPE != 8 OR P.INTERNAL_EXTERNAL_FLAG IN('B', 'E')));
            INSERT INTO REPORT.USER_PRIVS(USER_ID, PRIV_ID)
              SELECT pn_user_id, P.PRIV_ID
                FROM REPORT.PRIV P
                JOIN REPORT.USER_PRIVS UP ON UP.PRIV_ID = P.PRIV_ID
                JOIN REPORT.USER_LOGIN U ON UP.USER_ID = U.USER_ID
               WHERE P.PRIV_ID MEMBER OF pt_user_privileges
                 AND UP.USER_ID = pn_updating_user_id 
                 AND (U.USER_TYPE != 8 OR P.INTERNAL_EXTERNAL_FLAG IN('B', 'E'))
                 AND (ln_user_type != 8 OR P.INTERNAL_EXTERNAL_FLAG != 'I');
        END IF;
	END;
    
    PROCEDURE CREATE_CUSTOMER(
        pn_user_id OUT CORP.CUSTOMER.USER_ID%TYPE,
        pn_cust_id OUT CORP.CUSTOMER.CUSTOMER_ID%TYPE,
        pv_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
        pv_first_name REPORT.USER_LOGIN.FIRST_NAME%TYPE,
        pv_last_name REPORT.USER_LOGIN.LAST_NAME%TYPE,
        pv_email REPORT.USER_LOGIN.EMAIL%TYPE,
        pv_time_zone_guid REPORT.USER_LOGIN.TIME_ZONE_GUID%TYPE,
        pb_password_hash REPORT.USER_LOGIN.PASSWORD_HASH%TYPE,
        pb_password_salt REPORT.USER_LOGIN.PASSWORD_SALT%TYPE,
        pv_cust_name IN CORP.CUSTOMER.CUSTOMER_NAME%TYPE,
        pv_addr1 IN CORP.CUSTOMER_ADDR.ADDRESS1%TYPE,
        pv_city IN CORP.CUSTOMER_ADDR.CITY%TYPE,
        pv_state_cd IN CORP.CUSTOMER_ADDR.STATE%TYPE,
        pv_postal IN CORP.CUSTOMER_ADDR.ZIP%TYPE,
        pv_country_cd IN CORP.CUSTOMER_ADDR.COUNTRY_CD%TYPE,
        pv_telephone REPORT.USER_LOGIN.TELEPHONE%TYPE,
        pv_fax REPORT.USER_LOGIN.FAX%TYPE,
        pn_dealer_id IN CORP.CUSTOMER.DEALER_ID%TYPE,
        pv_tax_id_nbr IN CORP.CUSTOMER.TAX_ID_NBR%TYPE,
        pv_doing_business_as IN CORP.CUSTOMER.DOING_BUSINESS_AS%TYPE DEFAULT NULL,
        pv_license_id IN CORP.LICENSE.LICENSE_ID%TYPE DEFAULT NULL,
        pv_parent_customer_id IN CORP.CUSTOMER.PARENT_CUSTOMER_ID%TYPE DEFAULT NULL)
    IS
        ln_addr_id CORP.CUSTOMER_ADDR.ADDRESS_ID%TYPE;
        ln_lic_id CORP.LICENSE_NBR.LICENSE_ID%TYPE;
        ln_dealer_id CORP.DEALER.DEALER_ID%TYPE;
        l_user_privs NUMBER_TABLE;
    BEGIN
        ln_lic_id := pv_license_id;
        ln_dealer_id := pn_dealer_id;
        IF ln_lic_id IS NULL THEN
          SELECT LICENSE_ID
          INTO ln_lic_id
          FROM CORP.VW_DEALER_LICENSE
          WHERE DEALER_ID = ln_dealer_id;
        END IF;
        IF ln_dealer_id IS NULL THEN
          SELECT MAX(DL.DEALER_ID)
          INTO ln_dealer_id
          FROM CORP.VW_DEALER_LICENSE DL
          WHERE DL.LICENSE_ID = ln_lic_id;
        END IF;
        IF ln_lic_id IS NULL THEN
            RAISE_APPLICATION_ERROR(-20305, 'License not found, dealerId=' || ln_dealer_id || ', licenseId=' || ln_lic_id); 
        END IF; 
        IF ln_dealer_id IS NULL THEN
            RAISE_APPLICATION_ERROR(-20306, 'Dealer not found, dealerId=' || ln_dealer_id || ', licenseId=' || ln_lic_id); 
        END IF; 
        SELECT CORP.CUSTOMER_SEQ.NEXTVAL, CORP.CUSTOMER_ADDR_SEQ.NEXTVAL 
          INTO pn_cust_id, ln_addr_id 
          FROM DUAL;
        SELECT PRIV_ID
          BULK COLLECT INTO l_user_privs
        FROM REPORT.PRIV WHERE CUSTOMER_MASTER_USER_DEFAULT='Y';
        pn_user_id := CREATE_USER(8, pv_user_name, pv_first_name, pv_last_name, pv_email, pv_time_zone_guid, 0, pn_cust_id, pv_telephone, pv_fax, pb_password_hash, pb_password_salt, l_user_privs);
        INSERT INTO CORP.CUSTOMER(CUSTOMER_ID, CUSTOMER_NAME, USER_ID, CREATE_BY, DEALER_ID, TAX_ID_NBR, DOING_BUSINESS_AS, PARENT_CUSTOMER_ID)
             VALUES(pn_cust_id, pv_cust_name, pn_user_id, pn_user_id, ln_dealer_id, pv_tax_id_nbr, pv_doing_business_as, pv_parent_customer_id);
        INSERT INTO CORP.CUSTOMER_ADDR(ADDRESS_ID, CUSTOMER_ID, ADDR_TYPE, NAME, ADDRESS1, CITY, STATE, ZIP, COUNTRY_CD)
             VALUES (ln_addr_id, pn_cust_id, 2, pv_first_name || ' ' || pv_last_name, pv_addr1, pv_city, pv_state_cd, pv_postal, pv_country_cd);
        CORP.RECEIVED_CUSTOM_LICENSE(ln_lic_id, pn_cust_id, 0);
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
             RAISE_APPLICATION_ERROR(-20100, 'Could not find license agreement for this dealer');
        WHEN OTHERS THEN
             RAISE;
    END;
    
    PROCEDURE CREATE_CUSTOM_REPORT_LINK(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pv_label WEB_CONTENT.WEB_LINK.WEB_LINK_LABEL%TYPE,
        pv_url WEB_CONTENT.WEB_LINK.WEB_LINK_URL%TYPE,
        pv_desc WEB_CONTENT.WEB_LINK.WEB_LINK_DESC%TYPE,        
        pn_order WEB_CONTENT.WEB_LINK.WEB_LINK_ORDER%TYPE,
        pv_group WEB_CONTENT.WEB_LINK.WEB_LINK_GROUP%TYPE DEFAULT 'User Defined')
    IS
        ln_link_id WEB_CONTENT.WEB_LINK.WEB_LINK_ID%TYPE;
    BEGIN
        SELECT WEB_CONTENT.SEQ_WEB_LINK_ID.NEXTVAL
          INTO ln_link_id
          FROM DUAL;
        INSERT INTO WEB_CONTENT.WEB_LINK(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_ORDER, WEB_LINK_GROUP, WEB_LINK_USAGE)
            VALUES(ln_link_id, pv_label, pv_url, pv_desc, pn_order, pv_group, '-');
        INSERT INTO REPORT.USER_LINK(USER_ID, LINK_ID, INCLUDE, USAGE)
            VALUES(pn_user_id, ln_link_id, 'Y', 'U');
    END;
        
    PROCEDURE DELETE_USER_LINK(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_link_id WEB_CONTENT.WEB_LINK.WEB_LINK_ID%TYPE)
    IS
    BEGIN
        DELETE FROM REPORT.USER_LINK
         WHERE USER_ID = pn_user_id 
           AND LINK_ID = pn_link_id;
        IF SQL%NOTFOUND THEN
            RAISE NO_DATA_FOUND;
        END IF;
        DELETE FROM WEB_CONTENT.WEB_LINK
         WHERE WEB_LINK_ID = pn_link_id
           AND NOT EXISTS(SELECT 1 FROM REPORT.USER_LINK WHERE LINK_ID = pn_link_id);
    END;
    
    FUNCTION GET_USER_PREFERENCE(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_pref_id REPORT.PREFERENCE.PREFERENCE_ID%TYPE)
        RETURN REPORT.USER_PREFERENCE.PREFERENCE_VALUE%TYPE
    IS
        ln_value REPORT.USER_PREFERENCE.PREFERENCE_VALUE%TYPE;
    BEGIN
        SELECT DECODE(UP.PREFERENCE_ID, NULL, P.PREFERENCE_DEFAULT, UP.PREFERENCE_VALUE)
          INTO ln_value
          FROM REPORT.PREFERENCE P
          LEFT OUTER JOIN REPORT.USER_PREFERENCE UP ON P.PREFERENCE_ID = UP.PREFERENCE_ID AND UP.USER_ID = pn_user_id
         WHERE P.PREFERENCE_ID = pn_pref_id;
        RETURN ln_value;
    END;
    
    FUNCTION GET_HEALTH_CODE(
        pn_measured_date DATE,
        pn_min_days NUMBER,
        pn_max_days NUMBER)
        RETURN CHAR
    IS
    BEGIN
        IF pn_measured_date IS NULL THEN
            RETURN 'D'; -- Never accessed
        END IF;
        IF pn_min_days IS NULL OR pn_min_days <= 0 THEN
            RETURN '-'; -- Not measured
        ELSIF pn_measured_date < SYSDATE - pn_max_days THEN
            RETURN 'O'; -- Old
        ELSIF pn_measured_date < SYSDATE - pn_min_days THEN
            RETURN 'Y'; -- Not healthy
        ELSE
            RETURN 'A'; -- Healthy
        END IF;
    END;
    
    FUNCTION GET_HEALTH_CODE_BY_PREF(
        pn_date DATE,
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_pref_id REPORT.PREFERENCE.PREFERENCE_ID%TYPE)
        RETURN CHAR
    IS
        ln_pref_days NUMBER;
    BEGIN
        ln_pref_days := TO_NUMBER_OR_NULL(GET_USER_PREFERENCE(pn_user_id, pn_pref_id));
        RETURN GET_HEALTH_CODE(pn_date, ln_pref_days, 1 + 2 * ln_pref_days);
    END;
    
    PROCEDURE UPSERT_USER_PREFERENCE(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_preference_id REPORT.PREFERENCE.PREFERENCE_ID%TYPE,
        pv_preference_value REPORT.USER_PREFERENCE.PREFERENCE_VALUE%TYPE)
    IS
    BEGIN
        UPDATE REPORT.USER_PREFERENCE 
           SET PREFERENCE_VALUE = pv_preference_value 
         WHERE USER_ID = pn_user_id 
           AND PREFERENCE_ID = pn_preference_id;
        IF SQL%ROWCOUNT < 1 THEN
             BEGIN
                INSERT INTO REPORT.USER_PREFERENCE(
                    USER_ID,
                    PREFERENCE_ID,
                    PREFERENCE_VALUE
                ) VALUES(
                    pn_user_id,
                    pn_preference_id,
                    pv_preference_value
                ); 
            EXCEPTION
                WHEN DUP_VAL_ON_INDEX THEN
                    UPSERT_USER_PREFERENCE(pn_user_id, pn_preference_id, pv_preference_value);
            END;
        END IF;
    END;
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/PSS/PKG_TRAN.psk?rev=1.73
CREATE OR REPLACE PACKAGE PSS.PKG_TRAN IS

PROCEDURE SP_CREATE_REFUND (
    pv_global_trans_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
    pv_orig_global_trans_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
    pn_refund_utc_ts_ms IN NUMBER,
    pn_orig_upload_utc_ts_ms IN NUMBER,
    pn_refund_amt PSS.REFUND.REFUND_AMT%TYPE,
    pn_refund_desc PSS.REFUND.REFUND_DESC%TYPE,
    pn_refund_issue_by PSS.REFUND.REFUND_ISSUE_BY%TYPE,
    pn_refund_type_cd PSS.REFUND.REFUND_TYPE_CD%TYPE,
    pc_entry_method_cd PSS.REFUND.ACCT_ENTRY_METHOD_CD%TYPE,
    pc_already_inserted_flag OUT VARCHAR2,
    pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
    pn_orig_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
    pn_minor_currency_factor OUT PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
    pn_payment_subtype_key_id OUT PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE,
    pv_payment_subtype_class OUT PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE
);

PROCEDURE SP_CLOSE_CONSUMER_ACCT (
	pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
	pv_refund_issue_by PSS.REFUND.REFUND_ISSUE_BY%TYPE,
	pn_refund_amt OUT PSS.REFUND.REFUND_AMT%TYPE,
	pn_eft_credit_amt OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
	pn_doc_id OUT CORP.DOC.DOC_ID%TYPE
);

-- R33+ signature
PROCEDURE sp_create_sale(
    pc_global_event_cd_prefix IN CHAR,
    pv_device_name IN device.device_name%TYPE,
    pv_device_tran_cd IN pss.tran.tran_device_tran_cd%TYPE,
    pn_device_batch_id IN pss.sale.device_batch_id%TYPE,
    pc_sale_type_cd IN pss.sale.sale_type_cd%TYPE,
    pn_sale_utc_ts_ms IN NUMBER,
    pn_sale_utc_offset_min IN pss.sale.sale_utc_offset_min%TYPE,
    pv_tran_device_result_type_cd IN pss.tran.tran_device_result_type_cd%TYPE,
    pn_sale_result_id IN pss.sale.sale_result_id%TYPE,
    pn_sale_amount IN NUMBER,
    pc_receipt_result_cd IN pss.sale.receipt_result_cd%TYPE,
    pc_tran_batch_type_cd IN pss.tran_line_item_batch_type.tran_line_item_batch_type_cd%TYPE,
    pv_hash_type_cd IN pss.sale.hash_type_cd%TYPE,
    pv_tran_line_item_hash IN pss.sale.tran_line_item_hash%TYPE,
    pv_global_session_cd IN VARCHAR2,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_tran_id OUT pss.tran.tran_id%TYPE,
    pv_tran_state_cd OUT pss.tran.tran_state_cd%TYPE,
    pc_void_allowed IN PSS.SALE.VOID_ALLOWED%TYPE DEFAULT 'N'
);

PROCEDURE sp_create_tran_line_item
(
    pn_tran_id IN pss.tran_line_item.tran_id%TYPE,
    pn_host_port_num IN host.host_port_num%TYPE,
    pn_tli_type_id IN pss.tran_line_item.tran_line_item_type_id%TYPE,
    pn_tli_quantity IN pss.tran_line_item.tran_line_item_quantity%TYPE,    
    pn_tli_amount IN NUMBER,
    pn_tli_tax IN NUMBER,
    pv_tli_desc IN pss.tran_line_item.tran_line_item_desc%TYPE,    
    pn_tli_utc_ts_ms IN NUMBER,
    pn_tli_utc_offset_min IN NUMBER,
    pv_tli_position_cd IN pss.tran_line_item.tran_line_item_position_cd%TYPE,
    pc_tran_batch_type_cd IN pss.tran_line_item.tran_line_item_batch_type_cd%TYPE,
    pn_sale_result_id pss.tran_line_item.sale_result_id%TYPE DEFAULT NULL,
    pn_host_position_num host.host_position_num%TYPE DEFAULT 0,
	pn_sale_amount pss.sale.sale_amount%TYPE DEFAULT 0
);

-- R33+ signature
PROCEDURE sp_finalize_sale
(
    pn_tran_id IN pss.tran.tran_id%TYPE,
    pv_global_session_cd IN VARCHAR2,
    pv_tran_device_result_type_cd IN pss.tran.tran_device_result_type_cd%TYPE,
    pn_sale_result_id IN pss.sale.sale_result_id%TYPE,
    pn_sale_amount IN NUMBER,
    pn_sale_tax IN NUMBER,
    pc_tran_batch_type_cd IN pss.tran_line_item.tran_line_item_batch_type_cd%TYPE,
    pn_sale_utc_ts_ms IN NUMBER,
    pn_sale_duration_sec IN NUMBER,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pc_sale_type_cd pss.sale.sale_type_cd%TYPE DEFAULT NULL,
	pc_tran_import_needed OUT VARCHAR2,
	pc_session_update_needed OUT VARCHAR2,
	pc_client_payment_type_cd OUT VARCHAR2,
	pn_minor_currency_factor OUT PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
	pn_tli_count OUT NUMBER
);

PROCEDURE CREATE_REPLENISHMENT(
    pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
    pn_auth_id PSS.AUTH.AUTH_ID%TYPE,
    pn_replenish_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
    pv_tran_device_result_type_cd PSS.TRAN.TRAN_DEVICE_RESULT_TYPE_CD%TYPE,
    pn_device_batch_id PSS.SALE.DEVICE_BATCH_ID%TYPE,
    pc_receipt_result_cd PSS.SALE.RECEIPT_RESULT_CD%TYPE,
    pc_auth_only CHAR,
    pc_tran_state_cd OUT VARCHAR2,
    pc_client_payment_type_cd OUT VARCHAR2,
    pn_replenish_amount OUT PSS.AUTH.AUTH_AMT_APPROVED%TYPE,
    pn_replenish_balance_amount OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE);
    
PROCEDURE UPDATE_AUTHORIZATION(
    pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
    pn_auth_id PSS.AUTH.AUTH_ID%TYPE,
    pv_card_key PSS.AUTH.CARD_KEY%TYPE,
    pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE);

-- R37+ signature
PROCEDURE SP_CREATE_AUTH(
   pv_global_event_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
   pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
   pn_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE,
   pv_device_event_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE,
   pc_invalid_device_event_cd CHAR,
   pn_tran_start_time NUMBER,
   pn_auth_utc_ms NUMBER,
   pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
   pc_entry_method CHAR,
   pv_masked_card_number PSS.TRAN.TRAN_RECEIVED_RAW_ACCT_DATA%TYPE,
   pn_consumer_acct_id PSS.TRAN.CONSUMER_ACCT_ID%TYPE,
   pd_auth_ts PSS.AUTH.AUTH_TS%TYPE,
   pv_authority_resp_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
   pv_authority_resp_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
   pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
   pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
   pt_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
   pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
   pn_trace_number PSS.AUTH.TRACE_NUMBER%TYPE,
   pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
   pn_auth_amt PSS.AUTH.AUTH_AMT%TYPE,
   pn_balance_amt PSS.AUTH.AUTH_BALANCE_AMT%TYPE,
   pn_requested_amt PSS.AUTH.AUTH_AUTHORITY_AMT_RQST%TYPE,
   pn_received_amt PSS.AUTH.AUTH_AUTHORITY_AMT_RCVD%TYPE,
   pn_add_auth_hold_days NUMBER,
   pc_auth_hold_used PSS.AUTH.AUTH_HOLD_USED%TYPE,
   pv_global_session_cd VARCHAR2,
   pc_ignore_dup CHAR,
   pn_auth_action_id PSS.AUTH.AUTH_ACTION_ID%TYPE,
   pn_auth_action_bitmap PSS.AUTH.AUTH_ACTION_BITMAP%TYPE,
   pc_sent_to_device CHAR,
   pc_pass_thru CHAR,
   pv_card_key PSS.AUTH.CARD_KEY%TYPE,
   pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
   pc_tran_state_cd OUT VARCHAR2,
   pc_tran_import_needed OUT VARCHAR2,
   pc_session_update_needed OUT VARCHAR2,
   pv_sale_global_session_cd OUT VARCHAR2,
   pn_sale_session_start_time OUT PSS.SALE.SALE_SESSION_START_TIME%TYPE,
   pc_client_payment_type_cd OUT VARCHAR2,
   pn_sale_amount OUT pss.sale.sale_amount%TYPE,
   pn_tli_count OUT NUMBER,
   pn_auth_id OUT PSS.AUTH.AUTH_ID%TYPE,
   pv_denied_reason_name IN PSS.DENIED_REASON.DENIED_REASON_NAME%TYPE DEFAULT NULL) ;

-- R37 and above
PROCEDURE SP_INSERT_AUTH_STATS(
   pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
   pn_auth_id PSS.AUTH.AUTH_ID%TYPE,
   pn_request_time NUMBER,
   pn_applayer_start_time NUMBER,
   pn_authority_start_time NUMBER,
   pn_authority_end_time NUMBER,
   pn_applayer_end_time NUMBER,
   pn_response_time NUMBER
);

-- R37+ Signature
PROCEDURE PERMIT_CONSUMER_ACCT(
	pn_pos_pta_id PSS.POS_PTA.POS_PTA_ID%TYPE,
	pn_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
	pt_auth_ts PSS.CONSUMER_ACCT.CONSUMER_ACCT_ACTIVATION_TS%TYPE,
	pl_consumer_acct_type_ids NUMBER_TABLE,
	pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
	pn_consumer_id OUT PSS.CONSUMER_ACCT.CONSUMER_ID%TYPE,
    pn_consumer_acct_type_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_TYPE_ID%TYPE,
    pv_consumer_acct_type_label OUT PSS.CONSUMER_ACCT_TYPE.CONSUMER_ACCT_TYPE_LABEL%TYPE,
    pb_consumer_acct_raw_hash OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_RAW_HASH%TYPE, 
    pb_consumer_acct_raw_salt OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_RAW_SALT%TYPE, 
    pb_security_cd_hash OUT PSS.CONSUMER_ACCT.SECURITY_CD_HASH%TYPE, 
    pb_security_cd_salt OUT PSS.CONSUMER_ACCT.SECURITY_CD_SALT%TYPE,
	pn_action_id OUT PSS.PERMISSION_ACTION.ACTION_ID%TYPE,
	pn_action_code OUT NUMBER,
	pn_action_bitmap OUT NUMBER);

    -- R37+ signature
    PROCEDURE SP_CREATE_LOCAL_AUTH_SALE(
       pc_global_event_cd_prefix IN CHAR,
       pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
       pn_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE,
       pv_device_tran_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE,
       pn_sale_utc_ts_ms NUMBER,
       pn_sale_utc_offset_min IN pss.sale.sale_utc_offset_min%TYPE,
       pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
       pc_entry_method CHAR,
       pc_payment_type CHAR,
       pv_track_data PSS.TRAN.TRAN_RECEIVED_RAW_ACCT_DATA%TYPE,
       pn_consumer_acct_id PSS.TRAN.CONSUMER_ACCT_ID%TYPE,
       pd_auth_ts PSS.AUTH.AUTH_TS%TYPE,
       pn_trace_number PSS.AUTH.TRACE_NUMBER%TYPE,
       pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
       pn_auth_amt PSS.AUTH.AUTH_AMT%TYPE,
       pn_device_batch_id IN pss.sale.device_batch_id%TYPE,
       pc_sale_type_cd IN pss.sale.sale_type_cd%TYPE,
       pv_tran_device_result_type_cd IN pss.tran.tran_device_result_type_cd%TYPE,
       pn_sale_result_id IN pss.sale.sale_result_id%TYPE,
       pv_hash_type_cd IN pss.sale.hash_type_cd%TYPE,
       pv_tran_line_item_hash IN pss.sale.tran_line_item_hash%TYPE,
       pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
       pn_result_cd OUT NUMBER,
       pv_error_message OUT VARCHAR2
    );
    
    --R37 and above
    PROCEDURE DEBIT_CONSUMER_ACCT(
	   pn_amount PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
	   pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
	   pc_redelivery_flag CHAR,
       pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
	   pc_auth_result_cd OUT VARCHAR2,
       pn_debitted_amount OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
	   pn_new_balance OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
       pc_auto_replenish_flag OUT VARCHAR2,
       pn_used_cash_back_balance OUT PSS.CONSUMER_ACCT.TOWARD_CASH_BACK_BALANCE%TYPE,
	   pn_cash_back_amount OUT PSS.SALE.SALE_AMOUNT%TYPE);  

    --R37 and above
    PROCEDURE CREDIT_CONSUMER_ACCT(
	   pn_amount PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
	   pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
	   pc_redelivery_flag CHAR,
       pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
	   pc_auth_result_cd OUT VARCHAR2,
       pn_creditted_amount OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
	   pn_new_balance OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE);

    -- For R37 and above
	PROCEDURE CHECK_REPLENISH_CONSUMER_ACCT(
	   pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
	   pv_replenish_device_serial_cd OUT DEVICE.DEVICE_SERIAL_CD%TYPE,
	   pc_auto_replenish_flag OUT VARCHAR2,
	   pn_replenish_id OUT PSS.CONSUMER_ACCT_REPLENISH.CONSUMER_ACCT_REPLENISH_ID%TYPE,
	   pn_replenish_amount OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_AMOUNT%TYPE,
	   pv_replenish_card_key OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_CARD_KEY%TYPE,
	   pn_global_account_id OUT PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
	   pn_consumer_id OUT PSS.CONSUMER_ACCT.CONSUMER_ID%TYPE,
	   pn_replenish_next_master_id OUT NUMBER,
	   pv_replenish_device_name OUT DEVICE.DEVICE_NAME%TYPE,
       pn_max_denied_count PLS_INTEGER DEFAULT -1);
     
   
    PROCEDURE SETUP_REPLENISH(
        pn_replenish_id IN OUT PSS.CONSUMER_ACCT_REPLENISH.CONSUMER_ACCT_REPLENISH_ID%TYPE,
        pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
        pn_replenish_type_id PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_TYPE_ID%TYPE,
        pv_replenish_card_masked PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_CARD_MASKED%TYPE,
        pv_replenish_device_serial_cd IN OUT DEVICE.DEVICE_SERIAL_CD%TYPE,
        pn_replenish_threshhold PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_THRESHHOLD%TYPE,
        pn_replenish_amount IN OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_AMOUNT%TYPE,
        pc_replenish_flag OUT VARCHAR2,
        pn_replenish_next_master_id OUT NUMBER,
        pv_replenish_device_name OUT DEVICE.DEVICE_NAME%TYPE);
        
    
    -- R44 and below
    PROCEDURE UPDATE_PENDING_REPLENISH(
        pv_replenish_device_name DEVICE.DEVICE_NAME%TYPE,
        pn_replenish_id PSS.CONSUMER_ACCT_REPLENISH.CONSUMER_ACCT_REPLENISH_ID%TYPE,
        pn_replenish_next_master_id NUMBER,
        pc_submitted_flag PSS.CONSUMER_ACCT_PEND_REPLENISH.SUBMITTED_FLAG%TYPE,
        pc_initial_replenish_flag CHAR,
        pn_denied_count OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_DENIED_COUNT%TYPE,
        pv_replenish_card_masked OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_CARD_MASKED%TYPE);
    
    -- R45 and above
    PROCEDURE UPDATE_PENDING_REPLENISH(
        pv_replenish_device_name DEVICE.DEVICE_NAME%TYPE,
        pn_replenish_id PSS.CONSUMER_ACCT_REPLENISH.CONSUMER_ACCT_REPLENISH_ID%TYPE,
        pn_replenish_next_master_id NUMBER,
        pc_submitted_flag PSS.CONSUMER_ACCT_PEND_REPLENISH.SUBMITTED_FLAG%TYPE,
        pc_initial_replenish_flag CHAR,
        pd_expDate PSS.CONSUMER_ACCT_REPLENISH.EXPIRATION_DATE%TYPE,
        pn_denied_count OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_DENIED_COUNT%TYPE,
        pv_replenish_card_masked OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_CARD_MASKED%TYPE);
    
    -- R44 Signature
    PROCEDURE LOCK_AUTH_HOLD(
        pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
        pn_ignore_auth_id PSS.AUTH.AUTH_ID%TYPE,
        pn_auth_hold_total OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
        pn_balance OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE);
    
    -- R33 to R43 Signature
    PROCEDURE LOCK_AUTH_HOLD(
     	pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
		pn_ignore_auth_id PSS.AUTH.AUTH_ID%TYPE,
		pn_auth_hold_total OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE);
	
    -- R37+ Signature
    PROCEDURE AUTHORIZE_ISIS_PROMO(
		pn_global_account_id IN OUT PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
		pv_instance PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_INSTANCE%TYPE,
		pv_consumer_acct_cd PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD%TYPE,
		pv_consumer_identifier PSS.CONSUMER.CONSUMER_IDENTIFIER%TYPE,
		pv_currency_cd PSS.CURRENCY.CURRENCY_CD%TYPE,
		pv_auth_result_cd OUT VARCHAR2,
		pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
		pn_consumer_id OUT PSS.CONSUMER.CONSUMER_ID%TYPE,
		pn_trans_to_free_tran OUT NUMBER);
		
	PROCEDURE REFUND_ISIS_PROMO(
		pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
		pn_result_cd OUT NUMBER,
		pv_error_message OUT VARCHAR2);
        
    PROCEDURE GET_OR_CREATE_CONSUMER_ACCT(
		pn_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
		pn_global_account_instance PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_INSTANCE%TYPE,
		pn_pos_pta_id PSS.POS_PTA.POS_PTA_ID%TYPE,
		pv_currency_cd PSS.CONSUMER_ACCT_BASE.CURRENCY_CD%TYPE,
        pd_auth_ts DATE,
        pv_truncated_card_num PSS.CONSUMER_ACCT_BASE.TRUNCATED_CARD_NUMBER%TYPE,
        pn_consumer_acct_id OUT PSS.CONSUMER_ACCT_BASE.CONSUMER_ACCT_ID%TYPE);
    
    PROCEDURE RESOLVE_ACCOUNT_CONFLICT(
        pn_old_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
		pn_new_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
		pn_global_account_instance PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_INSTANCE%TYPE);

    PROCEDURE UPDATE_TRAN_ACCOUNT(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pn_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
        pn_global_account_instance PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_INSTANCE%TYPE);            
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/PSS/PKG_TRAN.pbk?rev=1.242
CREATE OR REPLACE PACKAGE BODY PSS.PKG_TRAN IS
    
FUNCTION sf_find_host_id(
    pn_device_id DEVICE.DEVICE_ID%TYPE,
    pn_host_port_num IN HOST.HOST_PORT_NUM%TYPE,
    pn_host_position_num HOST.HOST_POSITION_NUM%TYPE)
  RETURN HOST.HOST_ID%TYPE
IS
    ln_host_id HOST.HOST_ID%TYPE;
BEGIN
    SELECT MAX(H.HOST_ID)
      INTO ln_host_id
      FROM DEVICE.HOST H
     WHERE H.DEVICE_ID = pn_device_id
       AND H.HOST_PORT_NUM = pn_host_port_num
       AND H.HOST_POSITION_NUM = pn_host_position_num;

    IF ln_host_id IS NULL THEN
        -- Use base host
        SELECT MAX(H.HOST_ID)
          INTO ln_host_id
          FROM DEVICE.HOST H
         WHERE H.DEVICE_ID = pn_device_id
           AND H.HOST_PORT_NUM = 0;
    END IF;
    
    RETURN ln_host_id;
END;

PROCEDURE SP_CREATE_REFUND (
    pv_global_trans_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
    pv_orig_global_trans_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
    pn_refund_utc_ts_ms IN NUMBER,
    pn_orig_upload_utc_ts_ms IN NUMBER,
    pn_refund_amt PSS.REFUND.REFUND_AMT%TYPE,
    pn_refund_desc PSS.REFUND.REFUND_DESC%TYPE,
    pn_refund_issue_by PSS.REFUND.REFUND_ISSUE_BY%TYPE,
    pn_refund_type_cd PSS.REFUND.REFUND_TYPE_CD%TYPE,
    pc_entry_method_cd PSS.REFUND.ACCT_ENTRY_METHOD_CD%TYPE,
    pc_already_inserted_flag OUT VARCHAR2,
    pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
    pn_orig_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
    pn_minor_currency_factor OUT PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
    pn_payment_subtype_key_id OUT PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE,
    pv_payment_subtype_class OUT PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE)
IS
    ld_orig_tran_upload_ts PSS.TRAN.TRAN_UPLOAD_TS%TYPE;
    ld_refund_ts PSS.TRAN.TRAN_START_TS%TYPE;
    lv_last_lock_utc_ts VARCHAR2(128);
    ln_cnt PLS_INTEGER;
    ln_orig_tran_id PSS.TRAN.TRAN_ID%TYPE;
    lv_lock_string VARCHAR2(100);
    ln_start INTEGER;
    ln_end INTEGER;
    ln_consumer_acct_sub_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_SUB_TYPE_ID%TYPE;
    ln_override_trans_type_id PSS.REFUND.OVERRIDE_TRANS_TYPE_ID%TYPE;
BEGIN
    ln_start := INSTR(pv_global_trans_cd, ':', 1, 1) + 1;
    ln_end := INSTR(pv_global_trans_cd, ':', 1, 3);
    IF ln_end <= 0 THEN
        ln_end := LENGTH(pv_global_trans_cd) + 1;
    END IF;
    lv_lock_string := SUBSTR(pv_global_trans_cd, ln_start,  ln_end -  ln_start);
    ld_orig_tran_upload_ts := DBADMIN.UTC_TO_LOCAL_DATE(DBADMIN.MILLIS_TO_TIMESTAMP(pn_orig_upload_utc_ts_ms));
    ld_refund_ts := DBADMIN.UTC_TO_LOCAL_DATE(DBADMIN.MILLIS_TO_TIMESTAMP(pn_refund_utc_ts_ms));
    lv_last_lock_utc_ts := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', lv_lock_string);
    -- check if refund already exists
    BEGIN
        SELECT X.TRAN_ID, c.MINOR_CURRENCY_FACTOR, pta.PAYMENT_SUBTYPE_KEY_ID, PST.PAYMENT_SUBTYPE_CLASS, 'Y'
          INTO pn_tran_id, pn_minor_currency_factor, pn_payment_subtype_key_id, pv_payment_subtype_class, pc_already_inserted_flag
          FROM PSS.TRAN X
          JOIN PSS.POS_PTA PTA ON PTA.POS_PTA_ID = X.POS_PTA_ID
          JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
          JOIN PSS.PAYMENT_SUBTYPE PST ON PTA.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
         WHERE X.TRAN_GLOBAL_TRANS_CD = pv_global_trans_cd;
        RETURN;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            pc_already_inserted_flag := 'N';
    END;
    -- Find original transaction
    BEGIN
        SELECT X.TRAN_ID, PSS.SEQ_TRAN_ID.NEXTVAL, c.MINOR_CURRENCY_FACTOR, pta.PAYMENT_SUBTYPE_KEY_ID, PST.PAYMENT_SUBTYPE_CLASS, CA.CONSUMER_ACCT_SUB_TYPE_ID
          INTO pn_orig_tran_id, pn_tran_id, pn_minor_currency_factor, pn_payment_subtype_key_id, pv_payment_subtype_class, ln_consumer_acct_sub_type_id
          FROM PSS.TRAN X
          JOIN PSS.POS_PTA PTA ON PTA.POS_PTA_ID = X.POS_PTA_ID
          JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
          JOIN PSS.PAYMENT_SUBTYPE PST ON PTA.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
          LEFT OUTER JOIN PSS.CONSUMER_ACCT CA ON X.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
         WHERE X.TRAN_GLOBAL_TRANS_CD = pv_orig_global_trans_cd
           AND X.TRAN_UPLOAD_TS = ld_orig_tran_upload_ts;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
             RAISE_APPLICATION_ERROR(-20381, 'Original Transaction ''' || pv_orig_global_trans_cd || ''', uploaded at ' || TO_CHAR(ld_orig_tran_upload_ts, 'MM/DD/YYYY HH24:MI:SS') || ' not found');
    END;
    
    INSERT INTO PSS.TRAN (
            TRAN_ID,
            PARENT_TRAN_ID,
            TRAN_START_TS,
            TRAN_END_TS,
            TRAN_UPLOAD_TS,
            TRAN_GLOBAL_TRANS_CD,
            TRAN_STATE_CD,
            CONSUMER_ACCT_ID,
            TRAN_DEVICE_TRAN_CD,
            POS_PTA_ID,
            TRAN_DEVICE_RESULT_TYPE_CD,
            TRAN_RECEIVED_RAW_ACCT_DATA,
            PAYMENT_SUBTYPE_KEY_ID,
            PAYMENT_SUBTYPE_CLASS,
            CLIENT_PAYMENT_TYPE_CD,
            DEVICE_NAME
            )
     SELECT pn_tran_id,
            pn_orig_tran_id,
            ld_refund_ts,
            ld_refund_ts,
            NULL, /* Must be NULL so that PSSUpdater will not pick it up */
            pv_global_trans_cd,
            '8',
            O.CONSUMER_ACCT_ID,
            SUBSTR(pv_global_trans_cd, INSTR(pv_global_trans_cd, ':', 1, 2) + 1, LENGTH(pv_global_trans_cd)),
            O.POS_PTA_ID,
            O.TRAN_DEVICE_RESULT_TYPE_CD,
            O.TRAN_RECEIVED_RAW_ACCT_DATA,
            pp.PAYMENT_SUBTYPE_KEY_ID,
            ps.PAYMENT_SUBTYPE_CLASS,
            ps.CLIENT_PAYMENT_TYPE_CD,
            d.DEVICE_NAME
    FROM PSS.TRAN O
    JOIN pss.pos_pta pp ON O.pos_pta_id = pp.pos_pta_id
    JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
    JOIN pss.pos p ON pp.pos_id = p.pos_id
    JOIN device.device d ON p.device_id = d.device_id
    WHERE O.TRAN_ID = pn_orig_tran_id;
    
    IF ln_consumer_acct_sub_type_id IS NOT NULL THEN
        IF ln_consumer_acct_sub_type_id = 2 THEN
            ln_override_trans_type_id := 31;
        ELSE
            ln_override_trans_type_id := 20;
        END IF;
    END IF;
    
    INSERT INTO PSS.REFUND (
            TRAN_ID,
            REFUND_AMT,
            REFUND_DESC,
            REFUND_ISSUE_TS,
            REFUND_ISSUE_BY,
            REFUND_TYPE_CD,
            REFUND_STATE_ID,
            ACCT_ENTRY_METHOD_CD,
            OVERRIDE_TRANS_TYPE_ID
        ) VALUES (
            pn_tran_id,
            -ABS(pn_refund_amt),
            pn_refund_desc,
            ld_refund_ts,
            pn_refund_issue_by,
            pn_refund_type_cd,
            6,
            pc_entry_method_cd,
            ln_override_trans_type_id);
END;

PROCEDURE SP_CLOSE_CONSUMER_ACCT (
    pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
    pv_refund_issue_by PSS.REFUND.REFUND_ISSUE_BY%TYPE,
    pn_refund_amt OUT PSS.REFUND.REFUND_AMT%TYPE,
    pn_eft_credit_amt OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
    pn_doc_id OUT CORP.DOC.DOC_ID%TYPE
)
IS
    ln_consumer_acct_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_TYPE_ID%TYPE;
    ln_consumer_acct_sub_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_SUB_TYPE_ID%TYPE;
    ln_corp_customer_id PSS.CONSUMER_ACCT.CORP_CUSTOMER_ID%TYPE;
    lv_currency_cd PSS.CONSUMER_ACCT.CURRENCY_CD%TYPE;
    lv_consumer_acct_cd PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD%TYPE;
    lv_consumer_acct_identifier PSS.CONSUMER_ACCT.CONSUMER_ACCT_IDENTIFIER%TYPE;
    ln_refund_amt PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE; 
    ln_eft_credit_amt PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE; 
    ln_tran_id PSS.TRAN.TRAN_ID%TYPE;
    ln_orig_tran_id PSS.TRAN.PARENT_TRAN_ID%TYPE;
    ln_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE;
    ld_refund_ts PSS.TRAN.TRAN_START_TS%TYPE := SYSDATE;
    lv_replenish_card_masked PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD%TYPE;
    lv_tran_device_tran_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE;
    ln_ledger_id CORP.LEDGER.LEDGER_ID%TYPE;
BEGIN
    pn_refund_amt := 0;
    pn_eft_credit_amt := 0;
    
    UPDATE PSS.CONSUMER_ACCT
       SET CONSUMER_ACCT_ACTIVE_YN_FLAG = 'N'
     WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id
      RETURNING CONSUMER_ACCT_TYPE_ID, CONSUMER_ACCT_REPLEN_BALANCE, CONSUMER_ACCT_PROMO_BALANCE,
        CORP_CUSTOMER_ID, CURRENCY_CD, CONSUMER_ACCT_CD, CONSUMER_ACCT_IDENTIFIER, CONSUMER_ACCT_SUB_TYPE_ID
      INTO ln_consumer_acct_type_id, ln_refund_amt, ln_eft_credit_amt, ln_corp_customer_id, lv_currency_cd, lv_consumer_acct_cd, lv_consumer_acct_identifier, ln_consumer_acct_sub_type_id;
    
    UPDATE PSS.CONSUMER_ACCT
       SET CONSUMER_ACCT_BALANCE = 0,
           CONSUMER_ACCT_REPLEN_BALANCE = 0,
           CONSUMER_ACCT_PROMO_BALANCE = 0,
           CLOSE_DATE = sysdate
     WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id;
     
    IF ln_consumer_acct_type_id != 3 OR ln_consumer_acct_sub_type_id != 1 THEN
        RETURN;
    END IF;
    
    IF ln_refund_amt > 0 THEN
        --TODO: Handle when tran is no longer in table (b/c of retention policy)
        SELECT LAST_REPLENISH_TRAN_ID, REPLENISH_POS_PTA_ID, REPLENISH_CARD_MASKED
          INTO ln_orig_tran_id, ln_pos_pta_id, lv_replenish_card_masked
          FROM (SELECT LAST_REPLENISH_TRAN_ID, REPLENISH_POS_PTA_ID, REPLENISH_CARD_MASKED
                  FROM PSS.CONSUMER_ACCT_REPLENISH
                 WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id
                 ORDER BY LAST_REPLENISH_TRAN_TS DESC, PRIORITY, LAST_REPLENISH_TRAN_ID DESC)
         WHERE ROWNUM = 1;
     
        SELECT PSS.SEQ_TRAN_ID.NEXTVAL
        INTO ln_tran_id
        FROM DUAL;
        
        lv_tran_device_tran_cd := DBADMIN.DATE_TO_MILLIS(ld_refund_ts) / 1000;
    
        INSERT INTO PSS.TRAN (
                    TRAN_ID,
                    PARENT_TRAN_ID,
                    TRAN_START_TS,
                    TRAN_END_TS,
                    TRAN_UPLOAD_TS,
                    TRAN_GLOBAL_TRANS_CD,
                    TRAN_STATE_CD,
                    TRAN_DEVICE_TRAN_CD,
                    POS_PTA_ID,
                    TRAN_DEVICE_RESULT_TYPE_CD,
                    TRAN_RECEIVED_RAW_ACCT_DATA,
                    PAYMENT_SUBTYPE_KEY_ID,
                    PAYMENT_SUBTYPE_CLASS,
                    CLIENT_PAYMENT_TYPE_CD,
                    DEVICE_NAME
                    )
             SELECT ln_tran_id,
                    ln_orig_tran_id,
                    ld_refund_ts,
                    ld_refund_ts,
                    NULL,
                    'RF:' || d.device_name || ':' || lv_tran_device_tran_cd || ':R1',
                    PKG_CONST.TRAN_STATE__BATCH,
                    lv_tran_device_tran_cd,
                    ln_pos_pta_id,
                    PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_RCPT,
                    lv_replenish_card_masked,
                    pp.PAYMENT_SUBTYPE_KEY_ID,
                    ps.PAYMENT_SUBTYPE_CLASS,
                    ps.CLIENT_PAYMENT_TYPE_CD,
                    d.DEVICE_NAME
            FROM pss.pos_pta pp
            JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
            JOIN pss.pos p ON pp.pos_id = p.pos_id
            JOIN device.device d ON p.device_id = d.device_id
            WHERE pp.pos_pta_id = ln_pos_pta_id;
            
        INSERT INTO PSS.TRAN_LINE_ITEM (
            TRAN_ID,
            TRAN_LINE_ITEM_AMOUNT,
            TRAN_LINE_ITEM_POSITION_CD,
            TRAN_LINE_ITEM_TAX,
            TRAN_LINE_ITEM_TYPE_ID,
            TRAN_LINE_ITEM_QUANTITY,
            TRAN_LINE_ITEM_DESC,
            TRAN_LINE_ITEM_BATCH_TYPE_CD,
            SALE_RESULT_ID,
            APPLY_TO_CONSUMER_ACCT_ID
        ) VALUES (
            ln_tran_id,
            -ln_refund_amt,
            '0',
            0,
            500,
            1,
            'Prepaid account closure',
            PKG_CONST.TRAN_BATCH_TYPE__ACTUAL,
            PKG_CONST.SALE_RES__SUCCESS,
            pn_consumer_acct_id
        );
            
        INSERT INTO PSS.REFUND (
            TRAN_ID,
            REFUND_AMT,
            REFUND_DESC,
            REFUND_ISSUE_TS,
            REFUND_ISSUE_BY,
            REFUND_TYPE_CD,
            REFUND_STATE_ID,
            ACCT_ENTRY_METHOD_CD
        ) VALUES (
            ln_tran_id,
            -ln_refund_amt,
            'Prepaid account closure',
            ld_refund_ts,
            pv_refund_issue_by,
            'G',
            6,
            2);
            
        pn_refund_amt := ln_refund_amt;
    END IF;
    
    IF ln_eft_credit_amt > 0 THEN
        CORP.PAYMENTS_PKG.ADJUSTMENT_INS(ln_corp_customer_id, pv_refund_issue_by, lv_currency_cd,
            'Promo credit for prepaid account closure, card # ' || lv_consumer_acct_cd || ', card ID ' || lv_consumer_acct_identifier,
            ln_eft_credit_amt, pn_doc_id, ln_ledger_id);
        pn_eft_credit_amt := ln_eft_credit_amt;
    END IF;
END;

PROCEDURE PROCESS_ISIS_TRAN (
    pn_tran_id PSS.TRAN.TRAN_ID%TYPE    
)
IS
    ln_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE;
    lv_isis_promo_status_cd PSS.PROMOTION.STATUS_CD%TYPE;
    ln_tran_diff PSS.CONSUMER_PROMOTION.TRAN_COUNT%TYPE;
    lv_tran_info PSS.TRAN.TRAN_INFO%TYPE;
    lv_payment_subtype_class PSS.TRAN.PAYMENT_SUBTYPE_CLASS%TYPE; 
    lv_auth_resp_cd PSS.AUTH.AUTH_RESP_CD%TYPE;
    ln_sale_amount PSS.SALE.SALE_AMOUNT%TYPE;
    ln_isis_promo_pos_pta_id PSS.POS_PTA.POS_PTA_ID%TYPE;
    ld_sysdate DATE := SYSDATE;
BEGIN
    SELECT NVL(MAX(STATUS_CD), 'D')
    INTO lv_isis_promo_status_cd
    FROM PSS.PROMOTION 
    WHERE PROMOTION_ID = 1;
    
    IF lv_isis_promo_status_cd != 'A' THEN
        RETURN;
    END IF;
    
    SELECT MAX(CA.CONSUMER_ID), MAX(S.SALE_AMOUNT), NVL(MAX(T.TRAN_INFO), '-'), NVL(MAX(T.PAYMENT_SUBTYPE_CLASS), '-'), NVL(MAX(A.AUTH_RESP_CD), '-'), NVL(MAX(PP2.POS_PTA_ID), 0)
    INTO ln_consumer_id, ln_sale_amount, lv_tran_info, lv_payment_subtype_class, lv_auth_resp_cd, ln_isis_promo_pos_pta_id
    FROM PSS.TRAN T
    JOIN PSS.SALE S ON T.TRAN_ID = S.TRAN_ID
    JOIN PSS.CONSUMER_ACCT CA ON T.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID   
    JOIN PSS.POS_PTA PP ON T.POS_PTA_ID = PP.POS_PTA_ID
    JOIN PSS.PAYMENT_SUBTYPE PS ON PP.PAYMENT_SUBTYPE_ID = PS.PAYMENT_SUBTYPE_ID
    LEFT OUTER JOIN PSS.AUTH A ON T.TRAN_ID = A.TRAN_ID AND A.AUTH_TYPE_CD = 'N' AND A.AUTH_STATE_ID IN (2, 5)
    LEFT OUTER JOIN (PSS.POS_PTA PP2
        JOIN PSS.PAYMENT_SUBTYPE PS2 ON PP2.PAYMENT_SUBTYPE_ID = PS2.PAYMENT_SUBTYPE_ID AND PS2.PAYMENT_SUBTYPE_CLASS = 'Isis'
    ) ON PP.POS_ID = PP2.POS_ID AND PP2.POS_PTA_ACTIVATION_TS < ld_sysdate AND (PP2.POS_PTA_DEACTIVATION_TS IS NULL OR PP2.POS_PTA_DEACTIVATION_TS > ld_sysdate)
        AND NVL(PP.AUTHORITY_PAYMENT_MASK_ID, PS.AUTHORITY_PAYMENT_MASK_ID) = NVL(PP2.AUTHORITY_PAYMENT_MASK_ID, PS2.AUTHORITY_PAYMENT_MASK_ID)
        AND (T.PAYMENT_SUBTYPE_CLASS = 'Isis' OR PP.POS_PTA_ID != PP2.POS_PTA_ID)
    WHERE T.TRAN_ID = pn_tran_id;
    
    IF ln_consumer_id IS NULL OR ln_sale_amount IS NULL OR lv_tran_info LIKE '%Softcard loyalty updated%' OR ln_isis_promo_pos_pta_id < 1 THEN
        RETURN;
    END IF;
    
    IF ln_sale_amount > 0 THEN
        IF lv_payment_subtype_class != 'Isis' THEN
            LOOP
                UPDATE PSS.CONSUMER_PROMOTION
                SET TRAN_COUNT = TRAN_COUNT + 1
                WHERE CONSUMER_ID = ln_consumer_id AND PROMOTION_ID = 1;
                
                IF SQL%FOUND THEN
                    EXIT;
                END IF;
                
                BEGIN
                    INSERT INTO PSS.CONSUMER_PROMOTION(CONSUMER_ID, PROMOTION_ID, TRAN_COUNT)
                    VALUES(ln_consumer_id, 1, 1);
                    EXIT;
                EXCEPTION
                    WHEN DUP_VAL_ON_INDEX THEN
                        NULL;
                END;
            END LOOP;
        END IF;
    ELSE
        IF lv_payment_subtype_class = 'Isis' AND lv_auth_resp_cd = 'ISIS_PROMO' THEN
            UPDATE PSS.CONSUMER_PROMOTION
            SET TRAN_COUNT = CASE WHEN TRAN_COUNT - 1 >= 0 THEN TRAN_COUNT - 1 ELSE 0 END,
                PROMO_TRAN_COUNT = CASE WHEN PROMO_TRAN_COUNT - 1 >= 0 THEN PROMO_TRAN_COUNT - 1 ELSE 0 END
            WHERE CONSUMER_ID = ln_consumer_id AND PROMOTION_ID = 1;
        END IF;
    END IF;
    
    UPDATE PSS.TRAN
    SET TRAN_INFO = SUBSTR('Softcard loyalty updated' || DECODE(TRAN_INFO, NULL, '', ';' || TRAN_INFO), 1, 1000)
    WHERE TRAN_ID = pn_tran_id AND NVL(TRAN_INFO, '-') NOT LIKE '%Softcard loyalty updated%';
END;

-- R33+ signature
PROCEDURE sp_create_sale(
    pc_global_event_cd_prefix IN CHAR,
    pv_device_name IN device.device_name%TYPE,
    pv_device_tran_cd IN pss.tran.tran_device_tran_cd%TYPE,
    pn_device_batch_id IN pss.sale.device_batch_id%TYPE,
    pc_sale_type_cd IN pss.sale.sale_type_cd%TYPE,
    pn_sale_utc_ts_ms IN NUMBER,
    pn_sale_utc_offset_min IN pss.sale.sale_utc_offset_min%TYPE,
    pv_tran_device_result_type_cd IN pss.tran.tran_device_result_type_cd%TYPE,
    pn_sale_result_id IN pss.sale.sale_result_id%TYPE,
    pn_sale_amount IN NUMBER,
    pc_receipt_result_cd IN pss.sale.receipt_result_cd%TYPE,
    pc_tran_batch_type_cd IN pss.tran_line_item_batch_type.tran_line_item_batch_type_cd%TYPE,
    pv_hash_type_cd IN pss.sale.hash_type_cd%TYPE,
    pv_tran_line_item_hash IN pss.sale.tran_line_item_hash%TYPE,
    pv_global_session_cd IN VARCHAR2,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_tran_id OUT pss.tran.tran_id%TYPE,
    pv_tran_state_cd OUT pss.tran.tran_state_cd%TYPE,
    pc_void_allowed IN PSS.SALE.VOID_ALLOWED%TYPE DEFAULT 'N')
IS
/*
    Returned result codes:
        RESULT__SUCCESS
        RESULT__FAILURE
        RESULT__INVALID_PARAMETER
        RESULT__DUPLICATE
*/
    lv_global_trans_cd pss.tran.tran_global_trans_cd%TYPE;
    ld_tran_upload_ts pss.tran.tran_upload_ts%TYPE;
    lt_sale_start_utc_ts pss.sale.sale_start_utc_ts%TYPE;
    ld_tran_start_ts pss.tran.tran_start_ts%TYPE;
    ld_original_tran_start_ts pss.tran.tran_start_ts%TYPE;
    ld_tran_server_ts DATE;
    ln_device_id device.device_id%TYPE;
    ln_pos_pta_id pss.tran.pos_pta_id%TYPE;
    ld_current_ts DATE := SYSDATE;
    lc_client_payment_type_cd pss.client_payment_type.client_payment_type_cd%TYPE;
    ln_tran_exists NUMBER := PKG_CONST.BOOLEAN__FALSE;
    ln_tli_hash_match NUMBER;
    ln_insert_tran NUMBER := PKG_CONST.BOOLEAN__FALSE;
    ln_original_tran_id pss.tran.tran_id%TYPE;
    ln_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE;
    lc_sale_type_cd pss.sale.sale_type_cd%TYPE;
    lv_last_lock_utc_ts VARCHAR2(128);
    lc_auth_hold_used PSS.TRAN.AUTH_HOLD_USED%TYPE;
    lv_orig_tran_state_cd pss.tran.tran_state_cd%TYPE;
    ln_consumer_acct_id pss.tran.consumer_acct_id%TYPE;
    ln_auth_amt_approved pss.auth.auth_amt_approved%TYPE;
    ln_auth_amt_allowed pss.auth.auth_amt_approved%TYPE;
    ln_sale_over_auth_amt_percent NUMBER;
    lv_email_from_address engine.app_setting.app_setting_value%TYPE;
    lv_email_to_address engine.app_setting.app_setting_value%TYPE;
    lv_error pss.tran.tran_info%TYPE;
    lc_original_void_allowed PSS.SALE.VOID_ALLOWED%TYPE;
    lv_card_key PSS.AUTH.CARD_KEY%TYPE;
    ln_original_sale_amount PSS.SALE.SALE_AMOUNT%TYPE;  
    lc_entry_method_cd PSS.AUTH.ACCT_ENTRY_METHOD_CD%TYPE;
    lc_calc_tran_state_cd CHAR(1) := 'Y';
    ln_host_id PSS.TRAN_LINE_ITEM.HOST_ID%TYPE;
    ln_new_host_count PLS_INTEGER;
    lc_orig_term_capture_flag CHAR(1);
    ln_count PLS_INTEGER;   
    ln_remaining_refund_amt PSS.REFUND.REFUND_AMT%TYPE;
BEGIN
    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
    pn_tran_id := 0;

    IF pc_global_event_cd_prefix NOT IN (PKG_CONST.EVENT_CODE_PREFIX__APP_LAYER,
        PKG_CONST.EVENT_CODE_PREFIX__LEGACY) THEN
        pn_result_cd := PKG_CONST.RESULT__INVALID_PARAMETER;
        pv_error_message := 'Invalid pc_global_event_cd_prefix: ' || pc_global_event_cd_prefix;
        RETURN;
    END IF;

    lv_global_trans_cd := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(pc_global_event_cd_prefix, pv_device_name, pv_device_tran_cd);
    
    lt_sale_start_utc_ts := CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_sale_utc_ts_ms) AS DATE);
    ld_tran_start_ts := CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_sale_utc_ts_ms + pn_sale_utc_offset_min * 60 * 1000) AS DATE);
    ld_tran_server_ts := CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_sale_utc_ts_ms) - SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) + CURRENT_TIMESTAMP AS DATE);

    lv_last_lock_utc_ts := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', pv_device_name || ':' || pv_device_tran_cd);

    BEGIN
        SELECT tran_id, tran_state_cd, tran_start_ts, tran_upload_ts, PKG_CONST.BOOLEAN__TRUE, tli_hash_match, 
               pos_pta_id, sale_type_cd, auth_hold_used, consumer_acct_id, VOID_ALLOWED, 
               SALE_AMOUNT
        INTO pn_tran_id, pv_tran_state_cd, ld_original_tran_start_ts, ld_tran_upload_ts, ln_tran_exists, ln_tli_hash_match, 
             ln_pos_pta_id, lc_sale_type_cd, lc_auth_hold_used, ln_consumer_acct_id, lc_original_void_allowed, 
             ln_original_sale_amount
        FROM
        (
            SELECT /*+ INDEX(t IDX_DEVICE_TRAN_CD) */ t.tran_id, t.tran_state_cd, t.tran_start_ts, t.tran_upload_ts,
                CASE WHEN s.hash_type_cd = pv_hash_type_cd
                    AND s.tran_line_item_hash = pv_tran_line_item_hash
                    AND s.sale_type_cd = pc_sale_type_cd THEN PKG_CONST.BOOLEAN__TRUE
                ELSE PKG_CONST.BOOLEAN__FALSE END AS tli_hash_match,
                t.pos_pta_id, s.sale_type_cd, NVL(t.auth_hold_used, 'N') auth_hold_used, t.consumer_acct_id, 
                s.VOID_ALLOWED, s.SALE_AMOUNT
            FROM pss.tran t
            LEFT OUTER JOIN pss.sale s ON t.tran_id = s.tran_id
            LEFT OUTER JOIN pss.auth aa ON t.tran_id = aa.tran_id AND aa.auth_type_cd = 'N'
            WHERE t.tran_device_tran_cd = pv_device_tran_cd 
              AND (t.tran_global_trans_cd = lv_global_trans_cd OR t.tran_global_trans_cd LIKE lv_global_trans_cd || ':%')
              AND aa.auth_action_id IS NULL
            ORDER BY CASE WHEN s.sale_type_cd = pc_sale_type_cd THEN 1 
                          WHEN s.sale_type_cd IN('A', 'I') AND pc_sale_type_cd IN('A', 'I') THEN 2 
                          WHEN s.sale_type_cd IS NULL AND pc_sale_type_cd IN('A', 'I') THEN 3 
                          ELSE 4 END,
                CASE WHEN t.tran_global_trans_cd = lv_global_trans_cd THEN 1 ELSE 2 END,
                tli_hash_match DESC, t.tran_start_ts, t.created_ts
        )
        WHERE ROWNUM = 1;
    
        ln_original_tran_id := pn_tran_id;
        lv_orig_tran_state_cd := pv_tran_state_cd;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            ln_insert_tran := PKG_CONST.BOOLEAN__TRUE;
    END;

    -- Handle each case
    IF ln_tran_exists = PKG_CONST.BOOLEAN__TRUE THEN
        IF ln_tli_hash_match = PKG_CONST.BOOLEAN__TRUE THEN
            SELECT c.MINOR_CURRENCY_FACTOR
            INTO ln_minor_currency_factor
            FROM PSS.POS_PTA PTA
            JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
            WHERE PTA.POS_PTA_ID = ln_pos_pta_id;          
            IF ln_original_sale_amount * ln_minor_currency_factor = pn_sale_amount THEN
                UPDATE pss.sale
                   SET duplicate_count = duplicate_count + 1
                 WHERE tran_id = pn_tran_id;
        
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__DUPLICATE;
                pn_result_cd := PKG_CONST.RESULT__DUPLICATE;
                pv_error_message := 'Duplicate sale, original tran_id: ' || pn_tran_id;
                RETURN;
            END IF;
        END IF;
        IF pc_sale_type_cd = PKG_CONST.SALE_TYPE__CASH THEN
            lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CASH;
            pv_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE;
            ln_insert_tran := PKG_CONST.BOOLEAN__TRUE;
        ELSIF lc_sale_type_cd = PKG_CONST.SALE_TYPE__CASH THEN
            ln_insert_tran := PKG_CONST.BOOLEAN__TRUE;
            IF pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED THEN
                lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CREDIT;
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND;
            ELSIF pn_sale_result_id != 0 /* Success */ AND NVL(pn_sale_amount, 0) = 0 THEN
                lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CREDIT;
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED;
            ELSE
                lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CREDIT;
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__SALE_NO_AUTH;
            END IF;
        ELSIF lc_sale_type_cd = PKG_CONST.SALE_TYPE__ACTUAL AND pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED THEN
            RETURN; -- ignore this as we have already processd the actual sale
        ELSIF lc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED AND pc_sale_type_cd = PKG_CONST.SALE_TYPE__ACTUAL THEN
            NULL; -- just update the transaction  
        ELSIF lc_sale_type_cd IS NOT NULL THEN 
            lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CREDIT;
            IF NVL(ln_original_sale_amount, 0) = 0 AND pc_void_allowed = 'Y' THEN
                pn_result_cd := PKG_CONST.RESULT__SALE_VOIDED;
                RETURN; -- we have already processed the cancel of this charged sale
            ELSIF (pv_tran_device_result_type_cd IN (PKG_CONST.TRAN_DEV_RES__CANCELLED,
                PKG_CONST.TRAN_DEV_RES__AUTH_FAILURE,
                PKG_CONST.TRAN_DEV_RES__FAILURE,
                PKG_CONST.TRAN_DEV_RES__TIMEOUT) OR pn_sale_amount <= 0) 
                AND lc_original_void_allowed = 'Y' 
                AND NVL(ln_original_sale_amount, 0) > 0
                AND pv_tran_state_cd IN(
                    PKG_CONST.TRAN_STATE__SALE_NO_AUTH, 
                    PKG_CONST.TRAN_STATE__BATCH, 
                    PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND, 
                    PKG_CONST.TRAN_STATE__BATCH_INTENDED,
                    PKG_CONST.TRAN_STATE__PROCESSED_TRAN, 
                    PKG_CONST.TRAN_STATE__PROCESSED_TRAN_INT,
                    PKG_CONST.TRAN_STATE__PROCESSING_TRAN,
                    PKG_CONST.TRAN_STATE__INCOMPLETE,
                    PKG_CONST.TRAN_STATE__INCOMPLETE_ERROR,
                    PKG_CONST.TRAN_STATE__PROCESSING_STTLMT,
                    PKG_CONST.TRAN_STATE__SETTLEMENT,
                    PKG_CONST.TRAN_STATE__COMPLETE,
                    PKG_CONST.TRAN_STATE__STLMT_INCOMPLETE,
                    PKG_CONST.TRAN_STATE__STLMT_ERROR,
                    PKG_CONST.TRAN_STATE__PROCESSING_BATCH,
                    PKG_CONST.TRAN_STATE__PRCSNG_TRAN_RETRY) THEN -- cancel of a charge sale
                
                -- Lock tran row by updating it
                UPDATE PSS.TRAN
                   SET TRAN_STATE_CD = pv_tran_state_cd
                 WHERE TRAN_ID = pn_tran_id
                   AND TRAN_STATE_CD = pv_tran_state_cd;
                IF SQL%NOTFOUND THEN
                    RAISE_APPLICATION_ERROR(-20120, 'Tran State Cd changed while processing tran ' || pn_tran_id || '; please retry');
                END IF;
                -- figure out what to do based on current transaction state
                IF pv_tran_state_cd IN(
                    PKG_CONST.TRAN_STATE__SALE_NO_AUTH, 
                    PKG_CONST.TRAN_STATE__BATCH, 
                    PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND, 
                    PKG_CONST.TRAN_STATE__BATCH_INTENDED) THEN 
                    lc_calc_tran_state_cd := 'N'; -- easy case: update tran and line items and leave state as is
                ELSE
                    IF pv_tran_state_cd IN(PKG_CONST.TRAN_STATE__PROCESSED_TRAN) THEN
                        SELECT MAX(TB.TERMINAL_CAPTURE_FLAG) 
                          INTO lc_orig_term_capture_flag
                          FROM PSS.TERMINAL_BATCH TB
                          JOIN PSS.AUTH A ON A.TERMINAL_BATCH_ID = TB.TERMINAL_BATCH_ID
                         WHERE TB.TERMINAL_BATCH_CLOSE_TS IS NULL 
                          AND A.TRAN_ID = pn_tran_id;
                    END IF;
                    IF pv_tran_state_cd IN(PKG_CONST.TRAN_STATE__PROCESSED_TRAN) AND lc_orig_term_capture_flag = 'Y' THEN
                        IF lc_auth_hold_used = 'Y' THEN
                            UPDATE PSS.AUTH
                               SET AUTH_TYPE_CD = 'C', 
                                   AUTH_AMT = 0 
                             WHERE TRAN_ID = pn_tran_id 
                               AND AUTH_TYPE_CD = 'U';
                            lc_calc_tran_state_cd := 'N'; -- easy case: update tran and line items and leave state as is
                        ELSE --Remove from batch and cancel
                            UPDATE PSS.AUTH A
                               SET TERMINAL_BATCH_ID = NULL
                             WHERE A.TRAN_ID = pn_tran_id
                               AND A.TERMINAL_BATCH_ID IS NOT NULL
                               AND (SELECT TB.TERMINAL_BATCH_CLOSE_TS FROM PSS.TERMINAL_BATCH TB WHERE TB.TERMINAL_BATCH_ID = A.TERMINAL_BATCH_ID) IS NULL;
                            pv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED; 
                            lc_calc_tran_state_cd := 'N';
                        END IF;
                    ELSE -- create a refund and return
                        ln_original_tran_id := pn_tran_id;
                        pv_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH;
                        SELECT NVL(SUM(R.REFUND_AMT), 0) - ABS(ln_original_sale_amount), MAX(T.TRAN_ID)
                          INTO ln_remaining_refund_amt, pn_tran_id 
                          FROM PSS.TRAN T
                          JOIN PSS.REFUND R ON T.TRAN_ID = R.TRAN_ID
                         WHERE T.PARENT_TRAN_ID = ln_original_tran_id;
                        IF ln_remaining_refund_amt < 0 THEN
                            SELECT pss.seq_tran_id.NEXTVAL INTO pn_tran_id FROM DUAL; 
                            INSERT INTO PSS.TRAN (
                                    TRAN_ID,
                                    PARENT_TRAN_ID,
                                    TRAN_START_TS,
                                    TRAN_END_TS,
                                    TRAN_UPLOAD_TS,
                                    TRAN_GLOBAL_TRANS_CD,
                                    TRAN_STATE_CD,
                                    CONSUMER_ACCT_ID,
                                    TRAN_DEVICE_TRAN_CD,
                                    POS_PTA_ID,
                                    TRAN_DEVICE_RESULT_TYPE_CD,
                                    TRAN_RECEIVED_RAW_ACCT_DATA,
                                    PAYMENT_SUBTYPE_KEY_ID,
                                    PAYMENT_SUBTYPE_CLASS,
                                    CLIENT_PAYMENT_TYPE_CD,
                                    DEVICE_NAME)
                             SELECT pn_tran_id,
                                    ln_original_tran_id,
                                    ld_tran_start_ts,
                                    ld_tran_start_ts,
                                    ld_current_ts, /* Must NOT be NULL so that it will be imported */
                                    'RF' || SUBSTR(O.TRAN_GLOBAL_TRANS_CD, INSTR(O.TRAN_GLOBAL_TRANS_CD, ':'), 56) || ':1', 
                                    pv_tran_state_cd,
                                    ln_consumer_acct_id,
                                    pv_device_tran_cd,
                                    O.POS_PTA_ID,
                                    pv_tran_device_result_type_cd,
                                    O.TRAN_RECEIVED_RAW_ACCT_DATA,
                                    pp.PAYMENT_SUBTYPE_KEY_ID,
                                    ps.PAYMENT_SUBTYPE_CLASS,
                                    ps.CLIENT_PAYMENT_TYPE_CD,
                                    d.DEVICE_NAME
                            FROM PSS.TRAN O
                            JOIN pss.pos_pta pp ON O.pos_pta_id = pp.pos_pta_id
                            JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
                            JOIN pss.pos p ON pp.pos_id = p.pos_id
                            JOIN device.device d ON p.device_id = d.device_id
                            WHERE O.TRAN_ID = ln_original_tran_id;
                            SELECT MAX(ACCT_ENTRY_METHOD_CD)
                              INTO lc_entry_method_cd
                              FROM (SELECT ACCT_ENTRY_METHOD_CD
                                      FROM PSS.AUTH
                                     WHERE TRAN_ID = ln_original_tran_id
                                     ORDER BY DECODE(AUTH_TYPE_CD, 'N', 1, 5), AUTH_RESULT_CD DESC, AUTH_TS, AUTH_ID)
                             WHERE ROWNUM = 1;
                            SELECT p.DEVICE_ID
                              INTO ln_device_id
                              FROM PSS.POS_PTA PTA
                              JOIN PSS.POS P ON PTA.POS_ID = P.POS_ID
                             WHERE PTA.POS_PTA_ID = ln_pos_pta_id;
            
                            INSERT INTO PSS.REFUND (
                                    TRAN_ID,
                                    REFUND_AMT,
                                    REFUND_DESC,
                                    REFUND_ISSUE_TS,
                                    REFUND_ISSUE_BY,
                                    REFUND_TYPE_CD,
                                    REFUND_STATE_ID,
                                    ACCT_ENTRY_METHOD_CD
                                ) VALUES (
                                    pn_tran_id,
                                    ln_remaining_refund_amt,
                                    'Void of Charged Sale',
                                    ld_current_ts,
                                    'PSS',
                                    'V',
                                    6,
                                    lc_entry_method_cd);
                            -- Insert line item
                            ln_host_id := sf_find_host_id(ln_device_id, 0, 0);
                            IF ln_host_id IS NULL THEN
                                -- create default hosts
                                pkg_device_configuration.sp_create_default_hosts(ln_device_id, ln_new_host_count, pn_result_cd, pv_error_message);
                                IF pn_result_cd != PKG_CONST.RESULT__SUCCESS THEN
                                    RETURN;
                                END IF;
                                ln_host_id := sf_find_host_id(ln_device_id, 0, 0);
                            END IF;
                            INSERT INTO pss.tran_line_item (
                                tran_line_item_id,
                                tran_id,
                                tran_line_item_amount,
                                tran_line_item_position_cd,
                                tran_line_item_tax,
                                tran_line_item_type_id,
                                tran_line_item_quantity,
                                tran_line_item_desc,
                                host_id,
                                tran_line_item_batch_type_cd,
                                tran_line_item_ts,
                                sale_result_id
                            ) VALUES (
                                PSS.SEQ_TLI_ID.NEXTVAL,
                                pn_tran_id,
                                ln_remaining_refund_amt,
                                NULL,
                                NULL,
                                312, /*Cancellation Adjustment */
                                1,
                                'Void of Charged Sale',
                                ln_host_id,
                                'A',
                                ld_current_ts,
                                0);
                        END IF;       
                        pn_result_cd := PKG_CONST.RESULT__SALE_VOIDED;
                        pv_error_message := 'Refund issued for canceled transaction already in-process or settled, refund tran_id: ' || pn_tran_id || ', original tran_id: ' || ln_original_tran_id;
                        RETURN;
                    END IF;
                END IF;
            ELSE
                SELECT CASE WHEN (lc_auth_hold_used = 'Y' OR NVL(SUM(DECODE(TLI.TRAN_LINE_ITEM_BATCH_TYPE_CD, 'I', TLI.TRAN_LINE_ITEM_AMOUNT * TLI.TRAN_LINE_ITEM_QUANTITY)), 0) > 0) AND NVL(SUM(DECODE(TLI.TRAN_LINE_ITEM_BATCH_TYPE_CD, 'A', TLI.TRAN_LINE_ITEM_AMOUNT * TLI.TRAN_LINE_ITEM_QUANTITY)), 0) = 0 
                            THEN PKG_CONST.TRAN_STATE__COMPLETE_ERROR
                            ELSE PKG_CONST.TRAN_STATE__DUPLICATE
                       END
                  INTO pv_tran_state_cd
                  FROM PSS.TRAN_LINE_ITEM TLI
                 WHERE TLI.TRAN_ID = pn_tran_id;
                ln_insert_tran := PKG_CONST.BOOLEAN__TRUE;          
                IF pv_tran_state_cd = PKG_CONST.TRAN_STATE__DUPLICATE THEN
                    pn_result_cd := PKG_CONST.RESULT__DUPLICATE;
                    pv_error_message := 'Duplicate sale with different line items, original tran_id: ' || ln_original_tran_id;
                END IF;
            END IF;
        ELSE
            NULL; -- just update the transaction  
        END IF;
    END IF;
    
    ln_device_id := PKG_DEVICE_CONFIGURATION.GET_DEVICE_ID_BY_NAME(pv_device_name, ld_tran_server_ts);
    
    IF ln_insert_tran = PKG_CONST.BOOLEAN__TRUE THEN
        IF lc_client_payment_type_cd IS NULL THEN
            IF pc_sale_type_cd = PKG_CONST.SALE_TYPE__CASH THEN
                lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CASH;
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE;
            ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED THEN
                lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CREDIT;
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND;
            ELSIF pn_sale_result_id != 0 /* Success */ AND NVL(pn_sale_amount, 0) = 0 THEN
                lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CREDIT;
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED;
            ELSE
                lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CREDIT;
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__SALE_NO_AUTH;
            END IF;
        END IF;
        SELECT pss.seq_tran_id.NEXTVAL INTO pn_tran_id FROM DUAL;

        IF ln_tran_exists = PKG_CONST.BOOLEAN__TRUE THEN
            lv_global_trans_cd := lv_global_trans_cd || ':' || pn_tran_id;
        END IF;

        IF pc_sale_type_cd = PKG_CONST.SALE_TYPE__CASH THEN
            PKG_POS_PTA.SP_GET_OR_CREATE_POS_PTA(ln_device_id, pv_device_name, lc_client_payment_type_cd, lt_sale_start_utc_ts, pn_result_cd, pv_error_message, ln_pos_pta_id);
        ELSE
            PKG_POS_PTA.SP_GET_OR_CREATE_ERR_POS_PTA(ln_device_id, pv_device_name, lc_client_payment_type_cd, lt_sale_start_utc_ts, pn_result_cd, pv_error_message, ln_pos_pta_id);
        END IF;
        
        SELECT c.MINOR_CURRENCY_FACTOR
        INTO ln_minor_currency_factor
        FROM PSS.POS_PTA PTA
        JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
        WHERE PTA.POS_PTA_ID = ln_pos_pta_id;

        INSERT INTO pss.tran (
            tran_id,
            tran_start_ts,
            tran_end_ts,
            tran_upload_ts,
            tran_state_cd,
            tran_device_tran_cd,
            pos_pta_id,
            tran_global_trans_cd,
            tran_device_result_type_cd,
            payment_subtype_key_id,
            payment_subtype_class,
            client_payment_type_cd,
            device_name
        ) SELECT
            pn_tran_id,
            ld_tran_start_ts,
            ld_tran_start_ts,
            ld_current_ts,
            pv_tran_state_cd,
            pv_device_tran_cd,
            ln_pos_pta_id,
            lv_global_trans_cd,
            pv_tran_device_result_type_cd,
            pp.payment_subtype_key_id,
            ps.payment_subtype_class,
            ps.client_payment_type_cd,
            pv_device_name
        FROM pss.pos_pta pp
        JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
        WHERE pp.pos_pta_id = ln_pos_pta_id;
        IF INSTR(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('MASTER_ID_UPDATE_SALE_TYPES'), pc_sale_type_cd) > 0 AND TO_NUMBER_OR_NULL(pv_device_tran_cd) < DBADMIN.DATE_TO_MILLIS(SYSDATE + 365) / 1000 THEN
            UPDATE DEVICE.DEVICE_SETTING 
               SET DEVICE_SETTING_VALUE = pv_device_tran_cd
             WHERE DEVICE_ID = ln_device_id
               AND DEVICE_SETTING_PARAMETER_CD = '60' -- Master Id
               AND TO_NUMBER_OR_NULL(NVL(DEVICE_SETTING_VALUE, '0')) < TO_NUMBER_OR_NULL(pv_device_tran_cd);
        END IF;
    ELSE -- logic to determine pv_tran_state_cd
        SELECT c.MINOR_CURRENCY_FACTOR
        INTO ln_minor_currency_factor
        FROM PSS.POS_PTA PTA
        JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
        WHERE PTA.POS_PTA_ID = ln_pos_pta_id;
    
        IF lc_calc_tran_state_cd = 'Y' THEN
            IF pv_tran_device_result_type_cd IN (PKG_CONST.TRAN_DEV_RES__CANCELLED,
                    PKG_CONST.TRAN_DEV_RES__AUTH_FAILURE,
                    PKG_CONST.TRAN_DEV_RES__FAILURE,
                    PKG_CONST.TRAN_DEV_RES__TIMEOUT) OR pn_sale_amount <= 0 THEN
                IF pv_tran_state_cd IN (
                        PKG_CONST.TRAN_STATE__AUTH_SUCCESS,
                        PKG_CONST.TRAN_STATE__AUTH_SUCCESS_COND,
                        PKG_CONST.TRAN_STATE__BATCH_INTENDED,
                        PKG_CONST.TRAN_STATE__PROCESSED_TRAN_INT) THEN
                    IF lc_auth_hold_used = 'Y' OR pv_tran_state_cd = PKG_CONST.TRAN_STATE__PROCESSED_TRAN_INT THEN
                        IF pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED THEN
                            pv_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH_INTENDED;
                        ELSE
                            pv_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH;
                        END IF;
                    ELSE
                        pv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED;
                    END IF;
                ELSIF pv_tran_state_cd IN (
                        PKG_CONST.TRAN_STATE__AUTH_EXPIRED,
                        PKG_CONST.TRAN_STATE__AUTH_COND_EXPIRED) THEN
                    pv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED; -- Reversal not available since auth is expired
                ELSIF pv_tran_state_cd IN (
                        PKG_CONST.TRAN_STATE__AUTH_DECLINE,
                        PKG_CONST.TRAN_STATE__AUTH_FAILURE) THEN
                    pv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED; -- We already determined that that no reversal is needed
                ELSIF pv_tran_state_cd IN (
                        PKG_CONST.TRAN_STATE__AUTH_REVERSED,
                        PKG_CONST.TRAN_STATE__AUTH_PEND_RVRSL,
                        PKG_CONST.TRAN_STATE__COMPLETE_ERROR,
                        PKG_CONST.TRAN_STATE__PROCESSED_TRAN,
                        PKG_CONST.TRAN_STATE__PROCESSING_TRAN,
                        PKG_CONST.TRAN_STATE__INCOMPLETE,
                        PKG_CONST.TRAN_STATE__INCOMPLETE_ERROR,
                        PKG_CONST.TRAN_STATE__PROCESSING_STTLMT,
                        PKG_CONST.TRAN_STATE__STLMT_INCOMPLETE,
                        PKG_CONST.TRAN_STATE__STLMT_ERROR) THEN
                   -- don't change it
                   NULL;
                ELSE                
                    pv_error_message := 'Bad tran state for a cancelled cashless sale, tran_id: ' || pn_tran_id || ', tran_state_cd: ' || pv_tran_state_cd;
                    pv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED;
                END IF;
            ELSIF pv_tran_state_cd IN (
                    PKG_CONST.TRAN_STATE__AUTH_EXPIRED,
                    PKG_CONST.TRAN_STATE__AUTH_COND_EXPIRED)
                 OR (pv_tran_state_cd IN (
                    PKG_CONST.TRAN_STATE__AUTH_SUCCESS,
                    PKG_CONST.TRAN_STATE__AUTH_SUCCESS_COND)
                    AND ld_original_tran_start_ts < ld_current_ts - 8) THEN
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE_ERROR;
            ELSIF pv_tran_state_cd IN (
                    PKG_CONST.TRAN_STATE__AUTH_DECLINE,
                    PKG_CONST.TRAN_STATE__AUTH_FAILURE,
                    PKG_CONST.TRAN_STATE__AUTH_REVERSED,
                    PKG_CONST.TRAN_STATE__AUTH_PEND_RVRSL,
                    PKG_CONST.TRAN_STATE__INTENDED_ERROR) THEN
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE_ERROR;
                pv_error_message := 'Received a cashless sale for an unsuccessful auth, tran_id: ' || pn_tran_id || ', tran_state_cd: ' || pv_tran_state_cd;
            ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED
                AND pv_tran_device_result_type_cd IN (
                    PKG_CONST.TRAN_DEV_RES__INCOMPLETE,
                    PKG_CONST.TRAN_DEV_RES__SUCCESS,
                    PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_PRNTR,
                    PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_RCPT,
                    PKG_CONST.TRAN_DEV_RES__SUCCESS_RCPT_ERR) THEN
                IF pv_tran_state_cd IN (PKG_CONST.TRAN_STATE__AUTH_SUCCESS, PKG_CONST.TRAN_STATE__AUTH_SUCCESS_COND) THEN
                    -- normal case
                    pv_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH_INTENDED;
                    -- insert sale record
                ELSE
                    -- sale actual uploaded
                    -- don't change tran_state_cd
                    -- don't update sale record
                    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
                    pv_error_message := 'Actual uploaded before intended';
                    RETURN;
                END IF;
            -- we must let POSM processed cancelled sales too to do auth reversal
            ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__ACTUAL
                AND pv_tran_state_cd IN (
                    PKG_CONST.TRAN_STATE__AUTH_SUCCESS,
                    PKG_CONST.TRAN_STATE__AUTH_SUCCESS_COND,
                    PKG_CONST.TRAN_STATE__PROCESSING_BATCH,
                    PKG_CONST.TRAN_STATE__PRCSNG_BATCH_INTD,
                    PKG_CONST.TRAN_STATE__PRCSNG_BATCH_LOCAL,
                    PKG_CONST.TRAN_STATE__PROCESSED_TRAN_INT,
                    PKG_CONST.TRAN_STATE__BATCH_INTENDED)
                AND pv_tran_device_result_type_cd IN (
                    PKG_CONST.TRAN_DEV_RES__AUTH_FAILURE,
                    PKG_CONST.TRAN_DEV_RES__CANCELLED,
                    PKG_CONST.TRAN_DEV_RES__FAILURE,
                    PKG_CONST.TRAN_DEV_RES__INCOMPLETE,
                    PKG_CONST.TRAN_DEV_RES__SUCCESS,
                    PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_PRNTR,
                    PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_RCPT,
                    PKG_CONST.TRAN_DEV_RES__SUCCESS_RCPT_ERR,
                    PKG_CONST.TRAN_DEV_RES__TIMEOUT) THEN
                IF pv_tran_device_result_type_cd IN (
                    PKG_CONST.TRAN_DEV_RES__SUCCESS,
                    PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_PRNTR,
                    PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_RCPT,
                    PKG_CONST.TRAN_DEV_RES__SUCCESS_RCPT_ERR) THEN

                    SELECT NVL(MAX(auth_amt_approved), 0)
                    INTO ln_auth_amt_approved
                    FROM PSS.AUTH
                    WHERE TRAN_ID = pn_tran_id AND auth_type_cd = 'N' AND auth_state_id IN (2, 5);
                    
                    pv_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH;
                    IF pn_sale_amount > 0 AND ln_auth_amt_approved > 0 AND pn_sale_amount / ln_minor_currency_factor > ln_auth_amt_approved THEN
                        SELECT GREATEST(ln_auth_amt_approved, NVL(NVL(MAX(DBADMIN.TO_NUMBER_OR_NULL(DS_MAA.DEVICE_SETTING_VALUE) / 100), MAX(DBADMIN.TO_NUMBER_OR_NULL(DS_AUTH_AMT.DEVICE_SETTING_VALUE) / DECODE(D.DEVICE_TYPE_ID, 13, 100, 1))), 0))
                        INTO ln_auth_amt_allowed
                        FROM DEVICE.DEVICE D
                        LEFT OUTER JOIN DEVICE.DEVICE_SETTING DS_MAA ON D.DEVICE_ID = DS_MAA.DEVICE_ID AND DS_MAA.DEVICE_SETTING_PARAMETER_CD = 'MAX_AUTH_AMOUNT'
                        LEFT OUTER JOIN DEVICE.DEVICE_SETTING DS_AUTH_AMT ON D.DEVICE_ID = DS_AUTH_AMT.DEVICE_ID
                            AND DS_AUTH_AMT.DEVICE_SETTING_PARAMETER_CD = DECODE(D.DEVICE_TYPE_ID, 13, '1200', 1, '195', 11, 'AUTHORIZATION_AMOUNT', 0, '195')
                        WHERE D.DEVICE_ID = ln_device_id;
                    
                        ln_sale_over_auth_amt_percent := NVL(DBADMIN.TO_NUMBER_OR_NULL(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('ALLOWED_SALE_AMT_OVER_AUTH_AMT_PERCENT')), 100);
                        IF pn_sale_amount / ln_minor_currency_factor > ln_auth_amt_allowed + ln_auth_amt_allowed * ln_sale_over_auth_amt_percent / 100 THEN
                            pv_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE_ERROR;
                            lv_error := 'Error: Sale amount ' || TO_CHAR(pn_sale_amount / ln_minor_currency_factor, 'FM9,999,999,990.00') || ' exceeds allowed auth amount ' || TO_CHAR(ln_auth_amt_allowed, 'FM9,999,999,990.00') || ' by more than ' || ln_sale_over_auth_amt_percent || '%';
                            UPDATE PSS.TRAN
                            SET TRAN_INFO = SUBSTR(TRAN_INFO || DECODE(TRAN_INFO, NULL, '', ', ') || lv_error, 1, 1000)
                            WHERE TRAN_ID = pn_tran_id AND (TRAN_INFO IS NULL OR INSTR(TRAN_INFO, lv_error) = 0);
                            
                            UPDATE DEVICE.DEVICE
                            SET DEVICE_ACTIVE_YN_FLAG = 'N'
                            WHERE DEVICE_NAME = pv_device_name AND DEVICE_ACTIVE_YN_FLAG = 'Y';
                            
                            lv_email_from_address := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EMAIL_FROM_ADDRESS');
                            lv_email_to_address := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EMAIL_TO_ADDRESS_CUSTOMER_SERVICE');
                            INSERT INTO ENGINE.OB_EMAIL_QUEUE(OB_EMAIL_FROM_EMAIL_ADDR, OB_EMAIL_FROM_NAME, OB_EMAIL_TO_EMAIL_ADDR, OB_EMAIL_TO_NAME, OB_EMAIL_SUBJECT, OB_EMAIL_MSG)
                            VALUES(lv_email_from_address, lv_email_from_address, lv_email_to_address, lv_email_to_address, 'Invalid sale amount', lv_error || ', device: ' || pv_device_name || ', transaction ID: ' || pn_tran_id || '. Device has been disabled.');
                        END IF;
                    END IF;
                ELSE
                    pv_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH;
                END IF;
            ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__ACTUAL
                AND pv_tran_state_cd = PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND THEN
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__SALE_NO_AUTH;
            ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__ACTUAL
                AND pv_tran_state_cd = PKG_CONST.TRAN_STATE__PROCESSING_TRAN THEN
                NULL;-- don't change tran_state_cd
            ELSIF pv_tran_state_cd != PKG_CONST.TRAN_STATE__SALE_NO_AUTH THEN
                 pv_error_message := 'Unusual tran state for a cashless sale, tran_id: ' || pn_tran_id || ', tran_state_cd: ' || pv_tran_state_cd;
            END IF;
        END IF;
        
        UPDATE pss.tran
        SET tran_state_cd = DECODE(TRAN_STATE_CD, lv_orig_tran_state_cd, pv_tran_state_cd, TRAN_STATE_CD), -- it might have changed if POSMLayer is processing it
            tran_end_ts = tran_start_ts,
            tran_upload_ts = ld_current_ts,
            tran_device_result_type_cd = pv_tran_device_result_type_cd
        WHERE tran_id = pn_tran_id
        RETURNING client_payment_type_cd INTO lc_client_payment_type_cd;

        SELECT /*+ INDEX(tli IF1_TRAN_LINE_ITEM) */ COUNT(1)
        INTO ln_count
        FROM pss.tran_line_item tli
        WHERE tran_id = pn_tran_id
            AND tran_line_item_batch_type_cd = pc_tran_batch_type_cd;
        
        IF ln_count > 0 THEN
            DELETE /*+ INDEX(tli IF1_TRAN_LINE_ITEM) */ FROM pss.tran_line_item tli
            WHERE tran_id = pn_tran_id
                AND tran_line_item_batch_type_cd = pc_tran_batch_type_cd;
        END IF;
    END IF;    

    UPDATE pss.sale
    SET device_batch_id = pn_device_batch_id,
        sale_type_cd = pc_sale_type_cd,
        sale_start_utc_ts = lt_sale_start_utc_ts,
        sale_end_utc_ts = lt_sale_start_utc_ts,
        sale_utc_offset_min = pn_sale_utc_offset_min,
        sale_result_id = pn_sale_result_id,
        sale_amount = pn_sale_amount / ln_minor_currency_factor,
        receipt_result_cd = pc_receipt_result_cd,
        hash_type_cd = pv_hash_type_cd,
        tran_line_item_hash = pv_tran_line_item_hash,
        sale_global_session_cd = pv_global_session_cd,
        imported = CASE WHEN pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED OR pv_tran_state_cd IN('F', 'G', 'Z') THEN '?' WHEN pn_sale_result_id = 0 THEN 'N' ELSE '-' END,
        VOID_ALLOWED = NVL(pc_void_allowed, 'N')
    WHERE tran_id = pn_tran_id;

    IF SQL%NOTFOUND THEN
        INSERT INTO pss.sale (
            tran_id,
            device_batch_id,
            sale_type_cd,
            sale_start_utc_ts,
            sale_end_utc_ts,
            sale_utc_offset_min,
            sale_result_id,
            sale_amount,
            receipt_result_cd,
            hash_type_cd,
            tran_line_item_hash,
            sale_global_session_cd,
            imported,
            VOID_ALLOWED
        ) VALUES (
            pn_tran_id,
            pn_device_batch_id,
            pc_sale_type_cd,
            lt_sale_start_utc_ts,
            lt_sale_start_utc_ts,
            pn_sale_utc_offset_min,
            pn_sale_result_id,
            pn_sale_amount / ln_minor_currency_factor,
            pc_receipt_result_cd,
            pv_hash_type_cd,
            pv_tran_line_item_hash,
            pv_global_session_cd,
            CASE WHEN pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED OR pv_tran_state_cd IN('F', 'G', 'Z') THEN '?' WHEN pn_sale_result_id = 0 THEN 'N' ELSE '-' END,
            NVL(pc_void_allowed, 'N')
        );
        
        IF lc_client_payment_type_cd IN (PKG_CONST.CLNT_PMNT_TYPE__ISIS_CREDIT, PKG_CONST.CLNT_PMNT_TYPE__ISIS_SPECIAL)
            AND ln_consumer_acct_id > 0 AND pc_sale_type_cd = PKG_CONST.SALE_TYPE__ACTUAL THEN
            PROCESS_ISIS_TRAN(pn_tran_id);
        END IF;
    END IF;
    
    IF pn_sale_result_id != 0 AND ln_consumer_acct_id IS NOT NULL AND lc_auth_hold_used = 'N' THEN
        UPDATE pss.last_device_action
        SET device_action_utc_ts = device_action_utc_ts - INTERVAL '1' YEAR
        WHERE device_name = pv_device_name
            AND consumer_acct_id = ln_consumer_acct_id;
    END IF;
END;

    PROCEDURE ADD_REPLENISH_BONUSES(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pn_apply_to_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
        pn_amount PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
        pd_auth_ts PSS.AUTH.AUTH_TS%TYPE)
    IS
        ln_bonus_amount PSS.SALE.SALE_AMOUNT%TYPE;
        ln_bonus_percent REPORT.CAMPAIGN.DISCOUNT_PERCENT%TYPE;
        ln_bonus_threshhold REPORT.CAMPAIGN.THRESHOLD_AMOUNT%TYPE;
        ln_bonus_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE;
    BEGIN
        SELECT MAX(DISCOUNT_PERCENT), MAX(THRESHOLD_AMOUNT), MAX(CAMPAIGN_ID)
          INTO ln_bonus_percent, ln_bonus_threshhold, ln_bonus_campaign_id
          FROM (SELECT C.DISCOUNT_PERCENT, C.THRESHOLD_AMOUNT, C.CAMPAIGN_ID
          FROM REPORT.CAMPAIGN C
          JOIN PSS.CAMPAIGN_CONSUMER_ACCT CCA on C.CAMPAIGN_ID = CCA.CAMPAIGN_ID
         WHERE CCA.CONSUMER_ACCT_ID = pn_apply_to_consumer_acct_id
           AND pd_auth_ts BETWEEN NVL(C.START_DATE, MIN_DATE) AND NVL(C.END_DATE, MAX_DATE)
           AND C.CAMPAIGN_TYPE_ID = 2 /* Replenish reward - Bonus */
           AND C.DISCOUNT_PERCENT > 0
           AND C.DISCOUNT_PERCENT < 1
           AND (TRIM(C.RECUR_SCHEDULE) IS NULL OR REPORT.MATCH_CAMPAIGN_RECUR_SCHEDULE(C.RECUR_SCHEDULE, pd_auth_ts) = 'Y')
         ORDER BY C.DISCOUNT_PERCENT DESC, C.CAMPAIGN_ID DESC)
         WHERE ROWNUM = 1;
        IF ln_bonus_campaign_id IS NOT NULL AND ln_bonus_percent > 0 THEN
            DECLARE
                lv_currency_cd PSS.CONSUMER_ACCT.CURRENCY_CD%TYPE;
                ln_corp_customer_id PSS.CONSUMER_ACCT.CORP_CUSTOMER_ID%TYPE;
                lv_consumer_acct_cd PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD%TYPE;
                lv_consumer_acct_identifier PSS.CONSUMER_ACCT.CONSUMER_ACCT_IDENTIFIER%TYPE;
                lv_bonus_device_name DEVICE.DEVICE_NAME%TYPE;
                ln_bonus_next_master_id NUMBER;
                ln_result_cd NUMBER;
                lv_error_message VARCHAR2(4000);
                ln_bonus_tran_id PSS.TRAN.TRAN_ID%TYPE;
                lv_bonus_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
                ln_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE;
                lc_currency_symbol CORP.CURRENCY.CURRENCY_SYMBOL%TYPE;
                ln_host_id HOST.HOST_ID%TYPE;
                ld_bonus_ts DATE;
                ld_bonus_time NUMBER;
                lv_desc PSS.TRAN_LINE_ITEM.TRAN_LINE_ITEM_DESC%TYPE;
                ln_doc_id CORP.DOC.DOC_ID%TYPE;
                ln_ledger_id CORP.LEDGER.LEDGER_ID%TYPE; 
                ln_consumer_acct_sub_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_SUB_TYPE_ID%TYPE;
            BEGIN
                IF pn_amount >= ln_bonus_threshhold THEN 
                    ln_bonus_amount := ROUND(pn_amount * ln_bonus_percent, 2);
					UPDATE PSS.CONSUMER_ACCT
                       SET REPLENISH_BONUS_TOTAL = NVL(REPLENISH_BONUS_TOTAL, 0) + ln_bonus_amount,
                           CONSUMER_ACCT_BALANCE = CONSUMER_ACCT_BALANCE + ln_bonus_amount,
                           CONSUMER_ACCT_PROMO_BALANCE = CONSUMER_ACCT_PROMO_BALANCE + ln_bonus_amount,
                           CONSUMER_ACCT_PROMO_TOTAL = CONSUMER_ACCT_PROMO_TOTAL + ln_bonus_amount
                     WHERE CONSUMER_ACCT_ID = pn_apply_to_consumer_acct_id
					 RETURNING CURRENCY_CD, CORP_CUSTOMER_ID, CONSUMER_ACCT_CD, CONSUMER_ACCT_IDENTIFIER, CONSUMER_ACCT_SUB_TYPE_ID
					 INTO lv_currency_cd, ln_corp_customer_id, lv_consumer_acct_cd, lv_consumer_acct_identifier, ln_consumer_acct_sub_type_id;
                    -- add trans to virtual terminal
                    SELECT C.MINOR_CURRENCY_FACTOR, NVL(CC.CURRENCY_SYMBOL, '$'), DBADMIN.TIMESTAMP_TO_MILLIS(SYS_EXTRACT_UTC(SYSTIMESTAMP)), SYSDATE
                      INTO ln_minor_currency_factor, lc_currency_symbol, ld_bonus_time, ld_bonus_ts
                      FROM PSS.CURRENCY C
                      LEFT OUTER JOIN CORP.CURRENCY CC ON C.CURRENCY_CD = CC.CURRENCY_CODE
                     WHERE C.CURRENCY_CD = lv_currency_cd;
                     
                    PKG_DEVICE_CONFIGURATION.NEXT_MASTER_ID_BY_SERIAL('V1-' || CASE WHEN ln_consumer_acct_sub_type_id = 2 THEN ln_corp_customer_id ELSE 1 END || '-' || lv_currency_cd, lv_bonus_device_name, ln_bonus_next_master_id);
                    SP_CREATE_SALE('A', lv_bonus_device_name, ln_bonus_next_master_id,  0, 'C', ld_bonus_time, 
                        DBADMIN.PKG_UTL.TIME_ZONE_DIFF_MIN(PKG_CONST.DB_TIME_ZONE, PKG_CONST.GMT_TIME_ZONE), 'S', 0, ln_bonus_amount * ln_minor_currency_factor, 'U', 'A', 'SHA1', 
                        DBADMIN.HASH_CARD('Replenish Bonus on ' || pn_apply_to_consumer_acct_id || ' of ' || ln_bonus_amount),
                        NULL, ln_result_cd, lv_error_message, ln_bonus_tran_id, lv_bonus_tran_state_cd);
                    IF ln_result_cd != PKG_CONST.RESULT__SUCCESS THEN
                        RAISE_APPLICATION_ERROR(-20118, 'Could not create replenish bonus transaction: ' || lv_error_message);
                    END IF;
                    SELECT HOST_ID, 'Replenish Bonus for ' || lc_currency_symbol || TO_CHAR(pn_amount, 'FM9,999,999.00') || ' in replenishments'
                      INTO ln_host_id, lv_desc
                      FROM (SELECT H.HOST_ID
                              FROM DEVICE.HOST H
                              JOIN DEVICE.DEVICE D ON H.DEVICE_ID = D.DEVICE_ID
                             WHERE D.DEVICE_ACTIVE_YN_FLAG = 'Y'
                               AND H.HOST_PORT_NUM IN(0,1)
                               AND D.DEVICE_NAME = lv_bonus_device_name
                               AND H.HOST_ACTIVE_YN_FLAG = 'Y'
                             ORDER BY H.HOST_PORT_NUM DESC, H.HOST_ID DESC) 
                     WHERE ROWNUM = 1;
                    INSERT INTO PSS.TRAN_LINE_ITEM(
                        TRAN_ID,
                        TRAN_LINE_ITEM_AMOUNT,
                        TRAN_LINE_ITEM_POSITION_CD,
                        TRAN_LINE_ITEM_TAX,
                        TRAN_LINE_ITEM_TYPE_ID,
                        TRAN_LINE_ITEM_QUANTITY,
                        TRAN_LINE_ITEM_DESC,
                        HOST_ID,
                        TRAN_LINE_ITEM_BATCH_TYPE_CD,
                        TRAN_LINE_ITEM_TS,
                        SALE_RESULT_ID,
                        APPLY_TO_CONSUMER_ACCT_ID,
                        CAMPAIGN_ID)
                    SELECT
                        ln_bonus_tran_id,
                        pn_amount,
                        NULL,
                        NULL,
                        555,
                        ln_bonus_percent,
                        lv_desc,
                        ln_host_id,
                        'A',
                        ld_bonus_ts,
                        0,
                        pn_apply_to_consumer_acct_id,
                        ln_bonus_campaign_id
                    FROM DUAL;
                    UPDATE PSS.TRAN
                       SET PARENT_TRAN_ID = pn_tran_id
                     WHERE TRAN_ID = ln_bonus_tran_id;
                    IF ln_consumer_acct_sub_type_id = 1 THEN
                        -- add adjustment to ledger
                        CORP.PAYMENTS_PKG.ADJUSTMENT_INS(ln_corp_customer_id, 'Replenish Bonus Processing', lv_currency_cd, lv_desc || ', card # ' || lv_consumer_acct_cd || ', card ID ' || lv_consumer_acct_identifier,
                            -ln_bonus_amount, ln_doc_id, ln_ledger_id);
                    END IF;
                END IF; 
            END;
        END IF;  
    END;
    
    PROCEDURE FINISH_REPLENISH_SETUP(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pn_apply_to_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
        pv_card_key PSS.AUTH.CARD_KEY%TYPE,
        pd_auth_ts PSS.AUTH.AUTH_TS%TYPE,
        pv_masked_card_number PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_CARD_MASKED%TYPE,
        pn_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE)
    IS
        ln_replenish_id PSS.CONSUMER_ACCT_REPLENISH.CONSUMER_ACCT_REPLENISH_ID%TYPE;
        ln_capr_row_id ROWID;   
        ln_replen_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE;
    BEGIN
        SELECT MAX(CAR.CONSUMER_ACCT_REPLENISH_ID), MAX(CAPR.ROWID)
          INTO ln_replenish_id, ln_capr_row_id
          FROM PSS.CONSUMER_ACCT_REPLENISH CAR 
          JOIN PSS.CONSUMER_ACCT_PEND_REPLENISH CAPR ON CAR.CONSUMER_ACCT_REPLENISH_ID = CAPR.CONSUMER_ACCT_REPLENISH_ID
          JOIN PSS.TRAN X ON CAPR.DEVICE_NAME = X.DEVICE_NAME AND CAPR.DEVICE_TRAN_CD = X.TRAN_DEVICE_TRAN_CD
         WHERE X.TRAN_ID = pn_tran_id
           AND CAR.CONSUMER_ACCT_ID = pn_apply_to_consumer_acct_id;
        IF ln_replenish_id IS NOT NULL THEN
            SELECT CONSUMER_ACCT_ID INTO ln_replen_consumer_acct_id FROM PSS.TRAN WHERE TRAN_ID=pn_tran_id;
            UPDATE PSS.CONSUMER_ACCT_REPLENISH CAR
               SET REPLENISH_CARD_KEY = CASE WHEN pv_card_key IS NOT NULL AND pd_auth_ts > NVL(LAST_REPLENISH_TRAN_TS, MIN_DATE) THEN pv_card_key ELSE REPLENISH_CARD_KEY END,
                   REPLENISH_CARD_MASKED = CASE WHEN pv_card_key IS NOT NULL AND pd_auth_ts > NVL(LAST_REPLENISH_TRAN_TS, MIN_DATE) THEN DBADMIN.MASK_CREDIT_CARD(pv_masked_card_number) ELSE REPLENISH_CARD_MASKED END,                   
                   REPLENISH_POS_PTA_ID = CASE WHEN pv_card_key IS NOT NULL AND pd_auth_ts > NVL(LAST_REPLENISH_TRAN_TS, MIN_DATE) THEN pn_pos_pta_id ELSE REPLENISH_POS_PTA_ID END,
                   LAST_REPLENISH_TRAN_TS = CASE WHEN pv_card_key IS NOT NULL AND pd_auth_ts > NVL(LAST_REPLENISH_TRAN_TS, MIN_DATE) THEN pd_auth_ts ELSE LAST_REPLENISH_TRAN_TS END,
                   LAST_REPLENISH_TRAN_ID = CASE WHEN pv_card_key IS NOT NULL AND pd_auth_ts > NVL(LAST_REPLENISH_TRAN_TS, MIN_DATE) THEN pn_tran_id ELSE LAST_REPLENISH_TRAN_ID END,
                   REPLEN_CONSUMER_ACCT_ID = CASE WHEN pv_card_key IS NOT NULL AND pd_auth_ts > NVL(LAST_REPLENISH_TRAN_TS, MIN_DATE) THEN ln_replen_consumer_acct_id ELSE REPLEN_CONSUMER_ACCT_ID END
              WHERE (CONSUMER_ACCT_ID = pn_apply_to_consumer_acct_id AND CONSUMER_ACCT_REPLENISH_ID = ln_replenish_id)
              OR (CONSUMER_ACCT_ID in 
              (SELECT CONSUMER_ACCT_ID FROM PSS.CONSUMER_ACCT 
              where CONSUMER_ACCT_TYPE_ID=3 and consumer_id=(select consumer_id from pss.consumer_acct where consumer_acct_id=pn_apply_to_consumer_acct_id)) 
             AND STATUS='A'
             AND REPLEN_CONSUMER_ACCT_ID = ln_replen_consumer_acct_id
             AND REPLENISH_CARD_KEY IS NULL);
            DELETE 
              FROM PSS.CONSUMER_ACCT_PEND_REPLENISH 
             WHERE ROWID = ln_capr_row_id
               AND CONSUMER_ACCT_REPLENISH_ID = ln_replenish_id;
        END IF;
    END;

    PROCEDURE REPLENISH_CONSUMER_ACCT(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pv_tli_desc PSS.TRAN_LINE_ITEM.TRAN_LINE_ITEM_DESC%TYPE,
        pn_amount PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
        pn_tli_type_id PSS.TRAN_LINE_ITEM.TRAN_LINE_ITEM_TYPE_ID%TYPE,
        pv_card_key PSS.AUTH.CARD_KEY%TYPE,
        pd_auth_ts PSS.AUTH.AUTH_TS%TYPE,
        pv_masked_card_number PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_CARD_MASKED%TYPE,
        pn_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE,
        pn_apply_to_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE)
    IS
        ln_consumer_acct_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_TYPE_ID%TYPE;
    BEGIN
        UPDATE PSS.CONSUMER_ACCT
           SET CONSUMER_ACCT_BALANCE = CONSUMER_ACCT_BALANCE + pn_amount,
               CONSUMER_ACCT_REPLEN_BALANCE = CONSUMER_ACCT_REPLEN_BALANCE + pn_amount,
               CONSUMER_ACCT_REPLENISH_TOTAL = CONSUMER_ACCT_REPLENISH_TOTAL + pn_amount
         WHERE CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y'
           AND CONSUMER_ACCT_TYPE_ID = 3
           AND CONSUMER_ACCT_CD_HASH = HEXTORAW(pv_tli_desc)
          RETURNING CONSUMER_ACCT_ID, CONSUMER_ACCT_TYPE_ID 
          INTO pn_apply_to_consumer_acct_id, ln_consumer_acct_type_id;
        IF ln_consumer_acct_type_id IN(3) THEN          
            ADD_REPLENISH_BONUSES(
                pn_tran_id,
                pn_apply_to_consumer_acct_id,
                pn_amount,
                pd_auth_ts);
        END IF;
        FINISH_REPLENISH_SETUP(
            pn_tran_id,
            pn_apply_to_consumer_acct_id,
            pv_card_key,
            pd_auth_ts,
            pv_masked_card_number,
            pn_pos_pta_id);
    END;
    
PROCEDURE sp_create_tran_line_item
(
    pn_tran_id IN pss.tran_line_item.tran_id%TYPE,
    pn_host_port_num IN host.host_port_num%TYPE,
    pn_tli_type_id IN pss.tran_line_item.tran_line_item_type_id%TYPE,
    pn_tli_quantity IN pss.tran_line_item.tran_line_item_quantity%TYPE,
    pn_tli_amount IN NUMBER,
    pn_tli_tax IN NUMBER,
    pv_tli_desc IN pss.tran_line_item.tran_line_item_desc%TYPE,
    pn_tli_utc_ts_ms IN NUMBER,
    pn_tli_utc_offset_min IN NUMBER,
    pv_tli_position_cd IN pss.tran_line_item.tran_line_item_position_cd%TYPE,
    pc_tran_batch_type_cd IN pss.tran_line_item.tran_line_item_batch_type_cd%TYPE,
    pn_sale_result_id pss.tran_line_item.sale_result_id%TYPE DEFAULT NULL,
    pn_host_position_num host.host_position_num%TYPE DEFAULT 0,
    pn_sale_amount pss.sale.sale_amount%TYPE DEFAULT 0
)
IS
    ln_host_id pss.tran_line_item.host_id%TYPE;
    ln_device_id host.device_id%TYPE;
    ln_new_host_count NUMBER;
    ln_result_cd NUMBER;
    lv_error_message VARCHAR2(255);
    ln_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE;
    ln_tran_line_item_id pss.tran_line_item.tran_line_item_id%TYPE;
    ln_tli_desc pss.tran_line_item.tran_line_item_desc%TYPE;
    ln_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE;
    lc_tli_type_group_cd PSS.TRAN_LINE_ITEM_TYPE.TRAN_LINE_ITEM_TYPE_GROUP_CD%TYPE;
    lv_device_name DEVICE.DEVICE_NAME%TYPE;
    ln_convenience_fee_amount PSS.SALE.SALE_AMOUNT%TYPE;
    lv_email_from_address engine.app_setting.app_setting_value%TYPE;
    lv_email_to_address engine.app_setting.app_setting_value%TYPE;
    lv_error pss.tran.tran_info%TYPE;
    ln_tli_type_id PSS.TRAN_LINE_ITEM.TRAN_LINE_ITEM_TYPE_ID%TYPE := pn_tli_type_id;
    ln_apply_to_consumer_acct_id PSS.TRAN_LINE_ITEM.APPLY_TO_CONSUMER_ACCT_ID%TYPE;
    lv_card_key PSS.AUTH.CARD_KEY%TYPE;
    ld_auth_ts PSS.AUTH.AUTH_TS%TYPE;
    lv_masked_card_number PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_CARD_MASKED%TYPE;
    ln_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE;
BEGIN
    SELECT POS.DEVICE_ID, c.MINOR_CURRENCY_FACTOR, D.DEVICE_TYPE_ID, D.DEVICE_NAME
      INTO ln_device_id, ln_minor_currency_factor, ln_device_type_id, lv_device_name
      FROM PSS.POS POS
      JOIN DEVICE.DEVICE D ON POS.DEVICE_ID = D.DEVICE_ID
      JOIN PSS.POS_PTA PTA ON POS.POS_ID = PTA.POS_ID
      JOIN PSS.TRAN X ON PTA.POS_PTA_ID = X.POS_PTA_ID
      JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
    WHERE X.TRAN_ID = pn_tran_id;

    ln_host_id := sf_find_host_id(ln_device_id, pn_host_port_num, pn_host_position_num);
    IF ln_host_id IS NULL THEN
        -- create default hosts
        pkg_device_configuration.sp_create_default_hosts(ln_device_id, ln_new_host_count, ln_result_cd, lv_error_message);
        IF ln_result_cd != PKG_CONST.RESULT__SUCCESS THEN
            RETURN;
        END IF;
        ln_host_id := sf_find_host_id(ln_device_id, pn_host_port_num, pn_host_position_num);
    END IF;

    SELECT PSS.SEQ_TLI_ID.NEXTVAL
        INTO ln_tran_line_item_id
        FROM DUAL;

    -- For Kiosk type, use tran_line_item_type to find description
    IF ln_device_type_id = 11 THEN
        SELECT tran_line_item_type_desc || ' ' || SUBSTR(TRIM(pv_tli_desc), 1, 3999 - LENGTH(tran_line_item_type_desc))
          INTO ln_tli_desc
          FROM pss.tran_line_item_type
         WHERE tran_line_item_type_id = ln_tli_type_id;
    ELSIF ln_device_type_id = 5 THEN -- eSuds
        SELECT TLIT.TRAN_LINE_ITEM_TYPE_DESC || ', ' || CASE WHEN DTHT.DEVICE_TYPE_HOST_TYPE_CD IN('S', 'U', 'G', 'H', 'I', 'J') THEN DECODE(H.HOST_POSITION_NUM, 0, 'Bottom ', 1, 'Top ') END
                || GT.HOST_GROUP_TYPE_NAME || ' ' || H.HOST_LABEL_CD
          INTO ln_tli_desc
          FROM PSS.TRAN_LINE_ITEM_TYPE tlit
         CROSS JOIN DEVICE.HOST H
          JOIN DEVICE.DEVICE_TYPE_HOST_TYPE dtht ON DTHT.HOST_TYPE_ID = H.HOST_TYPE_ID AND DTHT.DEVICE_TYPE_ID = 5
          LEFT OUTER JOIN (DEVICE.HOST_TYPE_HOST_GROUP_TYPE HTGT
          JOIN DEVICE.HOST_GROUP_TYPE GT ON HTGT.HOST_GROUP_TYPE_ID = GT.HOST_GROUP_TYPE_ID)
            ON H.HOST_TYPE_ID = HTGT.HOST_TYPE_ID
         WHERE tlit.TRAN_LINE_ITEM_TYPE_ID = ln_tli_type_id
           AND H.HOST_ID = ln_host_id;
    ELSE
        ln_tli_desc := pv_tli_desc;
    END IF;

    INSERT INTO PSS.TRAN_LINE_ITEM (
        TRAN_LINE_ITEM_ID,
        TRAN_ID,
        TRAN_LINE_ITEM_AMOUNT,
        TRAN_LINE_ITEM_POSITION_CD,
        TRAN_LINE_ITEM_TAX,
        TRAN_LINE_ITEM_TYPE_ID,
        TRAN_LINE_ITEM_QUANTITY,
        TRAN_LINE_ITEM_DESC,
        HOST_ID,
        TRAN_LINE_ITEM_BATCH_TYPE_CD,
        TRAN_LINE_ITEM_TS,
        SALE_RESULT_ID,
        APPLY_TO_CONSUMER_ACCT_ID)
    SELECT
        ln_tran_line_item_id,
        pn_tran_id,
        pn_tli_amount * CASE tran_line_item_type_sign_pn
            WHEN 'N' THEN -1
            ELSE 1
        END / ln_minor_currency_factor,
        pv_tli_position_cd,
        pn_tli_tax * CASE tran_line_item_type_sign_pn
            WHEN 'N' THEN -1
            ELSE 1
        END / ln_minor_currency_factor,
        ln_tli_type_id,
        pn_tli_quantity,
        ln_tli_desc,
        ln_host_id,
        pc_tran_batch_type_cd,
        CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_tli_utc_ts_ms + pn_tli_utc_offset_min * 60 * 1000) AS DATE),
        pn_sale_result_id,
        ln_apply_to_consumer_acct_id
    FROM tran_line_item_type
    WHERE tran_line_item_type_id = ln_tli_type_id;

    -- For all device actual batch type only
    IF ln_host_id IS NOT NULL AND pc_tran_batch_type_cd = PKG_CONST.TRAN_BATCH_TYPE__ACTUAL THEN
        SELECT tran_line_item_type_group_cd
        INTO lc_tli_type_group_cd
        FROM pss.tran_line_item_type
        WHERE tran_line_item_type_id = ln_tli_type_id;
    
        IF lc_tli_type_group_cd IN ('P', 'S') THEN
            UPDATE PSS.TRAN_LINE_ITEM_RECENT
            SET tran_line_item_id = ln_tran_line_item_id,
                fkp_tran_id = pn_tran_id
            WHERE host_id = ln_host_id
                AND tran_line_item_type_id = ln_tli_type_id;
                
            IF SQL%NOTFOUND THEN
                BEGIN
                    INSERT INTO PSS.TRAN_LINE_ITEM_RECENT (
                        TRAN_LINE_ITEM_ID,
                        HOST_ID,
                        FKP_TRAN_ID,
                        TRAN_LINE_ITEM_TYPE_ID
                    ) VALUES (
                        ln_tran_line_item_id,
                        ln_host_id,
                        pn_tran_id,
                        ln_tli_type_id
                    );
                EXCEPTION
                    WHEN DUP_VAL_ON_INDEX THEN
                        UPDATE PSS.TRAN_LINE_ITEM_RECENT
                        SET tran_line_item_id = ln_tran_line_item_id,
                            fkp_tran_id = pn_tran_id
                        WHERE host_id = ln_host_id
                            AND tran_line_item_type_id = ln_tli_type_id;
                END;
            END IF;
        END IF;
    END IF;
    
    IF ln_tli_type_id = PKG_CONST.TLI__CONVENIENCE_FEE AND pc_tran_batch_type_cd = PKG_CONST.TRAN_BATCH_TYPE__ACTUAL AND pn_sale_amount > 0 THEN
        ln_convenience_fee_amount := (NVL(pn_tli_amount, 0) + NVL(pn_tli_tax, 0)) * NVL(pn_tli_quantity, 0);
        IF ln_convenience_fee_amount > pn_sale_amount - ln_convenience_fee_amount 
            AND ln_convenience_fee_amount > NVL(DBADMIN.TO_NUMBER_OR_NULL(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('MIN_INVALID_CONVENIENCE_FEE_AMT_PENNIES')), 100) THEN
            UPDATE PSS.TRAN
            SET tran_state_cd = PKG_CONST.TRAN_STATE__COMPLETE_ERROR
            WHERE TRAN_ID = pn_tran_id;

            lv_error := 'Error: Two-Tier Pricing amount ' || TO_CHAR(ln_convenience_fee_amount / ln_minor_currency_factor, 'FM9,999,999,990.00') || ' exceeds sale amount without Two-Tier Pricing ' || TO_CHAR((pn_sale_amount - ln_convenience_fee_amount) / ln_minor_currency_factor, 'FM9,999,999,990.00');
            UPDATE PSS.TRAN
            SET TRAN_INFO = SUBSTR(TRAN_INFO || DECODE(TRAN_INFO, NULL, '', ', ') || lv_error, 1, 1000)
            WHERE TRAN_ID = pn_tran_id AND (TRAN_INFO IS NULL OR INSTR(TRAN_INFO, lv_error) = 0);
            
            UPDATE DEVICE.DEVICE
            SET DEVICE_ACTIVE_YN_FLAG = 'N'
            WHERE DEVICE_NAME = lv_device_name AND DEVICE_ACTIVE_YN_FLAG = 'Y';
            
            lv_email_from_address := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EMAIL_FROM_ADDRESS');
            lv_email_to_address := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EMAIL_TO_ADDRESS_CUSTOMER_SERVICE');
            INSERT INTO ENGINE.OB_EMAIL_QUEUE(OB_EMAIL_FROM_EMAIL_ADDR, OB_EMAIL_FROM_NAME, OB_EMAIL_TO_EMAIL_ADDR, OB_EMAIL_TO_NAME, OB_EMAIL_SUBJECT, OB_EMAIL_MSG)
            VALUES(lv_email_from_address, lv_email_from_address, lv_email_to_address, lv_email_to_address, 'Invalid Two-Tier Pricing amount', lv_error || ', device: ' || lv_device_name || ', transaction ID: ' || pn_tran_id || '. Device has been disabled.');       
        END IF;
    END IF;
END;

-- R33+ signature
PROCEDURE sp_finalize_sale
(
    pn_tran_id IN pss.tran.tran_id%TYPE,
    pv_global_session_cd IN VARCHAR2,
    pv_tran_device_result_type_cd IN pss.tran.tran_device_result_type_cd%TYPE,
    pn_sale_result_id IN pss.sale.sale_result_id%TYPE,
    pn_sale_amount IN NUMBER,
    pn_sale_tax IN NUMBER,
    pc_tran_batch_type_cd IN pss.tran_line_item.tran_line_item_batch_type_cd%TYPE,
    pn_sale_utc_ts_ms IN NUMBER,
    pn_sale_duration_sec IN NUMBER,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pc_sale_type_cd pss.sale.sale_type_cd%TYPE DEFAULT NULL,
    pc_tran_import_needed OUT VARCHAR2,
    pc_session_update_needed OUT VARCHAR2,
    pc_client_payment_type_cd OUT VARCHAR2,
    pn_minor_currency_factor OUT PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
    pn_tli_count OUT NUMBER
)
IS
/*
    Returned result codes:
        RESULT__SUCCESS
        RESULT__FAILURE
        RESULT__HOST_NOT_FOUND
*/
    ln_tli_total pss.tran_line_item.tran_line_item_amount%TYPE;
    ln_adj_amt pss.tran_line_item.tran_line_item_amount%TYPE;
    ln_adj_tli pss.tran_line_item.tran_line_item_type_id%TYPE := -1;
    ln_base_host_id pss.tran_line_item.host_id%TYPE;
    lc_tli_batch_type_cd pss.tran_line_item.tran_line_item_batch_type_cd%TYPE;
    ln_new_host_count NUMBER;
    ln_device_id DEVICE.DEVICE_ID%TYPE;
    ln_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE;
    lc_tran_state_cd pss.tran.tran_state_cd%TYPE;
    ln_discount_percent report.campaign.discount_percent%TYPE;
    ln_discount_amount NUMBER;
    ln_sale_amount NUMBER := NVL(pn_sale_amount, 0);
	ln_original_sale_amount NUMBER := ln_sale_amount;
    ln_pos_pta_id pss.tran.pos_pta_id%TYPE;
    ld_tran_start_ts pss.tran.tran_start_ts%TYPE;
    lv_device_tran_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE;
    lv_device_name DEVICE.DEVICE_NAME%TYPE;
	lv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE;
    ln_consumer_acct_id PSS.TRAN.CONSUMER_ACCT_ID%TYPE;
    ln_campaign_id PSS.TRAN_LINE_ITEM.CAMPAIGN_ID%TYPE;
    lc_backoffice_virtual_flag CHAR(1);
BEGIN
    pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;
    pc_session_update_needed := 'N';
    
    IF pn_sale_duration_sec > 0 AND pc_tran_batch_type_cd = PKG_CONST.TRAN_BATCH_TYPE__ACTUAL THEN
        UPDATE pss.tran
        SET tran_end_ts = tran_start_ts + pn_sale_duration_sec / 86400
        WHERE tran_id = pn_tran_id;

        UPDATE pss.sale
        SET sale_end_utc_ts = sale_start_utc_ts + pn_sale_duration_sec / 86400
        WHERE tran_id = pn_tran_id;
    END IF;    

    SELECT POS.DEVICE_ID, X.POS_PTA_ID, c.MINOR_CURRENCY_FACTOR, PST.CLIENT_PAYMENT_TYPE_CD, D.DEVICE_TYPE_ID, DECODE(S.IMPORTED, 'N', 'Y', NULL, 'Y', 'N'), X.TRAN_STATE_CD, 
           X.TRAN_START_TS, X.TRAN_DEVICE_TRAN_CD, D.DEVICE_NAME, X.CONSUMER_ACCT_ID, CASE WHEN D.DEVICE_TYPE_ID = 14 AND D.DEVICE_SUB_TYPE_ID IN(3, 4) THEN 'Y' ELSE 'N' END, D.DEVICE_SERIAL_CD
      INTO ln_device_id, ln_pos_pta_id, pn_minor_currency_factor, pc_client_payment_type_cd, ln_device_type_id, pc_tran_import_needed, lc_tran_state_cd, 
           ld_tran_start_ts, lv_device_tran_cd, lv_device_name, ln_consumer_acct_id, lc_backoffice_virtual_flag, lv_device_serial_cd
      FROM PSS.POS POS
      JOIN DEVICE.DEVICE D ON POS.DEVICE_ID = D.DEVICE_ID
      JOIN PSS.POS_PTA PTA ON POS.POS_ID = PTA.POS_ID
      JOIN PSS.TRAN X ON PTA.POS_PTA_ID = X.POS_PTA_ID
      JOIN PSS.SALE S ON X.TRAN_ID = S.TRAN_ID
      JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
      JOIN PSS.PAYMENT_SUBTYPE PST ON PTA.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
      JOIN LOCATION.LOCATION L ON POS.LOCATION_ID = L.LOCATION_ID
      JOIN LOCATION.TIME_ZONE TZ ON L.LOCATION_TIME_ZONE_CD = TZ.TIME_ZONE_CD
    WHERE X.TRAN_ID = pn_tran_id;
    
    -- use the base host for adjustments
    SELECT MAX(H.HOST_ID)
    INTO ln_base_host_id
    FROM DEVICE.HOST H
    WHERE H.DEVICE_ID = ln_device_id
    AND H.HOST_PORT_NUM = 0;
    IF ln_base_host_id IS NULL THEN
        pkg_device_configuration.sp_create_default_hosts(ln_device_id, ln_new_host_count, pn_result_cd, pv_error_message);
        IF pn_result_cd != PKG_CONST.RESULT__SUCCESS THEN
            RETURN;
        END IF;
        SELECT H.HOST_ID
        INTO ln_base_host_id
        FROM DEVICE.HOST H
        WHERE H.DEVICE_ID = ln_device_id
        AND H.HOST_PORT_NUM = 0;
    END IF;
	
	-- Softcard Discount promotion
	IF pc_client_payment_type_cd IN (PKG_CONST.CLNT_PMNT_TYPE__ISIS_CREDIT, PKG_CONST.CLNT_PMNT_TYPE__ISIS_SPECIAL)
		AND ln_sale_amount > 0 AND REGEXP_LIKE(lv_device_serial_cd, DBADMIN.PKG_GLOBAL.GET_APP_SETTING('SOFTCARD_DISCOUNT_PROMO_SERIAL_CD_REGEX')) THEN
		ln_discount_percent := DBADMIN.TO_NUMBER_OR_NULL(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('SOFTCARD_DISCOUNT_PROMO_PERCENT'));
	
		IF ln_discount_percent IS NOT NULL THEN
			ln_discount_amount := ROUND(ln_sale_amount * ln_discount_percent);
			IF ln_discount_amount > 0 THEN
				IF ln_discount_amount > ln_sale_amount THEN
					ln_discount_amount := ln_sale_amount;
				END IF;
				ln_sale_amount := ln_sale_amount - ln_discount_amount;
				
				-- create Softcard Discount line item
				INSERT INTO PSS.TRAN_LINE_ITEM (
					TRAN_ID,
					HOST_ID,
					TRAN_LINE_ITEM_TYPE_ID,
					TRAN_LINE_ITEM_AMOUNT,
					TRAN_LINE_ITEM_QUANTITY,
					TRAN_LINE_ITEM_DESC,
					TRAN_LINE_ITEM_BATCH_TYPE_CD,
					TRAN_LINE_ITEM_TAX)
				SELECT
					pn_tran_id,
					ln_base_host_id,
					tran_line_item_type_id,
					-ln_discount_amount / pn_minor_currency_factor,
					1,
					tran_line_item_type_desc || ' ' || ln_discount_percent * 100 || '%',
					pc_tran_batch_type_cd,
					0
				FROM pss.tran_line_item_type
				WHERE tran_line_item_type_id = 208;
			END IF;
		END IF;	
	END IF;
	
	-- don't apply multiple discounts
	IF ln_sale_amount > 0 AND ln_sale_amount = ln_original_sale_amount THEN
		SELECT MAX(DISCOUNT_PERCENT), MAX(CAMPAIGN_ID)
		  INTO ln_discount_percent, ln_campaign_id
		  FROM (
		SELECT C.DISCOUNT_PERCENT, CPP.PRIORITY, C.CAMPAIGN_ID
		  FROM PSS.CAMPAIGN_POS_PTA CPP
		  JOIN REPORT.CAMPAIGN C ON CPP.CAMPAIGN_ID = C.CAMPAIGN_ID
		  JOIN PSS.CAMPAIGN_CONSUMER_ACCT CCA on CPP.CAMPAIGN_ID = CCA.CAMPAIGN_ID
		 WHERE CCA.CONSUMER_ACCT_ID = ln_consumer_acct_id
		   AND CPP.POS_PTA_ID = ln_pos_pta_id
		   AND ld_tran_start_ts BETWEEN NVL(CPP.START_DATE, MIN_DATE) AND NVL(CPP.END_DATE, MAX_DATE)
		   AND C.CAMPAIGN_TYPE_ID = 1
		   AND C.DISCOUNT_PERCENT > 0
		   AND C.DISCOUNT_PERCENT < 1
		   AND (TRIM(C.RECUR_SCHEDULE) IS NULL OR REPORT.MATCH_CAMPAIGN_RECUR_SCHEDULE(C.RECUR_SCHEDULE, ld_tran_start_ts) = 'Y')
		 ORDER BY CPP.PRIORITY, C.DISCOUNT_PERCENT DESC, C.CAMPAIGN_ID DESC)
		 WHERE ROWNUM = 1 ;
		
		IF ln_discount_percent IS NOT NULL THEN
			ln_discount_amount := ROUND(ln_sale_amount * ln_discount_percent);
			IF ln_discount_amount > 0 THEN
				IF ln_discount_amount > ln_sale_amount THEN
					ln_discount_amount := ln_sale_amount;
				END IF;
				ln_sale_amount := ln_sale_amount - ln_discount_amount;
				
				-- create Loyalty Discount line item
				INSERT INTO PSS.TRAN_LINE_ITEM (
					TRAN_ID,
					HOST_ID,
					TRAN_LINE_ITEM_TYPE_ID,
					TRAN_LINE_ITEM_AMOUNT,
					TRAN_LINE_ITEM_QUANTITY,
					TRAN_LINE_ITEM_DESC,
					TRAN_LINE_ITEM_BATCH_TYPE_CD,
					TRAN_LINE_ITEM_TAX,
					CAMPAIGN_ID)
				SELECT
					pn_tran_id,
					ln_base_host_id,
					tran_line_item_type_id,
					-ln_discount_amount / pn_minor_currency_factor,
					1,
					tran_line_item_type_desc || ' ' || ln_discount_percent * 100 || '%',
					pc_tran_batch_type_cd,
					0,
					ln_campaign_id
				FROM pss.tran_line_item_type
				WHERE tran_line_item_type_id = 204;
			END IF;
		END IF;
	END IF;
	
	IF ln_original_sale_amount != ln_sale_amount THEN
		UPDATE pss.sale
		SET sale_amount = ln_sale_amount / pn_minor_currency_factor
		WHERE tran_id = pn_tran_id;
	END IF;
    
    SELECT /*+ INDEX(TLI IF1_TRAN_LINE_ITEM) */ NVL(SUM((NVL(TRAN_LINE_ITEM_AMOUNT, 0) + NVL(TRAN_LINE_ITEM_TAX, 0)) * NVL(TRAN_LINE_ITEM_QUANTITY, 0)), 0),
           COUNT(1)
      INTO ln_tli_total, pn_tli_count
      FROM PSS.TRAN_LINE_ITEM TLI
     WHERE TRAN_ID = pn_tran_id
       AND TRAN_LINE_ITEM_BATCH_TYPE_CD = pc_tran_batch_type_cd;

    IF pv_tran_device_result_type_cd IN (PKG_CONST.TRAN_DEV_RES__CANCELLED,
            PKG_CONST.TRAN_DEV_RES__AUTH_FAILURE,
            PKG_CONST.TRAN_DEV_RES__FAILURE,
            PKG_CONST.TRAN_DEV_RES__TIMEOUT)
        OR NVL(pn_sale_result_id, -1) != PKG_CONST.SALE_RES__SUCCESS
        OR ln_sale_amount = 0 THEN
        IF ln_tli_total != 0 THEN
            ln_adj_tli := PKG_CONST.TLI__CANCELLATION_ADJMT;
            ln_adj_amt := -ln_tli_total;
        END IF;
    ELSE
        ln_adj_amt := ln_sale_amount / pn_minor_currency_factor - ln_tli_total;
        IF ln_adj_amt > 0 THEN
            ln_adj_tli := PKG_CONST.TLI__POS_DISCREPANCY_ADJMT;
        ELSIF ln_adj_amt < 0 THEN
            ln_adj_tli := PKG_CONST.TLI__NEG_DISCREPANCY_ADJMT;
        END IF;
    END IF;
    
    IF ln_device_type_id IN (0, 1) AND pc_sale_type_cd != PKG_CONST.SALE_TYPE__CASH THEN
        IF pn_tli_count > 0 AND ln_adj_amt != 0 THEN
            IF ln_adj_amt > 0 THEN
                -- create Transaction Amount Summary record
                INSERT INTO pss.tran_line_item (
                    tran_id,
                    host_id,
                    tran_line_item_type_id,
                    tran_line_item_amount,
                    tran_line_item_quantity,
                    tran_line_item_desc,
                    tran_line_item_batch_type_cd,
                    tran_line_item_tax)
                VALUES(
                    pn_tran_id,
                    ln_base_host_id,
                    201,
                    ln_adj_amt,
                    1,
                    'Transaction Amount Summary',
                    pc_tran_batch_type_cd,
                    pn_sale_tax);
            ELSE
                INSERT INTO pss.tran_line_item (
                    tran_id,
                    host_id,
                    tran_line_item_type_id,
                    tran_line_item_amount,
                    tran_line_item_quantity,
                    tran_line_item_desc,
                    tran_line_item_batch_type_cd,
                    tran_line_item_tax)
                SELECT
                    pn_tran_id,
                    ln_base_host_id,
                    tran_line_item_type_id,
                    ln_adj_amt,
                    1,
                    tran_line_item_type_desc,
                    pc_tran_batch_type_cd,
                    pn_sale_tax
                FROM pss.tran_line_item_type
                WHERE tran_line_item_type_id = PKG_CONST.TLI__NEG_DISCREPANCY_ADJMT;
            END IF;
        END IF;
   ELSE
        IF ln_adj_tli > -1 THEN
            INSERT INTO pss.tran_line_item (
                tran_id,
                host_id,
                tran_line_item_type_id,
                tran_line_item_amount,
                tran_line_item_quantity,
                tran_line_item_desc,
                tran_line_item_batch_type_cd,
                tran_line_item_tax)
            SELECT
                pn_tran_id,
                ln_base_host_id,
                ln_adj_tli,
                ln_adj_amt,
                1,
                tran_line_item_type_desc,
                pc_tran_batch_type_cd,
                pn_sale_tax
            FROM pss.tran_line_item_type
            WHERE tran_line_item_type_id = ln_adj_tli;
        END IF;            
    END IF;

    IF pn_tli_count = 0 AND pc_tran_import_needed = 'Y' THEN
        UPDATE PSS.SALE
           SET IMPORTED = '-'
         WHERE TRAN_ID = pn_tran_id
           AND IMPORTED NOT IN('-', 'Y');
    END IF;
    
    IF lc_tran_state_cd NOT IN (PKG_CONST.TRAN_STATE__SALE_NO_AUTH, PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND, PKG_CONST.TRAN_STATE__SALE_NO_AUTH_ERROR) THEN
        --if backoffice virtual device, update PAYOR.LAST_CHARGE_*
        IF pn_sale_amount > 0 AND ln_consumer_acct_id IS NOT NULL AND lc_backoffice_virtual_flag = 'Y' THEN
            UPDATE CORP.PAYOR
               SET LAST_CHARGE_AMOUNT = ln_sale_amount / pn_minor_currency_factor, LAST_CHARGE_TS = ld_tran_start_ts
             WHERE PAY_TO_DEVICE_ID = ln_device_id
               AND GLOBAL_ACCOUNT_ID = (SELECT GLOBAL_ACCOUNT_ID FROM PSS.CONSUMER_ACCT_BASE WHERE CONSUMER_ACCT_ID = ln_consumer_acct_id)
               AND NVL(LAST_CHARGE_TS, MIN_DATE) < ld_tran_start_ts;
        END IF;                                
        pc_session_update_needed := 'Y';
    END IF;

    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;

PROCEDURE CREATE_REPLENISHMENT(
    pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
    pn_auth_id PSS.AUTH.AUTH_ID%TYPE,
    pn_replenish_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
    pv_tran_device_result_type_cd PSS.TRAN.TRAN_DEVICE_RESULT_TYPE_CD%TYPE,
    pn_device_batch_id PSS.SALE.DEVICE_BATCH_ID%TYPE,
    pc_receipt_result_cd PSS.SALE.RECEIPT_RESULT_CD%TYPE,
    pc_auth_only CHAR,
    pc_tran_state_cd OUT VARCHAR2,
    pc_client_payment_type_cd OUT VARCHAR2,
    pn_replenish_amount OUT PSS.AUTH.AUTH_AMT_APPROVED%TYPE,
    pn_replenish_balance_amount OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE)
IS
    ln_consumer_acct_id PSS.TRAN.CONSUMER_ACCT_ID%TYPE;
    ln_host_id PSS.TRAN_LINE_ITEM.HOST_ID%TYPE;
    ln_tran_line_item_id PSS.TRAN_LINE_ITEM.TRAN_LINE_ITEM_ID%TYPE;
    ln_result_cd NUMBER;
    ln_new_host_count NUMBER;
    lv_error_message VARCHAR2(4000);
    ln_device_id PSS.POS.DEVICE_ID%TYPE;
    ln_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE;
    ld_sale_utc_ts PSS.SALE.SALE_START_UTC_TS%TYPE;
    ld_sale_local_date PSS.TRAN.TRAN_START_TS%TYPE;
    lv_global_session_cd PSS.SALE.SALE_GLOBAL_SESSION_CD%TYPE;
    lv_device_name PSS.TRAN.DEVICE_NAME%TYPE;
    lv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE;
    lv_device_tran_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE;
    ld_auth_ts PSS.AUTH.AUTH_TS%TYPE;
    ln_consumer_acct_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_TYPE_ID%TYPE;
    ln_consumer_acct_sub_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_SUB_TYPE_ID%TYPE;
    lc_payment_type_cd PSS.CLIENT_PAYMENT_TYPE.PAYMENT_TYPE_CD%TYPE;
    lc_auth_hold_used PSS.AUTH.AUTH_HOLD_USED%TYPE;
    lc_is_cash CHAR(1);
    ln_corp_customer_id PSS.CONSUMER_ACCT.CORP_CUSTOMER_ID%TYPE;
    ln_consumer_acct_identifier PSS.CONSUMER_ACCT.CONSUMER_ACCT_IDENTIFIER%TYPE;                       
    lv_currency_cd PSS.POS_PTA.CURRENCY_CD%TYPE;
    ln_doc_id CORP.DOC.DOC_ID%TYPE;
    ln_ledger_id CORP.LEDGER.LEDGER_ID%TYPE;
    ln_trans_type_id PSS.PAYMENT_SUBTYPE.TRANS_TYPE_ID%TYPE;
BEGIN
    SELECT DECODE(pc_auth_only, 'Y', 0, A.AUTH_AMT_APPROVED), A.AUTH_TS, A.AUTH_HOLD_USED
      INTO pn_replenish_amount, ld_auth_ts, lc_auth_hold_used
      FROM PSS.AUTH A
     WHERE A.AUTH_ID = pn_auth_id;
    
    UPDATE PSS.TRAN X
       SET TRAN_STATE_CD = DECODE(pc_auth_only, 'Y', DECODE(lc_auth_hold_used, 'Y', '8', 'C'), (SELECT DECODE(CPT.PAYMENT_TYPE_CD, 'M', 'D', '8') FROM PSS.CLIENT_PAYMENT_TYPE CPT WHERE CPT.CLIENT_PAYMENT_TYPE_CD = X.CLIENT_PAYMENT_TYPE_CD)),
           TRAN_END_TS = TRAN_START_TS,
           TRAN_UPLOAD_TS = SYSDATE,
           TRAN_DEVICE_RESULT_TYPE_CD = pv_tran_device_result_type_cd
     WHERE TRAN_ID = pn_tran_id
     RETURNING CLIENT_PAYMENT_TYPE_CD, POS_PTA_ID, TRAN_START_TS, AUTH_GLOBAL_SESSION_CD, DEVICE_NAME, TRAN_DEVICE_TRAN_CD, TRAN_STATE_CD
      INTO pc_client_payment_type_cd, ln_pos_pta_id, ld_sale_local_date, lv_global_session_cd, lv_device_name, lv_device_tran_cd, pc_tran_state_cd;
    
    SELECT DECODE(PAYMENT_TYPE_CD, 'M', 'Y', 'N') 
      INTO lc_is_cash
      FROM PSS.CLIENT_PAYMENT_TYPE
     WHERE CLIENT_PAYMENT_TYPE_CD = pc_client_payment_type_cd;
    
    ld_sale_utc_ts := TO_TIMESTAMP(ld_auth_ts) AT TIME ZONE 'GMT';
    INSERT INTO PSS.SALE (
            TRAN_ID,
            DEVICE_BATCH_ID,
            SALE_TYPE_CD,
            SALE_START_UTC_TS,
            SALE_END_UTC_TS,
            SALE_UTC_OFFSET_MIN,
            SALE_RESULT_ID,
            SALE_AMOUNT,
            RECEIPT_RESULT_CD,
            HASH_TYPE_CD,
            TRAN_LINE_ITEM_HASH,
            SALE_GLOBAL_SESSION_CD,
            IMPORTED,
            VOID_ALLOWED) 
     SELECT pn_tran_id,
            pn_device_batch_id,
            DECODE(lc_is_cash, 'Y', 'C', 'A'),
            ld_sale_utc_ts,
            ld_sale_utc_ts,
            (ld_sale_local_date - CAST(ld_sale_utc_ts AS DATE)) * 24 * 60,
            DECODE(pc_auth_only, 'Y', 1, 0),
            pn_replenish_amount,
            pc_receipt_result_cd,
            'SHA1',
            '00',
            lv_global_session_cd,
            'N',
            'Y'
       FROM DUAL;
        
    IF pc_client_payment_type_cd IN (PKG_CONST.CLNT_PMNT_TYPE__ISIS_CREDIT, PKG_CONST.CLNT_PMNT_TYPE__ISIS_SPECIAL) AND ln_consumer_acct_id > 0 THEN
        PROCESS_ISIS_TRAN(pn_tran_id);
    END IF;
        
    SELECT D.DEVICE_ID, D.DEVICE_SERIAL_CD, PP.CURRENCY_CD, PST.TRANS_TYPE_ID
      INTO ln_device_id, lv_device_serial_cd, lv_currency_cd, ln_trans_type_id
      FROM DEVICE.DEVICE D
      JOIN PSS.POS P ON D.DEVICE_ID = P.DEVICE_ID
      JOIN PSS.POS_PTA PP ON PP.POS_ID = P.POS_ID
      JOIN PSS.PAYMENT_SUBTYPE PST ON PP.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
     WHERE PP.POS_PTA_ID = ln_pos_pta_id;
     
    ln_host_id := SF_FIND_HOST_ID(ln_device_id, 0, 0);
    IF ln_host_id IS NULL THEN
        -- create default hosts
        PKG_DEVICE_CONFIGURATION.SP_CREATE_DEFAULT_HOSTS(ln_device_id, ln_new_host_count, ln_result_cd, lv_error_message);
        IF ln_result_cd != PKG_CONST.RESULT__SUCCESS THEN
            RETURN;
        END IF;
        ln_host_id := SF_FIND_HOST_ID(ln_device_id, 0, 0);
    END IF;

    SELECT PSS.SEQ_TLI_ID.NEXTVAL
        INTO ln_tran_line_item_id
        FROM DUAL;
                  
    INSERT INTO PSS.TRAN_LINE_ITEM (
        TRAN_LINE_ITEM_ID,
        TRAN_ID,
        TRAN_LINE_ITEM_AMOUNT,
        TRAN_LINE_ITEM_POSITION_CD,
        TRAN_LINE_ITEM_TAX,
        TRAN_LINE_ITEM_TYPE_ID,
        TRAN_LINE_ITEM_QUANTITY,
        TRAN_LINE_ITEM_DESC,
        HOST_ID,
        TRAN_LINE_ITEM_BATCH_TYPE_CD,
        TRAN_LINE_ITEM_TS,
        SALE_RESULT_ID,
        APPLY_TO_CONSUMER_ACCT_ID)
    SELECT
        ln_tran_line_item_id,
        pn_tran_id,
        pn_replenish_amount,
        NULL,
        0,
        550,
        1,
        'Replenishment of card id ' || CAB.GLOBAL_ACCOUNT_ID,
        ln_host_id,
        'A',
        ld_sale_local_date,
        0,
        pn_replenish_consumer_acct_id
    FROM PSS.CONSUMER_ACCT_BASE CAB
    WHERE CONSUMER_ACCT_ID = pn_replenish_consumer_acct_id;

    UPDATE PSS.CONSUMER_ACCT
       SET CONSUMER_ACCT_BALANCE = CONSUMER_ACCT_BALANCE + pn_replenish_amount,
           CONSUMER_ACCT_REPLEN_BALANCE = CONSUMER_ACCT_REPLEN_BALANCE + pn_replenish_amount,
           CONSUMER_ACCT_REPLENISH_TOTAL = CONSUMER_ACCT_REPLENISH_TOTAL + pn_replenish_amount
     WHERE CONSUMER_ACCT_ID = pn_replenish_consumer_acct_id
      RETURNING CONSUMER_ACCT_TYPE_ID, CONSUMER_ACCT_SUB_TYPE_ID, CONSUMER_ACCT_BALANCE, CORP_CUSTOMER_ID, CONSUMER_ACCT_IDENTIFIER
      INTO ln_consumer_acct_type_id, ln_consumer_acct_sub_type_id, pn_replenish_balance_amount, ln_corp_customer_id, ln_consumer_acct_identifier;
   
    IF ln_consumer_acct_type_id IN(3) THEN
        IF lc_is_cash = 'Y' AND ln_consumer_acct_sub_type_id = 1 THEN
            IF ln_consumer_acct_identifier IS NULL THEN
                SELECT GLOBAL_ACCOUNT_ID
                  INTO ln_consumer_acct_identifier
                  FROM PSS.CONSUMER_ACCT_BASE
                 WHERE CONSUMER_ACCT_ID = pn_replenish_consumer_acct_id;
            END IF;
            -- add adjustment to ledger
            CORP.PAYMENTS_PKG.ADJUSTMENT_INS(ln_corp_customer_id, 'Cash Replenishment', lv_currency_cd, 'Cash Replenishment of card id ' || ln_consumer_acct_identifier,
                -pn_replenish_amount, ln_doc_id, ln_ledger_id);
        ELSIF ln_consumer_acct_sub_type_id = 2 THEN
            UPDATE PSS.AUTH
               SET OVERRIDE_TRANS_TYPE_ID = (
                      SELECT TT.OPERATOR_TRANS_TYPE_ID
                        FROM REPORT.TRANS_TYPE TT
                       WHERE TT.TRANS_TYPE_ID = ln_trans_type_id)
             WHERE AUTH_ID = pn_auth_id;
        END IF;
        ADD_REPLENISH_BONUSES(
            pn_tran_id,
            pn_replenish_consumer_acct_id,
            pn_replenish_amount,
            ld_auth_ts);
    END IF;
END;

PROCEDURE UPDATE_AUTHORIZATION(
    pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
    pn_auth_id PSS.AUTH.AUTH_ID%TYPE,
    pv_card_key PSS.AUTH.CARD_KEY%TYPE,
    pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE)
IS
    ld_auth_ts PSS.AUTH.AUTH_TS%TYPE;
    lv_masked_card_number PSS.TRAN.TRAN_RECEIVED_RAW_ACCT_DATA%TYPE;
    ln_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE;
    lv_device_name PSS.TRAN.DEVICE_NAME%TYPE;
    lv_device_tran_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE;
    ln_apply_to_consumer_acct_id PSS.TRAN_LINE_ITEM.APPLY_TO_CONSUMER_ACCT_ID%TYPE;
    lc_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE; 
BEGIN
    UPDATE PSS.AUTH
       SET CARD_KEY = CASE WHEN CARD_KEY IS NOT NULL THEN CARD_KEY ELSE pv_card_key END
     WHERE AUTH_ID = pn_auth_id
       AND TRAN_ID = pn_tran_id
       AND pv_card_key IS NOT NULL
     RETURNING AUTH_TS, CASE WHEN AUTH_HOLD_USED = 'Y' AND AUTH_RESULT_CD = 'N' THEN 'W' END
     INTO ld_auth_ts, lc_tran_state_cd;
       
    UPDATE PSS.TRAN
       SET CONSUMER_ACCT_ID = CASE WHEN CONSUMER_ACCT_ID IS NOT NULL THEN CONSUMER_ACCT_ID ELSE pn_consumer_acct_id END,
           TRAN_STATE_CD = CASE WHEN TRAN_STATE_CD = '7' AND lc_tran_state_cd IS NOT NULL THEN lc_tran_state_cd ELSE TRAN_STATE_CD END
     WHERE TRAN_ID = pn_tran_id
     RETURNING TRAN_RECEIVED_RAW_ACCT_DATA, POS_PTA_ID, DEVICE_NAME, TRAN_DEVICE_TRAN_CD 
          INTO lv_masked_card_number, ln_pos_pta_id, lv_device_name, lv_device_tran_cd;
    
    IF pv_card_key IS NOT NULL THEN
        SELECT MAX(APPLY_TO_CONSUMER_ACCT_ID)
          INTO ln_apply_to_consumer_acct_id
          FROM PSS.TRAN_LINE_ITEM
         WHERE TRAN_ID = pn_tran_id
           AND TRAN_LINE_ITEM_TYPE_ID = 550;
        IF ln_apply_to_consumer_acct_id IS NOT NULL THEN
            FINISH_REPLENISH_SETUP(
                pn_tran_id,
                ln_apply_to_consumer_acct_id,
                pv_card_key,
                ld_auth_ts,
                lv_masked_card_number,
                ln_pos_pta_id);    
        END IF;
    END IF;
    DELETE FROM PSS.CONSUMER_ACCT_PEND_REPLENISH
         WHERE DEVICE_NAME = lv_device_name
           AND DEVICE_TRAN_CD = TO_NUMBER_OR_NULL(lv_device_tran_cd);
END;

-- R37+ signature
PROCEDURE SP_CREATE_AUTH(
   pv_global_event_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
   pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
   pn_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE,
   pv_device_event_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE,
   pc_invalid_device_event_cd CHAR,
   pn_tran_start_time NUMBER,
   pn_auth_utc_ms NUMBER,
   pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
   pc_entry_method CHAR,
   pv_masked_card_number PSS.TRAN.TRAN_RECEIVED_RAW_ACCT_DATA%TYPE,
   pn_consumer_acct_id PSS.TRAN.CONSUMER_ACCT_ID%TYPE,
   pd_auth_ts PSS.AUTH.AUTH_TS%TYPE,
   pv_authority_resp_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
   pv_authority_resp_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
   pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
   pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
   pt_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
   pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
   pn_trace_number PSS.AUTH.TRACE_NUMBER%TYPE,
   pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
   pn_auth_amt PSS.AUTH.AUTH_AMT%TYPE,
   pn_balance_amt PSS.AUTH.AUTH_BALANCE_AMT%TYPE,
   pn_requested_amt PSS.AUTH.AUTH_AUTHORITY_AMT_RQST%TYPE,
   pn_received_amt PSS.AUTH.AUTH_AUTHORITY_AMT_RCVD%TYPE,
   pn_add_auth_hold_days NUMBER,
   pc_auth_hold_used PSS.AUTH.AUTH_HOLD_USED%TYPE,
   pv_global_session_cd VARCHAR2,
   pc_ignore_dup CHAR,
   pn_auth_action_id PSS.AUTH.AUTH_ACTION_ID%TYPE,
   pn_auth_action_bitmap PSS.AUTH.AUTH_ACTION_BITMAP%TYPE,
   pc_sent_to_device CHAR,
   pc_pass_thru CHAR,
   pv_card_key PSS.AUTH.CARD_KEY%TYPE,
   pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
   pc_tran_state_cd OUT VARCHAR2,
   pc_tran_import_needed OUT VARCHAR2,
   pc_session_update_needed OUT VARCHAR2,
   pv_sale_global_session_cd OUT VARCHAR2,
   pn_sale_session_start_time OUT PSS.SALE.SALE_SESSION_START_TIME%TYPE,
   pc_client_payment_type_cd OUT VARCHAR2,
   pn_sale_amount OUT pss.sale.sale_amount%TYPE,
   pn_tli_count OUT NUMBER,
   pn_auth_id OUT PSS.AUTH.AUTH_ID%TYPE,
   pv_denied_reason_name IN PSS.DENIED_REASON.DENIED_REASON_NAME%TYPE DEFAULT NULL) 
IS
   lc_sale_type_cd PSS.SALE.SALE_TYPE_CD%TYPE;
   ln_sale_result_id PSS.SALE.SALE_RESULT_ID%TYPE;
   ld_orig_trace_number PSS.AUTH.TRACE_NUMBER%TYPE;
   lv_orig_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE;
   lv_last_lock_utc_ts VARCHAR2(128);
   lc_invalid_device_event_cd CHAR := pc_invalid_device_event_cd;
   ld_tran_start_ts PSS.TRAN.TRAN_START_TS%TYPE := CAST(MILLIS_TO_TIMESTAMP(pn_tran_start_time) AS DATE);
   lc_imported PSS.SALE.IMPORTED%TYPE;
   ln_auth_amt_approved pss.auth.auth_amt_approved%TYPE;
   ln_auth_amt_allowed pss.auth.auth_amt_approved%TYPE;
   ln_sale_over_auth_amt_percent NUMBER;
   lv_email_from_address engine.app_setting.app_setting_value%TYPE;
   lv_email_to_address engine.app_setting.app_setting_value%TYPE;
   lv_error pss.tran.tran_info%TYPE;
   ln_device_id device.device_id%TYPE;
   lc_payment_subtype_key_id pss.pos_pta.payment_subtype_key_id%TYPE;
   lc_payment_subtype_class pss.payment_subtype.payment_subtype_class%TYPE;
   lc_previous_tran_state_cd pss.tran.tran_state_cd%TYPE;
   lt_auth_utc_ts pss.consumer_acct_device.last_used_utc_ts%TYPE;
   ln_override_trans_type_id PSS.AUTH.OVERRIDE_TRANS_TYPE_ID%TYPE;
   ln_consumer_acct_sub_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_SUB_TYPE_ID%TYPE;
   lc_backoffice_virtual_flag CHAR(1);
   ln_denied_reason_id PSS.DENIED_REASON.DENIED_REASON_ID%TYPE;
   lv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE;
   ln_discount_percent report.campaign.discount_percent%TYPE;
   ln_discount_amount NUMBER;   
   ln_base_host_id PSS.TRAN_LINE_ITEM.HOST_ID%TYPE;
   ln_new_host_count PLS_INTEGER;
   ln_result_cd NUMBER;
   lv_error_message VARCHAR2(255);
BEGIN
    pn_sale_amount := 0;
    pc_session_update_needed := 'N';

    IF pc_auth_result_cd = 'Y' THEN
        ln_auth_amt_approved := pn_auth_amt / pn_minor_currency_factor;
    ELSIF pc_auth_result_cd = 'P' THEN
        ln_auth_amt_approved := pn_received_amt / pn_minor_currency_factor;
    ELSIF pc_auth_result_cd IN('V', 'C', 'M') THEN
        ln_auth_amt_approved :=  NVL(pn_received_amt, pn_auth_amt) / pn_minor_currency_factor;
    END IF;

    pc_tran_import_needed := 'N';
    lv_last_lock_utc_ts := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', pv_device_name || ':' || pv_device_event_cd);
    
    BEGIN
        SELECT TRAN_ID, SALE_TYPE_CD, TRAN_STATE_CD, TRACE_NUMBER, SALE_RESULT_ID, IMPORTED, SALE_GLOBAL_SESSION_CD, SALE_SESSION_START_TIME, AUTH_RESULT_CD
        INTO pn_tran_id, lc_sale_type_cd, pc_tran_state_cd, ld_orig_trace_number, ln_sale_result_id, lc_imported, pv_sale_global_session_cd, pn_sale_session_start_time, lv_orig_auth_result_cd
        FROM
        (
            SELECT /*+ INDEX(t IDX_DEVICE_TRAN_CD) */ T.TRAN_ID, S.SALE_TYPE_CD, T.TRAN_STATE_CD, A.TRACE_NUMBER, 
                   S.SALE_RESULT_ID, S.IMPORTED, S.SALE_GLOBAL_SESSION_CD, S.SALE_SESSION_START_TIME, A.AUTH_RESULT_CD, A.CARD_KEY
            FROM pss.tran t
            LEFT OUTER JOIN pss.sale s ON t.tran_id = s.tran_id
            LEFT OUTER JOIN pss.auth a ON t.tran_id = a.tran_id AND a.auth_type_cd = 'N'
            WHERE t.tran_device_tran_cd = pv_device_event_cd 
              AND (t.tran_global_trans_cd = pv_global_event_cd OR t.tran_global_trans_cd LIKE pv_global_event_cd || ':%')
            ORDER BY CASE WHEN a.trace_number = pn_trace_number THEN 1 ELSE 2 END,
                CASE WHEN t.AUTH_GLOBAL_SESSION_CD = pv_global_session_cd THEN 1 ELSE 2 END,
                CASE WHEN a.AUTH_RESULT_CD IN('Y', 'P', 'V', 'C', 'M') THEN 1 ELSE 2 END,
                CASE WHEN s.sale_type_cd IN('A', 'I') THEN 1 ELSE 2 END, 
                CASE WHEN t.tran_global_trans_cd = pv_global_event_cd THEN 1 ELSE 2 END,
                t.tran_start_ts, t.created_ts, a.created_ts
        )
        WHERE ROWNUM = 1;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            pn_tran_id := NULL;
    END;
    
    IF pn_tran_id IS NOT NULL AND lc_sale_type_cd IS NOT NULL AND (lc_sale_type_cd = PKG_CONST.SALE_TYPE__CASH OR pn_auth_action_id IS NOT NULL) THEN
        pn_tran_id := NULL; -- this is not a match so insert it
        lc_invalid_device_event_cd := 'Y'; -- add ':' || tran_id to tran_global_trans_cd
    END IF;
    
    IF pn_tran_id IS NOT NULL THEN
        lc_previous_tran_state_cd := pc_tran_state_cd;
        IF ld_orig_trace_number = pn_trace_number THEN
            IF lc_imported NOT IN('Y', '-') AND lc_sale_type_cd IN(PKG_CONST.SALE_TYPE__ACTUAL, PKG_CONST.SALE_TYPE__CASH) AND ln_sale_result_id = 0 AND pc_tran_state_cd NOT IN('F', 'G', 'Z') THEN
                pc_tran_import_needed := 'Y';
            ELSE
                pc_tran_import_needed := 'N';
            END IF;                    
            RETURN; -- already inserted; exit
        ELSIF pc_auth_result_cd IN('Y', 'P', 'V', 'C', 'M') AND lv_orig_auth_result_cd  IN('Y', 'P', 'V', 'C', 'M') THEN -- Device sent dup tran id in two different auths
            pc_tran_state_cd := PKG_CONST.TRAN_STATE__DUPLICATE;
            lc_invalid_device_event_cd := 'Y';
        ELSIF pc_pass_thru = 'N' THEN -- This allows saving pass-thru auths
            IF pc_tran_state_cd IN(PKG_CONST.TRAN_STATE__SALE_NO_AUTH, PKG_CONST.TRAN_STATE__SALE_NO_AUTH_ERROR, PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND)
                OR (pc_tran_state_cd = PKG_CONST.TRAN_STATE__CLIENT_CANCELLED AND ln_sale_result_id != 0 /*Not 'Success'*/) THEN
                
                IF pc_auth_result_cd IN('Y', 'P', 'V', 'C', 'M') THEN
                    IF lc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED THEN
                        pc_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH_INTENDED;
                    ELSE
                        BEGIN
                            SELECT S.SALE_AMOUNT, P.DEVICE_ID
                            INTO pn_sale_amount, ln_device_id
                            FROM PSS.SALE S
                            JOIN PSS.TRAN T ON S.TRAN_ID = T.TRAN_ID
                            JOIN PSS.POS_PTA PP ON T.POS_PTA_ID = PP.POS_PTA_ID
                            JOIN PSS.POS P ON PP.POS_ID = P.POS_ID
                            WHERE S.TRAN_ID = pn_tran_id;
                        EXCEPTION
                            WHEN NO_DATA_FOUND THEN
                                NULL;
                        END;
                        pc_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH;
                        IF pn_sale_amount > 0 AND ln_auth_amt_approved > 0 AND pn_sale_amount > ln_auth_amt_approved THEN
                            SELECT GREATEST(ln_auth_amt_approved, NVL(NVL(MAX(DBADMIN.TO_NUMBER_OR_NULL(DS_MAA.DEVICE_SETTING_VALUE) / 100), MAX(DBADMIN.TO_NUMBER_OR_NULL(DS_AUTH_AMT.DEVICE_SETTING_VALUE) / DECODE(D.DEVICE_TYPE_ID, 13, 100, 1))), 0))
                            INTO ln_auth_amt_allowed
                            FROM DEVICE.DEVICE D
                            LEFT OUTER JOIN DEVICE.DEVICE_SETTING DS_MAA ON D.DEVICE_ID = DS_MAA.DEVICE_ID AND DS_MAA.DEVICE_SETTING_PARAMETER_CD = 'MAX_AUTH_AMOUNT'
                            LEFT OUTER JOIN DEVICE.DEVICE_SETTING DS_AUTH_AMT ON D.DEVICE_ID = DS_AUTH_AMT.DEVICE_ID
                                AND DS_AUTH_AMT.DEVICE_SETTING_PARAMETER_CD = DECODE(D.DEVICE_TYPE_ID, 13, '1200', 1, '195', 11, 'AUTHORIZATION_AMOUNT', 0, '195')
                            WHERE D.DEVICE_ID = ln_device_id;
                        
                            ln_sale_over_auth_amt_percent := NVL(DBADMIN.TO_NUMBER_OR_NULL(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('ALLOWED_SALE_AMT_OVER_AUTH_AMT_PERCENT')), 100);
                            IF pn_sale_amount > ln_auth_amt_allowed + ln_auth_amt_allowed * ln_sale_over_auth_amt_percent / 100 THEN
                                pc_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE_ERROR;
                                lv_error := 'Error: Sale amount ' || TO_CHAR(pn_sale_amount, 'FM9,999,999,990.00') || ' exceeds allowed auth amount ' || TO_CHAR(ln_auth_amt_allowed, 'FM9,999,999,990.00') || ' by more than ' || ln_sale_over_auth_amt_percent || '%';
                                UPDATE PSS.TRAN
                                SET TRAN_INFO = SUBSTR(TRAN_INFO || DECODE(TRAN_INFO, NULL, '', ', ') || lv_error, 1, 1000)
                                WHERE TRAN_ID = pn_tran_id AND (TRAN_INFO IS NULL OR INSTR(TRAN_INFO, lv_error) = 0);
                                
                                UPDATE DEVICE.DEVICE
                                SET DEVICE_ACTIVE_YN_FLAG = 'N'
                                WHERE DEVICE_NAME = pv_device_name AND DEVICE_ACTIVE_YN_FLAG = 'Y';
                                
                                lv_email_from_address := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EMAIL_FROM_ADDRESS');
                                lv_email_to_address := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EMAIL_TO_ADDRESS_CUSTOMER_SERVICE');
                                INSERT INTO ENGINE.OB_EMAIL_QUEUE(OB_EMAIL_FROM_EMAIL_ADDR, OB_EMAIL_FROM_NAME, OB_EMAIL_TO_EMAIL_ADDR, OB_EMAIL_TO_NAME, OB_EMAIL_SUBJECT, OB_EMAIL_MSG)
                                VALUES(lv_email_from_address, lv_email_from_address, lv_email_to_address, lv_email_to_address, 'Invalid sale amount', lv_error || ', device: ' || pv_device_name || ', transaction ID: ' || pn_tran_id || '. Device has been disabled.');
                            END IF;
                        END IF;
                    END IF;
                ELSE
                    IF lc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED THEN
                        pc_tran_state_cd := PKG_CONST.TRAN_STATE__INTENDED_ERROR;
                    ELSE
                        pc_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE_ERROR;
                    END IF;
                END IF;
                
                SELECT pp.PAYMENT_SUBTYPE_KEY_ID, ps.PAYMENT_SUBTYPE_CLASS, ps.CLIENT_PAYMENT_TYPE_CD, P.DEVICE_ID, CASE WHEN D.DEVICE_TYPE_ID = 14 AND D.DEVICE_SUB_TYPE_ID IN(3, 4) THEN 'Y' ELSE 'N' END,
					D.DEVICE_SERIAL_CD, H.HOST_ID
                  INTO lc_payment_subtype_key_id, lc_payment_subtype_class, pc_client_payment_type_cd, ln_device_id, lc_backoffice_virtual_flag,
					lv_device_serial_cd, ln_base_host_id
                  FROM PSS.POS_PTA PP
                  JOIN PSS.POS P ON PP.POS_ID = P.POS_ID
                  JOIN DEVICE.DEVICE D ON P.DEVICE_ID = D.DEVICE_ID
                  JOIN PSS.PAYMENT_SUBTYPE PS ON PP.PAYMENT_SUBTYPE_ID = PS.PAYMENT_SUBTYPE_ID
				  LEFT OUTER JOIN DEVICE.HOST H ON D.DEVICE_ID = H.DEVICE_ID AND H.HOST_PORT_NUM = 0
                 WHERE PP.POS_PTA_ID = pn_pos_pta_id;
                
                UPDATE PSS.TRAN
                   SET (TRAN_START_TS,
                        TRAN_END_TS,
                        TRAN_STATE_CD,
                        TRAN_RECEIVED_RAW_ACCT_DATA,
                        POS_PTA_ID,
                        CONSUMER_ACCT_ID,
                        AUTH_GLOBAL_SESSION_CD,
                        PAYMENT_SUBTYPE_KEY_ID,
                        PAYMENT_SUBTYPE_CLASS,
                        CLIENT_PAYMENT_TYPE_CD,                     
                        AUTH_HOLD_USED,
                        DEVICE_NAME) =
                    (SELECT
                        ld_tran_start_ts,  /* TRAN_START_TS */
                        TRAN_END_TS - TRAN_START_TS + ld_tran_start_ts,  /* TRAN_END_TS */
                        pc_tran_state_cd,  /* TRAN_STATE_CD */
                        pv_masked_card_number,  /* TRAN_RECEIVED_RAW_ACCT_DATA */
                        pn_pos_pta_id, /* POS_PTA_ID */
                        pn_consumer_acct_id  /* CONSUMER_ACCT_ID */,
                        pv_global_session_cd,
                        lc_payment_subtype_key_id,
                        lc_payment_subtype_class,
                        pc_client_payment_type_cd,
                        pc_auth_hold_used,
                        pv_device_name
                    FROM dual
                    ) WHERE TRAN_ID = pn_tran_id;
                    
                IF lc_previous_tran_state_cd IN (PKG_CONST.TRAN_STATE__SALE_NO_AUTH, PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND, PKG_CONST.TRAN_STATE__SALE_NO_AUTH_ERROR) THEN
                    --if backoffice virtual device, update PAYOR.LAST_CHARGE_*
                    IF pn_sale_amount > 0 AND pn_consumer_acct_id IS NOT NULL AND lc_backoffice_virtual_flag = 'Y' THEN
                        UPDATE CORP.PAYOR
                           SET LAST_CHARGE_AMOUNT = pn_sale_amount, LAST_CHARGE_TS = ld_tran_start_ts
                         WHERE PAY_TO_DEVICE_ID = ln_device_id
                           AND GLOBAL_ACCOUNT_ID = (SELECT GLOBAL_ACCOUNT_ID FROM PSS.CONSUMER_ACCT_BASE WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id)
                           AND NVL(LAST_CHARGE_TS, MIN_DATE) < ld_tran_start_ts;
                    END IF;
                    SELECT /*+ INDEX(TLI IF1_TRAN_LINE_ITEM) */ COUNT(1)
                    INTO pn_tli_count
                    FROM PSS.TRAN_LINE_ITEM TLI
                    WHERE TRAN_ID = pn_tran_id
                        AND TRAN_LINE_ITEM_BATCH_TYPE_CD = lc_sale_type_cd
                        AND TRAN_LINE_ITEM_TYPE_ID NOT IN (PKG_CONST.TLI__CANCELLATION_ADJMT, PKG_CONST.TLI__POS_DISCREPANCY_ADJMT, PKG_CONST.TLI__NEG_DISCREPANCY_ADJMT, 201);
                    
                    IF pv_sale_global_session_cd IS NOT NULL THEN
                        pc_session_update_needed := 'Y';
                        pn_sale_amount := pn_sale_amount * pn_minor_currency_factor;
                    END IF;
                END IF;
            ELSE
                pc_tran_state_cd := PKG_CONST.TRAN_STATE__DUPLICATE;
                lc_invalid_device_event_cd := 'Y';
            END IF;
        END IF;
    ELSE
        IF lc_invalid_device_event_cd = 'N' AND INSTR(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('MASTER_ID_UPDATE_SALE_TYPES'), 'N') > 0 AND TO_NUMBER_OR_NULL(pv_device_event_cd) < DBADMIN.DATE_TO_MILLIS(SYSDATE + 365) / 1000 THEN
            SELECT P.DEVICE_ID
              INTO ln_device_id
              FROM PSS.POS_PTA PP
              JOIN PSS.POS P ON PP.POS_ID = P.POS_ID
             WHERE PP.POS_PTA_ID = pn_pos_pta_id;
            UPDATE DEVICE.DEVICE_SETTING 
               SET DEVICE_SETTING_VALUE = pv_device_event_cd
             WHERE DEVICE_ID = ln_device_id
               AND DEVICE_SETTING_PARAMETER_CD = '60' -- Master Id
               AND TO_NUMBER_OR_NULL(NVL(DEVICE_SETTING_VALUE, '0')) < TO_NUMBER_OR_NULL(pv_device_event_cd);
        END IF;
        IF pc_auth_result_cd IN('Y', 'P', 'V', 'C', 'M') AND pc_sent_to_device = 'N' AND pc_auth_hold_used = 'Y' THEN
            pc_tran_state_cd := 'W'; -- Pending Reversal
        ELSIF pc_auth_result_cd IN('F', 'N') AND pc_auth_hold_used = 'Y' AND pv_card_key IS NOT NULL THEN
            pc_tran_state_cd := 'W'; -- Pending Reversal
        ELSIF pc_auth_result_cd IN('Y', 'V', 'C', 'M') THEN
            pc_tran_state_cd := '6'; -- Auth Success
        ELSIF pc_auth_result_cd IN('P') THEN
            pc_tran_state_cd := '0'; -- Auth Success Conditional
        ELSIF pc_auth_result_cd IN('N', 'O', 'R') THEN
            pc_tran_state_cd := '7'; -- Auth Decline
        ELSIF pc_auth_result_cd IN('F') THEN
            pc_tran_state_cd := '5'; -- Auth Failure
        END IF;
    END IF;
        
    IF pn_tran_id IS NULL OR pc_tran_state_cd = PKG_CONST.TRAN_STATE__DUPLICATE THEN
        SELECT PSS.SEQ_TRAN_ID.NEXTVAL
          INTO pn_tran_id
          FROM DUAL;    
        INSERT INTO PSS.TRAN (
            TRAN_ID,
            TRAN_START_TS,
            TRAN_END_TS,
            TRAN_STATE_CD,
            TRAN_DEVICE_TRAN_CD,
            TRAN_RECEIVED_RAW_ACCT_DATA,
            POS_PTA_ID,
            TRAN_GLOBAL_TRANS_CD,
            CONSUMER_ACCT_ID,
            AUTH_GLOBAL_SESSION_CD,
            PAYMENT_SUBTYPE_KEY_ID,
            PAYMENT_SUBTYPE_CLASS,
            CLIENT_PAYMENT_TYPE_CD,
            AUTH_HOLD_USED,
            DEVICE_NAME)
        SELECT
            pn_tran_id, /* TRAN_ID */
            ld_tran_start_ts,  /* TRAN_START_TS */
            ld_tran_start_ts,  /* TRAN_END_TS */
            pc_tran_state_cd,  /* TRAN_STATE_CD */
            pv_device_event_cd,  /* TRAN_DEVICE_TRAN_CD */
            pv_masked_card_number,  /* TRAN_RECEIVED_RAW_ACCT_DATA */
            pn_pos_pta_id, /* POS_PTA_ID */
            DECODE(lc_invalid_device_event_cd, 'Y', pv_global_event_cd || ':' || pn_tran_id, pv_global_event_cd), /* TRAN_GLOBAL_TRANS_CD */
            pn_consumer_acct_id  /* CONSUMER_ACCT_ID */,
            pv_global_session_cd,
            pp.payment_subtype_key_id,
            ps.payment_subtype_class,
            ps.client_payment_type_cd,
            pc_auth_hold_used,
            pv_device_name
        FROM pss.pos_pta pp
        JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
        WHERE pp.pos_pta_id = pn_pos_pta_id;
    END IF;

    IF pn_consumer_acct_id IS NOT NULL THEN
        SELECT MAX(CONSUMER_ACCT_SUB_TYPE_ID)
          INTO ln_consumer_acct_sub_type_id
          FROM PSS.CONSUMER_ACCT
         WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id;
        IF ln_consumer_acct_sub_type_id IS NOT NULL THEN
            SELECT DECODE(ln_consumer_acct_sub_type_id, 2, TT.OPERATOR_TRANS_TYPE_ID, PST.TRANS_TYPE_ID)
              INTO ln_override_trans_type_id
              FROM PSS.POS_PTA PP
              JOIN PSS.PAYMENT_SUBTYPE PST ON PP.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
              JOIN REPORT.TRANS_TYPE TT ON PST.TRANS_TYPE_ID = TT.TRANS_TYPE_ID
             WHERE PP.POS_PTA_ID = pn_pos_pta_id;
        END IF;
    END IF;
    SELECT PSS.SEQ_AUTH_ID.NEXTVAL
      INTO pn_auth_id
      FROM DUAL;
    IF pv_denied_reason_name IS NOT NULL THEN
        SELECT DENIED_REASON_ID
          INTO ln_denied_reason_id
          FROM PSS.DENIED_REASON
         WHERE DENIED_REASON_NAME = pv_denied_reason_name;
    END IF;
    INSERT INTO PSS.AUTH (
        AUTH_ID,
        TRAN_ID,
        AUTH_STATE_ID,
        AUTH_TYPE_CD,
        ACCT_ENTRY_METHOD_CD,
        AUTH_TS,
        AUTH_RESULT_CD,
        AUTH_RESP_CD,
        AUTH_RESP_DESC,
        AUTH_AUTHORITY_TRAN_CD,
        AUTH_AUTHORITY_REF_CD,
        AUTH_AUTHORITY_TS,
        AUTH_AUTHORITY_MISC_DATA,
        AUTH_AMT,
        AUTH_AMT_APPROVED,
        AUTH_AUTHORITY_AMT_RQST,
        AUTH_AUTHORITY_AMT_RCVD,
        AUTH_BALANCE_AMT,
        TRACE_NUMBER,
        AUTH_ACTION_ID,
        AUTH_ACTION_BITMAP,
        AUTH_HOLD_USED,
        CARD_KEY,
        OVERRIDE_TRANS_TYPE_ID,
        DENIED_REASON_ID)
     VALUES(
        pn_auth_id, /* AUTH_ID */
        pn_tran_id, /* TRAN_ID */
        DECODE(pc_auth_result_cd, 'Y', 2, 'N', 3, 'P', 5, 'O', 7, 'F', 4, 'R', 7, 'V', 2, 'C', 2, 'M', 2), /* AUTH_STATE_ID */
        'N', /* AUTH_TYPE_CD */
        DECODE(pc_entry_method, 'B', 7, 'C', 6, 'M', 2, 'S', 3, 'I', 8, 1), /* ACCT_ENTRY_METHOD_CD */
        pd_auth_ts, /* AUTH_TS */
        pc_auth_result_cd, /* AUTH_RESULT_CD */
        pv_authority_resp_cd, /* AUTH_RESP_CD */
        pv_authority_resp_desc, /* AUTH_RESP_DESC */
        pv_authority_tran_cd, /* AUTH_AUTHORITY_TRAN_CD */
        pv_authority_ref_cd, /* AUTH_AUTHORITY_REF_CD */
        pt_authority_ts, /* AUTH_AUTHORITY_TS */
        pv_authority_misc_data, /* AUTH_AUTHORITY_MISC_DATA */
        NVL(pn_auth_amt / pn_minor_currency_factor, 0), /* AUTH_AMT */
        ln_auth_amt_approved, /* AUTH_AMT_APPROVED */
        pn_requested_amt / pn_minor_currency_factor, /* AUTH_AUTHORITY_AMT_RQST */
        pn_received_amt / pn_minor_currency_factor,  /* AUTH_AUTHORITY_AMT_RCVD */
        pn_balance_amt / pn_minor_currency_factor, /* AUTH_BALANCE_AMT */
        pn_trace_number, /* TRACE_NUMBER */
        pn_auth_action_id,
        pn_auth_action_bitmap,
        pc_auth_hold_used,
        pv_card_key,
        ln_override_trans_type_id,
        ln_denied_reason_id);

    IF NVL(pn_add_auth_hold_days, 0) > 0 AND pc_auth_result_cd IN('Y', 'P') AND pc_auth_hold_used = 'Y' THEN
        INSERT INTO PSS.CONSUMER_ACCT_AUTH_HOLD (CONSUMER_ACCT_ID, AUTH_ID, TRAN_ID, EXPIRATION_TS)
          VALUES(pn_consumer_acct_id, pn_auth_id, pn_tran_id, SYSDATE + pn_add_auth_hold_days);
    END IF;
    
    IF lc_imported NOT IN('Y', '-') AND lc_sale_type_cd IN(PKG_CONST.SALE_TYPE__ACTUAL, PKG_CONST.SALE_TYPE__CASH) AND ln_sale_result_id = 0 AND pc_tran_state_cd NOT IN('F', 'G', 'Z') THEN
        pc_tran_import_needed := 'Y';
    ELSE
        pc_tran_import_needed := 'N';
    END IF;
    
    IF pc_auth_result_cd = 'F' AND pv_authority_resp_cd = 'INVALID_AUTH_AMOUNT' AND pn_auth_amt > 0 THEN
         INSERT INTO ENGINE.MACHINE_CMD_PENDING(MACHINE_ID, DATA_TYPE, COMMAND, EXECUTE_ORDER)
             SELECT *
               FROM (
             SELECT DEVICE_NAME MACHINE_ID, DECODE(DEVICE_TYPE_ID, 13, 'CB', '88') DATA_TYPE, 
                    DECODE(DEVICE_TYPE_ID, 13, '00000E' || TO_CHAR(6 + LENGTH(TO_CHAR(AUTH_AMOUNT)), 'FM000X') || '313230303D' || RAWTOHEX(TO_CHAR(AUTH_AMOUNT)) || '0A', '420000006100000002') COMMAND, 20 EXECUTE_ORDER
               FROM (SELECT D.DEVICE_NAME, D.DEVICE_TYPE_ID, NVL(NVL(DBADMIN.TO_NUMBER_OR_NULL(DD.MAX_AUTH_AMOUNT), DBADMIN.TO_NUMBER_OR_NULL(DD.AUTH_AMOUNT) * DECODE(D.DEVICE_TYPE_ID, 13, 1, 100) * 3), 0) MAX_AUTH_AMOUNT,
                    DBADMIN.TO_NUMBER_OR_NULL(DD.AUTH_AMOUNT) AUTH_AMOUNT
               FROM PSS.POS_PTA PP
               JOIN PSS.POS P ON PP.POS_ID = P.POS_ID
               JOIN DEVICE.DEVICE D ON P.DEVICE_ID = D.DEVICE_ID
               JOIN DEVICE.DEVICE_DATA DD ON D.DEVICE_NAME = DD.DEVICE_NAME
              WHERE PP.POS_PTA_ID = pn_pos_pta_id
                AND D.DEVICE_TYPE_ID IN(0,1,13)
                AND DBADMIN.TO_NUMBER_OR_NULL(DD.AUTH_AMOUNT) > 0)
              WHERE MAX_AUTH_AMOUNT < pn_auth_amt
                AND MAX_AUTH_AMOUNT > 0) N
              WHERE NOT EXISTS(
                    SELECT 1 
                      FROM ENGINE.MACHINE_CMD_PENDING O
                    WHERE O.MACHINE_ID = N.MACHINE_ID
                      AND O.DATA_TYPE = N.DATA_TYPE
                      AND O.COMMAND = N.COMMAND);
    END IF;
    
    IF pn_consumer_acct_id IS NOT NULL AND (pc_pass_thru = 'N' OR pc_auth_result_cd IN('Y', 'P', 'V', 'C', 'M')) THEN
        lt_auth_utc_ts := DBADMIN.MILLIS_TO_TIMESTAMP(pn_auth_utc_ms);
    
        FOR i IN 1..2 LOOP
            UPDATE PSS.CONSUMER_ACCT_DEVICE
            SET USED_COUNT = USED_COUNT + 1,
                LAST_USED_UTC_TS = CASE WHEN LAST_USED_UTC_TS IS NULL OR lt_auth_utc_ts > LAST_USED_UTC_TS THEN lt_auth_utc_ts ELSE LAST_USED_UTC_TS END
            WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id AND DEVICE_NAME = pv_device_name;
            
            IF SQL%FOUND THEN
                EXIT;
            ELSE
                BEGIN
                    INSERT INTO PSS.CONSUMER_ACCT_DEVICE(CONSUMER_ACCT_ID, DEVICE_NAME, USED_COUNT, LAST_USED_UTC_TS)
                    VALUES(pn_consumer_acct_id, pv_device_name, 1, lt_auth_utc_ts);
                    EXIT;
                EXCEPTION
                    WHEN DUP_VAL_ON_INDEX THEN
                        NULL;
                END;
            END IF;
        END LOOP;
    END IF;
    
    IF pc_client_payment_type_cd IN (PKG_CONST.CLNT_PMNT_TYPE__ISIS_CREDIT, PKG_CONST.CLNT_PMNT_TYPE__ISIS_SPECIAL)
		AND lc_previous_tran_state_cd = PKG_CONST.TRAN_STATE__SALE_NO_AUTH THEN
		IF pn_consumer_acct_id > 0 THEN
        PROCESS_ISIS_TRAN(pn_tran_id);
		END IF;
		
		IF pn_sale_amount > 0 AND pc_pass_thru = 'N' THEN
			-- Softcard Discount promotion
			IF REGEXP_LIKE(lv_device_serial_cd, DBADMIN.PKG_GLOBAL.GET_APP_SETTING('SOFTCARD_DISCOUNT_PROMO_SERIAL_CD_REGEX')) THEN
				ln_discount_percent := DBADMIN.TO_NUMBER_OR_NULL(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('SOFTCARD_DISCOUNT_PROMO_PERCENT'));
			
				IF ln_discount_percent IS NOT NULL THEN
					ln_discount_amount := ROUND(pn_sale_amount * ln_discount_percent);
					IF ln_discount_amount > 0 THEN
						IF ln_discount_amount > pn_sale_amount THEN
							ln_discount_amount := pn_sale_amount;
						END IF;
						pn_sale_amount := pn_sale_amount - ln_discount_amount;
						
						IF ln_base_host_id IS NULL THEN
							pkg_device_configuration.sp_create_default_hosts(ln_device_id, ln_new_host_count, ln_result_cd, lv_error_message);
							IF ln_result_cd != PKG_CONST.RESULT__SUCCESS THEN
								RETURN;
							END IF;
							SELECT H.HOST_ID
							INTO ln_base_host_id
							FROM DEVICE.HOST H
							WHERE H.DEVICE_ID = ln_device_id
							AND H.HOST_PORT_NUM = 0;
						END IF;
										
						-- create Softcard Discount line item
						INSERT INTO PSS.TRAN_LINE_ITEM (
							TRAN_ID,
							HOST_ID,
							TRAN_LINE_ITEM_TYPE_ID,
							TRAN_LINE_ITEM_AMOUNT,
							TRAN_LINE_ITEM_QUANTITY,
							TRAN_LINE_ITEM_DESC,
							TRAN_LINE_ITEM_BATCH_TYPE_CD,
							TRAN_LINE_ITEM_TAX)
						SELECT
							pn_tran_id,
							ln_base_host_id,
							tran_line_item_type_id,
							-ln_discount_amount / pn_minor_currency_factor,
							1,
							tran_line_item_type_desc || ' ' || ln_discount_percent * 100 || '%',
							PKG_CONST.TRAN_BATCH_TYPE__ACTUAL,
							0
						FROM pss.tran_line_item_type
						WHERE tran_line_item_type_id = 208;
						
						UPDATE PSS.SALE
						SET SALE_AMOUNT = pn_sale_amount / pn_minor_currency_factor
						WHERE TRAN_ID = pn_tran_id;
					END IF;
				END IF;	
			END IF;
		END IF;
    END IF;
    
    IF REGEXP_LIKE(pv_authority_misc_data, 'ConsumerID=|OfferID=') THEN
        UPDATE REPORT.TRANS
        SET DESCRIPTION = pv_authority_misc_data
        WHERE MACHINE_TRANS_NO = pv_global_event_cd AND SOURCE_SYSTEM_CD = 'PSS' AND DESCRIPTION IS NULL;
    END IF;
END;

-- R37 and above
PROCEDURE SP_INSERT_AUTH_STATS(
   pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
   pn_auth_id PSS.AUTH.AUTH_ID%TYPE,
   pn_request_time NUMBER,
   pn_applayer_start_time NUMBER,
   pn_authority_start_time NUMBER,
   pn_authority_end_time NUMBER,
   pn_applayer_end_time NUMBER,
   pn_response_time NUMBER
) IS
BEGIN
    INSERT INTO PSS.TRAN_STAT(TRAN_ID, AUTH_ID, TRAN_STAT_TYPE_ID, TRAN_STAT_VALUE)
      SELECT pn_tran_id, pn_auth_id, TRAN_STAT_TYPE_ID, TRAN_STAT_VALUE FROM (
        SELECT 2 /* live auth "POSM" time*/ TRAN_STAT_TYPE_ID, (pn_applayer_end_time - pn_applayer_start_time) / 1000 TRAN_STAT_VALUE FROM DUAL WHERE pn_applayer_start_time IS NOT NULL AND pn_applayer_end_time IS NOT NULL
        UNION ALL SELECT 4 /* live auth network time*/, (pn_response_time - pn_request_time) / 1000 FROM DUAL WHERE pn_request_time IS NOT NULL AND pn_response_time IS NOT NULL
        UNION ALL SELECT 1 /* live auth gateway time*/, (pn_authority_end_time - pn_authority_start_time) / 1000 FROM DUAL WHERE pn_authority_start_time IS NOT NULL AND pn_authority_end_time IS NOT NULL) a
     WHERE NOT EXISTS(SELECT 1 FROM PSS.TRAN_STAT TS WHERE pn_tran_id = TS.TRAN_ID AND NVL(pn_auth_id, 0) = NVL(TS.AUTH_ID, 0) AND A.TRAN_STAT_TYPE_ID = TS.TRAN_STAT_TYPE_ID);    
END;

-- R37+ Signature
PROCEDURE PERMIT_CONSUMER_ACCT(
    pn_pos_pta_id PSS.POS_PTA.POS_PTA_ID%TYPE,
    pn_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
    pt_auth_ts PSS.CONSUMER_ACCT.CONSUMER_ACCT_ACTIVATION_TS%TYPE,
    pl_consumer_acct_type_ids NUMBER_TABLE,
    pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
    pn_consumer_id OUT PSS.CONSUMER_ACCT.CONSUMER_ID%TYPE,
    pn_consumer_acct_type_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_TYPE_ID%TYPE,
    pv_consumer_acct_type_label OUT PSS.CONSUMER_ACCT_TYPE.CONSUMER_ACCT_TYPE_LABEL%TYPE,
    pb_consumer_acct_raw_hash OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_RAW_HASH%TYPE, 
    pb_consumer_acct_raw_salt OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_RAW_SALT%TYPE, 
    pb_security_cd_hash OUT PSS.CONSUMER_ACCT.SECURITY_CD_HASH%TYPE, 
    pb_security_cd_salt OUT PSS.CONSUMER_ACCT.SECURITY_CD_SALT%TYPE,
    pn_action_id OUT PSS.PERMISSION_ACTION.ACTION_ID%TYPE,
    pn_action_code OUT NUMBER,
    pn_action_bitmap OUT NUMBER)
IS
    lc_store_action CHAR(1);
    lv_device_name DEVICE.DEVICE_NAME%TYPE;
    ln_device_id DEVICE.DEVICE_ID%TYPE;
    ln_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE;
BEGIN
    SELECT CONSUMER_ACCT_ID, CONSUMER_ID, CONSUMER_ACCT_TYPE_ID, CONSUMER_ACCT_TYPE_LABEL, CONSUMER_ACCT_RAW_HASH, CONSUMER_ACCT_RAW_SALT, SECURITY_CD_HASH, SECURITY_CD_SALT, DEVICE_ID, DEVICE_TYPE_ID, DEVICE_NAME
      INTO pn_consumer_acct_id, pn_consumer_id, pn_consumer_acct_type_id, pv_consumer_acct_type_label, pb_consumer_acct_raw_hash, pb_consumer_acct_raw_salt, pb_security_cd_hash, pb_security_cd_salt, ln_device_id, ln_device_type_id, lv_device_name
      FROM (
         SELECT CA.CONSUMER_ACCT_ID, CA.CONSUMER_ID, CA.CONSUMER_ACCT_TYPE_ID, CAT.CONSUMER_ACCT_TYPE_LABEL, CA.CONSUMER_ACCT_RAW_HASH, CA.CONSUMER_ACCT_RAW_SALT, CA.SECURITY_CD_HASH, CA.SECURITY_CD_SALT, D.DEVICE_ID, D.DEVICE_TYPE_ID, D.DEVICE_NAME, VLH.ANCESTOR_LOCATION_ID, MAX(CA.CONSUMER_ACCT_ISSUE_NUM) MAX_ISSUE_NUM, VLH.DEPTH
           FROM PSS.POS_PTA PTA
           JOIN PSS.POS POS ON PTA.POS_ID = POS.POS_ID
           JOIN DEVICE.DEVICE D ON D.DEVICE_ID = POS.DEVICE_ID
           JOIN LOCATION.VW_LOCATION_HIERARCHY VLH ON VLH.DESCENDENT_LOCATION_ID = POS.LOCATION_ID
           JOIN PSS.CONSUMER_ACCT CA ON VLH.ANCESTOR_LOCATION_ID = CA.LOCATION_ID
           JOIN PSS.CONSUMER_ACCT_TYPE CAT ON CAT.CONSUMER_ACCT_TYPE_ID = CA.CONSUMER_ACCT_TYPE_ID
           JOIN PSS.CONSUMER_ACCT_BASE CAB ON CAB.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
          WHERE PTA.POS_PTA_ID = pn_pos_pta_id
            AND CAB.GLOBAL_ACCOUNT_ID = pn_global_account_id
            AND CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y'
            AND NVL(CA.CONSUMER_ACCT_ACTIVATION_TS, MIN_DATE) <= pt_auth_ts
            AND NVL(CA.CONSUMER_ACCT_DEACTIVATION_TS, MAX_DATE) > pt_auth_ts
            AND CA.CONSUMER_ACCT_TYPE_ID MEMBER OF pl_consumer_acct_type_ids
            GROUP BY CA.CONSUMER_ACCT_ID, CA.CONSUMER_ID, CA.CONSUMER_ACCT_TYPE_ID, CAT.CONSUMER_ACCT_TYPE_LABEL, CA.CONSUMER_ACCT_RAW_HASH, CA.CONSUMER_ACCT_RAW_SALT, CA.SECURITY_CD_HASH, CA.SECURITY_CD_SALT, D.DEVICE_ID, D.DEVICE_TYPE_ID, D.DEVICE_NAME, VLH.ANCESTOR_LOCATION_ID, VLH.DEPTH
            ORDER BY VLH.DEPTH, MAX_ISSUE_NUM DESC    /* DEPTH IS ASCENDING, AS IT IS THE DIFFERENCE BETWEEN LOCATION AND ANCESTOR */
    ) WHERE ROWNUM = 1;

    SELECT A.ACTION_ID, DTA.DEVICE_TYPE_ACTION_CD,
           DECODE(A.ACTION_PARAM_TYPE_CD, 'B', SUM(POWER(2, AP.PROTOCOL_BIT_INDEX))) PROTOCOL_BITMAP,
           DECODE(A.ACTION_CLEAR_PARAMETER_CD, NULL, 'N', 'Y') STORE_LAST_ACTION
      INTO pn_action_id, pn_action_code, pn_action_bitmap, lc_store_action
      FROM (SELECT * FROM (
         SELECT CAP.PERMISSION_ACTION_ID, DTA.DEVICE_TYPE_ACTION_CD PROTOCOL_ACTION_CD,
                CASE WHEN LDA.DEVICE_ACTION_UTC_TS IS NOT NULL
                          AND CURRENT_TIMESTAMP < LDA.DEVICE_ACTION_UTC_TS
                          + NUMTODSINTERVAL(COALESCE(TO_NUMBER_OR_NULL(DS_T.DEVICE_SETTING_VALUE),
                          TO_NUMBER_OR_NULL(cts.CONFIG_TEMPLATE_SETTING_VALUE), 3600), 'SECOND') THEN 10
                     ELSE DTA.ACTION_ID END ACTION_ID
           FROM PSS.CONSUMER_ACCT_PERMISSION CAP
           JOIN PSS.PERMISSION_ACTION PA ON CAP.PERMISSION_ACTION_ID = PA.PERMISSION_ACTION_ID
           JOIN DEVICE.DEVICE_TYPE_ACTION DTA ON DTA.ACTION_ID = PA.ACTION_ID
           JOIN DEVICE.DEVICE_TYPE DT ON DTA.DEVICE_TYPE_ID = DT.DEVICE_TYPE_ID
           LEFT OUTER JOIN PSS.LAST_DEVICE_ACTION LDA
             ON LDA.DEVICE_NAME = lv_device_name
            AND CAP.CONSUMER_ACCT_ID = LDA.CONSUMER_ACCT_ID
            AND PA.ACTION_ID = LDA.DEVICE_ACTION_ID
           JOIN DEVICE.ACTION A ON PA.ACTION_ID = A.ACTION_ID
           LEFT OUTER JOIN DEVICE.DEVICE_SETTING DS_T ON ln_device_id = DS_T.DEVICE_ID AND DS_T.DEVICE_SETTING_PARAMETER_CD = A.ACTION_CLEAR_PARAMETER_CD
           LEFT OUTER JOIN DEVICE.DEVICE_SETTING ds_v ON ln_device_id = ds_v.DEVICE_ID AND ds_v.DEVICE_SETTING_PARAMETER_CD = 'Property List Version'
           LEFT OUTER JOIN DEVICE.CONFIG_TEMPLATE ct
             ON ct.CONFIG_TEMPLATE_NAME = DECODE(DBADMIN.PKG_UTL.COMPARE(DT.DEVICE_TYPE_ID, 13),
                -1, DT.DEFAULT_CONFIG_TEMPLATE_NAME, DT.DEFAULT_CONFIG_TEMPLATE_NAME || ds_v.DEVICE_SETTING_VALUE)
           LEFT OUTER JOIN DEVICE.CONFIG_TEMPLATE_SETTING cts
             ON cts.CONFIG_TEMPLATE_ID = ct.CONFIG_TEMPLATE_ID
            AND cts.DEVICE_SETTING_PARAMETER_CD = A.ACTION_CLEAR_PARAMETER_CD
          WHERE CAP.CONSUMER_ACCT_ID = pn_consumer_acct_id
            AND DTA.DEVICE_TYPE_ID = ln_device_type_id
          ORDER BY CAP.CONSUMER_ACCT_PERMISSION_ORDER
      ) WHERE ROWNUM = 1) O
      LEFT OUTER JOIN (PSS.PERMISSION_ACTION_PARAM PAP
      JOIN DEVICE.ACTION_PARAM AP ON PAP.ACTION_PARAM_ID = AP.ACTION_PARAM_ID)
        ON O.PERMISSION_ACTION_ID = PAP.PERMISSION_ACTION_ID
      JOIN DEVICE.ACTION A ON O.ACTION_ID = A.ACTION_ID
      JOIN DEVICE.DEVICE_TYPE_ACTION DTA ON DTA.ACTION_ID = O.ACTION_ID
     WHERE DTA.DEVICE_TYPE_ID = ln_device_type_id
      GROUP BY A.ACTION_ID, DTA.DEVICE_TYPE_ACTION_CD, A.ACTION_CLEAR_PARAMETER_CD, A.ACTION_PARAM_TYPE_CD;
    IF lc_store_action = 'Y' THEN
        MERGE INTO PSS.LAST_DEVICE_ACTION O
         USING (
              SELECT lv_device_name DEVICE_NAME,
                     pn_consumer_acct_id CONSUMER_ACCT_ID,
                     pn_action_id DEVICE_ACTION_ID,
                     CURRENT_TIMESTAMP DEVICE_ACTION_UTC_TS
                FROM DUAL) N
              ON (O.DEVICE_NAME = N.DEVICE_NAME)
              WHEN MATCHED THEN
               UPDATE
                  SET O.CONSUMER_ACCT_ID = N.CONSUMER_ACCT_ID,
                      O.DEVICE_ACTION_ID = N.DEVICE_ACTION_ID,
                      O.DEVICE_ACTION_UTC_TS = N.DEVICE_ACTION_UTC_TS
              WHEN NOT MATCHED THEN
               INSERT (O.DEVICE_NAME,
                       O.CONSUMER_ACCT_ID,
                       O.DEVICE_ACTION_ID,
                       O.DEVICE_ACTION_UTC_TS)
                VALUES(N.DEVICE_NAME,
                       N.CONSUMER_ACCT_ID,
                       N.DEVICE_ACTION_ID,
                       N.DEVICE_ACTION_UTC_TS
                );
    END IF;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN;
END;

    -- R37+ signature
    PROCEDURE SP_CREATE_LOCAL_AUTH_SALE(
       pc_global_event_cd_prefix IN CHAR,
       pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
       pn_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE,
       pv_device_tran_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE,
       pn_sale_utc_ts_ms NUMBER,
       pn_sale_utc_offset_min IN pss.sale.sale_utc_offset_min%TYPE,
       pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
       pc_entry_method CHAR,
       pc_payment_type CHAR,
       pv_track_data PSS.TRAN.TRAN_RECEIVED_RAW_ACCT_DATA%TYPE,
       pn_consumer_acct_id PSS.TRAN.CONSUMER_ACCT_ID%TYPE,
       pd_auth_ts PSS.AUTH.AUTH_TS%TYPE,
       pn_trace_number PSS.AUTH.TRACE_NUMBER%TYPE,
       pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
       pn_auth_amt PSS.AUTH.AUTH_AMT%TYPE,
       pn_device_batch_id IN pss.sale.device_batch_id%TYPE,
       pc_sale_type_cd IN pss.sale.sale_type_cd%TYPE,
       pv_tran_device_result_type_cd IN pss.tran.tran_device_result_type_cd%TYPE,
       pn_sale_result_id IN pss.sale.sale_result_id%TYPE,
       pv_hash_type_cd IN pss.sale.hash_type_cd%TYPE,
       pv_tran_line_item_hash IN pss.sale.tran_line_item_hash%TYPE,
       pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
       pn_result_cd OUT NUMBER,
       pv_error_message OUT VARCHAR2
    ) IS
       ln_auth_id PSS.AUTH.AUTH_ID%TYPE;
       lv_global_trans_cd pss.tran.tran_global_trans_cd%TYPE;
       lv_global_trans_cd_app_layer pss.tran.tran_global_trans_cd%TYPE;
       lv_global_trans_cd_legacy pss.tran.tran_global_trans_cd%TYPE;
       ld_tran_upload_ts pss.tran.tran_upload_ts%TYPE;
       ln_tran_exists NUMBER := PKG_CONST.BOOLEAN__FALSE;
       ln_tli_hash_match NUMBER;
       ld_current_ts DATE := SYSDATE;
       ld_tran_start_ts pss.tran.tran_start_ts%TYPE;
       lt_sale_start_utc_ts pss.sale.sale_start_utc_ts%TYPE;
       lv_last_lock_utc_ts VARCHAR2(128);
       ln_insert_tran NUMBER := PKG_CONST.BOOLEAN__FALSE;
       lv_tran_state_cd pss.tran.tran_state_cd%TYPE := PKG_CONST.TRAN_STATE__COMPLETE_ERROR;
    BEGIN
        IF pc_global_event_cd_prefix NOT IN (PKG_CONST.EVENT_CODE_PREFIX__APP_LAYER,
            PKG_CONST.EVENT_CODE_PREFIX__LEGACY) THEN
            pn_result_cd := PKG_CONST.RESULT__INVALID_PARAMETER;
            pv_error_message := 'Invalid pc_global_event_cd_prefix: ' || pc_global_event_cd_prefix;
            RETURN;
        END IF;
    
        lv_global_trans_cd := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(pc_global_event_cd_prefix, pv_device_name, pv_device_tran_cd);
        lv_global_trans_cd_app_layer := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(PKG_CONST.EVENT_CODE_PREFIX__APP_LAYER, pv_device_name, pv_device_tran_cd);
        lv_global_trans_cd_legacy := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(PKG_CONST.EVENT_CODE_PREFIX__LEGACY, pv_device_name, pv_device_tran_cd);
        ld_tran_start_ts := CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_sale_utc_ts_ms + pn_sale_utc_offset_min * 60 * 1000) AS DATE);
        lt_sale_start_utc_ts := CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_sale_utc_ts_ms) AS DATE);
        lv_last_lock_utc_ts := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', pv_device_name || ':' || pv_device_tran_cd);
        BEGIN
            SELECT tran_id, tran_upload_ts, PKG_CONST.BOOLEAN__TRUE, tli_hash_match
            INTO pn_tran_id, ld_tran_upload_ts, ln_tran_exists, ln_tli_hash_match
            FROM
            (
                SELECT /*+ INDEX(t IDX_DEVICE_TRAN_CD) */ t.tran_id, t.tran_upload_ts,
                    CASE WHEN s.hash_type_cd = pv_hash_type_cd
                        AND s.tran_line_item_hash = pv_tran_line_item_hash
                        AND s.sale_type_cd = pc_sale_type_cd THEN PKG_CONST.BOOLEAN__TRUE
                    ELSE PKG_CONST.BOOLEAN__FALSE END AS tli_hash_match
                FROM pss.tran t
                LEFT OUTER JOIN pss.sale s ON t.tran_id = s.tran_id
                WHERE t.tran_device_tran_cd = pv_device_tran_cd AND (
                    t.tran_global_trans_cd IN (lv_global_trans_cd_app_layer, lv_global_trans_cd_legacy)
                    OR t.tran_global_trans_cd LIKE lv_global_trans_cd_app_layer || ':%'
                    OR t.tran_global_trans_cd LIKE lv_global_trans_cd_legacy || ':%'
                )
                ORDER BY CASE WHEN t.tran_global_trans_cd IN (lv_global_trans_cd_app_layer, lv_global_trans_cd_legacy) THEN 1 ELSE 2 END,
                    CASE WHEN s.sale_type_cd = pc_sale_type_cd THEN 1 ELSE 2 END,
                    tli_hash_match DESC, t.tran_start_ts, t.created_ts
            )
            WHERE ROWNUM = 1;
    
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                ln_insert_tran := PKG_CONST.BOOLEAN__TRUE;
        END;
    
        IF ln_tran_exists = PKG_CONST.BOOLEAN__TRUE AND ld_tran_upload_ts IS NOT NULL THEN
            IF ln_tli_hash_match = PKG_CONST.BOOLEAN__TRUE THEN
                UPDATE pss.sale
                SET duplicate_count = duplicate_count + 1
                WHERE tran_id = pn_tran_id;
    
                pn_result_cd := PKG_CONST.RESULT__DUPLICATE;
                pv_error_message := 'Duplicate sale, original tran_id: ' || pn_tran_id;
                pn_tran_id := 0;
                RETURN;
            END IF;
        
            ln_insert_tran := PKG_CONST.BOOLEAN__TRUE;
        END IF;
    
        IF ln_insert_tran = PKG_CONST.BOOLEAN__TRUE THEN
            IF pc_sale_type_cd = PKG_CONST.SALE_TYPE__CASH THEN
                lv_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE;
            ELSIF pv_tran_device_result_type_cd IN (PKG_CONST.TRAN_DEV_RES__CANCELLED,
                    PKG_CONST.TRAN_DEV_RES__AUTH_FAILURE,
                    PKG_CONST.TRAN_DEV_RES__FAILURE,
                    PKG_CONST.TRAN_DEV_RES__TIMEOUT) OR pn_auth_amt <= 0 THEN
                lv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED;
            END IF;
            
            SELECT PSS.SEQ_TRAN_ID.NEXTVAL
            INTO pn_tran_id
            FROM DUAL;
    
            IF ln_tran_exists = PKG_CONST.BOOLEAN__TRUE THEN
                lv_global_trans_cd := lv_global_trans_cd || ':' || pn_tran_id;
            END IF;     
    
            INSERT INTO PSS.TRAN (
                TRAN_ID,
                TRAN_START_TS,
                TRAN_END_TS,
                TRAN_UPLOAD_TS,
                TRAN_STATE_CD,
                TRAN_DEVICE_TRAN_CD,
                TRAN_RECEIVED_RAW_ACCT_DATA,
                POS_PTA_ID,
                TRAN_GLOBAL_TRANS_CD,
                CONSUMER_ACCT_ID,
                TRAN_DEVICE_RESULT_TYPE_CD,
                PAYMENT_SUBTYPE_KEY_ID,
                PAYMENT_SUBTYPE_CLASS,
                CLIENT_PAYMENT_TYPE_CD,
                DEVICE_NAME)
            SELECT
                pn_tran_id, /* TRAN_ID */
                ld_tran_start_ts,  /* TRAN_START_TS */
                ld_tran_start_ts, /* TRAN_END_TS */
                ld_current_ts,
                lv_tran_state_cd,  /* TRAN_STATE_CD */
                pv_device_tran_cd,  /* TRAN_DEVICE_TRAN_CD */
                pv_track_data,  /* TRAN_RECEIVED_RAW_ACCT_DATA */
                pn_pos_pta_id, /* POS_PTA_ID */
                lv_global_trans_cd, /* TRAN_GLOBAL_TRANS_CD */
                pn_consumer_acct_id,  /* CONSUMER_ACCT_ID */
                pv_tran_device_result_type_cd,
                pp.payment_subtype_key_id,
                ps.payment_subtype_class,
                ps.client_payment_type_cd,
                pv_device_name
            FROM pss.pos_pta pp
            JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
            WHERE pp.pos_pta_id = pn_pos_pta_id;
    
            IF pc_sale_type_cd != PKG_CONST.SALE_TYPE__CASH THEN
            SELECT PSS.SEQ_AUTH_ID.NEXTVAL
              INTO ln_auth_id
              FROM DUAL;
            INSERT INTO PSS.AUTH (
                AUTH_ID,
                TRAN_ID,
                AUTH_STATE_ID,
                AUTH_TYPE_CD,
                AUTH_PARSED_ACCT_DATA,
                ACCT_ENTRY_METHOD_CD,
                AUTH_TS,
                AUTH_RESULT_CD,
                AUTH_RESP_CD,
                AUTH_RESP_DESC,
                AUTH_AUTHORITY_TS,
                AUTH_AMT,
                AUTH_AUTHORITY_AMT_RQST,
                TRACE_NUMBER)
             VALUES(
                ln_auth_id, /* AUTH_ID */
                pn_tran_id, /* TRAN_ID */
                DECODE(pc_auth_result_cd, 'Y', 2, 'N', 3, 'P', 5, 'O', 7, 'F', 4, 'R', 7, 'V', 2, 'C', 2, 'M', 2), /* AUTH_STATE_ID */
                'L', /* AUTH_TYPE_CD */
                pv_track_data, /* AUTH_PARSED_ACCT_DATA */
                DECODE(pc_entry_method, 'B', 7, 'C', 6, 'M', 2, 'S', 3, 'I', 8, 1), /* ACCT_ENTRY_METHOD_CD */
                pd_auth_ts, /* AUTH_TS */
                pc_auth_result_cd, /* AUTH_RESULT_CD */
                'LOCAL', /* AUTH_RESP_CD */
                'Local authorization not accepted', /* AUTH_RESP_DESC */
                pd_auth_ts, /* AUTH_AUTHORITY_TS */
                NVL(pn_auth_amt / pn_minor_currency_factor, 0), /* AUTH_AMT */
                pn_auth_amt / pn_minor_currency_factor, /* AUTH_AUTHORITY_AMT_RQST */
                pn_trace_number /* TRACE_NUMBER */
                );
            END IF;
    
            INSERT INTO pss.sale (
                tran_id,
                device_batch_id,
                sale_type_cd,
                sale_start_utc_ts,
                sale_end_utc_ts,
                sale_utc_offset_min,
                sale_result_id,
                sale_amount,
                receipt_result_cd,
                hash_type_cd,
                tran_line_item_hash
            ) VALUES (
                pn_tran_id,
                pn_device_batch_id,
                pc_sale_type_cd,
                lt_sale_start_utc_ts,
                lt_sale_start_utc_ts,
                pn_sale_utc_offset_min,
                pn_sale_result_id,
                pn_auth_amt / pn_minor_currency_factor,
                'U',
                pv_hash_type_cd,
                pv_tran_line_item_hash
            );
        END IF;
    
        pn_result_cd := PKG_CONST.RESULT__SUCCESS;
        pv_error_message := PKG_CONST.ERROR__NO_ERROR;
    END;

    PROCEDURE SETUP_REPLENISH_CONSUMER_ACCT(
       pn_replenish_id PSS.CONSUMER_ACCT_PEND_REPLENISH.CONSUMER_ACCT_REPLENISH_ID%TYPE,
       pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
       lc_always_auth_flag CHAR,
       pv_replenish_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
       pn_balance PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
       pn_replenish_threshhold PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_THRESHHOLD%TYPE,
       pn_replenish_amount IN OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_AMOUNT%TYPE,
       pc_replenish_flag OUT VARCHAR2,
       pn_replenish_next_master_id OUT NUMBER,
       pv_replenish_device_name OUT DEVICE.DEVICE_NAME%TYPE)
    IS
       ln_balance PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE;
       ln_pending_amount PSS.CONSUMER_ACCT_PEND_REPLENISH.AMOUNT%TYPE;
       ln_auth_hold_total PSS.AUTH.AUTH_AMT_APPROVED%TYPE;
    BEGIN
        SELECT NVL(SUM(A.AUTH_AMT_APPROVED), 0)
          INTO ln_auth_hold_total
          FROM PSS.CONSUMER_ACCT_AUTH_HOLD CAAH
          JOIN PSS.AUTH A ON CAAH.AUTH_ID = A.AUTH_ID
         WHERE CAAH.CONSUMER_ACCT_ID = pn_consumer_acct_id
           AND CAAH.EXPIRATION_TS > SYSDATE
           AND CAAH.CLEARED_YN_FLAG = 'N';
        SELECT NVL(SUM(AMOUNT), 0)
          INTO ln_pending_amount
          FROM PSS.CONSUMER_ACCT_PEND_REPLENISH
         WHERE CONSUMER_ACCT_REPLENISH_ID = pn_replenish_id
           AND EXPIRATION_TS > SYSDATE
           AND (SUBMITTED_FLAG = 'Y' OR (SUBMITTED_FLAG = '?' AND CREATED_UTC_TS > SYS_EXTRACT_UTC(SYSTIMESTAMP) - (1/24/60)));
        ln_balance := pn_balance + ln_pending_amount - ln_auth_hold_total;   
        IF lc_always_auth_flag = 'Y' OR pn_replenish_threshhold IS NULL OR ln_balance < pn_replenish_threshhold THEN
            IF pn_replenish_threshhold IS NOT NULL THEN
                IF pn_replenish_amount + ln_balance < pn_replenish_threshhold THEN
                    pn_replenish_amount := pn_replenish_threshhold - ln_balance;
                ELSIF ln_balance >= pn_replenish_threshhold AND lc_always_auth_flag = 'Y' THEN
                    pn_replenish_amount := 0;
                END IF;
            ELSIF lc_always_auth_flag = 'Y' AND pn_replenish_threshhold IS NULL THEN
              pn_replenish_amount := 0;
            END IF;
            PKG_DEVICE_CONFIGURATION.NEXT_MASTER_ID_BY_SERIAL(pv_replenish_device_serial_cd, pv_replenish_device_name, pn_replenish_next_master_id);
            INSERT INTO PSS.CONSUMER_ACCT_PEND_REPLENISH(CONSUMER_ACCT_REPLENISH_ID, DEVICE_NAME, DEVICE_TRAN_CD, AMOUNT, EXPIRATION_TS)
                VALUES(pn_replenish_id, pv_replenish_device_name, pn_replenish_next_master_id, pn_replenish_amount, SYSDATE + 7);
            pc_replenish_flag := 'Y';
        ELSE
            pc_replenish_flag := 'N';       
        END IF;
    END;
    
    -- R37 and above
    PROCEDURE DEBIT_CONSUMER_ACCT(
       pn_amount PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
       pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
       pc_redelivery_flag CHAR,
       pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
       pc_auth_result_cd OUT VARCHAR2,
       pn_debitted_amount OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
       pn_new_balance OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
       pc_auto_replenish_flag OUT VARCHAR2,
       pn_used_cash_back_balance OUT PSS.CONSUMER_ACCT.TOWARD_CASH_BACK_BALANCE%TYPE,
       pn_cash_back_amount OUT PSS.SALE.SALE_AMOUNT%TYPE)
    IS
       ln_auth_id PSS.AUTH.AUTH_ID%TYPE;
       ln_auth_amount PSS.AUTH.AUTH_AMT_APPROVED%TYPE;
       ln_auth_hold_used PSS.AUTH.AUTH_HOLD_USED%TYPE;
       ld_tran_start_ts PSS.TRAN.TRAN_START_TS%TYPE;
       ln_loyalty_discount PSS.CONSUMER_ACCT.LOYALTY_DISCOUNT_TOTAL%TYPE;
       ln_consumer_acct_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_TYPE_ID%TYPE;
       ln_consumer_acct_sub_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_SUB_TYPE_ID%TYPE;
       ln_cash_back_percent REPORT.CAMPAIGN.DISCOUNT_PERCENT%TYPE;
       ln_cash_back_threshhold REPORT.CAMPAIGN.THRESHOLD_AMOUNT%TYPE;
       ln_cash_back_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE;
       ln_trans_type_id PSS.PAYMENT_SUBTYPE.TRANS_TYPE_ID%TYPE;
       ln_operator_trans_type_id REPORT.TRANS_TYPE.OPERATOR_TRANS_TYPE_ID%TYPE;
       lv_lock VARCHAR2(128);
    BEGIN
        SELECT ca.CONSUMER_ACCT_ID, A.AUTH_HOLD_USED, A.AUTH_ID, X.TRAN_START_TS, A.AUTH_AMT_APPROVED, NVL(SUM(ABS(XI.TRAN_LINE_ITEM_AMOUNT * XI.TRAN_LINE_ITEM_QUANTITY)), 0), 
               CASE WHEN CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y' AND CA.CONSUMER_ACCT_TYPE_ID = 3 THEN 'Y' ELSE 'N' END, CA.CONSUMER_ACCT_TYPE_ID, PST.TRANS_TYPE_ID, TT.OPERATOR_TRANS_TYPE_ID
          INTO pn_consumer_acct_id, ln_auth_hold_used, ln_auth_id, ld_tran_start_ts, ln_auth_amount, ln_loyalty_discount, pc_auto_replenish_flag, ln_consumer_acct_type_id, ln_trans_type_id, ln_operator_trans_type_id
          FROM PSS.CONSUMER_ACCT ca
          JOIN PSS.TRAN X ON X.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
          JOIN PSS.AUTH A ON X.TRAN_ID = A.TRAN_ID AND A.AUTH_TYPE_CD = 'N' AND A.AUTH_RESULT_CD IN('Y', 'P', 'V', 'C', 'M')
          JOIN PSS.POS_PTA PP ON X.POS_PTA_ID = PP.POS_PTA_ID
          JOIN PSS.PAYMENT_SUBTYPE PST ON PP.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
          JOIN REPORT.TRANS_TYPE TT ON PST.TRANS_TYPE_ID = TT.TRANS_TYPE_ID
          LEFT OUTER JOIN PSS.TRAN_LINE_ITEM XI ON X.TRAN_ID = XI.TRAN_ID AND XI.TRAN_LINE_ITEM_TYPE_ID = 204
         WHERE X.TRAN_ID = pn_tran_id
         GROUP BY ca.CONSUMER_ACCT_ID, A.AUTH_HOLD_USED, A.AUTH_ID, X.TRAN_START_TS, A.AUTH_AMT_APPROVED, CA.CONSUMER_ACCT_ACTIVE_YN_FLAG, CA.CONSUMER_ACCT_TYPE_ID, PST.TRANS_TYPE_ID, TT.OPERATOR_TRANS_TYPE_ID;
        IF pc_auto_replenish_flag = 'Y' THEN
            SELECT NVL(MAX(REPLENISH_FLAG), 'N')
              INTO pc_auto_replenish_flag
              FROM (
                SELECT CASE WHEN CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y' 
                             AND CA.CONSUMER_ACCT_TYPE_ID = 3 
                             AND CAR.REPLENISH_THRESHHOLD > 0
                             AND CAR.REPLENISH_AMOUNT > 0 
                             AND CAR.REPLENISH_CARD_KEY IS NOT NULL THEN 'Y' ELSE 'N' END REPLENISH_FLAG
                  FROM PSS.CONSUMER_ACCT CA
                  JOIN PSS.CONSUMER_ACCT_REPLENISH CAR ON CA.CONSUMER_ACCT_ID = CAR.CONSUMER_ACCT_ID
                 WHERE ca.CONSUMER_ACCT_ID = pn_consumer_acct_id
                   AND CAR.REPLENISH_TYPE_ID = 1
                 ORDER BY CAR.PRIORITY, CAR.CONSUMER_ACCT_REPLENISH_ID)
             WHERE ROWNUM = 1;    
        END IF;
        IF ln_auth_hold_used = 'Y' THEN
            IF ln_consumer_acct_type_id IN(3) THEN
                SELECT MAX(DISCOUNT_PERCENT), MAX(THRESHOLD_AMOUNT), MAX(CAMPAIGN_ID)
                  INTO ln_cash_back_percent, ln_cash_back_threshhold, ln_cash_back_campaign_id
                  FROM (SELECT C.DISCOUNT_PERCENT, C.THRESHOLD_AMOUNT, C.CAMPAIGN_ID
                  FROM REPORT.CAMPAIGN C
                  JOIN PSS.CAMPAIGN_CONSUMER_ACCT CCA on C.CAMPAIGN_ID = CCA.CAMPAIGN_ID
                 WHERE CCA.CONSUMER_ACCT_ID = pn_consumer_acct_id
                   AND ld_tran_start_ts BETWEEN NVL(C.START_DATE, MIN_DATE) AND NVL(C.END_DATE, MAX_DATE)
                   AND C.CAMPAIGN_TYPE_ID = 3 /* Spend reward - Cash back */
                   AND C.DISCOUNT_PERCENT > 0
                   AND C.DISCOUNT_PERCENT < 1
                   AND (TRIM(C.RECUR_SCHEDULE) IS NULL OR REPORT.MATCH_CAMPAIGN_RECUR_SCHEDULE(C.RECUR_SCHEDULE, ld_tran_start_ts) = 'Y')
                 ORDER BY C.DISCOUNT_PERCENT DESC, C.CAMPAIGN_ID DESC)
                 WHERE ROWNUM = 1;
            END IF;
            lv_lock := PKG_GLOBAL.REQUEST_LOCK('PSS.CONSUMER_ACCT', pn_consumer_acct_id);
            DELETE 
              FROM PSS.CONSUMER_ACCT_AUTH_HOLD
             WHERE AUTH_ID = ln_auth_id
               AND EXPIRATION_TS > SYSDATE 
               AND CLEARED_YN_FLAG = 'N';
            IF SQL%FOUND THEN
                -- debit CONSUMER_ACCT_PROMO_BALANCE first and then CONSUMER_ACCT_REPLEN_BALANCE
                UPDATE PSS.CONSUMER_ACCT
                   SET CONSUMER_ACCT_BALANCE = CASE 
						WHEN pn_amount < CONSUMER_ACCT_BALANCE OR ALLOW_NEGATIVE_BALANCE = 'Y' THEN CONSUMER_ACCT_BALANCE - pn_amount
						ELSE 0 END,
                       LOYALTY_DISCOUNT_TOTAL = NVL(LOYALTY_DISCOUNT_TOTAL, 0) + ln_loyalty_discount,
                       CONSUMER_ACCT_PROMO_BALANCE = CONSUMER_ACCT_PROMO_BALANCE -
                            CASE WHEN CONSUMER_ACCT_PROMO_BALANCE > 0 AND CONSUMER_ACCT_PROMO_BALANCE >= pn_amount THEN pn_amount ELSE CONSUMER_ACCT_PROMO_BALANCE END,
                       CONSUMER_ACCT_REPLEN_BALANCE = CONSUMER_ACCT_REPLEN_BALANCE - 
                            CASE WHEN CONSUMER_ACCT_PROMO_BALANCE > 0 AND CONSUMER_ACCT_PROMO_BALANCE >= pn_amount THEN 0
                                WHEN CONSUMER_ACCT_REPLEN_BALANCE > 0 AND CONSUMER_ACCT_REPLEN_BALANCE >= pn_amount - CONSUMER_ACCT_PROMO_BALANCE THEN pn_amount - CONSUMER_ACCT_PROMO_BALANCE
                                ELSE CONSUMER_ACCT_REPLEN_BALANCE END,
                 TRAN_COUNT_TOTAL=CASE WHEN CONSUMER_ACCT_TYPE_ID = 3 THEN NVL(TRAN_COUNT_TOTAL, 0) + 1 ELSE TRAN_COUNT_TOTAL END
                 WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id
                  RETURNING CONSUMER_ACCT_BALANCE, CONSUMER_ACCT_SUB_TYPE_ID
                  INTO pn_new_balance, ln_consumer_acct_sub_type_id;
                pc_auth_result_cd := 'Y';
                UPDATE PSS.AUTH SA 
                   SET AUTH_STATE_ID = 2,
                       AUTH_RESULT_CD = pc_auth_result_cd,
                       OVERRIDE_TRANS_TYPE_ID = CASE WHEN ln_consumer_acct_sub_type_id = 2 THEN ln_operator_trans_type_id ELSE ln_trans_type_id END
                 WHERE SA.TRAN_ID = pn_tran_id
                   AND SA.AUTH_TYPE_CD IN('U', 'C')
                   AND SA.AUTH_STATE_ID = 6;
                IF SQL%ROWCOUNT != 1 THEN
                    RAISE_APPLICATION_ERROR(-20116, 'Auth Hold Inconsistent State for transaction ' || pn_tran_id);
                END IF;
                IF ln_cash_back_campaign_id IS NOT NULL AND ln_cash_back_percent > 0 THEN
                    DECLARE
                        ln_cash_back_balance PSS.CONSUMER_ACCT.TOWARD_CASH_BACK_BALANCE%TYPE;
                        lv_cash_back_device_name DEVICE.DEVICE_NAME%TYPE;
                        ln_cash_back_next_master_id NUMBER;
                        lv_currency_cd PSS.CONSUMER_ACCT.CURRENCY_CD%TYPE;
                        ln_result_cd NUMBER;
                        lv_error_message VARCHAR2(4000);
                        ln_cash_back_tran_id PSS.TRAN.TRAN_ID%TYPE;
                        lv_cash_back_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
                        ln_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE;
                        lc_currency_symbol CORP.CURRENCY.CURRENCY_SYMBOL%TYPE;
                        ln_host_id HOST.HOST_ID%TYPE;
                        ld_cash_back_ts DATE;
                        ld_cash_back_time NUMBER;
                        ln_corp_customer_id PSS.CONSUMER_ACCT.CORP_CUSTOMER_ID%TYPE;
                        lv_consumer_acct_cd PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD%TYPE;
                        lv_consumer_acct_identifier PSS.CONSUMER_ACCT.CONSUMER_ACCT_IDENTIFIER%TYPE;
                        lv_desc PSS.TRAN_LINE_ITEM.TRAN_LINE_ITEM_DESC%TYPE;
                        ln_doc_id CORP.DOC.DOC_ID%TYPE;
                        ln_ledger_id CORP.LEDGER.LEDGER_ID%TYPE;                        
                    BEGIN
                        -- update consumer acct
                        UPDATE PSS.CONSUMER_ACCT
                           SET TOWARD_CASH_BACK_BALANCE = NVL(TOWARD_CASH_BACK_BALANCE, 0) + pn_amount
                         WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id
                         RETURNING TOWARD_CASH_BACK_BALANCE, CURRENCY_CD, CORP_CUSTOMER_ID, CONSUMER_ACCT_CD, CONSUMER_ACCT_IDENTIFIER
                          INTO ln_cash_back_balance, lv_currency_cd, ln_corp_customer_id, lv_consumer_acct_cd, lv_consumer_acct_identifier;
                        IF ln_cash_back_balance >= ln_cash_back_threshhold THEN 
                            pn_used_cash_back_balance := TRUNC(ln_cash_back_balance / ln_cash_back_threshhold) * ln_cash_back_threshhold;
                            pn_cash_back_amount := ROUND(pn_used_cash_back_balance * ln_cash_back_percent, 2);
                            UPDATE PSS.CONSUMER_ACCT
                               SET TOWARD_CASH_BACK_BALANCE = TOWARD_CASH_BACK_BALANCE - pn_used_cash_back_balance,
                                   CASH_BACK_TOTAL = NVL(CASH_BACK_TOTAL, 0) + pn_cash_back_amount,
                                   CONSUMER_ACCT_BALANCE = CONSUMER_ACCT_BALANCE + pn_cash_back_amount,
                                   CONSUMER_ACCT_PROMO_BALANCE = CONSUMER_ACCT_PROMO_BALANCE + pn_cash_back_amount,
                                   CONSUMER_ACCT_PROMO_TOTAL = CONSUMER_ACCT_PROMO_TOTAL + pn_cash_back_amount
                             WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id;                         
                            -- add trans to virtual terminal
                            SELECT C.MINOR_CURRENCY_FACTOR, NVL(CC.CURRENCY_SYMBOL, '$'), DBADMIN.TIMESTAMP_TO_MILLIS(SYS_EXTRACT_UTC(SYSTIMESTAMP)), SYSDATE
                              INTO ln_minor_currency_factor, lc_currency_symbol, ld_cash_back_time, ld_cash_back_ts
                              FROM PSS.CURRENCY C
                              LEFT OUTER JOIN CORP.CURRENCY CC ON C.CURRENCY_CD = CC.CURRENCY_CODE
                             WHERE C.CURRENCY_CD = lv_currency_cd;
                             
                            PKG_DEVICE_CONFIGURATION.NEXT_MASTER_ID_BY_SERIAL('V1-' || ln_corp_customer_id || '-' || lv_currency_cd, lv_cash_back_device_name, ln_cash_back_next_master_id);
                            SP_CREATE_SALE('A', lv_cash_back_device_name, ln_cash_back_next_master_id,  0, 'C', ld_cash_back_time, 
                                DBADMIN.PKG_UTL.TIME_ZONE_DIFF_MIN(PKG_CONST.DB_TIME_ZONE, PKG_CONST.GMT_TIME_ZONE), 'S', 0, pn_cash_back_amount * ln_minor_currency_factor, 'U', 'A', 'SHA1', 
                                DBADMIN.HASH_CARD('Bonus Cash on ' || pn_consumer_acct_id || ' of ' || pn_cash_back_amount),
                                NULL, ln_result_cd, lv_error_message, ln_cash_back_tran_id, lv_cash_back_tran_state_cd);
                            IF ln_result_cd != PKG_CONST.RESULT__SUCCESS THEN
                                RAISE_APPLICATION_ERROR(-20118, 'Could not create cash back transaction: ' || lv_error_message);
                            END IF;
                            SELECT HOST_ID, 'Bonus Cash for ' || lc_currency_symbol || TO_NUMBER(pn_used_cash_back_balance, 'FM9,999,999.00') || ' in purchases'
                              INTO ln_host_id, lv_desc
                              FROM (SELECT H.HOST_ID
                                      FROM DEVICE.HOST H
                                      JOIN DEVICE.DEVICE D ON H.DEVICE_ID = D.DEVICE_ID
                                     WHERE D.DEVICE_ACTIVE_YN_FLAG = 'Y'
                                       AND H.HOST_PORT_NUM IN(0,1)
                                       AND D.DEVICE_NAME = lv_cash_back_device_name
                                       AND H.HOST_ACTIVE_YN_FLAG = 'Y'
                                     ORDER BY H.HOST_PORT_NUM DESC, H.HOST_ID DESC) 
                             WHERE ROWNUM = 1;
                            INSERT INTO PSS.TRAN_LINE_ITEM(
                                TRAN_ID,
                                TRAN_LINE_ITEM_AMOUNT,
                                TRAN_LINE_ITEM_POSITION_CD,
                                TRAN_LINE_ITEM_TAX,
                                TRAN_LINE_ITEM_TYPE_ID,
                                TRAN_LINE_ITEM_QUANTITY,
                                TRAN_LINE_ITEM_DESC,
                                HOST_ID,
                                TRAN_LINE_ITEM_BATCH_TYPE_CD,
                                TRAN_LINE_ITEM_TS,
                                SALE_RESULT_ID,
                                APPLY_TO_CONSUMER_ACCT_ID,
                                CAMPAIGN_ID)
                            SELECT
                                ln_cash_back_tran_id,
                                pn_used_cash_back_balance,
                                NULL,
                                NULL,
                                554,
                                ln_cash_back_percent,
                                lv_desc,
                                ln_host_id,
                                'A',
                                ld_cash_back_ts,
                                0,
                                pn_consumer_acct_id,
                                ln_cash_back_campaign_id
                            FROM DUAL;
                            UPDATE PSS.TRAN
                               SET PARENT_TRAN_ID = pn_tran_id
                             WHERE TRAN_ID = ln_cash_back_tran_id;
                            IF ln_consumer_acct_sub_type_id = 1 THEN
                                -- add adjustment to ledger
                                CORP.PAYMENTS_PKG.ADJUSTMENT_INS(ln_corp_customer_id, 'Bonus Cash Processing', lv_currency_cd, lv_desc || ', card # ' || lv_consumer_acct_cd || ', card ID ' || lv_consumer_acct_identifier,
                                    -pn_cash_back_amount, ln_doc_id, ln_ledger_id);
                            END IF;
                        END IF; 
                    END;
                END IF;
            ELSE
                SELECT MAX(AUTH_RESULT_CD)
                  INTO pc_auth_result_cd
                  FROM PSS.AUTH SA 
                 WHERE SA.TRAN_ID = pn_tran_id
                   AND SA.AUTH_TYPE_CD IN('U', 'C')
                   AND SA.AUTH_STATE_ID = 2;
                IF pc_auth_result_cd IS NULL THEN
                    DECLARE
                        ld_expiration_ts PSS.CONSUMER_ACCT_AUTH_HOLD.EXPIRATION_TS%TYPE;
                        lc_cleared_flag PSS.CONSUMER_ACCT_AUTH_HOLD.CLEARED_YN_FLAG%TYPE;
                    BEGIN
                        SELECT EXPIRATION_TS, CLEARED_YN_FLAG
                          INTO ld_expiration_ts, lc_cleared_flag
                          FROM PSS.CONSUMER_ACCT_AUTH_HOLD
                         WHERE AUTH_ID = ln_auth_id;
                        IF lc_cleared_flag != 'N' THEN
                            RAISE_APPLICATION_ERROR(-20114, 'Auth Hold Was Cleared for transaction ' || pn_tran_id);
                        ELSIF ld_expiration_ts <= SYSDATE THEN
                            RAISE_APPLICATION_ERROR(-20115, 'Auth Hold Has Expired for transaction ' || pn_tran_id);
                        ELSE
                            RAISE_APPLICATION_ERROR(-20116, 'Auth Hold Inconsistent State for transaction ' || pn_tran_id);
                        END IF;
                    EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                            RAISE_APPLICATION_ERROR(-20111, 'Auth Hold Not Found for transaction ' || pn_tran_id);
                    END;
                END IF;
            END IF;
            pn_debitted_amount := pn_amount;
        ELSE
            pn_debitted_amount := 0;
            pc_auto_replenish_flag := 'N';
        END IF;
    END;
    
    -- R37 and above
    PROCEDURE CREDIT_CONSUMER_ACCT(
       pn_amount PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
       pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
       pc_redelivery_flag CHAR,
       pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
       pc_auth_result_cd OUT VARCHAR2,
       pn_creditted_amount OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
       pn_new_balance OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE)
    IS
       ln_refund_id PSS.REFUND.REFUND_ID%TYPE;
       ln_refund_state_id PSS.REFUND.REFUND_STATE_ID%TYPE;
       ln_consumer_acct_sub_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_SUB_TYPE_ID%TYPE;
       ln_override_trans_type_id PSS.REFUND.OVERRIDE_TRANS_TYPE_ID%TYPE;
       lv_lock VARCHAR2(128);
    BEGIN
        SELECT ca.CONSUMER_ACCT_ID, R.REFUND_ID, R.REFUND_STATE_ID, CA.CONSUMER_ACCT_SUB_TYPE_ID
          INTO pn_consumer_acct_id, ln_refund_id, ln_refund_state_id, ln_consumer_acct_sub_type_id
          FROM PSS.CONSUMER_ACCT ca
          JOIN PSS.TRAN X ON X.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
          JOIN PSS.REFUND R ON X.TRAN_ID = R.TRAN_ID
         WHERE X.TRAN_ID = pn_tran_id;
        
        IF ln_refund_state_id IN(2,3,4,6) THEN
            pc_auth_result_cd := 'Y';
            IF ln_consumer_acct_sub_type_id IS NOT NULL THEN
                IF ln_consumer_acct_sub_type_id = 2 THEN
                    ln_override_trans_type_id := 31;
                ELSE
                    ln_override_trans_type_id := 20;
                END IF;
            END IF;                 
            lv_lock := PKG_GLOBAL.REQUEST_LOCK('PSS.CONSUMER_ACCT', pn_consumer_acct_id);
            UPDATE PSS.REFUND
               SET REFUND_STATE_ID = 1,
                   OVERRIDE_TRANS_TYPE_ID = ln_override_trans_type_id
             WHERE REFUND_ID = ln_refund_id
               AND REFUND_STATE_ID = ln_refund_state_id;
            IF SQL%FOUND THEN
                UPDATE PSS.CONSUMER_ACCT
                   SET CONSUMER_ACCT_BALANCE = CONSUMER_ACCT_BALANCE + pn_amount,
                       CONSUMER_ACCT_REPLEN_BALANCE = CONSUMER_ACCT_REPLEN_BALANCE + pn_amount,
                       TOWARD_CASH_BACK_BALANCE = CASE WHEN TOWARD_CASH_BACK_BALANCE IS NOT NULL AND CONSUMER_ACCT_TYPE_ID = 3 THEN TOWARD_CASH_BACK_BALANCE - pn_amount ELSE TOWARD_CASH_BACK_BALANCE END
                 WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id
                   AND NVL(CONSUMER_ACCT_SUB_TYPE_ID, 0) = NVL(ln_consumer_acct_sub_type_id, 0)
                 RETURNING CONSUMER_ACCT_BALANCE INTO pn_new_balance;
                IF NOT SQL%FOUND THEN
                    RAISE_APPLICATION_ERROR(-20116, 'Auth Hold Inconsistent State for transaction ' || pn_tran_id);
                END IF;
            ELSE
                RAISE_APPLICATION_ERROR(-20116, 'Auth Hold Inconsistent State for transaction ' || pn_tran_id);
            END IF;
            pn_creditted_amount := pn_amount;
        ELSIF pc_redelivery_flag != 'Y' THEN
            RAISE_APPLICATION_ERROR(-20111, 'Pending refund not found for transaction ' || pn_tran_id);
        ELSE
            pc_auth_result_cd := 'Y';
            pn_creditted_amount := pn_amount;
        END IF;
    END;
    
    -- For R37 and above
    PROCEDURE CHECK_REPLENISH_CONSUMER_ACCT(
       pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
       pv_replenish_device_serial_cd OUT DEVICE.DEVICE_SERIAL_CD%TYPE,
       pc_auto_replenish_flag OUT VARCHAR2,
       pn_replenish_id OUT PSS.CONSUMER_ACCT_REPLENISH.CONSUMER_ACCT_REPLENISH_ID%TYPE,
       pn_replenish_amount OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_AMOUNT%TYPE,
       pv_replenish_card_key OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_CARD_KEY%TYPE,
       pn_global_account_id OUT PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
       pn_consumer_id OUT PSS.CONSUMER_ACCT.CONSUMER_ID%TYPE,
       pn_replenish_next_master_id OUT NUMBER,
       pv_replenish_device_name OUT DEVICE.DEVICE_NAME%TYPE,
       pn_max_denied_count PLS_INTEGER DEFAULT -1)
    IS
       ln_replenish_threshhold PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_THRESHHOLD%TYPE;
       lv_lock VARCHAR2(128);
       CURSOR l_cur IS
            SELECT CAR.CONSUMER_ACCT_REPLENISH_ID, CAR.REPLENISH_TYPE_ID,
                   DECODE(CAR.REPLENISH_REQUESTED_FLAG, 'Y', NULL, CAR.REPLENISH_THRESHHOLD) REPLENISH_THRESHHOLD, 
                   CAR.REPLENISH_AMOUNT, CAR.REPLENISH_CARD_KEY, CA.CONSUMER_ACCT_BALANCE, CA.CURRENCY_CD,
                   'V1-' || CASE
                        WHEN CA.CONSUMER_ACCT_SUB_TYPE_ID = 2 THEN CA.CORP_CUSTOMER_ID
                        ELSE 1 
                    END || '-' || CA.CURRENCY_CD REPLENISH_DEVICE_SERIAL_CD,
                    CA.CONSUMER_ID,
                    CAB.GLOBAL_ACCOUNT_ID
              FROM PSS.CONSUMER_ACCT CA
              JOIN PSS.CONSUMER_ACCT_BASE CAB ON CA.CONSUMER_ACCT_ID = CAB.CONSUMER_ACCT_ID
              JOIN PSS.CONSUMER_ACCT_REPLENISH CAR ON CA.CONSUMER_ACCT_ID = CAR.CONSUMER_ACCT_ID
             WHERE ca.CONSUMER_ACCT_ID = pn_consumer_acct_id
               AND (CAR.REPLENISH_TYPE_ID = 1 OR CAR.REPLENISH_REQUESTED_FLAG = 'Y')
               AND (pn_max_denied_count < 1 OR CAR.REPLENISH_DENIED_COUNT < pn_max_denied_count OR CAR.REPLENISH_REQUESTED_FLAG = 'Y')
               AND CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y' 
               AND CAR.STATUS = 'A' 
               AND CA.CONSUMER_ACCT_TYPE_ID = 3 
               AND (CAR.REPLENISH_THRESHHOLD > 0 OR CAR.REPLENISH_REQUESTED_FLAG = 'Y')
               AND CAR.REPLENISH_AMOUNT > 0 
               AND CAR.REPLENISH_CARD_KEY IS NOT NULL
               AND CAR.PRIORITY=1
             ORDER BY CAR.PRIORITY, CAR.CONSUMER_ACCT_REPLENISH_ID;
    BEGIN
        lv_lock := PKG_GLOBAL.REQUEST_LOCK('PSS.CONSUMER_ACCT', pn_consumer_acct_id);
        pc_auto_replenish_flag := 'N';
        FOR l_rec IN l_cur LOOP
            UPDATE PSS.CONSUMER_ACCT_REPLENISH
               SET REPLENISH_REQUESTED_FLAG = 'N'
             WHERE CONSUMER_ACCT_REPLENISH_ID = l_rec.CONSUMER_ACCT_REPLENISH_ID
               AND REPLENISH_REQUESTED_FLAG = 'Y';
            IF SQL%FOUND THEN
                ln_replenish_threshhold := NULL; -- ignore threshhold
            ELSIF l_rec.REPLENISH_TYPE_ID != 1 THEN
                pc_auto_replenish_flag := 'N';
                CONTINUE;
            ELSE
                ln_replenish_threshhold := l_rec.REPLENISH_THRESHHOLD;
            END IF;
            pn_replenish_amount := l_rec.REPLENISH_AMOUNT;
            pn_global_account_id := l_rec.GLOBAL_ACCOUNT_ID;
            pn_consumer_id := l_rec.CONSUMER_ID;
            
            SETUP_REPLENISH_CONSUMER_ACCT(
                   l_rec.CONSUMER_ACCT_REPLENISH_ID,
                   pn_consumer_acct_id,
                   'N',
                   l_rec.REPLENISH_DEVICE_SERIAL_CD,
                   l_rec.CONSUMER_ACCT_BALANCE,
                   ln_replenish_threshhold,
                   pn_replenish_amount,
                   pc_auto_replenish_flag,
                   pn_replenish_next_master_id,
                   pv_replenish_device_name);
            IF pc_auto_replenish_flag = 'Y' THEN
                pn_replenish_id := l_rec.CONSUMER_ACCT_REPLENISH_ID;
                pv_replenish_card_key := l_rec.REPLENISH_CARD_KEY;
                pv_replenish_device_serial_cd := l_rec.REPLENISH_DEVICE_SERIAL_CD;
                RETURN;
            END IF;
        END LOOP;
    END;
    
    -- R44 Signature
    PROCEDURE LOCK_AUTH_HOLD(
        pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
        pn_ignore_auth_id PSS.AUTH.AUTH_ID%TYPE,
        pn_auth_hold_total OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
        pn_balance OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE)
    IS
        lv_lock VARCHAR2(128);
    BEGIN
        lv_lock := PKG_GLOBAL.REQUEST_LOCK('PSS.CONSUMER_ACCT', pn_consumer_acct_id);
        SELECT CA.CONSUMER_ACCT_BALANCE
          INTO pn_balance 
          FROM PSS.CONSUMER_ACCT CA
         WHERE CA.CONSUMER_ACCT_ID = pn_consumer_acct_id;
        SELECT NVL(SUM(A.AUTH_AMT_APPROVED), 0)
          INTO pn_auth_hold_total
          FROM PSS.CONSUMER_ACCT_AUTH_HOLD CAAH
          JOIN PSS.AUTH A ON CAAH.AUTH_ID = A.AUTH_ID
         WHERE CAAH.CONSUMER_ACCT_ID = pn_consumer_acct_id
           AND CAAH.EXPIRATION_TS > SYSDATE
           AND CAAH.CLEARED_YN_FLAG = 'N'
           AND (pn_ignore_auth_id IS NULL OR A.AUTH_ID != pn_ignore_auth_id);
    END;

    -- R33 to R43 Signature
    PROCEDURE LOCK_AUTH_HOLD(
        pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
        pn_ignore_auth_id PSS.AUTH.AUTH_ID%TYPE,
        pn_auth_hold_total OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE)
    IS
        ln_balance PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE;
    BEGIN
        LOCK_AUTH_HOLD(pn_consumer_acct_id, pn_ignore_auth_id, pn_auth_hold_total, ln_balance);
    END;
   
    PROCEDURE SETUP_REPLENISH(
        pn_replenish_id IN OUT PSS.CONSUMER_ACCT_REPLENISH.CONSUMER_ACCT_REPLENISH_ID%TYPE,
        pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
        pn_replenish_type_id PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_TYPE_ID%TYPE,
        pv_replenish_card_masked PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_CARD_MASKED%TYPE,
        pv_replenish_device_serial_cd IN OUT DEVICE.DEVICE_SERIAL_CD%TYPE, -- From R37 on we ignore the provided device_serial_cd
        pn_replenish_threshhold PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_THRESHHOLD%TYPE,
        pn_replenish_amount IN OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_AMOUNT%TYPE,
        pc_replenish_flag OUT VARCHAR2,
        pn_replenish_next_master_id OUT NUMBER,
        pv_replenish_device_name OUT DEVICE.DEVICE_NAME%TYPE)
    IS
        lv_lock VARCHAR2(128);
        ln_balance PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE;
        ln_priority PSS.CONSUMER_ACCT_REPLENISH.PRIORITY%TYPE;
        lc_always_auth_flag CHAR(1);
    BEGIN
        lv_lock := PKG_GLOBAL.REQUEST_LOCK('PSS.CONSUMER_ACCT', pn_consumer_acct_id);
        SELECT CONSUMER_ACCT_BALANCE, 'V1-' || CASE 
                WHEN CONSUMER_ACCT_SUB_TYPE_ID = 2 THEN CORP_CUSTOMER_ID
                ELSE 1 
            END || '-' || CURRENCY_CD
          INTO ln_balance, pv_replenish_device_serial_cd
          FROM PSS.CONSUMER_ACCT
         WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id;       
        IF pn_replenish_id IS NULL THEN
           
            SELECT PSS.SEQ_CONSUMER_ACCT_REPLENISH_ID.NEXTVAL, PRIORITY
              INTO pn_replenish_id, ln_priority
              FROM (SELECT NVL(MAX(PRIORITY), 0) + 1 PRIORITY
              FROM PSS.CONSUMER_ACCT_REPLENISH
             WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id);
            INSERT INTO PSS.CONSUMER_ACCT_REPLENISH(CONSUMER_ACCT_REPLENISH_ID, CONSUMER_ACCT_ID, PRIORITY, REPLENISH_TYPE_ID, REPLENISH_CARD_MASKED, REPLENISH_AMOUNT, REPLENISH_THRESHHOLD)
                VALUES(pn_replenish_id, pn_consumer_acct_id, ln_priority, pn_replenish_type_id, pv_replenish_card_masked, pn_replenish_amount, pn_replenish_threshhold);
            lc_always_auth_flag := 'Y';
        ELSE
            UPDATE PSS.CONSUMER_ACCT_REPLENISH CAR
               SET CAR.REPLENISH_THRESHHOLD = pn_replenish_threshhold,
                   CAR.REPLENISH_AMOUNT = pn_replenish_amount,
                   CAR.REPLENISH_TYPE_ID = pn_replenish_type_id
             WHERE CAR.CONSUMER_ACCT_REPLENISH_ID = pn_replenish_id
               AND CAR.CONSUMER_ACCT_ID = pn_consumer_acct_id;
            IF NOT SQL%FOUND THEN
                RAISE_APPLICATION_ERROR(-20119, 'ReplenishId ' || pn_replenish_id || ' does not belong to consumer acct ' || pn_consumer_acct_id);
            END IF;
            lc_always_auth_flag := 'N';
        END IF;
        SETUP_REPLENISH_CONSUMER_ACCT(
           pn_replenish_id,
           pn_consumer_acct_id,
           lc_always_auth_flag,
           pv_replenish_device_serial_cd,
           ln_balance,
           pn_replenish_threshhold,
           pn_replenish_amount,
           pc_replenish_flag,
           pn_replenish_next_master_id,
           pv_replenish_device_name);
        IF pc_replenish_flag = 'N' THEN
            PKG_DEVICE_CONFIGURATION.NEXT_MASTER_ID_BY_SERIAL(pv_replenish_device_serial_cd, pv_replenish_device_name, pn_replenish_next_master_id);
            pn_replenish_amount := 0;
        END IF;
    END;   
    -- R44 or below
    PROCEDURE UPDATE_PENDING_REPLENISH(
        pv_replenish_device_name DEVICE.DEVICE_NAME%TYPE,
        pn_replenish_id PSS.CONSUMER_ACCT_REPLENISH.CONSUMER_ACCT_REPLENISH_ID%TYPE,
        pn_replenish_next_master_id NUMBER,
        pc_submitted_flag PSS.CONSUMER_ACCT_PEND_REPLENISH.SUBMITTED_FLAG%TYPE,
        pc_initial_replenish_flag CHAR,
        pn_denied_count OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_DENIED_COUNT%TYPE,
        pv_replenish_card_masked OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_CARD_MASKED%TYPE)
    IS
    BEGIN
      UPDATE_PENDING_REPLENISH(pv_replenish_device_name,pn_replenish_id,pn_replenish_next_master_id,pc_submitted_flag,pc_initial_replenish_flag,null,pn_denied_count,pv_replenish_card_masked);
    END;
    
    -- R45 or above
    PROCEDURE UPDATE_PENDING_REPLENISH(
        pv_replenish_device_name DEVICE.DEVICE_NAME%TYPE,
        pn_replenish_id PSS.CONSUMER_ACCT_REPLENISH.CONSUMER_ACCT_REPLENISH_ID%TYPE,
        pn_replenish_next_master_id NUMBER,
        pc_submitted_flag PSS.CONSUMER_ACCT_PEND_REPLENISH.SUBMITTED_FLAG%TYPE,
        pc_initial_replenish_flag CHAR,
        pd_expDate PSS.CONSUMER_ACCT_REPLENISH.EXPIRATION_DATE%TYPE,
        pn_denied_count OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_DENIED_COUNT%TYPE,
        pv_replenish_card_masked OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_CARD_MASKED%TYPE)
    IS
        ln_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE;
        ln_replen_consumer_acct_id PSS.CONSUMER_ACCT.REPLEN_CONSUMER_ACCT_ID%TYPE;
    BEGIN
        IF pc_initial_replenish_flag = 'Y' THEN
            IF pc_submitted_flag NOT IN('Y', 'P') THEN
                DELETE
                  FROM PSS.CONSUMER_ACCT_PEND_REPLENISH
                 WHERE CONSUMER_ACCT_REPLENISH_ID = pn_replenish_id
                   AND DEVICE_TRAN_CD = pn_replenish_next_master_id;      
                DELETE
                  FROM PSS.CONSUMER_ACCT_REPLENISH
                 WHERE CONSUMER_ACCT_REPLENISH_ID = pn_replenish_id
                RETURNING CONSUMER_ACCT_ID, REPLENISH_CARD_MASKED 
                  INTO ln_consumer_acct_id, pv_replenish_card_masked;
                pn_denied_count := 1;
            ELSE
                IF pv_replenish_device_name is not null THEN
                  select consumer_acct_id into ln_replen_consumer_acct_id from PSS.TRAN 
                  WHERE TRAN_DEVICE_TRAN_CD=to_char(pn_replenish_next_master_id)
                  AND DEVICE_NAME=pv_replenish_device_name;
                END IF;
         
                UPDATE PSS.CONSUMER_ACCT_REPLENISH CAR
                   SET PRIORITY = PRIORITY + 1, REPLENISH_AMOUNT = DECODE(CONSUMER_ACCT_REPLENISH_ID, pn_replenish_id, REPLENISH_AMOUNT, 0)
                 WHERE CONSUMER_ACCT_ID = (SELECT CONSUMER_ACCT_ID FROM PSS.CONSUMER_ACCT_REPLENISH WHERE CONSUMER_ACCT_REPLENISH_ID = pn_replenish_id);
                   
                UPDATE PSS.CONSUMER_ACCT_REPLENISH
                   SET PRIORITY = 1, REPLEN_CONSUMER_ACCT_ID=ln_replen_consumer_acct_id,
                   EXPIRATION_DATE=pd_expDate
                 WHERE CONSUMER_ACCT_REPLENISH_ID = pn_replenish_id
                  RETURNING CONSUMER_ACCT_ID, REPLENISH_CARD_MASKED
                  INTO ln_consumer_acct_id, pv_replenish_card_masked;
                UPDATE PSS.CONSUMER_ACCT_PEND_REPLENISH
                   SET SUBMITTED_FLAG = pc_submitted_flag
                 WHERE CONSUMER_ACCT_REPLENISH_ID = pn_replenish_id
                   AND DEVICE_TRAN_CD = pn_replenish_next_master_id; 
				UPDATE PSS.CONSUMER_ACCT
				SET CONSUMER_ACCT_REPLENISH_ID = pn_replenish_id, REPLEN_CONSUMER_ACCT_ID=ln_replen_consumer_acct_id
				WHERE CONSUMER_ACCT_ID = ln_consumer_acct_id;
                pn_denied_count := 0;
            END IF;
        ELSE
            UPDATE PSS.CONSUMER_ACCT_PEND_REPLENISH
               SET SUBMITTED_FLAG = pc_submitted_flag
             WHERE CONSUMER_ACCT_REPLENISH_ID = pn_replenish_id
               AND DEVICE_TRAN_CD = pn_replenish_next_master_id;
            UPDATE PSS.CONSUMER_ACCT_REPLENISH
               SET REPLENISH_DENIED_COUNT = CASE WHEN pc_submitted_flag IN('Y','P','V') THEN 0 WHEN pc_submitted_flag = '?' THEN REPLENISH_DENIED_COUNT ELSE REPLENISH_DENIED_COUNT + 1 END
             WHERE CONSUMER_ACCT_REPLENISH_ID = pn_replenish_id
              RETURNING REPLENISH_DENIED_COUNT, REPLENISH_CARD_MASKED 
              INTO pn_denied_count, pv_replenish_card_masked;
        END IF;
    END;
    
    PROCEDURE CREATE_ISIS_CONSUMER(
        pn_consumer_id OUT PSS.CONSUMER.CONSUMER_ID%TYPE,
        pv_consumer_identifier PSS.CONSUMER.CONSUMER_IDENTIFIER%TYPE)
    IS
    BEGIN
        SELECT PSS.SEQ_CONSUMER_ID.NEXTVAL 
          INTO pn_consumer_id 
          FROM DUAL;
        INSERT INTO PSS.CONSUMER(CONSUMER_ID, CONSUMER_EMAIL_ADDR1, CONSUMER_TYPE_ID, CONSUMER_IDENTIFIER)
            VALUES(pn_consumer_id, 'softcard_' || pn_consumer_id || '@usatech.com', 7, pv_consumer_identifier);
    END;
    
    -- R37+ Signature
    PROCEDURE AUTHORIZE_ISIS_PROMO(
        pn_global_account_id IN OUT PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
        pv_instance PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_INSTANCE%TYPE,
        pv_consumer_acct_cd PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD%TYPE,
        pv_consumer_identifier PSS.CONSUMER.CONSUMER_IDENTIFIER%TYPE,
        pv_currency_cd PSS.CURRENCY.CURRENCY_CD%TYPE,
        pv_auth_result_cd OUT VARCHAR2,
        pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
        pn_consumer_id OUT PSS.CONSUMER.CONSUMER_ID%TYPE,
        pn_trans_to_free_tran OUT NUMBER)
    IS
        ln_old_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE;
    BEGIN
        pv_auth_result_cd := 'N';
        pn_consumer_id := NULL;
        
        IF pv_consumer_identifier IS NOT NULL THEN
            SELECT MAX(consumer_id)
              INTO pn_consumer_id
              FROM PSS.CONSUMER
              WHERE CONSUMER_IDENTIFIER = pv_consumer_identifier;
            
            IF pn_consumer_id IS NULL THEN
                BEGIN
                    CREATE_ISIS_CONSUMER(pn_consumer_id, pv_consumer_identifier);
                EXCEPTION
                    WHEN DUP_VAL_ON_INDEX THEN
                        SELECT MAX(consumer_id)
                          INTO pn_consumer_id
                          FROM PSS.CONSUMER
                         WHERE CONSUMER_IDENTIFIER = pv_consumer_identifier;
                END;
            END IF;
        END IF;
    
        IF pn_global_account_id IS NOT NULL THEN
            SELECT MAX(CAB.CONSUMER_ACCT_ID), MAX(CA.CONSUMER_ID)
              INTO pn_consumer_acct_id, ln_old_consumer_id
              FROM PSS.CONSUMER_ACCT_BASE CAB
              LEFT OUTER JOIN PSS.CONSUMER_ACCT CA ON CAB.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
             WHERE CAB.GLOBAL_ACCOUNT_ID = pn_global_account_id
               AND CAB.CURRENCY_CD = pv_currency_cd;
            
            IF pn_consumer_id IS NULL THEN
                pn_consumer_id := ln_old_consumer_id;
        END IF;
        END IF;
        
        IF pn_consumer_acct_id IS NULL OR ln_old_consumer_id IS NULL THEN
        IF pn_consumer_acct_id IS NULL THEN
                IF pn_global_account_id IS NULL THEN
                    SELECT PSS.SEQ_GLOBAL_ACCOUNT_ID_BASE.NEXTVAL
                      INTO pn_global_account_id 
                      FROM DUAL; 
                END IF;
                
            SELECT PSS.SEQ_CONSUMER_ACCT_ID.NEXTVAL
              INTO pn_consumer_acct_id
              FROM DUAL;
                  
                INSERT INTO PSS.CONSUMER_ACCT_BASE(CONSUMER_ACCT_ID, GLOBAL_ACCOUNT_ID, GLOBAL_ACCOUNT_INSTANCE, TRUNCATED_CARD_NUMBER, PAYMENT_SUBTYPE_ID, CURRENCY_CD)
                    VALUES(pn_consumer_acct_id, pn_global_account_id, 0, DBADMIN.MASK_CREDIT_CARD(pv_consumer_acct_cd), 1, pv_currency_cd);
            END IF;
            IF ln_old_consumer_id IS NULL THEN
            IF pn_consumer_id IS NULL THEN
                CREATE_ISIS_CONSUMER(pn_consumer_id, pv_consumer_identifier);
            END IF;
            INSERT INTO PSS.CONSUMER_ACCT(CONSUMER_ACCT_ID, CONSUMER_ACCT_CD, CONSUMER_ACCT_ACTIVE_YN_FLAG, CONSUMER_ACCT_BALANCE, CONSUMER_ID, LOCATION_ID, 
                    CONSUMER_ACCT_ISSUE_NUM, PAYMENT_SUBTYPE_ID, CURRENCY_CD, CONSUMER_ACCT_TYPE_ID) 
                    VALUES(pn_consumer_acct_id, DBADMIN.MASK_CREDIT_CARD(pv_consumer_acct_cd), 'Y', 0, pn_consumer_id, 1, 1, 1, pv_currency_cd, 5);
            END IF;
        ELSIF pn_consumer_id != ln_old_consumer_id THEN
            UPDATE PSS.CONSUMER_ACCT
               SET CONSUMER_ID = pn_consumer_id
             WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id;
        END IF;
    
        UPDATE PSS.CONSUMER_PROMOTION
           SET TRAN_COUNT = tran_count + 1,
               PROMO_TRAN_COUNT = promo_tran_count + 1
         WHERE CONSUMER_ID = pn_consumer_id
           AND PROMOTION_ID = 1
           AND (TRAN_COUNT + 1) / (PROMO_TRAN_COUNT + 1) >= 5
        RETURNING 'Y' INTO pv_auth_result_cd;
            
        LOOP
            BEGIN
                SELECT 4 - MOD(TRAN_COUNT + 1, 5)
                  INTO pn_trans_to_free_tran
                  FROM PSS.CONSUMER_PROMOTION
                 WHERE CONSUMER_ID = pn_consumer_id
                   AND PROMOTION_ID = 1;
                EXIT;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    BEGIN
                        INSERT INTO PSS.CONSUMER_PROMOTION(CONSUMER_ID, PROMOTION_ID)
                            VALUES(pn_consumer_id, 1);
                    EXCEPTION
                        WHEN DUP_VAL_ON_INDEX THEN
                            NULL;
                    END;
            END;
        END LOOP;
    END;

    PROCEDURE REFUND_ISIS_PROMO(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pn_result_cd OUT NUMBER,
        pv_error_message OUT VARCHAR2)
    IS
        ln_refund_state_id PSS.REFUND.REFUND_STATE_ID%TYPE;
        ln_parent_tran_id PSS.TRAN.PARENT_TRAN_ID%TYPE;
        ln_refund_count NUMBER;
        ln_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE;
    BEGIN
        pn_result_cd := PKG_CONST.RESULT__SUCCESS;
        pv_error_message := PKG_CONST.ERROR__NO_ERROR;
        
        SELECT PARENT_TRAN_ID
        INTO ln_parent_tran_id
        FROM PSS.TRAN
        WHERE TRAN_ID = pn_tran_id;
        
        SELECT COUNT(1)
        INTO ln_refund_count
        FROM PSS.REFUND R
        JOIN PSS.TRAN T ON R.TRAN_ID = T.TRAN_ID
        WHERE T.PARENT_TRAN_ID = ln_parent_tran_id AND R.REFUND_STATE_ID IN(1);
        
        IF ln_refund_count > 0 THEN
            RETURN;
        END IF;
        
        SELECT CA.CONSUMER_ID
        INTO ln_consumer_id
        FROM PSS.TRAN T
        JOIN PSS.CONSUMER_ACCT CA ON T.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
        WHERE T.TRAN_ID = pn_tran_id;
        
        UPDATE PSS.CONSUMER_PROMOTION
        SET TRAN_COUNT = CASE WHEN TRAN_COUNT - 1 >= 0 THEN TRAN_COUNT - 1 ELSE 0 END,
            PROMO_TRAN_COUNT = CASE WHEN PROMO_TRAN_COUNT - 1 >= 0 THEN PROMO_TRAN_COUNT - 1 ELSE 0 END
        WHERE CONSUMER_ID = ln_consumer_id AND PROMOTION_ID = 1;
    END;
    
    PROCEDURE GET_OR_CREATE_CONSUMER_ACCT(
        pn_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
        pn_global_account_instance PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_INSTANCE%TYPE,
        pn_pos_pta_id PSS.POS_PTA.POS_PTA_ID%TYPE,
        pv_currency_cd PSS.CONSUMER_ACCT_BASE.CURRENCY_CD%TYPE,
        pd_auth_ts DATE,
        pv_truncated_card_num PSS.CONSUMER_ACCT_BASE.TRUNCATED_CARD_NUMBER%TYPE,
        pn_consumer_acct_id OUT PSS.CONSUMER_ACCT_BASE.CONSUMER_ACCT_ID%TYPE)
    IS
        ln_new_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE;
        ln_new_global_account_instance PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_INSTANCE%TYPE;
    BEGIN
        SELECT MAX(CONSUMER_ACCT_ID)
          INTO pn_consumer_acct_id
          FROM (        
             SELECT CAB.CONSUMER_ACCT_ID  
               FROM (
                SELECT CAB.CONSUMER_ACCT_ID,
                       CASE WHEN CA.CONSUMER_ACCT_ACTIVE_YN_FLAG IS NULL THEN 5 WHEN CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y' AND NVL(CA.CONSUMER_ACCT_ACTIVATION_TS, MIN_DATE) <= pd_auth_ts AND NVL(CA.CONSUMER_ACCT_DEACTIVATION_TS, MAX_DATE) > pd_auth_ts THEN 1 ELSE 10 END CA_PRIORITY,
                       CAB.PAYMENT_SUBTYPE_ID,
                       CAB.LOCATION_ID,
                       CASE WHEN PTA.PAYMENT_SUBTYPE_ID = CAB.PAYMENT_SUBTYPE_ID THEN 1 ELSE 10 END PST_PRIORITY,
                       POS.LOCATION_ID POS_LOCATION_ID,
                       CA.CONSUMER_ACCT_ISSUE_NUM,
                       CASE WHEN CAB.LOCATION_ID IS NULL THEN NULL ELSE (SELECT VLH.DEPTH FROM LOCATION.VW_LOCATION_HIERARCHY VLH WHERE VLH.DESCENDENT_LOCATION_ID = POS.LOCATION_ID AND VLH.ANCESTOR_LOCATION_ID = CAB.LOCATION_ID) END DEPTH
                  FROM PSS.CONSUMER_ACCT_BASE CAB
                  LEFT OUTER JOIN PSS.CONSUMER_ACCT CA ON CAB.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
                 CROSS JOIN PSS.POS_PTA PTA
                  JOIN PSS.POS POS ON PTA.POS_ID = POS.POS_ID          
                 WHERE CAB.GLOBAL_ACCOUNT_ID = pn_global_account_id
                   AND PTA.POS_PTA_ID = pn_pos_pta_id 
                   AND CAB.CURRENCY_CD = pv_currency_cd) CAB
                ORDER BY CAB.CA_PRIORITY, CAB.PST_PRIORITY, CAB.DEPTH NULLS LAST, CAB.CONSUMER_ACCT_ISSUE_NUM DESC
        ) WHERE ROWNUM = 1;
        IF pn_consumer_acct_id IS NULL THEN
           SELECT MAX(NEW_GLOBAL_ACCOUNT_ID), MAX(NEW_GLOBAL_ACCOUNT_INSTANCE)
              INTO ln_new_global_account_id, ln_new_global_account_instance
              FROM PSS.GLOBAL_ACCOUNT_OVERWRITE
             WHERE OLD_GLOBAL_ACCOUNT_ID = pn_global_account_id;
           IF ln_new_global_account_id IS NOT NULL AND ln_new_global_account_id != pn_global_account_id THEN   
                GET_OR_CREATE_CONSUMER_ACCT(ln_new_global_account_id, ln_new_global_account_instance, pn_pos_pta_id, pv_currency_cd, pd_auth_ts, pv_truncated_card_num, pn_consumer_acct_id);
           ELSE
                SELECT PSS.SEQ_CONSUMER_ACCT_ID.NEXTVAL
                  INTO pn_consumer_acct_id
                  FROM DUAL;
                INSERT INTO PSS.CONSUMER_ACCT_BASE(CONSUMER_ACCT_ID, GLOBAL_ACCOUNT_ID, GLOBAL_ACCOUNT_INSTANCE, TRUNCATED_CARD_NUMBER, PAYMENT_SUBTYPE_ID, CURRENCY_CD) 
                    SELECT pn_consumer_acct_id, pn_global_account_id, NVL(pn_global_account_instance, MOD(pn_global_account_id, 10)), SUBSTR(pv_truncated_card_num, 1, 50), PTA.PAYMENT_SUBTYPE_ID, pv_currency_cd
                      FROM PSS.POS_PTA PTA
                     WHERE PTA.POS_PTA_ID = pn_pos_pta_id;
            END IF;
        END IF;
    END;
    
    PROCEDURE RESOLVE_ACCOUNT_CONFLICT(
        pn_old_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
        pn_new_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
        pn_global_account_instance PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_INSTANCE%TYPE)
    IS
        CURSOR l_old_cur IS
            SELECT CAB.CONSUMER_ACCT_ID, CA.CONSUMER_ID, CAB.CURRENCY_CD, CAB.LOCATION_ID, CAB.PAYMENT_SUBTYPE_ID
              FROM PSS.CONSUMER_ACCT_BASE CAB
              LEFT OUTER JOIN PSS.CONSUMER_ACCT CA ON CAB.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
             WHERE CAB.GLOBAL_ACCOUNT_ID = pn_old_global_account_id;
        ln_newest_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE;
        ln_newest_instance PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_INSTANCE%TYPE;
        ln_new_consumer_acct_id PSS.CONSUMER_ACCT_BASE.CONSUMER_ACCT_ID%TYPE;
        ln_new_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE;
        lv_new_consumer_identifier PSS.CONSUMER.CONSUMER_IDENTIFIER%TYPE;
        lv_old_consumer_identifier PSS.CONSUMER.CONSUMER_IDENTIFIER%TYPE;
    BEGIN
        BEGIN
            INSERT INTO PSS.GLOBAL_ACCOUNT_OVERWRITE(OLD_GLOBAL_ACCOUNT_ID, NEW_GLOBAL_ACCOUNT_ID, NEW_GLOBAL_ACCOUNT_INSTANCE)
                VALUES(pn_old_global_account_id, pn_new_global_account_id, pn_global_account_instance);
        EXCEPTION
            WHEN DUP_VAL_ON_INDEX THEN
                NULL;
        END;
        SELECT NVL(MAX(NEW_GLOBAL_ACCOUNT_ID), pn_new_global_account_id), NVL(MAX(NEW_GLOBAL_ACCOUNT_INSTANCE), pn_global_account_instance)
          INTO ln_newest_global_account_id, ln_newest_instance
          FROM (
                SELECT NEW_GLOBAL_ACCOUNT_ID,
                       NEW_GLOBAL_ACCOUNT_INSTANCE,
                       CONNECT_BY_ISLEAF ISLEAF
                  FROM PSS.GLOBAL_ACCOUNT_OVERWRITE
                 START WITH OLD_GLOBAL_ACCOUNT_ID = pn_new_global_account_id
                 CONNECT BY NOCYCLE PRIOR NEW_GLOBAL_ACCOUNT_ID = OLD_GLOBAL_ACCOUNT_ID)
          WHERE ISLEAF = 1;
        FOR l_old_rec IN l_old_cur LOOP
            -- Find best new CA
            SELECT MAX(CONSUMER_ACCT_ID), MAX(CONSUMER_ID), MAX(CONSUMER_IDENTIFIER)
              INTO ln_new_consumer_acct_id, ln_new_consumer_id, lv_new_consumer_identifier
              FROM (
            SELECT CAB.CONSUMER_ACCT_ID, CAB.CONSUMER_ID, CAB.CONSUMER_IDENTIFIER
              FROM (
                SELECT CAB.CONSUMER_ACCT_ID, 
                       CASE WHEN CA.CONSUMER_ACCT_ACTIVE_YN_FLAG IS NULL THEN 5 WHEN CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y' AND NVL(CA.CONSUMER_ACCT_ACTIVATION_TS, MIN_DATE) <= SYSDATE AND NVL(CA.CONSUMER_ACCT_DEACTIVATION_TS, MAX_DATE) > SYSDATE THEN 1 ELSE 10 END CA_PRIORITY,
                       CASE WHEN l_old_rec.PAYMENT_SUBTYPE_ID = CAB.PAYMENT_SUBTYPE_ID THEN 1 ELSE 10 END PST_PRIORITY,
                       CASE WHEN NVL(l_old_rec.LOCATION_ID, 0) = NVL(CAB.LOCATION_ID, 0) THEN 1 ELSE 10 END LOCATION_PRIORITY,
                       CA.CONSUMER_ACCT_ISSUE_NUM,
                       C.CONSUMER_ID,
                       C.CONSUMER_IDENTIFIER
                  FROM PSS.CONSUMER_ACCT_BASE CAB
                  LEFT OUTER JOIN (PSS.CONSUMER_ACCT CA 
                  JOIN PSS.CONSUMER C ON CA.CONSUMER_ID = C.CONSUMER_ID) ON CAB.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
                 WHERE CAB.GLOBAL_ACCOUNT_ID = ln_newest_global_account_id
                   AND CAB.CURRENCY_CD = l_old_rec.CURRENCY_CD) CAB
             ORDER BY CAB.CA_PRIORITY, CAB.PST_PRIORITY, CAB.LOCATION_PRIORITY, CAB.CONSUMER_ACCT_ISSUE_NUM DESC, CAB.CONSUMER_IDENTIFIER NULLS LAST
             ) WHERE ROWNUM = 1;
            IF ln_new_consumer_acct_id IS NULL THEN -- This occurs when currency of newest is different than old; we can just update old's global_account_id
                UPDATE PSS.CONSUMER_ACCT_BASE
                   SET GLOBAL_ACCOUNT_ID = ln_newest_global_account_id, GLOBAL_ACCOUNT_INSTANCE = ln_newest_instance
                 WHERE GLOBAL_ACCOUNT_ID = pn_old_global_account_id;
            ELSE
                IF l_old_rec.CONSUMER_ID IS NOT NULL THEN
                    IF ln_new_consumer_id IS NULL THEN
                        UPDATE PSS.CONSUMER_ACCT 
                           SET CONSUMER_ACCT_ID = ln_new_consumer_acct_id
                         WHERE CONSUMER_ACCT_ID = l_old_rec.CONSUMER_ACCT_ID;            
                    ELSE
                        DELETE 
                          FROM PSS.CONSUMER_ACCT
                         WHERE CONSUMER_ACCT_ID = l_old_rec.CONSUMER_ACCT_ID;  
                        IF ln_new_consumer_id != l_old_rec.CONSUMER_ID THEN 
                            UPDATE PSS.CONSUMER_ACCT 
                               SET CONSUMER_ID = ln_new_consumer_id
                             WHERE CONSUMER_ID = l_old_rec.CONSUMER_ID;
                             
                            UPDATE PSS.CONSUMER_PROMOTION CP
                               SET (TRAN_COUNT, PROMO_TRAN_COUNT) = 
                                   (SELECT NVL(SUM(CP0.TRAN_COUNT), 0) + CP.TRAN_COUNT, NVL(SUM(CP0.PROMO_TRAN_COUNT), 0) + CP.PROMO_TRAN_COUNT
                                      FROM PSS.CONSUMER_PROMOTION CP0
                                     WHERE CP0.CONSUMER_ID = l_old_rec.CONSUMER_ID)
                             WHERE CP.CONSUMER_ID = ln_new_consumer_id;
                            
                            DELETE FROM PSS.CONSUMER_PROMOTION
                             WHERE CONSUMER_ID = l_old_rec.CONSUMER_ID;
                            
                            DELETE FROM PSS.CONSUMER
                             WHERE CONSUMER_ID = l_old_rec.CONSUMER_ID
                             RETURNING CONSUMER_IDENTIFIER INTO lv_old_consumer_identifier;  
                            
                            IF lv_new_consumer_identifier IS NULL AND lv_old_consumer_identifier IS NOT NULL THEN
                                UPDATE PSS.CONSUMER
                                  SET CONSUMER_IDENTIFIER = lv_old_consumer_identifier
                                WHERE CONSUMER_ID = ln_new_consumer_id
                                  AND CONSUMER_IDENTIFIER IS NULL;
                            END IF;
                       END IF;
                    END IF; 
                END IF;
    
                UPDATE PSS.CONSUMER_ACCT_DEVICE CAD
                   SET (USED_COUNT, LAST_USED_UTC_TS) = 
                       (SELECT NVL(SUM(CAD0.USED_COUNT), 0) + CAD.USED_COUNT, NULLIF(GREATEST(NVL(MAX(CAD0.LAST_USED_UTC_TS), MIN_DATE), NVL(CAD.LAST_USED_UTC_TS, MIN_DATE)), MIN_DATE)
                          FROM PSS.CONSUMER_ACCT_DEVICE CAD0
                         WHERE CAD0.CONSUMER_ACCT_ID = l_old_rec.CONSUMER_ACCT_ID)
                 WHERE CAD.CONSUMER_ACCT_ID = ln_new_consumer_acct_id;
                
                IF SQL%ROWCOUNT > 0 THEN
                    DELETE FROM PSS.CONSUMER_ACCT_DEVICE 
                     WHERE CONSUMER_ACCT_ID = l_old_rec.CONSUMER_ACCT_ID;
                ELSE
                    UPDATE PSS.CONSUMER_ACCT_DEVICE
                       SET CONSUMER_ACCT_ID = ln_new_consumer_acct_id
                     WHERE CONSUMER_ACCT_ID = l_old_rec.CONSUMER_ACCT_ID;
                END IF;
                 
                UPDATE PSS.TRAN
                   SET CONSUMER_ACCT_ID = ln_new_consumer_acct_id
                 WHERE CONSUMER_ACCT_ID = l_old_rec.CONSUMER_ACCT_ID;
                
                UPDATE REPORT.TRANS
                   SET CONSUMER_ACCT_ID = ln_new_consumer_acct_id
                 WHERE CONSUMER_ACCT_ID = l_old_rec.CONSUMER_ACCT_ID;
                
                UPDATE REPORT.ACTIVITY_REF
                   SET CONSUMER_ACCT_ID = ln_new_consumer_acct_id
                 WHERE CONSUMER_ACCT_ID = l_old_rec.CONSUMER_ACCT_ID;
                       
                DELETE 
                  FROM PSS.CONSUMER_ACCT_BASE
                 WHERE CONSUMER_ACCT_ID = l_old_rec.CONSUMER_ACCT_ID;  
             END IF;
             COMMIT;
         END LOOP;
    END;
    
    PROCEDURE UPDATE_TRAN_ACCOUNT(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pn_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
        pn_global_account_instance PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_INSTANCE%TYPE)
    IS
        ln_consumer_acct_id PSS.CONSUMER_ACCT_BASE.CONSUMER_ACCT_ID%TYPE;
        ln_pos_pta_id PSS.POS_PTA.POS_PTA_ID%TYPE;
        lv_currency_cd PSS.CONSUMER_ACCT_BASE.CURRENCY_CD%TYPE;
        ld_auth_ts DATE;
        lv_truncated_card_num PSS.CONSUMER_ACCT_BASE.TRUNCATED_CARD_NUMBER%TYPE; 
        lv_machine_trans_no PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE;
        ln_report_tran_id REPORT.TRANS.TRAN_ID%TYPE;
    BEGIN
        SELECT X.POS_PTA_ID, NVL(PP.CURRENCY_CD, 'USD'), COALESCE(MIN(A.AUTH_TS), X.TRAN_UPLOAD_TS, X.CREATED_TS), X.TRAN_RECEIVED_RAW_ACCT_DATA, X.TRAN_GLOBAL_TRANS_CD
          INTO ln_pos_pta_id, lv_currency_cd, ld_auth_ts, lv_truncated_card_num, lv_machine_trans_no
          FROM PSS.TRAN X
          JOIN PSS.POS_PTA PP ON X.POS_PTA_ID = PP.POS_PTA_ID
          LEFT OUTER JOIN PSS.AUTH A ON X.TRAN_ID = A.TRAN_ID AND A.AUTH_TYPE_CD IN('L', 'N')
         WHERE X.TRAN_ID = pn_tran_id 
         GROUP BY X.POS_PTA_ID, PP.CURRENCY_CD, X.TRAN_UPLOAD_TS, X.CREATED_TS, X.TRAN_RECEIVED_RAW_ACCT_DATA, X.TRAN_GLOBAL_TRANS_CD;
         
        -- create CAB row if needed
        GET_OR_CREATE_CONSUMER_ACCT(pn_global_account_id, pn_global_account_instance, ln_pos_pta_id, lv_currency_cd, ld_auth_ts, lv_truncated_card_num, ln_consumer_acct_id);
        
        -- update TRAN
        UPDATE PSS.TRAN
           SET CONSUMER_ACCT_ID = ln_consumer_acct_id
         WHERE TRAN_ID = pn_tran_id;
        UPDATE REPORT.TRANS
           SET CONSUMER_ACCT_ID = ln_consumer_acct_id
         WHERE MACHINE_TRANS_NO = lv_machine_trans_no
          RETURNING TRAN_ID INTO ln_report_tran_id;  
        UPDATE REPORT.ACTIVITY_REF
           SET CONSUMER_ACCT_ID = ln_consumer_acct_id
         WHERE TRAN_ID = ln_report_tran_id;
    END;
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/PKG_CUSTOMER_MANAGEMENT.psk?rev=1.28
CREATE OR REPLACE PACKAGE REPORT.PKG_CUSTOMER_MANAGEMENT AS
    PROCEDURE CREATE_TERMINAL(
        pn_terminal_id OUT REPORT.TERMINAL.TERMINAL_ID%TYPE,
        pv_device_serial_cd IN REPORT.EPORT.EPORT_SERIAL_NUM%TYPE,
        pn_dealer_id IN CORP.DEALER.DEALER_ID%TYPE,
        pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
        pn_machine_id IN REPORT.TERMINAL.MACHINE_ID%TYPE,
        pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
        pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
        pn_region_id IN REPORT.REGION.REGION_ID%TYPE,
        pv_region_name IN REPORT.REGION.REGION_NAME%TYPE,
        pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
        pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
        pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
        pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
        pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
        pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
        pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
        pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
        pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_pc_id IN REPORT.TERMINAL.PRIMARY_CONTACT_ID%TYPE,
        pn_sc_id IN REPORT.TERMINAL.SECONDARY_CONTACT_ID%TYPE,
        pn_location_type_id IN REPORT.LOCATION.LOCATION_TYPE_ID%TYPE,
        pv_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
        pn_product_type_id IN REPORT.TERMINAL.PRODUCT_TYPE_ID%TYPE,
        pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
        pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
        pn_avg_amt IN REPORT.TERMINAL.AVG_TRANS_AMT%TYPE,
        pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
        pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
        pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
        pn_vends REPORT.TERMINAL.VENDS%TYPE,
        pv_term_var_1 IN REPORT.TERMINAL.TERM_VAR_1%TYPE,
        pv_term_var_2 IN REPORT.TERMINAL.TERM_VAR_2%TYPE,
        pv_term_var_3 IN REPORT.TERMINAL.TERM_VAR_3%TYPE,
        pv_term_var_4 IN REPORT.TERMINAL.TERM_VAR_4%TYPE,
        pv_term_var_5 IN REPORT.TERMINAL.TERM_VAR_5%TYPE,
        pv_term_var_6 IN REPORT.TERMINAL.TERM_VAR_6%TYPE,
        pv_term_var_7 IN REPORT.TERMINAL.TERM_VAR_7%TYPE,
        pv_term_var_8 IN REPORT.TERMINAL.TERM_VAR_8%TYPE,
        pd_activate_date IN REPORT.TERMINAL_EPORT.START_DATE%TYPE DEFAULT SYSDATE - 30,
        pn_fee_grace_days IN NUMBER DEFAULT 60,
        pv_doing_business_as IN REPORT.TERMINAL.DOING_BUSINESS_AS%TYPE DEFAULT NULL,
        pc_fee_no_trigger_event_flag IN CORP.SERVICE_FEES.NO_TRIGGER_EVENT_FLAG%TYPE DEFAULT NULL,
        pv_sales_order_number IN REPORT.TERMINAL.SALES_ORDER_NUMBER%TYPE DEFAULT NULL,
        pv_purchase_order_number IN REPORT.TERMINAL.PURCHASE_ORDER_NUMBER%TYPE DEFAULT NULL,
        pc_pos_environment_cd IN REPORT.TERMINAL.POS_ENVIRONMENT_CD%TYPE DEFAULT NULL,
        pv_entry_capability_cds IN REPORT.TERMINAL.ENTRY_CAPABILITY_CDS%TYPE DEFAULT NULL,
        pc_pin_capability_flag IN REPORT.TERMINAL.PIN_CAPABILITY_FLAG%TYPE DEFAULT NULL,
        pn_commission_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE DEFAULT NULL,
        pn_business_type_id IN REPORT.TERMINAL.BUSINESS_TYPE_ID%TYPE DEFAULT NULL,
       pv_customer_service_phone IN REPORT.TERMINAL.CUSTOMER_SERVICE_PHONE%TYPE DEFAULT NULL,
       pv_customer_service_email IN REPORT.TERMINAL.CUSTOMER_SERVICE_EMAIL%TYPE DEFAULT NULL);
    
    PROCEDURE CREATE_TERMINAL_MASS(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_terminal_id OUT REPORT.TERMINAL.TERMINAL_ID%TYPE,
        pv_device_serial_cd IN REPORT.EPORT.EPORT_SERIAL_NUM%TYPE,
        pn_dealer_id IN CORP.DEALER.DEALER_ID%TYPE,
        pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
        pv_machine_make REPORT.MACHINE.MAKE%TYPE,
        pv_machine_model REPORT.MACHINE.MODEL%TYPE,
        pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
        pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
        pv_region_name IN REPORT.REGION.REGION_NAME%TYPE,
        pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
        pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
        pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
        pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
        pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
        pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
        pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
        pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
        pv_location_type_name REPORT.LOCATION_TYPE.LOCATION_TYPE_NAME%TYPE,
        pn_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
        pv_product_type_name REPORT.PRODUCT_TYPE.PRODUCT_TYPE_NAME%TYPE,
        pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
        pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
        pn_avg_amt IN REPORT.TERMINAL.AVG_TRANS_AMT%TYPE,
        pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
        pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
        pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
        pn_vends REPORT.TERMINAL.VENDS%TYPE,
        pv_term_var_1 REPORT.TERMINAL.term_var_1%TYPE,
        pv_term_var_2 REPORT.TERMINAL.term_var_2%TYPE,
        pv_term_var_3 REPORT.TERMINAL.term_var_3%TYPE,
        pv_term_var_4 REPORT.TERMINAL.term_var_4%TYPE,
        pv_term_var_5 REPORT.TERMINAL.term_var_5%TYPE,
        pv_term_var_6 REPORT.TERMINAL.term_var_6%TYPE,
        pv_term_var_7 REPORT.TERMINAL.term_var_7%TYPE,
        pv_term_var_8 REPORT.TERMINAL.term_var_8%TYPE,
        pd_activate_date IN REPORT.TERMINAL_EPORT.START_DATE%TYPE DEFAULT SYSDATE - 30,
        pn_fee_grace_days IN NUMBER DEFAULT 60,
        pc_keep_existing_data IN CHAR DEFAULT 'N',
        pc_override_payment_schedule IN CHAR DEFAULT 'N',
        pv_doing_business_as IN REPORT.TERMINAL.DOING_BUSINESS_AS%TYPE DEFAULT NULL,
        pc_fee_no_trigger_event_flag IN CORP.SERVICE_FEES.NO_TRIGGER_EVENT_FLAG%TYPE DEFAULT NULL,
        pv_sales_order_number IN REPORT.TERMINAL.SALES_ORDER_NUMBER%TYPE DEFAULT NULL,
        pv_purchase_order_number IN REPORT.TERMINAL.PURCHASE_ORDER_NUMBER%TYPE DEFAULT NULL,
        pc_pos_environment_cd IN REPORT.TERMINAL.POS_ENVIRONMENT_CD%TYPE DEFAULT NULL,
        pv_entry_capability_cds IN REPORT.TERMINAL.ENTRY_CAPABILITY_CDS%TYPE DEFAULT NULL,
        pc_pin_capability_flag IN REPORT.TERMINAL.PIN_CAPABILITY_FLAG%TYPE DEFAULT NULL,
        pn_commission_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE DEFAULT NULL,
        pv_business_type_name IN REPORT.BUSINESS_TYPE.BUSINESS_TYPE_NAME%TYPE DEFAULT NULL,
       pv_customer_service_phone IN REPORT.TERMINAL.CUSTOMER_SERVICE_PHONE%TYPE DEFAULT NULL,
       pv_customer_service_email IN REPORT.TERMINAL.CUSTOMER_SERVICE_EMAIL%TYPE DEFAULT NULL);
        
    PROCEDURE UPDATE_TERMINAL(
       pn_terminal_id IN REPORT.TERMINAL.TERMINAL_ID%TYPE,
       pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
       pn_machine_id IN REPORT.TERMINAL.MACHINE_ID%TYPE,
       pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
       pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
       pn_region_id IN REPORT.REGION.REGION_ID%TYPE,
       pv_region_name IN REPORT.REGION.REGION_NAME%TYPE,
       pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
       pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
       pn_address_id REPORT.TERMINAL_ADDR.ADDRESS_ID%TYPE,
       pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
       pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
       pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
       pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
       pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
       pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
       pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
       pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
       pn_pc_id IN REPORT.TERMINAL.PRIMARY_CONTACT_ID%TYPE,
       pn_sc_id IN REPORT.TERMINAL.SECONDARY_CONTACT_ID%TYPE,
       pn_location_type_id IN REPORT.LOCATION.LOCATION_TYPE_ID%TYPE,
       pn_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
       pn_product_type_id IN REPORT.TERMINAL.PRODUCT_TYPE_ID%TYPE,
       pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
       pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
       pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
       pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
       pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
       pn_vends REPORT.TERMINAL.VENDS%TYPE,
       pv_term_var_1 REPORT.TERMINAL.term_var_1%TYPE,
       pv_term_var_2 REPORT.TERMINAL.term_var_2%TYPE,
       pv_term_var_3 REPORT.TERMINAL.term_var_3%TYPE,
       pv_term_var_4 REPORT.TERMINAL.term_var_4%TYPE,
       pv_term_var_5 REPORT.TERMINAL.term_var_5%TYPE,
       pv_term_var_6 REPORT.TERMINAL.term_var_6%TYPE,
       pv_term_var_7 REPORT.TERMINAL.term_var_7%TYPE,
       pv_term_var_8 REPORT.TERMINAL.term_var_8%TYPE,
       pv_doing_business_as IN REPORT.TERMINAL.DOING_BUSINESS_AS%TYPE DEFAULT NULL,
       pc_pos_environment_cd IN REPORT.TERMINAL.POS_ENVIRONMENT_CD%TYPE DEFAULT NULL,
       pv_entry_capability_cds IN REPORT.TERMINAL.ENTRY_CAPABILITY_CDS%TYPE DEFAULT NULL,
       pc_pin_capability_flag IN REPORT.TERMINAL.PIN_CAPABILITY_FLAG%TYPE DEFAULT NULL,
       pn_business_type_id IN REPORT.TERMINAL.BUSINESS_TYPE_ID%TYPE DEFAULT NULL,
       pv_customer_service_phone IN REPORT.TERMINAL.CUSTOMER_SERVICE_PHONE%TYPE DEFAULT NULL,
       pv_customer_service_email IN REPORT.TERMINAL.CUSTOMER_SERVICE_EMAIL%TYPE DEFAULT NULL);
       
   PROCEDURE UPDATE_TERMINAL_MASS(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_terminal_id IN REPORT.TERMINAL.TERMINAL_ID%TYPE,
        pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
        pv_machine_make REPORT.MACHINE.MAKE%TYPE,
        pv_machine_model REPORT.MACHINE.MODEL%TYPE,
        pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
        pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
        pv_region_name IN REPORT.REGION.REGION_NAME%TYPE,
        pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
        pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
        pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
        pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
        pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
        pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
        pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
        pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
        pn_location_type_id REPORT.LOCATION_TYPE.LOCATION_TYPE_ID%TYPE,
        pv_location_type_name REPORT.LOCATION_TYPE.LOCATION_TYPE_NAME%TYPE,
        pn_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
        pv_product_type_name REPORT.PRODUCT_TYPE.PRODUCT_TYPE_NAME%TYPE,
        pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
        pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
        pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
        pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
        pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
        pn_vends REPORT.TERMINAL.VENDS%TYPE,
        pv_term_var_1 REPORT.TERMINAL.term_var_1%TYPE,
        pv_term_var_2 REPORT.TERMINAL.term_var_2%TYPE,
        pv_term_var_3 REPORT.TERMINAL.term_var_3%TYPE,
        pv_term_var_4 REPORT.TERMINAL.term_var_4%TYPE,
        pv_term_var_5 REPORT.TERMINAL.term_var_5%TYPE,
        pv_term_var_6 REPORT.TERMINAL.term_var_6%TYPE,
        pv_term_var_7 REPORT.TERMINAL.term_var_7%TYPE,
        pv_term_var_8 REPORT.TERMINAL.term_var_8%TYPE,
        pv_doing_business_as IN REPORT.TERMINAL.DOING_BUSINESS_AS%TYPE DEFAULT NULL,
        pc_pos_environment_cd IN REPORT.TERMINAL.POS_ENVIRONMENT_CD%TYPE DEFAULT NULL,
        pv_entry_capability_cds IN REPORT.TERMINAL.ENTRY_CAPABILITY_CDS%TYPE DEFAULT NULL,
        pc_pin_capability_flag IN REPORT.TERMINAL.PIN_CAPABILITY_FLAG%TYPE DEFAULT NULL,
        pv_business_type_name IN REPORT.BUSINESS_TYPE.BUSINESS_TYPE_NAME%TYPE DEFAULT NULL,
       pv_customer_service_phone IN REPORT.TERMINAL.CUSTOMER_SERVICE_PHONE%TYPE DEFAULT NULL,
       pv_customer_service_email IN REPORT.TERMINAL.CUSTOMER_SERVICE_EMAIL%TYPE DEFAULT NULL);
     
    -- pre usalive 1.9 signature   
    PROCEDURE CREATE_CUSTOMER_BANK(
        pn_customer_bank_id OUT CORP.CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        pn_customer_id IN CORP.CUSTOMER_BANK.CUSTOMER_ID%TYPE,
        pn_user_id IN CORP.CUSTOMER.USER_ID%TYPE,
        pv_bank_name IN CORP.CUSTOMER_BANK.BANK_NAME%TYPE,
        pv_bank_address1 IN CORP.CUSTOMER_BANK.BANK_ADDRESS%TYPE,
        pv_bank_city IN CORP.CUSTOMER_BANK.BANK_CITY%TYPE,
        pv_bank_state_cd IN CORP.CUSTOMER_BANK.BANK_STATE%TYPE,
        pv_bank_postal IN CORP.CUSTOMER_BANK.BANK_ZIP%TYPE,
        pv_bank_country_cd IN CORP.CUSTOMER_BANK.BANK_COUNTRY_CD%TYPE,
        pv_account_title IN CORP.CUSTOMER_BANK.ACCOUNT_TITLE%TYPE,
        pv_account_type IN CORP.CUSTOMER_BANK.ACCOUNT_TYPE%TYPE,
        pv_bank_routing_nbr IN CORP.CUSTOMER_BANK.BANK_ROUTING_NBR%TYPE,
        pv_bank_acct_nbr IN CORP.CUSTOMER_BANK.BANK_ACCT_NBR%TYPE,
        pv_contact_name IN CORP.CUSTOMER_BANK.CONTACT_NAME%TYPE,
        pv_contact_title IN CORP.CUSTOMER_BANK.CONTACT_TITLE%TYPE,
        pv_contact_telephone IN CORP.CUSTOMER_BANK.CONTACT_TELEPHONE%TYPE,
        pv_contact_fax IN CORP.CUSTOMER_BANK.CONTACT_FAX%TYPE);
    
    -- usalive 1.9 signature    
    PROCEDURE CREATE_CUSTOMER_BANK(
        pn_customer_bank_id OUT CORP.CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        pn_customer_id IN CORP.CUSTOMER_BANK.CUSTOMER_ID%TYPE,
        pn_user_id IN CORP.CUSTOMER.USER_ID%TYPE,
        pv_bank_name IN CORP.CUSTOMER_BANK.BANK_NAME%TYPE,
        pv_bank_address1 IN CORP.CUSTOMER_BANK.BANK_ADDRESS%TYPE,
        pv_bank_city IN CORP.CUSTOMER_BANK.BANK_CITY%TYPE,
        pv_bank_state_cd IN CORP.CUSTOMER_BANK.BANK_STATE%TYPE,
        pv_bank_postal IN CORP.CUSTOMER_BANK.BANK_ZIP%TYPE,
        pv_bank_country_cd IN CORP.CUSTOMER_BANK.BANK_COUNTRY_CD%TYPE,
        pv_account_title IN CORP.CUSTOMER_BANK.ACCOUNT_TITLE%TYPE,
        pv_account_type IN CORP.CUSTOMER_BANK.ACCOUNT_TYPE%TYPE,
        pv_bank_routing_nbr IN CORP.CUSTOMER_BANK.BANK_ROUTING_NBR%TYPE,
        pv_bank_acct_nbr IN CORP.CUSTOMER_BANK.BANK_ACCT_NBR%TYPE,
        pv_contact_name IN CORP.CUSTOMER_BANK.CONTACT_NAME%TYPE,
        pv_contact_title IN CORP.CUSTOMER_BANK.CONTACT_TITLE%TYPE,
        pv_contact_telephone IN CORP.CUSTOMER_BANK.CONTACT_TELEPHONE%TYPE,
        pv_contact_fax IN CORP.CUSTOMER_BANK.CONTACT_FAX%TYPE,
        pv_swift_code IN CORP.CUSTOMER_BANK.SWIFT_CODE%TYPE,
        pn_pay_cycle_id IN CORP.CUSTOMER_BANK.PAY_CYCLE_ID%TYPE DEFAULT 3);
        
    PROCEDURE UPDATE_REGION_NAME(
        pn_region_id REPORT.REGION.REGION_ID%TYPE,
        pv_region_name REPORT.REGION.REGION_NAME%TYPE,
        pn_customer_id REPORT.TERMINAL.CUSTOMER_ID%TYPE);
   --R44 and below     
   PROCEDURE CREATE_CAMPAIGN(
        pv_campaign_name REPORT.CAMPAIGN.CAMPAIGN_NAME%TYPE,
        pv_campaign_desc REPORT.CAMPAIGN.CAMPAIGN_DESC%TYPE,
        pn_campaign_type_id REPORT.CAMPAIGN.CAMPAIGN_TYPE_ID%TYPE,
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pd_start_date REPORT.CAMPAIGN.START_DATE%TYPE,
        pd_end_date REPORT.CAMPAIGN.END_DATE%TYPE,
        pn_discount_percent REPORT.CAMPAIGN.DISCOUNT_PERCENT%TYPE,
        pn_recur_schedule REPORT.CAMPAIGN.RECUR_SCHEDULE%TYPE,
        pn_threshold_amount REPORT.CAMPAIGN.THRESHOLD_AMOUNT%TYPE,
        pn_campaign_id OUT REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE
        );
   --R45 and above   
   PROCEDURE CREATE_CAMPAIGN(
        pv_campaign_name REPORT.CAMPAIGN.CAMPAIGN_NAME%TYPE,
        pv_campaign_desc REPORT.CAMPAIGN.CAMPAIGN_DESC%TYPE,
        pn_campaign_type_id REPORT.CAMPAIGN.CAMPAIGN_TYPE_ID%TYPE,
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pd_start_date REPORT.CAMPAIGN.START_DATE%TYPE,
        pd_end_date REPORT.CAMPAIGN.END_DATE%TYPE,
        pn_discount_percent REPORT.CAMPAIGN.DISCOUNT_PERCENT%TYPE,
        pn_donation_percent REPORT.CAMPAIGN.DONATION_PERCENT%TYPE,
        pn_recur_schedule REPORT.CAMPAIGN.RECUR_SCHEDULE%TYPE,
        pn_threshold_amount REPORT.CAMPAIGN.THRESHOLD_AMOUNT%TYPE,
        pn_campaign_id OUT REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE
        );
   --R44 and below          
   PROCEDURE EDIT_CAMPAIGN(
        pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
        pv_campaign_name REPORT.CAMPAIGN.CAMPAIGN_NAME%TYPE,
        pv_campaign_desc REPORT.CAMPAIGN.CAMPAIGN_DESC%TYPE,
        pn_campaign_type_id REPORT.CAMPAIGN.CAMPAIGN_TYPE_ID%TYPE,
        pd_start_date REPORT.CAMPAIGN.START_DATE%TYPE,
        pd_end_date REPORT.CAMPAIGN.END_DATE%TYPE,
        pn_discount_percent REPORT.CAMPAIGN.DISCOUNT_PERCENT%TYPE,
        pn_recur_schedule REPORT.CAMPAIGN.RECUR_SCHEDULE%TYPE,
        pn_threshold_amount REPORT.CAMPAIGN.THRESHOLD_AMOUNT%TYPE
        );
  --R45 and above         
  PROCEDURE EDIT_CAMPAIGN(
        pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
        pv_campaign_name REPORT.CAMPAIGN.CAMPAIGN_NAME%TYPE,
        pv_campaign_desc REPORT.CAMPAIGN.CAMPAIGN_DESC%TYPE,
        pn_campaign_type_id REPORT.CAMPAIGN.CAMPAIGN_TYPE_ID%TYPE,
        pd_start_date REPORT.CAMPAIGN.START_DATE%TYPE,
        pd_end_date REPORT.CAMPAIGN.END_DATE%TYPE,
        pn_discount_percent REPORT.CAMPAIGN.DISCOUNT_PERCENT%TYPE,
        pn_donation_percent REPORT.CAMPAIGN.DONATION_PERCENT%TYPE,
        pn_recur_schedule REPORT.CAMPAIGN.RECUR_SCHEDULE%TYPE,
        pn_threshold_amount REPORT.CAMPAIGN.THRESHOLD_AMOUNT%TYPE
        );     
  PROCEDURE DELETE_CAMPAIGN(
        pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE
        );
   
   PROCEDURE ADD_CAMP_POS_PTA_BY_POS(
        pn_pos_pta_id PSS.CAMPAIGN_POS_PTA.POS_PTA_ID%TYPE,
    pn_pos_id PSS.POS.POS_ID%TYPE,
    pc_delete_flag CHAR);
    
   PROCEDURE ADD_CAM_POS_PTA_BY_REG(
        pn_terminal_id REPORT.TERMINAL.TERMINAL_ID%TYPE,
    pn_region_id REPORT.TERMINAL_REGION.REGION_ID%TYPE);
    
    PROCEDURE ADD_CAM_POS_PTA_BY_TER_EPORT(
    pn_terminal_id REPORT.TERMINAL_EPORT.TERMINAL_ID%TYPE,
        pn_eport_id REPORT.TERMINAL_EPORT.EPORT_ID%TYPE,
        pd_start_date REPORT.TERMINAL_EPORT.START_DATE%TYPE,
    pd_end_date REPORT.TERMINAL_EPORT.END_DATE%TYPE,
    pn_terminal_eport_id REPORT.TERMINAL_EPORT.TERMINAL_EPORT_ID%TYPE,
    pc_delete_only_flag CHAR DEFAULT 'N');
    
    PROCEDURE DELETE_CAMPAIGN_ASSIGNMENT(
        pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE);
    
    PROCEDURE EDIT_CAMPAIGN_CUSTOMER(
        pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
    pn_customer_id REPORT.CAMPAIGN_ASSIGNMENT.CUSTOMER_ID%TYPE,
    pv_add CHAR);
    
    PROCEDURE EDIT_CAMPAIGN_REGION(
    pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
    pn_customer_id REPORT.CAMPAIGN_ASSIGNMENT.CUSTOMER_ID%TYPE,
    pn_region_id NUMBER_TABLE,
    pv_add CHAR);
    
    PROCEDURE EDIT_CAMPAIGN_TERMINAL(
    pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
    pn_terminal_id NUMBER_TABLE,
    pv_add CHAR);
    
    PROCEDURE ADD_CAM_POS_PTA_BY_CAMPAIGN(
        pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE);
    
    PROCEDURE ADD_CARDS_TO_CAMPAIGN(
        pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
    pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
    pn_card_id NUMBER_TABLE);
    
    PROCEDURE REMOVE_CARDS_FROM_CAMPAIGN(
    pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
    pn_card_id NUMBER_TABLE);
    
    --begin pre R36, can be removed afterwards   
    FUNCTION                 FIND_CAMPAIGN (
    l_terminal_id REPORT.TERMINAL_EPORT.TERMINAL_ID%TYPE,
    l_region_id REPORT.TERMINAL_REGION.REGION_ID%TYPE,
    l_customer_id REPORT.TERMINAL.CUSTOMER_ID%TYPE)
    RETURN REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE;
    
    FUNCTION                 FIND_CAMPAIGN (
    l_terminal_id REPORT.TERMINAL_EPORT.TERMINAL_ID%TYPE)
    RETURN REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE;
    
    PROCEDURE ADD_CAM_POS_PTA_BY_TER(
        pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
        pn_terminal_id REPORT.TERMINAL.TERMINAL_ID%TYPE);
    
    PROCEDURE ADD_CAM_POS_PTA_BY_TER_EPORT(
    pn_terminal_id REPORT.TERMINAL_EPORT.TERMINAL_ID%TYPE,
        pn_eport_id REPORT.TERMINAL_EPORT.EPORT_ID%TYPE,
        pd_start_date REPORT.TERMINAL_EPORT.START_DATE%TYPE,
    pd_end_date REPORT.TERMINAL_EPORT.END_DATE%TYPE);
    
    PROCEDURE ADD_CAMPAIGN_CUSTOMER(
        pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
    pn_customer_id NUMBER_TABLE);
    
    PROCEDURE ADD_CAMPAIGN_TERMINAL(
    pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
    pn_terminal_id NUMBER_TABLE,
    pn_assgined_terminal_id OUT NUMBER_TABLE);
    
    PROCEDURE CREATE_CAMPAIGN(
        pv_campaign_name REPORT.CAMPAIGN.CAMPAIGN_NAME%TYPE,
        pv_campaign_desc REPORT.CAMPAIGN.CAMPAIGN_DESC%TYPE,
        pn_campaign_type_id REPORT.CAMPAIGN.CAMPAIGN_TYPE_ID%TYPE,
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pd_start_date REPORT.CAMPAIGN.START_DATE%TYPE,
        pd_end_date REPORT.CAMPAIGN.END_DATE%TYPE,
        pn_discount_percent REPORT.CAMPAIGN.DISCOUNT_PERCENT%TYPE,
        pn_campaign_id OUT REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE
        );
        
  
   PROCEDURE EDIT_CAMPAIGN(
        pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
        pv_campaign_name REPORT.CAMPAIGN.CAMPAIGN_NAME%TYPE,
        pv_campaign_desc REPORT.CAMPAIGN.CAMPAIGN_DESC%TYPE,
        pn_campaign_type_id REPORT.CAMPAIGN.CAMPAIGN_TYPE_ID%TYPE,
        pd_start_date REPORT.CAMPAIGN.START_DATE%TYPE,
        pd_end_date REPORT.CAMPAIGN.END_DATE%TYPE,
        pn_discount_percent REPORT.CAMPAIGN.DISCOUNT_PERCENT%TYPE
        );
   
    -- end of pre R36, can be removed afterwards
    
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/PKG_CUSTOMER_MANAGEMENT.pbk?rev=1.65
CREATE OR REPLACE PACKAGE BODY REPORT.PKG_CUSTOMER_MANAGEMENT AS     
    PROCEDURE INTERNAL_CREATE_TERMINAL(
       pn_terminal_id OUT REPORT.TERMINAL.TERMINAL_ID%TYPE,
       pv_device_serial_cd IN REPORT.EPORT.EPORT_SERIAL_NUM%TYPE,
       pn_dealer_id IN CORP.DEALER.DEALER_ID%TYPE,
       pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
       pn_machine_id IN REPORT.TERMINAL.MACHINE_ID%TYPE,
       pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
       pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
       pn_region_id IN REPORT.REGION.REGION_ID%TYPE,
       pv_region_name IN REPORT.REGION.REGION_NAME%TYPE,
       pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
       pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
       pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
       pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
       pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
       pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
       pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
       pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
       pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
       pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
       pn_pc_id IN REPORT.TERMINAL.PRIMARY_CONTACT_ID%TYPE,
       pn_sc_id IN REPORT.TERMINAL.SECONDARY_CONTACT_ID%TYPE,
       pn_location_type_id IN REPORT.LOCATION.LOCATION_TYPE_ID%TYPE,
       pv_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
       pn_product_type_id IN REPORT.TERMINAL.PRODUCT_TYPE_ID%TYPE,
       pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
       pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
       pn_avg_amt IN REPORT.TERMINAL.AVG_TRANS_AMT%TYPE,
       pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
       pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
       pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
       pn_vends REPORT.TERMINAL.VENDS%TYPE,
       pv_term_var_1 IN REPORT.TERMINAL.TERM_VAR_1%TYPE,
       pv_term_var_2 IN REPORT.TERMINAL.TERM_VAR_2%TYPE,
       pv_term_var_3 IN REPORT.TERMINAL.TERM_VAR_3%TYPE,
       pv_term_var_4 IN REPORT.TERMINAL.TERM_VAR_4%TYPE,
       pv_term_var_5 IN REPORT.TERMINAL.TERM_VAR_5%TYPE,
       pv_term_var_6 IN REPORT.TERMINAL.TERM_VAR_6%TYPE,
       pv_term_var_7 IN REPORT.TERMINAL.TERM_VAR_7%TYPE,
       pv_term_var_8 IN REPORT.TERMINAL.TERM_VAR_8%TYPE,
       pd_activate_date IN REPORT.TERMINAL_EPORT.START_DATE%TYPE,
       pn_fee_grace_days IN NUMBER,
       pc_keep_existing_data IN CHAR DEFAULT 'N',
       pc_override_payment_schedule IN CHAR DEFAULT 'N',
       pv_doing_business_as IN REPORT.TERMINAL.DOING_BUSINESS_AS%TYPE DEFAULT NULL,
       pc_fee_no_trigger_event_flag IN CORP.SERVICE_FEES.NO_TRIGGER_EVENT_FLAG%TYPE DEFAULT NULL,
       pv_sales_order_number IN REPORT.TERMINAL.SALES_ORDER_NUMBER%TYPE DEFAULT NULL,
       pv_purchase_order_number IN REPORT.TERMINAL.PURCHASE_ORDER_NUMBER%TYPE DEFAULT NULL,
       pc_pos_environment_cd IN REPORT.TERMINAL.POS_ENVIRONMENT_CD%TYPE DEFAULT NULL,
       pv_entry_capability_cds IN REPORT.TERMINAL.ENTRY_CAPABILITY_CDS%TYPE DEFAULT NULL,
       pc_pin_capability_flag IN REPORT.TERMINAL.PIN_CAPABILITY_FLAG%TYPE DEFAULT NULL,
       pn_commission_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE DEFAULT NULL,
       pn_business_type_id IN REPORT.TERMINAL.BUSINESS_TYPE_ID%TYPE DEFAULT NULL,
       pv_customer_service_phone IN REPORT.TERMINAL.CUSTOMER_SERVICE_PHONE%TYPE DEFAULT NULL,
       pv_customer_service_email IN REPORT.TERMINAL.CUSTOMER_SERVICE_EMAIL%TYPE DEFAULT NULL)
    IS
      l_eport_id REPORT.EPORT.EPORT_ID%TYPE;
      l_dev_type_id REPORT.EPORT.DEVICE_TYPE_ID%TYPE;
      l_location_id REPORT.TERMINAL.LOCATION_ID%TYPE;
      l_terminal_nbr REPORT.TERMINAL.TERMINAL_NBR%TYPE;
      l_customer_id REPORT.TERMINAL.CUSTOMER_ID%TYPE;
      l_license_id NUMBER;
      l_license_type CHAR(1);
      l_parent_license_id NUMBER;
      l_addr_id REPORT.TERMINAL_ADDR.ADDRESS_ID%TYPE;
      l_last_deactivate_date DATE;
      l_start_date DATE;
      l_cnt NUMBER;
      l_business_unit_id CORP.BUSINESS_UNIT.BUSINESS_UNIT_ID%TYPE;
      ln_new_pay_sched_id REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE;
      l_lock VARCHAR2(128);
      ln_user_id REPORT.USER_LOGIN.USER_ID%TYPE := pn_user_id;
      ln_pc_id REPORT.TERMINAL.PRIMARY_CONTACT_ID%TYPE := pn_pc_id;
      ln_currency_id CORP.COUNTRY.CURRENCY_ID%TYPE;
      ln_old_terminal_id REPORT.TERMINAL.TERMINAL_ID%TYPE;
      lc_keep_existing_data CHAR(1) := pc_keep_existing_data;
      lv_service_fee_ind REPORT.DEVICE_TYPE.SERVICE_FEE_IND%TYPE;
      lv_process_fee_ind REPORT.DEVICE_TYPE.PROCESS_FEE_IND%TYPE;
      ln_service_fee_id NUMBER;
      ln_process_fee_id NUMBER;
      ln_new_region_id REPORT.REGION.REGION_ID%TYPE;
    BEGIN
        IF lc_keep_existing_data NOT IN ('Y', 'N') THEN
            RAISE_APPLICATION_ERROR(-20207, 'Invalid pc_keep_existing_data value: ' || lc_keep_existing_data);
        END IF;
        
        IF pc_override_payment_schedule NOT IN ('Y', 'N') THEN
            RAISE_APPLICATION_ERROR(-20207, 'Invalid pc_override_payment_schedule value: ' || pc_override_payment_schedule);
        END IF;
    
         BEGIN
              SELECT TERMINAL_SEQ.NEXTVAL, E.EPORT_ID, E.DEVICE_TYPE_ID, TERMINAL_ADDR_SEQ.NEXTVAL, DT.SERVICE_FEE_IND, DT.PROCESS_FEE_IND
              INTO pn_terminal_id, l_eport_id, l_dev_type_id, l_addr_id, lv_service_fee_ind, lv_process_fee_ind
              FROM REPORT.EPORT E
              JOIN REPORT.DEVICE_TYPE DT ON E.DEVICE_TYPE_ID = DT.DEVICE_TYPE_ID
              WHERE E.EPORT_SERIAL_NUM = pv_device_serial_cd;
         EXCEPTION
              WHEN NO_DATA_FOUND THEN
                   RAISE_APPLICATION_ERROR(-20201, 'Invalid eport serial number');
              WHEN OTHERS THEN
                   RAISE;
         END;
         
         l_lock := GLOBALS_PKG.REQUEST_LOCK('EPORT.EPORT_ID', l_eport_id);
         -- CHECK THAT EPORT IS INACTIVE
         SELECT NVL(MAX(NVL(END_DATE, MAX_DATE)), MIN_DATE)
           INTO l_last_deactivate_date
           FROM REPORT.TERMINAL_EPORT 
           WHERE EPORT_ID = l_eport_id;
           
        SELECT MAX(TERMINAL_ID)
        INTO ln_old_terminal_id
        FROM (
            SELECT TERMINAL_ID
            FROM REPORT.TERMINAL_EPORT
            WHERE EPORT_ID = l_eport_id
            ORDER BY NVL(END_DATE, MAX_DATE) DESC, CREATE_DT DESC
        ) WHERE ROWNUM = 1;
         
        IF lc_keep_existing_data = 'Y' AND ln_old_terminal_id IS NULL THEN
            lc_keep_existing_data := 'N';
        END IF;
         
         IF l_last_deactivate_date > SYSDATE THEN
              SELECT COUNT(TERMINAL_ID) 
                INTO l_cnt 
                FROM USER_TERMINAL 
               WHERE USER_ID = ln_user_id 
                 AND TERMINAL_ID IN(SELECT TERMINAL_ID FROM TERMINAL_EPORT WHERE EPORT_ID = l_eport_id AND NVL(END_DATE, MAX_DATE) > SYSDATE);
             IF l_cnt > 0 THEN --USER CAN VIEW TERMINAL
                  RAISE_APPLICATION_ERROR(-20205, 'Eport has already been activated');
             ELSE -- THIS MAY INDICATE THAT SOMEONE ACTIVATED THE WRONG TERMINAL
                  RAISE_APPLICATION_ERROR(-20206, 'Eport already in use');
             END IF;
         ELSIF l_last_deactivate_date > pd_activate_date THEN
            l_start_date := l_last_deactivate_date;
         ELSE
            l_start_date := pd_activate_date;
         END IF;
         --VERIFY DEALER
         SELECT COUNT(DEALER_ID) INTO l_cnt FROM CORP.DEALER_EPORT WHERE DEALER_ID = pn_dealer_id AND EPORT_ID = l_eport_id;
         IF l_cnt = 0 THEN
              RAISE_APPLICATION_ERROR(-20202, 'Invalid dealer specified');
         END IF;
         --CREATE TERMINAL_NBR
         SELECT 'T0'|| TO_CHAR(TERMINAL_NBR_SEQ.NEXTVAL, 'FM99000000') INTO l_terminal_nbr FROM DUAL;
         
         --INSERT INTO TERMINAL, LOCATION, TERMINAL_ADDR
         IF pn_customer_bank_id <> 0 THEN
             BEGIN
                  SELECT CUSTOMER_ID INTO l_customer_id FROM CORP.CUSTOMER_BANK WHERE CUSTOMER_BANK_ID = pn_customer_bank_id;
             EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                       RAISE_APPLICATION_ERROR(-20203, 'Invalid customer bank');
                  WHEN OTHERS THEN
                       RAISE;
             END;
         ELSE
              BEGIN
                  SELECT CUSTOMER_ID INTO l_customer_id FROM USER_LOGIN WHERE USER_ID = ln_pc_id AND CUSTOMER_ID <> 0;
             EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                       RAISE_APPLICATION_ERROR(-20203, 'Invalid primary contact');
                  WHEN OTHERS THEN
                       RAISE;
             END;
         END IF;
         
         IF ln_user_id = 0 THEN
            SELECT MAX(USER_ID)
            INTO ln_user_id
            FROM CORP.CUSTOMER
            WHERE CUSTOMER_ID = l_customer_id;
            
            ln_pc_id := ln_user_id;
         END IF;
         
         BEGIN
              SELECT LICENSE_ID, LICENSE_TYPE, PARENT_LICENSE_ID
              INTO l_license_id, l_license_type, l_parent_license_id 
              FROM (
                SELECT L.LICENSE_ID, L.LICENSE_TYPE, L.PARENT_LICENSE_ID
                FROM CORP.CUSTOMER_LICENSE CL, CORP.LICENSE_NBR LN, CORP.LICENSE L
                WHERE CL.LICENSE_NBR = LN.LICENSE_NBR 
                AND LN.LICENSE_ID = L.LICENSE_ID
                AND CL.CUSTOMER_ID = l_customer_id 
                ORDER BY RECEIVED DESC)
              WHERE ROWNUM = 1;
         EXCEPTION
              WHEN NO_DATA_FOUND THEN
                   RAISE_APPLICATION_ERROR(-20204, 'No license agreement found for this customer');
              WHEN OTHERS THEN
                   RAISE;
         END;
         BEGIN
              SELECT LICENSE_ID, LICENSE_TYPE, PARENT_LICENSE_ID 
                INTO l_license_id, l_license_type, l_parent_license_id 
                FROM (SELECT l.license_id, l.license_type, l.parent_license_id
                        FROM CORP.DEALER_LICENSE dl, CORP.LICENSE l
                       WHERE dl.license_id = l.license_id
                         AND dl.DEALER_ID = pn_dealer_id
                         AND SYSDATE >= NVL(dl.START_DATE, MIN_DATE)
                         AND SYSDATE < NVL(dl.END_DATE, MAX_DATE)
                       ORDER BY dl.START_DATE DESC, l.LICENSE_ID DESC)
               WHERE ROWNUM = 1;
         EXCEPTION
              WHEN NO_DATA_FOUND THEN
                   RAISE_APPLICATION_ERROR(-20204, 'No license agreement found for this dealer');
              WHEN OTHERS THEN
                   RAISE;
         END;
         
         IF l_license_type = 'C' THEN
           IF pn_commission_bank_id IS NULL THEN
             RAISE_APPLICATION_ERROR(-20208, 'commission_bank_id is required for commissioned license');
           END IF;
           IF l_parent_license_id IS NULL THEN
             RAISE_APPLICATION_ERROR(-20208, 'parent_license_id is required for commissioned license');
           END IF;
         END IF;
         
         IF lc_keep_existing_data = 'N' OR pc_override_payment_schedule = 'Y' THEN
             l_business_unit_id := FIND_BUSINESS_UNIT(l_eport_id, l_customer_id);
             SELECT MAX(PAYMENT_SCHEDULE_ID)
              INTO ln_new_pay_sched_id
              FROM CORP.PAYMENT_SCHEDULE
             WHERE PAYMENT_SCHEDULE_ID = pn_pay_sched_id
               AND SELECTABLE = 'Y';
             IF l_business_unit_id IN(2,3,5) OR ln_new_pay_sched_id IS NULL THEN
                SELECT DEFAULT_PAYMENT_SCHEDULE_ID
                  INTO ln_new_pay_sched_id
                  FROM CORP.BUSINESS_UNIT
                 WHERE BUSINESS_UNIT_ID = l_business_unit_id;
             END IF;
         END IF;
         
         IF lc_keep_existing_data = 'N' THEN
             INSERT INTO TERMINAL_ADDR(ADDRESS_ID, CUSTOMER_ID, ADDRESS1, CITY, STATE, ZIP, COUNTRY_CD)
                 VALUES(l_addr_id, l_customer_id, pv_address1, pv_city, pv_state_cd, pv_postal, pv_country_cd);
             BEGIN
                 SELECT LOCATION_ID INTO l_location_id FROM LOCATION WHERE EQL(LOCATION_NAME, pv_location_name) = -1 AND EPORT_ID =  l_eport_id AND TERMINAL_ID = 0;
                 UPDATE LOCATION SET (DESCRIPTION, ADDRESS_ID, UPD_DATE, TERMINAL_ID, LOCATION_TYPE_ID, LOCATION_TYPE_SPECIFY) =
                       (SELECT pv_location_details, l_addr_id, SYSDATE, pn_terminal_id, pn_location_type_id,pv_location_type_specific FROM DUAL) WHERE LOCATION_ID = l_location_id;
             EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                      SELECT LOCATION_SEQ.NEXTVAL INTO l_location_id FROM DUAL;
                       INSERT INTO LOCATION(LOCATION_ID, LOCATION_NAME, DESCRIPTION, ADDRESS_ID, CREATE_BY, TERMINAL_ID, EPORT_ID, LOCATION_TYPE_ID, LOCATION_TYPE_SPECIFY)
                              VALUES(l_location_id, pv_location_name, pv_location_details, l_addr_id, ln_user_id, pn_terminal_id, l_eport_id, pn_location_type_id, pv_location_type_specific);
                  WHEN OTHERS THEN
                       RAISE;
             END;
             
             SELECT NVL(CURRENCY_ID, 1)
             INTO ln_currency_id
             FROM CORP.COUNTRY
             WHERE COUNTRY_CD = pv_country_cd;
         
             INSERT INTO TERMINAL(
                TERMINAL_ID,
                TERMINAL_NBR,
                TERMINAL_NAME,
                EPORT_ID,
                CUSTOMER_ID,
                LOCATION_ID,
                ASSET_NBR,
                MACHINE_ID,
                TELEPHONE,
                PREFIX,
                PRODUCT_TYPE_ID,
                PRODUCT_TYPE_SPECIFY,
                PRIMARY_CONTACT_ID,
                SECONDARY_CONTACT_ID,
                AUTHORIZATION_MODE,
                AVG_TRANS_AMT,
                TIME_ZONE_ID,
                DEX_DATA,
                RECEIPT,
                VENDS,
                TERM_VAR_1,
                TERM_VAR_2,
                TERM_VAR_3,
                TERM_VAR_4,
                TERM_VAR_5,
                TERM_VAR_6,
                TERM_VAR_7,
                TERM_VAR_8,
                BUSINESS_UNIT_ID,
                PAYMENT_SCHEDULE_ID,
                FEE_CURRENCY_ID,
                DOING_BUSINESS_AS,
                SALES_ORDER_NUMBER,
                PURCHASE_ORDER_NUMBER,
                POS_ENVIRONMENT_CD, 
                ENTRY_CAPABILITY_CDS, 
                PIN_CAPABILITY_FLAG,
                BUSINESS_TYPE_ID,
                CUSTOMER_SERVICE_PHONE,
                CUSTOMER_SERVICE_EMAIL)
             SELECT
                pn_terminal_id,
                l_terminal_nbr,
                l_terminal_nbr,
                l_eport_id,
                l_customer_id,
                l_location_id,
                pv_asset_nbr,
                pn_machine_id,
                pv_telephone,
                pv_prefix,
                NVL(pn_product_type_id, 0),
                pn_product_type_specific,
                ln_pc_id,
                pn_sc_id,
                pc_auth_mode,
                NVL(pn_avg_amt, 0),
                pn_time_zone_id,
                pc_dex_data,
                pc_receipt,
                pn_vends,
                pv_term_var_1,
                pv_term_var_2,
                pv_term_var_3,
                pv_term_var_4,
                pv_term_var_5,
                pv_term_var_6,
                pv_term_var_7,
                pv_term_var_8,
                l_business_unit_id,
                ln_new_pay_sched_id,
                ln_currency_id,
                pv_doing_business_as,
                pv_sales_order_number,
                pv_purchase_order_number,
                pc_pos_environment_cd, 
                pv_entry_capability_cds,
                pc_pin_capability_flag,
                pn_business_type_id,
                pv_customer_service_phone,
                pv_customer_service_email
        FROM DUAL;
         ELSE
            INSERT INTO TERMINAL(
                TERMINAL_ID,
                TERMINAL_NBR,
                TERMINAL_NAME,
                EPORT_ID,
                CUSTOMER_ID,
                LOCATION_ID,
                ASSET_NBR,
                MACHINE_ID,
                TELEPHONE,
                PREFIX,
                PRODUCT_TYPE_ID,
                PRODUCT_TYPE_SPECIFY,
                PRIMARY_CONTACT_ID,
                SECONDARY_CONTACT_ID,
                AUTHORIZATION_MODE,
                AVG_TRANS_AMT,
                TIME_ZONE_ID,
                DEX_DATA,
                RECEIPT,
                VENDS,
                TERM_VAR_1,
                TERM_VAR_2,
                TERM_VAR_3,
                TERM_VAR_4,
                TERM_VAR_5,
                TERM_VAR_6,
                TERM_VAR_7,
                TERM_VAR_8,
                BUSINESS_UNIT_ID,
                PAYMENT_SCHEDULE_ID,
                FEE_CURRENCY_ID,
                DOING_BUSINESS_AS,
                SALES_ORDER_NUMBER,
                PURCHASE_ORDER_NUMBER,
                POS_ENVIRONMENT_CD, 
                ENTRY_CAPABILITY_CDS, 
                PIN_CAPABILITY_FLAG,
                BUSINESS_TYPE_ID,
                CUSTOMER_SERVICE_PHONE,
                CUSTOMER_SERVICE_EMAIL)
            SELECT
                pn_terminal_id,
                l_terminal_nbr,
                l_terminal_nbr,
                l_eport_id,
                l_customer_id,
                LOCATION_ID,
                ASSET_NBR,
                MACHINE_ID,
                TELEPHONE,
                PREFIX,
                PRODUCT_TYPE_ID,
                PRODUCT_TYPE_SPECIFY,
                PRIMARY_CONTACT_ID,
                SECONDARY_CONTACT_ID,
                AUTHORIZATION_MODE,
                AVG_TRANS_AMT,
                TIME_ZONE_ID,
                DEX_DATA,
                RECEIPT,
                VENDS,
                TERM_VAR_1,
                TERM_VAR_2,
                TERM_VAR_3,
                TERM_VAR_4,
                TERM_VAR_5,
                TERM_VAR_6,
                TERM_VAR_7,
                TERM_VAR_8,
                BUSINESS_UNIT_ID,
                DECODE(pc_override_payment_schedule, 'N', PAYMENT_SCHEDULE_ID, ln_new_pay_sched_id),
                FEE_CURRENCY_ID,
                DOING_BUSINESS_AS,
                SALES_ORDER_NUMBER,
                PURCHASE_ORDER_NUMBER,
                NVL(pc_pos_environment_cd, POS_ENVIRONMENT_CD), 
                NVL(pv_entry_capability_cds, ENTRY_CAPABILITY_CDS), 
                NVL(pc_pin_capability_flag, PIN_CAPABILITY_FLAG),
                NVL(pn_business_type_id, BUSINESS_TYPE_ID),
                NVL(pv_customer_service_phone, CUSTOMER_SERVICE_PHONE),
                NVL(pv_customer_service_email, CUSTOMER_SERVICE_EMAIL)
            FROM REPORT.TERMINAL
            WHERE TERMINAL_ID = ln_old_terminal_id;
         END IF;
              
        IF lc_keep_existing_data = 'N' THEN
            IF pn_region_id != -1 THEN
                BEGIN
                  SELECT REGION_ID
                    INTO ln_new_region_id 
                    FROM REPORT.REGION
                   WHERE REGION_ID = pn_region_id
                     AND CUSTOMER_ID = l_customer_id;
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        RAISE_APPLICATION_ERROR(-20213, 'Invalid region id');
                END;
            ELSE
                REPORT.GET_OR_CREATE_REGION(ln_new_region_id, pv_region_name, l_customer_id);
            END IF;
         
            IF ln_new_region_id != 0 THEN
                INSERT INTO REPORT.TERMINAL_REGION(TERMINAL_ID, REGION_ID)
                  VALUES(pn_terminal_id, ln_new_region_id); 
            END IF;
        ELSE
            INSERT INTO REPORT.TERMINAL_REGION(TERMINAL_ID, REGION_ID)
            SELECT pn_terminal_id, REGION_ID
            FROM REPORT.TERMINAL_REGION
            WHERE TERMINAL_ID = ln_old_terminal_id;
        END IF;
            
         INSERT INTO REPORT.TERMINAL_EPORT(EPORT_ID, TERMINAL_ID, START_DATE, END_DATE)
            VALUES(l_eport_id, pn_terminal_id, l_start_date, NULL);
        
         --UPDATE DEALER_EPORT
         UPDATE CORP.DEALER_EPORT SET ACTIVATE_DATE = l_start_date WHERE DEALER_ID = pn_dealer_id AND EPORT_ID = l_eport_id;
         --INSERT INTO USER_TERMINAL
         INSERT INTO USER_TERMINAL(USER_ID, TERMINAL_ID, ALLOW_EDIT)
         /* A much faster way - BSK 04/10/2007
              SELECT USER_ID, pn_terminal_id, 'Y' FROM USER_LOGIN WHERE CAN_ADMIN_USER(ln_user_id, USER_ID) = 'Y'; */
             SELECT USER_ID, pn_terminal_id, 'Y'
               FROM REPORT.USER_LOGIN u
               CONNECT BY PRIOR u.admin_id = u.user_id
               START WITH u.user_id = ln_user_id
             UNION
             SELECT a.USER_ID, pn_terminal_id, 'Y'
               FROM REPORT.USER_LOGIN a
              INNER JOIN REPORT.USER_PRIVS up
                 ON a.USER_ID = up.user_id
              WHERE a.CUSTOMER_ID = l_customer_id
                AND up.priv_id IN(5,8);
    
         --INSERT INTO CORP.CUSTOMER_BANK_TERMINAL
         IF pn_customer_bank_id <> 0 THEN
             INSERT INTO CORP.CUSTOMER_BANK_TERMINAL(TERMINAL_ID, CUSTOMER_BANK_ID, START_DATE)
               VALUES(pn_terminal_id, pn_customer_bank_id, NULL); --l_start_date); : Use NULL (or MIN_DATE) instead
         END IF;
         
         IF lv_service_fee_ind = 'Y' THEN
             FOR l_cur in (SELECT lsf.FEE_ID as fee_id,
                          DECODE(lsf.FEE_ID, 8, 0, lsf.AMOUNT) as amount, 
                          lsf.FREQUENCY_ID as frequency_id,
                           CASE 
                              WHEN F.INITIATION_TYPE_CD IN('G', 'R') AND l_start_date + pn_fee_grace_days > SYSDATE THEN NULL
                              WHEN lsf.START_IMMEDIATELY_FLAG = 'Y' THEN l_start_date
                              ELSE NULL
                           END as last_payment,
                           DECODE(lsf.FEE_ID, 8, lsf.AMOUNT, NULL) as fee_percent,
                           CASE WHEN F.INITIATION_TYPE_CD IN('G', 'R') THEN l_start_date + pn_fee_grace_days END as grace_period,
                           lsf.INACTIVE_FEE_AMOUNT,
                           CASE WHEN F.INITIATION_TYPE_CD = 'R' AND lsf.INACTIVE_MONTHS_REMAINING IS NULL THEN 3 ELSE lsf.INACTIVE_MONTHS_REMAINING END INACTIVE_MONTHS_REMAINING
                      FROM CORP.LICENSE_SERVICE_FEES lsf
                      JOIN CORP.FEES F ON lsf.FEE_ID = F.FEE_ID
                     WHERE lsf.LICENSE_ID = l_license_id
                       AND (pc_dex_data IN('A', 'D') OR lsf.FEE_ID != 4)) 
             LOOP
                 SELECT CORP.SERVICE_FEE_SEQ.NEXTVAL INTO ln_service_fee_id FROM dual;
                 INSERT INTO CORP.SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID, FEE_AMOUNT, FREQUENCY_ID, LAST_PAYMENT, START_DATE, FEE_PERCENT, GRACE_PERIOD_DATE, NO_TRIGGER_EVENT_FLAG, ORIGINAL_LICENSE_ID, INACTIVE_FEE_AMOUNT, INACTIVE_MONTHS_REMAINING)
                 VALUES (ln_service_fee_id, pn_terminal_id, l_cur.fee_id, l_cur.amount, l_cur.frequency_id, l_cur.last_payment, l_start_date, l_cur.fee_percent, l_cur.grace_period, pc_fee_no_trigger_event_flag, l_license_id, l_cur.INACTIVE_FEE_AMOUNT, l_cur.INACTIVE_MONTHS_REMAINING);
                 IF l_license_type = 'C' THEN
                     INSERT INTO CORP.SERVICE_FEE_COMMISSION(SERVICE_FEE_ID, CUSTOMER_BANK_ID, BUY_AMOUNT, SELL_AMOUNT, COMMISSION_AMOUNT)
                     SELECT ln_service_fee_id, pn_commission_bank_id, nvl(p.amount, 0), nvl(c.amount,0), (nvl(c.amount, 0) - nvl(p.amount,0))
                       FROM corp.license_service_fees c, corp.license_service_fees p
                      WHERE c.license_id = l_license_id
                        AND c.fee_id = l_cur.fee_id
                        AND p.license_id = l_parent_license_id
                        AND p.fee_id = l_cur.fee_id;
                 END IF;
             END LOOP;
             IF pc_dex_data IN('A', 'D') THEN
                 INSERT INTO CORP.SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID, FEE_AMOUNT, FREQUENCY_ID, START_DATE, ORIGINAL_LICENSE_ID)
                     SELECT CORP.SERVICE_FEE_SEQ.NEXTVAL, pn_terminal_id, 4,
                     4.00, f.FREQUENCY_ID, l_start_date, l_license_id
                      FROM CORP.FREQUENCY f
                     WHERE f.FREQUENCY_ID = 2
                       AND NOT EXISTS(SELECT 1 
                                        FROM CORP.LICENSE_SERVICE_FEES lsf 
                                       WHERE lsf.LICENSE_ID = l_license_id
                                         AND lsf.FEE_ID = 4);
             END IF;
         END IF;
         
         IF lv_process_fee_ind = 'Y' THEN
             FOR l_cur in (SELECT TRANS_TYPE_ID, FEE_PERCENT, FEE_AMOUNT, MIN_AMOUNT
                             FROM CORP.LICENSE_PROCESS_FEES 
                            WHERE LICENSE_ID = l_license_id) 
             LOOP
               SELECT CORP.PROCESS_FEE_SEQ.NEXTVAL INTO ln_process_fee_id FROM dual;
               INSERT INTO CORP.PROCESS_FEES(PROCESS_FEE_ID, TERMINAL_ID, TRANS_TYPE_ID, FEE_PERCENT, FEE_AMOUNT, MIN_AMOUNT, START_DATE, ORIGINAL_LICENSE_ID)
               VALUES (ln_process_fee_id, pn_terminal_id, l_cur.TRANS_TYPE_ID, l_cur.FEE_PERCENT, l_cur.FEE_AMOUNT, l_cur.MIN_AMOUNT, null, l_license_id);
               IF l_license_type = 'C' THEN
                 INSERT INTO CORP.PROCESS_FEE_COMMISSION(PROCESS_FEE_ID, CUSTOMER_BANK_ID, 
                  BUY_AMOUNT, BUY_PERCENT, BUY_MIN,
                  SELL_AMOUNT, SELL_PERCENT, SELL_MIN,
                  COMMISSION_AMOUNT, COMMISSION_PERCENT, COMMISSION_MIN)
                 SELECT ln_process_fee_id, pn_commission_bank_id, 
                        nvl(p.fee_amount,0), nvl(p.fee_percent,0), nvl(p.min_amount,0),
                        nvl(c.fee_amount,0), nvl(c.fee_percent,0), nvl(c.min_amount,0),
                        nvl(c.fee_amount,0) - nvl(p.fee_amount,0),
                        nvl(c.fee_percent,0) - nvl(p.fee_percent,0),
                        nvl(c.min_amount,0) - nvl(p.min_amount,0)
                   FROM corp.license_process_fees c, corp.license_process_fees p
                  WHERE c.license_id = l_license_id
                    AND c.trans_type_id = l_cur.TRANS_TYPE_ID
                    AND p.license_id = l_parent_license_id
                    AND p.trans_type_id = l_cur.TRANS_TYPE_ID;
               END IF;
             END LOOP;
         END IF;
    END;

    PROCEDURE INTERNAL_UPDATE_TERMINAL(
       pn_terminal_id IN REPORT.TERMINAL.TERMINAL_ID%TYPE,
       pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
       pn_machine_id IN REPORT.TERMINAL.MACHINE_ID%TYPE,
       pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
       pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
       pn_region_id IN REPORT.REGION.REGION_ID%TYPE,
       pv_region_name IN REPORT.REGION.REGION_NAME%TYPE,
       pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
       pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
       pn_address_id REPORT.TERMINAL_ADDR.ADDRESS_ID%TYPE,
       pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
       pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
       pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
       pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
       pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
       pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
       pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
       pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
       pn_pc_id IN REPORT.TERMINAL.PRIMARY_CONTACT_ID%TYPE,
       pn_sc_id IN REPORT.TERMINAL.SECONDARY_CONTACT_ID%TYPE,
       pn_location_type_id IN REPORT.LOCATION.LOCATION_TYPE_ID%TYPE,
       pn_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
       pn_product_type_id IN REPORT.TERMINAL.PRODUCT_TYPE_ID%TYPE,
       pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
       pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
       pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
       pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
       pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
       pn_vends REPORT.TERMINAL.VENDS%TYPE,
       pv_term_var_1 REPORT.TERMINAL.term_var_1%TYPE,
       pv_term_var_2 REPORT.TERMINAL.term_var_2%TYPE,
       pv_term_var_3 REPORT.TERMINAL.term_var_3%TYPE,
       pv_term_var_4 REPORT.TERMINAL.term_var_4%TYPE,
       pv_term_var_5 REPORT.TERMINAL.term_var_5%TYPE,
       pv_term_var_6 REPORT.TERMINAL.term_var_6%TYPE,
       pv_term_var_7 REPORT.TERMINAL.term_var_7%TYPE,
       pv_term_var_8 REPORT.TERMINAL.term_var_8%TYPE,
       pv_doing_business_as IN REPORT.TERMINAL.DOING_BUSINESS_AS%TYPE DEFAULT NULL,
       pc_pos_environment_cd IN REPORT.TERMINAL.POS_ENVIRONMENT_CD%TYPE DEFAULT NULL,
       pv_entry_capability_cds IN REPORT.TERMINAL.ENTRY_CAPABILITY_CDS%TYPE DEFAULT NULL,
       pc_pin_capability_flag IN REPORT.TERMINAL.PIN_CAPABILITY_FLAG%TYPE DEFAULT NULL,
       pn_business_type_id IN REPORT.TERMINAL.BUSINESS_TYPE_ID%TYPE DEFAULT NULL,
       pv_customer_service_phone IN REPORT.TERMINAL.CUSTOMER_SERVICE_PHONE%TYPE DEFAULT NULL,
       pv_customer_service_email IN REPORT.TERMINAL.CUSTOMER_SERVICE_EMAIL%TYPE DEFAULT NULL)
    IS
      pv_location_name_id REPORT.TERMINAL.LOCATION_ID%TYPE;
      l_customer_id REPORT.TERMINAL.CUSTOMER_ID%TYPE;
      l_old_cb_id NUMBER;
      l_old_lname REPORT.LOCATION.LOCATION_NAME%TYPE;
      l_old_ldesc REPORT.LOCATION.DESCRIPTION%TYPE;
      l_new_address_id REPORT.TERMINAL_ADDR.ADDRESS_ID%TYPE;
      l_old_address_id REPORT.LOCATION.ADDRESS_ID%TYPE;
      l_old_dex REPORT.TERMINAL.DEX_DATA%TYPE;
      isnew BOOLEAN;
      l_date DATE := SYSDATE;
      l_lock VARCHAR2(128);
      ln_business_unit_id REPORT.TERMINAL.BUSINESS_UNIT_ID%TYPE;
      ln_old_pay_sched_id REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE;
      ln_new_pay_sched_id REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE;
      ln_currency_id CORP.COUNTRY.CURRENCY_ID%TYPE;
      ln_new_region_id REPORT.REGION.REGION_ID%TYPE;
    BEGIN
         l_lock := GLOBALS_PKG.REQUEST_LOCK('TERMINAL.TERMINAL_ID', pn_terminal_id);
         -- if a new location is named than create it
         SELECT T.LOCATION_ID, T.CUSTOMER_ID, L.LOCATION_NAME, L.DESCRIPTION, L.ADDRESS_ID, CB.CUSTOMER_BANK_ID, T.DEX_DATA, T.BUSINESS_UNIT_ID, T.PAYMENT_SCHEDULE_ID
           INTO pv_location_name_id, l_customer_id, l_old_lname, l_old_ldesc, l_old_address_id, l_old_cb_id, l_old_dex, ln_business_unit_id, ln_old_pay_sched_id
           FROM REPORT.TERMINAL T
           LEFT OUTER JOIN REPORT.LOCATION L ON T.LOCATION_ID = L.LOCATION_ID
           LEFT OUTER JOIN CORP.VW_CURRENT_BANK_ACCT CB ON T.TERMINAL_ID = CB.TERMINAL_ID
          WHERE T.TERMINAL_ID = pn_terminal_id;
         IF EQL(l_old_lname, pv_location_name) <> -1 THEN  -- location has changed
             UPDATE REPORT.LOCATION SET status = 'D' WHERE LOCATION_ID = pv_location_name_id;
            INSERT INTO REPORT.TERMINAL_CHANGES (TERMINAL_ID, ATTRIBUTE, OLD_VALUE, NEW_VALUE)
                   VALUES(pn_terminal_id, 'LOCATION',l_old_lname, pv_location_name);
            BEGIN
               SELECT LOCATION_ID INTO pv_location_name_id FROM REPORT.LOCATION WHERE LOCATION_NAME = pv_location_name AND TERMINAL_ID = pn_terminal_id;
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                     isnew := true;
               WHEN OTHERS THEN
                  RAISE;
            END;
         END IF;
         --RECORD CHANGES (SO USALIVE CAN BE UPDATED)
         DECLARE
             l_z TERMINAL.TIME_ZONE_ID%TYPE;
             l_cnt NUMBER;
         BEGIN
              SELECT TIME_ZONE_ID
                INTO l_z 
                FROM REPORT.TERMINAL 
               WHERE TERMINAL_ID = pn_terminal_id;
             IF EQL(l_z, pn_time_zone_id) <> -1 THEN
                 UPDATE REPORT.TERMINAL_CHANGES SET NEW_VALUE = pn_time_zone_id WHERE TERMINAL_ID = pn_terminal_id
                   AND ATTRIBUTE = 'TIME_ZONE' RETURNING 1 INTO l_cnt;
                IF NVL(l_cnt, 0) = 0 THEN
                    INSERT INTO REPORT.TERMINAL_CHANGES (TERMINAL_ID, ATTRIBUTE, OLD_VALUE, NEW_VALUE)
                   VALUES(pn_terminal_id, 'TIME_ZONE',l_z, pn_time_zone_id);
                END IF;
             END IF;
         END;
         -- UPDATE ADDRESS
         IF pn_address_id IS NULL THEN    -- none specified
            l_new_address_id := NULL;
         ELSIF pn_address_id = 0 THEN    --IS NEW
            SELECT REPORT.TERMINAL_ADDR_SEQ.NEXTVAL INTO l_new_address_id FROM DUAL;
            INSERT INTO REPORT.TERMINAL_ADDR(ADDRESS_ID, CUSTOMER_ID, ADDRESS1, CITY, STATE, ZIP, COUNTRY_CD)
                   VALUES(l_new_address_id, l_customer_id, pv_address1, pv_city, pv_state_cd, pv_postal, pv_country_cd);
         ELSE
             UPDATE REPORT.TERMINAL_ADDR SET ADDRESS1 = pv_address1, CITY = pv_city, STATE = pv_state_cd, ZIP = pv_postal, COUNTRY_CD = pv_country_cd
                WHERE ADDRESS_ID = pn_address_id AND (EQL(ADDRESS1, pv_address1) = 0
                OR EQL(CITY, pv_city) = 0 OR EQL(STATE, pv_state_cd) = 0 OR EQL(ZIP, pv_postal) = 0 OR EQL(COUNTRY_CD, pv_country_cd) = 0);
            l_new_address_id := pn_address_id;
         END IF;
         IF isnew THEN
             SELECT REPORT.LOCATION_SEQ.NEXTVAL INTO pv_location_name_id FROM DUAL;
             INSERT INTO REPORT.LOCATION(LOCATION_ID, LOCATION_NAME, DESCRIPTION, ADDRESS_ID, CREATE_BY, TERMINAL_ID, LOCATION_TYPE_ID, LOCATION_TYPE_SPECIFY)
                   VALUES(pv_location_name_id,pv_location_name,pv_location_details,l_new_address_id,pn_user_id,pn_terminal_id,pn_location_type_id,pn_location_type_specific);
         ELSE
             UPDATE REPORT.LOCATION L SET (DESCRIPTION, ADDRESS_ID, UPD_DATE, LOCATION_TYPE_ID, LOCATION_TYPE_SPECIFY) =
                   (SELECT pv_location_details,l_new_address_id,l_date,NVL(pn_location_type_id, L.LOCATION_TYPE_ID),DECODE(pn_location_type_id, NULL, L.LOCATION_TYPE_SPECIFY, pn_location_type_specific) FROM DUAL) WHERE location_id = pv_location_name_id;
         END IF;
  
         IF pn_pay_sched_id IS NULL THEN
            ln_new_pay_sched_id := ln_old_pay_sched_id;
         ELSIF pn_pay_sched_id != ln_old_pay_sched_id THEN
            SELECT MAX(PAYMENT_SCHEDULE_ID)
              INTO ln_new_pay_sched_id
              FROM CORP.PAYMENT_SCHEDULE
             WHERE PAYMENT_SCHEDULE_ID = pn_pay_sched_id
               AND SELECTABLE = 'Y';
            IF ln_new_pay_sched_id IS NULL THEN
                RAISE_APPLICATION_ERROR(-20217, 'Invalid payment schedule requested');
            ELSIF ln_business_unit_id IN(2,3,5) THEN
                RAISE_APPLICATION_ERROR(-20217, 'Not permitted to change payment schedule as requested');
            END IF;
         ELSE
            ln_new_pay_sched_id := ln_old_pay_sched_id;
         END IF;
         
         SELECT NVL(CURRENCY_ID, 1)
         INTO ln_currency_id
         FROM CORP.COUNTRY
         WHERE COUNTRY_CD = pv_country_cd;
         
         UPDATE REPORT.TERMINAL T SET (LOCATION_ID, ASSET_NBR, MACHINE_ID, TELEPHONE, PREFIX, PRODUCT_TYPE_ID, PRODUCT_TYPE_SPECIFY,
             PRIMARY_CONTACT_ID, SECONDARY_CONTACT_ID, AUTHORIZATION_MODE, TIME_ZONE_ID, DEX_DATA, RECEIPT, VENDS, PAYMENT_SCHEDULE_ID, STATUS,
             TERM_VAR_1, TERM_VAR_2, TERM_VAR_3, TERM_VAR_4, TERM_VAR_5, TERM_VAR_6, TERM_VAR_7, TERM_VAR_8, FEE_CURRENCY_ID, DOING_BUSINESS_AS,
             POS_ENVIRONMENT_CD, ENTRY_CAPABILITY_CDS, PIN_CAPABILITY_FLAG, BUSINESS_TYPE_ID, CUSTOMER_SERVICE_PHONE, CUSTOMER_SERVICE_EMAIL) =
            (SELECT pv_location_name_id, pv_asset_nbr, pn_machine_id, pv_telephone, pv_prefix, NVL(pn_product_type_id, 0), pn_product_type_specific,
             pn_pc_id, pn_sc_id, pc_auth_mode, pn_time_zone_id, NVL(pc_dex_data, T.DEX_DATA), NVL(pc_receipt, T.RECEIPT), pn_vends, ln_new_pay_sched_id, 'U',
             pv_term_var_1, pv_term_var_2, pv_term_var_3, pv_term_var_4, pv_term_var_5, pv_term_var_6, pv_term_var_7, pv_term_var_8, ln_currency_id, pv_doing_business_as,
             NVL(pc_pos_environment_cd, T.POS_ENVIRONMENT_CD), NVL(pv_entry_capability_cds, T.ENTRY_CAPABILITY_CDS), NVL(pc_pin_capability_flag, T.PIN_CAPABILITY_FLAG),
             pn_business_type_id, pv_customer_service_phone, pv_customer_service_email             
              FROM DUAL) 
             WHERE TERMINAL_ID = pn_terminal_id;
             
         IF pn_region_id IS NOT NULL AND pn_region_id > 0 THEN
             -- double-check customer_id or region
             BEGIN
               SELECT REGION_ID
                 INTO ln_new_region_id
                 FROM REPORT.REGION
                WHERE REGION_ID = pn_region_id
                  AND CUSTOMER_ID = l_customer_id;
             EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    RAISE_APPLICATION_ERROR(-20218, 'Invalid region id');
             END;
         END IF;
         IF ln_new_region_id IS NULL OR ln_new_region_id = 0 THEN
            REPORT.GET_OR_CREATE_REGION(ln_new_region_id, pv_region_name, l_customer_id);
         END IF;
         UPDATE REPORT.TERMINAL_REGION
            SET REGION_ID = ln_new_region_id
          WHERE TERMINAL_ID = pn_terminal_id;   
         IF SQL%ROWCOUNT < 1 THEN
            INSERT INTO REPORT.TERMINAL_REGION(TERMINAL_ID, REGION_ID)
                VALUES(pn_terminal_id, ln_new_region_id);
         END IF;
         
         --UPDATE BANK ACCOUNT
         IF EQL(l_old_cb_id, pn_customer_bank_id) = 0 THEN
             DECLARE
                 l_cb_date DATE;
            BEGIN
                UPDATE CORP.CUSTOMER_BANK_TERMINAL SET END_DATE = CASE WHEN END_DATE < l_date THEN END_DATE ELSE l_date END
                 WHERE TERMINAL_ID = pn_terminal_id
                 RETURNING MAX(CASE WHEN END_DATE < l_date THEN END_DATE ELSE l_date END) INTO l_cb_date;
                IF NVL(pn_customer_bank_id, 0) <> 0 THEN
                       INSERT INTO CORP.CUSTOMER_BANK_TERMINAL(CUSTOMER_BANK_TERMINAL_ID, TERMINAL_ID, CUSTOMER_BANK_ID, START_DATE)
                        VALUES(CORP.CUSTOMER_BANK_TERMINAL_SEQ.NEXTVAL, pn_terminal_id, pn_customer_bank_id, l_cb_date);
                END IF;
            END;
         END IF;
         /*
         IF NVL(l_old_cb_id, 0) = 0 AND NVL(pn_customer_bank_id, 0) <> 0 THEN
             DECLARE
                 l_cb_date DATE;
            BEGIN
                 SELECT NVL(MIN(CLOSE_DATE), l_date) INTO l_cb_date FROM TRANS WHERE TERMINAL_ID = pn_terminal_id AND CLOSE_DATE >=
                     (SELECT NVL(MAX(END_DATE), CLOSE_DATE) FROM CORP.CUSTOMER_BANK_TERMINAL
                     WHERE TERMINAL_ID = pn_terminal_id AND END_DATE > NVL(START_DATE, END_DATE));
                 CORP.TERMINAL_REASSIGN_BANK_ACCOUNT(pn_terminal_id, pn_customer_bank_id, l_cb_date);
            END;
         ELSIF EQL(l_old_cb_id, pn_customer_bank_id) = 0 THEN
             CORP.TERMINAL_REASSIGN_BANK_ACCOUNT(pn_terminal_id, pn_customer_bank_id, l_date);
         END IF;
         */
         IF l_old_dex NOT IN('D','A') AND pc_dex_data IN('D','A') THEN
             DECLARE
                ln_service_fee_id CORP.SERVICE_FEES.SERVICE_FEE_ID%TYPE;
                lc_copy CHAR(1);         
             BEGIN
                SELECT SERVICE_FEE_ID, DECODE(END_DATE, NULL, 'N', 'Y')
                  INTO ln_service_fee_id, lc_copy
                  FROM (SELECT SERVICE_FEE_ID, END_DATE
                          FROM CORP.SERVICE_FEES
                         WHERE TERMINAL_ID = pn_terminal_id
                           AND FEE_ID = 4
                         ORDER BY NVL(END_DATE, MAX_DATE) DESC, LAST_PAYMENT DESC, SERVICE_FEE_ID DESC)
                  WHERE ROWNUM = 1;
                IF lc_copy = 'Y' THEN
                  INSERT INTO CORP.SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID, FEE_AMOUNT, FREQUENCY_ID, LAST_PAYMENT, START_DATE, ORIGINAL_LICENSE_ID)
                     SELECT CORP.SERVICE_FEE_SEQ.NEXTVAL, pn_terminal_id, 4, FEE_AMOUNT, FREQUENCY_ID, LAST_PAYMENT, GREATEST(l_date, END_DATE), ORIGINAL_LICENSE_ID 
                       FROM CORP.SERVICE_FEES
                      WHERE SERVICE_FEE_ID = ln_service_fee_id;
                END IF;          
             EXCEPTION
                WHEN NO_DATA_FOUND THEN
                     INSERT INTO CORP.SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID, FEE_AMOUNT, FREQUENCY_ID, LAST_PAYMENT, START_DATE)
                         SELECT CORP.SERVICE_FEE_SEQ.NEXTVAL, pn_terminal_id, 4, 4, 2, l_date, l_date FROM DUAL;
             END;
         ELSIF pc_dex_data NOT IN('D','A') AND l_old_dex IN('D','A') THEN
              UPDATE CORP.SERVICE_FEES SET END_DATE = l_date 
               WHERE TERMINAL_ID = pn_terminal_id
                 AND NVL(END_DATE, l_date) >= l_date
                 AND FEE_ID = 4;
         END IF;
    EXCEPTION
         WHEN NO_DATA_FOUND THEN
              RAISE_APPLICATION_ERROR(-20100, 'No such terminal found');
         WHEN OTHERS THEN
             ROLLBACK;
             RAISE;
    END;
    
    PROCEDURE CREATE_TERMINAL(
        pn_terminal_id OUT REPORT.TERMINAL.TERMINAL_ID%TYPE,
        pv_device_serial_cd IN REPORT.EPORT.EPORT_SERIAL_NUM%TYPE,
        pn_dealer_id IN CORP.DEALER.DEALER_ID%TYPE,
        pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
        pn_machine_id IN REPORT.TERMINAL.MACHINE_ID%TYPE,
        pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
        pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
        pn_region_id IN REPORT.REGION.REGION_ID%TYPE,
        pv_region_name IN REPORT.REGION.REGION_NAME%TYPE,
        pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
        pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
        pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
        pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
        pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
        pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
        pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
        pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
        pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_pc_id IN REPORT.TERMINAL.PRIMARY_CONTACT_ID%TYPE,
        pn_sc_id IN REPORT.TERMINAL.SECONDARY_CONTACT_ID%TYPE,
        pn_location_type_id IN REPORT.LOCATION.LOCATION_TYPE_ID%TYPE,
        pv_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
        pn_product_type_id IN REPORT.TERMINAL.PRODUCT_TYPE_ID%TYPE,
        pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
        pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
        pn_avg_amt IN REPORT.TERMINAL.AVG_TRANS_AMT%TYPE,
        pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
        pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
        pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
        pn_vends REPORT.TERMINAL.VENDS%TYPE,
        pv_term_var_1 IN REPORT.TERMINAL.TERM_VAR_1%TYPE,
        pv_term_var_2 IN REPORT.TERMINAL.TERM_VAR_2%TYPE,
        pv_term_var_3 IN REPORT.TERMINAL.TERM_VAR_3%TYPE,
        pv_term_var_4 IN REPORT.TERMINAL.TERM_VAR_4%TYPE,
        pv_term_var_5 IN REPORT.TERMINAL.TERM_VAR_5%TYPE,
        pv_term_var_6 IN REPORT.TERMINAL.TERM_VAR_6%TYPE,
        pv_term_var_7 IN REPORT.TERMINAL.TERM_VAR_7%TYPE,
        pv_term_var_8 IN REPORT.TERMINAL.TERM_VAR_8%TYPE,
        pd_activate_date IN REPORT.TERMINAL_EPORT.START_DATE%TYPE DEFAULT SYSDATE - 30,
        pn_fee_grace_days IN NUMBER DEFAULT 60,
        pv_doing_business_as IN REPORT.TERMINAL.DOING_BUSINESS_AS%TYPE DEFAULT NULL,
        pc_fee_no_trigger_event_flag IN CORP.SERVICE_FEES.NO_TRIGGER_EVENT_FLAG%TYPE DEFAULT NULL,
        pv_sales_order_number IN REPORT.TERMINAL.SALES_ORDER_NUMBER%TYPE DEFAULT NULL,
        pv_purchase_order_number IN REPORT.TERMINAL.PURCHASE_ORDER_NUMBER%TYPE DEFAULT NULL,
        pc_pos_environment_cd IN REPORT.TERMINAL.POS_ENVIRONMENT_CD%TYPE DEFAULT NULL,
        pv_entry_capability_cds IN REPORT.TERMINAL.ENTRY_CAPABILITY_CDS%TYPE DEFAULT NULL,
        pc_pin_capability_flag IN REPORT.TERMINAL.PIN_CAPABILITY_FLAG%TYPE DEFAULT NULL,
        pn_commission_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE DEFAULT NULL,
        pn_business_type_id IN REPORT.TERMINAL.BUSINESS_TYPE_ID%TYPE DEFAULT NULL,
       pv_customer_service_phone IN REPORT.TERMINAL.CUSTOMER_SERVICE_PHONE%TYPE DEFAULT NULL,
       pv_customer_service_email IN REPORT.TERMINAL.CUSTOMER_SERVICE_EMAIL%TYPE DEFAULT NULL)
    IS
        ln_cnt PLS_INTEGER;
        lv_city REPORT.TERMINAL_ADDR.CITY%TYPE;
        lv_state_cd REPORT.TERMINAL_ADDR.STATE%TYPE;     
        ln_new_region_id REPORT.REGION.REGION_ID%TYPE;        
    BEGIN
        IF pv_state_cd IS NULL OR pv_city IS NULL THEN
            SELECT MAX(CITY), MAX(STATE_CD), MAX(CNT)
              INTO lv_city, lv_state_cd, ln_cnt
              FROM (SELECT P.CITY, S.STATE_CD, 1 CNT
                  FROM FRONT.POSTAL P 
                  JOIN CORP.STATE S ON P.STATE_ID = S.STATE_ID
                WHERE P.POSTAL_CD = pv_postal
                  AND S.COUNTRY_CD = pv_country_cd
                ORDER BY P.POSTAL_ID ASC)
             WHERE ROWNUM = 1;
        ELSE
            SELECT COUNT(*), pv_city, pv_state_cd
              INTO ln_cnt, lv_city, lv_state_cd 
              FROM CORP.STATE 
             WHERE STATE_CD = pv_state_cd
               AND COUNTRY_CD = pv_country_cd; 
        END IF; 
        IF ln_cnt = 0 THEN
            RAISE_APPLICATION_ERROR(-20220, 'Invalid state/country');
            RETURN;
        END IF;
        INTERNAL_CREATE_TERMINAL(
            pn_terminal_id,
            pv_device_serial_cd,
            pn_dealer_id,
            pv_asset_nbr,
            pn_machine_id,
            pv_telephone,
            pv_prefix,
            pn_region_id,
            pv_region_name,
            pv_location_name,
            pv_location_details,
            pv_address1,
            lv_city,
            lv_state_cd,
            pv_postal,
            pv_country_cd,
            pn_customer_bank_id,
            pn_pay_sched_id,
            pn_user_id,
            pn_pc_id,
            pn_sc_id,
            pn_location_type_id,
            pv_location_type_specific,
            pn_product_type_id,
            pn_product_type_specific,
            pc_auth_mode,
            pn_avg_amt,
            pn_time_zone_id,
            pc_dex_data,
            pc_receipt,
            pn_vends,
            pv_term_var_1,
            pv_term_var_2,
            pv_term_var_3,
            pv_term_var_4,
            pv_term_var_5,
            pv_term_var_6,
            pv_term_var_7,
            pv_term_var_8,
            pd_activate_date,
            pn_fee_grace_days,
            'N', -- pc_keep_existing_data
            'N', -- pc_override_payment_schedule    
            pv_doing_business_as,
            pc_fee_no_trigger_event_flag,
            pv_sales_order_number,
            pv_purchase_order_number,
            pc_pos_environment_cd,
            pv_entry_capability_cds,
            pc_pin_capability_flag,
            pn_commission_bank_id,
            pn_business_type_id,
            pv_customer_service_phone,
            pv_customer_service_email);
    END;

    PROCEDURE UPDATE_TERMINAL(
       pn_terminal_id IN REPORT.TERMINAL.TERMINAL_ID%TYPE,
       pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
       pn_machine_id IN REPORT.TERMINAL.MACHINE_ID%TYPE,
       pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
       pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
       pn_region_id IN REPORT.REGION.REGION_ID%TYPE,
       pv_region_name IN REPORT.REGION.REGION_NAME%TYPE,
       pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
       pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
       pn_address_id REPORT.TERMINAL_ADDR.ADDRESS_ID%TYPE,
       pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
       pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
       pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
       pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
       pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
       pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
       pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
       pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
       pn_pc_id IN REPORT.TERMINAL.PRIMARY_CONTACT_ID%TYPE,
       pn_sc_id IN REPORT.TERMINAL.SECONDARY_CONTACT_ID%TYPE,
       pn_location_type_id IN REPORT.LOCATION.LOCATION_TYPE_ID%TYPE,
       pn_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
       pn_product_type_id IN REPORT.TERMINAL.PRODUCT_TYPE_ID%TYPE,
       pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
       pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
       pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
       pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
       pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
       pn_vends REPORT.TERMINAL.VENDS%TYPE,
       pv_term_var_1 REPORT.TERMINAL.term_var_1%TYPE,
       pv_term_var_2 REPORT.TERMINAL.term_var_2%TYPE,
       pv_term_var_3 REPORT.TERMINAL.term_var_3%TYPE,
       pv_term_var_4 REPORT.TERMINAL.term_var_4%TYPE,
       pv_term_var_5 REPORT.TERMINAL.term_var_5%TYPE,
       pv_term_var_6 REPORT.TERMINAL.term_var_6%TYPE,
       pv_term_var_7 REPORT.TERMINAL.term_var_7%TYPE,
       pv_term_var_8 REPORT.TERMINAL.term_var_8%TYPE,
       pv_doing_business_as IN REPORT.TERMINAL.DOING_BUSINESS_AS%TYPE DEFAULT NULL,
       pc_pos_environment_cd IN REPORT.TERMINAL.POS_ENVIRONMENT_CD%TYPE DEFAULT NULL,
       pv_entry_capability_cds IN REPORT.TERMINAL.ENTRY_CAPABILITY_CDS%TYPE DEFAULT NULL,
       pc_pin_capability_flag IN REPORT.TERMINAL.PIN_CAPABILITY_FLAG%TYPE DEFAULT NULL,
       pn_business_type_id IN REPORT.TERMINAL.BUSINESS_TYPE_ID%TYPE DEFAULT NULL,
       pv_customer_service_phone IN REPORT.TERMINAL.CUSTOMER_SERVICE_PHONE%TYPE DEFAULT NULL,
       pv_customer_service_email IN REPORT.TERMINAL.CUSTOMER_SERVICE_EMAIL%TYPE DEFAULT NULL)
    IS
        ln_cnt PLS_INTEGER;
        lv_city REPORT.TERMINAL_ADDR.CITY%TYPE;
        lv_state_cd REPORT.TERMINAL_ADDR.STATE%TYPE;   
    BEGIN
        IF pv_state_cd IS NULL OR pv_city IS NULL THEN
            SELECT MAX(CITY), MAX(STATE_CD), MAX(CNT)
              INTO lv_city, lv_state_cd, ln_cnt
              FROM (SELECT P.CITY, S.STATE_CD, 1 CNT
                  FROM FRONT.POSTAL P 
                  JOIN CORP.STATE S ON P.STATE_ID = S.STATE_ID
                WHERE P.POSTAL_CD = pv_postal
                  AND S.COUNTRY_CD = pv_country_cd
                ORDER BY P.POSTAL_ID ASC)
             WHERE ROWNUM = 1;
        ELSE
            SELECT COUNT(*), pv_city, pv_state_cd
              INTO ln_cnt, lv_city, lv_state_cd
              FROM CORP.STATE 
             WHERE STATE_CD = pv_state_cd
               AND COUNTRY_CD = pv_country_cd; 
        END IF;
        IF ln_cnt = 0 THEN
            RAISE_APPLICATION_ERROR(-20220, 'Invalid state/country');
        ELSE
            INTERNAL_UPDATE_TERMINAL(
               pn_terminal_id,
               pv_asset_nbr,
               pn_machine_id,
               pv_telephone,
               pv_prefix,
               pn_region_id,
               pv_region_name,
               pv_location_name,
               pv_location_details,
               pn_address_id,
               pv_address1,
               lv_city,
               lv_state_cd,
               pv_postal,
               pv_country_cd,
               pn_customer_bank_id,
               pn_pay_sched_id,
               pn_user_id,
               pn_pc_id,
               pn_sc_id,
               pn_location_type_id,
               pn_location_type_specific,
               pn_product_type_id,
               pn_product_type_specific,
               pc_auth_mode,
               pn_time_zone_id,
               pc_dex_data,
               pc_receipt,
               pn_vends,
               pv_term_var_1,
               pv_term_var_2,
               pv_term_var_3,
               pv_term_var_4,
               pv_term_var_5,
               pv_term_var_6,
               pv_term_var_7,
               pv_term_var_8,
               pv_doing_business_as,
               pc_pos_environment_cd,
               pv_entry_capability_cds,
               pc_pin_capability_flag,
               pn_business_type_id,
               pv_customer_service_phone,
               pv_customer_service_email);
        END IF;
    END;
        
    PROCEDURE CREATE_TERMINAL_MASS(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_terminal_id OUT REPORT.TERMINAL.TERMINAL_ID%TYPE,
        pv_device_serial_cd IN REPORT.EPORT.EPORT_SERIAL_NUM%TYPE,
        pn_dealer_id IN CORP.DEALER.DEALER_ID%TYPE,
        pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
        pv_machine_make REPORT.MACHINE.MAKE%TYPE,
        pv_machine_model REPORT.MACHINE.MODEL%TYPE,
        pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
        pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
        pv_region_name IN REPORT.REGION.REGION_NAME%TYPE,
        pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
        pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
        pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
        pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
        pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
        pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
        pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
        pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
        pv_location_type_name REPORT.LOCATION_TYPE.LOCATION_TYPE_NAME%TYPE,
        pn_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
        pv_product_type_name REPORT.PRODUCT_TYPE.PRODUCT_TYPE_NAME%TYPE,
        pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
        pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
        pn_avg_amt IN REPORT.TERMINAL.AVG_TRANS_AMT%TYPE,
        pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
        pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
        pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
        pn_vends REPORT.TERMINAL.VENDS%TYPE,
        pv_term_var_1 REPORT.TERMINAL.term_var_1%TYPE,
        pv_term_var_2 REPORT.TERMINAL.term_var_2%TYPE,
        pv_term_var_3 REPORT.TERMINAL.term_var_3%TYPE,
        pv_term_var_4 REPORT.TERMINAL.term_var_4%TYPE,
        pv_term_var_5 REPORT.TERMINAL.term_var_5%TYPE,
        pv_term_var_6 REPORT.TERMINAL.term_var_6%TYPE,
        pv_term_var_7 REPORT.TERMINAL.term_var_7%TYPE,
        pv_term_var_8 REPORT.TERMINAL.term_var_8%TYPE,
        pd_activate_date IN REPORT.TERMINAL_EPORT.START_DATE%TYPE DEFAULT SYSDATE - 30,
        pn_fee_grace_days IN NUMBER DEFAULT 60,
        pc_keep_existing_data IN CHAR DEFAULT 'N',
        pc_override_payment_schedule IN CHAR DEFAULT 'N',
        pv_doing_business_as IN REPORT.TERMINAL.DOING_BUSINESS_AS%TYPE DEFAULT NULL,
        pc_fee_no_trigger_event_flag IN CORP.SERVICE_FEES.NO_TRIGGER_EVENT_FLAG%TYPE DEFAULT NULL,
        pv_sales_order_number IN REPORT.TERMINAL.SALES_ORDER_NUMBER%TYPE DEFAULT NULL,
        pv_purchase_order_number IN REPORT.TERMINAL.PURCHASE_ORDER_NUMBER%TYPE DEFAULT NULL,
        pc_pos_environment_cd IN REPORT.TERMINAL.POS_ENVIRONMENT_CD%TYPE DEFAULT NULL,
        pv_entry_capability_cds IN REPORT.TERMINAL.ENTRY_CAPABILITY_CDS%TYPE DEFAULT NULL,
        pc_pin_capability_flag IN REPORT.TERMINAL.PIN_CAPABILITY_FLAG%TYPE DEFAULT NULL,
        pn_commission_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE DEFAULT NULL,
        pv_business_type_name IN REPORT.BUSINESS_TYPE.BUSINESS_TYPE_NAME%TYPE DEFAULT NULL,
       pv_customer_service_phone IN REPORT.TERMINAL.CUSTOMER_SERVICE_PHONE%TYPE DEFAULT NULL,
       pv_customer_service_email IN REPORT.TERMINAL.CUSTOMER_SERVICE_EMAIL%TYPE DEFAULT NULL)
    IS
        ln_machine_id REPORT.MACHINE.MACHINE_ID%TYPE;
        ln_product_type_id REPORT.PRODUCT_TYPE.PRODUCT_TYPE_ID%TYPE;
        ln_location_type_id REPORT.LOCATION_TYPE.LOCATION_TYPE_ID%TYPE; 
        ln_cnt PLS_INTEGER;
        lv_city REPORT.TERMINAL_ADDR.CITY%TYPE;
        lv_state_cd REPORT.TERMINAL_ADDR.STATE%TYPE;     
        ln_region_id REPORT.REGION.REGION_ID%TYPE;    
        ln_business_type_id REPORT.TERMINAL.BUSINESS_TYPE_ID%TYPE;
    BEGIN
        IF pc_keep_existing_data = 'N' THEN
            BEGIN
                SELECT MAX(MACHINE_ID)
                  INTO ln_machine_id
                  FROM REPORT.MACHINE
                 WHERE UPPER(MAKE) = UPPER(pv_machine_make)
                   AND UPPER(MODEL) = UPPER(pv_machine_model)
                   AND STATUS IN('A','I');
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    REPORT.MACHINE_INS(ln_machine_id, pv_machine_make, pv_machine_model, pn_user_id);
            END;
            IF ln_machine_id IS NULL THEN
                REPORT.MACHINE_INS(ln_machine_id, pv_machine_make, pv_machine_model, pn_user_id);
            END IF;
            BEGIN
                IF pv_location_type_name IS NOT NULL AND LENGTH(TRIM(pv_location_type_name)) > 0 THEN
                    SELECT LOCATION_TYPE_ID
                      INTO ln_location_type_id
                      FROM REPORT.LOCATION_TYPE
                     WHERE UPPER(LOCATION_TYPE_NAME) = UPPER(pv_location_type_name)
                       AND STATUS IN('A','I');
                ELSE
                    ln_location_type_id := 0;
                END IF;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    RAISE_APPLICATION_ERROR(-20211, 'Invalid location type name');
            END;
            BEGIN
                SELECT PRODUCT_TYPE_ID
                  INTO ln_product_type_id
                  FROM REPORT.PRODUCT_TYPE
                 WHERE UPPER(PRODUCT_TYPE_NAME) = UPPER(pv_product_type_name)
                   AND STATUS IN('A','I');
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    RAISE_APPLICATION_ERROR(-20212, 'Invalid product type name');
            END;
            IF pv_business_type_name IS NOT NULL THEN
              BEGIN
                  SELECT BUSINESS_TYPE_ID
                    INTO ln_business_type_id
                    FROM REPORT.BUSINESS_TYPE
                   WHERE UPPER(BUSINESS_TYPE_NAME) = UPPER(pv_business_type_name);
              EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                      RAISE_APPLICATION_ERROR(-20213, 'Invalid business type name');
              END;
            END IF;
            IF pv_state_cd IS NULL OR pv_city IS NULL THEN
                SELECT MAX(CITY), MAX(STATE_CD), MAX(CNT)
                  INTO lv_city, lv_state_cd, ln_cnt
                  FROM (SELECT P.CITY, S.STATE_CD, 1 CNT
                      FROM FRONT.POSTAL P 
                      JOIN CORP.STATE S ON P.STATE_ID = S.STATE_ID
                    WHERE P.POSTAL_CD = pv_postal
                      AND S.COUNTRY_CD = pv_country_cd
                    ORDER BY P.POSTAL_ID ASC)
                 WHERE ROWNUM = 1;
            ELSE
                SELECT COUNT(*), pv_city, pv_state_cd
                  INTO ln_cnt, lv_city, lv_state_cd 
                  FROM CORP.STATE 
                 WHERE STATE_CD = pv_state_cd
                   AND COUNTRY_CD = pv_country_cd; 
            END IF; 
            IF ln_cnt = 0 THEN
                RAISE_APPLICATION_ERROR(-20220, 'Invalid state/country');
                RETURN;
            END IF;
        END IF;
        INTERNAL_CREATE_TERMINAL(
               pn_terminal_id,
               pv_device_serial_cd,
               pn_dealer_id,
               pv_asset_nbr,
               ln_machine_id,
               pv_telephone,
               pv_prefix,
               -1,
               pv_region_name,
               pv_location_name,
               pv_location_details,
               pv_address1,
               lv_city,
               lv_state_cd,
               pv_postal,
               pv_country_cd,
               pn_customer_bank_id,
               pn_pay_sched_id,
               pn_user_id,
               pn_user_id,
               NULL,
               ln_location_type_id,
               pn_location_type_specific,
               ln_product_type_id,
               pn_product_type_specific,
               pc_auth_mode,
               pn_avg_amt,
               pn_time_zone_id,
               pc_dex_data,
               pc_receipt,
               pn_vends,
               pv_term_var_1,
               pv_term_var_2,
               pv_term_var_3,
               pv_term_var_4,
               pv_term_var_5,
               pv_term_var_6,
               pv_term_var_7,
               pv_term_var_8,
               pd_activate_date,
               pn_fee_grace_days,
               pc_keep_existing_data,
               pc_override_payment_schedule,
               pv_doing_business_as,
               pc_fee_no_trigger_event_flag,
               pv_sales_order_number,
               pv_purchase_order_number,
               pc_pos_environment_cd,
               pv_entry_capability_cds,
               pc_pin_capability_flag,
               pn_commission_bank_id,
               ln_business_type_id,
               pv_customer_service_phone,
               pv_customer_service_email);
    END;
    
    PROCEDURE UPDATE_TERMINAL_MASS(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_terminal_id IN REPORT.TERMINAL.TERMINAL_ID%TYPE,
        pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
        pv_machine_make REPORT.MACHINE.MAKE%TYPE,
        pv_machine_model REPORT.MACHINE.MODEL%TYPE,
        pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
        pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
        pv_region_name IN REPORT.REGION.REGION_NAME%TYPE,
        pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
        pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
        pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
        pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
        pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
        pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
        pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
        pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
        pn_location_type_id REPORT.LOCATION_TYPE.LOCATION_TYPE_ID%TYPE,
        pv_location_type_name REPORT.LOCATION_TYPE.LOCATION_TYPE_NAME%TYPE,
        pn_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
        pv_product_type_name REPORT.PRODUCT_TYPE.PRODUCT_TYPE_NAME%TYPE,
        pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
        pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
        pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
        pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
        pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
        pn_vends REPORT.TERMINAL.VENDS%TYPE,
        pv_term_var_1 REPORT.TERMINAL.term_var_1%TYPE,
        pv_term_var_2 REPORT.TERMINAL.term_var_2%TYPE,
        pv_term_var_3 REPORT.TERMINAL.term_var_3%TYPE,
        pv_term_var_4 REPORT.TERMINAL.term_var_4%TYPE,
        pv_term_var_5 REPORT.TERMINAL.term_var_5%TYPE,
        pv_term_var_6 REPORT.TERMINAL.term_var_6%TYPE,
        pv_term_var_7 REPORT.TERMINAL.term_var_7%TYPE,
        pv_term_var_8 REPORT.TERMINAL.term_var_8%TYPE,
        pv_doing_business_as IN REPORT.TERMINAL.DOING_BUSINESS_AS%TYPE DEFAULT NULL,
        pc_pos_environment_cd IN REPORT.TERMINAL.POS_ENVIRONMENT_CD%TYPE DEFAULT NULL,
        pv_entry_capability_cds IN REPORT.TERMINAL.ENTRY_CAPABILITY_CDS%TYPE DEFAULT NULL,
        pc_pin_capability_flag IN REPORT.TERMINAL.PIN_CAPABILITY_FLAG%TYPE DEFAULT NULL,
        pv_business_type_name IN REPORT.BUSINESS_TYPE.BUSINESS_TYPE_NAME%TYPE DEFAULT NULL,
       pv_customer_service_phone IN REPORT.TERMINAL.CUSTOMER_SERVICE_PHONE%TYPE DEFAULT NULL,
       pv_customer_service_email IN REPORT.TERMINAL.CUSTOMER_SERVICE_EMAIL%TYPE DEFAULT NULL)
    IS
        ln_machine_id REPORT.MACHINE.MACHINE_ID%TYPE;
        ln_product_type_id REPORT.PRODUCT_TYPE.PRODUCT_TYPE_ID%TYPE;
        ln_location_type_id REPORT.LOCATION_TYPE.LOCATION_TYPE_ID%TYPE;
        ln_cnt PLS_INTEGER;
        lv_city REPORT.TERMINAL_ADDR.CITY%TYPE;
        lv_state_cd REPORT.TERMINAL_ADDR.STATE%TYPE;  
        ln_region_id REPORT.REGION.REGION_ID%TYPE;
        ln_business_type_id REPORT.TERMINAL.BUSINESS_TYPE_ID%TYPE;    
    BEGIN
        BEGIN
            SELECT MAX(MACHINE_ID)
              INTO ln_machine_id
              FROM REPORT.MACHINE
             WHERE UPPER(MAKE) = UPPER(pv_machine_make)
               AND UPPER(MODEL) = UPPER(pv_machine_model)
               AND STATUS IN('A','I');
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                REPORT.MACHINE_INS(ln_machine_id, pv_machine_make, pv_machine_model, pn_user_id);
        END;
        IF ln_machine_id IS NULL THEN
            REPORT.MACHINE_INS(ln_machine_id, pv_machine_make, pv_machine_model, pn_user_id);
        END IF;
        BEGIN
            IF pv_location_type_name IS NOT NULL AND LENGTH(TRIM(pv_location_type_name)) > 0 THEN
                SELECT LOCATION_TYPE_ID
                  INTO ln_location_type_id
                  FROM REPORT.LOCATION_TYPE
                 WHERE UPPER(LOCATION_TYPE_NAME) = UPPER(pv_location_type_name)
                   AND STATUS IN('A','I');
            ELSIF pn_location_type_id IS NULL THEN
                ln_location_type_id := 0;
            ELSE
                ln_location_type_id := pn_location_type_id;
            END IF;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                RAISE_APPLICATION_ERROR(-20211, 'Invalid location type name');
        END;
        BEGIN
            SELECT PRODUCT_TYPE_ID
              INTO ln_product_type_id
              FROM REPORT.PRODUCT_TYPE
             WHERE UPPER(PRODUCT_TYPE_NAME) = UPPER(pv_product_type_name)
               AND STATUS IN('A','I');
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                RAISE_APPLICATION_ERROR(-20212, 'Invalid product type name');
        END;
        IF pv_business_type_name IS NOT NULL THEN
            BEGIN
                SELECT BUSINESS_TYPE_ID
                  INTO ln_business_type_id
                  FROM REPORT.BUSINESS_TYPE
                 WHERE UPPER(BUSINESS_TYPE_NAME) = UPPER(pv_business_type_name);
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    RAISE_APPLICATION_ERROR(-20213, 'Invalid business type name');
            END; 
        END IF;
        IF pv_state_cd IS NULL OR pv_city IS NULL THEN
            SELECT MAX(CITY), MAX(STATE_CD), MAX(CNT)
              INTO lv_city, lv_state_cd, ln_cnt
              FROM (SELECT P.CITY, S.STATE_CD, 1 CNT
                  FROM FRONT.POSTAL P 
                  JOIN CORP.STATE S ON P.STATE_ID = S.STATE_ID
                WHERE P.POSTAL_CD = pv_postal
                  AND S.COUNTRY_CD = pv_country_cd
                ORDER BY P.POSTAL_ID ASC)
             WHERE ROWNUM = 1;
        ELSE
            SELECT COUNT(*), pv_city, pv_state_cd
              INTO ln_cnt, lv_city, lv_state_cd
              FROM CORP.STATE 
             WHERE STATE_CD = pv_state_cd
               AND COUNTRY_CD = pv_country_cd; 
        END IF; 
        IF ln_cnt = 0 THEN
            RAISE_APPLICATION_ERROR(-20220, 'Invalid state/country');
            RETURN;
        END IF;
        INTERNAL_UPDATE_TERMINAL(
               pn_terminal_id,
               pv_asset_nbr,
               ln_machine_id,
               pv_telephone,
               pv_prefix,
               NULL,
               pv_region_name,
               pv_location_name,
               pv_location_details,
               0,  /* address id (0=create a new address)*/
               pv_address1,
               lv_city,
               lv_state_cd,
               pv_postal,
               pv_country_cd,
               pn_customer_bank_id,
               pn_pay_sched_id,
               pn_user_id,
               pn_user_id,
               NULL,
               ln_location_type_id,
               pn_location_type_specific,
               ln_product_type_id,
               pn_product_type_specific,
               pc_auth_mode,
               pn_time_zone_id,
               pc_dex_data,
               pc_receipt,
               pn_vends,
               pv_term_var_1,
               pv_term_var_2,
               pv_term_var_3,
               pv_term_var_4,
               pv_term_var_5,
               pv_term_var_6,
               pv_term_var_7,
               pv_term_var_8,
               pv_doing_business_as,
               pc_pos_environment_cd,
               pv_entry_capability_cds,
               pc_pin_capability_flag,
               ln_business_type_id,
               pv_customer_service_phone,
               pv_customer_service_email);
    END;
    -- pre usalive 1.9 signature
    PROCEDURE CREATE_CUSTOMER_BANK(
        pn_customer_bank_id OUT CORP.CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        pn_customer_id IN CORP.CUSTOMER_BANK.CUSTOMER_ID%TYPE,
        pn_user_id IN CORP.CUSTOMER.USER_ID%TYPE,
        pv_bank_name IN CORP.CUSTOMER_BANK.BANK_NAME%TYPE,
        pv_bank_address1 IN CORP.CUSTOMER_BANK.BANK_ADDRESS%TYPE,
        pv_bank_city IN CORP.CUSTOMER_BANK.BANK_CITY%TYPE,
        pv_bank_state_cd IN CORP.CUSTOMER_BANK.BANK_STATE%TYPE,
        pv_bank_postal IN CORP.CUSTOMER_BANK.BANK_ZIP%TYPE,
        pv_bank_country_cd IN CORP.CUSTOMER_BANK.BANK_COUNTRY_CD%TYPE,
        pv_account_title IN CORP.CUSTOMER_BANK.ACCOUNT_TITLE%TYPE,
        pv_account_type IN CORP.CUSTOMER_BANK.ACCOUNT_TYPE%TYPE,
        pv_bank_routing_nbr IN CORP.CUSTOMER_BANK.BANK_ROUTING_NBR%TYPE,
        pv_bank_acct_nbr IN CORP.CUSTOMER_BANK.BANK_ACCT_NBR%TYPE,
        pv_contact_name IN CORP.CUSTOMER_BANK.CONTACT_NAME%TYPE,
        pv_contact_title IN CORP.CUSTOMER_BANK.CONTACT_TITLE%TYPE,
        pv_contact_telephone IN CORP.CUSTOMER_BANK.CONTACT_TELEPHONE%TYPE,
        pv_contact_fax IN CORP.CUSTOMER_BANK.CONTACT_FAX%TYPE)
    IS
    BEGIN
        CREATE_CUSTOMER_BANK(pn_customer_bank_id,pn_customer_id,pn_user_id,pv_bank_name,pv_bank_address1,pv_bank_city,
        pv_bank_state_cd,pv_bank_postal,pv_bank_country_cd,pv_account_title,pv_account_type,pv_bank_routing_nbr,
        pv_bank_acct_nbr,pv_contact_name,pv_contact_title,pv_contact_telephone,pv_contact_fax,null);
    END;
    
    -- usalive 1.9 signature
    PROCEDURE CREATE_CUSTOMER_BANK(
        pn_customer_bank_id OUT CORP.CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        pn_customer_id IN CORP.CUSTOMER_BANK.CUSTOMER_ID%TYPE,
        pn_user_id IN CORP.CUSTOMER.USER_ID%TYPE,
        pv_bank_name IN CORP.CUSTOMER_BANK.BANK_NAME%TYPE,
        pv_bank_address1 IN CORP.CUSTOMER_BANK.BANK_ADDRESS%TYPE,
        pv_bank_city IN CORP.CUSTOMER_BANK.BANK_CITY%TYPE,
        pv_bank_state_cd IN CORP.CUSTOMER_BANK.BANK_STATE%TYPE,
        pv_bank_postal IN CORP.CUSTOMER_BANK.BANK_ZIP%TYPE,
        pv_bank_country_cd IN CORP.CUSTOMER_BANK.BANK_COUNTRY_CD%TYPE,
        pv_account_title IN CORP.CUSTOMER_BANK.ACCOUNT_TITLE%TYPE,
        pv_account_type IN CORP.CUSTOMER_BANK.ACCOUNT_TYPE%TYPE,
        pv_bank_routing_nbr IN CORP.CUSTOMER_BANK.BANK_ROUTING_NBR%TYPE,
        pv_bank_acct_nbr IN CORP.CUSTOMER_BANK.BANK_ACCT_NBR%TYPE,
        pv_contact_name IN CORP.CUSTOMER_BANK.CONTACT_NAME%TYPE,
        pv_contact_title IN CORP.CUSTOMER_BANK.CONTACT_TITLE%TYPE,
        pv_contact_telephone IN CORP.CUSTOMER_BANK.CONTACT_TELEPHONE%TYPE,
        pv_contact_fax IN CORP.CUSTOMER_BANK.CONTACT_FAX%TYPE,
        pv_swift_code IN CORP.CUSTOMER_BANK.SWIFT_CODE%TYPE,
        pn_pay_cycle_id IN CORP.CUSTOMER_BANK.PAY_CYCLE_ID%TYPE DEFAULT 3)
    IS
    BEGIN
        SELECT CORP.CUSTOMER_BANK_SEQ.NEXTVAL INTO pn_customer_bank_id FROM DUAL;
        INSERT INTO CORP.CUSTOMER_BANK(CUSTOMER_BANK_ID, CUSTOMER_ID, BANK_NAME, BANK_ADDRESS, BANK_CITY, BANK_STATE, BANK_ZIP, BANK_COUNTRY_CD, CREATE_BY,
            ACCOUNT_TITLE, ACCOUNT_TYPE, BANK_ROUTING_NBR, BANK_ACCT_NBR, CONTACT_NAME, CONTACT_TITLE, CONTACT_TELEPHONE, CONTACT_FAX, SWIFT_CODE, PAY_CYCLE_ID)
            VALUES(pn_customer_bank_id, pn_customer_id, pv_bank_name, pv_bank_address1, pv_bank_city, pv_bank_state_cd, pv_bank_postal, pv_bank_country_cd, pn_user_id,
                   pv_account_title, pv_account_type, pv_bank_routing_nbr, pv_bank_acct_nbr, pv_contact_name, pv_contact_title, pv_contact_telephone, pv_contact_fax, pv_swift_code, NVL(pn_pay_cycle_id, 3));
        INSERT INTO CORP.USER_CUSTOMER_BANK (CUSTOMER_BANK_ID, USER_ID, ALLOW_EDIT)
            VALUES(pn_customer_bank_id, pn_user_id, 'Y');
    END;
    
    PROCEDURE UPDATE_REGION_NAME(
        pn_region_id REPORT.REGION.REGION_ID%TYPE,
        pv_region_name REPORT.REGION.REGION_NAME%TYPE,
        pn_customer_id REPORT.TERMINAL.CUSTOMER_ID%TYPE)
    IS
    BEGIN
        UPDATE REPORT.REGION
           SET REGION_NAME = TRIM(pv_region_name)
         WHERE REGION_ID = pn_region_id
           AND CUSTOMER_ID = pn_customer_id;
    END;    
    --R44 and below
    PROCEDURE CREATE_CAMPAIGN(
        pv_campaign_name REPORT.CAMPAIGN.CAMPAIGN_NAME%TYPE,
        pv_campaign_desc REPORT.CAMPAIGN.CAMPAIGN_DESC%TYPE,
        pn_campaign_type_id REPORT.CAMPAIGN.CAMPAIGN_TYPE_ID%TYPE,
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pd_start_date REPORT.CAMPAIGN.START_DATE%TYPE,
        pd_end_date REPORT.CAMPAIGN.END_DATE%TYPE,
        pn_discount_percent REPORT.CAMPAIGN.DISCOUNT_PERCENT%TYPE,
        pn_recur_schedule REPORT.CAMPAIGN.RECUR_SCHEDULE%TYPE,
        pn_threshold_amount REPORT.CAMPAIGN.THRESHOLD_AMOUNT%TYPE,
        pn_campaign_id OUT REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE
        )
     IS
      pn_customer_id REPORT.CAMPAIGN.CUSTOMER_ID%TYPE;
    BEGIN
        CREATE_CAMPAIGN(pv_campaign_name, pv_campaign_desc,pn_campaign_type_id,pn_user_id,pd_start_date,pd_end_date,pn_discount_percent,null,pn_recur_schedule,pn_threshold_amount,pn_campaign_id);
    END;
    --R45 and above   
    PROCEDURE CREATE_CAMPAIGN(
        pv_campaign_name REPORT.CAMPAIGN.CAMPAIGN_NAME%TYPE,
        pv_campaign_desc REPORT.CAMPAIGN.CAMPAIGN_DESC%TYPE,
        pn_campaign_type_id REPORT.CAMPAIGN.CAMPAIGN_TYPE_ID%TYPE,
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pd_start_date REPORT.CAMPAIGN.START_DATE%TYPE,
        pd_end_date REPORT.CAMPAIGN.END_DATE%TYPE,
        pn_discount_percent REPORT.CAMPAIGN.DISCOUNT_PERCENT%TYPE,
        pn_donation_percent REPORT.CAMPAIGN.DONATION_PERCENT%TYPE,
        pn_recur_schedule REPORT.CAMPAIGN.RECUR_SCHEDULE%TYPE,
        pn_threshold_amount REPORT.CAMPAIGN.THRESHOLD_AMOUNT%TYPE,
        pn_campaign_id OUT REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE
        )
     IS
      pn_customer_id REPORT.CAMPAIGN.CUSTOMER_ID%TYPE;
    BEGIN
        select report.seq_campaign_id.nextval into pn_campaign_id from dual;
        select decode(customer_id, 0,null,customer_id) into pn_customer_id from report.user_login where user_id=pn_user_id;
        insert into report.campaign 
        (campaign_id, CAMPAIGN_NAME, CAMPAIGN_DESC, CAMPAIGN_TYPE_ID, customer_id, START_DATE, END_DATE, DISCOUNT_PERCENT, DONATION_PERCENT, RECUR_SCHEDULE, THRESHOLD_AMOUNT) 
        values(pn_campaign_id, pv_campaign_name, pv_campaign_desc, pn_campaign_type_id, pn_customer_id, pd_start_date, pd_end_date, pn_discount_percent/100, pn_donation_percent/100, pn_recur_schedule,pn_threshold_amount);
    END;
    --R44 and below
    PROCEDURE EDIT_CAMPAIGN(
        pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
        pv_campaign_name REPORT.CAMPAIGN.CAMPAIGN_NAME%TYPE,
        pv_campaign_desc REPORT.CAMPAIGN.CAMPAIGN_DESC%TYPE,
        pn_campaign_type_id REPORT.CAMPAIGN.CAMPAIGN_TYPE_ID%TYPE,
        pd_start_date REPORT.CAMPAIGN.START_DATE%TYPE,
        pd_end_date REPORT.CAMPAIGN.END_DATE%TYPE,
        pn_discount_percent REPORT.CAMPAIGN.DISCOUNT_PERCENT%TYPE,
        pn_recur_schedule REPORT.CAMPAIGN.RECUR_SCHEDULE%TYPE,
        pn_threshold_amount REPORT.CAMPAIGN.THRESHOLD_AMOUNT%TYPE
        )
     IS
    BEGIN
        EDIT_CAMPAIGN(pn_campaign_id,pv_campaign_name,pv_campaign_desc,pn_campaign_type_id,pd_start_date,pd_end_date,pn_discount_percent,null, pn_recur_schedule,pn_threshold_amount);
    END;
    --R45 and above   
    PROCEDURE EDIT_CAMPAIGN(
        pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
        pv_campaign_name REPORT.CAMPAIGN.CAMPAIGN_NAME%TYPE,
        pv_campaign_desc REPORT.CAMPAIGN.CAMPAIGN_DESC%TYPE,
        pn_campaign_type_id REPORT.CAMPAIGN.CAMPAIGN_TYPE_ID%TYPE,
        pd_start_date REPORT.CAMPAIGN.START_DATE%TYPE,
        pd_end_date REPORT.CAMPAIGN.END_DATE%TYPE,
        pn_discount_percent REPORT.CAMPAIGN.DISCOUNT_PERCENT%TYPE,
        pn_donation_percent REPORT.CAMPAIGN.DONATION_PERCENT%TYPE,
        pn_recur_schedule REPORT.CAMPAIGN.RECUR_SCHEDULE%TYPE,
        pn_threshold_amount REPORT.CAMPAIGN.THRESHOLD_AMOUNT%TYPE
        )
     IS
    BEGIN
        update report.campaign set campaign_name=pv_campaign_name,
        campaign_desc=pv_campaign_desc,
        campaign_type_id=pn_campaign_type_id,
        start_date=pd_start_date,
        end_date=pd_end_date,
        discount_percent=pn_discount_percent/100,
        donation_percent=pn_donation_percent/100,
        recur_schedule=pn_recur_schedule,
        threshold_amount=pn_threshold_amount
        where campaign_id=pn_campaign_id;      
        IF pn_campaign_type_id != 1 THEN
          DELETE_CAMPAIGN_ASSIGNMENT(pn_campaign_id);
        END IF;
    END;
    
    PROCEDURE DELETE_CAMPAIGN(
        pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE
        )
     IS
    BEGIN
        DELETE_CAMPAIGN_ASSIGNMENT(pn_campaign_id);
        delete from pss.campaign_consumer_acct where campaign_id=pn_campaign_id;
        update report.campaign set status='D' where campaign_id=pn_campaign_id; 
    END;
    
    PROCEDURE ADD_CAMP_POS_PTA_BY_POS(
      pn_pos_pta_id PSS.CAMPAIGN_POS_PTA.POS_PTA_ID%TYPE,
      pn_pos_id PSS.POS.POS_ID%TYPE,
      pc_delete_flag CHAR)
    IS
      l_lock VARCHAR2(128);
    BEGIN 
      l_lock := GLOBALS_PKG.REQUEST_LOCK('PSS.CAMPAIGN_POS_PTA',pn_pos_pta_id);
      
      IF pc_delete_flag = 'Y' THEN
        DELETE FROM PSS.CAMPAIGN_POS_PTA WHERE pos_pta_id = pn_pos_pta_id;
      END IF;
      
      INSERT INTO PSS.CAMPAIGN_POS_PTA(CAMPAIGN_POS_PTA_ID, CAMPAIGN_ID, POS_PTA_ID, START_DATE, END_DATE, PRIORITY, TERMINAL_EPORT_ID)
      select PSS.CAMPAIGN_POS_PTA_SEQ.nextval, ca.campaign_id, pn_pos_pta_id, 
      greatest(NVL(c.START_DATE, MIN_DATE), NVL(te.START_DATE, MIN_DATE)) as start_date, 
      least(NVL(c.END_DATE, MAX_DATE),NVL(te.END_DATE, MAX_DATE)) as end_date,
        CASE WHEN ca.TERMINAL_ID is not null THEN 10 WHEN ca.region_id is not null THEN 20 ELSE 30 END as priority, te.terminal_eport_id
        from report.terminal t join report.terminal_eport te on t.terminal_id=te.terminal_id 
        left outer join report.terminal_region tr on tr.terminal_id =t.terminal_id
        join report.campaign_assignment ca on (t.terminal_id=ca.terminal_id 
        or (ca.customer_id=t.customer_id and ca.region_id=tr.region_id ) 
        or (ca.customer_id=t.customer_id and ca.region_id is null ))     
        join report.campaign c on ca.campaign_id=c.campaign_id 
        join report.eport e ON te.eport_id = e.eport_id
        join device.device d on d.device_serial_cd=e.eport_serial_num and d.device_active_yn_flag='Y' and not(d.device_type_id = 14 and d.device_sub_type_id IN(1,2))
        join pss.pos p on p.device_id=d.device_id and p.pos_id=pn_pos_id
        WHERE greatest(NVL(c.START_DATE, MIN_DATE), NVL(te.START_DATE, MIN_DATE))<least(NVL(c.END_DATE, MAX_DATE),NVL(te.END_DATE, MAX_DATE));
    END;
    
    PROCEDURE ADD_CAM_POS_PTA_BY_REG(
      pn_terminal_id REPORT.TERMINAL.TERMINAL_ID%TYPE,
      pn_region_id REPORT.TERMINAL_REGION.REGION_ID%TYPE)
    IS
      l_lock VARCHAR2(128);
      l_pos_pta_ids NUMBER_TABLE;
      l_count NUMBER;
    BEGIN 
      l_pos_pta_ids:=NUMBER_TABLE();
      FOR l_cur in (select pt.pos_pta_id, greatest(NVL(c.START_DATE, MIN_DATE), NVL(te.START_DATE, MIN_DATE)) as start_date, 
        least(NVL(c.END_DATE, MAX_DATE),NVL(te.END_DATE, MAX_DATE)) as end_date, ca.campaign_id, te.terminal_eport_id
        from report.terminal t join report.terminal_eport te on t.terminal_id=te.terminal_id and t.terminal_id=pn_terminal_id
        left outer join report.campaign_assignment ca on ((ca.customer_id=t.customer_id and ca.region_id=pn_region_id ))     
        left outer join report.campaign c on ca.campaign_id=c.campaign_id 
        join report.eport e ON te.eport_id = e.eport_id
        join device.device d on d.device_serial_cd=e.eport_serial_num and d.device_active_yn_flag='Y' and not(d.device_type_id = 14 and d.device_sub_type_id IN(1,2))
        join pss.pos p on p.device_id=d.device_id
        join pss.pos_pta pt on p.pos_id = pt.pos_id
        join pss.payment_subtype sb on pt.payment_subtype_id=sb.payment_subtype_id and sb.PAYMENT_SUBTYPE_CLASS='Internal::Prepaid'
        )
      LOOP
          l_count:=0;
          select count(1) into l_count from table(l_pos_pta_ids) where column_value=l_cur.pos_pta_id;
          IF l_count = 0 THEN
            l_lock := GLOBALS_PKG.REQUEST_LOCK('PSS.CAMPAIGN_POS_PTA',l_cur.pos_pta_id);
            DELETE FROM PSS.CAMPAIGN_POS_PTA WHERE pos_pta_id = l_cur.pos_pta_id and priority=20 and terminal_eport_id=l_cur.terminal_eport_id;
          END IF;
          l_pos_pta_ids.extend;
          l_pos_pta_ids(l_pos_pta_ids.last):=l_cur.pos_pta_id;

        IF l_cur.campaign_id is not null and l_cur.start_date <l_cur.end_date THEN
          INSERT INTO PSS.CAMPAIGN_POS_PTA(CAMPAIGN_POS_PTA_ID, CAMPAIGN_ID, POS_PTA_ID, START_DATE, END_DATE, PRIORITY, TERMINAL_EPORT_ID)
          VALUES(PSS.CAMPAIGN_POS_PTA_SEQ.nextval, l_cur.campaign_id, l_cur.pos_pta_id, l_cur.start_date, l_cur.end_date, 20, l_cur.terminal_eport_id );
         END IF;
       END LOOP;
      EXCEPTION
            WHEN NO_DATA_FOUND THEN
              RETURN;
    END;
    
    PROCEDURE ADD_CAM_POS_PTA_BY_TER_EPORT(
    pn_terminal_id REPORT.TERMINAL_EPORT.TERMINAL_ID%TYPE,
        pn_eport_id REPORT.TERMINAL_EPORT.EPORT_ID%TYPE,
        pd_start_date REPORT.TERMINAL_EPORT.START_DATE%TYPE,
    pd_end_date REPORT.TERMINAL_EPORT.END_DATE%TYPE,
    pn_terminal_eport_id REPORT.TERMINAL_EPORT.TERMINAL_EPORT_ID%TYPE,
    pc_delete_only_flag CHAR DEFAULT 'N')
    IS
      l_lock VARCHAR2(128);
      l_pos_pta_ids NUMBER_TABLE;
      l_count NUMBER;
    BEGIN   
      IF pc_delete_only_flag = 'Y' THEN
        DELETE FROM PSS.CAMPAIGN_POS_PTA WHERE terminal_eport_id=pn_terminal_eport_id;
      ELSE
        l_pos_pta_ids:=NUMBER_TABLE();
        FOR l_cur in (select pt.pos_pta_id, greatest(NVL(c.START_DATE, MIN_DATE), NVL(pd_start_date, MIN_DATE)) as start_date, least(NVL(c.END_DATE, MAX_DATE),NVL(pd_end_date, MAX_DATE)) as end_date,
          CASE WHEN ca.TERMINAL_ID is not null THEN 10 WHEN ca.region_id is not null THEN 20 ELSE 30 END as priority, ca.campaign_id
          from report.terminal t  
          left outer join report.terminal_region tr on tr.terminal_id =t.terminal_id
          left outer join report.campaign_assignment ca on (t.terminal_id=ca.terminal_id 
          or (ca.customer_id=t.customer_id and ca.region_id=tr.region_id ) 
          or (ca.customer_id=t.customer_id and ca.region_id is null ))     
          left outer join report.campaign c on ca.campaign_id=c.campaign_id 
          join report.eport e ON e.eport_id=pn_eport_id
          join device.device d on d.device_serial_cd=e.eport_serial_num and d.device_active_yn_flag='Y' and not(d.device_type_id = 14 and d.device_sub_type_id IN(1,2))
          join pss.pos p on p.device_id=d.device_id
          join pss.pos_pta pt on p.pos_id = pt.pos_id
          join pss.payment_subtype sb on pt.payment_subtype_id=sb.payment_subtype_id and sb.PAYMENT_SUBTYPE_CLASS='Internal::Prepaid'
          WHERE t.terminal_id=pn_terminal_id)
        LOOP     
            l_count:=0;
            select count(1) into l_count from table(l_pos_pta_ids) where column_value=l_cur.pos_pta_id;
            IF l_count = 0 THEN
               l_lock := GLOBALS_PKG.REQUEST_LOCK('PSS.CAMPAIGN_POS_PTA',l_cur.pos_pta_id);
               DELETE FROM PSS.CAMPAIGN_POS_PTA WHERE pos_pta_id = l_cur.pos_pta_id and terminal_eport_id=pn_terminal_eport_id;
            END IF;
            l_pos_pta_ids.extend;
            l_pos_pta_ids(l_pos_pta_ids.last):=l_cur.pos_pta_id;
             
          IF l_cur.campaign_id is not null and l_cur.start_date < l_cur.end_date THEN
            INSERT INTO PSS.CAMPAIGN_POS_PTA(CAMPAIGN_POS_PTA_ID, CAMPAIGN_ID, POS_PTA_ID, START_DATE, END_DATE, PRIORITY, TERMINAL_EPORT_ID)
            VALUES(PSS.CAMPAIGN_POS_PTA_SEQ.nextval, l_cur.campaign_id, l_cur.pos_pta_id, l_cur.start_date, l_cur.end_date, l_cur.priority, pn_terminal_eport_id);
          END IF;
        END LOOP;
      END IF;
      EXCEPTION
            WHEN NO_DATA_FOUND THEN
              RETURN;
    END;
    
    PROCEDURE DELETE_CAMPAIGN_ASSIGNMENT(
        pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE)
    IS
    BEGIN
      delete from report.CAMPAIGN_ASSIGNMENT where campaign_id = pn_campaign_id;
      delete from PSS.campaign_pos_pta where campaign_id=pn_campaign_id;
    END;
    
    PROCEDURE EDIT_CAMPAIGN_CUSTOMER(
      pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
      pn_customer_id REPORT.CAMPAIGN_ASSIGNMENT.CUSTOMER_ID%TYPE,
      pv_add CHAR)
    IS
    BEGIN    
      delete from report.campaign_assignment 
      where campaign_id=pn_campaign_id and customer_id = pn_customer_id and region_id is null;
      IF pv_add = 'Y' THEN 
        insert into report.campaign_assignment(campaign_assignment_id, campaign_id, customer_id)
        values(REPORT.CAMPAIGN_ASSIGNMENT_SEQ.nextval,pn_campaign_id,pn_customer_id);
      END IF;
    END;
    
    PROCEDURE EDIT_CAMPAIGN_REGION(
      pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
      pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
      pn_customer_id REPORT.CAMPAIGN_ASSIGNMENT.CUSTOMER_ID%TYPE,
      pn_region_id NUMBER_TABLE,
      pv_add CHAR)
    IS
    BEGIN    
      delete from report.campaign_assignment 
      where campaign_id=pn_campaign_id and customer_id = pn_customer_id and region_id member of pn_region_id;
      
      IF pv_add = 'Y' THEN
        insert into report.campaign_assignment(campaign_assignment_id, campaign_id, customer_id, region_id)
        select REPORT.CAMPAIGN_ASSIGNMENT_SEQ.nextval, pn_campaign_id, pn_customer_id, column_value customer_id from table(pn_region_id);
      END IF;
    END;
    
    PROCEDURE EDIT_CAMPAIGN_TERMINAL(
      pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
      pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
      pn_terminal_id NUMBER_TABLE,
      pv_add CHAR)
    IS
    BEGIN
      delete from report.campaign_assignment where campaign_id=pn_campaign_id and terminal_id member of pn_terminal_id;
      IF pv_add = 'Y' THEN
        insert into report.campaign_assignment(campaign_assignment_id, campaign_id, terminal_id)
        select REPORT.CAMPAIGN_ASSIGNMENT_SEQ.nextval, pn_campaign_id, column_value terminal_id from table(pn_terminal_id);
      END IF;
    END;
    
    PROCEDURE ADD_CAM_POS_PTA_BY_CAMPAIGN(
      pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE)
    IS
    BEGIN
      DELETE from PSS.CAMPAIGN_POS_PTA where CAMPAIGN_ID=pn_campaign_id;
      FOR l_cur in (
      select pt.pos_pta_id, greatest(NVL(c.START_DATE, MIN_DATE), NVL(te.START_DATE, MIN_DATE)) as start_date, least(NVL(c.END_DATE, MAX_DATE),NVL(te.END_DATE, MAX_DATE)) as end_date,
        CASE WHEN ca.TERMINAL_ID is not null THEN 10 WHEN ca.region_id is not null THEN 20 ELSE 30 END as priority, te.terminal_eport_id
        from report.terminal t join report.terminal_eport te on t.terminal_id=te.terminal_id
        left outer join report.terminal_region tr on tr.terminal_id =t.terminal_id
        join report.campaign_assignment ca on (t.terminal_id=ca.terminal_id 
        or (ca.customer_id=t.customer_id and ca.region_id=tr.region_id ) 
        or (ca.customer_id=t.customer_id and ca.region_id is null ))     
        join report.campaign c on ca.campaign_id=c.campaign_id and ca.campaign_id=pn_campaign_id
        join report.eport e ON te.eport_id = e.eport_id
        join device.device d on d.device_serial_cd=e.eport_serial_num and d.device_active_yn_flag='Y' and not(d.device_type_id = 14 and d.device_sub_type_id IN(1,2))
        join pss.pos p on p.device_id=d.device_id
        join pss.pos_pta pt on p.pos_id = pt.pos_id
        join pss.payment_subtype sb on pt.payment_subtype_id=sb.payment_subtype_id and sb.PAYMENT_SUBTYPE_CLASS='Internal::Prepaid'
        WHERE greatest(NVL(c.START_DATE, MIN_DATE), NVL(te.START_DATE, MIN_DATE))<least(NVL(c.END_DATE, MAX_DATE),NVL(te.END_DATE, MAX_DATE)))
      LOOP
        INSERT INTO PSS.CAMPAIGN_POS_PTA(CAMPAIGN_POS_PTA_ID, CAMPAIGN_ID, POS_PTA_ID, START_DATE, END_DATE, PRIORITY, TERMINAL_EPORT_ID)
        VALUES(PSS.CAMPAIGN_POS_PTA_SEQ.nextval, pn_campaign_id, l_cur.pos_pta_id, l_cur.start_date, l_cur.end_date, l_cur.priority, l_cur.terminal_eport_id);
      END LOOP;
    END;
    
    PROCEDURE ADD_CARDS_TO_CAMPAIGN(
      pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
      pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
      pn_card_id NUMBER_TABLE)
    IS
    BEGIN 
      insert into pss.campaign_consumer_acct(CONSUMER_ACCT_ID,CAMPAIGN_ID)
      SELECT CONSUMER_ACCT_ID,pn_campaign_id
      FROM (SELECT CA.CONSUMER_ACCT_ID
        FROM PSS.CONSUMER_ACCT CA 
        WHERE CA.CORP_CUSTOMER_ID in (SELECT DISTINCT T.CUSTOMER_ID
         FROM REPORT.VW_USER_TERMINAL UT
         INNER JOIN REPORT.VW_TERMINAL T ON UT.TERMINAL_ID = T.TERMINAL_ID      
         WHERE UT.USER_ID = pn_user_id) and CONSUMER_ACCT_ID member of pn_card_id
        minus
        SELECT CONSUMER_ACCT_ID from pss.campaign_consumer_acct where campaign_id=pn_campaign_id);
        
    END;
    
    PROCEDURE REMOVE_CARDS_FROM_CAMPAIGN(
      pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
      pn_card_id NUMBER_TABLE)
    IS
    BEGIN 
      delete from pss.campaign_consumer_acct where campaign_id=pn_campaign_id
      and CONSUMER_ACCT_ID member of pn_card_id;
    END;
    
    --begin pre R36, can be removed afterwards   
    FUNCTION                 FIND_CAMPAIGN (
        l_terminal_id REPORT.TERMINAL_EPORT.TERMINAL_ID%TYPE,
        l_region_id REPORT.TERMINAL_REGION.REGION_ID%TYPE,
        l_customer_id REPORT.TERMINAL.CUSTOMER_ID%TYPE)
    RETURN REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE IS
        l_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE;
        l_count NUMBER;
    BEGIN
        select max(campaign_id) into l_campaign_id from report.campaign_assignment where terminal_id=l_terminal_id;
        IF l_campaign_id is NULL THEN
          select max(campaign_id) into l_campaign_id from report.campaign_assignment where customer_id=l_customer_id and region_id=l_region_id;
          IF l_campaign_id is NULL THEN
            select max(campaign_id) into l_campaign_id from report.campaign_assignment where customer_id=l_customer_id and region_id is null;
          END IF;
        END IF;
      RETURN l_campaign_id;  
    END;
    
    FUNCTION                 FIND_CAMPAIGN (
        l_terminal_id REPORT.TERMINAL_EPORT.TERMINAL_ID%TYPE)
    RETURN REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE IS
        l_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE;
        l_region_id REPORT.TERMINAL_REGION.REGION_ID%TYPE;
        l_customer_id REPORT.CAMPAIGN.CUSTOMER_ID%TYPE;
        l_count NUMBER;
    BEGIN
        select t.customer_id, tr.region_id into l_customer_id, l_region_id 
        from report.terminal t left outer join report.terminal_region tr on t.terminal_id=tr.terminal_id
        where t.terminal_id=l_terminal_id;
        l_campaign_id:=FIND_CAMPAIGN(l_terminal_id, l_region_id, l_customer_id);
      RETURN l_campaign_id;  
    END;
    
    PROCEDURE ADD_CAM_POS_PTA_BY_TER(
      pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
      pn_terminal_id REPORT.TERMINAL.TERMINAL_ID%TYPE)
    IS
      l_start_date DATE;
      l_end_date DATE;
      l_lock VARCHAR2(128);
    BEGIN 
      FOR l_cur in (select pt.pos_pta_id as l_pos_pta_id, NVL(te.START_DATE, MIN_DATE) as l_start_date, NVL(te.END_DATE, MAX_DATE) as l_end_date
        from report.terminal t join report.terminal_eport te on t.terminal_id=te.terminal_id
        join report.eport e ON te.eport_id = e.eport_id
        join device.device d on d.device_serial_cd=e.eport_serial_num and d.device_active_yn_flag='Y' and not(d.device_type_id = 14 and d.device_sub_type_id IN(1,2))
        join pss.pos p on p.device_id=d.device_id
        join pss.pos_pta pt on p.pos_id = pt.pos_id
        join pss.payment_subtype sb on pt.payment_subtype_id=sb.payment_subtype_id and sb.PAYMENT_SUBTYPE_CLASS='Internal::Prepaid'
        WHERE SYSDATE >= NVL(te.START_DATE, MIN_DATE)
        AND SYSDATE    < NVL(te.END_DATE, MAX_DATE) and t.terminal_id=pn_terminal_id)
      LOOP
        l_lock := GLOBALS_PKG.REQUEST_LOCK('PSS.CAMPAIGN_POS_PTA',l_cur.l_pos_pta_id);
        DELETE FROM PSS.CAMPAIGN_POS_PTA WHERE pos_pta_id = l_cur.l_pos_pta_id;
        
        IF pn_campaign_id is not null THEN
          select greatest(NVL(START_DATE, MIN_DATE), l_cur.l_start_date), least(NVL(END_DATE, MAX_DATE), l_cur.l_end_date) into l_start_date, l_end_date 
          from report.campaign where campaign_id=pn_campaign_id;
          INSERT INTO PSS.CAMPAIGN_POS_PTA(CAMPAIGN_POS_PTA_ID, CAMPAIGN_ID, POS_PTA_ID, START_DATE, END_DATE)
          VALUES(PSS.CAMPAIGN_POS_PTA_SEQ.nextval, pn_campaign_id, l_cur.l_pos_pta_id, l_start_date, l_end_date );
         END IF;
       END LOOP;
      EXCEPTION
            WHEN NO_DATA_FOUND THEN
              RETURN;
    END;
    
    PROCEDURE ADD_CAM_POS_PTA_BY_TER_EPORT(
      pn_terminal_id REPORT.TERMINAL_EPORT.TERMINAL_ID%TYPE,
      pn_eport_id REPORT.TERMINAL_EPORT.EPORT_ID%TYPE,
      pd_start_date REPORT.TERMINAL_EPORT.START_DATE%TYPE,
    pd_end_date REPORT.TERMINAL_EPORT.END_DATE%TYPE)
    IS
      l_campaign_id NUMBER;
      l_start_date DATE;
      l_end_date DATE;
      l_lock VARCHAR2(128);
    BEGIN
      l_campaign_id:=find_campaign(pn_terminal_id);
      
      FOR l_cur in (select pt.pos_pta_id as l_pos_pta_id, NVL(pd_start_date, MIN_DATE) as l_start_date, NVL(pd_end_date, MAX_DATE) as l_end_date
        from device.device d 
        join report.eport e on d.device_serial_cd=e.eport_serial_num and d.device_active_yn_flag='Y' and not(d.device_type_id = 14 and d.device_sub_type_id IN(1,2))
        join pss.pos p on p.device_id=d.device_id
        join pss.pos_pta pt on p.pos_id = pt.pos_id
        join pss.payment_subtype sb on pt.payment_subtype_id=sb.payment_subtype_id and sb.PAYMENT_SUBTYPE_CLASS='Internal::Prepaid'
        WHERE SYSDATE >= NVL(pd_start_date, MIN_DATE)
        AND SYSDATE    < NVL(pd_end_date, MAX_DATE) and e.eport_id=pn_eport_id)
      LOOP
        
        l_lock := GLOBALS_PKG.REQUEST_LOCK('PSS.CAMPAIGN_POS_PTA',l_cur.l_pos_pta_id);
        DELETE FROM PSS.CAMPAIGN_POS_PTA WHERE pos_pta_id = l_cur.l_pos_pta_id;
        
        IF l_campaign_id is not null THEN
          select greatest(NVL(START_DATE, MIN_DATE), l_cur.l_start_date), least(NVL(END_DATE, MAX_DATE), l_cur.l_end_date) into l_start_date, l_end_date 
          from report.campaign where campaign_id=l_campaign_id;
          INSERT INTO PSS.CAMPAIGN_POS_PTA(CAMPAIGN_POS_PTA_ID, CAMPAIGN_ID, POS_PTA_ID, START_DATE, END_DATE)
          VALUES(PSS.CAMPAIGN_POS_PTA_SEQ.nextval, l_campaign_id, l_cur.l_pos_pta_id, l_start_date, l_end_date );
        END IF;
      END LOOP;
      EXCEPTION
            WHEN NO_DATA_FOUND THEN
              RETURN;
    END;
    
    PROCEDURE ADD_CAMPAIGN_CUSTOMER(
      pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
      pn_customer_id NUMBER_TABLE)
    IS
    BEGIN    
      delete from report.campaign_assignment 
      where customer_id member of pn_customer_id and region_id is null;
      
      insert into report.campaign_assignment(campaign_assignment_id, campaign_id, customer_id)
      select REPORT.CAMPAIGN_ASSIGNMENT_SEQ.nextval, pn_campaign_id, column_value customer_id from table(pn_customer_id);
    END;
    
    PROCEDURE ADD_CAMPAIGN_TERMINAL(
      pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
      pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
      pn_terminal_id NUMBER_TABLE,
      pn_assgined_terminal_id OUT NUMBER_TABLE)
    IS
    BEGIN
      select terminal_id bulk collect into pn_assgined_terminal_id from (
        select column_value terminal_id from table(pn_terminal_id) p join REPORT.VW_USER_TERMINAL UT on p.column_value=ut.terminal_id and ut.user_id = pn_user_id
        minus
        select t.terminal_id from report.terminal t join report.campaign_assignment ca on ca.campaign_id=pn_campaign_id and t.customer_id=ca.customer_id and ca.region_id is null
        minus
        select t.terminal_id from report.terminal t join report.terminal_region tr on t.terminal_id=tr.terminal_id 
        join report.campaign_assignment ca on ca.campaign_id=pn_campaign_id and t.customer_id=ca.customer_id and tr.region_id=ca.region_id
      );
      
      delete from report.campaign_assignment where terminal_id member of pn_assgined_terminal_id;

      insert into report.campaign_assignment(campaign_assignment_id, campaign_id, terminal_id)
      select REPORT.CAMPAIGN_ASSIGNMENT_SEQ.nextval, pn_campaign_id, column_value terminal_id from table(pn_assgined_terminal_id);
    END;
    
    PROCEDURE CREATE_CAMPAIGN(
        pv_campaign_name REPORT.CAMPAIGN.CAMPAIGN_NAME%TYPE,
        pv_campaign_desc REPORT.CAMPAIGN.CAMPAIGN_DESC%TYPE,
        pn_campaign_type_id REPORT.CAMPAIGN.CAMPAIGN_TYPE_ID%TYPE,
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pd_start_date REPORT.CAMPAIGN.START_DATE%TYPE,
        pd_end_date REPORT.CAMPAIGN.END_DATE%TYPE,
        pn_discount_percent REPORT.CAMPAIGN.DISCOUNT_PERCENT%TYPE,
        pn_campaign_id OUT REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE
        )
    IS
    BEGIN
       CREATE_CAMPAIGN(pv_campaign_name,pv_campaign_desc,pn_campaign_type_id,pn_user_id,pd_start_date,pd_end_date,pn_discount_percent, null, null, pn_campaign_id);
    END;
        
   PROCEDURE EDIT_CAMPAIGN(
        pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
        pv_campaign_name REPORT.CAMPAIGN.CAMPAIGN_NAME%TYPE,
        pv_campaign_desc REPORT.CAMPAIGN.CAMPAIGN_DESC%TYPE,
        pn_campaign_type_id REPORT.CAMPAIGN.CAMPAIGN_TYPE_ID%TYPE,
        pd_start_date REPORT.CAMPAIGN.START_DATE%TYPE,
        pd_end_date REPORT.CAMPAIGN.END_DATE%TYPE,
        pn_discount_percent REPORT.CAMPAIGN.DISCOUNT_PERCENT%TYPE
        )
     IS
    BEGIN
      EDIT_CAMPAIGN(pn_campaign_id, pv_campaign_name,pv_campaign_desc,pn_campaign_type_id,pd_start_date,pd_end_date,pn_discount_percent, null,null);
    END;
     -- end of pre R36, can be removed afterwards
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/PSS/PKG_PREPAID.psk?rev=1.14
CREATE OR REPLACE PACKAGE PSS.PKG_PREPAID 
IS
    -- R42 and above
    PROCEDURE CREATE_PREPAID_CONSUMER(
        pn_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
        pv_security_code PSS.CONSUMER_ACCT.SECURITY_CD_HASH%TYPE,
        pv_email PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pv_cell PSS.CONSUMER.CONSUMER_CELL_PHONE_NUM%TYPE,
        pn_carrier_id PSS.CONSUMER.CONSUMER_CELL_SMS_GATEWAY_ID%TYPE,
        pn_preferred_comm_type_id PSS.CONSUMER.PREFERRED_COMM_TYPE_ID%TYPE,
        pv_first PSS.CONSUMER.CONSUMER_FNAME%TYPE,
        pv_last PSS.CONSUMER.CONSUMER_LNAME%TYPE,
        pv_addr1 PSS.CONSUMER.CONSUMER_ADDR1%TYPE,
        pv_city PSS.CONSUMER.CONSUMER_CITY%TYPE,
        pv_state PSS.CONSUMER.CONSUMER_STATE_CD%TYPE,
        pv_postal PSS.CONSUMER.CONSUMER_POSTAL_CD%TYPE,
        pv_country PSS.CONSUMER.CONSUMER_COUNTRY_CD%TYPE,
        pb_credential_hash PSS.CONSUMER_CREDENTIAL.CREDENTIAL_HASH%TYPE,
        pb_credential_salt PSS.CONSUMER_CREDENTIAL.CREDENTIAL_SALT%TYPE,
        pv_credential_alg PSS.CONSUMER_CREDENTIAL.CREDENTIAL_ALG%TYPE,
        pn_consumer_id OUT PSS.CONSUMER.CONSUMER_ID%TYPE,
        pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
        pv_passcode OUT PSS.CONSUMER_PASSCODE.PASSCODE%TYPE,
        pv_username OUT PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pv_communication_email OUT PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pn_region_id PSS.CONSUMER.REGION_ID%TYPE DEFAULT NULL);
    
    PROCEDURE CREATE_PREPAID_CONSUMER_SIMPLE(
        pn_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
        pv_security_code PSS.CONSUMER_ACCT.SECURITY_CD_HASH%TYPE,
        pv_first PSS.CONSUMER.CONSUMER_FNAME%TYPE,
        pv_last PSS.CONSUMER.CONSUMER_LNAME%TYPE,
        pn_region_id PSS.CONSUMER.REGION_ID%TYPE,
        pn_consumer_id OUT PSS.CONSUMER.CONSUMER_ID%TYPE,
        pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE);
        
    -- R44 and below
    PROCEDURE ADD_PREPAID_ACCT(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pn_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
        pv_security_code PSS.CONSUMER_ACCT.SECURITY_CD_HASH%TYPE,
        pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE);
    
    -- R45 and above
    PROCEDURE ADD_PREPAID_ACCT(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pn_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
        pv_security_code PSS.CONSUMER_ACCT.SECURITY_CD_HASH%TYPE,
        pv_nickname PSS.CONSUMER_ACCT.NICK_NAME%TYPE,
        pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE);   
       
    -- R42 and above
    PROCEDURE UPDATE_PREPAID_CONSUMER(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pv_email PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pv_cell PSS.CONSUMER.CONSUMER_CELL_PHONE_NUM%TYPE DEFAULT NULL,
        pn_carrier_id PSS.CONSUMER.CONSUMER_CELL_SMS_GATEWAY_ID%TYPE DEFAULT NULL,
        pn_preferred_comm_type_id PSS.CONSUMER.PREFERRED_COMM_TYPE_ID%TYPE,
        pv_first PSS.CONSUMER.CONSUMER_FNAME%TYPE,
        pv_last PSS.CONSUMER.CONSUMER_LNAME%TYPE,
        pv_addr1 PSS.CONSUMER.CONSUMER_ADDR1%TYPE,
        pv_city PSS.CONSUMER.CONSUMER_CITY%TYPE,
        pv_state PSS.CONSUMER.CONSUMER_STATE_CD%TYPE,
        pv_postal PSS.CONSUMER.CONSUMER_POSTAL_CD%TYPE,
        pv_country PSS.CONSUMER.CONSUMER_COUNTRY_CD%TYPE,
        pb_credential_hash PSS.CONSUMER_CREDENTIAL.CREDENTIAL_HASH%TYPE,
        pb_credential_salt PSS.CONSUMER_CREDENTIAL.CREDENTIAL_SALT%TYPE,
        pv_credential_alg PSS.CONSUMER_CREDENTIAL.CREDENTIAL_ALG%TYPE,
        pv_passcode OUT PSS.CONSUMER_PASSCODE.PASSCODE%TYPE,
        pv_username OUT PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pv_communication_email OUT PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pn_region_id PSS.CONSUMER.REGION_ID%TYPE DEFAULT NULL);
   
    PROCEDURE UPDATE_CONSUMER_SIMPLE(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pv_first PSS.CONSUMER.CONSUMER_FNAME%TYPE,
        pv_last PSS.CONSUMER.CONSUMER_LNAME%TYPE,
        pn_region_id PSS.CONSUMER.REGION_ID%TYPE);
        
    -- R42 and above
    PROCEDURE CREATE_PASSCODE_FOR_PASSWORD(
        pv_username PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pn_consumer_id OUT PSS.CONSUMER.CONSUMER_ID%TYPE,
        pn_preferred_comm_type_id OUT PSS.CONSUMER.PREFERRED_COMM_TYPE_ID%TYPE,
        pv_first OUT PSS.CONSUMER.CONSUMER_FNAME%TYPE,
        pv_last OUT PSS.CONSUMER.CONSUMER_LNAME%TYPE,
        pv_passcode OUT PSS.CONSUMER_PASSCODE.PASSCODE%TYPE);
        
    PROCEDURE UPDATE_PREPAID_PASSWORD(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pv_passcode PSS.CONSUMER_PASSCODE.PASSCODE%TYPE,
        pb_credential_hash PSS.CONSUMER_CREDENTIAL.CREDENTIAL_HASH%TYPE,
        pb_credential_salt PSS.CONSUMER_CREDENTIAL.CREDENTIAL_SALT%TYPE,
        pv_credential_alg PSS.CONSUMER_CREDENTIAL.CREDENTIAL_ALG%TYPE);
    
    -- R42 and above
    PROCEDURE UPDATE_PREPAID_PASSWORD(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pb_credential_hash PSS.CONSUMER_CREDENTIAL.CREDENTIAL_HASH%TYPE,
        pb_credential_salt PSS.CONSUMER_CREDENTIAL.CREDENTIAL_SALT%TYPE,
        pv_credential_alg PSS.CONSUMER_CREDENTIAL.CREDENTIAL_ALG%TYPE);
        
    --R42 and above
    PROCEDURE REGISTER_PREPAID_CONSUMER(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pv_passcode PSS.CONSUMER_PASSCODE.PASSCODE%TYPE,
        pv_username OUT PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pv_communication_email OUT PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pn_preferred_comm_type_id OUT PSS.CONSUMER.PREFERRED_COMM_TYPE_ID%TYPE,
        pv_first OUT PSS.CONSUMER.CONSUMER_FNAME%TYPE,
        pv_last OUT PSS.CONSUMER.CONSUMER_LNAME%TYPE);
        
   --R42 and above
   PROCEDURE REREGISTER_PREPAID_CONSUMER(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pv_username OUT PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pv_communication_email OUT PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pn_preferred_comm_type_id OUT PSS.CONSUMER.PREFERRED_COMM_TYPE_ID%TYPE,
        pv_first OUT PSS.CONSUMER.CONSUMER_FNAME%TYPE,
        pv_last OUT PSS.CONSUMER.CONSUMER_LNAME%TYPE,
        pv_passcode OUT PSS.CONSUMER_PASSCODE.PASSCODE%TYPE);
        
    PROCEDURE CANCEL_PASSCODE(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pv_passcode PSS.CONSUMER_PASSCODE.PASSCODE%TYPE);
        
    PROCEDURE UPDATE_CONSUMER_SETTING(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pn_param_id PSS.CONSUMER_SETTING.CONSUMER_SETTING_PARAM_ID%TYPE,
        pv_param_value PSS.CONSUMER_SETTING.CONSUMER_SETTING_VALUE%TYPE);
    
    FUNCTION GET_PASSCODE(
        l_consumer_id CONSUMER_PASSCODE.CONSUMER_ID%TYPE)
    RETURN CONSUMER_PASSCODE.PASSCODE%TYPE;
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/PSS/PKG_PREPAID.pbk?rev=1.24
CREATE OR REPLACE PACKAGE BODY PSS.PKG_PREPAID 
IS  
    PROCEDURE VERIFY_REGION_BY_ACCT(
        pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE, 
        pn_region_id PSS.CONSUMER.REGION_ID%TYPE)
    IS
        ln_cnt PLS_INTEGER;
    BEGIN
        IF pn_region_id IS NULL THEN
            RETURN;
        END IF;
        SELECT COUNT(*)
          INTO ln_cnt
          FROM PSS.CONSUMER_ACCT CA
          JOIN REPORT.REGION R ON CA.CORP_CUSTOMER_ID = R.CUSTOMER_ID
         WHERE CA.CONSUMER_ACCT_ID = pn_consumer_acct_id
           AND R.REGION_ID = pn_region_id;
        IF ln_cnt = 0 THEN
            RAISE_APPLICATION_ERROR(-20304, 'Region not valid for this card');
        END IF;
    END;
       
    PROCEDURE VERIFY_REGION(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE, 
        pn_region_id PSS.CONSUMER.REGION_ID%TYPE)
    IS
        ln_cnt PLS_INTEGER;
    BEGIN
        IF pn_region_id IS NULL THEN
            RETURN;
        END IF;
        SELECT COUNT(*)
          INTO ln_cnt
          FROM PSS.CONSUMER_ACCT CA
          JOIN REPORT.REGION R ON CA.CORP_CUSTOMER_ID = R.CUSTOMER_ID
         WHERE CA.CONSUMER_ID = pn_consumer_id
           AND CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y'
           AND R.REGION_ID = pn_region_id;
        IF ln_cnt = 0 THEN
            RAISE_APPLICATION_ERROR(-20305, 'Region not valid for this consumer');
        END IF;
    END;     
    
    -- R42 and above
    PROCEDURE CREATE_PREPAID_CONSUMER(
        pn_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
        pv_security_code PSS.CONSUMER_ACCT.SECURITY_CD_HASH%TYPE,
        pv_email PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pv_cell PSS.CONSUMER.CONSUMER_CELL_PHONE_NUM%TYPE,
        pn_carrier_id PSS.CONSUMER.CONSUMER_CELL_SMS_GATEWAY_ID%TYPE,
        pn_preferred_comm_type_id PSS.CONSUMER.PREFERRED_COMM_TYPE_ID%TYPE,
        pv_first PSS.CONSUMER.CONSUMER_FNAME%TYPE,
        pv_last PSS.CONSUMER.CONSUMER_LNAME%TYPE,
        pv_addr1 PSS.CONSUMER.CONSUMER_ADDR1%TYPE,
        pv_city PSS.CONSUMER.CONSUMER_CITY%TYPE,
        pv_state PSS.CONSUMER.CONSUMER_STATE_CD%TYPE,
        pv_postal PSS.CONSUMER.CONSUMER_POSTAL_CD%TYPE,
        pv_country PSS.CONSUMER.CONSUMER_COUNTRY_CD%TYPE,
        pb_credential_hash PSS.CONSUMER_CREDENTIAL.CREDENTIAL_HASH%TYPE,
        pb_credential_salt PSS.CONSUMER_CREDENTIAL.CREDENTIAL_SALT%TYPE,
        pv_credential_alg PSS.CONSUMER_CREDENTIAL.CREDENTIAL_ALG%TYPE,
        pn_consumer_id OUT PSS.CONSUMER.CONSUMER_ID%TYPE,
        pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
        pv_passcode OUT PSS.CONSUMER_PASSCODE.PASSCODE%TYPE,
        pv_username OUT PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pv_communication_email OUT PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pn_region_id PSS.CONSUMER.REGION_ID%TYPE DEFAULT NULL)
    IS
        ln_cnt PLS_INTEGER;
        lc_flag CHAR(1);
        lv_lock VARCHAR2(28);
        ln_consumer_type_id PSS.CONSUMER.CONSUMER_TYPE_ID%TYPE;
        lv_city PSS.CONSUMER.CONSUMER_CITY%TYPE;
        lv_state_cd PSS.CONSUMER.CONSUMER_STATE_CD%TYPE;
        lv_email PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE;
        lv_cell PSS.CONSUMER.CONSUMER_CELL_PHONE_NUM%TYPE;  
    BEGIN
        SELECT TRIM(pv_email), REGEXP_REPLACE(pv_cell, '\s*\(?\s*(\d{3})\s*\)?\s*[-.]?\s*(\d{3})\s*[-.]?\s*(\d{4})\s*', '\1\2\3') -- format these two fields first
          INTO lv_email, lv_cell
          FROM DUAL;        
        lv_lock := DBADMIN.PKG_GLOBAL.REQUEST_LOCK('PSS.CONSUMER_ACCT', pn_global_account_id); 
        SELECT COUNT(*), MAX(CA.CONSUMER_ACCT_ID), 
               NVL(MAX(CASE 
                    WHEN C.CONSUMER_TYPE_ID = 5 AND (UPPER(C.CONSUMER_EMAIL_ADDR1) = UPPER(lv_email) OR C.CONSUMER_CELL_PHONE_NUM = lv_cell) AND CC.CREDENTIAL_ACTIVE_FLAG = 'Y' THEN 'Y' -- Registered and matches and not new
                    WHEN C.CONSUMER_TYPE_ID = 5 AND (UPPER(C.CONSUMER_EMAIL_ADDR1) = UPPER(lv_email) OR C.CONSUMER_CELL_PHONE_NUM = lv_cell) AND CC.CREDENTIAL_ACTIVE_FLAG = 'N' THEN 'A' -- Registered and matches and new - so send email
                    END), 'N'),
               MAX(CASE 
                    WHEN (UPPER(C.CONSUMER_EMAIL_ADDR1) = UPPER(lv_email) OR C.CONSUMER_CELL_PHONE_NUM = lv_cell) THEN C.CONSUMER_ID -- Registered and matches and new - so send email
                    END),
               MAX(C.CONSUMER_TYPE_ID)
          INTO ln_cnt, pn_consumer_acct_id, lc_flag, pn_consumer_id, ln_consumer_type_id
          FROM PSS.CONSUMER_ACCT CA
          JOIN PSS.CONSUMER_ACCT_BASE CAB ON CA.CONSUMER_ACCT_ID = CAB.CONSUMER_ACCT_ID
          JOIN PSS.CONSUMER C ON CA.CONSUMER_ID = C.CONSUMER_ID
          LEFT OUTER JOIN PSS.CONSUMER_CREDENTIAL CC ON C.CONSUMER_ID = CC.CONSUMER_ID AND CC.RESOURCE_KEY = 'PREPAID_WEBSITE'
         WHERE CAB.GLOBAL_ACCOUNT_ID = pn_global_account_id           
           AND CA.CONSUMER_ACCT_FMT_ID = 2
           AND CA.CONSUMER_ACCT_TYPE_ID = 3
           AND CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y'
		       AND DBADMIN.PKG_UTL.EQL(CA.SECURITY_CD_HASH, DBADMIN.GET_HASH(UTL_RAW.CAST_TO_RAW(pv_security_code), CA.SECURITY_CD_SALT, 'SHA-256/1000')) = 'Y';
        IF ln_cnt = 0 THEN
            RAISE_APPLICATION_ERROR(-20300, 'Did not find an account for card id ' || pn_global_account_id);
        ELSIF ln_consumer_type_id != 4 THEN
            IF lc_flag = 'A' THEN
                DELETE 
                  FROM PSS.CONSUMER_CREDENTIAL
                 WHERE CONSUMER_ID = pn_consumer_id
                   AND RESOURCE_KEY = 'PREPAID_WEBSITE';
                INSERT INTO PSS.CONSUMER_CREDENTIAL(CONSUMER_ID, CREDENTIAL_HASH, CREDENTIAL_SALT, CREDENTIAL_ALG, RESOURCE_KEY)
                    VALUES(pn_consumer_id, pb_credential_hash, pb_credential_salt, pv_credential_alg, 'PREPAID_WEBSITE');
                pv_passcode := PSS.PKG_CONSUMER_MAINT.CREATE_CONSUMER_PASSCODE(pn_consumer_id, 'REGISTER_PREPAID');
            ELSIF lc_flag = 'Y' THEN
                RAISE_APPLICATION_ERROR(-20301, 'Account already registered to this email');
            ELSE
                RAISE_APPLICATION_ERROR(-20302, 'Account already registered to another email');
            END IF;
        ELSE
            VERIFY_REGION_BY_ACCT(pn_consumer_acct_id, pn_region_id);
            IF pv_state IS NOT NULL THEN
                SELECT COUNT(*)
                  INTO ln_cnt
                  FROM LOCATION.STATE
                 WHERE STATE_CD = pv_state;
                IF ln_cnt = 0 THEN
                    RAISE_APPLICATION_ERROR(-20309, 'Invalid state ' || pv_state || ' provided');
                END IF;
                lv_city := pv_city;
                lv_state_cd := pv_state;
            ELSE
                SELECT MAX(CITY), MAX(STATE_CD)
                  INTO lv_city, lv_state_cd
                  FROM (
                  SELECT P.CITY, S.STATE_CD
                    FROM FRONT.POSTAL P 
                    JOIN CORP.STATE S ON P.STATE_ID = S.STATE_ID
                    JOIN CORP.COUNTRY C ON S.COUNTRY_CD = C.COUNTRY_CD
                  WHERE P.POSTAL_CD = REGEXP_REPLACE(pv_postal, C.POSTAL_REGEX, 
                    (SELECT LISTAGG('\' || LEVEL, '') WITHIN GROUP (ORDER BY LEVEL) FROM DUAL CONNECT BY LEVEL <= C.POSTAL_PRIMARY_PARTS))
                    AND C.COUNTRY_CD = pv_country
                    AND C.POSTAL_REGEX IS NOT NULL
                  ORDER BY P.POSTAL_ID ASC)
                WHERE ROWNUM = 1;
            END IF;
            SELECT PSS.SEQ_CONSUMER_ID.NEXTVAL
              INTO pn_consumer_id
              FROM DUAL;
            BEGIN
                INSERT INTO PSS.CONSUMER(
                    CONSUMER_ID,
                    CONSUMER_EMAIL_ADDR1,
                    CONSUMER_CELL_PHONE_NUM,
                    CONSUMER_CELL_SMS_GATEWAY_ID,
                    PREFERRED_COMM_TYPE_ID,
                    CONSUMER_FNAME,
                    CONSUMER_LNAME,
                    CONSUMER_ADDR1,
                    CONSUMER_CITY,
                    CONSUMER_STATE_CD,
                    CONSUMER_POSTAL_CD,
                    CONSUMER_COUNTRY_CD,
                    CONSUMER_TYPE_ID,
                    REGION_ID)
                VALUES(
                    pn_consumer_id,
                    lv_email,
                    lv_cell,
                    pn_carrier_id,
                    pn_preferred_comm_type_id,
                    pv_first,
                    pv_last,
                    pv_addr1,
                    lv_city,
                    lv_state_cd,
                    pv_postal,
                    pv_country,
                    5,
                    pn_region_id);
            EXCEPTION
                WHEN DUP_VAL_ON_INDEX THEN
                    RAISE_APPLICATION_ERROR(-20303, 'This email is already registered with another card.');
            END;
            UPDATE PSS.CONSUMER_ACCT
               SET CONSUMER_ID = pn_consumer_id
             WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id;
            INSERT INTO PSS.CONSUMER_CREDENTIAL(CONSUMER_ID, CREDENTIAL_HASH, CREDENTIAL_SALT, CREDENTIAL_ALG, RESOURCE_KEY)
                VALUES(pn_consumer_id, pb_credential_hash, pb_credential_salt, pv_credential_alg, 'PREPAID_WEBSITE');
            pv_passcode := PSS.PKG_CONSUMER_MAINT.CREATE_CONSUMER_PASSCODE(pn_consumer_id, 'REGISTER_PREPAID');   
        END IF;
        IF pn_preferred_comm_type_id = 1 THEN
            pv_username := lv_email;
            pv_communication_email := lv_email;
        ELSIF pn_preferred_comm_type_id = 2 THEN
            pv_username := lv_cell;
            IF pn_carrier_id IS NOT NULL THEN
              SELECT lv_cell || SMS_EMAIL_SUFFIX
                INTO pv_communication_email
                FROM FRONT.SMS_GATEWAY
               WHERE SMS_GATEWAY_ID = pn_carrier_id;
            END IF;
        END IF;
    END;
    
    PROCEDURE CREATE_PREPAID_CONSUMER_SIMPLE(
        pn_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
        pv_security_code PSS.CONSUMER_ACCT.SECURITY_CD_HASH%TYPE,
        pv_first PSS.CONSUMER.CONSUMER_FNAME%TYPE,
        pv_last PSS.CONSUMER.CONSUMER_LNAME%TYPE,
        pn_region_id PSS.CONSUMER.REGION_ID%TYPE,
        pn_consumer_id OUT PSS.CONSUMER.CONSUMER_ID%TYPE,
        pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE)
    IS
        ln_cnt PLS_INTEGER;
        lv_lock VARCHAR2(28);
        ln_consumer_type_id PSS.CONSUMER.CONSUMER_TYPE_ID%TYPE;
        lc_flag CHAR(1);
    BEGIN
      lv_lock := DBADMIN.PKG_GLOBAL.REQUEST_LOCK('PSS.CONSUMER_ACCT', pn_global_account_id); 
      
      SELECT COUNT(*), MAX(CA.CONSUMER_ACCT_ID), MAX(C.CONSUMER_ID), MAX(C.CONSUMER_TYPE_ID),
          MAX(CASE WHEN pv_first=C.CONSUMER_FNAME and pv_last=C.CONSUMER_LNAME THEN 'Y' ELSE 'N' END)
          INTO ln_cnt, pn_consumer_acct_id, pn_consumer_id, ln_consumer_type_id, lc_flag
          FROM PSS.CONSUMER_ACCT CA
          JOIN PSS.CONSUMER_ACCT_BASE CAB ON CA.CONSUMER_ACCT_ID = CAB.CONSUMER_ACCT_ID
          JOIN PSS.CONSUMER C ON CA.CONSUMER_ID = C.CONSUMER_ID
         WHERE CAB.GLOBAL_ACCOUNT_ID = pn_global_account_id           
           AND CA.CONSUMER_ACCT_FMT_ID = 2
           AND CA.CONSUMER_ACCT_TYPE_ID = 3
           AND CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y'
		       AND DBADMIN.PKG_UTL.EQL(CA.SECURITY_CD_HASH, DBADMIN.GET_HASH(UTL_RAW.CAST_TO_RAW(pv_security_code), CA.SECURITY_CD_SALT, 'SHA-256/1000')) = 'Y';
           
        IF ln_cnt = 0 THEN
            RAISE_APPLICATION_ERROR(-20300, 'Did not find an account for card id ' || pn_global_account_id);
        ELSIF ln_consumer_type_id != 4 THEN
          IF lc_flag = 'Y' THEN
            RAISE_APPLICATION_ERROR(-20301, 'Account already registered by you');
          ELSE
            RAISE_APPLICATION_ERROR(-20302, 'Account already registered by someone else');
          END IF;
        ELSE
            VERIFY_REGION_BY_ACCT(pn_consumer_acct_id, pn_region_id);
            SELECT PSS.SEQ_CONSUMER_ID.NEXTVAL
              INTO pn_consumer_id
              FROM DUAL;
            BEGIN
                INSERT INTO PSS.CONSUMER(
                    CONSUMER_ID,
                    CONSUMER_FNAME,
                    CONSUMER_LNAME,
                    CONSUMER_TYPE_ID,
                    REGION_ID)
                VALUES(
                    pn_consumer_id,
                    pv_first,
                    pv_last,
                    5,
                    pn_region_id);
            END;
            
            UPDATE PSS.CONSUMER_ACCT
               SET CONSUMER_ID = pn_consumer_id
             WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id; 
           
        END IF;
    END;
    
    -- R44
    PROCEDURE ADD_PREPAID_ACCT(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pn_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
        pv_security_code PSS.CONSUMER_ACCT.SECURITY_CD_HASH%TYPE,
        pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE)
    IS
    BEGIN
      ADD_PREPAID_ACCT(pn_consumer_id,pn_global_account_id,pv_security_code,null,pn_consumer_acct_id);
    END;
    -- R45 and above
    PROCEDURE ADD_PREPAID_ACCT(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pn_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
        pv_security_code PSS.CONSUMER_ACCT.SECURITY_CD_HASH%TYPE,
        pv_nickname PSS.CONSUMER_ACCT.NICK_NAME%TYPE,
        pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE)
    IS
        ln_cnt PLS_INTEGER;
        lc_flag CHAR(1);
        lv_lock VARCHAR2(28);
    BEGIN
        lv_lock := DBADMIN.PKG_GLOBAL.REQUEST_LOCK('PSS.CONSUMER_ACCT', pn_global_account_id);
        SELECT CC.CREDENTIAL_ACTIVE_FLAG
          INTO lc_flag
          FROM PSS.CONSUMER C
          LEFT OUTER JOIN PSS.CONSUMER_CREDENTIAL CC ON C.CONSUMER_ID = CC.CONSUMER_ID AND CC.RESOURCE_KEY = 'PREPAID_WEBSITE'
         WHERE C.CONSUMER_ID = pn_consumer_id
           AND C.CONSUMER_TYPE_ID = 5;      
       
        SELECT COUNT(*), MAX(DECODE(C.CONSUMER_TYPE_ID, 4, CA.CONSUMER_ACCT_ID)), MAX(DECODE(C.CONSUMER_ID, pn_consumer_id, 'Y', 'N'))
          INTO ln_cnt, pn_consumer_acct_id, lc_flag
          FROM PSS.CONSUMER_ACCT CA
          JOIN PSS.CONSUMER_ACCT_BASE CAB ON CA.CONSUMER_ACCT_ID = CAB.CONSUMER_ACCT_ID
          JOIN PSS.CONSUMER C ON CA.CONSUMER_ID = C.CONSUMER_ID
         WHERE CAB.GLOBAL_ACCOUNT_ID = pn_global_account_id           
           AND CA.CONSUMER_ACCT_FMT_ID = 2
           AND CA.CONSUMER_ACCT_TYPE_ID = 3
		   AND CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y'
		   AND DBADMIN.PKG_UTL.EQL(CA.SECURITY_CD_HASH, DBADMIN.GET_HASH(UTL_RAW.CAST_TO_RAW(pv_security_code), CA.SECURITY_CD_SALT, 'SHA-256/1000')) = 'Y';
        IF ln_cnt = 0 THEN
            RAISE_APPLICATION_ERROR(-20300, 'Did not find an account for card id ' || pn_global_account_id);
        ELSIF pn_consumer_acct_id IS NULL THEN
            IF lc_flag = 'Y' THEN
                RAISE_APPLICATION_ERROR(-20301, 'Account already registered to this email');
            ELSE
                RAISE_APPLICATION_ERROR(-20302, 'Account already registered to another email');
            END IF;
        ELSE
            UPDATE PSS.CONSUMER_ACCT
               SET CONSUMER_ID = pn_consumer_id,
               NICK_NAME = pv_nickname
             WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id;
        END IF;
    END;
    
    -- R42 and above
    PROCEDURE UPDATE_PREPAID_CONSUMER(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pv_email PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pv_cell PSS.CONSUMER.CONSUMER_CELL_PHONE_NUM%TYPE DEFAULT NULL,
        pn_carrier_id PSS.CONSUMER.CONSUMER_CELL_SMS_GATEWAY_ID%TYPE DEFAULT NULL,
        pn_preferred_comm_type_id PSS.CONSUMER.PREFERRED_COMM_TYPE_ID%TYPE,
        pv_first PSS.CONSUMER.CONSUMER_FNAME%TYPE,
        pv_last PSS.CONSUMER.CONSUMER_LNAME%TYPE,
        pv_addr1 PSS.CONSUMER.CONSUMER_ADDR1%TYPE,
        pv_city PSS.CONSUMER.CONSUMER_CITY%TYPE,
        pv_state PSS.CONSUMER.CONSUMER_STATE_CD%TYPE,
        pv_postal PSS.CONSUMER.CONSUMER_POSTAL_CD%TYPE,
        pv_country PSS.CONSUMER.CONSUMER_COUNTRY_CD%TYPE,
        pb_credential_hash PSS.CONSUMER_CREDENTIAL.CREDENTIAL_HASH%TYPE,
        pb_credential_salt PSS.CONSUMER_CREDENTIAL.CREDENTIAL_SALT%TYPE,
        pv_credential_alg PSS.CONSUMER_CREDENTIAL.CREDENTIAL_ALG%TYPE,
        pv_passcode OUT PSS.CONSUMER_PASSCODE.PASSCODE%TYPE,
        pv_username OUT PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pv_communication_email OUT PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pn_region_id PSS.CONSUMER.REGION_ID%TYPE DEFAULT NULL)
    IS
        ln_cnt PLS_INTEGER;
        lc_comm_address_changed CHAR(1);
        lv_city PSS.CONSUMER.CONSUMER_CITY%TYPE;
        lv_state_cd PSS.CONSUMER.CONSUMER_STATE_CD%TYPE;        
        lv_email PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE;
        lv_cell PSS.CONSUMER.CONSUMER_CELL_PHONE_NUM%TYPE;  
    BEGIN
        VERIFY_REGION(pn_consumer_id, pn_region_id);
        SELECT TRIM(pv_email), REGEXP_REPLACE(pv_cell, '\s*\(?\s*(\d{3})\s*\)?\s*[-.]?\s*(\d{3})\s*[-.]?\s*(\d{4})\s*', '\1\2\3') -- format these two fields first
          INTO lv_email, lv_cell
          FROM DUAL;        
        IF pv_state IS NOT NULL THEN
            SELECT COUNT(*)
              INTO ln_cnt
              FROM LOCATION.STATE
             WHERE STATE_CD = pv_state;
            IF ln_cnt = 0 THEN
                RAISE_APPLICATION_ERROR(-20309, 'Invalid state ' || pv_state || ' provided');
            END IF;
            lv_city := pv_city;
            lv_state_cd := pv_state;
        ELSE
            SELECT MAX(CITY), MAX(STATE_CD)
              INTO lv_city, lv_state_cd
              FROM (
              SELECT P.CITY, S.STATE_CD
                FROM FRONT.POSTAL P 
                JOIN CORP.STATE S ON P.STATE_ID = S.STATE_ID
                JOIN CORP.COUNTRY C ON S.COUNTRY_CD = C.COUNTRY_CD
              WHERE P.POSTAL_CD = REGEXP_REPLACE(pv_postal, C.POSTAL_REGEX, 
                (SELECT LISTAGG('\' || LEVEL, '') WITHIN GROUP (ORDER BY LEVEL) FROM DUAL CONNECT BY LEVEL <= C.POSTAL_PRIMARY_PARTS))
                AND C.COUNTRY_CD = pv_country
                AND C.POSTAL_REGEX IS NOT NULL
              ORDER BY P.POSTAL_ID ASC)
            WHERE ROWNUM = 1;
        END IF;
        SELECT CASE WHEN PREFERRED_COMM_TYPE_ID != pn_preferred_comm_type_id THEN 'Y'
                    WHEN PREFERRED_COMM_TYPE_ID = 1 AND UPPER(CONSUMER_EMAIL_ADDR1) != UPPER(lv_email) THEN 'Y'
                    WHEN PREFERRED_COMM_TYPE_ID = 2 AND CONSUMER_CELL_PHONE_NUM != lv_cell THEN 'Y'
                    ELSE 'N'
               END
          INTO lc_comm_address_changed
          FROM PSS.CONSUMER
         WHERE CONSUMER_ID = pn_consumer_id
           AND CONSUMER_TYPE_ID = 5
           FOR UPDATE;   
        UPDATE PSS.CONSUMER
           SET CONSUMER_EMAIL_ADDR1 = TRIM(pv_email),
               CONSUMER_CELL_PHONE_NUM = lv_cell,
               CONSUMER_CELL_SMS_GATEWAY_ID = pn_carrier_id,
               PREFERRED_COMM_TYPE_ID = pn_preferred_comm_type_id,
               CONSUMER_FNAME = pv_first,
               CONSUMER_LNAME = pv_last,
               CONSUMER_ADDR1 = pv_addr1,
               CONSUMER_CITY = lv_city,
               CONSUMER_STATE_CD = lv_state_cd,
               CONSUMER_POSTAL_CD = pv_postal,
               CONSUMER_COUNTRY_CD = pv_country,
               REGION_ID = pn_region_id
         WHERE CONSUMER_ID = pn_consumer_id
           AND CONSUMER_TYPE_ID = 5;
         
        IF pb_credential_hash IS NOT NULL THEN
            BEGIN
              UPDATE PSS.CONSUMER_CREDENTIAL
                 SET CREDENTIAL_HASH = pb_credential_hash, 
                     CREDENTIAL_SALT = pb_credential_salt,
                     CREDENTIAL_ALG = pv_credential_alg,
                     CREDENTIAL_ACTIVE_FLAG = DECODE(lc_comm_address_changed, 'N', CREDENTIAL_ACTIVE_FLAG, 'A')
               WHERE CONSUMER_ID = pn_consumer_id
                 AND RESOURCE_KEY = 'PREPAID_WEBSITE';
            IF sql%rowcount = 0 THEN
                INSERT INTO PSS.CONSUMER_CREDENTIAL(CONSUMER_ID, CREDENTIAL_HASH, CREDENTIAL_SALT, CREDENTIAL_ALG, RESOURCE_KEY)
                      VALUES(pn_consumer_id, pb_credential_hash, pb_credential_salt, pv_credential_alg, 'PREPAID_WEBSITE');
            END IF;
            END;
            IF lc_comm_address_changed = 'Y' THEN
                pv_passcode := PSS.PKG_CONSUMER_MAINT.CREATE_CONSUMER_PASSCODE(pn_consumer_id, 'REGISTER_PREPAID');     
            END IF;
        ELSIF lc_comm_address_changed = 'Y' THEN
            UPDATE PSS.CONSUMER_CREDENTIAL
               SET CREDENTIAL_ACTIVE_FLAG = 'N'
             WHERE CONSUMER_ID = pn_consumer_id
               AND RESOURCE_KEY = 'PREPAID_WEBSITE';
            pv_passcode := PSS.PKG_CONSUMER_MAINT.CREATE_CONSUMER_PASSCODE(pn_consumer_id, 'REGISTER_PREPAID');
        END IF;
        IF pn_preferred_comm_type_id = 1 THEN
            pv_username := lv_email;
            pv_communication_email := lv_email;
        ELSIF pn_preferred_comm_type_id = 2 THEN
            pv_username := lv_cell;
            IF pn_carrier_id IS NOT NULL THEN
              SELECT lv_cell || SMS_EMAIL_SUFFIX
                INTO pv_communication_email
                FROM FRONT.SMS_GATEWAY
               WHERE SMS_GATEWAY_ID = pn_carrier_id;
            END IF;
        END IF;
    EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
            RAISE_APPLICATION_ERROR(-20303, 'This email is already registered with another card.');
    END;
    
    PROCEDURE UPDATE_CONSUMER_SIMPLE(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pv_first PSS.CONSUMER.CONSUMER_FNAME%TYPE,
        pv_last PSS.CONSUMER.CONSUMER_LNAME%TYPE,
        pn_region_id PSS.CONSUMER.REGION_ID%TYPE)
    IS
    BEGIN
      VERIFY_REGION(pn_consumer_id, pn_region_id);
      UPDATE PSS.CONSUMER
      SET CONSUMER_FNAME = pv_first, CONSUMER_LNAME = pv_last, REGION_ID = pn_region_id
      WHERE CONSUMER_ID = pn_consumer_id AND CONSUMER_TYPE_ID = 5;
    END;
    
    --R42 and above
    PROCEDURE REGISTER_PREPAID_CONSUMER(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pv_passcode PSS.CONSUMER_PASSCODE.PASSCODE%TYPE,
        pv_username OUT PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pv_communication_email OUT PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pn_preferred_comm_type_id OUT PSS.CONSUMER.PREFERRED_COMM_TYPE_ID%TYPE,
        pv_first OUT PSS.CONSUMER.CONSUMER_FNAME%TYPE,
        pv_last OUT PSS.CONSUMER.CONSUMER_LNAME%TYPE)
    IS
        ld_exp_ts PSS.CONSUMER_PASSCODE.EXPIRATION_TS%TYPE;
        ln_consumer_passcode_id PSS.CONSUMER_PASSCODE.CONSUMER_PASSCODE_ID%TYPE;
        lc_active_flag PSS.CONSUMER_CREDENTIAL.CREDENTIAL_ACTIVE_FLAG%TYPE;
    BEGIN
        SELECT DECODE(C.PREFERRED_COMM_TYPE_ID, 1, C.CONSUMER_EMAIL_ADDR1, 2, C.CONSUMER_CELL_PHONE_NUM),
               C.PREFERRED_COMM_TYPE_ID,
               C.CONSUMER_FNAME, 
               C.CONSUMER_LNAME,
               CP.EXPIRATION_TS,
               CP.CONSUMER_PASSCODE_ID,
               CC.CREDENTIAL_ACTIVE_FLAG
          INTO pv_username, pn_preferred_comm_type_id, pv_first, pv_last, ld_exp_ts, ln_consumer_passcode_id, lc_active_flag
          FROM PSS.CONSUMER C
          LEFT OUTER JOIN (PSS.CONSUMER_PASSCODE CP
          JOIN PSS.PASSCODE_TYPE PT ON CP.PASSCODE_TYPE_ID = PT.PASSCODE_TYPE_ID AND PT.PASSCODE_TYPE_CD = 'REGISTER_PREPAID' AND CP.PASSCODE = pv_passcode) 
            ON C.CONSUMER_ID = CP.CONSUMER_ID
          LEFT OUTER JOIN PSS.CONSUMER_CREDENTIAL CC ON C.CONSUMER_ID = CC.CONSUMER_ID AND CC.RESOURCE_KEY = 'PREPAID_WEBSITE'
         WHERE C.CONSUMER_ID = pn_consumer_id;
        IF lc_active_flag IS NULL THEN
            RAISE_APPLICATION_ERROR(-20323, 'Credentials not found for consumer ' || pn_consumer_id);
        ELSIF lc_active_flag = 'Y' THEN
            RAISE_APPLICATION_ERROR(-20324, 'Consumer ' || pn_consumer_id || ' is already registered');
        ELSIF ln_consumer_passcode_id IS NULL THEN
            RAISE_APPLICATION_ERROR(-20321, 'Passcode ' || pv_passcode || ' does not exist for consumer ' || pn_consumer_id);
        ELSIF ld_exp_ts < SYSDATE THEN
            RAISE_APPLICATION_ERROR(-20322, 'Passcode ' || pv_passcode || ' is expired for consumer ' || pn_consumer_id || ' as of ' || TO_CHAR(ld_exp_ts, 'MM/DD/YYYY HH24:MI:SS'));
        ELSE
            UPDATE PSS.CONSUMER_CREDENTIAL
               SET CREDENTIAL_ACTIVE_FLAG = 'Y'
             WHERE CONSUMER_ID = pn_consumer_id
               AND RESOURCE_KEY = 'PREPAID_WEBSITE'
               AND CREDENTIAL_ACTIVE_FLAG IN('A', 'N');
            UPDATE PSS.CONSUMER_PASSCODE
               SET EXPIRATION_TS = SYSDATE
             WHERE CONSUMER_PASSCODE_ID = ln_consumer_passcode_id;
        END IF;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RAISE_APPLICATION_ERROR(-20320, 'Consumer ' || pn_consumer_id || ' does not exist');
    END;
    
    -- R42 and above
    PROCEDURE CREATE_PASSCODE_FOR_PASSWORD(
        pv_username PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pn_consumer_id OUT PSS.CONSUMER.CONSUMER_ID%TYPE,
        pn_preferred_comm_type_id OUT PSS.CONSUMER.PREFERRED_COMM_TYPE_ID%TYPE,
        pv_first OUT PSS.CONSUMER.CONSUMER_FNAME%TYPE,
        pv_last OUT PSS.CONSUMER.CONSUMER_LNAME%TYPE,
        pv_passcode OUT PSS.CONSUMER_PASSCODE.PASSCODE%TYPE)
    IS
    BEGIN
        SELECT CONSUMER_ID,
               PREFERRED_COMM_TYPE_ID,
               CONSUMER_FNAME, 
               CONSUMER_LNAME
          INTO pn_consumer_id, pn_preferred_comm_type_id, pv_first, pv_last
          FROM (
        SELECT C.CONSUMER_ID,
               C.PREFERRED_COMM_TYPE_ID,
               C.CONSUMER_FNAME, 
               C.CONSUMER_LNAME
          FROM PSS.CONSUMER C
         WHERE (UPPER(C.CONSUMER_EMAIL_ADDR1) = UPPER(TRIM(pv_username))
            OR C.CONSUMER_CELL_PHONE_NUM = REGEXP_REPLACE(pv_username, '\s*\(?\s*(\d{3})\s*\)?\s*[-.]?\s*(\d{3})\s*[-.]?\s*(\d{4})\s*', '\1\2\3')) 
           AND CONSUMER_TYPE_ID = 5
         ORDER BY CASE 
                    WHEN UPPER(C.CONSUMER_EMAIL_ADDR1) != TRIM(UPPER(pv_username)) THEN 0
                    WHEN C.PREFERRED_COMM_TYPE_ID = 1 THEN 10
                    ELSE 1 
                  END +
                  CASE 
                    WHEN C.CONSUMER_CELL_PHONE_NUM != REGEXP_REPLACE(pv_username, '\s*\(?\s*(\d{3})\s*\)?\s*[-.]?\s*(\d{3})\s*[-.]?\s*(\d{4})\s*', '\1\2\3') THEN 0
                    WHEN C.PREFERRED_COMM_TYPE_ID = 2 THEN 10
                    ELSE 1 
                  END DESC)
         WHERE ROWNUM = 1;
         
        SELECT MAX(PASSCODE)
          INTO pv_passcode
          FROM PSS.CONSUMER_PASSCODE CP
          JOIN PSS.PASSCODE_TYPE PT ON CP.PASSCODE_TYPE_ID = PT.PASSCODE_TYPE_ID AND PT.PASSCODE_TYPE_CD = 'RESET_PREPAID_PASS' 
         WHERE CP.CONSUMER_ID = pn_consumer_id AND EXPIRATION_TS > SYSDATE;
        IF pv_passcode IS NULL THEN
          pv_passcode := PSS.PKG_CONSUMER_MAINT.CREATE_CONSUMER_PASSCODE(pn_consumer_id, 'RESET_PREPAID_PASS');
        END IF;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RAISE_APPLICATION_ERROR(-20320, 'Consumer ' || pn_consumer_id || ' does not exist');
    END;
    
    -- R42 and above
    PROCEDURE UPDATE_PREPAID_PASSWORD(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pb_credential_hash PSS.CONSUMER_CREDENTIAL.CREDENTIAL_HASH%TYPE,
        pb_credential_salt PSS.CONSUMER_CREDENTIAL.CREDENTIAL_SALT%TYPE,
        pv_credential_alg PSS.CONSUMER_CREDENTIAL.CREDENTIAL_ALG%TYPE)
    IS
    BEGIN
        UPDATE PSS.CONSUMER_CREDENTIAL
           SET CREDENTIAL_HASH = pb_credential_hash, 
               CREDENTIAL_SALT = pb_credential_salt,
               CREDENTIAL_ALG = pv_credential_alg
         WHERE CONSUMER_ID = pn_consumer_id
           AND RESOURCE_KEY = 'PREPAID_WEBSITE';
    END;
    
    PROCEDURE UPDATE_PREPAID_PASSWORD(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pv_passcode PSS.CONSUMER_PASSCODE.PASSCODE%TYPE,
        pb_credential_hash PSS.CONSUMER_CREDENTIAL.CREDENTIAL_HASH%TYPE,
        pb_credential_salt PSS.CONSUMER_CREDENTIAL.CREDENTIAL_SALT%TYPE,
        pv_credential_alg PSS.CONSUMER_CREDENTIAL.CREDENTIAL_ALG%TYPE)
    IS
        ln_consumer_passcode_id PSS.CONSUMER_PASSCODE.CONSUMER_PASSCODE_ID%TYPE;
    BEGIN
        SELECT 
               MAX(CP.CONSUMER_PASSCODE_ID)
          INTO ln_consumer_passcode_id
          FROM PSS.CONSUMER_PASSCODE CP
          JOIN PSS.PASSCODE_TYPE PT ON CP.PASSCODE_TYPE_ID = PT.PASSCODE_TYPE_ID AND PT.PASSCODE_TYPE_CD = 'RESET_PREPAID_PASS' AND CP.PASSCODE = pv_passcode
         WHERE CP.CONSUMER_ID = pn_consumer_id AND EXPIRATION_TS > SYSDATE;
        IF ln_consumer_passcode_id IS NULL THEN
            RAISE_APPLICATION_ERROR(-20321, 'Passcode ' || pv_passcode || ' does not exist for consumer ' || pn_consumer_id);
        END IF;
        
        UPDATE PSS.CONSUMER_CREDENTIAL
           SET CREDENTIAL_HASH = pb_credential_hash, 
               CREDENTIAL_SALT = pb_credential_salt,
               CREDENTIAL_ALG = pv_credential_alg
         WHERE CONSUMER_ID = pn_consumer_id
           AND RESOURCE_KEY = 'PREPAID_WEBSITE';
    END;
   
   --R42 and above
   PROCEDURE REREGISTER_PREPAID_CONSUMER(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pv_username OUT PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pv_communication_email OUT PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pn_preferred_comm_type_id OUT PSS.CONSUMER.PREFERRED_COMM_TYPE_ID%TYPE,
        pv_first OUT PSS.CONSUMER.CONSUMER_FNAME%TYPE,
        pv_last OUT PSS.CONSUMER.CONSUMER_LNAME%TYPE,
        pv_passcode OUT PSS.CONSUMER_PASSCODE.PASSCODE%TYPE)
    IS
    BEGIN
        SELECT DECODE(C.PREFERRED_COMM_TYPE_ID, 1, C.CONSUMER_EMAIL_ADDR1, 2, C.CONSUMER_CELL_PHONE_NUM),
               DECODE(C.PREFERRED_COMM_TYPE_ID, 1, C.CONSUMER_EMAIL_ADDR1, 2, C.CONSUMER_CELL_PHONE_NUM || SG.SMS_EMAIL_SUFFIX),
               C.PREFERRED_COMM_TYPE_ID,
               C.CONSUMER_FNAME, 
               C.CONSUMER_LNAME
          INTO pv_username, pv_communication_email, pn_preferred_comm_type_id, pv_first, pv_last
          FROM PSS.CONSUMER C
          LEFT OUTER JOIN FRONT.SMS_GATEWAY SG ON C.CONSUMER_CELL_SMS_GATEWAY_ID = SG.SMS_GATEWAY_ID
         WHERE C.CONSUMER_ID = pn_consumer_id;
        pv_passcode := PSS.PKG_CONSUMER_MAINT.CREATE_CONSUMER_PASSCODE(pn_consumer_id, 'REGISTER_PREPAID');
    END;
    
    PROCEDURE CANCEL_PASSCODE(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pv_passcode PSS.CONSUMER_PASSCODE.PASSCODE%TYPE)
    IS
        ld_exp_ts PSS.CONSUMER_PASSCODE.EXPIRATION_TS%TYPE;
        ln_consumer_passcode_id PSS.CONSUMER_PASSCODE.CONSUMER_PASSCODE_ID%TYPE;
    BEGIN
        UPDATE PSS.CONSUMER_PASSCODE
           SET EXPIRATION_TS = LEAST(SYSDATE, NVL(EXPIRATION_TS, MAX_DATE))
         WHERE PASSCODE = pv_passcode
           AND CONSUMER_ID = pn_consumer_id;
        IF SQL%NOTFOUND THEN
            RAISE_APPLICATION_ERROR(-20321, 'Passcode ' || pv_passcode || ' does not exist for consumer ' || pn_consumer_id);
        END IF;
    END;
    
    PROCEDURE UPDATE_CONSUMER_SETTING(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pn_param_id PSS.CONSUMER_SETTING.CONSUMER_SETTING_PARAM_ID%TYPE,
        pv_param_value PSS.CONSUMER_SETTING.CONSUMER_SETTING_VALUE%TYPE)
    IS
        lv_param_value_actual PSS.CONSUMER_SETTING.CONSUMER_SETTING_VALUE%TYPE;
    BEGIN
        IF pv_param_value IS NULL THEN
            SELECT DEFAULT_VALUE
              INTO lv_param_value_actual
              FROM PSS.CONSUMER_SETTING_PARAM
             WHERE CONSUMER_SETTING_PARAM_ID = pn_param_id;
        ELSE
            lv_param_value_actual := pv_param_value;
        END IF;
        LOOP
            UPDATE PSS.CONSUMER_SETTING
               SET CONSUMER_SETTING_VALUE = lv_param_value_actual
             WHERE CONSUMER_ID = pn_consumer_id
               AND CONSUMER_SETTING_PARAM_ID = pn_param_id;
            EXIT WHEN SQL%FOUND;
            BEGIN
                INSERT INTO PSS.CONSUMER_SETTING(CONSUMER_ID, CONSUMER_SETTING_PARAM_ID, CONSUMER_SETTING_VALUE)
                    VALUES(pn_consumer_id, pn_param_id, lv_param_value_actual);
                EXIT;
            EXCEPTION
                WHEN DUP_VAL_ON_INDEX THEN
                    NULL;
            END;
        END LOOP;
    END;
    
    FUNCTION GET_PASSCODE(
    l_consumer_id CONSUMER_PASSCODE.CONSUMER_ID%TYPE)
    RETURN CONSUMER_PASSCODE.PASSCODE%TYPE
   IS
    l_passcode CONSUMER_PASSCODE.PASSCODE%TYPE;
   BEGIN
      SELECT MAX(PASSCODE) into l_passcode
          FROM PSS.CONSUMER_PASSCODE CP
          JOIN PSS.PASSCODE_TYPE PT ON CP.PASSCODE_TYPE_ID = PT.PASSCODE_TYPE_ID AND PT.PASSCODE_TYPE_CD = 'REGISTER_PREPAID' 
         WHERE CP.CONSUMER_ID = l_consumer_id AND EXPIRATION_TS > SYSDATE;
      IF l_passcode IS NULL THEN
        l_passcode:= PSS.PKG_CONSUMER_MAINT.CREATE_CONSUMER_PASSCODE(l_consumer_id, 'REGISTER_PREPAID');
      END IF;
      RETURN l_passcode;
   EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN NULL;
   END;
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/CORP/REFUND_PKG.pbk?rev=1.23
CREATE OR REPLACE PACKAGE BODY CORP.REFUND_PKG IS
-- Purpose: Briefly explain the functionality of the package body
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------
-- Noah S.     6/15/05 Creation

   /*
   Function REFUND_PKG.issue_refund

   Creates entries in REPORT.Refund and REPORT.Trans to represent
   the issued refund.

   Return Values:
         0: Success (Normal)
         1: Successful Manager Override
        -1: Failure (General)
        -2: Failed Manager Override
   */
   FUNCTION issue_refund(
        l_trans_id REPORT.TRANS.TRAN_ID%TYPE,
        l_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        l_amount REPORT.TRANS.TOTAL_AMOUNT%TYPE,
        l_reason_id CORP.REFUND_REASON.REFUND_REASON_ID%TYPE,
        l_comment CORP.REFUND.COMMENT_TEXT%TYPE,
        l_override CORP.REFUND.MANAGER_OVERRIDE_FLAG%TYPE
   ) RETURN INT IS
        l_return_val INT;
        l_remaining_amount REPORT.TRANS.total_amount%TYPE;
   BEGIN
        issue_refund_with_remains(l_trans_id, l_user_id, l_amount, l_reason_id, l_comment, l_override, 'N', l_return_val, l_remaining_amount);
        RETURN l_return_val;
   END;

   /*
   Function REFUND_PKG.issue_chargeback

   Creates entries in CORP.Chargeback and REPORT.Trans to represent
   the issued chargeback.

   Return Values:
         0: Success
        -1: Failure
   */
   FUNCTION issue_chargeback(
        l_trans_id REPORT.TRANS.TRAN_ID%TYPE,
        l_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        l_amount REPORT.TRANS.TOTAL_AMOUNT%TYPE,
        l_fee REPORT.TRANS.TOTAL_AMOUNT%TYPE,
        l_comment CORP.REFUND.COMMENT_TEXT%TYPE,
        l_override CORP.REFUND.MANAGER_OVERRIDE_FLAG%TYPE
   ) RETURN INT IS
        l_chargeback_trans_id REPORT.TRANS.TRAN_ID%TYPE;
        l_machine_trans_no REPORT.TRANS.MACHINE_TRANS_NO%TYPE;
        l_chargeback_id CORP.CHARGEBACK.CHARGEBACK_ID%TYPE;
        l_return_val INT;
        l_card_assoc_id CORP.CARD_ASSOC.CARD_ASSOC_ID%TYPE;
        l_card_assoc_name CORP.CARD_ASSOC.CARD_ASSOC_NAME%TYPE;
        l_prev_total REPORT.TRANS.TOTAL_AMOUNT%TYPE;
        l_trans_total REPORT.TRANS.TOTAL_AMOUNT%TYPE;
        l_total_chargeback_amt REPORT.TRANS.TOTAL_AMOUNT%TYPE;
        l_num PLS_INTEGER;
        l_date DATE := SYSDATE;
   BEGIN
        l_return_val := -1; --'Normal Failure'
        l_total_chargeback_amt := l_amount + NVL(l_fee,0);
        LOCK TABLE CORP.CHARGEBACK IN EXCLUSIVE MODE;

        BEGIN
            SELECT NVL(SUM(T.TOTAL_AMOUNT), 0), NVL(SUM(DECODE(T.TRANS_TYPE_ID, 21, 1, 0)), 0) + 1
              INTO l_prev_total, l_num
              FROM REPORT.TRANS T
			  JOIN REPORT.TRANS_TYPE TT ON T.TRANS_TYPE_ID = TT.TRANS_TYPE_ID
             WHERE T.ORIG_TRAN_ID = l_trans_id
               AND TT.REFUND_IND = 'Y';
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                l_prev_total := 0;
                l_num := 1;
            WHEN OTHERS THEN
                RAISE;
        END;
        SELECT
            REPORT.TRANS_SEQ.NEXTVAL,
            CORP.SEQ_CHARGEBACK_ID.NEXTVAL,
            'RF:' || SUBSTR(x.MACHINE_TRANS_NO,
                INSTR(x.MACHINE_TRANS_NO, ':') + 1, 100) || ':C'
                || TO_CHAR(l_num, 'FM9999999990'),
            x.TOTAL_AMOUNT
          INTO
            l_chargeback_trans_id,
            l_chargeback_id,
            l_machine_trans_no,
            l_trans_total
          FROM REPORT.TRANS x
         WHERE x.TRAN_ID = l_trans_id;

        IF (((ABS(l_prev_total) + ABS(l_total_chargeback_amt)) <= l_trans_total) OR (l_override = 'Y')) THEN
        	INSERT INTO REPORT.TRANS(
            	tran_id, 
            	card_number, 
            	total_amount, 
            	cc_appr_code,
                start_date, 
                close_date, 
                server_date, 
                settle_state_id, 
                eport_id,
                trans_type_id, 
                orig_tran_id, 
                terminal_id, 
                merchant_id,
                source_system_cd, 
                machine_trans_no, 
                customer_bank_id,
                process_fee_id, 
                create_date, 
                settle_date, 
                description, 
                currency_id,
                consumer_acct_id)
			SELECT
            	l_chargeback_trans_id, 
            	orig.CARD_NUMBER, 
            	-ABS(l_total_chargeback_amt), 
            	'SETTLED',
                l_date, 
                l_date, 
                l_date, 
                3, 
                orig.EPORT_ID, 
                21, 
                l_trans_id,
                orig.TERMINAL_ID, 
                orig.MERCHANT_ID, 
                'RA', 
                l_machine_trans_no,
                orig.CUSTOMER_BANK_ID, 
                orig.PROCESS_FEE_ID, 
                l_date, 
                l_date,
				REPORT.CARD_NAME(orig.CARDTYPE_AUTHORITY_ID, orig.TRANS_TYPE_ID, orig.CARD_NUMBER) || ' issued chargeback ' || 
                	orig.CC_APPR_CODE || ' ' || TO_CHAR(orig.start_date, 'MM/DD/yyyy') || 
                	' Tran #: ' || orig.TRAN_ID || ' Amt: ' || curr.CURRENCY_SYMBOL || 
                	TO_CHAR(l_amount,'FM9,999,990.00') || ' ' || curr.CURRENCY_CODE || ' Fee: ' 
                	|| curr.CURRENCY_SYMBOL || TO_CHAR(NVL(l_fee,0),'FM9,999,990.00') 
                	|| ' ' || curr.CURRENCY_CODE,
                orig.CURRENCY_ID,
                orig.CONSUMER_ACCT_ID
			FROM REPORT.TRANS orig 
                INNER JOIN CORP.CURRENCY curr ON orig.CURRENCY_ID = curr.CURRENCY_ID
                WHERE orig.TRAN_ID = l_trans_id;

            INSERT INTO CORP.CHARGEBACK(
            	chargeback_id, 
            	creator_user_id, 
            	tran_id, 
            	card_assoc_id, 
            	comment_text, 
            	manager_override_flag)
            SELECT
           		l_chargeback_id, 
           		l_user_id, 
           		l_chargeback_trans_id, 
           		(SELECT CA.CARD_ASSOC_ID 
				   FROM CORP.CARD_ASSOC CA, REPORT.TRANS X
				  WHERE CA.CARD_ASSOC_NAME = REPORT.CARD_COMPANY(x.CARD_NUMBER)
				    AND x.TRAN_ID = l_trans_id), 
           		l_comment, 
           		l_override
           	FROM DUAL;
            l_return_val := 0; --'Normal Success'
        END IF;
        
        IF ((ABS(l_prev_total) + ABS(l_total_chargeback_amt)) > l_trans_total) THEN
            IF (l_override = 'Y') THEN l_return_val := 1; --'Override Success'
            ELSE l_return_val := -2; --'Override Failure'
            END IF;
        END IF;

        RETURN l_return_val;
   END;
   
   PROCEDURE issue_refund_with_remains(
        l_trans_id REPORT.TRANS.TRAN_ID%TYPE,
        l_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        l_amount REPORT.TRANS.TOTAL_AMOUNT%TYPE,
        l_reason_id CORP.REFUND_REASON.REFUND_REASON_ID%TYPE,
        l_comment CORP.REFUND.COMMENT_TEXT%TYPE,
        l_override CORP.REFUND.MANAGER_OVERRIDE_FLAG%TYPE,
        l_has_more CHAR,
        l_return_val OUT INT,
        l_remaining_amount OUT REPORT.TRANS.total_amount%TYPE
   )IS
        l_refund_trans_id REPORT.TRANS.TRAN_ID%TYPE;
        l_machine_trans_no REPORT.TRANS.MACHINE_TRANS_NO%TYPE;
        l_refund_id CORP.REFUND.REFUND_ID%TYPE;
        l_refund_desc REPORT.TRANS.DESCRIPTION%TYPE;
        l_prev_total REPORT.TRANS.TOTAL_AMOUNT%TYPE;
        l_trans_total REPORT.TRANS.TOTAL_AMOUNT%TYPE;
        l_num PLS_INTEGER;
        l_lock VARCHAR2(128);
        l_update CHAR:='N';
        l_refund_amount REPORT.TRANS.TOTAL_AMOUNT%TYPE;
        l_upload_date REPORT.TRANS.SERVER_DATE%TYPE;
        l_max_refund_days INTEGER;
        lc_refundable_ind REPORT.TRANS_TYPE.REFUNDABLE_IND%TYPE;
        lc_payable_ind REPORT.TRANS_TYPE.PAYABLE_IND%TYPE;
        lc_replenish_ind REPORT.TRANS_TYPE.REPLENISH_IND%TYPE;
        ln_trans_type_id REPORT.TRANS_TYPE.TRANS_TYPE_ID%TYPE;
        lv_trans_type REPORT.TRANS_TYPE.TRANS_TYPE_NAME%TYPE;
        ln_source_tran_id REPORT.TRANS.SOURCE_TRAN_ID%TYPE;
        lv_currency_cd CORP.CURRENCY.CURRENCY_CODE%TYPE;
        lv_orig_machine_trans_no REPORT.TRANS.MACHINE_TRANS_NO%TYPE;
        lc_processed_flag CORP.REFUND.PROCESSED_FLAG%TYPE;
   BEGIN
        l_return_val := -1; --'Normal Failure'
        l_lock := GLOBALS_PKG.REQUEST_LOCK('REPORT.TRANS.TRAN_ID', l_trans_id);
        l_lock := GLOBALS_PKG.REQUEST_LOCK('CORP.REFUND.TRAN_ID', l_trans_id);
        BEGIN
            SELECT NVL(SUM(T.TOTAL_AMOUNT), 0), NVL(SUM(DECODE(T.TRANS_TYPE_ID, 21, 0, 1)), 0) + 1
              INTO l_prev_total, l_num
              FROM REPORT.TRANS T
              JOIN REPORT.TRANS_TYPE TT ON T.TRANS_TYPE_ID = TT.TRANS_TYPE_ID
             WHERE T.ORIG_TRAN_ID = l_trans_id
               AND TT.REFUND_IND = 'Y';
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                l_prev_total := 0;
                l_num := 1;
            WHEN OTHERS THEN
                RAISE;
        END;
        SELECT
            REPORT.TRANS_SEQ.NEXTVAL,
            CORP.SEQ_REFUND_ID.NEXTVAL,
            'RF:' || SUBSTR(x.MACHINE_TRANS_NO,
                INSTR(x.MACHINE_TRANS_NO, ':') + 1, 100) || ':R'
                || TO_CHAR(l_num, 'FM9999999990'),
            x.MACHINE_TRANS_NO,
            rr.REFUND_REASON,
            x.TOTAL_AMOUNT,
            X.SERVER_DATE,
            NVL(TO_NUMBER_OR_NULL(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('MAX_REFUND_DAYS')), 90),
            TT.REFUNDABLE_IND,
            TT.PAYABLE_IND,
            TT.REPLENISH_IND,
            TT.TRANS_TYPE_NAME,
            X.TRANS_TYPE_ID,
            X.SOURCE_TRAN_ID,
            CUR.CURRENCY_CODE
          INTO
            l_refund_trans_id,
            l_refund_id,
            l_machine_trans_no,
            lv_orig_machine_trans_no,
            l_refund_desc,
            l_trans_total,
            l_upload_date,
            l_max_refund_days,
            lc_refundable_ind,
            lc_payable_ind,
            lc_replenish_ind,
            lv_trans_type,
            ln_trans_type_id,
            ln_source_tran_id,
            lv_currency_cd
          FROM REPORT.TRANS x
         CROSS JOIN CORP.REFUND_REASON rr
          JOIN REPORT.TRANS_TYPE TT ON X.TRANS_TYPE_ID = TT.TRANS_TYPE_ID
          LEFT OUTER JOIN CORP.CURRENCY CUR ON X.CURRENCY_ID = CUR.CURRENCY_ID
         WHERE x.TRAN_ID = l_trans_id
           AND RR.REFUND_REASON_ID = l_reason_id;
	
        IF lc_refundable_ind != 'Y' THEN
            RAISE_APPLICATION_ERROR(-20802, 'Transaction '||TO_CHAR(l_trans_id)||' is not refundable because it is a ' || lv_trans_type || ' transaction');     
        ELSIF l_upload_date < SYSDATE - l_max_refund_days THEN
            RAISE_APPLICATION_ERROR(-20801, 'Transaction '||TO_CHAR(l_trans_id)||' was uploaded '||TO_CHAR(l_upload_date, 'MM/DD/YYYY')||' and is no longer eligible for a refund');
        ELSIF lc_replenish_ind = 'Y' THEN
            IF ((ABS(l_prev_total) + ABS(l_amount)) > l_trans_total) THEN
                RAISE_APPLICATION_ERROR(-20804, 'You may not refund a replenish transaction for more than it''s total amount. Refund must be $' ||TO_CHAR(l_trans_total - ABS(l_prev_total), 'FM9,999,990.00') || ' or less.');
            END IF;
            -- check balance on prepaid card
            -- check replenish bonus and adjust acct balance and eft as necessary
            DECLARE
                ln_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE;
                ln_consumer_acct_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_TYPE_ID%TYPE;
                ln_consumer_acct_balance PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE;
                ln_replenish_balance PSS.CONSUMER_ACCT.CONSUMER_ACCT_REPLEN_BALANCE%TYPE;
                ln_promo_balance PSS.CONSUMER_ACCT.CONSUMER_ACCT_PROMO_BALANCE%TYPE;
                ln_consumer_acct_sub_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_SUB_TYPE_ID%TYPE;
                ln_corp_customer_id PSS.CONSUMER_ACCT.CORP_CUSTOMER_ID%TYPE;
                lv_consumer_acct_cd PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD%TYPE;
                lv_consumer_acct_identifier PSS.CONSUMER_ACCT.CONSUMER_ACCT_IDENTIFIER%TYPE;
                ln_auth_hold_amt PSS.AUTH.AUTH_AMT%TYPE;
                ln_replenish_bonus NUMBER;
                lv_user_name VARCHAR2(4000);
                ln_doc_id CORP.DOC.DOC_ID%TYPE;
                ln_ledger_id CORP.LEDGER.LEDGER_ID%TYPE; 
                ln_replenish_change NUMBER;
                ln_promo_change NUMBER;
                ln_extra NUMBER;
            BEGIN
                SELECT P.APPLY_TO_CONSUMER_ACCT_ID
                  INTO ln_consumer_acct_id
                  FROM REPORT.PURCHASE P
                 WHERE P.TRAN_ID = l_trans_id
                   AND P.TRAN_LINE_ITEM_TYPE_ID = 550;
                SELECT CA.CONSUMER_ACCT_TYPE_ID, CA.CONSUMER_ACCT_BALANCE, CA.CONSUMER_ACCT_REPLEN_BALANCE, 
                       CA.CONSUMER_ACCT_PROMO_BALANCE, CA.CONSUMER_ACCT_SUB_TYPE_ID, CA.CORP_CUSTOMER_ID, CA.CONSUMER_ACCT_CD, CA.CONSUMER_ACCT_IDENTIFIER
                  INTO ln_consumer_acct_type_id, ln_consumer_acct_balance, ln_replenish_balance, ln_promo_balance, 
                       ln_consumer_acct_sub_type_id, ln_corp_customer_id, lv_consumer_acct_cd, lv_consumer_acct_identifier
                  FROM PSS.CONSUMER_ACCT CA
                 WHERE CA.CONSUMER_ACCT_ID = ln_consumer_acct_id
                   FOR UPDATE;
                SELECT COALESCE(SUM(A.AUTH_AMT_APPROVED), 0)
                  INTO ln_auth_hold_amt
                  FROM PSS.CONSUMER_ACCT_AUTH_HOLD CAAH
                  JOIN PSS.AUTH A ON CAAH.AUTH_ID = A.AUTH_ID
                 WHERE CAAH.CONSUMER_ACCT_ID = ln_consumer_acct_id
                   AND CAAH.EXPIRATION_TS > SYSDATE
                   AND CAAH.CLEARED_YN_FLAG = 'N';    
                IF ln_source_tran_id IS NULL THEN
                    SELECT TRAN_ID
                      INTO ln_source_tran_id
                      FROM PSS.TRAN
                     WHERE TRAN_GLOBAL_TRANS_CD = lv_orig_machine_trans_no;
                END IF;
                SELECT COALESCE(SUM(xi.TRAN_LINE_ITEM_QUANTITY * xi.TRAN_LINE_ITEM_AMOUNT) * l_amount / l_trans_total, 0)
                  INTO ln_replenish_bonus
                  FROM PSS.TRAN X
                  JOIN PSS.TRAN_LINE_ITEM XI ON X.TRAN_ID = XI.TRAN_ID
                 WHERE X.PARENT_TRAN_ID = ln_source_tran_id
                   AND XI.TRAN_LINE_ITEM_TYPE_ID = 555;
                IF ln_consumer_acct_balance - ln_auth_hold_amt < l_amount + ln_replenish_bonus THEN
                    RAISE_APPLICATION_ERROR(-20803, 'Prepaid account ' || TO_CHAR(ln_consumer_acct_id) || ' only has an available balance of $' 
                        ||TO_CHAR(ln_consumer_acct_balance - ln_auth_hold_amt - ln_replenish_bonus, 'FM9,999,990.00') || ' (including an auth hold of $'
                        ||TO_CHAR(ln_auth_hold_amt, 'FM9,999,990.00') || ' and a replenish bonus of $' || TO_CHAR(ln_replenish_bonus, 'FM9,999,990.00') 
                        || '). The refund amount must be equal to or less than this.');              
                END IF;
                l_update := 'Y';
                l_refund_amount:=l_amount;
                l_remaining_amount:=0;  
                l_return_val:=0;
                
                IF ln_promo_balance < ln_replenish_bonus THEN
                    ln_promo_change := ln_promo_balance;
                    ln_extra := ln_replenish_bonus - ln_promo_balance;
                ELSE
                    ln_promo_change := ln_replenish_bonus;
                    ln_extra := 0;
                END IF;
                ln_replenish_change := l_amount + ln_extra - ln_consumer_acct_balance + ln_promo_balance + ln_replenish_balance;
                IF ln_replenish_change < 0 THEN
                    ln_replenish_change := 0;
                END IF;
                UPDATE PSS.CONSUMER_ACCT
                   SET REPLENISH_BONUS_TOTAL = NVL(REPLENISH_BONUS_TOTAL, 0) - ln_replenish_bonus,
                       CONSUMER_ACCT_BALANCE = CONSUMER_ACCT_BALANCE - ln_replenish_bonus - l_amount,
                       CONSUMER_ACCT_REPLEN_BALANCE = CONSUMER_ACCT_REPLEN_BALANCE - ln_replenish_change,
                       CONSUMER_ACCT_REPLENISH_TOTAL = CONSUMER_ACCT_REPLENISH_TOTAL - l_amount,
                       CONSUMER_ACCT_PROMO_BALANCE =CONSUMER_ACCT_PROMO_BALANCE - ln_promo_change,
                       CONSUMER_ACCT_PROMO_TOTAL = CONSUMER_ACCT_PROMO_TOTAL - ln_replenish_bonus
                 WHERE CONSUMER_ACCT_ID = ln_consumer_acct_id;
                IF ln_replenish_bonus > 0 AND ln_consumer_acct_sub_type_id = 1 AND ln_corp_customer_id IS NOT NULL AND ln_corp_customer_id != 0 THEN
                    SELECT NVL(MAX(USER_NAME), 'Refund Replenishment Processing')
                      INTO lv_user_name
                      FROM REPORT.USER_LOGIN
                     WHERE USER_ID = l_user_id;
                    CORP.PAYMENTS_PKG.ADJUSTMENT_INS(ln_corp_customer_id, lv_user_name, lv_currency_cd, 
                        'Refund of Replenish Bonus, card # ' || lv_consumer_acct_cd || ', card ID ' || lv_consumer_acct_identifier,
                        ln_replenish_bonus, ln_doc_id, ln_ledger_id);
                END IF;               
           END;
        ELSIF ((ABS(l_prev_total) + ABS(l_amount)) <= l_trans_total) THEN
            l_update:='Y';
            l_refund_amount:=l_amount;
            l_remaining_amount:=0;  
            l_return_val:=0;
        ELSIF l_has_more = 'Y' THEN
            IF l_trans_total-ABS(l_prev_total) > 0 THEN
                l_update:='Y';
                l_refund_amount:=l_trans_total-ABS(l_prev_total);
                l_remaining_amount:=l_amount-l_refund_amount;
                l_return_val:=2;--'This transaction is refunded the diff'
            ELSE
                l_remaining_amount:=l_amount;
                l_return_val:=3; --'This transaction is already refunded'
            END IF;
        ELSIF l_override = 'Y' THEN 
            l_update:='Y';
            l_refund_amount:=l_amount;
            l_remaining_amount:=0;
            l_return_val := 1; --'Override Success'
        ELSE 
            l_remaining_amount:=l_amount;
            l_return_val := -2; --'Override Failure'
        END IF;

        IF l_update = 'Y' THEN
            DECLARE
                l_date DATE := SYSDATE;
            BEGIN
            INSERT INTO REPORT.TRANS(tran_id, card_number, total_amount, cc_appr_code,
                start_date, close_date, server_date, settle_state_id, eport_id,
                trans_type_id, orig_tran_id, terminal_id, merchant_id,
                source_system_cd, machine_trans_no, customer_bank_id,
                process_fee_id, create_date, description, currency_id, consumer_acct_id)
                (SELECT
                    l_refund_trans_id, orig.card_number, -ABS(l_refund_amount), 'PENDING',
                    l_date, l_date, l_date, 1, orig.eport_id, 20, l_trans_id,
                    orig.terminal_id, orig.merchant_id, 'RA', l_machine_trans_no,
                    orig.customer_bank_id, orig.process_fee_id, l_date,
                    l_refund_desc, orig.currency_id, orig.consumer_acct_id
                FROM REPORT.TRANS orig WHERE orig.tran_id = l_trans_id);
            select CASE WHEN USER_TYPE = 8 THEN 'W' ELSE 'N' END into lc_processed_flag FROM REPORT.USER_LOGIN WHERE USER_ID=l_user_id;
            INSERT INTO CORP.REFUND(refund_id, creator_user_id, tran_id, reason_id,
                comment_text, processed_flag, manager_override_flag, close_date) VALUES
                (l_refund_id, l_user_id, l_refund_trans_id, l_reason_id, l_comment,
                lc_processed_flag, l_override, l_date);
            END;
      END IF;
   END;
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/DW_PKG.pbk?rev=1.48
CREATE OR REPLACE PACKAGE BODY REPORT.DW_PKG IS
    FUNCTION GET_OR_CREATE_EXPORT_BATCH(
        l_customer_id CORP.CUSTOMER.CUSTOMER_ID%TYPE)
     RETURN ACTIVITY_REF.BATCH_ID%TYPE
    IS
    PRAGMA AUTONOMOUS_TRANSACTION;
        l_export_id ACTIVITY_REF.BATCH_ID%TYPE;
        l_lock VARCHAR2(128);
    BEGIN
        l_lock := GLOBALS_PKG.REQUEST_LOCK('EXPORT.CUSTOMER_ID',l_customer_id);
        SELECT eb.batch_id
          INTO l_export_id
          FROM REPORT.EXPORT_BATCH eb
         WHERE eb.CUSTOMER_ID = l_customer_id
           AND eb.EXPORT_TYPE = 1
           AND eb.STATUS = 'O';
        COMMIT;
        RETURN l_export_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            SELECT REPORT.EXPORT_BATCH_SEQ.NEXTVAL
              INTO l_export_id
              FROM DUAL;
            
            INSERT INTO REPORT.EXPORT_BATCH(
                   BATCH_ID,
                   CUSTOMER_ID,
                   EXPORT_TYPE,
                   STATUS)
               SELECT
                   l_export_id,
                   l_customer_id,
                   1,
                   'O'
                 FROM DUAL
                WHERE NOT EXISTS(
                    SELECT 1
                      FROM REPORT.EXPORT_BATCH eb
                     WHERE eb.CUSTOMER_ID = l_customer_id
                       AND eb.EXPORT_TYPE = 1
                       AND eb.STATUS = 'O');
            COMMIT;
            IF SQL%FOUND THEN
                RETURN l_export_id;
            ELSE
                RETURN GET_OR_CREATE_EXPORT_BATCH(l_customer_id);
            END IF;
        WHEN OTHERS THEN
            RAISE;
    END;
    
    PROCEDURE CLOSE_EXPORT_BATCH(
        l_export_id REPORT.EXPORT_BATCH.BATCH_ID%TYPE,
        l_customer_id REPORT.EXPORT_BATCH.CUSTOMER_ID%TYPE)
    IS
        l_lock VARCHAR2(128);
    BEGIN
        l_lock := GLOBALS_PKG.REQUEST_LOCK('EXPORT.CUSTOMER_ID',l_customer_id);
        UPDATE REPORT.EXPORT_BATCH EB SET (
                STATUS,
                TRAN_CREATE_DT_BEG,
                TRAN_CREATE_DT_END,
                TOT_TRAN_ROWS,
                TOT_TRAN_AMOUNT
              ) = (SELECT
                'A',
                MIN(A.TRAN_DATE),
                MAX(A.TRAN_DATE),
                COUNT(*),
                SUM(A.TOTAL_AMOUNT)
              FROM REPORT.ACTIVITY_REF A
             WHERE A.BATCH_ID = eb.BATCH_ID)
         WHERE EB.BATCH_ID = l_export_id
           AND EB.STATUS = 'O';
         CORP.PAYMENTS_PKG.UPDATE_EXPORT_BATCH(l_export_id);
    END;
    
    PROCEDURE CLOSE_EXPORT_BATCHES
    IS
        CURSOR l_cur IS
             SELECT EB.BATCH_ID, EB.CUSTOMER_ID
              FROM REPORT.EXPORT_BATCH EB
              JOIN (SELECT CUSTOMER_ID, 
              CASE WHEN BATCH_CLOSE_DATE > SYSDATE THEN BATCH_CLOSE_DATE - 1 ELSE BATCH_CLOSE_DATE END BATCH_CLOSE_DATE 
              FROM (SELECT CUSTOMER_ID, TO_DATE(TO_CHAR(SYSDATE, 'MM/DD/YYYY')||' '||NVL(BATCH_CLOSE_TIME,'05:00'), 'MM/DD/YYYY HH24:MI') BATCH_CLOSE_DATE 
              FROM CORP.CUSTOMER)) C ON EB.CUSTOMER_ID = C.CUSTOMER_ID
            WHERE EB.STATUS = 'O'
              AND EB.CRD_DATE < C.BATCH_CLOSE_DATE
              AND SYSDATE > C.BATCH_CLOSE_DATE;
    BEGIN
        FOR l_rec IN l_cur LOOP
            CLOSE_EXPORT_BATCH(l_rec.BATCH_ID, l_rec.CUSTOMER_ID);
            COMMIT;
        END LOOP;
    END;
    
    PROCEDURE CLOSE_EXPORT_BATCH_FOR(
        l_customer_id CORP.CUSTOMER.CUSTOMER_ID%TYPE)
    IS
        CURSOR l_cur IS
            SELECT BATCH_ID
              FROM REPORT.EXPORT_BATCH
            WHERE STATUS = 'O'
              AND CUSTOMER_ID = l_customer_id;
    BEGIN
        FOR l_rec IN l_cur LOOP
            CLOSE_EXPORT_BATCH(l_rec.BATCH_ID, l_customer_id);
            COMMIT;
        END LOOP;
    END;
    
    PROCEDURE UPDATE_SETTLEMENT(
        l_trans_id TRANS.TRAN_ID%TYPE,
        l_settle_state_id TRANS.SETTLE_STATE_ID%TYPE,
        l_settle_date TRANS.SETTLE_DATE%TYPE,
        l_appr_cd TRANS.CC_APPR_CODE%TYPE)
    IS
    BEGIN
        UPDATE ACTIVITY_REF SET CC_APPR_CODE = l_appr_cd,
            SETTLE_STATE = (SELECT STATE_LABEL FROM TRANS_STATE S
                WHERE l_settle_state_id = S.STATE_ID)
         WHERE TRAN_ID = l_trans_id;
    END;

PROCEDURE UPDATE_TRAN_INFO(
        l_trans_id TRANS.TRAN_ID%TYPE)
    IS
        l_ref_nbr ACTIVITY_REF.REF_NBR%TYPE;
        l_export_id ACTIVITY_REF.BATCH_ID%TYPE;
        l_create_dt ACTIVITY_REF.CREATE_DT%TYPE;
        l_update_dt ACTIVITY_REF.UPDATE_DT%TYPE;
        l_qty ACTIVITY_REF.QUANTITY%TYPE;
        l_terminal_id ACTIVITY_REF.TERMINAL_ID%TYPE;
        l_eport_id ACTIVITY_REF.EPORT_ID%TYPE;
        l_tran_date ACTIVITY_REF.TRAN_DATE%TYPE;
        l_trans_type_id ACTIVITY_REF.TRANS_TYPE_ID%TYPE;
        l_total_amount ACTIVITY_REF.TOTAL_AMOUNT%TYPE;
        l_currency_id ACTIVITY_REF.CURRENCY_ID%TYPE;
        l_customer_id CORP.CUSTOMER.CUSTOMER_ID%TYPE;
        l_convenience_fee ACTIVITY_REF.CONVENIENCE_FEE%TYPE;
        l_loyalty_discount ACTIVITY_REF.LOYALTY_DISCOUNT%TYPE;
        l_consumer_acct_id ACTIVITY_REF.CONSUMER_ACCT_ID%TYPE;
		lv_payment_entry_method_desc PSS.PAYMENT_ENTRY_METHOD.PAYMENT_ENTRY_METHOD_DESC%TYPE;
		lv_payment_subtype_class PSS.TRAN.PAYMENT_SUBTYPE_CLASS%TYPE;
		lv_card_company REPORT.ACTIVITY_REF.CARD_COMPANY%TYPE;
    l_payment_batch_id CORP.BATCH.BATCH_ID%TYPE;
        ln_cnt PLS_INTEGER;
        l_donate_count NUMBER;
        l_donate_yes NUMBER;
        lc_non_payable_entry_type CHAR(2);
        lc_conv_fee_entry_type CHAR(2);
        CURSOR l_cur IS
         SELECT
            T.CUSTOMER_BANK_ID,
            R.REGION_ID,
            R.REGION_NAME,
            L.LOCATION_NAME,
            L.LOCATION_ID,
            T.TERMINAL_ID,
            TERM.TERMINAL_NAME,
            TERM.TERMINAL_NBR,
            T.TRANS_TYPE_ID,
            TT.TRANS_TYPE_NAME,
            S.STATE_LABEL,
            T.CARD_NUMBER,
            T.CC_APPR_CODE,
            T.CLOSE_DATE,
            T.TOTAL_AMOUNT,
            T.ORIG_TRAN_ID,
            T.EPORT_ID,
            T.DESCRIPTION,
            T.CURRENCY_ID,
            T.CARDTYPE_AUTHORITY_ID,
            TERM.CUSTOMER_ID,
            SUM(CASE WHEN P.TRAN_LINE_ITEM_TYPE_ID = 203 OR (TRAN_LINE_ITEM_TYPE_ID IS NULL AND P.VEND_COLUMN IN ('Two-Tier Pricing', 'Convenience Fee')) THEN P.AMOUNT * NVL(P.PRICE,0) END) CONVENIENCE_FEE,
            SUM(CASE WHEN P.TRAN_LINE_ITEM_TYPE_ID = 204 THEN P.AMOUNT * NVL(P.PRICE,0) END) LOYALTY_DISCOUNT,
            T.CONSUMER_ACCT_ID,
            TT.CREDIT_IND,
            TT.CASH_IND,
            TT.PAYABLE_IND,
            CASE WHEN TT.CASH_IND = 'Y' THEN 'CA' WHEN TT.PAYABLE_IND != 'Y' THEN 'NP' END NON_PAYABLE_ENTRY_TYPE,
            CASE WHEN TT.PAYABLE_IND = 'Y' THEN 'TT' END CONV_FEE_ENTRY_TYPE,
            MAX(CASE WHEN P.TRAN_LINE_ITEM_TYPE_ID = 555 THEN P.APPLY_TO_CONSUMER_ACCT_ID END) APPLY_TO_CONSUMER_ACCT_ID
          FROM REPORT.TRANS T join REPORT.TRANS_TYPE TT on T.TRANS_TYPE_ID = TT.TRANS_TYPE_ID
          JOIN REPORT.TRANS_STATE S on T.SETTLE_STATE_ID = S.STATE_ID
          LEFT OUTER JOIN REPORT.PURCHASE P on T.TRAN_ID = P.TRAN_ID
          LEFT OUTER JOIN REPORT.TERMINAL TERM on T.TERMINAL_ID = TERM.TERMINAL_ID
          LEFT OUTER JOIN REPORT.LOCATION L on TERM.LOCATION_ID = L.LOCATION_ID
          LEFT OUTER JOIN REPORT.TERMINAL_REGION TR on TERM.TERMINAL_ID = TR.TERMINAL_ID
          LEFT OUTER JOIN REPORT.REGION R on TR.REGION_ID = R.REGION_ID
          WHERE T.TRAN_ID = l_trans_id
          GROUP BY
            T.CUSTOMER_BANK_ID,
            R.REGION_ID,
            R.REGION_NAME,
            L.LOCATION_NAME,
            L.LOCATION_ID,
            T.TERMINAL_ID,
            TERM.TERMINAL_NAME,
            TERM.TERMINAL_NBR,
            T.TRANS_TYPE_ID,
            TT.TRANS_TYPE_NAME,
            S.STATE_LABEL,
            T.CARD_NUMBER,
            T.CC_APPR_CODE,
            T.CLOSE_DATE,
            T.TOTAL_AMOUNT,
            T.ORIG_TRAN_ID,
            T.EPORT_ID,
            T.DESCRIPTION,
            T.CURRENCY_ID,
            T.CARDTYPE_AUTHORITY_ID,
            TERM.CUSTOMER_ID,
            T.CONSUMER_ACCT_ID,
            TT.CREDIT_IND,
            TT.CASH_IND,
            TT.PAYABLE_IND;
    BEGIN
        DELETE FROM REPORT.ACTIVITY_REF
         WHERE TRAN_ID = L_TRANS_ID
         RETURNING REF_NBR, BATCH_ID, CREATE_DT, SYSDATE, TERMINAL_ID, EPORT_ID, TRAN_DATE, TRANS_TYPE_ID, TOTAL_AMOUNT, QUANTITY, CURRENCY_ID, CONVENIENCE_FEE, LOYALTY_DISCOUNT, CONSUMER_ACCT_ID, PAYMENT_BATCH_ID
              INTO l_ref_nbr, l_export_id, l_create_dt, l_update_dt, l_terminal_id, l_eport_id, l_tran_date, l_trans_type_id, l_total_amount, l_qty, l_currency_id, l_convenience_fee, l_loyalty_discount,l_consumer_acct_id, l_payment_batch_id;       
        
        -- if exists, then update the trans_stat_by_day
        IF NVL(l_qty, 0) != 0 AND l_create_dt is NOT NULL THEN
            UPDATE REPORT.TRANS_STAT_BY_DAY 
               SET TRAN_COUNT = NVL(tran_count, 0) - 1,
                   VEND_COUNT = NVL(VEND_COUNT, 0) - l_qty,
                   TRAN_AMOUNT = NVL(TRAN_AMOUNT, 0) - NVL(l_total_amount, 0),
                   CONVENIENCE_FEE = NVL(CONVENIENCE_FEE, 0) - NVL(l_convenience_fee, 0),
                   LOYALTY_DISCOUNT = NVL(LOYALTY_DISCOUNT, 0) - NVL(l_loyalty_discount, 0)
             WHERE TERMINAL_ID = l_terminal_id
               AND EPORT_ID = l_eport_id
               AND TRAN_DATE = trunc(l_tran_date, 'DD')
               AND TRANS_TYPE_ID = l_trans_type_id
               AND CURRENCY_ID = l_currency_id;
            
            IF l_payment_batch_id IS NOT NULL THEN         
                SELECT CASE WHEN TT.CASH_IND = 'Y' THEN 'CA' WHEN TT.PAYABLE_IND != 'Y' THEN 'NP' END,
                       CASE WHEN TT.PAYABLE_IND = 'Y' THEN 'TT' END                  
                  INTO lc_non_payable_entry_type, lc_conv_fee_entry_type
                  FROM REPORT.TRANS_TYPE TT
                 WHERE TT.TRANS_TYPE_ID = l_trans_type_id;
                 
                IF lc_non_payable_entry_type IS NOT NULL THEN               
                    UPDATE CORP.BATCH_TOTAL
                       SET LEDGER_AMOUNT = LEDGER_AMOUNT - l_total_amount,
                           LEDGER_COUNT = LEDGER_COUNT - 1
                     WHERE BATCH_ID = l_payment_batch_id
                       AND ENTRY_TYPE = lc_non_payable_entry_type;
                END IF;  
                IF lc_conv_fee_entry_type IS NOT NULL AND l_convenience_fee > 0 THEN               
                    UPDATE CORP.BATCH_TOTAL
                       SET LEDGER_AMOUNT = LEDGER_AMOUNT - l_convenience_fee,
                           LEDGER_COUNT = LEDGER_COUNT - 1
                     WHERE BATCH_ID = l_payment_batch_id
                       AND ENTRY_TYPE = lc_conv_fee_entry_type;
                END IF;
            END IF;
        END IF;
        COMMIT;
        FOR l_rec IN l_cur LOOP
            IF NVL(l_export_id, 0) = 0 THEN
                IF l_rec.customer_id IS NULL THEN
                    l_export_id := 0;
                ELSE
                    l_export_id := GET_OR_CREATE_EXPORT_BATCH(l_rec.customer_id);
                END IF;
            END IF;
            IF l_ref_nbr IS NULL THEN
                SELECT REF_NBR_SEQ.NEXTVAL
                  INTO l_ref_nbr 
                  FROM DUAL;
            END IF;
            SELECT NVL(SUM(P.AMOUNT), 0)
              INTO l_qty
              FROM REPORT.PURCHASE P
              LEFT OUTER JOIN PSS.TRAN_LINE_ITEM_TYPE IT ON P.TRAN_LINE_ITEM_TYPE_ID = IT.TRAN_LINE_ITEM_TYPE_ID
             WHERE P.TRAN_ID = l_trans_id
              AND (IT.TRAN_LINE_ITEM_TYPE_GROUP_CD IN('S', 'P')
               OR (P.TRAN_LINE_ITEM_TYPE_ID IS NULL AND P.VEND_COLUMN NOT IN ('Two-Tier Pricing', 'Convenience Fee')));
			   
			SELECT MAX(PEM.PAYMENT_ENTRY_METHOD_DESC), MAX(X.PAYMENT_SUBTYPE_CLASS)
			INTO lv_payment_entry_method_desc, lv_payment_subtype_class
			FROM REPORT.TRANS T
			JOIN PSS.TRAN X ON T.MACHINE_TRANS_NO = X.TRAN_GLOBAL_TRANS_CD
			JOIN PSS.CLIENT_PAYMENT_TYPE CPT ON X.CLIENT_PAYMENT_TYPE_CD = CPT.CLIENT_PAYMENT_TYPE_CD
			LEFT OUTER JOIN PSS.PAYMENT_ENTRY_METHOD PEM ON CPT.PAYMENT_ENTRY_METHOD_CD = PEM.PAYMENT_ENTRY_METHOD_CD AND PEM.PAYMENT_ENTRY_METHOD_CD NOT IN ('M', 'S')
			WHERE T.TRAN_ID = l_trans_id;
			
			lv_card_company := REPORT.CARD_NAME(l_rec.CARDTYPE_AUTHORITY_ID, l_rec.TRANS_TYPE_ID, l_rec.CARD_NUMBER);
			IF lv_payment_subtype_class = 'Demo' THEN
				lv_card_company := lv_card_company || ' Demo';
			END IF;
			IF lv_payment_entry_method_desc IS NOT NULL THEN
				lv_card_company := lv_card_company || ' (' || lv_payment_entry_method_desc || ')';
			END IF;
        IF l_rec.customer_id is not null AND l_rec.customer_bank_id is not null AND (l_rec.NON_PAYABLE_ENTRY_TYPE IS NOT NULL  OR (l_rec.CONV_FEE_ENTRY_TYPE IS NOT NULL AND l_rec.convenience_fee > 0)) THEN
          l_payment_batch_id:=CORP.PAYMENTS_PKG.GET_OR_CREATE_BATCH(l_rec.TERMINAL_ID,l_rec.customer_bank_id,l_rec.CLOSE_DATE,l_rec.currency_id,'N');
        END IF;
            INSERT INTO ACTIVITY_REF(
                REGION_ID,
                REGION_NAME,
                LOCATION_NAME,
                LOCATION_ID,
                TERMINAL_ID,
                TERMINAL_NAME,
                TERMINAL_NBR,
                TRAN_ID,
                TRANS_TYPE_ID,
                TRANS_TYPE_NAME,
                SETTLE_STATE,
                CARD_NUMBER,
                CARD_COMPANY,
                CC_APPR_CODE,
                TRAN_DATE,
                FILL_DATE,
                TOTAL_AMOUNT,
                ORIG_TRAN_ID,
                BATCH_ID,
                VEND_COLUMN,
                QUANTITY,
                REF_NBR,
                EPORT_ID,
                DESCRIPTION,
                CREATE_DT,
                UPDATE_DT,
                CURRENCY_ID,
                CONVENIENCE_FEE,
                LOYALTY_DISCOUNT,
                CONSUMER_ACCT_ID,
                PAYMENT_BATCH_ID
                )
            SELECT
                l_rec.REGION_ID,
                l_rec.REGION_NAME,
                NVL(l_rec.LOCATION_NAME, 'Unknown'),
                NVL(l_rec.LOCATION_ID, 0),
                l_rec.TERMINAL_ID,
                l_rec.TERMINAL_NAME,
                l_rec.TERMINAL_NBR,
                l_trans_id,
                l_rec.TRANS_TYPE_ID,
                l_rec.TRANS_TYPE_NAME,
                l_rec.STATE_LABEL,
                l_rec.CARD_NUMBER,
                lv_card_company,
                l_rec.CC_APPR_CODE,
                l_rec.CLOSE_DATE TRAN_DATE,
                GET_FILL_DATE(l_trans_id) FILL_DATE,
                l_rec.TOTAL_AMOUNT,
                l_rec.ORIG_TRAN_ID,
                l_export_id,
                VEND_COLUMN_STRING(l_trans_id),
                l_qty,
                l_ref_nbr,
                l_rec.EPORT_ID,
                l_rec.DESCRIPTION,
                NVL(l_create_dt, SYSDATE),
                l_update_dt,
                l_rec.currency_id,
                l_rec.convenience_fee,
                l_rec.loyalty_discount,
                l_rec.consumer_acct_id,
                l_payment_batch_id
              FROM DUAL;
            IF l_rec.APPLY_TO_CONSUMER_ACCT_ID is not null THEN
              select count(1) into l_donate_yes from
              PSS.CONSUMER_ACCT CA 
              JOIN PSS.CONSUMER C ON C.CONSUMER_ID = CA.CONSUMER_ID
              LEFT OUTER JOIN (PSS.CONSUMER_SETTING CS
              JOIN PSS.CONSUMER_SETTING_PARAM CSP ON CSP.CONSUMER_SETTING_PARAM_ID = CS.CONSUMER_SETTING_PARAM_ID)
              ON CS.CONSUMER_ID = C.CONSUMER_ID AND CSP.CONSUMER_COMMUNICATION_TYPE ='DONATE_CHARITY'
              WHERE CA.CONSUMER_ACCT_ID=l_rec.APPLY_TO_CONSUMER_ACCT_ID
              AND NVL(CS.CONSUMER_SETTING_VALUE,'N') = 'Y';
              IF l_donate_yes !=0  THEN
                  INSERT INTO REPORT.PREPAID_DONATION (REPORT_TRAN_ID, CONSUMER_ACCT_ID, BASE_AMOUNT, DONATION_AMOUNT)
                  SELECT l_trans_id, l_rec.APPLY_TO_CONSUMER_ACCT_ID, l_rec.TOTAL_AMOUNT, ROUND(l_rec.TOTAL_AMOUNT * c.donation_percent, 2)
                  FROM PSS.TRAN_LINE_ITEM tli join report.campaign c on tli.campaign_id=c.campaign_id 
                  where tran_id=(select source_tran_id from report.trans where tran_id=l_trans_id)
                  and not exists (select 1 from REPORT.PREPAID_DONATION where report_tran_id=l_trans_id)
                  and c.donation_percent is not null;
              END IF;
            END IF;
            IF l_rec.TRANS_TYPE_ID in (20,31) THEN
              select count(1) into l_donate_count from report.prepaid_donation pd join report.trans t on pd.report_tran_id=t.tran_id
              and t.ORIG_TRAN_ID=(select tran_id from report.trans t2 join report.trans_type tt on t2.trans_type_id=tt.trans_type_id 
              where tran_id=l_rec.ORIG_TRAN_ID and tt.replenish_ind='Y') and t.trans_type_id=22;
              
              IF l_donate_count = 1 THEN
                  INSERT INTO REPORT.PREPAID_DONATION (REPORT_TRAN_ID, CONSUMER_ACCT_ID, BASE_AMOUNT, DONATION_AMOUNT)
                  select l_trans_id, pd.CONSUMER_ACCT_ID, 
                  case when orig.total_amount+rt.total_amount>=0 THEN round(pd.base_amount*(l_rec.TOTAL_AMOUNT/orig.total_amount),2)
                  ELSE (pd.base_amount+pdr.base_amount_sum)*-1 END,
                  case when orig.total_amount+rt.total_amount>=0 THEN round(pd.DONATION_AMOUNT*(l_rec.TOTAL_AMOUNT/orig.total_amount),2)
                  ELSE (pd.DONATION_AMOUNT+pdr.donation_amount_sum)*-1 END
                  from report.prepaid_donation pd,
                  (select sum(base_amount) base_amount_sum, sum(donation_amount) donation_amount_sum from report.prepaid_donation where report_tran_id in (select tran_id from report.trans t where t.ORIG_TRAN_ID=l_rec.ORIG_TRAN_ID and t.trans_type_id in (20,31))) pdr,
                  (select total_amount from report.trans where tran_id=l_rec.ORIG_TRAN_ID) orig,
                  (select sum(t.total_amount) total_amount from report.trans t where t.ORIG_TRAN_ID=l_rec.ORIG_TRAN_ID and t.trans_type_id in (20,31) ) rt
                  where pd.report_tran_id=(select tran_id from report.trans t where t.ORIG_TRAN_ID=l_rec.ORIG_TRAN_ID and t.trans_type_id =22)
                  and not exists (select 1 from REPORT.PREPAID_DONATION where report_tran_id=l_trans_id);
              END IF;
            END IF;
            -- update trans_stat_by_day
            IF l_qty != 0  OR l_rec.TOTAL_AMOUNT != 0 THEN
                LOOP
                    UPDATE REPORT.TRANS_STAT_BY_DAY
                       SET TRAN_COUNT = NVL(TRAN_COUNT, 0) + 1,
                           VEND_COUNT = NVL(VEND_COUNT, 0) + l_qty,
                           TRAN_AMOUNT = NVL(TRAN_AMOUNT, 0) + NVL(l_rec.TOTAL_AMOUNT, 0),
                           CONVENIENCE_FEE = NVL(CONVENIENCE_FEE, 0) + NVL(l_rec.CONVENIENCE_FEE, 0),
                           LOYALTY_DISCOUNT = NVL(LOYALTY_DISCOUNT, 0) + NVL(l_rec.loyalty_discount, 0)
                     WHERE TERMINAL_ID = l_rec.TERMINAL_ID
                       AND EPORT_ID = l_rec.EPORT_ID
                       AND TRAN_DATE = trunc(l_rec.CLOSE_DATE, 'DD')
                       AND TRANS_TYPE_ID = l_rec.TRANS_TYPE_ID
                       AND CURRENCY_ID = l_rec.CURRENCY_ID;
                    EXIT WHEN SQL%ROWCOUNT > 0;
                    BEGIN
                        INSERT INTO REPORT.TRANS_STAT_BY_DAY(
                            TERMINAL_ID,
                            EPORT_ID,
                            TRAN_DATE, 
                            TRANS_TYPE_ID,
                            TRAN_COUNT,
                            VEND_COUNT, 
                            TRAN_AMOUNT,
                            CURRENCY_ID, 
                            CONVENIENCE_FEE,
                            LOYALTY_DISCOUNT) 
                        VALUES( 
                            l_rec.TERMINAL_ID,
                            l_rec.EPORT_ID,
                            trunc(l_rec.CLOSE_DATE, 'DD'),
                            l_rec.TRANS_TYPE_ID, 
                            1,
                            l_qty,
                            NVL(l_rec.TOTAL_AMOUNT, 0),
                            l_rec.CURRENCY_ID,
                            l_rec.CONVENIENCE_FEE,
                            l_rec.loyalty_discount); 
                        EXIT;    
                    EXCEPTION
                        WHEN DUP_VAL_ON_INDEX THEN
                            NULL;
                    END;
                END LOOP;  
            END IF;
            COMMIT;
            
            -- update corp.batch_total
            IF l_payment_batch_id is not null AND (l_qty != 0  OR l_rec.TOTAL_AMOUNT != 0) THEN
                IF l_rec.NON_PAYABLE_ENTRY_TYPE IS NOT NULL THEN               
                    LOOP
                        UPDATE CORP.BATCH_TOTAL
                           SET LEDGER_AMOUNT = LEDGER_AMOUNT + l_rec.TOTAL_AMOUNT,
                               LEDGER_COUNT = LEDGER_COUNT + 1
                         WHERE BATCH_ID = l_payment_batch_id
                           AND ENTRY_TYPE = l_rec.NON_PAYABLE_ENTRY_TYPE;
                        EXIT WHEN SQL%ROWCOUNT > 0;
                        BEGIN
                            INSERT INTO CORP.BATCH_TOTAL(BATCH_ID, ENTRY_TYPE, PAYABLE, LEDGER_AMOUNT, LEDGER_COUNT)
                            VALUES(l_payment_batch_id,l_rec.NON_PAYABLE_ENTRY_TYPE,'N', l_rec.TOTAL_AMOUNT, 1); 
                            EXIT;    
                        EXCEPTION
                            WHEN DUP_VAL_ON_INDEX THEN
                                NULL;
                        END;
                    END LOOP;       
                END IF;  
                IF l_rec.CONV_FEE_ENTRY_TYPE IS NOT NULL AND l_rec.CONVENIENCE_FEE > 0 THEN               
                    LOOP
                        UPDATE CORP.BATCH_TOTAL
                           SET LEDGER_AMOUNT = LEDGER_AMOUNT + l_rec.CONVENIENCE_FEE,
                               LEDGER_COUNT = LEDGER_COUNT + 1
                         WHERE BATCH_ID = l_payment_batch_id
                           AND ENTRY_TYPE = l_rec.CONV_FEE_ENTRY_TYPE;
                        EXIT WHEN SQL%ROWCOUNT > 0;
                        BEGIN
                            INSERT INTO CORP.BATCH_TOTAL(BATCH_ID, ENTRY_TYPE, PAYABLE, LEDGER_AMOUNT, LEDGER_COUNT)
                            VALUES(l_payment_batch_id,l_rec.CONV_FEE_ENTRY_TYPE,'N', l_rec.CONVENIENCE_FEE, 1); 
                            EXIT;    
                        EXCEPTION
                            WHEN DUP_VAL_ON_INDEX THEN
                                NULL;
                        END;
                    END LOOP; 
                END IF;
            END IF;
            COMMIT;         
            IF l_qty != 0  OR l_rec.TOTAL_AMOUNT != 0 THEN
                --logic to update report.eport column for condition report
                IF l_rec.CREDIT_IND = 'Y' THEN
                  -- credit
                  UPDATE REPORT.EPORT SET
                   FIRST_TRAN_DATE=LEAST(NVL(FIRST_TRAN_DATE, max_date), l_rec.CLOSE_DATE),
                   FIRST_CREDIT_TRAN_DATE=LEAST(NVL(FIRST_CREDIT_TRAN_DATE, max_date), l_rec.CLOSE_DATE),
                   LAST_TRAN_DATE=GREATEST(NVL(LAST_TRAN_DATE, min_date), l_rec.CLOSE_DATE),
                   LAST_CREDIT_TRAN_DATE=GREATEST(NVL(LAST_CREDIT_TRAN_DATE, min_date), l_rec.CLOSE_DATE)
                  WHERE EPORT_ID=l_rec.EPORT_ID;
                ELSIF l_rec.CASH_IND = 'Y' THEN
                  -- cash
                  UPDATE REPORT.EPORT SET
                   FIRST_TRAN_DATE=LEAST(NVL(FIRST_TRAN_DATE, max_date), l_rec.CLOSE_DATE),
                   FIRST_CASH_TRAN_DATE=LEAST(NVL(FIRST_CASH_TRAN_DATE, max_date), l_rec.CLOSE_DATE),
                   LAST_TRAN_DATE=GREATEST(NVL(LAST_TRAN_DATE, min_date), l_rec.CLOSE_DATE),
                   LAST_CASH_TRAN_DATE=GREATEST(NVL(LAST_CASH_TRAN_DATE, min_date), l_rec.CLOSE_DATE)
                  WHERE EPORT_ID=l_rec.EPORT_ID;
                ELSE
                  UPDATE REPORT.EPORT SET
                   FIRST_TRAN_DATE=LEAST(NVL(FIRST_TRAN_DATE, max_date), l_rec.CLOSE_DATE),
                   LAST_TRAN_DATE=GREATEST(NVL(LAST_TRAN_DATE, min_date), l_rec.CLOSE_DATE)
                  WHERE EPORT_ID=l_rec.EPORT_ID;
                END IF;                 
            END IF;
        END LOOP;
    END;
 
    
    PROCEDURE UPDATE_TERMINAL(
        l_terminal_id TERMINAL.TERMINAL_ID%TYPE)
    IS
    BEGIN
        UPDATE ACTIVITY_REF A SET (
            region_id,
            region_name,
            location_name,
            location_id,
            terminal_name,
            terminal_nbr) = (
        SELECT
            R.REGION_ID,
            R.REGION_NAME,
            NVL(L.LOCATION_NAME, 'Unknown'),
            NVL(L.LOCATION_ID, 0),
            TERMINAL_NAME,
            TERM.TERMINAL_NBR
          FROM TERMINAL TERM,  REGION R, TERMINAL_REGION TR, LOCATION L
          WHERE TR.REGION_ID = R.REGION_ID (+)
            AND TERM.TERMINAL_ID = TR.TERMINAL_ID (+)
            AND TERM.LOCATION_ID = L.LOCATION_ID (+)
            AND TERM.TERMINAL_ID = A.TERMINAL_ID)
        WHERE A.TERMINAL_ID = l_terminal_id
          AND A.TRAN_DATE > SYSDATE - 90;
    END;

    PROCEDURE UPDATE_FILL_DATE(
        l_fill_id FILL.FILL_ID%TYPE)
    IS
        l_fill_date FILL.FILL_DATE%TYPE;
        l_prev_fill_date FILL.FILL_DATE%TYPE;
        l_eport_id FILL.EPORT_ID%TYPE;
    BEGIN
        SELECT FILL_DATE, EPORT_ID
          INTO l_fill_date, l_eport_id
          FROM FILL
         WHERE FILL_ID = l_fill_id;
         
         -- add update last fill date for condition report
         UPDATE REPORT.EPORT
         SET LAST_FILL_DATE=l_fill_date
         WHERE EPORT_ID=l_eport_id
         AND NVL(LAST_FILL_DATE, min_date) < l_fill_date;

          SELECT MAX(FILL_DATE)
          INTO l_prev_fill_date
          FROM FILL
         WHERE FILL_DATE < l_fill_date
           AND EPORT_ID = l_eport_id;
           
        -- Added to capture first fills (03-05-2007 BSK)
        IF l_prev_fill_date IS NULL THEN
            SELECT MAX(START_DATE)
              INTO l_prev_fill_date
              FROM REPORT.TERMINAL_EPORT
             WHERE START_DATE <= l_fill_date
               AND EPORT_ID = l_eport_id;
        END IF;
        
        -- Ignore first fill period (added June 8th for performance reasons - BSK)
        IF l_prev_fill_date IS NOT NULL THEN
            UPDATE ACTIVITY_REF SET FILL_DATE = GET_FILL_DATE(TRAN_ID)
             WHERE EPORT_ID = l_eport_id AND TRAN_DATE BETWEEN l_prev_fill_date AND l_fill_date;
          END IF;
    END;
    
    PROCEDURE UPDATE_FILL_INFO(
            l_fill_id   FILL.FILL_ID%TYPE) 
    IS
        l_export_batch_id FILL.BATCH_ID%TYPE;
        l_customer_id TERMINAL.CUSTOMER_ID%TYPE;
        l_terminal_id FILL.TERMINAL_ID%TYPE;
    
    BEGIN
        SELECT T.CUSTOMER_ID, T.TERMINAL_ID INTO l_customer_id, l_terminal_id
        FROM REPORT.TERMINAL T 
        JOIN REPORT.TERMINAL_EPORT TE ON T.TERMINAL_ID=TE.TERMINAL_ID
        JOIN REPORT.FILL F ON F.EPORT_ID=TE.EPORT_ID
        WHERE F.FILL_ID=l_fill_id
        AND F.FILL_DATE >= NVL(TE.START_DATE, MIN_DATE) AND F.FILL_DATE < NVL(TE.END_DATE, MAX_DATE);
        
        l_export_batch_id := GET_OR_CREATE_EXPORT_BATCH(l_customer_id);
        UPDATE REPORT.FILL SET BATCH_ID = l_export_batch_id, TERMINAL_ID = l_terminal_id,
        REF_NBR = NVL(REF_NBR, REF_NBR_SEQ.nextval)
        WHERE FILL_ID = l_fill_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN;  
    END;
    
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/ENGINE/PKG_DEVICE_SESSION.pbk?rev=1.57
CREATE OR REPLACE PACKAGE BODY ENGINE.PKG_DEVICE_SESSION IS    
    -- R33+ signature
	PROCEDURE UPDATE_SESSION_START(
      pv_device_name DEVICE.DEVICE_NAME%TYPE,
      pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
      pv_remote_address VARCHAR2,
      pd_device_call_start_date DATE,
	  pv_location_name OUT REPORT.LOCATION.LOCATION_NAME%TYPE,
      pv_customer_name OUT CORP.CUSTOMER.CUSTOMER_NAME%TYPE,
      pn_customer_id OUT CORP.CUSTOMER.CUSTOMER_ID%TYPE,
	  pn_terminal_id OUT REPORT.TERMINAL.TERMINAL_ID%TYPE,
	  pc_comm_method_cd OUT VARCHAR2
	)
	AS
        ln_device_id DEVICE.DEVICE_ID%TYPE;
        lv_device_name DEVICE.DEVICE_NAME%TYPE;
        lv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE;
    BEGIN
        IF pv_device_name IS NULL THEN
            RAISE_APPLICATION_ERROR(-20701, 'Invalid pv_device_name: ' || pv_device_name);
        END IF;
		
        BEGIN
			SELECT D.DEVICE_ID, D.DEVICE_NAME, D.DEVICE_SERIAL_CD, L.LOCATION_NAME, C.CUSTOMER_NAME, T.TERMINAL_ID, T.CUSTOMER_ID
              INTO ln_device_id, lv_device_name, lv_device_serial_cd, pv_location_name, pv_customer_name, pn_terminal_id, pn_customer_id
              FROM DEVICE.DEVICE D
              LEFT OUTER JOIN REPORT.EPORT E ON D.DEVICE_SERIAL_CD = E.EPORT_SERIAL_NUM
              LEFT OUTER JOIN REPORT.TERMINAL_EPORT TE ON E.EPORT_ID = TE.EPORT_ID 
               AND NVL(TE.START_DATE, MIN_DATE) <= pd_device_call_start_date              
               AND NVL(TE.END_DATE, MAX_DATE) > pd_device_call_start_date
              LEFT OUTER JOIN REPORT.TERMINAL T ON TE.TERMINAL_ID = T.TERMINAL_ID
              LEFT OUTER JOIN REPORT.LOCATION L ON T.LOCATION_ID = L.LOCATION_ID
              LEFT OUTER JOIN CORP.CUSTOMER C ON T.CUSTOMER_ID = C.CUSTOMER_ID
             WHERE D.DEVICE_ID = PKG_DEVICE_CONFIGURATION.GET_DEVICE_ID_BY_NAME(pv_device_name, pd_device_call_start_date);
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                IF pv_device_serial_cd IS NOT NULL THEN
                    BEGIN
                        -- Use serialCD
                        SELECT D.DEVICE_ID, D.DEVICE_NAME, D.DEVICE_SERIAL_CD, L.LOCATION_NAME, C.CUSTOMER_NAME, T.TERMINAL_ID, T.CUSTOMER_ID
                          INTO ln_device_id, lv_device_name, lv_device_serial_cd, pv_location_name, pv_customer_name, pn_terminal_id, pn_customer_id
                          FROM DEVICE.DEVICE D
                          LEFT OUTER JOIN REPORT.EPORT E ON D.DEVICE_SERIAL_CD = E.EPORT_SERIAL_NUM
                          LEFT OUTER JOIN REPORT.TERMINAL_EPORT TE ON E.EPORT_ID = TE.EPORT_ID 
                           AND NVL(TE.START_DATE, MIN_DATE) <= pd_device_call_start_date              
                           AND NVL(TE.END_DATE, MAX_DATE) > pd_device_call_start_date
                          LEFT OUTER JOIN REPORT.TERMINAL T ON TE.TERMINAL_ID = T.TERMINAL_ID
                          LEFT OUTER JOIN REPORT.LOCATION L ON T.LOCATION_ID = L.LOCATION_ID
                          LEFT OUTER JOIN CORP.CUSTOMER C ON T.CUSTOMER_ID = C.CUSTOMER_ID
                         WHERE D.DEVICE_ID = PKG_DEVICE_CONFIGURATION.GET_DEVICE_ID_BY_SERIAL(pv_device_serial_cd, pd_device_call_start_date);
                    EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                            lv_device_name := pv_device_name;
                    END;
                ELSE
                    lv_device_name := pv_device_name;
                END IF;
        END;
		
		-- use communication host to determine comm_method_cd first
		SELECT MAX(HT.COMM_METHOD_CD)
		INTO pc_comm_method_cd
		FROM DEVICE.HOST H
		JOIN DEVICE.HOST_TYPE HT ON H.HOST_TYPE_ID = HT.HOST_TYPE_ID
		WHERE H.DEVICE_ID = ln_device_id
			AND HT.COMM_METHOD_CD IS NOT NULL;
		
		IF pc_comm_method_cd IS NULL THEN
			-- fall back to ip address to determine comm_method_cd
			SELECT MAX(COMM_METHOD_CD)
			  INTO pc_comm_method_cd
			  FROM (SELECT COMM_METHOD_CD
					  FROM DEVICE.COMM_METHOD
					 WHERE REGEXP_LIKE(pv_remote_address, IP_MATCH_REGEX)
					 ORDER BY IP_MATCH_ORDER, LENGTH(IP_MATCH_REGEX) DESC, COMM_METHOD_CD)
			 WHERE ROWNUM = 1;
		END IF;
		
		-- make sure that device command pending count is accurate and update other device fields
		UPDATE DEVICE.DEVICE D
		   SET (CMD_PENDING_CNT, LAST_ACTIVITY_TS, COMM_METHOD_CD, COMM_METHOD_TS, LAST_CLIENT_IP_ADDRESS)
             = (SELECT COUNT(1),
                       GREATEST(pd_device_call_start_date, NVL(D.LAST_ACTIVITY_TS, MIN_DATE)),
                       DECODE(DBADMIN.PKG_UTL.COMPARE(pd_device_call_start_date, NVL(D.COMM_METHOD_TS, MIN_DATE)), 1, pc_comm_method_cd, D.COMM_METHOD_CD),
                       DECODE(DBADMIN.PKG_UTL.COMPARE(pd_device_call_start_date, NVL(D.COMM_METHOD_TS, MIN_DATE)), 1, pd_device_call_start_date, D.COMM_METHOD_TS),
                       CASE WHEN pv_remote_address IS NOT NULL AND (D.LAST_ACTIVITY_TS IS NULL OR D.LAST_ACTIVITY_TS < pd_device_call_start_date) THEN pv_remote_address ELSE D.LAST_CLIENT_IP_ADDRESS END
                  FROM ENGINE.MACHINE_CMD_PENDING MCP
                 WHERE MACHINE_ID = lv_device_name)
		 WHERE DEVICE_ID = ln_device_id;
        COMMIT;
    END;
	
	-- R35+ signature
    PROCEDURE UPDATE_SESSION_END(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
        pd_device_call_end_date DEVICE.LAST_ACTIVITY_TS%TYPE,
        pc_device_sent_config VARCHAR2,
		pv_remote_address VARCHAR2,
		pn_bytes_received DATA_TRANSFER.BYTES_RECEIVED%TYPE,
		pn_bytes_sent DATA_TRANSFER.BYTES_SENT%TYPE,
		pv_firmware_version OUT DEVICE.FIRMWARE_VERSION%TYPE)
    AS
        ln_device_id DEVICE.DEVICE_ID%TYPE;
		lc_comm_method_cd DEVICE.COMM_METHOD_CD%TYPE;
		ln_device_count DATA_TRANSFER.DEVICE_COUNT%TYPE;
		lc_upgrade_throttling_enabled COMM_METHOD.UPGRADE_THROTTLING_ENABLED%TYPE;
		ln_data_transfer_id DATA_TRANSFER.DATA_TRANSFER_ID%TYPE;
		ld_sysdate DATE := SYSDATE;
    BEGIN
		ln_device_id := PKG_DEVICE_CONFIGURATION.GET_DEVICE_ID_BY_NAME(pv_device_name, pd_device_call_end_date);
		IF ln_device_id IS NULL THEN
			ln_device_id := PKG_DEVICE_CONFIGURATION.GET_DEVICE_ID_BY_SERIAL(pv_device_serial_cd, pd_device_call_end_date);
			IF ln_device_id IS NULL THEN
				RETURN;
			END IF;
		END IF;		
		
		SELECT D.FIRMWARE_VERSION, D.COMM_METHOD_CD, CP.UPGRADE_THROTTLING_ENABLED
		  INTO pv_firmware_version, lc_comm_method_cd, lc_upgrade_throttling_enabled
		  FROM DEVICE.DEVICE D
		  LEFT OUTER JOIN DEVICE.COMM_METHOD CM ON D.COMM_METHOD_CD = CM.COMM_METHOD_CD
      LEFT OUTER JOIN DEVICE.COMM_PROVIDER CP ON CM.COMM_PROVIDER_ID = CP.COMM_PROVIDER_ID
		 WHERE D.DEVICE_ID = ln_device_id;
		
		IF lc_comm_method_cd IS NULL AND pv_remote_address IS NOT NULL THEN
			SELECT MAX(COMM_METHOD_CD)
			  INTO lc_comm_method_cd
			  FROM (SELECT COMM_METHOD_CD
					  FROM DEVICE.COMM_METHOD
					 WHERE REGEXP_LIKE(pv_remote_address, IP_MATCH_REGEX)
					 ORDER BY IP_MATCH_ORDER, LENGTH(IP_MATCH_REGEX) DESC, COMM_METHOD_CD)
			 WHERE ROWNUM = 1;
		END IF;
        
    UPDATE DEVICE.DEVICE
       SET LAST_ACTIVITY_TS = CASE WHEN pd_device_call_end_date > NVL(LAST_ACTIVITY_TS, MIN_DATE) THEN pd_device_call_end_date ELSE LAST_ACTIVITY_TS END,
           RECEIVED_CONFIG_FILE_TS = CASE WHEN pc_device_sent_config = 'Y' AND pd_device_call_end_date > NVL(RECEIVED_CONFIG_FILE_TS, MIN_DATE) THEN pd_device_call_end_date ELSE RECEIVED_CONFIG_FILE_TS END,
           COMM_METHOD_CD = CASE WHEN lc_comm_method_cd IS NOT NULL AND (LAST_ACTIVITY_TS IS NULL OR LAST_ACTIVITY_TS < pd_device_call_end_date) THEN lc_comm_method_cd ELSE COMM_METHOD_CD END,
           LAST_CLIENT_IP_ADDRESS = CASE WHEN pv_remote_address IS NOT NULL AND (LAST_ACTIVITY_TS IS NULL OR LAST_ACTIVITY_TS < pd_device_call_end_date) THEN pv_remote_address ELSE LAST_CLIENT_IP_ADDRESS END
    WHERE DEVICE_ID = ln_device_id;
		
    COMMIT;
		
		IF lc_upgrade_throttling_enabled = 'Y' AND (pn_bytes_received > 0 OR pn_bytes_sent > 0) THEN
			IF TO_NUMBER(TO_CHAR(ld_sysdate, 'HH24')) > 1 THEN
				SELECT NVL(MAX(DATA_TRANSFER_ID), 0), NVL(MAX(DEVICE_COUNT), 0)
				INTO ln_data_transfer_id, ln_device_count
				FROM DEVICE.DATA_TRANSFER
				WHERE DATA_TRANSFER_TS = TRUNC(ld_sysdate) - 1
					AND COMM_METHOD_CD = lc_comm_method_cd;

				IF ln_device_count < 1 AND ln_data_transfer_id > 0 THEN
					SELECT /*+INDEX(D IDX_DEVICE_LAST_ACTIVITY_TS)*/ COUNT(1)
					INTO ln_device_count
					FROM DEVICE.DEVICE D
					WHERE LAST_ACTIVITY_TS > TRUNC(ld_sysdate) - 1
						AND COMM_METHOD_CD = lc_comm_method_cd;
								
					UPDATE DEVICE.DATA_TRANSFER
					SET DEVICE_COUNT = ln_device_count
					WHERE DATA_TRANSFER_ID = ln_data_transfer_id;
					
					COMMIT;
				END IF;
			END IF;
		
			LOOP
				UPDATE DEVICE.DATA_TRANSFER
				SET BYTES_RECEIVED = BYTES_RECEIVED + pn_bytes_received,
					BYTES_SENT = BYTES_SENT + pn_bytes_sent
				WHERE DATA_TRANSFER_TS = TRUNC(pd_device_call_end_date)
					AND COMM_METHOD_CD = lc_comm_method_cd;
					
				IF SQL%FOUND THEN
					EXIT;
				ELSE
					BEGIN
						INSERT INTO DEVICE.DATA_TRANSFER(DATA_TRANSFER_TS, COMM_METHOD_CD, BYTES_RECEIVED, BYTES_SENT, DEVICE_COUNT)
						VALUES(TRUNC(pd_device_call_end_date), lc_comm_method_cd, pn_bytes_received, pn_bytes_sent, 0);
						EXIT;
					EXCEPTION
						WHEN DUP_VAL_ON_INDEX THEN
							NULL;
					END;
				END IF;
			END LOOP;
			
			COMMIT;
		END IF;
    END;
	
    FUNCTION GET_OR_CREATE_NET_LAYER(
        l_net_layer_desc ENGINE.NET_LAYER.NET_LAYER_DESC%TYPE,
        l_server_name ENGINE.NET_LAYER.SERVER_NAME%TYPE,
        l_server_port_num ENGINE.NET_LAYER.SERVER_PORT_NUM%TYPE)
      RETURN ENGINE.NET_LAYER.NET_LAYER_ID%TYPE
    AS        
	    i PLS_INTEGER := 1;
	    l_net_layer_id ENGINE.NET_LAYER.NET_LAYER_ID%TYPE;
	BEGIN
	    LOOP
	        BEGIN
	            SELECT NET_LAYER_ID
	              INTO l_net_layer_id
	              FROM ENGINE.NET_LAYER
	             WHERE NET_LAYER_DESC = l_net_layer_desc;
	            EXIT;
	        EXCEPTION
	            WHEN NO_DATA_FOUND THEN
	                BEGIN
	                    SELECT ENGINE.SEQ_NET_LAYER_ID.NEXTVAL
	                      INTO l_net_layer_id
	                      FROM DUAL;
	                    INSERT INTO ENGINE.NET_LAYER(
	                        NET_LAYER_ID, 
	                        NET_LAYER_DESC,
	                        SERVER_NAME,
	                        SERVER_PORT_NUM,
	                        NET_LAYER_ACTIVE_YN_FLAG)
	                      VALUES(
	                        l_net_layer_id,
	                        l_net_layer_desc,
	                        l_server_name,
	                        l_server_port_num,
	                        'Y');
	                    EXIT;
	                EXCEPTION
	                    WHEN DUP_VAL_ON_INDEX THEN
	                    	IF i > 9 THEN
	                    		RAISE;
	                    	END IF;
	                        i := i + 1;
	                END;
	        END;
	    END LOOP;
	    RETURN l_net_layer_id;
	END;
END;
/
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/CCS_PKG.psk?rev=1.14
CREATE OR REPLACE PACKAGE REPORT.CCS_PKG IS
--
-- Contains functions /procedures for managing user reports
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------       
-- B KRUG       4-4-05  NEW
-- B KRUG       3-8-07  Renamed package and made TRANSPORT_PROP_UPD generic
  PROCEDURE ALERT_NOTIF_INS(
    
        l_alert_notif_id  OUT REPORT.ALERT_NOTIF.alert_notif_id%TYPE,
        l_ccs_transport_id  OUT CCS_TRANSPORT.ccs_transport_id%TYPE, 
        l_user_id  IN ALERT_NOTIF.user_id%TYPE,
        l_allTerminalFlag IN ALERT_NOTIF.all_terminals_flag%TYPE,
        l_terminalid  ID_LIST,
        l_alerTypeId  ID_LIST);
 
  PROCEDURE ALERT_NOTIF_DEL(
        l_alert_notif_id IN ALERT_NOTIF.ALERT_NOTIF_ID%TYPE);
        
  PROCEDURE ALERT_NOTIF_UPD(
        l_alert_notif_id IN ALERT_NOTIF.ALERT_NOTIF_ID%TYPE,
        l_user_id  IN ALERT_NOTIF.user_id%TYPE,
        l_allTerminalFlag IN ALERT_NOTIF.all_terminals_flag%TYPE,
        l_terminal_id ID_LIST,
        l_alert_type_id ID_LIST,
        l_transport_id OUT CCS_TRANSPORT.CCS_TRANSPORT_ID%TYPE);
        
  PROCEDURE USER_REPORT_DEL(
        l_user_report_id IN USER_REPORT.USER_REPORT_ID%TYPE);
        
  PROCEDURE TRANSPORT_PROP_UPD(
        l_transport_id IN CCS_TRANSPORT_PROPERTY.CCS_TRANSPORT_ID%TYPE,
        l_tpt_id IN CCS_TRANSPORT_PROPERTY.CCS_TRANSPORT_PROPERTY_TYPE_ID%TYPE,
        l_value IN CCS_TRANSPORT_PROPERTY.CCS_TRANSPORT_PROPERTY_VALUE%TYPE);
  
  -- usalive 1.7.5      
  PROCEDURE USER_REPORT_INS(
        l_user_id IN USER_REPORT.USER_ID%TYPE,
        l_report_id IN USER_REPORT.REPORT_ID%TYPE,
        l_frequency_id IN USER_REPORT.FREQUENCY_ID%TYPE,
        l_transport_type_id IN CCS_TRANSPORT.CCS_TRANSPORT_TYPE_ID%TYPE,
        l_user_report_id OUT USER_REPORT.USER_REPORT_ID%TYPE,
        l_transport_id OUT CCS_TRANSPORT.CCS_TRANSPORT_ID%TYPE);
        
   -- usalive 1.8
   PROCEDURE USER_REPORT_INS(
        l_user_id IN USER_REPORT.USER_ID%TYPE,
        l_report_id IN USER_REPORT.REPORT_ID%TYPE,
        l_frequency_id IN USER_REPORT.FREQUENCY_ID%TYPE,
        l_transport_id IN CCS_TRANSPORT.CCS_TRANSPORT_ID%TYPE);
        
  -- usalive 1.7.5
  PROCEDURE USER_REPORT_UPD(
        l_user_report_id IN USER_REPORT.USER_REPORT_ID%TYPE,
        l_frequency_id IN USER_REPORT.FREQUENCY_ID%TYPE,
        l_transport_type_id IN CCS_TRANSPORT.CCS_TRANSPORT_TYPE_ID%TYPE,
        l_transport_id OUT CCS_TRANSPORT.CCS_TRANSPORT_ID%TYPE);
     
  -- usalive 1.8   
  PROCEDURE USER_REPORT_UPD(
        l_user_report_id IN USER_REPORT.USER_REPORT_ID%TYPE,
        l_frequency_id IN USER_REPORT.FREQUENCY_ID%TYPE,
        l_transport_id IN CCS_TRANSPORT.CCS_TRANSPORT_ID%TYPE);
 
  PROCEDURE REPORT_SENT_INS (
        l_user_report_id IN REPORT_SENT.USER_REPORT_ID%TYPE,
        l_report_request_id IN REPORT_SENT.REPORT_REQUEST_ID%TYPE,
        l_batch_id IN REPORT_SENT.BATCH_ID%TYPE,
        l_begin_date IN REPORT_SENT.BEGIN_DATE%TYPE,
        l_end_date IN REPORT_SENT.END_DATE%TYPE,
        l_rs_id OUT REPORT_SENT.REPORT_SENT_ID%TYPE,
        l_batch_type_id  IN REPORT_SENT.EXPORT_TYPE%TYPE  DEFAULT NULL);
  
  -- R45 and above
  PROCEDURE ADD_EVENT_ALERT(
        pn_event_id IN REPORT.TERMINAL_ALERT.REFERENCE_ID%TYPE,
        pn_terminal_alert_id OUT REPORT.TERMINAL_ALERT.TERMINAL_ALERT_ID%TYPE,
        pn_terminal_id OUT REPORT.TERMINAL.TERMINAL_ID%TYPE,
        pv_device_serial_cd OUT REPORT.EPORT.EPORT_SERIAL_NUM%TYPE,
        pc_dont_send_flag OUT VARCHAR2);
        
  -- R44 and below
  PROCEDURE ADD_EVENT_ALERT(
        pn_event_id IN REPORT.TERMINAL_ALERT.REFERENCE_ID%TYPE,
        pn_terminal_alert_id OUT REPORT.TERMINAL_ALERT.TERMINAL_ALERT_ID%TYPE,
        pn_terminal_id OUT REPORT.TERMINAL.TERMINAL_ID%TYPE,
        pv_device_serial_cd OUT REPORT.EPORT.EPORT_SERIAL_NUM%TYPE);	 
  
  -- pre usalive 1.9 signature
  PROCEDURE CCS_TRANSPORT_ERROR_INS(
        pn_report_sent_id IN REPORT_SENT.REPORT_SENT_ID%TYPE,
        pv_error_text IN CCS_TRANSPORT_ERROR.ERROR_TEXT%TYPE,
        pd_error_ts IN CCS_TRANSPORT_ERROR.ERROR_TS%TYPE,
        pn_error_id OUT CCS_TRANSPORT_ERROR.CCS_TRANSPORT_ERROR_ID%TYPE);
        
  PROCEDURE CCS_TRANSPORT_ERROR_INS(
        pn_report_sent_id IN REPORT_SENT.REPORT_SENT_ID%TYPE,
        pv_error_text IN CCS_TRANSPORT_ERROR.ERROR_TEXT%TYPE,
        pd_error_ts IN CCS_TRANSPORT_ERROR.ERROR_TS%TYPE,
        pd_error_type_id IN CCS_TRANSPORT_ERROR.ERROR_TYPE_ID%TYPE,
        pv_retry_props IN CCS_TRANSPORT_ERROR.RETRY_PROPS%TYPE,
        pn_error_id OUT CCS_TRANSPORT_ERROR.CCS_TRANSPORT_ERROR_ID%TYPE);
        
  PROCEDURE CCS_TRANSPORT_ERROR_RETRY(
        pn_ct_error_id IN CCS_TRANSPORT_ERROR.CCS_TRANSPORT_ERROR_ID%TYPE);
  
  PROCEDURE CCS_TRANSPORT_RETRY_SUCCESS(
        pn_ccs_transport_id IN CCS_TRANSPORT.CCS_TRANSPORT_ID%TYPE, 
        pn_ct_error_id IN CCS_TRANSPORT_ERROR.CCS_TRANSPORT_ERROR_ID%TYPE);
  
  PROCEDURE CCS_TRANSPORT_ERROR_UPDATE(
        pn_ccs_transport_id IN CCS_TRANSPORT.CCS_TRANSPORT_ID%TYPE, 
        pn_ct_error_id IN CCS_TRANSPORT_ERROR.CCS_TRANSPORT_ERROR_ID%TYPE,
        pv_error_text IN CCS_TRANSPORT_ERROR.ERROR_TEXT%TYPE,
        pd_error_type_id IN CCS_TRANSPORT_ERROR.ERROR_TYPE_ID%TYPE,
        pv_retry_props IN CCS_TRANSPORT_ERROR.RETRY_PROPS%TYPE,
        pn_disabled_transport OUT NUMBER);
        
  PROCEDURE CCS_UPDATE_REATTEMPTED_FLAG(
        pn_ct_error_id IN CCS_TRANSPORT_ERROR.CCS_TRANSPORT_ERROR_ID%TYPE,
        pn_reattempted_flag IN CCS_TRANSPORT_ERROR.REATTEMPTED_FLAG%TYPE);
        
END; -- Package spec
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/CCS_PKG.pbk?rev=1.25
CREATE OR REPLACE PACKAGE BODY REPORT.CCS_PKG IS
   
    PROCEDURE ALERT_NOTIF_INS(
       l_alert_notif_id  OUT REPORT.ALERT_NOTIF.alert_notif_id%TYPE,
        l_ccs_transport_id  OUT CCS_TRANSPORT.ccs_transport_id%TYPE, 
        l_user_id  IN ALERT_NOTIF.user_id%TYPE,
        l_allTerminalFlag IN ALERT_NOTIF.all_terminals_flag%TYPE,      
        l_terminalId  ID_LIST,
        l_alerTypeId  ID_LIST)
    IS
    BEGIN
        SELECT CCS_TRANSPORT_SEQ.NEXTVAL,
               SEQ_ALERT_NOTIF_ID.NEXTVAL
          INTO l_ccs_transport_id,
               l_alert_notif_id
          FROM DUAL;
        
         INSERT INTO CCS_TRANSPORT(CCS_TRANSPORT_ID, CCS_TRANSPORT_TYPE_ID, CCS_TRANSPORT_NAME)
            VALUES(l_ccs_transport_id, 1,
                'Transport for Alert Notif Id = ' || TO_CHAR(l_alert_notif_id));
    	 INSERT INTO ALERT_NOTIF (ALERT_NOTIF_ID, USER_ID, ccs_transport_id,All_TERMINALS_FLAG)
  	 		VALUES(l_alert_notif_id, l_user_id, l_ccs_transport_id,l_allTerminalFlag);
        
        
         IF l_allTerminalFlag <> 'Y' THEN
          INSERT INTO ALERT_NOTIF_TERMINAL (
              ALERT_NOTIF_TERMINAL_ID,
              ALERT_NOTIF_ID,
              TERMINAL_ID)
            SELECT 
              SEQ_ALERT_NOTIF_TERMINAL_ID.nextval,
              l_alert_notif_id,
              TERMINAL_ID
            FROM REPORT.VW_USER_TERMINAL
          WHERE USER_ID = l_user_id
           AND TERMINAL_ID MEMBER OF l_terminalId; 
         END IF;
         
         INSERT INTO ALERT_NOTIF_ALERT_TYPE (
               ALERT_NOTIF_ALERT_TYPE_ID,
               ALERT_NOTIF_ID,
               ALERT_TYPE_ID)
            SELECT 
               SEQ_ALERT_NOTIF_ALERT_TYPE_ID.nextval,
               l_alert_notif_id,
               ALERT_TYPE_ID
            FROM REPORT.ALERT_TYPE
           WHERE ALERT_TYPE_ID MEMBER OF l_alerTypeId;
    END;
    
    

    PROCEDURE ALERT_NOTIF_DEL(
        l_alert_notif_id IN ALERT_NOTIF.ALERT_NOTIF_ID%TYPE)
    IS
    BEGIN
    	 DELETE FROM  ALERT_NOTIF_ALERT_TYPE WHERE ALERT_NOTIF_ID = l_alert_notif_id;
    	 DELETE FROM  ALERT_NOTIF_TERMINAL WHERE ALERT_NOTIF_ID = l_alert_notif_id;
    	 DELETE FROM  ALERT_NOTIF WHERE ALERT_NOTIF_ID = l_alert_notif_id;
    END;
    
   PROCEDURE ALERT_NOTIF_UPD(
        l_alert_notif_id IN ALERT_NOTIF.ALERT_NOTIF_ID%TYPE,
        l_user_id  IN ALERT_NOTIF.user_id%TYPE,
        l_allTerminalFlag IN ALERT_NOTIF.all_terminals_flag%TYPE,
        l_terminal_id ID_LIST,
        l_alert_type_id ID_LIST,
        l_transport_id OUT CCS_TRANSPORT.CCS_TRANSPORT_ID%TYPE)
    IS
    BEGIN
    	 SELECT CCS_TRANSPORT_ID
          INTO  l_transport_id
          FROM ALERT_NOTIF 
         WHERE ALERT_NOTIF_ID = l_alert_notif_id;
        
         UPDATE ALERT_NOTIF
          SET   CCS_TRANSPORT_ID=l_transport_id, ALL_TERMINALS_FLAG=l_allTerminalFlag
         WHERE  ALERT_NOTIF_ID=l_alert_notif_id;
         DELETE FROM  ALERT_NOTIF_TERMINAL WHERE ALERT_NOTIF_ID = l_alert_notif_id;       
         IF l_allTerminalFlag <> 'Y' THEN
          INSERT INTO ALERT_NOTIF_TERMINAL (
              ALERT_NOTIF_TERMINAL_ID,
              ALERT_NOTIF_ID,
              TERMINAL_ID)
            SELECT 
              SEQ_ALERT_NOTIF_TERMINAL_ID.nextval,
              l_alert_notif_id,
              TERMINAL_ID
            FROM REPORT.VW_USER_TERMINAL
          WHERE USER_ID = l_user_id
           AND TERMINAL_ID MEMBER OF l_terminal_id; 
         END IF;
         
         DELETE FROM  ALERT_NOTIF_ALERT_TYPE WHERE ALERT_NOTIF_ID = l_alert_notif_id;
         INSERT INTO ALERT_NOTIF_ALERT_TYPE (
               ALERT_NOTIF_ALERT_TYPE_ID,
               ALERT_NOTIF_ID,
               ALERT_TYPE_ID)
            SELECT 
               SEQ_ALERT_NOTIF_ALERT_TYPE_ID.nextval,
               l_alert_notif_id,
               ALERT_TYPE_ID
            FROM REPORT.ALERT_TYPE
           WHERE ALERT_TYPE_ID MEMBER OF l_alert_type_id;
        
    END;
    
    -- usalive 1.7.5
    PROCEDURE USER_REPORT_INS(
        l_user_id IN USER_REPORT.USER_ID%TYPE,
        l_report_id IN USER_REPORT.REPORT_ID%TYPE,
        l_frequency_id IN USER_REPORT.FREQUENCY_ID%TYPE,
        l_transport_type_id IN CCS_TRANSPORT.CCS_TRANSPORT_TYPE_ID%TYPE,
        l_user_report_id OUT USER_REPORT.USER_REPORT_ID%TYPE,
        l_transport_id OUT CCS_TRANSPORT.CCS_TRANSPORT_ID%TYPE)
    IS
        l_freq REPORT.USER_REPORT.FREQUENCY_ID%TYPE;
        l_batch_type_id REPORT.REPORTS.BATCH_TYPE_ID%TYPE;
        l_start_date REPORT.USER_REPORT.UPD_DT%TYPE := SYSDATE;
    BEGIN
        SELECT DECODE(R.BATCH_TYPE_ID, 0, l_frequency_id, NULL),
               REPORT.CCS_TRANSPORT_SEQ.NEXTVAL,
               REPORT.USER_REPORT_SEQ.NEXTVAL
          INTO l_freq,
               l_transport_id,
               l_user_report_id
          FROM REPORT.REPORTS R
         WHERE R.REPORT_ID = l_report_id;
        IF l_freq IS NOT NULL THEN
            SELECT ADD_MONTHS(SYSDATE, -NVL(F.MONTHS, 0)) - NVL(F.DAYS, 0) 
              INTO l_start_date
              FROM CORP.FREQUENCY F
             WHERE F.FREQUENCY_ID = l_freq;
        END IF;
        INSERT INTO REPORT.CCS_TRANSPORT(CCS_TRANSPORT_ID, CCS_TRANSPORT_TYPE_ID, CCS_TRANSPORT_NAME)
            VALUES(l_transport_id, l_transport_type_id,
                'Transport for User Report Id = ' || TO_CHAR(l_user_report_id));
    	INSERT INTO REPORT.USER_REPORT (USER_REPORT_ID, USER_ID, REPORT_ID, FREQUENCY_ID, CCS_TRANSPORT_ID, UPD_DT)
  	 		VALUES(l_user_report_id, l_user_id, l_report_id, l_freq, l_transport_id, l_start_date);
    EXCEPTION
    	WHEN NO_DATA_FOUND THEN
    	 	 RAISE_APPLICATION_ERROR(-20100, 'Could not find this report');
    	WHEN OTHERS THEN
    	 	 RAISE;
    END;
    
  --usalive 1.8 
  PROCEDURE USER_REPORT_INS(
        l_user_id IN USER_REPORT.USER_ID%TYPE,
        l_report_id IN USER_REPORT.REPORT_ID%TYPE,
        l_frequency_id IN USER_REPORT.FREQUENCY_ID%TYPE,
        l_transport_id IN CCS_TRANSPORT.CCS_TRANSPORT_ID%TYPE)
    IS
        l_freq REPORT.USER_REPORT.FREQUENCY_ID%TYPE;
        l_batch_type_id REPORT.REPORTS.BATCH_TYPE_ID%TYPE;
        l_start_date REPORT.USER_REPORT.UPD_DT%TYPE := SYSDATE;
        l_user_report_id USER_REPORT.USER_REPORT_ID%TYPE;
        l_user_transport_id CCS_TRANSPORT.CCS_TRANSPORT_ID%TYPE;
    BEGIN
        SELECT DECODE(R.BATCH_TYPE_ID, 0, l_frequency_id, NULL),
               REPORT.USER_REPORT_SEQ.NEXTVAL
          INTO l_freq,
               l_user_report_id
          FROM REPORT.REPORTS R
         WHERE R.REPORT_ID = l_report_id;
        IF l_freq IS NOT NULL THEN
            SELECT ADD_MONTHS(SYSDATE, -NVL(F.MONTHS, 0)) - NVL(F.DAYS, 0) 
              INTO l_start_date
              FROM CORP.FREQUENCY F
             WHERE F.FREQUENCY_ID = l_freq;
        END IF;
      select ccs_transport_id into l_user_transport_id from report.ccs_transport where ccs_transport_id=l_transport_id and user_id=l_user_id;
    	INSERT INTO REPORT.USER_REPORT (USER_REPORT_ID, USER_ID, REPORT_ID, FREQUENCY_ID, CCS_TRANSPORT_ID, UPD_DT)
  	 		VALUES(l_user_report_id, l_user_id, l_report_id, l_freq, l_user_transport_id, l_start_date);
    EXCEPTION
    	WHEN NO_DATA_FOUND THEN
    	 	 RAISE_APPLICATION_ERROR(-20100, 'Could not find this report');
    	WHEN OTHERS THEN
    	 	 RAISE;
    END;
    
    -- usalive 1.7.5
    PROCEDURE USER_REPORT_UPD(
        l_user_report_id IN USER_REPORT.USER_REPORT_ID%TYPE,
        l_frequency_id IN USER_REPORT.FREQUENCY_ID%TYPE,
        l_transport_type_id IN CCS_TRANSPORT.CCS_TRANSPORT_TYPE_ID%TYPE,
        l_transport_id OUT CCS_TRANSPORT.CCS_TRANSPORT_ID%TYPE)
    IS
      l_freq USER_REPORT.FREQUENCY_ID%TYPE;
    BEGIN
    	 SELECT DECODE(R.BATCH_TYPE_ID, 0, l_frequency_id, NULL), UR.CCS_TRANSPORT_ID
          INTO l_freq, l_transport_id
          FROM REPORTS R, USER_REPORT UR
         WHERE R.REPORT_ID = UR.REPORT_ID
           AND UR.USER_REPORT_ID = l_user_report_id;
         UPDATE USER_REPORT SET FREQUENCY_ID = l_freq
          WHERE USER_REPORT_ID = l_user_report_id;
         UPDATE CCS_TRANSPORT SET CCS_TRANSPORT_TYPE_ID = l_transport_type_id
          WHERE CCS_TRANSPORT_ID = l_transport_id;
    EXCEPTION
    	 WHEN NO_DATA_FOUND THEN
    	 	 RAISE_APPLICATION_ERROR(-20100, 'Could not find this report');
    	 WHEN OTHERS THEN
    	 	 RAISE;
    END;
    
    -- usalive 1.8
    PROCEDURE USER_REPORT_UPD(
        l_user_report_id IN USER_REPORT.USER_REPORT_ID%TYPE,
        l_frequency_id IN USER_REPORT.FREQUENCY_ID%TYPE,
        l_transport_id IN CCS_TRANSPORT.CCS_TRANSPORT_ID%TYPE)
    IS
      l_freq USER_REPORT.FREQUENCY_ID%TYPE;
    BEGIN
    	 SELECT DECODE(R.BATCH_TYPE_ID, 0, l_frequency_id, NULL)
          INTO l_freq
          FROM REPORTS R JOIN USER_REPORT UR ON R.REPORT_ID = UR.REPORT_ID
         WHERE UR.USER_REPORT_ID = l_user_report_id;
         UPDATE USER_REPORT SET FREQUENCY_ID = l_freq, 
          CCS_TRANSPORT_ID = l_transport_id 
          WHERE USER_REPORT_ID = l_user_report_id;

    EXCEPTION
    	 WHEN NO_DATA_FOUND THEN
    	 	 RAISE_APPLICATION_ERROR(-20100, 'Could not find this report');
    	 WHEN OTHERS THEN
    	 	 RAISE;
    END;
    
    PROCEDURE USER_REPORT_DEL(
        l_user_report_id IN USER_REPORT.USER_REPORT_ID%TYPE)
    IS
    BEGIN
    	 UPDATE USER_REPORT SET STATUS = 'D' WHERE USER_REPORT_ID = l_user_report_id;
    END;
    
    PROCEDURE TRANSPORT_PROP_UPD(
        l_transport_id IN CCS_TRANSPORT_PROPERTY.CCS_TRANSPORT_ID%TYPE,
        l_tpt_id IN CCS_TRANSPORT_PROPERTY.CCS_TRANSPORT_PROPERTY_TYPE_ID%TYPE,
        l_value IN CCS_TRANSPORT_PROPERTY.CCS_TRANSPORT_PROPERTY_VALUE%TYPE)
    IS
    BEGIN
    	UPDATE CCS_TRANSPORT_PROPERTY
           SET CCS_TRANSPORT_PROPERTY_VALUE = l_value
         WHERE CCS_TRANSPORT_PROPERTY_TYPE_ID = l_tpt_id
           AND CCS_TRANSPORT_ID = l_transport_id;
        IF SQL%ROWCOUNT = 0 THEN
            INSERT INTO CCS_TRANSPORT_PROPERTY(
                CCS_TRANSPORT_PROPERTY_ID,
                CCS_TRANSPORT_ID,
                CCS_TRANSPORT_PROPERTY_TYPE_ID,
                CCS_TRANSPORT_PROPERTY_VALUE)
              VALUES(
                CCS_TRANSPORT_PROPERTY_SEQ.NEXTVAL,
                l_transport_id,
                l_tpt_id,
                l_value
              );
        END IF;
    END;
    
    PROCEDURE REPORT_SENT_INS (
        l_user_report_id IN REPORT_SENT.USER_REPORT_ID%TYPE,
        l_report_request_id IN REPORT_SENT.REPORT_REQUEST_ID%TYPE,
        l_batch_id IN REPORT_SENT.BATCH_ID%TYPE,
        l_begin_date IN REPORT_SENT.BEGIN_DATE%TYPE,
        l_end_date IN REPORT_SENT.END_DATE%TYPE,
        l_rs_id OUT REPORT_SENT.REPORT_SENT_ID%TYPE,
        l_batch_type_id  IN REPORT_SENT.EXPORT_TYPE%TYPE  DEFAULT NULL)
    IS
        l_cnt PLS_INTEGER;
    BEGIN
        
        SELECT REPORT_SENT_SEQ.NEXTVAL INTO l_rs_id FROM DUAL;
        
        INSERT INTO REPORT_SENT(
            REPORT_SENT_ID,
            REPORT_REQUEST_ID,
            USER_REPORT_ID,
            BEGIN_DATE,
            END_DATE,
            BATCH_ID,
            EXPORT_TYPE,
            REPORT_SENT_STATE_ID,
            DETAILS)
          VALUES(
            l_rs_id,
            l_report_request_id,
            l_user_report_id,
            l_begin_date,
            l_end_date,
            l_batch_id,
            l_batch_type_id,
            0,
            'NEW');
        
        IF l_batch_id IS NULL THEN               
            DELETE FROM RESEND_REPORTS
             WHERE USER_REPORT_ID = l_user_report_id
               AND BATCH_ID IS NULL
               AND BEGIN_DATE = l_begin_date
               AND END_DATE = l_end_date;
        ELSE    
            DELETE FROM RESEND_REPORTS
             WHERE USER_REPORT_ID = l_user_report_id
               AND BATCH_ID = l_batch_id;
            /* NOT NEEDED - handled in report requester   
            SELECT COUNT(*)
              INTO l_cnt
              FROM RESEND_REPORTS
             WHERE BATCH_ID = l_batch_id;
            IF l_cnt = 0 THEN
                IF l_batch_type_id = 3 THEN -- EFT
                    SELECT COUNT(*)
                      INTO l_cnt
                      FROM VW_UNSENT_EFT
                     WHERE BATCH_ID = l_batch_id;
                    IF l_cnt = 0 THEN
                        UPDATE CORP.DOC SET STATUS = 'S'
                         WHERE DOC_ID = l_batch_id;
                    END IF;
                ELSIF l_batch_type_id = 2 THEN -- DEX
                    SELECT COUNT(*)
                      INTO l_cnt
                      FROM VW_UNSENT_DEX
                     WHERE BATCH_ID = l_batch_id;
                    IF l_cnt = 0 THEN
                        UPDATE G4OP.DEX_FILE SET SENT = 'Y'
                         WHERE DEX_FILE_ID = l_batch_id;
                    END IF;
               -- This was causing performance issues
                 ELSE -- TRANS
                    SELECT COUNT(*)
                      INTO l_cnt
                      FROM VW_UNSENT_TRANS
                     WHERE BATCH_ID = l_batch_id;
                    IF l_cnt = 0 THEN
                        UPDATE EXPORT_BATCH SET EXPORT_SENT = l_sent_date, STATUS = 'S'
                         WHERE BATCH_ID = l_batch_id AND EXPORT_TYPE = l_batch_type_id;
                    END IF;               
                END IF;
            END IF;
            --*/
        END IF;      
    END;
    
    -- pre usalive 1.9 signature
    PROCEDURE CCS_TRANSPORT_ERROR_INS(
        pn_report_sent_id IN REPORT_SENT.REPORT_SENT_ID%TYPE,
        pv_error_text IN CCS_TRANSPORT_ERROR.ERROR_TEXT%TYPE,
        pd_error_ts IN CCS_TRANSPORT_ERROR.ERROR_TS%TYPE,
        pn_error_id OUT CCS_TRANSPORT_ERROR.CCS_TRANSPORT_ERROR_ID%TYPE)
    IS
    BEGIN
        CCS_TRANSPORT_ERROR_INS(pn_report_sent_id,pv_error_text, pd_error_ts,1, '', pn_error_id);
    END;
    
    PROCEDURE CCS_TRANSPORT_ERROR_INS(
        pn_report_sent_id IN REPORT_SENT.REPORT_SENT_ID%TYPE,
        pv_error_text IN CCS_TRANSPORT_ERROR.ERROR_TEXT%TYPE,
        pd_error_ts IN CCS_TRANSPORT_ERROR.ERROR_TS%TYPE,
        pd_error_type_id IN CCS_TRANSPORT_ERROR.ERROR_TYPE_ID%TYPE,
        pv_retry_props IN CCS_TRANSPORT_ERROR.RETRY_PROPS%TYPE,
        pn_error_id OUT CCS_TRANSPORT_ERROR.CCS_TRANSPORT_ERROR_ID%TYPE)
    IS
    BEGIN
        UPDATE REPORT.CCS_TRANSPORT 
           SET CCS_TRANSPORT_STATUS_CD = decode(CCS_TRANSPORT_STATUS_CD, 'D', 'D','E', 'E', 'R') 
         WHERE CCS_TRANSPORT_ID = (
           SELECT UR.CCS_TRANSPORT_ID 
             FROM REPORT.REPORT_SENT RS
             JOIN REPORT.USER_REPORT UR ON UR.USER_REPORT_ID = RS.USER_REPORT_ID
            WHERE RS.REPORT_SENT_ID = pn_report_sent_id);
        SELECT REPORT.SEQ_CCS_TRANSPORT_ERROR_ID.NEXTVAL 
          INTO pn_error_id 
           FROM DUAL;
        INSERT INTO REPORT.CCS_TRANSPORT_ERROR(
           CCS_TRANSPORT_ERROR_ID, 
           REPORT_SENT_ID, 
           ERROR_TEXT, 
           ERROR_TS,
           ERROR_TYPE_ID,
           RETRY_PROPS)
         VALUES(
           pn_error_id, 
           pn_report_sent_id, 
           pv_error_text, 
           pd_error_ts,
           pd_error_type_id,
           pv_retry_props);
    END;
    
    PROCEDURE CCS_TRANSPORT_ERROR_RETRY(
        pn_ct_error_id IN CCS_TRANSPORT_ERROR.CCS_TRANSPORT_ERROR_ID%TYPE)
    IS
    BEGIN
        UPDATE CCS_TRANSPORT_ERROR 
           SET REATTEMPTED_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP), 
               REATTEMPTED_FLAG = 'Y'
         WHERE CCS_TRANSPORT_ERROR_ID = pn_ct_error_id;
    END;
    
    PROCEDURE CCS_TRANSPORT_RETRY_SUCCESS(
        pn_ccs_transport_id IN CCS_TRANSPORT.CCS_TRANSPORT_ID%TYPE, 
        pn_ct_error_id IN CCS_TRANSPORT_ERROR.CCS_TRANSPORT_ERROR_ID%TYPE)
    IS
    BEGIN
        UPDATE CCS_TRANSPORT_ERROR 
           SET REATTEMPTED_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP), 
               REATTEMPTED_FLAG = 'Y',
               RETRY_COUNT = RETRY_COUNT+1
         WHERE CCS_TRANSPORT_ERROR_ID = pn_ct_error_id;
    END;
    
    PROCEDURE CCS_TRANSPORT_ERROR_UPDATE(
        pn_ccs_transport_id IN CCS_TRANSPORT.CCS_TRANSPORT_ID%TYPE, 
        pn_ct_error_id IN CCS_TRANSPORT_ERROR.CCS_TRANSPORT_ERROR_ID%TYPE,
        pv_error_text IN CCS_TRANSPORT_ERROR.ERROR_TEXT%TYPE,
        pd_error_type_id IN CCS_TRANSPORT_ERROR.ERROR_TYPE_ID%TYPE,
        pv_retry_props IN CCS_TRANSPORT_ERROR.RETRY_PROPS%TYPE,
        pn_disabled_transport OUT NUMBER)
    IS
      l_reattempted_ts CCS_TRANSPORT_ERROR.REATTEMPTED_TS%TYPE:=SYS_EXTRACT_UTC(SYSTIMESTAMP);
      l_created_ts CCS_TRANSPORT_ERROR.CREATED_TS%TYPE;
      l_prev_error_text CCS_TRANSPORT_ERROR.ERROR_TEXT%TYPE;
      l_prev_reattempted_ts CCS_TRANSPORT_ERROR.REATTEMPTED_TS%TYPE;
      l_retry_index NUMBER;
      l_last_sent_ts REPORT.USER_REPORT.LAST_SENT_UTC_TS%TYPE;
    BEGIN
       select ERROR_TEXT, REATTEMPTED_TS into l_prev_error_text, l_prev_reattempted_ts 
       from REPORT.CCS_TRANSPORT_ERROR WHERE CCS_TRANSPORT_ERROR_ID = pn_ct_error_id;
       
       UPDATE REPORT.CCS_TRANSPORT_ERROR 
           SET REATTEMPTED_TS = l_reattempted_ts,
               REATTEMPTED_FLAG = decode(REATTEMPTED_FLAG, 'D', 'D','N'),
               ERROR_TEXT = pv_error_text,
               ERROR_TYPE_ID = pd_error_type_id,
               RETRY_COUNT = RETRY_COUNT+1,
               RETRY_PROPS=pv_retry_props
         WHERE CCS_TRANSPORT_ERROR_ID = pn_ct_error_id returning created_ts, retry_count into l_created_ts, l_retry_index;
         
       insert into REPORT.CCS_TRANSPORT_ERROR_HISTORY(CCS_TRANSPORT_ERROR_ID, RETRY_INDEX, ERROR_TEXT,REATTEMPTED_TS)
       values( pn_ct_error_id, l_retry_index, l_prev_error_text, l_prev_reattempted_ts);
       
      IF (pd_error_type_id = 1 or pd_error_type_id = 3) and l_reattempted_ts > l_created_ts + 7 THEN
        select max(NVL(LAST_SENT_UTC_TS, SYS_EXTRACT_UTC(CAST (MIN_DATE AS TIMESTAMP)))) into l_last_sent_ts
        from report.user_report 
        where ccs_transport_id=pn_ccs_transport_id;
        
        IF l_reattempted_ts > l_last_sent_ts + 7 THEN
          UPDATE REPORT.CCS_TRANSPORT 
             SET CCS_TRANSPORT_STATUS_CD = 'E' 
          WHERE CCS_TRANSPORT_ID = pn_ccs_transport_id;   
          pn_disabled_transport:=1;
        ELSE
          UPDATE REPORT.USER_REPORT 
          SET STATUS='E'
          WHERE USER_REPORT_ID = (SELECT USER_REPORT_ID FROM REPORT.REPORT_SENT WHERE REPORT_SENT_ID=(SELECT REPORT_SENT_ID FROM REPORT.CCS_TRANSPORT_ERROR WHERE CCS_TRANSPORT_ERROR_ID = pn_ct_error_id))
          AND l_reattempted_ts > NVL(LAST_SENT_UTC_TS, SYS_EXTRACT_UTC(CAST (MIN_DATE AS TIMESTAMP)))+7;
          
          pn_disabled_transport:=0;
        END IF;
        
        UPDATE REPORT.CCS_TRANSPORT_ERROR set REATTEMPTED_FLAG='D' 
        WHERE CCS_TRANSPORT_ERROR_ID = pn_ct_error_id;
        
        UPDATE REPORT.REPORT_SENT set REPORT_SENT_STATE_ID=5 
        WHERE REPORT_SENT_ID = (SELECT REPORT_SENT_ID FROM REPORT.CCS_TRANSPORT_ERROR WHERE CCS_TRANSPORT_ERROR_ID = pn_ct_error_id);
      ELSE
        pn_disabled_transport:=0;
      END IF;
    END;
    
    PROCEDURE CCS_UPDATE_REATTEMPTED_FLAG(
        pn_ct_error_id IN CCS_TRANSPORT_ERROR.CCS_TRANSPORT_ERROR_ID%TYPE,
        pn_reattempted_flag IN CCS_TRANSPORT_ERROR.REATTEMPTED_FLAG%TYPE)
    IS
    BEGIN
      UPDATE REPORT.CCS_TRANSPORT_ERROR SET REATTEMPTED_FLAG=pn_reattempted_flag WHERE CCS_TRANSPORT_ERROR_ID=pn_ct_error_id;
    END;
    
    -- BEGIN Device alerting additions
    -- -------------------------------
    -- R45 and above
    PROCEDURE ADD_EVENT_ALERT(
        pn_event_id IN REPORT.TERMINAL_ALERT.REFERENCE_ID%TYPE,
        pn_terminal_alert_id OUT REPORT.TERMINAL_ALERT.TERMINAL_ALERT_ID%TYPE,
        pn_terminal_id OUT REPORT.TERMINAL.TERMINAL_ID%TYPE,
        pv_device_serial_cd OUT REPORT.EPORT.EPORT_SERIAL_NUM%TYPE,
        pc_dont_send_flag OUT VARCHAR2)
    IS
        lc_alert_status REPORT.ALERT.STATUS%TYPE;
        ln_alert_id REPORT.ALERT.ALERT_ID%TYPE;
        ld_alert_ts REPORT.TERMINAL_ALERT.ALERT_DATE%TYPE;
        lv_alert_description REPORT.TERMINAL_ALERT.DETAILS%TYPE;
        ln_component REPORT.TERMINAL_ALERT.COMPONENT_ID%TYPE;
        ld_notification_ts REPORT.TERMINAL_ALERT.NOTIFICATION_DATE%TYPE;
    BEGIN
        SELECT TE.TERMINAL_ID, D.DEVICE_SERIAL_CD, A.STATUS, A.ALERT_ID, E.EVENT_START_TS, 
               CASE WHEN A.ALERT_SOURCE_ID IN(2,3) AND REGEXP_LIKE(ED_D.EVENT_DETAIL_VALUE, '^\d\d/\d\d/\d{4} \d\d:\d\d:\d\d - ') THEN SUBSTR(ED_D.EVENT_DETAIL_VALUE, 23, 4000) ELSE ED_D.EVENT_DETAIL_VALUE END, 
               H.HOST_PORT_NUM, E.EVENT_UPLOAD_COMPLETE_TS
          INTO pn_terminal_id, pv_device_serial_cd, lc_alert_status, ln_alert_id, ld_alert_ts, lv_alert_description, ln_component, ld_notification_ts
          FROM DEVICE.EVENT E
          JOIN DEVICE.HOST H ON E.HOST_ID = H.HOST_ID
          JOIN DEVICE.DEVICE D ON H.DEVICE_ID = D.DEVICE_ID
          LEFT OUTER JOIN REPORT.ALERT A ON E.EVENT_TYPE_ID = A.EVENT_TYPE_ID AND A.ALERT_SOURCE_ID IN(2,3)
          LEFT OUTER JOIN (REPORT.TERMINAL_EPORT TE
          JOIN REPORT.EPORT EP ON TE.EPORT_ID = EP.EPORT_ID) 
            ON D.DEVICE_SERIAL_CD = EP.EPORT_SERIAL_NUM 
           AND E.EVENT_START_TS >= NVL(TE.START_DATE, MIN_DATE)
           AND E.EVENT_START_TS < NVL(TE.END_DATE, MAX_DATE)
          LEFT OUTER JOIN DEVICE.EVENT_DETAIL ED_D ON E.EVENT_ID = ED_D.EVENT_ID AND ED_D.EVENT_DETAIL_TYPE_ID = 3
         WHERE E.EVENT_ID = pn_event_id;
        IF pn_terminal_id IS NOT NULL AND lc_alert_status = 'A' THEN
           SELECT CASE WHEN COUNT(*) > 0 THEN 'Y' ELSE 'N' END
             INTO pc_dont_send_flag
             FROM REPORT.TERMINAL_ALERT
            WHERE ALERT_ID = ln_alert_id
              AND TERMINAL_ID = pn_terminal_id
              AND ALERT_DATE > ld_alert_ts - 0.5
              AND DETAILS = lv_alert_description
              AND RESPONSE_SENT IN('P', 'Y');
            SELECT REPORT.TERMINAL_ALERT_SEQ.NEXTVAL 
              INTO pn_terminal_alert_id
              FROM DUAL;
            INSERT INTO REPORT.TERMINAL_ALERT(	
                TERMINAL_ALERT_ID, 
                ALERT_ID, 
                TERMINAL_ID, 
                ALERT_DATE, 
                DETAILS, 
                RESPONSE_SENT,
                REFERENCE_ID, 
                COMPONENT_ID, 
                NOTIFICATION_DATE)
            VALUES(
                pn_terminal_alert_id,
                ln_alert_id,
                pn_terminal_id,
                ld_alert_ts,
                lv_alert_description,
                CASE WHEN pc_dont_send_flag = 'Y' THEN 'I' ELSE 'P' END,
                pn_event_id,
                ln_component,
                ld_notification_ts
            );
       ELSE
           pc_dont_send_flag := 'Y'; 
       END IF;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            NULL;
    END;
    
    -- R44 and below
    PROCEDURE ADD_EVENT_ALERT(
        pn_event_id IN REPORT.TERMINAL_ALERT.REFERENCE_ID%TYPE,
        pn_terminal_alert_id OUT REPORT.TERMINAL_ALERT.TERMINAL_ALERT_ID%TYPE,
        pn_terminal_id OUT REPORT.TERMINAL.TERMINAL_ID%TYPE,
        pv_device_serial_cd OUT REPORT.EPORT.EPORT_SERIAL_NUM%TYPE)
    IS
        lc_dont_send_flag VARCHAR2(4000);
    BEGIN
        ADD_EVENT_ALERT(
            pn_event_id,
            pn_terminal_alert_id,
            pn_terminal_id,
            pv_device_serial_cd,
            lc_dont_send_flag);
    END;
    
	-- END Device alerting additions
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/G4OP/DEX_DATA.psk?rev=1.2
CREATE OR REPLACE PACKAGE G4OP.DEX_DATA IS 
	PROCEDURE   DEX_FILE_SENT(
	    l_file_name IN DEX_FILE.FILE_NAME%TYPE,
	    l_count out number);

    PROCEDURE CREATE_DEX_FILE(
        pn_dex_file_id     OUT G4OP.DEX_FILE.DEX_FILE_ID%TYPE,
        pv_filename         IN G4OP.DEX_FILE.FILE_NAME%TYPE,
        pv_device_serial_cd IN REPORT.EPORT.EPORT_SERIAL_NUM%TYPE,
        pd_dex_date         IN G4OP.DEX_FILE.DEX_DATE%TYPE,
        pn_dex_type         IN G4OP.DEX_FILE.DEX_TYPE%TYPE,
        pn_file_transfer_id IN G4OP.DEX_FILE.FILE_TRANSFER_ID%TYPE,
        pl_file_content    OUT G4OP.DEX_FILE.FILE_CONTENT%TYPE);
        
    --R45 and above
    PROCEDURE ADD_DEX_FILE_LINE(
        pn_dex_file_id IN G4OP.DEX_FILE.DEX_FILE_ID%TYPE,
        pv_dex_code IN G4OP.DEX_FILE_LINE.DEX_CODE%TYPE,
        pv_details IN G4OP.DEX_FILE_LINE.DETAILS%TYPE,
        pn_terminal_id IN REPORT.TERMINAL_ALERT.TERMINAL_ID%TYPE,
        pn_alert_id IN REPORT.TERMINAL_ALERT.ALERT_ID%TYPE,
        pc_alert_status IN REPORT.ALERT.STATUS%TYPE,
        pd_dex_date IN REPORT.TERMINAL_ALERT.ALERT_DATE%TYPE,
        pn_file_transfer_id IN REPORT.TERMINAL_ALERT.REFERENCE_ID%TYPE,
        pn_component IN REPORT.TERMINAL_ALERT.COMPONENT_ID%TYPE,
        pd_upload_date IN REPORT.TERMINAL_ALERT.NOTIFICATION_DATE%TYPE,
        pn_terminal_alert_id OUT REPORT.TERMINAL_ALERT.TERMINAL_ALERT_ID%TYPE,
        pc_dont_send_flag OUT VARCHAR2);
        
    --R44 and below
    PROCEDURE ADD_DEX_FILE_LINE(
        pn_dex_file_id IN G4OP.DEX_FILE.DEX_FILE_ID%TYPE,
        pv_dex_code IN G4OP.DEX_FILE_LINE.DEX_CODE%TYPE,
        pv_details IN G4OP.DEX_FILE_LINE.DETAILS%TYPE,
        pn_terminal_id IN REPORT.TERMINAL_ALERT.TERMINAL_ID%TYPE,
        pn_alert_id IN REPORT.TERMINAL_ALERT.ALERT_ID%TYPE,
        pc_alert_status IN REPORT.ALERT.STATUS%TYPE,
        pd_dex_date IN REPORT.TERMINAL_ALERT.ALERT_DATE%TYPE,
        pn_file_transfer_id IN REPORT.TERMINAL_ALERT.REFERENCE_ID%TYPE,
        pn_component IN REPORT.TERMINAL_ALERT.COMPONENT_ID%TYPE,
        pd_upload_date IN REPORT.TERMINAL_ALERT.NOTIFICATION_DATE%TYPE,
        pn_terminal_alert_id OUT REPORT.TERMINAL_ALERT.TERMINAL_ALERT_ID%TYPE);
     
END ;
 
/
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/G4OP/DEX_DATA.pbk?rev=1.4
CREATE OR REPLACE PACKAGE BODY G4OP.DEX_DATA AS

	PROCEDURE DEX_FILE_SENT(
        l_file_name IN DEX_FILE.FILE_NAME%TYPE,
	    l_count out number)
	IS
	BEGIN	
		 Update DEX_FILE
		 Set 	sent = 'Y'
		 Where  file_name = l_file_name;
		 l_count := SQL%ROWCOUNT;	
		 COMMIT;
	END;

	PROCEDURE CREATE_DEX_FILE(
	    pn_dex_file_id     OUT G4OP.DEX_FILE.DEX_FILE_ID%TYPE,
	    pv_filename         IN G4OP.DEX_FILE.FILE_NAME%TYPE,
	    pv_device_serial_cd IN REPORT.EPORT.EPORT_SERIAL_NUM%TYPE,
	    pd_dex_date         IN G4OP.DEX_FILE.DEX_DATE%TYPE,
	    pn_dex_type         IN G4OP.DEX_FILE.DEX_TYPE%TYPE,
	    pn_file_transfer_id IN G4OP.DEX_FILE.FILE_TRANSFER_ID%TYPE,
	    pl_file_content    OUT G4OP.DEX_FILE.FILE_CONTENT%TYPE)
	IS
	    ln_eport_id G4OP.DEX_FILE.EPORT_ID%TYPE;
	    ln_cnt PLS_INTEGER;
	BEGIN
		SELECT REPORT.DATA_IN_PKG.GET_OR_CREATE_DEVICE(DLA.DEVICE_SERIAL_CD, 'PSS', D.DEVICE_TYPE_ID)
          INTO ln_eport_id
	      FROM DEVICE.DEVICE_LAST_ACTIVE DLA
	      JOIN DEVICE.DEVICE D ON DLA.DEVICE_ID = D.DEVICE_ID
	     WHERE DLA.DEVICE_SERIAL_CD = pv_device_serial_cd;
        -- Get dex_file_id only if file_transfer_id matches existing
	    SELECT MAX(DECODE(FILE_TRANSFER_ID, pn_file_transfer_id, DEX_FILE_ID)), COUNT(*)
	      INTO pn_dex_file_id, ln_cnt
	      FROM G4OP.DEX_FILE
	     WHERE FILE_NAME = pv_filename
	        OR FILE_TRANSFER_ID = pn_file_transfer_id
	        OR (EPORT_ID = ln_eport_id AND DEX_DATE = pd_dex_date);
	    IF ln_cnt > 0 THEN
	        RETURN;
	    END IF;
	    SELECT G4OP.DEX_FILE_SEQ.NEXTVAL
	      INTO pn_dex_file_id
	      FROM DUAL;
	    INSERT INTO G4OP.DEX_FILE(
	        DEX_FILE_ID,
	        FILE_NAME,
	        EPORT_ID,
	        DEX_DATE,
	        DEX_TYPE,
	        FILE_TRANSFER_ID,
	        FILE_CONTENT)
	     VALUES(
	        pn_dex_file_id,
	        pv_filename,
	        ln_eport_id,
	        pd_dex_date,
	        pn_dex_type,
	        pn_file_transfer_id,
	        EMPTY_CLOB());
        SELECT FILE_CONTENT
          INTO pl_file_content
          FROM G4OP.DEX_FILE
         WHERE DEX_FILE_ID = pn_dex_file_id
           FOR UPDATE;
	END;
	
    PROCEDURE ADD_DEX_FILE_LINE(
        pn_dex_file_id IN G4OP.DEX_FILE.DEX_FILE_ID%TYPE,
        pv_dex_code IN G4OP.DEX_FILE_LINE.DEX_CODE%TYPE,
        pv_details IN G4OP.DEX_FILE_LINE.DETAILS%TYPE,
        pn_terminal_id IN REPORT.TERMINAL_ALERT.TERMINAL_ID%TYPE,
        pn_alert_id IN REPORT.TERMINAL_ALERT.ALERT_ID%TYPE,
        pc_alert_status IN REPORT.ALERT.STATUS%TYPE,
        pd_dex_date IN REPORT.TERMINAL_ALERT.ALERT_DATE%TYPE,
        pn_file_transfer_id IN REPORT.TERMINAL_ALERT.REFERENCE_ID%TYPE,
        pn_component IN REPORT.TERMINAL_ALERT.COMPONENT_ID%TYPE,
        pd_upload_date IN REPORT.TERMINAL_ALERT.NOTIFICATION_DATE%TYPE,
        pn_terminal_alert_id OUT REPORT.TERMINAL_ALERT.TERMINAL_ALERT_ID%TYPE,
        pc_dont_send_flag OUT VARCHAR2)
    IS
        ln_line_nbr G4OP.DEX_FILE_LINE.LINE_NBR%TYPE;
    BEGIN
        SELECT NVL(MAX(LINE_NBR),0) + 1 
          INTO ln_line_nbr
          FROM G4OP.DEX_FILE_LINE 
         WHERE DEX_FILE_ID = pn_dex_file_id;
        INSERT INTO G4OP.DEX_FILE_LINE(DEX_FILE_ID, LINE_NBR, DEX_CODE, DETAILS)
            VALUES(pn_dex_file_id, ln_line_nbr, pv_dex_code, pv_details);
        IF pn_terminal_id IS NOT NULL AND pn_alert_id IS NOT NULL AND pc_alert_status = 'A' THEN 
            SELECT CASE WHEN COUNT(*) > 0 THEN 'Y' ELSE 'N' END
               INTO pc_dont_send_flag
               FROM REPORT.TERMINAL_ALERT
              WHERE ALERT_ID = pn_alert_id
                AND TERMINAL_ID = pn_terminal_id
                AND ALERT_DATE > pd_dex_date - 0.5
                AND DETAILS = pv_details
                AND RESPONSE_SENT IN('P', 'Y');
             
             SELECT REPORT.TERMINAL_ALERT_SEQ.NEXTVAL
              INTO pn_terminal_alert_id
              FROM DUAL;
       
             INSERT INTO REPORT.TERMINAL_ALERT(
              TERMINAL_ALERT_ID, 
              ALERT_ID, 
              TERMINAL_ID, 
              ALERT_DATE, 
              DETAILS, 
              RESPONSE_SENT,
              REFERENCE_ID, 
              COMPONENT_ID, 
              NOTIFICATION_DATE)
           VALUES(
              pn_terminal_alert_id,
              pn_alert_id,
              pn_terminal_id,
              pd_dex_date,
              pv_details,
              CASE WHEN pc_dont_send_flag = 'Y' THEN 'I' ELSE 'P' END,
              pn_file_transfer_id,
              pn_component,
              pd_upload_date);
        END IF;
    END;
           
    --R44 and below
    PROCEDURE ADD_DEX_FILE_LINE(
        pn_dex_file_id IN G4OP.DEX_FILE.DEX_FILE_ID%TYPE,
        pv_dex_code IN G4OP.DEX_FILE_LINE.DEX_CODE%TYPE,
        pv_details IN G4OP.DEX_FILE_LINE.DETAILS%TYPE,
        pn_terminal_id IN REPORT.TERMINAL_ALERT.TERMINAL_ID%TYPE,
        pn_alert_id IN REPORT.TERMINAL_ALERT.ALERT_ID%TYPE,
        pc_alert_status IN REPORT.ALERT.STATUS%TYPE,
        pd_dex_date IN REPORT.TERMINAL_ALERT.ALERT_DATE%TYPE,
        pn_file_transfer_id IN REPORT.TERMINAL_ALERT.REFERENCE_ID%TYPE,
        pn_component IN REPORT.TERMINAL_ALERT.COMPONENT_ID%TYPE,
        pd_upload_date IN REPORT.TERMINAL_ALERT.NOTIFICATION_DATE%TYPE,
        pn_terminal_alert_id OUT REPORT.TERMINAL_ALERT.TERMINAL_ALERT_ID%TYPE)
    IS
        lc_dont_send_flag VARCHAR2(4000);
    BEGIN
        ADD_DEX_FILE_LINE(
            pn_dex_file_id,
            pv_dex_code,
            pv_details,
            pn_terminal_id,
            pn_alert_id,
            pc_alert_status,
            pd_dex_date,
            pn_file_transfer_id,
            pn_component,
            pd_upload_date,
            pn_terminal_alert_id,
            lc_dont_send_flag);
    END;
    
END DEX_DATA;
/
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/DEVICE/PKG_DEVICE_CONFIGURATION.pbk?rev=1.154
CREATE OR REPLACE PACKAGE BODY DEVICE.PKG_DEVICE_CONFIGURATION IS
    
PROCEDURE SP_GET_FILE_TRANSFER_BLOB
(
    pn_file_transfer_id file_transfer.file_transfer_id%TYPE,
    pbl_file_transfer_content OUT file_transfer.file_transfer_content%TYPE
)
IS
BEGIN
    SELECT file_transfer_content
	INTO pbl_file_transfer_content
    FROM device.file_transfer
    WHERE file_transfer_id = pn_file_transfer_id;
END;

PROCEDURE SP_GET_MAP_CONFIG_FILE
(
    pn_device_id device.device_id%TYPE,
    pv_file_content_hex OUT VARCHAR2
)
IS
	ln_device_id DEVICE.DEVICE_ID%TYPE;
BEGIN
	SELECT MAX(dla.device_id)
	INTO ln_device_id
	FROM device.device d
	JOIN device.device_last_active dla ON d.device_name = dla.device_name
	WHERE d.device_id = pn_device_id;
		
    pv_file_content_hex := '';
	FOR rec IN (
		SELECT DS.DEVICE_SETTING_VALUE
		FROM DEVICE.DEVICE_SETTING DS
		JOIN DEVICE.DEVICE D ON DS.DEVICE_ID = D.DEVICE_ID
		JOIN DEVICE.CONFIG_TEMPLATE_SETTING CTS ON DECODE(D.DEVICE_TYPE_ID, PKG_CONST.DEVICE_TYPE__GX, PKG_CONST.DEVICE_TYPE__G4, D.DEVICE_TYPE_ID) = CTS.DEVICE_TYPE_ID
			AND DS.DEVICE_SETTING_PARAMETER_CD = CTS.DEVICE_SETTING_PARAMETER_CD
			AND CTS.FIELD_OFFSET BETWEEN 0 AND 511
		WHERE DS.DEVICE_ID = ln_device_id
		ORDER BY CTS.FIELD_OFFSET
	) 
	LOOP
		pv_file_content_hex := pv_file_content_hex || rec.DEVICE_SETTING_VALUE;
	END LOOP;
END;

PROCEDURE SP_UPSERT_KIOSK_CONFIG_UPDATE
(
	pv_device_name DEVICE.DEVICE_NAME%TYPE,
	pn_device_id DEVICE.DEVICE_ID%TYPE,
	pv_kiosk_config_name ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE
)
IS
	ln_file_transfer_id FILE_TRANSFER.FILE_TRANSFER_ID%TYPE;
	lb_file_transfer_content FILE_TRANSFER.FILE_TRANSFER_CONTENT%TYPE;
	ln_device_file_transfer_id DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_ID%TYPE;
	ln_required_fields NUMBER;
	ln_pending_file_transfer NUMBER;
	lv_line VARCHAR2(300);
	ln_device_id DEVICE.DEVICE_ID%TYPE;
BEGIN
	SELECT MAX(dla.device_id)
	INTO ln_device_id
	FROM device.device d
	JOIN device.device_last_active dla ON d.device_name = dla.device_name
	WHERE d.device_id = pn_device_id;
	
	SELECT COUNT(1) INTO ln_required_fields 
	FROM DEVICE.DEVICE_SETTING
	WHERE DEVICE_ID = ln_device_id
	AND DEVICE_SETTING_PARAMETER_CD IN ('SSN', 'VMC', 'EncKey')
	AND DECODE(DEVICE_SETTING_VALUE, NULL, 0, LENGTH(DEVICE_SETTING_VALUE)) > 6;

	IF ln_required_fields = 3 THEN
		BEGIN
			SELECT FILE_TRANSFER_ID
			INTO ln_file_transfer_id
			FROM (SELECT /*+index(FT IDX_FILE_TRANSFER_NAME)*/ FILE_TRANSFER_ID 
			FROM DEVICE.FILE_TRANSFER FT
			WHERE FILE_TRANSFER_NAME = pv_device_name || '-CFG'
				AND FILE_TRANSFER_TYPE_CD = PKG_CONST.FILE_TYPE__CONFIG
			ORDER BY CREATED_TS)
			WHERE ROWNUM = 1;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				SELECT seq_file_transfer_id.nextval INTO ln_file_transfer_id FROM DUAL;
				INSERT INTO DEVICE.FILE_TRANSFER(FILE_TRANSFER_ID, FILE_TRANSFER_NAME, FILE_TRANSFER_TYPE_CD)
				VALUES(ln_file_transfer_id, pv_device_name || '-CFG', PKG_CONST.FILE_TYPE__CONFIG);
		END;
		
		UPDATE DEVICE.FILE_TRANSFER
		SET FILE_TRANSFER_CONTENT = EMPTY_BLOB()
		WHERE FILE_TRANSFER_ID = ln_file_transfer_id;
		
		SELECT FILE_TRANSFER_CONTENT
		INTO lb_file_transfer_content
		FROM DEVICE.FILE_TRANSFER
		WHERE FILE_TRANSFER_ID = ln_file_transfer_id
		FOR UPDATE;
		
		FOR rec IN (
			SELECT DS.DEVICE_SETTING_PARAMETER_CD, DS.DEVICE_SETTING_VALUE
			FROM DEVICE.DEVICE_SETTING DS
			JOIN DEVICE.DEVICE_SETTING_PARAMETER DSP ON DS.DEVICE_SETTING_PARAMETER_CD = DSP.DEVICE_SETTING_PARAMETER_CD
			LEFT OUTER JOIN DEVICE.SETTING_PARAMETER_TYPE SPT ON DSP.SETTING_PARAMETER_TYPE_ID = SPT.SETTING_PARAMETER_TYPE_ID
			WHERE DS.DEVICE_ID = ln_device_id AND (SPT.SETTING_PARAMETER_TYPE_DESC IS NULL OR SPT.SETTING_PARAMETER_TYPE_DESC != 'SERVER_ONLY_DEVICE_SETTING')
			ORDER BY DS.FILE_ORDER
		) 
		LOOP
			lv_line := rec.device_setting_parameter_cd || '=' || rec.device_setting_value || PKG_CONST.ASCII__LF;
			dbms_lob.writeappend(lb_file_transfer_content, LENGTH(lv_line), UTL_RAW.CAST_TO_RAW(lv_line));
		END LOOP;
	
		UPDATE ENGINE.MACHINE_CMD_PENDING
		SET EXECUTE_CD = 'A'
		WHERE MACHINE_ID = pv_device_name
		AND DATA_TYPE = '9B'
		AND COMMAND = UPPER(pv_kiosk_config_name);
		
		SELECT COUNT(1) 
		INTO ln_pending_file_transfer
		FROM DEVICE.DEVICE_FILE_TRANSFER DFT, ENGINE.MACHINE_CMD_PENDING MCP 
		WHERE DFT.DEVICE_ID = ln_device_id
		AND DFT.FILE_TRANSFER_ID = ln_file_transfer_id
		AND DFT.DEVICE_FILE_TRANSFER_STATUS_CD = 0 
		AND DFT.DEVICE_FILE_TRANSFER_DIRECT = 'O' 
		AND MCP.MACHINE_ID = pv_device_name
		AND MCP.DATA_TYPE = 'A4'
		AND DFT.DEVICE_FILE_TRANSFER_ID = MCP.DEVICE_FILE_TRANSFER_ID;
			
		IF ln_pending_file_transfer = 0 THEN
			SELECT SEQ_DEVICE_FILE_TRANSFER_ID.NEXTVAL INTO ln_device_file_transfer_id FROM DUAL;
			INSERT INTO DEVICE.DEVICE_FILE_TRANSFER(
				DEVICE_FILE_TRANSFER_ID, 
				DEVICE_ID, 
				FILE_TRANSFER_ID, 
				DEVICE_FILE_TRANSFER_DIRECT, 
				DEVICE_FILE_TRANSFER_STATUS_CD, 
				DEVICE_FILE_TRANSFER_PKT_SIZE 
			) 
			VALUES(
				ln_device_file_transfer_id,
				ln_device_id, 
				ln_file_transfer_id, 
				'O',
				0,
				1024);
				
			INSERT INTO ENGINE.MACHINE_CMD_PENDING(
				MACHINE_ID, 
				DATA_TYPE, 
				DEVICE_FILE_TRANSFER_ID, 
				EXECUTE_CD, 
				EXECUTE_ORDER
			) 
			VALUES(
				pv_device_name,
				'A4',
				ln_device_file_transfer_id,
				'P',
				1
			);
		END IF;
	END IF;
END;

-- IS_DEVICE_SETTING_IN_CONFIG is deprecated in R30
FUNCTION IS_DEVICE_SETTING_IN_CONFIG(
	pn_device_id IN device.device_id%TYPE,
	pv_device_setting_parameter_cd IN device_setting.device_setting_parameter_cd%TYPE,
	pv_device_setting_value IN device_setting.device_setting_value%TYPE
)
RETURN VARCHAR
IS
BEGIN
    RETURN 'Y';
END;

	FUNCTION GET_DEVICE_SETTING(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pv_device_setting_parameter_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE
	) RETURN DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE
	IS
		lv_device_setting_value DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
	BEGIN
		SELECT DS.DEVICE_SETTING_VALUE
		INTO lv_device_setting_value
		FROM DEVICE.DEVICE D
		JOIN DEVICE.DEVICE_LAST_ACTIVE DLA ON D.DEVICE_NAME = DLA.DEVICE_NAME
		JOIN DEVICE.DEVICE_SETTING DS ON DLA.DEVICE_ID = DS.DEVICE_ID
		WHERE D.DEVICE_ID = pn_device_id
			AND DS.DEVICE_SETTING_PARAMETER_CD = pv_device_setting_parameter_cd;
		
		RETURN lv_device_setting_value;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			RETURN NULL;
	END;

PROCEDURE SP_UPDATE_DEVICE_SETTING(
	pn_device_id IN device.device_id%TYPE,
	pv_device_setting_parameter_cd IN device_setting.device_setting_parameter_cd%TYPE,
	pv_device_setting_value IN device_setting.device_setting_value%TYPE,
    pc_only_greater_flag IN CHAR DEFAULT 'N'
)
IS
	ln_exists NUMBER;
BEGIN
	SP_UPSERT_DEVICE_SETTING(pn_device_id, pv_device_setting_parameter_cd, pv_device_setting_value, ln_exists);
END;

	PROCEDURE SP_UPSERT_DEVICE_SETTING(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pv_device_setting_parameter_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_device_setting_value DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pn_file_order DEVICE_SETTING.FILE_ORDER%TYPE,
		pn_exists OUT NUMBER,
        pc_only_greater_flag IN CHAR DEFAULT 'N'
	)
	IS
		ln_count NUMBER;
		ln_device_id DEVICE.DEVICE_ID%TYPE;
	BEGIN
		SELECT MAX(dla.device_id)
		INTO ln_device_id
		FROM device.device d
		JOIN device.device_last_active dla ON d.device_name = dla.device_name
		WHERE d.device_id = pn_device_id;
	
		SELECT COUNT(1) INTO ln_count
		FROM device.device_setting_parameter
		WHERE device_setting_parameter_cd = pv_device_setting_parameter_cd;
			
		IF ln_count = 0 THEN
			BEGIN
				INSERT INTO device.device_setting_parameter(device_setting_parameter_cd)
				VALUES(pv_device_setting_parameter_cd);
			EXCEPTION
	        	WHEN DUP_VAL_ON_INDEX THEN
	        		NULL;
	        END;	
		END IF;
		
		UPDATE device.device_setting
		   SET device_setting_value = 
                CASE 
                    WHEN pc_only_greater_flag != 'Y' OR TO_NUMBER_OR_NULL(pv_device_setting_value) > TO_NUMBER_OR_NULL(NVL(device_setting_value, '0')) THEN pv_device_setting_value
                    ELSE device_setting_value 
                END
		 WHERE device_id = ln_device_id
           AND device_setting_parameter_cd = pv_device_setting_parameter_cd;
		
		IF SQL%NOTFOUND THEN
			pn_exists := 0;
			BEGIN
				INSERT INTO device.device_setting(device_id, device_setting_parameter_cd, device_setting_value, file_order)
				VALUES(ln_device_id, pv_device_setting_parameter_cd, pv_device_setting_value, pn_file_order);
			EXCEPTION
	        	WHEN DUP_VAL_ON_INDEX THEN
	        		SP_UPSERT_DEVICE_SETTING(ln_device_id, pv_device_setting_parameter_cd, pv_device_setting_value, pn_file_order, pn_exists, pc_only_greater_flag);
	        END;
		ELSE
			pn_exists := 1;
		END IF;
    END;
	
PROCEDURE SP_UPSERT_DEVICE_SETTING(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pv_device_setting_parameter_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_device_setting_value DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,		
		pn_exists OUT NUMBER
	)
	IS
	BEGIN
		SP_UPSERT_DEVICE_SETTING(pn_device_id, pv_device_setting_parameter_cd, pv_device_setting_value, NULL, pn_exists);
	END;

PROCEDURE SP_UPSERT_CFG_TEMPLATE_SETTING(
	pn_config_template_id CONFIG_TEMPLATE_SETTING.CONFIG_TEMPLATE_ID%TYPE,
	pv_device_setting_parameter_cd CONFIG_TEMPLATE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
	pv_config_template_setting_val CONFIG_TEMPLATE_SETTING.CONFIG_TEMPLATE_SETTING_VALUE%TYPE,
	pn_exists OUT NUMBER
) 
IS
	ln_count NUMBER;
BEGIN
	SELECT COUNT(1) INTO ln_count
	FROM device.device_setting_parameter
	WHERE device_setting_parameter_cd = pv_device_setting_parameter_cd;
		
	IF ln_count = 0 THEN
		BEGIN
			INSERT INTO device.device_setting_parameter(device_setting_parameter_cd)
			VALUES(pv_device_setting_parameter_cd);
		EXCEPTION
			WHEN DUP_VAL_ON_INDEX THEN
				NULL;
		END;	
	END IF;
	
	UPDATE device.config_template_setting
	SET config_template_setting_value = pv_config_template_setting_val
	WHERE config_template_id = pn_config_template_id
		AND device_setting_parameter_cd = pv_device_setting_parameter_cd;
	
	IF SQL%NOTFOUND THEN
		pn_exists := 0;
		BEGIN
			INSERT INTO device.config_template_setting(config_template_id, device_setting_parameter_cd, config_template_setting_value)
			VALUES(pn_config_template_id, pv_device_setting_parameter_cd, pv_config_template_setting_val);
		EXCEPTION
			WHEN DUP_VAL_ON_INDEX THEN
				SP_UPSERT_CFG_TEMPLATE_SETTING(pn_config_template_id, pv_device_setting_parameter_cd, pv_config_template_setting_val, pn_exists);
		END;
	ELSE
		pn_exists := 1;
	END IF;
END;
	
PROCEDURE SP_UPDATE_DEVICE_SETTINGS
(
    pn_device_id IN device.device_id%TYPE,
    pn_file_transfer_id IN file_transfer.file_transfer_id%TYPE,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_setting_count OUT NUMBER
)
IS
    ll_file_transfer_content file_transfer.file_transfer_content%TYPE;
    ln_pos NUMBER := 1;
    ln_content_size NUMBER := 2000;
    ln_content_pos NUMBER := 1;
    ls_content VARCHAR2(4000);
    ls_record VARCHAR2(4000);
    ls_param device_setting.device_setting_parameter_cd%TYPE;
    ls_value VARCHAR2(4000);
    ln_return NUMBER := 0;
    ln_file_transfer_type_cd file_transfer.file_transfer_type_cd%TYPE;
	lv_file_transfer_name file_transfer.file_transfer_name%TYPE;
	ln_file_order device_setting.file_order%TYPE := 0;
	ln_config_template_id CONFIG_TEMPLATE.CONFIG_TEMPLATE_ID%TYPE;
	ln_device_id DEVICE.DEVICE_ID%TYPE;
BEGIN
    pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;
	
	SELECT MAX(dla.device_id)
	INTO ln_device_id
	FROM device.device d
	JOIN device.device_last_active dla ON d.device_name = dla.device_name
	WHERE d.device_id = pn_device_id;

    SELECT file_transfer_type_cd, file_transfer_content, file_transfer_name
    INTO ln_file_transfer_type_cd, ll_file_transfer_content, lv_file_transfer_name
    FROM device.file_transfer
    WHERE file_transfer_id = pn_file_transfer_id;

    IF ln_file_transfer_type_cd = PKG_CONST.FILE_TYPE__CONFIG THEN
        DELETE FROM device.device_setting
        WHERE device_id = ln_device_id
            AND device_setting_parameter_cd IN (
                SELECT device_setting_parameter_cd
                FROM device.device_setting_parameter
                WHERE device_setting_ui_configurable = 'Y');
    ELSE
		SELECT config_template_id
		INTO ln_config_template_id
		FROM device.config_template
		WHERE config_template_name = lv_file_transfer_name;
	
        DELETE FROM device.config_template_setting
        WHERE config_template_id = ln_config_template_id;
    END IF;

    LOOP
        ls_content := ls_content || utl_raw.cast_to_varchar2(HEXTORAW(DBMS_LOB.SUBSTR(ll_file_transfer_content, ln_content_size, ln_content_pos)));
        ln_content_pos := ln_content_pos + ln_content_size;

        ln_pos := NVL(INSTR(ls_content, PKG_CONST.ASCII__LF), 0);
        IF ln_pos = 0 OR NVL(INSTR(ls_content, '='), 0) = 0 THEN
            ln_return := 1;
        END IF;

        LOOP
            ln_pos := NVL(INSTR(ls_content, PKG_CONST.ASCII__LF), 0);
            IF ln_pos = 0 AND ln_return = 0 THEN
                EXIT;
            END IF;

            IF ln_return = 0 THEN
                ls_record := SUBSTR(ls_content, 1, ln_pos - 1);
                ls_content := SUBSTR(ls_content, ln_pos + 1);
            ELSE
                ls_record := ls_content;
            END IF;

            ln_pos := NVL(INSTR(ls_record, '='), 0);
            IF ln_pos = 0 AND ln_return = 0 THEN
                EXIT;
            END IF;

            ls_param := TRIM(SUBSTR(ls_record, 1, ln_pos - 1));
            ls_value := REPLACE(TRIM(SUBSTR(ls_record, ln_pos + 1, LENGTH(ls_record) - ln_pos)), CHR(13), '');
			IF LENGTH(ls_value) > 200 THEN
				ls_value := NULL;
			END IF;

            IF NVL(LENGTH(ls_param), 0) > 0 THEN
				BEGIN
					INSERT INTO device.device_setting_parameter(device_setting_parameter_cd)
					SELECT ls_param FROM dual
					WHERE NOT EXISTS (
						SELECT 1 FROM device.device_setting_parameter
						WHERE device_setting_parameter_cd = ls_param);
				EXCEPTION
					WHEN DUP_VAL_ON_INDEX THEN
						NULL;
				END;

				ln_file_order := ln_file_order + 1;
				
				BEGIN
					IF ln_file_transfer_type_cd = PKG_CONST.FILE_TYPE__CONFIG THEN
						INSERT INTO device.device_setting(device_id, device_setting_parameter_cd, device_setting_value, file_order)
						VALUES(ln_device_id, ls_param, ls_value, ln_file_order);
					ELSE
						INSERT INTO device.config_template_setting(config_template_id, device_setting_parameter_cd, config_template_setting_value, file_order)
						VALUES(ln_config_template_id, ls_param, ls_value, ln_file_order);
					END IF;
				EXCEPTION
					WHEN DUP_VAL_ON_INDEX THEN
						NULL;
				END;					
            END IF;

            IF ln_return = 1 THEN
                IF ln_file_transfer_type_cd = PKG_CONST.FILE_TYPE__CONFIG THEN
                    SELECT COUNT(1)
                    INTO pn_setting_count
                    FROM device.device_setting
                    WHERE device_id = ln_device_id
                        AND device_setting_parameter_cd IN (
                            SELECT device_setting_parameter_cd
                            FROM device.device_setting_parameter
                            WHERE device_setting_ui_configurable = 'Y');
                ELSE
                    SELECT COUNT(1)
                    INTO pn_setting_count
                    FROM device.config_template_setting
                    WHERE config_template_id = ln_config_template_id
                        AND device_setting_parameter_cd IN (
                            SELECT device_setting_parameter_cd
                            FROM device.device_setting_parameter
                            WHERE device_setting_ui_configurable = 'Y');
                END IF;

                pn_result_cd := PKG_CONST.RESULT__SUCCESS;
                pv_error_message := PKG_CONST.ERROR__NO_ERROR;
                RETURN;
            END IF;
        END LOOP;

    END LOOP;
END;

PROCEDURE SP_UPDATE_CFG_TMPL_SETTINGS
(
    pn_file_transfer_id IN file_transfer.file_transfer_id%TYPE,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_setting_count OUT NUMBER
)
IS
BEGIN
    SP_UPDATE_DEVICE_SETTINGS(0, pn_file_transfer_id, pn_result_cd, pv_error_message, pn_setting_count);
END;

PROCEDURE SP_GET_HOST_BY_PORT_NUM
(
    pv_device_name IN device.device_name%TYPE,
    pn_host_port_num IN host.host_port_num%TYPE,
    pt_utc_ts IN TIMESTAMP,
    pn_host_id OUT host.host_id%TYPE
)
IS
BEGIN
    SELECT host_id INTO pn_host_id FROM (
        SELECT host_id
        FROM device.host h
        INNER JOIN device.device d ON h.device_id = d.device_id
        WHERE d.device_name = pv_device_name
            AND h.host_port_num = pn_host_port_num
            AND SYS_EXTRACT_UTC(CAST(h.created_ts AS TIMESTAMP)) <= pt_utc_ts
        ORDER BY h.created_ts DESC
    ) WHERE ROWNUM = 1;
END;

PROCEDURE SP_GET_HOST
(
    pn_device_id IN device.device_id%TYPE,
    pv_device_name IN device.device_name%TYPE,
    pn_host_port_num IN host.host_port_num%TYPE,
    pt_utc_ts IN TIMESTAMP,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_host_id OUT host.host_id%TYPE
)
IS
    ln_new_host_count NUMBER;
	ln_device_id DEVICE.DEVICE_ID%TYPE;
BEGIN
	pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;

	SELECT MAX(dla.device_id)
	INTO ln_device_id
	FROM device.device d
	JOIN device.device_last_active dla ON d.device_name = dla.device_name
	WHERE d.device_id = pn_device_id;

    BEGIN
        sp_get_host_by_port_num(pv_device_name, pn_host_port_num, pt_utc_ts, pn_host_id);
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            BEGIN
                -- fail over to the base host
                sp_get_host_by_port_num(pv_device_name, 0, pt_utc_ts, pn_host_id);
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    BEGIN
                        sp_create_default_hosts(ln_device_id, ln_new_host_count, pn_result_cd, pv_error_message);
                        IF pn_result_cd != PKG_CONST.RESULT__SUCCESS THEN
                            RETURN;
                        END IF;

                        sp_get_host_by_port_num(pv_device_name, 0, SYS_EXTRACT_UTC(SYSTIMESTAMP), pn_host_id);
                    EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                            pn_result_cd := PKG_CONST.RESULT__HOST_NOT_FOUND;
                            pv_error_message := 'Unable to find host for device_name: ' || pv_device_name || ', host_port_num: ' || pn_host_port_num;
                            RETURN;
                    END;
            END;
    END;

    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;
            
PROCEDURE SP_CREATE_DEFAULT_HOSTS
(
    pn_device_id                            IN  device.device_id%TYPE,
    pn_new_host_count                       OUT NUMBER,
    pn_result_cd                            OUT NUMBER,
    pv_error_message                        OUT VARCHAR2
)
IS
    ln_base_host_count                      NUMBER;
    ln_other_host_count                     NUMBER;
    lv_device_serial_cd                     device.device_serial_cd%TYPE;
    ln_device_type_id                       device.device_type_id%TYPE;
    ln_def_base_host_type_id                device_type.def_base_host_type_id%TYPE;
	ln_def_base_host_equipment_id           device_type.def_base_host_equipment_id%TYPE;
	ln_def_prim_host_type_id                device_type.def_prim_host_type_id%TYPE;
	ln_def_prim_host_equipment_id           device_type.def_prim_host_equipment_id%TYPE;
	ln_device_id DEVICE.DEVICE_ID%TYPE;
BEGIN
    pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;
    pn_new_host_count := 0;
	
	SELECT MAX(dla.device_id)
	INTO ln_device_id
	FROM device.device d
	JOIN device.device_last_active dla ON d.device_name = dla.device_name
	WHERE d.device_id = pn_device_id;	

    SELECT NVL(SUM(CASE WHEN HTGT.HOST_TYPE_ID IS NOT NULL OR H.HOST_PORT_NUM = 0 THEN 1 ELSE 0 END), 0), NVL(SUM(CASE WHEN HTGT.HOST_TYPE_ID IS NOT NULL OR H.HOST_PORT_NUM = 0 THEN 0 ELSE 1 END), 0)
      INTO ln_base_host_count, ln_other_host_count
      FROM DEVICE.HOST H
      LEFT OUTER JOIN (DEVICE.HOST_TYPE_HOST_GROUP_TYPE HTGT   
      JOIN DEVICE.HOST_GROUP_TYPE GT ON HTGT.HOST_GROUP_TYPE_ID = GT.HOST_GROUP_TYPE_ID) 
        ON H.HOST_TYPE_ID = HTGT.HOST_TYPE_ID
       AND GT.HOST_GROUP_TYPE_CD = 'BASE_HOST'
     WHERE H.DEVICE_ID = H.DEVICE_ID
       AND H.DEVICE_ID = ln_device_id;

    -- if exists both a base and another host, then there's nothing to do here
    IF ln_base_host_count > 0 AND ln_other_host_count > 0 THEN
        pn_result_cd := PKG_CONST.RESULT__SUCCESS;
        pv_error_message := PKG_CONST.ERROR__NO_ERROR;
        RETURN;
    END IF;

    SELECT
        D.DEVICE_SERIAL_CD,
        D.DEVICE_TYPE_ID,
        DECODE(DST.DEVICE_SUB_TYPE_ID, NULL, DT.DEF_BASE_HOST_TYPE_ID, DST.DEF_BASE_HOST_TYPE_ID),
        DECODE(DST.DEVICE_SUB_TYPE_ID, NULL, DT.DEF_BASE_HOST_EQUIPMENT_ID, DST.DEF_BASE_HOST_EQUIPMENT_ID),
        DECODE(DST.DEVICE_SUB_TYPE_ID, NULL, DT.DEF_PRIM_HOST_TYPE_ID, DST.DEF_PRIM_HOST_TYPE_ID),
        DECODE(DST.DEVICE_SUB_TYPE_ID, NULL, DT.DEF_PRIM_HOST_EQUIPMENT_ID, DST.DEF_PRIM_HOST_EQUIPMENT_ID)
    INTO
        lv_device_serial_cd,
        ln_device_type_id,
        ln_def_base_host_type_id,
        ln_def_base_host_equipment_id,
        ln_def_prim_host_type_id,
        ln_def_prim_host_equipment_id
    FROM DEVICE.DEVICE D
    JOIN DEVICE.DEVICE_TYPE DT ON D.DEVICE_TYPE_ID = DT.DEVICE_TYPE_ID
    LEFT OUTER JOIN DEVICE.DEVICE_SUB_TYPE DST ON D.DEVICE_TYPE_ID = DST.DEVICE_TYPE_ID AND D.DEVICE_SUB_TYPE_ID = DST.DEVICE_SUB_TYPE_ID
    WHERE D.DEVICE_ID = ln_device_id;

    -- Create base host if it doesn't exist
    IF ln_base_host_count = 0 AND ln_def_base_host_type_id IS NOT NULL THEN
        INSERT INTO device.host
        (
            host_status_cd,
            host_serial_cd,
            host_est_complete_minut,
            host_port_num,
            host_setting_updated_yn_flag,
            device_id,
            host_position_num,
            host_active_yn_flag,
            host_type_id,
            host_equipment_id
        )
        VALUES
        (
            0,
            lv_device_serial_cd,
            0,
            0,
            'N',
            ln_device_id,
            0,
            'Y',
            ln_def_base_host_type_id,
            ln_def_base_host_equipment_id
        );

        pn_new_host_count := pn_new_host_count + 1;
    END IF;

    -- Create primary host if no other hosts exist
    IF ln_other_host_count = 0 AND ln_def_prim_host_type_id IS NOT NULL THEN
        INSERT INTO device.host
        (
            host_status_cd,
            host_est_complete_minut,
            host_port_num,
            host_setting_updated_yn_flag,
            device_id,
            host_position_num,
            host_active_yn_flag,
            host_type_id,
            host_equipment_id
        )
        VALUES
        (
            0,
            0,
            1,
            'N',
            ln_device_id,
            0,
            'Y',
            ln_def_prim_host_type_id,
            ln_def_prim_host_equipment_id
        );

        pn_new_host_count := pn_new_host_count + 1;
    END IF;

    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;

    FUNCTION GET_OR_CREATE_HOST_EQUIPMENT(
        pv_host_equipment_mfgr IN HOST_EQUIPMENT.HOST_EQUIPMENT_MFGR%TYPE,
    	pv_host_equipment_model IN HOST_EQUIPMENT.HOST_EQUIPMENT_MODEL%TYPE,
        pn_existing_cnt OUT PLS_INTEGER)
        RETURN HOST_EQUIPMENT.HOST_EQUIPMENT_ID%TYPE
    IS
        ln_host_equipment_id HOST_EQUIPMENT.HOST_EQUIPMENT_ID%TYPE;
    BEGIN
        SELECT MAX(HOST_EQUIPMENT_ID), 1
          INTO ln_host_equipment_id, pn_existing_cnt
          FROM DEVICE.HOST_EQUIPMENT
         WHERE HOST_EQUIPMENT_MFGR = pv_host_equipment_mfgr
           AND HOST_EQUIPMENT_MODEL = pv_host_equipment_model;
        IF ln_host_equipment_id IS NULL THEN
            SELECT SEQ_HOST_EQUIPMENT_ID.NEXTVAL, 0
              INTO ln_host_equipment_id, pn_existing_cnt
              FROM DUAL;
            INSERT INTO DEVICE.HOST_EQUIPMENT(HOST_EQUIPMENT_ID, HOST_EQUIPMENT_MFGR, HOST_EQUIPMENT_MODEL)
                 VALUES(ln_host_equipment_id, pv_host_equipment_mfgr, pv_host_equipment_model);
        END IF;
        RETURN ln_host_equipment_id;
    EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
            RETURN GET_OR_CREATE_HOST_EQUIPMENT(pv_host_equipment_mfgr, pv_host_equipment_model, pn_existing_cnt);
    END;
        
    PROCEDURE UPSERT_HOST(
        pn_device_id IN DEVICE.DEVICE_ID%TYPE,
        pn_host_port_num IN HOST.HOST_PORT_NUM%TYPE,
        pn_host_position_num IN HOST.HOST_POSITION_NUM%TYPE,
        pn_host_type_id IN HOST.HOST_TYPE_ID%TYPE,
        pv_host_serial_cd IN HOST.HOST_SERIAL_CD%TYPE,
    	pv_host_label_cd IN HOST.HOST_LABEL_CD%TYPE,
    	pn_host_equipment_id HOST_EQUIPMENT.HOST_EQUIPMENT_ID%TYPE,
        pn_host_est_complete_minut IN HOST.HOST_EST_COMPLETE_MINUT%TYPE,
        pn_host_id OUT HOST.HOST_ID%TYPE,
        pn_existing_cnt OUT PLS_INTEGER)
    IS
		ln_device_id DEVICE.DEVICE_ID%TYPE;
    BEGIN
		SELECT MAX(dla.device_id)
		INTO ln_device_id
		FROM device.device d
		JOIN device.device_last_active dla ON d.device_name = dla.device_name
		WHERE d.device_id = pn_device_id;	
	
        UPDATE DEVICE.HOST
           SET (HOST_TYPE_ID, HOST_SERIAL_CD, HOST_LABEL_CD, HOST_EQUIPMENT_ID, HOST_EST_COMPLETE_MINUT, HOST_ACTIVE_YN_FLAG) =
               (SELECT pn_host_type_id, pv_host_serial_cd, pv_host_label_cd, pn_host_equipment_id, pn_host_est_complete_minut, 'Y' FROM DUAL)
         WHERE DEVICE_ID = ln_device_id
           AND HOST_PORT_NUM = pn_host_port_num
           AND HOST_POSITION_NUM = pn_host_position_num
           RETURNING HOST_ID, 1 INTO pn_host_id, pn_existing_cnt;
        IF pn_host_id IS NULL THEN
            SELECT SEQ_HOST_ID.NEXTVAL, 0
              INTO pn_host_id, pn_existing_cnt
              FROM DUAL;
            INSERT INTO DEVICE.HOST(HOST_ID, DEVICE_ID, HOST_PORT_NUM, HOST_POSITION_NUM, HOST_TYPE_ID, HOST_SERIAL_CD, HOST_LABEL_CD, HOST_EQUIPMENT_ID, HOST_EST_COMPLETE_MINUT, HOST_SETTING_UPDATED_YN_FLAG, HOST_ACTIVE_YN_FLAG)
                 VALUES(pn_host_id, ln_device_id, pn_host_port_num, pn_host_position_num, pn_host_type_id, pv_host_serial_cd, pv_host_label_cd, pn_host_equipment_id, pn_host_est_complete_minut, 'N', 'Y');	        		
       END IF;
    EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
            UPSERT_HOST(pn_device_id, pn_host_port_num, pn_host_position_num, pn_host_type_id, pv_host_serial_cd, pv_host_label_cd, pn_host_equipment_id, pn_host_est_complete_minut, pn_host_id, pn_existing_cnt);
    END;
    
    PROCEDURE UPSERT_HOST(
        pn_device_id IN DEVICE.DEVICE_ID%TYPE,
        pn_host_port_num IN HOST.HOST_PORT_NUM%TYPE,
        pn_host_position_num IN HOST.HOST_POSITION_NUM%TYPE,
        pn_host_type_id IN HOST.HOST_TYPE_ID%TYPE,
        pv_host_serial_cd IN HOST.HOST_SERIAL_CD%TYPE,
    	pv_host_label_cd IN HOST.HOST_LABEL_CD%TYPE,
    	pv_host_equipment_mfgr IN HOST_EQUIPMENT.HOST_EQUIPMENT_MFGR%TYPE,
    	pv_host_equipment_model IN HOST_EQUIPMENT.HOST_EQUIPMENT_MODEL%TYPE,
        pn_host_est_complete_minut IN HOST.HOST_EST_COMPLETE_MINUT%TYPE,
        pn_host_id OUT HOST.HOST_ID%TYPE,
        pn_existing_cnt OUT PLS_INTEGER)
    IS
        ln_host_equipment_existing_cnt PLS_INTEGER;
    BEGIN
        UPSERT_HOST(pn_device_id, pn_host_port_num, pn_host_position_num, pn_host_type_id, pv_host_serial_cd, pv_host_label_cd, GET_OR_CREATE_HOST_EQUIPMENT(pv_host_equipment_mfgr, pv_host_equipment_model, ln_host_equipment_existing_cnt), pn_host_est_complete_minut, pn_host_id, pn_existing_cnt);
    END;

    PROCEDURE UPSERT_HOST_SETTING(
		pn_host_id HOST.HOST_ID%TYPE,
		pv_host_setting_parameter_cd HOST_SETTING.HOST_SETTING_PARAMETER%TYPE,
		pv_host_setting_value HOST_SETTING.HOST_SETTING_VALUE%TYPE)
    IS
    BEGIN
       UPDATE DEVICE.HOST_SETTING
           SET HOST_SETTING_VALUE = pv_host_setting_value
         WHERE HOST_ID = pn_host_id 
           AND HOST_SETTING_PARAMETER = pv_host_setting_parameter_cd;
	    IF SQL%NOTFOUND THEN
            INSERT INTO DEVICE.HOST_SETTING(HOST_ID, HOST_SETTING_PARAMETER, HOST_SETTING_VALUE)
                VALUES(pn_host_id, pv_host_setting_parameter_cd, pv_host_setting_value);
        END IF;
    EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
            UPSERT_HOST_SETTING(pn_host_id, pv_host_setting_parameter_cd, pv_host_setting_value);
    END;
    
    PROCEDURE UPSERT_HOST_SETTING(
		pn_host_id HOST.HOST_ID%TYPE,
		pv_host_setting_parameter_cd HOST_SETTING.HOST_SETTING_PARAMETER%TYPE,
		pv_host_setting_value HOST_SETTING.HOST_SETTING_VALUE%TYPE,
        pv_old_host_setting_value OUT HOST_SETTING.HOST_SETTING_VALUE%TYPE)
    IS
    BEGIN
        SELECT MAX(HOST_SETTING_VALUE)
          INTO pv_old_host_setting_value
          FROM DEVICE.HOST_SETTING
         WHERE HOST_ID = pn_host_id 
           AND HOST_SETTING_PARAMETER = pv_host_setting_parameter_cd;         
        UPSERT_HOST_SETTING(pn_host_id, pv_host_setting_parameter_cd, pv_host_setting_value);
    END;
    
     PROCEDURE UPDATE_COMM_STATS(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pn_rssi NUMBER,
		pn_ber NUMBER,
        pd_update_ts GPRS_DEVICE.RSSI_TS%TYPE,
        pv_modem_info GPRS_DEVICE.MODEM_INFO%TYPE)
    IS
        ln_host_id HOST.HOST_ID%TYPE;
		ln_device_id DEVICE.DEVICE_ID%TYPE;	
        lv_device_name DEVICE.DEVICE_NAME%TYPE;	
    BEGIN
		SELECT MAX(dla.device_id), MAX(D.DEVICE_NAME)
		INTO ln_device_id, lv_device_name
		FROM device.device d
		JOIN device.device_last_active dla ON d.device_name = dla.device_name
		WHERE d.device_id = pn_device_id;
	
        UPDATE DEVICE.DEVICE_DATA
           SET RSSI = pn_rssi,
               RSSI_TS = pd_update_ts
         WHERE DEVICE_NAME = lv_device_name
           AND RSSI_TS < pd_update_ts;
	
        IF SQL%NOTFOUND THEN
            BEGIN
                INSERT INTO DEVICE.DEVICE_DATA(DEVICE_NAME, RSSI, RSSI_TS)
                    VALUES(lv_device_name, pn_rssi, pd_update_ts);
            EXCEPTION
                WHEN DUP_VAL_ON_INDEX THEN
                    NULL;
            END;
        END IF;      
        ENGINE.PKG_DATA_SYNC.SP_CREATE_DATA_SYNC('DEVICE_INFO', 'DEVICE.DEVICE_SETTING', 'U', pn_device_id, lv_device_name, NULL);
        
		UPDATE DEVICE.GPRS_DEVICE GD
		SET DEVICE_ID = NULL,
			GPRS_DEVICE_STATE_ID = DECODE(GPRS_DEVICE_STATE_ID, 5, 4, GPRS_DEVICE_STATE_ID)
		WHERE DEVICE_ID = ln_device_id
			AND ICCID NOT IN(SELECT TO_NUMBER_OR_NULL(H.HOST_SERIAL_CD) 
          FROM DEVICE.HOST H 
          JOIN DEVICE.HOST_TYPE_HOST_GROUP_TYPE HTGT ON H.HOST_TYPE_ID = HTGT.HOST_TYPE_ID
         WHERE DEVICE_ID = GD.DEVICE_ID 
           AND HTGT.HOST_GROUP_TYPE_ID = 10 /*MODEM*/);
	
		UPDATE DEVICE.GPRS_DEVICE GD
		   SET RSSI = pn_rssi || ',' || pn_ber, 
               RSSI_TS = pd_update_ts, 
               LAST_FILE_TRANSFER_ID = NULL, 
               MODEM_INFO_RECEIVED_TS = SYSDATE, 
               MODEM_INFO = pv_modem_info,
		   	   DEVICE_ID = ln_device_id, 
               GPRS_DEVICE_STATE_ID = 5,
		       ASSIGNED_BY = DECODE(DEVICE_ID, ln_device_id, ASSIGNED_BY, 'APP_LAYER'), 
		   	   ASSIGNED_TS = DECODE(DEVICE_ID, ln_device_id, ASSIGNED_TS, SYSDATE)
		 WHERE ICCID IN(SELECT TO_NUMBER_OR_NULL(H.HOST_SERIAL_CD) 
          FROM DEVICE.HOST H 
          JOIN DEVICE.HOST_TYPE_HOST_GROUP_TYPE HTGT ON H.HOST_TYPE_ID = HTGT.HOST_TYPE_ID
         WHERE H.DEVICE_ID = ln_device_id 
           AND HTGT.HOST_GROUP_TYPE_ID = 10 /*MODEM*/)
		   AND (RSSI_TS IS NULL OR RSSI_TS < pd_update_ts);
        SELECT MAX(H.HOST_ID)
          INTO ln_host_id
          FROM DEVICE.HOST H
          JOIN DEVICE.HOST_TYPE_HOST_GROUP_TYPE HTGT ON H.HOST_TYPE_ID = HTGT.HOST_TYPE_ID
         WHERE DEVICE_ID = ln_device_id
           AND HTGT.HOST_GROUP_TYPE_ID = 10 /*MODEM*/;
        IF ln_host_id IS NOT NULL THEN
            UPSERT_HOST_SETTING(ln_host_id,'CSQ',pn_rssi || ',' || pn_ber);
        END IF;
	END;

	-- SP_CONSTRUCT_PROPERTIES_FILE is deprecated in R30
    PROCEDURE SP_CONSTRUCT_PROPERTIES_FILE(
        pn_device_id IN device.device_id%TYPE,
        pl_file OUT CLOB)
    IS
    BEGIN
        NULL;
    END;

	-- SP_UPDATE_DEVICE_CONFIG_FILE is deprecated in R30
    PROCEDURE SP_UPDATE_DEVICE_CONFIG_FILE(
        pn_device_id DEVICE.DEVICE_ID%TYPE,
        pl_config_file LONG)
    IS
    BEGIN
		NULL;
    END;

    PROCEDURE SP_INITIALIZE_CONFIG_FILE(
        pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
        pn_new_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
        pn_new_property_list_version NUMBER,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pn_updated OUT NUMBER,
        pn_old_property_list_version OUT NUMBER)
    IS
        ln_device_id DEVICE.DEVICE_ID%TYPE;
        lv_default_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE;
        lv_old_default_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE;
		lv_last_lock_utc_ts VARCHAR2(128);
     BEGIN
		lv_last_lock_utc_ts := PKG_GLOBAL.REQUEST_LOCK('DEVICE.DEVICE', pv_device_name);
	 
        SELECT MAX(DEVICE_ID)
          INTO ln_device_id
          FROM (SELECT DEVICE_ID
             FROM DEVICE.DEVICE
            WHERE DEVICE_NAME = pv_device_name
            ORDER BY DEVICE_ACTIVE_YN_FLAG DESC, CREATED_TS DESC)
         WHERE ROWNUM = 1;

		IF ln_device_id IS NULL THEN
			RETURN;
		END IF;
		
		SELECT DECODE(DBADMIN.PKG_UTL.COMPARE(pn_new_device_type_id, 13), 
			-1, default_config_template_name, default_config_template_name || pn_new_property_list_version)
		INTO lv_default_name
		FROM device.device_type
		WHERE device_type_id = pn_new_device_type_id;
		
		INSERT INTO DEVICE.DEVICE_SETTING(DEVICE_ID, DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_VALUE)
		SELECT ln_device_id, CTS.DEVICE_SETTING_PARAMETER_CD, CTS.CONFIG_TEMPLATE_SETTING_VALUE
		FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS
		JOIN DEVICE.CONFIG_TEMPLATE CT ON CTS.CONFIG_TEMPLATE_ID = CT.CONFIG_TEMPLATE_ID
		WHERE CT.CONFIG_TEMPLATE_NAME = lv_default_name AND NOT EXISTS (
			SELECT 1 FROM DEVICE.DEVICE_SETTING
			WHERE DEVICE_ID = ln_device_id
				AND DEVICE_SETTING_PARAMETER_CD = CTS.DEVICE_SETTING_PARAMETER_CD
		);
		
		IF pn_new_device_type_id IN (0, 1) THEN
			-- set Call Home Now Flag to N
			UPDATE DEVICE.DEVICE_SETTING
			SET DEVICE_SETTING_VALUE = '4E'
			WHERE DEVICE_ID = ln_device_id
				AND DEVICE_SETTING_PARAMETER_CD = '210'
				AND DBADMIN.PKG_UTL.EQL(DEVICE_SETTING_VALUE, '4E') = 'N';
		END IF;
	 
		IF pn_new_device_type_id IN (0, 1, 6) THEN
			RETURN;
		END IF;

		SELECT MAX(TO_NUMBER(ds.DEVICE_SETTING_VALUE))
		  INTO pn_old_property_list_version
		  FROM DEVICE.DEVICE_SETTING ds
		 WHERE ds.DEVICE_ID = ln_device_id
		   AND ds.DEVICE_SETTING_PARAMETER_CD = PKG_CONST.DSP__PROPERTY_LIST_VERSION
		   AND REGEXP_LIKE(ds.DEVICE_SETTING_VALUE, '^[0-9]+$');
		IF pn_new_property_list_version IS NOT NULL AND pn_new_property_list_version <> NVL(pn_old_property_list_version, -1) THEN
			SP_UPDATE_DEVICE_SETTING(ln_device_id, PKG_CONST.DSP__PROPERTY_LIST_VERSION, pn_new_property_list_version);
			
		   -- Update all values that equal the default in the old version and where the default in the new version is differnt
		   lv_old_default_name := 'DEFAULT-CFG-' || pn_new_device_type_id || '-' || pn_old_property_list_version;
		   MERGE INTO DEVICE.DEVICE_SETTING O
			 USING (
				   SELECT ln_device_id DEVICE_ID, cts.DEVICE_SETTING_PARAMETER_CD SETTING_NAME, cts.CONFIG_TEMPLATE_SETTING_VALUE SETTING_VALUE
					FROM DEVICE.CONFIG_TEMPLATE ct
					JOIN DEVICE.CONFIG_TEMPLATE_SETTING cts
					  ON cts.CONFIG_TEMPLATE_ID = ct.CONFIG_TEMPLATE_ID
					JOIN DEVICE.CONFIG_TEMPLATE_SETTING cts0
					  ON cts0.DEVICE_SETTING_PARAMETER_CD = cts.DEVICE_SETTING_PARAMETER_CD
					JOIN DEVICE.CONFIG_TEMPLATE ct0
					  ON cts0.CONFIG_TEMPLATE_ID = ct0.CONFIG_TEMPLATE_ID
					JOIN DEVICE.DEVICE_SETTING ds
					  ON cts.DEVICE_SETTING_PARAMETER_CD = ds.DEVICE_SETTING_PARAMETER_CD
				   WHERE cts.CONFIG_TEMPLATE_SETTING_VALUE != cts0.CONFIG_TEMPLATE_SETTING_VALUE
					 AND cts0.CONFIG_TEMPLATE_SETTING_VALUE = ds.DEVICE_SETTING_VALUE
					 AND cts.DEVICE_SETTING_PARAMETER_CD NOT IN('50','51','52','60','61','62','63','64','80','81','70','100','101','102','103','104','105','106','107','108','200','201','202','203','204','205','206','207','208','300','301')
					 AND TO_NUMBER_OR_NULL(cts.DEVICE_SETTING_PARAMETER_CD) IS NOT NULL
					 AND ct.CONFIG_TEMPLATE_NAME = lv_default_name
					 AND ct0.CONFIG_TEMPLATE_NAME = lv_old_default_name
					 AND ds.DEVICE_ID = ln_device_id) N
				  ON (O.DEVICE_ID = N.DEVICE_ID AND O.DEVICE_SETTING_PARAMETER_CD = N.SETTING_NAME)
				  WHEN MATCHED THEN
				   UPDATE
					  SET O.DEVICE_SETTING_VALUE = N.SETTING_VALUE
				  WHEN NOT MATCHED THEN
				   INSERT (O.DEVICE_ID,
						   O.DEVICE_SETTING_PARAMETER_CD,
						   O.DEVICE_SETTING_VALUE)
					VALUES(N.DEVICE_ID,
						   N.SETTING_NAME,
						   N.SETTING_VALUE
					);
		END IF;

        pn_updated := PKG_CONST.BOOLEAN__TRUE;
    END;

    PROCEDURE SP_INITIALIZE_CONFIG_FILE(
        pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
        pn_new_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
        pn_new_property_list_version NUMBER,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pn_updated OUT NUMBER)
    IS
        ln_old_property_list_version NUMBER;
    BEGIN
        SP_INITIALIZE_CONFIG_FILE(pv_device_name, pn_new_device_type_id, pn_new_property_list_version, pn_file_transfer_id, pn_updated, ln_old_property_list_version);
    END;

    PROCEDURE ADD_PENDING_FILE_TRANSFER(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_id FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pc_data_type ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pn_file_transfer_group DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
        pn_file_transfer_packet_size DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
        pn_execute_order ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE
        )
    IS
        ln_device_file_transfer_id ENGINE.MACHINE_CMD_PENDING.DEVICE_FILE_TRANSFER_ID%TYPE;
    BEGIN
      ADD_PENDING_FILE_TRANSFER(
        pv_device_name,pv_file_transfer_name,pn_file_transfer_type_id,
        pc_data_type,pn_file_transfer_group,pn_file_transfer_packet_size,pn_execute_order,pn_command_id, pn_file_transfer_id, ln_device_file_transfer_id);
        
    END;
    
    PROCEDURE ADD_PENDING_FILE_TRANSFER(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_id FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pc_data_type ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pn_file_transfer_group DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
        pn_file_transfer_packet_size DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
        pn_execute_order ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pn_device_file_transfer_id OUT ENGINE.MACHINE_CMD_PENDING.DEVICE_FILE_TRANSFER_ID%TYPE
        )
    IS
        ln_device_id DEVICE_FILE_TRANSFER.DEVICE_ID%TYPE;
    BEGIN
        SELECT SEQ_FILE_TRANSFER_ID.NEXTVAL, SEQ_DEVICE_FILE_TRANSFER_ID.NEXTVAL, ENGINE.SEQ_MACHINE_CMD_PENDING_ID.NEXTVAL, DEVICE_ID
          INTO pn_file_transfer_id, pn_device_file_transfer_id, pn_command_id, ln_device_id
          FROM (SELECT DEVICE_ID
          FROM DEVICE.DEVICE
         WHERE DEVICE_NAME = pv_device_name
         ORDER BY DEVICE_ACTIVE_YN_FLAG DESC, DEVICE_ID DESC)
         WHERE ROWNUM = 1;
        INSERT INTO DEVICE.FILE_TRANSFER(FILE_TRANSFER_ID, FILE_TRANSFER_TYPE_CD, FILE_TRANSFER_NAME)
           VALUES(pn_file_transfer_id, pn_file_transfer_type_id, pv_file_transfer_name);
        INSERT INTO DEVICE.DEVICE_FILE_TRANSFER(DEVICE_FILE_TRANSFER_ID, DEVICE_ID, FILE_TRANSFER_ID, DEVICE_FILE_TRANSFER_DIRECT, DEVICE_FILE_TRANSFER_STATUS_CD, DEVICE_FILE_TRANSFER_GROUP_NUM, DEVICE_FILE_TRANSFER_PKT_SIZE)
            VALUES(pn_device_file_transfer_id, ln_device_id, pn_file_transfer_id, 'O', 0, pn_file_transfer_group, pn_file_transfer_packet_size);
        INSERT INTO ENGINE.MACHINE_CMD_PENDING(MACHINE_COMMAND_PENDING_ID, MACHINE_ID, DATA_TYPE, DEVICE_FILE_TRANSFER_ID, EXECUTE_CD, EXECUTE_ORDER, EXECUTE_DATE)
           VALUES(pn_command_id, pv_device_name, pc_data_type, pn_device_file_transfer_id, 'S', NVL(pn_execute_order, 999), SYSDATE);
    END;

	-- R30+
    PROCEDURE REQUEST_OUTBOUND_FILE_TRANSFER(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_id FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pc_data_type ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pn_file_transfer_group DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
        pn_file_transfer_packet_size DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
        pl_file_transfer_content OUT FILE_TRANSFER.FILE_TRANSFER_CONTENT%TYPE,
        pd_file_transfer_create_ts OUT FILE_TRANSFER.CREATED_TS%TYPE,
        pn_mcp_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
		pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE)
    IS
        l_dft_id DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_ID%TYPE;
        l_device_id DEVICE_FILE_TRANSFER.DEVICE_ID%TYPE;
    BEGIN
        SELECT FILE_TRANSFER_ID, CREATED_TS
        INTO pn_file_transfer_id, pd_file_transfer_create_ts
        FROM (
            SELECT FILE_TRANSFER_ID, CREATED_TS
            FROM DEVICE.FILE_TRANSFER
            WHERE FILE_TRANSFER_NAME = pv_file_transfer_name
                AND FILE_TRANSFER_TYPE_CD = pn_file_transfer_type_id
            ORDER BY CREATED_TS
        ) WHERE ROWNUM = 1;
        SELECT MAX(dft.DEVICE_FILE_TRANSFER_ID)
          INTO l_dft_id
          FROM DEVICE.DEVICE_FILE_TRANSFER dft
          JOIN DEVICE.DEVICE d ON dft.DEVICE_ID = d.DEVICE_ID
         WHERE d.DEVICE_NAME = pv_device_name
           AND dft.FILE_TRANSFER_ID = pn_file_transfer_id
           AND dft.DEVICE_FILE_TRANSFER_DIRECT = 'O'
           AND dft.DEVICE_FILE_TRANSFER_STATUS_CD = 1;
       IF l_dft_id IS NOT NULL THEN -- already exists, find pending command
         SELECT MAX(MACHINE_COMMAND_PENDING_ID) -- avoid NO_DATA_FOUND
           INTO pn_mcp_id
           FROM ENGINE.MACHINE_CMD_PENDING
          WHERE MACHINE_ID = pv_device_name
            AND DEVICE_FILE_TRANSFER_ID = l_dft_id;
        ELSE
            SELECT SEQ_DEVICE_FILE_TRANSFER_ID.NEXTVAL, DEVICE_ID
              INTO l_dft_id, l_device_id
              FROM (SELECT DEVICE_ID
              FROM DEVICE.DEVICE
             WHERE DEVICE_NAME = pv_device_name
             ORDER BY DEVICE_ACTIVE_YN_FLAG DESC, DEVICE_ID DESC)
             WHERE ROWNUM = 1;
            INSERT INTO DEVICE.DEVICE_FILE_TRANSFER(DEVICE_FILE_TRANSFER_ID, DEVICE_ID, FILE_TRANSFER_ID, DEVICE_FILE_TRANSFER_DIRECT, DEVICE_FILE_TRANSFER_STATUS_CD, DEVICE_FILE_TRANSFER_GROUP_NUM, DEVICE_FILE_TRANSFER_PKT_SIZE)
              VALUES(l_dft_id, l_device_id, pn_file_transfer_id, 'O', 0, pn_file_transfer_group, pn_file_transfer_packet_size);
        END IF;
        IF pn_mcp_id IS NULL THEN
            SELECT ENGINE.SEQ_MACHINE_CMD_PENDING_ID.NEXTVAL
              INTO pn_mcp_id
              FROM DUAL;
            INSERT INTO ENGINE.MACHINE_CMD_PENDING(MACHINE_COMMAND_PENDING_ID, MACHINE_ID, DATA_TYPE, DEVICE_FILE_TRANSFER_ID, EXECUTE_CD, EXECUTE_ORDER, EXECUTE_DATE)
              VALUES(pn_mcp_id, pv_device_name, pc_data_type, l_dft_id, 'S', 999, SYSDATE);
        END IF;
        SP_GET_FILE_TRANSFER_BLOB(pn_file_transfer_id, pl_file_transfer_content);
    EXCEPTION
       WHEN NO_DATA_FOUND THEN
               NULL; -- let variables be null
    END;
	
	-- R29
	PROCEDURE REQUEST_OUTBOUND_FILE_TRANSFER(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_id FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pc_data_type ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pn_file_transfer_group DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
        pn_file_transfer_packet_size DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
        pl_file_transfer_content OUT FILE_TRANSFER.FILE_TRANSFER_CONTENT%TYPE,
        pd_file_transfer_create_ts OUT FILE_TRANSFER.CREATED_TS%TYPE,
        pn_mcp_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE)
	IS
		ln_file_transfer_id FILE_TRANSFER.FILE_TRANSFER_ID%TYPE;
	BEGIN
		REQUEST_OUTBOUND_FILE_TRANSFER(
        pv_device_name,
        pv_file_transfer_name,
        pn_file_transfer_type_id,
        pc_data_type,
        pn_file_transfer_group,
        pn_file_transfer_packet_size,
        pl_file_transfer_content,
        pd_file_transfer_create_ts,
        pn_mcp_id,
		ln_file_transfer_id);
	END;
	
	-- R33+ signature
    PROCEDURE SP_RECORD_FILE_TRANSFER(
        pv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_cd FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pv_event_global_trans_cd VARCHAR2,
        pc_overwrite_flag CHAR,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pd_file_transfer_ts DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_TS%TYPE DEFAULT SYSDATE,
        pv_global_session_cd VARCHAR2
    )
    IS
        ln_device_file_transfer_id DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_ID%TYPE;
        ln_device_id DEVICE_FILE_TRANSFER.DEVICE_ID%TYPE;
    BEGIN
		ln_device_id := GET_DEVICE_ID_BY_NAME(pv_device_name, pd_file_transfer_ts);
		
		-- duplicate detection of already stored files re-delivered by QueueLayer
		SELECT /*+INDEX(DFT IDX_DEV_FILE_XFER_GLOB_SESS_CD)*/ MAX(DEVICE_FILE_TRANSFER_ID), MAX(FILE_TRANSFER_ID)
		INTO ln_device_file_transfer_id, pn_file_transfer_id
		FROM DEVICE.DEVICE_FILE_TRANSFER DFT
		WHERE GLOBAL_SESSION_CD = pv_global_session_cd
			AND DEVICE_ID = ln_device_id
			AND DEVICE_FILE_TRANSFER_TS = pd_file_transfer_ts;
		IF ln_device_file_transfer_id IS NOT NULL THEN
			RETURN;
		END IF;
		
        BEGIN
            SELECT FILE_TRANSFER_ID
              INTO pn_file_transfer_id
              FROM (SELECT /*+ index(FT INX_FILE_TRANSFER_TYPE_NAME) index(DFT IDX_DEVICE_FILE_TRANSFER_ID) */ ft.FILE_TRANSFER_ID
              FROM DEVICE.FILE_TRANSFER ft
              LEFT JOIN DEVICE.DEVICE_FILE_TRANSFER dft on ft.FILE_TRANSFER_ID = dft.FILE_TRANSFER_ID
             WHERE ft.FILE_TRANSFER_NAME = pv_file_transfer_name
               AND ft.FILE_TRANSFER_TYPE_CD = pn_file_transfer_type_cd
               AND NVL(dft.DEVICE_FILE_TRANSFER_DIRECT, 'I') = 'I'
               AND NVL(dft.DEVICE_FILE_TRANSFER_STATUS_CD, 1) = 1
               AND pc_overwrite_flag = 'Y'
             ORDER BY ft.FILE_TRANSFER_ID DESC)
             WHERE ROWNUM = 1;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                SELECT SEQ_FILE_TRANSFER_ID.NEXTVAL
                  INTO pn_file_transfer_id
                  FROM DUAL;
                INSERT INTO DEVICE.FILE_TRANSFER (
                    FILE_TRANSFER_ID,
                    FILE_TRANSFER_NAME,
                    FILE_TRANSFER_TYPE_CD)
                  VALUES(
                    pn_file_transfer_id,
                    pv_file_transfer_name,
                    pn_file_transfer_type_cd);
        END;
        SELECT SEQ_DEVICE_FILE_TRANSFER_ID.NEXTVAL
          INTO ln_device_file_transfer_id
          FROM DUAL;
        INSERT INTO DEVICE.DEVICE_FILE_TRANSFER(
            DEVICE_FILE_TRANSFER_ID,
            DEVICE_ID,
            FILE_TRANSFER_ID,
            DEVICE_FILE_TRANSFER_DIRECT,
            DEVICE_FILE_TRANSFER_STATUS_CD,
            DEVICE_FILE_TRANSFER_TS,
            GLOBAL_SESSION_CD)
         VALUES(
            ln_device_file_transfer_id,
            ln_device_id,
            pn_file_transfer_id,
            'I',
            1,
            pd_file_transfer_ts,
            pv_global_session_cd);
        IF pv_event_global_trans_cd IS NOT NULL THEN
            INSERT INTO DEVICE.DEVICE_FILE_TRANSFER_EVENT(EVENT_ID, DEVICE_FILE_TRANSFER_ID)
              SELECT EVENT_ID, ln_device_file_transfer_id
                FROM DEVICE.EVENT
                WHERE EVENT_GLOBAL_TRANS_CD = pv_event_global_trans_cd;
        END IF;
		
		-- mark any pending file transfer requests for the same file name as complete
		UPDATE engine.machine_cmd_pending
		SET execute_cd = 'A', global_session_cd = pv_global_session_cd
		WHERE machine_id = pv_device_name
			AND data_type = '9B'
			AND command = UPPER(RAWTOHEX(pv_file_transfer_name));
    END;

	FUNCTION GET_NORMALIZED_TIME(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
		pv_normalizer_start_min_cd ENGINE.APP_SETTING.APP_SETTING_CD%TYPE,
		pv_normalizer_end_min_cd ENGINE.APP_SETTING.APP_SETTING_CD%TYPE,
		pv_window_start_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_window_hours_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_device_time_zone_guid VARCHAR2
	) RETURN VARCHAR2
	IS
		ln_call_in_start_min NUMBER;
		ln_call_in_end_min NUMBER;
		ln_call_in_window_min NUMBER;
		ln_call_in_time_min NUMBER;
		ln_call_in_window_start NUMBER;
		ln_call_in_window_hours NUMBER;
		ln_seed_4 NUMBER := NVL(DBADMIN.TO_NUMBER_OR_NULL(SUBSTR(pv_device_serial_cd, -4)), 0);
		ln_convert_time_zone NUMBER := PKG_CONST.BOOLEAN__TRUE;
	BEGIN
		ln_call_in_window_start := ROUND(NVL(DBADMIN.TO_NUMBER_OR_NULL(GET_DEVICE_SETTING(pn_device_id, pv_window_start_cd)), -1));
		ln_call_in_window_hours := ROUND(NVL(DBADMIN.TO_NUMBER_OR_NULL(GET_DEVICE_SETTING(pn_device_id, pv_window_hours_cd)), 0));
		IF ln_call_in_window_start BETWEEN 0 AND 2359 AND ln_call_in_window_hours BETWEEN 1 AND 23 THEN
			ln_call_in_start_min := FLOOR(ln_call_in_window_start / 100) * 60 + MOD(ln_call_in_window_start, 100);
			ln_call_in_window_min := ln_call_in_window_hours * 60;
			ln_convert_time_zone := PKG_CONST.BOOLEAN__FALSE;
		ELSIF pv_normalizer_start_min_cd IS NULL OR pv_normalizer_end_min_cd IS NULL THEN
			RETURN NULL;
		ELSE
			ln_call_in_start_min := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING(pv_normalizer_start_min_cd));
			ln_call_in_end_min := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING(pv_normalizer_end_min_cd));
			
			IF ln_call_in_start_min < 0 OR ln_call_in_start_min = ln_call_in_end_min THEN
				RETURN NULL;
			END IF;

			ln_call_in_window_min := MOD(ln_call_in_end_min - ln_call_in_start_min, 1440);
			IF ln_call_in_window_min < 0 THEN
				ln_call_in_window_min := ln_call_in_window_min + 1440;
			END IF;
		END IF;
			
		-- use last 4 digits of device serial number to calculate its call-in time
		ln_call_in_time_min := MOD(ln_call_in_start_min + ROUND(ln_call_in_window_min * ln_seed_4 / 9999), 1440);
		
		IF ln_convert_time_zone = PKG_CONST.BOOLEAN__TRUE THEN
			-- convert call-in time from server time zone to device local time zone
			ln_call_in_time_min := MOD(ln_call_in_time_min + DBADMIN.PKG_UTL.TIME_ZONE_DIFF_MIN(pv_device_time_zone_guid, PKG_CONST.DB_TIME_ZONE), 1440);
			IF ln_call_in_time_min < 0 THEN
				ln_call_in_time_min := ln_call_in_time_min + 1440;
			END IF;
		END IF;
		
		RETURN LPAD(FLOOR(ln_call_in_time_min / 60), 2, '0') || LPAD(MOD(ln_call_in_time_min, 60), 2, '0');			
	END;
	
	FUNCTION GET_NORMALIZED_OFFSET(
		pv_normalized_time VARCHAR2,
		pv_device_time_zone_guid VARCHAR2
	) RETURN NUMBER
	IS
		ln_normalized_time_min NUMBER;
		ln_normalized_utc_time_min NUMBER;
	BEGIN
		ln_normalized_time_min := TO_NUMBER(SUBSTR(pv_normalized_time, 1, 2)) * 60 + TO_NUMBER(SUBSTR(pv_normalized_time, 3, 2));
		ln_normalized_utc_time_min := MOD(ln_normalized_time_min + DBADMIN.PKG_UTL.TIME_ZONE_DIFF_MIN(PKG_CONST.GMT_TIME_ZONE, pv_device_time_zone_guid), 1440);
		IF ln_normalized_utc_time_min < 0 THEN
			ln_normalized_utc_time_min := ln_normalized_utc_time_min + 1440;
		END IF;
		RETURN ln_normalized_utc_time_min * 60;
	END;

    PROCEDURE NORMALIZE_SCHEDULE(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
		pv_schedule_type DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_normalizer_start_min_cd ENGINE.APP_SETTING.APP_SETTING_CD%TYPE,
		pv_normalizer_end_min_cd ENGINE.APP_SETTING.APP_SETTING_CD%TYPE,
		pv_window_start_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_window_hours_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_device_time_zone_guid VARCHAR2,
		pn_result_cd IN OUT NUMBER,
		pv_schedule IN OUT DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE
	)
    IS
		lv_current_schedule DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
		ln_current_interval NUMBER;
		ln_min_allowed_interval NUMBER;
		lc_reoccurrence_type CHAR(1);
		ln_exists NUMBER;
		lv_normalized_time VARCHAR2(4) := GET_NORMALIZED_TIME(pn_device_id, pv_device_serial_cd, pv_normalizer_start_min_cd, pv_normalizer_end_min_cd, pv_window_start_cd, pv_window_hours_cd, pv_device_time_zone_guid);
		ln_normalized_offset NUMBER := GET_NORMALIZED_OFFSET(lv_normalized_time, pv_device_time_zone_guid);
		lv_schedule DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE := NULL;
    BEGIN
		lv_current_schedule := NVL(GET_DEVICE_SETTING(pn_device_id, pv_schedule_type), '0');
		IF lv_current_schedule = '0' THEN
			IF pv_schedule_type = PKG_CONST.DSP__NON_ACTIV_CALL_IN_SCHED THEN
				lv_schedule := PKG_CONST.REOCCURRENCE_TYPE__INTERVAL || PKG_CONST.SCHEDULE__FS || ln_normalized_offset || PKG_CONST.SCHEDULE__FS || DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EDGE_NON_ACTIVATED_CALL_IN_INTERVAL_SEC');
			ELSIF pv_schedule_type IN (PKG_CONST.DSP__ACTIVATED_CALL_IN_SCHED, PKG_CONST.DSP__SETTLEMENT_SCHEDULE) THEN
				IF lv_normalized_time IS NOT NULL THEN
					lv_schedule := PKG_CONST.REOCCURRENCE_TYPE__WEEKLY || PKG_CONST.SCHEDULE__FS || lv_normalized_time || PKG_CONST.SCHEDULE__FS || PKG_CONST.SCHEDULE__SUNDAY;
				END IF;
			END IF;
		ELSE
			lc_reoccurrence_type := SUBSTR(lv_current_schedule, 1, 1);
			IF lc_reoccurrence_type = PKG_CONST.REOCCURRENCE_TYPE__INTERVAL THEN
				ln_current_interval := NVL(DBADMIN.TO_NUMBER_OR_NULL(SUBSTR(lv_current_schedule, INSTR(lv_current_schedule, PKG_CONST.SCHEDULE__FS, -1, 1) + 1)), -1);
				ln_min_allowed_interval := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('MIN_INTERVAL_SCHEDULE_SEC'));
				IF ln_current_interval < ln_min_allowed_interval THEN
					ln_current_interval := ln_min_allowed_interval;
				END IF;
				lv_schedule := lc_reoccurrence_type || PKG_CONST.SCHEDULE__FS || ln_normalized_offset || PKG_CONST.SCHEDULE__FS || ln_current_interval;
			ELSIF lc_reoccurrence_type IN (PKG_CONST.REOCCURRENCE_TYPE__MONTHLY, PKG_CONST.REOCCURRENCE_TYPE__WEEKLY) THEN
				IF lv_normalized_time IS NOT NULL THEN
					lv_schedule := lc_reoccurrence_type || PKG_CONST.SCHEDULE__FS || lv_normalized_time || PKG_CONST.SCHEDULE__FS || SUBSTR(lv_current_schedule, INSTR(lv_current_schedule, PKG_CONST.SCHEDULE__FS, -1, 1) + 1);
				END IF;
			ELSE
				IF lv_normalized_time IS NOT NULL THEN
					lv_schedule := PKG_CONST.REOCCURRENCE_TYPE__DAILY || PKG_CONST.SCHEDULE__FS || lv_normalized_time;
				END IF;
			END IF;
		END IF;
		IF lv_schedule IS NOT NULL AND lv_schedule != lv_current_schedule THEN
			SP_UPSERT_DEVICE_SETTING(pn_device_id, pv_schedule_type, lv_schedule, ln_exists);
			pn_result_cd := 1;
			pv_schedule := lv_schedule;
		END IF;		
    END;
  
  PROCEDURE SP_NORMALIZE_CALL_IN_TIME(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pn_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE,
		pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
		pv_device_time_zone_guid VARCHAR2,
		pn_result_cd OUT NUMBER,
		pv_activated_call_in_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_non_activ_call_in_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_settlement_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_dex_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE
	)
	IS
    ln_command_id       ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE;
    lv_data_type        ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE;
    ll_command          ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE;
  BEGIN
    SP_NORMALIZE_CALL_IN_TIME(
      pn_device_id, 
      pn_device_type_id, 
      pv_device_serial_cd, 
      pv_device_time_zone_guid, 
      pn_result_cd, 
      pv_activated_call_in_schedule, 
      pv_non_activ_call_in_schedule, 
      pv_settlement_schedule, 
      pv_dex_schedule,
      ln_command_id, 
      lv_data_type, 
      ll_command);
  END; -- SP_NORMALIZE_CALL_IN_TIME stub
  
  /**
    * r29 Call-in normalization version
    */ 
	PROCEDURE SP_NORMALIZE_CALL_IN_TIME(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pn_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE,
		pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
		pv_device_time_zone_guid VARCHAR2,
		pn_result_cd OUT NUMBER,
		pv_activated_call_in_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_non_activ_call_in_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_settlement_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_dex_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
		pv_data_type OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
		pl_command OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE
	)
	IS
		lv_return_msg VARCHAR2(2048);
		ln_exists NUMBER;
		ln_command_id ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE;
		lv_data_type ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE;
		ll_command ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE;
		ln_result_cd NUMBER;
		lv_current_value DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
		lv_new_value DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
		ln_current_value NUMBER;
		ln_threshold NUMBER;
		ln_device_id DEVICE.DEVICE_ID%TYPE;
	BEGIN
		-- normalize device call-in time to avoid load peaks
		pn_result_cd := 0;
		pv_activated_call_in_schedule := PKG_CONST.ASCII__NUL;
		pv_non_activ_call_in_schedule := PKG_CONST.ASCII__NUL;
		pv_settlement_schedule := PKG_CONST.ASCII__NUL;
		pv_dex_schedule := PKG_CONST.ASCII__NUL;
		
		SELECT MAX(dla.device_id)
		INTO ln_device_id
		FROM device.device d
		JOIN device.device_last_active dla ON d.device_name = dla.device_name
		WHERE d.device_id = pn_device_id;
		
		IF pn_device_type_id IN (PKG_CONST.DEVICE_TYPE__G4, PKG_CONST.DEVICE_TYPE__GX) THEN
			pv_activated_call_in_schedule := GET_NORMALIZED_TIME(ln_device_id, pv_device_serial_cd, 'GX_CALL_IN_NORMALIZER_START_MIN', 'GX_CALL_IN_NORMALIZER_END_MIN',
				'CALL_IN_TIME_WINDOW_START', 'CALL_IN_TIME_WINDOW_HOURS', pv_device_time_zone_guid);
            IF pv_activated_call_in_schedule IS NOT NULL THEN
				IF pv_activated_call_in_schedule BETWEEN '0000' AND '0004' THEN
					pv_activated_call_in_schedule := '001' || SUBSTR(pv_activated_call_in_schedule, 4, 1);
				ELSIF pv_activated_call_in_schedule BETWEEN '2356' AND '2359' THEN
					pv_activated_call_in_schedule := '234' || SUBSTR(pv_activated_call_in_schedule, 4, 1);
				END IF;
				SP_UPDATE_CONFIG_AND_RETURN(ln_device_id, 172, pv_activated_call_in_schedule, 'H', pn_device_type_id, 0, pn_result_cd, lv_return_msg, pn_command_id, pv_data_type, pl_command);
			END IF;
			
			lv_current_value := GET_DEVICE_SETTING(ln_device_id, '358');
			pv_dex_schedule := GET_NORMALIZED_TIME(ln_device_id, pv_device_serial_cd,
				CASE WHEN lv_current_value = 'FFFF' THEN NULL ELSE 'GX_CALL_IN_NORMALIZER_START_MIN' END,
				CASE WHEN lv_current_value = 'FFFF' THEN NULL ELSE 'GX_CALL_IN_NORMALIZER_END_MIN' END,
				'CALL_IN_TIME_WINDOW_START_DEX', 'CALL_IN_TIME_WINDOW_HOURS_DEX', pv_device_time_zone_guid);
			IF pv_dex_schedule IS NOT NULL THEN
				IF pn_command_id IS NULL THEN
					SP_UPDATE_CONFIG_AND_RETURN(ln_device_id, 358, pv_dex_schedule, 'H', pn_device_type_id, 0, pn_result_cd, lv_return_msg, pn_command_id, pv_data_type, pl_command);
				ELSE
					SP_UPDATE_CONFIG_AND_RETURN(ln_device_id, 358, pv_dex_schedule, 'H', pn_device_type_id, 0, ln_result_cd, lv_return_msg, ln_command_id, lv_data_type, ll_command);
				END IF;
			END IF;
			
			--DEX Read and Send Interval Minutes
			lv_current_value := GET_DEVICE_SETTING(ln_device_id, '244');
			IF lv_current_value != '0000' THEN
				ln_current_value := NVL(DBADMIN.TO_NUMBER_OR_NULL(lv_current_value), 0);
				ln_threshold := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('MIN_DEX_READ_AND_SEND_INTERVAL_MIN'));
				IF ln_current_value < ln_threshold THEN
					lv_new_value := LPAD(ln_threshold, 4, '0');
					IF pn_command_id IS NULL THEN
						SP_UPDATE_CONFIG_AND_RETURN(ln_device_id, 244, lv_new_value, 'H', pn_device_type_id, 0, pn_result_cd, lv_return_msg, pn_command_id, pv_data_type, pl_command);
					ELSE
						SP_UPDATE_CONFIG_AND_RETURN(ln_device_id, 244, lv_new_value, 'H', pn_device_type_id, 0, ln_result_cd, lv_return_msg, ln_command_id, lv_data_type, ll_command);
					END IF;
				END IF;
			END IF;
		ELSIF pn_device_type_id = PKG_CONST.DEVICE_TYPE__MEI THEN
			pv_activated_call_in_schedule := GET_NORMALIZED_TIME(ln_device_id, pv_device_serial_cd, 'MEI_CALL_IN_NORMALIZER_START_MIN', 'MEI_CALL_IN_NORMALIZER_END_MIN',
				'CALL_IN_TIME_WINDOW_START', 'CALL_IN_TIME_WINDOW_HOURS', pv_device_time_zone_guid);
            IF pv_activated_call_in_schedule IS NOT NULL THEN
				lv_new_value := LPAD(TO_NUMBER(SUBSTR(pv_activated_call_in_schedule, 1, 2)) * 60 + TO_NUMBER(SUBSTR(pv_activated_call_in_schedule, 3, 2)), 4, '0');
				SP_UPDATE_CONFIG_AND_RETURN(ln_device_id, 17, lv_new_value, 'H', pn_device_type_id, 0, pn_result_cd, lv_return_msg, pn_command_id, pv_data_type, pl_command);
			END IF;			
		ELSIF pn_device_type_id = PKG_CONST.DEVICE_TYPE__EDGE THEN
			NORMALIZE_SCHEDULE(ln_device_id, pv_device_serial_cd, PKG_CONST.DSP__ACTIVATED_CALL_IN_SCHED, 'EDGE_CALL_IN_NORMALIZER_START_MIN', 'EDGE_CALL_IN_NORMALIZER_END_MIN', 
				'CALL_IN_TIME_WINDOW_START_ACTIVATED', 'CALL_IN_TIME_WINDOW_HOURS_ACTIVATED', pv_device_time_zone_guid, pn_result_cd, pv_activated_call_in_schedule);
			NORMALIZE_SCHEDULE(ln_device_id, pv_device_serial_cd, PKG_CONST.DSP__NON_ACTIV_CALL_IN_SCHED, 'EDGE_CALL_IN_NORMALIZER_START_MIN', 'EDGE_CALL_IN_NORMALIZER_END_MIN', 
				'CALL_IN_TIME_WINDOW_START_NON_ACTIVATED', 'CALL_IN_TIME_WINDOW_HOURS_NON_ACTIVATED', pv_device_time_zone_guid, pn_result_cd, pv_non_activ_call_in_schedule);
			NORMALIZE_SCHEDULE(ln_device_id, pv_device_serial_cd, PKG_CONST.DSP__SETTLEMENT_SCHEDULE, 'EDGE_CALL_IN_NORMALIZER_START_MIN', 'EDGE_CALL_IN_NORMALIZER_END_MIN', 
				'CALL_IN_TIME_WINDOW_START_SETTLEMENT', 'CALL_IN_TIME_WINDOW_HOURS_SETTLEMENT', pv_device_time_zone_guid, pn_result_cd, pv_settlement_schedule);

			IF DBADMIN.PKG_GLOBAL.GET_APP_SETTING('NORMALIZE_EDGE_DEX_SCHEDULE_FLAG') = 'Y' THEN
				NORMALIZE_SCHEDULE(ln_device_id, pv_device_serial_cd, PKG_CONST.DSP__DEX_SCHEDULE, 'EDGE_CALL_IN_NORMALIZER_START_MIN', 'EDGE_CALL_IN_NORMALIZER_END_MIN', 
					'CALL_IN_TIME_WINDOW_START_DEX', 'CALL_IN_TIME_WINDOW_HOURS_DEX', pv_device_time_zone_guid, pn_result_cd, pv_dex_schedule);
			END IF;
		END IF;
		
		UPDATE DEVICE.DEVICE
		SET CALL_IN_TIME_NORMALIZED_TS = SYSDATE
		WHERE DEVICE_ID = ln_device_id;
	END;
         
  /**
    * Part of the r29 version of SP_NEXT_PENDING_COMMAND
    * This non-public procedure adds commands for ESUDS Diagnostics, if existing diagnostics are beyond 16 hours old.
    */
    PROCEDURE SP_NPC_HELPER_ESUDS_DIAG(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pv_data_type OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pv_session_attributes IN OUT VARCHAR2, -- contains character 'E' if ESUDS diagnostics has been requested previously
        pn_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
        pn_commands_inserted OUT NUMBER,
        pl_command OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE
    )
    IS
        ld_oldest_diag DATE;
    BEGIN
		IF pn_device_type_id IN (PKG_CONST.DEVICE_TYPE__ESUDS) AND INSTR(NVL(pv_session_attributes,'-'),'E') = 0 THEN        
            pv_session_attributes := NVL(pv_session_attributes,'') || 'E';       
            -- if any records for the given device are old, (no records = old records)       
            SELECT MIN(MOST_RECENT_PORT_DIAG_DATE) 
              INTO ld_oldest_diag
              FROM (
                  SELECT d.device_id, h.host_port_num, MAX(NVL(hds.host_diag_last_reported_ts, MIN_DATE)) AS MOST_RECENT_PORT_DIAG_DATE
                    FROM device d  
                    LEFT OUTER JOIN host h ON (d.device_id = h.device_id) 
                    LEFT OUTER JOIN host_diag_status hds ON (hds.host_id = h.host_id) 
                    WHERE d.device_type_id = PKG_CONST.DEVICE_TYPE__ESUDS
                    AND d.device_name = pv_device_name
                    AND d.device_active_yn_flag = 'Y'
                    AND NVL(h.host_active_yn_flag,'Y') = 'Y'
                    AND h.host_port_num <> 0
                    GROUP BY d.device_id, h.host_port_num            
              );
    
            IF ld_oldest_diag < SYSDATE - 1 THEN           
                pv_data_type := '9A61';
                pl_command := '';
                UPSERT_PENDING_COMMAND(pv_device_name, pv_data_type, pl_command, 'S', 999, pn_commands_inserted, pn_command_id);                                   
            END IF;
		END IF;  
    END; -- SP_NPC_HELPER_ESUDS_DIAGNOSTICS

  /**
    * Part of the r29 version of SP_NEXT_PENDING_COMMAND
    * This non-public procedure adds commands for GPRS configuration files, if needed.
    */
    PROCEDURE SP_NPC_HELPER_MODEM(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pv_data_type OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pv_session_attributes IN OUT VARCHAR2, -- contains character 'C' if configuration test has been requested previously, R if configuration requested       
        pn_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
        pn_commands_inserted OUT NUMBER,
        pl_command OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE
    )
    IS
        ld_last_update_ts DATE;
    BEGIN
        -- For Gx and MEI, test to see if modem data is outdated by 7 days. If so, send the appropriate message (both happen to be 87)	
        -- Some G4's only send garbage for modem info, so don't keep requesting it for G4 (PKG_CONST.DEVICE_TYPE__G4)
    	IF pn_device_type_id IN (PKG_CONST.DEVICE_TYPE__GX, PKG_CONST.DEVICE_TYPE__MEI) AND INSTR(NVL(pv_session_attributes,'-'),'C') = 0 THEN             
            pv_session_attributes := NVL(pv_session_attributes,'') || 'C';
            SELECT NVL(MAX(H.LAST_UPDATED_TS), MIN_DATE)
              INTO ld_last_update_ts
              FROM DEVICE.DEVICE D
              JOIN DEVICE.HOST H ON D.DEVICE_ID = H.DEVICE_ID
             WHERE D.DEVICE_NAME = pv_device_name
               AND D.DEVICE_ACTIVE_YN_FLAG = 'Y'
               AND H.HOST_PORT_NUM = 2;
            IF ld_last_update_ts < SYSDATE - NVL(TO_NUMBER_OR_NULL(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DEVICE_MODEM_INFO_REFRESH_HR')) / 24, 7) THEN				
                pv_data_type := '87';					
                IF (pn_device_type_id = PKG_CONST.DEVICE_TYPE__MEI) THEN 
                    pl_command := '4200000000000000FF';							
                ELSIF (pn_device_type_id = PKG_CONST.DEVICE_TYPE__GX OR pn_device_type_id = PKG_CONST.DEVICE_TYPE__G4) THEN 
                    pl_command := '4400802C0000000190';
                END IF;
				pv_session_attributes := NVL(pv_session_attributes,'') || 'R';
                UPSERT_PENDING_COMMAND(pv_device_name, pv_data_type, pl_command, 'S', 999, pn_commands_inserted, pn_command_id);
            END IF;
        END IF;
    END;

  /**
    * Part of the r29 version of SP_NEXT_PENDING_COMMAND
    * This non-public procedure adds commands for G4 configuration files, if existing ones are stale.
    */
    PROCEDURE SP_NPC_HELPER_STALE_CONFIG(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pv_data_type OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pv_session_attributes IN OUT VARCHAR2, -- contains character 'G' if GX configuration has been requested previously
        pn_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
        pn_commands_inserted OUT NUMBER,
        pl_command OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE
    )
    IS
        ld_last_config_ts DATE;
    BEGIN
        -- For MEI/Gx test age of configuration data
		IF pn_device_type_id IN (PKG_CONST.DEVICE_TYPE__GX, PKG_CONST.DEVICE_TYPE__MEI, PKG_CONST.DEVICE_TYPE__G4) AND INSTR(NVL(pv_session_attributes,'-'),'G') = 0 THEN              
	        pv_session_attributes := NVL(pv_session_attributes,'') || 'G';	        
	        IF NOT(pn_device_type_id = PKG_CONST.DEVICE_TYPE__MEI AND INSTR(NVL(pv_session_attributes,'-'), 'R') > 0 ) THEN	        	 
	        	-- Now check last configuration received timestamp 	        	
		        SELECT NVL(D.RECEIVED_CONFIG_FILE_TS, MIN_DATE)
		          INTO ld_last_config_ts
		          FROM DEVICE.DEVICE D 
		         WHERE D.DEVICE_NAME = pv_device_name 
		           AND D.DEVICE_ACTIVE_YN_FLAG = 'Y';
                IF ld_last_config_ts < SYSDATE - NVL(TO_NUMBER_OR_NULL(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DEVICE_CONFIG_REFRESH_HR')) / 24, 30) THEN				
                    pv_data_type := '87';
					-- same command for all devices handled by this procedure					
					pl_command := '4200000000000000FF';		
					pv_session_attributes := NVL(pv_session_attributes,'') || 'R';	
					UPSERT_PENDING_COMMAND(pv_device_name, pv_data_type, pl_command, 'S', 999, pn_commands_inserted, pn_command_id);	                						
				END IF; -- actual test	
			END IF; -- 'R' flag			
    	END IF; -- 'G' flag    	
    END; -- SP_NPC_HELPER_STALE_CONFIG
    
  /**
    * Part of the r29 version of SP_NEXT_PENDING_COMMAND
    * This non-public procedure adds commands to set the Gx verbosity flag 
    * on a non-remotely updatable device.
    */
    PROCEDURE SP_NPC_HELPER_PHILLY_COKE(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pv_data_type OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pv_session_attributes IN OUT VARCHAR2, -- contains character 'P' if ESUDS diagnostics has been requested previously
        pn_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
        pv_firmware_version IN DEVICE.FIRMWARE_VERSION%TYPE,
        pn_commands_inserted OUT NUMBER,
        pl_command OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE
    )
    IS
        l_is_philly_coke NUMBER;
    BEGIN
        -- For Gx test age of configuration data
        IF pn_device_type_id IN (PKG_CONST.DEVICE_TYPE__GX) AND INSTR(NVL(pv_session_attributes,'-'),'P') = 0 AND REGEXP_LIKE(pv_firmware_version, 'USA-G5[0-9] ?[vV]4\.2\.(0|1[A-S]).*') THEN             
            pv_session_attributes := NVL(pv_session_attributes,'') || 'P';      
            pv_data_type := '88';
            pl_command := '4400807FE001';
            UPSERT_PENDING_COMMAND(pv_device_name, pv_data_type, pl_command, 'S', 2, pn_commands_inserted, pn_command_id);
		END IF;   
    END; -- SP_NPC_HELPER_PHILLY_COKE

  /**
    * r29 version of SP_NEXT_PENDING_COMMAND
    * This is the feed of commands from the UI tools to the App Layer and thus the devices
    */
    PROCEDURE SP_NEXT_PENDING_COMMAND(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pv_data_type OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pr_command_bytes OUT RAW,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pv_file_transfer_name OUT FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_id OUT FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pl_file_transfer_content OUT FILE_TRANSFER.FILE_TRANSFER_CONTENT%TYPE,
        pn_file_transfer_group_num OUT DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
        pn_file_transfer_pkt_size OUT DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
        pd_file_transfer_created_ts OUT DEVICE_FILE_TRANSFER.CREATED_TS%TYPE,
        pn_max_execute_order ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE DEFAULT 999,
        pn_priority_command_id ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE DEFAULT NULL,
        pv_session_attributes IN OUT VARCHAR2, -- contains character 'C' if configuration has been requested previously
		pv_global_session_cd VARCHAR2 DEFAULT NULL,
		pn_update_status NUMBER DEFAULT 0,
        pc_v4_messages CHAR DEFAULT '?'
        )
    IS
        ll_command ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE;
		ln_device_id DEVICE.DEVICE_ID%TYPE;
        ln_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE;
        lv_firmware_version DEVICE.FIRMWARE_VERSION%TYPE;
		lv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE;
		ld_call_in_time_normalized_ts DEVICE.CALL_IN_TIME_NORMALIZED_TS%TYPE;
		lv_device_time_zone_guid VARCHAR2(60);
		ln_result_cd NUMBER;
		lv_activated_call_in_schedule DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
		lv_non_activ_call_in_schedule DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
		lv_settlement_schedule DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
		lv_dex_schedule DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
		ln_command_id ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE;
		ln_file_transfer_id FILE_TRANSFER.FILE_TRANSFER_ID%TYPE;
		l_addr NUMBER;
        l_len NUMBER;
        l_pos NUMBER;
        l_commands_inserted NUMBER;
		ln_sending_file_cnt NUMBER;
		ln_sending_file_limit NUMBER := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DOWNLOAD_LIMIT_APP_UPGRADE'));
		ln_max_file_download_attempts NUMBER(3,0) := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('MAX_FILE_DOWNLOAD_ATTEMPTS'));
		lv_file_content_hex VARCHAR2(1024);
		lv_data_type ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE;
		lv_command ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE;
		lv_last_pending_cmd_session_cd VARCHAR2(100);
		lc_upgrade_throttling_enabled COMM_PROVIDER.UPGRADE_THROTTLING_ENABLED%TYPE;
		ln_upgrade_cap_bytes COMM_PROVIDER.UPGRADE_CAP_BYTES%TYPE;
		ln_billing_period_start COMM_PROVIDER.BILLING_PERIOD_START%TYPE;
		ld_billing_period_start_ts DATE;
		ln_average_bytes_transferred DATA_TRANSFER.BYTES_RECEIVED%TYPE := 0;
		ln_active_billing_devices DATA_TRANSFER.DEVICE_COUNT%TYPE;
		lc_comm_method_cd DEVICE.COMM_METHOD_CD%TYPE;
		ln_comm_provider_id COMM_PROVIDER.COMM_PROVIDER_ID%TYPE;
		ln_fw_upg_max_step_attempts NUMBER;
		ln_current_fw_upg_step_attempt DEVICE_FIRMWARE_UPGRADE.CURRENT_FW_UPG_STEP_ATTEMPT%TYPE;
		ln_device_file_transfer_id DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_ID%TYPE := 0;
		ln_device_fw_upg_id DEVICE_FIRMWARE_UPGRADE.DEVICE_FIRMWARE_UPGRADE_ID%TYPE;
		ln_firmware_upgrade_id FIRMWARE_UPGRADE.FIRMWARE_UPGRADE_ID%TYPE;
		ln_complete_fw_upg_step_id DEVICE_FIRMWARE_UPGRADE.COMPLETE_FW_UPG_STEP_ID%TYPE;
		lv_error_message DEVICE_FIRMWARE_UPGRADE.ERROR_MESSAGE%TYPE;
		ln_step_id FIRMWARE_UPGRADE_STEP.FIRMWARE_UPGRADE_STEP_ID%TYPE;
		ln_step_number FIRMWARE_UPGRADE_STEP.STEP_NUMBER%TYPE;
		lv_step_name FIRMWARE_UPGRADE_STEP.FIRMWARE_UPGRADE_STEP_NAME%TYPE;
		ln_file_id FILE_TRANSFER.FILE_TRANSFER_ID%TYPE;
		lv_peek_command ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE;
		ln_file_size NUMBER;
		ln_packet_size NUMBER;
		lv_branch_param_cd FIRMWARE_UPGRADE_STEP.BRANCH_PARAM_CD%TYPE;
		lv_branch_value_regex FIRMWARE_UPGRADE_STEP.BRANCH_VALUE_REGEX%TYPE;
		lv_target_param_cd FIRMWARE_UPGRADE_STEP.TARGET_PARAM_CD%TYPE;
		lv_target_version FIRMWARE_UPGRADE_STEP.TARGET_VERSION%TYPE;
		lv_target_value_regex FIRMWARE_UPGRADE_STEP.TARGET_VALUE_REGEX%TYPE;
		lv_current_version DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
		lv_current_value VARCHAR2(255);
		lv_app_setting_value ENGINE.APP_SETTING.APP_SETTING_VALUE%TYPE;
		ln_requirement_met NUMBER;
		lv_new_firmware_version VARCHAR2(20);
		ln_cancel_command NUMBER(1,0);
		ln_loop_count NUMBER := 0;
		ld_sysdate DATE := SYSDATE;
    BEGIN
		SELECT D.DEVICE_ID, D.DEVICE_TYPE_ID, D.DEVICE_SERIAL_CD, D.CALL_IN_TIME_NORMALIZED_TS, COALESCE(T_TZ.TIME_ZONE_GUID, TZ.TIME_ZONE_GUID, 'US/Eastern'), D.FIRMWARE_VERSION,
			     D.COMM_METHOD_CD, CP.COMM_PROVIDER_ID, CP.UPGRADE_THROTTLING_ENABLED, CP.UPGRADE_CAP_BYTES, CP.BILLING_PERIOD_START
		  INTO ln_device_id, ln_device_type_id, lv_device_serial_cd, ld_call_in_time_normalized_ts, lv_device_time_zone_guid, lv_firmware_version,
			     lc_comm_method_cd, ln_comm_provider_id, lc_upgrade_throttling_enabled, ln_upgrade_cap_bytes, ln_billing_period_start
      FROM DEVICE.DEVICE D
      LEFT OUTER JOIN PSS.POS P ON D.DEVICE_ID = P.DEVICE_ID
      LEFT OUTER JOIN LOCATION.LOCATION L ON P.LOCATION_ID = L.LOCATION_ID
      LEFT OUTER JOIN LOCATION.TIME_ZONE TZ ON L.LOCATION_TIME_ZONE_CD = TZ.TIME_ZONE_CD
      LEFT OUTER JOIN REPORT.EPORT E ON D.DEVICE_SERIAL_CD = E.EPORT_SERIAL_NUM
		  LEFT OUTER JOIN REPORT.VW_TERMINAL_EPORT TE ON E.EPORT_ID = TE.EPORT_ID
		  LEFT OUTER JOIN REPORT.TERMINAL T ON TE.TERMINAL_ID = T.TERMINAL_ID
		  LEFT OUTER JOIN REPORT.TIME_ZONE T_TZ ON T.TIME_ZONE_ID = T_TZ.TIME_ZONE_ID
		  LEFT OUTER JOIN DEVICE.COMM_METHOD CM ON D.COMM_METHOD_CD = CM.COMM_METHOD_CD
      LEFT OUTER JOIN DEVICE.COMM_PROVIDER CP ON CM.COMM_PROVIDER_ID = CP.COMM_PROVIDER_ID
		 WHERE D.DEVICE_NAME = pv_device_name
			 AND D.DEVICE_ACTIVE_YN_FLAG = 'Y';
	
		IF lv_device_serial_cd LIKE 'K3%' AND pn_update_status = PKG_CONST.UPDATE_STATUS__NORMAL AND pv_global_session_cd IS NOT NULL THEN
			BEGIN
				SELECT TO_CHAR(LAST_PENDING_COMMAND_ID), LAST_PENDING_CMD_SESSION_CD
				INTO ln_command_id, lv_last_pending_cmd_session_cd
				FROM DEVICE.DEVICE_INFO
				WHERE DEVICE_NAME = pv_device_name;
				
				IF ln_command_id IS NOT NULL THEN
					UPDATE ENGINE.MACHINE_CMD_PENDING
					SET EXECUTE_CD = 'A', GLOBAL_SESSION_CD = pv_global_session_cd
					WHERE MACHINE_COMMAND_PENDING_ID = ln_command_id
                      AND REGEXP_LIKE(DATA_TYPE, DECODE(pc_v4_messages, '?', '^[0-9A-Fa-f]{2,4}$', 'N', '^[0-9A-Ba-b][0-9A-Fa-f]{1,3}$', 'Y', '^C[0-9A-Fa-f]{1,3}$'))
					RETURNING DATA_TYPE, COMMAND, DEVICE_FILE_TRANSFER_ID INTO lv_data_type, lv_command, ln_device_file_transfer_id;
					
					IF ln_device_file_transfer_id > 0 THEN		  		
						UPDATE DEVICE.DEVICE_FILE_TRANSFER
						SET DEVICE_FILE_TRANSFER_STATUS_CD = 1,
							DEVICE_FILE_TRANSFER_TS = SYSDATE,
							GLOBAL_SESSION_CD = lv_last_pending_cmd_session_cd
						WHERE DEVICE_FILE_TRANSFER_ID = ln_device_file_transfer_id; 
					END IF;
					
					UPDATE DEVICE.DEVICE_INFO
					SET LAST_PENDING_COMMAND_ID = NULL,
						LAST_PENDING_CMD_SESSION_CD = NULL
					WHERE DEVICE_NAME = pv_device_name;
				END IF;
			EXCEPTION
				WHEN NO_DATA_FOUND THEN
					NULL;
			END;
		END IF;
		
		LOOP
			pn_command_id := NULL;
			pv_data_type := NULL;
			ll_command := NULL;
			ln_device_file_transfer_id := 0;
			ln_device_fw_upg_id := 0;
		
			ln_loop_count := ln_loop_count + 1;
			IF ln_loop_count > 100 THEN
				-- make sure we don't enter an infinite loop
				EXIT;
			END IF;
	
			-- throttle the number of concurrent app upgrade file downloads
			SELECT COUNT(1) INTO ln_sending_file_cnt
			FROM engine.machine_cmd_pending mcp
			WHERE mcp.execute_cd = 'S' AND mcp.execute_date > SYSDATE - 15/1440 AND (
				mcp.device_file_transfer_id > 0 AND EXISTS (
					SELECT 1 FROM device.device_file_transfer dft
					JOIN device.file_transfer ft ON dft.file_transfer_id = ft.file_transfer_id
					WHERE dft.device_file_transfer_id = mcp.device_file_transfer_id
						AND ft.file_transfer_type_cd IN (PKG_CONST.FILE_TYPE__APP_UPGRADE, PKG_CONST.FILE_TYPE__CARD_READER_APP)
				)
				OR mcp.data_type = 'FW_UPG' AND EXISTS (
					SELECT 1 FROM DEVICE.DEVICE_FIRMWARE_UPGRADE DFU
					WHERE DFU.DEVICE_FIRMWARE_UPGRADE_ID = mcp.device_firmware_upgrade_id
				)
			);
				
			IF lc_upgrade_throttling_enabled = 'Y' AND ln_sending_file_cnt < ln_sending_file_limit THEN
				ld_billing_period_start_ts := TRUNC(ld_sysdate, 'MONTH') + ln_billing_period_start - 1;
				IF ld_billing_period_start_ts > ld_sysdate THEN
					ld_billing_period_start_ts := ADD_MONTHS(TRUNC(ld_sysdate, 'MONTH'), -1) + ln_billing_period_start - 1;
				END IF;
				
				SELECT CASE WHEN SUM(DEVICE_COUNT) > 0 THEN SUM(BYTES_TOTAL) / SUM(DEVICE_COUNT) END
				  INTO ln_average_bytes_transferred
          FROM (
				SELECT DT.COMM_METHOD_CD, SUM(DT.BYTES_RECEIVED) + SUM(DT.BYTES_SENT) BYTES_TOTAL, MAX(DT.DEVICE_COUNT) DEVICE_COUNT
				  FROM DEVICE.DATA_TRANSFER DT
          JOIN DEVICE.COMM_METHOD CM ON DT.COMM_METHOD_CD = CM.COMM_METHOD_CD
				 WHERE DT.DATA_TRANSFER_TS >= ld_billing_period_start_ts
					 AND CM.COMM_PROVIDER_ID = ln_comm_provider_id
         GROUP BY DT.COMM_METHOD_CD);
			END IF;
		
			IF ln_sending_file_cnt >= ln_sending_file_limit OR lc_upgrade_throttling_enabled = 'Y' AND ln_average_bytes_transferred >= ln_upgrade_cap_bytes THEN
				-- mark commands Capped or Throttled
				UPDATE ENGINE.MACHINE_CMD_PENDING MCP
				SET EXECUTE_CD = CASE WHEN lc_upgrade_throttling_enabled = 'Y' AND ln_average_bytes_transferred >= ln_upgrade_cap_bytes THEN 'E'
					ELSE 'T' END,
					EXECUTE_DATE = SYSDATE, GLOBAL_SESSION_CD = pv_global_session_cd
				WHERE MCP.MACHINE_ID = pv_device_name AND EXECUTE_CD IN ('P', 'S') AND MCP.DEVICE_FILE_TRANSFER_ID > 0 AND (
					MCP.DATA_TYPE != 'C7' AND EXISTS (
						SELECT 1 FROM DEVICE.DEVICE_FILE_TRANSFER DFT
						JOIN DEVICE.FILE_TRANSFER FT ON DFT.FILE_TRANSFER_ID = FT.FILE_TRANSFER_ID
						WHERE DFT.DEVICE_FILE_TRANSFER_ID = MCP.DEVICE_FILE_TRANSFER_ID
							AND FT.FILE_TRANSFER_TYPE_CD IN (PKG_CONST.FILE_TYPE__APP_UPGRADE, PKG_CONST.FILE_TYPE__CARD_READER_APP)
					)
					OR MCP.DATA_TYPE = 'FW_UPG' AND EXISTS (
						SELECT 1 FROM DEVICE.DEVICE_FIRMWARE_UPGRADE DFU
						WHERE DFU.DEVICE_FIRMWARE_UPGRADE_ID = MCP.DEVICE_FIRMWARE_UPGRADE_ID
					)
				);
			ELSE
				-- mark commands Pending again
				UPDATE ENGINE.MACHINE_CMD_PENDING MCP
				SET EXECUTE_CD = 'P', EXECUTE_DATE = SYSDATE, GLOBAL_SESSION_CD = pv_global_session_cd
				WHERE MCP.MACHINE_ID = pv_device_name AND EXECUTE_CD IN ('E', 'T') AND MCP.DEVICE_FILE_TRANSFER_ID > 0 AND (
					MCP.DATA_TYPE != 'C7' AND EXISTS (
						SELECT 1 FROM DEVICE.DEVICE_FILE_TRANSFER DFT
						JOIN DEVICE.FILE_TRANSFER FT ON DFT.FILE_TRANSFER_ID = FT.FILE_TRANSFER_ID
						WHERE DFT.DEVICE_FILE_TRANSFER_ID = MCP.DEVICE_FILE_TRANSFER_ID
							AND FT.FILE_TRANSFER_TYPE_CD IN (PKG_CONST.FILE_TYPE__APP_UPGRADE, PKG_CONST.FILE_TYPE__CARD_READER_APP)
					)
					OR MCP.DATA_TYPE = 'FW_UPG' AND EXISTS (
						SELECT 1 FROM DEVICE.DEVICE_FIRMWARE_UPGRADE DFU
						WHERE DFU.DEVICE_FIRMWARE_UPGRADE_ID = MCP.DEVICE_FIRMWARE_UPGRADE_ID
					)
				);
			END IF;
			
			IF ln_max_file_download_attempts > 0 THEN
				-- cancel file downloads that exceeded MAX_FILE_DOWNLOAD_ATTEMPTS and any pending commands that exceed MAX_FILE_DOWNLOAD_ATTEMPTS * 4
				UPDATE ENGINE.MACHINE_CMD_PENDING
				SET EXECUTE_CD = 'C'
				WHERE MACHINE_ID = pv_device_name
					AND EXECUTE_CD IN('S', 'P')
					AND (
						DEVICE_FILE_TRANSFER_ID IS NOT NULL AND ATTEMPT_COUNT >= ln_max_file_download_attempts
						OR ATTEMPT_COUNT >= ln_max_file_download_attempts * 4
					);
			END IF;
			
			UPDATE ENGINE.MACHINE_CMD_PENDING MCP
			   SET EXECUTE_CD = 'S', EXECUTE_DATE = SYSDATE, GLOBAL_SESSION_CD = pv_global_session_cd, ATTEMPT_COUNT = COALESCE(ATTEMPT_COUNT, 0) + 1
			 WHERE MCP.MACHINE_ID = pv_device_name
			   AND MCP.EXECUTE_ORDER <= pn_max_execute_order
			   AND MCP.EXECUTE_CD IN('S', 'P')
			   AND (REGEXP_LIKE(DATA_TYPE, DECODE(pc_v4_messages, '?', '^[0-9A-Fa-f]{2,4}$', 'N', '^[0-9A-Ba-b][0-9A-Fa-f]{1,3}$', 'Y', '^C[0-9A-Fa-f]{1,3}$'))
					OR DATA_TYPE = 'FW_UPG' AND NOT EXISTS (
						SELECT 1 FROM DEVICE.DEVICE_FIRMWARE_UPGRADE DFU
						WHERE DFU.DEVICE_FIRMWARE_UPGRADE_ID = MCP.DEVICE_FIRMWARE_UPGRADE_ID
							AND DFU.LAST_GLOBAL_SESSION_CD = pv_global_session_cd
					)
			   )
			   AND NOT EXISTS(
							SELECT 1
							  FROM ENGINE.MACHINE_CMD_PENDING MCP2
							 WHERE MCP.MACHINE_ID = MCP2.MACHINE_ID
							   AND MCP2.EXECUTE_CD IN('S', 'P')
							   AND (REGEXP_LIKE(DATA_TYPE, DECODE(pc_v4_messages, '?', '^[0-9A-Fa-f]{2,4}$', 'N', '^[0-9A-Ba-b][0-9A-Fa-f]{1,3}$', 'Y', '^C[0-9A-Fa-f]{1,3}$'))
									OR DATA_TYPE = 'FW_UPG' AND NOT EXISTS (
										SELECT 1 FROM DEVICE.DEVICE_FIRMWARE_UPGRADE DFU
										WHERE DFU.DEVICE_FIRMWARE_UPGRADE_ID = MCP2.DEVICE_FIRMWARE_UPGRADE_ID
											AND DFU.LAST_GLOBAL_SESSION_CD = pv_global_session_cd
									)
							   )
							   AND MCP.MACHINE_COMMAND_PENDING_ID != MCP2.MACHINE_COMMAND_PENDING_ID
							   AND MCP.EXECUTE_ORDER >= MCP2.EXECUTE_ORDER
							   AND (MCP.EXECUTE_ORDER > MCP2.EXECUTE_ORDER
								   OR MCP2.MACHINE_COMMAND_PENDING_ID = pn_priority_command_id
								   OR (MCP.MACHINE_COMMAND_PENDING_ID != NVL(pn_priority_command_id, 0)
										AND MCP.CREATED_TS > MCP2.CREATED_TS
										OR (MCP.CREATED_TS = MCP2.CREATED_TS AND MCP.MACHINE_COMMAND_PENDING_ID > MCP2.MACHINE_COMMAND_PENDING_ID))))
				RETURNING MACHINE_COMMAND_PENDING_ID, DATA_TYPE, COMMAND, DEVICE_FILE_TRANSFER_ID, DEVICE_FIRMWARE_UPGRADE_ID
				INTO pn_command_id, pv_data_type, ll_command, ln_device_file_transfer_id, ln_device_fw_upg_id;
				
			IF pn_command_id IS NULL THEN
				IF ln_device_type_id IN (PKG_CONST.DEVICE_TYPE__G4, PKG_CONST.DEVICE_TYPE__GX, PKG_CONST.DEVICE_TYPE__EDGE, PKG_CONST.DEVICE_TYPE__MEI)
					AND NVL(ld_call_in_time_normalized_ts, MIN_DATE) < SYSDATE - TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DEVICE_CALL_IN_NORMALIZATION_INTERVAL_HR')) / 24 THEN
					SP_NORMALIZE_CALL_IN_TIME(ln_device_id, ln_device_type_id, lv_device_serial_cd, lv_device_time_zone_guid, ln_result_cd, lv_activated_call_in_schedule, lv_non_activ_call_in_schedule, lv_settlement_schedule, lv_dex_schedule, pn_command_id, pv_data_type, ll_command);
					IF ln_result_cd = 1 THEN
						IF ln_device_type_id = PKG_CONST.DEVICE_TYPE__EDGE THEN
							pv_data_type := 'C7';
							ADD_PENDING_FILE_TRANSFER(pv_device_name, pv_device_name || '-CFG-' || DBADMIN.TIMESTAMP_TO_MILLIS(SYSTIMESTAMP), 
								PKG_CONST.FILE_TYPE__PROPERTY_LIST, pv_data_type, 0, 1024, 1, ln_command_id, ln_file_transfer_id, ln_device_file_transfer_id);
							pn_command_id := ln_command_id;
							
							UPDATE DEVICE.FILE_TRANSFER
							SET FILE_TRANSFER_CONTENT = RAWTOHEX(DECODE(lv_settlement_schedule, PKG_CONST.ASCII__NUL, NULL, PKG_CONST.DSP__SETTLEMENT_SCHEDULE || '=' || lv_settlement_schedule || PKG_CONST.ASCII__LF)
							  || DECODE(lv_non_activ_call_in_schedule, PKG_CONST.ASCII__NUL, NULL, PKG_CONST.DSP__NON_ACTIV_CALL_IN_SCHED || '=' || lv_non_activ_call_in_schedule || PKG_CONST.ASCII__LF)
							  || DECODE(lv_activated_call_in_schedule, PKG_CONST.ASCII__NUL, NULL, PKG_CONST.DSP__ACTIVATED_CALL_IN_SCHED || '=' || lv_activated_call_in_schedule || PKG_CONST.ASCII__LF)
							  || DECODE(lv_dex_schedule, PKG_CONST.ASCII__NUL, NULL, PKG_CONST.DSP__DEX_SCHEDULE || '=' || lv_dex_schedule || PKG_CONST.ASCII__LF))
							WHERE FILE_TRANSFER_ID = ln_file_transfer_id;
						ELSE
							UPDATE ENGINE.MACHINE_CMD_PENDING MCP
							SET EXECUTE_CD = 'S', EXECUTE_DATE = SYSDATE, GLOBAL_SESSION_CD = pv_global_session_cd, ATTEMPT_COUNT = COALESCE(ATTEMPT_COUNT, 0) + 1
							WHERE MACHINE_COMMAND_PENDING_ID = pn_command_id;
						END IF;
					END IF;
				END IF;
				IF pn_command_id IS NULL THEN
					SP_NPC_HELPER_MODEM(pv_device_name, pn_command_id, pv_data_type, pv_session_attributes, ln_device_type_id, l_commands_inserted, ll_command); 
					IF pn_command_id IS NULL THEN
						SP_NPC_HELPER_STALE_CONFIG(pv_device_name, pn_command_id, pv_data_type, pv_session_attributes, ln_device_type_id, l_commands_inserted, ll_command);
						IF pn_command_id IS NULL THEN
							SP_NPC_HELPER_ESUDS_DIAG(pv_device_name, pn_command_id, pv_data_type, pv_session_attributes, ln_device_type_id, l_commands_inserted, ll_command);
							IF pn_command_id IS NULL THEN
								SP_NPC_HELPER_PHILLY_COKE(pv_device_name, pn_command_id, pv_data_type, pv_session_attributes, ln_device_type_id, lv_firmware_version, l_commands_inserted, ll_command);
							END IF;
						END IF;
					END IF;
				END IF;
			END IF;
			
			IF pn_command_id IS NULL THEN
				EXIT;
			END IF;
							
			IF ln_device_file_transfer_id > 0 OR pv_data_type = 'FW_UPG' THEN
				IF pv_data_type = 'FW_UPG' OR ln_device_fw_upg_id > 0 THEN
					IF pv_data_type = 'FW_UPG' THEN
						-- cancel existing firmware and bezel app upgrades
						UPDATE ENGINE.MACHINE_CMD_PENDING MCP
						SET EXECUTE_CD = 'C', GLOBAL_SESSION_CD = pv_global_session_cd
						WHERE MACHINE_ID = pv_device_name AND DEVICE_FILE_TRANSFER_ID > 0 AND EXECUTE_ORDER > 0 AND EXISTS (
							SELECT 1 FROM device.device_file_transfer dft
							JOIN device.file_transfer ft ON dft.file_transfer_id = ft.file_transfer_id
							WHERE dft.device_file_transfer_id = mcp.device_file_transfer_id
								AND ft.file_transfer_type_cd IN (PKG_CONST.FILE_TYPE__APP_UPGRADE, PKG_CONST.FILE_TYPE__CARD_READER_APP)
						);
					END IF;
				
					SELECT NVL(MAX(DFU.FIRMWARE_UPGRADE_ID), 0), NVL(MAX(DFU.COMPLETE_FW_UPG_STEP_ID), 0),
						NVL(MAX(DFU.CURRENT_FW_UPG_STEP_ID), 0), NVL(MAX(FUS.FILE_TRANSFER_ID), 0), NVL(MAX(DBMS_LOB.GETLENGTH(FT.FILE_TRANSFER_CONTENT)), 0), NVL(MAX(FUS.STEP_NUMBER), 0),
						MAX(FUS.TARGET_PARAM_CD), MAX(FUS.TARGET_VERSION), MAX(FUS.TARGET_VALUE_REGEX),
						MAX(FUS.FIRMWARE_UPGRADE_STEP_NAME), MAX(FUS.BRANCH_PARAM_CD), MAX(FUS.BRANCH_VALUE_REGEX)
					INTO ln_firmware_upgrade_id, ln_complete_fw_upg_step_id, 
						ln_step_id, ln_file_id, ln_file_size, ln_step_number, lv_target_param_cd, lv_target_version, lv_target_value_regex,
						lv_step_name, lv_branch_param_cd, lv_branch_value_regex
					FROM DEVICE.DEVICE_FIRMWARE_UPGRADE DFU
					LEFT OUTER JOIN DEVICE.FIRMWARE_UPGRADE_STEP FUS ON DFU.CURRENT_FW_UPG_STEP_ID = FUS.FIRMWARE_UPGRADE_STEP_ID AND FUS.STATUS_CD = 'A'
					LEFT OUTER JOIN DEVICE.FILE_TRANSFER FT ON FUS.FILE_TRANSFER_ID = FT.FILE_TRANSFER_ID
					WHERE DFU.DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id AND DFU.DEVICE_FW_UPG_STATUS_CD = 'I';
					
					IF pv_data_type = 'FW_UPG' THEN
						IF ln_firmware_upgrade_id < 1 THEN
							-- device firmware upgrade is no longer incomplete, cancel command
							UPDATE ENGINE.MACHINE_CMD_PENDING
							SET EXECUTE_CD = 'C'
							WHERE MACHINE_COMMAND_PENDING_ID = pn_command_id;
							CONTINUE;
						END IF;
						
						IF ln_step_id = 0 OR ln_step_id = ln_complete_fw_upg_step_id THEN
							-- no current step or current step is complete, find next step
							SELECT NVL(MAX(FIRMWARE_UPGRADE_STEP_ID), 0), NVL(MAX(FILE_TRANSFER_ID), 0), NVL(MAX(DBMS_LOB.GETLENGTH(FILE_TRANSFER_CONTENT)), 0),
								MAX(STEP_NUMBER), MAX(TARGET_PARAM_CD), MAX(TARGET_VERSION), MAX(TARGET_VALUE_REGEX),
								MAX(FIRMWARE_UPGRADE_STEP_NAME), MAX(BRANCH_PARAM_CD), MAX(BRANCH_VALUE_REGEX)
							INTO ln_step_id, ln_file_id, ln_file_size, ln_step_number, lv_target_param_cd, lv_target_version, lv_target_value_regex,
								lv_step_name, lv_branch_param_cd, lv_branch_value_regex
							FROM (
								SELECT FUS.FIRMWARE_UPGRADE_STEP_ID, FUS.FILE_TRANSFER_ID, FT.FILE_TRANSFER_CONTENT, FUS.STEP_NUMBER, FUS.TARGET_PARAM_CD, FUS.TARGET_VERSION,
									FUS.TARGET_VALUE_REGEX, FUS.FIRMWARE_UPGRADE_STEP_NAME, FUS.BRANCH_PARAM_CD, FUS.BRANCH_VALUE_REGEX
								FROM DEVICE.FIRMWARE_UPGRADE_STEP FUS
								JOIN DEVICE.FILE_TRANSFER FT ON FUS.FILE_TRANSFER_ID = FT.FILE_TRANSFER_ID
								WHERE FUS.FIRMWARE_UPGRADE_ID = ln_firmware_upgrade_id AND FUS.STATUS_CD = 'A' AND FUS.STEP_NUMBER > ln_step_number
								ORDER BY FUS.STEP_NUMBER
							) WHERE ROWNUM = 1;
							
							IF ln_step_id > 0 THEN
								UPDATE DEVICE.DEVICE_FIRMWARE_UPGRADE
								SET CURRENT_FW_UPG_STEP_ATTEMPT = 0
								WHERE DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id AND CURRENT_FW_UPG_STEP_ATTEMPT != 0;
							END IF;
						END IF;
					END IF;
					
					IF ln_step_id > 0 THEN
						IF ln_file_size <= 0 THEN
							lv_error_message := 'Size of file ID ' || ln_file_id || ' is ' || ln_file_size || ' bytes';
							UPDATE DEVICE.DEVICE_FIRMWARE_UPGRADE
							SET DEVICE_FW_UPG_STATUS_CD = 'F', DEVICE_FW_UPG_STATUS_TS = SYSDATE, 
								ERROR_MESSAGE = SUBSTR(CASE WHEN ERROR_MESSAGE LIKE '%' || lv_error_message || '%' THEN ERROR_MESSAGE
									WHEN ERROR_MESSAGE IS NULL THEN lv_error_message
									ELSE ERROR_MESSAGE || '; ' || lv_error_message END, 1, 4000)
							WHERE DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id;
							CONTINUE;
						END IF;
						
						IF pv_data_type = 'FW_UPG' THEN
							IF ln_device_type_id = PKG_CONST.DEVICE_TYPE__GX THEN
								lv_current_version := REGEXP_REPLACE(GET_DEVICE_SETTING(ln_device_id, 'Diagnostic App Rev'), 'Diag |Diagnostic = N/A', '');
								lv_app_setting_value := DBADMIN.PKG_GLOBAL.GET_APP_SETTING(CASE lc_comm_method_cd WHEN 'C' THEN 'GX_CDMA' ELSE 'GX' END || '_MIN_DIAGNOSTIC_VERSION_FOR_INTERNAL_FILE_TRANSFER_UPGRADES');
								IF REGEXP_LIKE(lv_current_version, '[0-9]') AND DBADMIN.VERSION_COMPARE(lv_current_version, lv_app_setting_value) < 0 THEN
									lv_error_message := 'Minimum required Gx diagnostic version for internal file transfers is ' || lv_app_setting_value;
									UPDATE DEVICE.DEVICE_FIRMWARE_UPGRADE
									SET DEVICE_FW_UPG_STATUS_CD = 'F', DEVICE_FW_UPG_STATUS_TS = SYSDATE,
										ERROR_MESSAGE = SUBSTR(CASE WHEN ERROR_MESSAGE LIKE '%' || lv_error_message || '%' THEN ERROR_MESSAGE
											WHEN ERROR_MESSAGE IS NULL THEN lv_error_message
											ELSE ERROR_MESSAGE || '; ' || lv_error_message END, 1, 4000)
									WHERE DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id;
									CONTINUE;
								END IF;
								
								lv_current_version := SUBSTR(GET_DEVICE_SETTING(ln_device_id, 'Firmware Version'), 9);
								lv_app_setting_value := DBADMIN.PKG_GLOBAL.GET_APP_SETTING(CASE lc_comm_method_cd WHEN 'C' THEN 'GX_CDMA' ELSE 'GX' END || '_MIN_FIRMWARE_VERSION_FOR_INTERNAL_FILE_TRANSFER_UPGRADES');
								IF REGEXP_LIKE(lv_current_version, '[0-9]') AND DBADMIN.VERSION_COMPARE(lv_current_version, lv_app_setting_value) < 0 THEN
									lv_error_message := 'Minimum required Gx firmware version for internal file transfers is ' || lv_app_setting_value;
									UPDATE DEVICE.DEVICE_FIRMWARE_UPGRADE
									SET DEVICE_FW_UPG_STATUS_CD = 'F', DEVICE_FW_UPG_STATUS_TS = SYSDATE,
										ERROR_MESSAGE = SUBSTR(CASE WHEN ERROR_MESSAGE LIKE '%' || lv_error_message || '%' THEN ERROR_MESSAGE
											WHEN ERROR_MESSAGE IS NULL THEN lv_error_message
											ELSE ERROR_MESSAGE || '; ' || lv_error_message END, 1, 4000)
									WHERE DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id;
									CONTINUE;
								END IF;
							END IF;
						
							UPDATE DEVICE.DEVICE_FIRMWARE_UPGRADE
							SET CURRENT_FW_UPG_STEP_ID = ln_step_id,
								CURRENT_FW_UPG_STEP_TS = SYSDATE,
								CURRENT_FW_UPG_STEP_ATTEMPT = CURRENT_FW_UPG_STEP_ATTEMPT + 1
							WHERE DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id
							RETURNING CURRENT_FW_UPG_STEP_ATTEMPT INTO ln_current_fw_upg_step_attempt;
						
							ln_fw_upg_max_step_attempts := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DEVICE_FIRMWARE_UPGRADE_MAX_STEP_ATTEMPTS'));
							IF ln_current_fw_upg_step_attempt > ln_fw_upg_max_step_attempts THEN
								lv_error_message := 'Exceeded maximum attempts ' || ln_fw_upg_max_step_attempts || ' for step # ' || ln_step_number || ': ' || lv_step_name;
								UPDATE DEVICE.DEVICE_FIRMWARE_UPGRADE
								SET DEVICE_FW_UPG_STATUS_CD = 'F', DEVICE_FW_UPG_STATUS_TS = SYSDATE,
									ERROR_MESSAGE = SUBSTR(CASE WHEN ERROR_MESSAGE LIKE '%' || lv_error_message || '%' THEN ERROR_MESSAGE
										WHEN ERROR_MESSAGE IS NULL THEN lv_error_message
										ELSE ERROR_MESSAGE || '; ' || lv_error_message END, 1, 4000)
								WHERE DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id;
								CONTINUE;
							END IF;
						
							IF lv_branch_param_cd IS NOT NULL AND lv_branch_value_regex IS NOT NULL THEN
								lv_current_value := NULL;
								IF lv_branch_param_cd = 'Bezel Mfgr' THEN
									SELECT MAX(he.host_equipment_mfgr)
									INTO lv_current_value
									FROM device.host h
									JOIN device.host_equipment he ON h.host_equipment_id = he.host_equipment_id
									WHERE h.device_id = ln_device_id AND h.host_type_id = 400;
								END IF;
								
								IF lv_current_value IS NULL THEN
									-- no current value, exit out of firmware upgrade until next session
									lv_error_message := lv_branch_param_cd || ' is NULL';
									UPDATE DEVICE.DEVICE_FIRMWARE_UPGRADE
									SET LAST_GLOBAL_SESSION_CD = pv_global_session_cd,
										ERROR_MESSAGE = SUBSTR(CASE WHEN ERROR_MESSAGE LIKE '%' || lv_error_message || '%' THEN ERROR_MESSAGE
											WHEN ERROR_MESSAGE IS NULL THEN lv_error_message
											ELSE ERROR_MESSAGE || '; ' || lv_error_message END, 1, 4000)
									WHERE DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id;
									CONTINUE;
								END IF;
								
								IF lv_branch_param_cd = 'Bezel Mfgr' AND lv_current_value = 'USAT' THEN
									-- this could be because the bezel firmware upgrade went wrong, try to re-send the bezel firmware file
									SELECT NVL(MAX(FILE_TRANSFER_ID), ln_file_id)
									INTO ln_file_id
									FROM (
										SELECT FT.FILE_TRANSFER_ID
										FROM ENGINE.MACHINE_CMD_PENDING_HIST MCPH
										JOIN DEVICE.DEVICE_FILE_TRANSFER DFT ON MCPH.DEVICE_FILE_TRANSFER_ID = DFT.DEVICE_FILE_TRANSFER_ID
										JOIN DEVICE.FILE_TRANSFER FT ON DFT.FILE_TRANSFER_ID = FT.FILE_TRANSFER_ID
										WHERE MACHINE_ID = pv_device_name AND MCPH.DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id
											AND DFT.DEVICE_FILE_TRANSFER_STATUS_CD = 1 AND FT.FILE_TRANSFER_TYPE_CD = PKG_CONST.FILE_TYPE__CARD_READER_APP
										ORDER BY DFT.DEVICE_FILE_TRANSFER_TS
									) WHERE ROWNUM = 1;
								ELSIF NOT REGEXP_LIKE(lv_current_value, lv_branch_value_regex) THEN
									-- current value doesn't match the branch regex, mark step as complete
									UPDATE DEVICE.DEVICE_FIRMWARE_UPGRADE
									SET COMPLETE_FW_UPG_STEP_ID = ln_step_id,
										COMPLETE_FW_UPG_STEP_TS = SYSDATE
									WHERE DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id;
									CONTINUE;
								END IF;
							END IF;
						END IF;
						
						lv_peek_command := NULL;
						IF lv_target_param_cd IS NOT NULL AND (REGEXP_LIKE(lv_target_version, '[0-9]') OR lv_target_value_regex IS NOT NULL) THEN
							IF lv_target_param_cd = 'Bezel App Rev' THEN
								SELECT MAX(hs.host_setting_value)
								INTO lv_current_value
								FROM device.host h
								JOIN device.host_setting hs ON h.host_id = hs.host_id AND hs.host_setting_parameter = 'Application Version'
								WHERE h.device_id = ln_device_id AND h.host_type_id = 400;
							ELSIF lv_target_param_cd = 'Bezel Mfgr' THEN
								SELECT MAX(he.host_equipment_mfgr)
								INTO lv_current_value
								FROM device.host h
								JOIN device.host_equipment he ON h.host_equipment_id = he.host_equipment_id
								WHERE h.device_id = ln_device_id AND h.host_type_id = 400;
							ELSE
								lv_current_value := GET_DEVICE_SETTING(ln_device_id, lv_target_param_cd);
							END IF;
														
							IF lv_target_param_cd = 'Bezel App Rev' THEN
								lv_peek_command := '4400807EB20000004C';
								lv_current_version := REGEXP_REPLACE(lv_current_value, 'EC5 GR ', '');
							ELSIF lv_target_param_cd = 'Bootloader Rev' THEN
								lv_peek_command := '4400807E9000000021';
								lv_current_version := REGEXP_REPLACE(lv_current_value, 'BtLdr |Bootloader = N/A', '');
							ELSIF lv_target_param_cd = 'Diagnostic App Rev' THEN
								lv_peek_command := '4400807E9000000021';
								lv_current_version := REGEXP_REPLACE(lv_current_value, 'Diag |Diagnostic = N/A', '');
							ELSIF lv_target_param_cd = 'Firmware Version' THEN
								lv_peek_command := '4400007F200000000F';
								lv_current_version := SUBSTR(lv_current_value, 9);
							ELSIF lv_target_param_cd = 'PTest Rev' THEN
								lv_current_version := REGEXP_REPLACE(lv_current_value, 'PTest |PTEST      = N/A', '');
							ELSE
								lv_current_version := lv_current_value;
							END IF;
							
							ln_requirement_met := 0;
							IF REGEXP_LIKE(lv_target_version, '[0-9]') AND lv_target_value_regex IS NOT NULL THEN
								IF DBADMIN.VERSION_COMPARE(lv_current_version, lv_target_version) >= 0 AND REGEXP_LIKE(lv_current_value, lv_target_value_regex) THEN
									ln_requirement_met := 1;
								END IF;
							ELSIF REGEXP_LIKE(lv_target_version, '[0-9]') THEN
								IF DBADMIN.VERSION_COMPARE(lv_current_version, lv_target_version) >= 0 THEN
									ln_requirement_met := 1;
								END IF;
							ELSIF lv_target_value_regex IS NOT NULL THEN								
								IF REGEXP_LIKE(lv_current_value, lv_target_value_regex) THEN
									ln_requirement_met := 1;
								END IF;
							END IF;
							
							IF ln_requirement_met = 1 THEN
								-- target version requirement met, mark step as complete
								UPDATE DEVICE.DEVICE_FIRMWARE_UPGRADE
								SET COMPLETE_FW_UPG_STEP_ID = ln_step_id,
									COMPLETE_FW_UPG_STEP_TS = SYSDATE
								WHERE DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id;
								
								IF pv_data_type != 'FW_UPG' AND ln_device_file_transfer_id > 0 THEN
									-- cancel current pending file transfer if target requirement is met
									UPDATE ENGINE.MACHINE_CMD_PENDING
									SET EXECUTE_CD = 'C'
									WHERE MACHINE_COMMAND_PENDING_ID = pn_command_id;
								END IF;
								
								CONTINUE;
							END IF;
						END IF;
						
						IF pv_data_type = 'FW_UPG' THEN
							IF ln_device_type_id = PKG_CONST.DEVICE_TYPE__GX THEN
								ln_packet_size := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('GX_PACKET_SIZE'));
							ELSIF ln_device_type_id = PKG_CONST.DEVICE_TYPE__KIOSK AND lv_device_serial_cd LIKE 'K3%' THEN
								ln_packet_size := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EPORT_CONNECT_PACKET_SIZE'));
							ELSE
								ln_packet_size := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DEFAULT_PACKET_SIZE'));
							END IF;
							
							IF ln_device_type_id IN (PKG_CONST.DEVICE_TYPE__GX, PKG_CONST.DEVICE_TYPE__KIOSK) THEN
								pv_data_type := 'A4';
							ELSIF ln_device_type_id = PKG_CONST.DEVICE_TYPE__EDGE THEN
								IF ln_file_size > ln_packet_size THEN
									pv_data_type := 'C8';
								ELSE
									pv_data_type := 'C7';
								END IF;
							ELSE
								pv_data_type := '7C';
							END IF;
						
							SELECT SEQ_DEVICE_FILE_TRANSFER_ID.NEXTVAL INTO ln_device_file_transfer_id FROM DUAL;
						
							INSERT INTO device.device_file_transfer(device_file_transfer_id, device_id, file_transfer_id, device_file_transfer_direct, device_file_transfer_status_cd, device_file_transfer_pkt_size)
							VALUES(ln_device_file_transfer_id, ln_device_id, ln_file_id, 'O', 0, ln_packet_size);
							
							SELECT ENGINE.SEQ_MACHINE_CMD_PENDING_ID.NEXTVAL INTO pn_command_id FROM DUAL;
							
							INSERT INTO engine.machine_cmd_pending (machine_command_pending_id, machine_id, data_type, execute_cd, execute_order, execute_date, global_session_cd, device_file_transfer_id, device_firmware_upgrade_id) 
							VALUES(pn_command_id, pv_device_name, pv_data_type, 'S', -2, SYSDATE, pv_global_session_cd, ln_device_file_transfer_id, ln_device_fw_upg_id);
							
							IF ln_device_type_id != PKG_CONST.DEVICE_TYPE__GX OR lv_target_param_cd IS NULL OR lv_target_version IS NULL AND lv_target_value_regex IS NULL THEN
								-- applayer will mark step as complete when it receives file download ack from device
								UPDATE DEVICE.DEVICE_FIRMWARE_UPGRADE
								SET CURRENT_PENDING_COMMAND_ID = pn_command_id
								WHERE DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id;
							END IF;
							
							IF lv_peek_command IS NOT NULL AND ln_device_type_id = PKG_CONST.DEVICE_TYPE__GX THEN
								INSERT INTO engine.machine_cmd_pending (machine_id, data_type, command, execute_cd, execute_order, device_firmware_upgrade_id) 
								VALUES(pv_device_name, '87', lv_peek_command, 'P', -1, ln_device_fw_upg_id);
							END IF;
						END IF;
					ELSIF pv_data_type = 'FW_UPG' THEN
						UPDATE DEVICE.DEVICE_FIRMWARE_UPGRADE
						SET DEVICE_FW_UPG_STATUS_CD = 'A', DEVICE_FW_UPG_STATUS_TS = SYSDATE
						WHERE DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id;
						
						UPDATE ENGINE.MACHINE_CMD_PENDING
						SET EXECUTE_CD = 'A'
						WHERE MACHINE_COMMAND_PENDING_ID = pn_command_id;
						
						CONTINUE;
					END IF;
				END IF;
				
				IF ln_device_file_transfer_id > 0 THEN
					SELECT MAX(ft.FILE_TRANSFER_ID), MAX(ft.FILE_TRANSFER_NAME), MAX(ft.FILE_TRANSFER_TYPE_CD),
						   MAX(dft.DEVICE_FILE_TRANSFER_GROUP_NUM), MAX(dft.DEVICE_FILE_TRANSFER_PKT_SIZE), MAX(dft.CREATED_TS)
					  INTO pn_file_transfer_id, pv_file_transfer_name, pn_file_transfer_type_id,
						   pn_file_transfer_group_num, pn_file_transfer_pkt_size, pd_file_transfer_created_ts
					  FROM DEVICE.DEVICE_FILE_TRANSFER dft
					  JOIN DEVICE.FILE_TRANSFER ft on dft.FILE_TRANSFER_ID = ft.FILE_TRANSFER_ID
					 WHERE DEVICE_FILE_TRANSFER_ID = ln_device_file_transfer_id;
					 
					IF pn_file_transfer_id IS NULL THEN
						-- device file transfer no longer exists, cancel command
						UPDATE ENGINE.MACHINE_CMD_PENDING
						SET EXECUTE_CD = 'C'
						WHERE MACHINE_COMMAND_PENDING_ID = pn_command_id;
						CONTINUE;
					END IF;
										
					IF pn_file_transfer_type_id = PKG_CONST.FILE_TYPE__APP_UPGRADE THEN
						ln_cancel_command := 0;
						IF ln_device_type_id = PKG_CONST.DEVICE_TYPE__GX THEN
							SELECT COALESCE(UTL_RAW.CAST_TO_VARCHAR2(DBMS_LOB.SUBSTR(FILE_TRANSFER_CONTENT, 19, 82)), '-')
							INTO lv_new_firmware_version
							FROM DEVICE.FILE_TRANSFER
							WHERE FILE_TRANSFER_ID = pn_file_transfer_id;						
						
							IF lv_new_firmware_version NOT LIKE 'USA-%' AND lv_new_firmware_version NOT LIKE 'Diag%' AND lv_new_firmware_version NOT LIKE 'PTest%' THEN
								ln_cancel_command := 1;
							END IF;
						ELSIF ln_device_type_id = PKG_CONST.DEVICE_TYPE__EDGE THEN
							SELECT COALESCE(UTL_RAW.CAST_TO_VARCHAR2(DBMS_LOB.SUBSTR(FILE_TRANSFER_CONTENT, 10, 93)), '-')
							INTO lv_new_firmware_version
							FROM DEVICE.FILE_TRANSFER
							WHERE FILE_TRANSFER_ID = pn_file_transfer_id;
							
							IF lv_device_serial_cd LIKE 'VJ%' THEN
								IF lv_new_firmware_version NOT LIKE '2.02.%' THEN
									ln_cancel_command := 1;
								END IF;
								IF lc_comm_method_cd = 'G' THEN
									IF DBADMIN.VERSION_COMPARE(lv_new_firmware_version, '2.02.008i') < 0 THEN
										ln_cancel_command := 1;
									END IF;
								END IF;
							ELSIF lv_device_serial_cd LIKE 'EE%' THEN
								IF lc_comm_method_cd = 'C' THEN
									IF lv_new_firmware_version NOT LIKE '1.02.%' THEN
										ln_cancel_command := 1;
									END IF;
								ELSIF lc_comm_method_cd = 'G' THEN
									IF lv_new_firmware_version NOT LIKE '1.00.%' AND lv_new_firmware_version NOT LIKE '1.01.%' THEN
										ln_cancel_command := 1;
									END IF;
								END IF;
							END IF;
						END IF;
						
						IF ln_cancel_command = 1 THEN
							-- incorrect file download, cancel command
							UPDATE ENGINE.MACHINE_CMD_PENDING
							SET EXECUTE_CD = 'C'
							WHERE MACHINE_COMMAND_PENDING_ID = pn_command_id;
							CONTINUE;
						END IF;
					END IF;

					SP_GET_FILE_TRANSFER_BLOB(pn_file_transfer_id, pl_file_transfer_content);
				ELSE
					CONTINUE;
				END IF;
			ELSIF pv_data_type = '88' THEN
				l_addr := to_number(substr(ll_command, 3, 8), 'XXXXXXXX');
				IF substr(ll_command, 1, 2) = '42' THEN -- EEROM: get data from device setting
					l_len := to_number(substr(ll_command, 11, 8), 'XXXXXXXX') * 2;
		
					IF ln_device_type_id in (PKG_CONST.DEVICE_TYPE__G4, PKG_CONST.DEVICE_TYPE__GX) THEN
						  l_pos := l_addr * 2;
					ELSE
						  l_pos := l_addr;
					END IF;
		
					l_pos := l_pos * 2 + 1;
		
					SP_GET_MAP_CONFIG_FILE(ln_device_id, lv_file_content_hex);
					pr_command_bytes := UTL_RAW.CONCAT(hextoraw(substr(ll_command, 1, 10)), SUBSTR(lv_file_content_hex, l_pos, l_len));
				ELSE
					pr_command_bytes := HEXTORAW(ll_command);
				END IF;
			ELSE
				pr_command_bytes := HEXTORAW(ll_command);
			END IF;
			
			EXIT;
		END LOOP;
		
		IF pn_command_id IS NOT NULL THEN
			IF lv_device_serial_cd LIKE 'K3%' THEN
				UPDATE DEVICE.DEVICE_INFO
				SET LAST_PENDING_COMMAND_ID = TO_NUMBER(pn_command_id),
					LAST_PENDING_CMD_SESSION_CD = pv_global_session_cd
				WHERE DEVICE_NAME = pv_device_name;
				
				IF SQL%NOTFOUND THEN
					BEGIN
						INSERT INTO DEVICE.DEVICE_INFO(DEVICE_NAME, LAST_PENDING_COMMAND_ID, LAST_PENDING_CMD_SESSION_CD)
						VALUES(pv_device_name, TO_NUMBER(pn_command_id), pv_global_session_cd);
					EXCEPTION
						WHEN DUP_VAL_ON_INDEX THEN
							UPDATE DEVICE.DEVICE_INFO
							SET LAST_PENDING_COMMAND_ID = TO_NUMBER(pn_command_id),
								LAST_PENDING_CMD_SESSION_CD = pv_global_session_cd
							WHERE DEVICE_NAME = pv_device_name;
					END;
				END IF;
			END IF;
		END IF;
    END;
    
    /**
      * r29+ version that doesn't return command id but does return row count
      */
    PROCEDURE UPSERT_PENDING_COMMAND(
        pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
        pv_date_type IN ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pv_command IN ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE,
        pv_execute_order IN ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE DEFAULT 999,
        pn_rows_inserted OUT PLS_INTEGER)
    IS
        ln_command_id ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE;
    BEGIN
      UPSERT_PENDING_COMMAND(pv_device_name,pv_date_type,pv_command,'P',pv_execute_order,pn_rows_inserted,ln_command_id);
    END;
    
    /** r29+ version
      * Inserts a pending command into the table, and returns a command id,
      * IF it does not already exist.
      */      
    PROCEDURE UPSERT_PENDING_COMMAND(
        pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
        pv_data_type IN ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pv_command IN ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE,
        pv_execute_cd IN ENGINE.MACHINE_CMD_PENDING.EXECUTE_CD%TYPE,
        pv_execute_order IN ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE DEFAULT 999,
        pn_rows_inserted OUT PLS_INTEGER,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE)
    IS
	  ld_execute_date ENGINE.MACHINE_CMD_PENDING.EXECUTE_DATE%TYPE;
	  ln_cnt NUMBER;
    BEGIN
		SELECT COUNT(1)
		INTO ln_cnt
		FROM ENGINE.MACHINE_CMD_PENDING
		WHERE MACHINE_ID = pv_device_name
			AND DATA_TYPE = pv_data_type
			AND DBADMIN.PKG_UTL.EQL(COMMAND, pv_command) = 'Y';
	
		IF ln_cnt = 0 THEN
			SELECT DECODE(pv_execute_cd, 'S', SYSDATE, NULL) INTO ld_execute_date FROM DUAL;
        
			-- This isn't atomic so could result in duplicate pending commands but it's not critical to avoid duplicates only nice
			INSERT INTO ENGINE.MACHINE_CMD_PENDING(MACHINE_COMMAND_PENDING_ID, MACHINE_ID, DATA_TYPE, COMMAND, EXECUTE_CD, EXECUTE_DATE, EXECUTE_ORDER)
			VALUES(SEQ_MACHINE_CMD_PENDING_ID.NEXTVAL, pv_device_name, pv_data_type, pv_command, pv_execute_cd, ld_execute_date, pv_execute_order)
			RETURNING MACHINE_COMMAND_PENDING_ID INTO pn_command_id;
        
			pn_rows_inserted := SQL%ROWCOUNT;
		ELSE
			pn_rows_inserted := 0;
		END IF;
    END;
    
    PROCEDURE CONFIG_POKE
    (pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
     pv_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE)
    IS
        l_command ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE;
        l_file_len NUMBER;
        l_num_parts NUMBER;
        l_pos NUMBER;
        l_poke_size NUMBER:=200;
        l_len NUMBER;
		ln_rows_inserted PLS_INTEGER;
        ln_command_id ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE;
    BEGIN
        IF pv_device_type_id in (0, 1) THEN
            UPDATE ENGINE.MACHINE_CMD_PENDING
            SET EXECUTE_CD='C'
            WHERE MACHINE_ID = pv_device_name
            AND DATA_TYPE = '88'
            AND EXECUTE_ORDER > 2
            AND (EXECUTE_CD = 'P' OR (EXECUTE_CD = 'S' AND EXECUTE_DATE < (SYSDATE-(90/86400))));

            UPSERT_PENDING_COMMAND(pv_device_name, '88', '420000000000000088', 'P', -100, ln_rows_inserted, ln_command_id);
            UPSERT_PENDING_COMMAND(pv_device_name, '88', '42000000470000008E', 'P', -99, ln_rows_inserted, ln_command_id);
            UPSERT_PENDING_COMMAND(pv_device_name, '88', '42000000B20000009C', 'P', -98, ln_rows_inserted, ln_command_id);
        END IF;
    END;
    
    FUNCTION GET_DEVICE_ID_BY_NAME(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pd_effective_date DEVICE.CREATED_TS%TYPE
    )
    RETURN DEVICE.DEVICE_ID%TYPE
    IS
        ln_device_id DEVICE.DEVICE_ID%TYPE;
    BEGIN
        SELECT MAX(DEVICE_ID)
          INTO ln_device_id
          FROM DEVICE.DEVICE
         WHERE DEVICE_ACTIVE_YN_FLAG = 'Y'
           AND DEVICE_NAME = pv_device_name
           AND CREATED_TS <= pd_effective_date;
        IF ln_device_id IS NULL THEN
            SELECT MAX(DEVICE_ID)
              INTO ln_device_id
              FROM (SELECT DEVICE_ID, CREATED_TS, NVL(LEAD(CREATED_TS) OVER (PARTITION BY DEVICE_NAME ORDER BY CREATED_TS), MAX_DATE) NEXT_CREATED_TS
                      FROM DEVICE.DEVICE
                     WHERE DEVICE_NAME = pv_device_name)
             WHERE CREATED_TS <= pd_effective_date
               AND NEXT_CREATED_TS > pd_effective_date;
            IF ln_device_id IS NULL THEN
                SELECT MAX(DEVICE_ID)
                  INTO ln_device_id
                  FROM (SELECT DEVICE_ID
                          FROM DEVICE.DEVICE
                         WHERE DEVICE_NAME = pv_device_name
                           AND CREATED_TS > pd_effective_date
                         ORDER BY CREATED_TS ASC)
                 WHERE ROWNUM = 1;
            END IF;
        END IF;
        RETURN ln_device_id;        
    END;
    
    FUNCTION GET_DEVICE_ID_BY_SERIAL(
        pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
        pd_effective_date DEVICE.CREATED_TS%TYPE
    )
    RETURN DEVICE.DEVICE_ID%TYPE
    IS
        ln_device_id DEVICE.DEVICE_ID%TYPE;
    BEGIN
        SELECT MAX(DEVICE_ID)
          INTO ln_device_id
          FROM DEVICE.DEVICE
         WHERE DEVICE_ACTIVE_YN_FLAG = 'Y'
           AND DEVICE_SERIAL_CD = pv_device_serial_cd
           AND CREATED_TS <= pd_effective_date;
        IF ln_device_id IS NULL THEN
            SELECT MAX(DEVICE_ID)
              INTO ln_device_id
              FROM (SELECT DEVICE_ID, CREATED_TS, NVL(LEAD(CREATED_TS) OVER (PARTITION BY DEVICE_SERIAL_CD ORDER BY CREATED_TS), MAX_DATE) NEXT_CREATED_TS
                      FROM DEVICE.DEVICE
                     WHERE DEVICE_SERIAL_CD = pv_device_serial_cd)
             WHERE CREATED_TS <= pd_effective_date
               AND NEXT_CREATED_TS > pd_effective_date;
        END IF;
        RETURN ln_device_id;        
    END;
	
	PROCEDURE CLONE_GX_CONFIG (
		pn_source_device_id IN DEVICE.DEVICE_ID%TYPE,
		pn_target_device_id IN DEVICE.DEVICE_ID%TYPE,
		pn_part1_changed_count OUT NUMBER,
		pn_part2_changed_count OUT NUMBER,
		pn_part3_changed_count OUT NUMBER,
		pn_counters_changed_count OUT NUMBER
	)
	IS
	BEGIN
		/*
		 * 	# we are sending very specific sections of the config with the offsets below, not the whole thing for Gx
		 *	#	0 	- 135
		 *	#	142 - 283
		 *	#	356 - 511
		 */	
		MERGE INTO DEVICE.DEVICE_SETTING O
		USING (
			SELECT DS_T.DEVICE_ID, DS_T.DEVICE_SETTING_PARAMETER_CD, DS_S.DEVICE_SETTING_VALUE
			FROM DEVICE.DEVICE_SETTING DS_T
			JOIN DEVICE.DEVICE_SETTING DS_S ON DS_T.DEVICE_SETTING_PARAMETER_CD = DS_S.DEVICE_SETTING_PARAMETER_CD
			WHERE DS_T.DEVICE_ID = pn_target_device_id
				AND DS_S.DEVICE_ID = pn_source_device_id
				AND DS_S.DEVICE_SETTING_PARAMETER_CD IN (
					SELECT DEVICE_SETTING_PARAMETER_CD
					FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS
					WHERE DEVICE_TYPE_ID = 0
						AND FIELD_OFFSET BETWEEN 0 AND 135
				) AND DBADMIN.PKG_UTL.EQL(DS_S.DEVICE_SETTING_VALUE, DS_T.DEVICE_SETTING_VALUE) = 'N') N
		ON (O.DEVICE_ID = N.DEVICE_ID AND O.DEVICE_SETTING_PARAMETER_CD = N.DEVICE_SETTING_PARAMETER_CD)
		WHEN MATCHED THEN
			UPDATE
				SET O.DEVICE_SETTING_VALUE = N.DEVICE_SETTING_VALUE;
		pn_part1_changed_count := SQL%ROWCOUNT;
		
		MERGE INTO DEVICE.DEVICE_SETTING O
		USING (
			SELECT DS_T.DEVICE_ID, DS_T.DEVICE_SETTING_PARAMETER_CD, DS_S.DEVICE_SETTING_VALUE
			FROM DEVICE.DEVICE_SETTING DS_T
			JOIN DEVICE.DEVICE_SETTING DS_S ON DS_T.DEVICE_SETTING_PARAMETER_CD = DS_S.DEVICE_SETTING_PARAMETER_CD
			WHERE DS_T.DEVICE_ID = pn_target_device_id
				AND DS_S.DEVICE_ID = pn_source_device_id
				AND DS_S.DEVICE_SETTING_PARAMETER_CD IN (
					SELECT DEVICE_SETTING_PARAMETER_CD
					FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS
					WHERE DEVICE_TYPE_ID = 0
						AND FIELD_OFFSET BETWEEN 142 AND 283
				) AND DBADMIN.PKG_UTL.EQL(DS_S.DEVICE_SETTING_VALUE, DS_T.DEVICE_SETTING_VALUE) = 'N') N
		ON (O.DEVICE_ID = N.DEVICE_ID AND O.DEVICE_SETTING_PARAMETER_CD = N.DEVICE_SETTING_PARAMETER_CD)
		WHEN MATCHED THEN
			UPDATE
				SET O.DEVICE_SETTING_VALUE = N.DEVICE_SETTING_VALUE;
		pn_part2_changed_count := SQL%ROWCOUNT;
		
		MERGE INTO DEVICE.DEVICE_SETTING O
		USING (
			SELECT DS_T.DEVICE_ID, DS_T.DEVICE_SETTING_PARAMETER_CD, DS_S.DEVICE_SETTING_VALUE
			FROM DEVICE.DEVICE_SETTING DS_T
			JOIN DEVICE.DEVICE_SETTING DS_S ON DS_T.DEVICE_SETTING_PARAMETER_CD = DS_S.DEVICE_SETTING_PARAMETER_CD
			WHERE DS_T.DEVICE_ID = pn_target_device_id
				AND DS_S.DEVICE_ID = pn_source_device_id
				AND DS_S.DEVICE_SETTING_PARAMETER_CD IN (
					SELECT DEVICE_SETTING_PARAMETER_CD
					FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS
					WHERE DEVICE_TYPE_ID = 0
						AND FIELD_OFFSET BETWEEN 356 AND 511
				) AND DBADMIN.PKG_UTL.EQL(DS_S.DEVICE_SETTING_VALUE, DS_T.DEVICE_SETTING_VALUE) = 'N') N
		ON (O.DEVICE_ID = N.DEVICE_ID AND O.DEVICE_SETTING_PARAMETER_CD = N.DEVICE_SETTING_PARAMETER_CD)
		WHEN MATCHED THEN
			UPDATE
				SET O.DEVICE_SETTING_VALUE = N.DEVICE_SETTING_VALUE;
		pn_part3_changed_count := SQL%ROWCOUNT;
		
		UPDATE DEVICE.DEVICE_SETTING
		SET DEVICE_SETTING_VALUE = '00000000'
		WHERE DEVICE_ID = pn_target_device_id
			AND DEVICE_SETTING_PARAMETER_CD IN (
				SELECT DEVICE_SETTING_PARAMETER_CD
				FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS
				WHERE DEVICE_TYPE_ID = 0
					AND FIELD_OFFSET BETWEEN 320 AND 352
			) AND DEVICE_SETTING_VALUE != '00000000';
		pn_counters_changed_count := SQL%ROWCOUNT;
	END;
    
    PROCEDURE INITIALIZE_DEVICE(
        pv_device_serial_cd IN DEVICE.DEVICE_SERIAL_CD%TYPE,
        pn_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
        pc_preregistering IN CHAR, 
        pn_device_id OUT DEVICE.DEVICE_ID%TYPE,
        pv_device_name OUT DEVICE.DEVICE_NAME%TYPE,
        pv_device_type_desc OUT DEVICE_TYPE.DEVICE_TYPE_DESC%TYPE,
        pn_device_sub_type_id OUT DEVICE.DEVICE_SUB_TYPE_ID%TYPE,
        pc_prev_device_active_yn_flag OUT VARCHAR,
        pc_master_id_always_inc OUT VARCHAR,
        pn_key_gen_time OUT NUMBER,
        pc_legacy_safe_key OUT VARCHAR,
        pv_time_zone_guid OUT TIME_ZONE.TIME_ZONE_GUID%TYPE,
        pn_new_host_count OUT NUMBER,
        pn_result_cd OUT NUMBER,
        pv_error_message OUT VARCHAR2)
    IS
        ln_pos_pta_tmpl_id DEVICE_TYPE.POS_PTA_TMPL_ID%TYPE;
        lc_require_preregister DEVICE_TYPE.REQUIRE_PREREGISTER%TYPE;
        ln_prev_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE;
        ln_prev_device_sub_type_id  DEVICE.DEVICE_SUB_TYPE_ID%TYPE;
        ln_pos_id PSS.POS.POS_ID%TYPE;
    BEGIN
        BEGIN
            SELECT DEVICE_SUB_TYPE_ID, DEVICE_SUB_TYPE_DESC, POS_PTA_TMPL_ID, REQUIRE_PREREGISTER, MASTER_ID_ALWAYS_INC
             INTO pn_device_sub_type_id, pv_device_type_desc, ln_pos_pta_tmpl_id, lc_require_preregister, pc_master_id_always_inc
             FROM 
                (SELECT 1 PRIORITY, DEVICE_SUB_TYPE_ID, DEVICE_SUB_TYPE_DESC, POS_PTA_TMPL_ID, REQUIRE_PREREGISTER, MASTER_ID_ALWAYS_INC
                  FROM DEVICE.DEVICE_SUB_TYPE
                 WHERE DEVICE_TYPE_ID = pn_device_type_id
                   AND REGEXP_LIKE(pv_device_serial_cd, DEVICE_SERIAL_CD_REGEX)
                UNION ALL 
                SELECT 2, NULL, DEVICE_TYPE_DESC, POS_PTA_TMPL_ID, REQUIRE_PREREGISTER, MASTER_ID_ALWAYS_INC
                  FROM DEVICE.DEVICE_TYPE
                 WHERE DEVICE_TYPE_ID = pn_device_type_id
                   AND REGEXP_LIKE(pv_device_serial_cd, DEVICE_TYPE_SERIAL_CD_REGEX)
             ORDER BY PRIORITY)
            WHERE ROWNUM = 1;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                pn_result_cd := PKG_CONST.RESULT__FAILURE;
                pv_error_message := 'No match for device type and serial cd';
                RETURN;
        END;
        BEGIN
            SELECT DEVICE_ID, DEVICE_NAME, DEVICE_TYPE_ID, DEVICE_SUB_TYPE_ID, CASE WHEN REGEXP_LIKE(ENCRYPTION_KEY, '^[0-9]{16}$') THEN 'Y' ELSE 'N' END, DEVICE_ACTIVE_YN_FLAG, DATE_TO_MILLIS(DEVICE_ENCR_KEY_GEN_TS)
              INTO pn_device_id, pv_device_name, ln_prev_device_type_id, ln_prev_device_sub_type_id, pc_legacy_safe_key, pc_prev_device_active_yn_flag, pn_key_gen_time
              FROM (SELECT * 
                      FROM DEVICE.DEVICE
                     WHERE DEVICE_SERIAL_CD = pv_device_serial_cd
                     ORDER BY DEVICE_ACTIVE_YN_FLAG DESC, DEVICE_ID DESC)
              WHERE ROWNUM = 1;
            IF ln_prev_device_type_id != pn_device_type_id OR NVL(ln_prev_device_sub_type_id,0) != NVL(pn_device_sub_type_id, 0) THEN
                UPDATE DEVICE.DEVICE
                   SET DEVICE_TYPE_ID = pn_device_type_id,
                       DEVICE_SUB_TYPE_ID = pn_device_sub_type_id
                 WHERE DEVICE_ID = pn_device_id;
            END IF;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                IF pc_preregistering != 'Y' AND lc_require_preregister = 'Y' THEN
                    pn_result_cd := PKG_CONST.RESULT__FAILURE;
                    pv_error_message := 'Device type ''' || pv_device_type_desc ||''' requires pre-registration and this device is not registered';
                    RETURN;
                END IF;
                SELECT SEQ_DEVICE_ID.NEXTVAL, SF_GENERATE_DEVICE_NAME(pn_device_type_id, pn_device_sub_type_id)
                  INTO pn_device_id, pv_device_name
                  FROM DUAL;
                INSERT INTO DEVICE.DEVICE(DEVICE_ID, DEVICE_NAME, DEVICE_TYPE_ID, DEVICE_SUB_TYPE_ID, DEVICE_SERIAL_CD, DEVICE_ACTIVE_YN_FLAG)
                    VALUES(pn_device_id, pv_device_name, pn_device_type_id, pn_device_sub_type_id, pv_device_serial_cd, 'Y');
        END;
        BEGIN
            SELECT P.POS_ID
              INTO ln_pos_id
              FROM PSS.POS P
             WHERE P.DEVICE_ID = pn_device_id;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                SELECT PSS.SEQ_POS_ID.NEXTVAL
                  INTO ln_pos_id
                  FROM DUAL; 
                INSERT INTO PSS.POS(POS_ID, DEVICE_ID) 
                    VALUES(ln_pos_id, pn_device_id);
	    END;
      SELECT MAX(T_TZ.TIME_ZONE_GUID)
        INTO pv_time_zone_guid
        FROM REPORT.EPORT E
        JOIN REPORT.VW_TERMINAL_EPORT TE ON E.EPORT_ID = TE.EPORT_ID
        JOIN REPORT.TERMINAL T ON TE.TERMINAL_ID = T.TERMINAL_ID
        JOIN REPORT.TIME_ZONE T_TZ ON T.TIME_ZONE_ID = T_TZ.TIME_ZONE_ID
       WHERE E.EPORT_SERIAL_NUM = pv_device_serial_cd;
      IF pv_time_zone_guid IS NULL THEN
          SELECT MAX(TZ.TIME_ZONE_GUID)
            INTO pv_time_zone_guid
            FROM PSS.POS P
            JOIN LOCATION.LOCATION L ON P.LOCATION_ID = L.LOCATION_ID
            JOIN LOCATION.TIME_ZONE TZ ON L.LOCATION_TIME_ZONE_CD = TZ.TIME_ZONE_CD
           WHERE P.POS_ID = ln_pos_id;
      END IF;
        SP_CREATE_DEFAULT_HOSTS(pn_device_id, pn_new_host_count, pn_result_cd, pv_error_message);
        IF pn_result_cd != PKG_CONST.RESULT__SUCCESS THEN
            RETURN;
        END IF;
        PSS.PKG_POS_PTA.SP_IMPORT_POS_PTA_TEMPLATE(pn_device_id, ln_pos_pta_tmpl_id, 'S', 'AE', 'N', pn_result_cd, pv_error_message);
        IF pn_result_cd != PKG_CONST.RESULT__SUCCESS THEN
            RETURN;
        END IF;
        pn_result_cd := PKG_CONST.RESULT__SUCCESS;
        pv_error_message := PKG_CONST.ERROR__NO_ERROR;
    END;
    
    PROCEDURE NEXT_MASTER_ID_BY_SERIAL(
        pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
        pv_device_name OUT DEVICE.DEVICE_NAME%TYPE,
        pn_master_id OUT NUMBER)
    IS
        ln_device_id DEVICE.DEVICE_ID%TYPE;
    BEGIN
        -- increment and set masterId
        SELECT DEVICE_ID, DEVICE_NAME
          INTO ln_device_id, pv_device_name
          FROM DEVICE.DEVICE
         WHERE DEVICE_SERIAL_CD = pv_device_serial_cd
           AND DEVICE_ACTIVE_YN_FLAG = 'Y';
        LOOP
            UPDATE DEVICE.DEVICE_SETTING
               SET DEVICE_SETTING_VALUE = GREATEST(DATE_TO_MILLIS(SYSDATE) / 1000, NVL(TO_NUMBER_OR_NULL(DEVICE_SETTING_VALUE), 0)) + 1
             WHERE DEVICE_SETTING_PARAMETER_CD = 'VIRTUAL_MASTER_ID'
               AND DEVICE_ID = ln_device_id
             RETURNING TO_NUMBER_OR_NULL(DEVICE_SETTING_VALUE)
              INTO pn_master_id;
            IF pn_master_id IS NOT NULL THEN
                RETURN;
            END IF;
            pn_master_id := DATE_TO_MILLIS(SYSDATE) / 1000;
            BEGIN
                INSERT INTO DEVICE.DEVICE_SETTING(DEVICE_ID, DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_VALUE)
                    VALUES(ln_device_id, 'VIRTUAL_MASTER_ID', pn_master_id);
                RETURN;
            EXCEPTION
                WHEN DUP_VAL_ON_INDEX THEN
                    pn_master_id := NULL;
            END;
        END LOOP;    
    END;
	
	PROCEDURE GET_OR_CREATE_PREPAID_DEVICE(
		pn_customer_id IN PSS.POS.CUSTOMER_ID%TYPE,
		pn_location_id IN PSS.POS.LOCATION_ID%TYPE,
		pn_corp_customer_id IN PSS.CONSUMER_ACCT.CORP_CUSTOMER_ID%TYPE,
		pv_currency_cd IN PSS.CONSUMER_ACCT.CURRENCY_CD%TYPE,
		pn_device_id OUT DEVICE.DEVICE_ID%TYPE,
		pv_device_serial_cd OUT DEVICE.DEVICE_SERIAL_CD%TYPE,
		pv_device_name OUT DEVICE.DEVICE_NAME%TYPE
	)
	IS
        lv_device_type_desc DEVICE_TYPE.DEVICE_TYPE_DESC%TYPE;
        ln_device_sub_type_id DEVICE.DEVICE_SUB_TYPE_ID%TYPE;
        lc_prev_device_active_yn_flag VARCHAR(1);
        lc_master_id_always_inc VARCHAR(1);
        ln_key_gen_time NUMBER;
        lc_legacy_safe_key VARCHAR(1);
        lv_time_zone_guid TIME_ZONE.TIME_ZONE_GUID%TYPE;
        ln_new_host_count NUMBER;
        ln_result_cd NUMBER;
        lv_error_message VARCHAR2(1000);
		lv_lock VARCHAR2(128);
		ln_terminal_id REPORT.TERMINAL.TERMINAL_ID%TYPE;
		ln_dealer_id CORP.DEALER.DEALER_ID%TYPE;
		ln_eport_id REPORT.EPORT.EPORT_ID%TYPE;
		ln_customer_bank_id CORP.CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE;
        lv_customer_address1 CORP.CUSTOMER_ADDR.ADDRESS1%TYPE;                                                                                                                                                               
        lv_customer_city CORP.CUSTOMER_ADDR.CITY%TYPE;                                                                                                                                                                              
        lv_customer_state_cd CORP.CUSTOMER_ADDR.STATE%TYPE;                                                                                                                                                                              
        lv_customer_postal CORP.CUSTOMER_ADDR.ZIP%TYPE;                                                                                                                                                                              
        lv_customer_country_cd CORP.CUSTOMER_ADDR.COUNTRY_CD%TYPE;                                                                                                                                                                              				
	BEGIN		
		pv_device_serial_cd := 'V1-' || pn_corp_customer_id || '-' || pv_currency_cd;
		
		SELECT MAX(D.DEVICE_ID), MAX(D.DEVICE_NAME)
		INTO pn_device_id, pv_device_name
		FROM DEVICE.DEVICE D
		JOIN REPORT.EPORT E ON D.DEVICE_SERIAL_CD = E.EPORT_SERIAL_NUM
		JOIN REPORT.VW_TERMINAL_EPORT TE ON E.EPORT_ID = TE.EPORT_ID
		JOIN REPORT.TERMINAL T ON TE.TERMINAL_ID = T.TERMINAL_ID
		WHERE D.DEVICE_SERIAL_CD = pv_device_serial_cd 
			AND D.DEVICE_ACTIVE_YN_FLAG = 'Y'
			AND T.CUSTOMER_ID = pn_corp_customer_id;
		
		IF pn_device_id > 0 THEN
			RETURN;
		END IF;
	
		lv_lock := PKG_GLOBAL.REQUEST_LOCK('DEVICE.DEVICE', pv_device_serial_cd);
	
		INITIALIZE_DEVICE(pv_device_serial_cd, 14, 'Y',
			pn_device_id,
			pv_device_name,
			lv_device_type_desc,
			ln_device_sub_type_id,
			lc_prev_device_active_yn_flag,
			lc_master_id_always_inc,
			ln_key_gen_time,
			lc_legacy_safe_key,
			lv_time_zone_guid,
			ln_new_host_count,
			ln_result_cd,
			lv_error_message		
		);
		
		UPDATE PSS.POS
		SET CUSTOMER_ID = pn_customer_id, LOCATION_ID = pn_location_id
		WHERE DEVICE_ID = pn_device_id;
		
		IF pn_device_id > 0 THEN
			SELECT MAX(DEALER_ID)
			INTO ln_dealer_id
			FROM (
				SELECT DEALER_ID
				FROM CORP.DEALER_LICENSE
				WHERE LICENSE_ID IN (
					SELECT MAX(LICENSE_ID)
					FROM (
						SELECT L.LICENSE_ID
						FROM CORP.CUSTOMER_LICENSE CL
						JOIN CORP.LICENSE_NBR LN ON CL.LICENSE_NBR = LN.LICENSE_NBR
						JOIN CORP.LICENSE L ON LN.LICENSE_ID = L.LICENSE_ID
						WHERE CL.CUSTOMER_ID = pn_corp_customer_id AND L.STATUS = 'A'
						ORDER BY CL.RECEIVED DESC
					) WHERE ROWNUM = 1
				)
				AND SYSDATE >= NVL(START_DATE, MIN_DATE)
				AND SYSDATE < NVL(END_DATE, MAX_DATE)
				ORDER BY START_DATE DESC, DEALER_ID DESC
			) WHERE ROWNUM = 1;
			
			IF ln_dealer_id IS NULL THEN
				SELECT DEALER_ID
				INTO ln_dealer_id
				FROM (
					SELECT DEALER_ID
					FROM CORP.DEALER
					WHERE DEALER_NAME = 'USA Technologies'
					ORDER BY DEALER_ID
				) WHERE ROWNUM = 1;
			END IF;
			
			SELECT CUSTOMER_BANK_ID
			INTO ln_customer_bank_id
			FROM (
				SELECT CB.CUSTOMER_BANK_ID
				FROM CORP.CUSTOMER_BANK CB
				LEFT OUTER JOIN CORP.CUSTOMER_BANK_TERMINAL CBT ON CB.CUSTOMER_BANK_ID = CBT.CUSTOMER_BANK_ID
					AND SYSDATE >= NVL(CBT.START_DATE, MIN_DATE)
					AND SYSDATE < NVL(CBT.END_DATE, MAX_DATE)
				LEFT OUTER JOIN REPORT.TERMINAL T ON CBT.TERMINAL_ID = T.TERMINAL_ID AND T.STATUS != 'D'
				WHERE CB.CUSTOMER_ID = pn_corp_customer_id
				GROUP BY CB.CUSTOMER_BANK_ID
				ORDER BY COUNT(1) DESC, CB.CUSTOMER_BANK_ID
			) WHERE ROWNUM = 1;
		
            SELECT MAX(ADDRESS1), NVL(MAX(CITY), 'Malvern'), NVL(MAX(STATE), 'PA'), NVL(MAX(ZIP), '19355'), NVL(MAX(COUNTRY_CD), 'US')                                                                                                                                                                     
              INTO lv_customer_address1, lv_customer_city, lv_customer_state_cd, lv_customer_postal, lv_customer_country_cd 
              FROM CORP.CUSTOMER_ADDR
             WHERE CUSTOMER_ID = pn_corp_customer_id;
             
			REPORT.GET_OR_CREATE_EPORT(ln_eport_id, pv_device_serial_cd, 14);
			CORP.DEALER_EPORT_UPD(ln_dealer_id, ln_eport_id);
			REPORT.PKG_CUSTOMER_MANAGEMENT.CREATE_TERMINAL_MASS(
				0, /* pn_user_id */
				ln_terminal_id, /* pn_terminal_id */
				pv_device_serial_cd, /* pv_device_serial_cd */
				ln_dealer_id, /* pn_dealer_id */
				'TBD', /* pv_asset_nbr */
				'TBD', /* pv_machine_make */
				'To Be Determined', /* pv_machine_model */
				NULL, /* pv_telephone */
				NULL, /* pv_prefix */
				NULL, /* pv_region_name */
				'TBD', /* pv_location_name */
				NULL, /* pv_location_details */
				lv_customer_address1, /* pv_address1 */
				lv_customer_city, /* pv_city */
				lv_customer_state_cd, /* pv_state_cd */
				lv_customer_postal, /* pv_postal */
				lv_customer_country_cd, /* pv_country_cd */
				ln_customer_bank_id, /* pn_customer_bank_id */
				1, /* pn_pay_sched_id */
				'- Not Specified -', /* pv_location_type_name */
				NULL, /* pn_location_type_specific */
				'Stored Value', /* pv_product_type_name */
				NULL, /* pn_product_type_specific */
				'?', /* pc_auth_mode */
				0, /* pn_avg_amt */
				0, /* pn_time_zone_id */
				'?', /* pc_dex_data */
				'?', /* pc_receipt */
				0, /* pn_vends */
				NULL, /* pv_term_var_1 */
				NULL, /* pv_term_var_2 */
				NULL, /* pv_term_var_3 */
				NULL, /* pv_term_var_4 */
				NULL, /* pv_term_var_5 */
				NULL, /* pv_term_var_6 */
				NULL, /* pv_term_var_7 */
				NULL, /* pv_term_var_8 */
				TRUNC(SYSDATE - 30), /* pd_activate_date */
				0, /* pn_fee_grace_days */
				'N', /* pc_keep_existing_data */
				'N' /* pc_override_payment_schedule */,
        NULL /* pv_doing_business_as */,
        NULL /* pc_fee_no_trigger_event_flag */,
        NULL /* pv_sales_order_number */,
        NULL /* pv_purchase_order_number */,
        NULL /* pc_pos_environment_cd */,
        NULL /* pv_entry_capability_cds */,
        NULL /* pc_pin_capability_flag */,
        NULL /* pn_commission_bank_id */,
        'Other Non-Traditional' /* pv_business_type_name */,
        NULL /* pv_customer_service_phone */,
        NULL /* pv_customer_service_email */    
			);
			
			UPDATE REPORT.TERMINAL
			SET STATUS = 'A'
			WHERE TERMINAL_ID = ln_terminal_id AND STATUS != 'A';
		END IF;
	END;
    
    PROCEDURE UPDATE_APP_VERSION(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pn_protocol_version NUMBER,
        pv_app_type DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
        pv_app_version DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE)
    IS
        ln_device_id DEVICE.DEVICE_ID%TYPE;
    BEGIN
        SELECT DEVICE_ID
          INTO ln_device_id
          FROM DEVICE.DEVICE_LAST_ACTIVE
         WHERE DEVICE_NAME = pv_device_name;
        INSERT INTO DEVICE.DEVICE_SETTING(DEVICE_ID, DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_VALUE)
            SELECT ln_device_id, A.NAME, A.VALUE
              FROM (SELECT 'App Type' NAME, pv_app_type VALUE FROM DUAL
              UNION ALL SELECT 'App Version', pv_app_version FROM DUAL
              UNION ALL SELECT 'Property List Version', TO_CHAR(pn_protocol_version) FROM DUAL) A
              LEFT OUTER JOIN DEVICE.DEVICE_SETTING O ON A.NAME = O.DEVICE_SETTING_PARAMETER_CD AND O.DEVICE_ID = ln_device_id
             WHERE O.DEVICE_SETTING_PARAMETER_CD IS NULL;
        IF SQL%ROWCOUNT != 3 THEN
            UPDATE DEVICE.DEVICE_SETTING
               SET DEVICE_SETTING_VALUE = DECODE(DEVICE_SETTING_PARAMETER_CD, 'App Type', pv_app_type, 'App Version', pv_app_version, 'Property List Version', TO_CHAR(pn_protocol_version))
             WHERE DEVICE_ID = ln_device_id
               AND DEVICE_SETTING_PARAMETER_CD IN('App Type', 'App Version','Property List Version')
               AND DEVICE_SETTING_VALUE != DECODE(DEVICE_SETTING_PARAMETER_CD, 'App Type', pv_app_type, 'App Version', pv_app_version, 'Property List Version', TO_CHAR(pn_protocol_version));
        END IF;
    END;
    
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/CORP/USAT_MANUAL_CLOSE_BATCH.prc?rev=1.2
CREATE OR REPLACE PROCEDURE CORP.USAT_MANUAL_CLOSE_BATCH AS
--Manual delete open fill batches daily as per RFC-000038
    l_cutoff_date_1 DATE := SYSDATE - (2/24);
    CURSOR l_close_cur is
      SELECT B.BATCH_ID
        FROM CORP.BATCH B
        JOIN CORP.BATCH_FILL BF ON B.BATCH_ID = BF.BATCH_ID
        JOIN REPORT.FILL F ON BF.END_FILL_ID = F.FILL_ID
       WHERE B.BATCH_STATE_CD = 'O'
         AND B.END_DATE < (l_cutoff_date_1 + 1) /*accomodate timezone)*/
         AND F.CREATE_DATE < l_cutoff_date_1
         AND B.PAYMENT_SCHEDULE_ID = 2;
    l_cutoff_date_2 DATE := TRUNC(SYSDATE) - 7;
    CURSOR l_match_cur IS
       SELECT B.BATCH_ID, TO_CHAR(l_cutoff_date_2, 'YYYY/MM/DD') CUTOFF_DATE_TEXT
         FROM CORP.BATCH B
         JOIN REPORT.TERMINAL T ON B.TERMINAL_ID = T.TERMINAL_ID
        WHERE B.BATCH_STATE_CD = 'U'
          AND B.PAYMENT_SCHEDULE_ID = 2
          AND T.CUSTOMER_ID IN(2974,9955)
          AND B.END_DATE < l_cutoff_date_2;
BEGIN
    FOR l_close_rec IN l_close_cur LOOP
        INSERT INTO CORP.MANUAL_CLOSE_BATCH(BATCH_ID)
         VALUES(l_close_rec.BATCH_ID);
        UPDATE CORP.BATCH B
           SET B.BATCH_STATE_CD = (
                SELECT DECODE(COUNT(*), 0, 'D', 'U')
                  FROM REPORT.TERMINAL t
                  JOIN CORP.BATCH_CONFIRM CN ON t.CUSTOMER_ID = cn.CUSTOMER_ID
                 WHERE b.TERMINAL_ID = t.TERMINAL_ID
                   AND b.PAYMENT_SCHEDULE_ID = cn.PAYMENT_SCHEDULE_ID),
               B.BATCH_CLOSED_TS = SYSDATE
          WHERE B.BATCH_ID = l_close_rec.BATCH_ID
            AND B.BATCH_STATE_CD = 'O';
        COMMIT;
    END LOOP;
    FOR l_match_rec IN l_match_cur LOOP
        CORP.SP_CONFIRM_BATCH(l_match_rec.BATCH_ID, 3, l_match_rec.CUTOFF_DATE_TEXT);
    END LOOP;

    UPDATE USAT_CUSTOM.CCE_FILL F SET F.CCE_FILL_STATE_ID = 8
     WHERE F.CCE_FILL_STATE_ID IN(3,6) AND F.BATCH_ID IS NULL
       AND F.CCE_FILL_DATE < l_cutoff_date_2
       AND NOT EXISTS(
          SELECT 1
            FROM USAT_CUSTOM.FILE_TRNSFR_CCE_FILL CFFT
WHERE F.CCE_FILL_ID = CFFT.CCE_FILL_ID
             AND CFFT.FILE_TRNSFR_CCE_FILL_TYPE_ID = 2);
    COMMIT;
END;
 

/

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/releases/REL_Edge_Update_R45/Apriva.USADBP.sql?rev=HEAD
INSERT INTO AUTHORITY.HANDLER(HANDLER_NAME, HANDLER_CLASS)
SELECT 'Apriva', 'Apriva' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM AUTHORITY.HANDLER WHERE HANDLER_NAME = 'Apriva');

INSERT INTO AUTHORITY.AUTHORITY_TYPE(AUTHORITY_TYPE_NAME, AUTHORITY_TYPE_DESC, HANDLER_ID)
SELECT 'Apriva', 'Apriva', HANDLER_ID FROM AUTHORITY.HANDLER WHERE HANDLER_NAME = 'Apriva'
AND NOT EXISTS (SELECT 1 FROM AUTHORITY.AUTHORITY_TYPE WHERE AUTHORITY_TYPE_NAME = 'Apriva');

INSERT INTO AUTHORITY.AUTHORITY(AUTHORITY_NAME, AUTHORITY_TYPE_ID, AUTHORITY_SERVICE_ID, REMOTE_SERVER_ADDR, REMOTE_SERVER_PORT_NUM, TERMINAL_CAPTURE_FLAG, SEND_LINE_ITEMS_IND)
SELECT 'Apriva', AUTHORITY_TYPE_ID, 1, NULL, NULL, 'N', 'N'
FROM AUTHORITY.AUTHORITY_TYPE WHERE AUTHORITY_TYPE_NAME = 'Apriva'
AND NOT EXISTS (SELECT 1 FROM AUTHORITY.AUTHORITY WHERE AUTHORITY_NAME = 'Apriva');

INSERT INTO PSS.MERCHANT(MERCHANT_CD, MERCHANT_NAME, MERCHANT_DESC, MERCHANT_BUS_NAME, AUTHORITY_ID, DOING_BUSINESS_AS)
SELECT '0', 'Apriva', 'Apriva Merchant', 'USA Technologies', AUTHORITY_ID, 'USA Technologies'
FROM AUTHORITY.AUTHORITY
WHERE AUTHORITY_NAME = 'Apriva'
	AND NOT EXISTS (SELECT 1 FROM PSS.MERCHANT WHERE MERCHANT_NAME = 'Apriva');

INSERT INTO PSS.TERMINAL(TERMINAL_CD, TERMINAL_NEXT_BATCH_NUM, MERCHANT_ID, TERMINAL_DESC, TERMINAL_STATE_ID,
	TERMINAL_MAX_BATCH_NUM, TERMINAL_BATCH_MAX_TRAN, TERMINAL_BATCH_CYCLE_NUM, TERMINAL_MIN_BATCH_NUM,
	TERMINAL_MIN_BATCH_CLOSE_HR, TERMINAL_MAX_BATCH_CLOSE_HR, TERMINAL_BATCH_MIN_TRAN)
SELECT '0', 1, MERCHANT_ID, 'Apriva Terminal', 1, 999, 1, 1, 1, 0, 4, 1
FROM PSS.MERCHANT
WHERE MERCHANT_NAME = 'Apriva'
	AND NOT EXISTS (SELECT 1 FROM PSS.TERMINAL WHERE TERMINAL_DESC = 'Apriva Terminal');

INSERT INTO PSS.PAYMENT_SUBTYPE(PAYMENT_SUBTYPE_NAME, PAYMENT_SUBTYPE_CLASS, PAYMENT_SUBTYPE_KEY_NAME, CLIENT_PAYMENT_TYPE_CD, PAYMENT_SUBTYPE_TABLE_NAME, PAYMENT_SUBTYPE_KEY_DESC_NAME, AUTHORITY_PAYMENT_MASK_ID, TRANS_TYPE_ID, CARD_TYPE_LABEL)
  SELECT 'Apriva Special Card', 'Apriva', 'TERMINAL_ID', CPT.CLIENT_PAYMENT_TYPE_CD, 'TERMINAL', 'TERMINAL_DESC', AUTHORITY_PAYMENT_MASK_ID, 17, 'Unknown'
    FROM PSS.AUTHORITY_PAYMENT_MASK APM, PSS.CLIENT_PAYMENT_TYPE CPT
   WHERE APM.AUTHORITY_PAYMENT_MASK_NAME = 'Standard Track2 Format'
     AND APM.STATUS_CD = 'A'
     AND CPT.PAYMENT_ACTION_TYPE_CD IN('C')
     AND CPT.PAYMENT_TYPE_CD IN('S')
     AND CPT.PAYMENT_ENTRY_METHOD_CD IN('S','C')
     AND NOT EXISTS(SELECT 1 FROM PSS.PAYMENT_SUBTYPE PST0 WHERE PST0.PAYMENT_SUBTYPE_NAME = 'Apriva Special Card' AND CPT.CLIENT_PAYMENT_TYPE_CD = PST0.CLIENT_PAYMENT_TYPE_CD);

COMMIT;

     
-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/CORP/SET_TERMINAL_BUY_SELL_RATES.prc?rev=HEAD
CREATE OR REPLACE PROCEDURE CORP.SET_TERMINAL_BUY_SELL_RATES (
  pn_terminal_id IN REPORT.TERMINAL.TERMINAL_ID%TYPE,
  pn_commission_bank_id IN CORP.CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
  pn_pf_br_cp_p IN CORP.PROCESS_FEES.FEE_PERCENT%TYPE,
  pn_pf_br_cp_f IN CORP.PROCESS_FEES.FEE_AMOUNT%TYPE,
  pn_pf_br_cp_m IN CORP.PROCESS_FEES.MIN_AMOUNT%TYPE,
  pn_pf_br_me_p IN CORP.PROCESS_FEES.FEE_PERCENT%TYPE,
  pn_pf_br_me_f IN CORP.PROCESS_FEES.FEE_AMOUNT%TYPE,
  pn_pf_br_me_m IN CORP.PROCESS_FEES.MIN_AMOUNT%TYPE,
  pn_pf_sr_cp_p IN CORP.PROCESS_FEES.FEE_PERCENT%TYPE,
  pn_pf_sr_cp_f IN CORP.PROCESS_FEES.FEE_AMOUNT%TYPE,
  pn_pf_sr_cp_m IN CORP.PROCESS_FEES.MIN_AMOUNT%TYPE,
  pn_pf_sr_me_p IN CORP.PROCESS_FEES.FEE_PERCENT%TYPE,
  pn_pf_sr_me_f IN CORP.PROCESS_FEES.FEE_AMOUNT%TYPE,
  pn_pf_sr_me_m IN CORP.PROCESS_FEES.MIN_AMOUNT%TYPE,
  pn_sf_br_mm_f IN CORP.SERVICE_FEES.FEE_AMOUNT%TYPE,
  pn_sf_sr_mm_f IN CORP.SERVICE_FEES.FEE_AMOUNT%TYPE)
IS
  -- pn = param numeric, ln = local numeric, cln = constant local numeric
  -- pf = process fee, sf = service fee
  -- br = buy rate, sr = sell rate
  -- cp = card present, me = manual entry
  -- p = percent, f = flat, m = minimum
  -- mm = monthly
  -- See com.usatech.usalive.servlet.AbstractFees for correspond Java object
  cln_pf_cp_tran_type_id CORP.PROCESS_FEES.TRANS_TYPE_ID%TYPE := 16;
  cln_pf_me_tran_type_id CORP.PROCESS_FEES.TRANS_TYPE_ID%TYPE := 13;
  cln_sf_fee_id CORP.SERVICE_FEES.FEE_ID%TYPE := 1;
  cln_sf_mm_frequency_id CORP.FREQUENCY.FREQUENCY_ID%TYPE := 2;
  ld_effective_date DATE := sysdate;
BEGIN
  IF pn_commission_bank_id is null THEN
    RAISE_APPLICATION_ERROR(-20001, 'commission_bank_id is required');
  END IF;
  -- pass the sell rate to the proc and calc the commission using (sell rate - buy rate)
  CORP.PAYMENTS_PKG.PROCESS_FEES_UPD(pn_terminal_id, cln_pf_cp_tran_type_id, pn_pf_sr_cp_p, pn_pf_sr_cp_f, pn_pf_sr_cp_m, ld_effective_date, 'N', (pn_pf_sr_cp_p - pn_pf_br_cp_p), (pn_pf_sr_cp_f - pn_pf_br_cp_f), (pn_pf_sr_cp_m - pn_pf_br_cp_m), pn_commission_bank_id);
  CORP.PAYMENTS_PKG.PROCESS_FEES_UPD(pn_terminal_id, cln_pf_me_tran_type_id, pn_pf_sr_me_p, pn_pf_sr_me_f, pn_pf_sr_me_m, ld_effective_date, 'N', (pn_pf_sr_me_p - pn_pf_br_me_p), (pn_pf_sr_me_f - pn_pf_br_me_f), (pn_pf_sr_me_m - pn_pf_br_me_m), pn_commission_bank_id);
  CORP.PAYMENTS_PKG.SERVICE_FEES_UPD(pn_terminal_id, cln_sf_fee_id, cln_sf_mm_frequency_id, pn_sf_sr_mm_f, 0, ld_effective_date, null, 'N', 60, null, null, null, (pn_sf_sr_mm_f - pn_sf_br_mm_f), pn_commission_bank_id);
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/CORP/PAYMENTS_PKG.pbk?rev=1.132
CREATE OR REPLACE PACKAGE BODY CORP.PAYMENTS_PKG IS
--
-- Holds all the procedures and functions related to EFTs and payments
--
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------
-- B Krug       09-16-04  NEW
-- B Krug       09-16-04  Moved CREATE_PAYMENT_FOR_ACCOUNT into this package to
--                        take advantage of other procs in this package
-- B Krug       10-05-04  Added Ledger sync procs (and batch-related stuff)
-- B Krug       01-31-08  Added Batch Confirmation Logic

    -- Returns 'Y' if an entry is payable (it's been settled)
    -- Returns 'N' if an entry is not payable
    -- Returns '?' if an entry has not been processed
    FUNCTION ENTRY_PAYABLE(
        l_settle_state LEDGER.SETTLE_STATE_ID%TYPE,
        l_entry_type LEDGER.ENTRY_TYPE%TYPE)
     RETURN CHAR
    IS
    BEGIN
        IF l_settle_state IN(2,3,6) THEN
            RETURN 'Y';
        /*ELSIF l_entry_type IN('CB','RF','SF') THEN
            RETURN 'Y';
        */ELSIF l_settle_state IN(5) THEN
            RETURN 'N';
        ELSE
            RETURN '?';
        END IF;
    END;
    
    -- Gets the batch_id or creates one if necessary
    FUNCTION GET_OR_CREATE_DOC(
        l_cust_bank_id DOC.CUSTOMER_BANK_ID%TYPE,
        l_currency_id DOC.CURRENCY_ID%TYPE,
        l_business_unit DOC.BUSINESS_UNIT_ID%TYPE
     )
     RETURN DOC.DOC_ID%TYPE
    IS
        l_doc_id DOC.DOC_ID%TYPE;
        l_batch_ref DOC.REF_NBR%TYPE;
        l_lock VARCHAR2(128);
    BEGIN
        -- lock this bank id until commit to ensure the the doc stays open until the ledger entry is set with it
        l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_cust_bank_id);
        SELECT MAX(D.DOC_ID) -- just to be safe in case there is more than one
          INTO l_doc_id
          FROM DOC D
         WHERE D.CUSTOMER_BANK_ID = l_cust_bank_id
           AND NVL(D.CURRENCY_ID, 0) = NVL(l_currency_id, 0)
           AND NVL(D.BUSINESS_UNIT_ID, 0) = NVL(l_business_unit, 0)
           AND D.STATUS = 'O';
        IF l_doc_id IS NULL THEN
            -- create new doc record
            SELECT EFT_SEQ.NEXTVAL, TO_CHAR(EFT_BATCH_SEQ.NEXTVAL, 'FM0000999999')
              INTO l_doc_id, l_batch_ref
              FROM DUAL;
            INSERT INTO DOC(
                DOC_ID,
                DOC_TYPE,
                REF_NBR,
                DESCRIPTION,
                CUSTOMER_BANK_ID,
                CURRENCY_ID,
                BUSINESS_UNIT_ID,
                BANK_ACCT_NBR,
                BANK_ROUTING_NBR,
                STATUS)
              SELECT
                l_doc_id,
                '--',
                l_batch_ref,
                NVL(EFT_PREFIX, 'USAT: ') || l_batch_ref,
                CUSTOMER_BANK_ID,
                DECODE(l_currency_id, 0, NULL, l_currency_id),
                DECODE(l_business_unit, 0, NULL, l_business_unit),
                BANK_ACCT_NBR,
                BANK_ROUTING_NBR,
                'O'
              FROM CUSTOMER_BANK
              WHERE CUSTOMER_BANK_ID = l_cust_bank_id;
        END IF;
        RETURN l_doc_id;
    END;

    -- Gets the batch_id or creates one if necessary
    FUNCTION GET_OR_CREATE_BATCH(
        l_terminal_id  BATCH.TERMINAL_ID%TYPE,
        l_cust_bank_id DOC.CUSTOMER_BANK_ID%TYPE,
        l_entry_date LEDGER.ENTRY_DATE%TYPE,
        l_currency_id DOC.CURRENCY_ID%TYPE,
        l_pay_sched TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
        l_business_unit CORP.BUSINESS_UNIT.BUSINESS_UNIT_ID%TYPE,
        l_always_as_accum CHAR
    )
     RETURN BATCH.BATCH_ID%TYPE
    IS
        l_batch_id BATCH.BATCH_ID%TYPE;
        l_doc_id DOC.DOC_ID%TYPE;
        l_start_date BATCH.START_DATE%TYPE;
        l_end_date BATCH.END_DATE%TYPE;
        l_batch_state_cd BATCH.BATCH_STATE_CD%TYPE;
        l_start_fill_id BATCH_FILL.START_FILL_ID%TYPE;
        l_end_fill_id BATCH_FILL.END_FILL_ID%TYPE;
        l_counters_must_show CHAR(1);
        l_lock VARCHAR2(128);
    BEGIN
        -- calculate batch (and create if necessary)
        -- Get an "Open" doc record
        l_doc_id := GET_OR_CREATE_DOC(l_cust_bank_id, l_currency_id, l_business_unit);
        --l_lock := GLOBALS_PKG.REQUEST_LOCK('BATCH.TERMINAL_ID',l_terminal_id); -- this may not be necessary since we lock the doc on customer_bank_id
        SELECT MAX(B.BATCH_ID) -- just to be safe in case there is more than one
          INTO l_batch_id
          FROM BATCH B
         WHERE B.DOC_ID = l_doc_id
           AND B.TERMINAL_ID = l_terminal_id
           AND B.PAYMENT_SCHEDULE_ID = l_pay_sched
           AND B.BATCH_STATE_CD IN('O', 'L') -- Open or Closeable only
           -- for "As Accumulated", batch start date does not matter
           AND (B.START_DATE <= l_entry_date OR l_pay_sched IN(1,5,8))
           AND (NVL(B.END_DATE, MAX_DATE) > l_entry_date OR (B.END_DATE = l_entry_date AND l_pay_sched IN(8)))
           AND (l_pay_sched NOT IN(8) OR DECODE(B.END_DATE, NULL, 0, 1) = DECODE(l_always_as_accum, 'E', 1, 0));
        IF l_batch_id IS NULL THEN
            -- calc start and end dates
            IF l_pay_sched IN(1, 5) THEN
                SELECT LEAST(NVL(MAX(B.END_DATE), l_entry_date), l_entry_date)
                  INTO l_start_date
                  FROM BATCH B, DOC D
                 WHERE B.DOC_ID = D.DOC_ID
                   AND B.TERMINAL_ID = l_terminal_id
                   AND D.CUSTOMER_BANK_ID = l_cust_bank_id
                   AND NVL(D.CURRENCY_ID, 0) = NVL(l_currency_id, 0)
                   AND B.PAYMENT_SCHEDULE_ID = l_pay_sched;
                l_batch_state_cd := 'L'; -- Closeable
                -- end date is null
            ELSIF l_pay_sched = 2 THEN
                SELECT DECODE(MAX(BCOT.BATCH_CONFIRM_OPTION_TYPE_ID), NULL, 'N', 'Y')
                  INTO l_counters_must_show
                  FROM REPORT.TERMINAL T
                  JOIN CORP.BATCH_CONFIRM BC ON T.CUSTOMER_ID = BC.CUSTOMER_ID
                  JOIN CORP.BATCH_CONFIRM_OPTION BCO
                    ON BC.BATCH_CONFIRM_ID = BCO.BATCH_CONFIRM_ID
                  JOIN CORP.BATCH_CONFIRM_OPTION_TYPE BCOT ON BCO.BATCH_CONFIRM_OPTION_TYPE_ID = BCOT.BATCH_CONFIRM_OPTION_TYPE_ID
                 WHERE BC.PAYMENT_SCHEDULE_ID = l_pay_sched 
                   AND BCOT.BATCH_CONFIRM_OPTION_TYPE_CD = 'COUNTER_MUST_SHOW'                  
                   AND T.TERMINAL_ID = l_terminal_id;
                       
                SELECT NVL(MAX(FILL_DATE), MIN_DATE), MAX(FILL_ID)
                  INTO l_start_date, l_start_fill_id
                  FROM (
                    SELECT f.FILL_DATE, f.FILL_ID
                      FROM REPORT.FILL f
                      JOIN REPORT.TERMINAL_EPORT te
                        ON f.EPORT_ID = te.EPORT_ID
                       AND f.FILL_DATE >= NVL(te.START_DATE, MIN_DATE)
                       AND f.FILL_DATE < NVL(te.END_DATE, MAX_DATE)
                     WHERE f.FILL_DATE <= l_entry_date
                       AND te.TERMINAL_ID = l_terminal_id
                       AND (F.COUNTERS_DISPLAYED_FLAG = 'Y' OR l_counters_must_show = 'N')
                     ORDER BY f.FILL_DATE DESC)
                 WHERE ROWNUM = 1 ;
                 SELECT MIN(FILL_DATE), MIN(FILL_ID)
                  INTO l_end_date, l_end_fill_id
                  FROM (
                    SELECT f.FILL_DATE, f.FILL_ID
                      FROM REPORT.FILL f
                      JOIN (SELECT CONNECT_BY_ROOT FILL_ID START_FILL_ID, FILL_ID END_FILL_ID, LEVEL - 1 DEPTH
                              FROM REPORT.FILL
                              START WITH FILL_ID = l_start_fill_id
                              CONNECT BY NOCYCLE PRIOR FILL_ID = PREV_FILL_ID
                              ) H ON F.FILL_ID = H.END_FILL_ID
                     JOIN REPORT.TERMINAL_EPORT te
                        ON f.EPORT_ID = te.EPORT_ID
                       AND f.FILL_DATE >= NVL(te.START_DATE, MIN_DATE)
                       AND f.FILL_DATE < NVL(te.END_DATE, MAX_DATE)
                     WHERE f.FILL_DATE > l_entry_date
                       AND te.TERMINAL_ID = l_terminal_id
                       AND (F.COUNTERS_DISPLAYED_FLAG = 'Y' OR l_counters_must_show = 'N')
                     ORDER BY f.FILL_DATE ASC)
                 WHERE ROWNUM = 1;
                
                l_batch_state_cd := 'O'; -- Open
            ELSIF l_pay_sched = 8 THEN
                SELECT LEAST(NVL(MAX(B.END_DATE), l_entry_date), l_entry_date)
                  INTO l_start_date
                  FROM BATCH B, DOC D
                 WHERE B.DOC_ID = D.DOC_ID
                   AND B.TERMINAL_ID = l_terminal_id
                   AND D.CUSTOMER_BANK_ID = l_cust_bank_id
                   AND NVL(D.CURRENCY_ID, 0) = NVL(l_currency_id, 0)
                   AND B.PAYMENT_SCHEDULE_ID = l_pay_sched;
                l_batch_state_cd := 'O'; -- Open
            ELSE
                SELECT TRUNC(l_entry_date - (OFFSET_HOURS / 24), INTERVAL) + (OFFSET_HOURS / 24),
                       ADD_MONTHS(TRUNC(l_entry_date - (OFFSET_HOURS / 24), INTERVAL) + DAYS, MONTHS) + (OFFSET_HOURS / 24)
                  INTO l_start_date, l_end_date
                  FROM PAYMENT_SCHEDULE
                 WHERE PAYMENT_SCHEDULE_ID = l_pay_sched;
                IF l_end_date <= l_entry_date THEN -- trouble, should not happen
                    l_end_date := l_entry_date + (1.0/(24*60*60));
                END IF;
                l_batch_state_cd := 'O'; -- Open
            END IF;

            -- create new batch record
            SELECT BATCH_SEQ.NEXTVAL
              INTO l_batch_id
              FROM DUAL;
            INSERT INTO BATCH(
                BATCH_ID,
                DOC_ID,
                TERMINAL_ID,
                PAYMENT_SCHEDULE_ID,
                START_DATE,
                END_DATE,
                BATCH_STATE_CD)
              VALUES(
                l_batch_id,
                l_doc_id,
                l_terminal_id,
                l_pay_sched,
                l_start_date,
                l_end_date,
                l_batch_state_cd);
            IF l_pay_sched = 2 THEN
                INSERT INTO CORP.BATCH_FILL(
                    BATCH_ID,
                    START_FILL_ID,
                    END_FILL_ID)
                  VALUES(
                    l_batch_id,
                    l_start_fill_id,
                    l_end_fill_id);
            END IF;
        END IF;
        RETURN l_batch_id;
    END;
    FUNCTION GET_OR_CREATE_BATCH(
        l_terminal_id  BATCH.TERMINAL_ID%TYPE,
        l_cust_bank_id DOC.CUSTOMER_BANK_ID%TYPE,
        l_entry_date LEDGER.ENTRY_DATE%TYPE,
        l_currency_id DOC.CURRENCY_ID%TYPE,
        l_always_as_accum CHAR
    )
     RETURN BATCH.BATCH_ID%TYPE
    IS
        l_business_unit CORP.BUSINESS_UNIT.BUSINESS_UNIT_ID%TYPE;
        l_pay_sched TERMINAL.PAYMENT_SCHEDULE_ID%TYPE;
    BEGIN
        -- calculate batch (and create if necessary)
        SELECT DECODE(l_always_as_accum,
                    'Y', 1,
                    'A', 5,
                    PAYMENT_SCHEDULE_ID),
               BUSINESS_UNIT_ID
          INTO l_pay_sched, l_business_unit
          FROM TERMINAL
         WHERE TERMINAL_ID = l_terminal_id;
        RETURN GET_OR_CREATE_BATCH(l_terminal_id,l_cust_bank_id,l_entry_date,l_currency_id,l_pay_sched, l_business_unit, l_always_as_accum);
    END;
    
    FUNCTION GET_NEW_BATCH(
        l_batch_id BATCH.BATCH_ID%TYPE,
        l_entry_date LEDGER.ENTRY_DATE%TYPE)
     RETURN BATCH.BATCH_ID%TYPE
    IS
        l_cb_id DOC.CUSTOMER_BANK_ID%TYPE;
        l_curr_id DOC.CURRENCY_ID%TYPE;
        l_term_id BATCH.TERMINAL_ID%TYPE;
    BEGIN

        SELECT D.CUSTOMER_BANK_ID, D.CURRENCY_ID, B.TERMINAL_ID
          INTO l_cb_id, l_curr_id, l_term_id
          FROM BATCH B, DOC D
          WHERE D.DOC_ID = B.DOC_ID
            AND B.BATCH_ID = l_batch_id;
        RETURN GET_OR_CREATE_BATCH(l_term_id, l_cb_id, l_entry_date, l_curr_id, 'Y');
    END;
    
    FUNCTION GET_DOC_STATUS(
        l_doc_id DOC.DOC_ID%TYPE)
     RETURN DOC.STATUS%TYPE
    IS
        l_status DOC.STATUS%TYPE;
    BEGIN
        SELECT STATUS INTO l_status FROM CORP.DOC WHERE DOC_ID = l_doc_id;
        RETURN l_status;
    END;

    FUNCTION FREEZE_DOC_STATUS_LEDGER_ID(
        l_ledger_id LEDGER.LEDGER_ID%TYPE)
     RETURN DOC.STATUS%TYPE
    IS
        l_doc_id DOC.DOC_ID%TYPE;
        l_customer_bank_id CORP.DOC.CUSTOMER_BANK_ID%TYPE;
        l_lock VARCHAR2(128);
    BEGIN
        SELECT D.DOC_ID, D.CUSTOMER_BANK_ID
          INTO l_doc_id, l_customer_bank_id
          FROM CORP.DOC D
         INNER JOIN CORP.BATCH B ON D.DOC_ID = B.DOC_ID
         INNER JOIN CORP.LEDGER L ON L.BATCH_ID = B.BATCH_ID
         WHERE L.LEDGER_ID = l_ledger_id;
        l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_customer_bank_id);
        RETURN GET_DOC_STATUS(l_doc_id);
    END;
    
    FUNCTION FREEZE_DOC_STATUS_DOC_ID(
        l_doc_id DOC.DOC_ID%TYPE)
     RETURN DOC.STATUS%TYPE
    IS
        l_customer_bank_id CORP.DOC.CUSTOMER_BANK_ID%TYPE;
        l_lock VARCHAR2(128);
    BEGIN
        SELECT D.CUSTOMER_BANK_ID
          INTO l_customer_bank_id
          FROM CORP.DOC D
         WHERE D.DOC_ID = l_doc_id;
        l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_customer_bank_id);
        RETURN GET_DOC_STATUS(l_doc_id);
    END;
    
    -- Returns 'Y' if the batch is either an "As Accumulated" or has no unsettled transactions
    FUNCTION BATCH_CLOSABLE(
        l_batch_id BATCH.BATCH_ID%TYPE,
        l_pay_sched BATCH.PAYMENT_SCHEDULE_ID%TYPE,
        l_end_date BATCH.END_DATE%TYPE,
        l_batch_state_cd BATCH.BATCH_STATE_CD%TYPE)
     RETURN CHAR
    IS
        l_cnt NUMBER;
    BEGIN
        IF l_batch_state_cd IN('L', 'F') THEN
            RETURN 'Y';
        ELSIF l_batch_state_cd IN('C', 'D') THEN
            IF l_pay_sched IN(2) THEN -- Fill To Fill
                SELECT COUNT(*)
                  INTO l_cnt
                  FROM LEDGER l
                 WHERE ENTRY_PAYABLE(L.SETTLE_STATE_ID, L.ENTRY_TYPE) = '?' -- not processed
                   AND L.DELETED = 'N'
                   AND L.BATCH_ID = l_batch_id;
                IF l_cnt = 0 THEN
                    RETURN 'Y';
                ELSE
                    RETURN 'N';
                END IF;
            ELSE
                RETURN 'Y';
            END IF;
        ELSIF l_batch_state_cd IN('O') AND l_pay_sched NOT IN(1,2,5,7,8) AND l_end_date <= SYSDATE THEN
            RETURN 'Y';
        ELSE
            RETURN 'N';
        END IF;
    END;

    -- Returns 'Y' if the batch is either an "As Accumulated" or has no unsettled transactions
    FUNCTION BATCH_CLOSABLE(
        l_batch_id BATCH.BATCH_ID%TYPE)
     RETURN CHAR
    IS
        l_pay_sched BATCH.PAYMENT_SCHEDULE_ID%TYPE;
        l_end_date BATCH.END_DATE%TYPE;
        l_batch_state_cd BATCH.BATCH_STATE_CD%TYPE;
    BEGIN
        SELECT PAYMENT_SCHEDULE_ID, END_DATE, BATCH_STATE_CD
          INTO l_pay_sched, l_end_date, l_batch_state_cd
          FROM BATCH
         WHERE BATCH_ID = l_batch_id;
        RETURN BATCH_CLOSABLE(l_batch_id, l_pay_sched, l_end_date, l_batch_state_cd);
    END;

    PROCEDURE CHECK_TRANS_WITH_PF_CLOSED(
        l_process_fee_id    CORP.LEDGER.PROCESS_FEE_ID%TYPE)
    IS
        CURSOR l_check_cur IS
           SELECT L.TRANS_ID
              FROM LEDGER L, BATCH B, DOC D
             WHERE L.PROCESS_FEE_ID = l_process_fee_id
               AND L.BATCH_ID = B.BATCH_ID
               AND B.DOC_ID = D.DOC_ID
             GROUP BY L.TRANS_ID
             HAVING SUM(DECODE(D.STATUS, 'O', 0, 'D', 0, 1)) > 0
                AND SUM(DECODE(D.STATUS, 'O', 1, 0)) = 0;
        l_batch_id BATCH.BATCH_ID%TYPE;
    BEGIN
        FOR l_check_rec IN l_check_cur LOOP
            SELECT MAX(B.BATCH_ID)
              INTO l_batch_id
              FROM LEDGER L, BATCH B, DOC D
             WHERE L.TRANS_ID = l_check_rec.TRANS_ID
               AND L.BATCH_ID = B.BATCH_ID
               AND B.DOC_ID = D.DOC_ID
               AND D.STATUS NOT IN('O', 'D');
            IF l_batch_id IS NOT NULL THEN
                RAISE_APPLICATION_ERROR(-20701, 'PROCESS FEE (process_fee_id='||TO_CHAR(l_process_fee_id)||' is used in a batch (id='||TO_CHAR(l_batch_id)||') that was already closed - cannot update ledger table!');
            END IF;
        END LOOP;
    END;

    -- Checks if the specified transaction is in a closed batch and if so raises an
    -- exception
    PROCEDURE CHECK_TRANS_CLOSED(
        l_trans_id    CORP.LEDGER.TRANS_ID%TYPE)
    IS
        l_closed_cnt PLS_INTEGER;
        l_open_cnt PLS_INTEGER;
        l_batch_id BATCH.BATCH_ID%TYPE;
    BEGIN
        -- get the batch
        SELECT SUM(DECODE(D.STATUS, 'O', 0, 'D', 0, 1)),
               SUM(DECODE(D.STATUS, 'O', 1, 0))
          INTO l_closed_cnt, l_open_cnt
          FROM LEDGER L, BATCH B, DOC D
         WHERE L.TRANS_ID = l_trans_id
           AND L.BATCH_ID = B.BATCH_ID
           AND B.DOC_ID = D.DOC_ID;
        IF l_open_cnt = 0 AND l_closed_cnt > 0 THEN
            SELECT MAX(B.BATCH_ID)
              INTO l_batch_id
              FROM LEDGER L, BATCH B, DOC D
             WHERE L.TRANS_ID = l_trans_id
               AND L.BATCH_ID = B.BATCH_ID
               AND B.DOC_ID = D.DOC_ID
               AND D.STATUS NOT IN('O', 'D');
            RAISE_APPLICATION_ERROR(-20701, 'TRANSACTION (tran_id='||TO_CHAR(l_trans_id)||' is in a batch (id='||TO_CHAR(l_batch_id)||') that was already closed - cannot update ledger table!');
        END IF;
    END;
    
    PROCEDURE CHECK_FILL_BATCH_COMPLETE(
        l_batch_id BATCH.BATCH_ID%TYPE)
    IS
        l_batch_amt NUMBER;
        l_batch_tran_cnt NUMBER;
        l_src_credit_amt NUMBER;
        l_src_credit_cnt NUMBER;
        l_first_tran_ts DATE;
        lv_source_system_cd REPORT.FILL.SOURCE_SYSTEM_CD%TYPE;
        ld_fill_date REPORT.FILL.FILL_DATE%TYPE;
    BEGIN
        SELECT MAX(F.SOURCE_SYSTEM_CD), MAX(F.FILL_DATE)
          INTO lv_source_system_cd, ld_fill_date
          FROM REPORT.FILL F
          JOIN CORP.BATCH_FILL BF ON BF.END_FILL_ID = F.FILL_ID
         WHERE BF.BATCH_ID = l_batch_id;
        IF lv_source_system_cd IS NULL THEN -- this is a manual fill, consider it complete after one day
          IF ld_fill_date <= SYSDATE - 1 THEN
              UPDATE CORP.BATCH B
               SET B.BATCH_STATE_CD = (
                    SELECT DECODE(COUNT(*), 0, 'D', 'U')
                      FROM REPORT.TERMINAL t
                      JOIN CORP.BATCH_CONFIRM CN ON t.CUSTOMER_ID = cn.CUSTOMER_ID
                     WHERE b.TERMINAL_ID = t.TERMINAL_ID
                       AND b.PAYMENT_SCHEDULE_ID = cn.PAYMENT_SCHEDULE_ID),
                   B.BATCH_CLOSED_TS = SYSDATE
              WHERE B.BATCH_ID = l_batch_id;
          END IF;
          RETURN;
        END IF;
        
        SELECT SUM(l.AMOUNT), MIN(l.ENTRY_DATE)
          INTO l_batch_amt, l_first_tran_ts
     	  FROM CORP.LEDGER l
         WHERE l.ENTRY_TYPE = 'CC'
           AND l.DELETED = 'N'
           AND l.BATCH_ID = l_batch_id;
                   
        SELECT SUM(p.AMOUNT)
          INTO l_batch_tran_cnt
          FROM REPORT.PURCHASE P
          LEFT OUTER JOIN PSS.TRAN_LINE_ITEM_TYPE XIT ON P.TRAN_LINE_ITEM_TYPE_ID = XIT.TRAN_LINE_ITEM_TYPE_ID
         WHERE P.TRAN_ID IN(
            SELECT l.TRANS_ID 
              FROM CORP.LEDGER l
             WHERE l.ENTRY_TYPE = 'CC'
               AND l.DELETED = 'N'
               AND l.BATCH_ID = l_batch_id)
           AND (XIT.TRAN_LINE_ITEM_TYPE_GROUP_CD IS NULL OR XIT.TRAN_LINE_ITEM_TYPE_GROUP_CD IN('P', 'S'));
        
        BEGIN
            SELECT SUM(HF.CREDIT_AMOUNT), SUM(HF.CREDIT_COUNT)
              INTO l_src_credit_amt, l_src_credit_cnt
              FROM CORP.BATCH_FILL BF
              JOIN (SELECT CONNECT_BY_ROOT FILL_ID ANCESTOR_FILL_ID, F.*
                      FROM REPORT.FILL F
                     START WITH F.FILL_ID = (SELECT START_FILL_ID FROM CORP.BATCH_FILL WHERE BATCH_ID = l_batch_id)
                   CONNECT BY NOCYCLE PRIOR FILL_ID = PREV_FILL_ID) HF ON BF.START_FILL_ID = HF.ANCESTOR_FILL_ID AND BF.END_FILL_ID = HF.FILL_ID
             WHERE BF.BATCH_ID = l_batch_id 
            HAVING COUNT(*) = COUNT(HF.CREDIT_AMOUNT) 
               AND COUNT(*) =  COUNT(HF.CREDIT_COUNT);
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                DECLARE
                    CURSOR l_cur IS
                        SELECT e.EPORT_SERIAL_NUM DEVICE_SERIAL_CD, 
                               GREATEST(l_first_tran_ts, NVL(te.START_DATE, MIN_DATE)) START_DATE,
                               LEAST(B.END_DATE,NVL(te.END_DATE, MAX_DATE)) END_DATE 
                          FROM CORP.BATCH B
                          JOIN REPORT.TERMINAL_EPORT te ON b.TERMINAL_ID = te.TERMINAL_ID
            	          JOIN REPORT.EPORT E ON te.EPORT_ID = e.EPORT_ID
        		         WHERE B.BATCH_ID = l_batch_id;
        		BEGIN
                    l_src_credit_amt := 0;
                    l_src_credit_cnt := 0;
                    FOR l_rec IN l_cur LOOP
                        SELECT l_src_credit_amt + NVL(SUM(T.TRAN_LINE_ITEM_AMOUNT * T.TRAN_LINE_ITEM_QUANTITY), 0), 
                               l_src_credit_cnt + NVL(SUM(DECODE(T.TRAN_LINE_ITEM_TYPE_GROUP_CD, 'U', NULL, 'I', NULL, T.TRAN_LINE_ITEM_QUANTITY)), 0)
                	      INTO l_src_credit_amt, l_src_credit_cnt                         
                          FROM PSS.VW_REPORTING_TRAN_LINE_ITEM T
                          WHERE T.DEVICE_SERIAL_CD = l_rec.DEVICE_SERIAL_CD
                            AND T.CLIENT_PAYMENT_TYPE_CD IN('C', 'R')
                            AND T.TRAN_START_TS BETWEEN l_rec.START_DATE AND l_rec.END_DATE;
                    END LOOP; 
                END;       
            WHEN OTHERS THEN
                RAISE;
        END;

        IF l_batch_amt = l_src_credit_amt AND l_batch_tran_cnt = l_src_credit_cnt THEN
            UPDATE CORP.BATCH B
               SET B.BATCH_STATE_CD = (
                    SELECT DECODE(COUNT(*), 0, 'D', 'U')
                      FROM REPORT.TERMINAL t
                      JOIN CORP.BATCH_CONFIRM CN ON t.CUSTOMER_ID = cn.CUSTOMER_ID
                     WHERE b.TERMINAL_ID = t.TERMINAL_ID
                       AND b.PAYMENT_SCHEDULE_ID = cn.PAYMENT_SCHEDULE_ID),
                   B.BATCH_CLOSED_TS = SYSDATE
              WHERE B.BATCH_ID = l_batch_id;
        END IF;	
    END;
    
    PROCEDURE ADD_BATCH_ROUNDING_ENTRY(
        l_batch_id BATCH.BATCH_ID%TYPE)
    IS
        l_round_amt LEDGER.AMOUNT%TYPE;
    BEGIN
        -- calculate rounding amount
        SELECT ROUND(SUM(AMOUNT), 2) - SUM(AMOUNT)
          INTO l_round_amt
          FROM LEDGER L
         WHERE L.DELETED = 'N'
           AND ENTRY_PAYABLE(L.SETTLE_STATE_ID,L.ENTRY_TYPE) = 'Y'
           AND L.BATCH_ID = l_batch_id;

        -- add entry if necessary
        IF l_round_amt <> 0 THEN
            INSERT INTO LEDGER(
                LEDGER_ID,
                ENTRY_TYPE,
                TRANS_ID,
                PROCESS_FEE_ID,
                AMOUNT,
                ENTRY_DATE,
                BATCH_ID,
                SETTLE_STATE_ID,
                LEDGER_DATE,
                DESCRIPTION)
            SELECT
                LEDGER_SEQ.NEXTVAL,
                'SB',
                NULL,
                NULL,
                l_round_amt,
                SYSDATE,
                l_batch_id,
                2,
                SYSDATE,
                NULL
              FROM DUAL;
        END IF;
    END;

    PROCEDURE ADD_BATCH_ROUNDING_ENTRIES(
        l_doc_id DOC.DOC_ID%TYPE)
    IS
        CURSOR l_cur IS
            SELECT BATCH_ID
              FROM BATCH
             WHERE DOC_ID = l_doc_id;
    BEGIN
        FOR l_rec IN l_cur LOOP
            ADD_BATCH_ROUNDING_ENTRY(l_rec.BATCH_ID);
        END LOOP;
    END;

    PROCEDURE ADD_ROUNDING_ENTRY(
        l_doc_id DOC.DOC_ID%TYPE)
    IS
        l_round_amt LEDGER.AMOUNT%TYPE;
        l_batch_id BATCH.BATCH_ID%TYPE;
    BEGIN
        -- calculate rounding amount
        SELECT ROUND(SUM(AMOUNT), 2) - SUM(AMOUNT)
          INTO l_round_amt
          FROM LEDGER L, BATCH B
         WHERE L.DELETED = 'N'
           AND ENTRY_PAYABLE(L.SETTLE_STATE_ID,L.ENTRY_TYPE) = 'Y'
           AND L.BATCH_ID = B.BATCH_ID
           AND B.DOC_ID = l_doc_id;

        -- add entry if necessary
        IF l_round_amt <> 0 THEN
            SELECT MAX(BATCH_ID)
              INTO l_batch_id
              FROM BATCH
             WHERE DOC_ID = l_doc_id
               AND TERMINAL_ID IS NULL;
            IF l_batch_id IS NULL THEN
                -- create a batch and set the doc id
                SELECT BATCH_SEQ.NEXTVAL
                  INTO l_batch_id
                  FROM DUAL;
                INSERT INTO BATCH(BATCH_ID, DOC_ID, TERMINAL_ID, PAYMENT_SCHEDULE_ID, START_DATE, END_DATE, BATCH_STATE_CD)
                    SELECT l_batch_id, l_doc_id, NULL, 1, SYSDATE, SYSDATE, 'F'
                      FROM DOC
                     WHERE DOC_ID = l_doc_id;
            END IF;

            INSERT INTO LEDGER(
                LEDGER_ID,
                ENTRY_TYPE,
                TRANS_ID,
                PROCESS_FEE_ID,
                AMOUNT,
                ENTRY_DATE,
                BATCH_ID,
                SETTLE_STATE_ID,
                LEDGER_DATE,
                DESCRIPTION)
            SELECT
                LEDGER_SEQ.NEXTVAL,
                'SB',
                NULL,
                NULL,
                l_round_amt,
                SYSDATE,
                l_batch_id,
                2,
                SYSDATE,
                NULL
              FROM DUAL;
        END IF;
    END;
    /*
     * This procedure references REPORT.TRAN
    */
    PROCEDURE UPDATE_SETTLEMENT(
        l_trans_id    CORP.LEDGER.TRANS_ID%TYPE,
        l_settle_state_id CORP.LEDGER.SETTLE_STATE_ID%TYPE,
        l_settle_date   CORP.LEDGER.LEDGER_DATE%TYPE)
    IS
        CURSOR l_cur IS
            SELECT D.DOC_ID, B.BATCH_ID, D.CUSTOMER_BANK_ID
              FROM LEDGER L, BATCH B, DOC D
             WHERE L.TRANS_ID = l_trans_id
               AND L.BATCH_ID = B.BATCH_ID
               AND B.DOC_ID = D.DOC_ID
               AND D.STATUS = 'O';
        TYPE t_rec_list IS TABLE OF l_cur%ROWTYPE;
        l_recs t_rec_list;
        l_batch_ids NUMBER_TABLE := NUMBER_TABLE();
        l_lock VARCHAR2(128);
    BEGIN
        l_lock := GLOBALS_PKG.REQUEST_LOCK('LEDGER.TRANS_ID',l_trans_id);
        OPEN l_cur;
        FETCH l_cur BULK COLLECT INTO l_recs;
        CLOSE l_cur;

        IF l_recs.FIRST IS NOT NULL THEN
            FOR i IN l_recs.FIRST .. l_recs.LAST LOOP
                l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_recs(i).CUSTOMER_BANK_ID);
                IF GET_DOC_STATUS(l_recs(i).DOC_ID) = 'O' THEN
                   l_batch_ids.EXTEND;
                   l_batch_ids(l_batch_ids.LAST) := l_recs(i).BATCH_ID;
                ELSE
                    EXIT;
                END IF;
            END LOOP;
        END IF;
        IF l_recs.FIRST IS NOT NULL AND l_recs.LAST = l_batch_ids.LAST THEN -- we can do direct update Yippee!
            UPDATE LEDGER SET
                SETTLE_STATE_ID = l_settle_state_id,
                LEDGER_DATE = l_settle_date
             WHERE TRANS_ID = l_trans_id
               AND BATCH_ID MEMBER OF l_batch_ids;
        ELSE
           DECLARE
                l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE;
                l_close_date LEDGER.LEDGER_DATE%TYPE;
                l_total_amount LEDGER.AMOUNT%TYPE;
                l_terminal_id BATCH.TERMINAL_ID%TYPE;
                l_customer_bank_id DOC.CUSTOMER_BANK_ID%TYPE;
                l_process_fee_id LEDGER.PROCESS_FEE_ID%TYPE;
                l_currency_id DOC.CURRENCY_ID%TYPE;
            BEGIN
                SELECT TRANS_TYPE_ID, CLOSE_DATE, TOTAL_AMOUNT, TERMINAL_ID,
                       CUSTOMER_BANK_ID, PROCESS_FEE_ID, CURRENCY_ID
                  INTO l_trans_type_id, l_close_date, l_total_amount, l_terminal_id,
                       l_customer_bank_id, l_process_fee_id, l_currency_id
                  FROM TRANS
                 WHERE TRAN_ID = l_trans_id;

                UPDATE_LEDGER(l_trans_id, l_trans_type_id, l_close_date,
                    l_settle_date, l_total_amount, l_settle_state_id,
                    l_terminal_id, l_customer_bank_id,l_process_fee_id, l_currency_id);
            END;
        END IF;
    END;

    PROCEDURE UPDATE_PROCESS_FEE_VALUES(
        l_process_fee_id CORP.LEDGER.PROCESS_FEE_ID%TYPE)
    IS
        CURSOR l_tran_cur IS
           SELECT DISTINCT L.TRANS_ID, X.TRANS_TYPE_ID, X.CLOSE_DATE, X.TOTAL_AMOUNT,
                  X.TERMINAL_ID, X.CUSTOMER_BANK_ID, X.SETTLE_STATE_ID,
                  X.SETTLE_DATE, X.CURRENCY_ID
              FROM LEDGER L, REPORT.TRANS X
             WHERE L.PROCESS_FEE_ID = l_process_fee_id
               AND L.DELETED = 'N'
               AND L.ENTRY_TYPE = 'PF'
               AND L.TRANS_ID = X.TRAN_ID;
    BEGIN
        FOR l_tran_rec IN l_tran_cur LOOP
            UPDATE_LEDGER(
                l_tran_rec.TRANS_ID,
                l_tran_rec.trans_type_id,
                l_tran_rec.close_date,
                l_tran_rec.settle_date,
                l_tran_rec.total_amount,
                l_tran_rec.settle_state_id,
                l_tran_rec.terminal_id,
                l_tran_rec.customer_bank_id,
                l_process_fee_id,
                l_tran_rec.currency_id);
        END LOOP;
    END;

    -- Puts CC, RF, or CB record into Ledger and also any required PF record
    PROCEDURE INSERT_TRANS_TO_LEDGER(
        l_trans_id    LEDGER.TRANS_ID%TYPE,
        l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE,
        l_close_date    LEDGER.ENTRY_DATE%TYPE,
        l_settle_date LEDGER.LEDGER_DATE%TYPE,
        l_amount LEDGER.AMOUNT%TYPE,
        l_settle_state_id LEDGER.SETTLE_STATE_ID%TYPE,
        l_process_fee_id    LEDGER.PROCESS_FEE_ID%TYPE,
        l_batch_id LEDGER.BATCH_ID%TYPE)
    IS
        l_ledger_id LEDGER.LEDGER_ID%TYPE;
        l_real_amt LEDGER.AMOUNT%TYPE;
        l_pay_sched BATCH.PAYMENT_SCHEDULE_ID%TYPE;
        l_end_date BATCH.END_DATE%TYPE;
        l_payable_ind REPORT.TRANS_TYPE.PAYABLE_IND%TYPE;
        l_customer_debit_ind REPORT.TRANS_TYPE.CUSTOMER_DEBIT_IND%TYPE;
        l_entry_type REPORT.TRANS_TYPE.ENTRY_TYPE%TYPE;
        l_fill_batch_ind REPORT.TRANS_TYPE.FILL_BATCH_IND%TYPE;
    BEGIN
        IF NVL(l_amount, 0) = 0 THEN
            RETURN;
        END IF;
		
		SELECT PAYABLE_IND, CUSTOMER_DEBIT_IND, ENTRY_TYPE, FILL_BATCH_IND
		INTO l_payable_ind, l_customer_debit_ind, l_entry_type, l_fill_batch_ind
		FROM REPORT.TRANS_TYPE
		WHERE TRANS_TYPE_ID = l_trans_type_id;
        
		IF l_payable_ind = 'Y' THEN
            SELECT LEDGER_SEQ.NEXTVAL,
                   DECODE(l_customer_debit_ind, 'Y', -ABS(l_amount), l_amount)
              INTO l_ledger_id,
                   l_real_amt
              FROM DUAL;
            INSERT INTO LEDGER(
                LEDGER_ID,
                ENTRY_TYPE,
                TRANS_ID,
                PROCESS_FEE_ID,
                AMOUNT,
                ENTRY_DATE,
                BATCH_ID,
                SETTLE_STATE_ID,
                LEDGER_DATE)
            SELECT
                l_ledger_id,
                l_entry_type,
                l_trans_id,
                l_process_fee_id,
                l_real_amt,
                l_close_date,
                l_batch_id,
                l_settle_state_id,
                l_settle_date
              FROM DUAL;
            -- create net revenue fee on transaction, if any
            INSERT INTO LEDGER(
                LEDGER_ID,
                RELATED_LEDGER_ID,
                ENTRY_TYPE,
                TRANS_ID,
                PROCESS_FEE_ID,
                SERVICE_FEE_ID,
                AMOUNT,
                ENTRY_DATE,
                BATCH_ID,
                SETTLE_STATE_ID,
                LEDGER_DATE,
                DESCRIPTION)
            SELECT
                LEDGER_SEQ.NEXTVAL,
                l_ledger_id,
                'SF',
                l_trans_id,
                l_process_fee_id,
                SF.SERVICE_FEE_ID,
                -l_real_amt * SF.FEE_PERCENT,
                l_close_date,
                l_batch_id,
                l_settle_state_id,
                l_settle_date,
                F.FEE_NAME
            FROM SERVICE_FEES SF, FEES F, BATCH B
           WHERE SF.FEE_ID = F.FEE_ID
             AND SF.TERMINAL_ID = B.TERMINAL_ID
             AND B.BATCH_ID = l_batch_id
             AND l_close_date BETWEEN NVL(SF.START_DATE, MIN_DATE)
                  AND NVL(SF.END_DATE, MAX_DATE)
             AND SF.FREQUENCY_ID = 6;
        END IF;
        IF l_process_fee_id IS NOT NULL THEN
            BEGIN
                SELECT LEDGER_SEQ.NEXTVAL,
                       -GREATEST((ABS(l_amount) * pf.FEE_PERCENT + pf.FEE_AMOUNT), pf.MIN_AMOUNT)
                  INTO l_ledger_id,
                       l_real_amt
                  FROM PROCESS_FEES pf
                WHERE pf.PROCESS_FEE_ID = l_process_fee_id
                  AND (pf.FEE_PERCENT > 0 OR pf.FEE_AMOUNT > 0);
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                RETURN;
              WHEN OTHERS THEN
                RAISE;
            END;
            --also insert process fee
            INSERT INTO LEDGER(
                LEDGER_ID,
                ENTRY_TYPE,
                TRANS_ID,
                PROCESS_FEE_ID,
                AMOUNT,
                ENTRY_DATE,
                BATCH_ID,
                SETTLE_STATE_ID,
                LEDGER_DATE)
            VALUES(
                l_ledger_id,
                'PF',
                l_trans_id,
                l_process_fee_id,
                l_real_amt,
                l_close_date,
                l_batch_id,
                l_settle_state_id,
                l_settle_date);
            PROCESS_FEE_COMMISSION_INS(l_ledger_id, l_trans_id, l_process_fee_id, l_settle_state_id, l_amount, l_close_date, l_settle_date);
            -- create net revenue fee on process fee, if any
            INSERT INTO LEDGER(
                LEDGER_ID,
                RELATED_LEDGER_ID,
                ENTRY_TYPE,
                TRANS_ID,
                PROCESS_FEE_ID,
                SERVICE_FEE_ID,
                AMOUNT,
                ENTRY_DATE,
                BATCH_ID,
                SETTLE_STATE_ID,
                LEDGER_DATE,
                DESCRIPTION)
            SELECT
                LEDGER_SEQ.NEXTVAL,
                l_ledger_id,
                'SF',
                l_trans_id,
                l_process_fee_id,
                SF.SERVICE_FEE_ID,
                -l_real_amt * SF.FEE_PERCENT,
                l_close_date,
                l_batch_id,
                l_settle_state_id,
                l_settle_date,
                F.FEE_NAME
            FROM SERVICE_FEES SF, FEES F, BATCH B
           WHERE SF.FEE_ID = F.FEE_ID
             AND SF.TERMINAL_ID = B.TERMINAL_ID
             AND B.BATCH_ID = l_batch_id
             AND l_close_date BETWEEN NVL(SF.START_DATE, MIN_DATE)
                  AND NVL(SF.END_DATE, MAX_DATE)
             AND SF.FREQUENCY_ID = 6;
        END IF;
        -- Now check batch complete
        IF l_fill_batch_ind = 'Y' THEN
            SELECT PAYMENT_SCHEDULE_ID, END_DATE
              INTO l_pay_sched, l_end_date
              FROM CORP.BATCH B
             WHERE B.BATCH_ID = l_batch_id;
            IF l_pay_sched IN(2) AND l_end_date IS NOT NULL THEN
               CHECK_FILL_BATCH_COMPLETE(l_batch_id);
            END IF;
        END IF;
    END;

    PROCEDURE INSERT_TRANS_TO_LEDGER(
        l_trans_id    LEDGER.TRANS_ID%TYPE,
        l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE,
        l_close_date    LEDGER.ENTRY_DATE%TYPE,
        l_settle_date LEDGER.LEDGER_DATE%TYPE,
        l_amount LEDGER.AMOUNT%TYPE,
        l_settle_state_id LEDGER.SETTLE_STATE_ID%TYPE,
        l_process_fee_id    LEDGER.PROCESS_FEE_ID%TYPE,
        l_terminal_id   BATCH.TERMINAL_ID%TYPE,
        l_customer_bank_id  DOC.CUSTOMER_BANK_ID%TYPE,
        l_currency_id       DOC.CURRENCY_ID%TYPE)
    IS
        l_batch_id LEDGER.BATCH_ID%TYPE;
        l_doc_status DOC.STATUS%TYPE;
        l_lock VARCHAR2(128);
		l_customer_debit_ind REPORT.TRANS_TYPE.CUSTOMER_DEBIT_IND%TYPE;
    BEGIN
		SELECT CUSTOMER_DEBIT_IND
		INTO l_customer_debit_ind
		FROM REPORT.TRANS_TYPE
		WHERE TRANS_TYPE_ID = l_trans_type_id;
	
        -- if refund or chargeback get the batch that the
        -- original trans is in, if open use it else use "as accumulated"
        IF l_customer_debit_ind = 'Y' THEN
            l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_customer_bank_id);
            l_lock := GLOBALS_PKG.REQUEST_LOCK('BATCH.TERMINAL_ID',l_terminal_id);
            BEGIN
                SELECT BATCH_ID, STATUS
                  INTO l_batch_id, l_doc_status
                  FROM (SELECT B.BATCH_ID, D.STATUS
                          FROM LEDGER L, BATCH B, TRANS T, DOC D
                         WHERE L.TRANS_ID = T.ORIG_TRAN_ID
                           AND T.TRAN_ID = l_trans_id
                           AND L.BATCH_ID = B.BATCH_ID
                           AND L.DELETED = 'N'
                           AND B.DOC_ID = D.DOC_ID
                           AND NVL(D.CURRENCY_ID, 0) = NVL(l_currency_id, 0)
                           AND L.ENTRY_TYPE = 'CC'
                           AND B.PAYMENT_SCHEDULE_ID NOT IN(8) -- As Exported requires each trans to be put in "open" batch
                         ORDER BY DECODE(D.STATUS, 'O', 0, 1), B.BATCH_ID DESC)
                 WHERE ROWNUM = 1;
                IF NVL(l_doc_status, ' ') <> 'O' THEN
                    l_batch_id := GET_OR_CREATE_BATCH(l_terminal_id, l_customer_bank_id, l_close_date, l_currency_id, 'Y');
                END IF;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN -- refund for trans not in ledger
                -- The following should be removed and the error re-enabled,
                -- once we enforce orig_tran_id for all refunds/ chargebacks
                l_batch_id := GET_OR_CREATE_BATCH(l_terminal_id, l_customer_bank_id, l_close_date, l_currency_id, 'Y');
                --RAISE_APPLICATION_ERROR(-20702, 'TRANSACTION (tran_id='||TO_CHAR(l_trans_id)||' is a refund or chargeback on a transaction NOT found in the ledger table!');
                WHEN OTHERS THEN
                    RAISE;
            END;
        ELSE
            l_batch_id := GET_OR_CREATE_BATCH(l_terminal_id, l_customer_bank_id, l_close_date, l_currency_id, 'N');
        END IF;
        INSERT_TRANS_TO_LEDGER(l_trans_id, l_trans_type_id, l_close_date, l_settle_date,
            l_amount, l_settle_state_id, l_process_fee_id, l_batch_id);
    END;
    
    -- Updates the ledger table with the transaction changes
    -- May fail if the transaction is already part of a document
    -- (i.e. - it has already been paid)
    PROCEDURE UPDATE_LEDGER(
        l_trans_id    LEDGER.TRANS_ID%TYPE,
        l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE,
        l_close_date    LEDGER.ENTRY_DATE%TYPE,
        l_settle_date LEDGER.LEDGER_DATE%TYPE,
        l_total_amount LEDGER.AMOUNT%TYPE,
        l_settle_state_id LEDGER.SETTLE_STATE_ID%TYPE,
        l_terminal_id   BATCH.TERMINAL_ID%TYPE,
        l_customer_bank_id  DOC.CUSTOMER_BANK_ID%TYPE,
        l_process_fee_id    LEDGER.PROCESS_FEE_ID%TYPE,
        l_currency_id       DOC.CURRENCY_ID%TYPE)
    IS
        l_batch_id BATCH.BATCH_ID%TYPE;
        l_closed_cnt PLS_INTEGER;
        l_lock VARCHAR2(128);
        l_prev_cb_ids NUMBER_TABLE;
		l_cash_ind REPORT.TRANS_TYPE.CASH_IND%TYPE;
		l_entry_type REPORT.TRANS_TYPE.ENTRY_TYPE%TYPE;
		l_payable_ind REPORT.TRANS_TYPE.PAYABLE_IND%TYPE;
    BEGIN
		SELECT CASH_IND, ENTRY_TYPE, PAYABLE_IND
		INTO l_cash_ind, l_entry_type, l_payable_ind
		FROM REPORT.TRANS_TYPE
		WHERE TRANS_TYPE_ID = l_trans_type_id;
		
        IF l_cash_ind = 'Y' THEN -- cash; ignore
            RETURN;
        END IF;
        l_lock := GLOBALS_PKG.REQUEST_LOCK('LEDGER.TRANS_ID',l_trans_id);
        -- get the batch
        SELECT MAX(B.BATCH_ID), SUM(DECODE(D.STATUS, 'O', 0, 'D', 0, 1)), -- are there ANY closed batches on this transaction?
               SET(CAST(COLLECT(CAST(D.CUSTOMER_BANK_ID AS NUMBER)) AS NUMBER_TABLE))
          INTO l_batch_id, l_closed_cnt, l_prev_cb_ids
          FROM LEDGER L, BATCH B, DOC D
         WHERE L.TRANS_ID = l_trans_id
           AND L.BATCH_ID = B.BATCH_ID
           AND L.DELETED = 'N'
           AND B.DOC_ID = D.DOC_ID;
        IF l_batch_id IS NULL THEN -- Create new batch for this ledger record
            -- If terminal is null then no need to do anything more
            -- Added: if cust bank is null don't do any more (we may need to change this in the future which would also entail making BATCH.CUSTOMER_BANK_ID nullable)
            IF l_terminal_id IS NOT NULL AND l_customer_bank_id IS NOT NULL THEN
                INSERT_TRANS_TO_LEDGER(l_trans_id, l_trans_type_id, l_close_date, l_settle_date,
                    l_total_amount, l_settle_state_id, l_process_fee_id, l_terminal_id, l_customer_bank_id, l_currency_id);
                RETURN; -- all done
            END IF;
        ELSIF l_closed_cnt = 0 THEN -- lock previous customer_bank_id's to ensure that none of the doc's closes
            FOR i IN l_prev_cb_ids.FIRST..l_prev_cb_ids.LAST LOOP
                l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_prev_cb_ids(i));
            END LOOP;
            -- double-check closed count
            SELECT SUM(DECODE(D.STATUS, 'O', 0, 'D', 0, 1))
              INTO l_closed_cnt
              FROM LEDGER L, BATCH B, DOC D
             WHERE L.TRANS_ID = l_trans_id
               AND L.BATCH_ID = B.BATCH_ID
               AND L.DELETED = 'N'
               AND B.DOC_ID = D.DOC_ID;
        END IF;
        IF l_closed_cnt > 0 THEN -- add adjustment for prev paid
            DECLARE
                CURSOR l_prev_cur IS
                    SELECT COUNT(*) PAID_CNT,
                           SUM(DECODE(ENTRY_PAYABLE(L.SETTLE_STATE_ID, L.ENTRY_TYPE), 'Y', AMOUNT, NULL)) PAID_AMT, B.TERMINAL_ID,
                           D.CUSTOMER_BANK_ID, D.CURRENCY_ID
                      FROM LEDGER L, BATCH B, DOC D
                     WHERE L.TRANS_ID = l_trans_id
                       AND L.BATCH_ID = B.BATCH_ID
                       AND B.DOC_ID = D.DOC_ID
                       AND L.DELETED = 'N'
                       AND L.RELATED_LEDGER_ID IS NULL -- Must exclude Net Revenue Fees
                     GROUP BY B.TERMINAL_ID, D.CUSTOMER_BANK_ID, D.CURRENCY_ID;

                l_adj_amt LEDGER.AMOUNT%TYPE;
                l_adjusted_new_cb BOOLEAN := FALSE;
                l_old_pf_amt LEDGER.AMOUNT%TYPE;
                l_old_amt LEDGER.AMOUNT%TYPE;
                l_desc LEDGER.DESCRIPTION%TYPE;
                l_new_payable CHAR(1);
                l_ledger_id LEDGER.LEDGER_ID%TYPE;
            BEGIN
                --figure out what change has occurred
                FOR l_prev_rec IN l_prev_cur LOOP
                    IF NVL(l_customer_bank_id, 0) = l_prev_rec.CUSTOMER_BANK_ID
                       AND NVL(l_currency_id, 0) = NVL(l_prev_rec.CURRENCY_ID, 0) THEN
                        IF NVL(l_prev_rec.PAID_CNT, 0) <> 0 AND NVL(l_prev_rec.PAID_AMT, 0) <> 0 THEN
                            --calc difference and add as adjustment
                            SELECT DECODE(l_prev_rec.TERMINAL_ID, l_terminal_id, ENTRY_PAYABLE(l_settle_state_id, l_entry_type), 'N')
                              INTO l_new_payable
                              FROM DUAL;
                            SELECT (CASE WHEN l_payable_ind = 'Y' THEN l_total_amount ELSE 0 END
                                   - NVL(SUM(GREATEST(ABS(l_total_amount) * pf.FEE_PERCENT + pf.FEE_AMOUNT, pf.MIN_AMOUNT)), 0))
                                   * DECODE(l_new_payable, 'Y', 1, 0)
                                   - l_prev_rec.PAID_AMT
                              INTO l_adj_amt
                              FROM PROCESS_FEES pf
                             WHERE pf.PROCESS_FEE_ID = l_process_fee_id;
                            IF l_adj_amt <> 0 THEN
                                IF l_new_payable = 'Y' THEN
                                    SELECT SUM(AMOUNT)
                                      INTO l_old_amt
                                      FROM LEDGER
                                     WHERE TRANS_ID = l_trans_id
                                       AND ENTRY_TYPE IN('CC')
                                       AND ENTRY_PAYABLE(SETTLE_STATE_ID, ENTRY_TYPE) = 'Y'
                                       AND DELETED = 'N';
                                    SELECT SUM(AMOUNT)
                                      INTO l_old_pf_amt
                                      FROM LEDGER
                                     WHERE TRANS_ID = l_trans_id
                                       AND ENTRY_TYPE IN('PF')
                                       AND ENTRY_PAYABLE(SETTLE_STATE_ID, ENTRY_TYPE) = 'Y'
                                       AND DELETED = 'N';
                                    IF NVL(l_old_amt, 0) = 0 AND l_payable_ind = 'Y' THEN
                                        l_desc := 'Correction for a previously ignored transaction';
                                    ELSIF l_old_amt <> l_total_amount AND l_payable_ind = 'Y' THEN
                                        l_desc := 'Correction for a value amount change on a transaction that has already been paid';
                                    ELSIF NVL(l_old_pf_amt, 0) = 0 THEN
                                        l_desc := 'Correction for a missing processing fee on a transaction that has already been paid';
                                    ELSIF NVL(l_terminal_id, 0) <> l_prev_rec.TERMINAL_ID THEN
                                        l_desc := 'Correction for a change of location on a transaction that has already been paid';
                                    ELSE
                                        l_desc := 'Correction for a change in processing fee on a transaction that has already been paid';
                                    END IF;
                                ELSIF NVL(l_terminal_id, 0) <> l_prev_rec.TERMINAL_ID THEN
                                    l_desc := 'Correction for a change of location on a transaction that has already been paid';
                                ELSE -- should not be paid now
                                    l_desc := 'Correction for a transaction that has already been paid but has since failed settlement';
                                END IF;
                                l_batch_id := GET_OR_CREATE_BATCH(l_prev_rec.TERMINAL_ID,
                                        l_customer_bank_id, l_close_date, l_currency_id, 'A');
                                -- create a negative adjustment based on this previous payment
                                SELECT LEDGER_SEQ.NEXTVAL
                                  INTO l_ledger_id
                                  FROM DUAL;
                                INSERT INTO LEDGER(
                                    LEDGER_ID,
                                    ENTRY_TYPE,
                                    TRANS_ID,
                                    PROCESS_FEE_ID,
                                    AMOUNT,
                                    ENTRY_DATE,
                                    BATCH_ID,
                                    SETTLE_STATE_ID,
                                    LEDGER_DATE,
                                    DESCRIPTION)
                                VALUES(
                                    l_ledger_id,
                                    'AD',
                                    l_trans_id,
                                    NULL,
                                    l_adj_amt,
                                    l_close_date,
                                    l_batch_id,
                                    2,
                                    l_settle_date,
                                    l_desc);
                                -- create net revenue adjustment, if any
                                INSERT INTO LEDGER(
                                    LEDGER_ID,
                                    RELATED_LEDGER_ID,
                                    ENTRY_TYPE,
                                    TRANS_ID,
                                    PROCESS_FEE_ID,
                                    SERVICE_FEE_ID,
                                    AMOUNT,
                                    ENTRY_DATE,
                                    BATCH_ID,
                                    SETTLE_STATE_ID,
                                    LEDGER_DATE,
                                    DESCRIPTION)
                                SELECT
                                    LEDGER_SEQ.NEXTVAL,
                                    l_ledger_id,
                                    'SF',
                                    l_trans_id,
                                    NULL,
                                    SF.SERVICE_FEE_ID,
                                    -l_adj_amt * SF.FEE_PERCENT,
                                    l_close_date,
                                    l_batch_id,
                                    2,
                                    l_settle_date,
                                    F.FEE_NAME
                                FROM SERVICE_FEES SF, FEES F
                               WHERE SF.FEE_ID = F.FEE_ID
                                 AND SF.TERMINAL_ID = l_prev_rec.TERMINAL_ID
                                 AND l_close_date BETWEEN NVL(SF.START_DATE, MIN_DATE)
                                      AND NVL(SF.END_DATE, MAX_DATE)
                                 AND SF.FREQUENCY_ID = 6;
                            END IF;
                            l_adjusted_new_cb := l_adjusted_new_cb OR l_adj_amt = 0 OR NVL(l_terminal_id, 0) = l_prev_rec.TERMINAL_ID; -- whether we adjust or not the new cb of the tran is appropriately dealt with
                        END IF;
                    --case: bank acct changed
                    ELSIF l_prev_rec.PAID_AMT <> 0 THEN
                        IF NVL(l_terminal_id, 0) <> l_prev_rec.TERMINAL_ID THEN
                            l_desc := 'Correction for a transaction that was assigned to the wrong location';
                        ELSIF NVL(l_currency_id, 0) <> NVL(l_prev_rec.CURRENCY_ID, 0) THEN
                            l_desc := 'Correction for a change in currency';
                        ELSE
                            l_desc := 'Correction for a transaction that was paid to the wrong bank account';
                        END IF;
                        -- deduct entire amt from old
                        l_batch_id := GET_OR_CREATE_BATCH(
                                l_prev_rec.TERMINAL_ID,
                                l_prev_rec.CUSTOMER_BANK_ID,
                                l_close_date,
                                l_prev_rec.CURRENCY_ID,
                                'A');
                        SELECT LEDGER_SEQ.NEXTVAL
                          INTO l_ledger_id
                          FROM DUAL;
                        INSERT INTO LEDGER(
                            LEDGER_ID,
                            ENTRY_TYPE,
                            TRANS_ID,
                            PROCESS_FEE_ID,
                            AMOUNT,
                            ENTRY_DATE,
                            BATCH_ID,
                            SETTLE_STATE_ID,
                            LEDGER_DATE,
                            DESCRIPTION)
                        VALUES(
                            l_ledger_id,
                            'AD',
                            l_trans_id,
                            NULL,
                            -l_prev_rec.PAID_AMT,
                            l_close_date,
                            l_batch_id,
                            l_settle_state_id,
                            l_settle_date,
                            l_desc);
                        -- create net revenue adjustment, if any
                        INSERT INTO LEDGER(
                            LEDGER_ID,
                            RELATED_LEDGER_ID,
                            ENTRY_TYPE,
                            TRANS_ID,
                            PROCESS_FEE_ID,
                            SERVICE_FEE_ID,
                            AMOUNT,
                            ENTRY_DATE,
                            BATCH_ID,
                            SETTLE_STATE_ID,
                            LEDGER_DATE,
                            DESCRIPTION)
                        SELECT
                            LEDGER_SEQ.NEXTVAL,
                            l_ledger_id,
                            'SF',
                            l_trans_id,
                            NULL,
                            SF.SERVICE_FEE_ID,
                            l_prev_rec.PAID_AMT * SF.FEE_PERCENT,
                            l_close_date,
                            l_batch_id,
                            l_settle_state_id,
                            l_settle_date,
                            F.FEE_NAME
                        FROM SERVICE_FEES SF, FEES F
                       WHERE SF.FEE_ID = F.FEE_ID
                         AND SF.TERMINAL_ID = l_prev_rec.TERMINAL_ID
                         AND l_close_date BETWEEN NVL(SF.START_DATE, MIN_DATE)
                              AND NVL(SF.END_DATE, MAX_DATE)
                         AND SF.FREQUENCY_ID = 6;
                    END IF;
                END LOOP;
                IF NOT l_adjusted_new_cb AND l_customer_bank_id IS NOT NULL THEN
                    l_batch_id := GET_OR_CREATE_BATCH(l_terminal_id,
                        l_customer_bank_id, l_close_date, l_currency_id, 'A');
                    INSERT_TRANS_TO_LEDGER(l_trans_id, l_trans_type_id,
                        l_close_date, l_settle_date, l_total_amount,
                        l_settle_state_id, l_process_fee_id, l_batch_id);
                END IF;
            END;
        ELSE -- an open batch exists
            -- To make it easy let's just delete what's there and re-insert
            --DELETE FROM LEDGER WHERE TRANS_ID = l_trans_id;
            UPDATE LEDGER SET DELETED = 'Y' WHERE TRANS_ID = l_trans_id;
            -- If terminal is null then no need to do anything more
            -- Added: if cust bank is null don't do any more (we may need to change this in the future which would also entail making BATCH.CUSTOMER_BANK_ID nullable)
            IF l_terminal_id IS NOT NULL AND l_customer_bank_id IS NOT NULL THEN
                INSERT_TRANS_TO_LEDGER(l_trans_id, l_trans_type_id, l_close_date, l_settle_date,
                    l_total_amount, l_settle_state_id, l_process_fee_id, l_terminal_id, l_customer_bank_id, l_currency_id);
            END IF;
        END IF;
    END;
    
    -- updates any fill-to-fill batch records upon the insertion of a record
    -- into the FILL table. Updates to the FILL table are NOT handled correctly
    -- by this procedure!
    PROCEDURE UPDATE_FILL_BATCH(
        l_fill_id   FILL.FILL_ID%TYPE)
    IS
        CURSOR l_batch_cur IS
            SELECT B.BATCH_ID, B.TERMINAL_ID, D.CUSTOMER_BANK_ID, D.CURRENCY_ID,
                   B.START_DATE, CASE WHEN BCOT.BATCH_CONFIRM_OPTION_TYPE_ID IS NULL THEN  'N' ELSE 'Y' END COUNTER_MUST_SHOW
              FROM CORP.BATCH B
              JOIN REPORT.TERMINAL_EPORT TE ON B.TERMINAL_ID = TE.TERMINAL_ID
              JOIN REPORT.FILL F ON TE.EPORT_ID = F.EPORT_ID
               AND F.FILL_DATE >= NVL(te.START_DATE, MIN_DATE)
               AND F.FILL_DATE < NVL(te.END_DATE, MAX_DATE)
              JOIN CORP.DOC D ON B.DOC_ID = D.DOC_ID
              JOIN REPORT.TERMINAL T ON B.TERMINAL_ID = T.TERMINAL_ID
              LEFT OUTER JOIN (CORP.BATCH_CONFIRM BC
              JOIN CORP.BATCH_CONFIRM_OPTION BCO
                ON BC.BATCH_CONFIRM_ID = BCO.BATCH_CONFIRM_ID
              JOIN CORP.BATCH_CONFIRM_OPTION_TYPE BCOT ON BCO.BATCH_CONFIRM_OPTION_TYPE_ID = BCOT.BATCH_CONFIRM_OPTION_TYPE_ID
               AND BCOT.BATCH_CONFIRM_OPTION_TYPE_CD = 'COUNTER_MUST_SHOW'
              ) ON BC.CUSTOMER_ID = T.CUSTOMER_ID
               AND BC.PAYMENT_SCHEDULE_ID = B.PAYMENT_SCHEDULE_ID
             WHERE B.START_DATE < F.FILL_DATE
               AND NVL(B.END_DATE, MAX_DATE) > F.FILL_DATE
               AND B.BATCH_STATE_CD = 'O'
               AND B.PAYMENT_SCHEDULE_ID = 2
               AND (F.COUNTERS_DISPLAYED_FLAG = 'Y' OR BCOT.BATCH_CONFIRM_OPTION_TYPE_ID IS NULL)
               AND F.FILL_ID = l_fill_id;
        l_end_date CORP.BATCH.END_DATE%TYPE;
        l_end_fill_id CORP.BATCH_FILL.END_FILL_ID%TYPE;
        l_lock VARCHAR2(128);
    BEGIN
        FOR l_batch_rec IN l_batch_cur LOOP
            SELECT MIN(FILL_DATE), MIN(FILL_ID)
                  INTO l_end_date, l_end_fill_id
                  FROM (
                    SELECT f.FILL_DATE, f.FILL_ID
                      FROM REPORT.FILL f, REPORT.TERMINAL_EPORT te
                     WHERE f.FILL_DATE > l_batch_rec.START_DATE
                       AND f.EPORT_ID = te.EPORT_ID
                       AND f.FILL_DATE >= NVL(te.START_DATE, MIN_DATE)
                       AND f.FILL_DATE < NVL(te.END_DATE, MAX_DATE)
                       AND te.TERMINAL_ID = l_batch_rec.TERMINAL_ID
                       AND (F.COUNTERS_DISPLAYED_FLAG = 'Y' OR l_batch_rec.COUNTER_MUST_SHOW = 'N')
                     ORDER BY f.FILL_DATE ASC)
                 WHERE ROWNUM = 1;
            l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_batch_rec.CUSTOMER_BANK_ID);
            -- it doesn't matter if the batch's doc has changed, because we assume the batch is in an open doc
            -- update batch record
            UPDATE CORP.BATCH B SET B.END_DATE = l_end_date
             WHERE B.BATCH_ID = l_batch_rec.BATCH_ID;
            UPDATE CORP.BATCH_FILL BF SET BF.END_FILL_ID = l_end_fill_id
             WHERE BF.BATCH_ID = l_batch_rec.BATCH_ID;

            -- also update ledger.batch_id of entries that are after fill_date
            DECLARE
                CURSOR l_cur IS
                  SELECT L.LEDGER_ID, L.ENTRY_DATE
                    FROM LEDGER L
                   WHERE L.BATCH_ID = l_batch_rec.BATCH_ID
                     AND L.ENTRY_DATE >= l_end_date;
                TYPE t_rec_list IS TABLE OF l_cur%ROWTYPE;
                l_recs t_rec_list;
                l_new_batch_id LEDGER.BATCH_ID%TYPE;
            BEGIN
                OPEN l_cur;
                FETCH l_cur BULK COLLECT INTO l_recs;
                CLOSE l_cur;

                IF l_recs.FIRST IS NOT NULL THEN
                    FOR i IN l_recs.FIRST .. l_recs.LAST LOOP
                        l_new_batch_id := GET_OR_CREATE_BATCH(
                            l_batch_rec.TERMINAL_ID,
                            l_batch_rec.CUSTOMER_BANK_ID,
                            l_recs(i).ENTRY_DATE,
                            l_batch_rec.CURRENCY_ID,
                            'N');
                        UPDATE LEDGER L
                           SET L.BATCH_ID = l_new_batch_id
                         WHERE L.LEDGER_ID = l_recs(i).LEDGER_ID;
                    END LOOP;
                END IF;
            END;
            GLOBALS_PKG.RELEASE_LOCK(l_lock);
            COMMIT;
            
            -- Now check batch complete
            CHECK_FILL_BATCH_COMPLETE(l_batch_rec.BATCH_ID);
            COMMIT;
         END LOOP;
    END;
    
    PROCEDURE UPDATE_EXPORT_BATCH(
        l_export_id   REPORT.EXPORT_BATCH.BATCH_ID%TYPE)
    IS
        CURSOR l_batch_cur IS
            SELECT DISTINCT B.BATCH_ID, B.TERMINAL_ID, D.CUSTOMER_BANK_ID, D.CURRENCY_ID, B.START_DATE, EX.TRAN_CREATE_DT_END NEW_END_DATE
              FROM CORP.BATCH B
             INNER JOIN CORP.DOC D ON B.DOC_ID = D.DOC_ID
             INNER JOIN CORP.CUSTOMER_BANK CB ON D.CUSTOMER_BANK_ID = CB.CUSTOMER_BANK_ID
             /*
             INNER JOIN REPORT.TERMINAL TERMINAL_ID ON B.TERMINAL_ID = T.TERMINAL_ID
             INNER JOIN FRONT.VW_LOCATION_HIERARCHY H ON T.LOCATION_ID = H.DESCENDANT_LOCATION_ID
             */
             INNER JOIN REPORT.EXPORT_BATCH EX ON CB.CUSTOMER_ID = EX.CUSTOMER_ID /* AND EX.LOCATION_ID = H.ANCESTOR_LOCATION_ID */
             WHERE B.BATCH_STATE_CD = 'O'
               AND B.END_DATE IS NULL
               AND B.PAYMENT_SCHEDULE_ID = 8
               AND EX.BATCH_ID = l_export_id;
        l_lock VARCHAR2(128);
    BEGIN
        FOR l_batch_rec IN l_batch_cur LOOP
            l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_batch_rec.CUSTOMER_BANK_ID);
            -- it doesn't matter if the batch's doc has changed, because we assume the batch is in an open doc
            -- update batch record
            UPDATE BATCH B
               SET B.END_DATE = l_batch_rec.new_end_date,
                   B.BATCH_STATE_CD = (SELECT DECODE(COUNT(*), 0, 'D', 'U')
                      FROM REPORT.TERMINAL t
                      JOIN CORP.BATCH_CONFIRM CN ON t.CUSTOMER_ID = cn.CUSTOMER_ID
                     WHERE b.TERMINAL_ID = t.TERMINAL_ID
                       AND b.PAYMENT_SCHEDULE_ID = cn.PAYMENT_SCHEDULE_ID),
                   B.BATCH_CLOSED_TS = SYSDATE
             WHERE B.BATCH_ID = l_batch_rec.BATCH_ID;

            -- also update ledger.batch_id of entries that are not exported
            DECLARE
                CURSOR l_cur IS
                  SELECT L.LEDGER_ID, L.ENTRY_DATE
                    FROM LEDGER L
                   WHERE L.BATCH_ID = l_batch_rec.BATCH_ID
                     AND (L.ENTRY_DATE > l_batch_rec.new_end_date
                         OR NOT EXISTS(SELECT 1 FROM REPORT.ACTIVITY_REF A
                              INNER JOIN REPORT.EXPORT_BATCH EB ON A.BATCH_ID = EB.BATCH_ID
                              WHERE L.TRANS_ID = A.TRAN_ID AND EB.STATUS IN('A','S')));
                TYPE t_rec_list IS TABLE OF l_cur%ROWTYPE;
                l_recs t_rec_list;
                l_new_batch_id LEDGER.BATCH_ID%TYPE;
            BEGIN
                OPEN l_cur;
                FETCH l_cur BULK COLLECT INTO l_recs;
                CLOSE l_cur;

                IF l_recs.FIRST IS NOT NULL THEN
                    FOR i IN l_recs.FIRST .. l_recs.LAST LOOP
                        l_new_batch_id := GET_OR_CREATE_BATCH(
                            l_batch_rec.TERMINAL_ID,
                            l_batch_rec.CUSTOMER_BANK_ID,
                            l_recs(i).ENTRY_DATE,
                            l_batch_rec.CURRENCY_ID,
                            'N');
                        UPDATE LEDGER L
                           SET L.BATCH_ID = l_new_batch_id
                         WHERE L.LEDGER_ID = l_recs(i).LEDGER_ID;
                    END LOOP;
                END IF;
            END;
         END LOOP;
    END;
/* not doing this yet
    PROCEDURE AUTO_CREATE_DOCS.
    IS
       CURSOR c_docs IS
             SELECT cb.customer_bank_id
              FROM (SELECT l.CUSTOMER_BANK_ID, l.TRANS_TYPE_ID, SUM(l.TOTAL_AMOUNT) TOTAL_AMOUNT,
                           SUM(l.TOTAL_AMOUNT * l.FEE_PERCENT + l.FEE_AMOUNT) FEE_TOTAL
                      FROM LEDGER l, TERMINAL t
                     WHERE l.PAID_STATUS = 'U'
                       AND l.TERMINAL_ID = t.TERMINAL_ID
                       AND t.PAYMENT_SCHEDULE_ID <> 1
                     GROUP BY l.CUSTOMER_BANK_ID, l.TRANS_TYPE_ID) lp,
                     CUSTOMER_BANK cb
              WHERE cb.CUSTOMER_BANK_ID = lp.CUSTOMER_BANK_ID
              GROUP BY cb.CUSTOMER_BANK_ID, cb.PAY_MIN_AMOUNT
              HAVING SUM(CASE TRANS_TYPE_ID
                      WHEN 16 THEN 1
                      WHEN 19 THEN 1
                      WHEN 20 THEN -1
                      WHEN 21 THEN -1
                      ELSE NULL
                   END * TOTAL_AMOUNT) - SUM (lp.FEE_TOTAL) > cb.PAY_MIN_AMOUNT;
    BEGIN
        -- determine each customer bank that whose total eft will exceed the min_eft_amt
        FOR r_docs IN c_docs LOOP
            BEGIN
                CREATE_DOC(r_efts.customer_bank_id, NULL, l_type,'Y');
                COMMIT;
            EXCEPTION
                WHEN OTHERS THEN
                    IF SQLCODE = -20910 THEN
                        ROLLBACK;
                    ELSE
                        RAISE;
                    END IF;
            END;
        END LOOP;
    END;

*/

    FUNCTION GET_UNPAID_TRANS_AND_FEES (
        l_as_of DATE
    ) RETURN GLOBALS_PKG.REF_CURSOR
    IS
        cur GLOBALS_PKG.REF_CURSOR;
    BEGIN
       OPEN cur FOR
            SELECT CUSTOMER_NAME, CB.CUSTOMER_ID, CB.CUSTOMER_BANK_ID,
                   BANK_ACCT_NBR, BANK_ROUTING_NBR, PAY_MIN_AMOUNT, CREDIT_AMOUNT,
                   REFUND_AMOUNT, CHARGEBACK_AMOUNT, PROCESS_FEE_AMOUNT,
                   SERVICE_FEE_AMOUNT, ADJUST_AMOUNT
                   FROM CUSTOMER C, CUSTOMER_BANK CB, (
                SELECT CUSTOMER_BANK_ID,
                       SUM(DECODE(ENTRY_TYPE, 'CC', AMOUNT, NULL)) CREDIT_AMOUNT,
                       SUM(DECODE(ENTRY_TYPE, 'RF', AMOUNT, NULL)) REFUND_AMOUNT,
                       SUM(DECODE(ENTRY_TYPE, 'CB', AMOUNT, NULL)) CHARGEBACK_AMOUNT,
                       SUM(DECODE(ENTRY_TYPE, 'PF', AMOUNT, NULL)) PROCESS_FEE_AMOUNT,
                       SUM(DECODE(ENTRY_TYPE, 'SF', AMOUNT, NULL)) SERVICE_FEE_AMOUNT,
                       SUM(DECODE(ENTRY_TYPE, 'AD', AMOUNT, NULL)) ADJUST_AMOUNT
                  FROM (
                    SELECT D.CUSTOMER_BANK_ID, L.ENTRY_TYPE, SUM(L.AMOUNT) AMOUNT
                      FROM LEDGER L, BATCH B, DOC D
                      WHERE L.BATCH_ID = B.BATCH_ID
                      AND L.DELETED = 'N'
                      AND PAYMENTS_PKG.ENTRY_PAYABLE(L.SETTLE_STATE_ID,L.ENTRY_TYPE) = 'Y'
                      AND B.DOC_ID = D.DOC_ID
                      AND NVL(L.PAID_DATE, MIN_DATE) < l_as_of
                      AND L.LEDGER_DATE < l_as_of
                      GROUP BY D.CUSTOMER_BANK_ID, L.ENTRY_TYPE) M
                  GROUP BY CUSTOMER_BANK_ID) A
            WHERE CB.CUSTOMER_BANK_ID = A.CUSTOMER_BANK_ID (+)
            AND CB.CUSTOMER_ID = C.CUSTOMER_ID
            ORDER BY UPPER(CUSTOMER_NAME), BANK_ACCT_NBR;
        RETURN cur;
    END;
    
    FUNCTION FORMAT_EFT_REASON(
        l_payment_sched_id BATCH.PAYMENT_SCHEDULE_ID%TYPE,
        l_min_pay_date BATCH.START_DATE%TYPE,
        l_max_pay_date BATCH.END_DATE%TYPE
     ) RETURN VARCHAR
    IS
        l_text VARCHAR(8000);
    BEGIN
        SELECT DESCRIPTION
          INTO l_text
          FROM PAYMENT_SCHEDULE
         WHERE PAYMENT_SCHEDULE_ID = l_payment_sched_id;
        IF l_min_pay_date = l_max_pay_date THEN
            l_text := l_text || ' on ' || TO_CHAR(l_min_pay_date, 'MM-DD-YYYY');
        ELSE
            l_text := l_text || ' from ' || TO_CHAR(l_min_pay_date, 'MM-DD-YYYY')
                || ' to ' || TO_CHAR(l_max_pay_date, 'MM-DD-YYYY');
        END IF;
        RETURN l_text;
    END;
    /*
    FUNCTION FORMAT_EFT_REASON(
        l_eft_id EFT.EFT_ID%TYPE
     ) RETURN VARCHAR
    IS
        l_text VARCHAR(8000) := '';
        CURSOR l_cur IS
            SELECT PAYMENT_SCHEDULE_ID, MIN(PAYMENT_SCHEDULE_DATE) MIN_PAY_DATE,
                   MAX(PAYMENT_SCHEDULE_DATE) MAX_PAY_DATE
            FROM PAYMENTS P
            WHERE P.EFT_ID = l_eft_id
            GROUP BY PAYMENT_SCHEDULE_ID;
    BEGIN
        FOR l_rec IN l_cur LOOP
            IF LENGTH(l_text) > 0 THEN
                l_text := l_text || ' and ';
            END IF;
            l_text := l_text || FORMAT_EFT_REASON(l_rec.PAYMENT_SCHEDULE_ID, l_rec.MIN_PAY_DATE, l_rec.MAX_PAY_DATE);
        END LOOP;
        RETURN l_text;
    END;
    */

    -- Puts ledger record into a new batch
    PROCEDURE      DELAY_ENTRY (
      l_ledger_id IN LEDGER.LEDGER_ID%TYPE
      )
    IS
      l_status DOC.STATUS%TYPE;
      l_batch_id LEDGER.BATCH_ID%TYPE;
      l_entry_date LEDGER.ENTRY_DATE%TYPE;
      l_new_batch_id LEDGER.BATCH_ID%TYPE;
    BEGIN
        l_status := FREEZE_DOC_STATUS_LEDGER_ID(l_ledger_id);
        IF l_status IN('O') THEN
    		 RAISE_APPLICATION_ERROR(-20101, 'The Document must be locked before this entry can be pulled out of it');
        ELSIF l_status NOT IN('L') THEN
             RAISE_APPLICATION_ERROR(-20142, 'This entry belongs to a document that is already approved.');
        END IF;
        -- put in new batch
        SELECT L.BATCH_ID, L.ENTRY_DATE
          INTO l_batch_id, l_entry_date
          FROM LEDGER L
         WHERE LEDGER_ID = l_ledger_id;
        l_new_batch_id := GET_NEW_BATCH(l_batch_id, l_entry_date);
        UPDATE LEDGER L
           SET BATCH_ID = l_new_batch_id
         WHERE LEDGER_ID = l_ledger_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
    		 RAISE_APPLICATION_ERROR(-20100, 'Entry not found');
    	WHEN OTHERS THEN
    		 RAISE;
    END;
    
    -- Marks ledger record as deleted
    PROCEDURE      DELETE_ENTRY (
      l_ledger_id IN LEDGER.LEDGER_ID%TYPE)
    IS
      l_status DOC.STATUS%TYPE;
    BEGIN
        l_status := FREEZE_DOC_STATUS_LEDGER_ID(l_ledger_id);
        IF l_status IN('O') THEN
    		 NULL; --RAISE_APPLICATION_ERROR(-20101, 'The Document must be locked before this entry can be pulled out of it');
        ELSIF l_status NOT IN('L') THEN
             RAISE_APPLICATION_ERROR(-20142, 'This entry belongs to a document that is already approved.');
        END IF;
        -- mark as deleted (remove net revenue fees also)
        UPDATE LEDGER L
           SET DELETED = 'Y'
         WHERE LEDGER_ID = l_ledger_id
            OR RELATED_LEDGER_ID = l_ledger_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
    		 RAISE_APPLICATION_ERROR(-20100, 'Entry not found');
    	WHEN OTHERS THEN
    		 RAISE;
    END;

    -- Puts refund into a new batch
    PROCEDURE      DELAY_REFUND_BY_TRAN_ID (
      l_trans_id IN LEDGER.TRANS_ID%TYPE)
    IS
      l_ledger_id LEDGER.LEDGER_ID%TYPE;
    BEGIN
    	 SELECT LEDGER_ID
           INTO l_ledger_id
           FROM LEDGER
          WHERE TRANS_ID = l_trans_id
            AND ENTRY_TYPE = 'RF';
          DELAY_ENTRY(l_ledger_id);
    END;
    
    PROCEDURE ADJUSTMENT_INS (
        l_ledger_id OUT NOCOPY LEDGER.LEDGER_ID%TYPE,
       	l_doc_id IN BATCH.DOC_ID%TYPE,
        l_terminal_id IN BATCH.TERMINAL_ID%TYPE,
    	l_reason IN LEDGER.DESCRIPTION%TYPE,
    	l_amt IN LEDGER.AMOUNT%TYPE,
    	l_user_id IN LEDGER.CREATE_BY%TYPE,
		lc_split_payment_flag IN CHAR,
		lc_split_payment_interval_cd IN CHAR,
		ln_split_number_of_payments IN INTEGER,
		ld_first_split_payment_date IN DATE
	)
    IS
        l_batch_id LEDGER.BATCH_ID%TYPE;
        l_curr_id DOC.CURRENCY_ID%TYPE;
        l_term_curr_id DOC.CURRENCY_ID%TYPE;
        l_fee_date LEDGER.ENTRY_DATE%TYPE;
        l_customer_bank_id CORP.DOC.CUSTOMER_BANK_ID%TYPE;
        l_status DOC.STATUS%TYPE;
        l_lock VARCHAR2(128);
		ln_count INTEGER;
		ln_split_payment_amt LEDGER.AMOUNT%TYPE;
		ld_payment_date BATCH.END_DATE%TYPE;
    BEGIN
        SELECT D.CUSTOMER_BANK_ID, D.CURRENCY_ID
          INTO l_customer_bank_id, l_curr_id
          FROM CORP.DOC D
         WHERE D.DOC_ID = l_doc_id;
		 
		IF l_terminal_id IS NOT NULL THEN
			SELECT FEE_CURRENCY_ID
			INTO l_term_curr_id
			FROM REPORT.TERMINAL
			WHERE TERMINAL_ID = l_terminal_id;
			
            -- ensure that currency matches
            IF NVL(l_curr_id, 0) <> l_term_curr_id THEN
                RAISE_APPLICATION_ERROR(-20020, 'Terminal uses a different currency than the doc''s currency. Adjustment not added');
            END IF;
		END IF;
		 
        l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_customer_bank_id);
        l_status := GET_DOC_STATUS(l_doc_id);
        IF l_status NOT IN('O', 'L') THEN
            RAISE_APPLICATION_ERROR(-20021, 'This doc must be open or locked to add adjustments. Adjustment not added');
    	END IF;
		
		IF lc_split_payment_flag = 'Y' AND lc_split_payment_interval_cd IN ('D', 'W', 'M') AND ln_split_number_of_payments BETWEEN 2 AND 999 AND TRUNC(ld_first_split_payment_date) >= TRUNC(SYSDATE) THEN
			ln_split_payment_amt := ROUND(l_amt / ln_split_number_of_payments, 2);
			ln_count := 0;
			ld_payment_date := TRUNC(ld_first_split_payment_date);
			LOOP
				ln_count := ln_count + 1;
				IF ln_count >= ln_split_number_of_payments THEN
					ln_split_payment_amt := l_amt - ln_split_payment_amt * (ln_count - 1);
				END IF;
				
				SELECT MAX(B.BATCH_ID)
				  INTO l_batch_id
				  FROM BATCH B
				 WHERE B.DOC_ID = l_doc_id
				   AND NVL(B.TERMINAL_ID, 0) = NVL(l_terminal_id, 0)
				   AND B.PAYMENT_SCHEDULE_ID = 9
				   AND B.END_DATE = ld_payment_date;
			
				IF l_batch_id IS NULL THEN
					-- create a batch and set the doc id
					SELECT BATCH_SEQ.NEXTVAL
					  INTO l_batch_id
					  FROM DUAL;
					INSERT INTO BATCH(
						BATCH_ID,
						DOC_ID,
						TERMINAL_ID,
						PAYMENT_SCHEDULE_ID,
						START_DATE,
						END_DATE,
						BATCH_STATE_CD)
					  VALUES(
						l_batch_id,
						l_doc_id,
						DECODE(l_terminal_id, 0, NULL, l_terminal_id),
						9,
						ld_payment_date,
						ld_payment_date,
						'O');
				END IF;
				SELECT LEDGER_SEQ.NEXTVAL
				  INTO l_ledger_id
				  FROM DUAL;
				INSERT INTO LEDGER(
					LEDGER_ID,
					ENTRY_TYPE,
					AMOUNT,
					ENTRY_DATE,
					BATCH_ID,
					SETTLE_STATE_ID,
					LEDGER_DATE,
					DESCRIPTION,
					CREATE_BY,
					REPORT_DATE)
				   VALUES (
					l_ledger_id,
					'AD',
					ln_split_payment_amt,
					ld_payment_date,
					l_batch_id,
					2 /*process-no require*/,
					ld_payment_date,
					l_reason,
					l_user_id,
					ld_payment_date);
				-- create net revenue fee also
				INSERT INTO LEDGER(
					LEDGER_ID,
					RELATED_LEDGER_ID,
					SERVICE_FEE_ID,
					ENTRY_TYPE,
					AMOUNT,
					ENTRY_DATE,
					BATCH_ID,
					SETTLE_STATE_ID,
					LEDGER_DATE,
					DESCRIPTION,
					REPORT_DATE)
				 SELECT
					LEDGER_SEQ.NEXTVAL,
					l_ledger_id,
					SF.SERVICE_FEE_ID,
					'SF',
					-ln_split_payment_amt * SF.FEE_PERCENT,
					ld_payment_date,
					l_batch_id,
					2 /*process-no require*/,
					ld_payment_date,
					F.FEE_NAME,
					ld_payment_date
				  FROM SERVICE_FEES SF, FEES F
				 WHERE SF.FEE_ID = F.FEE_ID
				   AND SF.TERMINAL_ID = l_terminal_id
				   AND ld_payment_date BETWEEN NVL(SF.START_DATE, MIN_DATE)
						  AND NVL(SF.END_DATE, MAX_DATE)
				   AND SF.FREQUENCY_ID = 6;
				   
				IF ln_count >= ln_split_number_of_payments THEN
					EXIT;
				END IF;
				
				IF lc_split_payment_interval_cd = 'D' THEN
					ld_payment_date := ld_payment_date + 1;
				ELSIF lc_split_payment_interval_cd = 'W' THEN
					ld_payment_date := ld_payment_date + 7;
				ELSIF lc_split_payment_interval_cd = 'M' THEN
					ld_payment_date := ADD_MONTHS(ld_payment_date, 1);
				END IF;
			END LOOP;
		ELSE
			SELECT MAX(B.BATCH_ID)
			  INTO l_batch_id
			  FROM BATCH B
			 WHERE B.DOC_ID = l_doc_id
			   AND NVL(B.TERMINAL_ID, 0) = NVL(l_terminal_id, 0)
			   AND B.PAYMENT_SCHEDULE_ID = 1;
		
			IF l_batch_id IS NULL THEN
				-- create a batch and set the doc id
				SELECT BATCH_SEQ.NEXTVAL
				  INTO l_batch_id
				  FROM DUAL;
				INSERT INTO BATCH(
					BATCH_ID,
					DOC_ID,
					TERMINAL_ID,
					PAYMENT_SCHEDULE_ID,
					START_DATE,
					END_DATE,
					BATCH_STATE_CD)
				  VALUES(
					l_batch_id,
					l_doc_id,
					DECODE(l_terminal_id, 0, NULL, l_terminal_id),
					1,
					SYSDATE,
					DECODE(l_status, 'L', SYSDATE, 'O', null),
					DECODE(l_status, 'L', 'F', 'O', 'L'));
			END IF;
			SELECT LEDGER_SEQ.NEXTVAL, SYSDATE
			  INTO l_ledger_id, l_fee_date
			  FROM DUAL;
			INSERT INTO LEDGER(
				LEDGER_ID,
				ENTRY_TYPE,
				AMOUNT,
				ENTRY_DATE,
				BATCH_ID,
				SETTLE_STATE_ID,
				LEDGER_DATE,
				DESCRIPTION,
				CREATE_BY)
			   VALUES (
				l_ledger_id,
				'AD',
				l_amt,
				l_fee_date,
				l_batch_id,
				2 /*process-no require*/,
				l_fee_date,
				l_reason,
				l_user_id);
			-- create net revenue fee also
			INSERT INTO LEDGER(
				LEDGER_ID,
				RELATED_LEDGER_ID,
				SERVICE_FEE_ID,
				ENTRY_TYPE,
				AMOUNT,
				ENTRY_DATE,
				BATCH_ID,
				SETTLE_STATE_ID,
				LEDGER_DATE,
				DESCRIPTION)
			 SELECT
				LEDGER_SEQ.NEXTVAL,
				l_ledger_id,
				SF.SERVICE_FEE_ID,
				'SF',
				-l_amt * SF.FEE_PERCENT,
				l_fee_date,
				l_batch_id,
				2 /*process-no require*/,
				l_fee_date,
				F.FEE_NAME
			  FROM SERVICE_FEES SF, FEES F
			 WHERE SF.FEE_ID = F.FEE_ID
			   AND SF.TERMINAL_ID = l_terminal_id
			   AND l_fee_date BETWEEN NVL(SF.START_DATE, MIN_DATE)
					  AND NVL(SF.END_DATE, MAX_DATE)
			   AND SF.FREQUENCY_ID = 6;
		END IF;
    END;
	
PROCEDURE ADJUSTMENT_INS (
		l_ledger_id OUT NOCOPY LEDGER.LEDGER_ID%TYPE,
       	l_doc_id IN BATCH.DOC_ID%TYPE,
        l_terminal_id IN BATCH.TERMINAL_ID%TYPE,
    	l_reason IN LEDGER.DESCRIPTION%TYPE,
    	l_amt IN LEDGER.AMOUNT%TYPE,
    	l_user_id IN LEDGER.CREATE_BY%TYPE
) IS
BEGIN
	ADJUSTMENT_INS(l_ledger_id, l_doc_id, l_terminal_id, l_reason, l_amt, l_user_id, 'N', NULL, NULL, NULL);
END;
	
PROCEDURE ADJUSTMENT_INS (
	pn_customer_id CORP.CUSTOMER.CUSTOMER_ID%TYPE,
	pv_user_name REPORT.USER_LOGIN.USER_NAME%TYPE,
	pv_currency_cd CORP.CURRENCY.CURRENCY_CODE%TYPE,
	pv_reason CORP.LEDGER.DESCRIPTION%TYPE,
	pn_amount CORP.LEDGER.AMOUNT%TYPE,
	pn_doc_id OUT CORP.DOC.DOC_ID%TYPE,
	pn_ledger_id OUT CORP.LEDGER.LEDGER_ID%TYPE
) IS
	ln_currency_id CORP.CURRENCY.CURRENCY_ID%TYPE;
	ln_customer_bank_id CORP.CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE;
	ln_business_unit_id CORP.BUSINESS_UNIT.BUSINESS_UNIT_ID%TYPE;
	ln_user_id REPORT.USER_LOGIN.USER_ID%TYPE;
BEGIN
	SELECT MAX(customer_bank_id) INTO ln_customer_bank_id
	FROM (
		SELECT cbt.customer_bank_id
		FROM corp.customer_bank_terminal cbt
		JOIN report.terminal t ON cbt.terminal_id = t.terminal_id
		WHERE t.customer_id = pn_customer_id AND NVL(cbt.start_date, MIN_DATE) <= SYSDATE AND NVL(cbt.end_date, MAX_DATE) > SYSDATE
		GROUP BY cbt.customer_bank_id
		ORDER BY COUNT(1) DESC
	) WHERE ROWNUM = 1;
	
	IF ln_customer_bank_id IS NULL THEN
		RAISE_APPLICATION_ERROR(-20703, 'Unable to find a customer bank account for this customer');
	END IF;
	
	SELECT MAX(business_unit_id) INTO ln_business_unit_id
	FROM (
		SELECT business_unit_id
		FROM report.terminal
		WHERE customer_id = pn_customer_id AND status != 'D'
		GROUP BY business_unit_id
		ORDER BY COUNT(1) DESC
	) WHERE ROWNUM = 1;
	
	IF ln_business_unit_id IS NULL THEN
		RAISE_APPLICATION_ERROR(-20704, 'Unable to find a terminal business unit for this customer');
	END IF;
	
	SELECT MAX(user_id) INTO ln_user_id
	FROM report.user_login
	WHERE user_name = pv_user_name;
	
	SELECT currency_id
	INTO ln_currency_id
	FROM corp.currency
	WHERE currency_code = pv_currency_cd;
	
	pn_doc_id := GET_OR_CREATE_DOC(ln_customer_bank_id, ln_currency_id, ln_business_unit_id);
	ADJUSTMENT_INS(pn_ledger_id, pn_doc_id, NULL, pv_reason, pn_amount, ln_user_id);
END;	
    
    PROCEDURE                             ADJUSTMENT_UPD
       (l_ledger_id IN LEDGER.LEDGER_ID%TYPE,
       	l_reason IN LEDGER.DESCRIPTION%TYPE,
       	l_amt IN LEDGER.AMOUNT%TYPE,
    	l_user_id IN LEDGER.CREATE_BY%TYPE)
    IS
        l_status CORP.DOC.STATUS%TYPE;
    BEGIN
  	    l_status := FREEZE_DOC_STATUS_LEDGER_ID(l_ledger_id);
        IF l_status NOT IN('O', 'L') THEN
            RAISE_APPLICATION_ERROR(-20021, 'This doc must be open or locked to update adjustments. Adjustment not updated');
    	END IF;
        UPDATE LEDGER SET DESCRIPTION = l_reason, AMOUNT = l_amt, CREATE_BY = l_user_id
          WHERE LEDGER_ID = l_ledger_id;
         -- update net revenue
         UPDATE LEDGER L SET AMOUNT = -l_amt * (
            SELECT SF.FEE_PERCENT
              FROM SERVICE_FEES SF
             WHERE L.SERVICE_FEE_ID = SF.SERVICE_FEE_ID
               AND SF.FREQUENCY_ID = 6)
          WHERE L.RELATED_LEDGER_ID = l_ledger_id
            AND L.ENTRY_TYPE = 'SF'
            AND EXISTS(SELECT 1
                FROM SERVICE_FEES SF
                WHERE L.SERVICE_FEE_ID = SF.SERVICE_FEE_ID
                AND SF.FREQUENCY_ID = 6
            );
    END;

    PROCEDURE LOCK_DOC(
        l_doc_id DOC.DOC_ID%TYPE,
        l_user_id IN DOC.CREATE_BY%TYPE)
    IS
    BEGIN
        IF FREEZE_DOC_STATUS_DOC_ID(l_doc_id) = 'O' THEN
            UPDATE DOC
               SET STATUS = 'L', UPDATE_DATE = SYSDATE
             WHERE DOC_ID = l_doc_id;

            --Close all closeable batches
            UPDATE BATCH B
               SET B.BATCH_STATE_CD = 'F',
                   B.BATCH_CLOSED_TS = SYSDATE,
                   B.END_DATE = NVL(B.END_DATE, SYSDATE)
             WHERE B.DOC_ID = l_doc_id
               AND B.BATCH_STATE_CD = 'L';

            UPDATE BATCH B
               SET B.BATCH_STATE_CD = (SELECT DECODE(COUNT(*), 0, 'D', 'U')
                      FROM REPORT.TERMINAL t
                      JOIN CORP.BATCH_CONFIRM CN ON t.CUSTOMER_ID = cn.CUSTOMER_ID
                     WHERE b.TERMINAL_ID = t.TERMINAL_ID
                       AND b.PAYMENT_SCHEDULE_ID = cn.PAYMENT_SCHEDULE_ID),
                   B.BATCH_CLOSED_TS = SYSDATE
             WHERE B.DOC_ID = l_doc_id
               AND B.BATCH_STATE_CD = 'O'
               AND B.PAYMENT_SCHEDULE_ID NOT IN(1,2,5,7,8)
               AND B.END_DATE <= SYSDATE;

            -- Remove unclosed batches
            DECLARE
                l_cnt PLS_INTEGER;
                l_cb_id DOC.CUSTOMER_BANK_ID%TYPE;
                l_cur_id DOC.CURRENCY_ID%TYPE;
                l_bu_id DOC.BUSINESS_UNIT_ID%TYPE;
                l_new_doc_id DOC.DOC_ID%TYPE;
            BEGIN
                SELECT COUNT(*)
                  INTO l_cnt
                  FROM BATCH B
                 WHERE B.DOC_ID = l_doc_id
                   AND BATCH_CLOSABLE(B.BATCH_ID, B.PAYMENT_SCHEDULE_ID, B.END_DATE, B.BATCH_STATE_CD) = 'N';
                IF l_cnt > 0 THEN
                    SELECT D.CUSTOMER_BANK_ID, D.CURRENCY_ID, D.BUSINESS_UNIT_ID
                      INTO l_cb_id, l_cur_id, l_bu_id
                      FROM DOC D
                    WHERE D.DOC_ID = l_doc_id;
                    l_new_doc_id := GET_OR_CREATE_DOC(l_cb_id, l_cur_id, l_bu_id);
                   UPDATE BATCH B
                       SET B.DOC_ID = l_new_doc_id
                     WHERE B.DOC_ID = l_doc_id
                       AND BATCH_CLOSABLE(B.BATCH_ID, B.PAYMENT_SCHEDULE_ID, B.END_DATE, B.BATCH_STATE_CD) = 'N';
                END IF;
            END;
            
            -- Remove any unsettled trans from as batches
            DECLARE
                CURSOR l_cur IS
                  SELECT L.LEDGER_ID, L.BATCH_ID, L.ENTRY_DATE
                    FROM LEDGER L
                   WHERE ENTRY_PAYABLE(L.SETTLE_STATE_ID, L.ENTRY_TYPE) = '?'
                     AND L.DELETED = 'N'
                     AND L.BATCH_ID IN(
                            SELECT B.BATCH_ID
                              FROM BATCH B
                             WHERE B.DOC_ID = l_doc_id);
                TYPE t_rec_list IS TABLE OF l_cur%ROWTYPE;
                l_recs t_rec_list;
                l_new_batch_id LEDGER.BATCH_ID%TYPE;
            BEGIN
                OPEN l_cur;
                FETCH l_cur BULK COLLECT INTO l_recs;
                CLOSE l_cur;

                IF l_recs.FIRST IS NOT NULL THEN
                    FOR i IN l_recs.FIRST .. l_recs.LAST LOOP
                        l_new_batch_id := GET_NEW_BATCH(l_recs(i).BATCH_ID, l_recs(i).ENTRY_DATE);
                        UPDATE LEDGER L
                           SET L.BATCH_ID = l_new_batch_id
                         WHERE L.LEDGER_ID = l_recs(i).LEDGER_ID;
                    END LOOP;
                END IF;
            END;

            -- add rounding entries
            ADD_BATCH_ROUNDING_ENTRIES(l_doc_id);
            ADD_ROUNDING_ENTRY(l_doc_id);
        END IF;
    END;

    PROCEDURE      APPROVE_PAYMENT (
      l_doc_id IN DOC.DOC_ID%TYPE,
      l_user_id IN DOC.APPROVE_BY%TYPE)
    IS
      l_total DOC.TOTAL_AMOUNT%TYPE;
    BEGIN
        IF FREEZE_DOC_STATUS_DOC_ID(l_doc_id) = 'L' THEN
            -- add Positive Net Revenue Adjustment, if necessary
            DECLARE
                l_nrf_amt LEDGER.AMOUNT%TYPE;
                l_fee_date LEDGER.ENTRY_DATE%TYPE;
                l_ledger_id LEDGER.LEDGER_ID%TYPE;
                l_batch_id BATCH.BATCH_ID%TYPE;
            BEGIN
                SELECT NVL(SUM(AMOUNT), 0), SYSDATE
                  INTO l_nrf_amt, l_fee_date
                  FROM LEDGER L, BATCH B, SERVICE_FEES SF
                 WHERE L.BATCH_ID = B.BATCH_ID
                   AND B.DOC_ID = l_doc_id
                   AND L.DELETED = 'N'
                   AND L.ENTRY_TYPE = 'SF'
                   AND L.SERVICE_FEE_ID = SF.SERVICE_FEE_ID
                   AND SF.FREQUENCY_ID = 6;
                 IF l_nrf_amt > 0 THEN
                    -- l_nrf_amt is positive (money to the customer), so back it out with an adjustment
                    SELECT MAX(B.BATCH_ID)
                      INTO l_batch_id
                      FROM BATCH B
                     WHERE B.DOC_ID = l_doc_id
                       AND B.PAYMENT_SCHEDULE_ID = 7;
                    IF l_batch_id IS NULL THEN
                	    SELECT BATCH_SEQ.NEXTVAL
                          INTO l_batch_id
                          FROM DUAL;
                        INSERT INTO BATCH(
                            BATCH_ID,
                            DOC_ID,
                            TERMINAL_ID,
                            PAYMENT_SCHEDULE_ID,
                            START_DATE,
                            END_DATE,
                            BATCH_STATE_CD)
                          VALUES(
                            l_batch_id,
                            l_doc_id,
                            NULL,
                            7,
                            l_fee_date,
                            l_fee_date,
                            'F');
                    END IF;
                	SELECT LEDGER_SEQ.NEXTVAL
                      INTO l_ledger_id
                      FROM DUAL;
                	INSERT INTO LEDGER(
                        LEDGER_ID,
                        ENTRY_TYPE,
                        AMOUNT,
                        ENTRY_DATE,
                        BATCH_ID,
                        SETTLE_STATE_ID,
                        LEDGER_DATE,
                        DESCRIPTION,
                        CREATE_BY)
                       VALUES (
                        l_ledger_id,
                        'AD',
                        -l_nrf_amt,
                        l_fee_date,
                        l_batch_id,
                        2 /*process-no require*/,
                        l_fee_date,
                        'Net Revenue Hurdle Not Met',
                        l_user_id);
                 END IF;
            END;
            ADD_ROUNDING_ENTRY(l_doc_id);
            SELECT NVL(SUM(AMOUNT), 0)
              INTO l_total
              FROM LEDGER L, BATCH B
             WHERE L.DELETED = 'N'
               AND ENTRY_PAYABLE(L.SETTLE_STATE_ID,L.ENTRY_TYPE) = 'Y'
               AND L.BATCH_ID = B.BATCH_ID
               AND B.DOC_ID = l_doc_id;

        	UPDATE DOC D
               SET APPROVE_BY = l_user_id,
                   UPDATE_DATE = SYSDATE,
                   STATUS = DECODE(l_total, 0, 'D', 'A'),
                   TOTAL_AMOUNT = l_total,
                   DOC_TYPE = (
                        SELECT DECODE(CB.IS_EFT, 'Y', 'E', 'P') -- electronic or paper
                               || CASE WHEN l_total > 0 THEN 'C' ELSE 'D' END -- credit or debit
                          FROM CUSTOMER_BANK CB
                         WHERE D.CUSTOMER_BANK_ID = CB.CUSTOMER_BANK_ID)
      	 		WHERE DOC_ID = l_doc_id;
          END IF;
    END;
    
    PROCEDURE      UNAPPROVE_PAYMENT (
      l_doc_id IN DOC.DOC_ID%TYPE)
    IS
    BEGIN
        UPDATE DOC D SET APPROVE_BY = NULL, UPDATE_DATE = SYSDATE, STATUS = 'L',
            TOTAL_AMOUNT = NULL, DOC_TYPE = '--'
  	 		WHERE DOC_ID = l_doc_id
              AND STATUS = 'A';
        IF SQL%FOUND THEN
            -- Remove adjustments for positive net revenue fees
            UPDATE LEDGER
               SET DELETED = 'Y'
             WHERE ENTRY_TYPE = 'AD'
               AND TRANS_ID IS NULL
               AND BATCH_ID IN(
                    SELECT BATCH_ID
                      FROM BATCH
                     WHERE DOC_ID = l_doc_id
                       AND PAYMENT_SCHEDULE_ID = 7);
        END IF;
    END;
    
    PROCEDURE      UPLOAD_TO_ORF (
      l_doc_id IN DOC.DOC_ID%TYPE)
    IS
    BEGIN
    	 INSERT INTO USAT_INVOICE_INTERFACE@FINANCIALS(
            VENDOR_NAME_I, --                                        VARCHAR2 (240)                        REQUIRED     vendor name
            INVOICE_NUMBER_I, --                                    VARCHAR2 (240)                       REQUIRED      invoice number
            INVOICE_DATE_I, --                                           DATE                                           OPTIONAL
            INVOICE_AMOUNT_I, --                                    NUMBER                                     REQUIRED      amount of invoice
            DISTRIBUTION_SET_NAME_I, --                     VARCHAR2 (240)                       REQUIRED
            GL_DATE_I, --                                                      DATE                                            OPTIONAL
            PAY_GROUP_LOOKUP_CODE_I, --                 VARCHAR2 (240)                        REQUIRED     pay group
            VENDOR_SITE_CODE_I, --                                VARCHAR2 (25)                          REQUIRED     site code
            INVOICE_CURRENCY_CODE_I, --                  VARCHAR2 (15)                           REQUIRED     invoice currency
            PAYMENT_METHOD_LOOKUP_CODE_I, --   VARCHAR2 (25)                         REQUIRED     payment method
            VENDOR_NUMBER_I) --                                    VARCHAR2 (200)                        OPTIONAL
        SELECT
            C.CUSTOMER_NAME,
            D.REF_NBR,
            SYSDATE,
            D.TOTAL_AMOUNT,
            BU.DISTRIBUTION_SET_CD,
            SYSDATE,
            (CASE WHEN CU.CURRENCY_CODE in ('CAD') THEN 'CAD' ELSE BU.PAY_GROUP_CD END)  "PAY_GROUP_CD",
            CB.CUSTOMER_BANK_ID,
            CU.CURRENCY_CODE,
            DECODE(CB.IS_EFT, 'Y', 'EFT', 'CHECK'),
            C.CUSTOMER_ID
        FROM CORP.CUSTOMER C, CORP.CUSTOMER_BANK CB, CORP.DOC D, CORP.BUSINESS_UNIT BU, CORP.CURRENCY CU
        WHERE D.DOC_ID = l_doc_id
        AND D.CUSTOMER_BANK_ID = CB.CUSTOMER_BANK_ID
        AND CB.CUSTOMER_ID = C.CUSTOMER_ID
        AND D.CURRENCY_ID = CU.CURRENCY_ID
        AND D.BUSINESS_UNIT_ID = BU.BUSINESS_UNIT_ID
		AND NVL(D.TOTAL_AMOUNT, 0) != 0;
    END;
    
    PROCEDURE      MARK_DOC_PAID (
      l_doc_id IN DOC.DOC_ID%TYPE,
      l_user_id IN DOC.APPROVE_BY%TYPE)
    IS
        l_paid_date DATE := SYSDATE;
    BEGIN
    	 UPDATE DOC SET UPDATE_DATE = l_paid_date, STATUS = 'P'
          WHERE DOC_ID = l_doc_id
            AND STATUS = 'A';
         IF SQL%FOUND THEN
             UPDATE LEDGER SET PAID_DATE = l_paid_date
              WHERE BATCH_ID IN(SELECT BATCH_ID FROM BATCH WHERE DOC_ID = l_doc_id);
             UPDATE BATCH SET PAID_DATE = l_paid_date
              WHERE DOC_ID = l_doc_id;
			 UPLOAD_TO_ORF(l_doc_id);
			 UPDATE DOC SET SENT_BY = l_user_id, SENT_DATE = l_paid_date
			 WHERE DOC_ID = l_doc_id;
         END IF;
    END;
    
    PROCEDURE PROCESS_FEES_UPD
       (l_terminal_id IN PROCESS_FEES.TERMINAL_ID%TYPE,
        l_trans_type_id IN PROCESS_FEES.TRANS_TYPE_ID%TYPE,
        l_fee_percent IN PROCESS_FEES.FEE_PERCENT%TYPE,
        l_fee_amount IN PROCESS_FEES.FEE_AMOUNT%TYPE,
        l_fee_minimum IN PROCESS_FEES.MIN_AMOUNT%TYPE,
        l_effective_date IN PROCESS_FEES.END_DATE%TYPE DEFAULT SYSDATE,
        l_override IN CHAR DEFAULT 'N',
        l_commission_percent IN PROCESS_FEE_COMMISSION.COMMISSION_PERCENT%TYPE DEFAULT NULL,
        l_commission_amount IN PROCESS_FEE_COMMISSION.COMMISSION_AMOUNT%TYPE DEFAULT NULL,
        l_commission_minimum IN PROCESS_FEE_COMMISSION.COMMISSION_MIN%TYPE DEFAULT NULL,
        l_commission_bank_id IN CORP.CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE DEFAULT NULL)
    IS
        l_lock VARCHAR2(128);
        l_current_date PROCESS_FEES.END_DATE%TYPE := SYSDATE;
        l_fee_effective_date PROCESS_FEES.END_DATE%TYPE := NVL(l_effective_date, l_current_date);
        l_fee_override CHAR := NVL(l_override, 'N');
        l_original_license_id PROCESS_FEES.ORIGINAL_LICENSE_ID%TYPE;
        l_process_fee_id PROCESS_FEES.PROCESS_FEE_ID%TYPE;
    BEGIN
    	 --CHECK IF WE already paid on transactions before the effective date
    	 IF l_fee_override <> 'Y' AND l_fee_effective_date < l_current_date THEN
    	 	 DECLARE
    		     l_tran_id LEDGER.TRANS_ID%TYPE;
    			BEGIN
    		 	 SELECT /*+ index(T USAT_IX_TRANS_TERMINAL_ID) */ MIN(L.TRANS_ID)
                   INTO l_tran_id
                   FROM LEDGER L, BATCH B, TRANS T, DOC D
                  WHERE T.TRAN_ID = L.TRANS_ID
                    AND T.TRANS_TYPE_ID = l_trans_type_id
                    AND T.TERMINAL_ID = l_terminal_id
                    AND T.CLOSE_DATE >= l_fee_effective_date
                    AND B.TERMINAL_ID = l_terminal_id
                    AND L.BATCH_ID = B.BATCH_ID
                    AND B.DOC_ID = D.DOC_ID
                    AND L.DELETED = 'N'
                    AND D.STATUS NOT IN('O', 'D');
    			 IF l_tran_id IS NOT NULL THEN  --BIG PROBLEM
    			 	 RAISE_APPLICATION_ERROR(-20011, 'Transaction #' || TO_CHAR(l_tran_id) || ', which ocurred on the terminal whose fees you are updating, has already been paid.');
    			 END IF;
    		 END;
    	 END IF;
       IF (nvl(l_commission_amount, 0) != 0 or nvl(l_commission_percent, 0) != 0) and nvl(l_commission_bank_id,0) = 0 THEN
         RAISE_APPLICATION_ERROR(-20308, 'commission_bank_id is required for commission fees');
       END IF;
       l_lock := GLOBALS_PKG.REQUEST_LOCK('TERMINAL.TERMINAL_ID', l_terminal_id);
       SELECT MAX(ORIGINAL_LICENSE_ID) INTO l_original_license_id FROM PROCESS_FEES WHERE TERMINAL_ID = l_terminal_id
          AND TRANS_TYPE_ID = l_trans_type_id AND NVL(END_DATE, l_fee_effective_date) >= l_fee_effective_date;
    	 UPDATE PROCESS_FEES SET END_DATE = l_fee_effective_date WHERE TERMINAL_ID = l_terminal_id
    	 	AND TRANS_TYPE_ID = l_trans_type_id AND NVL(END_DATE, l_fee_effective_date) >= l_fee_effective_date;
    	 BEGIN
            DELETE FROM PROCESS_FEES WHERE TERMINAL_ID = l_terminal_id
        	 	AND TRANS_TYPE_ID = l_trans_type_id AND NVL(START_DATE, MIN_DATE) >= NVL(END_DATE, MAX_DATE);	
         EXCEPTION
            WHEN CHILD_RECORD_FOUND THEN
                NULL;
            WHEN OTHERS THEN
                RAISE;
       END;
       IF NVL(l_fee_percent, 0) <> 0 OR NVL(l_fee_amount, 0) <> 0 THEN	
         select PROCESS_FEE_SEQ.NEXTVAL into l_process_fee_id from dual;
         INSERT INTO PROCESS_FEES(PROCESS_FEE_ID, TERMINAL_ID, TRANS_TYPE_ID, FEE_PERCENT, FEE_AMOUNT, MIN_AMOUNT, START_DATE, ORIGINAL_LICENSE_ID)
           VALUES(l_process_fee_id, l_terminal_id, l_trans_type_id, l_fee_percent, l_fee_amount, l_fee_minimum, l_fee_effective_date, l_original_license_id);
         IF nvl(l_commission_amount, 0) <> 0 or nvl(l_commission_percent, 0) <> 0 THEN
           insert into process_fee_commission(process_fee_id, customer_bank_id, 
             commission_amount, commission_percent, commission_min,
             sell_amount, sell_percent, sell_min,
             buy_amount, buy_percent, buy_min)
             values (l_process_fee_id, l_commission_bank_id, 
             nvl(l_commission_amount,0), nvl(l_commission_percent,0), nvl(l_commission_minimum,0),
             nvl(l_fee_amount,0), nvl(l_fee_percent,0), nvl(l_fee_minimum,0),
             nvl(l_fee_amount,0) - nvl(l_commission_amount,0), nvl(l_fee_percent,0) - nvl(l_commission_percent,0), nvl(l_fee_minimum,0) - nvl(l_commission_minimum,0));
         END IF;
      END IF;
    END;
    
    FUNCTION GET_SERVICE_FEE_DATE_FMT(
        l_months FREQUENCY.MONTHS%TYPE,
        l_days FREQUENCY.DAYS%TYPE)
     RETURN VARCHAR
    IS
    BEGIN
        IF l_months >= 12  AND MOD(l_months, 12.0) = 0 THEN
            RETURN 'FMYYYY';
        ELSIF l_months > 0 THEN
            RETURN 'FMMonth, YYYY';
        ELSIF l_days >= 1 AND MOD(l_days, 1.0) = 0 THEN
            RETURN 'FMMM/DD/YYYY';
        ELSE
            RETURN 'FMMM/DD/YYYY HH:MI AM';
        END IF;
    END;
    
    PROCEDURE SCAN_FOR_SERVICE_FEES
    IS
        CURSOR c_fee
        IS
            SELECT DISTINCT
                   SF.SERVICE_FEE_ID,
                   SF.TERMINAL_ID,
                   CBT.CUSTOMER_BANK_ID,
                   F.FEE_ID,
                   SF.FEE_AMOUNT,
                   Q.MONTHS,
                   Q.DAYS,
                   SF.LAST_PAYMENT,
                   SF.START_DATE,
                   LEAST(NVL(
                        CASE WHEN Q.MONTHS = 0 THEN SF.END_DATE + Q.DAYS
                             ELSE TRUNC(LAST_DAY(ADD_MONTHS(SF.END_DATE, Q.MONTHS)), 'DD')
                        END
                   , MAX_DATE), SYSDATE) EFFECTIVE_END_DATE,
                   F.FEE_NAME,
                   T.FEE_CURRENCY_ID,
                   DECODE(T.PAYMENT_SCHEDULE_ID, 2, 'Y', 8, 'Y', 'N') AS_ACCUM,
                   F.INITIATION_TYPE_CD,
                   SF.GRACE_PERIOD_DATE,
				   SF.TRIGGERING_DATE,
				   SF.NO_TRIGGER_EVENT_FLAG,
           SF.INACTIVE_FEE_AMOUNT,
           SF.INACTIVE_MONTHS_REMAINING
              FROM CORP.SERVICE_FEES SF
              JOIN CORP.FREQUENCY Q ON SF.FREQUENCY_ID = Q.FREQUENCY_ID
              JOIN CORP.CUSTOMER_BANK_TERMINAL CBT ON SF.TERMINAL_ID = CBT.TERMINAL_ID AND SYSDATE >= NVL(CBT.START_DATE, MIN_DATE) AND SYSDATE < NVL(CBT.END_DATE, MAX_DATE)
              JOIN CORP.FEES F ON SF.FEE_ID = F.FEE_ID
              JOIN REPORT.TERMINAL T ON SF.TERMINAL_ID = T.TERMINAL_ID              
             WHERE (Q.DAYS > 0 OR Q.MONTHS > 0)
               AND CASE WHEN Q.MONTHS = 0 THEN NVL(SF.LAST_PAYMENT, MIN_DATE) + Q.DAYS
                             ELSE TRUNC(LAST_DAY(ADD_MONTHS(NVL(SF.LAST_PAYMENT, MIN_DATE), Q.MONTHS)), 'DD')
                        END < LEAST(NVL(
                        CASE WHEN Q.MONTHS = 0 THEN SF.END_DATE + Q.DAYS
                             ELSE TRUNC(LAST_DAY(ADD_MONTHS(SF.END_DATE, Q.MONTHS)), 'DD')
                        END
                   , MAX_DATE), SYSDATE)
               AND (SF.LAST_PAYMENT IS NOT NULL 
				OR (SF.TRIGGERING_DATE IS NOT NULL AND (F.INITIATION_TYPE_CD = 'T'
					OR F.INITIATION_TYPE_CD IN('G', 'R') AND COALESCE(SF.NO_TRIGGER_EVENT_FLAG, 'N') != 'Y'))
				OR SF.GRACE_PERIOD_DATE < SYSDATE
        OR (F.INITIATION_TYPE_CD = 'I' AND SF.START_DATE < SYSDATE))
             ORDER BY SF.LAST_PAYMENT NULLS LAST, CBT.CUSTOMER_BANK_ID, SF.SERVICE_FEE_ID;

        l_last_payment                     SERVICE_FEES.LAST_PAYMENT%TYPE;
        l_prev_payment                     SERVICE_FEES.LAST_PAYMENT%TYPE;
        l_fmt VARCHAR2(50);
        l_ledger_id LEDGER.LEDGER_ID%TYPE;
        l_batch_id LEDGER.BATCH_ID%TYPE;
		    l_last_run_complete_ts DATE := TO_DATE(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('SCAN_FOR_SERVICE_FEES_LAST_RUN_COMPLETE_TS'), 'MM/DD/YYYY HH24:MI:SS');
        l_inactive_months_remaining SERVICE_FEES.INACTIVE_MONTHS_REMAINING%TYPE;
        l_cnt PLS_INTEGER;
    BEGIN
		IF l_last_run_complete_ts >= TO_DATE(TO_CHAR(CURRENT_TIMESTAMP, 'MM/DD/YYYY'), 'MM/DD/YYYY') 
			OR DBADMIN.PKG_GLOBAL.GET_APP_SETTING('SCAN_FOR_SERVICE_FEES_ENABLED') = 'N' THEN
			RETURN;
		END IF;
		
		UPDATE ENGINE.APP_SETTING
   		SET APP_SETTING_VALUE = TO_CHAR(CURRENT_TIMESTAMP, 'MM/DD/YYYY HH24:MI:SS')
   		WHERE APP_SETTING_CD = 'SCAN_FOR_SERVICE_FEES_LAST_RUN_START_TS';
		COMMIT;
	
        FOR r_fee IN c_fee LOOP
            -- If last payment is null, Use first transaction as last payment date
            IF r_fee.LAST_PAYMENT IS NULL THEN
                IF r_fee.INITIATION_TYPE_CD = 'I' THEN
                    l_last_payment := r_fee.START_DATE;
                ELSIF r_fee.INITIATION_TYPE_CD IN('G', 'T', 'R') THEN
                    l_last_payment := r_fee.TRIGGERING_DATE;
                    IF r_fee.INITIATION_TYPE_CD IN('G', 'R') AND r_fee.GRACE_PERIOD_DATE < NVL(l_last_payment, SYSDATE) THEN 
                        l_last_payment := r_fee.GRACE_PERIOD_DATE;
                        UPDATE CORP.SERVICE_FEES
                        SET TRIGGERING_DATE = l_last_payment
                        WHERE SERVICE_FEE_ID = r_fee.SERVICE_FEE_ID;
                     END IF;
                     IF l_last_payment IS NOT NULL THEN
                         IF r_fee.INITIATION_TYPE_CD IN('G', 'R') THEN
                             IF r_fee.MONTHS > 0 THEN
                                SELECT TRUNC(LAST_DAY(ADD_MONTHS(l_last_payment, -r_fee.MONTHS)), 'DD')
                                  INTO l_last_payment
                                  FROM DUAL;
                             ELSE
                                SELECT l_last_payment - r_fee.DAYS
                                  INTO l_last_payment
                                  FROM DUAL;
                             END IF;
                        END IF;
                    END IF;
                ELSE
                    l_last_payment := NULL;
                END IF;
            ELSE
                l_last_payment := r_fee.LAST_PAYMENT;
            END IF;
            IF l_last_payment IS NOT NULL THEN
              l_fmt := GET_SERVICE_FEE_DATE_FMT(r_fee.MONTHS, r_fee.DAYS);
              l_inactive_months_remaining := r_fee.INACTIVE_MONTHS_REMAINING;
              LOOP
                  l_prev_payment := l_last_payment;
                  IF r_fee.MONTHS > 0 THEN
                      SELECT TRUNC(LAST_DAY(ADD_MONTHS(l_last_payment, r_fee.MONTHS)), 'DD')
                        INTO l_last_payment
                        FROM DUAL;
                  ELSE
                      SELECT l_last_payment + r_fee.DAYS
                        INTO l_last_payment
                        FROM DUAL;
                  END IF;
                  EXIT WHEN l_last_payment >= r_fee.EFFECTIVE_END_DATE;
  
                  DECLARE
                     l_fee_amt CORP.LEDGER.AMOUNT%TYPE;
                     l_desc CORP.LEDGER.DESCRIPTION%TYPE;
                     l_hosts PLS_INTEGER;
                  BEGIN -- catch exception here
                      -- skip creation of fee if start date is after fee date
                      IF l_last_payment >= NVL(r_fee.start_date, l_last_payment) THEN
                          l_batch_id := GET_OR_CREATE_BATCH(
                                      r_fee.TERMINAL_ID,
                                      r_fee.CUSTOMER_BANK_ID,
                                      l_last_payment,
                                      r_fee.FEE_CURRENCY_ID,
                                      r_fee.AS_ACCUM);
                          SELECT LEDGER_SEQ.NEXTVAL
                            INTO l_ledger_id
                            FROM DUAL;
                          IF r_fee.fee_id = 9 THEN
                             SELECT MAX(DEVICE_HOST_COUNT(e.EPORT_SERIAL_NUM, l_last_payment))
                               INTO l_hosts
                               FROM REPORT.EPORT e, REPORT.TERMINAL_EPORT te
                              WHERE e.EPORT_ID = te.EPORT_ID
                                AND te.TERMINAL_ID = r_fee.TERMINAL_ID
                                AND l_last_payment BETWEEN NVL(te.START_DATE, MIN_DATE) AND NVL(te.END_DATE, MAX_DATE);
                             l_fee_amt := -ABS(r_fee.FEE_AMOUNT * l_hosts);
                             l_desc := r_fee.FEE_NAME || ' ('||TO_CHAR(l_hosts)||' hosts) ' || ' for ' || TO_CHAR(l_last_payment, l_fmt);
                          ELSIF r_fee.INITIATION_TYPE_CD IN('R') AND r_fee.INACTIVE_FEE_AMOUNT IS NOT NULL AND l_inactive_months_remaining > 0 THEN
                              SELECT ( 
                                  SELECT COUNT(*)
                                    FROM REPORT.TRANS x
                                   WHERE x.TERMINAL_ID = r_fee.TERMINAL_ID
                                     AND x.CLOSE_DATE >= l_prev_payment + 1
                                     AND x.CLOSE_DATE < l_last_payment + 1) + (   
                                  SELECT COUNT(*)
                                    FROM G4OP.DEX_FILE DF
                                    JOIN REPORT.TERMINAL_EPORT TE ON DF.EPORT_ID = TE.EPORT_ID AND DF.DEX_DATE >= NVL(TE.START_DATE, MIN_DATE) AND DF.DEX_DATE < NVL(TE.END_DATE, MAX_DATE)
                                   WHERE TE.TERMINAL_ID = r_fee.TERMINAL_ID 
                                     AND DF.CREATE_DT >= l_prev_payment + 1
                                     AND DF.CREATE_DT < l_last_payment + 1)
                               INTO l_cnt
                               FROM DUAL;
                             IF l_cnt = 0 THEN -- It's inactive
                                 l_fee_amt := -ABS(r_fee.INACTIVE_FEE_AMOUNT);
                                 l_desc := r_fee.FEE_NAME || ' for ' || TO_CHAR(l_last_payment, l_fmt) || ' (Inactive)';    
                                 l_inactive_months_remaining := l_inactive_months_remaining - 1;
                             ELSE
                                 l_fee_amt := -ABS(r_fee.FEE_AMOUNT);
                                 l_desc := r_fee.FEE_NAME || ' for ' || TO_CHAR(l_last_payment, l_fmt);                            
                             END IF;
                          ELSE
                             l_fee_amt := -ABS(r_fee.FEE_AMOUNT);
                             l_desc := r_fee.FEE_NAME || ' for ' || TO_CHAR(l_last_payment, l_fmt);
                          END IF;
                          INSERT INTO LEDGER(
                              LEDGER_ID,
                              ENTRY_TYPE,
                              SERVICE_FEE_ID,
                              AMOUNT,
                              ENTRY_DATE,
                              BATCH_ID,
                              SETTLE_STATE_ID,
                              LEDGER_DATE,
                              DESCRIPTION)
                          VALUES (
                              l_ledger_id,
                              'SF',
                              r_fee.SERVICE_FEE_ID,
                              l_fee_amt,
                              l_last_payment,
                              l_batch_id,
                              2,
                              l_last_payment,
                              l_desc);
                          SERVICE_FEE_COMMISSION_INS(l_ledger_id, null, r_fee.SERVICE_FEE_ID, r_fee.fee_id, 2, l_fee_amt, l_last_payment, l_last_payment);
                          -- create net revenue fee on transaction, if any
                          INSERT INTO LEDGER(
                              LEDGER_ID,
                              RELATED_LEDGER_ID,
                              ENTRY_TYPE,
                              SERVICE_FEE_ID,
                              AMOUNT,
                              ENTRY_DATE,
                              BATCH_ID,
                              SETTLE_STATE_ID,
                              LEDGER_DATE,
                              DESCRIPTION)
                          SELECT
                              LEDGER_SEQ.NEXTVAL,
                              l_ledger_id,
                              'SF',
                              SF.SERVICE_FEE_ID,
                              -l_fee_amt * SF.FEE_PERCENT,
                              l_last_payment,
                              l_batch_id,
                              2,
                              l_last_payment,
                              F.FEE_NAME
                          FROM SERVICE_FEES SF, FEES F
                         WHERE SF.FEE_ID = F.FEE_ID
                           AND SF.TERMINAL_ID = r_fee.TERMINAL_ID
                           AND l_last_payment BETWEEN NVL(SF.START_DATE, MIN_DATE)
                                AND NVL(SF.END_DATE, MAX_DATE)
                           AND SF.FREQUENCY_ID = 6;
                      END IF;
  
                      UPDATE SERVICE_FEES
                         SET LAST_PAYMENT = l_last_payment,
                             FIRST_PAYMENT = CASE WHEN FIRST_PAYMENT IS NULL OR l_last_payment < FIRST_PAYMENT THEN l_last_payment ELSE FIRST_PAYMENT END,
                             INACTIVE_MONTHS_REMAINING = l_inactive_months_remaining
                       WHERE SERVICE_FEE_ID = r_fee.SERVICE_FEE_ID;
  
                      COMMIT;             -- BECAREFUL OF CALLING THIS PROCEDURE SINCE IT HAS THIS COMMIT
                  EXCEPTION
                                   WHEN DUP_VAL_ON_INDEX THEN
                                           ROLLBACK;
                  END;
              END LOOP;
            END IF;
        END LOOP;
		
		UPDATE ENGINE.APP_SETTING
   		SET APP_SETTING_VALUE = TO_CHAR(CURRENT_TIMESTAMP, 'MM/DD/YYYY HH24:MI:SS')
   		WHERE APP_SETTING_CD = 'SCAN_FOR_SERVICE_FEES_LAST_RUN_COMPLETE_TS';
    END;

    PROCEDURE SERVICE_FEES_UPD(
        l_terminal_id IN SERVICE_FEES.TERMINAL_ID%TYPE,
        l_fee_id IN SERVICE_FEES.FEE_ID%TYPE,
        l_freq_id IN SERVICE_FEES.FREQUENCY_ID%TYPE,
        l_fee_amt IN SERVICE_FEES.FEE_AMOUNT%TYPE,
        l_fee_perc IN SERVICE_FEES.FEE_PERCENT%TYPE,
        l_effective_date IN SERVICE_FEES.END_DATE%TYPE DEFAULT SYSDATE,
        l_end_date IN SERVICE_FEES.END_DATE%TYPE,
        l_override IN CHAR DEFAULT 'N',
        pn_grace_days IN NUMBER DEFAULT 60,
        pc_no_trigger_event_flag IN SERVICE_FEES.NO_TRIGGER_EVENT_FLAG%TYPE DEFAULT NULL,
        pn_inactive_fee_amt IN SERVICE_FEES.INACTIVE_FEE_AMOUNT%TYPE DEFAULT NULL,
        pn_inactive_months IN SERVICE_FEES.INACTIVE_MONTHS_REMAINING%TYPE DEFAULT NULL,
        l_commission_amt IN SERVICE_FEE_COMMISSION.COMMISSION_AMOUNT%TYPE DEFAULT NULL,
        l_commission_bank_id IN CORP.CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE DEFAULT NULL)
    IS
        l_last_payment SERVICE_FEES.LAST_PAYMENT%TYPE;
        l_sf_id SERVICE_FEES.SERVICE_FEE_ID%TYPE;
        l_batch_id LEDGER.BATCH_ID%TYPE;
        l_next_payment SERVICE_FEES.LAST_PAYMENT%TYPE;
        l_first_payment SERVICE_FEES.LAST_PAYMENT%TYPE;
        l_lock VARCHAR2(128);
        l_current_date SERVICE_FEES.END_DATE%TYPE := SYSDATE;
        l_fee_effective_date SERVICE_FEES.END_DATE%TYPE := NVL(l_effective_date, l_current_date);
        l_fee_override CHAR := NVL(l_override, 'N');
        l_orig_start_date SERVICE_FEES.START_DATE%TYPE;
        l_orig_end_date SERVICE_FEES.END_DATE%TYPE;
        l_orig_amount SERVICE_FEES.FEE_AMOUNT%TYPE;
        l_orig_perc SERVICE_FEES.FEE_PERCENT%TYPE; 
        l_orig_inactive_amount SERVICE_FEES.INACTIVE_FEE_AMOUNT%TYPE;
        lc_initiation_type_cd CORP.FEES.INITIATION_TYPE_CD%TYPE;
        l_original_license_id SERVICE_FEES.ORIGINAL_LICENSE_ID%TYPE;
        l_service_fee_id SERVICE_FEES.SERVICE_FEE_ID%TYPE;
        l_orig_commission_amt CORP.SERVICE_FEE_COMMISSION.COMMISSION_AMOUNT%TYPE;
        l_orig_commission_bank_id CORP.SERVICE_FEE_COMMISSION.CUSTOMER_BANK_ID%TYPE;
        ln_use_freq_id CORP.SERVICE_FEES.FREQUENCY_ID%TYPE;
    BEGIN
         IF nvl(l_commission_amt, 0) != 0 and nvl(l_commission_bank_id, 0) = 0 THEN
           RAISE_APPLICATION_ERROR(-20308, 'commission_bank_id is required for commission fees');
         END IF;

    		SELECT INITIATION_TYPE_CD
          INTO lc_initiation_type_cd
          FROM CORP.FEES
         WHERE FEE_ID = l_fee_id;
        
        IF lc_initiation_type_cd = 'P' THEN
            ln_use_freq_id := 6;
        ELSIF lc_initiation_type_cd = 'A' THEN
            ln_use_freq_id := 7;
        ELSIF l_freq_id IS NULL OR l_freq_id IN(6,7) THEN
            SELECT DEFAULT_FREQUENCY_ID
              INTO ln_use_freq_id
              FROM CORP.FEES
             WHERE FEE_ID = l_fee_id;
        ELSE
            ln_use_freq_id := l_freq_id;
        END IF;
 
        l_lock := GLOBALS_PKG.REQUEST_LOCK('TERMINAL.TERMINAL_ID', l_terminal_id);
        
        BEGIN
            IF lc_initiation_type_cd = 'P' THEN
                --disallow change to net revenue fees that affect already paid entries
                SELECT MAX(ENTRY_DATE)
                  INTO l_last_payment
                  FROM LEDGER L, BATCH B, SERVICE_FEES SF, DOC D
                 WHERE B.TERMINAL_ID = l_terminal_id
                   AND SF.TERMINAL_ID = l_terminal_id
                   AND SF.FEE_ID = l_fee_id
                   AND SF.FREQUENCY_ID = ln_use_freq_id
                   AND B.DOC_ID = D.DOC_ID
                   AND D.STATUS NOT IN('O', 'D')
                   AND B.BATCH_ID = L.BATCH_ID
                   AND L.SERVICE_FEE_ID = SF.SERVICE_FEE_ID
                   AND L.ENTRY_TYPE = 'SF'
                   AND L.DELETED = 'N';
                IF NVL(l_last_payment, l_fee_effective_date) > l_fee_effective_date THEN -- BIG PROBLEM
                    RAISE_APPLICATION_ERROR(-20012, 'This net revenue fee was charged to the customer after the specified effective date.');
                ELSE
                    l_last_payment := MIN_DATE;
                END IF;
            ELSE
                SELECT MAX(LAST_PAYMENT), 
                       NVL(MIN(FIRST_PAYMENT), MIN_DATE),
                       CASE WHEN Q.MONTHS = 0 THEN l_fee_effective_date + Q.DAYS
                           ELSE TRUNC(LAST_DAY(ADD_MONTHS(l_fee_effective_date, Q.MONTHS)), 'DD')
                      END
                  INTO l_last_payment, l_first_payment, l_next_payment
                  FROM SERVICE_FEES SF, CORP.FREQUENCY Q
                 WHERE SF.TERMINAL_ID = l_terminal_id
                   AND SF.FEE_ID = l_fee_id
                   AND SF.FREQUENCY_ID = ln_use_freq_id
                   AND SF.FREQUENCY_ID = Q.FREQUENCY_ID
                 GROUP BY CASE WHEN Q.MONTHS = 0 THEN l_fee_effective_date + Q.DAYS
                           ELSE TRUNC(LAST_DAY(ADD_MONTHS(l_fee_effective_date, Q.MONTHS)), 'DD')
                      END;
                IF l_last_payment is not null AND l_last_payment > l_next_payment THEN
                    IF l_fee_override <> 'Y' THEN --BIG PROBLEM
                        RAISE_APPLICATION_ERROR(-20011, 'This service fee was charged to the customer after the specified effective date.');
                    END IF;
                END IF;
                IF l_last_payment is not null AND l_last_payment > l_end_date THEN
                    IF l_fee_override <> 'Y' THEN --BIG PROBLEM
                        RAISE_APPLICATION_ERROR(-20011, 'This service fee was charged to the customer after the specified end date.');
                    END IF;
                END IF;
            END IF;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
            	 NULL;
            WHEN OTHERS THEN
            	 RAISE;
        END;
        IF lc_initiation_type_cd = 'P' THEN
            --delete any old net revenue fees >= l_fee_effective_date
            DELETE FROM LEDGER L
             WHERE L.ENTRY_TYPE = 'SF'
               AND L.SERVICE_FEE_ID IN(
                 SELECT SF.SERVICE_FEE_ID
                   FROM SERVICE_FEES SF
                  WHERE SF.TERMINAL_ID = l_terminal_id
                    AND SF.FEE_ID = l_fee_id
                    AND SF.FREQUENCY_ID = ln_use_freq_id
                    --AND NVL(SF.END_DATE, l_fee_effective_date) >= l_fee_effective_date
                    )
               AND L.ENTRY_DATE >= l_fee_effective_date
               AND NOT EXISTS(
                    SELECT 1
                    FROM BATCH B, DOC D
                    WHERE B.BATCH_ID = L.BATCH_ID
                    AND B.DOC_ID = D.DOC_ID
                    AND D.STATUS NOT IN('O', 'D'));
        END IF;
        
        -- is the max(...) on these is necessary?
        SELECT MAX(START_DATE), MAX(END_DATE), MAX(FEE_AMOUNT), MAX(FEE_PERCENT), MAX(SERVICE_FEE_ID), MAX(INACTIVE_FEE_AMOUNT), MAX(COMMISSION_AMOUNT), MAX(CUSTOMER_BANK_ID)
        INTO l_orig_start_date, l_orig_end_date, l_orig_amount, l_orig_perc, l_sf_id, l_orig_inactive_amount, l_orig_commission_amt, l_orig_commission_bank_id
        FROM (
          SELECT SF.START_DATE, SF.END_DATE, SF.FEE_AMOUNT, SF.FEE_PERCENT, SF.SERVICE_FEE_ID, SF.INACTIVE_FEE_AMOUNT, SFC.COMMISSION_AMOUNT, SFC.CUSTOMER_BANK_ID
          FROM CORP.SERVICE_FEES SF
          LEFT OUTER JOIN CORP.SERVICE_FEE_COMMISSION SFC 
            ON SFC.SERVICE_FEE_ID = SF.SERVICE_FEE_ID
          WHERE SF.TERMINAL_ID = l_terminal_id
            AND SF.FEE_ID = l_fee_id
            AND SF.FREQUENCY_ID = ln_use_freq_id
          ORDER BY NVL(END_DATE, MAX_DATE) DESC
        ) WHERE ROWNUM = 1;
        
        IF l_sf_id IS NOT NULL 
          AND ((lc_initiation_type_cd = 'P' AND NVL(l_orig_perc, 0) = NVL(l_fee_perc, 0)) OR (lc_initiation_type_cd = 'R' AND NVL(l_orig_amount, 0) = NVL(l_fee_amt, 0) AND NVL(l_orig_inactive_amount, 0) = NVL(pn_inactive_fee_amt, 0)) OR (lc_initiation_type_cd NOT IN('P', 'R') AND NVL(l_orig_amount, 0) = NVL(l_fee_amt, 0) AND NVL(l_orig_commission_amt, 0) = NVL(l_commission_amt, 0) AND NVL(l_orig_commission_bank_id, 0) = NVL(l_commission_bank_id, 0))) 
          AND l_fee_effective_date <= NVL(l_orig_start_date, MIN_DATE) AND NVL(l_end_date, MAX_DATE) >= NVL(l_orig_start_date, MIN_DATE) THEN
            -- first update other records that may be affected
            UPDATE SERVICE_FEES 
               SET END_DATE = l_fee_effective_date
             WHERE TERMINAL_ID = l_terminal_id
               AND FEE_ID = l_fee_id
               AND FREQUENCY_ID = ln_use_freq_id
               AND NVL(END_DATE, l_fee_effective_date) >= l_fee_effective_date
               AND SERVICE_FEE_ID != l_sf_id;

            -- update existing record
            UPDATE SERVICE_FEES 
               SET END_DATE = l_end_date,
                   START_DATE = l_fee_effective_date,
                   GRACE_PERIOD_DATE = CASE WHEN lc_initiation_type_cd IN('G', 'R') THEN l_fee_effective_date + pn_grace_days END,
                   NO_TRIGGER_EVENT_FLAG = pc_no_trigger_event_flag,
                   INACTIVE_MONTHS_REMAINING = CASE WHEN lc_initiation_type_cd = 'R' AND pn_inactive_months IS NOT NULL THEN pn_inactive_months ELSE INACTIVE_MONTHS_REMAINING END
             WHERE SERVICE_FEE_ID = l_sf_id;           
        ELSE
            l_sf_id := NULL;
            SELECT MAX(ORIGINAL_LICENSE_ID) INTO l_original_license_id FROM SERVICE_FEES WHERE TERMINAL_ID = l_terminal_id
                   AND FEE_ID = l_fee_id AND FREQUENCY_ID = ln_use_freq_id AND NVL(END_DATE, l_fee_effective_date) >= l_fee_effective_date;
            UPDATE SERVICE_FEES SET END_DATE = l_fee_effective_date
             WHERE TERMINAL_ID = l_terminal_id
               AND FEE_ID = l_fee_id
               AND FREQUENCY_ID = ln_use_freq_id
               AND NVL(END_DATE, l_fee_effective_date) >= l_fee_effective_date;
            IF NVL(l_fee_amt, 0) <> 0 OR NVL(l_fee_perc, 0) <> 0 OR (lc_initiation_type_cd = 'R' AND NVL(pn_inactive_fee_amt, 0) <> 0) THEN
                SELECT SERVICE_FEE_SEQ.NEXTVAL INTO l_sf_id FROM DUAL;
                INSERT INTO SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID,
                    FREQUENCY_ID, FEE_AMOUNT, FEE_PERCENT, START_DATE,
                    END_DATE, LAST_PAYMENT, GRACE_PERIOD_DATE, NO_TRIGGER_EVENT_FLAG, INACTIVE_FEE_AMOUNT, INACTIVE_MONTHS_REMAINING, ORIGINAL_LICENSE_ID)
                    SELECT l_sf_id, l_terminal_id, l_fee_id, ln_use_freq_id, l_fee_amt,
                           l_fee_perc, l_fee_effective_date, l_end_date,
                           l_last_payment, CASE WHEN lc_initiation_type_cd IN('G', 'R') THEN l_fee_effective_date + pn_grace_days END,
                           pc_no_trigger_event_flag,
                           CASE WHEN lc_initiation_type_cd IN('R') THEN pn_inactive_fee_amt END,
                           CASE WHEN lc_initiation_type_cd != 'R' THEN NULL WHEN pn_inactive_months IS NULL THEN 3 ELSE pn_inactive_months END,
                           l_original_license_id
                      FROM FREQUENCY F
                     WHERE F.FREQUENCY_ID = ln_use_freq_id;
                IF NVL(l_commission_amt, 0) != 0 THEN
                  INSERT INTO service_fee_commission(service_fee_id, customer_bank_id, commission_amount, sell_amount, buy_amount)
                  VALUES (l_sf_id, l_commission_bank_id, nvl(l_commission_amt, 0), nvl(l_fee_amt,0), nvl(l_fee_amt,0) - nvl(l_commission_amt,0));
                END IF;
            END IF;	
        END IF;
		IF l_sf_id IS NOT NULL THEN
            IF lc_initiation_type_cd = 'P' THEN
                --add any new net revenue fees
                INSERT INTO LEDGER(
                    LEDGER_ID,
                    RELATED_LEDGER_ID,
                    ENTRY_TYPE,
                    TRANS_ID,
                    PROCESS_FEE_ID,
                    SERVICE_FEE_ID,
                    AMOUNT,
                    ENTRY_DATE,
                    BATCH_ID,
                    SETTLE_STATE_ID,
                    LEDGER_DATE,
                    DESCRIPTION)
                SELECT
                    LEDGER_SEQ.NEXTVAL,
                    L.LEDGER_ID,
                    'SF',
                    L.TRANS_ID,
                    L.PROCESS_FEE_ID,
                    l_sf_id,
                    -L.AMOUNT * l_fee_perc,
                    L.ENTRY_DATE,
                    L.BATCH_ID,
                    L.SETTLE_STATE_ID,
                    L.LEDGER_DATE,
                    F.FEE_NAME
                FROM LEDGER L, BATCH B, FEES F, DOC D
               WHERE F.FEE_ID = l_fee_id
                 AND L.BATCH_ID = B.BATCH_ID
                 AND B.TERMINAL_ID = l_terminal_id
                 AND L.ENTRY_DATE >= l_fee_effective_date
                 AND L.RELATED_LEDGER_ID IS NULL -- Avoid Percent of Percent Fee
                 AND L.DELETED = 'N'
                 AND B.DOC_ID = D.DOC_ID
                 AND D.STATUS IN('O')
                 AND L.ENTRY_TYPE IN('CC', 'PF', 'SF', 'AD', 'CB', 'RF');
            ELSIF l_fee_override = 'Y' AND l_last_payment IS NOT NULL THEN -- let's insert any missing (No need to do this for net rev fees)
                DECLARE
                    CURSOR l_cur IS
                        SELECT LEDGER_SEQ.NEXTVAL LEDGER_ID,
                               A.FEE_DATE,
                               CBT.CUSTOMER_BANK_ID,
                               T.FEE_CURRENCY_ID,
                               F.FEE_NAME || ' for ' || TO_CHAR(A.FEE_DATE, A.FMT) FEE_DESC,
                               'Y' AS_ACCUM
                    FROM (SELECT DECODE(Q.DAYS, 0, LAST_DAY(D.COLUMN_VALUE), D.COLUMN_VALUE + Q.DAYS - 1) FEE_DATE,
                                 GET_SERVICE_FEE_DATE_FMT(Q.MONTHS, Q.DAYS) FMT
                            FROM TABLE(CAST(CORP.GLOBALS_PKG.GET_DATE_LIST(GREATEST(l_fee_effective_date, l_first_payment),
                                 LEAST(NVL(l_last_payment, (SELECT  
                                   CASE WHEN Q.MONTHS = 0 THEN l_current_date - Q.DAYS
                                        ELSE TRUNC(LAST_DAY(ADD_MONTHS(l_current_date, -Q.MONTHS)), 'DD')
                                   END FROM CORP.FREQUENCY Q WHERE Q.FREQUENCY_ID = ln_use_freq_id)), NVL(l_end_date, MAX_DATE)),
                                 ln_use_freq_id, 0, 0) AS REPORT.DATE_LIST)) D,
                                 FREQUENCY Q
                            WHERE Q.FREQUENCY_ID = ln_use_freq_id
                              AND (Q.MONTHS > 0 OR Q.DAYS > 0)) A,
                          CUSTOMER_BANK_TERMINAL CBT,
                          FEES F,
                          REPORT.TERMINAL T
                    WHERE NOT EXISTS(SELECT 1
                        FROM CORP.LEDGER L, CORP.BATCH B
                        WHERE L.BATCH_ID = B.BATCH_ID
                          AND B.TERMINAL_ID = l_terminal_id
                          AND L.ENTRY_TYPE = 'SF'
                          AND L.DELETED = 'N'
                          AND L.AMOUNT <> 0
                          AND L.ENTRY_DATE = A.FEE_DATE)
                      AND WITHIN1(A.FEE_DATE, CBT.START_DATE, CBT.END_DATE) = 1
                      AND CBT.TERMINAL_ID = l_terminal_id
                      AND F.FEE_ID = l_fee_id
                      AND T.TERMINAL_ID = l_terminal_id;
                BEGIN
                    FOR l_rec IN l_cur LOOP
                        l_batch_id := GET_OR_CREATE_BATCH(
                                    l_terminal_id,
                                    l_rec.CUSTOMER_BANK_ID,
                                    l_rec.FEE_DATE,
                                    l_rec.FEE_CURRENCY_ID,
                                    l_rec.AS_ACCUM);

                        INSERT INTO LEDGER(
                                LEDGER_ID,
                                ENTRY_TYPE,
                                SERVICE_FEE_ID,
                                AMOUNT,
                                ENTRY_DATE,
                                BATCH_ID,
                                SETTLE_STATE_ID,
                                LEDGER_DATE,
                                DESCRIPTION)
                            VALUES(
                                l_rec.LEDGER_ID,
                                'SF',
                                l_sf_id,
                                -ABS(l_fee_amt),
                                l_rec.FEE_DATE,
                                l_batch_id,
                                2,
                                l_rec.FEE_DATE,
                                l_rec.FEE_DESC);
                        SERVICE_FEE_COMMISSION_INS(l_rec.LEDGER_ID, null, l_sf_id, l_fee_id, 2, -ABS(l_fee_amt), l_rec.FEE_DATE, l_rec.FEE_DATE);
                        -- create net revenue fee on service fee, if any
                        INSERT INTO LEDGER(
                            LEDGER_ID,
                            RELATED_LEDGER_ID,
                            ENTRY_TYPE,
                            SERVICE_FEE_ID,
                            AMOUNT,
                            ENTRY_DATE,
                            BATCH_ID,
                            SETTLE_STATE_ID,
                            LEDGER_DATE,
                            DESCRIPTION)
                        SELECT
                            LEDGER_SEQ.NEXTVAL,
                            l_rec.LEDGER_ID,
                            'SF',
                            SF.SERVICE_FEE_ID,
                            ABS(l_fee_amt) * SF.FEE_PERCENT,
                            l_rec.FEE_DATE,
                            l_batch_id,
                            2,
                            l_rec.FEE_DATE,
                            F.FEE_NAME
                        FROM SERVICE_FEES SF, FEES F
                       WHERE SF.FEE_ID = F.FEE_ID
                         AND SF.TERMINAL_ID = l_terminal_id
                         AND l_rec.FEE_DATE BETWEEN NVL(SF.START_DATE, MIN_DATE)
                              AND NVL(SF.END_DATE, MAX_DATE)
                         AND SF.FREQUENCY_ID = 6;
                        UPDATE CORP.SERVICE_FEES
                         SET LAST_PAYMENT = CASE WHEN LAST_PAYMENT IS NULL OR l_rec.FEE_DATE > LAST_PAYMENT THEN l_rec.FEE_DATE ELSE LAST_PAYMENT END,
                             FIRST_PAYMENT = CASE WHEN FIRST_PAYMENT IS NULL OR l_rec.FEE_DATE < FIRST_PAYMENT THEN l_rec.FEE_DATE ELSE FIRST_PAYMENT END
                       WHERE SERVICE_FEE_ID =  l_sf_id;
                    END LOOP;
                END;
            END IF;
        END IF;
    END;
    
    PROCEDURE UNLOCK_DOC (
      l_doc_id IN DOC.DOC_ID%TYPE)
    IS
        l_customer_bank_id CORP.DOC.CUSTOMER_BANK_ID%TYPE;
        l_lock VARCHAR2(128);
    BEGIN
    	IF FREEZE_DOC_STATUS_DOC_ID(l_doc_id) <> 'L' THEN
           RAISE_APPLICATION_ERROR(-20400, 'This document has already been approved; you can not unlock it.');
   	    END IF;
    	--Remove rounding entries
    	UPDATE LEDGER
           SET DELETED = 'Y'
         WHERE ENTRY_TYPE = 'SB'
           AND TRANS_ID IS NULL
           AND BATCH_ID IN(SELECT BATCH_ID FROM BATCH WHERE DOC_ID = l_doc_id);
           
        -- Blank out the end date of any as accum batches
        UPDATE BATCH B
           SET B.BATCH_STATE_CD = 'L',
               B.BATCH_CLOSED_TS = NULL,
               B.END_DATE = NULL
         WHERE B.DOC_ID = l_doc_id
           AND B.BATCH_STATE_CD = 'F'
           AND B.PAYMENT_SCHEDULE_ID IN(1,5);

        -- Change time-period batches to Open
        UPDATE BATCH B
           SET B.BATCH_STATE_CD = 'O',
               B.BATCH_CLOSED_TS = NULL
         WHERE B.DOC_ID = l_doc_id
           AND B.BATCH_STATE_CD IN('U', 'D')
           AND B.PAYMENT_SCHEDULE_ID NOT IN(1,2,5,7,8);

        -- first condense any docs that may match once they are re-opened
    	DECLARE
    	   l_new_doc_id DOC.DOC_ID%TYPE;
        BEGIN
    	    SELECT MAX(DNEW.DOC_ID)
              INTO l_new_doc_id
              FROM DOC DOLD, DOC DNEW
             WHERE DOLD.DOC_ID = l_doc_id
               AND DNEW.DOC_ID <> l_doc_id
               AND DOLD.CUSTOMER_BANK_ID = DNEW.CUSTOMER_BANK_ID
               AND NVL(DOLD.CURRENCY_ID, 0) = NVL(DNEW.CURRENCY_ID, 0)
               AND DNEW.STATUS = 'O';
            IF l_new_doc_id IS NOT NULL THEN
            	-- Update batches and delete old doc
            	UPDATE BATCH SET DOC_ID = l_new_doc_id WHERE DOC_ID = l_doc_id;
            	DELETE FROM DOC WHERE DOC_ID = l_doc_id;
            	
            	--now condense any batches that may match once they are moved
            	DECLARE
            	   CURSOR l_cur IS
                    	SELECT BNEW.BATCH_ID NEW_BATCH_ID,
                               BOLD.BATCH_ID OLD_BATCH_ID,
                               BOLD.END_DATE OLD_END_DATE,
                               BOLD.START_DATE OLD_START_DATE
                          FROM BATCH BOLD, BATCH BNEW
                         WHERE BOLD.DOC_ID = l_new_doc_id
                           AND BNEW.DOC_ID = l_new_doc_id
                           AND BOLD.PAYMENT_SCHEDULE_ID = BNEW.PAYMENT_SCHEDULE_ID
                           AND NVL(BOLD.TERMINAL_ID, 0) = NVL(BNEW.TERMINAL_ID, 0)
                           AND ((BOLD.BATCH_STATE_CD != 'C' AND BNEW.BATCH_STATE_CD = 'C')
                            OR ((BOLD.BATCH_STATE_CD != 'C' OR BNEW.BATCH_STATE_CD = 'C') AND BOLD.BATCH_ID > BNEW.BATCH_ID))
                           AND (BOLD.PAYMENT_SCHEDULE_ID IN(1,5)
                             OR BOLD.START_DATE = BNEW.START_DATE);
            	BEGIN
            	   FOR l_rec IN l_cur LOOP
            	       UPDATE LEDGER
            	          SET BATCH_ID = l_rec.NEW_BATCH_ID
                        WHERE BATCH_ID = l_rec.OLD_BATCH_ID;
                    LOOP
                      UPDATE CORP.BATCH_TOTAL
                      SET (LEDGER_AMOUNT, LEDGER_COUNT)= (select sum(LEDGER_AMOUNT) LEDGER_AMOUNT, sum(LEDGER_COUNT) LEDGER_COUNT
                      from (
                      select LEDGER_AMOUNT, LEDGER_COUNT
                      from corp.batch_total where batch_id= l_rec.OLD_BATCH_ID and ENTRY_TYPE='CA'
                      union
                      select LEDGER_AMOUNT, LEDGER_COUNT
                      from corp.batch_total where batch_id= l_rec.NEW_BATCH_ID and ENTRY_TYPE='CA'))
                      where batch_id=l_rec.NEW_BATCH_ID
                      and entry_type ='CA';
                      EXIT WHEN SQL%ROWCOUNT > 0;
                      BEGIN
                          INSERT INTO CORP.BATCH_TOTAL(BATCH_ID, ENTRY_TYPE, PAYABLE, LEDGER_AMOUNT, LEDGER_COUNT)                        
                          select l_rec.NEW_BATCH_ID,'CA','N', sum(LEDGER_AMOUNT) LEDGER_AMOUNT, sum(LEDGER_COUNT) LEDGER_COUNT
	                      from (
	                      select LEDGER_AMOUNT, LEDGER_COUNT
	                      from corp.batch_total where batch_id= l_rec.OLD_BATCH_ID and ENTRY_TYPE='CA'
	                      union
	                      select LEDGER_AMOUNT, LEDGER_COUNT
	                      from corp.batch_total where batch_id= l_rec.NEW_BATCH_ID and ENTRY_TYPE='CA')
                        GROUP BY l_rec.NEW_BATCH_ID,'CA','N';
                          EXIT;    
                      EXCEPTION
                          WHEN DUP_VAL_ON_INDEX THEN
                              NULL;
                      END;
                    END LOOP;  
                    LOOP
                     UPDATE CORP.BATCH_TOTAL
                      SET (LEDGER_AMOUNT, LEDGER_COUNT)= (select sum(LEDGER_AMOUNT) LEDGER_AMOUNT, sum(LEDGER_COUNT) LEDGER_COUNT
                      from (
                      select LEDGER_AMOUNT, LEDGER_COUNT
                      from corp.batch_total where batch_id= l_rec.OLD_BATCH_ID and ENTRY_TYPE='TT'
                      union
                      select LEDGER_AMOUNT, LEDGER_COUNT
                      from corp.batch_total where batch_id= l_rec.NEW_BATCH_ID and ENTRY_TYPE='TT'))
                      where batch_id=l_rec.NEW_BATCH_ID
                      and entry_type ='TT';
                      EXIT WHEN SQL%ROWCOUNT > 0;
                      BEGIN
                          INSERT INTO CORP.BATCH_TOTAL(BATCH_ID, ENTRY_TYPE, PAYABLE, LEDGER_AMOUNT, LEDGER_COUNT)                        
                          select l_rec.NEW_BATCH_ID,'TT','N', sum(LEDGER_AMOUNT) LEDGER_AMOUNT, sum(LEDGER_COUNT) LEDGER_COUNT
	                      from (
	                      select LEDGER_AMOUNT, LEDGER_COUNT
	                      from corp.batch_total where batch_id= l_rec.OLD_BATCH_ID and ENTRY_TYPE='TT'
	                      union
	                      select LEDGER_AMOUNT, LEDGER_COUNT
	                      from corp.batch_total where batch_id= l_rec.NEW_BATCH_ID and ENTRY_TYPE='TT')
                        GROUP BY l_rec.NEW_BATCH_ID,'TT','N';
                          EXIT;    
                      EXCEPTION
                          WHEN DUP_VAL_ON_INDEX THEN
                              NULL;
                      END;
                    END LOOP;  
                    UPDATE REPORT.ACTIVITY_REF SET PAYMENT_BATCH_ID=l_rec.NEW_BATCH_ID
                    WHERE PAYMENT_BATCH_ID=l_rec.OLD_BATCH_ID;
                       DELETE FROM BATCH_FILL WHERE BATCH_ID = l_rec.OLD_BATCH_ID;
                       DELETE FROM BATCH WHERE BATCH_ID = l_rec.OLD_BATCH_ID;
                       DELETE FROM BATCH_TOTAL WHERE BATCH_ID = l_rec.OLD_BATCH_ID;
                       
                       UPDATE BATCH
                          SET START_DATE = LEAST(START_DATE, l_rec.OLD_START_DATE),
                              END_DATE = GREATEST(END_DATE, l_rec.OLD_END_DATE)
                        WHERE BATCH_ID = l_rec.NEW_BATCH_ID;
            	   END LOOP;
         	   END;
            ELSE
                -- Update doc
            	UPDATE DOC SET STATUS = 'O' WHERE DOC_ID = l_doc_id;
            END IF;
    	END;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
    	   RAISE_APPLICATION_ERROR(-20401, 'This document does not exist.');
        WHEN OTHERS THEN
    	   RAISE;
    END;
    
    FUNCTION GET_TRANS_ADJ_DESC(
        l_trans_id LEDGER.TRANS_ID%TYPE,
        l_entry_type LEDGER.ENTRY_TYPE%TYPE,
        l_entry_date LEDGER.ENTRY_DATE%TYPE,
        l_terminal_id BATCH.TERMINAL_ID%TYPE,
        l_customer_bank_id DOC.CUSTOMER_BANK_ID%TYPE)
     RETURN LEDGER.DESCRIPTION%TYPE
    IS
        l_old_payable VARCHAR(4000); -- oracle raises an exception if you use anything with smaller length
        l_old_terminal_id BATCH.TERMINAL_ID%TYPE;
        l_old_customer_bank_id DOC.CUSTOMER_BANK_ID%TYPE;
        l_desc LEDGER.DESCRIPTION%TYPE;
    BEGIN
        -- figure out why this was added
        --possible reasons:
        --  1. previously denied
        --  2. changed location
        --  3. changed customer bank
        
        SELECT *
          INTO l_old_payable, l_old_terminal_id, l_old_customer_bank_id
          FROM (
            SELECT PAYMENTS_PKG.ENTRY_PAYABLE(L.SETTLE_STATE_ID, L.ENTRY_TYPE),
                   B.TERMINAL_ID, D.CUSTOMER_BANK_ID
              FROM LEDGER L, BATCH B, DOC D
             WHERE L.BATCH_ID = B.BATCH_ID
               AND L.DELETED = 'N'
               AND B.DOC_ID = D.DOC_ID
               AND D.STATUS NOT IN('O', 'D')
               AND L.TRANS_ID = l_trans_id
               AND L.ENTRY_TYPE = 'CC'
               AND B.PAYMENT_SCHEDULE_ID <> 5
             ORDER BY L.CREATE_DATE DESC) A
         WHERE ROWNUM = 1;
        IF l_old_payable = 'N' THEN
            IF l_entry_type = 'CC' THEN
                RETURN 'Payment of previously declined transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSIF l_entry_type = 'PF' THEN
                RETURN 'Process Fee for previously declined transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSE -- this should never occur
                RETURN 'Payment related to previously declined transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            END IF;
        ELSIF l_terminal_id <> l_old_terminal_id THEN
            IF l_entry_type = 'CC' THEN
                RETURN 'Payment of transaction previously assigned to a different location (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSIF l_entry_type = 'PF' THEN
                RETURN 'Process Fee for transaction previously assigned to a different location (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSE -- this should never occur
                RETURN 'Payment related to transaction previously assigned to a different location (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            END IF;
        ELSIF l_customer_bank_id <> l_old_customer_bank_id THEN
            IF l_entry_type = 'CC' THEN
                RETURN 'Payment of transaction previously assigned to a different bank account (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSIF l_entry_type = 'PF' THEN
                RETURN 'Process Fee for transaction previously assigned to a different bank account (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSE -- this should never occur
                RETURN 'Payment related to transaction previously assigned to a different bank account (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            END IF;
        ELSE
            IF l_entry_type = 'CC' THEN
                RETURN 'Payment of previously unpaid transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSIF l_entry_type = 'PF' THEN
                RETURN 'Process Fee for previously unpaid transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSE -- this should never occur
                RETURN 'Payment related to previously unpaid transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            END IF;
        END IF;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            IF l_entry_type = 'CC' THEN
                RETURN 'Payment of previously unpaid transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSIF l_entry_type = 'PF' THEN
                RETURN 'Process Fee for previously unpaid transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            ELSE -- this should never occur
                RETURN 'Payment related to previously unpaid transaction (Tran Date: ' || TO_CHAR(l_entry_date, 'MM/DD/YYYY HH:MI PM') ||')';
            END IF;
    END;
    
    PROCEDURE UPDATE_LEDGER(
              l_tran_id LEDGER.TRANS_ID%TYPE)
    IS
    	CURSOR l_cur IS
    		SELECT TRANS_TYPE_ID, CLOSE_DATE, SETTLE_DATE, TOTAL_AMOUNT, SETTLE_STATE_ID,
                   TERMINAL_ID, CUSTOMER_BANK_ID, PROCESS_FEE_ID, CURRENCY_ID
    		  FROM REPORT.TRANS
    		 WHERE TRAN_ID = l_tran_id;
    BEGIN
         FOR l_rec IN l_cur LOOP
    	     UPDATE_LEDGER(l_tran_id,l_rec.trans_type_id,l_rec.close_date,l_rec.settle_date,
                           l_rec.total_amount,l_rec.settle_state_id,l_rec.terminal_id,
                           l_rec.customer_bank_id,l_rec.process_fee_id,l_rec.currency_id);
   	     END LOOP;
    END;
    
    PROCEDURE ADD_ACTIVATION_FEE(
        l_terminal_id CORP.BATCH.TERMINAL_ID%TYPE,
        l_fee_amt CORP.LEDGER.AMOUNT%TYPE,
        l_activation_date  CORP.LEDGER.ENTRY_DATE%TYPE)
    IS
        l_batch_id CORP.BATCH.BATCH_ID%TYPE;
        l_service_fee_id CORP.LEDGER.SERVICE_FEE_ID%TYPE;
        l_desc CORP.LEDGER.DESCRIPTION%TYPE;
        l_ledger_id CORP.LEDGER.LEDGER_ID%TYPE;
        l_cust_bank_id CORP.DOC.CUSTOMER_BANK_ID%TYPE;
        l_fee_currency_id CORP.DOC.CURRENCY_ID%TYPE;
		l_lock VARCHAR2(128);
    BEGIN
        SELECT cbt.CUSTOMER_BANK_ID, t.FEE_CURRENCY_ID,
               'Activation Fee for Terminal ' || t.TERMINAL_NBR
          INTO l_cust_bank_id, l_fee_currency_id, l_desc
          FROM REPORT.TERMINAL t, CORP.CUSTOMER_BANK_TERMINAL cbt
         WHERE t.TERMINAL_ID = l_terminal_id
           AND t.TERMINAL_ID = cbt.TERMINAL_ID
           AND l_activation_date >= NVL(cbt.START_DATE, MIN_DATE)
           AND l_activation_date < NVL(cbt.END_DATE, MAX_DATE);
		   
		l_lock := GLOBALS_PKG.REQUEST_LOCK('TERMINAL.TERMINAL_ID', l_terminal_id);
		   
        l_batch_id := GET_OR_CREATE_BATCH(l_terminal_id, l_cust_bank_id, l_activation_date, l_fee_currency_id, 'N');
        SELECT SERVICE_FEE_SEQ.NEXTVAL
          INTO l_service_fee_id
          FROM DUAL;
        INSERT INTO SERVICE_FEES(
            SERVICE_FEE_ID,
            TERMINAL_ID,
            FEE_ID,
            FEE_AMOUNT,
            FREQUENCY_ID,
            LAST_PAYMENT,
            START_DATE)
       	  VALUES(
       	    l_service_fee_id,
       	    l_terminal_id,
       	    10,
       	    l_fee_amt,
       	    7,
       	    l_activation_date,
       	    l_activation_date);
       	    
        SELECT LEDGER_SEQ.NEXTVAL
          INTO l_ledger_id
          FROM DUAL;
        INSERT INTO LEDGER(
            LEDGER_ID,
            ENTRY_TYPE,
            SERVICE_FEE_ID,
            AMOUNT,
            ENTRY_DATE,
            BATCH_ID,
            SETTLE_STATE_ID,
            LEDGER_DATE,
            DESCRIPTION)
        VALUES (
            l_ledger_id,
            'SF',
            l_service_fee_id,
            -l_fee_amt,
            l_activation_date,
            l_batch_id,
            2,
            l_activation_date,
            l_desc);
        SERVICE_FEE_COMMISSION_INS(l_ledger_id, null, l_service_fee_id, 10, 2, -l_fee_amt, l_activation_date, l_activation_date);
    END;
    
    PROCEDURE SWITCH_PAYMENT_SCHEDULE(
        l_tran_id CORP.LEDGER.TRANS_ID%TYPE,
        l_payment_schedule_id  CORP.BATCH.PAYMENT_SCHEDULE_ID%TYPE)
    IS
        l_lock VARCHAR(128);
        l_adj_amt CORP.LEDGER.AMOUNT%TYPE;
        l_batch_id CORP.LEDGER.BATCH_ID%TYPE;
       	CURSOR l_cur IS
    		SELECT X.TRANS_TYPE_ID, X.CLOSE_DATE, X.SETTLE_DATE, X.TOTAL_AMOUNT, X.SETTLE_STATE_ID,
                   X.TERMINAL_ID, X.CUSTOMER_BANK_ID, X.PROCESS_FEE_ID, X.CURRENCY_ID, T.BUSINESS_UNIT_ID
    		  FROM REPORT.TRANS X
  		     INNER JOIN REPORT.TERMINAL T ON X.TERMINAL_ID = T.TERMINAL_ID
    		 WHERE TRAN_ID = l_tran_id;
    BEGIN
        FOR l_rec IN l_cur LOOP
            IF NVL(l_rec.terminal_id, 0) <> 0 AND NVL(l_rec.customer_bank_id, 0) <> 0 THEN
                l_lock := GLOBALS_PKG.REQUEST_LOCK('LEDGER.TRANS_ID',l_tran_id);
                -- Remove any open entries in different pay sched
                UPDATE CORP.LEDGER L SET DELETED = 'Y'
                 WHERE L.TRANS_ID = l_tran_id
                   AND L.DELETED = 'N'
                   AND EXISTS(
                      SELECT 1
                        FROM CORP.BATCH B
                       INNER JOIN CORP.DOC D ON B.DOC_ID = D.DOC_ID
                       WHERE B.PAYMENT_SCHEDULE_ID <> l_payment_schedule_id
                         AND B.BATCH_STATE_CD IN('O', 'L')
                         AND D.STATUS IN('O')
                         AND L.BATCH_ID = B.BATCH_ID);

                -- Make adjustment for old
                SELECT SUM(L.AMOUNT)
                  INTO l_adj_amt
                  FROM CORP.LEDGER L
                 INNER JOIN CORP.BATCH B ON L.BATCH_ID = B.BATCH_ID
                 INNER JOIN CORP.DOC D ON B.DOC_ID = D.DOC_ID
                 WHERE L.TRANS_ID = l_tran_id
                   AND L.DELETED = 'N'
                   AND D.STATUS NOT IN('D')
                   AND B.PAYMENT_SCHEDULE_ID <> l_payment_schedule_id
                   AND ENTRY_PAYABLE(l.settle_state_id,l.entry_type) = 'Y';

                IF NVL(l_adj_amt, 0) <> 0 THEN
                    l_batch_id := GET_OR_CREATE_BATCH(l_rec.terminal_id,
                                    l_rec.customer_bank_id, l_rec.close_date, l_rec.currency_id, 'A');
                    INSERT INTO LEDGER(
                        LEDGER_ID,
                        ENTRY_TYPE,
                        TRANS_ID,
                        PROCESS_FEE_ID,
                        AMOUNT,
                        ENTRY_DATE,
                        BATCH_ID,
                        SETTLE_STATE_ID,
                        LEDGER_DATE,
                        DESCRIPTION)
                    SELECT
                        LEDGER_SEQ.NEXTVAL,
                        'AD',
                        l_tran_id,
                        NULL,
                        -l_adj_amt,
                        l_rec.close_date,
                        l_batch_id,
                        l_rec.SETTLE_STATE_ID,
                        NVL(l_rec.SETTLE_DATE, SYSDATE),
                        'Payment schedule switched to ' || DESCRIPTION
                      FROM CORP.PAYMENT_SCHEDULE
                     WHERE PAYMENT_SCHEDULE_ID = l_payment_schedule_id;
                END IF;
                -- insert as new
                l_batch_id := GET_OR_CREATE_BATCH(l_rec.terminal_id, l_rec.customer_bank_id, l_rec.close_date, l_rec.currency_id, l_payment_schedule_id, l_rec.BUSINESS_UNIT_ID, 'N');
                INSERT_TRANS_TO_LEDGER(l_tran_id, l_rec.trans_type_id, l_rec.close_date, l_rec.settle_date,
                     l_rec.total_amount, l_rec.settle_state_id, l_rec.process_fee_id, l_batch_id);
            END IF;
        END LOOP;
    END;
	
	PROCEDURE CHECK_EFT_PROCESS_COMPLETION
	IS
		LD_EFT_PROPAGATION_START_TS DATE := TO_DATE(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EFT_PROPAGATION_LAST_RUN_START_TS'), 'MM/DD/YYYY HH24:MI:SS');
		LD_EFT_PROPAGATION_COMPLETE_TS DATE := TO_DATE(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EFT_PROPAGATION_LAST_RUN_COMPLETE_TS'), 'MM/DD/YYYY HH24:MI:SS');
		LV_EMAIL_FROM ENGINE.APP_SETTING.APP_SETTING_VALUE%TYPE := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EMAIL_FROM_ADDRESS');
		LV_EMAIL_TO ENGINE.APP_SETTING.APP_SETTING_VALUE%TYPE := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EFT_PROCESSING_EMAIL_TO');
		LV_EMAIL_CONTENT ENGINE.OB_EMAIL_QUEUE.OB_EMAIL_CONTENT%TYPE := 'Automatic EFT Processing is complete. Number of processed EFT(s) by currency:' || CHR(13) || CHR(10);
		LC_SEND_EMAIL CHAR(1) := 'N';
		LN_COUNT INTEGER;
	
		CURSOR L_CUR IS
			SELECT C.CURRENCY_NAME, COUNT(1) EFT_COUNT
			FROM CORP.DOC D
			JOIN CORP.CURRENCY C ON D.CURRENCY_ID = C.CURRENCY_ID
			WHERE D.AUTO_PROCESS_START_TS >= LD_EFT_PROPAGATION_START_TS
				AND D.STATUS != 'D'
			GROUP BY C.CURRENCY_NAME
			ORDER BY COUNT(1) DESC;
	BEGIN
		IF LD_EFT_PROPAGATION_COMPLETE_TS < TO_DATE(TO_CHAR(CURRENT_TIMESTAMP, 'MM/DD/YYYY'), 'MM/DD/YYYY') THEN
			RETURN;
		END IF;
	
		SELECT COUNT(1)
		INTO LN_COUNT
		FROM CORP.DOC
		WHERE AUTO_PROCESS_START_TS >= LD_EFT_PROPAGATION_START_TS
			AND AUTO_PROCESS_END_TS IS NULL;
			
		IF LN_COUNT > 0 THEN
			RETURN;
		END IF;
	
		FOR L_REC IN L_CUR LOOP
			IF LC_SEND_EMAIL = 'N' THEN
				LC_SEND_EMAIL := 'Y';
			END IF;
		
			LV_EMAIL_CONTENT := LV_EMAIL_CONTENT || CHR(13) || CHR(10) || L_REC.CURRENCY_NAME || ': ' || L_REC.EFT_COUNT;
		END LOOP;
		
		IF LC_SEND_EMAIL = 'Y' THEN
			LV_EMAIL_CONTENT := LV_EMAIL_CONTENT || CHR(13) || CHR(10) || CHR(13) || CHR(10) 
				|| DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DMS_URL')
				|| 'processedEFT.i?eft_from_date=' || TO_CHAR(CURRENT_TIMESTAMP, 'MM/DD/YYYY') || '&' || 'eft_from_time=00:00:00&' || 'eft_to_date='
				|| TO_CHAR(CURRENT_TIMESTAMP, 'MM/DD/YYYY') || '&' || 'eft_to_time=' || TO_CHAR(CURRENT_TIMESTAMP, 'HH24:MI:SS')
				|| '&' || 'auto_processed=Y&' || 'data_format=HTML&' || 'action=List+EFTs';
		
			INSERT INTO ENGINE.OB_EMAIL_QUEUE(OB_EMAIL_FROM_EMAIL_ADDR, OB_EMAIL_FROM_NAME, OB_EMAIL_MSG, OB_EMAIL_SUBJECT, OB_EMAIL_TO_EMAIL_ADDR, OB_EMAIL_TO_NAME, OB_EMAIL_CONTENT)
			SELECT LV_EMAIL_FROM, LV_EMAIL_FROM, ' ', 'EFT Processing', LV_EMAIL_TO, LV_EMAIL_TO, LV_EMAIL_CONTENT
			FROM DUAL
			WHERE NOT EXISTS (
				SELECT 1 FROM ENGINE.OB_EMAIL_QUEUE
				WHERE CREATED_TS >= LD_EFT_PROPAGATION_START_TS
					AND OB_EMAIL_SUBJECT = 'EFT Processing'
					AND OB_EMAIL_CONTENT LIKE 'Automatic EFT Processing is complete.%'
			);
		END IF;
		
		UPDATE ENGINE.APP_SETTING
   		SET APP_SETTING_VALUE = TO_CHAR(CURRENT_TIMESTAMP, 'MM/DD/YYYY HH24:MI:SS')
   		WHERE APP_SETTING_CD = 'EFT_PROCESSING_LAST_RUN_COMPLETE_TS';
	END;
	
	PROCEDURE START_EFT_PROPAGATION
	IS
	BEGIN
		UPDATE ENGINE.APP_SETTING
   		SET APP_SETTING_VALUE = TO_CHAR(CURRENT_TIMESTAMP, 'MM/DD/YYYY HH24:MI:SS')
   		WHERE APP_SETTING_CD = 'EFT_PROPAGATION_LAST_RUN_START_TS';
	END;
	
	PROCEDURE START_EFT_PROCESSING(
		PN_DOC_ID CORP.DOC.DOC_ID%TYPE)
	IS
	BEGIN
		UPDATE CORP.DOC
		SET AUTO_PROCESS_START_TS = CURRENT_TIMESTAMP
		WHERE DOC_ID = PN_DOC_ID;
	END;
	
	PROCEDURE PROCESS_EFT(
		PN_DOC_ID CORP.DOC.DOC_ID%TYPE)
	IS
    l_eft_cc_count NUMBER:=0;
    l_eft_max_cc_count NUMBER:=0;
	BEGIN
    select to_number(APP_SETTING_VALUE) into l_eft_max_cc_count from ENGINE.APP_SETTING where APP_SETTING_CD='EFT_MAX_CC_COUNT';
    select sum(ledger_count) into l_eft_cc_count from corp.customer_bank cb join corp.doc d on cb.customer_bank_id=d.customer_bank_id 
		join corp.batch b on b.doc_id=d.doc_id
		join corp.batch_total bt on bt.batch_id=b.batch_id and bt.entry_type='CC' and bt.payable='Y'
		where d.doc_id = PN_DOC_ID
    and cb.PAY_CYCLE_ID = 12;
    IF l_eft_cc_count >l_eft_max_cc_count THEN
      CORP.PAYMENTS_PKG.SPLIT_DOC(PN_DOC_ID);
    ELSE
      IF GET_DOC_STATUS(PN_DOC_ID) NOT IN ('P', 'S') THEN
        CORP.PAYMENTS_PKG.LOCK_DOC(PN_DOC_ID, NULL);
        CORP.PAYMENTS_PKG.APPROVE_PAYMENT(PN_DOC_ID, NULL);			
        CORP.PAYMENTS_PKG.MARK_DOC_PAID(PN_DOC_ID, NULL);
      END IF;
      
      UPDATE CORP.DOC
      SET AUTO_PROCESS_END_TS = CURRENT_TIMESTAMP
      WHERE DOC_ID = PN_DOC_ID;
      
      CHECK_EFT_PROCESS_COMPLETION;
    END IF;
		
	END;
	
	PROCEDURE COMPLETE_EFT_PROPAGATION
	IS
	BEGIN
		UPDATE ENGINE.APP_SETTING
   		SET APP_SETTING_VALUE = TO_CHAR(CURRENT_TIMESTAMP, 'MM/DD/YYYY HH24:MI:SS')
   		WHERE APP_SETTING_CD = 'EFT_PROPAGATION_LAST_RUN_COMPLETE_TS';
		
		CHECK_EFT_PROCESS_COMPLETION;
	END;
	
  PROCEDURE           SPLIT_DOC(
	l_doc_id CORP.DOC.DOC_ID%TYPE)
	IS
		l_min_amount NUMBER:=25;
		l_eft_max_cc_count NUMBER;
		l_cc_count NUMBER:=0;
		l_total_amount NUMBER;
		l_cb_id CORP.DOC.CUSTOMER_BANK_ID%TYPE;
	    l_cur_id CORP.DOC.CURRENCY_ID%TYPE;
	    l_bu_id CORP.DOC.BUSINESS_UNIT_ID%TYPE;
	    l_new_doc_id CORP.DOC.DOC_ID%TYPE;
	BEGIN
		IF GET_DOC_STATUS(l_doc_id) NOT IN ('P', 'S') THEN
	  CORP.PAYMENTS_PKG.LOCK_DOC(l_doc_id, NULL);
	  
	  SELECT D.CUSTOMER_BANK_ID, D.CURRENCY_ID, D.BUSINESS_UNIT_ID
	  INTO l_cb_id, l_cur_id, l_bu_id
	  FROM DOC D
	  WHERE D.DOC_ID = l_doc_id;
	   
	  select to_number(APP_SETTING_VALUE) into l_eft_max_cc_count 
	  from ENGINE.APP_SETTING where APP_SETTING_CD='EFT_MAX_CC_COUNT';
	                   
	  WHILE true LOOP
	  	  select sum(total_ledger_amount) into l_total_amount from corp.batch where doc_id=l_doc_id;
	      --dbms_output.put_line('before compare l_total_amount='||l_total_amount||' l_min_amount='||l_min_amount);
	  	  IF l_total_amount>= l_min_amount THEN
	  	  	  l_total_amount:=0;
	          l_cc_count:=0;
	          l_new_doc_id := CORP.PAYMENTS_PKG.GET_OR_CREATE_DOC(l_cb_id, l_cur_id, l_bu_id);
	           --dbms_output.put_line('before positive update doc_id='||l_new_doc_id);
	          FOR c_pos IN (select b.batch_id, b.total_ledger_amount, bt.ledger_count 
	          from corp.batch b join corp.batch_total bt on b.batch_id= bt.batch_id and bt.payable='Y' and bt.entry_type='CC' 
	          where b.doc_id=l_doc_id and b.total_ledger_amount>=0 order by b.batch_id) LOOP
	            IF l_cc_count+c_pos.ledger_count <= l_eft_max_cc_count or l_cc_count=0 THEN
	              l_cc_count:=l_cc_count+c_pos.ledger_count;
	              l_total_amount:=l_total_amount+c_pos.total_ledger_amount;
	              update corp.batch set doc_id=l_new_doc_id where batch_id=c_pos.batch_id;
	              --dbms_output.put_line('positive update:'||c_pos.batch_id||' to doc_id='||l_new_doc_id);
	            ELSE
	              EXIT;
	            END IF;
	          END LOOP;
	      
	          FOR c_neg IN (select b.batch_id, b.total_ledger_amount, nvl(bt.ledger_count,0) ledger_count
	          from corp.batch b left outer join corp.batch_total bt on b.batch_id= bt.batch_id and bt.payable='Y' and bt.entry_type='CC' 
	          where b.doc_id=l_doc_id and b.total_ledger_amount<=0 order by b.batch_id) LOOP
	              IF l_total_amount+c_neg.total_ledger_amount >= l_min_amount and l_cc_count+c_neg.ledger_count <= l_eft_max_cc_count THEN
	                l_cc_count:=l_cc_count+c_neg.ledger_count;
		              l_total_amount:=l_total_amount+c_neg.total_ledger_amount;
	                update corp.batch set doc_id=l_new_doc_id where batch_id=c_neg.batch_id;
	                --dbms_output.put_line('negative update:'||c_neg.batch_id||' to doc_id='||l_new_doc_id);
	              ELSE
	               EXIT;
	              END IF;
	          END LOOP;
	          --dbms_output.put_line('after negative doc_id='||l_new_doc_id);
	          IF l_total_amount > 0 THEN
	            CORP.PAYMENTS_PKG.LOCK_DOC(l_new_doc_id, NULL);
	            CORP.PAYMENTS_PKG.APPROVE_PAYMENT(l_new_doc_id, NULL);			
	            CORP.PAYMENTS_PKG.MARK_DOC_PAID(l_new_doc_id, NULL);
	            COMMIT;
	          ELSE
	            CORP.PAYMENTS_PKG.UNLOCK_DOC(l_doc_id);
	            EXIT;
	          END IF;         
	      ELSE
	        --dbms_output.put_line('unlock doc_id='||l_doc_id);
	        CORP.PAYMENTS_PKG.UNLOCK_DOC(l_doc_id);
	        EXIT;
	      END IF;
	  END LOOP;
	  END IF;
	END;
  
    PROCEDURE PROCESS_FEE_COMMISSION_INS (
      pn_ledger_id LEDGER.LEDGER_ID%TYPE,
      pn_trans_id CORP.LEDGER.TRANS_ID%TYPE,
      pn_process_fee_id CORP.LEDGER.PROCESS_FEE_ID%TYPE,
      pn_settle_state_id CORP.LEDGER.SETTLE_STATE_ID%TYPE,
      pn_amt LEDGER.AMOUNT%TYPE,
      pd_close_date LEDGER.LEDGER_DATE%TYPE,
      pd_settle_date CORP.LEDGER.LEDGER_DATE%TYPE)
    IS
      ln_terminal_id  BATCH.TERMINAL_ID%TYPE;
      ln_currency_id DOC.CURRENCY_ID%TYPE;
      ln_commission_batch_id BATCH.BATCH_ID%TYPE;
    BEGIN
        IF pn_amt <= 0 OR pn_process_fee_id IS NULL THEN
          RETURN;
        END IF;
        IF NVL(pn_process_fee_id,0) = 0 THEN
          RAISE_APPLICATION_ERROR(-20801, 'Can not create a commission on ledger ' || pn_ledger_id || ' for amount ' || pn_amt);
        END IF;
        SELECT b.terminal_id, d.currency_id
        INTO ln_terminal_id, ln_currency_id
        FROM corp.batch b
        JOIN corp.doc d on b.doc_id = d.doc_id
        JOIN corp.ledger l on b.batch_id = l.batch_id
        WHERE l.ledger_id = pn_ledger_id;
        FOR l_cur in (SELECT 
                        LEDGER_SEQ.NEXTVAL as ledger_id,
                        pfc.process_fee_commission_id as process_fee_commission_id,
                        least(greatest((nvl(pfc.sell_amount,0) + (nvl(pfc.sell_percent,0) * pn_amt)), nvl(pfc.sell_min,0)), pn_amt) - least(greatest((nvl(pfc.buy_amount,0) + (nvl(pfc.buy_percent,0) * pn_amt)), nvl(pfc.buy_min,0)), pn_amt) as commission_amt,
                        pfc.customer_bank_id as commission_bank_id
                      FROM corp.process_fee_commission pfc
                      WHERE pfc.process_fee_id = pn_process_fee_id) LOOP
            ln_commission_batch_id := GET_OR_CREATE_BATCH(ln_terminal_id, l_cur.commission_bank_id, pd_close_date, ln_currency_id, 'N');
            INSERT INTO LEDGER(
                LEDGER_ID,
                RELATED_LEDGER_ID,
                TRANS_ID,
                PROCESS_FEE_ID,
                PROCESS_FEE_COMMISSION_ID,
                ENTRY_TYPE,
                AMOUNT,
                ENTRY_DATE,
                BATCH_ID,
                SETTLE_STATE_ID,
                LEDGER_DATE)
            VALUES (
                l_cur.ledger_id,
                pn_ledger_id,
                pn_trans_id,
                pn_process_fee_id,
                l_cur.process_fee_commission_id,
                'PC',
                l_cur.commission_amt,
                pd_close_date,
                ln_commission_batch_id,
                pn_settle_state_id,
                pd_settle_date);
        END LOOP;
    END;
    
    PROCEDURE SERVICE_FEE_COMMISSION_INS (
      pn_ledger_id LEDGER.LEDGER_ID%TYPE,
      pn_trans_id CORP.LEDGER.TRANS_ID%TYPE,
      pn_service_fee_id CORP.LEDGER.SERVICE_FEE_ID%TYPE,
      pn_fee_id SERVICE_FEES.FEE_ID%TYPE,
      pn_settle_state_id CORP.LEDGER.SETTLE_STATE_ID%TYPE,
      pn_amt LEDGER.AMOUNT%TYPE,
      pd_close_date LEDGER.LEDGER_DATE%TYPE,
      pd_settle_date CORP.LEDGER.LEDGER_DATE%TYPE)
    IS
      ln_terminal_id  BATCH.TERMINAL_ID%TYPE;
      ln_currency_id DOC.CURRENCY_ID%TYPE;
      ln_commission_batch_id BATCH.BATCH_ID%TYPE;
    BEGIN
        -- currently service fee commissions can only be paid on monthly service fees
        IF pn_amt = 0 OR pn_service_fee_id IS NULL OR pn_fee_id != 1 THEN
          RETURN;
        END IF;
        IF pn_amt > 0 THEN
          RAISE_APPLICATION_ERROR(-20801, 'Can not create a commission on ledger ' || pn_ledger_id || ': for a credit amount ' || pn_amt);
        END IF;
        IF NVL(pn_service_fee_id,0) = 0 THEN
          RAISE_APPLICATION_ERROR(-20801, 'Can not create a commission on ledger ' || pn_ledger_id || ' for amount ' || pn_amt);
        END IF;
        SELECT b.terminal_id, d.currency_id
        INTO ln_terminal_id, ln_currency_id
        FROM corp.batch b
        JOIN corp.doc d on b.doc_id = d.doc_id
        JOIN corp.ledger l on b.batch_id = l.batch_id
        WHERE l.ledger_id = pn_ledger_id;
        FOR l_cur in (SELECT 
                        LEDGER_SEQ.NEXTVAL as ledger_id,
                        sfc.service_fee_commission_id as service_fee_commission_id,
                        (nvl(sfc.sell_amount,0) - nvl(sfc.buy_amount,0)) as commission_amt,
                        sfc.customer_bank_id as commission_bank_id
                      FROM corp.service_fee_commission sfc
                      WHERE sfc.service_fee_id = pn_service_fee_id) LOOP
            ln_commission_batch_id := GET_OR_CREATE_BATCH(ln_terminal_id, l_cur.commission_bank_id, pd_close_date, ln_currency_id, 'N');
            INSERT INTO LEDGER(
                LEDGER_ID,
                RELATED_LEDGER_ID,
                TRANS_ID,
                SERVICE_FEE_ID,
                SERVICE_FEE_COMMISSION_ID,
                ENTRY_TYPE,
                AMOUNT,
                ENTRY_DATE,
                BATCH_ID,
                SETTLE_STATE_ID,
                LEDGER_DATE)
            VALUES (
                l_cur.ledger_id,
                pn_ledger_id,
                pn_trans_id,
                pn_service_fee_id,
                l_cur.service_fee_commission_id,
                'SC',
                l_cur.commission_amt,
                pd_close_date,
                ln_commission_batch_id,
                pn_settle_state_id,
                pd_settle_date);
        END LOOP;
    END;
END;
/
-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/releases/REL_Edge_Update_R45/R45.PartnerUpdates.1.sql?rev=HEAD
GRANT EXECUTE ON CORP.SET_TERMINAL_BUY_SELL_RATES TO USALIVE_APP_ROLE;
GRANT EXECUTE ON CORP.SET_TERMINAL_BUY_SELL_RATES TO USAT_DMS_ROLE;

GRANT EXECUTE ON CORP.LICENSE_PF_UPDATE TO USALIVE_APP_ROLE;
GRANT EXECUTE ON CORP.LICENSE_SF_UPDATE TO USALIVE_APP_ROLE;

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/VW_USER_CUSTOMER_BANK.vws?rev=1.7
CREATE OR REPLACE FORCE VIEW REPORT.VW_USER_CUSTOMER_BANK(USER_ID, CUSTOMER_BANK_ID, ALLOW_EDIT, STATUS, CUSTOMER_ID)
AS
  SELECT USER_ID, CUSTOMER_BANK_ID, MAX(ALLOW_EDIT), STATUS, CUSTOMER_ID
    FROM (
  SELECT UCB.USER_ID,
         UCB.CUSTOMER_BANK_ID,
         UCB.ALLOW_EDIT,
         CB.STATUS,
         CB.CUSTOMER_ID
    FROM REPORT.USER_LOGIN U
    JOIN REPORT.USER_CUSTOMER_BANK UCB ON U.USER_ID = UCB.USER_ID
    JOIN CORP.CUSTOMER_BANK CB ON UCB.CUSTOMER_BANK_ID = CB.CUSTOMER_BANK_ID
   WHERE U.STATUS = 'A'
     AND (U.CUSTOMER_ID IN(0, -1) OR U.CUSTOMER_ID = CB.CUSTOMER_ID)
   UNION ALL
  SELECT U.USER_ID,
         CB.CUSTOMER_BANK_ID,
         'N',
         CB.STATUS,
         CB.CUSTOMER_ID
    FROM REPORT.USER_LOGIN U
    JOIN REPORT.USER_PRIVS UP ON U.USER_ID = UP.USER_ID
   CROSS JOIN CORP.CUSTOMER_BANK CB
   WHERE UP.PRIV_ID = 11
     AND (U.CUSTOMER_ID = 0 OR U.CUSTOMER_ID = CB.CUSTOMER_ID)
     AND U.USER_TYPE != 8
   UNION ALL
  SELECT U.USER_ID,
         CB.CUSTOMER_BANK_ID,
         CASE WHEN UP.PRIV_ID = 29 THEN 'Y' ELSE 'N' END,
         CB.STATUS,
         CB.CUSTOMER_ID
    FROM REPORT.USER_LOGIN U
    JOIN REPORT.USER_PRIVS UP ON U.USER_ID = UP.USER_ID
    JOIN CORP.VW_CUSTOMER_HIERARCHY C ON U.CUSTOMER_ID = C.ANCESTOR_CUSTOMER_ID
    JOIN CORP.CUSTOMER_BANK CB ON C.DESCENDENT_CUSTOMER_ID = CB.CUSTOMER_ID
   WHERE U.STATUS = 'A'
     AND UP.PRIV_ID IN(11, 29)
     AND U.USER_TYPE = 8
     AND U.CUSTOMER_ID > 0)
  GROUP BY USER_ID, CUSTOMER_BANK_ID, STATUS, CUSTOMER_ID     
;
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/VW_USER_TERMINAL.vws?rev=1.6
CREATE OR REPLACE FORCE VIEW REPORT.VW_USER_TERMINAL (USER_ID, TERMINAL_ID, ALLOW_EDIT) AS 
  SELECT USER_ID, TERMINAL_ID, MAX(ALLOW_EDIT)
FROM (
    SELECT UT.USER_ID, UT.TERMINAL_ID, ALLOW_EDIT
      FROM REPORT.USER_TERMINAL UT
      JOIN REPORT.TERMINAL T ON T.TERMINAL_ID = UT.TERMINAL_ID
     WHERE T.STATUS <> 'D'
    UNION ALL
    SELECT U.USER_ID, T.TERMINAL_ID, CASE WHEN UP.PRIV_ID = 7 THEN 'Y' ELSE 'N' END
      FROM REPORT.USER_LOGIN U
      JOIN REPORT.USER_PRIVS UP ON U.USER_ID = UP.USER_ID
      CROSS JOIN REPORT.TERMINAL T
     WHERE T.STATUS <> 'D'
       AND UP.PRIV_ID IN(6, 7)
       AND (U.CUSTOMER_ID = 0 OR U.CUSTOMER_ID = T.CUSTOMER_ID)
       AND U.STATUS = 'A'
    UNION ALL
    SELECT USER_ID, 0, 'N'
      FROM USER_LOGIN U
     WHERE U.STATUS = 'A'
    UNION ALL
    SELECT U.USER_ID, T.TERMINAL_ID, CASE WHEN UP.PRIV_ID = 7 THEN 'Y' ELSE 'N' END
      FROM REPORT.USER_LOGIN U
      JOIN REPORT.USER_PRIVS UP ON U.USER_ID = UP.USER_ID
      JOIN CORP.VW_CUSTOMER_HIERARCHY C ON U.CUSTOMER_ID = C.ANCESTOR_CUSTOMER_ID
      JOIN REPORT.TERMINAL T ON C.DESCENDENT_CUSTOMER_ID = T.CUSTOMER_ID
     WHERE T.STATUS <> 'D'
       AND UP.PRIV_ID IN(6, 7)
       AND U.STATUS = 'A'
       AND U.USER_TYPE = 8 
       AND U.CUSTOMER_ID > 0
       AND C.HIERARCHY_ACTIVE_YN_FLAG = 'Y')
GROUP BY USER_ID, TERMINAL_ID
;

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/DATA_IN_PKG.pbk?rev=1.42
CREATE OR REPLACE PACKAGE BODY REPORT.DATA_IN_PKG IS
   FK_NOT_FOUND EXCEPTION;
   PRAGMA EXCEPTION_INIT(FK_NOT_FOUND, -2291);

    FUNCTION GET_OR_CREATE_DEVICE(
        l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
        l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE,
        l_device_type EPORT.DEVICE_TYPE_ID%TYPE DEFAULT NULL,
        l_tries PLS_INTEGER DEFAULT 10)
     RETURN EPORT.EPORT_ID%TYPE
    IS
        PRAGMA AUTONOMOUS_TRANSACTION;
        l_device_id EPORT.EPORT_ID%TYPE;
        l_use_device_type EPORT.DEVICE_TYPE_ID%TYPE;
    BEGIN
        SELECT EPORT_ID
          INTO l_device_id
          FROM EPORT
         WHERE EPORT_SERIAL_NUM = l_device_serial;
         -- NOTE: eventually the above should be source system dependent
         RETURN l_device_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            SELECT EPORT_SEQ.NEXTVAL INTO l_device_id FROM DUAL;
            IF l_device_type IS NOT NULL THEN
                l_use_device_type := l_device_type;
            ELSIF l_device_serial LIKE 'E4%' THEN
                l_use_device_type := 0; -- G4
			ELSIF l_device_serial LIKE 'EE%' THEN
                l_use_device_type := 13; -- Edge
            ELSIF l_device_serial LIKE 'G%' THEN
                l_use_device_type := 1; -- G5-G8
			ELSIF l_device_serial LIKE 'K%' THEN
                l_use_device_type := 11; -- Kiosk
            ELSIF l_device_serial LIKE 'M1%' THEN
                l_use_device_type := 6; -- MEI
            --ELSIF l_device_serial LIKE '10%' THEN -- esuds
            ELSIF l_device_serial LIKE '10%' THEN
                l_use_device_type := 3; -- Radisys Brick
            ELSE
                l_use_device_type := 10;
            END IF;
            BEGIN
                INSERT INTO EPORT(EPORT_ID, EPORT_SERIAL_NUM, ACTIVATION_DATE, DEVICE_TYPE_ID)
			  		 VALUES(l_device_id, l_device_serial, SYSDATE, l_use_device_type);
 		        COMMIT;
            EXCEPTION
                WHEN FK_NOT_FOUND THEN
                    ROLLBACK;
                    RAISE_APPLICATION_ERROR(-20889, 'Device Type ''' || l_use_device_type || ''' does not exist');
                WHEN DUP_VAL_ON_INDEX THEN
                    ROLLBACK;
                    IF l_tries > 0 THEN
                        RETURN GET_OR_CREATE_DEVICE(l_device_serial, l_source_system_cd, l_device_type, l_tries - 1);
                    ELSE
                        RAISE;
                    END IF;
                WHEN OTHERS THEN
                    ROLLBACK;
                    RAISE;
            END;
            RETURN l_device_id;
        WHEN OTHERS THEN
            RAISE;
    END;
  
    FUNCTION GET_OR_CREATE_MERCHANT(
        l_merchant_num CORP.MERCHANT.MERCHANT_NBR%TYPE,
        l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE)
     RETURN CORP.MERCHANT.MERCHANT_ID%TYPE
    IS
        PRAGMA AUTONOMOUS_TRANSACTION;
        l_merchant_id CORP.MERCHANT.MERCHANT_ID%TYPE;
    BEGIN
        SELECT MERCHANT_ID
          INTO l_merchant_id
          FROM CORP.MERCHANT
         WHERE MERCHANT_NBR = l_merchant_num;
         RETURN l_merchant_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            SELECT CORP.MERCHANT_SEQ.NEXTVAL INTO l_merchant_id FROM DUAL;
            INSERT INTO CORP.MERCHANT(MERCHANT_ID, MERCHANT_NBR, DESCRIPTION, UPD_BY)
			  		 VALUES(l_merchant_id, l_merchant_num, 'Created for source system "'||l_source_system_cd||'"', 0);
            COMMIT;
            RETURN l_merchant_id;
        WHEN OTHERS THEN
            ROLLBACK;
            RAISE;
    END;
    
  PROCEDURE ADD_TRANSACTION(
        l_dup_flag OUT VARCHAR,
        l_report_tran_id OUT REPORT.TRANS.TRAN_ID%TYPE,
        l_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE,
        l_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE,
        l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
        l_start_date TRANS.START_DATE%TYPE,
        l_close_date TRANS.CLOSE_DATE%TYPE,
        l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE,
        l_total_amount TRANS.TOTAL_AMOUNT%TYPE,
        l_card_number TRANS.CARD_NUMBER%TYPE,
        l_received_date TRANS.SERVER_DATE%TYPE,
        l_settle_state_id TRANS.SETTLE_STATE_ID%TYPE,
        l_settle_date TRANS.SETTLE_DATE%TYPE,
        l_preauth_amount TRANS.PREAUTH_AMOUNT%TYPE,
        l_preauth_date   TRANS.PREAUTH_DATE%TYPE,
        l_approval_cd TRANS.CC_APPR_CODE%TYPE,
        l_merchant_num CORP.MERCHANT.MERCHANT_NBR%TYPE,
        l_description TRANS.DESCRIPTION%TYPE,
        l_orig_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE DEFAULT NULL,
        l_orig_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE DEFAULT NULL,
        l_currency_code CORP.CURRENCY.CURRENCY_CODE%TYPE,
        l_device_type EPORT.DEVICE_TYPE_ID%TYPE DEFAULT NULL,
        l_card_name REPORT.CARD_TYPE.CARD_NAME%TYPE DEFAULT NULL,
        l_consumer_acct_id TRANS.CONSUMER_ACCT_ID%TYPE DEFAULT NULL,
        l_source_tran_id REPORT.TRANS.SOURCE_TRAN_ID%TYPE DEFAULT NULL)
    IS
      l_reload_flag VARCHAR(1);
    BEGIN
      ADD_TRANSACTION(l_dup_flag,l_report_tran_id,l_reload_flag,l_machine_trans_no,l_source_system_cd,l_device_serial,
      l_start_date,l_close_date,l_trans_type_id,l_total_amount,l_card_number,
      l_received_date,l_settle_state_id,l_settle_date,l_preauth_amount,l_preauth_date,
      l_approval_cd,l_merchant_num,l_description,l_orig_machine_trans_no,l_orig_source_system_cd,
      l_currency_code,l_device_type,l_card_name,l_consumer_acct_id,l_source_tran_id);
    
    END;

  /* This allows external systems to add transactions. Credit, debit,
   * pass, access, or maintenance cards or refund, chargeback transaction
   * or cash should be added this way. Refunds
   * and chargebacks should always provide a valid original machine tran number.
   */
  -- R44 Patch and above
  PROCEDURE ADD_TRANSACTION(
        l_dup_flag OUT VARCHAR,
        l_report_tran_id OUT REPORT.TRANS.TRAN_ID%TYPE,
        l_reload_flag OUT VARCHAR,
        l_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE,
        l_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE,
        l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
        l_start_date TRANS.START_DATE%TYPE,
        l_close_date TRANS.CLOSE_DATE%TYPE,
        l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE,
        l_total_amount TRANS.TOTAL_AMOUNT%TYPE,
        l_card_number TRANS.CARD_NUMBER%TYPE,
        l_received_date TRANS.SERVER_DATE%TYPE,
        l_settle_state_id TRANS.SETTLE_STATE_ID%TYPE,
        l_settle_date TRANS.SETTLE_DATE%TYPE,
        l_preauth_amount TRANS.PREAUTH_AMOUNT%TYPE,
        l_preauth_date   TRANS.PREAUTH_DATE%TYPE,
        l_approval_cd TRANS.CC_APPR_CODE%TYPE,
        l_merchant_num CORP.MERCHANT.MERCHANT_NBR%TYPE,
        l_description TRANS.DESCRIPTION%TYPE,
        l_orig_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE DEFAULT NULL,
        l_orig_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE DEFAULT NULL,
        l_currency_code CORP.CURRENCY.CURRENCY_CODE%TYPE,
        l_device_type EPORT.DEVICE_TYPE_ID%TYPE DEFAULT NULL,
        l_card_name REPORT.CARD_TYPE.CARD_NAME%TYPE DEFAULT NULL,
        l_consumer_acct_id TRANS.CONSUMER_ACCT_ID%TYPE DEFAULT NULL,
        l_source_tran_id REPORT.TRANS.SOURCE_TRAN_ID%TYPE DEFAULT NULL)
    IS
        l_eport_id TRANS.EPORT_ID%TYPE;
        l_merchant_id TRANS.MERCHANT_ID%TYPE;
        l_orig_tran_id TRANS.ORIG_TRAN_ID%TYPE;
        l_real_amount TRANS.TOTAL_AMOUNT%TYPE;
        l_currency_id TRANS.CURRENCY_ID%TYPE;
		l_cardtype_authority_id TRANS.CARDTYPE_AUTHORITY_ID%TYPE := NULL;
		l_lock VARCHAR2(128) := GLOBALS_PKG.REQUEST_LOCK('REPORT.TRANS::' || l_machine_trans_no, 0);
		l_refund_ind REPORT.TRANS_TYPE.REFUND_IND%TYPE;
    ln_prev_amount REPORT.TRANS.TOTAL_AMOUNT%TYPE;
    BEGIN
        -- check for dup
        BEGIN
            SELECT TRAN_ID, CASE WHEN TOTAL_AMOUNT = l_total_amount AND COALESCE(CONSUMER_ACCT_ID, 0) = COALESCE(l_consumer_acct_id, 0) THEN 'Y' ELSE 'N' END
              INTO l_report_tran_id, l_dup_flag
              FROM TRANS
             WHERE MACHINE_TRANS_NO = l_machine_trans_no
               AND SOURCE_SYSTEM_CD = l_source_system_cd;
            IF l_dup_flag = 'Y' THEN -- this is NOT an update
                l_reload_flag :='Y';
                RETURN;  
            END IF;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                l_dup_flag:='N'; -- CONTINUE, no dup found
            WHEN OTHERS THEN
                RAISE;
        END;
        
        --get needed lookup values
        l_eport_id := GET_OR_CREATE_DEVICE(l_device_serial, l_source_system_cd, l_device_type);
        IF l_merchant_num IS NOT NULL THEN
            l_merchant_id := GET_OR_CREATE_MERCHANT(l_merchant_num, l_source_system_cd);
        END IF;
        
        IF l_orig_machine_trans_no IS NOT NULL THEN
            BEGIN
                SELECT TRAN_ID
                  INTO l_orig_tran_id
                  FROM TRANS
                 WHERE MACHINE_TRANS_NO = l_orig_machine_trans_no
                   AND SOURCE_SYSTEM_CD = NVL(l_orig_source_system_cd, l_source_system_cd);
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    RAISE_APPLICATION_ERROR(-20888, 'Could not find the original transaction with MACHINE_TRANS_NUM = "'||l_orig_machine_trans_no||'" from the source system "'||l_source_system_cd||'". Transaction was NOT added.');
                WHEN OTHERS THEN
                    RAISE;
            END;
        END IF;
        
        --get currency id from currency code
        IF l_currency_code IS NOT NULL THEN
        	BEGIN
        		SELECT currency_id
        		  INTO l_currency_id
        		  FROM CORP.currency
        		 WHERE currency_code = l_currency_code;
        	EXCEPTION
        		WHEN NO_DATA_FOUND THEN
                    RAISE_APPLICATION_ERROR(-20888, 'No currency is mapped in the'
						|| ' system for currency code ''' || l_currency_code
						|| '''. A transaction can not be added with no currency'
						|| ' identified. Transaction was NOT added.');
                WHEN OTHERS THEN
                    RAISE;
        	END;
        ELSE
        	RAISE_APPLICATION_ERROR(-20880, 'A transaction can not be entered '
				|| 'without a currency identified. Transaction was NOT added.');
        END IF;
		
		IF l_card_name IS NOT NULL THEN
			SELECT MAX(CA.CARDTYPE_AUTHORITY_ID)
			INTO l_cardtype_authority_id
			FROM REPORT.CARDTYPE_AUTHORITY CA
			JOIN REPORT.CARD_TYPE CT ON CA.CARDTYPE_ID = CT.CARD_TYPE_ID			
			WHERE CT.CARD_NAME = l_card_name;
			
			IF l_cardtype_authority_id IS NULL THEN
				SELECT MAX(CA.CARDTYPE_AUTHORITY_ID)
				INTO l_cardtype_authority_id
				FROM REPORT.CARDTYPE_AUTHORITY CA
				JOIN REPORT.CARD_TYPE CT ON CA.CARDTYPE_ID = CT.CARD_TYPE_ID			
				JOIN REPORT.TRANS_TYPE TT ON CT.CARD_NAME = TT.TRANS_TYPE_NAME
				WHERE TT.TRANS_TYPE_ID = l_trans_type_id;
			END IF;
		END IF;
		
		SELECT REFUND_IND
		INTO l_refund_ind
		FROM REPORT.TRANS_TYPE
		WHERE TRANS_TYPE_ID = l_trans_type_id;
        
        IF l_refund_ind = 'Y' THEN -- make it negative
            l_real_amount := -ABS(l_total_amount);
        ELSE
            l_real_amount := ABS(l_total_amount);
        END IF;
        
        IF l_report_tran_id IS NULL THEN
            SELECT TRANS_SEQ.NEXTVAL
              INTO l_report_tran_id
              FROM DUAL;
            INSERT INTO TRANS(
                TRAN_ID,
                MACHINE_TRANS_NO,
                SOURCE_SYSTEM_CD,
                EPORT_ID,
                CARDTYPE_AUTHORITY_ID,
                START_DATE,
                CLOSE_DATE,
                TRANS_TYPE_ID,
                TOTAL_AMOUNT,
                CARD_NUMBER,
                SERVER_DATE,
                SETTLE_STATE_ID,
                SETTLE_DATE,
                PREAUTH_AMOUNT,
                PREAUTH_DATE,
                CC_APPR_CODE,
                MERCHANT_ID,
                DESCRIPTION,
                ORIG_TRAN_ID,
                CURRENCY_ID,
                CONSUMER_ACCT_ID,
                SOURCE_TRAN_ID)
              SELECT
                l_report_tran_id,
                l_machine_trans_no,
                l_source_system_cd,
                l_eport_id,
                l_cardtype_authority_id,
                l_start_date,
                l_close_date,
                l_trans_type_id,
                l_real_amount,
                l_card_number,
                l_received_date,
                l_settle_state_id,
                l_settle_date,
                l_preauth_amount,
                l_preauth_date,
                l_approval_cd,
                l_merchant_id,
                l_description,
                l_orig_tran_id,
                l_currency_id,
                l_consumer_acct_id,
                l_source_tran_id
              FROM DUAL;
              l_reload_flag :='N';
        ELSE
            DELETE
              FROM REPORT.PURCHASE
             WHERE TRAN_ID = l_report_tran_id;
            UPDATE REPORT.TRANS
               SET (EPORT_ID,
                CARDTYPE_AUTHORITY_ID,
                START_DATE,
                CLOSE_DATE,
                TRANS_TYPE_ID,
                TOTAL_AMOUNT,
                CARD_NUMBER,
                SERVER_DATE,
                SETTLE_STATE_ID,
                SETTLE_DATE,
                PREAUTH_AMOUNT,
                PREAUTH_DATE,
                CC_APPR_CODE,
                MERCHANT_ID,
                DESCRIPTION,
                ORIG_TRAN_ID,
                CURRENCY_ID,
                CONSUMER_ACCT_ID,
                SOURCE_TRAN_ID) = (
                SELECT l_eport_id,
                l_cardtype_authority_id,
                l_start_date,
                l_close_date,
                l_trans_type_id,
                l_real_amount,
                l_card_number,
                l_received_date,
                l_settle_state_id,
                l_settle_date,
                l_preauth_amount,
                l_preauth_date,
                l_approval_cd,
                l_merchant_id,
                l_description,
                l_orig_tran_id,
                l_currency_id,
                l_consumer_acct_id,
                l_source_tran_id
              FROM DUAL)
             WHERE TRAN_ID = l_report_tran_id;
             l_reload_flag :='Y';
        END IF;
    END;
    
    
    PROCEDURE ADD_TRAN_ITEM(
        l_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE,
        l_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE,
        l_column_num PURCHASE.MDB_NUMBER%TYPE,
        l_column_label PURCHASE.VEND_COLUMN%TYPE,
        l_quantity PURCHASE.AMOUNT%TYPE DEFAULT 1,
        l_price PURCHASE.PRICE%TYPE DEFAULT NULL,
        l_product_desc PURCHASE.DESCRIPTION%TYPE DEFAULT NULL,
        l_tran_line_item_type_id PURCHASE.TRAN_LINE_ITEM_TYPE_ID%TYPE DEFAULT NULL,
        l_apply_to_consumer_acct_id PURCHASE.APPLY_TO_CONSUMER_ACCT_ID%TYPE DEFAULT NULL)
    IS
    BEGIN
		INSERT INTO REPORT.PURCHASE(
			PURCHASE_ID,
			TRAN_ID,
			TRAN_DATE,
			AMOUNT,
			PRICE,
			VEND_COLUMN,
			DESCRIPTION,
			MDB_NUMBER,
			TRAN_LINE_ITEM_TYPE_ID,
			APPLY_TO_CONSUMER_ACCT_ID)
		  SELECT
			PURCHASE_SEQ.NEXTVAL,
			TRAN_ID,
			CLOSE_DATE,
			l_quantity,
			l_price,
			l_column_label,
			l_product_desc,
			l_column_num,
			l_tran_line_item_type_id,
			l_apply_to_consumer_acct_id
		  FROM REPORT.TRANS
		 WHERE MACHINE_TRANS_NO = l_machine_trans_no
		   AND SOURCE_SYSTEM_CD = l_source_system_cd;
		IF SQL%ROWCOUNT = 0 THEN
			RAISE_APPLICATION_ERROR(-20888, 'Could not find a transaction with MACHINE_TRANS_NUM = "'||l_machine_trans_no||'" from the source system "'||l_source_system_cd||'". Item was NOT added.');
		END IF;

		-- create a Softcard Reward for every Softcard Discount, funded by Softcard
		IF l_tran_line_item_type_id = 208 THEN
            INSERT INTO REPORT.PURCHASE(
                PURCHASE_ID,
                TRAN_ID,
                TRAN_DATE,
                AMOUNT,
                PRICE,
                VEND_COLUMN,
                DESCRIPTION,
                MDB_NUMBER,
                TRAN_LINE_ITEM_TYPE_ID,
                APPLY_TO_CONSUMER_ACCT_ID
			)
			SELECT
                PURCHASE_SEQ.NEXTVAL,
                TRAN_ID,
                CLOSE_DATE,
                l_quantity,
                ABS(l_price),
                REPLACE(l_column_label, 'Discount', 'Reward'),
                REPLACE(l_product_desc, 'Discount', 'Reward'),
                l_column_num,
                209,
                l_apply_to_consumer_acct_id	
			FROM REPORT.TRANS
			WHERE MACHINE_TRANS_NO = l_machine_trans_no
				AND SOURCE_SYSTEM_CD = l_source_system_cd;
			
			UPDATE REPORT.TRANS
			SET TOTAL_AMOUNT = TOTAL_AMOUNT + ABS(l_price) * l_quantity
			WHERE MACHINE_TRANS_NO = l_machine_trans_no
				AND SOURCE_SYSTEM_CD = l_source_system_cd;
		END IF;
    END;
    
    /* This procedure allows external systems to update the settlement info
     * of a transaction.
     *
     */
    PROCEDURE UPDATE_SETTLE_INFO(
        l_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE,
        l_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE,
        l_settle_state_id TRANS.SETTLE_STATE_ID%TYPE,
        l_settle_date TRANS.SETTLE_DATE%TYPE,
        l_approval_cd TRANS.CC_APPR_CODE%TYPE,
        l_merchant_num CORP.MERCHANT.MERCHANT_NBR%TYPE,
        ln_amount REPORT.TRANS.TOTAL_AMOUNT%TYPE DEFAULT NULL,
        pn_override_trans_type_id REPORT.TRANS.TRANS_TYPE_ID%TYPE DEFAULT NULL)
    IS
        l_merchant_id TRANS.MERCHANT_ID%TYPE;
        ln_tran_id REPORT.TRANS.TRAN_ID%TYPE;
        lc_imported PSS.SALE.IMPORTED%TYPE;
        ln_old_amount REPORT.TRANS.TOTAL_AMOUNT%TYPE;
        ln_old_trans_type_id REPORT.TRANS.TRANS_TYPE_ID%TYPE;
    BEGIN
        IF l_merchant_num IS NOT NULL THEN
            l_merchant_id := GET_OR_CREATE_MERCHANT(l_merchant_num, l_source_system_cd);
        END IF;
        UPDATE TRANS T SET (SETTLE_STATE_ID, SETTLE_DATE, CC_APPR_CODE, MERCHANT_ID) =
            (SELECT NVL(l_settle_state_id, T.SETTLE_STATE_ID),
                    l_settle_date,
                    NVL(l_approval_cd, T.CC_APPR_CODE),
                    NVL(l_merchant_id, T.MERCHANT_ID)
              FROM DUAL)
          WHERE T.MACHINE_TRANS_NO = l_machine_trans_no
            RETURNING TRAN_ID, TOTAL_AMOUNT, TRANS_TYPE_ID
            INTO ln_tran_id, ln_old_amount, ln_old_trans_type_id;
        IF SQL%ROWCOUNT = 0 THEN
            IF l_settle_state_id = 2 AND ln_amount = 0 THEN
                SELECT NVL(MAX(IMPORTED), '-')
                  INTO lc_imported
                  FROM PSS.TRAN X
                  JOIN PSS.SALE S ON X.TRAN_ID = S.TRAN_ID
                 WHERE X.TRAN_GLOBAL_TRANS_CD = l_machine_trans_no;
                IF lc_imported = '-' THEN
                    RETURN; -- sale was cancelled before it was imported
                END IF;
            END IF;
            RAISE_APPLICATION_ERROR(-20888, 'Could not find a transaction with MACHINE_TRANS_NUM = "'||l_machine_trans_no||'". Settle info was NOT updated.');
        END IF;
        IF (ln_amount IS NOT NULL AND ln_amount != ln_old_amount) OR (pn_override_trans_type_id IS NOT NULL AND pn_override_trans_type_id != ln_old_trans_type_id) THEN
            UPDATE REPORT.TRANS T
               SET TOTAL_AMOUNT = NVL(ln_amount, T.TOTAL_AMOUNT),
                   TRANS_TYPE_ID = NVL(pn_override_trans_type_id, T.TRANS_TYPE_ID)
             WHERE TRAN_ID = ln_tran_id;
        END IF;
        REPORT.SYNC_PKG.RECEIVE_TRANS_SETTLEMENT(ln_tran_id, l_settle_state_id, l_settle_date, l_approval_cd);
    END;
    
    /* This procedure allows external systems to update the device info
     * of their devices
     */
    PROCEDURE UPDATE_DEVICE_INFO(
        l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
        l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE,
        l_device_type_id EPORT.DEVICE_TYPE_ID%TYPE/*,
        l_device_name EPORT.DEVICE_NAME%TYPE*/)
    IS
        l_device_id EPORT.EPORT_ID%TYPE;
    BEGIN
        l_device_id := GET_OR_CREATE_DEVICE(l_device_serial, l_source_system_cd);
        UPDATE EPORT E SET DEVICE_TYPE_ID = l_device_type_id
         WHERE E.EPORT_ID = l_device_id;
    END;
    
    PROCEDURE ADD_FILL(
       l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
       l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE,
       l_fill_date IN FILL.FILL_DATE%TYPE)
    IS
       l_prev_fill_date FILL.FILL_DATE%TYPE;
       l_eport_id FILL.EPORT_ID%TYPE;
    BEGIN
        l_eport_id := GET_OR_CREATE_DEVICE(l_device_serial, l_source_system_cd);
  	    INSERT INTO FILL (FILL_ID, EPORT_ID, FILL_DATE)
 	         VALUES(FILL_SEQ.NEXTVAL, l_eport_id, l_fill_date);
    END;
    /* need to change alerts to relate to devices not terminals
    PROCEDURE ADD_ALERT(
       l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
       l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE,
       l_alert_id ALERT.ALERT_ID%TYPE,
       l_alert_date TERMINAL_ALERT.ALERT_DATE%TYPE,
       l_details TERMINAL_ALERT.DETAILS%TYPE)
    IS
       l_eport_id FILL.EPORT_ID%TYPE;
    BEGIN
        l_eport_id := GET_OR_CREATE_DEVICE(l_device_serial, l_source_system_cd);
    	 SELECT TERMINAL_ALERT_SEQ.NEXTVAL INTO l_id FROM DUAL;
    	 INSERT INTO TERMINAL_ALERT (TERMINAL_ALERT_ID, ALERT_ID, TERMINAL_ID, ALERT_DATE, DETAILS, RESPONSE_SENT )
     		VALUES(l_id,l_alert_id,l_terminal_id,l_alert_date,l_details, l_sent);
    END;
    */
    /*
    Right now Legacy G4 and BEX do not have any additional refund information (PROBLEM_DATE is the TRAN_DATE, REFUND_STATUS is always 1)
    */
    
    PROCEDURE UPDATE_POS_INFO(
        l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
        l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE,
        l_location_name LOCATION.LOCATION_NAME%TYPE,
        l_customer_name CUSTOMER.CUSTOMER_NAME%TYPE,
        l_effective_date TERMINAL_EPORT.START_DATE%TYPE)
    IS
        l_device_id EPORT.EPORT_ID%TYPE;
    BEGIN
        l_device_id := GET_OR_CREATE_DEVICE(l_device_serial, l_source_system_cd);
        /*UPDATE EPORT E SET DEVICE_TYPE_ID = l_device_type_id
         WHERE E.EPORT_SERIAL_NUM = l_device_serial;*/
    END;

    /* Creates a customer, location and terminal, if necessary for the given device
     * This should only be used by BEX machines because they are currently not
     * configured in the Customer Reporting System.
     */
    PROCEDURE UPDATE_BEX_LOCATION(
        l_serial_num EPORT.EPORT_SERIAL_NUM%TYPE,
        l_location_name LOCATION.LOCATION_NAME%TYPE,
        l_customer_abbr CORP.CUSTOMER.CUSTOMER_ALT_NAME%TYPE,
        l_effective_date DATE)
    IS
        l_customer_id CORP.CUSTOMER.CUSTOMER_ID%TYPE;
        l_location_id REPORT.LOCATION.LOCATION_ID%TYPE;
        l_terminal_id REPORT.TERMINAL.TERMINAL_ID%TYPE;
        l_eport_id REPORT.EPORT.EPORT_ID%TYPE;
        l_terminal_nbr REPORT.TERMINAL.TERMINAL_NBR%TYPE;
        l_te_id REPORT.TERMINAL_EPORT.TERMINAL_EPORT_ID%TYPE;
        l_old_start_date REPORT.TERMINAL_EPORT.START_DATE%TYPE;
    BEGIN
        SELECT EPORT_ID
          INTO l_eport_id
          FROM EPORT
         WHERE EPORT_SERIAL_NUM = l_serial_num;

        SELECT MAX(CUSTOMER_ID)
          INTO l_customer_id
          FROM CORP.CUSTOMER
         WHERE CUSTOMER_ALT_NAME = l_customer_abbr;

        IF l_customer_id IS NULL THEN
            SELECT CORP.CUSTOMER_SEQ.NEXTVAL
              INTO l_customer_id
              FROM DUAL;

            INSERT INTO CORP.CUSTOMER(CUSTOMER_ID, CUSTOMER_NAME, CUSTOMER_ALT_NAME, STATUS, CREATE_BY)
	           VALUES(l_customer_id, l_customer_abbr, l_customer_abbr, 'A', 0);	
        END IF;

        SELECT MAX(LOCATION_ID)
          INTO l_location_id
          FROM LOCATION
         WHERE LOCATION_NAME = l_location_name
           AND EPORT_ID = l_eport_id;

        IF l_location_id IS NULL THEN
		  	 SELECT LOCATION_SEQ.NEXTVAL INTO l_location_id FROM DUAL;
	  		 INSERT INTO LOCATION(LOCATION_ID, TERMINAL_ID, LOCATION_NAME, CREATE_BY, STATUS, EPORT_ID)
	  		     VALUES(l_location_id, 0, l_location_name, 0, 'A', l_eport_id);
        END IF;

        SELECT MAX(TE.TERMINAL_ID)
          INTO l_terminal_id
          FROM TERMINAL_EPORT TE, TERMINAL T
         WHERE TE.TERMINAL_ID = T.TERMINAL_ID
           AND TE.EPORT_ID = l_eport_id
           AND T.CUSTOMER_ID = l_customer_id
           AND T.LOCATION_ID = l_location_id;

        IF l_terminal_id IS NULL THEN
            --customer_id or location_id changed, or its new
            SELECT MAX(T.TERMINAL_NBR)
              INTO l_terminal_nbr
              FROM TERMINAL_EPORT TE, TERMINAL T
             WHERE TE.TERMINAL_ID = T.TERMINAL_ID
               AND TE.EPORT_ID = l_eport_id;
            IF l_terminal_nbr IS NULL THEN
                l_terminal_nbr := l_serial_num;
            ELSIF l_terminal_nbr = l_serial_num THEN
                l_terminal_nbr := l_serial_num || '-1';
            ELSIF l_terminal_nbr LIKE l_serial_num || '-%' THEN
                l_terminal_nbr := l_serial_num || '-' || TO_CHAR(TO_NUMBER(SUBSTR(l_terminal_nbr, INSTR(l_terminal_nbr, '-', -1) + 1)) + 1);
            ELSE
                l_terminal_nbr := l_serial_num;
            END IF;
            SELECT TERMINAL_SEQ.NEXTVAL INTO l_terminal_id FROM DUAL;
            INSERT INTO TERMINAL(TERMINAL_ID, TERMINAL_NBR, TERMINAL_NAME, EPORT_ID, CUSTOMER_ID, LOCATION_ID, BUSINESS_UNIT_ID, PAYMENT_SCHEDULE_ID)
                VALUES(l_terminal_id, l_terminal_nbr, l_serial_num, l_eport_id, l_customer_id, l_location_id, 2, 4);
            TERMINAL_EPORT_UPD(l_terminal_id, l_eport_id, l_effective_date, NULL, NULL);
        ELSE
            -- ensure terminal_eport start date is early enough
            BEGIN
                SELECT TERMINAL_EPORT_ID, START_DATE
                  INTO l_te_id, l_old_start_date
                  FROM (SELECT TERMINAL_EPORT_ID, START_DATE
                          FROM TERMINAL_EPORT
                         WHERE TERMINAL_ID = l_terminal_id
                           AND EPORT_ID = l_eport_id
                           AND NVL(END_DATE, MAX_DATE) > l_effective_date
                           ORDER BY START_DATE ASC)
                 WHERE ROWNUM = 1;
                IF l_old_start_date > l_effective_date THEN -- we need to adjust
                    TERMINAL_EPORT_UPD(l_terminal_id, l_eport_id, l_effective_date, NULL, l_te_id);
                END IF;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN -- Need to add new entry
                    TERMINAL_EPORT_UPD(l_terminal_id, l_eport_id, l_effective_date, NULL, NULL);
                WHEN OTHERS THEN
                    RAISE;
            END;
        END IF;
    END;
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/CORP/VW_CURRENT_LICENSE.vws?rev=1.5
CREATE OR REPLACE VIEW corp.vw_current_license
(
    license_nbr
  , received
  , customer_id
  , license_id
  , license_type
)
AS
SELECT CL.LICENSE_NBR, CL.RECEIVED, CL.CUSTOMER_ID, L.LICENSE_ID, L.LICENSE_TYPE
FROM (SELECT MAX(LICENSE_NBR) LICENSE_NBR, RECEIVED, CLI.CUSTOMER_ID
  FROM CUSTOMER_LICENSE CLI, (
    SELECT MAX(RECEIVED) MAX_RECEIVED, CUSTOMER_ID 
    FROM CUSTOMER_LICENSE GROUP BY CUSTOMER_ID) M 
  WHERE RECEIVED = MAX_RECEIVED 
  AND CLI.CUSTOMER_ID = M.CUSTOMER_ID 
  GROUP BY RECEIVED, CLI.CUSTOMER_ID) CL
JOIN CORP.LICENSE_NBR NBR ON NBR.LICENSE_NBR = CL.LICENSE_NBR
JOIN CORP.LICENSE L ON L.LICENSE_ID = NBR.LICENSE_ID;
/
