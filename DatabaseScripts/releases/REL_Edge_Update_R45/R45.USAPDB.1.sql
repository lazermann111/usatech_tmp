SET DEFINE OFF;

INSERT INTO WEB_CONTENT.REQUIREMENT(REQUIREMENT_ID,REQUIREMENT_NAME,REQUIREMENT_DESCRIPTION,REQUIREMENT_CLASS,REQUIREMENT_PARAM_CLASS,REQUIREMENT_PARAM_VALUE) 
    SELECT 43,'REQ_MANAGE_PREPAID_CONSUMERS',NULL,'com.usatech.usalive.link.HasPrivilegeRequirement','java.lang.String','PRIV_MANAGE_PREPAID_CONSUMERS'
      FROM DUAL
      WHERE NOT EXISTS(SELECT 1 FROM WEB_CONTENT.REQUIREMENT WHERE REQUIREMENT_ID = 43); 
      
INSERT INTO WEB_CONTENT.WEB_LINK (WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER)
    SELECT WEB_CONTENT.SEQ_WEB_LINK_ID.NEXTVAL, 'Mass Card Update', './consumer_card_mass_manage.html','Consumer Card Mass Update','Administration','M', 177
      FROM DUAL
     WHERE NOT EXISTS(SELECT 1 FROM WEB_CONTENT.WEB_LINK WHERE WEB_LINK_LABEL = 'Mass Card Update' AND WEB_LINK_USAGE = 'M');
INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT (WEB_LINK_ID, REQUIREMENT_ID)
     SELECT WL.WEB_LINK_ID, R.COLUMN_VALUE
       FROM TABLE(NUMBER_TABLE(1, 43)) R, WEB_CONTENT.WEB_LINK WL
     WHERE WL.WEB_LINK_LABEL = 'Mass Card Update' 
       AND WL.WEB_LINK_USAGE = 'M'
       AND NOT EXISTS(SELECT 1 FROM WEB_CONTENT.WEB_LINK_REQUIREMENT WLR WHERE WLR.WEB_LINK_ID = WL.WEB_LINK_ID AND WLR.REQUIREMENT_ID = R.COLUMN_VALUE);
       
COMMIT;

ALTER TABLE PSS.CONSUMER_ACCT 
ADD NICK_NAME VARCHAR2(60);

CREATE UNIQUE INDEX REPORT.AK_CONSUMER_ACCT_NICK_NAME ON PSS.CONSUMER_ACCT(CASE WHEN NICK_NAME IS NOT NULL THEN TO_CHAR(CONSUMER_ID)||NICK_NAME END) TABLESPACE PSS_INDX ONLINE;

-- delete from report.reports where report_name='Single Transaction Data Export(CSV)';
ALTER SESSION SET CURRENT_SCHEMA = REPORT;    
DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := -1;
BEGIN
	SELECT MAX(REPORT_ID) INTO LN_REPORT_ID
	FROM REPORT.REPORTS WHERE REPORT_NAME = 'Single Transaction Data Export(CSV)';
	
	IF LN_REPORT_ID > 0 THEN
		RETURN;
	END IF;

	SELECT REPORT.REPORTS_SEQ.NEXTVAL INTO LN_REPORT_ID FROM DUAL;
	
	INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
	VALUES(LN_REPORT_ID, 'TRANS-{x}-{d}', 1, 10, 'Single Transaction Data Export(CSV)', 'Single Transaction data in csv format. Includes: Terminal Number, Transaction Reference Number, Transaction Type (Credit, Cash, Pass, etc), Card Number, Total Amount, Vended Columns, Number of Products Vended, Timestamp of the Transaction , Card Id', 'E', 0);
	
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
	VALUES( LN_REPORT_ID, 'query', 'SELECT  
	    UPPER(e.EPORT_SERIAL_NUM) "Device Serial Num",
	    ar.REF_NBR "Ref Nbr",
	    tt.TRANS_TYPE_CODE "Trans Type Code",
	    REPORT.MASK_CARD(UPPER(ar.CARD_NUMBER), ar.TRANS_TYPE_ID) "Masked Card Number",
	    ar.TOTAL_AMOUNT "Total Amount",
	    ar.VEND_COLUMN "Vend Column",
	    ar.quantity "Quantity",
	    TO_CHAR(ar.TRAN_DATE, ''MM/dd/yyyy'') "Tran Date",
	    TO_CHAR(ar.TRAN_DATE, ''HH:mm:ss'') "Tran Time",
	    cab.GLOBAL_ACCOUNT_ID "Card Id"
	FROM PSS.CONSUMER_ACCT_BASE cab
	    RIGHT OUTER JOIN (REPORT.ACTIVITY_REF ar
	    JOIN REPORT.EPORT e ON ar.EPORT_ID = e.EPORT_ID
	    JOIN REPORT.TRANS_TYPE tt ON ar.TRANS_TYPE_ID=tt.TRANS_TYPE_ID
	    ) ON ar.CONSUMER_ACCT_ID = cab.CONSUMER_ACCT_ID
	WHERE ar.TRAN_ID = {x}');

	commit;
END;
/

-- refund
GRANT UPDATE ON CORP.REFUND TO USALIVE_APP_ROLE;

update report.priv set INTERNAL_EXTERNAL_FLAG='B', customer_master_user_default='Y' where priv_id=12;
commit;

insert into web_content.web_link values(web_content.seq_web_link_id.nextval, 'Issue Refunds', './refund_search.i?folioId=81','Find Transaction and Issue Refunds','Administration','M', 178);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 1);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 4);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 15);
commit;

INSERT INTO REPORT.PRIV(PRIV_ID, DESCRIPTION, INTERNAL_EXTERNAL_FLAG, CUSTOMER_MASTER_USER_DEFAULT)
    SELECT 32, 'Admin Refund', 'B', 'Y'
      FROM DUAL
     WHERE NOT EXISTS(SELECT 1 FROM REPORT.PRIV WHERE PRIV_ID = 32);
     
COMMIT;

insert into web_content.requirement( requirement_id, requirement_name, requirement_class, requirement_param_class, requirement_param_value)
values(44, 'REQ_ADMIN_REFUND', 'com.usatech.usalive.link.HasPrivilegeRequirement', 'java.lang.String', 'PRIV_ADMIN_REFUND');

insert into web_content.web_link values(web_content.seq_web_link_id.nextval, 'Admin Refunds', './refund_admin.html','View, approve or cancel pending refunds','Administration','M', 179);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 1);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 44);
commit;

INSERT INTO REPORT.USER_PRIVS(USER_ID, PRIV_ID)
  	     SELECT U.USER_ID, 12
  	       FROM CORP.CUSTOMER C
  	       JOIN REPORT.USER_LOGIN U ON C.USER_ID = U.USER_ID
  	       LEFT OUTER JOIN REPORT.USER_PRIVS UP ON U.USER_ID = UP.USER_ID AND UP.PRIV_ID = 12
  	      WHERE C.STATUS = 'A'
  	        AND U.STATUS = 'A'
  	        AND UP.USER_ID IS NULL;
  	        
INSERT INTO REPORT.USER_PRIVS(USER_ID, PRIV_ID)
  	     SELECT U.USER_ID, 32
  	       FROM CORP.CUSTOMER C
  	       JOIN REPORT.USER_LOGIN U ON C.USER_ID = U.USER_ID
  	       LEFT OUTER JOIN REPORT.USER_PRIVS UP ON U.USER_ID = UP.USER_ID AND UP.PRIV_ID = 32
  	      WHERE C.STATUS = 'A'
  	        AND U.STATUS = 'A'
  	        AND UP.USER_ID IS NULL;

INSERT INTO REPORT.USER_PRIVS(USER_ID, PRIV_ID)
    SELECT U.USER_ID, 32
      FROM REPORT.USER_LOGIN U
      JOIN REPORT.USER_PRIVS UP0 on U.USER_ID = UP0.USER_ID
      LEFT OUTER JOIN REPORT.USER_PRIVS UP ON U.USER_ID = UP.USER_ID AND UP.PRIV_ID = 32
     WHERE U.STATUS = 'A'
       AND U.USER_TYPE != 8
       AND U.CUSTOMER_ID = 0
       AND UP0.PRIV_ID = 5
       AND UP.USER_ID IS NULL;
commit;

insert into web_content.web_link_requirement
select web_link_id, 14 from web_content.web_link where web_link_label='Issue Refunds' and web_link_group='Customer Service';
commit;

-- drop table CORP.REFUND_PROCESSED_FLAG;
CREATE TABLE CORP.REFUND_PROCESSED_FLAG
(
    PROCESSED_FLAG                          CHAR     (1)                    NOT NULL
  , DESCRIPTION                     VARCHAR2 (100)                  NOT NULL,
  CONSTRAINT PK_REFUND_PROCESSED_FLAG PRIMARY KEY(PROCESSED_FLAG)
) TABLESPACE CORP_DATA;

GRANT SELECT ON CORP.REFUND_PROCESSED_FLAG TO USALIVE_APP_ROLE;

insert into CORP.REFUND_PROCESSED_FLAG values('N', 'Not processed');
insert into CORP.REFUND_PROCESSED_FLAG values('Y', 'Processed');
insert into CORP.REFUND_PROCESSED_FLAG values('P', 'Pending');
insert into CORP.REFUND_PROCESSED_FLAG values('E', 'N/A');
insert into CORP.REFUND_PROCESSED_FLAG values('I', 'N/A');
insert into CORP.REFUND_PROCESSED_FLAG values('W', 'Waiting');
insert into CORP.REFUND_PROCESSED_FLAG values('C', 'Canceled');
insert into CORP.REFUND_PROCESSED_FLAG values('A', 'Approved');
commit;

ALTER TABLE CORP.REFUND
ADD CONSTRAINT FK_REFUND_PROCESSED_FLAG
  FOREIGN KEY (PROCESSED_FLAG)
  REFERENCES CORP.REFUND_PROCESSED_FLAG(PROCESSED_FLAG);

ALTER TABLE CORP.REFUND
DROP CONSTRAINT CK_REFUND_PROCESSED;

INSERT INTO REPORT.PREFERENCE(PREFERENCE_ID, PREFERENCE_LABEL, PREFERENCE_CATEGORY, PREFERENCE_DESC, PREFERENCE_EDITOR, PREFERENCE_LEVEL, PREFERENCE_DEFAULT)
	VALUES(106, 'Notify me when a refund is created', 'Refund', 'Whether to email the user when a refund is created', 'CHAR(1)', 1, 'Y');

COMMIT;

-- donation REPORT.VW_USER_CONSUMER_ACCT 
GRANT SELECT ON PSS.CONSUMER_ACCT TO REPORT WITH GRANT OPTION;
GRANT SELECT ON REPORT.VW_USER_CONSUMER_ACCT TO USALIVE_APP_ROLE;
GRANT SELECT ON REPORT.VW_USER_CONSUMER_ACCT TO USAT_RPT_GEN_ROLE;
GRANT SELECT ON REPORT.VW_USER_CONSUMER_ACCT TO USAT_DEV_READ_ONLY;

GRANT SELECT ON PSS.TRAN_LINE_ITEM to REPORT;
GRANT SELECT ON PSS.CONSUMER_SETTING to REPORT;
GRANT SELECT ON PSS.CONSUMER_SETTING_PARAM to REPORT;

ALTER TABLE REPORT.CAMPAIGN
ADD DONATION_PERCENT NUMBER(8,8);

INSERT INTO PSS.CONSUMER_SETTING_PARAM(CONSUMER_SETTING_PARAM_ID,DISPLAY_CONFIGURABLE_CD,DISPLAY_LABEL,DISPLAY_GROUP,EDITOR,CONVERTER,CONSUMER_COMMUNICATION_TYPE, DEFAULT_VALUE) 
    VALUES(9, 'P', 'Donate % of replenishment bonus to charity', 'Preferences', 'CHECKBOX:?Y=', null, 'DONATE_CHARITY','N');
COMMIT;

CREATE TABLE REPORT.PREPAID_DONATION(
	REPORT_TRAN_ID NUMBER(20,0) NOT NULL,
    CONSUMER_ACCT_ID NUMBER(20,0) NOT NULL,
    BASE_AMOUNT NUMBER(8,2) NOT NULL,
    DONATION_AMOUNT NUMBER(8,2) NOT NULL,
    CREATED_BY VARCHAR2(30) NOT NULL,
    CREATED_UTC_TS TIMESTAMP NOT NULL,
    LAST_UPDATED_BY VARCHAR2(30) NOT NULL,
    LAST_UPDATED_UTC_TS TIMESTAMP NOT NULL,
    CONSTRAINT PK_PD_REPORT_TRAN_ID  PRIMARY KEY(REPORT_TRAN_ID)
) TABLESPACE REPORT_DATA;

ALTER TABLE REPORT.PREPAID_DONATION
ADD CONSTRAINT FK_PD_REPORT_TRAN_ID
  FOREIGN KEY (REPORT_TRAN_ID)
  REFERENCES REPORT.TRANS(TRAN_ID);


CREATE OR REPLACE TRIGGER REPORT.TRBI_PREPAID_DONATION BEFORE 
INSERT ON REPORT.PREPAID_DONATION FOR EACH ROW 
BEGIN
    SELECT 
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER,
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER
    INTO 
        :NEW.CREATED_UTC_TS,
        :NEW.CREATED_BY,
        :NEW.LAST_UPDATED_UTC_TS,
        :NEW.LAST_UPDATED_BY
    FROM DUAL;
END;  
/

CREATE OR REPLACE TRIGGER REPORT.TRBU_PREPAID_DONATION BEFORE 
UPDATE ON REPORT.PREPAID_DONATION FOR EACH ROW 
BEGIN
    SELECT
        :OLD.CREATED_BY,
        :OLD.CREATED_UTC_TS,
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER
    INTO
        :NEW.CREATED_BY,
        :NEW.CREATED_UTC_TS,
        :NEW.LAST_UPDATED_UTC_TS,
        :NEW.LAST_UPDATED_BY
    FROM DUAL;
END; 
/

GRANT SELECT ON REPORT.PREPAID_DONATION TO USALIVE_APP_ROLE;
GRANT SELECT ON REPORT.PREPAID_DONATION TO USAT_RPT_GEN_ROLE;
GRANT SELECT ON REPORT.PREPAID_DONATION TO USAT_DEV_READ_ONLY;

ALTER SESSION SET CURRENT_SCHEMA = REPORT;    
DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := -1;
BEGIN
	SELECT MAX(REPORT_ID) INTO LN_REPORT_ID
	FROM REPORT.REPORTS WHERE REPORT_NAME = 'MORE Donation';
	
	IF LN_REPORT_ID > 0 THEN
		RETURN;
	END IF;

	SELECT REPORT.REPORTS_SEQ.NEXTVAL INTO LN_REPORT_ID FROM DUAL;
	
	INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
	VALUES( LN_REPORT_ID, 'MORE Donation - {b} to {e}', 6, 0, 'MORE Donation', 'Report that shows donation total for MORE consumer accounts grouped by region', 'N', 0);
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
	VALUES( LN_REPORT_ID, 'query', 'SELECT CUS.CUSTOMER_NAME "Operator Name", 
	CASE WHEN R.REGION_NAME IS NULL THEN ''NO REGION'' ELSE R.REGION_NAME END "Region Name",
	SUM(PD.BASE_AMOUNT) "Bonus Total",
	SUM(PD.DONATION_AMOUNT) "Donation Total"
	FROM REPORT.PREPAID_DONATION PD
	JOIN PSS.CONSUMER_ACCT CA ON PD.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
	JOIN PSS.CONSUMER C ON CA.CONSUMER_ID = C.CONSUMER_ID
	JOIN CORP.CUSTOMER CUS ON CA.CORP_CUSTOMER_ID = CUS.CUSTOMER_ID
	LEFT OUTER JOIN REPORT.REGION R ON C.REGION_ID = R.REGION_ID
    JOIN REPORT.VW_USER_CONSUMER_ACCT VUCA ON VUCA.CONSUMER_ACCT_ID=CA.CONSUMER_ACCT_ID
    JOIN REPORT.USER_LOGIN UL ON UL.USER_ID=VUCA.USER_ID 
	WHERE PD.CREATED_UTC_TS >= SYS_EXTRACT_UTC(CAST(? AS TIMESTAMP)) AND PD.CREATED_UTC_TS < SYS_EXTRACT_UTC(CAST(? + 1 AS TIMESTAMP))
	AND CA.CONSUMER_ACCT_TYPE_ID = 3
    AND UL.USER_ID = ?
	GROUP BY CUS.CUSTOMER_NAME, R.REGION_NAME');
	
	insert into report.report_param values(LN_REPORT_ID, 'paramNames','StartDate,EndDate,userId');
	insert into report.report_param values(LN_REPORT_ID, 'paramTypes','TIMESTAMP,TIMESTAMP,NUMBER');
	insert into report.report_param values(LN_REPORT_ID, 'params.StartDate','{b}');
	insert into report.report_param values(LN_REPORT_ID, 'params.EndDate','{e}');
	insert into report.report_param values(LN_REPORT_ID, 'params.userId','{u}');
	insert into report.report_param values(LN_REPORT_ID, 'isSQLFolio','true');
	insert into report.report_param values(LN_REPORT_ID, 'dateFormat','MM/dd/yyyy');
	
	insert into web_content.web_link values(web_content.seq_web_link_id.nextval, 'MORE Donation', './select_date_range_frame_sqlfolio.i?basicReportId='||LN_REPORT_ID,'MORE Donation','Daily Operations','W', 0);
	insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 1);
	commit;

END;
/
-- update Pepi replenish bonus campaign donation percent to 10
update report.campaign set donation_percent=0.1
where customer_id =(select customer_id from corp.customer where customer_name ='Pepi Food Services')
and campaign_type_id=2;
commit;

delete from web_content.web_link_requirement where web_link_id=(select web_link_id from web_content.web_link where web_link_label='MORE Accounts');
delete from web_content.web_link where web_link_id=(select web_link_id from web_content.web_link where web_link_label='MORE Accounts');
delete from report.report_param where report_id=(select report_id from report.reports where report_name='MORE Accounts');
delete from report.reports where report_id=(select report_id from report.reports where report_name='MORE Accounts');
commit;

ALTER SESSION SET CURRENT_SCHEMA = REPORT;    
DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := -1;
BEGIN

	SELECT REPORT.REPORTS_SEQ.NEXTVAL INTO LN_REPORT_ID FROM DUAL;
	
INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
VALUES( LN_REPORT_ID, 'MORE Accounts', 6, 0, 'MORE Accounts', 'Report that shows detailed more card accounts information', 'N', 0);
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( LN_REPORT_ID, 'query', 'SELECT CUS.CUSTOMER_NAME "Operator Name", R.REGION_NAME "Region Name", C.CONSUMER_FNAME "First Name", C.CONSUMER_LNAME "Last Name", CA.CONSUMER_ACCT_IDENTIFIER "Card Id", 
CA.CONSUMER_ACCT_BALANCE "Account Balance", CA.NICK_NAME "Card Nickname", CA.CONSUMER_ACCT_CD "Card Number", C.CONSUMER_EMAIL_ADDR1 "Email Address", C.CONSUMER_CELL_PHONE_NUM "Mobile Phone Number",
CA.CONSUMER_ACCT_ACTIVE_YN_FLAG "Account Enabled", DBADMIN.UTC_TO_LOCAL_DATE(CC.CREATED_UTC_TS) "Registration Time", CC.CREDENTIAL_ACTIVE_FLAG "Account Activated", 
CC.ACTIVATED_TS "Activation Time", CAS.CONSUMER_ACCT_SUB_TYPE_DESC "Account Sub Type", CA.CONSUMER_ACCT_PROMO_TOTAL "Promo Total",
CA.CONSUMER_ACCT_REPLENISH_TOTAL "Replenish Total", CA.REPLENISH_BONUS_TOTAL "Replenish Bonus Total", 
CA.LOYALTY_DISCOUNT_TOTAL "Loyalty Discount Total", CA.CASH_BACK_TOTAL "Cashback Total", 
RT.REPLENISH_TYPE_DESC "Replenish Type", CAR.REPLENISH_AMOUNT "Replenish Amount", CAR.REPLENISH_THRESHHOLD "Replenish Threshold", 
CAR.REPLENISH_CARD_MASKED "Replenish Card", CAR.LAST_REPLENISH_TRAN_TS "Last Replenish Time",
CA.CONSUMER_ACCT_REPLENISH_TOTAL+CA.CONSUMER_ACCT_PROMO_TOTAL-CA.CONSUMER_ACCT_BALANCE "Total Tran Amount",
CA.TRAN_COUNT_TOTAL "Total Tran Count"
FROM PSS.CONSUMER C
JOIN PSS.CONSUMER_ACCT CA ON C.CONSUMER_ID = CA.CONSUMER_ID
JOIN PSS.CONSUMER_ACCT_SUB_TYPE CAS ON CA.CONSUMER_ACCT_SUB_TYPE_ID = CAS.CONSUMER_ACCT_SUB_TYPE_ID
JOIN PSS.CONSUMER_CREDENTIAL CC ON C.CONSUMER_ID = CC.CONSUMER_ID
JOIN CORP.CUSTOMER CUS ON CA.CORP_CUSTOMER_ID = CUS.CUSTOMER_ID
LEFT OUTER JOIN REPORT.REGION R ON C.REGION_ID = R.REGION_ID
LEFT OUTER JOIN PSS.CONSUMER_ACCT_REPLENISH CAR ON CA.CONSUMER_ACCT_REPLENISH_ID = CAR.CONSUMER_ACCT_REPLENISH_ID
LEFT OUTER JOIN PSS.REPLENISH_TYPE RT ON CAR.REPLENISH_TYPE_ID = RT.REPLENISH_TYPE_ID
JOIN REPORT.VW_USER_CONSUMER_ACCT VUCA ON VUCA.CONSUMER_ACCT_ID=CA.CONSUMER_ACCT_ID
JOIN REPORT.USER_LOGIN UL ON UL.USER_ID=VUCA.USER_ID 
WHERE CA.CONSUMER_ACCT_TYPE_ID = 3 
AND UL.USER_ID = ?
ORDER BY CUS.CUSTOMER_NAME, CA.CONSUMER_ACCT_BALANCE, C.CONSUMER_FNAME, C.CONSUMER_LNAME');

insert into report.report_param values(LN_REPORT_ID, 'params.userId','{u}');
insert into report.report_param values(LN_REPORT_ID, 'paramNames','userId');
insert into report.report_param values(LN_REPORT_ID, 'paramTypes','NUMERIC');
insert into report.report_param values(LN_REPORT_ID, 'isSQLFolio','true');

insert into web_content.web_link values(web_content.seq_web_link_id.nextval, 'MORE Accounts', './run_sql_folio_report_async.i?basicReportId='||LN_REPORT_ID,'MORE Accounts','Daily Operations','W', 0);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 1);
commit;

END;
/

delete from web_content.web_link_requirement where web_link_id=(select web_link_id from web_content.web_link where web_link_label='Declined MORE Authorizations');
delete from web_content.web_link where web_link_id=(select web_link_id from web_content.web_link where web_link_label='Declined MORE Authorizations');
delete from report.report_param where report_id=(select report_id from report.reports where report_name='Declined MORE Authorizations');
delete from report.reports where report_id=(select report_id from report.reports where report_name='Declined MORE Authorizations');
commit;

DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := -1;
BEGIN

	SELECT REPORT.REPORTS_SEQ.NEXTVAL INTO LN_REPORT_ID FROM DUAL;
	
INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
VALUES( LN_REPORT_ID, 'Declined MORE Authorizations - {b} to {e}', 6, 0, 'Declined MORE Authorizations', 'Report that shows detailed more card declined authorization information', 'N', 0);
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( LN_REPORT_ID, 'query', 'SELECT CUS.CUSTOMER_NAME "Operator Name", R.REGION_NAME "Region Name", T.TRAN_START_TS "Transaction Time", A.AUTH_RESP_CD "Auth Response Code", 
A.AUTH_RESP_DESC "Auth Response Message", C.CONSUMER_FNAME "First Name", C.CONSUMER_LNAME "Last Name", CA.CONSUMER_ACCT_IDENTIFIER "Card Id",
CA.CONSUMER_ACCT_BALANCE "Account Balance", CA.CONSUMER_ACCT_CD "Card Number", C.CONSUMER_EMAIL_ADDR1 "Email Address", C.CONSUMER_CELL_PHONE_NUM "Mobile Phone Number",
CA.CONSUMER_ACCT_ACTIVE_YN_FLAG "Account Enabled", DBADMIN.UTC_TO_LOCAL_DATE(CC.CREATED_UTC_TS) "Registration Time", CC.CREDENTIAL_ACTIVE_FLAG "Account Activated", 
CC.ACTIVATED_TS "Activation Time", CAS.CONSUMER_ACCT_SUB_TYPE_DESC "Account Sub Type"
FROM PSS.TRAN T
JOIN PSS.AUTH A ON T.TRAN_ID = A.TRAN_ID AND A.AUTH_TYPE_CD = ''N''
JOIN PSS.CONSUMER_ACCT CA ON T.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
JOIN PSS.CONSUMER C ON CA.CONSUMER_ID = C.CONSUMER_ID
JOIN PSS.CONSUMER_ACCT_SUB_TYPE CAS ON CA.CONSUMER_ACCT_SUB_TYPE_ID = CAS.CONSUMER_ACCT_SUB_TYPE_ID
JOIN PSS.CONSUMER_CREDENTIAL CC ON C.CONSUMER_ID = CC.CONSUMER_ID
JOIN CORP.CUSTOMER CUS ON CA.CORP_CUSTOMER_ID = CUS.CUSTOMER_ID
LEFT OUTER JOIN REPORT.REGION R ON C.REGION_ID = R.REGION_ID
JOIN REPORT.VW_USER_CONSUMER_ACCT VUCA ON VUCA.CONSUMER_ACCT_ID=CA.CONSUMER_ACCT_ID
JOIN REPORT.USER_LOGIN UL ON UL.USER_ID=VUCA.USER_ID 
WHERE T.TRAN_START_TS >= CAST(? AS DATE) AND T.TRAN_START_TS < CAST(? AS DATE) + 1 AND T.TRAN_STATE_CD IN (''5'', ''7'') AND CA.CONSUMER_ACCT_TYPE_ID = 3
AND UL.USER_ID = ?
ORDER BY CUS.CUSTOMER_NAME, R.REGION_NAME, T.TRAN_START_TS DESC, C.CONSUMER_FNAME, C.CONSUMER_LNAME');

insert into report.report_param values(LN_REPORT_ID, 'paramNames','StartDate,EndDate,userId');
insert into report.report_param values(LN_REPORT_ID, 'paramTypes','TIMESTAMP,TIMESTAMP,NUMBER');
insert into report.report_param values(LN_REPORT_ID, 'params.StartDate','{b}');
insert into report.report_param values(LN_REPORT_ID, 'params.EndDate','{e}');
insert into report.report_param values(LN_REPORT_ID, 'params.userId','{u}');
insert into report.report_param values(LN_REPORT_ID, 'isSQLFolio','true');
insert into report.report_param values(LN_REPORT_ID, 'dateFormat', 'MM/dd/yyyy');

insert into web_content.web_link values(web_content.seq_web_link_id.nextval, 'Declined MORE Authorizations', './select_date_range_frame_sqlfolio.i?basicReportId='||LN_REPORT_ID,'MORE Accounts','Daily Operations','W', 0);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 1);
commit;

END;
/
 
UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET CUSTOMER_EDITABLE = 'Y' WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD IN ('1501', '1502', '1505');
COMMIT;

UPDATE PSS.AUTHORITY_PAYMENT_MASK
   SET AUTHORITY_PAYMENT_MASK_BREF = '1:1|2:3|3:4|5:12'
 WHERE AUTHORITY_PAYMENT_MASK_BREF = '1:1|2:3|3:4|4:5|5:12';
  
COMMIT;

DECLARE
	ln_report_id REPORT.REPORTS.REPORT_ID%TYPE := -1;
BEGIN
	SELECT MAX(REPORT_ID) 
    INTO ln_report_id
	  FROM REPORT.REPORTS 
   WHERE REPORT_NAME = 'Inactive MORE Accounts';
	
	IF ln_report_id IS NULL THEN
			SELECT REPORT.REPORTS_SEQ.NEXTVAL 
        INTO ln_report_id 
        FROM DUAL;
      INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
          VALUES( ln_report_id, 'Inactive MORE Accounts', 6, 0, 'Inactive MORE Accounts', 'Report that shows inactive MORE card information', 'N', 0);
  ELSE
      DELETE FROM report.report_param WHERE REPORT_ID = ln_report_id;
	END IF;

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( ln_report_id, 'query', 'SELECT * FROM (
SELECT CUS.CUSTOMER_NAME "Operator Name", R.REGION_NAME "Region Name", C.CONSUMER_FNAME "First Name", C.CONSUMER_LNAME "Last Name", CA.CONSUMER_ACCT_IDENTIFIER "Card Id", 
CA.CONSUMER_ACCT_BALANCE "Account Balance", CA.NICK_NAME "Card Nickname", CA.CONSUMER_ACCT_CD "Card Number", C.CONSUMER_EMAIL_ADDR1 "Email Address", C.CONSUMER_CELL_PHONE_NUM "Mobile Phone Number",
CA.CONSUMER_ACCT_ACTIVE_YN_FLAG "Account Enabled", DBADMIN.UTC_TO_LOCAL_DATE(CC.CREATED_UTC_TS) "Registration Time", CC.CREDENTIAL_ACTIVE_FLAG "Account Activated", 
CC.ACTIVATED_TS "Activation Time", CAS.CONSUMER_ACCT_SUB_TYPE_DESC "Account Sub Type", CA.CONSUMER_ACCT_PROMO_TOTAL "Promo Total",
CA.CONSUMER_ACCT_REPLENISH_TOTAL "Replenish Total", CA.REPLENISH_BONUS_TOTAL "Replenish Bonus Total", 
CA.LOYALTY_DISCOUNT_TOTAL "Loyalty Discount Total", CA.CASH_BACK_TOTAL "Cashback Total", 
RT.REPLENISH_TYPE_DESC "Replenish Type", CAR.REPLENISH_AMOUNT "Replenish Amount", CAR.REPLENISH_THRESHHOLD "Replenish Threshold", 
CAR.REPLENISH_CARD_MASKED "Replenish Card", CAR.LAST_REPLENISH_TRAN_TS "Last Replenish Time",
CA.CONSUMER_ACCT_REPLENISH_TOTAL+CA.CONSUMER_ACCT_PROMO_TOTAL-CA.CONSUMER_ACCT_BALANCE "Total Tran Amount",
CA.TRAN_COUNT_TOTAL "Total Tran Count",
(SELECT DBADMIN.UTC_TO_LOCAL_DATE(MAX(CAD.LAST_USED_UTC_TS)) FROM PSS.CONSUMER_ACCT_DEVICE CAD WHERE CAD.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID) "Last Activity Time"
FROM PSS.CONSUMER C
JOIN PSS.CONSUMER_ACCT CA ON C.CONSUMER_ID = CA.CONSUMER_ID
JOIN PSS.CONSUMER_ACCT_SUB_TYPE CAS ON CA.CONSUMER_ACCT_SUB_TYPE_ID = CAS.CONSUMER_ACCT_SUB_TYPE_ID
JOIN PSS.CONSUMER_CREDENTIAL CC ON C.CONSUMER_ID = CC.CONSUMER_ID
JOIN CORP.CUSTOMER CUS ON CA.CORP_CUSTOMER_ID = CUS.CUSTOMER_ID
LEFT OUTER JOIN REPORT.REGION R ON C.REGION_ID = R.REGION_ID
LEFT OUTER JOIN PSS.CONSUMER_ACCT_REPLENISH CAR ON CA.CONSUMER_ACCT_REPLENISH_ID = CAR.CONSUMER_ACCT_REPLENISH_ID
LEFT OUTER JOIN PSS.REPLENISH_TYPE RT ON CAR.REPLENISH_TYPE_ID = RT.REPLENISH_TYPE_ID
JOIN REPORT.VW_USER_CONSUMER_ACCT VUCA ON VUCA.CONSUMER_ACCT_ID=CA.CONSUMER_ACCT_ID
JOIN REPORT.USER_LOGIN UL ON UL.USER_ID=VUCA.USER_ID 
WHERE CA.CONSUMER_ACCT_TYPE_ID = 3
AND UL.USER_ID = ? 
) WHERE ("Last Activity Time" < CAST(? AS DATE) OR ("Last Activity Time" IS NULL AND ? = ''Y''))
ORDER BY "Operator Name", "Last Activity Time", "Account Balance", "First Name", "Last Name"');

INSERT INTO REPORT.REPORT_PARAM VALUES(ln_report_id, 'paramNames','userId,lastActivityDate,includeNeverActive');
INSERT INTO REPORT.REPORT_PARAM VALUES(ln_report_id, 'paramTypes','NUMERIC,TIMESTAMP,CHAR');
INSERT INTO REPORT.REPORT_PARAM VALUES(ln_report_id, 'isSQLFolio','true');
INSERT INTO REPORT.REPORT_PARAM VALUES(ln_report_id, 'params.userId','{u}');
INSERT INTO REPORT.REPORT_PARAM VALUES(ln_report_id, 'params.lastActivityDate','{b}');
INSERT INTO REPORT.REPORT_PARAM VALUES(ln_report_id, 'params.includeNeverActive','N');

COMMIT;
END;
/

DELETE FROM REPORT.REPORT_PARAM 
WHERE REPORT_ID IN(SELECT REPORT_ID FROM REPORT.REPORTS WHERE REPORT_NAME IN('MORE Accounts', 'Sprout Devices'))
AND PARAM_NAME = 'params.CustomerId';


UPDATE REPORT.REPORT_PARAM
   SET PARAM_VALUE = 
'select c.customer_name "Customer", d.device_serial_cd "Device", pp.pos_pta_device_serial_cd "Sprout POS ID", t.asset_nbr "Asset #",  
	l.location_name "Location" 
	from device.device d 
	join pss.pos p on d.device_id = p.device_id 
	join pss.pos_pta pp on p.pos_id = pp.pos_id and pp.pos_pta_activation_ts <= sysdate and (pp.pos_pta_deactivation_ts is null or pp.pos_pta_deactivation_ts > sysdate) 
	join pss.payment_subtype ps on pp.payment_subtype_id = ps.payment_subtype_id and ps.payment_subtype_class = ''Sprout'' 
	left outer join report.eport e on d.device_serial_cd = e.eport_serial_num 
	left outer join report.vw_terminal_eport te on e.eport_id = te.eport_id 
	left outer join report.terminal t on te.terminal_id = t.terminal_id 
	left outer join corp.customer c on t.customer_id = c.customer_id 
	left outer join report.location l on t.location_id = l.location_id
  JOIN REPORT.VW_USER_TERMINAL UT ON T.TERMINAL_ID = UT.TERMINAL_ID
	where d.device_active_yn_flag = ''Y'' and UT.USER_ID = ? 
	order by c.customer_name, e.eport_serial_num'
 WHERE REPORT_ID IN(SELECT REPORT_ID FROM REPORT.REPORTS WHERE REPORT_NAME = 'Sprout Devices')
   AND PARAM_NAME = 'query';

UPDATE REPORT.REPORT_PARAM
   SET PARAM_VALUE = 'userId'
 WHERE REPORT_ID IN(SELECT REPORT_ID FROM REPORT.REPORTS WHERE REPORT_NAME = 'Sprout Devices')
   AND PARAM_NAME = 'paramNames';   

UPDATE REPORT.REPORT_PARAM
   SET PARAM_VALUE = 'NUMERIC'
 WHERE REPORT_ID IN(SELECT REPORT_ID FROM REPORT.REPORTS WHERE REPORT_NAME = 'Sprout Devices')
   AND PARAM_NAME = 'paramTypes';  

INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE)
  SELECT REPORT_ID, 'params.userId','{u}'
    FROM REPORT.REPORTS R
   WHERE REPORT_NAME = 'Sprout Devices'
     AND NOT EXISTS(SELECT 1 FROM REPORT.REPORT_PARAM RP WHERE RP.REPORT_ID = R.REPORT_ID AND RP.PARAM_NAME = 'params.userId');

COMMIT;

INSERT INTO WEB_CONTENT.WEB_LINK(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER)
    SELECT WEB_CONTENT.SEQ_WEB_LINK_ID.NEXTVAL, 'Inactive MORE Accounts', './selection-date.html?basicReportId='||R.REPORT_ID||'&dateName=Last+Activity+Date','Inactive MORE Accounts','Daily Operations','W', 0
      FROM REPORT.REPORTS R
     WHERE R.REPORT_NAME = 'Inactive MORE Accounts'
       AND NOT EXISTS(SELECT 1 FROM WEB_CONTENT.WEB_LINK WHERE WEB_LINK_LABEL = 'Inactive MORE Accounts' AND WEB_LINK_USAGE = 'W');
INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT 
    SELECT WEB_LINK_ID, 1
      FROM WEB_CONTENT.WEB_LINK WL
     WHERE WEB_LINK_LABEL = 'Inactive MORE Accounts' AND WEB_LINK_USAGE = 'W'
       AND NOT EXISTS(SELECT 1 FROM WEB_CONTENT.WEB_LINK_REQUIREMENT  WHERE WEB_LINK_ID = WL.WEB_LINK_ID AND REQUIREMENT_ID = 1);
   
COMMIT;

UPDATE REPORT.REPORT_PARAM
   SET PARAM_VALUE = 'SELECT
	TO_CHAR(X.SETTLE_DATE, ''MM/DD/YYYY HH24:MI:SS'') "Settle Date", X.TRAN_ID "Tran Id", X.TOTAL_AMOUNT "Tran Amount", P.PRICE * P.AMOUNT "Reward Amount", CUR.CURRENCY_CODE "Currency",
	P.VEND_COLUMN "Tran Line Item", TT.TRANS_TYPE_NAME "Tran Type", CT.CARD_NAME "Card Type", X.CARD_NUMBER "Card Number", E.EPORT_SERIAL_NUM "Device Serial #", T.TERMINAL_NBR "Terminal #", C.CUSTOMER_NAME "Customer", L.LOCATION_NAME "Location"
	FROM REPORT.TRANS X
	JOIN REPORT.TRANS_TYPE TT ON X.TRANS_TYPE_ID = TT.TRANS_TYPE_ID
	JOIN REPORT.EPORT E ON X.EPORT_ID = E.EPORT_ID
	JOIN REPORT.PURCHASE P ON X.TRAN_ID = P.TRAN_ID
	LEFT OUTER JOIN REPORT.TERMINAL T ON X.TERMINAL_ID = T.TERMINAL_ID
	LEFT OUTER JOIN CORP.CUSTOMER C ON T.CUSTOMER_ID = C.CUSTOMER_ID
	LEFT OUTER JOIN REPORT.LOCATION L ON T.LOCATION_ID = L.LOCATION_ID
	LEFT OUTER JOIN CORP.CURRENCY CUR ON X.CURRENCY_ID = CUR.CURRENCY_ID
	LEFT OUTER JOIN REPORT.CARDTYPE_AUTHORITY CA ON X.CARDTYPE_AUTHORITY_ID = CA.CARDTYPE_AUTHORITY_ID
	LEFT OUTER JOIN REPORT.CARD_TYPE CT ON CA.CARDTYPE_ID = CT.CARD_TYPE_ID
  JOIN REPORT.VW_USER_TERMINAL UT ON X.TERMINAL_ID = UT.TERMINAL_ID
 WHERE X.SETTLE_DATE >= CAST(? AS DATE) AND X.SETTLE_DATE < CAST(? AS DATE) + 1 AND X.SETTLE_STATE_ID = 3 AND P.TRAN_LINE_ITEM_TYPE_ID = 209
   AND UT.USER_ID = ?
	ORDER BY X.SETTLE_DATE, X.TRAN_ID'
 WHERE REPORT_ID IN(SELECT REPORT_ID FROM REPORT.REPORTS WHERE REPORT_NAME = 'Softcard Discount Transactions')
   AND PARAM_NAME = 'query';

UPDATE REPORT.REPORT_PARAM
   SET PARAM_VALUE = 'beginDate,endDate,userId'
 WHERE REPORT_ID IN(SELECT REPORT_ID FROM REPORT.REPORTS WHERE REPORT_NAME = 'Softcard Discount Transactions')
   AND PARAM_NAME = 'paramNames';
 
UPDATE REPORT.REPORT_PARAM
   SET PARAM_VALUE = 'TIMESTAMP,TIMESTAMP,NUMERIC'
 WHERE REPORT_ID IN(SELECT REPORT_ID FROM REPORT.REPORTS WHERE REPORT_NAME = 'Softcard Discount Transactions')
   AND PARAM_NAME = 'paramTypes';

UPDATE REPORT.REPORT_PARAM
   SET PARAM_NAME = 'params.beginDate'
 WHERE REPORT_ID IN(SELECT REPORT_ID FROM REPORT.REPORTS WHERE REPORT_NAME = 'Softcard Discount Transactions')
   AND PARAM_NAME = 'params.StartDate';

UPDATE REPORT.REPORT_PARAM
   SET PARAM_NAME = 'params.endDate'
 WHERE REPORT_ID IN(SELECT REPORT_ID FROM REPORT.REPORTS WHERE REPORT_NAME = 'Softcard Discount Transactions')
   AND PARAM_NAME = 'params.EndDate';

INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE)
  SELECT REPORT_ID, 'params.userId','{u}'
    FROM REPORT.REPORTS R
   WHERE REPORT_NAME = 'Softcard Discount Transactions'
     AND NOT EXISTS(SELECT 1 FROM REPORT.REPORT_PARAM RP WHERE RP.REPORT_ID = R.REPORT_ID AND RP.PARAM_NAME = 'params.userId');

INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE)
  SELECT REPORT_ID, 'dateFormat', 'MM/dd/yyyy'
    FROM REPORT.REPORTS R
   WHERE REPORT_NAME = 'Softcard Discount Transactions'
     AND NOT EXISTS(SELECT 1 FROM REPORT.REPORT_PARAM RP WHERE RP.REPORT_ID = R.REPORT_ID AND RP.PARAM_NAME = 'dateFormat');

COMMIT;  

UPDATE REPORT.REPORT_PARAM
   SET PARAM_VALUE = 'SELECT DISTINCT
	C.CUSTOMER_NAME "Customer", L.LOCATION_NAME "Location", E.EPORT_SERIAL_NUM "Device", A.AUTHORITY_NAME "Authority", M.MERCHANT_NAME "Merchant Name",
	M.MERCHANT_CD "Merchant ID", TX.TERMINAL_CD "Terminal ID", COALESCE(T.DOING_BUSINESS_AS, C.DOING_BUSINESS_AS) "Doing Business As",
	TO_CHAR(D.LAST_ACTIVITY_TS, ''MM/DD/YYYY HH24:MI:SS'') "Last Activity"
	FROM REPORT.VW_TERMINAL_EPORT TE
	JOIN REPORT.EPORT E ON TE.EPORT_ID = E.EPORT_ID
	JOIN REPORT.TERMINAL T ON TE.TERMINAL_ID = T.TERMINAL_ID
	JOIN CORP.CUSTOMER C ON T.CUSTOMER_ID = C.CUSTOMER_ID
	JOIN REPORT.LOCATION L ON T.LOCATION_ID = L.LOCATION_ID
	JOIN DEVICE.DEVICE_LAST_ACTIVE DLA ON E.EPORT_SERIAL_NUM = DLA.DEVICE_SERIAL_CD
	JOIN DEVICE.DEVICE D ON DLA.DEVICE_ID = D.DEVICE_ID
	JOIN PSS.POS P ON D.DEVICE_ID = P.DEVICE_ID
	JOIN PSS.POS_PTA PP ON P.POS_ID = PP.POS_ID
	JOIN PSS.PAYMENT_SUBTYPE PS ON PP.PAYMENT_SUBTYPE_ID = PS.PAYMENT_SUBTYPE_ID
	JOIN PSS.TERMINAL TX ON PP.PAYMENT_SUBTYPE_KEY_ID = TX.TERMINAL_ID
	JOIN PSS.MERCHANT M ON TX.MERCHANT_ID = M.MERCHANT_ID
	JOIN AUTHORITY.AUTHORITY A ON M.AUTHORITY_ID = A.AUTHORITY_ID
  JOIN REPORT.VW_USER_TERMINAL UT ON T.TERMINAL_ID = UT.TERMINAL_ID
	WHERE PS.PAYMENT_SUBTYPE_TABLE_NAME = ''TERMINAL'' AND PS.PAYMENT_SUBTYPE_CLASS != ''Authority::NOP''
	AND PP.POS_PTA_ACTIVATION_TS < SYSDATE AND (PP.POS_PTA_DEACTIVATION_TS IS NULL OR PP.POS_PTA_DEACTIVATION_TS > SYSDATE)
  AND UT.USER_ID = ?
	ORDER BY C.CUSTOMER_NAME, L.LOCATION_NAME, E.EPORT_SERIAL_NUM'
 WHERE REPORT_ID IN(SELECT REPORT_ID FROM REPORT.REPORTS WHERE REPORT_NAME = 'Device MID Configuration')
   AND PARAM_NAME = 'query';

UPDATE REPORT.REPORT_PARAM
   SET PARAM_VALUE = 'userId'
 WHERE REPORT_ID IN(SELECT REPORT_ID FROM REPORT.REPORTS WHERE REPORT_NAME = 'Device MID Configuration')
   AND PARAM_NAME = 'paramNames';

INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE)
  SELECT REPORT_ID, 'paramTypes', 'NUMERIC'
    FROM REPORT.REPORTS R
   WHERE REPORT_NAME = 'Device MID Configuration'
     AND NOT EXISTS(SELECT 1 FROM REPORT.REPORT_PARAM RP WHERE RP.REPORT_ID = R.REPORT_ID AND RP.PARAM_NAME = 'paramTypes');

INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE)
  SELECT REPORT_ID, 'params.userId','{u}'
    FROM REPORT.REPORTS R
   WHERE REPORT_NAME = 'Device MID Configuration'
     AND NOT EXISTS(SELECT 1 FROM REPORT.REPORT_PARAM RP WHERE RP.REPORT_ID = R.REPORT_ID AND RP.PARAM_NAME = 'params.userId');

COMMIT;  

GRANT SELECT ON FRONT.TIME_ZONE TO USAT_DMS_ROLE;

INSERT INTO FOLIO_CONF.FIELD (FIELD_ID,FIELD_LABEL,FIELD_DESC,DISPLAY_EXPRESSION,SORT_EXPRESSION,DISPLAY_FORMAT,DISPLAY_SQL_TYPE,SORT_SQL_TYPE,IMPORTANCE,FIELD_CATEGORY_ID,CREATED_BY_USER_ID,LAST_UPDATED_BY_USER_ID,ACTIVE_FLAG) 
    VALUES (2814,'DEX File Name',NULL,'G4OP.DEX_FILE.FILE_NAME','UPPER(G4OP.DEX_FILE.FILE_NAME)',NULL,'VARCHAR','VARCHAR',50,518,7,7,'Y');

INSERT INTO FOLIO_CONF.FIELD_PRIV(FIELD_ID, FILTER_GROUP_ID, USER_GROUP_ID)
    SELECT 2814, NULL, USER_GROUP_ID
      FROM FOLIO_CONF.USER_GROUP UG
     WHERE USER_GROUP_ID NOT IN(SELECT USER_GROUP_ID FROM FOLIO_CONF.FIELD_PRIV WHERE FIELD_ID = 2814)
       AND USER_GROUP_ID IN(1,2,3);

INSERT INTO FOLIO_CONF.FIELD_PRIV(FIELD_ID, FILTER_GROUP_ID, USER_GROUP_ID)
    SELECT 2814, 1, USER_GROUP_ID
      FROM FOLIO_CONF.USER_GROUP UG
     WHERE USER_GROUP_ID NOT IN(SELECT USER_GROUP_ID FROM FOLIO_CONF.FIELD_PRIV WHERE FIELD_ID = 2814)
       AND USER_GROUP_ID IN(4,5,8);

COMMIT;

ALTER TABLE DEVICE.DEVICE ADD(LAST_CLIENT_IP_ADDRESS VARCHAR2(40));

GRANT UPDATE ON PSS.CONSUMER to USALIVE_APP_ROLE;

GRANT SELECT ON REPORT.TIME_ZONE TO DEVICE;

ALTER TABLE PSS.CONSUMER_ACCT_REPLENISH
ADD (EXPIRATION_DATE DATE,
EXP_NOTIFY_DATE DATE);

INSERT INTO PSS.CONSUMER_SETTING_PARAM(CONSUMER_SETTING_PARAM_ID,DISPLAY_CONFIGURABLE_CD,DISPLAY_LABEL,DISPLAY_GROUP,EDITOR,CONVERTER,CONSUMER_COMMUNICATION_TYPE, DEFAULT_VALUE) 
    VALUES(10 ,'P', 'Contact Me When Replenish Credit Card is about to expire', 'Preferences', 'CHECKBOX:?Y=', null, 'CARD_EXPIRATION','N');
COMMIT;

GRANT SELECT ON PSS.CONSUMER_ACCT_REPLENISH to USAT_RPT_REQ_ROLE;
GRANT SELECT ON PSS.CONSUMER_ACCT to USAT_RPT_REQ_ROLE;
GRANT UPDATE ON PSS.CONSUMER_ACCT_REPLENISH to USAT_RPT_REQ_ROLE;

DROP PROCEDURE REPORT.USER_LOGIN_DEL; 

CREATE OR REPLACE FORCE VIEW CORP.VW_ORF_JPMC_EFT_EXCEPTIONS AS
SELECT * FROM apps.usat_jpmc_returns_corrections@financials;

GRANT SELECT ON CORP.VW_ORF_JPMC_EFT_EXCEPTIONS TO USAT_RPT_GEN_ROLE;

ALTER SESSION SET CURRENT_SCHEMA = REPORT;
DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := -1;
BEGIN
	SELECT MAX(REPORT_ID) INTO LN_REPORT_ID
	FROM REPORT.REPORTS WHERE REPORT_NAME = 'Payment Exceptions';
	
	IF LN_REPORT_ID > 0 THEN
		RETURN;
	END IF;

	SELECT REPORT.REPORTS_SEQ.NEXTVAL INTO LN_REPORT_ID FROM DUAL;
	
INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
VALUES( LN_REPORT_ID, 'Payment Exceptions - {b} to {e}', 6, 0, 'Payment Exceptions', 'Report that shows bank account rejections and other payment exceptions', 'E', 0);
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( LN_REPORT_ID, 'query', 'select ojee.pymt_bank_name "Bank Name", substr(ojee.pymt_bank_account_num, -4) "Account # Last 4", 
ojee.payment_reference_number "Payment Reference #", c.customer_name "Customer", d.sent_date "Payment Report Date", d.ref_nbr "Reference #", 
d.total_amount "Total Amount", ojee.jpmc_return_date "Bank Return Date", ojee.jpmc_return_comments "Bank Return Comments"
from corp.vw_orf_jpmc_eft_exceptions ojee
join corp.doc d on ojee.invoice_num = d.ref_nbr
join corp.customer_bank cb on d.customer_bank_id = cb.customer_bank_id
join corp.customer c on cb.customer_id = c.customer_id
join report.vw_user_customer_bank ucb on cb.customer_bank_id = ucb.customer_bank_id
where ojee.jpmc_return_date >= CAST(? AS DATE) and ojee.jpmc_return_date < CAST(? AS DATE) + 1 and ucb.user_id = ? and ojee.jpmc_return_code like ''R%'' and ojee.amount_to_be_paid > 0
order by ojee.jpmc_return_date desc');

insert into report.report_param values(LN_REPORT_ID, 'paramNames','StartDate,EndDate,UserId');
insert into report.report_param values(LN_REPORT_ID, 'paramTypes','TIMESTAMP,TIMESTAMP,NUMBER');
insert into report.report_param values(LN_REPORT_ID, 'params.StartDate','{b}');
insert into report.report_param values(LN_REPORT_ID, 'params.EndDate','{e}');
insert into report.report_param values(LN_REPORT_ID, 'params.UserId','{u}');
insert into report.report_param values(LN_REPORT_ID, 'isSQLFolio','true');
insert into report.report_param values(LN_REPORT_ID, 'dateFormat','MM/dd/yyyy');

insert into web_content.web_link values(web_content.seq_web_link_id.nextval, 'Payment Exceptions', './select_date_range_frame_sqlfolio.i?basicReportId='||LN_REPORT_ID, 'Payment Exceptions', 'Daily Operations', 'W', 0);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 1);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 6);
commit;

END;
/

UPDATE REPORT.REPORTS SET USAGE = 'E' WHERE REPORT_NAME = 'Declined MORE Authorizations';
COMMIT;

CREATE INDEX PSS.IDX_CONSUMER_CRED_ACTIV_TS ON PSS.CONSUMER_CREDENTIAL(ACTIVATED_TS) TABLESPACE PSS_INDX ONLINE;

DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := -1;
BEGIN
	SELECT MAX(REPORT_ID) INTO LN_REPORT_ID
	FROM REPORT.REPORTS WHERE REPORT_NAME = 'MORE Activations';
	
	IF LN_REPORT_ID > 0 THEN
		RETURN;
	END IF;

	SELECT REPORT.REPORTS_SEQ.NEXTVAL INTO LN_REPORT_ID FROM DUAL;
	
INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
VALUES( LN_REPORT_ID, 'MORE Activations - {b} to {e}', 6, 0, 'MORE Activations', 'Report that shows new MORE account activations', 'E', 0);
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( LN_REPORT_ID, 'query', 'SELECT CUS.CUSTOMER_NAME "Operator Name", R.REGION_NAME "Region Name", C.CONSUMER_FNAME "First Name", C.CONSUMER_LNAME "Last Name", CA.CONSUMER_ACCT_IDENTIFIER "Card Id", 
CA.CONSUMER_ACCT_BALANCE "Account Balance", CA.NICK_NAME "Card Nickname", CA.CONSUMER_ACCT_CD "Card Number", C.CONSUMER_EMAIL_ADDR1 "Email Address", C.CONSUMER_CELL_PHONE_NUM "Mobile Phone Number",
CA.CONSUMER_ACCT_ACTIVE_YN_FLAG "Account Enabled", DBADMIN.UTC_TO_LOCAL_DATE(CC.CREATED_UTC_TS) "Registration Time", CC.CREDENTIAL_ACTIVE_FLAG "Account Activated", 
CC.ACTIVATED_TS "Activation Time", CAS.CONSUMER_ACCT_SUB_TYPE_DESC "Account Sub Type", CA.CONSUMER_ACCT_PROMO_TOTAL "Promo Total",
CA.CONSUMER_ACCT_REPLENISH_TOTAL "Replenish Total", CA.REPLENISH_BONUS_TOTAL "Replenish Bonus Total", 
CA.LOYALTY_DISCOUNT_TOTAL "Loyalty Discount Total", CA.CASH_BACK_TOTAL "Cashback Total", 
RT.REPLENISH_TYPE_DESC "Replenish Type", CAR.REPLENISH_AMOUNT "Replenish Amount", CAR.REPLENISH_THRESHHOLD "Replenish Threshold", 
CAR.REPLENISH_CARD_MASKED "Replenish Card", CAR.LAST_REPLENISH_TRAN_TS "Last Replenish Time",
CA.CONSUMER_ACCT_REPLENISH_TOTAL+CA.CONSUMER_ACCT_PROMO_TOTAL-CA.CONSUMER_ACCT_BALANCE "Total Tran Amount",
CA.TRAN_COUNT_TOTAL "Total Tran Count"
FROM PSS.CONSUMER C
JOIN PSS.CONSUMER_ACCT CA ON C.CONSUMER_ID = CA.CONSUMER_ID
JOIN PSS.CONSUMER_ACCT_SUB_TYPE CAS ON CA.CONSUMER_ACCT_SUB_TYPE_ID = CAS.CONSUMER_ACCT_SUB_TYPE_ID
JOIN PSS.CONSUMER_CREDENTIAL CC ON C.CONSUMER_ID = CC.CONSUMER_ID
JOIN CORP.CUSTOMER CUS ON CA.CORP_CUSTOMER_ID = CUS.CUSTOMER_ID
LEFT OUTER JOIN REPORT.REGION R ON C.REGION_ID = R.REGION_ID
LEFT OUTER JOIN PSS.CONSUMER_ACCT_REPLENISH CAR ON CA.CONSUMER_ACCT_REPLENISH_ID = CAR.CONSUMER_ACCT_REPLENISH_ID
LEFT OUTER JOIN PSS.REPLENISH_TYPE RT ON CAR.REPLENISH_TYPE_ID = RT.REPLENISH_TYPE_ID
JOIN REPORT.VW_USER_CONSUMER_ACCT VUCA ON VUCA.CONSUMER_ACCT_ID=CA.CONSUMER_ACCT_ID
JOIN REPORT.USER_LOGIN UL ON UL.USER_ID = VUCA.USER_ID 
WHERE CA.CONSUMER_ACCT_TYPE_ID = 3 AND CC.ACTIVATED_TS >= CAST(? AS DATE) AND CC.ACTIVATED_TS < CAST(? AS DATE) + 1
AND UL.USER_ID = ?
ORDER BY CUS.CUSTOMER_NAME, C.CONSUMER_FNAME, C.CONSUMER_LNAME');

insert into report.report_param values(LN_REPORT_ID, 'paramNames','StartDate,EndDate,UserId');
insert into report.report_param values(LN_REPORT_ID, 'paramTypes','TIMESTAMP,TIMESTAMP,NUMBER');
insert into report.report_param values(LN_REPORT_ID, 'params.StartDate','{b}');
insert into report.report_param values(LN_REPORT_ID, 'params.EndDate','{e}');
insert into report.report_param values(LN_REPORT_ID, 'params.UserId','{u}');
insert into report.report_param values(LN_REPORT_ID, 'isSQLFolio','true');
insert into report.report_param values(LN_REPORT_ID, 'dateFormat','MM/dd/yyyy');

insert into web_content.web_link values(web_content.seq_web_link_id.nextval, 'MORE Activations', './select_date_range_frame_sqlfolio.i?basicReportId='||LN_REPORT_ID||'&' ||'reportTitle=MORE+Activations', 'MORE Activations', 'Daily Operations', 'W', 0);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 1);
commit;

END;
/

UPDATE WEB_CONTENT.WEB_LINK
   SET WEB_LINK_URL = REGEXP_REPLACE(WEB_LINK_URL, '((\?)|&)reportTitle=[^&]*', '\2')
 WHERE WEB_LINK_URL LIKE '%reportTitle=%'
   AND WEB_LINK_USAGE = 'W';
   
COMMIT;

GRANT SELECT ON REPORT.VW_ADMIN_USER TO USALIVE_APP_ROLE;
GRANT EXECUTE ON DEVICE.UPDATE_GPRS_FOR_DEVICE TO USAT_APP_LAYER_ROLE;
CREATE PUBLIC SYNONYM UPDATE_GPRS_FOR_DEVICE FOR DEVICE.UPDATE_GPRS_FOR_DEVICE;

INSERT INTO DEVICE.HOST_TYPE(HOST_TYPE_ID, HOST_TYPE_DESC, HOST_DEFAULT_COMPLETE_MINUT)
    SELECT 100, 'ePort Beacon Module', 0
      FROM DUAL
     WHERE NOT EXISTS(SELECT 1 FROM DEVICE.HOST_TYPE WHERE HOST_TYPE_ID = 100);
     
COMMIT;

GRANT SELECT ON ENGINE.APP_SETTING TO USAT_PREPAID_APP_ROLE;

insert into engine.app_setting(app_setting_cd, app_setting_value, app_setting_desc)
values('MORE_ALLOW_SIMPLE_SIGNUP', 'N', 'Setting to display simple signup option or not on MORE website.''Y'' will allow simple signup, ''N'' will only support regular signup');
commit;

INSERT INTO DEVICE.BEZEL_MFGR(HOST_EQUIPMENT_MFGR, ENTRY_CAPABILITY_CDS)
   SELECT 'MEI 4-in-1+', 'SPE'
     FROM DUAL 
    WHERE NOT EXISTS(SELECT 1 FROM DEVICE.BEZEL_MFGR WHERE HOST_EQUIPMENT_MFGR = 'MEI 4-in-1+');
            
COMMIT;
