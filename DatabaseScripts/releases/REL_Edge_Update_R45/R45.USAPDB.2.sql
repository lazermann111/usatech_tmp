DECLARE
    CURSOR l_cur IS
        SELECT T.TERMINAL_ID, MAX(F.FILL_DATE) LAST_FILL_DATE 
          FROM REPORT.TERMINAL T
          LEFT OUTER JOIN REPORT.FILL F ON T.TERMINAL_ID = F.TERMINAL_ID
         WHERE T.PAYMENT_SCHEDULE_ID = 2  
         GROUP BY T.TERMINAL_ID
         ORDER BY T.TERMINAL_ID;
    ln_batch_id CORP.BATCH_TOTAL.BATCH_ID%TYPE;
BEGIN
    FOR l_rec IN l_cur LOOP 
        FOR l_detail_rec IN (
          SELECT MAX(AR.TRAN_DATE) TRAN_DATE, COUNT(*) CNT, SUM(AR.TOTAL_AMOUNT) AMT, X.CUSTOMER_BANK_ID, AR.CURRENCY_ID
            FROM REPORT.ACTIVITY_REF AR
            JOIN REPORT.TRANS X ON AR.TRAN_ID = X.TRAN_ID
            JOIN REPORT.TRANS_TYPE TT ON AR.TRANS_TYPE_ID = TT.TRANS_TYPE_ID
           WHERE TT.CASH_IND = 'N'
             AND TT.PAYABLE_IND = 'N'
             AND AR.FILL_DATE IS NULL
             AND AR.TERMINAL_ID = l_rec.TERMINAL_ID
             AND AR.TRAN_DATE >= l_rec.LAST_FILL_DATE
           GROUP BY X.CUSTOMER_BANK_ID, AR.CURRENCY_ID) LOOP
            IF l_detail_rec.CNT > 0 THEN
                ln_batch_id := CORP.PAYMENTS_PKG.GET_OR_CREATE_BATCH(l_rec.TERMINAL_ID, l_detail_rec.CUSTOMER_BANK_ID, l_detail_rec.TRAN_DATE, l_detail_rec.CURRENCY_ID, 'N');
                LOOP
                    UPDATE CORP.BATCH_TOTAL
                       SET LEDGER_AMOUNT = l_detail_rec.AMT,
                           LEDGER_COUNT = l_detail_rec.CNT
                     WHERE BATCH_ID = ln_batch_id
                       AND ENTRY_TYPE = 'NP';
                    EXIT WHEN SQL%ROWCOUNT > 0;
                    BEGIN
                        INSERT INTO CORP.BATCH_TOTAL(BATCH_ID, ENTRY_TYPE, PAYABLE, LEDGER_AMOUNT, LEDGER_COUNT)
                        VALUES(ln_batch_id,'NP','N', l_detail_rec.AMT, l_detail_rec.CNT); 
                        EXIT;    
                    EXCEPTION
                        WHEN DUP_VAL_ON_INDEX THEN
                            NULL;
                    END;
                END LOOP;
            END IF;
            COMMIT;
            DBMS_OUTPUT.PUT_LINE('Updated non-payable batch totals for terminal ' || l_rec.TERMINAL_ID || ', bank ' || l_detail_rec.CUSTOMER_BANK_ID || ', currency ' || l_detail_rec.CURRENCY_ID
            || ' to $' || l_detail_rec.AMT);
        END LOOP;
        DBMS_OUTPUT.PUT_LINE('Processed terminal ' || l_rec.TERMINAL_ID);
    END LOOP;
END;
/

DECLARE
    ln_min_refund_id CORP.REFUND.REFUND_ID%TYPE;
    ln_max_refund_id CORP.REFUND.REFUND_ID%TYPE;
BEGIN
    SELECT MIN(r.REFUND_ID), MAX(r.REFUND_ID) + 1
      INTO ln_min_refund_id, ln_max_refund_id
      FROM CORP.REFUND R
     WHERE R.PROCESSED_FLAG = 'Y';
    WHILE ln_min_refund_id < ln_max_refund_id LOOP
        UPDATE REPORT.TRANS X
           SET SOURCE_TRAN_ID = (SELECT SX.TRAN_ID FROM PSS.TRAN SX WHERE X.MACHINE_TRANS_NO = SX.TRAN_GLOBAL_TRANS_CD)
         WHERE SOURCE_TRAN_ID IS NULL
           AND X.TRAN_ID IN(
             SELECT R.TRAN_ID 
               FROM CORP.REFUND R
              WHERE R.PROCESSED_FLAG = 'Y' 
                AND R.REFUND_ID >= ln_min_refund_id
                AND R.REFUND_ID < ln_min_refund_id + 5000);
        COMMIT;
        ln_min_refund_id := ln_min_refund_id + 5000;
    END LOOP;
END;
/
   