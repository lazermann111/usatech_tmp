insert into corp.offer (offer_id, offer_name, offer_desc, status_cd, public_signup_page, usalive_signup_page)
values(7, 'MORE Loyalty and Rewards Spring Specials 2015', 'MORE Loyalty and Rewards Spring Specials 2015 with no monthly fee','A', 'more_spring_special_order.html','more_spring_special_order.html');
commit;

alter table corp.interest
add (
EPORT_MOBILE_QUANTITY VARCHAR2(10 BYTE),
NEED_EPORT_ONLINE_SIGNUP CHAR(1) DEFAULT 'N',
SEND_ME_MORE_INFO CHAR(1) DEFAULT 'N',
TOTAL_QUANTITY VARCHAR2(10 BYTE)
);


