UPDATE PSS.POS_PTA set pos_pta_deactivation_ts =  sysdate
where payment_subtype_id in (select payment_subtype_id from pss.payment_subtype where upper(payment_subtype_name) like upper('%softcard%'))
and NVL(pos_pta_deactivation_ts, MAX_DATE)>sysdate;
commit;

DECLARE
	ln_pos_pta_tmpl_id NUMBER;
BEGIN
	select distinct(ppte.pos_pta_tmpl_id) into ln_pos_pta_tmpl_id from PSS.pos_pta_tmpl ppt join pss.pos_pta_tmpl_entry ppte on ppt.pos_pta_tmpl_id=ppte.pos_pta_tmpl_id
where ppte.payment_subtype_id in (select payment_subtype_id from pss.payment_subtype where upper(payment_subtype_name) like upper('%softcard%'));

delete from pss.pos_pta_tmpl_entry 
where pos_pta_tmpl_id =ln_pos_pta_tmpl_id;

delete from PSS.pos_pta_tmpl where pos_pta_tmpl_id =ln_pos_pta_tmpl_id;

commit;
END;
/



