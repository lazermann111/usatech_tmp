alter table
   corp.fees modify
(
   FEE_AMOUNT_UBN    NUMBER(7,2)
);

insert into corp.fees 
select 13,'Quick Start TPC Payment', null,2,'I',130,'A', 'N',35000,0,0 from dual
where not exists(select 1 from corp.fees where fee_id=13);

insert into corp.fees 
select 14,'Quick Start Tax', null,2,'I',140,'A', 'N',35000,0,0 from dual
where not exists(select 1 from corp.fees where fee_id=14);

update corp.fees set FEE_AMOUNT_UBN=35000 where fee_id=7;
commit;