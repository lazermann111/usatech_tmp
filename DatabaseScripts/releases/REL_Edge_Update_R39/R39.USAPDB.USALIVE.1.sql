-- to allow login as customer to do activate terminal and manage campaign
delete from WEB_CONTENT.web_link_requirement where web_link_id=(select web_link_id from WEB_CONTENT.web_link where web_link_label = 'Manage Campaign' and web_link_group ='Setup') and requirement_id=22;
delete from WEB_CONTENT.web_link_requirement where web_link_id=(select web_link_id from WEB_CONTENT.web_link where web_link_label = 'Activate Device' and web_link_group ='Setup') and requirement_id=22;

insert into WEB_CONTENT.requirement values(35,'REQ_INTERNAL_LOGIN_AS_CUST', null, 'com.usatech.usalive.link.InternalUserLoginAsCustomerRequirement', null, null);

insert into WEB_CONTENT.web_link_requirement
select web_link_id,35 from WEB_CONTENT.web_link where web_link_label = 'Manage Campaign' and web_link_group ='Setup';

insert into WEB_CONTENT.web_link_requirement
select web_link_id,35 from WEB_CONTENT.web_link where web_link_label = 'Activate Device' and web_link_group ='Setup';

commit;

-- PKG_APP_USER.pbk