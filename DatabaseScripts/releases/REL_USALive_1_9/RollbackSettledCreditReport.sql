-- remove from report register
delete from report.report_param where report_id=(select report_id from report.reports where report_name='Settled Credit Export');
delete from report.user_report where report_id=(select report_id from report.reports where report_name='Settled Credit Export');
delete from report.reports where report_name='Settled Credit Export';
commit;

-- remove from web_link
delete from web_content.web_link where web_link_label='Settled Credit Export (CSV)';
commit;