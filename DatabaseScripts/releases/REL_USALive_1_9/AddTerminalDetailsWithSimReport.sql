SET DEFINE OFF;
ALTER SESSION SET CURRENT_SCHEMA = REPORT;   
DECLARE
	l_env VARCHAR2(3); 
	l_user_id NUMBER;
BEGIN

select NVL(MAX(DECODE(substr(subdomain_url, length('hotchoice') + 1, length(subdomain_url) - length('hotchoice.usatech.com')), '-ecc', 'ECC', '-dev', 'DEV', '-int', 'INT', null, 'PRD')), 'DEV')
  into l_env
  from WEB_CONTENT.subdomain where subdomain_url like 'hotchoice%.usatech.com'
  and subdomain_url not IN('hotchoice-PRD.usatech.com','hotchoice-USA.usatech.com') ;
  
  IF l_env = 'INT' THEN
  	select user_id into l_user_id from report.user_login where user_name='aroyce@usatech.com';
  ELSIF l_env = 'ECC' THEN
  	select user_id into l_user_id from report.user_login where user_name='aroyce@usatech.com';
  ELSIF l_env = 'PRD' THEN
  	select user_id into l_user_id from report.user_login where user_name='gharrum@usatech.com';
  ELSE
  	select user_id into l_user_id from report.user_login where user_name='aroyce@usatech.com';
  END IF;
  
INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
VALUES( REPORT.REPORTS_SEQ.NEXTVAL, 'Terminal Details With Sim-Id/MEID As Of {e}', 6, 0, 'Terminal Details With Sim-Id/MEID (CSV)', 'Terminal details report that also includes Sim-id or MEID', 'N', l_user_id);

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( REPORT.REPORTS_SEQ.CURRVAL, 'directiveNames', 'show-header');
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( REPORT.REPORTS_SEQ.CURRVAL, 'directiveValues', 'false');
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( REPORT.REPORTS_SEQ.CURRVAL, 'folioId', '1208');
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( REPORT.REPORTS_SEQ.CURRVAL, 'is_custom_report', 'true');
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( REPORT.REPORTS_SEQ.CURRVAL, 'outputType', '21');
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( REPORT.REPORTS_SEQ.CURRVAL, 'params.is_custom_report', 'true');
    
commit;

END;
/