-- add it to Daily Operations too
SET DEFINE OFF;
insert into web_content.web_link values(web_content.seq_web_link_id.nextval, 'Settled Credit Export (CSV)', './select_date_range_frame.i?folioId=1199&outputType=21&startParamName=params.beginDate&endParamName=params.endDate','Settled credit export report','Daily Operations','W', 0);
commit;