ALTER TABLE
   CORP.CUSTOMER_BANK
DROP (ACCOUNT_BALANCE, SWIFT_CODE);

-- remove Dex Status report link in usalive
delete from web_content.web_link_requirement where web_link_id = (select web_link_id from web_content.web_link where web_link_label='Dex Status');
delete from web_content.web_link where web_link_label='Dex Status';
commit;

-- remove Activity By Fill Date report in report register folio 1136
delete from report.report_param where report_id=(select report_id from report.reports where report_name='Activity By Fill Date');
delete from report.user_report where report_id=(select report_id from report.reports where report_name='Activity By Fill Date');
delete from report.reports where report_name='Activity By Fill Date';
commit;

update report.reports set report_name='Payment Item Export' where report_name='Compass Payment Item Export' and USAGE='N';
update report.reports set report_name='Daily Item Export' where report_name='Compass Daily Item Export' and USAGE='N';
commit;

delete from report.ccs_transport_status where ccs_transport_status_cd='R';
commit;

ALTER TABLE
   REPORT.CCS_TRANSPORT_ERROR
DROP CONSTRAINT
   FK_CCS_TRANSPORT_ERROR_TYPE_ID;
   
ALTER TABLE
   REPORT.CCS_TRANSPORT_ERROR
DROP (RETRY_COUNT,ERROR_TYPE_ID);

DROP TABLE REPORT.CCS_TRANSPORT_ERROR_TYPE;

delete from web_content.literal where literal_key='ct-enable-failure';
delete from web_content.literal where literal_key='ct-enable-success';
commit;

ALTER TABLE
   REPORT.TRANS
DROP (CREATED_BY, LAST_UPDATED_BY);

ALTER TABLE
   CORP.LEDGER
DROP (UPDATE_DATE, LAST_UPDATED_BY);

update folio_conf.output_type set output_type_key='direct:com.usatech.report.custom.USALiveXHTMLGenerator' where output_type_id =22;
update folio_conf.output_type set output_type_key='direct:com.usatech.report.custom.USALiveXHTMLGenerator:chart=true' where output_type_id=28;
commit;