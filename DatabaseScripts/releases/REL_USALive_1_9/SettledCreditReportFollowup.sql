-- add settled_credit_export report
INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
VALUES( REPORT.REPORTS_SEQ.NEXTVAL, 'Settled Credit Export - {b} to {e}', 6, 0, 'Settled Credit Export', 'Settled credit transaction export that contains region, location, asset #, device, transaction date, card type, sale amount, and process fee. NOTE: the report will retrieve maximum of 500000 records.', 'E', 0);

insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'folioId','1199');
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'outputType','21');
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'params.beginDate','{b}');
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'params.endDate','{e}');
commit;

update folio_conf.folio set max_rows=500000 where folio_id=1199;
commit;