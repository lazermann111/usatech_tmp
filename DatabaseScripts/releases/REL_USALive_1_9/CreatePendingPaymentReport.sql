-- Create report folio Pending Payment after the folio is created:

insert into FOLIO_CONF.report (report_id, report_name) values(5, 'Pending Payment');

insert into FOLIO_CONF.report_folio(report_folio_id, report_id, folio_id, folio_index)
values(folio_conf.seq_report_folio_id.nextval, 5, 1107, 1);
insert into FOLIO_CONF.report_folio(report_folio_id, report_id, folio_id, folio_index)
values(folio_conf.seq_report_folio_id.nextval, 5, 1103, 2);
insert into FOLIO_CONF.report_folio(report_folio_id, report_id, folio_id, folio_index)
values(folio_conf.seq_report_folio_id.nextval, 5, 1110, 3);
insert into FOLIO_CONF.report_folio(report_folio_id, report_id, folio_id, folio_index)
values(folio_conf.seq_report_folio_id.nextval, 5, 1099, 4);
insert into FOLIO_CONF.report_folio(report_folio_id, report_id, folio_id, folio_index)
values(folio_conf.seq_report_folio_id.nextval, 5, 1112, 5);
insert into FOLIO_CONF.report_folio(report_folio_id, report_id, folio_id, folio_index)
values(folio_conf.seq_report_folio_id.nextval, 5, 1114, 6);

commit;