DECLARE
  CURSOR l_customer_bank IS
   select customer_bank_id from corp.customer_bank;
  l_account_balance NUMBER;
  l_lock VARCHAR2(128);

BEGIN

    FOR l_rec IN l_customer_bank LOOP
    	l_lock := GLOBALS_PKG.REQUEST_LOCK('DOC.CUSTOMER_BANK_ID',l_rec.customer_bank_id);
      	select NVL(sum(l.amount),0) into l_account_balance from corp.ledger l join corp.batch b on l.batch_id=b.batch_id
		join corp.doc partition(DOC_DO_NEVER_DROP) d on b.doc_id=d.doc_id
		where d.customer_bank_id=l_rec.customer_bank_id
		and l.deleted ='N' and l.settle_state_id in (2,3,6) and d.currency_id is not null;
		update corp.customer_bank set account_balance = l_account_balance where customer_bank_id=l_rec.customer_bank_id;
		commit;
    END LOOP; 
  
END;
/