ALTER TABLE
   CORP.CUSTOMER_BANK
ADD
   (
      	ACCOUNT_BALANCE NUMBER(21,8) default 0 NOT NULL,
      	SWIFT_CODE VARCHAR2(12)
   );
   
UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_URL = REPLACE(WEB_LINK_URL, '%2FCustomerReporting', '')
WHERE WEB_LINK_URL LIKE '%%2FCustomerReporting%';

COMMIT;

insert into web_content.web_link values(web_content.seq_web_link_id.nextval,'Dex Status', './dex_status_parameters.i','Dex Status Report', 'Reports','M',141);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval,1);
commit;

insert into web_content.literal (literal_id, literal_key, literal_value) 
values(web_content.seq_literal_id.nextval, 'dex-invalid-profile', 'The current login may not view this Dex file.');
insert into web_content.literal (literal_id, literal_key, literal_value) 
values(web_content.seq_literal_id.nextval, 'customer-name-taken', 'This Company Name is already in use. Please choose a different Company Name');
commit;

INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
VALUES( REPORT.REPORTS_SEQ.NEXTVAL, 'Activity By Fill Date - {b} to {e}', 6, 0, 'Activity By Fill Date', 'Report shows transactions amount by currency,region,device,location,asset nbr and terminal during the fill begin date and fill end date.', 'E', 0);

insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'folioId','1136');
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'outputType','22');
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'params.beginDate','{b}');
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'params.endDate','{e}');
commit;

CREATE UNIQUE INDEX corp.udx_customer_name ON corp.customer (DECODE(status, 'D', NULL, UPPER(TRIM(customer_name)))) TABLESPACE CORP_INDX;

-- NEW DEX EA Codes
SET SCAN OFF;
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EA',5,'Coin Mechanism - General non-specific Coin Mechanism fault.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EAA',5,'Coin Entry Chute - A jam has occurred in the coin entry chute of the machine');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EAB',5,'Reject System or Change Return Cup - A problem has occurred with the payout at the mech/machine interface.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EAD',5,'Jammed Validator - A jam has occurred in the coin recognition part of the coin mech.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EAE',5,'Jammed Separator - A jam has occurred in the routing mechanism part of the mech.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EAF',5,'Jammed Dispenser - A jam has occurred in coin payout part of the mech.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EAG',5,'Jammed Changegiver - A jam has occurred in an indeterminate part of the changer');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EAH',5,'Reject Chute - A jam has occurred in the reject chute under the coin mech.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EAI',5,'Cash Box - A jam has occurred in the entry to the cash box.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EAO',5,'Failed Control PCB - Coin mech control PCB has failed.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EAP',5,'Failed Power Supply - Coin mech power supply has failed.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EAR',5,'Comm error - A comms error has occurred between mech and VMC (or peripheral).');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EB',7,'Cup System - General non-specific Cup Mechanism fault.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EBE',7,'Cup Transfer Fault or Cup Chute - Cups failing to arrive in dispense area');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EBJ',7,'No Cups - No Cups in Carousel / Turret. If multiple cup sizes / turrets used, indicate as EBJ_x where x is equal to size / turrect number (i.e.EBJ_2)');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EC',7,'Control System - General non-specific Control System fault.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*ECD',7,'Control PCB - A malfunction has been detected in the control board.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*ECU',7,'Battery - The backup battery is low, missing, or not charging.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EC1B',7,'Selection Panel / switch / mechanism - A malfunction has been detected in the selection panel / switch / mechanism.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EC1C',6,'Temperature Sensor Error - Temperature sensor is defective, disconnected or reading is out of range');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EC1G',7,'Product Delivery PCB - A malfunction has been detected in the product delivery board.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*ED',7,'Hot Drinks System - General non-specific Hot Drinks System fault.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EDK',7,'Water Heater Tank - Fault on boiler (water heater tank)');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EDL',7,'Heater Fuse - The boiler (heater element) fuse is blown.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EDM',7,'Thermostat - Faulty boiler control thermostat');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EDN',7,'H.T. Cut Out - Cut out operated because of an overboil situation.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EDP',7,'Level Control - Water Level control in the heater tank faulty');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EE',7,'Brewer Unit - General non-specific Brewer/Espresso Unit fault.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EEA',7,'Brewer Motor - Fault on Motor for brewer');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EEB',7,'Scraper Motor - Fault on Motor for brewer scraper');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EEC',7,'Filter Plate - Filter plate blocked or changed or cleaned.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EED',7,'Filter Paper - No filter paper or faulty filter paper.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EEE',7,'Scraper Arm - Fault on brewer scraper arm.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EEF',7,'Brew Chamber - Damaged brew chamber.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EEG',7,'Piston - Damaged piston in brewer');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EEH',7,'Timer - Fault in Timer unit for Brewer/Espresso unit');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EEI',7,'PCB - Fault on PCB for brewer/espresso unit');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EEJ',7,'Scraper Switch - Faulty scraper switch.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EEK',7,'Drive Mechanism - Fault on drive mechanism for Brewer/Espresso unit.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EEL',7,'Seals - Leakage in brewer/espresso unit.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EEV',7,'Modifications - Modification made to Brewer/Espresso Unit');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EEW',7,'No Fault Found - No Brewer/Espresso Unit fault could be found.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EEX',7,'Client Induced - Client induced Brewer/Espresso Unit fault.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EEY',7,'Service Induced - Service induced Brewer/Espresso Unit fault.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EEZ',7,'Other Unlisted Fault - Other undefined Brewer/Espresso Unit fault.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EF',7,'Water System - General non-specific Water System fault.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EFA',7,'Inlet Pipe - Fault on water connecting hose');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EFB',7,'Water Filter - Water needs to be changed/has been changed.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EFC',7,'Inlet Valve - Faulty Inlet valve');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EFD',7,'Non-return Valve Delivery Valve - Fault on non-return.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EFE',7,'Regulator - Faulty Water pressure regulator');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EFF',7,'Tank - Header tank fault');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EFG',7,'Float System  - Error in the water level detection system.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EFH',7,'Vend Valve - Faulty dispense valve');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EFI',7,'Vend Valve Scale - Valve needs descaling or has been descaled');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EFJ',7,'Hose / Joint Leak - Water leakage in hose or joint.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EFK',7,'Probes/holder - Fault on water level probe assembly');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EFL',7,'Water Supply Failure - No water (could be too low water pressure)');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EFM',7,'Manifold - Fault on water delivery Manifold or Branch pipe');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EFN',7,'Water Pump - Faulty water pump head');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EFO',7,'Pump Motor - Faulty water pump motor');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EFP',7,'Water Quality - Water quality issue reported');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EFV',7,'Modifications - Modifications made to the Water System');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EFW',7,'No Fault Found - No Water System fault could be found.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EFX',7,'Client Induced - Client induced Water System fault.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EFY',7,'Service Induced - Service induced Water System fault.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EFZ',7,'Other Unlisted Fault - Other undefined Water System fault.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EGN',7,'Waste Full - If required specify waste container number, i.e. EGN_2');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EGU',7,'Vend Door - Fault on the product delivery/vend door. This may include a solenoid, motor, mechanical binding, or inability to reach the desired position. This includes side moving doors and other internal doors but excludes the delivery assembly door at the bottom of a snack machine (see EJG).');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EJ',7,'Food, Snack, or Can/Bottle System  - General non-specific Food & Snack System fault.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EJB',7,'Dispense Motor - Errors exist on a product dispense motor which may include motor jammed, motor not home, coupling error, defective motor, or wiring error.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EJH',7,'Health Timer - The Health rules have been violated for the temperature controlled section. Dependant upon the regional rules, the food section may be Out of Service (food may not be served)');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EJJ',7,'Refrigeration - The cooling unit does not cool down to the pre-set temperature or the unit has frozen up');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EJK',7,'Thermostat - The Thermostat is defective.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EJL',7,'Product delivery detector error - The product delivery detection system has an error.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EJM',7,'Product Delivery System - An error has been detected in the product delivery system, e.g. elevator, arm, conveyor belt, etc.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EN',4,'Bill Validator - General non-specific Bill Validator fault.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*ENA',4,'Bill Entry - The bill entry path is blocked.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*ENB',4,'Detecting Frauds - Excessive frauds are being detected.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*ENC',4,'Jammed Validator - The validator recognition path is jammed.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*END',4,'Jammed Stacker - The stacker mechanism is jammed.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*ENE',4,'Bill / Cash Box Full - The bill / cash box is full.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*ENF',4,'Bill / Cash Box Removed - The bill / cash box is removed or not seated properly.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*ENG',4,'Failed Validator - The validator recognition system has failed.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*ENH',4,'Failed Stacker - The stacker mechanism has failed.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*ENI',4,'Failed Control Board - An error condition has been detected on the control board.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*ENJ',4,'Failed Power Supply - An error condition has been detected on one or more of the power supplies.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*ENK',4,'Comm Error - A communication error has been detected and / or all communications has stopped.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*ENU',4,'MDB Error Codes - The error codes will take the form of ENU_xx where the xx will indicate the hexidecimal equivalent of the Status Code reported by the MDB bill validator. Example � MDB status code 00000100b (ROM Checksum Error) reported. The event code will be: ENU_04. MDB Error code follows delimiter � underscore �');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*ENV',4,'Modification - Modifications have been made.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*ENW',4,'No Fault Found - No fault was found');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*ENX',4,'Client Induced - Fault occurred caused by client.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*ENY',4,'Service Induced - Fault occurred caused by service technician.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*ENZ',4,'Other unlisted operation request - Fault has occurred which is not listed above.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EO',7,'Refrigeration System - General non-specific Refrigeration System fault.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EOA',7,'Temperature sensor - Failed sensor or incorrect reading');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EOB',7,'Compressor Controller - Defective relay or triac causing compressor to run continously or not start');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EOC',7,'Compressor - Errors include: leaks, doesn�t run , reduced capacity or tripping due to overload');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EOK',7,'Evaporator motor - Evaporator fan motor failure');

INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EA',5,'Coin Mechanism - General non-specific Coin Mechanism fault.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EAA',5,'Coin Entry Chute - A jam has occurred in the coin entry chute of the machine');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EAB',5,'Reject System or Change Return Cup - A problem has occurred with the payout at the mech/machine interface.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EAD',5,'Jammed Validator - A jam has occurred in the coin recognition part of the coin mech.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EAE',5,'Jammed Separator - A jam has occurred in the routing mechanism part of the mech.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EAF',5,'Jammed Dispenser - A jam has occurred in coin payout part of the mech.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EAG',5,'Jammed Changegiver - A jam has occurred in an indeterminate part of the changer');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EAH',5,'Reject Chute - A jam has occurred in the reject chute under the coin mech.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EAI',5,'Cash Box - A jam has occurred in the entry to the cash box.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EAO',5,'Failed Control PCB - Coin mech control PCB has failed.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EAP',5,'Failed Power Supply - Coin mech power supply has failed.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EAR',5,'Comm error - A comms error has occurred between mech and VMC (or peripheral).');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EB',7,'Cup System - General non-specific Cup Mechanism fault.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EBE',7,'Cup Transfer Fault or Cup Chute - Cups failing to arrive in dispense area');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EBJ',7,'No Cups - No Cups in Carousel / Turret. If multiple cup sizes / turrets used, indicate as EBJ_x where x is equal to size / turrect number (i.e.EBJ_2)');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EC',7,'Control System - General non-specific Control System fault.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*ECD',7,'Control PCB - A malfunction has been detected in the control board.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*ECU',7,'Battery - The backup battery is low, missing, or not charging.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EC1B',7,'Selection Panel / switch / mechanism - A malfunction has been detected in the selection panel / switch / mechanism.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EC1C',6,'Temperature Sensor Error - Temperature sensor is defective, disconnected or reading is out of range');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EC1G',7,'Product Delivery PCB - A malfunction has been detected in the product delivery board.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*ED',7,'Hot Drinks System - General non-specific Hot Drinks System fault.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EDK',7,'Water Heater Tank - Fault on boiler (water heater tank)');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EDL',7,'Heater Fuse - The boiler (heater element) fuse is blown.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EDM',7,'Thermostat - Faulty boiler control thermostat');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EDN',7,'H.T. Cut Out - Cut out operated because of an overboil situation.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EDP',7,'Level Control - Water Level control in the heater tank faulty');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EE',7,'Brewer Unit - General non-specific Brewer/Espresso Unit fault.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EEA',7,'Brewer Motor - Fault on Motor for brewer');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EEB',7,'Scraper Motor - Fault on Motor for brewer scraper');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EEC',7,'Filter Plate - Filter plate blocked or changed or cleaned.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EED',7,'Filter Paper - No filter paper or faulty filter paper.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EEE',7,'Scraper Arm - Fault on brewer scraper arm.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EEF',7,'Brew Chamber - Damaged brew chamber.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EEG',7,'Piston - Damaged piston in brewer');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EEH',7,'Timer - Fault in Timer unit for Brewer/Espresso unit');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EEI',7,'PCB - Fault on PCB for brewer/espresso unit');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EEJ',7,'Scraper Switch - Faulty scraper switch.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EEK',7,'Drive Mechanism - Fault on drive mechanism for Brewer/Espresso unit.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EEL',7,'Seals - Leakage in brewer/espresso unit.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EEV',7,'Modifications - Modification made to Brewer/Espresso Unit');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EEW',7,'No Fault Found - No Brewer/Espresso Unit fault could be found.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EEX',7,'Client Induced - Client induced Brewer/Espresso Unit fault.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EEY',7,'Service Induced - Service induced Brewer/Espresso Unit fault.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EEZ',7,'Other Unlisted Fault - Other undefined Brewer/Espresso Unit fault.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EF',7,'Water System - General non-specific Water System fault.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EFA',7,'Inlet Pipe - Fault on water connecting hose');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EFB',7,'Water Filter - Water needs to be changed/has been changed.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EFC',7,'Inlet Valve - Faulty Inlet valve');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EFD',7,'Non-return Valve Delivery Valve - Fault on non-return.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EFE',7,'Regulator - Faulty Water pressure regulator');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EFF',7,'Tank - Header tank fault');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EFG',7,'Float System  - Error in the water level detection system.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EFH',7,'Vend Valve - Faulty dispense valve');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EFI',7,'Vend Valve Scale - Valve needs descaling or has been descaled');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EFJ',7,'Hose / Joint Leak - Water leakage in hose or joint.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EFK',7,'Probes/holder - Fault on water level probe assembly');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EFL',7,'Water Supply Failure - No water (could be too low water pressure)');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EFM',7,'Manifold - Fault on water delivery Manifold or Branch pipe');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EFN',7,'Water Pump - Faulty water pump head');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EFO',7,'Pump Motor - Faulty water pump motor');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EFP',7,'Water Quality - Water quality issue reported');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EFV',7,'Modifications - Modifications made to the Water System');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EFW',7,'No Fault Found - No Water System fault could be found.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EFX',7,'Client Induced - Client induced Water System fault.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EFY',7,'Service Induced - Service induced Water System fault.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EFZ',7,'Other Unlisted Fault - Other undefined Water System fault.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EGN',7,'Waste Full - If required specify waste container number, i.e. EGN_2');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EGU',7,'Vend Door - Fault on the product delivery/vend door. This may include a solenoid, motor, mechanical binding, or inability to reach the desired position. This includes side moving doors and other internal doors but excludes the delivery assembly door at the bottom of a snack machine (see EJG).');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EJ',7,'Food, Snack, or Can/Bottle System  - General non-specific Food & Snack System fault.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EJB',7,'Dispense Motor - Errors exist on a product dispense motor which may include motor jammed, motor not home, coupling error, defective motor, or wiring error.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EJH',7,'Health Timer - The Health rules have been violated for the temperature controlled section. Dependant upon the regional rules, the food section may be Out of Service (food may not be served)');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EJJ',7,'Refrigeration - The cooling unit does not cool down to the pre-set temperature or the unit has frozen up');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EJK',7,'Thermostat - The Thermostat is defective.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EJL',7,'Product delivery detector error - The product delivery detection system has an error.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EJM',7,'Product Delivery System - An error has been detected in the product delivery system, e.g. elevator, arm, conveyor belt, etc.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EN',4,'Bill Validator - General non-specific Bill Validator fault.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*ENA',4,'Bill Entry - The bill entry path is blocked.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*ENB',4,'Detecting Frauds - Excessive frauds are being detected.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*ENC',4,'Jammed Validator - The validator recognition path is jammed.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*END',4,'Jammed Stacker - The stacker mechanism is jammed.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*ENE',4,'Bill / Cash Box Full - The bill / cash box is full.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*ENF',4,'Bill / Cash Box Removed - The bill / cash box is removed or not seated properly.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*ENG',4,'Failed Validator - The validator recognition system has failed.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*ENH',4,'Failed Stacker - The stacker mechanism has failed.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*ENI',4,'Failed Control Board - An error condition has been detected on the control board.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*ENJ',4,'Failed Power Supply - An error condition has been detected on one or more of the power supplies.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*ENK',4,'Comm Error - A communication error has been detected and / or all communications has stopped.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*ENU',4,'MDB Error Codes - The error codes will take the form of ENU_xx where the xx will indicate the hexidecimal equivalent of the Status Code reported by the MDB bill validator. Example � MDB status code 00000100b (ROM Checksum Error) reported. The event code will be: ENU_04. MDB Error code follows delimiter � underscore �');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*ENV',4,'Modification - Modifications have been made.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*ENW',4,'No Fault Found - No fault was found');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*ENX',4,'Client Induced - Fault occurred caused by client.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*ENY',4,'Service Induced - Fault occurred caused by service technician.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*ENZ',4,'Other unlisted operation request - Fault has occurred which is not listed above.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EO',7,'Refrigeration System - General non-specific Refrigeration System fault.');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EOA',7,'Temperature sensor - Failed sensor or incorrect reading');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EOB',7,'Compressor Controller - Defective relay or triac causing compressor to run continously or not start');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EOC',7,'Compressor - Errors include: leaks, doesn�t run , reduced capacity or tripping due to overload');
INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA2*EOK',7,'Evaporator motor - Evaporator fan motor failure');

INSERT INTO G4OP.DEX_CODE_ALERT(DEX_CODE,ALERT_ID,MESSAGE) VALUES('EA1*EA1*EK2M*', 9, 'DEX Read Error: {6}');
COMMIT;

GRANT SELECT ON DEVICE.FILE_TRANSFER TO USALIVE_APP_ROLE;
GRANT SELECT ON DEVICE.DEVICE_FILE_TRANSFER TO USALIVE_APP_ROLE;
GRANT SELECT ON LOCATION.TIME_ZONE TO USALIVE_APP_ROLE;

-- Add generic payment export report and daily export report
update report.reports set report_name='Compass Payment Item Export' where report_name='Payment Item Export' and USAGE='N';
update report.reports set report_name='Compass Daily Item Export' where report_name='Daily Item Export' and USAGE='N';
commit;

INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
VALUES( REPORT.REPORTS_SEQ.NEXTVAL, 'Payment Item Export for #{x}', 6, 3, 'Payment Item Export', 'Payment item details for a payment.', 'E', 0);
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( REPORT.REPORTS_SEQ.CURRVAL, 'folioId', '1156');
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( REPORT.REPORTS_SEQ.CURRVAL, 'outputType', '21');
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( REPORT.REPORTS_SEQ.CURRVAL, 'params.DocId', '{x}');

INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
VALUES( REPORT.REPORTS_SEQ.NEXTVAL, 'Daily Item Export for #{x}', 6, 1, 'Daily Item Export', 'Daily transaction item details.', 'E', 0);
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( REPORT.REPORTS_SEQ.CURRVAL, 'folioId', '1197');
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( REPORT.REPORTS_SEQ.CURRVAL, 'outputType', '21');
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( REPORT.REPORTS_SEQ.CURRVAL, 'params.ExportGroupId', '{x}');

commit;

-- transport retry improvements:
insert into REPORT.ccs_transport_status values('R', 'RETRY');
commit;

CREATE TABLE REPORT.CCS_TRANSPORT_ERROR_TYPE(
    ERROR_TYPE_ID NUMBER NOT NULL,
    ERROR_TYPE_NAME VARCHAR2(50),
    CONSTRAINT PK_CCS_TRANSPORT_ERROR_TYPE PRIMARY KEY(ERROR_TYPE_ID)
) TABLESPACE REPORT_DATA;

GRANT SELECT ON REPORT.CCS_TRANSPORT_ERROR_TYPE to USAT_DEV_READ_ONLY,USALIVE_APP_ROLE,USAT_RPT_REQ_ROLE;

insert into REPORT.CCS_TRANSPORT_ERROR_TYPE values(0, 'Instance Error');
insert into REPORT.CCS_TRANSPORT_ERROR_TYPE values(1, 'Transport Error');
insert into REPORT.CCS_TRANSPORT_ERROR_TYPE values(2, 'Program Error');
commit;

ALTER TABLE
   REPORT.CCS_TRANSPORT_ERROR
ADD(
   RETRY_COUNT  NUMBER DEFAULT 0,
   ERROR_TYPE_ID  NUMBER DEFAULT 1,
   CONSTRAINT FK_CCS_TRANSPORT_ERROR_TYPE_ID FOREIGN KEY (ERROR_TYPE_ID) REFERENCES REPORT.CCS_TRANSPORT_ERROR_TYPE (ERROR_TYPE_ID));

   
insert into web_content.literal (literal_id, literal_key, literal_value) 
values(web_content.seq_literal_id.nextval, 'ct-enable-failure', 'Failed to enable the transport.');
insert into web_content.literal (literal_id, literal_key, literal_value) 
values(web_content.seq_literal_id.nextval, 'ct-enable-success', 'Successfully enabled the transport.');
commit;

GRANT SELECT ON REPORT.STORED_FILE to USAT_RPT_REQ_ROLE;
GRANT SELECT ON REPORT.CCS_TRANSPORT_ERROR to USAT_RPT_REQ_ROLE;
GRANT SELECT ON REPORT.CCS_TRANSPORT_ERROR_ATTR to USAT_RPT_REQ_ROLE;

grant select,insert,update,delete on QUARTZ.qrtz_calendars to USAT_RPT_REQ_ROLE;
grant select,insert,update,delete on QUARTZ.qrtz_fired_triggers to USAT_RPT_REQ_ROLE;
grant select,insert,update,delete on QUARTZ.qrtz_blob_triggers to USAT_RPT_REQ_ROLE;
grant select,insert,update,delete on QUARTZ.qrtz_cron_triggers to USAT_RPT_REQ_ROLE;
grant select,insert,update,delete on QUARTZ.qrtz_simple_triggers to USAT_RPT_REQ_ROLE;
grant select,insert,update,delete on QUARTZ.qrtz_simprop_triggers to USAT_RPT_REQ_ROLE;
grant select,insert,update,delete on QUARTZ.qrtz_triggers to USAT_RPT_REQ_ROLE;
grant select,insert,update,delete on QUARTZ.qrtz_job_details to USAT_RPT_REQ_ROLE;
grant select,insert,update,delete on QUARTZ.qrtz_paused_trigger_grps to USAT_RPT_REQ_ROLE;
grant select,insert,update,delete on QUARTZ.qrtz_locks to USAT_RPT_REQ_ROLE;
grant select,insert,update,delete on QUARTZ.qrtz_scheduler_state to USAT_RPT_REQ_ROLE;

INSERT INTO FOLIO_CONF.DIRECTIVE(DIRECTIVE_ID, DIRECTIVE_NAME, DIRECTIVE_DESCRIPTION)
    SELECT 15, 'alt-output-type-list', 'Comma-separated list of alternate target types (from simple.falcon.engine.standard.DirectXHTMLGenerator.TargetType) to display on the report'
      FROM DUAL
     WHERE NOT EXISTS(SELECT 1 FROM FOLIO_CONF.DIRECTIVE WHERE DIRECTIVE_NAME = 'alt-output-type-list');

COMMIT;

   
CREATE OR REPLACE TRIGGER CORP.TRBIU_LEDGER_AUDIT
BEFORE INSERT OR UPDATE ON CORP.LEDGER REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
BEGIN
	IF INSERTING THEN
    	SELECT 
           SYSDATE,
           USER
      	INTO :NEW.UPDATE_DATE,
           :NEW.LAST_UPDATED_BY
      	FROM DUAL;
    END IF;
    
    IF UPDATING THEN
    	SELECT 
           SYSDATE,
           USER
      	INTO 
           :NEW.UPDATE_DATE,
           :NEW.LAST_UPDATED_BY
      	FROM DUAL;
    END IF;
END;
/

CREATE OR REPLACE TRIGGER REPORT.USAT_TRBIU_TRANS 
BEFORE INSERT OR UPDATE ON REPORT.TRANS REFERENCING new AS NEW old AS OLD
FOR EACH ROW
BEGIN
    IF :NEW.refresh_ind = 'N' THEN
        :NEW.refresh_ind := NULL;
    ELSE
        :NEW.refresh_ind := 'Y';
    END IF;
    
    IF INSERTING THEN
    	SELECT 
           USER,
           SYSDATE,
           USER
      	INTO :NEW.CREATED_BY,
           :NEW.UPDATE_DATE,
           :NEW.LAST_UPDATED_BY
      	FROM DUAL;
    END IF;
    
    IF UPDATING THEN
    	SELECT 
           SYSDATE,
           USER
      	INTO 
           :NEW.UPDATE_DATE,
           :NEW.LAST_UPDATED_BY
      	FROM DUAL;
    END IF;
END;
/

-- add pdf back
update folio_conf.output_type set output_type_key='direct:com.usatech.report.custom.USALiveXHTMLGenerator:extendedOutputTypes=true' where output_type_id=22;
update folio_conf.output_type set output_type_key='direct:com.usatech.report.custom.USALiveXHTMLGenerator:chart=true;extendedOutputTypes=true' where output_type_id=28;
commit;

grant select on device.device_setting to USALIVE_APP_ROLE, REPORT;
grant select on device.host to USALIVE_APP_ROLE, REPORT;


CREATE OR REPLACE FUNCTION REPORT.GET_DEVICE_SIM_OR_MEID ( l_device_serial_cd IN REPORT.EPORT.EPORT_SERIAL_NUM%TYPE,
    l_device_type_id IN REPORT.EPORT.DEVICE_TYPE_ID%TYPE)
  RETURN VARCHAR2 IS
    l_sim_meid VARCHAR2(255);
    l_comm_method_cd CHAR(1);
BEGIN 
  IF l_device_type_id = 6 THEN -- for MEI
    select device_setting_value into l_sim_meid from device.device_setting ds join device.device d on ds.device_id=d.device_id and d.device_serial_cd=l_device_serial_cd and d.device_active_yn_flag='Y' where device_setting_parameter_cd='28' and REGEXP_LIKE(device_setting_value, '^\d{20}$');
  ELSE
    select COMM_METHOD_CD into l_comm_method_cd from device.device d where d.device_serial_cd=l_device_serial_cd and d.device_active_yn_flag='Y';
    IF l_comm_method_cd = 'C' THEN
      select h.host_serial_cd into l_sim_meid from device.host h join device.device d on h.device_id=d.device_id and d.device_serial_cd=l_device_serial_cd and d.device_active_yn_flag='Y'
      and h.host_type_id=204;
    ELSIF l_comm_method_cd = 'G' THEN
       select h.host_serial_cd into l_sim_meid from device.host h join device.device d on h.device_id=d.device_id and d.device_serial_cd=l_device_serial_cd and d.device_active_yn_flag='Y'
      and h.host_type_id=202;
    END IF;
  END IF;
  return l_sim_meid;
EXCEPTION
    when NO_DATA_FOUND then
     return null;
END;
/

grant execute on REPORT.GET_DEVICE_SIM_OR_MEID to USALIVE_APP_ROLE;

-- add best vendor reporting to customer 
CREATE OR REPLACE PROCEDURE REPORT.ADD_BEST_VENDOR_REPORTING ( l_customer_id IN REPORT.CUSTOM_FIELD_PARAM.CUSTOMER_ID%TYPE)
IS
	l_exists NUMBER:=0;
BEGIN 
	
  SELECT count(*) into l_exists FROM REPORT.CUSTOM_FIELD_PARAM WHERE customer_id = l_customer_id;
  IF l_exists = 0 THEN
  	INSERT INTO REPORT.CUSTOM_FIELD_PARAM(CUSTOMER_ID, FIELD_NAME, FIELD_LABEL, APP_RQD_FLG, FIELD_COMMENT)
  	values(l_customer_id, 'TERM_VAR_1', 'BV Operator AB Number', 'N', 'Best Vendors DEX Project June 2009');
	INSERT INTO REPORT.CUSTOM_FIELD_PARAM(CUSTOMER_ID, FIELD_NAME, FIELD_LABEL, APP_RQD_FLG, FIELD_COMMENT)
  	values(l_customer_id, 'TERM_VAR_2', 'BV Site AB Number', 'N', 'Best Vendors DEX Project June 2009');
	INSERT INTO REPORT.CUSTOM_FIELD_PARAM(CUSTOMER_ID, FIELD_NAME, FIELD_LABEL, APP_RQD_FLG, FIELD_COMMENT)
  	values(l_customer_id, 'TERM_VAR_3', 'BV Machine Number', 'N', 'Best Vendors DEX Project June 2009');
  END IF;

  SELECT count(*) into l_exists FROM REPORT.CUSTOM_COMMENT_PARAM WHERE customer_id = l_customer_id;

  IF l_exists = 0 THEN
  	INSERT INTO REPORT.CUSTOM_COMMENT_PARAM(CUSTOMER_ID, LINE_TEXT_1, COMMENT_HTML_CLASS)
        values(l_customer_id,'By entering information into the BV Fields below, consent is granted to Best Vendors for unlimited usage of the vending machine data.', 'required');
  END IF;

END;
/

GRANT execute on REPORT.ADD_BEST_VENDOR_REPORTING to USALIVE_APP_ROLE;
GRANT delete on REPORT.CUSTOM_COMMENT_PARAM to USALIVE_APP_ROLE;
GRANT delete on REPORT.CUSTOM_FIELD_PARAM to USALIVE_APP_ROLE;

alter table
  REPORT.CCS_TRANSPORT_ERROR
add RETRY_PROPS VARCHAR2(4000 BYTE);

insert into report.ccs_transport_error_type values(3, 'Partial Success');
commit;


