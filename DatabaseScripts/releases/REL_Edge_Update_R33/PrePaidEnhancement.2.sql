DECLARE
	lv_pwd VARCHAR2(30) := 'PREPAID_APP_1';
	ln_i NUMBER;
BEGIN
	FOR ln_i IN 1..4 LOOP
		BEGIN
            EXECUTE IMMEDIATE 'CREATE USER PREPAID_APP_' || ln_i || ' IDENTIFIED BY ' || lv_pwd || ' DEFAULT TABLESPACE USATAPPS_DEFAULT_TBS TEMPORARY TABLESPACE TEMPTS1';
        EXCEPTION
            WHEN OTHERS THEN
                IF SQLCODE != -1920 THEN
                    RAISE;
                END IF;
        END;
		EXECUTE IMMEDIATE 'ALTER USER PREPAID_APP_' || ln_i || ' PROFILE USAT_APPS';
		EXECUTE IMMEDIATE 'GRANT CONNECT TO PREPAID_APP_' || ln_i;
		EXECUTE IMMEDIATE 'GRANT CREATE SESSION TO PREPAID_APP_' || ln_i;
		EXECUTE IMMEDIATE 'GRANT USAT_PREPAID_APP_ROLE TO PREPAID_APP_' || ln_i;
		EXECUTE IMMEDIATE 'ALTER USER PREPAID_APP_' || ln_i || ' DEFAULT ROLE USAT_PREPAID_APP_ROLE';
	END LOOP;	
END;
/

CREATE USER POSTGRES_MAIN IDENTIFIED BY POSTGRES_MAIN DEFAULT TABLESPACE USATAPPS_DEFAULT_TBS TEMPORARY TABLESPACE TEMPTS1;
GRANT CONNECT TO POSTGRES_MAIN;
GRANT CREATE SESSION TO POSTGRES_MAIN;
GRANT SELECT ON REPORT.USER_TERMINAL TO POSTGRES_MAIN;

GRANT EXECUTE ON PSS.PKG_PREPAID TO USAT_PREPAID_APP_ROLE;
GRANT EXECUTE ON DBADMIN.GET_PRIMARY_POSTAL TO PUBLIC;

DECLARE
    ln_device_id DEVICE.DEVICE_ID%TYPE;
    lv_device_name DEVICE.DEVICE_NAME%TYPE;
    lv_device_type_desc DEVICE_TYPE.DEVICE_TYPE_DESC%TYPE;
    ln_device_sub_type_id DEVICE.DEVICE_SUB_TYPE_ID%TYPE;
    lc_prev_device_active_yn_flag VARCHAR2(1);
    lc_master_id_always_inc VARCHAR2(1);
    ln_key_gen_time NUMBER;
    lc_legacy_safe_key VARCHAR2(32);
    lv_time_zone_guid TIME_ZONE.TIME_ZONE_GUID%TYPE;
    ln_new_host_count NUMBER;
    ln_result_cd NUMBER;
    lv_error_message VARCHAR2(4000);
BEGIN
    PKG_DEVICE_CONFIGURATION.INITIALIZE_DEVICE(
        'V1-1-USD',
        14,
        'Y', 
        ln_device_id,
        lv_device_name,
        lv_device_type_desc,
        ln_device_sub_type_id,
        lc_prev_device_active_yn_flag,
        lc_master_id_always_inc,
        ln_key_gen_time,
        lc_legacy_safe_key,
        lv_time_zone_guid,
        ln_new_host_count,
        ln_result_cd,
        lv_error_message);
    DBMS_OUTPUT.PUT_LINE('INITIALIZED VIRTUAL DEVICE WITH RESULT = ' || ln_result_cd || ':');
    DBMS_OUTPUT.PUT_LINE('    ' || lv_error_message); 
END;
/

DECLARE
    ln_device_id DEVICE.DEVICE_ID%TYPE;
    lv_device_name DEVICE.DEVICE_NAME%TYPE;
    lv_device_type_desc DEVICE_TYPE.DEVICE_TYPE_DESC%TYPE;
    ln_device_sub_type_id DEVICE.DEVICE_SUB_TYPE_ID%TYPE;
    lc_prev_device_active_yn_flag VARCHAR2(1);
    lc_master_id_always_inc VARCHAR2(1);
    ln_key_gen_time NUMBER;
    lc_legacy_safe_key VARCHAR2(32);
    lv_time_zone_guid TIME_ZONE.TIME_ZONE_GUID%TYPE;
    ln_new_host_count NUMBER;
    ln_result_cd NUMBER;
    lv_error_message VARCHAR2(4000);
BEGIN
    PKG_DEVICE_CONFIGURATION.INITIALIZE_DEVICE(
        'V1-1-CAD',
        14,
        'Y', 
        ln_device_id,
        lv_device_name,
        lv_device_type_desc,
        ln_device_sub_type_id,
        lc_prev_device_active_yn_flag,
        lc_master_id_always_inc,
        ln_key_gen_time,
        lc_legacy_safe_key,
        lv_time_zone_guid,
        ln_new_host_count,
        ln_result_cd,
        lv_error_message);
    DBMS_OUTPUT.PUT_LINE('INITIALIZED VIRTUAL DEVICE WITH RESULT = ' || ln_result_cd || ':');
    DBMS_OUTPUT.PUT_LINE('    ' || lv_error_message); 
    -- TODO: Setup payment templates
    
END;
/
COMMIT;