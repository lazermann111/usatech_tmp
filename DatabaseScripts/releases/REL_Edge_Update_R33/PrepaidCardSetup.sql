GRANT SELECT, REFERENCES ON DEVICE.DEVICE_LAST_ACTIVE TO PSS;
GRANT EXECUTE ON RDW_LOADER.PKG_LOAD_DATA TO REPORT;

ALTER TABLE PSS.CONSUMER ADD LAST_CONSUMER_ACCT_CD VARCHAR2(20);
ALTER TABLE PSS.CONSUMER MODIFY CONSUMER_LNAME VARCHAR2(200);
ALTER TABLE PSS.INTERNAL_AUTHORITY ADD AUTHORITY_ID NUMBER(20);

INSERT INTO REPORT.CARD_TYPE(CARD_TYPE_ID, CARD_NAME, PATTERN) SELECT CORP.CARD_TYPE_SEQ.NEXTVAL, 'USAT Prepaid', '*' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM REPORT.CARD_TYPE WHERE CARD_NAME = 'USAT Prepaid');

INSERT INTO REPORT.CARDTYPE_AUTHORITY(CARDTYPE_AUTHORITY_ID, CARDTYPE_ID, AUTHORITY_ID)
SELECT (SELECT MAX(CARDTYPE_AUTHORITY_ID) + 1 FROM REPORT.CARDTYPE_AUTHORITY),
	(SELECT CARD_TYPE_ID FROM REPORT.CARD_TYPE WHERE CARD_NAME = 'USAT Prepaid'),
	(SELECT AUTHORITY_ID FROM REPORT.AUTHORITY WHERE ALIASNAME = 'Generic Authority')
FROM DUAL WHERE NOT EXISTS (
	SELECT 1 FROM REPORT.CARDTYPE_AUTHORITY 
	WHERE CARDTYPE_ID = (SELECT CARD_TYPE_ID FROM REPORT.CARD_TYPE WHERE CARD_NAME = 'USAT Prepaid')
);

INSERT INTO PSS.TRAN_LINE_ITEM_TYPE(TRAN_LINE_ITEM_TYPE_ID, TRAN_LINE_ITEM_TYPE_DESC, TRAN_LINE_ITEM_TYPE_SIGN_PN, TRAN_LINE_ITEM_TYPE_GROUP_CD)
SELECT 204, 'Loyalty Discount', 'N', 'A' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM PSS.TRAN_LINE_ITEM_TYPE WHERE TRAN_LINE_ITEM_TYPE_ID = 204);

INSERT INTO PSS.PAYMENT_ENTRY_METHOD(PAYMENT_ENTRY_METHOD_CD, PAYMENT_ENTRY_METHOD_DESC, DISPLAY_ORDER) 
SELECT 'N', 'Manual Entry', 400 FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM PSS.PAYMENT_ENTRY_METHOD WHERE PAYMENT_ENTRY_METHOD_CD = 'N');

INSERT INTO PSS.CLIENT_PAYMENT_TYPE(CLIENT_PAYMENT_TYPE_CD, CLIENT_PAYMENT_TYPE_DESC, PAYMENT_TYPE_CD, PAYMENT_ENTRY_METHOD_CD) 
SELECT 'N', 'Manual Entry Credit Card', 'C', 'N' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM PSS.CLIENT_PAYMENT_TYPE WHERE CLIENT_PAYMENT_TYPE_CD = 'N');

INSERT INTO PSS.CLIENT_PAYMENT_TYPE(CLIENT_PAYMENT_TYPE_CD, CLIENT_PAYMENT_TYPE_DESC, PAYMENT_TYPE_CD, PAYMENT_ENTRY_METHOD_CD) 
SELECT 'T', 'Manual Entry Special Card', 'S', 'N' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM PSS.CLIENT_PAYMENT_TYPE WHERE CLIENT_PAYMENT_TYPE_CD = 'T');

INSERT INTO PSS.AUTHORITY_PAYMENT_MASK_BREF(AUTHORITY_PAYMENT_MASK_BREF_ID, REGEX_BREF_NAME)
SELECT 18, 'Zip Code' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM PSS.AUTHORITY_PAYMENT_MASK_BREF WHERE AUTHORITY_PAYMENT_MASK_BREF_ID = 18);

INSERT INTO PSS.AUTHORITY_PAYMENT_MASK_BREF(AUTHORITY_PAYMENT_MASK_BREF_ID, REGEX_BREF_NAME)
SELECT 19, 'Address' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM PSS.AUTHORITY_PAYMENT_MASK_BREF WHERE AUTHORITY_PAYMENT_MASK_BREF_ID = 19);

INSERT INTO PSS.AUTHORITY_PAYMENT_MASK_BREF(AUTHORITY_PAYMENT_MASK_BREF_ID, REGEX_BREF_NAME)
SELECT 20, 'Partial Primary Account Number' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM PSS.AUTHORITY_PAYMENT_MASK_BREF WHERE AUTHORITY_PAYMENT_MASK_BREF_ID = 20);

INSERT INTO PSS.AUTHORITY_PAYMENT_MASK(AUTHORITY_PAYMENT_MASK_NAME, AUTHORITY_PAYMENT_MASK_DESC, AUTHORITY_PAYMENT_MASK_REGEX, AUTHORITY_PAYMENT_MASK_BREF, AUTHORITY_ASSN_ID, CARD_NAME)
SELECT 'Visa Manual Entry', 'Visa Credit Card Manual Entry', '^(4[0-9]{15})\|([0-9]{4})\|(|[0-9]{3,4})\|(|[A-Za-z ,.''-]{2,26})\|(|[0-9]{5,9})\|(|[A-Za-z0-9 ,.''-]{3,200})$', '1:1|2:3|3:5|4:2|5:18|6:19', AUTHORITY_ASSN_ID, 'Visa'
FROM AUTHORITY.AUTHORITY_ASSN
WHERE AUTHORITY_ASSN_NAME = 'Visa'
AND NOT EXISTS (SELECT 1 FROM PSS.AUTHORITY_PAYMENT_MASK WHERE AUTHORITY_PAYMENT_MASK_NAME = 'Visa Manual Entry');

INSERT INTO PSS.PAYMENT_SUBTYPE(PAYMENT_SUBTYPE_NAME, PAYMENT_SUBTYPE_CLASS, PAYMENT_SUBTYPE_KEY_NAME, CLIENT_PAYMENT_TYPE_CD, PAYMENT_SUBTYPE_TABLE_NAME, PAYMENT_SUBTYPE_KEY_DESC_NAME, AUTHORITY_PAYMENT_MASK_ID)
SELECT 'Visa (Manual Entry) - Elavon - USA', 'Authority::ISO8583::Elavon', 'TERMINAL_ID', 'N', 'TERMINAL', 'TERMINAL_DESC', AUTHORITY_PAYMENT_MASK_ID
FROM PSS.AUTHORITY_PAYMENT_MASK WHERE AUTHORITY_PAYMENT_MASK_NAME = 'Visa Manual Entry' AND NOT EXISTS (SELECT 1 FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'Visa (Manual Entry) - Elavon - USA');

INSERT INTO PSS.AUTHORITY_PAYMENT_MASK(AUTHORITY_PAYMENT_MASK_NAME, AUTHORITY_PAYMENT_MASK_DESC, AUTHORITY_PAYMENT_MASK_REGEX, AUTHORITY_PAYMENT_MASK_BREF, AUTHORITY_ASSN_ID, CARD_NAME)
SELECT 'MasterCard Manual Entry', 'MasterCard Credit Card Manual Entry', '^(5[1-5][0-9]{14})\|([0-9]{4})\|(|[0-9]{3,4})\|(|[A-Za-z ,.''-]{2,26})\|(|[0-9]{5,9})\|(|[A-Za-z0-9 ,.''-]{3,200})$', '1:1|2:3|3:5|4:2|5:18|6:19', AUTHORITY_ASSN_ID, 'MasterCard'
FROM AUTHORITY.AUTHORITY_ASSN
WHERE AUTHORITY_ASSN_NAME = 'MasterCard'
AND NOT EXISTS (SELECT 1 FROM PSS.AUTHORITY_PAYMENT_MASK WHERE AUTHORITY_PAYMENT_MASK_NAME = 'MasterCard Manual Entry');

INSERT INTO PSS.PAYMENT_SUBTYPE(PAYMENT_SUBTYPE_NAME, PAYMENT_SUBTYPE_CLASS, PAYMENT_SUBTYPE_KEY_NAME, CLIENT_PAYMENT_TYPE_CD, PAYMENT_SUBTYPE_TABLE_NAME, PAYMENT_SUBTYPE_KEY_DESC_NAME, AUTHORITY_PAYMENT_MASK_ID)
SELECT 'MasterCard (Manual Entry) - Elavon - USA', 'Authority::ISO8583::Elavon', 'TERMINAL_ID', 'N', 'TERMINAL', 'TERMINAL_DESC', AUTHORITY_PAYMENT_MASK_ID
FROM PSS.AUTHORITY_PAYMENT_MASK WHERE AUTHORITY_PAYMENT_MASK_NAME = 'MasterCard Manual Entry' AND NOT EXISTS (SELECT 1 FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'MasterCard (Manual Entry) - Elavon - USA');

INSERT INTO PSS.AUTHORITY_PAYMENT_MASK(AUTHORITY_PAYMENT_MASK_NAME, AUTHORITY_PAYMENT_MASK_DESC, AUTHORITY_PAYMENT_MASK_REGEX, AUTHORITY_PAYMENT_MASK_BREF, AUTHORITY_ASSN_ID, CARD_NAME)
SELECT 'American Express Manual Entry', 'American Express Credit Card Manual Entry', '^(3[47][0-9]{13})\|([0-9]{4})\|(|[0-9]{3,4})\|(|[A-Za-z ,.''-]{2,26})\|(|[0-9]{5,9})\|(|[A-Za-z0-9 ,.''-]{3,200})$', '1:1|2:3|3:5|4:2|5:18|6:19', AUTHORITY_ASSN_ID, 'American Express'
FROM AUTHORITY.AUTHORITY_ASSN
WHERE AUTHORITY_ASSN_NAME = 'American Express'
AND NOT EXISTS (SELECT 1 FROM PSS.AUTHORITY_PAYMENT_MASK WHERE AUTHORITY_PAYMENT_MASK_NAME = 'American Express Manual Entry');

INSERT INTO PSS.PAYMENT_SUBTYPE(PAYMENT_SUBTYPE_NAME, PAYMENT_SUBTYPE_CLASS, PAYMENT_SUBTYPE_KEY_NAME, CLIENT_PAYMENT_TYPE_CD, PAYMENT_SUBTYPE_TABLE_NAME, PAYMENT_SUBTYPE_KEY_DESC_NAME, AUTHORITY_PAYMENT_MASK_ID)
SELECT 'American Express (Manual Entry) - Elavon - USA', 'Authority::ISO8583::Elavon', 'TERMINAL_ID', 'N', 'TERMINAL', 'TERMINAL_DESC', AUTHORITY_PAYMENT_MASK_ID
FROM PSS.AUTHORITY_PAYMENT_MASK WHERE AUTHORITY_PAYMENT_MASK_NAME = 'American Express Manual Entry' AND NOT EXISTS (SELECT 1 FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'American Express (Manual Entry) - Elavon - USA');

INSERT INTO PSS.AUTHORITY_PAYMENT_MASK(AUTHORITY_PAYMENT_MASK_NAME, AUTHORITY_PAYMENT_MASK_DESC, AUTHORITY_PAYMENT_MASK_REGEX, AUTHORITY_PAYMENT_MASK_BREF, AUTHORITY_ASSN_ID, CARD_NAME)
SELECT 'Discover Manual Entry', 'Discover Credit Card Manual Entry', '^((?:30[0-5][0-9]|3095|35[2-8][0-9]|36|3[8-9][0-9]{2}|6011|622[1-9]|62[4-6][0-9]|628[2-8]|64[4-9][0-9]|65[0-9]{2})[0-9]{12})\|([0-9]{4})\|(|[0-9]{3,4})\|(|[A-Za-z ,.''-]{2,26})\|(|[0-9]{5,9})\|(|[A-Za-z0-9 ,.''-]{3,200})$', '1:1|2:3|3:5|4:2|5:18|6:19', AUTHORITY_ASSN_ID, 'Discover'
FROM AUTHORITY.AUTHORITY_ASSN
WHERE AUTHORITY_ASSN_NAME = 'Discover Card'
AND NOT EXISTS (SELECT 1 FROM PSS.AUTHORITY_PAYMENT_MASK WHERE AUTHORITY_PAYMENT_MASK_NAME = 'Discover Manual Entry');

INSERT INTO PSS.PAYMENT_SUBTYPE(PAYMENT_SUBTYPE_NAME, PAYMENT_SUBTYPE_CLASS, PAYMENT_SUBTYPE_KEY_NAME, CLIENT_PAYMENT_TYPE_CD, PAYMENT_SUBTYPE_TABLE_NAME, PAYMENT_SUBTYPE_KEY_DESC_NAME, AUTHORITY_PAYMENT_MASK_ID)
SELECT 'Discover Card (Manual Entry) - Elavon - USA', 'Authority::ISO8583::Elavon', 'TERMINAL_ID', 'N', 'TERMINAL', 'TERMINAL_DESC', AUTHORITY_PAYMENT_MASK_ID
FROM PSS.AUTHORITY_PAYMENT_MASK WHERE AUTHORITY_PAYMENT_MASK_NAME = 'Discover Manual Entry' AND NOT EXISTS (SELECT 1 FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'Discover Card (Manual Entry) - Elavon - USA');

INSERT INTO PSS.PAYMENT_SUBTYPE(PAYMENT_SUBTYPE_NAME, PAYMENT_SUBTYPE_CLASS, PAYMENT_SUBTYPE_KEY_NAME, CLIENT_PAYMENT_TYPE_CD, PAYMENT_SUBTYPE_TABLE_NAME, PAYMENT_SUBTYPE_KEY_DESC_NAME, AUTHORITY_PAYMENT_MASK_ID)
SELECT 'Visa (Manual Entry) - Canada', 'Authority::ISO8583::FHMS::Paymentech', 'TERMINAL_ID', 'N', 'TERMINAL', 'TERMINAL_DESC', AUTHORITY_PAYMENT_MASK_ID
FROM PSS.AUTHORITY_PAYMENT_MASK WHERE AUTHORITY_PAYMENT_MASK_NAME = 'Visa Manual Entry' AND NOT EXISTS (SELECT 1 FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'Visa (Manual Entry) - Canada');

INSERT INTO PSS.PAYMENT_SUBTYPE(PAYMENT_SUBTYPE_NAME, PAYMENT_SUBTYPE_CLASS, PAYMENT_SUBTYPE_KEY_NAME, CLIENT_PAYMENT_TYPE_CD, PAYMENT_SUBTYPE_TABLE_NAME, PAYMENT_SUBTYPE_KEY_DESC_NAME, AUTHORITY_PAYMENT_MASK_ID)
SELECT 'MasterCard (Manual Entry) - Canada', 'Authority::ISO8583::FHMS::Paymentech', 'TERMINAL_ID', 'N', 'TERMINAL', 'TERMINAL_DESC', AUTHORITY_PAYMENT_MASK_ID
FROM PSS.AUTHORITY_PAYMENT_MASK WHERE AUTHORITY_PAYMENT_MASK_NAME = 'MasterCard Manual Entry' AND NOT EXISTS (SELECT 1 FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'MasterCard (Manual Entry) - Canada');

INSERT INTO PSS.PAYMENT_SUBTYPE(PAYMENT_SUBTYPE_NAME, PAYMENT_SUBTYPE_CLASS, PAYMENT_SUBTYPE_KEY_NAME, CLIENT_PAYMENT_TYPE_CD, PAYMENT_SUBTYPE_TABLE_NAME, PAYMENT_SUBTYPE_KEY_DESC_NAME, AUTHORITY_PAYMENT_MASK_ID)
SELECT 'American Express (Manual Entry) - Canada', 'Authority::ISO8583::FHMS::Paymentech', 'TERMINAL_ID', 'N', 'TERMINAL', 'TERMINAL_DESC', AUTHORITY_PAYMENT_MASK_ID
FROM PSS.AUTHORITY_PAYMENT_MASK WHERE AUTHORITY_PAYMENT_MASK_NAME = 'American Express Manual Entry' AND NOT EXISTS (SELECT 1 FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'American Express (Manual Entry) - Canada');

INSERT INTO PSS.PAYMENT_SUBTYPE(PAYMENT_SUBTYPE_NAME, PAYMENT_SUBTYPE_CLASS, PAYMENT_SUBTYPE_KEY_NAME, CLIENT_PAYMENT_TYPE_CD, PAYMENT_SUBTYPE_TABLE_NAME, PAYMENT_SUBTYPE_KEY_DESC_NAME, AUTHORITY_PAYMENT_MASK_ID)
SELECT 'Discover Card (Manual Entry) - Canada', 'Authority::ISO8583::FHMS::Paymentech', 'TERMINAL_ID', 'N', 'TERMINAL', 'TERMINAL_DESC', AUTHORITY_PAYMENT_MASK_ID
FROM PSS.AUTHORITY_PAYMENT_MASK WHERE AUTHORITY_PAYMENT_MASK_NAME = 'Discover Manual Entry' AND NOT EXISTS (SELECT 1 FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'Discover Card (Manual Entry) - Canada');

INSERT INTO PSS.PAYMENT_SUBTYPE(PAYMENT_SUBTYPE_NAME, PAYMENT_SUBTYPE_CLASS, PAYMENT_SUBTYPE_KEY_NAME, CLIENT_PAYMENT_TYPE_CD, PAYMENT_SUBTYPE_TABLE_NAME, PAYMENT_SUBTYPE_KEY_DESC_NAME, AUTHORITY_PAYMENT_MASK_ID)
SELECT 'Manual Entry Error Bin', 'Authority::NOP', 'TERMINAL_ID', 'N', 'TERMINAL', 'TERMINAL_DESC', 
(SELECT MIN(AUTHORITY_PAYMENT_MASK_ID) FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_CLASS = 'Authority::NOP' AND PAYMENT_SUBTYPE_NAME LIKE '%Error Bin')
FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'Manual Entry Error Bin');

INSERT INTO AUTHORITY.HANDLER(HANDLER_NAME, HANDLER_CLASS)
SELECT 'USAT Internal Prepaid', 'Internal::Prepaid' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM AUTHORITY.HANDLER WHERE HANDLER_NAME = 'USAT Internal Prepaid');

INSERT INTO AUTHORITY.AUTHORITY_TYPE(AUTHORITY_TYPE_NAME, AUTHORITY_TYPE_DESC, HANDLER_ID)
SELECT 'USAT Internal Prepaid', 'USAT Internal Prepaid', HANDLER_ID FROM AUTHORITY.HANDLER WHERE HANDLER_NAME = 'USAT Internal Prepaid'
AND NOT EXISTS (SELECT 1 FROM AUTHORITY.AUTHORITY_TYPE WHERE AUTHORITY_TYPE_NAME = 'USAT Internal Prepaid');

INSERT INTO AUTHORITY.AUTHORITY(AUTHORITY_NAME, AUTHORITY_TYPE_ID, AUTHORITY_SERVICE_ID, TERMINAL_CAPTURE_FLAG)
SELECT 'USAT Prepaid Card Authority', AUTHORITY_TYPE_ID, 1, 'N' FROM AUTHORITY.AUTHORITY_TYPE WHERE AUTHORITY_TYPE_NAME = 'USAT Internal Prepaid'
AND NOT EXISTS (SELECT 1 FROM AUTHORITY.AUTHORITY WHERE AUTHORITY_NAME = 'USAT Prepaid Card Authority');

INSERT INTO PSS.INTERNAL_AUTHORITY(INTERNAL_AUTHORITY_NAME, AUTHORITY_SERVICE_TYPE_ID, AUTHORITY_ID)
SELECT 'USA Tech (prepaid)', 1, AUTHORITY_ID 
FROM AUTHORITY.AUTHORITY 
WHERE AUTHORITY_NAME = 'USAT Prepaid Card Authority'
	AND NOT EXISTS (SELECT 1 FROM PSS.INTERNAL_AUTHORITY WHERE INTERNAL_AUTHORITY_NAME = 'USA Tech (prepaid)');

INSERT INTO PSS.INTERNAL_PAYMENT_TYPE(INTERNAL_PAYMENT_TYPE_DESC, INTERNAL_AUTHORITY_ID)
SELECT 'Special Card (prepaid)', INTERNAL_AUTHORITY_ID
FROM PSS.INTERNAL_AUTHORITY
WHERE INTERNAL_AUTHORITY_NAME = 'USA Tech (prepaid)' 
	AND NOT EXISTS (SELECT 1 FROM PSS.INTERNAL_PAYMENT_TYPE WHERE INTERNAL_PAYMENT_TYPE_DESC = 'Special Card (prepaid)');

INSERT INTO PSS.CONSUMER_ACCT_FMT(CONSUMER_ACCT_FMT_ID, CONSUMER_ACCT_FMT_REGEX, CONSUMER_ACCT_FMT_REGEX_BREF, CONSUMER_ACCT_FMT_NAME, CONSUMER_ACCT_FMT_DESC)
SELECT 2, '^(6396212([0-9]{3})([0-9]{8})([0-9]{1}))=([0-9]{4})([0-9]{2})([0-9]{5})([0-9]{2})$', '1:1|2:15|3:20|4:13|5:3|6:10|7:16|8:14', 'Prepaid Card', 'USAT ISO Card Format Two'
FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM PSS.CONSUMER_ACCT_FMT WHERE CONSUMER_ACCT_FMT_ID = 2);

INSERT INTO PSS.CONSUMER_ACCT_FMT_AUTHORITY(CONSUMER_ACCT_FMT_ID, AUTHORITY_ID)
SELECT 2, AUTHORITY_ID FROM AUTHORITY.AUTHORITY WHERE AUTHORITY_NAME = 'USAT Prepaid Card Authority'
AND NOT EXISTS (SELECT 1 FROM PSS.CONSUMER_ACCT_FMT_AUTHORITY WHERE CONSUMER_ACCT_FMT_ID = 2);

INSERT INTO PSS.AUTHORITY_PAYMENT_MASK(AUTHORITY_PAYMENT_MASK_NAME, AUTHORITY_PAYMENT_MASK_REGEX, AUTHORITY_PAYMENT_MASK_BREF, CARD_NAME)
SELECT 'USAT ISO Card (Format Two)', '^(6396212([0-9]{11})([0-9]{1}))=([0-9]{4})([0-9]{2})([0-9]{5})([0-9]{2})$', '1:1|2:20|3:13|4:3|5:10|6:16|7:14', 'USAT Prepaid' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM PSS.AUTHORITY_PAYMENT_MASK WHERE AUTHORITY_PAYMENT_MASK_NAME = 'USAT ISO Card (Format Two)');

INSERT INTO PSS.PAYMENT_SUBTYPE(PAYMENT_SUBTYPE_NAME, PAYMENT_SUBTYPE_CLASS, PAYMENT_SUBTYPE_KEY_NAME, CLIENT_PAYMENT_TYPE_CD, PAYMENT_SUBTYPE_TABLE_NAME, PAYMENT_SUBTYPE_KEY_DESC_NAME, AUTHORITY_PAYMENT_MASK_ID)
SELECT 'USAT ISO Card - Prepaid Card', 'Internal::Prepaid', 'INTERNAL_PAYMENT_TYPE_ID', 'S', 'INTERNAL_PAYMENT_TYPE', 'INTERNAL_PAYMENT_TYPE_DESC', AUTHORITY_PAYMENT_MASK_ID
FROM PSS.AUTHORITY_PAYMENT_MASK WHERE AUTHORITY_PAYMENT_MASK_NAME = 'USAT ISO Card (Format Two)' AND NOT EXISTS (
SELECT 1 FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'USAT ISO Card - Prepaid Card');

INSERT INTO PSS.PAYMENT_SUBTYPE(PAYMENT_SUBTYPE_NAME, PAYMENT_SUBTYPE_CLASS, PAYMENT_SUBTYPE_KEY_NAME, CLIENT_PAYMENT_TYPE_CD, PAYMENT_SUBTYPE_TABLE_NAME, PAYMENT_SUBTYPE_KEY_DESC_NAME, AUTHORITY_PAYMENT_MASK_ID)
SELECT 'RFID USAT ISO Card - Prepaid Card', 'Internal::Prepaid', 'INTERNAL_PAYMENT_TYPE_ID', 'P', 'INTERNAL_PAYMENT_TYPE', 'INTERNAL_PAYMENT_TYPE_DESC', AUTHORITY_PAYMENT_MASK_ID
FROM PSS.AUTHORITY_PAYMENT_MASK WHERE AUTHORITY_PAYMENT_MASK_NAME = 'USAT ISO Card (Format Two)' AND NOT EXISTS (
SELECT 1 FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'RFID USAT ISO Card - Prepaid Card');

INSERT INTO PSS.AUTHORITY_PAYMENT_MASK(AUTHORITY_PAYMENT_MASK_NAME, AUTHORITY_PAYMENT_MASK_DESC, AUTHORITY_PAYMENT_MASK_REGEX, AUTHORITY_PAYMENT_MASK_BREF, CARD_NAME)
SELECT 'USAT ISO Card (Format Two, Manual Entry)', 'USAT Prepaid Card Manual Entry', '^(6396212[0-9]{12})\|([0-9]{4})\|(|[0-9]{3,4})\|(|[A-Za-z ,.''-]{2,26})\|(|[0-9]{5,9})\|(|[A-Za-z0-9 ,.''-]{3,200})$', '1:1|2:3|3:5|4:2|5:18|6:19', 'USAT Prepaid'
FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM PSS.AUTHORITY_PAYMENT_MASK WHERE AUTHORITY_PAYMENT_MASK_NAME = 'USAT ISO Card (Format Two, Manual Entry)');

INSERT INTO PSS.PAYMENT_SUBTYPE(PAYMENT_SUBTYPE_NAME, PAYMENT_SUBTYPE_CLASS, PAYMENT_SUBTYPE_KEY_NAME, CLIENT_PAYMENT_TYPE_CD, PAYMENT_SUBTYPE_TABLE_NAME, PAYMENT_SUBTYPE_KEY_DESC_NAME, AUTHORITY_PAYMENT_MASK_ID)
SELECT 'USAT ISO Card - Prepaid Card (Manual Entry)', 'Internal::Prepaid', 'INTERNAL_PAYMENT_TYPE_ID', 'T', 'INTERNAL_PAYMENT_TYPE', 'INTERNAL_PAYMENT_TYPE_DESC', AUTHORITY_PAYMENT_MASK_ID
FROM PSS.AUTHORITY_PAYMENT_MASK WHERE AUTHORITY_PAYMENT_MASK_NAME = 'USAT ISO Card (Format Two, Manual Entry)' AND NOT EXISTS (
SELECT 1 FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'USAT ISO Card - Prepaid Card (Manual Entry)');

INSERT INTO PSS.POS_PTA_TMPL(POS_PTA_TMPL_NAME, POS_PTA_TMPL_DESC)
SELECT 'SPECIAL MERGE: USAT ISO Prepaid Card', 'Setup to accept USATech issued ISO Card format 2' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'SPECIAL MERGE: USAT ISO Prepaid Card');

INSERT INTO PSS.POS_PTA_TMPL_ENTRY(POS_PTA_TMPL_ID, PAYMENT_SUBTYPE_ID, POS_PTA_ACTIVATION_OSET_HR, PAYMENT_SUBTYPE_KEY_ID, POS_PTA_PRIORITY, CURRENCY_CD)
SELECT (SELECT POS_PTA_TMPL_ID FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'SPECIAL MERGE: USAT ISO Prepaid Card'),
	(SELECT PAYMENT_SUBTYPE_ID FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'USAT ISO Card - Prepaid Card'),
	0, (SELECT INTERNAL_PAYMENT_TYPE_ID FROM PSS.INTERNAL_PAYMENT_TYPE WHERE INTERNAL_PAYMENT_TYPE_DESC = 'Special Card (prepaid)'),
	1, 'USD' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM PSS.POS_PTA_TMPL_ENTRY 
	WHERE POS_PTA_TMPL_ID = (SELECT POS_PTA_TMPL_ID FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'SPECIAL MERGE: USAT ISO Prepaid Card')
		AND PAYMENT_SUBTYPE_ID = (SELECT PAYMENT_SUBTYPE_ID FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'USAT ISO Card - Prepaid Card')
);

INSERT INTO PSS.POS_PTA_TMPL_ENTRY(POS_PTA_TMPL_ID, PAYMENT_SUBTYPE_ID, POS_PTA_ACTIVATION_OSET_HR, PAYMENT_SUBTYPE_KEY_ID, POS_PTA_PRIORITY, CURRENCY_CD)
SELECT (SELECT POS_PTA_TMPL_ID FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'SPECIAL MERGE: USAT ISO Prepaid Card'),
	(SELECT MIN(PAYMENT_SUBTYPE_ID) FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME LIKE '%Error Bin' AND CLIENT_PAYMENT_TYPE_CD = 'C'),
	0, (SELECT TERMINAL_ID FROM PSS.TERMINAL WHERE TERMINAL_DESC = 'Error Bin Terminal'), 
	2, 'USD' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM PSS.POS_PTA_TMPL_ENTRY 
	WHERE POS_PTA_TMPL_ID = (SELECT POS_PTA_TMPL_ID FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'SPECIAL MERGE: USAT ISO Prepaid Card')
		AND PAYMENT_SUBTYPE_ID = (SELECT MIN(PAYMENT_SUBTYPE_ID) FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME LIKE '%Error Bin' AND CLIENT_PAYMENT_TYPE_CD = 'C')
);

INSERT INTO PSS.POS_PTA_TMPL_ENTRY(POS_PTA_TMPL_ID, PAYMENT_SUBTYPE_ID, POS_PTA_ACTIVATION_OSET_HR, PAYMENT_SUBTYPE_KEY_ID, POS_PTA_PRIORITY, CURRENCY_CD)
SELECT (SELECT POS_PTA_TMPL_ID FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'SPECIAL MERGE: USAT ISO Prepaid Card'),
	(SELECT PAYMENT_SUBTYPE_ID FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'RFID USAT ISO Card - Prepaid Card'),
	0, (SELECT INTERNAL_PAYMENT_TYPE_ID FROM PSS.INTERNAL_PAYMENT_TYPE WHERE INTERNAL_PAYMENT_TYPE_DESC = 'Special Card (prepaid)'),
	1, 'USD' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM PSS.POS_PTA_TMPL_ENTRY 
	WHERE POS_PTA_TMPL_ID = (SELECT POS_PTA_TMPL_ID FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'SPECIAL MERGE: USAT ISO Prepaid Card')
		AND PAYMENT_SUBTYPE_ID = (SELECT PAYMENT_SUBTYPE_ID FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'RFID USAT ISO Card - Prepaid Card')
);

INSERT INTO PSS.POS_PTA_TMPL_ENTRY(POS_PTA_TMPL_ID, PAYMENT_SUBTYPE_ID, POS_PTA_ACTIVATION_OSET_HR, PAYMENT_SUBTYPE_KEY_ID, POS_PTA_PRIORITY, CURRENCY_CD)
SELECT (SELECT POS_PTA_TMPL_ID FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'SPECIAL MERGE: USAT ISO Prepaid Card'),
	(SELECT MIN(PAYMENT_SUBTYPE_ID) FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME LIKE '%Error Bin' AND CLIENT_PAYMENT_TYPE_CD = 'R'),
	0, (SELECT TERMINAL_ID FROM PSS.TERMINAL WHERE TERMINAL_DESC = 'Error Bin Terminal'), 
	2, 'USD' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM PSS.POS_PTA_TMPL_ENTRY 
	WHERE POS_PTA_TMPL_ID = (SELECT POS_PTA_TMPL_ID FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'SPECIAL MERGE: USAT ISO Prepaid Card')
		AND PAYMENT_SUBTYPE_ID = (SELECT MIN(PAYMENT_SUBTYPE_ID) FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME LIKE '%Error Bin' AND CLIENT_PAYMENT_TYPE_CD = 'R')
);

INSERT INTO PSS.POS_PTA_TMPL_ENTRY(POS_PTA_TMPL_ID, PAYMENT_SUBTYPE_ID, POS_PTA_ACTIVATION_OSET_HR, PAYMENT_SUBTYPE_KEY_ID, POS_PTA_PRIORITY, CURRENCY_CD)
SELECT (SELECT POS_PTA_TMPL_ID FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'SPECIAL MERGE: USAT ISO Prepaid Card'),
	(SELECT PAYMENT_SUBTYPE_ID FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'USAT ISO Card - Prepaid Card (Manual Entry)'),
	0, (SELECT INTERNAL_PAYMENT_TYPE_ID FROM PSS.INTERNAL_PAYMENT_TYPE WHERE INTERNAL_PAYMENT_TYPE_DESC = 'Special Card (prepaid)'),
	1, 'USD' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM PSS.POS_PTA_TMPL_ENTRY 
	WHERE POS_PTA_TMPL_ID = (SELECT POS_PTA_TMPL_ID FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'SPECIAL MERGE: USAT ISO Prepaid Card')
		AND PAYMENT_SUBTYPE_ID = (SELECT PAYMENT_SUBTYPE_ID FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'USAT ISO Card - Prepaid Card (Manual Entry)')
);

INSERT INTO PSS.POS_PTA_TMPL_ENTRY(POS_PTA_TMPL_ID, PAYMENT_SUBTYPE_ID, POS_PTA_ACTIVATION_OSET_HR, PAYMENT_SUBTYPE_KEY_ID, POS_PTA_PRIORITY, CURRENCY_CD)
SELECT (SELECT POS_PTA_TMPL_ID FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'SPECIAL MERGE: USAT ISO Prepaid Card'),
	(SELECT MIN(PAYMENT_SUBTYPE_ID) FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME LIKE '%Error Bin' AND CLIENT_PAYMENT_TYPE_CD = 'N'),
	0, (SELECT TERMINAL_ID FROM PSS.TERMINAL WHERE TERMINAL_DESC = 'Error Bin Terminal'), 
	2, 'USD' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM PSS.POS_PTA_TMPL_ENTRY 
	WHERE POS_PTA_TMPL_ID = (SELECT POS_PTA_TMPL_ID FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'SPECIAL MERGE: USAT ISO Prepaid Card')
		AND PAYMENT_SUBTYPE_ID = (SELECT MIN(PAYMENT_SUBTYPE_ID) FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME LIKE '%Error Bin' AND CLIENT_PAYMENT_TYPE_CD = 'N')
);

INSERT INTO PSS.POS_PTA_TMPL(POS_PTA_TMPL_NAME, POS_PTA_TMPL_DESC)
SELECT 'SPECIAL MERGE: USAT ISO Prepaid Card - Canada', 'Setup to accept USATech issued ISO Card format 2' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'SPECIAL MERGE: USAT ISO Prepaid Card - Canada');

INSERT INTO PSS.POS_PTA_TMPL_ENTRY(POS_PTA_TMPL_ID, PAYMENT_SUBTYPE_ID, POS_PTA_ACTIVATION_OSET_HR, PAYMENT_SUBTYPE_KEY_ID, POS_PTA_PRIORITY, CURRENCY_CD)
SELECT (SELECT POS_PTA_TMPL_ID FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'SPECIAL MERGE: USAT ISO Prepaid Card - Canada'),
	(SELECT PAYMENT_SUBTYPE_ID FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'USAT ISO Card - Prepaid Card'),
	0, (SELECT INTERNAL_PAYMENT_TYPE_ID FROM PSS.INTERNAL_PAYMENT_TYPE WHERE INTERNAL_PAYMENT_TYPE_DESC = 'Special Card (prepaid)'),
	1, 'CAD' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM PSS.POS_PTA_TMPL_ENTRY 
	WHERE POS_PTA_TMPL_ID = (SELECT POS_PTA_TMPL_ID FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'SPECIAL MERGE: USAT ISO Prepaid Card - Canada')
		AND PAYMENT_SUBTYPE_ID = (SELECT PAYMENT_SUBTYPE_ID FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'USAT ISO Card - Prepaid Card')
);

INSERT INTO PSS.POS_PTA_TMPL_ENTRY(POS_PTA_TMPL_ID, PAYMENT_SUBTYPE_ID, POS_PTA_ACTIVATION_OSET_HR, PAYMENT_SUBTYPE_KEY_ID, POS_PTA_PRIORITY, CURRENCY_CD)
SELECT (SELECT POS_PTA_TMPL_ID FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'SPECIAL MERGE: USAT ISO Prepaid Card - Canada'),
	(SELECT MIN(PAYMENT_SUBTYPE_ID) FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME LIKE '%Error Bin' AND CLIENT_PAYMENT_TYPE_CD = 'C'),
	0, (SELECT TERMINAL_ID FROM PSS.TERMINAL WHERE TERMINAL_DESC = 'Error Bin Terminal'), 
	2, 'CAD' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM PSS.POS_PTA_TMPL_ENTRY 
	WHERE POS_PTA_TMPL_ID = (SELECT POS_PTA_TMPL_ID FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'SPECIAL MERGE: USAT ISO Prepaid Card - Canada')
		AND PAYMENT_SUBTYPE_ID = (SELECT MIN(PAYMENT_SUBTYPE_ID) FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME LIKE '%Error Bin' AND CLIENT_PAYMENT_TYPE_CD = 'C')
);

INSERT INTO PSS.POS_PTA_TMPL_ENTRY(POS_PTA_TMPL_ID, PAYMENT_SUBTYPE_ID, POS_PTA_ACTIVATION_OSET_HR, PAYMENT_SUBTYPE_KEY_ID, POS_PTA_PRIORITY, CURRENCY_CD)
SELECT (SELECT POS_PTA_TMPL_ID FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'SPECIAL MERGE: USAT ISO Prepaid Card - Canada'),
	(SELECT PAYMENT_SUBTYPE_ID FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'RFID USAT ISO Card - Prepaid Card'),
	0, (SELECT INTERNAL_PAYMENT_TYPE_ID FROM PSS.INTERNAL_PAYMENT_TYPE WHERE INTERNAL_PAYMENT_TYPE_DESC = 'Special Card (prepaid)'),
	1, 'CAD' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM PSS.POS_PTA_TMPL_ENTRY 
	WHERE POS_PTA_TMPL_ID = (SELECT POS_PTA_TMPL_ID FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'SPECIAL MERGE: USAT ISO Prepaid Card - Canada')
		AND PAYMENT_SUBTYPE_ID = (SELECT PAYMENT_SUBTYPE_ID FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'RFID USAT ISO Card - Prepaid Card')
);

INSERT INTO PSS.POS_PTA_TMPL_ENTRY(POS_PTA_TMPL_ID, PAYMENT_SUBTYPE_ID, POS_PTA_ACTIVATION_OSET_HR, PAYMENT_SUBTYPE_KEY_ID, POS_PTA_PRIORITY, CURRENCY_CD)
SELECT (SELECT POS_PTA_TMPL_ID FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'SPECIAL MERGE: USAT ISO Prepaid Card - Canada'),
	(SELECT MIN(PAYMENT_SUBTYPE_ID) FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME LIKE '%Error Bin' AND CLIENT_PAYMENT_TYPE_CD = 'R'),
	0, (SELECT TERMINAL_ID FROM PSS.TERMINAL WHERE TERMINAL_DESC = 'Error Bin Terminal'), 
	2, 'CAD' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM PSS.POS_PTA_TMPL_ENTRY 
	WHERE POS_PTA_TMPL_ID = (SELECT POS_PTA_TMPL_ID FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'SPECIAL MERGE: USAT ISO Prepaid Card - Canada')
		AND PAYMENT_SUBTYPE_ID = (SELECT MIN(PAYMENT_SUBTYPE_ID) FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME LIKE '%Error Bin' AND CLIENT_PAYMENT_TYPE_CD = 'R')
);

INSERT INTO PSS.POS_PTA_TMPL_ENTRY(POS_PTA_TMPL_ID, PAYMENT_SUBTYPE_ID, POS_PTA_ACTIVATION_OSET_HR, PAYMENT_SUBTYPE_KEY_ID, POS_PTA_PRIORITY, CURRENCY_CD)
SELECT (SELECT POS_PTA_TMPL_ID FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'SPECIAL MERGE: USAT ISO Prepaid Card - Canada'),
	(SELECT PAYMENT_SUBTYPE_ID FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'USAT ISO Card - Prepaid Card (Manual Entry)'),
	0, (SELECT INTERNAL_PAYMENT_TYPE_ID FROM PSS.INTERNAL_PAYMENT_TYPE WHERE INTERNAL_PAYMENT_TYPE_DESC = 'Special Card (prepaid)'),
	1, 'CAD' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM PSS.POS_PTA_TMPL_ENTRY 
	WHERE POS_PTA_TMPL_ID = (SELECT POS_PTA_TMPL_ID FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'SPECIAL MERGE: USAT ISO Prepaid Card - Canada')
		AND PAYMENT_SUBTYPE_ID = (SELECT PAYMENT_SUBTYPE_ID FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'USAT ISO Card - Prepaid Card (Manual Entry)')
);

INSERT INTO PSS.POS_PTA_TMPL_ENTRY(POS_PTA_TMPL_ID, PAYMENT_SUBTYPE_ID, POS_PTA_ACTIVATION_OSET_HR, PAYMENT_SUBTYPE_KEY_ID, POS_PTA_PRIORITY, CURRENCY_CD)
SELECT (SELECT POS_PTA_TMPL_ID FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'SPECIAL MERGE: USAT ISO Prepaid Card - Canada'),
	(SELECT MIN(PAYMENT_SUBTYPE_ID) FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME LIKE '%Error Bin' AND CLIENT_PAYMENT_TYPE_CD = 'N'),
	0, (SELECT TERMINAL_ID FROM PSS.TERMINAL WHERE TERMINAL_DESC = 'Error Bin Terminal'), 
	2, 'CAD' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM PSS.POS_PTA_TMPL_ENTRY 
	WHERE POS_PTA_TMPL_ID = (SELECT POS_PTA_TMPL_ID FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'SPECIAL MERGE: USAT ISO Prepaid Card - Canada')
		AND PAYMENT_SUBTYPE_ID = (SELECT MIN(PAYMENT_SUBTYPE_ID) FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME LIKE '%Error Bin' AND CLIENT_PAYMENT_TYPE_CD = 'N')
);

COMMIT;

UPDATE PSS.CONSUMER_ACCT_FMT
SET CONSUMER_ACCT_FMT_REGEX = '^(6396210([0-9]{3})([0-9]{8})([0-9]{1}))=([0-9]{4})([0-9]{2})([0-9]{5})([0-9]{2})$',
	CONSUMER_ACCT_FMT_REGEX_BREF = '1:1|2:15|3:20|4:13|5:3|6:10|7:16|8:14'
WHERE CONSUMER_ACCT_FMT_NAME = 'Gift/Stored Value Card';

UPDATE PSS.CONSUMER_ACCT_FMT
SET CONSUMER_ACCT_FMT_REGEX = '^(6396211([0-9]{1})([0-9]{3})([0-9]{6})([0-9]{1}))=([0-9]{4})([0-9]{5})$',
	CONSUMER_ACCT_FMT_REGEX_BREF = '1:1|2:17|3:15|4:20|5:13|6:3|7:16'
WHERE CONSUMER_ACCT_FMT_NAME = 'Operator/Driver Maintenace Card';

UPDATE PSS.AUTHORITY_PAYMENT_MASK
SET AUTHORITY_PAYMENT_MASK_REGEX = '^(6396210([0-9]{11})([0-9]{1}))=([0-9]{4})([0-9]{2})([0-9]{5})([0-9]{2})$', AUTHORITY_PAYMENT_MASK_BREF = '1:1|2:20|3:13|4:3|5:10|6:16|7:14'
WHERE AUTHORITY_PAYMENT_MASK_NAME = 'USAT ISO Card (Format Zero)';

UPDATE PSS.AUTHORITY_PAYMENT_MASK
SET AUTHORITY_PAYMENT_MASK_REGEX = '^(6396211([0-9]{1})([0-9]{9})([0-9]{1}))=([0-9]{4})([0-9]{5})$', AUTHORITY_PAYMENT_MASK_BREF = '1:1|2:17|3:20|4:13|5:3|6:16'
WHERE AUTHORITY_PAYMENT_MASK_NAME = 'USAT ISO Card (Format One)';

UPDATE AUTHORITY.HANDLER
SET HANDLER_CLASS = 'Internal'
WHERE HANDLER_NAME = 'USAT Internal Value';

UPDATE AUTHORITY.HANDLER
SET HANDLER_CLASS = 'Internal::Permission'
WHERE HANDLER_NAME = 'USAT Internal Maintenance';

UPDATE PSS.INTERNAL_AUTHORITY
SET AUTHORITY_ID = DECODE(AUTHORITY_SERVICE_TYPE_ID, 2, 5, 4)
WHERE AUTHORITY_ID IS NULL;

UPDATE PSS.PAYMENT_SUBTYPE 
   SET PAYMENT_SUBTYPE_CLASS = 'Internal::Permission'
 WHERE PAYMENT_SUBTYPE_NAME LIKE '%USAT ISO Card - Operator Maintenance Card';
 
COMMIT;

ALTER TABLE PSS.INTERNAL_AUTHORITY MODIFY AUTHORITY_ID NUMBER(20) NOT NULL;
ALTER TABLE PSS.INTERNAL_AUTHORITY ADD CONSTRAINT FK_INTERNAL_AUTHORITY_AUTH_ID FOREIGN KEY (AUTHORITY_ID) REFERENCES AUTHORITY.AUTHORITY(AUTHORITY_ID);
