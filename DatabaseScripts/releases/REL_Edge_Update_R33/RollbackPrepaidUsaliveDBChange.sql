delete from web_content.web_link where web_link_label ='Manage Campaign';

UPDATE WEB_CONTENT.WEB_LINK
   SET WEB_LINK_URL = REGEXP_REPLACE(web_link_url, 'tranType=15%2C%20', 'tranType=')
where web_link_group <> 'User Defined' and web_link_url like '%tranType=%';
commit;