--GRANT SELECT ON PSS.CURRENCY TO REPORT;
-- @TERMINAL_EPORT_UPD.prc


--set serveroutput on
DECLARE
	ld_prev_fill_date REPORT.FILL.FILL_DATE%TYPE;
BEGIN
    
   FOR l_rec in (select f.fill_id, f.eport_id, f.fill_date from report.fill f
join report.terminal_eport te on f.eport_id=te.eport_id
and f.fill_date> te.start_date and f.fill_date<NVL(TE.END_DATE, MAX_DATE) and te.end_date>cast('02-Apr-2013' as DATE)
join report.terminal t on te.terminal_id=t.terminal_id and t.payment_schedule_id=2
where f.fill_id >=(select min(start_fill_id) from  corp.batch_fill) and f.source_system_cd is null and f.terminal_id is null
and f.prev_fill_id in (select start_fill_id from corp.batch_fill where end_fill_id is null)
and f.fill_date=te.end_date- 1/86400)
  loop
  		SELECT MAX(FILL_DATE)
          INTO ld_prev_fill_date
          FROM (
              SELECT FILL_DATE
              FROM REPORT.FILL
            WHERE EPORT_ID =l_rec.eport_id
              AND FILL_DATE < l_rec.fill_date
            ORDER BY FILL_DATE DESC)
         WHERE ROWNUM = 1 ;
        --dbms_output.put_line('previous fill date:'||ld_prev_fill_date);
   		RDW_LOADER.PKG_LOAD_DATA.PROCESS_FILL_INFO(l_rec.fill_date, l_rec.eport_id);
        REPORT.DW_PKG.UPDATE_FILL_INFO(l_rec.fill_id);
        COMMIT;
        CORP.PAYMENTS_PKG.UPDATE_FILL_BATCH(l_rec.fill_id);
        COMMIT;
        RDW_LOADER.PKG_LOAD_DATA.PROCESS_FILL_DATE(ld_prev_fill_date, l_rec.fill_date, l_rec.eport_id);  
        COMMIT;
  end loop;
  
  
END;
/