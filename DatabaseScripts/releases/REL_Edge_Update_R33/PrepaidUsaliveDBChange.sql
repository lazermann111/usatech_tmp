DROP TABLE REPORT.CAMPAIGN_DEVICE;
DROP TABLE REPORT.CAMPAIGN;
DROP TABLE REPORT.CAMPAIGN_TYPE;

BEGIN
    FOLIO_CONF.FOLIO_PKG.DELETE_FOLIO(402); 
END;
/
COMMIT;

CREATE TABLE REPORT.CAMPAIGN_TYPE(
    CAMPAIGN_TYPE_ID NUMBER NOT NULL,
    CAMPAIGN_TYPE_NAME VARCHAR2 (60),
    CAMPAIGN_TYPE_DESC  VARCHAR2 (4000),
    CREATED_BY VARCHAR2(30) NOT NULL,
    CREATED_UTC_TS TIMESTAMP NOT NULL,
    LAST_UPDATED_BY VARCHAR2(30) NOT NULL,
    LAST_UPDATED_UTC_TS TIMESTAMP NOT NULL,
    CONSTRAINT PK_CAMPAIGN_TYPE  PRIMARY KEY(CAMPAIGN_TYPE_ID)
) TABLESPACE REPORT_DATA;


CREATE OR REPLACE TRIGGER REPORT.TRBI_CAMPAIGN_TYPE BEFORE 
INSERT ON REPORT.CAMPAIGN_TYPE FOR EACH ROW 
BEGIN
    SELECT 
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER,
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER
    INTO 
        :NEW.CREATED_UTC_TS,
        :NEW.CREATED_BY,
        :NEW.LAST_UPDATED_UTC_TS,
        :NEW.LAST_UPDATED_BY
    FROM DUAL;
END;  
/

CREATE OR REPLACE TRIGGER REPORT.TRBU_CAMPAIGN_TYPE BEFORE 
UPDATE ON REPORT.CAMPAIGN_TYPE FOR EACH ROW 
BEGIN
    SELECT
        :OLD.CREATED_BY,
        :OLD.CREATED_UTC_TS,
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER
    INTO
        :NEW.CREATED_BY,
        :NEW.CREATED_UTC_TS,
        :NEW.LAST_UPDATED_UTC_TS,
        :NEW.LAST_UPDATED_BY
    FROM DUAL;
END; 
/

INSERT INTO REPORT.CAMPAIGN_TYPE (CAMPAIGN_TYPE_ID, CAMPAIGN_TYPE_NAME,CAMPAIGN_TYPE_DESC)
VALUES(1, 'Prepaid Loyalty', 'Campaign that offers prepaid loyalty discount.');
COMMIT;

CREATE TABLE REPORT.CAMPAIGN(
    CAMPAIGN_ID NUMBER NOT NULL,
    CAMPAIGN_NAME VARCHAR2 (60) NOT NULL,
    CAMPAIGN_DESC  VARCHAR2 (4000),
    CAMPAIGN_TYPE_ID NUMBER NOT NULL,
    CUSTOMER_ID NUMBER,
    START_DATE DATE,
    END_DATE DATE,
    DISCOUNT_PERCENT NUMBER(8,8),
    STATUS CHAR(1) DEFAULT 'A',
    CREATED_BY VARCHAR2(30) NOT NULL,
    CREATED_UTC_TS TIMESTAMP NOT NULL,
    LAST_UPDATED_BY VARCHAR2(30) NOT NULL,
    LAST_UPDATED_UTC_TS TIMESTAMP NOT NULL,
    CONSTRAINT PK_CAMPAIGN PRIMARY KEY(CAMPAIGN_ID),
    CONSTRAINT FK_CAMPAIGN_CAMPAIGN_TYPE_ID FOREIGN KEY (CAMPAIGN_TYPE_ID) REFERENCES REPORT.CAMPAIGN_TYPE(CAMPAIGN_TYPE_ID),
    CONSTRAINT FK_CAMPAIGN_CUSTOMER_ID FOREIGN KEY (CUSTOMER_ID) REFERENCES CORP.CUSTOMER(CUSTOMER_ID)
) TABLESPACE REPORT_DATA;

CREATE UNIQUE INDEX REPORT.UDX_CAMPAIGN_NAME ON REPORT.CAMPAIGN(DECODE(STATUS,'D', NULL,(DECODE(TO_CHAR(CUSTOMER_ID),NULL,CAMPAIGN_NAME,TO_CHAR(CUSTOMER_ID)||CAMPAIGN_NAME)))) TABLESPACE REPORT_INDX;

CREATE OR REPLACE TRIGGER REPORT.TRBI_CAMPAIGN BEFORE 
INSERT ON REPORT.CAMPAIGN FOR EACH ROW 
BEGIN
    SELECT 
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER,
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER
    INTO 
        :NEW.CREATED_UTC_TS,
        :NEW.CREATED_BY,
        :NEW.LAST_UPDATED_UTC_TS,
        :NEW.LAST_UPDATED_BY
    FROM DUAL;
END;  
/

CREATE OR REPLACE TRIGGER REPORT.TRBU_CAMPAIGN BEFORE 
UPDATE ON REPORT.CAMPAIGN FOR EACH ROW 
BEGIN
    SELECT
        :OLD.CREATED_BY,
        :OLD.CREATED_UTC_TS,
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER
    INTO
        :NEW.CREATED_BY,
        :NEW.CREATED_UTC_TS,
        :NEW.LAST_UPDATED_UTC_TS,
        :NEW.LAST_UPDATED_BY
    FROM DUAL;
END; 
/

CREATE TABLE REPORT.CAMPAIGN_ASSIGNMENT(
    CAMPAIGN_ASSIGNMENT_ID NUMBER NOT NULL,
    CAMPAIGN_ID NUMBER NOT NULL,
    CUSTOMER_ID NUMBER,
    REGION_ID NUMBER,
    TERMINAL_ID NUMBER,
    CREATED_BY VARCHAR2(30) NOT NULL,
    CREATED_UTC_TS TIMESTAMP NOT NULL,
    LAST_UPDATED_BY VARCHAR2(30) NOT NULL,
    LAST_UPDATED_UTC_TS TIMESTAMP NOT NULL,
    CONSTRAINT PK_CAMPAIGN_ASSIGNMENT PRIMARY KEY(CAMPAIGN_ASSIGNMENT_ID),
    CONSTRAINT FK_CAM_ASSIGNMENT_CAMPAIGN_ID FOREIGN KEY (CAMPAIGN_ID) REFERENCES REPORT.CAMPAIGN (CAMPAIGN_ID),
    CONSTRAINT FK_CAM_ASSIGNMENT_CUSTOMER_ID FOREIGN KEY (CUSTOMER_ID) REFERENCES CORP.CUSTOMER (CUSTOMER_ID),
    CONSTRAINT FK_CAM_ASSIGNMENT_REGION_ID FOREIGN KEY (REGION_ID) REFERENCES REPORT.REGION (REGION_ID),
    CONSTRAINT FK_CAM_ASSIGNMENT_TERMINAL_ID FOREIGN KEY (TERMINAL_ID) REFERENCES REPORT.TERMINAL (TERMINAL_ID)
) TABLESPACE REPORT_DATA;

CREATE OR REPLACE TRIGGER REPORT.TRBI_CAMPAIGN_ASSIGNMENT BEFORE 
INSERT ON REPORT.CAMPAIGN_ASSIGNMENT FOR EACH ROW 
BEGIN
    SELECT 
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER,
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER
    INTO 
        :NEW.CREATED_UTC_TS,
        :NEW.CREATED_BY,
        :NEW.LAST_UPDATED_UTC_TS,
        :NEW.LAST_UPDATED_BY
    FROM DUAL;
END;  
/

CREATE OR REPLACE TRIGGER REPORT.TRBU_CAMPAIGN_ASSIGNMENT BEFORE 
UPDATE ON REPORT.CAMPAIGN_ASSIGNMENT FOR EACH ROW 
BEGIN
    SELECT
        :OLD.CREATED_BY,
        :OLD.CREATED_UTC_TS,
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER
    INTO
        :NEW.CREATED_BY,
        :NEW.CREATED_UTC_TS,
        :NEW.LAST_UPDATED_UTC_TS,
        :NEW.LAST_UPDATED_BY
    FROM DUAL;
END; 
/

CREATE SEQUENCE REPORT.CAMPAIGN_ASSIGNMENT_SEQ MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE NOORDER NOCYCLE;

GRANT SELECT, REFERENCES on REPORT.CAMPAIGN to PSS;

CREATE TABLE PSS.CAMPAIGN_POS_PTA (
    CAMPAIGN_POS_PTA_ID NUMBER NOT NULL,
    CAMPAIGN_ID NUMBER NOT NULL,
    POS_PTA_ID NUMBER NOT NULL,
    START_DATE DATE,
    END_DATE DATE,
    CREATED_BY VARCHAR2(30) NOT NULL,
    CREATED_UTC_TS TIMESTAMP NOT NULL,
    LAST_UPDATED_BY VARCHAR2(30) NOT NULL,
    LAST_UPDATED_UTC_TS TIMESTAMP NOT NULL,
    CONSTRAINT PK_CAMPAIGN_POS_PTA PRIMARY KEY(CAMPAIGN_POS_PTA_ID),
    CONSTRAINT FK_CAM_POS_PTA_CAMPAIGN_ID FOREIGN KEY (CAMPAIGN_ID) REFERENCES REPORT.CAMPAIGN (CAMPAIGN_ID),
    CONSTRAINT FK_CAM_POS_PTA_POS_PTA_ID FOREIGN KEY (POS_PTA_ID) REFERENCES PSS.POS_PTA (POS_PTA_ID)
)
TABLESPACE PSS_DATA;


CREATE OR REPLACE TRIGGER PSS.TRBI_CAMPAIGN_POS_PTA BEFORE INSERT ON PSS.CAMPAIGN_POS_PTA
  FOR EACH ROW
BEGIN
    SELECT 
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER,
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER
    INTO 
        :NEW.CREATED_UTC_TS,
        :NEW.CREATED_BY,
        :NEW.LAST_UPDATED_UTC_TS,
        :NEW.LAST_UPDATED_BY
    FROM DUAL;
END; 
/

CREATE OR REPLACE TRIGGER PSS.TRBU_CAMPAIGN_POS_PTA BEFORE UPDATE ON PSS.CAMPAIGN_POS_PTA
  FOR EACH ROW 
BEGIN
    SELECT
        :OLD.CREATED_BY,
        :OLD.CREATED_UTC_TS,
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER
    INTO
        :NEW.CREATED_BY,
        :NEW.CREATED_UTC_TS,
        :NEW.LAST_UPDATED_UTC_TS,
        :NEW.LAST_UPDATED_BY
    FROM DUAL;
END; 
/

GRANT SELECT on REPORT.CAMPAIGN_TYPE to USALIVE_APP_ROLE;
GRANT SELECT,INSERT,UPDATE on REPORT.CAMPAIGN to USALIVE_APP_ROLE;
GRANT SELECT,INSERT,UPDATE, DELETE on REPORT.CAMPAIGN_ASSIGNMENT to USALIVE_APP_ROLE;
Grant select on REPORT.CAMPAIGN_ASSIGNMENT_SEQ to USALIVE_APP_ROLE;

-- for prepaid balance report
GRANT SELECT ON PSS.CONSUMER_ACCT TO USALIVE_APP_ROLE;
--folio_prepaid_balance_by_customer.sql
--folio_prepaid_balance_by_customer_field_update.sql

CREATE SEQUENCE PSS.CAMPAIGN_POS_PTA_SEQ MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE NOORDER NOCYCLE;

GRANT SELECT on PSS.POS to REPORT;
GRANT SELECT on PSS.POS_PTA to REPORT;
GRANT SELECT, INSERT, UPDATE, DELETE on PSS.CAMPAIGN_POS_PTA to REPORT;
GRANT SELECT, INSERT, UPDATE, DELETE on PSS.CAMPAIGN_POS_PTA to USALIVE_APP_ROLE;
GRANT SELECT on PSS.CAMPAIGN_POS_PTA_SEQ to REPORT;
GRANT SELECT on pss.payment_subtype to REPORT;


-- REPORT.TRAIUD_TERMINAL_REGION 
-- REPORT.TRAU_CAMPAIGN 

GRANT execute on REPORT.PKG_CUSTOMER_MANAGEMENT to PSS;

CREATE UNIQUE INDEX PSS.UDX_POS_PTA_CAMPAIGN_ID on PSS.CAMPAIGN_POS_PTA(CAMPAIGN_ID, POS_PTA_ID) TABLESPACE PSS_INDX;

CREATE UNIQUE INDEX PSS.UDX_CAMPAIGN_POS_PTA_ID on PSS.CAMPAIGN_POS_PTA(POS_PTA_ID) TABLESPACE PSS_INDX;

-- for PKG_DEVICE_MAINT.pbk to delete from campaign_pos_pta
GRANT DELETE on PSS.CAMPAIGN_POS_PTA to DEVICE;
GRANT SELECT on pss.payment_subtype to DEVICE;

ALTER TABLE ENGINE.OB_EMAIL_QUEUE ADD OB_EMAIL_CONTENT CLOB;

CREATE SEQUENCE REPORT.CAMPAIGN_BLAST_SEQ MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOORDER NOCYCLE;
GRANT SELECT ON REPORT.CAMPAIGN_BLAST_SEQ TO USALIVE_APP_ROLE;

CREATE TABLE REPORT.CAMPAIGN_BLAST(
    CAMPAIGN_BLAST_ID NUMBER(20,0) NOT NULL,
    CAMPAIGN_ID NUMBER NOT NULL,
	USER_ID NUMBER NOT NULL,
    CREATED_BY VARCHAR2(30) NOT NULL,
    CREATED_UTC_TS TIMESTAMP NOT NULL,
    LAST_UPDATED_BY VARCHAR2(30) NOT NULL,
    LAST_UPDATED_UTC_TS TIMESTAMP NOT NULL,
    CONSTRAINT PK_CAMPAIGN_BLAST PRIMARY KEY(CAMPAIGN_BLAST_ID),
    CONSTRAINT FK_CAM_BLAST_CAMPAIGN_ID FOREIGN KEY (CAMPAIGN_ID) REFERENCES REPORT.CAMPAIGN (CAMPAIGN_ID),
    CONSTRAINT FK_CAM_BLAST_USER_ID FOREIGN KEY (USER_ID) REFERENCES REPORT.USER_LOGIN (USER_ID)
) TABLESPACE REPORT_DATA;

CREATE OR REPLACE TRIGGER REPORT.TRBI_CAMPAIGN_BLAST BEFORE 
INSERT ON REPORT.CAMPAIGN_BLAST FOR EACH ROW 
BEGIN
	IF :NEW.CAMPAIGN_BLAST_ID IS NULL THEN
		SELECT REPORT.CAMPAIGN_BLAST_SEQ.NEXTVAL
		INTO :NEW.CAMPAIGN_BLAST_ID
		FROM DUAL;
	END IF;

    SELECT 
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER,
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER
    INTO 
        :NEW.CREATED_UTC_TS,
        :NEW.CREATED_BY,
        :NEW.LAST_UPDATED_UTC_TS,
        :NEW.LAST_UPDATED_BY
    FROM DUAL;
END;  
/

CREATE OR REPLACE TRIGGER REPORT.TRBU_CAMPAIGN_BLAST BEFORE 
UPDATE ON REPORT.CAMPAIGN_BLAST FOR EACH ROW 
BEGIN
    SELECT
        :OLD.CREATED_BY,
        :OLD.CREATED_UTC_TS,
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER
    INTO
        :NEW.CREATED_BY,
        :NEW.CREATED_UTC_TS,
        :NEW.LAST_UPDATED_UTC_TS,
        :NEW.LAST_UPDATED_BY
    FROM DUAL;
END; 
/

GRANT SELECT, INSERT, UPDATE ON REPORT.CAMPAIGN_BLAST TO USALIVE_APP_ROLE;
GRANT SELECT ON REPORT.CAMPAIGN_BLAST TO USAT_RPT_GEN_ROLE, USAT_DEV_READ_ONLY;

CREATE SEQUENCE REPORT.CAMPAIGN_BLAST_CONSUMER_SEQ MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOORDER NOCYCLE;

GRANT SELECT, REFERENCES ON PSS.CONSUMER TO REPORT;

CREATE TABLE REPORT.CAMPAIGN_BLAST_CONSUMER(
    CAMPAIGN_BLAST_CONSUMER_ID NUMBER(20,0) NOT NULL,
	CAMPAIGN_BLAST_ID NUMBER(20,0) NOT NULL,
    CONSUMER_ID NUMBER(20,0) NOT NULL,
	SENT_YN_FLAG VARCHAR2(1) NOT NULL,
    CREATED_BY VARCHAR2(30) NOT NULL,
    CREATED_UTC_TS TIMESTAMP NOT NULL,
    LAST_UPDATED_BY VARCHAR2(30) NOT NULL,
    LAST_UPDATED_UTC_TS TIMESTAMP NOT NULL,
    CONSTRAINT PK_CAMPAIGN_BLAST_CONSUMER PRIMARY KEY(CAMPAIGN_BLAST_CONSUMER_ID),
    CONSTRAINT FK_CAM_BLAST_CONS_CAM_BLAST_ID FOREIGN KEY (CAMPAIGN_BLAST_ID) REFERENCES REPORT.CAMPAIGN_BLAST (CAMPAIGN_BLAST_ID),
    CONSTRAINT FK_CAM_BLAST_CONS_CONSUMER_ID FOREIGN KEY (CONSUMER_ID) REFERENCES PSS.CONSUMER (CONSUMER_ID)
) TABLESPACE REPORT_DATA;

CREATE INDEX REPORT.IDX_CAM_BLAST_CON_CAM_BLAST_ID ON REPORT.CAMPAIGN_BLAST_CONSUMER(CAMPAIGN_BLAST_ID) TABLESPACE REPORT_INDX;
CREATE INDEX REPORT.IDX_CAM_BLAST_CON_CONSUMER_ID ON REPORT.CAMPAIGN_BLAST_CONSUMER(CONSUMER_ID) TABLESPACE REPORT_INDX;

CREATE OR REPLACE TRIGGER REPORT.TRBI_CAMPAIGN_BLAST_CONSUMER BEFORE 
INSERT ON REPORT.CAMPAIGN_BLAST_CONSUMER FOR EACH ROW 
BEGIN
	IF :NEW.CAMPAIGN_BLAST_CONSUMER_ID IS NULL THEN
		SELECT REPORT.CAMPAIGN_BLAST_CONSUMER_SEQ.NEXTVAL
		INTO :NEW.CAMPAIGN_BLAST_CONSUMER_ID
		FROM DUAL;
	END IF;

    SELECT 
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER,
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER
    INTO 
        :NEW.CREATED_UTC_TS,
        :NEW.CREATED_BY,
        :NEW.LAST_UPDATED_UTC_TS,
        :NEW.LAST_UPDATED_BY
    FROM DUAL;
END;  
/

CREATE OR REPLACE TRIGGER REPORT.TRBU_CAMPAIGN_BLAST_CONSUMER BEFORE 
UPDATE ON REPORT.CAMPAIGN_BLAST_CONSUMER FOR EACH ROW 
BEGIN
    SELECT
        :OLD.CREATED_BY,
        :OLD.CREATED_UTC_TS,
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER
    INTO
        :NEW.CREATED_BY,
        :NEW.CREATED_UTC_TS,
        :NEW.LAST_UPDATED_UTC_TS,
        :NEW.LAST_UPDATED_BY
    FROM DUAL;
END; 
/

GRANT SELECT, INSERT, UPDATE ON REPORT.CAMPAIGN_BLAST_CONSUMER TO USAT_RPT_GEN_ROLE;
GRANT SELECT ON REPORT.CAMPAIGN_BLAST_CONSUMER TO USAT_DEV_READ_ONLY;
GRANT SELECT, INSERT, UPDATE ON ENGINE.OB_EMAIL_QUEUE TO USAT_RPT_GEN_ROLE;
GRANT SELECT ON PSS.CONSUMER TO USAT_RPT_GEN_ROLE;
GRANT SELECT ON PSS.CONSUMER_ACCT TO USAT_RPT_GEN_ROLE;
GRANT SELECT ON LOCATION.VW_LOCATION_HIERARCHY TO USAT_RPT_GEN_ROLE;
GRANT SELECT ON PSS.POS TO USAT_RPT_GEN_ROLE;
GRANT SELECT ON PSS.POS_PTA TO USAT_RPT_GEN_ROLE;
GRANT SELECT ON PSS.CAMPAIGN_POS_PTA TO USAT_RPT_GEN_ROLE;
GRANT SELECT ON PSS.PAYMENT_SUBTYPE TO USAT_RPT_GEN_ROLE;
GRANT SELECT ON DEVICE.DEVICE TO USAT_RPT_GEN_ROLE;
GRANT SELECT ON REPORT.CAMPAIGN_BLAST TO USAT_RPT_GEN_ROLE;

ALTER TABLE REPORT.PURCHASE 
ADD(TRAN_LINE_ITEM_TYPE_ID NUMBER(20,0));

GRANT REFERENCES on PSS.TRAN_LINE_ITEM_TYPE to REPORT;

ALTER TABLE REPORT.PURCHASE 
add CONSTRAINT FK_PUR_TRAN_LINE_ITEM_TYPE
  FOREIGN KEY (TRAN_LINE_ITEM_TYPE_ID)
  REFERENCES PSS.TRAN_LINE_ITEM_TYPE(TRAN_LINE_ITEM_TYPE_ID)
  NOVALIDATE;

-- update report.purchase set tran_line_item_type_id=203 where vend_column='Convenience Fee'
  
ALTER TABLE
   REPORT.PRIV
ADD
   (
      	INTERNAL_EXTERNAL_FLAG CHAR(1),
      	CUSTOMER_MASTER_USER_DEFAULT CHAR(1)
   );

update report.priv set INTERNAL_EXTERNAL_FLAG='B' where priv_id in (1,2,3,4,6,7,8,11);

update report.priv set INTERNAL_EXTERNAL_FLAG='I' where priv_id in (5,9,10,12,13,14,15,16,17,18,19,20,21);

update report.priv set CUSTOMER_MASTER_USER_DEFAULT='Y' where priv_id in (1,2,3,4,6,7,8,11);

update report.priv set CUSTOMER_MASTER_USER_DEFAULT='N' where priv_id in (5,9,10,12,13,14,15,16,17,18,19,20,21);

insert into report.priv values(22, 'Manage Campaign', 'E', 'Y');

insert into report.priv values(23, 'Set Batch Close Time', 'E', 'Y');

commit;

BEGIN
	FOR l_cur in (select user_id from report.user_login where status='A' and customer_id>0 and admin_id=0 and user_type=8)
	LOOP
		insert into report.user_privs values(l_cur.user_id, 22);
		insert into report.user_privs values(l_cur.user_id, 23);
	END LOOP;
	commit;
END;
/

insert into web_content.requirement( requirement_id, requirement_name, requirement_class, requirement_param_class, requirement_param_value)
values(32, 'REQ_MANAGE_CAMPAIGN', 'com.usatech.usalive.link.HasPrivilegeRequirement', 'java.lang.String', 'PRIV_MANAGE_CAMPAIGN');

insert into web_content.web_link values(web_content.seq_web_link_id.nextval,'Manage Campaign', './manage_campaign.i','Manage Campaign', 'Setup','M',186);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval,1);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval,4);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval,22);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval,31);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval,32);
commit;

insert into web_content.web_link values(web_content.seq_web_link_id.nextval,'Manage Campaign', './manage_campaign_internal.i','Manage Campaign by internal user', 'Customer Service','M',253);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval,1);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval,14);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval,31);

commit;

insert into web_content.requirement( requirement_id, requirement_name, requirement_class, requirement_param_class, requirement_param_value)
values(33, 'REQ_SET_BATCH_CLOSE_TIME', 'com.usatech.usalive.link.HasPrivilegeRequirement', 'java.lang.String', 'PRIV_SET_BATCH_CLOSE_TIME');


insert into web_content.web_link values(web_content.seq_web_link_id.nextval,'Set Batch Close Time', './set_batch_close_time.i','Set Batch Close Time', 'Setup','M',186);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval,1);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval,4);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval,22);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval,31);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval,33);
commit;

-- R33 Implement generic customer forwarded file type db change
INSERT INTO device.file_transfer_type(file_transfer_type_cd, file_transfer_type_name, file_transfer_type_desc, deletable) 
VALUES(26, 'Custom File Upload', 'The file is sent by the device and we will forward onto the customer','Y');

insert into report.export_type (export_type_id, name, description, status)
values(7, 'FT_EVENT', 'File Transfer Event', 'A');

insert into report.export_type (export_type_id, name, description, status)
values(8, 'CF_UPLOAD', 'Custom File Upload', 'A');

insert into report.generator(generator_id, name)
values(10, 'Custom File Upload');

INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
VALUES( REPORT.REPORTS_SEQ.NEXTVAL, 'Custom File Upload', 10, 8, 'Custom File Upload', 'Custom file upload from the device.', 'E', 0);

commit;

GRANT select on device.file_transfer to REPORT;

CREATE TABLE REPORT.FILE_EXT_TO_CONTENT_TYPE(
    FILE_EXT VARCHAR2 (60),
    FILE_CONTENT_TYPE  VARCHAR2 (200),
    CREATED_BY VARCHAR2(30) NOT NULL,
    CREATED_UTC_TS TIMESTAMP NOT NULL,
    LAST_UPDATED_BY VARCHAR2(30) NOT NULL,
    LAST_UPDATED_UTC_TS TIMESTAMP NOT NULL,
    CONSTRAINT PK_FILE_EXT_TO_CONTENT_TYPE PRIMARY KEY(FILE_EXT)
) TABLESPACE REPORT_DATA;

GRANT select on report.file_ext_to_content_type to USALIVE_APP_ROLE;


CREATE OR REPLACE TRIGGER REPORT.TRBI_FILE_EXT_TO_CONTENT_TYPE BEFORE 
INSERT ON REPORT.FILE_EXT_TO_CONTENT_TYPE FOR EACH ROW 
BEGIN
    SELECT 
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER,
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER
    INTO 
        :NEW.CREATED_UTC_TS,
        :NEW.CREATED_BY,
        :NEW.LAST_UPDATED_UTC_TS,
        :NEW.LAST_UPDATED_BY
    FROM DUAL;
END;  
/

CREATE OR REPLACE TRIGGER REPORT.TRBU_FILE_EXT_TO_CONTENT_TYPE BEFORE 
UPDATE ON REPORT.FILE_EXT_TO_CONTENT_TYPE FOR EACH ROW 
BEGIN
    SELECT
        :OLD.CREATED_BY,
        :OLD.CREATED_UTC_TS,
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER
    INTO
        :NEW.CREATED_BY,
        :NEW.CREATED_UTC_TS,
        :NEW.LAST_UPDATED_UTC_TS,
        :NEW.LAST_UPDATED_BY
    FROM DUAL;
END; 
/

insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.3dm', 'x-world/x-3dmf');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.3dmf', 'x-world/x-3dmf');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.a', 'application/octet-stream');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.aab', 'application/x-authorware-bin');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.aam', 'application/x-authorware-map');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.aas', 'application/x-authorware-seg');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.abc', 'text/vnd.abc');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.acgi', 'text/html');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.afl', 'video/animaflex');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.ai', 'application/postscript');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.aif', 'audio/aiff');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.aifc', 'audio/aiff');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.aiff', 'audio/aiff');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.aim', 'application/x-aim');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.aip', 'text/x-audiosoft-intra');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.ani', 'application/x-navi-animation');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.aos', 'application/x-nokia-9000-communicator-add-on-software');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.aps', 'application/mime');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.arc', 'application/octet-stream');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.arj', 'application/arj');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.art', 'image/x-jg');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.asf', 'video/x-ms-asf');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.asm', 'text/x-asm');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.asp', 'text/asp');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.asx', 'application/x-mplayer2');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.au', 'audio/basic');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.avi', 'video/avi');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.avs', 'video/avs-video');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.bcpio', 'application/x-bcpio');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.bin', 'application/octet-stream');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.bm', 'image/bmp');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.bmp', 'image/bmp');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.boo', 'application/book');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.book', 'application/book');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.boz', 'application/x-bzip2');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.bsh', 'application/x-bsh');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.bz', 'application/x-bzip');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.bz2', 'application/x-bzip2');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.c', 'text/plain');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.c++', 'text/plain');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.cat', 'application/vnd.ms-pki.seccat');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.cc', 'text/plain');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.ccad', 'application/clariscad');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.cco', 'application/x-cocoa');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.cdf', 'application/cdf');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.cer', 'application/pkix-cert');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.cha', 'application/x-chat');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.chat', 'application/x-chat');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.class', 'application/java');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.com', 'application/octet-stream');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.conf', 'text/plain');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.cpio', 'application/x-cpio');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.cpp', 'text/x-c');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.cpt', 'application/mac-compactpro');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.crl', 'application/pkcs-crl');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.crt', 'application/pkix-cert');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.csh', 'application/x-csh');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.css', 'text/css');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.cxx', 'text/plain');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.dcr', 'application/x-director');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.deepv', 'application/x-deepv');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.def', 'text/plain');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.der', 'application/x-x509-ca-cert');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.dif', 'video/x-dv');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.dir', 'application/x-director');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.dl', 'video/dl');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.doc', 'application/msword');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.dot', 'application/msword');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.dp', 'application/commonground');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.drw', 'application/drafting');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.dump', 'application/octet-stream');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.dv', 'video/x-dv');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.dvi', 'application/x-dvi');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.dwf', 'drawing/x-dwf');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.dwg', 'application/acad');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.dxf', 'application/dxf');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.dxr', 'application/x-director');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.el', 'text/x-script.elisp');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.elc', 'application/x-bytecode.elisp');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.env', 'application/x-envoy');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.eps', 'application/postscript');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.es', 'application/x-esrehber');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.etx', 'text/x-setext');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.evy', 'application/envoy');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.exe', 'application/octet-stream');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.f', 'text/plain');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.f77', 'text/x-fortran');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.f90', 'text/plain');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.fdf', 'application/vnd.fdf');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.fif', 'application/fractals');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.fli', 'video/fli');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.flo', 'image/florian');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.flx', 'text/vnd.fmi.flexstor');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.fmf', 'video/x-atomic3d-feature');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.for', 'text/plain');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.fpx', 'image/vnd.fpx');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.frl', 'application/freeloader');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.funk', 'audio/make');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.g', 'text/plain');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.g3', 'image/g3fax');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.gif', 'image/gif');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.gl', 'video/gl');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.gsd', 'audio/x-gsm');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.gsm', 'audio/x-gsm');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.gsp', 'application/x-gsp');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.gss', 'application/x-gss');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.gtar', 'application/x-gtar');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.gz', 'application/x-compressed');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.gzip', 'application/x-gzip');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.h', 'text/plain');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.hdf', 'application/x-hdf');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.help', 'application/x-helpfile');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.hgl', 'application/vnd.hp-hpgl');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.hh', 'text/plain');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.hlb', 'text/x-script');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.hlp', 'application/hlp');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.hpg', 'application/vnd.hp-hpgl');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.hpgl', 'application/vnd.hp-hpgl');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.hqx', 'application/binhex');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.hta', 'application/hta');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.htc', 'text/x-component');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.htm', 'text/html');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.html', 'text/html');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.htmls', 'text/html');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.htt', 'text/webviewhtml');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.htx', 'text/html');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.ice', 'x-conference/x-cooltalk');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.ico', 'image/x-icon');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.idc', 'text/plain');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.ief', 'image/ief');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.iefs', 'image/ief');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.iges', 'application/iges');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.igs', 'application/iges');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.ima', 'application/x-ima');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.imap', 'application/x-httpd-imap');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.inf', 'application/inf');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.ins', 'application/x-internett-signup');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.ip', 'application/x-ip2');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.isu', 'video/x-isvideo');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.it', 'audio/it');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.iv', 'application/x-inventor');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.ivr', 'i-world/i-vrml');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.ivy', 'application/x-livescreen');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.jam', 'audio/x-jam');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.jav', 'text/plain');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.java', 'text/plain');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.jcm', 'application/x-java-commerce');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.jfif', 'image/jpeg');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.jfif-tbnl', 'image/jpeg');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.jpe', 'image/jpeg');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.jpeg', 'image/jpeg');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.jpg', 'image/jpeg');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.jps', 'image/x-jps');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.js', 'application/x-javascript');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.jut', 'image/jutvision');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.kar', 'audio/midi');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.ksh', 'application/x-ksh');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.la', 'audio/nspaudio');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.lam', 'audio/x-liveaudio');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.latex', 'application/x-latex');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.lha', 'application/lha');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.lhx', 'application/octet-stream');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.list', 'text/plain');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.lma', 'audio/nspaudio');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.log', 'text/plain');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.lsp', 'application/x-lisp');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.lst', 'text/plain');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.lsx', 'text/x-la-asf');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.ltx', 'application/x-latex');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.lzh', 'application/octet-stream');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.lzx', 'application/lzx');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.m', 'text/plain');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.m1v', 'video/mpeg');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.m2a', 'audio/mpeg');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.m2v', 'video/mpeg');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.m3u', 'audio/x-mpequrl');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.man', 'application/x-troff-man');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.map', 'application/x-navimap');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.mar', 'text/plain');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.mbd', 'application/mbedlet');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.mc$', 'application/x-magic-cap-package-1.0');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.mcd', 'application/mcad');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.mcf', 'image/vasa');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.mcp', 'application/netmc');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.me', 'application/x-troff-me');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.mht', 'message/rfc822');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.mhtml', 'message/rfc822');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.mid', 'application/x-midi');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.midi', 'application/x-midi');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.mif', 'application/x-frame');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.mime', 'message/rfc822');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.mjf', 'audio/x-vnd.audioexplosion.mjuicemediafile');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.mjpg', 'video/x-motion-jpeg');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.mm', 'application/base64');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.mme', 'application/base64');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.mod', 'audio/mod');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.moov', 'video/quicktime');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.mov', 'video/quicktime');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.movie', 'video/x-sgi-movie');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.mp2', 'audio/mpeg');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.mp3', 'audio/mpeg3');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.mpa', 'audio/mpeg');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.mpc', 'application/x-project');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.mpe', 'video/mpeg');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.mpeg', 'video/mpeg');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.mpg', 'audio/mpeg');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.mpga', 'audio/mpeg');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.mpp', 'application/vnd.ms-project');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.mpt', 'application/x-project');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.mpv', 'application/x-project');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.mpx', 'application/x-project');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.mrc', 'application/marc');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.ms', 'application/x-troff-ms');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.mv', 'video/x-sgi-movie');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.my', 'audio/make');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.mzz', 'application/x-vnd.audioexplosion.mzz');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.nap', 'image/naplps');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.naplps', 'image/naplps');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.nc', 'application/x-netcdf');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.ncm', 'application/vnd.nokia.configuration-message');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.nif', 'image/x-niff');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.niff', 'image/x-niff');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.nix', 'application/x-mix-transfer');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.nsc', 'application/x-conference');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.nvd', 'application/x-navidoc');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.o', 'application/octet-stream');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.oda', 'application/oda');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.omc', 'application/x-omc');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.omcd', 'application/x-omcdatamaker');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.omcr', 'application/x-omcregerator');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.p', 'text/x-pascal');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.p10', 'application/pkcs10');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.p12', 'application/pkcs-12');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.p7a', 'application/x-pkcs7-signature');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.p7c', 'application/pkcs7-mime');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.p7m', 'application/pkcs7-mime');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.p7r', 'application/x-pkcs7-certreqresp');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.p7s', 'application/pkcs7-signature');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.part', 'application/pro_eng');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.pas', 'text/pascal');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.pbm', 'image/x-portable-bitmap');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.pcl', 'application/vnd.hp-pcl');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.pct', 'image/x-pict');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.pcx', 'image/x-pcx');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.pdb', 'chemical/x-pdb');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.pdf', 'application/pdf');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.pfunk', 'audio/make');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.pgm', 'image/x-portable-graymap');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.pic', 'image/pict');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.pict', 'image/pict');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.pkg', 'application/x-newton-compatible-pkg');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.pko', 'application/vnd.ms-pki.pko');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.pl', 'text/plain');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.plx', 'application/x-pixclscript');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.pm', 'image/x-xpixmap');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.pm4', 'application/x-pagemaker');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.pm5', 'application/x-pagemaker');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.png', 'image/png');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.pnm', 'application/x-portable-anymap');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.pot', 'application/mspowerpoint');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.pov', 'model/x-pov');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.ppa', 'application/vnd.ms-powerpoint');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.ppm', 'image/x-portable-pixmap');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.pps', 'application/mspowerpoint');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.ppt', 'application/mspowerpoint');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.ppz', 'application/mspowerpoint');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.pre', 'application/x-freelance');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.prt', 'application/pro_eng');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.ps', 'application/postscript');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.psd', 'application/octet-stream');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.pvu', 'paleovu/x-pv');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.pwz', 'application/vnd.ms-powerpoint');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.py', 'text/x-script.phyton');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.pyc', 'applicaiton/x-bytecode.python');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.qcp', 'audio/vnd.qcelp');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.qd3', 'x-world/x-3dmf');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.qd3d', 'x-world/x-3dmf');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.qif', 'image/x-quicktime');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.qt', 'video/quicktime');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.qtc', 'video/x-qtc');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.qti', 'image/x-quicktime');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.qtif', 'image/x-quicktime');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.ra', 'audio/x-pn-realaudio');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.ram', 'audio/x-pn-realaudio');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.ras', 'application/x-cmu-raster');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.rast', 'image/cmu-raster');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.rexx', 'text/x-script.rexx');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.rf', 'image/vnd.rn-realflash');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.rgb', 'image/x-rgb');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.rm', 'application/vnd.rn-realmedia');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.rmi', 'audio/mid');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.rmm', 'audio/x-pn-realaudio');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.rmp', 'audio/x-pn-realaudio');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.rng', 'application/ringing-tones');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.rnx', 'application/vnd.rn-realplayer');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.roff', 'application/x-troff');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.rp', 'image/vnd.rn-realpix');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.rpm', 'audio/x-pn-realaudio-plugin');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.rt', 'text/richtext');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.rtf', 'application/rtf');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.rtx', 'application/rtf');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.rv', 'video/vnd.rn-realvideo');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.s', 'text/x-asm');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.s3m', 'audio/s3m');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.saveme', 'application/octet-stream');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.sbk', 'application/x-tbook');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.scm', 'application/x-lotusscreencam');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.sdml', 'text/plain');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.sdp', 'application/sdp');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.sdr', 'application/sounder');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.sea', 'application/sea');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.set', 'application/set');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.sgm', 'text/sgml');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.sgml', 'text/sgml');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.sh', 'application/x-bsh');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.shar', 'application/x-bsh');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.shtml', 'text/html');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.sid', 'audio/x-psid');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.sit', 'application/x-sit');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.skd', 'application/x-koan');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.skm', 'application/x-koan');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.skp', 'application/x-koan');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.skt', 'application/x-koan');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.sl', 'application/x-seelogo');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.smi', 'application/smil');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.smil', 'application/smil');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.snd', 'audio/basic');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.sol', 'application/solids');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.spc', 'application/x-pkcs7-certificates');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.spl', 'application/futuresplash');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.spr', 'application/x-sprite');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.sprite', 'application/x-sprite');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.src', 'application/x-wais-source');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.ssi', 'text/x-server-parsed-html');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.ssm', 'application/streamingmedia');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.sst', 'application/vnd.ms-pki.certstore');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.step', 'application/step');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.stl', 'application/sla');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.stp', 'application/step');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.sv4cpio', 'application/x-sv4cpio');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.sv4crc', 'application/x-sv4crc');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.svf', 'image/vnd.dwg');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.svr', 'application/x-world');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.swf', 'application/x-shockwave-flash');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.t', 'application/x-troff');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.talk', 'text/x-speech');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.tar', 'application/x-tar');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.tbk', 'applicationoolbook');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.tcl', 'application/x-tcl');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.tcsh', 'text/x-script.tcsh');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.tex', 'application/x-tex');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.texi', 'application/x-texinfo');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.texinfo', 'application/x-texinfo');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.text', 'text/plain');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.tgz', 'application/x-compressed');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.tif', 'imageiff');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.tiff', 'imageiff');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.tr', 'application/x-troff');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.tsi', 'audiosp-audio');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.tsp', 'application/dsptype');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.tsv', 'textab-separated-values');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.turbot', 'image/florian');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.txt', 'text/plain');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.uil', 'text/x-uil');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.uni', 'text/uri-list');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.unis', 'text/uri-list');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.unv', 'application/i-deas');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.uri', 'text/uri-list');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.uris', 'text/uri-list');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.ustar', 'application/x-ustar');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.uu', 'application/octet-stream');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.uue', 'text/x-uuencode');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.vcd', 'application/x-cdlink');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.vcs', 'text/x-vcalendar');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.vda', 'application/vda');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.vdo', 'video/vdo');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.vew', 'application/groupwise');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.viv', 'video/vivo');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.vivo', 'video/vivo');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.vmd', 'application/vocaltec-media-desc');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.vmf', 'application/vocaltec-media-file');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.voc', 'audio/voc');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.vos', 'video/vosaic');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.vox', 'audio/voxware');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.vqe', 'audio/x-twinvq-plugin');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.vqf', 'audio/x-twinvq');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.vql', 'audio/x-twinvq-plugin');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.vrml', 'application/x-vrml');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.vrt', 'x-world/x-vrt');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.vsd', 'application/x-visio');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.vst', 'application/x-visio');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.vsw', 'application/x-visio');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.w60', 'application/wordperfect6.0');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.w61', 'application/wordperfect6.1');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.w6w', 'application/msword');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.wav', 'audio/wav');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.wb1', 'application/x-qpro');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.wbmp', 'image/vnd.wap.wbmp');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.web', 'application/vnd.xara');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.wiz', 'application/msword');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.wk1', 'application/x-123');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.wmf', 'windows/metafile');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.wml', 'text/vnd.wap.wml');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.wmlc', 'application/vnd.wap.wmlc');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.wmls', 'text/vnd.wap.wmlscript');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.wmlsc', 'application/vnd.wap.wmlscriptc');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.word', 'application/msword');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.wp', 'application/wordperfect');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.wp5', 'application/wordperfect');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.wp6', 'application/wordperfect');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.wpd', 'application/wordperfect');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.wq1', 'application/x-lotus');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.wri', 'application/mswrite');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.wrl', 'application/x-world');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.wrz', 'model/vrml');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.wsc', 'text/scriplet');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.wsrc', 'application/x-wais-source');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.wtk', 'application/x-wintalk');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.x-png', 'image/png');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.xbm', 'image/x-xbitmap');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.xdr', 'video/x-amt-demorun');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.xgz', 'xgl/drawing');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.xif', 'image/vnd.xiff');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.xl', 'application/excel');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.xla', 'application/excel');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.xlb', 'application/excel');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.xlc', 'application/excel');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.xld', 'application/excel');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.xlk', 'application/excel');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.xll', 'application/excel');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.xlm', 'application/excel');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.xls', 'application/excel');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.xlt', 'application/excel');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.xlv', 'application/excel');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.xlw', 'application/excel');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.xm', 'audio/xm');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.xml', 'text/xml');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.xmz', 'xgl/movie');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.xpix', 'application/x-vnd.ls-xpix');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.xpm', 'image/x-xpixmap');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.xsr', 'video/x-amt-showrun');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.xwd', 'image/x-xwd');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.xyz', 'chemical/x-pdb');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.z', 'application/x-compress');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.zip', 'application/x-compressed');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.zoo', 'application/octet-stream');
insert into REPORT.FILE_EXT_TO_CONTENT_TYPE(FILE_EXT, FILE_CONTENT_TYPE) values('.zsh', 'text/x-script.zsh');

commit;

-- batch close time config change
alter table
	corp.customer
add batch_close_time varchar2(8);

GRANT UPDATE ON corp.customer to USALIVE_APP_ROLE;
GRANT EXECUTE on REPORT.DW_PKG to USAT_RPT_REQ_ROLE;

INSERT INTO WEB_CONTENT.WEB_LINK(WEB_LINK_ID,WEB_LINK_LABEL,WEB_LINK_URL,WEB_LINK_DESC,WEB_LINK_GROUP,WEB_LINK_USAGE,WEB_LINK_ORDER) 
    VALUES(172, 'New Customer','./new_customer.i','Create a New Customer','Administration','M',172);

INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT(WEB_LINK_ID, REQUIREMENT_ID)
    SELECT 172, COLUMN_VALUE FROM TABLE(NUMBER_TABLE(1,14));

COMMIT;

ALTER TABLE CORP.COUNTRY ADD(POSTAL_PRIMARY_PARTS NUMBER(3));

UPDATE CORP.COUNTRY 
   SET POSTAL_REGEX = '^(\d{5})\s*(\d{4})?$',
       POSTAL_MASK = '99999 9999',
       POSTAL_PRIMARY_PARTS = 1
 WHERE COUNTRY_CD = 'US'
   AND (POSTAL_REGEX != '^(\d{5})-?(\d{4})?$'
    OR NVL(POSTAL_PRIMARY_PARTS, 0) != 1
    OR POSTAL_MASK != '99999 9999');
   
UPDATE CORP.COUNTRY 
   SET POSTAL_REGEX = '^([A-Z][0-9][A-Z])\s*([0-9][A-Z][0-9])$',
       POSTAL_PRIMARY_PARTS = 2
 WHERE COUNTRY_CD = 'CA'
   AND (POSTAL_REGEX != '^([A-Z][0-9][A-Z])\s*([0-9][A-Z][0-9])$'
    OR NVL(POSTAL_PRIMARY_PARTS, 0) != 2);
 
UPDATE CORP.COUNTRY 
   SET POSTAL_REGEX = '^([A-Z]{2})\s*(\d{2})$',
       POSTAL_PRIMARY_PARTS = 2
 WHERE COUNTRY_CD = 'BM'
   AND (POSTAL_REGEX != '^([A-Z]{2})\s*(\d{2})$'
    OR NVL(POSTAL_PRIMARY_PARTS, 0) != 2);

UPDATE CORP.COUNTRY 
   SET POSTAL_PRIMARY_PARTS = 1
 WHERE COUNTRY_CD = 'GU'
    AND NVL(POSTAL_PRIMARY_PARTS, 0) != 1;
 
COMMIT;


GRANT SELECT on REPORT.CAMPAIGN_TYPE to USAT_DEV_READ_ONLY;
GRANT SELECT on REPORT.CAMPAIGN to USAT_DEV_READ_ONLY;
GRANT SELECT on REPORT.CAMPAIGN_ASSIGNMENT to USAT_DEV_READ_ONLY;
GRANT SELECT on PSS.CAMPAIGN_POS_PTA to USAT_DEV_READ_ONLY;
GRANT SELECT on REPORT.FILE_EXT_TO_CONTENT_TYPE to USAT_DEV_READ_ONLY;

-- add tranType 15 prepaid for web_link reports
UPDATE WEB_CONTENT.WEB_LINK
   SET WEB_LINK_URL = REGEXP_REPLACE(web_link_url, 'tranType=', 'tranType=15%2C%20')
where web_link_group <> 'User Defined' and web_link_url like '%tranType=%';
commit;

-- more grant for add cards to campaign
GRANT SELECT ON PSS.CONSUMER TO USALIVE_APP_ROLE;
GRANT SELECT ON PSS.CONSUMER_ACCT TO REPORT;