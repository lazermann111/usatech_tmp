set serveroutput on
DECLARE
	ld_prev_fill_date REPORT.FILL.FILL_DATE%TYPE;
BEGIN
    
   FOR l_rec in (select f.fill_id, f.eport_id, f.fill_date from report.fill f where f.fill_id=8732449)
  loop
      dbms_output.put_line('current fill date:'||l_rec.fill_date||' eport_id:'||l_rec.eport_id||' fill_date:'||l_rec.fill_date||' fill_id:'||l_rec.fill_id);
  		SELECT MAX(FILL_DATE)
          INTO ld_prev_fill_date
          FROM (
              SELECT FILL_DATE
              FROM REPORT.FILL
            WHERE EPORT_ID =l_rec.eport_id
              AND FILL_DATE < l_rec.fill_date
            ORDER BY FILL_DATE DESC)
         WHERE ROWNUM = 1 ;
         dbms_output.put_line('previous fill date:'||ld_prev_fill_date);
   		   RDW_LOADER.PKG_LOAD_DATA.PROCESS_FILL_INFO(l_rec.fill_date, l_rec.eport_id);
        REPORT.DW_PKG.UPDATE_FILL_INFO(l_rec.fill_id);
        COMMIT;
        CORP.PAYMENTS_PKG.UPDATE_FILL_BATCH(l_rec.fill_id);
        COMMIT;
        RDW_LOADER.PKG_LOAD_DATA.PROCESS_FILL_DATE(ld_prev_fill_date, l_rec.fill_date, l_rec.eport_id);  
        COMMIT;
  end loop;
  
  
END;
/

--select * from corp.batch_fill where end_fill_id=8732449
--select * from corp.batch where batch_id=39674233