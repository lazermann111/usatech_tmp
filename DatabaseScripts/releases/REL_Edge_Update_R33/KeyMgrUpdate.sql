DROP FUNCTION KM.RETRIEVE_ENCRYPTED(
    pn_encrypted_id KM.ENCRYPTED.ENCRYPTED_ID%TYPE);
    
CREATE OR REPLACE FUNCTION KM.RETRIEVE_ENCRYPTED(
    pn_encrypted_id KM.ENCRYPTED.ENCRYPTED_ID%TYPE,
    pn_key_id OUT KM.ENCRYPTED.KEY_ID%TYPE,
    pba_encrypted_data OUT KM.ENCRYPTED.ENCRYPTED_DATA%TYPE,
    pv_user_name OUT KM.ENCRYPTED.USER_NAME%TYPE,
    pd_expiration_utc_ts OUT KM.ENCRYPTED.EXPIRATION_UTC_TS%TYPE)
    SECURITY DEFINER
AS $$
DECLARE
BEGIN
    SELECT KEY_ID, ENCRYPTED_DATA, USER_NAME, EXPIRATION_UTC_TS
      INTO pn_key_id, pba_encrypted_data, pv_user_name, pd_expiration_utc_ts
      FROM KM.ENCRYPTED
     WHERE ENCRYPTED_ID = pn_encrypted_id;        
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION KM.RETRIEVE_ENCRYPTED(
    pn_encrypted_id KM.ENCRYPTED.ENCRYPTED_ID%TYPE) TO use_km;

REVOKE EXECUTE ON FUNCTION KM.RETRIEVE_ENCRYPTED(
    pn_encrypted_id KM.ENCRYPTED.ENCRYPTED_ID%TYPE) FROM public;
    
CREATE OR REPLACE FUNCTION KM.DROP_OLD_PARTITION(
    pd_expiration_utc_ts KM.ENCRYPTED.EXPIRATION_UTC_TS%TYPE)
    RETURNS INT
    SECURITY DEFINER
AS $$
DECLARE
    lv_encrypted_partition VARCHAR;
    ld_min_expiration_utc_ts KM.ENCRYPTED.EXPIRATION_UTC_TS%TYPE;
    ld_start_expiration_utc_ts KM.ENCRYPTED.EXPIRATION_UTC_TS%TYPE;
    ld_end_expiration_utc_ts KM.ENCRYPTED.EXPIRATION_UTC_TS%TYPE;
    ld_max_expiration_utc_ts KM.ENCRYPTED.EXPIRATION_UTC_TS%TYPE;
    ln_cnt INT := 0;
BEGIN
    SELECT MIN(EXPIRATION_UTC_TS)
      INTO ld_min_expiration_utc_ts
      FROM KM.ENCRYPTED;
    WHILE ld_min_expiration_utc_ts < pd_expiration_utc_ts LOOP
        SELECT a.pv_encrypted_partition, a.pn_start_expiration_utc_ts, a.pn_end_expiration_utc_ts
          INTO lv_encrypted_partition, ld_start_expiration_utc_ts, ld_end_expiration_utc_ts
          FROM KM.GET_ENCRYPTED_PARITION(ld_min_expiration_utc_ts) a;
        EXECUTE 'SELECT MAX(EXPIRATION_UTC_TS) FROM KM.' || lv_encrypted_partition INTO ld_max_expiration_utc_ts;
        IF ld_max_expiration_utc_ts IS NULL OR ld_max_expiration_utc_ts < pd_expiration_utc_ts THEN  
            BEGIN
                EXECUTE 'DROP TABLE KM.' || lv_encrypted_partition;
                ln_cnt := ln_cnt + 1;
            EXCEPTION
                WHEN undefined_table THEN
                    NULL;
            END;
        END IF;
        ld_min_expiration_utc_ts := ld_end_expiration_utc_ts;
    END LOOP;
    RETURN  ln_cnt;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION KM.CREATE_ENCRYPTED_PARITION(
    pv_encrypted_partition VARCHAR,
    pn_start_expiration_utc_ts KM.ENCRYPTED.EXPIRATION_UTC_TS%TYPE,
    pn_end_expiration_utc_ts KM.ENCRYPTED.EXPIRATION_UTC_TS%TYPE)
    RETURNS VOID
    SECURITY DEFINER
AS $$
BEGIN
    EXECUTE 'CREATE TABLE KM.' || pv_encrypted_partition || '('
        || 'CONSTRAINT CK_' || pv_encrypted_partition || ' CHECK (EXPIRATION_UTC_TS >= TIMESTAMP''' || pn_start_expiration_utc_ts ||  ''' AND EXPIRATION_UTC_TS < TIMESTAMP''' || pn_end_expiration_utc_ts || '''),'
        || 'PRIMARY KEY(ENCRYPTED_ID)'
        || ') INHERITS(KM.ENCRYPTED)';
    --EXECUTE 'GRANT SELECT, INSERT ON MQ.'|| pv_encrypted_partition || ' TO use_km';
    --EXECUTE 'ALTER TABLE MQ.'|| pv_encrypted_partition || ' OWNER TO use_km';
    EXECUTE 'CREATE INDEX IX_' || pv_encrypted_partition || '_KEY_ID ON KM.' || pv_encrypted_partition || '(KEY_ID)';
    EXECUTE 'CREATE INDEX IX_' || pv_encrypted_partition || '_EXPIRATION_UTC_TS ON KM.' || pv_encrypted_partition || '(EXPIRATION_UTC_TS)';
EXCEPTION
    WHEN duplicate_table THEN
       RETURN; 
END;
$$ LANGUAGE plpgsql;