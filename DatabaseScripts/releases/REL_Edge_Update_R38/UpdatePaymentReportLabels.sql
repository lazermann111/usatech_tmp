UPDATE FOLIO_CONF.FOLIO_PILLAR
   SET PILLAR_LABEL = REPLACE(PILLAR_LABEL, 'Convenience Fee', 'Two-Tier Pricing')
-- SELECT * FROM FOLIO_CONF.FOLIO_PILLAR 
 WHERE PILLAR_LABEL LIKE '%Convenience Fee%'
   AND FOLIO_ID NOT IN(1039, 1156, 1197, 1521);

UPDATE FOLIO_CONF.FOLIO_PILLAR FP
   SET PILLAR_LABEL = REPLACE(PILLAR_LABEL, 'Credit', 'Revenue')
-- SELECT * FROM FOLIO_CONF.FOLIO_PILLAR FP
 WHERE PILLAR_LABEL LIKE '%Credit%'
   AND EXISTS(SELECT 1 FROM FOLIO_CONF.FOLIO_PILLAR_FIELD FPF JOIN FOLIO_CONF.FIELD FI ON FPF.FIELD_ID = FI.FIELD_ID WHERE DISPLAY_EXPRESSION LIKE '%PAYMENT_ENTRY_TYPE_CD =''CC''%' OR DISPLAY_EXPRESSION LIKE '%ENTRY_TYPE = ''CC''%' AND FPF.FOLIO_PILLAR_ID = FP.FOLIO_PILLAR_ID) 
   AND FOLIO_ID NOT IN(366, 437, 1039);

COMMIT;

/*   
select distinct fp.folio_id, fp.pillar_label, fi.* 
from folio_conf.folio_pillar fp
join folio_conf.folio_pillar_field fpf on fp.folio_pillar_id = fpf.folio_pillar_id
join folio_conf.field fi on fpf.field_id = fi.field_id
where UPPER(fp.pillar_label) like '%CONVENIENCE FEE%'
and fp.folio_id not in(1039, 1156, 1197, 1521);


select distinct f.*, fp.pillar_label, fi.* , rp.*, r.*, u.*, ur.*
from folio_conf.folio_pillar fp
join folio_conf.folio_pillar_field fpf on fp.folio_pillar_id = fpf.folio_pillar_id
join folio_conf.field fi on fpf.field_id = fi.field_id
join folio_conf.folio f on fp.folio_id = f.folio_id
left outer join FOLIO_CONF.report_folio rf on f.folio_id = rf.folio_id
join REPORT.report_param rp on (rp.param_name = 'folioId' and rp.param_value = f.folio_id) OR  (rp.param_name = 'reportId' and rp.param_value = rf.report_id)
join REPORT.reports r on rp.report_id = r.report_id
join report.user_report ur on r.report_id = ur.report_id
join report.user_login u on ur.user_id = u.user_id
where ur.status != 'D'
and u.status != 'D'
and fp.pillar_label like '%Credit%'
and (fi.display_expression like '%PAYMENT_ENTRY_TYPE_CD =''CC''%' OR fi.display_expression like '%ENTRY_TYPE = ''CC''%') 
and ((f.default_output_type_id = 21 and not exists(select 1 from FOLIO_CONF.folio_directive fd join FOLIO_CONF.directive D on fd.directive_id = d.directive_id
where f.folio_id = fd.folio_id and d.directive_name = 'show-header' and fd.directive_value = 'false'))
OR (exists(select 1 from REPORT.report_param rpo WHERE r.report_id = rpo.report_id and rpo.param_name = 'outputType' and rpo.param_value = '21')
and not exists(select 1 from REPORT.report_param rpsh WHERE r.report_id = rpsh.report_id and rpsh.param_name = 'directiveNames' AND rpsh.PARAM_VALUE LIKE '%show-header%')))
;
*/