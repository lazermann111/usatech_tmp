WHENEVER SQLERROR EXIT FAILURE COMMIT;

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/releases/REL_Edge_Update_R38/R38.USAPDB.PostSteps.1.sql?rev=HEAD
-- Enqueue data sync for devices with the USALive time zone different from DMS time zone
DECLARE
    CURSOR L_CUR IS 
		SELECT D.DEVICE_ID, D.DEVICE_NAME
        FROM DEVICE.DEVICE D
		JOIN REPORT.EPORT E ON D.DEVICE_SERIAL_CD = E.EPORT_SERIAL_NUM
		JOIN REPORT.VW_TERMINAL_EPORT TE ON E.EPORT_ID = TE.EPORT_ID
		JOIN REPORT.TERMINAL T ON TE.TERMINAL_ID = T.TERMINAL_ID
		JOIN PSS.POS P ON D.DEVICE_ID = P.DEVICE_ID
	    JOIN LOCATION.LOCATION L ON P.LOCATION_ID = L.LOCATION_ID
	    JOIN LOCATION.TIME_ZONE TZ ON L.LOCATION_TIME_ZONE_CD = TZ.TIME_ZONE_CD
		JOIN REPORT.TIME_ZONE T_TZ ON T.TIME_ZONE_ID = T_TZ.TIME_ZONE_ID
		WHERE D.DEVICE_ACTIVE_YN_FLAG = 'Y'
			AND TZ.TIME_ZONE_CD != T_TZ.ABBREV;
BEGIN
    FOR L_REC IN L_CUR LOOP
        ENGINE.PKG_DATA_SYNC.SP_CREATE_DATA_SYNC(
			'DEVICE_INFO',
			'REPORT.TERMINAL',
			'U',
			L_REC.DEVICE_ID,
			L_REC.DEVICE_NAME
		);
        COMMIT;
    END LOOP;  
END;
/
-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/releases/REL_Edge_Update_R38/ReprocessSelectedSalesRollupData.sql?rev=HEAD
CREATE TABLE REPORT.TSBD_REPROCESS TABLESPACE REPORT_DATA03
AS
SELECT TERMINAL_ID, TRANS_TYPE_ID, CURRENCY_ID, EPORT_ID, TRUNC(TRAN_DATE) TRAN_DATE, 'N' PROCESSED_FLAG
  FROM REPORT.ACTIVITY_REF 
 WHERE 1 = 0;
COMMIT;

DECLARE
    ln_start_tran_id REPORT.ACTIVITY_REF.TRAN_ID%TYPE;
    ln_end_tran_id REPORT.ACTIVITY_REF.TRAN_ID%TYPE;
    ln_interval PLS_INTEGER := 1000000;
BEGIN
    SELECT MIN(TRAN_ID), MAX(TRAN_ID) + 1
      INTO ln_start_tran_id, ln_end_tran_id
      FROM REPORT.ACTIVITY_REF;
    WHILE ln_start_tran_id < ln_end_tran_id LOOP
        INSERT INTO REPORT.TSBD_REPROCESS( TERMINAL_ID, TRANS_TYPE_ID, CURRENCY_ID, EPORT_ID, TRAN_DATE, PROCESSED_FLAG)
            SELECT TERMINAL_ID, TRANS_TYPE_ID, CURRENCY_ID, EPORT_ID, TRUNC(TRAN_DATE) TRAN_DATE, 'N' PROCESSED_FLAG
              FROM REPORT.ACTIVITY_REF 
             WHERE NVL(QUANTITY, 0) = 0
               AND TRANS_TYPE_ID IS NOT NULL
               AND TOTAL_AMOUNT != 0 
               AND TERMINAL_ID != 0
               AND TRAN_ID >= ln_start_tran_id
               AND TRAN_ID < ln_start_tran_id + ln_interval
             GROUP BY TERMINAL_ID, TRANS_TYPE_ID, CURRENCY_ID, EPORT_ID, TRUNC(TRAN_DATE);
        COMMIT;
        ln_start_tran_id := ln_start_tran_id + ln_interval;
    END LOOP;
END;
/

CREATE INDEX REPORT.AK_TSBD_REPROCESS ON REPORT.TSBD_REPROCESS(TERMINAL_ID, TRAN_DATE, TRANS_TYPE_ID) tablespace REPORT_INDX03;

DECLARE
    CURSOR l_cur IS SELECT DISTINCT TERMINAL_ID, TRANS_TYPE_ID, CURRENCY_ID, EPORT_ID, TRAN_DATE FROM REPORT.TSBD_REPROCESS WHERE PROCESSED_FLAG = 'N';
BEGIN
    FOR l_rec IN l_cur LOOP
        INSERT INTO REPORT.TRANS_STAT_BY_DAY(TERMINAL_ID, EPORT_ID, TRAN_DATE, TRANS_TYPE_ID, CURRENCY_ID)
            SELECT l_rec.TERMINAL_ID, l_rec.EPORT_ID, l_rec.TRAN_DATE, l_rec.TRANS_TYPE_ID, l_rec.CURRENCY_ID
              FROM DUAL
             WHERE NOT EXISTS(SELECT 1 FROM REPORT.TRANS_STAT_BY_DAY T 
                    WHERE T.TERMINAL_ID = l_rec.TERMINAL_ID 
                      AND T.EPORT_ID = l_rec.EPORT_ID 
                      AND T.TRAN_DATE = l_rec.TRAN_DATE 
                      AND T.TRANS_TYPE_ID = l_rec.TRANS_TYPE_ID
                      AND NVL(T.CURRENCY_ID, 0) = NVL(l_rec.CURRENCY_ID, 0));
        UPDATE REPORT.TRANS_STAT_BY_DAY
           SET (TRAN_COUNT, VEND_COUNT, TRAN_AMOUNT, CONVENIENCE_FEE, LOYALTY_DISCOUNT) =
               (SELECT COUNT(DISTINCT TRAN_ID), NVL(SUM(QUANTITY), 0), NVL(SUM(TOTAL_AMOUNT), 0), NVL(SUM(CONVENIENCE_FEE), 0), NVL(SUM(LOYALTY_DISCOUNT), 0) 
                  FROM REPORT.ACTIVITY_REF
                 WHERE TERMINAL_ID = l_rec.TERMINAL_ID
                   AND EPORT_ID = l_rec.EPORT_ID
                   AND TRAN_DATE >= l_rec.TRAN_DATE AND TRAN_DATE < l_rec.TRAN_DATE + 1
                   AND TRANS_TYPE_ID = l_rec.TRANS_TYPE_ID
                   AND NVL(CURRENCY_ID, 0) = NVL(l_rec.CURRENCY_ID, 0))
         WHERE TERMINAL_ID = l_rec.TERMINAL_ID
           AND EPORT_ID = l_rec.EPORT_ID
           AND TRAN_DATE = l_rec.TRAN_DATE
           AND TRANS_TYPE_ID = l_rec.TRANS_TYPE_ID
           AND NVL(CURRENCY_ID, 0) = NVL(l_rec.CURRENCY_ID, 0);
        UPDATE REPORT.TSBD_REPROCESS 
           SET PROCESSED_FLAG = 'Y' 
         WHERE TERMINAL_ID = l_rec.TERMINAL_ID
           AND TRANS_TYPE_ID = l_rec.TRANS_TYPE_ID
           AND NVL(CURRENCY_ID, 0) = NVL(l_rec.CURRENCY_ID, 0)
           AND EPORT_ID = l_rec.EPORT_ID
           AND TRAN_DATE = l_rec.TRAN_DATE;
        COMMIT;
    END LOOP;
END;
/
/*
desc REPORT.TRANS_STAT_BY_DAY;

--32588	1000009863027
SELECT MIN(TRAN_ID), MAX(TRAN_ID) + 1
      --INTO ln_start_tran_id, ln_end_tran_id
      FROM REPORT.ACTIVITY_REF;
      -- SELECT 1000659365453 - 1000313202741 FROM DUAL; = 346,162,712
      
UPDATE REPORT.TSBD_REPROCESS SET PROCESSED_FLAG  = 'N';

delete from  REPORT.TRANS_STAT_BY_DAY
where trans_stat_by_day_id in(
SELECT t.trans_stat_by_day_id FROM REPORT.TRANS_STAT_BY_DAY t
JOIN REPORT.TSBD_REPROCESS R ON T.TERMINAL_ID = r.TERMINAL_ID 
                      AND T.EPORT_ID = r.EPORT_ID 
                      AND T.TRAN_DATE = r.TRAN_DATE 
                      AND T.TRANS_TYPE_ID = r.TRANS_TYPE_ID
                      AND NVL(T.CURRENCY_ID, 0) = NVL(r.CURRENCY_ID, 0)
left outer join report.activity_ref ar on T.TERMINAL_ID = ar.TERMINAL_ID 
                      AND T.EPORT_ID = ar.EPORT_ID 
                      AND T.TRAN_DATE = TRUNC(ar.TRAN_DATE) 
                      AND T.TRANS_TYPE_ID = ar.TRANS_TYPE_ID
                      AND NVL(T.CURRENCY_ID, 0) = NVL(ar.CURRENCY_ID, 0)                      
 where ar.tran_id is null);
 
select * from report.activity_ref ar
join REPORT.TRANS_STAT_BY_DAY t on T.TERMINAL_ID = ar.TERMINAL_ID 
                     AND T.EPORT_ID = ar.EPORT_ID 
                      AND T.TRAN_DATE = TRUNC(ar.TRAN_DATE)  
                      AND T.TRANS_TYPE_ID = ar.TRANS_TYPE_ID
                      AND NVL(T.CURRENCY_ID, 0) = NVL(ar.CURRENCY_ID, 0)
 where t.trans_stat_by_day_id = 5392602758;
 
SELECT * FROM REPORT.TRANS_STAT_BY_DAY t
JOIN REPORT.TSBD_REPROCESS R ON T.TERMINAL_ID = r.TERMINAL_ID 
                      AND T.EPORT_ID = r.EPORT_ID 
                      AND T.TRAN_DATE = r.TRAN_DATE 
                      AND T.TRANS_TYPE_ID = r.TRANS_TYPE_ID
                      AND NVL(T.CURRENCY_ID, 0) = NVL(r.CURRENCY_ID, 0);
*/
DROP TABLE REPORT.TSBD_REPROCESS;

