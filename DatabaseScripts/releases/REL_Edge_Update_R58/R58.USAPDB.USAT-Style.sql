UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_LABEL = 'New Bank Account'
WHERE WEB_LINK_LABEL = 'New Bank Acct';

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_GROUP = 'Configuration'
WHERE WEB_LINK_GROUP = 'Setup';

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_GROUP = 'Reports'
WHERE WEB_LINK_LABEL = 'MORE Reports';

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_GROUP = 'Administration'
WHERE WEB_LINK_LABEL IN ('Consumers', 'Mass Card Update');

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_LABEL = 'Bank Accounts Ids'
WHERE WEB_LINK_LABEL = 'Bank Acct Ids';

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_GROUP = 'Administration'
WHERE WEB_LINK_GROUP = 'RMA';

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_LABEL = 'Customer Service Reports'
WHERE WEB_LINK_LABEL = 'Reports' AND WEB_LINK_GROUP = 'Customer Service';

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_GROUP = 'Reports'
WHERE WEB_LINK_LABEL = 'Customer Service Reports';

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_GROUP = 'Administration'
WHERE WEB_LINK_GROUP = 'Customer Service';

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_GROUP = 'Configuration'
WHERE WEB_LINK_LABEL = 'Preferences and Setup';

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_GROUP = 'Configuration'
WHERE WEB_LINK_LABEL = 'Admin Welcome Info';

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_GROUP = 'Configuration'
WHERE WEB_LINK_LABEL = 'Report Register';

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_GROUP = 'Configuration'
WHERE WEB_LINK_LABEL = 'Manage Allowed Parts';

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_GROUP = 'Configuration'
WHERE WEB_LINK_LABEL = 'Mass Device Configuration';

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_GROUP = 'Configuration'
WHERE WEB_LINK_LABEL = 'Mass Device Update';

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_GROUP = 'Configuration'
WHERE WEB_LINK_LABEL = 'Devices';

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_GROUP = 'Configuration'
WHERE WEB_LINK_LABEL = 'New Customer';

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_GROUP = 'Configuration'
WHERE WEB_LINK_LABEL = 'Column Mappings';

UPDATE WEB_CONTENT.WEB_LINK 
SET WEB_LINK_ORDER = 3000 + WEB_LINK_ORDER
WHERE WEB_LINK_GROUP = 'Reports';

UPDATE WEB_CONTENT.WEB_LINK 
SET WEB_LINK_ORDER = 2000 + WEB_LINK_ORDER
WHERE WEB_LINK_GROUP = 'Configuration';

UPDATE WEB_CONTENT.WEB_LINK 
SET WEB_LINK_ORDER = 1000 + WEB_LINK_ORDER
WHERE WEB_LINK_GROUP = 'Administration'; 

COMMIT;

UPDATE WEB_CONTENT.WEB_LINK 
SET WEB_LINK_USAGE = 'D'
WHERE UPPER(WEB_LINK_LABEL) IN ('LOGIN', 'CONTACT US');

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_GROUP = 'Configuration'
WHERE UPPER(WEB_LINK_LABEL) = 'MASS CARD UPDATE';

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_ORDER = (SELECT MAX(WEB_LINK_ORDER) FROM WEB_CONTENT.WEB_LINK WHERE UPPER(WEB_LINK_LABEL) = 'MASS DEVICE UPDATE') + 1
WHERE UPPER(WEB_LINK_LABEL) = 'MASS CARD UPDATE';

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_LABEL = 'Bank Account Ids', WEB_LINK_GROUP = 'General'
WHERE WEB_LINK_LABEL = 'Bank Accounts Ids';

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_GROUP = 'General'
WHERE WEB_LINK_LABEL = 'Data Exchange';

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_ORDER = (SELECT MAX(WEB_LINK_ORDER) FROM WEB_CONTENT.WEB_LINK WHERE WEB_LINK_GROUP = 'General') + 10
WHERE UPPER(WEB_LINK_LABEL) = 'CHANGE PASSWORD';

COMMIT;

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_GROUP = 'Administration'
WHERE WEB_LINK_LABEL = 'Manage Campaign' AND WEB_LINK_GROUP = 'Configuration';

COMMIT;

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_LABEL = 'Welcome Info'
WHERE WEB_LINK_LABEL = 'Admin Welcome Info';

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_GROUP = 'Accounting', WEB_LINK_USAGE = 'W', WEB_LINK_ORDER = 0
WHERE WEB_LINK_LABEL = 'Customer Service Reports';

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_LABEL = 'Home'
WHERE WEB_LINK_LABEL = 'My Homepage';

UPDATE WEB_CONTENT.WEB_LINK 
SET WEB_LINK_USAGE = 'D'
WHERE WEB_LINK_LABEL = 'Set Batch Close Time';

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_GROUP = 'Reports'
WHERE WEB_LINK_LABEL = 'Report Register';

COMMIT;

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_GROUP = 'Sales Analysis'
WHERE WEB_LINK_GROUP IN ('Month To Date', 'Year To Date', 'Transaction Summaries');

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_GROUP = 'MORE Platform'
WHERE WEB_LINK_GROUP IN ('Payroll Deduct');

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_GROUP = 'MORE Platform', WEB_LINK_USAGE = 'W'
WHERE WEB_LINK_USAGE IN ('P');

UPDATE WEB_CONTENT.WEB_LINK 
SET WEB_LINK_USAGE = 'D'
WHERE WEB_LINK_LABEL = 'MORE Reports';

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_GROUP = 'Accounting', WEB_LINK_USAGE = 'W', WEB_LINK_ORDER = 0
WHERE WEB_LINK_LABEL = 'Bank Account Ids';

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_LABEL = 'Campaigns'
WHERE WEB_LINK_LABEL = 'Manage Campaign';

COMMIT;

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_LABEL = 'Refunds'
WHERE WEB_LINK_LABEL = 'Issue Refunds';

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_LABEL = 'Chargebacks'
WHERE WEB_LINK_LABEL = 'Issue Chargebacks';

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_LABEL = 'Column Maps'
WHERE WEB_LINK_LABEL = 'Column Mappings';

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_LABEL = 'Setup'
WHERE WEB_LINK_LABEL = 'Preferences and Setup';

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_LABEL = 'Parts'
WHERE WEB_LINK_LABEL = 'Manage Allowed Parts';

UPDATE WEB_CONTENT.WEB_LINK 
SET WEB_LINK_USAGE = 'D'
WHERE WEB_LINK_LABEL = 'Activate Device';

UPDATE REPORT.PRIV
SET DESCRIPTION = 'Mass Device Update'
WHERE DESCRIPTION = 'Device Activation';

UPDATE REPORT.PRIV
SET DESCRIPTION = 'Manage Campaigns'
WHERE DESCRIPTION = 'Manage Campaign';

COMMIT;

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_ORDER = CASE WEB_LINK_LABEL 
WHEN 'Logout' THEN 10 WHEN 'Login To Original' THEN 20 WHEN 'Home' THEN 30 WHEN 'Change Password' THEN 1000 ELSE 100 END
WHERE WEB_LINK_USAGE = 'M';

COMMIT;

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_GROUP = 'General'
WHERE WEB_LINK_LABEL IN ('Customers', 'Devices', 'Users') AND WEB_LINK_USAGE = 'M';

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_GROUP = 'Administration'
WHERE WEB_LINK_LABEL IN ('Complete Order', 'ePort Online', 'Data Exchange', 'New Customer', 'Parts', 'Welcome Info') AND WEB_LINK_USAGE = 'M';

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_GROUP = 'Configuration'
WHERE WEB_LINK_LABEL IN ('Bank Accounts', 'Campaigns', 'Consumers') AND WEB_LINK_USAGE = 'M';

INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT(WEB_LINK_ID, REQUIREMENT_ID)
SELECT 268, 4 FROM DUAL 
WHERE NOT EXISTS (SELECT 1 FROM WEB_CONTENT.WEB_LINK_REQUIREMENT WHERE WEB_LINK_ID = 268 AND REQUIREMENT_ID = 4);

UPDATE WEB_CONTENT.WEB_LINK 
SET WEB_LINK_USAGE = 'D'
WHERE WEB_LINK_LABEL IN ('Alert Register', 'Alerts', 'Machines', 'Main Page', 'Prints Remaining', 'Remote File Update') AND WEB_LINK_USAGE = 'M';

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_LABEL = 'DEX Status'
WHERE WEB_LINK_LABEL = 'Dex Status' AND WEB_LINK_USAGE = 'M';

COMMIT;

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_LABEL = 'Preferences'
WHERE WEB_LINK_LABEL = 'Setup' AND WEB_LINK_USAGE = 'M';

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_ORDER = 1000 
WHERE LOWER(WEB_LINK_URL) LIKE '%/rma_%' AND WEB_LINK_USAGE = 'M';

UPDATE WEB_CONTENT.WEB_LINK_REQUIREMENT
SET REQUIREMENT_ID = 20
WHERE WEB_LINK_ID = (SELECT WEB_LINK_ID FROM WEB_CONTENT.WEB_LINK WHERE WEB_LINK_LABEL = 'Welcome Info') 
AND REQUIREMENT_ID = 14;

UPDATE WEB_CONTENT.LITERAL
SET LITERAL_VALUE = 'If you have a valid account in the system, you will receive an email with a link to reset your password.'
WHERE LITERAL_KEY = 'password-reset-invalid-username';

UPDATE WEB_CONTENT.LITERAL
SET LITERAL_VALUE = 'Incorrect username or password.'
WHERE LITERAL_KEY IN ('login-account-locked', 'login-invalid-credentials', 'login-invalid-password', 'login-invalid-username');

UPDATE WEB_CONTENT.LITERAL
SET LITERAL_VALUE = 'Error occurred. Please contact Customer Service for assistance.'
WHERE LITERAL_KEY IN ('password-change-invalid-passcode', 'password-reset-failed-to-send');

INSERT INTO WEB_CONTENT.REQUIREMENT(REQUIREMENT_ID, REQUIREMENT_NAME, REQUIREMENT_CLASS, REQUIREMENT_PARAM_CLASS, REQUIREMENT_PARAM_VALUE)
VALUES(50, 'REQ_REGION_ADMIN', 'com.usatech.usalive.link.HasPrivilegeRequirement', 'java.lang.String', 'PRIV_REGION_ADMIN');

INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT(WEB_LINK_ID, REQUIREMENT_ID)
SELECT 165, 50 FROM DUAL WHERE NOT EXISTS (
SELECT 1 FROM WEB_CONTENT.WEB_LINK_REQUIREMENT WHERE WEB_LINK_ID = 165 AND REQUIREMENT_ID = 50);

COMMIT;

UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'Assign and Group Devices into Regions' WHERE WEB_LINK_LABEL = 'Regions';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'Build and run custom reports' WHERE WEB_LINK_LABEL = 'Build a Report';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'Campaigns' WHERE WEB_LINK_LABEL = 'Campaigns';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'Change preferences for your profile' WHERE WEB_LINK_LABEL = 'Preferences';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'Change your password' WHERE WEB_LINK_LABEL = 'Change Password';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'Charge your customers for your products and services' WHERE WEB_LINK_LABEL = 'ePort Online';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'Column Maps' WHERE WEB_LINK_LABEL = 'Column Maps';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'Complete your order' WHERE WEB_LINK_LABEL = 'Complete Order';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'Consumer Card Mass Update' WHERE WEB_LINK_LABEL = 'Mass Card Update';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'Consumers' WHERE WEB_LINK_LABEL = 'Consumers';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'Create ATT 2G Upgrade RMA' WHERE WEB_LINK_LABEL = 'ATT 2G Upgrade';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'Create Device Return And Replacement RMA' WHERE WEB_LINK_LABEL = 'Create RMA';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'Create Parts RMA' WHERE WEB_LINK_LABEL = 'Create Parts RMA';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'Create Rental For Credit RMA' WHERE WEB_LINK_LABEL = 'Rental For Credit';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'Create a New Customer' WHERE WEB_LINK_LABEL = 'New Customer';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'Customers' WHERE WEB_LINK_LABEL = 'Customers';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'DEX Status Report' WHERE WEB_LINK_LABEL = 'DEX Status';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'Dashboard' WHERE WEB_LINK_LABEL = 'Dashboard';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'Design or Edit Folios' WHERE WEB_LINK_LABEL = 'Folio Designer';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'Devices' WHERE WEB_LINK_LABEL = 'Devices';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'Edit information message for Home page' WHERE WEB_LINK_LABEL = 'Welcome Info';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'Establish a Bank Account to Receive Payments from USA Technologies' WHERE WEB_LINK_LABEL = 'New Bank Account';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'Home' WHERE WEB_LINK_LABEL = 'Home';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'Log out of current user and back in as original user' WHERE WEB_LINK_LABEL = 'Login To Original';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'Logout' WHERE WEB_LINK_LABEL = 'Logout';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'Manage Allowed Return Parts' WHERE WEB_LINK_LABEL = 'Parts';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'Pre-Defined Reports' WHERE WEB_LINK_LABEL = 'Saved Reports';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'Print Your License Agreement' WHERE WEB_LINK_LABEL = 'License Agreement';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'Print an EFT Authorization Form' WHERE WEB_LINK_LABEL = 'EFT Auth Form';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'Recent Report Request History' WHERE WEB_LINK_LABEL = 'Recent Reports';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'Register to receive auto-generated reports' WHERE WEB_LINK_LABEL = 'Report Register';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'Sales Summary' WHERE WEB_LINK_LABEL = 'Sales Summary';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'Search for Transactions and Issue Chargebacks' WHERE WEB_LINK_LABEL = 'Chargebacks';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'Search for Transactions and Issue Refunds' WHERE WEB_LINK_LABEL = 'Refunds';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'Shows history of Data Exchange file processing' WHERE WEB_LINK_LABEL = 'Data Exchange';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'Update Your Profile' WHERE WEB_LINK_LABEL = 'Users';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'Update configuration settings of multiple devices' WHERE WEB_LINK_LABEL = 'Mass Device Configuration';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'Update multiple devices using a spreadsheet' WHERE WEB_LINK_LABEL = 'Mass Device Update';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'Users' WHERE WEB_LINK_LABEL = 'Users';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'View Customer Bank Accounts' WHERE WEB_LINK_LABEL = 'Bank Accounts';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'View Details of Payments' WHERE WEB_LINK_LABEL = 'Payment Detail';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'View Device Diagnostic Alerts' WHERE WEB_LINK_LABEL = 'Diagnostics';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'View RMA' WHERE WEB_LINK_LABEL = 'View RMA';
UPDATE WEB_CONTENT.WEB_LINK SET WEB_LINK_DESC = 'View, approve or cancel pending refunds' WHERE WEB_LINK_LABEL = 'Admin Refunds';

COMMIT;

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_LABEL = 'Payments'
WHERE WEB_LINK_USAGE = 'M' AND WEB_LINK_LABEL = 'Payment Detail';

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_URL = REPLACE(WEB_LINK_URL, 'run_report.i', 'run_report_async.i')
WHERE WEB_LINK_URL LIKE '%run_report.i%';

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_ORDER = 0
WHERE WEB_LINK_USAGE = 'W' AND WEB_LINK_ORDER != 0;

UPDATE FOLIO_CONF.FILTER_OPERATOR 
SET OPERATOR_PATTERN = REPLACE(OPERATOR_PATTERN, 'REGEXP_REPLACE(UPPER(?)', 'REGEXP_REPLACE(UPPER(TRIM(?))')
WHERE OPERATOR_PATTERN LIKE '%REGEXP_REPLACE(UPPER(?)%';

INSERT INTO WEB_CONTENT.REQUIREMENT(REQUIREMENT_ID, REQUIREMENT_NAME, REQUIREMENT_CLASS)
VALUES(51, 'REQ_MASS_UPDATES', 'com.usatech.usalive.link.MassUpdatesRequirement');

INSERT INTO WEB_CONTENT.REQUIREMENT(REQUIREMENT_ID, REQUIREMENT_NAME, REQUIREMENT_CLASS)
VALUES(52, 'REQ_REFUND_SECTION', 'com.usatech.usalive.link.RefundSectionRequirement');

INSERT INTO WEB_CONTENT.REQUIREMENT(REQUIREMENT_ID, REQUIREMENT_NAME, REQUIREMENT_CLASS)
VALUES(53, 'REQ_SYSTEM_SECTION', 'com.usatech.usalive.link.SystemSectionRequirement');

COMMIT;
