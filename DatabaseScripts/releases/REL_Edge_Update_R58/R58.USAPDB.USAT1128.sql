UPDATE G4OP.DEX_CODE_ALERT
  SET MESSAGE = 'Product #{1} is sold out.'
  WHERE DEX_CODE = 'PA5';

UPDATE REPORT.ALERT
  SET ALERT_NAME = 'Product Sold Out', DESCRIPTION = 'A product is sold out', STATUS ='A'
  WHERE ALERT_ID = 10;

UPDATE REPORT.ALERT
  SET STATUS ='I'
  WHERE ALERT_ID = 1;

COMMIT;
