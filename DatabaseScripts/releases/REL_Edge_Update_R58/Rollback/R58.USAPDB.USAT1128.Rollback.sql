UPDATE G4OP.DEX_CODE_ALERT
  SET MESSAGE = 'Product #{~1-{1} for {2,cents}~} was selected {3} times.'
  WHERE DEX_CODE = 'PA5';

UPDATE REPORT.ALERT
  SET ALERT_NAME = 'Product Selected', DESCRIPTION = 'A product was vended', STATUS ='I'
  WHERE ALERT_ID = 10;

UPDATE REPORT.ALERT
  SET STATUS ='A'
  WHERE ALERT_ID = 1;

COMMIT;
