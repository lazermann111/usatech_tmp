CREATE OR REPLACE TRIGGER DEVICE.TRBU_DEVICE_FILE_TRANSFER
BEFORE UPDATE
ON DEVICE.DEVICE_FILE_TRANSFER
FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
     INTO :NEW.created_by,
          :NEW.created_ts,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;

	IF :NEW.device_file_transfer_status_cd = 1 AND :OLD.device_file_transfer_status_cd = 0 THEN
		IF :NEW.device_file_transfer_ts IS NULL THEN
			:NEW.device_file_transfer_ts := SYSDATE;
		END IF;

		SELECT DECODE(PARTITION_TS, EPOCH_DATE, :NEW.DEVICE_FILE_TRANSFER_TS, PARTITION_TS)
		INTO :NEW.PARTITION_TS
		FROM DEVICE.FILE_TRANSFER
		WHERE FILE_TRANSFER_ID = :NEW.FILE_TRANSFER_ID;
	END IF;
END;
/

CREATE OR REPLACE TRIGGER DEVICE.TRBI_DEVICE_FILE_TRANSFER
BEFORE INSERT
ON DEVICE.DEVICE_FILE_TRANSFER
FOR EACH ROW
BEGIN
   IF :NEW.device_file_transfer_id IS NULL THEN
      SELECT seq_device_file_transfer_id.NEXTVAL
        INTO :NEW.device_file_transfer_id
        FROM DUAL;
   END IF;
   
   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
	 
	IF :NEW.device_file_transfer_status_cd = 1 AND :NEW.device_file_transfer_ts IS NULL THEN
		:NEW.device_file_transfer_ts := SYSDATE;
    END IF;
	 
	IF :NEW.device_file_transfer_status_cd = 0 AND :NEW.device_file_transfer_direct = 'I' OR :NEW.device_file_transfer_status_cd = 1 THEN
		SELECT DECODE(PARTITION_TS, EPOCH_DATE, NVL(:NEW.DEVICE_FILE_TRANSFER_TS, :NEW.CREATED_TS), PARTITION_TS)
		INTO :NEW.PARTITION_TS
		FROM DEVICE.FILE_TRANSFER
		WHERE FILE_TRANSFER_ID = :NEW.FILE_TRANSFER_ID;        
	ELSE
		:NEW.PARTITION_TS := EPOCH_DATE;
	END IF;
END; 
/

ALTER TABLE DEVICE.FILE_TRANSFER_TYPE DROP COLUMN DELETABLE_AFTER_TRANSFER_IND;

COMMIT;
