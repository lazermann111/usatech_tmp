﻿START TRANSACTION;

DROP VIEW IF EXISTS main.app_request;

ALTER TABLE main._app_request ALTER folio_id TYPE bigint, ALTER report_id TYPE bigint, ALTER profile_id TYPE bigint;

CREATE OR REPLACE VIEW main.app_request AS 
 SELECT *
   FROM main._app_request;

ALTER TABLE main.app_request OWNER TO admin_1;
ALTER TABLE main.app_request ALTER COLUMN app_request_id SET DEFAULT nextval('main._app_request_app_request_id_seq'::regclass);
ALTER TABLE main.app_request ALTER COLUMN request_ts SET DEFAULT now();

GRANT ALL ON TABLE main.app_request TO admin_1;
GRANT SELECT ON TABLE main.app_request TO read_main;
GRANT UPDATE, INSERT, DELETE ON TABLE main.app_request TO write_main;

DROP TRIGGER IF EXISTS TRIIUD_APP_REQUEST ON MAIN.APP_REQUEST;
CREATE TRIGGER triiud_app_request
  INSTEAD OF INSERT OR UPDATE OR DELETE
  ON main.app_request
  FOR EACH ROW
  EXECUTE PROCEDURE main.friiud_app_request();
  
COMMIT;