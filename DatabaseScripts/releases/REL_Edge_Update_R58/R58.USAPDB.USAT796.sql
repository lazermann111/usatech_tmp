DECLARE
	min_tran_id NUMBER;
	max_tran_id NUMBER;
	tr_counter NUMBER;
BEGIN
	SELECT MIN(TRAN_ID) INTO min_tran_id FROM REPORT.TRANS; 
	SELECT MAX(TRAN_ID) INTO max_tran_id FROM REPORT.TRANS;
    tr_counter := max_tran_id;
    WHILE tr_counter >= min_tran_id
    LOOP
    	UPDATE REPORT.ACTIVITY_REF AR
        SET AR.PAYMENT_BATCH_ID = (SELECT MAX(DISTINCT L.BATCH_ID) FROM CORP.LEDGER L WHERE L.TRANS_ID = AR.TRAN_ID AND L.ENTRY_TYPE = 'CC')
        WHERE AR.TRAN_ID > tr_counter - 10000 AND AR.TRAN_ID <= tr_counter AND AR.PAYMENT_BATCH_ID IS NULL;
    	COMMIT;
    	tr_counter := tr_counter - 10000;
    END LOOP;
END;
/
