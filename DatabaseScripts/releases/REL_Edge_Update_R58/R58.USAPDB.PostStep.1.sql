update web_content.literal set literal_value = 
q'[We at USA Technologies, Inc. are continually striving to enhance our customers' power to run their business effectively. The USALive website is loaded with features to help you do just that.<br/><br/>
For the best website experience, please use the latest major version of your browser. Now new in the latest USALive and ePort Connect version:<br/><br/>
<ul>
<li>USALive website look and feel makeover</li>
<li>Additional integration with Seed platform</li>
<li>Apple Pay, Google Pay and card brand loyalty campaigns</li>
<li>Product specific loyalty campaign enhancements</li>
<li>Help videos in General > Help</li>
<li>Install documents in Reports > Saved Reports > Install Information
<li>Column Map instructions in Configuration > Column Maps
<li>Additional reporting capabilities</li>
<li>Various performance and user experience enhancements</li>
</ul>]'
where literal_key = 'home-page-message' and subdomain_id is null;
commit;

DECLARE
	ln_web_link_id NUMBER;
	ln_web_link_count NUMBER;
BEGIN
	SELECT COUNT(1) INTO ln_web_link_count FROM WEB_CONTENT.WEB_LINK WHERE WEB_LINK_LABEL = 'RMA' AND WEB_LINK_USAGE = 'M';
	IF ln_web_link_count = 0 THEN
		SELECT MAX(WEB_LINK_ID) + 1 INTO ln_web_link_id FROM WEB_CONTENT.WEB_LINK;
		INSERT INTO WEB_CONTENT.WEB_LINK(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER)
		VALUES(ln_web_link_id, 'RMA', 'sub_menu.html?usage=R&' || 'caption=RMA', 'RMAs', 'Administration', 'M', 100);
		
		INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT(WEB_LINK_ID, REQUIREMENT_ID) VALUES(ln_web_link_id, 1);
		INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT(WEB_LINK_ID, REQUIREMENT_ID) VALUES(ln_web_link_id, 14);
		
		SELECT MAX(WEB_LINK_ID) + 1 INTO ln_web_link_id FROM WEB_CONTENT.WEB_LINK;
		INSERT INTO WEB_CONTENT.WEB_LINK(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER) 
		VALUES(ln_web_link_id, 'RMA', 'sub_menu.html?usage=R&' || 'caption=RMA', 'RMAs', 'Administration', 'M', 100);
		
		INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT(WEB_LINK_ID, REQUIREMENT_ID) VALUES(ln_web_link_id, 1);
		INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT(WEB_LINK_ID, REQUIREMENT_ID) VALUES(ln_web_link_id, 4);
		INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT(WEB_LINK_ID, REQUIREMENT_ID) VALUES(ln_web_link_id, 39);
		INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT(WEB_LINK_ID, REQUIREMENT_ID) VALUES(ln_web_link_id, 46);
		
		COMMIT;
	END IF;
END;
/

INSERT INTO WEB_CONTENT.WEB_LINK_USAGE(WEB_LINK_USAGE, DESCRIPTION) VALUES('R', 'RMA');

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_ORDER = 100, WEB_LINK_USAGE = 'R'
WHERE WEB_LINK_USAGE = 'M' AND WEB_LINK_LABEL IN ('ATT 2G Upgrade', 'Create Parts RMA', 'Create RMA', 'Parts', 'Rental For Credit', 'View RMA');

COMMIT;

INSERT INTO WEB_CONTENT.WEB_LINK_USAGE(WEB_LINK_USAGE, DESCRIPTION) VALUES('S', 'Mass Updates');
INSERT INTO WEB_CONTENT.WEB_LINK_USAGE(WEB_LINK_USAGE, DESCRIPTION) VALUES('U', 'User defined report');
INSERT INTO WEB_CONTENT.WEB_LINK_USAGE(WEB_LINK_USAGE, DESCRIPTION) VALUES('u', 'N/A');
UPDATE WEB_CONTENT.WEB_LINK_USAGE SET DESCRIPTION = 'Menu' WHERE WEB_LINK_USAGE = 'M';

DECLARE
	ln_web_link_id NUMBER;
	ln_web_link_count NUMBER;
BEGIN
	SELECT COUNT(1) INTO ln_web_link_count FROM WEB_CONTENT.WEB_LINK WHERE WEB_LINK_LABEL = 'Mass Updates' AND WEB_LINK_USAGE = 'M';
	IF ln_web_link_count = 0 THEN
		SELECT MAX(WEB_LINK_ID) + 1 INTO ln_web_link_id FROM WEB_CONTENT.WEB_LINK;
		INSERT INTO WEB_CONTENT.WEB_LINK(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER)
		VALUES(ln_web_link_id, 'Mass Updates', 'sub_menu.html?usage=S&' || 'caption=Mass+Updates', 'Mass Updates', 'Configuration', 'M', 100);
		
		INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT(WEB_LINK_ID, REQUIREMENT_ID) VALUES(ln_web_link_id, 1);
		INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT(WEB_LINK_ID, REQUIREMENT_ID) VALUES(ln_web_link_id, 51);
		
		COMMIT;
	END IF;
END;
/

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_ORDER = 100, WEB_LINK_USAGE = 'S'
WHERE WEB_LINK_USAGE = 'M' AND WEB_LINK_LABEL IN ('Mass Card Update', 'Mass Device Configuration', 'Mass Device Update');

COMMIT;

INSERT INTO WEB_CONTENT.WEB_LINK_USAGE(WEB_LINK_USAGE, DESCRIPTION) VALUES('F', 'Refund Section');

DECLARE
	ln_web_link_id NUMBER;
	ln_web_link_count NUMBER;
BEGIN
	SELECT COUNT(1) INTO ln_web_link_count FROM WEB_CONTENT.WEB_LINK WHERE WEB_LINK_LABEL = 'Refunds' AND WEB_LINK_URL LIKE 'sub_menu.html%';
	IF ln_web_link_count = 0 THEN
		SELECT MAX(WEB_LINK_ID) + 1 INTO ln_web_link_id FROM WEB_CONTENT.WEB_LINK;
		INSERT INTO WEB_CONTENT.WEB_LINK(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER)
		VALUES(ln_web_link_id, 'Refunds', 'sub_menu.html?usage=F&' || 'caption=Refunds', 'Refunds', 'Administration', 'M', 100);
		
		INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT(WEB_LINK_ID, REQUIREMENT_ID) VALUES(ln_web_link_id, 1);
		INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT(WEB_LINK_ID, REQUIREMENT_ID) VALUES(ln_web_link_id, 52);
		
		COMMIT;
	END IF;
END;
/

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_ORDER = 100, WEB_LINK_USAGE = 'F'
WHERE WEB_LINK_USAGE = 'M' AND WEB_LINK_LABEL IN ('Refunds', 'Admin Refunds', 'Chargebacks') AND WEB_LINK_URL NOT LIKE 'sub_menu.html%';

COMMIT;

INSERT INTO WEB_CONTENT.WEB_LINK_USAGE(WEB_LINK_USAGE, DESCRIPTION) VALUES('A', 'Bank Account Section');

DECLARE
	ln_web_link_id NUMBER;
	ln_web_link_count NUMBER;
BEGIN
	SELECT COUNT(1) INTO ln_web_link_count FROM WEB_CONTENT.WEB_LINK WHERE WEB_LINK_LABEL = 'Bank Accounts' AND WEB_LINK_URL LIKE 'sub_menu.html%';
	IF ln_web_link_count = 0 THEN
		SELECT MAX(WEB_LINK_ID) + 1 INTO ln_web_link_id FROM WEB_CONTENT.WEB_LINK;
		INSERT INTO WEB_CONTENT.WEB_LINK(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER)
		VALUES(ln_web_link_id, 'Bank Accounts', 'sub_menu.html?usage=A&' || 'caption=Bank+Accounts', 'Bank Accounts', 'Configuration', 'M', 100);
		
		INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT(WEB_LINK_ID, REQUIREMENT_ID) VALUES(ln_web_link_id, 1);
		INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT(WEB_LINK_ID, REQUIREMENT_ID) VALUES(ln_web_link_id, 4);
		INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT(WEB_LINK_ID, REQUIREMENT_ID) VALUES(ln_web_link_id, 9);
		
		COMMIT;
	END IF;
END;
/

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_ORDER = 100, WEB_LINK_USAGE = 'A'
WHERE WEB_LINK_USAGE = 'M' AND WEB_LINK_LABEL IN ('Bank Accounts', 'EFT Auth Form', 'New Bank Account') AND WEB_LINK_URL NOT LIKE 'sub_menu.html%';

COMMIT;

INSERT INTO WEB_CONTENT.WEB_LINK_USAGE(WEB_LINK_USAGE, DESCRIPTION) VALUES('Y', 'System Section');

DECLARE
	ln_web_link_id NUMBER;
	ln_web_link_count NUMBER;
BEGIN
	SELECT COUNT(1) INTO ln_web_link_count FROM WEB_CONTENT.WEB_LINK WHERE WEB_LINK_LABEL = 'System' AND WEB_LINK_URL LIKE 'sub_menu.html%';
	IF ln_web_link_count = 0 THEN
		SELECT MAX(WEB_LINK_ID) + 1 INTO ln_web_link_id FROM WEB_CONTENT.WEB_LINK;
		INSERT INTO WEB_CONTENT.WEB_LINK(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER)
		VALUES(ln_web_link_id, 'System', 'sub_menu.html?usage=Y&' || 'caption=System', 'System', 'Administration', 'M', 100);
		
		INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT(WEB_LINK_ID, REQUIREMENT_ID) VALUES(ln_web_link_id, 1);
		INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT(WEB_LINK_ID, REQUIREMENT_ID) VALUES(ln_web_link_id, 53);
		
		COMMIT;
	END IF;
END;
/

UPDATE WEB_CONTENT.WEB_LINK
SET WEB_LINK_ORDER = 100, WEB_LINK_USAGE = 'Y', WEB_LINK_GROUP = 'Administration'
WHERE WEB_LINK_USAGE = 'M' AND WEB_LINK_LABEL IN ('Data Exchange', 'Welcome Info', 'Folio Designer') AND WEB_LINK_URL NOT LIKE 'sub_menu.html%';

COMMIT;

ALTER TABLE WEB_CONTENT.WEB_LINK MODIFY WEB_LINK_DESC VARCHAR2(1000);
INSERT INTO WEB_CONTENT.WEB_LINK_USAGE(WEB_LINK_USAGE, DESCRIPTION) VALUES('H', 'Help');
COMMIT;

DECLARE
	ln_web_link_id NUMBER;
	ln_web_link_count NUMBER;
BEGIN
	SELECT COUNT(1) INTO ln_web_link_count FROM WEB_CONTENT.WEB_LINK WHERE WEB_LINK_LABEL = 'Help' AND WEB_LINK_USAGE = 'M';
	IF ln_web_link_count = 0 THEN
		SELECT MAX(WEB_LINK_ID) + 1 INTO ln_web_link_id FROM WEB_CONTENT.WEB_LINK;
		INSERT INTO WEB_CONTENT.WEB_LINK(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER)
		VALUES(ln_web_link_id, 'Help', 'help.html', 'Help Videos', 'General', 'M', 9000);
		
		INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT(WEB_LINK_ID, REQUIREMENT_ID) VALUES(ln_web_link_id, 1);		
		COMMIT;
	END IF;
END;
/

DECLARE
	ln_web_link_id NUMBER;
	ln_web_link_count NUMBER;
BEGIN
	SELECT COUNT(1) INTO ln_web_link_count FROM WEB_CONTENT.WEB_LINK WHERE WEB_LINK_LABEL = 'Commonly Used Sections' AND WEB_LINK_USAGE = 'H';
	IF ln_web_link_count = 0 THEN
		SELECT MAX(WEB_LINK_ID) + 1 INTO ln_web_link_id FROM WEB_CONTENT.WEB_LINK;
		INSERT INTO WEB_CONTENT.WEB_LINK(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER)
		VALUES(ln_web_link_id, 'Commonly Used Sections', 'https://www.youtube.com/embed/Ds-rLo1MVrQ', 'This video walkthrough will help familiarize you with the most commonly used sections of the application. This includes navigating to areas for managing Devices, Users, ePort online, Refunds, RMAs, Bank Account management, Column Mapping, Mass Updates, System Preferences, Building Reports, Alerts, DEX Status, Report Register and Saved Reports.', 'Help', 'H', 100);
		
		INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT(WEB_LINK_ID, REQUIREMENT_ID) VALUES(ln_web_link_id, 1);		
		COMMIT;
	END IF;
END;
/

DECLARE
	ln_web_link_id NUMBER;
	ln_web_link_count NUMBER;
BEGIN
	SELECT COUNT(1) INTO ln_web_link_count FROM WEB_CONTENT.WEB_LINK WHERE WEB_LINK_LABEL = 'Home Page, Initial Reports, Change Password' AND WEB_LINK_USAGE = 'H';
	IF ln_web_link_count = 0 THEN
		SELECT MAX(WEB_LINK_ID) + 1 INTO ln_web_link_id FROM WEB_CONTENT.WEB_LINK;
		INSERT INTO WEB_CONTENT.WEB_LINK(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER)
		VALUES(ln_web_link_id, 'Home Page, Initial Reports, Change Password', 'https://www.youtube.com/embed/neeHSLc7bYQ', 'This video covers the Home page and changing your password. Also covered are initial reports provided on the Home page.', 'Help', 'H', 200);
		
		INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT(WEB_LINK_ID, REQUIREMENT_ID) VALUES(ln_web_link_id, 1);		
		COMMIT;
	END IF;
END;
/

DECLARE
	ln_web_link_id NUMBER;
	ln_web_link_count NUMBER;
BEGIN
	SELECT COUNT(1) INTO ln_web_link_count FROM WEB_CONTENT.WEB_LINK WHERE WEB_LINK_LABEL = 'User Management' AND WEB_LINK_USAGE = 'H';
	IF ln_web_link_count = 0 THEN
		SELECT MAX(WEB_LINK_ID) + 1 INTO ln_web_link_id FROM WEB_CONTENT.WEB_LINK;
		INSERT INTO WEB_CONTENT.WEB_LINK(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER)
		VALUES(ln_web_link_id, 'User Management', 'https://www.youtube.com/embed/s6mDX8Gu1pk', 'This video covers setting up sub-users and the different user permissions.', 'Help', 'H', 300);
		
		INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT(WEB_LINK_ID, REQUIREMENT_ID) VALUES(ln_web_link_id, 1);		
		COMMIT;
	END IF;
END;
/

DECLARE
	ln_web_link_id NUMBER;
	ln_web_link_count NUMBER;
BEGIN
	SELECT COUNT(1) INTO ln_web_link_count FROM WEB_CONTENT.WEB_LINK WHERE WEB_LINK_LABEL = 'Device Management' AND WEB_LINK_USAGE = 'H';
	IF ln_web_link_count = 0 THEN
		SELECT MAX(WEB_LINK_ID) + 1 INTO ln_web_link_id FROM WEB_CONTENT.WEB_LINK;
		INSERT INTO WEB_CONTENT.WEB_LINK(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER)
		VALUES(ln_web_link_id, 'Device Management', 'https://www.youtube.com/embed/LdqMjXtmmlg', 'This video covers how to setup location and machine specific information for ePorts, the importance of have the correct Zip Code and how it impacts a card holder''s statement, and how to make certain configuration changes like enabling and disabling two tier pricing.', 'Help', 'H', 400);
		
		INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT(WEB_LINK_ID, REQUIREMENT_ID) VALUES(ln_web_link_id, 1);		
		COMMIT;
	END IF;
END;
/

DECLARE
	ln_web_link_id NUMBER;
	ln_web_link_count NUMBER;
BEGIN
	SELECT COUNT(1) INTO ln_web_link_count FROM WEB_CONTENT.WEB_LINK WHERE WEB_LINK_LABEL = 'Column Maps' AND WEB_LINK_USAGE = 'H';
	IF ln_web_link_count = 0 THEN
		SELECT MAX(WEB_LINK_ID) + 1 INTO ln_web_link_id FROM WEB_CONTENT.WEB_LINK;
		INSERT INTO WEB_CONTENT.WEB_LINK(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER)
		VALUES(ln_web_link_id, 'Column Maps', 'https://www.youtube.com/embed/xwMDPbZtTns', 'This video covers Column Mapping, including why a device needs to be column mapped, existing maps, how to generate the information required to create a column map and then how to setup a column map from scratch, how to modify an existing column map and add products, how to apply and remove a column maps from a device.', 'Help', 'H', 500);
		
		INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT(WEB_LINK_ID, REQUIREMENT_ID) VALUES(ln_web_link_id, 1);		
		COMMIT;
	END IF;
END;
/

DECLARE
	ln_web_link_id NUMBER;
	ln_web_link_count NUMBER;
BEGIN
	SELECT COUNT(1) INTO ln_web_link_count FROM WEB_CONTENT.WEB_LINK WHERE WEB_LINK_LABEL = 'Payments' AND WEB_LINK_USAGE = 'H';
	IF ln_web_link_count = 0 THEN
		SELECT MAX(WEB_LINK_ID) + 1 INTO ln_web_link_id FROM WEB_CONTENT.WEB_LINK;
		INSERT INTO WEB_CONTENT.WEB_LINK(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER)
		VALUES(ln_web_link_id, 'Payments', 'https://www.youtube.com/embed/iAq2PZrdods', 'This video covers where deposit information is stored, details about the Payment Detail in EFT report including explanation about fee types, how to generate Transactions Included in EFT, and how to lookup pending balance.', 'Help', 'H', 600);
		
		INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT(WEB_LINK_ID, REQUIREMENT_ID) VALUES(ln_web_link_id, 1);		
		COMMIT;
	END IF;
END;
/

DECLARE
	ln_web_link_id NUMBER;
	ln_web_link_count NUMBER;
BEGIN
	SELECT COUNT(1) INTO ln_web_link_count FROM WEB_CONTENT.WEB_LINK WHERE WEB_LINK_LABEL = 'RMAs' AND WEB_LINK_USAGE = 'H';
	IF ln_web_link_count = 0 THEN
		SELECT MAX(WEB_LINK_ID) + 1 INTO ln_web_link_id FROM WEB_CONTENT.WEB_LINK;
		INSERT INTO WEB_CONTENT.WEB_LINK(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER)
		VALUES(ln_web_link_id, 'RMAs', 'https://www.youtube.com/embed/WWcWStlBLnI', 'This video covers the three RMA options, ePort swaps and repairs, parts swaps and repairs, and Rental Return RMAs.  This includes the difference in  policy difference between a swap and repair, and touches on which devices and parts are not eligible for advance swaps.  Contains specific instructions on how to submit the different RMAs and what to expect next.', 'Help', 'H', 700);
		
		INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT(WEB_LINK_ID, REQUIREMENT_ID) VALUES(ln_web_link_id, 1);		
		COMMIT;
	END IF;
END;
/

DECLARE
	ln_web_link_id NUMBER;
	ln_web_link_count NUMBER;
BEGIN
	SELECT COUNT(1) INTO ln_web_link_count FROM WEB_CONTENT.WEB_LINK WHERE WEB_LINK_LABEL = 'Reports' AND WEB_LINK_USAGE = 'H';
	IF ln_web_link_count = 0 THEN
		SELECT MAX(WEB_LINK_ID) + 1 INTO ln_web_link_id FROM WEB_CONTENT.WEB_LINK;
		INSERT INTO WEB_CONTENT.WEB_LINK(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER)
		VALUES(ln_web_link_id, 'Reports', 'https://www.youtube.com/embed/XqmMhxYisss', 'This video covers how to extensively utilize the Build A Report option.  The different options for running it, the different formats available, the different run options, and options within the report once it generates.  It covers how to retrieve older reports.  Shows how to run a specific Detailed Report good for assisting with inventory and how to save a user configured report and then how to regenerate that same report later.  Touches on the Saved Reports option and some recommended/useful reports.', 'Help', 'H', 800);
		
		INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT(WEB_LINK_ID, REQUIREMENT_ID) VALUES(ln_web_link_id, 1);		
		COMMIT;
	END IF;
END;
/

DECLARE
	ln_web_link_id NUMBER;
	ln_web_link_count NUMBER;
BEGIN
	SELECT COUNT(1) INTO ln_web_link_count FROM WEB_CONTENT.WEB_LINK WHERE WEB_LINK_LABEL = 'Report Register, Emailed Reports' AND WEB_LINK_USAGE = 'H';
	IF ln_web_link_count = 0 THEN
		SELECT MAX(WEB_LINK_ID) + 1 INTO ln_web_link_id FROM WEB_CONTENT.WEB_LINK;
		INSERT INTO WEB_CONTENT.WEB_LINK(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER)
		VALUES(ln_web_link_id, 'Report Register, Emailed Reports', 'https://www.youtube.com/embed/vGHu_AVPpwI', 'This video covers the four default reports setup with new accounts, some other emailed report options, how to setup new email recipients to receive reports, and how to view previously emailed reports.', 'Help', 'H', 900);
		
		INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT(WEB_LINK_ID, REQUIREMENT_ID) VALUES(ln_web_link_id, 1);		
		COMMIT;
	END IF;
END;
/

UPDATE REPORT.CAMPAIGN SET STATE = 'STARTED' 
WHERE START_DATE IS NULL OR START_DATE <= SYSDATE; 
COMMIT;

DELETE FROM PSS.CAMPAIGN_POS_PTA CA
WHERE CA.CAMPAIGN_ID IN 
    (SELECT C.CAMPAIGN_ID 
    FROM REPORT.CAMPAIGN C 
    WHERE C.STATE = 'DRAFT');
COMMIT;

BEGIN
	FOR L_REC IN (
		SELECT PP.POS_PTA_ID
		FROM PSS.POS_PTA PP
		JOIN PSS.PAYMENT_SUBTYPE PS ON PP.PAYMENT_SUBTYPE_ID = PS.PAYMENT_SUBTYPE_ID
		LEFT OUTER JOIN PSS.CAMPAIGN_POS_PTA CPP ON PP.POS_PTA_ID = CPP.POS_PTA_ID
		WHERE PS.PAYMENT_SUBTYPE_CLASS = 'Promotion' AND CPP.CAMPAIGN_ID IS NULL 
		AND (PP.POS_PTA_DEACTIVATION_TS IS NULL OR PP.POS_PTA_DEACTIVATION_TS > SYSDATE)
	) LOOP
		UPDATE PSS.POS_PTA
		SET POS_PTA_DEACTIVATION_TS = SYSDATE
		WHERE POS_PTA_ID = L_REC.POS_PTA_ID;
		COMMIT;
	END LOOP;
END;
/
COMMIT;

DECLARE
    CURSOR data_cur IS SELECT TO_DATE('01-JAN-2014') + LEVEL -1 AS DAY
                        FROM DUAL
                        CONNECT BY LEVEL <= TRUNC(SYSDATE) - TO_DATE('01-JAN-2014') + 1;
BEGIN
    FOR tran_data IN data_cur
    LOOP
        UPDATE /*+ index(TSBD IX_TSBD_TRAN_DATE) */ REPORT.TRANS_STAT_BY_DAY TSBD
        SET TSBD.LOYALTY_DISCOUNT_TOTAL = TSBD.LOYALTY_DISCOUNT,
            TSBD.PURCHASE_DISCOUNT_TOTAL = TSBD.PURCHASE_DISCOUNT,
            TSBD.CAMPAIGNS = (SELECT CAM.CAMPAIGN_NAME FROM REPORT.CAMPAIGN CAM WHERE CAM.CAMPAIGN_ID = TSBD.CAMPAIGN_ID),
            TSBD.CAMPAIGNS_TOTAL = (SELECT CAM.CAMPAIGN_NAME FROM REPORT.CAMPAIGN CAM WHERE CAM.CAMPAIGN_ID = TSBD.CAMPAIGN_ID)
        WHERE TSBD.TRAN_DATE between tran_data.day AND tran_data.day + 1 AND TSBD.CAMPAIGN_ID !=0;
        COMMIT;
    END LOOP;
END;
/

DECLARE
	min_tran_id NUMBER;
	max_tran_id NUMBER;
	tr_counter NUMBER;
BEGIN
	SELECT MIN(TRAN_ID) INTO min_tran_id FROM REPORT.TRANS;
    SELECT MAX(TRAN_ID) INTO max_tran_id FROM REPORT.TRANS;
    tr_counter := max_tran_id;
    WHILE tr_counter >= min_tran_id
    LOOP
        UPDATE REPORT.ACTIVITY_REF AR
        SET AR.LOYALTY_DISCOUNT_TOTAL = AR.LOYALTY_DISCOUNT,
            AR.PURCHASE_DISCOUNT_TOTAL = AR.PURCHASE_DISCOUNT,
            AR.CAMPAIGNS = (SELECT CAM.CAMPAIGN_NAME FROM REPORT.CAMPAIGN CAM WHERE CAM.CAMPAIGN_ID = AR.CAMPAIGN_ID),
            AR.CAMPAIGNS_TOTAL = (SELECT CAM.CAMPAIGN_NAME FROM REPORT.CAMPAIGN CAM WHERE CAM.CAMPAIGN_ID = AR.CAMPAIGN_ID)
        WHERE AR.TRAN_ID > tr_counter - 10000 AND AR.TRAN_ID <= tr_counter AND AR.CAMPAIGN_ID !=0;
        COMMIT;
        tr_counter := tr_counter - 10000;
    END LOOP;
END;
/
	
-- Set min_tran_id after execution of R58.USAPDB.USAT796.sql
DECLARE
	min_tran_id NUMBER;
	max_tran_id NUMBER;
	tr_counter NUMBER;
BEGIN
	SELECT MIN(TRAN_ID) INTO min_tran_id FROM REPORT.TRANS; 
	-- Use min_tran_id := SELECT MIN(TRAN_ID) INTO min_tran_id FROM REPORT.TRANS; executed on date of execution of R58.USAPDB.USAT796.sql
    SELECT MAX(TRAN_ID) INTO max_tran_id FROM REPORT.TRANS;
    tr_counter := max_tran_id;
    WHILE tr_counter >= min_tran_id
    LOOP
    	UPDATE REPORT.ACTIVITY_REF AR
        SET AR.PAYMENT_BATCH_ID = (SELECT MAX(DISTINCT L.BATCH_ID) FROM CORP.LEDGER L WHERE L.TRANS_ID = AR.TRAN_ID AND L.ENTRY_TYPE = 'CC')
        WHERE AR.TRAN_ID > tr_counter - 10000 AND AR.TRAN_ID <= tr_counter AND AR.PAYMENT_BATCH_ID IS NULL;
    	COMMIT;
    	tr_counter := tr_counter - 10000;
    END LOOP;
END;
/

-- Previous step must be completed.
--Batch_Total update.
DECLARE
	CURSOR bt_cur IS SELECT DISTINCT BATCH_ID FROM CORP.BATCH_TOTAL WHERE ENTRY_TYPE = 'CC'; 
	ln_loyalty_discount NUMBER;
	ln_ld_count NUMBER;
	ln_purchase_discount NUMBER;
	ln_pd_count NUMBER;
	ln_free_product_discount NUMBER;
	ln_fp_count NUMBER;
BEGIN
	FOR bt_data IN bt_cur
	LOOP
		SELECT SUM(LOYALTY_DISCOUNT), SUM(CASE WHEN LOYALTY_DISCOUNT != 0 THEN 1 ELSE 0 END), 
				SUM(PURCHASE_DISCOUNT), SUM(CASE WHEN PURCHASE_DISCOUNT != 0 THEN 1 ELSE 0 END),
				SUM(FREE_PRODUCT_DISCOUNT), SUM(CASE WHEN FREE_PRODUCT_DISCOUNT != 0 THEN 1 ELSE 0 END)
		INTO ln_loyalty_discount, ln_ld_count, ln_purchase_discount, ln_pd_count, ln_free_product_discount, ln_fp_count  
		FROM REPORT.ACTIVITY_REF
		WHERE PAYMENT_BATCH_ID = bt_data.BATCH_ID;
		IF ln_ld_count > 0 THEN
			INSERT INTO CORP.BATCH_TOTAL (BATCH_ID, ENTRY_TYPE, PAYABLE, LEDGER_AMOUNT, LEDGER_COUNT)
						VALUES (bt_data.BATCH_ID, 'LD', 'N', ln_loyalty_discount, ln_ld_count);
		END IF;	
		IF ln_pd_count > 0 THEN
			INSERT INTO CORP.BATCH_TOTAL (BATCH_ID, ENTRY_TYPE, PAYABLE, LEDGER_AMOUNT, LEDGER_COUNT)
						VALUES (bt_data.BATCH_ID, 'PD', 'N', ln_purchase_discount, ln_pd_count);
		END IF;	
		IF ln_fp_count > 0 THEN
			INSERT INTO CORP.BATCH_TOTAL (BATCH_ID, ENTRY_TYPE, PAYABLE, LEDGER_AMOUNT, LEDGER_COUNT)
						VALUES (bt_data.BATCH_ID, 'FP', 'N', ln_free_product_discount, ln_fp_count);
		END IF;	
		COMMIT;
	END LOOP;
END;
/
--Batch_Total_Hist update. 
DECLARE
	CURSOR bth_cur IS SELECT DISTINCT BATCH_ID FROM CORP.BATCH_TOTAL_HIST WHERE ENTRY_TYPE = 'CC'; 
	ln_loyalty_discount NUMBER;
	ln_ld_count NUMBER;
	ln_purchase_discount NUMBER;
	ln_pd_count NUMBER;
	ln_free_product_discount NUMBER;
	ln_fp_count NUMBER;
    if_exists NUMBER;
BEGIN
	FOR bth_data IN bth_cur
	LOOP
		SELECT SUM(LOYALTY_DISCOUNT), SUM(CASE WHEN LOYALTY_DISCOUNT != 0 THEN 1 ELSE 0 END), 
				SUM(PURCHASE_DISCOUNT), SUM(CASE WHEN PURCHASE_DISCOUNT != 0 THEN 1 ELSE 0 END),
				SUM(FREE_PRODUCT_DISCOUNT), SUM(CASE WHEN FREE_PRODUCT_DISCOUNT != 0 THEN 1 ELSE 0 END)
		INTO ln_loyalty_discount, ln_ld_count, ln_purchase_discount, ln_pd_count, ln_free_product_discount, ln_fp_count  
		FROM REPORT.ACTIVITY_REF
		WHERE PAYMENT_BATCH_ID = bth_data.BATCH_ID;
		IF ln_ld_count > 0 THEN
			UPDATE CORP.BATCH_TOTAL_HIST
            SET LEDGER_AMOUNT = ln_loyalty_discount,
                    LEDGER_COUNT = ln_ld_count
            WHERE BATCH_ID = bth_data.BATCH_ID AND ENTRY_TYPE = 'LD' AND PAYABLE = 'N';
            IF SQL%NOTFOUND THEN  
                INSERT INTO CORP.BATCH_TOTAL_HIST 
						VALUES (bth_data.BATCH_ID, 'LD', 'N', ln_loyalty_discount, ln_ld_count, 'SYSTEM', SYSTIMESTAMP, 'SYSTEM', SYSTIMESTAMP);
            END IF;
		END IF;	
		IF ln_pd_count > 0 THEN
            UPDATE CORP.BATCH_TOTAL_HIST
            SET LEDGER_AMOUNT = ln_purchase_discount,
                    LEDGER_COUNT = ln_pd_count
            WHERE BATCH_ID = bth_data.BATCH_ID AND ENTRY_TYPE = 'PD' AND PAYABLE = 'N';
            IF SQL%NOTFOUND THEN  
                INSERT INTO CORP.BATCH_TOTAL_HIST 
						VALUES (bth_data.BATCH_ID, 'PD', 'N', ln_purchase_discount, ln_pd_count, 'SYSTEM', SYSTIMESTAMP, 'SYSTEM', SYSTIMESTAMP);
            END IF;            
		END IF;	
		IF ln_fp_count > 0 THEN
			UPDATE CORP.BATCH_TOTAL_HIST
            SET LEDGER_AMOUNT = ln_free_product_discount,
                    LEDGER_COUNT = ln_pd_count
            WHERE BATCH_ID = bth_data.BATCH_ID AND ENTRY_TYPE = 'FP' AND PAYABLE = 'N';
            IF SQL%NOTFOUND THEN
                INSERT INTO CORP.BATCH_TOTAL_HIST 
						VALUES (bth_data.BATCH_ID, 'FP', 'N', ln_free_product_discount, ln_fp_count, 'SYSTEM', SYSTIMESTAMP, 'SYSTEM', SYSTIMESTAMP);
            END IF;
		END IF;	
		COMMIT;
	END LOOP;
END;
/
