DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := -1;
	LN_WEB_LINK_ID WEB_CONTENT.WEB_LINK.WEB_LINK_ID%TYPE := -1;
BEGIN
	SELECT MAX(REPORT_ID) INTO LN_REPORT_ID
	FROM REPORT.REPORTS WHERE REPORT_NAME = 'Transaction Processing Entry Summary With Device';
	
	IF LN_REPORT_ID > 0 THEN
		RETURN;
	END IF;

	SELECT REPORT.REPORTS_SEQ.NEXTVAL INTO LN_REPORT_ID FROM DUAL;

	INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
	SELECT LN_REPORT_ID, 'Transaction Processing Entry Summary With Device - {b} to {e}', 1, 0, 'Transaction Processing Entry Summary With Device', 'Transaction processing entry summary by customer, device and currency code.', 'N', USER_ID
	FROM REPORT.REPORTS WHERE REPORT_NAME = 'Transaction Processing Entry Summary';

	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE)
	VALUES(LN_REPORT_ID, 'query', 'SELECT 
		led.entry_type,
		NVL(dt.business_unit_name, ''~ Terminal Orphan'') business_unit_name,
		NVL(dt.monthly_payment, ''UNK'') monthly_payment,
		cr.currency_code,
		NVL(dt.customer_name, ''~ Terminal Orphan'') customer_name,
		TO_CHAR(TRUNC(DECODE (led.entry_type,
			''CC'', tr.settle_date,
			led.ledger_date
		), ''MONTH''), ''mm/dd/yyyy'') month_for,
		COUNT(1) tran_count,
		SUM(tr.total_amount) total_tran_amount,
		SUM(led.amount) total_ledger_amount,
		tr.trans_type_id,
		tt.trans_type_name,
		ee.eport_num
		FROM report.trans tr
		JOIN corp.currency cr
		ON cr.currency_id = tr.currency_id
		JOIN report.trans_state ts
		ON ts.state_id = tr.settle_state_id 
		LEFT OUTER JOIN (
			SELECT
			t.terminal_id,
			c.customer_name,
			bu.business_unit_name,
			DECODE(t.payment_schedule_id, 4, ''Y'', ''N'') monthly_payment
			FROM report.terminal t
			JOIN corp.customer c
			ON c.customer_id = t.customer_id
			JOIN corp.business_unit bu
			ON bu.business_unit_id = t.business_unit_id
		) dt
		ON dt.terminal_id = tr.terminal_id
		JOIN corp.ledger led
		ON led.trans_id = tr.tran_id
		AND led.deleted = ''N''
		JOIN report.trans_type tt
		ON tr.trans_type_id = tt.trans_type_id
		LEFT OUTER JOIN (
		SELECT DISTINCT terminal_id, FIRST_VALUE(eport_serial_num) OVER (PARTITION BY terminal_id ORDER BY start_date DESC) eport_num
		FROM report.terminal_eport te
		JOIN report.eport e ON te.eport_id = e.eport_id
		) ee ON tr.terminal_id = ee.terminal_id
		WHERE tr.settle_state_id IN (2, 3) 
		AND led.entry_type IN (''CC'', ''PF'', ''CB'', ''RF'') 
		AND NVL(tr.settle_date, led.create_date) >= CAST(? AS DATE) AND NVL(tr.settle_date, led.create_date) < CAST(? AS DATE) + 1
		GROUP BY
		led.entry_type,
		NVL(dt.business_unit_name, ''~ Terminal Orphan''),
		NVL(dt.monthly_payment, ''UNK''),
		cr.currency_code,
		NVL(dt.customer_name, ''~ Terminal Orphan''),
		TRUNC(DECODE (led.entry_type,
			''CC'', tr.settle_date,
			led.ledger_date
		), ''MONTH''),
		tr.trans_type_id,
		tt.trans_type_name,
		ee.eport_num');
				
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'dateFormat', 'MM/dd/yyyy');
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'header', 'true');	
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'paramNames', 'StartDate,EndDate');
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'paramTypes', 'TIMESTAMP,TIMESTAMP');
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'params.EndDate', '{e}');
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'params.StartDate', '{b}');	
		
	insert into web_content.web_link(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER) values(web_content.seq_web_link_id.nextval, 'Transaction Processing Entry Summary With Device', './select_date_range_frame_nonfolio.i?basicReportId='||LN_REPORT_ID, 'Transaction processing entry summary by customer, device and currency code.', 'Accounting', 'W', 0)
	RETURNING WEB_LINK_ID INTO LN_WEB_LINK_ID;
	insert into web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) values(LN_WEB_LINK_ID, 1);
	insert into web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) values(LN_WEB_LINK_ID, 14);
	
	INSERT INTO REPORT.USER_REPORT(USER_REPORT_ID, REPORT_ID, USER_ID, FREQUENCY_ID, STATUS, LATENCY, CCS_TRANSPORT_ID)
	SELECT REPORT.USER_REPORT_SEQ.NEXTVAL, LN_REPORT_ID, UR.USER_ID, UR.FREQUENCY_ID, UR.STATUS, UR.LATENCY, UR.CCS_TRANSPORT_ID
	FROM REPORT.REPORTS R
	JOIN REPORT.USER_REPORT UR ON R.REPORT_ID = UR.REPORT_ID
	WHERE R.REPORT_NAME = 'Transaction Processing Entry Summary' AND UR.STATUS = 'A';
	
	COMMIT;
END;
/

GRANT SELECT ON AUTHORITY.AUTHORITY TO USALIVE_APP_ROLE;
GRANT SELECT ON PSS.MERCHANT TO USALIVE_APP_ROLE;
GRANT SELECT ON PSS.TERMINAL TO USALIVE_APP_ROLE;

DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := -1;
	LN_WEB_LINK_ID WEB_CONTENT.WEB_LINK.WEB_LINK_ID%TYPE := -1;
BEGIN
	SELECT MAX(REPORT_ID) INTO LN_REPORT_ID
	FROM REPORT.REPORTS WHERE REPORT_NAME = 'Device MID Configuration';
	
	IF LN_REPORT_ID > 0 THEN
		RETURN;
	END IF;

	SELECT REPORT.REPORTS_SEQ.NEXTVAL INTO LN_REPORT_ID FROM DUAL;

	INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
	SELECT LN_REPORT_ID, 'Device MID Configuration', 6, 0, 'Device MID Configuration', 'Device MID Configuration report with Device Serial Number, Customer, Location, Merchant Name, MID, TID and DBA.', 'N', USER_ID
	FROM REPORT.USER_LOGIN
	WHERE USER_NAME = 'USATMaster';

	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE)
	VALUES(LN_REPORT_ID, 'query', 'SELECT DISTINCT
	C.CUSTOMER_NAME "Customer", L.LOCATION_NAME "Location", E.EPORT_SERIAL_NUM "Device", A.AUTHORITY_NAME "Authority", M.MERCHANT_NAME "Merchant Name",
	M.MERCHANT_CD "Merchant ID", TX.TERMINAL_CD "Terminal ID", COALESCE(T.DOING_BUSINESS_AS, C.DOING_BUSINESS_AS) "Doing Business As",
	TO_CHAR(D.LAST_ACTIVITY_TS, ''MM/DD/YYYY HH24:MI:SS'') "Last Activity"
	FROM REPORT.VW_TERMINAL_EPORT TE
	JOIN REPORT.EPORT E ON TE.EPORT_ID = E.EPORT_ID
	JOIN REPORT.TERMINAL T ON TE.TERMINAL_ID = T.TERMINAL_ID
	JOIN CORP.CUSTOMER C ON T.CUSTOMER_ID = C.CUSTOMER_ID
	JOIN REPORT.LOCATION L ON T.LOCATION_ID = L.LOCATION_ID
	JOIN DEVICE.DEVICE_LAST_ACTIVE DLA ON E.EPORT_SERIAL_NUM = DLA.DEVICE_SERIAL_CD
	JOIN DEVICE.DEVICE D ON DLA.DEVICE_ID = D.DEVICE_ID
	JOIN PSS.POS P ON D.DEVICE_ID = P.DEVICE_ID
	JOIN PSS.POS_PTA PP ON P.POS_ID = PP.POS_ID
	JOIN PSS.PAYMENT_SUBTYPE PS ON PP.PAYMENT_SUBTYPE_ID = PS.PAYMENT_SUBTYPE_ID
	JOIN PSS.TERMINAL TX ON PP.PAYMENT_SUBTYPE_KEY_ID = TX.TERMINAL_ID
	JOIN PSS.MERCHANT M ON TX.MERCHANT_ID = M.MERCHANT_ID
	JOIN AUTHORITY.AUTHORITY A ON M.AUTHORITY_ID = A.AUTHORITY_ID
	WHERE PS.PAYMENT_SUBTYPE_TABLE_NAME = ''TERMINAL'' AND PS.PAYMENT_SUBTYPE_CLASS != ''Authority::NOP''
	AND PP.POS_PTA_ACTIVATION_TS < SYSDATE AND (PP.POS_PTA_DEACTIVATION_TS IS NULL OR PP.POS_PTA_DEACTIVATION_TS > SYSDATE)
	ORDER BY C.CUSTOMER_NAME, L.LOCATION_NAME, E.EPORT_SERIAL_NUM');
				
	INSERT INTO report.report_param VALUES(LN_REPORT_ID, 'isSQLFolio', 'true');
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'paramNames', '');
		
	insert into web_content.web_link(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER) values(web_content.seq_web_link_id.nextval, 'Device MID Configuration', './run_sql_folio_report_async.i?basicReportId=' || LN_REPORT_ID, 'Device MID Configuration report with Device Serial Number, Customer, Location, Merchant Name, MID, TID and DBA.', 'Accounting', 'W', 0)
	RETURNING WEB_LINK_ID INTO LN_WEB_LINK_ID;
	insert into web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) values(LN_WEB_LINK_ID, 1);
	insert into web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) values(LN_WEB_LINK_ID, 14);
	
	COMMIT;
END;
/
