DROP SEQUENCE G4OP.DEX_COUNTER_SEQ;
DROP SEQUENCE G4OP.DEX_TOTAL_SEQ;
DROP SEQUENCE G4OP.DEX_DELTA_SEQ;
DROP PACKAGE G4OP.BVFUSION;
DROP TABLE G4OP.DEX_TOTAL;
DROP TABLE G4OP.DEX_DELTA;
DROP TABLE G4OP.DEX_COUNTER;
