ALTER TABLE KM.ENCRYPTED ADD USER_NAME VARCHAR(512);

CREATE OR REPLACE FUNCTION KM.STORE_ENCRYPTED(
    pn_encrypted_id OUT KM.ENCRYPTED.ENCRYPTED_ID%TYPE,
    pn_key_id KM.ENCRYPTED.KEY_ID%TYPE,
    pba_encrypted_data KM.ENCRYPTED.ENCRYPTED_DATA%TYPE,
    pd_expiration_utc_ts KM.ENCRYPTED.EXPIRATION_UTC_TS%TYPE,
    pv_user_name KM.ENCRYPTED.USER_NAME%TYPE DEFAULT NULL)
    SECURITY DEFINER
AS $$
DECLARE
    lv_encrypted_partition VARCHAR;
    ln_start_expiration_utc_ts KM.ENCRYPTED.EXPIRATION_UTC_TS%TYPE;
    ln_end_expiration_utc_ts KM.ENCRYPTED.EXPIRATION_UTC_TS%TYPE;
BEGIN
    pn_encrypted_id := NEXTVAL('KM.SEQ_ENCRYPTED_ID');
    SELECT a.pv_encrypted_partition, a.pn_start_expiration_utc_ts, a.pn_end_expiration_utc_ts
      INTO lv_encrypted_partition, ln_start_expiration_utc_ts, ln_end_expiration_utc_ts
      FROM KM.GET_ENCRYPTED_PARITION(pd_expiration_utc_ts) a;
    LOOP
        BEGIN
            EXECUTE 'INSERT INTO KM.' || lv_encrypted_partition || '(ENCRYPTED_ID, KEY_ID, ENCRYPTED_DATA, EXPIRATION_UTC_TS, USER_NAME) VALUES($1, $2, $3, $4, $5)'
                USING pn_encrypted_id, pn_key_id, pba_encrypted_data, pd_expiration_utc_ts, pv_user_name; 
            EXIT;
        EXCEPTION
            WHEN undefined_table THEN
                PERFORM KM.CREATE_ENCRYPTED_PARITION(lv_encrypted_partition, ln_start_expiration_utc_ts, ln_end_expiration_utc_ts);      
        END;    
    END LOOP;   
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION KM.STORE_ENCRYPTED(
    pn_key_id KM.ENCRYPTED.KEY_ID%TYPE,
    pba_encrypted_data KM.ENCRYPTED.ENCRYPTED_DATA%TYPE,
    pd_expiration_utc_ts KM.ENCRYPTED.EXPIRATION_UTC_TS%TYPE,
    pv_user_name KM.ENCRYPTED.USER_NAME%TYPE) TO use_km;

DROP FUNCTION KM.STORE_ENCRYPTED(
    pn_encrypted_id OUT KM.ENCRYPTED.ENCRYPTED_ID%TYPE,
    pn_key_id KM.ENCRYPTED.KEY_ID%TYPE,
    pba_encrypted_data KM.ENCRYPTED.ENCRYPTED_DATA%TYPE,
    pd_expiration_utc_ts KM.ENCRYPTED.EXPIRATION_UTC_TS%TYPE);
    
CREATE OR REPLACE FUNCTION KM.RETRIEVE_ENCRYPTED_WITH_USERNAME(
    pn_encrypted_id KM.ENCRYPTED.ENCRYPTED_ID%TYPE,
    pn_key_id OUT KM.ENCRYPTED.KEY_ID%TYPE,
    pba_encrypted_data OUT KM.ENCRYPTED.ENCRYPTED_DATA%TYPE,
    pv_user_name OUT KM.ENCRYPTED.USER_NAME%TYPE)
    SECURITY DEFINER
AS $$
DECLARE
BEGIN
    SELECT KEY_ID, ENCRYPTED_DATA, USER_NAME
      INTO pn_key_id, pba_encrypted_data, pv_user_name
      FROM KM.ENCRYPTED
     WHERE ENCRYPTED_ID = pn_encrypted_id;        
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION KM.RETRIEVE_ENCRYPTED_WITH_USERNAME(
    pn_encrypted_id KM.ENCRYPTED.ENCRYPTED_ID%TYPE) TO use_km;
    