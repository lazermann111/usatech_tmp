declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'ALTER TABLE PSS.TERMINAL ADD (VIRTUAL_TERMINAL NUMBER(1) DEFAULT 0 )';
    exception when column_exists then null;
end;
/

declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'ALTER TABLE PSS.ARAMARK_PAYMENT_TYPE ADD (VIRTUAL_ITEM NUMBER(1) DEFAULT 0 )';
    exception when column_exists then null;
end;
/

declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'ALTER TABLE PSS.INTERNAL_PAYMENT_TYPE ADD (VIRTUAL_ITEM NUMBER(1) DEFAULT 0 )';
    exception when column_exists then null;
end;
/

ALTER TABLE PSS.INTERNAL_PAYMENT_TYPE
MODIFY (INTERNAL_PAYMENT_TYPE_DESC VARCHAR2(255 BYTE) );

ALTER TABLE PSS.ARAMARK_PAYMENT_TYPE
MODIFY (ARAMARK_PAYMENT_TYPE_DESC VARCHAR2(255 BYTE) );


COMMENT ON COLUMN PSS.TERMINAL.VIRTUAL_TERMINAL IS '0 = standard terminal, 1 = virtual terminal used for balancing load (see task USAT-215)';

COMMENT ON COLUMN PSS.ARAMARK_PAYMENT_TYPE.VIRTUAL_ITEM IS '0 = standard terminal, 1 = virtual terminal used for balancing load (see task USAT-215)';

COMMENT ON COLUMN PSS.INTERNAL_PAYMENT_TYPE.VIRTUAL_ITEM IS '0 = standard terminal, 1 = virtual terminal used for balancing load (see task USAT-215)';

