DECLARE
  ln_exists_count NUMBER;
  PROCEDURE CREATE_TANDEM_MERCHANT (
    pv_tdid PSS.MERCHANT.AUTHORITY_ASSIGNED_NUMBER%TYPE,
    pv_merchant_name PSS.MERCHANT.MERCHANT_NAME%TYPE,
    pv_dba PSS.MERCHANT.DOING_BUSINESS_AS%TYPE,
    pv_merchant_cd PSS.MERCHANT.MERCHANT_CD%TYPE,
    pv_terminal_cd PSS.TERMINAL.TERMINAL_CD%TYPE,
    pn_mcc PSS.MERCHANT.MCC%TYPE,
    pv_merchant_bus_name PSS.MERCHANT.MERCHANT_BUS_NAME%TYPE,
    pv_merchant_group_cd PSS.MERCHANT.MERCHANT_GROUP_CD%TYPE)
  IS
  BEGIN
    SELECT COUNT(1)
    INTO ln_exists_count
    FROM PSS.MERCHANT 
    WHERE MERCHANT_CD = pv_merchant_cd
    AND AUTHORITY_ID = (select authority_id from authority.authority where authority_name = 'Tandem');
    
    IF ln_exists_count > 0 THEN
      dbms_output.put_line('Merchant account already exists: '||pv_merchant_cd);
      RETURN;
    END IF;
    
    INSERT INTO PSS.MERCHANT(MERCHANT_CD, MERCHANT_NAME, MERCHANT_DESC, MERCHANT_BUS_NAME, AUTHORITY_ID, DOING_BUSINESS_AS, MCC, AUTHORITY_ASSIGNED_NUMBER, MERCHANT_GROUP_CD)
    SELECT pv_merchant_cd, pv_merchant_name, pv_merchant_name, pv_merchant_bus_name, AUTHORITY_ID, pv_dba, pn_mcc, pv_tdid, pv_merchant_group_cd
    FROM AUTHORITY.AUTHORITY M
    WHERE AUTHORITY_NAME = 'Tandem';
  
    INSERT INTO PSS.TERMINAL(TERMINAL_CD, TERMINAL_NEXT_BATCH_NUM, MERCHANT_ID, TERMINAL_DESC, TERMINAL_STATE_ID,
      TERMINAL_MAX_BATCH_NUM, TERMINAL_BATCH_MAX_TRAN, TERMINAL_BATCH_CYCLE_NUM, TERMINAL_MIN_BATCH_NUM,
      TERMINAL_MIN_BATCH_CLOSE_HR, TERMINAL_MAX_BATCH_CLOSE_HR, TERMINAL_BATCH_MIN_TRAN)
    SELECT pv_terminal_cd, 1, MERCHANT_ID, pv_merchant_name || ' Terminal', 1, 999, 1, 1, 1, 0, 4, 1
    FROM PSS.MERCHANT M
    WHERE MERCHANT_CD = pv_merchant_cd
    AND AUTHORITY_ID = (select authority_id from authority.authority where authority_name = 'Tandem');
  END;
BEGIN
	CREATE_TANDEM_MERCHANT('318144', 'USAT Vending 30', 'Soda Snack Vending', '089000000001', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
	CREATE_TANDEM_MERCHANT('318145', 'USAT Vending 31', 'Soda Snack Vending', '225000000001', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
	CREATE_TANDEM_MERCHANT('318146', 'USAT Vending 32', 'Soda Snack Vending', '257000000000', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
	CREATE_TANDEM_MERCHANT('318148', 'USAT Vending 33', 'Soda Snack Vending', '321000000500', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
	CREATE_TANDEM_MERCHANT('318149', 'USAT Vending 34', 'Soda Snack Vending', '600100000000', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
	CREATE_TANDEM_MERCHANT('318150', 'USAT Vending 35', 'Soda Snack Vending', '820000051000', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
	CREATE_TANDEM_MERCHANT('318151', 'USAT Vending 36', 'Soda Snack Vending', '838000000000', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
	CREATE_TANDEM_MERCHANT('318152', 'USAT Vending 37', 'Soda Snack Vending', '889000000002', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
	CREATE_TANDEM_MERCHANT('318153', 'USAT Vending 38', 'Soda Snack Vending', '903000000000', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
	CREATE_TANDEM_MERCHANT('318156', 'USAT Vending 39', 'Soda Snack Vending', '967000000000', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
	CREATE_TANDEM_MERCHANT('318157', 'USAT Vending 40', 'Soda Snack Vending', '990000000000', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
	CREATE_TANDEM_MERCHANT('318158', 'USAT Amusement 02', 'Amusement Vending', '089000000002', '001', 7994, 'USA Technologies Amusement', 'amusement_7994');
	CREATE_TANDEM_MERCHANT('318159', 'USAT Amusement 03', 'Amusement Vending', '225000000002', '001', 7994, 'USA Technologies Amusement', 'amusement_7994');
	CREATE_TANDEM_MERCHANT('318160', 'USAT Car Wash 01', 'Car Wash Vending', '257000000001', '001', 7542, 'USA Technologies Car Wash', 'car_wash_7542');
	CREATE_TANDEM_MERCHANT('318161', 'USAT Car Wash 02', 'Car Wash Vending', '321000000501', '001', 7542, 'USA Technologies Car Wash', 'car_wash_7542');
	CREATE_TANDEM_MERCHANT('318163', 'USAT Other 01', 'USA Technologies', '600100000001', '001', 5999, 'USA Technologies Other', 'usat_other_5999');
	CREATE_TANDEM_MERCHANT('318165', 'USAT Other 02', 'USA Technologies', '820000051001', '001', 5999, 'USA Technologies Other', 'usat_other_5999');  
	COMMIT;
END;
/