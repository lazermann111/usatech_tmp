UPDATE REPORT.REPORTS SET USAGE = 'D' WHERE REPORT_NAME LIKE 'UNI %';
COMMIT;

DECLARE
	--Bank Acct Orphans
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := 111;
BEGIN
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'paramNames', 'StartDate,EndDate');
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'paramTypes', 'TIMESTAMP,TIMESTAMP');
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'params.StartDate', '{b}');
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'params.EndDate', '{e}');

	UPDATE REPORT.REPORT_PARAM
	SET PARAM_VALUE = REPLACE(REPLACE(PARAM_VALUE, 'TO_DATE(''{b}'',''MM/DD/YYYY HH24:MI:SS'')', 'CAST(? AS DATE)'), 'TO_DATE(''{e}'',''MM/DD/YYYY HH24:MI:SS'')', 'CAST(? AS DATE)')
	WHERE REPORT_ID = LN_REPORT_ID AND PARAM_NAME = 'query';
	
	COMMIT;
END;
/

DECLARE
	--EFT Data Export
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := 12;
BEGIN
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'paramNames', 'eftId');
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'paramTypes', 'NUMERIC');
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'params.eftId', '{x}');

	UPDATE REPORT.REPORT_PARAM
	SET PARAM_VALUE = REPLACE(PARAM_VALUE, '{x}', '?')
	WHERE REPORT_ID = LN_REPORT_ID AND PARAM_NAME = 'query';
	
	COMMIT;
END;
/

DECLARE
	--EFT Summary
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := 57;
BEGIN
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'paramNames', 'EndDate,StartDate,EndDate');
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'paramTypes', 'TIMESTAMP,TIMESTAMP,TIMESTAMP');
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'params.StartDate', '{b}');
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'params.EndDate', '{e}');
	
	UPDATE REPORT.REPORT_PARAM
	SET PARAM_VALUE = 'SELECT cus.customer_name, cbk.bank_acct_nbr, v.doc_id eft_id, v.sent_date eft_date, v.total_amount, v.tran_month, 
	SUM(CASE WHEN v.tid = 3 THEN v.cr_a ELSE NULL END) credit_settled, 
	SUM(CASE WHEN v.tid = 3 THEN v.rf_a ELSE NULL END) refund_settled, 
	SUM(CASE WHEN v.tid = 3 THEN v.cb_a ELSE NULL END) chargeback_settled, 
	SUM(CASE WHEN v.tid = 3 THEN v.pf_a ELSE NULL END) process_fee_settled, 
	SUM(CASE WHEN v.tid = 3 THEN v.sf_a ELSE NULL END) service_fee_settled, 
	SUM(CASE WHEN v.tid = 3 THEN v.ad_a ELSE NULL END) adjust_settled, 
	SUM(CASE WHEN v.tid = 3 THEN v.rd_a ELSE NULL END) rounding_settled, 
	SUM(CASE WHEN v.tid = 3 THEN v.nrf_a ELSE NULL END) net_revenue_fee_settled, 
	SUM(CASE WHEN v.tid = 3 THEN v.net_a ELSE NULL END) net_settled, 
	SUM(CASE WHEN v.tid = 4 THEN v.cr_a ELSE NULL END) credit_pending, 
	SUM(CASE WHEN v.tid = 4 THEN v.rf_a ELSE NULL END) refund_pending, 
	SUM(CASE WHEN v.tid = 4 THEN v.cb_a ELSE NULL END) chargeback_pending, 
	SUM(CASE WHEN v.tid = 4 THEN v.pf_a ELSE NULL END) process_fee_pending, 
	SUM(CASE WHEN v.tid = 4 THEN v.sf_a ELSE NULL END) service_fee_pending, 
	SUM(CASE WHEN v.tid = 4 THEN v.ad_a ELSE NULL END) adjust_pending, 
	SUM(CASE WHEN v.tid = 4 THEN v.rd_a ELSE NULL END) rounding_pending, 
	SUM(CASE WHEN v.tid = 4 THEN v.nrf_a ELSE NULL END) net_revenue_fee_pending, 
	SUM(CASE WHEN v.tid = 4 THEN v.net_a ELSE NULL END) net_pending, 
	SUM(CASE WHEN v.tid = 5 THEN v.cr_a ELSE NULL END) credit_failed, 
	SUM(CASE WHEN v.tid = 5 THEN v.rf_a ELSE NULL END) refund_failed, 
	SUM(CASE WHEN v.tid = 5 THEN v.cb_a ELSE NULL END) chargeback_failed, 
	SUM(CASE WHEN v.tid = 5 THEN v.pf_a ELSE NULL END) process_fee_failed, 
	SUM(CASE WHEN v.tid = 5 THEN v.sf_a ELSE NULL END) service_fee_failed, 
	SUM(CASE WHEN v.tid = 5 THEN v.ad_a ELSE NULL END) adjust_failed, 
	SUM(CASE WHEN v.tid = 5 THEN v.rd_a ELSE NULL END) rounding_failed, 
	SUM(CASE WHEN v.tid = 5 THEN v.nrf_a ELSE NULL END) net_revenue_fee_failed, 
	SUM(CASE WHEN v.tid = 5 THEN v.net_a ELSE NULL END) net_failed 
FROM corp.customer_bank cbk, corp.customer cus, ( 
	SELECT doc_id, sent_date, total_amount, customer_bank_id, tran_month, (CASE WHEN e.tid = 1 THEN 4 WHEN e.tid = 2 THEN 3 WHEN e.tid = 6 THEN 3 ELSE e.tid END) tid, 
	SUM(CASE WHEN e.en_type = ''CC'' THEN e.amt ELSE NULL END) cr_a, 
	SUM(CASE WHEN e.en_type = ''RF'' THEN e.amt ELSE NULL END) rf_a, 
	SUM(CASE WHEN e.en_type = ''CB'' THEN e.amt ELSE NULL END) cb_a, 
	SUM(CASE WHEN e.en_type = ''PF'' THEN e.amt ELSE NULL END) pf_a, 
	SUM(CASE WHEN e.en_type = ''SF'' THEN e.amt ELSE NULL END) sf_a, 
	SUM(CASE WHEN e.en_type = ''AD'' THEN e.amt ELSE NULL END) ad_a, 
	SUM(CASE WHEN e.en_type = ''SB'' THEN e.amt ELSE NULL END) rd_a, 
	SUM(CASE WHEN e.en_type = ''NR'' THEN e.amt ELSE NULL END) nrf_a, 
	SUM(e.amt) net_a FROM ( 
		SELECT doc_id, sent_date, total_amount, customer_bank_id, tran_month, en_type, tid, SUM(amount) amt FROM (
			SELECT d.doc_id, d.sent_date, d.total_amount, d.customer_bank_id, TRUNC(l.entry_date, ''MONTH'') tran_month, l.entry_type en_type, 
			(CASE WHEN NVL(t.settle_date, MAX_DATE) >= CAST(? AS DATE) THEN 1 ELSE l.settle_state_id END) tid, l.amount
			FROM corp.ledger l, corp.batch b, report.trans t, corp.doc d 
			WHERE l.batch_id = b.batch_id AND b.doc_id = d.doc_id AND l.deleted = ''N'' AND d.sent_date BETWEEN CAST(? AS DATE) AND CAST(? AS DATE) 
			AND l.trans_id = t.tran_id (+) 
		)
		GROUP BY doc_id, sent_date, total_amount, customer_bank_id, tran_month, en_type, tid 
	) e GROUP BY doc_id, sent_date, total_amount, customer_bank_id, tran_month, e.tid 
) v WHERE cbk.customer_id = cus.customer_id AND v.customer_bank_id = cbk.customer_bank_id 
GROUP BY cus.customer_name, cbk.bank_acct_nbr, v.sent_date, v.doc_id, v.total_amount, v.tran_month 
ORDER BY UPPER(cus.customer_name), cbk.bank_acct_nbr, v.sent_date, v.doc_id, v.tran_month'
	WHERE REPORT_ID = LN_REPORT_ID AND PARAM_NAME = 'query';
	
	COMMIT;
END;
/

DECLARE
	--EFT Trans Reference Export
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := 13;
BEGIN
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'paramNames', 'eftId');
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'paramTypes', 'NUMERIC');
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'params.eftId', '{x}');

	UPDATE REPORT.REPORT_PARAM
	SET PARAM_VALUE = REPLACE(PARAM_VALUE, '{x}', '?')
	WHERE REPORT_ID = LN_REPORT_ID AND PARAM_NAME = 'query';
	
	COMMIT;
END;
/

DECLARE
	--EFT Trans Reference Export (Tran Id)
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := 47;
BEGIN
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'paramNames', 'eftId');
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'paramTypes', 'NUMERIC');
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'params.eftId', '{x}');

	UPDATE REPORT.REPORT_PARAM
	SET PARAM_VALUE = REPLACE(PARAM_VALUE, '{x}', '?')
	WHERE REPORT_ID = LN_REPORT_ID AND PARAM_NAME = 'query';
	
	COMMIT;
END;
/

DECLARE
	--Service Fees
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := 54;
BEGIN
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'paramNames', 'StartDate');
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'paramTypes', 'TIMESTAMP');
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'params.StartDate', '{b}');

	UPDATE REPORT.REPORT_PARAM
	SET PARAM_VALUE = REPLACE(PARAM_VALUE, 'TO_DATE(''{b}'', ''MM/DD/YYYY'')', 'CAST(? AS DATE)')
	WHERE REPORT_ID = LN_REPORT_ID AND PARAM_NAME = 'query';
	
	COMMIT;
END;
/

GRANT SELECT ON REPORT.VW_TRAN_DETAIL TO USAT_RPT_GEN_ROLE;
GRANT SELECT ON REPORT.VW_USER_TERMINAL TO USAT_RPT_GEN_ROLE;

DECLARE
	--Transaction Data Export
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := 49;
BEGIN
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'paramNames', 'batchId,userId');
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'paramTypes', 'NUMERIC,NUMERIC');
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'params.batchId', '{x}');
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'params.userId', '{u}');

	UPDATE REPORT.REPORT_PARAM
	SET PARAM_VALUE = REPLACE(REPLACE(PARAM_VALUE, '{x}', '?'), '{u}', '?')
	WHERE REPORT_ID = LN_REPORT_ID AND PARAM_NAME = 'query';
	
	COMMIT;
END;
/

DECLARE
	--Transaction Data Export (Tran Id)
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := 10;
BEGIN
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'paramNames', 'batchId,userId');
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'paramTypes', 'NUMERIC,NUMERIC');
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'params.batchId', '{x}');
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'params.userId', '{u}');

	UPDATE REPORT.REPORT_PARAM
	SET PARAM_VALUE = REPLACE(REPLACE(PARAM_VALUE, '{x}', '?'), '{u}', '?')
	WHERE REPORT_ID = LN_REPORT_ID AND PARAM_NAME = 'query';
	
	COMMIT;
END;
/

DECLARE
	--Transaction Data Export with Device Tran Code
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := 69;
BEGIN
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'paramNames', 'batchId,userId');
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'paramTypes', 'NUMERIC,NUMERIC');
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'params.batchId', '{x}');
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'params.userId', '{u}');

	UPDATE REPORT.REPORT_PARAM
	SET PARAM_VALUE = REPLACE(REPLACE(PARAM_VALUE, '{x}', '?'), '{u}', '?')
	WHERE REPORT_ID = LN_REPORT_ID AND PARAM_NAME = 'query';
	
	COMMIT;
END;
/

GRANT EXECUTE ON REPORT.GET_RECEIPT_TRAN_ID TO USAT_RPT_GEN_ROLE;

DECLARE
	--Transaction Data Export with Receipt Number
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := 68;
BEGIN
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'paramNames', 'batchId,userId');
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'paramTypes', 'NUMERIC,NUMERIC');
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'params.batchId', '{x}');
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'params.userId', '{u}');

	UPDATE REPORT.REPORT_PARAM
	SET PARAM_VALUE = REPLACE(REPLACE(PARAM_VALUE, '{x}', '?'), '{u}', '?')
	WHERE REPORT_ID = LN_REPORT_ID AND PARAM_NAME = 'query';
	
	COMMIT;
END;
/

DECLARE
	--Transaction Data Included in EFT
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := 50;
BEGIN
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'paramNames', 'eftId');
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'paramTypes', 'NUMERIC');
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'params.eftId', '{x}');

	UPDATE REPORT.REPORT_PARAM
	SET PARAM_VALUE = REPLACE(PARAM_VALUE, '{x}', '?')
	WHERE REPORT_ID = LN_REPORT_ID AND PARAM_NAME = 'query';
	
	COMMIT;
END;
/

DECLARE
	--Transaction Data Included in EFT (Tran Id)
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := 5;
BEGIN
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'paramNames', 'eftId');
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'paramTypes', 'NUMERIC');
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'params.eftId', '{x}');

	UPDATE REPORT.REPORT_PARAM
	SET PARAM_VALUE = REPLACE(PARAM_VALUE, '{x}', '?')
	WHERE REPORT_ID = LN_REPORT_ID AND PARAM_NAME = 'query';
	
	COMMIT;
END;
/

DECLARE
	--Transaction Data Included in EFT (by Device)
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := 162;
BEGIN
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'paramNames', 'eftId');
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'paramTypes', 'NUMERIC');
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'params.eftId', '{x}');

	UPDATE REPORT.REPORT_PARAM
	SET PARAM_VALUE = REPLACE(PARAM_VALUE, '{x}', '?')
	WHERE REPORT_ID = LN_REPORT_ID AND PARAM_NAME = 'query';
	
	COMMIT;
END;
/

DECLARE
	--Zero Trans Report
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := 157;
BEGIN
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'paramNames', 'userId');
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'paramTypes', 'NUMERIC');
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'params.userId', '{u}');

	UPDATE REPORT.REPORT_PARAM
	SET PARAM_VALUE = REPLACE(PARAM_VALUE, '{u}', '?')
	WHERE REPORT_ID = LN_REPORT_ID AND PARAM_NAME = 'query';
	
	COMMIT;
END;
/

DECLARE
	--Zero Trans Report - No Headers
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := 11;
BEGIN
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'paramNames', 'userId');
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'paramTypes', 'NUMERIC');
	INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'params.userId', '{u}');

	UPDATE REPORT.REPORT_PARAM
	SET PARAM_VALUE = REPLACE(PARAM_VALUE, '{u}', '?')
	WHERE REPORT_ID = LN_REPORT_ID AND PARAM_NAME = 'query';
	
	COMMIT;
END;
/

