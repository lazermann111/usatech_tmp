begin
    -- Reassign first 10 devices for Tandem Merchant ID 612000000000 from terminal 001 to
    -- new Tandem terminals 002-010 for "USA Technologies Vending 01" in PROD
    PSS.PKG_TERMINAL_REASSIGN.SP_REASSIGN_TANDEM_TERMINALS('612000000000', 10);
end;
/