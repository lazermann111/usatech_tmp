DECLARE
	ln_property_list_version NUMBER := 21;
	ln_device_type_id device_type.device_type_id%TYPE := 13;
	ln_config_template_type_id config_template_type.config_template_type_id%TYPE := 1;
	lv_config_template_name config_template.config_template_name%TYPE := 'DEFAULT-CFG-' || ln_device_type_id || '-' || ln_property_list_version;
	ln_config_template_id config_template.config_template_id%TYPE;
	
	PROCEDURE ADD_DSP(
		pv_index VARCHAR2,
		pv_configurable VARCHAR2,
		pv_name VARCHAR2) 
	IS
	BEGIN
		INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_UI_CONFIGURABLE, DEVICE_SETTING_PARAMETER_NAME) 
		SELECT pv_index, pv_configurable, pv_name
		FROM DUAL 
		WHERE NOT EXISTS (
			SELECT 1 
			FROM DEVICE.DEVICE_SETTING_PARAMETER 
			WHERE DEVICE_SETTING_PARAMETER_CD = pv_index);
	END;

	PROCEDURE ADD_CTS_META(
		pv_PARAMETER_CD VARCHAR2,
		pv_SETTING_VALUE VARCHAR2,
		pn_CATEGORY_ID NUMBER,
		pv_DISPLAY VARCHAR2,
		pv_DESCRIPTION VARCHAR2,
		pv_EDITOR VARCHAR2,
		pn_FIELD_SIZE NUMBER,
		pn_DISPLAY_ORDER NUMBER,
		pv_DATA_MODE VARCHAR2,
		pv_DATA_MODE_AUX VARCHAR2,
		pn_REGEX_ID NUMBER,
		pv_NAME VARCHAR2,
		pv_CUSTOMER_EDITABLE VARCHAR2) 
	IS 
	BEGIN
		INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(
			CONFIG_TEMPLATE_ID,
			DEVICE_SETTING_PARAMETER_CD,
			CONFIG_TEMPLATE_SETTING_VALUE,
			CATEGORY_ID,
			DISPLAY,
			DESCRIPTION,
			EDITOR,
			CONVERTER,
			CLIENT_SEND,
			ALIGN,
			FIELD_SIZE,
			FIELD_OFFSET,
			PAD_CHAR,
			PROPERTY_LIST_VERSION,
			DEVICE_TYPE_ID,
			DISPLAY_ORDER,
			ALT_NAME,
			DATA_MODE,
			DATA_MODE_AUX,
			REGEX_ID,
			ACTIVE,
			NAME,
			CUSTOMER_EDITABLE,
			STORED_IN_PENNIES) 
		SELECT 
			CONFIG_TEMPLATE_ID, 
			pv_PARAMETER_CD,
			pv_SETTING_VALUE,
			pn_CATEGORY_ID,
			pv_DISPLAY,
			pv_DESCRIPTION,
			pv_EDITOR,
			'',
			'Y',
			NULL,
			pn_FIELD_SIZE,
			NULL,
			NULL,
			0,
			DEVICE_TYPE_ID,
			pn_DISPLAY_ORDER,
			NULL,
			pv_DATA_MODE,
			pv_DATA_MODE_AUX,
			pn_REGEX_ID,
			'Y',
			pv_NAME,
			pv_CUSTOMER_EDITABLE,
			'N'
		FROM DEVICE.CONFIG_TEMPLATE CT 
		WHERE CONFIG_TEMPLATE_TYPE_ID = 3 
			AND DEVICE_TYPE_ID = 13 
			AND NOT EXISTS (
				SELECT 1 
				FROM DEVICE.CONFIG_TEMPLATE_SETTING 
				WHERE DEVICE_TYPE_ID = 13 
				AND DEVICE_SETTING_PARAMETER_CD = pv_PARAMETER_CD);
	END;
	
	PROCEDURE ADD_CTS(
		pn_CONFIG_TEMPLATE_ID NUMBER,
		pv_PARAMETER_CD VARCHAR2,
		pv_SETTING_VALUE VARCHAR2) 
	IS
	BEGIN
		INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(
			CONFIG_TEMPLATE_ID, 
			DEVICE_SETTING_PARAMETER_CD, 
			CONFIG_TEMPLATE_SETTING_VALUE) 
		VALUES(pn_CONFIG_TEMPLATE_ID, pv_PARAMETER_CD, pv_SETTING_VALUE);
	END;
	
BEGIN
	-- add new indexes to device_setting_parameter
	
	ADD_DSP('451', 'Y', 'COMS: Diversity Ant Port Enable');
	ADD_DSP('1027', 'Y', 'MDB Setting Cash Sales Speed');
	ADD_DSP('1028', 'Y', 'MDB Display State Reporting');
	ADD_DSP('2000', 'Y', 'Bezel Protocol');
	ADD_DSP('2001', 'Y', 'Bezel DUKPT Encryption Auto Enable');
	ADD_DSP('2002', 'Y', 'Bezel VAS Mode');
	
	-- add new category id 43
	
	INSERT INTO device.config_template_category(config_template_category_id, config_template_category_name, config_template_category_desc)
	SELECT 43, 'Bezel Control', 'Bezel Control'
	FROM DUAL
	WHERE NOT EXISTS (
		SELECT 1
		FROM device.config_template_category
		WHERE config_template_category_id = 43);
	
	INSERT INTO device.config_template_category_disp(config_template_category_id, device_type_id, display, category_display_order)
	SELECT 43, 13, 'N', 25
	FROM DUAL
	WHERE NOT EXISTS (
		SELECT 1
		FROM DEVICE.CONFIG_TEMPLATE_CATEGORY_DISP
		WHERE config_template_category_id = 43);
	
	-- move the display order of some old settings to make room in Comm Settings
	
	UPDATE device.config_template_setting
	SET display_order = display_order + 10
	WHERE device_setting_parameter_cd IN ('30', '31', '32', '33', '34', '35')
	and device_type_id = 13;
	
	-- move this category to the bottom
	
	UPDATE DEVICE.CONFIG_TEMPLATE_CATEGORY_DISP 
	SET CATEGORY_DISPLAY_ORDER = 1000 
	WHERE CONFIG_TEMPLATE_CATEGORY_ID = 1 
	AND DEVICE_TYPE_ID = 13;

	-- add new configurable parameters to config_template_setting meta template
	
	ADD_CTS_META('451', '1', 3, 'Y', 'Many units only have one RF port located on the left side of the unit. This property has no effect on units like these with one port. LTE and some other modems have an extra port on the right called the Diversity port. Set to 1 to enable {default}. Set to 0 to disable. For best performance of units with 2 ports, ensure two antennas are installed if this property is enabled.', 'SELECT:0=0 - Disabled;1=1 - Enabled', 5, 25, 'A', NULL, 3, 'COMS: Diversity Ant Port Enable', 'N');

	ADD_CTS_META('1027', '30', 6, 'Y', 'Sets the time limit in 100ms of how fast the ePort will allow MDB cash messages to be processed. If they arrive any faster than this time, the unit will treat the cash message as spamming and reset the unit in attempt to recover. Default 30 (3 seconds).', 'TEXT:1 to 5', 5, 32, 'A', 'N', 3, 'MDB Setting Cash Sales Speed', 'N');
	ADD_CTS_META('1028', '0', 6, 'Y', 'Sets the detail level for reporting UI state to the VMC. 0- No reporting (default). 1- Basic Cashless Flow reporting, >=2 TBD. Contact Engineering for supported firmware revisions.', 'TEXT:1 to 2', 2, 35, 'A', 'N', 3, 'MDB Display State Reporting', 'N');

	ADD_CTS_META('2000', '0', 43, 'Y', '0 - Auto, enable auto selection of bezel protocol (default). 1 - Force Gx Protocol, use Gx protocol even if protocol 2 is available.', 'SELECT:0=0 - Auto;1=1 - Force Gx Protocol', 5, 5, 'A', NULL, 3, 'Bezel Protocol', 'N');
	ADD_CTS_META('2001', '1', 43, 'Y', '0 - No, don''t enable encryption. 1 - Yes, enable encryption if bezel has supported FW and valid keys (default).', 'SELECT:0=0 - No;1=1 - Yes', 5, 10, 'A', NULL, 3, 'Bezel DUKPT Encryption Auto Enable', 'N');
	ADD_CTS_META('2002', '1', 43, 'Y', '0 - VAS or Payment, if there is a pass in the wallet return pass, else return card data. 1 - VAS and Payment, return both pass and card data (default). 2 - VAS Only, return only pass. 3 - VAS Off, disable VAS protocol.', 'SELECT:0=0 - VAS or Payment;1=1 - VAS and Payment;2=2 - VAS Only;3=3 - VAS Off', 5, 15, 'A', NULL, 3, 'Bezel VAS Mode', 'N');
	
	-- updates to existing config template settings of meta template
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET DESCRIPTION = 'Device activation status. Deprecated Property. Please do not change without engineering guidance.' 
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '70';
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET DESCRIPTION = 'Sets VMC or Kiosk Interface type. Most devices support interfaces 1-6. G9 Rev >=2.04.005 supports interfaces 7 and 8. If set to either 7 or 8, the G9 will disable MDB Alerts.',
	EDITOR = 'SELECT:1=1 - Standard MDB;2=2 - eTrans MDB;3=3 - Coin Pulse;4=4 - Coin Pulse Dual Enable;5=5 - Top-off Coin Pulse;6=6 - Serial Edge;7=7 - Standard MDB via AUX Serial Port;8=8 - Tire Gage MDB via AUX Serial Port\enableOptions(document.querySelector(''*[name*="_1107"]'').options, [null,[0,1,3],[0,1,3],[0,1],[0,1],[0,1],[2],[4],[4]][$(this).getValue()], parseInt)'
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '1500';
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET DESCRIPTION = 'Sets how the Aux Serial Port is used. Most G9 devices support 0-2. G9 rev >=2.04.005 supports 4.',
	EDITOR = 'SELECT:0=0 - DIAGNOSTICS (Default);1=1 - DEX;2=2 - SERIAL EDGE;3=3 - RESERVED;4=4 - MDB VIA AUX SERIAL\checkSupport($(this));'
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '1107';
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET DESCRIPTION = 'Authorization response timeout in seconds. Most units (revisions) will not support over 20. G9 rev >=2.04.005 will support a range of 5 of 120. Please consult engineering on the use of this property.'
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '10';

	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET DESCRIPTION = 'Batch response timeout in seconds. Note: this property may not function. Please do not change without engineering guidance.'
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '30';
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET NAME = 'MDB Vending Machine Out-of-Service Detection Time'
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '1025';
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING 
	SET EDITOR = 'SELECT:3=3 - Activated' 
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '70';
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET EDITOR = 'SCHEDULE:=None;E=Daily Time List;D=Daily;I=Interval;W=Weekly'
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '1101';

	-- check if config template exists yet
	
	SELECT MAX(config_template_id) INTO ln_config_template_id
	FROM device.config_template
	WHERE config_template_name = lv_config_template_name;
	
	IF ln_config_template_id IS NULL THEN
		-- create it if not
		INSERT INTO device.config_template(config_template_type_id, config_template_name, device_type_id, property_list_version)
		VALUES(ln_config_template_type_id, lv_config_template_name, ln_device_type_id, ln_property_list_version);
		
		SELECT MAX(config_template_id) INTO ln_config_template_id
		FROM device.config_template
		WHERE config_template_name = lv_config_template_name;
	END IF;
	
	-- remove existing settings for this template, if any
	
	DELETE FROM DEVICE.CONFIG_TEMPLATE_SETTING WHERE CONFIG_TEMPLATE_ID = ln_config_template_id;

	-- add the new parameters to the default template
	
	ADD_CTS(ln_config_template_id, '451', '1');
	ADD_CTS(ln_config_template_id, '1027', '30');
	ADD_CTS(ln_config_template_id, '1028', '0');
	ADD_CTS(ln_config_template_id, '2000', '0');
	ADD_CTS(ln_config_template_id, '2001', '1');
	ADD_CTS(ln_config_template_id, '2002', '1');
	
	-- copy all other settings from previous properties list
	
	INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(CONFIG_TEMPLATE_ID, DEVICE_SETTING_PARAMETER_CD, CONFIG_TEMPLATE_SETTING_VALUE) 
	SELECT ln_config_template_id, DEVICE_SETTING_PARAMETER_CD, CONFIG_TEMPLATE_SETTING_VALUE
	FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS
	JOIN DEVICE.CONFIG_TEMPLATE CT ON CTS.CONFIG_TEMPLATE_ID = CT.CONFIG_TEMPLATE_ID
	WHERE CONFIG_TEMPLATE_NAME = 'DEFAULT-CFG-' || ln_device_type_id || '-' || (ln_property_list_version - 1)
	AND DEVICE_SETTING_PARAMETER_CD NOT IN (
		SELECT DEVICE_SETTING_PARAMETER_CD 
		FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS 
		WHERE CONFIG_TEMPLATE_ID = ln_config_template_id);
		
	-- change any default values for this template

	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET CONFIG_TEMPLATE_SETTING_VALUE = 'apn01.usatech.com'
	WHERE CONFIG_TEMPLATE_ID = ln_config_template_id
	AND DEVICE_SETTING_PARAMETER_CD = '444';

	-- delete if exists and insert the updated aggregated properties list
	
	DELETE FROM DEVICE.DEVICE_TYPE_PROPERTY_LIST
	WHERE device_type_id = ln_device_type_id
		AND property_list_version = ln_property_list_version;

	INSERT INTO DEVICE.DEVICE_TYPE_PROPERTY_LIST(device_type_id, property_list_version, all_property_list_ids, configurable_property_list_ids)
	VALUES(ln_device_type_id, ln_property_list_version, 
		'10|30-35|50-52|60-64|70|80|81|85-87|100-108|200-208|210-213|215|300-302|310-315|400|401|405|406|410|411|415|416|420|421|425|426|430|431|435|436|440-442|444|446|450-451|1001-1028|1101|1103|1104|1107|1110-1114|1200-1207|1300-1303|1400-1415|1500-1528|1600-1603|2000-2002',
		'10|30-35|70|85-87|215|400|401|405|406|410|411|415|416|420|421|425|426|430|431|435|436|440-442|444|446|450-451|1001-1028|1101|1103|1104|1107|1110-1114|1200-1207|1300-1303|1400-1415|1500-1528|1600-1603|2000-2002');


	COMMIT;
END;
/
