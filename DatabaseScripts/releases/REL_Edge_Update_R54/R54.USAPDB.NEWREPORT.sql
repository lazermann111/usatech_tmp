GRANT SELECT ON PSS.CONSUMER_ACCT_BASE to REPORT;

CREATE OR REPLACE FUNCTION REPORT.GET_APPLY_TO_CARD_ID (
    l_tran_id REPORT.TRANS.TRAN_ID%TYPE)
RETURN NUMBER IS
    l_card_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE;
    
BEGIN
    select max(GLOBAL_ACCOUNT_ID) into l_card_id
    FROM PSS.CONSUMER_ACCT_BASE cab join REPORT.TRANS t on cab.CONSUMER_ACCT_ID=t.APPLY_TO_CONSUMER_ACCT_ID
    and t.tran_id=l_tran_id;
    RETURN l_card_id;
END;
/

GRANT EXECUTE ON REPORT.GET_APPLY_TO_CARD_ID to USAT_DEV_READ_ONLY;
GRANT EXECUTE ON REPORT.GET_APPLY_TO_CARD_ID to USALIVE_APP_ROLE;

INSERT INTO FOLIO_CONF.FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
 VALUES(9711, 'Apply To Card Id','','REPORT.GET_APPLY_TO_CARD_ID(REPORT.ACTIVITY_REF.TRAN_ID)','REPORT.GET_APPLY_TO_CARD_ID(REPORT.ACTIVITY_REF.TRAN_ID)','','NUMERIC','NUMERIC',50,11,'Y');

INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 1,9711,NULL);                                                                                                                  
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 2,9711,NULL);                                                                                                                  
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 3,9711,NULL);                                                                                                                  
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 4,9711,NULL);                                                                                                                  
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 5,9711,NULL);                                                                                                                  
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 8,9711,NULL); 
 
 COMMIT;
 
CREATE OR REPLACE PROCEDURE REPORT.USAT_CLEAN_TRANSPORT_RETRY
IS
BEGIN
  update report.ccs_transport_error set REATTEMPTED_FLAG='D' 
  where ERROR_TS<sysdate-35 and REATTEMPTED_FLAG='N';
  COMMIT;

END;
/

GRANT EXECUTE ON REPORT.USAT_CLEAN_TRANSPORT_RETRY to USAT_DEV_READ_ONLY;

DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := -1;
	LV_REPORT_NAME VARCHAR(50) := 'Transaction Exceptions';
	l_user_id NUMBER;
BEGIN
	
	SELECT MAX(REPORT_ID) INTO LN_REPORT_ID
	FROM REPORT.REPORTS WHERE REPORT_NAME = LV_REPORT_NAME;

	IF LN_REPORT_ID > 0 THEN
		RETURN;
	ELSE
		LN_REPORT_ID:=471;
	END IF;
	
	select user_id into l_user_id from report.user_login ul cross join V$DATABASE d 
where user_name=(CASE WHEN d.name like 'USADEV%' THEN 'The Acropolis'
WHEN d.name like 'ECC%' THEN 'BlackWiddow'
ELSE 'compassgroup' END);
		
	INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
	SELECT LN_REPORT_ID, 'Transaction Exceptions {d}', 6, 0, 'Transaction Exceptions', 'Report that shows declined and canceled transactions.', 'U', l_user_id
	FROM DUAL;

	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
	VALUES(LN_REPORT_ID, 'query', 'SELECT 
    r.REGION_NAME "Region",
    l.LOCATION_NAME "Location",
    e.EPORT_SERIAL_NUM "Device",	
    t.ASSET_NBR "Asset #",
    x.TRAN_END_TS "Tran Date", 
    x.CARD_TYPE "Card Type", 
    x.TRAN_RECEIVED_RAW_ACCT_DATA "Card Number",
    cab.GLOBAL_ACCOUNT_ID "Card Id",
    a.auth_amt "Auth Amount",
    case when A.AUTH_STATE_ID in (3,7) then ''Declined'' else ''Canceled'' end "Tran State",
    x.tran_device_tran_cd "Device Tran Id"
FROM PSS.TRAN x left outer join PSS.CONSUMER_ACCT_BASE cab on X.CONSUMER_ACCT_ID=CAB.CONSUMER_ACCT_ID
    JOIN PSS.POS_PTA pp on pp.POS_PTA_ID=x.POS_PTA_ID
    JOIN PSS.POS p on p.POS_ID=pp.POS_ID
    JOIN pss.auth a ON x.tran_id = a.tran_id AND a.auth_type_cd = ''N''
    JOIN DEVICE.DEVICE d on d.device_id=p.device_id
    JOIN REPORT.EPORT e on d.device_serial_cd=e.eport_serial_num 
    JOIN REPORT.TERMINAL_EPORT te on te.eport_id=e.eport_id and SYSDATE BETWEEN NVL(TE.START_DATE, SYSDATE) AND NVL(TE.END_DATE - (1/86400), SYSDATE)
    JOIN REPORT.TERMINAL t on te.terminal_id=t.terminal_id
    JOIN REPORT.VW_USER_TERMINAL ut ON t.TERMINAL_ID = ut.TERMINAL_ID AND ut.TERMINAL_ID != 0
    left outer join REPORT.LOCATION l ON t.LOCATION_ID = l.LOCATION_ID
    left outer join REPORT.TERMINAL_REGION tr on t.terminal_id=tr.terminal_id
    LEFT OUTER JOIN REPORT.REGION r ON tr.REGION_ID = r.REGION_ID
WHERE ut.USER_ID = ?
and x.tran_start_ts >= TRUNC(sysdate-1, ''DD'') AND x.tran_start_ts < TRUNC(sysdate, ''DD'')
and x.device_name=d.device_name
and x.tran_state_cd in (''C'',''E'')
and a.auth_state_id in (2,3,5,7)
and x.payment_subtype_class not in (''Cash'',''Promotion'',''Coupon'')
ORDER BY 1, 2, 3');
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'header', 'true');
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) values(LN_REPORT_ID, 'dateFormat', 'MM/dd/yyyy');
	INSERT INTO Report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) values(LN_REPORT_ID, 'paramNames', 'userId');
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) values(LN_REPORT_ID, 'paramTypes', 'NUMERIC');
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'params.userId', '{u}');
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'isSQLFolio', 'true');
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'outputType', '21');
	
    COMMIT;
END;

