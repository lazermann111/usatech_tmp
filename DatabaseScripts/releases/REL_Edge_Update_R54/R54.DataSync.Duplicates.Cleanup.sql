DECLARE 
  ln_min_id NUMBER;
  ln_max_id NUMBER;
  ln_row_count NUMBER;
  ln_last_id NUMBER;
  ln_dup_max_id NUMBER;
  ln_delete_count NUMBER;
  ln_device_count NUMBER := 0;
  ln_device_delete_total NUMBER := 0;
  ln_pt_delete_total NUMBER := 0;
  CURSOR lc_item_cd (ln_ds_id NUMBER) IS
    SELECT distinct ds.item_cd
    FROM engine.data_sync_1 ds
    WHERE ds.data_sync_id BETWEEN ln_ds_id AND (ln_ds_id + 10000)
    AND data_sync_type_cd IN ('DEVICE_INFO', 'DEVICE_PTAS', 'MASTER_ID')
    GROUP BY data_sync_type_cd, item_cd
    HAVING count(1) > 1;
  CURSOR lc_dups (lv_item_cd VARCHAR2) IS
    SELECT data_sync_type_cd, item_cd, count(1) c
    FROM engine.data_sync_1
    WHERE item_cd = lv_item_cd
    AND data_sync_type_cd IN ('DEVICE_INFO', 'DEVICE_PTAS', 'MASTER_ID')
    GROUP BY data_sync_type_cd, item_cd
    HAVING count(1) > 1;
BEGIN
  --dbms_output.put_line('--- doing data_sync_1');
  SELECT MIN(data_sync_id), MAX(data_sync_id)
  into ln_min_id, ln_max_id
  FROM engine.data_sync_1 ds;
  ln_last_id := ln_min_id;
  --dbms_output.put_line('MIN = ' || ln_min_id || ', MAX = ' || ln_max_id);
  -- loop through the data sync items in increments
  while ln_last_id < ln_max_id loop
    --dbms_output.put_line('device count ' || ln_device_count || ' last_id = ' || ln_last_id);
    -- get a list of distinct item_cds within the current range (item_cd is device name and is indexed)
    FOR lr_item_cd IN lc_item_cd(ln_last_id) loop
      --dbms_output.put_line('item_cd = ' || lr_item_cd.item_cd);
      -- now find rows for that item_cd which have more than 1 copy (group by the 6 data columns)
      FOR lr_dups IN lc_dups(lr_item_cd.item_cd) loop
        -- for each type of duplicate, get the id of the data sync item
        SELECT MAX(ds.data_sync_id)
        INTO ln_dup_max_id
        FROM engine.data_sync_1 ds
        WHERE ds.data_sync_type_cd = lr_dups.data_sync_type_cd
        AND ds.item_cd = lr_dups.item_cd;
        --dbms_output.put_line('max data_sync_id is ' || ln_dup_max_id || ' of ' || lr_dups.c || ' records for data_sync_type_cd = ' || lr_dups.data_sync_type_cd || ', item_cd = ' || lr_dups.item_cd);
        -- delete all the rows where the 2 columns match and the data_sync_id is less than the max, that should leave only 1
        --SELECT /*+ INDEX (ds IDX_data_sync_1_ITEM_CD) */ count(1) INTO ln_delete_count
        DELETE /*+ INDEX (ds IDX_data_sync_1_ITEM_CD) */
        FROM engine.data_sync_1 ds
        WHERE ds.data_sync_type_cd = lr_dups.data_sync_type_cd
        AND ds.item_cd = lr_dups.item_cd
        AND ds.data_sync_id < ln_dup_max_id;
        --dbms_output.put_line('deleted ' || ln_delete_count || ' records for data_sync_type_cd = ' || lr_dups.data_sync_type_cd || ', item_cd = ' || lr_dups.item_cd);
        --ln_device_delete_total := ln_device_delete_total + ln_delete_count;
        ln_device_delete_total := ln_device_delete_total + SQL%rowcount;
        --dbms_output.put_line('deleted ' || SQL%rowcount || ' records for data_sync_type_cd = ' || lr_dups.data_sync_type_cd || ', item_cd = ' || lr_dups.item_cd);
        COMMIT;
      END loop; -- for lr_dups
      ln_device_count := ln_device_count + 1;
      -- this is to stop it early for testing
      --IF (ln_device_count > 150) THEN
      --  RETURN;
      --END IF;
    END loop; -- for lr_item_cd
    -- also delete INTERNAL_PAYMENT_TYPE records, they all point to deleted records
    DELETE
    --SELECT count(1) INTO ln_delete_count
    FROM engine.data_sync_1 ds
    WHERE ds.data_sync_type_cd = 'INTERNAL_PAYMENT_TYPE'
    AND ds.data_sync_id BETWEEN ln_last_id AND (ln_last_id + 10000);
    --dbms_output.put_line('deleted ' || SQL%rowcount || ' INTERNAL_PAYMENT_TYPE rows');
    --dbms_output.put_line('deleted ' || ln_delete_count || ' INTERNAL_PAYMENT_TYPE rows');
    ln_pt_delete_total := ln_pt_delete_total + SQL%rowcount;
    COMMIT;
    ln_last_id := (ln_last_id + 10000);
  END loop; -- while ln_last_id
  dbms_output.put_line('deleted ' || ln_device_delete_total || ' device records and ' || ln_pt_delete_total || ' payment type records');
END;
