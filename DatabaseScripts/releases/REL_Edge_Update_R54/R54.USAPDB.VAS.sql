-- VAS
insert into pss.payment_entry_method (payment_entry_method_cd, payment_entry_method_desc, display_order)
values('V','VAS',700);

insert into PSS.CLIENT_PAYMENT_TYPE (CLIENT_PAYMENT_TYPE_CD, CLIENT_PAYMENT_TYPE_DESC, payment_type_cd, payment_entry_method_cd, payment_action_type_cd)
select 'O', 'VAS', 'C', 'V', 'C' from dual
where not exists (select 1 from PSS.CLIENT_PAYMENT_TYPE WHERE CLIENT_PAYMENT_TYPE_CD = 'O');

INSERT INTO PSS.PAYMENT_SUBTYPE(PAYMENT_SUBTYPE_NAME, PAYMENT_SUBTYPE_CLASS, PAYMENT_SUBTYPE_KEY_NAME, CLIENT_PAYMENT_TYPE_CD, PAYMENT_SUBTYPE_TABLE_NAME, PAYMENT_SUBTYPE_KEY_DESC_NAME, AUTHORITY_PAYMENT_MASK_ID, TRANS_TYPE_ID, CARD_TYPE_LABEL)
SELECT 'USAT ISO Card - Prepaid Card (VAS)', PAYMENT_SUBTYPE_CLASS, PAYMENT_SUBTYPE_KEY_NAME, 'O', PAYMENT_SUBTYPE_TABLE_NAME, PAYMENT_SUBTYPE_KEY_DESC_NAME, AUTHORITY_PAYMENT_MASK_ID, TRANS_TYPE_ID, CARD_TYPE_LABEL
FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'USAT ISO Card - Prepaid Card (Tokenized)' AND NOT EXISTS (SELECT 1 FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'USAT ISO Card - Prepaid Card (VAS)');
commit;

alter table pss.consumer_acct
add pass_serial_number VARCHAR2(200);


INSERT INTO PSS.POS_PTA_TMPL(POS_PTA_TMPL_NAME, POS_PTA_TMPL_DESC)
SELECT 'SPECIAL MERGE: Prepaid VAS - USA', 'Prepaid VAS payment template' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'SPECIAL MERGE: Prepaid VAS - USA');

INSERT INTO PSS.POS_PTA_TMPL_ENTRY(POS_PTA_TMPL_ID, PAYMENT_SUBTYPE_ID, POS_PTA_ACTIVATION_OSET_HR, PAYMENT_SUBTYPE_KEY_ID, POS_PTA_PRIORITY, CURRENCY_CD, POS_PTA_PASSTHRU_ALLOW_YN_FLAG)
SELECT (SELECT POS_PTA_TMPL_ID FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'SPECIAL MERGE: Prepaid VAS - USA'),
	(SELECT PAYMENT_SUBTYPE_ID FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'USAT ISO Card - Prepaid Card (VAS)'),
	0, (select internal_payment_type_id FROM PSS.internal_payment_type where internal_payment_type_desc='Special Card (prepaid)'),
	1, 'USD', 'N' 
  FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM PSS.POS_PTA_TMPL_ENTRY 
	WHERE POS_PTA_TMPL_ID = (SELECT POS_PTA_TMPL_ID FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'SPECIAL MERGE: Prepaid VAS - USA')
		AND PAYMENT_SUBTYPE_ID = (SELECT PAYMENT_SUBTYPE_ID FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'USAT ISO Card - Prepaid Card (VAS)')
);

COMMIT;