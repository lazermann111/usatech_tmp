declare 
CURSOR l_cur IS
select 
    pta.POS_PTA_ID,
    pta.PAYMENT_SUBTYPE_KEY_ID,
    regexp_replace(ipt.INTERNAL_PAYMENT_TYPE_DESC,'[^[[:digit:]]]*') as original_ter_id,
    ipt.INTERNAL_PAYMENT_TYPE_DESC
from 
  pss.pos_pta pta 
  JOIN PSS.PAYMENT_SUBTYPE PS ON pta.PAYMENT_SUBTYPE_ID = PS.PAYMENT_SUBTYPE_ID
  join  PSS.INTERNAL_PAYMENT_TYPE ipt on ipt.INTERNAL_PAYMENT_TYPE_ID = pta.PAYMENT_SUBTYPE_KEY_ID
where 
  ipt.VIRTUAL_ITEM  = 1
  and ipt.INTERNAL_PAYMENT_TYPE_DESC like '%(VIRTUAL)%'
  AND PS.PAYMENT_SUBTYPE_CLASS LIKE 'Internal%';
begin
  -- it's needed only for backup 
  -- execute IMMEDIATE 'DROP TABLE PSS.ROLLBACK_IPT_TMP';
  execute IMMEDIATE 'CREATE TABLE PSS.ROLLBACK_IPT_TMP(pos_pta_id NUMBER, old_terminal_id NUMBER, new_terminal_id NUMBER, create_ts DATE)';
  for l_rec in l_cur loop
    update PSS.POS_PTA set PAYMENT_SUBTYPE_KEY_ID = l_rec.original_ter_id where POS_PTA_ID = l_rec.POS_PTA_ID;
    -- persists data for backup
    if sql%rowcount > 0 then
      insert into pss.ROLLBACK_IPT_TMP values (l_rec.POS_PTA_ID, 
        l_rec.PAYMENT_SUBTYPE_KEY_ID, l_rec.original_ter_id, SYSDATE);
    end if;
	commit;
  end loop;
end;