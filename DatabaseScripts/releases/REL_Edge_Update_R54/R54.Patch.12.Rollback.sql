UPDATE REPORT.REPORTS SET USAGE = 'N' WHERE REPORT_NAME LIKE 'UNI %';
COMMIT;

UPDATE REPORT.REPORT_PARAM SET PARAM_VALUE = 'SELECT DISTINCT LOCATION_NAME, TERMINAL_NBR
FROM REPORT.VW_ZERO_TRANS ZT, REPORT.VW_USER_TERMINAL UT, REPORT.VW_TERMINAL_EPORT TE
WHERE ZT.TERMINAL_ID = UT.TERMINAL_ID AND UT.USER_ID = {u}
AND TE.TERMINAL_ID = UT.TERMINAL_ID' WHERE REPORT_ID = 11 AND PARAM_NAME = 'query';

UPDATE REPORT.REPORT_PARAM SET PARAM_VALUE = 'SELECT EFT_ID, REF_NBR FROM REPORT.VW_PAYMENT_TRANS WHERE
EFT_ID = {x}' WHERE REPORT_ID = 13 AND PARAM_NAME = 'query';

UPDATE REPORT.REPORT_PARAM SET PARAM_VALUE = 'SELECT DISTINCT LOCATION_NAME "Location", TERMINAL_NBR "Terminal", E.EPORT_SERIAL_NUM "Device"
  FROM REPORT.VW_ZERO_TRANS ZT
  JOIN REPORT.VW_USER_TERMINAL UT ON ZT.TERMINAL_ID = UT.TERMINAL_ID
  JOIN REPORT.VW_TERMINAL_EPORT TE ON UT.TERMINAL_ID = TE.TERMINAL_ID
  JOIN REPORT.EPORT E ON TE.EPORT_ID = E.EPORT_ID
  WHERE USER_ID = {u}' WHERE REPORT_ID = 157 AND PARAM_NAME = 'query';
  
UPDATE REPORT.REPORT_PARAM SET PARAM_VALUE = 'SELECT EPORT_SERIAL_NUM, REF_NBR, TRANS_TYPE_CODE, CARD_NUMBER, TOTAL_AMOUNT, VEND_COLUMN, QUANTITY, TRAN_DATE, TRAN_TIME 
FROM REPORT.VW_TRAN_DETAIL X, REPORT.VW_USER_TERMINAL UT 
WHERE X.BATCH_ID = {x}
AND X.TERMINAL_ID = UT.TERMINAL_ID
AND UT.USER_ID = {u}' WHERE REPORT_ID = 49 AND PARAM_NAME = 'query';

UPDATE REPORT.REPORT_PARAM SET PARAM_VALUE = 'SELECT   c.customer_name,
         cb.bank_acct_nbr,
         cb.bank_routing_nbr,
         cb.account_title,
         f.fee_name,
         cu.currency_name,
         SUM (l.amount) amount,
         COUNT (*) "count",
         CASE WHEN d.STATUS IN(''O'', ''D'') THEN ''N'' ELSE ''Y'' END in_eft,
         bu.business_unit_name
    FROM corp.ledger l,
         corp.service_fees sf,
         corp.batch b,
         corp.customer_bank cb,
         corp.customer c,
         corp.fees f,
         corp.doc d,
         corp.business_unit bu,
        corp.currency cu
   WHERE l.entry_type = ''SF''
     AND TRUNC (l.entry_date, ''MONTH'') = TRUNC(TO_DATE(''{b}'', ''MM/DD/YYYY''), ''MONTH'')
     AND l.deleted = ''N''
     AND l.service_fee_id = sf.service_fee_id
     AND l.batch_id = b.batch_id
     AND d.customer_bank_id = cb.customer_bank_id
     AND cb.customer_id = c.customer_id
     AND sf.fee_id = f.fee_id
     AND b.doc_id = d.doc_id
     AND d.BUSINESS_UNIT_ID = bu.BUSINESS_UNIT_ID
    AND d.currency_id = cu.currency_id
GROUP BY bu.business_unit_name,
         c.customer_name,
         cb.bank_acct_nbr,
         cb.bank_routing_nbr,
         cb.account_title,
         f.fee_name,
         cu.currency_name,
         CASE WHEN d.STATUS IN(''O'', ''D'') THEN ''N'' ELSE ''Y'' END         
ORDER BY bu.business_unit_name,
         c.customer_name,
         cb.bank_acct_nbr,
         cb.bank_routing_nbr,
         cb.account_title,
         f.fee_name,
         cu.currency_name,
         CASE WHEN d.STATUS IN(''O'', ''D'') THEN ''N'' ELSE ''Y'' END
' WHERE REPORT_ID = 54 AND PARAM_NAME = 'query';

UPDATE REPORT.REPORT_PARAM SET PARAM_VALUE = 'SELECT X.EPORT_SERIAL_NUM "Device Serial Number", 
X.REF_NBR "USAT Reference Number", 
X.TRANS_TYPE_CODE "Tran Type Code", 
X.CARD_NUMBER "Card Number", 
X.TOTAL_AMOUNT "Total Amount", 
X.VEND_COLUMN "Column", 
X.QUANTITY "Quantity",
X.TRAN_DATE "Tran Date", 
X.TRAN_TIME "Tran Time",
SUBSTR(T.MACHINE_TRANS_NO, INSTR(T.MACHINE_TRANS_NO, '':'', 1, 2) + 1, 1000) "Device Tran Cd"
FROM REPORT.VW_TRAN_DETAIL X, REPORT.VW_USER_TERMINAL UT, REPORT.TRANS T 
WHERE X.BATCH_ID = {x}
AND X.TERMINAL_ID = UT.TERMINAL_ID
AND X.REAL_TRAN_ID = T.TRAN_ID
AND UT.USER_ID = {u}' WHERE REPORT_ID = 69 AND PARAM_NAME = 'query';
 
UPDATE REPORT.REPORT_PARAM SET PARAM_VALUE = 'SELECT E.EPORT_SERIAL_NUM, AR.LOCATION_NAME, AR.REF_NBR, AR.TRAN_DATE, AR.TRANS_TYPE_NAME, AR.CARD_NUMBER,
pt.PAYMENT_APPR_CODE, AR.TOTAL_AMOUNT, AR.VEND_COLUMN, AR.QUANTITY, CAB.GLOBAL_ACCOUNT_ID CARD_ID
FROM REPORT.ACTIVITY_REF AR
JOIN report.vw_payment_trans pt ON AR.tran_id = pt.tran_id
JOIN REPORT.EPORT E ON AR.EPORT_ID = E.EPORT_ID
LEFT OUTER JOIN PSS.CONSUMER_ACCT_BASE CAB ON AR.CONSUMER_ACCT_ID = CAB.CONSUMER_ACCT_ID
WHERE pt.eft_id = {x}' WHERE REPORT_ID = 162 AND PARAM_NAME = 'query';

UPDATE REPORT.REPORT_PARAM SET PARAM_VALUE = '	select TERMINAL_NBR, LOCATION_NAME, A.REF_NBR, TRAN_DATE, TRANS_TYPE_NAME, CARD_NUMBER,  pt.PAYMENT_APPR_CODE, TOTAL_AMOUNT,  VEND_COLUMN, QUANTITY from report.vw_activity a, report.vw_payment_trans pt where a.tran_id = pt.tran_id and eft_id = {x}	' WHERE REPORT_ID = 50 AND PARAM_NAME = 'query';

UPDATE REPORT.REPORT_PARAM SET PARAM_VALUE = 'SELECT * FROM CORP.VW_EFT_EXPORT WHERE
EFT_ID = {x}' WHERE REPORT_ID = 12 AND PARAM_NAME = 'query';

UPDATE REPORT.REPORT_PARAM SET PARAM_VALUE = '	select TERMINAL_NBR, LOCATION_NAME, A.TRAN_ID, TRAN_DATE, TRANS_TYPE_NAME, CARD_NUMBER,  pt.PAYMENT_APPR_CODE, TOTAL_AMOUNT,  VEND_COLUMN, QUANTITY from report.vw_activity a, report.vw_payment_trans pt where a.tran_id = pt.tran_id and eft_id = {x}	' WHERE REPORT_ID = 5 AND PARAM_NAME = 'query';

UPDATE REPORT.REPORT_PARAM SET PARAM_VALUE = '	SELECT cus.customer_name, cbk.bank_acct_nbr, v.doc_id eft_id, v.sent_date eft_date, v.total_amount, v.tran_month, SUM(CASE WHEN v.tid = 3 THEN v.cr_a ELSE NULL END) credit_settled, SUM(CASE WHEN v.tid = 3 THEN v.rf_a ELSE NULL END) refund_settled, SUM(CASE WHEN v.tid = 3 THEN v.cb_a ELSE NULL END) chargeback_settled, SUM(CASE WHEN v.tid = 3 THEN v.pf_a ELSE NULL END) process_fee_settled, SUM(CASE WHEN v.tid = 3 THEN v.sf_a ELSE NULL END) service_fee_settled, SUM(CASE WHEN v.tid = 3 THEN v.ad_a ELSE NULL END) adjust_settled, SUM(CASE WHEN v.tid = 3 THEN v.rd_a ELSE NULL END) rounding_settled, SUM(CASE WHEN v.tid = 3 THEN v.nrf_a ELSE NULL END) net_revenue_fee_settled, SUM(CASE WHEN v.tid = 3 THEN v.net_a ELSE NULL END) net_settled, SUM(CASE WHEN v.tid = 4 THEN v.cr_a ELSE NULL END) credit_pending, SUM(CASE WHEN v.tid = 4 THEN v.rf_a ELSE NULL END) refund_pending, SUM(CASE WHEN v.tid = 4 THEN v.cb_a ELSE NULL END) chargeback_pending, SUM(CASE WHEN v.tid = 4 THEN v.pf_a ELSE NULL END) process_fee_pending, SUM(CASE WHEN v.tid = 4 THEN v.sf_a ELSE NULL END) service_fee_pending, SUM(CASE WHEN v.tid = 4 THEN v.ad_a ELSE NULL END) adjust_pending, SUM(CASE WHEN v.tid = 4 THEN v.rd_a ELSE NULL END) rounding_pending, SUM(CASE WHEN v.tid = 4 THEN v.nrf_a ELSE NULL END) net_revenue_fee_pending, SUM(CASE WHEN v.tid = 4 THEN v.net_a ELSE NULL END) net_pending, SUM(CASE WHEN v.tid = 5 THEN v.cr_a ELSE NULL END) credit_failed, SUM(CASE WHEN v.tid = 5 THEN v.rf_a ELSE NULL END) refund_failed, SUM(CASE WHEN v.tid = 5 THEN v.cb_a ELSE NULL END) chargeback_failed, SUM(CASE WHEN v.tid = 5 THEN v.pf_a ELSE NULL END) process_fee_failed, SUM(CASE WHEN v.tid = 5 THEN v.sf_a ELSE NULL END) service_fee_failed, SUM(CASE WHEN v.tid = 5 THEN v.ad_a ELSE NULL END) adjust_failed, SUM(CASE WHEN v.tid = 5 THEN v.rd_a ELSE NULL END) rounding_failed, SUM(CASE WHEN v.tid = 5 THEN v.nrf_a ELSE NULL END) net_revenue_fee_failed, SUM(CASE WHEN v.tid = 5 THEN v.net_a ELSE NULL END) net_failed FROM corp.customer_bank cbk, corp.customer cus, ( SELECT doc_id, sent_date, total_amount, customer_bank_id, tran_month, (CASE WHEN e.tid = 1 THEN 4 WHEN e.tid = 2 THEN 3 WHEN e.tid = 6 THEN 3 ELSE e.tid END) tid, SUM(CASE WHEN e.en_type = ''CC'' THEN e.amt ELSE NULL END) cr_a, SUM(CASE WHEN e.en_type = ''RF'' THEN e.amt ELSE NULL END) rf_a, SUM(CASE WHEN e.en_type = ''CB'' THEN e.amt ELSE NULL END) cb_a, SUM(CASE WHEN e.en_type = ''PF'' THEN e.amt ELSE NULL END) pf_a, SUM(CASE WHEN e.en_type = ''SF'' THEN e.amt ELSE NULL END) sf_a, SUM(CASE WHEN e.en_type = ''AD'' THEN e.amt ELSE NULL END) ad_a, SUM(CASE WHEN e.en_type = ''SB'' THEN e.amt ELSE NULL END) rd_a, SUM(CASE WHEN e.en_type = ''NR'' THEN e.amt ELSE NULL END) nrf_a, SUM(e.amt) net_a FROM ( SELECT d.doc_id, d.sent_date, d.total_amount, d.customer_bank_id, TRUNC(l.entry_date, ''MONTH'') tran_month, l.entry_type en_type, (CASE WHEN NVL(t.settle_date, MAX_DATE) >= TO_DATE(''{e}'',''MM/DD/YYYY HH24:MI:SS'') THEN 1 ELSE l.settle_state_id END) tid, SUM(l.amount) amt FROM corp.ledger l, corp.batch b, report.trans t, corp.doc d WHERE l.batch_id = b.batch_id AND b.doc_id = d.doc_id AND l.deleted = ''N'' AND d.sent_date BETWEEN TO_DATE(''{b}'',''MM/DD/YYYY HH24:MI:SS'') AND TO_DATE(''{e}'',''MM/DD/YYYY HH24:MI:SS'') AND l.trans_id = t.tran_id (+) GROUP BY d.doc_id, d.sent_date, d.total_amount, d.customer_bank_id, TRUNC(l.entry_date, ''MONTH''), l.entry_type, corp.payments_pkg.entry_payable(l.settle_state_id, l.entry_type), (CASE WHEN NVL(t.settle_date, MAX_DATE) >= TO_DATE(''{e}'',''MM/DD/YYYY HH24:MI:SS'') THEN 1 ELSE l.settle_state_id END) ) e GROUP BY doc_id, sent_date, total_amount, customer_bank_id, tran_month, e.tid ) v WHERE cbk.customer_id = cus.customer_id AND v.customer_bank_id = cbk.customer_bank_id GROUP BY cus.customer_name, cbk.bank_acct_nbr, v.sent_date, v.doc_id, v.total_amount, v.tran_month ORDER BY UPPER(cus.customer_name), cbk.bank_acct_nbr, v.sent_date, v.doc_id, v.tran_month	' WHERE REPORT_ID = 57 AND PARAM_NAME = 'query';

UPDATE REPORT.REPORT_PARAM SET PARAM_VALUE = 'SELECT bu.BUSINESS_UNIT_NAME "Business Unit", c.CUSTOMER_NAME "Customer",
      T.TERMINAL_NBR "Terminal Nbr", l.LOCATION_NAME "Location", e.EPORT_SERIAL_NUM "Device",
      SUM(DECODE(x.SETTLE_STATE_ID, 2, 1, 3, 1, NULL)) "# of Settled Trans", SUM(DECODE(x.SETTLE_STATE_ID, 2, x.total_amount, 3, x.total_amount, NULL)) "Total Settled Amount",
    SUM(DECODE(x.SETTLE_STATE_ID, 1, 1, 4, 1, NULL)) "# of Pending Trans", SUM(DECODE(x.SETTLE_STATE_ID, 1, x.total_amount, 4, x.total_amount, NULL)) "Total Pending Amount",
     SUM(DECODE(x.SETTLE_STATE_ID, 5, 1, 6, 1, NULL)) "# of Failed Trans", SUM(DECODE(x.SETTLE_STATE_ID, 5, x.total_amount, 6, x.total_amount, NULL)) "Total Failed Amount",
MIN(x.CLOSE_DATE) "Oldest Tran Date", MAX(x.CLOSE_DATE) "Newest Tran Date"
 FROM report.trans x, report.eport e, report.terminal t, corp.customer c, report.location l, corp.business_unit bu, report.trans_type tt
WHERE c.customer_id = t.customer_id
 AND t.terminal_id = x.terminal_id
 AND e.eport_id = x.eport_id
 AND t.LOCATION_ID = l.LOCATION_ID
 AND t.BUSINESS_UNIT_ID = bu.BUSINESS_UNIT_ID
 AND x.trans_type_id = tt.trans_type_id
 AND x.server_date >= TO_DATE(''{b}'',''MM/DD/YYYY HH24:MI:SS'')
 AND x.server_date <= TO_DATE(''{e}'',''MM/DD/YYYY HH24:MI:SS'')
 AND tt.payable_ind = ''Y''
 AND x.CUSTOMER_BANK_ID IS NULL
GROUP BY bu.BUSINESS_UNIT_NAME, c.CUSTOMER_NAME, T.TERMINAL_NBR, l.LOCATION_NAME, e.EPORT_SERIAL_NUM' WHERE REPORT_ID = 111 AND PARAM_NAME = 'query';

UPDATE REPORT.REPORT_PARAM SET PARAM_VALUE = '	SELECT EFT_ID, TRAN_ID FROM REPORT.VW_PAYMENT_TRANS WHERE EFT_ID = {x}	' WHERE REPORT_ID = 47 AND PARAM_NAME = 'query';

UPDATE REPORT.REPORT_PARAM SET PARAM_VALUE = 'SELECT X.EPORT_SERIAL_NUM "Device Serial Number", 
X.REF_NBR "USAT Reference Number", 
DECODE(T.TRANS_TYPE_ID, 17, ''A'', X.TRANS_TYPE_CODE) "Tran Type Code", 
X.CARD_NUMBER "Card Number", 
X.TOTAL_AMOUNT "Total Amount", 
X.VEND_COLUMN "Column", 
X.QUANTITY "Quantity",
X.TRAN_DATE "Tran Date", 
X.TRAN_TIME "Tran Time",
REPORT.GET_RECEIPT_TRAN_ID(T.MACHINE_TRANS_NO) "Receipt Tran Id"
FROM REPORT.VW_TRAN_DETAIL X, REPORT.VW_USER_TERMINAL UT, REPORT.TRANS T 
WHERE X.BATCH_ID = {x}
AND X.TERMINAL_ID = UT.TERMINAL_ID
AND X.REAL_TRAN_ID = T.TRAN_ID
AND UT.USER_ID = {u}' WHERE REPORT_ID = 68 AND PARAM_NAME = 'query';

UPDATE REPORT.REPORT_PARAM SET PARAM_VALUE = 'SELECT X.EPORT_SERIAL_NUM, X.TRAN_ID, X.TRANS_TYPE_CODE, X.CARD_NUMBER, X.TOTAL_AMOUNT, X.VEND_COLUMN, X.QUANTITY, X.TRAN_DATE, X.TRAN_TIME 
FROM REPORT.VW_TRAN_DETAIL X,  REPORT.VW_USER_TERMINAL UT
WHERE X.BATCH_ID = {x}
AND X.TERMINAL_ID = UT.TERMINAL_ID
AND UT.USER_ID = {u}' WHERE REPORT_ID = 10 AND PARAM_NAME = 'query';

DELETE FROM REPORT.REPORT_PARAM
WHERE REPORT_ID IN (
10,
11,
111,
12,
13,
157,
162,
47,
49,
5,
50,
54,
57,
68,
69
)
AND PARAM_NAME LIKE 'param%';

COMMIT;
