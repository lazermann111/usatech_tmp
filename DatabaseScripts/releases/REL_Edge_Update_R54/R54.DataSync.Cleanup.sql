declare 
  min_id number;
  max_id NUMBER;
  row_count NUMBER;
  last_id NUMBER;
BEGIN
  -- data_sync_1
  --dbms_output.put_line('--- doing data_sync_1');
  SELECT MIN(data_sync_id), MAX(data_sync_id)
  into min_id, max_id
  FROM engine.data_sync_1 ds;
  last_id := min_id;
  --dbms_output.put_line('MIN = ' || min_id || ', MAX = ' || max_id);
  while last_id < max_id loop
    DELETE FROM engine.data_sync_1 
    WHERE data_sync_id IN (
      select data_sync_id
      FROM engine.data_sync_1 ds
      JOIN pss.pos_pta pp ON ds.item_id = pp.pos_pta_id
      WHERE ds.data_sync_type_cd = 'DEVICE_PTAS'
      AND ds.object_cd = 'PSS.POS_PTA'
      AND ds.created_by = 'SYSTEM'
      AND pp.pos_pta_deactivation_ts IS NOT NULL
      AND ds.data_sync_id BETWEEN last_id AND (last_id+10000));
    --dbms_output.put_line('last_id = ' || last_id || ', row_count = ' || row_count);
    COMMIT;
    last_id := (last_id + 10000);
  END loop;
  
  -- data_sync_2
  --dbms_output.put_line('--- doing data_sync_2');
  SELECT MIN(data_sync_id), MAX(data_sync_id)
  into min_id, max_id
  FROM engine.data_sync_2 ds;
  last_id := min_id;
  --dbms_output.put_line('MIN = ' || min_id || ', MAX = ' || max_id);
  while last_id < max_id loop
    DELETE FROM engine.data_sync_2
    WHERE data_sync_id IN (
      select data_sync_id
      FROM engine.data_sync_2 ds
      JOIN pss.pos_pta pp ON ds.item_id = pp.pos_pta_id
      WHERE ds.data_sync_type_cd = 'DEVICE_PTAS'
      AND ds.object_cd = 'PSS.POS_PTA'
      AND ds.created_by = 'SYSTEM'
      AND pp.pos_pta_deactivation_ts IS NOT NULL
      AND ds.data_sync_id BETWEEN last_id AND (last_id+10000));
    COMMIT;
    --dbms_output.put_line('last_id = ' || last_id || ', row_count = ' || row_count);
    last_id := (last_id + 10000);
  END loop;
  
  -- data_sync_3
  --dbms_output.put_line('--- doing data_sync_3');
  SELECT MIN(data_sync_id), MAX(data_sync_id)
  into min_id, max_id
  FROM engine.data_sync_3 ds;
  last_id := min_id;
  --dbms_output.put_line('MIN = ' || min_id || ', MAX = ' || max_id);
  while last_id < max_id loop
    DELETE FROM engine.data_sync_3
    WHERE data_sync_id IN (
      select data_sync_id
      FROM engine.data_sync_3 ds
      JOIN pss.pos_pta pp ON ds.item_id = pp.pos_pta_id
      WHERE ds.data_sync_type_cd = 'DEVICE_PTAS'
      AND ds.object_cd = 'PSS.POS_PTA'
      AND ds.created_by = 'SYSTEM'
      AND pp.pos_pta_deactivation_ts IS NOT NULL
      AND ds.data_sync_id BETWEEN last_id AND (last_id+10000));
    COMMIT;
    --dbms_output.put_line('last_id = ' || last_id || ', row_count = ' || row_count);
    last_id := (last_id + 10000);
  END loop;

  -- data_sync_4
  --dbms_output.put_line('--- doing data_sync_4');
  SELECT MIN(data_sync_id), MAX(data_sync_id)
  into min_id, max_id
  FROM engine.data_sync_4 ds;
  last_id := min_id;
  --dbms_output.put_line('MIN = ' || min_id || ', MAX = ' || max_id);
  while last_id < max_id loop
    DELETE FROM engine.data_sync_4
    WHERE data_sync_id IN (
      select data_sync_id
      FROM engine.data_sync_4 ds
      JOIN pss.pos_pta pp ON ds.item_id = pp.pos_pta_id
      WHERE ds.data_sync_type_cd = 'DEVICE_PTAS'
      AND ds.object_cd = 'PSS.POS_PTA'
      AND ds.created_by = 'SYSTEM'
      AND pp.pos_pta_deactivation_ts IS NOT NULL
      AND ds.data_sync_id BETWEEN last_id AND (last_id+10000));
    COMMIT;
    --dbms_output.put_line('last_id = ' || last_id || ', row_count = ' || row_count);
    last_id := (last_id + 10000);
  END loop;

END;
