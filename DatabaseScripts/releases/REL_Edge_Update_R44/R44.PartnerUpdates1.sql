GRANT SELECT ON CORP.PAY_CYCLE TO USALIVE_APP_ROLE;

INSERT INTO WEB_CONTENT.WEB_LINK(web_link_id, web_link_label, web_link_url, web_link_desc, web_link_group, web_link_usage, web_link_order) 
VALUES (268, 'Bank Accounts', './bank_accounts.html', 'View Customer Bank Accounts', 'Administration', 'M', 153);

INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT(web_link_id, requirement_id) 
VALUES (268, 1);

INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT(web_link_id, requirement_id) 
VALUES (268, 9);

GRANT SELECT, INSERT, UPDATE ON CORP.SERVICE_FEE_COMMISSION to REPORT;
GRANT SELECT, INSERT, UPDATE ON CORP.SERVICE_FEE_COMMISSION to USALIVE_APP_ROLE;

GRANT SELECT, INSERT, UPDATE ON CORP.PROCESS_FEE_COMMISSION to REPORT;
GRANT SELECT, INSERT, UPDATE ON CORP.PROCESS_FEE_COMMISSION to USALIVE_APP_ROLE;

alter table corp.ledger add (
  process_fee_commission_id number,
  service_fee_commission_id number
);
