DELETE 
  FROM WEB_CONTENT.LITERAL 
 WHERE LITERAL_KEY LIKE 'prepaid-%'
   AND (LITERAL_VALUE LIKE '/images/%' OR LITERAL_VALUE LIKE '/css/%');

COMMIT;

INSERT INTO WEB_CONTENT.SUBDOMAIN(SUBDOMAIN_URL, SUBDOMAIN_DESCRIPTION, APP_CD)
    SELECT SD.SUBDOMAIN_URL || '/canteen', 'Canteen ' || SUBDOMAIN_DESCRIPTION, 'Prepaid'
      FROM WEB_CONTENT.SUBDOMAIN SD
     WHERE SD.APP_CD = 'Prepaid'
       AND SD.SUBDOMAIN_URL NOT LIKE '%/%'
       AND NOT EXISTS(SELECT 1 FROM WEB_CONTENT.SUBDOMAIN SD0 WHERE SD0.SUBDOMAIN_URL = SD.SUBDOMAIN_URL || '/canteen');

INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, LITERAL_VALUE, SUBDOMAIN_ID)
    SELECT L.LITERAL_KEY, L.LITERAL_VALUE, SD.SUBDOMAIN_ID
      FROM (SELECT '' LITERAL_KEY, '' LITERAL_VALUE FROM DUAL WHERE 1=0
          --UNION ALL SELECT 'prepaid-company-address-line1', '2400 Yorkmont Road' FROM DUAL
          --UNION ALL SELECT 'prepaid-company-address-line2', 'Charlotte, NC 28217' FROM DUAL
          --UNION ALL SELECT 'prepaid-company-email-address', '' FROM DUAL
          --UNION ALL SELECT 'prepaid-company-name', 'Canteen Vending' FROM DUAL
          --UNION ALL SELECT 'prepaid-company-phone-number', '1-800-CANTEEN' FROM DUAL
          --UNION ALL SELECT 'prepaid-company-website-url', 'http://www.canteen.com/' FROM DUAL
          --UNION ALL SELECT 'prepaid-footer-image-title', '' FROM DUAL
          UNION ALL SELECT 'prepaid-home-description', 'Start getting <span class="more-text">more</span> with every purchase at participating vending locations with this ultra-convenient prepaid and loyalty program. Simply use your card at participating machines to see the benefits add up!' FROM DUAL
          --UNION ALL SELECT 'prepaid-home-image-title', '' FROM DUAL
          --UNION ALL SELECT 'prepaid-home-text1', '' FROM DUAL
          --UNION ALL SELECT 'prepaid-home-text2', '' FROM DUAL
          --UNION ALL SELECT 'prepaid-home-text3', '' FROM DUAL
          UNION ALL SELECT 'prepaid-more-color', '#559bc8' FROM DUAL
          --UNION ALL SELECT 'prepaid-new-user-image-title', '' FROM DUAL
          --UNION ALL SELECT 'prepaid-register-email-extra-html', '' FROM DUAL
          --UNION ALL SELECT 'prepaid-replenishment-note', '' FROM DUAL
          UNION ALL SELECT 'prepaid-title', 'Canteen Vending -' FROM DUAL
      ) L,
      WEB_CONTENT.SUBDOMAIN SD
     WHERE SD.SUBDOMAIN_URL LIKE '%/canteen'
       AND SD.APP_CD = 'Prepaid'
       AND NOT EXISTS(SELECT 1 FROM WEB_CONTENT.LITERAL L0 
       WHERE L0.LITERAL_KEY = L.LITERAL_KEY
         AND L0.SUBDOMAIN_ID = SD.SUBDOMAIN_ID);
  
COMMIT;
