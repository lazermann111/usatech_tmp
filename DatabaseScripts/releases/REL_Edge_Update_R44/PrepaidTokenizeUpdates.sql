
-- pos template additions
INSERT INTO PSS.POS_PTA_TMPL(POS_PTA_TMPL_NAME, POS_PTA_TMPL_DESC) 
    SELECT 'DEFAULT: Prepaid Virtual Tokenization - US','Default template for Virtual Tokenization in US during initialization' FROM DUAL
     WHERE NOT EXISTS (SELECT 1 FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'DEFAULT: Prepaid Virtual Tokenization - US');

INSERT INTO PSS.POS_PTA_TMPL(POS_PTA_TMPL_NAME, POS_PTA_TMPL_DESC) 
    SELECT 'DEFAULT: Prepaid Virtual Tokenization - Canada','Default template for Virtual Tokenization in Canada during initialization' FROM DUAL
     WHERE NOT EXISTS (SELECT 1 FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'DEFAULT: Prepaid Virtual Tokenization - Canada');
     
-- device type update
UPDATE DEVICE.DEVICE_TYPE 
   SET DEVICE_TYPE_SERIAL_CD_REGEX = '^V[1-3]-.*$'
 WHERE DEVICE_TYPE_ID = 14;

-- device sub type additions
INSERT INTO DEVICE.DEVICE_SUB_TYPE(DEVICE_TYPE_ID, DEVICE_SUB_TYPE_ID,
                                    DEVICE_SUB_TYPE_DESC, DEVICE_SERIAL_CD_REGEX, POS_PTA_TMPL_ID,
                                    DEF_BASE_HOST_TYPE_ID, DEF_BASE_HOST_EQUIPMENT_ID,
                                    MASTER_ID_ALWAYS_INC, REQUIRE_PREREGISTER) 
    SELECT 14,5,'Prepaid Tokenize - USD', '^V3-.*-USD$',T.POS_PTA_TMPL_ID,1,HE.HOST_EQUIPMENT_ID,'N','Y'
      FROM PSS.POS_PTA_TMPL T
      CROSS JOIN DEVICE.HOST_EQUIPMENT HE
     WHERE T.POS_PTA_TMPL_NAME = 'DEFAULT: Prepaid Virtual Tokenization - US'
       AND HE.HOST_EQUIPMENT_MFGR = 'Unknown'
       AND HE.HOST_EQUIPMENT_MODEL = 'Unknown'
       AND NOT EXISTS(SELECT 1 FROM DEVICE.DEVICE_SUB_TYPE WHERE DEVICE_TYPE_ID = 14 AND DEVICE_SUB_TYPE_ID = 5);

INSERT INTO DEVICE.DEVICE_SUB_TYPE(DEVICE_TYPE_ID, DEVICE_SUB_TYPE_ID, 
                                    DEVICE_SUB_TYPE_DESC, DEVICE_SERIAL_CD_REGEX, POS_PTA_TMPL_ID,
                                    DEF_BASE_HOST_TYPE_ID, DEF_BASE_HOST_EQUIPMENT_ID,
                                    MASTER_ID_ALWAYS_INC, REQUIRE_PREREGISTER) 
    SELECT 14,6,'Prepaid Tokenize - CAD', '^V3-.*-CAD$', T.POS_PTA_TMPL_ID,1,HE.HOST_EQUIPMENT_ID,'N','Y'
      FROM PSS.POS_PTA_TMPL T
      CROSS JOIN DEVICE.HOST_EQUIPMENT HE
     WHERE T.POS_PTA_TMPL_NAME = 'DEFAULT: Prepaid Virtual Tokenization - Canada'
       AND HE.HOST_EQUIPMENT_MFGR = 'Unknown'
       AND HE.HOST_EQUIPMENT_MODEL = 'Unknown'
       AND NOT EXISTS(SELECT 1 FROM DEVICE.DEVICE_SUB_TYPE WHERE DEVICE_TYPE_ID = 14 AND DEVICE_SUB_TYPE_ID = 6);

-- template addition
INSERT INTO PSS.POS_PTA_TMPL_ENTRY(POS_PTA_TMPL_ID, PAYMENT_SUBTYPE_ID, POS_PTA_ACTIVATION_OSET_HR, PAYMENT_SUBTYPE_KEY_ID, POS_PTA_PRIORITY, CURRENCY_CD, AUTHORITY_PAYMENT_MASK_ID)
    SELECT PPT.POS_PTA_TMPL_ID, PST.PAYMENT_SUBTYPE_ID, 0, IPT.INTERNAL_PAYMENT_TYPE_ID, 1,
           CASE WHEN PPT.POS_PTA_TMPL_NAME = 'DEFAULT: Prepaid Virtual Tokenization - US' THEN 'USD' WHEN PPT.POS_PTA_TMPL_NAME = 'DEFAULT: Prepaid Virtual Tokenization - Canada' THEN 'CAD' END,
           PST.AUTHORITY_PAYMENT_MASK_ID
      FROM PSS.POS_PTA_TMPL PPT
     CROSS JOIN PSS.PAYMENT_SUBTYPE PST
      JOIN AUTHORITY.HANDLER H ON PST.PAYMENT_SUBTYPE_CLASS = H.HANDLER_CLASS
      JOIN AUTHORITY.AUTHORITY_TYPE AUT ON AUT.HANDLER_ID = H.HANDLER_ID
      JOIN AUTHORITY.AUTHORITY AU ON AU.AUTHORITY_TYPE_ID = AUT.AUTHORITY_TYPE_ID
      JOIN PSS.INTERNAL_AUTHORITY IA ON IA.AUTHORITY_ID = AU.AUTHORITY_ID
      JOIN PSS.INTERNAL_PAYMENT_TYPE IPT ON IPT.INTERNAL_AUTHORITY_ID = IA.INTERNAL_AUTHORITY_ID
     WHERE PPT.POS_PTA_TMPL_NAME LIKE 'DEFAULT: Prepaid Virtual Tokenization - %'
       AND PST.CLIENT_PAYMENT_TYPE_CD = 'Q'
       AND PST.PAYMENT_SUBTYPE_CLASS = 'Internal::Prepaid'
       AND PST.STATUS_CD = 'A'
       AND IA.AUTHORITY_SERVICE_TYPE_ID = 1
       AND NOT EXISTS(SELECT 1 FROM PSS.POS_PTA_TMPL_ENTRY E WHERE E.POS_PTA_TMPL_ID = PPT.POS_PTA_TMPL_ID AND E.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID);

COMMIT;

-- device addition of V3-1-USD, V3-1-CAD
DECLARE
    CURSOR l_cur IS
      SELECT 'V3-1-' || CU.CURRENCY_CODE DEVICE_SERIAL_CD
        FROM CORP.COUNTRY CO
        JOIN CORP.CURRENCY CU ON CO.CURRENCY_ID = CU.CURRENCY_ID
       WHERE CO.ALLOW_BANKS = 'Y';
    ln_device_id NUMBER;
    lv_device_name VARCHAR2(60);
    lv_device_type_desc VARCHAR2(60);
    ln_device_sub_type_id NUMBER(3);
    lc_prev_device_active_yn_flag VARCHAR(1);
    lc_master_id_always_inc VARCHAR(1);
    ln_key_gen_time NUMBER;
    lc_legacy_safe_key VARCHAR(1);
    lv_time_zone_guid REPORT.TIME_ZONE.TIME_ZONE_GUID%TYPE;
    ln_new_host_count NUMBER;
    ln_result_cd NUMBER;
    lv_error_message VARCHAR2(1000);
BEGIN
    FOR l_rec IN l_cur LOOP  
        -- insert device
        PKG_DEVICE_CONFIGURATION.INITIALIZE_DEVICE(l_rec.DEVICE_SERIAL_CD, 14, 'Y',
            ln_device_id,
            lv_device_name,
            lv_device_type_desc,
            ln_device_sub_type_id,
            lc_prev_device_active_yn_flag,
            lc_master_id_always_inc,
            ln_key_gen_time,
            lc_legacy_safe_key,
            lv_time_zone_guid,
            ln_new_host_count,
            ln_result_cd,
            lv_error_message                              
        );
     
        IF ln_device_id <= 0 THEN
            RAISE_APPLICATION_ERROR(-20010, lv_error_message);
        END IF;
    END LOOP;
END;
/

COMMIT;