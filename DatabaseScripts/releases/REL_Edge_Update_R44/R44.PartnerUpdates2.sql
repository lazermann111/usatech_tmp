alter table corp.process_fee_commission add (
  COMMISSION_MIN NUMBER(17,4),
  BUY_AMOUNT NUMBER(17,4),
  BUY_PERCENT NUMBER(8,4),
  BUY_MIN NUMBER(17,4),
  SELL_AMOUNT NUMBER(17,4),
  SELL_PERCENT NUMBER(8,4),
  SELL_MIN NUMBER(17,4)
);

alter table corp.service_fee_commission add (
  BUY_AMOUNT NUMBER(17,4),
  SELL_AMOUNT NUMBER(17,4)
);

update corp.process_fee_commission u
set (buy_amount, buy_percent, buy_min, sell_amount, sell_percent, sell_min) = 
(select plpf.fee_amount, plpf.fee_percent, plpf.min_amount, clpf.fee_amount, clpf.fee_percent, clpf.min_amount
FROM corp.license cl, corp.license_process_fees clpf, corp.license pl, corp.license_process_fees plpf, corp.process_fees cpf, corp.process_fee_commission cpfc
WHERE cl.parent_license_id = pl.license_id
and cl.license_id = clpf.license_id
and pl.license_id = plpf.license_id
and clpf.trans_type_id = plpf.trans_type_id
and cpf.original_license_id = cl.license_id
and cpf.trans_type_id = clpf.trans_type_id
and cpf.process_fee_id = cpfc.process_fee_id
and u.process_fee_commission_id = cpfc.process_fee_commission_id);

update corp.service_fee_commission u
set (buy_amount, sell_amount) = 
(select plsf.amount, clsf.amount
FROM corp.license cl, corp.license_service_fees clsf, corp.license pl, corp.license_service_fees plsf, corp.service_fees csf, corp.service_fee_commission csfc
WHERE cl.parent_license_id = pl.license_id
and cl.license_id = clsf.license_id
and pl.license_id = plsf.license_id
and clsf.fee_id = plsf.fee_id
and csf.original_license_id = cl.license_id
and csf.fee_id = clsf.fee_id
and csf.service_fee_id = csfc.service_fee_id
and u.service_fee_commission_id = csfc.service_fee_commission_id);

COMMIT;
