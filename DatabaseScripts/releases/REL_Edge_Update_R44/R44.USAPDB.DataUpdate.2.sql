DECLARE
    ln_start_terminal_id REPORT.TERMINAL.TERMINAL_ID%TYPE;
    ln_end_terminal_id REPORT.TERMINAL.TERMINAL_ID%TYPE;
    ln_interval PLS_INTEGER := 1000;
BEGIN
    SELECT MIN(TERMINAL_ID)
      INTO ln_start_terminal_id
      FROM REPORT.TERMINAL;
    SELECT MAX(TERMINAL_ID)
      INTO ln_end_terminal_id
      FROM REPORT.TERMINAL;
    WHILE  ln_start_terminal_id <= ln_end_terminal_id LOOP 
        DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS') || ' - Processing TERMINAL_IDS ' || ln_start_terminal_id || ' to ' || (ln_start_terminal_id + ln_interval));
        INSERT INTO REPORT.TERMINAL_REGION(TERMINAL_ID, REGION_ID)
           SELECT T.TERMINAL_ID, R.REGION_ID
             FROM REPORT.TERMINAL T
             JOIN REPORT.REGION R ON T.CUSTOMER_ID = R.CUSTOMER_ID
            WHERE R.REGION_NAME IS NULL
              AND NOT EXISTS(SELECT 1 FROM REPORT.TERMINAL_REGION TR WHERE T.TERMINAL_ID = TR.TERMINAL_ID)
              AND T.TERMINAL_ID >= ln_start_terminal_id 
              AND T.TERMINAL_ID < ln_start_terminal_id + ln_interval;             
        DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS') || ' - Updated ' || SQL%ROWCOUNT || ' TERMINAL_IDS from ' || ln_start_terminal_id || ' to ' || (ln_start_terminal_id + ln_interval));
        COMMIT;
        ln_start_terminal_id := ln_start_terminal_id + ln_interval;
    END LOOP;
END;
/
