GRANT SELECT ON CORP.VW_PAYMENT_PFC TO USALIVE_APP_ROLE;
GRANT SELECT ON CORP.VW_PAYMENT_PFC TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.VW_PAYMENT_PFC TO USAT_DEV_READ_ONLY;

GRANT SELECT ON CORP.VW_PAYMENT_SFC TO USALIVE_APP_ROLE;
GRANT SELECT ON CORP.VW_PAYMENT_SFC TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.VW_PAYMENT_SFC TO USAT_DEV_READ_ONLY;

