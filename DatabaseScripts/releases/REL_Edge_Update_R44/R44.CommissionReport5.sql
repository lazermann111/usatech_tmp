ALTER SESSION SET CURRENT_SCHEMA = REPORT;
DECLARE
  LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := -1;
  LV_REPORT_NAME VARCHAR(50) := 'Transaction List';
BEGIN
  SELECT MAX(REPORT_ID) INTO LN_REPORT_ID
  FROM REPORT.REPORTS WHERE REPORT_NAME = LV_REPORT_NAME;
  
  IF LN_REPORT_ID > 0 THEN
    RETURN;
  END IF;

  SELECT REPORT.REPORTS_SEQ.NEXTVAL INTO LN_REPORT_ID FROM DUAL;
  
INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
VALUES( LN_REPORT_ID, LV_REPORT_NAME, 6, 0, LV_REPORT_NAME, LV_REPORT_NAME, 'N', 0);

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( LN_REPORT_ID, 'query', '
select 
  decode(corp.payments_pkg.entry_payable(c.commission_settle_state_id, c.commission_entry_type), ''Y'', ''Settled'', ''Unsettled'') "Transaction Status",
  fee_customer.customer_name "Customer",
  report.get_devices_for_terminal(c.terminal_id, c.fee_entry_date) "Serial Number",
  fee_location.location_name "Location",
  fee_terminal.asset_nbr "Asset Number",
  fee_entry_type.entry_type_name "Fee Type",
  TO_CHAR(c.fee_entry_date,''MM/DD/YYYY HH24:MI:SS'') "Transaction Date",
  c.buy_rate "Buy Rate",
  c.sell_rate "Sell Rate",
  TRIM(TO_CHAR(NVL(c.transaction_amount, 0), ''FML999,999,990.009999'')) "Transaction Amount",
  TRIM(TO_CHAR(NVL(c.commission_amount, 0), ''FML999,999,990.009999'')) "Commission Amount",
  commission_doc_status.status_name "EFT Status",
  commission_doc.ref_nbr "EFT Reference Number",
  TO_CHAR(commission_doc.sent_date,''MM/DD/YYYY HH24:MI:SS'') "EFT Date"
from corp.vw_tran_commission c
  join corp.entry_type fee_entry_type
    on fee_entry_type.entry_type = c.fee_entry_type
  join corp.doc commission_doc 
    on commission_doc.doc_id = c.commission_doc_id
  join corp.doc_status commission_doc_status
    on commission_doc_status.status = commission_doc.status
  join report.terminal fee_terminal
    on fee_terminal.terminal_id = c.terminal_id
  join report.location fee_location
    on fee_location.location_id = fee_terminal.location_id
  join corp.customer fee_customer
    on fee_customer.customer_id = c.fee_customer_id
where 
  c.commission_entry_date >= ?
  and c.commission_entry_date < ?
  and c.commission_customer_id = ?
order by
  1, 2, 3, 12
');

insert into report.report_param values(LN_REPORT_ID, 'paramNames','StartDate,EndDate,CustomerId');
insert into report.report_param values(LN_REPORT_ID, 'paramTypes','TIMESTAMP,TIMESTAMP,NUMBER');
insert into report.report_param values(LN_REPORT_ID, 'params.StartDate','{b}');
insert into report.report_param values(LN_REPORT_ID, 'params.EndDate','{e}');
insert into report.report_param values(LN_REPORT_ID, 'params.CustomerId','user.customerId');
insert into report.report_param values(LN_REPORT_ID, 'isSQLFolio','true');

insert into web_content.web_link(web_link_id, web_link_label, web_link_url, web_link_desc, web_link_group, web_link_usage, web_link_order) 
values(web_content.seq_web_link_id.nextval, LV_REPORT_NAME, './select_date_range_frame_sqlfolio.i?basicReportId='||LN_REPORT_ID,LV_REPORT_NAME, 'Commissions', 'W', 50);

insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 1);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 40);

END;
/

commit;
