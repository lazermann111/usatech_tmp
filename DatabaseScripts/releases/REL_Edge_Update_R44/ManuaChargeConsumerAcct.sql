set serveroutput on
set serveroutput on size 1000000
WHENEVER SQLERROR EXIT FAILURE COMMIT;
DECLARE
  		l_consumer_acct_promo_balance PSS.CONSUMER_ACCT.consumer_acct_promo_balance%TYPE;
        l_consumer_acct_replen_balance PSS.CONSUMER_ACCT.consumer_acct_replen_balance%TYPE;
        l_consumer_acct_balance PSS.CONSUMER_ACCT.consumer_acct_balance%TYPE;
        l_charge_amount PSS.CONSUMER_ACCT.consumer_acct_balance%TYPE;
        l_charge_diff PSS.CONSUMER_ACCT.consumer_acct_balance%TYPE;

BEGIN
    
	FOR l_charge in (select rownum, cardId, amount from yhe.charge_12212014) LOOP
		select consumer_acct_promo_balance, consumer_acct_replen_balance, consumer_acct_balance
		into l_consumer_acct_promo_balance, l_consumer_acct_replen_balance, l_consumer_acct_balance
		from pss.consumer_acct where consumer_acct_identifier=l_charge.cardId;
		
		dbms_output.put(l_charge.rownum||' check cardId='||l_charge.cardId||' amount='||l_charge.amount||' consumer_acct_promo_balance='||l_consumer_acct_promo_balance||' consumer_acct_replen_balance='||l_consumer_acct_replen_balance||' consumer_acct_balance='||l_consumer_acct_balance);
		IF l_consumer_acct_promo_balance >= l_charge.amount THEN
			dbms_output.put(' deduct '||l_charge.amount||' from consumer_acct_promo_balance='||l_consumer_acct_promo_balance);
			update pss.consumer_acct set consumer_acct_balance = consumer_acct_balance -l_charge.amount,
			consumer_acct_promo_balance=consumer_acct_promo_balance-l_charge.amount
			where consumer_acct_identifier = l_charge.cardId;
			IF SQL%ROWCOUNT <>1 THEN
		        dbms_output.put(' doublecheck ');
		    END IF;
		ELSIF l_charge.amount<=l_consumer_acct_promo_balance+l_consumer_acct_replen_balance THEN
			l_charge_diff:=l_charge.amount-l_consumer_acct_promo_balance;
			dbms_output.put(' deduct '||l_consumer_acct_promo_balance||' from l_consumer_acct_promo_balance='||l_consumer_acct_promo_balance);
			dbms_output.put(' deduct '||l_charge_diff||' from l_consumer_acct_replen_balance='||l_consumer_acct_replen_balance);
			update pss.consumer_acct set consumer_acct_balance = consumer_acct_balance -l_charge.amount,
			consumer_acct_promo_balance=0,
			consumer_acct_replen_balance=consumer_acct_replen_balance-l_charge_diff 
			where consumer_acct_identifier = l_charge.cardId;
			IF SQL%ROWCOUNT <>1 THEN
		        dbms_output.put(' doublecheck ');
		    END IF;
		ELSE
			dbms_output.put(' cardId='||l_charge.cardId||' insufficient funds');
			IF l_charge.amount < l_consumer_acct_balance THEN
        		dbms_output.put(' do it manually');
      		END IF;
		END IF;
		DBMS_OUTPUT.NEW_LINE;
		commit;
	END LOOP;
END;

-- create table yhe.charge_12212014(
         --cardId      NUMBER(20,0),
         --amount      NUMBER);
--RENAME charge_12212014 TO charge_12212014_done