CREATE OR REPLACE FUNCTION DBADMIN.WELFORD_S (
  pn_old_count IN NUMBER,
  pn_old_sum IN NUMBER,
  pn_old_s IN NUMBER,
  pn_amt IN NUMBER)
RETURN NUMBER IS
  ln_old_count NUMBER := NVL(pn_old_count,0);
  ln_old_sum NUMBER := NVL(pn_old_sum,0);
  ln_old_s NUMBER := NVL(pn_old_s,0);
  ln_amt NUMBER := NVL(pn_amt,0);
  ln_new_mean NUMBER;
BEGIN
  IF ln_old_count = 0 THEN
    RETURN 0;
  END IF;
  ln_new_mean := (ln_old_sum + ln_amt) / (ln_old_count + 1);
  RETURN (ln_old_s + (ln_amt - (ln_old_sum / ln_old_count)) * (ln_amt - ln_new_mean));
END;
/

GRANT EXECUTE ON DBADMIN.WELFORD_S TO PUBLIC;

CREATE OR REPLACE FUNCTION DBADMIN.WELFORD_STDEV (
  pn_old_count IN NUMBER,
  pn_old_sum IN NUMBER,
  pn_old_s IN NUMBER,
  pn_amt IN NUMBER)
RETURN NUMBER IS
BEGIN
  IF NVL(pn_old_count,0) = 0 THEN
    RETURN NULL;
  END IF;
  RETURN (SQRT(WELFORD_S(pn_old_count, pn_old_sum, pn_old_s, pn_amt)/pn_old_count));
END;
/

GRANT EXECUTE ON DBADMIN.WELFORD_STDEV TO PUBLIC;

CREATE OR REPLACE FUNCTION DBADMIN.WELFORD_REVERSE_S (
  pn_old_count IN NUMBER,
  pn_old_sum IN NUMBER,
  pn_old_s IN NUMBER,
  pn_amt IN NUMBER)
RETURN NUMBER IS
  ln_old_count NUMBER := NVL(pn_old_count,0);
  ln_old_sum NUMBER := NVL(pn_old_sum,0);
  ln_old_s NUMBER := NVL(pn_old_s,0);
  ln_amt NUMBER := NVL(pn_amt,0);
  ln_new_mean NUMBER;
  ln_s NUMBER;
BEGIN
  IF ln_old_count <= 1 THEN
    RETURN 0;
  END IF;
  ln_new_mean := (ln_old_sum - ln_amt) / (ln_old_count - 1);
  ln_s := (ln_old_s - (pn_amt - ln_new_mean) * (pn_amt - (ln_old_sum / ln_old_count)));
  IF ln_s < 0 THEN
    RETURN 0;
  END IF;
  RETURN ln_s;
END;
/

GRANT EXECUTE ON DBADMIN.WELFORD_REVERSE_S TO PUBLIC;

CREATE OR REPLACE FUNCTION DBADMIN.WELFORD_REVERSE_STDEV (
  pn_old_count IN NUMBER,
  pn_old_sum IN NUMBER,
  pn_old_s IN NUMBER,
  pn_amt IN NUMBER)
RETURN NUMBER IS
BEGIN
  IF NVL(pn_old_count,0) <= 1 THEN
    RETURN NULL;
  END IF;
  RETURN (SQRT(WELFORD_REVERSE_S(pn_old_count, pn_old_sum, pn_old_s, pn_amt)/(pn_old_count-1)));
END;
/

GRANT EXECUTE ON DBADMIN.WELFORD_REVERSE_STDEV TO PUBLIC;
