/*
select * from device.CONFIG_TEMPLATE_CATEGORY;
select * from device.CONFIG_TEMPLATE_setting where DEVICE_SETTING_PARAMETER_CD = '1201' and category_id is not null;
select * from device.CONFIG_TEMPLATE_setting where category_id = 6;
select * from device.CONFIG_TEMPLATE where device_type_id = 13 and property_list_version = 18;
*/

--BEGIN global changes (for all plvs)
  UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
     SET DESCRIPTION = '0 - Sale Amount, authorization for the exact purchase amount that the sale message will be sent with. Note, code will automatically reset to Fixed Amount if a problem occurs<br/>1 - Fixed Amount, authorization for the amount configured in the Authorization Amount property<br/>2 - Sale Amount Fixed, same as 0 however code will never change this setting',
         EDITOR = 'SELECT:0=0 - Sale Amount;1=1 - Fixed Amount;2=2 - Sale Amount Fixed'
   WHERE DEVICE_SETTING_PARAMETER_CD IN('1201')
      AND CATEGORY_ID IS NOT NULL;
--END global changes
COMMIT;

DECLARE
	ln_property_list_version NUMBER := 18;
	ln_device_type_id device_type.device_type_id%TYPE := 13;
	ln_config_template_type_id config_template_type.config_template_type_id%TYPE := 1;
	lv_config_template_name config_template.config_template_name%TYPE := 'DEFAULT-CFG-' || ln_device_type_id || '-' || ln_property_list_version;
	ln_config_template_id config_template.config_template_id%TYPE;
BEGIN
	SELECT MAX(config_template_id) INTO ln_config_template_id
	FROM device.config_template
	WHERE config_template_name = lv_config_template_name;
	
	IF ln_config_template_id IS NULL THEN
		INSERT INTO device.config_template(config_template_type_id, config_template_name, device_type_id, property_list_version)
		VALUES(ln_config_template_type_id, lv_config_template_name, ln_device_type_id, ln_property_list_version);
		
		SELECT MAX(config_template_id) INTO ln_config_template_id
		FROM device.config_template
		WHERE config_template_name = lv_config_template_name;
	END IF;
	
	DELETE FROM DEVICE.CONFIG_TEMPLATE_SETTING WHERE CONFIG_TEMPLATE_ID = ln_config_template_id;
  
  -- BEGIN Additions
  INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_UI_CONFIGURABLE) 
      SELECT '1026', 'Y'
        FROM DUAL
       WHERE NOT EXISTS(SELECT 1 FROM DEVICE.DEVICE_SETTING_PARAMETER WHERE DEVICE_SETTING_PARAMETER_CD = '1026');
	
  INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(CONFIG_TEMPLATE_ID,DEVICE_SETTING_PARAMETER_CD,CONFIG_TEMPLATE_SETTING_VALUE,CATEGORY_ID,DISPLAY,DESCRIPTION,EDITOR,CONVERTER,CLIENT_SEND,ALIGN,FIELD_SIZE,FIELD_OFFSET,PAD_CHAR,PROPERTY_LIST_VERSION,DEVICE_TYPE_ID,DISPLAY_ORDER,ALT_NAME,DATA_MODE,DATA_MODE_AUX,REGEX_ID,ACTIVE,NAME,CUSTOMER_EDITABLE,STORED_IN_PENNIES) 
    VALUES(ln_config_template_id, '1026', '3', 6, 'Y', 'This property directs what happens when the ePort receives a MDB Cash Message while a cashless session is in process<br/>"Ignore" means do not record the cash message (like G8).<br/>"Normal" means record the cash message as a normal separate Cash Sale (property 1022 must be enabled).<br/>"Mixed" means report the cash message as a line item within the Cashless Sale Record (Type 206, qty=1) (property 1022 does not matter).','SELECT:1=1 - Ignore;2=2 - Normal;3=3 - Mixed','','Y',NULL,5,NULL,NULL,ln_property_list_version,ln_device_type_id,29,NULL,NULL,NULL,3,'Y','MDB Setting Capture Cash Mixed Tender','N',NULL);

  -- END Additions
  
	INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(CONFIG_TEMPLATE_ID, DEVICE_SETTING_PARAMETER_CD, CONFIG_TEMPLATE_SETTING_VALUE) 
      SELECT ln_config_template_id, DEVICE_SETTING_PARAMETER_CD, CONFIG_TEMPLATE_SETTING_VALUE
        FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS
        JOIN DEVICE.CONFIG_TEMPLATE CT ON CTS.CONFIG_TEMPLATE_ID = CT.CONFIG_TEMPLATE_ID
       WHERE CONFIG_TEMPLATE_NAME = 'DEFAULT-CFG-' || ln_device_type_id || '-' || (ln_property_list_version - 1)
         AND DEVICE_SETTING_PARAMETER_CD NOT IN(SELECT DEVICE_SETTING_PARAMETER_CD FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS WHERE CONFIG_TEMPLATE_ID = ln_config_template_id);

  -- BEGIN Default Updates
  UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
     SET CONFIG_TEMPLATE_SETTING_VALUE = DECODE(DEVICE_SETTING_PARAMETER_CD,
         '1022', '1',
         '1024', '1',
         '1205', ' Use Your Credit',
         '1206', '  or Debit Card ',
         CONFIG_TEMPLATE_SETTING_VALUE)
   WHERE DEVICE_SETTING_PARAMETER_CD IN('1022','1024', '1205', '1206')
     AND CONFIG_TEMPLATE_ID = ln_config_template_id;
  -- END Default Updates
  
	DELETE FROM DEVICE.DEVICE_TYPE_PROPERTY_LIST
	WHERE device_type_id = ln_device_type_id
		AND property_list_version = ln_property_list_version;
	
	INSERT INTO DEVICE.DEVICE_TYPE_PROPERTY_LIST(device_type_id, property_list_version, all_property_list_ids, configurable_property_list_ids)
	VALUES(ln_device_type_id, ln_property_list_version, 
		'0-7|10|20-27|30-33|50-52|60-64|70|71|80|81|85-87|100-108|200-208|215|300|301|1001-1026|1101-1106|1200-1206|1300|1303|1400-1415|1500-1528',
		'0-7|10|20-27|30-33|70|71|85-87|215|1001-1026|1101-1106|1200-1206|1300|1303|1400-1415|1500-1528');
	
  -- Add settings to any devices with this plv
  INSERT INTO DEVICE.DEVICE_SETTING(DEVICE_ID, DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_VALUE)
     SELECT D.DEVICE_ID, CTS.DEVICE_SETTING_PARAMETER_CD, CTS.CONFIG_TEMPLATE_SETTING_VALUE
       FROM DEVICE.DEVICE D
       JOIN DEVICE.DEVICE_SETTING DS ON D.DEVICE_ID = DS.DEVICE_ID AND DS.DEVICE_SETTING_PARAMETER_CD = 'Property List Version'
      CROSS JOIN DEVICE.CONFIG_TEMPLATE_SETTING CTS
       LEFT JOIN DEVICE.DEVICE_SETTING DS_X ON D.DEVICE_ID = DS_X.DEVICE_ID AND CTS.DEVICE_SETTING_PARAMETER_CD = DS_X.DEVICE_SETTING_PARAMETER_CD
      WHERE D.DEVICE_TYPE_ID = ln_device_type_id
        AND DS.DEVICE_SETTING_VALUE = TO_CHAR(ln_property_list_version)
        AND CTS.CONFIG_TEMPLATE_ID = ln_config_template_id
        AND DS_X.DEVICE_SETTING_PARAMETER_CD IS NULL;
     
	COMMIT;
END;
/
