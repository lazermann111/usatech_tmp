UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET EDITOR = 'SCHEDULE:D=Daily;I=Interval;W=Weekly'
WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD IN ('85', '86', '87');

UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET EDITOR = 'SCHEDULE:=None;D=Daily;I=Interval;W=Weekly'
WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD IN ('1101');

UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET DESCRIPTION = 'Schedule for generating and sending Settlement messages to the server. Time is in military format and device local time zone. Valid time values: 0000 - 2359. Valid interval minutes: 60 - 1439.'
WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD IN ('85');

UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET DESCRIPTION = 'Schedule for server session call-ins when the device is not activated. Time is in military format and device local time zone. Valid time values: 0000 - 2359. Valid interval minutes: 60 - 1439.'
WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD IN ('86');

UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET DESCRIPTION = 'Schedule for server session call-ins when the device is activated. Time is in military format and device local time zone. Valid time values: 0000 - 2359. Valid interval minutes: 60 - 1439.'
WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD IN ('87');

UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET DESCRIPTION = 'Schedule for capturing and sending DEX files to the server. Time is in military format and device local time zone. Valid time values: 0000 - 2359. Valid interval minutes: 60 - 1439.', CUSTOMER_EDITABLE = 'Y'
WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD IN ('1101');

COMMIT;

DECLARE
	CURSOR L_CUR IS
	SELECT D.DEVICE_ID, D.DEVICE_NAME
	FROM DEVICE.DEVICE D
	JOIN DEVICE.DEVICE_SETTING DS ON D.DEVICE_ID = DS.DEVICE_ID
		AND DS.DEVICE_SETTING_PARAMETER_CD = CASE WHEN D.DEVICE_TYPE_ID = 13 THEN '1500' WHEN D.DEVICE_TYPE_ID IN (0, 1) THEN '241' ELSE NULL END
	WHERE D.DEVICE_ACTIVE_YN_FLAG = 'Y' AND (D.DEVICE_TYPE_ID = 13 AND DS.DEVICE_SETTING_VALUE IN ('3', '4', '5', '7', '8')
			OR D.DEVICE_TYPE_ID IN (0, 1) AND DS.DEVICE_SETTING_VALUE = '07');
BEGIN
	FOR L_REC IN L_CUR LOOP
		ENGINE.PKG_DATA_SYNC.SP_CREATE_DATA_SYNC(
				'DEVICE_DETAIL',
				'DEVICE.DEVICE_SETTING',
				'U',
				L_REC.DEVICE_ID,
				L_REC.DEVICE_NAME,
				NULL);
		COMMIT;
	END LOOP;
END;
/

update web_content.literal set literal_value='We at USA Technologies, Inc. are continually striving to enhance our customers'' power to run their business effectively. The USALive website is loaded with features to help you do just that. For the best website experience, please use the latest major version of your browser. Now new in the latest USALive and ePort Connect version: <ul><li>Ability to automate Mass Device Update</li><li>RMA process improvements</li><li>New "Payment Summary by Region" report in Report Register</li><li>DEX Status report enhancements</li><li>Various performance and user experience improvements</li></ul>' where literal_key = 'home-page-message' and subdomain_id is null;
commit;
