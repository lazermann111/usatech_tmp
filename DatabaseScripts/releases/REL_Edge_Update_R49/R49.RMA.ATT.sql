DECLARE
	LN_WEB_LINK_ID WEB_CONTENT.WEB_LINK.WEB_LINK_ID%TYPE;
	LV_LABEL VARCHAR(50) := 'ATT 2G Upgrade';
BEGIN
	SELECT MAX(web_link_id) INTO LN_WEB_LINK_ID
	FROM WEB_CONTENT.WEB_LINK WHERE WEB_LINK_LABEL = LV_LABEL;
	
	IF LN_WEB_LINK_ID > 0 THEN
		RETURN;
	ELSE
		insert into web_content.web_link values(web_content.seq_web_link_id.nextval, 'ATT 2G Upgrade', './rma_att.html','Create ATT 2G Upgrade RMA','RMA','M', 282);
		insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 1);
		insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 39);
		insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 46);
		commit;
	END IF;
END;
/