DECLARE
  l_min_device_id NUMBER;
  l_max_device_id NUMBER;
BEGIN
	select min(device_id), max(device_id) into l_min_device_id, l_max_device_id from device.device where DEVICE_ACTIVE_YN_FLAG='Y' and device_type_id in (0,1,11,13);
    while l_min_device_id <= l_max_device_id loop
    	DBADMIN.PKG_GLOBAL.SET_TRANSACTION_VARIABLE('SOURCE_DATA_EXCHANGE_TYPE_ID', 1);
    	FOR l_cur in (select device_id from device.device where DEVICE_ACTIVE_YN_FLAG='Y' and device_type_id in (0,1,11,13) and device_id between l_min_device_id and l_min_device_id+1000-1) LOOP
	    	INSERT INTO DEVICE.DEVICE_SETTING(DEVICE_ID, DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_VALUE)
	    	SELECT D.device_id, 'Cashless Enabled', CASE WHEN MAX(PP.POS_PTA_ID) IS NULL THEN 'N' ELSE 'Y' END
		    FROM DEVICE.DEVICE D
		    LEFT OUTER JOIN PSS.POS P on D.DEVICE_ID=P.DEVICE_ID
		    LEFT OUTER JOIN PSS.POS_PTA PP ON P.POS_ID=PP.POS_ID AND NVL(PP.POS_PTA_ACTIVATION_TS, MAX_DATE)<=SYSDATE AND NVL(PP.POS_PTA_DEACTIVATION_TS, MAX_DATE)>SYSDATE
		    LEFT OUTER JOIN PSS.PAYMENT_SUBTYPE PS ON PP.PAYMENT_SUBTYPE_ID=PS.PAYMENT_SUBTYPE_ID AND PAYMENT_SUBTYPE_CLASS<>'Authority::NOP'
		    LEFT OUTER JOIN REPORT.TRANS_TYPE TT ON PS.TRANS_TYPE_ID=TT.TRANS_TYPE_ID AND TT.CASH_IND='N'
		    WHERE D.DEVICE_ID=l_cur.device_id
		    AND not exists(select 1 from DEVICE.DEVICE_SETTING where device_id=l_cur.device_id and DEVICE_SETTING_PARAMETER_CD='Cashless Enabled')
		    GROUP BY D.device_id, 'Cashless Enabled';
	        
		END LOOP;
		commit;
		l_min_device_id:=l_min_device_id+1000;
	end loop;
END;
/