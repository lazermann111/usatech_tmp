DECLARE
	ln_property_list_version NUMBER := 23;
	ln_device_type_id device_type.device_type_id%TYPE := 13;
	ln_config_template_type_id config_template_type.config_template_type_id%TYPE := 1;
	lv_config_template_name config_template.config_template_name%TYPE := 'DEFAULT-CFG-' || ln_device_type_id || '-' || ln_property_list_version;
	ln_config_template_id config_template.config_template_id%TYPE;
	
	PROCEDURE ADD_DSP(
		pv_index VARCHAR2,
		pv_configurable VARCHAR2,
		pv_name VARCHAR2) 
	IS
	BEGIN
		INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_UI_CONFIGURABLE, DEVICE_SETTING_PARAMETER_NAME) 
		SELECT pv_index, pv_configurable, pv_name
		FROM DUAL 
		WHERE NOT EXISTS (
			SELECT 1 
			FROM DEVICE.DEVICE_SETTING_PARAMETER 
			WHERE DEVICE_SETTING_PARAMETER_CD = pv_index);
	END;

	PROCEDURE ADD_CTS_META(
		pv_PARAMETER_CD VARCHAR2,
		pv_SETTING_VALUE VARCHAR2,
		pn_CATEGORY_ID NUMBER,
		pv_DISPLAY VARCHAR2,
		pv_DESCRIPTION VARCHAR2,
		pv_EDITOR VARCHAR2,
		pn_FIELD_SIZE NUMBER,
		pn_DISPLAY_ORDER NUMBER,
		pv_DATA_MODE VARCHAR2,
		pv_DATA_MODE_AUX VARCHAR2,
		pn_REGEX_ID NUMBER,
		pv_NAME VARCHAR2,
		pv_CUSTOMER_EDITABLE VARCHAR2) 
	IS 
	BEGIN
		INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(
			CONFIG_TEMPLATE_ID,
			DEVICE_SETTING_PARAMETER_CD,
			CONFIG_TEMPLATE_SETTING_VALUE,
			CATEGORY_ID,
			DISPLAY,
			DESCRIPTION,
			EDITOR,
			CONVERTER,
			CLIENT_SEND,
			ALIGN,
			FIELD_SIZE,
			FIELD_OFFSET,
			PAD_CHAR,
			PROPERTY_LIST_VERSION,
			DEVICE_TYPE_ID,
			DISPLAY_ORDER,
			ALT_NAME,
			DATA_MODE,
			DATA_MODE_AUX,
			REGEX_ID,
			ACTIVE,
			NAME,
			CUSTOMER_EDITABLE,
			STORED_IN_PENNIES) 
		SELECT 
			CONFIG_TEMPLATE_ID, 
			pv_PARAMETER_CD,
			pv_SETTING_VALUE,
			pn_CATEGORY_ID,
			pv_DISPLAY,
			pv_DESCRIPTION,
			pv_EDITOR,
			'',
			'Y',
			NULL,
			pn_FIELD_SIZE,
			NULL,
			NULL,
			0,
			DEVICE_TYPE_ID,
			pn_DISPLAY_ORDER,
			NULL,
			pv_DATA_MODE,
			pv_DATA_MODE_AUX,
			pn_REGEX_ID,
			'Y',
			pv_NAME,
			pv_CUSTOMER_EDITABLE,
			'N'
		FROM DEVICE.CONFIG_TEMPLATE CT 
		WHERE CONFIG_TEMPLATE_TYPE_ID = 3 
			AND DEVICE_TYPE_ID = 13 
			AND NOT EXISTS (
				SELECT 1 
				FROM DEVICE.CONFIG_TEMPLATE_SETTING 
				WHERE DEVICE_TYPE_ID = 13 
				AND DEVICE_SETTING_PARAMETER_CD = pv_PARAMETER_CD);
	END;
	
	PROCEDURE ADD_CTS(
		pn_CONFIG_TEMPLATE_ID NUMBER,
		pv_PARAMETER_CD VARCHAR2,
		pv_SETTING_VALUE VARCHAR2) 
	IS
	BEGIN
		INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(
			CONFIG_TEMPLATE_ID, 
			DEVICE_SETTING_PARAMETER_CD, 
			CONFIG_TEMPLATE_SETTING_VALUE) 
		VALUES(pn_CONFIG_TEMPLATE_ID, pv_PARAMETER_CD, pv_SETTING_VALUE);
	END;
	
	PROCEDURE ADD_CTS_REGEX(
		pv_REGEX_NAME VARCHAR2,
		pv_REGEX_DESC VARCHAR2,
		pv_REGEX_VALUE VARCHAR2) 
	IS
	BEGIN
		INSERT INTO DEVICE.CONFIG_TEMPLATE_REGEX(
			REGEX_NAME, 
			REGEX_DESC, 
			REGEX_VALUE) 
		VALUES(pv_REGEX_NAME, pv_REGEX_DESC, pv_REGEX_VALUE);
	END;	
	
BEGIN
	-- add new indexes to device_setting_parameter
	
	ADD_DSP('2015', 'Y', 'Bezel EMV Mode');
	ADD_DSP('2016', 'Y', 'Bezel Language Mode');
	ADD_DSP('2017', 'Y', 'Bezel Init Parameters Visa');
	ADD_DSP('2018', 'Y', 'Bezel Init Parameters PayPass');

	-- add new configurable parameters to config_template_setting meta template
	
	ADD_CTS_META('2015', '0', 43, 'Y', 'Note: enabling Interac Flash will disable VAS. Supported Bezels Include: Vendi and other future capable readers.', 'SELECT:0=0 - MSD/EMV OFF;1=1 - Interac Flash Enabled;2=2 - EMV Enabled', 5, 40, 'A', NULL, 3, 'Bezel EMV Mode', 'N');
	ADD_CTS_META('2016', '1', 43, 'Y', '1 - English, 2 - French, 3 - French AND English {empty line items are replaced with a warning. Example, "French ????", 4 - French OR English {empty items are skipped}. Any properties that hold LCD messages can have French added to them by using the delimiter "|". Messages include: Welcome, Attract, Disable, and Coin Price Labels.', 'SELECT:1=1 - English;2=2 - French;3=3 - French AND English;4=4 - French OR English', 5, 45, 'A', NULL, 3, 'Bezel Language Mode', 'N');
	ADD_CTS_META('2017', null, 43, 'Y', 'Extra TLV items that could override hardcoded settings. Consult Engineering on use.', 'TEXT:0 to 64', 64, 50, 'A', NULL, 19, 'Bezel Init Parameters Visa', 'N');
	ADD_CTS_META('2018', null, 43, 'Y', 'Extra TLV items that could override hardcoded settings. Consult Engineering on use.', 'TEXT:0 to 64', 64, 55, 'A', NULL, 19, 'Bezel Init Parameters PayPass', 'N');

	-- updates to existing config template settings of meta template
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET EDITOR = REPLACE(EDITOR, ' to 16', ' to 33')
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD IN (
	'1203',
	'1204',
	'1205',
	'1206',
	'1527',
	'1528',
	'1507',
	'1509',
	'1511',
	'1513',
	'1515',
	'1517',
	'1519',
	'1521'	
	);
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET DESCRIPTION = DESCRIPTION 
	|| CASE WHEN DESCRIPTION LIKE '%.' THEN ' ' ELSE '. ' END
	|| 'If this device supports French, French can be added by using the form "English|French". Note: The entry of "|" implies to use the device''s internal English/French message."'
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD IN (
	'1203',
	'1204',
	'1205',
	'1206',
	'1527',
	'1528',
	'1507',
	'1509',
	'1511',
	'1513',
	'1515',
	'1517',
	'1519',
	'1521'	
	) AND DESCRIPTION NOT LIKE '%French%';
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET DESCRIPTION = '0 - Auto, enable auto selection of bezel protocol. 1 - Force Gx Protocol, use Gx protocol even if protocol 2 is available. Supported Bezels Include: Vendi.'
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '2000';
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET DESCRIPTION = '0 - No, don''t enable encryption. 1 - Yes, enable encryption if bezel has supported firmware and valid keys. Supported Bezels Include: Vendi and other future DUKPT capable readers.'
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '2001';
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET DESCRIPTION = '0 - VAS or Payment, if there is a pass in the wallet return pass, else return card data. 1 - VAS and Payment, return both pass and card data. 2 - VAS Only, return only pass. 3 - VAS Off, disable VAS protocol. Supported Bezels Include: Vendi and other future VAS capable readers.'
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '2002';
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET EDITOR = 'TEXT:0 to 64', FIELD_SIZE = 64
	WHERE DEVICE_TYPE_ID = 13
	AND DEVICE_SETTING_PARAMETER_CD IN ('2017', '2018');

	-- check if config template exists yet
	
	SELECT MAX(config_template_id) INTO ln_config_template_id
	FROM device.config_template
	WHERE config_template_name = lv_config_template_name;
	
	IF ln_config_template_id IS NULL THEN
		-- create it if not
		INSERT INTO device.config_template(config_template_type_id, config_template_name, device_type_id, property_list_version)
		VALUES(ln_config_template_type_id, lv_config_template_name, ln_device_type_id, ln_property_list_version);
		
		SELECT MAX(config_template_id) INTO ln_config_template_id
		FROM device.config_template
		WHERE config_template_name = lv_config_template_name;
	END IF;
	
	-- remove existing settings for this template, if any
	
	DELETE FROM DEVICE.CONFIG_TEMPLATE_SETTING WHERE CONFIG_TEMPLATE_ID = ln_config_template_id;

	-- add the new parameters to the default template
	
	ADD_CTS(ln_config_template_id, '2015', '0');
	ADD_CTS(ln_config_template_id, '2016', '1');
	ADD_CTS(ln_config_template_id, '2017', NULL);
	ADD_CTS(ln_config_template_id, '2018', NULL);
	
	-- copy all other settings from previous properties list
	INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(CONFIG_TEMPLATE_ID, DEVICE_SETTING_PARAMETER_CD, CONFIG_TEMPLATE_SETTING_VALUE) 
	SELECT ln_config_template_id, DEVICE_SETTING_PARAMETER_CD, CONFIG_TEMPLATE_SETTING_VALUE
	FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS
	JOIN DEVICE.CONFIG_TEMPLATE CT ON CTS.CONFIG_TEMPLATE_ID = CT.CONFIG_TEMPLATE_ID
	WHERE CONFIG_TEMPLATE_NAME = 'DEFAULT-CFG-' || ln_device_type_id || '-' || (ln_property_list_version - 1)
	AND DEVICE_SETTING_PARAMETER_CD NOT IN (
		SELECT DEVICE_SETTING_PARAMETER_CD 
		FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS 
		WHERE CONFIG_TEMPLATE_ID = ln_config_template_id);
		
	-- default value changes
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET CONFIG_TEMPLATE_SETTING_VALUE = '25'
	WHERE CONFIG_TEMPLATE_ID = ln_config_template_id AND DEVICE_SETTING_PARAMETER_CD = '10';

	-- delete if exists and insert the updated aggregated properties list
	
	DELETE FROM DEVICE.DEVICE_TYPE_PROPERTY_LIST
	WHERE device_type_id = ln_device_type_id
	AND property_list_version = ln_property_list_version;

	INSERT INTO DEVICE.DEVICE_TYPE_PROPERTY_LIST(device_type_id, property_list_version, all_property_list_ids, configurable_property_list_ids)
	VALUES(ln_device_type_id, ln_property_list_version, 
		'10|30-35|50-52|60-64|70|80|81|85-87|100-108|200-208|210-213|215|300-302|310-315|400|401|405|406|410|411|415|416|420|421|425|426|430|431|435|436|440-442|444|446|450-451|1001-1028|1101|1103|1104|1107|1110-1114|1200-1207|1300-1303|1400-1415|1500-1528|1600-1603|2000-2014',
		'10|30-35|70|85-87|215|400|401|405|406|410|411|415|416|420|421|425|426|430|431|435|436|440-442|444|446|450-451|1001-1028|1101|1103|1104|1107|1110-1114|1200-1207|1300-1303|1400-1415|1500-1528|1600-1603|2000-2018');

	COMMIT;
END;
/
