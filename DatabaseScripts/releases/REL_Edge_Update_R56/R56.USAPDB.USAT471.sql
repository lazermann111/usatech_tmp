/* All Other Fee Entries */
DECLARE
  l_report_id integer := 191;
BEGIN
  UPDATE report.report_param SET param_value = 'StartDate,EndDate,StartDate,EndDate,StartDate,EndDate,StartDate,EndDate'
    WHERE  report_id = l_report_id and param_name = 'paramNames';

  UPDATE report.report_param SET param_value = 'TIMESTAMP,TIMESTAMP,TIMESTAMP,TIMESTAMP,TIMESTAMP,TIMESTAMP,TIMESTAMP,TIMESTAMP'
    WHERE  report_id = l_report_id and param_name = 'paramTypes';
    
  UPDATE report.report_param SET param_value = q'[SELECT 
lts.entry_type, 
bu.business_unit_name, 
DECODE(bat.payment_schedule_id, 4, 'Y', 'N') monthly_payment, 
cu.currency_code, 
c.customer_name, 
lts.fee_name, 
DECODE(d.status, 
	'S', 'Y', 
	'P', 'Y', 
	'N' 
) in_eft, 
TO_CHAR(TRUNC(lts.entry_date, 'MONTH'),'mm/dd/yyyy') month_for, 
COUNT(1) entry_count, 
SUM (lts.amount) total_entry_amount, 
lts.description 
FROM ( 
	SELECT 
	led.batch_id, 
	led.amount, 
	led.entry_type, 
	led.settle_state_id, 
	led.create_date, 
	led.entry_date, 
	led.description, 
	sfi.fee_name 
	FROM corp.ledger led 
	LEFT OUTER JOIN ( 
		SELECT 
		sf.service_fee_id, 
		f.fee_name 
		FROM corp.service_fees sf 
		JOIN corp.fees f 
		ON f.fee_id = sf.fee_id 
	) sfi 
	ON sfi.service_fee_id = led.service_fee_id 
	WHERE led.entry_type IN ('SF', 'AD', 'SB') 
	AND led.deleted = 'N' 
	AND led.settle_state_id IN (2, 3) 
	AND (led.report_date >= CAST(? AS DATE) AND led.report_date < CAST(? AS DATE) + 1 
	OR led.create_date >= CAST(? AS DATE) AND led.create_date < CAST(? AS DATE) + 1 AND led.report_date IS NULL) 
)lts 
JOIN corp.batch bat 
ON bat.batch_id = lts.batch_id 
JOIN corp.doc d 
ON d.doc_id = bat.doc_id 
JOIN corp.customer_bank cb 
ON cb.customer_bank_id = d.customer_bank_id 
JOIN corp.customer c 
ON c.customer_id = cb.customer_id 
JOIN corp.business_unit bu 
ON bu.business_unit_id = d.business_unit_id 
JOIN corp.currency cu 
ON cu.currency_id = d.currency_id 
JOIN report.trans_state ts 
ON ts.state_id = lts.settle_state_id -- yes, this is non intuitive but correct 
GROUP BY 
lts.entry_type, 
bu.business_unit_name, 
DECODE(bat.payment_schedule_id, 4, 'Y', 'N'), 
cu.currency_code, 
c.customer_name, 
lts.fee_name, 
DECODE(d.status, 
	'S', 'Y', 
	'P', 'Y', 
	'N' 
), 
TRUNC(lts.entry_date, 'MONTH'), 
lts.description
UNION ALL
SELECT
lts.entry_type,
bu.business_unit_name,
DECODE(bat.payment_schedule_id, 4, 'Y', 'N') monthly_payment,
cu.currency_code,
c.customer_name,
lts.fee_name,
DECODE(d.status,
	'S', 'Y',
	'P', 'Y',
	'N'
) in_eft,
TO_CHAR(TRUNC(lts.entry_date, 'MONTH'),'mm/dd/yyyy') month_for,
COUNT(1) entry_count,
SUM (lts.amount) total_entry_amount,
lts.description
FROM (
	SELECT
	led.batch_id,
	led.amount,
	led.entry_type,
	led.settle_state_id,
	led.create_date,
	led.entry_date,
	led.description,
	sfi.fee_name
	FROM report_arch.ledger_arch led
	LEFT OUTER JOIN (
		SELECT
		sf.service_fee_id,
		f.fee_name
		FROM corp.service_fees sf
		JOIN corp.fees f
		ON f.fee_id = sf.fee_id
	) sfi
	ON sfi.service_fee_id = led.service_fee_id
	WHERE led.entry_type IN ('SF', 'AD', 'SB')
	AND led.deleted = 'N'
	AND led.settle_state_id IN (2, 3)
	AND (led.report_date >= CAST(? AS DATE) AND led.report_date < CAST(? AS DATE) + 1
	OR led.create_date >= CAST(? AS DATE) AND led.create_date < CAST(? AS DATE) + 1 AND led.report_date IS NULL)
)lts
JOIN report_arch.batch_arch bat
ON bat.batch_id = lts.batch_id
JOIN report_arch.doc_arch d
ON d.doc_id = bat.doc_id
JOIN corp.customer_bank cb
ON cb.customer_bank_id = d.customer_bank_id
JOIN corp.customer c
ON c.customer_id = cb.customer_id
JOIN corp.business_unit bu
ON bu.business_unit_id = d.business_unit_id
JOIN corp.currency cu
ON cu.currency_id = d.currency_id
JOIN report.trans_state ts
ON ts.state_id = lts.settle_state_id -- yes, this is non intuitive but correct
GROUP BY
lts.entry_type,
bu.business_unit_name,
DECODE(bat.payment_schedule_id, 4, 'Y', 'N'),
cu.currency_code,
c.customer_name,
lts.fee_name,
DECODE(d.status,
	'S', 'Y',
	'P', 'Y',
	'N'
),
TRUNC(lts.entry_date, 'MONTH'),
lts.description]'
    WHERE  report_id = l_report_id and param_name = 'query';  

  COMMIT;
END;
/

/* All Other Fee Entries With Device */
DECLARE
  l_report_id integer := 192;
BEGIN
  UPDATE report.report_param SET param_value = 'StartDate,EndDate,StartDate,EndDate,StartDate,EndDate,StartDate,EndDate'
    WHERE  report_id = l_report_id and param_name = 'paramNames';

  UPDATE report.report_param SET param_value = 'TIMESTAMP,TIMESTAMP,TIMESTAMP,TIMESTAMP,TIMESTAMP,TIMESTAMP,TIMESTAMP,TIMESTAMP'
    WHERE  report_id = l_report_id and param_name = 'paramTypes';
    
  UPDATE report.report_param SET param_value = q'[SELECT lts.entry_type,bu.business_unit_name,DECODE(bat.payment_schedule_id,4,'Y','N') monthly_payment,cu.currency_code,c.customer_name,ee.eport_num,lts.fee_name,DECODE(d.status,'S','Y','P','Y','N') in_eft,TO_CHAR(TRUNC(lts.entry_date,'MONTH'),'mm/dd/yyyy') month_for,COUNT(1) entry_count,SUM(lts.amount) total_entry_amount,lts.description,t.terminal_nbr,t.sales_order_number FROM (SELECT led.batch_id,led.amount,led.entry_type,led.settle_state_id,led.create_date,led.entry_date,led.description,sfi.fee_name FROM corp.ledger led LEFT OUTER JOIN (SELECT sf.service_fee_id,f.fee_name FROM corp.service_fees sf JOIN corp.fees f ON f.fee_id=sf.fee_id) sfi ON sfi.service_fee_id=led.service_fee_id WHERE led.entry_type IN ('SF','AD','SB') AND led.deleted='N' AND led.settle_state_id IN (2,3) AND (led.report_date >= CAST(? AS DATE) AND led.report_date<CAST(? AS DATE)+1 OR led.create_date >= CAST(? AS DATE) AND led.create_date<CAST(? AS DATE)+1 AND led.report_date IS NULL))lts JOIN corp.batch bat ON bat.batch_id=lts.batch_id JOIN corp.doc d ON d.doc_id=bat.doc_id JOIN corp.customer_bank cb ON cb.customer_bank_id=d.customer_bank_id JOIN corp.customer c ON c.customer_id=cb.customer_id JOIN corp.business_unit bu ON bu.business_unit_id=d.business_unit_id JOIN corp.currency cu ON cu.currency_id=d.currency_id JOIN report.trans_state ts ON ts.state_id=lts.settle_state_id LEFT outer JOIN (SELECT DISTINCT TERMINAL_ID,FIRST_VALUE(eport_serial_num) OVER (PARTITION BY TERMINAL_ID ORDER BY start_date DESC) Eport_num from report.terminal_eport te,report.eport e where te.eport_id=e.eport_id) ee ON bat.terminal_id=ee.terminal_id LEFT OUTER JOIN report.terminal t ON bat.terminal_id=t.terminal_id GROUP BY lts.entry_type,bu.business_unit_name,DECODE(bat.payment_schedule_id,4,'Y','N'),cu.currency_code,c.customer_name,lts.fee_name,DECODE(d.status,'S','Y','P','Y','N'),TRUNC(lts.entry_date,'MONTH'),lts.description,ee.Eport_num,t.terminal_nbr,t.sales_order_number UNION ALL SELECT lts.entry_type,bu.business_unit_name,DECODE(bat.payment_schedule_id,4,'Y','N') monthly_payment,cu.currency_code,c.customer_name,ee.eport_num,lts.fee_name,DECODE(d.status,'S','Y','P','Y','N') in_eft,TO_CHAR(TRUNC(lts.entry_date,'MONTH'),'mm/dd/yyyy') month_for,COUNT(1) entry_count,SUM(lts.amount) total_entry_amount,lts.description,t.terminal_nbr,t.sales_order_number FROM (SELECT led.batch_id,led.amount,led.entry_type,led.settle_state_id,led.create_date,led.entry_date,led.description,sfi.fee_name FROM report_arch.ledger_arch led LEFT OUTER JOIN (SELECT sf.service_fee_id,f.fee_name FROM corp.service_fees sf JOIN corp.fees f ON f.fee_id=sf.fee_id) sfi ON sfi.service_fee_id=led.service_fee_id WHERE led.entry_type IN ('SF','AD','SB') AND led.deleted='N' AND led.settle_state_id IN (2,3) AND (led.report_date >= CAST(? AS DATE) AND led.report_date<CAST(? AS DATE)+1 OR led.create_date >= CAST(? AS DATE) AND led.create_date<CAST(? AS DATE)+1 AND led.report_date IS NULL))lts JOIN report_arch.batch_arch bat ON bat.batch_id=lts.batch_id JOIN report_arch.doc_arch d ON d.doc_id=bat.doc_id JOIN corp.customer_bank cb ON cb.customer_bank_id=d.customer_bank_id JOIN corp.customer c ON c.customer_id=cb.customer_id JOIN corp.business_unit bu ON bu.business_unit_id=d.business_unit_id JOIN corp.currency cu ON cu.currency_id=d.currency_id JOIN report.trans_state ts ON ts.state_id=lts.settle_state_id LEFT outer JOIN (SELECT DISTINCT TERMINAL_ID,FIRST_VALUE(eport_serial_num) OVER (PARTITION BY TERMINAL_ID ORDER BY start_date DESC) Eport_num from report.terminal_eport te,report.eport e where te.eport_id=e.eport_id) ee ON bat.terminal_id=ee.terminal_id LEFT OUTER JOIN report.terminal t ON bat.terminal_id=t.terminal_id GROUP BY lts.entry_type,bu.business_unit_name,DECODE(bat.payment_schedule_id,4,'Y','N'),cu.currency_code,c.customer_name,lts.fee_name,DECODE(d.status,'S','Y','P','Y','N'),TRUNC(lts.entry_date,'MONTH'),lts.description,ee.Eport_num,t.terminal_nbr,t.sales_order_number]'
    WHERE  report_id = l_report_id and param_name = 'query';  

  COMMIT;
END;
/

/* EFT Summary 2 Weeks */
DECLARE
  l_report_id integer := 188;
BEGIN
  UPDATE report.report_param SET param_value = q'[SELECT l.entry_type, fn.fee_name service_fee_name, bu.business_unit_name, DECODE(bat.payment_schedule_id, 4, 'Y', 'N') monthly_payment,cu.currency_code,c.customer_name,cb.bank_acct_nbr,cb.bank_routing_nbr,cb.account_title,d.doc_id eft_id,TO_CHAR(d.sent_date, 'mm/dd/yyyy hh:mi:ssAM') eft_date,SUM(l.amount) ledger_amount,d.total_amount eft_total_amount 
		FROM corp.doc d JOIN corp.customer_bank cb ON cb.customer_bank_id = d.customer_bank_id JOIN corp.customer c ON c.customer_id = cb.customer_id JOIN corp.business_unit bu ON bu.business_unit_id = d.business_unit_id JOIN corp.currency cu ON cu.currency_id = d.currency_id JOIN corp.batch bat ON bat.doc_id = d.doc_id JOIN corp.ledger l ON l.batch_id = bat.batch_id 
		LEFT OUTER JOIN ( 
		    SELECT  
		    sf.service_fee_id, 
		    f.fee_name 
		    FROM corp.service_fees sf 
		    JOIN corp.fees f 
		    ON f.fee_id = sf.fee_id 
		) fn 
		ON fn.service_fee_id = l.service_fee_id 
		WHERE sent_date > sysdate -14 
		AND l.deleted = 'N' 
		AND l.settle_state_id IN (2, 3) 
		GROUP BY 
		l.entry_type, 
		fn.fee_name, 
		bu.business_unit_name, 
		DECODE(bat.payment_schedule_id, 4, 'Y', 'N'), 
		cu.currency_code, 
		c.customer_name, 
		cb.bank_acct_nbr, 
		cb.bank_routing_nbr, 
		cb.account_title, 
		d.doc_id, 
		d.sent_date, 
		d.total_amount
UNION ALL
SELECT l.entry_type, fn.fee_name service_fee_name, bu.business_unit_name, DECODE(bat.payment_schedule_id, 4, 'Y', 'N') monthly_payment,cu.currency_code,c.customer_name,cb.bank_acct_nbr,cb.bank_routing_nbr,cb.account_title,d.doc_id eft_id,TO_CHAR(d.sent_date, 'mm/dd/yyyy hh:mi:ssAM') eft_date,SUM(l.amount) ledger_amount,d.total_amount eft_total_amount 
		FROM report_arch.doc_arch d JOIN corp.customer_bank cb ON cb.customer_bank_id = d.customer_bank_id JOIN corp.customer c ON c.customer_id = cb.customer_id JOIN corp.business_unit bu ON bu.business_unit_id = d.business_unit_id JOIN corp.currency cu ON cu.currency_id = d.currency_id JOIN report_arch.batch_arch bat ON bat.doc_id = d.doc_id JOIN report_arch.ledger_arch l ON l.batch_id = bat.batch_id 
		LEFT OUTER JOIN ( 
		    SELECT  
		    sf.service_fee_id, 
		    f.fee_name 
		    FROM corp.service_fees sf 
		    JOIN corp.fees f 
		    ON f.fee_id = sf.fee_id 
		) fn 
		ON fn.service_fee_id = l.service_fee_id 
		WHERE sent_date > sysdate -14 
		AND l.deleted = 'N' 
		AND l.settle_state_id IN (2, 3) 
		GROUP BY 
		l.entry_type, 
		fn.fee_name, 
		bu.business_unit_name, 
		DECODE(bat.payment_schedule_id, 4, 'Y', 'N'), 
		cu.currency_code, 
		c.customer_name, 
		cb.bank_acct_nbr, 
		cb.bank_routing_nbr, 
		cb.account_title, 
		d.doc_id, 
		d.sent_date, 
		d.total_amount]'
    WHERE  report_id = l_report_id and param_name = 'query';  
 
	COMMIT;  
END;
/

/* EFT Summary Accounting */
DECLARE
  l_report_id integer := 189;
BEGIN
  UPDATE report.report_param SET param_value = 'StartDate,EndDate,StartDate,EndDate'
    WHERE  report_id = l_report_id and param_name = 'paramNames';

  UPDATE report.report_param SET param_value = 'TIMESTAMP,TIMESTAMP,TIMESTAMP,TIMESTAMP'
    WHERE  report_id = l_report_id and param_name = 'paramTypes';
    
  UPDATE report.report_param SET param_value = q'[SELECT
		l.entry_type,
		fn.fee_name service_fee_name,
		bu.business_unit_name,
		DECODE(bat.payment_schedule_id, 4, 'Y', 'N') monthly_payment,
		cu.currency_code,
		c.customer_name,
		cb.bank_acct_nbr,
		cb.bank_routing_nbr,
		cb.account_title,
		d.doc_id eft_id,
		TO_CHAR(d.sent_date, 'mm/dd/yyyy hh:mi:ssAM') eft_date,
		SUM(l.amount) ledger_amount,
		d.total_amount eft_total_amount
		FROM corp.doc d
		JOIN corp.customer_bank cb
		ON cb.customer_bank_id = d.customer_bank_id
		JOIN corp.customer c
		ON c.customer_id = cb.customer_id
		JOIN corp.business_unit bu
		ON bu.business_unit_id = d.business_unit_id
		JOIN corp.currency cu
		ON cu.currency_id = d.currency_id
		JOIN corp.batch bat
		ON bat.doc_id = d.doc_id
		JOIN corp.ledger l
		ON l.batch_id = bat.batch_id
		LEFT OUTER JOIN (
			SELECT 
			sf.service_fee_id,
			f.fee_name
			FROM corp.service_fees sf
			JOIN corp.fees f
			ON f.fee_id = sf.fee_id
		) fn
		ON fn.service_fee_id = l.service_fee_id
		WHERE d.sent_date >= CAST(? AS DATE) AND d.sent_date < CAST(? AS DATE) + 1
		AND l.deleted = 'N'
		AND l.settle_state_id IN (2, 3)
		GROUP BY
		l.entry_type,
		fn.fee_name,
		bu.business_unit_name,
		DECODE(bat.payment_schedule_id, 4, 'Y', 'N'),
		cu.currency_code,
		c.customer_name,
		cb.bank_acct_nbr,
		cb.bank_routing_nbr,
		cb.account_title,
		d.doc_id,
		d.sent_date,
		d.total_amount
UNION ALL
SELECT
		l.entry_type,
		fn.fee_name service_fee_name,
		bu.business_unit_name,
		DECODE(bat.payment_schedule_id, 4, 'Y', 'N') monthly_payment,
		cu.currency_code,
		c.customer_name,
		cb.bank_acct_nbr,
		cb.bank_routing_nbr,
		cb.account_title,
		d.doc_id eft_id,
		TO_CHAR(d.sent_date, 'mm/dd/yyyy hh:mi:ssAM') eft_date,
		SUM(l.amount) ledger_amount,
		d.total_amount eft_total_amount
		FROM report_arch.doc d
		JOIN corp.customer_bank cb
		ON cb.customer_bank_id = d.customer_bank_id
		JOIN corp.customer c
		ON c.customer_id = cb.customer_id
		JOIN corp.business_unit bu
		ON bu.business_unit_id = d.business_unit_id
		JOIN corp.currency cu
		ON cu.currency_id = d.currency_id
		JOIN report_arch.batch_arch bat
		ON bat.doc_id = d.doc_id
		JOIN report_arch.ledger_arch l
		ON l.batch_id = bat.batch_id
		LEFT OUTER JOIN (
			SELECT 
			sf.service_fee_id,
			f.fee_name
			FROM corp.service_fees sf
			JOIN corp.fees f
			ON f.fee_id = sf.fee_id
		) fn
		ON fn.service_fee_id = l.service_fee_id
		WHERE d.sent_date >= CAST(? AS DATE) AND d.sent_date < CAST(? AS DATE) + 1
		AND l.deleted = 'N'
		AND l.settle_state_id IN (2, 3)
		GROUP BY
		l.entry_type,
		fn.fee_name,
		bu.business_unit_name,
		DECODE(bat.payment_schedule_id, 4, 'Y', 'N'),
		cu.currency_code,
		c.customer_name,
		cb.bank_acct_nbr,
		cb.bank_routing_nbr,
		cb.account_title,
		d.doc_id,
		d.sent_date,
		d.total_amount]'
    WHERE  report_id = l_report_id and param_name = 'query';  

  COMMIT;
END;
/

/* High Process Fee Transaction Summary With Device */
DECLARE
  l_report_id integer := 214;
BEGIN
  UPDATE report.report_param SET param_value = 'StartDate,EndDate,StartDate,EndDate'
    WHERE  report_id = l_report_id and param_name = 'paramNames';

  UPDATE report.report_param SET param_value = 'TIMESTAMP,TIMESTAMP,TIMESTAMP,TIMESTAMP'
    WHERE  report_id = l_report_id and param_name = 'paramTypes';
    
  UPDATE report.report_param SET param_value = q'[SELECT led.entry_type,bu.business_unit_name,DECODE(t.payment_schedule_id,4,'Y','N') monthly_payment,cr.currency_code,c.customer_name,TO_CHAR(TRUNC(tr.settle_date,'MONTH'),'mm/dd/yyyy') month_for,e.eport_serial_num,COALESCE(pf2.fee_percent,pf.fee_percent) * 100 fee_percent,COUNT(1) tran_count,SUM(tr.total_amount) total_tran_amount,SUM(led.amount) total_ledger_amount FROM report.trans tr JOIN corp.currency cr ON cr.currency_id = tr.currency_id JOIN report.trans_state ts ON ts.state_id = tr.settle_state_id JOIN report.terminal t ON tr.terminal_id = t.terminal_id JOIN corp.customer c ON c.customer_id = t.customer_id JOIN corp.business_unit bu ON bu.business_unit_id = t.business_unit_id JOIN corp.ledger led ON led.trans_id = tr.tran_id AND led.deleted = 'N' JOIN report.eport e on tr.eport_id = e.eport_id JOIN corp.process_fees pf ON t.terminal_id = pf.terminal_id JOIN report.trans_type tt ON tr.trans_type_id = tt.trans_type_id LEFT OUTER JOIN corp.process_fees pf2 ON t.terminal_id = pf2.terminal_id AND tt.process_fee_trans_type_id = pf2.trans_type_id AND (pf2.start_date IS NULL OR pf2.start_date < SYSDATE) AND (pf2.end_date IS NULL OR pf2.end_date > SYSDATE) AND pf2.fee_percent > TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('STANDARD_CREDIT_PROCESS_FEE_PERCENT')) WHERE tt.payable_ind = 'Y' AND tr.settle_state_id IN (2,3) AND led.entry_type IN ('CC','PF','CB','RF') AND tr.settle_date >= CAST(? AS DATE) AND tr.settle_date < CAST(? AS DATE) + 1 AND pf.trans_type_id = 16 AND (pf.start_date IS NULL OR pf.start_date < SYSDATE) AND (pf.end_date IS NULL OR pf.end_date > SYSDATE) AND pf.fee_percent > TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('STANDARD_CREDIT_PROCESS_FEE_PERCENT')) GROUP BY led.entry_type,bu.business_unit_name,DECODE(t.payment_schedule_id,4,'Y','N'),cr.currency_code,c.customer_name,TRUNC(tr.settle_date,'MONTH'),e.eport_serial_num,pf.fee_percent,pf2.fee_percent UNION ALL SELECT led.entry_type,bu.business_unit_name,DECODE(t.payment_schedule_id,4,'Y','N') monthly_payment,cr.currency_code,c.customer_name,TO_CHAR(TRUNC(tr.settle_date,'MONTH'),'mm/dd/yyyy') month_for,e.eport_serial_num,COALESCE(pf2.fee_percent,pf.fee_percent) * 100 fee_percent,COUNT(1) tran_count,SUM(tr.total_amount) total_tran_amount,SUM(led.amount) total_ledger_amount FROM report_arch.trans_arch tr JOIN corp.currency cr ON cr.currency_id = tr.currency_id JOIN report.trans_state ts ON ts.state_id = tr.settle_state_id JOIN report.terminal t ON tr.terminal_id = t.terminal_id JOIN corp.customer c ON c.customer_id = t.customer_id JOIN corp.business_unit bu ON bu.business_unit_id = t.business_unit_id JOIN report_arch.ledger_arch led ON led.trans_id = tr.tran_id AND led.deleted = 'N' JOIN report.eport e on tr.eport_id = e.eport_id JOIN corp.process_fees pf ON t.terminal_id = pf.terminal_id JOIN report.trans_type tt ON tr.trans_type_id = tt.trans_type_id LEFT OUTER JOIN corp.process_fees pf2 ON t.terminal_id = pf2.terminal_id AND tt.process_fee_trans_type_id = pf2.trans_type_id AND (pf2.start_date IS NULL OR pf2.start_date < SYSDATE) AND (pf2.end_date IS NULL OR pf2.end_date > SYSDATE) AND pf2.fee_percent > TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('STANDARD_CREDIT_PROCESS_FEE_PERCENT')) WHERE tt.payable_ind = 'Y' AND tr.settle_state_id IN (2,3) AND led.entry_type IN ('CC','PF','CB','RF') AND tr.settle_date >= CAST ('01-JAN-2016' AS DATE) AND tr.settle_date >= CAST(? AS DATE) AND tr.settle_date < CAST(? AS DATE) + 1 AND pf.trans_type_id = 16 AND (pf.start_date IS NULL OR pf.start_date < SYSDATE) AND (pf.end_date IS NULL OR pf.end_date > SYSDATE) AND pf.fee_percent > TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('STANDARD_CREDIT_PROCESS_FEE_PERCENT')) GROUP BY led.entry_type,bu.business_unit_name,DECODE(t.payment_schedule_id,4,'Y','N'),cr.currency_code,c.customer_name,TRUNC(tr.settle_date,'MONTH'),e.eport_serial_num,pf.fee_percent,pf2.fee_percent ORDER BY customer_name,eport_serial_num,entry_type]'
    WHERE  report_id = l_report_id and param_name = 'query';  

  COMMIT;
END;
/

/* Orphan Device History */
DECLARE
  l_report_id integer := 190;
BEGIN
  UPDATE report.report_param SET param_value = 'EndDate,EndDate'
    WHERE  report_id = l_report_id and param_name = 'paramNames';

  UPDATE report.report_param SET param_value = 'TIMESTAMP,TIMESTAMP'
    WHERE  report_id = l_report_id and param_name = 'paramTypes';
    
  UPDATE report.report_param SET param_value = q'[SELECT 
e.eport_serial_num, 
dt.device_type_desc, 
tt.trans_type_name, 
ts.state_label settle_state, 
NVL2(tr.terminal_id, 'TB', 'T') orphan_type, 
NVL(ssy.source_system_name, '~ Legacy') source_system_name, 
NVL(cr.currency_code, 'USD') currency_code, 
TO_CHAR(TRUNC(tr.settle_date, 'MONTH'), 'mm/dd/yyyy') month_for, 
COUNT(1) tran_count, 
SUM(tr.total_amount) total_amount 
FROM report.trans tr 
JOIN report.eport e 
ON e.eport_id = tr.eport_id 
JOIN report.device_type dt 
ON dt.device_type_id = e.device_type_id 
JOIN report.trans_type tt 
ON tr.trans_type_id = tt.trans_type_id 
JOIN report.trans_state ts 
ON ts.state_id = tr.settle_state_id -- yes, this is non intuituve but correct 
LEFT OUTER JOIN corp.currency cr 
ON cr.currency_id = tr.currency_id 
LEFT OUTER JOIN report.source_system ssy 
ON ssy.source_system_cd = tr.source_system_cd 
WHERE tr.settle_date < TRUNC(CAST(? AS DATE) + 1, 'MONTH') 
AND tt.payable_ind = 'Y' 
AND tr.settle_state_id IN (2, 3) 
AND DECODE(tr.customer_bank_id, NULL, 0) = 0 
GROUP BY 
e.eport_serial_num, 
dt.device_type_desc, 
tt.trans_type_name, 
ts.state_label, 
NVL2(tr.terminal_id, 'TB', 'T'), 
NVL(ssy.source_system_name, '~ Legacy'), 
NVL(cr.currency_code, 'USD'), 
TRUNC(tr.settle_date, 'MONTH') 
UNION ALL
SELECT 
e.eport_serial_num, 
dt.device_type_desc, 
tt.trans_type_name, 
ts.state_label settle_state, 
NVL2(tr.terminal_id, 'TB', 'T') orphan_type, 
NVL(ssy.source_system_name, '~ Legacy') source_system_name, 
NVL(cr.currency_code, 'USD') currency_code, 
TO_CHAR(TRUNC(tr.settle_date, 'MONTH'), 'mm/dd/yyyy') month_for, 
COUNT(1) tran_count, 
SUM(tr.total_amount) total_amount 
FROM report_arch.trans_arch tr 
JOIN report.eport e 
ON e.eport_id = tr.eport_id 
JOIN report.device_type dt 
ON dt.device_type_id = e.device_type_id 
JOIN report.trans_type tt 
ON tr.trans_type_id = tt.trans_type_id 
JOIN report.trans_state ts 
ON ts.state_id = tr.settle_state_id -- yes, this is non intuituve but correct 
LEFT OUTER JOIN corp.currency cr 
ON cr.currency_id = tr.currency_id 
LEFT OUTER JOIN report.source_system ssy 
ON ssy.source_system_cd = tr.source_system_cd 
WHERE tr.settle_date >= CAST ('01-JAN-2016' AS DATE) AND tr.settle_date < TRUNC(CAST(? AS DATE) + 1, 'MONTH') 
AND tt.payable_ind = 'Y' 
AND tr.settle_state_id IN (2, 3) 
AND DECODE(tr.customer_bank_id, NULL, 0) = 0 
GROUP BY 
e.eport_serial_num, 
dt.device_type_desc, 
tt.trans_type_name, 
ts.state_label, 
NVL2(tr.terminal_id, 'TB', 'T'), 
NVL(ssy.source_system_name, '~ Legacy'), 
NVL(cr.currency_code, 'USD'), 
TRUNC(tr.settle_date, 'MONTH') 
ORDER BY 8, 2, 3, 4, 5, 6, 7, 1]'
    WHERE  report_id = l_report_id and param_name = 'query';  

  COMMIT;
END;
/

/* Transaction Processing Entry Summary */
DECLARE
  l_report_id integer := 193;
BEGIN
  UPDATE report.report_param SET param_value = 'StartDate,EndDate,StartDate,EndDate'
    WHERE  report_id = l_report_id and param_name = 'paramNames';

  UPDATE report.report_param SET param_value = 'TIMESTAMP,TIMESTAMP,TIMESTAMP,TIMESTAMP'
    WHERE  report_id = l_report_id and param_name = 'paramTypes';
    
  UPDATE report.report_param SET param_value = q'[SELECT 
led.entry_type,
NVL(dt.business_unit_name, '~ Terminal Orphan') business_unit_name,
NVL(dt.monthly_payment, 'UNK') monthly_payment,
cr.currency_code,
NVL(dt.customer_name, '~ Terminal Orphan') customer_name,
TO_CHAR(TRUNC(DECODE (led.entry_type,
	'CC', tr.settle_date,
	led.ledger_date
), 'MONTH'), 'mm/dd/yyyy') month_for,
COUNT(1) tran_count,
SUM(tr.total_amount) total_tran_amount,
SUM(led.amount) total_ledger_amount,
tr.trans_type_id,
tt.trans_type_name
FROM report.trans tr
JOIN corp.currency cr
ON cr.currency_id = tr.currency_id
JOIN report.trans_state ts
ON ts.state_id = tr.settle_state_id 
LEFT OUTER JOIN (
	SELECT
	t.terminal_id,
	c.customer_name,
	bu.business_unit_name,
	DECODE(t.payment_schedule_id, 4, 'Y', 'N') monthly_payment
	FROM report.terminal t
	JOIN corp.customer c
	ON c.customer_id = t.customer_id
	JOIN corp.business_unit bu
	ON bu.business_unit_id = t.business_unit_id
) dt
ON dt.terminal_id = tr.terminal_id
JOIN corp.ledger led
ON led.trans_id = tr.tran_id
AND led.deleted = 'N'
JOIN report.trans_type tt
ON tr.trans_type_id = tt.trans_type_id
WHERE tr.settle_state_id IN (2, 3) 
AND led.entry_type IN ('CC', 'PF', 'CB', 'RF') 
AND NVL(tr.settle_date, led.create_date) >= CAST(? AS DATE) AND NVL(tr.settle_date, led.create_date) < CAST(? AS DATE) + 1
GROUP BY
led.entry_type,
NVL(dt.business_unit_name, '~ Terminal Orphan'),
NVL(dt.monthly_payment, 'UNK'),
cr.currency_code,
NVL(dt.customer_name, '~ Terminal Orphan'),
TRUNC(DECODE (led.entry_type,
	'CC', tr.settle_date,
	led.ledger_date
), 'MONTH'),
tr.trans_type_id,
tt.trans_type_name
UNION ALL
SELECT 
led.entry_type,
NVL(dt.business_unit_name, '~ Terminal Orphan') business_unit_name,
NVL(dt.monthly_payment, 'UNK') monthly_payment,
cr.currency_code,
NVL(dt.customer_name, '~ Terminal Orphan') customer_name,
TO_CHAR(TRUNC(DECODE (led.entry_type,
	'CC', tr.settle_date,
	led.ledger_date
), 'MONTH'), 'mm/dd/yyyy') month_for,
COUNT(1) tran_count,
SUM(tr.total_amount) total_tran_amount,
SUM(led.amount) total_ledger_amount,
tr.trans_type_id,
tt.trans_type_name
FROM report_arch.trans_arch tr
JOIN corp.currency cr
ON cr.currency_id = tr.currency_id
JOIN report.trans_state ts
ON ts.state_id = tr.settle_state_id 
LEFT OUTER JOIN (
	SELECT
	t.terminal_id,
	c.customer_name,
	bu.business_unit_name,
	DECODE(t.payment_schedule_id, 4, 'Y', 'N') monthly_payment
	FROM report.terminal t
	JOIN corp.customer c
	ON c.customer_id = t.customer_id
	JOIN corp.business_unit bu
	ON bu.business_unit_id = t.business_unit_id
) dt
ON dt.terminal_id = tr.terminal_id
JOIN report_arch.ledger_arch led
ON led.trans_id = tr.tran_id
AND led.deleted = 'N'
JOIN report.trans_type tt
ON tr.trans_type_id = tt.trans_type_id
WHERE tr.settle_state_id IN (2, 3) 
AND led.entry_type IN ('CC', 'PF', 'CB', 'RF') 
AND  NVL(tr.settle_date, led.create_date) >= CAST ('01-JAN-2016' AS DATE) AND NVL(tr.settle_date, led.create_date) >= CAST(? AS DATE) AND NVL(tr.settle_date, led.create_date) < CAST(? AS DATE) + 1
GROUP BY
led.entry_type,
NVL(dt.business_unit_name, '~ Terminal Orphan'),
NVL(dt.monthly_payment, 'UNK'),
cr.currency_code,
NVL(dt.customer_name, '~ Terminal Orphan'),
TRUNC(DECODE (led.entry_type,
	'CC', tr.settle_date,
	led.ledger_date
), 'MONTH'),
tr.trans_type_id,
tt.trans_type_name]'
    WHERE  report_id = l_report_id and param_name = 'query';  

  COMMIT;
END;
/

/* Transaction Processing Entry Summary With Device */
DECLARE
  l_report_id integer := 226;
BEGIN
  UPDATE report.report_param SET param_value = 'StartDate,EndDate,StartDate,EndDate'
    WHERE  report_id = l_report_id and param_name = 'paramNames';

  UPDATE report.report_param SET param_value = 'TIMESTAMP,TIMESTAMP,TIMESTAMP,TIMESTAMP'
    WHERE  report_id = l_report_id and param_name = 'paramTypes';
    
  UPDATE report.report_param SET param_value = q'[SELECT
led.entry_type,
NVL(dt.business_unit_name, '~ Terminal Orphan') business_unit_name,
NVL(dt.monthly_payment, 'UNK') monthly_payment,
cr.currency_code,
NVL(dt.customer_name, '~ Terminal Orphan') customer_name,
TO_CHAR(TRUNC(DECODE (led.entry_type,
	'CC', tr.settle_date,
	led.ledger_date
), 'MONTH'), 'mm/dd/yyyy') month_for,
COUNT(1) tran_count,
SUM(tr.total_amount) total_tran_amount,
SUM(led.amount) total_ledger_amount,
tr.trans_type_id,
tt.trans_type_name,
ee.eport_num
FROM report.trans tr
JOIN corp.currency cr
ON cr.currency_id = tr.currency_id
JOIN report.trans_state ts
ON ts.state_id = tr.settle_state_id
LEFT OUTER JOIN (
	SELECT
	t.terminal_id,
	c.customer_name,
	bu.business_unit_name,
	DECODE(t.payment_schedule_id, 4, 'Y', 'N') monthly_payment
	FROM report.terminal t
	JOIN corp.customer c
	ON c.customer_id = t.customer_id
	JOIN corp.business_unit bu
	ON bu.business_unit_id = t.business_unit_id
) dt
ON dt.terminal_id = tr.terminal_id
JOIN corp.ledger led
ON led.trans_id = tr.tran_id
AND led.deleted = 'N'
JOIN report.trans_type tt
ON tr.trans_type_id = tt.trans_type_id
LEFT OUTER JOIN (
SELECT DISTINCT terminal_id, FIRST_VALUE(eport_serial_num) OVER (PARTITION BY terminal_id ORDER BY start_date DESC) eport_num
FROM report.terminal_eport te
JOIN report.eport e ON te.eport_id = e.eport_id
) ee ON tr.terminal_id = ee.terminal_id
WHERE tr.settle_state_id IN (2, 3)
AND led.entry_type IN ('CC', 'PF', 'CB', 'RF')
AND NVL(tr.settle_date, led.create_date) >= CAST(? AS DATE) AND NVL(tr.settle_date, led.create_date) < CAST(? AS DATE) + 1
GROUP BY
led.entry_type,
NVL(dt.business_unit_name, '~ Terminal Orphan'),
NVL(dt.monthly_payment, 'UNK'),
cr.currency_code,
NVL(dt.customer_name, '~ Terminal Orphan'),
TRUNC(DECODE (led.entry_type,
	'CC', tr.settle_date,
	led.ledger_date
), 'MONTH'),
tr.trans_type_id,
tt.trans_type_name,
ee.eport_num
UNION ALL
SELECT
led.entry_type,
NVL(dt.business_unit_name, '~ Terminal Orphan') business_unit_name,
NVL(dt.monthly_payment, 'UNK') monthly_payment,
cr.currency_code,
NVL(dt.customer_name, '~ Terminal Orphan') customer_name,
TO_CHAR(TRUNC(DECODE (led.entry_type,
	'CC', tr.settle_date,
	led.ledger_date
), 'MONTH'), 'mm/dd/yyyy') month_for,
COUNT(1) tran_count,
SUM(tr.total_amount) total_tran_amount,
SUM(led.amount) total_ledger_amount,
tr.trans_type_id,
tt.trans_type_name,
ee.eport_num
FROM report_arch.trans_arch tr
JOIN corp.currency cr
ON cr.currency_id = tr.currency_id
JOIN report.trans_state ts
ON ts.state_id = tr.settle_state_id
LEFT OUTER JOIN (
	SELECT
	t.terminal_id,
	c.customer_name,
	bu.business_unit_name,
	DECODE(t.payment_schedule_id, 4, 'Y', 'N') monthly_payment
	FROM report.terminal t
	JOIN corp.customer c
	ON c.customer_id = t.customer_id
	JOIN corp.business_unit bu
	ON bu.business_unit_id = t.business_unit_id
) dt
ON dt.terminal_id = tr.terminal_id
JOIN report_arch.ledger_arch led
ON led.trans_id = tr.tran_id
AND led.deleted = 'N'
JOIN report.trans_type tt
ON tr.trans_type_id = tt.trans_type_id
LEFT OUTER JOIN (
SELECT DISTINCT terminal_id, FIRST_VALUE(eport_serial_num) OVER (PARTITION BY terminal_id ORDER BY start_date DESC) eport_num
FROM report.terminal_eport te
JOIN report.eport e ON te.eport_id = e.eport_id
) ee ON tr.terminal_id = ee.terminal_id
WHERE tr.settle_state_id IN (2, 3)
AND led.entry_type IN ('CC', 'PF', 'CB', 'RF')
AND NVL(tr.settle_date, led.create_date) >= CAST ('01-JAN-2016' AS DATE) AND NVL(tr.settle_date, led.create_date) >= CAST(? AS DATE) AND NVL(tr.settle_date, led.create_date) < CAST(? AS DATE) + 1
GROUP BY
led.entry_type,
NVL(dt.business_unit_name, '~ Terminal Orphan'),
NVL(dt.monthly_payment, 'UNK'),
cr.currency_code,
NVL(dt.customer_name, '~ Terminal Orphan'),
TRUNC(DECODE (led.entry_type,
	'CC', tr.settle_date,
	led.ledger_date
), 'MONTH'),
tr.trans_type_id,
tt.trans_type_name,
ee.eport_num]'
    WHERE  report_id = l_report_id and param_name = 'query';  

  COMMIT;
END;
/

/* Unpaid Monies */
DECLARE
  l_report_id integer := 194;
BEGIN
  UPDATE report.report_param SET param_value = 'EndDate,EndDate,EndDate,EndDate'
    WHERE  report_id = l_report_id and param_name = 'paramNames';

  UPDATE report.report_param SET param_value = 'TIMESTAMP,TIMESTAMP,TIMESTAMP,TIMESTAMP'
    WHERE  report_id = l_report_id and param_name = 'paramTypes';
    
  UPDATE report.report_param SET param_value = q'[SELECT
		l.entry_type,
		bu.business_unit_name,
		DECODE(bat.payment_schedule_id, 4, 'Y', 'N') monthly_payment,
		cu.currency_code,
		c.customer_name,
		TO_CHAR(TRUNC(DECODE (l.entry_type,
		    'CC', t.settle_date,
		    l.entry_date
		), 'MONTH'),'mm/dd/yyyy') month_for,
		COUNT(1) tran_count,
		SUM (l.amount) total_ledger_amount
		FROM corp.ledger l
		JOIN corp.batch bat
		ON bat.batch_id = l.batch_id
		JOIN corp.doc d
		ON d.doc_id = bat.doc_id
		JOIN corp.customer_bank cb
		ON cb.customer_bank_id = d.customer_bank_id
		JOIN corp.customer c
		ON c.customer_id = cb.customer_id
		JOIN corp.business_unit bu
		ON bu.business_unit_id = d.business_unit_id
		JOIN corp.currency cu
		ON cu.currency_id = d.currency_id
		JOIN report.trans_state ts
		ON ts.state_id = l.settle_state_id
		LEFT OUTER JOIN report.trans t
		ON t.tran_id = l.trans_id
		WHERE l.deleted = 'N'
		AND d.status != 'D'
		AND l.settle_state_id IN (2, 3)
		AND (d.sent_date IS NULL OR d.sent_date >= TRUNC(CAST(? AS DATE) + 1, 'MONTH'))
		AND NVL(t.settle_date, l.create_date) < TRUNC(CAST(? AS DATE) + 1, 'MONTH')
		GROUP BY
		l.entry_type,
		bu.business_unit_name,
		DECODE(bat.payment_schedule_id, 4, 'Y', 'N'),
		cu.currency_code,
		c.customer_name,
		TRUNC(DECODE (l.entry_type,
		    'CC', t.settle_date,
		    l.entry_date
		), 'MONTH')
UNION ALL
SELECT
		l.entry_type,
		bu.business_unit_name,
		DECODE(bat.payment_schedule_id, 4, 'Y', 'N') monthly_payment,
		cu.currency_code,
		c.customer_name,
		TO_CHAR(TRUNC(DECODE (l.entry_type,
		    'CC', t.settle_date,
		    l.entry_date
		), 'MONTH'),'mm/dd/yyyy') month_for,
		COUNT(1) tran_count,
		SUM (l.amount) total_ledger_amount
		FROM report_arch.ledger_arch l
		JOIN report_arch.batch_arch bat
		ON bat.batch_id = l.batch_id
		JOIN report_arch.doc_arch d
		ON d.doc_id = bat.doc_id
		JOIN corp.customer_bank cb
		ON cb.customer_bank_id = d.customer_bank_id
		JOIN corp.customer c
		ON c.customer_id = cb.customer_id
		JOIN corp.business_unit bu
		ON bu.business_unit_id = d.business_unit_id
		JOIN corp.currency cu
		ON cu.currency_id = d.currency_id
		JOIN report.trans_state ts
		ON ts.state_id = l.settle_state_id
		LEFT OUTER JOIN report.trans t
		ON t.tran_id = l.trans_id
		WHERE l.deleted = 'N'
		AND d.status != 'D'
		AND l.settle_state_id IN (2, 3)
		AND (d.sent_date IS NULL OR d.sent_date >= TRUNC(CAST(? AS DATE) + 1, 'MONTH'))
		AND NVL(t.settle_date, l.create_date) >= CAST ('01-JAN-2016' AS DATE) AND NVL(t.settle_date, l.create_date) < TRUNC(CAST(? AS DATE) + 1, 'MONTH')
		GROUP BY
		l.entry_type,
		bu.business_unit_name,
		DECODE(bat.payment_schedule_id, 4, 'Y', 'N'),
		cu.currency_code,
		c.customer_name,
		TRUNC(DECODE (l.entry_type,
		    'CC', t.settle_date,
		    l.entry_date
		), 'MONTH')]'
    WHERE  report_id = l_report_id and param_name = 'query';  

  COMMIT;
END;
/