--ADD new column to hold full line from DEX file
--including counters flags text etc  
ALTER table G4OP.DEX_FILE_LINE
ADD(LINE_CONTENTS VARCHAR2(512) NULL);

--ADD new column to hold flag indicating if the full line 
--filter should be used when parsing for dex alert codes
ALTER table DEVICE.DEVICE
ADD(DEX_ALERT_LINE_FILTER_IND CHAR(1) DEFAULT 'N' NULL);

DECLARE
	ln_preference_Id NUMBER := 0;
BEGIN 
	--get the next preference id
	SELECT MAX(preference_id) + 1 INTO ln_preference_Id
	FROM REPORT.PREFERENCE;
	
	--New customer level preference
	INSERT INTO REPORT.PREFERENCE (
	PREFERENCE_ID,
	PREFERENCE_LABEL,
	PREFERENCE_CATEGORY,
	PREFERENCE_DESC,
	PREFERENCE_EDITOR,
	PREFERENCE_LEVEL,
	PREFERENCE_DEFAULT)
	SELECT 
	ln_preference_Id,
	'Use alternative DEX alert parsing',
	'Dex Parsing',
	'When this is checked the system will use an alternative form of dex alert parsing designed to only send a notification when the file changes.',
	'CHAR(1)',
	1,
	'N'
	FROM DUAL;
	
	--Update the description
	UPDATE REPORT.REPORTS SET DESCRIPTION = 'Alerts will be sent upon detection. The alert parsing method can be selected on the customer preferences and setup page.' WHERE REPORT_NAME = 'Device Alerts';
	
	COMMIT;
	
END; 
/