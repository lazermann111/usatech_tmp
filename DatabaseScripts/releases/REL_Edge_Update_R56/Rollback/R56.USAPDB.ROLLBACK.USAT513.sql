DELETE FROM ENGINE.APP_SETTING WHERE APP_SETTING_CD IN ('MORE_IP_LOGIN_ATTEMPTS_LIMIT_BEFORE_SOFTLOCK', 'MORE_IP_SOFTLOCK_COOLDOWN');
REVOKE SELECT, INSERT, UPDATE ON ENGINE.LOGIN_FAILS_BY_IP FROM USAT_PREPAID_APP_ROLE;
REVOKE EXECUTE ON ENGINE.PKG_LOGIN_FAILS FROM USAT_PREPAID_APP_ROLE;
REVOKE SELECT ON ENGINE.LOGIN_FAILS_BY_IP FROM USAT_DEV_READ_ONLY;
DROP TRIGGER ENGINE.TRBI_LOGIN_FAILS_BY_IP;
DROP TRIGGER ENGINE.TRBU_LOGIN_FAILS_BY_IP;
DROP TABLE ENGINE.LOGIN_FAILS_BY_IP;
COMMIT;
