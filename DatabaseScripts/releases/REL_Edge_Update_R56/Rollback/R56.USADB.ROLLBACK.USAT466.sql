-- remove column from dex file line table
ALTER TABLE G4OP.DEX_FILE_LINE
  DROP (LINE_CONTENTS);
  
-- remove column from device table  
ALTER TABLE DEVICE.DEVICE
  DROP (DEX_ALERT_LINE_FILTER_IND);
  
--delete the preference
DELETE FROM REPORT.PREFERENCE WHERE PREFERENCE_CATEGORY = 'Dex Parsing';
  
--rollback the description change
UPDATE REPORT.REPORTS SET DESCRIPTION = 'Alerts will be sent upon detection' WHERE REPORT_NAME = 'Device Alerts';
  
  
COMMIT;
/