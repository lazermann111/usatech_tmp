declare
  l_report_ids NUMBER_TABLE := NUMBER_TABLE(134,147,199);
BEGIN
  FOR I IN l_report_ids.FIRST .. l_report_ids.LAST 
  LOOP
    DELETE FROM REPORT.REPORT_PARAM
    WHERE REPORT_ID = l_report_ids(I) AND PARAM_NAME IN ('query', 'params.userId', 'paramTypes', 'paramNames', 'isSQLFolio', 'header');

    INSERT INTO REPORT.REPORT_PARAM (REPORT_ID, PARAM_NAME, PARAM_VALUE) 
      SELECT l_report_ids(i), 'folioId', CASE WHEN l_report_ids(i) in (134,147) THEN 453 WHEN l_report_ids(i) = 199 THEN 1208 END FROM dual 
        WHERE NOT EXISTS (SELECT 1 FROM report.report_param WHERE report_id = l_report_ids(I) AND PARAM_NAME = 'folioId');
  END LOOP;
  
  COMMIT;
END;
/