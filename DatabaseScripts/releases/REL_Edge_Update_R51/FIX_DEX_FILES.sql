declare
  cursor lc_file is 
    select df.dex_file_id, df.file_name, df.eport_id, ft.file_transfer_id, ft.file_transfer_content
    from g4op.dex_file df
    join device.file_transfer ft on ft.file_transfer_id = df.file_transfer_id
    where create_dt > (sysdate - 2)
    and nvl(dbms_lob.getLength(df.file_content), 0) = 0
    and dbms_lob.getLength(ft.file_transfer_content) > 0;
  lv_ft_content VARCHAR2(32767);
  lv_df_content VARCHAR2(32767);
  ln_from NUMBER;
  ln_len NUMBER;
begin
  for lr_file in lc_file loop
    --dbms_output.put_line('Working on ' || lr_file.file_name || ', dex_file_id=' || lr_file.dex_file_id || ', file_transfer_id=' || lr_file.file_transfer_id);
    lv_ft_content := UTL_RAW.CAST_TO_VARCHAR2(DBMS_LOB.SUBSTR(lr_file.file_transfer_content));
    --dbms_output.put_line('v_ft_content=' || v_ft_content);
    
    ln_from := instr(lv_ft_content, 'DXS*');
    if ln_from <= 0 then
      dbms_output.put_line('DXS* not found in file_transfer_id=' || lr_file.file_transfer_id);
      continue;
    end if;
    
    --ln_len := length(lv_ft_content) - ln_from;
    lv_df_content := substr(lv_ft_content, ln_from);
    
    update g4op.dex_file
    set file_content = lv_df_content,
    alert_count = nvl(alert_count, 0),
    exception_count = nvl(exception_count, 0)
    where dex_file_id = lr_file.dex_file_id;
    commit;

    dbms_output.put_line('Fixed dex_file_id ' || lr_file.dex_file_id);
  end loop;
  
  commit;
end;

