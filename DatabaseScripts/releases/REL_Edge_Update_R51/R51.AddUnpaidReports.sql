DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := 441;
	L_CUSTOMER_NAME CORP.CUSTOMER.CUSTOMER_NAME%TYPE:='Pepsi-Cola Hickory NC';
BEGIN
	
	FOR l_cur in (select user_id from report.user_login where customer_id=(select customer_id from corp.customer where customer_name=L_CUSTOMER_NAME) and status='A') LOOP
	LN_REPORT_ID:=LN_REPORT_ID+1;
		INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, USAGE, USER_ID)
		VALUES( LN_REPORT_ID, 'Remaining Unpaid As of Payment{x}', 6, 3, 'Unpaid Reconcile Groups As Of Last Payment', 'U', l_cur.user_id);
		
		insert into report.report_param (report_id,param_name,param_value) values(LN_REPORT_ID, 'outputType','27');
		insert into report.report_param (report_id,param_name,param_value) values(LN_REPORT_ID, 'params.DocId','{x}');
		insert into report.report_param (report_id,param_name,param_value) values(LN_REPORT_ID, 'folioId','441');
		
		commit;
	END LOOP;
END;
/