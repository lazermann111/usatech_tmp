-- set all the cards for the right card type and customer
UPDATE pss.consumer_acct
SET consumer_acct_type_id = 7,
consumer_acct_sub_type_id = 3,
corp_customer_id = 27488
WHERE consumer_acct_identifier BETWEEN 126270 AND 126274;

-- now activate every card with the employee data
CALL pss.pkg_payroll_deduct.activate_consumer_acct(1408643, 'Paul', 'Cowan', 'pwc21@yahoo.com', '12345', 'Southern', 'Engineering', 10, 45773);

-- now set the balance to zero on the unactivated cards so they can't be used
UPDATE pss.consumer_acct
SET consumer_acct_balance = 0
WHERE consumer_acct_id IN (
  SELECT consumer_acct_id
  FROM pss.consumer_acct ca
  JOIN pss.consumer c ON c.consumer_id = ca.consumer_id
  AND ca.consumer_acct_identifier BETWEEN 126270 AND 126274
  and c.consumer_type_id = 4);

commit;
