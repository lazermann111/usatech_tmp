INSERT INTO WEB_CONTENT.SUBDOMAIN(SUBDOMAIN_URL, SUBDOMAIN_DESCRIPTION, APP_CD)
    SELECT SD.SUBDOMAIN_URL || '/premier', 'Premier ' || SUBDOMAIN_DESCRIPTION, 'Prepaid'
      FROM WEB_CONTENT.SUBDOMAIN SD
     WHERE SD.APP_CD = 'Prepaid'
       AND SD.SUBDOMAIN_URL NOT LIKE '%/%'
       AND NOT EXISTS(SELECT 1 FROM WEB_CONTENT.SUBDOMAIN SD0 WHERE SD0.SUBDOMAIN_URL = SD.SUBDOMAIN_URL || '/premier');     

INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, LITERAL_VALUE, SUBDOMAIN_ID)
select 'prepaid-footer-image-title','premier markets', SUBDOMAIN_ID from WEB_CONTENT.SUBDOMAIN where SUBDOMAIN_URL like '%premier%';

INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, LITERAL_VALUE, SUBDOMAIN_ID)
select 'prepaid-new-user-image-title','premier markets', SUBDOMAIN_ID from WEB_CONTENT.SUBDOMAIN where SUBDOMAIN_URL like '%premier%';

INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, LITERAL_VALUE, SUBDOMAIN_ID)
select 'prepaid-title','Premier Markets', SUBDOMAIN_ID from WEB_CONTENT.SUBDOMAIN where SUBDOMAIN_URL like '%premier%';

INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, LITERAL_VALUE, SUBDOMAIN_ID)
select 'prepaid-home-description','Start getting <span class="more-text">more</span> with every purchase at Premier locations like vending machines, kiosks, and much, much more with this ultra-convenient Prepaid '||'&'||' Loyalty program. Simply use your card at participating machines to see the benefits add up!', SUBDOMAIN_ID from WEB_CONTENT.SUBDOMAIN where SUBDOMAIN_URL like '%premier%';

INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, LITERAL_VALUE, SUBDOMAIN_ID)
select 'prepaid-company-name','Premier Food Service', SUBDOMAIN_ID from WEB_CONTENT.SUBDOMAIN where SUBDOMAIN_URL like '%premier%';

INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, LITERAL_VALUE, SUBDOMAIN_ID)
select 'prepaid-company-phone-number','316-269-2447', SUBDOMAIN_ID from WEB_CONTENT.SUBDOMAIN where SUBDOMAIN_URL like '%premier%';

INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, LITERAL_VALUE, SUBDOMAIN_ID)
select 'prepaid-company-email-address','service@pfskansas.com', SUBDOMAIN_ID from WEB_CONTENT.SUBDOMAIN where SUBDOMAIN_URL like '%premier%';

INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, LITERAL_VALUE, SUBDOMAIN_ID)
select 'prepaid-faq-more-text','Premier Markets', SUBDOMAIN_ID from WEB_CONTENT.SUBDOMAIN where SUBDOMAIN_URL like '%premier%';

INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, LITERAL_VALUE, SUBDOMAIN_ID)
select 'prepaid-faq-more-program-text','It is a prepaid and customer loyalty program for the self-service retail marketplace, brought to you thorugh our partnership with USA Technologies, the industry leader in secure unattended, cashless transactions and consumer engagement services. The Premier Markets reloadable card gives you another way to pay at some of your favorite self-service retail terminals and can earn you discounts on future purchases, access to product information and more.', SUBDOMAIN_ID from WEB_CONTENT.SUBDOMAIN where SUBDOMAIN_URL like '%premier%';

INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, LITERAL_VALUE, SUBDOMAIN_ID)
select 'prepaid-faq-more-program-why-text','Partners like Premier Markets wanted to provide more value to their customers and reward them for their repeat business.', SUBDOMAIN_ID from WEB_CONTENT.SUBDOMAIN where SUBDOMAIN_URL like '%premier%';

INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, LITERAL_VALUE, SUBDOMAIN_ID)
select 'prepaid-company-address-line1','8225 W Irving St.', SUBDOMAIN_ID from WEB_CONTENT.SUBDOMAIN where SUBDOMAIN_URL like '%premier%';


INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, LITERAL_VALUE, SUBDOMAIN_ID)
select 'prepaid-company-address-line2','Wichita KS 67209', SUBDOMAIN_ID from WEB_CONTENT.SUBDOMAIN where SUBDOMAIN_URL like '%premier%';

INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, LITERAL_VALUE, SUBDOMAIN_ID)
select 'prepaid-company-website-url','http://www.pfskansas.com/', SUBDOMAIN_ID from WEB_CONTENT.SUBDOMAIN where SUBDOMAIN_URL like '%premier%';

commit;

update corp.customer set branding_subdirectory='/premier' where customer_name='Premier Food Service' and status='A';
commit;