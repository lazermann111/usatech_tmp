DECLARE
	ln_property_list_version NUMBER := 19;
	ln_device_type_id device_type.device_type_id%TYPE := 13;
	ln_config_template_type_id config_template_type.config_template_type_id%TYPE := 1;
	lv_config_template_name config_template.config_template_name%TYPE := 'DEFAULT-CFG-' || ln_device_type_id || '-' || ln_property_list_version;
	ln_config_template_id config_template.config_template_id%TYPE;
	lv_db_name VARCHAR2(255);
  lv_private_ip VARCHAR(255);
  lv_public_ip VARCHAR2(255);
	
	PROCEDURE ADD_DSP(
		pv_index VARCHAR2,
		pv_name VARCHAR2) 
	IS
	BEGIN
		INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_UI_CONFIGURABLE, DEVICE_SETTING_PARAMETER_NAME) 
		SELECT pv_index, 'Y', pv_name
		FROM DUAL 
		WHERE NOT EXISTS (
			SELECT 1 
			FROM DEVICE.DEVICE_SETTING_PARAMETER 
			WHERE DEVICE_SETTING_PARAMETER_CD = pv_index);
	END;
  
	PROCEDURE ADD_CTS_META(
		pv_PARAMETER_CD VARCHAR2,
		pv_SETTING_VALUE VARCHAR2,
		pn_CATEGORY_ID NUMBER,
		pv_DISPLAY VARCHAR2,
		pv_DESCRIPTION VARCHAR2,
		pv_EDITOR VARCHAR2,
		pn_FIELD_SIZE NUMBER,
		pn_DISPLAY_ORDER NUMBER,
		pv_DATA_MODE VARCHAR2,
		pv_DATA_MODE_AUX VARCHAR2,
		pn_REGEX_ID NUMBER,
		pv_NAME VARCHAR2) 
	IS 
	BEGIN
		INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(
			CONFIG_TEMPLATE_ID,
			DEVICE_SETTING_PARAMETER_CD,
			CONFIG_TEMPLATE_SETTING_VALUE,
			CATEGORY_ID,
			DISPLAY,
			DESCRIPTION,
			EDITOR,
			CONVERTER,
			CLIENT_SEND,
			ALIGN,
			FIELD_SIZE,
			FIELD_OFFSET,
			PAD_CHAR,
			PROPERTY_LIST_VERSION,
			DEVICE_TYPE_ID,
			DISPLAY_ORDER,
			ALT_NAME,
			DATA_MODE,
			DATA_MODE_AUX,
			REGEX_ID,
			ACTIVE,
			NAME,
			CUSTOMER_EDITABLE,
			STORED_IN_PENNIES) 
		SELECT 
			CONFIG_TEMPLATE_ID, 
			pv_PARAMETER_CD,
			pv_SETTING_VALUE,
			pn_CATEGORY_ID,
			pv_DISPLAY,
			pv_DESCRIPTION,
			pv_EDITOR,
			'',
			'Y',
			NULL,
			pn_FIELD_SIZE,
			NULL,
			NULL,
			0,
			DEVICE_TYPE_ID,
			pn_DISPLAY_ORDER,
			NULL,
			pv_DATA_MODE,
			pv_DATA_MODE_AUX,
			pn_REGEX_ID,
			'Y',
			pv_NAME,
			'N',
			'N'
		FROM DEVICE.CONFIG_TEMPLATE CT 
		WHERE CONFIG_TEMPLATE_TYPE_ID = 3 
			AND DEVICE_TYPE_ID = 13 
			AND NOT EXISTS (
				SELECT 1 
				FROM DEVICE.CONFIG_TEMPLATE_SETTING 
				WHERE DEVICE_TYPE_ID = 13 
				AND DEVICE_SETTING_PARAMETER_CD = pv_PARAMETER_CD);
	END;
	
	PROCEDURE ADD_CTS(
		pn_CONFIG_TEMPLATE_ID NUMBER,
		pv_PARAMETER_CD VARCHAR2,
		pv_SETTING_VALUE VARCHAR2) 
	IS
	BEGIN
		INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(
			CONFIG_TEMPLATE_ID, 
			DEVICE_SETTING_PARAMETER_CD, 
			CONFIG_TEMPLATE_SETTING_VALUE) 
		VALUES(pn_CONFIG_TEMPLATE_ID, pv_PARAMETER_CD, pv_SETTING_VALUE);
	END;
  
BEGIN
	-- add new indexes to device_setting_parameter
	ADD_DSP('400','COMS: Auth IP Address Private');
	ADD_DSP('401','COMS: Auth IP Address Private Backup');
	ADD_DSP('405','COMS: Auth IP Address Public');
	ADD_DSP('406','COMS: Auth IP Address Public Backup');
	ADD_DSP('410','COMS: Auth IP Port Private');
	ADD_DSP('411','COMS: Auth IP Port Private Backup');
	ADD_DSP('415','COMS: Auth IP Port Public');
	ADD_DSP('416','COMS: Auth IP Port Public');
	ADD_DSP('420','COMS: Misc IP Address Private');
	ADD_DSP('421','COMS: Misc IP Address Private Backup');
	ADD_DSP('425','COMS: Misc IP Address Public');
	ADD_DSP('426','COMS: Misc IP Address Public Backup');
	ADD_DSP('430','COMS: Misc IP Port Private');
	ADD_DSP('431','COMS: Misc IP Port Private Backup');
	ADD_DSP('435','COMS: Misc IP Port Public');
	ADD_DSP('436','COMS: Misc IP Port Public Backup');
	ADD_DSP('440','COMS: SIM ATT APN PRIMARY');
	ADD_DSP('441','COMS: SIM ATT APN SECONDARY');
	ADD_DSP('442','COMS: SIM ROGERS APN');
	ADD_DSP('444','COMS: SIM ESEYE APN');
	ADD_DSP('446','COMS: SIM VERZION APN');
	ADD_DSP('450','COMS: MIP SELECTION');
	ADD_DSP('1110','DEX Setting Alert Interval Seconds');
	ADD_DSP('1111','DEX Setting Retries-	Number of Attempts');
	ADD_DSP('1112','DEX Setting Retries- Interval Minutes ');
	ADD_DSP('1113','DEX Setting Reads- Number of Plug-In Attempts');
	ADD_DSP('1114','DEX Setting Reads- Number of ENQs');
	
	-- add new indexes to config_template_setting meta template
	ADD_CTS_META('400', NULL, 3, 'Y', 'Primary private IP address (IPV4) used for authorizations. Please do not change without engineering guidance.', 'TEXT:7 to 15', 15, 1, 'A', NULL, 4, 'COMS: Auth IP Address Private');
	ADD_CTS_META('401', NULL, 3, 'Y', 'Secondary private IP address (IPV4) used for authorizations. Please do not change without engineering guidance.', 'TEXT:7 to 15', 15, 2, 'A', NULL, 4, 'COMS: Auth IP Address Private Backup');
	ADD_CTS_META('405', NULL, 3, 'Y', 'Primary public IP address (IPV4) used for authorizations. Please do not change without engineering guidance.', 'TEXT:7 to 15', 15, 3, 'A', NULL, 4, 'COMS: Auth IP Address Public');
	ADD_CTS_META('406', NULL, 3, 'Y', 'Secondary public IP address (IPV4) used for authorizations. Please do not change without engineering guidance.', 'TEXT:7 to 15', 15, 4, 'A', NULL, 4, 'COMS: Auth IP Address Public Backup');
	ADD_CTS_META('410', '14109', 3, 'Y', 'Primary private IP port number used for authorizations. Please do not change without engineering guidance.', 'TEXT:1 to 5', 5, 5, 'A', 'N', 3, 'COMS: Auth IP Port Private');
	ADD_CTS_META('411', '14109', 3, 'Y', 'Secondary private IP port number used for authorizations. Please do not change without engineering guidance.', 'TEXT:1 to 5', 5, 6, 'A', 'N', 3, 'COMS: Auth IP Port Private Backup');
	ADD_CTS_META('415', '14109', 3, 'Y', 'Primary public IP port number used for authorizations. Please do not change without engineering guidance.', 'TEXT:1 to 5', 5, 7, 'A', 'N', 3, 'COMS: Auth IP Port Public');
	ADD_CTS_META('416', '14109', 3, 'Y', 'Primary public IP port number used for authorizations. Please do not change without engineering guidance.', 'TEXT:1 to 5', 5, 8, 'A', 'N', 3, 'COMS: Auth IP Port Public');
	ADD_CTS_META('420', NULL, 3, 'Y', 'Primary private IP address (IPV4) used for miscellaneous communications. Please do not change without engineering guidance.', 'TEXT:7 to 15', 15, 10, 'A', NULL, 4, 'COMS: Misc IP Address Private');
	ADD_CTS_META('421', NULL, 3, 'Y', 'Secondary private IP address (IPV4) used for miscellaneous communications. Please do not change without engineering guidance.', 'TEXT:7 to 15', 15, 11, 'A', NULL, 4, 'COMS: Misc IP Address Private Backup');
	ADD_CTS_META('425', NULL, 3, 'Y', 'Primary public IP address (IPV4) used for miscellaneous communications. Please do not change without engineering guidance.', 'TEXT:7 to 15', 15, 12, 'A', NULL, 4, 'COMS: Misc IP Address Public');
	ADD_CTS_META('426', NULL, 3, 'Y', 'Secondary public IP address (IPV4) used for miscellaneous communications. Please do not change without engineering guidance.', 'TEXT:7 to 15', 15, 13, 'A', NULL, 4, 'COMS: Misc IP Address Public Backup');
	ADD_CTS_META('430', '14108', 3, 'Y', 'Primary private IP port number used for miscellaneous communications. Please do not change without engineering guidance.', 'TEXT:1 to 5', 5, 14, 'A', 'N', 3, 'COMS: Misc IP Port Private');
	ADD_CTS_META('431', '14108', 3, 'Y', 'Secondary private IP port number used for miscellaneous communications. Please do not change without engineering guidance.', 'TEXT:1 to 5', 5, 15, 'A', 'N', 3, 'COMS: Misc IP Port Private Backup');
	ADD_CTS_META('435', '14108', 3, 'Y', 'Primary public IP port number used for miscellaneous communications. Please do not change without engineering guidance.', 'TEXT:1 to 5', 5, 16, 'A', 'N', 3, 'COMS: Misc IP Port Public');
	ADD_CTS_META('436', '14108', 3, 'Y', 'Secondary public IP port number used for miscellaneous communications. Please do not change without engineering guidance.', 'TEXT:1 to 5', 5, 17, 'A', 'N', 3, 'COMS: Misc IP Port Public Backup');
	ADD_CTS_META('440', NULL, 3, 'Y', 'The cell network''s (MNO) primary APN setting. Please do not change without engineering guidance.', 'TEXT:5 to 31', 31, 18, 'A', NULL, 19, 'COMS: SIM ATT APN PRIMARY');
	ADD_CTS_META('441', NULL, 3, 'Y', 'The cell network''s (MNO) secondary APN setting. Please do not change without engineering guidance.', 'TEXT:5 to 31', 31, 19, 'A', NULL, 19, 'COMS: SIM ATT APN SECONDARY');
	ADD_CTS_META('442', NULL, 3, 'Y', 'The cell network''s (MNO) primary APN setting. Please do not change without engineering guidance.', 'TEXT:5 to 31', 31, 20, 'A', NULL, 19, 'COMS: SIM ROGERS APN');
	ADD_CTS_META('444', NULL, 3, 'Y', 'The cell network''s (MNO) primary APN setting. Please do not change without engineering guidance.', 'TEXT:5 to 31', 31, 21, 'A', NULL, 19, 'COMS: SIM ESEYE APN');
	ADD_CTS_META('446', NULL, 3, 'Y', 'The cell network''s (MNO) primary APN setting. Please do not change without engineering guidance.', 'TEXT:5 to 31', 31, 22, 'A', NULL, 19, 'COMS: SIM VERZION APN');
	ADD_CTS_META('450', '2', 3, 'Y', 'Some cell modems, like CDMA, support different ways to obtain internet access. Simple IP (SIP) where internet access is obtained locally from cell tower. MIP (Mobile IP) where the internet is obtained from the network infrastructure. MIP is required to use the USA VPN. Please do not change without engineering guidance.', 'SELECT:0=0 - SIP_ONLY;1=1 - MIP with SIP Fallback;2=2 - MIP_ONLY', 1, 23, 'A', 'N', 3, 'COMS: MIP SELECTION');
	ADD_CTS_META('1110', '0', 5, 'Y', 'How often to poll the VMC to determine whether or not it requires the ePort to read new DEX file and upload it to the server. Set to 0 to disable polling.	Default is 0. Supersedes 1106. Minimum value is 10sec.', 'TEXT:1 to 16', 16, 11, 'A', NULL, 3, 'DEX Setting Alert Interval Seconds');
	ADD_CTS_META('1111', '4', 5, 'Y', 'Number of retries for unattended DEX reads (examples Scheduled DEX and Alert DEX). Set to 0 to disable retries. Default is 4. Note 4 means 5 reads max before uploading file to the server.', 'TEXT:1 to 8', 8, 12, 'A', NULL, 3, 'DEX Setting Retries-	Number of Attempts');
	ADD_CTS_META('1112', '2', 5, 'Y', 'Number of minutes between Retries. Min value is 1. Default 2.', 'TEXT:1 to 8', 8, 13, 'A', NULL, 3, 'DEX Setting Retries- Interval Minutes ');
	ADD_CTS_META('1113', '2', 5, 'Y', 'The G9 simulates a hand held device. This value represents the number of simulated plug-in attempts of a handheld. This value should only be changed with engineering guidance. Min value is 1. Default 2.', 'TEXT:1 to 8', 8, 14, 'A', NULL, 3, 'DEX Setting Reads- Number of Plug-In Attempts');
	ADD_CTS_META('1114', '10', 5, 'Y', 'The G9 attempts communication with a VMC by sending the ENQ control character. Some vendors, like ones equipped with Bill recyclers, require more ENQs to be sent to wake it up. This value should only be changed with engineering guidance. Min value is 5. Default is 10..', 'TEXT:1 to 8', 8, 15, 'A', NULL, 3, 'DEX Setting Reads- Number of ENQs');

	-- updates to existing config template settings of meta template
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET DESCRIPTION = 'Number of attempts before quitting DEX polling. Use Property 1113 if available as 1113 supersedes 1102.' 
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '1102'; 
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET DESCRIPTION = 'Schedule for DEX Query Alert. Please refer to the format specified for Scheduled Settlement. Use Property 1110 if available as 1110 supersedes 1106.' 
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '1106'; 
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET DISPLAY_ORDER = 24 
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '30';
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET DISPLAY_ORDER = 25
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '31';
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET DISPLAY_ORDER = 26 
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '32';
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET DISPLAY_ORDER = 27 
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '33';

	-- check if config template exists yet
	SELECT MAX(config_template_id) INTO ln_config_template_id
	FROM device.config_template
	WHERE config_template_name = lv_config_template_name;
	
	IF ln_config_template_id IS NULL THEN
		-- create it if not
		INSERT INTO device.config_template(config_template_type_id, config_template_name, device_type_id, property_list_version)
		VALUES(ln_config_template_type_id, lv_config_template_name, ln_device_type_id, ln_property_list_version);
		
		SELECT MAX(config_template_id) INTO ln_config_template_id
		FROM device.config_template
		WHERE config_template_name = lv_config_template_name;
	END IF;
	
	-- remove existing settings for this template, if any
	DELETE FROM DEVICE.CONFIG_TEMPLATE_SETTING WHERE CONFIG_TEMPLATE_ID = ln_config_template_id;
  
	SELECT REPLACE(GLOBAL_NAME, '.WORLD', '') 
	INTO lv_db_name 
	FROM GLOBAL_NAME;

  select DECODE(lv_db_name, 'USAPDB', '192.168.79.163', 'ECCDB', '192.168.79.61', 'USADEV02', '192.168.79.245', '192.168.79.245')
  into lv_private_ip
  from dual;
	
  select DECODE(lv_db_name, 'USAPDB', '208.116.216.163', 'ECCDB', '65.207.56.61', 'USADEV02', '65.207.56.245', '65.207.56.245')
  into lv_public_ip
  from dual;

	-- add the new settings
	ADD_CTS(ln_config_template_id, '400', lv_private_ip);
	ADD_CTS(ln_config_template_id, '401', lv_private_ip);
	ADD_CTS(ln_config_template_id, '405', lv_public_ip);
	ADD_CTS(ln_config_template_id, '406', lv_public_ip);
	ADD_CTS(ln_config_template_id, '410', '14109');
	ADD_CTS(ln_config_template_id, '411', '14109');
	ADD_CTS(ln_config_template_id, '415', '14109');
	ADD_CTS(ln_config_template_id, '416', '14109');
	ADD_CTS(ln_config_template_id, '420', lv_private_ip);
	ADD_CTS(ln_config_template_id, '421', lv_private_ip);
	ADD_CTS(ln_config_template_id, '425', lv_public_ip);
	ADD_CTS(ln_config_template_id, '426', lv_public_ip);
	ADD_CTS(ln_config_template_id, '430', '14108');
	ADD_CTS(ln_config_template_id, '431', '14108');
	ADD_CTS(ln_config_template_id, '435', '14108');
	ADD_CTS(ln_config_template_id, '436', '14108');
	ADD_CTS(ln_config_template_id, '440', 'apn01.usatech.com');
	ADD_CTS(ln_config_template_id, '441', 'apn02.usatech.com');
	ADD_CTS(ln_config_template_id, '442', 'INTERNET.COM');
	ADD_CTS(ln_config_template_id, '444', 'eseye.com');
	ADD_CTS(ln_config_template_id, '446', 'usat.gw14.vzwentp');
	ADD_CTS(ln_config_template_id, '450', '2');
	ADD_CTS(ln_config_template_id, '1110', '0');
	ADD_CTS(ln_config_template_id, '1111', '4');
	ADD_CTS(ln_config_template_id, '1112', '2');
	ADD_CTS(ln_config_template_id, '1113', '2');
	ADD_CTS(ln_config_template_id, '1114', '10');
	
	-- copy all other settings from previous properties list
	INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(CONFIG_TEMPLATE_ID, DEVICE_SETTING_PARAMETER_CD, CONFIG_TEMPLATE_SETTING_VALUE) 
	SELECT ln_config_template_id, DEVICE_SETTING_PARAMETER_CD, CONFIG_TEMPLATE_SETTING_VALUE
	FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS
	JOIN DEVICE.CONFIG_TEMPLATE CT ON CTS.CONFIG_TEMPLATE_ID = CT.CONFIG_TEMPLATE_ID
	WHERE CONFIG_TEMPLATE_NAME = 'DEFAULT-CFG-' || ln_device_type_id || '-' || (ln_property_list_version - 1)
	AND DEVICE_SETTING_PARAMETER_CD NOT IN (
		SELECT DEVICE_SETTING_PARAMETER_CD 
		FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS 
		WHERE CONFIG_TEMPLATE_ID = ln_config_template_id)
	-- and don't copy settings we're removing for this version
	AND DEVICE_SETTING_PARAMETER_CD NOT IN ('0', '1', '2', '3', '4', '5', '6', '7', '20', '21', '22', '23', '24', '25', '26', '27', '71', '1102', '1106');

	DELETE FROM DEVICE.DEVICE_TYPE_PROPERTY_LIST
	WHERE device_type_id = ln_device_type_id
		AND property_list_version = ln_property_list_version;
	
	INSERT INTO DEVICE.DEVICE_TYPE_PROPERTY_LIST(device_type_id, property_list_version, all_property_list_ids, configurable_property_list_ids)
	VALUES(ln_device_type_id, ln_property_list_version, 
		'10|30-33|50-52|60-64|70|80|81|85-87|100-108|200-208|210-213|215|300|301|400|401|405|406|410|411|415|416|420|421|425|426|430|431|435|436|440-442|444|446|450|1001-1026|1101|1103|1104|1107|1110-1114|1200-1207|1300-1303|1400-1415|1500-1528',
		'10|30-33|70|85-87|215|400|401|405|406|410|411|415|416|420|421|425|426|430|431|435|436|440-442|444|446|450|1001-1026|1101|1103|1104|1107|1110-1114|1200-1207|1300-1303|1400-1415|1500-1528');
END;
/
