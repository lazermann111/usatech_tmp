GRANT EXECUTE ON PSS.PKG_PAYROLL_DEDUCT TO USALIVE_APP_ROLE;
GRANT EXECUTE ON PSS.PKG_PAYROLL_DEDUCT TO USAT_DMS_ROLE;
GRANT EXECUTE ON PSS.PKG_PAYROLL_DEDUCT TO USAT_POSM_ROLE;

GRANT SELECT ON CORP.CUSTOMER TO USAT_RPT_REQ_ROLE;

insert into corp.frequency(frequency_id, name, interval, months, date_field, amount, offset_ms, days)
values (12, 'Pay Period', 'DD', 0, 'PAY_PERIOD', 1, 0, 1);

INSERT INTO PSS.POS_PTA_TMPL(POS_PTA_TMPL_NAME, POS_PTA_TMPL_DESC)
SELECT 'SPECIAL SETUP: Payroll Deduct', 'Template to accept payroll deduct cards' 
  FROM DUAL
  WHERE NOT EXISTS(SELECT 1 FROM PSS.POS_PTA_TMPL E WHERE POS_PTA_TMPL_NAME = 'SPECIAL SETUP: Payroll Deduct');

INSERT INTO PSS.POS_PTA_TMPL_ENTRY(POS_PTA_TMPL_ID, PAYMENT_SUBTYPE_ID, POS_PTA_ACTIVATION_OSET_HR, PAYMENT_SUBTYPE_KEY_ID, POS_PTA_PRIORITY, CURRENCY_CD, AUTHORITY_PAYMENT_MASK_ID)
SELECT PT.POS_PTA_TMPL_ID, PS.PAYMENT_SUBTYPE_ID, 0, PS.payment_subtype_key_id, 1, 'USD', PS.AUTHORITY_PAYMENT_MASK_ID
FROM PSS.POS_PTA_TMPL PT
CROSS JOIN (
  select psd.payment_subtype_id, psd.payment_subtype_key_id, psd.authority_payment_mask_id
  from APP_LAYER.VW_PAYMENT_SUBTYPE_DETAIL psd
  where psd.payment_subtype_class = 'Internal'
  and psd.card_type = 'Payroll Deduct'
  and psd.authority_service_type_id = 1) PS
WHERE POS_PTA_TMPL_NAME = 'SPECIAL SETUP: Payroll Deduct'
AND NOT EXISTS (
  select *
  from pss.pos_pta_tmpl_entry te
  join pss.pos_pta_tmpl t on t.pos_pta_tmpl_id = te.pos_pta_tmpl_id
  WHERE POS_PTA_TMPL_NAME = 'SPECIAL SETUP: Payroll Deduct');

commit;
