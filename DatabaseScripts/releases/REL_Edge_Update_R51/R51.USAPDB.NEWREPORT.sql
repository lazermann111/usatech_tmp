DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := -1;
	LV_REPORT_NAME VARCHAR(50) := 'Devices with low sales';
	LN_WEB_LINK_ID web_content.web_link.WEB_LINK_ID%TYPE;
BEGIN
	
	SELECT MAX(REPORT_ID) INTO LN_REPORT_ID
	FROM REPORT.REPORTS WHERE REPORT_NAME = LV_REPORT_NAME;

	IF LN_REPORT_ID > 0 THEN
		RETURN;
	ELSE
		LN_REPORT_ID:=436;
	END IF;
		
	INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID, LATENCY)
	SELECT LN_REPORT_ID, 'Devices with low sales - {b} to {e}', 6, 0, 'Devices with low sales', 'Devices with low sales that reports devices with sales less than $10 for the date range. Columns include:Customer Name, Location, Device Type, Device, Amount', 'E', 0, null
	FROM DUAL;

	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
	VALUES(LN_REPORT_ID, 'query', 'SELECT
	c.CUSTOMER_NAME "Customer Name",
	l.LOCATION_NAME "Location",
	dt.DEVICE_TYPE_DESC "Device Type",
	e.EPORT_SERIAL_NUM "Device",
	coalesce(SUM(tsbd.TRAN_AMOUNT),0) "Amount"
	FROM CORP.CUSTOMER c
	    JOIN REPORT.TERMINAL t ON t.CUSTOMER_ID = c.CUSTOMER_ID AND t.STATUS != ''D''
	    JOIN REPORT.VW_TERMINAL_EPORT te ON t.TERMINAL_ID = te.TERMINAL_ID
	    JOIN REPORT.EPORT e ON e.EPORT_ID = te.EPORT_ID
	    LEFT OUTER JOIN (DEVICE.DEVICE d
	    LEFT OUTER JOIN DEVICE.COMM_METHOD cm ON d.COMM_METHOD_CD = cm.COMM_METHOD_CD) ON e.EPORT_SERIAL_NUM = d.DEVICE_SERIAL_CD AND d.DEVICE_ACTIVE_YN_FLAG = ''Y''
	    LEFT OUTER JOIN DEVICE.DEVICE_TYPE dt on d.DEVICE_TYPE_ID=dt.DEVICE_TYPE_ID
	    JOIN REPORT.LOCATION l ON t.LOCATION_ID = l.LOCATION_ID
	    JOIN REPORT.VW_USER_TERMINAL vut ON t.TERMINAL_ID = vut.TERMINAL_ID AND vut.TERMINAL_ID != 0
	    JOIN REPORT.USER_LOGIN ul ON ul.USER_ID = vut.USER_ID
	    LEFT OUTER JOIN REPORT.TRANS_STAT_BY_DAY tsbd ON tsbd.EPORT_ID = e.EPORT_ID and tsbd.TERMINAL_ID = t.TERMINAL_ID and tsbd.TRAN_DATE >= CAST(? AS DATE) and tsbd.TRAN_DATE< CAST(? AS DATE) + 1
	WHERE vut.USER_ID=?
	GROUP BY c.CUSTOMER_NAME,l.LOCATION_NAME,dt.DEVICE_TYPE_DESC,e.EPORT_SERIAL_NUM
	having coalesce(SUM(tsbd.TRAN_AMOUNT),0) < ?
	ORDER BY c.CUSTOMER_NAME,l.LOCATION_NAME,dt.DEVICE_TYPE_DESC,e.EPORT_SERIAL_NUM');
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'header', 'true');
	insert into report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) values(LN_REPORT_ID, 'dateFormat', 'MM/dd/yyyy');
	insert into report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) values(LN_REPORT_ID, 'params.StartDate', '{b}');
	insert into report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) values(LN_REPORT_ID, 'params.EndDate', '{e}');
	insert into report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) values(LN_REPORT_ID, 'paramNames', 'StartDate,EndDate,userId,lowAmount');
	insert into report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) values(LN_REPORT_ID, 'paramTypes', 'TIMESTAMP,TIMESTAMP,NUMERIC,NUMERIC');
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'isSQLFolio', 'true');
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'params.userId', '{u}');
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE,EDITOR) VALUES(LN_REPORT_ID, 'params.lowAmount', '10','simple.param.DefaultNumberParamEditor');
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'outputType', '21');
	
	select max(web_link_id)+1 into LN_WEB_LINK_ID from web_content.web_link;
		
	insert into web_content.web_link(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER) values(LN_WEB_LINK_ID, 'Devices with low sales', './devices_with_low_sales.html?basicReportId=' || LN_REPORT_ID, 'Devices with low sales', 'Daily Operations', 'W', 0);
	insert into web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) select web_link_id, 1 from web_content.web_link where web_link_label = 'Devices with low sales';
	insert into web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) select web_link_id, 45 from web_content.web_link where web_link_label = 'Devices with low sales';

	COMMIT;
END;
/

