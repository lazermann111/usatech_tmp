alter table REPORT.RMA_ALLOWED_PARTS
add STATUS VARCHAR2(1) DEFAULT 'A' NOT NULL;

drop index REPORT.UDX_RMA_PART_NUMBER;

CREATE UNIQUE INDEX REPORT.UDX_RMA_PART_NUMBER ON REPORT.RMA_ALLOWED_PARTS (DECODE(STATUS,'D',NULL,PART_NUMBER)) 
  TABLESPACE REPORT_INDX ONLINE;

alter table REPORT.CAMPAIGN_PROMO_TEXT
add (USER_ID NUMBER,
APPROVAL_NOTIFIED_TIME TIMESTAMP);

ALTER TABLE REPORT.CAMPAIGN_PROMO_TEXT
ADD CONSTRAINT FK_CPT_USER_ID
  FOREIGN KEY (USER_ID)
  REFERENCES REPORT.USER_LOGIN(USER_ID);
  
GRANT INSERT,UPDATE,DELETE,SELECT on WEB_CONTENT.COTE to REPORT;
GRANT SELECT ON WEB_CONTENT.SEQ_COTE_ID TO REPORT;
GRANT INSERT,UPDATE,DELETE,SELECT on WEB_CONTENT.COTE to USALIVE_APP_ROLE;

CREATE OR REPLACE PROCEDURE REPORT.USAT_CLEAN_CAMPAIGN_POS_PTA 
IS
BEGIN
  DELETE
FROM pss.campaign_pos_pta where pos_pta_id in (
  select pos_pta_id from pss.pos_pta pp
  where pp.pos_pta_deactivation_ts < sysdate
  );
  COMMIT;

END;
/

GRANT EXECUTE ON REPORT.USAT_CLEAN_CAMPAIGN_POS_PTA to USAT_DEV_READ_ONLY;

ALTER TABLE report.campaign_blast
ADD (CONSUMER_REGISTRATION_DATE DATE);

update WEB_CONTENT.LITERAL set literal_value='Select filter creteria ''By Serial Number'',''By Location'', or ''By Asset Nbr'' then input search value. Search will find devices that contain the search value for the creteria.'
where LITERAL_KEY='campaign-device-search-content';
commit;

-- tax id change
alter table corp.customer_bank
add (TAX_ID_NBR VARCHAR2(12));

UPDATE corp.customer_bank cb
   SET TAX_ID_NBR = (SELECT c.TAX_ID_NBR
                         FROM corp.customer c
                        WHERE cb.customer_id = c.customer_id);
commit;

DECLARE
	l_count NUMBER;
BEGIN
	select count(1) into l_count FROM dba_tables  where upper(table_name) like upper('tmp_corp_customer_2016_R51'); 
	IF l_count = 0 THEN
		EXECUTE IMMEDIATE 'create table yhe.tmp_corp_customer_2016_R51 as select * from corp.customer where status=''A''';
	END IF;
END;
/


alter table corp.customer
drop column TAX_ID_NBR;

insert into corp.address_type values('Bank billing address','Customer bank''s billing address',4);
commit;

alter table corp.customer_addr
add (CUSTOMER_BANK_ID NUMBER);

ALTER TABLE CORP.CUSTOMER_ADDR ADD CONSTRAINT FK_CA_CUSTOMER_BANK_ID FOREIGN KEY(CUSTOMER_BANK_ID) REFERENCES CORP.CUSTOMER_BANK(CUSTOMER_BANK_ID);

CREATE UNIQUE INDEX CORP.UDX_CA_CUSTOMER_BANK_ID ON CORP.CUSTOMER_ADDR(CASE WHEN CUSTOMER_BANK_ID IS NULL THEN NULL ELSE CUSTOMER_BANK_ID END) TABLESPACE CORP_INDX ONLINE;

-- helpdesk 3753
update web_content.web_link set web_link_url='activity_detail_ext.i?sortBy=0%2C%204%2C%2012%2C%202'||'&'||'params.endDate=%7B*0%7D'||'&'||'params.beginDate=%7B*-2592000000%7D'||'&'||'params.tranType=15%2C%2016%2C19'||'&'||'settleState=Declined%2CPending%2CRetry'||'&'||'rangeType=ALL'
where web_link_label='Unsettled Transactions (Last 30 Days)';
commit;

-- recur change record error
ALTER TABLE CORP.PAYOR
ADD( RECUR_ERROR_MSG VARCHAR2(4000 BYTE),
RECUR_EXCEPTION_TS DATE,
RETRY_COUNT NUMBER);

-- more tax id change
CREATE OR REPLACE FUNCTION CORP.GET_CUSTOMER_TAX_ID (pn_customer_id IN NUMBER)
   RETURN varchar
IS
   l_tax_id_nbr varchar2(12);  
BEGIN

   select max(cb.tax_id_nbr) into l_tax_id_nbr from corp.customer_bank cb where cb.customer_id=pn_customer_id
   and cb.create_date=(select min(create_date) 
   from corp.customer_bank where customer_id=pn_customer_id and tax_id_nbr is not null
   and tax_id_nbr != '0'
   and tax_id_nbr not like '11111%'
   );
   
   return l_tax_id_nbr;
END;
/

GRANT execute on CORP.GET_CUSTOMER_TAX_ID to USAT_POSM_ROLE;
GRANT execute on CORP.GET_CUSTOMER_TAX_ID to USAT_DEV_READ_ONLY;

CREATE UNIQUE INDEX REPORT.UDX_RMA_SHIPPING_ADDR_NAME_A ON REPORT.RMA_SHIPPING_ADDR (DECODE(STATUS,'D', NULL,DECODE(NAME, NULL, NULL, USER_ID||'-'||NAME))) TABLESPACE REPORT_INDX;
drop index REPORT.UDX_RMA_SHIPPING_ADDR_NAME;

UPDATE web_content.web_link
SET web_link_url = './rma_att_question.html'
WHERE web_link_label = 'ATT 2G Upgrade'
AND web_link_usage = 'M';
