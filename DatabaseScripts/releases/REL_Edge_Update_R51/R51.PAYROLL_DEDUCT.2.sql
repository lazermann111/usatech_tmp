ALTER TABLE CORP.CUSTOMER ADD (
	PAY_PERIOD_TYPE VARCHAR2(128),
	PAY_PERIOD_DATA VARCHAR2(128),
	LAST_TOP_UP_DATE DATE);

ALTER TABLE PSS.CONSUMER_ACCT_SUB_TYPE ADD (
	OPERATOR_SERVICED_IND VARCHAR2(1) DEFAULT 'N',
	USAT_SERVICED_IND VARCHAR2(1) DEFAULT 'N');

UPDATE PSS.CONSUMER_ACCT_SUB_TYPE SET
	CONSUMER_ACCT_SUB_TYPE_DESC = 'USAT Serviced Prepaid',
	OPERATOR_SERVICED_IND = 'N',
	USAT_SERVICED_IND = 'Y'
WHERE CONSUMER_ACCT_SUB_TYPE_ID = 1;

UPDATE PSS.CONSUMER_ACCT_SUB_TYPE SET
	CONSUMER_ACCT_SUB_TYPE_DESC = 'Operator Serviced Prepaid',
	OPERATOR_SERVICED_IND = 'Y',
	USAT_SERVICED_IND = 'N'
WHERE CONSUMER_ACCT_SUB_TYPE_ID = 2;

INSERT INTO PSS.CONSUMER_ACCT_SUB_TYPE(CONSUMER_ACCT_SUB_TYPE_ID, CONSUMER_ACCT_SUB_TYPE_DESC, CONSUMER_ACCT_TYPE_ID, OPERATOR_SERVICED_IND, USAT_SERVICED_IND)
VALUES (3, 'Operator Serviced Payroll Deduct', 7, 'Y', 'N');

ALTER TABLE PSS.CONSUMER_ACCT ADD (
	TOP_UP_AMT NUMBER);

ALTER TABLE PSS.CONSUMER ADD (
	CUSTOMER_ASSIGNED_ID VARCHAR(128),
	DEPARTMENT_NAME VARCHAR(256));

ALTER TABLE PSS.CONSUMER_TYPE ADD (
	MORE_ENABLED_IND VARCHAR2(1) DEFAULT 'N',
	USALIVE_ENABLED_IND VARCHAR2(1) DEFAULT 'N');

UPDATE PSS.CONSUMER_TYPE SET
	MORE_ENABLED_IND = 'Y'
WHERE CONSUMER_TYPE_ID IN (5,7,8);

UPDATE PSS.CONSUMER_TYPE SET
	USALIVE_ENABLED_IND = 'Y'
WHERE CONSUMER_TYPE_ID IN (5,7,8,9);

ALTER TABLE PSS.CONSUMER_ACCT_TYPE ADD (
	MORE_ENABLED_IND VARCHAR2(1) DEFAULT 'N',
	USALIVE_ENABLED_IND VARCHAR2(1) DEFAULT 'N',
	USAT_GENERATED_IND VARCHAR2(1) DEFAULT 'N');

UPDATE PSS.CONSUMER_ACCT_TYPE SET
	MORE_ENABLED_IND = 'Y'
WHERE CONSUMER_ACCT_TYPE_ID IN (3,5,6);

UPDATE PSS.CONSUMER_ACCT_TYPE SET
	USALIVE_ENABLED_IND = 'Y'
WHERE CONSUMER_ACCT_TYPE_ID IN (3,5,6,7);

UPDATE PSS.CONSUMER_ACCT_TYPE SET
	USAT_GENERATED_IND = 'Y'
WHERE CONSUMER_ACCT_TYPE_ID IN (1,2,3,7);

UPDATE REPORT.PRIV
SET DESCRIPTION = 'Manage Consumers'
WHERE PRIV_ID = 31;
