UPDATE REPORT.REPORT_PARAM SET PARAM_VALUE = 
q'[SELECT
lts.entry_type,
bu.business_unit_name,
(CASE bat.payment_schedule_id WHEN 4 THEN 'Y' ELSE 'N' END) monthly_payment,
cu.currency_code,
c.customer_name,
lts.fee_name,
(CASE d.status
	WHEN 'S' THEN 'Y'
	WHEN 'P' THEN 'Y'
	ELSE 'N'
 END
) in_eft,
TO_CHAR(TRUNC(lts.entry_date, 'MONTH'),'mm/dd/yyyy') month_for,
COUNT(1) entry_count,
SUM (lts.amount) total_entry_amount,
lts.description,
c.customer_id
FROM (
	SELECT
	led.batch_id,
	led.amount,
	led.entry_type,
	led.settle_state_id,
	led.create_date,
	led.entry_date,
	led.description,
	f.fee_name
	FROM corp.ledger led
	LEFT OUTER JOIN (
		corp.service_fees sf
		JOIN corp.fees f
		ON f.fee_id = sf.fee_id
	)
	ON sf.service_fee_id = led.service_fee_id
	WHERE led.entry_type IN ('SF', 'AD', 'SB')
	AND led.deleted = 'N'
	AND led.settle_state_id IN (2, 3)
	AND (led.report_date >= CAST(? AS DATE) AND led.report_date < CAST(? AS DATE) + 1
	OR led.create_date >= CAST(? AS DATE) AND led.create_date < CAST(? AS DATE) + 1 AND led.report_date IS NULL)
)lts
JOIN corp.batch bat
ON bat.batch_id = lts.batch_id
JOIN corp.doc d
ON d.doc_id = bat.doc_id
JOIN corp.customer_bank cb
ON cb.customer_bank_id = d.customer_bank_id
JOIN corp.customer c
ON c.customer_id = cb.customer_id
JOIN corp.business_unit bu
ON bu.business_unit_id = d.business_unit_id
JOIN corp.currency cu
ON cu.currency_id = d.currency_id
JOIN report.trans_state ts
ON ts.state_id = lts.settle_state_id -- yes, this is non intuitive but correct
GROUP BY
lts.entry_type,
bu.business_unit_name,
(CASE bat.payment_schedule_id WHEN 4 THEN 'Y' ELSE 'N' END),
cu.currency_code,
c.customer_name,
c.customer_id,
lts.fee_name,
(CASE d.status
	WHEN 'S' THEN 'Y'
	WHEN 'P' THEN 'Y'
	ELSE 'N'
 END
),
TRUNC(lts.entry_date, 'MONTH'),
lts.description]'
WHERE report_id = 191 AND param_name = 'query';

UPDATE REPORT.REPORT_PARAM SET PARAM_VALUE = 
q'[SELECT
lts.entry_type,
bu.business_unit_name,
DECODE(bat.payment_schedule_id, 4, 'Y', 'N') monthly_payment,
cu.currency_code,
c.customer_name,
ee.eport_num,
lts.fee_name,
DECODE(d.status,
	'S', 'Y',
	'P', 'Y',
	'N'
) in_eft,
TO_CHAR(TRUNC(lts.entry_date, 'MONTH'),'mm/dd/yyyy') month_for,
COUNT(1) entry_count,
SUM (lts.amount) total_entry_amount,
lts.description,
t.terminal_nbr,
t.sales_order_number,
c.customer_id
FROM (
	SELECT
	led.batch_id,
	led.amount,
	led.entry_type,
	led.settle_state_id,
	led.create_date,
	led.entry_date,
	led.description,
	f.fee_name
	FROM corp.ledger led
	LEFT OUTER JOIN (
		corp.service_fees sf
		JOIN corp.fees f
		ON f.fee_id = sf.fee_id
	)
	ON sf.service_fee_id = led.service_fee_id
	WHERE led.entry_type IN ('SF', 'AD', 'SB')
	AND led.deleted = 'N'
	AND led.settle_state_id IN (2, 3)
	AND (led.report_date >= CAST(? AS DATE) AND led.report_date < CAST(? AS DATE) + 1
	OR led.create_date >= CAST(? AS DATE) AND led.create_date < CAST(? AS DATE) + 1 AND led.report_date IS NULL)
)lts
JOIN corp.batch bat
ON bat.batch_id = lts.batch_id
JOIN corp.doc d
ON d.doc_id = bat.doc_id
JOIN corp.customer_bank cb
ON cb.customer_bank_id = d.customer_bank_id
JOIN corp.customer c
ON c.customer_id = cb.customer_id
JOIN corp.business_unit bu
ON bu.business_unit_id = d.business_unit_id
JOIN corp.currency cu
ON cu.currency_id = d.currency_id
JOIN report.trans_state ts
ON ts.state_id = lts.settle_state_id
LEFT outer JOIN (SELECT DISTINCT TERMINAL_ID, FIRST_VALUE(eport_serial_num) OVER (PARTITION BY TERMINAL_ID ORDER BY start_date DESC) Eport_num
from report.terminal_eport te, report.eport e where te.eport_id=e.eport_id) ee
ON bat.terminal_id=ee.terminal_id
LEFT OUTER JOIN report.terminal t ON bat.terminal_id = t.terminal_id
GROUP BY
lts.entry_type,
bu.business_unit_name,
DECODE(bat.payment_schedule_id, 4, 'Y', 'N'),
cu.currency_code,
c.customer_name,
c.customer_id,
lts.fee_name,
DECODE(d.status,
	'S', 'Y',
	'P', 'Y',
	'N'
),
TRUNC(lts.entry_date, 'MONTH'),
lts.description
,ee.Eport_num,
t.terminal_nbr,
t.sales_order_number]'
WHERE report_id = 192 AND param_name = 'query';

UPDATE REPORT.REPORT_PARAM SET PARAM_VALUE = 
q'[SELECT D.DEVICE_SERIAL_CD, C.CUSTOMER_NAME, L.LOCATION_NAME, TA.ADDRESS1, TA.CITY, TA.STATE, TA.ZIP, D.FIRMWARE_VERSION, D.LAST_ACTIVITY_TS, ROUND(SYSDATE - D.LAST_ACTIVITY_TS) DAYS_AGO, C.CUSTOMER_ID
FROM DEVICE.DEVICE D
JOIN DEVICE.DEVICE_TYPE DT ON D.DEVICE_TYPE_ID = DT.DEVICE_TYPE_ID
JOIN DEVICE.DEVICE_LAST_ACTIVE DLA ON D.DEVICE_ID = DLA.DEVICE_ID
LEFT JOIN (REPORT.EPORT E 
JOIN REPORT.VW_TERMINAL_EPORT TE ON E.EPORT_ID = TE.EPORT_ID
JOIN REPORT.TERMINAL T ON TE.TERMINAL_ID = T.TERMINAL_ID
JOIN REPORT.LOCATION L ON T.LOCATION_ID = L.LOCATION_ID
LEFT JOIN REPORT.TERMINAL_ADDR TA ON L.ADDRESS_ID = TA.ADDRESS_ID
JOIN CORP.CUSTOMER C ON T.CUSTOMER_ID = C.CUSTOMER_ID) ON D.DEVICE_SERIAL_CD = E.EPORT_SERIAL_NUM
WHERE D.COMM_METHOD_CD = 'G' AND DT.EPORT_IND = 'Y' AND D.DEVICE_SERIAL_CD NOT LIKE 'VJ%'
ORDER BY D.DEVICE_SERIAL_CD]'
WHERE report_id = 457 AND param_name = 'query';

UPDATE REPORT.REPORT_PARAM SET PARAM_VALUE = 
q'[SELECT DC.DFR_CHARGEBACK_ID, DC.ENTITY_NUM, DC.CHARGEBACK_AMOUNT, DC.CHARGEBACK_CURRENCY, DC.PREVIOUS_PARTIAL, DC.ACTIVITY_CD, DAC.NAME ACTIVITY_NAME,
DC.STATUS_CD, DC.SEQUENCE_NUM, DC.MERCHANT_ORDER_NUM, DC.ACCOUNT_NUM, DC.REASON_CD, CBR.DESCRIPTION, TO_CHAR(DC.TRANSACTION_DATE, 'MM/DD/YYYY') TRANSACTION_DATE,
TO_CHAR(DC.INITIATED_DATE, 'MM/DD/YYYY') INITIATED_DATE, TO_CHAR(DC.ACTIVITY_DATE, 'MM/DD/YYYY') ACTIVITY_DATE, DC.ACTIVITY_AMOUNT, DC.FEE_AMOUNT, DC.USAGE_CD,
DC.MOP_CD, TO_CHAR(DC.AUTH_DATE, 'MM/DD/YYYY') AUTH_DATE, TO_CHAR(DC.DUE_DATE, 'MM/DD/YYYY') DUE_DATE, TO_CHAR(DC.CREATED_TS, 'MM/DD/YYYY HH24:MI:SS') DB_CREATED_TS,
X.TRAN_ID DMS_TRAN_ID, T.TRAN_ID REPORT_TRAN_ID, T.TOTAL_AMOUNT TRAN_AMOUNT, T.CARD_NUMBER, T.START_DATE TRAN_START_DATE, C.CUSTOMER_NAME, E.EPORT_SERIAL_NUM,
TERM.TERMINAL_NBR, AR.VEND_COLUMN, U.FIRST_NAME || ' ' ||  U.LAST_NAME FIRST_LAST, U.EMAIL, C.CUSTOMER_ID
FROM REPORT.DFR_CHARGEBACK DC
LEFT OUTER JOIN REPORT.TRANS T ON 'A:' || REPLACE(DC.MERCHANT_ORDER_NUM, '-', ':') = T.MACHINE_TRANS_NO
LEFT OUTER JOIN PSS.TRAN X ON T.MACHINE_TRANS_NO = X.TRAN_GLOBAL_TRANS_CD
LEFT OUTER JOIN REPORT.EPORT E ON T.EPORT_ID = E.EPORT_ID
LEFT OUTER JOIN REPORT.TERMINAL TERM ON T.TERMINAL_ID = TERM.TERMINAL_ID
LEFT OUTER JOIN CORP.CUSTOMER C ON TERM.CUSTOMER_ID = C.CUSTOMER_ID
LEFT OUTER JOIN REPORT.ACTIVITY_REF AR ON T.TRAN_ID = AR.TRAN_ID
LEFT OUTER JOIN REPORT.DFR_ACTIVITY_CATEGORY DAC ON DC.ACTIVITY_CD = DAC.CODE
LEFT OUTER JOIN REPORT.USER_LOGIN U ON U.USER_ID = C.USER_ID
LEFT OUTER JOIN REPORT.DFR_CHARGEBACK_REASON CBR ON DC.REASON_CD = CBR.CHARGEBACK_REASON_CD
WHERE DC.CREATED_TS >= CAST(? AS DATE) AND DC.CREATED_TS < CAST(? AS DATE) + 1
ORDER BY DC.CHARGEBACK_AMOUNT DESC, DC.DFR_CHARGEBACK_ID]'
WHERE report_id = 243 AND param_name = 'query';

UPDATE REPORT.REPORT_PARAM SET PARAM_VALUE = 
q'[select c.customer_name, x.previously_active_devices, x.currently_active_devices,
ul.user_name, ul.first_name, ul.last_name, ul.email, ul.telephone, c.customer_id
from corp.customer c join (
select c.customer_id,
sum(case when e.lastdialin_date between sysdate - 60 and sysdate - 30 then 1 else 0 end) previously_active_devices,
sum(case when e.lastdialin_date > sysdate - 30 then 1 else 0 end) currently_active_devices
from report.terminal t 
join report.vw_terminal_eport te on t.terminal_id = te.terminal_id
join report.eport e on te.eport_id = e.eport_id
join corp.customer c on t.customer_id = c.customer_id
where c.status != 'D' and t.status != 'D'
group by c.customer_id ) x on c.customer_id = x.customer_id
join report.user_login ul on c.user_id = ul.user_id
where x.previously_active_devices > 5 and x.currently_active_devices / x.previously_active_devices < 0.2
order by upper(c.customer_name)]'
WHERE report_id = 434 AND param_name = 'query';

UPDATE REPORT.REPORT_PARAM SET PARAM_VALUE = 
q'[SELECT l.entry_type, f.fee_name service_fee_name, bu.business_unit_name, DECODE(bat.payment_schedule_id, 4, 'Y', 'N') monthly_payment,
cu.currency_code, c.customer_name, cb.bank_acct_nbr, cb.bank_routing_nbr, cb.account_title, d.doc_id eft_id, 
TO_CHAR(d.sent_date, 'mm/dd/yyyy hh:mi:ssAM') eft_date, SUM(l.amount) ledger_amount, d.total_amount eft_total_amount, c.customer_id
FROM corp.doc d 
JOIN corp.customer_bank cb ON cb.customer_bank_id = d.customer_bank_id 
JOIN corp.customer c ON c.customer_id = cb.customer_id 
JOIN corp.business_unit bu ON bu.business_unit_id = d.business_unit_id 
JOIN corp.currency cu ON cu.currency_id = d.currency_id 
JOIN corp.batch bat ON bat.doc_id = d.doc_id 
JOIN corp.ledger l ON l.batch_id = bat.batch_id 
LEFT OUTER JOIN ( 
	corp.service_fees sf 
	JOIN corp.fees f 
	ON f.fee_id = sf.fee_id 
)
ON sf.service_fee_id = l.service_fee_id 
WHERE sent_date > sysdate -14 
AND l.deleted = 'N' 
AND l.settle_state_id IN (2, 3) 
GROUP BY 
l.entry_type, 
f.fee_name, 
bu.business_unit_name, 
DECODE(bat.payment_schedule_id, 4, 'Y', 'N'), 
cu.currency_code, 
c.customer_name,
c.customer_id,
cb.bank_acct_nbr, 
cb.bank_routing_nbr, 
cb.account_title, 
d.doc_id, 
d.sent_date, 
d.total_amount]'
WHERE report_id = 188 AND param_name = 'query';

UPDATE REPORT.REPORT_PARAM SET PARAM_VALUE = 
q'[SELECT
l.entry_type,
f.fee_name service_fee_name,
bu.business_unit_name,
DECODE(bat.payment_schedule_id, 4, 'Y', 'N') monthly_payment,
cu.currency_code,
c.customer_name,
cb.bank_acct_nbr,
cb.bank_routing_nbr,
cb.account_title,
d.doc_id eft_id,
TO_CHAR(d.sent_date, 'mm/dd/yyyy hh:mi:ssAM') eft_date,
SUM(l.amount) ledger_amount,
d.total_amount eft_total_amount,
c.customer_id
FROM corp.doc d
JOIN corp.customer_bank cb
ON cb.customer_bank_id = d.customer_bank_id
JOIN corp.customer c
ON c.customer_id = cb.customer_id
JOIN corp.business_unit bu
ON bu.business_unit_id = d.business_unit_id
JOIN corp.currency cu
ON cu.currency_id = d.currency_id
JOIN corp.batch bat
ON bat.doc_id = d.doc_id
JOIN corp.ledger l
ON l.batch_id = bat.batch_id
LEFT OUTER JOIN (
	corp.service_fees sf
	JOIN corp.fees f
	ON f.fee_id = sf.fee_id
)
ON sf.service_fee_id = l.service_fee_id
WHERE d.sent_date >= CAST(? AS DATE) AND d.sent_date < CAST(? AS DATE) + 1
AND l.deleted = 'N'
AND l.settle_state_id IN (2, 3)
GROUP BY
l.entry_type,
f.fee_name,
bu.business_unit_name,
DECODE(bat.payment_schedule_id, 4, 'Y', 'N'),
cu.currency_code,
c.customer_name,
c.customer_id,
cb.bank_acct_nbr,
cb.bank_routing_nbr,
cb.account_title,
d.doc_id,
d.sent_date,
d.total_amount]'
WHERE report_id = 189 AND param_name = 'query';

UPDATE REPORT.REPORT_PARAM SET PARAM_VALUE = 
q'[SELECT D.DEVICE_SERIAL_CD, C.CUSTOMER_NAME, L.LOCATION_NAME, CASE WHEN TRIM(TA.CITY) IS NOT NULL THEN TA.CITY || ', ' END || TA.STATE || ' ' || TA.ZIP ADDRESS, D.FIRMWARE_VERSION, D.LAST_ACTIVITY_TS, SYSDATE - D.LAST_ACTIVITY_TS DAYS_AGO, C.CUSTOMER_ID
FROM DEVICE.DEVICE D
JOIN DEVICE.HOST H ON D.DEVICE_ID = H.DEVICE_ID
JOIN DEVICE.HOST_TYPE HT ON H.HOST_TYPE_ID = HT.HOST_TYPE_ID
LEFT JOIN (REPORT.EPORT E
JOIN REPORT.VW_TERMINAL_EPORT TE ON E.EPORT_ID = TE.EPORT_ID
JOIN REPORT.TERMINAL T ON TE.TERMINAL_ID = T.TERMINAL_ID
JOIN REPORT.LOCATION L ON T.LOCATION_ID = L.LOCATION_ID
LEFT JOIN REPORT.TERMINAL_ADDR TA ON L.ADDRESS_ID = TA.ADDRESS_ID
JOIN CORP.CUSTOMER C ON T.CUSTOMER_ID = C.CUSTOMER_ID) ON D.DEVICE_SERIAL_CD = E.EPORT_SERIAL_NUM
WHERE D.DEVICE_TYPE_ID = 13
AND D.DEVICE_SERIAL_CD LIKE 'EE1000%'
AND H.HOST_SERIAL_CD LIKE '8944%'
ORDER BY D.DEVICE_SERIAL_CD]'
WHERE report_id = 433 AND param_name = 'query';

UPDATE REPORT.REPORT_PARAM SET PARAM_VALUE = 
q'[SELECT
led.entry_type,
bu.business_unit_name,
DECODE(t.payment_schedule_id, 4, 'Y', 'N') monthly_payment,
cr.currency_code,
c.customer_name,
TO_CHAR(TRUNC(tr.settle_date, 'MONTH'), 'mm/dd/yyyy') month_for,
e.eport_serial_num,
COALESCE(pf2.fee_percent, pf.fee_percent) * 100 fee_percent,
COUNT(1) tran_count,
SUM(tr.total_amount) total_tran_amount,
SUM(led.amount) total_ledger_amount,
c.customer_id
FROM report.trans tr
JOIN corp.currency cr
ON cr.currency_id = tr.currency_id
JOIN report.trans_state ts
ON ts.state_id = tr.settle_state_id
JOIN report.terminal t ON tr.terminal_id = t.terminal_id
JOIN corp.customer c ON c.customer_id = t.customer_id
JOIN corp.business_unit bu ON bu.business_unit_id = t.business_unit_id
JOIN corp.ledger led
ON led.trans_id = tr.tran_id
AND led.deleted = 'N'
JOIN report.eport e on tr.eport_id = e.eport_id
JOIN corp.process_fees pf ON t.terminal_id = pf.terminal_id
JOIN report.trans_type tt ON tr.trans_type_id = tt.trans_type_id
LEFT OUTER JOIN corp.process_fees pf2 ON t.terminal_id = pf2.terminal_id AND tt.process_fee_trans_type_id = pf2.trans_type_id
AND (pf2.start_date IS NULL OR pf2.start_date < SYSDATE) AND (pf2.end_date IS NULL OR pf2.end_date > SYSDATE)
AND pf2.fee_percent > TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('STANDARD_CREDIT_PROCESS_FEE_PERCENT'))
WHERE tt.payable_ind = 'Y'
AND tr.settle_state_id IN (2, 3)
AND led.entry_type IN ('CC', 'PF', 'CB', 'RF')
AND tr.settle_date >= CAST(? AS DATE) AND tr.settle_date < CAST(? AS DATE) + 1
AND pf.trans_type_id = 16
AND (pf.start_date IS NULL OR pf.start_date < SYSDATE) AND (pf.end_date IS NULL OR pf.end_date > SYSDATE)
AND pf.fee_percent > TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('STANDARD_CREDIT_PROCESS_FEE_PERCENT'))
GROUP BY
led.entry_type,
bu.business_unit_name,
DECODE(t.payment_schedule_id, 4, 'Y', 'N'),
cr.currency_code,
c.customer_name,
c.customer_id,
TRUNC(tr.settle_date, 'MONTH'),
e.eport_serial_num,
pf.fee_percent,
pf2.fee_percent
ORDER BY c.customer_name, e.eport_serial_num, led.entry_type]'
WHERE report_id = 214 AND param_name = 'query';

UPDATE REPORT.REPORT_PARAM SET PARAM_VALUE = 
q'[select c.customer_name, t.terminal_nbr, e.eport_serial_num,
to_char(d.last_activity_ts, 'mm/dd/yyyy hh24:mi:ss') last_activity_ts, c.customer_id
from report.terminal t
join corp.customer c on t.customer_id = c.customer_id
join report.vw_terminal_eport te on t.terminal_id = te.terminal_id
join report.eport e on te.eport_id = e.eport_id
left outer join (device.device d
join device.device_last_active dla on d.device_id = dla.device_id
) on e.eport_serial_num = d.device_serial_cd
where t.status != 'D' and t.terminal_id in (
select terminal_id from report.vw_terminal_eport
group by terminal_id having count(1) > 1
) order by t.terminal_nbr, d.last_activity_ts desc nulls last]'
WHERE report_id = 468 AND param_name = 'query';

UPDATE REPORT.REPORT_PARAM SET PARAM_VALUE = 
q'[SELECT 
led.entry_type,
COALESCE(bu.business_unit_name, '~ Terminal Orphan') business_unit_name,
COALESCE(DECODE(t.payment_schedule_id, 4, 'Y', 'N'), 'UNK') monthly_payment,
cr.currency_code,
COALESCE(c.customer_name, '~ Terminal Orphan') customer_name,
TO_CHAR(TRUNC(DECODE (led.entry_type,
	'CC', tr.settle_date,
	led.ledger_date
), 'MONTH'), 'mm/dd/yyyy') month_for,
COUNT(1) tran_count,
SUM(tr.total_amount) total_tran_amount,
SUM(led.amount) total_ledger_amount,
tr.trans_type_id,
tt.trans_type_name,
c.customer_id
FROM report.trans tr
JOIN corp.currency cr
ON cr.currency_id = tr.currency_id
JOIN report.trans_state ts
ON ts.state_id = tr.settle_state_id 
LEFT OUTER JOIN (
	report.terminal t
	JOIN corp.customer c
	ON c.customer_id = t.customer_id
	JOIN corp.business_unit bu
	ON bu.business_unit_id = t.business_unit_id
)
ON t.terminal_id = tr.terminal_id
JOIN corp.ledger led
ON led.trans_id = tr.tran_id
AND led.deleted = 'N'
JOIN report.trans_type tt
ON tr.trans_type_id = tt.trans_type_id
WHERE tr.settle_state_id IN (2, 3) 
AND led.entry_type IN ('CC', 'PF', 'CB', 'RF') 
AND (tr.settle_date >= CAST(? AS DATE) AND tr.settle_date < CAST(? AS DATE) + 1 
OR led.create_date >= CAST(? AS DATE) AND led.create_date < CAST(? AS DATE) + 1 AND tr.settle_date IS NULL)
GROUP BY
led.entry_type,
COALESCE(bu.business_unit_name, '~ Terminal Orphan'),
COALESCE(DECODE(t.payment_schedule_id, 4, 'Y', 'N'), 'UNK'),
cr.currency_code,
COALESCE(c.customer_name, '~ Terminal Orphan'),
c.customer_id, 
TRUNC(DECODE (led.entry_type,
	'CC', tr.settle_date,
	led.ledger_date
), 'MONTH'),
tr.trans_type_id,
tt.trans_type_name]'
WHERE report_id = 193 AND param_name = 'query';

UPDATE REPORT.REPORT_PARAM
SET PARAM_VALUE = 'StartDate,EndDate,StartDate,EndDate'
WHERE report_id = 193 AND param_name = 'paramNames';

UPDATE REPORT.REPORT_PARAM
SET PARAM_VALUE = 'TIMESTAMP,TIMESTAMP,TIMESTAMP,TIMESTAMP'
WHERE report_id = 193 AND param_name = 'paramTypes';

UPDATE REPORT.REPORT_PARAM SET PARAM_VALUE = 
q'[SELECT
led.entry_type,
COALESCE(bu.business_unit_name, '~ Terminal Orphan') business_unit_name,
COALESCE(DECODE(t.payment_schedule_id, 4, 'Y', 'N'), 'UNK') monthly_payment,
cr.currency_code,
COALESCE(c.customer_name, '~ Terminal Orphan') customer_name,
TO_CHAR(TRUNC(DECODE (led.entry_type,
	'CC', tr.settle_date,
	led.ledger_date
), 'MONTH'), 'mm/dd/yyyy') month_for,
COUNT(1) tran_count,
SUM(tr.total_amount) total_tran_amount,
SUM(led.amount) total_ledger_amount,
tr.trans_type_id,
tt.trans_type_name,
ee.eport_num,
c.customer_id
FROM report.trans tr
JOIN corp.currency cr
ON cr.currency_id = tr.currency_id
JOIN report.trans_state ts
ON ts.state_id = tr.settle_state_id
LEFT OUTER JOIN (
	report.terminal t
	JOIN corp.customer c
	ON c.customer_id = t.customer_id
	JOIN corp.business_unit bu
	ON bu.business_unit_id = t.business_unit_id
)
ON t.terminal_id = tr.terminal_id
JOIN corp.ledger led
ON led.trans_id = tr.tran_id
AND led.deleted = 'N'
JOIN report.trans_type tt
ON tr.trans_type_id = tt.trans_type_id
LEFT OUTER JOIN (
SELECT DISTINCT terminal_id, FIRST_VALUE(eport_serial_num) OVER (PARTITION BY terminal_id ORDER BY start_date DESC) eport_num
FROM report.terminal_eport te
JOIN report.eport e ON te.eport_id = e.eport_id
) ee ON tr.terminal_id = ee.terminal_id
WHERE tr.settle_state_id IN (2, 3)
AND led.entry_type IN ('CC', 'PF', 'CB', 'RF')
AND (tr.settle_date >= CAST(? AS DATE) AND tr.settle_date < CAST(? AS DATE) + 1 
OR led.create_date >= CAST(? AS DATE) AND led.create_date < CAST(? AS DATE) + 1 AND tr.settle_date IS NULL)
GROUP BY
led.entry_type,
COALESCE(bu.business_unit_name, '~ Terminal Orphan'),
COALESCE(DECODE(t.payment_schedule_id, 4, 'Y', 'N'), 'UNK'),
cr.currency_code,
COALESCE(c.customer_name, '~ Terminal Orphan'),
c.customer_id,
TRUNC(DECODE (led.entry_type,
	'CC', tr.settle_date,
	led.ledger_date
), 'MONTH'),
tr.trans_type_id,
tt.trans_type_name,
ee.eport_num]'
WHERE report_id = 226 AND param_name = 'query';

UPDATE REPORT.REPORT_PARAM
SET PARAM_VALUE = 'StartDate,EndDate,StartDate,EndDate'
WHERE report_id = 226 AND param_name = 'paramNames';

UPDATE REPORT.REPORT_PARAM
SET PARAM_VALUE = 'TIMESTAMP,TIMESTAMP,TIMESTAMP,TIMESTAMP'
WHERE report_id = 226 AND param_name = 'paramTypes';

UPDATE REPORT.REPORT_PARAM SET PARAM_VALUE = 
q'[SELECT
l.entry_type,
bu.business_unit_name,
DECODE(bat.payment_schedule_id, 4, 'Y', 'N') monthly_payment,
cu.currency_code,
c.customer_name,
TO_CHAR(TRUNC(DECODE (l.entry_type,
	'CC', t.settle_date,
	l.entry_date
), 'MONTH'),'mm/dd/yyyy') month_for,
COUNT(1) tran_count,
SUM (l.amount) total_ledger_amount,
c.customer_id
FROM corp.ledger l
JOIN corp.batch bat
ON bat.batch_id = l.batch_id
JOIN corp.doc d
ON d.doc_id = bat.doc_id
JOIN corp.customer_bank cb
ON cb.customer_bank_id = d.customer_bank_id
JOIN corp.customer c
ON c.customer_id = cb.customer_id
JOIN corp.business_unit bu
ON bu.business_unit_id = d.business_unit_id
JOIN corp.currency cu
ON cu.currency_id = d.currency_id
JOIN report.trans_state ts
ON ts.state_id = l.settle_state_id
LEFT OUTER JOIN report.trans t
ON t.tran_id = l.trans_id
WHERE l.deleted = 'N'
AND d.status != 'D'
AND l.settle_state_id IN (2, 3)
AND (d.sent_date IS NULL OR d.sent_date >= TRUNC(CAST(? AS DATE) + 1, 'MONTH'))
AND (t.settle_date < TRUNC(CAST(? AS DATE) + 1, 'MONTH') OR l.create_date < TRUNC(CAST(? AS DATE) + 1, 'MONTH') AND t.settle_date IS NULL)
GROUP BY
l.entry_type,
bu.business_unit_name,
DECODE(bat.payment_schedule_id, 4, 'Y', 'N'),
cu.currency_code,
c.customer_name,
c.customer_id,
TRUNC(DECODE (l.entry_type,
	'CC', t.settle_date,
	l.entry_date
), 'MONTH')]'
WHERE report_id = 194 AND param_name = 'query';

UPDATE REPORT.REPORT_PARAM
SET PARAM_VALUE = 'EndDate,EndDate,EndDate'
WHERE report_id = 194 AND param_name = 'paramNames';

UPDATE REPORT.REPORT_PARAM
SET PARAM_VALUE = 'TIMESTAMP,TIMESTAMP,TIMESTAMP'
WHERE report_id = 194 AND param_name = 'paramTypes';
    
COMMIT;
