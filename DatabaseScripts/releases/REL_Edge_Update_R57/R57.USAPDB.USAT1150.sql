INSERT INTO PSS.TRAN_STATE(TRAN_STATE_CD, TRAN_STATE_DESC, REPORTABLE_IND)
VALUES('!', 'TRAN_PENDING_APPROVAL', 'N');

INSERT INTO ENGINE.APP_SETTING(APP_SETTING_CD, APP_SETTING_VALUE, APP_SETTING_DESC)
VALUES('AUTO_REVERSAL_AMOUNT_LIMIT', '2000', 'If the total amount of auto reversals is above this limit, they will be pending approval or rejection, otherwise they will be released for processing');

INSERT INTO ENGINE.APP_SETTING(APP_SETTING_CD, APP_SETTING_VALUE, APP_SETTING_DESC) SELECT 'RISK_ALERT_EMAIL_TO',
CASE REPLACE(GLOBAL_NAME, '.WORLD', '') 
	WHEN 'USAPDB' THEN 'ApplicationSupportTeam@usatech.com,RiskManagement@usatech.com' 
	WHEN 'ECCDB' THEN 'QualityAssurance@usatech.com'
	WHEN 'USADEV02' THEN 'QualityAssurance@usatech.com'
	ELSE 'SoftwareDevelopmentTeam@usatech.com' END,
'Distribution list for risk alerts such as pending auto-reversals' FROM GLOBAL_NAME;

COMMIT;

BEGIN
	DBMS_SCHEDULER.CREATE_JOB (
	   job_name => 'PSS.UPDATE_PENDING_REVERSALS_JOB',
	   job_type => 'STORED_PROCEDURE',
	   job_action => 'PSS.UPDATE_PENDING_REVERSALS',
	   repeat_interval => 'FREQ=DAILY;BYHOUR=14;BYMINUTE=37;BYSECOND=21;',
	   job_class => 'DEFAULT_JOB_CLASS',
	   comments => 'Job to update pending reversals. If the total amount of pending auto reversals is above configured limit, they will remain pending approval or rejection, otherwise they will be released for processing.',
	   auto_drop => FALSE,
	   enabled => TRUE
	);
END;
/
