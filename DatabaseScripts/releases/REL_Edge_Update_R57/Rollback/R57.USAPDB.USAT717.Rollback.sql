DECLARE
  ln_report_id NUMBER;
  ln_web_link_id NUMBER;
BEGIN
  ln_report_id := 481;
  delete from report.report_param where report_id = ln_report_id;
  delete from report.reports where report_id = ln_report_id;
  select WEB_LINK_ID into ln_web_link_id from WEB_CONTENT.WEB_LINK WHERE WEB_LINK_URL = './select_date_range_frame_sqlfolio.i?basicReportId=' || ln_report_id;
  
  delete from WEB_CONTENT.WEB_LINK_REQUIREMENT WHERE WEB_LINK_ID =ln_web_link_id; 
  delete from WEB_CONTENT.WEB_LINK WHERE WEB_LINK_ID = ln_web_link_id;
  
  commit;
END;
/