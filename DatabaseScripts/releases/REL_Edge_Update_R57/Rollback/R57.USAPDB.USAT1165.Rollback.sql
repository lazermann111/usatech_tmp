BEGIN
  DBMS_SCHEDULER.DROP_JOB('PSS.UPDATE_PENDING_TRAN_JOB');
  DBMS_SCHEDULER.CREATE_JOB (
   job_name           =>  'PSS."CC DATA MASK"',
   job_type           =>  'PLSQL_BLOCK',
   job_action         =>  'begin
PSS.USAT_MASKCARDDATA();
end;',
   start_date         =>  SYSDATE,
   repeat_interval    =>  'FREQ=DAILY;BYHOUR=7;BYMINUTE=0;BYSECOND=0',
   enabled            =>  TRUE,
   comments           =>  'RFC0000328-PCI Mask Credit Card Data');
END;
/