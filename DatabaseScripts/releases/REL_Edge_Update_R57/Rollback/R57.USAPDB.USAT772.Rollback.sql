UPDATE REPORT.REPORT_PARAM SET PARAM_VALUE = 'select customer_name "Customer", device_serial_cd "Device", setting_name "Setting Name",
	case when stored_in_pennies = ''Y'' then to_char(dbadmin.to_number_or_null(device_setting_value) / 100) else device_setting_value end "Setting Value" from (
	select c.customer_name, d.device_serial_cd, coalesce(cts.alt_name, cts.name, dsp.device_setting_parameter_name, dsp.device_setting_parameter_cd) setting_name,
	case when cts.editor like ''SELECT:'' || ds.device_setting_value || ''=%'' or cts.editor like ''SELECT:%;'' || ds.device_setting_value || ''=%''
	then dbadmin.pkg_utl.get_delimited_field_value(regexp_replace(cts.editor, '':|;'', chr(28)) || chr(28), ds.device_setting_value)
	when cts.device_type_id = 0 and cts.field_offset between 0 and 511 and cts.data_mode = ''A'' and regexp_like(ds.device_setting_value, ''^[0-9A-Fa-f]+$'')
	and regexp_like(utl_raw.cast_to_varchar2(ds.device_setting_value), ''^[[:print:]]+$'') then utl_raw.cast_to_varchar2(ds.device_setting_value)
	else ds.device_setting_value end device_setting_value, cts.stored_in_pennies
	from device.device d
	left outer join report.eport e on d.device_serial_cd = e.eport_serial_num
	left outer join report.vw_terminal_eport te on e.eport_id = te.eport_id
	left outer join report.terminal t on te.terminal_id = t.terminal_id
	left outer join corp.customer c on t.customer_id = c.customer_id
	join report.vw_user_terminal ut on t.terminal_id = ut.terminal_id
	join device.device_setting ds on d.device_id = ds.device_id
	left outer join device.config_template_setting cts on case d.device_type_id when 1 then 0 else d.device_type_id end = cts.device_type_id
	and ds.device_setting_parameter_cd = cts.device_setting_parameter_cd and cts.customer_editable = ''Y''
	join device.device_setting_parameter dsp on ds.device_setting_parameter_cd = dsp.device_setting_parameter_cd
	where ut.user_id = ? and d.device_active_yn_flag = ''Y'' and (cts.device_setting_parameter_cd is not null or ds.device_setting_parameter_cd in (''Firmware Version''))
	) order by customer_name, device_serial_cd, setting_name'
  WHERE REPORT_ID = 241 AND PARAM_NAME = 'query';
ALTER TABLE DEVICE.CONFIG_TEMPLATE_SETTING DROP COLUMN CUSTOMER_VIEWABLE;
COMMIT;
