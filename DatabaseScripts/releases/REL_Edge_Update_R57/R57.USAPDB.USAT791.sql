INSERT INTO CORP.APP_SETTING(APP_SETTING_CD, APP_SETTING_VALUE, APP_SETTING_DESC)
SELECT 'MAXIMUM_ZIP_CODE_PROXIMITY', 600, 'In miles, maximum allowed distance to the nearest ZIP code'
FROM DUAL 
WHERE NOT EXISTS (SELECT 1 FROM CORP.APP_SETTING a WHERE a.APP_SETTING_CD = 'MAXIMUM_ZIP_CODE_PROXIMITY');

CREATE OR REPLACE PROCEDURE REPORT.GET_CITY_STATE_POSTAL_BY_COORD(
    pn_latitude NUMBER,
    pn_longitude NUMBER,
    pv_postal_cd    OUT FRONT.POSTAL.POSTAL_CD%TYPE,
    pv_city         OUT FRONT.POSTAL.CITY%TYPE,
    pv_state_cd     OUT CORP.STATE.STATE_CD%TYPE
)
IS
	ln_max_distance	NUMBER;
	ln_distance2	NUMBER;
	ln_pi		NUMBER;
	ln_coslat	NUMBER;
BEGIN
	SELECT 
		COALESCE(TO_NUMBER(a.APP_SETTING_VALUE), 1000)
	INTO
		ln_max_distance
	FROM CORP.APP_SETTING a
	WHERE a.APP_SETTING_CD = 'MAXIMUM_ZIP_CODE_PROXIMITY';

	ln_pi := ACOS(-1);
	ln_coslat := COS(ln_pi * pn_latitude / 180);

    SELECT p.POSTAL_CD, p.CITY, s.STATE_CD,
        (POWER(p.LATITUDE - pn_latitude, 2) + POWER(ln_coslat * (p.LONGITUDE - pn_longitude), 2))
    INTO pv_postal_cd, pv_city, pv_state_cd, ln_distance2
    FROM FRONT.POSTAL p JOIN CORP.STATE s ON p.STATE_ID = s.STATE_ID
    WHERE (POWER(p.LATITUDE - pn_latitude, 2) + POWER(ln_coslat * (p.LONGITUDE - pn_longitude), 2)) =
        (SELECT MIN(POWER(p1.LATITUDE - pn_latitude, 2) + POWER(ln_coslat * (p1.LONGITUDE - pn_longitude), 2))
             FROM FRONT.POSTAL p1) AND ROWNUM = 1;

    IF SQRT(ln_distance2)*69 > ln_max_distance THEN
        pv_postal_cd := 'Not Available';
        pv_city := 'Not Available';
        pv_state_cd := 'Not Available';
    END IF;
END;
/

GRANT EXECUTE ON REPORT.GET_CITY_STATE_POSTAL_BY_COORD TO USAT_DEV_READ_ONLY;
GRANT EXECUTE ON REPORT.GET_CITY_STATE_POSTAL_BY_COORD TO USALIVE_APP_ROLE;

COMMIT;
