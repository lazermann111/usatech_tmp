-- Campaign trigger

CREATE OR REPLACE TRIGGER REPORT.TRAU_CAMPAIGN AFTER 
UPDATE ON REPORT.CAMPAIGN FOR EACH ROW 
BEGIN
	IF COALESCE(:NEW.start_date, MIN_DATE) != COALESCE(:OLD.start_date, MIN_DATE) OR COALESCE(:NEW.end_date, MAX_DATE) != COALESCE(:OLD.end_date, MAX_DATE) THEN
		IF :NEW.start_date>=:OLD.start_date and :NEW.end_date<=:OLD.start_date THEN
			UPDATE PSS.CAMPAIGN_POS_PTA
			SET start_date=greatest(start_date, NVL(:NEW.start_date, MIN_DATE)),
				end_date=least(end_date, NVL(:NEW.end_date, MAX_DATE))
			where campaign_id=:NEW.campaign_id;
		ELSE
	  		FOR l_cur in (select pt.pos_pta_id, NVL(te.START_DATE, MIN_DATE) l_start_date, NVL(te.END_DATE, MAX_DATE) l_end_date
				from report.terminal_eport te join report.eport e on te.eport_id=e.eport_id
				join device.device d on d.device_serial_cd=e.eport_serial_num and device_active_yn_flag='Y'
				join pss.pos p on p.device_id=d.device_id
				join pss.pos_pta pt on p.pos_id=pt.pos_id
				where pt.pos_pta_id in (select pos_pta_id from PSS.CAMPAIGN_POS_PTA where campaign_id=:NEW.campaign_id)
					and  SYSDATE >= NVL(te.START_DATE, MIN_DATE) and SYSDATE    < NVL(te.END_DATE, MAX_DATE)) LOOP
			UPDATE PSS.CAMPAIGN_POS_PTA
			SET start_date=greatest(l_cur.l_start_date, NVL(:NEW.start_date, MIN_DATE)),
			end_date=least(l_cur.l_end_date, NVL(:NEW.end_date, MAX_DATE))
			where campaign_id=:NEW.campaign_id and pos_pta_id=l_cur.pos_pta_id;
			END LOOP;
		END IF;
	END IF;
END; 
/

-- Pass update to ECC

GRANT SELECT ON PSS.CUSTOMER_PASS_TYPE TO USAT_PREPAID_APP_ROLE;
GRANT SELECT ON PSS.CUSTOMER_PASS_TEMPLATE TO USAT_PREPAID_APP_ROLE;

GRANT SELECT, INSERT, UPDATE, DELETE ON PSS.CONSUMER_PASS TO USAT_PREPAID_APP_ROLE;
GRANT SELECT, INSERT, UPDATE, DELETE ON PSS.CONSUMER_PASS TO USALIVE_APP_ROLE;

UPDATE PSS.PASS_TYPE SET PASS_TEMPLATE_CD = 66358 WHERE PASS_TYPE_CD = 'p';
UPDATE PSS.PASS_TYPE SET PASS_TEMPLATE_CD = 66361 WHERE PASS_TYPE_CD = 'd';
UPDATE PSS.PASS_TYPE SET PASS_TEMPLATE_CD = 66390 WHERE PASS_TYPE_CD = 'c';
UPDATE PSS.CONSUMER_ACCT_TYPE_PASS_TMPL SET PASS_TEMPLATE_CD = 66358 WHERE CONSUMER_ACCT_TYPE_ID = 3;
UPDATE PSS.CONSUMER_ACCT_TYPE_PASS_TMPL SET PASS_TEMPLATE_CD = 66361 WHERE CONSUMER_ACCT_TYPE_ID = 7;
UPDATE PSS.CONSUMER_ACCT_TYPE_PASS_TMPL SET PASS_TEMPLATE_CD = 66390 WHERE CONSUMER_ACCT_TYPE_ID = 5;
DECLARE
  ln_customer_id NUMBER;
BEGIN
  SELECT MAX(CUSTOMER_ID) INTO ln_customer_id
  FROM CORP.CUSTOMER
  WHERE CUSTOMER_NAME like 'Boom Vending Inc%';
  IF ln_customer_id IS NOT NULL THEN
    UPDATE PSS.CUSTOMER_PASS_TYPE SET PASS_TEMPLATE_CD = 66401 WHERE CUSTOMER_ID = ln_customer_id AND PASS_TYPE_CD = 'p';
	UPDATE PSS.CUSTOMER_PASS_TYPE SET PASS_TEMPLATE_CD = 66402 WHERE CUSTOMER_ID = ln_customer_id AND PASS_TYPE_CD = 'c';
    UPDATE PSS.CUSTOMER_PASS_TEMPLATE SET PASS_TEMPLATE_ID = 66401 WHERE CUSTOMER_ID = ln_customer_id AND CONSUMER_ACCT_TYPE_ID = 3; 
    UPDATE PSS.CUSTOMER_PASS_TEMPLATE SET PASS_TEMPLATE_ID = 66402 WHERE CUSTOMER_ID = ln_customer_id AND CONSUMER_ACCT_TYPE_ID = 5; 
  END IF;
END;

/
COMMIT;	

-- Sales Rollup fields

UPDATE FOLIO_CONF.FIELD 
SET DISPLAY_EXPRESSION = 'CASE WHEN REPORT.CAMPAIGN.CUSTOMER_ID IS NULL THEN NULL ELSE REPORT.CAMPAIGN.CAMPAIGN_NAME END',
    SORT_EXPRESSION = 'CASE WHEN REPORT.CAMPAIGN.CUSTOMER_ID IS NULL THEN NULL ELSE REPORT.CAMPAIGN.CAMPAIGN_NAME END'
WHERE FIELD_ID = 9448;

UPDATE FOLIO_CONF.FIELD 
SET DISPLAY_EXPRESSION = 'CASE WHEN REPORT.CAMPAIGN.CUSTOMER_ID IS NULL THEN 0 ELSE NVL(REPORT.TRANS_STAT_BY_DAY.PURCHASE_DISCOUNT,0) END',
    SORT_EXPRESSION = 'CASE WHEN REPORT.CAMPAIGN.CUSTOMER_ID IS NULL THEN 0 ELSE NVL(REPORT.TRANS_STAT_BY_DAY.PURCHASE_DISCOUNT,0) END'
WHERE FIELD_ID = 9609;

UPDATE FOLIO_CONF.FIELD 
SET DISPLAY_EXPRESSION = 'CASE WHEN REPORT.CAMPAIGN.CUSTOMER_ID IS NULL THEN 0 ELSE NVL(REPORT.TRANS_STAT_BY_DAY.LOYALTY_DISCOUNT,0) END',
    SORT_EXPRESSION = 'CASE WHEN REPORT.CAMPAIGN.CUSTOMER_ID IS NULL THEN 0 ELSE NVL(REPORT.TRANS_STAT_BY_DAY.LOYALTY_DISCOUNT,0) END'
WHERE FIELD_ID = 2912;

COMMIT;


-- Update Trans info in PKG
CREATE OR REPLACE PACKAGE BODY REPORT.DW_PKG IS
    FUNCTION GET_OR_CREATE_EXPORT_BATCH(
        l_customer_id CORP.CUSTOMER.CUSTOMER_ID%TYPE)
     RETURN ACTIVITY_REF.BATCH_ID%TYPE
    IS
    PRAGMA AUTONOMOUS_TRANSACTION;
        l_export_id ACTIVITY_REF.BATCH_ID%TYPE;
        l_lock VARCHAR2(128);
    BEGIN
        l_lock := GLOBALS_PKG.REQUEST_LOCK('EXPORT.CUSTOMER_ID',l_customer_id);
        SELECT eb.batch_id
          INTO l_export_id
          FROM REPORT.EXPORT_BATCH eb
         WHERE eb.CUSTOMER_ID = l_customer_id
           AND eb.EXPORT_TYPE = 1
           AND eb.STATUS = 'O';
        COMMIT;
        RETURN l_export_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            SELECT REPORT.EXPORT_BATCH_SEQ.NEXTVAL
              INTO l_export_id
              FROM DUAL;
            
            INSERT INTO REPORT.EXPORT_BATCH(
                   BATCH_ID,
                   CUSTOMER_ID,
                   EXPORT_TYPE,
                   STATUS)
               SELECT
                   l_export_id,
                   l_customer_id,
                   1,
                   'O'
                 FROM DUAL
                WHERE NOT EXISTS(
                    SELECT 1
                      FROM REPORT.EXPORT_BATCH eb
                     WHERE eb.CUSTOMER_ID = l_customer_id
                       AND eb.EXPORT_TYPE = 1
                       AND eb.STATUS = 'O');
            COMMIT;
            IF SQL%FOUND THEN
                RETURN l_export_id;
            ELSE
                RETURN GET_OR_CREATE_EXPORT_BATCH(l_customer_id);
            END IF;
        WHEN OTHERS THEN
            RAISE;
    END;
    
    PROCEDURE CLOSE_EXPORT_BATCH(
        l_export_id REPORT.EXPORT_BATCH.BATCH_ID%TYPE,
        l_customer_id REPORT.EXPORT_BATCH.CUSTOMER_ID%TYPE)
    IS
        l_lock VARCHAR2(128);
    BEGIN
        l_lock := GLOBALS_PKG.REQUEST_LOCK('EXPORT.CUSTOMER_ID',l_customer_id);
        UPDATE REPORT.EXPORT_BATCH EB SET (
                STATUS,
                TRAN_CREATE_DT_BEG,
                TRAN_CREATE_DT_END,
                TOT_TRAN_ROWS,
                TOT_TRAN_AMOUNT
              ) = (SELECT
                'A',
                MIN(A.TRAN_DATE),
                MAX(A.TRAN_DATE),
                COUNT(*),
                SUM(A.TOTAL_AMOUNT)
              FROM REPORT.ACTIVITY_REF A
             WHERE A.BATCH_ID = eb.BATCH_ID)
         WHERE EB.BATCH_ID = l_export_id
           AND EB.STATUS = 'O';
         CORP.PAYMENTS_PKG.UPDATE_EXPORT_BATCH(l_export_id);
    END;
    
    PROCEDURE CLOSE_EXPORT_BATCHES
    IS
        CURSOR l_cur IS
             SELECT EB.BATCH_ID, EB.CUSTOMER_ID
              FROM REPORT.EXPORT_BATCH EB
              JOIN (SELECT CUSTOMER_ID, 
              CASE WHEN BATCH_CLOSE_DATE > SYSDATE THEN BATCH_CLOSE_DATE - 1 ELSE BATCH_CLOSE_DATE END BATCH_CLOSE_DATE 
              FROM (SELECT CUSTOMER_ID, TO_DATE(TO_CHAR(SYSDATE, 'MM/DD/YYYY')||' '||NVL(BATCH_CLOSE_TIME,'05:00'), 'MM/DD/YYYY HH24:MI') BATCH_CLOSE_DATE 
              FROM CORP.CUSTOMER)) C ON EB.CUSTOMER_ID = C.CUSTOMER_ID
            WHERE EB.STATUS = 'O'
              AND EB.CRD_DATE < C.BATCH_CLOSE_DATE
              AND SYSDATE > C.BATCH_CLOSE_DATE;
    BEGIN
        FOR l_rec IN l_cur LOOP
            CLOSE_EXPORT_BATCH(l_rec.BATCH_ID, l_rec.CUSTOMER_ID);
            COMMIT;
        END LOOP;
    END;
    
    PROCEDURE CLOSE_EXPORT_BATCH_FOR(
        l_customer_id CORP.CUSTOMER.CUSTOMER_ID%TYPE)
    IS
        CURSOR l_cur IS
            SELECT BATCH_ID
              FROM REPORT.EXPORT_BATCH
            WHERE STATUS = 'O'
              AND CUSTOMER_ID = l_customer_id;
    BEGIN
        FOR l_rec IN l_cur LOOP
            CLOSE_EXPORT_BATCH(l_rec.BATCH_ID, l_customer_id);
            COMMIT;
        END LOOP;
    END;
    
    PROCEDURE UPDATE_SETTLEMENT(
        l_trans_id TRANS.TRAN_ID%TYPE,
        l_settle_state_id TRANS.SETTLE_STATE_ID%TYPE,
        l_settle_date TRANS.SETTLE_DATE%TYPE,
        l_appr_cd TRANS.CC_APPR_CODE%TYPE)
    IS
    BEGIN
        UPDATE ACTIVITY_REF SET CC_APPR_CODE = l_appr_cd,
            SETTLE_STATE = (SELECT STATE_LABEL FROM TRANS_STATE S
                WHERE l_settle_state_id = S.STATE_ID)
         WHERE TRAN_ID = l_trans_id;
    END;

PROCEDURE UPDATE_TRAN_INFO(
        l_trans_id TRANS.TRAN_ID%TYPE)
    IS
        l_ref_nbr ACTIVITY_REF.REF_NBR%TYPE;
        l_export_id ACTIVITY_REF.BATCH_ID%TYPE;
        l_create_dt ACTIVITY_REF.CREATE_DT%TYPE;
        l_update_dt ACTIVITY_REF.UPDATE_DT%TYPE;
        l_qty ACTIVITY_REF.QUANTITY%TYPE;
        l_terminal_id ACTIVITY_REF.TERMINAL_ID%TYPE;
        l_eport_id ACTIVITY_REF.EPORT_ID%TYPE;
        l_tran_date ACTIVITY_REF.TRAN_DATE%TYPE;
        l_trans_type_id ACTIVITY_REF.TRANS_TYPE_ID%TYPE;
        l_total_amount ACTIVITY_REF.TOTAL_AMOUNT%TYPE;
        l_currency_id ACTIVITY_REF.CURRENCY_ID%TYPE;
        l_customer_id CORP.CUSTOMER.CUSTOMER_ID%TYPE;
        l_convenience_fee ACTIVITY_REF.CONVENIENCE_FEE%TYPE;
        l_loyalty_discount ACTIVITY_REF.LOYALTY_DISCOUNT%TYPE;
        l_consumer_acct_id ACTIVITY_REF.CONSUMER_ACCT_ID%TYPE;
        lc_payment_entry_method_cd PSS.PAYMENT_ENTRY_METHOD.PAYMENT_ENTRY_METHOD_CD%TYPE;
        lv_payment_entry_method_desc PSS.PAYMENT_ENTRY_METHOD.PAYMENT_ENTRY_METHOD_DESC%TYPE;
        lv_payment_subtype_class PSS.TRAN.PAYMENT_SUBTYPE_CLASS%TYPE;
        lv_card_company REPORT.ACTIVITY_REF.CARD_COMPANY%TYPE;
        l_payment_batch_id CORP.BATCH.BATCH_ID%TYPE;
        ln_cnt PLS_INTEGER;
        l_donate_count NUMBER;
        l_donate_yes NUMBER;
        lc_non_payable_entry_type CHAR(2);
        lc_conv_fee_entry_type CHAR(2);
        l_campaign_id NUMBER:=0;
        l_purchase_discount ACTIVITY_REF.PURCHASE_DISCOUNT%TYPE;
		ln_is_consumer_acct NUMBER;
		
        CURSOR l_cur IS
         SELECT
            T.CUSTOMER_BANK_ID,
            R.REGION_ID,
            R.REGION_NAME,
            L.LOCATION_NAME,
            L.LOCATION_ID,
            T.TERMINAL_ID,
            TERM.TERMINAL_NAME,
            TERM.TERMINAL_NBR,
            T.TRANS_TYPE_ID,
            TT.TRANS_TYPE_NAME,
            S.STATE_LABEL,
            T.CARD_NUMBER,
            T.CC_APPR_CODE,
            T.CLOSE_DATE,
            T.TOTAL_AMOUNT,
            T.ORIG_TRAN_ID,
            T.EPORT_ID,
            T.DESCRIPTION,
            T.CURRENCY_ID,
            T.CARDTYPE_AUTHORITY_ID,
            TERM.CUSTOMER_ID,
            SUM(CASE WHEN P.TRAN_LINE_ITEM_TYPE_ID = 203 OR (TRAN_LINE_ITEM_TYPE_ID IS NULL AND P.VEND_COLUMN IN ('Two-Tier Pricing', 'Convenience Fee')) THEN P.AMOUNT * NVL(P.PRICE,0) END) CONVENIENCE_FEE,
            SUM(CASE WHEN P.TRAN_LINE_ITEM_TYPE_ID = 204 THEN P.AMOUNT * NVL(P.PRICE,0) END) LOYALTY_DISCOUNT,
            SUM(CASE WHEN P.TRAN_LINE_ITEM_TYPE_ID = 211 THEN P.AMOUNT * NVL(P.PRICE,0) END) MORE_LOYALTY_DISCOUNT,
            SUM(CASE WHEN P.TRAN_LINE_ITEM_TYPE_ID = 213 THEN P.AMOUNT * NVL(P.PRICE,0) END) PURCHASE_DISCOUNT,
            T.CONSUMER_ACCT_ID,
            TT.CREDIT_IND,
            TT.CASH_IND,
            TT.PAYABLE_IND,
            CASE WHEN TT.CASH_IND = 'Y' THEN 'CA' WHEN TT.PAYABLE_IND != 'Y' THEN 'NP' END NON_PAYABLE_ENTRY_TYPE,
            CASE WHEN TT.PAYABLE_IND = 'Y' THEN 'TT' END CONV_FEE_ENTRY_TYPE,
            MAX(CASE WHEN P.TRAN_LINE_ITEM_TYPE_ID = 555 THEN P.APPLY_TO_CONSUMER_ACCT_ID END) APPLY_TO_CONSUMER_ACCT_ID,
            T.SOURCE_TRAN_ID,
            T.MACHINE_TRANS_NO,
            T.TRAN_DEVICE_TRAN_CD,
            T.CONSUMER_PASS_ID
          FROM REPORT.TRANS T join REPORT.TRANS_TYPE TT on T.TRANS_TYPE_ID = TT.TRANS_TYPE_ID
          JOIN REPORT.TRANS_STATE S on T.SETTLE_STATE_ID = S.STATE_ID
          LEFT OUTER JOIN REPORT.PURCHASE P on T.TRAN_ID = P.TRAN_ID
          LEFT OUTER JOIN REPORT.TERMINAL TERM on T.TERMINAL_ID = TERM.TERMINAL_ID
          LEFT OUTER JOIN REPORT.LOCATION L on TERM.LOCATION_ID = L.LOCATION_ID
          LEFT OUTER JOIN REPORT.TERMINAL_REGION TR on TERM.TERMINAL_ID = TR.TERMINAL_ID
          LEFT OUTER JOIN REPORT.REGION R on TR.REGION_ID = R.REGION_ID
          WHERE T.TRAN_ID = l_trans_id
          GROUP BY
            T.CUSTOMER_BANK_ID,
            R.REGION_ID,
            R.REGION_NAME,
            L.LOCATION_NAME,
            L.LOCATION_ID,
            T.TERMINAL_ID,
            TERM.TERMINAL_NAME,
            TERM.TERMINAL_NBR,
            T.TRANS_TYPE_ID,
            TT.TRANS_TYPE_NAME,
            S.STATE_LABEL,
            T.CARD_NUMBER,
            T.CC_APPR_CODE,
            T.CLOSE_DATE,
            T.TOTAL_AMOUNT,
            T.ORIG_TRAN_ID,
            T.EPORT_ID,
            T.DESCRIPTION,
            T.CURRENCY_ID,
            T.CARDTYPE_AUTHORITY_ID,
            TERM.CUSTOMER_ID,
            T.CONSUMER_ACCT_ID,
            TT.CREDIT_IND,
            TT.CASH_IND,
            TT.PAYABLE_IND,
            T.SOURCE_TRAN_ID,
            T.MACHINE_TRANS_NO,
            T.TRAN_DEVICE_TRAN_CD,
            T.CONSUMER_PASS_ID;
    BEGIN
        DELETE FROM REPORT.ACTIVITY_REF
         WHERE TRAN_ID = L_TRANS_ID
         RETURNING REF_NBR, BATCH_ID, CREATE_DT, SYSDATE, TERMINAL_ID, EPORT_ID, TRAN_DATE, TRANS_TYPE_ID, TOTAL_AMOUNT, QUANTITY, CURRENCY_ID, CONVENIENCE_FEE, LOYALTY_DISCOUNT, CONSUMER_ACCT_ID, PAYMENT_BATCH_ID,PURCHASE_DISCOUNT
              INTO l_ref_nbr, l_export_id, l_create_dt, l_update_dt, l_terminal_id, l_eport_id, l_tran_date, l_trans_type_id, l_total_amount, l_qty, l_currency_id, l_convenience_fee, l_loyalty_discount,l_consumer_acct_id, l_payment_batch_id,l_purchase_discount;       
        
        select COALESCE(max(campaign_id), 0) into l_campaign_id from PSS.TRAN_LINE_ITEM
        where tran_line_item_type_id in (204, 211, 213) and tran_id= (select source_tran_id from report.trans where tran_id=L_TRANS_ID);
        
        IF l_campaign_id = 0 THEN
          select COALESCE(max(to_number(auth_authority_ref_cd)), 0) into l_campaign_id from PSS.AUTH
          where tran_id= (select source_tran_id from report.trans where tran_id=L_TRANS_ID) and auth_authority_tran_cd='Promotion' and AUTH_TYPE_CD = 'N';
        END IF;
        
        -- if exists, then update the trans_stat_by_day
        IF NVL(l_qty, 0) != 0 AND l_create_dt is NOT NULL THEN
            UPDATE REPORT.TRANS_STAT_BY_DAY 
               SET TRAN_COUNT = NVL(tran_count, 0) - 1,
                   VEND_COUNT = NVL(VEND_COUNT, 0) - l_qty,
                   TRAN_AMOUNT = NVL(TRAN_AMOUNT, 0) - NVL(l_total_amount, 0),
                   CONVENIENCE_FEE = NVL(CONVENIENCE_FEE, 0) - NVL(l_convenience_fee, 0),
                   LOYALTY_DISCOUNT = NVL(LOYALTY_DISCOUNT, 0) - NVL(l_loyalty_discount, 0),
                   TRAN_AMOUNT_S = DBADMIN.WELFORD_REVERSE_S(TRAN_COUNT, TRAN_AMOUNT, TRAN_AMOUNT_S, l_total_amount/l_qty),
                   TRAN_AMOUNT_STDEV = DBADMIN.WELFORD_REVERSE_STDEV(TRAN_COUNT, TRAN_AMOUNT, TRAN_AMOUNT_S, l_total_amount/l_qty),
                   PURCHASE_DISCOUNT = NVL(PURCHASE_DISCOUNT, 0) - NVL(l_purchase_discount, 0)
             WHERE TERMINAL_ID = l_terminal_id
               AND EPORT_ID = l_eport_id
               AND TRAN_DATE = trunc(l_tran_date, 'DD')
               AND TRANS_TYPE_ID = l_trans_type_id
               AND CURRENCY_ID = l_currency_id
               AND CAMPAIGN_ID = l_campaign_id;
               
               IF l_campaign_id > 0 THEN
                  UPDATE REPORT.CONSUMER_ACCT_STAT 
                  SET TRAN_COUNT = NVL(tran_count, 0) - 1,
                   VEND_COUNT = NVL(VEND_COUNT, 0) - l_qty,
                   TRAN_AMOUNT = NVL(TRAN_AMOUNT, 0) - NVL(l_total_amount, 0),
                   CONVENIENCE_FEE = NVL(CONVENIENCE_FEE, 0) - NVL(l_convenience_fee, 0),
                   LOYALTY_DISCOUNT = NVL(LOYALTY_DISCOUNT, 0) - NVL(l_loyalty_discount, 0),
                   PURCHASE_DISCOUNT = NVL(PURCHASE_DISCOUNT, 0) - NVL(l_purchase_discount, 0)
                   WHERE CONSUMER_ACCT_ID = l_consumer_acct_id
                     AND CURRENCY_ID = l_currency_id
                     AND CAMPAIGN_ID = l_campaign_id;
               END IF;
            
            IF l_payment_batch_id IS NOT NULL THEN         
                SELECT CASE WHEN TT.CASH_IND = 'Y' THEN 'CA' WHEN TT.PAYABLE_IND != 'Y' THEN 'NP' END,
                       CASE WHEN TT.PAYABLE_IND = 'Y' THEN 'TT' END                  
                  INTO lc_non_payable_entry_type, lc_conv_fee_entry_type
                  FROM REPORT.TRANS_TYPE TT
                 WHERE TT.TRANS_TYPE_ID = l_trans_type_id;
                 
                IF lc_non_payable_entry_type IS NOT NULL THEN               
                    UPDATE CORP.BATCH_TOTAL
                       SET LEDGER_AMOUNT = LEDGER_AMOUNT - l_total_amount,
                           LEDGER_COUNT = LEDGER_COUNT - 1
                     WHERE BATCH_ID = l_payment_batch_id
                       AND ENTRY_TYPE = lc_non_payable_entry_type;
                END IF;  
                IF lc_conv_fee_entry_type IS NOT NULL AND l_convenience_fee > 0 THEN               
                    UPDATE CORP.BATCH_TOTAL
                       SET LEDGER_AMOUNT = LEDGER_AMOUNT - l_convenience_fee,
                           LEDGER_COUNT = LEDGER_COUNT - 1
                     WHERE BATCH_ID = l_payment_batch_id
                       AND ENTRY_TYPE = lc_conv_fee_entry_type;
                END IF;
            END IF;
        END IF;
        COMMIT;
        FOR l_rec IN l_cur LOOP
            IF NVL(l_export_id, 0) = 0 THEN
                IF l_rec.customer_id IS NULL THEN
                    l_export_id := 0;
                ELSE
                    l_export_id := GET_OR_CREATE_EXPORT_BATCH(l_rec.customer_id);
                END IF;
            END IF;
            IF l_ref_nbr IS NULL THEN
                SELECT REF_NBR_SEQ.NEXTVAL
                  INTO l_ref_nbr 
                  FROM DUAL;
            END IF;
            SELECT NVL(SUM(P.AMOUNT), 0)
              INTO l_qty
              FROM REPORT.PURCHASE P
              LEFT OUTER JOIN PSS.TRAN_LINE_ITEM_TYPE IT ON P.TRAN_LINE_ITEM_TYPE_ID = IT.TRAN_LINE_ITEM_TYPE_ID
             WHERE P.TRAN_ID = l_trans_id
              AND (IT.TRAN_LINE_ITEM_TYPE_GROUP_CD IN('S', 'P')
               OR (P.TRAN_LINE_ITEM_TYPE_ID IS NULL AND P.VEND_COLUMN NOT IN ('Two-Tier Pricing', 'Convenience Fee')));
               
            IF NVL(l_qty,0) = 0 THEN
              -- if qty is zero and not a refund/chargeback then set qty = 1 
              SELECT DECODE(COUNT(1), 1, 0, 1)
              INTO l_qty
              FROM REPORT.TRANS_TYPE TT
              WHERE TT.REFUND_IND = 'Y'
              AND TT.TRANS_TYPE_ID = l_trans_type_id;
            END IF;
            IF l_rec.SOURCE_TRAN_ID IS NOT NULL THEN
                SELECT MAX(PEM.PAYMENT_ENTRY_METHOD_CD), COALESCE(MAX(AEM.ACCT_ENTRY_METHOD_NAME), MAX(PEM.PAYMENT_ENTRY_METHOD_DESC)), MAX(X.PAYMENT_SUBTYPE_CLASS)
                  INTO lc_payment_entry_method_cd, lv_payment_entry_method_desc, lv_payment_subtype_class
                  FROM PSS.TRAN X
                  JOIN PSS.CLIENT_PAYMENT_TYPE CPT ON X.CLIENT_PAYMENT_TYPE_CD = CPT.CLIENT_PAYMENT_TYPE_CD
                  LEFT JOIN PSS.PAYMENT_ENTRY_METHOD PEM ON CPT.PAYMENT_ENTRY_METHOD_CD = PEM.PAYMENT_ENTRY_METHOD_CD AND PEM.PAYMENT_ENTRY_METHOD_CD NOT IN ('M', 'S')
				  LEFT OUTER JOIN PSS.AUTH A ON X.TRAN_ID = A.TRAN_ID AND A.AUTH_TYPE_CD IN('L','N') AND A.AUTH_STATE_ID IN (2,5)
				  LEFT OUTER JOIN PSS.ACCT_ENTRY_METHOD AEM ON A.ACCT_ENTRY_METHOD_CD = AEM.ACCT_ENTRY_METHOD_CD
                 WHERE X.TRAN_ID = l_rec.SOURCE_TRAN_ID;     
            ELSIF l_rec.MACHINE_TRANS_NO IS NOT NULL THEN
                SELECT MAX(PEM.PAYMENT_ENTRY_METHOD_CD), COALESCE(MAX(AEM.ACCT_ENTRY_METHOD_NAME), MAX(PEM.PAYMENT_ENTRY_METHOD_DESC)), MAX(X.PAYMENT_SUBTYPE_CLASS)
                  INTO lc_payment_entry_method_cd, lv_payment_entry_method_desc, lv_payment_subtype_class
                  FROM PSS.TRAN X
                  JOIN PSS.CLIENT_PAYMENT_TYPE CPT ON X.CLIENT_PAYMENT_TYPE_CD = CPT.CLIENT_PAYMENT_TYPE_CD
                  LEFT JOIN PSS.PAYMENT_ENTRY_METHOD PEM ON CPT.PAYMENT_ENTRY_METHOD_CD = PEM.PAYMENT_ENTRY_METHOD_CD AND PEM.PAYMENT_ENTRY_METHOD_CD NOT IN ('M', 'S')
				  LEFT OUTER JOIN PSS.AUTH A ON X.TRAN_ID = A.TRAN_ID AND A.AUTH_TYPE_CD IN('L','N') AND A.AUTH_STATE_ID IN (2,5)
				  LEFT OUTER JOIN PSS.ACCT_ENTRY_METHOD AEM ON A.ACCT_ENTRY_METHOD_CD = AEM.ACCT_ENTRY_METHOD_CD
                 WHERE X.TRAN_GLOBAL_TRANS_CD = l_rec.MACHINE_TRANS_NO;
            END IF;
      IF l_rec.TRANS_TYPE_ID in(63,64,65) THEN
          select COALESCE(max(auth_authority_ref_cd),'') into lv_card_company  from PSS.AUTH
          where tran_id= (select source_tran_id from report.trans where tran_id=L_TRANS_ID) and AUTH_TYPE_CD = 'N';
      ELSE
        lv_card_company := REPORT.CARD_NAME(l_rec.CARDTYPE_AUTHORITY_ID, l_rec.TRANS_TYPE_ID, l_rec.CARD_NUMBER);
        IF lv_payment_subtype_class = 'Demo' THEN
          lv_card_company := lv_card_company || ' Demo';
        END IF;
        IF lv_payment_entry_method_desc IS NOT NULL THEN
          IF l_rec.card_number LIKE '539648%' AND lc_payment_entry_method_cd = 'C' THEN
            lv_card_company := lv_card_company || ' (Google Wallet)';
          ELSIF lv_card_company != lv_payment_entry_method_desc THEN
            lv_card_company := lv_card_company || ' (' || lv_payment_entry_method_desc || ')';
          END IF;
        END IF;
      END IF;
        
     
        IF l_rec.customer_id is not null AND l_rec.customer_bank_id is not null AND (l_rec.NON_PAYABLE_ENTRY_TYPE IS NOT NULL  OR (l_rec.CONV_FEE_ENTRY_TYPE IS NOT NULL AND l_rec.convenience_fee > 0)) THEN
          l_payment_batch_id:=CORP.PAYMENTS_PKG.GET_OR_CREATE_BATCH(l_rec.TERMINAL_ID,l_rec.customer_bank_id,l_rec.CLOSE_DATE,l_rec.currency_id,'N');
        END IF;
            INSERT INTO ACTIVITY_REF(
                REGION_ID,
                REGION_NAME,
                LOCATION_NAME,
                LOCATION_ID,
                TERMINAL_ID,
                TERMINAL_NAME,
                TERMINAL_NBR,
                TRAN_ID,
                TRANS_TYPE_ID,
                TRANS_TYPE_NAME,
                SETTLE_STATE,
                CARD_NUMBER,
                CARD_COMPANY,
                CC_APPR_CODE,
                TRAN_DATE,
                FILL_DATE,
                TOTAL_AMOUNT,
                ORIG_TRAN_ID,
                BATCH_ID,
                VEND_COLUMN,
                QUANTITY,
                REF_NBR,
                EPORT_ID,
                DESCRIPTION,
                CREATE_DT,
                UPDATE_DT,
                CURRENCY_ID,
                CONVENIENCE_FEE,
                LOYALTY_DISCOUNT,
                CONSUMER_ACCT_ID,
                PAYMENT_BATCH_ID,
                CAMPAIGN_ID,
                TRAN_DEVICE_TRAN_CD,
                PURCHASE_DISCOUNT,
                CONSUMER_PASS_ID
                )
            SELECT
                l_rec.REGION_ID,
                l_rec.REGION_NAME,
                NVL(l_rec.LOCATION_NAME, 'Unknown'),
                NVL(l_rec.LOCATION_ID, 0),
                l_rec.TERMINAL_ID,
                l_rec.TERMINAL_NAME,
                l_rec.TERMINAL_NBR,
                l_trans_id,
                l_rec.TRANS_TYPE_ID,
                l_rec.TRANS_TYPE_NAME,
                l_rec.STATE_LABEL,
                l_rec.CARD_NUMBER,
                lv_card_company,
                l_rec.CC_APPR_CODE,
                l_rec.CLOSE_DATE TRAN_DATE,
                GET_FILL_DATE(l_trans_id) FILL_DATE,
                l_rec.TOTAL_AMOUNT,
                l_rec.ORIG_TRAN_ID,
                l_export_id,
                VEND_COLUMN_STRING(l_trans_id),
                l_qty,
                l_ref_nbr,
                l_rec.EPORT_ID,
                l_rec.DESCRIPTION,
                NVL(l_create_dt, SYSDATE),
                l_update_dt,
                l_rec.currency_id,
                l_rec.convenience_fee,
                CASE WHEN l_campaign_id > 0 THEN COALESCE(l_rec.more_loyalty_discount, 0)+COALESCE(l_rec.loyalty_discount, 0) ELSE l_rec.loyalty_discount END,
                l_rec.consumer_acct_id,
                l_payment_batch_id,
                l_campaign_id,
                l_rec.TRAN_DEVICE_TRAN_CD,
                l_rec.PURCHASE_DISCOUNT,
                l_rec.CONSUMER_PASS_ID
              FROM DUAL;
            IF l_rec.APPLY_TO_CONSUMER_ACCT_ID is not null THEN
              select count(1) into l_donate_yes from
              PSS.CONSUMER_ACCT CA 
              JOIN PSS.CONSUMER C ON C.CONSUMER_ID = CA.CONSUMER_ID
              LEFT OUTER JOIN (PSS.CONSUMER_SETTING CS
              JOIN PSS.CONSUMER_SETTING_PARAM CSP ON CSP.CONSUMER_SETTING_PARAM_ID = CS.CONSUMER_SETTING_PARAM_ID)
              ON CS.CONSUMER_ID = C.CONSUMER_ID AND CSP.CONSUMER_COMMUNICATION_TYPE ='DONATE_CHARITY'
              WHERE CA.CONSUMER_ACCT_ID=l_rec.APPLY_TO_CONSUMER_ACCT_ID
              AND NVL(CS.CONSUMER_SETTING_VALUE,'N') = 'Y';
              IF l_donate_yes !=0  THEN
                  INSERT INTO REPORT.PREPAID_DONATION (REPORT_TRAN_ID, CONSUMER_ACCT_ID, BASE_AMOUNT, DONATION_AMOUNT)
                  SELECT l_trans_id, l_rec.APPLY_TO_CONSUMER_ACCT_ID, l_rec.TOTAL_AMOUNT, ROUND(l_rec.TOTAL_AMOUNT * c.donation_percent, 2)
                  FROM PSS.TRAN_LINE_ITEM tli join report.campaign c on tli.campaign_id=c.campaign_id 
                  where tran_id=(select source_tran_id from report.trans where tran_id=l_trans_id)
                  and not exists (select 1 from REPORT.PREPAID_DONATION where report_tran_id=l_trans_id)
                  and c.donation_percent is not null;
              END IF;
            END IF;
            IF l_rec.TRANS_TYPE_ID in (20,31) THEN
              select count(1) into l_donate_count from report.prepaid_donation pd join report.trans t on pd.report_tran_id=t.tran_id
              and t.ORIG_TRAN_ID=(select tran_id from report.trans t2 join report.trans_type tt on t2.trans_type_id=tt.trans_type_id 
              where tran_id=l_rec.ORIG_TRAN_ID and tt.replenish_ind='Y') and t.trans_type_id=22;
              
              IF l_donate_count = 1 THEN
                  INSERT INTO REPORT.PREPAID_DONATION (REPORT_TRAN_ID, CONSUMER_ACCT_ID, BASE_AMOUNT, DONATION_AMOUNT)
                  select l_trans_id, pd.CONSUMER_ACCT_ID, 
                  case when orig.total_amount+rt.total_amount>=0 THEN round(pd.base_amount*(l_rec.TOTAL_AMOUNT/orig.total_amount),2)
                  ELSE (pd.base_amount+pdr.base_amount_sum)*-1 END,
                  case when orig.total_amount+rt.total_amount>=0 THEN round(pd.DONATION_AMOUNT*(l_rec.TOTAL_AMOUNT/orig.total_amount),2)
                  ELSE (pd.DONATION_AMOUNT+pdr.donation_amount_sum)*-1 END
                  from report.prepaid_donation pd,
                  (select sum(base_amount) base_amount_sum, sum(donation_amount) donation_amount_sum from report.prepaid_donation where report_tran_id in (select tran_id from report.trans t where t.ORIG_TRAN_ID=l_rec.ORIG_TRAN_ID and t.trans_type_id in (20,31))) pdr,
                  (select total_amount from report.trans where tran_id=l_rec.ORIG_TRAN_ID) orig,
                  (select sum(t.total_amount) total_amount from report.trans t where t.ORIG_TRAN_ID=l_rec.ORIG_TRAN_ID and t.trans_type_id in (20,31) ) rt
                  where pd.report_tran_id=(select tran_id from report.trans t where t.ORIG_TRAN_ID=l_rec.ORIG_TRAN_ID and t.trans_type_id =22)
                  and not exists (select 1 from REPORT.PREPAID_DONATION where report_tran_id=l_trans_id);
              END IF;
            END IF;
            -- update trans_stat_by_day
            IF l_qty != 0  OR l_rec.TOTAL_AMOUNT != 0 THEN
                LOOP
                    UPDATE REPORT.TRANS_STAT_BY_DAY
                       SET TRAN_COUNT = NVL(TRAN_COUNT, 0) + 1,
                           VEND_COUNT = NVL(VEND_COUNT, 0) + l_qty,
                           TRAN_AMOUNT = NVL(TRAN_AMOUNT, 0) + NVL(l_rec.TOTAL_AMOUNT, 0),
                           CONVENIENCE_FEE = NVL(CONVENIENCE_FEE, 0) + NVL(l_rec.CONVENIENCE_FEE, 0),
                           LOYALTY_DISCOUNT = CASE WHEN l_campaign_id > 0 THEN NVL(LOYALTY_DISCOUNT, 0) + COALESCE(l_rec.more_loyalty_discount, 0)+COALESCE(l_rec.loyalty_discount, 0) ELSE NVL(LOYALTY_DISCOUNT, 0) + NVL(l_rec.loyalty_discount, 0) END,
                           TRAN_AMOUNT_S = DBADMIN.WELFORD_S(TRAN_COUNT, TRAN_AMOUNT, TRAN_AMOUNT_S, l_rec.TOTAL_AMOUNT/l_qty),
                           TRAN_AMOUNT_STDEV = DBADMIN.WELFORD_STDEV(TRAN_COUNT, TRAN_AMOUNT, TRAN_AMOUNT_S, l_rec.TOTAL_AMOUNT/l_qty),
                           PURCHASE_DISCOUNT = NVL(PURCHASE_DISCOUNT, 0) + NVL(l_rec.PURCHASE_DISCOUNT, 0)
                     WHERE TERMINAL_ID = l_rec.TERMINAL_ID
                       AND EPORT_ID = l_rec.EPORT_ID
                       AND TRAN_DATE = trunc(l_rec.CLOSE_DATE, 'DD')
                       AND TRANS_TYPE_ID = l_rec.TRANS_TYPE_ID
                       AND CURRENCY_ID = l_rec.CURRENCY_ID
                       AND CAMPAIGN_ID = l_campaign_id;
                    EXIT WHEN SQL%ROWCOUNT > 0;
                    BEGIN
                        INSERT INTO REPORT.TRANS_STAT_BY_DAY(
                            TERMINAL_ID,
                            EPORT_ID,
                            TRAN_DATE, 
                            TRANS_TYPE_ID,
                            TRAN_COUNT,
                            VEND_COUNT, 
                            TRAN_AMOUNT,
                            CURRENCY_ID, 
                            CONVENIENCE_FEE,
                            LOYALTY_DISCOUNT,
                            CAMPAIGN_ID,
                            TRAN_AMOUNT_S,
                            PURCHASE_DISCOUNT) 
                        SELECT
                            l_rec.TERMINAL_ID,
                            l_rec.EPORT_ID,
                            trunc(l_rec.CLOSE_DATE, 'DD'),
                            l_rec.TRANS_TYPE_ID, 
                            1,
                            l_qty,
                            NVL(l_rec.TOTAL_AMOUNT, 0),
                            l_rec.CURRENCY_ID,
                            l_rec.CONVENIENCE_FEE,
                            CASE WHEN l_campaign_id > 0 THEN COALESCE(l_rec.more_loyalty_discount, 0)+COALESCE(l_rec.loyalty_discount, 0) ELSE l_rec.loyalty_discount END,
                            l_campaign_id,
                            0,
                            l_rec.PURCHASE_DISCOUNT
                        FROM DUAL;
                        EXIT;    
                    EXCEPTION
                        WHEN DUP_VAL_ON_INDEX THEN
                            NULL;
                    END;
                END LOOP;  
				SELECT COUNT(*) INTO ln_is_consumer_acct FROM PSS.CONSUMER_ACCT WHERE CONSUMER_ACCT_ID = l_rec.CONSUMER_ACCT_ID;
                IF l_campaign_id > 0 AND ln_is_consumer_acct > 0 THEN
                LOOP
                    UPDATE REPORT.CONSUMER_ACCT_STAT
                       SET TRAN_COUNT = NVL(TRAN_COUNT, 0) + 1,
                           VEND_COUNT = NVL(VEND_COUNT, 0) + l_qty,
                           TRAN_AMOUNT = NVL(TRAN_AMOUNT, 0) + NVL(l_rec.TOTAL_AMOUNT, 0),
                           CONVENIENCE_FEE = NVL(CONVENIENCE_FEE, 0) + NVL(l_rec.CONVENIENCE_FEE, 0),
                           LOYALTY_DISCOUNT = NVL(LOYALTY_DISCOUNT, 0) + COALESCE(l_rec.more_loyalty_discount, 0)+COALESCE(l_rec.loyalty_discount, 0),
                           PURCHASE_DISCOUNT = NVL(PURCHASE_DISCOUNT, 0) + NVL(l_rec.PURCHASE_DISCOUNT, 0)
                     WHERE CONSUMER_ACCT_ID =l_rec.CONSUMER_ACCT_ID
                       AND CURRENCY_ID = l_rec.CURRENCY_ID
                       AND CAMPAIGN_ID = l_campaign_id;
                    EXIT WHEN SQL%ROWCOUNT > 0;
                    BEGIN
                       	INSERT INTO REPORT.CONSUMER_ACCT_STAT(
                           	CONSUMER_ACCT_ID,
                           	TRAN_COUNT,
                           	VEND_COUNT, 
                           	TRAN_AMOUNT,
                           	CURRENCY_ID, 
                           	CONVENIENCE_FEE,
                           	LOYALTY_DISCOUNT,
                           	CAMPAIGN_ID,
                           	PURCHASE_DISCOUNT) 
                       	SELECT
                           	l_rec.CONSUMER_ACCT_ID,
                           	1,
                           	l_qty,
                           	NVL(l_rec.TOTAL_AMOUNT, 0),
                           	l_rec.CURRENCY_ID,
                           	l_rec.CONVENIENCE_FEE,
                           	COALESCE(l_rec.more_loyalty_discount, 0)+COALESCE(l_rec.loyalty_discount, 0),
                           	l_campaign_id,
                           	l_rec.PURCHASE_DISCOUNT
                       	FROM DUAL;
                       	EXIT;    
                    EXCEPTION
                    	WHEN DUP_VAL_ON_INDEX THEN
                    		NULL;
					END;
				END LOOP; 
                END IF;
            END IF;
            COMMIT;
            
            -- update corp.batch_total
            IF l_payment_batch_id is not null AND (l_qty != 0  OR l_rec.TOTAL_AMOUNT != 0) THEN
                IF l_rec.NON_PAYABLE_ENTRY_TYPE IS NOT NULL THEN               
                    LOOP
                        UPDATE CORP.BATCH_TOTAL
                           SET LEDGER_AMOUNT = LEDGER_AMOUNT + l_rec.TOTAL_AMOUNT,
                               LEDGER_COUNT = LEDGER_COUNT + 1
                         WHERE BATCH_ID = l_payment_batch_id
                           AND ENTRY_TYPE = l_rec.NON_PAYABLE_ENTRY_TYPE;
                        EXIT WHEN SQL%ROWCOUNT > 0;
                        BEGIN
                            INSERT INTO CORP.BATCH_TOTAL(BATCH_ID, ENTRY_TYPE, PAYABLE, LEDGER_AMOUNT, LEDGER_COUNT)
                            VALUES(l_payment_batch_id,l_rec.NON_PAYABLE_ENTRY_TYPE,'N', l_rec.TOTAL_AMOUNT, 1); 
                            EXIT;    
                        EXCEPTION
                            WHEN DUP_VAL_ON_INDEX THEN
                                NULL;
                        END;
                    END LOOP;       
                END IF;  
                IF l_rec.CONV_FEE_ENTRY_TYPE IS NOT NULL AND l_rec.CONVENIENCE_FEE > 0 THEN               
                    LOOP
                        UPDATE CORP.BATCH_TOTAL
                           SET LEDGER_AMOUNT = LEDGER_AMOUNT + l_rec.CONVENIENCE_FEE,
                               LEDGER_COUNT = LEDGER_COUNT + 1
                         WHERE BATCH_ID = l_payment_batch_id
                           AND ENTRY_TYPE = l_rec.CONV_FEE_ENTRY_TYPE;
                        EXIT WHEN SQL%ROWCOUNT > 0;
                        BEGIN
                            INSERT INTO CORP.BATCH_TOTAL(BATCH_ID, ENTRY_TYPE, PAYABLE, LEDGER_AMOUNT, LEDGER_COUNT)
                            VALUES(l_payment_batch_id,l_rec.CONV_FEE_ENTRY_TYPE,'N', l_rec.CONVENIENCE_FEE, 1); 
                            EXIT;    
                        EXCEPTION
                            WHEN DUP_VAL_ON_INDEX THEN
                                NULL;
                        END;
                    END LOOP; 
                END IF;
            END IF;
            COMMIT;         
            IF l_qty != 0  OR l_rec.TOTAL_AMOUNT != 0 THEN
                --logic to update report.eport column for condition report
                IF l_rec.CREDIT_IND = 'Y' THEN
                  -- credit
                  UPDATE REPORT.EPORT SET
                   FIRST_TRAN_DATE=LEAST(NVL(FIRST_TRAN_DATE, max_date), l_rec.CLOSE_DATE),
                   FIRST_CREDIT_TRAN_DATE=LEAST(NVL(FIRST_CREDIT_TRAN_DATE, max_date), l_rec.CLOSE_DATE),
                   LAST_TRAN_DATE=GREATEST(NVL(LAST_TRAN_DATE, min_date), l_rec.CLOSE_DATE),
                   LAST_CREDIT_TRAN_DATE=GREATEST(NVL(LAST_CREDIT_TRAN_DATE, min_date), l_rec.CLOSE_DATE)
                  WHERE EPORT_ID=l_rec.EPORT_ID;
                ELSIF l_rec.CASH_IND = 'Y' THEN
                  -- cash
                  UPDATE REPORT.EPORT SET
                   FIRST_TRAN_DATE=LEAST(NVL(FIRST_TRAN_DATE, max_date), l_rec.CLOSE_DATE),
                   FIRST_CASH_TRAN_DATE=LEAST(NVL(FIRST_CASH_TRAN_DATE, max_date), l_rec.CLOSE_DATE),
                   LAST_TRAN_DATE=GREATEST(NVL(LAST_TRAN_DATE, min_date), l_rec.CLOSE_DATE),
                   LAST_CASH_TRAN_DATE=GREATEST(NVL(LAST_CASH_TRAN_DATE, min_date), l_rec.CLOSE_DATE)
                  WHERE EPORT_ID=l_rec.EPORT_ID;
                ELSE
                  UPDATE REPORT.EPORT SET
                   FIRST_TRAN_DATE=LEAST(NVL(FIRST_TRAN_DATE, max_date), l_rec.CLOSE_DATE),
                   LAST_TRAN_DATE=GREATEST(NVL(LAST_TRAN_DATE, min_date), l_rec.CLOSE_DATE)
                  WHERE EPORT_ID=l_rec.EPORT_ID;
                END IF;                 
            END IF;
        END LOOP;
    END;
 
    
    PROCEDURE UPDATE_TERMINAL(
        l_terminal_id TERMINAL.TERMINAL_ID%TYPE)
    IS
    BEGIN
        UPDATE ACTIVITY_REF A SET (
            region_id,
            region_name,
            location_name,
            location_id,
            terminal_name,
            terminal_nbr) = (
        SELECT
            R.REGION_ID,
            R.REGION_NAME,
            NVL(L.LOCATION_NAME, 'Unknown'),
            NVL(L.LOCATION_ID, 0),
            TERMINAL_NAME,
            TERM.TERMINAL_NBR
          FROM TERMINAL TERM,  REGION R, TERMINAL_REGION TR, LOCATION L
          WHERE TR.REGION_ID = R.REGION_ID (+)
            AND TERM.TERMINAL_ID = TR.TERMINAL_ID (+)
            AND TERM.LOCATION_ID = L.LOCATION_ID (+)
            AND TERM.TERMINAL_ID = A.TERMINAL_ID)
        WHERE A.TERMINAL_ID = l_terminal_id
          AND A.TRAN_DATE > SYSDATE - 90;
    END;

    PROCEDURE UPDATE_FILL_DATE(
        l_fill_id FILL.FILL_ID%TYPE)
    IS
        l_fill_date FILL.FILL_DATE%TYPE;
        l_prev_fill_date FILL.FILL_DATE%TYPE;
        l_eport_id FILL.EPORT_ID%TYPE;
    BEGIN
        SELECT FILL_DATE, EPORT_ID
          INTO l_fill_date, l_eport_id
          FROM FILL
         WHERE FILL_ID = l_fill_id;
         
         -- add update last fill date for condition report
         UPDATE REPORT.EPORT
         SET LAST_FILL_DATE=l_fill_date
         WHERE EPORT_ID=l_eport_id
         AND NVL(LAST_FILL_DATE, min_date) < l_fill_date;

          SELECT MAX(FILL_DATE)
          INTO l_prev_fill_date
          FROM FILL
         WHERE FILL_DATE < l_fill_date
           AND EPORT_ID = l_eport_id;
           
        -- Added to capture first fills (03-05-2007 BSK)
        IF l_prev_fill_date IS NULL THEN
            SELECT MAX(START_DATE)
              INTO l_prev_fill_date
              FROM REPORT.TERMINAL_EPORT
             WHERE START_DATE <= l_fill_date
               AND EPORT_ID = l_eport_id;
        END IF;
        
        -- Ignore first fill period (added June 8th for performance reasons - BSK)
        IF l_prev_fill_date IS NOT NULL THEN
            UPDATE ACTIVITY_REF SET FILL_DATE = GET_FILL_DATE(TRAN_ID)
             WHERE EPORT_ID = l_eport_id AND TRAN_DATE BETWEEN l_prev_fill_date AND l_fill_date;
          END IF;
    END;
    
    PROCEDURE UPDATE_FILL_INFO(
            l_fill_id   FILL.FILL_ID%TYPE) 
    IS
        l_export_batch_id FILL.BATCH_ID%TYPE;
        l_customer_id TERMINAL.CUSTOMER_ID%TYPE;
        l_terminal_id FILL.TERMINAL_ID%TYPE;
    
    BEGIN
        SELECT T.CUSTOMER_ID, T.TERMINAL_ID INTO l_customer_id, l_terminal_id
        FROM REPORT.TERMINAL T 
        JOIN REPORT.TERMINAL_EPORT TE ON T.TERMINAL_ID=TE.TERMINAL_ID
        JOIN REPORT.FILL F ON F.EPORT_ID=TE.EPORT_ID
        WHERE F.FILL_ID=l_fill_id
        AND F.FILL_DATE >= NVL(TE.START_DATE, MIN_DATE) AND F.FILL_DATE < NVL(TE.END_DATE, MAX_DATE);
        
        l_export_batch_id := GET_OR_CREATE_EXPORT_BATCH(l_customer_id);
        UPDATE REPORT.FILL SET BATCH_ID = l_export_batch_id, TERMINAL_ID = l_terminal_id,
        REF_NBR = NVL(REF_NBR, REF_NBR_SEQ.nextval)
        WHERE FILL_ID = l_fill_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN;  
    END;
    
END;
/


