ALTER TABLE PSS.TRAN_STATE ADD PENDING_IND VARCHAR2(1 BYTE);

UPDATE PSS.TRAN_STATE SET PENDING_IND = CASE WHEN TRAN_STATE_CD IN ('0', '1', '2', '3', '4', '6', '8', '9', 'B', 'J', 'N', 'P', 'Q', 'R') THEN 'Y' ELSE 'N' END
WHERE PENDING_IND IS NULL;
COMMIT;

ALTER TABLE PSS.TRAN_STATE MODIFY PENDING_IND NOT NULL;

BEGIN
  DBMS_SCHEDULER.DROP_JOB('PSS."CC DATA MASK"');
  DBMS_SCHEDULER.CREATE_JOB (
   job_name           =>  'PSS.UPDATE_PENDING_TRAN_JOB',
   job_type           =>  'PLSQL_BLOCK',
   job_action         =>  'BEGIN PSS.UPDATE_PENDING_TRAN(); END;',
   start_date         =>  SYSDATE,
   repeat_interval    =>  'FREQ=DAILY;BYHOUR=7;BYMINUTE=0;BYSECOND=0',
   enabled            =>  TRUE,
   comments           =>  'Update tran_state_cd pending transactions',
   auto_drop          =>  FALSE);
END;
/
