INSERT INTO ENGINE.APP_SETTING(APP_SETTING_CD, APP_SETTING_VALUE, APP_SETTING_DESC)
SELECT 'TANDEM_EFT_AFTER_FUNDING_IND', 'N', 'Fund customer for transactions after receiving deposit confirmation from Tandem'
FROM DUAL WHERE NOT EXISTS(SELECT 1 FROM ENGINE.APP_SETTING WHERE APP_SETTING_CD = 'TANDEM_EFT_AFTER_FUNDING_IND');
COMMIT;

INSERT INTO PSS.TRAN_STATE(TRAN_STATE_CD, TRAN_STATE_DESC, REPORTABLE_IND)
VALUES('X', 'PROCESSED_TRAN_PENDING_DEPOSIT', 'Y');
INSERT INTO PSS.TRAN_STATE(TRAN_STATE_CD, TRAN_STATE_DESC, REPORTABLE_IND)
VALUES('Y', 'PROCESSED_TRAN_DEPOSIT_CONFIRMED', 'Y');
COMMIT;

INSERT INTO PSS.AUTH_STATE(AUTH_STATE_ID, AUTH_STATE_NAME)
VALUES(8, 'REJECTION');
INSERT INTO PSS.REFUND_STATE(REFUND_STATE_ID, REFUND_STATE_NAME, REFUND_STATE_DESC)
VALUES(7, 'SERVER_REFUND_REJECTION', 'Rejected');
COMMIT;

GRANT EXECUTE ON REPORT.DATA_IN_PKG TO PSS;
GRANT SELECT, UPDATE ON PSS.TRAN TO REPORT;

ALTER TABLE REPORT.TRANS ADD SUBMISSION_NUM VARCHAR2(11);

UPDATE REPORT.REPORT_PARAM
SET PARAM_VALUE = q'[SELECT
led.entry_type,
COALESCE(bu.business_unit_name, '~ Terminal Orphan') business_unit_name,
COALESCE(DECODE(t.payment_schedule_id, 4, 'Y', 'N'), 'UNK') monthly_payment,
cr.currency_code,
COALESCE(c.customer_name, '~ Terminal Orphan') customer_name,
TO_CHAR(TRUNC(DECODE (led.entry_type,
	'CC', tr.settle_date,
	led.ledger_date
), 'MONTH'), 'mm/dd/yyyy') month_for,
COUNT(1) tran_count,
SUM(tr.total_amount) total_tran_amount,
SUM(led.amount) total_ledger_amount,
tr.trans_type_id,
tt.trans_type_name,
c.customer_id,
tr.submission_num
FROM report.trans tr
JOIN corp.currency cr
ON cr.currency_id = tr.currency_id
JOIN report.trans_state ts
ON ts.state_id = tr.settle_state_id
LEFT OUTER JOIN (
	report.terminal t
	JOIN corp.customer c
	ON c.customer_id = t.customer_id
	JOIN corp.business_unit bu
	ON bu.business_unit_id = t.business_unit_id
)
ON t.terminal_id = tr.terminal_id
JOIN corp.ledger led
ON led.trans_id = tr.tran_id
AND led.deleted = 'N'
JOIN report.trans_type tt
ON tr.trans_type_id = tt.trans_type_id
WHERE tr.settle_state_id IN (2, 3)
AND led.entry_type IN ('CC', 'PF', 'CB', 'RF')
AND (tr.settle_date >= TRUNC(SYSDATE) - 1 AND tr.settle_date < TRUNC(SYSDATE)
OR led.create_date >= TRUNC(SYSDATE) - 1 AND led.create_date < TRUNC(SYSDATE) AND tr.settle_date IS NULL)
GROUP BY
led.entry_type,
COALESCE(bu.business_unit_name, '~ Terminal Orphan'),
COALESCE(DECODE(t.payment_schedule_id, 4, 'Y', 'N'), 'UNK'),
cr.currency_code,
COALESCE(c.customer_name, '~ Terminal Orphan'),
c.customer_id,
TRUNC(DECODE (led.entry_type,
	'CC', tr.settle_date,
	led.ledger_date
), 'MONTH'),
tr.trans_type_id,
tt.trans_type_name,
tr.submission_num]'
WHERE REPORT_ID = 486 AND PARAM_NAME = 'query';
COMMIT;

UPDATE REPORT.REPORT_PARAM
SET PARAM_VALUE = q'[SELECT
led.entry_type,
COALESCE(bu.business_unit_name, '~ Terminal Orphan') business_unit_name,
COALESCE(DECODE(t.payment_schedule_id, 4, 'Y', 'N'), 'UNK') monthly_payment,
cr.currency_code,
COALESCE(c.customer_name, '~ Terminal Orphan') customer_name,
TO_CHAR(TRUNC(DECODE (led.entry_type,
	'CC', tr.settle_date,
	led.ledger_date
), 'MONTH'), 'mm/dd/yyyy') month_for,
COUNT(1) tran_count,
SUM(tr.total_amount) total_tran_amount,
SUM(led.amount) total_ledger_amount,
tr.trans_type_id,
tt.trans_type_name,
ee.eport_num,
c.customer_id,
tr.submission_num
FROM report.trans tr
JOIN corp.currency cr
ON cr.currency_id = tr.currency_id
JOIN report.trans_state ts
ON ts.state_id = tr.settle_state_id
LEFT OUTER JOIN (
	report.terminal t
	JOIN corp.customer c
	ON c.customer_id = t.customer_id
	JOIN corp.business_unit bu
	ON bu.business_unit_id = t.business_unit_id
)
ON t.terminal_id = tr.terminal_id
JOIN corp.ledger led
ON led.trans_id = tr.tran_id
AND led.deleted = 'N'
JOIN report.trans_type tt
ON tr.trans_type_id = tt.trans_type_id
LEFT OUTER JOIN (
SELECT DISTINCT terminal_id, FIRST_VALUE(eport_serial_num) OVER (PARTITION BY terminal_id ORDER BY start_date DESC) eport_num
FROM report.terminal_eport te
JOIN report.eport e ON te.eport_id = e.eport_id
) ee ON tr.terminal_id = ee.terminal_id
WHERE tr.settle_state_id IN (2, 3)
AND led.entry_type IN ('CC', 'PF', 'CB', 'RF')
AND (tr.settle_date >= TRUNC(SYSDATE) - 1 AND tr.settle_date < TRUNC(SYSDATE)
OR led.create_date >= TRUNC(SYSDATE) - 1 AND led.create_date < TRUNC(SYSDATE) AND tr.settle_date IS NULL)
GROUP BY
led.entry_type,
COALESCE(bu.business_unit_name, '~ Terminal Orphan'),
COALESCE(DECODE(t.payment_schedule_id, 4, 'Y', 'N'), 'UNK'),
cr.currency_code,
COALESCE(c.customer_name, '~ Terminal Orphan'),
c.customer_id,
TRUNC(DECODE (led.entry_type,
	'CC', tr.settle_date,
	led.ledger_date
), 'MONTH'),
tr.trans_type_id,
tt.trans_type_name,
ee.eport_num,
tr.submission_num]'
WHERE REPORT_ID = 487 AND PARAM_NAME = 'query';
COMMIT;

GRANT SELECT ON PSS.DOWNGRADE_REASON TO USAT_DMS_ROLE;
