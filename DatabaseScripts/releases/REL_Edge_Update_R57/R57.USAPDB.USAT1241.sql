UPDATE REPORT.REPORT_PARAM
SET PARAM_VALUE = q'[SELECT
lts.entry_type,
bu.business_unit_name,
DECODE(bat.payment_schedule_id, 4, 'Y', 'N') monthly_payment,
cu.currency_code,
c.customer_name,
e.eport_serial_num eport_num,
lts.fee_name,
DECODE(d.status,
	'S', 'Y',
	'P', 'Y',
	'N'
) in_eft,
TO_CHAR(TRUNC(lts.entry_date, 'MONTH'),'mm/dd/yyyy') month_for,
COUNT(1) entry_count,
SUM (lts.amount) total_entry_amount,
lts.description,
t.terminal_nbr,
t.sales_order_number,
c.customer_id,
TO_CHAR(t.create_date, 'mm/dd/yyyy') terminal_create_date,
TO_CHAR(te.start_date, 'mm/dd/yyyy') terminal_start_date,
TO_CHAR(e.first_tran_date, 'mm/dd/yyyy') first_tran_date,
TO_CHAR(e.lastdialin_date, 'mm/dd/yyyy') last_call_in_date
FROM (
	SELECT
	led.batch_id,
	led.amount,
	led.entry_type,
	led.settle_state_id,
	led.create_date,
	led.entry_date,
	led.description,
	f.fee_name
	FROM corp.ledger led
	LEFT OUTER JOIN (
		corp.service_fees sf
		JOIN corp.fees f
		ON f.fee_id = sf.fee_id
	)
	ON sf.service_fee_id = led.service_fee_id
	WHERE led.entry_type IN ('SF', 'AD', 'SB')
	AND led.deleted = 'N'
	AND led.settle_state_id IN (2, 3)
	AND (led.report_date >= CAST(? AS DATE) AND led.report_date < CAST(? AS DATE) + 1
	OR led.create_date >= CAST(? AS DATE) AND led.create_date < CAST(? AS DATE) + 1 AND led.report_date IS NULL)
)lts
JOIN corp.batch bat
ON bat.batch_id = lts.batch_id
JOIN corp.doc d
ON d.doc_id = bat.doc_id
JOIN corp.customer_bank cb
ON cb.customer_bank_id = d.customer_bank_id
JOIN corp.customer c
ON c.customer_id = cb.customer_id
JOIN corp.business_unit bu
ON bu.business_unit_id = d.business_unit_id
JOIN corp.currency cu
ON cu.currency_id = d.currency_id
JOIN report.trans_state ts
ON ts.state_id = lts.settle_state_id
LEFT OUTER JOIN (SELECT DISTINCT TERMINAL_ID, FIRST_VALUE(terminal_eport_id) OVER (PARTITION BY TERMINAL_ID ORDER BY start_date DESC) terminal_eport_id
FROM report.terminal_eport) tex
ON bat.terminal_id = tex.terminal_id
LEFT OUTER JOIN REPORT.TERMINAL_EPORT TE ON TEX.TERMINAL_EPORT_ID = TE.TERMINAL_EPORT_ID
LEFT OUTER JOIN REPORT.EPORT E ON TE.EPORT_ID = E.EPORT_ID
LEFT OUTER JOIN report.terminal t ON bat.terminal_id = t.terminal_id
GROUP BY
lts.entry_type,
bu.business_unit_name,
DECODE(bat.payment_schedule_id, 4, 'Y', 'N'),
cu.currency_code,
c.customer_name,
c.customer_id,
lts.fee_name,
DECODE(d.status,
	'S', 'Y',
	'P', 'Y',
	'N'
),
TRUNC(lts.entry_date, 'MONTH'),
lts.description,
e.eport_serial_num,
t.terminal_nbr,
t.sales_order_number,
TO_CHAR(t.create_date, 'mm/dd/yyyy'),
TO_CHAR(te.start_date, 'mm/dd/yyyy'),
TO_CHAR(e.first_tran_date, 'mm/dd/yyyy'),
TO_CHAR(e.lastdialin_date, 'mm/dd/yyyy')]'
WHERE REPORT_ID = 192 AND PARAM_NAME = 'query';

COMMIT;
