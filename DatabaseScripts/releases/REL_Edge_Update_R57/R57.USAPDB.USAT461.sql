INSERT INTO DEVICE.COMM_METHOD(COMM_METHOD_CD, COMM_METHOD_NAME, IP_MATCH_ORDER, IP_MATCH_REGEX, COMM_PROVIDER_ID, MODEM_ID_DEVICE_SERIAL_REGEX)
SELECT 'L', 'LTE - AT&' || 'T', 60, '^10\.64\.', 2, '.*' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM DEVICE.COMM_METHOD WHERE COMM_METHOD_CD = 'L');
    
INSERT INTO DEVICE.HOST_TYPE(HOST_TYPE_ID, HOST_TYPE_DESC, COMM_METHOD_CD, HOST_DEFAULT_COMPLETE_MINUT, COMM_MODULE_IND, SIM_IND)
SELECT 217, 'LTE AT&' || 'T Modem', 'L', 0, 'Y', 'Y' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM DEVICE.HOST_TYPE WHERE HOST_TYPE_ID = 217);

COMMIT;

UPDATE DEVICE.DEVICE
SET COMM_METHOD_CD = 'L', COMM_METHOD_TS = SYSDATE
WHERE LAST_CLIENT_IP_ADDRESS LIKE '10.64.%' AND (COMM_METHOD_CD IS NULL OR COMM_METHOD_CD != 'L');
COMMIT;

INSERT INTO DEVICE.COMM_METHOD(COMM_METHOD_CD, COMM_METHOD_NAME, IP_MATCH_ORDER, IP_MATCH_REGEX, COMM_PROVIDER_ID, MODEM_ID_DEVICE_SERIAL_REGEX)
SELECT 'T', 'LTE - Eseye', 90, '^10\.2(49|54)\.', 3, '.*' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM DEVICE.COMM_METHOD WHERE COMM_METHOD_CD = 'T');
    
INSERT INTO DEVICE.HOST_TYPE(HOST_TYPE_ID, HOST_TYPE_DESC, COMM_METHOD_CD, HOST_DEFAULT_COMPLETE_MINUT, COMM_MODULE_IND, SIM_IND)
SELECT 218, 'LTE Eseye Modem', 'T', 0, 'Y', 'Y' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM DEVICE.HOST_TYPE WHERE HOST_TYPE_ID = 218);

COMMIT;
