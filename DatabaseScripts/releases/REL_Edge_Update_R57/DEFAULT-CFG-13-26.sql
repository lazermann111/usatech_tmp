DECLARE
	ln_property_list_version NUMBER := 26;
	ln_device_type_id device_type.device_type_id%TYPE := 13;
	ln_config_template_type_id config_template_type.config_template_type_id%TYPE := 1;
	lv_config_template_name config_template.config_template_name%TYPE := 'DEFAULT-CFG-' || ln_device_type_id || '-' || ln_property_list_version;
	ln_config_template_id config_template.config_template_id%TYPE;
	lv_db_name VARCHAR2(30);
	
	PROCEDURE ADD_DSP(
		pv_index VARCHAR2,
		pv_configurable VARCHAR2,
		pv_name VARCHAR2) 
	IS
	BEGIN
		INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_UI_CONFIGURABLE, DEVICE_SETTING_PARAMETER_NAME) 
		SELECT pv_index, pv_configurable, pv_name
		FROM DUAL 
		WHERE NOT EXISTS (
			SELECT 1 
			FROM DEVICE.DEVICE_SETTING_PARAMETER 
			WHERE DEVICE_SETTING_PARAMETER_CD = pv_index);
	END;

	PROCEDURE ADD_CTS_META(
		pv_PARAMETER_CD VARCHAR2,
		pv_SETTING_VALUE VARCHAR2,
		pn_CATEGORY_ID NUMBER,
		pv_DISPLAY VARCHAR2,
		pv_DESCRIPTION VARCHAR2,
		pv_EDITOR VARCHAR2,
		pn_FIELD_SIZE NUMBER,
		pn_DISPLAY_ORDER NUMBER,
		pv_DATA_MODE VARCHAR2,
		pv_DATA_MODE_AUX VARCHAR2,
		pn_REGEX_ID NUMBER,
		pv_NAME VARCHAR2,
		pv_CUSTOMER_EDITABLE VARCHAR2) 
	IS 
	BEGIN
		INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(
			CONFIG_TEMPLATE_ID,
			DEVICE_SETTING_PARAMETER_CD,
			CONFIG_TEMPLATE_SETTING_VALUE,
			CATEGORY_ID,
			DISPLAY,
			DESCRIPTION,
			EDITOR,
			CONVERTER,
			CLIENT_SEND,
			ALIGN,
			FIELD_SIZE,
			FIELD_OFFSET,
			PAD_CHAR,
			PROPERTY_LIST_VERSION,
			DEVICE_TYPE_ID,
			DISPLAY_ORDER,
			ALT_NAME,
			DATA_MODE,
			DATA_MODE_AUX,
			REGEX_ID,
			ACTIVE,
			NAME,
			CUSTOMER_EDITABLE,
			STORED_IN_PENNIES) 
		SELECT 
			CONFIG_TEMPLATE_ID, 
			pv_PARAMETER_CD,
			pv_SETTING_VALUE,
			pn_CATEGORY_ID,
			pv_DISPLAY,
			pv_DESCRIPTION,
			pv_EDITOR,
			'',
			'Y',
			NULL,
			pn_FIELD_SIZE,
			NULL,
			NULL,
			0,
			DEVICE_TYPE_ID,
			pn_DISPLAY_ORDER,
			NULL,
			pv_DATA_MODE,
			pv_DATA_MODE_AUX,
			pn_REGEX_ID,
			'Y',
			pv_NAME,
			pv_CUSTOMER_EDITABLE,
			'N'
		FROM DEVICE.CONFIG_TEMPLATE CT 
		WHERE CONFIG_TEMPLATE_TYPE_ID = 3 
			AND DEVICE_TYPE_ID = 13 
			AND NOT EXISTS (
				SELECT 1 
				FROM DEVICE.CONFIG_TEMPLATE_SETTING 
				WHERE DEVICE_TYPE_ID = 13 
				AND DEVICE_SETTING_PARAMETER_CD = pv_PARAMETER_CD);
	END;
	
	PROCEDURE ADD_CTS(
		pn_CONFIG_TEMPLATE_ID NUMBER,
		pv_PARAMETER_CD VARCHAR2,
		pv_SETTING_VALUE VARCHAR2) 
	IS
	BEGIN
		INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(
			CONFIG_TEMPLATE_ID, 
			DEVICE_SETTING_PARAMETER_CD, 
			CONFIG_TEMPLATE_SETTING_VALUE) 
		VALUES(pn_CONFIG_TEMPLATE_ID, pv_PARAMETER_CD, pv_SETTING_VALUE);
	END;
	
	PROCEDURE ADD_CTS_REGEX(
		pv_REGEX_NAME VARCHAR2,
		pv_REGEX_DESC VARCHAR2,
		pv_REGEX_VALUE VARCHAR2) 
	IS
	BEGIN
		INSERT INTO DEVICE.CONFIG_TEMPLATE_REGEX(
			REGEX_NAME, 
			REGEX_DESC, 
			REGEX_VALUE) 
		VALUES(pv_REGEX_NAME, pv_REGEX_DESC, pv_REGEX_VALUE);
	END;	
	
BEGIN
	SELECT REPLACE(GLOBAL_NAME, '.WORLD', '') INTO lv_db_name FROM GLOBAL_NAME;

	-- updates to existing config template settings of meta template
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET EDITOR = 'TEXT:0 to 33'
	WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD IN ('1203', '1204', '1205', '1206', '1527', '1528') AND EDITOR != 'TEXT:0 to 33';
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET DESCRIPTION = 'Number of seconds to poll modem for RSSI and misc data. 0 is off.' WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '34';
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET DESCRIPTION = 'If the ePort has not communicated in N seconds the modem''s PDP context will be forced closed. PDP context will be reestablish in line with the next TCP session. 0 is disabled.' WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '35';
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET DESCRIPTION = 'Many units only have one RF port located on the left side of the unit. This property has no effect on units like these with one port. LTE and some other modems have an extra port on the right called the Diversity port. Set to 1 to enable. Set to 0 to disable. For best performance of units with 2 ports, ensure two antennas are installed if this property is enabled.' WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '451';
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET DESCRIPTION = 'Represents the number of reattempts on when the ePort sends a Poll Response Message, but it did not receive an ACK from the VMC. Set to 0 to disable message retries.' WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1004';
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET DESCRIPTION = 'This number represents how fast the Edge device will respond to the VMC''s poll.<br/>Too fast or too slow and the VMC will not be able to receive our transmitted response.<br/>Response Time = This number x 0.2ms. The value of 12 = 2.4ms.<br/>Note: the MDB spec has a max of 5ms, but most VMC''s prefer >2ms and work higher than the 5ms spec.' WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1010';
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET DESCRIPTION = 'This value represents the address of our ePort on the MDB bus.<br/>The MDB spec now supports 2 card readers.<br/>The Primary reader address is 16.<br/>The Secondary reader address is 96.' WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1012';
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET DESCRIPTION = 'Currency scale is used to set the scale between the ePort and the VMC. Cashless readers prefer a scale of 1, but some older VMCs prefer a scale of 5 because they were developed to support coin mechs that held nickels and quarters. Settings include: 0 - VMC scale is set to 1 (a penny), 5 - VMC scale is set to 5 (a nickel), 15 - VMC scale is set to 5 (a nickel), but cash is reported at a scale of 1. Any other value is interpreted as 0, scale of 1.' WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1014';
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET DESCRIPTION = 'Enables or disables the End/Complete button during an MDB vend session.' WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1015';
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET DESCRIPTION = 'If 0, disabled, at the beginning of each cashless session, the ePort will send the VMC the balance of funds available for the consumer to spend. If 1, enabled, the VMC will be sent FFFF instead which a special code to instruct the VMC to hide the balance from the consumer.' WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1016';
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET DESCRIPTION = '0 means diagnostic event logging is disabled. See Engineering for use and revs that support this feature.' WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1017';
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET DESCRIPTION = 'Sets the time limit in 100ms of how fast the ePort will allow MDB cash messages to be processed. If they arrive any faster than this time, the unit will treat the cash message as spamming and reset the unit in attempt to recover. The value of 30 is 3 seconds.' WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1027';
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET DESCRIPTION = 'Sets the detail level for reporting UI state to the VMC. 0 - No reporting. 1 - Basic Cashless Flow reporting, >= 2 TBD. Contact Engineering for supported firmware revisions.' WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1028';
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET DESCRIPTION = 'How often to poll the VMC to determine whether or not it requires the ePort to read new DEX file and upload it to the server. Set to 0 to disable polling.	Supersedes 1106. Minimum value is 10sec.' WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1110';
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET DESCRIPTION = 'Number of retries for unattended DEX reads (examples Scheduled DEX and Alert DEX). Set to 0 to disable retries. Note: 4 means 5 reads max before uploading file to the server.' WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1111';
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET DESCRIPTION = 'Number of minutes between Retries. Min value is 1.' WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1112';
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET DESCRIPTION = 'The ePort simulates a hand held device. This value represents the number of simulated plug-in attempts of a handheld. This value should only be changed with engineering guidance. Min value is 1.' WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1113';
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET DESCRIPTION = 'The ePort attempts communication with a VMC by sending the ENQ control character. Some vendors, like ones equipped with Bill recyclers, require more ENQs to be sent to wake it up. This value should only be changed with engineering guidance. Min value is 5.' WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1114';
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET DESCRIPTION = 'This value sets the time gap in 200us increments on when to parse the incoming serial message. Example: 12 is 2.4ms. MIN is 2, MAX is 20.' WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1600';
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET DESCRIPTION = 'This value is the number of seconds that must elapse after the Host stops communicating with the ePort before all work in process is marked Failed and the unit will be disabled. Note: Device will change the improper setting of 0 to the value of 10 seconds.' WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1601';
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET DESCRIPTION = 'This value in seconds sets the timeout for the Host to post a Transaction Message after credit is received. Note: Device will change the improper setting of 0 to the value of 20 seconds.' WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1602';
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET DESCRIPTION = 'Set to 0 to Enable the END Button.' WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1603';
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET DESCRIPTION = 'Notes: When using Interac, set the language to "French OR English". "French AND English" is used to help set up DMS. When set, missing fields are replaced with a default like "French ????" to assist the user in finding what properties are missing a language. Any properties that hold LCD messages can have French added to them by using the delimiter "|". Messages include: Welcome, Attract, Disable, and Coin Price Labels.' WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '2016';
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET DESCRIPTION = q'[Similar to the Welcome Message Line 1, the device will show this message on the top line of the display when the card reader is enabled; however it will alternate with the Welcome Message Line 1. If this value is empty, the device will show its internal message. If this device supports French, French can be added by using the form "English|French". Note when the value is "|" or empty, this implies to use the device's internal English/French message. Note: This message is not shown if the device's interface is configured for single-item pulse. Example: ' Swipe Or Tap '.]', DISPLAY_ORDER = 6 WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1203';
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET DESCRIPTION = q'[Similar to the Welcome Message Line 2, the device will show this message on the bottom line of the display when the card reader is enabled; however it will alternate with the Welcome Message Line 2. If this value is empty, the device will show its internal message. If this device supports French, French can be added by using the form "English|French". Note when the value is "|" or empty, this implies to use the device's internal English/French message. Note: This message is not shown if the device's interface is configured for single-item pulse. Example: ' To Begin '.]', DISPLAY_ORDER = 7 WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1204';
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET DESCRIPTION = q'[The device will show this message on the top line of the display when the card reader is enabled. If this value is empty, the device will show its internal message. If this device supports French, French can be added by using the form "English|French". Note when the value is "|" or empty, this implies to use the device's internal English/French message. Example: ' Use Your Credit'.]', DISPLAY_ORDER = 4 WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1205';
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET DESCRIPTION = q'[The device will show this message on the bottom line of the display when the card reader is enabled. If this value is empty, the device will show its internal message. If this device supports French, French can be added by using the form "English|French". Note when the value is "|" or empty, this implies to use the device's internal English/French message. Example: ' or Debit Card '.]', DISPLAY_ORDER = 5 WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1206';
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET DESCRIPTION = q'[The device will show this message on the top line of the display when the device is disabled by the VMC. If this value is empty, the device will show its internal message. If this device supports French, French can be added by using the form "English|French". Note when the value is "|" or empty, this implies to use the device's internal English/French message. Example: 'Cards Offline'.]' WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1527';
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET DESCRIPTION = q'[The device will show this message on the bottom line of the display when the device is disabled by the VMC. If this value is empty, the device will show its internal message. If this device supports French, French can be added by using the form "English|French". Note when the value is "|" or empty, this implies to use the device's internal English/French message. Example: 'by Vendor'.]' WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1528';
	
	-- check if config template exists yet
	
	SELECT MAX(config_template_id) INTO ln_config_template_id
	FROM device.config_template
	WHERE config_template_name = lv_config_template_name;
	
	IF ln_config_template_id IS NULL THEN
		-- create it if not
		INSERT INTO device.config_template(config_template_type_id, config_template_name, device_type_id, property_list_version)
		VALUES(ln_config_template_type_id, lv_config_template_name, ln_device_type_id, ln_property_list_version);
		
		SELECT MAX(config_template_id) INTO ln_config_template_id
		FROM device.config_template
		WHERE config_template_name = lv_config_template_name;
	END IF;
	
	-- remove existing settings for this template, if any
	
	DELETE FROM DEVICE.CONFIG_TEMPLATE_SETTING WHERE CONFIG_TEMPLATE_ID = ln_config_template_id;
	
	-- copy all other settings from previous properties list
	INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(CONFIG_TEMPLATE_ID, DEVICE_SETTING_PARAMETER_CD, CONFIG_TEMPLATE_SETTING_VALUE) 
	SELECT ln_config_template_id, DEVICE_SETTING_PARAMETER_CD, CONFIG_TEMPLATE_SETTING_VALUE
	FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS
	JOIN DEVICE.CONFIG_TEMPLATE CT ON CTS.CONFIG_TEMPLATE_ID = CT.CONFIG_TEMPLATE_ID
	WHERE CONFIG_TEMPLATE_NAME = 'DEFAULT-CFG-' || ln_device_type_id || '-' || (ln_property_list_version - 1)
	AND DEVICE_SETTING_PARAMETER_CD NOT IN (
		SELECT DEVICE_SETTING_PARAMETER_CD 
		FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS 
		WHERE CONFIG_TEMPLATE_ID = ln_config_template_id);
		
	-- default value changes
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET CONFIG_TEMPLATE_SETTING_VALUE = NULL
	WHERE CONFIG_TEMPLATE_ID = ln_config_template_id AND DEVICE_SETTING_PARAMETER_CD IN ('1203', '1204', '1205', '1206', '1527', '1528');
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET CONFIG_TEMPLATE_SETTING_VALUE = 'usatech.com.attz'
	WHERE CONFIG_TEMPLATE_ID = ln_config_template_id AND DEVICE_SETTING_PARAMETER_CD IN ('440', '441');
	
	IF lv_db_name = 'USADEV04' THEN
		UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
		SET CONFIG_TEMPLATE_SETTING_VALUE = '192.168.79.243'
		WHERE CONFIG_TEMPLATE_ID = ln_config_template_id AND DEVICE_SETTING_PARAMETER_CD IN ('400', '401', '420', '421');
		
		UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
		SET CONFIG_TEMPLATE_SETTING_VALUE = '8.41.207.53'
		WHERE CONFIG_TEMPLATE_ID = ln_config_template_id AND DEVICE_SETTING_PARAMETER_CD IN ('405', '406', '425', '426');
	ELSIF lv_db_name = 'USADEV03' THEN
		UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
		SET CONFIG_TEMPLATE_SETTING_VALUE = '8.41.207.53'
		WHERE CONFIG_TEMPLATE_ID = ln_config_template_id AND DEVICE_SETTING_PARAMETER_CD IN ('405', '406', '425', '426');
	ELSIF lv_db_name = 'USADEV02' THEN
		UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
		SET CONFIG_TEMPLATE_SETTING_VALUE = '8.41.207.51'
		WHERE CONFIG_TEMPLATE_ID = ln_config_template_id AND DEVICE_SETTING_PARAMETER_CD IN ('405', '406', '425', '426');
	ELSIF lv_db_name = 'ECCDB' THEN
		UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
		SET CONFIG_TEMPLATE_SETTING_VALUE = '8.41.207.60'
		WHERE CONFIG_TEMPLATE_ID = ln_config_template_id AND DEVICE_SETTING_PARAMETER_CD IN ('405', '406', '425', '426');
	END IF;	
		
	-- delete if exists and insert the updated aggregated properties list
	
	DELETE FROM DEVICE.DEVICE_TYPE_PROPERTY_LIST
	WHERE device_type_id = ln_device_type_id
	AND property_list_version = ln_property_list_version;

	INSERT INTO DEVICE.DEVICE_TYPE_PROPERTY_LIST(device_type_id, property_list_version, all_property_list_ids, configurable_property_list_ids)
	VALUES(ln_device_type_id, ln_property_list_version, 
		'10|30-35|50-52|60-64|70|80|81|85-87|100-108|200-208|210-213|215|300-302|310-315|400|401|405|406|410|411|415|416|420|421|425|426|430|431|435|436|440-442|444|446|450-451|1001-1028|1101|1103|1104|1107|1110-1114|1200-1207|1300-1303|1400-1415|1500-1528|1600-1603|2000-2014',
		'10|30-35|70|85-87|215|400|401|405|406|410|411|415|416|420|421|425|426|430|431|435|436|440-442|444|446|450-451|1001-1028|1101|1103|1104|1107|1110-1114|1200-1207|1300-1303|1400-1415|1500-1528|1600-1603|2000-2022|2030');

	COMMIT;
END;
/
