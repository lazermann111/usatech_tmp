INSERT INTO REPORT.REPORTS(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
SELECT 486, 'Daily Transaction Processing Entry Summary - {b} to {e}', 1, 0, 'Daily Transaction Processing Entry Summary', 'Daily transaction processing entry summary by customer and currency code report.', 'U', USER_ID
FROM REPORT.USER_LOGIN WHERE USER_NAME = 'USATMaster'
AND NOT EXISTS(SELECT 1 FROM REPORT.REPORTS WHERE REPORT_ID = 486);

INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE)
SELECT 486, PARAM_NAME, PARAM_VALUE
FROM (SELECT '' PARAM_NAME, '' PARAM_VALUE FROM DUAL WHERE 1=0
UNION ALL SELECT 'query', q'[SELECT
led.entry_type,
COALESCE(bu.business_unit_name, '~ Terminal Orphan') business_unit_name,
COALESCE(DECODE(t.payment_schedule_id, 4, 'Y', 'N'), 'UNK') monthly_payment,
cr.currency_code,
COALESCE(c.customer_name, '~ Terminal Orphan') customer_name,
TO_CHAR(TRUNC(DECODE (led.entry_type,
	'CC', tr.settle_date,
	led.ledger_date
), 'MONTH'), 'mm/dd/yyyy') month_for,
COUNT(1) tran_count,
SUM(tr.total_amount) total_tran_amount,
SUM(led.amount) total_ledger_amount,
tr.trans_type_id,
tt.trans_type_name,
c.customer_id
FROM report.trans tr
JOIN corp.currency cr
ON cr.currency_id = tr.currency_id
JOIN report.trans_state ts
ON ts.state_id = tr.settle_state_id
LEFT OUTER JOIN (
	report.terminal t
	JOIN corp.customer c
	ON c.customer_id = t.customer_id
	JOIN corp.business_unit bu
	ON bu.business_unit_id = t.business_unit_id
)
ON t.terminal_id = tr.terminal_id
JOIN corp.ledger led
ON led.trans_id = tr.tran_id
AND led.deleted = 'N'
JOIN report.trans_type tt
ON tr.trans_type_id = tt.trans_type_id
WHERE tr.settle_state_id IN (2, 3)
AND led.entry_type IN ('CC', 'PF', 'CB', 'RF')
AND (tr.settle_date >= TRUNC(SYSDATE) - 1 AND tr.settle_date < TRUNC(SYSDATE)
OR led.create_date >= TRUNC(SYSDATE) - 1 AND led.create_date < TRUNC(SYSDATE) AND tr.settle_date IS NULL)
GROUP BY
led.entry_type,
COALESCE(bu.business_unit_name, '~ Terminal Orphan'),
COALESCE(DECODE(t.payment_schedule_id, 4, 'Y', 'N'), 'UNK'),
cr.currency_code,
COALESCE(c.customer_name, '~ Terminal Orphan'),
c.customer_id,
TRUNC(DECODE (led.entry_type,
	'CC', tr.settle_date,
	led.ledger_date
), 'MONTH'),
tr.trans_type_id,
tt.trans_type_name]' FROM DUAL
UNION ALL SELECT 'dateFormat', 'MM/dd/yyyy' FROM DUAL
UNION ALL SELECT 'header', 'true' FROM DUAL
UNION ALL SELECT 'params.StartDate', '{b}' FROM DUAL
UNION ALL SELECT 'params.EndDate', '{e}' FROM DUAL
) P
WHERE NOT EXISTS(
SELECT 1 FROM REPORT.REPORT_PARAM RP0
WHERE RP0.REPORT_ID = 486 AND RP0.PARAM_NAME = P.PARAM_NAME);

INSERT INTO REPORT.REPORTS(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
SELECT 487, 'Daily Transaction Processing Entry Summary With Device - {b} to {e}', 1, 0, 'Daily Tran Processing Entry Summary With Device', 'Daily transaction processing entry summary by customer, device and currency code.', 'U', USER_ID
FROM REPORT.USER_LOGIN WHERE USER_NAME = 'USATMaster'
AND NOT EXISTS(SELECT 1 FROM REPORT.REPORTS WHERE REPORT_ID = 487);

INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE)
SELECT 487, PARAM_NAME, PARAM_VALUE
FROM (SELECT '' PARAM_NAME, '' PARAM_VALUE FROM DUAL WHERE 1=0
UNION ALL SELECT 'query', q'[SELECT
led.entry_type,
COALESCE(bu.business_unit_name, '~ Terminal Orphan') business_unit_name,
COALESCE(DECODE(t.payment_schedule_id, 4, 'Y', 'N'), 'UNK') monthly_payment,
cr.currency_code,
COALESCE(c.customer_name, '~ Terminal Orphan') customer_name,
TO_CHAR(TRUNC(DECODE (led.entry_type,
	'CC', tr.settle_date,
	led.ledger_date
), 'MONTH'), 'mm/dd/yyyy') month_for,
COUNT(1) tran_count,
SUM(tr.total_amount) total_tran_amount,
SUM(led.amount) total_ledger_amount,
tr.trans_type_id,
tt.trans_type_name,
ee.eport_num,
c.customer_id
FROM report.trans tr
JOIN corp.currency cr
ON cr.currency_id = tr.currency_id
JOIN report.trans_state ts
ON ts.state_id = tr.settle_state_id
LEFT OUTER JOIN (
	report.terminal t
	JOIN corp.customer c
	ON c.customer_id = t.customer_id
	JOIN corp.business_unit bu
	ON bu.business_unit_id = t.business_unit_id
)
ON t.terminal_id = tr.terminal_id
JOIN corp.ledger led
ON led.trans_id = tr.tran_id
AND led.deleted = 'N'
JOIN report.trans_type tt
ON tr.trans_type_id = tt.trans_type_id
LEFT OUTER JOIN (
SELECT DISTINCT terminal_id, FIRST_VALUE(eport_serial_num) OVER (PARTITION BY terminal_id ORDER BY start_date DESC) eport_num
FROM report.terminal_eport te
JOIN report.eport e ON te.eport_id = e.eport_id
) ee ON tr.terminal_id = ee.terminal_id
WHERE tr.settle_state_id IN (2, 3)
AND led.entry_type IN ('CC', 'PF', 'CB', 'RF')
AND (tr.settle_date >= TRUNC(SYSDATE) - 1 AND tr.settle_date < TRUNC(SYSDATE)
OR led.create_date >= TRUNC(SYSDATE) - 1 AND led.create_date < TRUNC(SYSDATE) AND tr.settle_date IS NULL)
GROUP BY
led.entry_type,
COALESCE(bu.business_unit_name, '~ Terminal Orphan'),
COALESCE(DECODE(t.payment_schedule_id, 4, 'Y', 'N'), 'UNK'),
cr.currency_code,
COALESCE(c.customer_name, '~ Terminal Orphan'),
c.customer_id,
TRUNC(DECODE (led.entry_type,
	'CC', tr.settle_date,
	led.ledger_date
), 'MONTH'),
tr.trans_type_id,
tt.trans_type_name,
ee.eport_num]' FROM DUAL
UNION ALL SELECT 'dateFormat', 'MM/dd/yyyy' FROM DUAL
UNION ALL SELECT 'header', 'true' FROM DUAL
UNION ALL SELECT 'params.StartDate', '{b}' FROM DUAL
UNION ALL SELECT 'params.EndDate', '{e}' FROM DUAL
) P
WHERE NOT EXISTS(
SELECT 1 FROM REPORT.REPORT_PARAM RP0
WHERE RP0.REPORT_ID = 487 AND RP0.PARAM_NAME = P.PARAM_NAME);

COMMIT;
