WHENEVER SQLERROR EXIT FAILURE COMMIT;

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/releases/REL_Edge_Update_R57/R57.USAPDB.USAT1115.sql?revision=HEAD
INSERT INTO ENGINE.APP_SETTING(APP_SETTING_CD, APP_SETTING_VALUE, APP_SETTING_DESC)
SELECT 'TANDEM_EFT_AFTER_FUNDING_IND', 'N', 'Fund customer for transactions after receiving deposit confirmation from Tandem'
FROM DUAL WHERE NOT EXISTS(SELECT 1 FROM ENGINE.APP_SETTING WHERE APP_SETTING_CD = 'TANDEM_EFT_AFTER_FUNDING_IND');
COMMIT;

INSERT INTO PSS.TRAN_STATE(TRAN_STATE_CD, TRAN_STATE_DESC, REPORTABLE_IND)
VALUES('X', 'PROCESSED_TRAN_PENDING_DEPOSIT', 'Y');
INSERT INTO PSS.TRAN_STATE(TRAN_STATE_CD, TRAN_STATE_DESC, REPORTABLE_IND)
VALUES('Y', 'PROCESSED_TRAN_DEPOSIT_CONFIRMED', 'Y');
COMMIT;

INSERT INTO PSS.AUTH_STATE(AUTH_STATE_ID, AUTH_STATE_NAME)
VALUES(8, 'REJECTION');
INSERT INTO PSS.REFUND_STATE(REFUND_STATE_ID, REFUND_STATE_NAME, REFUND_STATE_DESC)
VALUES(7, 'SERVER_REFUND_REJECTION', 'Rejected');
COMMIT;

GRANT EXECUTE ON REPORT.DATA_IN_PKG TO PSS;
GRANT SELECT, UPDATE ON PSS.TRAN TO REPORT;

ALTER TABLE REPORT.TRANS ADD SUBMISSION_NUM VARCHAR2(11);

UPDATE REPORT.REPORT_PARAM
SET PARAM_VALUE = q'[SELECT
led.entry_type,
COALESCE(bu.business_unit_name, '~ Terminal Orphan') business_unit_name,
COALESCE(DECODE(t.payment_schedule_id, 4, 'Y', 'N'), 'UNK') monthly_payment,
cr.currency_code,
COALESCE(c.customer_name, '~ Terminal Orphan') customer_name,
TO_CHAR(TRUNC(DECODE (led.entry_type,
	'CC', tr.settle_date,
	led.ledger_date
), 'MONTH'), 'mm/dd/yyyy') month_for,
COUNT(1) tran_count,
SUM(tr.total_amount) total_tran_amount,
SUM(led.amount) total_ledger_amount,
tr.trans_type_id,
tt.trans_type_name,
c.customer_id,
tr.submission_num
FROM report.trans tr
JOIN corp.currency cr
ON cr.currency_id = tr.currency_id
JOIN report.trans_state ts
ON ts.state_id = tr.settle_state_id
LEFT OUTER JOIN (
	report.terminal t
	JOIN corp.customer c
	ON c.customer_id = t.customer_id
	JOIN corp.business_unit bu
	ON bu.business_unit_id = t.business_unit_id
)
ON t.terminal_id = tr.terminal_id
JOIN corp.ledger led
ON led.trans_id = tr.tran_id
AND led.deleted = 'N'
JOIN report.trans_type tt
ON tr.trans_type_id = tt.trans_type_id
WHERE tr.settle_state_id IN (2, 3)
AND led.entry_type IN ('CC', 'PF', 'CB', 'RF')
AND (tr.settle_date >= TRUNC(SYSDATE) - 1 AND tr.settle_date < TRUNC(SYSDATE)
OR led.create_date >= TRUNC(SYSDATE) - 1 AND led.create_date < TRUNC(SYSDATE) AND tr.settle_date IS NULL)
GROUP BY
led.entry_type,
COALESCE(bu.business_unit_name, '~ Terminal Orphan'),
COALESCE(DECODE(t.payment_schedule_id, 4, 'Y', 'N'), 'UNK'),
cr.currency_code,
COALESCE(c.customer_name, '~ Terminal Orphan'),
c.customer_id,
TRUNC(DECODE (led.entry_type,
	'CC', tr.settle_date,
	led.ledger_date
), 'MONTH'),
tr.trans_type_id,
tt.trans_type_name,
tr.submission_num]'
WHERE REPORT_ID = 486 AND PARAM_NAME = 'query';
COMMIT;

UPDATE REPORT.REPORT_PARAM
SET PARAM_VALUE = q'[SELECT
led.entry_type,
COALESCE(bu.business_unit_name, '~ Terminal Orphan') business_unit_name,
COALESCE(DECODE(t.payment_schedule_id, 4, 'Y', 'N'), 'UNK') monthly_payment,
cr.currency_code,
COALESCE(c.customer_name, '~ Terminal Orphan') customer_name,
TO_CHAR(TRUNC(DECODE (led.entry_type,
	'CC', tr.settle_date,
	led.ledger_date
), 'MONTH'), 'mm/dd/yyyy') month_for,
COUNT(1) tran_count,
SUM(tr.total_amount) total_tran_amount,
SUM(led.amount) total_ledger_amount,
tr.trans_type_id,
tt.trans_type_name,
ee.eport_num,
c.customer_id,
tr.submission_num
FROM report.trans tr
JOIN corp.currency cr
ON cr.currency_id = tr.currency_id
JOIN report.trans_state ts
ON ts.state_id = tr.settle_state_id
LEFT OUTER JOIN (
	report.terminal t
	JOIN corp.customer c
	ON c.customer_id = t.customer_id
	JOIN corp.business_unit bu
	ON bu.business_unit_id = t.business_unit_id
)
ON t.terminal_id = tr.terminal_id
JOIN corp.ledger led
ON led.trans_id = tr.tran_id
AND led.deleted = 'N'
JOIN report.trans_type tt
ON tr.trans_type_id = tt.trans_type_id
LEFT OUTER JOIN (
SELECT DISTINCT terminal_id, FIRST_VALUE(eport_serial_num) OVER (PARTITION BY terminal_id ORDER BY start_date DESC) eport_num
FROM report.terminal_eport te
JOIN report.eport e ON te.eport_id = e.eport_id
) ee ON tr.terminal_id = ee.terminal_id
WHERE tr.settle_state_id IN (2, 3)
AND led.entry_type IN ('CC', 'PF', 'CB', 'RF')
AND (tr.settle_date >= TRUNC(SYSDATE) - 1 AND tr.settle_date < TRUNC(SYSDATE)
OR led.create_date >= TRUNC(SYSDATE) - 1 AND led.create_date < TRUNC(SYSDATE) AND tr.settle_date IS NULL)
GROUP BY
led.entry_type,
COALESCE(bu.business_unit_name, '~ Terminal Orphan'),
COALESCE(DECODE(t.payment_schedule_id, 4, 'Y', 'N'), 'UNK'),
cr.currency_code,
COALESCE(c.customer_name, '~ Terminal Orphan'),
c.customer_id,
TRUNC(DECODE (led.entry_type,
	'CC', tr.settle_date,
	led.ledger_date
), 'MONTH'),
tr.trans_type_id,
tt.trans_type_name,
ee.eport_num,
tr.submission_num]'
WHERE REPORT_ID = 487 AND PARAM_NAME = 'query';
COMMIT;

GRANT SELECT ON PSS.DOWNGRADE_REASON TO USAT_DMS_ROLE;

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/DBADMIN/PKG_GLOBAL.pbk?revision=1.12
CREATE OR REPLACE PACKAGE BODY DBADMIN.PKG_GLOBAL IS
    PROCEDURE SET_TRANSACTION_VARIABLE(
        pv_name DBADMIN.TRANSACTION_VARIABLE.VARIABLE_NAME%TYPE,
        pv_value DBADMIN.TRANSACTION_VARIABLE.VARIABLE_VALUE%TYPE)
    AS
    BEGIN
        UPDATE DBADMIN.TRANSACTION_VARIABLE
           SET VARIABLE_VALUE = pv_value
         WHERE VARIABLE_NAME = pv_name;
        IF SQL%ROWCOUNT = 1 THEN
            RETURN;
        END IF;
        INSERT INTO DBADMIN.TRANSACTION_VARIABLE(VARIABLE_NAME, VARIABLE_VALUE)
          VALUES(pv_name, pv_value);    
    END;
    
    FUNCTION GET_TRANSACTION_VARIABLE(
        pv_name DBADMIN.TRANSACTION_VARIABLE.VARIABLE_NAME%TYPE)
    RETURN DBADMIN.TRANSACTION_VARIABLE.VARIABLE_VALUE%TYPE
    AS
        lv_value DBADMIN.TRANSACTION_VARIABLE.VARIABLE_VALUE%TYPE;
    BEGIN
        SELECT MAX(VARIABLE_VALUE)
          INTO lv_value
          FROM DBADMIN.TRANSACTION_VARIABLE
         WHERE VARIABLE_NAME = pv_name;
        RETURN lv_value;
    END;
    
    PROCEDURE CREATE_LOCK(
        pv_object_type VARCHAR2,
        pv_object_id   VARCHAR2,
        pt_last_lock_utc_ts TIMESTAMP)
    AS
    PRAGMA AUTONOMOUS_TRANSACTION;
    BEGIN
        INSERT INTO DBADMIN.APP_LOCK(
            APP_LOCK_TYPE,
            APP_LOCK_INSTANCE,
            LAST_LOCK_UTC_TS,
            LAST_LOCK_BY
        ) VALUES(
            pv_object_type,
            pv_object_id,
            pt_last_lock_utc_ts,
            USER
        ); 
        COMMIT;
    EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
            NULL;
    END;
   
    FUNCTION TRY_UPDATE_LOCK(
        pv_object_type VARCHAR2,
        pv_object_id   VARCHAR2,
        pt_last_lock_utc_ts TIMESTAMP)
        RETURN PLS_INTEGER
    IS
    BEGIN
        UPDATE DBADMIN.APP_LOCK 
           SET LAST_LOCK_UTC_TS = pt_last_lock_utc_ts,
               LAST_LOCK_BY = USER
         WHERE APP_LOCK_TYPE = pv_object_type
           AND APP_LOCK_INSTANCE = pv_object_id;
        RETURN SQL%ROWCOUNT;
    END;
    
    FUNCTION REQUEST_LOCK(
        pv_object_type VARCHAR2,
        pv_object_id   VARCHAR2,
        pb_update_first BOOLEAN DEFAULT FALSE)
     RETURN VARCHAR2
    IS
        lt_last_lock_utc_ts TIMESTAMP(6);
    BEGIN
	    lt_last_lock_utc_ts := SYS_EXTRACT_UTC(SYSTIMESTAMP);
        IF pb_update_first AND TRY_UPDATE_LOCK(pv_object_type, pv_object_id, lt_last_lock_utc_ts) >= 1 THEN
		    RETURN TO_CHAR(lt_last_lock_utc_ts);
	    END IF;
        CREATE_LOCK(pv_object_type, pv_object_id, lt_last_lock_utc_ts);
        IF TRY_UPDATE_LOCK(pv_object_type, pv_object_id, lt_last_lock_utc_ts) >= 1 THEN
            RETURN TO_CHAR(lt_last_lock_utc_ts);
        END IF;
        RAISE_APPLICATION_ERROR(-20111, 'AppLock Row for object ' || pv_object_type || '.' || pv_object_id || ' was deleted while using it');
    END;
    
    PROCEDURE RELEASE_LOCK(
        pv_handle VARCHAR2)
    IS
    BEGIN
        -- this procedure is provided for backwards compatibility only
        NULL;
    END;
	
	FUNCTION GET_APP_SETTING(
		pv_app_setting_cd ENGINE.APP_SETTING.APP_SETTING_CD%TYPE
	) RETURN ENGINE.APP_SETTING.APP_SETTING_VALUE%TYPE
	IS
		lv_app_setting_value ENGINE.APP_SETTING.APP_SETTING_VALUE%TYPE;
	BEGIN
		SELECT MAX(APP_SETTING_VALUE)
		INTO lv_app_setting_value
		FROM ENGINE.APP_SETTING
		WHERE APP_SETTING_CD = pv_app_setting_cd;
		
		RETURN lv_app_setting_value;
	END;
	
    PROCEDURE LOCK_PROCESS(
        pv_process_cd ENGINE.APP_SETTING.APP_SETTING_CD%TYPE,
		pv_process_token_cd ENGINE.APP_SETTING.APP_SETTING_VALUE%TYPE,
        pv_success_flag OUT VARCHAR2,
        pv_locked_by_process_token_cd OUT ENGINE.APP_SETTING.APP_SETTING_VALUE%TYPE)
    IS
		lv_app_setting_cd ENGINE.APP_SETTING.APP_SETTING_CD%TYPE := pv_process_cd || '_LOCK';
		ln_lock_max_duration_sec NUMBER;
		lt_last_updated_utc_ts ENGINE.APP_SETTING.LAST_UPDATED_UTC_TS%TYPE;
    BEGIN
		pv_success_flag := 'N';
	
		UPDATE ENGINE.APP_SETTING
		SET APP_SETTING_VALUE = pv_process_token_cd
		WHERE APP_SETTING_CD = lv_app_setting_cd
			AND (APP_SETTING_VALUE IS NULL
				OR APP_SETTING_VALUE = pv_process_token_cd)
		RETURNING 'Y', APP_SETTING_VALUE INTO pv_success_flag, pv_locked_by_process_token_cd;
			
		IF pv_success_flag = 'Y' THEN
			RETURN;
		END IF;
	
        SELECT APP_SETTING_VALUE, LAST_UPDATED_UTC_TS
        INTO pv_locked_by_process_token_cd, lt_last_updated_utc_ts
        FROM ENGINE.APP_SETTING
        WHERE APP_SETTING_CD = lv_app_setting_cd;
		ln_lock_max_duration_sec := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING(pv_process_cd || '_MAX_DURATION_SEC'));	
		IF lt_last_updated_utc_ts < SYS_EXTRACT_UTC(SYSTIMESTAMP) - ln_lock_max_duration_sec/86400 THEN
			pkg_exception_processor.sp_log_exception (
				pkg_app_exec_hist_globals.unknown_error_id,
				pv_process_cd || ' has been locked by ' || pv_locked_by_process_token_cd || ' since ' || lt_last_updated_utc_ts || ' UTC, unlocking',
				NULL,
				'DBADMIN.PKG_GLOBAL.LOCK_PROCESS'
			);
			
			UPDATE ENGINE.APP_SETTING
			SET APP_SETTING_VALUE = pv_process_token_cd
			WHERE APP_SETTING_CD = lv_app_setting_cd
			RETURNING 'Y', APP_SETTING_VALUE INTO pv_success_flag, pv_locked_by_process_token_cd;
		END IF;
    END;
    
    PROCEDURE UNLOCK_PROCESS(
        pv_process_cd ENGINE.APP_SETTING.APP_SETTING_CD%TYPE,
		pv_process_token_cd ENGINE.APP_SETTING.APP_SETTING_VALUE%TYPE)
    IS
    BEGIN
        UPDATE ENGINE.APP_SETTING
		SET APP_SETTING_VALUE = NULL
		WHERE APP_SETTING_CD = pv_process_cd || '_LOCK'
			AND APP_SETTING_VALUE = pv_process_token_cd;
    END;
END; 
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/DATA_IN_PKG.psk?revision=1.27
CREATE OR REPLACE PACKAGE REPORT.DATA_IN_PKG IS
--
-- Receives data from external systems
--
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------
-- BKRUG        10-18-04    NEW

  PROCEDURE ADD_TRAN_ITEM(
        l_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE,
        l_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE,
        l_column_num PURCHASE.MDB_NUMBER%TYPE,
        l_column_label PURCHASE.VEND_COLUMN%TYPE,
        l_quantity PURCHASE.AMOUNT%TYPE DEFAULT 1,
        l_price PURCHASE.PRICE%TYPE DEFAULT NULL,
        l_product_desc PURCHASE.DESCRIPTION%TYPE DEFAULT NULL,
        l_tran_line_item_type_id PURCHASE.TRAN_LINE_ITEM_TYPE_ID%TYPE DEFAULT NULL,
        l_apply_to_consumer_acct_id PURCHASE.APPLY_TO_CONSUMER_ACCT_ID%TYPE DEFAULT NULL, 
        l_campaign_id CAMPAIGN.CAMPAIGN_ID%TYPE DEFAULT NULL,
        pn_tran_id PURCHASE.TRAN_ID%TYPE DEFAULT NULL,
        pd_tran_date PURCHASE.TRAN_DATE%TYPE DEFAULT NULL);

  PROCEDURE UPDATE_DEVICE_INFO(
        l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
        l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE,
        l_device_type_id EPORT.DEVICE_TYPE_ID%TYPE/*,
        l_device_name EPORT.DEVICE_NAME%TYPE*/);

  PROCEDURE ADD_FILL(
       l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
       l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE,
       l_fill_date IN FILL.FILL_DATE%TYPE);

   PROCEDURE ADD_TRANSACTION(
        l_dup_flag OUT VARCHAR,
        l_report_tran_id OUT REPORT.TRANS.TRAN_ID%TYPE,
        l_reload_flag OUT VARCHAR,
        l_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE,
        l_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE,
        l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
        l_start_date TRANS.START_DATE%TYPE,
        l_close_date TRANS.CLOSE_DATE%TYPE,
        l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE,
        l_total_amount TRANS.TOTAL_AMOUNT%TYPE,
        l_card_number TRANS.CARD_NUMBER%TYPE,
        l_received_date TRANS.SERVER_DATE%TYPE,
        l_settle_state_id TRANS.SETTLE_STATE_ID%TYPE,
        l_settle_date TRANS.SETTLE_DATE%TYPE,
        l_preauth_amount TRANS.PREAUTH_AMOUNT%TYPE,
        l_preauth_date   TRANS.PREAUTH_DATE%TYPE,
        l_approval_cd TRANS.CC_APPR_CODE%TYPE,
        l_merchant_num CORP.MERCHANT.MERCHANT_NBR%TYPE,
        l_description TRANS.DESCRIPTION%TYPE,
        l_orig_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE DEFAULT NULL,
        l_orig_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE DEFAULT NULL,
        l_currency_code CORP.CURRENCY.CURRENCY_CODE%TYPE,
        l_device_type EPORT.DEVICE_TYPE_ID%TYPE DEFAULT NULL,
        l_card_name REPORT.CARD_TYPE.CARD_NAME%TYPE DEFAULT NULL,
        l_consumer_acct_id TRANS.CONSUMER_ACCT_ID%TYPE DEFAULT NULL,
        l_source_tran_id REPORT.TRANS.SOURCE_TRAN_ID%TYPE DEFAULT NULL,
        l_tran_device_tran_cd REPORT.TRANS.TRAN_DEVICE_TRAN_CD%TYPE DEFAULT NULL,
        l_consumer_pass_id REPORT.TRANS.CONSUMER_PASS_ID%TYPE DEFAULT NULL,
        l_orig_source_tran_id REPORT.TRANS.SOURCE_TRAN_ID%TYPE DEFAULT NULL);
       

  PROCEDURE UPDATE_SETTLE_INFO(
        l_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE,
        l_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE,
        l_settle_state_id TRANS.SETTLE_STATE_ID%TYPE,
        l_settle_date TRANS.SETTLE_DATE%TYPE,
        l_approval_cd TRANS.CC_APPR_CODE%TYPE,
        l_merchant_num CORP.MERCHANT.MERCHANT_NBR%TYPE,
        ln_amount REPORT.TRANS.TOTAL_AMOUNT%TYPE DEFAULT NULL,
        pn_override_trans_type_id REPORT.TRANS.TRANS_TYPE_ID%TYPE DEFAULT NULL,
        pn_source_tran_id REPORT.TRANS.SOURCE_TRAN_ID%TYPE DEFAULT NULL,
        pv_submission_num REPORT.TRANS.SUBMISSION_NUM%TYPE DEFAULT NULL);

  PROCEDURE UPDATE_BEX_LOCATION(
        l_serial_num EPORT.EPORT_SERIAL_NUM%TYPE,
        l_location_name LOCATION.LOCATION_NAME%TYPE,
        l_customer_abbr CORP.CUSTOMER.CUSTOMER_ALT_NAME%TYPE,
        l_effective_date DATE);

  FUNCTION GET_OR_CREATE_DEVICE(
        l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
        l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE,
        l_device_type EPORT.DEVICE_TYPE_ID%TYPE DEFAULT NULL,
        l_tries PLS_INTEGER DEFAULT 10)
     RETURN EPORT.EPORT_ID%TYPE;
END; -- Package spec
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/DATA_IN_PKG.pbk?revision=1.56.2.1
CREATE OR REPLACE PACKAGE BODY REPORT.DATA_IN_PKG IS
   FK_NOT_FOUND EXCEPTION;
   PRAGMA EXCEPTION_INIT(FK_NOT_FOUND, -2291);

    FUNCTION GET_OR_CREATE_DEVICE(
        l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
        l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE,
        l_device_type EPORT.DEVICE_TYPE_ID%TYPE DEFAULT NULL,
        l_tries PLS_INTEGER DEFAULT 10)
     RETURN EPORT.EPORT_ID%TYPE
    IS
        PRAGMA AUTONOMOUS_TRANSACTION;
        l_device_id EPORT.EPORT_ID%TYPE;
        l_use_device_type EPORT.DEVICE_TYPE_ID%TYPE;
    BEGIN
        SELECT EPORT_ID
          INTO l_device_id
          FROM EPORT
         WHERE EPORT_SERIAL_NUM = l_device_serial;
         -- NOTE: eventually the above should be source system dependent
         RETURN l_device_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            SELECT EPORT_SEQ.NEXTVAL INTO l_device_id FROM DUAL;
            IF l_device_type IS NOT NULL THEN
                l_use_device_type := l_device_type;
            ELSIF l_device_serial LIKE 'E4%' THEN
                l_use_device_type := 0; -- G4
			ELSIF l_device_serial LIKE 'EE%' THEN
                l_use_device_type := 13; -- Edge
            ELSIF l_device_serial LIKE 'G%' THEN
                l_use_device_type := 1; -- G5-G8
			ELSIF l_device_serial LIKE 'K%' THEN
                l_use_device_type := 11; -- Kiosk
            ELSIF l_device_serial LIKE 'M1%' THEN
                l_use_device_type := 6; -- MEI
            --ELSIF l_device_serial LIKE '10%' THEN -- esuds
            ELSIF l_device_serial LIKE '10%' THEN
                l_use_device_type := 3; -- Radisys Brick
            ELSE
                l_use_device_type := 10;
            END IF;
            BEGIN
                INSERT INTO EPORT(EPORT_ID, EPORT_SERIAL_NUM, ACTIVATION_DATE, DEVICE_TYPE_ID)
			  		 VALUES(l_device_id, l_device_serial, SYSDATE, l_use_device_type);
 		        COMMIT;
            EXCEPTION
                WHEN FK_NOT_FOUND THEN
                    ROLLBACK;
                    RAISE_APPLICATION_ERROR(-20889, 'Device Type ''' || l_use_device_type || ''' does not exist');
                WHEN DUP_VAL_ON_INDEX THEN
                    ROLLBACK;
                    IF l_tries > 0 THEN
                        RETURN GET_OR_CREATE_DEVICE(l_device_serial, l_source_system_cd, l_device_type, l_tries - 1);
                    ELSE
                        RAISE;
                    END IF;
                WHEN OTHERS THEN
                    ROLLBACK;
                    RAISE;
            END;
            RETURN l_device_id;
        WHEN OTHERS THEN
            RAISE;
    END;
  
    FUNCTION GET_OR_CREATE_MERCHANT(
        l_merchant_num CORP.MERCHANT.MERCHANT_NBR%TYPE,
        l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE)
     RETURN CORP.MERCHANT.MERCHANT_ID%TYPE
    IS
        PRAGMA AUTONOMOUS_TRANSACTION;
        l_merchant_id CORP.MERCHANT.MERCHANT_ID%TYPE;
    BEGIN
        SELECT MERCHANT_ID
          INTO l_merchant_id
          FROM CORP.MERCHANT
         WHERE MERCHANT_NBR = l_merchant_num;
         RETURN l_merchant_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            SELECT CORP.MERCHANT_SEQ.NEXTVAL INTO l_merchant_id FROM DUAL;
            INSERT INTO CORP.MERCHANT(MERCHANT_ID, MERCHANT_NBR, DESCRIPTION, UPD_BY)
			  		 VALUES(l_merchant_id, l_merchant_num, 'Created for source system "'||l_source_system_cd||'"', 0);
            COMMIT;
            RETURN l_merchant_id;
        WHEN OTHERS THEN
            ROLLBACK;
            RAISE;
    END;

  /* This allows external systems to add transactions. Credit, debit,
   * pass, access, or maintenance cards or refund, chargeback transaction
   * or cash should be added this way. Refunds
   * and chargebacks should always provide a valid original machine tran number.
   */
  PROCEDURE ADD_TRANSACTION(
        l_dup_flag OUT VARCHAR,
        l_report_tran_id OUT REPORT.TRANS.TRAN_ID%TYPE,
        l_reload_flag OUT VARCHAR,
        l_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE,
        l_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE,
        l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
        l_start_date TRANS.START_DATE%TYPE,
        l_close_date TRANS.CLOSE_DATE%TYPE,
        l_trans_type_id TRANS.TRANS_TYPE_ID%TYPE,
        l_total_amount TRANS.TOTAL_AMOUNT%TYPE,
        l_card_number TRANS.CARD_NUMBER%TYPE,
        l_received_date TRANS.SERVER_DATE%TYPE,
        l_settle_state_id TRANS.SETTLE_STATE_ID%TYPE,
        l_settle_date TRANS.SETTLE_DATE%TYPE,
        l_preauth_amount TRANS.PREAUTH_AMOUNT%TYPE,
        l_preauth_date   TRANS.PREAUTH_DATE%TYPE,
        l_approval_cd TRANS.CC_APPR_CODE%TYPE,
        l_merchant_num CORP.MERCHANT.MERCHANT_NBR%TYPE,
        l_description TRANS.DESCRIPTION%TYPE,
        l_orig_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE DEFAULT NULL,
        l_orig_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE DEFAULT NULL,
        l_currency_code CORP.CURRENCY.CURRENCY_CODE%TYPE,
        l_device_type EPORT.DEVICE_TYPE_ID%TYPE DEFAULT NULL,
        l_card_name REPORT.CARD_TYPE.CARD_NAME%TYPE DEFAULT NULL,
        l_consumer_acct_id TRANS.CONSUMER_ACCT_ID%TYPE DEFAULT NULL,
        l_source_tran_id REPORT.TRANS.SOURCE_TRAN_ID%TYPE DEFAULT NULL,
        l_tran_device_tran_cd REPORT.TRANS.TRAN_DEVICE_TRAN_CD%TYPE DEFAULT NULL,
        l_consumer_pass_id REPORT.TRANS.CONSUMER_PASS_ID%TYPE DEFAULT NULL,
        l_orig_source_tran_id REPORT.TRANS.SOURCE_TRAN_ID%TYPE DEFAULT NULL)
    IS
        l_eport_id TRANS.EPORT_ID%TYPE;
        l_merchant_id TRANS.MERCHANT_ID%TYPE;
        l_orig_tran_id TRANS.ORIG_TRAN_ID%TYPE;
        l_real_amount TRANS.TOTAL_AMOUNT%TYPE;
        l_currency_id TRANS.CURRENCY_ID%TYPE;
		l_cardtype_authority_id TRANS.CARDTYPE_AUTHORITY_ID%TYPE := NULL;
		l_lock VARCHAR2(128) := GLOBALS_PKG.REQUEST_LOCK('TRANS.SOURCE_TRAN_ID', l_source_tran_id);
		l_refund_ind REPORT.TRANS_TYPE.REFUND_IND%TYPE;
    	ln_prev_amount REPORT.TRANS.TOTAL_AMOUNT%TYPE;
    	l_cash_back VARCHAR2(1) := 'N';
    	l_rewardable_ind REPORT.TRANS_TYPE.REWARDABLE_IND%TYPE;
      	l_reward_trans_type_id REPORT.TRANS_TYPE.REWARD_TRANS_TYPE_ID%TYPE;
      	l_global_campaign VARCHAR2(1 BYTE);
      	l_trans_type_updated_id REPORT.TRANS_TYPE.TRANS_TYPE_ID%TYPE;
    BEGIN
        -- check for dup
        BEGIN
            SELECT TRAN_ID, CASE WHEN TOTAL_AMOUNT = l_total_amount AND COALESCE(CONSUMER_ACCT_ID, 0) = COALESCE(l_consumer_acct_id, 0) THEN 'Y' ELSE 'N' END
              INTO l_report_tran_id, l_dup_flag
              FROM REPORT.TRANS
             WHERE SOURCE_TRAN_ID = l_source_tran_id;
            IF l_dup_flag = 'Y' THEN -- this is NOT an update
                l_reload_flag :='Y';
                RETURN;  
            END IF;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                l_dup_flag:='N'; -- CONTINUE, no dup found
            WHEN OTHERS THEN
                RAISE;
        END;
        
        --get needed lookup values
        l_eport_id := GET_OR_CREATE_DEVICE(l_device_serial, l_source_system_cd, l_device_type);
        IF l_merchant_num IS NOT NULL THEN
            l_merchant_id := GET_OR_CREATE_MERCHANT(l_merchant_num, l_source_system_cd);
        END IF;
        
        IF l_orig_source_tran_id IS NOT NULL THEN
            BEGIN
                SELECT TRAN_ID
                  INTO l_orig_tran_id
                  FROM REPORT.TRANS
                 WHERE SOURCE_TRAN_ID = l_orig_source_tran_id;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    RAISE_APPLICATION_ERROR(-20888, 'Could not find the original transaction with SOURCE_TRAN_ID = "'||l_orig_source_tran_id ||'" from the source system "'||l_source_system_cd||'". Transaction was NOT added.');
                WHEN OTHERS THEN
                    RAISE;
            END;        
        ELSIF l_orig_machine_trans_no IS NOT NULL THEN
            BEGIN
                SELECT TRAN_ID
                  INTO l_orig_tran_id
                  FROM REPORT.TRANS
                 WHERE MACHINE_TRANS_NO = l_orig_machine_trans_no;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    RAISE_APPLICATION_ERROR(-20888, 'Could not find the original transaction with MACHINE_TRANS_NUM = "'||l_orig_machine_trans_no||'" from the source system "'||l_source_system_cd||'". Transaction was NOT added.');
                WHEN OTHERS THEN
                    RAISE;
            END;
        END IF;
        
        --get currency id from currency code
        IF l_currency_code IS NOT NULL THEN
        	BEGIN
        		SELECT currency_id
        		  INTO l_currency_id
        		  FROM CORP.currency
        		 WHERE currency_code = l_currency_code;
        	EXCEPTION
        		WHEN NO_DATA_FOUND THEN
                    RAISE_APPLICATION_ERROR(-20888, 'No currency is mapped in the'
						|| ' system for currency code ''' || l_currency_code
						|| '''. A transaction can not be added with no currency'
						|| ' identified. Transaction was NOT added.');
                WHEN OTHERS THEN
                    RAISE;
        	END;
        ELSE
        	RAISE_APPLICATION_ERROR(-20880, 'A transaction can not be entered '
				|| 'without a currency identified. Transaction was NOT added.');
        END IF;
		
		IF l_card_name IS NOT NULL THEN
			SELECT MAX(CA.CARDTYPE_AUTHORITY_ID)
			INTO l_cardtype_authority_id
			FROM REPORT.CARDTYPE_AUTHORITY CA
			JOIN REPORT.CARD_TYPE CT ON CA.CARDTYPE_ID = CT.CARD_TYPE_ID			
			WHERE CT.CARD_NAME = l_card_name;
			
			IF l_cardtype_authority_id IS NULL THEN
				SELECT MAX(CA.CARDTYPE_AUTHORITY_ID)
				INTO l_cardtype_authority_id
				FROM REPORT.CARDTYPE_AUTHORITY CA
				JOIN REPORT.CARD_TYPE CT ON CA.CARDTYPE_ID = CT.CARD_TYPE_ID			
				JOIN REPORT.TRANS_TYPE TT ON CT.CARD_NAME = TT.TRANS_TYPE_NAME
				WHERE TT.TRANS_TYPE_ID = l_trans_type_id;
			END IF;
		END IF;
		
		SELECT REFUND_IND, REWARDABLE_IND, REWARD_TRANS_TYPE_ID
		INTO l_refund_ind, l_rewardable_ind, l_reward_trans_type_id
		FROM REPORT.TRANS_TYPE
		WHERE TRANS_TYPE_ID = l_trans_type_id;
        
        SELECT MAX(CASE WHEN TRAN_LINE_ITEM_TYPE_ID IN (554, 555) THEN 'Y' ELSE 'N' END) 
   		INTO l_cash_back
    	FROM PSS.TRAN_LINE_ITEM
    	WHERE TRAN_ID = l_source_tran_id;
        
        IF l_refund_ind = 'Y' OR l_cash_back = 'Y' THEN -- make it negative
            l_real_amount := -ABS(l_total_amount);
        ELSE
            l_real_amount := ABS(l_total_amount);
        END IF;
        
        l_trans_type_updated_id := l_trans_type_id;
      IF l_rewardable_ind = 'Y' THEN
        SELECT MAX(CASE WHEN C.CUSTOMER_ID IS NULL THEN 'Y' ELSE 'N' END)
        INTO l_global_campaign
        FROM PSS.AUTH A
        JOIN REPORT.CAMPAIGN C ON A.AUTH_AUTHORITY_REF_CD = C.CAMPAIGN_ID
        WHERE A.TRAN_ID = l_source_tran_id
          AND A.AUTH_AUTHORITY_TRAN_CD = 'Promotion'
          AND A.AUTH_RESULT_CD = 'Y';
        IF l_global_campaign = 'Y' AND l_reward_trans_type_id IS NOT NULL THEN
          l_trans_type_updated_id := l_reward_trans_type_id;
        END IF;
      END IF;
      
        IF l_report_tran_id IS NULL THEN
            SELECT TRANS_SEQ.NEXTVAL
              INTO l_report_tran_id
              FROM DUAL;
            INSERT INTO REPORT.TRANS(
                TRAN_ID,
                MACHINE_TRANS_NO,
                SOURCE_SYSTEM_CD,
                EPORT_ID,
                CARDTYPE_AUTHORITY_ID,
                START_DATE,
                CLOSE_DATE,
                TRANS_TYPE_ID,
                TOTAL_AMOUNT,
                CARD_NUMBER,
                SERVER_DATE,
                SETTLE_STATE_ID,
                SETTLE_DATE,
                PREAUTH_AMOUNT,
                PREAUTH_DATE,
                CC_APPR_CODE,
                MERCHANT_ID,
                DESCRIPTION,
                ORIG_TRAN_ID,
                CURRENCY_ID,
                CONSUMER_ACCT_ID,
                SOURCE_TRAN_ID,
                TRAN_DEVICE_TRAN_CD,
                CONSUMER_PASS_ID)
              SELECT
                l_report_tran_id,
                l_machine_trans_no,
                l_source_system_cd,
                l_eport_id,
                l_cardtype_authority_id,
                l_start_date,
                l_close_date,
                l_trans_type_updated_id,
                l_real_amount,
                l_card_number,
                l_received_date,
                l_settle_state_id,
                l_settle_date,
                l_preauth_amount,
                l_preauth_date,
                l_approval_cd,
                l_merchant_id,
                l_description,
                l_orig_tran_id,
                l_currency_id,
                l_consumer_acct_id,
                l_source_tran_id,
                l_tran_device_tran_cd,
                l_consumer_pass_id
              FROM DUAL;
              l_reload_flag :='N';
        ELSE
            DELETE
              FROM REPORT.PURCHASE
             WHERE TRAN_ID = l_report_tran_id;
            UPDATE REPORT.TRANS
               SET (EPORT_ID,
                CARDTYPE_AUTHORITY_ID,
                START_DATE,
                CLOSE_DATE,
                TRANS_TYPE_ID,
                TOTAL_AMOUNT,
                CARD_NUMBER,
                SERVER_DATE,
                SETTLE_STATE_ID,
                SETTLE_DATE,
                PREAUTH_AMOUNT,
                PREAUTH_DATE,
                CC_APPR_CODE,
                MERCHANT_ID,
                DESCRIPTION,
                ORIG_TRAN_ID,
                CURRENCY_ID,
                CONSUMER_ACCT_ID,
                SOURCE_TRAN_ID,
                TRAN_DEVICE_TRAN_CD,
                CONSUMER_PASS_ID) = (
                SELECT l_eport_id,
                l_cardtype_authority_id,
                l_start_date,
                l_close_date,
                l_trans_type_updated_id,
                l_real_amount,
                l_card_number,
                l_received_date,
                l_settle_state_id,
                l_settle_date,
                l_preauth_amount,
                l_preauth_date,
                l_approval_cd,
                l_merchant_id,
                l_description,
                l_orig_tran_id,
                l_currency_id,
                l_consumer_acct_id,
                l_source_tran_id,
                l_tran_device_tran_cd,
                l_consumer_pass_id
              FROM DUAL)
             WHERE TRAN_ID = l_report_tran_id;
             l_reload_flag :='Y';
        END IF;
    END;
    
    
PROCEDURE ADD_TRAN_ITEM(
	l_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE,
	l_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE,
	l_column_num PURCHASE.MDB_NUMBER%TYPE,
	l_column_label PURCHASE.VEND_COLUMN%TYPE,
	l_quantity PURCHASE.AMOUNT%TYPE DEFAULT 1,
	l_price PURCHASE.PRICE%TYPE DEFAULT NULL,
	l_product_desc PURCHASE.DESCRIPTION%TYPE DEFAULT NULL,
	l_tran_line_item_type_id PURCHASE.TRAN_LINE_ITEM_TYPE_ID%TYPE DEFAULT NULL,
	l_apply_to_consumer_acct_id PURCHASE.APPLY_TO_CONSUMER_ACCT_ID%TYPE DEFAULT NULL,
	l_campaign_id CAMPAIGN.CAMPAIGN_ID%TYPE DEFAULT NULL,
	pn_tran_id PURCHASE.TRAN_ID%TYPE DEFAULT NULL,
    pd_tran_date PURCHASE.TRAN_DATE%TYPE DEFAULT NULL)
IS
	l_is_global_campaign VARCHAR2(1 BYTE);
	l_is_rewardable VARCHAR2(1 BYTE);
	l_reward_tli_type_id NUMBER;
	l_replace_from VARCHAR2(100 BYTE);
	l_replace_to VARCHAR2(100 BYTE);
	l_tran_id PURCHASE.TRAN_ID%TYPE := pn_tran_id;
    l_tran_date PURCHASE.TRAN_DATE%TYPE := pd_tran_date;
BEGIN	
	IF l_tran_id IS NULL THEN
		SELECT MIN(TRAN_ID), MIN(CLOSE_DATE)
		INTO l_tran_id, l_tran_date
		FROM REPORT.TRANS
		WHERE MACHINE_TRANS_NO = l_machine_trans_no;
		
		IF l_tran_id IS NULL THEN
			RAISE_APPLICATION_ERROR(-20888, 'Could not find a transaction with MACHINE_TRANS_NUM = "'||l_machine_trans_no||'" from the source system "'||l_source_system_cd||'". Item was NOT added.');
		END IF;	
	END IF;

	INSERT INTO REPORT.PURCHASE(
		PURCHASE_ID,
		TRAN_ID,
		TRAN_DATE,
		AMOUNT,
		PRICE,
		VEND_COLUMN,
		DESCRIPTION,
		MDB_NUMBER,
		TRAN_LINE_ITEM_TYPE_ID,
		APPLY_TO_CONSUMER_ACCT_ID)
	VALUES(
		PURCHASE_SEQ.NEXTVAL,
		l_tran_id,
		l_tran_date,
		l_quantity,
		l_price,
		l_column_label,
		l_product_desc,
		l_column_num,
		l_tran_line_item_type_id,
		l_apply_to_consumer_acct_id
	);
	
	IF (l_campaign_id IS NOT NULL) THEN
		SELECT MAX(CASE WHEN CUSTOMER_ID IS NULL THEN 'Y' ELSE 'N' END) 
		INTO l_is_global_campaign
		FROM REPORT.CAMPAIGN
		WHERE CAMPAIGN_ID = l_campaign_id;
		IF l_is_global_campaign = 'Y' THEN
			SELECT REWARDABLE_IND, REWARD_TRAN_LINE_ITEM_TYPE_ID
			INTO l_is_rewardable, l_reward_tli_type_id
			FROM PSS.TRAN_LINE_ITEM_TYPE
			WHERE TRAN_LINE_ITEM_TYPE_ID = l_tran_line_item_type_id;
		ELSE
			l_is_rewardable := 'N';
		END IF;
	ELSE
		l_is_global_campaign := 'N';
	END IF;
	-- create a Reward for every discount line for global campaigns
	IF (l_is_global_campaign = 'Y' AND l_is_rewardable = 'Y') THEN
		IF (l_tran_line_item_type_id IN (214,215)) THEN
			l_replace_from := 'free';
			l_replace_to := 'free Reward';
		ELSE
			l_replace_from := 'Discount';
			l_replace_to := 'Reward';
		END IF;
		INSERT INTO REPORT.PURCHASE(
			PURCHASE_ID,
			TRAN_ID,
			TRAN_DATE,
			AMOUNT,
			PRICE,
			VEND_COLUMN,
			DESCRIPTION,
			MDB_NUMBER,
			TRAN_LINE_ITEM_TYPE_ID,
			APPLY_TO_CONSUMER_ACCT_ID
		) VALUES(
			PURCHASE_SEQ.NEXTVAL,
			l_tran_id,
			l_tran_date,
			l_quantity,
			ABS(l_price),
			REPLACE(l_column_label, l_replace_from, l_replace_to),
			REPLACE(l_product_desc, l_replace_from, l_replace_to),
			l_column_num,
			l_reward_tli_type_id,
			l_apply_to_consumer_acct_id	
		);
		UPDATE REPORT.TRANS
		SET TOTAL_AMOUNT = TOTAL_AMOUNT + ABS(l_price) * l_quantity,
			APPLY_TO_CONSUMER_ACCT_ID = NVL(l_apply_to_consumer_acct_id, APPLY_TO_CONSUMER_ACCT_ID)
		WHERE TRAN_ID = l_tran_id;
	END IF;
	
	IF l_apply_to_consumer_acct_id IS NOT NULL THEN
		UPDATE REPORT.TRANS
		SET APPLY_TO_CONSUMER_ACCT_ID = l_apply_to_consumer_acct_id
		WHERE TRAN_ID = l_tran_id;
	END IF;
END;
    
    /* This procedure allows external systems to update the settlement info
     * of a transaction.
     *
     */
    PROCEDURE UPDATE_SETTLE_INFO(
        l_machine_trans_no TRANS.MACHINE_TRANS_NO%TYPE,
        l_source_system_cd TRANS.SOURCE_SYSTEM_CD%TYPE,
        l_settle_state_id TRANS.SETTLE_STATE_ID%TYPE,
        l_settle_date TRANS.SETTLE_DATE%TYPE,
        l_approval_cd TRANS.CC_APPR_CODE%TYPE,
        l_merchant_num CORP.MERCHANT.MERCHANT_NBR%TYPE,
        ln_amount REPORT.TRANS.TOTAL_AMOUNT%TYPE DEFAULT NULL,
        pn_override_trans_type_id REPORT.TRANS.TRANS_TYPE_ID%TYPE DEFAULT NULL,
        pn_source_tran_id REPORT.TRANS.SOURCE_TRAN_ID%TYPE DEFAULT NULL,
		pv_submission_num REPORT.TRANS.SUBMISSION_NUM%TYPE DEFAULT NULL)
    IS
        l_merchant_id TRANS.MERCHANT_ID%TYPE;
        ln_tran_id REPORT.TRANS.TRAN_ID%TYPE;
        lc_imported PSS.SALE.IMPORTED%TYPE;
        ln_old_amount REPORT.TRANS.TOTAL_AMOUNT%TYPE;
        ln_old_trans_type_id REPORT.TRANS.TRANS_TYPE_ID%TYPE;
    BEGIN
	    -- Reversal of Duplicate Submission transactions are not imported
	    IF l_machine_trans_no LIKE 'RV:%' THEN	    	
	    	RETURN;
	    END IF;
	    
        IF l_merchant_num IS NOT NULL THEN
            l_merchant_id := GET_OR_CREATE_MERCHANT(l_merchant_num, l_source_system_cd);
        END IF;
        IF pn_source_tran_id IS NOT NULL THEN
        	UPDATE REPORT.TRANS T SET (SETTLE_STATE_ID, SETTLE_DATE, CC_APPR_CODE, MERCHANT_ID, SUBMISSION_NUM) =
	            (SELECT NVL(l_settle_state_id, T.SETTLE_STATE_ID),
	                    l_settle_date,
	                    NVL(l_approval_cd, T.CC_APPR_CODE),
	                    NVL(l_merchant_id, T.MERCHANT_ID),
						NVL(pv_submission_num, T.SUBMISSION_NUM)
	              FROM DUAL)
	        WHERE T.SOURCE_TRAN_ID = pn_source_tran_id
            RETURNING TRAN_ID, TOTAL_AMOUNT, TRANS_TYPE_ID
            INTO ln_tran_id, ln_old_amount, ln_old_trans_type_id;
        ELSE
	        UPDATE REPORT.TRANS T SET (SETTLE_STATE_ID, SETTLE_DATE, CC_APPR_CODE, MERCHANT_ID, SUBMISSION_NUM) =
	            (SELECT NVL(l_settle_state_id, T.SETTLE_STATE_ID),
	                    l_settle_date,
	                    NVL(l_approval_cd, T.CC_APPR_CODE),
	                    NVL(l_merchant_id, T.MERCHANT_ID),
						NVL(pv_submission_num, T.SUBMISSION_NUM)
	              FROM DUAL)
	        WHERE T.MACHINE_TRANS_NO = l_machine_trans_no
            RETURNING TRAN_ID, TOTAL_AMOUNT, TRANS_TYPE_ID
            INTO ln_tran_id, ln_old_amount, ln_old_trans_type_id;
        END IF;
        IF SQL%ROWCOUNT = 0 THEN
            IF l_settle_state_id = 2 AND ln_amount = 0 THEN
                IF pn_source_tran_id IS NOT NULL THEN
                	SELECT NVL(MAX(IMPORTED), '-')
                  	INTO lc_imported
                  	FROM PSS.TRAN X
                  	JOIN PSS.SALE S ON X.TRAN_ID = S.TRAN_ID
                 	WHERE X.TRAN_ID = pn_source_tran_id;
                ELSE
            		SELECT NVL(MAX(IMPORTED), '-')
                  	INTO lc_imported
                  	FROM PSS.TRAN X
                  	JOIN PSS.SALE S ON X.TRAN_ID = S.TRAN_ID
                 	WHERE X.TRAN_GLOBAL_TRANS_CD = l_machine_trans_no;
                END IF;
                IF lc_imported = '-' THEN
                    RETURN; -- sale was cancelled before it was imported
                END IF;
            END IF;
            RAISE_APPLICATION_ERROR(-20888, 'Could not find a transaction with MACHINE_TRANS_NUM = "'||l_machine_trans_no||'". Settle info was NOT updated.');
        END IF;
        IF (ln_amount IS NOT NULL AND ln_amount != ln_old_amount) OR (pn_override_trans_type_id IS NOT NULL AND pn_override_trans_type_id != ln_old_trans_type_id) THEN
            UPDATE REPORT.TRANS T
               SET TOTAL_AMOUNT = NVL(ln_amount, T.TOTAL_AMOUNT),
                   TRANS_TYPE_ID = NVL(pn_override_trans_type_id, T.TRANS_TYPE_ID)
             WHERE TRAN_ID = ln_tran_id;
        END IF;
        REPORT.SYNC_PKG.RECEIVE_TRANS_SETTLEMENT(ln_tran_id, l_settle_state_id, l_settle_date, l_approval_cd);
    END;
    
    /* This procedure allows external systems to update the device info
     * of their devices
     */
    PROCEDURE UPDATE_DEVICE_INFO(
        l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
        l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE,
        l_device_type_id EPORT.DEVICE_TYPE_ID%TYPE/*,
        l_device_name EPORT.DEVICE_NAME%TYPE*/)
    IS
        l_device_id EPORT.EPORT_ID%TYPE;
    BEGIN
        l_device_id := GET_OR_CREATE_DEVICE(l_device_serial, l_source_system_cd);
        UPDATE EPORT E SET DEVICE_TYPE_ID = l_device_type_id
         WHERE E.EPORT_ID = l_device_id;
    END;
    
    PROCEDURE ADD_FILL(
       l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
       l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE,
       l_fill_date IN FILL.FILL_DATE%TYPE)
    IS
       l_prev_fill_date FILL.FILL_DATE%TYPE;
       l_eport_id FILL.EPORT_ID%TYPE;
    BEGIN
        l_eport_id := GET_OR_CREATE_DEVICE(l_device_serial, l_source_system_cd);
  	    INSERT INTO FILL (FILL_ID, EPORT_ID, FILL_DATE)
 	         VALUES(FILL_SEQ.NEXTVAL, l_eport_id, l_fill_date);
    END;
    /* need to change alerts to relate to devices not terminals
    PROCEDURE ADD_ALERT(
       l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
       l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE,
       l_alert_id ALERT.ALERT_ID%TYPE,
       l_alert_date TERMINAL_ALERT.ALERT_DATE%TYPE,
       l_details TERMINAL_ALERT.DETAILS%TYPE)
    IS
       l_eport_id FILL.EPORT_ID%TYPE;
    BEGIN
        l_eport_id := GET_OR_CREATE_DEVICE(l_device_serial, l_source_system_cd);
    	 SELECT TERMINAL_ALERT_SEQ.NEXTVAL INTO l_id FROM DUAL;
    	 INSERT INTO TERMINAL_ALERT (TERMINAL_ALERT_ID, ALERT_ID, TERMINAL_ID, ALERT_DATE, DETAILS, RESPONSE_SENT )
     		VALUES(l_id,l_alert_id,l_terminal_id,l_alert_date,l_details, l_sent);
    END;
    */
    /*
    Right now Legacy G4 and BEX do not have any additional refund information (PROBLEM_DATE is the TRAN_DATE, REFUND_STATUS is always 1)
    */
    
    PROCEDURE UPDATE_POS_INFO(
        l_device_serial EPORT.EPORT_SERIAL_NUM%TYPE,
        l_source_system_cd SOURCE_SYSTEM.SOURCE_SYSTEM_CD%TYPE,
        l_location_name LOCATION.LOCATION_NAME%TYPE,
        l_customer_name CUSTOMER.CUSTOMER_NAME%TYPE,
        l_effective_date TERMINAL_EPORT.START_DATE%TYPE)
    IS
        l_device_id EPORT.EPORT_ID%TYPE;
    BEGIN
        l_device_id := GET_OR_CREATE_DEVICE(l_device_serial, l_source_system_cd);
        /*UPDATE EPORT E SET DEVICE_TYPE_ID = l_device_type_id
         WHERE E.EPORT_SERIAL_NUM = l_device_serial;*/
    END;

    /* Creates a customer, location and terminal, if necessary for the given device
     * This should only be used by BEX machines because they are currently not
     * configured in the Customer Reporting System.
     */
    PROCEDURE UPDATE_BEX_LOCATION(
        l_serial_num EPORT.EPORT_SERIAL_NUM%TYPE,
        l_location_name LOCATION.LOCATION_NAME%TYPE,
        l_customer_abbr CORP.CUSTOMER.CUSTOMER_ALT_NAME%TYPE,
        l_effective_date DATE)
    IS
        l_customer_id CORP.CUSTOMER.CUSTOMER_ID%TYPE;
        l_location_id REPORT.LOCATION.LOCATION_ID%TYPE;
        l_terminal_id REPORT.TERMINAL.TERMINAL_ID%TYPE;
        l_eport_id REPORT.EPORT.EPORT_ID%TYPE;
        l_terminal_nbr REPORT.TERMINAL.TERMINAL_NBR%TYPE;
        l_te_id REPORT.TERMINAL_EPORT.TERMINAL_EPORT_ID%TYPE;
        l_old_start_date REPORT.TERMINAL_EPORT.START_DATE%TYPE;
    BEGIN
        SELECT EPORT_ID
          INTO l_eport_id
          FROM EPORT
         WHERE EPORT_SERIAL_NUM = l_serial_num;

        SELECT MAX(CUSTOMER_ID)
          INTO l_customer_id
          FROM CORP.CUSTOMER
         WHERE CUSTOMER_ALT_NAME = l_customer_abbr;

        IF l_customer_id IS NULL THEN
            SELECT CORP.CUSTOMER_SEQ.NEXTVAL
              INTO l_customer_id
              FROM DUAL;

            INSERT INTO CORP.CUSTOMER(CUSTOMER_ID, CUSTOMER_NAME, CUSTOMER_ALT_NAME, STATUS, CREATE_BY)
	           VALUES(l_customer_id, l_customer_abbr, l_customer_abbr, 'A', 0);	
        END IF;

        SELECT MAX(LOCATION_ID)
          INTO l_location_id
          FROM LOCATION
         WHERE LOCATION_NAME = l_location_name
           AND EPORT_ID = l_eport_id;

        IF l_location_id IS NULL THEN
		  	 SELECT LOCATION_SEQ.NEXTVAL INTO l_location_id FROM DUAL;
	  		 INSERT INTO LOCATION(LOCATION_ID, TERMINAL_ID, LOCATION_NAME, CREATE_BY, STATUS, EPORT_ID)
	  		     VALUES(l_location_id, 0, l_location_name, 0, 'A', l_eport_id);
        END IF;

        SELECT MAX(TE.TERMINAL_ID)
          INTO l_terminal_id
          FROM TERMINAL_EPORT TE, TERMINAL T
         WHERE TE.TERMINAL_ID = T.TERMINAL_ID
           AND TE.EPORT_ID = l_eport_id
           AND T.CUSTOMER_ID = l_customer_id
           AND T.LOCATION_ID = l_location_id;

        IF l_terminal_id IS NULL THEN
            --customer_id or location_id changed, or its new
            SELECT MAX(T.TERMINAL_NBR)
              INTO l_terminal_nbr
              FROM TERMINAL_EPORT TE, TERMINAL T
             WHERE TE.TERMINAL_ID = T.TERMINAL_ID
               AND TE.EPORT_ID = l_eport_id;
            IF l_terminal_nbr IS NULL THEN
                l_terminal_nbr := l_serial_num;
            ELSIF l_terminal_nbr = l_serial_num THEN
                l_terminal_nbr := l_serial_num || '-1';
            ELSIF l_terminal_nbr LIKE l_serial_num || '-%' THEN
                l_terminal_nbr := l_serial_num || '-' || TO_CHAR(TO_NUMBER(SUBSTR(l_terminal_nbr, INSTR(l_terminal_nbr, '-', -1) + 1)) + 1);
            ELSE
                l_terminal_nbr := l_serial_num;
            END IF;
            SELECT TERMINAL_SEQ.NEXTVAL INTO l_terminal_id FROM DUAL;
            INSERT INTO TERMINAL(TERMINAL_ID, TERMINAL_NBR, TERMINAL_NAME, EPORT_ID, CUSTOMER_ID, LOCATION_ID, BUSINESS_UNIT_ID, PAYMENT_SCHEDULE_ID)
                VALUES(l_terminal_id, l_terminal_nbr, l_serial_num, l_eport_id, l_customer_id, l_location_id, 2, 4);
            TERMINAL_EPORT_UPD(l_terminal_id, l_eport_id, l_effective_date, NULL, NULL);
        ELSE
            -- ensure terminal_eport start date is early enough
            BEGIN
                SELECT TERMINAL_EPORT_ID, START_DATE
                  INTO l_te_id, l_old_start_date
                  FROM (SELECT TERMINAL_EPORT_ID, START_DATE
                          FROM TERMINAL_EPORT
                         WHERE TERMINAL_ID = l_terminal_id
                           AND EPORT_ID = l_eport_id
                           AND NVL(END_DATE, MAX_DATE) > l_effective_date
                           ORDER BY START_DATE ASC)
                 WHERE ROWNUM = 1;
                IF l_old_start_date > l_effective_date THEN -- we need to adjust
                    TERMINAL_EPORT_UPD(l_terminal_id, l_eport_id, l_effective_date, NULL, l_te_id);
                END IF;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN -- Need to add new entry
                    TERMINAL_EPORT_UPD(l_terminal_id, l_eport_id, l_effective_date, NULL, NULL);
                WHEN OTHERS THEN
                    RAISE;
            END;
        END IF;
    END;
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/PSS/PKG_SETTLEMENT.psk?revision=1.27
CREATE OR REPLACE PACKAGE PSS.PKG_SETTLEMENT AS
    WRONG_SETTLE_BATCH_STATE EXCEPTION;
    PRAGMA EXCEPTION_INIT(WRONG_SETTLE_BATCH_STATE, -20561);
    
    WRONG_TRAN_STATE EXCEPTION;
    PRAGMA EXCEPTION_INIT(WRONG_TRAN_STATE, -20562);
    
    FUNCTION AFTER_SETTLE_TRAN_STATE_CD(
        pc_auth_type_cd PSS.AUTH.AUTH_TYPE_CD%TYPE,
        pc_refund_type_cd PSS.REFUND.REFUND_TYPE_CD%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pc_device_result_type_cd PSS.TRAN.TRAN_DEVICE_RESULT_TYPE_CD%TYPE)
    RETURN PSS.TRAN.TRAN_STATE_CD%TYPE
    PARALLEL_ENABLE DETERMINISTIC;
    
    PROCEDURE GET_PENDING_ACTIONS(
        pn_payment_subtype_key_id PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE, 
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_settlement_retry_interval NUMBER,
        pn_tran_retry_interval NUMBER,
        pn_max_settlements PLS_INTEGER, 
        pn_max_trans PLS_INTEGER,
        pn_terminal_state_id OUT PSS.TERMINAL.TERMINAL_STATE_ID%TYPE,
        pn_pending_terminal_batch_ids OUT NUMBER_TABLE,
        pn_pending_tran_ids OUT NUMBER_TABLE,
        pn_terminal_batch_id OUT PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE,
        pc_call_again_flag OUT VARCHAR2);
        
    PROCEDURE GET_OR_CREATE_SETTLEMENT_BATCH(
        pn_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE,
        pn_settlement_batch_id OUT PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_prior_attempts OUT PLS_INTEGER,
        pc_upload_needed_flag OUT VARCHAR2);
        
    PROCEDURE UPDATE_PROCESSED_BATCH(
        pn_settlement_batch_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pv_authority_response_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
        pv_authority_response_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
        pd_batch_closed_ts PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_END_TS%TYPE);
        
    PROCEDURE PREPARE_PENDING_TRAN(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pn_sale_auth_id OUT PSS.AUTH.AUTH_ID%TYPE,
        pn_refund_id OUT PSS.REFUND.REFUND_ID%TYPE,
        pc_sale_phase_cd OUT VARCHAR2,
        pn_prior_attempts OUT PLS_INTEGER,
        pn_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE);
    
    -- R37 and above signature
    PROCEDURE UPDATE_PROCESSED_TRAN(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pn_sale_auth_id PSS.AUTH.AUTH_ID%TYPE,
        pn_refund_id PSS.REFUND.REFUND_ID%TYPE,
        pc_settled_flag CHAR,
        pc_sale_phase_cd CHAR,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
        pn_auth_approved_amt PSS.AUTH.AUTH_AMT_APPROVED%TYPE,
        pv_authority_response_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
        pv_authority_response_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
        pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
        pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
        pd_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
        pc_ignore_reprocess_flag CHAR,
        pv_tran_global_trans_cd OUT PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
        pv_auth_authority_tran_cd OUT PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
		pc_settle_update_needed OUT VARCHAR2,
		pv_source_system_cd OUT VARCHAR2,
        pc_auth_type_cd OUT VARCHAR2,
        pn_override_trans_type_id OUT PSS.PAYMENT_SUBTYPE.TRANS_TYPE_ID%TYPE,
        pc_partial_reversal_flag PSS.AUTH.PARTIAL_REVERSAL_FLAG%TYPE DEFAULT NULL,
        pc_sprout_enabled_flag CHAR DEFAULT 'N');
        
    FUNCTION GET_TERMINAL_GLOBAL_TOKEN(
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_payment_subtype_key_id NUMBER)
    RETURN VARCHAR2;

    PROCEDURE UPDATE_TERMINAL_GLOBAL_TOKEN(
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_payment_subtype_key_id NUMBER,
        pv_old_global_token PSS.TERMINAL.GLOBAL_TOKEN_CD%TYPE,
        pv_new_global_token PSS.TERMINAL.GLOBAL_TOKEN_CD%TYPE,
        pn_update_count OUT PLS_INTEGER,
        pc_current_must_match CHAR DEFAULT 'N');
        
    PROCEDURE CLEAR_TERMINAL_GLOBAL_TOKEN(
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_payment_subtype_key_id NUMBER,
        pv_global_token PSS.TERMINAL.GLOBAL_TOKEN_CD%TYPE,
        pn_update_count OUT PLS_INTEGER);
    
    FUNCTION ADD_ADMIN_CMD(
        pv_payment_subtype_class PSS.ADMIN_CMD.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_payment_subtype_key_id PSS.ADMIN_CMD.PAYMENT_SUBTYPE_KEY_ID%TYPE,
        pn_admin_cmd_type_id PSS.ADMIN_CMD.ADMIN_CMD_TYPE_ID%TYPE,
        pv_requested_by PSS.ADMIN_CMD.REQUESTED_BY%TYPE,
        pn_priority PSS.ADMIN_CMD.PRIORITY%TYPE)
    RETURN PSS.ADMIN_CMD.ADMIN_CMD_ID%TYPE;
    
    FUNCTION ADD_ADMIN_CMD_PARAM(
        pn_admin_cmd_id PSS.ADMIN_CMD_PARAM.ADMIN_CMD_ID%TYPE,
        pv_param_name PSS.ADMIN_CMD_PARAM.PARAM_NAME%TYPE,
        pv_param_value PSS.ADMIN_CMD_PARAM.PARAM_VALUE%TYPE)
    RETURN PSS.ADMIN_CMD_PARAM.ADMIN_CMD_PARAM_ID%TYPE;
    
    PROCEDURE LOCK_PARTIAL_SETTLE_POLLING(
        pv_process_id PSS.POSM_SETTING.POSM_SETTING_VALUE%TYPE,
        pv_success_flag OUT VARCHAR2,
        pv_locked_by_process_id OUT PSS.POSM_SETTING.POSM_SETTING_VALUE%TYPE);
        
    PROCEDURE UNLOCK_PARTIAL_SETTLE_POLLING(
        pv_process_id PSS.POSM_SETTING.POSM_SETTING_VALUE%TYPE);
    
    PROCEDURE UPDATE_BATCH_FEEDBACK_REF_CD(
        pv_settlement_batch_ref_cd PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_REF_CD%TYPE,
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pd_auth_authority_ts PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_END_TS%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pv_authority_resp_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pn_settlement_batch_id OUT PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_terminal_batch_id OUT PSS.SETTLEMENT_BATCH.TERMINAL_BATCH_ID%TYPE);
    
    PROCEDURE UPDATE_BATCH_FEEDBACK(
        pv_merchant_cd PSS.MERCHANT.MERCHANT_CD%TYPE,
        pv_terminal_cd PSS.TERMINAL.TERMINAL_CD%TYPE,
        pv_terminal_batch_num PSS.TERMINAL_BATCH.TERMINAL_BATCH_NUM%TYPE,
        pd_auth_authority_ts PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_END_TS%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pv_authority_resp_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pn_settlement_batch_id OUT PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_terminal_batch_id OUT PSS.SETTLEMENT_BATCH.TERMINAL_BATCH_ID%TYPE);
    
    -- R37 and above signature 
    PROCEDURE UPDATE_TRAN_FEEDBACK(
        pn_terminal_batch_id PSS.SETTLEMENT_BATCH.TERMINAL_BATCH_ID%TYPE,
        pn_settlement_batch_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_sale_auth_id PSS.AUTH.AUTH_ID%TYPE,
        pn_refund_id PSS.REFUND.REFUND_ID%TYPE,
        pc_tran_type_cd CHAR,
        pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
        pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pv_authority_response_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
        pv_authority_response_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
        pd_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
        pn_approved_amt PSS.AUTH.AUTH_AMT_APPROVED%TYPE,
        pn_sale_amt PSS.AUTH.AUTH_AMT%TYPE,
        pc_major_currency_used CHAR,
        pv_tran_global_trans_cd OUT PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
		pc_settle_update_needed OUT VARCHAR2,
		pc_auth_type_cd OUT VARCHAR2,
        pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
        pn_override_trans_type_id OUT PSS.PAYMENT_SUBTYPE.TRANS_TYPE_ID%TYPE);

    -- R36 and below    
    PROCEDURE UPDATE_TRAN_FEEDBACK(
        pn_terminal_batch_id PSS.SETTLEMENT_BATCH.TERMINAL_BATCH_ID%TYPE,
        pn_settlement_batch_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_sale_auth_id PSS.AUTH.AUTH_ID%TYPE,
        pn_refund_id PSS.REFUND.REFUND_ID%TYPE,
        pc_tran_type_cd CHAR,
        pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
        pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pv_authority_response_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
        pv_authority_response_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
        pd_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
        pn_approved_amt PSS.AUTH.AUTH_AMT_APPROVED%TYPE,
        pn_sale_amt PSS.AUTH.AUTH_AMT%TYPE,
        pc_major_currency_used CHAR,
        pv_tran_global_trans_cd OUT PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
		pc_settle_update_needed OUT VARCHAR2,
        pc_auth_type_cd OUT VARCHAR2,
        pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE);
        
END PKG_SETTLEMENT;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/PSS/PKG_SETTLEMENT.pbk?revision=1.94
CREATE OR REPLACE PACKAGE BODY PSS.PKG_SETTLEMENT AS
    FUNCTION AFTER_SETTLE_TRAN_STATE_CD(
        pc_auth_type_cd PSS.AUTH.AUTH_TYPE_CD%TYPE,
        pc_refund_type_cd PSS.REFUND.REFUND_TYPE_CD%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pc_device_result_type_cd PSS.TRAN.TRAN_DEVICE_RESULT_TYPE_CD%TYPE)
    RETURN PSS.TRAN.TRAN_STATE_CD%TYPE
    PARALLEL_ENABLE DETERMINISTIC
    IS       
    BEGIN
        IF pc_auth_result_cd = 'Y' THEN
            IF pc_auth_type_cd IS NOT NULL THEN
                IF pc_auth_type_cd IN('C', 'V', 'E', 'I') THEN
                    IF pc_device_result_type_cd IS NULL THEN
                        RETURN 'V'; -- processed reversal
                    ELSE
                        RETURN 'C'; -- Cancelled 
                    END IF;
                ELSE
                    RETURN 'D'; -- Complete
                END IF;                    
            ELSIF pc_refund_type_cd IS NOT NULL THEN
                IF pc_refund_type_cd IN('C') THEN
                    RETURN 'C'; -- Cancelled 
                ELSE
                    RETURN 'D'; -- Complete
                END IF;
            ELSE
                RAISE_APPLICATION_ERROR(-20560, 'Both auth type cd and refund type cd are null');
            END IF;
        ELSIF pc_auth_result_cd = 'P' THEN
            RETURN 'Q'; -- settlement processed
        ELSIF pc_auth_result_cd = 'N' THEN
            RETURN 'N'; -- PROCESSED_SERVER_SETTLEMENT_INCOMPLETE
        ELSIF pc_auth_result_cd = 'F' THEN
            RETURN 'R'; -- PROCESSED_SERVER_SETTLEMENT_ERROR
        ELSIF pc_auth_result_cd IN('O', 'R') THEN
            IF pc_auth_type_cd IS NOT NULL THEN
                IF pc_auth_type_cd IN('C', 'V', 'E', 'I') THEN
                    IF pc_device_result_type_cd IS NULL THEN
                        RETURN 'V'; -- processed reversal
                    ELSE
                        RETURN 'C'; -- Cancelled 
                    END IF;
                ELSE
                    RETURN 'E'; -- Complete - Error
                END IF;                    
            ELSIF pc_refund_type_cd IS NOT NULL THEN
                IF pc_refund_type_cd IN('C') THEN
                    RETURN 'C'; -- Cancelled 
                ELSE
                    RETURN 'E'; -- Complete - Error
                END IF;
            ELSE
                RAISE_APPLICATION_ERROR(-20560, 'Both auth type cd and refund type cd are null');
            END IF;
        END IF;
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    FUNCTION CREATE_TERMINAL_BATCH(
        pn_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE)
    RETURN PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE
    IS
        ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
    BEGIN
        SELECT PSS.SEQ_TERMINAL_BATCH_ID.NEXTVAL
          INTO ln_terminal_batch_id
          FROM DUAL;
        INSERT INTO PSS.TERMINAL_BATCH (
                TERMINAL_BATCH_ID,
                TERMINAL_ID,
                TERMINAL_BATCH_NUM,
                TERMINAL_BATCH_OPEN_TS,
                TERMINAL_BATCH_CYCLE_NUM,
                TERMINAL_CAPTURE_FLAG) 
         SELECT ln_terminal_batch_id, 
                pn_terminal_id, 
                T.TERMINAL_NEXT_BATCH_NUM,
                SYSDATE,
                T.TERMINAL_BATCH_CYCLE_NUM,
                A.TERMINAL_CAPTURE_FLAG
           FROM PSS.TERMINAL T
           JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
           JOIN AUTHORITY.AUTHORITY A ON M.AUTHORITY_ID = A.AUTHORITY_ID
          WHERE TERMINAL_ID = pn_terminal_id;
          
         UPDATE PSS.TERMINAL
            SET TERMINAL_NEXT_BATCH_NUM = CASE 
                    WHEN TERMINAL_NEXT_BATCH_NUM >= TERMINAL_MAX_BATCH_NUM THEN NVL(TERMINAL_MIN_BATCH_NUM, 1) /* reset batch num */ 
                    ELSE TERMINAL_NEXT_BATCH_NUM + 1 /* increment batch num */
                END,
                TERMINAL_BATCH_CYCLE_NUM = CASE 
                    WHEN TERMINAL_NEXT_BATCH_NUM >= TERMINAL_MAX_BATCH_NUM THEN TERMINAL_BATCH_CYCLE_NUM + 1 /* next cycle */ 
                    ELSE TERMINAL_BATCH_CYCLE_NUM /* same cycle */
                END
            WHERE TERMINAL_ID = pn_terminal_id;
        RETURN ln_terminal_batch_id;
    END;
            
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    FUNCTION GET_AVAILABLE_TERMINAL_BATCH(
        pn_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE,
        pn_allowed_trans OUT PLS_INTEGER,
        pb_create_if_needed BOOLEAN)
    RETURN PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE
    IS
        ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
        lc_is_closed CHAR(1);
        ln_max_tran PSS.TERMINAL.TERMINAL_BATCH_MAX_TRAN%TYPE; 
        ln_terminal_state_id PSS.TERMINAL.TERMINAL_STATE_ID%TYPE;
        lc_terminal_capture_flag PSS.TERMINAL_BATCH.TERMINAL_CAPTURE_FLAG%TYPE;
        ln_attempts PLS_INTEGER;
    BEGIN
        -- get last terminal batch record and verify that it is open       
        SELECT /*+ FIRST_ROWS */ MAX(TERMINAL_BATCH_ID), MAX(DECODE(TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y')), MAX(TERMINAL_BATCH_MAX_TRAN), MAX(TERMINAL_STATE_ID), MAX(NVL(TERMINAL_CAPTURE_FLAG, 'N'))
          INTO ln_terminal_batch_id, lc_is_closed, ln_max_tran, ln_terminal_state_id, lc_terminal_capture_flag
          FROM (SELECT TB.TERMINAL_BATCH_ID,
                       TB.TERMINAL_BATCH_NUM, 
                       TB.TERMINAL_BATCH_CYCLE_NUM, 
                       TB.TERMINAL_BATCH_CLOSE_TS,
                       COALESCE(T.TERMINAL_BATCH_MAX_TRAN, AU.AUTHORITY_BATCH_MAX_TRAN, 999) TERMINAL_BATCH_MAX_TRAN,
                       T.TERMINAL_STATE_ID,
                       TB.TERMINAL_CAPTURE_FLAG
                  FROM PSS.TERMINAL T
                  JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
                  JOIN AUTHORITY.AUTHORITY AU ON M.AUTHORITY_ID = AU.AUTHORITY_ID 
                  LEFT OUTER JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID                   
                 WHERE T.TERMINAL_ID = pn_terminal_id
                 ORDER BY TB.TERMINAL_BATCH_CYCLE_NUM DESC,
                          TB.TERMINAL_BATCH_NUM DESC)
         WHERE ROWNUM = 1;
        pn_allowed_trans := ln_max_tran;
        IF ln_terminal_state_id NOT IN(3) THEN
            RAISE_APPLICATION_ERROR(-20559, 'Terminal ' || pn_terminal_id || ' is not locked and a new terminal batch can not be created for it');
        ELSIF ln_terminal_batch_id IS NULL THEN
            IF pb_create_if_needed THEN
                ln_terminal_batch_id := CREATE_TERMINAL_BATCH(pn_terminal_id);
            END IF;
        ELSIF lc_is_closed  = 'Y' THEN
            IF pb_create_if_needed THEN
                ln_terminal_batch_id := CREATE_TERMINAL_BATCH(pn_terminal_id);
            END IF;
        ELSE
            -- do we need a new batch?
            SELECT ln_max_tran - COUNT(DISTINCT TRAN_ID)
              INTO pn_allowed_trans
              FROM (SELECT A.TRAN_ID
                      FROM PSS.AUTH A
                     WHERE A.TERMINAL_BATCH_ID = ln_terminal_batch_id
                    UNION ALL
                    SELECT R.TRAN_ID
                      FROM PSS.REFUND R
                     WHERE R.TERMINAL_BATCH_ID = ln_terminal_batch_id);
            IF lc_terminal_capture_flag = 'Y' THEN              
                IF pn_allowed_trans > 0 THEN
                    SELECT COUNT(*)
                      INTO ln_attempts
                      FROM PSS.SETTLEMENT_BATCH
                     WHERE TERMINAL_BATCH_ID = ln_terminal_batch_id;
                END IF;
                IF ln_attempts > 0 OR pn_allowed_trans <= 0 THEN
                    -- Create new terminal batch
                    IF pb_create_if_needed THEN
                        ln_terminal_batch_id := CREATE_TERMINAL_BATCH(pn_terminal_id);
                    END IF;
                    pn_allowed_trans := ln_max_tran;
                END IF;
            END IF;
        END IF;
        
        RETURN ln_terminal_batch_id;
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE GET_PENDING_SETTLEMENTS(
        pn_payment_subtype_key_id PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE, 
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pc_ignore_minimums_flag CHAR,
        pn_max_settlements PLS_INTEGER, 
        pn_pending_terminal_batch_ids OUT NUMBER_TABLE)
    IS
    BEGIN
         -- The following should never occur:
        /*
        -- Find retry settlements
        SELECT TERMINAL_BATCH_ID
          BULK COLLECT INTO pn_pending_terminal_batch_ids
          FROM (SELECT TB.TERMINAL_BATCH_ID
                  FROM PSS.SETTLEMENT_BATCH SB 
                  JOIN PSS.TERMINAL_BATCH TB ON TB.TERMINAL_BATCH_ID = SB.TERMINAL_BATCH_ID
                 WHERE TB.TERMINAL_ID = pn_payment_subtype_key_id 
                   AND SB.SETTLEMENT_BATCH_STATE_ID = 4 
                 ORDER BY TB.TERMINAL_BATCH_OPEN_TS ASC)
         WHERE ROWNUM <= pn_max_settlements;
     
        IF pn_pending_terminal_batch_ids.COUNT > 0 THEN
            RETURN;
        END IF;
        */
        -- Find new open batches
        SELECT TERMINAL_BATCH_ID
          BULK COLLECT INTO pn_pending_terminal_batch_ids
          FROM (
            SELECT C.TERMINAL_BATCH_ID
              FROM PSS.MERCHANT M
              JOIN AUTHORITY.AUTHORITY AU ON M.AUTHORITY_ID = AU.AUTHORITY_ID
              JOIN (SELECT TERMINAL_ID, TERMINAL_BATCH_ID, MERCHANT_ID, TERMINAL_BATCH_OPEN_TS, 
                           TERMINAL_BATCH_MIN_TRAN, TERMINAL_BATCH_MAX_TRAN, TERMINAL_MIN_BATCH_CLOSE_HR, TERMINAL_MAX_BATCH_CLOSE_HR, 
                           SUM(CASE WHEN TRAN_STATE_CD IN('T') THEN 1 ELSE 0 END) NUM_TRAN
                      FROM (SELECT T.TERMINAL_ID, 
                                   TB.TERMINAL_BATCH_ID,
                                   T.MERCHANT_ID,
                                   X.TRAN_ID,
                                   X.TRAN_STATE_CD,
                                   T.TERMINAL_BATCH_MIN_TRAN,
                                   T.TERMINAL_BATCH_MAX_TRAN,
                                   T.TERMINAL_MIN_BATCH_CLOSE_HR,
                                   T.TERMINAL_MAX_BATCH_CLOSE_HR,
                                   TB.TERMINAL_BATCH_OPEN_TS
                              FROM PSS.TERMINAL T
                              JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
                              JOIN PSS.AUTH A ON TB.TERMINAL_BATCH_ID = A.TERMINAL_BATCH_ID
                              JOIN PSS.TRAN X ON A.TRAN_ID = X.TRAN_ID 
                             WHERE TB.TERMINAL_BATCH_OPEN_TS IS NOT NULL
                               AND DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y') = 'N'
                               AND T.TERMINAL_ID = pn_payment_subtype_key_id
                             UNION 
                            SELECT T.TERMINAL_ID, 
                                   TB.TERMINAL_BATCH_ID,
                                   T.MERCHANT_ID,
                                   X.TRAN_ID,
                                   X.TRAN_STATE_CD,
                                   T.TERMINAL_BATCH_MIN_TRAN,
                                   T.TERMINAL_BATCH_MAX_TRAN,
                                   T.TERMINAL_MIN_BATCH_CLOSE_HR,
                                   T.TERMINAL_MAX_BATCH_CLOSE_HR,
                                   TB.TERMINAL_BATCH_OPEN_TS
                              FROM PSS.TERMINAL T
                              JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
                              JOIN PSS.REFUND R ON TB.TERMINAL_BATCH_ID = R.TERMINAL_BATCH_ID
                              JOIN PSS.TRAN X ON R.TRAN_ID = X.TRAN_ID 
                             WHERE TB.TERMINAL_BATCH_OPEN_TS IS NOT NULL
                               AND DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y') = 'N'
                               AND T.TERMINAL_ID = pn_payment_subtype_key_id) 
                     GROUP BY TERMINAL_ID, TERMINAL_BATCH_ID, MERCHANT_ID, TERMINAL_BATCH_OPEN_TS, TERMINAL_BATCH_MIN_TRAN, TERMINAL_BATCH_MAX_TRAN, TERMINAL_MIN_BATCH_CLOSE_HR, TERMINAL_MAX_BATCH_CLOSE_HR
                     HAVING COUNT(TRAN_STATE_CD) = SUM(CASE WHEN TRAN_STATE_CD IN('T', 'E') THEN 1 ELSE 0 END)) C  ON C.MERCHANT_ID = M.MERCHANT_ID 
             WHERE C.NUM_TRAN > 0
               AND (pc_ignore_minimums_flag = 'Y'
                OR C.NUM_TRAN >= COALESCE(C.TERMINAL_BATCH_MAX_TRAN, AU.AUTHORITY_BATCH_MAX_TRAN, 999) 
                OR SYSDATE >= C.TERMINAL_BATCH_OPEN_TS + COALESCE(C.TERMINAL_MAX_BATCH_CLOSE_HR/24, AU.AUTHORITY_MAX_BATCH_CLOSE_HR/24, 1)
                OR (SYSDATE >= C.TERMINAL_BATCH_OPEN_TS + COALESCE(C.TERMINAL_MIN_BATCH_CLOSE_HR/24, AU.AUTHORITY_MIN_BATCH_CLOSE_HR/24, 0)
                    AND C.NUM_TRAN >= COALESCE(C.TERMINAL_BATCH_MIN_TRAN, AU.AUTHORITY_BATCH_MIN_TRAN, 25)))
             ORDER BY c.TERMINAL_BATCH_OPEN_TS ASC)
         WHERE ROWNUM <= pn_max_settlements;
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE GET_RETRY_SETTLEMENTS(
        pn_payment_subtype_key_id PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE, 
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_settlement_retry_interval NUMBER,
        pn_max_settlements PLS_INTEGER, 
        pn_pending_terminal_batch_ids OUT NUMBER_TABLE)
    IS
    BEGIN
        -- Find retyable batches
        SELECT TERMINAL_BATCH_ID
          BULK COLLECT INTO pn_pending_terminal_batch_ids
          FROM (
            SELECT C.TERMINAL_BATCH_ID
              FROM (SELECT TERMINAL_ID, TERMINAL_BATCH_ID, TERMINAL_BATCH_OPEN_TS, 
                           SUM(CASE WHEN TRAN_STATE_CD IN('R', 'N') THEN 1 ELSE 0 END) NUM_TRAN
                      FROM (SELECT T.TERMINAL_ID, 
                                   TB.TERMINAL_BATCH_ID,
                                   X.TRAN_ID,
                                   X.TRAN_STATE_CD,
                                   TB.TERMINAL_BATCH_OPEN_TS
                              FROM PSS.TERMINAL T
                              JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
                              JOIN PSS.AUTH A ON TB.TERMINAL_BATCH_ID = A.TERMINAL_BATCH_ID
                              JOIN PSS.TRAN X ON A.TRAN_ID = X.TRAN_ID 
                             WHERE TB.TERMINAL_BATCH_OPEN_TS IS NOT NULL
                               AND DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y') = 'N'
                               AND T.TERMINAL_ID = pn_payment_subtype_key_id
                             UNION 
                            SELECT T.TERMINAL_ID, 
                                   TB.TERMINAL_BATCH_ID,
                                   X.TRAN_ID,
                                   X.TRAN_STATE_CD,
                                   TB.TERMINAL_BATCH_OPEN_TS
                              FROM PSS.TERMINAL T
                              JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
                              JOIN PSS.REFUND R ON TB.TERMINAL_BATCH_ID = R.TERMINAL_BATCH_ID
                              JOIN PSS.TRAN X ON R.TRAN_ID = X.TRAN_ID 
                             WHERE TB.TERMINAL_BATCH_OPEN_TS IS NOT NULL
                               AND DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y') = 'N'
                               AND T.TERMINAL_ID = pn_payment_subtype_key_id) 
                     GROUP BY TERMINAL_ID, TERMINAL_BATCH_ID, TERMINAL_BATCH_OPEN_TS
                     HAVING COUNT(TRAN_STATE_CD) = SUM(CASE WHEN TRAN_STATE_CD IN('R', 'N', 'E') THEN 1 ELSE 0 END)) C
             WHERE C.NUM_TRAN > 0
               AND SYSDATE >= pn_settlement_retry_interval + (
                        SELECT NVL(MAX(LA.SETTLEMENT_BATCH_START_TS), MIN_DATE)
                          FROM PSS.SETTLEMENT_BATCH LA
                          WHERE C.TERMINAL_BATCH_ID = LA.TERMINAL_BATCH_ID)
             ORDER BY c.TERMINAL_BATCH_OPEN_TS ASC)
         WHERE ROWNUM <= pn_max_settlements;
    END;

/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE FORCE_SETTLE_BATCH(
        pn_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE,
        pv_force_reason PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_RESP_DESC%TYPE)
    IS
        ln_settlement_batch_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE;
        ln_settlement_batch_state_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_STATE_ID%TYPE;
        ln_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE;
        ln_terminal_state_id PSS.TERMINAL.TERMINAL_STATE_ID%TYPE;
        ln_is_open PLS_INTEGER;
        lv_msg VARCHAR2(4000);
    BEGIN
        -- check terminal state
        SELECT T.TERMINAL_ID, T.TERMINAL_STATE_ID
          INTO ln_terminal_id, ln_terminal_state_id
          FROM PSS.TERMINAL T
          JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
         WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id;
        IF ln_terminal_state_id NOT IN(3,5) THEN
            SELECT 'Terminal ' || ln_terminal_id || ' is currently ' || DECODE(ln_terminal_state_id, 1, 'not locked', 2, 'disabled', 5, 'busy with retry', 'unavailable')
              INTO lv_msg
              FROM DUAL;
            RAISE_APPLICATION_ERROR(-20560, lv_msg);
        END IF;
        
        -- Check status of most recent settlement batch to ensure it is failue or decline
        SELECT MAX(SETTLEMENT_BATCH_ID), NVL(MAX(SETTLEMENT_BATCH_STATE_ID), 0)
          INTO ln_settlement_batch_id, ln_settlement_batch_state_id
          FROM (
            SELECT SETTLEMENT_BATCH_ID, SETTLEMENT_BATCH_STATE_ID
              FROM PSS.SETTLEMENT_BATCH
             WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id
             ORDER BY SETTLEMENT_BATCH_START_TS DESC, SETTLEMENT_BATCH_ID DESC)
         WHERE ROWNUM = 1;
        IF ln_settlement_batch_state_id NOT IN(2, 3) THEN
            SELECT 'Terminal Batch ' || pn_terminal_batch_id || DECODE(ln_settlement_batch_state_id, 0, ' has not yet been tried', 1, ' was successfully settled already', 4, ' is awaiting a retry', 7, 'was partially settled already', ' is not ready for forced settlement')
              INTO lv_msg
              FROM DUAL;
            RAISE_APPLICATION_ERROR(-20561, lv_msg);
        END IF;
        
        -- create new settlement batch
        SELECT PSS.SEQ_SETTLEMENT_BATCH_ID.NEXTVAL
          INTO ln_settlement_batch_id
          FROM DUAL;            
        
        INSERT INTO PSS.SETTLEMENT_BATCH(
            SETTLEMENT_BATCH_ID,
            SETTLEMENT_BATCH_STATE_ID,
            SETTLEMENT_BATCH_START_TS,
            TERMINAL_BATCH_ID,
            SETTLEMENT_BATCH_RESP_CD,
            SETTLEMENT_BATCH_RESP_DESC,
            SETTLEMENT_BATCH_REF_CD,
            SETTLEMENT_BATCH_END_TS
        ) VALUES (
            ln_settlement_batch_id,
            1,
            SYSDATE,
            pn_terminal_batch_id,
            0,
            pv_force_reason, 
            'FORCE_SETTLEMENT',
            SYSDATE);
        -- add all trans and update their tran state cd
        INSERT INTO PSS.TRAN_SETTLEMENT_BATCH(
            SETTLEMENT_BATCH_ID,
            AUTH_ID,
            TRAN_ID,
            TRAN_SETTLEMENT_B_AMT)
          SELECT DISTINCT 
                 ln_settlement_batch_id,
                 FIRST_VALUE(A.AUTH_ID) OVER (PARTITION BY T.TRAN_ID ORDER BY A.CREATED_TS DESC, A.AUTH_ID DESC) AUTH_ID, 
                 T.TRAN_ID, 
                 FIRST_VALUE(A.AUTH_AMT) OVER (PARTITION BY T.TRAN_ID ORDER BY A.CREATED_TS DESC, A.AUTH_ID DESC) AUTH_AMT
            FROM PSS.TRAN T
            JOIN PSS.AUTH A ON T.TRAN_ID = A.TRAN_ID
           WHERE A.TERMINAL_BATCH_ID = pn_terminal_batch_id
             AND A.AUTH_STATE_ID IN(2,6);
        INSERT INTO PSS.REFUND_SETTLEMENT_BATCH(
            SETTLEMENT_BATCH_ID,
            REFUND_ID,
            TRAN_ID,
            REFUND_SETTLEMENT_B_AMT)
          SELECT DISTINCT 
                 ln_settlement_batch_id,
                 FIRST_VALUE(R.REFUND_ID) OVER (PARTITION BY T.TRAN_ID ORDER BY R.CREATED_TS DESC, R.REFUND_ID DESC) AUTH_ID, 
                 T.TRAN_ID, 
                 FIRST_VALUE(R.REFUND_AMT) OVER (PARTITION BY T.TRAN_ID ORDER BY R.CREATED_TS DESC, R.REFUND_ID DESC) REFUND_AMT
            FROM PSS.TRAN T
            JOIN PSS.REFUND R ON T.TRAN_ID = R.TRAN_ID
           WHERE R.TERMINAL_BATCH_ID = pn_terminal_batch_id
             AND R.REFUND_STATE_ID IN(1);
        UPDATE PSS.TRAN 
           SET TRAN_STATE_CD = 'D'
         WHERE TRAN_STATE_CD IN('R', 'N', 'Q', 'T')
           AND TRAN_ID IN(
            SELECT A.TRAN_ID
              FROM PSS.AUTH A 
             WHERE A.TERMINAL_BATCH_ID = pn_terminal_batch_id
               AND A.AUTH_STATE_ID IN(2,6)
            UNION ALL
            SELECT R.TRAN_ID
              FROM PSS.REFUND R
             WHERE R.TERMINAL_BATCH_ID = pn_terminal_batch_id
               AND R.REFUND_STATE_ID IN(1));
                   
        -- update terminal batch
        UPDATE PSS.TERMINAL_BATCH
           SET TERMINAL_BATCH_CLOSE_TS = SYSDATE
         WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id;
    END;

/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE FORCE_TRAN(
        pn_payment_subtype_key_id PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE, 
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pv_force_reason PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_RESP_DESC%TYPE)
    IS
        lc_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
        ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
        ln_sale_auth_id PSS.AUTH.AUTH_ID%TYPE; 
        ln_force_auth_id PSS.AUTH.AUTH_ID%TYPE;
        ln_force_refund_id PSS.REFUND.REFUND_ID%TYPE;
        ln_allowed_trans PLS_INTEGER;
        ln_settlement_batch_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE;
        ln_auth_amt PSS.AUTH.AUTH_AMT%TYPE;
    BEGIN
        -- check terminal state
        SELECT T.TRAN_STATE_CD
          INTO lc_tran_state_cd
          FROM PSS.TRAN T
         WHERE T.TRAN_ID = pn_tran_id;
        IF lc_tran_state_cd NOT IN('I', 'J') THEN
            RAISE_APPLICATION_ERROR(-20562, 'Tran ' || pn_tran_id || ' is not in INCOMPLETE or INCOMPLETE_ERROR state and cannot be forced');
        END IF;
        
        UPDATE PSS.TRAN
           SET TRAN_STATE_CD = CASE WHEN pv_payment_subtype_class LIKE 'Authority::ISO8583%' THEN 'T' ELSE 'D' END
         WHERE TRAN_ID = pn_tran_id
           AND TRAN_STATE_CD = lc_tran_state_cd; -- make sure it didn't change
        
        IF SQL%ROWCOUNT < 1 THEN
            FORCE_TRAN(pn_payment_subtype_key_id, pv_payment_subtype_class, pn_tran_id, pv_force_reason);
        ELSE
            -- add to batch if necessary
            SELECT MAX(AUTH_ID), MAX(AUTH_AMT)
              INTO ln_sale_auth_id, ln_auth_amt
              FROM (
                SELECT AUTH_ID, AUTH_TS, AUTH_AMT
                  FROM PSS.AUTH
                 WHERE TRAN_ID = pn_tran_id
                   AND AUTH_TYPE_CD IN('U','S','O','A','D','C','E','V','I')
                 ORDER BY AUTH_TS DESC, AUTH_ID DESC)
             WHERE ROWNUM = 1;
            IF ln_sale_auth_id IS NULL THEN
                SELECT MAX(REFUND_ID), MAX(REFUND_AMT)
                  INTO ln_force_refund_id, ln_auth_amt
                  FROM (
                    SELECT CREATED_TS, REFUND_ID, REFUND_AMT
                      FROM PSS.REFUND
                     WHERE TRAN_ID = pn_tran_id
                     ORDER BY CREATED_TS DESC, REFUND_ID DESC)
                 WHERE ROWNUM = 1;
            END IF;             
            IF pv_payment_subtype_class LIKE 'Authority::ISO8583%' THEN
                IF ln_sale_auth_id IS NOT NULL THEN
                    SELECT MAX(TERMINAL_BATCH_ID)
                      INTO ln_terminal_batch_id
                      FROM PSS.AUTH
                     WHERE TRAN_ID = pn_tran_id;
                ELSIF ln_force_refund_id IS NOT NULL THEN
                    SELECT MAX(TERMINAL_BATCH_ID)
                      INTO ln_terminal_batch_id
                      FROM PSS.REFUND
                     WHERE TRAN_ID = pn_tran_id;
                END IF;
                IF ln_terminal_batch_id IS NULL THEN
                    ln_terminal_batch_id := GET_AVAILABLE_TERMINAL_BATCH(pn_payment_subtype_key_id, ln_allowed_trans, TRUE);
                    IF ln_allowed_trans <= 0 THEN
                        RAISE_APPLICATION_ERROR(-20556, 'Terminal Batch ' || ln_terminal_batch_id || ' is full for terminal ' || pn_payment_subtype_key_id || '; Can not add tran ' || pn_tran_id || ' to it');
                    END IF;
                END IF;
            ELSE
                -- create settlement batch record
                SELECT PSS.SEQ_SETTLEMENT_BATCH_ID.NEXTVAL
                  INTO ln_settlement_batch_id
                  FROM DUAL;
                INSERT INTO PSS.SETTLEMENT_BATCH(
                    SETTLEMENT_BATCH_ID,
                    SETTLEMENT_BATCH_STATE_ID,
                    SETTLEMENT_BATCH_START_TS,
                    SETTLEMENT_BATCH_END_TS,
                    SETTLEMENT_BATCH_RESP_CD,
                    SETTLEMENT_BATCH_RESP_DESC
                ) VALUES (
                    ln_settlement_batch_id,
                    1,
                    SYSDATE,
                    SYSDATE,
                    'MANUAL',
                    pv_force_reason);
            END IF;
            IF ln_sale_auth_id IS NOT NULL THEN  
                SELECT PSS.SEQ_AUTH_ID.NEXTVAL
                  INTO ln_force_auth_id
                  FROM DUAL;
                INSERT INTO PSS.AUTH (
                        AUTH_ID,
                        TRAN_ID,
                        AUTH_TYPE_CD,
                        AUTH_STATE_ID,
                        AUTH_PARSED_ACCT_DATA,
                        ACCT_ENTRY_METHOD_CD,
                        AUTH_AMT,
                        AUTH_TS,
                        TERMINAL_BATCH_ID,
                        TRACE_NUMBER,
                        AUTH_RESULT_CD,
                        AUTH_RESP_CD,
                        AUTH_RESP_DESC,
                        AUTH_AMT_APPROVED)
                 SELECT ln_force_auth_id,
                        pn_tran_id,
                        A.AUTH_TYPE_CD,
                        2,
                        a.AUTH_PARSED_ACCT_DATA,
                        a.ACCT_ENTRY_METHOD_CD,
                        a.AUTH_AMT,	    
                        SYSDATE,
                        ln_terminal_batch_id,
                        a.TRACE_NUMBER,
                        'Y',
                        'MANUAL',
                        pv_force_reason,
                        a.AUTH_AMT
                   FROM PSS.AUTH a
                  WHERE a.AUTH_ID = ln_sale_auth_id;
                IF ln_settlement_batch_id IS NOT NULL THEN
                    INSERT INTO PSS.TRAN_SETTLEMENT_BATCH(
                        SETTLEMENT_BATCH_ID,
                        AUTH_ID,
                        TRAN_ID,
                        TRAN_SETTLEMENT_B_AMT
                    ) VALUES (
                        ln_settlement_batch_id,
                        ln_force_auth_id,
                        pn_tran_id,
                        ln_auth_amt
                    );               
                END IF;
            ELSIF ln_force_refund_id IS NOT NULL THEN
                UPDATE PSS.REFUND
                   SET REFUND_STATE_ID = 1,
                       REFUND_RESP_CD = 'MANUAL',
                       REFUND_RESP_DESC = pv_force_reason,
                       REFUND_AUTHORITY_TS = SYSDATE,
                       TERMINAL_BATCH_ID = NVL(ln_terminal_batch_id, TERMINAL_BATCH_ID)
                 WHERE REFUND_ID = ln_force_refund_id;
                IF ln_settlement_batch_id IS NOT NULL THEN
                    INSERT INTO PSS.REFUND_SETTLEMENT_BATCH(
                        SETTLEMENT_BATCH_ID,
                        REFUND_ID,
                        TRAN_ID,
                        REFUND_SETTLEMENT_B_AMT
                    ) VALUES (
                        ln_settlement_batch_id,
                        ln_force_refund_id,
                        pn_tran_id,
                        ln_auth_amt
                    );               
                END IF;
            END IF;
        END IF;
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE ERROR_TRAN(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pv_force_reason PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_RESP_DESC%TYPE)
    IS
        lc_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
        lc_terminal_capture_flag PSS.TERMINAL_BATCH.TERMINAL_CAPTURE_FLAG%TYPE;
    BEGIN
        -- check terminal state
        SELECT TRAN_STATE_CD
          INTO lc_tran_state_cd
          FROM PSS.TRAN
         WHERE TRAN_ID = pn_tran_id;
        IF lc_tran_state_cd IN('T','N') THEN
            SELECT MIN(TB.TERMINAL_CAPTURE_FLAG)
              INTO lc_terminal_capture_flag
              FROM PSS.TERMINAL_BATCH TB
              JOIN (SELECT A.TERMINAL_BATCH_ID FROM PSS.AUTH A WHERE A.AUTH_TYPE_CD NOT IN('N', 'L') AND A.TRAN_ID = pn_tran_id
                    UNION ALL
                    SELECT R.TERMINAL_BATCH_ID FROM PSS.REFUND R WHERE R.TRAN_ID = pn_tran_id) X ON TB.TERMINAL_BATCH_ID = X.TERMINAL_BATCH_ID;
            IF lc_terminal_capture_flag != 'Y' THEN
                RAISE_APPLICATION_ERROR(-20562, 'Tran ' || pn_tran_id || ' is not in INCOMPLETE or INCOMPLETE_ERROR state and it is in a Host-Capture batch and cannot be errored');
            END IF;
        ELSIF lc_tran_state_cd NOT IN('I', 'J') THEN
            RAISE_APPLICATION_ERROR(-20562, 'Tran ' || pn_tran_id || ' is not in INCOMPLETE or INCOMPLETE_ERROR state and cannot be errored');
        END IF;
        
        UPDATE PSS.TRAN
           SET TRAN_STATE_CD = 'E'
         WHERE TRAN_ID = pn_tran_id
           AND TRAN_STATE_CD = lc_tran_state_cd; -- make sure it didn't change
           
        IF SQL%ROWCOUNT < 1 THEN
            ERROR_TRAN(pn_tran_id, pv_force_reason);
        END IF;
    END;
         
    PROCEDURE MARK_ADMIN_CMD_EXECUTED(
        pn_admin_cmd_id PSS.ADMIN_CMD.ADMIN_CMD_ID%TYPE)
    IS
    BEGIN
        UPDATE PSS.ADMIN_CMD
           SET ADMIN_CMD_STATE_ID = 2,
               CLOSED_UTC_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP)
         WHERE ADMIN_CMD_ID = pn_admin_cmd_id;
    END;

    PROCEDURE MARK_ADMIN_CMD_ERRORED(
        pn_admin_cmd_id PSS.ADMIN_CMD.ADMIN_CMD_ID%TYPE,
        pv_error_msg PSS.ADMIN_CMD.ERROR_MSG%TYPE)
    IS
    BEGIN
        UPDATE PSS.ADMIN_CMD
           SET ADMIN_CMD_STATE_ID = 4,
               CLOSED_UTC_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP),
               ERROR_MSG = pv_error_msg
         WHERE ADMIN_CMD_ID = pn_admin_cmd_id;
    END;
    
    PROCEDURE GET_ACTIONS_FROM_ADMIN_CMDS(
        pn_payment_subtype_key_id PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE, 
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_max_settlements PLS_INTEGER, 
        pn_max_trans PLS_INTEGER,
        pc_allow_add_to_batch CHAR,
        pc_retry_only CHAR,
        pc_settle_processing_enabled CHAR,
        pc_tran_processing_enabled CHAR,
        pn_pending_terminal_batch_ids OUT NUMBER_TABLE,
        pn_pending_tran_ids OUT NUMBER_TABLE)
    IS
        CURSOR l_cur IS
            SELECT ADMIN_CMD_ID, ADMIN_CMD_TYPE_ID
              FROM PSS.ADMIN_CMD
             WHERE PAYMENT_SUBTYPE_KEY_ID = pn_payment_subtype_key_id
               AND PAYMENT_SUBTYPE_CLASS =  pv_payment_subtype_class
               AND ADMIN_CMD_STATE_ID = 1
               AND (pc_settle_processing_enabled = 'Y' OR ADMIN_CMD_TYPE_ID NOT IN(1,2,3,6,8))
               AND (pc_tran_processing_enabled = 'Y' OR ADMIN_CMD_TYPE_ID NOT IN(4,5,7))
               AND (pc_retry_only != 'Y' OR ADMIN_CMD_TYPE_ID IN(2, 3, 4, 5, 6, 7))
               AND (pc_allow_add_to_batch = 'Y' OR pv_payment_subtype_class NOT LIKE 'Authority::ISO8583%' OR ADMIN_CMD_TYPE_ID NOT IN(5))
             ORDER BY PRIORITY ASC, CREATED_UTC_TS ASC;
    BEGIN
        -- get the first command and translate it into a terminal batch or tran to process
        FOR l_rec IN l_cur LOOP
            IF l_rec.ADMIN_CMD_TYPE_ID IN(1,2,3,6,5,8) AND pv_payment_subtype_class NOT LIKE 'Authority::ISO8583%' THEN
                MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Settlement commands are not allowed for Payment Subtype Class ''' || pv_payment_subtype_class || '''');
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 1 THEN -- Settle All
                GET_PENDING_SETTLEMENTS(
                    pn_payment_subtype_key_id, 
                    pv_payment_subtype_class, 
                    'Y', 
                    pn_max_settlements,
                    pn_pending_terminal_batch_ids);
                IF pn_pending_terminal_batch_ids IS NULL OR pn_pending_terminal_batch_ids.COUNT < pn_max_settlements THEN
                    -- Mark command executed
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                END IF;
                IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
                    RETURN; -- found something to process so exit
                END IF;
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 2 THEN -- Retry All
                GET_RETRY_SETTLEMENTS(
                    pn_payment_subtype_key_id, 
                    pv_payment_subtype_class,
                    0,
                    pn_max_settlements, 
                    pn_pending_terminal_batch_ids);
                IF pn_pending_terminal_batch_ids IS NULL OR pn_pending_terminal_batch_ids.COUNT < pn_max_settlements THEN
                    -- Mark command executed
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                END IF;
                IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
                    RETURN; -- found something to process so exit
                END IF;   
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 3 THEN -- Retry Batch
                SELECT TERMINAL_BATCH_ID
                  BULK COLLECT INTO pn_pending_terminal_batch_ids
                  FROM (
                    SELECT C.TERMINAL_BATCH_ID
                      FROM (SELECT TERMINAL_ID, TERMINAL_BATCH_ID, TERMINAL_BATCH_OPEN_TS, 
                                   SUM(CASE WHEN TRAN_STATE_CD IN('R','N') THEN 1 ELSE 0 END) NUM_TRAN
                              FROM (SELECT T.TERMINAL_ID, 
                                           TB.TERMINAL_BATCH_ID,
                                           X.TRAN_ID,
                                           X.TRAN_STATE_CD,
                                           TB.TERMINAL_BATCH_OPEN_TS
                                      FROM PSS.TERMINAL T
                                      JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
                                      JOIN PSS.AUTH A ON TB.TERMINAL_BATCH_ID = A.TERMINAL_BATCH_ID
                                      JOIN PSS.TRAN X ON A.TRAN_ID = X.TRAN_ID 
                                     WHERE TB.TERMINAL_BATCH_OPEN_TS IS NOT NULL
                                       AND DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y') = 'N'
                                       AND T.TERMINAL_ID = pn_payment_subtype_key_id
                                     UNION 
                                    SELECT T.TERMINAL_ID, 
                                           TB.TERMINAL_BATCH_ID,
                                           X.TRAN_ID,
                                           X.TRAN_STATE_CD,
                                           TB.TERMINAL_BATCH_OPEN_TS
                                      FROM PSS.TERMINAL T
                                      JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
                                      JOIN PSS.REFUND R ON TB.TERMINAL_BATCH_ID = R.TERMINAL_BATCH_ID
                                      JOIN PSS.TRAN X ON R.TRAN_ID = X.TRAN_ID 
                                     WHERE TB.TERMINAL_BATCH_OPEN_TS IS NOT NULL
                                       AND DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y') = 'N'
                                       AND T.TERMINAL_ID = pn_payment_subtype_key_id) 
                             GROUP BY TERMINAL_ID, TERMINAL_BATCH_ID, TERMINAL_BATCH_OPEN_TS
                             HAVING COUNT(TRAN_STATE_CD) = SUM(CASE WHEN TRAN_STATE_CD IN('R', 'N', 'E') THEN 1 ELSE 0 END)) C
                      JOIN PSS.ADMIN_CMD_PARAM ACP ON C.TERMINAL_BATCH_ID = TO_NUMBER_OR_NULL(ACP.PARAM_VALUE)
                     WHERE C.NUM_TRAN > 0
                       AND ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'TERMINAL_BATCH_ID'
                   ORDER BY c.TERMINAL_BATCH_OPEN_TS ASC)
                 WHERE ROWNUM <= pn_max_settlements;
                IF pn_pending_terminal_batch_ids IS NULL OR pn_pending_terminal_batch_ids.COUNT < pn_max_settlements THEN
                    -- Mark command executed
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                END IF;
                IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
                    RETURN; -- found something to process so exit
                END IF;   
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 4 THEN -- Retry Tran
                SELECT TRAN_ID
                  BULK COLLECT INTO pn_pending_tran_ids
                  FROM (SELECT T.TRAN_ID 
                          FROM PSS.TRAN T
                          LEFT OUTER JOIN PSS.AUTH A   ON T.TRAN_ID = A.TRAN_ID
                          LEFT OUTER JOIN PSS.REFUND R ON T.TRAN_ID = R.TRAN_ID
                          JOIN PSS.ADMIN_CMD_PARAM ACP ON T.TRAN_ID = TO_NUMBER_OR_NULL(ACP.PARAM_VALUE)
                         WHERE T.TRAN_STATE_CD IN('I', 'J') /* transaction_incomplete, transaction_incomplete_error */
                           AND T.PAYMENT_SUBTYPE_KEY_ID = pn_payment_subtype_key_id 
                           AND T.PAYMENT_SUBTYPE_CLASS  = pv_payment_subtype_class
                           AND ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                           AND ACP.PARAM_NAME = 'TRAN_ID'
                           AND (pc_allow_add_to_batch = 'Y' OR NVL(A.TERMINAL_BATCH_ID, R.TERMINAL_BATCH_ID) IS NOT NULL)
                         GROUP BY T.TRAN_ID
                         ORDER BY T.TRAN_ID)
                 WHERE ROWNUM <= pn_max_trans;
                IF pn_pending_tran_ids IS NULL OR pn_pending_tran_ids.COUNT < pn_max_trans THEN
                    -- Mark command executed
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                END IF;
                IF pn_pending_tran_ids IS NOT NULL AND pn_pending_tran_ids.COUNT > 0 THEN 
                    RETURN; -- found something to process so exit
                END IF;
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 5 THEN -- Force Tran
                 DECLARE
                    ln_tran_id PSS.TRAN.TRAN_ID%TYPE;
                    lv_force_reason PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_RESP_DESC%TYPE;
                BEGIN
                    SELECT TO_NUMBER_OR_NULL(ACP.PARAM_VALUE)
                      INTO ln_tran_id
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'TRAN_ID';
                    SELECT ACP.PARAM_VALUE
                      INTO lv_force_reason
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'FORCE_REASON';    
                    FORCE_TRAN(pn_payment_subtype_key_id, pv_payment_subtype_class, ln_tran_id, lv_force_reason);
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Parameter ''TRAN_ID'' not found');
                    WHEN WRONG_TRAN_STATE THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, SQLERRM);
                    WHEN OTHERS THEN
                        IF SQLCODE IN(1, 20556) THEN
                            MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, SQLERRM);
                        ELSE
                            RAISE;
                        END IF;
                END;
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 6 THEN -- Force Batch
                DECLARE
                    ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
                    lv_force_reason PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_RESP_DESC%TYPE;
                BEGIN
                    SELECT TO_NUMBER_OR_NULL(ACP.PARAM_VALUE)
                      INTO ln_terminal_batch_id
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'TERMINAL_BATCH_ID';
                    SELECT ACP.PARAM_VALUE
                      INTO lv_force_reason
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'FORCE_REASON';    
                    FORCE_SETTLE_BATCH(ln_terminal_batch_id, lv_force_reason);
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Parameter ''TERMINAL_BATCH_ID'' or ''FORCE_REASON'' not found');
                    WHEN WRONG_SETTLE_BATCH_STATE THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, SQLERRM);
                    WHEN OTHERS THEN
                        IF SQLCODE = 1 THEN
                            MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, SQLERRM);
                        ELSE
                            RAISE;
                        END IF;
                END;
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 7 THEN -- Error Tran
                 DECLARE
                    ln_tran_id PSS.TRAN.TRAN_ID%TYPE;
                    lv_error_reason PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_RESP_DESC%TYPE;
                BEGIN
                    SELECT TO_NUMBER_OR_NULL(ACP.PARAM_VALUE)
                      INTO ln_tran_id
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'TRAN_ID';
                    SELECT ACP.PARAM_VALUE
                      INTO lv_error_reason
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'ERROR_REASON';    
                    ERROR_TRAN(ln_tran_id, lv_error_reason);
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Parameter ''TRAN_ID'' not found');
                    WHEN WRONG_TRAN_STATE THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, SQLERRM);
                    WHEN OTHERS THEN
                        IF SQLCODE = 1 THEN
                            MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, SQLERRM);
                        ELSE
                            RAISE;
                        END IF;
                END;
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 8 THEN -- Settle Batch
                DECLARE
                    ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
                    lc_terminal_batch_closed CHAR;
                    ln_num_tran PLS_INTEGER;
                    ln_count_tran PLS_INTEGER;
                    ln_okay_tran PLS_INTEGER;          
                BEGIN
                    SELECT MAX(TO_NUMBER_OR_NULL(ACP.PARAM_VALUE))
                      INTO ln_terminal_batch_id
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'TERMINAL_BATCH_ID';
                    IF ln_terminal_batch_id IS NULL THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Parameter ''TERMINAL_BATCH_ID'' not found');
                    ELSE
                        SELECT DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y'), 
                           SUM(CASE WHEN X.TRAN_STATE_CD IN('T') THEN 1 ELSE 0 END) NUM_TRAN,
                           COUNT(X.TRAN_STATE_CD) COUNT_TRAN,
                           SUM(CASE WHEN X.TRAN_STATE_CD IN('T', 'E') THEN 1 ELSE 0 END) OKAY_TRAN
                          INTO lc_terminal_batch_closed,
                               ln_num_tran,
                               ln_count_tran,
                               ln_okay_tran
                          FROM PSS.TERMINAL_BATCH TB
                          JOIN (SELECT A.TRAN_ID, A.TERMINAL_BATCH_ID
                                  FROM PSS.AUTH A
                                UNION
                                SELECT R.TRAN_ID, R.TERMINAL_BATCH_ID
                                  FROM PSS.REFUND R) A ON TB.TERMINAL_BATCH_ID = A.TERMINAL_BATCH_ID
                          JOIN PSS.TRAN X ON A.TRAN_ID = X.TRAN_ID
                         WHERE TB.TERMINAL_BATCH_ID = ln_terminal_batch_id 
                         GROUP BY DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y');
                        IF lc_terminal_batch_closed != 'N' THEN
                            MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Terminal Batch '|| ln_terminal_batch_id ||' is already closed');
                        ELSIF ln_okay_tran != ln_count_tran THEN
                            NULL; -- Not ready for settlement yet
                        ELSIF ln_num_tran = 0 THEN
                            MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID); -- No non-error transaction, so mark as complete and continue
                        ELSE
                            SELECT ln_terminal_batch_id
                              BULK COLLECT INTO pn_pending_terminal_batch_ids
                              FROM DUAL;
                            -- Mark command executed                          
                            MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                            RETURN; -- found something to process so exit
                        END IF;
                    END IF;
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Terminal Batch ' ||ln_terminal_batch_id||' not found');
                END;
            ELSE
                -- Unknown cmd type
                NULL;
            END IF;
        END LOOP;
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE GET_PENDING_ACTIONS(
        pn_payment_subtype_key_id PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE, 
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_settlement_retry_interval NUMBER,
        pn_tran_retry_interval NUMBER,
        pn_max_settlements PLS_INTEGER, 
        pn_max_trans PLS_INTEGER,
        pn_terminal_state_id OUT PSS.TERMINAL.TERMINAL_STATE_ID%TYPE,
        pn_pending_terminal_batch_ids OUT NUMBER_TABLE,
        pn_pending_tran_ids OUT NUMBER_TABLE,
        pn_terminal_batch_id OUT PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE,
        pc_call_again_flag OUT VARCHAR2)
    IS
        ln_batch_max_trans PSS.TERMINAL.TERMINAL_BATCH_MAX_TRAN%TYPE;
        ln_allowed_trans PLS_INTEGER;
        lc_settle_processing_enabled CHAR(1);
        lc_tran_processing_enabled CHAR(1);
        lv_terminal_global_token_cd PSS.TERMINAL.GLOBAL_TOKEN_CD%TYPE;
        lc_terminal_capture_flag PSS.TERMINAL_BATCH.TERMINAL_CAPTURE_FLAG%TYPE;
        ln_cnt PLS_INTEGER;
    BEGIN
        SELECT NVL(MAX(POSM_SETTING_VALUE), 'N')
          INTO lc_tran_processing_enabled
          FROM PSS.POSM_SETTING
         WHERE POSM_SETTING_NAME = 'TRAN_PROCESSING_ENABLED';
        
        -- Get terminal state id
        IF pv_payment_subtype_class  LIKE 'Authority::ISO8583%' THEN -- this is same as in APP_LAYER.VW_PAYMENT_SUBTYPE_DETAIL
            SELECT NVL(MAX(POSM_SETTING_VALUE), 'N')
              INTO lc_settle_processing_enabled
              FROM PSS.POSM_SETTING
             WHERE POSM_SETTING_NAME = 'SETTLE_PROCESSING_ENABLED';
        
            SELECT T.TERMINAL_STATE_ID, COALESCE(T.TERMINAL_BATCH_MAX_TRAN, AU.AUTHORITY_BATCH_MAX_TRAN, 999), AU.TERMINAL_CAPTURE_FLAG
              INTO pn_terminal_state_id, ln_batch_max_trans, lc_terminal_capture_flag
              FROM PSS.TERMINAL T
              JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
              JOIN AUTHORITY.AUTHORITY AU ON M.AUTHORITY_ID = AU.AUTHORITY_ID                       
             WHERE T.TERMINAL_ID = pn_payment_subtype_key_id;
            IF pn_terminal_state_id = 5 THEN
                -- see if there are retry admin commands
                GET_ACTIONS_FROM_ADMIN_CMDS(
                    pn_payment_subtype_key_id, 
                    pv_payment_subtype_class, 
                    pn_max_settlements, 
                    0,
                    'N', -- pc_allow_add_to_batch
                    'Y', -- pc_retry_only
                    lc_settle_processing_enabled,
                    lc_tran_processing_enabled,
                    pn_pending_terminal_batch_ids,
                    pn_pending_tran_ids);
                IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
                    RETURN; -- Early Exit if we found something to process
                END IF;
                IF lc_settle_processing_enabled = 'Y' THEN
                    -- Only settlement retry is available
                    -- Find retyable batches
                    GET_RETRY_SETTLEMENTS(
                        pn_payment_subtype_key_id, 
                        pv_payment_subtype_class,
                        pn_settlement_retry_interval,
                        pn_max_settlements, 
                        pn_pending_terminal_batch_ids);
                    IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
                        RETURN; -- Early Exit if we found something to process
                    END IF;  
                    -- Make sure terminal should be in state 5 (that there is an unclosed batch)
                    SELECT COUNT(*)
                      INTO ln_cnt
                      FROM PSS.TERMINAL_BATCH TB
                      JOIN PSS.SETTLEMENT_BATCH SB ON TB.TERMINAL_BATCH_ID = SB.TERMINAL_BATCH_ID
                     WHERE TB.TERMINAL_BATCH_OPEN_TS IS NOT NULL
                       AND DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y') = 'N'
                       AND TB.TERMINAL_ID = pn_payment_subtype_key_id;
                    IF ln_cnt = 0 THEN
                        UPDATE PSS.TERMINAL 
                           SET TERMINAL_STATE_ID = 3
                         WHERE TERMINAL_ID = pn_payment_subtype_key_id;
                    END IF;
                END IF;
                RETURN; -- if terminal is in state 5, we don't process anything else
            ELSIF pn_terminal_state_id != 3 THEN
                RETURN; -- Terminal not locked for processing
            END IF;
        ELSE
            lv_terminal_global_token_cd := GET_TERMINAL_GLOBAL_TOKEN(pv_payment_subtype_class, pn_payment_subtype_key_id);
            IF lv_terminal_global_token_cd IS NULL THEN
                pn_terminal_state_id := 1;
                RETURN;  -- Terminal not locked for processing
            ELSE
                pn_terminal_state_id := 3;
                lc_settle_processing_enabled := 'N';
            END IF;
        END IF;
        
        IF lc_tran_processing_enabled != 'Y' AND lc_settle_processing_enabled != 'Y' THEN
            RETURN; -- we are not doing any processing right now
        END IF;
        
        -- limit num of trans if need be
        IF ln_batch_max_trans IS NOT NULL THEN
            pn_terminal_batch_id := GET_AVAILABLE_TERMINAL_BATCH(pn_payment_subtype_key_id, ln_batch_max_trans, TRUE);
            IF ln_batch_max_trans < pn_max_trans THEN
                ln_allowed_trans := ln_batch_max_trans;
            ELSE
                ln_allowed_trans := pn_max_trans;
            END IF;
        ELSE
            ln_allowed_trans := pn_max_trans;
        END IF;
        
        -- Check pending commands and process them first
        IF ln_allowed_trans <= 0 THEN            
            GET_ACTIONS_FROM_ADMIN_CMDS(
                pn_payment_subtype_key_id, 
                pv_payment_subtype_class, 
                pn_max_settlements, 
                pn_max_trans,
                'N', -- pc_allow_add_to_batch
                'N', -- pc_retry_only
                lc_settle_processing_enabled,
                lc_tran_processing_enabled,
                pn_pending_terminal_batch_ids,
                pn_pending_tran_ids);
        ELSE
            GET_ACTIONS_FROM_ADMIN_CMDS(
                pn_payment_subtype_key_id, 
                pv_payment_subtype_class, 
                pn_max_settlements, 
                ln_allowed_trans,
                'Y', -- pc_allow_add_to_batch
                'N', -- pc_retry_only
                lc_settle_processing_enabled,
                lc_tran_processing_enabled,
                pn_pending_terminal_batch_ids,
                pn_pending_tran_ids);
        END IF;
        IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
            RETURN; -- Early Exit if we found something to process
        ELSIF pn_pending_tran_ids IS NOT NULL AND pn_pending_tran_ids.COUNT > 0 THEN
            RETURN; -- Early Exit if we found something to process
        END IF;
        
        -- Check for Settlements
        IF lc_settle_processing_enabled = 'Y' THEN
            GET_PENDING_SETTLEMENTS(
                pn_payment_subtype_key_id, 
                pv_payment_subtype_class, 
                'N', 
                pn_max_settlements,
                pn_pending_terminal_batch_ids);
            IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
                RETURN; -- Early Exit if we found something to process
            END IF;
            GET_RETRY_SETTLEMENTS(
                pn_payment_subtype_key_id, 
                pv_payment_subtype_class,
                pn_settlement_retry_interval,
                pn_max_settlements, 
                pn_pending_terminal_batch_ids);
            IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
                RETURN; -- Early Exit if we found something to process
            END IF;
        END IF;
        
        IF lc_tran_processing_enabled != 'Y' THEN
            RETURN; -- we are not doing any tran processing right now
        END IF;
        IF lc_terminal_capture_flag = 'Y' AND pn_terminal_batch_id IS NOT NULL THEN
            SELECT TRAN_ID
              BULK COLLECT INTO pn_pending_tran_ids
              FROM (SELECT /*+ index(T IDX_TRAN_STATE_CD) */ T.TRAN_ID, T.TRAN_UPLOAD_TS
                      FROM PSS.TRAN T
                     WHERE T.TRAN_STATE_CD IN ('8', 'A', 'W') /* processed_server_batch, processing_server_tran, processed_server_auth_pending_reversal */
                       AND T.PAYMENT_SUBTYPE_KEY_ID = pn_payment_subtype_key_id
                       AND T.PAYMENT_SUBTYPE_CLASS  = pv_payment_subtype_class
                     ORDER BY T.TRAN_UPLOAD_TS ASC, T.TRAN_ID ASC)
             WHERE ROWNUM <= ln_allowed_trans;
            IF pn_pending_tran_ids IS NULL OR pn_pending_tran_ids.COUNT = 0 THEN
                RETURN; -- early exit
            END IF;
        
            -- prepare transactions
            -- NOTE: update tran first so as to lock it
            UPDATE PSS.TRAN X
			   SET TRAN_STATE_CD = CASE 
                    WHEN X.AUTH_HOLD_USED = 'Y' THEN 'T' 
                    WHEN 0 != (
                        SELECT NVL(SUM(TLI.TRAN_LINE_ITEM_AMOUNT * TLI.TRAN_LINE_ITEM_QUANTITY), 0) 
                          FROM PSS.TRAN_LINE_ITEM TLI 
                         WHERE TLI.TRAN_ID = X.TRAN_ID 
                           AND TLI.TRAN_LINE_ITEM_BATCH_TYPE_CD = 'A'
                    ) THEN 'T' 
                    ELSE 'C' END
			 WHERE X.TRAN_ID IN(SELECT COLUMN_VALUE FROM TABLE(pn_pending_tran_ids)); -- NOTE: using MEMBER OF causes a FTS			
            INSERT INTO PSS.AUTH (
                        AUTH_ID,
                        TRAN_ID,
                        AUTH_TYPE_CD,
                        AUTH_STATE_ID,
                        AUTH_PARSED_ACCT_DATA,
                        ACCT_ENTRY_METHOD_CD,
                        AUTH_AMT,
                        AUTH_TS,
                        TERMINAL_BATCH_ID,
                        TRACE_NUMBER)
                 SELECT PSS.SEQ_AUTH_ID.NEXTVAL,
                        TRAN_ID,
                        CASE 
                            WHEN 0 != SALE_AMOUNT THEN 'U' 
                            ELSE 'C' END,
                        6,
                        AUTH_PARSED_ACCT_DATA,
                        ACCT_ENTRY_METHOD_CD,
                        SALE_AMOUNT,	    
                        SYSDATE,
                        pn_terminal_batch_id,
                        TRACE_NUMBER
                   FROM (SELECT /*+INDEX(X PK_TRAN)*/
                        A.TRAN_ID,
                        NVL(S.SALE_AMOUNT, 0) SALE_AMOUNT,
                        MAX(a.AUTH_PARSED_ACCT_DATA) AUTH_PARSED_ACCT_DATA,
                        MAX(a.ACCT_ENTRY_METHOD_CD) ACCT_ENTRY_METHOD_CD,
                        MAX(a.TRACE_NUMBER) TRACE_NUMBER
                   FROM PSS.AUTH a
                   JOIN PSS.TRAN X ON A.TRAN_ID = X.TRAN_ID
                   LEFT OUTER JOIN PSS.SALE S ON S.TRAN_ID = A.TRAN_ID AND S.SALE_TYPE_CD = 'A'
                  WHERE X.TRAN_STATE_CD = 'T'
                    AND A.AUTH_TYPE_CD IN('L', 'N') AND (A.AUTH_RESULT_CD IN('Y', 'P', 'V', 'C', 'M') OR (A.AUTH_RESULT_CD IN('N', 'F') AND A.AUTH_HOLD_USED = 'Y' AND NVL(S.SALE_AMOUNT, 0) = 0))
                    AND A.TRAN_ID IN(SELECT COLUMN_VALUE FROM TABLE(pn_pending_tran_ids))  -- NOTE: using MEMBER OF causes a FTS
                    GROUP BY A.TRAN_ID, NVL(S.SALE_AMOUNT, 0));
            UPDATE PSS.REFUND
               SET TERMINAL_BATCH_ID = pn_terminal_batch_id,
                   REFUND_STATE_ID = 6
             WHERE TRAN_ID IN(SELECT COLUMN_VALUE FROM TABLE(pn_pending_tran_ids)); 
            IF pn_pending_tran_ids.COUNT >= ln_batch_max_trans THEN
                pn_pending_terminal_batch_ids := NUMBER_TABLE(pn_terminal_batch_id);
            ELSIF pn_pending_tran_ids.COUNT >= pn_max_trans THEN
                pc_call_again_flag := 'Y';
            END IF;
            pn_pending_tran_ids := NULL;
        ELSE
        -- Find retry trans
        IF ln_allowed_trans <= 0 THEN
             -- We can only process trans already in a batch
             SELECT DISTINCT TRAN_ID
              BULK COLLECT INTO pn_pending_tran_ids
              FROM (SELECT /*+ USE_NL(T,A,R) index(T IDX_TRAN_STATE_CD) index(A IDX_AUTH_TRAN_ID) */ T.TRAN_ID, T.TRAN_UPLOAD_TS
                      FROM PSS.TRAN T
                      LEFT OUTER JOIN PSS.AUTH A   ON T.TRAN_ID = A.TRAN_ID
                      LEFT OUTER JOIN PSS.REFUND R ON T.TRAN_ID = R.TRAN_ID
                     WHERE T.TRAN_STATE_CD = 'I' /* transaction_incomplete */
                       AND T.PAYMENT_SUBTYPE_KEY_ID = pn_payment_subtype_key_id 
                       AND T.PAYMENT_SUBTYPE_CLASS  = pv_payment_subtype_class
                       AND NVL(A.TERMINAL_BATCH_ID, R.TERMINAL_BATCH_ID) IS NOT NULL
                     GROUP BY T.TRAN_ID, T.TRAN_UPLOAD_TS
                    HAVING SYSDATE >= pn_tran_retry_interval + GREATEST(NVL(MAX(A.AUTH_TS), MIN_DATE), NVL(MAX(R.LAST_UPDATED_TS), MIN_DATE))
                     ORDER BY T.TRAN_UPLOAD_TS ASC, T.TRAN_ID ASC)
             WHERE ROWNUM <= pn_max_trans;
                pn_terminal_batch_id := NULL;
            RETURN; -- we can't process trans right now
        END IF;
        
        SELECT DISTINCT TRAN_ID
          BULK COLLECT INTO pn_pending_tran_ids
          FROM (SELECT /*+ USE_NL(T,A,R) index(T IDX_TRAN_STATE_CD) index(A IDX_AUTH_TRAN_ID) */ T.TRAN_ID, T.TRAN_UPLOAD_TS
                  FROM PSS.TRAN T
                  LEFT OUTER JOIN PSS.AUTH A   ON T.TRAN_ID = A.TRAN_ID
                  LEFT OUTER JOIN PSS.REFUND R ON T.TRAN_ID = R.TRAN_ID
                 WHERE T.TRAN_STATE_CD = 'I' /* transaction_incomplete */
                   AND T.PAYMENT_SUBTYPE_KEY_ID = pn_payment_subtype_key_id 
                   AND T.PAYMENT_SUBTYPE_CLASS  = pv_payment_subtype_class
                 GROUP BY T.TRAN_ID, T.TRAN_UPLOAD_TS
                HAVING SYSDATE >= pn_tran_retry_interval + GREATEST(NVL(MAX(A.AUTH_TS), MIN_DATE), NVL(MAX(R.LAST_UPDATED_TS), MIN_DATE))
                 ORDER BY T.TRAN_UPLOAD_TS ASC, T.TRAN_ID ASC)
         WHERE ROWNUM <= ln_allowed_trans;
        IF pn_pending_tran_ids.COUNT > 0 THEN
            RETURN; -- Early Exit if we found something to process
        END IF;
          
        -- get new pending trans
        SELECT TRAN_ID
          BULK COLLECT INTO pn_pending_tran_ids
          FROM (SELECT /*+ index(T IDX_TRAN_STATE_CD) */ T.TRAN_ID, T.TRAN_UPLOAD_TS
                  FROM PSS.TRAN T
                 WHERE T.TRAN_STATE_CD IN ('8', '9', 'W') /* processed_server_batch, processed_server_batch_intended, processed_server_auth_pending_reversal */
                   AND (T.TRAN_STATE_CD != '9' OR T.AUTH_HOLD_USED != 'Y') /* Filters out the Intended Transactions which have an Auth Hold */
                   AND T.PAYMENT_SUBTYPE_KEY_ID = pn_payment_subtype_key_id
                   AND T.PAYMENT_SUBTYPE_CLASS  = pv_payment_subtype_class
                 ORDER BY T.TRAN_UPLOAD_TS ASC, T.TRAN_ID ASC)
         WHERE ROWNUM <= ln_allowed_trans;        
        END IF;
    END;
    
    PROCEDURE SET_UNIQUE_SECONDS(
        pn_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE,
        pn_authority_id PSS.TERMINAL_BATCH.AUTHORITY_ID%TYPE,
        pn_unique_seconds IN OUT PSS.TERMINAL_BATCH.UNIQUE_SECONDS%TYPE)
    IS
    BEGIN
        UPDATE PSS.TERMINAL_BATCH
           SET UNIQUE_SECONDS = pn_unique_seconds,
               AUTHORITY_ID = pn_authority_id
         WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id;
    EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
            pn_unique_seconds := pn_unique_seconds + 1;
            SET_UNIQUE_SECONDS(pn_terminal_batch_id, pn_authority_id, pn_unique_seconds);
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE GET_OR_CREATE_SETTLEMENT_BATCH(
        pn_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE,
        pn_settlement_batch_id OUT PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_prior_attempts OUT PLS_INTEGER,
        pc_upload_needed_flag OUT VARCHAR2)
    IS
        lc_terminal_capture_flag PSS.TERMINAL_BATCH.TERMINAL_CAPTURE_FLAG%TYPE;
        ln_terminal_id PSS.TERMINAL_BATCH.TERMINAL_ID%TYPE;
        ln_unique_seconds PSS.TERMINAL_BATCH.UNIQUE_SECONDS%TYPE;
        ln_authority_id PSS.TERMINAL_BATCH.AUTHORITY_ID%TYPE;
    BEGIN
        -- check for an incomplete settlement_batch
        SELECT MAX(SB.SETTLEMENT_BATCH_ID), TB.TERMINAL_ID, TB.TERMINAL_CAPTURE_FLAG, TB.UNIQUE_SECONDS
          INTO pn_settlement_batch_id, ln_terminal_id, lc_terminal_capture_flag, ln_unique_seconds
          FROM PSS.TERMINAL_BATCH TB
          LEFT OUTER JOIN PSS.SETTLEMENT_BATCH SB ON TB.TERMINAL_BATCH_ID = SB.TERMINAL_BATCH_ID AND SB.SETTLEMENT_BATCH_STATE_ID = 4
         WHERE TB.TERMINAL_BATCH_ID = pn_terminal_batch_id
         GROUP BY TB.TERMINAL_ID, TB.TERMINAL_CAPTURE_FLAG, TB.UNIQUE_SECONDS;
        IF pn_settlement_batch_id IS NULL THEN
            SELECT PSS.SEQ_SETTLEMENT_BATCH_ID.NEXTVAL
              INTO pn_settlement_batch_id
              FROM DUAL;            
            
            INSERT INTO PSS.SETTLEMENT_BATCH(
                SETTLEMENT_BATCH_ID,
                SETTLEMENT_BATCH_STATE_ID,
                SETTLEMENT_BATCH_START_TS,
                TERMINAL_BATCH_ID
            ) VALUES (
                pn_settlement_batch_id,
                4,
                SYSDATE,
                pn_terminal_batch_id);
            /* Do this after transaction is uploaded (SALEd) with the Authority)    
            -- add all trans and update their tran state cd
            INSERT INTO PSS.TRAN_SETTLEMENT_BATCH(
                SETTLEMENT_BATCH_ID,
                AUTH_ID,
                TRAN_ID,
                TRAN_SETTLEMENT_B_AMT)
              SELECT DISTINCT 
                     pn_settlement_batch_id,
                     FIRST_VALUE(A.AUTH_ID) OVER (PARTITION BY T.TRAN_ID ORDER BY A.CREATED_TS DESC, A.AUTH_ID DESC) AUTH_ID, 
                     T.TRAN_ID, 
                     FIRST_VALUE(A.AUTH_AMT) OVER (PARTITION BY T.TRAN_ID ORDER BY A.CREATED_TS DESC, A.AUTH_ID DESC) AUTH_AMT
                FROM PSS.TRAN T
                JOIN PSS.AUTH A ON T.TRAN_ID = A.TRAN_ID
               WHERE A.TERMINAL_BATCH_ID = pn_terminal_batch_id
                 AND (A.AUTH_STATE_ID = 2
                      OR (A.AUTH_STATE_ID = 4 AND A.AUTH_TYPE_CD = 'N' AND A.AUTH_HOLD_USED = 'Y'));
            INSERT INTO PSS.REFUND_SETTLEMENT_BATCH(
                SETTLEMENT_BATCH_ID,
                REFUND_ID,
                TRAN_ID,
                REFUND_SETTLEMENT_B_AMT)
              SELECT DISTINCT 
                     pn_settlement_batch_id,
                     FIRST_VALUE(R.REFUND_ID) OVER (PARTITION BY T.TRAN_ID ORDER BY R.CREATED_TS DESC, R.REFUND_ID DESC) AUTH_ID, 
                     T.TRAN_ID, 
                     FIRST_VALUE(R.REFUND_AMT) OVER (PARTITION BY T.TRAN_ID ORDER BY R.CREATED_TS DESC, R.REFUND_ID DESC) REFUND_AMT
                FROM PSS.TRAN T
                JOIN PSS.REFUND R ON T.TRAN_ID = R.TRAN_ID
               WHERE R.TERMINAL_BATCH_ID = pn_terminal_batch_id
                 AND R.REFUND_STATE_ID IN(1);
            */
        END IF;
        UPDATE PSS.TRAN 
           SET TRAN_STATE_CD = 'S'
         WHERE TRAN_STATE_CD IN('R', 'N', 'T')
           AND TRAN_ID IN(
            SELECT A.TRAN_ID
              FROM PSS.AUTH A 
             WHERE A.TERMINAL_BATCH_ID = pn_terminal_batch_id
               AND (A.AUTH_STATE_ID IN(2, 5, 6)
                  OR (A.AUTH_STATE_ID IN(3, 4) AND A.AUTH_TYPE_CD = 'N' AND A.AUTH_HOLD_USED = 'Y'))
            UNION ALL
            SELECT R.TRAN_ID
              FROM PSS.REFUND R
             WHERE R.TERMINAL_BATCH_ID = pn_terminal_batch_id
               AND R.REFUND_STATE_ID IN(1, 6));
        IF lc_terminal_capture_flag != 'Y' THEN
            UPDATE PSS.TERMINAL
               SET TERMINAL_STATE_ID = 5
             WHERE TERMINAL_ID = ln_terminal_id;
        END IF;
        SELECT COUNT(*)
          INTO pn_prior_attempts
    	  FROM PSS.SETTLEMENT_BATCH  
    	 WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id 
    	   AND SETTLEMENT_BATCH_STATE_ID IN(2, 3);
        IF pn_prior_attempts > 0 THEN
            SELECT DECODE(SETTLEMENT_BATCH_STATE_ID, 2, 'N', 3, 'Y')
              INTO pc_upload_needed_flag
              FROM (
                SELECT SETTLEMENT_BATCH_END_TS, SETTLEMENT_BATCH_ID, SETTLEMENT_BATCH_STATE_ID
                  FROM PSS.SETTLEMENT_BATCH  
                 WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id 
                   AND SETTLEMENT_BATCH_STATE_ID IN(2, 3)           
                 ORDER BY SETTLEMENT_BATCH_END_TS DESC, SETTLEMENT_BATCH_ID DESC)
             WHERE ROWNUM = 1;
        ELSE
            pc_upload_needed_flag := 'N';
        END IF;
        IF lc_terminal_capture_flag = 'Y' AND ln_unique_seconds IS NULL THEN
            SELECT ROUND(DATE_TO_MILLIS(SYSDATE) / 1000), M.AUTHORITY_ID
              INTO ln_unique_seconds, ln_authority_id
              FROM PSS.TERMINAL T
              JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
             WHERE T.TERMINAL_ID = ln_terminal_id;
            SET_UNIQUE_SECONDS(pn_terminal_batch_id, ln_authority_id, ln_unique_seconds);
        END IF;
    END;
    /*
    FUNCTION IS_LAST_TERMINAL_BATCH_OPEN(
        pn_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE)
    RETURN PLS_INTEGER
    IS
        ln_is_open PLS_INTEGER;
    BEGIN
        SELECT DECODE(TERMINAL_BATCH_CLOSE_TS, NULL, 1, 0)
          INTO ln_is_open
          FROM (SELECT TERMINAL_BATCH_NUM, 
                       TERMINAL_BATCH_CYCLE_NUM, 
                       TERMINAL_BATCH_CLOSE_TS
                  FROM PSS.TERMINAL_BATCH
                 WHERE TERMINAL_ID = pn_terminal_id
                 ORDER BY TERMINAL_BATCH_CYCLE_NUM DESC,
                          TERMINAL_BATCH_NUM DESC)
         WHERE ROWNUM = 1;
        RETURN ln_is_open;
    END;
    */
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE UPDATE_PROCESSED_BATCH(
        pn_settlement_batch_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pv_authority_response_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
        pv_authority_response_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
        pd_batch_closed_ts PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_END_TS%TYPE)
    IS
        ln_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE;
        ln_settlement_batch_state_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_STATE_ID%TYPE;
        ln_terminal_capture_flag PSS.TERMINAL_BATCH.TERMINAL_CAPTURE_FLAG%TYPE;
    BEGIN
        SELECT SB.SETTLEMENT_BATCH_STATE_ID, TB.TERMINAL_CAPTURE_FLAG
          INTO ln_settlement_batch_state_id, ln_terminal_capture_flag
          FROM PSS.SETTLEMENT_BATCH SB
          JOIN PSS.TERMINAL_BATCH TB ON TB.TERMINAL_BATCH_ID = SB.TERMINAL_BATCH_ID
         WHERE SB.SETTLEMENT_BATCH_ID = pn_settlement_batch_id;
        IF ln_settlement_batch_state_id = 1 OR (ln_settlement_batch_state_id IN(1,2,3,7) AND pc_auth_result_cd IN('P')) THEN
            RAISE_APPLICATION_ERROR(-20100, 'Settlement batch ' || pn_settlement_batch_id || ' was already processed');
        END IF;
        -- update the settlement batch
        UPDATE PSS.SETTLEMENT_BATCH
           SET SETTLEMENT_BATCH_STATE_ID = DECODE(pc_auth_result_cd, 'Y', 1, 'P', 7, 'N', 2, 'F', 3, 'O', 2, 'R', 2),
               SETTLEMENT_BATCH_RESP_CD = pv_authority_response_cd,
               SETTLEMENT_BATCH_RESP_DESC = pv_authority_response_desc,
               SETTLEMENT_BATCH_REF_CD = pv_authority_ref_cd,
               SETTLEMENT_BATCH_END_TS = pd_batch_closed_ts
         WHERE SETTLEMENT_BATCH_ID = pn_settlement_batch_id;
            
        -- add tran settlement record for each tran in batch
        INSERT INTO PSS.TRAN_SETTLEMENT_BATCH(
               SETTLEMENT_BATCH_ID,
               AUTH_ID,
               TRAN_ID,
               TRAN_SETTLEMENT_B_AMT,
               TRAN_SETTLEMENT_B_RESP_CD,
               TRAN_SETTLEMENT_B_RESP_DESC)
        SELECT pn_settlement_batch_id,
               SA.AUTH_ID,
               X.TRAN_ID,
               DECODE(pc_auth_result_cd, 'Y', SA.AUTH_AMT_APPROVED),
               pv_authority_response_cd,
               pv_authority_response_desc
          FROM PSS.AUTH SA 
          JOIN PSS.TRAN X ON SA.TRAN_ID = X.TRAN_ID
         WHERE SA.TERMINAL_BATCH_ID = pn_terminal_batch_id
           AND SA.AUTH_TYPE_CD IN('U','S','O','A','D','C','E','V','I', 'N')
           AND ((SA.AUTH_STATE_ID IN(2, 5) AND SA.AUTH_TYPE_CD != 'N')
                OR (SA.AUTH_STATE_ID IN(6) AND ln_terminal_capture_flag = 'Y' AND SA.AUTH_TYPE_CD != 'N')
                OR (SA.AUTH_STATE_ID IN(3,4) AND SA.AUTH_TYPE_CD = 'N' AND SA.AUTH_HOLD_USED = 'Y'))
           AND X.TRAN_STATE_CD IN('S');
        -- update the tran_state_cd of all sales in the batch
        UPDATE PSS.TRAN X
           SET TRAN_STATE_CD = (SELECT AFTER_SETTLE_TRAN_STATE_CD(sa.AUTH_TYPE_CD, NULL, pc_auth_result_cd, X.TRAN_DEVICE_RESULT_TYPE_CD)
            FROM PSS.AUTH SA 
           WHERE SA.TRAN_ID = X.TRAN_ID 
		     AND SA.TERMINAL_BATCH_ID = pn_terminal_batch_id
             AND SA.AUTH_TYPE_CD IN('U','S','O','A','D','C','E','V','I', 'N')
             AND ((SA.AUTH_STATE_ID IN(2, 5) AND SA.AUTH_TYPE_CD != 'N')
                OR (SA.AUTH_STATE_ID IN(6) AND ln_terminal_capture_flag = 'Y' AND SA.AUTH_TYPE_CD != 'N')
                OR (SA.AUTH_STATE_ID IN(3,4) AND SA.AUTH_TYPE_CD = 'N' AND SA.AUTH_HOLD_USED = 'Y')))
         WHERE X.TRAN_ID IN(SELECT SA.TRAN_ID 
          FROM PSS.AUTH SA 
         WHERE SA.TERMINAL_BATCH_ID = pn_terminal_batch_id
           AND SA.AUTH_TYPE_CD IN('U','S','O','A','D','C','E','V','I', 'N')
           AND ((SA.AUTH_STATE_ID IN(2, 5) AND SA.AUTH_TYPE_CD != 'N')
                OR (SA.AUTH_STATE_ID IN(6) AND ln_terminal_capture_flag = 'Y' AND SA.AUTH_TYPE_CD != 'N')
                OR (SA.AUTH_STATE_ID IN(3,4) AND SA.AUTH_TYPE_CD = 'N' AND SA.AUTH_HOLD_USED = 'Y')))
           AND X.TRAN_STATE_CD IN('S');
       
        -- add tran settlement record for each tran in batch
        INSERT INTO PSS.REFUND_SETTLEMENT_BATCH(
               SETTLEMENT_BATCH_ID,
               REFUND_ID,
               TRAN_ID,
               REFUND_SETTLEMENT_B_AMT,
               REFUND_SETTLEMENT_B_RESP_CD,
               REFUND_SETTLEMENT_B_RESP_DESC)
        SELECT pn_settlement_batch_id,
               R.REFUND_ID,
               X.TRAN_ID,
               DECODE(pc_auth_result_cd, 'Y', R.REFUND_AMT),
               pv_authority_response_cd,
               pv_authority_response_desc
          FROM PSS.REFUND R  
          JOIN PSS.TRAN X ON R.TRAN_ID = X.TRAN_ID
         WHERE R.TERMINAL_BATCH_ID = pn_terminal_batch_id
           AND (R.REFUND_STATE_ID IN(1) OR (ln_terminal_capture_flag = 'Y' AND R.REFUND_STATE_ID IN(6)))
           AND X.TRAN_STATE_CD IN('S');
        -- Update refunds in batch
        UPDATE PSS.TRAN X
           SET TRAN_STATE_CD =           
           (SELECT AFTER_SETTLE_TRAN_STATE_CD(NULL, R.REFUND_TYPE_CD, pc_auth_result_cd, X.TRAN_DEVICE_RESULT_TYPE_CD)               
            FROM PSS.REFUND R
           WHERE R.TRAN_ID = X.TRAN_ID 
             AND (R.REFUND_STATE_ID IN(1) OR (ln_terminal_capture_flag = 'Y' AND R.REFUND_STATE_ID IN(6))))
         WHERE X.TRAN_ID IN(SELECT R.TRAN_ID 
          FROM PSS.REFUND R 
         WHERE R.TERMINAL_BATCH_ID = pn_terminal_batch_id
           AND (R.REFUND_STATE_ID IN(1) OR (ln_terminal_capture_flag = 'Y' AND R.REFUND_STATE_ID IN(6))))
           AND X.TRAN_STATE_CD IN('S');
        
        -- if settlement successful or permanently declined, close old terminal batch and create new terminal batch
        IF pn_terminal_batch_id IS NOT NULL AND pc_auth_result_cd IN('Y', 'P', 'O', 'R') THEN
            UPDATE PSS.TERMINAL_BATCH
               SET TERMINAL_BATCH_CLOSE_TS = pd_batch_closed_ts
             WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id
             RETURNING TERMINAL_ID INTO ln_terminal_id;
            UPDATE PSS.TERMINAL
               SET TERMINAL_STATE_ID = 3
             WHERE TERMINAL_ID = ln_terminal_id;
        END IF;
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE PREPARE_PENDING_TRAN(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pn_sale_auth_id OUT PSS.AUTH.AUTH_ID%TYPE,
        pn_refund_id OUT PSS.REFUND.REFUND_ID%TYPE,
        pc_sale_phase_cd OUT VARCHAR2,
        pn_prior_attempts OUT PLS_INTEGER,
        pn_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE)
    IS
        lv_tran_lock_cd VARCHAR2(128);
        lv_lock_handle VARCHAR2(128);
        ln_actual_amt PSS.AUTH.AUTH_AMT%TYPE;
        ln_intended_amt PSS.AUTH.AUTH_AMT%TYPE;
        lc_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
        lc_auth_type_cd PSS.AUTH.AUTH_TYPE_CD%TYPE;
        ln_previous_saled_amt PSS.AUTH.AUTH_AMT%TYPE;
        lc_auth_hold_used PSS.AUTH.AUTH_HOLD_USED%TYPE;
        ln_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE;
        lc_terminal_capture_flag PSS.TERMINAL_BATCH.TERMINAL_CAPTURE_FLAG%TYPE;
        ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
        ln_is_open PLS_INTEGER;   
        ln_auth_auth_id PSS.AUTH.AUTH_ID%TYPE;
        ln_allowed_trans PLS_INTEGER;
        lc_new_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
        ln_device_result_type_cd PSS.TRAN.TRAN_DEVICE_RESULT_TYPE_CD%TYPE;
        ln_apply_to_consumer_acct_id PSS.TRAN_LINE_ITEM.APPLY_TO_CONSUMER_ACCT_ID%TYPE;
        ln_consumer_acct_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_TYPE_ID%TYPE;
        lc_auth_auth_type_cd PSS.AUTH.AUTH_TYPE_CD%TYPE;
    BEGIN
        SELECT DEVICE_NAME || ':' || TRAN_DEVICE_TRAN_CD, 
               TRAN_STATE_CD,
               NVL(AUTH_HOLD_USED, 'N'),
               TERMINAL_ID,
               NVL(TERMINAL_CAPTURE_FLAG, 'N'),
               AUTH_AUTH_ID,
               AUTH_AUTH_TYPE_CD,
               SALE_AUTH_ID,
               REFUND_ID,
               TRAN_DEVICE_RESULT_TYPE_CD
          INTO lv_tran_lock_cd,
               lc_tran_state_cd,
               lc_auth_hold_used,
               ln_terminal_id,
               lc_terminal_capture_flag,
               ln_auth_auth_id,
               lc_auth_auth_type_cd,
               pn_sale_auth_id,
               pn_refund_id,
               ln_device_result_type_cd
          FROM (SELECT T.DEVICE_NAME, 
                       T.TRAN_DEVICE_TRAN_CD, 
                       A.AUTH_ID AUTH_AUTH_ID,
                       A.AUTH_TYPE_CD AUTH_AUTH_TYPE_CD,
                       SA.AUTH_ID SALE_AUTH_ID,
                       R.REFUND_ID, 
                       T.TRAN_STATE_CD,
                       T.AUTH_HOLD_USED,
                       TE.TERMINAL_ID,
                       AU.TERMINAL_CAPTURE_FLAG,
                       T.TRAN_DEVICE_RESULT_TYPE_CD
                  FROM PSS.TRAN T
				  LEFT OUTER JOIN PSS.AUTH RA  ON T.TRAN_ID = RA.TRAN_ID AND RA.AUTH_TYPE_CD IN ('C', 'E')
                  LEFT OUTER JOIN PSS.AUTH A   ON T.TRAN_ID = A.TRAN_ID AND A.AUTH_TYPE_CD IN('L', 'N') AND (A.AUTH_RESULT_CD IN('Y', 'P', 'V', 'C', 'M') OR T.TRAN_STATE_CD IN('W') OR RA.AUTH_ID IS NOT NULL OR T.PAYMENT_SUBTYPE_CLASS='Sprout')
                  LEFT OUTER JOIN PSS.REFUND R ON T.TRAN_ID = R.TRAN_ID
                  LEFT OUTER JOIN PSS.AUTH SA  ON T.TRAN_ID = SA.TRAN_ID AND SA.AUTH_TYPE_CD IN('U','S','O','A','D','C','E','V','I') AND SA.AUTH_STATE_ID = 6
                  LEFT OUTER JOIN (PSS.TERMINAL TE
                        JOIN PSS.MERCHANT M ON TE.MERCHANT_ID = M.MERCHANT_ID
                        JOIN AUTHORITY.AUTHORITY AU ON M.AUTHORITY_ID = AU.AUTHORITY_ID
                  ) ON T.PAYMENT_SUBTYPE_CLASS NOT IN ('Aramark', 'Authority::NOP', 'BlackBoard', 'Cash') AND T.PAYMENT_SUBTYPE_CLASS NOT LIKE 'Internal%' AND T.PAYMENT_SUBTYPE_KEY_ID = TE.TERMINAL_ID
                 WHERE T.TRAN_ID = pn_tran_id
                 ORDER BY A.AUTH_RESULT_CD DESC, A.AUTH_TS DESC, A.AUTH_ID DESC, SA.AUTH_RESULT_CD DESC, SA.AUTH_TS DESC, SA.AUTH_ID DESC, R.CREATED_TS DESC, R.REFUND_ID DESC, RA.AUTH_RESULT_CD DESC, RA.AUTH_TS DESC, RA.AUTH_ID DESC)
         WHERE ROWNUM = 1;
        IF NOT(lc_tran_state_cd IN('8', 'I', 'W')
            OR (lc_tran_state_cd IN('9') AND lc_auth_hold_used != 'Y' AND lc_terminal_capture_flag != 'Y')) THEN
            pc_sale_phase_cd := '-'; -- Do nothing
            pn_prior_attempts := -1;
            RETURN;
        END IF;
        -- Thus, "lc_tran_state_cd IN('8', 'I', 'W') OR (lc_tran_state_cd IN('9') AND lc_auth_hold_used != 'Y' AND lc_terminal_capture_flag != 'Y')"
		lc_new_tran_state_cd := lc_tran_state_cd;
        -- Lock transaction
        lv_lock_handle := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', lv_tran_lock_cd);
        IF ln_auth_auth_id IS NOT NULL THEN -- it's a sale or a reversal
            IF lc_terminal_capture_flag = 'Y' AND lc_tran_state_cd != '9' THEN
                -- get the open terminal batch
                IF pn_terminal_batch_id IS NULL THEN
                	ln_terminal_batch_id := GET_AVAILABLE_TERMINAL_BATCH(ln_terminal_id, ln_allowed_trans, TRUE);
                ELSE
                    ln_terminal_batch_id := pn_terminal_batch_id;
                END IF;
                SELECT NVL(SUM(DECODE(TLI.TRAN_LINE_ITEM_BATCH_TYPE_CD, 'A', TLI.TRAN_LINE_ITEM_AMOUNT * TLI.TRAN_LINE_ITEM_QUANTITY)), 0) ACTUAL_AMT
                  INTO ln_actual_amt
                  FROM PSS.TRAN_LINE_ITEM TLI
                 WHERE TLI.TRAN_ID = pn_tran_id;
                pc_sale_phase_cd := '-'; -- No further processing
                IF ln_actual_amt != 0 THEN
                    lc_auth_type_cd := 'U'; -- pre-auth sale
                    lc_new_tran_state_cd := 'T'; -- Processed Server Batch
                ELSIF lc_auth_hold_used = 'Y' THEN
                     lc_auth_type_cd := 'C'; -- Auth Reversal
                     lc_new_tran_state_cd := 'T'; -- Processed Server Batch
                ELSE
                    -- do not create sale auth record
                    lc_new_tran_state_cd := 'C'; -- client cancelled
                END IF;
            ELSIF lc_tran_state_cd = 'W' THEN
                pc_sale_phase_cd := 'R'; -- Reversal
                ln_actual_amt := 0;
                lc_auth_type_cd := 'C'; -- Auth reversal
                lc_new_tran_state_cd := 'A'; -- processing tran
            ELSE
                SELECT NVL(SUM(DECODE(TLI.TRAN_LINE_ITEM_BATCH_TYPE_CD, 'A', TLI.TRAN_LINE_ITEM_AMOUNT * TLI.TRAN_LINE_ITEM_QUANTITY)), 0) ACTUAL_AMT,
                       NVL(SUM(DECODE(TLI.TRAN_LINE_ITEM_BATCH_TYPE_CD, 'I', TLI.TRAN_LINE_ITEM_AMOUNT * TLI.TRAN_LINE_ITEM_QUANTITY)), 0) INTENDED_AMT,
                       MAX(TLI.APPLY_TO_CONSUMER_ACCT_ID),
                       T.TRAN_STATE_CD -- get this again now that transaction is locked
                  INTO ln_actual_amt, ln_intended_amt, ln_apply_to_consumer_acct_id,lc_tran_state_cd
                  FROM PSS.TRAN_LINE_ITEM TLI
                 RIGHT OUTER JOIN PSS.TRAN T ON T.TRAN_ID = TLI.TRAN_ID
                 WHERE T.TRAN_ID = pn_tran_id 
                 group by T.TRAN_STATE_CD;
                 
                IF lc_tran_state_cd = '9' THEN
                    IF ln_actual_amt > 0 THEN -- sanity check
                        RAISE_APPLICATION_ERROR(-20555, 'Invalid tran_state_cd for tran ' || pn_tran_id || ': Intended batch with Actual Amount');
                    ELSIF ln_intended_amt > 0 THEN
                        pc_sale_phase_cd := 'I'; -- needs processing of intended sale
                        ln_actual_amt := NULL; -- Use Intended Amt
                        lc_auth_type_cd := 'S'; -- network sale
                        lc_new_tran_state_cd := 'A'; -- processing tran
                    ELSE -- we can assume lc_auth_hold_used != 'Y' or it would not have gotten here
                        pc_sale_phase_cd := '-'; -- no processing needed
                        -- do not create sale auth record
                        lc_new_tran_state_cd := 'U'; -- processed_server_tran_intended
                    END IF;             
                ELSE
                    SELECT NVL(MAX(AUTH_AMT), 0)
                      INTO ln_previous_saled_amt
                      FROM (SELECT /*+ index(AT IDX_AUTH_TRAN_ID) */ AT.AUTH_AMT
                              FROM PSS.AUTH AT
                             WHERE AT.TRAN_ID = pn_tran_id
                               AND AT.AUTH_TYPE_CD IN('U', 'S')
                               AND AT.AUTH_STATE_ID IN(2)
                             ORDER BY AT.AUTH_TS DESC, AT.AUTH_ID DESC)
                     WHERE ROWNUM = 1;
                    IF ln_previous_saled_amt != ln_actual_amt THEN
                        pc_sale_phase_cd := 'A'; -- needs processing of actual sale
                        IF ln_actual_amt = 0 THEN -- had previous amount, but now cancelled
                            lc_auth_type_cd := 'E'; -- Sale Reversal
                        ELSIF ln_previous_saled_amt > 0 THEN
                            lc_auth_type_cd := 'D'; -- Sale Adjustment
                        ELSIF lc_auth_hold_used = 'Y' THEN                    
                            lc_auth_type_cd := 'U'; -- Pre-Authed Sale
                        ELSE
                            lc_auth_type_cd := 'S'; -- Sale
                        END IF;
                        lc_new_tran_state_cd := 'A'; -- processing tran
                    ELSIF ln_actual_amt = 0 THEN -- previous amount = actual amount = 0
                        IF ln_intended_amt > 0 AND lc_tran_state_cd != '8' THEN
                            -- This may have been an intended sale that failed, in which case we need to retry the intended amt
                            SELECT SALE_TYPE_CD
                              INTO pc_sale_phase_cd
                              FROM PSS.SALE
                             WHERE TRAN_ID = pn_tran_id;
                            IF pc_sale_phase_cd = 'I' THEN -- needs re-processing of intended sale
                                ln_actual_amt := NULL; -- Use Intended Amt
                                lc_auth_type_cd := 'S'; -- network sale
                                lc_new_tran_state_cd := 'A'; -- processing tran
                            ELSIF lc_auth_hold_used = 'Y' THEN
                                pc_sale_phase_cd := 'A'; -- Actual
                                lc_auth_type_cd := 'C'; -- Auth reversal
                                lc_new_tran_state_cd := 'A'; -- processing tran
                            ELSE
                                pc_sale_phase_cd := '-'; -- no processing needed
                                -- do not create sale auth record
                                lc_new_tran_state_cd := 'C'; -- client cancelled
                            END IF;
                        ELSIF lc_auth_hold_used = 'Y' THEN
                            IF lc_tran_state_cd IN('I') AND ln_device_result_type_cd IS NULL THEN
                                pc_sale_phase_cd := 'R'; -- Reversal
                            ELSE
                                pc_sale_phase_cd := 'A'; -- Actual
                            END IF;
                            lc_auth_type_cd := 'C'; -- Auth reversal
                            lc_new_tran_state_cd := 'A'; -- processing tran
                        ELSE
                            pc_sale_phase_cd := '-'; -- no processing needed
                            -- do not create sale auth record
                            lc_new_tran_state_cd := 'C'; -- client cancelled
                        END IF;
                    ELSE
                        pc_sale_phase_cd := '-'; -- no processing needed
                        -- do not create sale auth record
                        lc_new_tran_state_cd := 'D'; -- tran complete
                    END IF;
                END IF;
                IF pc_sale_phase_cd != '-' THEN
                    SELECT COUNT(*)
                      INTO pn_prior_attempts
                      FROM PSS.AUTH A
                     WHERE A.TRAN_ID = pn_tran_id
                       AND A.AUTH_TYPE_CD NOT IN('N','L')
                       AND A.AUTH_STATE_ID IN(3, 4);  	
                END IF;
            END IF;
            select max(consumer_acct_type_id) into ln_consumer_acct_type_id from pss.consumer_acct where consumer_acct_id=ln_apply_to_consumer_acct_id;
            IF ln_consumer_acct_type_id = 6 AND lc_auth_auth_type_cd ='L' THEN -- logic for replenish refund, local auth and offline sale
              lc_auth_type_cd:='O';
            END IF;
            IF pn_sale_auth_id IS NULL AND lc_auth_type_cd IS NOT NULL THEN
                SELECT PSS.SEQ_AUTH_ID.NEXTVAL
                  INTO pn_sale_auth_id
                  FROM DUAL;
                INSERT INTO PSS.AUTH (
                        AUTH_ID,
                        TRAN_ID,
                        AUTH_TYPE_CD,
                        AUTH_STATE_ID,
                        AUTH_PARSED_ACCT_DATA,
                        ACCT_ENTRY_METHOD_CD,
                        AUTH_AMT,
                        AUTH_TS,
                        TERMINAL_BATCH_ID,
                        TRACE_NUMBER)
                 SELECT pn_sale_auth_id,
                        pn_tran_id,
                        lc_auth_type_cd,
                        6,
                        a.AUTH_PARSED_ACCT_DATA,
                        a.ACCT_ENTRY_METHOD_CD,
                        NVL(ln_actual_amt, ln_intended_amt),	    
                        SYSDATE,
                        ln_terminal_batch_id,
                        a.TRACE_NUMBER
                   FROM PSS.AUTH a
                  WHERE a.AUTH_ID = ln_auth_auth_id;
            END IF;
        ELSIF pn_refund_id IS NOT NULL THEN -- it's a refund
            IF lc_terminal_capture_flag = 'Y' THEN
                IF pn_terminal_batch_id IS NULL THEN
                	ln_terminal_batch_id := GET_AVAILABLE_TERMINAL_BATCH(ln_terminal_id, ln_allowed_trans, TRUE);
                ELSE
                    ln_terminal_batch_id := pn_terminal_batch_id;
                END IF;
                -- stick it in the open terminal batch
                UPDATE PSS.REFUND
                   SET TERMINAL_BATCH_ID = ln_terminal_batch_id,
                       REFUND_STATE_ID = 6
                 WHERE REFUND_ID = pn_refund_id;
                pc_sale_phase_cd := '-'; -- No further processing
                lc_new_tran_state_cd := 'T'; -- Processed Server Batch
            ELSE
                -- We could check the actual amount and the previous refunded amount for sanity, but that seems overkill. 
                -- Just assume actual amount > 0 and previous refunded amount = 0
                -- Also, assume REFUND_STATE_ID IN(6, 2, 3)
                pc_sale_phase_cd := 'A'; -- needs processing of actual sale
                lc_new_tran_state_cd := 'A'; -- processing tran
                SELECT COUNT(*)
                  INTO pn_prior_attempts
                  FROM PSS.REFUND R
                 WHERE R.TRAN_ID = pn_tran_id
                   AND R.REFUND_TYPE_CD IN ('G','C','V','S')
                   AND R.REFUND_STATE_ID IN(2, 3);
            END IF;
        ELSE -- neither authId nor refundId is found, set to error
            pc_sale_phase_cd := '-'; -- No further processing
            lc_new_tran_state_cd := 'E'; -- Complete Error
        END IF;
		IF lc_new_tran_state_cd != lc_tran_state_cd THEN
			UPDATE PSS.TRAN 
			   SET TRAN_STATE_CD = lc_new_tran_state_cd
			 WHERE TRAN_ID = pn_tran_id;
		END IF;
    END;
    
    -- R37 and above signature
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE UPDATE_PROCESSED_TRAN(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pn_sale_auth_id PSS.AUTH.AUTH_ID%TYPE,
        pn_refund_id PSS.REFUND.REFUND_ID%TYPE,
        pc_settled_flag CHAR,
        pc_sale_phase_cd CHAR,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
        pn_auth_approved_amt PSS.AUTH.AUTH_AMT_APPROVED%TYPE,
        pv_authority_response_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
        pv_authority_response_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
        pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
        pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
        pd_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
        pc_ignore_reprocess_flag CHAR,
        pv_tran_global_trans_cd OUT PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
        pv_auth_authority_tran_cd OUT PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
		pc_settle_update_needed OUT VARCHAR2,
		pv_source_system_cd OUT VARCHAR2,
        pc_auth_type_cd OUT VARCHAR2,
        pn_override_trans_type_id OUT PSS.PAYMENT_SUBTYPE.TRANS_TYPE_ID%TYPE,
        pc_partial_reversal_flag PSS.AUTH.PARTIAL_REVERSAL_FLAG%TYPE DEFAULT NULL,
        pc_sprout_enabled_flag CHAR DEFAULT 'N')
    IS
        lv_tran_lock_cd VARCHAR2(128);
        lv_lock_handle VARCHAR2(128);
        ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
        lc_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
        ln_payment_subtype_key_id PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE;
        lv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE;
        ln_allowed_trans PLS_INTEGER;
        ln_auth_auth_id PSS.AUTH.AUTH_ID%TYPE;
        lc_auth_hold_used PSS.AUTH.AUTH_HOLD_USED%TYPE;
        lc_imported PSS.SALE.IMPORTED%TYPE;
        lc_sale_type_cd PSS.SALE.SALE_TYPE_CD%TYPE;
        ln_sale_result_id PSS.SALE.SALE_RESULT_ID%TYPE;
        pc_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
        lc_match_flag CHAR(1);
        ln_apply_to_consumer_acct_id PSS.TRAN_LINE_ITEM.APPLY_TO_CONSUMER_ACCT_ID%TYPE;
        lv_device_name PSS.TRAN.DEVICE_NAME%TYPE;
        lv_device_tran_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE;
        ln_parent_tran_id PSS.TRAN.TRAN_ID%TYPE;
        ln_replenish_amount PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE;
        ln_replenish_bonus PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE;
        ln_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE;
        ld_tran_start_ts PSS.TRAN.TRAN_START_TS%TYPE;
        ln_consumer_acct_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_TYPE_ID%TYPE;
		lc_acct_entry_method_cd PSS.AUTH.ACCT_ENTRY_METHOD_CD%TYPE;
    BEGIN
		pc_settle_update_needed := 'Y';
        SELECT T.DEVICE_NAME || ':' || T.TRAN_DEVICE_TRAN_CD, T.PAYMENT_SUBTYPE_CLASS, T.PAYMENT_SUBTYPE_KEY_ID, T.TRAN_STATE_CD, T.TRAN_GLOBAL_TRANS_CD, AA.AUTH_AUTHORITY_TRAN_CD, AA.AUTH_ID, AA.AUTH_HOLD_USED, 'PSS', T.CONSUMER_ACCT_ID, T.TRAN_START_TS
          INTO lv_tran_lock_cd, lv_payment_subtype_class, ln_payment_subtype_key_id, lc_tran_state_cd, pv_tran_global_trans_cd, pv_auth_authority_tran_cd, ln_auth_auth_id, lc_auth_hold_used, pv_source_system_cd, ln_consumer_acct_id, ld_tran_start_ts
          FROM PSS.TRAN T
          LEFT OUTER JOIN PSS.AUTH AA ON T.TRAN_ID = AA.TRAN_ID AND AA.AUTH_TYPE_CD = 'N' AND (AA.AUTH_STATE_ID IN(2, 5, 6) OR (AA.AUTH_STATE_ID IN(3, 4) AND AA.AUTH_TYPE_CD = 'N' AND AA.AUTH_HOLD_USED = 'Y'))
         WHERE T.TRAN_ID = pn_tran_id;
        IF lc_tran_state_cd != 'A' THEN
            IF pc_ignore_reprocess_flag = 'Y' THEN
                IF pn_sale_auth_id IS NOT NULL THEN
                    SELECT AUTH_TYPE_CD, OVERRIDE_TRANS_TYPE_ID, CASE WHEN AUTH_RESULT_CD = pc_auth_result_cd 
                        AND (AUTH_RESP_CD = pv_authority_response_cd OR (AUTH_RESP_CD IS NULL AND pv_authority_response_cd IS NULL))
                        AND (AUTH_RESP_DESC = pv_authority_response_desc OR (AUTH_RESP_DESC IS NULL AND pv_authority_response_desc IS NULL))
                        AND (AUTH_AUTHORITY_TRAN_CD = pv_authority_tran_cd OR (AUTH_AUTHORITY_TRAN_CD IS NULL AND pv_authority_tran_cd IS NULL))
                        AND (AUTH_AUTHORITY_REF_CD = pv_authority_ref_cd OR (AUTH_AUTHORITY_REF_CD IS NULL AND pv_authority_ref_cd IS NULL))
                        AND AUTH_AUTHORITY_TS = pd_authority_ts
                        AND (AUTH_AMT_APPROVED = pn_auth_approved_amt / pn_minor_currency_factor OR (AUTH_AMT_APPROVED IS NULL AND pn_auth_approved_amt IS NULL))
                        AND (AUTH_AUTHORITY_MISC_DATA = pv_authority_misc_data OR (AUTH_AUTHORITY_MISC_DATA IS NULL AND pv_authority_misc_data IS NULL)) THEN 'Y' ELSE 'N' END
                      INTO pc_auth_type_cd, pn_override_trans_type_id, lc_match_flag
                      FROM PSS.AUTH 
                     WHERE AUTH_ID = pn_sale_auth_id;
                ELSIF pn_refund_id IS NOT NULL THEN
                    SELECT REFUND_TYPE_CD, OVERRIDE_TRANS_TYPE_ID, CASE WHEN REFUND_STATE_ID = DECODE(pc_auth_result_cd, 'Y', 1, 'P', 1, 'N', 2, 'O', 2, 'F', 3, 'R', 2)
                        AND (REFUND_RESP_CD = pv_authority_response_cd OR (REFUND_RESP_CD IS NULL AND pv_authority_response_cd IS NULL))
                        AND (REFUND_RESP_DESC = pv_authority_response_desc OR (REFUND_RESP_DESC IS NULL AND pv_authority_response_desc IS NULL))
                        AND (REFUND_AUTHORITY_TRAN_CD = pv_authority_tran_cd OR (REFUND_AUTHORITY_TRAN_CD IS NULL AND pv_authority_tran_cd IS NULL))
                        AND (REFUND_AUTHORITY_REF_CD = pv_authority_ref_cd OR (REFUND_AUTHORITY_REF_CD IS NULL AND pv_authority_ref_cd IS NULL))
                        AND REFUND_AUTHORITY_TS = pd_authority_ts
                        AND (REFUND_AUTHORITY_MISC_DATA = pv_authority_misc_data OR (REFUND_AUTHORITY_MISC_DATA IS NULL AND pv_authority_misc_data IS NULL)) THEN 'Y' ELSE 'N' END
                      INTO pc_auth_type_cd, pn_override_trans_type_id, lc_match_flag
                      FROM PSS.REFUND
                     WHERE REFUND_ID = pn_refund_id;               
                END IF;
                IF lc_match_flag = 'Y' THEN
                RETURN;
                END IF;
            END IF;
                RAISE_APPLICATION_ERROR(-20554, 'Invalid tran_state_cd for tran ' || pn_tran_id || ' when updating processed tran');      
            END IF;
        IF NVL(pc_settled_flag, '-') != 'Y' AND pc_auth_result_cd IN('Y', 'P') AND (lv_payment_subtype_class  LIKE 'Authority::ISO8583%') THEN
            -- If this is a retry of a failure, it may already have a terminal_batch assigned
            IF pn_sale_auth_id IS NOT NULL THEN
                SELECT MAX(TERMINAL_BATCH_ID)
                  INTO ln_terminal_batch_id
                  FROM PSS.AUTH
                 WHERE TRAN_ID = pn_tran_id;
            ELSIF pn_refund_id IS NOT NULL THEN
                SELECT MAX(TERMINAL_BATCH_ID)
                  INTO ln_terminal_batch_id
                  FROM PSS.REFUND
                 WHERE TRAN_ID = pn_tran_id;
            END IF;
            IF ln_terminal_batch_id IS NULL THEN
                ln_terminal_batch_id := GET_AVAILABLE_TERMINAL_BATCH(ln_payment_subtype_key_id, ln_allowed_trans, TRUE);
                IF ln_allowed_trans <= 0 THEN
                    RAISE_APPLICATION_ERROR(-20556, 'Terminal Batch ' || ln_terminal_batch_id || ' is full for terminal ' || ln_payment_subtype_key_id || '; Can not add tran ' || pn_tran_id || ' to it');
                END IF;
            END IF;
        END IF;
        -- Lock transaction
        lv_lock_handle := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', lv_tran_lock_cd);
        IF pn_sale_auth_id IS NOT NULL THEN
            UPDATE PSS.AUTH 
               SET (
			    AUTH_STATE_ID,
                AUTH_RESULT_CD,
                AUTH_RESP_CD,
                AUTH_RESP_DESC,
                AUTH_AUTHORITY_TRAN_CD,
                AUTH_AUTHORITY_REF_CD,
                AUTH_AUTHORITY_TS,
                TERMINAL_BATCH_ID,
                AUTH_AMT_APPROVED,
                AUTH_AUTHORITY_MISC_DATA,
                PARTIAL_REVERSAL_FLAG) =
            (SELECT 
                DECODE(pc_auth_result_cd, 'Y', 2, 'P', 5, 'N', 3, 'O', 7, 'F', 4, 'R', 7),
                pc_auth_result_cd,
                pv_authority_response_cd,
                pv_authority_response_desc,
                pv_authority_tran_cd,
                pv_authority_ref_cd,
                pd_authority_ts,
                ln_terminal_batch_id,
                pn_auth_approved_amt / pn_minor_currency_factor,
                pv_authority_misc_data,
                pc_partial_reversal_flag
             FROM DUAL)
             WHERE AUTH_ID = pn_sale_auth_id
             RETURNING AUTH_TYPE_CD, OVERRIDE_TRANS_TYPE_ID, ACCT_ENTRY_METHOD_CD INTO pc_auth_type_cd, pn_override_trans_type_id, lc_acct_entry_method_cd;
            IF lc_auth_hold_used = 'Y' AND pc_auth_result_cd IN('O', 'R') THEN
                DELETE 
                  FROM PSS.CONSUMER_ACCT_AUTH_HOLD
                 WHERE AUTH_ID = ln_auth_auth_id;
            END IF;
        ELSIF pn_refund_id IS NOT NULL THEN
           UPDATE PSS.REFUND
               SET REFUND_STATE_ID = DECODE(pc_auth_result_cd, 'Y', 1, 'P', 1, 'N', 2, 'O', 2, 'F', 3, 'R', 2),
                REFUND_RESP_CD = NVL(pv_authority_response_cd, REFUND_RESP_CD),
                REFUND_RESP_DESC = NVL(pv_authority_response_desc, REFUND_RESP_DESC),
                REFUND_AUTHORITY_TRAN_CD = NVL(pv_authority_tran_cd, REFUND_AUTHORITY_TRAN_CD),
                REFUND_AUTHORITY_REF_CD = NVL(pv_authority_ref_cd, REFUND_AUTHORITY_REF_CD),
                REFUND_AUTHORITY_TS = NVL(pd_authority_ts, REFUND_AUTHORITY_TS),
                TERMINAL_BATCH_ID = NVL(ln_terminal_batch_id, TERMINAL_BATCH_ID),
                REFUND_AUTHORITY_MISC_DATA = NVL(pv_authority_misc_data, REFUND_AUTHORITY_MISC_DATA)
            WHERE REFUND_ID = pn_refund_id
            RETURNING REFUND_TYPE_CD, OVERRIDE_TRANS_TYPE_ID INTO pc_auth_type_cd, pn_override_trans_type_id;
            END IF;
			
		IF lc_acct_entry_method_cd IS NOT NULL THEN
			SELECT COALESCE(MAX(OVERRIDE_TRANS_TYPE_ID), pn_override_trans_type_id)
			INTO pn_override_trans_type_id
			FROM PSS.TRANS_TYPE_ENTRY_OVERRIDE
			WHERE TRANS_TYPE_ID = pn_override_trans_type_id AND ACCT_ENTRY_METHOD_CD = lc_acct_entry_method_cd;
		END IF;
        
        -- update tran_state_cd
        IF pc_auth_result_cd IN('F') THEN
            lc_tran_state_cd := 'J'; -- Incomplete Error: Manual intervention needed
        ELSIF pc_auth_result_cd IN('O', 'R') THEN
            IF pc_sale_phase_cd = 'R' THEN -- it was an auth reversal
                lc_tran_state_cd := 'V';
            ELSIF pc_auth_type_cd IN('C', 'E') THEN -- sale reversal or auth reversal (indicates actual amount = 0)
                lc_tran_state_cd := 'C'; -- Client Cancelled
            ELSIF pc_sale_phase_cd = 'I' THEN
                -- check If actuals have since been uploaded
                SELECT DECODE(SALE_TYPE_CD, 'I', 'K', 'A', '8')
                  INTO lc_tran_state_cd
                  FROM PSS.SALE
                WHERE TRAN_ID = pn_tran_id;         
            ELSE
                lc_tran_state_cd := 'E'; -- Complete Error: don't retry
            END IF;
        ELSIF pc_auth_result_cd IN('N') THEN
            lc_tran_state_cd := 'I'; -- Incomplete: will retry
        ELSIF pc_auth_result_cd IN('Y', 'P') THEN
            IF pc_sale_phase_cd = 'R' THEN -- it was an auth reversal
                IF pc_settled_flag = 'Y' THEN
                    lc_tran_state_cd := 'V';
                ELSE
                     lc_tran_state_cd := 'T';
                END IF;
            ELSIF pc_sale_phase_cd = 'I' THEN
                -- check If actuals have since been uploaded
                SELECT DECODE(SALE_TYPE_CD, 'I', 'U', 'A', '8')
                  INTO lc_tran_state_cd
                  FROM PSS.SALE
                WHERE TRAN_ID = pn_tran_id;
            ELSIF pc_settled_flag = 'Y' THEN
                IF pc_auth_type_cd IN('C', 'E') THEN -- sale reversal or auth reversal (indicates actual amount = 0)
                    lc_tran_state_cd := 'C'; -- Client Cancelled
                ELSE
                	IF DBADMIN.PKG_GLOBAL.GET_APP_SETTING(UPPER(lv_payment_subtype_class) || '_EFT_AFTER_FUNDING_IND') = 'Y' THEN
                		lc_tran_state_cd := 'X'; -- Pending processor deposit
                		pc_settle_update_needed := 'N';
                	ELSE
                    	lc_tran_state_cd := 'D'; -- Complete
                    END IF;
                END IF;
            ELSE
                lc_tran_state_cd := 'T'; -- Processed Server Batch
            END IF;
        ELSE
            RAISE_APPLICATION_ERROR(-20557, 'Invalid auth result cd "' ||  pc_auth_result_cd + '" for tran ' || pn_tran_id);
        END IF;
        
        UPDATE PSS.TRAN
           SET TRAN_STATE_CD = lc_tran_state_cd
         WHERE TRAN_ID = pn_tran_id
         RETURNING PARENT_TRAN_ID
          INTO ln_parent_tran_id;
          
        IF pc_sprout_enabled_flag = 'Y' AND pc_auth_result_cd in ('Y','P') THEN
          PSS.UPDATE_SPROUT_CONSUMER_ACCT(pn_tran_id, ln_parent_tran_id,pn_sale_auth_id,pn_refund_id,pc_auth_type_cd,ln_consumer_acct_id,ld_tran_start_ts);
        END IF;
 
        select max(s.imported), max(CASE WHEN pn_refund_id IS NOT NULL THEN 'R' ELSE s.SALE_TYPE_CD END), max(CASE WHEN pn_refund_id IS NOT NULL THEN 0 ELSE s.sale_result_id END), max(x.tran_state_cd) 
        into lc_imported,lc_sale_type_cd,ln_sale_result_id,pc_tran_state_cd
        from pss.tran x left outer join pss.sale s on x.tran_id=s.tran_id 
        where x.tran_id=pn_tran_id;
        
        IF pc_settle_update_needed != 'N' AND (lc_imported is NULL or lc_imported != '-') AND lc_sale_type_cd IN(PKG_CONST.SALE_TYPE__ACTUAL, PKG_CONST.SALE_TYPE__CASH, 'R') AND ln_sale_result_id = 0 AND pc_tran_state_cd NOT IN('F', 'G', 'Z') THEN
            pc_settle_update_needed := 'Y';
        ELSE
            pc_settle_update_needed := 'N';
        END IF;
    END;      

    FUNCTION GET_TERMINAL_GLOBAL_TOKEN(
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_payment_subtype_key_id NUMBER)
    RETURN VARCHAR2
    IS
        lv_global_token PSS.TERMINAL.GLOBAL_TOKEN_CD%TYPE;
    BEGIN
        IF pv_payment_subtype_class IN('Authority::NOP', 'Cash') THEN
            RAISE_APPLICATION_ERROR(-20865, 'Payment subtype class ''' || pv_payment_subtype_class || ''' does not have terminals');
        ELSIF pv_payment_subtype_class = 'Aramark' THEN
            SELECT GLOBAL_TOKEN_CD
              INTO lv_global_token
              FROM PSS.ARAMARK_PAYMENT_TYPE
             WHERE ARAMARK_PAYMENT_TYPE_ID = pn_payment_subtype_key_id;
        ELSIF pv_payment_subtype_class = 'BlackBoard' THEN
            SELECT GLOBAL_TOKEN_CD
              INTO lv_global_token
              FROM PSS.BLACKBRD_TENDER
             WHERE BLACKBRD_TENDER_ID = pn_payment_subtype_key_id;
        ELSIF pv_payment_subtype_class LIKE 'Internal%' THEN
            SELECT GLOBAL_TOKEN_CD
              INTO lv_global_token
              FROM PSS.INTERNAL_PAYMENT_TYPE
             WHERE INTERNAL_PAYMENT_TYPE_ID = pn_payment_subtype_key_id;
        ELSE
            SELECT GLOBAL_TOKEN_CD
              INTO lv_global_token
              FROM PSS.TERMINAL
             WHERE TERMINAL_ID = pn_payment_subtype_key_id;
        END IF;
        RETURN lv_global_token;
    END;
    
    PROCEDURE UPDATE_TERMINAL_GLOBAL_TOKEN(
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_payment_subtype_key_id NUMBER,
        pv_old_global_token PSS.TERMINAL.GLOBAL_TOKEN_CD%TYPE,
        pv_new_global_token PSS.TERMINAL.GLOBAL_TOKEN_CD%TYPE,
        pn_update_count OUT PLS_INTEGER,
        pc_current_must_match CHAR DEFAULT 'N')
    IS
		ln_term_lock_max_duration_sec NUMBER := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('POSM_TERMINAL_LOCK_MAX_DURATION_SEC'));
		ld_sysdate DATE := SYSDATE;
		ln_forced_unlock_count NUMBER := 0;
    BEGIN
        IF pv_new_global_token IS NULL THEN
            RAISE_APPLICATION_ERROR(-20864, 'Global Token Code may not be null');
        ELSIF pv_payment_subtype_class IN('Authority::NOP', 'Cash') THEN
            RAISE_APPLICATION_ERROR(-20865, 'Payment subtype class ''' || pv_payment_subtype_class || ''' does not have terminals');
        ELSIF pv_payment_subtype_class = 'Aramark' THEN
            UPDATE PSS.ARAMARK_PAYMENT_TYPE
               SET GLOBAL_TOKEN_CD = pv_new_global_token
             WHERE ARAMARK_PAYMENT_TYPE_ID = pn_payment_subtype_key_id
               AND ((pc_current_must_match = 'N' AND GLOBAL_TOKEN_CD IS NULL) OR GLOBAL_TOKEN_CD = pv_old_global_token);
            pn_update_count := SQL%ROWCOUNT;
			IF pn_update_count = 0 THEN
				UPDATE PSS.ARAMARK_PAYMENT_TYPE
				   SET GLOBAL_TOKEN_CD = pv_new_global_token
				 WHERE ARAMARK_PAYMENT_TYPE_ID = pn_payment_subtype_key_id
					AND GLOBAL_TOKEN_CD IS NOT NULL
					AND LAST_UPDATED_TS < ld_sysdate - ln_term_lock_max_duration_sec/86400;
				pn_update_count := SQL%ROWCOUNT;
				ln_forced_unlock_count := pn_update_count;
			END IF;
        ELSIF pv_payment_subtype_class = 'BlackBoard' THEN
            UPDATE PSS.BLACKBRD_TENDER
               SET GLOBAL_TOKEN_CD = pv_new_global_token
             WHERE BLACKBRD_TENDER_ID = pn_payment_subtype_key_id
               AND ((pc_current_must_match = 'N' AND GLOBAL_TOKEN_CD IS NULL) OR GLOBAL_TOKEN_CD = pv_old_global_token);
            pn_update_count := SQL%ROWCOUNT;
			IF pn_update_count = 0 THEN
				UPDATE PSS.BLACKBRD_TENDER
				   SET GLOBAL_TOKEN_CD = pv_new_global_token
				 WHERE BLACKBRD_TENDER_ID = pn_payment_subtype_key_id
					AND GLOBAL_TOKEN_CD IS NOT NULL
					AND LAST_UPDATED_TS < ld_sysdate - ln_term_lock_max_duration_sec/86400;
				pn_update_count := SQL%ROWCOUNT;
				ln_forced_unlock_count := pn_update_count;
			END IF;
        ELSIF pv_payment_subtype_class LIKE 'Internal%' THEN
            UPDATE PSS.INTERNAL_PAYMENT_TYPE
               SET GLOBAL_TOKEN_CD = pv_new_global_token
             WHERE INTERNAL_PAYMENT_TYPE_ID = pn_payment_subtype_key_id
               AND ((pc_current_must_match = 'N' AND GLOBAL_TOKEN_CD IS NULL) OR GLOBAL_TOKEN_CD = pv_old_global_token);
            pn_update_count := SQL%ROWCOUNT;
			IF pn_update_count = 0 THEN
				UPDATE PSS.INTERNAL_PAYMENT_TYPE
				   SET GLOBAL_TOKEN_CD = pv_new_global_token
				 WHERE INTERNAL_PAYMENT_TYPE_ID = pn_payment_subtype_key_id
					AND GLOBAL_TOKEN_CD IS NOT NULL
					AND LAST_UPDATED_TS < ld_sysdate - ln_term_lock_max_duration_sec/86400;
				pn_update_count := SQL%ROWCOUNT;
				ln_forced_unlock_count := pn_update_count;
			END IF;
        ELSE
            UPDATE PSS.TERMINAL
               SET GLOBAL_TOKEN_CD = pv_new_global_token,
                   TERMINAL_STATE_ID =  DECODE(TERMINAL_STATE_ID, 5, 5, 3) -- terminal is busy or settlement retry now
             WHERE TERMINAL_ID = pn_payment_subtype_key_id
               AND ((pc_current_must_match = 'N' AND GLOBAL_TOKEN_CD IS NULL AND TERMINAL_STATE_ID IN(1, 5))
                OR (GLOBAL_TOKEN_CD = pv_old_global_token AND TERMINAL_STATE_ID IN(3, 5)));
            pn_update_count := SQL%ROWCOUNT;
			IF pn_update_count = 0 THEN
				UPDATE PSS.TERMINAL
				   SET GLOBAL_TOKEN_CD = pv_new_global_token,
					   TERMINAL_STATE_ID =  DECODE(TERMINAL_STATE_ID, 5, 5, 3) -- terminal is busy or settlement retry now
				 WHERE TERMINAL_ID = pn_payment_subtype_key_id
					AND GLOBAL_TOKEN_CD IS NOT NULL
					AND LAST_UPDATED_TS < ld_sysdate - ln_term_lock_max_duration_sec/86400;
				pn_update_count := SQL%ROWCOUNT;
				ln_forced_unlock_count := pn_update_count;
			END IF;
        END IF;
		
		IF ln_forced_unlock_count > 0 THEN
			pkg_exception_processor.sp_log_exception (
				pkg_app_exec_hist_globals.unknown_error_id,
				'POSM terminal ' || pn_payment_subtype_key_id || '(' || pv_payment_subtype_class || ') was locked for more than ' || ln_term_lock_max_duration_sec || ' seconds, forcefully unlocked',
				NULL,
				'PSS.PKG_SETTLEMENT.UPDATE_TERMINAL_GLOBAL_TOKEN'
			);
		END IF;
    END;
    
    PROCEDURE CLEAR_TERMINAL_GLOBAL_TOKEN(
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_payment_subtype_key_id NUMBER,
        pv_global_token PSS.TERMINAL.GLOBAL_TOKEN_CD%TYPE,
        pn_update_count OUT PLS_INTEGER)
    IS
    BEGIN
        IF pv_payment_subtype_class IN('Authority::NOP', 'Cash') THEN
            RAISE_APPLICATION_ERROR(-20865, 'Payment subtype class ''' || pv_payment_subtype_class || ''' does not have terminals');
        ELSIF pv_payment_subtype_class = 'Aramark' THEN
            UPDATE PSS.ARAMARK_PAYMENT_TYPE
               SET GLOBAL_TOKEN_CD = NULL,
                   LAST_RELEASED_UTC_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP)
             WHERE ARAMARK_PAYMENT_TYPE_ID = pn_payment_subtype_key_id
               AND GLOBAL_TOKEN_CD = pv_global_token;
            pn_update_count := SQL%ROWCOUNT;
        ELSIF pv_payment_subtype_class = 'BlackBoard' THEN
            UPDATE PSS.BLACKBRD_TENDER
               SET GLOBAL_TOKEN_CD = NULL,
                   LAST_RELEASED_UTC_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP)
             WHERE BLACKBRD_TENDER_ID = pn_payment_subtype_key_id
               AND GLOBAL_TOKEN_CD = pv_global_token;
            pn_update_count := SQL%ROWCOUNT;
        ELSIF pv_payment_subtype_class LIKE 'Internal%' THEN
            UPDATE PSS.INTERNAL_PAYMENT_TYPE
               SET GLOBAL_TOKEN_CD = NULL,
                   LAST_RELEASED_UTC_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP)
             WHERE INTERNAL_PAYMENT_TYPE_ID = pn_payment_subtype_key_id
               AND GLOBAL_TOKEN_CD = pv_global_token;
            pn_update_count := SQL%ROWCOUNT;
        ELSE
            UPDATE PSS.TERMINAL
               SET GLOBAL_TOKEN_CD = NULL,
                   TERMINAL_STATE_ID = DECODE(TERMINAL_STATE_ID, 5, 5, 1), -- terminal is active or settlement retry now
                   LAST_RELEASED_UTC_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP)
             WHERE TERMINAL_ID = pn_payment_subtype_key_id
               AND GLOBAL_TOKEN_CD = pv_global_token;
            pn_update_count := SQL%ROWCOUNT;
        END IF;
    END;

    FUNCTION ADD_ADMIN_CMD(
        pv_payment_subtype_class PSS.ADMIN_CMD.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_payment_subtype_key_id PSS.ADMIN_CMD.PAYMENT_SUBTYPE_KEY_ID%TYPE,
        pn_admin_cmd_type_id PSS.ADMIN_CMD.ADMIN_CMD_TYPE_ID%TYPE,
        pv_requested_by PSS.ADMIN_CMD.REQUESTED_BY%TYPE,
        pn_priority PSS.ADMIN_CMD.PRIORITY%TYPE)
    RETURN PSS.ADMIN_CMD.ADMIN_CMD_ID%TYPE
    IS
        ln_admin_cmd_id PSS.ADMIN_CMD.ADMIN_CMD_ID%TYPE;
    BEGIN
        SELECT PSS.SEQ_ADMIN_CMD_ID.NEXTVAL
          INTO ln_admin_cmd_id
          FROM DUAL;
        INSERT INTO PSS.ADMIN_CMD (
                ADMIN_CMD_ID,
                ADMIN_CMD_TYPE_ID,
                PAYMENT_SUBTYPE_KEY_ID,
                PAYMENT_SUBTYPE_CLASS,
                REQUESTED_BY,
                PRIORITY) 
         VALUES(ln_admin_cmd_id, 
                pn_admin_cmd_type_id, 
                pn_payment_subtype_key_id,
                pv_payment_subtype_class,
                pv_requested_by,
                pn_priority);
         RETURN ln_admin_cmd_id;
    END;
    
    FUNCTION ADD_ADMIN_CMD_PARAM(
        pn_admin_cmd_id PSS.ADMIN_CMD_PARAM.ADMIN_CMD_ID%TYPE,
        pv_param_name PSS.ADMIN_CMD_PARAM.PARAM_NAME%TYPE,
        pv_param_value PSS.ADMIN_CMD_PARAM.PARAM_VALUE%TYPE)
    RETURN PSS.ADMIN_CMD_PARAM.ADMIN_CMD_PARAM_ID%TYPE
    IS
        ln_admin_cmd_param_id PSS.ADMIN_CMD_PARAM.ADMIN_CMD_PARAM_ID%TYPE;
    BEGIN
        SELECT PSS.SEQ_ADMIN_CMD_PARAM_ID.NEXTVAL
          INTO ln_admin_cmd_param_id
          FROM DUAL;
        INSERT INTO PSS.ADMIN_CMD_PARAM (
                ADMIN_CMD_PARAM_ID,
                ADMIN_CMD_ID,
                PARAM_NAME,
                PARAM_VALUE) 
         VALUES(ln_admin_cmd_param_id, 
                pn_admin_cmd_id,
                pv_param_name, 
                pv_param_value);
         RETURN ln_admin_cmd_param_id;
    END;   
    
    PROCEDURE LOCK_PARTIAL_SETTLE_POLLING(
        pv_process_id PSS.POSM_SETTING.POSM_SETTING_VALUE%TYPE,
        pv_success_flag OUT VARCHAR2,
        pv_locked_by_process_id OUT PSS.POSM_SETTING.POSM_SETTING_VALUE%TYPE)
    IS
		ln_polling_max_duration_sec NUMBER := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('POSM_FEEDBACK_POLLING_MAX_DURATION_SEC'));
		lt_last_updated_utc_ts PSS.POSM_SETTING.LAST_UPDATED_UTC_TS%TYPE;
    BEGIN
        SELECT POSM_SETTING_VALUE, 'N', LAST_UPDATED_UTC_TS
          INTO pv_locked_by_process_id, pv_success_flag, lt_last_updated_utc_ts
          FROM PSS.POSM_SETTING
         WHERE POSM_SETTING_NAME = 'PARTIAL_SETTLEMENT_POLLING_LOCK';
		 
		IF lt_last_updated_utc_ts < SYS_EXTRACT_UTC(SYSTIMESTAMP) - ln_polling_max_duration_sec/86400 THEN
			pkg_exception_processor.sp_log_exception (
				pkg_app_exec_hist_globals.unknown_error_id,
				'POSM feedback polling has been locked by ' || pv_locked_by_process_id || ' since ' || lt_last_updated_utc_ts || ' UTC, unlocking',
				NULL,
				'PSS.PKG_SETTLEMENT.LOCK_PARTIAL_SETTLE_POLLING'
			);
			UNLOCK_PARTIAL_SETTLE_POLLING(pv_locked_by_process_id);
		END IF;
		
        SELECT POSM_SETTING_VALUE, 'N'
        INTO pv_locked_by_process_id, pv_success_flag
        FROM PSS.POSM_SETTING
        WHERE POSM_SETTING_NAME = 'PARTIAL_SETTLEMENT_POLLING_LOCK';
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            BEGIN
                INSERT INTO PSS.POSM_SETTING(POSM_SETTING_NAME, POSM_SETTING_VALUE)
                  VALUES('PARTIAL_SETTLEMENT_POLLING_LOCK', pv_process_id);
                pv_success_flag := 'Y';
            EXCEPTION
                WHEN DUP_VAL_ON_INDEX THEN
                    LOCK_PARTIAL_SETTLE_POLLING(pv_process_id, pv_success_flag, pv_locked_by_process_id);
            END;
    END;
    
    PROCEDURE UNLOCK_PARTIAL_SETTLE_POLLING(
        pv_process_id PSS.POSM_SETTING.POSM_SETTING_VALUE%TYPE)
    IS
    BEGIN
        DELETE 
          FROM PSS.POSM_SETTING
         WHERE POSM_SETTING_NAME = 'PARTIAL_SETTLEMENT_POLLING_LOCK'
           AND POSM_SETTING_VALUE = pv_process_id;
        IF SQL%ROWCOUNT != 1 THEN
            RAISE NO_DATA_FOUND;
        END IF;
    END;
    
    PROCEDURE UPDATE_BATCH_FEEDBACK_REF_CD(
        pv_settlement_batch_ref_cd PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_REF_CD%TYPE,
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pd_auth_authority_ts PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_END_TS%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pv_authority_resp_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pn_settlement_batch_id OUT PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_terminal_batch_id OUT PSS.SETTLEMENT_BATCH.TERMINAL_BATCH_ID%TYPE)
    IS
        ln_settlement_batch_state_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_STATE_ID%TYPE;
    BEGIN
        SELECT SETTLEMENT_BATCH_ID, TERMINAL_BATCH_ID, SETTLEMENT_BATCH_STATE_ID
          INTO pn_settlement_batch_id, pn_terminal_batch_id, ln_settlement_batch_state_id
          FROM (SELECT SB.SETTLEMENT_BATCH_ID, SB.TERMINAL_BATCH_ID, SB.SETTLEMENT_BATCH_STATE_ID
          FROM PSS.SETTLEMENT_BATCH SB
          JOIN PSS.TERMINAL_BATCH TB ON SB.TERMINAL_BATCH_ID = TB.TERMINAL_BATCH_ID
          JOIN PSS.TERMINAL T ON TB.TERMINAL_ID = T.TERMINAL_ID
          JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
          JOIN AUTHORITY.AUTHORITY A ON M.AUTHORITY_ID = A.AUTHORITY_ID
          JOIN AUTHORITY.AUTHORITY_TYPE AUT ON A.AUTHORITY_TYPE_ID = AUT.AUTHORITY_TYPE_ID
          JOIN AUTHORITY.HANDLER H ON H.HANDLER_ID = AUT.HANDLER_ID
         WHERE SB.SETTLEMENT_BATCH_REF_CD = pv_settlement_batch_ref_cd
           AND H.HANDLER_CLASS = pv_payment_subtype_class
           AND TB.TERMINAL_BATCH_CLOSE_TS IS NOT NULL
         ORDER BY DECODE(SB.SETTLEMENT_BATCH_STATE_ID, 7, 1, 5), TB.TERMINAL_BATCH_CYCLE_NUM DESC, SB.SETTLEMENT_BATCH_ID DESC)
         WHERE ROWNUM = 1; 
        
        IF ln_settlement_batch_state_id = 1 OR (ln_settlement_batch_state_id IN(1,2,3,7) AND pc_auth_result_cd IN('P')) THEN
            RETURN;
        END IF;
           
        UPDATE PSS.SETTLEMENT_BATCH SB
           SET SB.SETTLEMENT_BATCH_STATE_ID = DECODE(pc_auth_result_cd, 'Y', 1, 'P', 7, 'N', 2, 'F', 3, 'O', 2, 'R', 2),
               SB.SETTLEMENT_BATCH_END_TS = NVL(pd_auth_authority_ts, SYSDATE),
               SB.SETTLEMENT_BATCH_RESP_DESC = DECODE(TRIM(pv_authority_resp_desc), NULL, SETTLEMENT_BATCH_RESP_DESC, SUBSTR(SETTLEMENT_BATCH_RESP_DESC || ' => ' || TRIM(pv_authority_resp_desc) , 1, 2000))
         WHERE SB.SETTLEMENT_BATCH_ID = pn_settlement_batch_id;                 
    END;
    
    PROCEDURE UPDATE_BATCH_FEEDBACK(
        pv_merchant_cd PSS.MERCHANT.MERCHANT_CD%TYPE,
        pv_terminal_cd PSS.TERMINAL.TERMINAL_CD%TYPE,
        pv_terminal_batch_num PSS.TERMINAL_BATCH.TERMINAL_BATCH_NUM%TYPE,
        pd_auth_authority_ts PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_END_TS%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pv_authority_resp_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pn_settlement_batch_id OUT PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_terminal_batch_id OUT PSS.SETTLEMENT_BATCH.TERMINAL_BATCH_ID%TYPE)
    IS
        ln_settlement_batch_state_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_STATE_ID%TYPE;
    BEGIN
        SELECT SETTLEMENT_BATCH_ID, TERMINAL_BATCH_ID, SETTLEMENT_BATCH_STATE_ID
          INTO pn_settlement_batch_id, pn_terminal_batch_id, ln_settlement_batch_state_id
          FROM (SELECT SB.SETTLEMENT_BATCH_ID, SB.TERMINAL_BATCH_ID, SB.SETTLEMENT_BATCH_STATE_ID
          FROM PSS.SETTLEMENT_BATCH SB
          JOIN PSS.TERMINAL_BATCH TB ON SB.TERMINAL_BATCH_ID = TB.TERMINAL_BATCH_ID
          JOIN PSS.TERMINAL T ON TB.TERMINAL_ID = T.TERMINAL_ID
          JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
         WHERE M.MERCHANT_CD = pv_merchant_cd
           AND T.TERMINAL_CD = pv_terminal_cd
           AND TB.TERMINAL_BATCH_NUM = pv_terminal_batch_num
           AND TB.TERMINAL_BATCH_CLOSE_TS IS NOT NULL
         ORDER BY DECODE(SB.SETTLEMENT_BATCH_STATE_ID, 7, 1, 5), TB.TERMINAL_BATCH_CYCLE_NUM DESC, SB.SETTLEMENT_BATCH_ID DESC)
         WHERE ROWNUM = 1; 
           
        IF ln_settlement_batch_state_id = 1 OR (ln_settlement_batch_state_id IN(1,2,3,7) AND pc_auth_result_cd IN('P')) THEN
            RETURN;
        END IF;
        
        UPDATE PSS.SETTLEMENT_BATCH SB
           SET SB.SETTLEMENT_BATCH_STATE_ID = DECODE(pc_auth_result_cd, 'Y', 1, 'P', 7, 'N', 2, 'F', 3, 'O', 2, 'R', 2),
               SB.SETTLEMENT_BATCH_END_TS = NVL(pd_auth_authority_ts, SYSDATE),
               SB.SETTLEMENT_BATCH_RESP_DESC = DECODE(TRIM(pv_authority_resp_desc), NULL, SETTLEMENT_BATCH_RESP_DESC, SUBSTR(SETTLEMENT_BATCH_RESP_DESC || ' => ' || TRIM(pv_authority_resp_desc) , 1, 2000))
         WHERE SB.SETTLEMENT_BATCH_ID = pn_settlement_batch_id;                 
    END;
   
    -- R37 and above signature 
    PROCEDURE UPDATE_TRAN_FEEDBACK(
        pn_terminal_batch_id PSS.SETTLEMENT_BATCH.TERMINAL_BATCH_ID%TYPE,
        pn_settlement_batch_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_sale_auth_id PSS.AUTH.AUTH_ID%TYPE,
        pn_refund_id PSS.REFUND.REFUND_ID%TYPE,
        pc_tran_type_cd CHAR,
        pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
        pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pv_authority_response_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
        pv_authority_response_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
        pd_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
        pn_approved_amt PSS.AUTH.AUTH_AMT_APPROVED%TYPE,
        pn_sale_amt PSS.AUTH.AUTH_AMT%TYPE,
        pc_major_currency_used CHAR,
        pv_tran_global_trans_cd OUT PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
		pc_settle_update_needed OUT VARCHAR2,
		pc_auth_type_cd OUT VARCHAR2,
        pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
        pn_override_trans_type_id OUT PSS.PAYMENT_SUBTYPE.TRANS_TYPE_ID%TYPE)
    IS
        ln_original_auth_id PSS.AUTH.AUTH_ID%TYPE;
        ln_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE;
        lc_entry_method_cd PSS.AUTH.ACCT_ENTRY_METHOD_CD%TYPE;
        lv_trace_number PSS.AUTH.TRACE_NUMBER%TYPE;
        ln_auth_id PSS.AUTH.AUTH_ID%TYPE;
        ln_refund_id PSS.REFUND.REFUND_ID%TYPE;
        lc_imported PSS.SALE.IMPORTED%TYPE;
        lc_sale_type_cd PSS.SALE.SALE_TYPE_CD%TYPE;
        ln_sale_result_id PSS.SALE.SALE_RESULT_ID%TYPE;
        pc_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
		lc_acct_entry_method_cd PSS.AUTH.ACCT_ENTRY_METHOD_CD%TYPE;
    BEGIN
		pc_settle_update_needed := 'Y';
        IF pn_sale_auth_id IS NOT NULL THEN
            ln_auth_id := pn_sale_auth_id;
        ELSIF pn_refund_id IS NOT NULL THEN
            ln_refund_id := pn_refund_id;
        ELSIF pc_tran_type_cd = '+' THEN
            SELECT AUTH_ID
              INTO ln_auth_id
              FROM (SELECT SA.AUTH_ID
              FROM PSS.AUTH A
              JOIN PSS.AUTH SA ON A.TRAN_ID = SA.TRAN_ID
             WHERE SA.TERMINAL_BATCH_ID = pn_terminal_batch_id
               AND A.AUTH_AUTHORITY_TRAN_CD = pv_authority_tran_cd 
               AND A.AUTH_AUTHORITY_REF_CD = pv_authority_ref_cd
               AND SA.AUTH_TYPE_CD IN('U','S','O','A','D','C','E','V','I')
               AND A.AUTH_TYPE_CD IN('L','N')
             ORDER BY SA.AUTH_TS DESC, SA.AUTH_ID DESC)
             WHERE ROWNUM = 1;
        ELSIF pc_tran_type_cd = '-' THEN
            SELECT REFUND_ID
              INTO ln_refund_id
              FROM (SELECT R.REFUND_ID, R.TRAN_ID
                      FROM PSS.REFUND R
                     WHERE R.TERMINAL_BATCH_ID = pn_terminal_batch_id
                       AND R.REFUND_AUTHORITY_TRAN_CD = pv_authority_tran_cd 
                       AND R.REFUND_AUTHORITY_REF_CD = pv_authority_ref_cd
                       AND R.REFUND_STATE_ID IN(6, 1)
                     ORDER BY R.CREATED_TS DESC, R.REFUND_ID DESC)
             WHERE ROWNUM = 1;
        ELSE
            RAISE_APPLICATION_ERROR(-20558, 'Invalid tran type cd "' ||  pc_tran_type_cd + '"');
        END IF;
        
        IF ln_auth_id IS NOT NULL THEN
            IF pc_major_currency_used = 'Y' THEN
                ln_minor_currency_factor := 1;
            ELSE
                SELECT C.MINOR_CURRENCY_FACTOR
                  INTO ln_minor_currency_factor
                  FROM PSS.CURRENCY C
                  JOIN PSS.POS_PTA PTA ON C.CURRENCY_CD = NVL(pta.CURRENCY_CD, 'USD')
                  JOIN PSS.TRAN X ON PTA.POS_PTA_ID = X.POS_PTA_ID
                  JOIN PSS.AUTH A ON A.TRAN_ID = X.TRAN_ID
                 WHERE A.AUTH_ID = ln_auth_id;
            END IF;
            UPDATE PSS.AUTH 
               SET AUTH_STATE_ID = DECODE(pc_auth_result_cd, 'Y', 2, 'P', 5, 'N', 6, 'O', 6, 'F', 6, 'R', 6),
                   AUTH_RESULT_CD = pc_auth_result_cd,
                   AUTH_RESP_CD = NVL(pv_authority_response_cd, AUTH_RESP_CD),
                   AUTH_RESP_DESC = NVL(pv_authority_response_desc, AUTH_RESP_DESC),
                   AUTH_AUTHORITY_TRAN_CD = NVL(pv_authority_tran_cd, AUTH_AUTHORITY_TRAN_CD),
                   AUTH_AUTHORITY_REF_CD = NVL(pv_authority_ref_cd, AUTH_AUTHORITY_REF_CD),
                   AUTH_AUTHORITY_TS = NVL(pd_authority_ts, AUTH_AUTHORITY_TS),
                   AUTH_AMT_APPROVED = NVL(pn_approved_amt / ln_minor_currency_factor, AUTH_AMT_APPROVED),
                   AUTH_AUTHORITY_MISC_DATA = NVL(pv_authority_misc_data, AUTH_AUTHORITY_MISC_DATA)
             WHERE AUTH_ID = ln_auth_id
             RETURNING TRAN_ID, AUTH_TYPE_CD, OVERRIDE_TRANS_TYPE_ID, ACCT_ENTRY_METHOD_CD INTO pn_tran_id, pc_auth_type_cd, pn_override_trans_type_id, lc_acct_entry_method_cd;
			IF pn_settlement_batch_id IS NOT NULL THEN
                UPDATE PSS.TRAN_SETTLEMENT_BATCH
                   SET TRAN_SETTLEMENT_B_AMT = NVL(pn_approved_amt / ln_minor_currency_factor, TRAN_SETTLEMENT_B_AMT),
                       TRAN_SETTLEMENT_B_RESP_CD = NVL(pv_authority_response_cd, TRAN_SETTLEMENT_B_RESP_CD),
                       TRAN_SETTLEMENT_B_RESP_DESC = NVL(pv_authority_response_desc, TRAN_SETTLEMENT_B_RESP_DESC)
                 WHERE SETTLEMENT_BATCH_ID = pn_settlement_batch_id
                   AND AUTH_ID = ln_auth_id;               
            END IF;
        ELSIF ln_refund_id IS NOT NULL THEN
            IF pc_major_currency_used = 'Y' THEN
                ln_minor_currency_factor := 1;
            ELSE
                SELECT C.MINOR_CURRENCY_FACTOR
                  INTO ln_minor_currency_factor
                  FROM PSS.CURRENCY C
                  JOIN PSS.POS_PTA PTA ON C.CURRENCY_CD = NVL(pta.CURRENCY_CD, 'USD')
                  JOIN PSS.TRAN X ON PTA.POS_PTA_ID = X.POS_PTA_ID
                  JOIN PSS.REFUND R ON X.TRAN_ID = R.TRAN_ID
                 WHERE R.REFUND_ID = ln_refund_id;
            END IF;          
            UPDATE PSS.REFUND
               SET REFUND_STATE_ID = DECODE(pc_auth_result_cd, 'Y', 1, 'P', 1, 'N', 2, 'O', 2, 'F', 3, 'R', 2),
                REFUND_RESP_CD = NVL(pv_authority_response_cd, REFUND_RESP_CD),
                REFUND_RESP_DESC = NVL(pv_authority_response_desc, REFUND_RESP_DESC),
                REFUND_AUTHORITY_TS = NVL(pd_authority_ts, REFUND_AUTHORITY_TS),
                REFUND_AUTHORITY_MISC_DATA = NVL(pv_authority_misc_data, REFUND_AUTHORITY_MISC_DATA)
             WHERE REFUND_ID = ln_refund_id
             RETURNING TRAN_ID, REFUND_TYPE_CD, OVERRIDE_TRANS_TYPE_ID INTO pn_tran_id, pc_auth_type_cd, pn_override_trans_type_id;
            IF pn_settlement_batch_id IS NOT NULL THEN 
                UPDATE PSS.REFUND_SETTLEMENT_BATCH
                   SET REFUND_SETTLEMENT_B_AMT = NVL(pn_approved_amt / ln_minor_currency_factor, REFUND_SETTLEMENT_B_AMT),
                       REFUND_SETTLEMENT_B_RESP_CD = NVL(pv_authority_response_cd, REFUND_SETTLEMENT_B_RESP_CD),
                       REFUND_SETTLEMENT_B_RESP_DESC = NVL(pv_authority_response_desc, REFUND_SETTLEMENT_B_RESP_DESC)
                 WHERE SETTLEMENT_BATCH_ID = pn_settlement_batch_id
                   AND REFUND_ID = ln_refund_id;   
            END IF;
        END IF;
		
		IF lc_acct_entry_method_cd IS NOT NULL THEN
			SELECT COALESCE(MAX(OVERRIDE_TRANS_TYPE_ID), pn_override_trans_type_id)
			INTO pn_override_trans_type_id
			FROM PSS.TRANS_TYPE_ENTRY_OVERRIDE
			WHERE TRANS_TYPE_ID = pn_override_trans_type_id AND ACCT_ENTRY_METHOD_CD = lc_acct_entry_method_cd;
		END IF;
        
        -- update tran_state_cd
        UPDATE PSS.TRAN
           SET TRAN_STATE_CD = AFTER_SETTLE_TRAN_STATE_CD(DECODE(ln_auth_id, NULL, NULL, pc_auth_type_cd), DECODE(ln_auth_id, NULL,pc_auth_type_cd), pc_auth_result_cd, TRAN_DEVICE_RESULT_TYPE_CD)
         WHERE TRAN_ID = pn_tran_id
         RETURNING TRAN_GLOBAL_TRANS_CD INTO pv_tran_global_trans_cd;
         
         select max(s.imported), max(s.SALE_TYPE_CD), max(s.sale_result_id), max(x.tran_state_cd) 
        into lc_imported,lc_sale_type_cd,ln_sale_result_id,pc_tran_state_cd
        from pss.tran x left outer join pss.sale s on x.tran_id=s.tran_id 
        where x.tran_id=pn_tran_id;
        IF  (lc_imported is NULL or lc_imported != '-') AND lc_sale_type_cd IN(PKG_CONST.SALE_TYPE__ACTUAL, PKG_CONST.SALE_TYPE__CASH) AND ln_sale_result_id = 0 AND pc_tran_state_cd NOT IN('F', 'G', 'Z') THEN
            pc_settle_update_needed := 'Y';
        ELSE
            pc_settle_update_needed := 'N';
        END IF;
    END;
    
    -- R36 and below
    PROCEDURE UPDATE_TRAN_FEEDBACK(
        pn_terminal_batch_id PSS.SETTLEMENT_BATCH.TERMINAL_BATCH_ID%TYPE,
        pn_settlement_batch_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_sale_auth_id PSS.AUTH.AUTH_ID%TYPE,
        pn_refund_id PSS.REFUND.REFUND_ID%TYPE,
        pc_tran_type_cd CHAR,
        pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
        pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pv_authority_response_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
        pv_authority_response_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
        pd_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
        pn_approved_amt PSS.AUTH.AUTH_AMT_APPROVED%TYPE,
        pn_sale_amt PSS.AUTH.AUTH_AMT%TYPE,
        pc_major_currency_used CHAR,
        pv_tran_global_trans_cd OUT PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
		pc_settle_update_needed OUT VARCHAR2,
		pc_auth_type_cd OUT VARCHAR2,
        pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE)
    IS
        ln_override_trans_type_id PSS.PAYMENT_SUBTYPE.TRANS_TYPE_ID%TYPE;
    BEGIN
        UPDATE_TRAN_FEEDBACK(
            pn_terminal_batch_id,
            pn_settlement_batch_id,
            pn_sale_auth_id,
            pn_refund_id,
            pc_tran_type_cd,
            pv_authority_tran_cd,
            pv_authority_ref_cd,
            pc_auth_result_cd,
            pv_authority_response_cd,
            pv_authority_response_desc,
            pv_authority_misc_data,
            pd_authority_ts,
            pn_approved_amt,
            pn_sale_amt,
            pc_major_currency_used,
            pv_tran_global_trans_cd,
            pc_settle_update_needed,
            pc_auth_type_cd ,
            pn_tran_id,
            ln_override_trans_type_id);
    END;
    
END PKG_SETTLEMENT;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/PSS/UPDATE_DFR_SALE.prc?revision=1.12
CREATE OR REPLACE PROCEDURE PSS.UPDATE_DFR_SALE (
    pv_merchant_order_num VARCHAR2,
    pc_action_code CHAR,
    pn_amount PSS.AUTH.AUTH_AMT%TYPE,
    pn_file_cache_id PSS.AUTH.DFR_FILE_CACHE_ID%TYPE,
    pc_card_type_cd PSS.AUTH.CARD_TYPE_CD%TYPE,
    pc_interchange PSS.AUTH.INTERCHANGE_CD%TYPE,
    pv_fee_desc PSS.AUTH.FEE_DESC%TYPE,
    pn_fee_amount PSS.AUTH.FEE_AMOUNT%TYPE,
    pd_batch_date PSS.AUTH.BATCH_DATE%TYPE,
    pn_entity_num PSS.AUTH.ENTITY_NUM%TYPE,
    pv_submission_num PSS.AUTH.SUBMISSION_NUM%TYPE,
    pv_record_num PSS.AUTH.RECORD_NUM%TYPE,
    pn_line_number REPORT.FILE_CACHE_ERROR.LINE_NUMBER%TYPE,
    pc_sub_action_code CHAR DEFAULT '-')
IS
    ln_tran_id PSS.TRAN.TRAN_ID%TYPE;
    ln_auth_id PSS.AUTH.AUTH_ID%TYPE;
    lc_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
    lc_new_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE := NULL;
    lc_match_flag CHAR(1);
    lc_cancel_flag CHAR(1);
    ln_auth_amount PSS.AUTH.AUTH_AMT%TYPE;
    ln_reverse_tran_id PSS.TRAN.TRAN_ID%TYPE;
    ld_current_ts DATE;
    lc_entry_method_cd PSS.AUTH.ACCT_ENTRY_METHOD_CD%TYPE;
    ln_duplicate_completion_id PSS.DUPLICATE_COMPLETION.DUPLICATE_COMPLETION_ID%TYPE;
    ln_override_trans_type_id PSS.AUTH.OVERRIDE_TRANS_TYPE_ID%TYPE;
    ln_host_id HOST.HOST_ID%TYPE;
    ln_device_id DEVICE.DEVICE_ID%TYPE;
    ln_reverse_amount PSS.AUTH.AUTH_AMT%TYPE;
    lv_tran_global_trans_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE;
    lc_report_trans_update_needed CHAR(1) := 'Y';
BEGIN
    -- find trans and auth or refund
    IF pc_action_code = 'D' THEN
        BEGIN
            SELECT *
              INTO ln_tran_id,
                   ln_auth_id,
                   lc_tran_state_cd,
                   lc_match_flag,
                   lc_cancel_flag,
                   ln_auth_amount,
                   lc_entry_method_cd,
                   ln_override_trans_type_id,
                   lv_tran_global_trans_cd
              FROM (
            SELECT /*+INDEX(X IDX_DEVICE_TRAN_CD )*/ X.TRAN_ID,
                   A.AUTH_ID,
                   X.TRAN_STATE_CD,
                   CASE WHEN pc_sub_action_code = 'E' THEN 'N'
                   		WHEN A.SUBMISSION_NUM = pv_submission_num AND A.RECORD_NUM = pv_record_num THEN 'Y' 
                        WHEN A.SUBMISSION_NUM IS NOT NULL AND A.RECORD_NUM IS NOT NULL AND pv_submission_num IS NOT NULL AND pv_record_num IS NOT NULL THEN 'D'
                        ELSE 'N' END MATCH_FLAG,
                   CASE WHEN A.AUTH_AMT = 0 THEN 'Y' ELSE 'N' END CANCEL_FLAG,
                   A.AUTH_AMT,
                   A.ACCT_ENTRY_METHOD_CD,
                   A.OVERRIDE_TRANS_TYPE_ID,
                   X.TRAN_GLOBAL_TRANS_CD
              FROM PSS.TRAN X
              JOIN PSS.AUTH A ON X.TRAN_ID = A.TRAN_ID AND A.AUTH_TYPE_CD NOT IN('N', 'L')
             WHERE X.TRAN_DEVICE_TRAN_CD = REGEXP_REPLACE(pv_merchant_order_num, '[^-]+-(.*)', '\1') 
               AND X.TRAN_GLOBAL_TRANS_CD LIKE 'A:' || REPLACE(pv_merchant_order_num, '-', ':') || '%'
             ORDER BY 4 DESC, CASE WHEN A.AUTH_AMT = pn_amount THEN 1 ELSE 2 END, CASE WHEN A.AUTH_STATE_ID IN(2, 5) THEN 1 WHEN A.AUTH_STATE_ID = 4 THEN 2 ELSE 3 END, A.AUTH_AUTHORITY_TS DESC, A.AUTH_ID DESC)
             WHERE ROWNUM = 1;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                INSERT INTO REPORT.FILE_CACHE_ERROR(FILE_CACHE_ERROR_ID, FILE_CACHE_ID, LINE_NUMBER, REFERENCE_VALUE, ERROR_TEXT)
                    SELECT REPORT.SEQ_FILE_CACHE_ERROR_ID.NEXTVAL, pn_file_cache_id, pn_line_number, pv_merchant_order_num, 'Cannot find sale A:' || REPLACE(pv_merchant_order_num, '-', ':') || ' for $' || pn_amount FROM DUAL;
                RETURN;
        END;
    ELSIF pc_action_code = 'R' THEN
        BEGIN
            SELECT *
              INTO ln_tran_id,
                   ln_auth_id,
                   lc_tran_state_cd,
                   lc_match_flag,
                   lc_cancel_flag,
                   ln_auth_amount,
                   lc_entry_method_cd,
                   ln_override_trans_type_id,
                   lv_tran_global_trans_cd
              FROM (
            SELECT X.TRAN_ID,
                   R.REFUND_ID,
                   X.TRAN_STATE_CD,
                   CASE WHEN pc_sub_action_code = 'E' THEN 'N'
                   		WHEN R.SUBMISSION_NUM = pv_submission_num AND R.RECORD_NUM = pv_record_num THEN 'Y'
                        WHEN R.SUBMISSION_NUM IS NOT NULL AND R.RECORD_NUM IS NOT NULL AND pv_submission_num IS NOT NULL AND pv_record_num IS NOT NULL THEN 'D'
                        ELSE 'N' END MATCH_FLAG,
                   CASE WHEN R.REFUND_AMT = 0 THEN 'Y' ELSE 'N' END CANCEL_FLAG,
                   R.REFUND_AMT,
                   R.ACCT_ENTRY_METHOD_CD,
                   R.OVERRIDE_TRANS_TYPE_ID,
                   X.TRAN_GLOBAL_TRANS_CD
              FROM PSS.TRAN X
              JOIN PSS.REFUND R ON X.TRAN_ID = R.TRAN_ID
             WHERE X.TRAN_GLOBAL_TRANS_CD IN('RF:' || REPLACE(pv_merchant_order_num, '-', ':'), 'RV:' || REPLACE(pv_merchant_order_num, '-', ':')) 
             ORDER BY 4 DESC, CASE WHEN R.REFUND_AMT = pn_amount THEN 1 ELSE 2 END, CASE WHEN R.REFUND_STATE_ID IN(1) THEN 1 WHEN R.REFUND_STATE_ID = 3 THEN 2 ELSE 3 END, R.REFUND_AUTHORITY_TS DESC, R.REFUND_ID DESC)
             WHERE ROWNUM = 1;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                INSERT INTO REPORT.FILE_CACHE_ERROR(FILE_CACHE_ERROR_ID, FILE_CACHE_ID, LINE_NUMBER, REFERENCE_VALUE, ERROR_TEXT)
                    SELECT REPORT.SEQ_FILE_CACHE_ERROR_ID.NEXTVAL, pn_file_cache_id, pn_line_number, pv_merchant_order_num, 'Cannot find refund RF:' || REPLACE(pv_merchant_order_num, '-', ':') || ' for $' || ABS(pn_amount) FROM DUAL;
                RETURN;
        END;
    ELSE
        --RAISE_APPLICATION_ERROR(-20170, 'Invalid action code "' || pc_action_code ||'" for merchant order ' || pv_merchant_order_num);
        INSERT INTO REPORT.FILE_CACHE_ERROR(FILE_CACHE_ERROR_ID, FILE_CACHE_ID, LINE_NUMBER, REFERENCE_VALUE, ERROR_TEXT)
            SELECT REPORT.SEQ_FILE_CACHE_ERROR_ID.NEXTVAL, pn_file_cache_id, pn_line_number, pv_merchant_order_num, 'Invalid action code "' || pc_action_code ||'" for merchant order ' || pv_merchant_order_num FROM DUAL;
        RETURN;
    END IF;
    IF lc_tran_state_cd NOT IN('D', 'C', 'V', 'A', 'B', 'E', 'I', 'J', 'X', 'Y') THEN
        --RAISE_APPLICATION_ERROR(-20172, 'Transaction '||ln_tran_id||' is in an invalid state "' || lc_tran_state_cd ||'"');
        INSERT INTO REPORT.FILE_CACHE_ERROR(FILE_CACHE_ERROR_ID, FILE_CACHE_ID, LINE_NUMBER, REFERENCE_VALUE, ERROR_TEXT)
            SELECT REPORT.SEQ_FILE_CACHE_ERROR_ID.NEXTVAL, pn_file_cache_id, pn_line_number, pv_merchant_order_num, 'Transaction '||ln_tran_id||' is in an invalid state "' || lc_tran_state_cd ||'"' FROM DUAL;
        RETURN;
    ELSIF ABS(ln_auth_amount) != ABS(pn_amount) THEN
        --RAISE_APPLICATION_ERROR(-20173, 'Transaction '||ln_tran_id||' amount does not match (' || ln_auth_amount ||' vs ' || pn_amount || ')');
        INSERT INTO REPORT.FILE_CACHE_ERROR(FILE_CACHE_ERROR_ID, FILE_CACHE_ID, LINE_NUMBER, REFERENCE_VALUE, ERROR_TEXT)
            SELECT REPORT.SEQ_FILE_CACHE_ERROR_ID.NEXTVAL, pn_file_cache_id, pn_line_number, pv_merchant_order_num, 'Transaction '||ln_tran_id||' amount does not match (' || ln_auth_amount ||' vs ' || pn_amount || ')' FROM DUAL;
        RETURN;        
    END IF;
    IF lc_match_flag = 'D' AND pn_amount != 0 THEN
        -- already updated but does not match
        BEGIN
            SELECT PSS.SEQ_DUPLICATE_COMPLETION_ID.NEXTVAL
              INTO ln_duplicate_completion_id
              FROM DUAL;
            INSERT INTO PSS.DUPLICATE_COMPLETION(DUPLICATE_COMPLETION_ID, TRAN_ID, SIGN_CD, AUTH_ID, DFR_FILE_CACHE_ID, SUBMISSION_NUM, RECORD_NUM,  
                  CARD_TYPE_CD, INTERCHANGE_CD, FEE_DESC, FEE_AMOUNT, BATCH_DATE, ENTITY_NUM)
                SELECT ln_duplicate_completion_id, ln_tran_id, CASE WHEN pc_action_code = 'D' THEN '+' ELSE '-' END, ln_auth_id, pn_file_cache_id, pv_submission_num, pv_record_num,
                       pc_card_type_cd, pc_interchange, pv_fee_desc,  pn_fee_amount, pd_batch_date, pn_entity_num  
                  FROM DUAL;
        EXCEPTION
            WHEN DUP_VAL_ON_INDEX THEN
                RETURN; -- already processed this
        END;
        -- find sum of already reversed
        SELECT -pn_amount * COUNT_DUPS + NVL(EXISTING_REFUND_AMOUNT, 0) - NVL(EXISTING_CHARGE_AMOUNT, 0)
          INTO ln_reverse_amount
          FROM (SELECT COUNT(DISTINCT DUPLICATE_COMPLETION_ID) COUNT_DUPS
          FROM PSS.DUPLICATE_COMPLETION
         WHERE TRAN_ID = ln_tran_id)
         CROSS JOIN (SELECT SUM(ABS(R.REFUND_AMT)) EXISTING_REFUND_AMOUNT
          FROM PSS.REFUND R
          JOIN PSS.TRAN X ON R.TRAN_ID = X.TRAN_ID
         WHERE X.PARENT_TRAN_ID = ln_tran_id)
         CROSS JOIN (SELECT SUM(ABS(A.AUTH_AMT)) EXISTING_CHARGE_AMOUNT
          FROM PSS.AUTH A
          JOIN PSS.TRAN X ON A.TRAN_ID = X.TRAN_ID
         WHERE X.PARENT_TRAN_ID = ln_tran_id
           AND A.AUTH_TYPE_CD NOT IN('N', 'L'));
        
        IF ln_reverse_amount != 0 THEN 
        -- create tran to reverse it
        SELECT PSS.SEQ_TRAN_ID.NEXTVAL, SYSDATE
          INTO ln_reverse_tran_id, ld_current_ts
          FROM DUAL; 
        INSERT INTO PSS.TRAN (
                TRAN_ID,
                PARENT_TRAN_ID,
                TRAN_START_TS,
                TRAN_END_TS,
                TRAN_UPLOAD_TS,
                TRAN_GLOBAL_TRANS_CD,
                TRAN_STATE_CD,
                CONSUMER_ACCT_ID,
                TRAN_DEVICE_TRAN_CD,
                POS_PTA_ID,
                TRAN_DEVICE_RESULT_TYPE_CD,
                TRAN_RECEIVED_RAW_ACCT_DATA,
                PAYMENT_SUBTYPE_KEY_ID,
                PAYMENT_SUBTYPE_CLASS,
                CLIENT_PAYMENT_TYPE_CD,
                DEVICE_NAME)
         SELECT ln_reverse_tran_id,
                ln_tran_id,
                O.TRAN_START_TS,
                O.TRAN_END_TS,
                NULL, /* Must be NULL so that it will NOT be imported */
                'RV' || SUBSTR(O.TRAN_GLOBAL_TRANS_CD, INSTR(O.TRAN_GLOBAL_TRANS_CD, ':'), 56) || ':' || (SELECT COUNT(*) + 1 FROM PSS.TRAN C WHERE O.TRAN_ID = C.PARENT_TRAN_ID), 
                '8',
                O.CONSUMER_ACCT_ID,
                O.TRAN_DEVICE_TRAN_CD,
                O.POS_PTA_ID,
                O.TRAN_DEVICE_RESULT_TYPE_CD,
                O.TRAN_RECEIVED_RAW_ACCT_DATA,
                O.PAYMENT_SUBTYPE_KEY_ID,
                O.PAYMENT_SUBTYPE_CLASS,
                O.CLIENT_PAYMENT_TYPE_CD,
                O.DEVICE_NAME
           FROM PSS.TRAN O
          WHERE O.TRAN_ID = ln_tran_id;
        IF pc_action_code = 'D' AND ln_reverse_amount < 0 THEN
            INSERT INTO PSS.REFUND (
                TRAN_ID,
                REFUND_AMT,
                REFUND_DESC,
                REFUND_ISSUE_TS,
                REFUND_ISSUE_BY,
                REFUND_TYPE_CD,
                REFUND_STATE_ID,
                ACCT_ENTRY_METHOD_CD,
                OVERRIDE_TRANS_TYPE_ID)
              VALUES (
                ln_reverse_tran_id,
                ln_reverse_amount,
                'Reversal of Duplicate Submission',
                ld_current_ts,
                'PSS',
                'R',
                6,
                lc_entry_method_cd,
                ln_override_trans_type_id);
        ELSIF ln_reverse_amount > 0 THEN
            INSERT INTO PSS.AUTH (
                TRAN_ID,
                AUTH_STATE_ID,
                AUTH_TYPE_CD,
                ACCT_ENTRY_METHOD_CD,
                AUTH_TS,
                AUTH_AMT,
                TRACE_NUMBER,
                CARD_KEY,
                OVERRIDE_TRANS_TYPE_ID)
             SELECT
                ln_reverse_tran_id, /* TRAN_ID */
                6, /* AUTH_STATE_ID */
                'E', /* AUTH_TYPE_CD */
                lc_entry_method_cd, /* ACCT_ENTRY_METHOD_CD */
                ld_current_ts, /* AUTH_TS */
                ln_reverse_amount, /* AUTH_AMT */
                ln_auth_id, /* TRACE_NUMBER */
                MAX(A.CARD_KEY),
                ln_override_trans_type_id
              FROM PSS.TRAN X
              JOIN PSS.AUTH A ON X.PARENT_TRAN_ID = A.TRAN_ID
             WHERE A.AUTH_TYPE_CD = 'N'
               AND X.TRAN_ID = ln_tran_id;
        END IF;
        -- Insert line item
        SELECT P.DEVICE_ID
          INTO ln_device_id
          FROM PSS.TRAN O
          JOIN PSS.POS_PTA PP ON O.POS_PTA_ID = PP.POS_PTA_ID
          JOIN PSS.POS P ON PP.POS_ID = P.POS_ID
         WHERE O.TRAN_ID = ln_tran_id;
        ln_host_id := GET_OR_CREATE_HOST_ID(ln_device_id, 0, 0);
        INSERT INTO pss.tran_line_item (
            tran_line_item_id,
            tran_id,
            tran_line_item_amount,
            tran_line_item_position_cd,
            tran_line_item_tax,
            tran_line_item_type_id,
            tran_line_item_quantity,
            tran_line_item_desc,
            host_id,
            tran_line_item_batch_type_cd,
            tran_line_item_ts,
            sale_result_id)
          VALUES(
            PSS.SEQ_TLI_ID.NEXTVAL,
            ln_reverse_tran_id,
            ln_reverse_amount,
            NULL,
            NULL,
            505,
            1,
            'Reversal of Duplicate Submission ' || ln_duplicate_completion_id,
            ln_host_id,
            'A',
            ld_current_ts,
            0);
        END IF;
    END IF;
    IF lc_match_flag = 'Y' THEN
    	RETURN;
    END IF;
    IF pc_action_code = 'D' THEN
        UPDATE PSS.AUTH 
           SET DFR_FILE_CACHE_ID = pn_file_cache_id,
               CARD_TYPE_CD = pc_card_type_cd,
               INTERCHANGE_CD = pc_interchange,
               FEE_DESC = pv_fee_desc,
               FEE_AMOUNT = pn_fee_amount,
               BATCH_DATE = pd_batch_date,
               ENTITY_NUM = pn_entity_num,
               SUBMISSION_NUM = pv_submission_num,
               RECORD_NUM = pv_record_num,
               AUTH_STATE_ID = CASE pc_sub_action_code WHEN 'E' THEN 8 ELSE AUTH_STATE_ID END
         WHERE AUTH_ID = ln_auth_id;
    ELSE
        UPDATE PSS.REFUND
           SET DFR_FILE_CACHE_ID = pn_file_cache_id,
               CARD_TYPE_CD = pc_card_type_cd,
               INTERCHANGE_CD = pc_interchange,
               FEE_DESC = pv_fee_desc,
               FEE_AMOUNT = pn_fee_amount,
               BATCH_DATE = pd_batch_date,
               ENTITY_NUM = pn_entity_num,
               SUBMISSION_NUM = pv_submission_num,
               RECORD_NUM = pv_record_num,
               REFUND_STATE_ID = CASE pc_sub_action_code WHEN 'E' THEN 7 ELSE REFUND_STATE_ID END
         WHERE REFUND_ID = ln_auth_id;
    END IF;
    IF SQL%ROWCOUNT != 1 THEN
        --RAISE_APPLICATION_ERROR(-20171, 'Transaction ' || ln_tran_id ||' is being updated by another process');
        INSERT INTO REPORT.FILE_CACHE_ERROR(FILE_CACHE_ERROR_ID, FILE_CACHE_ID, LINE_NUMBER, REFERENCE_VALUE, ERROR_TEXT)
            SELECT REPORT.SEQ_FILE_CACHE_ERROR_ID.NEXTVAL, pn_file_cache_id, pn_line_number, pv_merchant_order_num, 'Transaction ' || ln_tran_id ||' is being updated by another process' FROM DUAL;
        RETURN;
    END IF;
    IF lc_tran_state_cd IN('A', 'B', 'E', 'I', 'J', 'X') THEN
    	IF lc_cancel_flag = 'Y' THEN 
    		lc_new_tran_state_cd := 'C';
    	ELSE
    		lc_new_tran_state_cd := 'D';    	    	
	        
    		IF lc_tran_state_cd = 'X' THEN
	        	-- update settlement state
	        	BEGIN
		        	lc_report_trans_update_needed := 'N';
	        		REPORT.DATA_IN_PKG.UPDATE_SETTLE_INFO(lv_tran_global_trans_cd, 'PSS', 3, SYSDATE, NULL, NULL, NULL, NULL, ln_tran_id, pv_submission_num);
				EXCEPTION
					WHEN OTHERS THEN
						-- transaction is most likely not in REPORT.TRANS yet, set to PROCESSED_TRAN_DEPOSIT_CONFIRMED to be picked up by UPDATE_DEPOSITED_TRANSACTIONS job later
						lc_new_tran_state_cd := 'Y';
				END;
	        END IF;
        END IF;
    END IF;
    IF lc_report_trans_update_needed = 'Y' AND pv_submission_num IS NOT NULL THEN
		UPDATE REPORT.TRANS
		SET SUBMISSION_NUM = pv_submission_num
		WHERE SOURCE_TRAN_ID = ln_tran_id;
		
		IF SQL%ROWCOUNT = 0 AND (lc_tran_state_cd = 'D' OR lc_new_tran_state_cd = 'D') THEN
			-- transaction is not in REPORT.TRANS yet, set to PROCESSED_TRAN_DEPOSIT_CONFIRMED to be picked up by UPDATE_DEPOSITED_TRANSACTIONS job later
			lc_new_tran_state_cd := 'Y';
		END IF;
	END IF;
	IF lc_new_tran_state_cd IS NOT NULL THEN
		UPDATE PSS.TRAN
        SET TRAN_STATE_CD = lc_new_tran_state_cd
        WHERE TRAN_ID = ln_tran_id
           AND TRAN_STATE_CD != lc_new_tran_state_cd;
	END IF;
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/PSS/UPDATE_DFR_AUTH.prc?revision=1.7
CREATE OR REPLACE PROCEDURE PSS.UPDATE_DFR_AUTH (
    pv_merchant_order_num VARCHAR2,
    pc_action_code CHAR,
    pn_amount PSS.AUTH.AUTH_AMT%TYPE,
    pn_file_cache_id PSS.AUTH.DFR_FILE_CACHE_ID%TYPE,
    pc_card_type_cd PSS.AUTH.CARD_TYPE_CD%TYPE,
    pv_fee_desc PSS.AUTH.FEE_DESC%TYPE,
    pn_fee_amount PSS.AUTH.FEE_AMOUNT%TYPE,
    pd_batch_date PSS.AUTH.BATCH_DATE%TYPE,
    pn_entity_num PSS.AUTH.ENTITY_NUM%TYPE,
    pv_submission_num PSS.AUTH.SUBMISSION_NUM%TYPE,
    pv_record_num PSS.AUTH.RECORD_NUM%TYPE,
    pn_line_number REPORT.FILE_CACHE_ERROR.LINE_NUMBER%TYPE)
IS
    ln_tran_id PSS.TRAN.TRAN_ID%TYPE;
    ln_auth_id PSS.AUTH.AUTH_ID%TYPE;
    lc_match_flag CHAR(1);
BEGIN
    -- find trans and auth or refund
    IF pc_action_code = 'A' THEN
        BEGIN
            SELECT *
              INTO ln_tran_id,
                   ln_auth_id,
                   lc_match_flag
              FROM (
            SELECT /*+INDEX(X IDX_DEVICE_TRAN_CD )*/ X.TRAN_ID,
                   A.AUTH_ID,
                   CASE WHEN A.SUBMISSION_NUM = pv_submission_num AND A.RECORD_NUM = pv_record_num THEN 'Y' 
                        WHEN A.SUBMISSION_NUM IS NOT NULL AND A.RECORD_NUM IS NOT NULL AND pv_submission_num IS NOT NULL AND pv_record_num IS NOT NULL THEN 'D'
                        ELSE 'N' END MATCH_FLAG
              FROM PSS.TRAN X
              JOIN PSS.AUTH A ON X.TRAN_ID = A.TRAN_ID AND A.AUTH_TYPE_CD = 'N'
             WHERE X.TRAN_DEVICE_TRAN_CD = REGEXP_REPLACE(pv_merchant_order_num, '[^-]+-(.*)', '\1') 
               AND X.TRAN_GLOBAL_TRANS_CD LIKE 'A:' || REPLACE(pv_merchant_order_num, '-', ':') || '%'
             ORDER BY 3 DESC, CASE WHEN A.AUTH_STATE_ID IN(2, 5) THEN 1 WHEN A.AUTH_STATE_ID = 4 THEN 2 ELSE 3 END, A.AUTH_AUTHORITY_TS DESC, A.AUTH_ID DESC)
             WHERE ROWNUM = 1;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                INSERT INTO REPORT.FILE_CACHE_ERROR(FILE_CACHE_ERROR_ID, FILE_CACHE_ID, LINE_NUMBER, REFERENCE_VALUE, ERROR_TEXT)
                    SELECT REPORT.SEQ_FILE_CACHE_ERROR_ID.NEXTVAL, pn_file_cache_id, pn_line_number, pv_merchant_order_num, 'Cannot find auth A:' || REPLACE(pv_merchant_order_num, '-', ':') || ' for $' || pn_amount FROM DUAL;
                RETURN;
        END;
        IF lc_match_flag != 'Y' THEN
            UPDATE /*+ index(A PK_AUTH_ID) */ PSS.AUTH A
               SET DFR_FILE_CACHE_ID = pn_file_cache_id,
                   CARD_TYPE_CD = pc_card_type_cd,
                   FEE_DESC = pv_fee_desc,
                   FEE_AMOUNT = pn_fee_amount,
                   BATCH_DATE = pd_batch_date,
                   ENTITY_NUM = pn_entity_num,
                   SUBMISSION_NUM = pv_submission_num,
                   RECORD_NUM = pv_record_num
             WHERE AUTH_ID = ln_auth_id;
        END IF;
    ELSIF TRIM(pv_fee_desc) IS NULL OR pn_fee_amount = 0 THEN
        RETURN; -- early exit
    ELSIF pc_action_code = 'D' THEN
        BEGIN
            SELECT *
              INTO ln_tran_id,
                   ln_auth_id
              FROM (
            SELECT X.TRAN_ID,
                   A.AUTH_ID
              FROM PSS.TRAN X
              JOIN PSS.AUTH A ON X.TRAN_ID = A.TRAN_ID AND A.AUTH_TYPE_CD = 'U'
             WHERE X.TRAN_DEVICE_TRAN_CD = REGEXP_REPLACE(pv_merchant_order_num, '[^-]+-(.*)', '\1') 
               AND X.TRAN_GLOBAL_TRANS_CD LIKE 'A:' || REPLACE(pv_merchant_order_num, '-', ':') || '%'
               AND A.SUBMISSION_NUM = pv_submission_num 
               AND A.RECORD_NUM = pv_record_num
               AND A.PARTIAL_REVERSAL_FLAG = 'Y'
             ORDER BY CASE WHEN A.AUTH_STATE_ID IN(2, 5) THEN 1 WHEN A.AUTH_STATE_ID = 4 THEN 2 ELSE 3 END, A.AUTH_AUTHORITY_TS DESC, A.AUTH_ID DESC)
             WHERE ROWNUM = 1;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                INSERT INTO REPORT.FILE_CACHE_ERROR(FILE_CACHE_ERROR_ID, FILE_CACHE_ID, LINE_NUMBER, REFERENCE_VALUE, ERROR_TEXT)
                    SELECT REPORT.SEQ_FILE_CACHE_ERROR_ID.NEXTVAL, pn_file_cache_id, pn_line_number, pv_merchant_order_num, 'Cannot find sale A:' || REPLACE(pv_merchant_order_num, '-', ':') || ' with partial reversal' FROM DUAL;
                RETURN;
        END;
        UPDATE PSS.AUTH 
           SET FEE_DESC = CASE WHEN FEE_DESC IS NOT NULL THEN FEE_DESC || '; ' END || pv_fee_desc,
               FEE_AMOUNT = NVL(FEE_AMOUNT, 0) + pn_fee_amount
         WHERE AUTH_ID = ln_auth_id
           AND (LENGTH(FEE_DESC) < LENGTH(pv_fee_desc) OR SUBSTR(FEE_DESC, -LENGTH(pv_fee_desc)) != pv_fee_desc);
    ELSE
        --RAISE_APPLICATION_ERROR(-20170, 'Invalid action code "' || pc_action_code ||'" for merchant order ' || pv_merchant_order_num);
        INSERT INTO REPORT.FILE_CACHE_ERROR(FILE_CACHE_ERROR_ID, FILE_CACHE_ID, LINE_NUMBER, REFERENCE_VALUE, ERROR_TEXT)
            SELECT REPORT.SEQ_FILE_CACHE_ERROR_ID.NEXTVAL, pn_file_cache_id, pn_line_number, pv_merchant_order_num, 'Invalid action code "' || pc_action_code ||'" for auth update of merchant order ' || pv_merchant_order_num FROM DUAL;
        RETURN;
    END IF;
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/UPDATE_DEPOSITED_TRANSACTIONS.prc?revision=1.3
CREATE OR REPLACE PROCEDURE REPORT.UPDATE_DEPOSITED_TRANSACTIONS AS 
    CURSOR L_CUR IS
	    SELECT T.TRAN_ID, T.TRAN_GLOBAL_TRANS_CD, S.APP_SETTING_VALUE EFT_AFTER_FUNDING_IND, MAX(A.SUBMISSION_NUM) SUBMISSION_NUM
	    FROM PSS.TRAN T
	    LEFT OUTER JOIN PSS.AUTH A ON T.TRAN_ID = A.TRAN_ID AND A.AUTH_TYPE_CD NOT IN('N', 'L') AND A.SUBMISSION_NUM IS NOT NULL
	    LEFT OUTER JOIN ENGINE.APP_SETTING S ON S.APP_SETTING_CD = UPPER(T.PAYMENT_SUBTYPE_CLASS) || '_EFT_AFTER_FUNDING_IND'
	    WHERE T.TRAN_STATE_CD = 'Y'
	    GROUP BY T.TRAN_ID, T.TRAN_GLOBAL_TRANS_CD, S.APP_SETTING_VALUE;
	
	CURSOR L_CUR_EXPIRED IS
	    SELECT TRAN_ID
	    FROM PSS.TRAN
	    WHERE TRAN_STATE_CD = 'Y' AND LAST_UPDATED_TS < SYSDATE - 90;
	    
	LC_REPORT_TRANS_UPDATED_IND CHAR(1);
BEGIN
    FOR L_REC IN L_CUR LOOP
		LC_REPORT_TRANS_UPDATED_IND := 'N';	
        	
		IF L_REC.EFT_AFTER_FUNDING_IND = 'Y' THEN
			BEGIN
				REPORT.DATA_IN_PKG.UPDATE_SETTLE_INFO(L_REC.TRAN_GLOBAL_TRANS_CD, 'PSS', 3, SYSDATE, NULL, NULL, NULL, NULL, L_REC.TRAN_ID, L_REC.SUBMISSION_NUM);
				LC_REPORT_TRANS_UPDATED_IND := 'Y';
			EXCEPTION
				WHEN OTHERS THEN
					ROLLBACK;
			END;
		ELSE
			UPDATE REPORT.TRANS
			SET SUBMISSION_NUM = L_REC.SUBMISSION_NUM
			WHERE SOURCE_TRAN_ID = L_REC.TRAN_ID;
	
			IF SQL%ROWCOUNT > 0 THEN
				LC_REPORT_TRANS_UPDATED_IND := 'Y';
			END IF;
		END IF;
		
		IF LC_REPORT_TRANS_UPDATED_IND = 'Y' THEN
			UPDATE PSS.TRAN
            SET TRAN_STATE_CD = 'D'
            WHERE TRAN_ID = L_REC.TRAN_ID;
			
            COMMIT;
        END IF;
    END LOOP;
    
    FOR L_REC IN L_CUR_EXPIRED LOOP
    	UPDATE PSS.TRAN
        SET TRAN_STATE_CD = 'E'
        WHERE TRAN_ID = L_REC.TRAN_ID;
		
        COMMIT;
    END LOOP;
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/PSS/VW_REPORTING_TRAN_INFO.vws?revision=1.14
CREATE OR REPLACE FORCE VIEW PSS.VW_REPORTING_TRAN_INFO (TRAN_ID, TRAN_STATE_CD, TRAN_GLOBAL_TRANS_CD, DEVICE_NAME, DEVICE_SERIAL_CD, TRAN_START_TS, TRAN_END_TS, CLIENT_PAYMENT_TYPE_CD, TRAN_UPLOAD_TS, CURRENCY_CD, DEVICE_TYPE_ID, TRAN_RECEIVED_RAW_ACCT_DATA, TRANS_TYPE_ID, CARD_NUMBER, POS_ID, POS_PTA_ID, PAYMENT_SUBTYPE_ID, DEVICE_ID) AS 
  SELECT /*+ index(T PK_TRAN)*/
        T.TRAN_ID,
        T.TRAN_STATE_CD,
        T.TRAN_GLOBAL_TRANS_CD,
        D.DEVICE_NAME,
        D.DEVICE_SERIAL_CD,
        T.TRAN_START_TS,
        T.TRAN_END_TS,
        PST.CLIENT_PAYMENT_TYPE_CD,
        T.TRAN_UPLOAD_TS,
        NVL(PTA.CURRENCY_CD, 'USD') CURRENCY_CD,
        D.DEVICE_TYPE_ID,
        T.TRAN_RECEIVED_RAW_ACCT_DATA,
        COALESCE(SA.OVERRIDE_TRANS_TYPE_ID, AA.OVERRIDE_TRANS_TYPE_ID, R.OVERRIDE_TRANS_TYPE_ID, PST.TRANS_TYPE_ID),
        DECODE(PST.CLIENT_PAYMENT_TYPE_CD,
                    'C', NVL(TRAN_RECEIVED_RAW_ACCT_DATA, '****************'), /*  credit */
                    'R', NVL(TRAN_RECEIVED_RAW_ACCT_DATA, '****************'), /*  credit */
                    'S', REGEXP_REPLACE(TRAN_RECEIVED_RAW_ACCT_DATA, '^\!', ''), /*  pass or access*/
                    'P', REGEXP_REPLACE(TRAN_RECEIVED_RAW_ACCT_DATA, '^\!', ''), /*  pass or access*/
                    TRAN_RECEIVED_RAW_ACCT_DATA
                ) CARD_NUMBER,
       PTA.POS_ID,
       PTA.POS_PTA_ID,
       PTA.PAYMENT_SUBTYPE_ID,
       D.DEVICE_ID		        
     FROM PSS.TRAN T
     JOIN PSS.POS_PTA PTA ON T.POS_PTA_ID = PTA.POS_PTA_ID
     JOIN PSS.PAYMENT_SUBTYPE PST ON PTA.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
     JOIN PSS.POS POS ON PTA.POS_ID = POS.POS_ID
     JOIN DEVICE.DEVICE D ON POS.DEVICE_ID = D.DEVICE_ID
    LEFT OUTER JOIN PSS.AUTH SA ON T.TRAN_ID = SA.TRAN_ID AND SA.AUTH_TYPE_CD IN('U','S','O') AND SA.OVERRIDE_TRANS_TYPE_ID IS NOT NULL
    LEFT OUTER JOIN PSS.AUTH AA ON T.TRAN_ID = AA.TRAN_ID AND AA.AUTH_TYPE_CD IN('L','N') AND AA.OVERRIDE_TRANS_TYPE_ID IS NOT NULL
    LEFT OUTER JOIN PSS.REFUND R ON T.TRAN_ID = R.TRAN_ID AND R.OVERRIDE_TRANS_TYPE_ID IS NOT NULL
    JOIN PSS.TRAN_STATE TS ON T.TRAN_STATE_CD = TS.TRAN_STATE_CD
    WHERE TS.REPORTABLE_IND = 'Y'
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/PSS/VW_REPORTING_TRAN_LINE_ITEM.vws?revision=1.4
CREATE OR REPLACE FORCE VIEW PSS.VW_REPORTING_TRAN_LINE_ITEM (TRAN_LINE_ITEM_AMOUNT, TRAN_LINE_ITEM_QUANTITY, TRAN_LINE_ITEM_TYPE_GROUP_CD, TRAN_ID, TRAN_START_TS, CLIENT_PAYMENT_TYPE_CD, DEVICE_SERIAL_CD) AS 
  SELECT TLI.TRAN_LINE_ITEM_AMOUNT,TLI.TRAN_LINE_ITEM_QUANTITY,IT.TRAN_LINE_ITEM_TYPE_GROUP_CD, TLI.TRAN_ID, T.TRAN_START_TS, PST.CLIENT_PAYMENT_TYPE_CD, D.DEVICE_SERIAL_CD  
      FROM PSS.TRAN T
      JOIN PSS.TRAN_LINE_ITEM TLI ON T.TRAN_ID = TLI.TRAN_ID
      JOIN PSS.POS_PTA PTA ON T.POS_PTA_ID = PTA.POS_PTA_ID
      JOIN PSS.PAYMENT_SUBTYPE PST ON PTA.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
      JOIN PSS.POS POS ON PTA.POS_ID = POS.POS_ID
      JOIN DEVICE.DEVICE D ON POS.DEVICE_ID = D.DEVICE_ID
      JOIN PSS.TRAN_LINE_ITEM_TYPE IT ON TLI.TRAN_LINE_ITEM_TYPE_ID = IT.TRAN_LINE_ITEM_TYPE_ID
      JOIN PSS.TRAN_STATE TS ON T.TRAN_STATE_CD = TS.TRAN_STATE_CD
     WHERE TS.REPORTABLE_IND = 'Y'
       AND TLI.TRAN_LINE_ITEM_BATCH_TYPE_CD = 'A'
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/PSS/VW_TRAN_COUNT_BY_DAY.vws?revision=1.7
CREATE OR REPLACE FORCE VIEW PSS.VW_TRAN_COUNT_BY_DAY (TRAN_UPLOAD_TS, UPLOAD_DAY, SOURCE_SYSTEM_CD, DEVICE_TYPE_ID, TRAN_TYPE, TRAN_ID, TRAN_GLOBAL_TRANS_CD, TRAN_LINE_ITEM_AMOUNT, TRAN_LINE_ITEM_QUANTITY) AS 
SELECT X.TRAN_UPLOAD_TS,
       TRUNC(X.TRAN_UPLOAD_TS) UPLOAD_DAY,
       'PSS' SOURCE_SYSTEM_CD, 
       D.DEVICE_TYPE_ID, 
       TT.TRANS_TYPE_NAME TRAN_TYPE,
       X.TRAN_ID, 
       X.TRAN_GLOBAL_TRANS_CD,
       TLI.TRAN_LINE_ITEM_AMOUNT, 
       TLI.TRAN_LINE_ITEM_QUANTITY
  FROM PSS.TRAN X
  JOIN PSS.POS_PTA PTA ON X.POS_PTA_ID = PTA.POS_PTA_ID 
  JOIN PSS.POS POS ON PTA.POS_ID = POS.POS_ID 
  JOIN DEVICE.DEVICE D ON POS.DEVICE_ID = D.DEVICE_ID 
  JOIN PSS.PAYMENT_SUBTYPE PST ON PTA.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
  JOIN PSS.TRAN_LINE_ITEM TLI ON X.TRAN_ID = TLI.TRAN_ID
  LEFT OUTER JOIN PSS.AUTH SA ON X.TRAN_ID = SA.TRAN_ID AND SA.AUTH_TYPE_CD IN('U','S','O') AND SA.OVERRIDE_TRANS_TYPE_ID IS NOT NULL
  LEFT OUTER JOIN PSS.AUTH AA ON X.TRAN_ID = AA.TRAN_ID AND AA.AUTH_TYPE_CD IN('L','N') AND AA.OVERRIDE_TRANS_TYPE_ID IS NOT NULL
  LEFT OUTER JOIN PSS.REFUND R ON X.TRAN_ID = R.TRAN_ID AND R.OVERRIDE_TRANS_TYPE_ID IS NOT NULL
  JOIN REPORT.TRANS_TYPE TT ON COALESCE(SA.OVERRIDE_TRANS_TYPE_ID, AA.OVERRIDE_TRANS_TYPE_ID, R.OVERRIDE_TRANS_TYPE_ID, PST.TRANS_TYPE_ID) = TT.TRANS_TYPE_ID
  JOIN PSS.TRAN_STATE TS ON X.TRAN_STATE_CD = TS.TRAN_STATE_CD
 WHERE TLI.TRAN_LINE_ITEM_BATCH_TYPE_CD = 'A'
   AND TS.REPORTABLE_IND = 'Y'
/

