alter table device.device_modem_state add (temp_column NUMBER(20,0));
update device.device_modem_state set temp_column=device_id;
alter table device.device_modem_state rename column device_id to modem_serial_cd;
alter table device.device_modem_state rename column temp_column to device_id;
alter table device.device_modem_state disable constraint DEVICE_MODEM_STATE_PK;
alter table device.device_modem_state modify (modem_serial_cd NUMBER(20,0) NULL);
update device.device_modem_state set modem_serial_cd=null;
alter table device.device_modem_state modify (modem_serial_cd varchar2(255));
comment on column device.device_modem_state.modem_serial_cd is 'Modem''s serial code (or host''s serial code), ICCID (for LTE) or MEID (for CDMA)';
comment on column device.device_modem_state.device_id is 'Deprecated and not used anymore';
update device.device_modem_state set modem_serial_cd=device_id;
alter table device.device_modem_state enable constraint DEVICE_MODEM_STATE_PK;

update device.comm_method set modem_id_regex='^89148' where comm_method_cd='V';
update device.comm_method set modem_id_regex='^A10' where comm_method_cd='C';
commit;
