insert into pss.authority_payment_mask(AUTHORITY_PAYMENT_MASK_NAME, AUTHORITY_PAYMENT_MASK_REGEX, AUTHORITY_PAYMENT_MASK_BREF)
select 'Apriva Special Card Track 2',
'^;?(?!(?:4[0-9]{15}|5[1-5][0-9]{14}|3[47][0-9]{13}|(?:30[0-5][0-9]|3095|35[2-8][0-9]|36|3[8-9][0-9]{2}|6011|622[1-9]|62[4-6][0-9]|628[2-8]|64[4-9][0-9]|65[0-9]{2})[0-9]{12}|30[0-5][0-9]{11}|36[0-9]{12}|639621[0-9]{10,13}|627722[0-9]{10})(?:=[0-9]+)?)([0-9]{6,}(?:=[0-9]+)?)(?:\?([\x00-\xFF])?)?$',
'1:1' from dual 
where not exists (select 1 from pss.authority_payment_mask where authority_payment_mask_name = 'Apriva Special Card Track 2');
commit;

UPDATE PSS.PAYMENT_SUBTYPE 
SET AUTHORITY_PAYMENT_MASK_ID = (SELECT AUTHORITY_PAYMENT_MASK_ID FROM PSS.AUTHORITY_PAYMENT_MASK WHERE AUTHORITY_PAYMENT_MASK_NAME = 'Apriva Special Card Track 2')
WHERE PAYMENT_SUBTYPE_CLASS = 'Apriva';
COMMIT;

UPDATE PSS.POS_PTA
SET AUTHORITY_PAYMENT_MASK_ID = (SELECT AUTHORITY_PAYMENT_MASK_ID FROM PSS.AUTHORITY_PAYMENT_MASK WHERE AUTHORITY_PAYMENT_MASK_NAME = 'Apriva Special Card Track 2')
WHERE AUTHORITY_PAYMENT_MASK_ID IN (SELECT AUTHORITY_PAYMENT_MASK_ID FROM PSS.AUTHORITY_PAYMENT_MASK WHERE AUTHORITY_PAYMENT_MASK_NAME IN ('Demo Only: Any Card Track 2', 'Non-Credit Card Track 2', 'Standard Track2 Format'))
AND PAYMENT_SUBTYPE_ID IN (SELECT PAYMENT_SUBTYPE_ID FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_CLASS = 'Apriva');
COMMIT;

UPDATE PSS.POS_PTA_TMPL_ENTRY
SET AUTHORITY_PAYMENT_MASK_ID = (SELECT AUTHORITY_PAYMENT_MASK_ID FROM PSS.AUTHORITY_PAYMENT_MASK WHERE AUTHORITY_PAYMENT_MASK_NAME = 'Apriva Special Card Track 2')
WHERE AUTHORITY_PAYMENT_MASK_ID IN (SELECT AUTHORITY_PAYMENT_MASK_ID FROM PSS.AUTHORITY_PAYMENT_MASK WHERE AUTHORITY_PAYMENT_MASK_NAME IN ('Demo Only: Any Card Track 2', 'Non-Credit Card Track 2', 'Standard Track2 Format'))
AND PAYMENT_SUBTYPE_ID IN (SELECT PAYMENT_SUBTYPE_ID FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_CLASS = 'Apriva');
COMMIT;
