update report.report_param set param_value='SELECT CUS.CUSTOMER_NAME "Operator Name", R.REGION_NAME "Region Name", C.CONSUMER_FNAME "First Name", C.CONSUMER_LNAME "Last Name", CA.CONSUMER_ACCT_IDENTIFIER "Card Id", 
CA.CONSUMER_ACCT_BALANCE "Account Balance", CA.NICK_NAME "Card Nickname", CA.CONSUMER_ACCT_CD "Card Number", C.CONSUMER_EMAIL_ADDR1 "Email Address", C.CONSUMER_CELL_PHONE_NUM "Mobile Phone Number",
CA.CONSUMER_ACCT_ACTIVE_YN_FLAG "Account Enabled", DBADMIN.UTC_TO_LOCAL_DATE(CC.CREATED_UTC_TS) "Registration Time", CC.CREDENTIAL_ACTIVE_FLAG "Account Activated", 
CC.ACTIVATED_TS "Activation Time", CAS.CONSUMER_ACCT_SUB_TYPE_DESC "Account Sub Type", CA.CONSUMER_ACCT_PROMO_TOTAL "Promo Total",
CA.CONSUMER_ACCT_REPLENISH_TOTAL "Replenish Total", CA.REPLENISH_BONUS_TOTAL "Replenish Bonus Total", 
CA.LOYALTY_DISCOUNT_TOTAL "Loyalty Discount Total", CA.CASH_BACK_TOTAL "Cashback Total", 
RT.REPLENISH_TYPE_DESC "Replenish Type", CAR.REPLENISH_AMOUNT "Replenish Amount", CAR.REPLENISH_THRESHHOLD "Replenish Threshold", 
CAR.REPLENISH_CARD_MASKED "Replenish Card", CAR.LAST_REPLENISH_TRAN_TS "Last Replenish Time",
CA.CONSUMER_ACCT_REPLENISH_TOTAL+CA.CONSUMER_ACCT_PROMO_TOTAL-CA.CONSUMER_ACCT_BALANCE "Total Tran Amount",
CA.TRAN_COUNT_TOTAL "Total Tran Count"
FROM PSS.CONSUMER C
JOIN PSS.CONSUMER_ACCT CA ON C.CONSUMER_ID = CA.CONSUMER_ID
JOIN PSS.CONSUMER_ACCT_SUB_TYPE CAS ON CA.CONSUMER_ACCT_SUB_TYPE_ID = CAS.CONSUMER_ACCT_SUB_TYPE_ID
JOIN PSS.CONSUMER_CREDENTIAL CC ON C.CONSUMER_ID = CC.CONSUMER_ID
JOIN CORP.CUSTOMER CUS ON CA.CORP_CUSTOMER_ID = CUS.CUSTOMER_ID
LEFT OUTER JOIN REPORT.REGION R ON C.REGION_ID = R.REGION_ID
LEFT OUTER JOIN PSS.CONSUMER_ACCT_REPLENISH CAR ON CA.CONSUMER_ACCT_REPLENISH_ID = CAR.CONSUMER_ACCT_REPLENISH_ID
LEFT OUTER JOIN PSS.REPLENISH_TYPE RT ON CAR.REPLENISH_TYPE_ID = RT.REPLENISH_TYPE_ID
JOIN REPORT.VW_USER_CONSUMER_ACCT VUCA ON VUCA.CONSUMER_ACCT_ID=CA.CONSUMER_ACCT_ID
JOIN REPORT.USER_LOGIN UL ON UL.USER_ID = VUCA.USER_ID 
WHERE CA.CONSUMER_ACCT_TYPE_ID = 3 AND DBADMIN.UTC_TO_LOCAL_DATE(CC.CREATED_UTC_TS) >= CAST(? AS DATE) AND DBADMIN.UTC_TO_LOCAL_DATE(CC.CREATED_UTC_TS) < CAST(? AS DATE) + 1
AND UL.USER_ID = ?
ORDER BY CUS.CUSTOMER_NAME, C.CONSUMER_FNAME, C.CONSUMER_LNAME'
where report_id=(select report_id from report.reports where report_name='MORE Activations')
and param_name='query';
commit;

ALTER SESSION SET CURRENT_SCHEMA = REPORT;    
DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := -1;
BEGIN
	SELECT MAX(REPORT_ID) INTO LN_REPORT_ID
	FROM REPORT.REPORTS WHERE REPORT_NAME = 'MORE Consumer Activity';
	
	IF LN_REPORT_ID > 0 THEN
		RETURN;
	END IF;

	SELECT REPORT.REPORTS_SEQ.NEXTVAL INTO LN_REPORT_ID FROM DUAL;
	
	INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
	VALUES( LN_REPORT_ID, 'MORE Consumer Activity - {b} to {e}', 6, 0, 'MORE Consumer Activity', 'Report that shows a summary of consumer activity during a period.', 'N', 0);
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
	VALUES( LN_REPORT_ID, 'query', 'SELECT
bt.BUSINESS_TYPE_NAME "Business Type",
l.LOCATION_NAME "Location",
r.REGION_NAME "Region",
SUM(CASE WHEN tsbd.trans_type_id =30 THEN tsbd.TRAN_COUNT ELSE NULL END) "Tran Counts",
round(SUM(CASE WHEN tsbd.trans_type_id =30 THEN tsbd.TRAN_AMOUNT ELSE NULL END),2) "Tran Amounts",
round(SUM(CASE WHEN tsbd.trans_type_id =30 THEN tsbd.TRAN_AMOUNT ELSE NULL END)/SUM(CASE WHEN tsbd.trans_type_id =30 THEN tsbd.TRAN_COUNT ELSE NULL END),2) "Average Amount/Tran",
SUM(NVL(tsbd.LOYALTY_DISCOUNT,0)) "Discounts",
round((SUM(CASE WHEN tsbd.trans_type_id =30 THEN tsbd.TRAN_COUNT ELSE NULL END)/(cast( ? as date)-cast( ? as date)))*7,2) "Average Tran Counts/Week",
round((SUM(CASE WHEN tsbd.trans_type_id =30 THEN tsbd.TRAN_AMOUNT ELSE NULL END)/(cast( ? as date)-cast( ? as date)))*7,2) "Average Tran Amounts/Week",
SUM(CASE WHEN tsbd.trans_type_id =31 THEN tsbd.TRAN_COUNT ELSE NULL END) "Refund Counts",
round(SUM(CASE WHEN tsbd.trans_type_id =31 THEN tsbd.TRAN_AMOUNT ELSE NULL END),2) "Refund Amounts"
FROM REPORT.TRANS_STAT_BY_DAY tsbd 
JOIN REPORT.EPORT e ON tsbd.EPORT_ID = e.EPORT_ID    
JOIN REPORT.TERMINAL t ON tsbd.TERMINAL_ID = t.TERMINAL_ID AND t.STATUS != ''D''    
JOIN CORP.CUSTOMER c_1 ON t.CUSTOMER_ID = c_1.CUSTOMER_ID    
LEFT OUTER JOIN REPORT.MACHINE m ON t.MACHINE_ID = m.MACHINE_ID    
JOIN REPORT.BUSINESS_TYPE bt ON t.BUSINESS_TYPE_ID = bt.BUSINESS_TYPE_ID    
LEFT OUTER JOIN REPORT.TERMINAL_REGION tr ON t.TERMINAL_ID = tr.TERMINAL_ID  
LEFT OUTER JOIN REPORT.REGION r ON tr.REGION_ID = r.REGION_ID
JOIN REPORT.LOCATION l ON t.LOCATION_ID = l.LOCATION_ID   
JOIN REPORT.VW_USER_TERMINAL UT ON T.TERMINAL_ID = UT.TERMINAL_ID AND UT.USER_ID=?
where tsbd.TRAN_DATE >= TRUNC(cast( ?  as date), ''DD'') AND tsbd.TRAN_DATE < 1 + TRUNC(cast( ? as date), ''DD'')
AND tsbd.trans_type_id in (30,31)
AND (? is NULL or r.REGION_ID MEMBER OF CAST(? AS DBADMIN.NUMBER_TABLE) )
GROUP BY bt.BUSINESS_TYPE_NAME, l.LOCATION_NAME, r.REGION_NAME
ORDER BY bt.BUSINESS_TYPE_NAME, l.LOCATION_NAME, r.REGION_NAME');
	
	insert into report.report_param values(LN_REPORT_ID, 'paramNames','EndDate,StartDate,EndDate,StartDate,userId,StartDate,EndDate,regionIds,regionIds');
	insert into report.report_param values(LN_REPORT_ID, 'paramTypes','TIMESTAMP,TIMESTAMP,TIMESTAMP,TIMESTAMP,NUMBER,TIMESTAMP,TIMESTAMP,ARRAY:NUMERIC,ARRAY:NUMERIC');
	insert into report.report_param values(LN_REPORT_ID, 'paramRequired','true,true,true,true,true,true,true,false,false');
	insert into report.report_param values(LN_REPORT_ID, 'params.StartDate','{b}');
	insert into report.report_param values(LN_REPORT_ID, 'params.EndDate','{e}');
	insert into report.report_param values(LN_REPORT_ID, 'params.userId','{u}');
	insert into report.report_param values(LN_REPORT_ID, 'params.regionIds','{g}');
	insert into report.report_param values(LN_REPORT_ID, 'isSQLFolio','true');
	insert into report.report_param values(LN_REPORT_ID, 'dateFormat','MM/dd/yyyy');
	
	insert into web_content.web_link values(web_content.seq_web_link_id.nextval, 'MORE Consumer Activity', './more_consumer_activity.html?basicReportId='||LN_REPORT_ID,'MORE Consumer Activity','Daily Operations','P', 0);
	insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 1);
	insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 4);
	insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 45);
	commit;

END;
/

insert into report.report_param 
select report_id, 'params.campaignIds', null 
from report.reports where report_name='Sales Rollup'
and not exists (select 1 from report.report_param where param_name='params.campaignIds' and report_id=(select report_id from report.reports where report_name='Sales Rollup'));
commit;

ALTER SESSION SET CURRENT_SCHEMA = REPORT;    
DECLARE
	LN_WEB_LINK_ID NUMBER := -1;
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := -1;
BEGIN
	SELECT MAX(web_link_id) INTO LN_WEB_LINK_ID
	FROM web_content.web_link WHERE web_link_label = 'MORE Sales Rollup';
	
	IF LN_WEB_LINK_ID > 0 THEN
		RETURN;
	END IF;

	insert into web_content.web_link values(web_content.seq_web_link_id.nextval, 'MORE Sales Rollup', './more_sales_rollup.html?folioId=969','MORE Sales Rollup','Daily Operations','P', 0);
	insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 1);
	insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 4);
	insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 45);
	commit;

END;
/

