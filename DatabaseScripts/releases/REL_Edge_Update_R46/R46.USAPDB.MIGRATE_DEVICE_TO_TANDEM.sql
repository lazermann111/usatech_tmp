GRANT SELECT, UPDATE ON CORP.CUSTOMER TO PSS;
GRANT SELECT, UPDATE ON REPORT.TERMINAL TO PSS;

CREATE OR REPLACE PROCEDURE PSS.MIGRATE_DEVICE_TO_TANDEM(
    pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE)
IS
    ln_customer_primary_merch_id PSS.MERCHANT.MERCHANT_ID%TYPE;
    lv_customer_primary_dba CORP.CUSTOMER.DOING_BUSINESS_AS%TYPE;
    ln_elavon_merchant_id PSS.MERCHANT.MERCHANT_ID%TYPE;
    lv_merchant_group_cd PSS.MERCHANT.MERCHANT_GROUP_CD%TYPE;
    ln_tandem_merchant_id PSS.MERCHANT.MERCHANT_ID%TYPE;
    ln_tandem_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE;
    ln_tandem_payment_subtype_id PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_ID%TYPE;
    ln_cutover_ts DATE := SYSDATE;
    ln_new_pos_pta_id PSS.POS_PTA.POS_PTA_ID%TYPE;
    lb_first BOOLEAN := TRUE;

    CURSOR l_cur IS
        select pp.pos_pta_id, pp.payment_subtype_id, t.terminal_id, t.terminal_cd, m.merchant_id, m.merchant_cd, c.customer_id, c.doing_business_as customer_doing_business_as, t.doing_business_as terminal_doing_business_as, m.doing_business_as merchant_doing_business_as,
               pp.pos_id, pp.POS_PTA_ENCRYPT_KEY, pp.POS_PTA_ENCRYPT_KEY2, pp.POS_PTA_DEVICE_SERIAL_CD, pp.POS_PTA_PIN_REQ_YN_FLAG, pp.POS_PTA_REGEX, pp.POS_PTA_REGEX_BREF,
               pp.AUTHORITY_PAYMENT_MASK_ID, pp.POS_PTA_PASSTHRU_ALLOW_YN_FLAG, pp.POS_PTA_DISABLE_DEBIT_DENIAL, pp.POS_PTA_PREF_AUTH_AMT, pp.POS_PTA_PREF_AUTH_AMT_MAX,
               pp.CURRENCY_CD, pp.NO_CONVENIENCE_FEE, pp.POS_PTA_PRIORITY
        from pss.pos_pta pp
        join pss.terminal t on t.terminal_id = pp.payment_subtype_key_id
        join pss.merchant m on m.merchant_id = t.merchant_id
        join pss.pos p on pp.pos_id = p.pos_id
        join pss.payment_subtype ps on pp.payment_subtype_id = ps.payment_subtype_id
        join device.device_last_active d on p.device_id = d.device_id
        left outer join (corp.customer c
            join report.terminal t on t.customer_id = c.customer_id
            join report.vw_terminal_eport te on t.terminal_id = te.terminal_id
            join report.eport e on te.eport_id = e.eport_id) on d.device_serial_cd = e.eport_serial_num and t.status != 'D'
        where d.device_serial_cd = pv_device_serial_cd
        and ps.payment_subtype_class = 'Authority::ISO8583::Elavon'
        and NVL(pp.POS_PTA_DEACTIVATION_TS, MAX_DATE) > SYSDATE
        ORDER BY M.MERCHANT_ID, T.TERMINAL_ID, pp.POS_PTA_PRIORITY, pp.pos_pta_id;
BEGIN
    FOR l_rec IN l_cur LOOP
        IF lb_first THEN
            lb_first := FALSE;
            IF l_rec.customer_id IS NOT NULL AND l_rec.customer_doing_business_as IS NULL THEN
                -- find the primary (most frequently used) merchant id for this customer
                select merchant_id, doing_business_as
                into ln_customer_primary_merch_id, lv_customer_primary_dba
                from (
                    select m.merchant_id, m.doing_business_as, count(1)
                    from pss.merchant m
                    join authority.authority a on m.authority_id = a.authority_id
                    join pss.terminal mt on mt.merchant_id = m.merchant_id
                    join pss.pos_pta pp on mt.terminal_id = pp.payment_subtype_key_id
                    join pss.payment_subtype ps on pp.payment_subtype_id = ps.payment_subtype_id
                    join pss.pos p on p.pos_id = pp.pos_id
                    join device.device d on d.device_id = p.device_id
                    join report.eport e on e.eport_serial_num = d.device_serial_cd
                    join report.vw_terminal_eport te on te.eport_id = e.eport_id
                    join report.terminal t on t.terminal_id = te.terminal_id
                    where ps.payment_subtype_class = 'Authority::ISO8583::Elavon'
                    and (pp.pos_pta_activation_ts <= sysdate or pp.pos_pta_activation_ts is null)
                    and (pp.pos_pta_deactivation_ts is null or sysdate < pp.pos_pta_deactivation_ts)
                    and mt.status_cd = 'A'
                    and m.status_cd = 'A'
                    and a.authority_name = 'Elavon ISO8583'
                    and d.device_active_yn_flag = 'Y'
                    and t.customer_id = l_rec.customer_id
                    and t.status != 'D'
                    group by m.merchant_id, m.doing_business_as
                    order by count(1) desc) 
                where rownum = 1;

                update corp.customer
                set doing_business_as = lv_customer_primary_dba
                where customer_id = l_rec.customer_id;

                IF l_rec.TERMINAL_ID IS NOT NULL AND l_rec.TERMINAL_DOING_BUSINESS_AS IS NULL AND l_rec.MERCHANT_DOING_BUSINESS_AS != lv_customer_primary_dba THEN
                    update report.terminal
                    set doing_business_as = l_rec.MERCHANT_DOING_BUSINESS_AS
                    where terminal_id = l_rec.terminal_id
                    and doing_business_as is null;
                END IF;
            END IF;
        END IF;
        
        -- now convert the device's payment settings to Tandem
        ln_tandem_payment_subtype_id := null;
        ln_new_pos_pta_id := null;
   
        IF ln_elavon_merchant_id IS NULL OR ln_elavon_merchant_id != l_rec.merchant_id THEN        
        -- get the Tandem merchant_group_code to convert the Elavon merchant to
        select merchant_group_cd 
        into lv_merchant_group_cd
        from (
            SELECT 0 OLD_MERCHANT_ID, '' MERCHANT_GROUP_CD FROM DUAL WHERE 1=0
            UNION ALL SELECT 1012132, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012135, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012172, 'business_express_5999' FROM DUAL
            UNION ALL SELECT 1012173, 'business_express_5999' FROM DUAL
            UNION ALL SELECT 1012174, 'business_express_5999' FROM DUAL
            UNION ALL SELECT 1012175, 'business_express_5999' FROM DUAL
            UNION ALL SELECT 1012176, 'business_express_5999' FROM DUAL
            UNION ALL SELECT 1012177, 'business_express_5999' FROM DUAL
            UNION ALL SELECT 1012178, 'business_express_5999' FROM DUAL
            UNION ALL SELECT 1012179, 'business_express_5999' FROM DUAL
            UNION ALL SELECT 1012180, 'business_express_5999' FROM DUAL
            UNION ALL SELECT 1012181, 'business_express_5999' FROM DUAL
            UNION ALL SELECT 1012182, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012183, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012184, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012185, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012186, 'car_wash_7542' FROM DUAL
            UNION ALL SELECT 1012187, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012188, 'kiosk_5999' FROM DUAL
            UNION ALL SELECT 1012189, 'business_express_5999' FROM DUAL
            UNION ALL SELECT 1012190, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012191, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012192, 'esuds_7211' FROM DUAL
            UNION ALL SELECT 1012193, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012194, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012195, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012196, 'kiosk_5999' FROM DUAL
            UNION ALL SELECT 1012197, 'business_express_5999' FROM DUAL
            UNION ALL SELECT 1012199, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012200, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012201, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012203, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012204, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012205, 'amusement_7994' FROM DUAL
            UNION ALL SELECT 1012206, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012207, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012209, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012210, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012211, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012212, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012213, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012214, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012215, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012216, 'kiosk_5999' FROM DUAL
            UNION ALL SELECT 1012232, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012233, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012235, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012253, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012295, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012301, 'business_express_5999' FROM DUAL
            UNION ALL SELECT 1012304, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012306, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012307, 'amusement_7994' FROM DUAL
            UNION ALL SELECT 1012695, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012757, 'amusement_7994' FROM DUAL
            UNION ALL SELECT 1012857, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1013177, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1013261, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1013390, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1014106, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 2014885, 'amusement_7994' FROM DUAL
            UNION ALL SELECT 2014886, 'amusement_7994' FROM DUAL
            UNION ALL SELECT 2014887, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 2016025, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 2016046, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2017968, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 2018569, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 2018889, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 2018890, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 2018891, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 2018950, 'setomatics_7211' FROM DUAL
            UNION ALL SELECT 2019010, 'setomatics_7211' FROM DUAL
            UNION ALL SELECT 2019011, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 2020408, 'kiosk_5999' FROM DUAL
            UNION ALL SELECT 2021268, 'taxi_4121' FROM DUAL
            UNION ALL SELECT 2021269, 'kiosk_5999' FROM DUAL
            UNION ALL SELECT 2021270, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 2021393, 'kiosk_5999' FROM DUAL
            UNION ALL SELECT 2021493, 'kiosk_5999' FROM DUAL
            UNION ALL SELECT 2022334, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 2022436, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2022614, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 2022615, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 2023194, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 2023995, 'eport_online_5999' FROM DUAL
            UNION ALL SELECT 2024674, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 2025096, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2026875, 'micro_market_5999' FROM DUAL
            UNION ALL SELECT 2026876, 'taxi_4121' FROM DUAL
            UNION ALL SELECT 2026974, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2027094, 'kiosk_5999' FROM DUAL
            UNION ALL SELECT 2027115, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2027414, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2027495, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2027754, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 2027895, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2027896, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2027897, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2027898, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2027899, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2027900, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2027901, 'kiosk_5999' FROM DUAL
            UNION ALL SELECT 2027902, 'kiosk_5999' FROM DUAL
            UNION ALL SELECT 2027956, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2027957, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2027958, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2027959, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2027960, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2027974, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2027976, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2028295, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2028296, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2028575, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 2028774, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 2028914, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2028934, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2029494, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 1012296, 'business_express_5999' FROM DUAL
            UNION ALL SELECT 2020269, 'taxi_4121' FROM DUAL
            UNION ALL SELECT 1012294, 'business_express_5999' FROM DUAL
            UNION ALL SELECT 2028757, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 2023314, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 2022435, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 2020230, 'taxi_4121' FROM DUAL
            UNION ALL SELECT 2026994, 'taxi_4121' FROM DUAL
            UNION ALL SELECT 1012297, 'business_express_5999' FROM DUAL
            UNION ALL SELECT 2027954, 'taxi_4121' FROM DUAL
            UNION ALL SELECT 1012302, 'car_wash_7542' FROM DUAL
            UNION ALL SELECT 2023276, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 2023315, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 2022396, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 1012308, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 2027955, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 2028014, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 2023814, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 1012692, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 2027374, 'eport_online_5999' FROM DUAL
            UNION ALL SELECT 2025095, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 2027394, 'eport_online_5999' FROM DUAL
            UNION ALL SELECT 1012298, 'business_express_5999' FROM DUAL
            UNION ALL SELECT 1012293, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 2023754, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 2027375, 'eport_online_5999' FROM DUAL
            UNION ALL SELECT 2019088, 'setomatics_7211' FROM DUAL
            UNION ALL SELECT 1012303, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 2028214, 'eport_online_5999' FROM DUAL
            UNION ALL SELECT 2027354, 'eport_online_5999' FROM DUAL
            UNION ALL SELECT 1012208, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012299, 'business_express_5999' FROM DUAL
            UNION ALL SELECT 2027975, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 1012171, 'kiosk_5999' FROM DUAL
            UNION ALL SELECT 2027994, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 1012202, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1014105, 'car_wash_7542' FROM DUAL
            UNION ALL SELECT 1012305, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012198, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012300, 'business_express_5999' FROM DUAL
            UNION ALL SELECT 1012292, 'soda_snack_5814' FROM DUAL
            -- For ECC
            UNION ALL SELECT 6609, 'CPT_TEST_1' FROM DUAL           
            -- For INT
            UNION ALL SELECT 464, 'CPT_TEST_1' FROM DUAL           
            -- for test merchants on usadev04
            UNION ALL SELECT 362, 'CPT_TEST_1' FROM DUAL
            UNION ALL SELECT 405, 'tandem_soda_snack_5814' FROM DUAL
            UNION ALL SELECT 406, 'CPT_REAL_TEST' FROM DUAL) MM 
        where mm.old_merchant_id = l_rec.merchant_id;
      
        ln_elavon_merchant_id := l_rec.merchant_id; -- store for compare on next go round
        -- now get the next available terminal/merchant for this merchant_group_cd
        SELECT MERCHANT_ID, TERMINAL_ID
        INTO ln_tandem_merchant_id, ln_tandem_terminal_id
        FROM (
            SELECT M.MERCHANT_ID, T.TERMINAL_ID, A.MAX_POS_PTA_NUM - A.MIN_POS_PTA_NUM + 1 - COUNT(DISTINCT MPP.DEVICE_NAME) REMAINING_POS_PTA_NUMS, A.MIN_POS_PTA_NUM, A.MAX_POS_PTA_NUM
            FROM PSS.TERMINAL T
            JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
            JOIN AUTHORITY.AUTHORITY A ON M.AUTHORITY_ID = A.AUTHORITY_ID
            LEFT OUTER JOIN PSS.MERCHANT_POS_PTA_NUM MPP ON M.MERCHANT_ID = MPP.MERCHANT_ID AND MPP.POS_PTA_NUM BETWEEN A.MIN_POS_PTA_NUM AND A.MAX_POS_PTA_NUM
            WHERE A.AUTHORITY_NAME = 'Tandem'
            AND M.MERCHANT_GROUP_CD = lv_merchant_group_cd
            GROUP BY M.MERCHANT_ID, T.TERMINAL_ID, A.MAX_POS_PTA_NUM, A.MIN_POS_PTA_NUM
            HAVING (MAX_POS_PTA_NUM - MIN_POS_PTA_NUM + 1 - COUNT(DISTINCT MPP.DEVICE_NAME) > 0)
            ORDER BY 1, 3 DESC, 4)
        WHERE ROWNUM = 1;
        END IF;
        -- find the corresponding Tandem payment_subtype_id for the Elavon pos_pta.payment_subtype_id
        select ps_tandem.payment_subtype_id
        into ln_tandem_payment_subtype_id
        from pss.payment_subtype ps_tandem
        where exists (
            select 1
            from pss.payment_subtype ps_elavon
            where ps_elavon.payment_subtype_class = 'Authority::ISO8583::Elavon'
            and ps_elavon.client_payment_type_cd = ps_tandem.client_payment_type_cd
            and ps_elavon.authority_payment_mask_id = ps_tandem.authority_payment_mask_id
            and ps_elavon.trans_type_id = ps_tandem.trans_type_id
            and ps_elavon.payment_subtype_id = l_rec.payment_subtype_id)
        and ps_tandem.payment_subtype_class = 'Tandem'
        and ps_tandem.status_cd = 'A';
  
        -- create a matching pos_pta record using the Tandem terminal and payment_subtype_id
        PSS.PKG_POS_PTA.INSERT_POS_PTA(
            ln_new_pos_pta_id,
            l_rec.pos_id,
            ln_tandem_payment_subtype_id,
            l_rec.POS_PTA_ENCRYPT_KEY,
            l_rec.POS_PTA_ENCRYPT_KEY2,
            ln_tandem_terminal_id,
            l_rec.POS_PTA_DEVICE_SERIAL_CD,
            l_rec.POS_PTA_PIN_REQ_YN_FLAG,
            l_rec.POS_PTA_REGEX,
            l_rec.POS_PTA_REGEX_BREF,
            l_rec.AUTHORITY_PAYMENT_MASK_ID,
            l_rec.POS_PTA_PASSTHRU_ALLOW_YN_FLAG,
            l_rec.POS_PTA_DISABLE_DEBIT_DENIAL,
            l_rec.POS_PTA_PREF_AUTH_AMT,
            l_rec.POS_PTA_PREF_AUTH_AMT_MAX,
            l_rec.CURRENCY_CD,
            ln_cutover_ts,
            l_rec.NO_CONVENIENCE_FEE);
            
        update pss.pos_pta 
        set POS_PTA_PRIORITY = l_rec.POS_PTA_PRIORITY
        where pos_pta_id = ln_new_pos_pta_id;
        
        -- deactivate the Elavon pos_pta
        update pss.pos_pta 
        set pos_pta_deactivation_ts = ln_cutover_ts
        where pos_pta_id = l_rec.pos_pta_id;
    END LOOP;
END;
/

GRANT EXECUTE ON PSS.MIGRATE_DEVICE_TO_TANDEM TO USAT_DEV_READ_ONLY;