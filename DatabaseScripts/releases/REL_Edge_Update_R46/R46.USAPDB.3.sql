INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, LITERAL_VALUE)
select 'prepaid-registration-sms-prompt','To complete your registration,'||chr(10)||'please enter the passcode found in the text message that has been sent to you.'
from dual
where not exists (select 1 from WEB_CONTENT.LITERAL where LITERAL_KEY='prepaid-registration-sms-prompt');

INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, LITERAL_VALUE)
select 'prepaid-passcode-expired-sms','This registration is expired. To complete your registration,'||chr(10)||'please enter the new passcode found in the text message that has been sent to you.'
from dual
where not exists (select 1 from WEB_CONTENT.LITERAL where LITERAL_KEY='prepaid-passcode-expired-sms');

INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, LITERAL_VALUE)
select 'prepaid-not-registered-sms','Your account is not yet fully registered. To complete your registration,'||chr(10)||'please enter the passcode found in the text message that has been sent to you.'
from dual
where not exists (select 1 from WEB_CONTENT.LITERAL where LITERAL_KEY='prepaid-not-registered-sms');
commit;

-- add to data sync for MCC
DECLARE
    CURSOR l_cur IS
    SELECT D.*
     FROM DEVICE.DEVICE D
     JOIN REPORT.EPORT E ON D.DEVICE_SERIAL_CD = E.EPORT_SERIAL_NUM
     JOIN REPORT.VW_TERMINAL_EPORT TE ON E.EPORT_ID = TE.EPORT_ID
     JOIN REPORT.TERMINAL T ON TE.TERMINAL_ID = T.TERMINAL_ID
     JOIN REPORT.BUSINESS_TYPE BT ON T.BUSINESS_TYPE_ID = BT.BUSINESS_TYPE_ID
    WHERE D.DEVICE_ACTIVE_YN_FLAG = 'Y'
      AND BT.MCC IS NOT NULL;
BEGIN
    FOR l_rec IN l_cur LOOP
        ENGINE.PKG_DATA_SYNC.SP_CREATE_DATA_SYNC('DEVICE_DETAIL', 'REPORT.BUSINESS_TYPE', 'U', 0, l_rec.DEVICE_NAME);
        COMMIT;
    END LOOP;
END;
/

-- add to data sync for passwords
DECLARE
    CURSOR l_cur IS
    SELECT D.*
     FROM DEVICE.DEVICE D
     JOIN DEVICE.CREDENTIAL C ON D.CREDENTIAL_ID = C.CREDENTIAL_ID
     LEFT OUTER JOIN DEVICE.DEVICE_INFO DI ON DI.DEVICE_NAME = D.DEVICE_NAME
    WHERE D.DEVICE_ACTIVE_YN_FLAG = 'Y'
      AND (DI.USERNAME IS NULL OR DI.PASSWORD_HASH IS NULL OR DI.PASSWORD_SALT IS NULL);
BEGIN
    FOR l_rec IN l_cur LOOP
        ENGINE.PKG_DATA_SYNC.SP_CREATE_DATA_SYNC('DEVICE_INFO', 'DEVICE.DEVICE_INFO', 'U', 0, l_rec.DEVICE_NAME);
        COMMIT;
    END LOOP;
END;
/

UPDATE DEVICE.DEVICE_INFO DI
   SET (PREVIOUS_USERNAME, PREVIOUS_PASSWORD_HASH, PREVIOUS_PASSWORD_SALT, TEMP_PASSCODE_YN_FLAG, NEW_USERNAME, NEW_PASSWORD_HASH, NEW_PASSWORD_SALT) =
      (SELECT CASE WHEN TEMP_PASSCODE_YN_FLAG = 'Y' AND MAX(C.CREDENTIAL_ID) IS NULL THEN NEW_USERNAME END,
              CASE WHEN TEMP_PASSCODE_YN_FLAG = 'Y' AND MAX(C.CREDENTIAL_ID) IS NULL THEN NEW_PASSWORD_HASH END,
              CASE WHEN TEMP_PASSCODE_YN_FLAG = 'Y' AND MAX(C.CREDENTIAL_ID) IS NULL THEN NEW_PASSWORD_SALT END,
              CASE WHEN TEMP_PASSCODE_YN_FLAG = 'Y' AND MAX(C.CREDENTIAL_ID) IS NULL THEN 'O'
                   WHEN TEMP_PASSCODE_YN_FLAG = 'Y' AND MAX(C.CREDENTIAL_ID) IS NOT NULL THEN 'C'
                   ELSE TEMP_PASSCODE_YN_FLAG
              END,
              CASE WHEN TEMP_PASSCODE_YN_FLAG != 'Y' THEN NEW_USERNAME END,
              CASE WHEN TEMP_PASSCODE_YN_FLAG != 'Y' THEN NEW_PASSWORD_HASH END,
              CASE WHEN TEMP_PASSCODE_YN_FLAG != 'Y' THEN NEW_PASSWORD_SALT END
         FROM DEVICE.DEVICE D
         JOIN DEVICE.CREDENTIAL C ON D.CREDENTIAL_ID = C.CREDENTIAL_ID
        WHERE D.DEVICE_ACTIVE_YN_FLAG = 'Y'
          AND D.DEVICE_NAME = DI.DEVICE_NAME
          AND C.PASSWORD_HASH = DI.NEW_PASSWORD_HASH
          AND C.PASSWORD_SALT = DI.NEW_PASSWORD_SALT)      
 WHERE TEMP_PASSCODE_YN_FLAG = 'Y'
    OR PREVIOUS_USERNAME IS NOT NULL
    OR PREVIOUS_PASSWORD_HASH IS NOT NULL
    OR PREVIOUS_PASSWORD_SALT IS NOT NULL;
COMMIT;

DECLARE
    CURSOR l_cur IS
    SELECT DISTINCT E.EPORT_ID, E.EPORT_SERIAL_NUM, E.LAST_DEX_DATE, 
           FIRST_VALUE(DF.DEX_FILE_ID) OVER(PARTITION BY DF.EPORT_ID ORDER BY DF.DEX_FILE_ID DESC) DEX_FILE_ID,
           FIRST_VALUE(DF.FILE_TRANSFER_ID) OVER(PARTITION BY DF.EPORT_ID ORDER BY DF.DEX_FILE_ID DESC) FILE_TRANSFER_ID           
      FROM REPORT.EPORT E
      JOIN G4OP.DEX_FILE DF ON E.EPORT_ID = DF.EPORT_ID AND E.LAST_DEX_DATE = DF.DEX_DATE
     WHERE E.LAST_DEX_DATE IS NOT NULL
       AND E.LAST_DEX_ALERTS IS NULL
       AND (NVL(DF.ALERT_COUNT, -1) != 0 OR NVL(DF.EXCEPTION_COUNT, -1) != 0);
    lt_details VARCHAR2_TABLE;
    lv_alert_summary VARCHAR2(4000);
BEGIN
    FOR l_rec IN l_cur LOOP
        SELECT DISTINCT TA.DETAILS
          BULK COLLECT INTO lt_details
          FROM REPORT.TERMINAL_ALERT TA
          JOIN REPORT.ALERT A ON TA.ALERT_ID = A.ALERT_ID
         WHERE TA.REFERENCE_ID = l_rec.FILE_TRANSFER_ID
           AND A.STATUS = 'A'
           AND A.ALERT_SOURCE_ID IN(1,4);
        IF lt_details.COUNT > 0 THEN
            lv_alert_summary := '';
            FOR i IN lt_details.FIRST..lt_details.LAST LOOP
                IF LENGTH(lv_alert_summary) > 0 THEN
                    lv_alert_summary := lv_alert_summary || '; ';
                END IF;
                IF LENGTH(lv_alert_summary) + LENGTH(lt_details(i)) > 3997 THEN
                    lv_alert_summary := lv_alert_summary || SUBSTR(lt_details(i), 1, 3997 - LENGTH(lv_alert_summary)) || '...';
                    EXIT;
                END IF;
                lv_alert_summary := lv_alert_summary || lt_details(i);
						END LOOP;
            /*
            DBMS_OUTPUT.PUT_LINE('For Device ' || l_rec.EPORT_SERIAL_NUM || ', setting LAST_DEX_ALERTS to text of length ' || LENGTH(lv_alert_summary) || ':');
            DBMS_OUTPUT.PUT_LINE(lv_alert_summary);          
						/*/
            UPDATE REPORT.EPORT
               SET LAST_DEX_ALERTS = lv_alert_summary
             WHERE EPORT_ID = l_rec.EPORT_ID
               AND LAST_DEX_DATE = l_rec.LAST_DEX_DATE;
            COMMIT;
            --*/
        END IF;
    END LOOP;
END;
/

DECLARE
    CURSOR l_cur IS
      SELECT MERCHANT_ID FROM PSS.MERCHANT;
BEGIN
    FOR l_rec IN l_cur LOOP
        ENGINE.PKG_DATA_SYNC.SP_CREATE_DATA_SYNC(
              'MERCHANT',
              'PSS.MERCHANT',
              'U',
              l_rec.MERCHANT_ID,
              l_rec.MERCHANT_ID
            );
        COMMIT;
    END LOOP;
END;
/

-- populate consumer acct id					
BEGIN
	FOR l_cur IN (select TRAN_ID from report.trans t where t.TRANS_TYPE_ID in (24,25,27,28) and t.close_date>sysdate-90 and APPLY_TO_CONSUMER_ACCT_ID IS NULL) LOOP
		UPDATE REPORT.TRANS T SET (APPLY_TO_CONSUMER_ACCT_ID)=(
			SELECT APPLY_TO_CONSUMER_ACCT_ID
			FROM REPORT.PURCHASE P
			WHERE P.TRAN_ID= l_cur.TRAN_ID and tran_line_item_type_id=550)
    	where T.TRAN_ID=l_cur.TRAN_ID
      ;
		commit;
	END LOOP;
END;
/						