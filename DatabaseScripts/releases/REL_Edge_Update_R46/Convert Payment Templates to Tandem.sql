/* To get Mapping Spreadsheet:
    select m.*, bt.merchant_group_cds
    from authority.authority a 
    join pss.merchant m on a.authority_id = m.authority_id
    left outer join (select mbt.merchant_id, dbadmin.adhere(cast(collect(bt.merchant_group_cd || ' (' || mbt.cnt || ')') as VARCHAR2_TABLE), ', ') merchant_group_cds
    from
    (SELECT pt.merchant_id, t.business_type_id, count(distinct d.device_serial_cd) cnt
    from pss.pos_pta pp
     join pss.pos p on pp.pos_id = p.pos_id
join device.device d on p.device_id = d.device_id
join (report.eport e
join report.vw_terminal_eport te on te.eport_id = e.eport_id
join report.terminal t on t.terminal_id = te.terminal_id) on e.eport_serial_num = d.device_serial_cd
    join pss.payment_subtype pst on pp.payment_subtype_id = pst.payment_subtype_id
    join pss.terminal pt on pp.payment_subtype_key_id = pt.terminal_id
     where pst.payment_subtype_class = 'Authority::ISO8583::Elavon'
    and pp.pos_pta_deactivation_ts is null
    and d.device_active_yn_flag = 'Y'
    group by pt.merchant_id, t.business_type_id) mbt
    left outer join (
    select 0 BUSINESS_TYPE_ID, '' merchant_group_cd from dual where 1=0
    union all select 1, 'amusement_7994' from dual
    union all select 2, 'business_express_5999' from dual
    union all select 3, 'car_wash_7542' from dual
    union all select 4, 'usat_other_5999' from dual
    union all select 5, 'soda_snack_5814' from dual
    union all select 6, 'usat_other_5999' from dual
    union all select 7, 'usat_other_5999' from dual
    union all select 8, 'eport_mobile_5999' from dual
    union all select 9, 'eport_online_5999' from dual
    union all select 10, 'esuds_7211' from dual
    union all select 11, 'esuds_7211' from dual
    union all select 12, 'kiosk_5999' from dual
    union all select 13, 'setomatics_7211' from dual
    union all select 14, 'micro_market_5999' from dual
    union all select 15, 'usat_other_5999' from dual
    union all select 16, 'taxi_4121' from dual
    union all select 17, 'soda_snack_5814' from dual) bt on mbt.BUSINESS_TYPE_ID = bt.BUSINESS_TYPE_ID
    group by mbt.merchant_id
    ) bt on bt.merchant_id = m.merchant_id     
    where a.authority_name like 'Elavon ISO8583';
   DROP TABLE BKRUG.TMP_CONVERTED_POS_PTA_ENTRY;
   
*/

CREATE TABLE BKRUG.TMP_CONVERTED_POS_PTA_ENTRY
  AS
SELECT PPT.POS_PTA_TMPL_ID, PPT.POS_PTA_TMPL_NAME, PPT.POS_PTA_TMPL_DESC, PP.POS_PTA_TMPL_ENTRY_ID, PP.PAYMENT_SUBTYPE_ID, PP.PAYMENT_SUBTYPE_KEY_ID, N_PST.PAYMENT_SUBTYPE_ID NEW_PAYMENT_SUBTYPE_ID, N_M.TERMINAL_ID NEW_PAYMENT_SUBTYPE_KEY_ID, MM.MERCHANT_GROUP_CD 
  FROM PSS.POS_PTA_TMPL PPT
  JOIN PSS.POS_PTA_TMPL_ENTRY PP ON PPT.POS_PTA_TMPL_ID = PP.POS_PTA_TMPL_ID
  JOIN PSS.PAYMENT_SUBTYPE PST ON PP.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
  JOIN PSS.TERMINAL T ON PP.PAYMENT_SUBTYPE_KEY_ID = T.TERMINAL_ID
  LEFT OUTER JOIN (SELECT 0 OLD_MERCHANT_ID, '' MERCHANT_GROUP_CD FROM DUAL WHERE 1=0
            UNION ALL SELECT 1012132, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012135, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012172, 'business_express_5999' FROM DUAL
            UNION ALL SELECT 1012173, 'business_express_5999' FROM DUAL
            UNION ALL SELECT 1012174, 'business_express_5999' FROM DUAL
            UNION ALL SELECT 1012175, 'business_express_5999' FROM DUAL
            UNION ALL SELECT 1012176, 'business_express_5999' FROM DUAL
            UNION ALL SELECT 1012177, 'business_express_5999' FROM DUAL
            UNION ALL SELECT 1012178, 'business_express_5999' FROM DUAL
            UNION ALL SELECT 1012179, 'business_express_5999' FROM DUAL
            UNION ALL SELECT 1012180, 'business_express_5999' FROM DUAL
            UNION ALL SELECT 1012181, 'business_express_5999' FROM DUAL
            UNION ALL SELECT 1012182, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012183, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012184, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012185, 'setomatics_7211' FROM DUAL
            UNION ALL SELECT 1012186, 'car_wash_7542' FROM DUAL
            UNION ALL SELECT 1012187, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012188, 'kiosk_5999' FROM DUAL
            UNION ALL SELECT 1012189, 'business_express_5999' FROM DUAL
            UNION ALL SELECT 1012190, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012191, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012192, 'esuds_7211' FROM DUAL
            UNION ALL SELECT 1012193, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012194, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012195, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012196, 'kiosk_5999' FROM DUAL
            UNION ALL SELECT 1012197, 'business_express_5999' FROM DUAL
            UNION ALL SELECT 1012199, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012200, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012201, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012203, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012204, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012205, 'amusement_7994' FROM DUAL
            UNION ALL SELECT 1012206, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012207, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012209, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012210, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012211, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012212, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012213, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012214, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012215, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012216, 'kiosk_5999' FROM DUAL
            UNION ALL SELECT 1012232, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012233, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012235, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012253, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012295, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012301, 'business_express_5999' FROM DUAL
            UNION ALL SELECT 1012304, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012306, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012307, 'amusement_7994' FROM DUAL
            UNION ALL SELECT 1012695, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012757, 'amusement_7994' FROM DUAL
            UNION ALL SELECT 1012857, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1013177, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1013261, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1013390, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1014106, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 2014885, 'amusement_7994' FROM DUAL
            UNION ALL SELECT 2014886, 'amusement_7994' FROM DUAL
            UNION ALL SELECT 2014887, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 2016025, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 2016046, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2017968, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 2018569, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 2018889, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 2018890, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 2018891, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 2018950, 'setomatics_7211' FROM DUAL
            UNION ALL SELECT 2019010, 'setomatics_7211' FROM DUAL
            UNION ALL SELECT 2019011, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 2020408, 'kiosk_5999' FROM DUAL
            UNION ALL SELECT 2021268, 'taxi_4121' FROM DUAL
            UNION ALL SELECT 2021269, 'kiosk_5999' FROM DUAL
            UNION ALL SELECT 2021270, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 2021393, 'kiosk_5999' FROM DUAL
            UNION ALL SELECT 2021493, 'kiosk_5999' FROM DUAL
            UNION ALL SELECT 2022334, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 2022436, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2022614, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 2022615, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 2023194, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 2023995, 'eport_online_5999' FROM DUAL
            UNION ALL SELECT 2024674, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 2025096, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2026875, 'micro_market_5999' FROM DUAL
            UNION ALL SELECT 2026876, 'taxi_4121' FROM DUAL
            UNION ALL SELECT 2026974, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2027094, 'kiosk_5999' FROM DUAL
            UNION ALL SELECT 2027115, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2027414, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2027495, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2027754, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 2027895, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2027896, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2027897, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2027898, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2027899, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2027900, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2027901, 'kiosk_5999' FROM DUAL
            UNION ALL SELECT 2027902, 'kiosk_5999' FROM DUAL
            UNION ALL SELECT 2027956, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2027957, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2027958, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2027959, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2027960, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2027974, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2027976, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2028295, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2028296, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2028575, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 2028774, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 2028914, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2028934, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 2029494, 'eport_mobile_5999' FROM DUAL
            UNION ALL SELECT 1012296, 'business_express_5999' FROM DUAL
            UNION ALL SELECT 2020269, 'taxi_4121' FROM DUAL
            UNION ALL SELECT 1012294, 'business_express_5999' FROM DUAL
            UNION ALL SELECT 2028757, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 2023314, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 2022435, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 2020230, 'taxi_4121' FROM DUAL
            UNION ALL SELECT 2026994, 'taxi_4121' FROM DUAL
            UNION ALL SELECT 1012297, 'business_express_5999' FROM DUAL
            UNION ALL SELECT 2027954, 'taxi_4121' FROM DUAL
            UNION ALL SELECT 1012302, 'car_wash_7542' FROM DUAL
            UNION ALL SELECT 2023276, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 2023315, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 2022396, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 1012308, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 2027955, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 2028014, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 2023814, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 1012692, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 2027374, 'eport_online_5999' FROM DUAL
            UNION ALL SELECT 2025095, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 2027394, 'eport_online_5999' FROM DUAL
            UNION ALL SELECT 1012298, 'business_express_5999' FROM DUAL
            UNION ALL SELECT 1012293, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 2023754, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 2027375, 'eport_online_5999' FROM DUAL
            UNION ALL SELECT 2019088, 'setomatics_7211' FROM DUAL
            UNION ALL SELECT 1012303, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 2028214, 'eport_online_5999' FROM DUAL
            UNION ALL SELECT 2027354, 'eport_online_5999' FROM DUAL
            UNION ALL SELECT 1012208, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012299, 'business_express_5999' FROM DUAL
            UNION ALL SELECT 2027975, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 1012171, 'kiosk_5999' FROM DUAL
            UNION ALL SELECT 2027994, 'usat_other_5999' FROM DUAL
            UNION ALL SELECT 1012202, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1014105, 'car_wash_7542' FROM DUAL
            UNION ALL SELECT 1012305, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012198, 'soda_snack_5814' FROM DUAL
            UNION ALL SELECT 1012300, 'business_express_5999' FROM DUAL
            UNION ALL SELECT 1012292, 'soda_snack_5814' FROM DUAL
            -- For ECC
            UNION ALL SELECT 6609, 'CPT_TEST_1' FROM DUAL           
            -- For INT
            UNION ALL SELECT 464, 'CPT_TEST_1' FROM DUAL 
            -- FOR DEV
            UNION ALL SELECT 546, 'CPT_TEST_1' FROM DUAL 
            UNION ALL SELECT 509, 'CPT_TEST_1' FROM DUAL 
            -- for test merchants on usadev04
            UNION ALL SELECT 362, 'CPT_TEST_1' FROM DUAL
            UNION ALL SELECT 405, 'tandem_soda_snack_5814' FROM DUAL
            UNION ALL SELECT 406, 'CPT_REAL_TEST' FROM DUAL
      ) MM ON T.MERCHANT_ID = MM.OLD_MERCHANT_ID
  LEFT OUTER JOIN (SELECT MIN(T.TERMINAL_ID) TERMINAL_ID, M.MERCHANT_GROUP_CD 
  FROM PSS.MERCHANT M
  JOIN PSS.TERMINAL T ON M.MERCHANT_ID = T.MERCHANT_ID
  JOIN AUTHORITY.AUTHORITY A ON M.AUTHORITY_ID = A.AUTHORITY_ID
  AND M.MERCHANT_GROUP_CD IS NOT NULL
  AND A.AUTHORITY_NAME = 'Tandem'
  GROUP BY M.MERCHANT_GROUP_CD) N_M ON MM.MERCHANT_GROUP_CD = N_M.MERCHANT_GROUP_CD
  LEFT OUTER JOIN PSS.PAYMENT_SUBTYPE N_PST ON PST.CLIENT_PAYMENT_TYPE_CD = N_PST.CLIENT_PAYMENT_TYPE_CD
    AND PST.AUTHORITY_PAYMENT_MASK_ID = N_PST.AUTHORITY_PAYMENT_MASK_ID
    AND N_PST.PAYMENT_SUBTYPE_CLASS = 'Tandem'
 WHERE PST.PAYMENT_SUBTYPE_CLASS = 'Authority::ISO8583::Elavon'         
  AND PPT.STATUS_CD = 'A'
  AND PST.STATUS_CD = 'A'
  AND MM.MERCHANT_GROUP_CD NOT IN('taxi_4121', 'eport_mobile_5999')
  AND PPT.POS_PTA_TMPL_NAME NOT LIKE '% PUERTO RICO%';

COMMIT;
    
    /*
    SELECT DISTINCT T.POS_PTA_TMPL_ID, CASE WHEN REPLACE(T.POS_PTA_TMPL_NAME, 'Elavon', 'Tandem') LIKE '%Tandem%' THEN REPLACE(T.POS_PTA_TMPL_NAME, 'Elavon', 'Tandem') ELSE REPLACE(T.POS_PTA_TMPL_NAME, 'Elavon', 'Tandem') || ' ~ Tandem' END NEW_POS_PTA_TMPL_NAME, 
           CASE WHEN REPLACE(T.POS_PTA_TMPL_DESC, 'Elavon', 'Tandem') LIKE '%Tandem%' THEN REPLACE(T.POS_PTA_TMPL_DESC, 'Elavon', 'Tandem') ELSE REPLACE(T.POS_PTA_TMPL_DESC, 'Elavon', 'Tandem') || ' ~ Tandem' END NEW_POS_PTA_TMPL_DESC,
           CASE WHEN T.POS_PTA_TMPL_NAME LIKE '%Elavon%' THEN T.POS_PTA_TMPL_NAME ELSE T.POS_PTA_TMPL_NAME || ' ~ Elavon' END OLD_POS_PTA_TMPL_NAME, 
           CASE WHEN T.POS_PTA_TMPL_DESC LIKE '%Elavon%' THEN T.POS_PTA_TMPL_DESC ELSE T.POS_PTA_TMPL_DESC || ' ~ Elavon' END OLD_POS_PTA_TMPL_DESC,
           MERCHANT_GROUP_CD
      FROM BKRUG.TMP_CONVERTED_POS_PTA_ENTRY N
      JOIN PSS.POS_PTA_TMPL T ON T.POS_PTA_TMPL_ID = N.POS_PTA_TMPL_ID
      --WHERE T.STATUS_CD = 'A'
      ORDER BY 2;*/
DECLARE
    CURSOR l_cur IS
    SELECT DISTINCT T.POS_PTA_TMPL_ID, CASE WHEN REPLACE(T.POS_PTA_TMPL_NAME, 'Elavon', 'Tandem') LIKE '%Tandem%' THEN REPLACE(T.POS_PTA_TMPL_NAME, 'Elavon', 'Tandem') ELSE REPLACE(T.POS_PTA_TMPL_NAME, 'Elavon', 'Tandem') || ' - Tandem' END NEW_POS_PTA_TMPL_NAME, 
           CASE WHEN REPLACE(T.POS_PTA_TMPL_DESC, 'Elavon', 'Tandem') LIKE '%Tandem%' THEN REPLACE(T.POS_PTA_TMPL_DESC, 'Elavon', 'Tandem') ELSE REPLACE(T.POS_PTA_TMPL_DESC, 'Elavon', 'Tandem') || ' - Tandem' END NEW_POS_PTA_TMPL_DESC,
           CASE WHEN T.POS_PTA_TMPL_NAME LIKE '%Elavon%' THEN T.POS_PTA_TMPL_NAME ELSE T.POS_PTA_TMPL_NAME || ' - Elavon' END OLD_POS_PTA_TMPL_NAME, 
           CASE WHEN T.POS_PTA_TMPL_DESC LIKE '%Elavon%' THEN T.POS_PTA_TMPL_DESC ELSE T.POS_PTA_TMPL_DESC || ' - Elavon' END OLD_POS_PTA_TMPL_DESC
      FROM BKRUG.TMP_CONVERTED_POS_PTA_ENTRY N
      JOIN PSS.POS_PTA_TMPL T ON T.POS_PTA_TMPL_ID = N.POS_PTA_TMPL_ID
      WHERE T.STATUS_CD = 'A'
      ORDER BY T.POS_PTA_TMPL_ID;
    ln_pos_pta_tmpl_id PSS.POS_PTA_TMPL.POS_PTA_TMPL_ID%TYPE;
BEGIN
    FOR l_rec IN l_cur LOOP
        SELECT PSS.SEQ_POS_PTA_TMPL_ID.NEXTVAL
          INTO ln_pos_pta_tmpl_id
          FROM DUAL;
        UPDATE PSS.POS_PTA_TMPL
           SET STATUS_CD = 'D', POS_PTA_TMPL_NAME = l_rec.OLD_POS_PTA_TMPL_NAME, POS_PTA_TMPL_DESC = l_rec.OLD_POS_PTA_TMPL_DESC
         WHERE POS_PTA_TMPL_ID = l_rec.POS_PTA_TMPL_ID;
        INSERT INTO PSS.POS_PTA_TMPL(POS_PTA_TMPL_ID, POS_PTA_TMPL_NAME, POS_PTA_TMPL_DESC)
           VALUES(ln_pos_pta_tmpl_id, l_rec.NEW_POS_PTA_TMPL_NAME, l_rec.NEW_POS_PTA_TMPL_DESC);
        INSERT INTO PSS.POS_PTA_TMPL_ENTRY(POS_PTA_TMPL_ENTRY_ID, POS_PTA_TMPL_ID, PAYMENT_SUBTYPE_ID, PAYMENT_SUBTYPE_KEY_ID, 
            POS_PTA_ENCRYPT_KEY, POS_PTA_ACTIVATION_OSET_HR, POS_PTA_DEACTIVATION_OSET_HR, POS_PTA_PIN_REQ_YN_FLAG, 
            POS_PTA_DEVICE_SERIAL_CD, POS_PTA_ENCRYPT_KEY2, AUTHORITY_PAYMENT_MASK_ID, POS_PTA_PRIORITY, 
            TERMINAL_ID, MERCHANT_BANK_ACCT_ID, CURRENCY_CD, POS_PTA_PASSTHRU_ALLOW_YN_FLAG, POS_PTA_PREF_AUTH_AMT, 
            POS_PTA_PREF_AUTH_AMT_MAX, POS_PTA_DISABLE_DEBIT_DENIAL,NO_CONVENIENCE_FEE)
           SELECT PSS.SEQ_POS_PTA_TMPL_ENTRY_ID.NEXTVAL, ln_pos_pta_tmpl_id, N.NEW_PAYMENT_SUBTYPE_ID, N.NEW_PAYMENT_SUBTYPE_KEY_ID,
                  O.POS_PTA_ENCRYPT_KEY, O.POS_PTA_ACTIVATION_OSET_HR, O.POS_PTA_DEACTIVATION_OSET_HR, O.POS_PTA_PIN_REQ_YN_FLAG, 
                  O.POS_PTA_DEVICE_SERIAL_CD, O.POS_PTA_ENCRYPT_KEY2, O.AUTHORITY_PAYMENT_MASK_ID, O.POS_PTA_PRIORITY, 
                  O.TERMINAL_ID, O.MERCHANT_BANK_ACCT_ID, O.CURRENCY_CD, O.POS_PTA_PASSTHRU_ALLOW_YN_FLAG, O.POS_PTA_PREF_AUTH_AMT, 
                  O.POS_PTA_PREF_AUTH_AMT_MAX, O.POS_PTA_DISABLE_DEBIT_DENIAL, O.NO_CONVENIENCE_FEE
             FROM PSS.POS_PTA_TMPL_ENTRY O
             JOIN BKRUG.TMP_CONVERTED_POS_PTA_ENTRY N ON O.POS_PTA_TMPL_ENTRY_ID = N.POS_PTA_TMPL_ENTRY_ID
            WHERE N.POS_PTA_TMPL_ID = l_rec.POS_PTA_TMPL_ID;
        UPDATE DEVICE.DEVICE_TYPE
           SET POS_PTA_TMPL_ID = ln_pos_pta_tmpl_id
         WHERE POS_PTA_TMPL_ID = l_rec.POS_PTA_TMPL_ID;
        UPDATE DEVICE.DEVICE_SUB_TYPE
           SET POS_PTA_TMPL_ID = ln_pos_pta_tmpl_id
         WHERE POS_PTA_TMPL_ID = l_rec.POS_PTA_TMPL_ID;
        UPDATE CORP.CUSTOMER
           SET POS_PTA_TMPL_ID = ln_pos_pta_tmpl_id
         WHERE POS_PTA_TMPL_ID = l_rec.POS_PTA_TMPL_ID;
         
        COMMIT;
    END LOOP;
END;
/


update pss.pos_pta_tmpl
set pos_pta_tmpl_name = pos_pta_tmpl_name || ' - Elavon'
where pos_pta_tmpl_id in (
select ppt.pos_pta_tmpl_id from pss.pos_pta_tmpl ppt join pss.pos_pta_tmpl_entry ppte on ppt.pos_pta_tmpl_id = ppte.pos_pta_tmpl_id
join pss.payment_subtype ps on ppte.payment_subtype_id = ps.payment_subtype_id
where ps.payment_subtype_class = 'Authority::ISO8583::Elavon'
and ppt.status_cd = 'A' and lower(ppt.pos_pta_tmpl_name) not like '%elavon%'
);
commit;


DECLARE
    CURSOR L_CUR IS
	SELECT PPT.POS_PTA_TMPL_ID OLD_TMPL_ID, PPT2.POS_PTA_TMPL_ID NEW_TMPL_ID
	FROM PSS.POS_PTA_TMPL PPT JOIN PSS.POS_PTA_TMPL PPT2 ON REPLACE(PPT.POS_PTA_TMPL_NAME, 'Elavon', 'Tandem') = PPT2.POS_PTA_TMPL_NAME
	AND PPT.POS_PTA_TMPL_ID != PPT2.POS_PTA_TMPL_ID;
BEGIN
	FOR L_REC IN L_CUR LOOP
		INSERT INTO PSS.POS_PTA_TMPL_ENTRY(POS_PTA_TMPL_ENTRY_ID, POS_PTA_TMPL_ID, PAYMENT_SUBTYPE_ID, PAYMENT_SUBTYPE_KEY_ID, 
			POS_PTA_ENCRYPT_KEY, POS_PTA_ACTIVATION_OSET_HR, POS_PTA_DEACTIVATION_OSET_HR, POS_PTA_PIN_REQ_YN_FLAG, 
			POS_PTA_DEVICE_SERIAL_CD, POS_PTA_ENCRYPT_KEY2, AUTHORITY_PAYMENT_MASK_ID, POS_PTA_PRIORITY, 
			TERMINAL_ID, MERCHANT_BANK_ACCT_ID, CURRENCY_CD, POS_PTA_PASSTHRU_ALLOW_YN_FLAG, POS_PTA_PREF_AUTH_AMT, 
			POS_PTA_PREF_AUTH_AMT_MAX, POS_PTA_DISABLE_DEBIT_DENIAL, NO_CONVENIENCE_FEE)
		SELECT PSS.SEQ_POS_PTA_TMPL_ENTRY_ID.NEXTVAL, L_REC.NEW_TMPL_ID, O.PAYMENT_SUBTYPE_ID, O.PAYMENT_SUBTYPE_KEY_ID,
			  O.POS_PTA_ENCRYPT_KEY, O.POS_PTA_ACTIVATION_OSET_HR, O.POS_PTA_DEACTIVATION_OSET_HR, O.POS_PTA_PIN_REQ_YN_FLAG, 
			  O.POS_PTA_DEVICE_SERIAL_CD, O.POS_PTA_ENCRYPT_KEY2, O.AUTHORITY_PAYMENT_MASK_ID, O.POS_PTA_PRIORITY, 
			  O.TERMINAL_ID, O.MERCHANT_BANK_ACCT_ID, O.CURRENCY_CD, O.POS_PTA_PASSTHRU_ALLOW_YN_FLAG, O.POS_PTA_PREF_AUTH_AMT, 
			  O.POS_PTA_PREF_AUTH_AMT_MAX, O.POS_PTA_DISABLE_DEBIT_DENIAL, O.NO_CONVENIENCE_FEE
		FROM PSS.POS_PTA_TMPL_ENTRY O
		JOIN PSS.PAYMENT_SUBTYPE PS ON O.PAYMENT_SUBTYPE_ID = PS.PAYMENT_SUBTYPE_ID
		WHERE O.POS_PTA_TMPL_ID = L_REC.OLD_TMPL_ID 
			AND PS.PAYMENT_SUBTYPE_CLASS != 'Authority::ISO8583::Elavon'
			AND NOT EXISTS (
			SELECT 1 FROM PSS.POS_PTA_TMPL_ENTRY
			WHERE POS_PTA_TMPL_ID = L_REC.NEW_TMPL_ID 
				AND PAYMENT_SUBTYPE_ID = O.PAYMENT_SUBTYPE_ID
				AND COALESCE(PAYMENT_SUBTYPE_KEY_ID, 0) = COALESCE(O.PAYMENT_SUBTYPE_KEY_ID, 0)
		);
		
		COMMIT;
	END LOOP;
END;
/
