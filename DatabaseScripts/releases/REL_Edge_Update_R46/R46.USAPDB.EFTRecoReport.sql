ALTER SESSION SET CURRENT_SCHEMA = REPORT;    
DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := -1;
	l_user_id NUMBER;
BEGIN
	SELECT MAX(REPORT_ID) INTO LN_REPORT_ID
	FROM REPORT.REPORTS WHERE REPORT_NAME = 'EFT Reconcile Export With Two Tier (CSV)';
	
	IF LN_REPORT_ID > 0 THEN
		RETURN;
	END IF;

	SELECT REPORT.REPORTS_SEQ.NEXTVAL INTO LN_REPORT_ID FROM DUAL;
	
	select user_id into l_user_id from report.user_login ul cross join V$DATABASE d 
	where user_name=(CASE WHEN d.name like 'USADEV%' THEN 'yhe@usatech.com'
	WHEN d.name like 'ECC%' THEN 'aroyce_customer'
	ELSE 'jbare' END);
  
	INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
	VALUES( LN_REPORT_ID, 'EFTRECO-{x}-{d}', 6, 3, 'EFT Reconcile Export With Two Tier (CSV)', 'Comma-separated file showing payment info broken down by reconcile group', 'N', l_user_id);

	
	insert into report.report_param values(LN_REPORT_ID, 'folioId','1745');
	insert into report.report_param values(LN_REPORT_ID, 'params.DocId','{x}');
	
	commit;

END;
/