DECLARE
  ln_exists_count NUMBER;
  PROCEDURE CREATE_TANDEM_MERCHANT (
    pv_tdid PSS.MERCHANT.AUTHORITY_ASSIGNED_NUMBER%TYPE,
    pv_merchant_name PSS.MERCHANT.MERCHANT_NAME%TYPE,
    pv_dba PSS.MERCHANT.DOING_BUSINESS_AS%TYPE,
    pv_merchant_cd PSS.MERCHANT.MERCHANT_CD%TYPE,
    pv_terminal_cd PSS.TERMINAL.TERMINAL_CD%TYPE,
    pn_mcc PSS.MERCHANT.MCC%TYPE,
    pv_merchant_bus_name PSS.MERCHANT.MERCHANT_BUS_NAME%TYPE,
    pv_merchant_group_cd PSS.MERCHANT.MERCHANT_GROUP_CD%TYPE)
  IS
  BEGIN
    SELECT COUNT(1)
    INTO ln_exists_count
    FROM PSS.MERCHANT 
    WHERE MERCHANT_CD = pv_merchant_cd
    AND AUTHORITY_ID = (select authority_id from authority.authority where authority_name = 'Tandem');
    
    IF ln_exists_count > 0 THEN
      dbms_output.put_line('Merchant account already exists: '||pv_merchant_cd);
      RETURN;
    END IF;
    
    INSERT INTO PSS.MERCHANT(MERCHANT_CD, MERCHANT_NAME, MERCHANT_DESC, MERCHANT_BUS_NAME, AUTHORITY_ID, DOING_BUSINESS_AS, MCC, AUTHORITY_ASSIGNED_NUMBER, MERCHANT_GROUP_CD)
    SELECT pv_merchant_cd, pv_merchant_name, pv_merchant_name, pv_merchant_bus_name, AUTHORITY_ID, pv_dba, pn_mcc, pv_tdid, pv_merchant_group_cd
    FROM AUTHORITY.AUTHORITY M
    WHERE AUTHORITY_NAME = 'Tandem';
  
    INSERT INTO PSS.TERMINAL(TERMINAL_CD, TERMINAL_NEXT_BATCH_NUM, MERCHANT_ID, TERMINAL_DESC, TERMINAL_STATE_ID,
      TERMINAL_MAX_BATCH_NUM, TERMINAL_BATCH_MAX_TRAN, TERMINAL_BATCH_CYCLE_NUM, TERMINAL_MIN_BATCH_NUM,
      TERMINAL_MIN_BATCH_CLOSE_HR, TERMINAL_MAX_BATCH_CLOSE_HR, TERMINAL_BATCH_MIN_TRAN)
    SELECT pv_terminal_cd, 1, MERCHANT_ID, pv_merchant_name || ' Terminal', 1, 999, 1, 1, 1, 0, 4, 1
    FROM PSS.MERCHANT M
    WHERE MERCHANT_CD = pv_merchant_cd
    AND AUTHORITY_ID = (select authority_id from authority.authority where authority_name = 'Tandem');
  END;
BEGIN
--  CREATE_TANDEM_MERCHANT('254242', 'USA Technologies Vending 01', 'Soda Snack Vending', '000390048138', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814'); /*
--  000390048138 is the test account!
  CREATE_TANDEM_MERCHANT('255484', 'USA Technologies Vending 02', 'Soda Snack Vending', '000390048003', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255488', 'USA Technologies Vending 03', 'Soda Snack Vending', '000390048006', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255490', 'USA Technologies Vending 04', 'Soda Snack Vending', '000390048009', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255492', 'USA Technologies Vending 05', 'Soda Snack Vending', '000390048012', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255493', 'USA Technologies Vending 06', 'Soda Snack Vending', '000390048015', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255495', 'USA Technologies Vending 07', 'Soda Snack Vending', '000390048018', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255496', 'USA Technologies Vending 08', 'Soda Snack Vending', '000390048021', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255498', 'USA Technologies Vending 09', 'Soda Snack Vending', '000390048024', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255500', 'USA Technologies Vending 10', 'Soda Snack Vending', '000390048027', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255501', 'USA Technologies Vending 11', 'Soda Snack Vending', '000390048030', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255503', 'USA Technologies Vending 12', 'Soda Snack Vending', '000390048033', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255504', 'USA Technologies Vending 13', 'Soda Snack Vending', '000390048036', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255506', 'USA Technologies Vending 14', 'Soda Snack Vending', '000390048039', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255508', 'USA Technologies Vending 15', 'Soda Snack Vending', '000390048042', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255509', 'USA Technologies Vending 16', 'Soda Snack Vending', '000390048045', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255510', 'USA Technologies Vending 17', 'Soda Snack Vending', '000390048048', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255512', 'USA Technologies Vending 18', 'Soda Snack Vending', '000390048051', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255514', 'USA Technologies Vending 19', 'Soda Snack Vending', '000390048054', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255515', 'USA Technologies Vending 20', 'Soda Snack Vending', '000390048057', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255517', 'USA Technologies Vending 21', 'Soda Snack Vending', '000390048060', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255518', 'USA Technologies Vending 22', 'Soda Snack Vending', '000390048063', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255520', 'USA Technologies Vending 23', 'Soda Snack Vending', '000390048066', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255522', 'USA Technologies Vending 24', 'Soda Snack Vending', '000390048069', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255523', 'USA Technologies Vending 25', 'Soda Snack Vending', '000390048072', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255525', 'USA Technologies Vending 26', 'Soda Snack Vending', '000390048075', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255526', 'USA Technologies Vending 27', 'Soda Snack Vending', '000390048078', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255528', 'USA Technologies Vending 28', 'Soda Snack Vending', '000390048081', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255529', 'USA Technologies Vending 29', 'Soda Snack Vending', '000390048084', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255530', 'USA Technologies Vending 30', 'Soda Snack Vending', '000390048087', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255532', 'USA Technologies Vending 31', 'Soda Snack Vending', '000390048090', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255533', 'USA Technologies Vending 32', 'Soda Snack Vending', '000390048093', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255535', 'USA Technologies Vending 33', 'Soda Snack Vending', '000390048096', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255537', 'USA Technologies Vending 34', 'Soda Snack Vending', '000390048099', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255538', 'USA Technologies Vending 35', 'Soda Snack Vending', '000390048102', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255541', 'USA Technologies Vending 36', 'Soda Snack Vending', '000390048105', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255542', 'USA Technologies Vending 37', 'Soda Snack Vending', '000390048108', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255544', 'USA Technologies Vending 38', 'Soda Snack Vending', '000390048111', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255545', 'USA Technologies Vending 39', 'Soda Snack Vending', '000390048114', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255546', 'USA Technologies Vending 40', 'Soda Snack Vending', '000390048117', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255549', 'USA Technologies Vending 41', 'Soda Snack Vending', '000390048120', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255550', 'USA Technologies Vending 42', 'Soda Snack Vending', '000390048123', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255551', 'USA Technologies Vending 43', 'Soda Snack Vending', '000390048126', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255553', 'USA Technologies Vending 44', 'Soda Snack Vending', '000390048129', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255555', 'USA Technologies Vending 45', 'Soda Snack Vending', '000390048132', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255557', 'USA Technologies Vending 46', 'Soda Snack Vending', '000390048135', '001', 5814, 'USA Technologies Vending', 'soda_snack_5814');
  CREATE_TANDEM_MERCHANT('255558', 'USA Technologies Amusement 01', 'Amusement Vending', '000000131500', '001', 7994, 'USA Technologies Amusement', 'amusement_7994');
  CREATE_TANDEM_MERCHANT('255559', 'USA Technologies Amusement 02', 'Amusement Vending', '000000131503', '001', 7994, 'USA Technologies Amusement', 'amusement_7994');
  CREATE_TANDEM_MERCHANT('255561', 'USA Technologies Amusement 03', 'Amusement Vending', '000000131506', '001', 7994, 'USA Technologies Amusement', 'amusement_7994');
  CREATE_TANDEM_MERCHANT('255563', 'USA Technologies Amusement 04', 'Amusement Vending', '000000131509', '001', 7994, 'USA Technologies Amusement', 'amusement_7994');
  CREATE_TANDEM_MERCHANT('255565', 'USA Technologies Amusement 05', 'Amusement Vending', '000000131512', '001', 7994, 'USA Technologies Amusement', 'amusement_7994');
  CREATE_TANDEM_MERCHANT('255566', 'USA Technologies Amusement 06', 'Amusement Vending', '000000131515', '001', 7994, 'USA Technologies Amusement', 'amusement_7994');
  CREATE_TANDEM_MERCHANT('255567', 'USA Tech Laundry-Setomatics 1', 'Laundry Service', '000000250000', '001', 7211, 'USA Tech Laundry-Setomatics', 'setomatics_7211');
  CREATE_TANDEM_MERCHANT('255569', 'USA Tech Laundry-Setomatics 2', 'Laundry Service', '000000250003', '001', 7211, 'USA Tech Laundry-Setomatics', 'setomatics_7211');
  CREATE_TANDEM_MERCHANT('255571', 'USA Tech Laundry-Setomatics 3', 'Laundry Service', '000000250006', '001', 7211, 'USA Tech Laundry-Setomatics', 'setomatics_7211');
  CREATE_TANDEM_MERCHANT('255572', 'USA Tech Laundry-eSuds', 'eSuds Laundry', '000390228000', '001', 7211, 'USA Tech Laundry-eSuds', 'esuds_7211');
  CREATE_TANDEM_MERCHANT('255573', 'USA Technologies Taxi 1', 'Taxi Limo Fare', '000000344000', '001', 4121, 'USA Technologies Taxi', 'taxi_4121');
  CREATE_TANDEM_MERCHANT('255574', 'USA Technologies Taxi 2', 'Taxi Limo Fare', '000000344003', '001', 4121, 'USA Technologies Taxi', 'taxi_4121');
  CREATE_TANDEM_MERCHANT('255576', 'USA Technologies Kiosk', 'USA Tech Kiosk', '000000311000', '001', 5999, 'USA Technologies Kiosk', 'kiosk_5999');
  CREATE_TANDEM_MERCHANT('255577', 'USA Technologies Car Wash', 'Car Wash Vending', '000000326000', '001', 7542, 'USA Technologies Car Wash', 'car_wash_7542');
  CREATE_TANDEM_MERCHANT('255598', 'USA Technologies ePort Mobile', 'ePort Mobile', '000390081021', '001', 5999, 'USA Technologies ePort Mobile', 'eport_mobile_5999');
  CREATE_TANDEM_MERCHANT('255607', 'USA Technologies ePort Online', 'ePort Online', '000390081000', '001', 5999, 'USA Technologies ePort Online', 'eport_online_5999');
  CREATE_TANDEM_MERCHANT('255608', 'USA Tech Business Express', 'Business Express', '000390081006', '001', 5999, 'USA Tech Business Express', 'business_express_5999');
  CREATE_TANDEM_MERCHANT('255610', 'USA Technologies Micro Market', 'USA Technologies', '000390081011', '001', 5999, 'USA Technologies Micro Market', 'micro_market_5999');
  CREATE_TANDEM_MERCHANT('255611', 'USA Technologies Other', 'USA Technologies', '000390081016', '001', 5999, 'USA Technologies Other', 'usat_other_5999');
  COMMIT; --*/
END;
/