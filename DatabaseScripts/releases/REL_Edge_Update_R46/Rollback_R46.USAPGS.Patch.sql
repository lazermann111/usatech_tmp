DROP VIEW MAIN.APP_REQUEST;

ALTER TABLE main._app_request
    ALTER COLUMN remote_addr TYPE VARCHAR(15);
    
CREATE OR REPLACE VIEW MAIN.APP_REQUEST
	AS SELECT * FROM MAIN._APP_REQUEST;

ALTER VIEW MAIN.APP_REQUEST ALTER APP_REQUEST_ID SET DEFAULT nextval('MAIN._app_request_app_request_id_seq'::regclass);
ALTER VIEW MAIN.APP_REQUEST ALTER REQUEST_TS SET DEFAULT CURRENT_TIMESTAMP;

GRANT SELECT ON MAIN.APP_REQUEST TO read_main;
GRANT INSERT, UPDATE, DELETE ON MAIN.APP_REQUEST TO write_main;

DROP TRIGGER IF EXISTS TRIIUD_APP_REQUEST ON MAIN.APP_REQUEST;
CREATE TRIGGER TRIIUD_APP_REQUEST
INSTEAD OF INSERT OR UPDATE OR DELETE ON MAIN.APP_REQUEST
	FOR EACH ROW
	EXECUTE PROCEDURE MAIN.FRIIUD_APP_REQUEST();

DROP FUNCTION MAIN.UPSERT_APP_REQUEST(
	p_app_request_id BIGINT, 
	p_app_cd VARCHAR(50), 
	p_request_url TEXT, 
	p_request_ts TIMESTAMP WITH TIME ZONE, 
	p_remote_addr VARCHAR(39), 
	p_remote_port INTEGER, 
	p_referer TEXT, 
	p_http_user_agent TEXT, 
	p_session_id VARCHAR(100), 
	p_username VARCHAR(100), 
	p_request_headers TEXT, 
	p_request_parameters TEXT, 
	p_process_time_ms INTEGER, 
	p_exception_flag CHAR(1), 
	p_exception_text TEXT, 
	p_request_handler VARCHAR(100), 
	p_folio_id INTEGER, 
	p_report_id INTEGER, 
	p_profile_id INTEGER, 
	p_action_name VARCHAR(100), 
	p_object_type_cd VARCHAR(50), 
	p_object_cd VARCHAR(200),
	p_user_id BIGINT,
	p_row_count OUT INTEGER);
	
CREATE OR REPLACE FUNCTION MAIN.UPSERT_APP_REQUEST(
	p_app_request_id BIGINT, 
	p_app_cd VARCHAR(50), 
	p_request_url TEXT, 
	p_request_ts TIMESTAMP WITH TIME ZONE, 
	p_remote_addr VARCHAR(15), 
	p_remote_port INTEGER, 
	p_referer TEXT, 
	p_http_user_agent TEXT, 
	p_session_id VARCHAR(100), 
	p_username VARCHAR(100), 
	p_request_headers TEXT, 
	p_request_parameters TEXT, 
	p_process_time_ms INTEGER, 
	p_exception_flag CHAR(1), 
	p_exception_text TEXT, 
	p_request_handler VARCHAR(100), 
	p_folio_id INTEGER, 
	p_report_id INTEGER, 
	p_profile_id INTEGER, 
	p_action_name VARCHAR(100), 
	p_object_type_cd VARCHAR(50), 
	p_object_cd VARCHAR(200),
	p_user_id BIGINT,
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
BEGIN
	LOOP
		BEGIN
			UPDATE MAIN.APP_REQUEST
			   SET APP_CD = p_app_cd,
			       REQUEST_URL = p_request_url,
			       REQUEST_TS = COALESCE(p_request_ts, REQUEST_TS),
			       REMOTE_ADDR = p_remote_addr,
			       REMOTE_PORT = p_remote_port,
			       REFERER = p_referer,
			       HTTP_USER_AGENT = p_http_user_agent,
			       SESSION_ID = p_session_id,
			       USERNAME = p_username,
			       REQUEST_HEADERS = p_request_headers,
			       REQUEST_PARAMETERS = p_request_parameters,
			       PROCESS_TIME_MS = p_process_time_ms,
			       EXCEPTION_FLAG = p_exception_flag,
			       EXCEPTION_TEXT = p_exception_text,
			       REQUEST_HANDLER = p_request_handler,
			       FOLIO_ID = p_folio_id,
			       REPORT_ID = p_report_id,
			       PROFILE_ID = p_profile_id,
			       ACTION_NAME = p_action_name,
			       OBJECT_TYPE_CD = p_object_type_cd,
			       OBJECT_CD = p_object_cd,
			       USER_ID = p_user_id
			 WHERE APP_REQUEST_ID = p_app_request_id;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				INSERT INTO MAIN.APP_REQUEST(APP_REQUEST_ID, APP_CD, REQUEST_URL, REQUEST_TS, REMOTE_ADDR, REMOTE_PORT, REFERER, HTTP_USER_AGENT, SESSION_ID, USERNAME, REQUEST_HEADERS, REQUEST_PARAMETERS, PROCESS_TIME_MS, EXCEPTION_FLAG, EXCEPTION_TEXT, REQUEST_HANDLER, FOLIO_ID, REPORT_ID, PROFILE_ID, ACTION_NAME, OBJECT_TYPE_CD, OBJECT_CD, USER_ID)
				 VALUES(p_app_request_id, p_app_cd, p_request_url, COALESCE(p_request_ts, CURRENT_TIMESTAMP), p_remote_addr, p_remote_port, p_referer, p_http_user_agent, p_session_id, p_username, p_request_headers, p_request_parameters, p_process_time_ms, p_exception_flag, p_exception_text, p_request_handler, p_folio_id, p_report_id, p_profile_id, p_action_name, p_object_type_cd, p_object_cd, p_user_id);
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			END IF;
			EXIT;
		EXCEPTION
			WHEN unique_violation THEN
				NULL;
			WHEN serialization_failure THEN
				NULL;
		END;
	END LOOP;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MAIN.UPSERT_APP_REQUEST(
	p_app_request_id BIGINT, 
	p_app_cd VARCHAR(50), 
	p_request_url TEXT, 
	p_request_ts TIMESTAMP WITH TIME ZONE, 
	p_remote_addr VARCHAR(15), 
	p_remote_port INTEGER, 
	p_referer TEXT, 
	p_http_user_agent TEXT, 
	p_session_id VARCHAR(100), 
	p_username VARCHAR(100), 
	p_request_headers TEXT, 
	p_request_parameters TEXT, 
	p_process_time_ms INTEGER, 
	p_exception_flag CHAR(1), 
	p_exception_text TEXT, 
	p_request_handler VARCHAR(100), 
	p_folio_id INTEGER, 
	p_report_id INTEGER, 
	p_profile_id INTEGER, 
	p_action_name VARCHAR(100), 
	p_object_type_cd VARCHAR(50), 
	p_object_cd VARCHAR(200),
	p_user_id BIGINT,
	p_row_count OUT INTEGER) TO write_main;