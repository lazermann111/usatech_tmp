CREATE TABLE REPORT.DFR_REPORT_DEF (
	DFR_REPORT_DEF_ID NUMBER NOT NULL,
	HEADER_RECORD_TYPE_ID VARCHAR2(32) NOT NULL,
	DATA_RECORD_TYPE_ID VARCHAR2(32),
	NAME VARCHAR2(256) NOT NULL,
	DESCRIPTION VARCHAR2(4000),
	USAGE VARCHAR2(4000),
	ENABLED CHAR(1) DEFAULT 'N' NOT NULL,
	TABLE_NAME VARCHAR2(50),
	CREATED_BY VARCHAR2(30) NOT NULL,
	CREATED_TS DATE NOT NULL,
	LAST_UPDATED_BY VARCHAR2(30) NOT NULL,
	LAST_UPDATED_TS DATE NOT NULL,
	CONSTRAINT PK_DFR_REPORT_DEF PRIMARY KEY(DFR_REPORT_DEF_ID) USING INDEX TABLESPACE REPORT_INDX);

CREATE SEQUENCE REPORT.SEQ_DFR_REPORT_DEF
	MINVALUE 1 
	MAXVALUE 999999999999999999999999999 
	INCREMENT BY 1 
	START WITH 1000
	CACHE 20 
	NOORDER 
	NOCYCLE ;

CREATE OR REPLACE TRIGGER REPORT.TRBI_DFR_REPORT_DEF BEFORE INSERT ON REPORT.DFR_REPORT_DEF
	FOR EACH ROW
BEGIN
	IF :NEW.DFR_REPORT_DEF_ID IS NULL THEN
		SELECT SEQ_DFR_REPORT_DEF.NEXTVAL INTO :NEW.DFR_REPORT_DEF_ID FROM DUAL;
	END IF;
	SELECT 
		USER,
		SYSDATE,
		SYSDATE,
		USER
	INTO 
		:NEW.CREATED_BY,
		:NEW.CREATED_TS,
		:NEW.LAST_UPDATED_TS,
		:NEW.LAST_UPDATED_BY
	FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER REPORT.TRBU_DFR_REPORT_DEF BEFORE UPDATE ON REPORT.DFR_REPORT_DEF
	FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.CREATED_BY,
		:NEW.CREATED_TS,
		:NEW.LAST_UPDATED_TS,
		:NEW.LAST_UPDATED_BY
	FROM DUAL;
END;
/

CREATE UNIQUE INDEX REPORT.IX_DFR_DEF_DAT_REC_TYPE_ID ON REPORT.DFR_REPORT_DEF(DATA_RECORD_TYPE_ID) TABLESPACE REPORT_INDX ONLINE;

GRANT SELECT,INSERT,UPDATE ON REPORT.DFR_REPORT_DEF TO USALIVE_APP_ROLE;
GRANT SELECT,INSERT,UPDATE ON REPORT.DFR_REPORT_DEF TO USAT_DMS_ROLE;
GRANT SELECT,INSERT,UPDATE ON REPORT.DFR_REPORT_DEF TO USAT_POSM_ROLE;

insert into report.dfr_report_def(header_record_type_id, data_record_type_id, name, description, usage) values ('HFIN0010', 'RFIN0010', 'Deposit Activity Summary', 'Merchants use this report to get a high level view of their successful deposits for a date range. This report will assist merchants in reconciling processing activity by comparing any applicable internal reports to the data from this report.', 'The FIN0010 summarizes the deposit activity Chase Paymentech received during a specified date range and is used for verification and reconciliation to merchant systems. This report also summarizes any fees and adjustments that financially impact the merchant.');
insert into report.dfr_report_def(header_record_type_id, data_record_type_id, name, description, usage) values ('HFIN0010T', 'RFIN0010T', 'Deposit Activity Transfer Summary', 'Merchants use this report to get a high level view of their funds transfer activity for a date range. This report will assist merchants in reconciling their bank accounts by comparing their bank statements to the data from this report.', 'The FIN0010T summarizes the funds transfers that have been created and are pending, on hold, rejected or scheduled for credit to their bank account on the current date.');
insert into report.dfr_report_def(header_record_type_id, data_record_type_id, name, description, usage) values ('HLNK010A', 'RLNK010A', 'Transaction Summary', 'This report is additional detail for the FIN0010 and will normally only be delivered in conjunction with it.', 'Merchants use this report to understand how we arrived at Successful Deposits on the FIN0010. The merchant''s financial contact may use this report to help reconcile the activity that resulted in successful deposit transactions. The report provides a count of transactions processed, authorizations, rejects, declined deposits, and non-financial transactions. When used in conjunction with the FIN0010, it provides the merchant with a complete picture of all transactions received for processing for a given activity period.');
insert into report.dfr_report_def(header_record_type_id, data_record_type_id, name, description, usage) values ('HACT0033', 'RACT0033', 'Reject Summary', 'Provides a summary of rejects by action code. This report assists a large volume processor with reject management when the volume makes a detail level report unmanageable.', 'Merchant uses data in the ACT0033 report to categorize rejects by action code.');
insert into report.dfr_report_def(header_record_type_id, data_record_type_id, name, description, usage) values ('HACT0012', 'RACT0012', 'Submission Listing', 'Provides a listing of submissions received from a merchant or presenter within a specified timeframe including a summary of transactions per file.', 'Merchants use this report to determine what submissions were received at Chase Paymentech by date and time. Merchants also use this report to verify the contents and status of a submission or a summary-level status of transactions.');
insert into report.dfr_report_def(header_record_type_id, data_record_type_id, name, description, usage) values ('HFIN0011', 'RFIN0011', 'Service Charge Detail', 'This report identifies fees assessed for services that were posted during the reporting period.', 'Merchants use this report to identify the total service charges (fees) posted for a timeframe by category and to validate all such fees at a more granular level than is presented on the FIN0010.');
insert into report.dfr_report_def(header_record_type_id, data_record_type_id, name, description, usage) values ('HANS0039', 'RANS0039', 'Debit Authorization Aging', 'This report provides a detailed listing of debit transactions for which the merchant has submitted an authorization transaction but has not yet submitted a deposit transaction and therefore has not been funded. This report helps merchants to reconcile transactions presented to Chase Paymentech for authorization with transactions presented to Chase Paymentech for settlement. The report may assist merchants with identifying operational issues that may be impacting their bottom line. This report is a snapshot of a given moment in time. As such, it can only be set up as a daily frequency report (monthly or weekly are not allowed) and cannot be re-generated for a historical date or date range.','Merchants use this data to trigger their reconciliation and/or collection and recovery processes. This may include research, updates to their customer databases, contact with the endpoint or the customer, etc.');
insert into report.dfr_report_def(header_record_type_id, data_record_type_id, name, description, usage) values ('HANS0041', 'RANS0041', 'Merchant Settlement Aging', 'This report provides a detailed listing of transactions that have not been settled by the endpoint and therefore will not be funded. This report helps merchants to reconcile transactions presented to Chase Paymentech for authorization with transactions presented to Chase Paymentech for settlement. The report will assist merchants with identifying operational issues that may be impacting their bottom line. This report is a snapshot of a given moment in time. As such, it can only be set up as a daily frequency report (monthly or weekly are not allowed) and cannot be re-generated for a historical date or date range. Outstanding transaction details are stored for 30 days only. On the 31st day a transaction will fall off of this report whether or not it has been funded.','Merchants use this data to trigger their reconciliation and/or collection and recovery processes. This may include research, updates to their customer databases, contact with the endpoint or the customer, etc.');
insert into report.dfr_report_def(header_record_type_id, data_record_type_id, name, description, usage) values ('HACT0002', 'RACT0002', 'Exception Detail', 'The ACT0002 Report provides transaction level detail for unsuccessful deposit transactions. Exception items within the scope of this report include rejected transactions, declined deposits and transactions that were included in a submission that was cancelled in its entirety.', 'Merchants use this report to verify what was sent and processed against the merchant''s own internal transaction reports. Specific usage will vary by merchant, but may be used for reconciliation or research of deposit activity. Any transaction that appears on this report was not included in a merchant''s funded activity for the activity date of the report. This information can be extremely valuable in reconciling a merchant''s submitted transaction activity to a bank account statement or other Chase Paymentech Solutions report, such as the FIN0010 - Deposit Summary.');
insert into report.dfr_report_def(header_record_type_id, data_record_type_id, name, description, usage) values ('HACT0010', 'RACT0010', 'Deposit Detail', 'The ACT0010 Report provides transaction level detail for successful deposit transactions.','Merchants use this report to verify what was sent and processed against the merchant''s own internal transaction reports. Specific usage will vary by merchant, but may be used for reconciliation or research of deposit activity. The ACT0010 report is available in multiple options. The format and total data record length will vary depending on the report options selected.');
insert into report.dfr_report_def(header_record_type_id, data_record_type_id, name, description, usage) values ('HACT0027', 'RACT0027', 'Terminal/Batch Detail', 'This delimited report provides transaction level detail (sales and refunds) received from a POS terminal or similar device presented by batch. This report is specific to retail merchants.','Merchants use this report to compare and identify discrepancies between the transaction detail reports from their terminals and those reported in the Terminal Batch Detail Report. (ACT0027)');
insert into report.dfr_report_def(header_record_type_id, data_record_type_id, name, description, usage) values ('HACT0036', 'RACT0036', 'Authorization Detail', 'This delimited file report provides transaction detail information on credit card authorizations and ECP verification/validation and pre-notes for Merchants. The report is available in coma (,), pipe (|) or tab delimited format.','Merchants use this report to help justify credit card authorizations, ECP Verification/Validation and Pre-Notes fees and verifies activity reporting in the submission summary and service charge detail.');
insert into report.dfr_report_def(header_record_type_id, data_record_type_id, name, description, usage) values ('HANS0013', 'RANS0013', 'Interchange Qualification Detail', 'This delimited file report provides transaction detail information for Merchants on front end downgrades to facilitate further research','Merchants use this report to see if they are qualifying for the best possible rates based on their business environment.');
insert into report.dfr_report_def(header_record_type_id, data_record_type_id, name, description, usage) values ('HANS0016', 'RANS0016', 'Interchange Qualification Summary', 'This delimited report provides merchants with a summary of different Bankcard Interchange Rates which their transactions have qualified for in a given time frame. It is a snapshot which identifies if the merchant is qualifying at the best possible rates based on their specific business environment.','Merchants use this data to determine if their transactions are qualifying at the best possible rate.');
insert into report.dfr_report_def(header_record_type_id, data_record_type_id, name, description, usage) values ('HANS0017', 'RANS0017', 'Interchange Downgrade Summary', 'This delimited file report provides merchants with a summary of Front End Interchange Downgrades for which their transactions have qualified for in a given time frame. It will report Front End downgrade reasons within each Interchange code and (MOP) Method of Payment and should help merchants to identify their most common Front End downgrade reasons within each Interchange Code.','Merchants use this report to help identify the most common Front End downgrade reasons within each interchange code.');
insert into report.dfr_report_def(header_record_type_id, data_record_type_id, name, description, usage) values ('HACT0069', 'RACT0069', 'IBAN Conversion Detail', 'This delimited file report provides merchants with the detail needed to support the Euro-zone''s migration from legacy bank account numbers and SORT Codes to International Bank Account Number s (IBANs) and Bank Identification Codes (BICs) in order to process European Direct Debit (EUDD) payments.','Merchants use this report to help identify IBANs and BICs provided to consumers that use (European) Direct Debit as a method of payment but have not provided merchants the required account details in the new format.');
--insert into report.dfr_report_def(header_record_type_id, data_record_type_id, name, description, usage) values ('HPDE0017', 'RPDE0017S', 'Chargeback Activity Summary', 'Provides a detailed listing of chargeback transactions by division at each stage of the chargeback life cycle. Identifies new chargebacks received, returned to the merchant, received for recourse or represented to the issuing bank.','Merchants use this data to trigger their chargeback management process. This may include research, updates to their customer databases, gathering documentation for representments, etc.');
insert into report.dfr_report_def(header_record_type_id, data_record_type_id, name, description, usage) values ('HPDE0017', 'RPDE0017D', 'Chargeback Activity Detail', 'Provides a detailed listing of chargeback transactions by division at each stage of the chargeback life cycle. Identifies new chargebacks received, returned to the merchant, received for recourse or represented to the issuing bank.','Merchants use this data to trigger their chargeback management process. This may include research, updates to their customer databases, gathering documentation for representments, etc.');
--insert into report.dfr_report_def(header_record_type_id, data_record_type_id, name, description, usage) values ('HPDE0018', 'RPDE0018S', 'ECP Returns Summary', 'Provides a detailed listing of ECP return transactions by division at each stage of the ECP return life cycle. Identifies any new ECP returns received, returned to the merchant, or represented to the depository financial institution.','Merchants use this data to trigger their ECP return management process. This may include research, updates to their customer databases, pursuing alternate methods of payment, etc.');
insert into report.dfr_report_def(header_record_type_id, data_record_type_id, name, description, usage) values ('HPDE0018', 'RPDE0018D', 'ECP Returns Detail', 'Provides a detailed listing of ECP return transactions by division at each stage of the ECP return life cycle. Identifies any new ECP returns received, returned to the merchant, or represented to the depository financial institution.','Merchants use this data to trigger their ECP return management process. This may include research, updates to their customer databases, pursuing alternate methods of payment, etc.');
insert into report.dfr_report_def(header_record_type_id, data_record_type_id, name, description, usage) values ('HPDE0020', 'RPDE0020', 'Chargebacks Received', 'Provides a detailed listing of new chargeback transactions received by Transaction Division. This report is similar to the PDE0017 Chargeback Activity Report, except that it includes detail only for new chargebacks received. It does not include items returned to the merchant, received for recourse or represented to the issuing bank, nor does it include the summary section.','Merchants use this data to trigger their chargeback management process. This may include research, updates to their customer databases, gathering documentation for representments, etc.');
insert into report.dfr_report_def(header_record_type_id, data_record_type_id, name, description, usage) values ('HPDE0022', 'RPDE0022', 'ECP Returns Received', 'Provides a detailed listing of new ECP return transactions received by Transaction Division. Identifies any new ECP returns received, returned to the merchant, or represented to the depository financial institution. This report is similar to the PDE0018 ECP Return Activity report, except that it includes detail only for new ECP Returns received. It does not include items returned to the merchant or represented to the issuing bank, nor does it include the summary section.','Merchants use this data to trigger their ECP return management process. This may include research, updates to their customer databases, pursuing alternate methods of payment, etc.');
insert into report.dfr_report_def(header_record_type_id, data_record_type_id, name, description, usage) values ('HPDE0029', 'RPDE0029', 'Retrieval Detail', 'The Retrieval Detail report provides a detailed listing of retrieval requests received from issuers by division and due date. Retrieval requests must be responded to in a timely manner or the merchant runs the risk of experiencing a chargeback and may forfeit any representment rights. Card not present merchants have their retrieval requests fulfilled automatically by Chase Paymentech, with the exception of Discover retrievals. Card not present merchants processing Discover settled with Chase Paymentech will need this report to identify their Discover retrievals.','Merchants use this report to trigger their internal retrieval request fulfillment processes, such as obtaining copies of sales slips or other transaction validation documentation and providing it to Chase Paymentech to forward to the issuing bank.');
insert into report.dfr_report_def(header_record_type_id, data_record_type_id, name, description, usage) values ('HPDE0036', 'RPDE0036', 'Debit Adjustments', 'Provides a detailed listing of Debit Adjustment transactions by division at each stage of the Debit Adjustment life cycle. Debit adjustments are transactions that were originally presented to the debit networks for settlement and have been disputed by a cardholder or refused by the bank. This report identifies any new Debit Adjustments received, returned to the merchant, or represented to the depository financial institution. The report will provide transaction level details about each Debit Adjustment (Merchant Initiated) and Debit Chargeback (Issuer Initiated) transaction.','Merchants use this data to trigger their Debit Adjustment management process. This may include research, updates to their customer databases, pursuing alternate methods of payment, etc.');
insert into report.dfr_report_def(header_record_type_id, data_record_type_id, name, description, usage) values ('HACT0019', 'RACT0019', 'Notification of Change', 'Provides a detailed listing of updated bank account information for ECP transactions. The source of the updates may be the Receiving Depository Financial Institution (RDFI), which provides notifications via the Federal Reserve Bank, or Chase Paymentech may initiate the notification based on prior transactions.','Merchants use this data to update their internal systems with corrected bank account information to prevent errors and financial losses on subsequent ECP transactions. The source of the Notification of Change (NOC) helps the merchant determine whether or not they have been updating their systems with previous NOC information provided by Chase Paymentech.');
insert into report.dfr_report_def(header_record_type_id, data_record_type_id, name, description, usage) values ('HRSK0030', 'RRSK0030', 'MasterCard Excessive Chargeback', 'This delimited report provides an account of the MasterCard Merchant Chargeback to Transaction Ratio (CTR) activity. It is used to monitor a merchant''s status in the MC Excessive Chargeback Program.','This report allows a merchant to review their Chargeback to Sales activity and CTR (ratio). It is expected to assist the merchant in monitoring their activity and in preventing reporting fees and penalties assessed in the MC Excessive Chargeback Program.');
insert into report.dfr_report_def(header_record_type_id, data_record_type_id, name, description, usage) values ('HRSK0036', 'RRSK0036', 'Discover Chargeback Summary', 'This delimited summary report provides merchants with counts and amounts of Discover chargeback activity to net sales and calculates their chargeback ratio. This data is used in determining whether a merchant''s chargeback activity is qualified by Discover as excessive.','Merchants and internal users use this report to monitor and track Discover chargeback activity to ensure compliance and prevent penalties in relation to Discover''s excessive chargeback guidelines. The user should be able to run this report monthly to produce a snapshot of their status in relation to these guidelines.');
insert into report.dfr_report_def(header_record_type_id, data_record_type_id, name, description, usage) values ('HACT0062', 'RACT0062', 'E-File Image Upload Exception', 'This delimited file report provides detail information on exceptions in the image upload process.','Merchants use this report to help identify any upload issues relating to E-File and determine what areas are involved in the exceptions.');
insert into report.dfr_report_def(header_record_type_id, data_record_type_id, name, description, usage) values ('HINF0042', 'RINF0042', 'Merchant FX Rate File', 'This report is used by merchants who participate in Chase Paymentech Cross Currency settlement program. The INF0042 report provides a list of each currency pair, presentment currency and settlement currency, exchange rate (FX) for the following business day.','Merchants use this data as advance notice of the exchange rate to be applied for the next activity date.');
insert into report.dfr_report_def(header_record_type_id, data_record_type_id, name, description, usage) values ('HACT0078', 'RACT0078', 'Deposit Activity Attributes', 'This delimited file report provides transaction level informational attributes on transactions processed by the entity. The report is available in comma, pipe or tab delimited format.','Merchants use this report to supplement information regarding deposited transactions associated with payment processing. The ACT0078 report is not intended to be used as a reconciliation tool.');
insert into report.dfr_report_def(header_record_type_id, data_record_type_id, name, description, usage) values ('*DFRBEG', null, 'File Header', 'Header', 'Header');
insert into report.dfr_report_def(header_record_type_id, data_record_type_id, name, description, usage) values ('*DFREND', null, 'File Trailer', 'Trailer', 'Trailer');

update report.dfr_report_def
set table_name = 'REPORT.DFR_CHARGEBACK', enabled = 'Y'
where data_record_type_id = 'RPDE0017D';

CREATE TABLE REPORT.DFR_REPORT_FIELD_DEF (
	DFR_REPORT_FIELD_DEF_ID NUMBER NOT NULL,
	DFR_REPORT_DEF_ID NUMBER NOT NULL,
	NAME VARCHAR2(256) NOT NULL,
	DESCRIPTION VARCHAR2(4000),
	FIELD_NUMBER NUMBER NOT NULL,
	FIELD_LENGTH NUMBER NOT NULL,
	DATA_TYPE VARCHAR2(32) NOT NULL,
	DATA_FORMAT VARCHAR2(256),
	COLUMN_NAME VARCHAR2(50),
	IS_KEY CHAR(1) DEFAULT 'N' NOT NULL,
	CREATED_BY VARCHAR2(30) NOT NULL,
	CREATED_TS DATE NOT NULL,
	LAST_UPDATED_BY VARCHAR2(30) NOT NULL,
	LAST_UPDATED_TS DATE NOT NULL,
	CONSTRAINT PK_DFR_REPORT_FIELD_DEF PRIMARY KEY(DFR_REPORT_FIELD_DEF_ID) USING INDEX TABLESPACE REPORT_INDX);

CREATE SEQUENCE REPORT.SEQ_DFR_REPORT_FIELD_DEF
	MINVALUE 1 
	MAXVALUE 999999999999999999999999999 
	INCREMENT BY 1 
	START WITH 1000
	CACHE 20 
	NOORDER 
	NOCYCLE ;

CREATE OR REPLACE TRIGGER REPORT.TRBI_DFR_REPORT_FIELD_DEF BEFORE INSERT ON REPORT.DFR_REPORT_FIELD_DEF
	FOR EACH ROW
BEGIN
	IF :NEW.DFR_REPORT_FIELD_DEF_ID IS NULL THEN
		SELECT SEQ_DFR_REPORT_FIELD_DEF.NEXTVAL INTO :NEW.DFR_REPORT_FIELD_DEF_ID FROM DUAL;
	END IF;
	SELECT 
		USER,
		SYSDATE,
		SYSDATE,
		USER
	INTO 
		:NEW.CREATED_BY,
		:NEW.CREATED_TS,
		:NEW.LAST_UPDATED_TS,
		:NEW.LAST_UPDATED_BY
	FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER REPORT.TRBU_DFR_REPORT_FIELD_DEF BEFORE UPDATE ON REPORT.DFR_REPORT_FIELD_DEF
	FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.CREATED_BY,
		:NEW.CREATED_TS,
		:NEW.LAST_UPDATED_TS,
		:NEW.LAST_UPDATED_BY
	FROM DUAL;
END;
/

CREATE INDEX REPORT.IX_DFR_FIELD_REPORT_DEF_ID ON REPORT.DFR_REPORT_FIELD_DEF(DFR_REPORT_DEF_ID) TABLESPACE REPORT_INDX ONLINE;

GRANT SELECT,INSERT,UPDATE ON REPORT.DFR_REPORT_FIELD_DEF TO USALIVE_APP_ROLE;
GRANT SELECT,INSERT,UPDATE ON REPORT.DFR_REPORT_FIELD_DEF TO USAT_DMS_ROLE;
GRANT SELECT,INSERT,UPDATE ON REPORT.DFR_REPORT_FIELD_DEF TO USAT_POSM_ROLE;

declare 
  ln_report_def_id NUMBER;
begin
  select dfr_report_def.dfr_report_def_id 
  into ln_report_def_id
  from report.dfr_report_def
  where data_record_type_id = 'RPDE0017D';
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, null, 'N', 1, 2, 'java.lang.String', null, 'Entity Type', 'Level at which reported event occurred. For this report, value will always be: TD Transaction Division');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'ENTITY_NUM', 'Y', 2, 10, 'java.lang.Integer', null, 'Entity #', 'Associated TD identification number, less any leading zeroes. Chase Paymentech assigned');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'CHARGEBACK_AMOUNT', 'N', 3, 17, 'java.lang.Double', null, 'Issuer Chargeback Presentment Currency Amount', 'The amount of this chargeback at the time it was received from the issuer, expressed in the cardholder''s Presentment Currency. The amount will always be preceded by either a dash (-) if the value is negative or by a space if the value is positive.');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'PREVIOUS_PARTIAL', 'N', 4, 1, 'java.lang.String', null, 'Previous Partial Representment', 'Indicates if a partial representment was processed as part of an earlier action on this chargeback.');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'CHARGEBACK_CURRENCY', 'N', 5, 3, 'java.lang.String', null, 'Presentment Currency', 'Three character abbreviation for currency being reported.');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'ACTIVITY_CD', 'Y', 6, 6, 'java.lang.String', null, 'Category', 'Indicates the type of activity represented in this record. For this report, value will always be: RECD CB/Returns Received');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'STATUS_CD', 'N', 7, 1, 'java.lang.String', null, 'Status flag', 'Set to "R" if this CB activity is related to a Reversed item - may be blank');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'SEQUENCE_NUM', 'Y', 8, 10, 'java.lang.Integer', null, 'Sequence #', 'Chase Paymentech assigned chargeback transaction identification number');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'MERCHANT_ORDER_NUM', 'N', 9, 22, 'java.lang.String', null, 'Merchant Order #', 'The order number submitted by the merchant with the original sale');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'ACCOUNT_NUM', 'N', 10, 22, 'java.lang.String', null, 'Account #', 'Customer''s credit card account number');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'REASON_CD', 'N', 11, 3, 'java.lang.String', null, 'Reason Code', 'Code used to identify the reason for the Chargeback. Reason codes are subject to change periodically');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'TRANSACTION_DATE', 'N', 12, 10, 'java.util.Date', 'MM/dd/yyyy', 'Transaction Date', 'Date of the original transaction');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'INITIATED_DATE', 'N', 13, 10, 'java.util.Date', 'MM/dd/yyyy', 'Chargeback Date', 'Date that the chargeback was initiated by the issuing bank');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'ACTIVITY_DATE', 'N', 14, 10, 'java.util.Date', 'MM/dd/yyyy', 'Activity Date', 'Chase Paymentech''s activity date for this chargeback');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'ACTIVITY_AMOUNT', 'N', 15, 17, 'java.lang.Double', null, 'Presentment Currency Chargeback Amount', 'The value of the items included in a Data Record, expressed in the cardholder''s presentment currency. The amount will always be preceded by either a dash (-) if the value is negative or by a space if the value is positive.');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'FEE_AMOUNT', 'N', 16, 17, 'java.lang.Double', null, 'Fee Amount', 'The amount of the fee that may be assessed by the issuing bank for certain MasterCard chargeback reason codes. The amount will always be preceded by either a dash (-) if the value is negative or by a space if the value is positive.');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'USAGE_CD', 'N', 17, 1, 'java.lang.Integer', null, 'Usage Code', 'Single digit code 1, 2, 3 identifying whether this is a first or second chargeback. May be blank.');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'MOP_CD', 'N', 18, 2, 'java.lang.String', null, 'MOP Code', 'Method of payment code describing the Payment Type associated to the chargeback.');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'AUTH_DATE', 'N', 19, 10, 'java.util.Date', null, 'Authorization Date', 'Date of the original authorization. May be blank.');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'DUE_DATE', 'N', 20, 10, 'java.util.Date', null, 'Chargeback Due Date', 'This date represents the due date of the chargeback May be blank.');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, null, 'N', 21, 15, 'java.lang.String', null, 'Ticket #', 'An alpha-numeric ticket number submitted with the original sale. This field is only applicable to merchants with the Airline MCC codes. May be blank');
end;
/

CREATE TABLE REPORT.DFR_REPORT (
	DFR_REPORT_ID NUMBER NOT NULL,
	DFR_REPORT_DEF_ID NUMBER NOT NULL,
	FILE_CACHE_ID NUMBER,
	HEADER_GENERATION_TS DATE NOT NULL,
	PROCESS_STATUS_CD VARCHAR(16),
	PROCESS_START_TS DATE,
	PROCESS_FINISH_TS DATE,
	PROCESS_MESSAGE VARCHAR2(4000),
	CREATED_BY VARCHAR2(30) NOT NULL,
	CREATED_TS DATE NOT NULL,
	LAST_UPDATED_BY VARCHAR2(30) NOT NULL,
	LAST_UPDATED_TS DATE NOT NULL,
	CONSTRAINT PK_DFR_REPORT PRIMARY KEY(DFR_REPORT_ID) USING INDEX TABLESPACE REPORT_INDX);

COMMENT ON COLUMN REPORT.DFR_REPORT.PROCESS_STATUS_CD IS 'IP=In-Process, S=Success, F=Failed';

CREATE SEQUENCE REPORT.SEQ_DFR_REPORT
	MINVALUE 1 
	MAXVALUE 999999999999999999999999999 
	INCREMENT BY 1 
	START WITH 1000
	CACHE 20 
	NOORDER 
	NOCYCLE ;

CREATE OR REPLACE TRIGGER REPORT.TRBI_DFR_REPORT BEFORE INSERT ON REPORT.DFR_REPORT
	FOR EACH ROW
BEGIN
	IF :NEW.DFR_REPORT_ID IS NULL THEN
		SELECT SEQ_DFR_REPORT.NEXTVAL INTO :NEW.DFR_REPORT_ID FROM DUAL;
	END IF;
	SELECT 
		USER,
		SYSDATE,
		SYSDATE,
		USER
	INTO 
		:NEW.CREATED_BY,
		:NEW.CREATED_TS,
		:NEW.LAST_UPDATED_TS,
		:NEW.LAST_UPDATED_BY
	FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER REPORT.TRBU_DFR_REPORT BEFORE UPDATE ON REPORT.DFR_REPORT
	FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.CREATED_BY,
		:NEW.CREATED_TS,
		:NEW.LAST_UPDATED_TS,
		:NEW.LAST_UPDATED_BY
	FROM DUAL;
END;
/

CREATE INDEX REPORT.IX_DFR_REPORT_FILE_CACHE_ID ON REPORT.DFR_REPORT(FILE_CACHE_ID) TABLESPACE REPORT_INDX ONLINE;
CREATE INDEX REPORT.IX_DFR_REPORT_DEF_ID ON REPORT.DFR_REPORT(DFR_REPORT_DEF_ID) TABLESPACE REPORT_INDX ONLINE;
CREATE UNIQUE INDEX REPORT.IX_DFR_REPORT_AK ON REPORT.DFR_REPORT(FILE_CACHE_ID, DFR_REPORT_DEF_ID, HEADER_GENERATION_TS) TABLESPACE REPORT_INDX ONLINE;

GRANT SELECT,INSERT,UPDATE ON REPORT.DFR_REPORT TO USALIVE_APP_ROLE;
GRANT SELECT,INSERT,UPDATE ON REPORT.DFR_REPORT TO USAT_DMS_ROLE;
GRANT SELECT,INSERT,UPDATE ON REPORT.DFR_REPORT TO USAT_POSM_ROLE;

CREATE TABLE REPORT.DFR_CHARGEBACK (
	DFR_CHARGEBACK_ID NUMBER NOT NULL ENABLE,
	DFR_REPORT_ID NUMBER  NOT NULL ENABLE,
	ENTITY_NUM NUMBER NOT NULL ENABLE,
	CHARGEBACK_AMOUNT NUMBER(15,2),
	PREVIOUS_PARTIAL CHAR(1) DEFAULT 'N' NOT NULL ENABLE,
	CHARGEBACK_CURRENCY VARCHAR(128),
	ACTIVITY_CD VARCHAR(128) NOT NULL,
	STATUS_CD CHAR(1),
	SEQUENCE_NUM NUMBER,
	MERCHANT_ORDER_NUM VARCHAR(128),
	ACCOUNT_NUM VARCHAR(128),
	REASON_CD VARCHAR(128),
	TRANSACTION_DATE DATE,
	INITIATED_DATE DATE,
	ACTIVITY_DATE DATE,
	ACTIVITY_AMOUNT NUMBER(15,2),
	FEE_AMOUNT NUMBER(15,2),
	USAGE_CD NUMBER,
	MOP_CD VARCHAR(128),
	AUTH_DATE DATE,
	DUE_DATE DATE,
	CREATED_BY VARCHAR2(30) NOT NULL,
	CREATED_TS DATE NOT NULL,
	LAST_UPDATED_BY VARCHAR2(30) NOT NULL,
	LAST_UPDATED_TS DATE NOT NULL,
	CONSTRAINT PK_DFR_CHARGEBACK PRIMARY KEY(DFR_CHARGEBACK_ID) USING INDEX TABLESPACE REPORT_INDX);

CREATE SEQUENCE REPORT.SEQ_DFR_CHARGEBACK
	MINVALUE 1 
	MAXVALUE 999999999999999999999999999 
	INCREMENT BY 1 
	START WITH 100
	CACHE 20 
	NOORDER 
	NOCYCLE ;

CREATE OR REPLACE TRIGGER REPORT.TRBI_DFR_CHARGEBACK BEFORE INSERT ON REPORT.DFR_CHARGEBACK
	FOR EACH ROW
BEGIN
	IF :NEW.DFR_CHARGEBACK_ID IS NULL THEN
		SELECT SEQ_DFR_CHARGEBACK.NEXTVAL INTO :NEW.DFR_CHARGEBACK_ID FROM DUAL;
	END IF;
	SELECT 
		USER,
		SYSDATE,
		SYSDATE,
		USER
	INTO 
		:NEW.CREATED_BY,
		:NEW.CREATED_TS,
		:NEW.LAST_UPDATED_TS,
		:NEW.LAST_UPDATED_BY
	FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER REPORT.TRBU_DFR_CHARGEBACK BEFORE UPDATE ON REPORT.DFR_CHARGEBACK
	FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.CREATED_BY,
		:NEW.CREATED_TS,
		:NEW.LAST_UPDATED_TS,
		:NEW.LAST_UPDATED_BY
	FROM DUAL;
END;
/

CREATE INDEX REPORT.IX_DFR_CHARGEBACK_ORDER_NUM ON REPORT.DFR_CHARGEBACK(MERCHANT_ORDER_NUM) TABLESPACE REPORT_INDX ONLINE;
CREATE INDEX REPORT.IX_DFR_CHARGEBACK_ACTIVITY ON REPORT.DFR_CHARGEBACK(ENTITY_NUM,ACTIVITY_CD,SEQUENCE_NUM) TABLESPACE REPORT_INDX ONLINE;

GRANT SELECT,INSERT,UPDATE ON REPORT.DFR_CHARGEBACK TO USALIVE_APP_ROLE;
GRANT SELECT,INSERT,UPDATE ON REPORT.DFR_CHARGEBACK TO USAT_DMS_ROLE;
GRANT SELECT,INSERT,UPDATE ON REPORT.DFR_CHARGEBACK TO USAT_POSM_ROLE;

insert into engine.app_setting(app_setting_cd, app_setting_value, app_setting_desc) values ('DFR_LAST_RUN_TS', '0', 'Last timestamp Paymentech DFR files were parsed.');
