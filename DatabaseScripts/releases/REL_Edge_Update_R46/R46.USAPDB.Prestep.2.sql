CREATE UNIQUE INDEX REPORT.UDX_CAMPAIGN_PROMO_CODE ON REPORT.CAMPAIGN(PROMO_CODE) TABLESPACE REPORT_INDX ONLINE;

DROP INDEX REPORT.UIX_TSBD_AK;

CREATE UNIQUE INDEX REPORT.UIX_TSBD_AK ON REPORT.TRANS_STAT_BY_DAY(TRAN_DATE, TERMINAL_ID, TRANS_TYPE_ID, CURRENCY_ID, EPORT_ID, CAMPAIGN_ID) LOCAL
    TABLESPACE REPORT_INDX ONLINE;

CREATE INDEX REPORT.AK_ACTIVITY_REF_CAMPAIGN_ID ON REPORT.ACTIVITY_REF(CAMPAIGN_ID) TABLESPACE REPORT_INDX ONLINE;

GRANT SELECT ON REPORT.BUSINESS_TYPE TO APP_LAYER WITH GRANT OPTION;

update report.business_type set mcc = 7994 where business_type_name = 'Amusement';
update report.business_type set mcc = 5999 where business_type_name = 'Business Express';
update report.business_type set mcc = 7542 where business_type_name = 'Car Wash';
update report.business_type set mcc = 5814 where business_type_name = 'Catering';
update report.business_type set mcc = 5814 where business_type_name = 'Coffee/Water';
update report.business_type set mcc = 5814 where business_type_name = 'Dining Center';
update report.business_type set mcc = 5999 where business_type_name = 'Direct Store Delivery';
update report.business_type set mcc = 5999 where business_type_name = 'ePort Mobile';
update report.business_type set mcc = 5999 where business_type_name = 'ePort Online';
update report.business_type set mcc = 7211 where business_type_name = 'eSuds';
update report.business_type set mcc = 7211 where business_type_name = 'eSuds Commercial';
update report.business_type set mcc = 5999 where business_type_name = 'Kiosk';
update report.business_type set mcc = 7211 where business_type_name = 'Laundry';
update report.business_type set mcc = 5999 where business_type_name = 'Micro Market';
update report.business_type set mcc = 5999 where business_type_name = 'Other Non-Traditional';
update report.business_type set mcc = 4121 where business_type_name = 'Transportation';
update report.business_type set mcc = 5814 where business_type_name = 'Vending';

COMMIT;

CREATE UNIQUE INDEX PSS.AK_CONSUMER_EMAIL_ADDR1 ON PSS.CONSUMER (UPPER(CONSUMER_EMAIL_ADDR1), DECODE(CONSUMER_TYPE_ID, 7, 5, CONSUMER_TYPE_ID), CASE WHEN CONSUMER_EMAIL_ADDR1 IS NULL THEN CONSUMER_ID END)
  TABLESPACE PSS_INDX
  ONLINE;
  
CREATE UNIQUE INDEX PSS.AK_CONSUMER_CELL_PHONE_NUM ON PSS.CONSUMER (UPPER(CONSUMER_CELL_PHONE_NUM), DECODE(CONSUMER_TYPE_ID, 7, 5, CONSUMER_TYPE_ID), CASE WHEN CONSUMER_CELL_PHONE_NUM IS NULL THEN CONSUMER_ID END)
  TABLESPACE PSS_INDX
  ONLINE;
  
DROP INDEX PSS.AK_CONSUMER_EMAIL;
DROP INDEX PSS.AK_CONSUMER_CELL;

GRANT SELECT ON PSS.MERCHANT TO AUTHORITY;
GRANT SELECT ON PSS.POS TO AUTHORITY;
GRANT SELECT ON PSS.POS_PTA TO AUTHORITY;
GRANT SELECT ON PSS.PAYMENT_SUBTYPE TO AUTHORITY;
GRANT SELECT ON DEVICE.DEVICE TO AUTHORITY;

GRANT EXECUTE ON DBADMIN.GET_LONG TO PSS;

GRANT SELECT ON DEVICE.HOST_TYPE_HOST_GROUP_TYPE TO USALIVE_APP_ROLE;
GRANT SELECT ON DEVICE.BEZEL_MFGR TO USALIVE_APP_ROLE;
GRANT SELECT ON DEVICE.APP_TYPE TO USALIVE_APP_ROLE;
