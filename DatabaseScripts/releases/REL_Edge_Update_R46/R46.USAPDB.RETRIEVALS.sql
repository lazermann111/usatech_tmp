declare 
  ln_report_def_id NUMBER;
begin
  select dfr_report_def.dfr_report_def_id 
  into ln_report_def_id
  from report.dfr_report_def
  where data_record_type_id = 'RPDE0029';
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, null, 'N', 1, 2, 'java.lang.String', null, 'Entity Type', 'Level at which reported event occurred. For this report, value will always be: TD Transaction Division');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'ENTITY_NUM', 'Y', 2, 10, 'java.lang.Integer', null, 'Entity #', 'Associated TD identification number, less any leading zeroes. Chase Paymentech assigned');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'CURRENCY', 'N', 3, 3, 'java.lang.String', null, 'Currency', 'Three character abbreviation for currency being reported.');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'ACTIVITY_CD', 'Y', 4, 6, 'java.lang.String', null, 'Category', 'Indicates the type of activity represented in this record. For this report, value will always be: RETREQ Retrieval Request');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'SEQUENCE_NUM', 'Y', 5, 10, 'java.lang.Integer', null, 'Sequence #', 'Chase Paymentech assigned chargeback transaction identification number');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'MERCHANT_ORDER_NUM', 'N', 6, 22, 'java.lang.String', null, 'Merchant Order #', 'The order number submitted by the merchant with the original sale');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'ACCOUNT_NUM', 'N', 7, 22, 'java.lang.String', null, 'Account #', 'Customer''s credit card account number');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'AUTH_CD', 'N', 8, 6, 'java.lang.String', null, 'Authorization Code', 'The authorization code that was returned by the issuer. This field may be blank');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'REASON_CD', 'N', 9, 3, 'java.lang.String', null, 'Reason Code', 'Code used to identify the reason for the Retrieval Request. Reason codes are subject to change.');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'TRANSACTION_DATE', 'N', 10, 10, 'java.util.Date', 'MM/dd/yyyy', 'Transaction Date', 'Date of the original transaction');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'RECEIVED_DATE', 'N', 11, 10, 'java.util.Date', 'MM/dd/yyyy', 'Activity Date', 'Chase Paymentech''s activity date for this chargeback');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'DUE_DATE', 'N', 12, 10, 'java.util.Date', null, 'Chargeback Due Date', 'This date represents the due date of the chargeback May be blank.');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'AMOUNT', 'N', 13, 17, 'java.lang.Double', null, 'Presentment Currency Chargeback Amount', 'The value of the items included in a Data Record, expressed in the cardholder''s presentment currency. The amount will always be preceded by either a dash (-) if the value is negative or by a space if the value is positive.');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'TERMINAL_NUM', 'N', 14, 16, 'java.lang.Integer', null, 'Terminal #', 'The number assigned to the terminal from which the batch that contained the referenced transaction was submitted');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'BATCH_NUM', 'N', 15, 14, 'java.lang.Integer', null, 'Batch #', 'The number assigned to the batch in which the referenced transaction was submitted');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'FULFILLMENT_TYPE', 'N', 16, 4, 'java.lang.String', null, 'Fulfillment Attribute', 'This field indicates whether the retrieval request was auto-fulfilled by Chase Paymentech or whether a fulfillment response from the merchant is required Valid values: AUTO Auto-fulfilled by Chase Paymentech, RESP Merchant response required by the due date indicated');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'AUTH_DATE', 'N', 17, 10, 'java.util.Date', 'MM/dd/yyyy', 'Authorization Date', 'Date of the authorization performed for this transaction.');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'ARN', 'N', 18, 23, 'java.lang.String', null, 'ARN', 'The acquirer�s reference number assigned to the transaction at the time of the sale/refund.');
end;
/

CREATE TABLE REPORT.DFR_RETRIEVAL (
	DFR_RETRIEVAL_ID NUMBER NOT NULL ENABLE,
	DFR_REPORT_ID NUMBER  NOT NULL ENABLE,
	ENTITY_NUM NUMBER NOT NULL ENABLE,
	CURRENCY VARCHAR(3) NOT NULL ENABLE,
	ACTIVITY_CD VARCHAR(6),
	SEQUENCE_NUM NUMBER,
	MERCHANT_ORDER_NUM VARCHAR(22),
	ACCOUNT_NUM VARCHAR(22),
	AUTH_CD VARCHAR(6),
	REASON_CD VARCHAR(3),
	TRANSACTION_DATE DATE,
	RECEIVED_DATE DATE,
	DUE_DATE DATE,
	AMOUNT NUMBER(15,2),
	TERMINAL_NUM NUMBER,
	BATCH_NUM NUMBER,
	FULFILLMENT_TYPE VARCHAR(4),
	AUTH_DATE DATE,
	ARN VARCHAR(23),
	CREATED_BY VARCHAR2(30) NOT NULL,
	CREATED_TS DATE NOT NULL,
	LAST_UPDATED_BY VARCHAR2(30) NOT NULL,
	LAST_UPDATED_TS DATE NOT NULL,
	CONSTRAINT PK_DFR_RETRIEVAL PRIMARY KEY(DFR_RETRIEVAL_ID) USING INDEX TABLESPACE REPORT_INDX);

CREATE SEQUENCE REPORT.SEQ_DFR_RETRIEVAL
	MINVALUE 1 
	MAXVALUE 999999999999999999999999999 
	INCREMENT BY 1 
	START WITH 100
	CACHE 20 
	NOORDER 
	NOCYCLE ;

CREATE OR REPLACE TRIGGER REPORT.TRBI_DFR_RETRIEVAL BEFORE INSERT ON REPORT.DFR_RETRIEVAL
	FOR EACH ROW
BEGIN
	IF :NEW.DFR_RETRIEVAL_ID IS NULL THEN
		SELECT SEQ_DFR_RETRIEVAL.NEXTVAL INTO :NEW.DFR_RETRIEVAL_ID FROM DUAL;
	END IF;
	SELECT 
		USER,
		SYSDATE,
		SYSDATE,
		USER
	INTO 
		:NEW.CREATED_BY,
		:NEW.CREATED_TS,
		:NEW.LAST_UPDATED_TS,
		:NEW.LAST_UPDATED_BY
	FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER REPORT.TRBU_DFR_RETRIEVAL BEFORE UPDATE ON REPORT.DFR_RETRIEVAL
	FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.CREATED_BY,
		:NEW.CREATED_TS,
		:NEW.LAST_UPDATED_TS,
		:NEW.LAST_UPDATED_BY
	FROM DUAL;
END;
/

CREATE INDEX REPORT.IX_DFR_RETRIEVAL_ORDER_NUM ON REPORT.DFR_RETRIEVAL(MERCHANT_ORDER_NUM) TABLESPACE REPORT_INDX ONLINE;

GRANT SELECT,INSERT,UPDATE ON REPORT.DFR_RETRIEVAL TO USALIVE_APP_ROLE;
GRANT SELECT,INSERT,UPDATE ON REPORT.DFR_RETRIEVAL TO USAT_DMS_ROLE;
GRANT SELECT,INSERT,UPDATE ON REPORT.DFR_RETRIEVAL TO USAT_POSM_ROLE;

update report.dfr_report_def
set table_name = 'REPORT.DFR_RETRIEVAL', enabled = 'Y'
where data_record_type_id = 'RPDE0029';

