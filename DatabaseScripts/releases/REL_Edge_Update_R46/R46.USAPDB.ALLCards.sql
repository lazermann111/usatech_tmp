INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, LITERAL_VALUE)
    VALUES('campaign-donation-instructions-title', 'Donation Percentage');
INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, LITERAL_VALUE)
    VALUES('campaign-donation-instructions-content', 'Use only if you wish to donate funds to charity');
COMMIT;

INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, LITERAL_VALUE)
    VALUES('campaign-morecards-instructions-title', 'For MORE cards');
INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, LITERAL_VALUE)
    VALUES('campaign-morecards-instructions-content', 'Check the option when the campaign is for MORE cards');
COMMIT;

INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, LITERAL_VALUE)
    VALUES('campaign-allcards-instructions-title', 'For All Other cards');
INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, LITERAL_VALUE)
    VALUES('campaign-allcards-instructions-content', 'Check the option when the campaign is for all other cards except MORE cards');
COMMIT;


INSERT INTO PSS.TRAN_LINE_ITEM_TYPE(TRAN_LINE_ITEM_TYPE_ID, TRAN_LINE_ITEM_TYPE_DESC, TRAN_LINE_ITEM_TYPE_SIGN_PN, TRAN_LINE_ITEM_TYPE_GROUP_CD, MDB_NUMBER_IND,TYPE_DESC_AS_PRODUCT_IND)
SELECT 211, 'MORE Loyalty Discount', 'N', 'A', 'N','N' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM PSS.TRAN_LINE_ITEM_TYPE WHERE TRAN_LINE_ITEM_TYPE_ID = 211);
COMMIT;

GRANT SELECT ON DEVICE.DEVICE_LAST_ACTIVE TO USAT_PREPAID_APP_ROLE;
GRANT SELECT ON REPORT.TRANS_TYPE TO USAT_PREPAID_APP_ROLE;
GRANT SELECT ON PSS.TRAN_LINE_ITEM TO USAT_PREPAID_APP_ROLE;

DECLARE 
	l_payment_subtype_key_id NUMBER;
	l_pos_id NUMBER;
	l_priority NUMBER:=1;
  	l_activation_ts DATE:=sysdate;
  	l_currency_cd VARCHAR(3):='USD';
BEGIN
select pos_id into l_pos_id from pss.pos where device_id=(select device_id from device.device where device_serial_cd='V1-1-USD');

select internal_payment_type_id into l_payment_subtype_key_id from pss.internal_payment_type where internal_payment_type_desc='Special Card (prepaid)';
  
insert into pss.pos_pta (pos_pta_id, pos_pta_activation_ts, pos_id, payment_subtype_id,payment_subtype_key_id,pos_pta_pin_req_yn_flag,pos_pta_priority, currency_cd, pos_pta_passthru_allow_yn_flag, pos_pta_disable_debit_denial, no_convenience_fee)
select pss.seq_pos_pta_id.NEXTVAL, l_activation_ts, l_pos_id, payment_subtype_id, l_payment_subtype_key_id,'N',l_priority,l_currency_cd,'N','N','N'
from pss.payment_subtype where payment_subtype_class='Internal::Prepaid' and client_payment_type_cd='S' and status_cd='A'
and not exists(select 1 from pss.pos_pta where pos_id=l_pos_id and payment_subtype_id in (select payment_subtype_id from pss.payment_subtype where payment_subtype_class='Internal::Prepaid' and client_payment_type_cd='S' and status_cd='A') and nvl(pos_pta_deactivation_ts, MAX_DATE)>sysdate);

select distinct payment_subtype_key_id into l_payment_subtype_key_id from pss.pos_pta pta join pss.payment_subtype ps on pta.payment_subtype_id=ps.payment_subtype_id 
where pos_id=l_pos_id and nvl(pos_pta_deactivation_ts, MAX_DATE)>sysdate
and ps.client_payment_type_cd='Y' and ps.payment_subtype_class='Authority::ISO8583::Elavon';

insert into pss.pos_pta (pos_pta_id, pos_pta_activation_ts, pos_id, payment_subtype_id,payment_subtype_key_id,pos_pta_pin_req_yn_flag,pos_pta_priority, currency_cd, pos_pta_passthru_allow_yn_flag, pos_pta_disable_debit_denial, no_convenience_fee)
select pss.seq_pos_pta_id.NEXTVAL, l_activation_ts,l_pos_id, payment_subtype_id, l_payment_subtype_key_id,'N',l_priority+rownum,l_currency_cd,'N','N','N'
from pss.payment_subtype where payment_subtype_class='Authority::ISO8583::Elavon' and client_payment_type_cd='C' and status_cd='A'
and not exists(select 1 from pss.pos_pta where pos_id=l_pos_id and payment_subtype_id in (select payment_subtype_id from pss.payment_subtype where payment_subtype_class='Authority::ISO8583::Elavon' and client_payment_type_cd='C' and status_cd='A') and nvl(pos_pta_deactivation_ts, MAX_DATE)>sysdate);

select terminal_id into l_payment_subtype_key_id from pss.terminal where merchant_id = (select merchant_id from pss.merchant where merchant_name='Error Bin');
insert into pss.pos_pta (pos_pta_id, pos_pta_activation_ts, pos_id, payment_subtype_id,payment_subtype_key_id,pos_pta_pin_req_yn_flag,pos_pta_priority, currency_cd, pos_pta_passthru_allow_yn_flag, pos_pta_disable_debit_denial, no_convenience_fee)
select pss.seq_pos_pta_id.NEXTVAL, l_activation_ts,l_pos_id, payment_subtype_id, l_payment_subtype_key_id,'N',6,l_currency_cd,'N','N','N'
from pss.payment_subtype where payment_subtype_name='Error Bin' and client_payment_type_cd='C' and status_cd='A'
and not exists(select 1 from pss.pos_pta where pos_id=l_pos_id and payment_subtype_id in (select payment_subtype_id from pss.payment_subtype where payment_subtype_name='Error Bin' and client_payment_type_cd='C' and status_cd='A') and nvl(pos_pta_deactivation_ts, MAX_DATE)>sysdate);

l_priority:=1;
select internal_payment_type_id into l_payment_subtype_key_id from pss.internal_payment_type where internal_payment_type_desc='Special Card (prepaid)';
  
insert into pss.pos_pta (pos_pta_id, pos_pta_activation_ts, pos_id, payment_subtype_id,payment_subtype_key_id,pos_pta_pin_req_yn_flag,pos_pta_priority, currency_cd, pos_pta_passthru_allow_yn_flag, pos_pta_disable_debit_denial, no_convenience_fee)
select pss.seq_pos_pta_id.NEXTVAL, l_activation_ts, l_pos_id, payment_subtype_id, l_payment_subtype_key_id,'N',l_priority,l_currency_cd,'N','N','N'
from pss.payment_subtype where payment_subtype_name='USAT ISO Card - Prepaid Card (Manual Entry)' and client_payment_type_cd='T' and status_cd='A'
and not exists(select 1 from pss.pos_pta where pos_id=l_pos_id and payment_subtype_id in (select payment_subtype_id from pss.payment_subtype where payment_subtype_name='USAT ISO Card - Prepaid Card (Manual Entry)' and client_payment_type_cd='T' and status_cd='A') and nvl(pos_pta_deactivation_ts, MAX_DATE)>sysdate);

select distinct payment_subtype_key_id into l_payment_subtype_key_id from pss.pos_pta pta join pss.payment_subtype ps on pta.payment_subtype_id=ps.payment_subtype_id 
where pos_id=l_pos_id and nvl(pos_pta_deactivation_ts, MAX_DATE)>sysdate
and ps.client_payment_type_cd='Y' and ps.payment_subtype_class='Authority::ISO8583::Elavon';

insert into pss.pos_pta (pos_pta_id, pos_pta_activation_ts, pos_id, payment_subtype_id,payment_subtype_key_id,pos_pta_pin_req_yn_flag,pos_pta_priority, currency_cd, pos_pta_passthru_allow_yn_flag, pos_pta_disable_debit_denial, no_convenience_fee)
select pss.seq_pos_pta_id.NEXTVAL, l_activation_ts,l_pos_id, payment_subtype_id, l_payment_subtype_key_id,'N',l_priority+rownum,l_currency_cd,'N','N','N'
from pss.payment_subtype where payment_subtype_class='Authority::ISO8583::Elavon' and client_payment_type_cd='N' and status_cd='A'
and not exists(select 1 from pss.pos_pta where pos_id=l_pos_id and payment_subtype_id in (select payment_subtype_id from pss.payment_subtype where payment_subtype_class='Authority::ISO8583::Elavon' and client_payment_type_cd='N' and status_cd='A') and nvl(pos_pta_deactivation_ts, MAX_DATE)>sysdate);

select terminal_id into l_payment_subtype_key_id from pss.terminal where merchant_id = (select merchant_id from pss.merchant where merchant_name='Error Bin');
insert into pss.pos_pta (pos_pta_id, pos_pta_activation_ts, pos_id, payment_subtype_id,payment_subtype_key_id,pos_pta_pin_req_yn_flag,pos_pta_priority, currency_cd, pos_pta_passthru_allow_yn_flag, pos_pta_disable_debit_denial, no_convenience_fee)
select pss.seq_pos_pta_id.NEXTVAL, l_activation_ts,l_pos_id, payment_subtype_id, l_payment_subtype_key_id,'N',6,l_currency_cd,'N','N','N'
from pss.payment_subtype where payment_subtype_name='Error Bin' and client_payment_type_cd='N' and status_cd='A'
and not exists(select 1 from pss.pos_pta where pos_id=l_pos_id and payment_subtype_id in (select payment_subtype_id from pss.payment_subtype where payment_subtype_name='Error Bin' and client_payment_type_cd='N' and status_cd='A') and nvl(pos_pta_deactivation_ts, MAX_DATE)>sysdate);

l_currency_cd:='CAD';

l_priority:=1;

select pos_id into l_pos_id from pss.pos where device_id=(select device_id from device.device where device_serial_cd='V1-1-CAD');

select internal_payment_type_id into l_payment_subtype_key_id from pss.internal_payment_type where internal_payment_type_desc='Special Card (prepaid)';

insert into pss.pos_pta (pos_pta_id, pos_pta_activation_ts, pos_id, payment_subtype_id,payment_subtype_key_id,pos_pta_pin_req_yn_flag,pos_pta_priority, currency_cd, pos_pta_passthru_allow_yn_flag, pos_pta_disable_debit_denial, no_convenience_fee)
select pss.seq_pos_pta_id.NEXTVAL, l_activation_ts, l_pos_id, payment_subtype_id, l_payment_subtype_key_id,'N',l_priority,l_currency_cd,'N','N','N'
from pss.payment_subtype where payment_subtype_class='Internal::Prepaid' and client_payment_type_cd='S' and status_cd='A'
and not exists(select 1 from pss.pos_pta where pos_id=l_pos_id and payment_subtype_id in (select payment_subtype_id from pss.payment_subtype where payment_subtype_class='Internal::Prepaid' and client_payment_type_cd='S' and status_cd='A') and nvl(pos_pta_deactivation_ts, MAX_DATE)>sysdate);

select distinct payment_subtype_key_id into l_payment_subtype_key_id from pss.pos_pta pta join pss.payment_subtype ps on pta.payment_subtype_id=ps.payment_subtype_id 
where pos_id=l_pos_id and nvl(pos_pta_deactivation_ts, MAX_DATE)>sysdate
and ps.client_payment_type_cd='Y' and ps.payment_subtype_class='Authority::ISO8583::FHMS::Paymentech';

insert into pss.pos_pta (pos_pta_id, pos_pta_activation_ts, pos_id, payment_subtype_id,payment_subtype_key_id,pos_pta_pin_req_yn_flag,pos_pta_priority, currency_cd, pos_pta_passthru_allow_yn_flag, pos_pta_disable_debit_denial, no_convenience_fee)
select pss.seq_pos_pta_id.NEXTVAL, l_activation_ts,l_pos_id, payment_subtype_id, l_payment_subtype_key_id,'N',l_priority+rownum,l_currency_cd,'N','N','N'
from pss.payment_subtype where payment_subtype_class='Authority::ISO8583::FHMS::Paymentech' and client_payment_type_cd='C' and status_cd='A'
and not exists(select 1 from pss.pos_pta where pos_id=l_pos_id and payment_subtype_id in (select payment_subtype_id from pss.payment_subtype where payment_subtype_class='Authority::ISO8583::FHMS::Paymentech' and client_payment_type_cd='C' and status_cd='A') and nvl(pos_pta_deactivation_ts, MAX_DATE)>sysdate);

select terminal_id into l_payment_subtype_key_id from pss.terminal where merchant_id = (select merchant_id from pss.merchant where merchant_name='Error Bin');
insert into pss.pos_pta (pos_pta_id, pos_pta_activation_ts, pos_id, payment_subtype_id,payment_subtype_key_id,pos_pta_pin_req_yn_flag,pos_pta_priority, currency_cd, pos_pta_passthru_allow_yn_flag, pos_pta_disable_debit_denial, no_convenience_fee)
select pss.seq_pos_pta_id.NEXTVAL, l_activation_ts,l_pos_id, payment_subtype_id, l_payment_subtype_key_id,'N',6,l_currency_cd,'N','N','N'
from pss.payment_subtype where payment_subtype_name='Error Bin' and client_payment_type_cd='C' and status_cd='A'
and not exists(select 1 from pss.pos_pta where pos_id=l_pos_id and payment_subtype_id in (select payment_subtype_id from pss.payment_subtype where payment_subtype_name='Error Bin' and client_payment_type_cd='C' and status_cd='A') and nvl(pos_pta_deactivation_ts, MAX_DATE)>sysdate);

l_priority:=1;
select internal_payment_type_id into l_payment_subtype_key_id from pss.internal_payment_type where internal_payment_type_desc='Special Card (prepaid)';
  
insert into pss.pos_pta (pos_pta_id, pos_pta_activation_ts, pos_id, payment_subtype_id,payment_subtype_key_id,pos_pta_pin_req_yn_flag,pos_pta_priority, currency_cd, pos_pta_passthru_allow_yn_flag, pos_pta_disable_debit_denial, no_convenience_fee)
select pss.seq_pos_pta_id.NEXTVAL, l_activation_ts, l_pos_id, payment_subtype_id, l_payment_subtype_key_id,'N',l_priority,l_currency_cd,'N','N','N'
from pss.payment_subtype where payment_subtype_name='USAT ISO Card - Prepaid Card (Manual Entry)' and client_payment_type_cd='T' and status_cd='A'
and not exists(select 1 from pss.pos_pta where pos_id=l_pos_id and payment_subtype_id in (select payment_subtype_id from pss.payment_subtype where payment_subtype_name='USAT ISO Card - Prepaid Card (Manual Entry)' and client_payment_type_cd='T' and status_cd='A') and nvl(pos_pta_deactivation_ts, MAX_DATE)>sysdate);

select distinct payment_subtype_key_id into l_payment_subtype_key_id from pss.pos_pta pta join pss.payment_subtype ps on pta.payment_subtype_id=ps.payment_subtype_id 
where pos_id=l_pos_id and nvl(pos_pta_deactivation_ts, MAX_DATE)>sysdate
and ps.client_payment_type_cd='Y' and ps.payment_subtype_class='Authority::ISO8583::FHMS::Paymentech';

insert into pss.pos_pta (pos_pta_id, pos_pta_activation_ts, pos_id, payment_subtype_id,payment_subtype_key_id,pos_pta_pin_req_yn_flag,pos_pta_priority, currency_cd, pos_pta_passthru_allow_yn_flag, pos_pta_disable_debit_denial, no_convenience_fee)
select pss.seq_pos_pta_id.NEXTVAL, l_activation_ts,l_pos_id, payment_subtype_id, l_payment_subtype_key_id,'N',l_priority+rownum,l_currency_cd,'N','N','N'
from pss.payment_subtype where payment_subtype_class='Authority::ISO8583::FHMS::Paymentech' and client_payment_type_cd='N' and status_cd='A'
and not exists(select 1 from pss.pos_pta where pos_id=l_pos_id and payment_subtype_id in (select payment_subtype_id from pss.payment_subtype where payment_subtype_class='Authority::ISO8583::FHMS::Paymentech' and client_payment_type_cd='N' and status_cd='A') and nvl(pos_pta_deactivation_ts, MAX_DATE)>sysdate);

select terminal_id into l_payment_subtype_key_id from pss.terminal where merchant_id = (select merchant_id from pss.merchant where merchant_name='Error Bin');
insert into pss.pos_pta (pos_pta_id, pos_pta_activation_ts, pos_id, payment_subtype_id,payment_subtype_key_id,pos_pta_pin_req_yn_flag,pos_pta_priority, currency_cd, pos_pta_passthru_allow_yn_flag, pos_pta_disable_debit_denial, no_convenience_fee)
select pss.seq_pos_pta_id.NEXTVAL, l_activation_ts,l_pos_id, payment_subtype_id, l_payment_subtype_key_id,'N',6,l_currency_cd,'N','N','N'
from pss.payment_subtype where payment_subtype_name='Error Bin' and client_payment_type_cd='N' and status_cd='A'
and not exists(select 1 from pss.pos_pta where pos_id=l_pos_id and payment_subtype_id in (select payment_subtype_id from pss.payment_subtype where payment_subtype_name='Error Bin' and client_payment_type_cd='N' and status_cd='A') and nvl(pos_pta_deactivation_ts, MAX_DATE)>sysdate);

commit;
END;
/

ALTER TABLE PSS.CONSUMER_SETTING_PARAM
ADD ALLOWED_LOYALTY_IND CHAR(1);

UPDATE PSS.CONSUMER_SETTING_PARAM SET ALLOWED_LOYALTY_IND='Y' WHERE CONSUMER_SETTING_PARAM_ID IN (1,5);
UPDATE PSS.CONSUMER_SETTING_PARAM SET ALLOWED_LOYALTY_IND='N' WHERE CONSUMER_SETTING_PARAM_ID NOT IN (1,5);
commit;


  




  
