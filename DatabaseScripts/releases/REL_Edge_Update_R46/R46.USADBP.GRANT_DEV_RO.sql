GRANT SELECT ON REPORT.FILE_CACHE TO USAT_DEV_READ_ONLY;
GRANT SELECT ON REPORT.DFR_CHARGEBACK TO USAT_DEV_READ_ONLY;
GRANT SELECT ON REPORT.DFR_DOWNGRADE TO USAT_DEV_READ_ONLY;
GRANT SELECT ON REPORT.DFR_REPORT TO USAT_DEV_READ_ONLY;
GRANT SELECT ON REPORT.DFR_REPORT_DEF TO USAT_DEV_READ_ONLY;
GRANT SELECT ON REPORT.DFR_REPORT_FIELD_DEF TO USAT_DEV_READ_ONLY;
GRANT SELECT ON REPORT.DFR_RETRIEVAL TO USAT_DEV_READ_ONLY;
