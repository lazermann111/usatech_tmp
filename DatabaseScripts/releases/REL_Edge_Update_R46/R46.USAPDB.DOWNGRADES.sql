declare 
  ln_report_def_id NUMBER;
begin
  select dfr_report_def.dfr_report_def_id 
  into ln_report_def_id
  from report.dfr_report_def
  where data_record_type_id = 'RANS0013';
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, null, 'N', 1, 2, 'java.lang.String', null, 'Entity Type', 'Level at which reported event occurred.');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'ENTITY_NUM', 'Y', 2, 10, 'java.lang.Integer', null, 'Entity #', 'Associated TD identification number, less any leading zeroes. Chase Paymentech assigned');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'PID', 'N', 3, 10, 'java.lang.Integer', null, 'PID #', 'Merchantís presenter id number. Chase Paymentech assigned');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'CURRENCY', 'N', 4, 3, 'java.lang.String', null, 'Currency', 'Three character abbreviation for currency being reported.');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'MOP_CD', 'N', 5, 2, 'java.lang.String', null, 'MOP Code', 'Code that identifies the (MOP) method of payment for the transaction.');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'QUALIFICATION_CD', 'N', 6, 4, 'java.lang.String', null, 'Interchange Qualification', 'A code that identifies the rate at which Chase Paymentech qualified the transaction');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'REASON_CD', 'N', 7, 4, 'java.lang.String', null, 'Reason Code', 'The value representing the reason the transaction qualified for a specific Interchange Qualification');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'REASON_DESC', 'N', 8, 70, 'java.lang.String', null, 'Reason Code Description', 'The description of the value representing the reason the transaction qualified for a specific Interchange Qualification');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'ACCOUNT_NUM', 'N', 9, 22, 'java.lang.String', null, 'Account #', 'Cardholders account number');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'MERCHANT_ORDER_NUM', 'Y', 10, 22, 'java.lang.String', null, 'Merchant Order #', 'The order number submitted by the merchant with the original sale');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'DEPOSIT_DATE', 'N', 11, 10, 'java.util.Date', 'MM/dd/yyyy', 'Deposit Date', 'Merchants day the deposit transaction was received');
  insert into report.dfr_report_field_def(dfr_report_def_id, column_name, is_key, field_number, field_length, data_type, data_format, name, description) values (ln_report_def_id, 'AMOUNT', 'N', 12, 10, 'java.lang.Double', null, 'Amount', 'The value of the items included in a Data Record.');
end;
/

update report.dfr_report_def
set table_name = 'REPORT.DFR_DOWNGRADE', enabled = 'Y'
where data_record_type_id = 'RANS0013';

CREATE TABLE REPORT.DFR_DOWNGRADE (
	DFR_DOWNGRADE_ID NUMBER NOT NULL ENABLE,
	DFR_REPORT_ID NUMBER  NOT NULL ENABLE,
	ENTITY_NUM NUMBER NOT NULL ENABLE,
	PID NUMBER,
	CURRENCY VARCHAR(3) NOT NULL ENABLE,
	MOP_CD VARCHAR(128),
	QUALIFICATION_CD VARCHAR(128),
	REASON_CD VARCHAR(128),
	REASON_DESC VARCHAR(1024),
	ACCOUNT_NUM VARCHAR(22),
	MERCHANT_ORDER_NUM VARCHAR(128),
	DEPOSIT_DATE DATE,
	AMOUNT NUMBER(15,2),
	CREATED_BY VARCHAR2(30) NOT NULL,
	CREATED_TS DATE NOT NULL,
	LAST_UPDATED_BY VARCHAR2(30) NOT NULL,
	LAST_UPDATED_TS DATE NOT NULL,
	CONSTRAINT PK_DFR_DOWNGRADE PRIMARY KEY(DFR_DOWNGRADE_ID) USING INDEX TABLESPACE REPORT_INDX);

CREATE SEQUENCE REPORT.SEQ_DFR_DOWNGRADE
	MINVALUE 1 
	MAXVALUE 999999999999999999999999999 
	INCREMENT BY 1 
	START WITH 100
	CACHE 20 
	NOORDER 
	NOCYCLE ;

CREATE OR REPLACE TRIGGER REPORT.TRBI_DFR_DOWNGRADE BEFORE INSERT ON REPORT.DFR_DOWNGRADE
	FOR EACH ROW
BEGIN
	IF :NEW.DFR_DOWNGRADE_ID IS NULL THEN
		SELECT SEQ_DFR_DOWNGRADE.NEXTVAL INTO :NEW.DFR_DOWNGRADE_ID FROM DUAL;
	END IF;
	SELECT 
		USER,
		SYSDATE,
		SYSDATE,
		USER
	INTO 
		:NEW.CREATED_BY,
		:NEW.CREATED_TS,
		:NEW.LAST_UPDATED_TS,
		:NEW.LAST_UPDATED_BY
	FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER REPORT.TRBU_DFR_DOWNGRADE BEFORE UPDATE ON REPORT.DFR_DOWNGRADE
	FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.CREATED_BY,
		:NEW.CREATED_TS,
		:NEW.LAST_UPDATED_TS,
		:NEW.LAST_UPDATED_BY
	FROM DUAL;
END;
/

CREATE INDEX REPORT.IX_DFR_DOWNGRADE_ORDER_NUM ON REPORT.DFR_DOWNGRADE(MERCHANT_ORDER_NUM) TABLESPACE REPORT_INDX ONLINE;

GRANT SELECT,INSERT,UPDATE ON REPORT.DFR_DOWNGRADE TO USALIVE_APP_ROLE;
GRANT SELECT,INSERT,UPDATE ON REPORT.DFR_DOWNGRADE TO USAT_DMS_ROLE;
GRANT SELECT,INSERT,UPDATE ON REPORT.DFR_DOWNGRADE TO USAT_POSM_ROLE;
