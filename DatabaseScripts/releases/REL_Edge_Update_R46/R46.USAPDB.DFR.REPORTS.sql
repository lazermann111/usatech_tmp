CREATE TABLE REPORT.DFR_ACTIVITY_CATEGORY (
	CODE VARCHAR2(32) NOT NULL,
	NAME VARCHAR2(1024) NOT NULL,
	DESCRIPTION VARCHAR(4000),
	CONSTRAINT PK_DFR_ACTIVITY_CATEGORY PRIMARY KEY(CODE) USING INDEX TABLESPACE REPORT_INDX);

GRANT SELECT ON REPORT.DFR_ACTIVITY_CATEGORY TO USALIVE_APP_ROLE;
GRANT SELECT ON REPORT.DFR_ACTIVITY_CATEGORY TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.DFR_ACTIVITY_CATEGORY TO USAT_POSM_ROLE;

INSERT INTO REPORT.DFR_ACTIVITY_CATEGORY(CODE, NAME, DESCRIPTION) VALUES ('ADJPAR', 'Adjustment for Partial Items', 'This line item is provided as an inventory balance control item for ''split chargebacks''. A split chargeback enters inventory as one item but is deducted from inventory two times: once as a Representment and once as a Return To Merchant item. This balancing entry adjusts the inventory counts only (not amounts) to remove the redundancy.');
INSERT INTO REPORT.DFR_ACTIVITY_CATEGORY(CODE, NAME, DESCRIPTION) VALUES ('ADJPDE', 'Chase Paymentech Adjustments', 'Any manual adjustments performed by Chase Paymentech to adjust inventories during the reporting period');
INSERT INTO REPORT.DFR_ACTIVITY_CATEGORY(CODE, NAME, DESCRIPTION) VALUES ('AUTH', 'Auth Only', 'Transactions submitted with an Action Code A. Auth Only excludes authorizations performed as a result of a B or D action code. Auth Only includes authorizations received via batch or online');
INSERT INTO REPORT.DFR_ACTIVITY_CATEGORY(CODE, NAME, DESCRIPTION) VALUES ('BEGINV', 'Beginning Inventory', 'Count and value of items in chargeback/return inventory at the beginning of the reporting period');
INSERT INTO REPORT.DFR_ACTIVITY_CATEGORY(CODE, NAME, DESCRIPTION) VALUES ('BUW', 'Back Up Withholding', 'This value represents the amount withheld for the timeframe reported. Data records with this category will only appear on the FIN0010 report if the BUW amount is greater than zero. Back Up Withholding reflects a percentage of gross sales withheld by the IRS if the Tax Identification Number (TIN) on file with Chase Paymentech is either blank or invalid according to the IRS.');
INSERT INTO REPORT.DFR_ACTIVITY_CATEGORY(CODE, NAME, DESCRIPTION) VALUES ('CB', 'Net Chargeback Activity', 'The sum of adjustments affecting the merchant''s current balance related to chargeback activity');
INSERT INTO REPORT.DFR_ACTIVITY_CATEGORY(CODE, NAME, DESCRIPTION) VALUES ('CNREF', 'Cancelled Refunds', 'Action-code based. This is any refund type action code txn that was included in a cancelled submission');
INSERT INTO REPORT.DFR_ACTIVITY_CATEGORY(CODE, NAME, DESCRIPTION) VALUES ('CNSALE', 'Cancelled Sales', 'Action-code based. This is any sale type action code txn that was included in a cancelled submission');
INSERT INTO REPORT.DFR_ACTIVITY_CATEGORY(CODE, NAME, DESCRIPTION) VALUES ('DBADJ', 'Net Debit Adjustment', 'Activity The sum of adjustments affecting the merchant''s running balance related to Debit Adjustment activity');
INSERT INTO REPORT.DFR_ACTIVITY_CATEGORY(CODE, NAME, DESCRIPTION) VALUES ('DBREF', 'Auth Only - Debit Refund', 'Debit Refund authorization action codes');
INSERT INTO REPORT.DFR_ACTIVITY_CATEGORY(CODE, NAME, DESCRIPTION) VALUES ('DBSALE', ' Auth Only - Debit Sale', 'Debit Sale authorization action codes');
INSERT INTO REPORT.DFR_ACTIVITY_CATEGORY(CODE, NAME, DESCRIPTION) VALUES ('DEC', 'Declined Deposits', 'Deposit-type action code transactions that were not depositable due to a hard decline');
INSERT INTO REPORT.DFR_ACTIVITY_CATEGORY(CODE, NAME, DESCRIPTION) VALUES ('EFEFT', 'Effective Funds Transfer', 'Funds transfer that is anticipated to credited to the merchant''s bank account by the merchant''s financial institution. These are funds transfers that have reached their effective date');
INSERT INTO REPORT.DFR_ACTIVITY_CATEGORY(CODE, NAME, DESCRIPTION) VALUES ('ENDINV', 'Ending Inventory', 'Items included in chargeback/return inventory at the end of the reporting period');
INSERT INTO REPORT.DFR_ACTIVITY_CATEGORY(CODE, NAME, DESCRIPTION) VALUES ('ERET', 'Net ECP Return Activity', 'The sum of adjustments affecting the merchant''s running balance related to ECP Return activity');
INSERT INTO REPORT.DFR_ACTIVITY_CATEGORY(CODE, NAME, DESCRIPTION) VALUES ('FEEREC', 'Fee Received', 'The amount of the fee assessed by the issuing bank for certain MasterCard chargeback reason codes. The monetary impact of the fee itself is included in the FIN0011 report under the category PDE. This Category is used on the PDE0017 report to provide detail about the chargeback transaction that resulted in this fee');
INSERT INTO REPORT.DFR_ACTIVITY_CATEGORY(CODE, NAME, DESCRIPTION) VALUES ('FEEREP', 'Fee Represent', 'The amount being returned to the merchant as the result of representment of the fee assessed by the issuing bank for certain MasterCard chargeback reason codes. The monetary impact of representment of the fee itself is included in the FIN0011 report under the category PDE. This Category is used on the PDE0017 report to provide detail about the chargeback transaction that resulted in this fee');
INSERT INTO REPORT.DFR_ACTIVITY_CATEGORY(CODE, NAME, DESCRIPTION) VALUES ('HOLDFT', 'On Hold Funds Transfer', 'Funds transfer that was created but put on hold and not yet transferred to the merchant''s financial institution');
INSERT INTO REPORT.DFR_ACTIVITY_CATEGORY(CODE, NAME, DESCRIPTION) VALUES ('IA', 'Interchange and Assessment Fees', 'The sum of fees for all service codes that represent pass through costs from certain endpoints (interchange and assessments). Note: ChaseNet Fees will be reported with this category code.');
INSERT INTO REPORT.DFR_ACTIVITY_CATEGORY(CODE, NAME, DESCRIPTION) VALUES ('NET', 'Net Settled Deposit Amount', 'Settled Sales deposited less Settled Refunds deposited');
INSERT INTO REPORT.DFR_ACTIVITY_CATEGORY(CODE, NAME, DESCRIPTION) VALUES ('NON', 'Non-Financial', 'Non-financial transactions include FPO Modification requests, Stored Value or any other transactions that have no financial impact');
INSERT INTO REPORT.DFR_ACTIVITY_CATEGORY(CODE, NAME, DESCRIPTION) VALUES ('OTH', 'Other Adjustments', 'The sum of adjustments affecting the merchant''s running balance related to anything other than fees, chargebacks, ECP Returns, or Reserves');
INSERT INTO REPORT.DFR_ACTIVITY_CATEGORY(CODE, NAME, DESCRIPTION) VALUES ('PARREP', 'Partial Representment', 'Indicates the represented portion of a ''split chargeback'', where a part of the value of the original CB was able to represented and the balance is Returned To Merchant');
INSERT INTO REPORT.DFR_ACTIVITY_CATEGORY(CODE, NAME, DESCRIPTION) VALUES ('PARRTM', 'Partial Return To Merchant', 'Indicates the returned-to-merchant portion of a ''split chargeback'', where a part of the value of the original CB was able to represented and the balance is Returned To Merchant');
INSERT INTO REPORT.DFR_ACTIVITY_CATEGORY(CODE, NAME, DESCRIPTION) VALUES ('PENDFT', 'Pending Funds Transfer', 'Funds transfer that was created to be transferred to the merchant''s financial institution but has not yet reached its effective date (the date on which it is anticipated that the financial institution will credit the merchant''s bank account');
INSERT INTO REPORT.DFR_ACTIVITY_CATEGORY(CODE, NAME, DESCRIPTION) VALUES ('PFEE', 'Chase Paymentech Fees', 'The sum of fees assessed by Chase Paymentech for all service codes that include Chase Paymentech costs');
INSERT INTO REPORT.DFR_ACTIVITY_CATEGORY(CODE, NAME, DESCRIPTION) VALUES ('PRE', 'ECP Ver/Val/Prenote', 'Transactions that were verified, validated or prenoted as part of Electronic Check processing'); 
INSERT INTO REPORT.DFR_ACTIVITY_CATEGORY(CODE, NAME, DESCRIPTION) VALUES ('PREDB', 'Pre-Auth - Debit', 'Debit pre-authorization action codes');
INSERT INTO REPORT.DFR_ACTIVITY_CATEGORY(CODE, NAME, DESCRIPTION) VALUES ('RECD', 'New Chargebacks/ECP Returns Received', 'New chargeback/return items received during the reporting period');
INSERT INTO REPORT.DFR_ACTIVITY_CATEGORY(CODE, NAME, DESCRIPTION) VALUES ('RECRS', 'Accepted items received for recourse', 'Items received from the merchant and accepted for recourse during the reporting period');
INSERT INTO REPORT.DFR_ACTIVITY_CATEGORY(CODE, NAME, DESCRIPTION) VALUES ('REF', 'Refunds', 'All refund type items successfully submitted for deposit. Rejects and Declined Deposits are not included'); 
INSERT INTO REPORT.DFR_ACTIVITY_CATEGORY(CODE, NAME, DESCRIPTION) VALUES ('REJ', 'Rejected Items', 'Transactions of any action code that did not pass Chase Paymentech front-end edit checks');
INSERT INTO REPORT.DFR_ACTIVITY_CATEGORY(CODE, NAME, DESCRIPTION) VALUES ('REJFT', 'Rejected Funds Transfer', 'Funds transfer that was created and sent to and subsequently rejected by the merchant''s financial institution'); 
INSERT INTO REPORT.DFR_ACTIVITY_CATEGORY(CODE, NAME, DESCRIPTION) VALUES ('REPR', 'Items represented', 'Items submitted for representment during the reporting period');
INSERT INTO REPORT.DFR_ACTIVITY_CATEGORY(CODE, NAME, DESCRIPTION) VALUES ('RES', 'Reserve Adjustments', 'The sum of adjustments that affected the merchant''s current balance for reserve entries during the reporting period');
INSERT INTO REPORT.DFR_ACTIVITY_CATEGORY(CODE, NAME, DESCRIPTION) VALUES ('RETREQ', 'Retrieval Request', 'Received New retrieval request items received during the reporting period');
INSERT INTO REPORT.DFR_ACTIVITY_CATEGORY(CODE, NAME, DESCRIPTION) VALUES ('RTM', 'Items Returned to Merchant', 'Items returned to the merchant for handling during the reporting period');
INSERT INTO REPORT.DFR_ACTIVITY_CATEGORY(CODE, NAME, DESCRIPTION) VALUES ('SALE', 'Sales', 'All sale type items successfully submitted for deposit. Rejects and Declined Deposits are not included');

ALTER SESSION SET CURRENT_SCHEMA = REPORT;

DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := -1;
BEGIN
	SELECT MAX(REPORT_ID) INTO LN_REPORT_ID
	FROM REPORT.REPORTS WHERE REPORT_NAME = 'Paymentech Chargebacks';
	
	IF LN_REPORT_ID > 0 THEN
		RETURN;
	END IF;

	SELECT REPORT.REPORTS_SEQ.NEXTVAL INTO LN_REPORT_ID FROM DUAL;
	
	INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
	VALUES(LN_REPORT_ID, 'Paymentech Chargebacks - {b} to {e}', 1, 0, 'Paymentech Chargebacks', 'Chargebacks received from Chase Paymentech.', 'N', 0);
	
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
	VALUES(LN_REPORT_ID, 'query', 'select cb.dfr_chargeback_id, cb.sequence_num, cb.chargeback_amount, cb.fee_amount, cb.chargeback_currency, cb.reason_cd, cb.account_num, cb.merchant_order_num, ac.name activity_name, to_char(cb.auth_date, ''MM/DD/YYYY'') auth_date, to_char(cb.transaction_date, ''MM/DD/YYYY'') transaction_date, to_char(cb.initiated_date, ''MM/DD/YYYY'') initiated_date, to_char(cb.activity_date, ''MM/DD/YYYY'') activity_date, to_char(cb.due_date, ''MM/DD/YYYY'') due_date
		from report.dfr_chargeback cb
		join report.dfr_activity_category ac on ac.code = cb.activity_cd
		where cb.activity_date >= cast(? as date)
		and cb.activity_date < cast(? as date)
		order by cb.activity_date desc');

	insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'header','true');
	insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'dateFormat','MM/dd/yyyy');
	insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'params.StartDate','{b}');
	insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'params.EndDate','{e}');
	insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'paramNames','StartDate,EndDate');
	insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'paramTypes','TIMESTAMP,TIMESTAMP');

	insert into web_content.web_link values(web_content.seq_web_link_id.nextval, 'Paymentech Chargebacks', './select_date_range_frame_nonfolio.i?basicReportId='||LN_REPORT_ID,'Chargebacks received from Chase Paymentech.','Accounting','W', 0);
	insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 1);
	insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 14);
	
	commit;
END;
/

DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := -1;
BEGIN
	SELECT MAX(REPORT_ID) INTO LN_REPORT_ID
	FROM REPORT.REPORTS WHERE REPORT_NAME = 'Paymentech Retrievals';
	
	IF LN_REPORT_ID > 0 THEN
		RETURN;
	END IF;

	SELECT REPORT.REPORTS_SEQ.NEXTVAL INTO LN_REPORT_ID FROM DUAL;
	
	INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
	VALUES(LN_REPORT_ID, 'Paymentech Retrievals - {b} to {e}', 1, 0, 'Paymentech Retrievals', 'Retrieval requests received from Chase Paymentech.', 'N', 0);
	
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
	VALUES(LN_REPORT_ID, 'query', 'select r.dfr_retrieval_id, r.sequence_num, r.amount, r.currency, r.reason_cd, r.account_num, r.merchant_order_num, ac.name activity_name, r.auth_cd, r.terminal_num, r.batch_num, r.fulfillment_type, r.arn,
		to_char(r.auth_date, ''MM/DD/YYYY'') auth_date, to_char(r.transaction_date, ''MM/DD/YYYY'') transaction_date, to_char(r.received_date, ''MM/DD/YYYY'') received_date, to_char(r.due_date, ''MM/DD/YYYY'') due_date
		from report.dfr_retrieval r
		join report.dfr_activity_category ac on ac.code = r.activity_cd
		where r.received_date >= cast(? as date)
		and r.received_date < cast(? as date)
		order by r.received_date desc');

	insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'header','true');
	insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'dateFormat','MM/dd/yyyy');
	insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'params.StartDate','{b}');
	insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'params.EndDate','{e}');
	insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'paramNames','StartDate,EndDate');
	insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'paramTypes','TIMESTAMP,TIMESTAMP');

	insert into web_content.web_link values(web_content.seq_web_link_id.nextval, 'Paymentech Retrievals', './select_date_range_frame_nonfolio.i?basicReportId='||LN_REPORT_ID,'Retrieval requests received from Chase Paymentech.','Accounting','W', 0);
	insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 1);
	insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 14);
	
	commit;
END;
/

DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := -1;
BEGIN
	SELECT MAX(REPORT_ID) INTO LN_REPORT_ID
	FROM REPORT.REPORTS WHERE REPORT_NAME = 'Paymentech Downgrades';
	
	IF LN_REPORT_ID > 0 THEN
		RETURN;
	END IF;

	SELECT REPORT.REPORTS_SEQ.NEXTVAL INTO LN_REPORT_ID FROM DUAL;
	
	INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
	VALUES(LN_REPORT_ID, 'Paymentech Downgrades - {b} to {e}', 1, 0, 'Paymentech Downgrades', 'Transaction Interchange Downgrades from Chase Paymentech.', 'N', 0);
	
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
	VALUES(LN_REPORT_ID, 'query', 'select d.dfr_downgrade_id, d.amount, d.currency, d.account_num, d.merchant_order_num, d.qualification_cd, d.reason_cd, d.reason_desc, to_char(d.deposit_date, ''MM/DD/YYYY'')
		from report.dfr_downgrade d
		where d.deposit_date >= cast(? as date)
		and r.deposit_date < cast(? as date)
		order by d.deposit_date desc');

	insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'header','true');
	insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'dateFormat','MM/dd/yyyy');
	insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'params.StartDate','{b}');
	insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'params.EndDate','{e}');
	insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'paramNames','StartDate,EndDate');
	insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'paramTypes','TIMESTAMP,TIMESTAMP');

	insert into web_content.web_link values(web_content.seq_web_link_id.nextval, 'Paymentech Downgrades', './select_date_range_frame_nonfolio.i?basicReportId='||LN_REPORT_ID,'Transaction Interchange Downgrades from Chase Paymentech.','Accounting','W', 0);
	insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 1);
	insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 14);
	
	commit;
END;
/
