CREATE OR REPLACE PACKAGE PSS.PKG_RANDOM IS

  -- Returns random integer between [0, r-1]
  FUNCTION rndint(r IN NUMBER) RETURN NUMBER;

  -- Returns random real between [0, 1]
  FUNCTION rndflt RETURN NUMBER;

END;
/

CREATE OR REPLACE PACKAGE BODY PSS.PKG_RANDOM IS
-- Purpose: Linear congruential algorithm random num generation.
--
-- MODIFICATION HISTORY
-- Person       Date        Comments
-- ---------    ------      ---------------------------------------
-- erybski      06.07.21    Initial release.

  m         CONSTANT NUMBER:=100000000;  /* initial conditions */
  m1        CONSTANT NUMBER:=10000;      /* (for best results) */
  b         CONSTANT NUMBER:=31415821;   /*      */
  a         NUMBER;                      /* seed */
  the_date  DATE;                        /*      */
  days      NUMBER;                      /* for generating initial seed */
  secs      NUMBER;                      /*      */

  -- ------------------------
  -- Private utility FUNCTION
  -- ------------------------
  FUNCTION mult(p IN NUMBER, q IN NUMBER) RETURN NUMBER IS
    p1     NUMBER; 
    p0     NUMBER; 
    q1     NUMBER; 
    q0     NUMBER; 
  BEGIN 
    p1:=TRUNC(p/m1); 
    p0:=MOD(p,m1); 
    q1:=TRUNC(q/m1); 
    q0:=MOD(q,m1); 
    RETURN(MOD((MOD(p0*q1+p1*q0,m1)*m1+p0*q0),m)); 
  END;

  -- ---------------------------------------
  -- Returns random integer between [0, r-1]
  -- ---------------------------------------
  FUNCTION rndint (r IN NUMBER) RETURN NUMBER IS 
  BEGIN 
    -- Generate a random NUMBER, and set it to be the new seed
    a:=MOD(mult(a,b)+1,m); 

    -- Convert it to integer between [0, r-1] and return it
    RETURN(TRUNC((TRUNC(a/m1)*r)/m1));
  END;
 
  -- ----------------------------------
  -- Returns random real between [0, 1]
  -- ----------------------------------
  FUNCTION rndflt RETURN NUMBER IS
    BEGIN
      -- Generate a random NUMBER, and set it to be the new seed
      a:=MOD(mult(a,b)+1,m);
      RETURN(a/m);
    END;

BEGIN
  -- Generate initial seed "a" based on system date
  the_date:=SYSDATE;
  days:=TO_NUMBER(TO_CHAR(the_date, 'J'));
  secs:=TO_NUMBER(TO_CHAR(the_date, 'SSSSS'));
  a:=days*24*3600+secs;
END;
/

CREATE OR REPLACE PACKAGE REPORT.PKG_REPORT_SP IS
--
-- To modify this template, edit file PKGSPEC.TXT in TEMPLATE
-- directory of SQL Navigator
--
-- Purpose: Briefly explain the functionality of the package
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------
   -- Enter package declarations as shown below
   TYPE curs_ref IS REF CURSOR;

   /*RETURN (g4op.dex_file.dex_file_id%type,
       g4op.dex_file.DEX_FILE_NAME%type,
       g4op.dex_file.DEX_FILE_PATH%type,
       char(1),
       corp.customer.customer_id%type,
       report.terminal.cust_terminal_nbr%type,
       report.region.region_name%type,
       g4op.dex_file.dex_date%type,
       g4op.dex_file.dex_type%type);*/
   PROCEDURE get_unsent_dexfiles_bycust (
      p_cust_id   IN       VARCHAR,
      p_cursor    IN OUT   curs_ref
   );


END;
/

CREATE OR REPLACE PACKAGE BODY REPORT.PKG_REPORT_SP
IS
--
-- To modify this template, edit file PKGBODY.TXT in TEMPLATE
-- directory of SQL Navigator
--
-- Purpose: Briefly explain the functionality of the package body
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------
   -- Enter procedure, function bodies as shown below
   PROCEDURE get_unsent_dexfiles_bycust (
      p_cust_id   IN       varchar,
      p_cursor    IN OUT   curs_ref
   )
   AS
   BEGIN
      OPEN p_cursor
       FOR
          SELECT   a.dex_file_id, a.file_name, a.file_path, a.dex_date,
                   a.dex_type, a.create_dt, a.sent, a.eport_id, a.processed,
                   a.upd_dt
              FROM g4op.dex_file a, report.terminal t, report.vw_terminal_eport te
             WHERE t.customer_id = p_cust_id
               AND t.terminal_id = te.terminal_id
               AND te.eport_id = a.eport_id
               AND a.sent = 'N'
               AND a.dex_file_id IN (
                      SELECT dex_file_id
                        FROM g4op.dex_file
                       WHERE eport_id = a.eport_id
                         AND dex_date = (SELECT MAX (dex_date)
                                           FROM g4op.dex_file
                                          WHERE eport_id = a.eport_id))
          ORDER BY a.eport_id, a.dex_date DESC;
   END;
END;
/