-- add the new setting to enable or disable the funtionality
INSERT INTO ENGINE.APP_SETTING (APP_SETTING_CD,APP_SETTING_VALUE,APP_SETTING_DESC)
SELECT
'TURNOFF_EPORT_QS_FEES_AFTER_INACTIVITY_DAYS',
'Y',
'This flag is used to to automatically turn off customer’s monthly ePort Connect Fees when an ePort that is either owned or under a Quick Start lease, does not call in for +XX+ number of days'
FROM DUAL
WHERE NOT EXISTS(SELECT 1 FROM ENGINE.APP_SETTING WHERE APP_SETTING_CD = 'TURNOFF_EPORT_QS_FEES_AFTER_INACTIVITY_DAYS');

COMMIT;

--ADD new column to hold number of inactive days 
--after which the fees are shut off for ePort on
--the quick start program   
ALTER table REPORT.TERMINAL
ADD(EPORT_QS_FEE_INACTIVITY_DAYS decimal(6,0) NULL);

--ADD new column to hold indicator that this fee can be shutoff
--based on the inactive days of the device
ALTER TABLE CORP.FEES ADD (INACTIVITY_HANDLING_IND VARCHAR2(1));

--Populate new column 
--at this point on the Quick Start Service Fee and Terminal Service Fee
--can be shut off automatically
UPDATE CORP.FEES SET INACTIVITY_HANDLING_IND = CASE WHEN FEE_ID IN (1, 15) THEN 'Y' ELSE 'N' END; 

COMMIT; 

--Set the column to be nonnullable 
ALTER TABLE CORP.FEES MODIFY (INACTIVITY_HANDLING_IND VARCHAR2(1) NOT NULL);

--ADD new column to service fees to store date fee was auto shut off
ALTER TABLE CORP.SERVICE_FEES ADD (AUTO_TURNED_OFF_DATE DATE NULL);

--ADD new column to service fees to store date fee was auto turned back on
ALTER TABLE CORP.SERVICE_FEES ADD (AUTO_TURNED_ON_DATE DATE NULL);
