DECLARE
	ln_property_list_version NUMBER := 22;
	ln_device_type_id device_type.device_type_id%TYPE := 13;
	ln_config_template_type_id config_template_type.config_template_type_id%TYPE := 1;
	lv_config_template_name config_template.config_template_name%TYPE := 'DEFAULT-CFG-' || ln_device_type_id || '-' || ln_property_list_version;
	ln_config_template_id config_template.config_template_id%TYPE;

BEGIN	
	-- get the config template id of new template
	
	SELECT MAX(CONFIG_TEMPLATE_ID) INTO ln_config_template_id
	FROM DEVICE.CONFIG_TEMPLATE
	WHERE CONFIG_TEMPLATE_NAME = lv_config_template_name 
	AND DEVICE_TYPE_ID = ln_device_type_id;
		
	-- delete settings from meta template 
		
	DELETE FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS
	WHERE
		CTS.DEVICE_SETTING_PARAMETER_CD IN ('2003', '2004', '2005', '2006', '2007', '2008','2009', '2010', '2011', '2012', '2013', '2014')
		AND CTS.CONFIG_TEMPLATE_ID IN (
		SELECT CONFIG_TEMPLATE_ID 
		FROM DEVICE.CONFIG_TEMPLATE CT 
		WHERE CONFIG_TEMPLATE_TYPE_ID = 3 
		AND DEVICE_TYPE_ID = ln_device_type_id);
			
	-- delete settings from new template 

	DELETE FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS WHERE CTS.CONFIG_TEMPLATE_ID = ln_config_template_id; 		
	
	-- delete new template  

	DELETE FROM DEVICE.CONFIG_TEMPLATE CT WHERE CT.CONFIG_TEMPLATE_ID = ln_config_template_id;	
	
	-- delete device type property list version
	
	DELETE FROM DEVICE.DEVICE_TYPE_PROPERTY_LIST
	WHERE DEVICE_TYPE_ID = ln_device_type_id
	AND PROPERTY_LIST_VERSION = ln_property_list_version;	
	
	-- delete parameters 
		
	DELETE FROM DEVICE.DEVICE_SETTING_PARAMETER WHERE DEVICE_SETTING_PARAMETER_CD IN ('2003', '2004', '2005', '2006', '2007', '2008','2009', '2010', '2011', '2012', '2013', '2014');
	
	-- delete new regex for AVAS Merchant Id
	
	DELETE FROM DEVICE.CONFIG_TEMPLATE_REGEX WHERE REGEX_NAME = 'HEX_ASCII_64';
	
	-- update back to descriptions used prior to R55
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET DESCRIPTION = 'Sets VMC or Kiosk Interface type. Most devices support interfaces 1-6. G9 Rev >=2.04.005 supports interfaces 7 and 8. If set to either 7 or 8, the G9 will disable MDB Alerts.'	
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '1500';
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET DESCRIPTION = 'Represents the number of reattempts on when the G9 sends a Poll Response Message, but it did not receive an ACK from the VMC. 5 is default. Set to 0 to disable message retries.'
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '1004';
		
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET DESCRIPTION = 'This number represents the delay between transmitted bytes. It can either be:<br/> Edges and G9s before 2.02.009e: 1 = no delay, 2= 1 bit delay.<br/> G9s >= 2.02.009e: 1= 1 bit delay, 2= 4 bit delay'
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '1009';
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET DESCRIPTION = 'If 0, disabled {default}, at the beginning of each cashless session, the G9 will send the VMC the balance of funds available for the consumer to spend. If 1, enabled, the VMC will be sent FFFF instead which a special code to instruct the VMC to hide the balance from the consumer.'
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '1016';

	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET DESCRIPTION = 'Sets how the Aux Serial Port is used. Most G9 devices support 0-2. G9 rev >=2.04.005 supports 4.'
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '1107';
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET DESCRIPTION = 'The G9 simulates a hand held device. This value represents the number of simulated plug-in attempts of a handheld. This value should only be changed with engineering guidance. Min value is 1. Default 2.'
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '1113';
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING 
	SET DESCRIPTION = 'The G9 attempts communication with a VMC by sending the ENQ control character. Some vendors, like ones equipped with Bill recyclers, require more ENQs to be sent to wake it up. This value should only be changed with engineering guidance. Min value is 5. Default is 10.'
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '1114';
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET DESCRIPTION = 'This value is the number of seconds that must elapse after the Host stops communicating with the G9 before all work in process is marked Failed and the unit will be disabled. Note: Device will change the improper setting of 0 to the value of 10 seconds. {default is 7200}'
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '1601';
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET DESCRIPTION = 'Authorization response timeout in seconds. Most units (revisions) will not support over 20. G9 rev >=2.04.005 will support a range of 5 of 120. Please consult engineering on the use of this property.' 
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '10';
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET DESCRIPTION = 'If the G9 has not communicated in N seconds the modem''s PDP context will be forced closed. PDP context will be reestablish in line with the next TCP session. {7200 default, 0 is disabled}' 
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '35';			
		
	COMMIT;
END;
/		