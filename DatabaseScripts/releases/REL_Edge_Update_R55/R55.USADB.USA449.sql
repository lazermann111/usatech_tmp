
--ADD new column to hold indicator for new parameter
--that only lets a MORE card go into a negative balance if 
--the 'offline' attribute is in the auth call
ALTER table CORP.CUSTOMER
ADD(ALLOW_NEG_BAL_ONLY_OFFLINE_IND char(1) DEFAULT 'N' NULL);

--SET New indicator to N by default for all existing customers
UPDATE CORP.CUSTOMER SET ALLOW_NEG_BAL_ONLY_OFFLINE_IND = 'N';

--Grant SELECT to inside auth role 
GRANT SELECT ON CORP.CUSTOMER TO USAT_IN_AUTH_LAYER_ROLE;

COMMIT; 