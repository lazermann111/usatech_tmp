DECLARE
	ln_property_list_version NUMBER := 0;
	ln_device_type_id device_type.device_type_id%TYPE := 11;
	ln_config_template_type_id config_template_type.config_template_type_id%TYPE := 1;
	lv_config_template_name config_template.config_template_name%TYPE := 'EPORT-INTERACTIVE-DEFAULT-CFG-' || ln_device_type_id || '-' || ln_property_list_version;
	ln_config_template_id config_template.config_template_id%TYPE;

BEGIN	
	-- get the config template id of new template
	
	SELECT MAX(CONFIG_TEMPLATE_ID) INTO ln_config_template_id
	FROM DEVICE.CONFIG_TEMPLATE
	WHERE CONFIG_TEMPLATE_NAME = lv_config_template_name 
	AND DEVICE_TYPE_ID = ln_device_type_id;
		
	-- delete settings from meta template 
		
	DELETE FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS
	WHERE
		CTS.DEVICE_SETTING_PARAMETER_CD IN ('1022', '1024', '1200', '1202', '1500', '1501','1502', '1503', '1504', '1505', '1506', '1507', 
		'1508', '1509','1510', '1511', '1512', '1513', '1514', '1515', '1516', '1517','1518', '1519', '1520', '1521', '1522', '1523', '1524', '1525', '1526')
		AND CTS.CONFIG_TEMPLATE_ID IN (
		SELECT CONFIG_TEMPLATE_ID 
		FROM DEVICE.CONFIG_TEMPLATE CT 
		WHERE CONFIG_TEMPLATE_TYPE_ID = 3 
		AND DEVICE_TYPE_ID = ln_device_type_id);	
	
	-- delete settings from new template 

	DELETE FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS WHERE CTS.CONFIG_TEMPLATE_ID = ln_config_template_id; 		
	
	-- delete new template  

	DELETE FROM DEVICE.CONFIG_TEMPLATE CT WHERE CT.CONFIG_TEMPLATE_ID = ln_config_template_id;	
	
	-- delete device type property list version
	
	DELETE FROM DEVICE.DEVICE_TYPE_PROPERTY_LIST
	WHERE DEVICE_TYPE_ID = ln_device_type_id
	AND PROPERTY_LIST_VERSION = ln_property_list_version;	
		
		
	COMMIT;
END;
/	