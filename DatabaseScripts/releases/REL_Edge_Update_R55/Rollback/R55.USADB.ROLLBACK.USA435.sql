-- remove the setting 
DELETE FROM ENGINE.APP_SETTING WHERE APP_SETTING_CD = 'TURNOFF_EPORT_QS_FEES_AFTER_INACTIVITY_DAYS';

COMMIT;

-- drop the new job
BEGIN
DBMS_SCHEDULER.DROP_JOB (
   'TURNOFF_INACTIVE_EPORT_QS_FEES',
   force=>true);
END;
/

-- drop new procedure
DROP PROCEDURE CORP.TURNOFF_INACTIVE_EPORT_QS_FEES; 

-- remove column from terminal to store days of inactivity
ALTER TABLE REPORT.TERMINAL
  DROP (EPORT_QS_FEE_INACTIVITY_DAYS);

-- remove column from fees to indicate if it can be shut off due to inactivity
ALTER TABLE CORP.FEES
  DROP (INACTIVITY_HANDLING_IND);
  
-- remove column from service fees to store date auto shut off  
ALTER TABLE CORP.SERVICE_FEES 
  DROP (AUTO_TURNED_OFF_DATE);  
  
COMMIT;
/
