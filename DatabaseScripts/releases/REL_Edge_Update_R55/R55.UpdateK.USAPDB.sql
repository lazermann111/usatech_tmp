DROP INDEX PSS.AK_CONSUMER_EMAIL_ADDR1;
CREATE UNIQUE INDEX PSS.AK_CONSUMER_EMAIL_ADDR1 ON PSS.CONSUMER (UPPER(CONSUMER_EMAIL_ADDR1), DECODE(CONSUMER_TYPE_ID, 7, 5, 9, 5, 10, 5, CONSUMER_TYPE_ID), CASE WHEN CONSUMER_EMAIL_ADDR1 IS NULL THEN CONSUMER_ID END)
TABLESPACE PSS_INDX
ONLINE;
 
DROP INDEX PSS.AK_CONSUMER_CELL_PHONE_NUM; 
CREATE UNIQUE INDEX PSS.AK_CONSUMER_CELL_PHONE_NUM ON PSS.CONSUMER (UPPER(CONSUMER_CELL_PHONE_NUM), DECODE(CONSUMER_TYPE_ID, 7, 5, 9, 5, 10, 5, CONSUMER_TYPE_ID), CASE WHEN CONSUMER_CELL_PHONE_NUM IS NULL THEN CONSUMER_ID END)
TABLESPACE PSS_INDX
ONLINE;

ALTER TABLE PSS.PAYMENT_SUBTYPE ADD CAMPAIGN_ENABLED_IND VARCHAR(1) DEFAULT 'N' NOT NULL;
UPDATE PSS.PAYMENT_SUBTYPE SET CAMPAIGN_ENABLED_IND = 'Y' WHERE PAYMENT_SUBTYPE_CLASS = 'Internal::Prepaid' OR PAYMENT_SUBTYPE_NAME = 'Payroll Deduct (Track 2)';
COMMIT;

UPDATE DEVICE.CONFIG_TEMPLATE
SET CONFIG_TEMPLATE_NAME = 'Custom DEFAULT-CFG-13-22'
WHERE CONFIG_TEMPLATE_NAME = 'DEFAULT-CFG-13-22' AND CONFIG_TEMPLATE_TYPE_ID = 2;

CREATE TABLE CUSTOM_CONFIG_TMPL_BACKUP_R55 AS
select * from device.config_template_setting
where config_template_id in (
	select config_template_id from device.config_template where device_type_id = 13 and config_template_type_id = 2
) and device_setting_parameter_cd in (
	select device_setting_parameter_cd from device.config_template_setting where device_type_id = 13 and editor like 'TEXT:%' and editor not like 'TEXT:0%'
) and config_template_setting_value is null;

-- delete invalid required records with null values from custom G9 config templates
delete from device.config_template_setting
where config_template_id in (
	select config_template_id from device.config_template where device_type_id = 13 and config_template_type_id = 2
) and device_setting_parameter_cd in (
	select device_setting_parameter_cd from device.config_template_setting where device_type_id = 13 and editor like 'TEXT:%' and editor not like 'TEXT:0%'
) and config_template_setting_value is null;

COMMIT;
