DECLARE
	ln_property_list_version NUMBER := 22;
	ln_device_type_id device_type.device_type_id%TYPE := 13;
	ln_config_template_type_id config_template_type.config_template_type_id%TYPE := 1;
	lv_config_template_name config_template.config_template_name%TYPE := 'DEFAULT-CFG-' || ln_device_type_id || '-' || ln_property_list_version;
	ln_config_template_id config_template.config_template_id%TYPE;
	ln_config_regex_id NUMBER := 1;
	
	PROCEDURE ADD_DSP(
		pv_index VARCHAR2,
		pv_configurable VARCHAR2,
		pv_name VARCHAR2) 
	IS
	BEGIN
		INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_UI_CONFIGURABLE, DEVICE_SETTING_PARAMETER_NAME) 
		SELECT pv_index, pv_configurable, pv_name
		FROM DUAL 
		WHERE NOT EXISTS (
			SELECT 1 
			FROM DEVICE.DEVICE_SETTING_PARAMETER 
			WHERE DEVICE_SETTING_PARAMETER_CD = pv_index);
	END;

	PROCEDURE ADD_CTS_META(
		pv_PARAMETER_CD VARCHAR2,
		pv_SETTING_VALUE VARCHAR2,
		pn_CATEGORY_ID NUMBER,
		pv_DISPLAY VARCHAR2,
		pv_DESCRIPTION VARCHAR2,
		pv_EDITOR VARCHAR2,
		pn_FIELD_SIZE NUMBER,
		pn_DISPLAY_ORDER NUMBER,
		pv_DATA_MODE VARCHAR2,
		pv_DATA_MODE_AUX VARCHAR2,
		pn_REGEX_ID NUMBER,
		pv_NAME VARCHAR2,
		pv_CUSTOMER_EDITABLE VARCHAR2) 
	IS 
	BEGIN
		INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(
			CONFIG_TEMPLATE_ID,
			DEVICE_SETTING_PARAMETER_CD,
			CONFIG_TEMPLATE_SETTING_VALUE,
			CATEGORY_ID,
			DISPLAY,
			DESCRIPTION,
			EDITOR,
			CONVERTER,
			CLIENT_SEND,
			ALIGN,
			FIELD_SIZE,
			FIELD_OFFSET,
			PAD_CHAR,
			PROPERTY_LIST_VERSION,
			DEVICE_TYPE_ID,
			DISPLAY_ORDER,
			ALT_NAME,
			DATA_MODE,
			DATA_MODE_AUX,
			REGEX_ID,
			ACTIVE,
			NAME,
			CUSTOMER_EDITABLE,
			STORED_IN_PENNIES) 
		SELECT 
			CONFIG_TEMPLATE_ID, 
			pv_PARAMETER_CD,
			pv_SETTING_VALUE,
			pn_CATEGORY_ID,
			pv_DISPLAY,
			pv_DESCRIPTION,
			pv_EDITOR,
			'',
			'Y',
			NULL,
			pn_FIELD_SIZE,
			NULL,
			NULL,
			0,
			DEVICE_TYPE_ID,
			pn_DISPLAY_ORDER,
			NULL,
			pv_DATA_MODE,
			pv_DATA_MODE_AUX,
			pn_REGEX_ID,
			'Y',
			pv_NAME,
			pv_CUSTOMER_EDITABLE,
			'N'
		FROM DEVICE.CONFIG_TEMPLATE CT 
		WHERE CONFIG_TEMPLATE_TYPE_ID = 3 
			AND DEVICE_TYPE_ID = 13 
			AND NOT EXISTS (
				SELECT 1 
				FROM DEVICE.CONFIG_TEMPLATE_SETTING 
				WHERE DEVICE_TYPE_ID = 13 
				AND DEVICE_SETTING_PARAMETER_CD = pv_PARAMETER_CD);
	END;
	
	PROCEDURE ADD_CTS(
		pn_CONFIG_TEMPLATE_ID NUMBER,
		pv_PARAMETER_CD VARCHAR2,
		pv_SETTING_VALUE VARCHAR2) 
	IS
	BEGIN
		INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(
			CONFIG_TEMPLATE_ID, 
			DEVICE_SETTING_PARAMETER_CD, 
			CONFIG_TEMPLATE_SETTING_VALUE) 
		VALUES(pn_CONFIG_TEMPLATE_ID, pv_PARAMETER_CD, pv_SETTING_VALUE);
	END;
	
	PROCEDURE ADD_CTS_REGEX(
		pv_REGEX_NAME VARCHAR2,
		pv_REGEX_DESC VARCHAR2,
		pv_REGEX_VALUE VARCHAR2) 
	IS
	BEGIN
		INSERT INTO DEVICE.CONFIG_TEMPLATE_REGEX(
			REGEX_NAME, 
			REGEX_DESC, 
			REGEX_VALUE) 
		VALUES(pv_REGEX_NAME, pv_REGEX_DESC, pv_REGEX_VALUE);
	END;	
	
BEGIN
	
	-- add new regex for AVAS merchant Id validation
	
	ADD_CTS_REGEX('HEX_ASCII_64', 'Validate exactly 64 Hex symbols', '^(?:[0-9a-fA-F]{64})?$');	
	
	SELECT MAX(regex_id) INTO ln_config_regex_id
	FROM device.config_template_regex
	WHERE regex_name = 'HEX_ASCII_64';
	
	-- add new indexes to device_setting_parameter
	
	ADD_DSP('2003', 'Y', 'Bezel VAS Program 1');
	ADD_DSP('2004', 'Y', 'Bezel VAS Program 1 - Custom Merchant ID');
	ADD_DSP('2005', 'Y', 'Bezel VAS Program 1 - Custom URL');
	ADD_DSP('2006', 'Y', 'Bezel VAS Program 2');
	ADD_DSP('2007', 'Y', 'Bezel VAS Program 2 - Custom Merchant ID');
	ADD_DSP('2008', 'Y', 'Bezel VAS Program 2 - Custom URL');
	ADD_DSP('2009', 'Y', 'Bezel VAS Program 3');
	ADD_DSP('2010', 'Y', 'Bezel VAS Program 3 - Custom Merchant ID');
	ADD_DSP('2011', 'Y', 'Bezel VAS Program 3 - Custom URL');
	ADD_DSP('2012', 'Y', 'Bezel VAS Program 4');
	ADD_DSP('2013', 'Y', 'Bezel VAS Program 4 - Custom Merchant ID');
	ADD_DSP('2014', 'Y', 'Bezel VAS Program 4 - Custom URL');

	-- add new configurable parameters to config_template_setting meta template
	
	ADD_CTS_META('2003', '2', 43, 'Y', 'This property works in conjunction with property "Bezel VAS Mode" to pick what VAS promotion to use. The user can pick one of the pre-established programs or chose an advance custom mode.', 'SELECT:0=0 - None;1=1 - Custom_1;2=2 - USAT;3=3 - USConnect', 5, 20, 'A', NULL, 3, 'Bezel VAS Program 1 - Type', 'N');
	ADD_CTS_META('2004', null, 43, 'Y', 'The Custom Merchant Id is only required if the Program 1 Type (above) is set to Custom. Please use HEX ASCII.', 'TEXT:0 to 64', 64, 21, 'A', NULL, ln_config_regex_id, 'Bezel VAS Program 1 - Custom Merchant ID', 'N');
	ADD_CTS_META('2005', null, 43, 'Y', 'The Custom URL is only required if the Program 1 Type (above) is set to Custom. Note "d=" will be replaced with the ePort''s serial number.', 'TEXT:0 to 64', 64, 22, 'A', NULL, 19, 'Bezel VAS Program 1 - Custom URL', 'N');

	ADD_CTS_META('2006', '0', 43, 'Y', 'This property works in conjunction with property "Bezel VAS Mode" to pick what VAS promotion to use. The user can pick one of the pre-established programs or chose an advance custom mode.', 'SELECT:0=0 - None;1=1 - Custom_2;2=2 - USAT;3=3 - USConnect', 5, 25, 'A', NULL, 3, 'Bezel VAS Program 2 - Type', 'N');
	ADD_CTS_META('2007', null, 43, 'Y', 'The Custom Merchant Id is only required if the Program 2 Type (above) is set to Custom. Please use HEX ASCII.', 'TEXT:0 to 64', 64, 26, 'A', NULL, ln_config_regex_id, 'Bezel VAS Program 2 - Custom Merchant ID', 'N');
	ADD_CTS_META('2008', null, 43, 'Y', 'The Custom URL is only required if the Program 2 Type (above) is set to Custom. Note "d=" will be replaced with the ePort''s serial number.', 'TEXT:0 to 64', 64, 27, 'A', NULL, 19, 'Bezel VAS Program 2 - Custom URL', 'N');

	ADD_CTS_META('2009', '0', 43, 'Y', 'This property works in conjunction with property "Bezel VAS Mode" to pick what VAS promotion to use. The user can pick one of the pre-established programs or chose an advance custom mode.', 'SELECT:0=0 - None;1=1 - Custom_3;2=2 - USAT;3=3 - USConnect', 5, 30, 'A', NULL, 3, 'Bezel VAS Program 3 - Type', 'N');
	ADD_CTS_META('2010', null, 43, 'Y', 'The Custom Merchant Id is only required if the Program 3 Type (above) is set to Custom. Please use HEX ASCII.', 'TEXT:0 to 64', 64, 31, 'A', NULL, ln_config_regex_id, 'Bezel VAS Program 3 - Custom Merchant ID', 'N');
	ADD_CTS_META('2011', null, 43, 'Y', 'The Custom URL is only required if the Program 3 Type (above) is set to Custom. Note "d=" will be replaced with the ePort''s serial number.', 'TEXT:0 to 64', 64, 32, 'A', NULL, 19, 'Bezel VAS Program 3 - Custom URL', 'N');

	ADD_CTS_META('2012', '0', 43, 'Y', 'This property works in conjunction with property "Bezel VAS Mode" to pick what VAS promotion to use. The user can pick one of the pre-established programs or chose an advance custom mode.', 'SELECT:0=0 - None;1=1 - Custom_4;2=2 - USAT;3=3 - USConnect', 5, 35, 'A', NULL, 3, 'Bezel VAS Program 4 - Type', 'N');
	ADD_CTS_META('2013', null, 43, 'Y', 'The Custom Merchant Id is only required if the Program 4 Type (above) is set to Custom. Please use HEX ASCII.', 'TEXT:0 to 64', 64, 36, 'A', NULL, ln_config_regex_id, 'Bezel VAS Program 4 - Custom Merchant ID', 'N');
	ADD_CTS_META('2014', null, 43, 'Y', 'The Custom URL is only required if the Program 4 Type (above) is set to Custom. Note "d=" will be replaced with the ePort''s serial number.', 'TEXT:0 to 64', 64, 37, 'A', NULL, 19, 'Bezel VAS Program 4 - Custom URL', 'N');
	
	-- updates to existing config template settings of meta template
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET DESCRIPTION = 'Sets VMC or Kiosk Interface type. Most devices support interfaces 1-6. ePort G9 or G10 with firmware >=2.04.005 supports interfaces 7 and 8. If set to either 7 or 8, the ePort will disable MDB Alerts.'	
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '1500';
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET DESCRIPTION = 'Represents the number of reattempts on when the ePort sends a Poll Response Message, but it did not receive an ACK from the VMC. 5 is default. Set to 0 to disable message retries.'
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '1004';
		
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET DESCRIPTION = 'This number represents the delay between transmitted bytes. It can either be:<br/> Edges (all revs) and G9/G10s (before 2.02.009e): 1 = no delay, 2= 1 bit delay.<br/> G9/G10s (>= 2.02.009e): 1= 1 bit delay, 2= 4 bit delay'
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '1009';
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET DESCRIPTION = 'If 0, disabled {default}, at the beginning of each cashless session, the ePort will send the VMC the balance of funds available for the consumer to spend. If 1, enabled, the VMC will be sent FFFF instead which a special code to instruct the VMC to hide the balance from the consumer.'
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '1016';

	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET DESCRIPTION = 'Sets how the Aux Serial Port is used. Most G9/G10 devices support 0-2. G9/G10 rev >=2.04.005 supports 4.'
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '1107';
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET DESCRIPTION = 'The ePort simulates a hand held device. This value represents the number of simulated plug-in attempts of a handheld. This value should only be changed with engineering guidance. Min value is 1. Default 2.'
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '1113';
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING 
	SET DESCRIPTION = 'The ePort attempts communication with a VMC by sending the ENQ control character. Some vendors, like ones equipped with Bill recyclers, require more ENQs to be sent to wake it up. This value should only be changed with engineering guidance. Min value is 5. Default is 10.'
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '1114';
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET DESCRIPTION = 'This value is the number of seconds that must elapse after the Host stops communicating with the ePort before all work in process is marked Failed and the unit will be disabled. Note: Device will change the improper setting of 0 to the value of 10 seconds. {default is 7200}'
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '1601';
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET DESCRIPTION = 'Authorization response timeout in seconds. Most ePort revisions will not support over 20. G9 or G10 rev >=2.04.005 will support a range of 5 of 120. Please consult engineering on the use of this property.' 
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '10';
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET DESCRIPTION = 'If the ePort has not communicated in N seconds the modem''s PDP context will be forced closed. PDP context will be reestablish in line with the next TCP session. {7200 default, 0 is disabled}' 
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '35';	

	-- check if config template exists yet
	
	SELECT MAX(config_template_id) INTO ln_config_template_id
	FROM device.config_template
	WHERE config_template_name = lv_config_template_name;
	
	IF ln_config_template_id IS NULL THEN
		-- create it if not
		INSERT INTO device.config_template(config_template_type_id, config_template_name, device_type_id, property_list_version)
		VALUES(ln_config_template_type_id, lv_config_template_name, ln_device_type_id, ln_property_list_version);
		
		SELECT MAX(config_template_id) INTO ln_config_template_id
		FROM device.config_template
		WHERE config_template_name = lv_config_template_name;
	END IF;
	
	-- remove existing settings for this template, if any
	
	DELETE FROM DEVICE.CONFIG_TEMPLATE_SETTING WHERE CONFIG_TEMPLATE_ID = ln_config_template_id;

	-- add the new parameters to the default template
	
	ADD_CTS(ln_config_template_id, '2003', '2');
	ADD_CTS(ln_config_template_id, '2004', null);
	ADD_CTS(ln_config_template_id, '2005', null);
	ADD_CTS(ln_config_template_id, '2006', '0');
	ADD_CTS(ln_config_template_id, '2007', null);
	ADD_CTS(ln_config_template_id, '2008', null);	
	ADD_CTS(ln_config_template_id, '2009', '0');
	ADD_CTS(ln_config_template_id, '2010', null);
	ADD_CTS(ln_config_template_id, '2011', null);
	ADD_CTS(ln_config_template_id, '2012', '0');
	ADD_CTS(ln_config_template_id, '2013', null);
	ADD_CTS(ln_config_template_id, '2014', null);
	
	-- copy all other settings from previous properties list
	INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(CONFIG_TEMPLATE_ID, DEVICE_SETTING_PARAMETER_CD, CONFIG_TEMPLATE_SETTING_VALUE) 
	SELECT ln_config_template_id, DEVICE_SETTING_PARAMETER_CD, CONFIG_TEMPLATE_SETTING_VALUE
	FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS
	JOIN DEVICE.CONFIG_TEMPLATE CT ON CTS.CONFIG_TEMPLATE_ID = CT.CONFIG_TEMPLATE_ID
	WHERE CONFIG_TEMPLATE_NAME = 'DEFAULT-CFG-' || ln_device_type_id || '-' || (ln_property_list_version - 1)
	AND DEVICE_SETTING_PARAMETER_CD NOT IN (
		SELECT DEVICE_SETTING_PARAMETER_CD 
		FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS 
		WHERE CONFIG_TEMPLATE_ID = ln_config_template_id);

	-- delete if exists and insert the updated aggregated properties list
	
	DELETE FROM DEVICE.DEVICE_TYPE_PROPERTY_LIST
	WHERE device_type_id = ln_device_type_id
	AND property_list_version = ln_property_list_version;

	INSERT INTO DEVICE.DEVICE_TYPE_PROPERTY_LIST(device_type_id, property_list_version, all_property_list_ids, configurable_property_list_ids)
	VALUES(ln_device_type_id, ln_property_list_version, 
		'10|30-35|50-52|60-64|70|80|81|85-87|100-108|200-208|210-213|215|300-302|310-315|400|401|405|406|410|411|415|416|420|421|425|426|430|431|435|436|440-442|444|446|450-451|1001-1028|1101|1103|1104|1107|1110-1114|1200-1207|1300-1303|1400-1415|1500-1528|1600-1603|2000-2014',
		'10|30-35|70|85-87|215|400|401|405|406|410|411|415|416|420|421|425|426|430|431|435|436|440-442|444|446|450-451|1001-1028|1101|1103|1104|1107|1110-1114|1200-1207|1300-1303|1400-1415|1500-1528|1600-1603|2000-2014');


	COMMIT;
END;
/
