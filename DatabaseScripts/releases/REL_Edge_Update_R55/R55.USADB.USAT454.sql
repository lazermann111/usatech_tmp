DECLARE
	ln_property_list_version NUMBER := 0;
	ln_device_type_id device_type.device_type_id%TYPE := 11;
	ln_config_template_type_id config_template_type.config_template_type_id%TYPE := 1;
	lv_config_template_name config_template.config_template_name%TYPE := 'EPORT-INTERACTIVE-DEFAULT-CFG-' || ln_device_type_id || '-' || ln_property_list_version;
	ln_config_template_id config_template.config_template_id%TYPE;

	PROCEDURE ADD_CTS(
		pn_CONFIG_TEMPLATE_ID NUMBER,
		pv_PARAMETER_CD VARCHAR2,
		pv_SETTING_VALUE VARCHAR2) 
	IS
	BEGIN
		INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(
			CONFIG_TEMPLATE_ID, 
			DEVICE_SETTING_PARAMETER_CD, 
			CONFIG_TEMPLATE_SETTING_VALUE) 
		VALUES(pn_CONFIG_TEMPLATE_ID, pv_PARAMETER_CD, pv_SETTING_VALUE);
	END;

	PROCEDURE ADD_CTS_META(
		pv_PARAMETER_CD VARCHAR2,
		pv_SETTING_VALUE VARCHAR2,
		pn_CATEGORY_ID NUMBER,
		pv_DISPLAY VARCHAR2,
		pv_DESCRIPTION VARCHAR2,
		pv_EDITOR VARCHAR2,
		pn_FIELD_SIZE NUMBER,
		pn_DISPLAY_ORDER NUMBER,
		pv_DATA_MODE VARCHAR2,
		pv_DATA_MODE_AUX VARCHAR2,
		pn_REGEX_ID NUMBER,
		pv_NAME VARCHAR2,
		pv_CUSTOMER_EDITABLE VARCHAR2,
		pv_STORED_IN_PENNIES VARCHAR2) 
	IS 
	BEGIN
		INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(
			CONFIG_TEMPLATE_ID,
			DEVICE_SETTING_PARAMETER_CD,
			CONFIG_TEMPLATE_SETTING_VALUE,
			CATEGORY_ID,
			DISPLAY,
			DESCRIPTION,
			EDITOR,
			CONVERTER,
			CLIENT_SEND,
			ALIGN,
			FIELD_SIZE,
			FIELD_OFFSET,
			PAD_CHAR,
			PROPERTY_LIST_VERSION,
			DEVICE_TYPE_ID,
			DISPLAY_ORDER,
			ALT_NAME,
			DATA_MODE,
			DATA_MODE_AUX,
			REGEX_ID,
			ACTIVE,
			NAME,
			CUSTOMER_EDITABLE,
			STORED_IN_PENNIES) 
		SELECT 
			CONFIG_TEMPLATE_ID, 
			pv_PARAMETER_CD,
			pv_SETTING_VALUE,
			pn_CATEGORY_ID,
			pv_DISPLAY,
			pv_DESCRIPTION,
			pv_EDITOR,
			'',
			'Y',
			NULL,
			pn_FIELD_SIZE,
			NULL,
			NULL,
			0,
			DEVICE_TYPE_ID,
			pn_DISPLAY_ORDER,
			NULL,
			pv_DATA_MODE,
			pv_DATA_MODE_AUX,
			pn_REGEX_ID,
			'Y',
			pv_NAME,
			pv_CUSTOMER_EDITABLE,
			pv_STORED_IN_PENNIES
		FROM DEVICE.CONFIG_TEMPLATE CT 
		WHERE CONFIG_TEMPLATE_TYPE_ID = 3 
			AND DEVICE_TYPE_ID = 11 
			AND NOT EXISTS (
				SELECT 1 
				FROM DEVICE.CONFIG_TEMPLATE_SETTING 
				WHERE DEVICE_TYPE_ID = 11 
				AND DEVICE_SETTING_PARAMETER_CD = pv_PARAMETER_CD);
	END;
		
BEGIN
	
	-- add meta data for new settings
	-- application
	ADD_CTS_META('1500', '1', 2, 'Y', 'Sets VMC or Kiosk Interface type.', 'SELECT:1=1 - Standard MDB;3=3 - Coin Pulse;5=5 - Top-off Coin Pulse', 1, 0, 'A', NULL, 3, 'VMC Interface Type', 'Y', 'N');
	ADD_CTS_META('1200', '1000', 2, 'Y', 'Amount in Authorization Requests', 'TEXT:1 to 11', 10, 1, 'A', NULL, 26, 'Authorization Amount', 'Y', 'Y');
	ADD_CTS_META('1022', '0', 2, 'Y', 'Enables or disables recording of cash transactions. It supports MDB and Coin Pulse VMC Interface Types.', 'SELECT:0=0 - Disabled;1=1 - Enabled', 5, 3, 'A', NULL, 3, 'Cash Transaction Recording', 'Y', 'N');
	ADD_CTS_META('1202', null, 2, 'Y', 'Amount added to the sale amount per each vended item.', 'TEXT:1 to 5', 4, 3, 'A', NULL, 26, 'Two-Tier Pricing', 'Y', 'Y');
	
	-- mdb
	ADD_CTS_META('1024', null, 6, 'N', 'Enables or disables MDB Alerts. Note, Alerts must be enabled to support USALive''s Device Alerts of types MDB Alerts Bill Acceptor and MDB Alerts Coin Changer.', 'SELECT:0=0 - Disabled;1=1 - Enabled', 1, 24, 'A', NULL, 3, 'MDB Alerts', 'Y', 'N');
	
	-- coin pulse
	ADD_CTS_META('1501', null, 4, 'Y', 'Used to setup how long each pulse will stay in the active state. (i.e. pulse width.) The pulse width duration is calculated by multiplying this parameter by 10ms.  A value of 10 would yield a pulse width of 100 ms. Valid values: 1-255.', 'TEXT:1 to 3', 3, 2, 'A', 'N', 3, 'Coin Pulse Duration', 'Y', 'N');
	ADD_CTS_META('1502', null, 4, 'Y', 'Used to setup how much time is required between pulses that the pulse output line will stay in the inactive state.  The duration of time that the pulse output will stay in the inactive state between consecutive pulses is calculated by multiplying this parameter by 10ms. A value of "25" would yield a delay between consecutive pulses of 250 ms.  Valid Values: 1 - 1000.', 'TEXT:1 to 4', 4, 3, 'A', 'N', 3, 'Coin Pulse Spacing', 'Y', 'N');
	ADD_CTS_META('1503', null, 4, 'Y', 'The equivalent monetary value of each output pulse.  If the VMC (Vending Machine Controller) / connected equipment expect that each pulse is equal to 25 cents, then this value would be 0.25.  This field combined with the item price is used to calculate how many pulses to transmit when an item is purchased.  Valid Values: 0.01 - 655.35.', 'TEXT:1 to 6', 5, 4, 'A', 'N', 26, 'Coin Pulse Value', 'Y', 'Y');
	ADD_CTS_META('1504', null, 4, 'Y', 'The number of seconds in which a "Multi-Item Selection Session" will time out if there is no activity.  Valid Values: 1-255.', 'TEXT:1 to 3', 3, 5, 'A', 'N', 3, 'Coin Auto Timeout', 'Y', 'N');
	ADD_CTS_META('1505', null, 4, 'Y', 'Determines whether the Reader Enable input is active high or active low', 'SELECT:1=1 - Active Low;2=2 - Active High', 1, 6, 'A', NULL, 19, 'Coin Reader Enable Active State', 'Y', 'N');
	ADD_CTS_META('1506', null, 4, 'Y', 'This field is used with the coin pulse interface to set pricing for 1st button press prior to starting a transaction. An entry of 0 will cause the item to be skipped.', 'TEXT:1 to 6', 5, 7, 'A', 'N', 26, 'Coin Item Price #1', 'Y','Y');
	ADD_CTS_META('1507', null, 4, 'Y', 'Used to display a description instead of a $ value for 1st button press prior to starting a transaction. Data entry example: Charge #1.', 'TEXT:0 to 16', 16, 8, 'A', NULL, 19, 'Coin Item Price #1 Label', 'Y', 'N');
	ADD_CTS_META('1508', null, 4, 'Y', 'This field is used with the coin pulse interface to set pricing for 2nd button press prior to starting a transaction. An entry of 0 will cause the item to be skipped.', 'TEXT:1 to 6', 5, 7, 'A', 'N', 26, 'Coin Item Price #2', 'Y', 'Y');
	ADD_CTS_META('1509', null, 4, 'Y', 'Used to display a description instead of a $ value for 2nd button press prior to starting a transaction. Data entry example: Charge #2.', 'TEXT:0 to 16', 16, 8, 'A', NULL, 19, 'Coin Item Price #2 Label', 'Y', 'N');
	ADD_CTS_META('1510', null, 4, 'Y', 'This field is used with the coin pulse interface to set pricing for 3rd button press prior to starting a transaction. An entry of 0 will cause the item to be skipped.', 'TEXT:1 to 6', 5, 7, 'A', 'N', 26, 'Coin Item Price #3', 'Y', 'Y');
	ADD_CTS_META('1511', null, 4, 'Y', 'Used to display a description instead of a $ value for 3rd button press prior to starting a transaction. Data entry example: Charge #3.', 'TEXT:0 to 16', 16, 8, 'A', NULL, 19, 'Coin Item Price #3 Label', 'Y', 'N');
	ADD_CTS_META('1512', null, 4, 'Y', 'This field is used with the coin pulse interface to set pricing for 4th button press prior to starting a transaction. An entry of 0 will cause the item to be skipped.', 'TEXT:1 to 6', 5, 7, 'A', 'N', 26, 'Coin Item Price #4', 'Y', 'Y');
	ADD_CTS_META('1513', null, 4, 'Y', 'Used to display a description instead of a $ value for 4th button press prior to starting a transaction. Data entry example: Charge #4.', 'TEXT:0 to 16', 16, 8, 'A', NULL, 19, 'Coin Item Price #4 Label', 'Y', 'N');
	ADD_CTS_META('1514', null, 4, 'Y', 'This field is used with the coin pulse interface to set pricing for 5th button press prior to starting a transaction. An entry of 0 will cause the item to be skipped.', 'TEXT:1 to 6', 5, 7, 'A', 'N', 26, 'Coin Item Price #5', 'Y', 'Y');
	ADD_CTS_META('1515', null, 4, 'Y', 'Used to display a description instead of a $ value for 5th button press prior to starting a transaction. Data entry example: Charge #5.', 'TEXT:0 to 16', 16, 8, 'A', NULL, 19, 'Coin Item Price #5 Label', 'Y', 'N');
	ADD_CTS_META('1516', null, 4, 'Y', 'This field is used with the coin pulse interface to set pricing for 6th button press prior to starting a transaction. An entry of 0 will cause the item to be skipped.', 'TEXT:1 to 6', 5, 7, 'A', 'N', 26, 'Coin Item Price #6', 'Y', 'Y');
	ADD_CTS_META('1517', null, 4, 'Y', 'Used to display a description instead of a $ value for 6th button press prior to starting a transaction. Data entry example: Charge #6.', 'TEXT:0 to 16', 16, 8, 'A', NULL, 19, 'Coin Item Price #6 Label', 'Y', 'N');
	ADD_CTS_META('1518', null, 4, 'Y', 'This field is used with the coin pulse interface to set pricing for 7th button press prior to starting a transaction. An entry of 0 will cause the item to be skipped.', 'TEXT:1 to 6', 5, 7, 'A', 'N', 26, 'Coin Item Price #7', 'Y', 'Y');
	ADD_CTS_META('1519', null, 4, 'Y', 'Used to display a description instead of a $ value for 7th button press prior to starting a transaction. Data entry example: Charge #7.', 'TEXT:0 to 16', 16, 8, 'A', NULL, 19, 'Coin Item Price #7 Label', 'Y', 'N');
	ADD_CTS_META('1520', null, 4, 'Y', 'This field is used with the coin pulse interface to set pricing for 8th button press prior to starting a transaction. An entry of 0 will cause the item to be skipped.', 'TEXT:1 to 6', 5, 7, 'A', 'N', 26, 'Coin Item Price #8', 'Y', 'Y');
	ADD_CTS_META('1521', null, 4, 'Y', 'Used to display a description instead of a $ value for 8th button press prior to starting a transaction. Data entry example: Charge #8.', 'TEXT:0 to 16', 16, 8, 'A', NULL, 19, 'Coin Item Price #8 Label', 'Y', 'N');
	ADD_CTS_META('1522', null, 4, 'Y', 'Number of seconds associated with Coin Pulse Value. This tells the device how long a Top-off session is running. For a unit with Coin Pulse Value of 25 and Coin Item Price #1 of 2.50, the time would be 300 seconds.', 'TEXT:1 to 5', 5, 23, 'A', NULL, 3, 'Pulse Time Value', 'Y', 'N');
	ADD_CTS_META('1523', null, 4, 'Y', 'The equivalent monetary value that the consumer will be charged per each additional Card Swipe. If the VMC / connected equipment expect that each pulse is equal to 25 cents, then 4 pulses will be sent to the VMC and an additional 120 seconds will be added to the running Top-off time. Valid Values: 0.01 - 655.35.', 'TEXT:1 to 6', 6, 24, 'A', NULL, 26, 'Top-off Value', 'Y', 'Y');
	ADD_CTS_META('1524', null, 4, 'Y', 'Determines whether the Cash Input Pulse is active high or active low. Cash Transaction Recording has to be enabled along with setting this value.', 'USConnect', 1, 25, 'A', NULL, 3, 'Pulse Capture Polarity', 'Y', 'N');
	ADD_CTS_META('1525', null, 4, 'Y', 'Used to set up how long each input pulse will have to stay in the active state (in milliseconds) for a Pulse Cash Transaction to be acknowledged (i.e. pulse width). Valid Values: 1 - 255. Cash Transaction Recording has to be enabled along with setting this value.', 'TEXT:1 to 3', 3, 26, 'A', NULL, 3, 'Pulse Capture Debounce', 'Y', 'N');
	ADD_CTS_META('1526', null, 4, 'Y', 'The equivalent monetary value of each Cash input pulse. Valid Values: 0.01 - 655.35. Cash Transaction Recording has to be enabled along with setting this value.', 'TEXT:1 to 6', 6, 27, 'A', NULL, 26, 'Pulse Capture Value', 'Y', 'Y');

	-- check if config template exists yet
	
	SELECT MAX(config_template_id) INTO ln_config_template_id
	FROM device.config_template
	WHERE config_template_name = lv_config_template_name;
	
	IF ln_config_template_id IS NULL THEN
		-- create it if not
		INSERT INTO device.config_template(config_template_type_id, config_template_name, device_type_id, property_list_version)
		VALUES(ln_config_template_type_id, lv_config_template_name, ln_device_type_id, ln_property_list_version);
		
		SELECT MAX(config_template_id) INTO ln_config_template_id
		FROM device.config_template
		WHERE config_template_name = lv_config_template_name;
	END IF;
	
	-- remove existing settings for this template, if any
	
	DELETE FROM DEVICE.CONFIG_TEMPLATE_SETTING WHERE CONFIG_TEMPLATE_ID = ln_config_template_id;

	-- add the parameters to the default template
	
	ADD_CTS(ln_config_template_id, '1500', '1');
	ADD_CTS(ln_config_template_id, '1200', '1000');
	ADD_CTS(ln_config_template_id, '1022', '1');	
	ADD_CTS(ln_config_template_id, '1202', '0');
	
	ADD_CTS(ln_config_template_id, '1024', '1');
	
	ADD_CTS(ln_config_template_id, '1501', '15');
	ADD_CTS(ln_config_template_id, '1502', '25');	
	ADD_CTS(ln_config_template_id, '1503', '25');
	ADD_CTS(ln_config_template_id, '1504', '20');
	ADD_CTS(ln_config_template_id, '1505', '2');
	ADD_CTS(ln_config_template_id, '1506', '25');
	ADD_CTS(ln_config_template_id, '1507', null);
	ADD_CTS(ln_config_template_id, '1508', '0');
	ADD_CTS(ln_config_template_id, '1509', null);
	ADD_CTS(ln_config_template_id, '1510', '0');	
	ADD_CTS(ln_config_template_id, '1511', null);
	ADD_CTS(ln_config_template_id, '1512', '0');
	ADD_CTS(ln_config_template_id, '1513', null);
	ADD_CTS(ln_config_template_id, '1514', '0');
	ADD_CTS(ln_config_template_id, '1515', null);
	ADD_CTS(ln_config_template_id, '1516', '0');
	ADD_CTS(ln_config_template_id, '1517', null);
	ADD_CTS(ln_config_template_id, '1518', '0');	
	ADD_CTS(ln_config_template_id, '1519', null);
	ADD_CTS(ln_config_template_id, '1520', '0');
	ADD_CTS(ln_config_template_id, '1521', null);
	ADD_CTS(ln_config_template_id, '1522', '30');
	ADD_CTS(ln_config_template_id, '1523', '100');
	ADD_CTS(ln_config_template_id, '1524', '2');
	ADD_CTS(ln_config_template_id, '1525', '10');
	ADD_CTS(ln_config_template_id, '1526', '25');
	
	COMMIT;
END;
/