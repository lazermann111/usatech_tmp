-- view permissions
GRANT SELECT ON REPORT.VW_CUSTOMER_SALES_ANALYTICS TO USALIVE_APP_ROLE;
GRANT SELECT ON REPORT.VW_CUSTOMER_SALES_ANALYTICS TO USAT_DEV_READ_ONLY;  

COMMIT;

DECLARE
	ln_report_id NUMBER := 1;
	ln_user_id NUMBER :=1;
BEGIN
	
	SELECT MAX(REPORT_ID) + 1 INTO ln_report_id 
	FROM REPORT.REPORTS;
	
	-- insert report
	INSERT INTO REPORT.REPORTS
	(REPORT_ID, TITLE, GENERATOR_ID, UPD_DT, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID, CATEGORY)
	SELECT
	 ln_report_id,
	  'Customer Sales Analysis', 
	  1,
	  sysdate,
	  0,
	  'Customer Sales Analysis',
	  'Generates an csv file containing 24 months of sales data for the customer specified in the parameter.',
	  'N',
	  0,
	  'Accounting'
	FROM DUAL; 

	-- insert report parameters 
	INSERT INTO REPORT.REPORT_PARAM
	(REPORT_ID, PARAM_NAME, PARAM_VALUE)
	SELECT
	  ln_report_id,
	  'query',
	  'SELECT 
  customer_name,  
  eport_serial_num, 
  activation_date,
  min_tran_date,
  FIRST_31_CC_SALES,
  FIRST_31_CASH_SALES,
  FIRST_31_TOTAL_SALES,
  FIRST_31_CC_COUNT,
  FIRST_31_CASH_COUNT,
  Month2_CC_SALES,
  Month2_CASH_SALES,
  Month2_TOTAL_SALES,
  Month2_CC_COUNT,
  Month2_CASH_COUNT,
  Month3_CC_SALES,
  Month3_CASH_SALES,
  Month3_TOTAL_SALES,
  Month3_CC_COUNT,
  Month3_CASH_COUNT,
  Month4_CC_SALES,
  Month4_CASH_SALES,
  Month4_TOTAL_SALES,
  Month4_CC_COUNT,
  Month4_CASH_COUNT,
  Month5_CC_SALES,
  Month5_CASH_SALES,
  Month5_TOTAL_SALES,
  Month5_CC_COUNT,
  Month5_CASH_COUNT,
  Month6_CC_SALES,
  Month6_CASH_SALES,
  Month6_TOTAL_SALES,
  Month6_CC_COUNT,
  Month6_CASH_COUNT,
  Month7_CC_SALES,
  Month7_CASH_SALES,
  Month7_TOTAL_SALES,
  Month7_CC_COUNT,
  Month7_CASH_COUNT,
  Month8_CC_SALES,
  Month8_CASH_SALES,
  Month8_TOTAL_SALES,
  Month8_CC_COUNT,
  Month8_CASH_COUNT,
  Month9_CC_SALES,
  Month9_CASH_SALES,
  Month9_TOTAL_SALES,
  Month9_CC_COUNT,
  Month9_CASH_COUNT,
  Month10_CC_SALES,
  Month10_CASH_SALES,
  Month10_TOTAL_SALES,
  Month10_CC_COUNT,
  Month10_CASH_COUNT,
  Month11_CC_SALES,
  Month11_CASH_SALES,
  Month11_TOTAL_SALES,
  Month11_CC_COUNT,
  Month11_CASH_COUNT,
  Month12_CC_SALES,
  Month12_CASH_SALES,
  Month12_TOTAL_SALES,
  Month12_CC_COUNT,
  Month12_CASH_COUNT,
  Month13_CC_SALES,
  Month13_CASH_SALES,
  Month13_TOTAL_SALES,
  Month13_CC_COUNT,
  Month13_CASH_COUNT,
  Month14_CC_SALES,
  Month14_CASH_SALES,
  Month14_TOTAL_SALES,
  Month14_CC_COUNT,
  Month14_CASH_COUNT,
  Month15_CC_SALES,
  Month15_CASH_SALES,
  Month15_TOTAL_SALES,
  Month15_CC_COUNT,
  Month15_CASH_COUNT,
  Month16_CC_SALES,
  Month16_CASH_SALES,
  Month16_TOTAL_SALES,
  Month16_CC_COUNT,
  Month16_CASH_COUNT,
  Month17_CC_SALES,
  Month17_CASH_SALES,
  Month17_TOTAL_SALES,
  Month17_CC_COUNT,
  Month17_CASH_COUNT,
  Month18_CC_SALES,
  Month18_CASH_SALES,
  Month18_TOTAL_SALES,
  Month18_CC_COUNT,
  Month18_CASH_COUNT,
  Month19_CC_SALES,
  Month19_CASH_SALES,
  Month19_TOTAL_SALES,
  Month19_CC_COUNT,
  Month19_CASH_COUNT,
  Month20_CC_SALES,
  Month20_CASH_SALES,
  Month20_TOTAL_SALES,
  Month20_CC_COUNT,
  Month20_CASH_COUNT,
  Month21_CC_SALES,
  Month21_CASH_SALES,
  Month21_TOTAL_SALES,
  Month21_CC_COUNT,
  Month21_CASH_COUNT,
  Month22_CC_SALES,
  Month22_CASH_SALES,
  Month22_TOTAL_SALES,
  Month22_CC_COUNT,
  Month22_CASH_COUNT,
  Month23_CC_SALES,
  Month23_CASH_SALES,
  Month23_TOTAL_SALES,
  Month23_CC_COUNT,
  Month23_CASH_COUNT,
  Month24_CC_SALES,
  Month24_CASH_SALES,
  Month24_TOTAL_SALES,
  Month24_CC_COUNT,
  Month24_CASH_COUNT
FROM REPORT.VW_CUSTOMER_SALES_ANALYTICS 
WHERE CUSTOMER_ID = ? '
	FROM DUAL;

	INSERT INTO REPORT.REPORT_PARAM
	(REPORT_ID, PARAM_NAME, PARAM_VALUE)
	SELECT
	  ln_report_id,
	  'paramNames',
	  'customerIds'
	FROM DUAL;    
	  
	INSERT INTO REPORT.REPORT_PARAM
	(REPORT_ID, PARAM_NAME, PARAM_VALUE)
	SELECT
	  ln_report_id,
	  'paramTypes',
	  'BIGINT'
	FROM DUAL; 

	INSERT INTO REPORT.REPORT_PARAM
	(REPORT_ID, PARAM_NAME, PARAM_VALUE)
	SELECT
	  ln_report_id,
	  'params.customerIds',
	  '{m}'
	FROM DUAL;
	
	INSERT INTO REPORT.REPORT_PARAM
	(REPORT_ID, PARAM_NAME, PARAM_VALUE)
	SELECT
	  ln_report_id,
	  'header',
	  'true'
	FROM DUAL;	

	-- insert the web link to display customer selector
	INSERT INTO WEB_CONTENT.WEB_LINK
	(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER)
	SELECT
	 (SELECT MAX(WEB_LINK_ID) + 1 FROM WEB_CONTENT.WEB_LINK) AS ID,
	  'Customer Sales Analysis', 
	  './select_customer.html?basicReportId=' || ln_report_id,
	  'Generates an csv file containing 24 months of sales data for the customer specified in the parameter.',
	  'Accounting',
	  'W',
	  0  
	FROM DUAL;
	
	-- link permissions / requirements
    INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT
    (WEB_LINK_ID, REQUIREMENT_ID)
    SELECT
    (SELECT MAX(WEB_LINK_ID) FROM WEB_CONTENT.WEB_LINK),
    1
    FROM DUAL;

    INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT
    (WEB_LINK_ID, REQUIREMENT_ID)
    SELECT
    (SELECT MAX(WEB_LINK_ID) FROM WEB_CONTENT.WEB_LINK),
    28
    FROM DUAL;    
    
	COMMIT;
END;
/