grant select on report.VW_TERMINAL to USAT_PREPAID_APP_ROLE;
grant select on LOCATION.CUSTOMER_MERCHANT to USAT_PREPAID_APP_ROLE;
grant select on PSS.MERCHANT to USAT_PREPAID_APP_ROLE;
grant select on ENGINE.APP_SETTING to USAT_PREPAID_APP_ROLE;
grant update on ENGINE.APP_SETTING to USAT_PREPAID_APP_ROLE;
grant execute on PSS.PKG_CONSUMER_MAINT to USAT_PREPAID_APP_ROLE;
grant select on PSS.CONSUMER to USAT_PREPAID_APP_ROLE;
grant update on PSS.CONSUMER to USAT_PREPAID_APP_ROLE;
grant insert on PSS.CONSUMER_ACCT_BASE to USAT_PREPAID_APP_ROLE;
grant select on PSS.SEQ_GLOBAL_ACCOUNT_ID_BASE to USAT_PREPAID_APP_ROLE;
grant insert on PSS.MERCHANT_CONSUMER_ACCT to USAT_PREPAID_APP_ROLE;
grant select on PSS.MERCHANT_CONSUMER_ACCT to USAT_PREPAID_APP_ROLE;
commit;

-- Add payment type " USAT ISO Card - Prepaid Card (Tokenized) " for virtual device template 
INSERT INTO PSS.POS_PTA_TMPL_ENTRY
  (POS_PTA_TMPL_ID, 
  PAYMENT_SUBTYPE_ID, 
  POS_PTA_ACTIVATION_OSET_HR, 
  PAYMENT_SUBTYPE_KEY_ID, 
  POS_PTA_PRIORITY, 
  AUTHORITY_PAYMENT_MASK_ID, 
  CURRENCY_CD, 
  POS_PTA_PASSTHRU_ALLOW_YN_FLAG, 
  NO_CONVENIENCE_FEE)
SELECT 
	(select dst.POS_PTA_TMPL_ID from device.device_sub_type dst 
    join PSS.POS_PTA_TMPL_ENTRY ppte on dst.POS_PTA_TMPL_ID = ppte.POS_PTA_TMPL_ID 
    where dst.device_type_id = 14 and dst.device_sub_type_id in (1, 2) and ppte.CURRENCY_CD = 'USD'
    group by dst.POS_PTA_TMPL_ID), 
	(select 
      PAYMENT_SUBTYPE_ID
    from pss.payment_subtype 
    where 
      payment_subtype_name = 'USAT ISO Card - Prepaid Card (Tokenized)'),
	0, 
	(SELECT INTERNAL_PAYMENT_TYPE_ID
    FROM PSS.INTERNAL_PAYMENT_TYPE 
  where INTERNAL_PAYMENT_TYPE_DESC = 'Special Card (prepaid)'),
	1, 
	( SELECT 
    AUTHORITY_PAYMENT_MASK_ID 
  FROM PSS.AUTHORITY_PAYMENT_MASK 
  where AUTHORITY_PAYMENT_MASK_DESC = 'USAT Prepaid Card Manual Entry'),
	'USD', 
	'N',
	'Y'
FROM DUAL
WHERE NOT EXISTS (
	select ppte.POS_PTA_TMPL_ENTRY_ID  from device.device_sub_type dst 
    join PSS.POS_PTA_TMPL_ENTRY ppte on dst.POS_PTA_TMPL_ID = ppte.POS_PTA_TMPL_ID 
    join PSS.PAYMENT_SUBTYPE ps on ppte.PAYMENT_SUBTYPE_ID = ps.PAYMENT_SUBTYPE_ID
  where dst.device_type_id = 14 
    and dst.device_sub_type_id in (1, 2) 
    and ppte.CURRENCY_CD = 'USD'
    and ps.PAYMENT_SUBTYPE_NAME = 'USAT ISO Card - Prepaid Card (Tokenized)');
commit;

INSERT INTO PSS.POS_PTA_TMPL_ENTRY
  (POS_PTA_TMPL_ID, 
  PAYMENT_SUBTYPE_ID, 
  POS_PTA_ACTIVATION_OSET_HR, 
  PAYMENT_SUBTYPE_KEY_ID, 
  POS_PTA_PRIORITY, 
  AUTHORITY_PAYMENT_MASK_ID, 
  CURRENCY_CD, 
  POS_PTA_PASSTHRU_ALLOW_YN_FLAG, 
  NO_CONVENIENCE_FEE)
SELECT 
	(select dst.POS_PTA_TMPL_ID from device.device_sub_type dst 
    join PSS.POS_PTA_TMPL_ENTRY ppte on dst.POS_PTA_TMPL_ID = ppte.POS_PTA_TMPL_ID 
    where dst.device_type_id = 14 and dst.device_sub_type_id in (1, 2) and ppte.CURRENCY_CD = 'CAD'
    group by dst.POS_PTA_TMPL_ID), 
	(select 
      PAYMENT_SUBTYPE_ID
    from pss.payment_subtype 
    where 
      payment_subtype_name = 'USAT ISO Card - Prepaid Card (Tokenized)'),
	0, 
	(SELECT INTERNAL_PAYMENT_TYPE_ID
    FROM PSS.INTERNAL_PAYMENT_TYPE 
  where INTERNAL_PAYMENT_TYPE_DESC = 'Special Card (prepaid)'),
	1, 
	( SELECT 
    AUTHORITY_PAYMENT_MASK_ID 
  FROM PSS.AUTHORITY_PAYMENT_MASK 
  where AUTHORITY_PAYMENT_MASK_DESC = 'USAT Prepaid Card Manual Entry'),
	'CAD', 
	'N',
	'Y'
FROM DUAL
WHERE NOT EXISTS (
	select ppte.POS_PTA_TMPL_ENTRY_ID  from device.device_sub_type dst 
    join PSS.POS_PTA_TMPL_ENTRY ppte on dst.POS_PTA_TMPL_ID = ppte.POS_PTA_TMPL_ID 
    join PSS.PAYMENT_SUBTYPE ps on ppte.PAYMENT_SUBTYPE_ID = ps.PAYMENT_SUBTYPE_ID
  where dst.device_type_id = 14 
    and dst.device_sub_type_id in (1, 2) 
    and ppte.CURRENCY_CD = 'CAD'
    and ps.PAYMENT_SUBTYPE_NAME = 'USAT ISO Card - Prepaid Card (Tokenized)');
commit;

-- Add payment type " USAT ISO Card - Prepaid Card (Tokenized) " for all V1 devices 
declare 
   CURSOR l_devices IS  select 
      d.device_id,
      p.POS_ID,
      d.DEVICE_SERIAL_CD
    from DEVICE.DEVICE d join PSS.POS p on d.DEVICE_ID = p.DEVICE_ID
    where REGEXP_LIKE(d.DEVICE_SERIAL_CD, '^V1-.*-USD$|^V1-.*-CAD$') and not EXISTS (
    select 1 from 
      PSS.POS p 
      join PSS.POS_PTA pta on pta.POS_ID = p.POS_ID
      join PSS.PAYMENT_SUBTYPE ps on pta.PAYMENT_SUBTYPE_ID = ps.PAYMENT_SUBTYPE_ID 
      where ps.PAYMENT_SUBTYPE_NAME = 'USAT ISO Card - Prepaid Card (Tokenized)' 
        AND (pta.pos_pta_deactivation_ts IS NULL OR pta.pos_pta_deactivation_ts > SYSDATE)
        and p.DEVICE_ID = d.DEVICE_ID
    )
and d.DEVICE_ACTIVE_YN_FLAG = 'Y';
begin
  for l_device_rec IN l_devices LOOP
    update PSS.POS_PTA set POS_PTA_PRIORITY = POS_PTA_PRIORITY + 1 
    where POS_PTA_ID in (
      select 
        pta.POS_PTA_ID
      from DEVICE.DEVICE d 
        join PSS.POS p on p.DEVICE_ID = d.DEVICE_ID
        join PSS.POS_PTA pta on pta.POS_ID = p.POS_ID
        join PSS.PAYMENT_SUBTYPE ps on pta.PAYMENT_SUBTYPE_ID = ps.PAYMENT_SUBTYPE_ID
        JOIN PSS.CLIENT_PAYMENT_TYPE CPT ON PS.CLIENT_PAYMENT_TYPE_CD = CPT.CLIENT_PAYMENT_TYPE_CD
        JOIN pss.payment_action_type pat ON cpt.payment_action_type_cd = pat.payment_action_type_cd
        JOIN pss.payment_entry_method pem ON cpt.payment_entry_method_cd = pem.payment_entry_method_cd
      where 
        d.DEVICE_ID = l_device_rec.device_id
        and(pta.pos_pta_deactivation_ts IS NULL OR pta.pos_pta_deactivation_ts > SYSDATE)
        and pat.PAYMENT_ACTION_TYPE_DESC = 'Purchase' and  pem.PAYMENT_ENTRY_METHOD_DESC = 'Token'
      );
      
       INSERT INTO pss.pos_pta
      (
        pos_id,
          payment_subtype_key_id,
          --pos_pta_device_serial_cd,
          pos_pta_activation_ts,
          pos_pta_encrypt_key,
          pos_pta_encrypt_key2,
          authority_payment_mask_id,
          pos_pta_pin_req_yn_flag,
          pos_pta_priority,
          merchant_bank_acct_id,
          currency_cd,
          payment_subtype_id,
          pos_pta_passthru_allow_yn_flag,
      pos_pta_pref_auth_amt,
      pos_pta_pref_auth_amt_max,
      pos_pta_disable_debit_denial,
      no_convenience_fee
      )
    SELECT 
      l_device_rec.pos_id,
      ppte.payment_subtype_key_id,
      --CASE WHEN pv_set_terminal_cd_to_serial = 'Y' AND ps.payment_subtype_class != 'Authority::NOP' THEN lv_device_serial_cd ELSE ppte.pos_pta_device_serial_cd END,
      sysdate,
      ppte.pos_pta_encrypt_key,
      ppte.pos_pta_encrypt_key2,
          ppte.authority_payment_mask_id,
          ppte.pos_pta_pin_req_yn_flag,
      1,
      ppte.merchant_bank_acct_id,
      ppte.currency_cd,
      ps.payment_subtype_id,
      ppte.pos_pta_passthru_allow_yn_flag,
      ppte.pos_pta_pref_auth_amt,
      ppte.pos_pta_pref_auth_amt_max,
      ppte.pos_pta_disable_debit_denial,
      ppte.no_convenience_fee
    from device.device_sub_type dst 
      join PSS.POS_PTA_TMPL_ENTRY ppte on dst.POS_PTA_TMPL_ID = ppte.POS_PTA_TMPL_ID 
    JOIN pss.payment_subtype ps ON ps.payment_subtype_id = ppte.payment_subtype_id
    JOIN pss.client_payment_type cpt ON ps.client_payment_type_cd = cpt.client_payment_type_cd
    where dst.device_type_id = 14 
      and dst.device_sub_type_id in (1, 2) 
      and ppte.CURRENCY_CD = case when REGEXP_LIKE(l_device_rec.DEVICE_SERIAL_CD, '^V1-.*-USD$') then 'USD' else 'CAD' end
      and ps.PAYMENT_SUBTYPE_NAME = 'USAT ISO Card - Prepaid Card (Tokenized)';
      commit;
  end loop;
end;



