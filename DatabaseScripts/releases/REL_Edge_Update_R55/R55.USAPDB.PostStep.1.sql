update web_content.literal set literal_value = 'We at USA Technologies, Inc. are continually striving to enhance our customers'' power to run their business effectively. The USALive website is loaded with features to help you do just that. For the best website experience, please use the latest major version of your browser. Now new in the latest USALive and ePort Connect version: <ul><li>Ability to allow negative balance for MORE cards only in offline mode</li><li>MORE website integration and rewards campaigns for payroll deduct cards</li><li>Technician card processing</li><li>Pending Payment Summary report generation based on pay cycle</li><li>Option to receive report delivery errors via email in Report Register</li><li>New weekly report delivery options</li><li>Receive a report for the previous time period shortly after report scheduling in Report Register</li><li>Fill event processing for kiosk devices</li><li>Activity By Fill Date And Region and Activity By Fill Date And Region-Credit report improvements</li><li>Report performance optimizations</li><li>Display deactivated devices on Payment Item Export reports</li><li>Various performance and user experience enhancements</li></ul>' where literal_key = 'home-page-message' and subdomain_id is null;
commit;

UPDATE DEVICE.CONFIG_TEMPLATE
SET CONFIG_TEMPLATE_NAME = 'Custom DEFAULT-CFG-13-22'
WHERE CONFIG_TEMPLATE_NAME = 'DEFAULT-CFG-13-22' AND CONFIG_TEMPLATE_TYPE_ID = 2;

CREATE TABLE CUSTOM_CONFIG_TMPL_BACKUP_R55 AS
select * from device.config_template_setting
where config_template_id in (
	select config_template_id from device.config_template where device_type_id = 13 and config_template_type_id = 2
) and device_setting_parameter_cd in (
	select device_setting_parameter_cd from device.config_template_setting where device_type_id = 13 and editor like 'TEXT:%' and editor not like 'TEXT:0%'
) and config_template_setting_value is null;

-- delete invalid required records with null values from custom G9 config templates
delete from device.config_template_setting
where config_template_id in (
	select config_template_id from device.config_template where device_type_id = 13 and config_template_type_id = 2
) and device_setting_parameter_cd in (
	select device_setting_parameter_cd from device.config_template_setting where device_type_id = 13 and editor like 'TEXT:%' and editor not like 'TEXT:0%'
) and config_template_setting_value is null;

COMMIT;
