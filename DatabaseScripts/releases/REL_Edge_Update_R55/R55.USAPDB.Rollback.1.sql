-- All Other Fee Entries report
UPDATE REPORT.REPORT_PARAM
SET PARAM_VALUE = 'SELECT
lts.entry_type,
bu.business_unit_name,
DECODE(bat.payment_schedule_id, 4, ''Y'', ''N'') monthly_payment,
cu.currency_code,
c.customer_name,
lts.fee_name,
DECODE(d.status,
	''S'', ''Y'',
	''P'', ''Y'',
	''N''
) in_eft,
TO_CHAR(TRUNC(lts.entry_date, ''MONTH''),''mm/dd/yyyy'') month_for,
COUNT(1) entry_count,
SUM (lts.amount) total_entry_amount,
lts.description
FROM (
	SELECT
	led.batch_id,
	led.amount,
	led.entry_type,
	led.settle_state_id,
	led.create_date,
	led.entry_date,
	led.description,
	sfi.fee_name
	FROM corp.ledger led
	LEFT OUTER JOIN report.trans tr
	ON tr.tran_id = led.trans_id
	LEFT OUTER JOIN (
		SELECT
		sf.service_fee_id,
		f.fee_name
		FROM corp.service_fees sf
		JOIN corp.fees f
		ON f.fee_id = sf.fee_id
	) sfi
	ON sfi.service_fee_id = led.service_fee_id
	WHERE led.entry_type IN (''SF'', ''AD'', ''SB'')
	AND led.deleted = ''N''
	AND led.settle_state_id IN (2, 3)
	AND (led.report_date >= CAST(? AS DATE) AND led.report_date < CAST(? AS DATE) + 1
	OR led.create_date >= CAST(? AS DATE) AND led.create_date < CAST(? AS DATE) + 1 AND led.report_date IS NULL)
)lts
JOIN corp.batch bat
ON bat.batch_id = lts.batch_id
JOIN corp.doc d
ON d.doc_id = bat.doc_id
JOIN corp.customer_bank cb
ON cb.customer_bank_id = d.customer_bank_id
JOIN corp.customer c
ON c.customer_id = cb.customer_id
JOIN corp.business_unit bu
ON bu.business_unit_id = d.business_unit_id
JOIN corp.currency cu
ON cu.currency_id = d.currency_id
JOIN report.trans_state ts
ON ts.state_id = lts.settle_state_id -- yes, this is non intuitive but correct
GROUP BY
lts.entry_type,
bu.business_unit_name,
DECODE(bat.payment_schedule_id, 4, ''Y'', ''N''),
cu.currency_code,
c.customer_name,
lts.fee_name,
DECODE(d.status,
	''S'', ''Y'',
	''P'', ''Y'',
	''N''
),
TRUNC(lts.entry_date, ''MONTH''),
lts.description'
WHERE REPORT_ID = (SELECT REPORT_ID FROM REPORT.REPORTS WHERE REPORT_NAME = 'All Other Fee Entries') AND PARAM_NAME = 'query';

-- All Other Fee Entries With Device report
UPDATE REPORT.REPORT_PARAM
SET PARAM_VALUE = 'SELECT
lts.entry_type,
bu.business_unit_name,
DECODE(bat.payment_schedule_id, 4, ''Y'', ''N'') monthly_payment,
cu.currency_code,
c.customer_name,
ee.eport_num,
lts.fee_name,
DECODE(d.status,
	''S'', ''Y'',
	''P'', ''Y'',
	''N''
) in_eft,
TO_CHAR(TRUNC(lts.entry_date, ''MONTH''),''mm/dd/yyyy'') month_for,
COUNT(1) entry_count,
SUM (lts.amount) total_entry_amount,
lts.description,
t.terminal_nbr,
t.sales_order_number
FROM (
	SELECT
	led.batch_id,
	led.amount,
	led.entry_type,
	led.settle_state_id,
	led.create_date,
	led.entry_date,
	led.description,
	sfi.fee_name
	FROM corp.ledger led
	LEFT OUTER JOIN report.trans tr
	ON tr.tran_id = led.trans_id
	LEFT OUTER JOIN (
		SELECT
		sf.service_fee_id,
		f.fee_name
		FROM corp.service_fees sf
		JOIN corp.fees f
		ON f.fee_id = sf.fee_id
	) sfi
	ON sfi.service_fee_id = led.service_fee_id
	WHERE led.entry_type IN (''SF'', ''AD'', ''SB'')
	AND led.deleted = ''N''
	AND led.settle_state_id IN (2, 3)
	AND (led.report_date >= CAST(? AS DATE) AND led.report_date < CAST(? AS DATE) + 1
	OR led.create_date >= CAST(? AS DATE) AND led.create_date < CAST(? AS DATE) + 1 AND led.report_date IS NULL)
)lts
JOIN corp.batch bat
ON bat.batch_id = lts.batch_id
JOIN corp.doc d
ON d.doc_id = bat.doc_id
JOIN corp.customer_bank cb
ON cb.customer_bank_id = d.customer_bank_id
JOIN corp.customer c
ON c.customer_id = cb.customer_id
JOIN corp.business_unit bu
ON bu.business_unit_id = d.business_unit_id
JOIN corp.currency cu
ON cu.currency_id = d.currency_id
JOIN report.trans_state ts
ON ts.state_id = lts.settle_state_id
LEFT outer JOIN (SELECT DISTINCT TERMINAL_ID, FIRST_VALUE(eport_serial_num) OVER (PARTITION BY TERMINAL_ID ORDER BY start_date DESC) Eport_num
from report.terminal_eport te, report.eport e where te.eport_id=e.eport_id) ee
ON bat.terminal_id=ee.terminal_id
LEFT OUTER JOIN report.terminal t ON bat.terminal_id = t.terminal_id
GROUP BY
lts.entry_type,
bu.business_unit_name,
DECODE(bat.payment_schedule_id, 4, ''Y'', ''N''),
cu.currency_code,
c.customer_name,
lts.fee_name,
DECODE(d.status,
	''S'', ''Y'',
	''P'', ''Y'',
	''N''
),
TRUNC(lts.entry_date, ''MONTH''),
lts.description
,ee.Eport_num,
t.terminal_nbr,
t.sales_order_number'
WHERE REPORT_ID = (SELECT REPORT_ID FROM REPORT.REPORTS WHERE REPORT_NAME = 'All Other Fee Entries With Device') AND PARAM_NAME = 'query';

COMMIT;
