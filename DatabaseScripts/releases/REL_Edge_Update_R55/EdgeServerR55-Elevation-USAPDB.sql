WHENEVER SQLERROR EXIT FAILURE COMMIT;

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/releases/REL_Edge_Update_R55/R55.USAPDB.1.sql?revision=HEAD
INSERT INTO pss.authority_payment_mask(payment_subtype_id, authority_payment_mask_name, authority_payment_mask_regex, authority_payment_mask_bref)
SELECT (SELECT MAX(payment_subtype_id) FROM pss.payment_subtype WHERE payment_subtype_name = 'USAT Demo - Any Card (Track 2)'),
'Demo Only: Any Card', '^(.+)$', '1:1' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM pss.authority_payment_mask WHERE authority_payment_mask_name = 'Demo Only: Any Card');

INSERT INTO PSS.PAYMENT_SUBTYPE(PAYMENT_SUBTYPE_NAME, PAYMENT_SUBTYPE_CLASS, PAYMENT_SUBTYPE_KEY_NAME, CLIENT_PAYMENT_TYPE_CD, PAYMENT_SUBTYPE_TABLE_NAME, PAYMENT_SUBTYPE_KEY_DESC_NAME, AUTHORITY_PAYMENT_MASK_ID, TRANS_TYPE_ID, CARD_TYPE_LABEL)
SELECT 'USAT Demo - Any Card (VAS)', 'Demo', 'TERMINAL_ID', 'O', 'TERMINAL', 'TERMINAL_DESC', AUTHORITY_PAYMENT_MASK_ID, 17, 'Demo'
FROM PSS.AUTHORITY_PAYMENT_MASK WHERE AUTHORITY_PAYMENT_MASK_NAME = 'Demo Only: Any Card' AND NOT EXISTS (
SELECT 1 FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'USAT Demo - Any Card (VAS)');

INSERT INTO PSS.POS_PTA_TMPL_ENTRY(POS_PTA_TMPL_ID, PAYMENT_SUBTYPE_ID, POS_PTA_ACTIVATION_OSET_HR, PAYMENT_SUBTYPE_KEY_ID, POS_PTA_PRIORITY, CURRENCY_CD)
SELECT (SELECT POS_PTA_TMPL_ID FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'DEMO: USAT Demo'), 
PAYMENT_SUBTYPE_ID, 0, (SELECT TERMINAL_ID FROM PSS.TERMINAL WHERE TERMINAL_DESC = 'USAT Demo Terminal'), 10, 'USD'
FROM PSS.PAYMENT_SUBTYPE PS
WHERE PAYMENT_SUBTYPE_NAME = 'USAT Demo - Any Card (VAS)'
AND NOT EXISTS (SELECT 1 FROM PSS.POS_PTA_TMPL_ENTRY 
WHERE POS_PTA_TMPL_ID = (SELECT POS_PTA_TMPL_ID FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'DEMO: USAT Demo')
AND PAYMENT_SUBTYPE_ID = PS.PAYMENT_SUBTYPE_ID);

UPDATE DEVICE.CONFIG_TEMPLATE_SETTING 
SET CUSTOMER_EDITABLE = NULL 
WHERE DEVICE_TYPE_ID IS NOT NULL AND DEVICE_SETTING_PARAMETER_CD = 'MAX_AUTH_AMOUNT' AND CUSTOMER_EDITABLE = 'Y'; 

UPDATE REPORT.REPORTS
SET DESCRIPTION = 'Pending Payment summary report for the bank account, checked daily at 6:00 am EST, based on the pay cycle, triggered when the EFT balance is below $25.'
WHERE REPORT_NAME LIKE 'Pending Payment Summary%';

COMMIT;

ALTER TABLE report.user_report ADD ERROR_EMAIL_LIST VARCHAR2(4000);

GRANT SELECT, INSERT, UPDATE, DELETE ON ENGINE.OB_EMAIL_QUEUE to REPORT;

INSERT INTO ENGINE.APP_SETTING(APP_SETTING_CD, APP_SETTING_VALUE, APP_SETTING_DESC)
    VALUES ('REPORT_TRANSPORT_ERROR_EMAIL_SUBJECT', 
            'Transport error has occurred. Report transport was failed.', 
            'Subject for report transport error email');
            
INSERT INTO ENGINE.APP_SETTING(APP_SETTING_CD, APP_SETTING_VALUE, APP_SETTING_DESC) 
SELECT 'REPORT_TRANSPORT_ERROR_EMAIL_FROM',
CASE REPLACE(GLOBAL_NAME, '.WORLD', '') 
            WHEN 'USAPDB' THEN 'USALiveReports@usatech.com' 
            WHEN 'ECCDB' THEN 'USALiveReports-ECC@usatech.com'
            WHEN 'USADEV02' THEN 'QualityAssurance@usatech.com'
            ELSE 'USALiveReports-local@usatech.com' END,
'Distribution list for EFT Processing notifications' FROM GLOBAL_NAME;

GRANT DELETE ON PSS.CAMPAIGN_CONSUMER_ACCT to USAT_PREPAID_APP_ROLE;
GRANT DELETE ON CONSUMER_ACCT to USAT_PREPAID_APP_ROLE;

UPDATE PSS.CONSUMER_TYPE SET MORE_ENABLED_IND='Y' WHERE CONSUMER_TYPE_ID=9;
UPDATE PSS.CONSUMER_ACCT_TYPE SET MORE_ENABLED_IND='Y' WHERE CONSUMER_ACCT_TYPE_ID=7;

ALTER TABLE report.campaign ADD ASSIGNED_TO_ALLPAYROLLCARDS CHAR(1 BYTE);

COMMIT;

UPDATE WEB_CONTENT.LITERAL SET LITERAL_VALUE = 'Select the Program' WHERE LITERAL_VALUE = 'Select the Dealer From Whom You Obtained the Devices';
UPDATE WEB_CONTENT.LITERAL SET LITERAL_VALUE = 'Row #{1}: Invalid program specified.' WHERE LITERAL_VALUE = 'Row #{1}: Invalid dealer specified.';
UPDATE WEB_CONTENT.LITERAL SET LITERAL_VALUE = 'Invalid program. Please contact Customer Service for assistance.' WHERE LITERAL_VALUE = 'Invalid dealer. Please contact Customer Service for assistence.';
COMMIT;

ALTER TABLE DEVICE.HOST_TYPE MODIFY (HOST_DEFAULT_COMPLETE_MINUT DEFAULT 0, COMM_MODULE_IND DEFAULT 'N', SIM_IND DEFAULT 'N');

INSERT INTO DEVICE.HOST_TYPE(HOST_TYPE_ID, HOST_TYPE_DESC)
SELECT 401, 'Flash' FROM DUAL
WHERE NOT EXISTS(SELECT 1 FROM DEVICE.HOST_TYPE WHERE HOST_TYPE_ID = 401);
COMMIT;

INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, LITERAL_VALUE)
    VALUES('campaign-payrolldeductcards-instructions-title', 'For Payroll Deduct cards');
INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, LITERAL_VALUE)
    VALUES('campaign-payrolldeductcards-instructions-content', 'Check the option when the campaign is for Payroll Deduct cards');

UPDATE WEB_CONTENT.LITERAL 
  SET LITERAL_VALUE = 'Check the option when the campaign is for all other cards except MORE cards and Payroll Deduct cards'
  WHERE LITERAL_KEY = 'campaign-allcards-instructions-content';
COMMIT; 

INSERT INTO REPORT.REPORTS(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
SELECT 474, 'Devices with Daily Fees', 6, 0, 'Devices with Daily Fees', 'List of devices with daily service fees', 'U', USER_ID
FROM REPORT.USER_LOGIN 
WHERE USER_NAME = 'USATMaster'
 AND NOT EXISTS(
SELECT 1
FROM REPORT.REPORTS 
WHERE REPORT_ID = 474);

INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE)
SELECT 474, PARAM_NAME, PARAM_VALUE
  FROM (SELECT '' PARAM_NAME, '' PARAM_VALUE FROM DUAL WHERE 1=0
  UNION ALL SELECT 'query', 'SELECT DISTINCT C.CUSTOMER_NAME, C.CUSTOMER_ID, E.EPORT_SERIAL_NUM, T.TERMINAL_NBR, FS.FEE_NAME, SF.FEE_AMOUNT
FROM REPORT.EPORT E 
JOIN REPORT.VW_TERMINAL_EPORT TE ON E.EPORT_ID = TE.EPORT_ID
JOIN REPORT.TERMINAL T ON TE.TERMINAL_ID = T.TERMINAL_ID
JOIN CORP.CUSTOMER C ON T.CUSTOMER_ID = C.CUSTOMER_ID
JOIN CORP.SERVICE_FEES SF ON T.TERMINAL_ID = SF.TERMINAL_ID
JOIN CORP.FREQUENCY F ON SF.FREQUENCY_ID = F.FREQUENCY_ID
JOIN CORP.FEES FS ON SF.FEE_ID = FS.FEE_ID
WHERE F.DATE_FIELD = ''DAY'' AND (SF.START_DATE IS NULL OR SF.START_DATE < SYSDATE) AND (SF.END_DATE IS NULL OR SF.END_DATE > SYSDATE)
ORDER BY UPPER(C.CUSTOMER_NAME), E.EPORT_SERIAL_NUM, FS.FEE_NAME' FROM DUAL
UNION ALL SELECT 'header', 'true' FROM DUAL
UNION ALL SELECT 'isSQLFolio', 'true' FROM DUAL
UNION ALL SELECT 'outputType', '21' FROM DUAL 
  ) P
 WHERE NOT EXISTS(
 SELECT 1
	FROM REPORT.REPORT_PARAM RP0 
   WHERE RP0.REPORT_ID = 474
	 AND RP0.PARAM_NAME = P.PARAM_NAME);
	 
COMMIT;

UPDATE REPORT.REPORT_PARAM
SET PARAM_VALUE = 'SELECT
lts.entry_type,
bu.business_unit_name,
DECODE(bat.payment_schedule_id, 4, ''Y'', ''N'') monthly_payment,
cu.currency_code,
c.customer_name,
lts.fee_name,
DECODE(d.status,
	''S'', ''Y'',
	''P'', ''Y'',
	''N''
) in_eft,
TO_CHAR(TRUNC(lts.entry_date, ''MONTH''),''mm/dd/yyyy'') month_for,
COUNT(1) entry_count,
SUM (lts.amount) total_entry_amount,
lts.description
FROM (
	SELECT
	led.batch_id,
	led.amount,
	led.entry_type,
	led.settle_state_id,
	led.create_date,
	led.entry_date,
	led.description,
	sfi.fee_name
	FROM corp.ledger led
	LEFT OUTER JOIN (
		SELECT
		sf.service_fee_id,
		f.fee_name
		FROM corp.service_fees sf
		JOIN corp.fees f
		ON f.fee_id = sf.fee_id
	) sfi
	ON sfi.service_fee_id = led.service_fee_id
	WHERE led.entry_type IN (''SF'', ''AD'', ''SB'')
	AND led.deleted = ''N''
	AND led.settle_state_id IN (2, 3)
	AND (led.report_date >= CAST(? AS DATE) AND led.report_date < CAST(? AS DATE) + 1
	OR led.create_date >= CAST(? AS DATE) AND led.create_date < CAST(? AS DATE) + 1 AND led.report_date IS NULL)
)lts
JOIN corp.batch bat
ON bat.batch_id = lts.batch_id
JOIN corp.doc d
ON d.doc_id = bat.doc_id
JOIN corp.customer_bank cb
ON cb.customer_bank_id = d.customer_bank_id
JOIN corp.customer c
ON c.customer_id = cb.customer_id
JOIN corp.business_unit bu
ON bu.business_unit_id = d.business_unit_id
JOIN corp.currency cu
ON cu.currency_id = d.currency_id
JOIN report.trans_state ts
ON ts.state_id = lts.settle_state_id -- yes, this is non intuitive but correct
GROUP BY
lts.entry_type,
bu.business_unit_name,
DECODE(bat.payment_schedule_id, 4, ''Y'', ''N''),
cu.currency_code,
c.customer_name,
lts.fee_name,
DECODE(d.status,
	''S'', ''Y'',
	''P'', ''Y'',
	''N''
),
TRUNC(lts.entry_date, ''MONTH''),
lts.description'
WHERE REPORT_ID = (SELECT REPORT_ID FROM REPORT.REPORTS WHERE REPORT_NAME = 'All Other Fee Entries') AND PARAM_NAME = 'query';

UPDATE REPORT.REPORT_PARAM
SET PARAM_VALUE = 'SELECT
lts.entry_type,
bu.business_unit_name,
DECODE(bat.payment_schedule_id, 4, ''Y'', ''N'') monthly_payment,
cu.currency_code,
c.customer_name,
ee.eport_num,
lts.fee_name,
DECODE(d.status,
	''S'', ''Y'',
	''P'', ''Y'',
	''N''
) in_eft,
TO_CHAR(TRUNC(lts.entry_date, ''MONTH''),''mm/dd/yyyy'') month_for,
COUNT(1) entry_count,
SUM (lts.amount) total_entry_amount,
lts.description,
t.terminal_nbr,
t.sales_order_number
FROM (
	SELECT
	led.batch_id,
	led.amount,
	led.entry_type,
	led.settle_state_id,
	led.create_date,
	led.entry_date,
	led.description,
	sfi.fee_name
	FROM corp.ledger led
	LEFT OUTER JOIN (
		SELECT
		sf.service_fee_id,
		f.fee_name
		FROM corp.service_fees sf
		JOIN corp.fees f
		ON f.fee_id = sf.fee_id
	) sfi
	ON sfi.service_fee_id = led.service_fee_id
	WHERE led.entry_type IN (''SF'', ''AD'', ''SB'')
	AND led.deleted = ''N''
	AND led.settle_state_id IN (2, 3)
	AND (led.report_date >= CAST(? AS DATE) AND led.report_date < CAST(? AS DATE) + 1
	OR led.create_date >= CAST(? AS DATE) AND led.create_date < CAST(? AS DATE) + 1 AND led.report_date IS NULL)
)lts
JOIN corp.batch bat
ON bat.batch_id = lts.batch_id
JOIN corp.doc d
ON d.doc_id = bat.doc_id
JOIN corp.customer_bank cb
ON cb.customer_bank_id = d.customer_bank_id
JOIN corp.customer c
ON c.customer_id = cb.customer_id
JOIN corp.business_unit bu
ON bu.business_unit_id = d.business_unit_id
JOIN corp.currency cu
ON cu.currency_id = d.currency_id
JOIN report.trans_state ts
ON ts.state_id = lts.settle_state_id
LEFT outer JOIN (SELECT DISTINCT TERMINAL_ID, FIRST_VALUE(eport_serial_num) OVER (PARTITION BY TERMINAL_ID ORDER BY start_date DESC) Eport_num
from report.terminal_eport te, report.eport e where te.eport_id=e.eport_id) ee
ON bat.terminal_id=ee.terminal_id
LEFT OUTER JOIN report.terminal t ON bat.terminal_id = t.terminal_id
GROUP BY
lts.entry_type,
bu.business_unit_name,
DECODE(bat.payment_schedule_id, 4, ''Y'', ''N''),
cu.currency_code,
c.customer_name,
lts.fee_name,
DECODE(d.status,
	''S'', ''Y'',
	''P'', ''Y'',
	''N''
),
TRUNC(lts.entry_date, ''MONTH''),
lts.description
,ee.Eport_num,
t.terminal_nbr,
t.sales_order_number'
WHERE REPORT_ID = (SELECT REPORT_ID FROM REPORT.REPORTS WHERE REPORT_NAME = 'All Other Fee Entries With Device') AND PARAM_NAME = 'query';

COMMIT;

INSERT INTO PSS.AUTHORITY_PAYMENT_MASK(AUTHORITY_PAYMENT_MASK_NAME, AUTHORITY_PAYMENT_MASK_DESC, AUTHORITY_PAYMENT_MASK_REGEX, AUTHORITY_PAYMENT_MASK_BREF, CARD_NAME)
SELECT 'USAT ISO Card (Format Three, Manual Entry)', 'USAT Payroll Deduct Card Manual Entry', '^(6396213[0-9]{12})\|([0-9]{4})\|(|[0-9]{3,4})\|([^|]*)\|([^|]*)\|([^|]*)$', '1:1|2:3|3:5|4:2|5:18', 'Payroll Deduct'
FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM PSS.AUTHORITY_PAYMENT_MASK WHERE AUTHORITY_PAYMENT_MASK_NAME = 'USAT ISO Card (Format Three, Manual Entry)');

INSERT INTO REPORT.TRANS_TYPE(TRANS_TYPE_ID, TRANS_TYPE_NAME,PROCESS_FEE_IND,DISPLAY_ORDER,FEE_PERCENT_UBN,
FEE_AMOUNT_UBN,FEE_MIN_UBN,FEE_PERCENT_LBN,PAYABLE_IND,CUSTOMER_DEBIT_IND,
FILL_BATCH_IND,CASH_IND,TRANS_TYPE_CODE,TRANS_ITEM_TYPE,REFUND_IND,
REFUNDABLE_IND,CHARGEBACK_ENABLED_IND,CREDIT_IND,OPERATOR_TRANS_TYPE_ID,CASHLESS_DEVICE_TRAN_IND,
PREPAID_IND,PROCESS_FEE_TRANS_TYPE_ID,PASS_IND,ACCESS_IND,DEBIT_IND, ENTRY_TYPE, FREE_IND)
SELECT 70, 'Payroll Deduct Promo','N',207,10,
1,1,1,'N','N',
'Y','N','R','PAYROLL DEDUCT PROMO','N'
,'Y','N','Y',70,'Y',
'N',70,'N','N','N','CC','Y' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM REPORT.TRANS_TYPE WHERE TRANS_TYPE_ID = 70);

INSERT INTO REPORT.TRANS_TYPE(TRANS_TYPE_ID, TRANS_TYPE_NAME,PROCESS_FEE_IND,DISPLAY_ORDER,FEE_PERCENT_UBN,
FEE_AMOUNT_UBN,FEE_MIN_UBN,FEE_PERCENT_LBN,PAYABLE_IND,CUSTOMER_DEBIT_IND,
FILL_BATCH_IND,CASH_IND,TRANS_TYPE_CODE,TRANS_ITEM_TYPE,REFUND_IND,
REFUNDABLE_IND,CHARGEBACK_ENABLED_IND,CREDIT_IND,OPERATOR_TRANS_TYPE_ID,CASHLESS_DEVICE_TRAN_IND,
PREPAID_IND,PROCESS_FEE_TRANS_TYPE_ID,PASS_IND,ACCESS_IND,DEBIT_IND, ENTRY_TYPE, FREE_IND)
SELECT 71, 'Payroll Deduct Promo (Contactless)','N',207,10,
1,1,1,'N','N',
'Y','N','R','PAYROLL DEDUCT PROMO (MANUAL ENTRY)','N'
,'Y','N','Y',71,'Y',
'N',71,'N','N','N','CC','Y' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM REPORT.TRANS_TYPE WHERE TRANS_TYPE_ID = 71);

INSERT INTO REPORT.TRANS_TYPE(TRANS_TYPE_ID, TRANS_TYPE_NAME,PROCESS_FEE_IND,DISPLAY_ORDER,FEE_PERCENT_UBN,
FEE_AMOUNT_UBN,FEE_MIN_UBN,FEE_PERCENT_LBN,PAYABLE_IND,CUSTOMER_DEBIT_IND,
FILL_BATCH_IND,CASH_IND,TRANS_TYPE_CODE,TRANS_ITEM_TYPE,REFUND_IND,
REFUNDABLE_IND,CHARGEBACK_ENABLED_IND,CREDIT_IND,OPERATOR_TRANS_TYPE_ID,CASHLESS_DEVICE_TRAN_IND,
PREPAID_IND,PROCESS_FEE_TRANS_TYPE_ID,PASS_IND,ACCESS_IND,DEBIT_IND, ENTRY_TYPE, FREE_IND)
SELECT 72, 'Payroll Deduct Promo (Manual Entry)','N',207,10,
1,1,1,'N','N',
'Y','N','R','PAYROLL DEDUCT PROMO (MANUAL ENTRY)','N'
,'Y','N','Y',72,'Y',
'N',72,'N','N','N','CC','Y' FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM REPORT.TRANS_TYPE WHERE TRANS_TYPE_ID = 72);

UPDATE REPORT.TRANS_TYPE SET TRANS_ITEM_TYPE = 'PREPAID PROMO (MANUAL ENTRY)' WHERE TRANS_TYPE_ID = 59;

INSERT INTO PSS.PAYMENT_SUBTYPE(PAYMENT_SUBTYPE_NAME, PAYMENT_SUBTYPE_CLASS, PAYMENT_SUBTYPE_KEY_NAME, CLIENT_PAYMENT_TYPE_CD, PAYMENT_SUBTYPE_TABLE_NAME, PAYMENT_SUBTYPE_KEY_DESC_NAME, AUTHORITY_PAYMENT_MASK_ID,TRANS_TYPE_ID,CARD_TYPE_LABEL)
SELECT 'Promo Payroll Deduct', 'Promotion', 'TERMINAL_ID', 'S', 'TERMINAL', 'TERMINAL_DESC', AUTHORITY_PAYMENT_MASK_ID, 70,'Promotion'
FROM PSS.AUTHORITY_PAYMENT_MASK WHERE AUTHORITY_PAYMENT_MASK_NAME = 'USAT ISO Card (Format Three)' AND NOT EXISTS (
SELECT 1 FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'Promo Payroll Deduct');

INSERT INTO PSS.PAYMENT_SUBTYPE(PAYMENT_SUBTYPE_NAME, PAYMENT_SUBTYPE_CLASS, PAYMENT_SUBTYPE_KEY_NAME, CLIENT_PAYMENT_TYPE_CD, PAYMENT_SUBTYPE_TABLE_NAME, PAYMENT_SUBTYPE_KEY_DESC_NAME, AUTHORITY_PAYMENT_MASK_ID,TRANS_TYPE_ID,CARD_TYPE_LABEL)
SELECT 'Promo Payroll Deduct (Contactless)', 'Promotion', 'TERMINAL_ID', 'P', 'TERMINAL', 'TERMINAL_DESC', AUTHORITY_PAYMENT_MASK_ID, 71,'Promotion'
FROM PSS.AUTHORITY_PAYMENT_MASK WHERE AUTHORITY_PAYMENT_MASK_NAME = 'USAT ISO Card (Format Three)' AND NOT EXISTS (
SELECT 1 FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'Promo Payroll Deduct (Contactless)');
commit;

INSERT INTO PSS.PAYMENT_SUBTYPE(PAYMENT_SUBTYPE_NAME, PAYMENT_SUBTYPE_CLASS, PAYMENT_SUBTYPE_KEY_NAME, CLIENT_PAYMENT_TYPE_CD, PAYMENT_SUBTYPE_TABLE_NAME, PAYMENT_SUBTYPE_KEY_DESC_NAME, AUTHORITY_PAYMENT_MASK_ID,TRANS_TYPE_ID,CARD_TYPE_LABEL)
SELECT 'Promo Payroll Deduct (Manual Entry)', 'Promotion', 'TERMINAL_ID', 'T', 'TERMINAL', 'TERMINAL_DESC', AUTHORITY_PAYMENT_MASK_ID, 72,'Promotion'
FROM PSS.AUTHORITY_PAYMENT_MASK WHERE AUTHORITY_PAYMENT_MASK_NAME = 'USAT ISO Card (Format Three, Manual Entry)' AND NOT EXISTS (
SELECT 1 FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'Promo Payroll Deduct (Manual Entry)');

COMMIT;

INSERT INTO PSS.PAYMENT_SUBTYPE(PAYMENT_SUBTYPE_NAME, PAYMENT_SUBTYPE_CLASS, PAYMENT_SUBTYPE_KEY_NAME, CLIENT_PAYMENT_TYPE_CD, PAYMENT_SUBTYPE_TABLE_NAME, PAYMENT_SUBTYPE_KEY_DESC_NAME, AUTHORITY_PAYMENT_MASK_ID, TRANS_TYPE_ID, CARD_TYPE_LABEL)
SELECT 'Error Bin', 'Authority::NOP', 'TERMINAL_ID', 'O', 'TERMINAL', 'TERMINAL_DESC', 
(SELECT MIN(AUTHORITY_PAYMENT_MASK_ID) FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_CLASS = 'Authority::NOP' AND PAYMENT_SUBTYPE_NAME LIKE '%Error Bin'), 18, 'Unknown'
FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'Error Bin' AND CLIENT_PAYMENT_TYPE_CD = 'O');

COMMIT;

GRANT DELETE ON REPORT.CONSUMER_ACCT_STAT TO USAT_PREPAID_APP_ROLE;
COMMIT;

CREATE TABLE PSS.CONSUMER_PROMOCODE (
    CONSUMER_ID NUMBER NOT NULL,
	CAMPAIGN_ID NUMBER NOT NULL,
    CREATED_BY VARCHAR2(30) NOT NULL,
    CREATED_UTC_TS TIMESTAMP NOT NULL,
    LAST_UPDATED_BY VARCHAR2(30) NOT NULL,
    LAST_UPDATED_UTC_TS TIMESTAMP NOT NULL,
    CONSTRAINT PK_CONSUMER_PROMOCODE PRIMARY KEY(CONSUMER_ID, CAMPAIGN_ID),
	CONSTRAINT FK_CPC_CAMPAIGN_ID FOREIGN KEY (CAMPAIGN_ID) REFERENCES REPORT.CAMPAIGN(CAMPAIGN_ID),
    CONSTRAINT FK_CPC_CONSUMER_ID FOREIGN KEY (CONSUMER_ID) REFERENCES PSS.CONSUMER(CONSUMER_ID)    
) TABLESPACE PSS_DATA;

CREATE OR REPLACE TRIGGER PSS.TRBI_CONSUMER_PROMOCODE BEFORE INSERT ON PSS.CONSUMER_PROMOCODE
  FOR EACH ROW
BEGIN
    SELECT 
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER,
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER
    INTO 
        :NEW.CREATED_UTC_TS,
        :NEW.CREATED_BY,
        :NEW.LAST_UPDATED_UTC_TS,
        :NEW.LAST_UPDATED_BY
    FROM DUAL;
END; 
/

CREATE OR REPLACE TRIGGER PSS.TRBU_CONSUMER_PROMOCODE BEFORE UPDATE ON PSS.CONSUMER_PROMOCODE
  FOR EACH ROW 
BEGIN
    SELECT
        :OLD.CREATED_BY,
        :OLD.CREATED_UTC_TS,
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER
    INTO
        :NEW.CREATED_BY,
        :NEW.CREATED_UTC_TS,
        :NEW.LAST_UPDATED_UTC_TS,
        :NEW.LAST_UPDATED_BY
    FROM DUAL;
END; 
/
 
CREATE INDEX PSS.IX_CPC_CAMPAIGN_ID ON PSS.CONSUMER_PROMOCODE(CAMPAIGN_ID) ONLINE TABLESPACE PSS_INDX;

GRANT SELECT,INSERT,UPDATE ON PSS.CONSUMER_PROMOCODE TO USAT_PREPAID_APP_ROLE;
GRANT SELECT ON PSS.CONSUMER_PROMOCODE TO USAT_DEV_READ_ONLY;
COMMIT;

INSERT INTO PSS.CONSUMER_PROMOCODE(CONSUMER_ID, CAMPAIGN_ID)
        SELECT C.CONSUMER_ID, CAM.CAMPAIGN_ID 
            from PSS.CONSUMER C
            JOIN PSS.CONSUMER_ACCT CA ON CA.CONSUMER_ID = C.CONSUMER_ID 
            JOIN PSS.CAMPAIGN_CONSUMER_ACCT CCA ON CCA.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
            JOIN REPORT.CAMPAIGN CAM ON CAM.CAMPAIGN_ID = CCA.CAMPAIGN_ID
                WHERE C.CONSUMER_TYPE_ID IN (5,7,8,9)  
                  AND CAM.STATUS = 'A'
                  AND (CAM.END_DATE is NULL OR CAM.END_DATE > SYSDATE)
                  AND CAM.PROMO_CODE is not NULL
        GROUP BY C.CONSUMER_ID, CAM.CAMPAIGN_ID; 
COMMIT;

ALTER TABLE PSS.PAYMENT_SUBTYPE ADD CAMPAIGN_ENABLED_IND VARCHAR(1) DEFAULT 'N' NOT NULL;
UPDATE PSS.PAYMENT_SUBTYPE SET CAMPAIGN_ENABLED_IND = 'Y' WHERE PAYMENT_SUBTYPE_CLASS = 'Internal::Prepaid' OR PAYMENT_SUBTYPE_NAME = 'Payroll Deduct (Track 2)';
COMMIT;

UPDATE CORP.CURRENCY SET CURRENCY_SYMBOL = '$' WHERE CURRENCY_CODE = 'EUR';
COMMIT;

UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
SET DESCRIPTION = 'Schedule for capturing and sending DEX files to the server. Time is in military format and device local time zone. Valid time values: 0000 - 2359. Valid interval minutes: '
	|| (SELECT CASE REPLACE(GLOBAL_NAME, '.WORLD', '') WHEN 'USAPDB' THEN '180' ELSE '5' END FROM GLOBAL_NAME) || ' - 1439.'
WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1101';
COMMIT;

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/releases/REL_Edge_Update_R55/R55.USAPDB.USAT364.1.sql?revision=1.3
INSERT INTO DEVICE.CONFIG_TEMPLATE_CATEGORY(CONFIG_TEMPLATE_CATEGORY_ID, CONFIG_TEMPLATE_CATEGORY_NAME, CONFIG_TEMPLATE_CATEGORY_DESC)
SELECT 45,
       'Customized Auth Flooding Protection',
       'Settings to change default behavior for AFP parameters'
FROM DUAL
WHERE NOT EXISTS
    (SELECT 1
     FROM DEVICE.CONFIG_TEMPLATE_CATEGORY
     WHERE CONFIG_TEMPLATE_CATEGORY_ID = 45);


INSERT INTO device.config_template_category_disp(config_template_category_id, device_type_id, display, category_display_order)
SELECT 45,
       0,
       'N',
       75
FROM DUAL
WHERE NOT EXISTS
    (SELECT 1
     FROM DEVICE.CONFIG_TEMPLATE_CATEGORY_DISP
     WHERE config_template_category_id = 45
       AND device_type_id = 0);


INSERT INTO device.config_template_category_disp(config_template_category_id, device_type_id, display, category_display_order)
SELECT 45,
       6,
       'N',
       30
FROM DUAL
WHERE NOT EXISTS
    (SELECT 1
     FROM DEVICE.CONFIG_TEMPLATE_CATEGORY_DISP
     WHERE config_template_category_id = 45
       AND device_type_id = 6);


INSERT INTO device.config_template_category_disp(config_template_category_id, device_type_id, display, category_display_order)
SELECT 45,
       11,
       'N',
       110
FROM DUAL
WHERE NOT EXISTS
    (SELECT 1
     FROM DEVICE.CONFIG_TEMPLATE_CATEGORY_DISP
     WHERE config_template_category_id = 45
       AND device_type_id = 11);


INSERT INTO device.config_template_category_disp(config_template_category_id, device_type_id, display, category_display_order)
SELECT 45,
       13,
       'N',
       85
FROM DUAL
WHERE NOT EXISTS
    (SELECT 1
     FROM DEVICE.CONFIG_TEMPLATE_CATEGORY_DISP
     WHERE config_template_category_id = 45
       AND device_type_id = 13);


INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_UI_CONFIGURABLE, DEVICE_SETTING_PARAMETER_NAME, SETTING_PARAMETER_TYPE_ID)
SELECT 'CUSTOM_AFP_ENABLED',
       'N',
       'Override global Auth Flooding Protection settings',
       1
FROM DUAL
WHERE NOT EXISTS
    (SELECT 1
     FROM DEVICE.DEVICE_SETTING_PARAMETER
     WHERE DEVICE_SETTING_PARAMETER_CD = 'CUSTOM_AFP_ENABLED');


INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(DEVICE_SETTING_PARAMETER_CD, CATEGORY_ID, DISPLAY, DESCRIPTION, EDITOR, CLIENT_SEND, FIELD_SIZE, FIELD_OFFSET, DEVICE_TYPE_ID, DISPLAY_ORDER, REGEX_ID, ACTIVE, CONFIG_TEMPLATE_ID)
SELECT 'CUSTOM_AFP_ENABLED',
       45,
       'N',
       'Customized AFP: Override global Auth Flooding Protection settings',
       'SELECT:=N;Y=Y',
       'N',
       NULL,
       DECODE(CT.DEVICE_TYPE_ID, 0, 20300),
       CT.DEVICE_TYPE_ID,
       20300,
       19,
       'Y',
       CONFIG_TEMPLATE_ID
FROM DEVICE.CONFIG_TEMPLATE CT
WHERE CT.DEVICE_TYPE_ID IN(0,
                           6,
                           11,
                           13)
  AND CT.CONFIG_TEMPLATE_TYPE_ID = 3
  AND NOT EXISTS
    (SELECT 1
     FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS
     WHERE CTS.DEVICE_TYPE_ID = CT.DEVICE_TYPE_ID
       AND CTS.DEVICE_SETTING_PARAMETER_CD = 'CUSTOM_AFP_ENABLED' );


INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_UI_CONFIGURABLE, DEVICE_SETTING_PARAMETER_NAME, SETTING_PARAMETER_TYPE_ID)
SELECT 'CUSTOM_AFP_NUM_AUTHS',
       'N',
       'Number of Authorizations within (Check Time Minutes)',
       1
FROM DUAL
WHERE NOT EXISTS
    (SELECT 1
     FROM DEVICE.DEVICE_SETTING_PARAMETER
     WHERE DEVICE_SETTING_PARAMETER_CD = 'CUSTOM_AFP_NUM_AUTHS');


INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(DEVICE_SETTING_PARAMETER_CD, CONFIG_TEMPLATE_SETTING_VALUE, CATEGORY_ID, DISPLAY, DESCRIPTION, EDITOR, CLIENT_SEND, FIELD_SIZE, FIELD_OFFSET, DEVICE_TYPE_ID, DISPLAY_ORDER, REGEX_ID, ACTIVE, CONFIG_TEMPLATE_ID)
SELECT 'CUSTOM_AFP_NUM_AUTHS',
       '12',
       45,
       'N',
       'Customized AFP: Number of approved authorizations in <b>Check Time</b> after which the device will be blacklisted for <b>Blacklist Minutes</b>. If enabled, default value is <b>12</b>.',
       'TEXT:0 to 2',
       'N',
       NULL,
       DECODE(CT.DEVICE_TYPE_ID, 0, 20310),
       CT.DEVICE_TYPE_ID,
       20310,
       3,
       'Y',
       CONFIG_TEMPLATE_ID
FROM DEVICE.CONFIG_TEMPLATE CT
WHERE CT.DEVICE_TYPE_ID IN(0,
                           6,
                           11,
                           13)
  AND CT.CONFIG_TEMPLATE_TYPE_ID = 3
  AND NOT EXISTS
    (SELECT 1
     FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS
     WHERE CTS.DEVICE_TYPE_ID = CT.DEVICE_TYPE_ID
       AND CTS.DEVICE_SETTING_PARAMETER_CD = 'CUSTOM_AFP_NUM_AUTHS' );


INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_UI_CONFIGURABLE, DEVICE_SETTING_PARAMETER_NAME, SETTING_PARAMETER_TYPE_ID)
SELECT 'CUSTOM_AFP_CHECK_MINUTES',
       'N',
       'Check Time (minutes)',
       1
FROM DUAL
WHERE NOT EXISTS
    (SELECT 1
     FROM DEVICE.DEVICE_SETTING_PARAMETER
     WHERE DEVICE_SETTING_PARAMETER_CD = 'CUSTOM_AFP_CHECK_MINUTES');


INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(DEVICE_SETTING_PARAMETER_CD, CONFIG_TEMPLATE_SETTING_VALUE, CATEGORY_ID, DISPLAY, DESCRIPTION, EDITOR, CLIENT_SEND, FIELD_SIZE, FIELD_OFFSET, DEVICE_TYPE_ID, DISPLAY_ORDER, REGEX_ID, ACTIVE, CONFIG_TEMPLATE_ID)
SELECT 'CUSTOM_AFP_CHECK_MINUTES',
       '2',
       45,
       'N',
       'Customized AFP: Number of minutes to check. The device will be blacklisted for <b>Blacklist Minutes</b> after <b>Number of Authorizations</b> for the same card in this time span. If enabled, default value is <b>2</b>.',
       'TEXT:0 to 2',
       'N',
       NULL,
       DECODE(CT.DEVICE_TYPE_ID, 0, 20320),
       CT.DEVICE_TYPE_ID,
       20320,
       3,
       'Y',
       CONFIG_TEMPLATE_ID
FROM DEVICE.CONFIG_TEMPLATE CT
WHERE CT.DEVICE_TYPE_ID IN(0,
                           6,
                           11,
                           13)
  AND CT.CONFIG_TEMPLATE_TYPE_ID = 3
  AND NOT EXISTS
    (SELECT 1
     FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS
     WHERE CTS.DEVICE_TYPE_ID = CT.DEVICE_TYPE_ID
       AND CTS.DEVICE_SETTING_PARAMETER_CD = 'CUSTOM_AFP_CHECK_MINUTES' );


INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_UI_CONFIGURABLE, DEVICE_SETTING_PARAMETER_NAME, SETTING_PARAMETER_TYPE_ID)
SELECT 'CUSTOM_AFP_BLACKLIST_MINUTES',
       'N',
       'Blacklist Minutes',
       1
FROM DUAL
WHERE NOT EXISTS
    (SELECT 1
     FROM DEVICE.DEVICE_SETTING_PARAMETER
     WHERE DEVICE_SETTING_PARAMETER_CD = 'CUSTOM_AFP_BLACKLIST_MINUTES');


INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(DEVICE_SETTING_PARAMETER_CD, CONFIG_TEMPLATE_SETTING_VALUE, CATEGORY_ID, DISPLAY, DESCRIPTION, EDITOR, CLIENT_SEND, FIELD_SIZE, FIELD_OFFSET, DEVICE_TYPE_ID, DISPLAY_ORDER, REGEX_ID, ACTIVE, CONFIG_TEMPLATE_ID)
SELECT 'CUSTOM_AFP_BLACKLIST_MINUTES',
       '5',
       45,
       'N',
       'Customized AFP: The device will be blacklisted for this many minutes after <b>Number of Authorizations</b> for the same card in <b>Check Time</b>. If enabled, default value is <b>5</b>.',
       'TEXT:0 to 2',
       'N',
       NULL,
       DECODE(CT.DEVICE_TYPE_ID, 0, 20330),
       CT.DEVICE_TYPE_ID,
       20330,
       3,
       'Y',
       CONFIG_TEMPLATE_ID
FROM DEVICE.CONFIG_TEMPLATE CT
WHERE CT.DEVICE_TYPE_ID IN(0,
                           6,
                           11,
                           13)
  AND CT.CONFIG_TEMPLATE_TYPE_ID = 3
  AND NOT EXISTS
    (SELECT 1
     FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS
     WHERE CTS.DEVICE_TYPE_ID = CT.DEVICE_TYPE_ID
       AND CTS.DEVICE_SETTING_PARAMETER_CD = 'CUSTOM_AFP_BLACKLIST_MINUTES' );


INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_UI_CONFIGURABLE, DEVICE_SETTING_PARAMETER_NAME, SETTING_PARAMETER_TYPE_ID)
SELECT 'CUSTOM_AFP_DECLINED_PERC_MIN',
       'N',
       'Minimum failed auth rate (percents)',
       1
FROM DUAL
WHERE NOT EXISTS
    (SELECT 1
     FROM DEVICE.DEVICE_SETTING_PARAMETER
     WHERE DEVICE_SETTING_PARAMETER_CD = 'CUSTOM_AFP_DECLINED_PERC_MIN');


INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(DEVICE_SETTING_PARAMETER_CD, CONFIG_TEMPLATE_SETTING_VALUE, CATEGORY_ID, DISPLAY, DESCRIPTION, EDITOR, CLIENT_SEND, FIELD_SIZE, FIELD_OFFSET, DEVICE_TYPE_ID, DISPLAY_ORDER, REGEX_ID, ACTIVE, CONFIG_TEMPLATE_ID)
SELECT 'CUSTOM_AFP_DECLINED_PERC_MIN',
       '65',
       45,
       'N',
       'Customized AFP: Minimum (Failed auths) / (Success auths) rate to blacklist device. If enabled, default value is <b>65</b>.',
       'TEXT:0 to 2',
       'N',
       NULL,
       DECODE(CT.DEVICE_TYPE_ID, 0, 20340),
       CT.DEVICE_TYPE_ID,
       20340,
       3,
       'Y',
       CONFIG_TEMPLATE_ID
FROM DEVICE.CONFIG_TEMPLATE CT
WHERE CT.DEVICE_TYPE_ID IN(0,
                           6,
                           11,
                           13)
  AND CT.CONFIG_TEMPLATE_TYPE_ID = 3
  AND NOT EXISTS
    (SELECT 1
     FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS
     WHERE CTS.DEVICE_TYPE_ID = CT.DEVICE_TYPE_ID
       AND CTS.DEVICE_SETTING_PARAMETER_CD = 'CUSTOM_AFP_DECLINED_PERC_MIN' );


UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
SET CUSTOMER_EDITABLE = 'Y'
WHERE DEVICE_SETTING_PARAMETER_CD LIKE 'CUSTOM_AFP_%'
  AND DEVICE_TYPE_ID IS NOT NULL;


COMMIT;

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/PSS/DEVICE_AUTH_FLOOD_PROTECT.prc?revision=1.8
create or replace PROCEDURE PSS.DEVICE_AUTH_FLOOD_PROTECT
IS
  ld_sysdate DATE := SYSDATE;
  ld_decline_until_ts DATE;
  lv_from VARCHAR2(128) := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EMAIL_FROM_ADDRESS');
  lv_to VARCHAR2(128) := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EMAIL_TO_ADDRESS_CUSTOMER_SERVICE');
  lv_dms_url VARCHAR2(512) := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DMS_URL');
  CURSOR lc_auth_flood_devices IS
  with
    global_params as (
    select DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DEVICE_AUTH_FLOOD_COUNT') DEVICE_AUTH_FLOOD_COUNT,
    DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DEVICE_AUTH_FLOOD_MINUTES') DEVICE_AUTH_FLOOD_MINUTES,
    DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DEVICE_AUTH_FLOOD_DECLINE_MINUTES') DEVICE_AUTH_DECLINE_MINUTES,
    DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DEVICE_AUTH_FLOOD_DECLINED_PERCENT_MIN') DEVICE_AUTH_DECLINE_PERC_MIN
    from dual
    ),
    custom_afp_devices as (
    select D.device_id, d.device_name, NVL(DS.DEVICE_SETTING_VALUE, 'N') CUSTOM_AFP_ENABLED
    from DEVICE.DEVICE D
    LEFT JOIN DEVICE.DEVICE_SETTING DS ON D.DEVICE_ID = DS.DEVICE_ID
    AND DS.DEVICE_SETTING_PARAMETER_CD = 'CUSTOM_AFP_ENABLED'),
    CUSTOM_AFP_NUM_AUTHS as (
    select device_id, DEVICE_SETTING_VALUE CUSTOM_AFP_NUM_AUTHS
    from DEVICE.DEVICE_SETTING
    where DEVICE_SETTING_PARAMETER_CD = 'CUSTOM_AFP_NUM_AUTHS'
    and device_id in (select device_id from custom_afp_devices WHERE CUSTOM_AFP_ENABLED = 'Y')),
    CUSTOM_AFP_CHECK_MINUTES as (
    select device_id, DEVICE_SETTING_VALUE CUSTOM_AFP_CHECK_MINUTES
    from DEVICE.DEVICE_SETTING
    where DEVICE_SETTING_PARAMETER_CD = 'CUSTOM_AFP_CHECK_MINUTES'
    and device_id in (select device_id from custom_afp_devices WHERE CUSTOM_AFP_ENABLED = 'Y')),
    CUSTOM_AFP_BLACKLIST_MINUTES as (
    select device_id, DEVICE_SETTING_VALUE CUSTOM_AFP_BLACKLIST_MINUTES
    from DEVICE.DEVICE_SETTING
    where DEVICE_SETTING_PARAMETER_CD = 'CUSTOM_AFP_BLACKLIST_MINUTES'
    and device_id in (select device_id from custom_afp_devices WHERE CUSTOM_AFP_ENABLED = 'Y')),
    CUSTOM_AFP_DECLINED_PERC_MIN as (
    select device_id, DEVICE_SETTING_VALUE CUSTOM_AFP_DECLINED_PERC_MIN
    from DEVICE.DEVICE_SETTING
    where DEVICE_SETTING_PARAMETER_CD = 'CUSTOM_AFP_DECLINED_PERC_MIN'
    and device_id in (select device_id from custom_afp_devices WHERE CUSTOM_AFP_ENABLED = 'Y')),
	afp_params as (
    select cad.device_id, cad.device_name,
    TO_NUMBER(NVL(CUSTOM_AFP_NUM_AUTHS, gp.DEVICE_AUTH_FLOOD_COUNT)) CUSTOM_AFP_NUM_AUTHS,
    TO_NUMBER(NVL(CUSTOM_AFP_CHECK_MINUTES, gp.DEVICE_AUTH_FLOOD_MINUTES)) CUSTOM_AFP_CHECK_MINUTES,
    TO_NUMBER(NVL(CUSTOM_AFP_BLACKLIST_MINUTES, gp.DEVICE_AUTH_DECLINE_MINUTES)) CUSTOM_AFP_BLACKLIST_MINUTES,
    TO_NUMBER(NVL(CUSTOM_AFP_DECLINED_PERC_MIN / 100, gp.DEVICE_AUTH_DECLINE_PERC_MIN)) CUSTOM_AFP_DECLINED_PERC_MIN
    from custom_afp_devices cad
    cross join global_params gp
    left join CUSTOM_AFP_NUM_AUTHS nums on cad.device_id = nums.device_id
    left join CUSTOM_AFP_CHECK_MINUTES chk on cad.device_id = chk.device_id
    left join CUSTOM_AFP_BLACKLIST_MINUTES bl on cad.device_id = bl.device_id
    left join CUSTOM_AFP_DECLINED_PERC_MIN dcl on cad.device_id = dcl.device_id)
    select asr.device_name, asr.payment_subtype_class, sum(asr.auth_count - asr.auth_failed_count) auth_total,
	afp.CUSTOM_AFP_NUM_AUTHS, afp.CUSTOM_AFP_CHECK_MINUTES, afp.CUSTOM_AFP_BLACKLIST_MINUTES, afp.CUSTOM_AFP_DECLINED_PERC_MIN
    from pss.auth_stat_recent asr
	join afp_params afp on asr.device_name = afp.device_name
    where asr.interval_ts > (ld_sysdate - afp.CUSTOM_AFP_CHECK_MINUTES/1440)
    and asr.payment_subtype_class in ('Tandem', 'Authority::ISO8583::Elavon', 'Authority::ISO8583::FHMS::Paymentech')
    group by asr.device_name, asr.payment_subtype_class, afp.CUSTOM_AFP_NUM_AUTHS, afp.CUSTOM_AFP_CHECK_MINUTES, afp.CUSTOM_AFP_BLACKLIST_MINUTES, afp.CUSTOM_AFP_DECLINED_PERC_MIN
    having sum(asr.auth_count - asr.auth_failed_count) >= afp.CUSTOM_AFP_NUM_AUTHS
    and sum(asr.auth_declined_count)/sum(asr.auth_count - asr.auth_failed_count) >= afp.CUSTOM_AFP_DECLINED_PERC_MIN;
  lt_pos_pta_ids NUMBER_TABLE;
  ln_device_id NUMBER;
  lv_device_serial_cd VARCHAR2(128);
  lv_location_name VARCHAR2(128);
  lv_customer_name VARCHAR2(128);
  lv_subject VARCHAR(256);
  lv_msg VARCHAR(4000);
BEGIN
  FOR lr_auth_flood_devices IN lc_auth_flood_devices LOOP
	IF lr_auth_flood_devices.CUSTOM_AFP_BLACKLIST_MINUTES <= 0 THEN
      CONTINUE;
    END IF;
    DBMS_OUTPUT.PUT_LINE('Auth flood detected: device_name=' || lr_auth_flood_devices.device_name || ', payment_subtype_class=' || lr_auth_flood_devices.payment_subtype_class || ', auth_total=' || lr_auth_flood_devices.auth_total);
    SELECT pp.pos_pta_id
    BULK COLLECT INTO lt_pos_pta_ids
    FROM device.device d
    JOIN device.device_last_active dla ON d.device_name = dla.device_name
    JOIN pss.pos p ON dla.device_id = p.device_id
    JOIN pss.pos_pta pp ON p.pos_id = pp.pos_id
    JOIN pss.payment_subtype ps ON ps.payment_subtype_id = pp.payment_subtype_id
    WHERE d.device_name = lr_auth_flood_devices.device_name
    AND ps.payment_subtype_class = lr_auth_flood_devices.payment_subtype_class
    AND SYSDATE between NVL(pp.pos_pta_activation_ts, MIN_DATE) and NVL(pp.pos_pta_deactivation_ts, MAX_DATE)
    AND NVL(pp.whitelist, 'N') != 'Y'
    AND nvl(pp.decline_until, MIN_DATE) < (ld_sysdate - (lr_auth_flood_devices.CUSTOM_AFP_CHECK_MINUTES+1)/1440); -- don't blacklist where it's already blacklisted or has been blacklisted recently

    DBMS_OUTPUT.PUT_LINE('Blacklisting: ' || lt_pos_pta_ids.COUNT || ' pos_ptas for ' || lr_auth_flood_devices.CUSTOM_AFP_BLACKLIST_MINUTES || ' minutes until ' || TO_CHAR(ld_decline_until_ts, 'MM/DD/YYYY HH24:MI:SS'));

    IF lt_pos_pta_ids.COUNT > 0 THEN
	  ld_decline_until_ts := ld_sysdate + lr_auth_flood_devices.CUSTOM_AFP_BLACKLIST_MINUTES/1440;
      FOR i IN lt_pos_pta_ids.FIRST .. lt_pos_pta_ids.LAST LOOP
        UPDATE pss.pos_pta
        SET decline_until = ld_decline_until_ts
        WHERE pos_pta_id = lt_pos_pta_ids(i);
      END LOOP;

      SELECT d.device_id, NVL(e.eport_serial_num, 'Unknown'), NVL(l.location_name, 'Unknown'), NVL(c.customer_name, 'Unknown')
      INTO ln_device_id, lv_device_serial_cd, lv_location_name, lv_customer_name
      FROM device.device d
      LEFT OUTER JOIN report.eport e ON e.eport_serial_num = d.device_serial_cd
      LEFT OUTER JOIN report.vw_terminal_eport te on te.eport_id = e.eport_id
      LEFT OUTER JOIN report.terminal t on t.terminal_id = te.terminal_id
      LEFT OUTER JOIN report.location l on l.location_id = t.location_id
      LEFT OUTER JOIN corp.customer c on c.customer_id = t.customer_id
      WHERE d.device_active_yn_flag = 'Y'
      AND e.status = 'A'
      AND d.device_name = lr_auth_flood_devices.device_name;

      DBMS_OUTPUT.PUT_LINE('Sending notification: to=' || lv_to || ', serial=' || lv_device_serial_cd || ', customer_name=' || lv_customer_name || ', location_name=' || lv_location_name);

      SELECT 'Device ' || lr_auth_flood_devices.device_name || ' blacklisted for ' || lr_auth_flood_devices.CUSTOM_AFP_BLACKLIST_MINUTES || ' minutes until ' || TO_CHAR(ld_decline_until_ts, 'MM/DD/YYYY HH24:MI:SS'),
        'Serial    : ' || lv_device_serial_cd || CHR(10) ||
        'Processor : ' || lr_auth_flood_devices.payment_subtype_class || CHR(10) ||
        'Customer  : ' || lv_customer_name || CHR(10) ||
        'Location  : ' || lv_location_name || CHR(10) ||
        'Timestamp : ' || TO_CHAR(ld_sysdate, 'MM/DD/YYYY HH24:MI:SS') || CHR(10) ||
        'Reason    : ' || lr_auth_flood_devices.auth_total || ' authorizations in the last ' || lr_auth_flood_devices.CUSTOM_AFP_CHECK_MINUTES || ' minutes exceeds the maximum of ' || lr_auth_flood_devices.CUSTOM_AFP_NUM_AUTHS || CHR(10) ||
        CHR(10) ||
        lv_dms_url || 'paymentConfig.i?device_id=' || ln_device_id || chr(38) || 'userOP=payment_types' || CHR(10)
      INTO lv_subject, lv_msg
      FROM DUAL;

      INSERT INTO ENGINE.OB_EMAIL_QUEUE(OB_EMAIL_FROM_EMAIL_ADDR, OB_EMAIL_FROM_NAME, OB_EMAIL_MSG, OB_EMAIL_SUBJECT, OB_EMAIL_TO_EMAIL_ADDR, OB_EMAIL_TO_NAME, OB_EMAIL_CONTENT)
      SELECT lv_from, lv_from, ' ', lv_subject, lv_to, lv_to, lv_msg
      FROM DUAL;

    END IF;

  END LOOP;

END;
/

GRANT EXECUTE ON PSS.DEVICE_AUTH_FLOOD_PROTECT TO USAT_POSM_ROLE;

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/releases/REL_Edge_Update_R55/R55.USAPDB.USAT115.1.sql?revision=1.1
ALTER TABLE DEVICE.DEVICE_MODEM_STATE
DROP CONSTRAINT DEVICE_MODEM_STATE_FK1;

ALTER TABLE DEVICE.DEVICE_MODEM_STATE
ADD (PROVIDER VARCHAR2(64) );

COMMENT ON COLUMN DEVICE.DEVICE_MODEM_STATE.PROVIDER IS 'Modem services provider name: EsEye, Verizon, etc';


CREATE INDEX DEVICE.DEVICE_MODEM_STATE_PROVIDER ON DEVICE.DEVICE_MODEM_STATE (PROVIDER ASC);

CREATE INDEX DEVICE.DEVICE_MODEM_STATE_ST_UP_TS ON DEVICE.DEVICE_MODEM_STATE (STATUS_UPDATED_TS DESC);


-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/CCS_PKG.psk?revision=1.17
CREATE OR REPLACE PACKAGE REPORT.CCS_PKG IS
--
-- Contains functions /procedures for managing user reports
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------       
-- B KRUG       4-4-05  NEW
-- B KRUG       3-8-07  Renamed package and made TRANSPORT_PROP_UPD generic
  PROCEDURE ALERT_NOTIF_INS(
    
        l_alert_notif_id  OUT REPORT.ALERT_NOTIF.alert_notif_id%TYPE,
        l_ccs_transport_id  OUT CCS_TRANSPORT.ccs_transport_id%TYPE, 
        l_user_id  IN ALERT_NOTIF.user_id%TYPE,
        l_allTerminalFlag IN ALERT_NOTIF.all_terminals_flag%TYPE,
        l_terminalid  ID_LIST,
        l_alerTypeId  ID_LIST);
 
  PROCEDURE ALERT_NOTIF_DEL(
        l_alert_notif_id IN ALERT_NOTIF.ALERT_NOTIF_ID%TYPE);
        
  PROCEDURE ALERT_NOTIF_UPD(
        l_alert_notif_id IN ALERT_NOTIF.ALERT_NOTIF_ID%TYPE,
        l_user_id  IN ALERT_NOTIF.user_id%TYPE,
        l_allTerminalFlag IN ALERT_NOTIF.all_terminals_flag%TYPE,
        l_terminal_id ID_LIST,
        l_alert_type_id ID_LIST,
        l_transport_id OUT CCS_TRANSPORT.CCS_TRANSPORT_ID%TYPE);
        
  PROCEDURE USER_REPORT_DEL(
        l_user_report_id IN USER_REPORT.USER_REPORT_ID%TYPE);
        
  PROCEDURE TRANSPORT_PROP_UPD(
        l_transport_id IN CCS_TRANSPORT_PROPERTY.CCS_TRANSPORT_ID%TYPE,
        l_tpt_id IN CCS_TRANSPORT_PROPERTY.CCS_TRANSPORT_PROPERTY_TYPE_ID%TYPE,
        l_value IN CCS_TRANSPORT_PROPERTY.CCS_TRANSPORT_PROPERTY_VALUE%TYPE);
      
    PROCEDURE USER_REPORT_INS(
        pn_user_id IN REPORT.USER_REPORT.USER_ID%TYPE,
        pn_report_id IN REPORT.USER_REPORT.REPORT_ID%TYPE,
        pn_frequency_id IN REPORT.USER_REPORT.FREQUENCY_ID%TYPE,
        pn_transport_id IN REPORT.CCS_TRANSPORT.CCS_TRANSPORT_ID%TYPE,
        pv_emailErrorsTo IN REPORT.USER_REPORT.ERROR_EMAIL_LIST%TYPE,
        pn_user_report_id OUT REPORT.USER_REPORT.USER_REPORT_ID%TYPE);
  
  PROCEDURE USER_REPORT_INS(
        pn_user_id IN REPORT.USER_REPORT.USER_ID%TYPE,
        pn_report_id IN REPORT.USER_REPORT.REPORT_ID%TYPE,
        pn_frequency_id IN REPORT.USER_REPORT.FREQUENCY_ID%TYPE,
        pn_transport_id IN REPORT.CCS_TRANSPORT.CCS_TRANSPORT_ID%TYPE,
        pn_user_report_id OUT REPORT.USER_REPORT.USER_REPORT_ID%TYPE);
        
  PROCEDURE USER_REPORT_UPD(
        l_user_report_id IN USER_REPORT.USER_REPORT_ID%TYPE,
        l_frequency_id IN USER_REPORT.FREQUENCY_ID%TYPE,
        l_transport_id IN CCS_TRANSPORT.CCS_TRANSPORT_ID%TYPE,
        pv_emailErrorsTo IN USER_REPORT.ERROR_EMAIL_LIST%TYPE);
  
  PROCEDURE USER_REPORT_UPD(
        l_user_report_id IN USER_REPORT.USER_REPORT_ID%TYPE,
        l_frequency_id IN USER_REPORT.FREQUENCY_ID%TYPE,
        l_transport_id IN CCS_TRANSPORT.CCS_TRANSPORT_ID%TYPE);
 
  PROCEDURE REPORT_SENT_INS (
        l_user_report_id IN REPORT_SENT.USER_REPORT_ID%TYPE,
        l_report_request_id IN REPORT_SENT.REPORT_REQUEST_ID%TYPE,
        l_batch_id IN REPORT_SENT.BATCH_ID%TYPE,
        l_begin_date IN REPORT_SENT.BEGIN_DATE%TYPE,
        l_end_date IN REPORT_SENT.END_DATE%TYPE,
        l_rs_id OUT REPORT_SENT.REPORT_SENT_ID%TYPE,
        l_batch_type_id  IN REPORT_SENT.EXPORT_TYPE%TYPE  DEFAULT NULL);
  
  -- R45 and above
  PROCEDURE ADD_EVENT_ALERT(
        pn_event_id IN REPORT.TERMINAL_ALERT.REFERENCE_ID%TYPE,
        pn_terminal_alert_id OUT REPORT.TERMINAL_ALERT.TERMINAL_ALERT_ID%TYPE,
        pn_terminal_id OUT REPORT.TERMINAL.TERMINAL_ID%TYPE,
        pv_device_serial_cd OUT REPORT.EPORT.EPORT_SERIAL_NUM%TYPE,
        pc_dont_send_flag OUT VARCHAR2);
        
  PROCEDURE CCS_TRANSPORT_ERROR_INS(
        pn_report_sent_id IN REPORT_SENT.REPORT_SENT_ID%TYPE,
        pv_error_text IN CCS_TRANSPORT_ERROR.ERROR_TEXT%TYPE,
        pd_error_ts IN CCS_TRANSPORT_ERROR.ERROR_TS%TYPE,
        pd_error_type_id IN CCS_TRANSPORT_ERROR.ERROR_TYPE_ID%TYPE,
        pv_retry_props IN CCS_TRANSPORT_ERROR.RETRY_PROPS%TYPE,
        pn_error_id OUT CCS_TRANSPORT_ERROR.CCS_TRANSPORT_ERROR_ID%TYPE);
        
  PROCEDURE CCS_TRANSPORT_ERROR_RETRY(
        pn_ct_error_id IN CCS_TRANSPORT_ERROR.CCS_TRANSPORT_ERROR_ID%TYPE);
  
  PROCEDURE CCS_TRANSPORT_RETRY_SUCCESS(
        pn_ccs_transport_id IN CCS_TRANSPORT.CCS_TRANSPORT_ID%TYPE, 
        pn_ct_error_id IN CCS_TRANSPORT_ERROR.CCS_TRANSPORT_ERROR_ID%TYPE);
  
  PROCEDURE CCS_TRANSPORT_ERROR_UPDATE(
        pn_ccs_transport_id IN CCS_TRANSPORT.CCS_TRANSPORT_ID%TYPE, 
        pn_ct_error_id IN CCS_TRANSPORT_ERROR.CCS_TRANSPORT_ERROR_ID%TYPE,
        pv_error_text IN CCS_TRANSPORT_ERROR.ERROR_TEXT%TYPE,
        pd_error_type_id IN CCS_TRANSPORT_ERROR.ERROR_TYPE_ID%TYPE,
        pv_retry_props IN CCS_TRANSPORT_ERROR.RETRY_PROPS%TYPE,
        pn_disabled_transport OUT NUMBER);
        
  PROCEDURE CCS_UPDATE_REATTEMPTED_FLAG(
        pn_ct_error_id IN CCS_TRANSPORT_ERROR.CCS_TRANSPORT_ERROR_ID%TYPE,
        pn_reattempted_flag IN CCS_TRANSPORT_ERROR.REATTEMPTED_FLAG%TYPE);
        
END; -- Package spec
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/CCS_PKG.pbk?revision=1.30
CREATE OR REPLACE PACKAGE BODY REPORT.CCS_PKG IS
   
    PROCEDURE ALERT_NOTIF_INS(
       l_alert_notif_id  OUT REPORT.ALERT_NOTIF.alert_notif_id%TYPE,
        l_ccs_transport_id  OUT CCS_TRANSPORT.ccs_transport_id%TYPE, 
        l_user_id  IN ALERT_NOTIF.user_id%TYPE,
        l_allTerminalFlag IN ALERT_NOTIF.all_terminals_flag%TYPE,      
        l_terminalId  ID_LIST,
        l_alerTypeId  ID_LIST)
    IS
    BEGIN
        SELECT CCS_TRANSPORT_SEQ.NEXTVAL,
               SEQ_ALERT_NOTIF_ID.NEXTVAL
          INTO l_ccs_transport_id,
               l_alert_notif_id
          FROM DUAL;
        
         INSERT INTO CCS_TRANSPORT(CCS_TRANSPORT_ID, CCS_TRANSPORT_TYPE_ID, CCS_TRANSPORT_NAME)
            VALUES(l_ccs_transport_id, 1,
                'Transport for Alert Notif Id = ' || TO_CHAR(l_alert_notif_id));
    	 INSERT INTO ALERT_NOTIF (ALERT_NOTIF_ID, USER_ID, ccs_transport_id,All_TERMINALS_FLAG)
  	 		VALUES(l_alert_notif_id, l_user_id, l_ccs_transport_id,l_allTerminalFlag);
        
        
         IF l_allTerminalFlag <> 'Y' THEN
          INSERT INTO ALERT_NOTIF_TERMINAL (
              ALERT_NOTIF_TERMINAL_ID,
              ALERT_NOTIF_ID,
              TERMINAL_ID)
            SELECT 
              SEQ_ALERT_NOTIF_TERMINAL_ID.nextval,
              l_alert_notif_id,
              TERMINAL_ID
            FROM REPORT.VW_USER_TERMINAL
          WHERE USER_ID = l_user_id
           AND TERMINAL_ID MEMBER OF l_terminalId; 
         END IF;
         
         INSERT INTO ALERT_NOTIF_ALERT_TYPE (
               ALERT_NOTIF_ALERT_TYPE_ID,
               ALERT_NOTIF_ID,
               ALERT_TYPE_ID)
            SELECT 
               SEQ_ALERT_NOTIF_ALERT_TYPE_ID.nextval,
               l_alert_notif_id,
               ALERT_TYPE_ID
            FROM REPORT.ALERT_TYPE
           WHERE ALERT_TYPE_ID MEMBER OF l_alerTypeId;
    END;
    
    

    PROCEDURE ALERT_NOTIF_DEL(
        l_alert_notif_id IN ALERT_NOTIF.ALERT_NOTIF_ID%TYPE)
    IS
    BEGIN
    	 DELETE FROM  ALERT_NOTIF_ALERT_TYPE WHERE ALERT_NOTIF_ID = l_alert_notif_id;
    	 DELETE FROM  ALERT_NOTIF_TERMINAL WHERE ALERT_NOTIF_ID = l_alert_notif_id;
    	 DELETE FROM  ALERT_NOTIF WHERE ALERT_NOTIF_ID = l_alert_notif_id;
    END;
    
   PROCEDURE ALERT_NOTIF_UPD(
        l_alert_notif_id IN ALERT_NOTIF.ALERT_NOTIF_ID%TYPE,
        l_user_id  IN ALERT_NOTIF.user_id%TYPE,
        l_allTerminalFlag IN ALERT_NOTIF.all_terminals_flag%TYPE,
        l_terminal_id ID_LIST,
        l_alert_type_id ID_LIST,
        l_transport_id OUT CCS_TRANSPORT.CCS_TRANSPORT_ID%TYPE)
    IS
    BEGIN
    	 SELECT CCS_TRANSPORT_ID
          INTO  l_transport_id
          FROM ALERT_NOTIF 
         WHERE ALERT_NOTIF_ID = l_alert_notif_id;
        
         UPDATE ALERT_NOTIF
          SET   CCS_TRANSPORT_ID=l_transport_id, ALL_TERMINALS_FLAG=l_allTerminalFlag
         WHERE  ALERT_NOTIF_ID=l_alert_notif_id;
         DELETE FROM  ALERT_NOTIF_TERMINAL WHERE ALERT_NOTIF_ID = l_alert_notif_id;       
         IF l_allTerminalFlag <> 'Y' THEN
          INSERT INTO ALERT_NOTIF_TERMINAL (
              ALERT_NOTIF_TERMINAL_ID,
              ALERT_NOTIF_ID,
              TERMINAL_ID)
            SELECT 
              SEQ_ALERT_NOTIF_TERMINAL_ID.nextval,
              l_alert_notif_id,
              TERMINAL_ID
            FROM REPORT.VW_USER_TERMINAL
          WHERE USER_ID = l_user_id
           AND TERMINAL_ID MEMBER OF l_terminal_id; 
         END IF;
         
         DELETE FROM  ALERT_NOTIF_ALERT_TYPE WHERE ALERT_NOTIF_ID = l_alert_notif_id;
         INSERT INTO ALERT_NOTIF_ALERT_TYPE (
               ALERT_NOTIF_ALERT_TYPE_ID,
               ALERT_NOTIF_ID,
               ALERT_TYPE_ID)
            SELECT 
               SEQ_ALERT_NOTIF_ALERT_TYPE_ID.nextval,
               l_alert_notif_id,
               ALERT_TYPE_ID
            FROM REPORT.ALERT_TYPE
           WHERE ALERT_TYPE_ID MEMBER OF l_alert_type_id;
        
    END;
  
    PROCEDURE USER_REPORT_INS(
        pn_user_id IN REPORT.USER_REPORT.USER_ID%TYPE,
        pn_report_id IN REPORT.USER_REPORT.REPORT_ID%TYPE,
        pn_frequency_id IN REPORT.USER_REPORT.FREQUENCY_ID%TYPE,
        pn_transport_id IN REPORT.CCS_TRANSPORT.CCS_TRANSPORT_ID%TYPE,
        pv_emailErrorsTo IN REPORT.USER_REPORT.ERROR_EMAIL_LIST%TYPE,
        pn_user_report_id OUT REPORT.USER_REPORT.USER_REPORT_ID%TYPE)
    IS
        ln_freq REPORT.USER_REPORT.FREQUENCY_ID%TYPE;
        ld_start_date REPORT.USER_REPORT.UPD_DT%TYPE := SYSDATE;
    BEGIN
        SELECT DECODE(R.BATCH_TYPE_ID, 0, pn_frequency_id, NULL),
               REPORT.USER_REPORT_SEQ.NEXTVAL
          INTO ln_freq,
               pn_user_report_id
          FROM REPORT.REPORTS R, REPORT.CCS_TRANSPORT T
         WHERE R.REPORT_ID = pn_report_id
           AND T.CCS_TRANSPORT_ID = pn_transport_id 
           AND T.USER_ID = pn_user_id;
        IF ln_freq IS NOT NULL THEN
            SELECT ADD_MONTHS(SYSDATE, -NVL(F.MONTHS, 0)) - NVL(F.DAYS, 0) 
              INTO ld_start_date
              FROM CORP.FREQUENCY F
             WHERE F.FREQUENCY_ID = ln_freq;
        END IF;
    	INSERT INTO REPORT.USER_REPORT (USER_REPORT_ID, USER_ID, REPORT_ID, FREQUENCY_ID, CCS_TRANSPORT_ID, UPD_DT, ERROR_EMAIL_LIST)
  	 		VALUES(pn_user_report_id, pn_user_id, pn_report_id, ln_freq, pn_transport_id, ld_start_date, pv_emailErrorsTo);
    EXCEPTION
    	WHEN NO_DATA_FOUND THEN
    	 	 RAISE_APPLICATION_ERROR(-20100, 'Could not find this report');
    	WHEN OTHERS THEN
    	 	 RAISE;
    END;
    
    PROCEDURE USER_REPORT_INS(
        pn_user_id IN REPORT.USER_REPORT.USER_ID%TYPE,
        pn_report_id IN REPORT.USER_REPORT.REPORT_ID%TYPE,
        pn_frequency_id IN REPORT.USER_REPORT.FREQUENCY_ID%TYPE,
        pn_transport_id IN REPORT.CCS_TRANSPORT.CCS_TRANSPORT_ID%TYPE,
        pn_user_report_id OUT REPORT.USER_REPORT.USER_REPORT_ID%TYPE)
    IS
    BEGIN
        USER_REPORT_INS(pn_user_id, pn_report_id, pn_frequency_id, pn_transport_id, null, pn_user_report_id);
    END;
    
    PROCEDURE USER_REPORT_UPD(
        l_user_report_id IN USER_REPORT.USER_REPORT_ID%TYPE,
        l_frequency_id IN USER_REPORT.FREQUENCY_ID%TYPE,
        l_transport_id IN CCS_TRANSPORT.CCS_TRANSPORT_ID%TYPE,
        pv_emailErrorsTo IN USER_REPORT.ERROR_EMAIL_LIST%TYPE)
    IS
      l_freq USER_REPORT.FREQUENCY_ID%TYPE;
    BEGIN
    	 SELECT DECODE(R.BATCH_TYPE_ID, 0, l_frequency_id, NULL)
          INTO l_freq
          FROM REPORTS R JOIN USER_REPORT UR ON R.REPORT_ID = UR.REPORT_ID
         WHERE UR.USER_REPORT_ID = l_user_report_id;
         UPDATE USER_REPORT SET FREQUENCY_ID = l_freq, 
          CCS_TRANSPORT_ID = l_transport_id, ERROR_EMAIL_LIST = pv_emailErrorsTo 
          WHERE USER_REPORT_ID = l_user_report_id;

    EXCEPTION
    	 WHEN NO_DATA_FOUND THEN
    	 	 RAISE_APPLICATION_ERROR(-20100, 'Could not find this report');
    	 WHEN OTHERS THEN
    	 	 RAISE;
    END;
    
    PROCEDURE USER_REPORT_UPD(
        l_user_report_id IN USER_REPORT.USER_REPORT_ID%TYPE,
        l_frequency_id IN USER_REPORT.FREQUENCY_ID%TYPE,
        l_transport_id IN CCS_TRANSPORT.CCS_TRANSPORT_ID%TYPE)
    IS
    BEGIN
    	 USER_REPORT_UPD(l_user_report_id, l_frequency_id, l_transport_id, null);
    END;
    
    PROCEDURE USER_REPORT_DEL(
        l_user_report_id IN USER_REPORT.USER_REPORT_ID%TYPE)
    IS
    BEGIN
    	 UPDATE USER_REPORT SET STATUS = 'D' WHERE USER_REPORT_ID = l_user_report_id;
    END;
    
    PROCEDURE TRANSPORT_PROP_UPD(
        l_transport_id IN CCS_TRANSPORT_PROPERTY.CCS_TRANSPORT_ID%TYPE,
        l_tpt_id IN CCS_TRANSPORT_PROPERTY.CCS_TRANSPORT_PROPERTY_TYPE_ID%TYPE,
        l_value IN CCS_TRANSPORT_PROPERTY.CCS_TRANSPORT_PROPERTY_VALUE%TYPE)
    IS
    BEGIN
    	UPDATE CCS_TRANSPORT_PROPERTY
           SET CCS_TRANSPORT_PROPERTY_VALUE = l_value
         WHERE CCS_TRANSPORT_PROPERTY_TYPE_ID = l_tpt_id
           AND CCS_TRANSPORT_ID = l_transport_id;
        IF SQL%ROWCOUNT = 0 THEN
            INSERT INTO CCS_TRANSPORT_PROPERTY(
                CCS_TRANSPORT_PROPERTY_ID,
                CCS_TRANSPORT_ID,
                CCS_TRANSPORT_PROPERTY_TYPE_ID,
                CCS_TRANSPORT_PROPERTY_VALUE)
              VALUES(
                CCS_TRANSPORT_PROPERTY_SEQ.NEXTVAL,
                l_transport_id,
                l_tpt_id,
                l_value
              );
        END IF;
    END;
    
    PROCEDURE REPORT_SENT_INS (
        l_user_report_id IN REPORT_SENT.USER_REPORT_ID%TYPE,
        l_report_request_id IN REPORT_SENT.REPORT_REQUEST_ID%TYPE,
        l_batch_id IN REPORT_SENT.BATCH_ID%TYPE,
        l_begin_date IN REPORT_SENT.BEGIN_DATE%TYPE,
        l_end_date IN REPORT_SENT.END_DATE%TYPE,
        l_rs_id OUT REPORT_SENT.REPORT_SENT_ID%TYPE,
        l_batch_type_id  IN REPORT_SENT.EXPORT_TYPE%TYPE  DEFAULT NULL)
    IS
        l_cnt PLS_INTEGER;
    BEGIN
        
        SELECT REPORT_SENT_SEQ.NEXTVAL INTO l_rs_id FROM DUAL;
        
        INSERT INTO REPORT_SENT(
            REPORT_SENT_ID,
            REPORT_REQUEST_ID,
            USER_REPORT_ID,
            BEGIN_DATE,
            END_DATE,
            BATCH_ID,
            EXPORT_TYPE,
            REPORT_SENT_STATE_ID,
            DETAILS)
          VALUES(
            l_rs_id,
            l_report_request_id,
            l_user_report_id,
            l_begin_date,
            l_end_date,
            l_batch_id,
            l_batch_type_id,
            0,
            'NEW');
        
        IF l_batch_id IS NULL THEN               
            DELETE FROM RESEND_REPORTS
             WHERE USER_REPORT_ID = l_user_report_id
               AND BATCH_ID IS NULL
               AND BEGIN_DATE = l_begin_date
               AND END_DATE = l_end_date;
        ELSE    
            DELETE FROM RESEND_REPORTS
             WHERE USER_REPORT_ID = l_user_report_id
               AND BATCH_ID = l_batch_id;
           
        END IF;      
    END;
    
    PROCEDURE CCS_TRANSPORT_ERROR_INS(
        pn_report_sent_id IN REPORT_SENT.REPORT_SENT_ID%TYPE,
        pv_error_text IN CCS_TRANSPORT_ERROR.ERROR_TEXT%TYPE,
        pd_error_ts IN CCS_TRANSPORT_ERROR.ERROR_TS%TYPE,
        pd_error_type_id IN CCS_TRANSPORT_ERROR.ERROR_TYPE_ID%TYPE,
        pv_retry_props IN CCS_TRANSPORT_ERROR.RETRY_PROPS%TYPE,
        pn_error_id OUT CCS_TRANSPORT_ERROR.CCS_TRANSPORT_ERROR_ID%TYPE)
    IS
        pv_mail_list REPORT.USER_REPORT.ERROR_EMAIL_LIST%TYPE;
        pv_mail_subject ENGINE.APP_SETTING.APP_SETTING_VALUE%TYPE;
        pv_mail_from ENGINE.APP_SETTING.APP_SETTING_VALUE%TYPE;
    BEGIN
        UPDATE REPORT.CCS_TRANSPORT 
           SET CCS_TRANSPORT_STATUS_CD = decode(CCS_TRANSPORT_STATUS_CD, 'D', 'D','E', 'E', 'R') 
         WHERE CCS_TRANSPORT_ID = (
           SELECT UR.CCS_TRANSPORT_ID 
             FROM REPORT.REPORT_SENT RS
             JOIN REPORT.USER_REPORT UR ON UR.USER_REPORT_ID = RS.USER_REPORT_ID
            WHERE RS.REPORT_SENT_ID = pn_report_sent_id);
        SELECT REPORT.SEQ_CCS_TRANSPORT_ERROR_ID.NEXTVAL 
          INTO pn_error_id 
           FROM DUAL;
        INSERT INTO REPORT.CCS_TRANSPORT_ERROR(
           CCS_TRANSPORT_ERROR_ID, 
           REPORT_SENT_ID, 
           ERROR_TEXT, 
           ERROR_TS,
           ERROR_TYPE_ID,
           RETRY_PROPS)
         VALUES(
           pn_error_id, 
           pn_report_sent_id, 
           pv_error_text, 
           pd_error_ts,
           pd_error_type_id,
           pv_retry_props);
           
        SELECT ERROR_EMAIL_LIST
           INTO pv_mail_list
           FROM REPORT.USER_REPORT ur
           JOIN REPORT.REPORT_SENT urs ON ur.USER_REPORT_ID = urs.USER_REPORT_ID
           WHERE urs.REPORT_SENT_ID = pn_report_sent_id;
        IF pv_mail_list IS NOT NULL THEN   
        	SELECT APP_SETTING_VALUE 
          		INTO pv_mail_subject
          		FROM ENGINE.APP_SETTING 
          		WHERE APP_SETTING_CD = 'REPORT_TRANSPORT_ERROR_EMAIL_SUBJECT';
        	SELECT APP_SETTING_VALUE
          		INTO pv_mail_from
          		FROM ENGINE.APP_SETTING 
          		WHERE APP_SETTING_CD = 'REPORT_TRANSPORT_ERROR_EMAIL_FROM';
        	INSERT INTO ENGINE.OB_EMAIL_QUEUE(
          		OB_EMAIL_FROM_EMAIL_ADDR, 
          		OB_EMAIL_FROM_NAME, 
          		OB_EMAIL_MSG, 
          		OB_EMAIL_SUBJECT, 
          		OB_EMAIL_TO_EMAIL_ADDR, 
          		OB_EMAIL_TO_NAME, 
          		OB_EMAIL_CONTENT)
        	VALUES(
          		pv_mail_from,
          		pv_mail_from,
          		' ',
          		pv_mail_subject,
          		pv_mail_list,
          		pv_mail_list,
          		pv_error_text);
        END IF;
    END;
    
    PROCEDURE CCS_TRANSPORT_ERROR_RETRY(
        pn_ct_error_id IN CCS_TRANSPORT_ERROR.CCS_TRANSPORT_ERROR_ID%TYPE)
    IS
    BEGIN
        UPDATE CCS_TRANSPORT_ERROR 
           SET REATTEMPTED_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP), 
               REATTEMPTED_FLAG = 'Y'
         WHERE CCS_TRANSPORT_ERROR_ID = pn_ct_error_id;
    END;
    
    PROCEDURE CCS_TRANSPORT_RETRY_SUCCESS(
        pn_ccs_transport_id IN CCS_TRANSPORT.CCS_TRANSPORT_ID%TYPE, 
        pn_ct_error_id IN CCS_TRANSPORT_ERROR.CCS_TRANSPORT_ERROR_ID%TYPE)
    IS
    BEGIN
        UPDATE CCS_TRANSPORT_ERROR 
           SET REATTEMPTED_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP), 
               REATTEMPTED_FLAG = 'Y',
               RETRY_COUNT = RETRY_COUNT+1
         WHERE CCS_TRANSPORT_ERROR_ID = pn_ct_error_id;
    END;
    
    PROCEDURE CCS_TRANSPORT_ERROR_UPDATE(
        pn_ccs_transport_id IN CCS_TRANSPORT.CCS_TRANSPORT_ID%TYPE, 
        pn_ct_error_id IN CCS_TRANSPORT_ERROR.CCS_TRANSPORT_ERROR_ID%TYPE,
        pv_error_text IN CCS_TRANSPORT_ERROR.ERROR_TEXT%TYPE,
        pd_error_type_id IN CCS_TRANSPORT_ERROR.ERROR_TYPE_ID%TYPE,
        pv_retry_props IN CCS_TRANSPORT_ERROR.RETRY_PROPS%TYPE,
        pn_disabled_transport OUT NUMBER)
    IS
      l_reattempted_ts CCS_TRANSPORT_ERROR.REATTEMPTED_TS%TYPE:=SYS_EXTRACT_UTC(SYSTIMESTAMP);
      l_created_ts CCS_TRANSPORT_ERROR.CREATED_TS%TYPE;
      l_prev_error_text CCS_TRANSPORT_ERROR.ERROR_TEXT%TYPE;
      l_prev_reattempted_ts CCS_TRANSPORT_ERROR.REATTEMPTED_TS%TYPE;
      l_retry_index NUMBER;
      l_last_sent_ts REPORT.USER_REPORT.LAST_SENT_UTC_TS%TYPE;
    BEGIN
       select ERROR_TEXT, REATTEMPTED_TS into l_prev_error_text, l_prev_reattempted_ts 
       from REPORT.CCS_TRANSPORT_ERROR WHERE CCS_TRANSPORT_ERROR_ID = pn_ct_error_id;
       
       UPDATE REPORT.CCS_TRANSPORT_ERROR 
           SET REATTEMPTED_TS = l_reattempted_ts,
               REATTEMPTED_FLAG = decode(REATTEMPTED_FLAG, 'D', 'D','N'),
               ERROR_TEXT = pv_error_text,
               ERROR_TYPE_ID = pd_error_type_id,
               RETRY_COUNT = RETRY_COUNT+1,
               RETRY_PROPS=pv_retry_props
         WHERE CCS_TRANSPORT_ERROR_ID = pn_ct_error_id returning created_ts, retry_count into l_created_ts, l_retry_index;
         
       insert into REPORT.CCS_TRANSPORT_ERROR_HISTORY(CCS_TRANSPORT_ERROR_ID, RETRY_INDEX, ERROR_TEXT,REATTEMPTED_TS)
       values( pn_ct_error_id, l_retry_index, l_prev_error_text, l_prev_reattempted_ts);
       
      IF (pd_error_type_id = 1 or pd_error_type_id = 3) and l_reattempted_ts > l_created_ts + 7 THEN
        select max(NVL(LAST_SENT_UTC_TS, SYS_EXTRACT_UTC(CAST (MIN_DATE AS TIMESTAMP)))) into l_last_sent_ts
        from report.user_report 
        where ccs_transport_id=pn_ccs_transport_id;
        
        IF l_reattempted_ts > l_last_sent_ts + 7 THEN
          UPDATE REPORT.CCS_TRANSPORT 
             SET CCS_TRANSPORT_STATUS_CD = 'E' 
          WHERE CCS_TRANSPORT_ID = pn_ccs_transport_id;   
          pn_disabled_transport:=1;
        ELSE
          UPDATE REPORT.USER_REPORT 
          SET STATUS='E'
          WHERE USER_REPORT_ID = (SELECT USER_REPORT_ID FROM REPORT.REPORT_SENT WHERE REPORT_SENT_ID=(SELECT REPORT_SENT_ID FROM REPORT.CCS_TRANSPORT_ERROR WHERE CCS_TRANSPORT_ERROR_ID = pn_ct_error_id))
          AND l_reattempted_ts > NVL(LAST_SENT_UTC_TS, SYS_EXTRACT_UTC(CAST (MIN_DATE AS TIMESTAMP)))+7;
          
          pn_disabled_transport:=0;
        END IF;
        
        UPDATE REPORT.CCS_TRANSPORT_ERROR set REATTEMPTED_FLAG='D' 
        WHERE CCS_TRANSPORT_ERROR_ID = pn_ct_error_id;
        
        UPDATE REPORT.REPORT_SENT set REPORT_SENT_STATE_ID=5 
        WHERE REPORT_SENT_ID = (SELECT REPORT_SENT_ID FROM REPORT.CCS_TRANSPORT_ERROR WHERE CCS_TRANSPORT_ERROR_ID = pn_ct_error_id);
      ELSE
        pn_disabled_transport:=0;
      END IF;
    END;
    
    PROCEDURE CCS_UPDATE_REATTEMPTED_FLAG(
        pn_ct_error_id IN CCS_TRANSPORT_ERROR.CCS_TRANSPORT_ERROR_ID%TYPE,
        pn_reattempted_flag IN CCS_TRANSPORT_ERROR.REATTEMPTED_FLAG%TYPE)
    IS
    BEGIN
      UPDATE REPORT.CCS_TRANSPORT_ERROR SET REATTEMPTED_FLAG=pn_reattempted_flag WHERE CCS_TRANSPORT_ERROR_ID=pn_ct_error_id;
    END;
    
    -- BEGIN Device alerting additions
    -- -------------------------------
    -- R45 and above
    PROCEDURE ADD_EVENT_ALERT(
        pn_event_id IN REPORT.TERMINAL_ALERT.REFERENCE_ID%TYPE,
        pn_terminal_alert_id OUT REPORT.TERMINAL_ALERT.TERMINAL_ALERT_ID%TYPE,
        pn_terminal_id OUT REPORT.TERMINAL.TERMINAL_ID%TYPE,
        pv_device_serial_cd OUT REPORT.EPORT.EPORT_SERIAL_NUM%TYPE,
        pc_dont_send_flag OUT VARCHAR2)
    IS
        lc_alert_status REPORT.ALERT.STATUS%TYPE;
        ln_alert_id REPORT.ALERT.ALERT_ID%TYPE;
        ld_alert_ts REPORT.TERMINAL_ALERT.ALERT_DATE%TYPE;
        lv_alert_description REPORT.TERMINAL_ALERT.DETAILS%TYPE;
        ln_component REPORT.TERMINAL_ALERT.COMPONENT_ID%TYPE;
        ld_notification_ts REPORT.TERMINAL_ALERT.NOTIFICATION_DATE%TYPE;
    BEGIN
        SELECT TE.TERMINAL_ID, D.DEVICE_SERIAL_CD, A.STATUS, A.ALERT_ID, E.EVENT_START_TS, 
               CASE WHEN A.ALERT_SOURCE_ID IN(2,3) AND REGEXP_LIKE(ED_D.EVENT_DETAIL_VALUE, '^\d\d/\d\d/\d{4} \d\d:\d\d:\d\d - ') THEN SUBSTR(ED_D.EVENT_DETAIL_VALUE, 23, 4000) ELSE ED_D.EVENT_DETAIL_VALUE END, 
               H.HOST_PORT_NUM, E.EVENT_UPLOAD_COMPLETE_TS
          INTO pn_terminal_id, pv_device_serial_cd, lc_alert_status, ln_alert_id, ld_alert_ts, lv_alert_description, ln_component, ld_notification_ts
          FROM DEVICE.EVENT E
          JOIN DEVICE.HOST H ON E.HOST_ID = H.HOST_ID
          JOIN DEVICE.DEVICE D ON H.DEVICE_ID = D.DEVICE_ID
          LEFT OUTER JOIN REPORT.ALERT A ON E.EVENT_TYPE_ID = A.EVENT_TYPE_ID AND A.ALERT_SOURCE_ID IN(2,3)
          LEFT OUTER JOIN (REPORT.TERMINAL_EPORT TE
          JOIN REPORT.EPORT EP ON TE.EPORT_ID = EP.EPORT_ID) 
            ON D.DEVICE_SERIAL_CD = EP.EPORT_SERIAL_NUM 
           AND E.EVENT_START_TS >= NVL(TE.START_DATE, MIN_DATE)
           AND E.EVENT_START_TS < NVL(TE.END_DATE, MAX_DATE)
          LEFT OUTER JOIN DEVICE.EVENT_DETAIL ED_D ON E.EVENT_ID = ED_D.EVENT_ID AND ED_D.EVENT_DETAIL_TYPE_ID = 3
         WHERE E.EVENT_ID = pn_event_id;
        IF pn_terminal_id IS NOT NULL AND lc_alert_status = 'A' THEN
           IF ld_alert_ts >= SYSDATE - 20 THEN
	           SELECT CASE WHEN COUNT(*) > 0 THEN 'Y' ELSE 'N' END
	             INTO pc_dont_send_flag
	             FROM REPORT.TERMINAL_ALERT
	            WHERE ALERT_ID = ln_alert_id
	              AND TERMINAL_ID = pn_terminal_id
	              AND ALERT_DATE > ld_alert_ts - 0.5
	              AND DETAILS = lv_alert_description
	              AND RESPONSE_SENT IN('P', 'Y');
	        ELSE
	           pc_dont_send_flag := 'Y'; /*Don't send old alert*/
	        END IF;
            SELECT REPORT.TERMINAL_ALERT_SEQ.NEXTVAL 
              INTO pn_terminal_alert_id
              FROM DUAL;
            INSERT INTO REPORT.TERMINAL_ALERT(	
                TERMINAL_ALERT_ID, 
                ALERT_ID, 
                TERMINAL_ID, 
                ALERT_DATE, 
                DETAILS, 
                RESPONSE_SENT,
                REFERENCE_ID, 
                COMPONENT_ID, 
                NOTIFICATION_DATE)
            VALUES(
                pn_terminal_alert_id,
                ln_alert_id,
                pn_terminal_id,
                ld_alert_ts,
                lv_alert_description,
                CASE WHEN pc_dont_send_flag = 'Y' THEN 'I' ELSE 'P' END,
                pn_event_id,
                ln_component,
                ld_notification_ts
            );
       ELSE
           pc_dont_send_flag := 'Y'; 
       END IF;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            NULL;
    END;
    
	-- END Device alerting additions
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/VW_CUSTOMER_SALES_ANALYTICS.vws?revision=1.1
CREATE OR REPLACE VIEW REPORT.VW_CUSTOMER_SALES_ANALYTICS AS
SELECT  /*+ index(tt IX_TSBD_TRAN_DATE) */ 
  EP.customer_id,
  EP.customer_name,  
  EP.eport_serial_num, 
  EP.activation_date,
  EP.min_tran_date,
  -- First Month
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= EP.min_tran_date AND TSD.tran_date < TO_DATE(EP.min_tran_date+31) THEN TSD.TRAN_AMOUNT  END) FIRST_31_CC_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= EP.min_tran_date AND TSD.tran_date < TO_DATE(EP.min_tran_date+31) THEN TSD.TRAN_AMOUNT  END) FIRST_31_CASH_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13,22) AND TSD.tran_date >= EP.min_tran_date AND TSD.tran_date < TO_DATE(EP.min_tran_date+31) THEN TSD.TRAN_AMOUNT  END) FIRST_31_TOTAL_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= EP.min_tran_date AND TSD.tran_date < TO_DATE(EP.min_tran_date+31) THEN TSD.TRAN_COUNT  END) FIRST_31_CC_COUNT,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= EP.min_tran_date AND TSD.tran_date < TO_DATE(EP.min_tran_date+31) THEN TSD.TRAN_COUNT  END) FIRST_31_CASH_COUNT,
  -- Month 2
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+31) AND TSD.tran_date < TO_DATE(EP.min_tran_date+61) THEN TSD.TRAN_AMOUNT  END) Month2_CC_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+31) AND TSD.tran_date < TO_DATE(EP.min_tran_date+61) THEN TSD.TRAN_AMOUNT  END) Month2_CASH_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13,22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+31) AND TSD.tran_date < TO_DATE(EP.min_tran_date+61) THEN TSD.TRAN_AMOUNT  END) Month2_TOTAL_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+31) AND TSD.tran_date < TO_DATE(EP.min_tran_date+61) THEN TSD.TRAN_COUNT  END) Month2_CC_COUNT,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+31) AND TSD.tran_date < TO_DATE(EP.min_tran_date+61) THEN TSD.TRAN_COUNT END) Month2_CASH_COUNT,
  -- Month 3
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+61) AND TSD.tran_date < TO_DATE(EP.min_tran_date+91) THEN TSD.TRAN_AMOUNT  END) Month3_CC_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+61) AND TSD.tran_date < TO_DATE(EP.min_tran_date+91) THEN TSD.TRAN_AMOUNT  END) Month3_CASH_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13,22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+61) AND TSD.tran_date < TO_DATE(EP.min_tran_date+91) THEN TSD.TRAN_AMOUNT  END) Month3_TOTAL_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+61) AND TSD.tran_date < TO_DATE(EP.min_tran_date+91) THEN TSD.TRAN_COUNT  END) Month3_CC_COUNT,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+61) AND TSD.tran_date < TO_DATE(EP.min_tran_date+91) THEN TSD.TRAN_COUNT END) Month3_CASH_COUNT,
  -- Month 4
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+91) AND TSD.tran_date < TO_DATE(EP.min_tran_date+121) THEN TSD.TRAN_AMOUNT  END) Month4_CC_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+91) AND TSD.tran_date < TO_DATE(EP.min_tran_date+121) THEN TSD.TRAN_AMOUNT END) Month4_CASH_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13,22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+91) AND TSD.tran_date < TO_DATE(EP.min_tran_date+121) THEN TSD.TRAN_AMOUNT  END) Month4_TOTAL_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+91) AND TSD.tran_date < TO_DATE(EP.min_tran_date+121) THEN TSD.TRAN_COUNT  END) Month4_CC_COUNT,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+91) AND TSD.tran_date < TO_DATE(EP.min_tran_date+121) THEN TSD.TRAN_COUNT END) Month4_CASH_COUNT,
  -- Month 5
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+121) AND TSD.tran_date < TO_DATE(EP.min_tran_date+151) THEN TSD.TRAN_AMOUNT  END) Month5_CC_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+121) AND TSD.tran_date < TO_DATE(EP.min_tran_date+151) THEN TSD.TRAN_AMOUNT  END) Month5_CASH_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13,22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+121) AND TSD.tran_date < TO_DATE(EP.min_tran_date+151) THEN TSD.TRAN_AMOUNT  END) Month5_TOTAL_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+121) AND TSD.tran_date < TO_DATE(EP.min_tran_date+151) THEN TSD.TRAN_COUNT  END) Month5_CC_COUNT,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+121) AND TSD.tran_date < TO_DATE(EP.min_tran_date+151) THEN TSD.TRAN_COUNT END) Month5_CASH_COUNT,
  -- Month 6
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+151) AND TSD.tran_date < TO_DATE(EP.min_tran_date+181) THEN TSD.TRAN_AMOUNT  END) Month6_CC_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+151) AND TSD.tran_date < TO_DATE(EP.min_tran_date+181) THEN TSD.TRAN_AMOUNT  END) Month6_CASH_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13,22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+151) AND TSD.tran_date < TO_DATE(EP.min_tran_date+181) THEN TSD.TRAN_AMOUNT  END) Month6_TOTAL_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+151) AND TSD.tran_date < TO_DATE(EP.min_tran_date+181) THEN TSD.TRAN_COUNT  END) Month6_CC_COUNT,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+151) AND TSD.tran_date < TO_DATE(EP.min_tran_date+181) THEN TSD.TRAN_COUNT END) Month6_CASH_COUNT,
  -- Month 7
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+181) AND TSD.tran_date < TO_DATE(EP.min_tran_date+211) THEN TSD.TRAN_AMOUNT  END) Month7_CC_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+181) AND TSD.tran_date < TO_DATE(EP.min_tran_date+211) THEN TSD.TRAN_AMOUNT  END) Month7_CASH_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13,22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+181) AND TSD.tran_date < TO_DATE(EP.min_tran_date+211) THEN TSD.TRAN_AMOUNT  END) Month7_TOTAL_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+181) AND TSD.tran_date < TO_DATE(EP.min_tran_date+211) THEN TSD.TRAN_COUNT  END) Month7_CC_COUNT,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+181) AND TSD.tran_date < TO_DATE(EP.min_tran_date+211) THEN TSD.TRAN_COUNT END) Month7_CASH_COUNT,
  -- Month 8 
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+211) AND TSD.tran_date < TO_DATE(EP.min_tran_date+241) THEN TSD.TRAN_AMOUNT  END) Month8_CC_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+211) AND TSD.tran_date < TO_DATE(EP.min_tran_date+241) THEN TSD.TRAN_AMOUNT  END) Month8_CASH_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13,22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+211) AND TSD.tran_date < TO_DATE(EP.min_tran_date+241) THEN TSD.TRAN_AMOUNT  END) Month8_TOTAL_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+211) AND TSD.tran_date < TO_DATE(EP.min_tran_date+241) THEN TSD.TRAN_COUNT  END) Month8_CC_COUNT,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+211) AND TSD.tran_date < TO_DATE(EP.min_tran_date+241) THEN TSD.TRAN_COUNT END) Month8_CASH_COUNT,
  -- Month 9
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+241) AND TSD.tran_date < TO_DATE(EP.min_tran_date+271) THEN TSD.TRAN_AMOUNT  END) Month9_CC_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+241) AND TSD.tran_date < TO_DATE(EP.min_tran_date+271) THEN TSD.TRAN_AMOUNT  END) Month9_CASH_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13,22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+241) AND TSD.tran_date < TO_DATE(EP.min_tran_date+271) THEN TSD.TRAN_AMOUNT  END) Month9_TOTAL_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+241) AND TSD.tran_date < TO_DATE(EP.min_tran_date+271) THEN TSD.TRAN_COUNT  END) Month9_CC_COUNT,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+241) AND TSD.tran_date < TO_DATE(EP.min_tran_date+271) THEN TSD.TRAN_COUNT END) Month9_CASH_COUNT,
  -- Month 10
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+271) AND TSD.tran_date < TO_DATE(EP.min_tran_date+301) THEN TSD.TRAN_AMOUNT  END) Month10_CC_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+271) AND TSD.tran_date < TO_DATE(EP.min_tran_date+301) THEN TSD.TRAN_AMOUNT  END) Month10_CASH_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13,22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+271) AND TSD.tran_date < TO_DATE(EP.min_tran_date+301) THEN TSD.TRAN_AMOUNT  END) Month10_TOTAL_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+271) AND TSD.tran_date < TO_DATE(EP.min_tran_date+301) THEN TSD.TRAN_COUNT  END) Month10_CC_COUNT,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+271) AND TSD.tran_date < TO_DATE(EP.min_tran_date+301) THEN TSD.TRAN_COUNT END) Month10_CASH_COUNT,
  -- Month 11
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+301) AND TSD.tran_date < TO_DATE(EP.min_tran_date+331) THEN TSD.TRAN_AMOUNT  END) Month11_CC_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+301) AND TSD.tran_date < TO_DATE(EP.min_tran_date+331) THEN TSD.TRAN_AMOUNT  END) Month11_CASH_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13,22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+301) AND TSD.tran_date < TO_DATE(EP.min_tran_date+331) THEN TSD.TRAN_AMOUNT  END) Month11_TOTAL_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+301) AND TSD.tran_date < TO_DATE(EP.min_tran_date+331) THEN TSD.TRAN_COUNT  END) Month11_CC_COUNT,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+301) AND TSD.tran_date < TO_DATE(EP.min_tran_date+331) THEN TSD.TRAN_COUNT END) Month11_CASH_COUNT,
  -- Month 12
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+331) AND TSD.tran_date < TO_DATE(EP.min_tran_date+361) THEN TSD.TRAN_AMOUNT  END) Month12_CC_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+331) AND TSD.tran_date < TO_DATE(EP.min_tran_date+361) THEN TSD.TRAN_AMOUNT  END) Month12_CASH_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13,22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+331) AND TSD.tran_date < TO_DATE(EP.min_tran_date+361) THEN TSD.TRAN_AMOUNT  END) Month12_TOTAL_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+331) AND TSD.tran_date < TO_DATE(EP.min_tran_date+361) THEN TSD.TRAN_COUNT  END) Month12_CC_COUNT,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+331) AND TSD.tran_date < TO_DATE(EP.min_tran_date+361) THEN TSD.TRAN_COUNT END) Month12_CASH_COUNT,
  -- Month 13
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+361) AND TSD.tran_date < TO_DATE(EP.min_tran_date+391) THEN TSD.TRAN_AMOUNT  END) Month13_CC_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+361) AND TSD.tran_date < TO_DATE(EP.min_tran_date+391) THEN TSD.TRAN_AMOUNT  END) Month13_CASH_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13,22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+361) AND TSD.tran_date < TO_DATE(EP.min_tran_date+391) THEN TSD.TRAN_AMOUNT  END) Month13_TOTAL_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+361) AND TSD.tran_date < TO_DATE(EP.min_tran_date+391) THEN TSD.TRAN_COUNT  END) Month13_CC_COUNT,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+361) AND TSD.tran_date < TO_DATE(EP.min_tran_date+391) THEN TSD.TRAN_COUNT END) Month13_CASH_COUNT,
  -- Month 14
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+391) AND TSD.tran_date < TO_DATE(EP.min_tran_date+421) THEN TSD.TRAN_AMOUNT  END) Month14_CC_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+391) AND TSD.tran_date < TO_DATE(EP.min_tran_date+421) THEN TSD.TRAN_AMOUNT  END) Month14_CASH_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13,22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+391) AND TSD.tran_date < TO_DATE(EP.min_tran_date+421) THEN TSD.TRAN_AMOUNT  END) Month14_TOTAL_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+391) AND TSD.tran_date < TO_DATE(EP.min_tran_date+421) THEN TSD.TRAN_COUNT  END) Month14_CC_COUNT,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+391) AND TSD.tran_date < TO_DATE(EP.min_tran_date+421) THEN TSD.TRAN_COUNT END) Month14_CASH_COUNT,
  -- Month 15
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+421) AND TSD.tran_date < TO_DATE(EP.min_tran_date+451) THEN TSD.TRAN_AMOUNT  END) Month15_CC_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+421) AND TSD.tran_date < TO_DATE(EP.min_tran_date+451) THEN TSD.TRAN_AMOUNT  END) Month15_CASH_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13,22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+421) AND TSD.tran_date < TO_DATE(EP.min_tran_date+451) THEN TSD.TRAN_AMOUNT  END) Month15_TOTAL_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+421) AND TSD.tran_date < TO_DATE(EP.min_tran_date+451) THEN TSD.TRAN_COUNT  END) Month15_CC_COUNT,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+421) AND TSD.tran_date < TO_DATE(EP.min_tran_date+451) THEN TSD.TRAN_COUNT END) Month15_CASH_COUNT,
  -- Month 16
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+451) AND TSD.tran_date < TO_DATE(EP.min_tran_date+481) THEN TSD.TRAN_AMOUNT  END) Month16_CC_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+451) AND TSD.tran_date < TO_DATE(EP.min_tran_date+481) THEN TSD.TRAN_AMOUNT  END) Month16_CASH_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13,22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+451) AND TSD.tran_date < TO_DATE(EP.min_tran_date+481) THEN TSD.TRAN_AMOUNT  END) Month16_TOTAL_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+451) AND TSD.tran_date < TO_DATE(EP.min_tran_date+481) THEN TSD.TRAN_COUNT  END) Month16_CC_COUNT,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+451) AND TSD.tran_date < TO_DATE(EP.min_tran_date+481) THEN TSD.TRAN_COUNT END) Month16_CASH_COUNT,
  -- Month 17
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+481) AND TSD.tran_date < TO_DATE(EP.min_tran_date+511) THEN TSD.TRAN_AMOUNT  END) Month17_CC_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+481) AND TSD.tran_date < TO_DATE(EP.min_tran_date+511) THEN TSD.TRAN_AMOUNT  END) Month17_CASH_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13,22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+481) AND TSD.tran_date < TO_DATE(EP.min_tran_date+511) THEN TSD.TRAN_AMOUNT  END) Month17_TOTAL_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+481) AND TSD.tran_date < TO_DATE(EP.min_tran_date+511) THEN TSD.TRAN_COUNT  END) Month17_CC_COUNT,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+481) AND TSD.tran_date < TO_DATE(EP.min_tran_date+511) THEN TSD.TRAN_COUNT END) Month17_CASH_COUNT,
  -- Month 18
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+511) AND TSD.tran_date < TO_DATE(EP.min_tran_date+541) THEN TSD.TRAN_AMOUNT  END) Month18_CC_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+511) AND TSD.tran_date < TO_DATE(EP.min_tran_date+541) THEN TSD.TRAN_AMOUNT  END) Month18_CASH_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13,22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+511) AND TSD.tran_date < TO_DATE(EP.min_tran_date+541) THEN TSD.TRAN_AMOUNT  END) Month18_TOTAL_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+511) AND TSD.tran_date < TO_DATE(EP.min_tran_date+541) THEN TSD.TRAN_COUNT  END) Month18_CC_COUNT,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+511) AND TSD.tran_date < TO_DATE(EP.min_tran_date+541) THEN TSD.TRAN_COUNT END) Month18_CASH_COUNT,
  -- Month 19
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+541) AND TSD.tran_date < TO_DATE(EP.min_tran_date+571) THEN TSD.TRAN_AMOUNT  END) Month19_CC_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+541) AND TSD.tran_date < TO_DATE(EP.min_tran_date+571) THEN TSD.TRAN_AMOUNT  END) Month19_CASH_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13,22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+541) AND TSD.tran_date < TO_DATE(EP.min_tran_date+571) THEN TSD.TRAN_AMOUNT  END) Month19_TOTAL_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+541) AND TSD.tran_date < TO_DATE(EP.min_tran_date+571) THEN TSD.TRAN_COUNT  END) Month19_CC_COUNT,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+541) AND TSD.tran_date < TO_DATE(EP.min_tran_date+571) THEN TSD.TRAN_COUNT END) Month19_CASH_COUNT,
  -- Month 20
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+571) AND TSD.tran_date < TO_DATE(EP.min_tran_date+601) THEN TSD.TRAN_AMOUNT  END) Month20_CC_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+571) AND TSD.tran_date < TO_DATE(EP.min_tran_date+601) THEN TSD.TRAN_AMOUNT  END) Month20_CASH_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13,22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+571) AND TSD.tran_date < TO_DATE(EP.min_tran_date+601) THEN TSD.TRAN_AMOUNT  END) Month20_TOTAL_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+571) AND TSD.tran_date < TO_DATE(EP.min_tran_date+601) THEN TSD.TRAN_COUNT  END) Month20_CC_COUNT,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+571) AND TSD.tran_date < TO_DATE(EP.min_tran_date+601) THEN TSD.TRAN_COUNT END) Month20_CASH_COUNT,
  -- Month 21
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+601) AND TSD.tran_date < TO_DATE(EP.min_tran_date+631) THEN TSD.TRAN_AMOUNT  END) Month21_CC_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+601) AND TSD.tran_date < TO_DATE(EP.min_tran_date+631) THEN TSD.TRAN_AMOUNT  END) Month21_CASH_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13,22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+601) AND TSD.tran_date < TO_DATE(EP.min_tran_date+631) THEN TSD.TRAN_AMOUNT  END) Month21_TOTAL_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+601) AND TSD.tran_date < TO_DATE(EP.min_tran_date+631) THEN TSD.TRAN_COUNT  END) Month21_CC_COUNT,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+601) AND TSD.tran_date < TO_DATE(EP.min_tran_date+631) THEN TSD.TRAN_COUNT END) Month21_CASH_COUNT,
  -- Month 22
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+631) AND TSD.tran_date < TO_DATE(EP.min_tran_date+661) THEN TSD.TRAN_AMOUNT  END) Month22_CC_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+631) AND TSD.tran_date < TO_DATE(EP.min_tran_date+661) THEN TSD.TRAN_AMOUNT  END) Month22_CASH_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13,22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+631) AND TSD.tran_date < TO_DATE(EP.min_tran_date+661) THEN TSD.TRAN_AMOUNT  END) Month22_TOTAL_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+631) AND TSD.tran_date < TO_DATE(EP.min_tran_date+661) THEN TSD.TRAN_COUNT  END) Month22_CC_COUNT,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+631) AND TSD.tran_date < TO_DATE(EP.min_tran_date+661) THEN TSD.TRAN_COUNT END) Month22_CASH_COUNT,
  -- Month 23
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+661) AND TSD.tran_date < TO_DATE(EP.min_tran_date+691) THEN TSD.TRAN_AMOUNT  END) Month23_CC_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+661) AND TSD.tran_date < TO_DATE(EP.min_tran_date+691) THEN TSD.TRAN_AMOUNT  END) Month23_CASH_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13,22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+661) AND TSD.tran_date < TO_DATE(EP.min_tran_date+691) THEN TSD.TRAN_AMOUNT  END) Month23_TOTAL_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+661) AND TSD.tran_date < TO_DATE(EP.min_tran_date+691) THEN TSD.TRAN_COUNT  END) Month23_CC_COUNT,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+661) AND TSD.tran_date < TO_DATE(EP.min_tran_date+691) THEN TSD.TRAN_COUNT END) Month23_CASH_COUNT,
  -- Month 24
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+691) AND TSD.tran_date < TO_DATE(EP.min_tran_date+721) THEN TSD.TRAN_AMOUNT  END) Month24_CC_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+691) AND TSD.tran_date < TO_DATE(EP.min_tran_date+721) THEN TSD.TRAN_AMOUNT  END) Month24_CASH_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13,22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+691) AND TSD.tran_date < TO_DATE(EP.min_tran_date+721) THEN TSD.TRAN_AMOUNT  END) Month24_TOTAL_SALES,
  SUM(CASE WHEN TSD.trans_type_id IN (16,17,18,15,14,13) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+691) AND TSD.tran_date < TO_DATE(EP.min_tran_date+721) THEN TSD.TRAN_COUNT  END) Month24_CC_COUNT,
  SUM(CASE WHEN TSD.trans_type_id IN (22) AND TSD.tran_date >= TO_DATE(EP.min_tran_date+691) AND TSD.tran_date < TO_DATE(EP.min_tran_date+721) THEN TSD.TRAN_COUNT END) Month24_CASH_COUNT 
FROM 
  REPORT.TRANS_STAT_BY_DAY TSD,
  (SELECT ce.customer_id, ce.customer_name,  e.eport_serial_num, e.activation_date, t.eport_id, MIN(t.tran_date) min_tran_date 
  FROM REPORT.TRANS_STAT_BY_DAY t,  REPORT.EPORT e, REPORT.VW_CUSTOMER_EPORT ce
  WHERE t.eport_id=e.eport_id AND ce.eport_id=e.eport_id 
  AND trans_type_id=16 AND tran_count> 1 AND t.tran_date >= sysdate-721
  GROUP BY ce.customer_id, ce.customer_name,  t.eport_id, e.eport_serial_num, e.activation_date) EP
WHERE  
  TSD.eport_id=EP.eport_id 
  AND TSD.tran_date >= sysdate-721
  AND  TSD.trans_type_id IN (16,17,18,15,14,13,22) 
GROUP BY
  EP.customer_id,
  EP.customer_name,  
  EP.eport_serial_num, 
  EP.activation_date, 
  EP.min_tran_date
ORDER BY 
  1,
  2,
  3
 / 
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/releases/REL_Edge_Update_R55/R55.USADB.USA433.sql?revision=HEAD
-- view permissions
GRANT SELECT ON REPORT.VW_CUSTOMER_SALES_ANALYTICS TO USALIVE_APP_ROLE;
GRANT SELECT ON REPORT.VW_CUSTOMER_SALES_ANALYTICS TO USAT_DEV_READ_ONLY;  

COMMIT;

DECLARE
	ln_report_id NUMBER := 1;
	ln_user_id NUMBER :=1;
BEGIN
	
	SELECT MAX(REPORT_ID) + 1 INTO ln_report_id 
	FROM REPORT.REPORTS;
	
	-- insert report
	INSERT INTO REPORT.REPORTS
	(REPORT_ID, TITLE, GENERATOR_ID, UPD_DT, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID, CATEGORY)
	SELECT
	 ln_report_id,
	  'Customer Sales Analysis', 
	  1,
	  sysdate,
	  0,
	  'Customer Sales Analysis',
	  'Generates an csv file containing 24 months of sales data for the customer specified in the parameter.',
	  'N',
	  0,
	  'Accounting'
	FROM DUAL; 

	-- insert report parameters 
	INSERT INTO REPORT.REPORT_PARAM
	(REPORT_ID, PARAM_NAME, PARAM_VALUE)
	SELECT
	  ln_report_id,
	  'query',
	  'SELECT 
  customer_name,  
  eport_serial_num, 
  activation_date,
  min_tran_date,
  FIRST_31_CC_SALES,
  FIRST_31_CASH_SALES,
  FIRST_31_TOTAL_SALES,
  FIRST_31_CC_COUNT,
  FIRST_31_CASH_COUNT,
  Month2_CC_SALES,
  Month2_CASH_SALES,
  Month2_TOTAL_SALES,
  Month2_CC_COUNT,
  Month2_CASH_COUNT,
  Month3_CC_SALES,
  Month3_CASH_SALES,
  Month3_TOTAL_SALES,
  Month3_CC_COUNT,
  Month3_CASH_COUNT,
  Month4_CC_SALES,
  Month4_CASH_SALES,
  Month4_TOTAL_SALES,
  Month4_CC_COUNT,
  Month4_CASH_COUNT,
  Month5_CC_SALES,
  Month5_CASH_SALES,
  Month5_TOTAL_SALES,
  Month5_CC_COUNT,
  Month5_CASH_COUNT,
  Month6_CC_SALES,
  Month6_CASH_SALES,
  Month6_TOTAL_SALES,
  Month6_CC_COUNT,
  Month6_CASH_COUNT,
  Month7_CC_SALES,
  Month7_CASH_SALES,
  Month7_TOTAL_SALES,
  Month7_CC_COUNT,
  Month7_CASH_COUNT,
  Month8_CC_SALES,
  Month8_CASH_SALES,
  Month8_TOTAL_SALES,
  Month8_CC_COUNT,
  Month8_CASH_COUNT,
  Month9_CC_SALES,
  Month9_CASH_SALES,
  Month9_TOTAL_SALES,
  Month9_CC_COUNT,
  Month9_CASH_COUNT,
  Month10_CC_SALES,
  Month10_CASH_SALES,
  Month10_TOTAL_SALES,
  Month10_CC_COUNT,
  Month10_CASH_COUNT,
  Month11_CC_SALES,
  Month11_CASH_SALES,
  Month11_TOTAL_SALES,
  Month11_CC_COUNT,
  Month11_CASH_COUNT,
  Month12_CC_SALES,
  Month12_CASH_SALES,
  Month12_TOTAL_SALES,
  Month12_CC_COUNT,
  Month12_CASH_COUNT,
  Month13_CC_SALES,
  Month13_CASH_SALES,
  Month13_TOTAL_SALES,
  Month13_CC_COUNT,
  Month13_CASH_COUNT,
  Month14_CC_SALES,
  Month14_CASH_SALES,
  Month14_TOTAL_SALES,
  Month14_CC_COUNT,
  Month14_CASH_COUNT,
  Month15_CC_SALES,
  Month15_CASH_SALES,
  Month15_TOTAL_SALES,
  Month15_CC_COUNT,
  Month15_CASH_COUNT,
  Month16_CC_SALES,
  Month16_CASH_SALES,
  Month16_TOTAL_SALES,
  Month16_CC_COUNT,
  Month16_CASH_COUNT,
  Month17_CC_SALES,
  Month17_CASH_SALES,
  Month17_TOTAL_SALES,
  Month17_CC_COUNT,
  Month17_CASH_COUNT,
  Month18_CC_SALES,
  Month18_CASH_SALES,
  Month18_TOTAL_SALES,
  Month18_CC_COUNT,
  Month18_CASH_COUNT,
  Month19_CC_SALES,
  Month19_CASH_SALES,
  Month19_TOTAL_SALES,
  Month19_CC_COUNT,
  Month19_CASH_COUNT,
  Month20_CC_SALES,
  Month20_CASH_SALES,
  Month20_TOTAL_SALES,
  Month20_CC_COUNT,
  Month20_CASH_COUNT,
  Month21_CC_SALES,
  Month21_CASH_SALES,
  Month21_TOTAL_SALES,
  Month21_CC_COUNT,
  Month21_CASH_COUNT,
  Month22_CC_SALES,
  Month22_CASH_SALES,
  Month22_TOTAL_SALES,
  Month22_CC_COUNT,
  Month22_CASH_COUNT,
  Month23_CC_SALES,
  Month23_CASH_SALES,
  Month23_TOTAL_SALES,
  Month23_CC_COUNT,
  Month23_CASH_COUNT,
  Month24_CC_SALES,
  Month24_CASH_SALES,
  Month24_TOTAL_SALES,
  Month24_CC_COUNT,
  Month24_CASH_COUNT
FROM REPORT.VW_CUSTOMER_SALES_ANALYTICS 
WHERE CUSTOMER_ID = ? '
	FROM DUAL;

	INSERT INTO REPORT.REPORT_PARAM
	(REPORT_ID, PARAM_NAME, PARAM_VALUE)
	SELECT
	  ln_report_id,
	  'paramNames',
	  'customerIds'
	FROM DUAL;    
	  
	INSERT INTO REPORT.REPORT_PARAM
	(REPORT_ID, PARAM_NAME, PARAM_VALUE)
	SELECT
	  ln_report_id,
	  'paramTypes',
	  'BIGINT'
	FROM DUAL; 

	INSERT INTO REPORT.REPORT_PARAM
	(REPORT_ID, PARAM_NAME, PARAM_VALUE)
	SELECT
	  ln_report_id,
	  'params.customerIds',
	  '{m}'
	FROM DUAL;
	
	INSERT INTO REPORT.REPORT_PARAM
	(REPORT_ID, PARAM_NAME, PARAM_VALUE)
	SELECT
	  ln_report_id,
	  'header',
	  'true'
	FROM DUAL;	

	-- insert the web link to display customer selector
	INSERT INTO WEB_CONTENT.WEB_LINK
	(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER)
	SELECT
	 (SELECT MAX(WEB_LINK_ID) + 1 FROM WEB_CONTENT.WEB_LINK) AS ID,
	  'Customer Sales Analysis', 
	  './select_customer.html?basicReportId=' || ln_report_id,
	  'Generates an csv file containing 24 months of sales data for the customer specified in the parameter.',
	  'Accounting',
	  'W',
	  0  
	FROM DUAL;
	
	-- link permissions / requirements
    INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT
    (WEB_LINK_ID, REQUIREMENT_ID)
    SELECT
    (SELECT MAX(WEB_LINK_ID) FROM WEB_CONTENT.WEB_LINK),
    1
    FROM DUAL;

    INSERT INTO WEB_CONTENT.WEB_LINK_REQUIREMENT
    (WEB_LINK_ID, REQUIREMENT_ID)
    SELECT
    (SELECT MAX(WEB_LINK_ID) FROM WEB_CONTENT.WEB_LINK),
    28
    FROM DUAL;    
    
	COMMIT;
END;
/
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/releases/REL_Edge_Update_R55/R55.USADB.USA449.sql?revision=HEAD

--ADD new column to hold indicator for new parameter
--that only lets a MORE card go into a negative balance if 
--the 'offline' attribute is in the auth call
ALTER table CORP.CUSTOMER
ADD(ALLOW_NEG_BAL_ONLY_OFFLINE_IND char(1) DEFAULT 'N' NULL);

--SET New indicator to N by default for all existing customers
UPDATE CORP.CUSTOMER SET ALLOW_NEG_BAL_ONLY_OFFLINE_IND = 'N';

--Grant SELECT to inside auth role 
GRANT SELECT ON CORP.CUSTOMER TO USAT_IN_AUTH_LAYER_ROLE;

COMMIT; 
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/DEVICE/PKG_DEVICE_CONFIGURATION.pbk?revision=1.175
CREATE OR REPLACE PACKAGE BODY DEVICE.PKG_DEVICE_CONFIGURATION IS

	PROCEDURE SP_GET_FILE_TRANSFER_BLOB (
		pn_file_transfer_id file_transfer.file_transfer_id%TYPE,
		pbl_file_transfer_content OUT file_transfer.file_transfer_content%TYPE) 
	IS
	BEGIN
		SELECT file_transfer_content
		INTO pbl_file_transfer_content
		FROM device.file_transfer
		WHERE file_transfer_id = pn_file_transfer_id;
	END;
	
	PROCEDURE SP_GET_MAP_CONFIG_FILE (
		pn_device_id device.device_id%TYPE,
		pv_file_content_hex OUT VARCHAR2) 
	IS
		ln_device_id DEVICE.DEVICE_ID%TYPE;
	BEGIN
		SELECT MAX(dla.device_id)
		INTO ln_device_id
		FROM device.device d
		JOIN device.device_last_active dla ON d.device_name = dla.device_name
		WHERE d.device_id = pn_device_id;
	
		pv_file_content_hex := '';
		FOR rec IN (
			SELECT DS.DEVICE_SETTING_VALUE
			FROM DEVICE.DEVICE_SETTING DS
			JOIN DEVICE.DEVICE D ON DS.DEVICE_ID = D.DEVICE_ID
			JOIN DEVICE.CONFIG_TEMPLATE_SETTING CTS ON DECODE(D.DEVICE_TYPE_ID, PKG_CONST.DEVICE_TYPE__GX, PKG_CONST.DEVICE_TYPE__G4, D.DEVICE_TYPE_ID) = CTS.DEVICE_TYPE_ID
				AND DS.DEVICE_SETTING_PARAMETER_CD = CTS.DEVICE_SETTING_PARAMETER_CD
				AND CTS.FIELD_OFFSET BETWEEN 0 AND 511
			WHERE DS.DEVICE_ID = ln_device_id
			ORDER BY CTS.FIELD_OFFSET
		) LOOP
			pv_file_content_hex := pv_file_content_hex || rec.DEVICE_SETTING_VALUE;
		END LOOP;
	END;

	PROCEDURE ADD_DEFAULT_SETTINGS(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pn_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE,
		pn_device_sub_type_id DEVICE.DEVICE_SUB_TYPE_ID%TYPE,
		pv_app_type DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pn_propery_list_version NUMBER)
	IS
		lv_config_template_name CONFIG_TEMPLATE.CONFIG_TEMPLATE_NAME%TYPE;
		lt_possible_names VARCHAR2_TABLE;
		ln_config_template_id CONFIG_TEMPLATE.CONFIG_TEMPLATE_ID%TYPE;
		ln_pos PLS_INTEGER := 1;
	BEGIN
		lv_config_template_name := 'DEFAULT-CFG-' || pn_device_type_id;
		IF pn_device_sub_type_id IS NOT NULL THEN
			lv_config_template_name := lv_config_template_name || '-' || pn_device_sub_type_id;
		END IF;
		lt_possible_names := VARCHAR2_TABLE(lv_config_template_name);
		IF pv_app_type IS NOT NULL THEN
			LOOP
				ln_pos := INSTR(pv_app_type, '-', ln_pos);
				IF ln_pos < 1 THEN
					lv_config_template_name := lv_config_template_name || '-' || pv_app_type;
					lt_possible_names.EXTEND;
					lt_possible_names(lt_possible_names.LAST) := lv_config_template_name;
					EXIT;
				ELSE
					lt_possible_names.EXTEND;
					lt_possible_names(lt_possible_names.LAST) := lv_config_template_name || '-' || SUBSTR(pv_app_type, 1, ln_pos - 1);
					ln_pos := ln_pos + 1;
				END IF;
			END LOOP;
		END IF;
		IF pn_propery_list_version IS NOT NULL THEN
			lt_possible_names.EXTEND;
			lt_possible_names(lt_possible_names.LAST) := lv_config_template_name || '-' || pn_propery_list_version;
		END IF;
		SELECT MAX(CONFIG_TEMPLATE_ID)
		INTO ln_config_template_id
		FROM (
		SELECT CONFIG_TEMPLATE_ID
		FROM DEVICE.CONFIG_TEMPLATE
		WHERE CONFIG_TEMPLATE_NAME MEMBER OF lt_possible_names
		AND CONFIG_TEMPLATE_TYPE_ID = 1
		ORDER BY LENGTH(CONFIG_TEMPLATE_NAME) DESC)
		WHERE ROWNUM = 1;
		IF ln_config_template_id IS NOT NULL THEN
			INSERT INTO DEVICE.DEVICE_SETTING(DEVICE_ID, DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_VALUE)
				SELECT pn_device_id, CTS.DEVICE_SETTING_PARAMETER_CD, CTS.CONFIG_TEMPLATE_SETTING_VALUE
				FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS
				WHERE CTS.CONFIG_TEMPLATE_ID = ln_config_template_id
				AND NOT EXISTS(SELECT 1 FROM DEVICE.DEVICE_SETTING DS WHERE DS.DEVICE_ID = pn_device_id AND DS.DEVICE_SETTING_PARAMETER_CD = CTS.DEVICE_SETTING_PARAMETER_CD);
		END IF;
	END;

	PROCEDURE SP_UPSERT_KIOSK_CONFIG_UPDATE(
		pv_device_name DEVICE.DEVICE_NAME%TYPE,
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pv_kiosk_config_name ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE) 
	IS
		ln_file_transfer_id FILE_TRANSFER.FILE_TRANSFER_ID%TYPE;
		lb_file_transfer_content FILE_TRANSFER.FILE_TRANSFER_CONTENT%TYPE;
		ln_device_file_transfer_id DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_ID%TYPE;
		ln_required_fields NUMBER;
		ln_pending_file_transfer NUMBER;
		lv_line VARCHAR2(300);
		ln_device_id DEVICE.DEVICE_ID%TYPE;
		lv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE;
		ln_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE;
		ln_device_sub_type_id DEVICE.DEVICE_SUB_TYPE_ID%TYPE;
		lv_app_type APP_TYPE.APP_TYPE_VALUE%TYPE;
		ln_propery_list_version NUMBER;
	BEGIN
		SELECT MAX(DLA.DEVICE_ID), MAX(DLA.DEVICE_SERIAL_CD), MAX(D.DEVICE_TYPE_ID), MAX(D.DEVICE_SUB_TYPE_ID), MAX(DS_APP.DEVICE_SETTING_VALUE), MAX(TO_NUMBER_OR_NULL(DS_PLV.DEVICE_SETTING_VALUE))
		INTO ln_device_id, lv_device_serial_cd, ln_device_type_id, ln_device_sub_type_id, lv_app_type, ln_propery_list_version
		FROM DEVICE.DEVICE D
		JOIN DEVICE.DEVICE_LAST_ACTIVE DLA ON D.DEVICE_NAME = DLA.DEVICE_NAME
		LEFT JOIN DEVICE.DEVICE_SETTING DS_APP ON DLA.DEVICE_ID = DS_APP.DEVICE_ID AND DS_APP.DEVICE_SETTING_PARAMETER_CD = 'App Type'
		LEFT JOIN DEVICE.DEVICE_SETTING DS_PLV ON DLA.DEVICE_ID = DS_PLV.DEVICE_ID AND DS_PLV.DEVICE_SETTING_PARAMETER_CD = 'Property List Version'
		WHERE D.DEVICE_ID = pn_device_id;
	
		IF lv_app_type IS NOT NULL THEN
			ADD_DEFAULT_SETTINGS(ln_device_id, ln_device_type_id, ln_device_sub_type_id, lv_app_type, ln_propery_list_version);
		ELSIF ln_device_type_id != 11 OR ln_device_sub_type_id NOT IN(1,2) THEN
			-- Try to get app type from serial cd
			SELECT MAX(APP_TYPE_VALUE)
			INTO lv_app_type
			FROM (SELECT APP_TYPE_VALUE, APP_TYPE_ID
			FROM DEVICE.APP_TYPE
			WHERE DEVICE_TYPE_ID = ln_device_type_id
			AND DEVICE_SUB_TYPE_ID = ln_device_sub_type_id
			AND REGEXP_LIKE(lv_device_serial_cd, DEVICE_SERIAL_CD_REGEX)
			ORDER BY APP_TYPE_ID DESC)
			WHERE ROWNUM = 1;
			INSERT INTO DEVICE.DEVICE_SETTING(DEVICE_ID, DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_VALUE)
				VALUES(ln_device_id, 'App Type', lv_app_type);
			IF lv_app_type IS NOT NULL THEN
				ADD_DEFAULT_SETTINGS(ln_device_id, ln_device_type_id, ln_device_sub_type_id, lv_app_type, ln_propery_list_version);
			END IF;
		ELSE
			SELECT COUNT(1) INTO ln_required_fields
			FROM DEVICE.DEVICE_SETTING
			WHERE DEVICE_ID = ln_device_id
			AND DEVICE_SETTING_PARAMETER_CD IN ('SSN', 'VMC', 'EncKey')
			AND DECODE(DEVICE_SETTING_VALUE, NULL, 0, LENGTH(DEVICE_SETTING_VALUE)) > 6;
	
			IF ln_required_fields = 3 THEN
			BEGIN
				SELECT FILE_TRANSFER_ID
				INTO ln_file_transfer_id
				FROM (SELECT /*+index(FT IDX_FILE_TRANSFER_NAME)*/ FILE_TRANSFER_ID
				FROM DEVICE.FILE_TRANSFER FT
				WHERE FILE_TRANSFER_NAME = pv_device_name || '-CFG'
				AND FILE_TRANSFER_TYPE_CD = PKG_CONST.FILE_TYPE__CONFIG
				ORDER BY CREATED_TS)
				WHERE ROWNUM = 1;
			EXCEPTION WHEN NO_DATA_FOUND THEN
				SELECT seq_file_transfer_id.nextval INTO ln_file_transfer_id FROM DUAL;
				INSERT INTO DEVICE.FILE_TRANSFER(FILE_TRANSFER_ID, FILE_TRANSFER_NAME, FILE_TRANSFER_TYPE_CD)
				VALUES(ln_file_transfer_id, pv_device_name || '-CFG', PKG_CONST.FILE_TYPE__CONFIG);
			END;
	
			UPDATE DEVICE.FILE_TRANSFER
			SET FILE_TRANSFER_CONTENT = EMPTY_BLOB()
			WHERE FILE_TRANSFER_ID = ln_file_transfer_id;
	
			SELECT FILE_TRANSFER_CONTENT
			INTO lb_file_transfer_content
			FROM DEVICE.FILE_TRANSFER
			WHERE FILE_TRANSFER_ID = ln_file_transfer_id
			FOR UPDATE;
	
			FOR rec IN (
				SELECT DS.DEVICE_SETTING_PARAMETER_CD, DS.DEVICE_SETTING_VALUE
				FROM DEVICE.DEVICE_SETTING DS
				JOIN DEVICE.DEVICE_SETTING_PARAMETER DSP ON DS.DEVICE_SETTING_PARAMETER_CD = DSP.DEVICE_SETTING_PARAMETER_CD
				LEFT OUTER JOIN DEVICE.SETTING_PARAMETER_TYPE SPT ON DSP.SETTING_PARAMETER_TYPE_ID = SPT.SETTING_PARAMETER_TYPE_ID
				WHERE DS.DEVICE_ID = ln_device_id AND (SPT.SETTING_PARAMETER_TYPE_DESC IS NULL OR SPT.SETTING_PARAMETER_TYPE_DESC != 'SERVER_ONLY_DEVICE_SETTING')
				ORDER BY DS.FILE_ORDER
			) LOOP
				lv_line := rec.device_setting_parameter_cd || '=' || rec.device_setting_value || PKG_CONST.ASCII__LF;
				dbms_lob.writeappend(lb_file_transfer_content, LENGTH(lv_line), UTL_RAW.CAST_TO_RAW(lv_line));
			END LOOP;
	
			UPDATE ENGINE.MACHINE_CMD_PENDING
			SET EXECUTE_CD = 'A'
			WHERE MACHINE_ID = pv_device_name
			AND DATA_TYPE = '9B'
			AND COMMAND = UPPER(pv_kiosk_config_name);
	
			SELECT COUNT(1)
			INTO ln_pending_file_transfer
			FROM DEVICE.DEVICE_FILE_TRANSFER DFT, ENGINE.MACHINE_CMD_PENDING MCP
			WHERE DFT.DEVICE_ID = ln_device_id
			AND DFT.FILE_TRANSFER_ID = ln_file_transfer_id
			AND DFT.DEVICE_FILE_TRANSFER_STATUS_CD = 0
			AND DFT.DEVICE_FILE_TRANSFER_DIRECT = 'O'
			AND MCP.MACHINE_ID = pv_device_name
			AND MCP.DATA_TYPE = 'A4'
			AND DFT.DEVICE_FILE_TRANSFER_ID = MCP.DEVICE_FILE_TRANSFER_ID;
	
			IF ln_pending_file_transfer = 0 THEN
				SELECT SEQ_DEVICE_FILE_TRANSFER_ID.NEXTVAL INTO ln_device_file_transfer_id FROM DUAL;
				INSERT INTO DEVICE.DEVICE_FILE_TRANSFER (
					DEVICE_FILE_TRANSFER_ID,
					DEVICE_ID,
					FILE_TRANSFER_ID,
					DEVICE_FILE_TRANSFER_DIRECT,
					DEVICE_FILE_TRANSFER_STATUS_CD,
					DEVICE_FILE_TRANSFER_PKT_SIZE) 
				VALUES (
					ln_device_file_transfer_id,
					ln_device_id,
					ln_file_transfer_id,
					'O',
					0,
					1024);
	
				INSERT INTO ENGINE.MACHINE_CMD_PENDING (
					MACHINE_ID,
					DATA_TYPE,
					DEVICE_FILE_TRANSFER_ID,
					EXECUTE_CD,
					EXECUTE_ORDER)
				VALUES (
					pv_device_name,
					'A4',
					ln_device_file_transfer_id,
					'P',
					1);
				END IF;
			END IF;
		END IF;
	END;
	
	-- IS_DEVICE_SETTING_IN_CONFIG is deprecated in R30
	FUNCTION IS_DEVICE_SETTING_IN_CONFIG(
		pn_device_id IN device.device_id%TYPE,
		pv_device_setting_parameter_cd IN device_setting.device_setting_parameter_cd%TYPE,
		pv_device_setting_value IN device_setting.device_setting_value%TYPE
	)
	RETURN VARCHAR
	IS
	BEGIN
		RETURN 'Y';
	END;
	
		FUNCTION GET_DEVICE_SETTING (
			pn_device_id DEVICE.DEVICE_ID%TYPE,
			pv_device_setting_parameter_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE
		) RETURN DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE
		IS
			lv_device_setting_value DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
		BEGIN
			SELECT DS.DEVICE_SETTING_VALUE
			INTO lv_device_setting_value
			FROM DEVICE.DEVICE D
			JOIN DEVICE.DEVICE_LAST_ACTIVE DLA ON D.DEVICE_NAME = DLA.DEVICE_NAME
			JOIN DEVICE.DEVICE_SETTING DS ON DLA.DEVICE_ID = DS.DEVICE_ID
			WHERE D.DEVICE_ID = pn_device_id
				AND DS.DEVICE_SETTING_PARAMETER_CD = pv_device_setting_parameter_cd;
	
			RETURN lv_device_setting_value;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				RETURN NULL;
		END;
	
	PROCEDURE SP_UPDATE_DEVICE_SETTING (
		pn_device_id IN device.device_id%TYPE,
		pv_device_setting_parameter_cd IN device_setting.device_setting_parameter_cd%TYPE,
		pv_device_setting_value IN device_setting.device_setting_value%TYPE,
		pc_only_greater_flag IN CHAR DEFAULT 'N')
	IS
		ln_exists NUMBER;
	BEGIN
		SP_UPSERT_DEVICE_SETTING(pn_device_id, pv_device_setting_parameter_cd, pv_device_setting_value, NULL, ln_exists, pc_only_greater_flag, NULL);
	END;
	
	PROCEDURE SP_UPSERT_DEVICE_SETTING(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pv_device_setting_parameter_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_device_setting_value DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pn_file_order DEVICE_SETTING.FILE_ORDER%TYPE,
		pn_exists OUT NUMBER,
		pc_only_greater_flag IN CHAR DEFAULT 'N',
		pv_changed_by IN DEVICE_SETTING.CHANGED_BY%TYPE DEFAULT NULL)
	IS
		ln_count NUMBER;
		ln_device_id DEVICE.DEVICE_ID%TYPE;
	BEGIN
		SELECT MAX(dla.device_id)
		INTO ln_device_id
		FROM device.device d
		JOIN device.device_last_active dla ON d.device_name = dla.device_name
		WHERE d.device_id = pn_device_id;
	
		SELECT COUNT(1) INTO ln_count
		FROM device.device_setting_parameter
		WHERE device_setting_parameter_cd = pv_device_setting_parameter_cd;
	
		IF ln_count = 0 THEN
			BEGIN
				INSERT INTO device.device_setting_parameter(device_setting_parameter_cd)
				VALUES(pv_device_setting_parameter_cd);
			EXCEPTION
				WHEN DUP_VAL_ON_INDEX THEN
					NULL;
			END;
		END IF;
	
		UPDATE device.device_setting
		SET device_setting_value =
			CASE
				WHEN pc_only_greater_flag IS NULL OR pc_only_greater_flag != 'Y' OR TO_NUMBER_OR_NULL(pv_device_setting_value) > TO_NUMBER_OR_NULL(NVL(device_setting_value, '0')) THEN pv_device_setting_value
				ELSE device_setting_value
			END,
			changed_by = pv_changed_by
		WHERE device_id = ln_device_id
			AND device_setting_parameter_cd = pv_device_setting_parameter_cd;
	
		IF SQL%NOTFOUND THEN
			pn_exists := 0;
			BEGIN
				INSERT INTO device.device_setting(device_id, device_setting_parameter_cd, device_setting_value, file_order, changed_by)
				VALUES(ln_device_id, pv_device_setting_parameter_cd, pv_device_setting_value, pn_file_order, pv_changed_by);
			EXCEPTION
				WHEN DUP_VAL_ON_INDEX THEN
					SP_UPSERT_DEVICE_SETTING(ln_device_id, pv_device_setting_parameter_cd, pv_device_setting_value, pn_file_order, pn_exists, pc_only_greater_flag, pv_changed_by);
			END;
		ELSE
			pn_exists := 1;
		END IF;
	END;
	
	PROCEDURE SP_UPSERT_DEVICE_SETTING (
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pv_device_setting_parameter_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_device_setting_value DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pn_exists OUT NUMBER) 
	IS
	BEGIN
		SP_UPSERT_DEVICE_SETTING(pn_device_id, pv_device_setting_parameter_cd, pv_device_setting_value, NULL, pn_exists);
	END;
	
	PROCEDURE SP_UPSERT_CFG_TEMPLATE_SETTING (
		pn_config_template_id CONFIG_TEMPLATE_SETTING.CONFIG_TEMPLATE_ID%TYPE,
		pv_device_setting_parameter_cd CONFIG_TEMPLATE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_config_template_setting_val CONFIG_TEMPLATE_SETTING.CONFIG_TEMPLATE_SETTING_VALUE%TYPE,
		pn_exists OUT NUMBER) 
	IS
		ln_count NUMBER;
	BEGIN
		SELECT COUNT(1) INTO ln_count
		FROM device.device_setting_parameter
		WHERE device_setting_parameter_cd = pv_device_setting_parameter_cd;
	
		IF ln_count = 0 THEN
			BEGIN
				INSERT INTO device.device_setting_parameter(device_setting_parameter_cd)
				VALUES(pv_device_setting_parameter_cd);
			EXCEPTION
				WHEN DUP_VAL_ON_INDEX THEN
					NULL;
			END;
		END IF;
	
		UPDATE device.config_template_setting
		SET config_template_setting_value = pv_config_template_setting_val
		WHERE config_template_id = pn_config_template_id
			AND device_setting_parameter_cd = pv_device_setting_parameter_cd;
	
		IF SQL%NOTFOUND THEN
			pn_exists := 0;
			BEGIN
				INSERT INTO device.config_template_setting(config_template_id, device_setting_parameter_cd, config_template_setting_value)
				VALUES(pn_config_template_id, pv_device_setting_parameter_cd, pv_config_template_setting_val);
			EXCEPTION
				WHEN DUP_VAL_ON_INDEX THEN
					SP_UPSERT_CFG_TEMPLATE_SETTING(pn_config_template_id, pv_device_setting_parameter_cd, pv_config_template_setting_val, pn_exists);
			END;
		ELSE
			pn_exists := 1;
		END IF;
	END;
	
	PROCEDURE SP_UPDATE_DEVICE_SETTINGS (
		pn_device_id IN device.device_id%TYPE,
		pn_file_transfer_id IN file_transfer.file_transfer_id%TYPE,
		pn_result_cd OUT NUMBER,
		pv_error_message OUT VARCHAR2,
		pn_setting_count OUT NUMBER) 
	IS
		ll_file_transfer_content file_transfer.file_transfer_content%TYPE;
		ln_pos NUMBER := 1;
		ln_content_size NUMBER := 2000;
		ln_content_pos NUMBER := 1;
		ls_content VARCHAR2(4000);
		ls_record VARCHAR2(4000);
		ls_param device_setting.device_setting_parameter_cd%TYPE;
		ls_value VARCHAR2(4000);
		ln_return NUMBER := 0;
		ln_file_transfer_type_cd file_transfer.file_transfer_type_cd%TYPE;
		lv_file_transfer_name file_transfer.file_transfer_name%TYPE;
		ln_file_order device_setting.file_order%TYPE := 0;
		ln_config_template_id CONFIG_TEMPLATE.CONFIG_TEMPLATE_ID%TYPE;
		ln_device_id DEVICE.DEVICE_ID%TYPE;
	BEGIN
		pn_result_cd := PKG_CONST.RESULT__FAILURE;
		pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;
	
		SELECT MAX(dla.device_id)
		INTO ln_device_id
		FROM device.device d
		JOIN device.device_last_active dla ON d.device_name = dla.device_name
		WHERE d.device_id = pn_device_id;
	
		SELECT file_transfer_type_cd, file_transfer_content, file_transfer_name
		INTO ln_file_transfer_type_cd, ll_file_transfer_content, lv_file_transfer_name
		FROM device.file_transfer
		WHERE file_transfer_id = pn_file_transfer_id;
	
		IF ln_file_transfer_type_cd = PKG_CONST.FILE_TYPE__CONFIG THEN
			DELETE FROM device.device_setting
			WHERE device_id = ln_device_id
				AND device_setting_parameter_cd IN (
					SELECT device_setting_parameter_cd
					FROM device.device_setting_parameter
					WHERE device_setting_ui_configurable = 'Y');
		ELSE
			SELECT config_template_id
			INTO ln_config_template_id
			FROM device.config_template
			WHERE config_template_name = lv_file_transfer_name;
	
			DELETE FROM device.config_template_setting
			WHERE config_template_id = ln_config_template_id;
		END IF;
	
		LOOP
			ls_content := ls_content || utl_raw.cast_to_varchar2(HEXTORAW(DBMS_LOB.SUBSTR(ll_file_transfer_content, ln_content_size, ln_content_pos)));
			ln_content_pos := ln_content_pos + ln_content_size;
	
			ln_pos := NVL(INSTR(ls_content, PKG_CONST.ASCII__LF), 0);
			IF ln_pos = 0 OR NVL(INSTR(ls_content, '='), 0) = 0 THEN
				ln_return := 1;
			END IF;
	
			LOOP
				ln_pos := NVL(INSTR(ls_content, PKG_CONST.ASCII__LF), 0);
				IF ln_pos = 0 AND ln_return = 0 THEN
					EXIT;
				END IF;
	
				IF ln_return = 0 THEN
					ls_record := SUBSTR(ls_content, 1, ln_pos - 1);
					ls_content := SUBSTR(ls_content, ln_pos + 1);
				ELSE
					ls_record := ls_content;
				END IF;
	
				ln_pos := NVL(INSTR(ls_record, '='), 0);
				IF ln_pos = 0 AND ln_return = 0 THEN
					EXIT;
				END IF;
	
				ls_param := TRIM(SUBSTR(ls_record, 1, ln_pos - 1));
				ls_value := REPLACE(TRIM(SUBSTR(ls_record, ln_pos + 1, LENGTH(ls_record) - ln_pos)), CHR(13), '');
				IF LENGTH(ls_value) > 200 THEN
					ls_value := NULL;
				END IF;
	
				IF NVL(LENGTH(ls_param), 0) > 0 THEN
					BEGIN
						INSERT INTO device.device_setting_parameter(device_setting_parameter_cd)
						SELECT ls_param FROM dual
						WHERE NOT EXISTS (
							SELECT 1 FROM device.device_setting_parameter
							WHERE device_setting_parameter_cd = ls_param);
					EXCEPTION
						WHEN DUP_VAL_ON_INDEX THEN
							NULL;
					END;
	
					ln_file_order := ln_file_order + 1;
	
					BEGIN
						IF ln_file_transfer_type_cd = PKG_CONST.FILE_TYPE__CONFIG THEN
							INSERT INTO device.device_setting(device_id, device_setting_parameter_cd, device_setting_value, file_order)
							VALUES(ln_device_id, ls_param, ls_value, ln_file_order);
						ELSE
							INSERT INTO device.config_template_setting(config_template_id, device_setting_parameter_cd, config_template_setting_value, file_order)
							VALUES(ln_config_template_id, ls_param, ls_value, ln_file_order);
						END IF;
					EXCEPTION
						WHEN DUP_VAL_ON_INDEX THEN
							NULL;
					END;
				END IF;
	
				IF ln_return = 1 THEN
					IF ln_file_transfer_type_cd = PKG_CONST.FILE_TYPE__CONFIG THEN
						SELECT COUNT(1)
						INTO pn_setting_count
						FROM device.device_setting
						WHERE device_id = ln_device_id
							AND device_setting_parameter_cd IN (
								SELECT device_setting_parameter_cd
								FROM device.device_setting_parameter
								WHERE device_setting_ui_configurable = 'Y');
					ELSE
						SELECT COUNT(1)
						INTO pn_setting_count
						FROM device.config_template_setting
						WHERE config_template_id = ln_config_template_id
							AND device_setting_parameter_cd IN (
								SELECT device_setting_parameter_cd
								FROM device.device_setting_parameter
								WHERE device_setting_ui_configurable = 'Y');
					END IF;
	
					pn_result_cd := PKG_CONST.RESULT__SUCCESS;
					pv_error_message := PKG_CONST.ERROR__NO_ERROR;
					RETURN;
				END IF;
			END LOOP;
	
		END LOOP;
	END;
	
	PROCEDURE SP_UPDATE_CFG_TMPL_SETTINGS (
		pn_file_transfer_id IN file_transfer.file_transfer_id%TYPE,
		pn_result_cd OUT NUMBER,
		pv_error_message OUT VARCHAR2,
		pn_setting_count OUT NUMBER) 
	IS
	BEGIN
		SP_UPDATE_DEVICE_SETTINGS(0, pn_file_transfer_id, pn_result_cd, pv_error_message, pn_setting_count);
	END;
	
	PROCEDURE SP_GET_HOST_BY_PORT_NUM (
		pv_device_name IN device.device_name%TYPE,
		pn_host_port_num IN host.host_port_num%TYPE,
		pt_utc_ts IN TIMESTAMP,
		pn_host_id OUT host.host_id%TYPE) 
	IS
	BEGIN
		SELECT host_id INTO pn_host_id FROM (
			SELECT host_id
			FROM device.host h
			INNER JOIN device.device d ON h.device_id = d.device_id
			WHERE d.device_name = pv_device_name
				AND h.host_port_num = pn_host_port_num
				AND SYS_EXTRACT_UTC(CAST(h.created_ts AS TIMESTAMP)) <= pt_utc_ts
			ORDER BY h.created_ts DESC
		) WHERE ROWNUM = 1;
	END;
	
	PROCEDURE SP_GET_HOST (
		pn_device_id IN device.device_id%TYPE,
		pv_device_name IN device.device_name%TYPE,
		pn_host_port_num IN host.host_port_num%TYPE,
		pt_utc_ts IN TIMESTAMP,
		pn_result_cd OUT NUMBER,
		pv_error_message OUT VARCHAR2,
		pn_host_id OUT host.host_id%TYPE) 
	IS
		ln_new_host_count NUMBER;
		ln_device_id DEVICE.DEVICE_ID%TYPE;
	BEGIN
		pn_result_cd := PKG_CONST.RESULT__FAILURE;
		pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;
	
		SELECT MAX(dla.device_id)
		INTO ln_device_id
		FROM device.device d
		JOIN device.device_last_active dla ON d.device_name = dla.device_name
		WHERE d.device_id = pn_device_id;
	
		BEGIN
			sp_get_host_by_port_num(pv_device_name, pn_host_port_num, pt_utc_ts, pn_host_id);
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				BEGIN
					-- fail over to the base host
					sp_get_host_by_port_num(pv_device_name, 0, pt_utc_ts, pn_host_id);
				EXCEPTION
					WHEN NO_DATA_FOUND THEN
						BEGIN
							sp_create_default_hosts(ln_device_id, ln_new_host_count, pn_result_cd, pv_error_message);
							IF pn_result_cd != PKG_CONST.RESULT__SUCCESS THEN
								RETURN;
							END IF;
	
							sp_get_host_by_port_num(pv_device_name, 0, SYS_EXTRACT_UTC(SYSTIMESTAMP), pn_host_id);
						EXCEPTION
							WHEN NO_DATA_FOUND THEN
								pn_result_cd := PKG_CONST.RESULT__HOST_NOT_FOUND;
								pv_error_message := 'Unable to find host for device_name: ' || pv_device_name || ', host_port_num: ' || pn_host_port_num;
								RETURN;
						END;
				END;
		END;
	
		pn_result_cd := PKG_CONST.RESULT__SUCCESS;
		pv_error_message := PKG_CONST.ERROR__NO_ERROR;
	END;
	
	PROCEDURE SP_CREATE_DEFAULT_HOSTS (
		pn_device_id IN device.device_id%TYPE,
		pn_new_host_count OUT NUMBER,
		pn_result_cd OUT NUMBER,
		pv_error_message OUT VARCHAR2) 
	IS
		ln_base_host_count NUMBER;
		ln_other_host_count NUMBER;
		lv_device_serial_cd device.device_serial_cd%TYPE;
		ln_device_type_id device.device_type_id%TYPE;
		ln_def_base_host_type_id device_type.def_base_host_type_id%TYPE;
		ln_def_base_host_equipment_id device_type.def_base_host_equipment_id%TYPE;
		ln_def_prim_host_type_id device_type.def_prim_host_type_id%TYPE;
		ln_def_prim_host_equipment_id device_type.def_prim_host_equipment_id%TYPE;
		ln_device_id DEVICE.DEVICE_ID%TYPE;
	BEGIN
		pn_result_cd := PKG_CONST.RESULT__FAILURE;
		pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;
		pn_new_host_count := 0;
	
		SELECT MAX(dla.device_id)
		INTO ln_device_id
		FROM device.device d
		JOIN device.device_last_active dla ON d.device_name = dla.device_name
		WHERE d.device_id = pn_device_id;
	
		SELECT NVL(SUM(CASE WHEN HTGT.HOST_TYPE_ID IS NOT NULL OR H.HOST_PORT_NUM = 0 THEN 1 ELSE 0 END), 0), NVL(SUM(CASE WHEN HTGT.HOST_TYPE_ID IS NOT NULL OR H.HOST_PORT_NUM = 0 THEN 0 ELSE 1 END), 0)
		  INTO ln_base_host_count, ln_other_host_count
		  FROM DEVICE.HOST H
		  LEFT OUTER JOIN (DEVICE.HOST_TYPE_HOST_GROUP_TYPE HTGT
		  JOIN DEVICE.HOST_GROUP_TYPE GT ON HTGT.HOST_GROUP_TYPE_ID = GT.HOST_GROUP_TYPE_ID)
			ON H.HOST_TYPE_ID = HTGT.HOST_TYPE_ID
		   AND GT.HOST_GROUP_TYPE_CD = 'BASE_HOST'
		 WHERE H.DEVICE_ID = H.DEVICE_ID
		   AND H.DEVICE_ID = ln_device_id;
	
		-- if exists both a base and another host, then there's nothing to do here
		IF ln_base_host_count > 0 AND ln_other_host_count > 0 THEN
			pn_result_cd := PKG_CONST.RESULT__SUCCESS;
			pv_error_message := PKG_CONST.ERROR__NO_ERROR;
			RETURN;
		END IF;
	
		SELECT
			D.DEVICE_SERIAL_CD,
			D.DEVICE_TYPE_ID,
			DECODE(DST.DEVICE_SUB_TYPE_ID, NULL, DT.DEF_BASE_HOST_TYPE_ID, DST.DEF_BASE_HOST_TYPE_ID),
			DECODE(DST.DEVICE_SUB_TYPE_ID, NULL, DT.DEF_BASE_HOST_EQUIPMENT_ID, DST.DEF_BASE_HOST_EQUIPMENT_ID),
			DECODE(DST.DEVICE_SUB_TYPE_ID, NULL, DT.DEF_PRIM_HOST_TYPE_ID, DST.DEF_PRIM_HOST_TYPE_ID),
			DECODE(DST.DEVICE_SUB_TYPE_ID, NULL, DT.DEF_PRIM_HOST_EQUIPMENT_ID, DST.DEF_PRIM_HOST_EQUIPMENT_ID)
		INTO
			lv_device_serial_cd,
			ln_device_type_id,
			ln_def_base_host_type_id,
			ln_def_base_host_equipment_id,
			ln_def_prim_host_type_id,
			ln_def_prim_host_equipment_id
		FROM DEVICE.DEVICE D
		JOIN DEVICE.DEVICE_TYPE DT ON D.DEVICE_TYPE_ID = DT.DEVICE_TYPE_ID
		LEFT OUTER JOIN DEVICE.DEVICE_SUB_TYPE DST ON D.DEVICE_TYPE_ID = DST.DEVICE_TYPE_ID AND D.DEVICE_SUB_TYPE_ID = DST.DEVICE_SUB_TYPE_ID
		WHERE D.DEVICE_ID = ln_device_id;
	
		-- Create base host if it doesn't exist
		IF ln_base_host_count = 0 AND ln_def_base_host_type_id IS NOT NULL THEN
			INSERT INTO device.host
			(
				host_status_cd,
				host_serial_cd,
				host_est_complete_minut,
				host_port_num,
				host_setting_updated_yn_flag,
				device_id,
				host_position_num,
				host_active_yn_flag,
				host_type_id,
				host_equipment_id
			)
			VALUES
			(
				0,
				lv_device_serial_cd,
				0,
				0,
				'N',
				ln_device_id,
				0,
				'Y',
				ln_def_base_host_type_id,
				ln_def_base_host_equipment_id
			);
	
			pn_new_host_count := pn_new_host_count + 1;
		END IF;
	
		-- Create primary host if no other hosts exist
		IF ln_other_host_count = 0 AND ln_def_prim_host_type_id IS NOT NULL THEN
			INSERT INTO device.host
			(
				host_status_cd,
				host_est_complete_minut,
				host_port_num,
				host_setting_updated_yn_flag,
				device_id,
				host_position_num,
				host_active_yn_flag,
				host_type_id,
				host_equipment_id
			)
			VALUES
			(
				0,
				0,
				1,
				'N',
				ln_device_id,
				0,
				'Y',
				ln_def_prim_host_type_id,
				ln_def_prim_host_equipment_id
			);
	
			pn_new_host_count := pn_new_host_count + 1;
		END IF;
	
		pn_result_cd := PKG_CONST.RESULT__SUCCESS;
		pv_error_message := PKG_CONST.ERROR__NO_ERROR;
	END;

	FUNCTION GET_OR_CREATE_HOST_EQUIPMENT(
		pv_host_equipment_mfgr IN HOST_EQUIPMENT.HOST_EQUIPMENT_MFGR%TYPE,
		pv_host_equipment_model IN HOST_EQUIPMENT.HOST_EQUIPMENT_MODEL%TYPE,
		pn_existing_cnt OUT PLS_INTEGER)
		RETURN HOST_EQUIPMENT.HOST_EQUIPMENT_ID%TYPE
	IS
		ln_host_equipment_id HOST_EQUIPMENT.HOST_EQUIPMENT_ID%TYPE;
	BEGIN
		SELECT MAX(HOST_EQUIPMENT_ID), 1
		  INTO ln_host_equipment_id, pn_existing_cnt
		  FROM DEVICE.HOST_EQUIPMENT
		 WHERE HOST_EQUIPMENT_MFGR = pv_host_equipment_mfgr
		   AND HOST_EQUIPMENT_MODEL = pv_host_equipment_model;
		IF ln_host_equipment_id IS NULL THEN
			SELECT SEQ_HOST_EQUIPMENT_ID.NEXTVAL, 0
			  INTO ln_host_equipment_id, pn_existing_cnt
			  FROM DUAL;
			INSERT INTO DEVICE.HOST_EQUIPMENT(HOST_EQUIPMENT_ID, HOST_EQUIPMENT_MFGR, HOST_EQUIPMENT_MODEL)
				 VALUES(ln_host_equipment_id, pv_host_equipment_mfgr, pv_host_equipment_model);
		END IF;
		RETURN ln_host_equipment_id;
	EXCEPTION
		WHEN DUP_VAL_ON_INDEX THEN
			RETURN GET_OR_CREATE_HOST_EQUIPMENT(pv_host_equipment_mfgr, pv_host_equipment_model, pn_existing_cnt);
	END;

	PROCEDURE UPSERT_HOST(
		pn_device_id IN DEVICE.DEVICE_ID%TYPE,
		pn_host_port_num IN HOST.HOST_PORT_NUM%TYPE,
		pn_host_position_num IN HOST.HOST_POSITION_NUM%TYPE,
		pn_host_type_id IN HOST.HOST_TYPE_ID%TYPE,
		pv_host_serial_cd IN HOST.HOST_SERIAL_CD%TYPE,
		pv_host_label_cd IN HOST.HOST_LABEL_CD%TYPE,
		pn_host_equipment_id HOST_EQUIPMENT.HOST_EQUIPMENT_ID%TYPE,
		pn_host_est_complete_minut IN HOST.HOST_EST_COMPLETE_MINUT%TYPE,
		pn_host_id OUT HOST.HOST_ID%TYPE,
		pn_existing_cnt OUT PLS_INTEGER)
	IS
		ln_device_id DEVICE.DEVICE_ID%TYPE;
	BEGIN
		SELECT MAX(dla.device_id)
		INTO ln_device_id
		FROM device.device d
		JOIN device.device_last_active dla ON d.device_name = dla.device_name
		WHERE d.device_id = pn_device_id;

		UPDATE DEVICE.HOST
		   SET (HOST_TYPE_ID, HOST_SERIAL_CD, HOST_LABEL_CD, HOST_EQUIPMENT_ID, HOST_EST_COMPLETE_MINUT, HOST_ACTIVE_YN_FLAG) =
			   (SELECT pn_host_type_id, pv_host_serial_cd, pv_host_label_cd, pn_host_equipment_id, pn_host_est_complete_minut, 'Y' FROM DUAL)
		 WHERE DEVICE_ID = ln_device_id
		   AND HOST_PORT_NUM = pn_host_port_num
		   AND HOST_POSITION_NUM = pn_host_position_num
		   RETURNING HOST_ID, 1 INTO pn_host_id, pn_existing_cnt;
		IF pn_host_id IS NULL THEN
			SELECT SEQ_HOST_ID.NEXTVAL, 0
			  INTO pn_host_id, pn_existing_cnt
			  FROM DUAL;
			INSERT INTO DEVICE.HOST(HOST_ID, DEVICE_ID, HOST_PORT_NUM, HOST_POSITION_NUM, HOST_TYPE_ID, HOST_SERIAL_CD, HOST_LABEL_CD, HOST_EQUIPMENT_ID, HOST_EST_COMPLETE_MINUT, HOST_SETTING_UPDATED_YN_FLAG, HOST_ACTIVE_YN_FLAG)
				 VALUES(pn_host_id, ln_device_id, pn_host_port_num, pn_host_position_num, pn_host_type_id, pv_host_serial_cd, pv_host_label_cd, pn_host_equipment_id, pn_host_est_complete_minut, 'N', 'Y');
		END IF;
	EXCEPTION
		WHEN DUP_VAL_ON_INDEX THEN
			UPSERT_HOST(pn_device_id, pn_host_port_num, pn_host_position_num, pn_host_type_id, pv_host_serial_cd, pv_host_label_cd, pn_host_equipment_id, pn_host_est_complete_minut, pn_host_id, pn_existing_cnt);
	END;

	PROCEDURE UPSERT_HOST(
		pn_device_id IN DEVICE.DEVICE_ID%TYPE,
		pn_host_port_num IN HOST.HOST_PORT_NUM%TYPE,
		pn_host_position_num IN HOST.HOST_POSITION_NUM%TYPE,
		pn_host_type_id IN HOST.HOST_TYPE_ID%TYPE,
		pv_host_serial_cd IN HOST.HOST_SERIAL_CD%TYPE,
		pv_host_label_cd IN HOST.HOST_LABEL_CD%TYPE,
		pv_host_equipment_mfgr IN HOST_EQUIPMENT.HOST_EQUIPMENT_MFGR%TYPE,
		pv_host_equipment_model IN HOST_EQUIPMENT.HOST_EQUIPMENT_MODEL%TYPE,
		pn_host_est_complete_minut IN HOST.HOST_EST_COMPLETE_MINUT%TYPE,
		pn_host_id OUT HOST.HOST_ID%TYPE,
		pn_existing_cnt OUT PLS_INTEGER)
	IS
		ln_host_equipment_existing_cnt PLS_INTEGER;
	BEGIN
		UPSERT_HOST(pn_device_id, pn_host_port_num, pn_host_position_num, pn_host_type_id, pv_host_serial_cd, pv_host_label_cd, GET_OR_CREATE_HOST_EQUIPMENT(pv_host_equipment_mfgr, pv_host_equipment_model, ln_host_equipment_existing_cnt), pn_host_est_complete_minut, pn_host_id, pn_existing_cnt);
	END;

	PROCEDURE UPSERT_HOST_SETTING(
		pn_host_id HOST.HOST_ID%TYPE,
		pv_host_setting_parameter_cd HOST_SETTING.HOST_SETTING_PARAMETER%TYPE,
		pv_host_setting_value HOST_SETTING.HOST_SETTING_VALUE%TYPE)
	IS
	BEGIN
		UPDATE DEVICE.HOST_SETTING
		   SET HOST_SETTING_VALUE = pv_host_setting_value
		 WHERE HOST_ID = pn_host_id
		   AND HOST_SETTING_PARAMETER = pv_host_setting_parameter_cd;
		IF SQL%NOTFOUND THEN
			INSERT INTO DEVICE.HOST_SETTING(HOST_ID, HOST_SETTING_PARAMETER, HOST_SETTING_VALUE)
				VALUES(pn_host_id, pv_host_setting_parameter_cd, pv_host_setting_value);
	END IF;
	EXCEPTION
		WHEN DUP_VAL_ON_INDEX THEN
			UPSERT_HOST_SETTING(pn_host_id, pv_host_setting_parameter_cd, pv_host_setting_value);
	END;

	PROCEDURE UPSERT_HOST_SETTING(
		pn_host_id HOST.HOST_ID%TYPE,
		pv_host_setting_parameter_cd HOST_SETTING.HOST_SETTING_PARAMETER%TYPE,
		pv_host_setting_value HOST_SETTING.HOST_SETTING_VALUE%TYPE,
		pv_old_host_setting_value OUT HOST_SETTING.HOST_SETTING_VALUE%TYPE)
	IS
	BEGIN
		SELECT MAX(HOST_SETTING_VALUE)
		  INTO pv_old_host_setting_value
		  FROM DEVICE.HOST_SETTING
		 WHERE HOST_ID = pn_host_id
		   AND HOST_SETTING_PARAMETER = pv_host_setting_parameter_cd;
		UPSERT_HOST_SETTING(pn_host_id, pv_host_setting_parameter_cd, pv_host_setting_value);
	END;

	PROCEDURE UPDATE_COMM_STATS(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pn_rssi NUMBER,
		pn_ber NUMBER,
		pd_update_ts GPRS_DEVICE.RSSI_TS%TYPE,
		pv_modem_info GPRS_DEVICE.MODEM_INFO%TYPE)
	IS
		ln_host_id HOST.HOST_ID%TYPE;
		ln_device_id DEVICE.DEVICE_ID%TYPE;
		lv_device_name DEVICE.DEVICE_NAME%TYPE;
	BEGIN
		SELECT MAX(dla.device_id), MAX(D.DEVICE_NAME)
		INTO ln_device_id, lv_device_name
		FROM device.device d
		JOIN device.device_last_active dla ON d.device_name = dla.device_name
		WHERE d.device_id = pn_device_id;

		UPDATE DEVICE.DEVICE_DATA
		   SET RSSI = pn_rssi,
			   RSSI_TS = pd_update_ts
		 WHERE DEVICE_NAME = lv_device_name
		   AND (RSSI_TS < pd_update_ts OR RSSI_TS is null);

		IF SQL%NOTFOUND THEN
			BEGIN
				INSERT INTO DEVICE.DEVICE_DATA(DEVICE_NAME, RSSI, RSSI_TS)
					VALUES(lv_device_name, pn_rssi, pd_update_ts);
			EXCEPTION
				WHEN DUP_VAL_ON_INDEX THEN
					NULL;
			END;
		END IF;
		ENGINE.PKG_DATA_SYNC.SP_CREATE_DATA_SYNC('DEVICE_INFO', 'DEVICE.DEVICE_SETTING', 'U', pn_device_id, lv_device_name, NULL);

		UPDATE DEVICE.GPRS_DEVICE GD
		SET DEVICE_ID = NULL,
			GPRS_DEVICE_STATE_ID = DECODE(GPRS_DEVICE_STATE_ID, 5, 4, GPRS_DEVICE_STATE_ID)
		WHERE DEVICE_ID = ln_device_id
			AND ICCID NOT IN(SELECT TO_NUMBER_OR_NULL(H.HOST_SERIAL_CD)
		  FROM DEVICE.HOST H
		  JOIN DEVICE.HOST_TYPE_HOST_GROUP_TYPE HTGT ON H.HOST_TYPE_ID = HTGT.HOST_TYPE_ID
		 WHERE DEVICE_ID = GD.DEVICE_ID
		   AND HTGT.HOST_GROUP_TYPE_ID = 10 /*MODEM*/);

		UPDATE DEVICE.GPRS_DEVICE GD
		   SET RSSI = pn_rssi || ',' || pn_ber,
			   RSSI_TS = pd_update_ts,
			   LAST_FILE_TRANSFER_ID = NULL,
			   MODEM_INFO_RECEIVED_TS = SYSDATE,
			   MODEM_INFO = pv_modem_info,
		   	   DEVICE_ID = ln_device_id,
			   GPRS_DEVICE_STATE_ID = 5,
			   ASSIGNED_BY = DECODE(DEVICE_ID, ln_device_id, ASSIGNED_BY, 'APP_LAYER'),
		   	   ASSIGNED_TS = DECODE(DEVICE_ID, ln_device_id, ASSIGNED_TS, SYSDATE)
		 WHERE ICCID IN(SELECT TO_NUMBER_OR_NULL(H.HOST_SERIAL_CD)
		  FROM DEVICE.HOST H
		  JOIN DEVICE.HOST_TYPE_HOST_GROUP_TYPE HTGT ON H.HOST_TYPE_ID = HTGT.HOST_TYPE_ID
		 WHERE H.DEVICE_ID = ln_device_id
		   AND HTGT.HOST_GROUP_TYPE_ID = 10 /*MODEM*/)
		   AND (RSSI_TS IS NULL OR RSSI_TS < pd_update_ts);
		SELECT MAX(H.HOST_ID)
		  INTO ln_host_id
		  FROM DEVICE.HOST H
		  JOIN DEVICE.HOST_TYPE_HOST_GROUP_TYPE HTGT ON H.HOST_TYPE_ID = HTGT.HOST_TYPE_ID
		 WHERE DEVICE_ID = ln_device_id
		   AND HTGT.HOST_GROUP_TYPE_ID = 10 /*MODEM*/;
		IF ln_host_id IS NOT NULL THEN
			UPSERT_HOST_SETTING(ln_host_id,'CSQ',pn_rssi || ',' || pn_ber);
		END IF;
	END;

	-- SP_CONSTRUCT_PROPERTIES_FILE is deprecated in R30
	PROCEDURE SP_CONSTRUCT_PROPERTIES_FILE(
		pn_device_id IN device.device_id%TYPE,
		pl_file OUT CLOB)
	IS
	BEGIN
		NULL;
	END;

	-- SP_UPDATE_DEVICE_CONFIG_FILE is deprecated in R30
	PROCEDURE SP_UPDATE_DEVICE_CONFIG_FILE(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pl_config_file LONG)
	IS
	BEGIN
		NULL;
	END;

	PROCEDURE SP_INITIALIZE_CONFIG_FILE(
		pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
		pn_new_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
		pn_new_property_list_version NUMBER,
		pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
		pn_updated OUT NUMBER,
		pn_old_property_list_version OUT NUMBER)
	IS
		ln_device_id DEVICE.DEVICE_ID%TYPE;
		lv_default_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE;
		lv_old_default_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE;
		lv_last_lock_utc_ts VARCHAR2(128);
	BEGIN
		lv_last_lock_utc_ts := PKG_GLOBAL.REQUEST_LOCK('DEVICE.DEVICE', pv_device_name);

		SELECT MAX(DEVICE_ID)
		  INTO ln_device_id
		  FROM (SELECT DEVICE_ID
			 FROM DEVICE.DEVICE
			WHERE DEVICE_NAME = pv_device_name
			ORDER BY DEVICE_ACTIVE_YN_FLAG DESC, CREATED_TS DESC)
		 WHERE ROWNUM = 1;

		IF ln_device_id IS NULL THEN
			RETURN;
		END IF;

		SELECT DECODE(DBADMIN.PKG_UTL.COMPARE(pn_new_device_type_id, 13),
			-1, default_config_template_name, default_config_template_name || pn_new_property_list_version)
		INTO lv_default_name
		FROM device.device_type
		WHERE device_type_id = pn_new_device_type_id;

		INSERT INTO DEVICE.DEVICE_SETTING(DEVICE_ID, DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_VALUE)
		SELECT ln_device_id, CTS.DEVICE_SETTING_PARAMETER_CD, CTS.CONFIG_TEMPLATE_SETTING_VALUE
		FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS
		JOIN DEVICE.CONFIG_TEMPLATE CT ON CTS.CONFIG_TEMPLATE_ID = CT.CONFIG_TEMPLATE_ID
		WHERE CT.CONFIG_TEMPLATE_NAME = lv_default_name AND NOT EXISTS (
			SELECT 1 FROM DEVICE.DEVICE_SETTING
			WHERE DEVICE_ID = ln_device_id
				AND DEVICE_SETTING_PARAMETER_CD = CTS.DEVICE_SETTING_PARAMETER_CD
		);

		IF pn_new_device_type_id IN (0, 1) THEN
			-- set Call Home Now Flag to N
			UPDATE DEVICE.DEVICE_SETTING
			SET DEVICE_SETTING_VALUE = '4E'
			WHERE DEVICE_ID = ln_device_id
				AND DEVICE_SETTING_PARAMETER_CD = '210'
				AND DBADMIN.PKG_UTL.EQL(DEVICE_SETTING_VALUE, '4E') = 'N';
		END IF;

		IF pn_new_device_type_id IN (0, 1, 6) THEN
			RETURN;
		END IF;

		SELECT MAX(TO_NUMBER(ds.DEVICE_SETTING_VALUE))
		  INTO pn_old_property_list_version
		  FROM DEVICE.DEVICE_SETTING ds
		 WHERE ds.DEVICE_ID = ln_device_id
		   AND ds.DEVICE_SETTING_PARAMETER_CD = PKG_CONST.DSP__PROPERTY_LIST_VERSION
		   AND REGEXP_LIKE(ds.DEVICE_SETTING_VALUE, '^[0-9]+$');
		IF pn_new_property_list_version IS NOT NULL AND pn_new_property_list_version <> NVL(pn_old_property_list_version, -1) THEN
			SP_UPDATE_DEVICE_SETTING(ln_device_id, PKG_CONST.DSP__PROPERTY_LIST_VERSION, pn_new_property_list_version);
		END IF;

		pn_updated := PKG_CONST.BOOLEAN__TRUE;
	END;

	PROCEDURE SP_INITIALIZE_CONFIG_FILE(
		pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
		pn_new_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
		pn_new_property_list_version NUMBER,
		pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
		pn_updated OUT NUMBER)
	IS
		ln_old_property_list_version NUMBER;
	BEGIN
		SP_INITIALIZE_CONFIG_FILE(pv_device_name, pn_new_device_type_id, pn_new_property_list_version, pn_file_transfer_id, pn_updated, ln_old_property_list_version);
	END;

	PROCEDURE ADD_PENDING_FILE_TRANSFER(
		pv_device_name DEVICE.DEVICE_NAME%TYPE,
		pv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
		pn_file_transfer_type_id FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
		pc_data_type ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
		pn_file_transfer_group DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
		pn_file_transfer_packet_size DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
		pn_execute_order ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE,
		pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
		pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE)
	IS
		ln_device_file_transfer_id ENGINE.MACHINE_CMD_PENDING.DEVICE_FILE_TRANSFER_ID%TYPE;
	BEGIN
		ADD_PENDING_FILE_TRANSFER(
		pv_device_name,pv_file_transfer_name,pn_file_transfer_type_id,
		pc_data_type,pn_file_transfer_group,pn_file_transfer_packet_size,pn_execute_order,pn_command_id, pn_file_transfer_id, ln_device_file_transfer_id);

	END;

	PROCEDURE ADD_PENDING_FILE_TRANSFER(
		pv_device_name DEVICE.DEVICE_NAME%TYPE,
		pv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
		pn_file_transfer_type_id FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
		pc_data_type ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
		pn_file_transfer_group DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
		pn_file_transfer_packet_size DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
		pn_execute_order ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE,
		pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
		pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
		pn_device_file_transfer_id OUT ENGINE.MACHINE_CMD_PENDING.DEVICE_FILE_TRANSFER_ID%TYPE)
	IS
		ln_device_id DEVICE_FILE_TRANSFER.DEVICE_ID%TYPE;
	BEGIN
		SELECT SEQ_FILE_TRANSFER_ID.NEXTVAL, SEQ_DEVICE_FILE_TRANSFER_ID.NEXTVAL, ENGINE.SEQ_MACHINE_CMD_PENDING_ID.NEXTVAL, DEVICE_ID
		  INTO pn_file_transfer_id, pn_device_file_transfer_id, pn_command_id, ln_device_id
		  FROM (SELECT DEVICE_ID
		  FROM DEVICE.DEVICE
		 WHERE DEVICE_NAME = pv_device_name
		 ORDER BY DEVICE_ACTIVE_YN_FLAG DESC, DEVICE_ID DESC)
		 WHERE ROWNUM = 1;
		INSERT INTO DEVICE.FILE_TRANSFER(FILE_TRANSFER_ID, FILE_TRANSFER_TYPE_CD, FILE_TRANSFER_NAME)
		   VALUES(pn_file_transfer_id, pn_file_transfer_type_id, pv_file_transfer_name);
		INSERT INTO DEVICE.DEVICE_FILE_TRANSFER(DEVICE_FILE_TRANSFER_ID, DEVICE_ID, FILE_TRANSFER_ID, DEVICE_FILE_TRANSFER_DIRECT, DEVICE_FILE_TRANSFER_STATUS_CD, DEVICE_FILE_TRANSFER_GROUP_NUM, DEVICE_FILE_TRANSFER_PKT_SIZE)
			VALUES(pn_device_file_transfer_id, ln_device_id, pn_file_transfer_id, 'O', 0, pn_file_transfer_group, pn_file_transfer_packet_size);
		INSERT INTO ENGINE.MACHINE_CMD_PENDING(MACHINE_COMMAND_PENDING_ID, MACHINE_ID, DATA_TYPE, DEVICE_FILE_TRANSFER_ID, EXECUTE_CD, EXECUTE_ORDER, EXECUTE_DATE)
		   VALUES(pn_command_id, pv_device_name, pc_data_type, pn_device_file_transfer_id, 'S', NVL(pn_execute_order, 999), SYSDATE);
	END;

	-- R30+
	PROCEDURE REQUEST_OUTBOUND_FILE_TRANSFER(
		pv_device_name DEVICE.DEVICE_NAME%TYPE,
		pv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
		pn_file_transfer_type_id FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
		pc_data_type ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
		pn_file_transfer_group DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
		pn_file_transfer_packet_size DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
		pl_file_transfer_content OUT FILE_TRANSFER.FILE_TRANSFER_CONTENT%TYPE,
		pd_file_transfer_create_ts OUT FILE_TRANSFER.CREATED_TS%TYPE,
		pn_mcp_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
		pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE)
	IS
		l_dft_id DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_ID%TYPE;
		l_device_id DEVICE_FILE_TRANSFER.DEVICE_ID%TYPE;
	BEGIN
		SELECT FILE_TRANSFER_ID, CREATED_TS
		INTO pn_file_transfer_id, pd_file_transfer_create_ts
		FROM (
			SELECT FILE_TRANSFER_ID, CREATED_TS
			FROM DEVICE.FILE_TRANSFER
			WHERE FILE_TRANSFER_NAME = pv_file_transfer_name
				AND FILE_TRANSFER_TYPE_CD = pn_file_transfer_type_id
			ORDER BY CREATED_TS
		) WHERE ROWNUM = 1;
		SELECT MAX(dft.DEVICE_FILE_TRANSFER_ID)
		  INTO l_dft_id
		  FROM DEVICE.DEVICE_FILE_TRANSFER dft
		  JOIN DEVICE.DEVICE d ON dft.DEVICE_ID = d.DEVICE_ID
		 WHERE d.DEVICE_NAME = pv_device_name
		   AND dft.FILE_TRANSFER_ID = pn_file_transfer_id
		   AND dft.DEVICE_FILE_TRANSFER_DIRECT = 'O'
		   AND dft.DEVICE_FILE_TRANSFER_STATUS_CD = 1;
	   IF l_dft_id IS NOT NULL THEN -- already exists, find pending command
		 SELECT MAX(MACHINE_COMMAND_PENDING_ID) -- avoid NO_DATA_FOUND
		   INTO pn_mcp_id
		   FROM ENGINE.MACHINE_CMD_PENDING
		  WHERE MACHINE_ID = pv_device_name
			AND DEVICE_FILE_TRANSFER_ID = l_dft_id;
		ELSE
			SELECT SEQ_DEVICE_FILE_TRANSFER_ID.NEXTVAL, DEVICE_ID
			  INTO l_dft_id, l_device_id
			  FROM (SELECT DEVICE_ID
			  FROM DEVICE.DEVICE
			 WHERE DEVICE_NAME = pv_device_name
			 ORDER BY DEVICE_ACTIVE_YN_FLAG DESC, DEVICE_ID DESC)
			 WHERE ROWNUM = 1;
			INSERT INTO DEVICE.DEVICE_FILE_TRANSFER(DEVICE_FILE_TRANSFER_ID, DEVICE_ID, FILE_TRANSFER_ID, DEVICE_FILE_TRANSFER_DIRECT, DEVICE_FILE_TRANSFER_STATUS_CD, DEVICE_FILE_TRANSFER_GROUP_NUM, DEVICE_FILE_TRANSFER_PKT_SIZE)
			  VALUES(l_dft_id, l_device_id, pn_file_transfer_id, 'O', 0, pn_file_transfer_group, pn_file_transfer_packet_size);
		END IF;
		IF pn_mcp_id IS NULL THEN
			SELECT ENGINE.SEQ_MACHINE_CMD_PENDING_ID.NEXTVAL
			  INTO pn_mcp_id
			  FROM DUAL;
			INSERT INTO ENGINE.MACHINE_CMD_PENDING(MACHINE_COMMAND_PENDING_ID, MACHINE_ID, DATA_TYPE, DEVICE_FILE_TRANSFER_ID, EXECUTE_CD, EXECUTE_ORDER, EXECUTE_DATE)
			  VALUES(pn_mcp_id, pv_device_name, pc_data_type, l_dft_id, 'S', 999, SYSDATE);
		END IF;
		SP_GET_FILE_TRANSFER_BLOB(pn_file_transfer_id, pl_file_transfer_content);
	EXCEPTION
	   WHEN NO_DATA_FOUND THEN
			   NULL; -- let variables be null
	END;

	-- R29
	PROCEDURE REQUEST_OUTBOUND_FILE_TRANSFER(
		pv_device_name DEVICE.DEVICE_NAME%TYPE,
		pv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
		pn_file_transfer_type_id FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
		pc_data_type ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
		pn_file_transfer_group DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
		pn_file_transfer_packet_size DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
		pl_file_transfer_content OUT FILE_TRANSFER.FILE_TRANSFER_CONTENT%TYPE,
		pd_file_transfer_create_ts OUT FILE_TRANSFER.CREATED_TS%TYPE,
		pn_mcp_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE)
	IS
		ln_file_transfer_id FILE_TRANSFER.FILE_TRANSFER_ID%TYPE;
	BEGIN
		REQUEST_OUTBOUND_FILE_TRANSFER(
		pv_device_name,
		pv_file_transfer_name,
		pn_file_transfer_type_id,
		pc_data_type,
		pn_file_transfer_group,
		pn_file_transfer_packet_size,
		pl_file_transfer_content,
		pd_file_transfer_create_ts,
		pn_mcp_id,
		ln_file_transfer_id);
	END;

	-- R33+ signature
	PROCEDURE SP_RECORD_FILE_TRANSFER(
		pv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
		pn_file_transfer_type_cd FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
		pv_device_name DEVICE.DEVICE_NAME%TYPE,
		pv_event_global_trans_cd VARCHAR2,
		pc_overwrite_flag CHAR,
		pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
		pd_file_transfer_ts DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_TS%TYPE DEFAULT SYSDATE,
		pv_global_session_cd VARCHAR2)
	IS
		ln_device_file_transfer_id DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_ID%TYPE;
		ln_device_id DEVICE_FILE_TRANSFER.DEVICE_ID%TYPE;
	BEGIN
		ln_device_id := GET_DEVICE_ID_BY_NAME(pv_device_name, pd_file_transfer_ts);

		-- duplicate detection of already stored files re-delivered by QueueLayer
		SELECT /*+INDEX(DFT IDX_DEV_FILE_XFER_GLOB_SESS_CD)*/ MAX(DFT.DEVICE_FILE_TRANSFER_ID), MAX(DFT.FILE_TRANSFER_ID)
		  INTO ln_device_file_transfer_id, pn_file_transfer_id
		  FROM DEVICE.DEVICE_FILE_TRANSFER DFT
		JOIN DEVICE.FILE_TRANSFER FT ON FT.FILE_TRANSFER_ID = DFT.FILE_TRANSFER_ID
		WHERE DFT.GLOBAL_SESSION_CD = pv_global_session_cd
			AND DFT.DEVICE_ID = ln_device_id
			AND DFT.DEVICE_FILE_TRANSFER_TS = pd_file_transfer_ts
		AND DFT.DEVICE_FILE_TRANSFER_DIRECT = 'I'
		AND FT.FILE_TRANSFER_NAME = pv_file_transfer_name
		AND FT.FILE_TRANSFER_TYPE_CD = pn_file_transfer_type_cd;
		IF ln_device_file_transfer_id IS NOT NULL THEN
			RETURN;
		END IF;

		BEGIN
			SELECT FILE_TRANSFER_ID
			  INTO pn_file_transfer_id
			  FROM (SELECT /*+ index(FT INX_FILE_TRANSFER_TYPE_NAME) index(DFT IDX_DEVICE_FILE_TRANSFER_ID) */ ft.FILE_TRANSFER_ID
			  FROM DEVICE.FILE_TRANSFER ft
			  LEFT JOIN DEVICE.DEVICE_FILE_TRANSFER dft on ft.FILE_TRANSFER_ID = dft.FILE_TRANSFER_ID
			 WHERE ft.FILE_TRANSFER_NAME = pv_file_transfer_name
			   AND ft.FILE_TRANSFER_TYPE_CD = pn_file_transfer_type_cd
			   AND NVL(dft.DEVICE_FILE_TRANSFER_DIRECT, 'I') = 'I'
			   AND NVL(dft.DEVICE_FILE_TRANSFER_STATUS_CD, 1) = 1
			   AND pc_overwrite_flag = 'Y'
			 ORDER BY ft.FILE_TRANSFER_ID DESC)
			 WHERE ROWNUM = 1;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				SELECT SEQ_FILE_TRANSFER_ID.NEXTVAL
				  INTO pn_file_transfer_id
				  FROM DUAL;
				INSERT INTO DEVICE.FILE_TRANSFER (
					FILE_TRANSFER_ID,
					FILE_TRANSFER_NAME,
					FILE_TRANSFER_TYPE_CD)
				  VALUES(
					pn_file_transfer_id,
					pv_file_transfer_name,
					pn_file_transfer_type_cd);
		END;
		SELECT SEQ_DEVICE_FILE_TRANSFER_ID.NEXTVAL
		INTO ln_device_file_transfer_id
		FROM DUAL;
		INSERT INTO DEVICE.DEVICE_FILE_TRANSFER(
			DEVICE_FILE_TRANSFER_ID,
			DEVICE_ID,
			FILE_TRANSFER_ID,
			DEVICE_FILE_TRANSFER_DIRECT,
			DEVICE_FILE_TRANSFER_STATUS_CD,
			DEVICE_FILE_TRANSFER_TS,
			GLOBAL_SESSION_CD)
		 VALUES(
			ln_device_file_transfer_id,
			ln_device_id,
			pn_file_transfer_id,
			'I',
			1,
			pd_file_transfer_ts,
			pv_global_session_cd);
		IF pv_event_global_trans_cd IS NOT NULL THEN
			INSERT INTO DEVICE.DEVICE_FILE_TRANSFER_EVENT(EVENT_ID, DEVICE_FILE_TRANSFER_ID)
			  SELECT EVENT_ID, ln_device_file_transfer_id
				FROM DEVICE.EVENT
				WHERE EVENT_GLOBAL_TRANS_CD = pv_event_global_trans_cd;
		END IF;

		-- mark any pending file transfer requests for the same file name as complete
		UPDATE engine.machine_cmd_pending
		SET execute_cd = 'A', global_session_cd = pv_global_session_cd
		WHERE machine_id = pv_device_name
			AND data_type = '9B'
			AND command = UPPER(RAWTOHEX(pv_file_transfer_name));
	END;

	FUNCTION GET_NORMALIZED_TIME(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
		pv_normalizer_start_min_cd ENGINE.APP_SETTING.APP_SETTING_CD%TYPE,
		pv_normalizer_end_min_cd ENGINE.APP_SETTING.APP_SETTING_CD%TYPE,
		pv_window_start_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_window_hours_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_device_time_zone_guid VARCHAR2) 
	RETURN VARCHAR2
	IS
		ln_call_in_start_min NUMBER;
		ln_call_in_end_min NUMBER;
		ln_call_in_window_min NUMBER;
		ln_call_in_time_min NUMBER;
		ln_call_in_window_start NUMBER;
		ln_call_in_window_hours NUMBER;
		ln_seed_4 NUMBER := NVL(DBADMIN.TO_NUMBER_OR_NULL(SUBSTR(pv_device_serial_cd, -4)), 0);
		ln_convert_time_zone NUMBER := PKG_CONST.BOOLEAN__TRUE;
	BEGIN
		ln_call_in_window_start := ROUND(NVL(DBADMIN.TO_NUMBER_OR_NULL(GET_DEVICE_SETTING(pn_device_id, pv_window_start_cd)), -1));
		ln_call_in_window_hours := ROUND(NVL(DBADMIN.TO_NUMBER_OR_NULL(GET_DEVICE_SETTING(pn_device_id, pv_window_hours_cd)), 0));
		IF ln_call_in_window_start BETWEEN 0 AND 2359 AND ln_call_in_window_hours BETWEEN 1 AND 23 THEN
			ln_call_in_start_min := FLOOR(ln_call_in_window_start / 100) * 60 + MOD(ln_call_in_window_start, 100);
			ln_call_in_window_min := ln_call_in_window_hours * 60;
			ln_convert_time_zone := PKG_CONST.BOOLEAN__FALSE;
		ELSIF pv_normalizer_start_min_cd IS NULL OR pv_normalizer_end_min_cd IS NULL THEN
			RETURN NULL;
		ELSE
			ln_call_in_start_min := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING(pv_normalizer_start_min_cd));
			ln_call_in_end_min := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING(pv_normalizer_end_min_cd));

			IF ln_call_in_start_min < 0 OR ln_call_in_start_min = ln_call_in_end_min THEN
				RETURN NULL;
			END IF;

			ln_call_in_window_min := MOD(ln_call_in_end_min - ln_call_in_start_min, 1440);
			IF ln_call_in_window_min < 0 THEN
				ln_call_in_window_min := ln_call_in_window_min + 1440;
			END IF;
		END IF;

		-- use last 4 digits of device serial number to calculate its call-in time
		ln_call_in_time_min := MOD(ln_call_in_start_min + ROUND(ln_call_in_window_min * ln_seed_4 / 9999), 1440);

		IF ln_convert_time_zone = PKG_CONST.BOOLEAN__TRUE THEN
			-- convert call-in time from server time zone to device local time zone
			ln_call_in_time_min := MOD(ln_call_in_time_min + DBADMIN.PKG_UTL.TIME_ZONE_DIFF_MIN(pv_device_time_zone_guid, PKG_CONST.DB_TIME_ZONE), 1440);
			IF ln_call_in_time_min < 0 THEN
				ln_call_in_time_min := ln_call_in_time_min + 1440;
			END IF;
		END IF;

		RETURN LPAD(FLOOR(ln_call_in_time_min / 60), 2, '0') || LPAD(MOD(ln_call_in_time_min, 60), 2, '0');
	END;

	FUNCTION GET_NORMALIZED_OFFSET(
		pv_normalized_time VARCHAR2,
		pv_device_time_zone_guid VARCHAR2) 
	RETURN NUMBER
	IS
		ln_normalized_time_min NUMBER;
		ln_normalized_utc_time_min NUMBER;
	BEGIN
		ln_normalized_time_min := TO_NUMBER(SUBSTR(pv_normalized_time, 1, 2)) * 60 + TO_NUMBER(SUBSTR(pv_normalized_time, 3, 2));
		ln_normalized_utc_time_min := MOD(ln_normalized_time_min + DBADMIN.PKG_UTL.TIME_ZONE_DIFF_MIN(PKG_CONST.GMT_TIME_ZONE, pv_device_time_zone_guid), 1440);
		IF ln_normalized_utc_time_min < 0 THEN
			ln_normalized_utc_time_min := ln_normalized_utc_time_min + 1440;
		END IF;
		RETURN ln_normalized_utc_time_min * 60;
	END;

	FUNCTION NORMALIZE_TIME_LIST (
		pv_current_schedule VARCHAR2,
		pv_normalized_time VARCHAR2) 
	RETURN VARCHAR2
	IS
		lv_new_schedule VARCHAR2(256);
		lt_times_table VARCHAR2_TABLE;
	BEGIN
		lv_new_schedule := PKG_CONST.REOCCURRENCE_TYPE__TIME_LIST || PKG_CONST.SCHEDULE__FS;
		SELECT DISTINCT regexp_substr(pv_current_schedule,'\d+', 1, LEVEL)
		BULK COLLECT INTO lt_times_table
		FROM dual
		CONNECT BY regexp_substr(pv_current_schedule,'\d+', 1, LEVEL) IS NOT NULL
		ORDER BY 1;
		FOR i IN lt_times_table.FIRST..lt_times_table.LAST loop
			lv_new_schedule := lv_new_schedule || substr(lt_times_table(i), 0, 2) || substr(pv_normalized_time, 3, 4) || '|';
		END loop;
		lv_new_schedule := rtrim(lv_new_schedule, '|');
	RETURN lv_new_schedule;
	END;

	PROCEDURE NORMALIZE_SCHEDULE(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
		pv_schedule_type DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_normalizer_start_min_cd ENGINE.APP_SETTING.APP_SETTING_CD%TYPE,
		pv_normalizer_end_min_cd ENGINE.APP_SETTING.APP_SETTING_CD%TYPE,
		pv_window_start_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_window_hours_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_device_time_zone_guid VARCHAR2,
		pn_result_cd IN OUT NUMBER,
		pv_schedule IN OUT DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE)
	IS
		lv_current_schedule DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
		ln_current_interval NUMBER;
		ln_min_allowed_interval NUMBER;
		lc_reoccurrence_type CHAR(1);
		ln_exists NUMBER;
		lv_normalized_time VARCHAR2(4) := GET_NORMALIZED_TIME(pn_device_id, pv_device_serial_cd, pv_normalizer_start_min_cd, pv_normalizer_end_min_cd, pv_window_start_cd, pv_window_hours_cd, pv_device_time_zone_guid);
		ln_normalized_offset NUMBER := GET_NORMALIZED_OFFSET(lv_normalized_time, pv_device_time_zone_guid);
		lv_schedule DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE := NULL;
	BEGIN
		lv_current_schedule := NVL(GET_DEVICE_SETTING(pn_device_id, pv_schedule_type), '0');
		IF lv_current_schedule = '0' THEN
			IF pv_schedule_type = PKG_CONST.DSP__NON_ACTIV_CALL_IN_SCHED THEN
				lv_schedule := PKG_CONST.REOCCURRENCE_TYPE__INTERVAL || PKG_CONST.SCHEDULE__FS || ln_normalized_offset || PKG_CONST.SCHEDULE__FS || DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EDGE_NON_ACTIVATED_CALL_IN_INTERVAL_SEC');
			ELSIF pv_schedule_type IN (PKG_CONST.DSP__ACTIVATED_CALL_IN_SCHED) THEN
				IF lv_normalized_time IS NOT NULL THEN
					lv_schedule := PKG_CONST.REOCCURRENCE_TYPE__WEEKLY || PKG_CONST.SCHEDULE__FS || lv_normalized_time || PKG_CONST.SCHEDULE__FS || PKG_CONST.SCHEDULE__SUNDAY;
				END IF;
			END IF;
		ELSE
			lc_reoccurrence_type := SUBSTR(lv_current_schedule, 1, 1);
			IF lc_reoccurrence_type = PKG_CONST.REOCCURRENCE_TYPE__INTERVAL THEN
				ln_current_interval := NVL(DBADMIN.TO_NUMBER_OR_NULL(SUBSTR(lv_current_schedule, INSTR(lv_current_schedule, PKG_CONST.SCHEDULE__FS, -1, 1) + 1)), -1);
				ln_min_allowed_interval := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('MIN_INTERVAL_SCHEDULE_SEC'));
				IF ln_current_interval < ln_min_allowed_interval THEN
					ln_current_interval := ln_min_allowed_interval;
				END IF;
				lv_schedule := lc_reoccurrence_type || PKG_CONST.SCHEDULE__FS || ln_normalized_offset || PKG_CONST.SCHEDULE__FS || ln_current_interval;
			ELSIF lc_reoccurrence_type = PKG_CONST.REOCCURRENCE_TYPE__TIME_LIST THEN
				IF lv_normalized_time IS NOT NULL THEN
					lv_schedule := NORMALIZE_TIME_LIST(lv_current_schedule, lv_normalized_time);
				END IF;
			ELSIF lc_reoccurrence_type IN (PKG_CONST.REOCCURRENCE_TYPE__MONTHLY, PKG_CONST.REOCCURRENCE_TYPE__WEEKLY) THEN
				IF lv_normalized_time IS NOT NULL THEN
					lv_schedule := lc_reoccurrence_type || PKG_CONST.SCHEDULE__FS || lv_normalized_time || PKG_CONST.SCHEDULE__FS || SUBSTR(lv_current_schedule, INSTR(lv_current_schedule, PKG_CONST.SCHEDULE__FS, -1, 1) + 1);
				END IF;
			ELSE
				IF lv_normalized_time IS NOT NULL THEN
					lv_schedule := PKG_CONST.REOCCURRENCE_TYPE__DAILY || PKG_CONST.SCHEDULE__FS || lv_normalized_time;
				END IF;
			END IF;
		END IF;
		IF lv_schedule IS NOT NULL AND lv_schedule != lv_current_schedule THEN
			SP_UPSERT_DEVICE_SETTING(pn_device_id, pv_schedule_type, lv_schedule, ln_exists);
			pn_result_cd := 1;
			pv_schedule := lv_schedule;
		END IF;
	END;

	PROCEDURE SP_NORMALIZE_CALL_IN_TIME(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pn_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE,
		pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
		pv_device_time_zone_guid VARCHAR2,
		pn_result_cd OUT NUMBER,
		pv_activated_call_in_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_non_activ_call_in_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_settlement_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_dex_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE)
	IS
		ln_command_id ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE;
		lv_data_type ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE;
		ll_command ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE;
	BEGIN
		SP_NORMALIZE_CALL_IN_TIME(
		  pn_device_id,
		  pn_device_type_id,
		  pv_device_serial_cd,
		  pv_device_time_zone_guid,
		  pn_result_cd,
		  pv_activated_call_in_schedule,
		  pv_non_activ_call_in_schedule,
		  pv_settlement_schedule,
		  pv_dex_schedule,
		  ln_command_id,
		  lv_data_type,
		  ll_command);
	END; -- SP_NORMALIZE_CALL_IN_TIME stub

	/**
	* r29 Call-in normalization version
	*/
	PROCEDURE SP_NORMALIZE_CALL_IN_TIME(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pn_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE,
		pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
		pv_device_time_zone_guid VARCHAR2,
		pn_result_cd OUT NUMBER,
		pv_activated_call_in_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_non_activ_call_in_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_settlement_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_dex_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
		pv_data_type OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
		pl_command OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE)
	IS
		lv_return_msg VARCHAR2(2048);
		ln_exists NUMBER;
		ln_command_id ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE;
		lv_data_type ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE;
		ll_command ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE;
		ln_result_cd NUMBER;
		lv_current_value DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
		lv_new_value DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
		ln_current_value NUMBER;
		ln_threshold NUMBER;
		ln_device_id DEVICE.DEVICE_ID%TYPE;
	BEGIN
		-- normalize device call-in time to avoid load peaks
		pn_result_cd := 0;
		pv_activated_call_in_schedule := PKG_CONST.ASCII__NUL;
		pv_non_activ_call_in_schedule := PKG_CONST.ASCII__NUL;
		pv_settlement_schedule := PKG_CONST.ASCII__NUL;
		pv_dex_schedule := PKG_CONST.ASCII__NUL;

		SELECT MAX(dla.device_id)
		INTO ln_device_id
		FROM device.device d
		JOIN device.device_last_active dla ON d.device_name = dla.device_name
		WHERE d.device_id = pn_device_id;

		IF pn_device_type_id IN (PKG_CONST.DEVICE_TYPE__G4, PKG_CONST.DEVICE_TYPE__GX) THEN
			pv_activated_call_in_schedule := GET_NORMALIZED_TIME(ln_device_id, pv_device_serial_cd, 'GX_CALL_IN_NORMALIZER_START_MIN', 'GX_CALL_IN_NORMALIZER_END_MIN',
				'CALL_IN_TIME_WINDOW_START', 'CALL_IN_TIME_WINDOW_HOURS', pv_device_time_zone_guid);
			IF pv_activated_call_in_schedule IS NOT NULL THEN
				IF pv_activated_call_in_schedule BETWEEN '0000' AND '0004' THEN
					pv_activated_call_in_schedule := '001' || SUBSTR(pv_activated_call_in_schedule, 4, 1);
				ELSIF pv_activated_call_in_schedule BETWEEN '2356' AND '2359' THEN
					pv_activated_call_in_schedule := '234' || SUBSTR(pv_activated_call_in_schedule, 4, 1);
				END IF;
				SP_UPDATE_CONFIG_AND_RETURN(ln_device_id, 172, pv_activated_call_in_schedule, 'H', pn_device_type_id, 0, pn_result_cd, lv_return_msg, pn_command_id, pv_data_type, pl_command);
			END IF;

			lv_current_value := GET_DEVICE_SETTING(ln_device_id, '358');
			pv_dex_schedule := GET_NORMALIZED_TIME(ln_device_id, pv_device_serial_cd,
				CASE WHEN lv_current_value = 'FFFF' THEN NULL ELSE 'GX_CALL_IN_NORMALIZER_START_MIN' END,
				CASE WHEN lv_current_value = 'FFFF' THEN NULL ELSE 'GX_CALL_IN_NORMALIZER_END_MIN' END,
				'CALL_IN_TIME_WINDOW_START_DEX', 'CALL_IN_TIME_WINDOW_HOURS_DEX', pv_device_time_zone_guid);
			IF pv_dex_schedule IS NOT NULL THEN
				IF pn_command_id IS NULL THEN
					SP_UPDATE_CONFIG_AND_RETURN(ln_device_id, 358, pv_dex_schedule, 'H', pn_device_type_id, 0, pn_result_cd, lv_return_msg, pn_command_id, pv_data_type, pl_command);
				ELSE
					SP_UPDATE_CONFIG_AND_RETURN(ln_device_id, 358, pv_dex_schedule, 'H', pn_device_type_id, 0, ln_result_cd, lv_return_msg, ln_command_id, lv_data_type, ll_command);
				END IF;
			END IF;

			--DEX Read and Send Interval Minutes
			lv_current_value := GET_DEVICE_SETTING(ln_device_id, '244');
			IF lv_current_value != '0000' THEN
				ln_current_value := NVL(DBADMIN.TO_NUMBER_OR_NULL(lv_current_value), 0);
				ln_threshold := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('MIN_DEX_READ_AND_SEND_INTERVAL_MIN'));
				IF ln_current_value < ln_threshold THEN
					lv_new_value := LPAD(ln_threshold, 4, '0');
					IF pn_command_id IS NULL THEN
						SP_UPDATE_CONFIG_AND_RETURN(ln_device_id, 244, lv_new_value, 'H', pn_device_type_id, 0, pn_result_cd, lv_return_msg, pn_command_id, pv_data_type, pl_command);
					ELSE
						SP_UPDATE_CONFIG_AND_RETURN(ln_device_id, 244, lv_new_value, 'H', pn_device_type_id, 0, ln_result_cd, lv_return_msg, ln_command_id, lv_data_type, ll_command);
					END IF;
				END IF;
			END IF;
		ELSIF pn_device_type_id = PKG_CONST.DEVICE_TYPE__MEI THEN
			pv_activated_call_in_schedule := GET_NORMALIZED_TIME(ln_device_id, pv_device_serial_cd, 'MEI_CALL_IN_NORMALIZER_START_MIN', 'MEI_CALL_IN_NORMALIZER_END_MIN',
				'CALL_IN_TIME_WINDOW_START', 'CALL_IN_TIME_WINDOW_HOURS', pv_device_time_zone_guid);
			IF pv_activated_call_in_schedule IS NOT NULL THEN
				lv_new_value := LPAD(TO_NUMBER(SUBSTR(pv_activated_call_in_schedule, 1, 2)) * 60 + TO_NUMBER(SUBSTR(pv_activated_call_in_schedule, 3, 2)), 4, '0');
				SP_UPDATE_CONFIG_AND_RETURN(ln_device_id, 17, lv_new_value, 'H', pn_device_type_id, 0, pn_result_cd, lv_return_msg, pn_command_id, pv_data_type, pl_command);
			END IF;
		ELSIF pn_device_type_id = PKG_CONST.DEVICE_TYPE__EDGE THEN
			NORMALIZE_SCHEDULE(ln_device_id, pv_device_serial_cd, PKG_CONST.DSP__ACTIVATED_CALL_IN_SCHED, 'EDGE_CALL_IN_NORMALIZER_START_MIN', 'EDGE_CALL_IN_NORMALIZER_END_MIN',
				'CALL_IN_TIME_WINDOW_START_ACTIVATED', 'CALL_IN_TIME_WINDOW_HOURS_ACTIVATED', pv_device_time_zone_guid, pn_result_cd, pv_activated_call_in_schedule);
			NORMALIZE_SCHEDULE(ln_device_id, pv_device_serial_cd, PKG_CONST.DSP__NON_ACTIV_CALL_IN_SCHED, 'EDGE_CALL_IN_NORMALIZER_START_MIN', 'EDGE_CALL_IN_NORMALIZER_END_MIN',
				'CALL_IN_TIME_WINDOW_START_NON_ACTIVATED', 'CALL_IN_TIME_WINDOW_HOURS_NON_ACTIVATED', pv_device_time_zone_guid, pn_result_cd, pv_non_activ_call_in_schedule);
			NORMALIZE_SCHEDULE(ln_device_id, pv_device_serial_cd, PKG_CONST.DSP__SETTLEMENT_SCHEDULE, 'EDGE_CALL_IN_NORMALIZER_START_MIN', 'EDGE_CALL_IN_NORMALIZER_END_MIN',
				'CALL_IN_TIME_WINDOW_START_SETTLEMENT', 'CALL_IN_TIME_WINDOW_HOURS_SETTLEMENT', pv_device_time_zone_guid, pn_result_cd, pv_settlement_schedule);

			IF DBADMIN.PKG_GLOBAL.GET_APP_SETTING('NORMALIZE_EDGE_DEX_SCHEDULE_FLAG') = 'Y' THEN
				NORMALIZE_SCHEDULE(ln_device_id, pv_device_serial_cd, PKG_CONST.DSP__DEX_SCHEDULE, 'EDGE_CALL_IN_NORMALIZER_START_MIN', 'EDGE_CALL_IN_NORMALIZER_END_MIN',
					'CALL_IN_TIME_WINDOW_START_DEX', 'CALL_IN_TIME_WINDOW_HOURS_DEX', pv_device_time_zone_guid, pn_result_cd, pv_dex_schedule);
			END IF;
		END IF;

		UPDATE DEVICE.DEVICE
		SET CALL_IN_TIME_NORMALIZED_TS = SYSDATE
		WHERE DEVICE_ID = ln_device_id;
	END;

	/**
	* Part of the r29 version of SP_NEXT_PENDING_COMMAND
	* This non-public procedure adds commands for ESUDS Diagnostics, if existing diagnostics are beyond 16 hours old.
	*/
	PROCEDURE SP_NPC_HELPER_ESUDS_DIAG(
		pv_device_name DEVICE.DEVICE_NAME%TYPE,
		pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
		pv_data_type OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
		pv_session_attributes IN OUT VARCHAR2, -- contains character 'E' if ESUDS diagnostics has been requested previously
		pn_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
		pn_commands_inserted OUT NUMBER,
		pl_command OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE)
	IS
		ld_oldest_diag DATE;
	BEGIN
		IF pn_device_type_id IN (PKG_CONST.DEVICE_TYPE__ESUDS) AND INSTR(NVL(pv_session_attributes,'-'),'E') = 0 THEN
			pv_session_attributes := NVL(pv_session_attributes,'') || 'E';
			-- if any records for the given device are old, (no records = old records)
			SELECT MIN(MOST_RECENT_PORT_DIAG_DATE)
			  INTO ld_oldest_diag
			  FROM (
				  SELECT d.device_id, h.host_port_num, MAX(NVL(hds.host_diag_last_reported_ts, MIN_DATE)) AS MOST_RECENT_PORT_DIAG_DATE
					FROM device d
					LEFT OUTER JOIN host h ON (d.device_id = h.device_id)
					LEFT OUTER JOIN host_diag_status hds ON (hds.host_id = h.host_id)
					WHERE d.device_type_id = PKG_CONST.DEVICE_TYPE__ESUDS
					AND d.device_name = pv_device_name
					AND d.device_active_yn_flag = 'Y'
					AND NVL(h.host_active_yn_flag,'Y') = 'Y'
					AND h.host_port_num <> 0
					GROUP BY d.device_id, h.host_port_num
			  );

			IF ld_oldest_diag < SYSDATE - 1 THEN
				pv_data_type := '9A61';
				pl_command := '';
				UPSERT_PENDING_COMMAND(pv_device_name, pv_data_type, pl_command, 'S', 999, pn_commands_inserted, pn_command_id);
			END IF;
		END IF;
	END; -- SP_NPC_HELPER_ESUDS_DIAGNOSTICS

	/**
	* Part of the r29 version of SP_NEXT_PENDING_COMMAND
	* This non-public procedure adds commands for GPRS configuration files, if needed.
	*/
	PROCEDURE SP_NPC_HELPER_MODEM(
		pv_device_name DEVICE.DEVICE_NAME%TYPE,
		pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
		pv_data_type OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
		pv_session_attributes IN OUT VARCHAR2, -- contains character 'C' if configuration test has been requested previously, R if configuration requested
		pn_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
		pn_commands_inserted OUT NUMBER,
		pl_command OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE)
	IS
		ld_last_update_ts DATE;
	BEGIN
		-- For Gx and MEI, test to see if modem data is outdated by 7 days. If so, send the appropriate message (both happen to be 87)
		-- Some G4's only send garbage for modem info, so don't keep requesting it for G4 (PKG_CONST.DEVICE_TYPE__G4)
		IF pn_device_type_id IN (PKG_CONST.DEVICE_TYPE__GX, PKG_CONST.DEVICE_TYPE__MEI) AND INSTR(NVL(pv_session_attributes,'-'),'C') = 0 THEN
			pv_session_attributes := NVL(pv_session_attributes,'') || 'C';
			SELECT NVL(MAX(H.LAST_UPDATED_TS), MIN_DATE)
			  INTO ld_last_update_ts
			  FROM DEVICE.DEVICE D
			  JOIN DEVICE.HOST H ON D.DEVICE_ID = H.DEVICE_ID
			 WHERE D.DEVICE_NAME = pv_device_name
			   AND D.DEVICE_ACTIVE_YN_FLAG = 'Y'
			   AND H.HOST_PORT_NUM = 2;
			IF ld_last_update_ts < SYSDATE - NVL(TO_NUMBER_OR_NULL(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DEVICE_MODEM_INFO_REFRESH_HR')) / 24, 7) THEN
				pv_data_type := '87';
				IF (pn_device_type_id = PKG_CONST.DEVICE_TYPE__MEI) THEN
					pl_command := '4200000000000000FF';
				ELSIF (pn_device_type_id = PKG_CONST.DEVICE_TYPE__GX OR pn_device_type_id = PKG_CONST.DEVICE_TYPE__G4) THEN
					pl_command := '4400802C0000000190';
				END IF;
				pv_session_attributes := NVL(pv_session_attributes,'') || 'R';
				UPSERT_PENDING_COMMAND(pv_device_name, pv_data_type, pl_command, 'S', 999, pn_commands_inserted, pn_command_id);
			END IF;
		END IF;
	END;

	/**
	* Part of the r29 version of SP_NEXT_PENDING_COMMAND
	* This non-public procedure adds commands for G4 configuration files, if existing ones are stale.
	*/
	PROCEDURE SP_NPC_HELPER_STALE_CONFIG(
		pv_device_name DEVICE.DEVICE_NAME%TYPE,
		pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
		pv_data_type OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
		pv_session_attributes IN OUT VARCHAR2, -- contains character 'G' if GX configuration has been requested previously
		pn_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
		pn_commands_inserted OUT NUMBER,
		pl_command OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE)
	IS
		ld_last_config_ts DATE;
	BEGIN
		-- For MEI/Gx test age of configuration data
		IF pn_device_type_id IN (PKG_CONST.DEVICE_TYPE__GX, PKG_CONST.DEVICE_TYPE__MEI, PKG_CONST.DEVICE_TYPE__G4) AND INSTR(NVL(pv_session_attributes,'-'),'G') = 0 THEN
			pv_session_attributes := NVL(pv_session_attributes,'') || 'G';
			IF NOT(pn_device_type_id = PKG_CONST.DEVICE_TYPE__MEI AND INSTR(NVL(pv_session_attributes,'-'), 'R') > 0 ) THEN
				-- Now check last configuration received timestamp
				SELECT NVL(D.RECEIVED_CONFIG_FILE_TS, MIN_DATE)
				  INTO ld_last_config_ts
				  FROM DEVICE.DEVICE D
				 WHERE D.DEVICE_NAME = pv_device_name
				   AND D.DEVICE_ACTIVE_YN_FLAG = 'Y';
				IF ld_last_config_ts < SYSDATE - NVL(TO_NUMBER_OR_NULL(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DEVICE_CONFIG_REFRESH_HR')) / 24, 30) THEN
					pv_data_type := '87';
					-- same command for all devices handled by this procedure
					pl_command := '4200000000000000FF';
					pv_session_attributes := NVL(pv_session_attributes,'') || 'R';
					UPSERT_PENDING_COMMAND(pv_device_name, pv_data_type, pl_command, 'S', 999, pn_commands_inserted, pn_command_id);
				END IF; -- actual test
			END IF; -- 'R' flag
		END IF; -- 'G' flag
	END; -- SP_NPC_HELPER_STALE_CONFIG

	/**
	* Part of the r29 version of SP_NEXT_PENDING_COMMAND
	* This non-public procedure adds commands to set the Gx verbosity flag
	* on a non-remotely updatable device.
	*/
	PROCEDURE SP_NPC_HELPER_PHILLY_COKE(
		pv_device_name DEVICE.DEVICE_NAME%TYPE,
		pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
		pv_data_type OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
		pv_session_attributes IN OUT VARCHAR2, -- contains character 'P' if ESUDS diagnostics has been requested previously
		pn_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
		pv_firmware_version IN DEVICE.FIRMWARE_VERSION%TYPE,
		pn_commands_inserted OUT NUMBER,
		pl_command OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE)
	IS
		l_is_philly_coke NUMBER;
	BEGIN
		-- For Gx test age of configuration data
		IF pn_device_type_id IN (PKG_CONST.DEVICE_TYPE__GX) AND INSTR(NVL(pv_session_attributes,'-'),'P') = 0 AND REGEXP_LIKE(pv_firmware_version, 'USA-G5[0-9] ?[vV]4\.2\.(0|1[A-S]).*') THEN
			pv_session_attributes := NVL(pv_session_attributes,'') || 'P';
			pv_data_type := '88';
			pl_command := '4400807FE001';
			UPSERT_PENDING_COMMAND(pv_device_name, pv_data_type, pl_command, 'S', 2, pn_commands_inserted, pn_command_id);
		END IF;
	END; -- SP_NPC_HELPER_PHILLY_COKE

	/**
	* r29 version of SP_NEXT_PENDING_COMMAND
	* This is the feed of commands from the UI tools to the App Layer and thus the devices
	*/
	PROCEDURE SP_NEXT_PENDING_COMMAND(
		pv_device_name DEVICE.DEVICE_NAME%TYPE,
		pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
		pv_data_type OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
		pr_command_bytes OUT RAW,
		pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
		pv_file_transfer_name OUT FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
		pn_file_transfer_type_id OUT FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
		pl_file_transfer_content OUT FILE_TRANSFER.FILE_TRANSFER_CONTENT%TYPE,
		pn_file_transfer_group_num OUT DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
		pn_file_transfer_pkt_size OUT DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
		pd_file_transfer_created_ts OUT DEVICE_FILE_TRANSFER.CREATED_TS%TYPE,
		pn_max_execute_order ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE DEFAULT 999,
		pn_priority_command_id ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE DEFAULT NULL,
		pv_session_attributes IN OUT VARCHAR2, -- contains character 'C' if configuration has been requested previously
		pv_global_session_cd VARCHAR2 DEFAULT NULL,
		pn_update_status NUMBER DEFAULT 0,
		pc_v4_messages CHAR DEFAULT '?')
	IS
		ll_command ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE;
		ln_device_id DEVICE.DEVICE_ID%TYPE;
		ln_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE;
		lv_firmware_version DEVICE.FIRMWARE_VERSION%TYPE;
		lv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE;
		ld_call_in_time_normalized_ts DEVICE.CALL_IN_TIME_NORMALIZED_TS%TYPE;
		lv_device_time_zone_guid VARCHAR2(60);
		ln_result_cd NUMBER;
		lv_activated_call_in_schedule DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
		lv_non_activ_call_in_schedule DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
		lv_settlement_schedule DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
		lv_dex_schedule DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
		ln_command_id ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE;
		ln_file_transfer_id FILE_TRANSFER.FILE_TRANSFER_ID%TYPE;
		l_addr NUMBER;
		l_len NUMBER;
		l_pos NUMBER;
		l_commands_inserted NUMBER;
		ln_sending_file_cnt NUMBER;
		ln_sending_file_limit NUMBER := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DOWNLOAD_LIMIT_APP_UPGRADE'));
		ln_max_file_download_attempts NUMBER(3,0) := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('MAX_FILE_DOWNLOAD_ATTEMPTS'));
		ln_max_fw_upg_attempts NUMBER(4,0) := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DEVICE_FIRMWARE_UPGRADE_MAX_ATTEMPTS'));
		ln_fw_upg_hours_to_wait NUMBER(3,0) := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DEVICE_FIRMWARE_UPGRADE_HOURS_TO_WAIT_FOR_DEVICE'));
		lv_file_content_hex VARCHAR2(1024);
		lv_data_type ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE;
		lv_command ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE;
		lv_last_pending_cmd_session_cd VARCHAR2(100);
		lc_upgrade_throttling_enabled COMM_PROVIDER.UPGRADE_THROTTLING_ENABLED%TYPE;
		ln_upgrade_cap_bytes COMM_PROVIDER.UPGRADE_CAP_BYTES%TYPE;
		ln_billing_period_start COMM_PROVIDER.BILLING_PERIOD_START%TYPE;
		ld_billing_period_start_ts DATE;
		ln_average_bytes_transferred DATA_TRANSFER.BYTES_RECEIVED%TYPE := 0;
		ln_active_billing_devices DATA_TRANSFER.DEVICE_COUNT%TYPE;
		lc_comm_method_cd DEVICE.COMM_METHOD_CD%TYPE;
		ln_comm_provider_id COMM_PROVIDER.COMM_PROVIDER_ID%TYPE;
		ln_fw_upg_max_step_attempts NUMBER;
		ln_current_fw_upg_step_attempt DEVICE_FIRMWARE_UPGRADE.CURRENT_FW_UPG_STEP_ATTEMPT%TYPE;
		ln_device_file_transfer_id DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_ID%TYPE := 0;
		ln_device_fw_upg_id DEVICE_FIRMWARE_UPGRADE.DEVICE_FIRMWARE_UPGRADE_ID%TYPE;
		ln_firmware_upgrade_id FIRMWARE_UPGRADE.FIRMWARE_UPGRADE_ID%TYPE;
		ln_complete_fw_upg_step_id DEVICE_FIRMWARE_UPGRADE.COMPLETE_FW_UPG_STEP_ID%TYPE;
		lv_error_message DEVICE_FIRMWARE_UPGRADE.ERROR_MESSAGE%TYPE;
		ln_step_id FIRMWARE_UPGRADE_STEP.FIRMWARE_UPGRADE_STEP_ID%TYPE;
		ln_step_number FIRMWARE_UPGRADE_STEP.STEP_NUMBER%TYPE;
		lv_step_name FIRMWARE_UPGRADE_STEP.FIRMWARE_UPGRADE_STEP_NAME%TYPE;
		ln_file_id FILE_TRANSFER.FILE_TRANSFER_ID%TYPE;
		lv_peek_command ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE;
		ln_file_size NUMBER;
		ln_packet_size NUMBER;
		lv_branch_param_cd FIRMWARE_UPGRADE_STEP.BRANCH_PARAM_CD%TYPE;
		lv_branch_value_regex FIRMWARE_UPGRADE_STEP.BRANCH_VALUE_REGEX%TYPE;
		lv_target_param_cd FIRMWARE_UPGRADE_STEP.TARGET_PARAM_CD%TYPE;
		lv_target_version FIRMWARE_UPGRADE_STEP.TARGET_VERSION%TYPE;
		lv_target_value_regex FIRMWARE_UPGRADE_STEP.TARGET_VALUE_REGEX%TYPE;
		lv_current_version DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
		lv_current_value VARCHAR2(255);
		lv_current_value_ts DATE;
		lv_last_fw_upg_file_ts ENGINE.MACHINE_CMD_PENDING_HIST.EXECUTE_DATE%TYPE;
		lv_app_setting_value ENGINE.APP_SETTING.APP_SETTING_VALUE%TYPE;
		lv_restrict_firmware_update DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
		ln_requirement_met NUMBER;
		lv_new_firmware_version VARCHAR2(20);
		ln_cancel_command NUMBER(1,0);
		ln_loop_count NUMBER := 0;
		ld_sysdate DATE := SYSDATE;
	BEGIN
		SELECT D.DEVICE_ID, D.DEVICE_TYPE_ID, D.DEVICE_SERIAL_CD, D.CALL_IN_TIME_NORMALIZED_TS, COALESCE(T_TZ.TIME_ZONE_GUID, TZ.TIME_ZONE_GUID, 'US/Eastern'), D.FIRMWARE_VERSION,
				 D.COMM_METHOD_CD, CP.COMM_PROVIDER_ID, CP.UPGRADE_THROTTLING_ENABLED, CP.UPGRADE_CAP_BYTES, CP.BILLING_PERIOD_START, DS.DEVICE_SETTING_VALUE
		INTO ln_device_id, ln_device_type_id, lv_device_serial_cd, ld_call_in_time_normalized_ts, lv_device_time_zone_guid, lv_firmware_version,
				 lc_comm_method_cd, ln_comm_provider_id, lc_upgrade_throttling_enabled, ln_upgrade_cap_bytes, ln_billing_period_start, lv_restrict_firmware_update
		FROM DEVICE.DEVICE D
		LEFT OUTER JOIN PSS.POS P ON D.DEVICE_ID = P.DEVICE_ID
		LEFT OUTER JOIN LOCATION.LOCATION L ON P.LOCATION_ID = L.LOCATION_ID
		LEFT OUTER JOIN LOCATION.TIME_ZONE TZ ON L.LOCATION_TIME_ZONE_CD = TZ.TIME_ZONE_CD
		LEFT OUTER JOIN REPORT.EPORT E ON D.DEVICE_SERIAL_CD = E.EPORT_SERIAL_NUM
		LEFT OUTER JOIN REPORT.VW_TERMINAL_EPORT TE ON E.EPORT_ID = TE.EPORT_ID
		LEFT OUTER JOIN REPORT.TERMINAL T ON TE.TERMINAL_ID = T.TERMINAL_ID
		LEFT OUTER JOIN REPORT.TIME_ZONE T_TZ ON T.TIME_ZONE_ID = T_TZ.TIME_ZONE_ID
		LEFT OUTER JOIN DEVICE.COMM_METHOD CM ON D.COMM_METHOD_CD = CM.COMM_METHOD_CD
		LEFT OUTER JOIN DEVICE.COMM_PROVIDER CP ON CM.COMM_PROVIDER_ID = CP.COMM_PROVIDER_ID
		LEFT OUTER JOIN DEVICE.DEVICE_SETTING DS ON D.DEVICE_ID = DS.DEVICE_ID AND DS.DEVICE_SETTING_PARAMETER_CD = 'RESTRICT_FIRMWARE_UPDATE'
		WHERE D.DEVICE_NAME = pv_device_name
			AND D.DEVICE_ACTIVE_YN_FLAG = 'Y';

		IF lv_restrict_firmware_update = 'Y' THEN
			UPDATE ENGINE.MACHINE_CMD_PENDING MCP
			SET EXECUTE_CD = 'R', EXECUTE_DATE = SYSDATE, GLOBAL_SESSION_CD = pv_global_session_cd
			WHERE MCP.MACHINE_ID = pv_device_name AND EXECUTE_CD IN ('P', 'S') AND (
				MCP.DEVICE_FILE_TRANSFER_ID > 0 AND EXISTS (
					SELECT 1 FROM DEVICE.DEVICE_FILE_TRANSFER DFT
					JOIN DEVICE.FILE_TRANSFER FT ON DFT.FILE_TRANSFER_ID = FT.FILE_TRANSFER_ID
					JOIN DEVICE.FILE_TRANSFER_TYPE FTT ON FT.FILE_TRANSFER_TYPE_CD = FTT.FILE_TRANSFER_TYPE_CD
					WHERE DFT.DEVICE_FILE_TRANSFER_ID = MCP.DEVICE_FILE_TRANSFER_ID AND FTT.FIRMWARE_UPDATE_IND = 'Y'
				)
				OR MCP.DATA_TYPE = 'FW_UPG' AND EXISTS (
					SELECT 1 FROM DEVICE.DEVICE_FIRMWARE_UPGRADE DFU
					WHERE DFU.DEVICE_FIRMWARE_UPGRADE_ID = MCP.DEVICE_FIRMWARE_UPGRADE_ID
				)
			);
		ELSE
			UPDATE ENGINE.MACHINE_CMD_PENDING MCP
			SET EXECUTE_CD = 'P', EXECUTE_DATE = SYSDATE, GLOBAL_SESSION_CD = pv_global_session_cd
			WHERE MCP.MACHINE_ID = pv_device_name AND EXECUTE_CD = 'R' AND (
				MCP.DEVICE_FILE_TRANSFER_ID > 0 AND EXISTS (
					SELECT 1 FROM DEVICE.DEVICE_FILE_TRANSFER DFT
					JOIN DEVICE.FILE_TRANSFER FT ON DFT.FILE_TRANSFER_ID = FT.FILE_TRANSFER_ID
					JOIN DEVICE.FILE_TRANSFER_TYPE FTT ON FT.FILE_TRANSFER_TYPE_CD = FTT.FILE_TRANSFER_TYPE_CD
					WHERE DFT.DEVICE_FILE_TRANSFER_ID = MCP.DEVICE_FILE_TRANSFER_ID AND FTT.FIRMWARE_UPDATE_IND = 'Y'
				)
				OR MCP.DATA_TYPE = 'FW_UPG' AND EXISTS (
					SELECT 1 FROM DEVICE.DEVICE_FIRMWARE_UPGRADE DFU
					WHERE DFU.DEVICE_FIRMWARE_UPGRADE_ID = MCP.DEVICE_FIRMWARE_UPGRADE_ID
				)
			);
		END IF;

		IF lv_device_serial_cd LIKE 'K3%' AND pn_update_status = PKG_CONST.UPDATE_STATUS__NORMAL AND pv_global_session_cd IS NOT NULL THEN
			BEGIN
				SELECT TO_CHAR(LAST_PENDING_COMMAND_ID), LAST_PENDING_CMD_SESSION_CD
				INTO ln_command_id, lv_last_pending_cmd_session_cd
				FROM DEVICE.DEVICE_INFO
				WHERE DEVICE_NAME = pv_device_name;

				IF ln_command_id IS NOT NULL THEN
					UPDATE ENGINE.MACHINE_CMD_PENDING
					SET EXECUTE_CD = 'A', GLOBAL_SESSION_CD = pv_global_session_cd
					WHERE MACHINE_COMMAND_PENDING_ID = ln_command_id
					  AND REGEXP_LIKE(DATA_TYPE, DECODE(pc_v4_messages, '?', '^[0-9A-Fa-f]{2,4}$', 'N', '^[0-9A-Ba-b][0-9A-Fa-f]{1,3}$', 'Y', '^C[0-9A-Fa-f]{1,3}$'))
					RETURNING DATA_TYPE, COMMAND, DEVICE_FILE_TRANSFER_ID INTO lv_data_type, lv_command, ln_device_file_transfer_id;

					IF ln_device_file_transfer_id > 0 THEN
						UPDATE DEVICE.DEVICE_FILE_TRANSFER
						SET DEVICE_FILE_TRANSFER_STATUS_CD = 1,
							DEVICE_FILE_TRANSFER_TS = SYSDATE,
							GLOBAL_SESSION_CD = lv_last_pending_cmd_session_cd
						WHERE DEVICE_FILE_TRANSFER_ID = ln_device_file_transfer_id;
					END IF;

					UPDATE DEVICE.DEVICE_INFO
					SET LAST_PENDING_COMMAND_ID = NULL,
						LAST_PENDING_CMD_SESSION_CD = NULL
					WHERE DEVICE_NAME = pv_device_name;
				END IF;
			EXCEPTION
				WHEN NO_DATA_FOUND THEN
					NULL;
			END;
		END IF;

		LOOP
			pn_command_id := NULL;
			pv_data_type := NULL;
			ll_command := NULL;
			ln_device_file_transfer_id := 0;
			ln_device_fw_upg_id := 0;

			ln_loop_count := ln_loop_count + 1;
			IF ln_loop_count > 100 THEN
				-- make sure we don't enter an infinite loop
				EXIT;
			END IF;

			-- throttle the number of concurrent app upgrade file downloads
			SELECT COUNT(1) INTO ln_sending_file_cnt
			FROM engine.machine_cmd_pending mcp
			WHERE mcp.execute_cd = 'S' AND mcp.execute_date > SYSDATE - 15/1440 AND (
				mcp.device_file_transfer_id > 0 AND EXISTS (
					SELECT 1 FROM device.device_file_transfer dft
					JOIN device.file_transfer ft ON dft.file_transfer_id = ft.file_transfer_id
					JOIN DEVICE.FILE_TRANSFER_TYPE FTT ON FT.FILE_TRANSFER_TYPE_CD = FTT.FILE_TRANSFER_TYPE_CD
					WHERE dft.device_file_transfer_id = mcp.device_file_transfer_id AND FTT.FIRMWARE_UPDATE_IND = 'Y'
				)
				OR mcp.data_type = 'FW_UPG' AND EXISTS (
					SELECT 1 FROM DEVICE.DEVICE_FIRMWARE_UPGRADE DFU
					WHERE DFU.DEVICE_FIRMWARE_UPGRADE_ID = mcp.device_firmware_upgrade_id
				)
			);

			IF lc_upgrade_throttling_enabled = 'Y' AND ln_sending_file_cnt < ln_sending_file_limit THEN
				ld_billing_period_start_ts := TRUNC(ld_sysdate, 'MONTH') + ln_billing_period_start - 1;
				IF ld_billing_period_start_ts > ld_sysdate THEN
					ld_billing_period_start_ts := ADD_MONTHS(TRUNC(ld_sysdate, 'MONTH'), -1) + ln_billing_period_start - 1;
				END IF;

				SELECT CASE WHEN SUM(DEVICE_COUNT) > 0 THEN SUM(BYTES_TOTAL) / SUM(DEVICE_COUNT) END
				INTO ln_average_bytes_transferred
				FROM (
				SELECT DT.COMM_METHOD_CD, SUM(DT.BYTES_RECEIVED) + SUM(DT.BYTES_SENT) BYTES_TOTAL, MAX(DT.DEVICE_COUNT) DEVICE_COUNT
				FROM DEVICE.DATA_TRANSFER DT
				JOIN DEVICE.COMM_METHOD CM ON DT.COMM_METHOD_CD = CM.COMM_METHOD_CD
				WHERE DT.DATA_TRANSFER_TS >= ld_billing_period_start_ts
				AND CM.COMM_PROVIDER_ID = ln_comm_provider_id
		 		GROUP BY DT.COMM_METHOD_CD);
			END IF;

			IF ln_sending_file_cnt >= ln_sending_file_limit OR lc_upgrade_throttling_enabled = 'Y' AND ln_average_bytes_transferred >= ln_upgrade_cap_bytes THEN
				-- mark commands Capped or Throttled
				UPDATE ENGINE.MACHINE_CMD_PENDING MCP
				SET EXECUTE_CD = CASE WHEN lc_upgrade_throttling_enabled = 'Y' AND ln_average_bytes_transferred >= ln_upgrade_cap_bytes THEN 'E'
					ELSE 'T' END,
					EXECUTE_DATE = SYSDATE, GLOBAL_SESSION_CD = pv_global_session_cd
				WHERE MCP.MACHINE_ID = pv_device_name AND EXECUTE_CD IN ('P', 'S') AND (
					MCP.DEVICE_FILE_TRANSFER_ID > 0 AND MCP.DATA_TYPE != 'C7' AND EXISTS (
						SELECT 1 FROM DEVICE.DEVICE_FILE_TRANSFER DFT
						JOIN DEVICE.FILE_TRANSFER FT ON DFT.FILE_TRANSFER_ID = FT.FILE_TRANSFER_ID
						JOIN DEVICE.FILE_TRANSFER_TYPE FTT ON FT.FILE_TRANSFER_TYPE_CD = FTT.FILE_TRANSFER_TYPE_CD
						WHERE DFT.DEVICE_FILE_TRANSFER_ID = MCP.DEVICE_FILE_TRANSFER_ID AND FTT.FIRMWARE_UPDATE_IND = 'Y'
					)
					OR MCP.DATA_TYPE = 'FW_UPG' AND EXISTS (
						SELECT 1 FROM DEVICE.DEVICE_FIRMWARE_UPGRADE DFU
						WHERE DFU.DEVICE_FIRMWARE_UPGRADE_ID = MCP.DEVICE_FIRMWARE_UPGRADE_ID
					)
				);
			ELSE
				-- mark commands Pending again
				UPDATE ENGINE.MACHINE_CMD_PENDING MCP
				SET EXECUTE_CD = 'P', EXECUTE_DATE = SYSDATE, GLOBAL_SESSION_CD = pv_global_session_cd
				WHERE MCP.MACHINE_ID = pv_device_name AND EXECUTE_CD IN ('E', 'T') AND (
					MCP.DEVICE_FILE_TRANSFER_ID > 0 AND MCP.DATA_TYPE != 'C7' AND EXISTS (
						SELECT 1 FROM DEVICE.DEVICE_FILE_TRANSFER DFT
						JOIN DEVICE.FILE_TRANSFER FT ON DFT.FILE_TRANSFER_ID = FT.FILE_TRANSFER_ID
						JOIN DEVICE.FILE_TRANSFER_TYPE FTT ON FT.FILE_TRANSFER_TYPE_CD = FTT.FILE_TRANSFER_TYPE_CD
						WHERE DFT.DEVICE_FILE_TRANSFER_ID = MCP.DEVICE_FILE_TRANSFER_ID AND FTT.FIRMWARE_UPDATE_IND = 'Y'
					)
					OR MCP.DATA_TYPE = 'FW_UPG' AND EXISTS (
						SELECT 1 FROM DEVICE.DEVICE_FIRMWARE_UPGRADE DFU
						WHERE DFU.DEVICE_FIRMWARE_UPGRADE_ID = MCP.DEVICE_FIRMWARE_UPGRADE_ID
					)
				);
			END IF;

			IF ln_max_file_download_attempts > 0 THEN
				-- cancel file downloads that exceeded MAX_FILE_DOWNLOAD_ATTEMPTS and any pending commands that exceed MAX_FILE_DOWNLOAD_ATTEMPTS * 4
				UPDATE ENGINE.MACHINE_CMD_PENDING
				SET EXECUTE_CD = 'C'
				WHERE MACHINE_ID = pv_device_name
					AND EXECUTE_CD IN('S', 'P')
					AND (
						DEVICE_FILE_TRANSFER_ID IS NOT NULL AND ATTEMPT_COUNT >= ln_max_file_download_attempts
						OR ATTEMPT_COUNT >= ln_max_fw_upg_attempts
					);
			END IF;

			UPDATE ENGINE.MACHINE_CMD_PENDING MCP
			   SET EXECUTE_CD = 'S', EXECUTE_DATE = SYSDATE, GLOBAL_SESSION_CD = pv_global_session_cd, ATTEMPT_COUNT = COALESCE(ATTEMPT_COUNT, 0) + 1
			 WHERE MCP.MACHINE_ID = pv_device_name
			   AND MCP.EXECUTE_ORDER <= pn_max_execute_order
			   AND MCP.EXECUTE_CD IN('S', 'P')
			   AND (REGEXP_LIKE(DATA_TYPE, DECODE(pc_v4_messages, '?', '^[0-9A-Fa-f]{2,4}$', 'N', '^[0-9A-Ba-b][0-9A-Fa-f]{1,3}$', 'Y', '^C[0-9A-Fa-f]{1,3}$'))
					OR DATA_TYPE = 'FW_UPG' AND NOT EXISTS (
						SELECT 1 FROM DEVICE.DEVICE_FIRMWARE_UPGRADE DFU
						WHERE DFU.DEVICE_FIRMWARE_UPGRADE_ID = MCP.DEVICE_FIRMWARE_UPGRADE_ID
							AND DFU.LAST_GLOBAL_SESSION_CD = pv_global_session_cd
					)
			   )
			   AND NOT EXISTS(
							SELECT 1
							  FROM ENGINE.MACHINE_CMD_PENDING MCP2
							 WHERE MCP.MACHINE_ID = MCP2.MACHINE_ID
							   AND MCP2.EXECUTE_CD IN('S', 'P')
							   AND (REGEXP_LIKE(DATA_TYPE, DECODE(pc_v4_messages, '?', '^[0-9A-Fa-f]{2,4}$', 'N', '^[0-9A-Ba-b][0-9A-Fa-f]{1,3}$', 'Y', '^C[0-9A-Fa-f]{1,3}$'))
									OR DATA_TYPE = 'FW_UPG' AND NOT EXISTS (
										SELECT 1 FROM DEVICE.DEVICE_FIRMWARE_UPGRADE DFU
										WHERE DFU.DEVICE_FIRMWARE_UPGRADE_ID = MCP2.DEVICE_FIRMWARE_UPGRADE_ID
											AND DFU.LAST_GLOBAL_SESSION_CD = pv_global_session_cd
									)
							   )
							   AND MCP.MACHINE_COMMAND_PENDING_ID != MCP2.MACHINE_COMMAND_PENDING_ID
							   AND MCP.EXECUTE_ORDER >= MCP2.EXECUTE_ORDER
							   AND (MCP.EXECUTE_ORDER > MCP2.EXECUTE_ORDER
								   OR MCP2.MACHINE_COMMAND_PENDING_ID = pn_priority_command_id
								   OR (MCP.MACHINE_COMMAND_PENDING_ID != NVL(pn_priority_command_id, 0)
										AND MCP.CREATED_TS > MCP2.CREATED_TS
										OR (MCP.CREATED_TS = MCP2.CREATED_TS AND MCP.MACHINE_COMMAND_PENDING_ID > MCP2.MACHINE_COMMAND_PENDING_ID))))
				RETURNING MACHINE_COMMAND_PENDING_ID, DATA_TYPE, COMMAND, DEVICE_FILE_TRANSFER_ID, DEVICE_FIRMWARE_UPGRADE_ID
				INTO pn_command_id, pv_data_type, ll_command, ln_device_file_transfer_id, ln_device_fw_upg_id;

			IF pn_command_id IS NULL THEN
				IF ln_device_type_id IN (PKG_CONST.DEVICE_TYPE__G4, PKG_CONST.DEVICE_TYPE__GX, PKG_CONST.DEVICE_TYPE__EDGE, PKG_CONST.DEVICE_TYPE__MEI)
					AND NVL(ld_call_in_time_normalized_ts, MIN_DATE) < SYSDATE - TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DEVICE_CALL_IN_NORMALIZATION_INTERVAL_HR')) / 24 THEN
					SP_NORMALIZE_CALL_IN_TIME(ln_device_id, ln_device_type_id, lv_device_serial_cd, lv_device_time_zone_guid, ln_result_cd, lv_activated_call_in_schedule, lv_non_activ_call_in_schedule, lv_settlement_schedule, lv_dex_schedule, pn_command_id, pv_data_type, ll_command);
					IF ln_result_cd = 1 THEN
						IF ln_device_type_id = PKG_CONST.DEVICE_TYPE__EDGE THEN
							pv_data_type := 'C7';
							ADD_PENDING_FILE_TRANSFER(pv_device_name, pv_device_name || '-CFG-' || DBADMIN.TIMESTAMP_TO_MILLIS(SYSTIMESTAMP),
								PKG_CONST.FILE_TYPE__PROPERTY_LIST, pv_data_type, 0, 1024, 1, ln_command_id, ln_file_transfer_id, ln_device_file_transfer_id);
							pn_command_id := ln_command_id;

							UPDATE ENGINE.MACHINE_CMD_PENDING MCP
							SET
								GLOBAL_SESSION_CD = pv_global_session_cd,
								ATTEMPT_COUNT = COALESCE(ATTEMPT_COUNT, 0) + 1
							WHERE
								MACHINE_COMMAND_PENDING_ID = pn_command_id
								AND GLOBAL_SESSION_CD IS NULL
								AND pn_command_id IS NOT NULL;

							UPDATE DEVICE.FILE_TRANSFER
							SET FILE_TRANSFER_CONTENT = RAWTOHEX(DECODE(lv_settlement_schedule, PKG_CONST.ASCII__NUL, NULL, PKG_CONST.DSP__SETTLEMENT_SCHEDULE || '=' || lv_settlement_schedule || PKG_CONST.ASCII__LF)
							  || DECODE(lv_non_activ_call_in_schedule, PKG_CONST.ASCII__NUL, NULL, PKG_CONST.DSP__NON_ACTIV_CALL_IN_SCHED || '=' || lv_non_activ_call_in_schedule || PKG_CONST.ASCII__LF)
							  || DECODE(lv_activated_call_in_schedule, PKG_CONST.ASCII__NUL, NULL, PKG_CONST.DSP__ACTIVATED_CALL_IN_SCHED || '=' || lv_activated_call_in_schedule || PKG_CONST.ASCII__LF)
							  || DECODE(lv_dex_schedule, PKG_CONST.ASCII__NUL, NULL, PKG_CONST.DSP__DEX_SCHEDULE || '=' || lv_dex_schedule || PKG_CONST.ASCII__LF))
							WHERE FILE_TRANSFER_ID = ln_file_transfer_id;
						ELSE
							UPDATE ENGINE.MACHINE_CMD_PENDING MCP
							SET EXECUTE_CD = 'S', EXECUTE_DATE = SYSDATE, GLOBAL_SESSION_CD = pv_global_session_cd, ATTEMPT_COUNT = COALESCE(ATTEMPT_COUNT, 0) + 1
							WHERE MACHINE_COMMAND_PENDING_ID = pn_command_id;
						END IF;
					END IF;
				END IF;
				IF pn_command_id IS NULL THEN
					SP_NPC_HELPER_MODEM(pv_device_name, pn_command_id, pv_data_type, pv_session_attributes, ln_device_type_id, l_commands_inserted, ll_command);
					IF pn_command_id IS NULL THEN
						SP_NPC_HELPER_STALE_CONFIG(pv_device_name, pn_command_id, pv_data_type, pv_session_attributes, ln_device_type_id, l_commands_inserted, ll_command);
						IF pn_command_id IS NULL THEN
							SP_NPC_HELPER_ESUDS_DIAG(pv_device_name, pn_command_id, pv_data_type, pv_session_attributes, ln_device_type_id, l_commands_inserted, ll_command);
							IF pn_command_id IS NULL THEN
								SP_NPC_HELPER_PHILLY_COKE(pv_device_name, pn_command_id, pv_data_type, pv_session_attributes, ln_device_type_id, lv_firmware_version, l_commands_inserted, ll_command);
							END IF;
						END IF;
					END IF;
				END IF;
			END IF;

			IF pn_command_id IS NULL THEN
				EXIT;
			END IF;

			IF ln_device_file_transfer_id > 0 OR pv_data_type = 'FW_UPG' THEN
				IF pv_data_type = 'FW_UPG' OR ln_device_fw_upg_id > 0 THEN
					IF pv_data_type = 'FW_UPG' THEN
						-- cancel existing firmware upgrades
						UPDATE ENGINE.MACHINE_CMD_PENDING MCP
						SET EXECUTE_CD = 'C', GLOBAL_SESSION_CD = pv_global_session_cd
						WHERE MACHINE_ID = pv_device_name AND DEVICE_FILE_TRANSFER_ID > 0 AND EXECUTE_ORDER > 0 AND EXISTS (
							SELECT 1 FROM device.device_file_transfer dft
							JOIN device.file_transfer ft ON dft.file_transfer_id = ft.file_transfer_id
							JOIN DEVICE.FILE_TRANSFER_TYPE FTT ON FT.FILE_TRANSFER_TYPE_CD = FTT.FILE_TRANSFER_TYPE_CD
							WHERE dft.device_file_transfer_id = mcp.device_file_transfer_id AND FTT.FIRMWARE_UPDATE_IND = 'Y'
						);
					END IF;

					SELECT NVL(MAX(DFU.FIRMWARE_UPGRADE_ID), 0), NVL(MAX(DFU.COMPLETE_FW_UPG_STEP_ID), 0),
						NVL(MAX(DFU.CURRENT_FW_UPG_STEP_ID), 0), NVL(MAX(FUS.FILE_TRANSFER_ID), 0), NVL(MAX(DBMS_LOB.GETLENGTH(FT.FILE_TRANSFER_CONTENT)), 0), NVL(MAX(FUS.STEP_NUMBER), 0),
						MAX(FUS.TARGET_PARAM_CD), MAX(FUS.TARGET_VERSION), MAX(FUS.TARGET_VALUE_REGEX),
						MAX(FUS.FIRMWARE_UPGRADE_STEP_NAME), MAX(FUS.BRANCH_PARAM_CD), MAX(FUS.BRANCH_VALUE_REGEX)
					INTO ln_firmware_upgrade_id, ln_complete_fw_upg_step_id,
						ln_step_id, ln_file_id, ln_file_size, ln_step_number, lv_target_param_cd, lv_target_version, lv_target_value_regex,
						lv_step_name, lv_branch_param_cd, lv_branch_value_regex
					FROM DEVICE.DEVICE_FIRMWARE_UPGRADE DFU
					LEFT OUTER JOIN DEVICE.FIRMWARE_UPGRADE_STEP FUS ON DFU.CURRENT_FW_UPG_STEP_ID = FUS.FIRMWARE_UPGRADE_STEP_ID AND FUS.STATUS_CD = 'A'
					LEFT OUTER JOIN DEVICE.FILE_TRANSFER FT ON FUS.FILE_TRANSFER_ID = FT.FILE_TRANSFER_ID
					WHERE DFU.DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id AND DFU.DEVICE_FW_UPG_STATUS_CD = 'I';

					IF pv_data_type = 'FW_UPG' THEN
						IF ln_firmware_upgrade_id < 1 THEN
							-- device firmware upgrade is no longer incomplete, cancel command
							UPDATE ENGINE.MACHINE_CMD_PENDING
							SET EXECUTE_CD = 'C'
							WHERE MACHINE_COMMAND_PENDING_ID = pn_command_id;
							CONTINUE;
						END IF;

						IF ln_step_id = 0 OR ln_step_id = ln_complete_fw_upg_step_id THEN
							-- no current step or current step is complete, find next step
							SELECT NVL(MAX(FIRMWARE_UPGRADE_STEP_ID), 0), NVL(MAX(FILE_TRANSFER_ID), 0), NVL(MAX(DBMS_LOB.GETLENGTH(FILE_TRANSFER_CONTENT)), 0),
								MAX(STEP_NUMBER), MAX(TARGET_PARAM_CD), MAX(TARGET_VERSION), MAX(TARGET_VALUE_REGEX),
								MAX(FIRMWARE_UPGRADE_STEP_NAME), MAX(BRANCH_PARAM_CD), MAX(BRANCH_VALUE_REGEX)
							INTO ln_step_id, ln_file_id, ln_file_size, ln_step_number, lv_target_param_cd, lv_target_version, lv_target_value_regex,
								lv_step_name, lv_branch_param_cd, lv_branch_value_regex
							FROM (
								SELECT FUS.FIRMWARE_UPGRADE_STEP_ID, FUS.FILE_TRANSFER_ID, FT.FILE_TRANSFER_CONTENT, FUS.STEP_NUMBER, FUS.TARGET_PARAM_CD, FUS.TARGET_VERSION,
									FUS.TARGET_VALUE_REGEX, FUS.FIRMWARE_UPGRADE_STEP_NAME, FUS.BRANCH_PARAM_CD, FUS.BRANCH_VALUE_REGEX
								FROM DEVICE.FIRMWARE_UPGRADE_STEP FUS
								JOIN DEVICE.FILE_TRANSFER FT ON FUS.FILE_TRANSFER_ID = FT.FILE_TRANSFER_ID
								WHERE FUS.FIRMWARE_UPGRADE_ID = ln_firmware_upgrade_id AND FUS.STATUS_CD = 'A' AND FUS.STEP_NUMBER > ln_step_number
								ORDER BY FUS.STEP_NUMBER
							) WHERE ROWNUM = 1;

							IF ln_step_id > 0 THEN
								UPDATE DEVICE.DEVICE_FIRMWARE_UPGRADE
								SET CURRENT_FW_UPG_STEP_ATTEMPT = 0
								WHERE DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id AND CURRENT_FW_UPG_STEP_ATTEMPT != 0;
							END IF;
						END IF;
					END IF;

					IF ln_step_id > 0 THEN
						IF ln_file_size <= 0 THEN
							lv_error_message := 'Size of file ID ' || ln_file_id || ' is ' || ln_file_size || ' bytes';
							UPDATE DEVICE.DEVICE_FIRMWARE_UPGRADE
							SET DEVICE_FW_UPG_STATUS_CD = 'F', DEVICE_FW_UPG_STATUS_TS = SYSDATE,
								ERROR_MESSAGE = SUBSTR(CASE WHEN ERROR_MESSAGE LIKE '%' || lv_error_message || '%' THEN ERROR_MESSAGE
									WHEN ERROR_MESSAGE IS NULL THEN lv_error_message
									ELSE ERROR_MESSAGE || '; ' || lv_error_message END, 1, 4000)
							WHERE DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id;
							CONTINUE;
						END IF;

						IF pv_data_type = 'FW_UPG' THEN
							-- get the timestamp of the last successfully downloaded file as part of this firmware upgrade
							SELECT MAX(EXECUTE_DATE)
							INTO lv_last_fw_upg_file_ts
							FROM ENGINE.MACHINE_CMD_PENDING_HIST
							WHERE MACHINE_ID = pv_device_name AND EXECUTE_DATE > SYSDATE - ln_fw_upg_hours_to_wait/24 AND EXECUTE_CD = 'A' AND DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id;
						
							IF ln_device_type_id = PKG_CONST.DEVICE_TYPE__GX THEN
								lv_current_version := REGEXP_REPLACE(GET_DEVICE_SETTING(ln_device_id, 'Diagnostic App Rev'), 'Diag |Diagnostic = N/A', '');
								lv_app_setting_value := DBADMIN.PKG_GLOBAL.GET_APP_SETTING(CASE lc_comm_method_cd WHEN 'C' THEN 'GX_CDMA' ELSE 'GX' END || '_MIN_DIAGNOSTIC_VERSION_FOR_INTERNAL_FILE_TRANSFER_UPGRADES');
								IF REGEXP_LIKE(lv_current_version, '[0-9]') AND DBADMIN.VERSION_COMPARE(lv_current_version, lv_app_setting_value) < 0 THEN
									lv_error_message := 'Minimum required Gx diagnostic version for internal file transfers is ' || lv_app_setting_value;
									UPDATE DEVICE.DEVICE_FIRMWARE_UPGRADE
									SET DEVICE_FW_UPG_STATUS_CD = 'F', DEVICE_FW_UPG_STATUS_TS = SYSDATE,
										ERROR_MESSAGE = SUBSTR(CASE WHEN ERROR_MESSAGE LIKE '%' || lv_error_message || '%' THEN ERROR_MESSAGE
											WHEN ERROR_MESSAGE IS NULL THEN lv_error_message
											ELSE ERROR_MESSAGE || '; ' || lv_error_message END, 1, 4000)
									WHERE DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id;
									CONTINUE;
								END IF;

								lv_current_version := GET_DEVICE_SETTING(ln_device_id, 'Firmware Version');
								IF ln_device_type_id IN (PKG_CONST.DEVICE_TYPE__G4, PKG_CONST.DEVICE_TYPE__GX) THEN
									lv_current_version := SUBSTR(lv_current_version, 9);
								END IF;
								lv_app_setting_value := DBADMIN.PKG_GLOBAL.GET_APP_SETTING(CASE lc_comm_method_cd WHEN 'C' THEN 'GX_CDMA' ELSE 'GX' END || '_MIN_FIRMWARE_VERSION_FOR_INTERNAL_FILE_TRANSFER_UPGRADES');
								IF REGEXP_LIKE(lv_current_version, '[0-9]') AND DBADMIN.VERSION_COMPARE(lv_current_version, lv_app_setting_value) < 0 THEN
									lv_error_message := 'Minimum required Gx firmware version for internal file transfers is ' || lv_app_setting_value;
									UPDATE DEVICE.DEVICE_FIRMWARE_UPGRADE
									SET DEVICE_FW_UPG_STATUS_CD = 'F', DEVICE_FW_UPG_STATUS_TS = SYSDATE,
										ERROR_MESSAGE = SUBSTR(CASE WHEN ERROR_MESSAGE LIKE '%' || lv_error_message || '%' THEN ERROR_MESSAGE
											WHEN ERROR_MESSAGE IS NULL THEN lv_error_message
											ELSE ERROR_MESSAGE || '; ' || lv_error_message END, 1, 4000)
									WHERE DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id;
									CONTINUE;
								END IF;
							END IF;

							UPDATE DEVICE.DEVICE_FIRMWARE_UPGRADE
							SET CURRENT_FW_UPG_STEP_ID = ln_step_id,
								CURRENT_FW_UPG_STEP_TS = SYSDATE,
								CURRENT_FW_UPG_STEP_ATTEMPT = CURRENT_FW_UPG_STEP_ATTEMPT + 1
							WHERE DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id
							RETURNING CURRENT_FW_UPG_STEP_ATTEMPT INTO ln_current_fw_upg_step_attempt;

							ln_fw_upg_max_step_attempts := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DEVICE_FIRMWARE_UPGRADE_MAX_STEP_ATTEMPTS'));
							IF ln_current_fw_upg_step_attempt > ln_fw_upg_max_step_attempts THEN
								lv_error_message := 'Exceeded maximum attempts ' || ln_fw_upg_max_step_attempts || ' for step # ' || ln_step_number || ': ' || lv_step_name;
								UPDATE DEVICE.DEVICE_FIRMWARE_UPGRADE
								SET DEVICE_FW_UPG_STATUS_CD = 'F', DEVICE_FW_UPG_STATUS_TS = SYSDATE,
									ERROR_MESSAGE = SUBSTR(CASE WHEN ERROR_MESSAGE LIKE '%' || lv_error_message || '%' THEN ERROR_MESSAGE
										WHEN ERROR_MESSAGE IS NULL THEN lv_error_message
										ELSE ERROR_MESSAGE || '; ' || lv_error_message END, 1, 4000)
								WHERE DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id;
								CONTINUE;
							END IF;

							IF lv_branch_param_cd IS NOT NULL AND lv_branch_value_regex IS NOT NULL THEN
								lv_current_value := NULL;
								IF lv_branch_param_cd = 'Bezel Mfgr' THEN
									SELECT MAX(he.host_equipment_mfgr), MAX(GREATEST(h.last_updated_ts, he.last_updated_ts))
									INTO lv_current_value, lv_current_value_ts
									FROM device.host h
									JOIN device.host_equipment he ON h.host_equipment_id = he.host_equipment_id
									WHERE h.device_id = ln_device_id AND h.host_type_id = 400;
								END IF;

								IF lv_current_value IS NULL THEN
									-- no current value, exit out of firmware upgrade until next session
									lv_error_message := lv_branch_param_cd || ' is NULL';
									UPDATE DEVICE.DEVICE_FIRMWARE_UPGRADE
									SET LAST_GLOBAL_SESSION_CD = pv_global_session_cd,
										ERROR_MESSAGE = SUBSTR(CASE WHEN ERROR_MESSAGE LIKE '%' || lv_error_message || '%' THEN ERROR_MESSAGE
											WHEN ERROR_MESSAGE IS NULL THEN lv_error_message
											ELSE ERROR_MESSAGE || '; ' || lv_error_message END, 1, 4000),
										CURRENT_FW_UPG_STEP_ATTEMPT = CURRENT_FW_UPG_STEP_ATTEMPT - 1
									WHERE DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id;
									CONTINUE;
								END IF;
								
								IF lv_last_fw_upg_file_ts IS NOT NULL AND lv_current_value_ts IS NOT NULL AND lv_current_value_ts < lv_last_fw_upg_file_ts THEN
									-- The device hasn't uploaded the new host info yet, we'll wait for it. If we still don't receive it after ln_fw_upg_hours_to_wait hours, we'll resend the file.
									lv_error_message := 'No new host info from device';
									UPDATE DEVICE.DEVICE_FIRMWARE_UPGRADE
									SET LAST_GLOBAL_SESSION_CD = pv_global_session_cd,
										ERROR_MESSAGE = SUBSTR(CASE WHEN ERROR_MESSAGE LIKE '%' || lv_error_message || '%' THEN ERROR_MESSAGE
											WHEN ERROR_MESSAGE IS NULL THEN lv_error_message
											ELSE ERROR_MESSAGE || '; ' || lv_error_message END, 1, 4000),
										CURRENT_FW_UPG_STEP_ATTEMPT = CURRENT_FW_UPG_STEP_ATTEMPT - 1
									WHERE DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id;
									CONTINUE;
								END IF;

								IF lv_branch_param_cd = 'Bezel Mfgr' AND lv_current_value = 'USAT' THEN
									-- this could be because the bezel firmware upgrade went wrong, try to re-send the bezel firmware file
									SELECT NVL(MAX(FILE_TRANSFER_ID), ln_file_id)
									INTO ln_file_id
									FROM (
										SELECT FT.FILE_TRANSFER_ID
										FROM ENGINE.MACHINE_CMD_PENDING_HIST MCPH
										JOIN DEVICE.DEVICE_FILE_TRANSFER DFT ON MCPH.DEVICE_FILE_TRANSFER_ID = DFT.DEVICE_FILE_TRANSFER_ID
										JOIN DEVICE.FILE_TRANSFER FT ON DFT.FILE_TRANSFER_ID = FT.FILE_TRANSFER_ID
										WHERE MACHINE_ID = pv_device_name AND MCPH.DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id
											AND DFT.DEVICE_FILE_TRANSFER_STATUS_CD = 1 AND FT.FILE_TRANSFER_TYPE_CD = PKG_CONST.FILE_TYPE__CARD_READER_APP
										ORDER BY DFT.DEVICE_FILE_TRANSFER_TS
									) WHERE ROWNUM = 1;
								ELSIF NOT REGEXP_LIKE(lv_current_value, lv_branch_value_regex) THEN
									-- current value doesn't match the branch regex, mark step as complete
									UPDATE DEVICE.DEVICE_FIRMWARE_UPGRADE
									SET COMPLETE_FW_UPG_STEP_ID = ln_step_id,
										COMPLETE_FW_UPG_STEP_TS = SYSDATE
									WHERE DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id;
									CONTINUE;
								END IF;
							END IF;
						END IF;

						lv_peek_command := NULL;
						IF lv_target_param_cd IS NOT NULL AND (REGEXP_LIKE(lv_target_version, '[0-9]') OR lv_target_value_regex IS NOT NULL) THEN
							IF lv_target_param_cd = 'Bezel App Rev' THEN
								SELECT MAX(hs.host_setting_value), MAX(hs.last_updated_ts)
								INTO lv_current_value, lv_current_value_ts
								FROM device.host h
								JOIN device.host_setting hs ON h.host_id = hs.host_id AND hs.host_setting_parameter = 'Application Version'
								WHERE h.device_id = ln_device_id AND h.host_type_id = 400;
							ELSIF lv_target_param_cd = 'Bezel Mfgr' THEN
								SELECT MAX(he.host_equipment_mfgr), MAX(GREATEST(h.last_updated_ts, he.last_updated_ts))
								INTO lv_current_value, lv_current_value_ts
								FROM device.host h
								JOIN device.host_equipment he ON h.host_equipment_id = he.host_equipment_id
								WHERE h.device_id = ln_device_id AND h.host_type_id = 400;
							ELSE
								SELECT MAX(DEVICE_SETTING_VALUE), MAX(LAST_UPDATED_TS)
								INTO lv_current_value, lv_current_value_ts
								FROM DEVICE.DEVICE_SETTING
								WHERE DEVICE_ID = ln_device_id AND DEVICE_SETTING_PARAMETER_CD = lv_target_param_cd;
							END IF;
							
							IF lv_last_fw_upg_file_ts IS NOT NULL AND lv_current_value_ts IS NOT NULL AND lv_current_value_ts < lv_last_fw_upg_file_ts THEN
								-- The device hasn't uploaded the new version string yet, we'll wait for it. If we still don't receive it after ln_fw_upg_hours_to_wait hours, we'll resend the file.
								lv_error_message := 'No new version from device';
								UPDATE DEVICE.DEVICE_FIRMWARE_UPGRADE
								SET LAST_GLOBAL_SESSION_CD = pv_global_session_cd,
									ERROR_MESSAGE = SUBSTR(CASE WHEN ERROR_MESSAGE LIKE '%' || lv_error_message || '%' THEN ERROR_MESSAGE
										WHEN ERROR_MESSAGE IS NULL THEN lv_error_message
										ELSE ERROR_MESSAGE || '; ' || lv_error_message END, 1, 4000),
									CURRENT_FW_UPG_STEP_ATTEMPT = CURRENT_FW_UPG_STEP_ATTEMPT - 1
								WHERE DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id;
								CONTINUE;
							END IF;

							IF lv_target_param_cd = 'Bezel App Rev' THEN
								lv_peek_command := '4400807EB20000004C';
								lv_current_version := REGEXP_REPLACE(lv_current_value, 'EC5 GR ', '');
							ELSIF lv_target_param_cd = 'Bootloader Rev' THEN
								lv_peek_command := '4400807E9000000021';
								lv_current_version := REGEXP_REPLACE(lv_current_value, 'BtLdr |Bootloader = N/A', '');
							ELSIF lv_target_param_cd = 'Diagnostic App Rev' THEN
								lv_peek_command := '4400807E9000000021';
								lv_current_version := REGEXP_REPLACE(lv_current_value, 'Diag |Diagnostic = N/A', '');
							ELSIF lv_target_param_cd = 'Firmware Version' THEN
								lv_peek_command := '4400007F200000000F';
								lv_current_version := lv_current_value;
								IF ln_device_type_id IN (PKG_CONST.DEVICE_TYPE__G4, PKG_CONST.DEVICE_TYPE__GX) THEN
									lv_current_version := SUBSTR(lv_current_version, 9);
								END IF;
							ELSIF lv_target_param_cd = 'PTest Rev' THEN
								lv_current_version := REGEXP_REPLACE(lv_current_value, 'PTest |PTEST	  = N/A', '');
							ELSE
								lv_current_version := lv_current_value;
							END IF;

							ln_requirement_met := 0;
							IF REGEXP_LIKE(lv_target_version, '[0-9]') AND lv_target_value_regex IS NOT NULL THEN
								IF DBADMIN.VERSION_COMPARE(lv_current_version, lv_target_version) >= 0 AND REGEXP_LIKE(lv_current_value, lv_target_value_regex) THEN
									ln_requirement_met := 1;
								END IF;
							ELSIF REGEXP_LIKE(lv_target_version, '[0-9]') THEN
								IF DBADMIN.VERSION_COMPARE(lv_current_version, lv_target_version) >= 0 THEN
									ln_requirement_met := 1;
								END IF;
							ELSIF lv_target_value_regex IS NOT NULL THEN
								IF REGEXP_LIKE(lv_current_value, lv_target_value_regex) THEN
									ln_requirement_met := 1;
								END IF;
							END IF;

							IF ln_requirement_met = 1 THEN
								-- target version requirement met, mark step as complete
								UPDATE DEVICE.DEVICE_FIRMWARE_UPGRADE
								SET COMPLETE_FW_UPG_STEP_ID = ln_step_id,
									COMPLETE_FW_UPG_STEP_TS = SYSDATE
								WHERE DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id;

								IF pv_data_type != 'FW_UPG' AND ln_device_file_transfer_id > 0 THEN
									-- cancel current pending file transfer if target requirement is met
									UPDATE ENGINE.MACHINE_CMD_PENDING
									SET EXECUTE_CD = 'C'
									WHERE MACHINE_COMMAND_PENDING_ID = pn_command_id;
								END IF;

								CONTINUE;
							END IF;
						END IF;

						IF pv_data_type = 'FW_UPG' THEN
							IF ln_device_type_id = PKG_CONST.DEVICE_TYPE__GX THEN
								ln_packet_size := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('GX_PACKET_SIZE'));
							ELSIF ln_device_type_id = PKG_CONST.DEVICE_TYPE__KIOSK AND lv_device_serial_cd LIKE 'K3%' THEN
								ln_packet_size := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EPORT_CONNECT_PACKET_SIZE'));
							ELSE
								ln_packet_size := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DEFAULT_PACKET_SIZE'));
							END IF;

							IF ln_device_type_id IN (PKG_CONST.DEVICE_TYPE__GX, PKG_CONST.DEVICE_TYPE__KIOSK) THEN
								pv_data_type := 'A4';
							ELSIF ln_device_type_id = PKG_CONST.DEVICE_TYPE__EDGE THEN
								IF ln_file_size > ln_packet_size THEN
									pv_data_type := 'C8';
								ELSE
									pv_data_type := 'C7';
								END IF;
							ELSE
								pv_data_type := '7C';
							END IF;

							SELECT SEQ_DEVICE_FILE_TRANSFER_ID.NEXTVAL INTO ln_device_file_transfer_id FROM DUAL;

							INSERT INTO device.device_file_transfer(device_file_transfer_id, device_id, file_transfer_id, device_file_transfer_direct, device_file_transfer_status_cd, device_file_transfer_pkt_size)
							VALUES(ln_device_file_transfer_id, ln_device_id, ln_file_id, 'O', 0, ln_packet_size);

							SELECT ENGINE.SEQ_MACHINE_CMD_PENDING_ID.NEXTVAL INTO pn_command_id FROM DUAL;

							INSERT INTO engine.machine_cmd_pending (machine_command_pending_id, machine_id, data_type, execute_cd, execute_order, execute_date, global_session_cd, device_file_transfer_id, device_firmware_upgrade_id)
							SELECT /*+INDEX(mcp IDX_MACHINE_CMD_PENDING_1)*/ pn_command_id, pv_device_name, pv_data_type, 'S', COALESCE(MAX(execute_order), 0) + 1, SYSDATE, pv_global_session_cd, ln_device_file_transfer_id, ln_device_fw_upg_id
							FROM engine.machine_cmd_pending mcp
							WHERE machine_id = pv_device_name;

							IF ln_device_type_id != PKG_CONST.DEVICE_TYPE__GX OR lv_target_param_cd IS NULL OR lv_target_version IS NULL AND lv_target_value_regex IS NULL THEN
								-- applayer will mark step as complete when it receives file download ack from device
								UPDATE DEVICE.DEVICE_FIRMWARE_UPGRADE
								SET CURRENT_PENDING_COMMAND_ID = pn_command_id
								WHERE DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id;
							END IF;

							IF lv_peek_command IS NOT NULL AND ln_device_type_id = PKG_CONST.DEVICE_TYPE__GX THEN
								INSERT INTO engine.machine_cmd_pending (machine_id, data_type, command, execute_cd, execute_order, device_firmware_upgrade_id)
								VALUES(pv_device_name, '87', lv_peek_command, 'P', -1, ln_device_fw_upg_id);
							END IF;
						END IF;
					ELSIF pv_data_type = 'FW_UPG' THEN
						UPDATE DEVICE.DEVICE_FIRMWARE_UPGRADE
						SET DEVICE_FW_UPG_STATUS_CD = 'A', DEVICE_FW_UPG_STATUS_TS = SYSDATE
						WHERE DEVICE_FIRMWARE_UPGRADE_ID = ln_device_fw_upg_id;

						UPDATE ENGINE.MACHINE_CMD_PENDING
						SET EXECUTE_CD = 'A'
						WHERE MACHINE_COMMAND_PENDING_ID = pn_command_id;

						CONTINUE;
					END IF;
				END IF;

				IF ln_device_file_transfer_id > 0 THEN
					SELECT MAX(ft.FILE_TRANSFER_ID), MAX(ft.FILE_TRANSFER_NAME), MAX(ft.FILE_TRANSFER_TYPE_CD),
						   MAX(dft.DEVICE_FILE_TRANSFER_GROUP_NUM), MAX(dft.DEVICE_FILE_TRANSFER_PKT_SIZE), MAX(dft.CREATED_TS)
					  INTO pn_file_transfer_id, pv_file_transfer_name, pn_file_transfer_type_id,
						   pn_file_transfer_group_num, pn_file_transfer_pkt_size, pd_file_transfer_created_ts
					  FROM DEVICE.DEVICE_FILE_TRANSFER dft
					  JOIN DEVICE.FILE_TRANSFER ft on dft.FILE_TRANSFER_ID = ft.FILE_TRANSFER_ID
					 WHERE DEVICE_FILE_TRANSFER_ID = ln_device_file_transfer_id;

					IF pn_file_transfer_id IS NULL THEN
						-- device file transfer no longer exists, cancel command
						UPDATE ENGINE.MACHINE_CMD_PENDING
						SET EXECUTE_CD = 'C'
						WHERE MACHINE_COMMAND_PENDING_ID = pn_command_id;
						CONTINUE;
					END IF;

					IF pn_file_transfer_type_id = PKG_CONST.FILE_TYPE__APP_UPGRADE THEN
						ln_cancel_command := 0;
						IF ln_device_type_id = PKG_CONST.DEVICE_TYPE__GX THEN
							SELECT COALESCE(UTL_RAW.CAST_TO_VARCHAR2(DBMS_LOB.SUBSTR(FILE_TRANSFER_CONTENT, 19, 82)), '-')
							INTO lv_new_firmware_version
							FROM DEVICE.FILE_TRANSFER
							WHERE FILE_TRANSFER_ID = pn_file_transfer_id;

							IF lv_new_firmware_version NOT LIKE 'USA-%' AND lv_new_firmware_version NOT LIKE 'Diag%' AND lv_new_firmware_version NOT LIKE 'PTest%' THEN
								ln_cancel_command := 1;
							END IF;
						ELSIF ln_device_type_id = PKG_CONST.DEVICE_TYPE__EDGE THEN
							SELECT COALESCE(UTL_RAW.CAST_TO_VARCHAR2(DBMS_LOB.SUBSTR(FILE_TRANSFER_CONTENT, 10, 93)), '-')
							INTO lv_new_firmware_version
							FROM DEVICE.FILE_TRANSFER
							WHERE FILE_TRANSFER_ID = pn_file_transfer_id;

							IF lv_device_serial_cd LIKE 'VJ%' THEN
								IF NOT REGEXP_LIKE(lv_new_firmware_version, '^2\.0[2-9]\.') THEN
									ln_cancel_command := 1;
								END IF;
								IF lc_comm_method_cd = 'G' THEN
									IF DBADMIN.VERSION_COMPARE(lv_new_firmware_version, '2.02.008i') < 0 THEN
										ln_cancel_command := 1;
									END IF;
								END IF;
							ELSIF lv_device_serial_cd LIKE 'EE%' THEN
								IF lc_comm_method_cd = 'C' THEN
									IF lv_new_firmware_version NOT LIKE '1.02.%' THEN
										ln_cancel_command := 1;
									END IF;
								ELSIF lc_comm_method_cd = 'G' THEN
									IF lv_new_firmware_version NOT LIKE '1.00.%' AND lv_new_firmware_version NOT LIKE '1.01.%' THEN
										ln_cancel_command := 1;
									END IF;
								END IF;
							END IF;
						END IF;

						IF ln_cancel_command = 1 THEN
							-- incorrect file download, cancel command
							UPDATE ENGINE.MACHINE_CMD_PENDING
							SET EXECUTE_CD = 'C'
							WHERE MACHINE_COMMAND_PENDING_ID = pn_command_id;
							CONTINUE;
						END IF;
					END IF;

					SP_GET_FILE_TRANSFER_BLOB(pn_file_transfer_id, pl_file_transfer_content);
				ELSE
					CONTINUE;
				END IF;
			ELSIF pv_data_type = '88' THEN
				l_addr := to_number(substr(ll_command, 3, 8), 'XXXXXXXX');
				IF substr(ll_command, 1, 2) = '42' THEN -- EEROM: get data from device setting
					l_len := to_number(substr(ll_command, 11, 8), 'XXXXXXXX') * 2;

					IF ln_device_type_id in (PKG_CONST.DEVICE_TYPE__G4, PKG_CONST.DEVICE_TYPE__GX) THEN
						  l_pos := l_addr * 2;
					ELSE
						  l_pos := l_addr;
					END IF;

					l_pos := l_pos * 2 + 1;

					SP_GET_MAP_CONFIG_FILE(ln_device_id, lv_file_content_hex);
					pr_command_bytes := UTL_RAW.CONCAT(hextoraw(substr(ll_command, 1, 10)), SUBSTR(lv_file_content_hex, l_pos, l_len));
				ELSE
					pr_command_bytes := HEXTORAW(ll_command);
				END IF;
			ELSE
				pr_command_bytes := HEXTORAW(ll_command);
			END IF;

			EXIT;
		END LOOP;

		IF pn_command_id IS NOT NULL THEN
			IF lv_device_serial_cd LIKE 'K3%' THEN
				UPDATE DEVICE.DEVICE_INFO
				SET LAST_PENDING_COMMAND_ID = TO_NUMBER(pn_command_id),
					LAST_PENDING_CMD_SESSION_CD = pv_global_session_cd
				WHERE DEVICE_NAME = pv_device_name;

				IF SQL%NOTFOUND THEN
					BEGIN
						INSERT INTO DEVICE.DEVICE_INFO(DEVICE_NAME, LAST_PENDING_COMMAND_ID, LAST_PENDING_CMD_SESSION_CD)
						VALUES(pv_device_name, TO_NUMBER(pn_command_id), pv_global_session_cd);
					EXCEPTION
						WHEN DUP_VAL_ON_INDEX THEN
							UPDATE DEVICE.DEVICE_INFO
							SET LAST_PENDING_COMMAND_ID = TO_NUMBER(pn_command_id),
								LAST_PENDING_CMD_SESSION_CD = pv_global_session_cd
							WHERE DEVICE_NAME = pv_device_name;
					END;
				END IF;
			END IF;
		END IF;
	END;

	/**
	  * r29+ version that doesn't return command id but does return row count
	  */
	PROCEDURE UPSERT_PENDING_COMMAND(
		pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
		pv_date_type IN ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
		pv_command IN ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE,
		pv_execute_order IN ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE DEFAULT 999,
		pn_rows_inserted OUT PLS_INTEGER)
	IS
		ln_command_id ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE;
	BEGIN
		UPSERT_PENDING_COMMAND(pv_device_name,pv_date_type,pv_command,'P',pv_execute_order,pn_rows_inserted,ln_command_id);
	END;

	/** r29+ version
	  * Inserts a pending command into the table, and returns a command id,
	  * IF it does not already exist.
	  */
	PROCEDURE UPSERT_PENDING_COMMAND(
		pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
		pv_data_type IN ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
		pv_command IN ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE,
		pv_execute_cd IN ENGINE.MACHINE_CMD_PENDING.EXECUTE_CD%TYPE,
		pv_execute_order IN ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE DEFAULT 999,
		pn_rows_inserted OUT PLS_INTEGER,
		pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE)
	IS
		ld_execute_date ENGINE.MACHINE_CMD_PENDING.EXECUTE_DATE%TYPE;
		ln_cnt NUMBER;
	BEGIN
		SELECT COUNT(1)
		INTO ln_cnt
		FROM ENGINE.MACHINE_CMD_PENDING
		WHERE MACHINE_ID = pv_device_name
			AND DATA_TYPE = pv_data_type
			AND DBADMIN.PKG_UTL.EQL(COMMAND, pv_command) = 'Y';

		IF ln_cnt = 0 THEN
			SELECT DECODE(pv_execute_cd, 'S', SYSDATE, NULL) INTO ld_execute_date FROM DUAL;

			-- This isn't atomic so could result in duplicate pending commands but it's not critical to avoid duplicates only nice
			INSERT INTO ENGINE.MACHINE_CMD_PENDING(MACHINE_COMMAND_PENDING_ID, MACHINE_ID, DATA_TYPE, COMMAND, EXECUTE_CD, EXECUTE_DATE, EXECUTE_ORDER)
			VALUES(SEQ_MACHINE_CMD_PENDING_ID.NEXTVAL, pv_device_name, pv_data_type, pv_command, pv_execute_cd, ld_execute_date, pv_execute_order)
			RETURNING MACHINE_COMMAND_PENDING_ID INTO pn_command_id;

			pn_rows_inserted := SQL%ROWCOUNT;
		ELSE
			pn_rows_inserted := 0;
		END IF;
	END;

	PROCEDURE CONFIG_POKE(
		pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
		pv_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE)
	IS
		l_command ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE;
		l_file_len NUMBER;
		l_num_parts NUMBER;
		l_pos NUMBER;
		l_poke_size NUMBER:=200;
		l_len NUMBER;
		ln_rows_inserted PLS_INTEGER;
		ln_command_id ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE;
	BEGIN
		IF pv_device_type_id in (0, 1) THEN
			UPDATE ENGINE.MACHINE_CMD_PENDING
			SET EXECUTE_CD='C'
			WHERE MACHINE_ID = pv_device_name
			AND DATA_TYPE = '88'
			AND EXECUTE_ORDER > 2
			AND (EXECUTE_CD = 'P' OR (EXECUTE_CD = 'S' AND EXECUTE_DATE < (SYSDATE-(90/86400))));

			UPSERT_PENDING_COMMAND(pv_device_name, '88', '420000000000000088', 'P', -100, ln_rows_inserted, ln_command_id);
			UPSERT_PENDING_COMMAND(pv_device_name, '88', '42000000470000008E', 'P', -99, ln_rows_inserted, ln_command_id);
			UPSERT_PENDING_COMMAND(pv_device_name, '88', '42000000B20000009C', 'P', -98, ln_rows_inserted, ln_command_id);
		END IF;
	END;

	FUNCTION GET_DEVICE_ID_BY_NAME(
		pv_device_name DEVICE.DEVICE_NAME%TYPE,
		pd_effective_date DEVICE.CREATED_TS%TYPE)
	RETURN DEVICE.DEVICE_ID%TYPE
	IS
		ln_device_id DEVICE.DEVICE_ID%TYPE;
	BEGIN
		SELECT MAX(DEVICE_ID)
		  INTO ln_device_id
		  FROM DEVICE.DEVICE
		 WHERE DEVICE_ACTIVE_YN_FLAG = 'Y'
		   AND DEVICE_NAME = pv_device_name
		   AND CREATED_TS <= pd_effective_date;
		IF ln_device_id IS NULL THEN
			SELECT MAX(DEVICE_ID)
			  INTO ln_device_id
			  FROM (SELECT DEVICE_ID, CREATED_TS, NVL(LEAD(CREATED_TS) OVER (PARTITION BY DEVICE_NAME ORDER BY CREATED_TS), MAX_DATE) NEXT_CREATED_TS
					  FROM DEVICE.DEVICE
					 WHERE DEVICE_NAME = pv_device_name)
			 WHERE CREATED_TS <= pd_effective_date
			   AND NEXT_CREATED_TS > pd_effective_date;
			IF ln_device_id IS NULL THEN
				SELECT MAX(DEVICE_ID)
				  INTO ln_device_id
				  FROM (SELECT DEVICE_ID
						  FROM DEVICE.DEVICE
						 WHERE DEVICE_NAME = pv_device_name
						   AND CREATED_TS > pd_effective_date
						 ORDER BY CREATED_TS ASC)
				 WHERE ROWNUM = 1;
			END IF;
		END IF;
		RETURN ln_device_id;
	END;

	FUNCTION GET_DEVICE_ID_BY_SERIAL(
		pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
		pd_effective_date DEVICE.CREATED_TS%TYPE)
	RETURN DEVICE.DEVICE_ID%TYPE
	IS
		ln_device_id DEVICE.DEVICE_ID%TYPE;
	BEGIN
		SELECT MAX(DEVICE_ID)
		  INTO ln_device_id
		  FROM DEVICE.DEVICE
		 WHERE DEVICE_ACTIVE_YN_FLAG = 'Y'
		   AND DEVICE_SERIAL_CD = pv_device_serial_cd
		   AND CREATED_TS <= pd_effective_date;
		IF ln_device_id IS NULL THEN
			SELECT MAX(DEVICE_ID)
			  INTO ln_device_id
			  FROM (SELECT DEVICE_ID, CREATED_TS, NVL(LEAD(CREATED_TS) OVER (PARTITION BY DEVICE_SERIAL_CD ORDER BY CREATED_TS), MAX_DATE) NEXT_CREATED_TS
					  FROM DEVICE.DEVICE
					 WHERE DEVICE_SERIAL_CD = pv_device_serial_cd)
			 WHERE CREATED_TS <= pd_effective_date
			   AND NEXT_CREATED_TS > pd_effective_date;
		END IF;
		RETURN ln_device_id;
	END;

	PROCEDURE CLONE_GX_CONFIG (
		pn_source_device_id IN DEVICE.DEVICE_ID%TYPE,
		pn_target_device_id IN DEVICE.DEVICE_ID%TYPE,
		pn_part1_changed_count OUT NUMBER,
		pn_part2_changed_count OUT NUMBER,
		pn_part3_changed_count OUT NUMBER,
		pn_counters_changed_count OUT NUMBER)
	IS
	BEGIN
		/*
		 * # we are sending very specific sections of the config with the offsets below, not the whole thing for Gx
		 * # 0 - 135
		 * # 142 - 283
		 * # 356 - 511
		 */
		MERGE INTO DEVICE.DEVICE_SETTING O
		USING (
			SELECT DS_T.DEVICE_ID, DS_T.DEVICE_SETTING_PARAMETER_CD, DS_S.DEVICE_SETTING_VALUE
			FROM DEVICE.DEVICE_SETTING DS_T
			JOIN DEVICE.DEVICE_SETTING DS_S ON DS_T.DEVICE_SETTING_PARAMETER_CD = DS_S.DEVICE_SETTING_PARAMETER_CD
			WHERE DS_T.DEVICE_ID = pn_target_device_id
				AND DS_S.DEVICE_ID = pn_source_device_id
				AND DS_S.DEVICE_SETTING_PARAMETER_CD IN (
					SELECT DEVICE_SETTING_PARAMETER_CD
					FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS
					WHERE DEVICE_TYPE_ID = 0
						AND FIELD_OFFSET BETWEEN 0 AND 135
				) AND DBADMIN.PKG_UTL.EQL(DS_S.DEVICE_SETTING_VALUE, DS_T.DEVICE_SETTING_VALUE) = 'N') N
		ON (O.DEVICE_ID = N.DEVICE_ID AND O.DEVICE_SETTING_PARAMETER_CD = N.DEVICE_SETTING_PARAMETER_CD)
		WHEN MATCHED THEN
			UPDATE
				SET O.DEVICE_SETTING_VALUE = N.DEVICE_SETTING_VALUE;
		pn_part1_changed_count := SQL%ROWCOUNT;

		MERGE INTO DEVICE.DEVICE_SETTING O
		USING (
			SELECT DS_T.DEVICE_ID, DS_T.DEVICE_SETTING_PARAMETER_CD, DS_S.DEVICE_SETTING_VALUE
			FROM DEVICE.DEVICE_SETTING DS_T
			JOIN DEVICE.DEVICE_SETTING DS_S ON DS_T.DEVICE_SETTING_PARAMETER_CD = DS_S.DEVICE_SETTING_PARAMETER_CD
			WHERE DS_T.DEVICE_ID = pn_target_device_id
				AND DS_S.DEVICE_ID = pn_source_device_id
				AND DS_S.DEVICE_SETTING_PARAMETER_CD IN (
					SELECT DEVICE_SETTING_PARAMETER_CD
					FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS
					WHERE DEVICE_TYPE_ID = 0
						AND FIELD_OFFSET BETWEEN 142 AND 283
				) AND DBADMIN.PKG_UTL.EQL(DS_S.DEVICE_SETTING_VALUE, DS_T.DEVICE_SETTING_VALUE) = 'N') N
		ON (O.DEVICE_ID = N.DEVICE_ID AND O.DEVICE_SETTING_PARAMETER_CD = N.DEVICE_SETTING_PARAMETER_CD)
		WHEN MATCHED THEN
			UPDATE
				SET O.DEVICE_SETTING_VALUE = N.DEVICE_SETTING_VALUE;
		pn_part2_changed_count := SQL%ROWCOUNT;

		MERGE INTO DEVICE.DEVICE_SETTING O
		USING (
			SELECT DS_T.DEVICE_ID, DS_T.DEVICE_SETTING_PARAMETER_CD, DS_S.DEVICE_SETTING_VALUE
			FROM DEVICE.DEVICE_SETTING DS_T
			JOIN DEVICE.DEVICE_SETTING DS_S ON DS_T.DEVICE_SETTING_PARAMETER_CD = DS_S.DEVICE_SETTING_PARAMETER_CD
			WHERE DS_T.DEVICE_ID = pn_target_device_id
				AND DS_S.DEVICE_ID = pn_source_device_id
				AND DS_S.DEVICE_SETTING_PARAMETER_CD IN (
					SELECT DEVICE_SETTING_PARAMETER_CD
					FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS
					WHERE DEVICE_TYPE_ID = 0
						AND FIELD_OFFSET BETWEEN 356 AND 511
				) AND DBADMIN.PKG_UTL.EQL(DS_S.DEVICE_SETTING_VALUE, DS_T.DEVICE_SETTING_VALUE) = 'N') N
		ON (O.DEVICE_ID = N.DEVICE_ID AND O.DEVICE_SETTING_PARAMETER_CD = N.DEVICE_SETTING_PARAMETER_CD)
		WHEN MATCHED THEN
			UPDATE
				SET O.DEVICE_SETTING_VALUE = N.DEVICE_SETTING_VALUE;
		pn_part3_changed_count := SQL%ROWCOUNT;

		UPDATE DEVICE.DEVICE_SETTING
		SET DEVICE_SETTING_VALUE = '00000000'
		WHERE DEVICE_ID = pn_target_device_id
			AND DEVICE_SETTING_PARAMETER_CD IN (
				SELECT DEVICE_SETTING_PARAMETER_CD
				FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS
				WHERE DEVICE_TYPE_ID = 0
					AND FIELD_OFFSET BETWEEN 320 AND 352
			) AND DEVICE_SETTING_VALUE != '00000000';
		pn_counters_changed_count := SQL%ROWCOUNT;
	END;

	PROCEDURE INITIALIZE_DEVICE(
		pv_device_serial_cd IN DEVICE.DEVICE_SERIAL_CD%TYPE,
		pn_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
		pc_preregistering IN CHAR,
		pn_device_id OUT DEVICE.DEVICE_ID%TYPE,
		pv_device_name OUT DEVICE.DEVICE_NAME%TYPE,
		pv_device_type_desc OUT DEVICE_TYPE.DEVICE_TYPE_DESC%TYPE,
		pn_device_sub_type_id OUT DEVICE.DEVICE_SUB_TYPE_ID%TYPE,
		pc_prev_device_active_yn_flag OUT VARCHAR,
		pc_master_id_always_inc OUT VARCHAR,
		pn_key_gen_time OUT NUMBER,
		pc_legacy_safe_key OUT VARCHAR,
		pv_time_zone_guid OUT TIME_ZONE.TIME_ZONE_GUID%TYPE,
		pn_new_host_count OUT NUMBER,
		pn_result_cd OUT NUMBER,
		pv_error_message OUT VARCHAR2)
	IS
		ln_pos_pta_tmpl_id DEVICE_TYPE.POS_PTA_TMPL_ID%TYPE;
		lc_require_preregister DEVICE_TYPE.REQUIRE_PREREGISTER%TYPE;
		ln_prev_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE;
		ln_prev_device_sub_type_id  DEVICE.DEVICE_SUB_TYPE_ID%TYPE;
		ln_pos_id PSS.POS.POS_ID%TYPE;
		lv_app_type APP_TYPE.APP_TYPE_VALUE%TYPE;
		lv_prev_app_type APP_TYPE.APP_TYPE_VALUE%TYPE;
	BEGIN
		BEGIN
			SELECT DEVICE_SUB_TYPE_ID, DEVICE_SUB_TYPE_DESC, POS_PTA_TMPL_ID, REQUIRE_PREREGISTER, MASTER_ID_ALWAYS_INC
			 INTO pn_device_sub_type_id, pv_device_type_desc, ln_pos_pta_tmpl_id, lc_require_preregister, pc_master_id_always_inc
			 FROM
				(SELECT 1 PRIORITY, DEVICE_SUB_TYPE_ID, DEVICE_SUB_TYPE_DESC, POS_PTA_TMPL_ID, REQUIRE_PREREGISTER, MASTER_ID_ALWAYS_INC
				  FROM DEVICE.DEVICE_SUB_TYPE
				 WHERE DEVICE_TYPE_ID = pn_device_type_id
				   AND REGEXP_LIKE(pv_device_serial_cd, DEVICE_SERIAL_CD_REGEX)
				UNION ALL
				SELECT 2, NULL, DEVICE_TYPE_DESC, POS_PTA_TMPL_ID, REQUIRE_PREREGISTER, MASTER_ID_ALWAYS_INC
				  FROM DEVICE.DEVICE_TYPE
				 WHERE DEVICE_TYPE_ID = pn_device_type_id
				   AND REGEXP_LIKE(pv_device_serial_cd, DEVICE_TYPE_SERIAL_CD_REGEX)
			 ORDER BY PRIORITY)
			WHERE ROWNUM = 1;
			IF pn_device_sub_type_id IS NOT NULL THEN
				SELECT MAX(APP_TYPE_VALUE), NVL(MAX(POS_PTA_TMPL_ID), ln_pos_pta_tmpl_id)
				  INTO lv_app_type, ln_pos_pta_tmpl_id
				  FROM (SELECT APP_TYPE_VALUE, APP_TYPE_ID, POS_PTA_TMPL_ID
				  FROM DEVICE.APP_TYPE
				 WHERE DEVICE_TYPE_ID = pn_device_type_id
				   AND DEVICE_SUB_TYPE_ID = pn_device_sub_type_id
				   AND REGEXP_LIKE(pv_device_serial_cd, DEVICE_SERIAL_CD_REGEX)
				 ORDER BY APP_TYPE_ID DESC)
				 WHERE ROWNUM = 1;
			END IF;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				pn_result_cd := PKG_CONST.RESULT__FAILURE;
				pv_error_message := 'No match for device type and serial cd';
				RETURN;
		END;
		BEGIN
			SELECT DEVICE_ID, DEVICE_NAME, DEVICE_TYPE_ID, DEVICE_SUB_TYPE_ID, CASE WHEN REGEXP_LIKE(ENCRYPTION_KEY, '^[0-9]{16}$') THEN 'Y' ELSE 'N' END, DEVICE_ACTIVE_YN_FLAG, DATE_TO_MILLIS(DEVICE_ENCR_KEY_GEN_TS), APP_TYPE_VALUE
			  INTO pn_device_id, pv_device_name, ln_prev_device_type_id, ln_prev_device_sub_type_id, pc_legacy_safe_key, pc_prev_device_active_yn_flag, pn_key_gen_time, lv_prev_app_type
			  FROM (SELECT D.*, DS_APP.DEVICE_SETTING_VALUE APP_TYPE_VALUE
					  FROM DEVICE.DEVICE D
					  LEFT JOIN DEVICE.DEVICE_SETTING DS_APP ON D.DEVICE_ID = DS_APP.DEVICE_ID AND DS_APP.DEVICE_SETTING_PARAMETER_CD = 'App Type'
					 WHERE D.DEVICE_SERIAL_CD = pv_device_serial_cd
					 ORDER BY D.DEVICE_ACTIVE_YN_FLAG DESC, D.DEVICE_ID DESC, DS_APP.DEVICE_SETTING_VALUE DESC)
			  WHERE ROWNUM = 1;
			IF ln_prev_device_type_id != pn_device_type_id OR NVL(ln_prev_device_sub_type_id,0) != NVL(pn_device_sub_type_id, 0) THEN
				UPDATE DEVICE.DEVICE
				   SET DEVICE_TYPE_ID = pn_device_type_id,
					   DEVICE_SUB_TYPE_ID = pn_device_sub_type_id
				 WHERE DEVICE_ID = pn_device_id;
			END IF;
			IF lv_prev_app_type IS NULL AND lv_app_type IS NOT NULL THEN
				INSERT INTO DEVICE.DEVICE_SETTING(DEVICE_ID, DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_VALUE)
				  VALUES(pn_device_id, 'App Type', lv_app_type);
			ELSIF lv_prev_app_type != lv_app_type THEN
				UPDATE DEVICE.DEVICE_SETTING
				   SET DEVICE_SETTING_VALUE = lv_app_type
				 WHERE DEVICE_ID = pn_device_id
				   AND DEVICE_SETTING_PARAMETER_CD = 'App Type';
			ELSIF lv_prev_app_type IS NOT NULL AND lv_app_type IS NULL THEN
				DELETE
				  FROM DEVICE.DEVICE_SETTING
				 WHERE DEVICE_ID = pn_device_id
				   AND DEVICE_SETTING_PARAMETER_CD = 'App Type';
			END IF;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				IF pc_preregistering != 'Y' AND lc_require_preregister = 'Y' THEN
					pn_result_cd := PKG_CONST.RESULT__FAILURE;
					pv_error_message := 'Device type ''' || pv_device_type_desc ||''' requires pre-registration and this device is not registered';
					RETURN;
				END IF;
				SELECT SEQ_DEVICE_ID.NEXTVAL, SF_GENERATE_DEVICE_NAME(pn_device_type_id, pn_device_sub_type_id)
				  INTO pn_device_id, pv_device_name
				  FROM DUAL;
				INSERT INTO DEVICE.DEVICE(DEVICE_ID, DEVICE_NAME, DEVICE_TYPE_ID, DEVICE_SUB_TYPE_ID, DEVICE_SERIAL_CD, DEVICE_ACTIVE_YN_FLAG)
					VALUES(pn_device_id, pv_device_name, pn_device_type_id, pn_device_sub_type_id, pv_device_serial_cd, 'Y');
				IF lv_app_type IS NOT NULL THEN
					INSERT INTO DEVICE.DEVICE_SETTING(DEVICE_ID, DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_VALUE)
						VALUES(pn_device_id, 'App Type', lv_app_type);
				END IF;
		END;
		IF pn_device_type_id in (0,1,11,13) THEN
				  INSERT INTO DEVICE.DEVICE_SETTING(DEVICE_ID, DEVICE_SETTING_PARAMETER_CD)
				  SELECT pn_device_id,'Cashless Enabled' from dual
				  where not exists(select 1 from DEVICE.DEVICE_SETTING where device_id=pn_device_id and DEVICE_SETTING_PARAMETER_CD='Cashless Enabled');
		END IF;
		BEGIN
			SELECT P.POS_ID
			  INTO ln_pos_id
			  FROM PSS.POS P
			 WHERE P.DEVICE_ID = pn_device_id;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				SELECT PSS.SEQ_POS_ID.NEXTVAL
				  INTO ln_pos_id
				  FROM DUAL;
				INSERT INTO PSS.POS(POS_ID, DEVICE_ID)
					VALUES(ln_pos_id, pn_device_id);
		END;
	  SELECT MAX(T_TZ.TIME_ZONE_GUID)
		INTO pv_time_zone_guid
		FROM REPORT.EPORT E
		JOIN REPORT.VW_TERMINAL_EPORT TE ON E.EPORT_ID = TE.EPORT_ID
		JOIN REPORT.TERMINAL T ON TE.TERMINAL_ID = T.TERMINAL_ID
		JOIN REPORT.TIME_ZONE T_TZ ON T.TIME_ZONE_ID = T_TZ.TIME_ZONE_ID
	   WHERE E.EPORT_SERIAL_NUM = pv_device_serial_cd;
	  IF pv_time_zone_guid IS NULL THEN
		  SELECT MAX(TZ.TIME_ZONE_GUID)
			INTO pv_time_zone_guid
			FROM PSS.POS P
			JOIN LOCATION.LOCATION L ON P.LOCATION_ID = L.LOCATION_ID
			JOIN LOCATION.TIME_ZONE TZ ON L.LOCATION_TIME_ZONE_CD = TZ.TIME_ZONE_CD
		   WHERE P.POS_ID = ln_pos_id;
	  END IF;
		SP_CREATE_DEFAULT_HOSTS(pn_device_id, pn_new_host_count, pn_result_cd, pv_error_message);
		IF pn_result_cd != PKG_CONST.RESULT__SUCCESS THEN
			RETURN;
		END IF;
		PSS.PKG_POS_PTA.SP_IMPORT_POS_PTA_TEMPLATE(pn_device_id, ln_pos_pta_tmpl_id, 'S', 'AE', 'N', pn_result_cd, pv_error_message);
		IF pn_result_cd != PKG_CONST.RESULT__SUCCESS THEN
			RETURN;
		END IF;
		pn_result_cd := PKG_CONST.RESULT__SUCCESS;
		pv_error_message := PKG_CONST.ERROR__NO_ERROR;
	END;

	PROCEDURE NEXT_MASTER_ID_BY_SERIAL(
		pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
		pv_device_name OUT DEVICE.DEVICE_NAME%TYPE,
		pn_master_id OUT NUMBER)
	IS
		ln_device_id DEVICE.DEVICE_ID%TYPE;
	BEGIN
		-- increment and set masterId
		SELECT DEVICE_ID, DEVICE_NAME
		  INTO ln_device_id, pv_device_name
		  FROM DEVICE.DEVICE
		 WHERE DEVICE_SERIAL_CD = pv_device_serial_cd
		   AND DEVICE_ACTIVE_YN_FLAG = 'Y';
		LOOP
			UPDATE DEVICE.DEVICE_SETTING
			   SET DEVICE_SETTING_VALUE = GREATEST(DATE_TO_MILLIS(SYSDATE) / 1000, NVL(TO_NUMBER_OR_NULL(DEVICE_SETTING_VALUE), 0)) + 1
			 WHERE DEVICE_SETTING_PARAMETER_CD = 'VIRTUAL_MASTER_ID'
			   AND DEVICE_ID = ln_device_id
			 RETURNING TO_NUMBER_OR_NULL(DEVICE_SETTING_VALUE)
			  INTO pn_master_id;
			IF pn_master_id IS NOT NULL THEN
				RETURN;
			END IF;
			pn_master_id := DATE_TO_MILLIS(SYSDATE) / 1000;
			BEGIN
				INSERT INTO DEVICE.DEVICE_SETTING(DEVICE_ID, DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_VALUE)
					VALUES(ln_device_id, 'VIRTUAL_MASTER_ID', pn_master_id);
				RETURN;
			EXCEPTION
				WHEN DUP_VAL_ON_INDEX THEN
					pn_master_id := NULL;
			END;
		END LOOP;
	END;

	PROCEDURE GET_OR_CREATE_PREPAID_DEVICE(
		pn_customer_id IN PSS.POS.CUSTOMER_ID%TYPE,
		pn_location_id IN PSS.POS.LOCATION_ID%TYPE,
		pn_corp_customer_id IN PSS.CONSUMER_ACCT.CORP_CUSTOMER_ID%TYPE,
		pv_currency_cd IN PSS.CONSUMER_ACCT.CURRENCY_CD%TYPE,
		pn_device_id OUT DEVICE.DEVICE_ID%TYPE,
		pv_device_serial_cd OUT DEVICE.DEVICE_SERIAL_CD%TYPE,
		pv_device_name OUT DEVICE.DEVICE_NAME%TYPE)
	IS
		lv_device_type_desc DEVICE_TYPE.DEVICE_TYPE_DESC%TYPE;
		ln_device_sub_type_id DEVICE.DEVICE_SUB_TYPE_ID%TYPE;
		lc_prev_device_active_yn_flag VARCHAR(1);
		lc_master_id_always_inc VARCHAR(1);
		ln_key_gen_time NUMBER;
		lc_legacy_safe_key VARCHAR(1);
		lv_time_zone_guid TIME_ZONE.TIME_ZONE_GUID%TYPE;
		ln_new_host_count NUMBER;
		ln_result_cd NUMBER;
		lv_error_message VARCHAR2(1000);
		lv_lock VARCHAR2(128);
		ln_terminal_id REPORT.TERMINAL.TERMINAL_ID%TYPE;
		ln_dealer_id CORP.DEALER.DEALER_ID%TYPE;
		ln_eport_id REPORT.EPORT.EPORT_ID%TYPE;
		ln_customer_bank_id CORP.CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE;
		lv_customer_address1 CORP.CUSTOMER_ADDR.ADDRESS1%TYPE;
		lv_customer_city CORP.CUSTOMER_ADDR.CITY%TYPE;
		lv_customer_state_cd CORP.CUSTOMER_ADDR.STATE%TYPE;
		lv_customer_postal CORP.CUSTOMER_ADDR.ZIP%TYPE;
		lv_customer_country_cd CORP.CUSTOMER_ADDR.COUNTRY_CD%TYPE;
	BEGIN
		pv_device_serial_cd := 'V1-' || pn_corp_customer_id || '-' || pv_currency_cd;

		SELECT MAX(D.DEVICE_ID), MAX(D.DEVICE_NAME)
		INTO pn_device_id, pv_device_name
		FROM DEVICE.DEVICE D
		JOIN REPORT.EPORT E ON D.DEVICE_SERIAL_CD = E.EPORT_SERIAL_NUM
		JOIN REPORT.VW_TERMINAL_EPORT TE ON E.EPORT_ID = TE.EPORT_ID
		JOIN REPORT.TERMINAL T ON TE.TERMINAL_ID = T.TERMINAL_ID
		WHERE D.DEVICE_SERIAL_CD = pv_device_serial_cd
			AND D.DEVICE_ACTIVE_YN_FLAG = 'Y'
			AND T.CUSTOMER_ID = pn_corp_customer_id;

		IF pn_device_id > 0 THEN
			RETURN;
		END IF;

		lv_lock := PKG_GLOBAL.REQUEST_LOCK('DEVICE.DEVICE', pv_device_serial_cd);

		INITIALIZE_DEVICE(pv_device_serial_cd, 14, 'Y',
			pn_device_id,
			pv_device_name,
			lv_device_type_desc,
			ln_device_sub_type_id,
			lc_prev_device_active_yn_flag,
			lc_master_id_always_inc,
			ln_key_gen_time,
			lc_legacy_safe_key,
			lv_time_zone_guid,
			ln_new_host_count,
			ln_result_cd,
			lv_error_message
		);

		UPDATE PSS.POS
		SET CUSTOMER_ID = pn_customer_id, LOCATION_ID = pn_location_id
		WHERE DEVICE_ID = pn_device_id;

		IF pn_device_id > 0 THEN
			SELECT MAX(DEALER_ID)
			INTO ln_dealer_id
			FROM (
				SELECT DEALER_ID
				FROM CORP.DEALER_LICENSE
				WHERE LICENSE_ID IN (
					SELECT MAX(LICENSE_ID)
					FROM (
						SELECT L.LICENSE_ID
						FROM CORP.CUSTOMER_LICENSE CL
						JOIN CORP.LICENSE_NBR LN ON CL.LICENSE_NBR = LN.LICENSE_NBR
						JOIN CORP.LICENSE L ON LN.LICENSE_ID = L.LICENSE_ID
						WHERE CL.CUSTOMER_ID = pn_corp_customer_id AND L.STATUS = 'A'
						ORDER BY CL.RECEIVED DESC
					) WHERE ROWNUM = 1
				)
				AND SYSDATE >= NVL(START_DATE, MIN_DATE)
				AND SYSDATE < NVL(END_DATE, MAX_DATE)
				ORDER BY START_DATE DESC, DEALER_ID DESC
			) WHERE ROWNUM = 1;

			IF ln_dealer_id IS NULL THEN
				SELECT DEALER_ID
				INTO ln_dealer_id
				FROM (
					SELECT DEALER_ID
					FROM CORP.DEALER
					WHERE DEALER_NAME = 'USA Technologies'
					ORDER BY DEALER_ID
				) WHERE ROWNUM = 1;
			END IF;

			SELECT CUSTOMER_BANK_ID
			INTO ln_customer_bank_id
			FROM (
				SELECT CB.CUSTOMER_BANK_ID
				FROM CORP.CUSTOMER_BANK CB
				LEFT OUTER JOIN CORP.CUSTOMER_BANK_TERMINAL CBT ON CB.CUSTOMER_BANK_ID = CBT.CUSTOMER_BANK_ID
					AND SYSDATE >= NVL(CBT.START_DATE, MIN_DATE)
					AND SYSDATE < NVL(CBT.END_DATE, MAX_DATE)
				LEFT OUTER JOIN REPORT.TERMINAL T ON CBT.TERMINAL_ID = T.TERMINAL_ID AND T.STATUS != 'D'
				WHERE CB.CUSTOMER_ID = pn_corp_customer_id
				GROUP BY CB.CUSTOMER_BANK_ID
				ORDER BY COUNT(1) DESC, CB.CUSTOMER_BANK_ID
			) WHERE ROWNUM = 1;

			SELECT MAX(ADDRESS1), NVL(MAX(CITY), 'Malvern'), NVL(MAX(STATE), 'PA'), NVL(MAX(ZIP), '19355'), NVL(MAX(COUNTRY_CD), 'US')
			  INTO lv_customer_address1, lv_customer_city, lv_customer_state_cd, lv_customer_postal, lv_customer_country_cd
			  FROM CORP.CUSTOMER_ADDR
			 WHERE CUSTOMER_ID = pn_corp_customer_id;

			REPORT.GET_OR_CREATE_EPORT(ln_eport_id, pv_device_serial_cd, 14);
			CORP.DEALER_EPORT_UPD(ln_dealer_id, ln_eport_id);
			REPORT.PKG_CUSTOMER_MANAGEMENT.CREATE_TERMINAL_MASS(
				0, /* pn_user_id */
				ln_terminal_id, /* pn_terminal_id */
				pv_device_serial_cd, /* pv_device_serial_cd */
				ln_dealer_id, /* pn_dealer_id */
				'TBD', /* pv_asset_nbr */
				'TBD', /* pv_machine_make */
				'To Be Determined', /* pv_machine_model */
				NULL, /* pv_telephone */
				NULL, /* pv_prefix */
				NULL, /* pv_region_name */
				'TBD', /* pv_location_name */
				NULL, /* pv_location_details */
				lv_customer_address1, /* pv_address1 */
				lv_customer_city, /* pv_city */
				lv_customer_state_cd, /* pv_state_cd */
				lv_customer_postal, /* pv_postal */
				lv_customer_country_cd, /* pv_country_cd */
				ln_customer_bank_id, /* pn_customer_bank_id */
				1, /* pn_pay_sched_id */
				'- Not Specified -', /* pv_location_type_name */
				NULL, /* pn_location_type_specific */
				'Stored Value', /* pv_product_type_name */
				NULL, /* pn_product_type_specific */
				'?', /* pc_auth_mode */
				0, /* pn_avg_amt */
				0, /* pn_time_zone_id */
				'?', /* pc_dex_data */
				'?', /* pc_receipt */
				0, /* pn_vends */
				NULL, /* pv_term_var_1 */
				NULL, /* pv_term_var_2 */
				NULL, /* pv_term_var_3 */
				NULL, /* pv_term_var_4 */
				NULL, /* pv_term_var_5 */
				NULL, /* pv_term_var_6 */
				NULL, /* pv_term_var_7 */
				NULL, /* pv_term_var_8 */
				TRUNC(SYSDATE - 30), /* pd_activate_date */
				0, /* pn_fee_grace_days */
				'N', /* pc_keep_existing_data */
						'N' /* pc_override_payment_schedule */,
				NULL /* pv_doing_business_as */,
				NULL /* pc_fee_no_trigger_event_flag */,
				NULL /* pv_sales_order_number */,
				NULL /* pv_purchase_order_number */,
				NULL /* pc_pos_environment_cd */,
				NULL /* pv_entry_capability_cds */,
				NULL /* pc_pin_capability_flag */,
				NULL /* pn_commission_bank_id */,
				'Other Non-Traditional' /* pv_business_type_name */,
				NULL /* pv_customer_service_phone */,
				NULL /* pv_customer_service_email */
			);

			UPDATE REPORT.TERMINAL
			SET STATUS = 'A'
			WHERE TERMINAL_ID = ln_terminal_id AND STATUS != 'A';
		END IF;
	END;

	PROCEDURE UPDATE_APP_VERSION(
		pv_device_name DEVICE.DEVICE_NAME%TYPE,
		pn_protocol_version NUMBER,
		pv_app_type DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_app_version DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE)
	IS
		ln_device_id DEVICE.DEVICE_ID%TYPE;
		ln_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE;
		ln_device_sub_type_id DEVICE.DEVICE_SUB_TYPE_ID%TYPE;
	BEGIN
		SELECT DLA.DEVICE_ID, D.DEVICE_TYPE_ID, D.DEVICE_SUB_TYPE_ID
		  INTO ln_device_id, ln_device_type_id, ln_device_sub_type_id
		  FROM DEVICE.DEVICE_LAST_ACTIVE DLA
		  JOIN DEVICE.DEVICE D ON DLA.DEVICE_ID = D.DEVICE_ID
		 WHERE DLA.DEVICE_NAME = pv_device_name;
		INSERT INTO DEVICE.DEVICE_SETTING(DEVICE_ID, DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_VALUE)
			SELECT ln_device_id, A.NAME, A.VALUE
			  FROM (SELECT 'App Type' NAME, pv_app_type VALUE FROM DUAL
			  UNION ALL SELECT 'App Version', pv_app_version FROM DUAL
			  UNION ALL SELECT 'Property List Version', TO_CHAR(pn_protocol_version) FROM DUAL) A
			  LEFT OUTER JOIN DEVICE.DEVICE_SETTING O ON A.NAME = O.DEVICE_SETTING_PARAMETER_CD AND O.DEVICE_ID = ln_device_id
			 WHERE O.DEVICE_SETTING_PARAMETER_CD IS NULL;
		IF SQL%ROWCOUNT != 3 THEN
			UPDATE DEVICE.DEVICE_SETTING
			   SET DEVICE_SETTING_VALUE = DECODE(DEVICE_SETTING_PARAMETER_CD, 'App Type', pv_app_type, 'App Version', pv_app_version, 'Property List Version', TO_CHAR(pn_protocol_version))
			 WHERE DEVICE_ID = ln_device_id
			   AND DEVICE_SETTING_PARAMETER_CD IN('App Type', 'App Version','Property List Version')
			   AND DBADMIN.PKG_UTL.EQL(DEVICE_SETTING_VALUE, DECODE(DEVICE_SETTING_PARAMETER_CD, 'App Type', pv_app_type, 'App Version', pv_app_version, 'Property List Version', TO_CHAR(pn_protocol_version))) = 'N';
		END IF;
		ADD_DEFAULT_SETTINGS(ln_device_id, ln_device_type_id, ln_device_sub_type_id, pv_app_type, pn_protocol_version);
	END;

	PROCEDURE CREATE_PENDING_COMMAND(
		pv_machine_id ENGINE.MACHINE_CMD_PENDING.MACHINE_ID%TYPE,
		pv_data_type IN OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
		pv_command IN OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE,
		pv_execute_cd IN OUT ENGINE.MACHINE_CMD_PENDING.EXECUTE_CD%TYPE,
		pn_execute_order IN OUT ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE,
		pn_cancel_existing NUMBER,
		pn_file_transfer_id IN OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
		pn_check_existing_file NUMBER,
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pn_packet_size DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
		pn_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE,
		pn_command_count OUT NUMBER
	) IS
		ln_file_transfer_type_cd FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE;
		ln_device_file_transfer_id DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_ID%TYPE;
		ln_file_size NUMBER;
	BEGIN
		IF pn_file_transfer_id IS NULL THEN
			pn_file_transfer_id := 0;
		END IF;
		IF pn_file_transfer_id > 0 THEN
			SELECT FILE_TRANSFER_TYPE_CD, COALESCE(pv_command, RAWTOHEX(file_transfer_name)),
				CASE pn_device_type_id WHEN 13 THEN DBMS_LOB.GETLENGTH(FILE_TRANSFER_CONTENT) ELSE -1 END
			INTO ln_file_transfer_type_cd, pv_command, ln_file_size
			FROM DEVICE.FILE_TRANSFER
			WHERE FILE_TRANSFER_ID = pn_file_transfer_id;

			IF pn_device_type_id = 13 THEN
				IF ln_file_size > pn_packet_size THEN
					pv_data_type := 'C8';
				ELSE
					pv_data_type := 'C7';
				END IF;
			ELSIF pn_device_type_id IN (1, 11) THEN
				pv_data_type := 'A4';
			ELSE
				pv_data_type := '7C';
			END IF;

			IF pn_check_existing_file = 1 THEN
				select /*+INDEX(mcp IDX_MACHINE_CMD_PENDING_1)*/ count(1)
				into pn_command_count
				from device.device_file_transfer dft
				join engine.machine_cmd_pending mcp on dft.device_file_transfer_id = mcp.device_file_transfer_id
				where dft.device_id = pn_device_id and dft.file_transfer_id = pn_file_transfer_id and dft.device_file_transfer_status_cd = 0
				and dft.device_file_transfer_direct = 'O' and mcp.machine_id = pv_machine_id and mcp.data_type = pv_data_type;

				IF pn_command_count > 0 THEN
					RETURN;
				END IF;
			END IF;
		END IF;

		IF pn_cancel_existing = 1 THEN
			UPDATE engine.machine_cmd_pending
			SET execute_cd = 'C'
			WHERE machine_id = pv_machine_id
				AND data_type = pv_data_type;
		END IF;

		IF pn_file_transfer_id > 0 THEN
			UPDATE engine.machine_cmd_pending
			SET execute_cd = 'C'
			WHERE machine_command_pending_id IN (
				SELECT mcp.machine_command_pending_id
				FROM engine.machine_cmd_pending mcp
				JOIN device.device_file_transfer dft ON mcp.device_file_transfer_id = dft.device_file_transfer_id
				JOIN device.file_transfer ft ON dft.file_transfer_id = ft.file_transfer_id
				JOIN device.file_transfer_type ftt ON ft.file_transfer_type_cd = ftt.file_transfer_type_cd
				WHERE mcp.machine_id = pv_machine_id AND (
					ft.file_transfer_type_cd = ln_file_transfer_type_cd AND ftt.cancel_pending_transfers_ind = 'Y'
					OR mcp.data_type = '9B' AND mcp.command = pv_command
				)
			);

			SELECT SEQ_DEVICE_FILE_TRANSFER_ID.NEXTVAL INTO ln_device_file_transfer_id FROM DUAL;

			INSERT INTO device.device_file_transfer(device_file_transfer_id, device_id, file_transfer_id, device_file_transfer_direct, device_file_transfer_status_cd, device_file_transfer_pkt_size)
			VALUES(ln_device_file_transfer_id, pn_device_id, pn_file_transfer_id, 'O', 0, pn_packet_size);
		END IF;

		IF COALESCE(pn_execute_order, 0) < 1 THEN
			SELECT /*+INDEX(mcp IDX_MACHINE_CMD_PENDING_1)*/ NVL(MAX(execute_order), 0) + 1
			INTO pn_execute_order
			FROM engine.machine_cmd_pending
			WHERE machine_id = pv_machine_id;
		END IF;

		INSERT INTO engine.machine_cmd_pending (machine_id, data_type, command, execute_cd, execute_order, device_file_transfer_id)
		SELECT pv_machine_id, pv_data_type, pv_command, pv_execute_cd, pn_execute_order, ln_device_file_transfer_id
		FROM DUAL WHERE NOT EXISTS (
			SELECT 1 FROM engine.machine_cmd_pending
			WHERE machine_id = pv_machine_id
				AND data_type = pv_data_type
				AND DBADMIN.PKG_UTL.EQL(command, pv_command) = 'Y'
				AND DBADMIN.PKG_UTL.EQL(device_file_transfer_id, ln_device_file_transfer_id) = 'Y'
		);

		pn_command_count := SQL%ROWCOUNT;
	END;

END;
/
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/releases/REL_Edge_Update_R55/R55.USAPDB.VAS.sql?revision=HEAD
-- new consumer type
insert into pss.consumer_type(consumer_type_id, consumer_type_desc, more_enabled_ind, usalive_enabled_ind)
values(10, 'Apple VAS', 'Y','Y');
commit;

-- this will help us track pass type id for the NFC enabled cert. for USAT universal program, we have one static PASS_TYPE_ID defined in ENGINE.APP_SETTING once determined
-- the hash can be added so we don't have the recalculate the hash to save time, or we don't save the hash field
ALTER TABLE CORP.CUSTOMER ADD(PASS_TYPE_ID VARCHAR2(200), PASS_TYPE_ID_HASH VARCHAR2(200));
    
-- this table saves the tempalte id we customize in urbanairship REACH   
CREATE TABLE PSS.CUSTOMER_PASS_TEMPLATE(
    CUSTOMER_ID NUMBER NOT NULL,
    CONSUMER_ACCT_TYPE_ID NUMBER NOT NULL,
    PASS_TEMPLATE_ID NUMBER NOT NULL,
    CREATED_BY VARCHAR2(30) NOT NULL,
    CREATED_TS DATE NOT NULL,
    LAST_UPDATED_BY VARCHAR2(30) NOT NULL,
    LAST_UPDATED_TS DATE NOT NULL,
    CONSTRAINT PK_CUS_CONSUMER_ACCT_TYPE_ID PRIMARY KEY(CUSTOMER_ID,CONSUMER_ACCT_TYPE_ID),
    CONSTRAINT FK_CUS_CONSUMER_ACCT_TYPE_ID
    FOREIGN KEY (CONSUMER_ACCT_TYPE_ID)
    REFERENCES PSS.CONSUMER_ACCT_TYPE(CONSUMER_ACCT_TYPE_ID))
TABLESPACE PSS_DATA;

CREATE OR REPLACE TRIGGER PSS.TRBI_CUSTOMER_PASS_TEMPLATE BEFORE INSERT ON PSS.CUSTOMER_PASS_TEMPLATE
  FOR EACH ROW
BEGIN
  SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.CREATED_TS,
		:NEW.CREATED_BY,
		:NEW.LAST_UPDATED_TS,
		:NEW.LAST_UPDATED_BY
	FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER PSS.TRBU_CUSTOMER_PASS_TEMPLATE BEFORE UPDATE ON PSS.CUSTOMER_PASS_TEMPLATE
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.CREATED_BY,
		:OLD.CREATED_TS,
		SYSDATE,
		USER
	INTO
		:NEW.CREATED_BY,
		:NEW.CREATED_TS,
		:NEW.LAST_UPDATED_TS,
		:NEW.LAST_UPDATED_BY
	FROM DUAL;
END;
/

GRANT SELECT ON PSS.CUSTOMER_PASS_TEMPLATE to USAT_PREPAID_APP_ROLE;
GRANT SELECT ON PSS.CUSTOMER_PASS_TEMPLATE to USALIVE_APP_ROLE;

insert into PSS.CUSTOMER_PASS_TEMPLATE(CUSTOMER_ID, CONSUMER_ACCT_TYPE_ID, PASS_TEMPLATE_ID)
select 1, 5, 1 from dual where not exists (select 1 from PSS.CUSTOMER_PASS_TEMPLATE where customer_id=1);
commit;

-- new consumer field we want to capture
ALTER table PSS.CONSUMER
ADD(BIRTHDAY DATE);
    
-- this tables save the core information regarding consumer enrolled in VAS and its pass information    
CREATE TABLE PSS.CONSUMER_PASS (
    CONSUMER_PASS_ID NUMBER NOT NULL,
    CONSUMER_ID NUMBER,-- null only when user did not enroll
    CONSUMER_ACCT_ID NUMBER(20,0), --can be null for new all cards VAS enrollment, for prepaid, this will be linked to pss.consumer_acct.CONSUMER_ACCT_ID
    CUSTOMER_ID NUMBER,--null means Universal pass loyalty program, no customized loyalty pass layout
    PASS_SERIAL_NUMBER VARCHAR2(200),-- @TODO make it unique
    PASS_EXPIRATION_DATE DATE, -- Amelia wants never expire, shall we need this?
    PASS_INIT_DEVICE_ID NUMBER, -- null means through MORE site registration, otherwise records the device that does the pass enrollment prompt
    PASS_INIT_CUSTOMER_ID NUMBER, -- which customer's device the user initially enroll the loyalty program
    PASS_CONSUMER_ACCT_TYPE_ID NUMBER(3,0) default 5, -- this could differentiate the card types
    WELCOME_SENT_FLAG VARCHAR(1) default 'N',-- N, P, Y can use this to resend if needed
    WELCOME_LAST_SENT_TS DATE,
    CREATED_BY VARCHAR2(30) NOT NULL,
    CREATED_TS DATE NOT NULL,
    LAST_UPDATED_BY VARCHAR2(30) NOT NULL,
    LAST_UPDATED_TS DATE NOT NULL,
    CONSTRAINT PK_CONSUMER_PASS_ID PRIMARY KEY(CONSUMER_PASS_ID))
TABLESPACE PSS_DATA;

CREATE OR REPLACE TRIGGER PSS.TRBI_CONSUMER_PASS BEFORE INSERT ON PSS.CONSUMER_PASS
  FOR EACH ROW
BEGIN
  SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.CREATED_TS,
		:NEW.CREATED_BY,
		:NEW.LAST_UPDATED_TS,
		:NEW.LAST_UPDATED_BY
	FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER PSS.TRBU_CONSUMER_PASS BEFORE UPDATE ON PSS.CONSUMER_PASS
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.CREATED_BY,
		:OLD.CREATED_TS,
		SYSDATE,
		USER
	INTO
		:NEW.CREATED_BY,
		:NEW.CREATED_TS,
		:NEW.LAST_UPDATED_TS,
		:NEW.LAST_UPDATED_BY
	FROM DUAL;
END;
/

CREATE SEQUENCE PSS.CONSUMER_PASS_SEQ MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE ORDER NOCYCLE;

GRANT SELECT ON PSS.CONSUMER_PASS_SEQ to USAT_PREPAID_APP_ROLE;
GRANT SELECT, INSERT,UPDATE, DELETE ON PSS.CONSUMER_PASS to USAT_PREPAID_APP_ROLE;
GRANT SELECT, INSERT,UPDATE, DELETE ON PSS.CONSUMER_PASS to USALIVE_APP_ROLE;


-- VAS trans type, more to come for prepaid and others
insert into report.trans_type (TRANS_TYPE_ID,TRANS_TYPE_NAME,PROCESS_FEE_IND,DISPLAY_ORDER,FEE_PERCENT_UBN,FEE_AMOUNT_UBN,FEE_MIN_UBN,FEE_PERCENT_LBN,PAYABLE_IND,CUSTOMER_DEBIT_IND,ENTRY_TYPE,FILL_BATCH_IND,CASH_IND,TRANS_TYPE_CODE,TRANS_ITEM_TYPE,REFUND_IND,REFUNDABLE_IND,CHARGEBACK_ENABLED_IND,STATUS_CD,CREDIT_IND,OPERATOR_TRANS_TYPE_ID,CASHLESS_DEVICE_TRAN_IND,REPLENISH_IND,PREPAID_IND,PROCESS_FEE_TRANS_TYPE_ID,PASS_IND,ACCESS_IND,DEBIT_IND,FREE_IND) 
select 69,'Credit (Apple VAS)','Y',214,10,1,1,1,'Y','N','CC','Y','N','R','CREDIT (APPLE VAS)','N','Y','Y','A','Y',13,'Y','N','N',13,'N','N','N',null
from dual where not exists(select 1 from report.trans_type where TRANS_TYPE_ID=69);
commit;

-- recording this new field could helps us apply discount depends on how operator wants to config VAS promotion, in create_auth and create_sale in PKG.TRAN, we could use this info to figure out discount
ALTER table PSS.TRAN
ADD(CONSUMER_PASS_ID NUMBER);

ALTER table REPORT.TRANS
ADD(CONSUMER_PASS_ID NUMBER);

ALTER table REPORT.ACTIVITY_REF
ADD(CONSUMER_PASS_ID NUMBER);

-- this table we could potentially know which device VAS consumer ever visited so operator could communicate to their consumers, we could add more info if needed.
CREATE TABLE PSS.CONSUMER_PASS_TERMINAL (
    CONSUMER_PASS_ID NUMBER NOT NULL,
    TERMINAL_ID NUMBER NOT NULL,
    EPORT_ID NUMBER NOT NULL,
    FIRST_TRAN_DATE DATE,
    LAST_TRAN_DATE DATE,
    TRAN_COUNT NUMBER,
    TRAN_AMOUNT NUMBER,
    CREATED_BY VARCHAR2(30) NOT NULL,
    CREATED_TS DATE NOT NULL,
    LAST_UPDATED_BY VARCHAR2(30) NOT NULL,
    LAST_UPDATED_TS DATE NOT NULL,
    CONSTRAINT PK_CONSUMER_PASS_TERMINAL PRIMARY KEY(CONSUMER_PASS_ID,TERMINAL_ID,EPORT_ID))
TABLESPACE PSS_DATA;

CREATE OR REPLACE TRIGGER PSS.TRBI_CONSUMER_PASS_TERMINAL BEFORE INSERT ON PSS.CONSUMER_PASS_TERMINAL
  FOR EACH ROW
BEGIN
  SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.CREATED_TS,
		:NEW.CREATED_BY,
		:NEW.LAST_UPDATED_TS,
		:NEW.LAST_UPDATED_BY
	FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER PSS.TRBU_CONSUMER_PASS_TERMINAL BEFORE UPDATE ON PSS.CONSUMER_PASS_TERMINAL
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.CREATED_BY,
		:OLD.CREATED_TS,
		SYSDATE,
		USER
	INTO
		:NEW.CREATED_BY,
		:NEW.CREATED_TS,
		:NEW.LAST_UPDATED_TS,
		:NEW.LAST_UPDATED_BY
	FROM DUAL;
END;
/

INSERT INTO PSS.PAYMENT_SUBTYPE(PAYMENT_SUBTYPE_NAME, PAYMENT_SUBTYPE_CLASS, PAYMENT_SUBTYPE_KEY_NAME, CLIENT_PAYMENT_TYPE_CD, PAYMENT_SUBTYPE_TABLE_NAME, PAYMENT_SUBTYPE_KEY_DESC_NAME, AUTHORITY_PAYMENT_MASK_ID, TRANS_TYPE_ID, CARD_TYPE_LABEL)
SELECT distinct 'Visa (Track 2) VAS- Chase Paymentech - USA', PAYMENT_SUBTYPE_CLASS, PAYMENT_SUBTYPE_KEY_NAME, 'O', PAYMENT_SUBTYPE_TABLE_NAME, PAYMENT_SUBTYPE_KEY_DESC_NAME, AUTHORITY_PAYMENT_MASK_ID, 69, CARD_TYPE_LABEL
FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'Visa (Track 2) - Chase Paymentech - USA' AND NOT EXISTS (SELECT 1 FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'Visa (Track 2) VAS- Chase Paymentech - USA') and trans_type_id=16;
INSERT INTO PSS.PAYMENT_SUBTYPE(PAYMENT_SUBTYPE_NAME, PAYMENT_SUBTYPE_CLASS, PAYMENT_SUBTYPE_KEY_NAME, CLIENT_PAYMENT_TYPE_CD, PAYMENT_SUBTYPE_TABLE_NAME, PAYMENT_SUBTYPE_KEY_DESC_NAME, AUTHORITY_PAYMENT_MASK_ID, TRANS_TYPE_ID, CARD_TYPE_LABEL)
SELECT distinct 'MasterCard (Track 2) VAS- Chase Paymentech - USA', PAYMENT_SUBTYPE_CLASS, PAYMENT_SUBTYPE_KEY_NAME, 'O', PAYMENT_SUBTYPE_TABLE_NAME, PAYMENT_SUBTYPE_KEY_DESC_NAME, AUTHORITY_PAYMENT_MASK_ID, 69, CARD_TYPE_LABEL
FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'MasterCard (Track 2) - Chase Paymentech - USA' AND NOT EXISTS (SELECT 1 FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'MasterCard (Track 2) VAS- Chase Paymentech - USA') and trans_type_id=16;
INSERT INTO PSS.PAYMENT_SUBTYPE(PAYMENT_SUBTYPE_NAME, PAYMENT_SUBTYPE_CLASS, PAYMENT_SUBTYPE_KEY_NAME, CLIENT_PAYMENT_TYPE_CD, PAYMENT_SUBTYPE_TABLE_NAME, PAYMENT_SUBTYPE_KEY_DESC_NAME, AUTHORITY_PAYMENT_MASK_ID, TRANS_TYPE_ID, CARD_TYPE_LABEL)
SELECT distinct 'American Express (Track 2) VAS- Chase Paymentech - USA', PAYMENT_SUBTYPE_CLASS, PAYMENT_SUBTYPE_KEY_NAME, 'O', PAYMENT_SUBTYPE_TABLE_NAME, PAYMENT_SUBTYPE_KEY_DESC_NAME, AUTHORITY_PAYMENT_MASK_ID, 69, CARD_TYPE_LABEL
FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'American Express (Track 2) - Chase Paymentech - USA' AND NOT EXISTS (SELECT 1 FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'American Express (Track 2) VAS- Chase Paymentech - USA') and trans_type_id=16;
INSERT INTO PSS.PAYMENT_SUBTYPE(PAYMENT_SUBTYPE_NAME, PAYMENT_SUBTYPE_CLASS, PAYMENT_SUBTYPE_KEY_NAME, CLIENT_PAYMENT_TYPE_CD, PAYMENT_SUBTYPE_TABLE_NAME, PAYMENT_SUBTYPE_KEY_DESC_NAME, AUTHORITY_PAYMENT_MASK_ID, TRANS_TYPE_ID, CARD_TYPE_LABEL)
SELECT distinct 'Discover Card (Track 2) VAS- Chase Paymentech - USA', PAYMENT_SUBTYPE_CLASS, PAYMENT_SUBTYPE_KEY_NAME, 'O', PAYMENT_SUBTYPE_TABLE_NAME, PAYMENT_SUBTYPE_KEY_DESC_NAME, AUTHORITY_PAYMENT_MASK_ID, 69, CARD_TYPE_LABEL
FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'Discover Card (Track 2) - Chase Paymentech - USA' AND NOT EXISTS (SELECT 1 FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'Discover Card (Track 2) VAS- Chase Paymentech - USA') and trans_type_id=16;

INSERT INTO PSS.POS_PTA_TMPL(POS_PTA_TMPL_NAME, POS_PTA_TMPL_DESC)
SELECT 'VAS CREDIT - USA', 'VAS credit tandem payment template' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'VAS CREDIT - USA');

commit;

DECLARE
	l_PAYMENT_SUBTYPE_KEY_ID NUMBER;
BEGIN
	
select ppte.payment_subtype_key_id into l_PAYMENT_SUBTYPE_KEY_ID from PSS.POS_PTA_TMPL_ENTRY ppte 
join PSS.POS_PTA_TMPL ppt on ppte.POS_PTA_TMPL_ID= ppt.POS_PTA_TMPL_ID where ppte.PAYMENT_SUBTYPE_ID in (select PAYMENT_SUBTYPE_ID from PSS.PAYMENT_SUBTYPE where status_cd='A' 
and payment_subtype_name = 'Visa (Track 2) - Chase Paymentech - USA' and payment_subtype_class='Tandem' and trans_type_id=16) 
and rownum=1;
	
INSERT INTO PSS.POS_PTA_TMPL_ENTRY(POS_PTA_TMPL_ID, PAYMENT_SUBTYPE_ID, POS_PTA_ACTIVATION_OSET_HR, PAYMENT_SUBTYPE_KEY_ID, POS_PTA_PRIORITY, CURRENCY_CD, POS_PTA_PASSTHRU_ALLOW_YN_FLAG)
SELECT (SELECT POS_PTA_TMPL_ID FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'VAS CREDIT - USA'),
	(SELECT PAYMENT_SUBTYPE_ID FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'Visa (Track 2) VAS- Chase Paymentech - USA'),
	0, l_PAYMENT_SUBTYPE_KEY_ID,
	1, 'USD', 'N' 
  FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM PSS.POS_PTA_TMPL_ENTRY 
	WHERE POS_PTA_TMPL_ID = (SELECT POS_PTA_TMPL_ID FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'VAS CREDIT - USA')
		AND PAYMENT_SUBTYPE_ID = (SELECT PAYMENT_SUBTYPE_ID FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'Visa (Track 2) VAS- Chase Paymentech - USA')
);

INSERT INTO PSS.POS_PTA_TMPL_ENTRY(POS_PTA_TMPL_ID, PAYMENT_SUBTYPE_ID, POS_PTA_ACTIVATION_OSET_HR, PAYMENT_SUBTYPE_KEY_ID, POS_PTA_PRIORITY, CURRENCY_CD, POS_PTA_PASSTHRU_ALLOW_YN_FLAG)
SELECT (SELECT POS_PTA_TMPL_ID FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'VAS CREDIT - USA'),
	(SELECT PAYMENT_SUBTYPE_ID FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'MasterCard (Track 2) VAS- Chase Paymentech - USA'),
	0, l_PAYMENT_SUBTYPE_KEY_ID,
	1, 'USD', 'N' 
  FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM PSS.POS_PTA_TMPL_ENTRY 
	WHERE POS_PTA_TMPL_ID = (SELECT POS_PTA_TMPL_ID FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'VAS CREDIT - USA')
		AND PAYMENT_SUBTYPE_ID = (SELECT PAYMENT_SUBTYPE_ID FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'MasterCard (Track 2) VAS- Chase Paymentech - USA')
);

INSERT INTO PSS.POS_PTA_TMPL_ENTRY(POS_PTA_TMPL_ID, PAYMENT_SUBTYPE_ID, POS_PTA_ACTIVATION_OSET_HR, PAYMENT_SUBTYPE_KEY_ID, POS_PTA_PRIORITY, CURRENCY_CD, POS_PTA_PASSTHRU_ALLOW_YN_FLAG)
SELECT (SELECT POS_PTA_TMPL_ID FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'VAS CREDIT - USA'),
	(SELECT PAYMENT_SUBTYPE_ID FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'American Express (Track 2) VAS- Chase Paymentech - USA'),
	0, l_PAYMENT_SUBTYPE_KEY_ID,
	1, 'USD', 'N' 
  FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM PSS.POS_PTA_TMPL_ENTRY 
	WHERE POS_PTA_TMPL_ID = (SELECT POS_PTA_TMPL_ID FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'VAS CREDIT - USA')
		AND PAYMENT_SUBTYPE_ID = (SELECT PAYMENT_SUBTYPE_ID FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'American Express (Track 2) VAS- Chase Paymentech - USA')
);

INSERT INTO PSS.POS_PTA_TMPL_ENTRY(POS_PTA_TMPL_ID, PAYMENT_SUBTYPE_ID, POS_PTA_ACTIVATION_OSET_HR, PAYMENT_SUBTYPE_KEY_ID, POS_PTA_PRIORITY, CURRENCY_CD, POS_PTA_PASSTHRU_ALLOW_YN_FLAG)
SELECT (SELECT POS_PTA_TMPL_ID FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'VAS CREDIT - USA'),
	(SELECT PAYMENT_SUBTYPE_ID FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'Discover Card (Track 2) VAS- Chase Paymentech - USA'),
	0, l_PAYMENT_SUBTYPE_KEY_ID,
	1, 'USD', 'N' 
  FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM PSS.POS_PTA_TMPL_ENTRY 
	WHERE POS_PTA_TMPL_ID = (SELECT POS_PTA_TMPL_ID FROM PSS.POS_PTA_TMPL WHERE POS_PTA_TMPL_NAME = 'VAS CREDIT - USA')
		AND PAYMENT_SUBTYPE_ID = (SELECT PAYMENT_SUBTYPE_ID FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'Discover Card (Track 2) VAS- Chase Paymentech - USA')
);

commit;
END;
/

UPDATE REPORT.TRANS_TYPE SET PROCESS_FEE_IND = 'N', OPERATOR_TRANS_TYPE_ID = 69, PROCESS_FEE_TRANS_TYPE_ID = 16 WHERE TRANS_TYPE_ID = 69;
COMMIT;

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/PSS/PKG_PREPAID.psk?revision=1.21
CREATE OR REPLACE PACKAGE PSS.PKG_PREPAID 
IS
    -- R42 and above
    PROCEDURE CREATE_PREPAID_CONSUMER(
        pn_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
        pv_security_code PSS.CONSUMER_ACCT.SECURITY_CD_HASH%TYPE,
        pv_email PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pv_cell PSS.CONSUMER.CONSUMER_CELL_PHONE_NUM%TYPE,
        pn_carrier_id PSS.CONSUMER.CONSUMER_CELL_SMS_GATEWAY_ID%TYPE,
        pn_preferred_comm_type_id PSS.CONSUMER.PREFERRED_COMM_TYPE_ID%TYPE,
        pv_first PSS.CONSUMER.CONSUMER_FNAME%TYPE,
        pv_last PSS.CONSUMER.CONSUMER_LNAME%TYPE,
        pv_addr1 PSS.CONSUMER.CONSUMER_ADDR1%TYPE,
        pv_city PSS.CONSUMER.CONSUMER_CITY%TYPE,
        pv_state PSS.CONSUMER.CONSUMER_STATE_CD%TYPE,
        pv_postal PSS.CONSUMER.CONSUMER_POSTAL_CD%TYPE,
        pv_country PSS.CONSUMER.CONSUMER_COUNTRY_CD%TYPE,
        pb_credential_hash PSS.CONSUMER_CREDENTIAL.CREDENTIAL_HASH%TYPE,
        pb_credential_salt PSS.CONSUMER_CREDENTIAL.CREDENTIAL_SALT%TYPE,
        pv_credential_alg PSS.CONSUMER_CREDENTIAL.CREDENTIAL_ALG%TYPE,
        pn_consumer_id OUT PSS.CONSUMER.CONSUMER_ID%TYPE,
        pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
        pv_passcode OUT PSS.CONSUMER_PASSCODE.PASSCODE%TYPE,
        pv_username OUT PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pv_communication_email OUT PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pn_region_id PSS.CONSUMER.REGION_ID%TYPE DEFAULT NULL,
        pc_is_credit CHAR DEFAULT NULL,
        pd_exp_date PSS.CONSUMER_ACCT.EXPIRATION_DATE%TYPE DEFAULT NULL,
        pv_promo_code VARCHAR2 DEFAULT NULL,
        pv_currency_cd VARCHAR2 DEFAULT NULL,
        pn_consumer_acct_identifier PSS.CONSUMER_ACCT.CONSUMER_ACCT_IDENTIFIER%TYPE DEFAULT NULL);
    
    PROCEDURE CREATE_PREPAID_CONSUMER_SIMPLE(
        pn_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
        pv_security_code PSS.CONSUMER_ACCT.SECURITY_CD_HASH%TYPE,
        pv_first PSS.CONSUMER.CONSUMER_FNAME%TYPE,
        pv_last PSS.CONSUMER.CONSUMER_LNAME%TYPE,
        pn_region_id PSS.CONSUMER.REGION_ID%TYPE,
        pn_consumer_id OUT PSS.CONSUMER.CONSUMER_ID%TYPE,
        pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE);
    

    PROCEDURE ADD_PREPAID_ACCT(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pn_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
        pv_security_code PSS.CONSUMER_ACCT.SECURITY_CD_HASH%TYPE,
        pv_nickname PSS.CONSUMER_ACCT.NICK_NAME%TYPE,
        pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE);
        
	PROCEDURE ADD_PAYROLL_DEDUCT_ACCT(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pn_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
        pv_security_code PSS.CONSUMER_ACCT.SECURITY_CD_HASH%TYPE,
        pv_nickname PSS.CONSUMER_ACCT.NICK_NAME%TYPE,
        pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE);       
        

    PROCEDURE ADD_CREDIT_ACCT(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pn_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
        pv_currency_cd VARCHAR2,
        pd_exp_date PSS.CONSUMER_ACCT.EXPIRATION_DATE%TYPE,
        pv_nickname PSS.CONSUMER_ACCT.NICK_NAME%TYPE,
        pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
        pv_promo_code REPORT.CAMPAIGN.PROMO_CODE%TYPE DEFAULT NULL); 
        
   PROCEDURE ADD_SPROUT_ACCT(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pn_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
        pv_security_code PSS.CONSUMER_ACCT.SECURITY_CD_HASH%TYPE,
        pv_nickname PSS.CONSUMER_ACCT.NICK_NAME%TYPE,
        pn_consumer_acct_identifier PSS.CONSUMER_ACCT.CONSUMER_ACCT_IDENTIFIER%TYPE,
        pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE);   
       
    -- R42 and above
    PROCEDURE UPDATE_PREPAID_CONSUMER(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pv_email PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pv_cell PSS.CONSUMER.CONSUMER_CELL_PHONE_NUM%TYPE DEFAULT NULL,
        pn_carrier_id PSS.CONSUMER.CONSUMER_CELL_SMS_GATEWAY_ID%TYPE DEFAULT NULL,
        pn_preferred_comm_type_id PSS.CONSUMER.PREFERRED_COMM_TYPE_ID%TYPE,
        pv_first PSS.CONSUMER.CONSUMER_FNAME%TYPE,
        pv_last PSS.CONSUMER.CONSUMER_LNAME%TYPE,
        pv_addr1 PSS.CONSUMER.CONSUMER_ADDR1%TYPE,
        pv_city PSS.CONSUMER.CONSUMER_CITY%TYPE,
        pv_state PSS.CONSUMER.CONSUMER_STATE_CD%TYPE,
        pv_postal PSS.CONSUMER.CONSUMER_POSTAL_CD%TYPE,
        pv_country PSS.CONSUMER.CONSUMER_COUNTRY_CD%TYPE,
        pb_credential_hash PSS.CONSUMER_CREDENTIAL.CREDENTIAL_HASH%TYPE,
        pb_credential_salt PSS.CONSUMER_CREDENTIAL.CREDENTIAL_SALT%TYPE,
        pv_credential_alg PSS.CONSUMER_CREDENTIAL.CREDENTIAL_ALG%TYPE,
        pv_passcode OUT PSS.CONSUMER_PASSCODE.PASSCODE%TYPE,
        pv_username OUT PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pv_communication_email OUT PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pn_region_id PSS.CONSUMER.REGION_ID%TYPE DEFAULT NULL);
   
    PROCEDURE UPDATE_CONSUMER_SIMPLE(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pv_first PSS.CONSUMER.CONSUMER_FNAME%TYPE,
        pv_last PSS.CONSUMER.CONSUMER_LNAME%TYPE,
        pn_region_id PSS.CONSUMER.REGION_ID%TYPE);
        
    -- R42 and above
    PROCEDURE CREATE_PASSCODE_FOR_PASSWORD(
        pv_username PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pn_consumer_id OUT PSS.CONSUMER.CONSUMER_ID%TYPE,
        pn_preferred_comm_type_id OUT PSS.CONSUMER.PREFERRED_COMM_TYPE_ID%TYPE,
        pv_first OUT PSS.CONSUMER.CONSUMER_FNAME%TYPE,
        pv_last OUT PSS.CONSUMER.CONSUMER_LNAME%TYPE,
        pv_passcode OUT PSS.CONSUMER_PASSCODE.PASSCODE%TYPE);
        
    PROCEDURE UPDATE_PREPAID_PASSWORD(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pv_passcode PSS.CONSUMER_PASSCODE.PASSCODE%TYPE,
        pb_credential_hash PSS.CONSUMER_CREDENTIAL.CREDENTIAL_HASH%TYPE,
        pb_credential_salt PSS.CONSUMER_CREDENTIAL.CREDENTIAL_SALT%TYPE,
        pv_credential_alg PSS.CONSUMER_CREDENTIAL.CREDENTIAL_ALG%TYPE);
    
    -- R42 and above
    PROCEDURE UPDATE_PREPAID_PASSWORD(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pb_credential_hash PSS.CONSUMER_CREDENTIAL.CREDENTIAL_HASH%TYPE,
        pb_credential_salt PSS.CONSUMER_CREDENTIAL.CREDENTIAL_SALT%TYPE,
        pv_credential_alg PSS.CONSUMER_CREDENTIAL.CREDENTIAL_ALG%TYPE);
        
    --R42 and above
    PROCEDURE REGISTER_PREPAID_CONSUMER(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pv_passcode PSS.CONSUMER_PASSCODE.PASSCODE%TYPE,
        pv_username OUT PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pv_communication_email OUT PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pn_preferred_comm_type_id OUT PSS.CONSUMER.PREFERRED_COMM_TYPE_ID%TYPE,
        pv_first OUT PSS.CONSUMER.CONSUMER_FNAME%TYPE,
        pv_last OUT PSS.CONSUMER.CONSUMER_LNAME%TYPE);
        
   --R42 and above
   PROCEDURE REREGISTER_PREPAID_CONSUMER(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pv_username OUT PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pv_communication_email OUT PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pn_preferred_comm_type_id OUT PSS.CONSUMER.PREFERRED_COMM_TYPE_ID%TYPE,
        pv_first OUT PSS.CONSUMER.CONSUMER_FNAME%TYPE,
        pv_last OUT PSS.CONSUMER.CONSUMER_LNAME%TYPE,
        pv_passcode OUT PSS.CONSUMER_PASSCODE.PASSCODE%TYPE);
        
    PROCEDURE CANCEL_PASSCODE(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pv_passcode PSS.CONSUMER_PASSCODE.PASSCODE%TYPE);
        
    PROCEDURE UPDATE_CONSUMER_SETTING(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pn_param_id PSS.CONSUMER_SETTING.CONSUMER_SETTING_PARAM_ID%TYPE,
        pv_param_value PSS.CONSUMER_SETTING.CONSUMER_SETTING_VALUE%TYPE);
    
    FUNCTION GET_PASSCODE(
        l_consumer_id CONSUMER_PASSCODE.CONSUMER_ID%TYPE)
    RETURN CONSUMER_PASSCODE.PASSCODE%TYPE;
    
    PROCEDURE ADD_PROMO(
        pv_promo_code REPORT.CAMPAIGN.PROMO_CODE%TYPE,
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE);   
     
     PROCEDURE CREATE_NEXT_CONSUMER_PASS_ID(
        pv_device_serail_cd REPORT.EPORT.EPORT_SERIAL_NUM%TYPE,
        pv_pass_type_id CORP.CUSTOMER.PASS_TYPE_ID%TYPE,
        pv_consumer_pass_id OUT PSS.CONSUMER_PASS.CONSUMER_PASS_ID%TYPE,
        pn_pass_template_id OUT PSS.CUSTOMER_PASS_TEMPLATE.PASS_TEMPLATE_ID%TYPE);
        
    PROCEDURE CREATE_VAS_CONSUMER(
        pv_pass_serial_num PSS.CONSUMER_PASS.PASS_SERIAL_NUMBER%TYPE,
        pv_email PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pv_cell PSS.CONSUMER.CONSUMER_CELL_PHONE_NUM%TYPE,
        pn_preferred_comm_type_id PSS.CONSUMER.PREFERRED_COMM_TYPE_ID%TYPE,
        pv_first PSS.CONSUMER.CONSUMER_FNAME%TYPE,
        pv_last PSS.CONSUMER.CONSUMER_LNAME%TYPE,
        pv_postal PSS.CONSUMER.CONSUMER_POSTAL_CD%TYPE,
        pv_country PSS.CONSUMER.CONSUMER_COUNTRY_CD%TYPE
        );
        
    PROCEDURE CREATE_VAS_CREDENTIAL(
      pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
      pb_credential_hash PSS.CONSUMER_CREDENTIAL.CREDENTIAL_HASH%TYPE,
      pb_credential_salt PSS.CONSUMER_CREDENTIAL.CREDENTIAL_SALT%TYPE,
      pv_credential_alg PSS.CONSUMER_CREDENTIAL.CREDENTIAL_ALG%TYPE,
      pv_addr1 PSS.CONSUMER.CONSUMER_ADDR1%TYPE,
      pd_birthday PSS.CONSUMER.BIRTHDAY%TYPE);
      
	PROCEDURE ASSIGN_ACCT_BY_PROMO(
      pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
      pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE);
      
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/PSS/PKG_PREPAID.pbk?revision=1.45
create or replace PACKAGE BODY PSS.PKG_PREPAID 
IS  
    PROCEDURE VERIFY_REGION_BY_ACCT(
        pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE, 
        pn_region_id PSS.CONSUMER.REGION_ID%TYPE)
    IS
        ln_cnt PLS_INTEGER;
    BEGIN
        IF pn_region_id IS NULL THEN
            RETURN;
        END IF;
        SELECT COUNT(*)
          INTO ln_cnt
          FROM PSS.CONSUMER_ACCT CA
          JOIN REPORT.REGION R ON CA.CORP_CUSTOMER_ID = R.CUSTOMER_ID
         WHERE CA.CONSUMER_ACCT_ID = pn_consumer_acct_id
           AND R.REGION_ID = pn_region_id;
        IF ln_cnt = 0 THEN
            RAISE_APPLICATION_ERROR(-20304, 'Region not valid for this card');
        END IF;
    END;
       
    PROCEDURE VERIFY_REGION(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE, 
        pn_region_id PSS.CONSUMER.REGION_ID%TYPE)
    IS
        ln_cnt PLS_INTEGER;
    BEGIN
        IF pn_region_id IS NULL THEN
            RETURN;
        END IF;
        SELECT COUNT(*)
          INTO ln_cnt
          FROM PSS.CONSUMER_ACCT CA
          JOIN REPORT.REGION R ON CA.CORP_CUSTOMER_ID = R.CUSTOMER_ID
         WHERE CA.CONSUMER_ID = pn_consumer_id
           AND CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y'
           AND R.REGION_ID = pn_region_id;
        IF ln_cnt = 0 THEN
            RAISE_APPLICATION_ERROR(-20305, 'Region not valid for this consumer');
        END IF;
    END;     
    
    -- R42 and above
    PROCEDURE CREATE_PREPAID_CONSUMER(
        pn_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
        pv_security_code PSS.CONSUMER_ACCT.SECURITY_CD_HASH%TYPE,
        pv_email PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pv_cell PSS.CONSUMER.CONSUMER_CELL_PHONE_NUM%TYPE,
        pn_carrier_id PSS.CONSUMER.CONSUMER_CELL_SMS_GATEWAY_ID%TYPE,
        pn_preferred_comm_type_id PSS.CONSUMER.PREFERRED_COMM_TYPE_ID%TYPE,
        pv_first PSS.CONSUMER.CONSUMER_FNAME%TYPE,
        pv_last PSS.CONSUMER.CONSUMER_LNAME%TYPE,
        pv_addr1 PSS.CONSUMER.CONSUMER_ADDR1%TYPE,
        pv_city PSS.CONSUMER.CONSUMER_CITY%TYPE,
        pv_state PSS.CONSUMER.CONSUMER_STATE_CD%TYPE,
        pv_postal PSS.CONSUMER.CONSUMER_POSTAL_CD%TYPE,
        pv_country PSS.CONSUMER.CONSUMER_COUNTRY_CD%TYPE,
        pb_credential_hash PSS.CONSUMER_CREDENTIAL.CREDENTIAL_HASH%TYPE,
        pb_credential_salt PSS.CONSUMER_CREDENTIAL.CREDENTIAL_SALT%TYPE,
        pv_credential_alg PSS.CONSUMER_CREDENTIAL.CREDENTIAL_ALG%TYPE,
        pn_consumer_id OUT PSS.CONSUMER.CONSUMER_ID%TYPE,
        pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
        pv_passcode OUT PSS.CONSUMER_PASSCODE.PASSCODE%TYPE,
        pv_username OUT PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pv_communication_email OUT PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pn_region_id PSS.CONSUMER.REGION_ID%TYPE DEFAULT NULL,
        pc_is_credit CHAR DEFAULT NULL,
        pd_exp_date PSS.CONSUMER_ACCT.EXPIRATION_DATE%TYPE DEFAULT NULL,
        pv_promo_code VARCHAR2 DEFAULT NULL,
        pv_currency_cd VARCHAR2 DEFAULT NULL,
        pn_consumer_acct_identifier PSS.CONSUMER_ACCT.CONSUMER_ACCT_IDENTIFIER%TYPE DEFAULT NULL)
    IS
        ln_cnt PLS_INTEGER;
        lc_flag CHAR(1);
        lv_lock VARCHAR2(28);
        ln_consumer_type_id PSS.CONSUMER.CONSUMER_TYPE_ID%TYPE;
        lv_city PSS.CONSUMER.CONSUMER_CITY%TYPE;
        lv_state_cd PSS.CONSUMER.CONSUMER_STATE_CD%TYPE;
        lv_email PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE;
        lv_cell PSS.CONSUMER.CONSUMER_CELL_PHONE_NUM%TYPE;  
        lv_consumer_acct_cd PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD%TYPE;
        lv_consumer_email_addr1 PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE;
        ln_softcard_cnt PLS_INTEGER;
        ln_campaign_ids NUMBER_TABLE:=NUMBER_TABLE();
        lv_acc_fmt_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_FMT_ID%TYPE; 

    BEGIN
        select campaign_id bulk collect into ln_campaign_ids 
        from report.campaign 
        where upper(promo_code)=upper(pv_promo_code) 
              and status='A' and (END_DATE is NULL or END_DATE > SYSDATE-1) 
              and (campaign_type_id in(4,5) or campaign_type_id = 1 and is_for_all_cards = 'Y');

        IF pc_is_credit is not null and pc_is_credit = 'Y' THEN
          IF ln_campaign_ids.count = 0 THEN
            RAISE_APPLICATION_ERROR(-20310, 'Did not find the promotion code');
          END IF;
        END IF;
        
        SELECT MAX(CONSUMER_ACCT_FMT_ID) into lv_acc_fmt_id 
        FROM PSS.CONSUMER_ACCT CA join PSS.CONSUMER_ACCT_BASE AB on CA.CONSUMER_ACCT_ID = AB.CONSUMER_ACCT_ID 
         where AB.GLOBAL_ACCOUNT_ID=pn_global_account_id;
        IF ((pc_is_credit is NULL AND lv_acc_fmt_id != 2) OR (pc_is_credit = 'P' AND lv_acc_fmt_id != 3)) THEN 
          RAISE_APPLICATION_ERROR(-20300, 'Did not find an account for card id ' || pn_global_account_id);
        END IF;
        
        SELECT TRIM(pv_email), REGEXP_REPLACE(pv_cell, '\s*\(?\s*(\d{3})\s*\)?\s*[-.]?\s*(\d{3})\s*[-.]?\s*(\d{4})\s*', '\1\2\3') -- format these two fields first
          INTO lv_email, lv_cell
          FROM DUAL;        
        lv_lock := DBADMIN.PKG_GLOBAL.REQUEST_LOCK('PSS.CONSUMER_ACCT', pn_global_account_id); 
        SELECT COUNT(*), MAX(CA.CONSUMER_ACCT_ID), 
               NVL(MAX(CASE 
                    WHEN C.CONSUMER_TYPE_ID in (5,7,9,10) AND (UPPER(C.CONSUMER_EMAIL_ADDR1) = UPPER(lv_email) OR C.CONSUMER_CELL_PHONE_NUM = lv_cell) AND CC.CREDENTIAL_ACTIVE_FLAG = 'Y' THEN 'Y' -- Registered and matches and not new
                    WHEN C.CONSUMER_TYPE_ID in (5,7,9,10) AND (UPPER(C.CONSUMER_EMAIL_ADDR1) = UPPER(lv_email) OR C.CONSUMER_CELL_PHONE_NUM = lv_cell) AND CC.CREDENTIAL_ACTIVE_FLAG = 'N' THEN 'A' -- Registered and matches and new - so send email
                    END), 'N'),
               MAX(CASE 
                    WHEN (UPPER(C.CONSUMER_EMAIL_ADDR1) = UPPER(lv_email) OR C.CONSUMER_CELL_PHONE_NUM = lv_cell) THEN C.CONSUMER_ID -- Registered and matches and new - so send email
                    WHEN C.CONSUMER_TYPE_ID =7 THEN C.CONSUMER_ID
                    END),
               MAX(C.CONSUMER_TYPE_ID),
               MAX(C.CONSUMER_EMAIL_ADDR1)
          INTO ln_cnt, pn_consumer_acct_id, lc_flag, pn_consumer_id, ln_consumer_type_id,lv_consumer_email_addr1
          FROM PSS.CONSUMER_ACCT CA
          JOIN PSS.CONSUMER_ACCT_BASE CAB ON CA.CONSUMER_ACCT_ID = CAB.CONSUMER_ACCT_ID
          JOIN PSS.CONSUMER C ON CA.CONSUMER_ID = C.CONSUMER_ID
          LEFT OUTER JOIN PSS.CONSUMER_CREDENTIAL CC ON C.CONSUMER_ID = CC.CONSUMER_ID AND CC.RESOURCE_KEY = 'PREPAID_WEBSITE'
         WHERE CAB.GLOBAL_ACCOUNT_ID = pn_global_account_id   
           AND CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y'
           AND (((CA.CONSUMER_ACCT_FMT_ID = 2 AND CA.CONSUMER_ACCT_TYPE_ID = 3 
           OR CA.CONSUMER_ACCT_FMT_ID = 3 AND CA.CONSUMER_ACCT_TYPE_ID = 7)
		       AND (pv_security_code is NULL or DBADMIN.PKG_UTL.EQL(CA.SECURITY_CD_HASH, DBADMIN.GET_HASH(UTL_RAW.CAST_TO_RAW(pv_security_code), CA.SECURITY_CD_SALT, 'SHA-256/1000')) = 'Y'))
           OR CA.CONSUMER_ACCT_TYPE_ID in (5,6)
           );
        
        IF ln_cnt = 0 THEN
          IF pc_is_credit is null OR pc_is_credit = 'P' OR pc_is_credit = 'S' THEN
            RAISE_APPLICATION_ERROR(-20300, 'Did not find an account for card id ' || pn_global_account_id);
          ELSE
              IF pv_state IS NOT NULL THEN
                SELECT COUNT(*)
                  INTO ln_cnt
                  FROM LOCATION.STATE
                 WHERE STATE_CD = pv_state;
                IF ln_cnt = 0 THEN
                    RAISE_APPLICATION_ERROR(-20309, 'Invalid state ' || pv_state || ' provided');
                END IF;
                lv_city := pv_city;
                lv_state_cd := pv_state;
            ELSE
                SELECT MAX(CITY), MAX(STATE_CD)
                  INTO lv_city, lv_state_cd
                  FROM (
                  SELECT P.CITY, S.STATE_CD
                    FROM FRONT.POSTAL P 
                    JOIN CORP.STATE S ON P.STATE_ID = S.STATE_ID
                    JOIN CORP.COUNTRY C ON S.COUNTRY_CD = C.COUNTRY_CD
                  WHERE P.POSTAL_CD = REGEXP_REPLACE(pv_postal, C.POSTAL_REGEX, 
                    (SELECT LISTAGG('\' || LEVEL, '') WITHIN GROUP (ORDER BY LEVEL) FROM DUAL CONNECT BY LEVEL <= C.POSTAL_PRIMARY_PARTS))
                    AND C.COUNTRY_CD = pv_country
                    AND C.POSTAL_REGEX IS NOT NULL
                  ORDER BY P.POSTAL_ID ASC)
                WHERE ROWNUM = 1;
            END IF;
            
            SELECT MAX(CONSUMER_ACCT_ID), MAX(TRUNCATED_CARD_NUMBER)  
              INTO pn_consumer_acct_id,lv_consumer_acct_cd
              FROM (
            SELECT CAB.CONSUMER_ACCT_ID, CAB.CONSUMER_ID, CAB.TRUNCATED_CARD_NUMBER
              FROM (
                SELECT CAB.CONSUMER_ACCT_ID, 
                       CASE WHEN CA.CONSUMER_ACCT_ACTIVE_YN_FLAG IS NULL THEN 5 WHEN CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y' AND NVL(CA.CONSUMER_ACCT_ACTIVATION_TS, MIN_DATE) <= SYSDATE AND NVL(CA.CONSUMER_ACCT_DEACTIVATION_TS, MAX_DATE) > SYSDATE THEN 1 ELSE 10 END CA_PRIORITY,
                       CASE WHEN 1 = CAB.PAYMENT_SUBTYPE_ID THEN 1 ELSE 10 END PST_PRIORITY,
                       CASE WHEN 1 = NVL(CAB.LOCATION_ID, 0) THEN 1 ELSE 10 END LOCATION_PRIORITY,
                       CA.CONSUMER_ACCT_ISSUE_NUM,
                       C.CONSUMER_ID,
                       C.CONSUMER_IDENTIFIER,
                       CAB.TRUNCATED_CARD_NUMBER
                  FROM PSS.CONSUMER_ACCT_BASE CAB
                  LEFT OUTER JOIN (PSS.CONSUMER_ACCT CA 
                  JOIN PSS.CONSUMER C ON CA.CONSUMER_ID = C.CONSUMER_ID) ON CAB.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
                 WHERE CAB.GLOBAL_ACCOUNT_ID = pn_global_account_id
                   AND CAB.CURRENCY_CD = pv_currency_cd) CAB
             ORDER BY CAB.CA_PRIORITY, CAB.PST_PRIORITY, CAB.LOCATION_PRIORITY, CAB.CONSUMER_ACCT_ISSUE_NUM DESC, CAB.CONSUMER_IDENTIFIER NULLS LAST
             ) WHERE ROWNUM = 1;
            
            SELECT PSS.SEQ_CONSUMER_ID.NEXTVAL
              INTO pn_consumer_id
              FROM DUAL;
              BEGIN
                INSERT INTO PSS.CONSUMER(
                    CONSUMER_ID,
                    CONSUMER_EMAIL_ADDR1,
                    CONSUMER_CELL_PHONE_NUM,
                    CONSUMER_CELL_SMS_GATEWAY_ID,
                    PREFERRED_COMM_TYPE_ID,
                    CONSUMER_FNAME,
                    CONSUMER_LNAME,
                    CONSUMER_ADDR1,
                    CONSUMER_CITY,
                    CONSUMER_STATE_CD,
                    CONSUMER_POSTAL_CD,
                    CONSUMER_COUNTRY_CD,
                    CONSUMER_TYPE_ID,
                    REGION_ID)
                VALUES(
                    pn_consumer_id,
                    lv_email,
                    lv_cell,
                    pn_carrier_id,
                    pn_preferred_comm_type_id,
                    pv_first,
                    pv_last,
                    pv_addr1,
                    lv_city,
                    lv_state_cd,
                    pv_postal,
                    pv_country,
                    CASE WHEN pc_is_credit = 'Y' THEN 7 ELSE 8 END, -- loyalty or sprout
                    pn_region_id);
            EXCEPTION
                WHEN DUP_VAL_ON_INDEX THEN
                    RAISE_APPLICATION_ERROR(-20303, 'This email is already registered with another card.');
            END;
            IF pc_is_credit = 'Y' THEN
              INSERT INTO PSS.CONSUMER_ACCT(CONSUMER_ACCT_ID, CONSUMER_ACCT_CD, CONSUMER_ACCT_ACTIVE_YN_FLAG, CONSUMER_ACCT_BALANCE, CONSUMER_ID, LOCATION_ID, 
                      CONSUMER_ACCT_ISSUE_NUM, PAYMENT_SUBTYPE_ID, CURRENCY_CD, CONSUMER_ACCT_TYPE_ID, EXPIRATION_DATE,CONSUMER_ACCT_SUB_TYPE_ID) 
                      VALUES(pn_consumer_acct_id, lv_consumer_acct_cd, 'Y', 0, pn_consumer_id, 1, 1, 1, pv_currency_cd, 5, pd_exp_date,1);
                      
              UPDATE PSS.CONSUMER_ACCT_BASE SET PAYMENT_SUBTYPE_ID =1 WHERE CONSUMER_ACCT_ID=pn_consumer_acct_id;
              
              INSERT INTO PSS.CONSUMER_CREDENTIAL(CONSUMER_ID, CREDENTIAL_HASH, CREDENTIAL_SALT, CREDENTIAL_ALG, RESOURCE_KEY)
              VALUES(pn_consumer_id, pb_credential_hash, pb_credential_salt, pv_credential_alg, 'PREPAID_WEBSITE');
              pv_passcode := PSS.PKG_CONSUMER_MAINT.CREATE_CONSUMER_PASSCODE(pn_consumer_id, 'REGISTER_PREPAID'); 
              
              ADD_PROMO(pv_promo_code, pn_consumer_id);                
            END IF;
          END IF;
        ELSIF ln_consumer_type_id != 4 THEN
            IF lc_flag = 'A' THEN
                DELETE 
                  FROM PSS.CONSUMER_CREDENTIAL
                 WHERE CONSUMER_ID = pn_consumer_id
                   AND RESOURCE_KEY = 'PREPAID_WEBSITE';
                INSERT INTO PSS.CONSUMER_CREDENTIAL(CONSUMER_ID, CREDENTIAL_HASH, CREDENTIAL_SALT, CREDENTIAL_ALG, RESOURCE_KEY)
                    VALUES(pn_consumer_id, pb_credential_hash, pb_credential_salt, pv_credential_alg, 'PREPAID_WEBSITE');
                pv_passcode := PSS.PKG_CONSUMER_MAINT.CREATE_CONSUMER_PASSCODE(pn_consumer_id, 'REGISTER_PREPAID');
            ELSIF lc_flag = 'Y' THEN
                RAISE_APPLICATION_ERROR(-20301, 'Account already registered to this email');
            ELSE
               IF pc_is_credit is null OR pc_is_credit IN ('P', 'S') THEN
                  RAISE_APPLICATION_ERROR(-20302, 'Account already registered to another email');
                ELSE
                  select count(1) into ln_softcard_cnt from dual where REGEXP_LIKE(lv_consumer_email_addr1, '^(isis|softcard).*usatech.com$');
                  IF ln_softcard_cnt > 0 THEN
                    UPDATE PSS.CONSUMER SET
                    CONSUMER_EMAIL_ADDR1 = lv_email,
                    CONSUMER_CELL_PHONE_NUM =lv_cell,
                    CONSUMER_CELL_SMS_GATEWAY_ID = pn_carrier_id,
                    PREFERRED_COMM_TYPE_ID =pn_preferred_comm_type_id,
                    CONSUMER_FNAME = pv_first,
                    CONSUMER_LNAME = pv_last,
                    CONSUMER_ADDR1 = pv_addr1,
                    CONSUMER_CITY = lv_city,
                    CONSUMER_STATE_CD = lv_state_cd,
                    CONSUMER_POSTAL_CD = pv_postal,
                    CONSUMER_COUNTRY_CD = pv_country,
                    CONSUMER_TYPE_ID = 7
                    WHERE CONSUMER_ID = pn_consumer_id;
                    
                    UPDATE PSS.CONSUMER_ACCT SET EXPIRATION_DATE=pd_exp_date, CONSUMER_ACCT_SUB_TYPE_ID=1 WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id;
                    
                    INSERT INTO PSS.CONSUMER_CREDENTIAL(CONSUMER_ID, CREDENTIAL_HASH, CREDENTIAL_SALT, CREDENTIAL_ALG, RESOURCE_KEY)
                    VALUES(pn_consumer_id, pb_credential_hash, pb_credential_salt, pv_credential_alg, 'PREPAID_WEBSITE');
                    pv_passcode := PSS.PKG_CONSUMER_MAINT.CREATE_CONSUMER_PASSCODE(pn_consumer_id, 'REGISTER_PREPAID'); 
                    
                    INSERT INTO PSS.CAMPAIGN_CONSUMER_ACCT(CAMPAIGN_ID, CONSUMER_ACCT_ID,TRAN_COUNT, PROMO_TRAN_COUNT) 
                    SELECT column_value, pn_consumer_acct_id,0,0 from table(ln_campaign_ids)
                    where not exists (select 1 from PSS.CAMPAIGN_CONSUMER_ACCT where CAMPAIGN_ID =column_value and CONSUMER_ACCT_ID=pn_consumer_acct_id);
                  ELSE
                    RAISE_APPLICATION_ERROR(-20302, 'Account already registered to another email');
                  END IF;               
               END IF;
            END IF;
        ELSE
            IF  pc_is_credit is null OR pc_is_credit = 'P' THEN
              VERIFY_REGION_BY_ACCT(pn_consumer_acct_id, pn_region_id);
            END IF;
            IF pv_state IS NOT NULL THEN
                SELECT COUNT(*)
                  INTO ln_cnt
                  FROM LOCATION.STATE
                 WHERE STATE_CD = pv_state;
                IF ln_cnt = 0 THEN
                    RAISE_APPLICATION_ERROR(-20309, 'Invalid state ' || pv_state || ' provided');
                END IF;
                lv_city := pv_city;
                lv_state_cd := pv_state;
            ELSE
                SELECT MAX(CITY), MAX(STATE_CD)
                  INTO lv_city, lv_state_cd
                  FROM (
                  SELECT P.CITY, S.STATE_CD
                    FROM FRONT.POSTAL P 
                    JOIN CORP.STATE S ON P.STATE_ID = S.STATE_ID
                    JOIN CORP.COUNTRY C ON S.COUNTRY_CD = C.COUNTRY_CD
                  WHERE P.POSTAL_CD = REGEXP_REPLACE(pv_postal, C.POSTAL_REGEX, 
                    (SELECT LISTAGG('\' || LEVEL, '') WITHIN GROUP (ORDER BY LEVEL) FROM DUAL CONNECT BY LEVEL <= C.POSTAL_PRIMARY_PARTS))
                    AND C.COUNTRY_CD = pv_country
                    AND C.POSTAL_REGEX IS NOT NULL
                  ORDER BY P.POSTAL_ID ASC)
                WHERE ROWNUM = 1;
            END IF;
            SELECT PSS.SEQ_CONSUMER_ID.NEXTVAL
              INTO pn_consumer_id
              FROM DUAL;
            BEGIN
                INSERT INTO PSS.CONSUMER(
                    CONSUMER_ID,
                    CONSUMER_EMAIL_ADDR1,
                    CONSUMER_CELL_PHONE_NUM,
                    CONSUMER_CELL_SMS_GATEWAY_ID,
                    PREFERRED_COMM_TYPE_ID,
                    CONSUMER_FNAME,
                    CONSUMER_LNAME,
                    CONSUMER_ADDR1,
                    CONSUMER_CITY,
                    CONSUMER_STATE_CD,
                    CONSUMER_POSTAL_CD,
                    CONSUMER_COUNTRY_CD,
                    CONSUMER_TYPE_ID,
                    REGION_ID)
                VALUES(
                    pn_consumer_id,
                    lv_email,
                    lv_cell,
                    pn_carrier_id,
                    pn_preferred_comm_type_id,
                    pv_first,
                    pv_last,
                    pv_addr1,
                    lv_city,
                    lv_state_cd,
                    pv_postal,
                    pv_country,
                    CASE WHEN pc_is_credit = 'S' THEN 8 
                         WHEN pc_is_credit = 'P' THEN 9
                         ELSE 5
                         END, 
                    pn_region_id);
            EXCEPTION
                WHEN DUP_VAL_ON_INDEX THEN
                    RAISE_APPLICATION_ERROR(-20303, 'This email is already registered with another card.');
            END;
            UPDATE PSS.CONSUMER_ACCT
               SET CONSUMER_ID = pn_consumer_id
             WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id;
            INSERT INTO PSS.CONSUMER_CREDENTIAL(CONSUMER_ID, CREDENTIAL_HASH, CREDENTIAL_SALT, CREDENTIAL_ALG, RESOURCE_KEY)
                VALUES(pn_consumer_id, pb_credential_hash, pb_credential_salt, pv_credential_alg, 'PREPAID_WEBSITE');
            pv_passcode := PSS.PKG_CONSUMER_MAINT.CREATE_CONSUMER_PASSCODE(pn_consumer_id, 'REGISTER_PREPAID');   
            IF pv_promo_code is not null THEN
               INSERT INTO PSS.CAMPAIGN_CONSUMER_ACCT(CAMPAIGN_ID, CONSUMER_ACCT_ID,TRAN_COUNT, PROMO_TRAN_COUNT) 
               SELECT column_value, pn_consumer_acct_id,0,0 from table(ln_campaign_ids)
               where not exists (select 1 from PSS.CAMPAIGN_CONSUMER_ACCT where CAMPAIGN_ID =column_value and CONSUMER_ACCT_ID=pn_consumer_acct_id);
            END IF;
        END IF;
        IF pn_preferred_comm_type_id = 1 THEN
            pv_username := lv_email;
            pv_communication_email := lv_email;
        ELSIF pn_preferred_comm_type_id = 2 THEN
            pv_username := lv_cell;
            IF pn_carrier_id IS NOT NULL THEN
              SELECT lv_cell || SMS_EMAIL_SUFFIX
                INTO pv_communication_email
                FROM FRONT.SMS_GATEWAY
               WHERE SMS_GATEWAY_ID = pn_carrier_id;
            END IF;
        END IF;
    END;
    
    PROCEDURE CREATE_PREPAID_CONSUMER_SIMPLE(
        pn_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
        pv_security_code PSS.CONSUMER_ACCT.SECURITY_CD_HASH%TYPE,
        pv_first PSS.CONSUMER.CONSUMER_FNAME%TYPE,
        pv_last PSS.CONSUMER.CONSUMER_LNAME%TYPE,
        pn_region_id PSS.CONSUMER.REGION_ID%TYPE,
        pn_consumer_id OUT PSS.CONSUMER.CONSUMER_ID%TYPE,
        pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE)
    IS
        ln_cnt PLS_INTEGER;
        lv_lock VARCHAR2(28);
        ln_consumer_type_id PSS.CONSUMER.CONSUMER_TYPE_ID%TYPE;
        lc_flag CHAR(1);
    BEGIN
      lv_lock := DBADMIN.PKG_GLOBAL.REQUEST_LOCK('PSS.CONSUMER_ACCT', pn_global_account_id); 
      
      SELECT COUNT(*), MAX(CA.CONSUMER_ACCT_ID), MAX(C.CONSUMER_ID), MAX(C.CONSUMER_TYPE_ID),
          MAX(CASE WHEN pv_first=C.CONSUMER_FNAME and pv_last=C.CONSUMER_LNAME THEN 'Y' ELSE 'N' END)
          INTO ln_cnt, pn_consumer_acct_id, pn_consumer_id, ln_consumer_type_id, lc_flag
          FROM PSS.CONSUMER_ACCT CA
          JOIN PSS.CONSUMER_ACCT_BASE CAB ON CA.CONSUMER_ACCT_ID = CAB.CONSUMER_ACCT_ID
          JOIN PSS.CONSUMER C ON CA.CONSUMER_ID = C.CONSUMER_ID
         WHERE CAB.GLOBAL_ACCOUNT_ID = pn_global_account_id           
           AND CA.CONSUMER_ACCT_FMT_ID = 2
           AND CA.CONSUMER_ACCT_TYPE_ID = 3
           AND CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y'
		       AND DBADMIN.PKG_UTL.EQL(CA.SECURITY_CD_HASH, DBADMIN.GET_HASH(UTL_RAW.CAST_TO_RAW(pv_security_code), CA.SECURITY_CD_SALT, 'SHA-256/1000')) = 'Y';
           
        IF ln_cnt = 0 THEN
            RAISE_APPLICATION_ERROR(-20300, 'Did not find an account for card id ' || pn_global_account_id);
        ELSIF ln_consumer_type_id != 4 THEN
          IF lc_flag = 'Y' THEN
            RAISE_APPLICATION_ERROR(-20301, 'Account already registered by you');
          ELSE
            RAISE_APPLICATION_ERROR(-20302, 'Account already registered by someone else');
          END IF;
        ELSE
            VERIFY_REGION_BY_ACCT(pn_consumer_acct_id, pn_region_id);
            SELECT PSS.SEQ_CONSUMER_ID.NEXTVAL
              INTO pn_consumer_id
              FROM DUAL;
            BEGIN
                INSERT INTO PSS.CONSUMER(
                    CONSUMER_ID,
                    CONSUMER_FNAME,
                    CONSUMER_LNAME,
                    CONSUMER_TYPE_ID,
                    REGION_ID)
                VALUES(
                    pn_consumer_id,
                    pv_first,
                    pv_last,
                    5,
                    pn_region_id);
            END;
            
            UPDATE PSS.CONSUMER_ACCT
               SET CONSUMER_ID = pn_consumer_id
             WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id; 
           
        END IF;
    END;
    
    -- R45 and above
    PROCEDURE ADD_PREPAID_ACCT(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pn_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
        pv_security_code PSS.CONSUMER_ACCT.SECURITY_CD_HASH%TYPE,
        pv_nickname PSS.CONSUMER_ACCT.NICK_NAME%TYPE,
        pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE)
    IS
        ln_cnt PLS_INTEGER;
        lc_flag CHAR(1);
        lv_lock VARCHAR2(28);
    BEGIN
        lv_lock := DBADMIN.PKG_GLOBAL.REQUEST_LOCK('PSS.CONSUMER_ACCT', pn_global_account_id);
        SELECT CC.CREDENTIAL_ACTIVE_FLAG
          INTO lc_flag
          FROM PSS.CONSUMER C
          LEFT OUTER JOIN PSS.CONSUMER_CREDENTIAL CC ON C.CONSUMER_ID = CC.CONSUMER_ID AND CC.RESOURCE_KEY = 'PREPAID_WEBSITE'
         WHERE C.CONSUMER_ID = pn_consumer_id
           AND C.CONSUMER_TYPE_ID in (5,7,9,10);
       
        SELECT COUNT(*), MAX(DECODE(C.CONSUMER_TYPE_ID, 4, CA.CONSUMER_ACCT_ID)), MAX(DECODE(C.CONSUMER_ID, pn_consumer_id, 'Y', 'N'))
          INTO ln_cnt, pn_consumer_acct_id, lc_flag
          FROM PSS.CONSUMER_ACCT CA
          JOIN PSS.CONSUMER_ACCT_BASE CAB ON CA.CONSUMER_ACCT_ID = CAB.CONSUMER_ACCT_ID
          JOIN PSS.CONSUMER C ON CA.CONSUMER_ID = C.CONSUMER_ID
         WHERE CAB.GLOBAL_ACCOUNT_ID = pn_global_account_id           
           AND CA.CONSUMER_ACCT_FMT_ID = 2
           AND CA.CONSUMER_ACCT_TYPE_ID = 3
		   AND CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y'
		   AND (pv_security_code is NULL or DBADMIN.PKG_UTL.EQL(CA.SECURITY_CD_HASH, DBADMIN.GET_HASH(UTL_RAW.CAST_TO_RAW(pv_security_code), CA.SECURITY_CD_SALT, 'SHA-256/1000')) = 'Y');
        IF ln_cnt = 0 THEN
            RAISE_APPLICATION_ERROR(-20300, 'Did not find an account for card id ' || pn_global_account_id);
        ELSIF pn_consumer_acct_id IS NULL THEN
            IF lc_flag = 'Y' THEN
                RAISE_APPLICATION_ERROR(-20301, 'Account already registered to this email');
            ELSE
                RAISE_APPLICATION_ERROR(-20302, 'Account already registered to another email');
            END IF;
        ELSE
            UPDATE PSS.CONSUMER_ACCT
               SET CONSUMER_ID = pn_consumer_id,
               NICK_NAME = pv_nickname
             WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id; 
             ASSIGN_ACCT_BY_PROMO(pn_consumer_id, pn_consumer_acct_id);           
        END IF;
    END;
    
     PROCEDURE ADD_PAYROLL_DEDUCT_ACCT(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pn_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
        pv_security_code PSS.CONSUMER_ACCT.SECURITY_CD_HASH%TYPE,
        pv_nickname PSS.CONSUMER_ACCT.NICK_NAME%TYPE,
        pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE)
    IS
        ln_cnt PLS_INTEGER;
        lc_flag CHAR(1);
        lv_lock VARCHAR2(28);
    BEGIN
        lv_lock := DBADMIN.PKG_GLOBAL.REQUEST_LOCK('PSS.CONSUMER_ACCT', pn_global_account_id);
        SELECT CC.CREDENTIAL_ACTIVE_FLAG
          INTO lc_flag
          FROM PSS.CONSUMER C
          LEFT OUTER JOIN PSS.CONSUMER_CREDENTIAL CC ON C.CONSUMER_ID = CC.CONSUMER_ID AND CC.RESOURCE_KEY = 'PREPAID_WEBSITE'
         WHERE C.CONSUMER_ID = pn_consumer_id
           AND C.CONSUMER_TYPE_ID in (5,7,9,10);
       
        SELECT COUNT(*), MAX(DECODE(C.CONSUMER_TYPE_ID, 4, CA.CONSUMER_ACCT_ID)), MAX(DECODE(C.CONSUMER_ID, pn_consumer_id, 'Y', 'N'))
          INTO ln_cnt, pn_consumer_acct_id, lc_flag
          FROM PSS.CONSUMER_ACCT CA
          JOIN PSS.CONSUMER_ACCT_BASE CAB ON CA.CONSUMER_ACCT_ID = CAB.CONSUMER_ACCT_ID
          JOIN PSS.CONSUMER C ON CA.CONSUMER_ID = C.CONSUMER_ID
         WHERE CAB.GLOBAL_ACCOUNT_ID = pn_global_account_id           
           AND CA.CONSUMER_ACCT_FMT_ID = 3
           AND CA.CONSUMER_ACCT_TYPE_ID = 7
		   AND CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y'
		   AND (pv_security_code is NULL or DBADMIN.PKG_UTL.EQL(CA.SECURITY_CD_HASH, DBADMIN.GET_HASH(UTL_RAW.CAST_TO_RAW(pv_security_code), CA.SECURITY_CD_SALT, 'SHA-256/1000')) = 'Y');
        IF ln_cnt = 0 THEN
            RAISE_APPLICATION_ERROR(-20300, 'Did not find an account for card id ' || pn_global_account_id);
        ELSIF pn_consumer_acct_id IS NULL THEN
            IF lc_flag = 'Y' THEN
                RAISE_APPLICATION_ERROR(-20301, 'Account already registered to this email');
            ELSE
                RAISE_APPLICATION_ERROR(-20302, 'Account already registered to another email');
            END IF;
        ELSE
            UPDATE PSS.CONSUMER_ACCT
               SET CONSUMER_ID = pn_consumer_id,
               NICK_NAME = pv_nickname
             WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id; 
             ASSIGN_ACCT_BY_PROMO(pn_consumer_id, pn_consumer_acct_id);           
        END IF;
    END;
    
    PROCEDURE ADD_CREDIT_ACCT(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pn_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
        pv_currency_cd VARCHAR2,
        pd_exp_date PSS.CONSUMER_ACCT.EXPIRATION_DATE%TYPE,
        pv_nickname PSS.CONSUMER_ACCT.NICK_NAME%TYPE,
        pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
        pv_promo_code REPORT.CAMPAIGN.PROMO_CODE%TYPE DEFAULT NULL)
   IS
      lc_flag CHAR(1);
      lv_lock VARCHAR2(28);
      ln_consumer_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE;
      lv_consumer_acct_cd PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD%TYPE;
      lv_consumer_email_addr1 PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE;
      ln_softcard_cnt PLS_INTEGER;
      ln_promo_count NUMBER;
      ln_promo_code_promo_count NUMBER;
      lc_need_add_promo CHAR(1):='N';
      ln_promo_campaign_count NUMBER;
      
   BEGIN
    lv_lock := DBADMIN.PKG_GLOBAL.REQUEST_LOCK('PSS.CONSUMER_ACCT', pn_global_account_id);
    
    select count(distinct c.campaign_id), count(distinct c2.campaign_id)
    into ln_promo_count,ln_promo_code_promo_count
    from pss.consumer_promocode cp
    join report.campaign c on cp.campaign_id=c.campaign_id and c.promo_code is not null and (c.END_DATE is NULL or c.END_DATE > Sysdate-1) and c.status='A'
    left outer join report.campaign c2 on cp.campaign_id=c2.campaign_id and upper(c2.promo_code)=upper(pv_promo_code) 
    where cp.consumer_id = pn_consumer_id and (c.campaign_type_id in(4,5) or c.campaign_type_id = 1 and c.is_for_all_cards = 'Y');
    
    select count(campaign_id)
    into ln_promo_campaign_count
    from report.campaign where (END_DATE is NULL or END_DATE > Sysdate-1) and status='A' 
                        	and upper(promo_code)=upper(pv_promo_code) 
                        	and (campaign_type_id in(4,5) or campaign_type_id = 1 and is_for_all_cards = 'Y');
    
    IF ln_promo_count = 0 THEN
      IF pv_promo_code is NULL OR ln_promo_campaign_count = 0 THEN
          RAISE_APPLICATION_ERROR(-20330, 'No credit card promotion exsits for consumer ' || pn_consumer_id);
      ELSE
        lc_need_add_promo:='Y';
      END IF;
    ELSE
      IF pv_promo_code is NOT NULL AND ln_promo_code_promo_count = 0 THEN
        ADD_PROMO(pv_promo_code,pn_consumer_id);
      END IF;
    END IF;
        SELECT MAX(CONSUMER_ACCT_ID), MAX(CONSUMER_ID), MAX(TRUNCATED_CARD_NUMBER), MAX(CONSUMER_EMAIL_ADDR1) 
        INTO pn_consumer_acct_id, ln_consumer_id, lv_consumer_acct_cd,lv_consumer_email_addr1
              FROM (
            SELECT CAB.CONSUMER_ACCT_ID, CAB.CONSUMER_ID, CAB.TRUNCATED_CARD_NUMBER, CAB.CONSUMER_EMAIL_ADDR1
              FROM (
                SELECT CAB.CONSUMER_ACCT_ID, 
                       CASE WHEN CA.CONSUMER_ACCT_ACTIVE_YN_FLAG IS NULL THEN 5 WHEN CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y' AND NVL(CA.CONSUMER_ACCT_ACTIVATION_TS, MIN_DATE) <= SYSDATE AND NVL(CA.CONSUMER_ACCT_DEACTIVATION_TS, MAX_DATE) > SYSDATE THEN 1 ELSE 10 END CA_PRIORITY,
                       CASE WHEN 1 = CAB.PAYMENT_SUBTYPE_ID THEN 1 ELSE 10 END PST_PRIORITY,
                       CASE WHEN 1 = NVL(CAB.LOCATION_ID, 0) THEN 1 ELSE 10 END LOCATION_PRIORITY,
                       CA.CONSUMER_ACCT_ISSUE_NUM,
                       C.CONSUMER_ID,
                       C.CONSUMER_IDENTIFIER,
                       C.CONSUMER_EMAIL_ADDR1,
                       CAB.TRUNCATED_CARD_NUMBER
                  FROM PSS.CONSUMER_ACCT_BASE CAB
                  LEFT OUTER JOIN (PSS.CONSUMER_ACCT CA 
                  JOIN PSS.CONSUMER C ON CA.CONSUMER_ID = C.CONSUMER_ID) ON CAB.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
                 WHERE CAB.GLOBAL_ACCOUNT_ID = pn_global_account_id
                   AND CAB.CURRENCY_CD = pv_currency_cd) CAB
             ORDER BY CAB.CA_PRIORITY, CAB.PST_PRIORITY, CAB.LOCATION_PRIORITY, CAB.CONSUMER_ACCT_ISSUE_NUM DESC, CAB.CONSUMER_IDENTIFIER NULLS LAST
             ) WHERE ROWNUM = 1;
             
       IF pn_consumer_acct_id is NULL THEN
        RAISE_APPLICATION_ERROR(-20300, 'Did not find an account for card id ' || pn_global_account_id);
       ELSE
        IF ln_consumer_id IS NULL THEN
            INSERT INTO PSS.CONSUMER_ACCT(CONSUMER_ACCT_ID, CONSUMER_ACCT_CD, CONSUMER_ACCT_ACTIVE_YN_FLAG, CONSUMER_ACCT_BALANCE, CONSUMER_ID, LOCATION_ID, 
           CONSUMER_ACCT_ISSUE_NUM, PAYMENT_SUBTYPE_ID, CURRENCY_CD, CONSUMER_ACCT_TYPE_ID, EXPIRATION_DATE, CONSUMER_ACCT_SUB_TYPE_ID) 
            VALUES(pn_consumer_acct_id, lv_consumer_acct_cd, 'Y', 0, pn_consumer_id, 1, 1, 1, pv_currency_cd, 5, pd_exp_date, 1);
                    
            UPDATE PSS.CONSUMER_ACCT_BASE SET PAYMENT_SUBTYPE_ID =1 WHERE CONSUMER_ACCT_ID=pn_consumer_acct_id; 
            IF lc_need_add_promo = 'Y' THEN
              ADD_PROMO(pv_promo_code,pn_consumer_id);
            END IF;
            ASSIGN_ACCT_BY_PROMO(pn_consumer_id, pn_consumer_acct_id);
        ELSE
          IF ln_consumer_id = pn_consumer_id THEN
             RAISE_APPLICATION_ERROR(-20301, 'Account already registered to this user');
          ELSE
             select count(1) into ln_softcard_cnt from dual where REGEXP_LIKE(lv_consumer_email_addr1, '^(isis|softcard).*usatech.com$');
             IF ln_softcard_cnt > 0 THEN                   
                    UPDATE PSS.CONSUMER_ACCT SET EXPIRATION_DATE=pd_exp_date, consumer_id= pn_consumer_id, CONSUMER_ACCT_SUB_TYPE_ID = 1 WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id;                                       
                    IF lc_need_add_promo = 'Y' THEN
                      ADD_PROMO(pv_promo_code,pn_consumer_id);
                    ELSE
                    INSERT INTO PSS.CAMPAIGN_CONSUMER_ACCT(CAMPAIGN_ID, CONSUMER_ACCT_ID,TRAN_COUNT, PROMO_TRAN_COUNT) 
                    select distinct cca.campaign_id, pn_consumer_acct_id,0,0 from pss.campaign_consumer_acct cca 
                    where consumer_acct_id in (select consumer_acct_id from pss.consumer_acct where consumer_id=pn_consumer_id and consumer_acct_type_id=5)
                    and not exists (select 1 from PSS.CAMPAIGN_CONSUMER_ACCT where CAMPAIGN_ID =cca.campaign_id and CONSUMER_ACCT_ID=pn_consumer_acct_id);
                    END IF;
                    ASSIGN_ACCT_BY_PROMO(pn_consumer_id, pn_consumer_acct_id);
            ELSE
                    RAISE_APPLICATION_ERROR(-20302, 'Account already registered to another user');
            END IF;
          END IF;
        END IF;
       END IF;
   END;
   
    PROCEDURE ADD_SPROUT_ACCT(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pn_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
        pv_security_code PSS.CONSUMER_ACCT.SECURITY_CD_HASH%TYPE,
        pv_nickname PSS.CONSUMER_ACCT.NICK_NAME%TYPE,
        pn_consumer_acct_identifier PSS.CONSUMER_ACCT.CONSUMER_ACCT_IDENTIFIER%TYPE,
        pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE)
    IS
        lv_lock VARCHAR2(28);
        ln_cnt PLS_INTEGER;
        ln_consumer_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE;
        lv_consumer_acct_cd PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD%TYPE;
        ln_consumer_type_id PSS.CONSUMER.CONSUMER_TYPE_ID%TYPE;
    BEGIN
        lv_lock := DBADMIN.PKG_GLOBAL.REQUEST_LOCK('PSS.CONSUMER_ACCT', pn_global_account_id);
    
        SELECT count(*), MAX(CONSUMER_ACCT_ID), MAX(CONSUMER_ID), MAX(TRUNCATED_CARD_NUMBER), MAX(CONSUMER_TYPE_ID)
        INTO ln_cnt, pn_consumer_acct_id, ln_consumer_id, lv_consumer_acct_cd,ln_consumer_type_id
              FROM (
            SELECT CAB.CONSUMER_ACCT_ID, CAB.CONSUMER_ID, CAB.TRUNCATED_CARD_NUMBER, CAB.CONSUMER_TYPE_ID
              FROM (
                SELECT CAB.CONSUMER_ACCT_ID, 
                       CASE WHEN CA.CONSUMER_ACCT_ACTIVE_YN_FLAG IS NULL THEN 5 WHEN CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y' AND NVL(CA.CONSUMER_ACCT_ACTIVATION_TS, MIN_DATE) <= SYSDATE AND NVL(CA.CONSUMER_ACCT_DEACTIVATION_TS, MAX_DATE) > SYSDATE THEN 1 ELSE 10 END CA_PRIORITY,
                       CASE WHEN 1 = CAB.PAYMENT_SUBTYPE_ID THEN 1 ELSE 10 END PST_PRIORITY,
                       CASE WHEN 1 = NVL(CAB.LOCATION_ID, 0) THEN 1 ELSE 10 END LOCATION_PRIORITY,
                       CA.CONSUMER_ACCT_ISSUE_NUM,
                       C.CONSUMER_ID,
                       C.CONSUMER_IDENTIFIER,
                       C.CONSUMER_TYPE_ID,
                       CAB.TRUNCATED_CARD_NUMBER
                  FROM PSS.CONSUMER_ACCT_BASE CAB
                  JOIN PSS.CONSUMER_ACCT CA ON CAB.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID AND CA.CONSUMER_ACCT_TYPE_ID=6
                  JOIN PSS.CONSUMER C ON CA.CONSUMER_ID = C.CONSUMER_ID
                 WHERE CAB.GLOBAL_ACCOUNT_ID = pn_global_account_id
                   AND CAB.CURRENCY_CD = 'USD') CAB
             ORDER BY CAB.CA_PRIORITY, CAB.PST_PRIORITY, CAB.LOCATION_PRIORITY, CAB.CONSUMER_ACCT_ISSUE_NUM DESC, CAB.CONSUMER_IDENTIFIER NULLS LAST
             ) WHERE ROWNUM = 1 ;      
             
       IF ln_cnt = 0  THEN
        RAISE_APPLICATION_ERROR(-20300, 'Did not find an account for card id ' || pn_global_account_id);
       ELSE
        IF ln_consumer_type_id = 4 THEN
            UPDATE PSS.CONSUMER_ACCT
               SET CONSUMER_ID = pn_consumer_id,
               NICK_NAME = pv_nickname
             WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id;
                    
            INSERT INTO PSS.CAMPAIGN_CONSUMER_ACCT(CAMPAIGN_ID, CONSUMER_ACCT_ID,TRAN_COUNT, PROMO_TRAN_COUNT) 
            SELECT distinct cca.campaign_id, pn_consumer_acct_id,0,0 from pss.campaign_consumer_acct cca
            where consumer_acct_id in (select consumer_acct_id from pss.consumer_acct where consumer_id=pn_consumer_id and consumer_acct_type_id=6)
            and not exists (select 1 from PSS.CAMPAIGN_CONSUMER_ACCT where CAMPAIGN_ID =cca.campaign_id and CONSUMER_ACCT_ID=pn_consumer_acct_id);
        ELSE
          IF ln_consumer_id = pn_consumer_id THEN
             RAISE_APPLICATION_ERROR(-20301, 'Account already registered to this user');
          ELSE
             RAISE_APPLICATION_ERROR(-20302, 'Account already registered to another user');
          END IF;
        END IF;
       END IF;
    END;
    
    -- R42 and above
    PROCEDURE UPDATE_PREPAID_CONSUMER(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pv_email PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pv_cell PSS.CONSUMER.CONSUMER_CELL_PHONE_NUM%TYPE DEFAULT NULL,
        pn_carrier_id PSS.CONSUMER.CONSUMER_CELL_SMS_GATEWAY_ID%TYPE DEFAULT NULL,
        pn_preferred_comm_type_id PSS.CONSUMER.PREFERRED_COMM_TYPE_ID%TYPE,
        pv_first PSS.CONSUMER.CONSUMER_FNAME%TYPE,
        pv_last PSS.CONSUMER.CONSUMER_LNAME%TYPE,
        pv_addr1 PSS.CONSUMER.CONSUMER_ADDR1%TYPE,
        pv_city PSS.CONSUMER.CONSUMER_CITY%TYPE,
        pv_state PSS.CONSUMER.CONSUMER_STATE_CD%TYPE,
        pv_postal PSS.CONSUMER.CONSUMER_POSTAL_CD%TYPE,
        pv_country PSS.CONSUMER.CONSUMER_COUNTRY_CD%TYPE,
        pb_credential_hash PSS.CONSUMER_CREDENTIAL.CREDENTIAL_HASH%TYPE,
        pb_credential_salt PSS.CONSUMER_CREDENTIAL.CREDENTIAL_SALT%TYPE,
        pv_credential_alg PSS.CONSUMER_CREDENTIAL.CREDENTIAL_ALG%TYPE,
        pv_passcode OUT PSS.CONSUMER_PASSCODE.PASSCODE%TYPE,
        pv_username OUT PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pv_communication_email OUT PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pn_region_id PSS.CONSUMER.REGION_ID%TYPE DEFAULT NULL)
    IS
        ln_cnt PLS_INTEGER;
        lc_comm_address_changed CHAR(1);
        lv_city PSS.CONSUMER.CONSUMER_CITY%TYPE;
        lv_state_cd PSS.CONSUMER.CONSUMER_STATE_CD%TYPE;        
        lv_email PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE;
        lv_cell PSS.CONSUMER.CONSUMER_CELL_PHONE_NUM%TYPE;  
    BEGIN
        VERIFY_REGION(pn_consumer_id, pn_region_id);
        SELECT TRIM(pv_email), REGEXP_REPLACE(pv_cell, '\s*\(?\s*(\d{3})\s*\)?\s*[-.]?\s*(\d{3})\s*[-.]?\s*(\d{4})\s*', '\1\2\3') -- format these two fields first
          INTO lv_email, lv_cell
          FROM DUAL;        
        IF pv_state IS NOT NULL THEN
            SELECT COUNT(*)
              INTO ln_cnt
              FROM LOCATION.STATE
             WHERE STATE_CD = pv_state;
            IF ln_cnt = 0 THEN
                RAISE_APPLICATION_ERROR(-20309, 'Invalid state ' || pv_state || ' provided');
            END IF;
            lv_city := pv_city;
            lv_state_cd := pv_state;
        ELSE
            SELECT MAX(CITY), MAX(STATE_CD)
              INTO lv_city, lv_state_cd
              FROM (
              SELECT P.CITY, S.STATE_CD
                FROM FRONT.POSTAL P 
                JOIN CORP.STATE S ON P.STATE_ID = S.STATE_ID
                JOIN CORP.COUNTRY C ON S.COUNTRY_CD = C.COUNTRY_CD
              WHERE P.POSTAL_CD = REGEXP_REPLACE(pv_postal, C.POSTAL_REGEX, 
                (SELECT LISTAGG('\' || LEVEL, '') WITHIN GROUP (ORDER BY LEVEL) FROM DUAL CONNECT BY LEVEL <= C.POSTAL_PRIMARY_PARTS))
                AND C.COUNTRY_CD = pv_country
                AND C.POSTAL_REGEX IS NOT NULL
              ORDER BY P.POSTAL_ID ASC)
            WHERE ROWNUM = 1;
        END IF;
        SELECT CASE WHEN PREFERRED_COMM_TYPE_ID != pn_preferred_comm_type_id THEN 'Y'
                    WHEN PREFERRED_COMM_TYPE_ID = 1 AND UPPER(CONSUMER_EMAIL_ADDR1) != UPPER(lv_email) THEN 'Y'
                    WHEN PREFERRED_COMM_TYPE_ID = 2 AND CONSUMER_CELL_PHONE_NUM != lv_cell THEN 'Y'
                    ELSE 'N'
               END
          INTO lc_comm_address_changed
          FROM PSS.CONSUMER
         WHERE CONSUMER_ID = pn_consumer_id
           AND CONSUMER_TYPE_ID in (5,7,8,9,10)
           FOR UPDATE;   
        UPDATE PSS.CONSUMER
           SET CONSUMER_EMAIL_ADDR1 = TRIM(pv_email),
               CONSUMER_CELL_PHONE_NUM = lv_cell,
               CONSUMER_CELL_SMS_GATEWAY_ID = pn_carrier_id,
               PREFERRED_COMM_TYPE_ID = pn_preferred_comm_type_id,
               CONSUMER_FNAME = pv_first,
               CONSUMER_LNAME = pv_last,
               CONSUMER_ADDR1 = pv_addr1,
               CONSUMER_CITY = lv_city,
               CONSUMER_STATE_CD = lv_state_cd,
               CONSUMER_POSTAL_CD = pv_postal,
               CONSUMER_COUNTRY_CD = pv_country,
               REGION_ID = pn_region_id
         WHERE CONSUMER_ID = pn_consumer_id
           AND CONSUMER_TYPE_ID in (5,7,8,9,10);
         
        IF pb_credential_hash IS NOT NULL THEN
            BEGIN
              UPDATE PSS.CONSUMER_CREDENTIAL
                 SET CREDENTIAL_HASH = pb_credential_hash, 
                     CREDENTIAL_SALT = pb_credential_salt,
                     CREDENTIAL_ALG = pv_credential_alg,
                     CREDENTIAL_ACTIVE_FLAG = DECODE(lc_comm_address_changed, 'N', CREDENTIAL_ACTIVE_FLAG, 'A')
               WHERE CONSUMER_ID = pn_consumer_id
                 AND RESOURCE_KEY = 'PREPAID_WEBSITE';
            IF sql%rowcount = 0 THEN
                INSERT INTO PSS.CONSUMER_CREDENTIAL(CONSUMER_ID, CREDENTIAL_HASH, CREDENTIAL_SALT, CREDENTIAL_ALG, RESOURCE_KEY)
                      VALUES(pn_consumer_id, pb_credential_hash, pb_credential_salt, pv_credential_alg, 'PREPAID_WEBSITE');
            END IF;
            END;
            IF lc_comm_address_changed = 'Y' THEN
                pv_passcode := PSS.PKG_CONSUMER_MAINT.CREATE_CONSUMER_PASSCODE(pn_consumer_id, 'REGISTER_PREPAID');     
            END IF;
        ELSIF lc_comm_address_changed = 'Y' THEN
            UPDATE PSS.CONSUMER_CREDENTIAL
               SET CREDENTIAL_ACTIVE_FLAG = 'N'
             WHERE CONSUMER_ID = pn_consumer_id
               AND RESOURCE_KEY = 'PREPAID_WEBSITE';
            pv_passcode := PSS.PKG_CONSUMER_MAINT.CREATE_CONSUMER_PASSCODE(pn_consumer_id, 'REGISTER_PREPAID');
        END IF;
        IF pn_preferred_comm_type_id = 1 THEN
            pv_username := lv_email;
            pv_communication_email := lv_email;
        ELSIF pn_preferred_comm_type_id = 2 THEN
            pv_username := lv_cell;
            IF pn_carrier_id IS NOT NULL THEN
              SELECT lv_cell || SMS_EMAIL_SUFFIX
                INTO pv_communication_email
                FROM FRONT.SMS_GATEWAY
               WHERE SMS_GATEWAY_ID = pn_carrier_id;
            END IF;
        END IF;
    EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
            RAISE_APPLICATION_ERROR(-20303, 'This email is already registered with another card.');
    END;
    
    PROCEDURE UPDATE_CONSUMER_SIMPLE(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pv_first PSS.CONSUMER.CONSUMER_FNAME%TYPE,
        pv_last PSS.CONSUMER.CONSUMER_LNAME%TYPE,
        pn_region_id PSS.CONSUMER.REGION_ID%TYPE)
    IS
    BEGIN
      VERIFY_REGION(pn_consumer_id, pn_region_id);
      UPDATE PSS.CONSUMER
      SET CONSUMER_FNAME = pv_first, CONSUMER_LNAME = pv_last, REGION_ID = pn_region_id
      WHERE CONSUMER_ID = pn_consumer_id AND CONSUMER_TYPE_ID = 5;
    END;
    
    --R42 and above
    PROCEDURE REGISTER_PREPAID_CONSUMER(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pv_passcode PSS.CONSUMER_PASSCODE.PASSCODE%TYPE,
        pv_username OUT PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pv_communication_email OUT PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pn_preferred_comm_type_id OUT PSS.CONSUMER.PREFERRED_COMM_TYPE_ID%TYPE,
        pv_first OUT PSS.CONSUMER.CONSUMER_FNAME%TYPE,
        pv_last OUT PSS.CONSUMER.CONSUMER_LNAME%TYPE)
    IS
        ld_exp_ts PSS.CONSUMER_PASSCODE.EXPIRATION_TS%TYPE;
        ln_consumer_passcode_id PSS.CONSUMER_PASSCODE.CONSUMER_PASSCODE_ID%TYPE;
        lc_active_flag PSS.CONSUMER_CREDENTIAL.CREDENTIAL_ACTIVE_FLAG%TYPE;
    BEGIN
        SELECT DECODE(C.PREFERRED_COMM_TYPE_ID, 1, C.CONSUMER_EMAIL_ADDR1, 2, C.CONSUMER_CELL_PHONE_NUM),
               C.PREFERRED_COMM_TYPE_ID,
               C.CONSUMER_FNAME, 
               C.CONSUMER_LNAME,
               CP.EXPIRATION_TS,
               CP.CONSUMER_PASSCODE_ID,
               CC.CREDENTIAL_ACTIVE_FLAG
          INTO pv_username, pn_preferred_comm_type_id, pv_first, pv_last, ld_exp_ts, ln_consumer_passcode_id, lc_active_flag
          FROM PSS.CONSUMER C
          LEFT OUTER JOIN (PSS.CONSUMER_PASSCODE CP
          JOIN PSS.PASSCODE_TYPE PT ON CP.PASSCODE_TYPE_ID = PT.PASSCODE_TYPE_ID AND PT.PASSCODE_TYPE_CD = 'REGISTER_PREPAID' AND CP.PASSCODE = pv_passcode) 
            ON C.CONSUMER_ID = CP.CONSUMER_ID
          LEFT OUTER JOIN PSS.CONSUMER_CREDENTIAL CC ON C.CONSUMER_ID = CC.CONSUMER_ID AND CC.RESOURCE_KEY = 'PREPAID_WEBSITE'
         WHERE C.CONSUMER_ID = pn_consumer_id;
        IF lc_active_flag IS NULL THEN
            RAISE_APPLICATION_ERROR(-20323, 'Credentials not found for consumer ' || pn_consumer_id);
        ELSIF lc_active_flag = 'Y' THEN
            RAISE_APPLICATION_ERROR(-20324, 'Consumer ' || pn_consumer_id || ' is already registered');
        ELSIF ln_consumer_passcode_id IS NULL THEN
            RAISE_APPLICATION_ERROR(-20321, 'Passcode ' || pv_passcode || ' does not exist for consumer ' || pn_consumer_id);
        ELSIF ld_exp_ts < SYSDATE THEN
            RAISE_APPLICATION_ERROR(-20322, 'Passcode ' || pv_passcode || ' is expired for consumer ' || pn_consumer_id || ' as of ' || TO_CHAR(ld_exp_ts, 'MM/DD/YYYY HH24:MI:SS'));
        ELSE
            UPDATE PSS.CONSUMER_CREDENTIAL
               SET CREDENTIAL_ACTIVE_FLAG = 'Y'
             WHERE CONSUMER_ID = pn_consumer_id
               AND RESOURCE_KEY = 'PREPAID_WEBSITE'
               AND CREDENTIAL_ACTIVE_FLAG IN('A', 'N');
            UPDATE PSS.CONSUMER_PASSCODE
               SET EXPIRATION_TS = SYSDATE
             WHERE CONSUMER_PASSCODE_ID = ln_consumer_passcode_id;
        END IF;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RAISE_APPLICATION_ERROR(-20320, 'Consumer ' || pn_consumer_id || ' does not exist');
    END;
    
    -- R42 and above
    PROCEDURE CREATE_PASSCODE_FOR_PASSWORD(
        pv_username PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pn_consumer_id OUT PSS.CONSUMER.CONSUMER_ID%TYPE,
        pn_preferred_comm_type_id OUT PSS.CONSUMER.PREFERRED_COMM_TYPE_ID%TYPE,
        pv_first OUT PSS.CONSUMER.CONSUMER_FNAME%TYPE,
        pv_last OUT PSS.CONSUMER.CONSUMER_LNAME%TYPE,
        pv_passcode OUT PSS.CONSUMER_PASSCODE.PASSCODE%TYPE)
    IS
    BEGIN
        SELECT CONSUMER_ID,
               PREFERRED_COMM_TYPE_ID,
               CONSUMER_FNAME, 
               CONSUMER_LNAME
          INTO pn_consumer_id, pn_preferred_comm_type_id, pv_first, pv_last
          FROM (
        SELECT C.CONSUMER_ID,
               C.PREFERRED_COMM_TYPE_ID,
               C.CONSUMER_FNAME, 
               C.CONSUMER_LNAME
          FROM PSS.CONSUMER C
         WHERE (UPPER(C.CONSUMER_EMAIL_ADDR1) = UPPER(TRIM(pv_username))
            OR C.CONSUMER_CELL_PHONE_NUM = REGEXP_REPLACE(pv_username, '\s*\(?\s*(\d{3})\s*\)?\s*[-.]?\s*(\d{3})\s*[-.]?\s*(\d{4})\s*', '\1\2\3')) 
           AND CONSUMER_TYPE_ID in(5,7,9,10)
         ORDER BY CASE 
                    WHEN UPPER(C.CONSUMER_EMAIL_ADDR1) != TRIM(UPPER(pv_username)) THEN 0
                    WHEN C.PREFERRED_COMM_TYPE_ID = 1 THEN 10
                    ELSE 1 
                  END +
                  CASE 
                    WHEN C.CONSUMER_CELL_PHONE_NUM != REGEXP_REPLACE(pv_username, '\s*\(?\s*(\d{3})\s*\)?\s*[-.]?\s*(\d{3})\s*[-.]?\s*(\d{4})\s*', '\1\2\3') THEN 0
                    WHEN C.PREFERRED_COMM_TYPE_ID = 2 THEN 10
                    ELSE 1 
                  END DESC)
         WHERE ROWNUM = 1;
         
        SELECT MAX(PASSCODE)
          INTO pv_passcode
          FROM PSS.CONSUMER_PASSCODE CP
          JOIN PSS.PASSCODE_TYPE PT ON CP.PASSCODE_TYPE_ID = PT.PASSCODE_TYPE_ID AND PT.PASSCODE_TYPE_CD = 'RESET_PREPAID_PASS' 
         WHERE CP.CONSUMER_ID = pn_consumer_id AND EXPIRATION_TS > SYSDATE;
        IF pv_passcode IS NULL THEN
          pv_passcode := PSS.PKG_CONSUMER_MAINT.CREATE_CONSUMER_PASSCODE(pn_consumer_id, 'RESET_PREPAID_PASS');
        END IF;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RAISE_APPLICATION_ERROR(-20320, 'Consumer ' || pn_consumer_id || ' does not exist');
    END;
    
    -- R42 and above
    PROCEDURE UPDATE_PREPAID_PASSWORD(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pb_credential_hash PSS.CONSUMER_CREDENTIAL.CREDENTIAL_HASH%TYPE,
        pb_credential_salt PSS.CONSUMER_CREDENTIAL.CREDENTIAL_SALT%TYPE,
        pv_credential_alg PSS.CONSUMER_CREDENTIAL.CREDENTIAL_ALG%TYPE)
    IS
    BEGIN
        UPDATE PSS.CONSUMER_CREDENTIAL
           SET CREDENTIAL_HASH = pb_credential_hash, 
               CREDENTIAL_SALT = pb_credential_salt,
               CREDENTIAL_ALG = pv_credential_alg
         WHERE CONSUMER_ID = pn_consumer_id
           AND RESOURCE_KEY = 'PREPAID_WEBSITE';
    END;
    
    PROCEDURE UPDATE_PREPAID_PASSWORD(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pv_passcode PSS.CONSUMER_PASSCODE.PASSCODE%TYPE,
        pb_credential_hash PSS.CONSUMER_CREDENTIAL.CREDENTIAL_HASH%TYPE,
        pb_credential_salt PSS.CONSUMER_CREDENTIAL.CREDENTIAL_SALT%TYPE,
        pv_credential_alg PSS.CONSUMER_CREDENTIAL.CREDENTIAL_ALG%TYPE)
    IS
        ln_consumer_passcode_id PSS.CONSUMER_PASSCODE.CONSUMER_PASSCODE_ID%TYPE;
    BEGIN
        SELECT 
               MAX(CP.CONSUMER_PASSCODE_ID)
          INTO ln_consumer_passcode_id
          FROM PSS.CONSUMER_PASSCODE CP
          JOIN PSS.PASSCODE_TYPE PT ON CP.PASSCODE_TYPE_ID = PT.PASSCODE_TYPE_ID AND PT.PASSCODE_TYPE_CD = 'RESET_PREPAID_PASS' AND CP.PASSCODE = pv_passcode
         WHERE CP.CONSUMER_ID = pn_consumer_id AND EXPIRATION_TS > SYSDATE;
        IF ln_consumer_passcode_id IS NULL THEN
            RAISE_APPLICATION_ERROR(-20321, 'Passcode ' || pv_passcode || ' does not exist for consumer ' || pn_consumer_id);
        END IF;

        DELETE FROM PSS.CONSUMER_PASSCODE
        WHERE CONSUMER_ID = pn_consumer_id AND PASSCODE = pv_passcode;
        
        UPDATE PSS.CONSUMER_CREDENTIAL
           SET CREDENTIAL_HASH = pb_credential_hash, 
               CREDENTIAL_SALT = pb_credential_salt,
               CREDENTIAL_ALG = pv_credential_alg
         WHERE CONSUMER_ID = pn_consumer_id
           AND RESOURCE_KEY = 'PREPAID_WEBSITE';
    END;
   
   --R42 and above
   PROCEDURE REREGISTER_PREPAID_CONSUMER(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pv_username OUT PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pv_communication_email OUT PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pn_preferred_comm_type_id OUT PSS.CONSUMER.PREFERRED_COMM_TYPE_ID%TYPE,
        pv_first OUT PSS.CONSUMER.CONSUMER_FNAME%TYPE,
        pv_last OUT PSS.CONSUMER.CONSUMER_LNAME%TYPE,
        pv_passcode OUT PSS.CONSUMER_PASSCODE.PASSCODE%TYPE)
    IS
    BEGIN
        SELECT DECODE(C.PREFERRED_COMM_TYPE_ID, 1, C.CONSUMER_EMAIL_ADDR1, 2, C.CONSUMER_CELL_PHONE_NUM),
               DECODE(C.PREFERRED_COMM_TYPE_ID, 1, C.CONSUMER_EMAIL_ADDR1, 2, C.CONSUMER_CELL_PHONE_NUM || SG.SMS_EMAIL_SUFFIX),
               C.PREFERRED_COMM_TYPE_ID,
               C.CONSUMER_FNAME, 
               C.CONSUMER_LNAME
          INTO pv_username, pv_communication_email, pn_preferred_comm_type_id, pv_first, pv_last
          FROM PSS.CONSUMER C
          LEFT OUTER JOIN FRONT.SMS_GATEWAY SG ON C.CONSUMER_CELL_SMS_GATEWAY_ID = SG.SMS_GATEWAY_ID
         WHERE C.CONSUMER_ID = pn_consumer_id;
        pv_passcode := PSS.PKG_CONSUMER_MAINT.CREATE_CONSUMER_PASSCODE(pn_consumer_id, 'REGISTER_PREPAID');
    END;
    
    PROCEDURE CANCEL_PASSCODE(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pv_passcode PSS.CONSUMER_PASSCODE.PASSCODE%TYPE)
    IS
        ld_exp_ts PSS.CONSUMER_PASSCODE.EXPIRATION_TS%TYPE;
        ln_consumer_passcode_id PSS.CONSUMER_PASSCODE.CONSUMER_PASSCODE_ID%TYPE;
    BEGIN
        UPDATE PSS.CONSUMER_PASSCODE
           SET EXPIRATION_TS = LEAST(SYSDATE, NVL(EXPIRATION_TS, MAX_DATE))
         WHERE PASSCODE = pv_passcode
           AND CONSUMER_ID = pn_consumer_id;
        IF SQL%NOTFOUND THEN
            RAISE_APPLICATION_ERROR(-20321, 'Passcode ' || pv_passcode || ' does not exist for consumer ' || pn_consumer_id);
        END IF;
    END;
    
    PROCEDURE UPDATE_CONSUMER_SETTING(
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
        pn_param_id PSS.CONSUMER_SETTING.CONSUMER_SETTING_PARAM_ID%TYPE,
        pv_param_value PSS.CONSUMER_SETTING.CONSUMER_SETTING_VALUE%TYPE)
    IS
        lv_param_value_actual PSS.CONSUMER_SETTING.CONSUMER_SETTING_VALUE%TYPE;
    BEGIN
        IF pv_param_value IS NULL THEN
            SELECT DEFAULT_VALUE
              INTO lv_param_value_actual
              FROM PSS.CONSUMER_SETTING_PARAM
             WHERE CONSUMER_SETTING_PARAM_ID = pn_param_id;
        ELSE
            lv_param_value_actual := pv_param_value;
        END IF;
        LOOP
            UPDATE PSS.CONSUMER_SETTING
               SET CONSUMER_SETTING_VALUE = lv_param_value_actual
             WHERE CONSUMER_ID = pn_consumer_id
               AND CONSUMER_SETTING_PARAM_ID = pn_param_id;
            EXIT WHEN SQL%FOUND;
            BEGIN
                INSERT INTO PSS.CONSUMER_SETTING(CONSUMER_ID, CONSUMER_SETTING_PARAM_ID, CONSUMER_SETTING_VALUE)
                    VALUES(pn_consumer_id, pn_param_id, lv_param_value_actual);
                EXIT;
            EXCEPTION
                WHEN DUP_VAL_ON_INDEX THEN
                    NULL;
            END;
        END LOOP;
    END;
    
    FUNCTION GET_PASSCODE(
    l_consumer_id CONSUMER_PASSCODE.CONSUMER_ID%TYPE)
    RETURN CONSUMER_PASSCODE.PASSCODE%TYPE
   IS
    l_passcode CONSUMER_PASSCODE.PASSCODE%TYPE;
   BEGIN
      SELECT MAX(PASSCODE) into l_passcode
          FROM PSS.CONSUMER_PASSCODE CP
          JOIN PSS.PASSCODE_TYPE PT ON CP.PASSCODE_TYPE_ID = PT.PASSCODE_TYPE_ID AND PT.PASSCODE_TYPE_CD = 'REGISTER_PREPAID' 
         WHERE CP.CONSUMER_ID = l_consumer_id AND EXPIRATION_TS > SYSDATE;
      IF l_passcode IS NULL THEN
        l_passcode:= PSS.PKG_CONSUMER_MAINT.CREATE_CONSUMER_PASSCODE(l_consumer_id, 'REGISTER_PREPAID');
      END IF;
      RETURN l_passcode;
   EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN NULL;
   END;
   
   PROCEDURE ADD_PROMO(
        pv_promo_code REPORT.CAMPAIGN.PROMO_CODE%TYPE,
        pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE)
   IS
    ln_campaign_id PSS.CONSUMER.CONSUMER_ID%TYPE;
    ln_campaign_type_id REPORT.CAMPAIGN.campaign_type_id%TYPE;
    ln_campaign_ids NUMBER_TABLE:=NUMBER_TABLE();
    l_cam_count NUMBER;
   BEGIN

        select campaign_id bulk collect into ln_campaign_ids 
        from report.campaign 
        where upper(promo_code)=upper(pv_promo_code) and status='A' and (end_date is NULL or end_date > Sysdate-1);
        IF ln_campaign_ids.count = 0 THEN
          RAISE_APPLICATION_ERROR(-20310, 'Did not find the promotion code');
        END IF; 
        INSERT INTO PSS.CAMPAIGN_CONSUMER_ACCT(CAMPAIGN_ID, CONSUMER_ACCT_ID,TRAN_COUNT, PROMO_TRAN_COUNT) 
          SELECT column_value, ca.consumer_acct_id,0,0 from table(ln_campaign_ids)
          join pss.consumer_acct ca on consumer_id=pn_consumer_id and consumer_acct_type_id=5
          join REPORT.campaign on campaign_id=column_value and (campaign_type_id in (4,5) or (campaign_type_id=1 and is_for_all_cards='Y'))
          where not exists (select 1 from PSS.CAMPAIGN_CONSUMER_ACCT where campaign_id=column_value and CONSUMER_ACCT_ID=ca.consumer_acct_id)
          union
          SELECT column_value, ca.consumer_acct_id,0,0 from table(ln_campaign_ids)
          join pss.consumer_acct ca on consumer_id=pn_consumer_id and consumer_acct_type_id=3
          join REPORT.campaign on campaign_id=column_value and (campaign_type_id in (4,5) or (campaign_type_id=1 and is_for_all_cards='N')) 
          where not exists (select 1 from PSS.CAMPAIGN_CONSUMER_ACCT where campaign_id=column_value and CONSUMER_ACCT_ID=ca.consumer_acct_id)
          union
          SELECT column_value, ca.consumer_acct_id,0,0 from table(ln_campaign_ids)
          join pss.consumer_acct ca on consumer_id=pn_consumer_id and consumer_acct_type_id=7
          join REPORT.campaign on campaign_id=column_value and (campaign_type_id in (4,5) or (campaign_type_id=1 and is_for_all_cards='P')) 
          where not exists (select 1 from PSS.CAMPAIGN_CONSUMER_ACCT where campaign_id=column_value and CONSUMER_ACCT_ID=ca.consumer_acct_id);
        INSERT INTO PSS.CONSUMER_PROMOCODE(CAMPAIGN_ID, CONSUMER_ID)
          SELECT column_value, pn_consumer_id from table(ln_campaign_ids)
          WHERE NOT EXISTS (SELECT 1 FROM PSS.CONSUMER_PROMOCODE WHERE CAMPAIGN_ID = column_value and CONSUMER_ID = pn_consumer_id);
   END;

PROCEDURE CREATE_NEXT_CONSUMER_PASS_ID(
        pv_device_serail_cd REPORT.EPORT.EPORT_SERIAL_NUM%TYPE,
        pv_pass_type_id CORP.CUSTOMER.PASS_TYPE_ID%TYPE,
        pv_consumer_pass_id OUT PSS.CONSUMER_PASS.CONSUMER_PASS_ID%TYPE,
        pn_pass_template_id OUT PSS.CUSTOMER_PASS_TEMPLATE.PASS_TEMPLATE_ID%TYPE)
IS
  l_consumerPassId NUMBER;
  l_customerId NUMBER;
  l_initDeviceId NUMBER;
  l_initCustomerId NUMBER;
BEGIN
    SELECT PSS.CONSUMER_PASS_SEQ.NEXTVAL into pv_consumer_pass_id FROM DUAL;
    	 SELECT MAX(customer_id) into l_customerId from corp.customer where PASS_TYPE_ID=pv_pass_type_id;
    	 SELECT MAX(DEVICE_ID) into l_initDeviceId FROM DEVICE.DEVICE WHERE DEVICE_SERIAL_CD=pv_device_serail_cd AND DEVICE_ACTIVE_YN_FLAG='Y';
    	 SELECT MAX(T.CUSTOMER_ID) into l_initCustomerId FROM REPORT.TERMINAL_EPORT TE JOIN REPORT.EPORT E ON TE.EPORT_ID=E.EPORT_ID
		 JOIN REPORT.TERMINAL T ON TE.TERMINAL_ID=T.TERMINAL_ID
		 WHERE E.EPORT_SERIAL_NUM=pv_device_serail_cd;
		 INSERT INTO PSS.CONSUMER_PASS(CONSUMER_PASS_ID,CUSTOMER_ID, PASS_INIT_DEVICE_ID, PASS_INIT_CUSTOMER_ID)
		 values(pv_consumer_pass_id, l_customerId, l_initDeviceId,l_initCustomerId);
     select pass_template_id into pn_pass_template_id from PSS.customer_pass_template where customer_id=COALESCE (l_customerId,1) and consumer_acct_type_id=5;
END;

PROCEDURE CREATE_VAS_CONSUMER(
        pv_pass_serial_num PSS.CONSUMER_PASS.PASS_SERIAL_NUMBER%TYPE,
        pv_email PSS.CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
        pv_cell PSS.CONSUMER.CONSUMER_CELL_PHONE_NUM%TYPE,
        pn_preferred_comm_type_id PSS.CONSUMER.PREFERRED_COMM_TYPE_ID%TYPE,
        pv_first PSS.CONSUMER.CONSUMER_FNAME%TYPE,
        pv_last PSS.CONSUMER.CONSUMER_LNAME%TYPE,
        pv_postal PSS.CONSUMER.CONSUMER_POSTAL_CD%TYPE,
        pv_country PSS.CONSUMER.CONSUMER_COUNTRY_CD%TYPE
        )
  IS
    l_consumerPassId NUMBER;
    l_consumerId NUMBER;
  BEGIN
    select max(consumer_pass_id) into l_consumerPassId from pss.consumer_pass where PASS_SERIAL_NUMBER=pv_pass_serial_num and consumer_id is null;
    IF l_consumerPassId is NULL THEN
      RAISE_APPLICATION_ERROR(-20322, 'pass serial number ' || pv_pass_serial_num || ' does not exist');
    ELSE
      select pss.seq_consumer_id.nextval into l_consumerId from dual;
      INSERT INTO PSS.CONSUMER(
                    CONSUMER_ID,
                    CONSUMER_EMAIL_ADDR1,
                    CONSUMER_CELL_PHONE_NUM,
                    PREFERRED_COMM_TYPE_ID,
                    CONSUMER_FNAME,
                    CONSUMER_LNAME,
                    CONSUMER_POSTAL_CD,
                    CONSUMER_COUNTRY_CD,
                    CONSUMER_TYPE_ID)
                VALUES(
                    l_consumerId,
                    pv_email,
                    pv_cell,
                    pn_preferred_comm_type_id,
                    pv_first,
                    pv_last,
                    pv_postal,
                    pv_country,
                    5);
      update pss.consumer_pass set consumer_id=l_consumerId where consumer_pass_id=l_consumerPassId;
    END IF;
  END;
  
PROCEDURE CREATE_VAS_CREDENTIAL(
      pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
      pb_credential_hash PSS.CONSUMER_CREDENTIAL.CREDENTIAL_HASH%TYPE,
      pb_credential_salt PSS.CONSUMER_CREDENTIAL.CREDENTIAL_SALT%TYPE,
      pv_credential_alg PSS.CONSUMER_CREDENTIAL.CREDENTIAL_ALG%TYPE,
      pv_addr1 PSS.CONSUMER.CONSUMER_ADDR1%TYPE,
      pd_birthday PSS.CONSUMER.BIRTHDAY%TYPE)
IS
BEGIN
  INSERT INTO PSS.CONSUMER_CREDENTIAL(CONSUMER_ID, CREDENTIAL_HASH, CREDENTIAL_SALT, CREDENTIAL_ALG, RESOURCE_KEY, CREDENTIAL_ACTIVE_FLAG, ACTIVATED_TS)
  VALUES(pn_consumer_id, pb_credential_hash, pb_credential_salt, pv_credential_alg, 'PREPAID_WEBSITE','Y', sysdate);
END;

PROCEDURE ASSIGN_ACCT_BY_PROMO(
      pn_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE,
      pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE)
IS
      ln_campaign_ids NUMBER_TABLE:=NUMBER_TABLE();
BEGIN
      SELECT CP.CAMPAIGN_ID bulk collect into ln_campaign_ids 
      FROM PSS.CONSUMER_PROMOCODE CP 
      JOIN REPORT.CAMPAIGN CAM ON CAM.CAMPAIGN_ID = CP.CAMPAIGN_ID
      JOIN PSS.CONSUMER_ACCT CA ON CA.CONSUMER_ID = CP.CONSUMER_ID 
      WHERE CP.CONSUMER_ID = pn_consumer_id
            AND CA.CONSUMER_ACCT_ID = pn_consumer_acct_id
            AND CAM.STATUS='A'
            AND (CAM.END_DATE is NULL or CAM.END_DATE > Sysdate-1)
            AND (CAM.CAMPAIGN_TYPE_ID in (4,5) 
                 OR (CAM.IS_FOR_ALL_CARDS = 'Y' and CA.CONSUMER_ACCT_TYPE_ID = 5) 
                 OR (CAM.IS_FOR_ALL_CARDS = 'N' and CA.CONSUMER_ACCT_TYPE_ID = 3) 
                 OR (CAM.IS_FOR_ALL_CARDS = 'P' and CA.CONSUMER_ACCT_TYPE_ID = 7));
        
      INSERT INTO PSS.CAMPAIGN_CONSUMER_ACCT(CAMPAIGN_ID, CONSUMER_ACCT_ID,TRAN_COUNT, PROMO_TRAN_COUNT) 
          SELECT column_value, pn_consumer_acct_id,0,0 from table(ln_campaign_ids)
          where not exists (select 1 from PSS.CAMPAIGN_CONSUMER_ACCT where campaign_id=column_value and CONSUMER_ACCT_ID = pn_consumer_acct_id);
END;

END;
/
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/PSS/PKG_POS_PTA.pbk?revision=1.46
CREATE OR REPLACE PACKAGE BODY PSS.PKG_POS_PTA IS

/*
	Payment Template import mode codes
 	S = Safe Mode
		- Create new pos_pta records based on the given template where no active
		  pos_pta already exists per payment_action_type_cd/payment_entry_method_cd
	MS = Merge Safe Mode
		- Create new pos_pta records based on the given template where no active
		  pos_pta already exists per payment_subtype_id
	MO = Merge Overwrite Mode
		- Deactivate any existing pos_pta records where a new pos_pta record is defined
		  in the template per payment_subtype_id
		- Create all new pos_pta records based on the given template
	O = Overwrite Mode
		- Deactivate all existing pos_pta records where a new pos_pta record is
		  being imported for the payment_action_type_cd/payment_entry_method_cd
		- Create all new pos_pta records based on the given template
	CO = Complete Overwrite
		- Deactivate all existing pos_pta records
		- Create all new pos_pta records based on the given template
*/
IMPORT_MODE__SAFE                            CONSTANT VARCHAR2(1)            := 'S';
IMPORT_MODE__MERGE_SAFE                      CONSTANT VARCHAR2(2)            := 'MS';
IMPORT_MODE__MERGE_OVERWRITE                 CONSTANT VARCHAR2(2)            := 'MO';
IMPORT_MODE__OVERWRITE                       CONSTANT VARCHAR2(1)            := 'O';
IMPORT_MODE__CMPLT_OVERWRITE              	 CONSTANT VARCHAR2(2)            := 'CO';

ORDER_MODE__ABOVE_ERROR_BIN                 CONSTANT VARCHAR2(2)            := 'AE';
ORDER_MODE__TOP		                        CONSTANT VARCHAR2(1)            := 'T';
ORDER_MODE__BOTTOM                 			CONSTANT VARCHAR2(1)            := 'B';

PROCEDURE SP_GET_DEVICE_DATA
(
    pn_device_id                            IN  device.device_id%TYPE,
    pn_pos_id                               OUT pss.pos.pos_id%TYPE,
    pn_result_cd                            OUT NUMBER,
    pv_error_message                        OUT VARCHAR2
)
IS
BEGIN
    pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;

    SELECT p.pos_id
    INTO pn_pos_id
    FROM pss.pos p, device.device d
    WHERE p.device_id = d.device_id
      AND d.device_id = pn_device_id;

    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;

PROCEDURE SP_DISABLE_POS_PTA
(
    pn_pos_pta_id       IN       pss.pos_pta.pos_pta_id%TYPE,
    pn_result_cd        OUT      NUMBER,
    pv_error_message    OUT      VARCHAR2
)
IS
    ln_sys_date                  DATE                           := SYSDATE;
    l_pos_id pss.pos_pta.pos_id%TYPE;
BEGIN
    pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;

    /* update activation timestamp to 1 second ago to prevent active and inactive records from
    having exactly the same activation timestamp */
    UPDATE pss.pos_pta
    SET pos_pta_activation_ts = NVL(pos_pta_activation_ts, ln_sys_date - 1/86400),
        pos_pta_deactivation_ts = ln_sys_date
    WHERE pos_pta_id = pn_pos_pta_id
  returning pos_id into l_pos_id;

    UPDATE_DEVICE_CASHLESS_ENABLED(l_pos_id);
    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;

PROCEDURE FIND_ALT_TERMINAL(
    pn_authority_id PSS.MERCHANT.AUTHORITY_ID%TYPE,
    pv_authority_name VARCHAR2,
    pv_merchant_group_cd PSS.MERCHANT.MERCHANT_GROUP_CD%TYPE,
    pv_terminal_cd PSS.TERMINAL.TERMINAL_CD%TYPE,
    pv_device_name PSS.MERCHANT_POS_PTA_NUM.DEVICE_NAME%TYPE,
    pn_alternate_merchant_id OUT PSS.MERCHANT.MERCHANT_ID%TYPE,
    pn_alternate_terminal_id OUT PSS.TERMINAL.TERMINAL_CD%TYPE)
IS
    lc_existing_flag CHAR(1);
    ln_pos_pta_num PSS.MERCHANT_POS_PTA_NUM.POS_PTA_NUM%TYPE;
    ln_min_pos_pta_num PSS.MERCHANT_POS_PTA_NUM.POS_PTA_NUM%TYPE;
    ln_max_pos_pta_num PSS.MERCHANT_POS_PTA_NUM.POS_PTA_NUM%TYPE;
BEGIN
    SELECT MERCHANT_ID, TERMINAL_ID, EXISTING_FLAG, MIN_POS_PTA_NUM, MAX_POS_PTA_NUM
      INTO pn_alternate_merchant_id, pn_alternate_terminal_id, lc_existing_flag, ln_min_pos_pta_num, ln_max_pos_pta_num
      FROM (
    SELECT M.MERCHANT_ID, T.TERMINAL_ID, COALESCE(MAX(CASE WHEN MPP.DEVICE_NAME = pv_device_name THEN 'Y' ELSE 'N' END), 'N') EXISTING_FLAG,
           A.MAX_POS_PTA_NUM - A.MIN_POS_PTA_NUM + 1 - COUNT(DISTINCT MPP.DEVICE_NAME) REMAINING_POS_PTA_NUMS,
           A.MIN_POS_PTA_NUM, A.MAX_POS_PTA_NUM
      FROM PSS.TERMINAL T
      JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
      JOIN AUTHORITY.AUTHORITY A ON M.AUTHORITY_ID = A.AUTHORITY_ID
      LEFT OUTER JOIN PSS.MERCHANT_POS_PTA_NUM MPP ON M.MERCHANT_ID = MPP.MERCHANT_ID AND MPP.POS_PTA_NUM BETWEEN A.MIN_POS_PTA_NUM AND A.MAX_POS_PTA_NUM
     WHERE A.AUTHORITY_ID = pn_authority_id
       AND M.MERCHANT_GROUP_CD = pv_merchant_group_cd
       AND T.TERMINAL_CD = pv_terminal_cd
     GROUP BY M.MERCHANT_ID, T.TERMINAL_ID, A.MAX_POS_PTA_NUM, A.MIN_POS_PTA_NUM
     HAVING (MAX(CASE WHEN MPP.DEVICE_NAME = pv_device_name THEN 'Y' ELSE 'N' END) = 'Y'
         OR MAX_POS_PTA_NUM - MIN_POS_PTA_NUM + 1 - COUNT(DISTINCT MPP.DEVICE_NAME) > 0)
     ORDER BY 3 DESC, 4)
     WHERE ROWNUM = 1;
    IF lc_existing_flag != 'Y' THEN
        ln_pos_pta_num := CREATE_POS_PTA_NUM(pn_alternate_merchant_id, ln_min_pos_pta_num, ln_max_pos_pta_num, pv_device_name);
    END IF;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RAISE_APPLICATION_ERROR(-20152, 'Could not find a Merchant with an available slot for Group Code ''' || pv_merchant_group_cd || ''' and authority ''' || pv_authority_name || ''' for device ' || pv_device_name);
END;

FUNCTION CREATE_POS_PTA_NUM(
    pn_merchant_id PSS.MERCHANT.MERCHANT_ID%TYPE,
    pn_min_pos_pta_num PSS.MERCHANT_POS_PTA_NUM.POS_PTA_NUM%TYPE,
    pn_max_pos_pta_num PSS.MERCHANT_POS_PTA_NUM.POS_PTA_NUM%TYPE,
    pv_device_name DEVICE.DEVICE_NAME%TYPE)
    RETURN PSS.MERCHANT_POS_PTA_NUM.POS_PTA_NUM%TYPE
IS
    ln_pos_pta_num PSS.MERCHANT_POS_PTA_NUM.POS_PTA_NUM%TYPE;
BEGIN
    LOOP
        BEGIN
            SELECT MIN(A.N)
              INTO ln_pos_pta_num
              FROM PSS.MERCHANT_POS_PTA_NUM MPP
              RIGHT OUTER JOIN (
                    SELECT LEVEL - 1 + pn_min_pos_pta_num N
                      FROM DUAL
                      CONNECT BY LEVEL <= 1 + pn_max_pos_pta_num - pn_min_pos_pta_num) A
                ON MPP.POS_PTA_NUM = A.N AND MERCHANT_ID = pn_merchant_id
             WHERE MPP.POS_PTA_NUM IS NULL;
            IF ln_pos_pta_num IS NULL OR ln_pos_pta_num > pn_max_pos_pta_num THEN
                RAISE_APPLICATION_ERROR(-20151, 'Reached maximum number of devices (' || (1 + pn_max_pos_pta_num - pn_min_pos_pta_num) || ') for merchant ' || pn_merchant_id);
            END IF;
                INSERT INTO PSS.MERCHANT_POS_PTA_NUM(MERCHANT_ID, POS_PTA_NUM, DEVICE_NAME)
                    VALUES(pn_merchant_id, ln_pos_pta_num, pv_device_name);
        EXCEPTION
            WHEN DUP_VAL_ON_INDEX THEN
                SELECT MAX(POS_PTA_NUM)
                  INTO ln_pos_pta_num
                  FROM PSS.MERCHANT_POS_PTA_NUM
                 WHERE DEVICE_NAME = pv_device_name
                   AND MERCHANT_ID = pn_merchant_id;
                IF ln_pos_pta_num IS NULL THEN
                  CONTINUE;-- try again
                END IF;
        END;
        RETURN ln_pos_pta_num;
    END LOOP;
END;

PROCEDURE CLEANUP_POS_PTA_NUM(
    pv_device_name DEVICE.DEVICE_NAME%TYPE)
IS
BEGIN
    DELETE
      FROM PSS.MERCHANT_POS_PTA_NUM MPP
     WHERE MPP.DEVICE_NAME = pv_device_name
       AND NOT EXISTS(SELECT 1
        FROM PSS.MERCHANT M
        JOIN AUTHORITY.AUTHORITY A ON A.AUTHORITY_ID = M.AUTHORITY_ID
        JOIN AUTHORITY.AUTHORITY_TYPE AUT ON A.AUTHORITY_TYPE_ID = AUT.AUTHORITY_TYPE_ID
        JOIN AUTHORITY.HANDLER H ON H.HANDLER_ID = AUT.HANDLER_ID
        JOIN PSS.PAYMENT_SUBTYPE PS ON PS.PAYMENT_SUBTYPE_CLASS = H.HANDLER_CLASS
		CROSS JOIN DEVICE.DEVICE_LAST_ACTIVE DLA
        JOIN DEVICE.DEVICE D ON DLA.DEVICE_ID = D.DEVICE_ID
        JOIN PSS.POS P ON D.DEVICE_ID = P.DEVICE_ID
        JOIN PSS.POS_PTA PP ON PP.POS_ID = P.POS_ID AND PS.PAYMENT_SUBTYPE_ID = PP.PAYMENT_SUBTYPE_ID
        JOIN PSS.TERMINAL T ON PP.PAYMENT_SUBTYPE_KEY_ID = T.TERMINAL_ID AND M.MERCHANT_ID = T.MERCHANT_ID
       WHERE MPP.MERCHANT_ID = M.MERCHANT_ID
	     AND MPP.DEVICE_NAME = DLA.DEVICE_NAME
		 AND D.LAST_ACTIVITY_TS > SYSDATE - 365
         AND PS.PAYMENT_SUBTYPE_CLASS NOT IN('Authority::NOP', 'Cash', 'Aramark', 'BlackBoard')
         AND PS.PAYMENT_SUBTYPE_CLASS NOT LIKE 'Internal%'
         AND A.POS_PTA_CD_EXPRESSION IS NOT NULL
         AND (PP.POS_PTA_DEACTIVATION_TS IS NULL OR PP.POS_PTA_DEACTIVATION_TS > SYSDATE));
END;

PROCEDURE INSERT_POS_PTA(
    pn_pos_pta_id OUT PSS.POS_PTA.POS_PTA_ID%TYPE,
    pn_pos_id PSS.POS_PTA.POS_ID%TYPE,
    pn_payment_subtype_id PSS.POS_PTA.PAYMENT_SUBTYPE_ID%TYPE,
    pv_pos_pta_encrypt_key PSS.POS_PTA.POS_PTA_ENCRYPT_KEY%TYPE,
    pv_pos_pta_encrypt_key2 PSS.POS_PTA.POS_PTA_ENCRYPT_KEY2%TYPE,
    pn_payment_subtype_key_id PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE,
    pv_pos_pta_device_serial_cd PSS.POS_PTA.POS_PTA_DEVICE_SERIAL_CD%TYPE,
    pv_pos_pta_pin_req_yn_flag PSS.POS_PTA.POS_PTA_PIN_REQ_YN_FLAG%TYPE,
    pv_pos_pta_regex PSS.POS_PTA.POS_PTA_REGEX%TYPE,
    pv_pos_pta_regex_bref PSS.POS_PTA.POS_PTA_REGEX_BREF%TYPE,
    pn_authority_payment_mask_id PSS.POS_PTA.AUTHORITY_PAYMENT_MASK_ID%TYPE,
    pv_passthru_allow_yn_flag PSS.POS_PTA.POS_PTA_PASSTHRU_ALLOW_YN_FLAG%TYPE,
    pv_disable_debit_denial PSS.POS_PTA.POS_PTA_DISABLE_DEBIT_DENIAL%TYPE,
    pn_pos_pta_pref_auth_amt PSS.POS_PTA.POS_PTA_PREF_AUTH_AMT%TYPE,
    pn_pos_pta_pref_auth_amt_max PSS.POS_PTA.POS_PTA_PREF_AUTH_AMT_MAX%TYPE,
    pv_currency_cd PSS.POS_PTA.CURRENCY_CD%TYPE,
    pd_pos_pta_activation_ts PSS.POS_PTA.POS_PTA_ACTIVATION_TS%TYPE,
    pv_no_convenience_fee PSS.POS_PTA.NO_CONVENIENCE_FEE%TYPE,
    pd_decline_until PSS.POS_PTA.DECLINE_UNTIL%TYPE DEFAULT NULL,
    pc_whitelist PSS.POS_PTA.WHITELIST%TYPE DEFAULT NULL)
IS
    CURSOR l_cur IS
        SELECT DISTINCT T.TERMINAL_ID, T.TERMINAL_CD, M.MERCHANT_ID, M.MERCHANT_GROUP_CD, A.AUTHORITY_ID, A.AUTHORITY_NAME, A.MIN_POS_PTA_NUM, A.MAX_POS_PTA_NUM, (
           SELECT COUNT(DISTINCT MPP.POS_PTA_NUM)
             FROM PSS.MERCHANT_POS_PTA_NUM MPP
            WHERE M.MERCHANT_ID = MPP.MERCHANT_ID
              AND MPP.POS_PTA_NUM BETWEEN A.MIN_POS_PTA_NUM AND A.MAX_POS_PTA_NUM) USED_POS_PTA_NUMS, D.DEVICE_NAME
          FROM PSS.POS P
          JOIN DEVICE.DEVICE D ON P.DEVICE_ID = D.DEVICE_ID
          CROSS JOIN PSS.PAYMENT_SUBTYPE PS
          JOIN AUTHORITY.HANDLER H ON PS.PAYMENT_SUBTYPE_CLASS = H.HANDLER_CLASS
          JOIN AUTHORITY.AUTHORITY_TYPE AUT ON H.HANDLER_ID = AUT.HANDLER_ID
          JOIN AUTHORITY.AUTHORITY A ON A.AUTHORITY_TYPE_ID = AUT.AUTHORITY_TYPE_ID
          JOIN PSS.MERCHANT M ON M.AUTHORITY_ID = A.AUTHORITY_ID
          JOIN PSS.TERMINAL T ON T.MERCHANT_ID = M.MERCHANT_ID
          LEFT OUTER JOIN PSS.MERCHANT_POS_PTA_NUM MPP ON M.MERCHANT_ID = MPP.MERCHANT_ID AND MPP.DEVICE_NAME = D.DEVICE_NAME AND MPP.POS_PTA_NUM BETWEEN A.MIN_POS_PTA_NUM AND A.MAX_POS_PTA_NUM
         WHERE P.POS_ID = pn_pos_id
           AND PS.PAYMENT_SUBTYPE_ID = pn_payment_subtype_id
           AND T.TERMINAL_ID = pn_payment_subtype_key_id
           AND PS.PAYMENT_SUBTYPE_CLASS NOT IN('Authority::NOP', 'Cash', 'Aramark', 'BlackBoard')
           AND PS.PAYMENT_SUBTYPE_CLASS NOT LIKE 'Internal%'
           AND MPP.POS_PTA_NUM IS NULL
           AND A.POS_PTA_CD_EXPRESSION IS NOT NULL;
    ln_pos_pta_num PSS.MERCHANT_POS_PTA_NUM.POS_PTA_NUM%TYPE;
    ln_merchant_id PSS.MERCHANT.MERCHANT_ID%TYPE;
    ln_alternate_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE;
    ln_max_priority PSS.POS_PTA.POS_PTA_PRIORITY%TYPE;
BEGIN
    IF pn_payment_subtype_key_id IS NOT NULL THEN
         FOR l_rec IN l_cur LOOP
            BEGIN
                IF l_rec.MAX_POS_PTA_NUM - l_rec.MIN_POS_PTA_NUM < l_rec.USED_POS_PTA_NUMS THEN
                    FIND_ALT_TERMINAL(l_rec.AUTHORITY_ID, l_rec.AUTHORITY_NAME, l_rec.MERCHANT_GROUP_CD, l_rec.TERMINAL_CD, l_rec.DEVICE_NAME, ln_merchant_id, ln_alternate_terminal_id);
                ELSE
                    ln_merchant_id := l_rec.MERCHANT_ID;
                    ln_pos_pta_num := CREATE_POS_PTA_NUM(ln_merchant_id, l_rec.MIN_POS_PTA_NUM, l_rec.MAX_POS_PTA_NUM, l_rec.DEVICE_NAME);
                END IF;
            EXCEPTION
                WHEN MERCHANT_FULL THEN
                    FIND_ALT_TERMINAL(l_rec.AUTHORITY_ID, l_rec.AUTHORITY_NAME, l_rec.MERCHANT_GROUP_CD, l_rec.TERMINAL_CD, l_rec.DEVICE_NAME, ln_merchant_id, ln_alternate_terminal_id);
            END;
        END LOOP;
    END IF;
    SELECT NVL(MAX(PP.POS_PTA_PRIORITY), 0)
      INTO ln_max_priority
      FROM PSS.POS_PTA PP
      JOIN PSS.PAYMENT_SUBTYPE PS ON PS.PAYMENT_SUBTYPE_ID = PP.PAYMENT_SUBTYPE_ID
      JOIN PSS.CLIENT_PAYMENT_TYPE CPT ON PS.CLIENT_PAYMENT_TYPE_CD = CPT.CLIENT_PAYMENT_TYPE_CD
      JOIN PSS.CLIENT_PAYMENT_TYPE CPTX ON CPT.PAYMENT_ENTRY_METHOD_CD = CPTX.PAYMENT_ENTRY_METHOD_CD
      JOIN PSS.PAYMENT_SUBTYPE PSX ON PSX.CLIENT_PAYMENT_TYPE_CD = CPTX.CLIENT_PAYMENT_TYPE_CD
      WHERE PP.POS_ID = pn_pos_id
      AND PSX.PAYMENT_SUBTYPE_ID = pn_payment_subtype_id
      AND (PP.POS_PTA_DEACTIVATION_TS IS NULL OR PP.POS_PTA_DEACTIVATION_TS > SYSDATE);
    SELECT PSS.SEQ_POS_PTA_ID.NEXTVAL
      INTO pn_pos_pta_id
      FROM DUAL;
    INSERT INTO PSS.POS_PTA(
        POS_PTA_ID,
        POS_ID,
        PAYMENT_SUBTYPE_ID,
        POS_PTA_ENCRYPT_KEY,
        POS_PTA_ENCRYPT_KEY2,
        PAYMENT_SUBTYPE_KEY_ID,
        POS_PTA_DEVICE_SERIAL_CD,
        POS_PTA_PIN_REQ_YN_FLAG,
        POS_PTA_REGEX,
        POS_PTA_REGEX_BREF,
        AUTHORITY_PAYMENT_MASK_ID,
        POS_PTA_PASSTHRU_ALLOW_YN_FLAG,
        POS_PTA_DISABLE_DEBIT_DENIAL,
        POS_PTA_PREF_AUTH_AMT,
        POS_PTA_PREF_AUTH_AMT_MAX,
        CURRENCY_CD,
        POS_PTA_PRIORITY,
        POS_PTA_ACTIVATION_TS,
        NO_CONVENIENCE_FEE,
        DECLINE_UNTIL,
        WHITELIST)
    VALUES(
        pn_pos_pta_id,
        pn_pos_id,
        pn_payment_subtype_id,
        pv_pos_pta_encrypt_key,
        pv_pos_pta_encrypt_key2,
        NVL(ln_alternate_terminal_id, pn_payment_subtype_key_id),
        pv_pos_pta_device_serial_cd,
        pv_pos_pta_pin_req_yn_flag,
        pv_pos_pta_regex,
        pv_pos_pta_regex_bref,
        pn_authority_payment_mask_id,
        pv_passthru_allow_yn_flag,
        pv_disable_debit_denial,
        pn_pos_pta_pref_auth_amt,
        pn_pos_pta_pref_auth_amt_max,
        pv_currency_cd,
        ln_max_priority + 1,
        pd_pos_pta_activation_ts,
        pv_no_convenience_fee,
        pd_decline_until,
        pc_whitelist);
    UPDATE_DEVICE_CASHLESS_ENABLED(pn_pos_id);
END;

PROCEDURE UPDATE_POS_PTA(
    pn_pos_pta_id PSS.POS_PTA.POS_PTA_ID%TYPE,
    pv_pos_pta_encrypt_key PSS.POS_PTA.POS_PTA_ENCRYPT_KEY%TYPE,
    pv_pos_pta_encrypt_key2 PSS.POS_PTA.POS_PTA_ENCRYPT_KEY2%TYPE,
    pn_payment_subtype_key_id PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE,
    pv_pos_pta_device_serial_cd PSS.POS_PTA.POS_PTA_DEVICE_SERIAL_CD%TYPE,
    pv_pos_pta_pin_req_yn_flag PSS.POS_PTA.POS_PTA_PIN_REQ_YN_FLAG%TYPE,
    pv_pos_pta_regex PSS.POS_PTA.POS_PTA_REGEX%TYPE,
    pv_pos_pta_regex_bref PSS.POS_PTA.POS_PTA_REGEX_BREF%TYPE,
    pn_authority_payment_mask_id PSS.POS_PTA.AUTHORITY_PAYMENT_MASK_ID%TYPE,
    pv_passthru_allow_yn_flag PSS.POS_PTA.POS_PTA_PASSTHRU_ALLOW_YN_FLAG%TYPE,
    pv_disable_debit_denial PSS.POS_PTA.POS_PTA_DISABLE_DEBIT_DENIAL%TYPE,
    pn_pos_pta_pref_auth_amt PSS.POS_PTA.POS_PTA_PREF_AUTH_AMT%TYPE,
    pn_pos_pta_pref_auth_amt_max PSS.POS_PTA.POS_PTA_PREF_AUTH_AMT_MAX%TYPE,
    pv_currency_cd PSS.POS_PTA.CURRENCY_CD%TYPE,
    pd_pos_pta_activation_ts PSS.POS_PTA.POS_PTA_ACTIVATION_TS%TYPE,
    pd_pos_pta_deactivation_ts PSS.POS_PTA.POS_PTA_DEACTIVATION_TS%TYPE,
    pv_no_convenience_fee PSS.POS_PTA.NO_CONVENIENCE_FEE%TYPE,
    pd_decline_until PSS.POS_PTA.DECLINE_UNTIL%TYPE DEFAULT NULL,
    pc_whitelist PSS.POS_PTA.WHITELIST%TYPE DEFAULT NULL)
IS
    CURSOR l_cur IS
       SELECT DISTINCT T.TERMINAL_ID, T.TERMINAL_CD, M.MERCHANT_ID, M.MERCHANT_GROUP_CD, A.AUTHORITY_ID, A.AUTHORITY_NAME, A.MIN_POS_PTA_NUM, A.MAX_POS_PTA_NUM, (
           SELECT COUNT(DISTINCT MPP.POS_PTA_NUM)
             FROM PSS.MERCHANT_POS_PTA_NUM MPP
            WHERE M.MERCHANT_ID = MPP.MERCHANT_ID
              AND MPP.POS_PTA_NUM BETWEEN A.MIN_POS_PTA_NUM AND A.MAX_POS_PTA_NUM) USED_POS_PTA_NUMS, D.DEVICE_NAME
          FROM PSS.POS_PTA PP
          JOIN PSS.POS P ON PP.POS_ID = P.POS_ID
          JOIN DEVICE.DEVICE D ON P.DEVICE_ID = D.DEVICE_ID
          JOIN PSS.PAYMENT_SUBTYPE PS ON PP.PAYMENT_SUBTYPE_ID = PS.PAYMENT_SUBTYPE_ID
          JOIN AUTHORITY.HANDLER H ON PS.PAYMENT_SUBTYPE_CLASS = H.HANDLER_CLASS
          JOIN AUTHORITY.AUTHORITY_TYPE AUT ON H.HANDLER_ID = AUT.HANDLER_ID
          JOIN AUTHORITY.AUTHORITY A ON A.AUTHORITY_TYPE_ID = AUT.AUTHORITY_TYPE_ID
          JOIN PSS.MERCHANT M ON M.AUTHORITY_ID = A.AUTHORITY_ID
          JOIN PSS.TERMINAL T ON T.MERCHANT_ID = M.MERCHANT_ID
          LEFT OUTER JOIN PSS.MERCHANT_POS_PTA_NUM MPP ON M.MERCHANT_ID = MPP.MERCHANT_ID AND MPP.DEVICE_NAME = D.DEVICE_NAME AND MPP.POS_PTA_NUM BETWEEN A.MIN_POS_PTA_NUM AND A.MAX_POS_PTA_NUM
         WHERE PP.POS_PTA_ID = pn_pos_pta_id
           AND T.TERMINAL_ID = pn_payment_subtype_key_id
           AND PS.PAYMENT_SUBTYPE_CLASS NOT IN('Authority::NOP', 'Cash', 'Aramark', 'BlackBoard')
           AND PS.PAYMENT_SUBTYPE_CLASS NOT LIKE 'Internal%'
           AND MPP.POS_PTA_NUM IS NULL
           AND A.POS_PTA_CD_EXPRESSION IS NOT NULL;
    ln_pos_pta_num PSS.MERCHANT_POS_PTA_NUM.POS_PTA_NUM%TYPE;
    ln_merchant_id PSS.MERCHANT.MERCHANT_ID%TYPE;
    ln_alternate_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE;
    ln_pos_id PSS.POS.POS_ID%TYPE;
    lv_device_name DEVICE.DEVICE_NAME%TYPE;
BEGIN
    IF pn_payment_subtype_key_id IS NOT NULL THEN
         FOR l_rec IN l_cur LOOP
            BEGIN
                IF l_rec.MAX_POS_PTA_NUM - l_rec.MIN_POS_PTA_NUM < l_rec.USED_POS_PTA_NUMS THEN
                    FIND_ALT_TERMINAL(l_rec.AUTHORITY_ID, l_rec.AUTHORITY_NAME, l_rec.MERCHANT_GROUP_CD, l_rec.TERMINAL_CD, l_rec.DEVICE_NAME, ln_merchant_id, ln_alternate_terminal_id);
                ELSE
                    ln_merchant_id := l_rec.MERCHANT_ID;
                    ln_pos_pta_num := CREATE_POS_PTA_NUM(ln_merchant_id, l_rec.MIN_POS_PTA_NUM, l_rec.MAX_POS_PTA_NUM, l_rec.DEVICE_NAME);
                END IF;
            EXCEPTION
                WHEN MERCHANT_FULL THEN
                    FIND_ALT_TERMINAL(l_rec.AUTHORITY_ID, l_rec.AUTHORITY_NAME, l_rec.MERCHANT_GROUP_CD, l_rec.TERMINAL_CD, l_rec.DEVICE_NAME, ln_merchant_id, ln_alternate_terminal_id);
            END;
        END LOOP;
    END IF;
    UPDATE PSS.POS_PTA
       SET (
            POS_PTA_ENCRYPT_KEY,
            POS_PTA_ENCRYPT_KEY2,
            PAYMENT_SUBTYPE_KEY_ID,
            POS_PTA_DEVICE_SERIAL_CD,
            POS_PTA_PIN_REQ_YN_FLAG,
            POS_PTA_REGEX,
            POS_PTA_REGEX_BREF,
            AUTHORITY_PAYMENT_MASK_ID,
            POS_PTA_PASSTHRU_ALLOW_YN_FLAG,
            POS_PTA_DISABLE_DEBIT_DENIAL,
            POS_PTA_PREF_AUTH_AMT,
            POS_PTA_PREF_AUTH_AMT_MAX,
            CURRENCY_CD,
            POS_PTA_ACTIVATION_TS,
            POS_PTA_DEACTIVATION_TS,
            NO_CONVENIENCE_FEE,
            DECLINE_UNTIL,
            WHITELIST) =
    (SELECT pv_pos_pta_encrypt_key,
            pv_pos_pta_encrypt_key2,
            COALESCE(ln_alternate_terminal_id, pn_payment_subtype_key_id),
            pv_pos_pta_device_serial_cd,
            pv_pos_pta_pin_req_yn_flag,
            pv_pos_pta_regex,
            pv_pos_pta_regex_bref,
            pn_authority_payment_mask_id,
            pv_passthru_allow_yn_flag,
            pv_disable_debit_denial,
            pn_pos_pta_pref_auth_amt,
            pn_pos_pta_pref_auth_amt_max,
            pv_currency_cd,
            CASE WHEN pd_pos_pta_activation_ts IS NULL THEN POS_PTA_ACTIVATION_TS WHEN pd_pos_pta_activation_ts = MIN_DATE THEN NULL ELSE pd_pos_pta_activation_ts END,
            CASE WHEN pd_pos_pta_deactivation_ts IS NULL THEN POS_PTA_DEACTIVATION_TS WHEN pd_pos_pta_deactivation_ts = MAX_DATE THEN NULL ELSE pd_pos_pta_deactivation_ts END,
            pv_no_convenience_fee,
            pd_decline_until,
            pc_whitelist
       FROM DUAL)
      WHERE POS_PTA_ID = pn_pos_pta_id
      RETURNING POS_ID
       INTO ln_pos_id;
    SELECT D.DEVICE_NAME
      INTO lv_device_name
      FROM PSS.POS P
      JOIN DEVICE.DEVICE D ON P.DEVICE_ID = D.DEVICE_ID
     WHERE P.POS_ID = ln_pos_id;
     
    CLEANUP_POS_PTA_NUM(lv_device_name);
    UPDATE_DEVICE_CASHLESS_ENABLED(ln_pos_id);
END;

PROCEDURE COPY_POS_PTAS(
    pn_target_pos_id PSS.POS.POS_ID%TYPE,
    pn_source_pos_id PSS.POS.POS_ID%TYPE)
IS
    CURSOR l_cur IS
       SELECT DISTINCT T.TERMINAL_ID, T.TERMINAL_CD, M.MERCHANT_ID, M.MERCHANT_GROUP_CD, A.AUTHORITY_ID, A.AUTHORITY_NAME, A.MIN_POS_PTA_NUM, A.MAX_POS_PTA_NUM, (
           SELECT COUNT(DISTINCT MPP.POS_PTA_NUM)
             FROM PSS.MERCHANT_POS_PTA_NUM MPP
            WHERE M.MERCHANT_ID = MPP.MERCHANT_ID
              AND MPP.POS_PTA_NUM BETWEEN A.MIN_POS_PTA_NUM AND A.MAX_POS_PTA_NUM) USED_POS_PTA_NUMS, D.DEVICE_NAME
          FROM PSS.POS P
          JOIN DEVICE.DEVICE D ON P.DEVICE_ID = D.DEVICE_ID
          CROSS JOIN PSS.POS_PTA PP
          JOIN PSS.PAYMENT_SUBTYPE PS ON PP.PAYMENT_SUBTYPE_ID = PS.PAYMENT_SUBTYPE_ID
          JOIN AUTHORITY.HANDLER H ON PS.PAYMENT_SUBTYPE_CLASS = H.HANDLER_CLASS
          JOIN AUTHORITY.AUTHORITY_TYPE AUT ON H.HANDLER_ID = AUT.HANDLER_ID
          JOIN AUTHORITY.AUTHORITY A ON A.AUTHORITY_TYPE_ID = AUT.AUTHORITY_TYPE_ID
          JOIN PSS.MERCHANT M ON M.AUTHORITY_ID = A.AUTHORITY_ID
          JOIN PSS.TERMINAL T ON T.MERCHANT_ID = M.MERCHANT_ID
          LEFT OUTER JOIN PSS.MERCHANT_POS_PTA_NUM MPP ON M.MERCHANT_ID = MPP.MERCHANT_ID AND MPP.DEVICE_NAME = D.DEVICE_NAME AND MPP.POS_PTA_NUM BETWEEN A.MIN_POS_PTA_NUM AND A.MAX_POS_PTA_NUM
         WHERE P.POS_ID = pn_target_pos_id
           AND PP.POS_ID = pn_source_pos_id
           AND T.TERMINAL_ID = PP.PAYMENT_SUBTYPE_KEY_ID
           AND PS.PAYMENT_SUBTYPE_CLASS NOT IN('Authority::NOP', 'Cash', 'Aramark', 'BlackBoard','Promotion')
           AND PS.PAYMENT_SUBTYPE_CLASS NOT LIKE 'Internal%'
           AND MPP.POS_PTA_NUM IS NULL
           AND A.POS_PTA_CD_EXPRESSION IS NOT NULL;
    ln_pos_pta_num PSS.MERCHANT_POS_PTA_NUM.POS_PTA_NUM%TYPE;
    ln_merchant_id PSS.MERCHANT.MERCHANT_ID%TYPE;
    ln_alternate_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE;
    lt_replace_terminal_ids NUMBER_TABLE := NUMBER_TABLE();
    lt_alternate_terminal_ids NUMBER_TABLE := NUMBER_TABLE();
BEGIN
     FOR l_rec IN l_cur LOOP
        BEGIN
            IF l_rec.MAX_POS_PTA_NUM - l_rec.MIN_POS_PTA_NUM < l_rec.USED_POS_PTA_NUMS THEN
                FIND_ALT_TERMINAL(l_rec.AUTHORITY_ID, l_rec.AUTHORITY_NAME, l_rec.MERCHANT_GROUP_CD, l_rec.TERMINAL_CD, l_rec.DEVICE_NAME, ln_merchant_id, ln_alternate_terminal_id);
            ELSE
                ln_merchant_id := l_rec.MERCHANT_ID;
                ln_pos_pta_num := CREATE_POS_PTA_NUM(ln_merchant_id, l_rec.MIN_POS_PTA_NUM, l_rec.MAX_POS_PTA_NUM, l_rec.DEVICE_NAME);
            END IF;
        EXCEPTION
            WHEN MERCHANT_FULL THEN
                FIND_ALT_TERMINAL(l_rec.AUTHORITY_ID, l_rec.AUTHORITY_NAME, l_rec.MERCHANT_GROUP_CD, l_rec.TERMINAL_CD, l_rec.DEVICE_NAME, ln_merchant_id, ln_alternate_terminal_id);
        END;
        IF ln_alternate_terminal_id IS NOT NULL THEN
            lt_alternate_terminal_ids.EXTEND;
            lt_alternate_terminal_ids(lt_alternate_terminal_ids.LAST) := ln_alternate_terminal_id;
            lt_replace_terminal_ids.EXTEND;
            lt_replace_terminal_ids(lt_replace_terminal_ids.LAST) := l_rec.TERMINAL_ID;
        END IF;
    END LOOP;
    INSERT INTO PSS.POS_PTA(
            POS_ID,
            PAYMENT_SUBTYPE_ID,
            POS_PTA_ENCRYPT_KEY,
            POS_PTA_ENCRYPT_KEY2,
            PAYMENT_SUBTYPE_KEY_ID,
            POS_PTA_DEVICE_SERIAL_CD,
            POS_PTA_PIN_REQ_YN_FLAG,
            POS_PTA_REGEX,
            POS_PTA_REGEX_BREF,
            AUTHORITY_PAYMENT_MASK_ID,
            POS_PTA_PASSTHRU_ALLOW_YN_FLAG,
            POS_PTA_DISABLE_DEBIT_DENIAL,
            POS_PTA_PREF_AUTH_AMT,
            POS_PTA_PREF_AUTH_AMT_MAX,
            CURRENCY_CD,
            POS_PTA_PRIORITY,
            POS_PTA_ACTIVATION_TS,
            POS_PTA_DEACTIVATION_TS,
            NO_CONVENIENCE_FEE,
            TERMINAL_ID,
            MERCHANT_BANK_ACCT_ID,
            DECLINE_UNTIL,
            WHITELIST)
     SELECT pn_target_pos_id,
            PAYMENT_SUBTYPE_ID,
            POS_PTA_ENCRYPT_KEY,
            POS_PTA_ENCRYPT_KEY2,
            COALESCE(ALT.ALTERNATE_TERMINAL_ID, PP.PAYMENT_SUBTYPE_KEY_ID),
            POS_PTA_DEVICE_SERIAL_CD,
            POS_PTA_PIN_REQ_YN_FLAG,
            POS_PTA_REGEX,
            POS_PTA_REGEX_BREF,
            AUTHORITY_PAYMENT_MASK_ID,
            POS_PTA_PASSTHRU_ALLOW_YN_FLAG,
            POS_PTA_DISABLE_DEBIT_DENIAL,
            POS_PTA_PREF_AUTH_AMT,
            POS_PTA_PREF_AUTH_AMT_MAX,
            CURRENCY_CD,
            POS_PTA_PRIORITY,
            CASE WHEN POS_PTA_ACTIVATION_TS < SYSDATE THEN SYSDATE ELSE POS_PTA_ACTIVATION_TS END,
            POS_PTA_DEACTIVATION_TS,
            NO_CONVENIENCE_FEE,
            TERMINAL_ID,
            MERCHANT_BANK_ACCT_ID,
            DECLINE_UNTIL,
            WHITELIST
       FROM PSS.POS_PTA PP
       LEFT OUTER JOIN (SELECT A.REPLACE_TERMINAL_ID, B.ALTERNATE_TERMINAL_ID
            FROM (
            SELECT ROWNUM N, COLUMN_VALUE REPLACE_TERMINAL_ID FROM TABLE(lt_replace_terminal_ids)) A
            JOIN (
            SELECT ROWNUM N, COLUMN_VALUE ALTERNATE_TERMINAL_ID FROM TABLE(lt_alternate_terminal_ids)) B ON A.N = B.N
            ) ALT ON PP.PAYMENT_SUBTYPE_KEY_ID = ALT.REPLACE_TERMINAL_ID
      WHERE POS_ID = pn_source_pos_id
        AND (POS_PTA_ACTIVATION_TS IS NULL OR POS_PTA_DEACTIVATION_TS IS NULL OR POS_PTA_DEACTIVATION_TS > SYSDATE);

    UPDATE_DEVICE_CASHLESS_ENABLED(pn_target_pos_id);
END;

PROCEDURE SP_IMPORT_POS_PTA_TEMPLATE
(
    pn_device_id        IN       device.device_id%TYPE,
    pn_pos_pta_tmpl_id  IN       pss.pos_pta_tmpl.pos_pta_tmpl_id%TYPE,
    pv_mode_cd          IN       VARCHAR2,
    pv_order_cd         IN       VARCHAR2,
    pv_set_terminal_cd_to_serial IN VARCHAR2,
    pn_result_cd        OUT      NUMBER,
    pv_error_message    OUT      VARCHAR2,
    pv_only_no_two_tier_pricing IN VARCHAR2 DEFAULT 'N')
IS
    ln_device_id 				 DEVICE.DEVICE_ID%TYPE;
    lv_device_serial_cd			 DEVICE.DEVICE_SERIAL_CD%TYPE;
    lv_device_name DEVICE.DEVICE_NAME%TYPE;
    ln_pos_id                    pss.pos.pos_id%TYPE;
    ln_sys_date                  DATE                           := SYSDATE;
    lt_replace_terminal_ids NUMBER_TABLE := NUMBER_TABLE();
    lt_alternate_terminal_ids NUMBER_TABLE := NUMBER_TABLE();
    l_fee_currency_id NUMBER;
BEGIN
    pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;

    IF pv_mode_cd NOT IN (
		IMPORT_MODE__SAFE,
        IMPORT_MODE__MERGE_SAFE,
        IMPORT_MODE__MERGE_OVERWRITE,
        IMPORT_MODE__OVERWRITE,
		IMPORT_MODE__CMPLT_OVERWRITE
		) THEN
        pv_error_message := 'Unrecognized import mode_cd: ' || pv_mode_cd;
        RETURN;
    END IF;

	IF pv_order_cd NOT IN (ORDER_MODE__ABOVE_ERROR_BIN,
		ORDER_MODE__TOP,
		ORDER_MODE__BOTTOM) THEN
		pv_error_message := 'Unrecognized import order_cd: ' || pv_order_cd;
        RETURN;
	END IF;

	SELECT MAX(dla.device_id), MAX(dla.device_serial_cd), MAX(d.device_name)
	INTO ln_device_id, lv_device_serial_cd, lv_device_name
	FROM device.device d
	JOIN device.device_last_active dla ON d.device_name = dla.device_name
	WHERE d.device_id = pn_device_id;

    SP_GET_DEVICE_DATA(ln_device_id, ln_pos_id, pn_result_cd, pv_error_message);

	IF pv_only_no_two_tier_pricing = 'Y' THEN
		UPDATE PSS.POS_PTA PP
		SET NO_CONVENIENCE_FEE = (
			SELECT MAX(NO_CONVENIENCE_FEE)
			FROM PSS.POS_PTA_TMPL_ENTRY
			WHERE POS_PTA_TMPL_ID = pn_pos_pta_tmpl_id
				AND PAYMENT_SUBTYPE_ID = PP.PAYMENT_SUBTYPE_ID
		)
		WHERE pos_id = ln_pos_id
            AND (pos_pta_deactivation_ts IS NULL OR ln_sys_date < pos_pta_deactivation_ts)
			AND EXISTS (
				SELECT 1
				FROM PSS.POS_PTA_TMPL_ENTRY
				WHERE POS_PTA_TMPL_ID = pn_pos_pta_tmpl_id
					AND PAYMENT_SUBTYPE_ID = PP.PAYMENT_SUBTYPE_ID
			);

		pn_result_cd := PKG_CONST.RESULT__SUCCESS;
		pv_error_message := PKG_CONST.ERROR__NO_ERROR;
		RETURN;
	END IF;

    --Create ln_pos_pta_num on all merchant ids that need it
    DECLARE
        CURSOR l_cur IS
        SELECT DISTINCT T.TERMINAL_ID, T.TERMINAL_CD, M.MERCHANT_ID, M.MERCHANT_GROUP_CD, A.AUTHORITY_ID, A.AUTHORITY_NAME, A.MIN_POS_PTA_NUM, A.MAX_POS_PTA_NUM, (
		       SELECT COUNT(DISTINCT MPP.POS_PTA_NUM)
		         FROM PSS.MERCHANT_POS_PTA_NUM MPP
		        WHERE M.MERCHANT_ID = MPP.MERCHANT_ID
		          AND MPP.POS_PTA_NUM BETWEEN A.MIN_POS_PTA_NUM AND A.MAX_POS_PTA_NUM) USED_POS_PTA_NUMS
          FROM PSS.POS_PTA_TMPL_ENTRY PPTE
          JOIN PSS.PAYMENT_SUBTYPE PS ON PS.PAYMENT_SUBTYPE_ID = PPTE.PAYMENT_SUBTYPE_ID
          JOIN PSS.CLIENT_PAYMENT_TYPE CPT ON PS.CLIENT_PAYMENT_TYPE_CD = CPT.CLIENT_PAYMENT_TYPE_CD
          JOIN PSS.TERMINAL T ON PPTE.PAYMENT_SUBTYPE_KEY_ID = T.TERMINAL_ID
          JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
          JOIN AUTHORITY.AUTHORITY A ON M.AUTHORITY_ID = A.AUTHORITY_ID
          JOIN AUTHORITY.AUTHORITY_TYPE AUT ON A.AUTHORITY_TYPE_ID = AUT.AUTHORITY_TYPE_ID
          JOIN AUTHORITY.HANDLER H ON H.HANDLER_ID = AUT.HANDLER_ID AND PS.PAYMENT_SUBTYPE_CLASS = H.HANDLER_CLASS
          LEFT OUTER JOIN PSS.MERCHANT_POS_PTA_NUM MPP ON M.MERCHANT_ID = MPP.MERCHANT_ID AND MPP.DEVICE_NAME = lv_device_name AND MPP.POS_PTA_NUM BETWEEN A.MIN_POS_PTA_NUM AND A.MAX_POS_PTA_NUM
         WHERE PPTE.POS_PTA_TMPL_ID = pn_pos_pta_tmpl_id
           AND PS.PAYMENT_SUBTYPE_CLASS NOT IN('Authority::NOP', 'Cash', 'Aramark', 'BlackBoard')
           AND PS.PAYMENT_SUBTYPE_CLASS NOT LIKE 'Internal%'
           AND MPP.POS_PTA_NUM IS NULL
           AND A.POS_PTA_CD_EXPRESSION IS NOT NULL
           AND (
                -- create all the template entries
                pv_mode_cd IN (IMPORT_MODE__CMPLT_OVERWRITE, IMPORT_MODE__OVERWRITE, IMPORT_MODE__MERGE_OVERWRITE)
                -- create the template entries unless the payment_subtype_id already exists
                OR pv_mode_cd = IMPORT_MODE__MERGE_SAFE AND PS.PAYMENT_SUBTYPE_ID NOT IN
                (
                    SELECT PAYMENT_SUBTYPE_ID
                    FROM PSS.POS_PTA
                    WHERE POS_ID = ln_pos_id
                        AND (POS_PTA_ACTIVATION_TS <= ln_sys_date OR POS_PTA_ACTIVATION_TS IS NULL)
                        AND (POS_PTA_DEACTIVATION_TS IS NULL OR ln_sys_date < POS_PTA_DEACTIVATION_TS)
                )
                -- create the template entries unless the payment_action_type_cd/payment_entry_method_cd already exists
                OR pv_mode_cd = IMPORT_MODE__SAFE AND CPT.PAYMENT_ACTION_TYPE_CD NOT IN
                (
                    SELECT S_CPT.PAYMENT_ACTION_TYPE_CD
                    FROM PSS.POS_PTA S_PP
            JOIN PSS.PAYMENT_SUBTYPE S_PS ON S_PP.PAYMENT_SUBTYPE_ID = S_PS.PAYMENT_SUBTYPE_ID
            JOIN PSS.CLIENT_PAYMENT_TYPE S_CPT ON S_PS.CLIENT_PAYMENT_TYPE_CD = S_CPT.CLIENT_PAYMENT_TYPE_CD
                    WHERE S_PP.POS_ID = ln_pos_id
                        AND (POS_PTA_ACTIVATION_TS <= ln_sys_date OR POS_PTA_ACTIVATION_TS IS NULL)
                        AND (POS_PTA_DEACTIVATION_TS IS NULL OR ln_sys_date < POS_PTA_DEACTIVATION_TS)
                )
          AND CPT.PAYMENT_ENTRY_METHOD_CD NOT IN
                (
                    SELECT S_CPT.PAYMENT_ENTRY_METHOD_CD
                    FROM PSS.POS_PTA S_PP
            JOIN PSS.PAYMENT_SUBTYPE S_PS ON S_PP.PAYMENT_SUBTYPE_ID = S_PS.PAYMENT_SUBTYPE_ID
            JOIN PSS.CLIENT_PAYMENT_TYPE S_CPT ON S_PS.CLIENT_PAYMENT_TYPE_CD = S_CPT.CLIENT_PAYMENT_TYPE_CD
                    WHERE S_PP.POS_ID = ln_pos_id
                        AND (POS_PTA_ACTIVATION_TS <= ln_sys_date OR POS_PTA_ACTIVATION_TS IS NULL)
                        AND (POS_PTA_DEACTIVATION_TS IS NULL OR ln_sys_date < POS_PTA_DEACTIVATION_TS)
                )
            );
		    ln_pos_pta_num PSS.MERCHANT_POS_PTA_NUM.POS_PTA_NUM%TYPE;
        ln_merchant_id PSS.MERCHANT.MERCHANT_ID%TYPE;
        ln_alternate_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE;
    BEGIN
        FOR l_rec IN l_cur LOOP
            BEGIN
                IF l_rec.MAX_POS_PTA_NUM - l_rec.MIN_POS_PTA_NUM < l_rec.USED_POS_PTA_NUMS THEN
                    FIND_ALT_TERMINAL(l_rec.AUTHORITY_ID, l_rec.AUTHORITY_NAME, l_rec.MERCHANT_GROUP_CD, l_rec.TERMINAL_CD, lv_device_name, ln_merchant_id, ln_alternate_terminal_id);
                ELSE
                    ln_merchant_id := l_rec.MERCHANT_ID;
                    ln_pos_pta_num := CREATE_POS_PTA_NUM(ln_merchant_id, l_rec.MIN_POS_PTA_NUM, l_rec.MAX_POS_PTA_NUM, lv_device_name);
                END IF;
            EXCEPTION
                WHEN MERCHANT_FULL THEN
                    FIND_ALT_TERMINAL(l_rec.AUTHORITY_ID, l_rec.AUTHORITY_NAME, l_rec.MERCHANT_GROUP_CD, l_rec.TERMINAL_CD, lv_device_name, ln_merchant_id, ln_alternate_terminal_id);
            END;
            IF ln_alternate_terminal_id IS NOT NULL THEN
                lt_alternate_terminal_ids.EXTEND;
                lt_alternate_terminal_ids(lt_alternate_terminal_ids.LAST) := ln_alternate_terminal_id;
                lt_replace_terminal_ids.EXTEND;
                lt_replace_terminal_ids(lt_replace_terminal_ids.LAST) := l_rec.TERMINAL_ID;
            END IF;
        END LOOP;
    END;
    IF pv_mode_cd = IMPORT_MODE__CMPLT_OVERWRITE THEN
		-- deactivate any active or inactive pos_pta
        UPDATE pss.pos_pta
        SET pos_pta_activation_ts = NVL(pos_pta_activation_ts, ln_sys_date - 1/86400),
            pos_pta_deactivation_ts = ln_sys_date
		WHERE pos_id = ln_pos_id
			AND (pos_pta_activation_ts <= ln_sys_date OR pos_pta_activation_ts IS NULL)
            AND (pos_pta_deactivation_ts IS NULL OR ln_sys_date < pos_pta_deactivation_ts);
	ELSIF pv_mode_cd = IMPORT_MODE__OVERWRITE THEN
        -- deactivate any active or inactive pos_pta that have a payment_action_type_cd/payment_entry_method_cd in our template
        UPDATE pss.pos_pta
        SET pos_pta_activation_ts = NVL(pos_pta_activation_ts, ln_sys_date - 1/86400),
            pos_pta_deactivation_ts = ln_sys_date
    	WHERE pos_pta_id IN
    	(
           	SELECT pp.pos_pta_id
            FROM pss.pos_pta pp
			JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
			JOIN pss.client_payment_type cpt ON ps.client_payment_type_cd = cpt.client_payment_type_cd
			JOIN pss.pos_pta_tmpl_entry tmpl_ppte ON tmpl_ppte.pos_pta_tmpl_id = pn_pos_pta_tmpl_id
			JOIN pss.payment_subtype tmpl_ps ON tmpl_ppte.payment_subtype_id = tmpl_ps.payment_subtype_id
			JOIN pss.client_payment_type tmpl_cpt ON tmpl_ps.client_payment_type_cd = tmpl_cpt.client_payment_type_cd
            WHERE pp.pos_id = ln_pos_id
                AND (pp.pos_pta_activation_ts <= ln_sys_date OR pp.pos_pta_activation_ts IS NULL)
                AND (pp.pos_pta_deactivation_ts IS NULL OR ln_sys_date < pp.pos_pta_deactivation_ts)
				AND cpt.payment_action_type_cd = tmpl_cpt.payment_action_type_cd
				AND cpt.payment_entry_method_cd = tmpl_cpt.payment_entry_method_cd
        );
    ELSIF pv_mode_cd = IMPORT_MODE__MERGE_OVERWRITE THEN
        -- deactivate any active active or inactive pos_pta that have a payment_subtype_id in our template
        UPDATE pss.pos_pta
        SET pos_pta_activation_ts = NVL(pos_pta_activation_ts, ln_sys_date - 1/86400),
            pos_pta_deactivation_ts = ln_sys_date
    	WHERE pos_pta_id IN
    	(
           	SELECT pp.pos_pta_id
            FROM pss.pos_pta pp
			JOIN pss.pos_pta_tmpl_entry tmpl_ppte ON pp.payment_subtype_id = tmpl_ppte.payment_subtype_id
            WHERE pp.pos_id = ln_pos_id
                AND (pp.pos_pta_activation_ts <= ln_sys_date OR pp.pos_pta_activation_ts IS NULL)
                AND (pp.pos_pta_deactivation_ts IS NULL OR ln_sys_date < pp.pos_pta_deactivation_ts)
                AND tmpl_ppte.pos_pta_tmpl_id = pn_pos_pta_tmpl_id
        );
    END IF;

	INSERT INTO pss.pos_pta
    (
    	pos_id,
		terminal_id,
        payment_subtype_key_id,
        pos_pta_device_serial_cd,
        pos_pta_activation_ts,
        pos_pta_deactivation_ts,
        pos_pta_encrypt_key,
        pos_pta_encrypt_key2,
        authority_payment_mask_id,
        pos_pta_pin_req_yn_flag,
        pos_pta_priority,
        merchant_bank_acct_id,
        currency_cd,
        payment_subtype_id,
        pos_pta_passthru_allow_yn_flag,
		pos_pta_pref_auth_amt,
		pos_pta_pref_auth_amt_max,
		pos_pta_disable_debit_denial,
		no_convenience_fee
    )
	SELECT ln_pos_id,
		ppte.terminal_id,
		COALESCE(alt.alternate_terminal_id, ppte.payment_subtype_key_id),
		CASE WHEN pv_set_terminal_cd_to_serial = 'Y' AND ps.payment_subtype_class != 'Authority::NOP' THEN lv_device_serial_cd ELSE ppte.pos_pta_device_serial_cd END,
		CASE WHEN ppte.pos_pta_activation_oset_hr IS NULL THEN NULL
            ELSE ln_sys_date + ppte.pos_pta_activation_oset_hr / 24 END,
		CASE WHEN ppte.pos_pta_deactivation_oset_hr IS NULL THEN NULL
            ELSE ln_sys_date + ppte.pos_pta_deactivation_oset_hr / 24 END,
		ppte.pos_pta_encrypt_key,
		ppte.pos_pta_encrypt_key2,
        /* if the template specifies an authority payment mask id, and it is different than the payment subtype's
        default mask id, then set it in the pos_pta, otherwise leave it null to use the default */
        CASE WHEN ppte.authority_payment_mask_id IS NOT NULL
		  AND ppte.authority_payment_mask_id <> ps.authority_payment_mask_id
            THEN ppte.authority_payment_mask_id
        ELSE NULL END,
		ppte.pos_pta_pin_req_yn_flag,
		-- query for the max priority values per payment_entry_method_cd
		CASE
			WHEN pv_mode_cd IN (IMPORT_MODE__CMPLT_OVERWRITE, IMPORT_MODE__OVERWRITE) THEN 0
			ELSE
				CASE WHEN pv_order_cd IN (ORDER_MODE__ABOVE_ERROR_BIN, ORDER_MODE__BOTTOM) THEN
					NVL((SELECT MAX(pty_pp.pos_pta_priority)
						FROM pss.pos_pta pty_pp, pss.payment_subtype pty_ps, pss.client_payment_type pty_cpt
						WHERE pty_pp.pos_id = ln_pos_id
							AND pty_pp.payment_subtype_id = pty_ps.payment_subtype_id
							AND pty_ps.client_payment_type_cd = pty_cpt.client_payment_type_cd
							AND pty_cpt.payment_action_type_cd = cpt.payment_action_type_cd
							AND pty_cpt.payment_entry_method_cd = cpt.payment_entry_method_cd
							AND (pty_pp.pos_pta_activation_ts <= ln_sys_date OR pty_pp.pos_pta_activation_ts IS NULL)
							AND (pty_pp.pos_pta_deactivation_ts IS NULL OR ln_sys_date < pty_pp.pos_pta_deactivation_ts)
							AND pty_ps.payment_subtype_class != CASE WHEN pv_order_cd = ORDER_MODE__ABOVE_ERROR_BIN THEN 'Authority::NOP' ELSE CHR(0) END
					), 0)
					ELSE 0
				END
        END + ppte.pos_pta_priority,
		ppte.merchant_bank_acct_id,
		ppte.currency_cd,
		ps.payment_subtype_id,
		ppte.pos_pta_passthru_allow_yn_flag,
		ppte.pos_pta_pref_auth_amt,
		ppte.pos_pta_pref_auth_amt_max,
		ppte.pos_pta_disable_debit_denial,
		ppte.no_convenience_fee
    FROM pss.pos_pta_tmpl_entry ppte
	JOIN pss.payment_subtype ps ON ps.payment_subtype_id = ppte.payment_subtype_id
	JOIN pss.client_payment_type cpt ON ps.client_payment_type_cd = cpt.client_payment_type_cd
  LEFT OUTER JOIN (SELECT A.REPLACE_TERMINAL_ID, B.ALTERNATE_TERMINAL_ID
      FROM (
      SELECT ROWNUM N, COLUMN_VALUE REPLACE_TERMINAL_ID FROM TABLE(lt_replace_terminal_ids)) A
      JOIN (
      SELECT ROWNUM N, COLUMN_VALUE ALTERNATE_TERMINAL_ID FROM TABLE(lt_alternate_terminal_ids)) B ON A.N = B.N
      ) ALT ON PPTE.PAYMENT_SUBTYPE_KEY_ID = ALT.REPLACE_TERMINAL_ID
      AND PS.PAYMENT_SUBTYPE_CLASS NOT IN('Authority::NOP', 'Cash', 'Aramark', 'BlackBoard')
      AND PS.PAYMENT_SUBTYPE_CLASS NOT LIKE 'Internal%'
    WHERE ppte.pos_pta_tmpl_id = pn_pos_pta_tmpl_id
        AND
        (
            -- create all the template entries
            pv_mode_cd IN (IMPORT_MODE__CMPLT_OVERWRITE, IMPORT_MODE__OVERWRITE, IMPORT_MODE__MERGE_OVERWRITE)
            -- create the template entries unless the payment_subtype_id already exists
            OR pv_mode_cd = IMPORT_MODE__MERGE_SAFE AND ps.payment_subtype_id NOT IN
            (
                SELECT payment_subtype_id
                FROM pss.pos_pta
                WHERE pos_id = ln_pos_id
                    AND (pos_pta_activation_ts <= ln_sys_date OR pos_pta_activation_ts IS NULL)
                    AND (pos_pta_deactivation_ts IS NULL OR ln_sys_date < pos_pta_deactivation_ts)
            )
            -- create the template entries unless the payment_action_type_cd/payment_entry_method_cd already exists
            OR pv_mode_cd = IMPORT_MODE__SAFE
			AND cpt.payment_action_type_cd NOT IN
            (
                SELECT s_cpt.payment_action_type_cd
                FROM pss.pos_pta s_pp
				JOIN pss.payment_subtype s_ps ON s_pp.payment_subtype_id = s_ps.payment_subtype_id
				JOIN pss.client_payment_type s_cpt ON s_ps.client_payment_type_cd = s_cpt.client_payment_type_cd
                WHERE s_pp.pos_id = ln_pos_id
                    AND (pos_pta_activation_ts <= ln_sys_date OR pos_pta_activation_ts IS NULL)
                    AND (pos_pta_deactivation_ts IS NULL OR ln_sys_date < pos_pta_deactivation_ts)
            )
			AND cpt.payment_entry_method_cd NOT IN
            (
                SELECT s_cpt.payment_entry_method_cd
                FROM pss.pos_pta s_pp
				JOIN pss.payment_subtype s_ps ON s_pp.payment_subtype_id = s_ps.payment_subtype_id
				JOIN pss.client_payment_type s_cpt ON s_ps.client_payment_type_cd = s_cpt.client_payment_type_cd
                WHERE s_pp.pos_id = ln_pos_id
                    AND (pos_pta_activation_ts <= ln_sys_date OR pos_pta_activation_ts IS NULL)
                    AND (pos_pta_deactivation_ts IS NULL OR ln_sys_date < pos_pta_deactivation_ts)
            )
        );

	IF pv_mode_cd NOT IN (IMPORT_MODE__CMPLT_OVERWRITE, IMPORT_MODE__OVERWRITE) AND pv_order_cd IN (ORDER_MODE__ABOVE_ERROR_BIN, ORDER_MODE__TOP) THEN
		UPDATE pss.pos_pta pp
		SET pp.pos_pta_priority = pp.pos_pta_priority +
			NVL((SELECT MAX(pty_pp.pos_pta_priority)
				FROM pss.pos_pta pty_pp, pss.payment_subtype pty_ps, pss.client_payment_type pty_cpt
				WHERE pty_pp.pos_id = ln_pos_id
					AND pty_pp.payment_subtype_id = pty_ps.payment_subtype_id
					AND pty_ps.client_payment_type_cd = pty_cpt.client_payment_type_cd
					AND pty_cpt.payment_action_type_cd = (
						SELECT cpt.payment_action_type_cd
						FROM pss.pos_pta pp2
						JOIN pss.payment_subtype ps ON pp2.payment_subtype_id = ps.payment_subtype_id
						JOIN pss.client_payment_type cpt ON ps.client_payment_type_cd = cpt.client_payment_type_cd
						WHERE pp2.pos_pta_id = pp.pos_pta_id
					)
					AND pty_cpt.payment_entry_method_cd = (
						SELECT cpt.payment_entry_method_cd
						FROM pss.pos_pta pp2
						JOIN pss.payment_subtype ps ON pp2.payment_subtype_id = ps.payment_subtype_id
						JOIN pss.client_payment_type cpt ON ps.client_payment_type_cd = cpt.client_payment_type_cd
						WHERE pp2.pos_pta_id = pp.pos_pta_id
					)
					AND (pty_pp.pos_pta_activation_ts <= ln_sys_date OR pty_pp.pos_pta_activation_ts IS NULL)
					AND (pty_pp.pos_pta_deactivation_ts IS NULL OR ln_sys_date < pty_pp.pos_pta_deactivation_ts)
					AND pty_pp.created_ts >= ln_sys_date)
			, 0)
		WHERE pp.pos_pta_id IN (
			SELECT pp3.pos_pta_id
			FROM pss.pos_pta pp3
			JOIN pss.payment_subtype ps3 ON pp3.payment_subtype_id = ps3.payment_subtype_id
			WHERE pp3.pos_id = ln_pos_id
				AND pp3.created_ts < ln_sys_date
				AND (pp3.pos_pta_activation_ts <= ln_sys_date OR pp3.pos_pta_activation_ts IS NULL)
				AND (pp3.pos_pta_deactivation_ts IS NULL OR ln_sys_date < pp3.pos_pta_deactivation_ts)
				AND ps3.payment_subtype_class = CASE WHEN pv_order_cd = ORDER_MODE__ABOVE_ERROR_BIN THEN 'Authority::NOP' ELSE ps3.payment_subtype_class END
        and ps3.payment_subtype_class!='Promotion'
			);
	END IF;
  CLEANUP_POS_PTA_NUM(lv_device_name);
    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;

    UPDATE_DEVICE_CASHLESS_ENABLED(ln_pos_id);
    select max(t.fee_currency_id) into l_fee_currency_id
          from report.terminal t join report.terminal_eport te on t.terminal_id=te.terminal_id
          left outer join report.terminal_region tr on tr.terminal_id =t.terminal_id
          join report.campaign_assignment ca on (t.terminal_id=ca.terminal_id
          or (ca.customer_id=t.customer_id and ca.region_id=tr.region_id )
          or (ca.customer_id=t.customer_id and ca.region_id is null ))
          join report.campaign c on ca.campaign_id=c.campaign_id  and c.campaign_type_id=4
          join report.eport e ON te.eport_id = e.eport_id
          join device.device d on d.device_serial_cd=e.eport_serial_num and d.device_active_yn_flag='Y' and not(d.device_type_id = 14 and d.device_sub_type_id IN(1,2))
          join pss.pos p on p.device_id=d.device_id and p.pos_id=ln_pos_id
          WHERE greatest(NVL(c.START_DATE, MIN_DATE), NVL(te.START_DATE, MIN_DATE))<least(NVL(c.END_DATE, MAX_DATE),NVL(te.END_DATE, MAX_DATE));
   IF l_fee_currency_id is not null THEN
      INSERT_PROMO_POS_PTA(ln_pos_id,l_fee_currency_id);
   END IF;
END;

PROCEDURE SP_IMPORT_POS_PTA_TEMPLATE
(
    pn_device_id        IN       device.device_id%TYPE,
    pn_pos_pta_tmpl_id  IN       pss.pos_pta_tmpl.pos_pta_tmpl_id%TYPE,
    pv_mode_cd          IN       VARCHAR2,
    pn_result_cd        OUT      NUMBER,
    pv_error_message    OUT      VARCHAR2
)
IS
BEGIN
	SP_IMPORT_POS_PTA_TEMPLATE(pn_device_id, pn_pos_pta_tmpl_id, pv_mode_cd, ORDER_MODE__BOTTOM, 'N', pn_result_cd, pv_error_message, 'N');
END;

PROCEDURE SP_GET_OR_CREATE_POS
(
	pn_device_id IN device.device_id%TYPE,
	pd_effective_date IN pss.pos.pos_activation_ts%TYPE,
	pn_pos_id OUT pss.pos.pos_id%TYPE,
	pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2
)
IS
BEGIN
	pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;

	SELECT MAX(pos_id)
	INTO pn_pos_id
	FROM (
		SELECT pos_id
		FROM pss.pos
		WHERE device_id = pn_device_id
			AND pos_activation_ts <= pd_effective_date
		ORDER BY pos_activation_ts DESC, created_ts DESC
	)
	WHERE ROWNUM = 1;

	IF pn_pos_id IS NULL THEN
		SELECT MAX(pos_id)
		INTO pn_pos_id
		FROM (
			SELECT pos_id
			FROM pss.pos
			WHERE device_id = pn_device_id
			ORDER BY pos_activation_ts, created_ts
		)
		WHERE ROWNUM = 1;

		IF pn_pos_id IS NULL THEN
			SELECT pss.seq_pos_id.NEXTVAL INTO pn_pos_id FROM DUAL;

			INSERT INTO pss.pos (
			   pos_id,
			   location_id,
			   customer_id,
			   device_id,
			   pos_activation_ts
			) VALUES (
			   pn_pos_id,
			   1,
			   1,
			   pn_device_id,
			   pd_effective_date
			);
		ELSE
			UPDATE pss.pos
			SET pos_activation_ts = pd_effective_date
			WHERE pos_id = pn_pos_id;
		END IF;
	END IF;

	pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;

PROCEDURE SP_GET_POS_PTA
(
    pn_device_id IN device.device_id%TYPE,
    pv_device_name IN device.device_name%TYPE,
    pc_client_payment_type_cd IN pss.client_payment_type.client_payment_type_cd%TYPE,
    pt_utc_ts IN TIMESTAMP,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_pos_pta_id OUT pss.pos_pta.pos_pta_id%TYPE
)
IS
    ld_effective_date DATE := DBADMIN.UTC_TO_LOCAL_DATE(pt_utc_ts);
	ln_pos_id pss.pos.pos_id%TYPE;
BEGIN
    pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;

	SP_GET_OR_CREATE_POS(pn_device_id, ld_effective_date, ln_pos_id, pn_result_cd, pv_error_message);

    BEGIN
        SELECT pos_pta_id INTO pn_pos_pta_id FROM
        (
            SELECT pp.pos_pta_id
            FROM pss.pos_pta pp, pss.payment_subtype ps
            WHERE ps.payment_subtype_id = pp.payment_subtype_id
            AND pp.pos_id = ln_pos_id
            AND ps.client_payment_type_cd = pc_client_payment_type_cd
            AND pp.pos_pta_activation_ts <= ld_effective_date
            AND (pp.pos_pta_deactivation_ts IS NULL
                OR pp.pos_pta_deactivation_ts > ld_effective_date)
            ORDER BY pp.pos_pta_priority, pp.pos_pta_activation_ts DESC, pp.created_ts DESC
        )
        WHERE ROWNUM = 1;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            pn_result_cd := PKG_CONST.RESULT__PAYMENT_NOT_ACCEPTED;
            pv_error_message := 'Payment method is not accepted';
            RETURN;
    END;

    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;

PROCEDURE SP_GET_OR_CREATE_POS_PTA
(
    pn_device_id IN device.device_id%TYPE,
    pv_device_name IN device.device_name%TYPE,
    pc_client_payment_type_cd IN pss.client_payment_type.client_payment_type_cd%TYPE,
    pt_utc_ts IN TIMESTAMP,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_pos_pta_id OUT pss.pos_pta.pos_pta_id%TYPE
)
IS
l_pos_id pss.pos_pta.pos_id%TYPE;
BEGIN
    pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;

    SP_GET_POS_PTA(pn_device_id, pv_device_name, pc_client_payment_type_cd, pt_utc_ts, pn_result_cd, pv_error_message, pn_pos_pta_id);
    IF pn_result_cd = PKG_CONST.RESULT__PAYMENT_NOT_ACCEPTED THEN
		SP_GET_OR_CREATE_ERR_POS_PTA(pn_device_id, pv_device_name, pc_client_payment_type_cd, pt_utc_ts, pn_result_cd, pv_error_message, pn_pos_pta_id);
    END IF;

    select max(pos_id) into l_pos_id from pss.pos_pta where pos_pta_id=pn_pos_pta_id;
    UPDATE_DEVICE_CASHLESS_ENABLED(l_pos_id);

    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;

PROCEDURE SP_GET_OR_CREATE_ERR_POS_PTA (
    pn_device_id IN device.device_id%TYPE,
    pv_device_name IN device.device_name%TYPE,
    pc_client_payment_type_cd IN pss.client_payment_type.client_payment_type_cd%TYPE,
    pt_utc_ts IN TIMESTAMP,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_pos_pta_id OUT pss.pos_pta.pos_pta_id%TYPE
)
IS
    ln_pos_id pss.pos.pos_id%TYPE;
    ln_payment_subtype_id pss.payment_subtype.payment_subtype_id%TYPE;
    ln_payment_subtype_key_id pss.pos_pta.payment_subtype_key_id%TYPE := NULL;
    ld_effective_date DATE := DBADMIN.UTC_TO_LOCAL_DATE(pt_utc_ts);
	ln_pos_pta_priority pss.pos_pta.pos_pta_priority%TYPE := 100;
	lc_payment_action_type_cd pss.client_payment_type.payment_action_type_cd%TYPE;
	lc_payment_entry_method_cd pss.client_payment_type.payment_entry_method_cd%TYPE;
BEGIN
    pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;

	SP_GET_OR_CREATE_POS(pn_device_id, ld_effective_date, ln_pos_id, pn_result_cd, pv_error_message);

    BEGIN
        SELECT POS_PTA_ID
          INTO pn_pos_pta_id
          FROM (
            SELECT PP.POS_PTA_ID
            FROM PSS.POS_PTA PP
            JOIN PSS.PAYMENT_SUBTYPE PS ON PS.PAYMENT_SUBTYPE_ID = PP.PAYMENT_SUBTYPE_ID
            WHERE PP.POS_ID = ln_pos_id
			AND ps.client_payment_type_cd = pc_client_payment_type_cd
			AND ps.payment_subtype_class = CASE pc_client_payment_type_cd
				WHEN PKG_CONST.CLNT_PMNT_TYPE__CASH THEN ps.payment_subtype_class
				ELSE 'Authority::NOP'
			END
			AND UPPER(ps.payment_subtype_name) LIKE CASE pc_client_payment_type_cd
				WHEN PKG_CONST.CLNT_PMNT_TYPE__CASH THEN UPPER(ps.payment_subtype_name)
				ELSE '%ERROR BIN'
			END
            AND PP.POS_PTA_ACTIVATION_TS <= ld_effective_date
            AND (PP.POS_PTA_DEACTIVATION_TS IS NULL
                OR PP.POS_PTA_DEACTIVATION_TS > ld_effective_date)
            ORDER BY PP.POS_PTA_PRIORITY DESC, PP.POS_PTA_ACTIVATION_TS DESC, PP.CREATED_TS DESC
        )
        WHERE ROWNUM = 1;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            IF pc_client_payment_type_cd = PKG_CONST.CLNT_PMNT_TYPE__CASH THEN
                SELECT MIN(payment_subtype_id) INTO ln_payment_subtype_id
                FROM pss.payment_subtype
                WHERE client_payment_type_cd = PKG_CONST.CLNT_PMNT_TYPE__CASH;
            ELSE
                SELECT MIN(payment_subtype_id) INTO ln_payment_subtype_id
                FROM pss.payment_subtype
                WHERE client_payment_type_cd = pc_client_payment_type_cd
                    AND payment_subtype_class = 'Authority::NOP'
                    AND UPPER(payment_subtype_name) LIKE '%ERROR BIN';

                SELECT MIN(terminal_id) INTO ln_payment_subtype_key_id
                FROM authority.authority a
                INNER JOIN pss.merchant m ON a.authority_id = m.authority_id
                INNER JOIN pss.terminal t ON m.merchant_id = t.merchant_id
                WHERE UPPER(a.authority_name) = 'ERROR BIN';

				SELECT payment_action_type_cd, payment_entry_method_cd
				INTO lc_payment_action_type_cd, lc_payment_entry_method_cd
				FROM pss.client_payment_type
				WHERE client_payment_type_cd = pc_client_payment_type_cd;

				SELECT NVL(MAX(pos_pta_priority), 99) + 1
				INTO ln_pos_pta_priority
				FROM pss.pos_pta pp
				JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
				JOIN pss.client_payment_type cpt ON ps.client_payment_type_cd = cpt.client_payment_type_cd
				WHERE pos_id = ln_pos_id
					AND cpt.payment_action_type_cd = lc_payment_action_type_cd
					AND cpt.payment_entry_method_cd = lc_payment_entry_method_cd
					AND (pp.pos_pta_deactivation_ts IS NULL OR SYSDATE < pp.pos_pta_deactivation_ts);
            END IF;

            SELECT pss.seq_pos_pta_id.NEXTVAL INTO pn_pos_pta_id FROM DUAL;

            INSERT INTO pss.pos_pta (
                pos_pta_id,
                pos_id,
                payment_subtype_id,
                payment_subtype_key_id,
                pos_pta_activation_ts,
                pos_pta_priority
            ) VALUES (
                pn_pos_pta_id,
                ln_pos_id,
                ln_payment_subtype_id,
                ln_payment_subtype_key_id,
                TO_DATE('01/01/1970', 'MM/DD/YYYY'),
                ln_pos_pta_priority
            );
    END;

    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;

    PROCEDURE CREATE_PTA_FOR_TRAN(
    	pv_device_name DEVICE.DEVICE_NAME%TYPE,
    	pd_tran_server_ts PSS.TRAN.TRAN_START_TS%TYPE,
    	pt_utc_ts TIMESTAMP,
        pc_client_payment_type_cd pss.client_payment_type.client_payment_type_cd%TYPE,
        pn_pos_pta_id OUT PSS.POS_PTA.POS_PTA_ID%TYPE,
        pn_minor_currency_factor OUT PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
        pc_currency_cd OUT PSS.CURRENCY.CURRENCY_CD%TYPE)
    IS
        ln_device_id DEVICE.DEVICE_ID%TYPE;
        ln_result_cd NUMBER;
    	lv_error_message VARCHAR2(4000);
    BEGIN
		ln_device_id := PKG_DEVICE_CONFIGURATION.GET_DEVICE_ID_BY_NAME(pv_device_name, pd_tran_server_ts);
        PSS.PKG_POS_PTA.SP_GET_OR_CREATE_ERR_POS_PTA(ln_device_id, pv_device_name, pc_client_payment_type_cd, pt_utc_ts, ln_result_cd, lv_error_message, pn_pos_pta_id);
		IF ln_result_cd != PKG_CONST.RESULT__SUCCESS THEN
			RAISE_APPLICATION_ERROR(-20501, lv_error_message);
		END IF;
		SELECT c.MINOR_CURRENCY_FACTOR, c.CURRENCY_CD
		  INTO pn_minor_currency_factor, pc_currency_cd
		  FROM PSS.POS_PTA pta
		  JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
         WHERE pta.POS_PTA_ID = pn_pos_pta_id;
	END;

  PROCEDURE INSERT_PROMO_POS_PTA(
    pn_pos_id PSS.POS_PTA.POS_ID%TYPE,
    pn_currency_id NUMBER DEFAULT 1)
    IS

    BEGIN
      INSERT INTO PSS.POS_PTA(
        POS_PTA_ID,
        POS_ID,
        PAYMENT_SUBTYPE_ID,
        PAYMENT_SUBTYPE_KEY_ID,
        POS_PTA_PIN_REQ_YN_FLAG,
        POS_PTA_PASSTHRU_ALLOW_YN_FLAG,
        POS_PTA_DISABLE_DEBIT_DENIAL,
        CURRENCY_CD,
        POS_PTA_PRIORITY,
        POS_PTA_ACTIVATION_TS,
        NO_CONVENIENCE_FEE)
SELECT PSS.SEQ_POS_PTA_ID.NEXTVAL,
       POS_ID,
       PAYMENT_SUBTYPE_ID,
       PAYMENT_SUBTYPE_KEY_ID,
       'N' POS_PTA_PIN_REQ_YN_FLAG,
       'Y' POS_PTA_PASSTHRU_ALLOW_YN_FLAG,
       'N' POS_PTA_DISABLE_DEBIT_DENIAL,
       CURRENCY_CD,
       MIN_POS_PTA_PRIORITY - RANK() OVER (PARTITION BY PAYMENT_ENTRY_METHOD_CD ORDER BY LENGTH(AUTHORITY_PAYMENT_MASK_REGEX), AUTHORITY_PAYMENT_MASK_ID) POS_PTA_PRIORITY,
       SYSDATE POS_PTA_ACTIVATION_TS,
       'N' NO_CONVENIENCE_FEE
  FROM (
SELECT PP.POS_ID,
       PS_F.PAYMENT_SUBTYPE_ID,
       T.TERMINAL_ID PAYMENT_SUBTYPE_KEY_ID,
       C.CURRENCY_CODE CURRENCY_CD,
       CPT_F.PAYMENT_ENTRY_METHOD_CD,
       PS_F.AUTHORITY_PAYMENT_MASK_ID,
       APM.AUTHORITY_PAYMENT_MASK_REGEX,
       MIN(PP.POS_PTA_PRIORITY) MIN_POS_PTA_PRIORITY
  FROM PSS.POS_PTA PP
  JOIN PSS.PAYMENT_SUBTYPE PS ON PP.PAYMENT_SUBTYPE_ID = PS.PAYMENT_SUBTYPE_ID
  JOIN PSS.CLIENT_PAYMENT_TYPE CPT ON PS.CLIENT_PAYMENT_TYPE_CD = CPT.CLIENT_PAYMENT_TYPE_CD
  JOIN PSS.CLIENT_PAYMENT_TYPE CPT_F ON CPT.PAYMENT_ENTRY_METHOD_CD = CPT_F.PAYMENT_ENTRY_METHOD_CD
  JOIN PSS.PAYMENT_SUBTYPE PS_F ON PS_F.CLIENT_PAYMENT_TYPE_CD = CPT_F.CLIENT_PAYMENT_TYPE_CD
  LEFT JOIN PSS.AUTHORITY_PAYMENT_MASK APM ON PS_F.AUTHORITY_PAYMENT_MASK_ID = APM.AUTHORITY_PAYMENT_MASK_ID
CROSS JOIN CORP.CURRENCY C
CROSS JOIN PSS.TERMINAL T
WHERE PP.POS_ID = pn_pos_id
   AND CPT.PAYMENT_ENTRY_METHOD_CD IN ('S','C','N','E','Q')
   AND PP.POS_PTA_ACTIVATION_TS IS NOT NULL
   AND COALESCE(PP.POS_PTA_DEACTIVATION_TS,MAX_DATE) > SYSDATE
   AND C.CURRENCY_ID = pn_currency_id
   AND T.TERMINAL_DESC = 'Promotion Terminal'
   AND PS_F.PAYMENT_SUBTYPE_CLASS = 'Promotion'
   AND PS_F.STATUS_CD = 'A'
   AND NOT EXISTS(
        SELECT 1
          FROM PSS.POS_PTA PP0
          JOIN PSS.PAYMENT_SUBTYPE PS0 ON PP0.PAYMENT_SUBTYPE_ID = PS0.PAYMENT_SUBTYPE_ID
         WHERE PP0.POS_ID = pn_pos_id
           AND PP0.PAYMENT_SUBTYPE_ID = PS_F.PAYMENT_SUBTYPE_ID
           AND PP0.POS_PTA_ACTIVATION_TS IS NOT NULL
           AND COALESCE(PP0.POS_PTA_DEACTIVATION_TS,MAX_DATE) > SYSDATE
           AND PP0.CURRENCY_CD = C.CURRENCY_CODE)
GROUP BY PP.POS_ID,
       PS_F.PAYMENT_SUBTYPE_ID,
       T.TERMINAL_ID,
       C.CURRENCY_CODE,
       CPT_F.PAYMENT_ENTRY_METHOD_CD,
       PS_F.AUTHORITY_PAYMENT_MASK_ID,
       APM.AUTHORITY_PAYMENT_MASK_REGEX);

    END;

PROCEDURE DISABLE_PROMO_POS_PTA(
    pn_pos_id PSS.POS_PTA.POS_ID%TYPE,
    pv_check VARCHAR DEFAULT 'Y')
IS
  l_sys_date                  DATE                           := SYSDATE;
  l_count NUMBER:=0;
BEGIN
        IF pv_check ='Y' THEN
          select count(1) into l_count
          from report.terminal t join report.terminal_eport te on t.terminal_id=te.terminal_id
          left outer join report.terminal_region tr on tr.terminal_id =t.terminal_id
          join report.campaign_assignment ca on (t.terminal_id=ca.terminal_id
          or (ca.customer_id=t.customer_id and ca.region_id=tr.region_id )
          or (ca.customer_id=t.customer_id and ca.region_id is null ))
          join report.campaign c on ca.campaign_id=c.campaign_id  and c.campaign_type_id=4
          join report.eport e ON te.eport_id = e.eport_id
          join device.device d on d.device_serial_cd=e.eport_serial_num and d.device_active_yn_flag='Y' and not(d.device_type_id = 14 and d.device_sub_type_id IN(1,2))
          join pss.pos p on p.device_id=d.device_id and p.pos_id=pn_pos_id;
        END IF;
        IF l_count =0 THEN
          update pss.pos_pta
          SET pos_pta_activation_ts = NVL(pos_pta_activation_ts, l_sys_date - 1/86400),
                pos_pta_deactivation_ts = l_sys_date
          where pos_pta_id in (select pos_pta_id from pss.pos_pta pp join pss.PAYMENT_SUBTYPE ps on pp.payment_subtype_id=ps.payment_subtype_id
              where pp.pos_id=pn_pos_id and ps.trans_type_id in (select trans_type_id from report.trans_type where free_ind='Y')
              and COALESCE(pp.pos_pta_deactivation_ts,MAX_DATE)>sysdate);
       END IF;
END;

PROCEDURE DECLINE_UNTIL (
    pn_pos_id PSS.POS_PTA.POS_ID%TYPE,
    pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
    pd_decline_until TIMESTAMP,
    pd_override_whitelist CHAR DEFAULT 'N')
IS
BEGIN
  update pss.pos_pta
  set decline_until = pd_decline_until,
  whitelist = case when pd_override_whitelist = 'Y' and pd_decline_until is not null then null else whitelist end -- clear whitelist if override is Y and we're blacklisting it
  where pos_pta_id in (
    select pp.pos_pta_id
    from pss.pos_pta pp
    join pss.payment_subtype ps on ps.payment_subtype_id = pp.payment_subtype_id
    where pp.pos_id = pn_pos_id
    and ps.payment_subtype_class = pv_payment_subtype_class
    and pp.pos_pta_activation_ts < sysdate
    and (pp.pos_pta_deactivation_ts is null or pp.pos_pta_deactivation_ts > sysdate)
    and ps.payment_subtype_class not in ('Authority::NOP')
    and (
      (pd_decline_until is not null and nvl(pp.whitelist,'N') = 'N') -- we're blacklisting it and it's not whitelisted
      or (pd_override_whitelist = 'Y'))); -- or we're overriding the whitelist
END;

PROCEDURE UPDATE_WHITELIST (
    pn_pos_id PSS.POS_PTA.POS_ID%TYPE,
    pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
    pc_whitelist PSS.POS_PTA.WHITELIST%TYPE)
IS
BEGIN
  update pss.pos_pta
  set whitelist = pc_whitelist,
  decline_until = case when nvl(pc_whitelist,'N') = 'Y' then null else decline_until end
  where pos_pta_id in (
    select pp.pos_pta_id
    from pss.pos_pta pp
    join pss.payment_subtype ps on ps.payment_subtype_id = pp.payment_subtype_id
    where pp.pos_id = pn_pos_id
    and ps.payment_subtype_class = pv_payment_subtype_class
    and pp.pos_pta_activation_ts < sysdate
    and (pp.pos_pta_deactivation_ts is null or pp.pos_pta_deactivation_ts > sysdate)
    and ps.payment_subtype_class not in ('Authority::NOP'));
END;

END;
/
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/PSS/PKG_CONSUMER_MAINT.pbk?revision=1.21
CREATE OR REPLACE PACKAGE BODY PSS.PKG_CONSUMER_MAINT IS
/* The following rules were applied to this procedure

1.       The only unique identifier we have is the student?s email address.
         We determine if a student already exists in our database by comparing the
         email address provided in the data file with the records in our database.

2.       If a student?s email address changes then a new student record will be
         created.  Transactions from the original student record will not be
         associated with the new student record.

3.       All fields, except the password and email address, will be updated when
         new information is sent about an existing student.   The only exception
         to this rule is:

        "When a student?s id number changes, and the password was never changed
        by the user, the password will be set to the last four digits of the new
        student id."

4.      Student accounts in the database will be deactivated if they are not
        present when loading the data file.

5.      When we receive data about a student that has been deactivated we will
        reactivate the account.  A new account will not be created and any
        historical data will be re-associated with the student.

*/
    cv_package_name   CONSTANT VARCHAR2 (30) := 'pkg_consumer_maint';

   PROCEDURE CONSUMER_NOTIF_UPD(
    l_consumer_id CONSUMER_NOTIF.CONSUMER_ID%TYPE,
    l_host_status_notif_type_id CONSUMER_NOTIF.HOST_STATUS_NOTIF_TYPE_ID%TYPE,
    l_notify_on CONSUMER_NOTIF.NOTIFY_ON%TYPE,
    l_notify_email_addr CONSUMER.CONSUMER_EMAIL_ADDR2%TYPE)
   IS
   BEGIN
    UPDATE CONSUMER_NOTIF
       SET NOTIFY_ON = l_notify_on
     WHERE CONSUMER_ID = l_consumer_id
       AND HOST_STATUS_NOTIF_TYPE_ID = l_host_status_notif_type_id;
     IF SQL%ROWCOUNT = 0 THEN
        INSERT INTO CONSUMER_NOTIF(CONSUMER_ID, HOST_STATUS_NOTIF_TYPE_ID, NOTIFY_ON)
            VALUES(l_consumer_id, l_host_status_notif_type_id, l_notify_on);
     END IF;
     UPDATE CONSUMER
        SET CONSUMER_EMAIL_ADDR2 = l_notify_email_addr
      WHERE CONSUMER_ID = l_consumer_id;
   END;

    PROCEDURE sp_log_upload (
        pn_location_id                   IN       consumer_upload_log.location_id%TYPE,
        pn_app_user_id                   IN       consumer_upload_log.app_user_id%TYPE,
        pn_processed_cnt                 IN       consumer_upload_log.processed_cnt%TYPE,
        pn_ignored_cnt                   IN       consumer_upload_log.ignored_cnt%TYPE,
        pn_error_cnt                     IN       consumer_upload_log.error_cnt%TYPE,
        pn_error_row                     IN       consumer_upload_log.first_error_row%TYPE,
        pv_error_message                 IN       consumer_upload_log.error_message%TYPE
    ) IS
    BEGIN
        INSERT INTO consumer_upload_log(
            consumer_upload_log_id,
            location_id,
            app_user_id,
            processed_cnt,
            ignored_cnt,
            error_cnt,
            first_error_row,
            error_message)
          SELECT
            SEQ_CONSUMER_UPLOAD_LOG_ID.NEXTVAL,
            pn_location_id,
            pn_app_user_id,
            pn_processed_cnt,
            pn_ignored_cnt,
            pn_error_cnt,
            pn_error_row,
            pv_error_message
          FROM DUAL;
    END;
    
    PROCEDURE CREATE_INTERNAL_ACCOUNT(
        pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
        pn_global_account_id OUT PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
        pv_truncated_card_num PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD%TYPE,
		pv_last_consumer_acct_cd PSS.CONSUMER.LAST_CONSUMER_ACCT_CD%TYPE,
		pv_consumer_acct_promo_total PSS.CONSUMER_ACCT.CONSUMER_ACCT_PROMO_TOTAL%TYPE,
		pn_consumer_acct_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_TYPE_ID%TYPE,
		pr_consumer_acct_cd_hash PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD_HASH%TYPE,
		pr_security_cd_hash PSS.CONSUMER_ACCT.SECURITY_CD_HASH%TYPE,
		pr_security_cd_salt PSS.CONSUMER_ACCT.SECURITY_CD_SALT%TYPE,
		pv_security_cd_hash_alg PSS.CONSUMER_ACCT.SECURITY_CD_HASH_ALG%TYPE,
		pn_consumer_acct_identifier PSS.CONSUMER_ACCT.CONSUMER_ACCT_IDENTIFIER%TYPE,
		pn_consumer_acct_sub_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_SUB_TYPE_ID%TYPE,
        pn_consumer_acct_balance PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
        pn_consumer_id PSS.CONSUMER_ACCT.CONSUMER_ID%TYPE,
        pn_location_id PSS.CONSUMER_ACCT.LOCATION_ID%TYPE,
        pn_consumer_acct_issue_num PSS.CONSUMER_ACCT.CONSUMER_ACCT_ISSUE_NUM%TYPE,
        pn_payment_subtype_id PSS.CONSUMER_ACCT.PAYMENT_SUBTYPE_ID%TYPE,
        pd_consumer_acct_activation_ts PSS.CONSUMER_ACCT.CONSUMER_ACCT_ACTIVATION_TS%TYPE,
        pd_consumer_acct_deactiv_ts PSS.CONSUMER_ACCT.CONSUMER_ACCT_DEACTIVATION_TS%TYPE,
        pv_currency_cd PSS.CONSUMER_ACCT.CURRENCY_CD%TYPE,
        pn_consumer_acct_fmt_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_FMT_ID%TYPE,
        pn_corp_customer_id PSS.CONSUMER_ACCT.CORP_CUSTOMER_ID%TYPE,
        pr_consumer_acct_raw_hash PSS.CONSUMER_ACCT.CONSUMER_ACCT_RAW_HASH%TYPE DEFAULT NULL,
        pr_consumer_acct_raw_salt PSS.CONSUMER_ACCT.CONSUMER_ACCT_RAW_SALT%TYPE DEFAULT NULL,
        pv_consumer_acct_raw_alg PSS.CONSUMER_ACCT.CONSUMER_ACCT_RAW_ALG%TYPE DEFAULT NULL)
    IS
    BEGIN
        IF pr_consumer_acct_cd_hash IS NOT NULL AND pn_consumer_acct_type_id != 4 THEN
            SELECT MIN(CONSUMER_ACCT_ID)
              INTO pn_consumer_acct_id
              FROM PSS.CONSUMER_ACCT
             WHERE CONSUMER_ACCT_CD_HASH = pr_consumer_acct_cd_hash;
            IF pn_consumer_acct_id IS NOT NULL THEN -- this card number already exists, don't create
                pn_consumer_acct_id := 0;
                pn_global_account_id := 0;
                RETURN;
            END IF;
        END IF;
        IF pn_consumer_acct_identifier IS NOT NULL THEN
            pn_global_account_id := pn_consumer_acct_identifier;
            SELECT MIN(CONSUMER_ACCT_ID)
              INTO pn_consumer_acct_id
              FROM PSS.CONSUMER_ACCT_BASE
             WHERE GLOBAL_ACCOUNT_ID = pn_global_account_id
               AND GLOBAL_ACCOUNT_INSTANCE = 0;
            IF pn_consumer_acct_id IS NOT NULL THEN -- this global_account_id already exists, don't create
                pn_consumer_acct_id := 0;
                pn_global_account_id := 0;
                RETURN;
            END IF;
        ELSE           
            SELECT MIN(CAB.GLOBAL_ACCOUNT_ID)
              INTO pn_global_account_id
              FROM PSS.CONSUMER_ACCT CA
              JOIN PSS.CONSUMER_ACCT_BASE CAB ON CAB.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
             WHERE CA.CONSUMER_ACCT_IDENTIFIER IS NULL
               AND CA.CONSUMER_ACCT_TYPE_ID = pn_consumer_acct_type_id
               AND CA.CONSUMER_ACCT_CD = pv_truncated_card_num
               AND CAB.GLOBAL_ACCOUNT_INSTANCE IN(0, -1)
               AND ((pr_consumer_acct_cd_hash IS NULL AND CA.CONSUMER_ACCT_CD_HASH IS NULL) OR CA.CONSUMER_ACCT_CD_HASH = pr_consumer_acct_cd_hash);
            IF pn_global_account_id IS NULL THEN
                SELECT PSS.SEQ_GLOBAL_ACCOUNT_ID_BASE.NEXTVAL
                  INTO pn_global_account_id
                  FROM DUAL;
            END IF;
        END IF;
        SELECT PSS.SEQ_CONSUMER_ACCT_ID.NEXTVAL
          INTO pn_consumer_acct_id
          FROM DUAL;
        INSERT INTO PSS.CONSUMER_ACCT_BASE(CONSUMER_ACCT_ID, GLOBAL_ACCOUNT_ID, GLOBAL_ACCOUNT_INSTANCE, TRUNCATED_CARD_NUMBER, PAYMENT_SUBTYPE_ID, CURRENCY_CD)
            VALUES(pn_consumer_acct_id, pn_global_account_id, 0, pv_truncated_card_num, pn_payment_subtype_id, pv_currency_cd);               
        INSERT INTO PSS.CONSUMER_ACCT(
            CONSUMER_ACCT_ID,
            CONSUMER_ACCT_CD, 
            CONSUMER_ACCT_ACTIVE_YN_FLAG, 
            CONSUMER_ACCT_BALANCE, 
            CONSUMER_ID, 
            LOCATION_ID, 
            CONSUMER_ACCT_ISSUE_NUM, 
            PAYMENT_SUBTYPE_ID,
            CONSUMER_ACCT_ACTIVATION_TS, 
            CONSUMER_ACCT_DEACTIVATION_TS, 
            CURRENCY_CD, 
            CONSUMER_ACCT_FMT_ID, 
            CORP_CUSTOMER_ID,
			CONSUMER_ACCT_PROMO_BALANCE,
            CONSUMER_ACCT_PROMO_TOTAL,			
            CONSUMER_ACCT_TYPE_ID, 
            SECURITY_CD_HASH, 
            SECURITY_CD_SALT, 
            SECURITY_CD_HASH_ALG, 
            CONSUMER_ACCT_IDENTIFIER,
            CONSUMER_ACCT_SUB_TYPE_ID,
            CONSUMER_ACCT_CD_HASH,
            CONSUMER_ACCT_RAW_HASH, 
            CONSUMER_ACCT_RAW_SALT,
            CONSUMER_ACCT_RAW_ALG) 
        VALUES(
            pn_consumer_acct_id,
            pv_truncated_card_num,
            'Y',
            pn_consumer_acct_balance,
            pn_consumer_id,
            pn_location_id,
            pn_consumer_acct_issue_num,
            pn_payment_subtype_id,
            pd_consumer_acct_activation_ts,
            pd_consumer_acct_deactiv_ts,
            pv_currency_cd,
            pn_consumer_acct_fmt_id,
            pn_corp_customer_id,
			pv_consumer_acct_promo_total,
            pv_consumer_acct_promo_total,
            pn_consumer_acct_type_id,
            pr_security_cd_hash,
            pr_security_cd_salt,
            pv_security_cd_hash_alg,
            pn_consumer_acct_identifier,
            pn_consumer_acct_sub_type_id,
            pr_consumer_acct_cd_hash,
            pr_consumer_acct_raw_hash,
            pr_consumer_acct_raw_salt,
            pv_consumer_acct_raw_alg);
            
        UPDATE PSS.CONSUMER
           SET LAST_CONSUMER_ACCT_CD = pv_last_consumer_acct_cd
         WHERE CONSUMER_ID = pn_consumer_id;
    EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
            ROLLBACK;
            pn_consumer_acct_id := 0;
            pn_global_account_id := 0;
    END;
            
    PROCEDURE sp_add_consumer (
        pv_consumer_fname                IN       consumer.consumer_fname%TYPE,
        pv_consumer_lname                IN       consumer.consumer_lname%TYPE,
        pv_consumer_addr1                IN       consumer.consumer_addr1%TYPE,
        pv_consumer_addr2                IN       consumer.consumer_addr2%TYPE,
        pv_consumer_city                 IN       consumer.consumer_city%TYPE,
        pv_consumer_state                IN       consumer.consumer_state_cd%TYPE,
        pv_consumer_postal_cd            IN       consumer.consumer_postal_cd%TYPE,
        pv_consumer_email_addr1          IN       consumer.consumer_email_addr1%TYPE,
        pn_consumer_type_id              IN       consumer.consumer_type_id%TYPE,
        pn_location_id                   IN       consumer_acct.location_id%TYPE,
        pv_consumer_account_cd           IN       consumer_acct.consumer_acct_cd%TYPE,
        pv_consumer_accnt_actv_yn_flag   IN       consumer_acct.consumer_acct_active_yn_flag%TYPE,
        pv_consumer_account_balance      IN       consumer_acct.consumer_acct_balance%TYPE,
        pn_return_code                   OUT      exception_code.exception_code_id%TYPE,
        pv_error_message                 OUT      exception_data.additional_information%TYPE,
        pc_auto_notify_on                IN       consumer_notif.notify_on%TYPE DEFAULT 'N'
    ) IS
        v_error_msg                  exception_data.additional_information%TYPE;
        cv_procedure_name   CONSTANT VARCHAR2 (30)                     := 'sp_add_consumer';
        n_return_cd                  NUMBER;
        n_issue_num                  NUMBER;
        n_consumer_id                consumer.consumer_id%TYPE;
        n_app_user_id                app_user.app_user_id%TYPE;
        d_effective_date             DATE := SYSDATE;
        n_consumer_acct_id           consumer_acct.consumer_acct_id%TYPE;
		n_global_account_id			 pss.consumer_acct_base.global_account_id%TYPE;
    BEGIN
        -- Check if the user already exists in the db.  If so we can not
        -- add a new one
        IF pv_consumer_email_addr1 IS NULL THEN
            RAISE_APPLICATION_ERROR(-20171, 'An email address must be provided');
        END IF;
        SELECT MAX(consumer_id)
          INTO n_consumer_id
          FROM consumer
         WHERE UPPER(consumer_email_addr1) = UPPER(pv_consumer_email_addr1);

        IF n_consumer_id IS NULL THEN
            SELECT seq_consumer_id.NEXTVAL
              INTO n_consumer_id
              FROM DUAL;

            INSERT INTO consumer
                     (consumer_id,
                      consumer_fname,
                      consumer_lname,
                      consumer_addr1,
                      consumer_addr2,
                      consumer_city,
                      consumer_state_cd,
                      consumer_postal_cd,
                      consumer_email_addr1,
                      consumer_type_id
                     )
              VALUES (n_consumer_id,
                      pv_consumer_fname,
                      pv_consumer_lname,
                      pv_consumer_addr1,
                      pv_consumer_addr2,
                      pv_consumer_city,
                      pv_consumer_state,
                      pv_consumer_postal_cd,
                      pv_consumer_email_addr1,
                      pn_consumer_type_id
                     );
        ELSE
            DECLARE
                l_cnt PLS_INTEGER;
            BEGIN
            /* We must check that this consumer has an account that is in the same
            location heirarchy tree as the location_id specified - otherwise someone
            from one school could update consumer info of someone from another school!!!
                BSK - 2005/08/16
                
            Allow edit of consumer if all consumer accts are at one school - otherwise
            silently ignore changes and proceed.
                BSK - 2007/08/07
                
            TODO: We really ought to specify the server names of email addresses
                  allowed for each location. (i.e. - Goucher College may only have
                  email addresses ending in '@goucher.edu').
            */
                SELECT COUNT(DISTINCT l.PARENT_LOCATION_ID)
                  INTO l_cnt
                  FROM PSS.CONSUMER_ACCT CA
                 INNER JOIN LOCATION.LOCATION L ON CA.LOCATION_ID = L.LOCATION_ID
                 WHERE CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y'
                   AND (CA.CONSUMER_ID = n_consumer_id
                       OR CA.LOCATION_ID = pn_location_id);
                IF l_cnt <= 1 THEN
                    UPDATE consumer
                       SET consumer_fname = pv_consumer_fname,
                           consumer_lname = pv_consumer_lname,
                           consumer_addr1 = pv_consumer_addr1,
                           consumer_addr2 = pv_consumer_addr2,
                           consumer_city = pv_consumer_city,
                           consumer_state_cd = pv_consumer_state,
                           consumer_postal_cd = pv_consumer_postal_cd,
                           consumer_type_id = pn_consumer_type_id
                     WHERE consumer_id = n_consumer_id;
                END IF;
            END;
        END IF;
        --If deactivating then just update.
        IF pv_consumer_accnt_actv_yn_flag = 'N' THEN
            UPDATE consumer_acct
               SET consumer_acct_deactivation_ts = d_effective_date,
                   consumer_acct_active_yn_flag = 'N'
             WHERE consumer_acct_cd = pv_consumer_account_cd
               AND location_id = pn_location_id
               AND consumer_acct_active_yn_flag = 'Y';
        ELSE
            -- If balance, consumer, or active flag changed then create new record otherwise, do nothing
            BEGIN
                SELECT consumer_acct_id
                  INTO n_consumer_acct_id
                  FROM consumer_acct
                 WHERE consumer_acct_cd = pv_consumer_account_cd
                   AND location_id = pn_location_id
                   AND consumer_acct_active_yn_flag = 'Y'
                   AND consumer_id = n_consumer_id
                   AND (pv_consumer_account_balance IS NULL
                        OR pv_consumer_account_balance = consumer_acct_balance);
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    NULL;
                WHEN OTHERS THEN
                    RAISE;
            END;
            IF n_consumer_acct_id IS NULL THEN
                --deactivate old ones and create new one
                UPDATE consumer_acct
                   SET consumer_acct_deactivation_ts = d_effective_date,
                       consumer_acct_active_yn_flag = 'N'
                 WHERE consumer_acct_cd = pv_consumer_account_cd
                   AND location_id = pn_location_id
                   AND consumer_acct_active_yn_flag = 'Y';
                -- get issue num
                SELECT MAX(consumer_acct_issue_num)
                  INTO n_issue_num
                  FROM consumer_acct
                 WHERE consumer_acct_cd = pv_consumer_account_cd
                   AND location_id = pn_location_id;
                -- Add record
                SELECT MIN(CAB.GLOBAL_ACCOUNT_ID)
                  INTO n_global_account_id
                  FROM PSS.CONSUMER_ACCT CA
                  JOIN PSS.CONSUMER_ACCT_BASE CAB ON CAB.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
                 WHERE CA.CONSUMER_ACCT_IDENTIFIER IS NULL
                   AND CA.CONSUMER_ACCT_TYPE_ID = 4
                   AND CA.CONSUMER_ACCT_CD = pv_consumer_account_cd
                   AND CAB.GLOBAL_ACCOUNT_INSTANCE IN(0, -1)
                   AND CA.CONSUMER_ACCT_CD_HASH IS NULL;
                IF n_global_account_id IS NULL THEN
                    SELECT PSS.SEQ_GLOBAL_ACCOUNT_ID_BASE.NEXTVAL
                      INTO n_global_account_id
                      FROM DUAL;
                END IF;
                SELECT PSS.SEQ_CONSUMER_ACCT_ID.NEXTVAL
                  INTO n_consumer_acct_id
                  FROM DUAL;
                INSERT INTO PSS.CONSUMER_ACCT_BASE(CONSUMER_ACCT_ID, GLOBAL_ACCOUNT_ID, GLOBAL_ACCOUNT_INSTANCE, TRUNCATED_CARD_NUMBER, PAYMENT_SUBTYPE_ID, LOCATION_ID, CURRENCY_CD)
                    VALUES(n_consumer_acct_id, n_global_account_id, -1, DBADMIN.MASK_CREDIT_CARD(pv_consumer_account_cd), 1, pn_location_id, 'USD');               
                INSERT INTO pss.consumer_acct
                     (CONSUMER_ACCT_ID,
                      CONSUMER_ACCT_CD,
                      CONSUMER_ACCT_ACTIVE_YN_FLAG,
                      CONSUMER_ACCT_BALANCE,
                      CONSUMER_ID,
                      PAYMENT_SUBTYPE_ID,
                      LOCATION_ID,
                      CONSUMER_ACCT_ISSUE_NUM,
                      CONSUMER_ACCT_ACTIVATION_TS
                     )
                VALUES
                     (n_consumer_acct_id,
                      pv_consumer_account_cd,
                      pv_consumer_accnt_actv_yn_flag,
                      NVL(pv_consumer_account_balance, 0),
                      n_consumer_id,
                      1,
                      pn_location_id,
                      NVL(n_issue_num, 0) + 1,
                      SYSDATE
                     );
            END IF;

            -- Now create an app user
            SELECT MAX(app_user_id)
              INTO n_app_user_id
              FROM app_user
             WHERE UPPER(app_user_email_addr) = UPPER(pv_consumer_email_addr1);

            IF n_app_user_id IS NULL THEN
                SELECT seq_app_user_id.NEXTVAL
                  INTO n_app_user_id
                  FROM DUAL;

                INSERT INTO app_user
                     (app_user_id,
                      app_user_password,
                      app_user_active_yn_flag,
                      force_pw_change_yn_flag,
                      app_user_name,
                      app_user_fname,
                      app_user_lname,
                      app_user_email_addr
                     )
                VALUES (
                      n_app_user_id,
                      SUBSTR (pv_consumer_account_cd,
                              LENGTH (pv_consumer_account_cd) - 3,
                              4),
                      pv_consumer_accnt_actv_yn_flag,
                      'N',
                      pv_consumer_email_addr1,
                      pv_consumer_fname,
                      pv_consumer_lname,
                      pv_consumer_email_addr1
                     );

                INSERT INTO app_user_object_permission
                     (app_user_id,
                      app_id,
                      app_object_type_id,
                      allow_object_create_yn_flag,
                      allow_object_read_yn_flag,
                      allow_object_modify_yn_flag,
                      allow_object_delete_yn_flag,
                      object_cd
                     )
                VALUES (
                      n_app_user_id,
                      1,
                      1,
                      'N',
                      'Y',
                      'Y',
                      'N',
                      n_consumer_id
                     );
            ELSE
                /* I think we don't need this code - because now a student could have
                more than one account - so let's just not change the password
                 SELECT app_user_password,
                        c.consumer_acct_cd
                   INTO v_app_user_pw,
                        v_old_consumer_acct_cd
                   FROM app_user a,
                        consumer b,
                        consumer_acct c
                  WHERE a.app_user_id = n_app_user_id
                    AND UPPER (a.app_user_email_addr) = UPPER (b.consumer_email_addr1)
                    AND b.consumer_id = c.consumer_id
                    AND c.consumer_acct_deactivation_ts IS NULL;

                 IF SUBSTR (v_old_consumer_acct_cd,
                            LENGTH (v_old_consumer_acct_cd) - 3,
                            4
                           ) = v_app_user_pw THEN
                    v_new_consumer_acct_cd :=
                           SUBSTR (pv_consumer_account_cd,
                                   LENGTH (pv_consumer_account_cd) - 3,
                                   4
                                  );
                 ELSE
                    v_new_consumer_acct_cd := v_app_user_pw;
                 END IF;
                */
                UPDATE app_user
                   SET --app_user_password = v_new_consumer_acct_cd,
                       app_user_active_yn_flag = pv_consumer_accnt_actv_yn_flag,
                       force_pw_change_yn_flag = 'N',
                       app_user_name = pv_consumer_email_addr1,
                       app_user_fname = pv_consumer_fname,
                       app_user_lname = pv_consumer_lname
                 WHERE UPPER (app_user_email_addr) = UPPER (pv_consumer_email_addr1);

                INSERT INTO app_user_object_permission
                     (app_user_id,
                      app_id,
                      app_object_type_id,
                      allow_object_create_yn_flag,
                      allow_object_read_yn_flag,
                      allow_object_modify_yn_flag,
                      allow_object_delete_yn_flag,
                      object_cd
                     )
                  SELECT
                      n_app_user_id,
                      1,
                      1,
                      'N',
                      'Y',
                      'Y',
                      'N',
                      n_consumer_id
                    FROM DUAL
                   WHERE NOT EXISTS(SELECT 1
                          FROM app_user_object_permission
                         WHERE app_user_id = n_app_user_id
                           AND app_id = 1
                           AND app_object_type_id = 1
                           AND object_cd = TO_CHAR(n_consumer_id));
            END IF;
            CONSUMER_NOTIF_UPD(n_consumer_id, 2, NVL(pc_auto_notify_on, 'N'), pv_consumer_email_addr1);
        END IF;
   /* changed this to report exceptions (BSK 11-10-04)
   EXCEPTION
      WHEN OTHERS THEN
         -- changed this slightly to set pv_error_message to only SQLERRM and
         -- pn_return_code to SQLCODE for web file upload functionality (BSK 11-05-04)
         pv_error_message := SQLERRM;
         pn_return_code := SQLCODE;
         pkg_exception_processor.sp_log_exception
                                               (pkg_app_exec_hist_globals.unknown_error_id,
                                                v_error_msg,
                                                pkg_app_exec_hist_globals.cv_server_name,
                                                cv_package_name || '.'
                                                || cv_procedure_name
                                               );
   --*/
   END;

   FUNCTION  DEACTIVATE_NOT_LISTED_ACCTS (
      pvt_consumer_account_cd   IN       VARCHAR2_TABLE,
      pn_location_id            IN       CONSUMER_ACCT.LOCATION_ID%TYPE
   ) RETURN PLS_INTEGER IS
      n_consumer_account_count     PLS_INTEGER;
   BEGIN
      UPDATE consumer_acct ca
         SET ca.consumer_acct_active_yn_flag = 'N',
             ca.consumer_acct_deactivation_ts = SYSDATE
       WHERE ca.CONSUMER_ACCT_CD NOT MEMBER OF pvt_consumer_account_cd
         AND ca.location_id = pn_location_id
         AND ca.consumer_acct_active_yn_flag = 'Y';
      n_consumer_account_count := SQL%ROWCOUNT;
      -- Should we deactivate app_user rows that now have no consumer acct active?
      --/*
      UPDATE app_user ap
         SET ap.app_user_active_yn_flag = 'N'
       WHERE UPPER (ap.app_user_email_addr) IN(
           SELECT UPPER (c.consumer_email_addr1)
             FROM PSS.CONSUMER c
            INNER JOIN PSS.CONSUMER_ACCT ca ON C.CONSUMER_ID = CA.CONSUMER_ID
            WHERE ca.CONSUMER_ACCT_CD NOT MEMBER OF pvt_consumer_account_cd
              AND CA.LOCATION_ID = pn_location_id
              AND NOT EXISTS(
                 SELECT 1
                   FROM PSS.CONSUMER_ACCT ca1
                  WHERE ca1.consumer_id = c.consumer_id
                    AND ca1.consumer_acct_active_yn_flag = 'Y')
           );
       --*/
       RETURN n_consumer_account_count;
   END;
   
   PROCEDURE sp_deactivate_consumer (
      pv_consumer_email_addr1   IN       consumer.consumer_email_addr1%TYPE,
      pn_return_code            OUT      exception_code.exception_code_id%TYPE,
      pv_error_message          OUT      exception_data.additional_information%TYPE
   ) IS
      v_error_msg                  exception_data.additional_information%TYPE;
      cv_procedure_name   CONSTANT VARCHAR2 (30)             := 'sp_add_consumer_account';
      n_return_cd                  NUMBER;
      n_consumer_account_count     NUMBER;
   BEGIN
      -- Check if the user already exists in the db.  If so we can not
      -- add a new one
      UPDATE consumer_acct ca
         SET ca.consumer_acct_active_yn_flag = 'N',
             ca.consumer_acct_deactivation_ts = SYSDATE
       WHERE ca.consumer_id IN (
                       SELECT consumer_id
                         FROM consumer
                        WHERE UPPER (consumer_email_addr1) =
                                                           UPPER (pv_consumer_email_addr1) );

      UPDATE app_user ap
         SET ap.app_user_active_yn_flag = 'N'
       WHERE UPPER (ap.app_user_email_addr) = UPPER (pv_consumer_email_addr1);
   EXCEPTION
      WHEN OTHERS THEN
         pv_error_message :=
                         'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pn_return_code := pkg_app_exec_hist_globals.unknown_error_id;
         pkg_exception_processor.sp_log_exception
                                               (pn_return_code,
                                                v_error_msg,
                                                pkg_app_exec_hist_globals.cv_server_name,
                                                cv_package_name || '.'
                                                || cv_procedure_name
                                               );
         pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution;
   END;
   
   FUNCTION CREATE_CONSUMER_PASSCODE(
    l_consumer_id CONSUMER_PASSCODE.CONSUMER_ID%TYPE,
    l_passcode_type_id PASSCODE_TYPE.PASSCODE_TYPE_ID%TYPE,
    l_expiration_dt CONSUMER_PASSCODE.EXPIRATION_TS%TYPE)
    RETURN CONSUMER_PASSCODE.PASSCODE%TYPE
   IS
    l_passcode CONSUMER_PASSCODE.PASSCODE%TYPE;
   BEGIN
      l_passcode := DBMS_RANDOM.STRING('A', 10);
      INSERT INTO CONSUMER_PASSCODE(CONSUMER_PASSCODE_ID, CONSUMER_ID, PASSCODE, PASSCODE_TYPE_ID, EXPIRATION_TS)
        VALUES(SEQ_CONSUMER_PASSCODE_ID.NEXTVAL, l_consumer_id, l_passcode, l_passcode_type_id, l_expiration_dt);
      RETURN l_passcode;
   EXCEPTION
    WHEN DUP_VAL_ON_INDEX THEN
        RETURN CREATE_CONSUMER_PASSCODE(l_consumer_id, l_passcode_type_id, l_expiration_dt);
   END;

   FUNCTION CREATE_CONSUMER_PASSCODE(
    l_consumer_id CONSUMER_PASSCODE.CONSUMER_ID%TYPE,
    l_passcode_type_cd PASSCODE_TYPE.PASSCODE_TYPE_CD%TYPE)
    RETURN CONSUMER_PASSCODE.PASSCODE%TYPE
   IS
    l_passcode CONSUMER_PASSCODE.PASSCODE%TYPE;
    l_passcode_type_id PASSCODE_TYPE.PASSCODE_TYPE_ID%TYPE;
    l_expiration_dt CONSUMER_PASSCODE.EXPIRATION_TS%TYPE;
   BEGIN
      SELECT PASSCODE_TYPE_ID, SYSDATE + DURATION_DAYS
        INTO l_passcode_type_id, l_expiration_dt
        FROM PASSCODE_TYPE
       WHERE PASSCODE_TYPE_CD = l_passcode_type_cd;
       
      RETURN CREATE_CONSUMER_PASSCODE(l_consumer_id, l_passcode_type_id, l_expiration_dt);
   EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RAISE_APPLICATION_ERROR(-20180, 'Invalid Passcode Type "'||l_passcode_type_cd||'"');
   END;
   
   PROCEDURE GET_CONSUMER_NOTIF(
    l_primary_email_addr CONSUMER.CONSUMER_EMAIL_ADDR1%TYPE,
    l_host_status_notif_type_id CONSUMER_NOTIF.HOST_STATUS_NOTIF_TYPE_ID%TYPE,
    l_passcode CONSUMER_PASSCODE.PASSCODE%TYPE,
    l_passcode_type_cd PASSCODE_TYPE.PASSCODE_TYPE_CD%TYPE,
    l_consumer_id OUT CONSUMER.consumer_id%TYPE,
    l_expired OUT CHAR,
    l_notify_email_addr OUT CONSUMER.CONSUMER_EMAIL_ADDR2%TYPE,
    l_notify_on OUT CONSUMER_NOTIF.NOTIFY_ON%TYPE)
   IS
   BEGIN
       SELECT CPC.CONSUMER_ID,
              CASE WHEN CPC.EXPIRATION_TS < SYSDATE THEN 'Y' ELSE 'N' END,
              C.CONSUMER_EMAIL_ADDR2,
              NVL(CN.NOTIFY_ON, 'N')
         INTO l_consumer_id,
              l_expired,
              l_notify_email_addr,
              l_notify_on
         FROM CONSUMER_PASSCODE CPC, CONSUMER C, CONSUMER_NOTIF CN, PASSCODE_TYPE PT
        WHERE CPC.CONSUMER_ID = C.CONSUMER_ID
          AND C.CONSUMER_ID = CN.CONSUMER_ID (+)
          AND UPPER(C.CONSUMER_EMAIL_ADDR1) = UPPER(l_primary_email_addr)
          AND CN.HOST_STATUS_NOTIF_TYPE_ID (+) = l_host_status_notif_type_id
          AND CPC.PASSCODE = l_passcode
          AND CPC.PASSCODE_TYPE_ID = PT.PASSCODE_TYPE_ID
          AND PT.PASSCODE_TYPE_CD = l_passcode_type_cd;
   END;
   
   PROCEDURE EXPIRE_CONSUMER_PASSCODE(
    l_passcode CONSUMER_PASSCODE.PASSCODE%TYPE)
   IS
   BEGIN
    UPDATE CONSUMER_PASSCODE CPC
       SET CPC.EXPIRATION_TS = SYSDATE
      WHERE CPC.PASSCODE = l_passcode;
   END;


END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/releases/REL_Edge_Update_R55/R55.USADB.USAT454.sql?revision=HEAD
DECLARE
	ln_property_list_version NUMBER := 0;
	ln_device_type_id device_type.device_type_id%TYPE := 11;
	ln_config_template_type_id config_template_type.config_template_type_id%TYPE := 1;
	lv_config_template_name config_template.config_template_name%TYPE := 'EPORT-INTERACTIVE-DEFAULT-CFG-' || ln_device_type_id || '-' || ln_property_list_version;
	ln_config_template_id config_template.config_template_id%TYPE;

	PROCEDURE ADD_CTS(
		pn_CONFIG_TEMPLATE_ID NUMBER,
		pv_PARAMETER_CD VARCHAR2,
		pv_SETTING_VALUE VARCHAR2) 
	IS
	BEGIN
		INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(
			CONFIG_TEMPLATE_ID, 
			DEVICE_SETTING_PARAMETER_CD, 
			CONFIG_TEMPLATE_SETTING_VALUE) 
		VALUES(pn_CONFIG_TEMPLATE_ID, pv_PARAMETER_CD, pv_SETTING_VALUE);
	END;

	PROCEDURE ADD_CTS_META(
		pv_PARAMETER_CD VARCHAR2,
		pv_SETTING_VALUE VARCHAR2,
		pn_CATEGORY_ID NUMBER,
		pv_DISPLAY VARCHAR2,
		pv_DESCRIPTION VARCHAR2,
		pv_EDITOR VARCHAR2,
		pn_FIELD_SIZE NUMBER,
		pn_DISPLAY_ORDER NUMBER,
		pv_DATA_MODE VARCHAR2,
		pv_DATA_MODE_AUX VARCHAR2,
		pn_REGEX_ID NUMBER,
		pv_NAME VARCHAR2,
		pv_CUSTOMER_EDITABLE VARCHAR2,
		pv_STORED_IN_PENNIES VARCHAR2) 
	IS 
	BEGIN
		INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(
			CONFIG_TEMPLATE_ID,
			DEVICE_SETTING_PARAMETER_CD,
			CONFIG_TEMPLATE_SETTING_VALUE,
			CATEGORY_ID,
			DISPLAY,
			DESCRIPTION,
			EDITOR,
			CONVERTER,
			CLIENT_SEND,
			ALIGN,
			FIELD_SIZE,
			FIELD_OFFSET,
			PAD_CHAR,
			PROPERTY_LIST_VERSION,
			DEVICE_TYPE_ID,
			DISPLAY_ORDER,
			ALT_NAME,
			DATA_MODE,
			DATA_MODE_AUX,
			REGEX_ID,
			ACTIVE,
			NAME,
			CUSTOMER_EDITABLE,
			STORED_IN_PENNIES) 
		SELECT 
			CONFIG_TEMPLATE_ID, 
			pv_PARAMETER_CD,
			pv_SETTING_VALUE,
			pn_CATEGORY_ID,
			pv_DISPLAY,
			pv_DESCRIPTION,
			pv_EDITOR,
			'',
			'Y',
			NULL,
			pn_FIELD_SIZE,
			NULL,
			NULL,
			0,
			DEVICE_TYPE_ID,
			pn_DISPLAY_ORDER,
			NULL,
			pv_DATA_MODE,
			pv_DATA_MODE_AUX,
			pn_REGEX_ID,
			'Y',
			pv_NAME,
			pv_CUSTOMER_EDITABLE,
			pv_STORED_IN_PENNIES
		FROM DEVICE.CONFIG_TEMPLATE CT 
		WHERE CONFIG_TEMPLATE_TYPE_ID = 3 
			AND DEVICE_TYPE_ID = 11 
			AND NOT EXISTS (
				SELECT 1 
				FROM DEVICE.CONFIG_TEMPLATE_SETTING 
				WHERE DEVICE_TYPE_ID = 11 
				AND DEVICE_SETTING_PARAMETER_CD = pv_PARAMETER_CD);
	END;
		
BEGIN
	
	-- add meta data for new settings
	-- application
	ADD_CTS_META('1500', '1', 2, 'Y', 'Sets VMC or Kiosk Interface type.', 'SELECT:1=1 - Standard MDB;3=3 - Coin Pulse;5=5 - Top-off Coin Pulse', 1, 0, 'A', NULL, 3, 'VMC Interface Type', 'Y', 'N');
	ADD_CTS_META('1200', '1000', 2, 'Y', 'Amount in Authorization Requests', 'TEXT:1 to 11', 10, 1, 'A', NULL, 26, 'Authorization Amount', 'Y', 'Y');
	ADD_CTS_META('1022', '0', 2, 'Y', 'Enables or disables recording of cash transactions. It supports MDB and Coin Pulse VMC Interface Types.', 'SELECT:0=0 - Disabled;1=1 - Enabled', 5, 3, 'A', NULL, 3, 'Cash Transaction Recording', 'Y', 'N');
	ADD_CTS_META('1202', null, 2, 'Y', 'Amount added to the sale amount per each vended item.', 'TEXT:1 to 5', 4, 3, 'A', NULL, 26, 'Two-Tier Pricing', 'Y', 'Y');
	
	-- mdb
	ADD_CTS_META('1024', null, 6, 'N', 'Enables or disables MDB Alerts. Note, Alerts must be enabled to support USALive''s Device Alerts of types MDB Alerts Bill Acceptor and MDB Alerts Coin Changer.', 'SELECT:0=0 - Disabled;1=1 - Enabled', 1, 24, 'A', NULL, 3, 'MDB Alerts', 'Y', 'N');
	
	-- coin pulse
	ADD_CTS_META('1501', null, 4, 'Y', 'Used to setup how long each pulse will stay in the active state. (i.e. pulse width.) The pulse width duration is calculated by multiplying this parameter by 10ms.  A value of 10 would yield a pulse width of 100 ms. Valid values: 1-255.', 'TEXT:1 to 3', 3, 2, 'A', 'N', 3, 'Coin Pulse Duration', 'Y', 'N');
	ADD_CTS_META('1502', null, 4, 'Y', 'Used to setup how much time is required between pulses that the pulse output line will stay in the inactive state.  The duration of time that the pulse output will stay in the inactive state between consecutive pulses is calculated by multiplying this parameter by 10ms. A value of "25" would yield a delay between consecutive pulses of 250 ms.  Valid Values: 1 - 1000.', 'TEXT:1 to 4', 4, 3, 'A', 'N', 3, 'Coin Pulse Spacing', 'Y', 'N');
	ADD_CTS_META('1503', null, 4, 'Y', 'The equivalent monetary value of each output pulse.  If the VMC (Vending Machine Controller) / connected equipment expect that each pulse is equal to 25 cents, then this value would be 0.25.  This field combined with the item price is used to calculate how many pulses to transmit when an item is purchased.  Valid Values: 0.01 - 655.35.', 'TEXT:1 to 6', 5, 4, 'A', 'N', 26, 'Coin Pulse Value', 'Y', 'Y');
	ADD_CTS_META('1504', null, 4, 'Y', 'The number of seconds in which a "Multi-Item Selection Session" will time out if there is no activity.  Valid Values: 1-255.', 'TEXT:1 to 3', 3, 5, 'A', 'N', 3, 'Coin Auto Timeout', 'Y', 'N');
	ADD_CTS_META('1505', null, 4, 'Y', 'Determines whether the Reader Enable input is active high or active low', 'SELECT:1=1 - Active Low;2=2 - Active High', 1, 6, 'A', NULL, 19, 'Coin Reader Enable Active State', 'Y', 'N');
	ADD_CTS_META('1506', null, 4, 'Y', 'This field is used with the coin pulse interface to set pricing for 1st button press prior to starting a transaction. An entry of 0 will cause the item to be skipped.', 'TEXT:1 to 6', 5, 7, 'A', 'N', 26, 'Coin Item Price #1', 'Y','Y');
	ADD_CTS_META('1507', null, 4, 'Y', 'Used to display a description instead of a $ value for 1st button press prior to starting a transaction. Data entry example: Charge #1.', 'TEXT:0 to 16', 16, 8, 'A', NULL, 19, 'Coin Item Price #1 Label', 'Y', 'N');
	ADD_CTS_META('1508', null, 4, 'Y', 'This field is used with the coin pulse interface to set pricing for 2nd button press prior to starting a transaction. An entry of 0 will cause the item to be skipped.', 'TEXT:1 to 6', 5, 7, 'A', 'N', 26, 'Coin Item Price #2', 'Y', 'Y');
	ADD_CTS_META('1509', null, 4, 'Y', 'Used to display a description instead of a $ value for 2nd button press prior to starting a transaction. Data entry example: Charge #2.', 'TEXT:0 to 16', 16, 8, 'A', NULL, 19, 'Coin Item Price #2 Label', 'Y', 'N');
	ADD_CTS_META('1510', null, 4, 'Y', 'This field is used with the coin pulse interface to set pricing for 3rd button press prior to starting a transaction. An entry of 0 will cause the item to be skipped.', 'TEXT:1 to 6', 5, 7, 'A', 'N', 26, 'Coin Item Price #3', 'Y', 'Y');
	ADD_CTS_META('1511', null, 4, 'Y', 'Used to display a description instead of a $ value for 3rd button press prior to starting a transaction. Data entry example: Charge #3.', 'TEXT:0 to 16', 16, 8, 'A', NULL, 19, 'Coin Item Price #3 Label', 'Y', 'N');
	ADD_CTS_META('1512', null, 4, 'Y', 'This field is used with the coin pulse interface to set pricing for 4th button press prior to starting a transaction. An entry of 0 will cause the item to be skipped.', 'TEXT:1 to 6', 5, 7, 'A', 'N', 26, 'Coin Item Price #4', 'Y', 'Y');
	ADD_CTS_META('1513', null, 4, 'Y', 'Used to display a description instead of a $ value for 4th button press prior to starting a transaction. Data entry example: Charge #4.', 'TEXT:0 to 16', 16, 8, 'A', NULL, 19, 'Coin Item Price #4 Label', 'Y', 'N');
	ADD_CTS_META('1514', null, 4, 'Y', 'This field is used with the coin pulse interface to set pricing for 5th button press prior to starting a transaction. An entry of 0 will cause the item to be skipped.', 'TEXT:1 to 6', 5, 7, 'A', 'N', 26, 'Coin Item Price #5', 'Y', 'Y');
	ADD_CTS_META('1515', null, 4, 'Y', 'Used to display a description instead of a $ value for 5th button press prior to starting a transaction. Data entry example: Charge #5.', 'TEXT:0 to 16', 16, 8, 'A', NULL, 19, 'Coin Item Price #5 Label', 'Y', 'N');
	ADD_CTS_META('1516', null, 4, 'Y', 'This field is used with the coin pulse interface to set pricing for 6th button press prior to starting a transaction. An entry of 0 will cause the item to be skipped.', 'TEXT:1 to 6', 5, 7, 'A', 'N', 26, 'Coin Item Price #6', 'Y', 'Y');
	ADD_CTS_META('1517', null, 4, 'Y', 'Used to display a description instead of a $ value for 6th button press prior to starting a transaction. Data entry example: Charge #6.', 'TEXT:0 to 16', 16, 8, 'A', NULL, 19, 'Coin Item Price #6 Label', 'Y', 'N');
	ADD_CTS_META('1518', null, 4, 'Y', 'This field is used with the coin pulse interface to set pricing for 7th button press prior to starting a transaction. An entry of 0 will cause the item to be skipped.', 'TEXT:1 to 6', 5, 7, 'A', 'N', 26, 'Coin Item Price #7', 'Y', 'Y');
	ADD_CTS_META('1519', null, 4, 'Y', 'Used to display a description instead of a $ value for 7th button press prior to starting a transaction. Data entry example: Charge #7.', 'TEXT:0 to 16', 16, 8, 'A', NULL, 19, 'Coin Item Price #7 Label', 'Y', 'N');
	ADD_CTS_META('1520', null, 4, 'Y', 'This field is used with the coin pulse interface to set pricing for 8th button press prior to starting a transaction. An entry of 0 will cause the item to be skipped.', 'TEXT:1 to 6', 5, 7, 'A', 'N', 26, 'Coin Item Price #8', 'Y', 'Y');
	ADD_CTS_META('1521', null, 4, 'Y', 'Used to display a description instead of a $ value for 8th button press prior to starting a transaction. Data entry example: Charge #8.', 'TEXT:0 to 16', 16, 8, 'A', NULL, 19, 'Coin Item Price #8 Label', 'Y', 'N');
	ADD_CTS_META('1522', null, 4, 'Y', 'Number of seconds associated with Coin Pulse Value. This tells the device how long a Top-off session is running. For a unit with Coin Pulse Value of 25 and Coin Item Price #1 of 2.50, the time would be 300 seconds.', 'TEXT:1 to 5', 5, 23, 'A', NULL, 3, 'Pulse Time Value', 'Y', 'N');
	ADD_CTS_META('1523', null, 4, 'Y', 'The equivalent monetary value that the consumer will be charged per each additional Card Swipe. If the VMC / connected equipment expect that each pulse is equal to 25 cents, then 4 pulses will be sent to the VMC and an additional 120 seconds will be added to the running Top-off time. Valid Values: 0.01 - 655.35.', 'TEXT:1 to 6', 6, 24, 'A', NULL, 26, 'Top-off Value', 'Y', 'Y');
	ADD_CTS_META('1524', null, 4, 'Y', 'Determines whether the Cash Input Pulse is active high or active low. Cash Transaction Recording has to be enabled along with setting this value.', 'USConnect', 1, 25, 'A', NULL, 3, 'Pulse Capture Polarity', 'Y', 'N');
	ADD_CTS_META('1525', null, 4, 'Y', 'Used to set up how long each input pulse will have to stay in the active state (in milliseconds) for a Pulse Cash Transaction to be acknowledged (i.e. pulse width). Valid Values: 1 - 255. Cash Transaction Recording has to be enabled along with setting this value.', 'TEXT:1 to 3', 3, 26, 'A', NULL, 3, 'Pulse Capture Debounce', 'Y', 'N');
	ADD_CTS_META('1526', null, 4, 'Y', 'The equivalent monetary value of each Cash input pulse. Valid Values: 0.01 - 655.35. Cash Transaction Recording has to be enabled along with setting this value.', 'TEXT:1 to 6', 6, 27, 'A', NULL, 26, 'Pulse Capture Value', 'Y', 'Y');

	-- check if config template exists yet
	
	SELECT MAX(config_template_id) INTO ln_config_template_id
	FROM device.config_template
	WHERE config_template_name = lv_config_template_name;
	
	IF ln_config_template_id IS NULL THEN
		-- create it if not
		INSERT INTO device.config_template(config_template_type_id, config_template_name, device_type_id, property_list_version)
		VALUES(ln_config_template_type_id, lv_config_template_name, ln_device_type_id, ln_property_list_version);
		
		SELECT MAX(config_template_id) INTO ln_config_template_id
		FROM device.config_template
		WHERE config_template_name = lv_config_template_name;
	END IF;
	
	-- remove existing settings for this template, if any
	
	DELETE FROM DEVICE.CONFIG_TEMPLATE_SETTING WHERE CONFIG_TEMPLATE_ID = ln_config_template_id;

	-- add the parameters to the default template
	
	ADD_CTS(ln_config_template_id, '1500', '1');
	ADD_CTS(ln_config_template_id, '1200', '1000');
	ADD_CTS(ln_config_template_id, '1022', '1');	
	ADD_CTS(ln_config_template_id, '1202', '0');
	
	ADD_CTS(ln_config_template_id, '1024', '1');
	
	ADD_CTS(ln_config_template_id, '1501', '15');
	ADD_CTS(ln_config_template_id, '1502', '25');	
	ADD_CTS(ln_config_template_id, '1503', '25');
	ADD_CTS(ln_config_template_id, '1504', '20');
	ADD_CTS(ln_config_template_id, '1505', '2');
	ADD_CTS(ln_config_template_id, '1506', '25');
	ADD_CTS(ln_config_template_id, '1507', null);
	ADD_CTS(ln_config_template_id, '1508', '0');
	ADD_CTS(ln_config_template_id, '1509', null);
	ADD_CTS(ln_config_template_id, '1510', '0');	
	ADD_CTS(ln_config_template_id, '1511', null);
	ADD_CTS(ln_config_template_id, '1512', '0');
	ADD_CTS(ln_config_template_id, '1513', null);
	ADD_CTS(ln_config_template_id, '1514', '0');
	ADD_CTS(ln_config_template_id, '1515', null);
	ADD_CTS(ln_config_template_id, '1516', '0');
	ADD_CTS(ln_config_template_id, '1517', null);
	ADD_CTS(ln_config_template_id, '1518', '0');	
	ADD_CTS(ln_config_template_id, '1519', null);
	ADD_CTS(ln_config_template_id, '1520', '0');
	ADD_CTS(ln_config_template_id, '1521', null);
	ADD_CTS(ln_config_template_id, '1522', '30');
	ADD_CTS(ln_config_template_id, '1523', '100');
	ADD_CTS(ln_config_template_id, '1524', '2');
	ADD_CTS(ln_config_template_id, '1525', '10');
	ADD_CTS(ln_config_template_id, '1526', '25');
	
	COMMIT;
END;
/
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/releases/REL_Edge_Update_R55/R55.USADB.USAT465.sql?revision=HEAD
-- USAT 465 remove access to MAX AUTH AMOUNT config setting from USA Live
UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET CUSTOMER_EDITABLE = NULL WHERE DEVICE_SETTING_PARAMETER_CD = 'MAX_AUTH_AMOUNT' AND DEVICE_TYPE_ID IS NOT NULL AND CUSTOMER_EDITABLE IS NOT NULL;

COMMIT;
/
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/releases/REL_Edge_Update_R55/R55.USADB.USA435.sql?revision=HEAD
-- add the new setting to enable or disable the funtionality
INSERT INTO ENGINE.APP_SETTING (APP_SETTING_CD,APP_SETTING_VALUE,APP_SETTING_DESC)
SELECT
'TURNOFF_EPORT_QS_FEES_AFTER_INACTIVITY_DAYS',
'Y',
'This flag is used to to automatically turn off customer’s monthly ePort Connect Fees when an ePort that is either owned or under a Quick Start lease, does not call in for +XX+ number of days'
FROM DUAL
WHERE NOT EXISTS(SELECT 1 FROM ENGINE.APP_SETTING WHERE APP_SETTING_CD = 'TURNOFF_EPORT_QS_FEES_AFTER_INACTIVITY_DAYS');

COMMIT;

--ADD new column to hold number of inactive days 
--after which the fees are shut off for ePort on
--the quick start program   
ALTER table REPORT.TERMINAL
ADD(EPORT_QS_FEE_INACTIVITY_DAYS decimal(6,0) NULL);

--ADD new column to hold indicator that this fee can be shutoff
--based on the inactive days of the device
ALTER TABLE CORP.FEES ADD (INACTIVITY_HANDLING_IND VARCHAR2(1));

--Populate new column 
--at this point on the Quick Start Service Fee and Terminal Service Fee
--can be shut off automatically
UPDATE CORP.FEES SET INACTIVITY_HANDLING_IND = CASE WHEN FEE_ID IN (1, 15) THEN 'Y' ELSE 'N' END; 

COMMIT; 

--Set the column to be nonnullable 
ALTER TABLE CORP.FEES MODIFY (INACTIVITY_HANDLING_IND VARCHAR2(1) NOT NULL);

--ADD new column to service fees to store date fee was auto shut off
ALTER TABLE CORP.SERVICE_FEES ADD (AUTO_TURNED_OFF_DATE DATE NULL);

--ADD new column to service fees to store date fee was auto turned back on
ALTER TABLE CORP.SERVICE_FEES ADD (AUTO_TURNED_ON_DATE DATE NULL);

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/CORP/TURNOFF_INACTIVE_EPORT_QS_FEES.prc?revision=1.7
CREATE OR REPLACE PROCEDURE CORP.TURNOFF_INACTIVE_EPORT_QS_FEES
IS
  	CURSOR L_CUR_T IS
	SELECT DISTINCT
	  SF.SERVICE_FEE_ID, E.LASTDIALIN_DATE
	FROM 
	  REPORT.EPORT E
	  INNER JOIN REPORT.VW_TERMINAL_EPORT TE
	  ON E.EPORT_ID = TE.EPORT_ID 
	  INNER JOIN REPORT.TERMINAL T
	  ON TE.TERMINAL_ID = T.TERMINAL_ID
	  AND T.EPORT_QS_FEE_INACTIVITY_DAYS IS NOT NULL
	  INNER JOIN CORP.SERVICE_FEES SF
	  ON TE.TERMINAL_ID = SF.TERMINAL_ID
	  AND (SF.START_DATE IS NULL OR SF.START_DATE < SYSDATE) 
	  AND (SF.END_DATE IS NULL OR SF.END_DATE > SYSDATE)
	  INNER JOIN CORP.FEES F
	  ON SF.FEE_ID = F.FEE_ID
	WHERE 
	  E.LASTDIALIN_DATE IS NOT NULL
	  AND E.LASTDIALIN_DATE < TRUNC(SYSDATE) - T.EPORT_QS_FEE_INACTIVITY_DAYS
	  AND F.INACTIVITY_HANDLING_IND = 'Y'
	  AND SF.LAST_PAYMENT IS NOT NULL
	ORDER BY 
	  SERVICE_FEE_ID;
	
	CURSOR L_CUR_R IS
	SELECT DISTINCT
	  SF.SERVICE_FEE_ID
	FROM 
	  REPORT.EPORT E
	  INNER JOIN REPORT.VW_TERMINAL_EPORT TE
	  ON E.EPORT_ID = TE.EPORT_ID 
	  INNER JOIN REPORT.TERMINAL T
	  ON TE.TERMINAL_ID = T.TERMINAL_ID
	  AND T.EPORT_QS_FEE_INACTIVITY_DAYS IS NOT NULL
	  INNER JOIN CORP.SERVICE_FEES SF
	  ON TE.TERMINAL_ID = SF.TERMINAL_ID
	  AND SF.END_DATE IS NOT NULL
	  AND SF.AUTO_TURNED_OFF_DATE IS NOT NULL
	WHERE 
	  E.LASTDIALIN_DATE IS NOT NULL
	  AND SF.AUTO_TURNED_OFF_DATE < E.LASTDIALIN_DATE
   	  AND SF.AUTO_TURNED_OFF_DATE = (SELECT MAX(TSF.AUTO_TURNED_OFF_DATE) FROM CORP.SERVICE_FEES TSF WHERE TSF.TERMINAL_ID = TE.TERMINAL_ID AND TSF.FEE_ID = SF.FEE_ID) 
	  AND SF.FEE_ID NOT IN 
	  (SELECT DISTINCT
	  	 OSF.FEE_ID 
	   FROM 
	     CORP.SERVICE_FEES OSF
	   WHERE 
	     OSF.TERMINAL_ID = TE.TERMINAL_ID
	     AND (OSF.END_DATE IS NULL OR OSF.END_DATE > SYSDATE))  
	ORDER BY 
	  SERVICE_FEE_ID;
 BEGIN
	IF DBADMIN.PKG_GLOBAL.GET_APP_SETTING('TURNOFF_EPORT_QS_FEES_AFTER_INACTIVITY_DAYS') = 'Y' THEN
		-- find the fees to terminate 	
		FOR L_REC IN L_CUR_T LOOP
			-- turn off the fees
			UPDATE CORP.SERVICE_FEES 
			SET END_DATE = L_REC.LASTDIALIN_DATE, AUTO_TURNED_OFF_DATE = L_REC.LASTDIALIN_DATE
			WHERE SERVICE_FEE_ID = L_REC.SERVICE_FEE_ID; 
			COMMIT;
		END LOOP;
		
		-- find the fees to reinstate 	
		FOR L_REC IN L_CUR_R LOOP
		    --add new record copying old record with new id and start date
		    INSERT INTO CORP.SERVICE_FEES(
				 SERVICE_FEE_ID, 
				 TERMINAL_ID, 
				 FEE_ID, 
				 FEE_AMOUNT, 
				 FREQUENCY_ID,  
				 START_DATE, 
				 FEE_PERCENT, 
				 NO_TRIGGER_EVENT_FLAG, 
				 ORIGINAL_LICENSE_ID,
				 GRACE_PERIOD_DATE,
				 LAST_PAYMENT,
				 AUTO_TURNED_ON_DATE)
             SELECT
				 CORP.SERVICE_FEE_SEQ.NEXTVAL, 
				 TERMINAL_ID, 
				 FEE_ID, 
				 FEE_AMOUNT, 
				 FREQUENCY_ID,  
				 SYSDATE, 
				 FEE_PERCENT, 
				 NO_TRIGGER_EVENT_FLAG, 
				 ORIGINAL_LICENSE_ID,
				 SYSDATE,
				 TRUNC(SYSDATE, 'DD'),
				 SYSDATE
			 FROM CORP.SERVICE_FEES OSF
			 WHERE OSF.SERVICE_FEE_ID = L_REC.SERVICE_FEE_ID;
			 COMMIT;
		END LOOP;		
	END IF;
	
END TURNOFF_INACTIVE_EPORT_QS_FEES;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/releases/REL_Edge_Update_R55/R55.USADB.USAT435.2.sql?revision=HEAD
--Schedule DBA Jobs to run CORP.TURNOFF_INACTIVE_EPORT_QS_FEES to run daily in the early morning:
BEGIN
DBMS_SCHEDULER.CREATE_JOB (
   'TURNOFF_INACTIVE_EPORT_QS_FEES',
   'STORED_PROCEDURE',
   'CORP.TURNOFF_INACTIVE_EPORT_QS_FEES',
   0,
   NULL,
   'FREQ=DAILY; BYHOUR=5; BYMINUTE=17;',
   NULL,
   'DEFAULT_JOB_CLASS',
   TRUE);
END;
/
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/PKG_CUSTOMER_MANAGEMENT.psk?revision=1.46
create or replace PACKAGE REPORT.PKG_CUSTOMER_MANAGEMENT AS
    TYPE VARCHAR2_SET IS TABLE OF CHAR(1) INDEX BY VARCHAR2(4000);
    FUNCTION CREATE_DEFAULT_COLUMN_SET
        RETURN VARCHAR2_SET;

    PROCEDURE CREATE_TERMINAL(
        pn_terminal_id OUT REPORT.TERMINAL.TERMINAL_ID%TYPE,
        pv_device_serial_cd IN REPORT.EPORT.EPORT_SERIAL_NUM%TYPE,
        pn_dealer_id IN CORP.DEALER.DEALER_ID%TYPE,
        pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
        pn_machine_id IN REPORT.TERMINAL.MACHINE_ID%TYPE,
        pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
        pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
        pn_region_id IN REPORT.REGION.REGION_ID%TYPE,
        pv_region_name IN REPORT.REGION.REGION_NAME%TYPE,
        pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
        pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
        pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
        pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
        pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
        pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
        pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
        pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
        pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_pc_id IN REPORT.TERMINAL.PRIMARY_CONTACT_ID%TYPE,
        pn_sc_id IN REPORT.TERMINAL.SECONDARY_CONTACT_ID%TYPE,
        pn_location_type_id IN REPORT.LOCATION.LOCATION_TYPE_ID%TYPE,
        pv_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
        pn_product_type_id IN REPORT.TERMINAL.PRODUCT_TYPE_ID%TYPE,
        pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
        pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
        pn_avg_amt IN REPORT.TERMINAL.AVG_TRANS_AMT%TYPE,
        pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
        pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
        pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
        pn_vends REPORT.TERMINAL.VENDS%TYPE,
        pv_term_var_1 IN REPORT.TERMINAL.TERM_VAR_1%TYPE,
        pv_term_var_2 IN REPORT.TERMINAL.TERM_VAR_2%TYPE,
        pv_term_var_3 IN REPORT.TERMINAL.TERM_VAR_3%TYPE,
        pv_term_var_4 IN REPORT.TERMINAL.TERM_VAR_4%TYPE,
        pv_term_var_5 IN REPORT.TERMINAL.TERM_VAR_5%TYPE,
        pv_term_var_6 IN REPORT.TERMINAL.TERM_VAR_6%TYPE,
        pv_term_var_7 IN REPORT.TERMINAL.TERM_VAR_7%TYPE,
        pv_term_var_8 IN REPORT.TERMINAL.TERM_VAR_8%TYPE,
        pd_activate_date IN REPORT.TERMINAL_EPORT.START_DATE%TYPE DEFAULT SYSDATE - 30,
        pn_fee_grace_days IN NUMBER DEFAULT 60,
        pv_doing_business_as IN REPORT.TERMINAL.DOING_BUSINESS_AS%TYPE DEFAULT NULL,
        pc_fee_no_trigger_event_flag IN CORP.SERVICE_FEES.NO_TRIGGER_EVENT_FLAG%TYPE DEFAULT NULL,
        pv_sales_order_number IN REPORT.TERMINAL.SALES_ORDER_NUMBER%TYPE DEFAULT NULL,
        pv_purchase_order_number IN REPORT.TERMINAL.PURCHASE_ORDER_NUMBER%TYPE DEFAULT NULL,
        pc_pos_environment_cd IN REPORT.TERMINAL.POS_ENVIRONMENT_CD%TYPE DEFAULT NULL,
        pv_entry_capability_cds IN REPORT.TERMINAL.ENTRY_CAPABILITY_CDS%TYPE DEFAULT NULL,
        pc_pin_capability_flag IN REPORT.TERMINAL.PIN_CAPABILITY_FLAG%TYPE DEFAULT NULL,
        pn_commission_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE DEFAULT NULL,
        pn_business_type_id IN REPORT.TERMINAL.BUSINESS_TYPE_ID%TYPE DEFAULT NULL,
       pv_customer_service_phone IN REPORT.TERMINAL.CUSTOMER_SERVICE_PHONE%TYPE DEFAULT NULL,
       pv_customer_service_email IN REPORT.TERMINAL.CUSTOMER_SERVICE_EMAIL%TYPE DEFAULT NULL,
       pv_client IN REPORT.TERMINAL.CLIENT%TYPE DEFAULT NULL);
    
    PROCEDURE CREATE_TERMINAL_MASS(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_terminal_id OUT REPORT.TERMINAL.TERMINAL_ID%TYPE,
        pv_device_serial_cd IN REPORT.EPORT.EPORT_SERIAL_NUM%TYPE,
        pn_dealer_id IN CORP.DEALER.DEALER_ID%TYPE,
        pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
        pv_machine_make REPORT.MACHINE.MAKE%TYPE,
        pv_machine_model REPORT.MACHINE.MODEL%TYPE,
        pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
        pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
        pv_region_name IN REPORT.REGION.REGION_NAME%TYPE,
        pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
        pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
        pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
        pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
        pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
        pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
        pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
        pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
        pv_location_type_name REPORT.LOCATION_TYPE.LOCATION_TYPE_NAME%TYPE,
        pn_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
        pv_product_type_name REPORT.PRODUCT_TYPE.PRODUCT_TYPE_NAME%TYPE,
        pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
        pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
        pn_avg_amt IN REPORT.TERMINAL.AVG_TRANS_AMT%TYPE,
        pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
        pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
        pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
        pn_vends REPORT.TERMINAL.VENDS%TYPE,
        pv_term_var_1 REPORT.TERMINAL.term_var_1%TYPE,
        pv_term_var_2 REPORT.TERMINAL.term_var_2%TYPE,
        pv_term_var_3 REPORT.TERMINAL.term_var_3%TYPE,
        pv_term_var_4 REPORT.TERMINAL.term_var_4%TYPE,
        pv_term_var_5 REPORT.TERMINAL.term_var_5%TYPE,
        pv_term_var_6 REPORT.TERMINAL.term_var_6%TYPE,
        pv_term_var_7 REPORT.TERMINAL.term_var_7%TYPE,
        pv_term_var_8 REPORT.TERMINAL.term_var_8%TYPE,
        pd_activate_date IN REPORT.TERMINAL_EPORT.START_DATE%TYPE DEFAULT SYSDATE - 30,
        pn_fee_grace_days IN NUMBER DEFAULT 60,
        pc_keep_existing_data IN CHAR DEFAULT 'N',
        pc_override_payment_schedule IN CHAR DEFAULT 'N',
        pv_doing_business_as IN REPORT.TERMINAL.DOING_BUSINESS_AS%TYPE DEFAULT NULL,
        pc_fee_no_trigger_event_flag IN CORP.SERVICE_FEES.NO_TRIGGER_EVENT_FLAG%TYPE DEFAULT NULL,
        pv_sales_order_number IN REPORT.TERMINAL.SALES_ORDER_NUMBER%TYPE DEFAULT NULL,
        pv_purchase_order_number IN REPORT.TERMINAL.PURCHASE_ORDER_NUMBER%TYPE DEFAULT NULL,
        pc_pos_environment_cd IN REPORT.TERMINAL.POS_ENVIRONMENT_CD%TYPE DEFAULT NULL,
        pv_entry_capability_cds IN REPORT.TERMINAL.ENTRY_CAPABILITY_CDS%TYPE DEFAULT NULL,
        pc_pin_capability_flag IN REPORT.TERMINAL.PIN_CAPABILITY_FLAG%TYPE DEFAULT NULL,
        pn_commission_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE DEFAULT NULL,
        pv_business_type_name IN REPORT.BUSINESS_TYPE.BUSINESS_TYPE_NAME%TYPE DEFAULT NULL,
       pv_customer_service_phone IN REPORT.TERMINAL.CUSTOMER_SERVICE_PHONE%TYPE DEFAULT NULL,
       pv_customer_service_email IN REPORT.TERMINAL.CUSTOMER_SERVICE_EMAIL%TYPE DEFAULT NULL,
       pv_client IN REPORT.TERMINAL.CLIENT%TYPE DEFAULT NULL,
       pn_fee_swap_terminal_id IN REPORT.TERMINAL.TERMINAL_ID%TYPE DEFAULT NULL,
       pn_master_user_id REPORT.USER_LOGIN.USER_ID%TYPE DEFAULT NULL,
       pv_activate_detail REPORT.TERMINAL_EPORT.ACTIVATE_DETAIL%TYPE DEFAULT NULL,
       pn_activate_detail_id REPORT.TERMINAL_EPORT.ACTIVATE_DETAIL_ID%TYPE DEFAULT NULL);
        
    PROCEDURE UPDATE_TERMINAL(
       pn_terminal_id IN REPORT.TERMINAL.TERMINAL_ID%TYPE,
       pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
       pn_machine_id IN REPORT.TERMINAL.MACHINE_ID%TYPE,
       pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
       pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
       pn_region_id IN REPORT.REGION.REGION_ID%TYPE,
       pv_region_name IN REPORT.REGION.REGION_NAME%TYPE,
       pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
       pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
       pn_address_id REPORT.TERMINAL_ADDR.ADDRESS_ID%TYPE,
       pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
       pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
       pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
       pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
       pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
       pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
       pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
       pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
       pn_pc_id IN REPORT.TERMINAL.PRIMARY_CONTACT_ID%TYPE,
       pn_sc_id IN REPORT.TERMINAL.SECONDARY_CONTACT_ID%TYPE,
       pn_location_type_id IN REPORT.LOCATION.LOCATION_TYPE_ID%TYPE,
       pn_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
       pn_product_type_id IN REPORT.TERMINAL.PRODUCT_TYPE_ID%TYPE,
       pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
       pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
       pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
       pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
       pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
       pn_vends REPORT.TERMINAL.VENDS%TYPE,
       pv_term_var_1 REPORT.TERMINAL.term_var_1%TYPE,
       pv_term_var_2 REPORT.TERMINAL.term_var_2%TYPE,
       pv_term_var_3 REPORT.TERMINAL.term_var_3%TYPE,
       pv_term_var_4 REPORT.TERMINAL.term_var_4%TYPE,
       pv_term_var_5 REPORT.TERMINAL.term_var_5%TYPE,
       pv_term_var_6 REPORT.TERMINAL.term_var_6%TYPE,
       pv_term_var_7 REPORT.TERMINAL.term_var_7%TYPE,
       pv_term_var_8 REPORT.TERMINAL.term_var_8%TYPE,
       pv_doing_business_as IN REPORT.TERMINAL.DOING_BUSINESS_AS%TYPE DEFAULT NULL,
       pc_pos_environment_cd IN REPORT.TERMINAL.POS_ENVIRONMENT_CD%TYPE DEFAULT NULL,
       pv_entry_capability_cds IN REPORT.TERMINAL.ENTRY_CAPABILITY_CDS%TYPE DEFAULT NULL,
       pc_pin_capability_flag IN REPORT.TERMINAL.PIN_CAPABILITY_FLAG%TYPE DEFAULT NULL,
       pn_business_type_id IN REPORT.TERMINAL.BUSINESS_TYPE_ID%TYPE DEFAULT NULL,
       pv_customer_service_phone IN REPORT.TERMINAL.CUSTOMER_SERVICE_PHONE%TYPE DEFAULT NULL,
       pv_customer_service_email IN REPORT.TERMINAL.CUSTOMER_SERVICE_EMAIL%TYPE DEFAULT NULL,
       pv_client IN REPORT.TERMINAL.CLIENT%TYPE DEFAULT NULL,
       pn_master_user_id REPORT.USER_LOGIN.USER_ID%TYPE DEFAULT NULL);
       
   PROCEDURE UPDATE_TERMINAL_MASS(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_terminal_id IN REPORT.TERMINAL.TERMINAL_ID%TYPE,
        pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
        pv_machine_make REPORT.MACHINE.MAKE%TYPE,
        pv_machine_model REPORT.MACHINE.MODEL%TYPE,
        pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
        pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
        pv_region_name IN REPORT.REGION.REGION_NAME%TYPE,
        pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
        pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
        pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
        pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
        pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
        pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
        pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
        pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
        pn_location_type_id REPORT.LOCATION_TYPE.LOCATION_TYPE_ID%TYPE,
        pv_location_type_name REPORT.LOCATION_TYPE.LOCATION_TYPE_NAME%TYPE,
        pn_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
        pv_product_type_name REPORT.PRODUCT_TYPE.PRODUCT_TYPE_NAME%TYPE,
        pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
        pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
        pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
        pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
        pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
        pn_vends REPORT.TERMINAL.VENDS%TYPE,
        pv_term_var_1 REPORT.TERMINAL.term_var_1%TYPE,
        pv_term_var_2 REPORT.TERMINAL.term_var_2%TYPE,
        pv_term_var_3 REPORT.TERMINAL.term_var_3%TYPE,
        pv_term_var_4 REPORT.TERMINAL.term_var_4%TYPE,
        pv_term_var_5 REPORT.TERMINAL.term_var_5%TYPE,
        pv_term_var_6 REPORT.TERMINAL.term_var_6%TYPE,
        pv_term_var_7 REPORT.TERMINAL.term_var_7%TYPE,
        pv_term_var_8 REPORT.TERMINAL.term_var_8%TYPE,
        pv_doing_business_as IN REPORT.TERMINAL.DOING_BUSINESS_AS%TYPE DEFAULT NULL,
        pc_pos_environment_cd IN REPORT.TERMINAL.POS_ENVIRONMENT_CD%TYPE DEFAULT NULL,
        pv_entry_capability_cds IN REPORT.TERMINAL.ENTRY_CAPABILITY_CDS%TYPE DEFAULT NULL,
        pc_pin_capability_flag IN REPORT.TERMINAL.PIN_CAPABILITY_FLAG%TYPE DEFAULT NULL,
        pv_business_type_name IN REPORT.BUSINESS_TYPE.BUSINESS_TYPE_NAME%TYPE DEFAULT NULL,
       pv_customer_service_phone IN REPORT.TERMINAL.CUSTOMER_SERVICE_PHONE%TYPE DEFAULT NULL,
       pv_customer_service_email IN REPORT.TERMINAL.CUSTOMER_SERVICE_EMAIL%TYPE DEFAULT NULL,
       pv_client IN REPORT.TERMINAL.CLIENT%TYPE DEFAULT NULL,
       pt_columns IN VARCHAR2_TABLE DEFAULT NULL,
       pn_master_user_id REPORT.USER_LOGIN.USER_ID%TYPE DEFAULT NULL);
    
    PROCEDURE CREATE_CUSTOMER_BANK(
        pn_customer_bank_id OUT CORP.CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        pn_customer_id IN CORP.CUSTOMER_BANK.CUSTOMER_ID%TYPE,
        pn_user_id IN CORP.CUSTOMER.USER_ID%TYPE,
        pv_bank_name IN CORP.CUSTOMER_BANK.BANK_NAME%TYPE,
        pv_bank_address1 IN CORP.CUSTOMER_BANK.BANK_ADDRESS%TYPE,
        pv_bank_city IN CORP.CUSTOMER_BANK.BANK_CITY%TYPE,
        pv_bank_state_cd IN CORP.CUSTOMER_BANK.BANK_STATE%TYPE,
        pv_bank_postal IN CORP.CUSTOMER_BANK.BANK_ZIP%TYPE,
        pv_bank_country_cd IN CORP.CUSTOMER_BANK.BANK_COUNTRY_CD%TYPE,
        pv_account_title IN CORP.CUSTOMER_BANK.ACCOUNT_TITLE%TYPE,
        pv_account_type IN CORP.CUSTOMER_BANK.ACCOUNT_TYPE%TYPE,
        pv_bank_routing_nbr IN CORP.CUSTOMER_BANK.BANK_ROUTING_NBR%TYPE,
        pv_bank_acct_nbr IN CORP.CUSTOMER_BANK.BANK_ACCT_NBR%TYPE,
        pv_contact_name IN CORP.CUSTOMER_BANK.CONTACT_NAME%TYPE,
        pv_contact_title IN CORP.CUSTOMER_BANK.CONTACT_TITLE%TYPE,
        pv_contact_telephone IN CORP.CUSTOMER_BANK.CONTACT_TELEPHONE%TYPE,
        pv_contact_fax IN CORP.CUSTOMER_BANK.CONTACT_FAX%TYPE,
        pv_swift_code IN CORP.CUSTOMER_BANK.SWIFT_CODE%TYPE,
        pn_pay_cycle_id IN CORP.CUSTOMER_BANK.PAY_CYCLE_ID%TYPE DEFAULT 3,
        pv_bank_tax_id IN CORP.CUSTOMER_BANK.TAX_ID_NBR%TYPE DEFAULT NULL,
        pv_billing_address1 IN CORP.CUSTOMER_ADDR.ADDRESS1%TYPE DEFAULT NULL,
        pv_billing_city IN CORP.CUSTOMER_ADDR.CITY%TYPE DEFAULT NULL,
        pv_billing_state_cd IN CORP.CUSTOMER_ADDR.STATE%TYPE DEFAULT NULL,
        pv_billing_postal IN CORP.CUSTOMER_ADDR.ZIP%TYPE DEFAULT NULL,
        pv_billing_country_cd IN CORP.CUSTOMER_ADDR.ZIP%TYPE DEFAULT NULL,
        pv_use_tax_id_for_all VARCHAR DEFAULT NULL);
        
    PROCEDURE UPDATE_REGION_NAME(
        pn_region_id REPORT.REGION.REGION_ID%TYPE,
        pv_region_name REPORT.REGION.REGION_NAME%TYPE,
        pn_customer_id REPORT.TERMINAL.CUSTOMER_ID%TYPE);
   --R44 and below     
        
   -- R46 and above     
   PROCEDURE CREATE_CAMPAIGN(
        pv_campaign_name REPORT.CAMPAIGN.CAMPAIGN_NAME%TYPE,
        pv_campaign_desc REPORT.CAMPAIGN.CAMPAIGN_DESC%TYPE,
        pn_campaign_type_id REPORT.CAMPAIGN.CAMPAIGN_TYPE_ID%TYPE,
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pd_start_date REPORT.CAMPAIGN.START_DATE%TYPE,
        pd_end_date REPORT.CAMPAIGN.END_DATE%TYPE,
        pn_discount_percent REPORT.CAMPAIGN.DISCOUNT_PERCENT%TYPE,
        pn_donation_percent REPORT.CAMPAIGN.DONATION_PERCENT%TYPE,
        pn_recur_schedule REPORT.CAMPAIGN.RECUR_SCHEDULE%TYPE,
        pn_threshold_amount REPORT.CAMPAIGN.THRESHOLD_AMOUNT%TYPE,
        pc_is_for_all_cards CHAR,
        pn_campaign_id OUT REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
        pv_promo_code IN OUT REPORT.CAMPAIGN.PROMO_CODE%TYPE,
        pv_assign_to_all_devices REPORT.CAMPAIGN.ASSIGNED_TO_ALLDEVICES%TYPE DEFAULT NULL,
        pv_assign_to_all_cards REPORT.CAMPAIGN.ASSIGNED_TO_ALLCARDS%TYPE DEFAULT NULL,
        pn_nth_vend_free_num REPORT.CAMPAIGN.NTH_VEND_FREE_NUM%TYPE DEFAULT NULL,
        pn_free_vend_max_amount REPORT.CAMPAIGN.FREE_VEND_MAX_AMOUNT%TYPE DEFAULT NULL,
        pn_assign_to_existing_cards NUMBER DEFAULT 1,
        pn_purchase_discount REPORT.CAMPAIGN.PURCHASE_DISCOUNT%TYPE DEFAULT NULL,
        pv_reuse_promo_code CHAR DEFAULT NULL,
        pv_assign_to_all_payroll_cards REPORT.CAMPAIGN.ASSIGNED_TO_ALLPAYROLLCARDS%TYPE DEFAULT NULL
        );
  --R45 and above         
  PROCEDURE EDIT_CAMPAIGN(
        pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
        pv_campaign_name REPORT.CAMPAIGN.CAMPAIGN_NAME%TYPE,
        pv_campaign_desc REPORT.CAMPAIGN.CAMPAIGN_DESC%TYPE,
        pn_campaign_type_id REPORT.CAMPAIGN.CAMPAIGN_TYPE_ID%TYPE,
        pd_start_date REPORT.CAMPAIGN.START_DATE%TYPE,
        pd_end_date REPORT.CAMPAIGN.END_DATE%TYPE,
        pn_discount_percent REPORT.CAMPAIGN.DISCOUNT_PERCENT%TYPE,
        pn_donation_percent REPORT.CAMPAIGN.DONATION_PERCENT%TYPE,
        pn_recur_schedule REPORT.CAMPAIGN.RECUR_SCHEDULE%TYPE,
        pn_threshold_amount REPORT.CAMPAIGN.THRESHOLD_AMOUNT%TYPE,
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE DEFAULT NULL,
        pv_assign_to_all_devices REPORT.CAMPAIGN.ASSIGNED_TO_ALLDEVICES%TYPE DEFAULT NULL,
        pv_assign_to_all_cards REPORT.CAMPAIGN.ASSIGNED_TO_ALLCARDS%TYPE DEFAULT NULL,
        pn_nth_vend_free_num REPORT.CAMPAIGN.NTH_VEND_FREE_NUM%TYPE DEFAULT NULL,
        pn_free_vend_max_amount REPORT.CAMPAIGN.FREE_VEND_MAX_AMOUNT%TYPE DEFAULT NULL,
        pn_purchase_discount REPORT.CAMPAIGN.PURCHASE_DISCOUNT%TYPE DEFAULT NULL,
        pv_assign_to_all_payroll_cards REPORT.CAMPAIGN.ASSIGNED_TO_ALLPAYROLLCARDS%TYPE DEFAULT NULL
        );     
  PROCEDURE DELETE_CAMPAIGN(
        pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE
        );
   
   PROCEDURE ADD_CAMP_POS_PTA_BY_POS(
        pn_pos_pta_id PSS.CAMPAIGN_POS_PTA.POS_PTA_ID%TYPE,
    pn_pos_id PSS.POS.POS_ID%TYPE,
    pc_delete_flag CHAR);
    
   PROCEDURE ADD_CAM_POS_PTA_BY_REG(
        pn_terminal_id REPORT.TERMINAL.TERMINAL_ID%TYPE,
    pn_region_id REPORT.TERMINAL_REGION.REGION_ID%TYPE);
    
    PROCEDURE ADD_CAM_POS_PTA_BY_TER_EPORT(
    pn_terminal_id REPORT.TERMINAL_EPORT.TERMINAL_ID%TYPE,
        pn_eport_id REPORT.TERMINAL_EPORT.EPORT_ID%TYPE,
        pd_start_date REPORT.TERMINAL_EPORT.START_DATE%TYPE,
    pd_end_date REPORT.TERMINAL_EPORT.END_DATE%TYPE,
    pn_terminal_eport_id REPORT.TERMINAL_EPORT.TERMINAL_EPORT_ID%TYPE,
    pc_delete_only_flag CHAR DEFAULT 'N');
    
    PROCEDURE DELETE_CAMPAIGN_ASSIGNMENT(
        pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
        pn_campaign_type_id REPORT.CAMPAIGN.CAMPAIGN_TYPE_ID%TYPE DEFAULT NULL);
    
    PROCEDURE EDIT_CAMPAIGN_CUSTOMER(
        pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
    pn_customer_id REPORT.CAMPAIGN_ASSIGNMENT.CUSTOMER_ID%TYPE,
    pv_add CHAR);
    
    PROCEDURE EDIT_CAMPAIGN_REGION(
    pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
    pn_customer_id REPORT.CAMPAIGN_ASSIGNMENT.CUSTOMER_ID%TYPE,
    pn_region_id NUMBER_TABLE,
    pv_add CHAR);
    
    PROCEDURE EDIT_CAMPAIGN_TERMINAL(
    pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
    pn_terminal_id NUMBER_TABLE,
    pv_add CHAR);
    
    PROCEDURE ADD_CAM_POS_PTA_BY_CAMPAIGN(
        pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE);
    
    PROCEDURE ADD_CARDS_TO_CAMPAIGN(
        pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
    pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
    pn_card_id NUMBER_TABLE,
    pv_card_type CHAR);
    
    PROCEDURE REMOVE_CARDS_FROM_CAMPAIGN(
    pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
    pn_card_id NUMBER_TABLE,
    pv_card_type CHAR);
    

    FUNCTION                 FIND_CAMPAIGN (
    l_terminal_id REPORT.TERMINAL_EPORT.TERMINAL_ID%TYPE,
    l_region_id REPORT.TERMINAL_REGION.REGION_ID%TYPE,
    l_customer_id REPORT.TERMINAL.CUSTOMER_ID%TYPE)
    RETURN REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE;
    
    FUNCTION                 FIND_CAMPAIGN (
    l_terminal_id REPORT.TERMINAL_EPORT.TERMINAL_ID%TYPE)
    RETURN REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE;
    
    PROCEDURE ADD_CAM_POS_PTA_BY_TER(
        pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
        pn_terminal_id REPORT.TERMINAL.TERMINAL_ID%TYPE);
    
    PROCEDURE ADD_CAM_POS_PTA_BY_TER_EPORT(
    pn_terminal_id REPORT.TERMINAL_EPORT.TERMINAL_ID%TYPE,
        pn_eport_id REPORT.TERMINAL_EPORT.EPORT_ID%TYPE,
        pd_start_date REPORT.TERMINAL_EPORT.START_DATE%TYPE,
    pd_end_date REPORT.TERMINAL_EPORT.END_DATE%TYPE);
    
    PROCEDURE ADD_CAMPAIGN_CUSTOMER(
        pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
    pn_customer_id NUMBER_TABLE);
    
    PROCEDURE ADD_CAMPAIGN_TERMINAL(
    pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
    pn_terminal_id NUMBER_TABLE,
    pn_assgined_terminal_id OUT NUMBER_TABLE);
    
END;
/
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/PKG_CUSTOMER_MANAGEMENT.pbk?revision=1.93.2.1
create or replace PACKAGE BODY REPORT.PKG_CUSTOMER_MANAGEMENT AS
    cc_default_column_set CONSTANT VARCHAR2_SET := REPORT.PKG_CUSTOMER_MANAGEMENT.CREATE_DEFAULT_COLUMN_SET();
    
    FUNCTION CREATE_DEFAULT_COLUMN_SET
        RETURN VARCHAR2_SET
    IS
        lc_default_columns VARCHAR2_TABLE := VARCHAR2_TABLE('LOCATION_NAME', 'ASSET_NUMBER', 'CLIENT', 'ADDRESS', 'LOCATION_DETAILS', 'CITY', 'STATE', 'COUNTRY', 'POSTAL', 'TIME_ZONE', 'MACHINE_MAKE', 'MACHINE_MODEL', 'TELEPHONE', 'PRODUCT_TYPE', 'LOCATION_TYPE', 'AVG_ITEM_PRICE', 'PAY_SCHEDULE', 'CURRENCY', 'REGION', 'BUSINESS_TYPE', 'DOING_BUSINESS_AS', 'CUSTOMER_SERVICE_PHONE', 'CUSTOMER_SERVICE_EMAIL', 'MOBILE', 'SIGNATURE_CAPTURE', 'ATTENDED', 'CUSTOM_1', 'CUSTOM_2', 'CUSTOM_3', 'CUSTOM_4', 'CUSTOM_5', 'CUSTOM_6', 'CUSTOM_7', 'CUSTOM_8', 'BANK_ACCT_ID','PRIMARY_CONTACT', 'SECONDARY_CONTACT');
        lt_columns_indexed VARCHAR2_SET;
    BEGIN
        FOR i IN lc_default_columns.FIRST..lc_default_columns.LAST LOOP
           lt_columns_indexed(lc_default_columns(i)) := 'Y';
        END LOOP;
        RETURN lt_columns_indexed;
    END;

    FUNCTION CONTAINS(pt_columns_indexed VARCHAR2_SET, pv_column_name VARCHAR2)
        RETURN VARCHAR2
        DETERMINISTIC
        PARALLEL_ENABLE
    IS
    BEGIN
        IF pt_columns_indexed.EXISTS(pv_column_name) THEN
            RETURN 'Y';
        ELSE
            RETURN 'N';
        END IF;
    END;
    
    PROCEDURE ADJUST_PROMO_POS_PTA(
      pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE)
    IS
      
    BEGIN
    FOR l_cur in (
      select p.pos_id, t.fee_currency_id, max (case when ca.campaign_id is null THEN 'N' ELSE 'Y' END) as needPromo
        from report.terminal t join report.campaign c on t.customer_id=c.customer_id and c.campaign_id=pn_campaign_id
        join report.terminal_eport te on t.terminal_id=te.terminal_id
        left outer join report.terminal_region tr on tr.terminal_id =t.terminal_id
        left outer join report.campaign_assignment ca on (t.terminal_id=ca.terminal_id 
        or (ca.customer_id=t.customer_id and ca.region_id=tr.region_id ) 
        or (ca.customer_id=t.customer_id and ca.region_id is null )) and ca.campaign_id=c.campaign_id    
        join report.eport e ON te.eport_id = e.eport_id
        join device.device d on d.device_serial_cd=e.eport_serial_num and d.device_active_yn_flag='Y' and not(d.device_type_id = 14 and d.device_sub_type_id IN(1,2))
        join pss.pos p on p.device_id=d.device_id
        WHERE greatest(NVL(c.START_DATE, MIN_DATE), NVL(te.START_DATE, MIN_DATE))<least(NVL(c.END_DATE, MAX_DATE),NVL(te.END_DATE, MAX_DATE))
        group by p.pos_id, t.fee_currency_id
        ) LOOP
            IF l_cur.needPromo ='Y' THEN
              PSS.PKG_POS_PTA.INSERT_PROMO_POS_PTA(l_cur.pos_id,l_cur.fee_currency_id);
            ELSE
              PSS.PKG_POS_PTA.DISABLE_PROMO_POS_PTA(l_cur.pos_id);
            END IF;
      END LOOP;
      
    END;
    
    PROCEDURE INTERNAL_CREATE_TERMINAL(
       pn_terminal_id OUT REPORT.TERMINAL.TERMINAL_ID%TYPE,
       pv_device_serial_cd IN REPORT.EPORT.EPORT_SERIAL_NUM%TYPE,
       pn_dealer_id IN CORP.DEALER.DEALER_ID%TYPE,
       pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
       pn_machine_id IN REPORT.TERMINAL.MACHINE_ID%TYPE,
       pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
       pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
       pn_region_id IN REPORT.REGION.REGION_ID%TYPE,
       pv_region_name IN REPORT.REGION.REGION_NAME%TYPE,
       pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
       pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
       pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
       pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
       pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
       pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
       pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
       pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
       pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
       pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
       pn_pc_id IN REPORT.TERMINAL.PRIMARY_CONTACT_ID%TYPE,
       pn_sc_id IN REPORT.TERMINAL.SECONDARY_CONTACT_ID%TYPE,
       pn_location_type_id IN REPORT.LOCATION.LOCATION_TYPE_ID%TYPE,
       pv_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
       pn_product_type_id IN REPORT.TERMINAL.PRODUCT_TYPE_ID%TYPE,
       pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
       pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
       pn_avg_amt IN REPORT.TERMINAL.AVG_TRANS_AMT%TYPE,
       pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
       pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
       pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
       pn_vends REPORT.TERMINAL.VENDS%TYPE,
       pv_term_var_1 IN REPORT.TERMINAL.TERM_VAR_1%TYPE,
       pv_term_var_2 IN REPORT.TERMINAL.TERM_VAR_2%TYPE,
       pv_term_var_3 IN REPORT.TERMINAL.TERM_VAR_3%TYPE,
       pv_term_var_4 IN REPORT.TERMINAL.TERM_VAR_4%TYPE,
       pv_term_var_5 IN REPORT.TERMINAL.TERM_VAR_5%TYPE,
       pv_term_var_6 IN REPORT.TERMINAL.TERM_VAR_6%TYPE,
       pv_term_var_7 IN REPORT.TERMINAL.TERM_VAR_7%TYPE,
       pv_term_var_8 IN REPORT.TERMINAL.TERM_VAR_8%TYPE,
       pd_activate_date IN REPORT.TERMINAL_EPORT.START_DATE%TYPE,
       pn_fee_grace_days IN NUMBER,
       pc_keep_existing_data IN CHAR DEFAULT 'N',
       pc_override_payment_schedule IN CHAR DEFAULT 'N',
       pv_doing_business_as IN REPORT.TERMINAL.DOING_BUSINESS_AS%TYPE DEFAULT NULL,
       pc_fee_no_trigger_event_flag IN CORP.SERVICE_FEES.NO_TRIGGER_EVENT_FLAG%TYPE DEFAULT NULL,
       pv_sales_order_number IN REPORT.TERMINAL.SALES_ORDER_NUMBER%TYPE DEFAULT NULL,
       pv_purchase_order_number IN REPORT.TERMINAL.PURCHASE_ORDER_NUMBER%TYPE DEFAULT NULL,
       pc_pos_environment_cd IN REPORT.TERMINAL.POS_ENVIRONMENT_CD%TYPE DEFAULT NULL,
       pv_entry_capability_cds IN REPORT.TERMINAL.ENTRY_CAPABILITY_CDS%TYPE DEFAULT NULL,
       pc_pin_capability_flag IN REPORT.TERMINAL.PIN_CAPABILITY_FLAG%TYPE DEFAULT NULL,
       pn_commission_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE DEFAULT NULL,
       pn_business_type_id IN REPORT.TERMINAL.BUSINESS_TYPE_ID%TYPE DEFAULT NULL,
       pv_customer_service_phone IN REPORT.TERMINAL.CUSTOMER_SERVICE_PHONE%TYPE DEFAULT NULL,
       pv_customer_service_email IN REPORT.TERMINAL.CUSTOMER_SERVICE_EMAIL%TYPE DEFAULT NULL,
       pv_client IN REPORT.TERMINAL.CLIENT%TYPE DEFAULT NULL,
       pn_fee_swap_terminal_id IN REPORT.TERMINAL.TERMINAL_ID%TYPE DEFAULT NULL,
       pn_master_user_id REPORT.USER_LOGIN.USER_ID%TYPE DEFAULT NULL,
       pv_activate_detail REPORT.TERMINAL_EPORT.ACTIVATE_DETAIL%TYPE DEFAULT NULL,
       pn_activate_detail_id REPORT.TERMINAL_EPORT.ACTIVATE_DETAIL_ID%TYPE DEFAULT NULL)
    IS
      l_eport_id REPORT.EPORT.EPORT_ID%TYPE;
      l_dev_type_id REPORT.EPORT.DEVICE_TYPE_ID%TYPE;
      l_location_id REPORT.TERMINAL.LOCATION_ID%TYPE;
      l_terminal_nbr REPORT.TERMINAL.TERMINAL_NBR%TYPE;
      l_customer_id REPORT.TERMINAL.CUSTOMER_ID%TYPE;
      l_license_id NUMBER;
      l_license_type CHAR(1);
      l_parent_license_id NUMBER;
      l_addr_id REPORT.TERMINAL_ADDR.ADDRESS_ID%TYPE;
      l_last_deactivate_date DATE;
      l_start_date DATE;
      l_cnt NUMBER;
      l_business_unit_id CORP.BUSINESS_UNIT.BUSINESS_UNIT_ID%TYPE;
      ln_new_pay_sched_id REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE;
      l_lock VARCHAR2(128);
      ln_user_id REPORT.USER_LOGIN.USER_ID%TYPE := pn_user_id;
      ln_pc_id REPORT.TERMINAL.PRIMARY_CONTACT_ID%TYPE := pn_pc_id;
      ln_currency_id CORP.COUNTRY.CURRENCY_ID%TYPE;
      ln_old_terminal_id REPORT.TERMINAL.TERMINAL_ID%TYPE;
      lc_keep_existing_data CHAR(1) := pc_keep_existing_data;
      lv_service_fee_ind REPORT.DEVICE_TYPE.SERVICE_FEE_IND%TYPE;
      lv_process_fee_ind REPORT.DEVICE_TYPE.PROCESS_FEE_IND%TYPE;
      ln_service_fee_id NUMBER;
      ln_process_fee_id NUMBER;
      ln_new_region_id REPORT.REGION.REGION_ID%TYPE;
      lv_allow_edit REPORT.VW_USER_TERMINAL.ALLOW_EDIT%TYPE;
      ln_old_customer_id REPORT.TERMINAL.CUSTOMER_ID%TYPE;
      lc_check_dealer CHAR(1) := 'Y';
      ln_parent_customer_id CORP.CUSTOMER.CUSTOMER_ID%TYPE;
      ln_commission_bank_customer CORP.CUSTOMER.CUSTOMER_ID%TYPE;
      ln_user_type_id REPORT.USER_LOGIN.USER_TYPE%TYPE;
    BEGIN
        IF pn_user_id = 0 THEN -- dms call
          ln_user_type_id :=5;
        ELSE
          select coalesce(max(user_type),8) into ln_user_type_id from report.user_login where user_id=pn_user_id;
        END IF;
        IF ln_user_type_id =8 THEN
          IF pn_master_user_id is not null THEN
            select coalesce(max(user_type),8) into ln_user_type_id from report.user_login where user_id=pn_master_user_id;
          END IF;
        END IF;
        IF lc_keep_existing_data NOT IN ('Y', 'N') THEN
            RAISE_APPLICATION_ERROR(-20207, 'Invalid pc_keep_existing_data value: ' || lc_keep_existing_data);
        END IF;
        
        IF pc_override_payment_schedule NOT IN ('Y', 'N') THEN
            RAISE_APPLICATION_ERROR(-20207, 'Invalid pc_override_payment_schedule value: ' || pc_override_payment_schedule);
        END IF;
    
        BEGIN
              SELECT TERMINAL_SEQ.NEXTVAL, E.EPORT_ID, E.DEVICE_TYPE_ID, TERMINAL_ADDR_SEQ.NEXTVAL, DT.SERVICE_FEE_IND, DT.PROCESS_FEE_IND
              INTO pn_terminal_id, l_eport_id, l_dev_type_id, l_addr_id, lv_service_fee_ind, lv_process_fee_ind
              FROM REPORT.EPORT E
              JOIN REPORT.DEVICE_TYPE DT ON E.DEVICE_TYPE_ID = DT.DEVICE_TYPE_ID
              WHERE E.EPORT_SERIAL_NUM = pv_device_serial_cd;
        EXCEPTION
              WHEN NO_DATA_FOUND THEN
                   RAISE_APPLICATION_ERROR(-20201, 'Invalid eport serial number');
              WHEN OTHERS THEN
                   RAISE;
        END;
         
        l_lock := GLOBALS_PKG.REQUEST_LOCK('EPORT.EPORT_ID', l_eport_id);
        -- CHECK THAT EPORT IS INACTIVE
        SELECT NVL(MAX(NVL(END_DATE, MAX_DATE)), MIN_DATE)
          INTO l_last_deactivate_date
          FROM REPORT.TERMINAL_EPORT 
         WHERE EPORT_ID = l_eport_id;
           
        SELECT MAX(TERMINAL_ID)
        INTO ln_old_terminal_id
        FROM (
            SELECT TERMINAL_ID
            FROM REPORT.TERMINAL_EPORT
            WHERE EPORT_ID = l_eport_id
            ORDER BY NVL(END_DATE, MAX_DATE) DESC, CREATE_DT DESC
        ) WHERE ROWNUM = 1;
         
        IF lc_keep_existing_data = 'Y' AND ln_old_terminal_id IS NULL THEN
            lc_keep_existing_data := 'N';
        END IF;
        
        IF pn_customer_bank_id <> 0 THEN
             BEGIN
                  SELECT CUSTOMER_ID INTO l_customer_id FROM CORP.CUSTOMER_BANK WHERE CUSTOMER_BANK_ID = pn_customer_bank_id;
             EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                       RAISE_APPLICATION_ERROR(-20203, 'Invalid customer bank');
                  WHEN OTHERS THEN
                       RAISE;
             END;
        ELSE
              BEGIN
                  SELECT CUSTOMER_ID INTO l_customer_id FROM USER_LOGIN WHERE USER_ID = ln_pc_id AND CUSTOMER_ID <> 0;
             EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                       RAISE_APPLICATION_ERROR(-20203, 'Invalid primary contact');
                  WHEN OTHERS THEN
                       RAISE;
             END;
        END IF;
          
        IF l_last_deactivate_date > SYSDATE THEN
              SELECT MAX(ALLOW_EDIT), MAX(CUSTOMER_ID), MAX(TERMINAL_ID)
                INTO lv_allow_edit, ln_old_customer_id, ln_old_terminal_id
                FROM (SELECT UT.ALLOW_EDIT, UT.TERMINAL_ID, T.CUSTOMER_ID
                FROM REPORT.VW_USER_TERMINAL UT
                JOIN REPORT.TERMINAL T ON UT.TERMINAL_ID = T.TERMINAL_ID
                JOIN REPORT.TERMINAL_EPORT TE ON UT.TERMINAL_ID = TE.TERMINAL_ID AND NVL(TE.END_DATE, MAX_DATE) > SYSDATE AND NVL(TE.START_DATE, MIN_DATE) <= SYSDATE
               WHERE UT.USER_ID = ln_user_id
                 AND TE.EPORT_ID = l_eport_id
               ORDER BY UT.ALLOW_EDIT DESC, UT.TERMINAL_ID DESC)
               WHERE ROWNUM = 1;
             IF lv_allow_edit = 'Y' AND REPORT.CHECK_PRIV(ln_user_id, 29)  = 'Y' AND ln_old_customer_id != l_customer_id THEN --USER CAN EDIT TERMINAL AND IS A PARTNER and is assigning to a new customer
                l_start_date := pd_activate_date;    
                UPDATE REPORT.TERMINAL_EPORT
                   SET END_DATE = l_start_date
                 WHERE EPORT_ID = l_eport_id
                   AND TERMINAL_ID = ln_old_terminal_id
                   AND NVL(END_DATE, MAX_DATE) > l_start_date;
                lc_check_dealer := 'N';
             ELSIF lv_allow_edit IS NOT NULL THEN --USER CAN VIEW TERMINAL
                RAISE_APPLICATION_ERROR(-20205, 'Eport has already been activated');
             ELSE -- THIS MAY INDICATE THAT SOMEONE ACTIVATED THE WRONG TERMINAL
                  RAISE_APPLICATION_ERROR(-20206, 'Eport already in use');
             END IF;
         ELSIF l_last_deactivate_date > pd_activate_date THEN
            l_start_date := l_last_deactivate_date;
         ELSE
            l_start_date := pd_activate_date;
         END IF;
         --VERIFY DEALER
         IF lc_check_dealer = 'Y' THEN
             SELECT COUNT(DEALER_ID) INTO l_cnt FROM CORP.DEALER_EPORT WHERE DEALER_ID = pn_dealer_id AND EPORT_ID = l_eport_id;
             IF l_cnt = 0 THEN
                  RAISE_APPLICATION_ERROR(-20202, 'Invalid dealer specified');
             END IF;
         END IF;
         --CREATE TERMINAL_NBR
         SELECT 'T0'|| TO_CHAR(TERMINAL_NBR_SEQ.NEXTVAL, 'FM99000000') INTO l_terminal_nbr FROM DUAL;
         
         --INSERT INTO TERMINAL, LOCATION, TERMINAL_ADDR         
         IF ln_user_id = 0 THEN
            SELECT MAX(USER_ID)
            INTO ln_user_id
            FROM CORP.CUSTOMER
            WHERE CUSTOMER_ID = l_customer_id;
            
            ln_pc_id := ln_user_id;
         END IF;
         
         BEGIN
              SELECT LICENSE_ID, LICENSE_TYPE, PARENT_LICENSE_ID
              INTO l_license_id, l_license_type, l_parent_license_id 
              FROM (
                SELECT L.LICENSE_ID, L.LICENSE_TYPE, L.PARENT_LICENSE_ID
                FROM CORP.CUSTOMER_LICENSE CL, CORP.LICENSE_NBR LN, CORP.LICENSE L
                WHERE CL.LICENSE_NBR = LN.LICENSE_NBR 
                AND LN.LICENSE_ID = L.LICENSE_ID
                AND CL.CUSTOMER_ID = l_customer_id 
                ORDER BY RECEIVED DESC)
              WHERE ROWNUM = 1;
         EXCEPTION
              WHEN NO_DATA_FOUND THEN
                   RAISE_APPLICATION_ERROR(-20204, 'No license agreement found for this customer');
              WHEN OTHERS THEN
                   RAISE;
         END;
         IF lc_check_dealer = 'Y' OR NVL(pn_dealer_id, -1) > 0 THEN
             BEGIN
                  SELECT LICENSE_ID, LICENSE_TYPE, PARENT_LICENSE_ID 
                    INTO l_license_id, l_license_type, l_parent_license_id 
                    FROM (SELECT l.license_id, l.license_type, l.parent_license_id
                            FROM CORP.DEALER_LICENSE dl, CORP.LICENSE l
                           WHERE dl.license_id = l.license_id
                             AND dl.DEALER_ID = pn_dealer_id
                             AND SYSDATE >= NVL(dl.START_DATE, MIN_DATE)
                             AND SYSDATE < NVL(dl.END_DATE, MAX_DATE)
                           ORDER BY dl.START_DATE DESC, l.LICENSE_ID DESC)
                   WHERE ROWNUM = 1;
             EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                       RAISE_APPLICATION_ERROR(-20204, 'No license agreement found for this dealer');
                  WHEN OTHERS THEN
                       RAISE;
             END;
         END IF;
         
         IF l_license_type = 'C' THEN
           IF NVL(pn_commission_bank_id, -1) <= 0 THEN
             RAISE_APPLICATION_ERROR(-20208, 'Reseller bank account is required for a SELL RATE license');
           END IF;
           IF l_parent_license_id IS NULL THEN
             RAISE_APPLICATION_ERROR(-20208, 'The selected SELL RATE license does not have a parent license');
           END IF;
           -- verify selected customer has a parent
             BEGIN
               SELECT P.CUSTOMER_ID
               INTO ln_parent_customer_id
               FROM CORP.CUSTOMER P
               JOIN REPORT.USER_LOGIN PU ON PU.USER_ID = P.USER_ID
               JOIN CORP.CUSTOMER C ON C.PARENT_CUSTOMER_ID = P.CUSTOMER_ID
               WHERE C.CUSTOMER_ID = l_customer_id;
             EXCEPTION
               WHEN NO_DATA_FOUND THEN
                 RAISE_APPLICATION_ERROR(-20208, 'A SELL RATE license was selected but the selected customer does not have a parent customer');
               WHEN OTHERS THEN
                 RAISE;
             END;
             -- verify the selected commission bank account is owned by the parent
             SELECT CB.CUSTOMER_ID
             INTO ln_commission_bank_customer
             FROM CORP.CUSTOMER_BANK CB
             WHERE CB.CUSTOMER_BANK_ID = pn_commission_bank_id;
             IF ln_commission_bank_customer != ln_parent_customer_id THEN
                 RAISE_APPLICATION_ERROR(-20208, 'The selected reseller bank account is not owned by the parent of this customer.');
             END IF;
         END IF;
         
         IF lc_keep_existing_data = 'N' OR pc_override_payment_schedule = 'Y' THEN
             l_business_unit_id := FIND_BUSINESS_UNIT(l_eport_id, l_customer_id);
             SELECT MAX(PAYMENT_SCHEDULE_ID)
              INTO ln_new_pay_sched_id
              FROM CORP.PAYMENT_SCHEDULE
             WHERE PAYMENT_SCHEDULE_ID = pn_pay_sched_id
               AND SELECTABLE = 'Y';
             IF l_business_unit_id IN(2,3,5) OR ln_new_pay_sched_id IS NULL THEN
                SELECT DEFAULT_PAYMENT_SCHEDULE_ID
                  INTO ln_new_pay_sched_id
                  FROM CORP.BUSINESS_UNIT
                 WHERE BUSINESS_UNIT_ID = l_business_unit_id;
             END IF;
         END IF;
         
         IF lc_keep_existing_data = 'N' THEN
             INSERT INTO TERMINAL_ADDR(ADDRESS_ID, CUSTOMER_ID, ADDRESS1, CITY, STATE, ZIP, COUNTRY_CD)
                 VALUES(l_addr_id, l_customer_id, pv_address1, pv_city, pv_state_cd, pv_postal, pv_country_cd);
             SELECT LOCATION_SEQ.NEXTVAL 
               INTO l_location_id 
               FROM DUAL;
             INSERT INTO LOCATION(LOCATION_ID, LOCATION_NAME, DESCRIPTION, ADDRESS_ID, CREATE_BY, TERMINAL_ID, EPORT_ID, LOCATION_TYPE_ID, LOCATION_TYPE_SPECIFY)
                    VALUES(l_location_id, pv_location_name, pv_location_details, l_addr_id, ln_user_id, pn_terminal_id, l_eport_id, pn_location_type_id, pv_location_type_specific);
            
             SELECT NVL(CURRENCY_ID, 1)
             INTO ln_currency_id
             FROM CORP.COUNTRY
             WHERE COUNTRY_CD = pv_country_cd;
         
             INSERT INTO TERMINAL(
                TERMINAL_ID,
                TERMINAL_NBR,
                TERMINAL_NAME,
                EPORT_ID,
                CUSTOMER_ID,
                LOCATION_ID,
                ASSET_NBR,
                MACHINE_ID,
                TELEPHONE,
                PREFIX,
                PRODUCT_TYPE_ID,
                PRODUCT_TYPE_SPECIFY,
                PRIMARY_CONTACT_ID,
                SECONDARY_CONTACT_ID,
                AUTHORIZATION_MODE,
                AVG_TRANS_AMT,
                TIME_ZONE_ID,
                DEX_DATA,
                RECEIPT,
                VENDS,
                TERM_VAR_1,
                TERM_VAR_2,
                TERM_VAR_3,
                TERM_VAR_4,
                TERM_VAR_5,
                TERM_VAR_6,
                TERM_VAR_7,
                TERM_VAR_8,
                BUSINESS_UNIT_ID,
                PAYMENT_SCHEDULE_ID,
                FEE_CURRENCY_ID,
                DOING_BUSINESS_AS,
                SALES_ORDER_NUMBER,
                PURCHASE_ORDER_NUMBER,
                POS_ENVIRONMENT_CD, 
                ENTRY_CAPABILITY_CDS, 
                PIN_CAPABILITY_FLAG,
                BUSINESS_TYPE_ID,
                CUSTOMER_SERVICE_PHONE,
                CUSTOMER_SERVICE_EMAIL,
                CLIENT)
             SELECT
                pn_terminal_id,
                l_terminal_nbr,
                l_terminal_nbr,
                l_eport_id,
                l_customer_id,
                l_location_id,
                pv_asset_nbr,
                pn_machine_id,
                pv_telephone,
                pv_prefix,
                NVL(pn_product_type_id, 0),
                pn_product_type_specific,
                ln_pc_id,
                pn_sc_id,
                pc_auth_mode,
                NVL(pn_avg_amt, 0),
                pn_time_zone_id,
                pc_dex_data,
                pc_receipt,
                pn_vends,
                pv_term_var_1,
                pv_term_var_2,
                pv_term_var_3,
                pv_term_var_4,
                pv_term_var_5,
                pv_term_var_6,
                pv_term_var_7,
                pv_term_var_8,
                l_business_unit_id,
                ln_new_pay_sched_id,
                ln_currency_id,
                CASE WHEN ln_user_type_id!=8 THEN pv_doing_business_as ELSE NULL END,
                pv_sales_order_number,
                pv_purchase_order_number,
                pc_pos_environment_cd, 
                pv_entry_capability_cds,
                pc_pin_capability_flag,
                CASE WHEN ln_user_type_id!=8 THEN pn_business_type_id  ELSE NULL END,
                pv_customer_service_phone,
                pv_customer_service_email,
                pv_client
        FROM DUAL;
         ELSE
            INSERT INTO REPORT.TERMINAL_ADDR(ADDRESS_ID, CUSTOMER_ID, ADDRESS1, CITY, STATE, ZIP, COUNTRY_CD)
                 SELECT l_addr_id, l_customer_id, ADDRESS1, CITY, STATE, ZIP, COUNTRY_CD
                   FROM REPORT.TERMINAL_ADDR TA
                   JOIN REPORT.LOCATION L ON TA.ADDRESS_ID = L.ADDRESS_ID
                  JOIN REPORT.TERMINAL T ON T.LOCATION_ID = L.LOCATION_ID
                 WHERE T.TERMINAL_ID = ln_old_terminal_id;
                 
             SELECT REPORT.LOCATION_SEQ.NEXTVAL 
               INTO l_location_id 
               FROM DUAL;
             INSERT INTO REPORT.LOCATION(LOCATION_ID, LOCATION_NAME, DESCRIPTION, ADDRESS_ID, CREATE_BY, TERMINAL_ID, EPORT_ID, LOCATION_TYPE_ID, LOCATION_TYPE_SPECIFY)
                SELECT l_location_id, L.LOCATION_NAME, L.DESCRIPTION, l_addr_id, ln_user_id, pn_terminal_id, l_eport_id, L.LOCATION_TYPE_ID, L.LOCATION_TYPE_SPECIFY
                  FROM REPORT.LOCATION L
                  JOIN REPORT.TERMINAL T ON T.LOCATION_ID = L.LOCATION_ID
                 WHERE T.TERMINAL_ID = ln_old_terminal_id;
            
            INSERT INTO REPORT.TERMINAL(
                TERMINAL_ID,
                TERMINAL_NBR,
                TERMINAL_NAME,
                EPORT_ID,
                CUSTOMER_ID,
                LOCATION_ID,
                ASSET_NBR,
                MACHINE_ID,
                TELEPHONE,
                PREFIX,
                PRODUCT_TYPE_ID,
                PRODUCT_TYPE_SPECIFY,
                PRIMARY_CONTACT_ID,
                SECONDARY_CONTACT_ID,
                AUTHORIZATION_MODE,
                AVG_TRANS_AMT,
                TIME_ZONE_ID,
                DEX_DATA,
                RECEIPT,
                VENDS,
                TERM_VAR_1,
                TERM_VAR_2,
                TERM_VAR_3,
                TERM_VAR_4,
                TERM_VAR_5,
                TERM_VAR_6,
                TERM_VAR_7,
                TERM_VAR_8,
                BUSINESS_UNIT_ID,
                PAYMENT_SCHEDULE_ID,
                FEE_CURRENCY_ID,
                DOING_BUSINESS_AS,
                SALES_ORDER_NUMBER,
                PURCHASE_ORDER_NUMBER,
                POS_ENVIRONMENT_CD, 
                ENTRY_CAPABILITY_CDS, 
                PIN_CAPABILITY_FLAG,
                BUSINESS_TYPE_ID,
                CUSTOMER_SERVICE_PHONE,
                CUSTOMER_SERVICE_EMAIL,
                CLIENT)
            SELECT
                pn_terminal_id,
                l_terminal_nbr,
                l_terminal_nbr,
                l_eport_id,
                l_customer_id,
                l_location_id,
                ASSET_NBR,
                MACHINE_ID,
                TELEPHONE,
                PREFIX,
                PRODUCT_TYPE_ID,
                PRODUCT_TYPE_SPECIFY,
                PRIMARY_CONTACT_ID,
                SECONDARY_CONTACT_ID,
                AUTHORIZATION_MODE,
                AVG_TRANS_AMT,
                TIME_ZONE_ID,
                DEX_DATA,
                RECEIPT,
                VENDS,
                TERM_VAR_1,
                TERM_VAR_2,
                TERM_VAR_3,
                TERM_VAR_4,
                TERM_VAR_5,
                TERM_VAR_6,
                TERM_VAR_7,
                TERM_VAR_8,
                BUSINESS_UNIT_ID,
                DECODE(pc_override_payment_schedule, 'N', PAYMENT_SCHEDULE_ID, ln_new_pay_sched_id),
                FEE_CURRENCY_ID,
                CASE WHEN ln_user_type_id!=8 THEN DOING_BUSINESS_AS ELSE NULL END,
                SALES_ORDER_NUMBER,
                PURCHASE_ORDER_NUMBER,
                NVL(pc_pos_environment_cd, POS_ENVIRONMENT_CD), 
                NVL(pv_entry_capability_cds, ENTRY_CAPABILITY_CDS), 
                NVL(pc_pin_capability_flag, PIN_CAPABILITY_FLAG),
                CASE WHEN ln_user_type_id!=8 THEN NVL(pn_business_type_id, BUSINESS_TYPE_ID) ELSE NULL END,
                NVL(pv_customer_service_phone, CUSTOMER_SERVICE_PHONE),
                NVL(pv_customer_service_email, CUSTOMER_SERVICE_EMAIL),
                NVL(pv_client, CLIENT)
            FROM REPORT.TERMINAL
            WHERE TERMINAL_ID = ln_old_terminal_id;
         END IF;
              
        IF lc_keep_existing_data = 'N' THEN
            IF pn_region_id != -1 THEN
                BEGIN
                  SELECT REGION_ID
                    INTO ln_new_region_id 
                    FROM REPORT.REGION
                   WHERE REGION_ID = pn_region_id
                     AND CUSTOMER_ID = l_customer_id;
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        RAISE_APPLICATION_ERROR(-20213, 'Invalid region id');
                END;
            ELSE
                REPORT.GET_OR_CREATE_REGION(ln_new_region_id, pv_region_name, l_customer_id);
            END IF;
         
            IF ln_new_region_id != 0 THEN
                INSERT INTO REPORT.TERMINAL_REGION(TERMINAL_ID, REGION_ID)
                  VALUES(pn_terminal_id, ln_new_region_id); 
            END IF;
        ELSE
            INSERT INTO REPORT.TERMINAL_REGION(TERMINAL_ID, REGION_ID)
            SELECT pn_terminal_id, REGION_ID
            FROM REPORT.TERMINAL_REGION
            WHERE TERMINAL_ID = ln_old_terminal_id;
        END IF;
            
         INSERT INTO REPORT.TERMINAL_EPORT(EPORT_ID, TERMINAL_ID, START_DATE, END_DATE, ACTIVATE_DETAIL, ACTIVATE_DETAIL_ID)
            VALUES(l_eport_id, pn_terminal_id, l_start_date, NULL, pv_activate_detail, pn_activate_detail_id);
        
         --UPDATE DEALER_EPORT
         UPDATE CORP.DEALER_EPORT SET ACTIVATE_DATE = l_start_date WHERE DEALER_ID = pn_dealer_id AND EPORT_ID = l_eport_id;
         --INSERT INTO USER_TERMINAL
         INSERT INTO USER_TERMINAL(USER_ID, TERMINAL_ID, ALLOW_EDIT)
         /* A much faster way - BSK 04/10/2007
              SELECT USER_ID, pn_terminal_id, 'Y' FROM USER_LOGIN WHERE CAN_ADMIN_USER(ln_user_id, USER_ID) = 'Y'; */
             SELECT USER_ID, pn_terminal_id, 'Y'
               FROM REPORT.USER_LOGIN u
               CONNECT BY PRIOR u.admin_id = u.user_id
               START WITH u.user_id = ln_user_id
             UNION
             SELECT a.USER_ID, pn_terminal_id, 'Y'
               FROM REPORT.USER_LOGIN a
              INNER JOIN REPORT.USER_PRIVS up
                 ON a.USER_ID = up.user_id
              WHERE a.CUSTOMER_ID = l_customer_id
                AND up.priv_id IN(5,8);
    
         --INSERT INTO CORP.CUSTOMER_BANK_TERMINAL
         IF pn_customer_bank_id <> 0 THEN
             INSERT INTO CORP.CUSTOMER_BANK_TERMINAL(TERMINAL_ID, CUSTOMER_BANK_ID, START_DATE)
               VALUES(pn_terminal_id, pn_customer_bank_id, NULL); --l_start_date); : Use NULL (or MIN_DATE) instead
         END IF;
         
         IF pn_fee_swap_terminal_id IS NULL THEN
         
         IF lv_service_fee_ind = 'Y' THEN
             FOR l_cur in (SELECT lsf.FEE_ID as fee_id,
                          DECODE(lsf.FEE_ID, 8, 0, lsf.AMOUNT) as amount, 
                          lsf.FREQUENCY_ID as frequency_id,
                           CASE 
                              WHEN F.INITIATION_TYPE_CD IN('G', 'R') AND l_start_date + pn_fee_grace_days > SYSDATE THEN NULL
                              WHEN lsf.START_IMMEDIATELY_FLAG = 'Y' THEN l_start_date
                              ELSE NULL
                           END as last_payment,
                           DECODE(lsf.FEE_ID, 8, lsf.AMOUNT, NULL) as fee_percent,
                           CASE WHEN F.INITIATION_TYPE_CD IN('G', 'R') THEN l_start_date + pn_fee_grace_days END as grace_period,
                           lsf.INACTIVE_FEE_AMOUNT,
                           CASE WHEN F.INITIATION_TYPE_CD = 'R' AND lsf.INACTIVE_MONTHS_REMAINING IS NULL THEN 3 ELSE lsf.INACTIVE_MONTHS_REMAINING END INACTIVE_MONTHS_REMAINING
                      FROM CORP.LICENSE_SERVICE_FEES lsf
                      JOIN CORP.FEES F ON lsf.FEE_ID = F.FEE_ID
                     WHERE lsf.LICENSE_ID = l_license_id
                       AND (pc_dex_data IN('A', 'D') OR lsf.FEE_ID != 4)) 
             LOOP
                 SELECT CORP.SERVICE_FEE_SEQ.NEXTVAL INTO ln_service_fee_id FROM dual;
                 INSERT INTO CORP.SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID, FEE_AMOUNT, FREQUENCY_ID, LAST_PAYMENT, START_DATE, FEE_PERCENT, GRACE_PERIOD_DATE, NO_TRIGGER_EVENT_FLAG, ORIGINAL_LICENSE_ID, INACTIVE_FEE_AMOUNT, INACTIVE_MONTHS_REMAINING)
                 VALUES (ln_service_fee_id, pn_terminal_id, l_cur.fee_id, l_cur.amount, l_cur.frequency_id, l_cur.last_payment, l_start_date, l_cur.fee_percent, l_cur.grace_period, pc_fee_no_trigger_event_flag, l_license_id, l_cur.INACTIVE_FEE_AMOUNT, l_cur.INACTIVE_MONTHS_REMAINING);
                 IF l_license_type = 'C' THEN
                     INSERT INTO CORP.SERVICE_FEE_COMMISSION(SERVICE_FEE_ID, CUSTOMER_BANK_ID, BUY_AMOUNT, SELL_AMOUNT, COMMISSION_AMOUNT)
                     SELECT ln_service_fee_id, pn_commission_bank_id, nvl(p.amount, 0), nvl(c.amount,0), (nvl(c.amount, 0) - nvl(p.amount,0))
                       FROM corp.license_service_fees c, corp.license_service_fees p
                      WHERE c.license_id = l_license_id
                        AND c.fee_id = l_cur.fee_id
                        AND p.license_id = l_parent_license_id
                        AND p.fee_id = l_cur.fee_id;
                 END IF;
             END LOOP;
             IF pc_dex_data IN('A', 'D') THEN
                 INSERT INTO CORP.SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID, FEE_AMOUNT, FREQUENCY_ID, START_DATE, ORIGINAL_LICENSE_ID)
                     SELECT CORP.SERVICE_FEE_SEQ.NEXTVAL, pn_terminal_id, 4,
                     4.00, f.FREQUENCY_ID, l_start_date, l_license_id
                      FROM CORP.FREQUENCY f
                     WHERE f.FREQUENCY_ID = 2
                       AND NOT EXISTS(SELECT 1 
                                        FROM CORP.LICENSE_SERVICE_FEES lsf 
                                       WHERE lsf.LICENSE_ID = l_license_id
                                         AND lsf.FEE_ID = 4);
             END IF;
         END IF;
         
         IF lv_process_fee_ind = 'Y' THEN
             FOR l_cur in (SELECT TRANS_TYPE_ID, FEE_PERCENT, FEE_AMOUNT, MIN_AMOUNT
                             FROM CORP.LICENSE_PROCESS_FEES 
                            WHERE LICENSE_ID = l_license_id) 
             LOOP
               SELECT CORP.PROCESS_FEE_SEQ.NEXTVAL INTO ln_process_fee_id FROM dual;
               INSERT INTO CORP.PROCESS_FEES(PROCESS_FEE_ID, TERMINAL_ID, TRANS_TYPE_ID, FEE_PERCENT, FEE_AMOUNT, MIN_AMOUNT, START_DATE, ORIGINAL_LICENSE_ID)
               VALUES (ln_process_fee_id, pn_terminal_id, l_cur.TRANS_TYPE_ID, l_cur.FEE_PERCENT, l_cur.FEE_AMOUNT, l_cur.MIN_AMOUNT, null, l_license_id);
               IF l_license_type = 'C' THEN
                 INSERT INTO CORP.PROCESS_FEE_COMMISSION(PROCESS_FEE_ID, CUSTOMER_BANK_ID, 
                  BUY_AMOUNT, BUY_PERCENT, BUY_MIN,
                  SELL_AMOUNT, SELL_PERCENT, SELL_MIN,
                  COMMISSION_AMOUNT, COMMISSION_PERCENT, COMMISSION_MIN)
                 SELECT ln_process_fee_id, pn_commission_bank_id, 
                        nvl(p.fee_amount,0), nvl(p.fee_percent,0), nvl(p.min_amount,0),
                        nvl(c.fee_amount,0), nvl(c.fee_percent,0), nvl(c.min_amount,0),
                        nvl(c.fee_amount,0) - nvl(p.fee_amount,0),
                        nvl(c.fee_percent,0) - nvl(p.fee_percent,0),
                        nvl(c.min_amount,0) - nvl(p.min_amount,0)
                   FROM corp.license_process_fees c, corp.license_process_fees p
                  WHERE c.license_id = l_license_id
                    AND c.trans_type_id = l_cur.TRANS_TYPE_ID
                    AND p.license_id = l_parent_license_id
                    AND p.trans_type_id = l_cur.TRANS_TYPE_ID;
               END IF;
             END LOOP;
         END IF;
      ELSE -- pc_fee_swap_terminal_id IS NULL
       
       insert all 
       when 1=1 then
       into corp.service_fees (terminal_id, fee_id, fee_amount, frequency_id, last_payment, start_date, end_date, service_fee_id, fee_percent, 
                        first_payment, grace_period_date, triggering_date, no_trigger_event_flag, original_license_id, 
                        inactive_fee_amount, inactive_months_remaining)
       values (pn_terminal_id, fee_id, fee_amount, frequency_id, last_payment, start_date, end_date, CORP.service_FEE_SEQ.NEXTVAL, fee_percent, 
                        first_payment, grace_period_date, triggering_date, no_trigger_event_flag, original_license_id, 
                        inactive_fee_amount, inactive_months_remaining)
       when service_fee_commission_id is not null then
       into corp.service_fee_commission (SERVICE_FEE_ID, CUSTOMER_BANK_ID, BUY_AMOUNT, SELL_AMOUNT, COMMISSION_AMOUNT)
       values (CORP.service_FEE_SEQ.NEXTVAL, customer_bank_id, buy_amount, sell_amount, commission_amount)
       select sf.fee_id, sf.fee_amount, sf.frequency_id, sf.last_payment, sf.start_date, sf.end_date, sf.fee_percent, 
                        sf.first_payment, sf.grace_period_date, sf.triggering_date, sf.no_trigger_event_flag, sf.original_license_id, 
                        sf.inactive_fee_amount, sf.inactive_months_remaining, sfc.service_fee_commission_id, sfc.customer_bank_id, sfc.buy_amount, sfc.sell_amount, sfc.commission_amount
       from corp.service_fees sf
       left join corp.service_fee_commission sfc on sfc.service_fee_id = sf.service_fee_id
       where sf.terminal_id = pn_fee_swap_terminal_id;
       
       insert all 
       when 1=1 then
       into corp.process_fees (terminal_id, trans_type_id, fee_percent, start_date, end_date, process_fee_id, fee_amount, min_amount, original_license_id)
       values (pn_terminal_id, trans_type_id, fee_percent, start_date, end_date, CORP.PROCESS_FEE_SEQ.NEXTVAL, fee_amount, min_amount, original_license_id)
       when process_fee_commission_id is not null then
       into corp.process_fee_commission (PROCESS_FEE_ID, CUSTOMER_BANK_ID, BUY_AMOUNT, BUY_PERCENT, BUY_MIN, SELL_AMOUNT, SELL_PERCENT, SELL_MIN, COMMISSION_AMOUNT, 
                                         COMMISSION_PERCENT, COMMISSION_MIN)
       values (CORP.PROCESS_FEE_SEQ.NEXTVAL, customer_bank_id, buy_amount, buy_percent, buy_min, sell_amount, sell_percent, sell_min, commission_amount,
              commission_percent, commission_min)
       select pf.trans_type_id, pf.fee_percent, pf.start_date, pf.end_date, pf.fee_amount, pf.min_amount, pf.original_license_id,
              pfc.customer_bank_id, pfc.buy_amount, pfc.buy_percent, pfc.buy_min, pfc.sell_amount, pfc.sell_percent, pfc.sell_min, pfc.commission_amount,
              pfc.commission_percent, pfc.commission_min, pfc.process_fee_commission_id
       from corp.process_fees pf
       left join corp.process_fee_commission pfc on pfc.process_fee_id = pf.process_fee_id
       where pf.terminal_id = pn_fee_swap_terminal_id;

      END IF;
    END;

    PROCEDURE INTERNAL_UPDATE_TERMINAL(
       pn_terminal_id IN REPORT.TERMINAL.TERMINAL_ID%TYPE,
       pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
       pn_machine_id IN REPORT.TERMINAL.MACHINE_ID%TYPE,
       pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
       pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
       pn_region_id IN REPORT.REGION.REGION_ID%TYPE,
       pv_region_name IN REPORT.REGION.REGION_NAME%TYPE,
       pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
       pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
       pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
       pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
       pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
       pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
       pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
       pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
       pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
       pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
       pn_pc_id IN REPORT.TERMINAL.PRIMARY_CONTACT_ID%TYPE,
       pn_sc_id IN REPORT.TERMINAL.SECONDARY_CONTACT_ID%TYPE,
       pn_location_type_id IN REPORT.LOCATION.LOCATION_TYPE_ID%TYPE,
       pn_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
       pn_product_type_id IN REPORT.TERMINAL.PRODUCT_TYPE_ID%TYPE,
       pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
       pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
       pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
       pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
       pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
       pn_vends REPORT.TERMINAL.VENDS%TYPE,
       pv_term_var_1 REPORT.TERMINAL.term_var_1%TYPE,
       pv_term_var_2 REPORT.TERMINAL.term_var_2%TYPE,
       pv_term_var_3 REPORT.TERMINAL.term_var_3%TYPE,
       pv_term_var_4 REPORT.TERMINAL.term_var_4%TYPE,
       pv_term_var_5 REPORT.TERMINAL.term_var_5%TYPE,
       pv_term_var_6 REPORT.TERMINAL.term_var_6%TYPE,
       pv_term_var_7 REPORT.TERMINAL.term_var_7%TYPE,
       pv_term_var_8 REPORT.TERMINAL.term_var_8%TYPE,
       pv_doing_business_as IN REPORT.TERMINAL.DOING_BUSINESS_AS%TYPE,
       pc_pos_environment_cd IN REPORT.TERMINAL.POS_ENVIRONMENT_CD%TYPE,
       pv_entry_capability_cds IN REPORT.TERMINAL.ENTRY_CAPABILITY_CDS%TYPE,
       pc_pin_capability_flag IN REPORT.TERMINAL.PIN_CAPABILITY_FLAG%TYPE,
       pn_business_type_id IN REPORT.TERMINAL.BUSINESS_TYPE_ID%TYPE,
       pv_customer_service_phone IN REPORT.TERMINAL.CUSTOMER_SERVICE_PHONE%TYPE,
       pv_customer_service_email IN REPORT.TERMINAL.CUSTOMER_SERVICE_EMAIL%TYPE,
       pv_client IN REPORT.TERMINAL.CLIENT%TYPE,
       pt_columns_indexed VARCHAR2_SET,
       pn_master_user_id REPORT.USER_LOGIN.USER_ID%TYPE DEFAULT NULL)
    IS
      ln_location_id REPORT.TERMINAL.LOCATION_ID%TYPE;
      l_customer_id REPORT.TERMINAL.CUSTOMER_ID%TYPE;
      l_old_cb_id NUMBER;
      l_old_lname REPORT.LOCATION.LOCATION_NAME%TYPE;
      l_old_ldesc REPORT.LOCATION.DESCRIPTION%TYPE;
      ln_address_id REPORT.TERMINAL_ADDR.ADDRESS_ID%TYPE;
      l_old_dex REPORT.TERMINAL.DEX_DATA%TYPE;
      l_date DATE := SYSDATE;
      l_lock VARCHAR2(128);
      ln_business_unit_id REPORT.TERMINAL.BUSINESS_UNIT_ID%TYPE;
      ln_old_pay_sched_id REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE;
      ln_new_pay_sched_id REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE;
      ln_currency_id CORP.COUNTRY.CURRENCY_ID%TYPE;
      ln_new_region_id REPORT.REGION.REGION_ID%TYPE;
      lc_address_provided CHAR(1);
      lc_city_provided CHAR(1);
      lc_state_provided  CHAR(1);
      lc_country_provided CHAR(1);
      lc_postal_provided CHAR(1);
      lc_loc_name_provided CHAR(1);  
      lc_details_provided CHAR(1);
      lc_loc_type_provided CHAR(1);  
      lc_asset_provided CHAR(1);
      lc_machine_provided CHAR(1);
      lc_telephone_provided CHAR(1);
      lc_prod_type_provided CHAR(1);
      lc_pc_provided CHAR(1);
      lc_sc_provided CHAR(1);
      lc_timezone_provided CHAR(1);
      lc_custom_1_provided CHAR(1);
      lc_custom_2_provided CHAR(1);
      lc_custom_3_provided CHAR(1);
      lc_custom_4_provided CHAR(1);
      lc_custom_5_provided CHAR(1);
      lc_custom_6_provided CHAR(1);
      lc_custom_7_provided CHAR(1);
      lc_custom_8_provided CHAR(1);
      lc_dba_provided CHAR(1);
      lc_pos_type_provided CHAR(1);
      lc_bus_type_provided CHAR(1);
      lc_cs_phone_provided CHAR(1);
      lc_cs_email_provided CHAR(1);
      lc_client_provided CHAR(1);    
      ln_user_type_id REPORT.USER_LOGIN.USER_TYPE%TYPE;
    BEGIN
        l_lock := GLOBALS_PKG.REQUEST_LOCK('TERMINAL.TERMINAL_ID', pn_terminal_id);
        IF pn_user_id = 0 THEN 
          ln_user_type_id :=5;
        ELSE
          select coalesce(max(user_type),8) into ln_user_type_id from report.user_login where user_id=pn_user_id;
        END IF;
        IF ln_user_type_id =8 THEN
          IF pn_master_user_id is not null THEN
            select coalesce(max(user_type),8) into ln_user_type_id from report.user_login where user_id=pn_master_user_id;
          END IF;
        END IF;
        -- if a new location is named than create it
        SELECT T.LOCATION_ID, T.CUSTOMER_ID, L.LOCATION_NAME, L.DESCRIPTION, L.ADDRESS_ID, CB.CUSTOMER_BANK_ID, T.DEX_DATA, T.BUSINESS_UNIT_ID, T.PAYMENT_SCHEDULE_ID
          INTO ln_location_id, l_customer_id, l_old_lname, l_old_ldesc, ln_address_id, l_old_cb_id, l_old_dex, ln_business_unit_id, ln_old_pay_sched_id
          FROM REPORT.TERMINAL T
          LEFT OUTER JOIN REPORT.LOCATION L ON T.LOCATION_ID = L.LOCATION_ID
          LEFT OUTER JOIN CORP.VW_CURRENT_BANK_ACCT CB ON T.TERMINAL_ID = CB.TERMINAL_ID
         WHERE T.TERMINAL_ID = pn_terminal_id;
        IF pt_columns_indexed.EXISTS('LOCATION_NAME') AND EQL(l_old_lname, pv_location_name) <> -1 THEN  -- location has changed
            INSERT INTO REPORT.TERMINAL_CHANGES (TERMINAL_ID, ATTRIBUTE, OLD_VALUE, NEW_VALUE)
                   VALUES(pn_terminal_id, 'LOCATION',l_old_lname, pv_location_name);
        END IF;
        --RECORD CHANGES (SO USALIVE CAN BE UPDATED)
        IF pt_columns_indexed.EXISTS('TIME_ZONE') THEN
            DECLARE
                l_z TERMINAL.TIME_ZONE_ID%TYPE;
                l_cnt NUMBER;
            BEGIN
                  SELECT TIME_ZONE_ID
                    INTO l_z 
                    FROM REPORT.TERMINAL 
                   WHERE TERMINAL_ID = pn_terminal_id;
                 IF EQL(l_z, pn_time_zone_id) <> -1 THEN
                     UPDATE REPORT.TERMINAL_CHANGES SET NEW_VALUE = pn_time_zone_id WHERE TERMINAL_ID = pn_terminal_id
                       AND ATTRIBUTE = 'TIME_ZONE' RETURNING 1 INTO l_cnt;
                    IF NVL(l_cnt, 0) = 0 THEN
                        INSERT INTO REPORT.TERMINAL_CHANGES (TERMINAL_ID, ATTRIBUTE, OLD_VALUE, NEW_VALUE)
                       VALUES(pn_terminal_id, 'TIME_ZONE',l_z, pn_time_zone_id);
                    END IF;
                 END IF;
             END;
        END IF;
        -- UPDATE ADDRESS
        IF pt_columns_indexed.EXISTS('ADDRESS') OR pt_columns_indexed.EXISTS('CITY') OR pt_columns_indexed.EXISTS('STATE') OR pt_columns_indexed.EXISTS('COUNTRY') OR pt_columns_indexed.EXISTS('POSTAL') THEN
            IF pt_columns_indexed.EXISTS('COUNTRY') THEN
                lc_country_provided := 'Y';
            END IF;
            IF ln_address_id IS NULL THEN    -- none specified
               SELECT REPORT.TERMINAL_ADDR_SEQ.NEXTVAL 
                 INTO ln_address_id 
                 FROM DUAL;
               INSERT INTO REPORT.TERMINAL_ADDR(ADDRESS_ID, CUSTOMER_ID, ADDRESS1, CITY, STATE, ZIP, COUNTRY_CD)
                      VALUES(ln_address_id, l_customer_id, pv_address1, pv_city, pv_state_cd, pv_postal, pv_country_cd);
            ELSE
                -- SQL can handle associative arrays, so we must translate them before the SQL statement
                IF pt_columns_indexed.EXISTS('ADDRESS') THEN
                    lc_address_provided := 'Y';
                END IF;
                IF pt_columns_indexed.EXISTS('CITY') THEN
                    lc_city_provided := 'Y';
                END IF;
                IF pt_columns_indexed.EXISTS('STATE') THEN
                    lc_state_provided := 'Y';
                END IF;
                IF pt_columns_indexed.EXISTS('POSTAL') THEN
                    lc_postal_provided := 'Y';
                END IF;
            
                UPDATE REPORT.TERMINAL_ADDR 
                   SET ADDRESS1 = CASE WHEN lc_address_provided = 'Y' THEN pv_address1 ELSE ADDRESS1 END, 
                       CITY = CASE WHEN lc_city_provided = 'Y' THEN pv_city ELSE CITY END, 
                       STATE = CASE WHEN lc_state_provided = 'Y' THEN pv_state_cd ELSE STATE END, 
                       ZIP = CASE WHEN lc_postal_provided = 'Y' THEN pv_postal ELSE ZIP END, 
                       COUNTRY_CD = CASE WHEN lc_country_provided = 'Y' THEN pv_country_cd ELSE COUNTRY_CD END
                 WHERE ADDRESS_ID = ln_address_id 
                  AND ((EQL(ADDRESS1, pv_address1) = 0 AND lc_address_provided = 'Y')
                   OR (EQL(CITY, pv_city) = 0 AND lc_city_provided = 'Y') 
                   OR (EQL(STATE, pv_state_cd) = 0 AND lc_state_provided = 'Y') 
                   OR (EQL(ZIP, pv_postal) = 0 AND lc_postal_provided = 'Y') 
                   OR (EQL(COUNTRY_CD, pv_country_cd) = 0 AND lc_country_provided = 'Y'));
            END IF;
        END IF;
        IF ln_location_id IS NULL THEN
             SELECT REPORT.LOCATION_SEQ.NEXTVAL 
               INTO ln_location_id 
               FROM DUAL;
             INSERT INTO REPORT.LOCATION(LOCATION_ID, LOCATION_NAME, DESCRIPTION, ADDRESS_ID, CREATE_BY, TERMINAL_ID, LOCATION_TYPE_ID, LOCATION_TYPE_SPECIFY)
                   VALUES(ln_location_id,pv_location_name,pv_location_details,ln_address_id,pn_user_id,pn_terminal_id,pn_location_type_id,pn_location_type_specific);
        ELSE
            IF pt_columns_indexed.EXISTS('LOCATION_NAME') THEN
                lc_loc_name_provided := 'Y';
            END IF;           
            IF pt_columns_indexed.EXISTS('LOCATION_DETAILS') THEN
                lc_details_provided := 'Y';
            END IF;
            IF pt_columns_indexed.EXISTS('LOCATION_TYPE') THEN
                lc_loc_type_provided := 'Y';
            END IF;
            
            UPDATE REPORT.LOCATION L 
               SET ADDRESS_ID = ln_address_id, 
                   UPD_DATE = l_date, 
                   LOCATION_NAME = CASE WHEN lc_loc_name_provided = 'Y' THEN pv_location_name ELSE LOCATION_NAME END, 
                   DESCRIPTION = CASE WHEN lc_details_provided = 'Y' THEN pv_location_details ELSE DESCRIPTION END, 
                   LOCATION_TYPE_ID = CASE WHEN lc_loc_type_provided = 'Y' THEN pn_location_type_id ELSE LOCATION_TYPE_ID END, 
                   LOCATION_TYPE_SPECIFY = CASE WHEN lc_loc_type_provided = 'Y' THEN pn_location_type_specific ELSE LOCATION_TYPE_SPECIFY END
             WHERE LOCATION_ID = ln_location_id
               AND ((EQL(LOCATION_NAME, pv_location_name) = 0 AND lc_loc_name_provided = 'Y')
                   OR (EQL(DESCRIPTION, pv_location_details) = 0 AND lc_details_provided = 'Y')
                   OR (EQL(LOCATION_TYPE_ID, pn_location_type_id) = 0 AND lc_loc_type_provided = 'Y') 
                   OR (EQL(LOCATION_TYPE_SPECIFY, pn_location_type_specific) = 0 AND lc_loc_type_provided = 'Y'));
        END IF;
  
        IF pt_columns_indexed.EXISTS('PAY_SCHEDULE') AND pn_pay_sched_id IS NOT NULL AND pn_pay_sched_id != ln_old_pay_sched_id THEN
            SELECT MAX(PAYMENT_SCHEDULE_ID)
              INTO ln_new_pay_sched_id
              FROM CORP.PAYMENT_SCHEDULE
             WHERE PAYMENT_SCHEDULE_ID = pn_pay_sched_id
               AND SELECTABLE = 'Y';
            IF ln_new_pay_sched_id IS NULL THEN
                RAISE_APPLICATION_ERROR(-20217, 'Invalid payment schedule requested');
            ELSIF ln_business_unit_id IN(2,3,5) THEN
                RAISE_APPLICATION_ERROR(-20217, 'Not permitted to change payment schedule as requested');
            END IF;
        ELSE
            ln_new_pay_sched_id := ln_old_pay_sched_id;
        END IF;
         
        SELECT NVL(CURRENCY_ID, 1)
          INTO ln_currency_id
          FROM CORP.COUNTRY
         WHERE COUNTRY_CD = pv_country_cd;
         
        IF pt_columns_indexed.EXISTS('ASSET_NUMBER') THEN
            lc_asset_provided := 'Y';
        END IF;
        IF pt_columns_indexed.EXISTS('MACHINE_MAKE') OR pt_columns_indexed.EXISTS('MACHINE_MODEL') THEN
            lc_machine_provided := 'Y';
        END IF;
        IF pt_columns_indexed.EXISTS('TELEPHONE') THEN
            lc_telephone_provided := 'Y';
        END IF;
        IF pt_columns_indexed.EXISTS('PRODUCT_TYPE') THEN
            lc_prod_type_provided := 'Y';
        END IF;
        IF pt_columns_indexed.EXISTS('PRIMARY_CONTACT') THEN
            lc_pc_provided := 'Y';
        END IF;
        IF pt_columns_indexed.EXISTS('SECONDARY_CONTACT') THEN
            lc_sc_provided := 'Y';
        END IF;
        IF pt_columns_indexed.EXISTS('TIME_ZONE') THEN
            lc_timezone_provided := 'Y';
        END IF;
        IF pt_columns_indexed.EXISTS('CUSTOM_1') THEN
            lc_custom_1_provided := 'Y';
        END IF;
        IF pt_columns_indexed.EXISTS('CUSTOM_2') THEN
            lc_custom_2_provided := 'Y';
        END IF;
        IF pt_columns_indexed.EXISTS('CUSTOM_3') THEN
            lc_custom_3_provided := 'Y';
        END IF;
        IF pt_columns_indexed.EXISTS('CUSTOM_4') THEN
            lc_custom_4_provided := 'Y';
        END IF;
        IF pt_columns_indexed.EXISTS('CUSTOM_5') THEN
            lc_custom_5_provided := 'Y';
        END IF;
        IF pt_columns_indexed.EXISTS('CUSTOM_6') THEN
            lc_custom_6_provided := 'Y';
        END IF;
        IF pt_columns_indexed.EXISTS('CUSTOM_7') THEN
            lc_custom_7_provided := 'Y';
        END IF;
        IF pt_columns_indexed.EXISTS('CUSTOM_8') THEN
            lc_custom_8_provided := 'Y';
        END IF;
        IF pt_columns_indexed.EXISTS('DOING_BUSINESS_AS') AND ln_user_type_id!=8 THEN
            lc_dba_provided := 'Y';
        END IF;
        IF pt_columns_indexed.EXISTS('MOBILE') OR pt_columns_indexed.EXISTS('SIGNATURE_CAPTURE') THEN
            lc_pos_type_provided := 'Y';
        END IF;
        IF pt_columns_indexed.EXISTS('BUSINESS_TYPE') AND ln_user_type_id!=8 THEN
            lc_bus_type_provided := 'Y';
        END IF;
        IF pt_columns_indexed.EXISTS('CUSTOMER_SERVICE_PHONE') THEN
            lc_cs_phone_provided := 'Y';
        END IF;
        IF pt_columns_indexed.EXISTS('CUSTOMER_SERVICE_EMAIL') THEN
            lc_cs_email_provided := 'Y';
        END IF;
        IF pt_columns_indexed.EXISTS('CLIENT') THEN
            lc_client_provided := 'Y';
        END IF;
            
        UPDATE REPORT.TERMINAL T 
           SET LOCATION_ID = ln_location_id,
               ASSET_NBR = CASE WHEN lc_asset_provided = 'Y' THEN pv_asset_nbr ELSE ASSET_NBR END,  
               MACHINE_ID = CASE WHEN lc_machine_provided = 'Y' THEN pn_machine_id ELSE MACHINE_ID END, 
               TELEPHONE = CASE WHEN lc_telephone_provided = 'Y' THEN pv_telephone ELSE TELEPHONE END, 
               PREFIX = CASE WHEN lc_telephone_provided = 'Y' THEN pv_prefix ELSE PREFIX END,  
               PRODUCT_TYPE_ID = CASE WHEN lc_prod_type_provided = 'Y' THEN pn_product_type_id ELSE PRODUCT_TYPE_ID END,  
               PRODUCT_TYPE_SPECIFY = CASE WHEN lc_prod_type_provided = 'Y' THEN pn_product_type_specific ELSE PRODUCT_TYPE_SPECIFY END, 
               PRIMARY_CONTACT_ID = CASE WHEN lc_pc_provided = 'Y' THEN pn_pc_id ELSE PRIMARY_CONTACT_ID END,  
               SECONDARY_CONTACT_ID = CASE WHEN lc_sc_provided = 'Y' THEN pn_sc_id ELSE SECONDARY_CONTACT_ID END, 
               TIME_ZONE_ID = CASE WHEN lc_timezone_provided = 'Y' THEN pn_time_zone_id ELSE TIME_ZONE_ID END,  
               PAYMENT_SCHEDULE_ID = ln_new_pay_sched_id,  
               STATUS = 'U',
               TERM_VAR_1 = CASE WHEN lc_custom_1_provided = 'Y' THEN pv_term_var_1 ELSE TERM_VAR_1 END,  
               TERM_VAR_2 = CASE WHEN lc_custom_2_provided = 'Y' THEN pv_term_var_2 ELSE TERM_VAR_2 END, 
               TERM_VAR_3 = CASE WHEN lc_custom_3_provided = 'Y' THEN pv_term_var_3 ELSE TERM_VAR_3 END, 
               TERM_VAR_4 = CASE WHEN lc_custom_4_provided = 'Y' THEN pv_term_var_4 ELSE TERM_VAR_4 END, 
               TERM_VAR_5 = CASE WHEN lc_custom_5_provided = 'Y' THEN pv_term_var_5 ELSE TERM_VAR_5 END, 
               TERM_VAR_6 = CASE WHEN lc_custom_6_provided = 'Y' THEN pv_term_var_6 ELSE TERM_VAR_6 END, 
               TERM_VAR_7 = CASE WHEN lc_custom_7_provided = 'Y' THEN pv_term_var_7 ELSE TERM_VAR_7 END, 
               TERM_VAR_8 = CASE WHEN lc_custom_8_provided = 'Y' THEN pv_term_var_8 ELSE TERM_VAR_8 END, 
               FEE_CURRENCY_ID = CASE WHEN lc_country_provided = 'Y' THEN ln_currency_id ELSE FEE_CURRENCY_ID END, 
               DOING_BUSINESS_AS = CASE WHEN lc_dba_provided = 'Y' THEN pv_doing_business_as ELSE DOING_BUSINESS_AS END, 
               POS_ENVIRONMENT_CD = CASE WHEN lc_pos_type_provided = 'Y' THEN pc_pos_environment_cd ELSE POS_ENVIRONMENT_CD END,  
               ENTRY_CAPABILITY_CDS = CASE WHEN lc_pos_type_provided = 'Y' THEN pv_entry_capability_cds ELSE ENTRY_CAPABILITY_CDS END,  
               PIN_CAPABILITY_FLAG = CASE WHEN lc_pos_type_provided = 'Y' THEN pc_pin_capability_flag ELSE PIN_CAPABILITY_FLAG END, 
               BUSINESS_TYPE_ID = CASE WHEN lc_bus_type_provided = 'Y' THEN pn_business_type_id ELSE BUSINESS_TYPE_ID END, 
               CUSTOMER_SERVICE_PHONE = CASE WHEN lc_cs_phone_provided = 'Y' THEN pv_customer_service_phone ELSE CUSTOMER_SERVICE_PHONE END, 
               CUSTOMER_SERVICE_EMAIL = CASE WHEN lc_cs_email_provided = 'Y' THEN pv_customer_service_email ELSE CUSTOMER_SERVICE_EMAIL END, 
               CLIENT = CASE WHEN lc_client_provided = 'Y' THEN pv_client ELSE CLIENT END
         WHERE TERMINAL_ID = pn_terminal_id;
         
         IF pt_columns_indexed.EXISTS('REGION') THEN    
             IF pn_region_id IS NOT NULL AND pn_region_id > 0 THEN
                 -- double-check customer_id or region
                 BEGIN
                   SELECT REGION_ID
                     INTO ln_new_region_id
                     FROM REPORT.REGION
                    WHERE REGION_ID = pn_region_id
                      AND CUSTOMER_ID = l_customer_id;
                 EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        RAISE_APPLICATION_ERROR(-20218, 'Invalid region id');
                 END;
             END IF;
             IF ln_new_region_id IS NULL OR ln_new_region_id = 0 THEN
                REPORT.GET_OR_CREATE_REGION(ln_new_region_id, pv_region_name, l_customer_id);
             END IF;
             UPDATE REPORT.TERMINAL_REGION
                SET REGION_ID = ln_new_region_id
              WHERE TERMINAL_ID = pn_terminal_id;   
             IF SQL%ROWCOUNT < 1 THEN
                INSERT INTO REPORT.TERMINAL_REGION(TERMINAL_ID, REGION_ID)
                    VALUES(pn_terminal_id, ln_new_region_id);
            END IF;
        END IF;
         
        --UPDATE BANK ACCOUNT
        IF pt_columns_indexed.EXISTS('BANK_ACCT_ID') AND EQL(l_old_cb_id, pn_customer_bank_id) = 0 THEN
            DECLARE
                l_cb_date DATE;
           BEGIN
               UPDATE CORP.CUSTOMER_BANK_TERMINAL SET END_DATE = CASE WHEN END_DATE < l_date THEN END_DATE ELSE l_date END
                WHERE TERMINAL_ID = pn_terminal_id
                RETURNING MAX(CASE WHEN END_DATE < l_date THEN END_DATE ELSE l_date END) INTO l_cb_date;
               IF NVL(pn_customer_bank_id, 0) <> 0 THEN
                      INSERT INTO CORP.CUSTOMER_BANK_TERMINAL(CUSTOMER_BANK_TERMINAL_ID, TERMINAL_ID, CUSTOMER_BANK_ID, START_DATE)
                       VALUES(CORP.CUSTOMER_BANK_TERMINAL_SEQ.NEXTVAL, pn_terminal_id, pn_customer_bank_id, l_cb_date);
               END IF;
           END;
        END IF;
        /*
        IF NVL(l_old_cb_id, 0) = 0 AND NVL(pn_customer_bank_id, 0) <> 0 THEN
            DECLARE
                l_cb_date DATE;
           BEGIN
                SELECT NVL(MIN(CLOSE_DATE), l_date) INTO l_cb_date FROM TRANS WHERE TERMINAL_ID = pn_terminal_id AND CLOSE_DATE >=
                    (SELECT NVL(MAX(END_DATE), CLOSE_DATE) FROM CORP.CUSTOMER_BANK_TERMINAL
                    WHERE TERMINAL_ID = pn_terminal_id AND END_DATE > NVL(START_DATE, END_DATE));
                CORP.TERMINAL_REASSIGN_BANK_ACCOUNT(pn_terminal_id, pn_customer_bank_id, l_cb_date);
           END;
        ELSIF EQL(l_old_cb_id, pn_customer_bank_id) = 0 THEN
            CORP.TERMINAL_REASSIGN_BANK_ACCOUNT(pn_terminal_id, pn_customer_bank_id, l_date);
        END IF;
        */
        IF pt_columns_indexed.EXISTS('DEX_DATA') THEN
            IF l_old_dex NOT IN('D','A') AND pc_dex_data IN('D','A') THEN
               DECLARE
                  ln_service_fee_id CORP.SERVICE_FEES.SERVICE_FEE_ID%TYPE;
                  lc_copy CHAR(1);         
               BEGIN
                  SELECT SERVICE_FEE_ID, DECODE(END_DATE, NULL, 'N', 'Y')
                    INTO ln_service_fee_id, lc_copy
                    FROM (SELECT SERVICE_FEE_ID, END_DATE
                            FROM CORP.SERVICE_FEES
                           WHERE TERMINAL_ID = pn_terminal_id
                             AND FEE_ID = 4
                           ORDER BY NVL(END_DATE, MAX_DATE) DESC, LAST_PAYMENT DESC, SERVICE_FEE_ID DESC)
                    WHERE ROWNUM = 1;
                  IF lc_copy = 'Y' THEN
                    INSERT INTO CORP.SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID, FEE_AMOUNT, FREQUENCY_ID, LAST_PAYMENT, START_DATE, ORIGINAL_LICENSE_ID)
                       SELECT CORP.SERVICE_FEE_SEQ.NEXTVAL, pn_terminal_id, 4, FEE_AMOUNT, FREQUENCY_ID, LAST_PAYMENT, GREATEST(l_date, END_DATE), ORIGINAL_LICENSE_ID 
                         FROM CORP.SERVICE_FEES
                        WHERE SERVICE_FEE_ID = ln_service_fee_id;
                  END IF;          
               EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                       INSERT INTO CORP.SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID, FEE_AMOUNT, FREQUENCY_ID, LAST_PAYMENT, START_DATE)
                           SELECT CORP.SERVICE_FEE_SEQ.NEXTVAL, pn_terminal_id, 4, 4, 2, l_date, l_date FROM DUAL;
               END;
           ELSIF pc_dex_data NOT IN('D','A') AND l_old_dex IN('D','A') THEN
                UPDATE CORP.SERVICE_FEES SET END_DATE = l_date 
                 WHERE TERMINAL_ID = pn_terminal_id
                   AND NVL(END_DATE, l_date) >= l_date
                   AND FEE_ID = 4;
           END IF;
        END IF;
    EXCEPTION
         WHEN NO_DATA_FOUND THEN
              RAISE_APPLICATION_ERROR(-20100, 'No such terminal found');
         WHEN OTHERS THEN
             ROLLBACK;
             RAISE;
    END;
    
    PROCEDURE CREATE_TERMINAL(
        pn_terminal_id OUT REPORT.TERMINAL.TERMINAL_ID%TYPE,
        pv_device_serial_cd IN REPORT.EPORT.EPORT_SERIAL_NUM%TYPE,
        pn_dealer_id IN CORP.DEALER.DEALER_ID%TYPE,
        pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
        pn_machine_id IN REPORT.TERMINAL.MACHINE_ID%TYPE,
        pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
        pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
        pn_region_id IN REPORT.REGION.REGION_ID%TYPE,
        pv_region_name IN REPORT.REGION.REGION_NAME%TYPE,
        pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
        pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
        pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
        pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
        pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
        pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
        pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
        pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
        pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_pc_id IN REPORT.TERMINAL.PRIMARY_CONTACT_ID%TYPE,
        pn_sc_id IN REPORT.TERMINAL.SECONDARY_CONTACT_ID%TYPE,
        pn_location_type_id IN REPORT.LOCATION.LOCATION_TYPE_ID%TYPE,
        pv_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
        pn_product_type_id IN REPORT.TERMINAL.PRODUCT_TYPE_ID%TYPE,
        pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
        pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
        pn_avg_amt IN REPORT.TERMINAL.AVG_TRANS_AMT%TYPE,
        pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
        pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
        pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
        pn_vends REPORT.TERMINAL.VENDS%TYPE,
        pv_term_var_1 IN REPORT.TERMINAL.TERM_VAR_1%TYPE,
        pv_term_var_2 IN REPORT.TERMINAL.TERM_VAR_2%TYPE,
        pv_term_var_3 IN REPORT.TERMINAL.TERM_VAR_3%TYPE,
        pv_term_var_4 IN REPORT.TERMINAL.TERM_VAR_4%TYPE,
        pv_term_var_5 IN REPORT.TERMINAL.TERM_VAR_5%TYPE,
        pv_term_var_6 IN REPORT.TERMINAL.TERM_VAR_6%TYPE,
        pv_term_var_7 IN REPORT.TERMINAL.TERM_VAR_7%TYPE,
        pv_term_var_8 IN REPORT.TERMINAL.TERM_VAR_8%TYPE,
        pd_activate_date IN REPORT.TERMINAL_EPORT.START_DATE%TYPE DEFAULT SYSDATE - 30,
        pn_fee_grace_days IN NUMBER DEFAULT 60,
        pv_doing_business_as IN REPORT.TERMINAL.DOING_BUSINESS_AS%TYPE DEFAULT NULL,
        pc_fee_no_trigger_event_flag IN CORP.SERVICE_FEES.NO_TRIGGER_EVENT_FLAG%TYPE DEFAULT NULL,
        pv_sales_order_number IN REPORT.TERMINAL.SALES_ORDER_NUMBER%TYPE DEFAULT NULL,
        pv_purchase_order_number IN REPORT.TERMINAL.PURCHASE_ORDER_NUMBER%TYPE DEFAULT NULL,
        pc_pos_environment_cd IN REPORT.TERMINAL.POS_ENVIRONMENT_CD%TYPE DEFAULT NULL,
        pv_entry_capability_cds IN REPORT.TERMINAL.ENTRY_CAPABILITY_CDS%TYPE DEFAULT NULL,
        pc_pin_capability_flag IN REPORT.TERMINAL.PIN_CAPABILITY_FLAG%TYPE DEFAULT NULL,
        pn_commission_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE DEFAULT NULL,
        pn_business_type_id IN REPORT.TERMINAL.BUSINESS_TYPE_ID%TYPE DEFAULT NULL,
       pv_customer_service_phone IN REPORT.TERMINAL.CUSTOMER_SERVICE_PHONE%TYPE DEFAULT NULL,
       pv_customer_service_email IN REPORT.TERMINAL.CUSTOMER_SERVICE_EMAIL%TYPE DEFAULT NULL,
       pv_client IN REPORT.TERMINAL.CLIENT%TYPE DEFAULT NULL)
    IS
        ln_cnt PLS_INTEGER;
        lv_city REPORT.TERMINAL_ADDR.CITY%TYPE;
        lv_state_cd REPORT.TERMINAL_ADDR.STATE%TYPE;     
        ln_new_region_id REPORT.REGION.REGION_ID%TYPE;        
    BEGIN
        IF pv_state_cd IS NULL OR pv_city IS NULL THEN
            SELECT MAX(CITY), MAX(STATE_CD), MAX(CNT)
              INTO lv_city, lv_state_cd, ln_cnt
              FROM (SELECT P.CITY, S.STATE_CD, 1 CNT
                  FROM FRONT.POSTAL P 
                  JOIN CORP.STATE S ON P.STATE_ID = S.STATE_ID
                WHERE P.POSTAL_CD = pv_postal
                  AND S.COUNTRY_CD = pv_country_cd
                ORDER BY P.POSTAL_ID ASC)
             WHERE ROWNUM = 1;
        ELSE
            SELECT COUNT(*), pv_city, pv_state_cd
              INTO ln_cnt, lv_city, lv_state_cd 
              FROM CORP.STATE 
             WHERE STATE_CD = pv_state_cd
               AND COUNTRY_CD = pv_country_cd; 
        END IF; 
        IF ln_cnt = 0 THEN
            RAISE_APPLICATION_ERROR(-20220, 'Invalid state/country');
            RETURN;
        END IF;
        INTERNAL_CREATE_TERMINAL(
            pn_terminal_id,
            pv_device_serial_cd,
            pn_dealer_id,
            pv_asset_nbr,
            pn_machine_id,
            pv_telephone,
            pv_prefix,
            pn_region_id,
            pv_region_name,
            pv_location_name,
            pv_location_details,
            pv_address1,
            lv_city,
            lv_state_cd,
            pv_postal,
            pv_country_cd,
            pn_customer_bank_id,
            pn_pay_sched_id,
            pn_user_id,
            pn_pc_id,
            pn_sc_id,
            pn_location_type_id,
            pv_location_type_specific,
            pn_product_type_id,
            pn_product_type_specific,
            pc_auth_mode,
            pn_avg_amt,
            pn_time_zone_id,
            pc_dex_data,
            pc_receipt,
            pn_vends,
            pv_term_var_1,
            pv_term_var_2,
            pv_term_var_3,
            pv_term_var_4,
            pv_term_var_5,
            pv_term_var_6,
            pv_term_var_7,
            pv_term_var_8,
            pd_activate_date,
            pn_fee_grace_days,
            'N', -- pc_keep_existing_data
            'N', -- pc_override_payment_schedule    
            pv_doing_business_as,
            pc_fee_no_trigger_event_flag,
            pv_sales_order_number,
            pv_purchase_order_number,
            pc_pos_environment_cd,
            pv_entry_capability_cds,
            pc_pin_capability_flag,
            pn_commission_bank_id,
            pn_business_type_id,
            pv_customer_service_phone,
            pv_customer_service_email,
            pv_client);
    END;

    PROCEDURE UPDATE_TERMINAL(
       pn_terminal_id IN REPORT.TERMINAL.TERMINAL_ID%TYPE,
       pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
       pn_machine_id IN REPORT.TERMINAL.MACHINE_ID%TYPE,
       pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
       pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
       pn_region_id IN REPORT.REGION.REGION_ID%TYPE,
       pv_region_name IN REPORT.REGION.REGION_NAME%TYPE,
       pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
       pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
       pn_address_id REPORT.TERMINAL_ADDR.ADDRESS_ID%TYPE,
       pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
       pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
       pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
       pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
       pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
       pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
       pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
       pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
       pn_pc_id IN REPORT.TERMINAL.PRIMARY_CONTACT_ID%TYPE,
       pn_sc_id IN REPORT.TERMINAL.SECONDARY_CONTACT_ID%TYPE,
       pn_location_type_id IN REPORT.LOCATION.LOCATION_TYPE_ID%TYPE,
       pn_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
       pn_product_type_id IN REPORT.TERMINAL.PRODUCT_TYPE_ID%TYPE,
       pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
       pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
       pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
       pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
       pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
       pn_vends REPORT.TERMINAL.VENDS%TYPE,
       pv_term_var_1 REPORT.TERMINAL.term_var_1%TYPE,
       pv_term_var_2 REPORT.TERMINAL.term_var_2%TYPE,
       pv_term_var_3 REPORT.TERMINAL.term_var_3%TYPE,
       pv_term_var_4 REPORT.TERMINAL.term_var_4%TYPE,
       pv_term_var_5 REPORT.TERMINAL.term_var_5%TYPE,
       pv_term_var_6 REPORT.TERMINAL.term_var_6%TYPE,
       pv_term_var_7 REPORT.TERMINAL.term_var_7%TYPE,
       pv_term_var_8 REPORT.TERMINAL.term_var_8%TYPE,
       pv_doing_business_as IN REPORT.TERMINAL.DOING_BUSINESS_AS%TYPE DEFAULT NULL,
       pc_pos_environment_cd IN REPORT.TERMINAL.POS_ENVIRONMENT_CD%TYPE DEFAULT NULL,
       pv_entry_capability_cds IN REPORT.TERMINAL.ENTRY_CAPABILITY_CDS%TYPE DEFAULT NULL,
       pc_pin_capability_flag IN REPORT.TERMINAL.PIN_CAPABILITY_FLAG%TYPE DEFAULT NULL,
       pn_business_type_id IN REPORT.TERMINAL.BUSINESS_TYPE_ID%TYPE DEFAULT NULL,
       pv_customer_service_phone IN REPORT.TERMINAL.CUSTOMER_SERVICE_PHONE%TYPE DEFAULT NULL,
       pv_customer_service_email IN REPORT.TERMINAL.CUSTOMER_SERVICE_EMAIL%TYPE DEFAULT NULL,
       pv_client IN REPORT.TERMINAL.CLIENT%TYPE DEFAULT NULL,
       pn_master_user_id REPORT.USER_LOGIN.USER_ID%TYPE DEFAULT NULL)
    IS
        ln_cnt PLS_INTEGER;
        lv_city REPORT.TERMINAL_ADDR.CITY%TYPE;
        lv_state_cd REPORT.TERMINAL_ADDR.STATE%TYPE;   
    BEGIN
        IF pv_state_cd IS NULL OR pv_city IS NULL THEN
            SELECT MAX(CITY), MAX(STATE_CD), MAX(CNT)
              INTO lv_city, lv_state_cd, ln_cnt
              FROM (SELECT P.CITY, S.STATE_CD, 1 CNT
                  FROM FRONT.POSTAL P 
                  JOIN CORP.STATE S ON P.STATE_ID = S.STATE_ID
                WHERE P.POSTAL_CD = pv_postal
                  AND S.COUNTRY_CD = pv_country_cd
                ORDER BY P.POSTAL_ID ASC)
             WHERE ROWNUM = 1;
        ELSE
            SELECT COUNT(*), pv_city, pv_state_cd
              INTO ln_cnt, lv_city, lv_state_cd
              FROM CORP.STATE 
             WHERE STATE_CD = pv_state_cd
               AND COUNTRY_CD = pv_country_cd; 
        END IF;
        IF ln_cnt = 0 THEN
            RAISE_APPLICATION_ERROR(-20220, 'Invalid state/country');
        ELSE
            INTERNAL_UPDATE_TERMINAL(
               pn_terminal_id,
               pv_asset_nbr,
               pn_machine_id,
               pv_telephone,
               pv_prefix,
               pn_region_id,
               pv_region_name,
               pv_location_name,
               pv_location_details,
               pv_address1,
               lv_city,
               lv_state_cd,
               pv_postal,
               pv_country_cd,
               pn_customer_bank_id,
               pn_pay_sched_id,
               pn_user_id,
               pn_pc_id,
               pn_sc_id,
               pn_location_type_id,
               pn_location_type_specific,
               pn_product_type_id,
               pn_product_type_specific,
               pc_auth_mode,
               pn_time_zone_id,
               pc_dex_data,
               pc_receipt,
               pn_vends,
               pv_term_var_1,
               pv_term_var_2,
               pv_term_var_3,
               pv_term_var_4,
               pv_term_var_5,
               pv_term_var_6,
               pv_term_var_7,
               pv_term_var_8,
               pv_doing_business_as,
               pc_pos_environment_cd,
               pv_entry_capability_cds,
               pc_pin_capability_flag,
               pn_business_type_id,
               pv_customer_service_phone,
               pv_customer_service_email,
               pv_client,
               cc_default_column_set,
               pn_master_user_id);
        END IF;
    END;
        
    PROCEDURE CREATE_TERMINAL_MASS(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_terminal_id OUT REPORT.TERMINAL.TERMINAL_ID%TYPE,
        pv_device_serial_cd IN REPORT.EPORT.EPORT_SERIAL_NUM%TYPE,
        pn_dealer_id IN CORP.DEALER.DEALER_ID%TYPE,
        pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
        pv_machine_make REPORT.MACHINE.MAKE%TYPE,
        pv_machine_model REPORT.MACHINE.MODEL%TYPE,
        pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
        pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
        pv_region_name IN REPORT.REGION.REGION_NAME%TYPE,
        pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
        pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
        pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
        pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
        pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
        pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
        pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
        pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
        pv_location_type_name REPORT.LOCATION_TYPE.LOCATION_TYPE_NAME%TYPE,
        pn_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
        pv_product_type_name REPORT.PRODUCT_TYPE.PRODUCT_TYPE_NAME%TYPE,
        pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
        pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
        pn_avg_amt IN REPORT.TERMINAL.AVG_TRANS_AMT%TYPE,
        pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
        pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
        pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
        pn_vends REPORT.TERMINAL.VENDS%TYPE,
        pv_term_var_1 REPORT.TERMINAL.term_var_1%TYPE,
        pv_term_var_2 REPORT.TERMINAL.term_var_2%TYPE,
        pv_term_var_3 REPORT.TERMINAL.term_var_3%TYPE,
        pv_term_var_4 REPORT.TERMINAL.term_var_4%TYPE,
        pv_term_var_5 REPORT.TERMINAL.term_var_5%TYPE,
        pv_term_var_6 REPORT.TERMINAL.term_var_6%TYPE,
        pv_term_var_7 REPORT.TERMINAL.term_var_7%TYPE,
        pv_term_var_8 REPORT.TERMINAL.term_var_8%TYPE,
        pd_activate_date IN REPORT.TERMINAL_EPORT.START_DATE%TYPE DEFAULT SYSDATE - 30,
        pn_fee_grace_days IN NUMBER DEFAULT 60,
        pc_keep_existing_data IN CHAR DEFAULT 'N',
        pc_override_payment_schedule IN CHAR DEFAULT 'N',
        pv_doing_business_as IN REPORT.TERMINAL.DOING_BUSINESS_AS%TYPE DEFAULT NULL,
        pc_fee_no_trigger_event_flag IN CORP.SERVICE_FEES.NO_TRIGGER_EVENT_FLAG%TYPE DEFAULT NULL,
        pv_sales_order_number IN REPORT.TERMINAL.SALES_ORDER_NUMBER%TYPE DEFAULT NULL,
        pv_purchase_order_number IN REPORT.TERMINAL.PURCHASE_ORDER_NUMBER%TYPE DEFAULT NULL,
        pc_pos_environment_cd IN REPORT.TERMINAL.POS_ENVIRONMENT_CD%TYPE DEFAULT NULL,
        pv_entry_capability_cds IN REPORT.TERMINAL.ENTRY_CAPABILITY_CDS%TYPE DEFAULT NULL,
        pc_pin_capability_flag IN REPORT.TERMINAL.PIN_CAPABILITY_FLAG%TYPE DEFAULT NULL,
        pn_commission_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE DEFAULT NULL,
        pv_business_type_name IN REPORT.BUSINESS_TYPE.BUSINESS_TYPE_NAME%TYPE DEFAULT NULL,
       pv_customer_service_phone IN REPORT.TERMINAL.CUSTOMER_SERVICE_PHONE%TYPE DEFAULT NULL,
       pv_customer_service_email IN REPORT.TERMINAL.CUSTOMER_SERVICE_EMAIL%TYPE DEFAULT NULL,
       pv_client IN REPORT.TERMINAL.CLIENT%TYPE DEFAULT NULL,
       pn_fee_swap_terminal_id IN REPORT.TERMINAL.TERMINAL_ID%TYPE DEFAULT NULL,
       pn_master_user_id REPORT.USER_LOGIN.USER_ID%TYPE DEFAULT NULL,
       pv_activate_detail REPORT.TERMINAL_EPORT.ACTIVATE_DETAIL%TYPE DEFAULT NULL,
       pn_activate_detail_id REPORT.TERMINAL_EPORT.ACTIVATE_DETAIL_ID%TYPE DEFAULT NULL)
    IS
        ln_machine_id REPORT.MACHINE.MACHINE_ID%TYPE;
        ln_product_type_id REPORT.PRODUCT_TYPE.PRODUCT_TYPE_ID%TYPE;
        ln_location_type_id REPORT.LOCATION_TYPE.LOCATION_TYPE_ID%TYPE; 
        ln_cnt PLS_INTEGER;
        lv_city REPORT.TERMINAL_ADDR.CITY%TYPE;
        lv_state_cd REPORT.TERMINAL_ADDR.STATE%TYPE;     
        ln_region_id REPORT.REGION.REGION_ID%TYPE;    
        ln_business_type_id REPORT.TERMINAL.BUSINESS_TYPE_ID%TYPE;
    BEGIN
        IF pc_keep_existing_data = 'N' THEN
            BEGIN
                SELECT MAX(MACHINE_ID)
                  INTO ln_machine_id
                  FROM REPORT.MACHINE
                 WHERE UPPER(MAKE) = UPPER(pv_machine_make)
                   AND UPPER(MODEL) = UPPER(pv_machine_model)
                   AND STATUS IN('A','I');
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    REPORT.MACHINE_INS(ln_machine_id, pv_machine_make, pv_machine_model, pn_user_id);
            END;
            IF ln_machine_id IS NULL THEN
                REPORT.MACHINE_INS(ln_machine_id, pv_machine_make, pv_machine_model, pn_user_id);
            END IF;
            BEGIN
                IF pv_location_type_name IS NOT NULL AND LENGTH(TRIM(pv_location_type_name)) > 0 THEN
                    SELECT LOCATION_TYPE_ID
                      INTO ln_location_type_id
                      FROM REPORT.LOCATION_TYPE
                     WHERE UPPER(LOCATION_TYPE_NAME) = UPPER(pv_location_type_name)
                       AND STATUS IN('A','I');
                ELSE
                    ln_location_type_id := 0;
                END IF;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    RAISE_APPLICATION_ERROR(-20211, 'Invalid location type name');
            END;
            BEGIN
                SELECT PRODUCT_TYPE_ID
                  INTO ln_product_type_id
                  FROM REPORT.PRODUCT_TYPE
                 WHERE UPPER(PRODUCT_TYPE_NAME) = UPPER(pv_product_type_name)
                   AND STATUS IN('A','I');
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    RAISE_APPLICATION_ERROR(-20212, 'Invalid product type name');
            END;
            IF pv_state_cd IS NULL OR pv_city IS NULL THEN
                SELECT MAX(CITY), MAX(STATE_CD), MAX(CNT)
                  INTO lv_city, lv_state_cd, ln_cnt
                  FROM (SELECT P.CITY, S.STATE_CD, 1 CNT
                      FROM FRONT.POSTAL P 
                      JOIN CORP.STATE S ON P.STATE_ID = S.STATE_ID
                    WHERE P.POSTAL_CD = pv_postal
                      AND S.COUNTRY_CD = pv_country_cd
                    ORDER BY P.POSTAL_ID ASC)
                 WHERE ROWNUM = 1;
            ELSE
                SELECT COUNT(*), pv_city, pv_state_cd
                  INTO ln_cnt, lv_city, lv_state_cd 
                  FROM CORP.STATE 
                 WHERE STATE_CD = pv_state_cd
                   AND COUNTRY_CD = pv_country_cd; 
            END IF; 
            IF ln_cnt = 0 THEN
                RAISE_APPLICATION_ERROR(-20220, 'Invalid state/country');
                RETURN;
            END IF;
        END IF;
        IF pv_business_type_name IS NOT NULL THEN
          BEGIN
              SELECT BUSINESS_TYPE_ID
                INTO ln_business_type_id
                FROM REPORT.BUSINESS_TYPE
               WHERE UPPER(BUSINESS_TYPE_NAME) = UPPER(pv_business_type_name);
          EXCEPTION
              WHEN NO_DATA_FOUND THEN
                  RAISE_APPLICATION_ERROR(-20213, 'Invalid business type name');
          END;
        END IF;            
        INTERNAL_CREATE_TERMINAL(
               pn_terminal_id,
               pv_device_serial_cd,
               pn_dealer_id,
               pv_asset_nbr,
               ln_machine_id,
               pv_telephone,
               pv_prefix,
               -1,
               pv_region_name,
               pv_location_name,
               pv_location_details,
               pv_address1,
               lv_city,
               lv_state_cd,
               pv_postal,
               pv_country_cd,
               pn_customer_bank_id,
               pn_pay_sched_id,
               pn_user_id,
               pn_user_id,
               NULL,
               ln_location_type_id,
               pn_location_type_specific,
               ln_product_type_id,
               pn_product_type_specific,
               pc_auth_mode,
               pn_avg_amt,
               pn_time_zone_id,
               pc_dex_data,
               pc_receipt,
               pn_vends,
               pv_term_var_1,
               pv_term_var_2,
               pv_term_var_3,
               pv_term_var_4,
               pv_term_var_5,
               pv_term_var_6,
               pv_term_var_7,
               pv_term_var_8,
               pd_activate_date,
               pn_fee_grace_days,
               pc_keep_existing_data,
               pc_override_payment_schedule,
               pv_doing_business_as,
               pc_fee_no_trigger_event_flag,
               pv_sales_order_number,
               pv_purchase_order_number,
               pc_pos_environment_cd,
               pv_entry_capability_cds,
               pc_pin_capability_flag,
               pn_commission_bank_id,
               ln_business_type_id,
               pv_customer_service_phone,
               pv_customer_service_email,
               pv_client,
               pn_fee_swap_terminal_id,
               pn_master_user_id,
               pv_activate_detail,
               pn_activate_detail_id);
    END;
    
    PROCEDURE UPDATE_TERMINAL_MASS(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_terminal_id IN REPORT.TERMINAL.TERMINAL_ID%TYPE,
        pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
        pv_machine_make REPORT.MACHINE.MAKE%TYPE,
        pv_machine_model REPORT.MACHINE.MODEL%TYPE,
        pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
        pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
        pv_region_name IN REPORT.REGION.REGION_NAME%TYPE,
        pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
        pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
        pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
        pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
        pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
        pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
        pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
        pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
        pn_location_type_id REPORT.LOCATION_TYPE.LOCATION_TYPE_ID%TYPE,
        pv_location_type_name REPORT.LOCATION_TYPE.LOCATION_TYPE_NAME%TYPE,
        pn_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
        pv_product_type_name REPORT.PRODUCT_TYPE.PRODUCT_TYPE_NAME%TYPE,
        pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
        pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
        pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
        pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
        pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
        pn_vends REPORT.TERMINAL.VENDS%TYPE,
        pv_term_var_1 REPORT.TERMINAL.term_var_1%TYPE,
        pv_term_var_2 REPORT.TERMINAL.term_var_2%TYPE,
        pv_term_var_3 REPORT.TERMINAL.term_var_3%TYPE,
        pv_term_var_4 REPORT.TERMINAL.term_var_4%TYPE,
        pv_term_var_5 REPORT.TERMINAL.term_var_5%TYPE,
        pv_term_var_6 REPORT.TERMINAL.term_var_6%TYPE,
        pv_term_var_7 REPORT.TERMINAL.term_var_7%TYPE,
        pv_term_var_8 REPORT.TERMINAL.term_var_8%TYPE,
        pv_doing_business_as IN REPORT.TERMINAL.DOING_BUSINESS_AS%TYPE DEFAULT NULL,
        pc_pos_environment_cd IN REPORT.TERMINAL.POS_ENVIRONMENT_CD%TYPE DEFAULT NULL,
        pv_entry_capability_cds IN REPORT.TERMINAL.ENTRY_CAPABILITY_CDS%TYPE DEFAULT NULL,
        pc_pin_capability_flag IN REPORT.TERMINAL.PIN_CAPABILITY_FLAG%TYPE DEFAULT NULL,
        pv_business_type_name IN REPORT.BUSINESS_TYPE.BUSINESS_TYPE_NAME%TYPE DEFAULT NULL,
       pv_customer_service_phone IN REPORT.TERMINAL.CUSTOMER_SERVICE_PHONE%TYPE DEFAULT NULL,
       pv_customer_service_email IN REPORT.TERMINAL.CUSTOMER_SERVICE_EMAIL%TYPE DEFAULT NULL,
       pv_client IN REPORT.TERMINAL.CLIENT%TYPE DEFAULT NULL,
       pt_columns IN VARCHAR2_TABLE DEFAULT NULL,
       pn_master_user_id REPORT.USER_LOGIN.USER_ID%TYPE DEFAULT NULL)
    IS
        ln_machine_id REPORT.MACHINE.MACHINE_ID%TYPE;
        ln_product_type_id REPORT.PRODUCT_TYPE.PRODUCT_TYPE_ID%TYPE;
        ln_location_type_id REPORT.LOCATION_TYPE.LOCATION_TYPE_ID%TYPE;
        ln_cnt PLS_INTEGER;
        lv_city REPORT.TERMINAL_ADDR.CITY%TYPE;
        lv_state_cd REPORT.TERMINAL_ADDR.STATE%TYPE;  
        ln_region_id REPORT.REGION.REGION_ID%TYPE;
        ln_business_type_id REPORT.TERMINAL.BUSINESS_TYPE_ID%TYPE;    
        lt_columns_indexed VARCHAR2_SET;
    BEGIN
        IF pt_columns IS NULL THEN
            lt_columns_indexed := cc_default_column_set;
        ELSE
            FOR i IN pt_columns.FIRST..pt_columns.LAST LOOP
               lt_columns_indexed(pt_columns(i)) := 'Y';
            END LOOP;
        END IF;
        IF lt_columns_indexed.EXISTS('MACHINE_MAKE') AND lt_columns_indexed.EXISTS('MACHINE_MODEL') THEN
            BEGIN
                SELECT MAX(MACHINE_ID)
                  INTO ln_machine_id
                  FROM REPORT.MACHINE
                 WHERE UPPER(MAKE) = UPPER(pv_machine_make)
                   AND UPPER(MODEL) = UPPER(pv_machine_model)
                   AND STATUS IN('A','I');
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    REPORT.MACHINE_INS(ln_machine_id, pv_machine_make, pv_machine_model, pn_user_id);
            END;
            IF ln_machine_id IS NULL THEN
                REPORT.MACHINE_INS(ln_machine_id, pv_machine_make, pv_machine_model, pn_user_id);
            END IF;
        END IF;
        IF lt_columns_indexed.EXISTS('LOCATION_TYPE') THEN
            BEGIN
                IF pv_location_type_name IS NOT NULL AND LENGTH(TRIM(pv_location_type_name)) > 0 THEN
                    SELECT LOCATION_TYPE_ID
                      INTO ln_location_type_id
                      FROM REPORT.LOCATION_TYPE
                     WHERE UPPER(LOCATION_TYPE_NAME) = UPPER(pv_location_type_name)
                       AND STATUS IN('A','I');
                ELSIF pn_location_type_id IS NULL THEN
                    ln_location_type_id := 0;
                ELSE
                    ln_location_type_id := pn_location_type_id;
                END IF;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    RAISE_APPLICATION_ERROR(-20211, 'Invalid location type name');
            END;
        END IF;
        IF lt_columns_indexed.EXISTS('PRODUCT_TYPE') THEN
            BEGIN
                SELECT PRODUCT_TYPE_ID
                  INTO ln_product_type_id
                  FROM REPORT.PRODUCT_TYPE
                 WHERE UPPER(PRODUCT_TYPE_NAME) = UPPER(pv_product_type_name)
                   AND STATUS IN('A','I');
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    RAISE_APPLICATION_ERROR(-20212, 'Invalid product type name');
            END;
        END IF;
        IF lt_columns_indexed.EXISTS('BUSINESS_TYPE') AND pv_business_type_name IS NOT NULL THEN
            BEGIN
                SELECT BUSINESS_TYPE_ID
                  INTO ln_business_type_id
                  FROM REPORT.BUSINESS_TYPE
                 WHERE UPPER(BUSINESS_TYPE_NAME) = UPPER(pv_business_type_name);
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    RAISE_APPLICATION_ERROR(-20213, 'Invalid business type name');
            END; 
        END IF;
        IF lt_columns_indexed.EXISTS('POSTAL') AND (pv_state_cd IS NULL OR pv_city IS NULL) THEN
            SELECT MAX(CITY), MAX(STATE_CD), MAX(CNT)
              INTO lv_city, lv_state_cd, ln_cnt
              FROM (SELECT P.CITY, S.STATE_CD, 1 CNT
                  FROM FRONT.POSTAL P 
                  JOIN CORP.STATE S ON P.STATE_ID = S.STATE_ID
                WHERE P.POSTAL_CD = pv_postal
                  AND S.COUNTRY_CD = pv_country_cd
                ORDER BY P.POSTAL_ID ASC)
             WHERE ROWNUM = 1;
             IF ln_cnt = 0 THEN
                RAISE_APPLICATION_ERROR(-20220, 'Invalid postal code');
                RETURN;
            END IF; 
        ELSE
            IF lt_columns_indexed.EXISTS('STATE') THEN
                SELECT COUNT(*), pv_state_cd
                  INTO ln_cnt, lv_state_cd
                  FROM CORP.STATE 
                 WHERE STATE_CD = pv_state_cd
                   AND COUNTRY_CD = pv_country_cd;
                IF ln_cnt = 0 THEN
                    RAISE_APPLICATION_ERROR(-20220, 'Invalid state/country');
                    RETURN;
                END IF;
            END IF;
            lv_city := pv_city;
        END IF; 
        INTERNAL_UPDATE_TERMINAL(
               pn_terminal_id,
               pv_asset_nbr,
               ln_machine_id,
               pv_telephone,
               pv_prefix,
               NULL,
               pv_region_name,
               pv_location_name,
               pv_location_details,
               pv_address1,
               lv_city,
               lv_state_cd,
               pv_postal,
               pv_country_cd,
               pn_customer_bank_id,
               pn_pay_sched_id,
               pn_user_id,
               pn_user_id,
               NULL,
               ln_location_type_id,
               pn_location_type_specific,
               ln_product_type_id,
               pn_product_type_specific,
               pc_auth_mode,
               pn_time_zone_id,
               pc_dex_data,
               pc_receipt,
               pn_vends,
               pv_term_var_1,
               pv_term_var_2,
               pv_term_var_3,
               pv_term_var_4,
               pv_term_var_5,
               pv_term_var_6,
               pv_term_var_7,
               pv_term_var_8,
               pv_doing_business_as,
               pc_pos_environment_cd,
               pv_entry_capability_cds,
               pc_pin_capability_flag,
               ln_business_type_id,
               pv_customer_service_phone,
               pv_customer_service_email,
               pv_client,
               lt_columns_indexed,
               pn_master_user_id);
    END;
    
    -- R51 signature
    PROCEDURE CREATE_CUSTOMER_BANK(
        pn_customer_bank_id OUT CORP.CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        pn_customer_id IN CORP.CUSTOMER_BANK.CUSTOMER_ID%TYPE,
        pn_user_id IN CORP.CUSTOMER.USER_ID%TYPE,
        pv_bank_name IN CORP.CUSTOMER_BANK.BANK_NAME%TYPE,
        pv_bank_address1 IN CORP.CUSTOMER_BANK.BANK_ADDRESS%TYPE,
        pv_bank_city IN CORP.CUSTOMER_BANK.BANK_CITY%TYPE,
        pv_bank_state_cd IN CORP.CUSTOMER_BANK.BANK_STATE%TYPE,
        pv_bank_postal IN CORP.CUSTOMER_BANK.BANK_ZIP%TYPE,
        pv_bank_country_cd IN CORP.CUSTOMER_BANK.BANK_COUNTRY_CD%TYPE,
        pv_account_title IN CORP.CUSTOMER_BANK.ACCOUNT_TITLE%TYPE,
        pv_account_type IN CORP.CUSTOMER_BANK.ACCOUNT_TYPE%TYPE,
        pv_bank_routing_nbr IN CORP.CUSTOMER_BANK.BANK_ROUTING_NBR%TYPE,
        pv_bank_acct_nbr IN CORP.CUSTOMER_BANK.BANK_ACCT_NBR%TYPE,
        pv_contact_name IN CORP.CUSTOMER_BANK.CONTACT_NAME%TYPE,
        pv_contact_title IN CORP.CUSTOMER_BANK.CONTACT_TITLE%TYPE,
        pv_contact_telephone IN CORP.CUSTOMER_BANK.CONTACT_TELEPHONE%TYPE,
        pv_contact_fax IN CORP.CUSTOMER_BANK.CONTACT_FAX%TYPE,
        pv_swift_code IN CORP.CUSTOMER_BANK.SWIFT_CODE%TYPE,
        pn_pay_cycle_id IN CORP.CUSTOMER_BANK.PAY_CYCLE_ID%TYPE DEFAULT 3,
        pv_bank_tax_id IN CORP.CUSTOMER_BANK.TAX_ID_NBR%TYPE DEFAULT NULL,
        pv_billing_address1 IN CORP.CUSTOMER_ADDR.ADDRESS1%TYPE DEFAULT NULL,
        pv_billing_city IN CORP.CUSTOMER_ADDR.CITY%TYPE DEFAULT NULL,
        pv_billing_state_cd IN CORP.CUSTOMER_ADDR.STATE%TYPE DEFAULT NULL,
        pv_billing_postal IN CORP.CUSTOMER_ADDR.ZIP%TYPE DEFAULT NULL,
        pv_billing_country_cd IN CORP.CUSTOMER_ADDR.ZIP%TYPE DEFAULT NULL,
        pv_use_tax_id_for_all VARCHAR DEFAULT NULL)
    IS
      l_bank_tax_id CORP.CUSTOMER_BANK.TAX_ID_NBR%TYPE;
    BEGIN
        SELECT CORP.CUSTOMER_BANK_SEQ.NEXTVAL INTO pn_customer_bank_id FROM DUAL;
        IF pv_bank_tax_id is not null THEN
          l_bank_tax_id:=replace(pv_bank_tax_id,'-','');
        END IF;
        INSERT INTO CORP.CUSTOMER_BANK(CUSTOMER_BANK_ID, CUSTOMER_ID, BANK_NAME, BANK_ADDRESS, BANK_CITY, BANK_STATE, BANK_ZIP, BANK_COUNTRY_CD, CREATE_BY,
            ACCOUNT_TITLE, ACCOUNT_TYPE, BANK_ROUTING_NBR, BANK_ACCT_NBR, CONTACT_NAME, CONTACT_TITLE, CONTACT_TELEPHONE, CONTACT_FAX, SWIFT_CODE, PAY_CYCLE_ID,TAX_ID_NBR)
            VALUES(pn_customer_bank_id, pn_customer_id, pv_bank_name, pv_bank_address1, pv_bank_city, pv_bank_state_cd, pv_bank_postal, pv_bank_country_cd, pn_user_id,
                   pv_account_title, pv_account_type, pv_bank_routing_nbr, pv_bank_acct_nbr, pv_contact_name, pv_contact_title, pv_contact_telephone, pv_contact_fax, pv_swift_code, NVL(pn_pay_cycle_id, 3),l_bank_tax_id);
        INSERT INTO CORP.USER_CUSTOMER_BANK (CUSTOMER_BANK_ID, USER_ID, ALLOW_EDIT)
            VALUES(pn_customer_bank_id, pn_user_id, 'Y');
        IF pv_billing_address1 is NOT NULL THEN
          INSERT INTO CORP.CUSTOMER_ADDR(ADDRESS_ID, CUSTOMER_ID, CUSTOMER_BANK_ID, ADDR_TYPE, ADDRESS1, CITY, STATE, ZIP, COUNTRY_CD)
             VALUES (CORP.CUSTOMER_ADDR_SEQ.NEXTVAL, pn_customer_id, pn_customer_bank_id, 4, pv_billing_address1, pv_billing_city, pv_billing_state_cd, pv_billing_postal, pv_billing_country_cd);
        END IF;
        
        IF pv_use_tax_id_for_all = 'Y' THEN
          update CORP.CUSTOMER_BANK set tax_id_nbr=l_bank_tax_id
          where customer_id=pn_customer_id and status<>'D' and customer_bank_id<>pn_customer_bank_id;
        END IF;
    END;
    
    PROCEDURE UPDATE_REGION_NAME(
        pn_region_id REPORT.REGION.REGION_ID%TYPE,
        pv_region_name REPORT.REGION.REGION_NAME%TYPE,
        pn_customer_id REPORT.TERMINAL.CUSTOMER_ID%TYPE)
    IS
    BEGIN
        UPDATE REPORT.REGION
           SET REGION_NAME = TRIM(pv_region_name)
         WHERE REGION_ID = pn_region_id
           AND CUSTOMER_ID = pn_customer_id;
    END;   

    --R46 and above   
    PROCEDURE CREATE_CAMPAIGN(
        pv_campaign_name REPORT.CAMPAIGN.CAMPAIGN_NAME%TYPE,
        pv_campaign_desc REPORT.CAMPAIGN.CAMPAIGN_DESC%TYPE,
        pn_campaign_type_id REPORT.CAMPAIGN.CAMPAIGN_TYPE_ID%TYPE,
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
        pd_start_date REPORT.CAMPAIGN.START_DATE%TYPE,
        pd_end_date REPORT.CAMPAIGN.END_DATE%TYPE,
        pn_discount_percent REPORT.CAMPAIGN.DISCOUNT_PERCENT%TYPE,
        pn_donation_percent REPORT.CAMPAIGN.DONATION_PERCENT%TYPE,
        pn_recur_schedule REPORT.CAMPAIGN.RECUR_SCHEDULE%TYPE,
        pn_threshold_amount REPORT.CAMPAIGN.THRESHOLD_AMOUNT%TYPE,
        pc_is_for_all_cards CHAR,
        pn_campaign_id OUT REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
        pv_promo_code IN OUT REPORT.CAMPAIGN.PROMO_CODE%TYPE,
        pv_assign_to_all_devices REPORT.CAMPAIGN.ASSIGNED_TO_ALLDEVICES%TYPE DEFAULT NULL,
        pv_assign_to_all_cards REPORT.CAMPAIGN.ASSIGNED_TO_ALLCARDS%TYPE DEFAULT NULL,
        pn_nth_vend_free_num REPORT.CAMPAIGN.NTH_VEND_FREE_NUM%TYPE DEFAULT NULL,
        pn_free_vend_max_amount REPORT.CAMPAIGN.FREE_VEND_MAX_AMOUNT%TYPE DEFAULT NULL,
        pn_assign_to_existing_cards NUMBER DEFAULT 1,
        pn_purchase_discount REPORT.CAMPAIGN.PURCHASE_DISCOUNT%TYPE DEFAULT NULL,
        pv_reuse_promo_code CHAR DEFAULT NULL,
        pv_assign_to_all_payroll_cards REPORT.CAMPAIGN.ASSIGNED_TO_ALLPAYROLLCARDS%TYPE DEFAULT NULL
        )
     IS
      pn_customer_id REPORT.CAMPAIGN.CUSTOMER_ID%TYPE;
      l_assign_to_existing_cards NUMBER:=pn_assign_to_existing_cards;
      l_promo_code_exists NUMBER:=0;
    BEGIN
        select report.seq_campaign_id.nextval into pn_campaign_id from dual;
        select decode(customer_id, 0,null,customer_id) into pn_customer_id from report.user_login where user_id=pn_user_id;
        IF pn_campaign_type_id in (1,4,5) THEN
          IF pv_promo_code is null THEN
            pv_promo_code:=UPPER(DBMS_RANDOM.STRING('A', 6));
            l_assign_to_existing_cards:=0;
          ELSE
            IF pv_reuse_promo_code = 'N' THEN
              l_assign_to_existing_cards:=0;
              select count(1) into l_promo_code_exists from report.campaign where upper(promo_code)=upper(pv_promo_code) and status='A';
              IF l_promo_code_exists > 0 THEN
                RAISE_APPLICATION_ERROR(-20230, 'The promo code already exists in the system.');
              END IF;
            END IF;
          END IF;
        END IF;
        insert into report.campaign 
        (campaign_id, CAMPAIGN_NAME, CAMPAIGN_DESC, CAMPAIGN_TYPE_ID, customer_id, START_DATE, END_DATE, DISCOUNT_PERCENT, DONATION_PERCENT, RECUR_SCHEDULE, THRESHOLD_AMOUNT, assigned_to_alldevices,assigned_to_allcards, promo_code,nth_vend_free_num,free_vend_max_amount,is_for_all_cards,purchase_discount, ASSIGNED_TO_ALLPAYROLLCARDS) 
        values(pn_campaign_id, pv_campaign_name, pv_campaign_desc, pn_campaign_type_id, pn_customer_id, pd_start_date, pd_end_date, pn_discount_percent/100, pn_donation_percent/100, pn_recur_schedule,pn_threshold_amount,pv_assign_to_all_devices,pv_assign_to_all_cards, pv_promo_code,pn_nth_vend_free_num,pn_free_vend_max_amount,pc_is_for_all_cards,pn_purchase_discount,pv_assign_to_all_payroll_cards);
        IF pv_assign_to_all_devices = 'Y' THEN
          FOR l_cur in (SELECT DISTINCT T.CUSTOMER_ID
                 FROM REPORT.VW_USER_TERMINAL UT
                 INNER JOIN REPORT.VW_TERMINAL T ON UT.TERMINAL_ID = T.TERMINAL_ID      
                 WHERE UT.USER_ID = pn_user_id) LOOP
                 EDIT_CAMPAIGN_CUSTOMER(pn_campaign_id, l_cur.CUSTOMER_ID,'Y');
            END LOOP;
            REPORT.SYNC_PKG.ENQUEUE_ADD_CAM_POS_PTA_BY_CAM(pn_campaign_id);
        END IF;
        IF pv_assign_to_all_cards = 'Y' THEN
          INSERT INTO PSS.CAMPAIGN_CONSUMER_ACCT(CONSUMER_ACCT_ID,CAMPAIGN_ID,TRAN_COUNT, PROMO_TRAN_COUNT)
          SELECT CONSUMER_ACCT_ID,pn_campaign_id,0,0
          FROM (SELECT CA.CONSUMER_ACCT_ID
          FROM PSS.CONSUMER_ACCT CA
          WHERE CA.CORP_CUSTOMER_ID IN (SELECT DISTINCT T.CUSTOMER_ID
          FROM REPORT.VW_USER_TERMINAL UT
          INNER JOIN REPORT.VW_TERMINAL T ON UT.TERMINAL_ID = T.TERMINAL_ID
          WHERE UT.USER_ID = pn_user_id)
          AND CA.CONSUMER_ACCT_TYPE_ID = 3);
        END IF;
        IF pv_assign_to_all_payroll_cards = 'Y' THEN
          INSERT INTO PSS.CAMPAIGN_CONSUMER_ACCT(CONSUMER_ACCT_ID,CAMPAIGN_ID,TRAN_COUNT, PROMO_TRAN_COUNT)
          SELECT CONSUMER_ACCT_ID,pn_campaign_id,0,0
          FROM (SELECT CA.CONSUMER_ACCT_ID
          FROM PSS.CONSUMER_ACCT CA
          WHERE CA.CORP_CUSTOMER_ID IN (SELECT DISTINCT T.CUSTOMER_ID
          FROM REPORT.VW_USER_TERMINAL UT
          INNER JOIN REPORT.VW_TERMINAL T ON UT.TERMINAL_ID = T.TERMINAL_ID
          WHERE UT.USER_ID = pn_user_id)
          AND CA.CONSUMER_ACCT_TYPE_ID = 7);
        END IF;
        IF pn_campaign_type_id in (1,4,5) and l_assign_to_existing_cards = 1 THEN
          BEGIN
            IF pv_assign_to_all_cards = 'Y' and pv_assign_to_all_payroll_cards = 'Y' THEN
              INSERT INTO PSS.CAMPAIGN_CONSUMER_ACCT(CONSUMER_ACCT_ID,CAMPAIGN_ID,TRAN_COUNT, PROMO_TRAN_COUNT)
              select distinct cca.consumer_acct_id, pn_campaign_id,0,0 from pss.campaign_consumer_acct cca join pss.consumer_acct ca on cca.consumer_acct_id=ca.consumer_acct_id
              where campaign_id in (select campaign_id from report.campaign where promo_code=pv_promo_code and campaign_id <>pn_campaign_id)
              and ca.consumer_acct_type_id<>3 and ca.consumer_acct_type_id<>7;
            ELSIF pv_assign_to_all_cards = 'Y' THEN
              INSERT INTO PSS.CAMPAIGN_CONSUMER_ACCT(CONSUMER_ACCT_ID,CAMPAIGN_ID,TRAN_COUNT, PROMO_TRAN_COUNT)
              select distinct cca.consumer_acct_id, pn_campaign_id,0,0 from pss.campaign_consumer_acct cca join pss.consumer_acct ca on cca.consumer_acct_id=ca.consumer_acct_id
              where campaign_id in (select campaign_id from report.campaign where promo_code=pv_promo_code and campaign_id <>pn_campaign_id)
              and ca.consumer_acct_type_id<>3;  
            ELSIF pv_assign_to_all_payroll_cards = 'Y' THEN
              INSERT INTO PSS.CAMPAIGN_CONSUMER_ACCT(CONSUMER_ACCT_ID,CAMPAIGN_ID,TRAN_COUNT, PROMO_TRAN_COUNT)
              select distinct cca.consumer_acct_id, pn_campaign_id,0,0 from pss.campaign_consumer_acct cca join pss.consumer_acct ca on cca.consumer_acct_id=ca.consumer_acct_id
              where campaign_id in (select campaign_id from report.campaign where promo_code=pv_promo_code and campaign_id <>pn_campaign_id)
              and ca.consumer_acct_type_id<>7;
            ELSE
              INSERT INTO PSS.CAMPAIGN_CONSUMER_ACCT(CONSUMER_ACCT_ID,CAMPAIGN_ID,TRAN_COUNT, PROMO_TRAN_COUNT)
              select distinct cca.consumer_acct_id, pn_campaign_id,0,0 from pss.campaign_consumer_acct cca 
              where campaign_id in (select campaign_id from report.campaign where promo_code=pv_promo_code and campaign_id <>pn_campaign_id);
            END IF;
          END;
        END IF;
    END;
   
    --R45 and above   
    PROCEDURE EDIT_CAMPAIGN(
        pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
        pv_campaign_name REPORT.CAMPAIGN.CAMPAIGN_NAME%TYPE,
        pv_campaign_desc REPORT.CAMPAIGN.CAMPAIGN_DESC%TYPE,
        pn_campaign_type_id REPORT.CAMPAIGN.CAMPAIGN_TYPE_ID%TYPE,
        pd_start_date REPORT.CAMPAIGN.START_DATE%TYPE,
        pd_end_date REPORT.CAMPAIGN.END_DATE%TYPE,
        pn_discount_percent REPORT.CAMPAIGN.DISCOUNT_PERCENT%TYPE,
        pn_donation_percent REPORT.CAMPAIGN.DONATION_PERCENT%TYPE,
        pn_recur_schedule REPORT.CAMPAIGN.RECUR_SCHEDULE%TYPE,
        pn_threshold_amount REPORT.CAMPAIGN.THRESHOLD_AMOUNT%TYPE,
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE DEFAULT NULL,
        pv_assign_to_all_devices REPORT.CAMPAIGN.ASSIGNED_TO_ALLDEVICES%TYPE DEFAULT NULL,
        pv_assign_to_all_cards REPORT.CAMPAIGN.ASSIGNED_TO_ALLCARDS%TYPE DEFAULT NULL,
        pn_nth_vend_free_num REPORT.CAMPAIGN.NTH_VEND_FREE_NUM%TYPE DEFAULT NULL,
        pn_free_vend_max_amount REPORT.CAMPAIGN.FREE_VEND_MAX_AMOUNT%TYPE DEFAULT NULL,
        pn_purchase_discount REPORT.CAMPAIGN.PURCHASE_DISCOUNT%TYPE DEFAULT NULL,
        pv_assign_to_all_payroll_cards REPORT.CAMPAIGN.ASSIGNED_TO_ALLPAYROLLCARDS%TYPE DEFAULT NULL
        )
     IS
     lc_assign_alldevices_changed CHAR(1);
     lc_assign_allcards_changed CHAR(1);
     lc_assign_allprcards_changed CHAR(1);
    BEGIN
        SELECT CASE WHEN ASSIGNED_TO_ALLDEVICES != pv_assign_to_all_devices THEN 'Y' ELSE 'N' END,
          CASE WHEN ASSIGNED_TO_ALLCARDS != pv_assign_to_all_cards THEN 'Y' ELSE 'N' END,
          CASE WHEN ASSIGNED_TO_ALLPAYROLLCARDS != pv_assign_to_all_payroll_cards THEN 'Y' ELSE 'N' END
        INTO lc_assign_alldevices_changed, lc_assign_allcards_changed, lc_assign_allprcards_changed
        FROM REPORT.CAMPAIGN
        WHERE CAMPAIGN_ID=pn_campaign_id
        FOR UPDATE; 
        
        update report.campaign set campaign_name=pv_campaign_name,
        campaign_desc=pv_campaign_desc,
        campaign_type_id=pn_campaign_type_id,
        start_date=pd_start_date,
        end_date=pd_end_date,
        discount_percent=pn_discount_percent/100,
        donation_percent=pn_donation_percent/100,
        recur_schedule=pn_recur_schedule,
        threshold_amount=pn_threshold_amount,
        ASSIGNED_TO_ALLDEVICES=pv_assign_to_all_devices,
        ASSIGNED_TO_ALLCARDS=pv_assign_to_all_cards,
        NTH_VEND_FREE_NUM = pn_nth_vend_free_num,
        FREE_VEND_MAX_AMOUNT =pn_free_vend_max_amount,
        purchase_discount=pn_purchase_discount,
        ASSIGNED_TO_ALLPAYROLLCARDS = pv_assign_to_all_payroll_cards
        where campaign_id=pn_campaign_id;      
        
        IF pn_campaign_type_id not in (1,4,5) THEN
          DELETE_CAMPAIGN_ASSIGNMENT(pn_campaign_id,pn_campaign_type_id);
        END IF;
        
        IF lc_assign_alldevices_changed = 'Y' THEN
          IF pv_assign_to_all_devices = 'Y' THEN
            FOR l_cur in (SELECT DISTINCT T.CUSTOMER_ID
                         FROM REPORT.VW_USER_TERMINAL UT
                         INNER JOIN REPORT.VW_TERMINAL T ON UT.TERMINAL_ID = T.TERMINAL_ID      
                         WHERE UT.USER_ID = pn_user_id) LOOP
                         EDIT_CAMPAIGN_CUSTOMER(pn_campaign_id, l_cur.CUSTOMER_ID,'Y');
                    END LOOP;
                    REPORT.SYNC_PKG.ENQUEUE_ADD_CAM_POS_PTA_BY_CAM(pn_campaign_id);
          ELSE
            IF pn_campaign_type_id in(1,4,5) THEN
              DELETE_CAMPAIGN_ASSIGNMENT(pn_campaign_id,pn_campaign_type_id);
            END IF;
          END IF;
        END IF;  
        
        IF lc_assign_allcards_changed = 'Y' THEN
          BEGIN
            DELETE FROM PSS.CAMPAIGN_CONSUMER_ACCT 
              WHERE CONSUMER_ACCT_ID IN
                (SELECT cca.CONSUMER_ACCT_ID 
                    FROM PSS.CAMPAIGN_CONSUMER_ACCT cca
                    JOIN PSS.CONSUMER_ACCT ca ON cca.CONSUMER_ACCT_ID=ca.CONSUMER_ACCT_ID
                    WHERE cca.CAMPAIGN_ID=pn_campaign_id AND ca.CONSUMER_ACCT_TYPE_ID=3)
				AND CAMPAIGN_ID = pn_campaign_id;
            IF pv_assign_to_all_cards = 'Y' THEN
              BEGIN
                  INSERT INTO PSS.CAMPAIGN_CONSUMER_ACCT(CONSUMER_ACCT_ID,CAMPAIGN_ID)
                    SELECT CONSUMER_ACCT_ID,pn_campaign_id
                    FROM (SELECT CA.CONSUMER_ACCT_ID
                    FROM PSS.CONSUMER_ACCT CA
                    WHERE CA.CORP_CUSTOMER_ID IN (SELECT DISTINCT T.CUSTOMER_ID
                    FROM REPORT.VW_USER_TERMINAL UT
                    INNER JOIN REPORT.VW_TERMINAL T ON UT.TERMINAL_ID = T.TERMINAL_ID
                    WHERE UT.USER_ID = pn_user_id)
                          AND CA.CONSUMER_ACCT_TYPE_ID = 3);
              END;
            END IF;
          END;
        END IF; 
        
       IF lc_assign_allprcards_changed = 'Y' THEN
          BEGIN
            DELETE FROM PSS.CAMPAIGN_CONSUMER_ACCT 
              WHERE CONSUMER_ACCT_ID IN
                (SELECT cca.CONSUMER_ACCT_ID 
                    FROM PSS.CAMPAIGN_CONSUMER_ACCT cca
                    JOIN PSS.CONSUMER_ACCT ca ON cca.CONSUMER_ACCT_ID=ca.CONSUMER_ACCT_ID
                    WHERE cca.CAMPAIGN_ID=pn_campaign_id AND ca.CONSUMER_ACCT_TYPE_ID=7)
				AND CAMPAIGN_ID = pn_campaign_id;
            IF pv_assign_to_all_payroll_cards = 'Y' THEN
              BEGIN
                INSERT INTO PSS.CAMPAIGN_CONSUMER_ACCT(CONSUMER_ACCT_ID,CAMPAIGN_ID)
                  SELECT CONSUMER_ACCT_ID,pn_campaign_id
                  FROM (SELECT CA.CONSUMER_ACCT_ID
                  FROM PSS.CONSUMER_ACCT CA
                  WHERE CA.CORP_CUSTOMER_ID IN (SELECT DISTINCT T.CUSTOMER_ID
                  FROM REPORT.VW_USER_TERMINAL UT
                  INNER JOIN REPORT.VW_TERMINAL T ON UT.TERMINAL_ID = T.TERMINAL_ID
                  WHERE UT.USER_ID = pn_user_id)
                  AND CA.CONSUMER_ACCT_TYPE_ID = 7);
              END;
            END IF;
          END;
        END IF; 
    END;
    
    PROCEDURE DELETE_CAMPAIGN(
        pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE
        )
     IS
    BEGIN
        DELETE_CAMPAIGN_ASSIGNMENT(pn_campaign_id);
        delete from pss.campaign_consumer_acct where campaign_id=pn_campaign_id;
        update report.campaign set status='D' where campaign_id=pn_campaign_id; 
    END;
    
    PROCEDURE ADD_CAMP_POS_PTA_BY_POS(
      pn_pos_pta_id PSS.CAMPAIGN_POS_PTA.POS_PTA_ID%TYPE,
      pn_pos_id PSS.POS.POS_ID%TYPE,
      pc_delete_flag CHAR)
    IS
      l_lock VARCHAR2(128);
    BEGIN 
      l_lock := GLOBALS_PKG.REQUEST_LOCK('PSS.CAMPAIGN_POS_PTA',pn_pos_pta_id);
      
      IF pc_delete_flag = 'Y' THEN
        DELETE FROM PSS.CAMPAIGN_POS_PTA WHERE pos_pta_id = pn_pos_pta_id;
      END IF;
      
      INSERT INTO PSS.CAMPAIGN_POS_PTA(CAMPAIGN_POS_PTA_ID, CAMPAIGN_ID, POS_PTA_ID, START_DATE, END_DATE, PRIORITY, TERMINAL_EPORT_ID)
      select PSS.CAMPAIGN_POS_PTA_SEQ.nextval, ca.campaign_id, pn_pos_pta_id, 
      greatest(NVL(c.START_DATE, MIN_DATE), NVL(te.START_DATE, MIN_DATE)) as start_date, 
      least(NVL(c.END_DATE, MAX_DATE),NVL(te.END_DATE, MAX_DATE)) as end_date,
        CASE WHEN ca.TERMINAL_ID is not null THEN 10 WHEN ca.region_id is not null THEN 20 ELSE 30 END as priority, te.terminal_eport_id
        from report.terminal t join report.terminal_eport te on t.terminal_id=te.terminal_id 
        left outer join report.terminal_region tr on tr.terminal_id =t.terminal_id
        join report.campaign_assignment ca on (t.terminal_id=ca.terminal_id 
        or (ca.customer_id=t.customer_id and ca.region_id=tr.region_id ) 
        or (ca.customer_id=t.customer_id and ca.region_id is null ))     
        join report.campaign c on ca.campaign_id=c.campaign_id 
        join report.eport e ON te.eport_id = e.eport_id
        join device.device d on d.device_serial_cd=e.eport_serial_num and d.device_active_yn_flag='Y' and not(d.device_type_id = 14 and d.device_sub_type_id IN(1,2))
        join pss.pos p on p.device_id=d.device_id and p.pos_id=pn_pos_id
        WHERE greatest(NVL(c.START_DATE, MIN_DATE), NVL(te.START_DATE, MIN_DATE))<least(NVL(c.END_DATE, MAX_DATE),NVL(te.END_DATE, MAX_DATE));
    END;
    
    PROCEDURE ADD_CAM_POS_PTA_BY_REG(
      pn_terminal_id REPORT.TERMINAL.TERMINAL_ID%TYPE,
      pn_region_id REPORT.TERMINAL_REGION.REGION_ID%TYPE)
    IS
      l_lock VARCHAR2(128);
      l_pos_pta_ids NUMBER_TABLE;
      l_count NUMBER;
      l_pos_id NUMBER;
      l_currency_id NUMBER;
      l_min_start_date DATE;
      l_max_end_date DATE;
    BEGIN 
      l_pos_pta_ids:=NUMBER_TABLE();
      select min(c.start_date),COALESCE(max(c.end_date),MAX_DATE) into l_min_start_date,l_max_end_date from report.campaign_assignment ca 
      join report.campaign c on c.campaign_id=ca.campaign_id and c.campaign_type_id=4
      join report.terminal t on t.terminal_id=pn_terminal_id and t.customer_id=ca.customer_id and ca.region_id=pn_region_id;
      IF l_min_start_date is not null and l_max_end_date>sysdate THEN
         select max(pos_id), max(t.fee_currency_id) into l_pos_id,l_currency_id
          from report.terminal t join report.terminal_eport te on t.terminal_id=te.terminal_id 
          join report.eport e ON te.eport_id = e.eport_id
          join device.device d on d.device_serial_cd=e.eport_serial_num and d.device_active_yn_flag='Y' and not(d.device_type_id = 14 and d.device_sub_type_id IN(1,2))
          join pss.pos p on p.device_id=d.device_id  and p.POS_ACTIVE_YN_FLAG='Y'  
          WHERE t.terminal_id=pn_terminal_id and greatest(NVL(l_min_start_date, MIN_DATE), NVL(te.START_DATE, MIN_DATE))<least(NVL(l_max_end_date, MAX_DATE),NVL(te.END_DATE, MAX_DATE));
          PSS.PKG_POS_PTA.INSERT_PROMO_POS_PTA(l_pos_id,l_currency_id);
      ELSE
        select max(pos_id) into l_pos_id
          from report.terminal t join report.terminal_eport te on t.terminal_id=te.terminal_id 
          join report.eport e ON te.eport_id = e.eport_id
          join device.device d on d.device_serial_cd=e.eport_serial_num and d.device_active_yn_flag='Y' and not(d.device_type_id = 14 and d.device_sub_type_id IN(1,2))
          join pss.pos p on p.device_id=d.device_id  and p.POS_ACTIVE_YN_FLAG='Y'  
          WHERE t.terminal_id=pn_terminal_id;
          PSS.PKG_POS_PTA.DISABLE_PROMO_POS_PTA(l_pos_id,'N');
      END IF;
      FOR l_cur in (select pt.pos_pta_id, greatest(NVL(c.START_DATE, MIN_DATE), NVL(te.START_DATE, MIN_DATE)) as start_date, 
        least(NVL(c.END_DATE, MAX_DATE),NVL(te.END_DATE, MAX_DATE)) as end_date, ca.campaign_id, te.terminal_eport_id
        from report.terminal t join report.terminal_eport te on t.terminal_id=te.terminal_id and t.terminal_id=pn_terminal_id
        left outer join report.campaign_assignment ca on ((ca.customer_id=t.customer_id and ca.region_id=pn_region_id ))     
        left outer join report.campaign c on ca.campaign_id=c.campaign_id 
        join report.eport e ON te.eport_id = e.eport_id
        join device.device d on d.device_serial_cd=e.eport_serial_num and d.device_active_yn_flag='Y' and not(d.device_type_id = 14 and d.device_sub_type_id IN(1,2))
        join pss.pos p on p.device_id=d.device_id
        join pss.pos_pta pt on p.pos_id = pt.pos_id AND COALESCE (pt.pos_pta_deactivation_ts,MAX_DATE)>sysdate
        join pss.payment_subtype sb on pt.payment_subtype_id=sb.payment_subtype_id and sb.PAYMENT_SUBTYPE_CLASS not in('Authority::NOP', 'Cash')
        )
      LOOP
          l_count:=0;
          select count(1) into l_count from table(l_pos_pta_ids) where column_value=l_cur.pos_pta_id;
          IF l_count = 0 THEN
            l_lock := GLOBALS_PKG.REQUEST_LOCK('PSS.CAMPAIGN_POS_PTA',l_cur.pos_pta_id);
            DELETE FROM PSS.CAMPAIGN_POS_PTA WHERE pos_pta_id = l_cur.pos_pta_id and priority=20 and terminal_eport_id=l_cur.terminal_eport_id;
          END IF;
          l_pos_pta_ids.extend;
          l_pos_pta_ids(l_pos_pta_ids.last):=l_cur.pos_pta_id;

        IF l_cur.campaign_id is not null and l_cur.start_date <l_cur.end_date THEN
          INSERT INTO PSS.CAMPAIGN_POS_PTA(CAMPAIGN_POS_PTA_ID, CAMPAIGN_ID, POS_PTA_ID, START_DATE, END_DATE, PRIORITY, TERMINAL_EPORT_ID)
          VALUES(PSS.CAMPAIGN_POS_PTA_SEQ.nextval, l_cur.campaign_id, l_cur.pos_pta_id, l_cur.start_date, l_cur.end_date, 20, l_cur.terminal_eport_id );
         END IF;
       END LOOP;
      EXCEPTION
            WHEN NO_DATA_FOUND THEN
              RETURN;
    END;
    
    PROCEDURE ADD_CAM_POS_PTA_BY_TER_EPORT(
    pn_terminal_id REPORT.TERMINAL_EPORT.TERMINAL_ID%TYPE,
        pn_eport_id REPORT.TERMINAL_EPORT.EPORT_ID%TYPE,
        pd_start_date REPORT.TERMINAL_EPORT.START_DATE%TYPE,
    pd_end_date REPORT.TERMINAL_EPORT.END_DATE%TYPE,
    pn_terminal_eport_id REPORT.TERMINAL_EPORT.TERMINAL_EPORT_ID%TYPE,
    pc_delete_only_flag CHAR DEFAULT 'N')
    IS
      l_lock VARCHAR2(128);
      l_pos_pta_ids NUMBER_TABLE;
      l_count NUMBER;
    BEGIN   
      IF pc_delete_only_flag = 'Y' THEN
        DELETE FROM PSS.CAMPAIGN_POS_PTA WHERE terminal_eport_id=pn_terminal_eport_id;
      ELSE
        l_pos_pta_ids:=NUMBER_TABLE();
        FOR l_cur in (select pt.pos_pta_id, greatest(NVL(c.START_DATE, MIN_DATE), NVL(pd_start_date, MIN_DATE)) as start_date, least(NVL(c.END_DATE, MAX_DATE),NVL(pd_end_date, MAX_DATE)) as end_date,
          CASE WHEN ca.TERMINAL_ID is not null THEN 10 WHEN ca.region_id is not null THEN 20 ELSE 30 END as priority, ca.campaign_id
          from report.terminal t  
          left outer join report.terminal_region tr on tr.terminal_id =t.terminal_id
          left outer join report.campaign_assignment ca on (t.terminal_id=ca.terminal_id 
          or (ca.customer_id=t.customer_id and ca.region_id=tr.region_id ) 
          or (ca.customer_id=t.customer_id and ca.region_id is null ))     
          left outer join report.campaign c on ca.campaign_id=c.campaign_id 
          join report.eport e ON e.eport_id=pn_eport_id
          join device.device d on d.device_serial_cd=e.eport_serial_num and d.device_active_yn_flag='Y' and not(d.device_type_id = 14 and d.device_sub_type_id IN(1,2))
          join pss.pos p on p.device_id=d.device_id
          join pss.pos_pta pt on p.pos_id = pt.pos_id AND COALESCE (pt.pos_pta_deactivation_ts,MAX_DATE)>sysdate
          join pss.payment_subtype sb on pt.payment_subtype_id=sb.payment_subtype_id and sb.PAYMENT_SUBTYPE_CLASS not in('Authority::NOP', 'Cash')
          WHERE t.terminal_id=pn_terminal_id)
        LOOP     
            l_count:=0;
            select count(1) into l_count from table(l_pos_pta_ids) where column_value=l_cur.pos_pta_id;
            IF l_count = 0 THEN
               l_lock := GLOBALS_PKG.REQUEST_LOCK('PSS.CAMPAIGN_POS_PTA',l_cur.pos_pta_id);
               DELETE FROM PSS.CAMPAIGN_POS_PTA WHERE pos_pta_id = l_cur.pos_pta_id and terminal_eport_id=pn_terminal_eport_id;
            END IF;
            l_pos_pta_ids.extend;
            l_pos_pta_ids(l_pos_pta_ids.last):=l_cur.pos_pta_id;
             
          IF l_cur.campaign_id is not null and l_cur.start_date < l_cur.end_date THEN
            INSERT INTO PSS.CAMPAIGN_POS_PTA(CAMPAIGN_POS_PTA_ID, CAMPAIGN_ID, POS_PTA_ID, START_DATE, END_DATE, PRIORITY, TERMINAL_EPORT_ID)
            VALUES(PSS.CAMPAIGN_POS_PTA_SEQ.nextval, l_cur.campaign_id, l_cur.pos_pta_id, l_cur.start_date, l_cur.end_date, l_cur.priority, pn_terminal_eport_id);
          END IF;
        END LOOP;
      END IF;
      EXCEPTION
            WHEN NO_DATA_FOUND THEN
              RETURN;
    END;
    
    PROCEDURE DELETE_CAMPAIGN_ASSIGNMENT(
        pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
        pn_campaign_type_id REPORT.CAMPAIGN.CAMPAIGN_TYPE_ID%TYPE DEFAULT NULL)
    IS
      l_campaign_type_id REPORT.CAMPAIGN.CAMPAIGN_TYPE_ID%TYPE:=pn_campaign_type_id;
    BEGIN
      delete from report.CAMPAIGN_ASSIGNMENT where campaign_id = pn_campaign_id;
      IF l_campaign_type_id IS NULL THEN
        select campaign_type_id into l_campaign_type_id from report.campaign where campaign_id=pn_campaign_id;
      END IF;
      IF l_campaign_type_id = 4 THEN
        FOR l_cur in (SELECT distinct p.pos_id from pss.pos p join pss.pos_pta pp on p.pos_id=pp.pos_id join PSS.campaign_pos_pta cpp on pp.pos_pta_id=cpp.pos_pta_id
        where cpp.campaign_id=pn_campaign_id) LOOP
          SYNC_PKG.ENQUEUE_DISABLE_PROMO_POS_PTA(l_cur.pos_id);
        END LOOP;
      END IF;
      delete from PSS.campaign_pos_pta where campaign_id=pn_campaign_id;
    END;
    
    PROCEDURE EDIT_CAMPAIGN_CUSTOMER(
      pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
      pn_customer_id REPORT.CAMPAIGN_ASSIGNMENT.CUSTOMER_ID%TYPE,
      pv_add CHAR)
    IS
    BEGIN    
      delete from report.campaign_assignment 
      where campaign_id=pn_campaign_id and customer_id = pn_customer_id and region_id is null;
      IF pv_add = 'Y' THEN 
        insert into report.campaign_assignment(campaign_assignment_id, campaign_id, customer_id)
        values(REPORT.CAMPAIGN_ASSIGNMENT_SEQ.nextval,pn_campaign_id,pn_customer_id);
      END IF;
      UPDATE REPORT.CAMPAIGN SET ASSIGNED_TO_ALLDEVICES='N' WHERE CAMPAIGN_ID=pn_campaign_id;
    END;
    
    PROCEDURE EDIT_CAMPAIGN_REGION(
      pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
      pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
      pn_customer_id REPORT.CAMPAIGN_ASSIGNMENT.CUSTOMER_ID%TYPE,
      pn_region_id NUMBER_TABLE,
      pv_add CHAR)
    IS
    BEGIN    
      delete from report.campaign_assignment 
      where campaign_id=pn_campaign_id and customer_id = pn_customer_id and region_id member of pn_region_id;
      
      IF pv_add = 'Y' THEN
        insert into report.campaign_assignment(campaign_assignment_id, campaign_id, customer_id, region_id)
        select REPORT.CAMPAIGN_ASSIGNMENT_SEQ.nextval, pn_campaign_id, pn_customer_id, column_value customer_id from table(pn_region_id);
      END IF;
      UPDATE REPORT.CAMPAIGN SET ASSIGNED_TO_ALLDEVICES='N' WHERE CAMPAIGN_ID=pn_campaign_id;
    END;
    
    PROCEDURE EDIT_CAMPAIGN_TERMINAL(
      pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
      pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
      pn_terminal_id NUMBER_TABLE,
      pv_add CHAR)
    IS
    BEGIN
      delete from report.campaign_assignment where campaign_id=pn_campaign_id and terminal_id member of pn_terminal_id;
      IF pv_add = 'Y' THEN
        insert into report.campaign_assignment(campaign_assignment_id, campaign_id, terminal_id)
        select REPORT.CAMPAIGN_ASSIGNMENT_SEQ.nextval, pn_campaign_id, column_value terminal_id from table(pn_terminal_id);
      END IF;
      UPDATE REPORT.CAMPAIGN SET ASSIGNED_TO_ALLDEVICES='N' WHERE CAMPAIGN_ID=pn_campaign_id; 
    END;
    
    PROCEDURE ADD_CAM_POS_PTA_BY_CAMPAIGN(
      pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE)
    IS
      l_campaign_type_id REPORT.CAMPAIGN.CAMPAIGN_TYPE_ID%TYPE;    
    BEGIN
      SELECT campaign_type_id into l_campaign_type_id FROM REPORT.CAMPAIGN where campaign_id=pn_campaign_id;
      IF l_campaign_type_id = 4 THEN
        ADJUST_PROMO_POS_PTA(pn_campaign_id);
       END IF;
      DELETE from PSS.CAMPAIGN_POS_PTA where CAMPAIGN_ID=pn_campaign_id;
      FOR l_cur in (
      select pt.pos_pta_id, greatest(NVL(c.START_DATE, MIN_DATE), NVL(te.START_DATE, MIN_DATE)) as start_date, least(NVL(c.END_DATE, MAX_DATE),NVL(te.END_DATE, MAX_DATE)) as end_date,
        CASE WHEN ca.TERMINAL_ID is not null THEN 10 WHEN ca.region_id is not null THEN 20 ELSE 30 END as priority, te.terminal_eport_id
        from report.terminal t join report.terminal_eport te on t.terminal_id=te.terminal_id
        left outer join report.terminal_region tr on tr.terminal_id =t.terminal_id
        join report.campaign_assignment ca on (t.terminal_id=ca.terminal_id 
        or (ca.customer_id=t.customer_id and ca.region_id=tr.region_id ) 
        or (ca.customer_id=t.customer_id and ca.region_id is null ))     
        join report.campaign c on ca.campaign_id=c.campaign_id and ca.campaign_id=pn_campaign_id
        join report.eport e ON te.eport_id = e.eport_id
        join device.device d on d.device_serial_cd=e.eport_serial_num and d.device_active_yn_flag='Y' and not(d.device_type_id = 14 and d.device_sub_type_id IN(1,2))
        join pss.pos p on p.device_id=d.device_id
        join pss.pos_pta pt on p.pos_id = pt.pos_id AND COALESCE (pt.pos_pta_deactivation_ts,MAX_DATE)>sysdate
        join pss.payment_subtype sb on pt.payment_subtype_id=sb.payment_subtype_id and sb.PAYMENT_SUBTYPE_CLASS not in('Authority::NOP', 'Cash')
        WHERE greatest(NVL(c.START_DATE, MIN_DATE), NVL(te.START_DATE, MIN_DATE))<least(NVL(c.END_DATE, MAX_DATE),NVL(te.END_DATE, MAX_DATE)))
      LOOP
        INSERT INTO PSS.CAMPAIGN_POS_PTA(CAMPAIGN_POS_PTA_ID, CAMPAIGN_ID, POS_PTA_ID, START_DATE, END_DATE, PRIORITY, TERMINAL_EPORT_ID)
        VALUES(PSS.CAMPAIGN_POS_PTA_SEQ.nextval, pn_campaign_id, l_cur.pos_pta_id, l_cur.start_date, l_cur.end_date, l_cur.priority, l_cur.terminal_eport_id);
      END LOOP;
    END;
    
    PROCEDURE ADD_CARDS_TO_CAMPAIGN(
      pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
      pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
      pn_card_id NUMBER_TABLE,
      pv_card_type CHAR)
    IS
    BEGIN 
      insert into pss.campaign_consumer_acct(CONSUMER_ACCT_ID,CAMPAIGN_ID)
      SELECT CONSUMER_ACCT_ID,pn_campaign_id
      FROM (SELECT CA.CONSUMER_ACCT_ID
        FROM PSS.CONSUMER_ACCT CA 
        WHERE CA.CORP_CUSTOMER_ID in (SELECT DISTINCT T.CUSTOMER_ID
         FROM REPORT.VW_USER_TERMINAL UT
         INNER JOIN REPORT.VW_TERMINAL T ON UT.TERMINAL_ID = T.TERMINAL_ID      
         WHERE UT.USER_ID = pn_user_id) and CONSUMER_ACCT_ID member of pn_card_id
        minus
        SELECT CONSUMER_ACCT_ID from pss.campaign_consumer_acct where campaign_id=pn_campaign_id);
      IF pv_card_type = 'P' THEN
           UPDATE REPORT.CAMPAIGN 
              SET ASSIGNED_TO_ALLPAYROLLCARDS = 'N' 
              WHERE REPORT.CAMPAIGN.CAMPAIGN_ID = pn_campaign_id;  
      ELSIF pv_card_type = 'N' THEN
          UPDATE REPORT.CAMPAIGN 
              SET ASSIGNED_TO_ALLCARDS = 'N' 
              WHERE REPORT.CAMPAIGN.CAMPAIGN_ID = pn_campaign_id;
      END IF;
    END;
    
    PROCEDURE REMOVE_CARDS_FROM_CAMPAIGN(
      pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
      pn_card_id NUMBER_TABLE,
      pv_card_type CHAR)
    IS
    BEGIN 
      delete from pss.campaign_consumer_acct where campaign_id=pn_campaign_id
      and CONSUMER_ACCT_ID member of pn_card_id;
      IF pv_card_type = 'P' THEN
           UPDATE REPORT.CAMPAIGN 
              SET ASSIGNED_TO_ALLPAYROLLCARDS = 'N' 
              WHERE REPORT.CAMPAIGN.CAMPAIGN_ID = pn_campaign_id;  
      ELSIF pv_card_type = 'N' THEN
          UPDATE REPORT.CAMPAIGN 
              SET ASSIGNED_TO_ALLCARDS = 'N' 
              WHERE REPORT.CAMPAIGN.CAMPAIGN_ID = pn_campaign_id;
      END IF;
    END;
    

    FUNCTION                 FIND_CAMPAIGN (
        l_terminal_id REPORT.TERMINAL_EPORT.TERMINAL_ID%TYPE,
        l_region_id REPORT.TERMINAL_REGION.REGION_ID%TYPE,
        l_customer_id REPORT.TERMINAL.CUSTOMER_ID%TYPE)
    RETURN REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE IS
        l_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE;
        l_count NUMBER;
    BEGIN
        select max(campaign_id) into l_campaign_id from report.campaign_assignment where terminal_id=l_terminal_id;
        IF l_campaign_id is NULL THEN
          select max(campaign_id) into l_campaign_id from report.campaign_assignment where customer_id=l_customer_id and region_id=l_region_id;
          IF l_campaign_id is NULL THEN
            select max(campaign_id) into l_campaign_id from report.campaign_assignment where customer_id=l_customer_id and region_id is null;
          END IF;
        END IF;
      RETURN l_campaign_id;  
    END;
    
    FUNCTION                 FIND_CAMPAIGN (
        l_terminal_id REPORT.TERMINAL_EPORT.TERMINAL_ID%TYPE)
    RETURN REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE IS
        l_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE;
        l_region_id REPORT.TERMINAL_REGION.REGION_ID%TYPE;
        l_customer_id REPORT.CAMPAIGN.CUSTOMER_ID%TYPE;
        l_count NUMBER;
    BEGIN
        select t.customer_id, tr.region_id into l_customer_id, l_region_id 
        from report.terminal t left outer join report.terminal_region tr on t.terminal_id=tr.terminal_id
        where t.terminal_id=l_terminal_id;
        l_campaign_id:=FIND_CAMPAIGN(l_terminal_id, l_region_id, l_customer_id);
      RETURN l_campaign_id;  
    END;
    
    PROCEDURE ADD_CAM_POS_PTA_BY_TER(
      pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
      pn_terminal_id REPORT.TERMINAL.TERMINAL_ID%TYPE)
    IS
      l_start_date DATE;
      l_end_date DATE;
      l_lock VARCHAR2(128);
    BEGIN 
      FOR l_cur in (select pt.pos_pta_id as l_pos_pta_id, NVL(te.START_DATE, MIN_DATE) as l_start_date, NVL(te.END_DATE, MAX_DATE) as l_end_date
        from report.terminal t join report.terminal_eport te on t.terminal_id=te.terminal_id
        join report.eport e ON te.eport_id = e.eport_id
        join device.device d on d.device_serial_cd=e.eport_serial_num and d.device_active_yn_flag='Y' and not(d.device_type_id = 14 and d.device_sub_type_id IN(1,2))
        join pss.pos p on p.device_id=d.device_id
        join pss.pos_pta pt on p.pos_id = pt.pos_id AND COALESCE (pt.pos_pta_deactivation_ts,MAX_DATE)>sysdate
        join pss.payment_subtype sb on pt.payment_subtype_id=sb.payment_subtype_id and sb.PAYMENT_SUBTYPE_CLASS not in('Authority::NOP', 'Cash')
        WHERE SYSDATE >= NVL(te.START_DATE, MIN_DATE)
        AND SYSDATE    < NVL(te.END_DATE, MAX_DATE) and t.terminal_id=pn_terminal_id)
      LOOP
        l_lock := GLOBALS_PKG.REQUEST_LOCK('PSS.CAMPAIGN_POS_PTA',l_cur.l_pos_pta_id);
        DELETE FROM PSS.CAMPAIGN_POS_PTA WHERE pos_pta_id = l_cur.l_pos_pta_id;
        
        IF pn_campaign_id is not null THEN
          select greatest(NVL(START_DATE, MIN_DATE), l_cur.l_start_date), least(NVL(END_DATE, MAX_DATE), l_cur.l_end_date) into l_start_date, l_end_date 
          from report.campaign where campaign_id=pn_campaign_id;
          INSERT INTO PSS.CAMPAIGN_POS_PTA(CAMPAIGN_POS_PTA_ID, CAMPAIGN_ID, POS_PTA_ID, START_DATE, END_DATE)
          VALUES(PSS.CAMPAIGN_POS_PTA_SEQ.nextval, pn_campaign_id, l_cur.l_pos_pta_id, l_start_date, l_end_date );
         END IF;
       END LOOP;
      EXCEPTION
            WHEN NO_DATA_FOUND THEN
              RETURN;
    END;
    
    PROCEDURE ADD_CAM_POS_PTA_BY_TER_EPORT(
      pn_terminal_id REPORT.TERMINAL_EPORT.TERMINAL_ID%TYPE,
      pn_eport_id REPORT.TERMINAL_EPORT.EPORT_ID%TYPE,
      pd_start_date REPORT.TERMINAL_EPORT.START_DATE%TYPE,
    pd_end_date REPORT.TERMINAL_EPORT.END_DATE%TYPE)
    IS
      l_campaign_id NUMBER;
      l_start_date DATE;
      l_end_date DATE;
      l_lock VARCHAR2(128);
    BEGIN
      l_campaign_id:=find_campaign(pn_terminal_id);
      
      FOR l_cur in (select pt.pos_pta_id as l_pos_pta_id, NVL(pd_start_date, MIN_DATE) as l_start_date, NVL(pd_end_date, MAX_DATE) as l_end_date
        from device.device d 
        join report.eport e on d.device_serial_cd=e.eport_serial_num and d.device_active_yn_flag='Y' and not(d.device_type_id = 14 and d.device_sub_type_id IN(1,2))
        join pss.pos p on p.device_id=d.device_id
        join pss.pos_pta pt on p.pos_id = pt.pos_id AND COALESCE (pt.pos_pta_deactivation_ts,MAX_DATE)>sysdate
        join pss.payment_subtype sb on pt.payment_subtype_id=sb.payment_subtype_id and sb.PAYMENT_SUBTYPE_CLASS not in('Authority::NOP', 'Cash')
        WHERE SYSDATE >= NVL(pd_start_date, MIN_DATE)
        AND SYSDATE    < NVL(pd_end_date, MAX_DATE) and e.eport_id=pn_eport_id)
      LOOP
        
        l_lock := GLOBALS_PKG.REQUEST_LOCK('PSS.CAMPAIGN_POS_PTA',l_cur.l_pos_pta_id);
        DELETE FROM PSS.CAMPAIGN_POS_PTA WHERE pos_pta_id = l_cur.l_pos_pta_id;
        
        IF l_campaign_id is not null THEN
          select greatest(NVL(START_DATE, MIN_DATE), l_cur.l_start_date), least(NVL(END_DATE, MAX_DATE), l_cur.l_end_date) into l_start_date, l_end_date 
          from report.campaign where campaign_id=l_campaign_id;
          INSERT INTO PSS.CAMPAIGN_POS_PTA(CAMPAIGN_POS_PTA_ID, CAMPAIGN_ID, POS_PTA_ID, START_DATE, END_DATE)
          VALUES(PSS.CAMPAIGN_POS_PTA_SEQ.nextval, l_campaign_id, l_cur.l_pos_pta_id, l_start_date, l_end_date );
        END IF;
      END LOOP;
      EXCEPTION
            WHEN NO_DATA_FOUND THEN
              RETURN;
    END;
    
    PROCEDURE ADD_CAMPAIGN_CUSTOMER(
      pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
      pn_customer_id NUMBER_TABLE)
    IS
    BEGIN    
      delete from report.campaign_assignment 
      where customer_id member of pn_customer_id and region_id is null;
      
      insert into report.campaign_assignment(campaign_assignment_id, campaign_id, customer_id)
      select REPORT.CAMPAIGN_ASSIGNMENT_SEQ.nextval, pn_campaign_id, column_value customer_id from table(pn_customer_id);
    END;
    
    PROCEDURE ADD_CAMPAIGN_TERMINAL(
      pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
      pn_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
      pn_terminal_id NUMBER_TABLE,
      pn_assgined_terminal_id OUT NUMBER_TABLE)
    IS
    BEGIN
      select terminal_id bulk collect into pn_assgined_terminal_id from (
        select column_value terminal_id from table(pn_terminal_id) p join REPORT.VW_USER_TERMINAL UT on p.column_value=ut.terminal_id and ut.user_id = pn_user_id
        minus
        select t.terminal_id from report.terminal t join report.campaign_assignment ca on ca.campaign_id=pn_campaign_id and t.customer_id=ca.customer_id and ca.region_id is null
        minus
        select t.terminal_id from report.terminal t join report.terminal_region tr on t.terminal_id=tr.terminal_id 
        join report.campaign_assignment ca on ca.campaign_id=pn_campaign_id and t.customer_id=ca.customer_id and tr.region_id=ca.region_id
      );
      
      delete from report.campaign_assignment where terminal_id member of pn_assgined_terminal_id;

      insert into report.campaign_assignment(campaign_assignment_id, campaign_id, terminal_id)
      select REPORT.CAMPAIGN_ASSIGNMENT_SEQ.nextval, pn_campaign_id, column_value terminal_id from table(pn_assgined_terminal_id);
    END;
        
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/PSS/PKG_TRAN.pbk?revision=1.283
CREATE OR REPLACE PACKAGE BODY PSS.PKG_TRAN IS
FUNCTION cal_purchase_discount_amount(
    pn_tran_id IN PSS.TRAN.TRAN_ID%TYPE,
    pn_purchase_discount IN NUMBER,
    pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE)
  RETURN NUMBER
IS
    ln_discount_amount NUMBER:=0;
BEGIN
    FOR l_cur in (select t.tran_line_item_amount,t.tran_line_item_quantity
    from pss.tran_line_item t join pss.tran_line_item_type tt on t.tran_line_item_type_id=tt.tran_line_item_type_id
    where tt.DISCOUNTABLE='Y' and t.tran_line_item_amount>0 and t.tran_line_item_quantity>0 and t.tran_id=pn_tran_id) LOOP
      IF l_cur.tran_line_item_amount*pn_minor_currency_factor>=pn_purchase_discount THEN
        ln_discount_amount:=ln_discount_amount+pn_purchase_discount*l_cur.tran_line_item_quantity;
      END IF;
    END LOOP;
    RETURN ln_discount_amount;
END;

PROCEDURE CREATE_DISCOUNT_TRAN_LINE_ITEM (
    pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
    pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
    pn_pos_pta_id PSS.POS_PTA.POS_PTA_ID%TYPE,
    pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
    pd_tran_start_ts pss.tran.tran_start_ts%TYPE,
    pn_base_host_id pss.tran_line_item.host_id%TYPE,
    pn_device_id DEVICE.DEVICE_ID%TYPE,
    pn_sale_amount IN OUT pss.sale.sale_amount%TYPE
)
IS
   ln_discount_percent report.campaign.discount_percent%TYPE;
   ln_discount_amount NUMBER;
   ln_purchase_discount_amount NUMBER;
   ln_p_discount_campaign_id PSS.TRAN_LINE_ITEM.CAMPAIGN_ID%TYPE;
   ln_campaign_id PSS.TRAN_LINE_ITEM.CAMPAIGN_ID%TYPE;
   lv_campaign_promo_code REPORT.CAMPAIGN.PROMO_CODE%TYPE;
   ln_tran_line_item_type_id PSS.TRAN_LINE_ITEM_TYPE.TRAN_LINE_ITEM_TYPE_ID%TYPE;
   ln_new_host_count PLS_INTEGER;
   ln_result_cd NUMBER;
   lv_error_message VARCHAR2(255);
   ln_base_host_id pss.tran_line_item.host_id%TYPE:=pn_base_host_id;
   ln_device_id DEVICE.DEVICE_ID%TYPE:=pn_device_id;
   ln_purchase_discount NUMBER;
   
BEGIN
        SELECT MAX(DISCOUNT_PERCENT), MAX(CAMPAIGN_ID), MAX(PROMO_CODE)
          INTO ln_discount_percent, ln_campaign_id, lv_campaign_promo_code
          FROM (
        SELECT C.DISCOUNT_PERCENT, CPP.PRIORITY, C.CAMPAIGN_ID, C.PROMO_CODE
          FROM PSS.CAMPAIGN_POS_PTA CPP
          JOIN REPORT.CAMPAIGN C ON CPP.CAMPAIGN_ID = C.CAMPAIGN_ID
          JOIN PSS.CAMPAIGN_CONSUMER_ACCT CCA on CPP.CAMPAIGN_ID = CCA.CAMPAIGN_ID
         WHERE CCA.CONSUMER_ACCT_ID = pn_consumer_acct_id
           AND CPP.POS_PTA_ID = pn_pos_pta_id
           AND pd_tran_start_ts BETWEEN NVL(CPP.START_DATE, MIN_DATE) AND NVL(CPP.END_DATE, MAX_DATE)
           AND C.CAMPAIGN_TYPE_ID = 1
           AND C.DISCOUNT_PERCENT > 0
           AND C.DISCOUNT_PERCENT < 1
           AND (TRIM(C.RECUR_SCHEDULE) IS NULL OR REPORT.MATCH_CAMPAIGN_RECUR_SCHEDULE(C.RECUR_SCHEDULE, pd_tran_start_ts) = 'Y')
         ORDER BY CPP.PRIORITY, C.DISCOUNT_PERCENT DESC, C.CAMPAIGN_ID DESC)
         WHERE ROWNUM = 1  ;
         
         SELECT MAX(PURCHASE_DISCOUNT), MAX(CAMPAIGN_ID)
          INTO ln_purchase_discount, ln_p_discount_campaign_id
          FROM (
        SELECT C.PURCHASE_DISCOUNT, CPP.PRIORITY, C.CAMPAIGN_ID
          FROM PSS.CAMPAIGN_POS_PTA CPP
          JOIN REPORT.CAMPAIGN C ON CPP.CAMPAIGN_ID = C.CAMPAIGN_ID
          JOIN PSS.CAMPAIGN_CONSUMER_ACCT CCA on CPP.CAMPAIGN_ID = CCA.CAMPAIGN_ID
         WHERE CCA.CONSUMER_ACCT_ID = pn_consumer_acct_id
           AND CPP.POS_PTA_ID = pn_pos_pta_id
           AND pd_tran_start_ts BETWEEN NVL(CPP.START_DATE, MIN_DATE) AND NVL(CPP.END_DATE, MAX_DATE)
           AND C.CAMPAIGN_TYPE_ID = 5
           AND C.PURCHASE_DISCOUNT > 0
           AND (TRIM(C.RECUR_SCHEDULE) IS NULL OR REPORT.MATCH_CAMPAIGN_RECUR_SCHEDULE(C.RECUR_SCHEDULE, pd_tran_start_ts) = 'Y')
         ORDER BY CPP.PRIORITY, C.PURCHASE_DISCOUNT DESC, C.CAMPAIGN_ID DESC)
         WHERE ROWNUM = 1  ;
        ln_purchase_discount_amount:= ln_purchase_discount *pn_minor_currency_factor;    
        IF ln_discount_percent IS NOT NULL and ln_purchase_discount_amount IS NOT NULL THEN -- compare the max discount and use it
            ln_discount_amount := ROUND(pn_sale_amount * ln_discount_percent);
            IF ln_discount_amount > 0 THEN
                IF ln_discount_amount > pn_sale_amount THEN
                    ln_discount_amount := pn_sale_amount;
                END IF;
            END IF;
            ln_purchase_discount_amount:=cal_purchase_discount_amount(pn_tran_id,ln_purchase_discount_amount,pn_minor_currency_factor);
            IF ln_discount_amount >=ln_purchase_discount_amount THEN -- apply loyal discount
                IF lv_campaign_promo_code IS NULL THEN
                  ln_tran_line_item_type_id:=204;
                ELSE
                  ln_tran_line_item_type_id:=211;
                  UPDATE PSS.CONSUMER_ACCT SET LOYALTY_DISCOUNT_TOTAL=NVL(LOYALTY_DISCOUNT_TOTAL, 0) + ln_discount_amount / pn_minor_currency_factor
                  WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id;
                END IF;
            ELSE -- apply purchase discount
              ln_campaign_id:=ln_p_discount_campaign_id;
              ln_tran_line_item_type_id:=213;
              ln_discount_amount:=ln_purchase_discount_amount;
               UPDATE PSS.CONSUMER_ACCT SET PURCHASE_DISCOUNT_TOTAL=NVL(PURCHASE_DISCOUNT_TOTAL, 0) + ln_discount_amount / pn_minor_currency_factor
              WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id;
            END IF;
        ELSIF ln_discount_percent IS NOT NULL THEN -- apply loyal discount
            ln_discount_amount := ROUND(pn_sale_amount * ln_discount_percent);
            IF ln_discount_amount > 0 THEN
                IF ln_discount_amount > pn_sale_amount THEN
                    ln_discount_amount := pn_sale_amount;
                END IF;
            END IF;
            IF lv_campaign_promo_code IS NULL THEN
              ln_tran_line_item_type_id:=204;
            ELSE
              ln_tran_line_item_type_id:=211;
              UPDATE PSS.CONSUMER_ACCT SET LOYALTY_DISCOUNT_TOTAL=NVL(LOYALTY_DISCOUNT_TOTAL, 0) + ln_discount_amount / pn_minor_currency_factor
              WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id;
            END IF;
        ELSIF ln_purchase_discount_amount IS NOT NULL THEN --apply purchase discount
              ln_campaign_id:=ln_p_discount_campaign_id;
              ln_tran_line_item_type_id:=213;
              ln_discount_amount:=cal_purchase_discount_amount(pn_tran_id,ln_purchase_discount_amount,pn_minor_currency_factor);
              UPDATE PSS.CONSUMER_ACCT SET PURCHASE_DISCOUNT_TOTAL=NVL(PURCHASE_DISCOUNT_TOTAL, 0) + ln_discount_amount / pn_minor_currency_factor
              WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id;
        END IF;
        
        IF ln_discount_amount >0 THEN
           pn_sale_amount := pn_sale_amount - ln_discount_amount;
          IF ln_base_host_id IS NULL THEN
              IF ln_device_id is null THEN
                SELECT P.DEVICE_ID
                  INTO ln_device_id
                  FROM PSS.POS_PTA PP
                  JOIN PSS.POS P ON PP.POS_ID = P.POS_ID
                 WHERE PP.POS_PTA_ID = pn_pos_pta_id;
              END IF;
              pkg_device_configuration.sp_create_default_hosts(ln_device_id, ln_new_host_count, ln_result_cd, lv_error_message);
              IF ln_result_cd != PKG_CONST.RESULT__SUCCESS THEN
                RAISE_APPLICATION_ERROR(-20201, 'Could not create base host for device ' || ln_device_id || ': ' || lv_error_message);
              END IF;
              SELECT H.HOST_ID
              INTO ln_base_host_id
              FROM DEVICE.HOST H
              WHERE H.DEVICE_ID = ln_device_id
              AND H.HOST_PORT_NUM = 0;
          END IF;
                -- create discount line item
                INSERT INTO PSS.TRAN_LINE_ITEM (
                    TRAN_ID,
                    HOST_ID,
                    TRAN_LINE_ITEM_TYPE_ID,
                    TRAN_LINE_ITEM_AMOUNT,
                    TRAN_LINE_ITEM_QUANTITY,
                    TRAN_LINE_ITEM_DESC,
                    TRAN_LINE_ITEM_BATCH_TYPE_CD,
                    TRAN_LINE_ITEM_TAX,
                    CAMPAIGN_ID)
                SELECT
                    pn_tran_id,
                    ln_base_host_id,
                    tran_line_item_type_id,
                    -ln_discount_amount / pn_minor_currency_factor,
                    1,
                    CASE WHEN ln_tran_line_item_type_id = 213 THEN tran_line_item_type_desc || ' $' ||rtrim(to_char(ln_purchase_discount, 'FM90.999'), '.')|| ' per item' ELSE tran_line_item_type_desc || ' ' || ln_discount_percent * 100 || '%' END,
                    PKG_CONST.TRAN_BATCH_TYPE__ACTUAL,
                    0,
                    ln_campaign_id
                FROM pss.tran_line_item_type
                WHERE tran_line_item_type_id = ln_tran_line_item_type_id;

        END IF;
END;

FUNCTION sf_find_host_id(
    pn_device_id DEVICE.DEVICE_ID%TYPE,
    pn_host_port_num IN HOST.HOST_PORT_NUM%TYPE,
    pn_host_position_num HOST.HOST_POSITION_NUM%TYPE)
  RETURN HOST.HOST_ID%TYPE
IS
    ln_host_id HOST.HOST_ID%TYPE;
BEGIN
    SELECT MAX(H.HOST_ID)
      INTO ln_host_id
      FROM DEVICE.HOST H
     WHERE H.DEVICE_ID = pn_device_id
       AND H.HOST_PORT_NUM = pn_host_port_num
       AND H.HOST_POSITION_NUM = pn_host_position_num;

    IF ln_host_id IS NULL THEN
        -- Use base host
        SELECT MAX(H.HOST_ID)
          INTO ln_host_id
          FROM DEVICE.HOST H
         WHERE H.DEVICE_ID = pn_device_id
           AND H.HOST_PORT_NUM = 0;
    END IF;

    RETURN ln_host_id;
END;

PROCEDURE PROCESS_PROMO_TRAN (
    pn_tran_id PSS.TRAN.TRAN_ID%TYPE
)
IS
    ln_consumer_acct_id PSS.CONSUMER_ACCT_BASE.CONSUMER_ACCT_ID%TYPE;
    lv_tran_info PSS.TRAN.TRAN_INFO%TYPE;
    lv_payment_subtype_class PSS.TRAN.PAYMENT_SUBTYPE_CLASS%TYPE;
    lv_auth_resp_cd PSS.AUTH.AUTH_RESP_CD%TYPE;
    ln_sale_amount PSS.SALE.SALE_AMOUNT%TYPE;
    ln_promo_pass_pos_pta_id PSS.POS_PTA.POS_PTA_ID%TYPE;
    ln_campaign_id NUMBER;
    ld_sysdate DATE := SYSDATE;

BEGIN

    SELECT MAX(T.CONSUMER_ACCT_ID),MAX(S.SALE_AMOUNT), NVL(MAX(T.TRAN_INFO), '-'), NVL(MAX(T.PAYMENT_SUBTYPE_CLASS), '-'), NVL(MAX(A.AUTH_RESP_CD), '-'), NVL(MAX(PP2.POS_PTA_ID),
0),MAX(TO_NUMBER_OR_NULL(A2.AUTH_AUTHORITY_REF_CD))
    INTO ln_consumer_acct_id, ln_sale_amount, lv_tran_info, lv_payment_subtype_class, lv_auth_resp_cd, ln_promo_pass_pos_pta_id,ln_campaign_id
    FROM PSS.TRAN T
    JOIN PSS.SALE S ON T.TRAN_ID = S.TRAN_ID
    JOIN PSS.POS_PTA PP ON T.POS_PTA_ID = PP.POS_PTA_ID
    JOIN PSS.PAYMENT_SUBTYPE PS ON PP.PAYMENT_SUBTYPE_ID = PS.PAYMENT_SUBTYPE_ID
    LEFT OUTER JOIN PSS.AUTH A ON T.TRAN_ID = A.TRAN_ID AND A.AUTH_TYPE_CD = 'N' AND A.AUTH_STATE_ID IN (2, 5)
    LEFT OUTER JOIN (PSS.POS_PTA PP2
        JOIN PSS.PAYMENT_SUBTYPE PS2 ON PP2.PAYMENT_SUBTYPE_ID = PS2.PAYMENT_SUBTYPE_ID AND PS2.PAYMENT_SUBTYPE_CLASS = 'Promotion'
    ) ON PP.POS_ID = PP2.POS_ID AND PP2.POS_PTA_ACTIVATION_TS < ld_sysdate AND (PP2.POS_PTA_DEACTIVATION_TS IS NULL OR PP2.POS_PTA_DEACTIVATION_TS > ld_sysdate)
        AND (T.PAYMENT_SUBTYPE_CLASS = 'Promotion' OR PP.POS_PTA_ID != PP2.POS_PTA_ID)
    LEFT OUTER JOIN (PSS.AUTH A2
           JOIN PSS.POS_PTA PP ON A2.POS_PTA_ID = PP.POS_PTA_ID
           JOIN PSS.PAYMENT_SUBTYPE PS ON PP.PAYMENT_SUBTYPE_ID = PS.PAYMENT_SUBTYPE_ID AND PS.PAYMENT_SUBTYPE_CLASS = 'Promotion') on A2.TRAN_ID=T.TRAN_ID
    WHERE T.TRAN_ID = pn_tran_id;

    IF ln_sale_amount IS NULL OR lv_tran_info LIKE '%Promotion updated%' OR ln_promo_pass_pos_pta_id < 1 THEN
        RETURN;
    END IF;

    IF ln_campaign_id is null THEN
        -- in case passthrough timeout and need to retrive campaign_id
        SELECT MAX(CAMPAIGN_ID)
        into ln_campaign_id
          FROM (
        SELECT C.CAMPAIGN_ID
          FROM PSS.CAMPAIGN_POS_PTA CPP
          JOIN REPORT.CAMPAIGN C ON CPP.CAMPAIGN_ID = C.CAMPAIGN_ID
          JOIN PSS.POS_PTA PTA ON CPP.POS_PTA_ID = PTA.POS_PTA_ID
          JOIN PSS.CURRENCY cur ON NVL(pta.CURRENCY_CD, 'USD') = cur.CURRENCY_CD
          JOIN PSS.POS P ON P.POS_ID = PTA.POS_ID
          JOIN LOCATION.LOCATION L ON NVL(P.LOCATION_ID, 1) = L.LOCATION_ID
          JOIN LOCATION.TIME_ZONE TZ ON L.LOCATION_TIME_ZONE_CD = TZ.TIME_ZONE_CD
         WHERE CPP.POS_PTA_ID = ln_promo_pass_pos_pta_id
           AND ld_sysdate BETWEEN NVL(CPP.START_DATE, MIN_DATE) AND NVL(CPP.END_DATE, MAX_DATE)
           AND C.CAMPAIGN_TYPE_ID = 4
         ORDER BY CPP.PRIORITY, C.NTH_VEND_FREE_NUM, C.CAMPAIGN_ID DESC)
         WHERE ROWNUM = 1;
    END IF;

    IF ln_sale_amount > 0 THEN
        IF lv_payment_subtype_class != 'Promotion' THEN
           UPDATE PSS.CAMPAIGN_CONSUMER_ACCT
           SET TRAN_COUNT = TRAN_COUNT + 1
           WHERE CAMPAIGN_ID = ln_campaign_id AND CONSUMER_ACCT_ID = ln_consumer_acct_id;
        END IF;
    ELSE
        IF lv_payment_subtype_class = 'Promotion' THEN
            UPDATE PSS.CAMPAIGN_CONSUMER_ACCT
            SET TRAN_COUNT = CASE WHEN TRAN_COUNT - 1 >= 0 THEN TRAN_COUNT - 1 ELSE 0 END,
                PROMO_TRAN_COUNT = CASE WHEN PROMO_TRAN_COUNT - 1 >= 0 THEN PROMO_TRAN_COUNT - 1 ELSE 0 END
            WHERE CAMPAIGN_ID = ln_campaign_id AND CONSUMER_ACCT_ID = ln_consumer_acct_id;
        END IF;
    END IF;


    UPDATE PSS.TRAN
    SET TRAN_INFO = SUBSTR('Promotion updated' || DECODE(TRAN_INFO, NULL, '', ';' || TRAN_INFO), 1, 1000)
    WHERE TRAN_ID = pn_tran_id AND NVL(TRAN_INFO, '-') NOT LIKE '%Promotion updated%';
END;

PROCEDURE SP_CREATE_REFUND (
    pv_global_trans_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
    pv_orig_global_trans_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
    pn_refund_utc_ts_ms IN NUMBER,
    pn_orig_upload_utc_ts_ms IN NUMBER,
    pn_refund_amt PSS.REFUND.REFUND_AMT%TYPE,
    pn_refund_desc PSS.REFUND.REFUND_DESC%TYPE,
    pn_refund_issue_by PSS.REFUND.REFUND_ISSUE_BY%TYPE,
    pn_refund_type_cd PSS.REFUND.REFUND_TYPE_CD%TYPE,
    pc_entry_method_cd PSS.REFUND.ACCT_ENTRY_METHOD_CD%TYPE,
    pc_already_inserted_flag OUT VARCHAR2,
    pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
    pn_orig_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
    pn_minor_currency_factor OUT PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
    pn_payment_subtype_key_id OUT PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE,
    pv_payment_subtype_class OUT PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE)
IS
    ld_orig_tran_upload_ts PSS.TRAN.TRAN_UPLOAD_TS%TYPE;
    ld_refund_ts PSS.TRAN.TRAN_START_TS%TYPE;
    lv_last_lock_utc_ts VARCHAR2(128);
    ln_cnt PLS_INTEGER;
    ln_orig_tran_id PSS.TRAN.TRAN_ID%TYPE;
    lv_lock_string VARCHAR2(100);
    ln_start INTEGER;
    ln_end INTEGER;
    ln_consumer_acct_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_TYPE_ID%TYPE;
    ln_consumer_acct_sub_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_SUB_TYPE_ID%TYPE;
    ln_override_trans_type_id PSS.REFUND.OVERRIDE_TRANS_TYPE_ID%TYPE;
    lv_refund_parsed_acct_data pss.refund.refund_parsed_acct_data%TYPE;
BEGIN
    ln_start := INSTR(pv_global_trans_cd, ':', 1, 1) + 1;
    ln_end := INSTR(pv_global_trans_cd, ':', 1, 3);
    IF ln_end <= 0 THEN
        ln_end := LENGTH(pv_global_trans_cd) + 1;
    END IF;
    lv_lock_string := SUBSTR(pv_global_trans_cd, ln_start,  ln_end -  ln_start);
    ld_orig_tran_upload_ts := DBADMIN.UTC_TO_LOCAL_DATE(DBADMIN.MILLIS_TO_TIMESTAMP(pn_orig_upload_utc_ts_ms));
    ld_refund_ts := DBADMIN.UTC_TO_LOCAL_DATE(DBADMIN.MILLIS_TO_TIMESTAMP(pn_refund_utc_ts_ms));
    lv_last_lock_utc_ts := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', lv_lock_string);
    -- check if refund already exists
    BEGIN
        SELECT X.TRAN_ID, c.MINOR_CURRENCY_FACTOR, pta.PAYMENT_SUBTYPE_KEY_ID, PST.PAYMENT_SUBTYPE_CLASS, 'Y'
          INTO pn_tran_id, pn_minor_currency_factor, pn_payment_subtype_key_id, pv_payment_subtype_class, pc_already_inserted_flag
          FROM PSS.TRAN X
          JOIN PSS.POS_PTA PTA ON PTA.POS_PTA_ID = X.POS_PTA_ID
          JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
          JOIN PSS.PAYMENT_SUBTYPE PST ON PTA.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
         WHERE X.TRAN_GLOBAL_TRANS_CD = pv_global_trans_cd;
        RETURN;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            pc_already_inserted_flag := 'N';
    END;
    -- Find original transaction
    BEGIN
        SELECT X.TRAN_ID, PSS.SEQ_TRAN_ID.NEXTVAL, c.MINOR_CURRENCY_FACTOR, pta.PAYMENT_SUBTYPE_KEY_ID, PST.PAYMENT_SUBTYPE_CLASS, CA.CONSUMER_ACCT_TYPE_ID, CA.CONSUMER_ACCT_SUB_TYPE_ID
          INTO pn_orig_tran_id, pn_tran_id, pn_minor_currency_factor, pn_payment_subtype_key_id, pv_payment_subtype_class, ln_consumer_acct_type_id, ln_consumer_acct_sub_type_id
          FROM PSS.TRAN X
          JOIN PSS.POS_PTA PTA ON PTA.POS_PTA_ID = X.POS_PTA_ID
          JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
          JOIN PSS.PAYMENT_SUBTYPE PST ON PTA.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
          LEFT OUTER JOIN PSS.CONSUMER_ACCT CA ON X.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
         WHERE X.TRAN_GLOBAL_TRANS_CD = pv_orig_global_trans_cd
           AND X.TRAN_UPLOAD_TS = ld_orig_tran_upload_ts;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
             RAISE_APPLICATION_ERROR(-20381, 'Original Transaction ''' || pv_orig_global_trans_cd || ''', uploaded at ' || TO_CHAR(ld_orig_tran_upload_ts, 'MM/DD/YYYY HH24:MI:SS') || ' not found');
    END;

    INSERT INTO PSS.TRAN (
            TRAN_ID,
            PARENT_TRAN_ID,
            TRAN_START_TS,
            TRAN_END_TS,
            TRAN_UPLOAD_TS,
            TRAN_GLOBAL_TRANS_CD,
            TRAN_STATE_CD,
            CONSUMER_ACCT_ID,
            TRAN_DEVICE_TRAN_CD,
            POS_PTA_ID,
            TRAN_DEVICE_RESULT_TYPE_CD,
            TRAN_RECEIVED_RAW_ACCT_DATA,
            PAYMENT_SUBTYPE_KEY_ID,
            PAYMENT_SUBTYPE_CLASS,
            CLIENT_PAYMENT_TYPE_CD,
            DEVICE_NAME
            )
     SELECT pn_tran_id,
            pn_orig_tran_id,
            ld_refund_ts,
            ld_refund_ts,
            NULL, /* Must be NULL so that PSSUpdater will not pick it up */
            pv_global_trans_cd,
            '8',
            O.CONSUMER_ACCT_ID,
            SUBSTR(pv_global_trans_cd, INSTR(pv_global_trans_cd, ':', 1, 2) + 1, LENGTH(pv_global_trans_cd)),
            O.POS_PTA_ID,
            O.TRAN_DEVICE_RESULT_TYPE_CD,
            O.TRAN_RECEIVED_RAW_ACCT_DATA,
            pp.PAYMENT_SUBTYPE_KEY_ID,
            ps.PAYMENT_SUBTYPE_CLASS,
            ps.CLIENT_PAYMENT_TYPE_CD,
            d.DEVICE_NAME
    FROM PSS.TRAN O
    JOIN pss.pos_pta pp ON O.pos_pta_id = pp.pos_pta_id
    JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
    JOIN pss.pos p ON pp.pos_id = p.pos_id
    JOIN device.device d ON p.device_id = d.device_id
    WHERE O.TRAN_ID = pn_orig_tran_id;

    IF ln_consumer_acct_type_id = 3 AND ln_consumer_acct_sub_type_id IS NOT NULL THEN
        IF ln_consumer_acct_sub_type_id = 2 THEN
            ln_override_trans_type_id := 31;
        ELSE
            ln_override_trans_type_id := 20;
        END IF;
    END IF;

    select max(consumer_acct_identifier) into lv_refund_parsed_acct_data
    from pss.consumer_acct where consumer_acct_id =(select CONSUMER_ACCT_ID from pss.tran where tran_id=pn_orig_tran_id)
    and consumer_acct_type_id=6;

    INSERT INTO PSS.REFUND (
            TRAN_ID,
            REFUND_AMT,
            REFUND_DESC,
            REFUND_ISSUE_TS,
            REFUND_ISSUE_BY,
            REFUND_TYPE_CD,
            REFUND_STATE_ID,
            ACCT_ENTRY_METHOD_CD,
            OVERRIDE_TRANS_TYPE_ID,
            refund_parsed_acct_data
        ) VALUES (
            pn_tran_id,
            -ABS(pn_refund_amt),
            pn_refund_desc,
            ld_refund_ts,
            pn_refund_issue_by,
            pn_refund_type_cd,
            6,
            pc_entry_method_cd,
            ln_override_trans_type_id,
            lv_refund_parsed_acct_data);
END;

PROCEDURE SP_CLOSE_CONSUMER_ACCT (
    pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
    pv_refund_issue_by PSS.REFUND.REFUND_ISSUE_BY%TYPE,
    pn_refund_amt OUT PSS.REFUND.REFUND_AMT%TYPE,
    pn_eft_credit_amt OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
    pn_doc_id OUT CORP.DOC.DOC_ID%TYPE
)
IS
    ln_consumer_acct_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_TYPE_ID%TYPE;
    ln_consumer_acct_sub_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_SUB_TYPE_ID%TYPE;
    ln_corp_customer_id PSS.CONSUMER_ACCT.CORP_CUSTOMER_ID%TYPE;
    lv_currency_cd PSS.CONSUMER_ACCT.CURRENCY_CD%TYPE;
    lv_consumer_acct_cd PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD%TYPE;
    lv_consumer_acct_identifier PSS.CONSUMER_ACCT.CONSUMER_ACCT_IDENTIFIER%TYPE;
    ln_refund_amt PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE;
    ln_eft_credit_amt PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE;
    ln_tran_id PSS.TRAN.TRAN_ID%TYPE;
    ln_orig_tran_id PSS.TRAN.PARENT_TRAN_ID%TYPE;
    ln_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE;
    ld_refund_ts PSS.TRAN.TRAN_START_TS%TYPE := SYSDATE;
    lv_replenish_card_masked PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD%TYPE;
    lv_tran_device_tran_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE;
    ln_ledger_id CORP.LEDGER.LEDGER_ID%TYPE;
    ln_replenish_ca_id PSS.CONSUMER_ACCT_REPLENISH.REPLEN_CONSUMER_ACCT_ID%TYPE;
    lv_card_key PSS.REFUND.CARD_KEY%TYPE;
BEGIN
    pn_refund_amt := 0;
    pn_eft_credit_amt := 0;

    UPDATE PSS.CONSUMER_ACCT
       SET CONSUMER_ACCT_ACTIVE_YN_FLAG = 'N'
     WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id
      RETURNING CONSUMER_ACCT_TYPE_ID, CONSUMER_ACCT_REPLEN_BALANCE, CONSUMER_ACCT_PROMO_BALANCE,
        CORP_CUSTOMER_ID, CURRENCY_CD, CONSUMER_ACCT_CD, CONSUMER_ACCT_IDENTIFIER, CONSUMER_ACCT_SUB_TYPE_ID
      INTO ln_consumer_acct_type_id, ln_refund_amt, ln_eft_credit_amt, ln_corp_customer_id, lv_currency_cd, lv_consumer_acct_cd, lv_consumer_acct_identifier, ln_consumer_acct_sub_type_id;

    UPDATE PSS.CONSUMER_ACCT
       SET CONSUMER_ACCT_BALANCE = 0,
           CONSUMER_ACCT_REPLEN_BALANCE = 0,
           CONSUMER_ACCT_PROMO_BALANCE = 0,
           CLOSE_DATE = sysdate
     WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id;

    IF ln_consumer_acct_type_id != 3 OR ln_consumer_acct_sub_type_id != 1 THEN
        RETURN;
    END IF;

    IF ln_refund_amt > 0 THEN
        SELECT LAST_REPLENISH_TRAN_ID, REPLENISH_POS_PTA_ID, REPLENISH_CARD_MASKED, REPLEN_CONSUMER_ACCT_ID, REPLENISH_CARD_KEY
          INTO ln_orig_tran_id, ln_pos_pta_id, lv_replenish_card_masked, ln_replenish_ca_id, lv_card_key
          FROM (SELECT LAST_REPLENISH_TRAN_ID, REPLENISH_POS_PTA_ID, REPLENISH_CARD_MASKED, REPLEN_CONSUMER_ACCT_ID, REPLENISH_CARD_KEY
                  FROM PSS.CONSUMER_ACCT_REPLENISH
                 WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id
                 ORDER BY LAST_REPLENISH_TRAN_TS DESC, PRIORITY, LAST_REPLENISH_TRAN_ID DESC)
         WHERE ROWNUM = 1;

        SELECT PSS.SEQ_TRAN_ID.NEXTVAL
        INTO ln_tran_id
        FROM DUAL;

        lv_tran_device_tran_cd := DBADMIN.DATE_TO_MILLIS(ld_refund_ts) / 1000;

        INSERT INTO PSS.TRAN (
                    TRAN_ID,
                    PARENT_TRAN_ID,
                    TRAN_START_TS,
                    TRAN_END_TS,
                    TRAN_UPLOAD_TS,
                    TRAN_GLOBAL_TRANS_CD,
                    TRAN_STATE_CD,
                    TRAN_DEVICE_TRAN_CD,
                    POS_PTA_ID,
                    TRAN_DEVICE_RESULT_TYPE_CD,
                    TRAN_RECEIVED_RAW_ACCT_DATA,
                    PAYMENT_SUBTYPE_KEY_ID,
                    PAYMENT_SUBTYPE_CLASS,
                    CLIENT_PAYMENT_TYPE_CD,
                    DEVICE_NAME,
                    CONSUMER_ACCT_ID)
             SELECT ln_tran_id,
                    ln_orig_tran_id,
                    ld_refund_ts,
                    ld_refund_ts,
                    NULL,
                    'RF:' || d.device_name || ':' || lv_tran_device_tran_cd || ':R1',
                    PKG_CONST.TRAN_STATE__BATCH,
                    lv_tran_device_tran_cd,
                    ln_pos_pta_id,
                    PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_RCPT,
                    lv_replenish_card_masked,
                    pp.PAYMENT_SUBTYPE_KEY_ID,
                    ps.PAYMENT_SUBTYPE_CLASS,
                    ps.CLIENT_PAYMENT_TYPE_CD,
                    d.DEVICE_NAME,
                    ln_replenish_ca_id
            FROM pss.pos_pta pp
            JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
            JOIN pss.pos p ON pp.pos_id = p.pos_id
            JOIN device.device d ON p.device_id = d.device_id
            WHERE pp.pos_pta_id = ln_pos_pta_id;

        INSERT INTO PSS.TRAN_LINE_ITEM (
            TRAN_ID,
            TRAN_LINE_ITEM_AMOUNT,
            TRAN_LINE_ITEM_POSITION_CD,
            TRAN_LINE_ITEM_TAX,
            TRAN_LINE_ITEM_TYPE_ID,
            TRAN_LINE_ITEM_QUANTITY,
            TRAN_LINE_ITEM_DESC,
            TRAN_LINE_ITEM_BATCH_TYPE_CD,
            SALE_RESULT_ID,
            APPLY_TO_CONSUMER_ACCT_ID
        ) VALUES (
            ln_tran_id,
            -ln_refund_amt,
            '0',
            0,
            500,
            1,
            'Prepaid account closure',
            PKG_CONST.TRAN_BATCH_TYPE__ACTUAL,
            PKG_CONST.SALE_RES__SUCCESS,
            pn_consumer_acct_id
        );

        INSERT INTO PSS.REFUND (
            TRAN_ID,
            REFUND_AMT,
            REFUND_DESC,
            REFUND_ISSUE_TS,
            REFUND_ISSUE_BY,
            REFUND_TYPE_CD,
            REFUND_STATE_ID,
            ACCT_ENTRY_METHOD_CD,
            CARD_KEY
        ) VALUES (
            ln_tran_id,
            -ln_refund_amt,
            'Prepaid account closure',
            ld_refund_ts,
            pv_refund_issue_by,
            'G',
            6,
            2,
            lv_card_key);

        pn_refund_amt := ln_refund_amt;
    END IF;

    IF ln_eft_credit_amt > 0 THEN
        CORP.PAYMENTS_PKG.ADJUSTMENT_INS(ln_corp_customer_id, pv_refund_issue_by, lv_currency_cd,
            'Promo credit for prepaid account closure, card # ' || lv_consumer_acct_cd || ', card ID ' || lv_consumer_acct_identifier,
            ln_eft_credit_amt, pn_doc_id, ln_ledger_id);
        pn_eft_credit_amt := ln_eft_credit_amt;
    END IF;
END;

PROCEDURE PROCESS_ISIS_TRAN (
    pn_tran_id PSS.TRAN.TRAN_ID%TYPE
)
IS
    ln_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE;
    lv_isis_promo_status_cd PSS.PROMOTION.STATUS_CD%TYPE;
    ln_tran_diff PSS.CONSUMER_PROMOTION.TRAN_COUNT%TYPE;
    lv_tran_info PSS.TRAN.TRAN_INFO%TYPE;
    lv_payment_subtype_class PSS.TRAN.PAYMENT_SUBTYPE_CLASS%TYPE;
    lv_auth_resp_cd PSS.AUTH.AUTH_RESP_CD%TYPE;
    ln_sale_amount PSS.SALE.SALE_AMOUNT%TYPE;
    ln_isis_promo_pos_pta_id PSS.POS_PTA.POS_PTA_ID%TYPE;
    ld_sysdate DATE := SYSDATE;
BEGIN
    SELECT NVL(MAX(STATUS_CD), 'D')
    INTO lv_isis_promo_status_cd
    FROM PSS.PROMOTION
    WHERE PROMOTION_ID = 1;

    IF lv_isis_promo_status_cd != 'A' THEN
        RETURN;
    END IF;

    SELECT MAX(CA.CONSUMER_ID), MAX(S.SALE_AMOUNT), NVL(MAX(T.TRAN_INFO), '-'), NVL(MAX(T.PAYMENT_SUBTYPE_CLASS), '-'), NVL(MAX(A.AUTH_RESP_CD), '-'), NVL(MAX(PP2.POS_PTA_ID), 0)
    INTO ln_consumer_id, ln_sale_amount, lv_tran_info, lv_payment_subtype_class, lv_auth_resp_cd, ln_isis_promo_pos_pta_id
    FROM PSS.TRAN T
    JOIN PSS.SALE S ON T.TRAN_ID = S.TRAN_ID
    JOIN PSS.CONSUMER_ACCT CA ON T.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
    JOIN PSS.POS_PTA PP ON T.POS_PTA_ID = PP.POS_PTA_ID
    JOIN PSS.PAYMENT_SUBTYPE PS ON PP.PAYMENT_SUBTYPE_ID = PS.PAYMENT_SUBTYPE_ID
    LEFT OUTER JOIN PSS.AUTH A ON T.TRAN_ID = A.TRAN_ID AND A.AUTH_TYPE_CD = 'N' AND A.AUTH_STATE_ID IN (2, 5)
    LEFT OUTER JOIN (PSS.POS_PTA PP2
        JOIN PSS.PAYMENT_SUBTYPE PS2 ON PP2.PAYMENT_SUBTYPE_ID = PS2.PAYMENT_SUBTYPE_ID AND PS2.PAYMENT_SUBTYPE_CLASS = 'Isis'
    ) ON PP.POS_ID = PP2.POS_ID AND PP2.POS_PTA_ACTIVATION_TS < ld_sysdate AND (PP2.POS_PTA_DEACTIVATION_TS IS NULL OR PP2.POS_PTA_DEACTIVATION_TS > ld_sysdate)
        AND NVL(PP.AUTHORITY_PAYMENT_MASK_ID, PS.AUTHORITY_PAYMENT_MASK_ID) = NVL(PP2.AUTHORITY_PAYMENT_MASK_ID, PS2.AUTHORITY_PAYMENT_MASK_ID)
        AND (T.PAYMENT_SUBTYPE_CLASS = 'Isis' OR PP.POS_PTA_ID != PP2.POS_PTA_ID)
    WHERE T.TRAN_ID = pn_tran_id;

    IF ln_consumer_id IS NULL OR ln_sale_amount IS NULL OR lv_tran_info LIKE '%Softcard loyalty updated%' OR ln_isis_promo_pos_pta_id < 1 THEN
        RETURN;
    END IF;

    IF ln_sale_amount > 0 THEN
        IF lv_payment_subtype_class != 'Isis' THEN
            LOOP
                UPDATE PSS.CONSUMER_PROMOTION
                SET TRAN_COUNT = TRAN_COUNT + 1
                WHERE CONSUMER_ID = ln_consumer_id AND PROMOTION_ID = 1;

                IF SQL%FOUND THEN
                    EXIT;
                END IF;

                BEGIN
                    INSERT INTO PSS.CONSUMER_PROMOTION(CONSUMER_ID, PROMOTION_ID, TRAN_COUNT)
                    VALUES(ln_consumer_id, 1, 1);
                    EXIT;
                EXCEPTION
                    WHEN DUP_VAL_ON_INDEX THEN
                        NULL;
                END;
            END LOOP;
        END IF;
    ELSE
        IF lv_payment_subtype_class = 'Isis' AND lv_auth_resp_cd = 'ISIS_PROMO' THEN
            UPDATE PSS.CONSUMER_PROMOTION
            SET TRAN_COUNT = CASE WHEN TRAN_COUNT - 1 >= 0 THEN TRAN_COUNT - 1 ELSE 0 END,
                PROMO_TRAN_COUNT = CASE WHEN PROMO_TRAN_COUNT - 1 >= 0 THEN PROMO_TRAN_COUNT - 1 ELSE 0 END
            WHERE CONSUMER_ID = ln_consumer_id AND PROMOTION_ID = 1;
        END IF;
    END IF;

    UPDATE PSS.TRAN
    SET TRAN_INFO = SUBSTR('Softcard loyalty updated' || DECODE(TRAN_INFO, NULL, '', ';' || TRAN_INFO), 1, 1000)
    WHERE TRAN_ID = pn_tran_id AND NVL(TRAN_INFO, '-') NOT LIKE '%Softcard loyalty updated%';
END;

-- R33+ signature
PROCEDURE sp_create_sale(
    pc_global_event_cd_prefix IN CHAR,
    pv_device_name IN device.device_name%TYPE,
    pv_device_tran_cd IN pss.tran.tran_device_tran_cd%TYPE,
    pn_device_batch_id IN pss.sale.device_batch_id%TYPE,
    pc_sale_type_cd IN pss.sale.sale_type_cd%TYPE,
    pn_sale_utc_ts_ms IN NUMBER,
    pn_sale_utc_offset_min IN pss.sale.sale_utc_offset_min%TYPE,
    pv_tran_device_result_type_cd IN pss.tran.tran_device_result_type_cd%TYPE,
    pn_sale_result_id IN pss.sale.sale_result_id%TYPE,
    pn_sale_amount IN NUMBER,
    pc_receipt_result_cd IN pss.sale.receipt_result_cd%TYPE,
    pc_tran_batch_type_cd IN pss.tran_line_item_batch_type.tran_line_item_batch_type_cd%TYPE,
    pv_hash_type_cd IN pss.sale.hash_type_cd%TYPE,
    pv_tran_line_item_hash IN pss.sale.tran_line_item_hash%TYPE,
    pv_global_session_cd IN VARCHAR2,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_tran_id OUT pss.tran.tran_id%TYPE,
    pv_tran_state_cd OUT pss.tran.tran_state_cd%TYPE,
    pc_void_allowed IN PSS.SALE.VOID_ALLOWED%TYPE DEFAULT 'N')
IS
/*
    Returned result codes:
        RESULT__SUCCESS
        RESULT__FAILURE
        RESULT__INVALID_PARAMETER
        RESULT__DUPLICATE
*/
    lv_global_trans_cd pss.tran.tran_global_trans_cd%TYPE;
    ld_tran_upload_ts pss.tran.tran_upload_ts%TYPE;
    lt_sale_start_utc_ts pss.sale.sale_start_utc_ts%TYPE;
    ld_tran_start_ts pss.tran.tran_start_ts%TYPE;
    ld_original_tran_start_ts pss.tran.tran_start_ts%TYPE;
    ld_tran_server_ts DATE;
    ln_device_id device.device_id%TYPE;
	ln_device_type_id device.device_type_id%TYPE;
	lv_device_sub_type_desc device_sub_type.device_sub_type_desc%TYPE;
    ln_pos_pta_id pss.tran.pos_pta_id%TYPE;
    ld_current_ts DATE := SYSDATE;
    lc_client_payment_type_cd pss.client_payment_type.client_payment_type_cd%TYPE;
    ln_tran_exists NUMBER := PKG_CONST.BOOLEAN__FALSE;
    ln_tli_hash_match NUMBER;
    ln_insert_tran NUMBER := PKG_CONST.BOOLEAN__FALSE;
    ln_original_tran_id pss.tran.tran_id%TYPE;
    ln_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE;
    lc_sale_type_cd pss.sale.sale_type_cd%TYPE;
    lv_last_lock_utc_ts VARCHAR2(128);
    lc_auth_hold_used PSS.TRAN.AUTH_HOLD_USED%TYPE;
    lv_orig_tran_state_cd pss.tran.tran_state_cd%TYPE;
    ln_consumer_acct_id pss.tran.consumer_acct_id%TYPE;
    ln_auth_amt_approved pss.auth.auth_amt_approved%TYPE;
    ln_auth_amt_allowed pss.auth.auth_amt_approved%TYPE;
    ln_sale_over_auth_amt_percent NUMBER;
    lv_email_from_address engine.app_setting.app_setting_value%TYPE;
    lv_email_to_address engine.app_setting.app_setting_value%TYPE;
    lv_error pss.tran.tran_info%TYPE;
    lc_original_void_allowed PSS.SALE.VOID_ALLOWED%TYPE;
    lv_card_key PSS.AUTH.CARD_KEY%TYPE;
    ln_original_sale_amount PSS.SALE.SALE_AMOUNT%TYPE;
    lc_entry_method_cd PSS.AUTH.ACCT_ENTRY_METHOD_CD%TYPE;
    lc_calc_tran_state_cd CHAR(1) := 'Y';
    ln_host_id PSS.TRAN_LINE_ITEM.HOST_ID%TYPE;
    ln_new_host_count PLS_INTEGER;
    lc_orig_term_capture_flag CHAR(1);
    ln_count PLS_INTEGER;
    ln_remaining_refund_amt PSS.REFUND.REFUND_AMT%TYPE;
    lv_payment_subtype_class pss.payment_subtype.payment_subtype_class%TYPE;
    l_promo_pass_through NUMBER;
BEGIN
    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
    pn_tran_id := 0;

    IF pc_global_event_cd_prefix NOT IN (PKG_CONST.EVENT_CODE_PREFIX__APP_LAYER,
        PKG_CONST.EVENT_CODE_PREFIX__LEGACY) THEN
        pn_result_cd := PKG_CONST.RESULT__INVALID_PARAMETER;
        pv_error_message := 'Invalid pc_global_event_cd_prefix: ' || pc_global_event_cd_prefix;
        RETURN;
    END IF;

    lv_global_trans_cd := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(pc_global_event_cd_prefix, pv_device_name, pv_device_tran_cd);

    lt_sale_start_utc_ts := CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_sale_utc_ts_ms) AS DATE);
    ld_tran_start_ts := CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_sale_utc_ts_ms + pn_sale_utc_offset_min * 60 * 1000) AS DATE);
    ld_tran_server_ts := CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_sale_utc_ts_ms) - SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) + CURRENT_TIMESTAMP AS DATE);

    lv_last_lock_utc_ts := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', pv_device_name || ':' || pv_device_tran_cd);

    BEGIN
        SELECT tran_id, tran_state_cd, tran_start_ts, tran_upload_ts, PKG_CONST.BOOLEAN__TRUE, tli_hash_match,
               pos_pta_id, sale_type_cd, auth_hold_used, consumer_acct_id, VOID_ALLOWED,
               SALE_AMOUNT
        INTO pn_tran_id, pv_tran_state_cd, ld_original_tran_start_ts, ld_tran_upload_ts, ln_tran_exists, ln_tli_hash_match,
             ln_pos_pta_id, lc_sale_type_cd, lc_auth_hold_used, ln_consumer_acct_id, lc_original_void_allowed,
             ln_original_sale_amount
        FROM
        (
            SELECT /*+ INDEX(t IDX_DEVICE_TRAN_CD) */ t.tran_id, t.tran_state_cd, t.tran_start_ts, t.tran_upload_ts,
                CASE WHEN s.hash_type_cd = pv_hash_type_cd
                    AND s.tran_line_item_hash = pv_tran_line_item_hash
                    AND s.sale_type_cd = pc_sale_type_cd THEN PKG_CONST.BOOLEAN__TRUE
                ELSE PKG_CONST.BOOLEAN__FALSE END AS tli_hash_match,
                t.pos_pta_id, s.sale_type_cd, NVL(t.auth_hold_used, 'N') auth_hold_used, t.consumer_acct_id,
                s.VOID_ALLOWED, s.SALE_AMOUNT
            FROM pss.tran t
            LEFT OUTER JOIN pss.sale s ON t.tran_id = s.tran_id
            LEFT OUTER JOIN pss.auth aa ON t.tran_id = aa.tran_id AND aa.auth_type_cd = 'N'
            WHERE t.tran_device_tran_cd = pv_device_tran_cd
              AND (t.tran_global_trans_cd = lv_global_trans_cd OR t.tran_global_trans_cd LIKE lv_global_trans_cd || ':%')
              AND aa.auth_action_id IS NULL
            ORDER BY CASE WHEN s.sale_type_cd = pc_sale_type_cd THEN 1
                          WHEN s.sale_type_cd IN('A', 'I') AND pc_sale_type_cd IN('A', 'I') THEN 2
                          WHEN s.sale_type_cd IS NULL AND pc_sale_type_cd IN('A', 'I') THEN 3
                          ELSE 4 END,
                tli_hash_match DESC, CASE WHEN t.tran_global_trans_cd = lv_global_trans_cd THEN 1 ELSE 2 END,
                t.tran_start_ts, t.created_ts
        )
        WHERE ROWNUM = 1;

        ln_original_tran_id := pn_tran_id;
        lv_orig_tran_state_cd := pv_tran_state_cd;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            ln_insert_tran := PKG_CONST.BOOLEAN__TRUE;
    END;

    -- Handle each case
    IF ln_tran_exists = PKG_CONST.BOOLEAN__TRUE THEN
        IF ln_tli_hash_match = PKG_CONST.BOOLEAN__TRUE THEN
            SELECT c.MINOR_CURRENCY_FACTOR
            INTO ln_minor_currency_factor
            FROM PSS.POS_PTA PTA
            JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
            WHERE PTA.POS_PTA_ID = ln_pos_pta_id;
            IF ln_original_sale_amount * ln_minor_currency_factor = pn_sale_amount THEN
                UPDATE pss.sale
                   SET duplicate_count = duplicate_count + 1
                 WHERE tran_id = pn_tran_id;

                pv_tran_state_cd := PKG_CONST.TRAN_STATE__DUPLICATE;
                pn_result_cd := PKG_CONST.RESULT__DUPLICATE;
                pv_error_message := 'Duplicate sale, original tran_id: ' || pn_tran_id;
                RETURN;
            END IF;
        END IF;
        IF pc_sale_type_cd = PKG_CONST.SALE_TYPE__CASH THEN
            lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CASH;
            pv_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE;
            ln_insert_tran := PKG_CONST.BOOLEAN__TRUE;
        ELSIF lc_sale_type_cd = PKG_CONST.SALE_TYPE__CASH THEN
            ln_insert_tran := PKG_CONST.BOOLEAN__TRUE;
            IF pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED THEN
                lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CREDIT;
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND;
            ELSIF pn_sale_result_id != 0 /* Success */ AND NVL(pn_sale_amount, 0) = 0 THEN
                lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CREDIT;
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED;
            ELSE
                lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CREDIT;
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__SALE_NO_AUTH;
            END IF;
        ELSIF lc_sale_type_cd = PKG_CONST.SALE_TYPE__ACTUAL AND pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED THEN
            RETURN; -- ignore this as we have already processd the actual sale
        ELSIF lc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED AND pc_sale_type_cd = PKG_CONST.SALE_TYPE__ACTUAL THEN
            NULL; -- just update the transaction
        ELSIF lc_sale_type_cd IS NOT NULL THEN
            lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CREDIT;
            IF NVL(ln_original_sale_amount, 0) = 0 AND pc_void_allowed = 'Y' THEN
                pn_result_cd := PKG_CONST.RESULT__SALE_VOIDED;
                RETURN; -- we have already processed the cancel of this charged sale
            ELSIF (pv_tran_device_result_type_cd IN (PKG_CONST.TRAN_DEV_RES__CANCELLED,
                PKG_CONST.TRAN_DEV_RES__AUTH_FAILURE,
                PKG_CONST.TRAN_DEV_RES__FAILURE,
                PKG_CONST.TRAN_DEV_RES__TIMEOUT) OR pn_sale_amount <= 0)
                AND lc_original_void_allowed = 'Y'
                AND NVL(ln_original_sale_amount, 0) > 0
                AND pv_tran_state_cd IN(
                    PKG_CONST.TRAN_STATE__SALE_NO_AUTH,
                    PKG_CONST.TRAN_STATE__BATCH,
                    PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND,
                    PKG_CONST.TRAN_STATE__BATCH_INTENDED,
                    PKG_CONST.TRAN_STATE__PROCESSED_TRAN,
                    PKG_CONST.TRAN_STATE__PROCESSED_TRAN_INT,
                    PKG_CONST.TRAN_STATE__PROCESSING_TRAN,
                    PKG_CONST.TRAN_STATE__INCOMPLETE,
                    PKG_CONST.TRAN_STATE__INCOMPLETE_ERROR,
                    PKG_CONST.TRAN_STATE__PROCESSING_STTLMT,
                    PKG_CONST.TRAN_STATE__SETTLEMENT,
                    PKG_CONST.TRAN_STATE__COMPLETE,
                    PKG_CONST.TRAN_STATE__STLMT_INCOMPLETE,
                    PKG_CONST.TRAN_STATE__STLMT_ERROR,
                    PKG_CONST.TRAN_STATE__PROCESSING_BATCH,
                    PKG_CONST.TRAN_STATE__PRCSNG_TRAN_RETRY) THEN -- cancel of a charge sale

                -- Lock tran row by updating it
                UPDATE PSS.TRAN
                   SET TRAN_STATE_CD = pv_tran_state_cd
                 WHERE TRAN_ID = pn_tran_id
                   AND TRAN_STATE_CD = pv_tran_state_cd;
                IF SQL%NOTFOUND THEN
                    RAISE_APPLICATION_ERROR(-20120, 'Tran State Cd changed while processing tran ' || pn_tran_id || '; please retry');
                END IF;
                -- figure out what to do based on current transaction state
                IF pv_tran_state_cd IN(
                    PKG_CONST.TRAN_STATE__SALE_NO_AUTH,
                    PKG_CONST.TRAN_STATE__BATCH,
                    PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND,
                    PKG_CONST.TRAN_STATE__BATCH_INTENDED) THEN
                    lc_calc_tran_state_cd := 'N'; -- easy case: update tran and line items and leave state as is
                ELSE
                    IF pv_tran_state_cd IN(PKG_CONST.TRAN_STATE__PROCESSED_TRAN) THEN
                        SELECT MAX(TB.TERMINAL_CAPTURE_FLAG)
                          INTO lc_orig_term_capture_flag
                          FROM PSS.TERMINAL_BATCH TB
                          JOIN PSS.AUTH A ON A.TERMINAL_BATCH_ID = TB.TERMINAL_BATCH_ID
                         WHERE TB.TERMINAL_BATCH_CLOSE_TS IS NULL
                          AND A.TRAN_ID = pn_tran_id;
                    END IF;
                    IF pv_tran_state_cd IN(PKG_CONST.TRAN_STATE__PROCESSED_TRAN) AND lc_orig_term_capture_flag = 'Y' THEN
                        IF lc_auth_hold_used = 'Y' THEN
                            UPDATE PSS.AUTH
                               SET AUTH_TYPE_CD = 'C',
                                   AUTH_AMT = 0
                             WHERE TRAN_ID = pn_tran_id
                               AND AUTH_TYPE_CD = 'U';
                            lc_calc_tran_state_cd := 'N'; -- easy case: update tran and line items and leave state as is
                        ELSE --Remove from batch and cancel
                            UPDATE PSS.AUTH A
                               SET TERMINAL_BATCH_ID = NULL
                             WHERE A.TRAN_ID = pn_tran_id
                               AND A.TERMINAL_BATCH_ID IS NOT NULL
                               AND (SELECT TB.TERMINAL_BATCH_CLOSE_TS FROM PSS.TERMINAL_BATCH TB WHERE TB.TERMINAL_BATCH_ID = A.TERMINAL_BATCH_ID) IS NULL;
                            pv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED;
                            lc_calc_tran_state_cd := 'N';
                        END IF;
                    ELSE -- create a refund and return
                        ln_original_tran_id := pn_tran_id;
                        pv_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH;
                        SELECT ABS(NVL(SUM(R.REFUND_AMT), 0)) - ABS(ln_original_sale_amount), MAX(T.TRAN_ID)
                          INTO ln_remaining_refund_amt, pn_tran_id
                          FROM PSS.TRAN T
                          JOIN PSS.REFUND R ON T.TRAN_ID = R.TRAN_ID
                         WHERE T.PARENT_TRAN_ID = ln_original_tran_id;
                        IF ln_remaining_refund_amt < 0 THEN
                            SELECT pss.seq_tran_id.NEXTVAL INTO pn_tran_id FROM DUAL;
                            INSERT INTO PSS.TRAN (
                                    TRAN_ID,
                                    PARENT_TRAN_ID,
                                    TRAN_START_TS,
                                    TRAN_END_TS,
                                    TRAN_UPLOAD_TS,
                                    TRAN_GLOBAL_TRANS_CD,
                                    TRAN_STATE_CD,
                                    CONSUMER_ACCT_ID,
                                    TRAN_DEVICE_TRAN_CD,
                                    POS_PTA_ID,
                                    TRAN_DEVICE_RESULT_TYPE_CD,
                                    TRAN_RECEIVED_RAW_ACCT_DATA,
                                    PAYMENT_SUBTYPE_KEY_ID,
                                    PAYMENT_SUBTYPE_CLASS,
                                    CLIENT_PAYMENT_TYPE_CD,
                                    DEVICE_NAME)
                             SELECT pn_tran_id,
                                    ln_original_tran_id,
                                    ld_tran_start_ts,
                                    ld_tran_start_ts,
                                    ld_current_ts, /* Must NOT be NULL so that it will be imported */
                                    'RF' || SUBSTR(O.TRAN_GLOBAL_TRANS_CD, INSTR(O.TRAN_GLOBAL_TRANS_CD, ':'), 56) || ':1',
                                    pv_tran_state_cd,
                                    ln_consumer_acct_id,
                                    pv_device_tran_cd,
                                    O.POS_PTA_ID,
                                    pv_tran_device_result_type_cd,
                                    O.TRAN_RECEIVED_RAW_ACCT_DATA,
                                    pp.PAYMENT_SUBTYPE_KEY_ID,
                                    ps.PAYMENT_SUBTYPE_CLASS,
                                    ps.CLIENT_PAYMENT_TYPE_CD,
                                    d.DEVICE_NAME
                            FROM PSS.TRAN O
                            JOIN pss.pos_pta pp ON O.pos_pta_id = pp.pos_pta_id
                            JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
                            JOIN pss.pos p ON pp.pos_id = p.pos_id
                            JOIN device.device d ON p.device_id = d.device_id
                            WHERE O.TRAN_ID = ln_original_tran_id;
                            SELECT MAX(ACCT_ENTRY_METHOD_CD)
                              INTO lc_entry_method_cd
                              FROM (SELECT ACCT_ENTRY_METHOD_CD
                                      FROM PSS.AUTH
                                     WHERE TRAN_ID = ln_original_tran_id
                                     ORDER BY DECODE(AUTH_TYPE_CD, 'N', 1, 5), AUTH_RESULT_CD DESC, AUTH_TS, AUTH_ID)
                             WHERE ROWNUM = 1;
                            SELECT p.DEVICE_ID
                              INTO ln_device_id
                              FROM PSS.POS_PTA PTA
                              JOIN PSS.POS P ON PTA.POS_ID = P.POS_ID
                             WHERE PTA.POS_PTA_ID = ln_pos_pta_id;

                            INSERT INTO PSS.REFUND (
                                    TRAN_ID,
                                    REFUND_AMT,
                                    REFUND_DESC,
                                    REFUND_ISSUE_TS,
                                    REFUND_ISSUE_BY,
                                    REFUND_TYPE_CD,
                                    REFUND_STATE_ID,
                                    ACCT_ENTRY_METHOD_CD
                                ) VALUES (
                                    pn_tran_id,
                                    ln_remaining_refund_amt,
                                    'Void of Charged Sale',
                                    ld_current_ts,
                                    'PSS',
                                    'V',
                                    6,
                                    lc_entry_method_cd);
                            -- Insert line item
                            ln_host_id := sf_find_host_id(ln_device_id, 0, 0);
                            IF ln_host_id IS NULL THEN
                                -- create default hosts
                                pkg_device_configuration.sp_create_default_hosts(ln_device_id, ln_new_host_count, pn_result_cd, pv_error_message);
                                IF pn_result_cd != PKG_CONST.RESULT__SUCCESS THEN
                                    RETURN;
                                END IF;
                                ln_host_id := sf_find_host_id(ln_device_id, 0, 0);
                            END IF;
                            INSERT INTO pss.tran_line_item (
                                tran_line_item_id,
                                tran_id,
                                tran_line_item_amount,
                                tran_line_item_position_cd,
                                tran_line_item_tax,
                                tran_line_item_type_id,
                                tran_line_item_quantity,
                                tran_line_item_desc,
                                host_id,
                                tran_line_item_batch_type_cd,
                                tran_line_item_ts,
                                sale_result_id
                            ) VALUES (
                                PSS.SEQ_TLI_ID.NEXTVAL,
                                pn_tran_id,
                                ln_remaining_refund_amt,
                                NULL,
                                NULL,
                                312, /*Cancellation Adjustment */
                                1,
                                'Void of Charged Sale',
                                ln_host_id,
                                'A',
                                ld_current_ts,
                                0);
                        END IF;
                        pn_result_cd := PKG_CONST.RESULT__SALE_VOIDED;
                        pv_error_message := 'Refund issued for canceled transaction already in-process or settled, refund tran_id: ' || pn_tran_id || ', original tran_id: ' || ln_original_tran_id;
                        RETURN;
                    END IF;
                END IF;
            ELSE
                SELECT CASE WHEN (lc_auth_hold_used = 'Y' OR NVL(SUM(DECODE(TLI.TRAN_LINE_ITEM_BATCH_TYPE_CD, 'I', TLI.TRAN_LINE_ITEM_AMOUNT * TLI.TRAN_LINE_ITEM_QUANTITY)), 0) > 0) AND NVL(SUM(DECODE(TLI.TRAN_LINE_ITEM_BATCH_TYPE_CD, 'A', TLI.TRAN_LINE_ITEM_AMOUNT * TLI.TRAN_LINE_ITEM_QUANTITY)), 0) = 0
                            THEN PKG_CONST.TRAN_STATE__COMPLETE_ERROR
                            ELSE PKG_CONST.TRAN_STATE__DUPLICATE
                       END
                  INTO pv_tran_state_cd
                  FROM PSS.TRAN_LINE_ITEM TLI
                 WHERE TLI.TRAN_ID = pn_tran_id;
                ln_insert_tran := PKG_CONST.BOOLEAN__TRUE;
                IF pv_tran_state_cd = PKG_CONST.TRAN_STATE__DUPLICATE THEN
                    pn_result_cd := PKG_CONST.RESULT__DUPLICATE;
                    pv_error_message := 'Duplicate sale with different line items, original tran_id: ' || ln_original_tran_id;
                END IF;
            END IF;
        ELSE
            NULL; -- just update the transaction
        END IF;
    END IF;

    ln_device_id := PKG_DEVICE_CONFIGURATION.GET_DEVICE_ID_BY_NAME(pv_device_name, ld_tran_server_ts);

    IF ln_insert_tran = PKG_CONST.BOOLEAN__TRUE THEN
        IF lc_client_payment_type_cd IS NULL THEN
            IF pc_sale_type_cd = PKG_CONST.SALE_TYPE__CASH THEN
                lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CASH;
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE;
            ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED THEN
                lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CREDIT;
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND;
            ELSIF pn_sale_result_id != 0 /* Success */ AND NVL(pn_sale_amount, 0) = 0 THEN
                lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CREDIT;
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED;
            ELSE
                lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CREDIT;
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__SALE_NO_AUTH;
            END IF;
        END IF;
        SELECT pss.seq_tran_id.NEXTVAL INTO pn_tran_id FROM DUAL;

        IF ln_tran_exists = PKG_CONST.BOOLEAN__TRUE THEN
            lv_global_trans_cd := lv_global_trans_cd || ':' || pn_tran_id;
        END IF;

        IF pc_sale_type_cd = PKG_CONST.SALE_TYPE__CASH THEN
            PKG_POS_PTA.SP_GET_OR_CREATE_POS_PTA(ln_device_id, pv_device_name, lc_client_payment_type_cd, lt_sale_start_utc_ts, pn_result_cd, pv_error_message, ln_pos_pta_id);
        ELSE
            PKG_POS_PTA.SP_GET_OR_CREATE_ERR_POS_PTA(ln_device_id, pv_device_name, lc_client_payment_type_cd, lt_sale_start_utc_ts, pn_result_cd, pv_error_message, ln_pos_pta_id);
        END IF;

        SELECT c.MINOR_CURRENCY_FACTOR
        INTO ln_minor_currency_factor
        FROM PSS.POS_PTA PTA
        JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
        WHERE PTA.POS_PTA_ID = ln_pos_pta_id;

        INSERT INTO pss.tran (
            tran_id,
            tran_start_ts,
            tran_end_ts,
            tran_upload_ts,
            tran_state_cd,
            tran_device_tran_cd,
            pos_pta_id,
            tran_global_trans_cd,
            tran_device_result_type_cd,
            payment_subtype_key_id,
            payment_subtype_class,
            client_payment_type_cd,
            device_name
        ) SELECT
            pn_tran_id,
            ld_tran_start_ts,
            ld_tran_start_ts,
            ld_current_ts,
            pv_tran_state_cd,
            pv_device_tran_cd,
            ln_pos_pta_id,
            lv_global_trans_cd,
            pv_tran_device_result_type_cd,
            pp.payment_subtype_key_id,
            ps.payment_subtype_class,
            ps.client_payment_type_cd,
            pv_device_name
        FROM pss.pos_pta pp
        JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
        WHERE pp.pos_pta_id = ln_pos_pta_id;
        IF INSTR(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('MASTER_ID_UPDATE_SALE_TYPES'), pc_sale_type_cd) > 0 AND TO_NUMBER_OR_NULL(pv_device_tran_cd) < DBADMIN.DATE_TO_MILLIS(SYSDATE + 365) / 1000 THEN
            UPDATE DEVICE.DEVICE_SETTING
               SET DEVICE_SETTING_VALUE = pv_device_tran_cd
             WHERE DEVICE_ID = ln_device_id
               AND DEVICE_SETTING_PARAMETER_CD = '60' -- Master Id
               AND TO_NUMBER_OR_NULL(NVL(DEVICE_SETTING_VALUE, '0')) < TO_NUMBER_OR_NULL(pv_device_tran_cd);
        END IF;
    ELSE -- logic to determine pv_tran_state_cd
        SELECT c.MINOR_CURRENCY_FACTOR
        INTO ln_minor_currency_factor
        FROM PSS.POS_PTA PTA
        JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
        WHERE PTA.POS_PTA_ID = ln_pos_pta_id;

        IF lc_calc_tran_state_cd = 'Y' THEN
            IF pv_tran_device_result_type_cd IN (PKG_CONST.TRAN_DEV_RES__CANCELLED,
                    PKG_CONST.TRAN_DEV_RES__AUTH_FAILURE,
                    PKG_CONST.TRAN_DEV_RES__FAILURE,
                    PKG_CONST.TRAN_DEV_RES__TIMEOUT) OR pn_sale_amount <= 0 THEN
                IF pv_tran_state_cd IN (
                        PKG_CONST.TRAN_STATE__AUTH_SUCCESS,
                        PKG_CONST.TRAN_STATE__AUTH_SUCCESS_COND,
                        PKG_CONST.TRAN_STATE__BATCH_INTENDED,
                        PKG_CONST.TRAN_STATE__PROCESSED_TRAN_INT) THEN
                    IF lc_auth_hold_used = 'Y' OR pv_tran_state_cd = PKG_CONST.TRAN_STATE__PROCESSED_TRAN_INT THEN
                        IF pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED THEN
                            pv_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH_INTENDED;
                        ELSE
                            pv_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH;
                        END IF;
                    ELSE
                        pv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED;
                    END IF;
                ELSIF pv_tran_state_cd IN (
                        PKG_CONST.TRAN_STATE__AUTH_EXPIRED,
                        PKG_CONST.TRAN_STATE__AUTH_COND_EXPIRED) THEN
                    pv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED; -- Reversal not available since auth is expired
                ELSIF pv_tran_state_cd IN (
                        PKG_CONST.TRAN_STATE__AUTH_DECLINE,
                        PKG_CONST.TRAN_STATE__AUTH_FAILURE) THEN
                    pv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED; -- We already determined that that no reversal is needed
                ELSIF pv_tran_state_cd IN (
                        PKG_CONST.TRAN_STATE__AUTH_REVERSED,
                        PKG_CONST.TRAN_STATE__AUTH_PEND_RVRSL,
                        PKG_CONST.TRAN_STATE__COMPLETE_ERROR,
                        PKG_CONST.TRAN_STATE__PROCESSED_TRAN,
                        PKG_CONST.TRAN_STATE__PROCESSING_TRAN,
                        PKG_CONST.TRAN_STATE__INCOMPLETE,
                        PKG_CONST.TRAN_STATE__INCOMPLETE_ERROR,
                        PKG_CONST.TRAN_STATE__PROCESSING_STTLMT,
                        PKG_CONST.TRAN_STATE__STLMT_INCOMPLETE,
                        PKG_CONST.TRAN_STATE__STLMT_ERROR) THEN
                   -- don't change it
                   NULL;
                ELSE
                    pv_error_message := 'Bad tran state for a cancelled cashless sale, tran_id: ' || pn_tran_id || ', tran_state_cd: ' || pv_tran_state_cd;
                    pv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED;
                END IF;
            ELSIF pv_tran_state_cd IN (
                    PKG_CONST.TRAN_STATE__AUTH_EXPIRED,
                    PKG_CONST.TRAN_STATE__AUTH_COND_EXPIRED)
                 OR (pv_tran_state_cd IN (
                    PKG_CONST.TRAN_STATE__AUTH_SUCCESS,
                    PKG_CONST.TRAN_STATE__AUTH_SUCCESS_COND)
                    AND ld_original_tran_start_ts < ld_current_ts - 8) THEN
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE_ERROR;
            ELSIF pv_tran_state_cd IN (
                    PKG_CONST.TRAN_STATE__AUTH_DECLINE,
                    PKG_CONST.TRAN_STATE__AUTH_FAILURE,
                    PKG_CONST.TRAN_STATE__AUTH_REVERSED,
                    PKG_CONST.TRAN_STATE__AUTH_PEND_RVRSL,
                    PKG_CONST.TRAN_STATE__INTENDED_ERROR) THEN
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE_ERROR;
                pv_error_message := 'Received a cashless sale for an unsuccessful auth, tran_id: ' || pn_tran_id || ', tran_state_cd: ' || pv_tran_state_cd;
            ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED
                AND pv_tran_device_result_type_cd IN (
                    PKG_CONST.TRAN_DEV_RES__INCOMPLETE,
                    PKG_CONST.TRAN_DEV_RES__SUCCESS,
                    PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_PRNTR,
                    PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_RCPT,
                    PKG_CONST.TRAN_DEV_RES__SUCCESS_RCPT_ERR) THEN
                IF pv_tran_state_cd IN (PKG_CONST.TRAN_STATE__AUTH_SUCCESS, PKG_CONST.TRAN_STATE__AUTH_SUCCESS_COND) THEN
                    -- normal case
                    pv_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH_INTENDED;
                    -- insert sale record
                ELSE
                    -- sale actual uploaded
                    -- don't change tran_state_cd
                    -- don't update sale record
                    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
                    pv_error_message := 'Actual uploaded before intended';
                    RETURN;
                END IF;
            -- we must let POSM processed cancelled sales too to do auth reversal
            ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__ACTUAL
                AND pv_tran_state_cd IN (
                    PKG_CONST.TRAN_STATE__AUTH_SUCCESS,
                    PKG_CONST.TRAN_STATE__AUTH_SUCCESS_COND,
                    PKG_CONST.TRAN_STATE__PROCESSING_BATCH,
                    PKG_CONST.TRAN_STATE__PRCSNG_BATCH_INTD,
                    PKG_CONST.TRAN_STATE__PRCSNG_BATCH_LOCAL,
                    PKG_CONST.TRAN_STATE__PROCESSED_TRAN_INT,
                    PKG_CONST.TRAN_STATE__BATCH_INTENDED)
                AND pv_tran_device_result_type_cd IN (
                    PKG_CONST.TRAN_DEV_RES__AUTH_FAILURE,
                    PKG_CONST.TRAN_DEV_RES__CANCELLED,
                    PKG_CONST.TRAN_DEV_RES__FAILURE,
                    PKG_CONST.TRAN_DEV_RES__INCOMPLETE,
                    PKG_CONST.TRAN_DEV_RES__SUCCESS,
                    PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_PRNTR,
                    PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_RCPT,
                    PKG_CONST.TRAN_DEV_RES__SUCCESS_RCPT_ERR,
                    PKG_CONST.TRAN_DEV_RES__TIMEOUT) THEN
                IF pv_tran_device_result_type_cd IN (
                    PKG_CONST.TRAN_DEV_RES__SUCCESS,
                    PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_PRNTR,
                    PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_RCPT,
                    PKG_CONST.TRAN_DEV_RES__SUCCESS_RCPT_ERR) THEN

                    SELECT NVL(MAX(auth_amt_approved), 0)
                    INTO ln_auth_amt_approved
                    FROM PSS.AUTH
                    WHERE TRAN_ID = pn_tran_id AND auth_type_cd = 'N' AND auth_state_id IN (2, 5);

                    pv_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH;
                    IF pn_sale_amount > 0 AND ln_auth_amt_approved > 0 AND pn_sale_amount / ln_minor_currency_factor > ln_auth_amt_approved THEN
                        SELECT GREATEST(ln_auth_amt_approved, NVL(NVL(MAX(DBADMIN.TO_NUMBER_OR_NULL(DS_MAA.DEVICE_SETTING_VALUE) / 100), MAX(DBADMIN.TO_NUMBER_OR_NULL(DS_AUTH_AMT.DEVICE_SETTING_VALUE) / DECODE(D.DEVICE_TYPE_ID, 13, 100, 1))), 0))
                        INTO ln_auth_amt_allowed
                        FROM DEVICE.DEVICE D
                        LEFT OUTER JOIN DEVICE.DEVICE_SETTING DS_MAA ON D.DEVICE_ID = DS_MAA.DEVICE_ID AND DS_MAA.DEVICE_SETTING_PARAMETER_CD = 'MAX_AUTH_AMOUNT'
                        LEFT OUTER JOIN DEVICE.DEVICE_SETTING DS_AUTH_AMT ON D.DEVICE_ID = DS_AUTH_AMT.DEVICE_ID
                            AND DS_AUTH_AMT.DEVICE_SETTING_PARAMETER_CD = DECODE(D.DEVICE_TYPE_ID, 13, '1200', 1, '195', 11, 'AUTHORIZATION_AMOUNT', 0, '195')
                        WHERE D.DEVICE_ID = ln_device_id;

                        ln_sale_over_auth_amt_percent := NVL(DBADMIN.TO_NUMBER_OR_NULL(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('ALLOWED_SALE_AMT_OVER_AUTH_AMT_PERCENT')), 100);
                        IF pn_sale_amount / ln_minor_currency_factor > ln_auth_amt_allowed + ln_auth_amt_allowed * ln_sale_over_auth_amt_percent / 100 THEN
                            pv_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE_ERROR;
                            lv_error := 'Error: Sale amount ' || TO_CHAR(pn_sale_amount / ln_minor_currency_factor, 'FM9,999,999,990.00') || ' exceeds allowed auth amount ' || TO_CHAR(ln_auth_amt_allowed, 'FM9,999,999,990.00') || ' by more than ' || ln_sale_over_auth_amt_percent || '%';
                            UPDATE PSS.TRAN
                            SET TRAN_INFO = SUBSTR(TRAN_INFO || DECODE(TRAN_INFO, NULL, '', ', ') || lv_error, 1, 1000)
                            WHERE TRAN_ID = pn_tran_id AND (TRAN_INFO IS NULL OR INSTR(TRAN_INFO, lv_error) = 0);

                            UPDATE DEVICE.DEVICE
                            SET DEVICE_ACTIVE_YN_FLAG = 'N'
                            WHERE DEVICE_NAME = pv_device_name AND DEVICE_ACTIVE_YN_FLAG = 'Y';

                            lv_email_from_address := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EMAIL_FROM_ADDRESS');
                            lv_email_to_address := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EMAIL_TO_ADDRESS_CUSTOMER_SERVICE');
                            INSERT INTO ENGINE.OB_EMAIL_QUEUE(OB_EMAIL_FROM_EMAIL_ADDR, OB_EMAIL_FROM_NAME, OB_EMAIL_TO_EMAIL_ADDR, OB_EMAIL_TO_NAME, OB_EMAIL_SUBJECT, OB_EMAIL_MSG)
                            VALUES(lv_email_from_address, lv_email_from_address, lv_email_to_address, lv_email_to_address, 'Invalid sale amount', lv_error || ', device: ' || pv_device_name || ', transaction ID: ' || pn_tran_id || '. Device has been disabled.');
                        END IF;
                    END IF;
                ELSE
                    pv_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH;
                END IF;
            ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__ACTUAL
                AND pv_tran_state_cd = PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND THEN
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__SALE_NO_AUTH;
            ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__ACTUAL
                AND pv_tran_state_cd = PKG_CONST.TRAN_STATE__PROCESSING_TRAN THEN
                NULL;-- don't change tran_state_cd
            ELSIF pv_tran_state_cd != PKG_CONST.TRAN_STATE__SALE_NO_AUTH THEN
                 pv_error_message := 'Unusual tran state for a cashless sale, tran_id: ' || pn_tran_id || ', tran_state_cd: ' || pv_tran_state_cd;
            END IF;
        END IF;

        UPDATE pss.tran
        SET tran_state_cd = DECODE(TRAN_STATE_CD, lv_orig_tran_state_cd, pv_tran_state_cd, TRAN_STATE_CD), -- it might have changed if POSMLayer is processing it
            tran_end_ts = tran_start_ts,
            tran_upload_ts = ld_current_ts,
            tran_device_result_type_cd = pv_tran_device_result_type_cd
        WHERE tran_id = pn_tran_id
        RETURNING client_payment_type_cd, payment_subtype_class INTO lc_client_payment_type_cd, lv_payment_subtype_class;

        SELECT /*+ INDEX(tli IF1_TRAN_LINE_ITEM) */ COUNT(1)
        INTO ln_count
        FROM pss.tran_line_item tli
        WHERE tran_id = pn_tran_id
            AND tran_line_item_batch_type_cd = pc_tran_batch_type_cd;

        IF ln_count > 0 THEN
            DELETE /*+ INDEX(tli IF1_TRAN_LINE_ITEM) */ FROM pss.tran_line_item tli
            WHERE tran_id = pn_tran_id
                AND tran_line_item_batch_type_cd = pc_tran_batch_type_cd;
        END IF;
    END IF;
	
	IF pn_sale_amount < 0 THEN
		SELECT D.DEVICE_TYPE_ID, COALESCE(DST.DEVICE_SUB_TYPE_DESC, '-')
		INTO ln_device_type_id, lv_device_sub_type_desc
		FROM DEVICE.DEVICE D
		LEFT OUTER JOIN DEVICE.DEVICE_SUB_TYPE DST ON D.DEVICE_TYPE_ID = DST.DEVICE_TYPE_ID AND D.DEVICE_SUB_TYPE_ID = DST.DEVICE_SUB_TYPE_ID
		WHERE D.DEVICE_ID = ln_device_id;
		
		IF ln_device_type_id != 14 OR lv_device_sub_type_desc NOT LIKE 'Prepaid Virtual - %' THEN
			-- Set transaction state to error if sale amount is negative except for Prepaid Virtual devices which create transactions with negative sale amounts for card balance increases (Administrator Adjustments)
			pv_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE_ERROR;
			lv_error := 'Error: Received negative sale amount ' || TO_CHAR(pn_sale_amount / ln_minor_currency_factor, 'FM9,999,999,990.00');
			UPDATE PSS.TRAN
			SET tran_state_cd = pv_tran_state_cd, TRAN_INFO = CASE WHEN TRAN_INFO IS NULL OR INSTR(TRAN_INFO, lv_error) = 0 THEN 
				SUBSTR(TRAN_INFO || CASE WHEN TRAN_INFO IS NULL THEN '' ELSE ', ' END || lv_error, 1, 1000)
				ELSE TRAN_INFO END
			WHERE TRAN_ID = pn_tran_id;
		END IF;
	END IF;

    UPDATE pss.sale
    SET device_batch_id = pn_device_batch_id,
        sale_type_cd = pc_sale_type_cd,
        sale_start_utc_ts = lt_sale_start_utc_ts,
        sale_end_utc_ts = lt_sale_start_utc_ts,
        sale_utc_offset_min = pn_sale_utc_offset_min,
        sale_result_id = pn_sale_result_id,
        sale_amount = pn_sale_amount / ln_minor_currency_factor,
        receipt_result_cd = pc_receipt_result_cd,
        hash_type_cd = pv_hash_type_cd,
        tran_line_item_hash = pv_tran_line_item_hash,
        sale_global_session_cd = pv_global_session_cd,
        imported = CASE WHEN pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED OR pv_tran_state_cd IN('F', 'G', 'Z') THEN '?' WHEN pn_sale_result_id = 0 THEN 'N' ELSE '-' END,
        VOID_ALLOWED = NVL(pc_void_allowed, 'N')
    WHERE tran_id = pn_tran_id;

    IF SQL%NOTFOUND THEN
        INSERT INTO pss.sale (
            tran_id,
            device_batch_id,
            sale_type_cd,
            sale_start_utc_ts,
            sale_end_utc_ts,
            sale_utc_offset_min,
            sale_result_id,
            sale_amount,
            receipt_result_cd,
            hash_type_cd,
            tran_line_item_hash,
            sale_global_session_cd,
            imported,
            VOID_ALLOWED
        ) VALUES (
            pn_tran_id,
            pn_device_batch_id,
            pc_sale_type_cd,
            lt_sale_start_utc_ts,
            lt_sale_start_utc_ts,
            pn_sale_utc_offset_min,
            pn_sale_result_id,
            pn_sale_amount / ln_minor_currency_factor,
            pc_receipt_result_cd,
            pv_hash_type_cd,
            pv_tran_line_item_hash,
            pv_global_session_cd,
            CASE WHEN pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED OR pv_tran_state_cd IN('F', 'G', 'Z') THEN '?' WHEN pn_sale_result_id = 0 THEN 'N' ELSE '-' END,
            NVL(pc_void_allowed, 'N')
        );

        IF lc_client_payment_type_cd IN (PKG_CONST.CLNT_PMNT_TYPE__ISIS_CREDIT, PKG_CONST.CLNT_PMNT_TYPE__ISIS_SPECIAL)
            AND ln_consumer_acct_id > 0 AND pc_sale_type_cd = PKG_CONST.SALE_TYPE__ACTUAL THEN
            PROCESS_ISIS_TRAN(pn_tran_id);
        END IF;

        select count(1) into l_promo_pass_through from
        pss.auth a join pss.pos_pta pp on a.pos_pta_id=pp.pos_pta_id
        join pss.payment_subtype ps on ps.payment_subtype_id=pp.payment_subtype_id
        where tran_id=pn_tran_id
        and ps.payment_subtype_class='Promotion';

        IF (l_promo_pass_through>0 or lv_payment_subtype_class ='Promotion')
            AND ln_consumer_acct_id > 0 AND pc_sale_type_cd = PKG_CONST.SALE_TYPE__ACTUAL THEN
            PROCESS_PROMO_TRAN(pn_tran_id);
        END IF;

    END IF;

    IF pn_sale_result_id != 0 AND ln_consumer_acct_id IS NOT NULL AND lc_auth_hold_used = 'N' THEN
        UPDATE pss.last_device_action
        SET device_action_utc_ts = device_action_utc_ts - INTERVAL '1' YEAR
        WHERE device_name = pv_device_name
            AND consumer_acct_id = ln_consumer_acct_id;
    END IF;
END;

    PROCEDURE ADD_REPLENISH_BONUSES(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pn_apply_to_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
        pn_amount PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
        pd_auth_ts PSS.AUTH.AUTH_TS%TYPE,
        pn_consumer_acct_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_TYPE_ID%TYPE DEFAULT NULL)
    IS
        ln_bonus_amount PSS.SALE.SALE_AMOUNT%TYPE;
        ln_bonus_percent REPORT.CAMPAIGN.DISCOUNT_PERCENT%TYPE;
        ln_bonus_threshhold REPORT.CAMPAIGN.THRESHOLD_AMOUNT%TYPE;
        ln_bonus_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE;
        ln_replenish_and_bonus_amount PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE:=pn_amount;
        lv_consumer_acct_identifier PSS.CONSUMER_ACCT.CONSUMER_ACCT_IDENTIFIER%TYPE;
        ln_tran_id PSS.TRAN.TRAN_ID%TYPE;
        l_sprout_refund_ts DATE;
        l_replenish_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE;
        l_replenish_device_name DEVICE.DEVICE_NAME%TYPE;
        l_replenish_next_master_id NUMBER;
        l_bonus_type NUMBER:=0;
    BEGIN
        SELECT MAX(DISCOUNT_PERCENT), MAX(THRESHOLD_AMOUNT), MAX(CAMPAIGN_ID)
          INTO ln_bonus_percent, ln_bonus_threshhold, ln_bonus_campaign_id
          FROM (SELECT C.DISCOUNT_PERCENT, C.THRESHOLD_AMOUNT, C.CAMPAIGN_ID
          FROM REPORT.CAMPAIGN C
          JOIN PSS.CAMPAIGN_CONSUMER_ACCT CCA on C.CAMPAIGN_ID = CCA.CAMPAIGN_ID
         WHERE CCA.CONSUMER_ACCT_ID = pn_apply_to_consumer_acct_id
           AND pd_auth_ts BETWEEN NVL(C.START_DATE, MIN_DATE) AND NVL(C.END_DATE, MAX_DATE)
           AND C.CAMPAIGN_TYPE_ID = 2 /* Replenish reward - Bonus */
           AND C.DISCOUNT_PERCENT > 0
           AND C.DISCOUNT_PERCENT < 1
           AND C.THRESHOLD_AMOUNT<=pn_amount
         ORDER BY C.DISCOUNT_PERCENT DESC, C.CAMPAIGN_ID DESC)
         WHERE ROWNUM = 1;
        IF ln_bonus_campaign_id IS NOT NULL AND ln_bonus_percent > 0 THEN
        	l_bonus_type:=1;
        ELSE
        	SELECT MAX(PURCHASE_DISCOUNT), MAX(THRESHOLD_AMOUNT), MAX(CAMPAIGN_ID)
          INTO ln_bonus_amount, ln_bonus_threshhold, ln_bonus_campaign_id
          FROM (SELECT C.PURCHASE_DISCOUNT, C.THRESHOLD_AMOUNT, C.CAMPAIGN_ID
          FROM REPORT.CAMPAIGN C
          JOIN PSS.CAMPAIGN_CONSUMER_ACCT CCA on C.CAMPAIGN_ID = CCA.CAMPAIGN_ID
         WHERE CCA.CONSUMER_ACCT_ID = pn_apply_to_consumer_acct_id
           AND pd_auth_ts BETWEEN NVL(C.START_DATE, MIN_DATE) AND NVL(C.END_DATE, MAX_DATE)
           AND C.CAMPAIGN_TYPE_ID = 6 /* Replenish reward - Flat Bonus */
           AND C.PURCHASE_DISCOUNT > 0
           AND THRESHOLD_AMOUNT=pn_amount
         ORDER BY C.PURCHASE_DISCOUNT DESC, C.CAMPAIGN_ID DESC)
         WHERE ROWNUM = 1;
         	IF ln_bonus_amount is NOT NULL AND ln_bonus_campaign_id is NOT NULL THEN
         		l_bonus_type:=2;
         	END IF;
        END IF;
        IF l_bonus_type > 0 THEN
            DECLARE
                lv_currency_cd PSS.CONSUMER_ACCT.CURRENCY_CD%TYPE;
                ln_corp_customer_id PSS.CONSUMER_ACCT.CORP_CUSTOMER_ID%TYPE;
                lv_consumer_acct_cd PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD%TYPE;
                lv_bonus_device_name DEVICE.DEVICE_NAME%TYPE;
                ln_bonus_next_master_id NUMBER;
                ln_result_cd NUMBER;
                lv_error_message VARCHAR2(4000);
                ln_bonus_tran_id PSS.TRAN.TRAN_ID%TYPE;
                lv_bonus_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
                ln_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE;
                lc_currency_symbol CORP.CURRENCY.CURRENCY_SYMBOL%TYPE;
                ln_host_id HOST.HOST_ID%TYPE;
                ld_bonus_ts DATE;
                ld_bonus_time NUMBER;
                lv_desc PSS.TRAN_LINE_ITEM.TRAN_LINE_ITEM_DESC%TYPE;
                ln_doc_id CORP.DOC.DOC_ID%TYPE;
                ln_ledger_id CORP.LEDGER.LEDGER_ID%TYPE;
                ln_consumer_acct_sub_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_SUB_TYPE_ID%TYPE;
            BEGIN
                IF l_bonus_type=1 THEN
                    ln_bonus_amount := ROUND(pn_amount * ln_bonus_percent, 2);
                END IF;
                ln_replenish_and_bonus_amount:=ln_replenish_and_bonus_amount+ln_bonus_amount;
          IF pn_consumer_acct_type_id = 3 THEN
                    UPDATE PSS.CONSUMER_ACCT
                       SET REPLENISH_BONUS_TOTAL = NVL(REPLENISH_BONUS_TOTAL, 0) + ln_bonus_amount,
                           CONSUMER_ACCT_BALANCE = CONSUMER_ACCT_BALANCE + ln_bonus_amount,
                           CONSUMER_ACCT_PROMO_BALANCE = CASE WHEN pn_consumer_acct_type_id is null or pn_consumer_acct_type_id=3 THEN CONSUMER_ACCT_PROMO_BALANCE + ln_bonus_amount ELSE CONSUMER_ACCT_PROMO_BALANCE END,
                           CONSUMER_ACCT_PROMO_TOTAL = CONSUMER_ACCT_PROMO_TOTAL + ln_bonus_amount
                     WHERE CONSUMER_ACCT_ID = pn_apply_to_consumer_acct_id
                     RETURNING CURRENCY_CD, CORP_CUSTOMER_ID, CONSUMER_ACCT_CD, CONSUMER_ACCT_IDENTIFIER, CONSUMER_ACCT_SUB_TYPE_ID
                     INTO lv_currency_cd, ln_corp_customer_id, lv_consumer_acct_cd, lv_consumer_acct_identifier, ln_consumer_acct_sub_type_id;
           ELSE
            select CURRENCY_CD, CORP_CUSTOMER_ID, CONSUMER_ACCT_CD, CONSUMER_ACCT_IDENTIFIER, CONSUMER_ACCT_SUB_TYPE_ID
            into lv_currency_cd, ln_corp_customer_id, lv_consumer_acct_cd, lv_consumer_acct_identifier, ln_consumer_acct_sub_type_id
            from PSS.CONSUMER_ACCT where CONSUMER_ACCT_ID = pn_apply_to_consumer_acct_id;
           END IF;
                    -- add trans to virtual terminal
                    SELECT C.MINOR_CURRENCY_FACTOR, NVL(CC.CURRENCY_SYMBOL, '$'), DBADMIN.TIMESTAMP_TO_MILLIS(SYS_EXTRACT_UTC(SYSTIMESTAMP)), SYSDATE
                      INTO ln_minor_currency_factor, lc_currency_symbol, ld_bonus_time, ld_bonus_ts
                      FROM PSS.CURRENCY C
                      LEFT OUTER JOIN CORP.CURRENCY CC ON C.CURRENCY_CD = CC.CURRENCY_CODE
                     WHERE C.CURRENCY_CD = lv_currency_cd;

                    PKG_DEVICE_CONFIGURATION.NEXT_MASTER_ID_BY_SERIAL('V1-' || CASE WHEN ln_consumer_acct_sub_type_id = 2 THEN ln_corp_customer_id ELSE 1 END || '-' || lv_currency_cd, lv_bonus_device_name, ln_bonus_next_master_id);
                    SP_CREATE_SALE('A', lv_bonus_device_name, ln_bonus_next_master_id,  0, 'C', ld_bonus_time,
                        DBADMIN.PKG_UTL.TIME_ZONE_DIFF_MIN(PKG_CONST.DB_TIME_ZONE, PKG_CONST.GMT_TIME_ZONE), 'S', 0, ln_bonus_amount * ln_minor_currency_factor, 'U', 'A', 'SHA1',
                        DBADMIN.HASH_CARD('Replenish Bonus on ' || pn_apply_to_consumer_acct_id || ' of ' || ln_bonus_amount),
                        NULL, ln_result_cd, lv_error_message, ln_bonus_tran_id, lv_bonus_tran_state_cd);
                    IF ln_result_cd != PKG_CONST.RESULT__SUCCESS THEN
                        RAISE_APPLICATION_ERROR(-20118, 'Could not create replenish bonus transaction: ' || lv_error_message);
                    END IF;
                    SELECT HOST_ID, 'Replenish Bonus for ' || lc_currency_symbol || TO_CHAR(pn_amount, 'FM9,999,999.00') || ' in replenishments'
                      INTO ln_host_id, lv_desc
                      FROM (SELECT H.HOST_ID
                              FROM DEVICE.HOST H
                              JOIN DEVICE.DEVICE D ON H.DEVICE_ID = D.DEVICE_ID
                             WHERE D.DEVICE_ACTIVE_YN_FLAG = 'Y'
                               AND H.HOST_PORT_NUM IN(0,1)
                               AND D.DEVICE_NAME = lv_bonus_device_name
                               AND H.HOST_ACTIVE_YN_FLAG = 'Y'
                             ORDER BY H.HOST_PORT_NUM DESC, H.HOST_ID DESC)
                     WHERE ROWNUM = 1;
                    INSERT INTO PSS.TRAN_LINE_ITEM(
                        TRAN_ID,
                        TRAN_LINE_ITEM_AMOUNT,
                        TRAN_LINE_ITEM_POSITION_CD,
                        TRAN_LINE_ITEM_TAX,
                        TRAN_LINE_ITEM_TYPE_ID,
                        TRAN_LINE_ITEM_QUANTITY,
                        TRAN_LINE_ITEM_DESC,
                        HOST_ID,
                        TRAN_LINE_ITEM_BATCH_TYPE_CD,
                        TRAN_LINE_ITEM_TS,
                        SALE_RESULT_ID,
                        APPLY_TO_CONSUMER_ACCT_ID,
                        CAMPAIGN_ID)
                    SELECT
                        ln_bonus_tran_id,
                        case when l_bonus_type=1 then pn_amount else ln_bonus_amount END,
                        NULL,
                        NULL,
                        555,
                        case when l_bonus_type=1 then ln_bonus_percent else 1 END,
                        lv_desc,
                        ln_host_id,
                        'A',
                        ld_bonus_ts,
                        0,
                        pn_apply_to_consumer_acct_id,
                        ln_bonus_campaign_id
                    FROM DUAL;
                    UPDATE PSS.TRAN
                       SET PARENT_TRAN_ID = pn_tran_id
                     WHERE TRAN_ID = ln_bonus_tran_id;
                    IF ln_consumer_acct_sub_type_id = 1 AND pn_consumer_acct_type_id = 3 THEN
                        -- add adjustment to ledger
                        CORP.PAYMENTS_PKG.ADJUSTMENT_INS(ln_corp_customer_id, 'Replenish Bonus Processing', lv_currency_cd, lv_desc || ', card # ' || lv_consumer_acct_cd || ', card ID ' || lv_consumer_acct_identifier,
                            -ln_bonus_amount, ln_doc_id, ln_ledger_id);
                    END IF;
            END;
        END IF;


        IF pn_consumer_acct_type_id = 6  and ln_replenish_and_bonus_amount >  0 THEN -- sprout replenish
          select PSS.SEQ_TRAN_ID.NEXTVAL into ln_tran_id from dual;
          select sysdate into l_sprout_refund_ts from dual;
          select 'V1-'||CORP_CUSTOMER_ID||'-USD' into l_replenish_device_serial_cd
          from pss.consumer_acct where consumer_acct_id=pn_apply_to_consumer_acct_id;
          PKG_DEVICE_CONFIGURATION.NEXT_MASTER_ID_BY_SERIAL(l_replenish_device_serial_cd, l_replenish_device_name, l_replenish_next_master_id);
          IF lv_consumer_acct_identifier is null THEN
            select consumer_acct_identifier into lv_consumer_acct_identifier from pss.consumer_acct where consumer_acct_id=pn_apply_to_consumer_acct_id;
          END IF;
          INSERT INTO PSS.TRAN (
            TRAN_ID,
            PARENT_TRAN_ID,
            TRAN_START_TS,
            TRAN_END_TS,
            TRAN_UPLOAD_TS,
            TRAN_GLOBAL_TRANS_CD,
            TRAN_STATE_CD,
            CONSUMER_ACCT_ID,
            TRAN_DEVICE_TRAN_CD,
            POS_PTA_ID,
            TRAN_DEVICE_RESULT_TYPE_CD,
            TRAN_RECEIVED_RAW_ACCT_DATA,
            PAYMENT_SUBTYPE_KEY_ID,
            PAYMENT_SUBTYPE_CLASS,
            CLIENT_PAYMENT_TYPE_CD,
            DEVICE_NAME
            )
           SELECT ln_tran_id,
                  pn_tran_id,
                  l_sprout_refund_ts,
                  l_sprout_refund_ts,
                  NULL, /* Must be NULL so that PSSUpdater will not pick it up */
                  'RF:'||l_replenish_device_name||':'||l_replenish_next_master_id,
                  '8',
                  pn_apply_to_consumer_acct_id,
                  l_replenish_next_master_id,
                  pp.POS_PTA_ID, -- ? use V1-1-USD entry Sprout Prepaid Card Manual Entry
                  null,
                  null,
                  pp.PAYMENT_SUBTYPE_KEY_ID,
                  ps.PAYMENT_SUBTYPE_CLASS,
                  ps.CLIENT_PAYMENT_TYPE_CD,
                  d.DEVICE_NAME
          FROM pss.pos_pta pp
          JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id and ps.payment_subtype_class='Sprout' and client_payment_type_cd='T'
          JOIN pss.pos p ON pp.pos_id = p.pos_id
          JOIN device.device d ON p.device_id = d.device_id
          where d.device_serial_cd=l_replenish_device_serial_cd;

          INSERT INTO PSS.TRAN_LINE_ITEM(
                        TRAN_ID,
                        TRAN_LINE_ITEM_AMOUNT,
                        TRAN_LINE_ITEM_POSITION_CD,
                        TRAN_LINE_ITEM_TAX,
                        TRAN_LINE_ITEM_TYPE_ID,
                        TRAN_LINE_ITEM_QUANTITY,
                        TRAN_LINE_ITEM_DESC,
                        HOST_ID,
                        TRAN_LINE_ITEM_BATCH_TYPE_CD,
                        TRAN_LINE_ITEM_TS,
                        SALE_RESULT_ID,
                        APPLY_TO_CONSUMER_ACCT_ID,
                        CAMPAIGN_ID)
                    SELECT
                        ln_tran_id,
                        pn_amount,
                        NULL,
                        NULL,
                        556,
                        1,
                         'Sprout replenishment for consumer_acct_id ' || pn_apply_to_consumer_acct_id,
                        NULL,
                        'A',
                        l_sprout_refund_ts,
                        0,
                        pn_apply_to_consumer_acct_id,
                        ln_bonus_campaign_id
                    FROM DUAL;
          IF NVL(ln_bonus_amount,0) > 0 THEN
          INSERT INTO PSS.TRAN_LINE_ITEM(
                        TRAN_ID,
                        TRAN_LINE_ITEM_AMOUNT,
                        TRAN_LINE_ITEM_POSITION_CD,
                        TRAN_LINE_ITEM_TAX,
                        TRAN_LINE_ITEM_TYPE_ID,
                        TRAN_LINE_ITEM_QUANTITY,
                        TRAN_LINE_ITEM_DESC,
                        HOST_ID,
                        TRAN_LINE_ITEM_BATCH_TYPE_CD,
                        TRAN_LINE_ITEM_TS,
                        SALE_RESULT_ID,
                        APPLY_TO_CONSUMER_ACCT_ID,
                        CAMPAIGN_ID)
                    SELECT
                        ln_tran_id,
                        ln_bonus_amount,
                        NULL,
                        NULL,
                        560,
                        1,
                         'Sprout replenishment Bonus for consumer_acct_id ' || pn_apply_to_consumer_acct_id,
                        NULL,
                        'A',
                        l_sprout_refund_ts,
                        0,
                        pn_apply_to_consumer_acct_id,
                        ln_bonus_campaign_id
                    FROM DUAL;
          END IF;
          INSERT INTO PSS.REFUND (
            TRAN_ID,
            REFUND_AMT,
            REFUND_DESC,
            REFUND_ISSUE_TS,
            REFUND_ISSUE_BY,
            REFUND_TYPE_CD,
            REFUND_STATE_ID,
            ACCT_ENTRY_METHOD_CD,
            OVERRIDE_TRANS_TYPE_ID,
            REFUND_PARSED_ACCT_DATA
        ) VALUES (
            ln_tran_id,  -- seq
            -ABS(ln_replenish_and_bonus_amount),
            'Replenish for sprout consumer_acct_id '||pn_apply_to_consumer_acct_id,
            l_sprout_refund_ts,
            'Sprout Replenish',
            'S',
            6,
            '2',
            31,
            lv_consumer_acct_identifier);

        END IF;

    END;

    PROCEDURE FINISH_REPLENISH_SETUP(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pn_apply_to_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
        pv_card_key PSS.AUTH.CARD_KEY%TYPE,
        pd_auth_ts PSS.AUTH.AUTH_TS%TYPE,
        pv_masked_card_number PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_CARD_MASKED%TYPE,
        pn_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE)
    IS
        ln_replenish_id PSS.CONSUMER_ACCT_REPLENISH.CONSUMER_ACCT_REPLENISH_ID%TYPE;
        ln_capr_row_id ROWID;
        ln_replen_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE;
    BEGIN
        SELECT MAX(CAR.CONSUMER_ACCT_REPLENISH_ID), MAX(CAPR.ROWID)
          INTO ln_replenish_id, ln_capr_row_id
          FROM PSS.CONSUMER_ACCT_REPLENISH CAR
          JOIN PSS.CONSUMER_ACCT_PEND_REPLENISH CAPR ON CAR.CONSUMER_ACCT_REPLENISH_ID = CAPR.CONSUMER_ACCT_REPLENISH_ID
          JOIN PSS.TRAN X ON CAPR.DEVICE_NAME = X.DEVICE_NAME AND CAPR.DEVICE_TRAN_CD = X.TRAN_DEVICE_TRAN_CD
         WHERE X.TRAN_ID = pn_tran_id
           AND CAR.CONSUMER_ACCT_ID = pn_apply_to_consumer_acct_id;
        IF ln_replenish_id IS NOT NULL THEN
            SELECT CONSUMER_ACCT_ID INTO ln_replen_consumer_acct_id FROM PSS.TRAN WHERE TRAN_ID=pn_tran_id;
            UPDATE PSS.CONSUMER_ACCT_REPLENISH CAR
               SET REPLENISH_CARD_KEY = CASE WHEN pv_card_key IS NOT NULL AND pd_auth_ts > NVL(LAST_REPLENISH_TRAN_TS, MIN_DATE) THEN pv_card_key ELSE REPLENISH_CARD_KEY END,
                   REPLENISH_CARD_MASKED = CASE WHEN pv_card_key IS NOT NULL AND pd_auth_ts > NVL(LAST_REPLENISH_TRAN_TS, MIN_DATE) THEN DBADMIN.MASK_CREDIT_CARD(pv_masked_card_number) ELSE REPLENISH_CARD_MASKED END,
                   REPLENISH_POS_PTA_ID = CASE WHEN pv_card_key IS NOT NULL AND pd_auth_ts > NVL(LAST_REPLENISH_TRAN_TS, MIN_DATE) THEN pn_pos_pta_id ELSE REPLENISH_POS_PTA_ID END,
                   LAST_REPLENISH_TRAN_TS = CASE WHEN pv_card_key IS NOT NULL AND pd_auth_ts > NVL(LAST_REPLENISH_TRAN_TS, MIN_DATE) THEN pd_auth_ts ELSE LAST_REPLENISH_TRAN_TS END,
                   LAST_REPLENISH_TRAN_ID = CASE WHEN pv_card_key IS NOT NULL AND pd_auth_ts > NVL(LAST_REPLENISH_TRAN_TS, MIN_DATE) THEN pn_tran_id ELSE LAST_REPLENISH_TRAN_ID END,
                   REPLEN_CONSUMER_ACCT_ID = CASE WHEN pv_card_key IS NOT NULL AND pd_auth_ts > NVL(LAST_REPLENISH_TRAN_TS, MIN_DATE) THEN ln_replen_consumer_acct_id ELSE REPLEN_CONSUMER_ACCT_ID END
              WHERE (CONSUMER_ACCT_ID = pn_apply_to_consumer_acct_id AND CONSUMER_ACCT_REPLENISH_ID = ln_replenish_id)
              OR (CONSUMER_ACCT_ID in
              (SELECT CONSUMER_ACCT_ID FROM PSS.CONSUMER_ACCT
              where CONSUMER_ACCT_TYPE_ID=3 and consumer_id=(select consumer_id from pss.consumer_acct where consumer_acct_id=pn_apply_to_consumer_acct_id))
             AND STATUS='A'
             AND REPLEN_CONSUMER_ACCT_ID = ln_replen_consumer_acct_id
             AND REPLENISH_CARD_KEY IS NULL);
            DELETE
              FROM PSS.CONSUMER_ACCT_PEND_REPLENISH
             WHERE ROWID = ln_capr_row_id
               AND CONSUMER_ACCT_REPLENISH_ID = ln_replenish_id
               AND NOT EXISTS(SELECT 1 FROM PSS.CONSUMER_ACCT where consumer_acct_id=pn_apply_to_consumer_acct_id and CONSUMER_ACCT_TYPE_ID=6);
        END IF;
    END;

    PROCEDURE REPLENISH_CONSUMER_ACCT(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pv_tli_desc PSS.TRAN_LINE_ITEM.TRAN_LINE_ITEM_DESC%TYPE,
        pn_amount PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
        pn_tli_type_id PSS.TRAN_LINE_ITEM.TRAN_LINE_ITEM_TYPE_ID%TYPE,
        pv_card_key PSS.AUTH.CARD_KEY%TYPE,
        pd_auth_ts PSS.AUTH.AUTH_TS%TYPE,
        pv_masked_card_number PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_CARD_MASKED%TYPE,
        pn_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE,
        pn_apply_to_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE)
    IS
        ln_consumer_acct_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_TYPE_ID%TYPE;
    BEGIN
        UPDATE PSS.CONSUMER_ACCT
           SET CONSUMER_ACCT_BALANCE = CONSUMER_ACCT_BALANCE + pn_amount,
               CONSUMER_ACCT_REPLEN_BALANCE = CONSUMER_ACCT_REPLEN_BALANCE + pn_amount,
               CONSUMER_ACCT_REPLENISH_TOTAL = CONSUMER_ACCT_REPLENISH_TOTAL + pn_amount
         WHERE CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y'
           AND CONSUMER_ACCT_TYPE_ID = 3
           AND CONSUMER_ACCT_CD_HASH = HEXTORAW(pv_tli_desc)
          RETURNING CONSUMER_ACCT_ID, CONSUMER_ACCT_TYPE_ID
          INTO pn_apply_to_consumer_acct_id, ln_consumer_acct_type_id;
        IF ln_consumer_acct_type_id IN(3) THEN
            ADD_REPLENISH_BONUSES(
                pn_tran_id,
                pn_apply_to_consumer_acct_id,
                pn_amount,
                pd_auth_ts,
                ln_consumer_acct_type_id);
        END IF;
        FINISH_REPLENISH_SETUP(
            pn_tran_id,
            pn_apply_to_consumer_acct_id,
            pv_card_key,
            pd_auth_ts,
            pv_masked_card_number,
            pn_pos_pta_id);
    END;

PROCEDURE sp_create_tran_line_item
(
    pn_tran_id IN pss.tran_line_item.tran_id%TYPE,
    pn_host_port_num IN host.host_port_num%TYPE,
    pn_tli_type_id IN pss.tran_line_item.tran_line_item_type_id%TYPE,
    pn_tli_quantity IN pss.tran_line_item.tran_line_item_quantity%TYPE,
    pn_tli_amount IN NUMBER,
    pn_tli_tax IN NUMBER,
    pv_tli_desc IN pss.tran_line_item.tran_line_item_desc%TYPE,
    pn_tli_utc_ts_ms IN NUMBER,
    pn_tli_utc_offset_min IN NUMBER,
    pv_tli_position_cd IN pss.tran_line_item.tran_line_item_position_cd%TYPE,
    pc_tran_batch_type_cd IN pss.tran_line_item.tran_line_item_batch_type_cd%TYPE,
    pn_sale_result_id pss.tran_line_item.sale_result_id%TYPE DEFAULT NULL,
    pn_host_position_num host.host_position_num%TYPE DEFAULT 0,
    pn_sale_amount pss.sale.sale_amount%TYPE DEFAULT 0
)
IS
    ln_host_id pss.tran_line_item.host_id%TYPE;
    ln_device_id host.device_id%TYPE;
    ln_new_host_count NUMBER;
    ln_result_cd NUMBER;
    lv_error_message VARCHAR2(255);
    ln_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE;
    ln_tran_line_item_id pss.tran_line_item.tran_line_item_id%TYPE;
    ln_tli_desc pss.tran_line_item.tran_line_item_desc%TYPE;
    ln_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE;
    lc_tli_type_group_cd PSS.TRAN_LINE_ITEM_TYPE.TRAN_LINE_ITEM_TYPE_GROUP_CD%TYPE;
    lv_device_name DEVICE.DEVICE_NAME%TYPE;
    ln_convenience_fee_amount PSS.SALE.SALE_AMOUNT%TYPE;
    lv_email_from_address engine.app_setting.app_setting_value%TYPE;
    lv_email_to_address engine.app_setting.app_setting_value%TYPE;
    lv_error pss.tran.tran_info%TYPE;
    ln_tli_type_id PSS.TRAN_LINE_ITEM.TRAN_LINE_ITEM_TYPE_ID%TYPE := pn_tli_type_id;
    ln_apply_to_consumer_acct_id PSS.TRAN_LINE_ITEM.APPLY_TO_CONSUMER_ACCT_ID%TYPE;
    lv_card_key PSS.AUTH.CARD_KEY%TYPE;
    ld_auth_ts PSS.AUTH.AUTH_TS%TYPE;
    lv_masked_card_number PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_CARD_MASKED%TYPE;
    ln_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE;
BEGIN
    SELECT POS.DEVICE_ID, c.MINOR_CURRENCY_FACTOR, D.DEVICE_TYPE_ID, D.DEVICE_NAME
      INTO ln_device_id, ln_minor_currency_factor, ln_device_type_id, lv_device_name
      FROM PSS.POS POS
      JOIN DEVICE.DEVICE D ON POS.DEVICE_ID = D.DEVICE_ID
      JOIN PSS.POS_PTA PTA ON POS.POS_ID = PTA.POS_ID
      JOIN PSS.TRAN X ON PTA.POS_PTA_ID = X.POS_PTA_ID
      JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
    WHERE X.TRAN_ID = pn_tran_id;

    ln_host_id := sf_find_host_id(ln_device_id, pn_host_port_num, pn_host_position_num);
    IF ln_host_id IS NULL THEN
        -- create default hosts
        pkg_device_configuration.sp_create_default_hosts(ln_device_id, ln_new_host_count, ln_result_cd, lv_error_message);
        IF ln_result_cd != PKG_CONST.RESULT__SUCCESS THEN
            RETURN;
        END IF;
        ln_host_id := sf_find_host_id(ln_device_id, pn_host_port_num, pn_host_position_num);
    END IF;

    SELECT PSS.SEQ_TLI_ID.NEXTVAL
        INTO ln_tran_line_item_id
        FROM DUAL;

    -- For Kiosk type, use tran_line_item_type to find description
    IF ln_device_type_id = 11 THEN
        SELECT tran_line_item_type_desc || ' ' || SUBSTR(TRIM(pv_tli_desc), 1, 3999 - LENGTH(tran_line_item_type_desc))
          INTO ln_tli_desc
          FROM pss.tran_line_item_type
         WHERE tran_line_item_type_id = ln_tli_type_id;
    ELSIF ln_device_type_id = 5 THEN -- eSuds
        SELECT TLIT.TRAN_LINE_ITEM_TYPE_DESC || ', ' || CASE WHEN DTHT.DEVICE_TYPE_HOST_TYPE_CD IN('S', 'U', 'G', 'H', 'I', 'J') THEN DECODE(H.HOST_POSITION_NUM, 0, 'Bottom ', 1, 'Top ') END
                || GT.HOST_GROUP_TYPE_NAME || ' ' || H.HOST_LABEL_CD
          INTO ln_tli_desc
          FROM PSS.TRAN_LINE_ITEM_TYPE tlit
         CROSS JOIN DEVICE.HOST H
          JOIN DEVICE.DEVICE_TYPE_HOST_TYPE dtht ON DTHT.HOST_TYPE_ID = H.HOST_TYPE_ID AND DTHT.DEVICE_TYPE_ID = 5
          LEFT OUTER JOIN (DEVICE.HOST_TYPE_HOST_GROUP_TYPE HTGT
          JOIN DEVICE.HOST_GROUP_TYPE GT ON HTGT.HOST_GROUP_TYPE_ID = GT.HOST_GROUP_TYPE_ID)
            ON H.HOST_TYPE_ID = HTGT.HOST_TYPE_ID
         WHERE tlit.TRAN_LINE_ITEM_TYPE_ID = ln_tli_type_id
           AND H.HOST_ID = ln_host_id;
    ELSE
        ln_tli_desc := pv_tli_desc;
    END IF;

    INSERT INTO PSS.TRAN_LINE_ITEM (
        TRAN_LINE_ITEM_ID,
        TRAN_ID,
        TRAN_LINE_ITEM_AMOUNT,
        TRAN_LINE_ITEM_POSITION_CD,
        TRAN_LINE_ITEM_TAX,
        TRAN_LINE_ITEM_TYPE_ID,
        TRAN_LINE_ITEM_QUANTITY,
        TRAN_LINE_ITEM_DESC,
        HOST_ID,
        TRAN_LINE_ITEM_BATCH_TYPE_CD,
        TRAN_LINE_ITEM_TS,
        SALE_RESULT_ID,
        APPLY_TO_CONSUMER_ACCT_ID)
    SELECT
        ln_tran_line_item_id,
        pn_tran_id,
        pn_tli_amount * CASE tran_line_item_type_sign_pn
            WHEN 'N' THEN -1
            ELSE 1
        END / ln_minor_currency_factor,
        pv_tli_position_cd,
        pn_tli_tax * CASE tran_line_item_type_sign_pn
            WHEN 'N' THEN -1
            ELSE 1
        END / ln_minor_currency_factor,
        ln_tli_type_id,
        pn_tli_quantity,
        ln_tli_desc,
        ln_host_id,
        pc_tran_batch_type_cd,
        CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_tli_utc_ts_ms + pn_tli_utc_offset_min * 60 * 1000) AS DATE),
        pn_sale_result_id,
        ln_apply_to_consumer_acct_id
    FROM tran_line_item_type
    WHERE tran_line_item_type_id = ln_tli_type_id;

    -- For all device actual batch type only
    IF ln_host_id IS NOT NULL AND pc_tran_batch_type_cd = PKG_CONST.TRAN_BATCH_TYPE__ACTUAL THEN
        SELECT tran_line_item_type_group_cd
        INTO lc_tli_type_group_cd
        FROM pss.tran_line_item_type
        WHERE tran_line_item_type_id = ln_tli_type_id;

        IF lc_tli_type_group_cd IN ('P', 'S') THEN
            UPDATE PSS.TRAN_LINE_ITEM_RECENT
            SET tran_line_item_id = ln_tran_line_item_id,
                fkp_tran_id = pn_tran_id
            WHERE host_id = ln_host_id
                AND tran_line_item_type_id = ln_tli_type_id;

            IF SQL%NOTFOUND THEN
                BEGIN
                    INSERT INTO PSS.TRAN_LINE_ITEM_RECENT (
                        TRAN_LINE_ITEM_ID,
                        HOST_ID,
                        FKP_TRAN_ID,
                        TRAN_LINE_ITEM_TYPE_ID
                    ) VALUES (
                        ln_tran_line_item_id,
                        ln_host_id,
                        pn_tran_id,
                        ln_tli_type_id
                    );
                EXCEPTION
                    WHEN DUP_VAL_ON_INDEX THEN
                        UPDATE PSS.TRAN_LINE_ITEM_RECENT
                        SET tran_line_item_id = ln_tran_line_item_id,
                            fkp_tran_id = pn_tran_id
                        WHERE host_id = ln_host_id
                            AND tran_line_item_type_id = ln_tli_type_id;
                END;
            END IF;
        END IF;
    END IF;

    IF ln_tli_type_id = PKG_CONST.TLI__CONVENIENCE_FEE AND pc_tran_batch_type_cd = PKG_CONST.TRAN_BATCH_TYPE__ACTUAL AND pn_sale_amount > 0 THEN
        ln_convenience_fee_amount := (NVL(pn_tli_amount, 0) + NVL(pn_tli_tax, 0)) * NVL(pn_tli_quantity, 0);
        IF ln_convenience_fee_amount > pn_sale_amount - ln_convenience_fee_amount
            AND ln_convenience_fee_amount > NVL(DBADMIN.TO_NUMBER_OR_NULL(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('MIN_INVALID_CONVENIENCE_FEE_AMT_PENNIES')), 100) THEN
            UPDATE PSS.TRAN
            SET tran_state_cd = PKG_CONST.TRAN_STATE__COMPLETE_ERROR
            WHERE TRAN_ID = pn_tran_id;

            lv_error := 'Error: Two-Tier Pricing amount ' || TO_CHAR(ln_convenience_fee_amount / ln_minor_currency_factor, 'FM9,999,999,990.00') || ' exceeds sale amount without Two-Tier Pricing ' || TO_CHAR((pn_sale_amount - ln_convenience_fee_amount) / ln_minor_currency_factor, 'FM9,999,999,990.00');
            UPDATE PSS.TRAN
            SET TRAN_INFO = SUBSTR(TRAN_INFO || DECODE(TRAN_INFO, NULL, '', ', ') || lv_error, 1, 1000)
            WHERE TRAN_ID = pn_tran_id AND (TRAN_INFO IS NULL OR INSTR(TRAN_INFO, lv_error) = 0);

            UPDATE DEVICE.DEVICE
            SET DEVICE_ACTIVE_YN_FLAG = 'N'
            WHERE DEVICE_NAME = lv_device_name AND DEVICE_ACTIVE_YN_FLAG = 'Y';

            lv_email_from_address := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EMAIL_FROM_ADDRESS');
            lv_email_to_address := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EMAIL_TO_ADDRESS_CUSTOMER_SERVICE');
            INSERT INTO ENGINE.OB_EMAIL_QUEUE(OB_EMAIL_FROM_EMAIL_ADDR, OB_EMAIL_FROM_NAME, OB_EMAIL_TO_EMAIL_ADDR, OB_EMAIL_TO_NAME, OB_EMAIL_SUBJECT, OB_EMAIL_MSG)
            VALUES(lv_email_from_address, lv_email_from_address, lv_email_to_address, lv_email_to_address, 'Invalid Two-Tier Pricing amount', lv_error || ', device: ' || lv_device_name || ', transaction ID: ' || pn_tran_id || '. Device has been disabled.');
        END IF;
    END IF;
END;

-- R33+ signature
PROCEDURE sp_finalize_sale
(
    pn_tran_id IN pss.tran.tran_id%TYPE,
    pv_global_session_cd IN VARCHAR2,
    pv_tran_device_result_type_cd IN pss.tran.tran_device_result_type_cd%TYPE,
    pn_sale_result_id IN pss.sale.sale_result_id%TYPE,
    pn_sale_amount IN NUMBER,
    pn_sale_tax IN NUMBER,
    pc_tran_batch_type_cd IN pss.tran_line_item.tran_line_item_batch_type_cd%TYPE,
    pn_sale_utc_ts_ms IN NUMBER,
    pn_sale_duration_sec IN NUMBER,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pc_sale_type_cd pss.sale.sale_type_cd%TYPE DEFAULT NULL,
    pc_tran_import_needed OUT VARCHAR2,
    pc_session_update_needed OUT VARCHAR2,
    pc_client_payment_type_cd OUT VARCHAR2,
    pn_minor_currency_factor OUT PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
    pn_tli_count OUT NUMBER
)
IS
/*
    Returned result codes:
        RESULT__SUCCESS
        RESULT__FAILURE
        RESULT__HOST_NOT_FOUND
*/
    ln_tli_total pss.tran_line_item.tran_line_item_amount%TYPE;
    ln_adj_amt pss.tran_line_item.tran_line_item_amount%TYPE;
    ln_adj_tli pss.tran_line_item.tran_line_item_type_id%TYPE := -1;
    ln_base_host_id pss.tran_line_item.host_id%TYPE;
    lc_tli_batch_type_cd pss.tran_line_item.tran_line_item_batch_type_cd%TYPE;
    ln_new_host_count NUMBER;
    ln_device_id DEVICE.DEVICE_ID%TYPE;
    ln_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE;
    lc_tran_state_cd pss.tran.tran_state_cd%TYPE;
    ln_discount_percent report.campaign.discount_percent%TYPE;
    ln_discount_amount NUMBER;
    ln_sale_amount NUMBER := NVL(pn_sale_amount, 0);
    ln_original_sale_amount NUMBER := ln_sale_amount;
    ln_pos_pta_id pss.tran.pos_pta_id%TYPE;
    ld_tran_start_ts pss.tran.tran_start_ts%TYPE;
    lv_device_tran_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE;
    lv_device_name DEVICE.DEVICE_NAME%TYPE;
    lv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE;
    ln_consumer_acct_id PSS.TRAN.CONSUMER_ACCT_ID%TYPE;
    ln_campaign_id PSS.TRAN_LINE_ITEM.CAMPAIGN_ID%TYPE;
    lc_backoffice_virtual_flag CHAR(1);
    lv_campaign_promo_code REPORT.CAMPAIGN.PROMO_CODE%TYPE;
    ln_tran_line_item_type_id PSS.TRAN_LINE_ITEM_TYPE.TRAN_LINE_ITEM_TYPE_ID%TYPE;
    ln_partial_auth_amount PSS.AUTH.AUTH_AMT_APPROVED%TYPE;
    lc_payment_subtype_class pss.payment_subtype.payment_subtype_class%TYPE;

BEGIN
    pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;
    pc_session_update_needed := 'N';

    IF pn_sale_duration_sec > 0 AND pc_tran_batch_type_cd = PKG_CONST.TRAN_BATCH_TYPE__ACTUAL THEN
        UPDATE pss.tran
        SET tran_end_ts = tran_start_ts + pn_sale_duration_sec / 86400
        WHERE tran_id = pn_tran_id;

        UPDATE pss.sale
        SET sale_end_utc_ts = sale_start_utc_ts + pn_sale_duration_sec / 86400
        WHERE tran_id = pn_tran_id;
    END IF;

    SELECT POS.DEVICE_ID, X.POS_PTA_ID, c.MINOR_CURRENCY_FACTOR, PST.CLIENT_PAYMENT_TYPE_CD, D.DEVICE_TYPE_ID, DECODE(S.IMPORTED, 'N', 'Y', NULL, 'Y', 'N'), X.TRAN_STATE_CD,
           X.TRAN_START_TS, X.TRAN_DEVICE_TRAN_CD, D.DEVICE_NAME, X.CONSUMER_ACCT_ID, CASE WHEN D.DEVICE_TYPE_ID = 14 AND D.DEVICE_SUB_TYPE_ID IN(3, 4) THEN 'Y' ELSE 'N' END, D.DEVICE_SERIAL_CD,
           AA.AUTH_AMT_APPROVED * c.MINOR_CURRENCY_FACTOR,pst.payment_subtype_class
      INTO ln_device_id, ln_pos_pta_id, pn_minor_currency_factor, pc_client_payment_type_cd, ln_device_type_id, pc_tran_import_needed, lc_tran_state_cd,
           ld_tran_start_ts, lv_device_tran_cd, lv_device_name, ln_consumer_acct_id, lc_backoffice_virtual_flag, lv_device_serial_cd, ln_partial_auth_amount,lc_payment_subtype_class
      FROM PSS.POS POS
      JOIN DEVICE.DEVICE D ON POS.DEVICE_ID = D.DEVICE_ID
      JOIN PSS.POS_PTA PTA ON POS.POS_ID = PTA.POS_ID
      JOIN PSS.TRAN X ON PTA.POS_PTA_ID = X.POS_PTA_ID
      JOIN PSS.SALE S ON X.TRAN_ID = S.TRAN_ID
      JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
      JOIN PSS.PAYMENT_SUBTYPE PST ON PTA.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
      JOIN LOCATION.LOCATION L ON POS.LOCATION_ID = L.LOCATION_ID
      JOIN LOCATION.TIME_ZONE TZ ON L.LOCATION_TIME_ZONE_CD = TZ.TIME_ZONE_CD
      LEFT OUTER JOIN PSS.AUTH AA ON X.TRAN_ID = AA.TRAN_ID AND AA.AUTH_TYPE_CD = 'N' AND AA.AUTH_STATE_ID = 5
    WHERE X.TRAN_ID = pn_tran_id;

    -- use the base host for adjustments
    SELECT MAX(H.HOST_ID)
    INTO ln_base_host_id
    FROM DEVICE.HOST H
    WHERE H.DEVICE_ID = ln_device_id
    AND H.HOST_PORT_NUM = 0;

    IF ln_base_host_id IS NULL THEN
        pkg_device_configuration.sp_create_default_hosts(ln_device_id, ln_new_host_count, pn_result_cd, pv_error_message);
        IF pn_result_cd != PKG_CONST.RESULT__SUCCESS THEN
            RETURN;
        END IF;
        SELECT H.HOST_ID
        INTO ln_base_host_id
        FROM DEVICE.HOST H
        WHERE H.DEVICE_ID = ln_device_id
        AND H.HOST_PORT_NUM = 0;
    END IF;

  -- check that sale amount is not greater than partial approval
  IF ln_partial_auth_amount IS NOT NULL AND ln_partial_auth_amount < ln_sale_amount THEN
      INSERT INTO PSS.TRAN_LINE_ITEM (
                    TRAN_ID,
                    HOST_ID,
                    TRAN_LINE_ITEM_TYPE_ID,
                    TRAN_LINE_ITEM_AMOUNT,
                    TRAN_LINE_ITEM_QUANTITY,
                    TRAN_LINE_ITEM_DESC,
                    TRAN_LINE_ITEM_BATCH_TYPE_CD,
                    TRAN_LINE_ITEM_TAX)
                SELECT
                    pn_tran_id,
                    ln_base_host_id,
                    TRAN_LINE_ITEM_TYPE_ID,
                    (ln_partial_auth_amount - ln_sale_amount) / pn_minor_currency_factor,
                    1,
                    'Sale amount higher than approved',
                    pc_tran_batch_type_cd,
                    0
                FROM PSS.TRAN_LINE_ITEM_TYPE
                WHERE TRAN_LINE_ITEM_TYPE_ID = 325;
      ln_sale_amount := ln_partial_auth_amount;
  END IF;

    -- Softcard Discount promotion
    IF pc_client_payment_type_cd IN (PKG_CONST.CLNT_PMNT_TYPE__ISIS_CREDIT, PKG_CONST.CLNT_PMNT_TYPE__ISIS_SPECIAL)
        AND ln_sale_amount > 0 AND ln_sale_amount = ln_original_sale_amount AND REGEXP_LIKE(lv_device_serial_cd, DBADMIN.PKG_GLOBAL.GET_APP_SETTING('SOFTCARD_DISCOUNT_PROMO_SERIAL_CD_REGEX')) THEN
        ln_discount_percent := DBADMIN.TO_NUMBER_OR_NULL(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('SOFTCARD_DISCOUNT_PROMO_PERCENT'));

        IF ln_discount_percent IS NOT NULL THEN
            ln_discount_amount := ROUND(ln_sale_amount * ln_discount_percent);
            IF ln_discount_amount > 0 THEN
                IF ln_discount_amount > ln_sale_amount THEN
                    ln_discount_amount := ln_sale_amount;
                END IF;
                ln_sale_amount := ln_sale_amount - ln_discount_amount;

                -- create Softcard Discount line item
                INSERT INTO PSS.TRAN_LINE_ITEM (
                    TRAN_ID,
                    HOST_ID,
                    TRAN_LINE_ITEM_TYPE_ID,
                    TRAN_LINE_ITEM_AMOUNT,
                    TRAN_LINE_ITEM_QUANTITY,
                    TRAN_LINE_ITEM_DESC,
                    TRAN_LINE_ITEM_BATCH_TYPE_CD,
                    TRAN_LINE_ITEM_TAX)
                SELECT
                    pn_tran_id,
                    ln_base_host_id,
                    tran_line_item_type_id,
                    -ln_discount_amount / pn_minor_currency_factor,
                    1,
                    tran_line_item_type_desc || ' ' || ln_discount_percent * 100 || '%',
                    pc_tran_batch_type_cd,
                    0
                FROM pss.tran_line_item_type
                WHERE tran_line_item_type_id = 208;
            END IF;
        END IF;
    END IF;
    -- don't apply multiple discounts
    IF ln_sale_amount > 0 AND ln_sale_amount = ln_original_sale_amount AND lc_payment_subtype_class!='Promotion' THEN
        CREATE_DISCOUNT_TRAN_LINE_ITEM (pn_tran_id,
        ln_consumer_acct_id,
        ln_pos_pta_id,
        pn_minor_currency_factor,
        ld_tran_start_ts,
        ln_base_host_id,
        ln_device_id,
        ln_sale_amount);
    END IF;


    IF ln_original_sale_amount != ln_sale_amount THEN
        UPDATE pss.sale
        SET sale_amount = ln_sale_amount / pn_minor_currency_factor
        WHERE tran_id = pn_tran_id;
    END IF;

    SELECT /*+ INDEX(TLI IF1_TRAN_LINE_ITEM) */ NVL(SUM((NVL(TRAN_LINE_ITEM_AMOUNT, 0) + NVL(TRAN_LINE_ITEM_TAX, 0)) * NVL(TRAN_LINE_ITEM_QUANTITY, 0)), 0),
           COUNT(1)
      INTO ln_tli_total, pn_tli_count
      FROM PSS.TRAN_LINE_ITEM TLI
     WHERE TRAN_ID = pn_tran_id
       AND TRAN_LINE_ITEM_BATCH_TYPE_CD = pc_tran_batch_type_cd;

    IF pv_tran_device_result_type_cd IN (PKG_CONST.TRAN_DEV_RES__CANCELLED,
            PKG_CONST.TRAN_DEV_RES__AUTH_FAILURE,
            PKG_CONST.TRAN_DEV_RES__FAILURE,
            PKG_CONST.TRAN_DEV_RES__TIMEOUT)
        OR NVL(pn_sale_result_id, -1) != PKG_CONST.SALE_RES__SUCCESS
        OR ln_sale_amount = 0 THEN
        IF ln_tli_total != 0 THEN
            ln_adj_tli := PKG_CONST.TLI__CANCELLATION_ADJMT;
            ln_adj_amt := -ln_tli_total;
        END IF;
    ELSE
        ln_adj_amt := ln_sale_amount / pn_minor_currency_factor - ln_tli_total;
		IF pn_tli_count = 0 THEN
			-- Purchase line item, mostly for micro-markets and other Quick Connect kiosks that don't send line items
			ln_adj_tli := 326;
        ELSIF ln_adj_amt > 0 THEN
			ln_adj_tli := PKG_CONST.TLI__POS_DISCREPANCY_ADJMT;
        ELSIF ln_adj_amt < 0 THEN
            ln_adj_tli := PKG_CONST.TLI__NEG_DISCREPANCY_ADJMT;
        END IF;
    END IF;

    IF ln_device_type_id IN (0, 1) AND pc_sale_type_cd != PKG_CONST.SALE_TYPE__CASH THEN
        IF pn_tli_count > 0 AND ln_adj_amt != 0 THEN
            IF ln_adj_amt > 0 THEN
                -- create Transaction Amount Summary record
                INSERT INTO pss.tran_line_item (
                    tran_id,
                    host_id,
                    tran_line_item_type_id,
                    tran_line_item_amount,
                    tran_line_item_quantity,
                    tran_line_item_desc,
                    tran_line_item_batch_type_cd,
                    tran_line_item_tax)
                VALUES(
                    pn_tran_id,
                    ln_base_host_id,
                    201,
                    ln_adj_amt,
                    1,
                    'Transaction Amount Summary',
                    pc_tran_batch_type_cd,
                    pn_sale_tax);
            ELSE
                INSERT INTO pss.tran_line_item (
                    tran_id,
                    host_id,
                    tran_line_item_type_id,
                    tran_line_item_amount,
                    tran_line_item_quantity,
                    tran_line_item_desc,
                    tran_line_item_batch_type_cd,
                    tran_line_item_tax)
                SELECT
                    pn_tran_id,
                    ln_base_host_id,
                    tran_line_item_type_id,
                    ln_adj_amt,
                    1,
                    tran_line_item_type_desc,
                    pc_tran_batch_type_cd,
                    pn_sale_tax
                FROM pss.tran_line_item_type
                WHERE tran_line_item_type_id = PKG_CONST.TLI__NEG_DISCREPANCY_ADJMT;
            END IF;
        END IF;
   ELSE
        IF ln_adj_tli > -1 THEN
            INSERT INTO pss.tran_line_item (
                tran_id,
                host_id,
                tran_line_item_type_id,
                tran_line_item_amount,
                tran_line_item_quantity,
                tran_line_item_desc,
                tran_line_item_batch_type_cd,
                tran_line_item_tax)
            SELECT
                pn_tran_id,
                ln_base_host_id,
                ln_adj_tli,
                ln_adj_amt,
                1,
                tran_line_item_type_desc,
                pc_tran_batch_type_cd,
                pn_sale_tax
            FROM pss.tran_line_item_type
            WHERE tran_line_item_type_id = ln_adj_tli;
        END IF;
    END IF;

    IF pn_tli_count = 0 AND pc_tran_import_needed = 'Y' THEN
        UPDATE PSS.SALE
           SET IMPORTED = '-'
         WHERE TRAN_ID = pn_tran_id
           AND IMPORTED NOT IN('-', 'Y');
    END IF;

    IF lc_tran_state_cd NOT IN (PKG_CONST.TRAN_STATE__SALE_NO_AUTH, PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND, PKG_CONST.TRAN_STATE__SALE_NO_AUTH_ERROR) THEN
        --if backoffice virtual device, update PAYOR.LAST_CHARGE_*
        IF pn_sale_amount > 0 AND ln_consumer_acct_id IS NOT NULL AND lc_backoffice_virtual_flag = 'Y' THEN
            DECLARE
                ld_last_ts DATE;
            BEGIN
                SELECT MAX(TO_DATE(TRIM(XI.TRAN_LINE_ITEM_DESC), F.FMT))
                  INTO ld_last_ts
                  FROM PSS.TRAN_LINE_ITEM XI
                  JOIN (SELECT '' REGEX, '' FMT FROM DUAL WHERE 1=0
                  -- This allows us to handle multiple formats
                  UNION ALL SELECT '[0-9][0-9]/[0-9][0-9]/[0-9]{4}', 'MM/DD/YYYY' FROM DUAL) F ON REGEXP_LIKE(TRIM(XI.TRAN_LINE_ITEM_DESC), F.REGEX)
                 WHERE XI.TRAN_ID = pn_tran_id
                   AND XI.TRAN_LINE_ITEM_TYPE_ID = 409
                   AND XI.TRAN_LINE_ITEM_DESC IS NOT NULL;
                UPDATE CORP.PAYOR
                   SET LAST_CHARGE_AMOUNT = CASE WHEN NVL(LAST_CHARGE_TS, MIN_DATE) < ld_tran_start_ts THEN ln_sale_amount / pn_minor_currency_factor ELSE LAST_CHARGE_AMOUNT END,
                       LAST_CHARGE_TS = CASE WHEN NVL(LAST_CHARGE_TS, MIN_DATE) < ld_tran_start_ts THEN ld_tran_start_ts ELSE LAST_CHARGE_TS END,
                       RECUR_LAST_TS = CASE WHEN NVL(RECUR_LAST_TS, MIN_DATE) < ld_last_ts THEN ld_last_ts ELSE RECUR_LAST_TS END
                 WHERE PAY_TO_DEVICE_ID = ln_device_id
                   AND GLOBAL_ACCOUNT_ID = (SELECT GLOBAL_ACCOUNT_ID FROM PSS.CONSUMER_ACCT_BASE WHERE CONSUMER_ACCT_ID = ln_consumer_acct_id);
            EXCEPTION
                WHEN OTHERS THEN
                    IF SQLCODE < -1899 OR SQLCODE > -1800 THEN
                        RAISE;
                    END IF;
            END;
        END IF;
        pc_session_update_needed := 'Y';
    END IF;

    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;

PROCEDURE CREATE_REPLENISHMENT(
    pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
    pn_auth_id PSS.AUTH.AUTH_ID%TYPE,
    pn_replenish_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
    pv_tran_device_result_type_cd PSS.TRAN.TRAN_DEVICE_RESULT_TYPE_CD%TYPE,
    pn_device_batch_id PSS.SALE.DEVICE_BATCH_ID%TYPE,
    pc_receipt_result_cd PSS.SALE.RECEIPT_RESULT_CD%TYPE,
    pc_auth_only CHAR,
    pc_tran_state_cd OUT VARCHAR2,
    pc_client_payment_type_cd OUT VARCHAR2,
    pn_replenish_amount OUT PSS.AUTH.AUTH_AMT_APPROVED%TYPE,
    pn_replenish_balance_amount OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE)
IS
    ln_host_id PSS.TRAN_LINE_ITEM.HOST_ID%TYPE;
    ln_tran_line_item_id PSS.TRAN_LINE_ITEM.TRAN_LINE_ITEM_ID%TYPE;
    ln_result_cd NUMBER;
    ln_new_host_count NUMBER;
    lv_error_message VARCHAR2(4000);
    ln_device_id PSS.POS.DEVICE_ID%TYPE;
    ln_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE;
    ld_sale_utc_ts PSS.SALE.SALE_START_UTC_TS%TYPE;
    ld_sale_local_date PSS.TRAN.TRAN_START_TS%TYPE;
    lv_global_session_cd PSS.SALE.SALE_GLOBAL_SESSION_CD%TYPE;
    lv_device_name PSS.TRAN.DEVICE_NAME%TYPE;
    lv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE;
    lv_device_tran_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE;
    ld_auth_ts PSS.AUTH.AUTH_TS%TYPE;
    ln_consumer_acct_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_TYPE_ID%TYPE;
    ln_consumer_acct_sub_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_SUB_TYPE_ID%TYPE;
    lc_payment_type_cd PSS.CLIENT_PAYMENT_TYPE.PAYMENT_TYPE_CD%TYPE;
    lc_auth_hold_used PSS.AUTH.AUTH_HOLD_USED%TYPE;
    lc_is_cash CHAR(1);
    ln_corp_customer_id PSS.CONSUMER_ACCT.CORP_CUSTOMER_ID%TYPE;
    ln_consumer_acct_identifier PSS.CONSUMER_ACCT.CONSUMER_ACCT_IDENTIFIER%TYPE;
    lv_currency_cd PSS.POS_PTA.CURRENCY_CD%TYPE;
    ln_doc_id CORP.DOC.DOC_ID%TYPE;
    ln_ledger_id CORP.LEDGER.LEDGER_ID%TYPE;
    ln_trans_type_id PSS.PAYMENT_SUBTYPE.TRANS_TYPE_ID%TYPE;
    lc_complete_flag CHAR(1);
    lc_new_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
BEGIN
    SELECT DECODE(pc_auth_only, 'Y', 0, A.AUTH_AMT_APPROVED), A.AUTH_TS, A.AUTH_HOLD_USED,
           CASE WHEN A.CARD_KEY IS NOT NULL OR (a.AUTH_PARSED_ACCT_DATA IS NOT NULL AND INSTR(a.AUTH_PARSED_ACCT_DATA, '*') = 0) THEN 'Y' ELSE 'N' END,
           DECODE(CPT.PAYMENT_TYPE_CD, 'M', 'Y', 'N')
      INTO pn_replenish_amount, ld_auth_ts, lc_auth_hold_used, lc_complete_flag, lc_is_cash
      FROM PSS.AUTH A
      JOIN PSS.TRAN X ON A.TRAN_ID = X.TRAN_ID
      LEFT OUTER JOIN PSS.CLIENT_PAYMENT_TYPE CPT ON X.CLIENT_PAYMENT_TYPE_CD = CPT.CLIENT_PAYMENT_TYPE_CD
     WHERE A.AUTH_ID = pn_auth_id;

    UPDATE PSS.TRAN X
       SET TRAN_STATE_CD = CASE WHEN lc_is_cash = 'Y' THEN 'D'
                                WHEN TRAN_STATE_CD NOT IN('6', '0') THEN TRAN_STATE_CD
                                WHEN lc_complete_flag = 'N' THEN '4'
                                WHEN pc_auth_only = 'Y' AND lc_auth_hold_used = 'Y' THEN '8'
                                WHEN pc_auth_only = 'Y' AND lc_auth_hold_used != 'Y' THEN 'C'
                                ELSE '8' END,
           TRAN_END_TS = TRAN_START_TS,
           TRAN_UPLOAD_TS = SYSDATE,
           TRAN_DEVICE_RESULT_TYPE_CD = pv_tran_device_result_type_cd
     WHERE TRAN_ID = pn_tran_id
     RETURNING CLIENT_PAYMENT_TYPE_CD, POS_PTA_ID, TRAN_START_TS, AUTH_GLOBAL_SESSION_CD, DEVICE_NAME, TRAN_DEVICE_TRAN_CD, TRAN_STATE_CD
      INTO pc_client_payment_type_cd, ln_pos_pta_id, ld_sale_local_date, lv_global_session_cd, lv_device_name, lv_device_tran_cd, pc_tran_state_cd;

    ld_sale_utc_ts := TO_TIMESTAMP(ld_auth_ts) AT TIME ZONE 'GMT';
    INSERT INTO PSS.SALE (
            TRAN_ID,
            DEVICE_BATCH_ID,
            SALE_TYPE_CD,
            SALE_START_UTC_TS,
            SALE_END_UTC_TS,
            SALE_UTC_OFFSET_MIN,
            SALE_RESULT_ID,
            SALE_AMOUNT,
            RECEIPT_RESULT_CD,
            HASH_TYPE_CD,
            TRAN_LINE_ITEM_HASH,
            SALE_GLOBAL_SESSION_CD,
            IMPORTED,
            VOID_ALLOWED)
     SELECT pn_tran_id,
            pn_device_batch_id,
            DECODE(lc_is_cash, 'Y', 'C', 'A'),
            ld_sale_utc_ts,
            ld_sale_utc_ts,
            (ld_sale_local_date - CAST(ld_sale_utc_ts AS DATE)) * 24 * 60,
            DECODE(pc_auth_only, 'Y', 1, 0),
            pn_replenish_amount,
            pc_receipt_result_cd,
            'SHA1',
            '00',
            lv_global_session_cd,
            'N',
            'Y'
       FROM DUAL;

    SELECT D.DEVICE_ID, D.DEVICE_SERIAL_CD, PP.CURRENCY_CD, PST.TRANS_TYPE_ID
      INTO ln_device_id, lv_device_serial_cd, lv_currency_cd, ln_trans_type_id
      FROM DEVICE.DEVICE D
      JOIN PSS.POS P ON D.DEVICE_ID = P.DEVICE_ID
      JOIN PSS.POS_PTA PP ON PP.POS_ID = P.POS_ID
      JOIN PSS.PAYMENT_SUBTYPE PST ON PP.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
     WHERE PP.POS_PTA_ID = ln_pos_pta_id;

    ln_host_id := SF_FIND_HOST_ID(ln_device_id, 0, 0);
    IF ln_host_id IS NULL THEN
        -- create default hosts
        PKG_DEVICE_CONFIGURATION.SP_CREATE_DEFAULT_HOSTS(ln_device_id, ln_new_host_count, ln_result_cd, lv_error_message);
        IF ln_result_cd != PKG_CONST.RESULT__SUCCESS THEN
            RETURN;
        END IF;
        ln_host_id := SF_FIND_HOST_ID(ln_device_id, 0, 0);
    END IF;

    SELECT PSS.SEQ_TLI_ID.NEXTVAL
        INTO ln_tran_line_item_id
        FROM DUAL;

    INSERT INTO PSS.TRAN_LINE_ITEM (
        TRAN_LINE_ITEM_ID,
        TRAN_ID,
        TRAN_LINE_ITEM_AMOUNT,
        TRAN_LINE_ITEM_POSITION_CD,
        TRAN_LINE_ITEM_TAX,
        TRAN_LINE_ITEM_TYPE_ID,
        TRAN_LINE_ITEM_QUANTITY,
        TRAN_LINE_ITEM_DESC,
        HOST_ID,
        TRAN_LINE_ITEM_BATCH_TYPE_CD,
        TRAN_LINE_ITEM_TS,
        SALE_RESULT_ID,
        APPLY_TO_CONSUMER_ACCT_ID)
    SELECT
        ln_tran_line_item_id,
        pn_tran_id,
        pn_replenish_amount,
        NULL,
        0,
        550,
        1,
        'Replenishment of card id ' || CAB.GLOBAL_ACCOUNT_ID,
        ln_host_id,
        'A',
        ld_sale_local_date,
        0,
        pn_replenish_consumer_acct_id
    FROM PSS.CONSUMER_ACCT_BASE CAB
    WHERE CONSUMER_ACCT_ID = pn_replenish_consumer_acct_id;

    UPDATE PSS.CONSUMER_ACCT
       SET CONSUMER_ACCT_BALANCE = CONSUMER_ACCT_BALANCE + pn_replenish_amount,
           CONSUMER_ACCT_REPLEN_BALANCE = CONSUMER_ACCT_REPLEN_BALANCE + pn_replenish_amount,
           CONSUMER_ACCT_REPLENISH_TOTAL = CONSUMER_ACCT_REPLENISH_TOTAL + pn_replenish_amount
     WHERE CONSUMER_ACCT_ID = pn_replenish_consumer_acct_id
      RETURNING CONSUMER_ACCT_TYPE_ID, CONSUMER_ACCT_SUB_TYPE_ID, CONSUMER_ACCT_BALANCE, CORP_CUSTOMER_ID, CONSUMER_ACCT_IDENTIFIER
      INTO ln_consumer_acct_type_id, ln_consumer_acct_sub_type_id, pn_replenish_balance_amount, ln_corp_customer_id, ln_consumer_acct_identifier;

    IF ln_consumer_acct_type_id IN(3,6) THEN
        IF lc_is_cash = 'Y' AND ln_consumer_acct_sub_type_id = 1 THEN
            IF ln_consumer_acct_identifier IS NULL THEN
                SELECT GLOBAL_ACCOUNT_ID
                  INTO ln_consumer_acct_identifier
                  FROM PSS.CONSUMER_ACCT_BASE
                 WHERE CONSUMER_ACCT_ID = pn_replenish_consumer_acct_id;
            END IF;
            -- add adjustment to ledger
            CORP.PAYMENTS_PKG.ADJUSTMENT_INS(ln_corp_customer_id, 'Cash Replenishment', lv_currency_cd, 'Cash Replenishment of card id ' || ln_consumer_acct_identifier,
                -pn_replenish_amount, ln_doc_id, ln_ledger_id);
        ELSIF ln_consumer_acct_sub_type_id = 2 THEN
            UPDATE PSS.AUTH
               SET OVERRIDE_TRANS_TYPE_ID = (
                      SELECT TT.OPERATOR_TRANS_TYPE_ID
                        FROM REPORT.TRANS_TYPE TT
                       WHERE TT.TRANS_TYPE_ID = ln_trans_type_id)
             WHERE AUTH_ID = pn_auth_id;
        END IF;
        ADD_REPLENISH_BONUSES(
            pn_tran_id,
            pn_replenish_consumer_acct_id,
            pn_replenish_amount,
            ld_auth_ts,
            ln_consumer_acct_type_id);
    END IF;
END;

PROCEDURE UPDATE_AUTHORIZATION(
    pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
    pn_auth_id PSS.AUTH.AUTH_ID%TYPE,
    pv_card_key PSS.AUTH.CARD_KEY%TYPE,
    pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE)
IS
    ld_auth_ts PSS.AUTH.AUTH_TS%TYPE;
    lv_masked_card_number PSS.TRAN.TRAN_RECEIVED_RAW_ACCT_DATA%TYPE;
    ln_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE;
    lv_device_name PSS.TRAN.DEVICE_NAME%TYPE;
    lv_device_tran_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE;
    ln_apply_to_consumer_acct_id PSS.TRAN_LINE_ITEM.APPLY_TO_CONSUMER_ACCT_ID%TYPE;
    lc_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
BEGIN
    UPDATE PSS.AUTH
       SET CARD_KEY = CASE WHEN CARD_KEY IS NOT NULL THEN CARD_KEY ELSE pv_card_key END
     WHERE AUTH_ID = pn_auth_id
       AND TRAN_ID = pn_tran_id
       AND pv_card_key IS NOT NULL
     RETURNING AUTH_TS, CASE
            WHEN AUTH_HOLD_USED = 'Y' AND AUTH_STATE_ID = 3 THEN 'W'
            WHEN AUTH_HOLD_USED = 'Y' AND AUTH_STATE_ID IN(2,5) THEN '8'
            WHEN AUTH_HOLD_USED != 'Y' AND AUTH_STATE_ID IN(2,5) THEN 'C'END
     INTO ld_auth_ts, lc_tran_state_cd;

    UPDATE PSS.TRAN
       SET CONSUMER_ACCT_ID = CASE WHEN CONSUMER_ACCT_ID IS NOT NULL THEN CONSUMER_ACCT_ID ELSE pn_consumer_acct_id END,
           TRAN_STATE_CD = CASE
                WHEN TRAN_STATE_CD = '4' AND pv_card_key IS NOT NULL AND lc_tran_state_cd IS NOT NULL THEN lc_tran_state_cd
                WHEN TRAN_STATE_CD = '7' AND lc_tran_state_cd IS NOT NULL THEN lc_tran_state_cd
                ELSE TRAN_STATE_CD END
     WHERE TRAN_ID = pn_tran_id
     RETURNING TRAN_RECEIVED_RAW_ACCT_DATA, POS_PTA_ID, DEVICE_NAME, TRAN_DEVICE_TRAN_CD
          INTO lv_masked_card_number, ln_pos_pta_id, lv_device_name, lv_device_tran_cd;

    SELECT MAX(APPLY_TO_CONSUMER_ACCT_ID)
          INTO ln_apply_to_consumer_acct_id
          FROM PSS.TRAN_LINE_ITEM
         WHERE TRAN_ID = pn_tran_id
           AND TRAN_LINE_ITEM_TYPE_ID in(550, 556);
    IF pv_card_key IS NOT NULL THEN
        IF ln_apply_to_consumer_acct_id IS NOT NULL THEN
            FINISH_REPLENISH_SETUP(
                pn_tran_id,
                ln_apply_to_consumer_acct_id,
                pv_card_key,
                ld_auth_ts,
                lv_masked_card_number,
                ln_pos_pta_id);
        END IF;
    END IF;
    DELETE FROM PSS.CONSUMER_ACCT_PEND_REPLENISH
         WHERE DEVICE_NAME = lv_device_name
           AND DEVICE_TRAN_CD = TO_NUMBER_OR_NULL(lv_device_tran_cd)
           AND NOT EXISTS(SELECT 1 FROM PSS.CONSUMER_ACCT where consumer_acct_id=ln_apply_to_consumer_acct_id and CONSUMER_ACCT_TYPE_ID=6);
END;

-- R37+ signature
PROCEDURE SP_CREATE_AUTH(
   pv_global_event_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
   pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
   pn_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE,
   pv_device_event_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE,
   pc_invalid_device_event_cd CHAR,
   pn_tran_start_time NUMBER,
   pn_auth_utc_ms NUMBER,
   pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
   pc_entry_method CHAR,
   pv_masked_card_number PSS.TRAN.TRAN_RECEIVED_RAW_ACCT_DATA%TYPE,
   pn_consumer_acct_id PSS.TRAN.CONSUMER_ACCT_ID%TYPE,
   pd_auth_ts PSS.AUTH.AUTH_TS%TYPE,
   pv_authority_resp_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
   pv_authority_resp_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
   pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
   pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
   pt_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
   pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
   pn_trace_number PSS.AUTH.TRACE_NUMBER%TYPE,
   pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
   pn_auth_amt PSS.AUTH.AUTH_AMT%TYPE,
   pn_balance_amt PSS.AUTH.AUTH_BALANCE_AMT%TYPE,
   pn_requested_amt PSS.AUTH.AUTH_AUTHORITY_AMT_RQST%TYPE,
   pn_received_amt PSS.AUTH.AUTH_AUTHORITY_AMT_RCVD%TYPE,
   pn_add_auth_hold_days NUMBER,
   pc_auth_hold_used PSS.AUTH.AUTH_HOLD_USED%TYPE,
   pv_global_session_cd VARCHAR2,
   pc_ignore_dup CHAR,
   pn_auth_action_id PSS.AUTH.AUTH_ACTION_ID%TYPE,
   pn_auth_action_bitmap PSS.AUTH.AUTH_ACTION_BITMAP%TYPE,
   pc_sent_to_device CHAR,
   pc_pass_thru CHAR,
   pv_card_key PSS.AUTH.CARD_KEY%TYPE,
   pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
   pc_tran_state_cd OUT VARCHAR2,
   pc_tran_import_needed OUT VARCHAR2,
   pc_session_update_needed OUT VARCHAR2,
   pv_sale_global_session_cd OUT VARCHAR2,
   pn_sale_session_start_time OUT PSS.SALE.SALE_SESSION_START_TIME%TYPE,
   pc_client_payment_type_cd OUT VARCHAR2,
   pn_sale_amount OUT pss.sale.sale_amount%TYPE,
   pn_tli_count OUT NUMBER,
   pn_auth_id OUT PSS.AUTH.AUTH_ID%TYPE,
   pv_denied_reason_name IN PSS.DENIED_REASON.DENIED_REASON_NAME%TYPE DEFAULT NULL,
   pv_card_type PSS.TRAN.CARD_TYPE%TYPE DEFAULT NULL,
   pn_longitude PSS.TRAN.LONGITUDE%TYPE DEFAULT NULL,
   pn_latitude PSS.TRAN.LATITUDE%TYPE DEFAULT NULL,
   pv_merchant_cd PSS.AUTH.MERCHANT_CD%TYPE DEFAULT NULL,
   pv_terminal_cd PSS.AUTH.TERMINAL_CD%TYPE DEFAULT NULL
   )
IS
   lc_sale_type_cd PSS.SALE.SALE_TYPE_CD%TYPE;
   ln_sale_result_id PSS.SALE.SALE_RESULT_ID%TYPE;
   ld_orig_trace_number PSS.AUTH.TRACE_NUMBER%TYPE;
   lv_orig_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE;
   lv_last_lock_utc_ts VARCHAR2(128);
   lc_invalid_device_event_cd CHAR := pc_invalid_device_event_cd;
   ld_tran_start_ts PSS.TRAN.TRAN_START_TS%TYPE := CAST(MILLIS_TO_TIMESTAMP(pn_tran_start_time) AS DATE);
   lc_imported PSS.SALE.IMPORTED%TYPE;
   ln_auth_amt_approved pss.auth.auth_amt_approved%TYPE;
   ln_auth_amt_allowed pss.auth.auth_amt_approved%TYPE;
   ln_sale_over_auth_amt_percent NUMBER;
   lv_email_from_address engine.app_setting.app_setting_value%TYPE;
   lv_email_to_address engine.app_setting.app_setting_value%TYPE;
   lv_error pss.tran.tran_info%TYPE;
   ln_device_id device.device_id%TYPE;
   lc_payment_subtype_key_id pss.pos_pta.payment_subtype_key_id%TYPE;
   lc_payment_subtype_class pss.payment_subtype.payment_subtype_class%TYPE;
   lc_previous_tran_state_cd pss.tran.tran_state_cd%TYPE;
   lt_auth_utc_ts pss.consumer_acct_device.last_used_utc_ts%TYPE;
   ln_override_trans_type_id PSS.AUTH.OVERRIDE_TRANS_TYPE_ID%TYPE;
   ln_consumer_acct_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_TYPE_ID%TYPE;
   ln_consumer_acct_sub_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_SUB_TYPE_ID%TYPE;
   lc_backoffice_virtual_flag CHAR(1);
   ln_denied_reason_id PSS.DENIED_REASON.DENIED_REASON_ID%TYPE;
   lv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE;
   ln_discount_percent report.campaign.discount_percent%TYPE;
   ln_discount_amount NUMBER;
   ln_base_host_id PSS.TRAN_LINE_ITEM.HOST_ID%TYPE;
   ln_new_host_count PLS_INTEGER;
   ln_result_cd NUMBER;
   lv_error_message VARCHAR2(255);
   ld_orig_auth_ts PSS.AUTH.AUTH_TS%TYPE;
   ln_campaign_id PSS.TRAN_LINE_ITEM.CAMPAIGN_ID%TYPE;
   lv_campaign_promo_code REPORT.CAMPAIGN.PROMO_CODE%TYPE;
   ln_tran_line_item_type_id PSS.TRAN_LINE_ITEM_TYPE.TRAN_LINE_ITEM_TYPE_ID%TYPE;
   ln_orig_sale_amount pss.sale.sale_amount%TYPE;
   ln_count INTEGER;
   ln_check_min INTEGER;
   ln_num_auths INTEGER;
   ln_blacklist_min INTEGER;
   ld_sysdate DATE;
   ln_auth_state_id PSS.AUTH.AUTH_STATE_ID%TYPE;
   lc_card_velocity_ind PSS.PAYMENT_SUBTYPE_CLASS.CARD_VELOCITY_IND%TYPE;
BEGIN
    pn_sale_amount := 0;
    pc_session_update_needed := 'N';

    IF pc_auth_result_cd = 'Y' THEN
        ln_auth_amt_approved := pn_auth_amt;
    ELSIF pc_auth_result_cd = 'P' THEN
        ln_auth_amt_approved := pn_received_amt;
    ELSIF pc_auth_result_cd IN('V', 'C', 'M') THEN
        ln_auth_amt_approved :=  NVL(pn_received_amt, pn_auth_amt);
    END IF;

    pc_tran_import_needed := 'N';
    lv_last_lock_utc_ts := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', pv_device_name || ':' || pv_device_event_cd);
    
    BEGIN
        SELECT TRAN_ID, SALE_TYPE_CD, TRAN_STATE_CD, TRACE_NUMBER, SALE_RESULT_ID, IMPORTED, SALE_GLOBAL_SESSION_CD, SALE_SESSION_START_TIME, AUTH_RESULT_CD, AUTH_TS, SALE_AMOUNT_MINOR
        INTO pn_tran_id, lc_sale_type_cd, pc_tran_state_cd, ld_orig_trace_number, ln_sale_result_id, lc_imported, pv_sale_global_session_cd, pn_sale_session_start_time, lv_orig_auth_result_cd, ld_orig_auth_ts, pn_sale_amount
        FROM
        (
            SELECT /*+ INDEX(t IDX_DEVICE_TRAN_CD) */ T.TRAN_ID, S.SALE_TYPE_CD, T.TRAN_STATE_CD, A.TRACE_NUMBER,
                   S.SALE_RESULT_ID, S.IMPORTED, S.SALE_GLOBAL_SESSION_CD, S.SALE_SESSION_START_TIME, A.AUTH_RESULT_CD, A.CARD_KEY, A.AUTH_TS, S.SALE_AMOUNT * pn_minor_currency_factor SALE_AMOUNT_MINOR
            FROM pss.tran t
            LEFT OUTER JOIN pss.sale s ON t.tran_id = s.tran_id
            LEFT OUTER JOIN pss.auth a ON t.tran_id = a.tran_id AND a.auth_type_cd = 'N'
            WHERE t.tran_device_tran_cd = pv_device_event_cd
              AND (t.tran_global_trans_cd = pv_global_event_cd OR t.tran_global_trans_cd LIKE pv_global_event_cd || ':%')
            ORDER BY CASE WHEN a.trace_number = pn_trace_number AND a.AUTH_TS = pd_auth_ts THEN 1 ELSE 2 END,
                CASE WHEN t.AUTH_GLOBAL_SESSION_CD = pv_global_session_cd THEN 1 ELSE 2 END,
                CASE WHEN a.AUTH_RESULT_CD IN('Y', 'P', 'V', 'C', 'M') THEN 1 ELSE 2 END,
                CASE WHEN s.sale_type_cd IN('A', 'I') THEN 1 ELSE 2 END,
                CASE WHEN t.tran_global_trans_cd = pv_global_event_cd THEN 1 ELSE 2 END,
                t.tran_start_ts, t.created_ts, a.created_ts
        )
        WHERE ROWNUM = 1;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            pn_tran_id := NULL;
    END;

    IF pn_tran_id IS NOT NULL AND ((lc_sale_type_cd IS NOT NULL AND (lc_sale_type_cd = PKG_CONST.SALE_TYPE__CASH OR pn_auth_action_id IS NOT NULL))
        OR (lc_sale_type_cd IS NULL AND pn_auth_amt = 0 AND pv_device_event_cd = '0' AND (ld_orig_trace_number != pn_trace_number OR ld_orig_auth_ts != pd_auth_ts))) THEN
        pn_tran_id := NULL; -- this is not a match so insert it
        lc_invalid_device_event_cd := 'Y'; -- add ':' || tran_id to tran_global_trans_cd
    END IF;

    ln_orig_sale_amount := pn_sale_amount;
    IF pn_tran_id IS NOT NULL THEN
        lc_previous_tran_state_cd := pc_tran_state_cd;
        IF ld_orig_trace_number = pn_trace_number THEN
            IF lc_imported NOT IN('Y', '-') AND lc_sale_type_cd IN(PKG_CONST.SALE_TYPE__ACTUAL, PKG_CONST.SALE_TYPE__CASH) AND ln_sale_result_id = 0 AND pc_tran_state_cd NOT IN('F', 'G', 'Z') THEN
                pc_tran_import_needed := 'Y';
            ELSE
                pc_tran_import_needed := 'N';
            END IF;
            RETURN; -- already inserted; exit
        ELSIF pc_auth_result_cd IN('Y', 'P', 'V', 'C', 'M') AND lv_orig_auth_result_cd  IN('Y', 'P', 'V', 'C', 'M') THEN -- Device sent dup tran id in two different auths
            pc_tran_state_cd := PKG_CONST.TRAN_STATE__DUPLICATE;
            lc_invalid_device_event_cd := 'Y';
        ELSIF pc_pass_thru = 'N' THEN -- This allows saving pass-thru auths
            IF pc_tran_state_cd IN(PKG_CONST.TRAN_STATE__SALE_NO_AUTH, PKG_CONST.TRAN_STATE__SALE_NO_AUTH_ERROR, PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND, PKG_CONST.TRAN_STATE__COMPLETE_ERROR)
                OR (pc_tran_state_cd = PKG_CONST.TRAN_STATE__CLIENT_CANCELLED AND ln_sale_result_id != 0 /*Not 'Success'*/) THEN

                IF pc_auth_result_cd IN('Y', 'P', 'V', 'C', 'M') THEN
                    IF pn_sale_amount > 0 AND ln_auth_amt_approved > 0 AND pn_sale_amount > ln_auth_amt_approved THEN
                        SELECT GREATEST(ln_auth_amt_approved, NVL(NVL(MAX(DBADMIN.TO_NUMBER_OR_NULL(DS_MAA.DEVICE_SETTING_VALUE)), MAX(DBADMIN.TO_NUMBER_OR_NULL(DS_AUTH_AMT.DEVICE_SETTING_VALUE) * DECODE(D.DEVICE_TYPE_ID, 13, 1, 100))), 0)), MAX(H.HOST_ID), MAX(D.DEVICE_ID)
                          INTO ln_auth_amt_allowed, ln_base_host_id, ln_device_id
                          FROM PSS.POS_PTA PP
                          JOIN PSS.POS P ON PP.POS_ID = P.POS_ID
                          JOIN DEVICE.DEVICE D ON P.DEVICE_ID = D.DEVICE_ID
                          LEFT OUTER JOIN DEVICE.DEVICE_SETTING DS_MAA ON D.DEVICE_ID = DS_MAA.DEVICE_ID AND DS_MAA.DEVICE_SETTING_PARAMETER_CD = 'MAX_AUTH_AMOUNT'
                          LEFT OUTER JOIN DEVICE.DEVICE_SETTING DS_AUTH_AMT ON D.DEVICE_ID = DS_AUTH_AMT.DEVICE_ID
                           AND DS_AUTH_AMT.DEVICE_SETTING_PARAMETER_CD = DECODE(D.DEVICE_TYPE_ID, 13, '1200', 1, '195', 11, 'AUTHORIZATION_AMOUNT', 0, '195')
                          LEFT OUTER JOIN DEVICE.HOST H ON D.DEVICE_ID = H.DEVICE_ID
                         WHERE PP.POS_PTA_ID = pn_pos_pta_id;

                        ln_sale_over_auth_amt_percent := NVL(DBADMIN.TO_NUMBER_OR_NULL(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('ALLOWED_SALE_AMT_OVER_AUTH_AMT_PERCENT')), 100);
                        IF pn_sale_amount > ln_auth_amt_allowed + ln_auth_amt_allowed * ln_sale_over_auth_amt_percent / 100 THEN
                            pc_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE_ERROR;
                            lv_error := 'Error: Sale amount ' || TO_CHAR(pn_sale_amount / pn_minor_currency_factor, 'FM9,999,999,990.00') || ' exceeds allowed auth amount ' || TO_CHAR(ln_auth_amt_allowed / pn_minor_currency_factor, 'FM9,999,999,990.00') || ' by more than ' || ln_sale_over_auth_amt_percent || '%';
                            UPDATE PSS.TRAN
                            SET TRAN_INFO = SUBSTR(TRAN_INFO || DECODE(TRAN_INFO, NULL, '', ', ') || lv_error, 1, 1000)
                            WHERE TRAN_ID = pn_tran_id AND (TRAN_INFO IS NULL OR INSTR(TRAN_INFO, lv_error) = 0);

                            UPDATE DEVICE.DEVICE
                            SET DEVICE_ACTIVE_YN_FLAG = 'N'
                            WHERE DEVICE_NAME = pv_device_name AND DEVICE_ACTIVE_YN_FLAG = 'Y';

                            lv_email_from_address := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EMAIL_FROM_ADDRESS');
                            lv_email_to_address := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EMAIL_TO_ADDRESS_CUSTOMER_SERVICE');
                            INSERT INTO ENGINE.OB_EMAIL_QUEUE(OB_EMAIL_FROM_EMAIL_ADDR, OB_EMAIL_FROM_NAME, OB_EMAIL_TO_EMAIL_ADDR, OB_EMAIL_TO_NAME, OB_EMAIL_SUBJECT, OB_EMAIL_MSG)
                            VALUES(lv_email_from_address, lv_email_from_address, lv_email_to_address, lv_email_to_address, 'Invalid sale amount', lv_error || ', device: ' || pv_device_name || ', transaction ID: ' || pn_tran_id || '. Device has been disabled.');
                        ELSIF pc_auth_result_cd = 'P' THEN
                            -- use the base host for adjustments
                            IF ln_base_host_id IS NULL THEN
                                pkg_device_configuration.sp_create_default_hosts(ln_device_id, ln_new_host_count, ln_result_cd, lv_error_message);
                                IF ln_result_cd != PKG_CONST.RESULT__SUCCESS THEN
                                    RAISE_APPLICATION_ERROR(-20201, 'Could not create base host for device ' || ln_device_id || ': ' || lv_error_message);
                                END IF;
                                SELECT H.HOST_ID
                                  INTO ln_base_host_id
                                  FROM DEVICE.HOST H
                                 WHERE H.DEVICE_ID = ln_device_id
                                   AND H.HOST_PORT_NUM = 0;
                            END IF;

                            -- Create adjustment
                            INSERT INTO PSS.TRAN_LINE_ITEM (
                                TRAN_ID,
                                HOST_ID,
                                TRAN_LINE_ITEM_TYPE_ID,
                                TRAN_LINE_ITEM_AMOUNT,
                                TRAN_LINE_ITEM_QUANTITY,
                                TRAN_LINE_ITEM_DESC,
                                TRAN_LINE_ITEM_BATCH_TYPE_CD,
                                TRAN_LINE_ITEM_TAX)
                              SELECT
                                pn_tran_id,
                                ln_base_host_id,
                                TRAN_LINE_ITEM_TYPE_ID,
                                (ln_auth_amt_approved - pn_sale_amount) / pn_minor_currency_factor,
                                1,
                                'Sale amount higher than approved',
                                lc_sale_type_cd,
                                0
                              FROM PSS.TRAN_LINE_ITEM_TYPE
                              WHERE TRAN_LINE_ITEM_TYPE_ID = 325;
                            pn_sale_amount := ln_auth_amt_approved;
                            UPDATE PSS.SALE
                               SET SALE_AMOUNT = pn_sale_amount / pn_minor_currency_factor
                             WHERE TRAN_ID = pn_tran_id;
                        END IF;
                    END IF;
                    IF pc_tran_state_cd != PKG_CONST.TRAN_STATE__COMPLETE_ERROR THEN
                        IF lc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED THEN
                            pc_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH_INTENDED;
                        ELSE
                            pc_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH;
                        END IF;
                    END IF;
                ELSE
                    IF lc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED THEN
                        pc_tran_state_cd := PKG_CONST.TRAN_STATE__INTENDED_ERROR;
                    ELSE
                        pc_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE_ERROR;
                    END IF;
                END IF;

                SELECT pp.PAYMENT_SUBTYPE_KEY_ID, ps.PAYMENT_SUBTYPE_CLASS, ps.CLIENT_PAYMENT_TYPE_CD, P.DEVICE_ID, CASE WHEN D.DEVICE_TYPE_ID = 14 AND D.DEVICE_SUB_TYPE_ID IN(3, 4) THEN 'Y' ELSE 'N' END,
                    D.DEVICE_SERIAL_CD, H.HOST_ID, PSC.CARD_VELOCITY_IND
                  INTO lc_payment_subtype_key_id, lc_payment_subtype_class, pc_client_payment_type_cd, ln_device_id, lc_backoffice_virtual_flag,
                    lv_device_serial_cd, ln_base_host_id, lc_card_velocity_ind
                  FROM PSS.POS_PTA PP
                  JOIN PSS.POS P ON PP.POS_ID = P.POS_ID
                  JOIN DEVICE.DEVICE D ON P.DEVICE_ID = D.DEVICE_ID
                  JOIN PSS.PAYMENT_SUBTYPE PS ON PP.PAYMENT_SUBTYPE_ID = PS.PAYMENT_SUBTYPE_ID
				  JOIN PSS.PAYMENT_SUBTYPE_CLASS PSC ON PS.PAYMENT_SUBTYPE_CLASS = PSC.PAYMENT_SUBTYPE_CLASS
                  LEFT OUTER JOIN DEVICE.HOST H ON D.DEVICE_ID = H.DEVICE_ID AND H.HOST_PORT_NUM = 0
                 WHERE PP.POS_PTA_ID = pn_pos_pta_id;

                UPDATE PSS.TRAN
                   SET (TRAN_START_TS,
                        TRAN_END_TS,
                        TRAN_STATE_CD,
                        TRAN_RECEIVED_RAW_ACCT_DATA,
                        POS_PTA_ID,
                        CONSUMER_ACCT_ID,
                        AUTH_GLOBAL_SESSION_CD,
                        PAYMENT_SUBTYPE_KEY_ID,
                        PAYMENT_SUBTYPE_CLASS,
                        CLIENT_PAYMENT_TYPE_CD,
                        AUTH_HOLD_USED,
                        DEVICE_NAME,
                        CARD_TYPE,
                        LONGITUDE,
                        LATITUDE) =
                    (SELECT
                        ld_tran_start_ts,  /* TRAN_START_TS */
                        TRAN_END_TS - TRAN_START_TS + ld_tran_start_ts,  /* TRAN_END_TS */
                        pc_tran_state_cd,  /* TRAN_STATE_CD */
                        pv_masked_card_number,  /* TRAN_RECEIVED_RAW_ACCT_DATA */
                        pn_pos_pta_id, /* POS_PTA_ID */
                        pn_consumer_acct_id  /* CONSUMER_ACCT_ID */,
                        pv_global_session_cd,
                        lc_payment_subtype_key_id,
                        lc_payment_subtype_class,
                        pc_client_payment_type_cd,
                        pc_auth_hold_used,
                        pv_device_name,
                        pv_card_type,
                        pn_longitude,
                        pn_latitude
                    FROM dual
                    ) WHERE TRAN_ID = pn_tran_id;

                IF lc_previous_tran_state_cd IN (PKG_CONST.TRAN_STATE__SALE_NO_AUTH, PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND, PKG_CONST.TRAN_STATE__SALE_NO_AUTH_ERROR) THEN
                    --if backoffice virtual device, update PAYOR.LAST_CHARGE_*
                    IF pn_sale_amount > 0 AND pn_consumer_acct_id IS NOT NULL AND lc_backoffice_virtual_flag = 'Y' THEN
                        UPDATE CORP.PAYOR
                           SET LAST_CHARGE_AMOUNT = pn_sale_amount / pn_minor_currency_factor, LAST_CHARGE_TS = ld_tran_start_ts
                         WHERE PAY_TO_DEVICE_ID = ln_device_id
                           AND GLOBAL_ACCOUNT_ID = (SELECT GLOBAL_ACCOUNT_ID FROM PSS.CONSUMER_ACCT_BASE WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id)
                           AND NVL(LAST_CHARGE_TS, MIN_DATE) < ld_tran_start_ts;
                    END IF;
                    SELECT /*+ INDEX(TLI IF1_TRAN_LINE_ITEM) */ COUNT(1)
                    INTO pn_tli_count
                    FROM PSS.TRAN_LINE_ITEM TLI
                    WHERE TRAN_ID = pn_tran_id
                        AND TRAN_LINE_ITEM_BATCH_TYPE_CD = lc_sale_type_cd
                        AND TRAN_LINE_ITEM_TYPE_ID NOT IN (PKG_CONST.TLI__CANCELLATION_ADJMT, PKG_CONST.TLI__POS_DISCREPANCY_ADJMT, PKG_CONST.TLI__NEG_DISCREPANCY_ADJMT, 201, 326);

                    IF pv_sale_global_session_cd IS NOT NULL THEN
                        pc_session_update_needed := 'Y';
                    END IF;
                END IF;
            ELSE
                pc_tran_state_cd := PKG_CONST.TRAN_STATE__DUPLICATE;
                lc_invalid_device_event_cd := 'Y';
            END IF;
        END IF;
    ELSE
        IF lc_invalid_device_event_cd = 'N' AND INSTR(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('MASTER_ID_UPDATE_SALE_TYPES'), 'N') > 0 AND TO_NUMBER_OR_NULL(pv_device_event_cd) < DBADMIN.DATE_TO_MILLIS(SYSDATE + 365) / 1000 THEN
            SELECT P.DEVICE_ID
              INTO ln_device_id
              FROM PSS.POS_PTA PP
              JOIN PSS.POS P ON PP.POS_ID = P.POS_ID
             WHERE PP.POS_PTA_ID = pn_pos_pta_id;
            UPDATE DEVICE.DEVICE_SETTING
               SET DEVICE_SETTING_VALUE = pv_device_event_cd
             WHERE DEVICE_ID = ln_device_id
               AND DEVICE_SETTING_PARAMETER_CD = '60' -- Master Id
               AND TO_NUMBER_OR_NULL(NVL(DEVICE_SETTING_VALUE, '0')) < TO_NUMBER_OR_NULL(pv_device_event_cd);
        END IF;
        IF pc_auth_result_cd IN('Y', 'P', 'V', 'C', 'M') AND pc_auth_hold_used = 'Y' AND (pc_sent_to_device = 'N' OR (pn_auth_amt = 0 AND pn_received_amt > 0)) THEN
            pc_tran_state_cd := 'W'; -- Pending Reversal
        ELSIF pc_auth_result_cd IN('F', 'N') AND pc_auth_hold_used = 'Y' AND pv_card_key IS NOT NULL THEN
            pc_tran_state_cd := 'W'; -- Pending Reversal
        ELSIF pc_auth_result_cd IN('Y', 'P', 'V', 'C', 'M') AND pn_auth_amt = 0 AND pn_received_amt = 0 THEN
            pc_tran_state_cd := 'C'; -- Canceled (Balance Check really)
        ELSIF pc_auth_result_cd IN('Y', 'V', 'C', 'M') THEN
            pc_tran_state_cd := '6'; -- Auth Success
        ELSIF pc_auth_result_cd IN('P') THEN
            pc_tran_state_cd := '0'; -- Auth Success Conditional
        ELSIF pc_auth_result_cd IN('N', 'O', 'R') THEN
            pc_tran_state_cd := '7'; -- Auth Decline
        ELSIF pc_auth_result_cd IN('F') THEN
            pc_tran_state_cd := '5'; -- Auth Failure
        END IF;
    END IF;

    IF pn_tran_id IS NULL OR pc_tran_state_cd = PKG_CONST.TRAN_STATE__DUPLICATE THEN
        SELECT PSS.SEQ_TRAN_ID.NEXTVAL
          INTO pn_tran_id
          FROM DUAL;
        INSERT INTO PSS.TRAN (
            TRAN_ID,
            TRAN_START_TS,
            TRAN_END_TS,
            TRAN_STATE_CD,
            TRAN_DEVICE_TRAN_CD,
            TRAN_RECEIVED_RAW_ACCT_DATA,
            POS_PTA_ID,
            TRAN_GLOBAL_TRANS_CD,
            CONSUMER_ACCT_ID,
            AUTH_GLOBAL_SESSION_CD,
            PAYMENT_SUBTYPE_KEY_ID,
            PAYMENT_SUBTYPE_CLASS,
            CLIENT_PAYMENT_TYPE_CD,
            AUTH_HOLD_USED,
            DEVICE_NAME,
            CARD_TYPE,
            LONGITUDE,
            LATITUDE)
        SELECT
            pn_tran_id, /* TRAN_ID */
            ld_tran_start_ts,  /* TRAN_START_TS */
            ld_tran_start_ts,  /* TRAN_END_TS */
            pc_tran_state_cd,  /* TRAN_STATE_CD */
            pv_device_event_cd,  /* TRAN_DEVICE_TRAN_CD */
            pv_masked_card_number,  /* TRAN_RECEIVED_RAW_ACCT_DATA */
            pn_pos_pta_id, /* POS_PTA_ID */
            DECODE(lc_invalid_device_event_cd, 'Y', pv_global_event_cd || ':' || pn_tran_id, pv_global_event_cd), /* TRAN_GLOBAL_TRANS_CD */
            pn_consumer_acct_id  /* CONSUMER_ACCT_ID */,
            pv_global_session_cd,
            pp.payment_subtype_key_id,
            ps.payment_subtype_class,
            ps.client_payment_type_cd,
            pc_auth_hold_used,
            pv_device_name,
            pv_card_type,
            pn_longitude,
            pn_latitude
        FROM pss.pos_pta pp
        JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
        WHERE pp.pos_pta_id = pn_pos_pta_id;
    END IF;

    IF pn_consumer_acct_id IS NOT NULL THEN
        SELECT MAX(CONSUMER_ACCT_TYPE_ID), MAX(CONSUMER_ACCT_SUB_TYPE_ID)
          INTO ln_consumer_acct_type_id, ln_consumer_acct_sub_type_id
          FROM PSS.CONSUMER_ACCT
         WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id;
        IF ln_consumer_acct_type_id = 3 AND ln_consumer_acct_sub_type_id = 2 THEN
            SELECT TT.OPERATOR_TRANS_TYPE_ID
              INTO ln_override_trans_type_id
              FROM PSS.POS_PTA PP
              JOIN PSS.PAYMENT_SUBTYPE PST ON PP.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
              JOIN REPORT.TRANS_TYPE TT ON PST.TRANS_TYPE_ID = TT.TRANS_TYPE_ID
             WHERE PP.POS_PTA_ID = pn_pos_pta_id;
        END IF;
    END IF;
    SELECT PSS.SEQ_AUTH_ID.NEXTVAL
      INTO pn_auth_id
      FROM DUAL;
    IF pv_denied_reason_name IS NOT NULL THEN
        SELECT DENIED_REASON_ID
          INTO ln_denied_reason_id
          FROM PSS.DENIED_REASON
         WHERE DENIED_REASON_NAME = pv_denied_reason_name;
    END IF;

    IF lc_payment_subtype_class is null THEN
      SELECT ps.PAYMENT_SUBTYPE_CLASS, PSC.CARD_VELOCITY_IND
	  into lc_payment_subtype_class, lc_card_velocity_ind
	  FROM PSS.POS_PTA PP
	  JOIN PSS.POS P ON PP.POS_ID = P.POS_ID
	  JOIN DEVICE.DEVICE D ON P.DEVICE_ID = D.DEVICE_ID
	  JOIN PSS.PAYMENT_SUBTYPE PS ON PP.PAYMENT_SUBTYPE_ID = PS.PAYMENT_SUBTYPE_ID
	  JOIN PSS.PAYMENT_SUBTYPE_CLASS PSC ON PS.PAYMENT_SUBTYPE_CLASS = PSC.PAYMENT_SUBTYPE_CLASS
	  LEFT OUTER JOIN DEVICE.HOST H ON D.DEVICE_ID = H.DEVICE_ID AND H.HOST_PORT_NUM = 0
	  WHERE PP.POS_PTA_ID = pn_pos_pta_id;
    END IF;
    IF lc_payment_subtype_class = 'Coupon' THEN
      SELECT case when upper(pv_authority_ref_cd) ='INSTANT' then 64
      when upper(pv_authority_ref_cd) ='MANUAL' then 65
      else ln_override_trans_type_id END into ln_override_trans_type_id FROM DUAL;
    END IF;	
	
    INSERT INTO PSS.AUTH (
        AUTH_ID,
        TRAN_ID,
        AUTH_STATE_ID,
        AUTH_TYPE_CD,
        ACCT_ENTRY_METHOD_CD,
        AUTH_TS,
        AUTH_RESULT_CD,
        AUTH_RESP_CD,
        AUTH_RESP_DESC,
        AUTH_AUTHORITY_TRAN_CD,
        AUTH_AUTHORITY_REF_CD,
        AUTH_AUTHORITY_TS,
        AUTH_AUTHORITY_MISC_DATA,
        AUTH_AMT,
        AUTH_AMT_APPROVED,
        AUTH_AUTHORITY_AMT_RQST,
        AUTH_AUTHORITY_AMT_RCVD,
        AUTH_BALANCE_AMT,
        TRACE_NUMBER,
        AUTH_ACTION_ID,
        AUTH_ACTION_BITMAP,
        AUTH_HOLD_USED,
        CARD_KEY,
        OVERRIDE_TRANS_TYPE_ID,
        DENIED_REASON_ID,
        MERCHANT_CD,
        TERMINAL_CD,
        POS_PTA_ID)
     VALUES(
        pn_auth_id, /* AUTH_ID */
        pn_tran_id, /* TRAN_ID */
        DECODE(pc_auth_result_cd, 'Y', 2, 'N', 3, 'P', 5, 'O', 7, 'F', 4, 'R', 7, 'V', 2, 'C', 2, 'M', 2), /* AUTH_STATE_ID */
        'N', /* AUTH_TYPE_CD */
        DECODE(pc_entry_method, 'B', 7, 'C', 6, 'M', 2, 'S', 3, 'I', 8, 1), /* ACCT_ENTRY_METHOD_CD */
        pd_auth_ts, /* AUTH_TS */
        pc_auth_result_cd, /* AUTH_RESULT_CD */
        pv_authority_resp_cd, /* AUTH_RESP_CD */
        pv_authority_resp_desc, /* AUTH_RESP_DESC */
        pv_authority_tran_cd, /* AUTH_AUTHORITY_TRAN_CD */
        pv_authority_ref_cd, /* AUTH_AUTHORITY_REF_CD */
        pt_authority_ts, /* AUTH_AUTHORITY_TS */
        pv_authority_misc_data, /* AUTH_AUTHORITY_MISC_DATA */
        NVL(pn_auth_amt / pn_minor_currency_factor, 0), /* AUTH_AMT */
        ln_auth_amt_approved / pn_minor_currency_factor, /* AUTH_AMT_APPROVED */
        pn_requested_amt / pn_minor_currency_factor, /* AUTH_AUTHORITY_AMT_RQST */
        pn_received_amt / pn_minor_currency_factor,  /* AUTH_AUTHORITY_AMT_RCVD */
        pn_balance_amt / pn_minor_currency_factor, /* AUTH_BALANCE_AMT */
        pn_trace_number, /* TRACE_NUMBER */
        pn_auth_action_id,
        pn_auth_action_bitmap,
        pc_auth_hold_used,
        pv_card_key,
        ln_override_trans_type_id,
        ln_denied_reason_id,
        pv_merchant_cd,
        pv_terminal_cd,
        pn_pos_pta_id)
	RETURNING AUTH_STATE_ID INTO ln_auth_state_id;
	
	-- Card Velocity
	IF ln_auth_state_id IN (2, 5) AND pn_consumer_acct_id IS NOT NULL AND lc_card_velocity_ind = 'Y'
		AND PKG_DEVICE_CONFIGURATION.GET_DEVICE_SETTING(ln_device_id, 'CARD_VELOCITY_ENABLED') = 'Y' THEN
		ln_check_min := COALESCE(PKG_DEVICE_CONFIGURATION.GET_DEVICE_SETTING(ln_device_id, 'CARD_VELOCITY_CHECK_MIN'), 3);
		ln_num_auths := COALESCE(PKG_DEVICE_CONFIGURATION.GET_DEVICE_SETTING(ln_device_id, 'CARD_VELOCITY_NUM_AUTHS'), 3);
		ln_blacklist_min := COALESCE(PKG_DEVICE_CONFIGURATION.GET_DEVICE_SETTING(ln_device_id, 'CARD_VELOCITY_BLACKLIST_MIN'), 30);
		ld_sysdate := SYSDATE;
		
		SELECT /*+INDEX(A IDX_AUTH_AUTH_TS_STATE_ID)*/ COUNT(1)
		INTO ln_count
		FROM PSS.TRAN T
		JOIN PSS.AUTH A ON T.TRAN_ID = A.TRAN_ID
		WHERE A.AUTH_TS > ld_sysdate - ln_check_min / 1440 AND A.AUTH_STATE_ID IN (2, 5) AND A.AUTH_TYPE_CD = 'N' AND T.CONSUMER_ACCT_ID = pn_consumer_acct_id;
		
		IF ln_count >= ln_num_auths THEN
			UPDATE pss.pos_pta
			SET decline_until = ld_sysdate + ln_blacklist_min / 1440
			WHERE pos_pta_id IN (
				SELECT pp.pos_pta_id
				FROM pss.pos p
				JOIN pss.pos_pta pp ON p.pos_id = pp.pos_id
				JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
				WHERE p.device_id = ln_device_id
					AND ps.payment_subtype_class = lc_payment_subtype_class
					AND ld_sysdate BETWEEN pp.pos_pta_activation_ts AND COALESCE(pp.pos_pta_deactivation_ts, MAX_DATE)
					AND COALESCE(pp.whitelist, 'N') != 'Y'
					AND COALESCE(pp.decline_until, MIN_DATE) < ld_sysdate
			);
			
			IF SQL%ROWCOUNT > 0 THEN
				lv_error := 'Card Velocity: Blacklisted device for  ' || ln_blacklist_min || ' minutes based on ' || ln_count || ' auths for account ' || pn_consumer_acct_id || ' in the past ' || ln_check_min || ' minutes';
				UPDATE PSS.TRAN
				SET TRAN_INFO = SUBSTR(TRAN_INFO || DECODE(TRAN_INFO, NULL, '', ', ') || lv_error, 1, 1000)
				WHERE TRAN_ID = pn_tran_id AND (TRAN_INFO IS NULL OR INSTR(TRAN_INFO, lv_error) = 0);
			END IF;
		END IF;
	END IF;

    IF (pc_pass_thru = 'N' OR (pc_pass_thru = 'Y' AND pc_auth_result_cd IN('Y', 'P'))) THEN
        UPDATE_AUTH_STAT_BY_DAY(pn_auth_id);
    END IF;

    IF NVL(pn_add_auth_hold_days, 0) > 0 AND pc_auth_result_cd IN('Y', 'P') AND pc_auth_hold_used = 'Y' THEN
        INSERT INTO PSS.CONSUMER_ACCT_AUTH_HOLD (CONSUMER_ACCT_ID, AUTH_ID, TRAN_ID, EXPIRATION_TS)
          VALUES(pn_consumer_acct_id, pn_auth_id, pn_tran_id, SYSDATE + pn_add_auth_hold_days);
    END IF;

    IF lc_imported NOT IN('Y', '-') AND lc_sale_type_cd IN(PKG_CONST.SALE_TYPE__ACTUAL, PKG_CONST.SALE_TYPE__CASH) AND ln_sale_result_id = 0 AND pc_tran_state_cd NOT IN('F', 'G', 'Z') THEN
        pc_tran_import_needed := 'Y';
    ELSE
        pc_tran_import_needed := 'N';
    END IF;

    IF pc_auth_result_cd = 'F' AND pv_authority_resp_cd = 'INVALID_AUTH_AMOUNT' AND pn_auth_amt > 0 THEN
         INSERT INTO ENGINE.MACHINE_CMD_PENDING(MACHINE_ID, DATA_TYPE, COMMAND, EXECUTE_ORDER)
             SELECT *
               FROM (
             SELECT DEVICE_NAME MACHINE_ID, DECODE(DEVICE_TYPE_ID, 13, 'CB', '88') DATA_TYPE,
                    DECODE(DEVICE_TYPE_ID, 13, '00000E' || TO_CHAR(6 + LENGTH(TO_CHAR(AUTH_AMOUNT)), 'FM000X') || '313230303D' || RAWTOHEX(TO_CHAR(AUTH_AMOUNT)) || '0A', '420000006100000002') COMMAND, 20 EXECUTE_ORDER
               FROM (SELECT D.DEVICE_NAME, D.DEVICE_TYPE_ID, NVL(NVL(DBADMIN.TO_NUMBER_OR_NULL(DD.MAX_AUTH_AMOUNT), DBADMIN.TO_NUMBER_OR_NULL(DD.AUTH_AMOUNT) * DECODE(D.DEVICE_TYPE_ID, 13, 1, 100) * 3), 0) MAX_AUTH_AMOUNT,
                    DBADMIN.TO_NUMBER_OR_NULL(DD.AUTH_AMOUNT) AUTH_AMOUNT
               FROM PSS.POS_PTA PP
               JOIN PSS.POS P ON PP.POS_ID = P.POS_ID
               JOIN DEVICE.DEVICE D ON P.DEVICE_ID = D.DEVICE_ID
               JOIN DEVICE.DEVICE_DATA DD ON D.DEVICE_NAME = DD.DEVICE_NAME
              WHERE PP.POS_PTA_ID = pn_pos_pta_id
                AND D.DEVICE_TYPE_ID IN(0,1,13)
                AND DBADMIN.TO_NUMBER_OR_NULL(DD.AUTH_AMOUNT) > 0)
              WHERE MAX_AUTH_AMOUNT < pn_auth_amt
                AND MAX_AUTH_AMOUNT > 0) N
              WHERE NOT EXISTS(
                    SELECT 1
                      FROM ENGINE.MACHINE_CMD_PENDING O
                    WHERE O.MACHINE_ID = N.MACHINE_ID
                      AND O.DATA_TYPE = N.DATA_TYPE
                      AND O.COMMAND = N.COMMAND);
    END IF;

    IF pn_consumer_acct_id IS NOT NULL AND (pc_pass_thru = 'N' OR pc_auth_result_cd IN('Y', 'P', 'V', 'C', 'M')) THEN
        lt_auth_utc_ts := DBADMIN.MILLIS_TO_TIMESTAMP(pn_auth_utc_ms);

        FOR i IN 1..2 LOOP
            UPDATE PSS.CONSUMER_ACCT_DEVICE
            SET USED_COUNT = USED_COUNT + 1,
                LAST_USED_UTC_TS = CASE WHEN LAST_USED_UTC_TS IS NULL OR lt_auth_utc_ts > LAST_USED_UTC_TS THEN lt_auth_utc_ts ELSE LAST_USED_UTC_TS END
            WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id AND DEVICE_NAME = pv_device_name;

            IF SQL%FOUND THEN
                EXIT;
            ELSE
                BEGIN
                    INSERT INTO PSS.CONSUMER_ACCT_DEVICE(CONSUMER_ACCT_ID, DEVICE_NAME, USED_COUNT, LAST_USED_UTC_TS)
                    VALUES(pn_consumer_acct_id, pv_device_name, 1, lt_auth_utc_ts);
                    EXIT;
                EXCEPTION
                    WHEN DUP_VAL_ON_INDEX THEN
                        NULL;
                END;
            END IF;
        END LOOP;
    END IF;
    
    IF lc_payment_subtype_class = 'Promotion' THEN
        PROCESS_PROMO_TRAN(pn_tran_id);
    END IF;

    IF pc_client_payment_type_cd IN (PKG_CONST.CLNT_PMNT_TYPE__ISIS_CREDIT, PKG_CONST.CLNT_PMNT_TYPE__ISIS_SPECIAL)
        AND lc_previous_tran_state_cd = PKG_CONST.TRAN_STATE__SALE_NO_AUTH THEN
        IF pn_consumer_acct_id > 0 THEN
        PROCESS_ISIS_TRAN(pn_tran_id);
        END IF;

        IF pn_sale_amount > 0 AND pc_pass_thru = 'N' AND ln_orig_sale_amount = pn_sale_amount THEN
            -- Softcard Discount promotion
            IF REGEXP_LIKE(lv_device_serial_cd, DBADMIN.PKG_GLOBAL.GET_APP_SETTING('SOFTCARD_DISCOUNT_PROMO_SERIAL_CD_REGEX')) THEN
                ln_discount_percent := DBADMIN.TO_NUMBER_OR_NULL(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('SOFTCARD_DISCOUNT_PROMO_PERCENT'));

                IF ln_discount_percent IS NOT NULL THEN
                    ln_discount_amount := ROUND(pn_sale_amount * ln_discount_percent);
                    IF ln_discount_amount > 0 THEN
                        IF ln_discount_amount > pn_sale_amount THEN
                            ln_discount_amount := pn_sale_amount;
                        END IF;
                        pn_sale_amount := pn_sale_amount - ln_discount_amount;
                        IF ln_base_host_id IS NULL THEN
                            pkg_device_configuration.sp_create_default_hosts(ln_device_id, ln_new_host_count, ln_result_cd, lv_error_message);
                            IF ln_result_cd != PKG_CONST.RESULT__SUCCESS THEN
                  RAISE_APPLICATION_ERROR(-20201, 'Could not create base host for device ' || ln_device_id || ': ' || lv_error_message);
                            END IF;
                            SELECT H.HOST_ID
                            INTO ln_base_host_id
                            FROM DEVICE.HOST H
                            WHERE H.DEVICE_ID = ln_device_id
                            AND H.HOST_PORT_NUM = 0;
                        END IF;

                        -- create Softcard Discount line item
                        INSERT INTO PSS.TRAN_LINE_ITEM (
                            TRAN_ID,
                            HOST_ID,
                            TRAN_LINE_ITEM_TYPE_ID,
                            TRAN_LINE_ITEM_AMOUNT,
                            TRAN_LINE_ITEM_QUANTITY,
                            TRAN_LINE_ITEM_DESC,
                            TRAN_LINE_ITEM_BATCH_TYPE_CD,
                            TRAN_LINE_ITEM_TAX)
                        SELECT
                            pn_tran_id,
                            ln_base_host_id,
                            tran_line_item_type_id,
                            -ln_discount_amount / pn_minor_currency_factor,
                            1,
                            tran_line_item_type_desc || ' ' || ln_discount_percent * 100 || '%',
                            PKG_CONST.TRAN_BATCH_TYPE__ACTUAL,
                            0
                        FROM pss.tran_line_item_type
                        WHERE tran_line_item_type_id = 208;

                        UPDATE PSS.SALE
                        SET SALE_AMOUNT = pn_sale_amount / pn_minor_currency_factor
                        WHERE TRAN_ID = pn_tran_id;
                    END IF;
                END IF;
            END IF;
        END IF;
    END IF;
    -- don't apply multiple discounts
    IF pn_sale_amount > 0  AND ln_orig_sale_amount = pn_sale_amount AND lc_payment_subtype_class != 'Promotion' THEN
        -- apply loyalty discount or purchase discount
        CREATE_DISCOUNT_TRAN_LINE_ITEM (pn_tran_id,
        pn_consumer_acct_id,
        pn_pos_pta_id,
        pn_minor_currency_factor,
        ld_tran_start_ts,
        ln_base_host_id,
        ln_device_id,
        pn_sale_amount);
        IF ln_orig_sale_amount != pn_sale_amount THEN
          UPDATE pss.sale
          SET sale_amount = pn_sale_amount / pn_minor_currency_factor
          WHERE tran_id = pn_tran_id;
        END IF;
    END IF;

    IF REGEXP_LIKE(pv_authority_misc_data, 'ConsumerID=|OfferID=') THEN
        UPDATE REPORT.TRANS
        SET DESCRIPTION = pv_authority_misc_data
        WHERE MACHINE_TRANS_NO = pv_global_event_cd AND SOURCE_SYSTEM_CD = 'PSS' AND DESCRIPTION IS NULL;
    END IF;
END;

-- R37 and above
PROCEDURE SP_INSERT_AUTH_STATS(
   pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
   pn_auth_id PSS.AUTH.AUTH_ID%TYPE,
   pn_request_time NUMBER,
   pn_applayer_start_time NUMBER,
   pn_authority_start_time NUMBER,
   pn_authority_end_time NUMBER,
   pn_applayer_end_time NUMBER,
   pn_response_time NUMBER
) IS
BEGIN
    INSERT INTO PSS.TRAN_STAT(TRAN_ID, AUTH_ID, TRAN_STAT_TYPE_ID, TRAN_STAT_VALUE)
      SELECT pn_tran_id, pn_auth_id, TRAN_STAT_TYPE_ID, TRAN_STAT_VALUE FROM (
        SELECT 2 /* live auth "POSM" time*/ TRAN_STAT_TYPE_ID, (pn_applayer_end_time - pn_applayer_start_time) / 1000 TRAN_STAT_VALUE FROM DUAL WHERE pn_applayer_start_time IS NOT NULL AND pn_applayer_end_time IS NOT NULL
        UNION ALL SELECT 4 /* live auth network time*/, (pn_response_time - pn_request_time) / 1000 FROM DUAL WHERE pn_request_time IS NOT NULL AND pn_response_time IS NOT NULL
        UNION ALL SELECT 1 /* live auth gateway time*/, (pn_authority_end_time - pn_authority_start_time) / 1000 FROM DUAL WHERE pn_authority_start_time IS NOT NULL AND pn_authority_end_time IS NOT NULL) a
     WHERE NOT EXISTS(SELECT 1 FROM PSS.TRAN_STAT TS WHERE pn_tran_id = TS.TRAN_ID AND NVL(pn_auth_id, 0) = NVL(TS.AUTH_ID, 0) AND A.TRAN_STAT_TYPE_ID = TS.TRAN_STAT_TYPE_ID);
END;

-- R49+ Signature
PROCEDURE PERMIT_CONSUMER_ACCT(
    pn_pos_pta_id PSS.POS_PTA.POS_PTA_ID%TYPE,
    pn_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
    pt_auth_ts PSS.CONSUMER_ACCT.CONSUMER_ACCT_ACTIVATION_TS%TYPE,
    pl_consumer_acct_type_ids NUMBER_TABLE,
    pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
    pn_consumer_id OUT PSS.CONSUMER_ACCT.CONSUMER_ID%TYPE,
    pn_consumer_acct_type_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_TYPE_ID%TYPE,
    pv_consumer_acct_type_label OUT PSS.CONSUMER_ACCT_TYPE.CONSUMER_ACCT_TYPE_LABEL%TYPE,
    pb_consumer_acct_raw_hash OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_RAW_HASH%TYPE,
    pb_consumer_acct_raw_salt OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_RAW_SALT%TYPE,
    pb_security_cd_hash OUT PSS.CONSUMER_ACCT.SECURITY_CD_HASH%TYPE,
    pb_security_cd_salt OUT PSS.CONSUMER_ACCT.SECURITY_CD_SALT%TYPE,
    pn_action_id OUT PSS.PERMISSION_ACTION.ACTION_ID%TYPE,
    pn_action_code OUT NUMBER,
    pn_action_bitmap OUT NUMBER,
    pv_action_attributes OUT VARCHAR2)
IS
    lc_store_action CHAR(1);
    lv_device_name DEVICE.DEVICE_NAME%TYPE;
    ln_device_id DEVICE.DEVICE_ID%TYPE;
    ln_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE;
BEGIN
    SELECT CONSUMER_ACCT_ID, CONSUMER_ID, CONSUMER_ACCT_TYPE_ID, CONSUMER_ACCT_TYPE_LABEL, CONSUMER_ACCT_RAW_HASH, CONSUMER_ACCT_RAW_SALT, SECURITY_CD_HASH, SECURITY_CD_SALT, DEVICE_ID, DEVICE_TYPE_ID, DEVICE_NAME
      INTO pn_consumer_acct_id, pn_consumer_id, pn_consumer_acct_type_id, pv_consumer_acct_type_label, pb_consumer_acct_raw_hash, pb_consumer_acct_raw_salt, pb_security_cd_hash, pb_security_cd_salt, ln_device_id, ln_device_type_id, lv_device_name
      FROM (
         SELECT CA.CONSUMER_ACCT_ID, CA.CONSUMER_ID, CA.CONSUMER_ACCT_TYPE_ID, CAT.CONSUMER_ACCT_TYPE_LABEL, CA.CONSUMER_ACCT_RAW_HASH, CA.CONSUMER_ACCT_RAW_SALT, CA.SECURITY_CD_HASH, CA.SECURITY_CD_SALT, D.DEVICE_ID, D.DEVICE_TYPE_ID, D.DEVICE_NAME, VLH.ANCESTOR_LOCATION_ID, MAX(CA.CONSUMER_ACCT_ISSUE_NUM) MAX_ISSUE_NUM, VLH.DEPTH
           FROM PSS.POS_PTA PTA
           JOIN PSS.POS POS ON PTA.POS_ID = POS.POS_ID
           JOIN DEVICE.DEVICE D ON D.DEVICE_ID = POS.DEVICE_ID
           JOIN LOCATION.VW_LOCATION_HIERARCHY VLH ON VLH.DESCENDENT_LOCATION_ID = POS.LOCATION_ID
           JOIN PSS.CONSUMER_ACCT CA ON VLH.ANCESTOR_LOCATION_ID = CA.LOCATION_ID
           JOIN PSS.CONSUMER_ACCT_TYPE CAT ON CAT.CONSUMER_ACCT_TYPE_ID = CA.CONSUMER_ACCT_TYPE_ID
           JOIN PSS.CONSUMER_ACCT_BASE CAB ON CAB.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
          WHERE PTA.POS_PTA_ID = pn_pos_pta_id
            AND CAB.GLOBAL_ACCOUNT_ID = pn_global_account_id
            AND CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y'
            AND NVL(CA.CONSUMER_ACCT_ACTIVATION_TS, MIN_DATE) <= pt_auth_ts
            AND NVL(CA.CONSUMER_ACCT_DEACTIVATION_TS, MAX_DATE) > pt_auth_ts
            AND CA.CONSUMER_ACCT_TYPE_ID MEMBER OF pl_consumer_acct_type_ids
            GROUP BY CA.CONSUMER_ACCT_ID, CA.CONSUMER_ID, CA.CONSUMER_ACCT_TYPE_ID, CAT.CONSUMER_ACCT_TYPE_LABEL, CA.CONSUMER_ACCT_RAW_HASH, CA.CONSUMER_ACCT_RAW_SALT, CA.SECURITY_CD_HASH, CA.SECURITY_CD_SALT, D.DEVICE_ID, D.DEVICE_TYPE_ID, D.DEVICE_NAME, VLH.ANCESTOR_LOCATION_ID, VLH.DEPTH
            ORDER BY VLH.DEPTH, MAX_ISSUE_NUM DESC    /* DEPTH IS ASCENDING, AS IT IS THE DIFFERENCE BETWEEN LOCATION AND ANCESTOR */
    ) WHERE ROWNUM = 1;

    SELECT A.ACTION_ID, DTA.DEVICE_TYPE_ACTION_CD, SUM(POWER(2, AP.PROTOCOL_BIT_INDEX)) PROTOCOL_BITMAP,
           DECODE(A.ACTION_PARAM_TYPE_CD, 'A', CASE WHEN O.REPEAT_FLAG = 'Y' THEN 'repeated=true' || CASE WHEN COUNT(AP.ACTION_PARAM_CD) = 0 THEN CHR(10) END END || DBADMIN.ADHERE(CAST(COLLECT(AP.ACTION_PARAM_CD || '=true') AS VARCHAR2_TABLE), CHR(10))) ACTION_ATTRIBUTES,
           DECODE(A.ACTION_CLEAR_PARAMETER_CD, NULL, 'N', 'Y') STORE_LAST_ACTION
      INTO pn_action_id, pn_action_code, pn_action_bitmap, pv_action_attributes, lc_store_action
      FROM (SELECT * FROM (
         SELECT CAP.PERMISSION_ACTION_ID, DTA.DEVICE_TYPE_ACTION_CD PROTOCOL_ACTION_CD,
                DTA.ACTION_ID,
                CASE WHEN LDA.DEVICE_ACTION_UTC_TS IS NOT NULL
                          AND CURRENT_TIMESTAMP < LDA.DEVICE_ACTION_UTC_TS
                          + NUMTODSINTERVAL(COALESCE(TO_NUMBER_OR_NULL(DS_T.DEVICE_SETTING_VALUE),
                          TO_NUMBER_OR_NULL(cts.CONFIG_TEMPLATE_SETTING_VALUE), 3600), 'SECOND') THEN 'Y'
                     ELSE 'N' END REPEAT_FLAG
           FROM PSS.CONSUMER_ACCT_PERMISSION CAP
           JOIN PSS.PERMISSION_ACTION PA ON CAP.PERMISSION_ACTION_ID = PA.PERMISSION_ACTION_ID
           JOIN DEVICE.DEVICE_TYPE_ACTION DTA ON DTA.ACTION_ID = PA.ACTION_ID
           JOIN DEVICE.DEVICE_TYPE DT ON DTA.DEVICE_TYPE_ID = DT.DEVICE_TYPE_ID
           LEFT OUTER JOIN PSS.LAST_DEVICE_ACTION LDA
             ON LDA.DEVICE_NAME = lv_device_name
            AND CAP.CONSUMER_ACCT_ID = LDA.CONSUMER_ACCT_ID
            AND PA.ACTION_ID = LDA.DEVICE_ACTION_ID
           JOIN DEVICE.ACTION A ON PA.ACTION_ID = A.ACTION_ID
           LEFT OUTER JOIN DEVICE.DEVICE_SETTING DS_T ON ln_device_id = DS_T.DEVICE_ID AND DS_T.DEVICE_SETTING_PARAMETER_CD = A.ACTION_CLEAR_PARAMETER_CD
           LEFT OUTER JOIN DEVICE.DEVICE_SETTING ds_v ON ln_device_id = ds_v.DEVICE_ID AND ds_v.DEVICE_SETTING_PARAMETER_CD = 'Property List Version'
           LEFT OUTER JOIN DEVICE.CONFIG_TEMPLATE ct
             ON ct.CONFIG_TEMPLATE_NAME = DECODE(DBADMIN.PKG_UTL.COMPARE(DT.DEVICE_TYPE_ID, 13),
                -1, DT.DEFAULT_CONFIG_TEMPLATE_NAME, DT.DEFAULT_CONFIG_TEMPLATE_NAME || ds_v.DEVICE_SETTING_VALUE)
           LEFT OUTER JOIN DEVICE.CONFIG_TEMPLATE_SETTING cts
             ON cts.CONFIG_TEMPLATE_ID = ct.CONFIG_TEMPLATE_ID
            AND cts.DEVICE_SETTING_PARAMETER_CD = A.ACTION_CLEAR_PARAMETER_CD
          WHERE CAP.CONSUMER_ACCT_ID = pn_consumer_acct_id
            AND DTA.DEVICE_TYPE_ID = ln_device_type_id
          ORDER BY CAP.CONSUMER_ACCT_PERMISSION_ORDER
      ) WHERE ROWNUM = 1) O
      LEFT OUTER JOIN (PSS.PERMISSION_ACTION_PARAM PAP
      JOIN DEVICE.ACTION_PARAM AP ON PAP.ACTION_PARAM_ID = AP.ACTION_PARAM_ID)
        ON O.PERMISSION_ACTION_ID = PAP.PERMISSION_ACTION_ID
      JOIN DEVICE.ACTION A ON A.ACTION_ID = CASE WHEN O.REPEAT_FLAG = 'Y' AND A.ACTION_PARAM_TYPE_CD != 'A' THEN 10 ELSE O.ACTION_ID END
      JOIN DEVICE.DEVICE_TYPE_ACTION DTA ON DTA.ACTION_ID = A.ACTION_ID
     WHERE DTA.DEVICE_TYPE_ID = ln_device_type_id
      GROUP BY A.ACTION_ID, DTA.DEVICE_TYPE_ACTION_CD, A.ACTION_CLEAR_PARAMETER_CD, A.ACTION_PARAM_TYPE_CD, O.REPEAT_FLAG;
    IF lc_store_action = 'Y' THEN
        MERGE INTO PSS.LAST_DEVICE_ACTION O
         USING (
              SELECT lv_device_name DEVICE_NAME,
                     pn_consumer_acct_id CONSUMER_ACCT_ID,
                     pn_action_id DEVICE_ACTION_ID,
                     CURRENT_TIMESTAMP DEVICE_ACTION_UTC_TS
                FROM DUAL) N
              ON (O.DEVICE_NAME = N.DEVICE_NAME)
              WHEN MATCHED THEN
               UPDATE
                  SET O.CONSUMER_ACCT_ID = N.CONSUMER_ACCT_ID,
                      O.DEVICE_ACTION_ID = N.DEVICE_ACTION_ID,
                      O.DEVICE_ACTION_UTC_TS = N.DEVICE_ACTION_UTC_TS
              WHEN NOT MATCHED THEN
               INSERT (O.DEVICE_NAME,
                       O.CONSUMER_ACCT_ID,
                       O.DEVICE_ACTION_ID,
                       O.DEVICE_ACTION_UTC_TS)
                VALUES(N.DEVICE_NAME,
                       N.CONSUMER_ACCT_ID,
                       N.DEVICE_ACTION_ID,
                       N.DEVICE_ACTION_UTC_TS
                );
    END IF;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN;
END;

-- R48 Signature
PROCEDURE PERMIT_CONSUMER_ACCT(
    pn_pos_pta_id PSS.POS_PTA.POS_PTA_ID%TYPE,
    pn_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
    pt_auth_ts PSS.CONSUMER_ACCT.CONSUMER_ACCT_ACTIVATION_TS%TYPE,
    pl_consumer_acct_type_ids NUMBER_TABLE,
    pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
    pn_consumer_id OUT PSS.CONSUMER_ACCT.CONSUMER_ID%TYPE,
    pn_consumer_acct_type_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_TYPE_ID%TYPE,
    pv_consumer_acct_type_label OUT PSS.CONSUMER_ACCT_TYPE.CONSUMER_ACCT_TYPE_LABEL%TYPE,
    pb_consumer_acct_raw_hash OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_RAW_HASH%TYPE,
    pb_consumer_acct_raw_salt OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_RAW_SALT%TYPE,
    pb_security_cd_hash OUT PSS.CONSUMER_ACCT.SECURITY_CD_HASH%TYPE,
    pb_security_cd_salt OUT PSS.CONSUMER_ACCT.SECURITY_CD_SALT%TYPE,
    pn_action_id OUT PSS.PERMISSION_ACTION.ACTION_ID%TYPE,
    pn_action_code OUT NUMBER,
    pn_action_bitmap OUT NUMBER)
IS
    lv_action_attributes VARCHAR2(4000);
BEGIN
    PERMIT_CONSUMER_ACCT(
      pn_pos_pta_id,
      pn_global_account_id,
      pt_auth_ts,
      pl_consumer_acct_type_ids,
      pn_consumer_acct_id,
      pn_consumer_id,
      pn_consumer_acct_type_id,
      pv_consumer_acct_type_label,
      pb_consumer_acct_raw_hash,
      pb_consumer_acct_raw_salt,
      pb_security_cd_hash,
      pb_security_cd_salt,
      pn_action_id,
      pn_action_code,
      pn_action_bitmap,
      lv_action_attributes);
END;

    -- R37+ signature
    PROCEDURE SP_CREATE_LOCAL_AUTH_SALE(
       pc_global_event_cd_prefix IN CHAR,
       pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
       pn_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE,
       pv_device_tran_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE,
       pn_sale_utc_ts_ms NUMBER,
       pn_sale_utc_offset_min IN pss.sale.sale_utc_offset_min%TYPE,
       pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
       pc_entry_method CHAR,
       pc_payment_type CHAR,
       pv_track_data PSS.TRAN.TRAN_RECEIVED_RAW_ACCT_DATA%TYPE,
       pn_consumer_acct_id PSS.TRAN.CONSUMER_ACCT_ID%TYPE,
       pd_auth_ts PSS.AUTH.AUTH_TS%TYPE,
       pn_trace_number PSS.AUTH.TRACE_NUMBER%TYPE,
       pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
       pn_auth_amt PSS.AUTH.AUTH_AMT%TYPE,
       pn_device_batch_id IN pss.sale.device_batch_id%TYPE,
       pc_sale_type_cd IN pss.sale.sale_type_cd%TYPE,
       pv_tran_device_result_type_cd IN pss.tran.tran_device_result_type_cd%TYPE,
       pn_sale_result_id IN pss.sale.sale_result_id%TYPE,
       pv_hash_type_cd IN pss.sale.hash_type_cd%TYPE,
       pv_tran_line_item_hash IN pss.sale.tran_line_item_hash%TYPE,
       pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
       pn_result_cd OUT NUMBER,
       pv_error_message OUT VARCHAR2
    ) IS
       ln_auth_id PSS.AUTH.AUTH_ID%TYPE;
       lv_global_trans_cd pss.tran.tran_global_trans_cd%TYPE;
       lv_global_trans_cd_app_layer pss.tran.tran_global_trans_cd%TYPE;
       lv_global_trans_cd_legacy pss.tran.tran_global_trans_cd%TYPE;
       ld_tran_upload_ts pss.tran.tran_upload_ts%TYPE;
       ln_tran_exists NUMBER := PKG_CONST.BOOLEAN__FALSE;
       ln_tli_hash_match NUMBER;
       ld_current_ts DATE := SYSDATE;
       ld_tran_start_ts pss.tran.tran_start_ts%TYPE;
       lt_sale_start_utc_ts pss.sale.sale_start_utc_ts%TYPE;
       lv_last_lock_utc_ts VARCHAR2(128);
       ln_insert_tran NUMBER := PKG_CONST.BOOLEAN__FALSE;
       lv_tran_state_cd pss.tran.tran_state_cd%TYPE := PKG_CONST.TRAN_STATE__COMPLETE_ERROR;
    BEGIN
        IF pc_global_event_cd_prefix NOT IN (PKG_CONST.EVENT_CODE_PREFIX__APP_LAYER,
            PKG_CONST.EVENT_CODE_PREFIX__LEGACY) THEN
            pn_result_cd := PKG_CONST.RESULT__INVALID_PARAMETER;
            pv_error_message := 'Invalid pc_global_event_cd_prefix: ' || pc_global_event_cd_prefix;
            RETURN;
        END IF;

        lv_global_trans_cd := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(pc_global_event_cd_prefix, pv_device_name, pv_device_tran_cd);
        lv_global_trans_cd_app_layer := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(PKG_CONST.EVENT_CODE_PREFIX__APP_LAYER, pv_device_name, pv_device_tran_cd);
        lv_global_trans_cd_legacy := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(PKG_CONST.EVENT_CODE_PREFIX__LEGACY, pv_device_name, pv_device_tran_cd);
        ld_tran_start_ts := CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_sale_utc_ts_ms + pn_sale_utc_offset_min * 60 * 1000) AS DATE);
        lt_sale_start_utc_ts := CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_sale_utc_ts_ms) AS DATE);
        lv_last_lock_utc_ts := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', pv_device_name || ':' || pv_device_tran_cd);
        BEGIN
            SELECT tran_id, tran_upload_ts, PKG_CONST.BOOLEAN__TRUE, tli_hash_match
            INTO pn_tran_id, ld_tran_upload_ts, ln_tran_exists, ln_tli_hash_match
            FROM
            (
                SELECT /*+ INDEX(t IDX_DEVICE_TRAN_CD) */ t.tran_id, t.tran_upload_ts,
                    CASE WHEN s.hash_type_cd = pv_hash_type_cd
                        AND s.tran_line_item_hash = pv_tran_line_item_hash
                        AND s.sale_type_cd = pc_sale_type_cd THEN PKG_CONST.BOOLEAN__TRUE
                    ELSE PKG_CONST.BOOLEAN__FALSE END AS tli_hash_match
                FROM pss.tran t
                LEFT OUTER JOIN pss.sale s ON t.tran_id = s.tran_id
                WHERE t.tran_device_tran_cd = pv_device_tran_cd AND (
                    t.tran_global_trans_cd IN (lv_global_trans_cd_app_layer, lv_global_trans_cd_legacy)
                    OR t.tran_global_trans_cd LIKE lv_global_trans_cd_app_layer || ':%'
                    OR t.tran_global_trans_cd LIKE lv_global_trans_cd_legacy || ':%'
                )
                ORDER BY CASE WHEN t.tran_global_trans_cd IN (lv_global_trans_cd_app_layer, lv_global_trans_cd_legacy) THEN 1 ELSE 2 END,
                    CASE WHEN s.sale_type_cd = pc_sale_type_cd THEN 1 ELSE 2 END,
                    tli_hash_match DESC, t.tran_start_ts, t.created_ts
            )
            WHERE ROWNUM = 1;

        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                ln_insert_tran := PKG_CONST.BOOLEAN__TRUE;
        END;

        IF ln_tran_exists = PKG_CONST.BOOLEAN__TRUE AND ld_tran_upload_ts IS NOT NULL THEN
            IF ln_tli_hash_match = PKG_CONST.BOOLEAN__TRUE THEN
                UPDATE pss.sale
                SET duplicate_count = duplicate_count + 1
                WHERE tran_id = pn_tran_id;

                pn_result_cd := PKG_CONST.RESULT__DUPLICATE;
                pv_error_message := 'Duplicate sale, original tran_id: ' || pn_tran_id;
                pn_tran_id := 0;
                RETURN;
            END IF;

            ln_insert_tran := PKG_CONST.BOOLEAN__TRUE;
        END IF;

        IF ln_insert_tran = PKG_CONST.BOOLEAN__TRUE THEN
            IF pc_sale_type_cd = PKG_CONST.SALE_TYPE__CASH THEN
                lv_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE;
            ELSIF pv_tran_device_result_type_cd IN (PKG_CONST.TRAN_DEV_RES__CANCELLED,
                    PKG_CONST.TRAN_DEV_RES__AUTH_FAILURE,
                    PKG_CONST.TRAN_DEV_RES__FAILURE,
                    PKG_CONST.TRAN_DEV_RES__TIMEOUT) OR pn_auth_amt <= 0 THEN
                lv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED;
            END IF;

            SELECT PSS.SEQ_TRAN_ID.NEXTVAL
            INTO pn_tran_id
            FROM DUAL;

            IF ln_tran_exists = PKG_CONST.BOOLEAN__TRUE THEN
                lv_global_trans_cd := lv_global_trans_cd || ':' || pn_tran_id;
            END IF;

            INSERT INTO PSS.TRAN (
                TRAN_ID,
                TRAN_START_TS,
                TRAN_END_TS,
                TRAN_UPLOAD_TS,
                TRAN_STATE_CD,
                TRAN_DEVICE_TRAN_CD,
                TRAN_RECEIVED_RAW_ACCT_DATA,
                POS_PTA_ID,
                TRAN_GLOBAL_TRANS_CD,
                CONSUMER_ACCT_ID,
                TRAN_DEVICE_RESULT_TYPE_CD,
                PAYMENT_SUBTYPE_KEY_ID,
                PAYMENT_SUBTYPE_CLASS,
                CLIENT_PAYMENT_TYPE_CD,
                DEVICE_NAME)
            SELECT
                pn_tran_id, /* TRAN_ID */
                ld_tran_start_ts,  /* TRAN_START_TS */
                ld_tran_start_ts, /* TRAN_END_TS */
                ld_current_ts,
                lv_tran_state_cd,  /* TRAN_STATE_CD */
                pv_device_tran_cd,  /* TRAN_DEVICE_TRAN_CD */
                pv_track_data,  /* TRAN_RECEIVED_RAW_ACCT_DATA */
                pn_pos_pta_id, /* POS_PTA_ID */
                lv_global_trans_cd, /* TRAN_GLOBAL_TRANS_CD */
                pn_consumer_acct_id,  /* CONSUMER_ACCT_ID */
                pv_tran_device_result_type_cd,
                pp.payment_subtype_key_id,
                ps.payment_subtype_class,
                ps.client_payment_type_cd,
                pv_device_name
            FROM pss.pos_pta pp
            JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
            WHERE pp.pos_pta_id = pn_pos_pta_id;

            IF pc_sale_type_cd != PKG_CONST.SALE_TYPE__CASH THEN
            SELECT PSS.SEQ_AUTH_ID.NEXTVAL
              INTO ln_auth_id
              FROM DUAL;
            INSERT INTO PSS.AUTH (
                AUTH_ID,
                TRAN_ID,
                AUTH_STATE_ID,
                AUTH_TYPE_CD,
                AUTH_PARSED_ACCT_DATA,
                ACCT_ENTRY_METHOD_CD,
                AUTH_TS,
                AUTH_RESULT_CD,
                AUTH_RESP_CD,
                AUTH_RESP_DESC,
                AUTH_AUTHORITY_TS,
                AUTH_AMT,
                AUTH_AUTHORITY_AMT_RQST,
                TRACE_NUMBER)
             VALUES(
                ln_auth_id, /* AUTH_ID */
                pn_tran_id, /* TRAN_ID */
                DECODE(pc_auth_result_cd, 'Y', 2, 'N', 3, 'P', 5, 'O', 7, 'F', 4, 'R', 7, 'V', 2, 'C', 2, 'M', 2), /* AUTH_STATE_ID */
                'L', /* AUTH_TYPE_CD */
                pv_track_data, /* AUTH_PARSED_ACCT_DATA */
                DECODE(pc_entry_method, 'B', 7, 'C', 6, 'M', 2, 'S', 3, 'I', 8, 1), /* ACCT_ENTRY_METHOD_CD */
                pd_auth_ts, /* AUTH_TS */
                pc_auth_result_cd, /* AUTH_RESULT_CD */
                'LOCAL', /* AUTH_RESP_CD */
                'Local authorization not accepted', /* AUTH_RESP_DESC */
                pd_auth_ts, /* AUTH_AUTHORITY_TS */
                NVL(pn_auth_amt / pn_minor_currency_factor, 0), /* AUTH_AMT */
                pn_auth_amt / pn_minor_currency_factor, /* AUTH_AUTHORITY_AMT_RQST */
                pn_trace_number /* TRACE_NUMBER */
                );
            END IF;

            INSERT INTO pss.sale (
                tran_id,
                device_batch_id,
                sale_type_cd,
                sale_start_utc_ts,
                sale_end_utc_ts,
                sale_utc_offset_min,
                sale_result_id,
                sale_amount,
                receipt_result_cd,
                hash_type_cd,
                tran_line_item_hash
            ) VALUES (
                pn_tran_id,
                pn_device_batch_id,
                pc_sale_type_cd,
                lt_sale_start_utc_ts,
                lt_sale_start_utc_ts,
                pn_sale_utc_offset_min,
                pn_sale_result_id,
                pn_auth_amt / pn_minor_currency_factor,
                'U',
                pv_hash_type_cd,
                pv_tran_line_item_hash
            );
        END IF;

        pn_result_cd := PKG_CONST.RESULT__SUCCESS;
        pv_error_message := PKG_CONST.ERROR__NO_ERROR;
    END;

    PROCEDURE SETUP_REPLENISH_CONSUMER_ACCT(
       pn_replenish_id PSS.CONSUMER_ACCT_PEND_REPLENISH.CONSUMER_ACCT_REPLENISH_ID%TYPE,
       pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
       lc_always_auth_flag CHAR,
       pv_replenish_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
       pn_balance PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
       pn_replenish_threshhold PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_THRESHHOLD%TYPE,
       pn_replenish_amount IN OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_AMOUNT%TYPE,
       pc_replenish_flag OUT VARCHAR2,
       pn_replenish_next_master_id OUT NUMBER,
       pv_replenish_device_name OUT DEVICE.DEVICE_NAME%TYPE,
       pn_consumer_acct_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_TYPE_ID%TYPE DEFAULT NULL)
    IS
       ln_balance PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE;
       ln_pending_amount PSS.CONSUMER_ACCT_PEND_REPLENISH.AMOUNT%TYPE;
       ln_auth_hold_total PSS.AUTH.AUTH_AMT_APPROVED%TYPE;
    BEGIN
        SELECT NVL(SUM(A.AUTH_AMT_APPROVED), 0)
          INTO ln_auth_hold_total
          FROM PSS.CONSUMER_ACCT_AUTH_HOLD CAAH
          JOIN PSS.AUTH A ON CAAH.AUTH_ID = A.AUTH_ID
         WHERE CAAH.CONSUMER_ACCT_ID = pn_consumer_acct_id
           AND CAAH.EXPIRATION_TS > SYSDATE
           AND CAAH.CLEARED_YN_FLAG = 'N';
        SELECT NVL(SUM(AMOUNT), 0)
          INTO ln_pending_amount
          FROM PSS.CONSUMER_ACCT_PEND_REPLENISH
         WHERE CONSUMER_ACCT_REPLENISH_ID = pn_replenish_id
           AND EXPIRATION_TS > SYSDATE
           AND (SUBMITTED_FLAG = 'Y' OR (SUBMITTED_FLAG = '?' AND CREATED_UTC_TS > SYS_EXTRACT_UTC(SYSTIMESTAMP) - (1/24/60)));
        ln_balance := pn_balance + ln_pending_amount - ln_auth_hold_total;
        IF lc_always_auth_flag = 'Y' OR pn_replenish_threshhold IS NULL OR ln_balance < pn_replenish_threshhold THEN
            IF pn_replenish_threshhold IS NOT NULL THEN
                IF pn_replenish_amount + ln_balance < pn_replenish_threshhold THEN
                    pn_replenish_amount := pn_replenish_threshhold - ln_balance;
                ELSIF ln_balance >= pn_replenish_threshhold AND lc_always_auth_flag = 'Y' THEN
                    pn_replenish_amount := 0;
                END IF;
            ELSIF lc_always_auth_flag = 'Y' AND pn_replenish_threshhold IS NULL THEN
              pn_replenish_amount := 0;
            END IF;
            IF pn_consumer_acct_type_id is not null and pn_consumer_acct_type_id = 6 and lc_always_auth_flag = 'Y' THEN
              pn_replenish_amount := 0;
            END IF;
            PKG_DEVICE_CONFIGURATION.NEXT_MASTER_ID_BY_SERIAL(pv_replenish_device_serial_cd, pv_replenish_device_name, pn_replenish_next_master_id);
            INSERT INTO PSS.CONSUMER_ACCT_PEND_REPLENISH(CONSUMER_ACCT_REPLENISH_ID, DEVICE_NAME, DEVICE_TRAN_CD, AMOUNT, EXPIRATION_TS)
                VALUES(pn_replenish_id, pv_replenish_device_name, pn_replenish_next_master_id, pn_replenish_amount, SYSDATE + 7);

            pc_replenish_flag := 'Y';
        ELSE
            pc_replenish_flag := 'N';
        END IF;
    END;

    -- R37 and above
    PROCEDURE DEBIT_CONSUMER_ACCT(
       pn_amount PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
       pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
       pc_redelivery_flag CHAR,
       pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
       pc_auth_result_cd OUT VARCHAR2,
       pn_debitted_amount OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
       pn_new_balance OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
       pc_auto_replenish_flag OUT VARCHAR2,
       pn_used_cash_back_balance OUT PSS.CONSUMER_ACCT.TOWARD_CASH_BACK_BALANCE%TYPE,
       pn_cash_back_amount OUT PSS.SALE.SALE_AMOUNT%TYPE)
    IS
       ln_auth_id PSS.AUTH.AUTH_ID%TYPE;
       ln_auth_amount PSS.AUTH.AUTH_AMT_APPROVED%TYPE;
       ln_auth_hold_used PSS.AUTH.AUTH_HOLD_USED%TYPE;
       ld_tran_start_ts PSS.TRAN.TRAN_START_TS%TYPE;
       ln_loyalty_discount PSS.CONSUMER_ACCT.LOYALTY_DISCOUNT_TOTAL%TYPE;
       ln_consumer_acct_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_TYPE_ID%TYPE;
       ln_consumer_acct_sub_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_SUB_TYPE_ID%TYPE;
       ln_cash_back_percent REPORT.CAMPAIGN.DISCOUNT_PERCENT%TYPE;
       ln_cash_back_threshhold REPORT.CAMPAIGN.THRESHOLD_AMOUNT%TYPE;
       ln_cash_back_campaign_id REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE;
       ln_trans_type_id PSS.PAYMENT_SUBTYPE.TRANS_TYPE_ID%TYPE;
       ln_operator_trans_type_id REPORT.TRANS_TYPE.OPERATOR_TRANS_TYPE_ID%TYPE;
       lv_lock VARCHAR2(128);
    BEGIN
        SELECT ca.CONSUMER_ACCT_ID, A.AUTH_HOLD_USED, A.AUTH_ID, X.TRAN_START_TS, A.AUTH_AMT_APPROVED, NVL(SUM(ABS(XI.TRAN_LINE_ITEM_AMOUNT * XI.TRAN_LINE_ITEM_QUANTITY)), 0),
               CASE WHEN CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y' AND CA.CONSUMER_ACCT_TYPE_ID = 3 THEN 'Y' ELSE 'N' END, CA.CONSUMER_ACCT_TYPE_ID, PST.TRANS_TYPE_ID, TT.OPERATOR_TRANS_TYPE_ID
          INTO pn_consumer_acct_id, ln_auth_hold_used, ln_auth_id, ld_tran_start_ts, ln_auth_amount, ln_loyalty_discount, pc_auto_replenish_flag, ln_consumer_acct_type_id, ln_trans_type_id, ln_operator_trans_type_id
          FROM PSS.CONSUMER_ACCT ca
          JOIN PSS.TRAN X ON X.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
          JOIN PSS.AUTH A ON X.TRAN_ID = A.TRAN_ID AND A.AUTH_TYPE_CD = 'N' AND A.AUTH_RESULT_CD IN('Y', 'P', 'V', 'C', 'M')
          JOIN PSS.POS_PTA PP ON X.POS_PTA_ID = PP.POS_PTA_ID
          JOIN PSS.PAYMENT_SUBTYPE PST ON PP.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
          JOIN REPORT.TRANS_TYPE TT ON PST.TRANS_TYPE_ID = TT.TRANS_TYPE_ID
          LEFT OUTER JOIN PSS.TRAN_LINE_ITEM XI ON X.TRAN_ID = XI.TRAN_ID AND XI.TRAN_LINE_ITEM_TYPE_ID = 204
         WHERE X.TRAN_ID = pn_tran_id
         GROUP BY ca.CONSUMER_ACCT_ID, A.AUTH_HOLD_USED, A.AUTH_ID, X.TRAN_START_TS, A.AUTH_AMT_APPROVED, CA.CONSUMER_ACCT_ACTIVE_YN_FLAG, CA.CONSUMER_ACCT_TYPE_ID, PST.TRANS_TYPE_ID, TT.OPERATOR_TRANS_TYPE_ID;
        IF pc_auto_replenish_flag = 'Y' THEN
            SELECT NVL(MAX(REPLENISH_FLAG), 'N')
              INTO pc_auto_replenish_flag
              FROM (
                SELECT CASE WHEN CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y'
                             AND CA.CONSUMER_ACCT_TYPE_ID = 3
                             AND CAR.REPLENISH_THRESHHOLD > 0
                             AND CAR.REPLENISH_AMOUNT > 0
                             AND CAR.REPLENISH_CARD_KEY IS NOT NULL THEN 'Y' ELSE 'N' END REPLENISH_FLAG
                  FROM PSS.CONSUMER_ACCT CA
                  JOIN PSS.CONSUMER_ACCT_REPLENISH CAR ON CA.CONSUMER_ACCT_ID = CAR.CONSUMER_ACCT_ID
                 WHERE ca.CONSUMER_ACCT_ID = pn_consumer_acct_id
                   AND CAR.REPLENISH_TYPE_ID = 1
                 ORDER BY CAR.PRIORITY, CAR.CONSUMER_ACCT_REPLENISH_ID)
             WHERE ROWNUM = 1;
        END IF;
        IF ln_auth_hold_used = 'Y' THEN
            IF ln_consumer_acct_type_id IN (3,7) THEN
                SELECT MAX(DISCOUNT_PERCENT), MAX(THRESHOLD_AMOUNT), MAX(CAMPAIGN_ID)
                  INTO ln_cash_back_percent, ln_cash_back_threshhold, ln_cash_back_campaign_id
                  FROM (SELECT C.DISCOUNT_PERCENT, C.THRESHOLD_AMOUNT, C.CAMPAIGN_ID
                  FROM REPORT.CAMPAIGN C
                  JOIN PSS.CAMPAIGN_CONSUMER_ACCT CCA on C.CAMPAIGN_ID = CCA.CAMPAIGN_ID
                 WHERE CCA.CONSUMER_ACCT_ID = pn_consumer_acct_id
                   AND ld_tran_start_ts BETWEEN NVL(C.START_DATE, MIN_DATE) AND NVL(C.END_DATE, MAX_DATE)
                   AND C.CAMPAIGN_TYPE_ID = 3 /* Spend reward - Cash back */
                   AND C.DISCOUNT_PERCENT > 0
                   AND C.DISCOUNT_PERCENT < 1
                   AND (TRIM(C.RECUR_SCHEDULE) IS NULL OR REPORT.MATCH_CAMPAIGN_RECUR_SCHEDULE(C.RECUR_SCHEDULE, ld_tran_start_ts) = 'Y')
                 ORDER BY C.DISCOUNT_PERCENT DESC, C.CAMPAIGN_ID DESC)
                 WHERE ROWNUM = 1;
            END IF;
            lv_lock := PKG_GLOBAL.REQUEST_LOCK('PSS.CONSUMER_ACCT', pn_consumer_acct_id);
            DELETE
              FROM PSS.CONSUMER_ACCT_AUTH_HOLD
             WHERE AUTH_ID = ln_auth_id
               AND EXPIRATION_TS > SYSDATE
               AND CLEARED_YN_FLAG = 'N';
            IF SQL%FOUND THEN
                -- debit CONSUMER_ACCT_PROMO_BALANCE first and then CONSUMER_ACCT_REPLEN_BALANCE
                UPDATE PSS.CONSUMER_ACCT
                   SET CONSUMER_ACCT_BALANCE = CASE
                        WHEN pn_amount < CONSUMER_ACCT_BALANCE OR ALLOW_NEGATIVE_BALANCE = 'Y' THEN CONSUMER_ACCT_BALANCE - pn_amount
                        ELSE 0 END,
                       LOYALTY_DISCOUNT_TOTAL = NVL(LOYALTY_DISCOUNT_TOTAL, 0) + ln_loyalty_discount,
                       CONSUMER_ACCT_PROMO_BALANCE = CONSUMER_ACCT_PROMO_BALANCE -
                            CASE WHEN CONSUMER_ACCT_PROMO_BALANCE > 0 AND CONSUMER_ACCT_PROMO_BALANCE >= pn_amount THEN pn_amount ELSE CONSUMER_ACCT_PROMO_BALANCE END,
                       CONSUMER_ACCT_REPLEN_BALANCE = CONSUMER_ACCT_REPLEN_BALANCE -
                            CASE WHEN CONSUMER_ACCT_PROMO_BALANCE > 0 AND CONSUMER_ACCT_PROMO_BALANCE >= pn_amount THEN 0
                                WHEN CONSUMER_ACCT_REPLEN_BALANCE > 0 AND CONSUMER_ACCT_REPLEN_BALANCE >= pn_amount - CONSUMER_ACCT_PROMO_BALANCE THEN pn_amount - CONSUMER_ACCT_PROMO_BALANCE
                                ELSE CONSUMER_ACCT_REPLEN_BALANCE END,
                 TRAN_COUNT_TOTAL=CASE WHEN CONSUMER_ACCT_TYPE_ID in (3,7) THEN NVL(TRAN_COUNT_TOTAL, 0) + 1 ELSE TRAN_COUNT_TOTAL END
                 WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id
                  RETURNING CONSUMER_ACCT_BALANCE, CONSUMER_ACCT_SUB_TYPE_ID
                  INTO pn_new_balance, ln_consumer_acct_sub_type_id;
                pc_auth_result_cd := 'Y';
                UPDATE PSS.AUTH SA
                   SET AUTH_STATE_ID = 2,
                       AUTH_RESULT_CD = pc_auth_result_cd,
                       OVERRIDE_TRANS_TYPE_ID = CASE WHEN ln_consumer_acct_type_id = 3 AND ln_consumer_acct_sub_type_id = 2 THEN ln_operator_trans_type_id END
                 WHERE SA.TRAN_ID = pn_tran_id
                   AND SA.AUTH_TYPE_CD IN('U', 'C')
                   AND SA.AUTH_STATE_ID = 6;
                IF SQL%ROWCOUNT != 1 THEN
                    RAISE_APPLICATION_ERROR(-20116, 'Auth Hold Inconsistent State for transaction ' || pn_tran_id);
                END IF;
                IF ln_cash_back_campaign_id IS NOT NULL AND ln_cash_back_percent > 0 THEN
                    DECLARE
                        ln_cash_back_balance PSS.CONSUMER_ACCT.TOWARD_CASH_BACK_BALANCE%TYPE;
                        lv_cash_back_device_name DEVICE.DEVICE_NAME%TYPE;
                        ln_cash_back_next_master_id NUMBER;
                        lv_currency_cd PSS.CONSUMER_ACCT.CURRENCY_CD%TYPE;
                        ln_result_cd NUMBER;
                        lv_error_message VARCHAR2(4000);
                        ln_cash_back_tran_id PSS.TRAN.TRAN_ID%TYPE;
                        lv_cash_back_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
                        ln_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE;
                        lc_currency_symbol CORP.CURRENCY.CURRENCY_SYMBOL%TYPE;
                        ln_host_id HOST.HOST_ID%TYPE;
                        ld_cash_back_ts DATE;
                        ld_cash_back_time NUMBER;
                        ln_corp_customer_id PSS.CONSUMER_ACCT.CORP_CUSTOMER_ID%TYPE;
                        lv_consumer_acct_cd PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD%TYPE;
                        lv_consumer_acct_identifier PSS.CONSUMER_ACCT.CONSUMER_ACCT_IDENTIFIER%TYPE;
                        lv_desc PSS.TRAN_LINE_ITEM.TRAN_LINE_ITEM_DESC%TYPE;
                        ln_doc_id CORP.DOC.DOC_ID%TYPE;
                        ln_ledger_id CORP.LEDGER.LEDGER_ID%TYPE;
                    BEGIN
                        -- update consumer acct
                        UPDATE PSS.CONSUMER_ACCT
                           SET TOWARD_CASH_BACK_BALANCE = NVL(TOWARD_CASH_BACK_BALANCE, 0) + pn_amount
                         WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id
                         RETURNING TOWARD_CASH_BACK_BALANCE, CURRENCY_CD, CORP_CUSTOMER_ID, CONSUMER_ACCT_CD, CONSUMER_ACCT_IDENTIFIER
                          INTO ln_cash_back_balance, lv_currency_cd, ln_corp_customer_id, lv_consumer_acct_cd, lv_consumer_acct_identifier;
                        IF ln_cash_back_balance >= ln_cash_back_threshhold THEN
                            pn_used_cash_back_balance := TRUNC(ln_cash_back_balance / ln_cash_back_threshhold) * ln_cash_back_threshhold;
                            pn_cash_back_amount := ROUND(pn_used_cash_back_balance * ln_cash_back_percent, 2);
                            UPDATE PSS.CONSUMER_ACCT
                               SET TOWARD_CASH_BACK_BALANCE = TOWARD_CASH_BACK_BALANCE - pn_used_cash_back_balance,
                                   CASH_BACK_TOTAL = NVL(CASH_BACK_TOTAL, 0) + pn_cash_back_amount,
                                   CONSUMER_ACCT_BALANCE = CONSUMER_ACCT_BALANCE + pn_cash_back_amount,
                                   CONSUMER_ACCT_PROMO_BALANCE = CONSUMER_ACCT_PROMO_BALANCE + pn_cash_back_amount,
                                   CONSUMER_ACCT_PROMO_TOTAL = CONSUMER_ACCT_PROMO_TOTAL + pn_cash_back_amount
                             WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id;
                            -- add trans to virtual terminal
                            SELECT C.MINOR_CURRENCY_FACTOR, NVL(CC.CURRENCY_SYMBOL, '$'), DBADMIN.TIMESTAMP_TO_MILLIS(SYS_EXTRACT_UTC(SYSTIMESTAMP)), SYSDATE
                              INTO ln_minor_currency_factor, lc_currency_symbol, ld_cash_back_time, ld_cash_back_ts
                              FROM PSS.CURRENCY C
                              LEFT OUTER JOIN CORP.CURRENCY CC ON C.CURRENCY_CD = CC.CURRENCY_CODE
                             WHERE C.CURRENCY_CD = lv_currency_cd;

                            PKG_DEVICE_CONFIGURATION.NEXT_MASTER_ID_BY_SERIAL('V1-' || ln_corp_customer_id || '-' || lv_currency_cd, lv_cash_back_device_name, ln_cash_back_next_master_id);
                            SP_CREATE_SALE('A', lv_cash_back_device_name, ln_cash_back_next_master_id,  0, 'C', ld_cash_back_time,
                                DBADMIN.PKG_UTL.TIME_ZONE_DIFF_MIN(PKG_CONST.DB_TIME_ZONE, PKG_CONST.GMT_TIME_ZONE), 'S', 0, pn_cash_back_amount * ln_minor_currency_factor, 'U', 'A', 'SHA1',
                                DBADMIN.HASH_CARD('Bonus Cash on ' || pn_consumer_acct_id || ' of ' || pn_cash_back_amount),
                                NULL, ln_result_cd, lv_error_message, ln_cash_back_tran_id, lv_cash_back_tran_state_cd);
                            IF ln_result_cd != PKG_CONST.RESULT__SUCCESS THEN
                                RAISE_APPLICATION_ERROR(-20118, 'Could not create cash back transaction: ' || lv_error_message);
                            END IF;
                            SELECT HOST_ID, 'Bonus Cash for ' || lc_currency_symbol || TO_NUMBER(pn_used_cash_back_balance, 'FM9,999,999.00') || ' in purchases'
                              INTO ln_host_id, lv_desc
                              FROM (SELECT H.HOST_ID
                                      FROM DEVICE.HOST H
                                      JOIN DEVICE.DEVICE D ON H.DEVICE_ID = D.DEVICE_ID
                                     WHERE D.DEVICE_ACTIVE_YN_FLAG = 'Y'
                                       AND H.HOST_PORT_NUM IN(0,1)
                                       AND D.DEVICE_NAME = lv_cash_back_device_name
                                       AND H.HOST_ACTIVE_YN_FLAG = 'Y'
                                     ORDER BY H.HOST_PORT_NUM DESC, H.HOST_ID DESC)
                             WHERE ROWNUM = 1;
                            INSERT INTO PSS.TRAN_LINE_ITEM(
                                TRAN_ID,
                                TRAN_LINE_ITEM_AMOUNT,
                                TRAN_LINE_ITEM_POSITION_CD,
                                TRAN_LINE_ITEM_TAX,
                                TRAN_LINE_ITEM_TYPE_ID,
                                TRAN_LINE_ITEM_QUANTITY,
                                TRAN_LINE_ITEM_DESC,
                                HOST_ID,
                                TRAN_LINE_ITEM_BATCH_TYPE_CD,
                                TRAN_LINE_ITEM_TS,
                                SALE_RESULT_ID,
                                APPLY_TO_CONSUMER_ACCT_ID,
                                CAMPAIGN_ID)
                            SELECT
                                ln_cash_back_tran_id,
                                pn_used_cash_back_balance,
                                NULL,
                                NULL,
                                554,
                                ln_cash_back_percent,
                                lv_desc,
                                ln_host_id,
                                'A',
                                ld_cash_back_ts,
                                0,
                                pn_consumer_acct_id,
                                ln_cash_back_campaign_id
                            FROM DUAL;
                            UPDATE PSS.TRAN
                               SET PARENT_TRAN_ID = pn_tran_id
                             WHERE TRAN_ID = ln_cash_back_tran_id;
                            IF ln_consumer_acct_sub_type_id = 1 THEN
                                -- add adjustment to ledger
                                CORP.PAYMENTS_PKG.ADJUSTMENT_INS(ln_corp_customer_id, 'Bonus Cash Processing', lv_currency_cd, lv_desc || ', card # ' || lv_consumer_acct_cd || ', card ID ' || lv_consumer_acct_identifier,
                                    -pn_cash_back_amount, ln_doc_id, ln_ledger_id);
                            END IF;
                        END IF;
                    END;
                END IF;
            ELSE
                SELECT MAX(AUTH_RESULT_CD)
                  INTO pc_auth_result_cd
                  FROM PSS.AUTH SA
                 WHERE SA.TRAN_ID = pn_tran_id
                   AND SA.AUTH_TYPE_CD IN('U', 'C')
                   AND SA.AUTH_STATE_ID = 2;
                IF pc_auth_result_cd IS NULL THEN
                    DECLARE
                        ld_expiration_ts PSS.CONSUMER_ACCT_AUTH_HOLD.EXPIRATION_TS%TYPE;
                        lc_cleared_flag PSS.CONSUMER_ACCT_AUTH_HOLD.CLEARED_YN_FLAG%TYPE;
                    BEGIN
                        SELECT EXPIRATION_TS, CLEARED_YN_FLAG
                          INTO ld_expiration_ts, lc_cleared_flag
                          FROM PSS.CONSUMER_ACCT_AUTH_HOLD
                         WHERE AUTH_ID = ln_auth_id;
                        IF lc_cleared_flag != 'N' THEN
                            RAISE_APPLICATION_ERROR(-20114, 'Auth Hold Was Cleared for transaction ' || pn_tran_id);
                        ELSIF ld_expiration_ts <= SYSDATE THEN
                            RAISE_APPLICATION_ERROR(-20115, 'Auth Hold Has Expired for transaction ' || pn_tran_id);
                        ELSE
                            RAISE_APPLICATION_ERROR(-20116, 'Auth Hold Inconsistent State for transaction ' || pn_tran_id);
                        END IF;
                    EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                            RAISE_APPLICATION_ERROR(-20111, 'Auth Hold Not Found for transaction ' || pn_tran_id);
                    END;
                END IF;
            END IF;
            pn_debitted_amount := pn_amount;
        ELSE
            pn_debitted_amount := 0;
            pc_auto_replenish_flag := 'N';
        END IF;
    END;

    -- R37 and above
    PROCEDURE CREDIT_CONSUMER_ACCT(
       pn_amount PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
       pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
       pc_redelivery_flag CHAR,
       pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
       pc_auth_result_cd OUT VARCHAR2,
       pn_creditted_amount OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
       pn_new_balance OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE)
    IS
       ln_refund_id PSS.REFUND.REFUND_ID%TYPE;
       ln_refund_state_id PSS.REFUND.REFUND_STATE_ID%TYPE;
       ln_consumer_acct_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_TYPE_ID%TYPE;
       ln_consumer_acct_sub_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_SUB_TYPE_ID%TYPE;
       ln_override_trans_type_id PSS.REFUND.OVERRIDE_TRANS_TYPE_ID%TYPE;
       lv_lock VARCHAR2(128);
    BEGIN
        SELECT ca.CONSUMER_ACCT_ID, R.REFUND_ID, R.REFUND_STATE_ID, CA.CONSUMER_ACCT_TYPE_ID, CA.CONSUMER_ACCT_SUB_TYPE_ID
          INTO pn_consumer_acct_id, ln_refund_id, ln_refund_state_id, ln_consumer_acct_type_id, ln_consumer_acct_sub_type_id
          FROM PSS.CONSUMER_ACCT ca
          JOIN PSS.TRAN X ON X.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
          JOIN PSS.REFUND R ON X.TRAN_ID = R.TRAN_ID
         WHERE X.TRAN_ID = pn_tran_id;

        IF ln_refund_state_id IN(2,3,4,6) THEN
            pc_auth_result_cd := 'Y';
            IF ln_consumer_acct_type_id = 3 AND ln_consumer_acct_sub_type_id IS NOT NULL THEN
                IF ln_consumer_acct_sub_type_id = 2 THEN
                    ln_override_trans_type_id := 31;
                ELSE
                    ln_override_trans_type_id := 20;
                END IF;
            END IF;
            lv_lock := PKG_GLOBAL.REQUEST_LOCK('PSS.CONSUMER_ACCT', pn_consumer_acct_id);
            UPDATE PSS.REFUND
               SET REFUND_STATE_ID = 1,
                   OVERRIDE_TRANS_TYPE_ID = ln_override_trans_type_id
             WHERE REFUND_ID = ln_refund_id
               AND REFUND_STATE_ID = ln_refund_state_id;
            IF SQL%FOUND THEN
                UPDATE PSS.CONSUMER_ACCT
                   SET CONSUMER_ACCT_BALANCE = CONSUMER_ACCT_BALANCE + pn_amount,
                       CONSUMER_ACCT_REPLEN_BALANCE = CONSUMER_ACCT_REPLEN_BALANCE + pn_amount,
                       TOWARD_CASH_BACK_BALANCE = CASE WHEN TOWARD_CASH_BACK_BALANCE IS NOT NULL AND CONSUMER_ACCT_TYPE_ID = 3 THEN TOWARD_CASH_BACK_BALANCE - pn_amount ELSE TOWARD_CASH_BACK_BALANCE END
                 WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id
                   AND NVL(CONSUMER_ACCT_SUB_TYPE_ID, 0) = NVL(ln_consumer_acct_sub_type_id, 0)
                 RETURNING CONSUMER_ACCT_BALANCE INTO pn_new_balance;
                IF NOT SQL%FOUND THEN
                    RAISE_APPLICATION_ERROR(-20116, 'Auth Hold Inconsistent State for transaction ' || pn_tran_id);
                END IF;
            ELSE
                RAISE_APPLICATION_ERROR(-20116, 'Auth Hold Inconsistent State for transaction ' || pn_tran_id);
            END IF;
            pn_creditted_amount := pn_amount;
        ELSIF pc_redelivery_flag != 'Y' THEN
            RAISE_APPLICATION_ERROR(-20111, 'Pending refund not found for transaction ' || pn_tran_id);
        ELSE
            pc_auth_result_cd := 'Y';
            pn_creditted_amount := pn_amount;
        END IF;
    END;

    -- For R37 and above
    PROCEDURE CHECK_REPLENISH_CONSUMER_ACCT(
       pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
       pv_replenish_device_serial_cd OUT DEVICE.DEVICE_SERIAL_CD%TYPE,
       pc_auto_replenish_flag OUT VARCHAR2,
       pn_replenish_id OUT PSS.CONSUMER_ACCT_REPLENISH.CONSUMER_ACCT_REPLENISH_ID%TYPE,
       pn_replenish_amount OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_AMOUNT%TYPE,
       pv_replenish_card_key OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_CARD_KEY%TYPE,
       pn_global_account_id OUT PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
       pn_consumer_id OUT PSS.CONSUMER_ACCT.CONSUMER_ID%TYPE,
       pn_replenish_next_master_id OUT NUMBER,
       pv_replenish_device_name OUT DEVICE.DEVICE_NAME%TYPE,
       pn_max_denied_count PLS_INTEGER DEFAULT -1,
       pn_consumer_acct_balance PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE DEFAULT NULL)
    IS
       ln_replenish_threshhold PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_THRESHHOLD%TYPE;
       lv_lock VARCHAR2(128);
       ln_device_count NUMBER;
       ln_device_id DEVICE.DEVICE_ID%TYPE;
       lv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE;
       lv_device_name DEVICE.DEVICE_NAME%TYPE;
       CURSOR l_cur IS
            SELECT CAR.CONSUMER_ACCT_REPLENISH_ID, CAR.REPLENISH_TYPE_ID,
                   DECODE(CAR.REPLENISH_REQUESTED_FLAG, 'Y', NULL, CAR.REPLENISH_THRESHHOLD) REPLENISH_THRESHHOLD,
                   CAR.REPLENISH_AMOUNT, CAR.REPLENISH_CARD_KEY, NVL(pn_consumer_acct_balance,CA.CONSUMER_ACCT_BALANCE) CONSUMER_ACCT_BALANCE, CA.CURRENCY_CD,
                   'V1-' || CASE
                        WHEN CA.CONSUMER_ACCT_SUB_TYPE_ID = 2 THEN CA.CORP_CUSTOMER_ID
                        ELSE 1
                    END || '-' || CA.CURRENCY_CD REPLENISH_DEVICE_SERIAL_CD,
                    CA.CONSUMER_ID,
                    CAB.GLOBAL_ACCOUNT_ID,
                    CA.CONSUMER_ACCT_TYPE_ID,
                    CA.CORP_CUSTOMER_ID,
                    CA.LOCATION_ID
              FROM PSS.CONSUMER_ACCT CA
              JOIN PSS.CONSUMER_ACCT_BASE CAB ON CA.CONSUMER_ACCT_ID = CAB.CONSUMER_ACCT_ID
              JOIN PSS.CONSUMER_ACCT_REPLENISH CAR ON CA.CONSUMER_ACCT_ID = CAR.CONSUMER_ACCT_ID
             WHERE ca.CONSUMER_ACCT_ID = pn_consumer_acct_id
               AND (CAR.REPLENISH_TYPE_ID = 1 OR CAR.REPLENISH_REQUESTED_FLAG = 'Y')
               AND (pn_max_denied_count < 1 OR CAR.REPLENISH_DENIED_COUNT < pn_max_denied_count OR CAR.REPLENISH_REQUESTED_FLAG = 'Y')
               AND CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y'
               AND CAR.STATUS = 'A'
               AND CA.CONSUMER_ACCT_TYPE_ID in(3,6)
               AND (CAR.REPLENISH_THRESHHOLD > 0 OR CAR.REPLENISH_REQUESTED_FLAG = 'Y')
               AND CAR.REPLENISH_AMOUNT > 0
               AND CAR.REPLENISH_CARD_KEY IS NOT NULL
               AND CAR.PRIORITY=1
             ORDER BY CAR.PRIORITY, CAR.CONSUMER_ACCT_REPLENISH_ID;
    BEGIN
        lv_lock := PKG_GLOBAL.REQUEST_LOCK('PSS.CONSUMER_ACCT', pn_consumer_acct_id);
        pc_auto_replenish_flag := 'N';
        FOR l_rec IN l_cur LOOP
            UPDATE PSS.CONSUMER_ACCT_REPLENISH
               SET REPLENISH_REQUESTED_FLAG = 'N'
             WHERE CONSUMER_ACCT_REPLENISH_ID = l_rec.CONSUMER_ACCT_REPLENISH_ID
               AND REPLENISH_REQUESTED_FLAG = 'Y';
            IF SQL%FOUND THEN
                ln_replenish_threshhold := NULL; -- ignore threshhold
            ELSIF l_rec.REPLENISH_TYPE_ID != 1 THEN
                pc_auto_replenish_flag := 'N';
                CONTINUE;
            ELSE
                ln_replenish_threshhold := l_rec.REPLENISH_THRESHHOLD;
            END IF;
            pn_replenish_amount := l_rec.REPLENISH_AMOUNT;
            pn_global_account_id := l_rec.GLOBAL_ACCOUNT_ID;
            pn_consumer_id := l_rec.CONSUMER_ID;

            SELECT
              COUNT(0)
            INTO
              ln_device_count
            FROM
              device.device
            WHERE
              device.device_serial_cd = l_rec.REPLENISH_DEVICE_SERIAL_CD;
            IF (ln_device_count = 0) THEN
              PKG_DEVICE_CONFIGURATION.GET_OR_CREATE_PREPAID_DEVICE(1, l_rec.LOCATION_ID, l_rec.CORP_CUSTOMER_ID, l_rec.CURRENCY_CD, ln_device_id, lv_device_serial_cd, lv_device_name);
            END IF;

            SETUP_REPLENISH_CONSUMER_ACCT(
                   l_rec.CONSUMER_ACCT_REPLENISH_ID,
                   pn_consumer_acct_id,
                   'N',
                   l_rec.REPLENISH_DEVICE_SERIAL_CD,
                   l_rec.CONSUMER_ACCT_BALANCE,
                   ln_replenish_threshhold,
                   pn_replenish_amount,
                   pc_auto_replenish_flag,
                   pn_replenish_next_master_id,
                   pv_replenish_device_name,
                   l_rec.CONSUMER_ACCT_TYPE_ID);
            IF pc_auto_replenish_flag = 'Y' THEN
                pn_replenish_id := l_rec.CONSUMER_ACCT_REPLENISH_ID;
                pv_replenish_card_key := l_rec.REPLENISH_CARD_KEY;
                pv_replenish_device_serial_cd := l_rec.REPLENISH_DEVICE_SERIAL_CD;
                RETURN;
            END IF;
        END LOOP;
    END;

    -- R44 Signature
    PROCEDURE LOCK_AUTH_HOLD(
        pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
        pn_ignore_auth_id PSS.AUTH.AUTH_ID%TYPE,
        pn_auth_hold_total OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE,
        pn_balance OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE)
    IS
        lv_lock VARCHAR2(128);
    BEGIN
        lv_lock := PKG_GLOBAL.REQUEST_LOCK('PSS.CONSUMER_ACCT', pn_consumer_acct_id);
        SELECT CA.CONSUMER_ACCT_BALANCE
          INTO pn_balance
          FROM PSS.CONSUMER_ACCT CA
         WHERE CA.CONSUMER_ACCT_ID = pn_consumer_acct_id;
        SELECT NVL(SUM(A.AUTH_AMT_APPROVED), 0)
          INTO pn_auth_hold_total
          FROM PSS.CONSUMER_ACCT_AUTH_HOLD CAAH
          JOIN PSS.AUTH A ON CAAH.AUTH_ID = A.AUTH_ID
         WHERE CAAH.CONSUMER_ACCT_ID = pn_consumer_acct_id
           AND CAAH.EXPIRATION_TS > SYSDATE
           AND CAAH.CLEARED_YN_FLAG = 'N'
           AND (pn_ignore_auth_id IS NULL OR A.AUTH_ID != pn_ignore_auth_id);
    END;

    -- R33 to R43 Signature
    PROCEDURE LOCK_AUTH_HOLD(
        pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
        pn_ignore_auth_id PSS.AUTH.AUTH_ID%TYPE,
        pn_auth_hold_total OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE)
    IS
        ln_balance PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE;
    BEGIN
        LOCK_AUTH_HOLD(pn_consumer_acct_id, pn_ignore_auth_id, pn_auth_hold_total, ln_balance);
    END;

    PROCEDURE SETUP_REPLENISH(
        pn_replenish_id IN OUT PSS.CONSUMER_ACCT_REPLENISH.CONSUMER_ACCT_REPLENISH_ID%TYPE,
        pn_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
        pn_replenish_type_id PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_TYPE_ID%TYPE,
        pv_replenish_card_masked PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_CARD_MASKED%TYPE,
        pv_replenish_device_serial_cd IN OUT DEVICE.DEVICE_SERIAL_CD%TYPE, -- From R37 on we ignore the provided device_serial_cd
        pn_replenish_threshhold PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_THRESHHOLD%TYPE,
        pn_replenish_amount IN OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_AMOUNT%TYPE,
        pc_replenish_flag OUT VARCHAR2,
        pn_replenish_next_master_id OUT NUMBER,
        pv_replenish_device_name OUT DEVICE.DEVICE_NAME%TYPE)
    IS
        lv_lock VARCHAR2(128);
        ln_balance PSS.CONSUMER_ACCT.CONSUMER_ACCT_BALANCE%TYPE;
        ln_priority PSS.CONSUMER_ACCT_REPLENISH.PRIORITY%TYPE;
        lc_always_auth_flag CHAR(1);
        ln_consumer_acct_type_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_TYPE_ID%TYPE;
        ln_device_count NUMBER;
        ln_device_id DEVICE.DEVICE_ID%TYPE;
        lv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE;
        lv_device_name DEVICE.DEVICE_NAME%TYPE;
        ln_location_id PSS.CONSUMER_ACCT.LOCATION_ID%TYPE;
        ln_corp_customer_id PSS.CONSUMER_ACCT.CORP_CUSTOMER_ID%TYPE;
        ln_currency_cd PSS.CONSUMER_ACCT.CURRENCY_CD%TYPE;
    BEGIN
        lv_lock := PKG_GLOBAL.REQUEST_LOCK('PSS.CONSUMER_ACCT', pn_consumer_acct_id);
        SELECT CONSUMER_ACCT_BALANCE, 'V1-' || CASE
                WHEN CONSUMER_ACCT_SUB_TYPE_ID = 2 THEN CORP_CUSTOMER_ID
                ELSE 1
            END || '-' || CURRENCY_CD,
            CONSUMER_ACCT_TYPE_ID
          INTO ln_balance, pv_replenish_device_serial_cd, ln_consumer_acct_type_id
          FROM PSS.CONSUMER_ACCT
         WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id;
        IF pn_replenish_id IS NULL THEN

            SELECT PSS.SEQ_CONSUMER_ACCT_REPLENISH_ID.NEXTVAL, PRIORITY
              INTO pn_replenish_id, ln_priority
              FROM (SELECT NVL(MAX(PRIORITY), 0) + 1 PRIORITY
              FROM PSS.CONSUMER_ACCT_REPLENISH
             WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id);
            INSERT INTO PSS.CONSUMER_ACCT_REPLENISH(CONSUMER_ACCT_REPLENISH_ID, CONSUMER_ACCT_ID, PRIORITY, REPLENISH_TYPE_ID, REPLENISH_CARD_MASKED, REPLENISH_AMOUNT, REPLENISH_THRESHHOLD)
                VALUES(pn_replenish_id, pn_consumer_acct_id, ln_priority, pn_replenish_type_id, pv_replenish_card_masked, pn_replenish_amount, pn_replenish_threshhold);
            lc_always_auth_flag := 'Y';
        ELSE
            UPDATE PSS.CONSUMER_ACCT_REPLENISH CAR
               SET CAR.REPLENISH_THRESHHOLD = pn_replenish_threshhold,
                   CAR.REPLENISH_AMOUNT = pn_replenish_amount,
                   CAR.REPLENISH_TYPE_ID = pn_replenish_type_id
             WHERE CAR.CONSUMER_ACCT_REPLENISH_ID = pn_replenish_id
               AND CAR.CONSUMER_ACCT_ID = pn_consumer_acct_id;
            IF NOT SQL%FOUND THEN
                RAISE_APPLICATION_ERROR(-20119, 'ReplenishId ' || pn_replenish_id || ' does not belong to consumer acct ' || pn_consumer_acct_id);
            END IF;
            lc_always_auth_flag := 'N';
        END IF;

        SELECT
          COUNT(0)
        INTO
          ln_device_count
        FROM
          device.device
        WHERE
          device.device_serial_cd = pv_replenish_device_serial_cd;
        IF (ln_device_count = 0) THEN
          SELECT
            location_id, corp_customer_id, currency_cd
          INTO
            ln_location_id, ln_corp_customer_id, ln_currency_cd
          FROM
            PSS.CONSUMER_ACCT
          WHERE
            CONSUMER_ACCT_ID = pn_consumer_acct_id;
          PKG_DEVICE_CONFIGURATION.GET_OR_CREATE_PREPAID_DEVICE(1, ln_location_id, ln_corp_customer_id, ln_currency_cd, ln_device_id, lv_device_serial_cd, lv_device_name);
        END IF;
        
        SETUP_REPLENISH_CONSUMER_ACCT(
           pn_replenish_id,
           pn_consumer_acct_id,
           lc_always_auth_flag,
           pv_replenish_device_serial_cd,
           ln_balance,
           pn_replenish_threshhold,
           pn_replenish_amount,
           pc_replenish_flag,
           pn_replenish_next_master_id,
           pv_replenish_device_name,
           ln_consumer_acct_type_id);
        IF pc_replenish_flag = 'N' THEN
            PKG_DEVICE_CONFIGURATION.NEXT_MASTER_ID_BY_SERIAL(pv_replenish_device_serial_cd, pv_replenish_device_name, pn_replenish_next_master_id);
            pn_replenish_amount := 0;
        END IF;
    END;
    -- R44 or below
    PROCEDURE UPDATE_PENDING_REPLENISH(
        pv_replenish_device_name DEVICE.DEVICE_NAME%TYPE,
        pn_replenish_id PSS.CONSUMER_ACCT_REPLENISH.CONSUMER_ACCT_REPLENISH_ID%TYPE,
        pn_replenish_next_master_id NUMBER,
        pc_submitted_flag PSS.CONSUMER_ACCT_PEND_REPLENISH.SUBMITTED_FLAG%TYPE,
        pc_initial_replenish_flag CHAR,
        pn_denied_count OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_DENIED_COUNT%TYPE,
        pv_replenish_card_masked OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_CARD_MASKED%TYPE)
    IS
    BEGIN
      UPDATE_PENDING_REPLENISH(pv_replenish_device_name,pn_replenish_id,pn_replenish_next_master_id,pc_submitted_flag,pc_initial_replenish_flag,null,pn_denied_count,pv_replenish_card_masked);
    END;

    -- R45 or above
    PROCEDURE UPDATE_PENDING_REPLENISH(
        pv_replenish_device_name DEVICE.DEVICE_NAME%TYPE,
        pn_replenish_id PSS.CONSUMER_ACCT_REPLENISH.CONSUMER_ACCT_REPLENISH_ID%TYPE,
        pn_replenish_next_master_id NUMBER,
        pc_submitted_flag PSS.CONSUMER_ACCT_PEND_REPLENISH.SUBMITTED_FLAG%TYPE,
        pc_initial_replenish_flag CHAR,
        pd_expDate PSS.CONSUMER_ACCT_REPLENISH.EXPIRATION_DATE%TYPE,
        pn_denied_count OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_DENIED_COUNT%TYPE,
        pv_replenish_card_masked OUT PSS.CONSUMER_ACCT_REPLENISH.REPLENISH_CARD_MASKED%TYPE)
    IS
        ln_consumer_acct_id PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE;
        ln_replen_consumer_acct_id PSS.CONSUMER_ACCT.REPLEN_CONSUMER_ACCT_ID%TYPE;
    BEGIN
        IF pc_initial_replenish_flag = 'Y' THEN
            IF pc_submitted_flag NOT IN('Y', 'P') THEN
                DELETE
                  FROM PSS.CONSUMER_ACCT_PEND_REPLENISH
                 WHERE CONSUMER_ACCT_REPLENISH_ID = pn_replenish_id
                   AND DEVICE_TRAN_CD = pn_replenish_next_master_id;
                DELETE
                  FROM PSS.CONSUMER_ACCT_REPLENISH
                 WHERE CONSUMER_ACCT_REPLENISH_ID = pn_replenish_id
                RETURNING CONSUMER_ACCT_ID, REPLENISH_CARD_MASKED
                  INTO ln_consumer_acct_id, pv_replenish_card_masked;
                pn_denied_count := 1;
            ELSE
                IF pv_replenish_device_name is not null THEN
                  select consumer_acct_id into ln_replen_consumer_acct_id from PSS.TRAN
                  WHERE TRAN_DEVICE_TRAN_CD=to_char(pn_replenish_next_master_id)
                  AND DEVICE_NAME=pv_replenish_device_name;
                END IF;

                UPDATE PSS.CONSUMER_ACCT_REPLENISH CAR
                   SET PRIORITY = PRIORITY + 1, REPLENISH_AMOUNT = DECODE(CONSUMER_ACCT_REPLENISH_ID, pn_replenish_id, REPLENISH_AMOUNT, 0)
                 WHERE CONSUMER_ACCT_ID = (SELECT CONSUMER_ACCT_ID FROM PSS.CONSUMER_ACCT_REPLENISH WHERE CONSUMER_ACCT_REPLENISH_ID = pn_replenish_id);

                UPDATE PSS.CONSUMER_ACCT_REPLENISH
                   SET PRIORITY = 1, REPLEN_CONSUMER_ACCT_ID=ln_replen_consumer_acct_id,
                   EXPIRATION_DATE=pd_expDate
                 WHERE CONSUMER_ACCT_REPLENISH_ID = pn_replenish_id
                  RETURNING CONSUMER_ACCT_ID, REPLENISH_CARD_MASKED
                  INTO ln_consumer_acct_id, pv_replenish_card_masked;
                UPDATE PSS.CONSUMER_ACCT_PEND_REPLENISH
                   SET SUBMITTED_FLAG = pc_submitted_flag
                 WHERE CONSUMER_ACCT_REPLENISH_ID = pn_replenish_id
                   AND DEVICE_TRAN_CD = pn_replenish_next_master_id;
                UPDATE PSS.CONSUMER_ACCT
                SET CONSUMER_ACCT_REPLENISH_ID = pn_replenish_id, REPLEN_CONSUMER_ACCT_ID=ln_replen_consumer_acct_id
                WHERE CONSUMER_ACCT_ID = ln_consumer_acct_id;
                pn_denied_count := 0;
            END IF;
        ELSE
            UPDATE PSS.CONSUMER_ACCT_PEND_REPLENISH
               SET SUBMITTED_FLAG = pc_submitted_flag
             WHERE CONSUMER_ACCT_REPLENISH_ID = pn_replenish_id
               AND DEVICE_TRAN_CD = pn_replenish_next_master_id;
            UPDATE PSS.CONSUMER_ACCT_REPLENISH
               SET REPLENISH_DENIED_COUNT = CASE WHEN pc_submitted_flag IN('Y','P','V') THEN 0 WHEN pc_submitted_flag = '?' THEN REPLENISH_DENIED_COUNT ELSE REPLENISH_DENIED_COUNT + 1 END
             WHERE CONSUMER_ACCT_REPLENISH_ID = pn_replenish_id
              RETURNING REPLENISH_DENIED_COUNT, REPLENISH_CARD_MASKED
              INTO pn_denied_count, pv_replenish_card_masked;
        END IF;
    END;

    PROCEDURE CREATE_ISIS_CONSUMER(
        pn_consumer_id OUT PSS.CONSUMER.CONSUMER_ID%TYPE,
        pv_consumer_identifier PSS.CONSUMER.CONSUMER_IDENTIFIER%TYPE)
    IS
    BEGIN
        SELECT PSS.SEQ_CONSUMER_ID.NEXTVAL
          INTO pn_consumer_id
          FROM DUAL;
        INSERT INTO PSS.CONSUMER(CONSUMER_ID, CONSUMER_EMAIL_ADDR1, CONSUMER_TYPE_ID, CONSUMER_IDENTIFIER)
            VALUES(pn_consumer_id, 'softcard_' || pn_consumer_id || '@usatech.com', 7, pv_consumer_identifier);
    END;

    -- R37+ Signature
    PROCEDURE AUTHORIZE_ISIS_PROMO(
        pn_global_account_id IN OUT PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
        pv_instance PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_INSTANCE%TYPE,
        pv_consumer_acct_cd PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD%TYPE,
        pv_consumer_identifier PSS.CONSUMER.CONSUMER_IDENTIFIER%TYPE,
        pv_currency_cd PSS.CURRENCY.CURRENCY_CD%TYPE,
        pv_auth_result_cd OUT VARCHAR2,
        pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
        pn_consumer_id OUT PSS.CONSUMER.CONSUMER_ID%TYPE,
        pn_trans_to_free_tran OUT NUMBER)
    IS
        ln_old_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE;
    BEGIN
        pv_auth_result_cd := 'N';
        pn_consumer_id := NULL;

        IF pv_consumer_identifier IS NOT NULL THEN
            SELECT MAX(consumer_id)
              INTO pn_consumer_id
              FROM PSS.CONSUMER
              WHERE CONSUMER_IDENTIFIER = pv_consumer_identifier;

            IF pn_consumer_id IS NULL THEN
                BEGIN
                    CREATE_ISIS_CONSUMER(pn_consumer_id, pv_consumer_identifier);
                EXCEPTION
                    WHEN DUP_VAL_ON_INDEX THEN
                        SELECT MAX(consumer_id)
                          INTO pn_consumer_id
                          FROM PSS.CONSUMER
                         WHERE CONSUMER_IDENTIFIER = pv_consumer_identifier;
                END;
            END IF;
        END IF;

        IF pn_global_account_id IS NOT NULL THEN
            SELECT MAX(CAB.CONSUMER_ACCT_ID), MAX(CA.CONSUMER_ID)
              INTO pn_consumer_acct_id, ln_old_consumer_id
              FROM PSS.CONSUMER_ACCT_BASE CAB
              LEFT OUTER JOIN PSS.CONSUMER_ACCT CA ON CAB.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
             WHERE CAB.GLOBAL_ACCOUNT_ID = pn_global_account_id
               AND CAB.CURRENCY_CD = pv_currency_cd;

            IF pn_consumer_id IS NULL THEN
                pn_consumer_id := ln_old_consumer_id;
            END IF;
        END IF;

        IF pn_consumer_acct_id IS NULL OR ln_old_consumer_id IS NULL THEN
        IF pn_consumer_acct_id IS NULL THEN
                IF pn_global_account_id IS NULL THEN
                    SELECT PSS.SEQ_GLOBAL_ACCOUNT_ID_BASE.NEXTVAL
                      INTO pn_global_account_id
                      FROM DUAL;
                END IF;

            SELECT PSS.SEQ_CONSUMER_ACCT_ID.NEXTVAL
              INTO pn_consumer_acct_id
              FROM DUAL;

                INSERT INTO PSS.CONSUMER_ACCT_BASE(CONSUMER_ACCT_ID, GLOBAL_ACCOUNT_ID, GLOBAL_ACCOUNT_INSTANCE, TRUNCATED_CARD_NUMBER, PAYMENT_SUBTYPE_ID, CURRENCY_CD)
                    VALUES(pn_consumer_acct_id, pn_global_account_id, 0, DBADMIN.MASK_CREDIT_CARD(pv_consumer_acct_cd), 1, pv_currency_cd);
            END IF;
            IF ln_old_consumer_id IS NULL THEN
            IF pn_consumer_id IS NULL THEN
                CREATE_ISIS_CONSUMER(pn_consumer_id, pv_consumer_identifier);
            END IF;
            INSERT INTO PSS.CONSUMER_ACCT(CONSUMER_ACCT_ID, CONSUMER_ACCT_CD, CONSUMER_ACCT_ACTIVE_YN_FLAG, CONSUMER_ACCT_BALANCE, CONSUMER_ID, LOCATION_ID,
                    CONSUMER_ACCT_ISSUE_NUM, PAYMENT_SUBTYPE_ID, CURRENCY_CD, CONSUMER_ACCT_TYPE_ID)
                    VALUES(pn_consumer_acct_id, DBADMIN.MASK_CREDIT_CARD(pv_consumer_acct_cd), 'Y', 0, pn_consumer_id, 1, 1, 1, pv_currency_cd, 5);
            END IF;
        ELSIF pn_consumer_id != ln_old_consumer_id THEN
            UPDATE PSS.CONSUMER_ACCT
               SET CONSUMER_ID = pn_consumer_id
             WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id;
        END IF;

        UPDATE PSS.CONSUMER_PROMOTION
           SET TRAN_COUNT = tran_count + 1,
               PROMO_TRAN_COUNT = promo_tran_count + 1
         WHERE CONSUMER_ID = pn_consumer_id
           AND PROMOTION_ID = 1
           AND (TRAN_COUNT + 1) / (PROMO_TRAN_COUNT + 1) >= 5
        RETURNING 'Y' INTO pv_auth_result_cd;

        LOOP
            BEGIN
                SELECT 4 - MOD(TRAN_COUNT + 1, 5)
                  INTO pn_trans_to_free_tran
                  FROM PSS.CONSUMER_PROMOTION
                 WHERE CONSUMER_ID = pn_consumer_id
                   AND PROMOTION_ID = 1;
                EXIT;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    BEGIN
                        INSERT INTO PSS.CONSUMER_PROMOTION(CONSUMER_ID, PROMOTION_ID)
                            VALUES(pn_consumer_id, 1);
                    EXCEPTION
                        WHEN DUP_VAL_ON_INDEX THEN
                            NULL;
                    END;
            END;
        END LOOP;
    END;

    PROCEDURE REFUND_ISIS_PROMO(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pn_result_cd OUT NUMBER,
        pv_error_message OUT VARCHAR2)
    IS
        ln_refund_state_id PSS.REFUND.REFUND_STATE_ID%TYPE;
        ln_parent_tran_id PSS.TRAN.PARENT_TRAN_ID%TYPE;
        ln_refund_count NUMBER;
        ln_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE;
    BEGIN
        pn_result_cd := PKG_CONST.RESULT__SUCCESS;
        pv_error_message := PKG_CONST.ERROR__NO_ERROR;

        SELECT PARENT_TRAN_ID
        INTO ln_parent_tran_id
        FROM PSS.TRAN
        WHERE TRAN_ID = pn_tran_id;

        SELECT COUNT(1)
        INTO ln_refund_count
        FROM PSS.REFUND R
        JOIN PSS.TRAN T ON R.TRAN_ID = T.TRAN_ID
        WHERE T.PARENT_TRAN_ID = ln_parent_tran_id AND R.REFUND_STATE_ID IN(1);

        IF ln_refund_count > 0 THEN
            RETURN;
        END IF;

        SELECT CA.CONSUMER_ID
        INTO ln_consumer_id
        FROM PSS.TRAN T
        JOIN PSS.CONSUMER_ACCT CA ON T.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
        WHERE T.TRAN_ID = pn_tran_id;

        UPDATE PSS.CONSUMER_PROMOTION
        SET TRAN_COUNT = CASE WHEN TRAN_COUNT - 1 >= 0 THEN TRAN_COUNT - 1 ELSE 0 END,
            PROMO_TRAN_COUNT = CASE WHEN PROMO_TRAN_COUNT - 1 >= 0 THEN PROMO_TRAN_COUNT - 1 ELSE 0 END
        WHERE CONSUMER_ID = ln_consumer_id AND PROMOTION_ID = 1;
    END;

    PROCEDURE GET_OR_CREATE_CONSUMER_ACCT(
        pn_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
        pn_global_account_instance PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_INSTANCE%TYPE,
        pn_pos_pta_id PSS.POS_PTA.POS_PTA_ID%TYPE,
        pv_currency_cd PSS.CONSUMER_ACCT_BASE.CURRENCY_CD%TYPE,
        pd_auth_ts DATE,
        pv_truncated_card_num PSS.CONSUMER_ACCT_BASE.TRUNCATED_CARD_NUMBER%TYPE,
        pn_consumer_acct_id OUT PSS.CONSUMER_ACCT_BASE.CONSUMER_ACCT_ID%TYPE)
    IS
        ln_new_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE;
        ln_new_global_account_instance PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_INSTANCE%TYPE;
        lv_lock VARCHAR2(128);
    BEGIN
        lv_lock := PKG_GLOBAL.REQUEST_LOCK('PSS.CONSUMER_ACCT_BASE', pn_global_account_id);
        SELECT MAX(CONSUMER_ACCT_ID)
          INTO pn_consumer_acct_id
          FROM (
             SELECT CAB.CONSUMER_ACCT_ID
               FROM (
                SELECT CAB.CONSUMER_ACCT_ID,
                       CASE WHEN CA.CONSUMER_ACCT_ACTIVE_YN_FLAG IS NULL THEN 5 WHEN CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y' AND NVL(CA.CONSUMER_ACCT_ACTIVATION_TS, MIN_DATE) <= pd_auth_ts AND NVL(CA.CONSUMER_ACCT_DEACTIVATION_TS, MAX_DATE) > pd_auth_ts THEN 1 ELSE 10 END CA_PRIORITY,
                       CAB.PAYMENT_SUBTYPE_ID,
                       CAB.LOCATION_ID,
                       CASE WHEN PTA.PAYMENT_SUBTYPE_ID = CAB.PAYMENT_SUBTYPE_ID THEN 1 ELSE 10 END PST_PRIORITY,
                       POS.LOCATION_ID POS_LOCATION_ID,
                       CA.CONSUMER_ACCT_ISSUE_NUM,
                       CASE WHEN CAB.LOCATION_ID IS NULL THEN NULL ELSE (SELECT VLH.DEPTH FROM LOCATION.VW_LOCATION_HIERARCHY VLH WHERE VLH.DESCENDENT_LOCATION_ID = POS.LOCATION_ID AND VLH.ANCESTOR_LOCATION_ID = CAB.LOCATION_ID) END DEPTH
                  FROM PSS.CONSUMER_ACCT_BASE CAB
                  LEFT OUTER JOIN PSS.CONSUMER_ACCT CA ON CAB.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
                 CROSS JOIN PSS.POS_PTA PTA
                  JOIN PSS.POS POS ON PTA.POS_ID = POS.POS_ID
                 WHERE CAB.GLOBAL_ACCOUNT_ID = pn_global_account_id
                   AND PTA.POS_PTA_ID = pn_pos_pta_id
                   AND CAB.CURRENCY_CD = pv_currency_cd) CAB
                ORDER BY CAB.CA_PRIORITY, CAB.PST_PRIORITY, CAB.DEPTH NULLS LAST, CAB.CONSUMER_ACCT_ISSUE_NUM DESC
        ) WHERE ROWNUM = 1;
        IF pn_consumer_acct_id IS NULL THEN
           SELECT MAX(NEW_GLOBAL_ACCOUNT_ID), MAX(NEW_GLOBAL_ACCOUNT_INSTANCE)
              INTO ln_new_global_account_id, ln_new_global_account_instance
              FROM PSS.GLOBAL_ACCOUNT_OVERWRITE
             WHERE OLD_GLOBAL_ACCOUNT_ID = pn_global_account_id;
           IF ln_new_global_account_id IS NOT NULL AND ln_new_global_account_id != pn_global_account_id THEN
                GET_OR_CREATE_CONSUMER_ACCT(ln_new_global_account_id, ln_new_global_account_instance, pn_pos_pta_id, pv_currency_cd, pd_auth_ts, pv_truncated_card_num, pn_consumer_acct_id);
           ELSE
                SELECT PSS.SEQ_CONSUMER_ACCT_ID.NEXTVAL
                  INTO pn_consumer_acct_id
                  FROM DUAL;
                INSERT INTO PSS.CONSUMER_ACCT_BASE(CONSUMER_ACCT_ID, GLOBAL_ACCOUNT_ID, GLOBAL_ACCOUNT_INSTANCE, TRUNCATED_CARD_NUMBER, PAYMENT_SUBTYPE_ID, CURRENCY_CD)
                    SELECT pn_consumer_acct_id, pn_global_account_id, NVL(pn_global_account_instance, MOD(pn_global_account_id, 10)), SUBSTR(pv_truncated_card_num, 1, 50), PTA.PAYMENT_SUBTYPE_ID, pv_currency_cd
                      FROM PSS.POS_PTA PTA
                     WHERE PTA.POS_PTA_ID = pn_pos_pta_id;
            END IF;
        END IF;
    END;

    PROCEDURE RESOLVE_ACCOUNT_CONFLICT(
        pn_old_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
        pn_new_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
        pn_global_account_instance PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_INSTANCE%TYPE)
    IS
        CURSOR l_old_cur IS
            SELECT CAB.CONSUMER_ACCT_ID, CA.CONSUMER_ID, CAB.CURRENCY_CD, CAB.LOCATION_ID, CAB.PAYMENT_SUBTYPE_ID
              FROM PSS.CONSUMER_ACCT_BASE CAB
              LEFT OUTER JOIN PSS.CONSUMER_ACCT CA ON CAB.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
             WHERE CAB.GLOBAL_ACCOUNT_ID = pn_old_global_account_id;
        ln_newest_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE;
        ln_newest_instance PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_INSTANCE%TYPE;
        ln_new_consumer_acct_id PSS.CONSUMER_ACCT_BASE.CONSUMER_ACCT_ID%TYPE;
        ln_new_consumer_id PSS.CONSUMER.CONSUMER_ID%TYPE;
        lv_new_consumer_identifier PSS.CONSUMER.CONSUMER_IDENTIFIER%TYPE;
        lv_old_consumer_identifier PSS.CONSUMER.CONSUMER_IDENTIFIER%TYPE;
    BEGIN
        BEGIN
            INSERT INTO PSS.GLOBAL_ACCOUNT_OVERWRITE(OLD_GLOBAL_ACCOUNT_ID, NEW_GLOBAL_ACCOUNT_ID, NEW_GLOBAL_ACCOUNT_INSTANCE)
                VALUES(pn_old_global_account_id, pn_new_global_account_id, pn_global_account_instance);
        EXCEPTION
            WHEN DUP_VAL_ON_INDEX THEN
                NULL;
        END;
        SELECT NVL(MAX(NEW_GLOBAL_ACCOUNT_ID), pn_new_global_account_id), NVL(MAX(NEW_GLOBAL_ACCOUNT_INSTANCE), pn_global_account_instance)
          INTO ln_newest_global_account_id, ln_newest_instance
          FROM (
                SELECT NEW_GLOBAL_ACCOUNT_ID,
                       NEW_GLOBAL_ACCOUNT_INSTANCE,
                       CONNECT_BY_ISLEAF ISLEAF
                  FROM PSS.GLOBAL_ACCOUNT_OVERWRITE
                 START WITH OLD_GLOBAL_ACCOUNT_ID = pn_new_global_account_id
                 CONNECT BY NOCYCLE PRIOR NEW_GLOBAL_ACCOUNT_ID = OLD_GLOBAL_ACCOUNT_ID)
          WHERE ISLEAF = 1;
        FOR l_old_rec IN l_old_cur LOOP
            -- Find best new CA
            SELECT MAX(CONSUMER_ACCT_ID), MAX(CONSUMER_ID), MAX(CONSUMER_IDENTIFIER)
              INTO ln_new_consumer_acct_id, ln_new_consumer_id, lv_new_consumer_identifier
              FROM (
            SELECT CAB.CONSUMER_ACCT_ID, CAB.CONSUMER_ID, CAB.CONSUMER_IDENTIFIER
              FROM (
                SELECT CAB.CONSUMER_ACCT_ID,
                       CASE WHEN CA.CONSUMER_ACCT_ACTIVE_YN_FLAG IS NULL THEN 5 WHEN CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y' AND NVL(CA.CONSUMER_ACCT_ACTIVATION_TS, MIN_DATE) <= SYSDATE AND NVL(CA.CONSUMER_ACCT_DEACTIVATION_TS, MAX_DATE) > SYSDATE THEN 1 ELSE 10 END CA_PRIORITY,
                       CASE WHEN l_old_rec.PAYMENT_SUBTYPE_ID = CAB.PAYMENT_SUBTYPE_ID THEN 1 ELSE 10 END PST_PRIORITY,
                       CASE WHEN NVL(l_old_rec.LOCATION_ID, 0) = NVL(CAB.LOCATION_ID, 0) THEN 1 ELSE 10 END LOCATION_PRIORITY,
                       CA.CONSUMER_ACCT_ISSUE_NUM,
                       C.CONSUMER_ID,
                       C.CONSUMER_IDENTIFIER
                  FROM PSS.CONSUMER_ACCT_BASE CAB
                  LEFT OUTER JOIN (PSS.CONSUMER_ACCT CA
                  JOIN PSS.CONSUMER C ON CA.CONSUMER_ID = C.CONSUMER_ID) ON CAB.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID
                 WHERE CAB.GLOBAL_ACCOUNT_ID = ln_newest_global_account_id
                   AND CAB.CURRENCY_CD = l_old_rec.CURRENCY_CD) CAB
             ORDER BY CAB.CA_PRIORITY, CAB.PST_PRIORITY, CAB.LOCATION_PRIORITY, CAB.CONSUMER_ACCT_ISSUE_NUM DESC, CAB.CONSUMER_IDENTIFIER NULLS LAST
             ) WHERE ROWNUM = 1;
            IF ln_new_consumer_acct_id IS NULL THEN -- This occurs when currency of newest is different than old; we can just update old's global_account_id
                UPDATE PSS.CONSUMER_ACCT_BASE
                   SET GLOBAL_ACCOUNT_ID = ln_newest_global_account_id, GLOBAL_ACCOUNT_INSTANCE = ln_newest_instance
                 WHERE GLOBAL_ACCOUNT_ID = pn_old_global_account_id;
            ELSE
                IF l_old_rec.CONSUMER_ID IS NOT NULL THEN
                    IF ln_new_consumer_id IS NULL THEN
                        UPDATE PSS.CONSUMER_ACCT
                           SET CONSUMER_ACCT_ID = ln_new_consumer_acct_id
                         WHERE CONSUMER_ACCT_ID = l_old_rec.CONSUMER_ACCT_ID;
                    ELSE
                        DELETE
                          FROM PSS.CONSUMER_ACCT
                         WHERE CONSUMER_ACCT_ID = l_old_rec.CONSUMER_ACCT_ID;
                        IF ln_new_consumer_id != l_old_rec.CONSUMER_ID THEN
                            UPDATE PSS.CONSUMER_ACCT
                               SET CONSUMER_ID = ln_new_consumer_id
                             WHERE CONSUMER_ID = l_old_rec.CONSUMER_ID;

                            UPDATE PSS.CONSUMER_PROMOTION CP
                               SET (TRAN_COUNT, PROMO_TRAN_COUNT) =
                                   (SELECT NVL(SUM(CP0.TRAN_COUNT), 0) + CP.TRAN_COUNT, NVL(SUM(CP0.PROMO_TRAN_COUNT), 0) + CP.PROMO_TRAN_COUNT
                                      FROM PSS.CONSUMER_PROMOTION CP0
                                     WHERE CP0.CONSUMER_ID = l_old_rec.CONSUMER_ID)
                             WHERE CP.CONSUMER_ID = ln_new_consumer_id;

                            DELETE FROM PSS.CONSUMER_PROMOTION
                             WHERE CONSUMER_ID = l_old_rec.CONSUMER_ID;

                            DELETE FROM PSS.CONSUMER
                             WHERE CONSUMER_ID = l_old_rec.CONSUMER_ID
                             RETURNING CONSUMER_IDENTIFIER INTO lv_old_consumer_identifier;

                            IF lv_new_consumer_identifier IS NULL AND lv_old_consumer_identifier IS NOT NULL THEN
                                UPDATE PSS.CONSUMER
                                  SET CONSUMER_IDENTIFIER = lv_old_consumer_identifier
                                WHERE CONSUMER_ID = ln_new_consumer_id
                                  AND CONSUMER_IDENTIFIER IS NULL;
                            END IF;
                       END IF;
                    END IF;
                END IF;

                UPDATE PSS.CONSUMER_ACCT_DEVICE CAD
                   SET (USED_COUNT, LAST_USED_UTC_TS) =
                       (SELECT NVL(SUM(CAD0.USED_COUNT), 0) + CAD.USED_COUNT, NULLIF(GREATEST(NVL(MAX(CAD0.LAST_USED_UTC_TS), MIN_DATE), NVL(CAD.LAST_USED_UTC_TS, MIN_DATE)), MIN_DATE)
                          FROM PSS.CONSUMER_ACCT_DEVICE CAD0
                         WHERE CAD0.CONSUMER_ACCT_ID = l_old_rec.CONSUMER_ACCT_ID)
                 WHERE CAD.CONSUMER_ACCT_ID = ln_new_consumer_acct_id;

                IF SQL%ROWCOUNT > 0 THEN
                    DELETE FROM PSS.CONSUMER_ACCT_DEVICE
                     WHERE CONSUMER_ACCT_ID = l_old_rec.CONSUMER_ACCT_ID;
                ELSE
                    UPDATE PSS.CONSUMER_ACCT_DEVICE
                       SET CONSUMER_ACCT_ID = ln_new_consumer_acct_id
                     WHERE CONSUMER_ACCT_ID = l_old_rec.CONSUMER_ACCT_ID;
                END IF;

                UPDATE PSS.TRAN
                   SET CONSUMER_ACCT_ID = ln_new_consumer_acct_id
                 WHERE CONSUMER_ACCT_ID = l_old_rec.CONSUMER_ACCT_ID;

                UPDATE REPORT.TRANS
                   SET CONSUMER_ACCT_ID = ln_new_consumer_acct_id
                 WHERE CONSUMER_ACCT_ID = l_old_rec.CONSUMER_ACCT_ID;

                UPDATE REPORT.ACTIVITY_REF
                   SET CONSUMER_ACCT_ID = ln_new_consumer_acct_id
                 WHERE CONSUMER_ACCT_ID = l_old_rec.CONSUMER_ACCT_ID;

                DELETE
                  FROM PSS.CONSUMER_ACCT_BASE
                 WHERE CONSUMER_ACCT_ID = l_old_rec.CONSUMER_ACCT_ID;
             END IF;
             COMMIT;
         END LOOP;
    END;

    PROCEDURE UPDATE_TRAN_ACCOUNT(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pn_global_account_id PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
        pn_global_account_instance PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_INSTANCE%TYPE)
    IS
        ln_consumer_acct_id PSS.CONSUMER_ACCT_BASE.CONSUMER_ACCT_ID%TYPE;
        ln_pos_pta_id PSS.POS_PTA.POS_PTA_ID%TYPE;
        lv_currency_cd PSS.CONSUMER_ACCT_BASE.CURRENCY_CD%TYPE;
        ld_auth_ts DATE;
        lv_truncated_card_num PSS.CONSUMER_ACCT_BASE.TRUNCATED_CARD_NUMBER%TYPE;
        lv_machine_trans_no PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE;
        ln_report_tran_id REPORT.TRANS.TRAN_ID%TYPE;
    BEGIN
        SELECT X.POS_PTA_ID, NVL(PP.CURRENCY_CD, 'USD'), COALESCE(MIN(A.AUTH_TS), X.TRAN_UPLOAD_TS, X.CREATED_TS), X.TRAN_RECEIVED_RAW_ACCT_DATA, X.TRAN_GLOBAL_TRANS_CD
          INTO ln_pos_pta_id, lv_currency_cd, ld_auth_ts, lv_truncated_card_num, lv_machine_trans_no
          FROM PSS.TRAN X
          JOIN PSS.POS_PTA PP ON X.POS_PTA_ID = PP.POS_PTA_ID
          LEFT OUTER JOIN PSS.AUTH A ON X.TRAN_ID = A.TRAN_ID AND A.AUTH_TYPE_CD IN('L', 'N')
         WHERE X.TRAN_ID = pn_tran_id
         GROUP BY X.POS_PTA_ID, PP.CURRENCY_CD, X.TRAN_UPLOAD_TS, X.CREATED_TS, X.TRAN_RECEIVED_RAW_ACCT_DATA, X.TRAN_GLOBAL_TRANS_CD;

        -- create CAB row if needed
        GET_OR_CREATE_CONSUMER_ACCT(pn_global_account_id, pn_global_account_instance, ln_pos_pta_id, lv_currency_cd, ld_auth_ts, lv_truncated_card_num, ln_consumer_acct_id);

        -- update TRAN
        UPDATE PSS.TRAN
           SET CONSUMER_ACCT_ID = ln_consumer_acct_id
         WHERE TRAN_ID = pn_tran_id;
        UPDATE REPORT.TRANS
           SET CONSUMER_ACCT_ID = ln_consumer_acct_id
         WHERE MACHINE_TRANS_NO = lv_machine_trans_no
          RETURNING TRAN_ID INTO ln_report_tran_id;
        UPDATE REPORT.ACTIVITY_REF
           SET CONSUMER_ACCT_ID = ln_consumer_acct_id
         WHERE TRAN_ID = ln_report_tran_id;
    END;

    PROCEDURE UPDATE_AUTH_STAT_BY_DAY (
        pn_auth_id PSS.AUTH.AUTH_ID%TYPE)
    IS
        lv_device_name PSS.TRAN.DEVICE_NAME%TYPE;
        lv_auth_type_cd PSS.AUTH.AUTH_TYPE_CD%TYPE;
        ld_auth_day DATE;
        lt_auth_ts TIMESTAMP;
        ln_trans_type_id PSS.PAYMENT_SUBTYPE.TRANS_TYPE_ID%TYPE;
        lv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE;
        lv_currency_cd PSS.POS_PTA.CURRENCY_CD%TYPE;
        lv_country_cd PSS.CONSUMER_ACCT_BASE.COUNTRY_CODE%TYPE;
        ln_auth_amt PSS.AUTH.AUTH_AMT%TYPE;
        lv_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE;
        lv_credit_debit_ind PSS.CONSUMER_ACCT_BASE.CREDIT_DEBIT_IND%TYPE;
        lv_prepaid_flag PSS.CONSUMER_ACCT_BASE.PREPAID_FLAG%TYPE;
        ln_interval NUMBER;
        ln_intervals NUMBER := 60;
        lt_interval_ts TIMESTAMP;
        lt_now TIMESTAMP := SYSTIMESTAMP;
        lt_cutoff TIMESTAMP := trunc(lt_now - ((ln_intervals-1)/1440), 'MI');
    BEGIN
        select t.device_name, a.auth_type_cd, trunc(a.auth_ts), a.auth_ts, ps.trans_type_id, ps.payment_subtype_class, pp.currency_cd, cab.country_code, a.auth_amt, a.auth_result_cd, cab.credit_debit_ind, cab.prepaid_flag, ps.payment_subtype_class
        into lv_device_name, lv_auth_type_cd, ld_auth_day, lt_auth_ts, ln_trans_type_id, lv_payment_subtype_class, lv_currency_cd, lv_country_cd, ln_auth_amt, lv_auth_result_cd, lv_credit_debit_ind, lv_prepaid_flag, lv_payment_subtype_class
        from pss.auth a
        join pss.tran t on t.tran_id = a.tran_id
        join pss.pos_pta pp on pp.pos_pta_id = t.pos_pta_id
        join pss.payment_subtype ps on ps.payment_subtype_id = pp.payment_subtype_id
        left outer join pss.consumer_acct_base cab on cab.consumer_acct_id = t.consumer_acct_id
        where a.auth_id = pn_auth_id;

        IF lv_auth_type_cd <> 'N' OR lv_payment_subtype_class in ('Authority::NOP', 'Cash', 'Demo') THEN
            RETURN;
        END IF;

        LOOP
            update pss.auth_stat_by_day set
                auth_count = nvl(auth_count, 0) + 1,
                auth_amount = nvl(auth_amount, 0) + ln_auth_amt,
                auth_amount_s = dbadmin.welford_s(auth_count, auth_amount, auth_amount_s, ln_auth_amt),
                auth_amount_stdev = dbadmin.welford_stdev(auth_count, auth_amount, auth_amount_s, ln_auth_amt),
                auth_declined_count = auth_declined_count + decode(lv_auth_result_cd, 'N', 1, 'O', 1, 'R', 1, 0),
                auth_failed_count = auth_failed_count + decode(lv_auth_result_cd, 'F', 1, 0),
                credit_count = credit_count + decode(lv_credit_debit_ind, 'C', 1, 0),
                debit_count = debit_count + decode(lv_credit_debit_ind, 'D', 1, 0),
                prepaid_count = prepaid_count + decode(lv_prepaid_flag, 'Y', 1, 0)
            where device_name = lv_device_name
                and auth_date = ld_auth_day
                and trans_type_id = ln_trans_type_id
                and nvl(currency_cd,'X') = nvl(lv_currency_cd,'X')
                and nvl(consumer_acct_coutry_code,'X') = nvl(lv_country_cd,'X');
            EXIT WHEN SQL%ROWCOUNT > 0;
            BEGIN
                insert into pss.auth_stat_by_day(
                    device_name,
                    auth_date,
                    trans_type_id,
                    currency_cd,
                    consumer_acct_coutry_code,
                    auth_count,
                    auth_amount,
                    auth_amount_s,
                    auth_amount_stdev,
                    auth_declined_count,
                    auth_failed_count,
                    credit_count,
                    debit_count,
                    prepaid_count)
                select
                    lv_device_name,
                    ld_auth_day,
                    ln_trans_type_id,
                    lv_currency_cd,
                    lv_country_cd,
                    1,
                    ln_auth_amt,
                    0,
                    null,
                    decode(lv_auth_result_cd, 'N', 1, 'O', 1, 'R', 1, 0),
                    decode(lv_auth_result_cd, 'F', 1, 0),
                    decode(lv_credit_debit_ind, 'C', 1, 0),
                    decode(lv_credit_debit_ind, 'D', 1, 0),
                    decode(lv_prepaid_flag, 'Y', 1, 0)
                from dual;
                EXIT;
            EXCEPTION
                WHEN DUP_VAL_ON_INDEX THEN
                    NULL;
            END;
        END LOOP;

        IF lt_auth_ts > lt_cutoff THEN
          lt_interval_ts := trunc(lt_auth_ts, 'MI');
          ln_interval := mod((extract(hour from lt_interval_ts) * 60) + extract(minute from lt_interval_ts), ln_intervals);

          LOOP
              update pss.auth_stat_recent set
                  auth_count = case when interval_ts < lt_cutoff then 1 else 1 + nvl(auth_count, 0) end,
                  auth_amount = case when interval_ts < lt_cutoff then ln_auth_amt else ln_auth_amt + nvl(auth_amount, 0) end,
                  auth_declined_count = case when interval_ts < lt_cutoff then decode(lv_auth_result_cd, 'N', 1, 'O', 1, 'R', 1, 0) else auth_declined_count + decode(lv_auth_result_cd, 'N', 1, 'O', 1, 'R', 1, 0) end,
                  auth_failed_count = case when interval_ts < lt_cutoff then decode(lv_auth_result_cd, 'F', 1, 0) else auth_failed_count + decode(lv_auth_result_cd, 'F', 1, 0) end,
                  credit_count = case when interval_ts < lt_cutoff then decode(lv_credit_debit_ind, 'C', 1, 0) else credit_count + decode(lv_credit_debit_ind, 'C', 1, 0) end,
                  debit_count = case when interval_ts < lt_cutoff then decode(lv_credit_debit_ind, 'D', 1, 0) else debit_count + decode(lv_credit_debit_ind, 'D', 1, 0) end,
                  prepaid_count = case when interval_ts < lt_cutoff then decode(lv_prepaid_flag, 'Y', 1, 0) else prepaid_count + decode(lv_prepaid_flag, 'Y', 1, 0) end,
                  interval_ts = lt_interval_ts
              where device_name = lv_device_name
                  and interval_num = ln_interval
                  and interval_count = ln_intervals
                  and payment_subtype_class = lv_payment_subtype_class
                  and currency_cd = lv_currency_cd
                  and nvl(consumer_acct_coutry_code,'X') = nvl(lv_country_cd,'X');
              EXIT WHEN SQL%ROWCOUNT > 0;
              BEGIN
                  insert into pss.auth_stat_recent(
                      device_name,
                      interval_num,
                      interval_count,
                      interval_ts,
                      payment_subtype_class,
                      currency_cd,
                      consumer_acct_coutry_code,
                      auth_count,
                      auth_amount,
                      auth_declined_count,
                      auth_failed_count,
                      credit_count,
                      debit_count,
                      prepaid_count)
                  select
                      lv_device_name,
                      ln_interval,
                      ln_intervals,
                      lt_interval_ts,
                      lv_payment_subtype_class,
                      lv_currency_cd,
                      lv_country_cd,
                      1,
                      ln_auth_amt,
                      decode(lv_auth_result_cd, 'N', 1, 'O', 1, 'R', 1, 0),
                      decode(lv_auth_result_cd, 'F', 1, 0),
                      decode(lv_credit_debit_ind, 'C', 1, 0),
                      decode(lv_credit_debit_ind, 'D', 1, 0),
                      decode(lv_prepaid_flag, 'Y', 1, 0)
                  from dual;
                  EXIT;
              EXCEPTION
                  WHEN DUP_VAL_ON_INDEX THEN
                      NULL;
              END;
          END LOOP;
        END IF;

    END;

    PROCEDURE AUTHORIZE_PROMO(
        pn_pos_pta_id PSS.POS_PTA.POS_PTA_ID%TYPE,
        pn_tran_start_time NUMBER,
        pn_global_account_id IN OUT PSS.CONSUMER_ACCT_BASE.GLOBAL_ACCOUNT_ID%TYPE,
        pv_consumer_acct_cd PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD%TYPE,
        pv_currency_cd PSS.CURRENCY.CURRENCY_CD%TYPE,
        pv_allow_partial_auth VARCHAR2,
        pn_requested_amount NUMBER,
        pv_auth_result_cd OUT VARCHAR2,
        pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
        pn_trans_to_free_tran OUT NUMBER,
        pn_approved_auth_amount OUT NUMBER,
        pn_campaign_id OUT REPORT.CAMPAIGN.CAMPAIGN_ID%TYPE,
        pn_nth_vend_free_num OUT NUMBER)
    IS
        ld_tran_start_ts PSS.TRAN.TRAN_START_TS%TYPE := CAST(MILLIS_TO_TIMESTAMP(pn_tran_start_time) AS DATE);
        ln_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE;
    BEGIN
        pv_auth_result_cd := 'N';
        SELECT MAX(NTH_VEND_FREE_NUM), MAX(FREE_VEND_MAX_AMOUNT),MAX(CAMPAIGN_ID), MAX(MINOR_CURRENCY_FACTOR)
        into pn_nth_vend_free_num,pn_approved_auth_amount,pn_campaign_id,ln_minor_currency_factor
          FROM (
        SELECT C.NTH_VEND_FREE_NUM, FREE_VEND_MAX_AMOUNT, CPP.PRIORITY, C.CAMPAIGN_ID,cur.MINOR_CURRENCY_FACTOR
          FROM PSS.CAMPAIGN_POS_PTA CPP
          JOIN REPORT.CAMPAIGN C ON CPP.CAMPAIGN_ID = C.CAMPAIGN_ID
          JOIN PSS.POS_PTA PTA ON CPP.POS_PTA_ID = PTA.POS_PTA_ID
          JOIN PSS.CURRENCY cur ON NVL(pta.CURRENCY_CD, 'USD') = cur.CURRENCY_CD
          JOIN PSS.POS P ON P.POS_ID = PTA.POS_ID
         WHERE CPP.POS_PTA_ID = pn_pos_pta_id
           AND ld_tran_start_ts BETWEEN NVL(CPP.START_DATE, MIN_DATE) AND NVL(CPP.END_DATE, MAX_DATE)
           AND C.CAMPAIGN_TYPE_ID = 4
         ORDER BY CPP.PRIORITY, C.NTH_VEND_FREE_NUM, C.CAMPAIGN_ID DESC)
         WHERE ROWNUM = 1;

         IF pn_campaign_id is null THEN
           RETURN;
         END IF;

         IF pn_approved_auth_amount * ln_minor_currency_factor < pn_requested_amount AND pv_allow_partial_auth ='N' THEN
          pv_auth_result_cd:='R';
          RETURN;
         END IF;

        IF pn_global_account_id IS NOT NULL THEN
            SELECT MAX(CAB.CONSUMER_ACCT_ID)
              INTO pn_consumer_acct_id
              FROM PSS.CONSUMER_ACCT_BASE CAB
             WHERE CAB.GLOBAL_ACCOUNT_ID = pn_global_account_id
               AND CAB.CURRENCY_CD = pv_currency_cd;
        END IF;

        IF pn_consumer_acct_id IS NULL THEN
          IF pn_global_account_id IS NULL THEN
            SELECT PSS.SEQ_GLOBAL_ACCOUNT_ID_BASE.NEXTVAL
            INTO pn_global_account_id
            FROM DUAL;
          END IF;

          SELECT PSS.SEQ_CONSUMER_ACCT_ID.NEXTVAL
          INTO pn_consumer_acct_id
          FROM DUAL;

          INSERT INTO PSS.CONSUMER_ACCT_BASE(CONSUMER_ACCT_ID, GLOBAL_ACCOUNT_ID, GLOBAL_ACCOUNT_INSTANCE, TRUNCATED_CARD_NUMBER, PAYMENT_SUBTYPE_ID, CURRENCY_CD)
          SELECT pn_consumer_acct_id, pn_global_account_id, 0, DBADMIN.MASK_CREDIT_CARD(pv_consumer_acct_cd), payment_subtype_id, pv_currency_cd
          FROM PSS.POS_PTA WHERE POS_PTA_ID=pn_pos_pta_id;
        END IF;


        UPDATE PSS.CAMPAIGN_CONSUMER_ACCT
           SET TRAN_COUNT = tran_count + 1,
               PROMO_TRAN_COUNT = promo_tran_count + 1
         WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id
           AND CAMPAIGN_ID = pn_campaign_id
           AND (TRAN_COUNT + 1) / (PROMO_TRAN_COUNT + 1) >= pn_nth_vend_free_num
        RETURNING 'Y' INTO pv_auth_result_cd;

        SELECT MAX((pn_nth_vend_free_num-1) - MOD(TRAN_COUNT + 1, pn_nth_vend_free_num))
        INTO pn_trans_to_free_tran
        FROM PSS.CAMPAIGN_CONSUMER_ACCT
        WHERE CONSUMER_ACCT_ID = pn_consumer_acct_id
        AND CAMPAIGN_ID = pn_campaign_id;

    END;

    PROCEDURE REFUND_PROMO(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pn_result_cd OUT NUMBER)
    IS
        ln_refund_state_id PSS.REFUND.REFUND_STATE_ID%TYPE;
        ln_parent_tran_id PSS.TRAN.PARENT_TRAN_ID%TYPE;
        ln_refund_count NUMBER;
        ln_campaign_id NUMBER;
        ln_consumer_acct_id NUMBER;
    BEGIN
        pn_result_cd := PKG_CONST.RESULT__SUCCESS;

        SELECT T.PARENT_TRAN_ID
        INTO ln_parent_tran_id
        FROM PSS.TRAN T
        WHERE T.TRAN_ID = pn_tran_id  ;


        SELECT COUNT(1)
        INTO ln_refund_count
        FROM PSS.REFUND R
        JOIN PSS.TRAN T ON R.TRAN_ID = T.TRAN_ID
        WHERE T.PARENT_TRAN_ID = ln_parent_tran_id AND R.REFUND_STATE_ID IN(1);

        IF ln_refund_count > 0 THEN
            RETURN;
        END IF;

        SELECT MAX(TO_NUMBER(A.AUTH_AUTHORITY_REF_CD)), MAX(T.consumer_acct_id)
        INTO ln_campaign_id,ln_consumer_acct_id
        FROM PSS.TRAN T join PSS.AUTH A on T.tran_id=A.tran_id and A.AUTH_TYPE_CD='N'
        WHERE T.TRAN_ID = ln_parent_tran_id  ;

        UPDATE PSS.CAMPAIGN_CONSUMER_ACCT
        SET TRAN_COUNT = CASE WHEN TRAN_COUNT - 1 >= 0 THEN TRAN_COUNT - 1 ELSE 0 END,
            PROMO_TRAN_COUNT = CASE WHEN PROMO_TRAN_COUNT - 1 >= 0 THEN PROMO_TRAN_COUNT - 1 ELSE 0 END
        WHERE CAMPAIGN_ID = ln_campaign_id AND CONSUMER_ACCT_ID = ln_consumer_acct_id;
    END;

END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/releases/REL_Edge_Update_R55/R55.USAPDB.USAT211.1.sql?revision=1.1
ALTER TABLE DEVICE.FILE_TRANSFER
  ADD (STATUS_CD VARCHAR2(1),
  STATUS_CHANGED_BY VARCHAR2(256),
  CONSTRAINT FK_FILE_TRANSFER_STATUS_CD
    FOREIGN KEY (STATUS_CD)
    REFERENCES PSS.STATUS (STATUS_CD))
;
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/PSS/TRAIUD_CONSUMER_ACCT.trg?revision=1.14
CREATE OR REPLACE TRIGGER PSS.TRAIUD_CONSUMER_ACCT 
AFTER INSERT OR UPDATE OR DELETE ON PSS.CONSUMER_ACCT
FOR EACH ROW
BEGIN
	IF :NEW.ALLOW_NEGATIVE_BALANCE = 'Y' AND ((:NEW.CONSUMER_ACCT_TYPE_ID != 3 OR :NEW.CONSUMER_ACCT_SUB_TYPE_ID != 2) AND :NEW.CONSUMER_ACCT_TYPE_ID != 7) THEN
		RAISE_APPLICATION_ERROR(-20208, 'Negative balance can not be enabled for this account type!');
	END IF;

	IF INSERTING
			AND :NEW.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y' AND :NEW.CONSUMER_ACCT_TYPE_ID = 5 AND :NEW.VZM2P_LOYALTY_ENABLED = 'Y'
		OR UPDATING 
			AND (:NEW.CONSUMER_ACCT_ACTIVE_YN_FLAG != :OLD.CONSUMER_ACCT_ACTIVE_YN_FLAG
				OR :NEW.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y'
					AND (:NEW.CONSUMER_ACCT_CD != :OLD.CONSUMER_ACCT_CD
						OR :NEW.CURRENCY_CD != :OLD.CURRENCY_CD
						OR :NEW.CONSUMER_ACCT_TYPE_ID != :OLD.CONSUMER_ACCT_TYPE_ID						
						OR DBADMIN.PKG_UTL.EQL(:NEW.CONSUMER_ACCT_CD_HASH, :OLD.CONSUMER_ACCT_CD_HASH) = 'N'
						OR DBADMIN.PKG_UTL.EQL(:NEW.CONSUMER_ACCT_TOKEN, :OLD.CONSUMER_ACCT_TOKEN) = 'N'
						OR DBADMIN.PKG_UTL.EQL(:NEW.VZM2P_LOYALTY_ENABLED, :OLD.VZM2P_LOYALTY_ENABLED) = 'N'						
						)
				)
		OR DELETING
			AND :OLD.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y' THEN
		ENGINE.PKG_DATA_SYNC.SP_CREATE_DATA_SYNC(
			'CONSUMER_ACCT',
			'PSS.CONSUMER_ACCT',
			CASE WHEN INSERTING THEN 'I' WHEN UPDATING THEN 'U' WHEN DELETING THEN 'D' END,
			COALESCE(:NEW.CONSUMER_ACCT_ID, :OLD.CONSUMER_ACCT_ID),
			COALESCE(:NEW.CONSUMER_ACCT_CD, :OLD.CONSUMER_ACCT_CD)
		);
	END IF;
	
	IF INSERTING AND :NEW.CONSUMER_ACCT_TYPE_ID in (3,6) AND :NEW.CORP_CUSTOMER_ID IS NOT NULL THEN
		INSERT INTO PSS.CAMPAIGN_CONSUMER_ACCT(CONSUMER_ACCT_ID,CAMPAIGN_ID)
		SELECT :NEW.CONSUMER_ACCT_ID, CAMPAIGN_ID FROM REPORT.CAMPAIGN WHERE CUSTOMER_ID=:NEW.CORP_CUSTOMER_ID AND ASSIGNED_TO_ALLCARDS='Y' AND STATUS='A';
	ELSIF UPDATING AND :NEW.CONSUMER_ACCT_TYPE_ID in (3,6) AND :NEW.CORP_CUSTOMER_ID IS NOT NULL AND :OLD.CORP_CUSTOMER_ID IS NOT NULL AND :NEW.CORP_CUSTOMER_ID <> :OLD.CORP_CUSTOMER_ID THEN	
		DELETE FROM PSS.CAMPAIGN_CONSUMER_ACCT 
		WHERE CONSUMER_ACCT_ID=:NEW.CONSUMER_ACCT_ID and CAMPAIGN_ID IN (SELECT CAMPAIGN_ID FROM REPORT.CAMPAIGN WHERE CUSTOMER_ID=:OLD.CORP_CUSTOMER_ID AND ASSIGNED_TO_ALLCARDS='Y');
		INSERT INTO PSS.CAMPAIGN_CONSUMER_ACCT(CONSUMER_ACCT_ID,CAMPAIGN_ID)
		SELECT :NEW.CONSUMER_ACCT_ID, CAMPAIGN_ID FROM REPORT.CAMPAIGN WHERE CUSTOMER_ID=:NEW.CORP_CUSTOMER_ID AND ASSIGNED_TO_ALLCARDS='Y' AND STATUS='A';
	ELSIF DELETING THEN
		DELETE FROM PSS.CAMPAIGN_CONSUMER_ACCT WHERE CONSUMER_ACCT_ID=:NEW.CONSUMER_ACCT_ID; 
	END IF;
	
	IF INSERTING AND :NEW.CONSUMER_ACCT_TYPE_ID in (7) AND :NEW.CORP_CUSTOMER_ID IS NOT NULL THEN
		INSERT INTO PSS.CAMPAIGN_CONSUMER_ACCT(CONSUMER_ACCT_ID,CAMPAIGN_ID)
		SELECT :NEW.CONSUMER_ACCT_ID, CAMPAIGN_ID FROM REPORT.CAMPAIGN WHERE CUSTOMER_ID=:NEW.CORP_CUSTOMER_ID AND ASSIGNED_TO_ALLPAYROLLCARDS='Y' AND STATUS='A';
	ELSIF UPDATING AND :NEW.CONSUMER_ACCT_TYPE_ID in (7) AND :NEW.CORP_CUSTOMER_ID IS NOT NULL AND :OLD.CORP_CUSTOMER_ID IS NOT NULL AND :NEW.CORP_CUSTOMER_ID <> :OLD.CORP_CUSTOMER_ID THEN	
		DELETE FROM PSS.CAMPAIGN_CONSUMER_ACCT 
		WHERE CONSUMER_ACCT_ID=:NEW.CONSUMER_ACCT_ID and CAMPAIGN_ID IN (SELECT CAMPAIGN_ID FROM REPORT.CAMPAIGN WHERE CUSTOMER_ID=:OLD.CORP_CUSTOMER_ID AND ASSIGNED_TO_ALLPAYROLLCARDS='Y');
		INSERT INTO PSS.CAMPAIGN_CONSUMER_ACCT(CONSUMER_ACCT_ID,CAMPAIGN_ID)
		SELECT :NEW.CONSUMER_ACCT_ID, CAMPAIGN_ID FROM REPORT.CAMPAIGN WHERE CUSTOMER_ID=:NEW.CORP_CUSTOMER_ID AND ASSIGNED_TO_ALLPAYROLLCARDS='Y' AND STATUS='A';
	ELSIF DELETING THEN
		DELETE FROM PSS.CAMPAIGN_CONSUMER_ACCT WHERE CONSUMER_ACCT_ID=:NEW.CONSUMER_ACCT_ID; 
	END IF;
	
END;
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/releases/REL_Edge_Update_R55/R55.USAPDB.USAT463.sql?revision=1.1
INSERT INTO DEVICE.FILE_TRANSFER_TYPE(FILE_TRANSFER_TYPE_CD, FILE_TRANSFER_TYPE_NAME, FILE_TRANSFER_TYPE_DESC, DELETABLE, CANCEL_PENDING_TRANSFERS_IND, FIRMWARE_UPDATE_IND)
  VALUES(35, 'Bezel Profile', 'Bezel Profile', 'N', 'N', 'N');
ALTER TABLE DEVICE.FILE_TRANSFER_TYPE ADD FILE_TRANSFER_TYPE_SIZE_LIMIT NUMBER(20,0);
UPDATE DEVICE.FILE_TRANSFER_TYPE SET FILE_TRANSFER_TYPE_SIZE_LIMIT=4096 WHERE FILE_TRANSFER_TYPE_CD IN (34, 35);
COMMIT;

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/releases/REL_Edge_Update_R55/R55.USAPDB.USA415.sql?revision=HEAD
grant select on report.VW_TERMINAL to USAT_PREPAID_APP_ROLE;
grant select on LOCATION.CUSTOMER_MERCHANT to USAT_PREPAID_APP_ROLE;
grant select on PSS.MERCHANT to USAT_PREPAID_APP_ROLE;
grant select on ENGINE.APP_SETTING to USAT_PREPAID_APP_ROLE;
grant update on ENGINE.APP_SETTING to USAT_PREPAID_APP_ROLE;
grant execute on PSS.PKG_CONSUMER_MAINT to USAT_PREPAID_APP_ROLE;
grant select on PSS.CONSUMER to USAT_PREPAID_APP_ROLE;
grant update on PSS.CONSUMER to USAT_PREPAID_APP_ROLE;
grant insert on PSS.CONSUMER_ACCT_BASE to USAT_PREPAID_APP_ROLE;
grant select on PSS.SEQ_GLOBAL_ACCOUNT_ID_BASE to USAT_PREPAID_APP_ROLE;
grant insert on PSS.MERCHANT_CONSUMER_ACCT to USAT_PREPAID_APP_ROLE;
grant select on PSS.MERCHANT_CONSUMER_ACCT to USAT_PREPAID_APP_ROLE;
commit;

-- Add payment type " USAT ISO Card - Prepaid Card (Tokenized) " for virtual device template 
INSERT INTO PSS.POS_PTA_TMPL_ENTRY
  (POS_PTA_TMPL_ID, 
  PAYMENT_SUBTYPE_ID, 
  POS_PTA_ACTIVATION_OSET_HR, 
  PAYMENT_SUBTYPE_KEY_ID, 
  POS_PTA_PRIORITY, 
  AUTHORITY_PAYMENT_MASK_ID, 
  CURRENCY_CD, 
  POS_PTA_PASSTHRU_ALLOW_YN_FLAG, 
  NO_CONVENIENCE_FEE)
SELECT 
	(select dst.POS_PTA_TMPL_ID from device.device_sub_type dst 
    join PSS.POS_PTA_TMPL_ENTRY ppte on dst.POS_PTA_TMPL_ID = ppte.POS_PTA_TMPL_ID 
    where dst.device_type_id = 14 and dst.device_sub_type_id in (1, 2) and ppte.CURRENCY_CD = 'USD'
    group by dst.POS_PTA_TMPL_ID), 
	(select 
      PAYMENT_SUBTYPE_ID
    from pss.payment_subtype 
    where 
      payment_subtype_name = 'USAT ISO Card - Prepaid Card (Tokenized)'),
	0, 
	(SELECT INTERNAL_PAYMENT_TYPE_ID
    FROM PSS.INTERNAL_PAYMENT_TYPE 
  where INTERNAL_PAYMENT_TYPE_DESC = 'Special Card (prepaid)'),
	1, 
	( SELECT 
    AUTHORITY_PAYMENT_MASK_ID 
  FROM PSS.AUTHORITY_PAYMENT_MASK 
  where AUTHORITY_PAYMENT_MASK_DESC = 'USAT Prepaid Card Manual Entry'),
	'USD', 
	'N',
	'Y'
FROM DUAL
WHERE NOT EXISTS (
	select ppte.POS_PTA_TMPL_ENTRY_ID  from device.device_sub_type dst 
    join PSS.POS_PTA_TMPL_ENTRY ppte on dst.POS_PTA_TMPL_ID = ppte.POS_PTA_TMPL_ID 
    join PSS.PAYMENT_SUBTYPE ps on ppte.PAYMENT_SUBTYPE_ID = ps.PAYMENT_SUBTYPE_ID
  where dst.device_type_id = 14 
    and dst.device_sub_type_id in (1, 2) 
    and ppte.CURRENCY_CD = 'USD'
    and ps.PAYMENT_SUBTYPE_NAME = 'USAT ISO Card - Prepaid Card (Tokenized)');
commit;

INSERT INTO PSS.POS_PTA_TMPL_ENTRY
  (POS_PTA_TMPL_ID, 
  PAYMENT_SUBTYPE_ID, 
  POS_PTA_ACTIVATION_OSET_HR, 
  PAYMENT_SUBTYPE_KEY_ID, 
  POS_PTA_PRIORITY, 
  AUTHORITY_PAYMENT_MASK_ID, 
  CURRENCY_CD, 
  POS_PTA_PASSTHRU_ALLOW_YN_FLAG, 
  NO_CONVENIENCE_FEE)
SELECT 
	(select dst.POS_PTA_TMPL_ID from device.device_sub_type dst 
    join PSS.POS_PTA_TMPL_ENTRY ppte on dst.POS_PTA_TMPL_ID = ppte.POS_PTA_TMPL_ID 
    where dst.device_type_id = 14 and dst.device_sub_type_id in (1, 2) and ppte.CURRENCY_CD = 'CAD'
    group by dst.POS_PTA_TMPL_ID), 
	(select 
      PAYMENT_SUBTYPE_ID
    from pss.payment_subtype 
    where 
      payment_subtype_name = 'USAT ISO Card - Prepaid Card (Tokenized)'),
	0, 
	(SELECT INTERNAL_PAYMENT_TYPE_ID
    FROM PSS.INTERNAL_PAYMENT_TYPE 
  where INTERNAL_PAYMENT_TYPE_DESC = 'Special Card (prepaid)'),
	1, 
	( SELECT 
    AUTHORITY_PAYMENT_MASK_ID 
  FROM PSS.AUTHORITY_PAYMENT_MASK 
  where AUTHORITY_PAYMENT_MASK_DESC = 'USAT Prepaid Card Manual Entry'),
	'CAD', 
	'N',
	'Y'
FROM DUAL
WHERE NOT EXISTS (
	select ppte.POS_PTA_TMPL_ENTRY_ID  from device.device_sub_type dst 
    join PSS.POS_PTA_TMPL_ENTRY ppte on dst.POS_PTA_TMPL_ID = ppte.POS_PTA_TMPL_ID 
    join PSS.PAYMENT_SUBTYPE ps on ppte.PAYMENT_SUBTYPE_ID = ps.PAYMENT_SUBTYPE_ID
  where dst.device_type_id = 14 
    and dst.device_sub_type_id in (1, 2) 
    and ppte.CURRENCY_CD = 'CAD'
    and ps.PAYMENT_SUBTYPE_NAME = 'USAT ISO Card - Prepaid Card (Tokenized)');
commit;

-- Add payment type " USAT ISO Card - Prepaid Card (Tokenized) " for all V1 devices 
declare 
   CURSOR l_devices IS  select 
      d.device_id,
      p.POS_ID,
      d.DEVICE_SERIAL_CD
    from DEVICE.DEVICE d join PSS.POS p on d.DEVICE_ID = p.DEVICE_ID
    where REGEXP_LIKE(d.DEVICE_SERIAL_CD, '^V1-.*-USD$|^V1-.*-CAD$') and not EXISTS (
    select 1 from 
      PSS.POS p 
      join PSS.POS_PTA pta on pta.POS_ID = p.POS_ID
      join PSS.PAYMENT_SUBTYPE ps on pta.PAYMENT_SUBTYPE_ID = ps.PAYMENT_SUBTYPE_ID 
      where ps.PAYMENT_SUBTYPE_NAME = 'USAT ISO Card - Prepaid Card (Tokenized)' 
        AND (pta.pos_pta_deactivation_ts IS NULL OR pta.pos_pta_deactivation_ts > SYSDATE)
        and p.DEVICE_ID = d.DEVICE_ID
    )
and d.DEVICE_ACTIVE_YN_FLAG = 'Y';
begin
  for l_device_rec IN l_devices LOOP
    update PSS.POS_PTA set POS_PTA_PRIORITY = POS_PTA_PRIORITY + 1 
    where POS_PTA_ID in (
      select 
        pta.POS_PTA_ID
      from DEVICE.DEVICE d 
        join PSS.POS p on p.DEVICE_ID = d.DEVICE_ID
        join PSS.POS_PTA pta on pta.POS_ID = p.POS_ID
        join PSS.PAYMENT_SUBTYPE ps on pta.PAYMENT_SUBTYPE_ID = ps.PAYMENT_SUBTYPE_ID
        JOIN PSS.CLIENT_PAYMENT_TYPE CPT ON PS.CLIENT_PAYMENT_TYPE_CD = CPT.CLIENT_PAYMENT_TYPE_CD
        JOIN pss.payment_action_type pat ON cpt.payment_action_type_cd = pat.payment_action_type_cd
        JOIN pss.payment_entry_method pem ON cpt.payment_entry_method_cd = pem.payment_entry_method_cd
      where 
        d.DEVICE_ID = l_device_rec.device_id
        and(pta.pos_pta_deactivation_ts IS NULL OR pta.pos_pta_deactivation_ts > SYSDATE)
        and pat.PAYMENT_ACTION_TYPE_DESC = 'Purchase' and  pem.PAYMENT_ENTRY_METHOD_DESC = 'Token'
      );
      
       INSERT INTO pss.pos_pta
      (
        pos_id,
          payment_subtype_key_id,
          --pos_pta_device_serial_cd,
          pos_pta_activation_ts,
          pos_pta_encrypt_key,
          pos_pta_encrypt_key2,
          authority_payment_mask_id,
          pos_pta_pin_req_yn_flag,
          pos_pta_priority,
          merchant_bank_acct_id,
          currency_cd,
          payment_subtype_id,
          pos_pta_passthru_allow_yn_flag,
      pos_pta_pref_auth_amt,
      pos_pta_pref_auth_amt_max,
      pos_pta_disable_debit_denial,
      no_convenience_fee
      )
    SELECT 
      l_device_rec.pos_id,
      ppte.payment_subtype_key_id,
      --CASE WHEN pv_set_terminal_cd_to_serial = 'Y' AND ps.payment_subtype_class != 'Authority::NOP' THEN lv_device_serial_cd ELSE ppte.pos_pta_device_serial_cd END,
      sysdate,
      ppte.pos_pta_encrypt_key,
      ppte.pos_pta_encrypt_key2,
          ppte.authority_payment_mask_id,
          ppte.pos_pta_pin_req_yn_flag,
      1,
      ppte.merchant_bank_acct_id,
      ppte.currency_cd,
      ps.payment_subtype_id,
      ppte.pos_pta_passthru_allow_yn_flag,
      ppte.pos_pta_pref_auth_amt,
      ppte.pos_pta_pref_auth_amt_max,
      ppte.pos_pta_disable_debit_denial,
      ppte.no_convenience_fee
    from device.device_sub_type dst 
      join PSS.POS_PTA_TMPL_ENTRY ppte on dst.POS_PTA_TMPL_ID = ppte.POS_PTA_TMPL_ID 
    JOIN pss.payment_subtype ps ON ps.payment_subtype_id = ppte.payment_subtype_id
    JOIN pss.client_payment_type cpt ON ps.client_payment_type_cd = cpt.client_payment_type_cd
    where dst.device_type_id = 14 
      and dst.device_sub_type_id in (1, 2) 
      and ppte.CURRENCY_CD = case when REGEXP_LIKE(l_device_rec.DEVICE_SERIAL_CD, '^V1-.*-USD$') then 'USD' else 'CAD' end
      and ps.PAYMENT_SUBTYPE_NAME = 'USAT ISO Card - Prepaid Card (Tokenized)';
      commit;
  end loop;
end;




/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/ENGINE/TRBI_MACHINE_CMD_PENDING.trg?revision=1.19
CREATE OR REPLACE TRIGGER engine.trbi_machine_cmd_pending
BEFORE INSERT ON engine.machine_cmd_pending
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
DECLARE
	ln_device_id DEVICE.DEVICE_ID%TYPE;
	ln_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE;
	lv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE;
	lc_comm_method_cd DEVICE.COMM_METHOD_CD%TYPE;
	lv_comm_method_name COMM_METHOD.COMM_METHOD_NAME%TYPE;
	lv_firmware_version DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
	lv_diag_app_version DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
	ln_file_transfer_id FILE_TRANSFER.FILE_TRANSFER_ID%TYPE;
	ln_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE;
	ln_file_transfer_type_cd FILE_TRANSFER_TYPE.FILE_TRANSFER_TYPE_CD%TYPE;
	lv_file_transfer_type_name FILE_TRANSFER_TYPE.FILE_TRANSFER_TYPE_NAME%TYPE;
	ln_file_size NUMERIC;
	ln_max_file_size NUMERIC := 0;
	lv_app_setting_value ENGINE.APP_SETTING.APP_SETTING_VALUE%TYPE;
	lv_new_firmware_version VARCHAR2(20);
	lv_flash_model HOST_EQUIPMENT.HOST_EQUIPMENT_MODEL%TYPE;
BEGIN
	IF :NEW.DATA_TYPE IN('7C', 'A4', 'C7', 'C8') AND :NEW.DEVICE_FILE_TRANSFER_ID IS NULL THEN
		:NEW.DEVICE_FILE_TRANSFER_ID := DBADMIN.TO_NUMBER_OR_NULL(:NEW.COMMAND);
	END IF;

	IF :NEW.DATA_TYPE IN('A4', 'C7', 'C8') THEN
		SELECT MAX(D.DEVICE_ID), MAX(D.DEVICE_TYPE_ID), MAX(D.DEVICE_SERIAL_CD), MAX(D.COMM_METHOD_CD), MAX(CM.COMM_METHOD_NAME), MAX(D.FIRMWARE_VERSION), MAX(DD.DIAG_APP_VERSION), MAX(HE.HOST_EQUIPMENT_MODEL)
		INTO ln_device_id, ln_device_type_id, lv_device_serial_cd, lc_comm_method_cd, lv_comm_method_name, lv_firmware_version, lv_diag_app_version, lv_flash_model
		FROM DEVICE.DEVICE_LAST_ACTIVE DLA
		JOIN DEVICE.DEVICE D ON DLA.DEVICE_ID = D.DEVICE_ID
		LEFT OUTER JOIN DEVICE.DEVICE_DATA DD ON DLA.DEVICE_NAME = DD.DEVICE_NAME
		LEFT OUTER JOIN DEVICE.COMM_METHOD CM ON D.COMM_METHOD_CD = CM.COMM_METHOD_CD
		LEFT OUTER JOIN DEVICE.HOST H ON D.DEVICE_ID = H.DEVICE_ID AND H.HOST_TYPE_ID = 401
		LEFT OUTER JOIN DEVICE.HOST_EQUIPMENT HE ON H.HOST_EQUIPMENT_ID = HE.HOST_EQUIPMENT_ID
		WHERE DLA.DEVICE_NAME = :NEW.MACHINE_ID;
		
		IF ln_device_type_id IN (1, 13) THEN
			SELECT FT.FILE_TRANSFER_ID, FT.FILE_TRANSFER_NAME, FTT.FILE_TRANSFER_TYPE_CD, FTT.FILE_TRANSFER_TYPE_NAME, COALESCE(DBMS_LOB.GETLENGTH(FT.FILE_TRANSFER_CONTENT), 0)
			INTO ln_file_transfer_id, ln_file_transfer_name, ln_file_transfer_type_cd, lv_file_transfer_type_name, ln_file_size
			FROM DEVICE.DEVICE_FILE_TRANSFER DFT
			JOIN DEVICE.FILE_TRANSFER FT ON DFT.FILE_TRANSFER_ID = FT.FILE_TRANSFER_ID
			JOIN DEVICE.FILE_TRANSFER_TYPE FTT ON FT.FILE_TRANSFER_TYPE_CD = FTT.FILE_TRANSFER_TYPE_CD
			WHERE DFT.DEVICE_FILE_TRANSFER_ID = :NEW.DEVICE_FILE_TRANSFER_ID;
			
			IF ln_device_type_id = 1 THEN
				IF ln_file_transfer_type_cd NOT IN (5, 24, 25, 27, 28, 33) THEN
					RAISE_APPLICATION_ERROR(-20208, lv_file_transfer_type_name || ' internal file download is not supported by Gx');
				END IF;
				
				lv_firmware_version := SUBSTR(lv_firmware_version, 9);
				lv_diag_app_version := REGEXP_REPLACE(lv_diag_app_version, 'Diag |Diagnostic = N/A', '');
				
				lv_app_setting_value := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('GX_MIN_FIRMWARE_VERSION_FOR_INTERNAL_FILE_TRANSFERS');
				IF lv_firmware_version IS NOT NULL AND DBADMIN.VERSION_COMPARE(lv_firmware_version, lv_app_setting_value) < 0 THEN
					RAISE_APPLICATION_ERROR(-20208, 'Invalid Gx firmware version: ' || lv_firmware_version || ', ' || lv_app_setting_value || ' or greater is required for internal file downloads');
				END IF;
		
				IF ln_file_transfer_type_cd = 5 THEN --Application Software Upgrade
					lv_app_setting_value := DBADMIN.PKG_GLOBAL.GET_APP_SETTING(CASE lc_comm_method_cd WHEN 'C' THEN 'GX_CDMA' ELSE 'GX' END || '_MIN_DIAGNOSTIC_VERSION_FOR_INTERNAL_FILE_TRANSFER_UPGRADES');
					IF lv_diag_app_version IS NOT NULL AND DBADMIN.VERSION_COMPARE(lv_diag_app_version, lv_app_setting_value) < 0 THEN
						RAISE_APPLICATION_ERROR(-20208, 'Invalid ' || lv_comm_method_name || ' Gx diagnostic app version: ' || lv_diag_app_version || ', ' || lv_app_setting_value || ' or greater is required for ' || lv_file_transfer_type_name);
					END IF;
					
					lv_app_setting_value := DBADMIN.PKG_GLOBAL.GET_APP_SETTING(CASE lc_comm_method_cd WHEN 'C' THEN 'GX_CDMA' ELSE 'GX' END || '_MIN_FIRMWARE_VERSION_FOR_INTERNAL_FILE_TRANSFER_UPGRADES');
					IF lv_firmware_version IS NOT NULL AND DBADMIN.VERSION_COMPARE(lv_firmware_version, lv_app_setting_value) < 0 THEN
						RAISE_APPLICATION_ERROR(-20208, 'Invalid ' || lv_comm_method_name || ' Gx firmware version: ' || lv_firmware_version || ', ' || lv_app_setting_value || ' or greater is required for ' || lv_file_transfer_type_name);
					END IF;
					
					SELECT COALESCE(UTL_RAW.CAST_TO_VARCHAR2(DBMS_LOB.SUBSTR(FILE_TRANSFER_CONTENT, 19, 82)), '-')
					INTO lv_new_firmware_version
					FROM DEVICE.FILE_TRANSFER
					WHERE FILE_TRANSFER_ID = ln_file_transfer_id;
					
					IF lv_new_firmware_version NOT LIKE 'USA-%' AND lv_new_firmware_version NOT LIKE 'Diag%' AND lv_new_firmware_version NOT LIKE 'PTest%' THEN
						RAISE_APPLICATION_ERROR(-20208, ln_file_transfer_name || ' is incorrect ' || lv_file_transfer_type_name || ' file download for Gx');
					END IF;
				ELSIF ln_file_transfer_type_cd IN (27, 28) THEN --Card Reader Application Firmware, Isis SmartTap Configuration
					lv_app_setting_value := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('GX_MIN_FIRMWARE_VERSION_FOR_CARD_READER_UPGRADES');
					IF lv_firmware_version IS NOT NULL AND DBADMIN.VERSION_COMPARE(lv_firmware_version, lv_app_setting_value) < 0 THEN
						RAISE_APPLICATION_ERROR(-20208, 'Invalid Gx firmware version: ' || lv_firmware_version || ', ' || lv_app_setting_value || ' or greater is required for ' || lv_file_transfer_type_name);
					END IF;
				END IF;
			ELSIF ln_device_type_id = 13 THEN
				IF lv_device_serial_cd LIKE 'VJ%' THEN
					IF ln_file_transfer_type_cd NOT IN (5, 16, 19, 24, 25, 27, 28, 31, 33, 34, 35) THEN
						RAISE_APPLICATION_ERROR(-20208, lv_file_transfer_type_name || ' file download is not supported by G9');
					END IF;					
					IF ln_file_transfer_type_cd IN (27, 31) AND lv_firmware_version IS NOT NULL THEN
						lv_app_setting_value := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('G9_MIN_FIRMWARE_VERSION_FOR_CARD_READER_UPGRADES');
						IF DBADMIN.VERSION_COMPARE(lv_firmware_version, lv_app_setting_value) < 0 THEN
							RAISE_APPLICATION_ERROR(-20208, 'Invalid G9 firmware version: ' || lv_firmware_version || ', ' || lv_app_setting_value || ' or greater is required for ' || lv_file_transfer_type_name);
						END IF;
					END IF;
				ELSIF lv_device_serial_cd LIKE 'EE%' THEN
					IF ln_file_transfer_type_cd NOT IN (5, 19) THEN
						RAISE_APPLICATION_ERROR(-20208, lv_file_transfer_type_name || ' file download is not supported by Edge');
					END IF;
				END IF;
				
				IF lv_flash_model = '128Mbit' THEN
					ln_max_file_size := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('G9_MAX_FILE_SIZE_BYTES_FOR_128MBIT_FLASH');
				ELSE
					ln_max_file_size := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('G9_EDGE_DEFAULT_MAX_FILE_SIZE_BYTES');
				END IF;
				
				IF ln_file_size > ln_max_file_size THEN
					RAISE_APPLICATION_ERROR(-20208, 'File size ' || ROUND(ln_file_size / 1024 / 1024, 1) || ' MB is bigger than maximum file size ' || ROUND(ln_max_file_size / 1024 / 1024, 1) || ' MB supported by device');
				END IF;
				
				IF ln_file_transfer_type_cd = 5 THEN --Application Software Upgrade
					SELECT COALESCE(UTL_RAW.CAST_TO_VARCHAR2(DBMS_LOB.SUBSTR(FILE_TRANSFER_CONTENT, 10, 93)), '-')
					INTO lv_new_firmware_version
					FROM DEVICE.FILE_TRANSFER
					WHERE FILE_TRANSFER_ID = ln_file_transfer_id;
					
					IF lv_device_serial_cd LIKE 'VJ%' THEN
						IF NOT REGEXP_LIKE(lv_new_firmware_version, '^2\.0[2-9]\.') THEN
							RAISE_APPLICATION_ERROR(-20208, ln_file_transfer_name || ' is incorrect ' || lv_file_transfer_type_name || ' file download for ' || lv_comm_method_name || ' G9');
						END IF;
						IF lc_comm_method_cd = 'G' THEN
							IF DBADMIN.VERSION_COMPARE(lv_new_firmware_version, '2.02.008i') < 0 THEN
								RAISE_APPLICATION_ERROR(-20208, 'Firmware version 2.02.008i or greater is required for ' || lv_comm_method_name || ' G9');
							END IF;
						END IF;
						IF lv_flash_model = '128Mbit' THEN
							lv_app_setting_value := DBADMIN.PKG_GLOBAL.GET_APP_SETTING('G9_MIN_FIRMWARE_VERSION_FOR_128MBIT_FLASH');
							IF DBADMIN.VERSION_COMPARE(lv_new_firmware_version, lv_app_setting_value) < 0 THEN
								RAISE_APPLICATION_ERROR(-20208, 'Firmware version ' || lv_app_setting_value || ' or greater is required for G9 with ' || lv_flash_model || ' flash');
							END IF;
						END IF;						
					ELSIF lv_device_serial_cd LIKE 'EE%' THEN
						IF lc_comm_method_cd = 'C' THEN
							IF lv_new_firmware_version NOT LIKE '1.02.%' THEN
								RAISE_APPLICATION_ERROR(-20208, ln_file_transfer_name || ' is incorrect ' || lv_file_transfer_type_name || ' file download for ' || lv_comm_method_name || ' Edge');
							END IF;
						ELSIF lc_comm_method_cd = 'G' THEN
							IF lv_new_firmware_version NOT LIKE '1.00.%' AND lv_new_firmware_version NOT LIKE '1.01.%' THEN
								RAISE_APPLICATION_ERROR(-20208, ln_file_transfer_name || ' is incorrect ' || lv_file_transfer_type_name || ' file download for ' || lv_comm_method_name || ' Edge');
							END IF;
						END IF;
					END IF;
				END IF;
			END IF;
		END IF;
	END IF;

   if (:NEW.machine_command_pending_id is null) then
      SELECT SEQ_MACHINE_CMD_PENDING_ID.NEXTVAL
      INTO :NEW.machine_command_pending_id
      FROM dual ;
   end if;
	SELECT	sysdate,
			user,
			sysdate,
			user
	into	:new.created_ts,
			:new.created_by,
			:new.last_updated_ts,
			:new.last_updated_by
	FROM	dual;
END;
/
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/CORP/TRBU_SERVICE_FEES.trg?revision=1.7
CREATE OR REPLACE TRIGGER CORP.TRBU_SERVICE_FEES BEFORE UPDATE ON CORP.SERVICE_FEES
  FOR EACH ROW 
DECLARE
	l_fee_schedule_id NUMBER;
	l_customer_id NUMBER;
BEGIN
    SELECT
        :OLD.CREATED_BY,
        :OLD.CREATED_UTC_TS,
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER
    INTO
        :NEW.CREATED_BY,
        :NEW.CREATED_UTC_TS,
        :NEW.LAST_UPDATED_UTC_TS,
        :NEW.LAST_UPDATED_BY
    FROM DUAL;
    
    IF :NEW.FREQUENCY_ID = 4 OR :NEW.FREQUENCY_ID = 2 THEN
    	IF :NEW.FREQUENCY_ID = 2 THEN
    			select c.fee_schedule_id, c.customer_id into  l_fee_schedule_id, l_customer_id from REPORT.TERMINAL t join corp.customer c on t.customer_id=c.customer_id 
    			where terminal_id=:NEW.TERMINAL_ID;
    	END IF;
    	IF :NEW.START_DATE IS NOT NULL AND DBADMIN.PKG_UTL.EQL(:NEW.START_DATE, :OLD.START_DATE) = 'N' THEN
    		IF NOT(:NEW.FREQUENCY_ID = 2 AND l_fee_schedule_id = 2) THEN 
    			:NEW.START_DATE:=TRUNC(LAST_DAY(:NEW.START_DATE), 'DD');
    		END IF;
    	END IF;
    	
    	IF :NEW.END_DATE IS NOT NULL AND DBADMIN.PKG_UTL.EQL(:NEW.END_DATE, :OLD.END_DATE) = 'N' THEN
    		IF :NEW.FREQUENCY_ID = 2 THEN
    			:NEW.END_DATE:=CORP.GET_SERVICE_FEE_DATE(l_customer_id,:NEW.END_DATE);
    		ELSE
    			IF NOT (:NEW.AUTO_TURNED_OFF_DATE IS NOT NULL AND :OLD.AUTO_TURNED_OFF_DATE IS NULL) THEN
    				:NEW.END_DATE:=TRUNC(LAST_DAY(:NEW.END_DATE), 'DD');
    			END IF;
    		END IF;
    	END IF;
    END IF;
END; 
/

-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/CORP/TRBI_SERVICE_FEES.trg?revision=1.7
CREATE OR REPLACE TRIGGER CORP.TRBI_SERVICE_FEES BEFORE INSERT ON CORP.SERVICE_FEES
  FOR EACH ROW
DECLARE
	l_fee_schedule_id NUMBER;
  l_customer_id NUMBER;
BEGIN
    SELECT 
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER,
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER
    INTO 
        :NEW.CREATED_UTC_TS,
        :NEW.CREATED_BY,
        :NEW.LAST_UPDATED_UTC_TS,
        :NEW.LAST_UPDATED_BY
    FROM DUAL;
    IF :NEW.FREQUENCY_ID = 4 OR :NEW.FREQUENCY_ID = 2 THEN
    	IF :NEW.FREQUENCY_ID = 2 THEN
    			select c.fee_schedule_id, c.customer_id into  l_fee_schedule_id, l_customer_id from REPORT.TERMINAL t join corp.customer c on t.customer_id=c.customer_id 
    			where terminal_id=:NEW.TERMINAL_ID;
    	END IF;
    	IF :NEW.START_DATE IS NOT NULL THEN
    		IF NOT(:NEW.FREQUENCY_ID = 2 AND l_fee_schedule_id = 2) THEN
    			--This is temporary to restart daily fees when they are turned on by job
				IF :NEW.AUTO_TURNED_ON_DATE IS NULL THEN
    				:NEW.START_DATE:=TRUNC(LAST_DAY(:NEW.START_DATE), 'DD');
    			END IF;
    		END IF;
    	END IF;

    	IF :NEW.END_DATE IS NOT NULL THEN
    		IF :NEW.FREQUENCY_ID = 2 THEN
    			:NEW.END_DATE:=CORP.GET_SERVICE_FEE_DATE(l_customer_id,:NEW.END_DATE);
    		ELSE
    			:NEW.END_DATE:=TRUNC(LAST_DAY(:NEW.END_DATE), 'DD');
    		END IF;
    	END IF;
    END IF;
END; 
/

