INSERT INTO DEVICE.ACTION(ACTION_ID, ACTION_NAME, ACTION_PARAM_TYPE_CD, ACTION_PARAM_SIZE, ACTION_CLEAR_PARAMETER_CD)
SELECT 12, 'Technician Card: Read Scheduled DEX with Diagnostic Codes', 'B', 3, null
FROM DUAL
WHERE NOT EXISTS(SELECT 1 FROM DEVICE.ACTION WHERE ACTION_ID = 12);

INSERT INTO DEVICE.ACTION(ACTION_ID, ACTION_NAME, ACTION_PARAM_TYPE_CD, ACTION_PARAM_SIZE, ACTION_CLEAR_PARAMETER_CD)
SELECT 13, 'Technician Card: Show RSSI', 'N', 0, null
FROM DUAL
WHERE NOT EXISTS(SELECT 1 FROM DEVICE.ACTION WHERE ACTION_ID = 13);

INSERT INTO DEVICE.ACTION(ACTION_ID, ACTION_NAME, ACTION_PARAM_TYPE_CD, ACTION_PARAM_SIZE, ACTION_CLEAR_PARAMETER_CD)
SELECT 14, 'Technician Card: Display Service Menu', 'N', 0, null
FROM DUAL
WHERE NOT EXISTS(SELECT 1 FROM DEVICE.ACTION WHERE ACTION_ID = 14);


INSERT INTO DEVICE.DEVICE_TYPE_ACTION(DEVICE_TYPE_ID,ACTION_ID,DEVICE_TYPE_ACTION_CD)
SELECT 13,12,12
FROM DUAL
WHERE NOT EXISTS(SELECT 1 FROM DEVICE.DEVICE_TYPE_ACTION WHERE ACTION_ID = 12 AND DEVICE_TYPE_ID = 13);

INSERT INTO DEVICE.DEVICE_TYPE_ACTION(DEVICE_TYPE_ID,ACTION_ID,DEVICE_TYPE_ACTION_CD)
SELECT 13,13,13
FROM DUAL
WHERE NOT EXISTS(SELECT 1 FROM DEVICE.DEVICE_TYPE_ACTION WHERE ACTION_ID = 13 AND DEVICE_TYPE_ID = 13);

INSERT INTO DEVICE.DEVICE_TYPE_ACTION(DEVICE_TYPE_ID,ACTION_ID,DEVICE_TYPE_ACTION_CD)
SELECT 13,14,14
FROM DUAL
WHERE NOT EXISTS(SELECT 1 FROM DEVICE.DEVICE_TYPE_ACTION WHERE ACTION_ID = 14 AND DEVICE_TYPE_ID = 13);


INSERT INTO DEVICE.ACTION_PARAM(ACTION_PARAM_ID, ACTION_ID, ACTION_PARAM_NAME, ACTION_PARAM_CD, PROTOCOL_BIT_INDEX)
SELECT DEVICE.SEQ_ACTION_PARAM_ID.NEXTVAL, 12, 'Send DEX up to server', 'send', 0
FROM DUAL
WHERE NOT EXISTS(SELECT 1 FROM DEVICE.ACTION_PARAM WHERE ACTION_ID = 12 AND ACTION_PARAM_CD = 'send');

INSERT INTO DEVICE.ACTION_PARAM(ACTION_PARAM_ID, ACTION_ID, ACTION_PARAM_NAME, ACTION_PARAM_CD, PROTOCOL_BIT_INDEX)
SELECT DEVICE.SEQ_ACTION_PARAM_ID.NEXTVAL, 12, 'Display universal diagnostic codes', 'ucodes', 1
FROM DUAL
WHERE NOT EXISTS(SELECT 1 FROM DEVICE.ACTION_PARAM WHERE ACTION_ID = 12 AND ACTION_PARAM_CD = 'ucodes');

INSERT INTO DEVICE.ACTION_PARAM(ACTION_PARAM_ID, ACTION_ID, ACTION_PARAM_NAME, ACTION_PARAM_CD, PROTOCOL_BIT_INDEX)
SELECT DEVICE.SEQ_ACTION_PARAM_ID.NEXTVAL, 12, 'Perform RSSI reading before reading DEX', 'rssi', 2
FROM DUAL
WHERE NOT EXISTS(SELECT 1 FROM DEVICE.ACTION_PARAM WHERE ACTION_ID = 12 AND ACTION_PARAM_CD = 'rssi');

INSERT INTO DEVICE.ACTION_PARAM(ACTION_PARAM_ID, ACTION_ID, ACTION_PARAM_NAME, ACTION_PARAM_CD, PROTOCOL_BIT_INDEX)
SELECT DEVICE.SEQ_ACTION_PARAM_ID.NEXTVAL, 11, 'Perform RSSI reading before reading DEX', 'rssi', 2
FROM DUAL
WHERE NOT EXISTS(SELECT 1 FROM DEVICE.ACTION_PARAM WHERE ACTION_ID = 11 AND ACTION_PARAM_CD = 'rssi');

COMMIT;
