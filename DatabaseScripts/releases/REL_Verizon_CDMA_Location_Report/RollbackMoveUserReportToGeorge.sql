DECLARE
    l_report_id NUMBER;
BEGIN
	
	select report_id into l_report_id from report.reports where report_name='CDMA Device Locations';
	update report.reports set user_id=7 where report_id=l_report_id;
	update report.user_report set user_id=7 where report_id=l_report_id and status='A';
	commit;
END;