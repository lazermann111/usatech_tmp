DECLARE
    l_user_report_id NUMBER;
    l_ccs_transport_id NUMBER;
    l_report_id NUMBER;
    l_user_id NUMBER := 7;
BEGIN
	
	select report_id into l_report_id from report.reports where report_name='CDMA Device Locations';

	-- make report available
	select report.user_report_seq.nextval into l_user_report_id from dual;
    select report.ccs_transport_seq.nextval into l_ccs_transport_id from dual;
    insert into report.ccs_transport (ccs_transport_id, ccs_transport_type_id, ccs_transport_name)
    values(l_ccs_transport_id, 2, 'Transport for User Report Id = '||l_user_report_id);
    insert into report.ccs_transport_property( ccs_transport_property_id, ccs_transport_id, ccs_transport_property_type_id,ccs_transport_property_value)
	values( report.ccs_transport_property_seq.nextval, l_ccs_transport_id, 3, 'devmst05.usatech.com');
    insert into report.ccs_transport_property( ccs_transport_property_id, ccs_transport_id, ccs_transport_property_type_id,ccs_transport_property_value)
	values( report.ccs_transport_property_seq.nextval, l_ccs_transport_id, 4, '22');
    insert into report.ccs_transport_property( ccs_transport_property_id, ccs_transport_id, ccs_transport_property_type_id,ccs_transport_property_value)
	values( report.ccs_transport_property_seq.nextval, l_ccs_transport_id, 5, 'ultest');
    insert into report.ccs_transport_property( ccs_transport_property_id, ccs_transport_id, ccs_transport_property_type_id,ccs_transport_property_value)
	values( report.ccs_transport_property_seq.nextval, l_ccs_transport_id, 6, 'Usalive1');
	--insert into report.ccs_transport_property( ccs_transport_property_id, ccs_transport_id, ccs_transport_property_type_id,ccs_transport_property_value)
	--values( report.ccs_transport_property_seq.nextval, l_ccs_transport_id, 7, 'path');

    insert into report.user_report (user_report_id, report_id, user_id, status, ccs_transport_id, frequency_id)
    values(l_user_report_id, l_report_id, l_user_id, 'A', l_ccs_transport_id, 4);-- frequency id 4 is daily 
    -- this will control when the first time the report will start being generated.
    update report.user_report set upd_dt = to_date('05-02-2012 00:00:00','MM-DD-YYYY HH24:MI:SS') where user_report_id=l_user_report_id;

commit;
END;