DECLARE
    l_report_id NUMBER;
    l_user_report_id NUMBER;
    l_ccs_transport_id NUMBER;
    l_user_id NUMBER := 7;
BEGIN

    select report_id into l_report_id from report.reports where report_name='CDMA Device Locations';

    -- delete the user report for cdma devices report
    delete from report.user_report where report_id=l_report_id and user_id=l_user_id;
  
    delete from report.report_param where report_id=l_report_id;
    delete from report.reports where report_id=l_report_id;
    commit;
    
    revoke select on REPORT.VW_CURRENT_DEVICE_LOCATION from USAT_RPT_GEN_ROLE;

END;