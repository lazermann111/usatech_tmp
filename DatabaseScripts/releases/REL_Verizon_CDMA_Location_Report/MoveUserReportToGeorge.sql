DECLARE
    l_user_id NUMBER;
    l_report_id NUMBER;
BEGIN
	
	select user_id into l_user_id from report.user_login where user_name like 'gharrum@usatech.com';
	select report_id into l_report_id from report.reports where report_name='CDMA Device Locations';
	update report.reports set user_id=l_user_id where report_id=l_report_id;
	update report.user_report set user_id=l_user_id where report_id=l_report_id and status='A';
	commit;
END;