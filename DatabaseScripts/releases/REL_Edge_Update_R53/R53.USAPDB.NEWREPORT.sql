update report.report_param set param_value='SELECT CUS.CUSTOMER_NAME "Operator Name", R.REGION_NAME "Region Name", C.CONSUMER_FNAME "First Name", C.CONSUMER_LNAME "Last Name", CA.CONSUMER_ACCT_IDENTIFIER "Card Id",
CA.CONSUMER_ACCT_BALANCE "Account Balance", CA.NICK_NAME "Card Nickname", CA.CONSUMER_ACCT_CD "Card Number", C.CONSUMER_EMAIL_ADDR1 "Email Address", C.CONSUMER_CELL_PHONE_NUM "Mobile Phone Number",
CA.CONSUMER_ACCT_ACTIVE_YN_FLAG "Account Enabled", DBADMIN.UTC_TO_LOCAL_DATE(CC.CREATED_UTC_TS) "Registration Time", CC.CREDENTIAL_ACTIVE_FLAG "Account Activated",
CC.ACTIVATED_TS "Activation Time", CAS.CONSUMER_ACCT_SUB_TYPE_DESC "Account Sub Type", CA.CONSUMER_ACCT_PROMO_TOTAL "Promo Total",
CA.CONSUMER_ACCT_REPLENISH_TOTAL "Replenish Total", CA.REPLENISH_BONUS_TOTAL "Replenish Bonus Total",
CA.LOYALTY_DISCOUNT_TOTAL "Loyalty Discount Total", CA.CASH_BACK_TOTAL "Cashback Total",
RT.REPLENISH_TYPE_DESC "Replenish Type", CAR.REPLENISH_AMOUNT "Replenish Amount", CAR.REPLENISH_THRESHHOLD "Replenish Threshold",
CAR.REPLENISH_CARD_MASKED "Replenish Card", CAR.LAST_REPLENISH_TRAN_TS "Last Replenish Time",
CA.CONSUMER_ACCT_REPLENISH_TOTAL+CA.CONSUMER_ACCT_PROMO_TOTAL-CA.CONSUMER_ACCT_BALANCE "Total Tran Amount",
CA.TRAN_COUNT_TOTAL "Total Tran Count",
CA.PURCHASE_DISCOUNT_TOTAL "Purchase Discount Total"
FROM PSS.CONSUMER C
JOIN PSS.CONSUMER_ACCT CA ON C.CONSUMER_ID = CA.CONSUMER_ID
JOIN PSS.CONSUMER_ACCT_SUB_TYPE CAS ON CA.CONSUMER_ACCT_SUB_TYPE_ID = CAS.CONSUMER_ACCT_SUB_TYPE_ID
JOIN PSS.CONSUMER_CREDENTIAL CC ON C.CONSUMER_ID = CC.CONSUMER_ID
JOIN CORP.CUSTOMER CUS ON CA.CORP_CUSTOMER_ID = CUS.CUSTOMER_ID
LEFT OUTER JOIN REPORT.REGION R ON C.REGION_ID = R.REGION_ID
LEFT OUTER JOIN PSS.CONSUMER_ACCT_REPLENISH CAR ON CA.CONSUMER_ACCT_REPLENISH_ID = CAR.CONSUMER_ACCT_REPLENISH_ID
LEFT OUTER JOIN PSS.REPLENISH_TYPE RT ON CAR.REPLENISH_TYPE_ID = RT.REPLENISH_TYPE_ID
JOIN REPORT.VW_USER_CONSUMER_ACCT VUCA ON VUCA.CONSUMER_ACCT_ID=CA.CONSUMER_ACCT_ID
JOIN REPORT.USER_LOGIN UL ON UL.USER_ID=VUCA.USER_ID
WHERE CA.CONSUMER_ACCT_TYPE_ID = 3
AND UL.USER_ID = ?
ORDER BY CUS.CUSTOMER_NAME, CA.CONSUMER_ACCT_BALANCE, C.CONSUMER_FNAME, C.CONSUMER_LNAME'
where param_name='query' and report_id=(select report_id from report.reports where report_name='MORE Accounts');


update report.report_param set param_value='SELECT * FROM (
SELECT CUS.CUSTOMER_NAME "Operator Name", R.REGION_NAME "Region Name", C.CONSUMER_FNAME "First Name", C.CONSUMER_LNAME "Last Name", CA.CONSUMER_ACCT_IDENTIFIER "Card Id",
CA.CONSUMER_ACCT_BALANCE "Account Balance", CA.NICK_NAME "Card Nickname", CA.CONSUMER_ACCT_CD "Card Number", C.CONSUMER_EMAIL_ADDR1 "Email Address", C.CONSUMER_CELL_PHONE_NUM "Mobile Phone Number",
CA.CONSUMER_ACCT_ACTIVE_YN_FLAG "Account Enabled", DBADMIN.UTC_TO_LOCAL_DATE(CC.CREATED_UTC_TS) "Registration Time", CC.CREDENTIAL_ACTIVE_FLAG "Account Activated",
CC.ACTIVATED_TS "Activation Time", CAS.CONSUMER_ACCT_SUB_TYPE_DESC "Account Sub Type", CA.CONSUMER_ACCT_PROMO_TOTAL "Promo Total",
CA.CONSUMER_ACCT_REPLENISH_TOTAL "Replenish Total", CA.REPLENISH_BONUS_TOTAL "Replenish Bonus Total",
CA.LOYALTY_DISCOUNT_TOTAL "Loyalty Discount Total", CA.CASH_BACK_TOTAL "Cashback Total",
RT.REPLENISH_TYPE_DESC "Replenish Type", CAR.REPLENISH_AMOUNT "Replenish Amount", CAR.REPLENISH_THRESHHOLD "Replenish Threshold",
CAR.REPLENISH_CARD_MASKED "Replenish Card", CAR.LAST_REPLENISH_TRAN_TS "Last Replenish Time",
CA.CONSUMER_ACCT_REPLENISH_TOTAL+CA.CONSUMER_ACCT_PROMO_TOTAL-CA.CONSUMER_ACCT_BALANCE "Total Tran Amount",
CA.TRAN_COUNT_TOTAL "Total Tran Count",
(SELECT DBADMIN.UTC_TO_LOCAL_DATE(MAX(CAD.LAST_USED_UTC_TS)) FROM PSS.CONSUMER_ACCT_DEVICE CAD WHERE CAD.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID) "Last Activity Time",
CA.PURCHASE_DISCOUNT_TOTAL "Purchase Discount Total"
FROM PSS.CONSUMER C
JOIN PSS.CONSUMER_ACCT CA ON C.CONSUMER_ID = CA.CONSUMER_ID
JOIN PSS.CONSUMER_ACCT_SUB_TYPE CAS ON CA.CONSUMER_ACCT_SUB_TYPE_ID = CAS.CONSUMER_ACCT_SUB_TYPE_ID
JOIN PSS.CONSUMER_CREDENTIAL CC ON C.CONSUMER_ID = CC.CONSUMER_ID
JOIN CORP.CUSTOMER CUS ON CA.CORP_CUSTOMER_ID = CUS.CUSTOMER_ID
LEFT OUTER JOIN REPORT.REGION R ON C.REGION_ID = R.REGION_ID
LEFT OUTER JOIN PSS.CONSUMER_ACCT_REPLENISH CAR ON CA.CONSUMER_ACCT_REPLENISH_ID = CAR.CONSUMER_ACCT_REPLENISH_ID
LEFT OUTER JOIN PSS.REPLENISH_TYPE RT ON CAR.REPLENISH_TYPE_ID = RT.REPLENISH_TYPE_ID
JOIN REPORT.VW_USER_CONSUMER_ACCT VUCA ON VUCA.CONSUMER_ACCT_ID=CA.CONSUMER_ACCT_ID
JOIN REPORT.USER_LOGIN UL ON UL.USER_ID=VUCA.USER_ID
WHERE CA.CONSUMER_ACCT_TYPE_ID = 3
AND UL.USER_ID = ?
) WHERE ("Last Activity Time" < CAST(? AS DATE) OR ("Last Activity Time" IS NULL AND ? = ''Y''))
ORDER BY "Operator Name", "Last Activity Time", "Account Balance", "First Name", "Last Name"'
where param_name='query' and report_id=(select report_id from report.reports where report_name='Inactive MORE Accounts');


update report.report_param set param_value='SELECT CUS.CUSTOMER_NAME "Operator Name", R.REGION_NAME "Region Name", C.CONSUMER_FNAME "First Name", C.CONSUMER_LNAME "Last Name", CA.CONSUMER_ACCT_IDENTIFIER "Card Id",
CA.CONSUMER_ACCT_BALANCE "Account Balance", CA.NICK_NAME "Card Nickname", CA.CONSUMER_ACCT_CD "Card Number", C.CONSUMER_EMAIL_ADDR1 "Email Address", C.CONSUMER_CELL_PHONE_NUM "Mobile Phone Number",
CA.CONSUMER_ACCT_ACTIVE_YN_FLAG "Account Enabled", DBADMIN.UTC_TO_LOCAL_DATE(CC.CREATED_UTC_TS) "Registration Time", CC.CREDENTIAL_ACTIVE_FLAG "Account Activated",
CC.ACTIVATED_TS "Activation Time", CAS.CONSUMER_ACCT_SUB_TYPE_DESC "Account Sub Type", CA.CONSUMER_ACCT_PROMO_TOTAL "Promo Total",
CA.CONSUMER_ACCT_REPLENISH_TOTAL "Replenish Total", CA.REPLENISH_BONUS_TOTAL "Replenish Bonus Total",
CA.LOYALTY_DISCOUNT_TOTAL "Loyalty Discount Total", CA.CASH_BACK_TOTAL "Cashback Total",
RT.REPLENISH_TYPE_DESC "Replenish Type", CAR.REPLENISH_AMOUNT "Replenish Amount", CAR.REPLENISH_THRESHHOLD "Replenish Threshold",
CAR.REPLENISH_CARD_MASKED "Replenish Card", CAR.LAST_REPLENISH_TRAN_TS "Last Replenish Time",
CA.CONSUMER_ACCT_REPLENISH_TOTAL+CA.CONSUMER_ACCT_PROMO_TOTAL-CA.CONSUMER_ACCT_BALANCE "Total Tran Amount",
CA.TRAN_COUNT_TOTAL "Total Tran Count",
CA.PURCHASE_DISCOUNT_TOTAL "Purchase Discount Total"
FROM PSS.CONSUMER C
JOIN PSS.CONSUMER_ACCT CA ON C.CONSUMER_ID = CA.CONSUMER_ID
JOIN PSS.CONSUMER_ACCT_SUB_TYPE CAS ON CA.CONSUMER_ACCT_SUB_TYPE_ID = CAS.CONSUMER_ACCT_SUB_TYPE_ID
JOIN PSS.CONSUMER_CREDENTIAL CC ON C.CONSUMER_ID = CC.CONSUMER_ID
JOIN CORP.CUSTOMER CUS ON CA.CORP_CUSTOMER_ID = CUS.CUSTOMER_ID
LEFT OUTER JOIN REPORT.REGION R ON C.REGION_ID = R.REGION_ID
LEFT OUTER JOIN PSS.CONSUMER_ACCT_REPLENISH CAR ON CA.CONSUMER_ACCT_REPLENISH_ID = CAR.CONSUMER_ACCT_REPLENISH_ID
LEFT OUTER JOIN PSS.REPLENISH_TYPE RT ON CAR.REPLENISH_TYPE_ID = RT.REPLENISH_TYPE_ID
JOIN REPORT.VW_USER_CONSUMER_ACCT VUCA ON VUCA.CONSUMER_ACCT_ID=CA.CONSUMER_ACCT_ID
JOIN REPORT.USER_LOGIN UL ON UL.USER_ID = VUCA.USER_ID
WHERE CA.CONSUMER_ACCT_TYPE_ID = 3 AND DBADMIN.UTC_TO_LOCAL_DATE(CC.CREATED_UTC_TS) >= CAST(? AS DATE) AND DBADMIN.UTC_TO_LOCAL_DATE(CC.CREATED_UTC_TS) < CAST(? AS DATE) + 1
AND UL.USER_ID = ?
ORDER BY CUS.CUSTOMER_NAME, C.CONSUMER_FNAME, C.CONSUMER_LNAME'
where param_name='query' and report_id=(select report_id from report.reports where report_name='MORE Activations');


update report.report_param set param_value='SELECT
bt.BUSINESS_TYPE_NAME "Business Type",
l.LOCATION_NAME "Location",
r.REGION_NAME "Region",
SUM(CASE WHEN tsbd.trans_type_id =30 THEN tsbd.TRAN_COUNT ELSE NULL END) "Tran Counts",
round(SUM(CASE WHEN tsbd.trans_type_id =30 THEN tsbd.TRAN_AMOUNT ELSE NULL END),2) "Tran Amounts",
round(SUM(CASE WHEN tsbd.trans_type_id =30 THEN tsbd.TRAN_AMOUNT ELSE NULL END)/SUM(CASE WHEN tsbd.trans_type_id =30 THEN tsbd.TRAN_COUNT ELSE NULL END),2) "Average Amount/Tran",
SUM(NVL(tsbd.LOYALTY_DISCOUNT,0)) "Loyalty Discounts",
SUM(NVL(tsbd.PURCHASE_DISCOUNT,0)) "Purchase Discounts",
round((SUM(CASE WHEN tsbd.trans_type_id =30 THEN tsbd.TRAN_COUNT ELSE NULL END)/(cast( ? as date)-cast( ? as date)))*7,2) "Average Tran Counts/Week",
round((SUM(CASE WHEN tsbd.trans_type_id =30 THEN tsbd.TRAN_AMOUNT ELSE NULL END)/(cast( ? as date)-cast( ? as date)))*7,2) "Average Tran Amounts/Week",
SUM(CASE WHEN tsbd.trans_type_id =31 THEN tsbd.TRAN_COUNT ELSE NULL END) "Refund Counts",
round(SUM(CASE WHEN tsbd.trans_type_id =31 THEN tsbd.TRAN_AMOUNT ELSE NULL END),2) "Refund Amounts"
FROM REPORT.TRANS_STAT_BY_DAY tsbd
JOIN REPORT.EPORT e ON tsbd.EPORT_ID = e.EPORT_ID
JOIN REPORT.TERMINAL t ON tsbd.TERMINAL_ID = t.TERMINAL_ID AND t.STATUS != ''D''
JOIN CORP.CUSTOMER c_1 ON t.CUSTOMER_ID = c_1.CUSTOMER_ID
LEFT OUTER JOIN REPORT.MACHINE m ON t.MACHINE_ID = m.MACHINE_ID
LEFT OUTER JOIN REPORT.BUSINESS_TYPE bt ON t.BUSINESS_TYPE_ID = bt.BUSINESS_TYPE_ID
LEFT OUTER JOIN REPORT.TERMINAL_REGION tr ON t.TERMINAL_ID = tr.TERMINAL_ID
LEFT OUTER JOIN REPORT.REGION r ON tr.REGION_ID = r.REGION_ID
JOIN REPORT.LOCATION l ON t.LOCATION_ID = l.LOCATION_ID
JOIN REPORT.VW_USER_TERMINAL UT ON T.TERMINAL_ID = UT.TERMINAL_ID AND UT.USER_ID=?
where tsbd.TRAN_DATE >= TRUNC(cast( ?  as date), ''DD'') AND tsbd.TRAN_DATE < 1 + TRUNC(cast( ? as date), ''DD'')
AND tsbd.trans_type_id in (30,31)
AND (? is NULL or r.REGION_ID MEMBER OF CAST(? AS DBADMIN.NUMBER_TABLE) )
GROUP BY bt.BUSINESS_TYPE_NAME, l.LOCATION_NAME, r.REGION_NAME
ORDER BY bt.BUSINESS_TYPE_NAME, l.LOCATION_NAME, r.REGION_NAME'
where param_name='query' and report_id=(select report_id from report.reports where report_name='MORE Consumer Activity');

update report.report_param set param_value='SELECT C.CONSUMER_FNAME "First Name", C.CONSUMER_LNAME "Last Name", CA.NICK_NAME "Card Nickname", CA.CONSUMER_ACCT_CD "Card Number", C.CONSUMER_EMAIL_ADDR1 
"Email Address", C.CONSUMER_CELL_PHONE_NUM "Mobile Phone Number",
CA.CONSUMER_ACCT_ACTIVE_YN_FLAG "Account Enabled", DBADMIN.UTC_TO_LOCAL_DATE(CC.CREATED_UTC_TS) "Registration Time", CC.CREDENTIAL_ACTIVE_FLAG "Account 
Activated", 
CC.ACTIVATED_TS "Activation Time", CAS.CONSUMER_ACCT_SUB_TYPE_DESC "Account Sub Type",
ABS(SUM(CAT.LOYALTY_DISCOUNT)) "Loyalty Discount Total",
SUM(CAT.TRAN_AMOUNT) "Total Tran Amount",
SUM(CAT.TRAN_COUNT) "Total Tran Count",
ABS(SUM(CAT.PURCHASE_DISCOUNT)) "Purchase Discount Total"
FROM PSS.CONSUMER C
JOIN PSS.CONSUMER_ACCT CA ON C.CONSUMER_ID = CA.CONSUMER_ID
JOIN PSS.CAMPAIGN_CONSUMER_ACCT CCA on CA.CONSUMER_ACCT_ID=CCA.CONSUMER_ACCT_ID
JOIN PSS.CONSUMER_ACCT_SUB_TYPE CAS ON CA.CONSUMER_ACCT_SUB_TYPE_ID = CAS.CONSUMER_ACCT_SUB_TYPE_ID
JOIN PSS.CONSUMER_CREDENTIAL CC ON C.CONSUMER_ID = CC.CONSUMER_ID
JOIN REPORT.VW_USER_CONSUMER_ACCT VUCA ON VUCA.CONSUMER_ACCT_ID=CA.CONSUMER_ACCT_ID
JOIN REPORT.USER_LOGIN UL ON UL.USER_ID=VUCA.USER_ID 
JOIN REPORT.CAMPAIGN CAM ON CCA.CAMPAIGN_ID=CAM.CAMPAIGN_ID and (UL.CUSTOMER_ID = 0 OR UL.CUSTOMER_ID = CAM.CUSTOMER_ID)
LEFT OUTER JOIN REPORT.CONSUMER_ACCT_STAT CAT ON CA.CONSUMER_ACCT_ID=CAT.CONSUMER_ACCT_ID and CAT.CAMPAIGN_ID=CAM.CAMPAIGN_ID
WHERE CA.CONSUMER_ACCT_TYPE_ID = 5 
AND UL.USER_ID = ? GROUP BY C.CONSUMER_FNAME, C.CONSUMER_LNAME, CA.NICK_NAME, CA.CONSUMER_ACCT_CD, C.CONSUMER_EMAIL_ADDR1, C.CONSUMER_CELL_PHONE_NUM, CA.CONSUMER_ACCT_ACTIVE_YN_FLAG, DBADMIN.UTC_TO_LOCAL_DATE(CC.CREATED_UTC_TS), CC.CREDENTIAL_ACTIVE_FLAG, CC.ACTIVATED_TS, CAS.CONSUMER_ACCT_SUB_TYPE_DESC
ORDER BY C.CONSUMER_FNAME, C.CONSUMER_LNAME'
where param_name='query' and report_id=(select report_id from report.reports where report_name='MORE Credit Accounts');


update report.report_param set param_value='SELECT CUS.CUSTOMER_NAME "Operator Name", R.REGION_NAME "Region Name", C.CONSUMER_FNAME "First Name", C.CONSUMER_LNAME "Last Name", CA.CONSUMER_ACCT_IDENTIFIER "Card Id",
CA.CONSUMER_ACCT_BALANCE "Account Balance", CA.NICK_NAME "Card Nickname", CA.CONSUMER_ACCT_CD "Card Number", C.CONSUMER_EMAIL_ADDR1 "Email Address", C.CONSUMER_CELL_PHONE_NUM "Mobile Phone Number",
CA.CONSUMER_ACCT_ACTIVE_YN_FLAG "Account Enabled", DBADMIN.UTC_TO_LOCAL_DATE(CC.CREATED_UTC_TS) "Registration Time", CC.CREDENTIAL_ACTIVE_FLAG "Account Activated",
CC.ACTIVATED_TS "Activation Time", CAS.CONSUMER_ACCT_SUB_TYPE_DESC "Account Sub Type", CA.CONSUMER_ACCT_PROMO_TOTAL "Promo Total",
CA.CONSUMER_ACCT_REPLENISH_TOTAL "Replenish Total", CA.REPLENISH_BONUS_TOTAL "Replenish Bonus Total",
CA.LOYALTY_DISCOUNT_TOTAL "Loyalty Discount Total", CA.CASH_BACK_TOTAL "Cashback Total",
RT.REPLENISH_TYPE_DESC "Replenish Type", CAR.REPLENISH_AMOUNT "Replenish Amount", CAR.REPLENISH_THRESHHOLD "Replenish Threshold",
CAR.REPLENISH_CARD_MASKED "Replenish Card", CAR.LAST_REPLENISH_TRAN_TS "Last Replenish Time",
CA.CONSUMER_ACCT_REPLENISH_TOTAL+CA.CONSUMER_ACCT_PROMO_TOTAL-CA.CONSUMER_ACCT_BALANCE "Total Tran Amount",
CA.TRAN_COUNT_TOTAL "Total Tran Count",
CA.PURCHASE_DISCOUNT_TOTAL "Purchase Discount Total"
FROM PSS.CONSUMER C
JOIN PSS.CONSUMER_ACCT CA ON C.CONSUMER_ID = CA.CONSUMER_ID
JOIN PSS.CONSUMER_ACCT_SUB_TYPE CAS ON CA.CONSUMER_ACCT_SUB_TYPE_ID = CAS.CONSUMER_ACCT_SUB_TYPE_ID
LEFT OUTER JOIN PSS.CONSUMER_CREDENTIAL CC ON C.CONSUMER_ID = CC.CONSUMER_ID
JOIN CORP.CUSTOMER CUS ON CA.CORP_CUSTOMER_ID = CUS.CUSTOMER_ID
LEFT OUTER JOIN REPORT.REGION R ON C.REGION_ID = R.REGION_ID
LEFT OUTER JOIN PSS.CONSUMER_ACCT_REPLENISH CAR ON CA.CONSUMER_ACCT_REPLENISH_ID = CAR.CONSUMER_ACCT_REPLENISH_ID
LEFT OUTER JOIN PSS.REPLENISH_TYPE RT ON CAR.REPLENISH_TYPE_ID = RT.REPLENISH_TYPE_ID
JOIN REPORT.VW_USER_CONSUMER_ACCT VUCA ON VUCA.CONSUMER_ACCT_ID=CA.CONSUMER_ACCT_ID
JOIN REPORT.USER_LOGIN UL ON UL.USER_ID=VUCA.USER_ID
WHERE CA.CONSUMER_ACCT_TYPE_ID = 3
AND UL.USER_ID =?
ORDER BY CUS.CUSTOMER_NAME, CC.CREATED_UTC_TS,CA.CONSUMER_ACCT_BALANCE, C.CONSUMER_FNAME, C.CONSUMER_LNAME'
where param_name='query' and report_id=(select report_id from report.reports where report_name='All MORE Accounts');
commit;

-- new field for SalesRollup folioId=969
INSERT INTO FOLIO_CONF.FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
 VALUES(9609, 'Purchase Discount','','NVL(REPORT.TRANS_STAT_BY_DAY.PURCHASE_DISCOUNT,0)','NVL(REPORT.TRANS_STAT_BY_DAY.PURCHASE_DISCOUNT,0)','','NUMERIC','NUMERIC',50,497,'Y');
 
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 1,9609,NULL);                                                                                                                   
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 2,9609,NULL);                                                                                                                   
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 3,9609,NULL);                                                                                                                   
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 4,9609,1);                                                                                                                      
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 5,9609,1);                                                                                                                      
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 8,9609,1); 
 commit;
 
INSERT INTO FOLIO_CONF.FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
 VALUES(9611, 'Purchase Discount','','REPORT.ACTIVITY_REF[CONVENIENCE_FEE].PURCHASE_DISCOUNT','REPORT.ACTIVITY_REF[CONVENIENCE_FEE].PURCHASE_DISCOUNT','','NUMERIC','NUMERIC',50,63,'Y');
 
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 1,9611,NULL);                                                                                                                   
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 2,9611,NULL);                                                                                                                   
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 3,9611,NULL);                                                                                                                   
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 4,9611,1);                                                                                                                      
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 5,9611,1);                                                                                                                      
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 8,9611,1); 
 commit;
 
 
ALTER SESSION SET CURRENT_SCHEMA = REPORT;    
DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := -1;
  LN_WEB_LINK_ID NUMBER;
BEGIN
	SELECT NVL(MAX(REPORT_ID),-1) INTO LN_REPORT_ID
	FROM REPORT.REPORTS WHERE REPORT_NAME = 'Refund And Chargeback';
	
	IF LN_REPORT_ID > 0 THEN
		RETURN;
	ELSE
		LN_REPORT_ID:=467;
	END IF;

	
	INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
	VALUES( LN_REPORT_ID, 'Refund And Chargeback {b} to {e}', 6, 0, 'Refund And Chargeback', 'Report shows refund and chargeback details.', 'N', 0);

	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
	VALUES(LN_REPORT_ID, 'query', 'SELECT 
    CUS.CUSTOMER_NAME "Customer",
    upper(ar.TRANS_TYPE_NAME) "Trans Type Name",
    c.CURRENCY_NAME "Currency",
    REPORT.MASK_CARD(ar.CARD_NUMBER, ar.TRANS_TYPE_ID) "Card Number",
    e.EPORT_SERIAL_NUM "Device",
    ar_1.CC_APPR_CODE "CC APPR Code",
    ar.TRAN_DATE "Tran Date",
    SUM(ar_1.TOTAL_AMOUNT) "Original Amount",
    ul.FIRST_NAME || '' '' || ul.LAST_NAME "Issuer",
    SUM(ar.TOTAL_AMOUNT) "Chargeback Amount",
    CASE WHEN ar.DESCRIPTION LIKE CHR(28) || ''%'' THEN NULL ELSE ar.DESCRIPTION END "Description",
    r.COMMENT_TEXT "Comment"
FROM CORP.CURRENCY c
    JOIN REPORT.ACTIVITY_REF ar ON ar.CURRENCY_ID = c.CURRENCY_ID
    JOIN REPORT.ACTIVITY_REF ar_1 ON ar_1.TRAN_ID = ar.ORIG_TRAN_ID
    JOIN REPORT.EPORT e ON ar.EPORT_ID = e.EPORT_ID
    JOIN CORP.REFUND r ON r.TRAN_ID = ar.TRAN_ID
    JOIN REPORT.USER_LOGIN ul ON ul.USER_ID = r.CREATOR_USER_ID
    LEFT OUTER JOIN REPORT.TERMINAL T ON AR.TERMINAL_ID = T.TERMINAL_ID
    LEFT OUTER JOIN CORP.CUSTOMER CUS ON T.CUSTOMER_ID = CUS.CUSTOMER_ID
WHERE r.CREATED_TS >= TRUNC(cast( ?  as date), ''DD'') AND r.CREATED_TS  < TRUNC(cast( ?  as date), ''DD'') + 1
AND (? is NULL or T.CUSTOMER_ID MEMBER OF CAST(? AS DBADMIN.NUMBER_TABLE) ) 
GROUP BY 
    CUS.CUSTOMER_NAME,
    c.CURRENCY_NAME,
    REPORT.MASK_CARD(ar.CARD_NUMBER, ar.TRANS_TYPE_ID),
    REPORT.MASK_CARD(UPPER(ar.CARD_NUMBER), ar.TRANS_TYPE_ID),
    e.EPORT_SERIAL_NUM,
    UPPER(e.EPORT_SERIAL_NUM),
    ar_1.CC_APPR_CODE,
    UPPER(ar_1.CC_APPR_CODE),
    ar.TRAN_DATE,
    ul.FIRST_NAME || '' '' || ul.LAST_NAME,
    UPPER(ul.FIRST_NAME || '' '' || ul.LAST_NAME),
    ar_1.TRAN_DATE,
    CASE WHEN ar.DESCRIPTION LIKE CHR(28) || ''%'' THEN NULL ELSE ar.DESCRIPTION END,
    r.COMMENT_TEXT,
    ar.TRANS_TYPE_NAME
UNION ALL
SELECT 
    CUS.CUSTOMER_NAME "Customer",
    upper(ar.TRANS_TYPE_NAME) "Trans Type Name",
    c.CURRENCY_NAME "Currency",
    REPORT.MASK_CARD(ar.CARD_NUMBER, ar.TRANS_TYPE_ID) "Card Number",
    e.EPORT_SERIAL_NUM "Card Number",
    ar_1.CC_APPR_CODE "CC APPR Code",
    ar.TRAN_DATE "Tran Date",
    SUM(ar_1.TOTAL_AMOUNT) "Original Amount",
    ul.FIRST_NAME || '' '' || ul.LAST_NAME "Issuer",
    SUM(ar.TOTAL_AMOUNT) "Chargeback Amount",
    CASE WHEN ar.DESCRIPTION LIKE CHR(28) || ''%'' THEN NULL ELSE ar.DESCRIPTION END "Description",
    cb.COMMENT_TEXT "Comment"
FROM CORP.CURRENCY c
    JOIN REPORT.ACTIVITY_REF ar ON ar.CURRENCY_ID = c.CURRENCY_ID
    JOIN REPORT.ACTIVITY_REF ar_1 ON ar_1.TRAN_ID = ar.ORIG_TRAN_ID
    JOIN REPORT.EPORT e ON ar.EPORT_ID = e.EPORT_ID
    JOIN CORP.CHARGEBACK cb ON cb.TRAN_ID = ar.TRAN_ID
    JOIN REPORT.USER_LOGIN ul ON ul.USER_ID = cb.CREATOR_USER_ID
    LEFT OUTER JOIN REPORT.TERMINAL T ON AR.TERMINAL_ID = T.TERMINAL_ID
    LEFT OUTER JOIN CORP.CUSTOMER CUS ON T.CUSTOMER_ID = CUS.CUSTOMER_ID
WHERE cb.CREATED_TS >= TRUNC(cast( ?  as date), ''DD'') AND cb.CREATED_TS  < TRUNC(cast( ?  as date), ''DD'') + 1
AND (? is NULL or T.CUSTOMER_ID MEMBER OF CAST(? AS DBADMIN.NUMBER_TABLE) ) 
GROUP BY 
    CUS.CUSTOMER_NAME,
    c.CURRENCY_NAME,
    REPORT.MASK_CARD(ar.CARD_NUMBER, ar.TRANS_TYPE_ID),
    REPORT.MASK_CARD(UPPER(ar.CARD_NUMBER), ar.TRANS_TYPE_ID),
    e.EPORT_SERIAL_NUM,
    UPPER(e.EPORT_SERIAL_NUM),
    ar_1.CC_APPR_CODE,
    UPPER(ar_1.CC_APPR_CODE),
    ar.TRAN_DATE,
    ul.FIRST_NAME || '' '' || ul.LAST_NAME,
    UPPER(ul.FIRST_NAME || '' '' || ul.LAST_NAME),
    ar_1.TRAN_DATE,
    CASE WHEN ar.DESCRIPTION LIKE CHR(28) || ''%'' THEN NULL ELSE ar.DESCRIPTION END,
    cb.COMMENT_TEXT,
    ar.TRANS_TYPE_NAME
ORDER BY 
    "Customer",
    "Tran Date",
    "Currency",
    "Card Number",
    "Device",
    "CC APPR Code"');
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'header', 'true');
	insert into report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) values(LN_REPORT_ID, 'dateFormat', 'MM/dd/yyyy');
	insert into report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) values(LN_REPORT_ID, 'params.beginDate', '{b}');
	insert into report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) values(LN_REPORT_ID, 'params.endDate', '{e}');
	insert into report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) values(LN_REPORT_ID, 'paramNames', 'beginDate,endDate,customerId,customerId,beginDate,endDate,customerId,customerId');
	insert into report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) values(LN_REPORT_ID, 'paramTypes', 'TIMESTAMP,TIMESTAMP,ARRAY:NUMERIC,ARRAY:NUMERIC,TIMESTAMP,TIMESTAMP,ARRAY:NUMERIC,ARRAY:NUMERIC');
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'isSQLFolio', 'true');
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'params.customerId', '{m}');
	insert into report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) values(LN_REPORT_ID, 'paramRequired', 'true,true,false,false,true,true,false,false');
	
	select max(web_link_id)+1 into LN_WEB_LINK_ID from web_content.web_link;
		
	insert into web_content.web_link(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER) values(LN_WEB_LINK_ID, 'Refund And Chargeback', './select_date_range_sqlfolio_by_customer.html?basicReportId=' || LN_REPORT_ID, 'Refund And Chargeback', 'Accounting', 'W', 0);
	insert into web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) select web_link_id, 1 from web_content.web_link where web_link_label = 'Refund And Chargeback';
	insert into web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) select web_link_id, 14 from web_content.web_link where web_link_label = 'Refund And Chargeback';
	commit;
END;
/

INSERT INTO FOLIO_CONF.JOIN_FILTER(JOIN_FILTER_ID, FROM_TABLE, TO_TABLE, JOIN_EXPRESSION, JOIN_TYPE, JOIN_CARDINALITY)
    VALUES(2258, 'REPORT.REGION[PARENT]', 'REPORT.REGION[Payment]', 'REPORT.REGION[PARENT].REGION_ID = REPORT.REGION[Payment].PARENT_REGION_ID', 'RIGHT', '<');
COMMIT;

GRANT SELECT ON REPORT.CCS_TRANSPORT_STATUS TO USALIVE_APP_ROLE;

create or replace FUNCTION REPORT.GET_TRANSPORT_PROPERTY(pn_ccs_transport_id IN REPORT.CCS_TRANSPORT.CCS_TRANSPORT_ID%TYPE)
   RETURN VARCHAR2
IS
   l_str VARCHAR2(4000);
BEGIN
   SELECT WM_CONCAT(DECODE(UPPER(TPT.CCS_TPT_NAME), 'PASSWORD',UPPER(TPT.CCS_TPT_NAME)||':'||RPAD('*',LENGTH(TP.CCS_TRANSPORT_PROPERTY_VALUE), '*'), UPPER(TPT.CCS_TPT_NAME)||':'||TP.CCS_TRANSPORT_PROPERTY_VALUE)) TRANSPORT_PROPERTY
   into l_str
   FROM REPORT.CCS_TRANSPORT_PROPERTY TP 
	 JOIN REPORT.CCS_TRANSPORT_PROPERTY_TYPE TPT ON TPT.CCS_TRANSPORT_PROPERTY_TYPE_ID = TP.CCS_TRANSPORT_PROPERTY_TYPE_ID
   where TP.CCS_TRANSPORT_ID=pn_ccs_transport_id;

   RETURN l_str;
END;
/

GRANT EXECUTE on REPORT.GET_TRANSPORT_PROPERTY to USAT_DEV_READ_ONLY;
GRANT EXECUTE on REPORT.GET_TRANSPORT_PROPERTY to USALIVE_APP_ROLE;

INSERT INTO WEB_CONTENT.REQUIREMENT(REQUIREMENT_ID, REQUIREMENT_NAME, REQUIREMENT_CLASS, REQUIREMENT_PARAM_CLASS, REQUIREMENT_PARAM_VALUE)
VALUES( 49, 'REQ_ADMIN_ALL_CUST_USERS', 'com.usatech.usalive.link.HasPrivilegeRequirement', 'java.lang.String', 'PRIV_ADMIN_ALL_CUST_USERS');
commit;


ALTER SESSION SET CURRENT_SCHEMA = REPORT;    
DECLARE
  LN_WEB_LINK_ID NUMBER;
BEGIN
	SELECT max(web_link_id) INTO LN_WEB_LINK_ID
	FROM web_content.web_link WHERE WEB_LINK_LABEL = 'View User Reports';
	
	IF LN_WEB_LINK_ID > 0 THEN
		RETURN;
	END IF;
	select max(web_link_id)+1 into LN_WEB_LINK_ID from web_content.web_link;
		
	insert into web_content.web_link(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER) values(LN_WEB_LINK_ID, 'View User Reports', './view_customer_all_user_reports.html', 'View User Reports', 'Daily Operations', 'W', 0);
	insert into web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) select web_link_id, 1 from web_content.web_link where web_link_label = 'View User Reports';
	insert into web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) select web_link_id, 4 from web_content.web_link where web_link_label = 'View User Reports';
	insert into web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) select web_link_id, 49 from web_content.web_link where web_link_label = 'View User Reports';
	commit;
END;
/

