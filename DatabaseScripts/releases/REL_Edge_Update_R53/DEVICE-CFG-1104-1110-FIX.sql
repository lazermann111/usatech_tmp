DECLARE
  CURSOR l_cur IS
    SELECT d.device_id, d.device_name, d.device_serial_cd, dd.property_list_version,
    substr(ds06.device_setting_value, 5, 3) ds1106,
    ds10.device_setting_value ds1110
    FROM device.device d
    JOIN device.device_data dd ON d.device_name = dd.device_name
    LEFT OUTER JOIN device.device_setting ds06 ON d.device_id = ds06.device_id
      AND (ds06.device_setting_parameter_cd = '1106'
      or ds06.device_setting_parameter_cd is null)
    LEFT OUTER JOIN device.device_setting ds10 ON d.device_id = ds10.device_id
      AND (ds10.device_setting_parameter_cd = '1110'
      or ds10.device_setting_parameter_cd is null)
    WHERE d.device_active_yn_flag = 'Y'
    AND dd.property_list_version IS NOT NULL
    AND d.device_type_id = 13
    AND ds06.device_setting_value IS NOT NULL
    AND REGEXP_LIKE(ds06.device_setting_value, 'I\^0\^[0-9]{3}')
    AND nvl(ds10.device_setting_value, '0') = '0'
    AND d.device_serial_cd LIKE 'VJ%';
    ln_file_transfer_id NUMBER;
    lv_data_type VARCHAR(256) := NULL;
    lv_command VARCHAR(256) := NULL;
    lv_execute_cd VARCHAR(256) := 'P';
    ln_execute_order NUMBER := 0;
    ln_command_count NUMBER;
BEGIN
  FOR l_rec IN l_cur loop
      dbms_output.put_line(l_rec.device_serial_cd || ': PLV = ' || l_rec.property_list_version);
      IF l_rec.ds1110 IS NULL THEN
        INSERT INTO device.device_setting(device_id, device_setting_parameter_cd, device_setting_value)
        SELECT l_rec.device_id, '1110', l_rec.ds1106 FROM dual;
        dbms_output.put_line('insert ' || l_rec.device_serial_cd || ': ' || l_rec.ds1106);
      ELSE
        UPDATE device.device_setting
        SET device_setting_value = l_rec.ds1106
        WHERE device_id = l_rec.device_id
        AND device_setting_parameter_cd = '1110';
        dbms_output.put_line('update ' || l_rec.device_serial_cd || ': ' || l_rec.ds1106);
      END IF;
      IF l_rec.property_list_version >= 19 THEN
        SELECT seq_file_transfer_id.nextval INTO ln_file_transfer_id FROM dual;
        INSERT INTO device.file_transfer(file_transfer_id, file_transfer_name, file_transfer_type_cd, file_transfer_content)
        SELECT ln_file_transfer_id, l_rec.device_name || '-CFG-' || ln_file_transfer_id, 19, utl_raw.cast_to_raw('1110=' || l_rec.ds1106 || chr(10)) FROM dual;
        pkg_device_configuration.create_pending_command(l_rec.device_name,lv_data_type,lv_command,lv_execute_cd,ln_execute_order,0,ln_file_transfer_id,0,l_rec.device_id,1024,13,ln_command_count);
        dbms_output.put_line('add pending ' || l_rec.device_serial_cd || ': file_transfer_id = ' || ln_file_transfer_id || ', ln_command_count = ' || ln_command_count);
      END IF;
  END LOOP;
  COMMIT;
END;
