ALTER TABLE REPORT.REPORTS
ADD (DEFAULT_USER_REPORT NUMBER(1,0) DEFAULT 0 );

CREATE INDEX REPORT.IX_REPORTS_DEF_USR_REPORT ON REPORT.REPORTS (DEFAULT_USER_REPORT);

COMMENT ON COLUMN REPORT.REPORTS.DEFAULT_USER_REPORT IS '1=default report for all users in report register, 0=standard report';

update report.reports set default_user_report = 1
where
report_name = 'Payment Detail for EFT' or
report_name = 'Pending Payment Summary (Html)' or
report_name = 'Sales Activity By Batch' or
report_name = 'Transactions Included in EFT';

commit;
