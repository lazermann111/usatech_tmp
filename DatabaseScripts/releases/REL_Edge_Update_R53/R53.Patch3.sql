update report.reports set description=description||', Sales Order Number' where report_name in ('Terminal Details (CSV) (Custom)',
'Terminal Details (Excel) (Custom)','Terminal Details With Sim-Id/MEID (CSV)','Terminal Details (CSV)','Terminal Details (Excel)');

update report.reports set description=description||', Network City, Network State, Network Zip' where report_name in ('Terminal Details (CSV)','Terminal Details (Excel)');
commit;