INSERT INTO FOLIO_CONF.FIELD(FIELD_ID,           
                             FIELD_LABEL,        
                             FIELD_DESC,         
                             DISPLAY_EXPRESSION, 
                             SORT_EXPRESSION,    
                             DISPLAY_FORMAT,     
                             DISPLAY_SQL_TYPE,   
                             SORT_SQL_TYPE,      
                             IMPORTANCE,         
                             FIELD_CATEGORY_ID,  
                             ACTIVE_FLAG)        
 VALUES(9700,                           
        'Last Call In Date (local)',    
        '',                             
        'REPORT.CONVERT_TO_EPORT_LOCAL_TIME(REPORT.EPORT.LASTDIALIN_DATE, REPORT.EPORT.EPORT_ID)', 
        'REPORT.EPORT.LASTDIALIN_DATE', 
        '',                             
        'TIMESTAMP',     
        'TIMESTAMP',                    
        50,                             
        477,                            
        'Y');                           

commit;
create or replace FUNCTION        REPORT.GET_EPORT_TZ
 (
  p_EPORT_ID IN NUMBER
 ) RETURN VARCHAR2
IS
 l_rv VARCHAR2(4000);
 BEGIN
  select tz.time_zone_guid
  into l_rv
  from
   report.time_zone tz
   join report.terminal term on tz.TIME_ZONE_ID = term.TIME_ZONE_ID
   join report.vw_terminal_eport vte on vte.TERMINAL_ID = term.TERMINAL_ID
   join report.eport ep on ep.eport_id = vte.eport_id
  where ep.eport_id=p_EPORT_ID;
  RETURN l_rv;
 END;
/

create or replace FUNCTION        REPORT.CONVERT_TO_EPORT_LOCAL_TIME
 (
    p_SERVER_TIME IN DATE
  , p_EPORT_ID IN NUMBER
 ) RETURN TIMESTAMP --WITH TIME ZONE
IS
 l_rv TIMESTAMP; --WITH TIME ZONE;
 BEGIN
  SELECT FROM_TZ((select cast(p_SERVER_TIME as timestamp) from REPORT.EPORT
  where eport_id=p_EPORT_ID), PKG_CONST.GET_DB_TIME_ZONE) AT TIME ZONE report.get_eport_tz(p_EPORT_ID)
  into l_rv
  FROM DUAL;
  return l_rv;
 END;
 /

INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
VALUES(NULL, 1,9700,NULL);
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
VALUES(NULL, 2,9700,NULL);
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
VALUES(NULL, 3,9700,NULL);
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
VALUES(NULL, 4,9700,1);
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
VALUES(NULL, 5,9700,1);
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID , USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
VALUES(NULL, 8,9700,1);

UPDATE FOLIO_CONF.FOLIO_PILLAR_FIELD SET FIELD_ID = 9700 WHERE FIELD_ID = 2393 AND FOLIO_PILLAR_ID = (
 SELECT FP.FOLIO_PILLAR_ID FROM FOLIO_CONF.FOLIO_PILLAR_FIELD PF
  JOIN FOLIO_CONF.FOLIO_PILLAR FP on PF.FOLIO_PILLAR_ID = FP.FOLIO_PILLAR_ID
  JOIN FOLIO_CONF.FOLIO FO on FO.FOLIO_ID = FP.FOLIO_ID
 WHERE FIELD_ID = 2393 AND FO.FOLIO_ID = 1087);
 
COMMIT;

grant execute on REPORT.GET_EPORT_TZ to report;
GRANT EXECUTE ON REPORT.GET_EPORT_TZ TO USAT_DMS_ROLE;
GRANT EXECUTE ON REPORT.GET_EPORT_TZ TO USAT_APP_LAYER_ROLE;
GRANT EXECUTE ON REPORT.GET_EPORT_TZ TO USALIVE_APP_ROLE;
GRANT EXECUTE ON REPORT.GET_EPORT_TZ TO USAT_RPT_REQ_ROLE;
GRANT EXECUTE ON REPORT.GET_EPORT_TZ TO USAT_RPT_GEN_ROLE;

grant execute on REPORT.CONVERT_TO_EPORT_LOCAL_TIME to report;
GRANT EXECUTE ON REPORT.CONVERT_TO_EPORT_LOCAL_TIME TO USAT_DMS_ROLE;
GRANT EXECUTE ON REPORT.CONVERT_TO_EPORT_LOCAL_TIME TO USAT_APP_LAYER_ROLE;
GRANT EXECUTE ON REPORT.CONVERT_TO_EPORT_LOCAL_TIME TO USALIVE_APP_ROLE;
GRANT EXECUTE ON REPORT.CONVERT_TO_EPORT_LOCAL_TIME TO USAT_RPT_REQ_ROLE;
GRANT EXECUTE ON REPORT.CONVERT_TO_EPORT_LOCAL_TIME TO USAT_RPT_GEN_ROLE;