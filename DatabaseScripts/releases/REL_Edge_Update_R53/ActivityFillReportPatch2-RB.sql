UPDATE report.report_param
SET param_value = 'MM/dd/yyyy'
WHERE param_name = 'dateFormat'
AND report_id IN (
  SELECT report_id
  FROM report.reports
  WHERE report_name LIKE 'Activity By Fill Date And Region%');
  
DELETE FROM FOLIO_CONF.FIELD_PRIV WHERE FIELD_ID = 9729;
DELETE FROM FOLIO_CONF.FIELD WHERE FIELD_ID = 9729;

COMMIT;
