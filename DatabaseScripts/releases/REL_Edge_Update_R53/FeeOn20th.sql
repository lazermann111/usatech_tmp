set serveroutput on
DECLARE
l_run_date DATE :=TO_DATE('2017/01/20 16:30:25', 'YYYY/MM/DD HH24:MI:SS');
l_service_fee_id NUMBER:=25506;

CURSOR c_fee
        IS
            SELECT DISTINCT
                   SF.SERVICE_FEE_ID,
                   SF.TERMINAL_ID,
                   CBT.CUSTOMER_BANK_ID,
                   F.FEE_ID,
                   SF.FEE_AMOUNT,
                   Q.MONTHS,
                   Q.DAYS,
                   SF.LAST_PAYMENT,
                   CASE WHEN Q.FREQUENCY_ID = 4 THEN SF.START_DATE+ Q.DAYS 
                   ELSE SF.START_DATE END START_DATE,
                   --SF.START_DATE START_DATE,
                   LEAST(NVL(
                        CASE --WHEN Q.FREQUENCY_ID = 4 THEN SF.END_DATE 
                             WHEN Q.MONTHS = 0 THEN SF.END_DATE + Q.DAYS  
                             ELSE CORP.GET_SERVICE_FEE_DATE(T.CUSTOMER_ID, ADD_MONTHS(SF.END_DATE, Q.MONTHS))
                        END
                   , MAX_DATE), l_run_date) EFFECTIVE_END_DATE,
                   F.FEE_NAME,
                   T.FEE_CURRENCY_ID,
                   DECODE(T.PAYMENT_SCHEDULE_ID, 2, 'Y', 8, 'Y', 'N') AS_ACCUM,
                   F.INITIATION_TYPE_CD,
                   SF.GRACE_PERIOD_DATE,
				   SF.TRIGGERING_DATE,
				   SF.NO_TRIGGER_EVENT_FLAG,
           SF.INACTIVE_FEE_AMOUNT,
           SF.INACTIVE_MONTHS_REMAINING,Q.FREQUENCY_ID, T.CUSTOMER_ID
              FROM CORP.SERVICE_FEES SF
              JOIN CORP.FREQUENCY Q ON SF.FREQUENCY_ID = Q.FREQUENCY_ID
              JOIN CORP.CUSTOMER_BANK_TERMINAL CBT ON SF.TERMINAL_ID = CBT.TERMINAL_ID AND l_run_date >= NVL(CBT.START_DATE, MIN_DATE) AND l_run_date < NVL(CBT.END_DATE, MAX_DATE)
              JOIN CORP.FEES F ON SF.FEE_ID = F.FEE_ID
              JOIN REPORT.TERMINAL T ON SF.TERMINAL_ID = T.TERMINAL_ID              
             WHERE SF.service_fee_id=l_service_fee_id
               and (Q.DAYS > 0 OR Q.MONTHS > 0)
               AND CASE WHEN Q.MONTHS = 0 THEN NVL(SF.LAST_PAYMENT, MIN_DATE) + Q.DAYS
                             ELSE CORP.GET_SERVICE_FEE_DATE(T.CUSTOMER_ID, ADD_MONTHS(NVL(SF.LAST_PAYMENT, MIN_DATE), Q.MONTHS))
                        END < LEAST(NVL(
                        CASE WHEN Q.MONTHS = 0 THEN SF.END_DATE + Q.DAYS
                             ELSE CORP.GET_SERVICE_FEE_DATE(T.CUSTOMER_ID,ADD_MONTHS(SF.END_DATE, Q.MONTHS))
                        END
                   , MAX_DATE), l_run_date)
               AND (SF.LAST_PAYMENT IS NOT NULL 
				OR (SF.TRIGGERING_DATE IS NOT NULL AND (F.INITIATION_TYPE_CD = 'T'
					OR F.INITIATION_TYPE_CD IN('G', 'R') AND COALESCE(SF.NO_TRIGGER_EVENT_FLAG, 'N') != 'Y'))
				OR SF.GRACE_PERIOD_DATE < l_run_date
        OR (F.INITIATION_TYPE_CD IN('I', 'Q') AND SF.START_DATE < l_run_date))
             ORDER BY SF.LAST_PAYMENT NULLS LAST, CBT.CUSTOMER_BANK_ID, SF.SERVICE_FEE_ID;

        l_last_payment                     CORP.SERVICE_FEES.LAST_PAYMENT%TYPE;
        l_prev_payment                     CORP.SERVICE_FEES.LAST_PAYMENT%TYPE;
        l_fmt VARCHAR2(50);
        l_ledger_id CORP.LEDGER.LEDGER_ID%TYPE;
        l_batch_id CORP.LEDGER.BATCH_ID%TYPE;
		    l_last_run_complete_ts DATE := TO_DATE(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('SCAN_FOR_SERVICE_FEES_LAST_RUN_COMPLETE_TS'), 'MM/DD/YYYY HH24:MI:SS');
        l_inactive_months_remaining CORP.SERVICE_FEES.INACTIVE_MONTHS_REMAINING%TYPE;
        l_cnt PLS_INTEGER;
    BEGIN
       dbms_output.put_line('begin:'||to_char(systimestamp,'DD.MM.YYYY HH24:MI:SSFF3'));
        
        FOR r_fee IN c_fee LOOP
        dbms_output.put_line('EFFECTIVE_END_DATE:'||r_fee.EFFECTIVE_END_DATE);
        dbms_output.put_line('START_DATE:'||r_fee.START_DATE);
        dbms_output.put_line('fee_amount:'||r_fee.fee_amount);
            -- If last payment is null, Use first transaction as last payment date
            IF r_fee.LAST_PAYMENT IS NULL THEN
                IF r_fee.INITIATION_TYPE_CD = 'I' THEN
                    IF r_fee.frequency_id = 4 THEN
                      SELECT r_fee.START_DATE - r_fee.DAYS
                          INTO l_last_payment
                          FROM DUAL;
                    ELSE
                      l_last_payment := r_fee.START_DATE;
                    END IF;
                ELSIF r_fee.INITIATION_TYPE_CD = 'Q' THEN
                    IF r_fee.MONTHS > 0 THEN
                        SELECT CORP.GET_SERVICE_FEE_DATE(r_fee.CUSTOMER_ID,ADD_MONTHS(r_fee.START_DATE, -r_fee.MONTHS))
                          INTO l_last_payment
                          FROM DUAL;
                     ELSE
                        SELECT r_fee.START_DATE - r_fee.DAYS
                          INTO l_last_payment
                          FROM DUAL;
                    END IF;
                ELSIF r_fee.INITIATION_TYPE_CD IN('G', 'T', 'R') THEN
                    l_last_payment := r_fee.TRIGGERING_DATE;
                    IF r_fee.INITIATION_TYPE_CD IN('G', 'R') AND r_fee.GRACE_PERIOD_DATE < NVL(l_last_payment, l_run_date) THEN 
                        l_last_payment := r_fee.GRACE_PERIOD_DATE;
                        --UPDATE CORP.SERVICE_FEES
                        --SET TRIGGERING_DATE = l_last_payment
                        --WHERE SERVICE_FEE_ID = r_fee.SERVICE_FEE_ID;
                     END IF;
                     IF l_last_payment IS NOT NULL THEN
                         IF r_fee.INITIATION_TYPE_CD IN('G', 'R') THEN
                             IF r_fee.MONTHS > 0 THEN
                                SELECT CORP.GET_SERVICE_FEE_DATE(r_fee.CUSTOMER_ID,ADD_MONTHS(l_last_payment, -r_fee.MONTHS))
                                  INTO l_last_payment
                                  FROM DUAL;
                             ELSE
                                IF r_fee.frequency_id = 4 THEN
                                  SELECT TRUNC(LAST_DAY(ADD_MONTHS(l_last_payment, -1)), 'DD')
                                  INTO l_last_payment
                                  FROM DUAL;
                                ELSE
                                  SELECT l_last_payment - r_fee.DAYS
                                    INTO l_last_payment
                                    FROM DUAL;
                                END IF;
                             END IF;
                        ELSIF r_fee.INITIATION_TYPE_CD = 'T' AND r_fee.frequency_id = 4 THEN
                          l_last_payment:=TRUNC(LAST_DAY(l_last_payment), 'DD');
                        END IF;
                    END IF;
                ELSE
                    l_last_payment := NULL;
                END IF;
            ELSE
                l_last_payment := r_fee.LAST_PAYMENT;
            END IF;
            dbms_output.put_line('1 service_fee l_last_payment:'||l_last_payment);
            --l_last_payment:=TO_DATE('2016/10/20 01:30:25', 'YYYY/MM/DD HH:MI:SS');
            IF l_last_payment IS NOT NULL THEN
              l_fmt := CORP.PAYMENTS_PKG.GET_SERVICE_FEE_DATE_FMT(r_fee.MONTHS, r_fee.DAYS);
              l_inactive_months_remaining := r_fee.INACTIVE_MONTHS_REMAINING;
              LOOP
                  l_prev_payment := l_last_payment;
                  IF r_fee.MONTHS > 0 THEN
                      SELECT CORP.GET_SERVICE_FEE_DATE(r_fee.CUSTOMER_ID,ADD_MONTHS(l_last_payment, r_fee.MONTHS))
                        INTO l_last_payment
                        FROM DUAL;
                  ELSE 
                      SELECT l_last_payment + r_fee.DAYS
                        INTO l_last_payment
                        FROM DUAL;
                  END IF;
                  dbms_output.put_line('l_last_payment:'||l_last_payment);
                 -- EXIT WHEN l_last_payment >= r_fee.EFFECTIVE_END_DATE;
                 EXIT WHEN l_last_payment >= r_fee.EFFECTIVE_END_DATE;
                 
                 IF l_last_payment >= NVL(r_fee.start_date, l_last_payment)  and l_last_payment >=coalesce(r_fee.GRACE_PERIOD_DATE, MIN_DATE) THEN
                  dbms_output.put_line('charge l_last_payment:'||l_last_payment||' amount:'||r_fee.FEE_AMOUNT);
                 END IF;
             END LOOP;
             dbms_output.put_line('2 l_last_payment:'||l_last_payment);
            END IF;
      END LOOP;
      dbms_output.put_line('end:'||to_char(systimestamp,'DD.MM.YYYY HH24:MI:SSFF3'));
END;
  
--ALTER TABLE CORP.CUSTOMER
--modify FEE_SCHEDULE_ID NUMBER default 2;     




