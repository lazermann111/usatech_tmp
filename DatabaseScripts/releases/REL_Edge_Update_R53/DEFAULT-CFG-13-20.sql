DECLARE
	ln_property_list_version NUMBER := 20;
	ln_device_type_id device_type.device_type_id%TYPE := 13;
	ln_config_template_type_id config_template_type.config_template_type_id%TYPE := 1;
	lv_config_template_name config_template.config_template_name%TYPE := 'DEFAULT-CFG-' || ln_device_type_id || '-' || ln_property_list_version;
	ln_config_template_id config_template.config_template_id%TYPE;
	
	PROCEDURE ADD_DSP(
		pv_index VARCHAR2,
		pv_configurable VARCHAR2,
		pv_name VARCHAR2) 
	IS
	BEGIN
		INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_UI_CONFIGURABLE, DEVICE_SETTING_PARAMETER_NAME) 
		SELECT pv_index, pv_configurable, pv_name
		FROM DUAL 
		WHERE NOT EXISTS (
			SELECT 1 
			FROM DEVICE.DEVICE_SETTING_PARAMETER 
			WHERE DEVICE_SETTING_PARAMETER_CD = pv_index);
	END;

	PROCEDURE ADD_CTS_META(
		pv_PARAMETER_CD VARCHAR2,
		pv_SETTING_VALUE VARCHAR2,
		pn_CATEGORY_ID NUMBER,
		pv_DISPLAY VARCHAR2,
		pv_DESCRIPTION VARCHAR2,
		pv_EDITOR VARCHAR2,
		pn_FIELD_SIZE NUMBER,
		pn_DISPLAY_ORDER NUMBER,
		pv_DATA_MODE VARCHAR2,
		pv_DATA_MODE_AUX VARCHAR2,
		pn_REGEX_ID NUMBER,
		pv_NAME VARCHAR2,
		pv_CUSTOMER_EDITABLE VARCHAR2) 
	IS 
	BEGIN
		INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(
			CONFIG_TEMPLATE_ID,
			DEVICE_SETTING_PARAMETER_CD,
			CONFIG_TEMPLATE_SETTING_VALUE,
			CATEGORY_ID,
			DISPLAY,
			DESCRIPTION,
			EDITOR,
			CONVERTER,
			CLIENT_SEND,
			ALIGN,
			FIELD_SIZE,
			FIELD_OFFSET,
			PAD_CHAR,
			PROPERTY_LIST_VERSION,
			DEVICE_TYPE_ID,
			DISPLAY_ORDER,
			ALT_NAME,
			DATA_MODE,
			DATA_MODE_AUX,
			REGEX_ID,
			ACTIVE,
			NAME,
			CUSTOMER_EDITABLE,
			STORED_IN_PENNIES) 
		SELECT 
			CONFIG_TEMPLATE_ID, 
			pv_PARAMETER_CD,
			pv_SETTING_VALUE,
			pn_CATEGORY_ID,
			pv_DISPLAY,
			pv_DESCRIPTION,
			pv_EDITOR,
			'',
			'Y',
			NULL,
			pn_FIELD_SIZE,
			NULL,
			NULL,
			0,
			DEVICE_TYPE_ID,
			pn_DISPLAY_ORDER,
			NULL,
			pv_DATA_MODE,
			pv_DATA_MODE_AUX,
			pn_REGEX_ID,
			'Y',
			pv_NAME,
			pv_CUSTOMER_EDITABLE,
			'N'
		FROM DEVICE.CONFIG_TEMPLATE CT 
		WHERE CONFIG_TEMPLATE_TYPE_ID = 3 
			AND DEVICE_TYPE_ID = 13 
			AND NOT EXISTS (
				SELECT 1 
				FROM DEVICE.CONFIG_TEMPLATE_SETTING 
				WHERE DEVICE_TYPE_ID = 13 
				AND DEVICE_SETTING_PARAMETER_CD = pv_PARAMETER_CD);
	END;
	
	PROCEDURE ADD_CTS(
		pn_CONFIG_TEMPLATE_ID NUMBER,
		pv_PARAMETER_CD VARCHAR2,
		pv_SETTING_VALUE VARCHAR2) 
	IS
	BEGIN
		INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(
			CONFIG_TEMPLATE_ID, 
			DEVICE_SETTING_PARAMETER_CD, 
			CONFIG_TEMPLATE_SETTING_VALUE) 
		VALUES(pn_CONFIG_TEMPLATE_ID, pv_PARAMETER_CD, pv_SETTING_VALUE);
	END;
	
BEGIN
	-- add new indexes to device_setting_parameter
	ADD_DSP('34', 'Y', 'Misc Background Check Time');
	ADD_DSP('35', 'Y', 'Misc PDP Context Idle Timeout');
	ADD_DSP('302', 'N', 'RFTFS');
	ADD_DSP('310', 'N', 'Misc PDP Context Attempt Counter');
	ADD_DSP('311', 'N', 'Misc PDP Context Error Counter');
	ADD_DSP('312', 'N', 'Misc TCP Session Attempt Counter');
	ADD_DSP('313', 'N', 'Misc TCP Session Error Counter');
	ADD_DSP('314', 'N', 'Auth TCP Session Attempt Counter');
	ADD_DSP('315', 'N', 'Auth TCP Session Error Counter');
	ADD_DSP('1600', 'Y', 'eSerial Setting Parse Timeout');
	ADD_DSP('1601', 'Y', 'eSerial Setting No Coms Timeout');
	ADD_DSP('1602', 'Y', 'eSerial Setting Transaction Timeout');
	ADD_DSP('1603', 'Y', 'eSerial End/Complete Button Enable');
	
	-- add new category id 42
	
	INSERT INTO device.config_template_category(config_template_category_id, config_template_category_name, config_template_category_desc)
	VALUES (42, 'ePort Serial', 'ePort Serial'); 
	
	INSERT INTO DEVICE.CONFIG_TEMPLATE_CATEGORY_DISP(CONFIG_TEMPLATE_CATEGORY_ID, DEVICE_TYPE_ID, DISPLAY, CATEGORY_DISPLAY_ORDER)
	VALUES (42, 13, 'Y', 65);
	
	-- add new configurable parameters to config_template_setting meta template
	ADD_CTS_META('34', '7200', 3, 'Y', 'Number of seconds to poll modem for RSSI and misc data. {7200 default, 0 is off}', 'TEXT:1 to 5', 5, 28, 'A', NULL, 3, 'Misc Background Check Time', 'N');
	ADD_CTS_META('35', '7200', 3, 'Y', 'If the G9 has not communicated in N seconds the modem''s PDP context will be forced closed. PDP context will be reestablish in line with the next TCP session. {7200 default, 0 is disabled}', 'TEXT:1 to 5', 5, 29, 'A', NULL, 3, 'Misc PDP Context Idle Timeout', 'N');
	ADD_CTS_META('1600', '12', 42, 'Y', 'This value sets the time gap in 200us increments on when to parse the incoming serial message. Example 12 is 2.4ms. {Default is 12, MIN is 2, MAX is 20}', 'TEXT:1 to 2', 2, 1, 'A', NULL, 3, 'eSerial Setting Parse Timeout', 'N');
	ADD_CTS_META('1601', '7200', 42, 'Y', 'This value is the number of seconds that must elapse after the Host stops communicating with the G9 before all work in process is marked Failed and the unit will be disabled. Note: Device will change the improper setting of 0 to the value of 10 seconds. {default is 7200}', 'TEXT:1 to 16', 16, 2, 'A', NULL, 3, 'eSerial Setting No Coms Timeout', 'Y');
	ADD_CTS_META('1602', '1800', 42, 'Y', 'This value in seconds sets the timeout for the Host to post a Transaction Message after credit is received. Note: Device will change the improper setting of 0 to the value of 20 seconds. {default is 1800}', 'TEXT:1 to 16', 16, 3, 'A', NULL, 3, 'eSerial Setting Transaction Timeout', 'Y');
	ADD_CTS_META('1603', '0', 42, 'Y', 'Set to 0 to Enable the END Button. {default is 0}', 'TEXT:1 to 1', 1, 4, 'A', NULL, 3, 'eSerial End/Complete Button Enable', 'Y');

	-- updates to existing config template settings of meta template
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET DESCRIPTION = 'Sets the timeout of the VEND portion of the MDB Cashless Session. Example, how long to wait for the can to drop. Notes: 1) A minimum of 30 is recommended. 2) Device will change the improper setting of 0 to the value of 300 seconds (5 minutes).' 
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '1021'; 
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET DESCRIPTION = 'This is the number of vends that the ePort will attempt in a multisession if funds are available. Notes: 1) To set single-vend (disable multi-vend) set this value to one. 2) Max items depends on firmware revision and property 1201. For 02.04.002b, Fixed Amount limits are: 20 sale items which means 19 two-tier items.' 
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '1008'; 
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET DESCRIPTION = 'Represents the number of reattempts on when the G9 sends a Poll Response Message, but it did not receive an ACK from the VMC. 5 is default. Set to 0 to disable message retries. ',
	NAME = 'MDB Setting Number of Message Reattempts'
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '1004'; 
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET DESCRIPTION = 'Currency scale is used to set the scale between the ePort and the VMC. Cashless readers prefer a scale of 1, but some older VMCs prefer a scale of 5 because they were developed to support coin mechs that held nickels and quarters. Settings include: 0: VMC scale is set to 1 (a penny) {default} 5: VMC scale is set to 5 (a nickel) 15: VMC scale is set to 5 (a nickel), but cash is reported at a scale of 1. Any other value is interpreted as 0, scale of 1.',
	NAME = 'MDB Setting Currency Scale'
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '1014'; 
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET DESCRIPTION = 'Enables or disables the End/Complete button during an MDB vend session. {Default 0 = Enabled}',
	NAME = 'MDB End/Complete Button Enable'
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '1015'; 
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET DESCRIPTION = 'If 0, disabled {default}, at the beginning of each cashless session, the G9 will send the VMC the balance of funds available for the consumer to spend. If 1, enabled, the VMC will be sent FFFF instead which a special code to instruct the VMC to hide the balance from the consumer.',
	NAME = 'MDB Setting Hide Balance'
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '1016'; 

	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET DESCRIPTION = '0 {default} means diagnostic event logging is disabled. See Engineering for use and revs that support this feature.',
	NAME = 'MDB Diagnostic Event Logging'
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '1017'; 

	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET DESCRIPTION = 'Enables or disables MDB Alerts. Note, Alerts must be enabled to support USALive''s Device Alerts of types MDB Alerts Bill Acceptor and MDB Alerts Coin Changer.',
	NAME = 'MDB Alerts',
	CUSTOMER_EDITABLE = 'Y'
	WHERE DEVICE_TYPE_ID = 13 
	AND DEVICE_SETTING_PARAMETER_CD = '1024'; 

	-- check if config template exists yet
	SELECT MAX(config_template_id) INTO ln_config_template_id
	FROM device.config_template
	WHERE config_template_name = lv_config_template_name;
	
	IF ln_config_template_id IS NULL THEN
		-- create it if not
		INSERT INTO device.config_template(config_template_type_id, config_template_name, device_type_id, property_list_version)
		VALUES(ln_config_template_type_id, lv_config_template_name, ln_device_type_id, ln_property_list_version);
		
		SELECT MAX(config_template_id) INTO ln_config_template_id
		FROM device.config_template
		WHERE config_template_name = lv_config_template_name;
	END IF;
	
	-- remove existing settings for this template, if any
	DELETE FROM DEVICE.CONFIG_TEMPLATE_SETTING WHERE CONFIG_TEMPLATE_ID = ln_config_template_id;

	ADD_CTS(ln_config_template_id, '34', '7200');
	ADD_CTS(ln_config_template_id, '35', '7200');
	ADD_CTS(ln_config_template_id, '1600', '12');
	ADD_CTS(ln_config_template_id, '1601', '7200');
	ADD_CTS(ln_config_template_id, '1602', '1800');
	ADD_CTS(ln_config_template_id, '1603', '0');
	
	-- copy all other settings from previous properties list
	INSERT INTO DEVICE.CONFIG_TEMPLATE_SETTING(CONFIG_TEMPLATE_ID, DEVICE_SETTING_PARAMETER_CD, CONFIG_TEMPLATE_SETTING_VALUE) 
	SELECT ln_config_template_id, DEVICE_SETTING_PARAMETER_CD, CONFIG_TEMPLATE_SETTING_VALUE
	FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS
	JOIN DEVICE.CONFIG_TEMPLATE CT ON CTS.CONFIG_TEMPLATE_ID = CT.CONFIG_TEMPLATE_ID
	WHERE CONFIG_TEMPLATE_NAME = 'DEFAULT-CFG-' || ln_device_type_id || '-' || (ln_property_list_version - 1)
	AND DEVICE_SETTING_PARAMETER_CD NOT IN (
		SELECT DEVICE_SETTING_PARAMETER_CD 
		FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS 
		WHERE CONFIG_TEMPLATE_ID = ln_config_template_id);
		
	-- change the default value for this template
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET CONFIG_TEMPLATE_SETTING_VALUE = '0'
	WHERE CONFIG_TEMPLATE_ID = ln_config_template_id
	AND DEVICE_SETTING_PARAMETER_CD = '1004';
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET CONFIG_TEMPLATE_SETTING_VALUE = '0'
	WHERE CONFIG_TEMPLATE_ID = ln_config_template_id
	AND DEVICE_SETTING_PARAMETER_CD = '1014';
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET CONFIG_TEMPLATE_SETTING_VALUE = '0'
	WHERE CONFIG_TEMPLATE_ID = ln_config_template_id
	AND DEVICE_SETTING_PARAMETER_CD = '1015';
	
	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET CONFIG_TEMPLATE_SETTING_VALUE = '0'
	WHERE CONFIG_TEMPLATE_ID = ln_config_template_id
	AND DEVICE_SETTING_PARAMETER_CD = '1016';

	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET CONFIG_TEMPLATE_SETTING_VALUE = '0'
	WHERE CONFIG_TEMPLATE_ID = ln_config_template_id
	AND DEVICE_SETTING_PARAMETER_CD = '1017';

	DELETE FROM DEVICE.DEVICE_TYPE_PROPERTY_LIST
	WHERE device_type_id = ln_device_type_id
		AND property_list_version = ln_property_list_version;

	INSERT INTO DEVICE.DEVICE_TYPE_PROPERTY_LIST(device_type_id, property_list_version, all_property_list_ids, configurable_property_list_ids)
	VALUES(ln_device_type_id, ln_property_list_version, 
		'10|30-35|50-52|60-64|70|80|81|85-87|100-108|200-208|210-213|215|300-302|310-315|400|401|405|406|410|411|415|416|420|421|425|426|430|431|435|436|440-442|444|446|450|1001-1026|1101|1103|1104|1107|1110-1114|1200-1207|1300-1303|1400-1415|1500-1528|1600-1603',
		'10|30-35|70|85-87|215|400|401|405|406|410|411|415|416|420|421|425|426|430|431|435|436|440-442|444|446|450|1001-1026|1101|1103|1104|1107|1110-1114|1200-1207|1300-1303|1400-1415|1500-1528|1600-1603');
		
	COMMIT;
END;
/
