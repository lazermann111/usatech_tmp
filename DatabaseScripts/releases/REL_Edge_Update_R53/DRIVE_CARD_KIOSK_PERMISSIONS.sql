DECLARE
  ln_pa_id NUMBER;
BEGIN
  -- lookup the right permission action for service event with dex and fill
  SELECT pa.permission_action_id
  INTO ln_pa_id
  FROM pss.permission_action pa
  JOIN device.action A ON pa.action_id = A.action_id
    AND A.action_name = 'Service Event'
  JOIN device.device_type_action dta ON dta.action_id = A.action_id
    and dta.device_type_id = 11
  JOIN pss.permission_action_param pap1 ON pa.permission_action_id = pap1.permission_action_id
  JOIN device.action_param ap1 ON pap1.action_param_id = ap1.action_param_id
    AND ap1.action_param_cd = 'dex'
  JOIN pss.permission_action_param pap2 ON pa.permission_action_id = pap2.permission_action_id
  JOIN device.action_param ap2 ON pap2.action_param_id = ap2.action_param_id
    AND ap2.action_param_cd = 'fill';
    
  -- insert it for every drive card type that does not have it already
  INSERT INTO pss.consumer_acct_permission(consumer_acct_id, permission_action_id, consumer_acct_permission_order)
    SELECT ca.consumer_acct_id, ln_pa_id, nvl(MAX(cap.consumer_acct_permission_order), 0) + 1
  FROM pss.consumer_acct ca
  LEFT OUTER JOIN pss.consumer_acct_permission cap ON cap.consumer_acct_id = ca.consumer_acct_id
  WHERE ca.consumer_acct_type_id = 1
  AND NOT EXISTS (
    SELECT 1
    FROM pss.consumer_acct_permission cap2
    WHERE cap2.consumer_acct_id = ca.consumer_acct_id
    AND cap2.permission_action_id = ln_pa_id)
  GROUP BY ca.consumer_acct_id;
END;
COMMIT;

