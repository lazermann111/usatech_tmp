INSERT INTO REPORT.TRANS_TYPE(
	TRANS_TYPE_ID,
	TRANS_TYPE_NAME,
	PROCESS_FEE_IND,
	DISPLAY_ORDER,
	FEE_PERCENT_UBN,
	FEE_AMOUNT_UBN,
	FEE_MIN_UBN,
	FEE_PERCENT_LBN,
	PAYABLE_IND,
	CUSTOMER_DEBIT_IND,
	ENTRY_TYPE,
	FILL_BATCH_IND,
	CASH_IND,
	TRANS_TYPE_CODE,
	TRANS_ITEM_TYPE,
	REFUND_IND,
	REFUNDABLE_IND,
	CHARGEBACK_ENABLED_IND,
	STATUS_CD,
	CREDIT_IND,
	OPERATOR_TRANS_TYPE_ID,
	CASHLESS_DEVICE_TRAN_IND,
	REPLENISH_IND,
	PREPAID_IND,
	PROCESS_FEE_TRANS_TYPE_ID,
	PASS_IND,
	ACCESS_IND,
	DEBIT_IND,
	FREE_IND) 
select
	68,
	'USConnect',
	'Y',
	212,
	10,
	1,
	1,
	1,
	'N',
	'N',
	null,
	'N',
	'N',
	'P',
	'USCONNECT',
	'N',
	'Y',
	'N',
	'A',
	'N',
	68,
	'Y',
	'N',
	'N',
	68,
	'N',
	'Y',
	'N',
	'N' 
from dual
where not exists (
	select 1 
	from report.trans_type 
	where TRANS_TYPE_ID = 68);

INSERT INTO AUTHORITY.HANDLER(HANDLER_NAME, HANDLER_CLASS)
SELECT 'USConnect Apollo', 'USConnect::Apollo' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM AUTHORITY.HANDLER WHERE HANDLER_NAME = 'USConnect Apollo');

INSERT INTO AUTHORITY.AUTHORITY_TYPE(AUTHORITY_TYPE_NAME, AUTHORITY_TYPE_DESC, HANDLER_ID)
SELECT 'USConnect Apollo', 'USConnect Apollo JSON API', HANDLER_ID 
FROM AUTHORITY.HANDLER 
WHERE HANDLER_NAME = 'USConnect Apollo'
AND NOT EXISTS (SELECT 1 FROM AUTHORITY.AUTHORITY_TYPE WHERE AUTHORITY_TYPE_NAME = 'USConnect Apollo');

INSERT INTO AUTHORITY.AUTHORITY(AUTHORITY_NAME, AUTHORITY_TYPE_ID, AUTHORITY_SERVICE_ID, REMOTE_SERVER_ADDR, REMOTE_SERVER_PORT_NUM, TERMINAL_CAPTURE_FLAG, SEND_LINE_ITEMS_IND, MIN_POS_PTA_NUM, MAX_POS_PTA_NUM)
SELECT 'USConnect Apollo', AUTHORITY_TYPE_ID, 1, null, null, 'N', 'Y', null, null
FROM AUTHORITY.AUTHORITY_TYPE 
WHERE AUTHORITY_TYPE_NAME = 'USConnect Apollo'
AND NOT EXISTS (
	SELECT 1 
	FROM AUTHORITY.AUTHORITY 
	WHERE AUTHORITY_NAME = 'USConnect Apollo');

INSERT INTO PSS.MERCHANT(MERCHANT_CD, MERCHANT_NAME, MERCHANT_DESC, MERCHANT_BUS_NAME, AUTHORITY_ID, DOING_BUSINESS_AS)
SELECT '0', 'USConnect Apollo', 'USConnect Apollo Merchant', 'USA Technologies', AUTHORITY_ID, 'USA Technologies'
FROM AUTHORITY.AUTHORITY
WHERE AUTHORITY_NAME = 'USConnect Apollo'
	AND NOT EXISTS (SELECT 1 FROM PSS.MERCHANT WHERE MERCHANT_NAME = 'USConnect Apollo');

INSERT INTO PSS.TERMINAL(TERMINAL_CD, TERMINAL_NEXT_BATCH_NUM, MERCHANT_ID, TERMINAL_DESC, TERMINAL_STATE_ID,
	TERMINAL_MAX_BATCH_NUM, TERMINAL_BATCH_MAX_TRAN, TERMINAL_BATCH_CYCLE_NUM, TERMINAL_MIN_BATCH_NUM,
	TERMINAL_MIN_BATCH_CLOSE_HR, TERMINAL_MAX_BATCH_CLOSE_HR, TERMINAL_BATCH_MIN_TRAN)
SELECT '0', 1, MERCHANT_ID, 'USConnect Apollo Terminal', 1, 999, 1, 1, 1, 0, 4, 1
FROM PSS.MERCHANT
WHERE MERCHANT_NAME = 'USConnect Apollo'
	AND NOT EXISTS (SELECT 1 FROM PSS.TERMINAL WHERE TERMINAL_DESC = 'USConnect Apollo Terminal');
	
INSERT INTO pss.authority_payment_mask(authority_payment_mask_name, authority_payment_mask_regex, authority_payment_mask_bref, card_name)
SELECT 'Generic Special Card Track 2 (with Sprout)', '^;?(?!(?:4[0-9]{15,18}|5[1-5][0-9]{14}|3[47][0-9]{13}|(?:30[0-5][0-9]|3095|35[2-8][0-9]|36|3[8-9][0-9]{2}|6011|622[1-9]|62[4-6][0-9]|628[2-8]|64[4-9][0-9]|65[0-9]{2})[0-9]{12}|639621[0-9]{10,13})(?:=[0-9]+|$))([0-9]{5,})(?:=[0-9]*)*(?:\?([\x00-\xFF])?)?$', '1:1', null FROM dual
WHERE NOT EXISTS (SELECT 1 FROM pss.authority_payment_mask WHERE authority_payment_mask_name = 'Generic Special Card Track 2 (with Sprout)');

INSERT INTO PSS.PAYMENT_SUBTYPE(PAYMENT_SUBTYPE_NAME, PAYMENT_SUBTYPE_CLASS, PAYMENT_SUBTYPE_KEY_NAME, CLIENT_PAYMENT_TYPE_CD, PAYMENT_SUBTYPE_TABLE_NAME, PAYMENT_SUBTYPE_KEY_DESC_NAME, AUTHORITY_PAYMENT_MASK_ID, TRANS_TYPE_ID, CARD_TYPE_LABEL)
	SELECT 'USConnect Apollo (Track 2)', 'USConnect::Apollo', 'TERMINAL_ID', 'S', 'TERMINAL', 'TERMINAL_DESC', AUTHORITY_PAYMENT_MASK_ID, 68, 'USConnect'
	FROM PSS.AUTHORITY_PAYMENT_MASK
	WHERE AUTHORITY_PAYMENT_MASK_NAME = 'Generic Special Card Track 2 (with Sprout)'
	AND NOT EXISTS (SELECT 1 FROM PSS.PAYMENT_SUBTYPE PST0 WHERE PST0.PAYMENT_SUBTYPE_NAME = 'USConnect Apollo (Track 2)');

INSERT INTO PSS.POS_PTA_TMPL(POS_PTA_TMPL_NAME, POS_PTA_TMPL_DESC)
SELECT 'SPECIAL MERGE: USConnect Apollo', 'USConnect Card payment template' 
FROM DUAL
WHERE NOT EXISTS (
	SELECT 1 
	FROM PSS.POS_PTA_TMPL 
	WHERE POS_PTA_TMPL_NAME = 'SPECIAL MERGE: USConnect Apollo');

INSERT INTO PSS.POS_PTA_TMPL_ENTRY(POS_PTA_TMPL_ID, PAYMENT_SUBTYPE_ID, POS_PTA_ACTIVATION_OSET_HR, PAYMENT_SUBTYPE_KEY_ID, POS_PTA_PRIORITY, AUTHORITY_PAYMENT_MASK_ID, CURRENCY_CD, POS_PTA_PASSTHRU_ALLOW_YN_FLAG, NO_CONVENIENCE_FEE)
SELECT 
	(SELECT pos_pta_tmpl_id FROM pss.pos_pta_tmpl WHERE pos_pta_tmpl_name = 'SPECIAL MERGE: USConnect Apollo'), 
	(SELECT PAYMENT_SUBTYPE_ID FROM PSS.PAYMENT_SUBTYPE WHERE PAYMENT_SUBTYPE_NAME = 'USConnect Apollo (Track 2)'),
	0, 
	(SELECT TERMINAL_ID FROM PSS.TERMINAL WHERE TERMINAL_DESC = 'USConnect Apollo Terminal'),
	1, 
	(SELECT AUTHORITY_PAYMENT_MASK_ID FROM PSS.AUTHORITY_PAYMENT_MASK WHERE AUTHORITY_PAYMENT_MASK_NAME = 'Generic Special Card Track 2 (with Sprout)'),
	'USD', 
	'N',
	'Y'
FROM DUAL
WHERE NOT EXISTS (
	SELECT 1 
	FROM PSS.POS_PTA_TMPL_ENTRY 
	WHERE POS_PTA_TMPL_ID = (
		SELECT POS_PTA_TMPL_ID 
		FROM PSS.POS_PTA_TMPL 
		WHERE POS_PTA_TMPL_NAME = 'SPECIAL MERGE: USConnect Apollo')
		AND PAYMENT_SUBTYPE_ID = (
			SELECT PAYMENT_SUBTYPE_ID 
			FROM PSS.PAYMENT_SUBTYPE 
			WHERE PAYMENT_SUBTYPE_NAME = 'USConnect Apollo (Track 2)'));

INSERT INTO REPORT.CARD_TYPE(CARD_TYPE_ID, CARD_NAME, PATTERN) 
SELECT CORP.CARD_TYPE_SEQ.NEXTVAL, 'USConnect', '*' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM REPORT.CARD_TYPE WHERE CARD_NAME = 'USConnect');

INSERT INTO REPORT.CARDTYPE_AUTHORITY(CARDTYPE_AUTHORITY_ID, CARDTYPE_ID, AUTHORITY_ID)
SELECT (SELECT MAX(CARDTYPE_AUTHORITY_ID) + 1 FROM REPORT.CARDTYPE_AUTHORITY),
	(SELECT CARD_TYPE_ID FROM REPORT.CARD_TYPE WHERE CARD_NAME = 'USConnect'),
	(SELECT AUTHORITY_ID FROM REPORT.AUTHORITY WHERE ALIASNAME = 'Generic Authority')
FROM DUAL WHERE NOT EXISTS (
	SELECT 1 FROM REPORT.CARDTYPE_AUTHORITY 
	WHERE CARDTYPE_ID = (SELECT CARD_TYPE_ID FROM REPORT.CARD_TYPE WHERE CARD_NAME = 'USConnect'));

COMMIT;

INSERT INTO REPORT.REPORTS(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
SELECT 472, 'USConnect Devices', 6, 0, 'USConnect Devices', 'List of devices with USConnect payment type', 'U', USER_ID
FROM REPORT.USER_LOGIN 
WHERE USER_NAME = 'USATMaster'
 AND NOT EXISTS(
SELECT 1
FROM REPORT.REPORTS 
WHERE REPORT_ID = 472);

INSERT INTO REPORT.REPORT_PARAM(REPORT_ID, PARAM_NAME, PARAM_VALUE)
SELECT 472, PARAM_NAME, PARAM_VALUE
  FROM (SELECT '' PARAM_NAME, '' PARAM_VALUE FROM DUAL WHERE 1=0
  UNION ALL SELECT 'query', 'SELECT DISTINCT C.CUSTOMER_NAME, C.CUSTOMER_ID, D.DEVICE_SERIAL_CD
FROM DEVICE.DEVICE D
JOIN DEVICE.DEVICE_LAST_ACTIVE DLA ON D.DEVICE_ID = DLA.DEVICE_ID
JOIN PSS.POS P ON D.DEVICE_ID = P.DEVICE_ID
JOIN PSS.POS_PTA PP ON P.POS_ID = PP.POS_ID
JOIN PSS.PAYMENT_SUBTYPE PS ON PP.PAYMENT_SUBTYPE_ID = PS.PAYMENT_SUBTYPE_ID
LEFT JOIN (REPORT.EPORT E 
JOIN REPORT.VW_TERMINAL_EPORT TE ON E.EPORT_ID = TE.EPORT_ID
JOIN REPORT.TERMINAL T ON TE.TERMINAL_ID = T.TERMINAL_ID
JOIN CORP.CUSTOMER C ON T.CUSTOMER_ID = C.CUSTOMER_ID) ON D.DEVICE_SERIAL_CD = E.EPORT_SERIAL_NUM
WHERE PS.PAYMENT_SUBTYPE_CLASS = ''USConnect::Apollo'' AND PP.POS_PTA_ACTIVATION_TS < SYSDATE
AND (PP.POS_PTA_DEACTIVATION_TS IS NULL OR PP.POS_PTA_DEACTIVATION_TS > SYSDATE)
ORDER BY UPPER(C.CUSTOMER_NAME), D.DEVICE_SERIAL_CD' FROM DUAL
UNION ALL SELECT 'header', 'true' FROM DUAL
UNION ALL SELECT 'isSQLFolio', 'true' FROM DUAL
UNION ALL SELECT 'outputType', '21' FROM DUAL 
  ) P
 WHERE NOT EXISTS(
 SELECT 1
	FROM REPORT.REPORT_PARAM RP0 
   WHERE RP0.REPORT_ID = 472
	 AND RP0.PARAM_NAME = P.PARAM_NAME);
	 
COMMIT;
