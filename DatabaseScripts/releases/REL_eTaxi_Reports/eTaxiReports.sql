GRANT SELECT ON PSS.AUTH TO REPORT;
GRANT SELECT ON PSS.TRAN TO REPORT;
ALTER TABLE REPORT.TRANS ADD ROYALTY_FEE_FLAG VARCHAR2(1);
ALTER TABLE REPORT.EPORT ADD (FIRST_CASHLESS_TRAN_RECORD_DT DATE, CASHLESS_TRAN_COUNT NUMBER(20,0));
ALTER TABLE REPORT.REPORTS ADD LATENCY NUMBER;
ALTER TABLE REPORT.REPORTS MODIFY TITLE VARCHAR2(100);

ALTER TABLE REPORT.EPORT MODIFY EPORT_SERIAL_NUM VARCHAR2(60);

DROP INDEX DEVICE.IDX_DEVICE_2;
ALTER TABLE DEVICE.DEVICE MODIFY DEVICE_SERIAL_CD VARCHAR2(60);
CREATE UNIQUE INDEX DEVICE.UDX_DEVICE_ACTIVE_SERIAL ON DEVICE.DEVICE(DECODE(DEVICE_ACTIVE_YN_FLAG, 'Y', DEVICE_SERIAL_CD, NULL)) TABLESPACE DEVICE_INDX ONLINE;

ALTER TABLE DEVICE.DEVICE_LAST_ACTIVE MODIFY DEVICE_SERIAL_CD VARCHAR2(60);

UPDATE DEVICE.DEVICE_TYPE SET DEVICE_TYPE_SERIAL_CD_REGEX = '^K(1[0-9A-F]{16}|[23][A-Z]{2}[0-9A-Z]{6,26})$' WHERE DEVICE_TYPE_ID = 11;
COMMIT;

DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := -1;
BEGIN
	SELECT REPORT.REPORTS_SEQ.NEXTVAL INTO LN_REPORT_ID FROM DUAL WHERE NOT EXISTS (
		SELECT 1 FROM REPORT.REPORTS WHERE REPORT_NAME = 'eTaxi Device Activations'
	);

	IF LN_REPORT_ID < 1 THEN
		RETURN;
	END IF;
		
	INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID, LATENCY)
	SELECT LN_REPORT_ID, 'eTaxi Device Activations - {b} to {e}', 1, 0, 'eTaxi Device Activations', 'eTaxi Device Activations report', 'N', USER_ID, 1
	FROM REPORT.USER_LOGIN
	WHERE USER_NAME = 'USATMaster';

	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
	VALUES(LN_REPORT_ID, 'query', 'SELECT EPORT_SERIAL_NUM, TO_CHAR(FIRST_CASHLESS_TRAN_RECORD_DT, ''MM/DD/YYYY'') FIRST_TRAN_DATE
		FROM REPORT.EPORT
		WHERE EPORT_SERIAL_NUM LIKE ''K3ET%''
		AND FIRST_CASHLESS_TRAN_RECORD_DT >= CAST(? AS DATE) AND FIRST_CASHLESS_TRAN_RECORD_DT < CAST(? AS DATE) + 1
		ORDER BY EPORT_SERIAL_NUM
');
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'header', 'true');
	insert into report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) values(LN_REPORT_ID, 'dateFormat', 'MM/dd/yyyy');
	insert into report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) values(LN_REPORT_ID, 'params.StartDate', '{b}');
	insert into report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) values(LN_REPORT_ID, 'params.EndDate', '{e}');
	insert into report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) values(LN_REPORT_ID, 'paramNames', 'StartDate,EndDate');
	insert into report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) values(LN_REPORT_ID, 'paramTypes', 'TIMESTAMP,TIMESTAMP');
		
	insert into web_content.web_link(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER) values(web_content.seq_web_link_id.nextval, 'eTaxi Device Activations', './select_date_range_frame_nonfolio.i?basicReportId=' || LN_REPORT_ID, 'eTaxi Device Activations report', 'Accounting', 'W', 0);
	insert into web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) select web_link_id, 1 from web_content.web_link where web_link_label = 'eTaxi Device Activations';
	insert into web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) select web_link_id, 14 from web_content.web_link where web_link_label = 'eTaxi Device Activations';

	COMMIT;
END;
/

DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := -1;
BEGIN
	SELECT REPORT.REPORTS_SEQ.NEXTVAL INTO LN_REPORT_ID FROM DUAL WHERE NOT EXISTS (
		SELECT 1 FROM REPORT.REPORTS WHERE REPORT_NAME = 'eTaxi Royalty Transaction Counts'
	);

	IF LN_REPORT_ID < 1 THEN
		RETURN;
	END IF;
		
	INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID, LATENCY)
	SELECT LN_REPORT_ID, 'eTaxi Royalty Transaction Counts - {b} to {e}', 1, 0, 'eTaxi Royalty Transaction Counts', 'eTaxi Royalty Transaction Counts report', 'N', USER_ID, 1
	FROM REPORT.USER_LOGIN
	WHERE USER_NAME = 'USATMaster';

	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
	VALUES(LN_REPORT_ID, 'query', 'SELECT /*+INDEX(X IDX_TRANS_SETTLE_DATE_APPR_CD)*/ E.EPORT_SERIAL_NUM, COUNT(1) TRAN_COUNT
		FROM REPORT.TRANS X
		JOIN REPORT.EPORT E ON X.EPORT_ID = E.EPORT_ID
		WHERE X.SETTLE_DATE >= CAST(? AS DATE) AND X.SETTLE_DATE < CAST(? AS DATE) + 1 AND X.ROYALTY_FEE_FLAG = ''Y''
		AND E.EPORT_SERIAL_NUM LIKE ''K3ET%''
		GROUP BY E.EPORT_SERIAL_NUM
		ORDER BY E.EPORT_SERIAL_NUM
');
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) VALUES(LN_REPORT_ID, 'header', 'true');
	insert into report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) values(LN_REPORT_ID, 'dateFormat', 'MM/dd/yyyy');
	insert into report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) values(LN_REPORT_ID, 'params.StartDate', '{b}');
	insert into report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) values(LN_REPORT_ID, 'params.EndDate', '{e}');
	insert into report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) values(LN_REPORT_ID, 'paramNames', 'StartDate,EndDate');
	insert into report.report_param(REPORT_ID, PARAM_NAME, PARAM_VALUE) values(LN_REPORT_ID, 'paramTypes', 'TIMESTAMP,TIMESTAMP');
		
	insert into web_content.web_link(WEB_LINK_ID, WEB_LINK_LABEL, WEB_LINK_URL, WEB_LINK_DESC, WEB_LINK_GROUP, WEB_LINK_USAGE, WEB_LINK_ORDER) values(web_content.seq_web_link_id.nextval, 'eTaxi Royalty Transaction Counts', './select_date_range_frame_nonfolio.i?basicReportId=' || LN_REPORT_ID, 'eTaxi Royalty Transaction Counts report', 'Accounting', 'W', 0);
	insert into web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) select web_link_id, 1 from web_content.web_link where web_link_label = 'eTaxi Royalty Transaction Counts';
	insert into web_content.web_link_requirement(WEB_LINK_ID, REQUIREMENT_ID) select web_link_id, 14 from web_content.web_link where web_link_label = 'eTaxi Royalty Transaction Counts';

	COMMIT;
END;
/
