CREATE TABLE CORP.APP_SETTING (
    APP_SETTING_CD VARCHAR2(100) NOT NULL,
    APP_SETTING_VALUE VARCHAR2(4000),
	APP_SETTING_DESC VARCHAR2(4000) NOT NULL,
    CREATED_BY VARCHAR2(30) NOT NULL,
    CREATED_UTC_TS TIMESTAMP NOT NULL,
    LAST_UPDATED_BY VARCHAR2(30) NOT NULL,
    LAST_UPDATED_UTC_TS TIMESTAMP NOT NULL,
    CONSTRAINT PK_APP_SETTING PRIMARY KEY(APP_SETTING_CD)
) TABLESPACE CORP_DATA;

CREATE OR REPLACE TRIGGER CORP.TRBI_APP_SETTING BEFORE INSERT ON CORP.APP_SETTING
  FOR EACH ROW
BEGIN
    SELECT 
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER,
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER
    INTO 
        :NEW.CREATED_UTC_TS,
        :NEW.CREATED_BY,
        :NEW.LAST_UPDATED_UTC_TS,
        :NEW.LAST_UPDATED_BY
    FROM DUAL;
END; 
/

CREATE OR REPLACE TRIGGER CORP.TRBU_APP_SETTING BEFORE UPDATE ON CORP.APP_SETTING
  FOR EACH ROW 
BEGIN
    SELECT
        :OLD.CREATED_BY,
        :OLD.CREATED_UTC_TS,
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER
    INTO
        :NEW.CREATED_BY,
        :NEW.CREATED_UTC_TS,
        :NEW.LAST_UPDATED_UTC_TS,
        :NEW.LAST_UPDATED_BY
    FROM DUAL;
END; 
/

insert into CORP.APP_SETTING (app_setting_cd,app_setting_value, app_setting_desc)
values('GET_UNSENT_TRAN_BATCHES_REPORT_REQUEST_LOCK',null,'Record for locking batch trans report request');

insert into CORP.APP_SETTING (app_setting_cd,app_setting_value, app_setting_desc)
values('GET_UNSENT_TRAN_BATCHES_REPORT_REQUEST_MAX_DURATION_SEC',3600,'Number of seconds for generating the batch trans report request by report requester.');

insert into CORP.APP_SETTING (app_setting_cd,app_setting_value, app_setting_desc)
values('GET_UNSENT_DEX_BATCHES_REPORT_REQUEST_LOCK',null,'Record for locking batch dex report request');

insert into CORP.APP_SETTING (app_setting_cd,app_setting_value, app_setting_desc)
values('GET_UNSENT_DEX_BATCHES_REPORT_REQUEST_MAX_DURATION_SEC',3600,'Number of seconds for generating the batch dex report request by report requester.');

insert into CORP.APP_SETTING (app_setting_cd,app_setting_value, app_setting_desc)
values('GET_UNSENT_DOC_BATCHES_REPORT_REQUEST_LOCK',null,'Record for locking batch payment report request');

insert into CORP.APP_SETTING (app_setting_cd,app_setting_value, app_setting_desc)
values('GET_UNSENT_DOC_BATCHES_REPORT_REQUEST_MAX_DURATION_SEC',3600,'Number of seconds for generating the batch payment report request by report requester.');

insert into CORP.APP_SETTING (app_setting_cd,app_setting_value, app_setting_desc)
values('GET_UNSENT_LOCKED_DOC_BATCHES_REPORT_REQUEST_LOCK',null,'Record for locking batch pending payment report request');

insert into CORP.APP_SETTING (app_setting_cd,app_setting_value, app_setting_desc)
values('GET_UNSENT_LOCKED_DOC_BATCHES_REPORT_REQUEST_MAX_DURATION_SEC',3600,'Number of seconds for generating the batch pending payment report request by report requester.');

insert into CORP.APP_SETTING (app_setting_cd,app_setting_value, app_setting_desc)
values('GET_TIMED_REPORTS_REPORT_REQUEST_LOCK',null,'Record for locking unsent timed report request');

insert into CORP.APP_SETTING (app_setting_cd,app_setting_value, app_setting_desc)
values('GET_TIMED_REPORTS_REPORT_REQUEST_MAX_DURATION_SEC',3600,'Number of seconds for generating the unsent timed report request by report requester.');

insert into CORP.APP_SETTING (app_setting_cd,app_setting_value, app_setting_desc)
values('GET_RESENT_TIMED_REPORTS_REPORT_REQUEST_LOCK',null,'Record for locking resend timed report request');

insert into CORP.APP_SETTING (app_setting_cd,app_setting_value, app_setting_desc)
values('GET_RESENT_TIMED_REPORTS_REPORT_REQUEST_MAX_DURATION_SEC',3600,'Number of seconds for generating the resend timed report request by report requester.');

COMMIT;
