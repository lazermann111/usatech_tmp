-- add dex status report to report register
INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
VALUES( REPORT.REPORTS_SEQ.NEXTVAL, 'Dex Status {b} to {e}', 6, 0, 'Dex Status', 'Dex Status Report contains location, address, city, state, dex alert count and dex exception count.', 'E', 0);
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES(REPORT.REPORTS_SEQ.CURRVAL, 'dateFormat', 'MM-dd-yyyy');
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES(REPORT.REPORTS_SEQ.CURRVAL, 'folioId', '1116');
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES(REPORT.REPORTS_SEQ.CURRVAL, 'outputType', '27');
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES(REPORT.REPORTS_SEQ.CURRVAL, 'params.beginDate', '{b}');
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES(REPORT.REPORTS_SEQ.CURRVAL, 'params.endDate', '{e}');
commit;

-- add dex exception report to report register
INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
VALUES( REPORT.REPORTS_SEQ.NEXTVAL, 'Dex Exceptions {b} to {e}', 6, 0, 'Dex Exceptions', 'Dex Exceptions Report contains location, address, city, state, dex alert count and dex exception count. The report will only show Dex records that contain Dex exception.', 'E', 0);
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES(REPORT.REPORTS_SEQ.CURRVAL, 'dateFormat', 'MM-dd-yyyy');
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES(REPORT.REPORTS_SEQ.CURRVAL, 'folioId', '1263');
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES(REPORT.REPORTS_SEQ.CURRVAL, 'outputType', '27');
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES(REPORT.REPORTS_SEQ.CURRVAL, 'params.beginDate', '{b}');
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES(REPORT.REPORTS_SEQ.CURRVAL, 'params.endDate', '{e}');
commit;

-- BEGIN add pending payment summary report to report register
insert into report.export_type (export_type_id, name, description, status)
values(9, 'CONDITION', 'Condition based', 'A');

INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
VALUES( REPORT.REPORTS_SEQ.NEXTVAL, 'Pending Payment Summary-{x}-{d}', 6, 9, 'Pending Payment Summary', 'Pending Payment summary report for the bank account, checked daily at 6:00 am EST, based on the pay cycle, triggered when the EFT balance is below $25.', 'E', 0);
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES(REPORT.REPORTS_SEQ.CURRVAL, 'dateFormat', 'MM-dd-yyyy');
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES(REPORT.REPORTS_SEQ.CURRVAL, 'reportId', '5');
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES(REPORT.REPORTS_SEQ.CURRVAL, 'outputType', '27');
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES(REPORT.REPORTS_SEQ.CURRVAL, 'params.customerBankId', '{x}');

commit;

GRANT SELECT ON CORP.BATCH to USAT_RPT_REQ_ROLE;
GRANT SELECT ON DEVICE.DEVICE to USAT_RPT_REQ_ROLE;
GRANT SELECT ON DEVICE.DEVICE_FILE_TRANSFER to USAT_RPT_REQ_ROLE;
-- END add pending payment summary report to report register

insert into web_content.requirement( requirement_id, requirement_name, requirement_class, requirement_param_class, requirement_param_value)
values(34, 'REQ_COLUMN_MAPPINGS', 'com.usatech.usalive.link.HasPrivilegeRequirement', 'java.lang.String', 'PRIV_COLUMN_MAPPINGS');

insert into web_content.web_link values(web_content.seq_web_link_id.nextval,'Column Mappings', './column_mapping.i','Column Mappings', 'Administration','M',146);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval,1);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval,34);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval,31);
commit;

SET DEFINE OFF;
update web_content.literal set literal_value='We at USA Technologies, Inc. are continually striving to enhance our customers'' power to run their business effectively. The USALive Website is loaded with features to help you do just that. For the best web site experience, please use the latest major version of your web browser. Now new in Version 1.10: <ul><li>Manage your machine column mappings in USALive</li><li>Display ISIS Mobile Wallet transactions</li><li>Feature-enhanced Report Register</li><li>Additional DEX reports in Report Register</li><li>More detailed location information in some DEX reports</li><li>Resend multiple reports at a time instead of one by one</li><li>Email fields allow Internet Top Level Domains (.com, .net, .org, etc) greater than three characters</li><li>Performance improvements to the reporting engine</li></ul>' where literal_key='home-page-message' and subdomain_id is null;
commit;


