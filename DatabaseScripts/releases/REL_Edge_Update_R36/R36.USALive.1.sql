update web_content.literal set literal_value='We at USA Technologies, Inc. are continually striving to enhance our customers'' power to run their business effectively. The USALive Website is loaded with features to help you do just that. For the best web site experience, please use the latest major version of your browser. Now new in Version 1.12: <ul><li>Improved terminal selection for various reports</li><li>Added a "Refresh Data" icon and function to reports</li><li>Improve conditions for determining a completed Fill Batch</li><li>Minimum Gx firmware version supporting two-tier pricing is now 6.0.4</li><li>Show default values as grayed-out disabled text boxes if the firmware revision does not support those parameters</li><li>Added Quick Connect Information section for devices using the web service to provide information related to password generation and password update status for that device</li><li>Significant changes to the More PrePaid campaign and assignment pages</li><li>Minor bug fixes</li></ul>' where literal_key='home-page-message' and subdomain_id is null;
commit;

ALTER TABLE REPORT.CAMPAIGN ADD(
 	RECUR_SCHEDULE VARCHAR2(4000),
    THRESHOLD_AMOUNT NUMBER(8,2)
);
     
update REPORT.CAMPAIGN_TYPE set campaign_type_name='Loyalty Discount' where campaign_type_id=1; 

INSERT INTO REPORT.CAMPAIGN_TYPE(CAMPAIGN_TYPE_ID, CAMPAIGN_TYPE_NAME, CAMPAIGN_TYPE_DESC) 
values(2, 'Replenish Bonus', 'Campaign that offers replenish bonus, such as for every $20 replenishment, you get a certain percentage cash amount back.');

INSERT INTO REPORT.CAMPAIGN_TYPE(CAMPAIGN_TYPE_ID, CAMPAIGN_TYPE_NAME, CAMPAIGN_TYPE_DESC) 
values(3, 'Bonus Cash', 'Campaign that offers bonus cash, such as for every $20 spent, you get a certain percentage cash amount back.');

commit;

INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, LITERAL_VALUE)
    VALUES('campaign-day-of-month-instructions-title', 'Day of Month');
INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, LITERAL_VALUE)
    VALUES('campaign-day-of-month-instructions-content', 'Required when Day of Month is selected. Campaign will only apply on the indicated days. You can use single day or duration of days separated by comma or semicolon.Valid days are 1-31. eg. 1,2,20-31');
INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, LITERAL_VALUE)
    VALUES('campaign-day-of-week-instructions-title', 'Day of Week');
INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, LITERAL_VALUE)
    VALUES('campaign-day-of-week-instructions-content', 'Required when Day of Week is selected. Campaign will only apply on the indicated days in the week. Use the checkbox to select the days.');
INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, LITERAL_VALUE)
    VALUES('campaign-specific-days-instructions-title', 'Specific Days');
INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, LITERAL_VALUE)
    VALUES('campaign-specific-days-instructions-content', 'Required when Specific Days is selected. Campaign will only apply on the indicated specific days. You can use date in MM/DD/YYYY and sperated by comma or semicolon for multiple dates. eg. 01/01/2014,01/02/2014');
INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, LITERAL_VALUE)
    VALUES('campaign-hour-instructions-title', 'Hours');
INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, LITERAL_VALUE)
    VALUES('campaign-hour-instructions-content', 'Not required. Empty value means all hours. You can use 24hr format or 12hr format for duration separated by comma or semicolon. eg. 09:00-11:00,13:00-17:00');
INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, LITERAL_VALUE)
    VALUES('campaign-device-search-title', 'Device Filter');
INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, LITERAL_VALUE)
    VALUES('campaign-device-search-content', 'Select filter creteria ''By Serial Number'',''By Location'', or ''By Asset Nbr'' then input comma delimited values to filter device for search results. eg. EE100000001,EE100000002');

commit;

ALTER TABLE PSS.CONSUMER_ACCT ADD(
    TOWARD_CASH_BACK_BALANCE NUMBER, 
    CASH_BACK_TOTAL NUMBER, 
    TOWARD_REPLENISH_BONUS_BALANCE NUMBER,
    REPLENISH_BONUS_TOTAL NUMBER);
    
INSERT INTO PSS.TRAN_LINE_ITEM_TYPE(TRAN_LINE_ITEM_TYPE_ID, TRAN_LINE_ITEM_TYPE_DESC, TRAN_LINE_ITEM_TYPE_SIGN_PN, TRAN_LINE_ITEM_TYPE_GROUP_CD)
    VALUES(554, 'Prepaid Card Bonus Cash', 'P', 'S');

INSERT INTO PSS.TRAN_LINE_ITEM_TYPE(TRAN_LINE_ITEM_TYPE_ID, TRAN_LINE_ITEM_TYPE_DESC, TRAN_LINE_ITEM_TYPE_SIGN_PN, TRAN_LINE_ITEM_TYPE_GROUP_CD)
    VALUES(555, 'Prepaid Card Replenish Bonus', 'P', 'S');

COMMIT;

INSERT INTO WEB_CONTENT.SUBDOMAIN(SUBDOMAIN_ID, SUBDOMAIN_URL, SUBDOMAIN_DESCRIPTION)
    SELECT WEB_CONTENT.SEQ_SUBDOMAIN_ID.NEXTVAL, 'getmore' || URL_SUFFIX || '.usatech.com', 'GetMore ' || DESC_SUFFIX
      FROM (
            SELECT DECODE(NAME, 'USAPDB', '', 'ECCDB', '-ecc', 'USADEV02', '-int', 'USADEV03', '-dev', 'USADEV04', '-local') URL_SUFFIX,
                   DECODE(NAME, 'USAPDB', '', 'ECCDB', ' Ecc', 'USADEV02', ' Int', 'USADEV03', ' Dev', 'USADEV04', ' Local') DESC_SUFFIX
              FROM V$DATABASE
             WHERE NOT EXISTS(SELECT 1 FROM WEB_CONTENT.SUBDOMAIN WHERE SUBDOMAIN_DESCRIPTION LIKE 'GetMore %')
      );

INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, SUBDOMAIN_ID, LOCALE_CD, LITERAL_VALUE)
    SELECT 'consumer-comm-email-from-addr', SD.SUBDOMAIN_ID, L.LOCALE_CD, L.LITERAL_VALUE
      FROM WEB_CONTENT.SUBDOMAIN SD, WEB_CONTENT.LITERAL L
     WHERE SD.SUBDOMAIN_DESCRIPTION LIKE 'GetMore %'
       AND L.SUBDOMAIN_ID IS NULL
       AND L.LITERAL_KEY = 'prepaid-email-from-addr';

INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, SUBDOMAIN_ID, LOCALE_CD, LITERAL_VALUE)
    SELECT 'consumer-comm-email-from-name', SD.SUBDOMAIN_ID, L.LOCALE_CD, L.LITERAL_VALUE
      FROM WEB_CONTENT.SUBDOMAIN SD, WEB_CONTENT.LITERAL L
     WHERE SD.SUBDOMAIN_DESCRIPTION LIKE 'GetMore %'
       AND L.SUBDOMAIN_ID IS NULL
       AND L.LITERAL_KEY = 'prepaid-email-from-name';

INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, SUBDOMAIN_ID, LOCALE_CD, LITERAL_VALUE)
    SELECT 'consumer-comm-subject-cash-back-on-purchase', SD.SUBDOMAIN_ID, NULL, 'You earned MORE!'
      FROM WEB_CONTENT.SUBDOMAIN SD
     WHERE SD.SUBDOMAIN_DESCRIPTION LIKE 'GetMore %';

INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, SUBDOMAIN_ID, LOCALE_CD, LITERAL_VALUE)
    SELECT 'consumer-comm-subject-replenish-bonus', SD.SUBDOMAIN_ID, NULL, 'You earned a MORE!'
      FROM WEB_CONTENT.SUBDOMAIN SD
     WHERE SD.SUBDOMAIN_DESCRIPTION LIKE 'GetMore %';

INSERT INTO WEB_CONTENT.LITERAL(LITERAL_KEY, SUBDOMAIN_ID, LOCALE_CD, LITERAL_VALUE)
    SELECT 'consumer-comm-subject-replenish-disabled', SD.SUBDOMAIN_ID, NULL, 'Please correct replenish settings for MORE!'
      FROM WEB_CONTENT.SUBDOMAIN SD
     WHERE SD.SUBDOMAIN_DESCRIPTION LIKE 'GetMore %';
                
COMMIT;

ALTER TABLE PSS.CONSUMER_SETTING_PARAM ADD (CONSUMER_COMMUNICATION_TYPE VARCHAR2(30));

CREATE UNIQUE INDEX PSS.CSP_CONSUMER_COMM_TYPE ON PSS.CONSUMER_SETTING_PARAM(CONSUMER_COMMUNICATION_TYPE);

INSERT INTO PSS.CONSUMER_SETTING_PARAM(CONSUMER_SETTING_PARAM_ID,DISPLAY_CONFIGURABLE_CD,DISPLAY_LABEL,DISPLAY_GROUP,EDITOR,CONVERTER,CONSUMER_COMMUNICATION_TYPE) 
    VALUES(2, 'P', 'Email Me When I Earn Bonus Cash', 'Preferences', 'CHECKBOX:?Y=', null, 'CASH_BACK_ON_PURCHASE');

INSERT INTO PSS.CONSUMER_SETTING_PARAM(CONSUMER_SETTING_PARAM_ID,DISPLAY_CONFIGURABLE_CD,DISPLAY_LABEL,DISPLAY_GROUP,EDITOR,CONVERTER,CONSUMER_COMMUNICATION_TYPE) 
    VALUES(3, 'P', 'Email Me When I Earn a Replenish Bonus', 'Preferences', 'CHECKBOX:?Y=', null, 'REPLENISH_BONUS');

INSERT INTO PSS.CONSUMER_SETTING_PARAM(CONSUMER_SETTING_PARAM_ID,DISPLAY_CONFIGURABLE_CD,DISPLAY_LABEL,DISPLAY_GROUP,EDITOR,CONVERTER,CONSUMER_COMMUNICATION_TYPE) 
    VALUES(4, 'P', 'Email Me About Replenish Issues', 'Preferences', 'CHECKBOX:?Y=', null, 'REPLENISH_DISABLED');

COMMIT;

GRANT SELECT ON PSS.TRAN_LINE_ITEM TO USAT_IN_AUTH_LAYER_ROLE;
GRANT SELECT ON PSS.CAMPAIGN_POS_PTA TO USAT_IN_AUTH_LAYER_ROLE;
GRANT SELECT ON REPORT.CAMPAIGN TO USAT_IN_AUTH_LAYER_ROLE;
GRANT SELECT ON PSS.CAMPAIGN_CONSUMER_ACCT TO USAT_IN_AUTH_LAYER_ROLE;
GRANT SELECT ON LOCATION.LOCATION TO USAT_IN_AUTH_LAYER_ROLE;
GRANT SELECT ON LOCATION.TIME_ZONE TO USAT_IN_AUTH_LAYER_ROLE;
GRANT SELECT ON PSS.CONSUMER_SETTING_PARAM TO USAT_RPT_GEN_ROLE;

ALTER TABLE REPORT.PURCHASE MODIFY(AMOUNT NUMBER);
   
INSERT INTO FOLIO_CONF.FILTER(FILTER_ID, FIELD_ID, FILTER_OPERATOR_ID, FILTER_GROUP_ID)
    SELECT FILTER_ID + 8, FIELD_ID, 9, FILTER_GROUP_ID
      FROM FOLIO_CONF.FILTER
     WHERE FILTER_GROUP_ID IN(1,100);
     
UPDATE FOLIO_CONF.FILTER_GROUP 
   SET SEPARATOR = 'OR'
 WHERE FILTER_GROUP_ID IN(1,100)
   AND SEPARATOR != 'OR';

COMMIT;

ALTER TABLE PSS.CAMPAIGN_POS_PTA ADD(
    PRIORITY NUMBER(3) DEFAULT 0 NOT NULL,
    TERMINAL_EPORT_ID NUMBER);

DROP INDEX PSS.UDX_CAMPAIGN_POS_PTA_ID;

CREATE INDEX PSS.IX_CPP_POS_PTA_ID ON PSS.CAMPAIGN_POS_PTA(POS_PTA_ID);

DROP INDEX PSS.UDX_POS_PTA_CAMPAIGN_ID;

CREATE INDEX PSS.IX_CPP_CAMPAIGN_ID ON PSS.CAMPAIGN_POS_PTA(CAMPAIGN_ID);

ALTER TABLE PSS.TRAN_LINE_ITEM ADD(CAMPAIGN_ID NUMBER) ;

ALTER TABLE PSS.CONSUMER_ACCT_REPLENISH ADD(REPLENISH_DENIED_COUNT NUMBER(3) DEFAULT 0 NOT NULL);

INSERT INTO CORP.APP_SETTING(APP_SETTING_CD, APP_SETTING_VALUE, APP_SETTING_DESC)
  SELECT APP_SETTING_CD, APP_SETTING_VALUE, REPLACE(REPLACE(APP_SETTING_DESC, 'AppLayer', 'Report Requester'), 'App Layer', 'Report Requester')
    FROM ENGINE.APP_SETTING where APP_SETTING_CD like 'MAIL_FORWARD%';

COMMIT;

GRANT SELECT, UPDATE, DELETE ON ENGINE.OB_EMAIL_QUEUE TO USAT_RPT_REQ_ROLE;
GRANT SELECT ON PSS.POS_PTA TO USALIVE_APP_ROLE;
GRANT SELECT ON PSS.POS TO USALIVE_APP_ROLE;
GRANT SELECT ON LOCATION.LOCATION TO USALIVE_APP_ROLE;
GRANT SELECT ON LOCATION.TIME_ZONE TO USALIVE_APP_ROLE;
GRANT SELECT, REFERENCES ON CORP.CURRENCY TO PSS;
GRANT SELECT ON PSS.TRAN_LINE_ITEM_TYPE TO CORP;
