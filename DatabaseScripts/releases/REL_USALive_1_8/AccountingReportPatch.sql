WHENEVER SQLERROR EXIT FAILURE COMMIT;

-- change SYSDATE -1 to SYSDATE - 16/24 because this procedure runs everyday at 6pm so we want 1st of each month run to take care of previous month's clean up. This is under REPORT schema
create or replace
procedure        REPORT.USAT_SETTLT_UPDATE as
--RFC000136 - Add Settlement Update Clean-up Script
CURSOR l_cur IS
SELECT x.TRAN_ID, x.SETTLE_STATE_ID, x.SETTLE_DATE, x.CC_APPR_CODE
FROM CORP.LEDGER L
JOIN REPORT.TRANS X ON L.TRANS_ID = X.TRAN_ID
WHERE L.ENTRY_TYPE = 'CC'
AND L.DELETED = 'N'
AND L.PAID_DATE IS NULL
AND L.SETTLE_STATE_ID IN (1,4)
AND X.SETTLE_STATE_ID NOT IN(1,4)
AND X.SERVER_DATE < SYSDATE - 16/24;
BEGIN
FOR l_rec IN l_cur LOOP
REPORT.SYNC_PKG.RECEIVE_TRANS_SETTLEMENT(l_rec.TRAN_ID, l_rec.SETTLE_STATE_ID, l_rec.SETTLE_DATE, l_rec.CC_APPR_CODE);
COMMIT;
END LOOP;
END;
/
-- make monthly accounting report to run 2nd of each month so the USAT_SETTLT_UPDATE will finish the cleanup
update report.user_report set latency=1 where report_id in 
(select report_id from report.reports where report_name in ('EFT Summary Accounting', 'Orphan Device History','All Other Fee Entries', 'All Other Fee Entries With Device','Transaction Processing Entry Summary','Unpaid Monies'));

commit;

-- fix Transaction Processing Entry Summary
update report.report_param set param_value='SELECT 
		led.entry_type,
		NVL(dt.business_unit_name, ''~ Terminal Orphan'') business_unit_name,
		NVL(dt.monthly_payment, ''UNK'') monthly_payment,
		cr.currency_code,
		NVL(dt.customer_name, ''~ Terminal Orphan'') customer_name,
		TO_CHAR(TRUNC(DECODE (led.entry_type,
		    ''CC'', tr.settle_date,
		    led.ledger_date
		), ''MONTH''), ''mm/dd/yyyy'') month_for,
		COUNT(1) tran_count,
		SUM(tr.total_amount) total_tran_amount,
		SUM(led.amount) total_ledger_amount
		FROM report.trans tr
		JOIN corp.currency cr
		ON cr.currency_id = tr.currency_id
		JOIN report.trans_state ts
		ON ts.state_id = tr.settle_state_id 
		LEFT OUTER JOIN (
			SELECT
			t.terminal_id,
			c.customer_name,
			bu.business_unit_name,
			DECODE(t.payment_schedule_id, 4, ''Y'', ''N'') monthly_payment
			FROM report.terminal t
			JOIN corp.customer c
			ON c.customer_id = t.customer_id
			JOIN corp.business_unit bu
			ON bu.business_unit_id = t.business_unit_id
		) dt
		ON dt.terminal_id = tr.terminal_id
		JOIN corp.ledger led
		ON led.trans_id = tr.tran_id
		AND led.deleted = ''N''
		WHERE tr.trans_type_id IN (16, 19, 20, 21) 
		AND tr.settle_state_id IN (2, 3) 
		AND led.entry_type IN (''CC'', ''PF'', ''CB'', ''RF'') 
		AND NVL(tr.settle_date, led.create_date) >= CAST(? AS DATE) AND NVL(tr.settle_date, led.create_date) < CAST(? AS DATE) + 1
		GROUP BY
		led.entry_type,
		NVL(dt.business_unit_name, ''~ Terminal Orphan''),
		NVL(dt.monthly_payment, ''UNK''),
		cr.currency_code,
		NVL(dt.customer_name, ''~ Terminal Orphan''),
		TRUNC(DECODE (led.entry_type,
		    ''CC'', tr.settle_date,
		    led.ledger_date
		), ''MONTH'')' where report_id=(select report_id from report.reports where report_name='Transaction Processing Entry Summary') and PARAM_NAME='query';
    
update report.report_param set param_value='StartDate,EndDate' where report_id=(select report_id from report.reports where report_name='Transaction Processing Entry Summary') and PARAM_NAME='paramNames';
update report.report_param set param_value='TIMESTAMP,TIMESTAMP' where report_id=(select report_id from report.reports where report_name='Transaction Processing Entry Summary') and PARAM_NAME='paramTypes';

-- Fix Unpaid Monies
update report.report_param set param_value='SELECT
		l.entry_type,
		bu.business_unit_name,
		DECODE(bat.payment_schedule_id, 4, ''Y'', ''N'') monthly_payment,
		cu.currency_code,
		c.customer_name,
		TO_CHAR(TRUNC(DECODE (l.entry_type,
		    ''CC'', t.settle_date,
		    l.entry_date
		), ''MONTH''),''mm/dd/yyyy'') month_for,
		COUNT(1) tran_count,
		SUM (l.amount) total_ledger_amount
		FROM corp.ledger l
		JOIN corp.batch bat
		ON bat.batch_id = l.batch_id
		JOIN corp.doc d
		ON d.doc_id = bat.doc_id
		JOIN corp.customer_bank cb
		ON cb.customer_bank_id = d.customer_bank_id
		JOIN corp.customer c
		ON c.customer_id = cb.customer_id
		JOIN corp.business_unit bu
		ON bu.business_unit_id = d.business_unit_id
		JOIN corp.currency cu
		ON cu.currency_id = d.currency_id
		JOIN report.trans_state ts
		ON ts.state_id = l.settle_state_id 
		LEFT OUTER JOIN report.trans t
		ON t.tran_id = l.trans_id
		WHERE l.deleted = ''N''
		AND d.status != ''D''
		AND l.settle_state_id IN (2, 3)
		AND (d.sent_date IS NULL OR d.sent_date >= TRUNC(CAST(? AS DATE) + 1, ''MONTH''))
		AND (l.partition_ts = DBADMIN.EPOCH_DATE OR l.partition_ts >= TRUNC(CAST(? AS DATE) + 1, ''MONTH''))
		AND NVL(t.settle_date, l.create_date) < TRUNC(CAST(? AS DATE) + 1, ''MONTH'')
		GROUP BY 
		l.entry_type,
		bu.business_unit_name,
		DECODE(bat.payment_schedule_id, 4, ''Y'', ''N''),
		cu.currency_code,
		c.customer_name,
		TRUNC(DECODE (l.entry_type,
		    ''CC'', t.settle_date,
		    l.entry_date
		), ''MONTH'')' where report_id=(select report_id from report.reports where report_name='Unpaid Monies') and PARAM_NAME='query';
    
update report.report_param set param_value='EndDate,EndDate,EndDate' where report_id=(select report_id from report.reports where report_name='Unpaid Monies') and PARAM_NAME='paramNames';
update report.report_param set param_value='TIMESTAMP,TIMESTAMP,TIMESTAMP' where report_id=(select report_id from report.reports where report_name='Unpaid Monies') and PARAM_NAME='paramTypes';

commit;



