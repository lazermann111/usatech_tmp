delete from report.report_param where report_id=(select report_id from report.reports where report_name='EFT Summary 2 Weeks');
delete from report.user_report where report_id=(select report_id from report.reports where report_name='EFT Summary 2 Weeks');
delete from report.reports where report_name='EFT Summary 2 Weeks';
    

-- eft_entry_summary_previous_month.sql
-- eft_entry_summary_current_month.sql
delete from web_content.web_link_requirement where web_link_id=(select web_link_id from web_content.web_link where web_link_label='EFT Summary Accounting');
delete from web_content.web_link where web_link_label='EFT Summary Accounting';
    
delete from report.report_param where report_id=(select report_id from report.reports where report_name='EFT Summary Accounting' and title='EFT Summary Accounting - {b} to {e}');
delete from report.user_report where report_id=(select report_id from report.reports where report_name='EFT Summary Accounting' and title='EFT Summary Accounting - {b} to {e}');
delete from report.reports where report_name='EFT Summary Accounting' and title='EFT Summary Accounting - {b} to {e}';

-- add orphan device history
delete from web_content.web_link_requirement where web_link_id=(select web_link_id from web_content.web_link where web_link_label='Orphan Device History');
delete from web_content.web_link where web_link_label='Orphan Device History';
    
delete from report.report_param where report_id=(select report_id from report.reports where report_name='Orphan Device History');
delete from report.user_report where report_id=(select report_id from report.reports where report_name='Orphan Device History');
delete from report.reports where report_name='Orphan Device History';

-- all_other_entries.sql

delete from web_content.web_link_requirement where web_link_id=(select web_link_id from web_content.web_link where web_link_label='All Other Fee Entries');
delete from web_content.web_link where web_link_label='All Other Fee Entries';
    
delete from report.report_param where report_id=(select report_id from report.reports where report_name='All Other Fee Entries');
delete from report.user_report where report_id=(select report_id from report.reports where report_name='All Other Fee Entries');
delete from report.reports where report_name='All Other Fee Entries';
    

-- all_other_entries_device.sql

delete from web_content.web_link_requirement where web_link_id=(select web_link_id from web_content.web_link where web_link_label='All Other Fee Entries With Device');
delete from web_content.web_link where web_link_label='All Other Fee Entries With Device';
    
delete from report.report_param where report_id=(select report_id from report.reports where report_name='All Other Fee Entries With Device');
delete from report.user_report where report_id=(select report_id from report.reports where report_name='All Other Fee Entries With Device');
delete from report.reports where report_name='All Other Fee Entries With Device';
    

-- tran_proc_rev_no_led_orpahns.sql
delete from web_content.web_link_requirement where web_link_id=(select web_link_id from web_content.web_link where web_link_label='Transaction Processing Entry Summary');
delete from web_content.web_link where web_link_label='Transaction Processing Entry Summary';
    
delete from report.report_param where report_id=(select report_id from report.reports where report_name='Transaction Processing Entry Summary');
delete from report.user_report where report_id=(select report_id from report.reports where report_name='Transaction Processing Entry Summary');
delete from report.reports where report_name='Transaction Processing Entry Summary';
    

-- unpaid_monies
delete from web_content.web_link_requirement where web_link_id=(select web_link_id from web_content.web_link where web_link_label='Unpaid Monies');
delete from web_content.web_link where web_link_label='Unpaid Monies';
    
delete from report.report_param where report_id=(select report_id from report.reports where report_name='Unpaid Monies');
delete from report.user_report where report_id=(select report_id from report.reports where report_name='Unpaid Monies');
delete from report.reports where report_name='Unpaid Monies';
    
COMMIT;