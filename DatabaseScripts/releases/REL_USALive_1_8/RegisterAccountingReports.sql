DECLARE
	l_env VARCHAR2(3); 
  	l_ccs_transport_id NUMBER;
  	l_user_report_id NUMBER;
	l_user_id NUMBER;
	l_host_id NUMBER;
	l_port_id NUMBER;
	l_username_id NUMBER;
	l_password_id NUMBER;
	l_date_formatted_folder_id NUMBER;
	l_date_formatted_folder VARCHAR(100):='''EOM ''MMM-yyyy';
	l_host VARCHAR2(100);
	l_port VARCHAR2(100);
	l_username VARCHAR2(100);
	l_password VARCHAR2(100);
BEGIN

select DECODE(substr(subdomain_url, length('hotchoice') + 1, length(subdomain_url) - length('hotchoice.usatech.com')), '-ecc', 'ECC', '-dev', 'DEV', '-int', 'INT',null, 'PRD')
  into l_env
  from WEB_CONTENT.subdomain where subdomain_url like 'hotchoice%.usatech.com'
  and subdomain_url not IN('hotchoice-PRD.usatech.com','hotchoice-USA.usatech.com') ;
  
  IF l_env = 'INT' THEN
  	select user_id into l_user_id from report.user_login where user_name='yhe@usatech.com';
  	l_host:='usaxfr01.usatech.com';
  	l_port:='21';
  	l_username:='ftpuser';
  	l_password:='Tr@nsfer';
  ELSIF l_env = 'ECC' THEN
  	select user_id into l_user_id from report.user_login where user_name='aroyce@usatech.com';
  	l_host:='usaxfr01.usatech.com';
  	l_port:='21';
  	l_username:='ftpuser';
  	l_password:='Tr@nsfer';
  ELSIF l_env = 'PRD' THEN
  	-- @Todo replace with prod user
  	select user_id into l_user_id from report.user_login where user_name='eforsythe@usatech.com'; 
  	l_host:='usaxfr01.usatech.com';
  	l_port:='21';
  	l_username:='ftpuser';
  	l_password:='Tr@nsfer';
  ELSE
  	select user_id into l_user_id from report.user_login where user_name='yhe@usatech.com';
  	l_host:='usaxfr01.usatech.com';
  	l_port:='21';
  	l_username:='ftpuser';
  	l_password:='Tr@nsfer';
  END IF;
  
  select ccs_transport_property_type_id into l_host_id from report.ccs_transport_property_type where ccs_transport_type_id=5 and upper(ccs_tpt_name)='HOST';
  select ccs_transport_property_type_id into l_port_id from report.ccs_transport_property_type where ccs_transport_type_id=5 and upper(ccs_tpt_name)='PORT'; 
  select ccs_transport_property_type_id into l_username_id from report.ccs_transport_property_type where ccs_transport_type_id=5 and upper(ccs_tpt_name)='USERNAME'; 
  select ccs_transport_property_type_id into l_password_id from report.ccs_transport_property_type where ccs_transport_type_id=5 and upper(ccs_tpt_name)='PASSWORD'; 
  select ccs_transport_property_type_id into l_date_formatted_folder_id from report.ccs_transport_property_type where ccs_transport_type_id=5 and upper(ccs_tpt_name)='DATE FORMATTED FOLDER'; 

    select report.ccs_transport_seq.nextval into l_ccs_transport_id from dual;
        insert into report.ccs_transport (ccs_transport_id, ccs_transport_type_id, ccs_transport_name, user_id)
    values(l_ccs_transport_id, 5, 'ftpsForAccounting', l_user_id);
        insert into report.ccs_transport_property( ccs_transport_property_id, ccs_transport_id, ccs_transport_property_type_id,ccs_transport_property_value)
values( report.ccs_transport_property_seq.nextval, l_ccs_transport_id, l_host_id, l_host);
    insert into report.ccs_transport_property( ccs_transport_property_id, ccs_transport_id, ccs_transport_property_type_id,ccs_transport_property_value)
values( report.ccs_transport_property_seq.nextval, l_ccs_transport_id, l_port_id, l_port);
    insert into report.ccs_transport_property( ccs_transport_property_id, ccs_transport_id, ccs_transport_property_type_id,ccs_transport_property_value)
values( report.ccs_transport_property_seq.nextval, l_ccs_transport_id, l_username_id, l_username);
    insert into report.ccs_transport_property( ccs_transport_property_id, ccs_transport_id, ccs_transport_property_type_id,ccs_transport_property_value)
values( report.ccs_transport_property_seq.nextval, l_ccs_transport_id, l_password_id, l_password);
	insert into report.ccs_transport_property( ccs_transport_property_id, ccs_transport_id, ccs_transport_property_type_id,ccs_transport_property_value)
values( report.ccs_transport_property_seq.nextval, l_ccs_transport_id, l_date_formatted_folder_id, l_date_formatted_folder);

	select report.user_report_seq.nextval into l_user_report_id from dual;
    insert into report.user_report (user_report_id, report_id, user_id, status, ccs_transport_id, frequency_id, latency)
    select l_user_report_id, report_id, l_user_id, 'A', l_ccs_transport_id, 1, 4.58 from report.reports where report_name='EFT Summary 2 Weeks';
    update report.user_report set upd_dt = to_date('10-11-2012 00:00:00','MM-DD-YYYY HH24:MI:SS') where user_report_id=l_user_report_id; 

    select report.user_report_seq.nextval into l_user_report_id from dual;
    insert into report.user_report (user_report_id, report_id, user_id, status, ccs_transport_id, frequency_id, latency)
    select l_user_report_id, report_id, l_user_id, 'A', l_ccs_transport_id, 2, 0 from report.reports where report_name='EFT Summary Accounting';
    update report.user_report set upd_dt = to_date('09-30-2012 00:00:00','MM-DD-YYYY HH24:MI:SS') where user_report_id=l_user_report_id; 

    select report.user_report_seq.nextval into l_user_report_id from dual;
    insert into report.user_report (user_report_id, report_id, user_id, status, ccs_transport_id, frequency_id, latency)
    select l_user_report_id, report_id, l_user_id, 'A', l_ccs_transport_id, 2, 0 from report.reports where report_name='Orphan Device History';
    update report.user_report set upd_dt = to_date('09-30-2012 00:00:00','MM-DD-YYYY HH24:MI:SS') where user_report_id=l_user_report_id; 

    select report.user_report_seq.nextval into l_user_report_id from dual;
    insert into report.user_report (user_report_id, report_id, user_id, status, ccs_transport_id, frequency_id, latency)
    select l_user_report_id, report_id, l_user_id, 'A', l_ccs_transport_id, 2, 0 from report.reports where report_name='All Other Fee Entries';
    update report.user_report set upd_dt = to_date('09-30-2012 00:00:00','MM-DD-YYYY HH24:MI:SS') where user_report_id=l_user_report_id; 

    select report.user_report_seq.nextval into l_user_report_id from dual;
    insert into report.user_report (user_report_id, report_id, user_id, status, ccs_transport_id, frequency_id, latency)
    select l_user_report_id, report_id, l_user_id, 'A', l_ccs_transport_id, 2, 0 from report.reports where report_name='All Other Fee Entries With Device';
    update report.user_report set upd_dt = to_date('09-30-2012 00:00:00','MM-DD-YYYY HH24:MI:SS') where user_report_id=l_user_report_id; 
    
    select report.user_report_seq.nextval into l_user_report_id from dual;
    insert into report.user_report (user_report_id, report_id, user_id, status, ccs_transport_id, frequency_id, latency)
    select l_user_report_id, report_id, l_user_id, 'A', l_ccs_transport_id, 2, 0 from report.reports where report_name='Transaction Processing Entries Summary';
    update report.user_report set upd_dt = to_date('09-30-2012 00:00:00','MM-DD-YYYY HH24:MI:SS') where user_report_id=l_user_report_id; 
    
    select report.user_report_seq.nextval into l_user_report_id from dual;
    insert into report.user_report (user_report_id, report_id, user_id, status, ccs_transport_id, frequency_id, latency)
    select l_user_report_id, report_id, l_user_id, 'A', l_ccs_transport_id, 2, 0 from report.reports where report_name='Unpaid Monies';
    update report.user_report set upd_dt = to_date('09-30-2012 00:00:00','MM-DD-YYYY HH24:MI:SS') where user_report_id=l_user_report_id; 
    commit;
END;
/