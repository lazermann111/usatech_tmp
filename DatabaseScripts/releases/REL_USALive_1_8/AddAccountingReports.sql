SET DEFINE OFF;
DECLARE
	l_env VARCHAR2(3); 
	l_user_id NUMBER;
BEGIN

select NVL(MAX(DECODE(substr(subdomain_url, length('hotchoice') + 1, length(subdomain_url) - length('hotchoice.usatech.com')), '-ecc', 'ECC', '-dev', 'DEV', '-int', 'INT', null, 'PRD')), 'DEV')
  into l_env
  from WEB_CONTENT.subdomain where subdomain_url like 'hotchoice%.usatech.com'
  and subdomain_url not IN('hotchoice-PRD.usatech.com','hotchoice-USA.usatech.com') ;
  
  IF l_env = 'INT' THEN
  	select user_id into l_user_id from report.user_login where user_name='yhe@usatech.com';
  ELSIF l_env = 'ECC' THEN
  	select user_id into l_user_id from report.user_login where user_name='aroyce@usatech.com';
  ELSIF l_env = 'PRD' THEN
  	select user_id into l_user_id from report.user_login where user_name='eforsythe@usatech.com';
  ELSE
  	select user_id into l_user_id from report.user_login where user_name='yhe@usatech.com';
  END IF;
  
INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
VALUES( REPORT.REPORTS_SEQ.NEXTVAL, 'EFT Summary 2 Weeks - {e}', 1, 0, 'EFT Summary 2 Weeks', 'EFT summary report for the past 2 weeks.', 'N', l_user_id);
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( REPORT.REPORTS_SEQ.CURRVAL, 'query', 'SELECT l.entry_type, fn.fee_name service_fee_name, bu.business_unit_name, DECODE(bat.payment_schedule_id, 4, ''Y'', ''N'') monthly_payment,cu.currency_code,c.customer_name,cb.bank_acct_nbr,cb.bank_routing_nbr,cb.account_title,d.doc_id eft_id,d.sent_date eft_date,SUM(l.amount) ledger_amount,d.total_amount eft_total_amount
		FROM corp.doc d JOIN corp.customer_bank cb ON cb.customer_bank_id = d.customer_bank_id JOIN corp.customer c ON c.customer_id = cb.customer_id JOIN corp.business_unit bu ON bu.business_unit_id = d.business_unit_id JOIN corp.currency cu ON cu.currency_id = d.currency_id JOIN corp.batch bat ON bat.doc_id = d.doc_id JOIN corp.ledger l ON l.batch_id = bat.batch_id
		LEFT OUTER JOIN (
		    SELECT 
		    sf.service_fee_id,
		    f.fee_name
		    FROM corp.service_fees sf
		    JOIN corp.fees f
		    ON f.fee_id = sf.fee_id
		) fn
		ON fn.service_fee_id = l.service_fee_id
		WHERE sent_date > sysdate -14
		AND l.deleted = ''N''
		AND l.settle_state_id IN (2, 3)
		GROUP BY
		l.entry_type,
		fn.fee_name,
		bu.business_unit_name,
		DECODE(bat.payment_schedule_id, 4, ''Y'', ''N''),
		cu.currency_code,
		c.customer_name,
		cb.bank_acct_nbr,
		cb.bank_routing_nbr,
		cb.account_title,
		d.doc_id,
		d.sent_date,
		d.total_amount');
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( REPORT.REPORTS_SEQ.CURRVAL, 'header', 'true');
    

-- eft_entry_summary_previous_month.sql monthly run
-- eft_entry_summary_current_month.sql manually run after BEX 
INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
VALUES( REPORT.REPORTS_SEQ.NEXTVAL, 'EFT Summary Accounting - {b} to {e}', 1, 0, 'EFT Summary Accounting', 'EFT summary accounting report for the specified date range.', 'N', l_user_id);
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( REPORT.REPORTS_SEQ.CURRVAL, 'query', 'SELECT
		l.entry_type,
		fn.fee_name service_fee_name,
		bu.business_unit_name,
		DECODE(bat.payment_schedule_id, 4, ''Y'', ''N'') monthly_payment,
		cu.currency_code,
		c.customer_name,
		cb.bank_acct_nbr,
		cb.bank_routing_nbr,
		cb.account_title,
		d.doc_id eft_id,
		d.sent_date eft_date,
		SUM(l.amount) ledger_amount,
		d.total_amount eft_total_amount
		FROM corp.doc d
		JOIN corp.customer_bank cb
		ON cb.customer_bank_id = d.customer_bank_id
		JOIN corp.customer c
		ON c.customer_id = cb.customer_id
		JOIN corp.business_unit bu
		ON bu.business_unit_id = d.business_unit_id
		JOIN corp.currency cu
		ON cu.currency_id = d.currency_id
		JOIN corp.batch bat
		ON bat.doc_id = d.doc_id
		JOIN corp.ledger l
		ON l.batch_id = bat.batch_id
		LEFT OUTER JOIN (
			SELECT 
			sf.service_fee_id,
			f.fee_name
			FROM corp.service_fees sf
			JOIN corp.fees f
			ON f.fee_id = sf.fee_id
		) fn
		ON fn.service_fee_id = l.service_fee_id
		WHERE d.sent_date >= CAST(? AS DATE) AND d.sent_date < CAST(? AS DATE) + 1
		AND l.deleted = ''N''
		AND l.settle_state_id IN (2, 3)
		GROUP BY
		l.entry_type,
		fn.fee_name,
		bu.business_unit_name,
		DECODE(bat.payment_schedule_id, 4, ''Y'', ''N''),
		cu.currency_code,
		c.customer_name,
		cb.bank_acct_nbr,
		cb.bank_routing_nbr,
		cb.account_title,
		d.doc_id,
		d.sent_date,
		d.total_amount');
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( REPORT.REPORTS_SEQ.CURRVAL, 'header', 'true');
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'dateFormat','MM/dd/yyyy');
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'params.StartDate','{b}');
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'params.EndDate','{e}');
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'paramNames','StartDate,EndDate');
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'paramTypes','TIMESTAMP,TIMESTAMP');

    insert into web_content.web_link values(web_content.seq_web_link_id.nextval, 'EFT Summary Accounting', './select_date_range_frame_nonfolio.i?basicReportId='||REPORT.REPORTS_SEQ.CURRVAL,'EFT summary accounting report.','Accounting','W', 0);
    insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 1);
    insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 14);


-- add orphan_device_history.sql
INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
VALUES( REPORT.REPORTS_SEQ.NEXTVAL, 'Orphan Device History - {e}', 1, 0, 'Orphan Device History', 'Orphan device history report.', 'N', l_user_id);
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( REPORT.REPORTS_SEQ.CURRVAL, 'query', 'SELECT
		e.eport_serial_num,
		dt.device_type_desc,
		tt.trans_type_name,
		ts.state_label settle_state,
		NVL2(tr.terminal_id, ''TB'', ''T'') orphan_type,
		NVL(ssy.source_system_name, ''~ Legacy'') source_system_name,
		NVL(cr.currency_code, ''USD'') currency_code,
		TO_CHAR(TRUNC(tr.settle_date, ''MONTH''), ''mm/dd/yyyy'') month_for,
		COUNT(1) tran_count,
		SUM(tr.total_amount) total_amount
		FROM report.trans tr
		JOIN report.eport e
		ON e.eport_id = tr.eport_id
		JOIN report.device_type dt
		ON dt.device_type_id = e.device_type_id
		JOIN report.trans_type tt
		ON tr.trans_type_id = tt.trans_type_id
		JOIN report.trans_state ts
		ON ts.state_id = tr.settle_state_id -- yes, this is non intuituve but correct
		LEFT OUTER JOIN corp.currency cr
		ON cr.currency_id = tr.currency_id
		LEFT OUTER JOIN report.source_system ssy
		ON ssy.source_system_cd = tr.source_system_cd
		WHERE tr.settle_date < TRUNC(CAST(? AS DATE) + 1, ''MONTH'')
		AND tr.trans_type_id IN (16, 19, 20)
		AND tr.settle_state_id IN (2, 3) 
		AND DECODE(tr.customer_bank_id, NULL, 0) = 0
		GROUP BY
		e.eport_serial_num,
		dt.device_type_desc,
		tt.trans_type_name,
		ts.state_label,
		NVL2(tr.terminal_id, ''TB'', ''T''),
		NVL(ssy.source_system_name, ''~ Legacy''),
		NVL(cr.currency_code, ''USD''),
		TRUNC(tr.settle_date, ''MONTH'')
		ORDER BY 8, 2, 3, 4, 5, 6, 7, 1');
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( REPORT.REPORTS_SEQ.CURRVAL, 'header', 'true');
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'dateFormat','MM/dd/yyyy'); 
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'paramNames','EndDate');
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'paramTypes','TIMESTAMP');
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'params.EndDate','{e}');
    
insert into web_content.web_link values(web_content.seq_web_link_id.nextval, 'Orphan Device History', './selection_month.i?basicReportId='||REPORT.REPORTS_SEQ.CURRVAL,'Orphan device history report.','Accounting','W', 0);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 1);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 14);



-- all_other_entries.sql
INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
VALUES( REPORT.REPORTS_SEQ.NEXTVAL, 'All Other Fee Entries - {b} to {e}', 1, 0, 'All Other Fee Entries', 'All other fee entries report.', 'N', l_user_id);
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( REPORT.REPORTS_SEQ.CURRVAL, 'query', 'SELECT
		lts.entry_type,
		bu.business_unit_name,
		DECODE(bat.payment_schedule_id, 4, ''Y'', ''N'') monthly_payment,
		cu.currency_code,
		c.customer_name,
		lts.fee_name,
		DECODE(d.status,
			''S'', ''Y'',
			''P'', ''Y'',
			''N''
		) in_eft,
		TO_CHAR(TRUNC(lts.entry_date, ''MONTH''),''mm/dd/yyyy'') month_for,
		COUNT(1) entry_count,
		SUM (lts.amount) total_entry_amount,
		lts.description
		FROM (
		    SELECT
		    led.batch_id,
		    led.amount,
		    led.entry_type,
		    led.settle_state_id,
		    led.create_date,
		    led.entry_date,
		    led.description,
		    sfi.fee_name
		    FROM corp.ledger led
		    LEFT OUTER JOIN report.trans tr
		    ON tr.tran_id = led.trans_id
		    LEFT OUTER JOIN (
		        SELECT 
		        sf.service_fee_id,
		        f.fee_name
		        FROM corp.service_fees sf
		        JOIN corp.fees f
		        ON f.fee_id = sf.fee_id
		    ) sfi
		    ON sfi.service_fee_id = led.service_fee_id
		    WHERE led.entry_type IN (''SF'', ''AD'', ''SB'')
		    AND led.deleted = ''N''
		    AND led.settle_state_id IN (2, 3)
		    AND led.create_date >= CAST(? AS DATE) AND led.create_date < CAST(? AS DATE) + 1
		)lts
		JOIN corp.batch bat
		ON bat.batch_id = lts.batch_id
		JOIN corp.doc d
		ON d.doc_id = bat.doc_id
		JOIN corp.customer_bank cb
		ON cb.customer_bank_id = d.customer_bank_id
		JOIN corp.customer c
		ON c.customer_id = cb.customer_id
		JOIN corp.business_unit bu
		ON bu.business_unit_id = d.business_unit_id
		JOIN corp.currency cu
		ON cu.currency_id = d.currency_id
		JOIN report.trans_state ts
		ON ts.state_id = lts.settle_state_id -- yes, this is non intuitive but correct
		GROUP BY
		lts.entry_type,
		bu.business_unit_name,
		DECODE(bat.payment_schedule_id, 4, ''Y'', ''N''),
		cu.currency_code,
		c.customer_name,
		lts.fee_name,
		DECODE(d.status,
			''S'', ''Y'',
			''P'', ''Y'',
			''N''
		),
		TRUNC(lts.entry_date, ''MONTH''),
		lts.description');
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( REPORT.REPORTS_SEQ.CURRVAL, 'header', 'true');
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'dateFormat','MM/dd/yyyy');
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'params.StartDate','{b}');
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'params.EndDate','{e}');
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'paramNames','StartDate,EndDate');
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'paramTypes','TIMESTAMP,TIMESTAMP');
    
insert into web_content.web_link values(web_content.seq_web_link_id.nextval, 'All Other Fee Entries', './select_date_range_frame_nonfolio.i?basicReportId='||REPORT.REPORTS_SEQ.CURRVAL,'All other fee entries report.','Accounting','W', 0);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 1);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 14);



-- all_other_entries_device.sql
INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
VALUES( REPORT.REPORTS_SEQ.NEXTVAL, 'All Other Fee Entries With Device - {b} to {e}', 1, 0, 'All Other Fee Entries With Device', 'All other fee entries with eport info report.', 'N', l_user_id);
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( REPORT.REPORTS_SEQ.CURRVAL, 'query', 'SELECT
		lts.entry_type,
		bu.business_unit_name,
		DECODE(bat.payment_schedule_id, 4, ''Y'', ''N'') monthly_payment,
		cu.currency_code,
		c.customer_name,
		ee.eport_num,
		lts.fee_name,
		DECODE(d.status,
			''S'', ''Y'',
			''P'', ''Y'',
			''N''
		) in_eft,
		TO_CHAR(TRUNC(lts.entry_date, ''MONTH''),''mm/dd/yyyy'') month_for,
		COUNT(1) entry_count,
		SUM (lts.amount) total_entry_amount,
		lts.description,
		t.terminal_nbr
		FROM (
		    SELECT
		    led.batch_id,
		    led.amount,
		    led.entry_type,
		    led.settle_state_id,
		    led.create_date,
		    led.entry_date,
		    led.description,
		    sfi.fee_name
		    FROM corp.ledger led
		    LEFT OUTER JOIN report.trans tr
		    ON tr.tran_id = led.trans_id
		    LEFT OUTER JOIN (
		        SELECT 
		        sf.service_fee_id,
		        f.fee_name
		        FROM corp.service_fees sf
		        JOIN corp.fees f
		        ON f.fee_id = sf.fee_id
		    ) sfi
		    ON sfi.service_fee_id = led.service_fee_id
		    WHERE led.entry_type IN (''SF'', ''AD'', ''SB'') 
		    AND led.deleted = ''N''
		    AND led.settle_state_id IN (2, 3)
		 AND led.create_date >= CAST(? AS DATE) AND led.create_date < CAST(? AS DATE) + 1
		)lts
		JOIN corp.batch bat
		ON bat.batch_id = lts.batch_id
		JOIN corp.doc d
		ON d.doc_id = bat.doc_id
		JOIN corp.customer_bank cb
		ON cb.customer_bank_id = d.customer_bank_id
		JOIN corp.customer c
		ON c.customer_id = cb.customer_id
		JOIN corp.business_unit bu
		ON bu.business_unit_id = d.business_unit_id
		JOIN corp.currency cu
		ON cu.currency_id = d.currency_id
		JOIN report.trans_state ts
		ON ts.state_id = lts.settle_state_id 
		LEFT outer JOIN (SELECT DISTINCT TERMINAL_ID, FIRST_VALUE(eport_serial_num) OVER (PARTITION BY TERMINAL_ID ORDER BY start_date DESC) Eport_num
		from report.terminal_eport te, report.eport e where te.eport_id=e.eport_id) ee
		ON bat.terminal_id=ee.terminal_id
		LEFT OUTER JOIN report.terminal t ON bat.terminal_id = t.terminal_id
		GROUP BY
		lts.entry_type,
		bu.business_unit_name,
		DECODE(bat.payment_schedule_id, 4, ''Y'', ''N''),
		cu.currency_code,
		c.customer_name,
		lts.fee_name,
		DECODE(d.status,
			''S'', ''Y'',
			''P'', ''Y'',
			''N''
		),
		TRUNC(lts.entry_date, ''MONTH''),
		lts.description
		,ee.Eport_num,
		t.terminal_nbr');
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( REPORT.REPORTS_SEQ.CURRVAL, 'header', 'true');
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'dateFormat','MM/dd/yyyy');
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'params.StartDate','{b}');
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'params.EndDate','{e}');
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'paramNames','StartDate,EndDate');
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'paramTypes','TIMESTAMP,TIMESTAMP');
    
insert into web_content.web_link values(web_content.seq_web_link_id.nextval, 'All Other Fee Entries With Device', './select_date_range_frame_nonfolio.i?basicReportId='||REPORT.REPORTS_SEQ.CURRVAL,'All other fee entries with eport info report.','Accounting','W', 0);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 1);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 14);



-- tran_proc_rev_no_led_orpahns.sql
INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
VALUES( REPORT.REPORTS_SEQ.NEXTVAL, 'Transaction Processing Entry Summary - {b} to {e}', 1, 0, 'Transaction Processing Entry Summary', 'Transaction processing entry summary by customer and currency code report.', 'N', l_user_id);
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( REPORT.REPORTS_SEQ.CURRVAL, 'query', 'SELECT 
		led.entry_type,
		NVL(dt.business_unit_name, ''~ Terminal Orphan'') business_unit_name,
		NVL(dt.monthly_payment, ''UNK'') monthly_payment,
		cr.currency_code,
		NVL(dt.customer_name, ''~ Terminal Orphan'') customer_name,
		TO_CHAR(TRUNC(DECODE (led.entry_type,
		    ''CC'', tr.settle_date,
		    led.ledger_date
		), ''MONTH''), ''mm/dd/yyyy'') month_for,
		COUNT(1) tran_count,
		SUM(tr.total_amount) total_tran_amount,
		SUM(led.amount) total_ledger_amount
		FROM report.trans tr
		JOIN corp.currency cr
		ON cr.currency_id = tr.currency_id
		JOIN report.trans_state ts
		ON ts.state_id = tr.settle_state_id 
		LEFT OUTER JOIN (
			SELECT
			t.terminal_id,
			c.customer_name,
			bu.business_unit_name,
			DECODE(t.payment_schedule_id, 4, ''Y'', ''N'') monthly_payment
			FROM report.terminal t
			JOIN corp.customer c
			ON c.customer_id = t.customer_id
			JOIN corp.business_unit bu
			ON bu.business_unit_id = t.business_unit_id
		) dt
		ON dt.terminal_id = tr.terminal_id
		JOIN corp.ledger led
		ON led.trans_id = tr.tran_id
		AND led.deleted = ''N''
		WHERE tr.trans_type_id IN (16, 19, 20, 21) 
		AND tr.settle_state_id IN (2, 3) 
		AND led.entry_type IN (''CC'', ''PF'', ''CB'', ''RF'') 
		AND (led.entry_type = ''CC'' AND tr.settle_date >= CAST(? AS DATE) AND tr.settle_date < CAST(? AS DATE) + 1
		    OR led.entry_type != ''CC'' AND led.create_date >= CAST(? AS DATE) AND led.create_date < CAST(? AS DATE) + 1)
		GROUP BY
		led.entry_type,
		NVL(dt.business_unit_name, ''~ Terminal Orphan''),
		NVL(dt.monthly_payment, ''UNK''),
		cr.currency_code,
		NVL(dt.customer_name, ''~ Terminal Orphan''),
		TRUNC(DECODE (led.entry_type,
		    ''CC'', tr.settle_date,
		    led.ledger_date
		), ''MONTH'')');
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( REPORT.REPORTS_SEQ.CURRVAL, 'header', 'true');
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'dateFormat','MM/dd/yyyy');
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'params.StartDate','{b}');
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'params.EndDate','{e}');
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'paramNames','StartDate,EndDate,StartDate,EndDate');
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'paramTypes','TIMESTAMP,TIMESTAMP,TIMESTAMP,TIMESTAMP');
    
insert into web_content.web_link values(web_content.seq_web_link_id.nextval, 'Transaction Processing Entry Summary', './select_date_range_frame_nonfolio.i?basicReportId='||REPORT.REPORTS_SEQ.CURRVAL,'Transaction processing entry summary by customer and currency code report.','Accounting','W', 0);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 1);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 14);



-- unpaid_monies.sql
INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
VALUES( REPORT.REPORTS_SEQ.NEXTVAL, 'Unpaid Monies - {e}', 1, 0, 'Unpaid Monies', 'Unpaid monies report.', 'N', l_user_id);
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( REPORT.REPORTS_SEQ.CURRVAL, 'query', 'SELECT
		l.entry_type,
		bu.business_unit_name,
		DECODE(bat.payment_schedule_id, 4, ''Y'', ''N'') monthly_payment,
		cu.currency_code,
		c.customer_name,
		TO_CHAR(TRUNC(DECODE (l.entry_type,
		    ''CC'', t.settle_date,
		    l.entry_date
		), ''MONTH''),''mm/dd/yyyy'') month_for,
		COUNT(1) tran_count,
		SUM (l.amount) total_ledger_amount
		FROM corp.ledger l
		JOIN corp.batch bat
		ON bat.batch_id = l.batch_id
		JOIN corp.doc d
		ON d.doc_id = bat.doc_id
		JOIN corp.customer_bank cb
		ON cb.customer_bank_id = d.customer_bank_id
		JOIN corp.customer c
		ON c.customer_id = cb.customer_id
		JOIN corp.business_unit bu
		ON bu.business_unit_id = d.business_unit_id
		JOIN corp.currency cu
		ON cu.currency_id = d.currency_id
		JOIN report.trans_state ts
		ON ts.state_id = l.settle_state_id 
		LEFT OUTER JOIN report.trans t
		ON t.tran_id = l.trans_id
		WHERE l.deleted = ''N''
		AND d.status != ''D''
		AND l.settle_state_id IN (2, 3)
		AND (d.sent_date IS NULL OR d.sent_date >= TRUNC(CAST(? AS DATE) + 1, ''MONTH''))
		AND (l.partition_ts = DBADMIN.EPOCH_DATE OR l.partition_ts >= TRUNC(CAST(? AS DATE) + 1, ''MONTH''))
		AND (l.entry_type = ''CC'' AND t.settle_date < TRUNC(CAST(? AS DATE) + 1, ''MONTH'')
			OR l.entry_type != ''CC'' AND l.create_date < TRUNC(CAST(? AS DATE) + 1, ''MONTH''))
		GROUP BY 
		l.entry_type,
		bu.business_unit_name,
		DECODE(bat.payment_schedule_id, 4, ''Y'', ''N''),
		cu.currency_code,
		c.customer_name,
		TRUNC(DECODE (l.entry_type,
		    ''CC'', t.settle_date,
		    l.entry_date
		), ''MONTH'')');
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( REPORT.REPORTS_SEQ.CURRVAL, 'header', 'true');
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'dateFormat','MM/dd/yyyy'); 
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'paramNames','EndDate,EndDate,EndDate,EndDate');
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'paramTypes','TIMESTAMP,TIMESTAMP,TIMESTAMP,TIMESTAMP');
insert into report.report_param values(REPORT.REPORTS_SEQ.CURRVAL, 'params.EndDate','{e}');
    
insert into web_content.web_link values(web_content.seq_web_link_id.nextval, 'Unpaid Monies', './selection_month.i?basicReportId='||REPORT.REPORTS_SEQ.CURRVAL,'Unpaid monies report.','Accounting','W', 0);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 1);
insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 14);

COMMIT;

END;
/