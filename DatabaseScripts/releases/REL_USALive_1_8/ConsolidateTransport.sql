--set serveroutput on
DECLARE
  CURSOR l_user IS
   select distinct ur.user_id from report.user_report ur
    join report.user_login ul on ur.user_id=ul.user_id and ur.status='A' and ul.status='A';
  l_count NUMBER:=0;
  l_property_type_id NUMBER;

BEGIN
	
-- Disable Report Register restart usalive
-- update web_content.web_link set web_link_usage='D' where WEB_LINK_LABEL='Report Register'

-- Enable Report Register restart usalive
-- update web_content.web_link set web_link_usage='M' where WEB_LINK_LABEL='Report Register'


   update report.ccs_transport ct set user_id=
  (select distinct ur.user_id from report.user_report ur
  join report.user_login ul on ur.user_id=ul.user_id and ur.status='A' and ul.status='A'
  where ur.ccs_transport_id=ct.ccs_transport_id)
  where exists(select 1 from report.user_report ur
  join report.user_login ul on ur.user_id=ul.user_id and ur.status='A' and ul.status='A'
  where ur.ccs_transport_id=ct.ccs_transport_id);

   update report.ccs_transport set ccs_transport_status_cd='D' where user_id = 0;
   commit;
   
    FOR l_rec IN l_user LOOP
      --dbms_output.put_line('for userId:'||l_rec.user_id);
      	FOR l_ccs in (select b.min_tranport_id,  a.ccs_transport_id from (
          select ct.ccs_transport_id, cast(wm_concat(ct.CCS_TRANSPORT_type_id||ctp.CCS_TRANSPORT_PROPERTY_type_id||ctp.CCS_TRANSPORT_PROPERTY_value) as varchar2(2000)) as transport_value
          from REPORT.CCS_TRANSPORT_PROPERTY ctp join report.ccs_transport ct on ctp.ccs_transport_id=ct.ccs_transport_id 
          join REPORT.CCS_TRANSPORT_PROPERTY_type ctpt on ctp.CCS_TRANSPORT_PROPERTY_type_id=ctpt.CCS_TRANSPORT_PROPERTY_type_id and ct.CCS_TRANSPORT_type_id=ctpt.CCS_TRANSPORT_type_id
          where ct.user_id=l_rec.user_id and ct.ccs_transport_status_cd<>'D'
          group by ct.ccs_transport_id
          order by ccs_transport_id
          ) a
          join 
          (select min(ccs_transport_id) min_tranport_id, transport_value from 
          (select ct.ccs_transport_id, cast(wm_concat(ct.CCS_TRANSPORT_type_id||ctp.CCS_TRANSPORT_PROPERTY_type_id||ctp.CCS_TRANSPORT_PROPERTY_value) as varchar2(2000)) as transport_value
          from REPORT.CCS_TRANSPORT_PROPERTY ctp join report.ccs_transport ct on ctp.ccs_transport_id=ct.ccs_transport_id 
          join REPORT.CCS_TRANSPORT_PROPERTY_type ctpt on ctp.CCS_TRANSPORT_PROPERTY_type_id=ctpt.CCS_TRANSPORT_PROPERTY_type_id and ct.CCS_TRANSPORT_type_id=ctpt.CCS_TRANSPORT_type_id
          where ct.user_id=l_rec.user_id and ct.ccs_transport_status_cd<>'D'
          group by ct.ccs_transport_id
          order by ccs_transport_id) 
          group by transport_value) b
          on a.transport_value=b.transport_value
          where a.ccs_transport_id!=min_tranport_id)
        LOOP              
          --dbms_output.put_line('update:'||l_ccs.ccs_transport_id||' min:'||l_ccs.min_tranport_id);
          -- save the change to a tmp table for rollback
          insert into report.tmp_ccs_transport (user_report_id, ccs_transport_id, min_ccs_transport_id)
          select user_report_id, l_ccs.ccs_transport_id, l_ccs.min_tranport_id from report.user_report where ccs_transport_id=l_ccs.ccs_transport_id;
          
          update report.user_report set ccs_transport_id=l_ccs.min_tranport_id 
          where ccs_transport_id=l_ccs.ccs_transport_id;
          
          update report.ccs_transport set ccs_transport_status_cd='D' where ccs_transport_id=l_ccs.ccs_transport_id;
          commit;
        END LOOP;
      --dbms_output.put_line('---------');
    END LOOP;
    --dbms_output.put_line('updateCount:'||l_count);
    
       -- update ccs_transport_name 100 bytes max
   FOR l_ccs_name in (SELECT tp.ccs_transport_id, substr(decode(t.CCS_TRANSPORT_TYPE_ID, 1, 'mailto: ', 2, 'sftp://', 3, 'ftp://', 4, '', 5, 'ftps://',6, 'link mailto: ', 7, '')
    ||  MAX(CASE WHEN UPPER(tpt.CCS_TPT_NAME) IN('HOST', 'EMAIL', 'URL')-- tpt.CCS_TRANSPORT_PROPERTY_TYPE_ID IN(3, 8, 3863 /*HOST*/, 13, 37162 /*url*/, 1 /*email to: */) 
    THEN tp.CCS_TRANSPORT_PROPERTY_VALUE ELSE NULL END) ||
    MAX(CASE WHEN UPPER(tpt.CCS_TPT_NAME) IN('PORT') --tpt.CCS_TRANSPORT_PROPERTY_TYPE_ID IN(4,9,3865 /*port*/) 
    THEN ':' || tp.CCS_TRANSPORT_PROPERTY_VALUE 
    WHEN UPPER(tpt.CCS_TPT_NAME) IN('CC') --tpt.CCS_TRANSPORT_PROPERTY_TYPE_ID IN(2 /*email cc:*/) 
    THEN '; cc: ' || tp.CCS_TRANSPORT_PROPERTY_VALUE ELSE NULL END)||
    MAX(CASE WHEN UPPER(tpt.CCS_TPT_NAME) IN('REMOTE PATH')
    THEN '/' ||tp.CCS_TRANSPORT_PROPERTY_VALUE ELSE NULL END), 0,100) ccs_transport_name
    FROM report.ccs_transport_property tp
    JOIN report.ccs_transport t ON tp.CCS_TRANSPORT_ID = t.CCS_TRANSPORT_ID
    JOIN report.CCS_TRANSPORT_PROPERTY_TYPE tpt ON tpt.CCS_TRANSPORT_TYPE_ID = t.CCS_TRANSPORT_TYPE_ID
    AND tpt.CCS_TRANSPORT_PROPERTY_TYPE_ID = tp.CCS_TRANSPORT_PROPERTY_TYPE_ID
    AND UPPER(tpt.CCS_TPT_NAME) IN('HOST', 'EMAIL', 'URL', 'PORT', 'CC', 'REMOTE PATH')
    where ccs_transport_status_cd<>'D' group by tp.ccs_transport_id, t.CCS_TRANSPORT_TYPE_ID)
  loop
    update report.ccs_transport set ccs_transport_name=l_ccs_name.ccs_transport_name where ccs_transport_id=l_ccs_name.ccs_transport_id;
  end loop;
  commit;
  
  
  select CCS_TRANSPORT_PROPERTY_TYPE_ID into l_property_type_id from report.ccs_transport_property_type where CCS_TRANSPORT_TYPE_ID=3 and CCS_TPT_NAME='Date Formatted Folder';
  FOR l_rec IN (select CCS_TRANSPORT_ID from REPORT.CCS_TRANSPORT where CCS_TRANSPORT_TYPE_ID=3 and ccs_transport_status_cd='N') LOOP
    insert into REPORT.CCS_TRANSPORT_PROPERTY(CCS_TRANSPORT_PROPERTY_ID, CCS_TRANSPORT_ID, CCS_TRANSPORT_PROPERTY_TYPE_ID, CCS_TRANSPORT_PROPERTY_VALUE) 
    values(report.ccs_transport_property_seq.nextval, l_rec.ccs_transport_id, l_property_type_id, null);
  END LOOP;
  commit;
  
  select CCS_TRANSPORT_PROPERTY_TYPE_ID into l_property_type_id from report.ccs_transport_property_type where CCS_TRANSPORT_TYPE_ID=5 and CCS_TPT_NAME='Date Formatted Folder';
  FOR l_rec IN (select CCS_TRANSPORT_ID from REPORT.CCS_TRANSPORT where CCS_TRANSPORT_TYPE_ID=5 and ccs_transport_status_cd='N') LOOP
    insert into REPORT.CCS_TRANSPORT_PROPERTY(CCS_TRANSPORT_PROPERTY_ID, CCS_TRANSPORT_ID, CCS_TRANSPORT_PROPERTY_TYPE_ID, CCS_TRANSPORT_PROPERTY_VALUE) 
    values(report.ccs_transport_property_seq.nextval, l_rec.ccs_transport_id, l_property_type_id, null);
  END LOOP;
  commit;
  
END;
/
-- unique ccs_transport_name 
CREATE UNIQUE INDEX report.udx_ccs_transport_name ON report.ccs_transport (DECODE(ccs_transport_status_cd, 'D', NULL, user_id||ccs_transport_name)) TABLESPACE REPORT_INDX;
