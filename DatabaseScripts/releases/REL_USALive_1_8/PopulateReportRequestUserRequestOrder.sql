DECLARE
  l_min_report_request_id NUMBER;
  l_max_report_request_id NUMBER;
BEGIN
	select min(report_request_id), max(report_request_id) into l_min_report_request_id, l_max_report_request_id from REPORT.REPORT_REQUEST where stored_file_id>0 and report_name is null;
    while l_min_report_request_id <= l_max_report_request_id loop
		UPDATE REPORT.REPORT_REQUEST RR SET (REPORT_NAME, MAX_PAGE_ID)=(
			SELECT  STORED_FILE_NAME, MAX(PAGE_ID) FROM REPORT.STORED_FILE SF
			WHERE SF.STORED_FILE_ID=RR.STORED_FILE_ID
			GROUP BY STORED_FILE_ID, STORED_FILE_NAME)
    	where report_request_id between l_min_report_request_id and l_min_report_request_id+1000-1;
		commit;
		l_min_report_request_id:=l_min_report_request_id+1000;
	end loop;
END;
/

DECLARE
  l_count NUMBER:=0;

BEGIN
	INSERT INTO REPORT.REPORT_REQUEST_ORDER (USER_ID, PROFILE_MAX_REQUEST_ORDER,USER_MAX_REQUEST_ORDER) 
	SELECT UL.USER_ID, NVL(P.PROFILE_MAX_REQUEST,0) PROFILE_MAX_REQUEST_ORDER, NVL(U.USER_MAX_REQUEST_ORDER,0) USER_MAX_REQUEST_ORDER FROM 
	REPORT.USER_LOGIN UL 
	LEFT OUTER JOIN
	(SELECT PROFILE_ID, count(*) PROFILE_MAX_REQUEST FROM REPORT.REPORT_REQUEST GROUP BY PROFILE_ID) P ON UL.USER_ID=P.PROFILE_ID
	LEFT OUTER JOIN
	(SELECT USER_ID, count(*) USER_MAX_REQUEST_ORDER FROM REPORT.REPORT_REQUEST_USER GROUP BY USER_ID) U ON UL.USER_ID=U.USER_ID;
	COMMIT;
	
	FOR l_rec IN (select user_id, profile_max_request_order from report.report_request_order where profile_max_request_order>0 ) LOOP
		FOR l_rec_user IN (select report_request_id from (
select report_request_id from REPORT.REPORT_REQUEST where profile_id=l_rec.user_id order by report_request_id desc) where rownum <401) LOOP
			update report.report_request set REQUEST_ORDER=l_rec.profile_max_request_order-l_count
			where report_request_id=l_rec_user.report_request_id;
			l_count:=l_count+1;
		END LOOP;
		commit;
		l_count:=0;
	END LOOP;
	
	FOR l_rec IN (select user_id, user_max_request_order from report.report_request_order where user_max_request_order>0 ) LOOP
		FOR l_rec_user IN (select report_request_user_id from (
select report_request_user_id from REPORT.REPORT_REQUEST_USER where user_id=l_rec.user_id and page_id=0 order by report_request_user_id desc) where rownum <401) LOOP
			update report.report_request_user set REQUEST_ORDER=l_rec.user_max_request_order-l_count
			where report_request_user_id=l_rec_user.report_request_user_id;
			l_count:=l_count+1;
		END LOOP;
		commit;
		l_count:=0;
	END LOOP;
	
END;
/

-- CREATE UNIQUE INDEX REPORT.IDX_REPORT_REQUEST_ORDER ON REPORT.REPORT_REQUEST(PROFILE_ID, REQUEST_ORDER) TABLESPACE REPORT_INDX ONLINE;
