ALTER TABLE
	REPORT.STORED_FILE
DROP CONSTRAINT PK_STORED_FILE;

ALTER TABLE
   REPORT.STORED_FILE
drop column
   PAGE_ID;

ALTER TABLE
	REPORT.REPORT_REQUEST_USER
drop column	
	PAGE_ID;

delete from web_content.literal where literal_key='report-cancel-invalid-pageid';
commit;

drop INDEX report.udx_ccs_transport_name;

update report.ccs_transport set ccs_transport_status_cd='N';

update report.user_report ur set ccs_transport_id=(
select ccs_transport_id from report.tmp_ccs_transport t
where t.user_report_id=ur.user_report_id)
where exists(select 1 from report.tmp_ccs_transport t
where t.user_report_id=ur.user_report_id);

delete from report.ccs_transport_status where ccs_transport_status_cd='D';

commit;   

ALTER TABLE
   REPORT.CCS_TRANSPORT
drop column
   USER_ID;

delete from report.ccs_transport_property where ccs_transport_property_type_id in (select ccs_transport_property_type_id from report.ccs_transport_property_type where ccs_tpt_name='Date Formatted Folder');
delete from report.CCS_TRANSPORT_PROPERTY_TYPE where CCS_TPT_NAME='Date Formatted Folder';
commit;

ALTER table
	REPORT.REPORT_REQUEST
DROP (REQUEST_ORDER, MAX_PAGE_ID);
	

ALTER table
	REPORT.REPORT_REQUEST_USER
DROP COLUMN REQUEST_ORDER;
	
DROP TRIGGER REPORT.TRBI_REPORT_REQUEST_ORDER;
DROP TRIGGER REPORT.TRBU_REPORT_REQUEST_ORDER;
DROP TABLE REPORT.REPORT_REQUEST_ORDER;

DROP TRIGGER REPORT.TRBI_USER_PREFERENCE;
DROP TRIGGER REPORT.TRBU_USER_PREFERENCE;
DROP TRIGGER REPORT.TRBI_PREFERENCE;
DROP TRIGGER REPORT.TRBU_PREFERENCE;
DROP TABLE REPORT.USER_PREFERENCE;
DROP TABLE REPORT.PREFERENCE;

  
delete from folio_conf.directive where directive_id=172;
commit;

delete from folio_conf.output_type where output_type_id in (2,4,7,8,11,23);

--select * from folio_conf.output_type where output_type_id in (22,24,27,28,31);

update folio_conf.output_type set output_type_key='xsl:resource:simple/falcon/templates/report/show-report-html.xsl', content_type='text/html' where output_type_id =22;

update folio_conf.output_type set output_type_key='pdf:resource:simple/falcon/templates/report/show-report-pdf.xsl' where output_type_id =24;

update folio_conf.output_type set output_type_key='xsl:resource:simple/falcon/templates/report/show-report-xls.xsl' where output_type_id =27;

update folio_conf.output_type set output_type_key='chart-xsl:resource:simple/falcon/templates/report/show-report-html.xsl', content_type='text/html' where output_type_id =28;

update folio_conf.output_type set output_type_key='chart-pdf:resource:simple/falcon/templates/report/show-report-pdf.xsl' where output_type_id=31;

commit;

delete from web_content.web_link_requirement where web_link_id=(select web_link_id from web_content.web_link where web_link_label='Dashboard');
delete from web_content.web_link where web_link_label='Dashboard';
commit;

update web_content.literal set literal_value='We at USA Technologies, Inc are continually striving to enhance our customers'' power to run their business effectively. And the USALive Website is loaded with features to help you do just that. For the best web site experience, please use the latest major version of your browser. Now new in Version 1.7: <ul><li>Enhanced Diagnostic Reports - you can view reports of machine alerts with greater customization</li><li>Password Reset - you can reset the password on your account securely in case you lose it</li><li>Fuller Browser Compatibility - so you can work in your favorite browser</li><li>Many Other minor functional and performance improvements</li></ul>' where literal_key='home-page-message' and subdomain_id is null; 
commit;



