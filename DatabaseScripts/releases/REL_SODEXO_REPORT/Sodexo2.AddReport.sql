SET DEFINE OFF;
DECLARE 
	l_user_id NUMBER;
BEGIN
  
select user_id into l_user_id from report.user_login ul cross join V$DATABASE d 
where user_name=(CASE WHEN d.name like 'USADEV%' THEN 'yhe@usatech.com'
WHEN d.name like 'ECC%' THEN 'aroyce@usatech.com'
ELSE 'acaddell' END);
  
INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
VALUES( REPORT.REPORTS_SEQ.NEXTVAL, 'ePort Summary Activity for {b} - {e}', 6, 0, 'ePort Summary Activity - CSV','Payment details with cash filtered by date range', 'N', l_user_id);

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( REPORT.REPORTS_SEQ.CURRVAL, 'folioId', '1650');
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( REPORT.REPORTS_SEQ.CURRVAL, 'outputType', '21');
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( REPORT.REPORTS_SEQ.CURRVAL, 'params.StartDate', '{b}');
INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
VALUES( REPORT.REPORTS_SEQ.CURRVAL, 'params.EndDate', '{e}');
commit;

    insert into web_content.web_link values(web_content.seq_web_link_id.nextval, 'ePort Summary Activity', './select_date_range_frame.i?folioId=1650' ,'ePort Summary Activity report.','Transaction Summaries','-', 10);
    insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 1);
    
    insert into report.user_link values(l_user_id,web_content.seq_web_link_id.currval,'Y','W');

COMMIT;

END;
/