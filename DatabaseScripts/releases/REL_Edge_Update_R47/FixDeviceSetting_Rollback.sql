set serveroutput on
     
BEGIN
	FOR l_dev in (select distinct device_id from yhe.tmp_device_setting_08252015) LOOP
		FOR l_rec IN (
			SELECT ds.device_setting_parameter_cd,DS.DEVICE_SETTING_VALUE, default_temp.DEVICE_SETTING_VALUE default_DEVICE_SETTING_VALUE
	    from device.device_setting ds
	    JOIN (select device_id,device_setting_parameter_cd,DEVICE_SETTING_VALUE from yhe.tmp_device_setting_08252015) default_temp
	    on ds.device_setting_parameter_cd=default_temp.device_setting_parameter_cd and ds.device_id=default_temp.device_id			
	    JOIN DEVICE.DEVICE D ON DS.DEVICE_ID = D.DEVICE_ID
			JOIN DEVICE.CONFIG_TEMPLATE_SETTING CTS ON DECODE(D.DEVICE_TYPE_ID, 1, 0, D.DEVICE_TYPE_ID) = CTS.DEVICE_TYPE_ID
				AND DS.DEVICE_SETTING_PARAMETER_CD = CTS.DEVICE_SETTING_PARAMETER_CD
				AND CTS.FIELD_OFFSET BETWEEN 0 AND 511
	    where ds.device_id=2531851
			ORDER BY CTS.FIELD_OFFSET
		) 
		LOOP
			update DEVICE.DEVICE_SETTING set DEVICE_SETTING_VALUE=l_rec.default_DEVICE_SETTING_VALUE
			where device_id=l_dev.device_id and device_setting_parameter_cd=l_rec.device_setting_parameter_cd;
			commit;
		END LOOP;
			DBMS_OUTPUT.PUT_LINE('Rollback Updated setting for ' || l_dev.device_id);
	END LOOP;
         
END;