DO $$
BEGIN
IF NOT EXISTS(select * from pg_constraint where conname = 'pk_account' and contype = 'p' and conkey = '{1,4}'::smallint[]) THEN
	ALTER TABLE KM.ACCOUNT DROP CONSTRAINT PK_ACCOUNT;
	ALTER TABLE KM.ACCOUNT ADD CONSTRAINT PK_ACCOUNT PRIMARY KEY(ENCRYPTED_ACCOUNT_CD, SOURCE_INSTANCE);
	DROP INDEX KM.UX_ACCOUNT_GLOBAL_ACCOUNT_ID;
	CREATE UNIQUE INDEX UX_ACCOUNT_GLOBAL_ACCOUNT_ID ON KM.ACCOUNT(GLOBAL_ACCOUNT_ID, SOURCE_INSTANCE);
END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION KM.UPSERT_ACCOUNT_ID(
    pn_old_global_account_id OUT KM.ACCOUNT.GLOBAL_ACCOUNT_ID%TYPE,
    pn_old_instance OUT KM.ACCOUNT.SOURCE_INSTANCE%TYPE,
    pn_changed OUT BOOLEAN,
    pn_global_account_id KM.ACCOUNT.GLOBAL_ACCOUNT_ID%TYPE,
    pb_encrypted_account_cd KM.ACCOUNT.ENCRYPTED_ACCOUNT_CD%TYPE,
    pb_encrypted_crc KM.ACCOUNT.ENCRYPTED_CRC%TYPE,
    pn_instance KM.ACCOUNT.SOURCE_INSTANCE%TYPE)
    SECURITY DEFINER
AS $$
DECLARE
    ln_old_encrypted_crc KM.ACCOUNT.ENCRYPTED_CRC%TYPE;
BEGIN
    FOR i IN 1..10 LOOP
        BEGIN
            INSERT INTO KM.ACCOUNT(ENCRYPTED_ACCOUNT_CD, GLOBAL_ACCOUNT_ID, SOURCE_INSTANCE, ENCRYPTED_CRC)
                VALUES(pb_encrypted_account_cd, pn_global_account_id, pn_instance, pb_encrypted_crc);
            pn_changed := TRUE;
            RETURN;
        EXCEPTION
            WHEN unique_violation THEN  
            -- Allow multiple rows (one for each instance) so that even it we override it we can still get the info with the old global_account_id
            --      this will also solve the issue of a TOKEN record without an ACCOUNT record
                SELECT GLOBAL_ACCOUNT_ID, SOURCE_INSTANCE, ENCRYPTED_CRC
                  INTO pn_old_global_account_id, pn_old_instance, ln_old_encrypted_crc
                  FROM KM.ACCOUNT
                 WHERE ENCRYPTED_ACCOUNT_CD = pb_encrypted_account_cd
                 ORDER BY SOURCE_INSTANCE;
                 IF pn_old_global_account_id IS NULL THEN
                    -- Didn't find the row which means either it was deleted out from under us or the global_account_id/source_instance already exists but not with this encrypted_account_cd
                    UPDATE KM.ACCOUNT
                       SET ENCRYPTED_ACCOUNT_CD = pb_encrypted_account_cd,
                           ENCRYPTED_CRC = COALESCE(pb_encrypted_crc, ENCRYPTED_CRC)
                     WHERE GLOBAL_ACCOUNT_ID = pn_global_account_id
                       AND SOURCE_INSTANCE = pn_instance;
                    IF NOT FOUND THEN
                        CONTINUE; -- Try again
                    END IF;
                    pn_changed := TRUE;
                    pn_old_global_account_id := pn_global_account_id;
                    pn_old_instance := pn_instance;
                    RETURN;
                ELSIF pn_old_instance < pn_instance OR (pn_old_instance = pn_instance AND pn_old_global_account_id >= pn_global_account_id 
                        AND (pb_encrypted_crc IS NULL OR pb_encrypted_crc IS NOT DISTINCT FROM ln_old_encrypted_crc))                   
                        THEN
                    pn_changed := FALSE;
                    RETURN;
                END IF;
                IF pb_encrypted_crc IS NOT NULL THEN
                    UPDATE KM.ACCOUNT
                       SET ENCRYPTED_CRC = pb_encrypted_crc
                     WHERE ENCRYPTED_ACCOUNT_CD = pb_encrypted_account_cd
                       AND SOURCE_INSTANCE = pn_instance
                       AND GLOBAL_ACCOUNT_ID = pn_global_account_id;
                END IF;
                pn_changed := TRUE;
                RETURN;
        END;    
    END LOOP;
    RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION KM.GET_OR_CREATE_ACCOUNT_ID(
    pn_global_account_id OUT KM.ACCOUNT.GLOBAL_ACCOUNT_ID%TYPE,
    pn_old_instance OUT KM.ACCOUNT.SOURCE_INSTANCE%TYPE,
    pn_changed OUT BOOLEAN,
    pb_encrypted_account_cd KM.ACCOUNT.ENCRYPTED_ACCOUNT_CD%TYPE,
    pb_encrypted_crc KM.ACCOUNT.ENCRYPTED_CRC%TYPE,
    pn_instance KM.ACCOUNT.SOURCE_INSTANCE%TYPE)
    SECURITY DEFINER
AS $$
DECLARE
    lb_old_encrypted_crc KM.ACCOUNT.ENCRYPTED_CRC%TYPE;
BEGIN
    FOR i IN 1..10 LOOP
        SELECT GLOBAL_ACCOUNT_ID, SOURCE_INSTANCE, ENCRYPTED_CRC 
          INTO pn_global_account_id, pn_old_instance, lb_old_encrypted_crc
          FROM KM.ACCOUNT
         WHERE ENCRYPTED_ACCOUNT_CD = pb_encrypted_account_cd
         ORDER BY SOURCE_INSTANCE;
        IF FOUND THEN
            IF (pb_encrypted_crc IS NOT NULL AND pb_encrypted_crc IS DISTINCT FROM lb_old_encrypted_crc) THEN
                UPDATE KM.ACCOUNT
                   SET ENCRYPTED_CRC = COALESCE(pb_encrypted_crc, ENCRYPTED_CRC)                       
                 WHERE ENCRYPTED_ACCOUNT_CD = pb_encrypted_account_cd
                   AND SOURCE_INSTANCE = pn_old_instance
                   AND GLOBAL_ACCOUNT_ID = pn_global_account_id
                   AND (pb_encrypted_crc IS NOT NULL AND pb_encrypted_crc IS DISTINCT FROM ENCRYPTED_CRC);
                pn_changed := TRUE; 
            ELSE
                pn_changed := FALSE;
            END IF;
            RETURN;
        END IF;
        BEGIN
            SELECT NEXTVAL('km.seq_account_id_base'::regclass) + pn_instance INTO pn_global_account_id;
            INSERT INTO KM.ACCOUNT(ENCRYPTED_ACCOUNT_CD, GLOBAL_ACCOUNT_ID, ENCRYPTED_CRC, SOURCE_INSTANCE)
                VALUES(pb_encrypted_account_cd, pn_global_account_id, pb_encrypted_crc, pn_instance);
            pn_changed := TRUE;
            RETURN;
        EXCEPTION
            WHEN unique_violation THEN    
                NULL;
        END;    
    END LOOP;  
    RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION KM.GET_OR_CREATE_ACCOUNT_ID_WITH_CRC(
    pn_global_account_id OUT KM.ACCOUNT.GLOBAL_ACCOUNT_ID%TYPE,
    pn_old_instance OUT KM.ACCOUNT.SOURCE_INSTANCE%TYPE,
    pb_old_encrypted_crc OUT KM.ACCOUNT.ENCRYPTED_CRC%TYPE,
    pn_changed OUT BOOLEAN,
    pb_encrypted_account_cd KM.ACCOUNT.ENCRYPTED_ACCOUNT_CD%TYPE,
    pb_encrypted_crc KM.ACCOUNT.ENCRYPTED_CRC%TYPE,
    pn_instance KM.ACCOUNT.SOURCE_INSTANCE%TYPE)
    SECURITY DEFINER
AS $$
DECLARE
BEGIN
    FOR i IN 1..10 LOOP
        SELECT GLOBAL_ACCOUNT_ID, SOURCE_INSTANCE, ENCRYPTED_CRC 
          INTO pn_global_account_id, pn_old_instance, pb_old_encrypted_crc
          FROM KM.ACCOUNT
         WHERE ENCRYPTED_ACCOUNT_CD = pb_encrypted_account_cd
         ORDER BY SOURCE_INSTANCE;
        IF FOUND THEN
            IF (pb_encrypted_crc IS NOT NULL AND pb_encrypted_crc IS DISTINCT FROM pb_old_encrypted_crc) THEN
                UPDATE KM.ACCOUNT
                   SET ENCRYPTED_CRC = COALESCE(pb_encrypted_crc, ENCRYPTED_CRC)                       
                 WHERE ENCRYPTED_ACCOUNT_CD = pb_encrypted_account_cd
                   AND SOURCE_INSTANCE = pn_old_instance
                   AND GLOBAL_ACCOUNT_ID = pn_global_account_id
                   AND (pb_encrypted_crc IS NOT NULL AND pb_encrypted_crc IS DISTINCT FROM ENCRYPTED_CRC);
                pn_changed := TRUE; 
            ELSE
                pn_changed := FALSE;
            END IF;
            RETURN;
        END IF;
        BEGIN
            SELECT NEXTVAL('km.seq_account_id_base'::regclass) + pn_instance INTO pn_global_account_id;
            INSERT INTO KM.ACCOUNT(ENCRYPTED_ACCOUNT_CD, GLOBAL_ACCOUNT_ID, ENCRYPTED_CRC, SOURCE_INSTANCE)
                VALUES(pb_encrypted_account_cd, pn_global_account_id, pb_encrypted_crc, pn_instance);
            pn_changed := TRUE;
            RETURN;
        EXCEPTION
            WHEN unique_violation THEN    
                NULL;
        END;    
    END LOOP;  
    RAISE serialization_failure;
END;
$$ LANGUAGE plpgsql;

COMMIT;