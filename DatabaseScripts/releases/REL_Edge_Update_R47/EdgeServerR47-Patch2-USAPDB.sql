WHENEVER SQLERROR EXIT FAILURE COMMIT;

GRANT SELECT ON REPORT.TRANS_TYPE TO PSS WITH GRANT OPTION;

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/CCS_PKG.pbk?revision=1.26
CREATE OR REPLACE PACKAGE BODY REPORT.CCS_PKG IS
   
    PROCEDURE ALERT_NOTIF_INS(
       l_alert_notif_id  OUT REPORT.ALERT_NOTIF.alert_notif_id%TYPE,
        l_ccs_transport_id  OUT CCS_TRANSPORT.ccs_transport_id%TYPE, 
        l_user_id  IN ALERT_NOTIF.user_id%TYPE,
        l_allTerminalFlag IN ALERT_NOTIF.all_terminals_flag%TYPE,      
        l_terminalId  ID_LIST,
        l_alerTypeId  ID_LIST)
    IS
    BEGIN
        SELECT CCS_TRANSPORT_SEQ.NEXTVAL,
               SEQ_ALERT_NOTIF_ID.NEXTVAL
          INTO l_ccs_transport_id,
               l_alert_notif_id
          FROM DUAL;
        
         INSERT INTO CCS_TRANSPORT(CCS_TRANSPORT_ID, CCS_TRANSPORT_TYPE_ID, CCS_TRANSPORT_NAME)
            VALUES(l_ccs_transport_id, 1,
                'Transport for Alert Notif Id = ' || TO_CHAR(l_alert_notif_id));
    	 INSERT INTO ALERT_NOTIF (ALERT_NOTIF_ID, USER_ID, ccs_transport_id,All_TERMINALS_FLAG)
  	 		VALUES(l_alert_notif_id, l_user_id, l_ccs_transport_id,l_allTerminalFlag);
        
        
         IF l_allTerminalFlag <> 'Y' THEN
          INSERT INTO ALERT_NOTIF_TERMINAL (
              ALERT_NOTIF_TERMINAL_ID,
              ALERT_NOTIF_ID,
              TERMINAL_ID)
            SELECT 
              SEQ_ALERT_NOTIF_TERMINAL_ID.nextval,
              l_alert_notif_id,
              TERMINAL_ID
            FROM REPORT.VW_USER_TERMINAL
          WHERE USER_ID = l_user_id
           AND TERMINAL_ID MEMBER OF l_terminalId; 
         END IF;
         
         INSERT INTO ALERT_NOTIF_ALERT_TYPE (
               ALERT_NOTIF_ALERT_TYPE_ID,
               ALERT_NOTIF_ID,
               ALERT_TYPE_ID)
            SELECT 
               SEQ_ALERT_NOTIF_ALERT_TYPE_ID.nextval,
               l_alert_notif_id,
               ALERT_TYPE_ID
            FROM REPORT.ALERT_TYPE
           WHERE ALERT_TYPE_ID MEMBER OF l_alerTypeId;
    END;
    
    

    PROCEDURE ALERT_NOTIF_DEL(
        l_alert_notif_id IN ALERT_NOTIF.ALERT_NOTIF_ID%TYPE)
    IS
    BEGIN
    	 DELETE FROM  ALERT_NOTIF_ALERT_TYPE WHERE ALERT_NOTIF_ID = l_alert_notif_id;
    	 DELETE FROM  ALERT_NOTIF_TERMINAL WHERE ALERT_NOTIF_ID = l_alert_notif_id;
    	 DELETE FROM  ALERT_NOTIF WHERE ALERT_NOTIF_ID = l_alert_notif_id;
    END;
    
   PROCEDURE ALERT_NOTIF_UPD(
        l_alert_notif_id IN ALERT_NOTIF.ALERT_NOTIF_ID%TYPE,
        l_user_id  IN ALERT_NOTIF.user_id%TYPE,
        l_allTerminalFlag IN ALERT_NOTIF.all_terminals_flag%TYPE,
        l_terminal_id ID_LIST,
        l_alert_type_id ID_LIST,
        l_transport_id OUT CCS_TRANSPORT.CCS_TRANSPORT_ID%TYPE)
    IS
    BEGIN
    	 SELECT CCS_TRANSPORT_ID
          INTO  l_transport_id
          FROM ALERT_NOTIF 
         WHERE ALERT_NOTIF_ID = l_alert_notif_id;
        
         UPDATE ALERT_NOTIF
          SET   CCS_TRANSPORT_ID=l_transport_id, ALL_TERMINALS_FLAG=l_allTerminalFlag
         WHERE  ALERT_NOTIF_ID=l_alert_notif_id;
         DELETE FROM  ALERT_NOTIF_TERMINAL WHERE ALERT_NOTIF_ID = l_alert_notif_id;       
         IF l_allTerminalFlag <> 'Y' THEN
          INSERT INTO ALERT_NOTIF_TERMINAL (
              ALERT_NOTIF_TERMINAL_ID,
              ALERT_NOTIF_ID,
              TERMINAL_ID)
            SELECT 
              SEQ_ALERT_NOTIF_TERMINAL_ID.nextval,
              l_alert_notif_id,
              TERMINAL_ID
            FROM REPORT.VW_USER_TERMINAL
          WHERE USER_ID = l_user_id
           AND TERMINAL_ID MEMBER OF l_terminal_id; 
         END IF;
         
         DELETE FROM  ALERT_NOTIF_ALERT_TYPE WHERE ALERT_NOTIF_ID = l_alert_notif_id;
         INSERT INTO ALERT_NOTIF_ALERT_TYPE (
               ALERT_NOTIF_ALERT_TYPE_ID,
               ALERT_NOTIF_ID,
               ALERT_TYPE_ID)
            SELECT 
               SEQ_ALERT_NOTIF_ALERT_TYPE_ID.nextval,
               l_alert_notif_id,
               ALERT_TYPE_ID
            FROM REPORT.ALERT_TYPE
           WHERE ALERT_TYPE_ID MEMBER OF l_alert_type_id;
        
    END;
    
    -- usalive 1.7.5
    PROCEDURE USER_REPORT_INS(
        l_user_id IN USER_REPORT.USER_ID%TYPE,
        l_report_id IN USER_REPORT.REPORT_ID%TYPE,
        l_frequency_id IN USER_REPORT.FREQUENCY_ID%TYPE,
        l_transport_type_id IN CCS_TRANSPORT.CCS_TRANSPORT_TYPE_ID%TYPE,
        l_user_report_id OUT USER_REPORT.USER_REPORT_ID%TYPE,
        l_transport_id OUT CCS_TRANSPORT.CCS_TRANSPORT_ID%TYPE)
    IS
        l_freq REPORT.USER_REPORT.FREQUENCY_ID%TYPE;
        l_batch_type_id REPORT.REPORTS.BATCH_TYPE_ID%TYPE;
        l_start_date REPORT.USER_REPORT.UPD_DT%TYPE := SYSDATE;
    BEGIN
        SELECT DECODE(R.BATCH_TYPE_ID, 0, l_frequency_id, NULL),
               REPORT.CCS_TRANSPORT_SEQ.NEXTVAL,
               REPORT.USER_REPORT_SEQ.NEXTVAL
          INTO l_freq,
               l_transport_id,
               l_user_report_id
          FROM REPORT.REPORTS R
         WHERE R.REPORT_ID = l_report_id;
        IF l_freq IS NOT NULL THEN
            SELECT ADD_MONTHS(SYSDATE, -NVL(F.MONTHS, 0)) - NVL(F.DAYS, 0) 
              INTO l_start_date
              FROM CORP.FREQUENCY F
             WHERE F.FREQUENCY_ID = l_freq;
        END IF;
        INSERT INTO REPORT.CCS_TRANSPORT(CCS_TRANSPORT_ID, CCS_TRANSPORT_TYPE_ID, CCS_TRANSPORT_NAME)
            VALUES(l_transport_id, l_transport_type_id,
                'Transport for User Report Id = ' || TO_CHAR(l_user_report_id));
    	INSERT INTO REPORT.USER_REPORT (USER_REPORT_ID, USER_ID, REPORT_ID, FREQUENCY_ID, CCS_TRANSPORT_ID, UPD_DT)
  	 		VALUES(l_user_report_id, l_user_id, l_report_id, l_freq, l_transport_id, l_start_date);
    EXCEPTION
    	WHEN NO_DATA_FOUND THEN
    	 	 RAISE_APPLICATION_ERROR(-20100, 'Could not find this report');
    	WHEN OTHERS THEN
    	 	 RAISE;
    END;
    
  --usalive 1.8 
  PROCEDURE USER_REPORT_INS(
        l_user_id IN USER_REPORT.USER_ID%TYPE,
        l_report_id IN USER_REPORT.REPORT_ID%TYPE,
        l_frequency_id IN USER_REPORT.FREQUENCY_ID%TYPE,
        l_transport_id IN CCS_TRANSPORT.CCS_TRANSPORT_ID%TYPE)
    IS
        l_freq REPORT.USER_REPORT.FREQUENCY_ID%TYPE;
        l_batch_type_id REPORT.REPORTS.BATCH_TYPE_ID%TYPE;
        l_start_date REPORT.USER_REPORT.UPD_DT%TYPE := SYSDATE;
        l_user_report_id USER_REPORT.USER_REPORT_ID%TYPE;
        l_user_transport_id CCS_TRANSPORT.CCS_TRANSPORT_ID%TYPE;
    BEGIN
        SELECT DECODE(R.BATCH_TYPE_ID, 0, l_frequency_id, NULL),
               REPORT.USER_REPORT_SEQ.NEXTVAL
          INTO l_freq,
               l_user_report_id
          FROM REPORT.REPORTS R
         WHERE R.REPORT_ID = l_report_id;
        IF l_freq IS NOT NULL THEN
            SELECT ADD_MONTHS(SYSDATE, -NVL(F.MONTHS, 0)) - NVL(F.DAYS, 0) 
              INTO l_start_date
              FROM CORP.FREQUENCY F
             WHERE F.FREQUENCY_ID = l_freq;
        END IF;
      select ccs_transport_id into l_user_transport_id from report.ccs_transport where ccs_transport_id=l_transport_id and user_id=l_user_id;
    	INSERT INTO REPORT.USER_REPORT (USER_REPORT_ID, USER_ID, REPORT_ID, FREQUENCY_ID, CCS_TRANSPORT_ID, UPD_DT)
  	 		VALUES(l_user_report_id, l_user_id, l_report_id, l_freq, l_user_transport_id, l_start_date);
    EXCEPTION
    	WHEN NO_DATA_FOUND THEN
    	 	 RAISE_APPLICATION_ERROR(-20100, 'Could not find this report');
    	WHEN OTHERS THEN
    	 	 RAISE;
    END;
    
    -- usalive 1.7.5
    PROCEDURE USER_REPORT_UPD(
        l_user_report_id IN USER_REPORT.USER_REPORT_ID%TYPE,
        l_frequency_id IN USER_REPORT.FREQUENCY_ID%TYPE,
        l_transport_type_id IN CCS_TRANSPORT.CCS_TRANSPORT_TYPE_ID%TYPE,
        l_transport_id OUT CCS_TRANSPORT.CCS_TRANSPORT_ID%TYPE)
    IS
      l_freq USER_REPORT.FREQUENCY_ID%TYPE;
    BEGIN
    	 SELECT DECODE(R.BATCH_TYPE_ID, 0, l_frequency_id, NULL), UR.CCS_TRANSPORT_ID
          INTO l_freq, l_transport_id
          FROM REPORTS R, USER_REPORT UR
         WHERE R.REPORT_ID = UR.REPORT_ID
           AND UR.USER_REPORT_ID = l_user_report_id;
         UPDATE USER_REPORT SET FREQUENCY_ID = l_freq
          WHERE USER_REPORT_ID = l_user_report_id;
         UPDATE CCS_TRANSPORT SET CCS_TRANSPORT_TYPE_ID = l_transport_type_id
          WHERE CCS_TRANSPORT_ID = l_transport_id;
    EXCEPTION
    	 WHEN NO_DATA_FOUND THEN
    	 	 RAISE_APPLICATION_ERROR(-20100, 'Could not find this report');
    	 WHEN OTHERS THEN
    	 	 RAISE;
    END;
    
    -- usalive 1.8
    PROCEDURE USER_REPORT_UPD(
        l_user_report_id IN USER_REPORT.USER_REPORT_ID%TYPE,
        l_frequency_id IN USER_REPORT.FREQUENCY_ID%TYPE,
        l_transport_id IN CCS_TRANSPORT.CCS_TRANSPORT_ID%TYPE)
    IS
      l_freq USER_REPORT.FREQUENCY_ID%TYPE;
    BEGIN
    	 SELECT DECODE(R.BATCH_TYPE_ID, 0, l_frequency_id, NULL)
          INTO l_freq
          FROM REPORTS R JOIN USER_REPORT UR ON R.REPORT_ID = UR.REPORT_ID
         WHERE UR.USER_REPORT_ID = l_user_report_id;
         UPDATE USER_REPORT SET FREQUENCY_ID = l_freq, 
          CCS_TRANSPORT_ID = l_transport_id 
          WHERE USER_REPORT_ID = l_user_report_id;

    EXCEPTION
    	 WHEN NO_DATA_FOUND THEN
    	 	 RAISE_APPLICATION_ERROR(-20100, 'Could not find this report');
    	 WHEN OTHERS THEN
    	 	 RAISE;
    END;
    
    PROCEDURE USER_REPORT_DEL(
        l_user_report_id IN USER_REPORT.USER_REPORT_ID%TYPE)
    IS
    BEGIN
    	 UPDATE USER_REPORT SET STATUS = 'D' WHERE USER_REPORT_ID = l_user_report_id;
    END;
    
    PROCEDURE TRANSPORT_PROP_UPD(
        l_transport_id IN CCS_TRANSPORT_PROPERTY.CCS_TRANSPORT_ID%TYPE,
        l_tpt_id IN CCS_TRANSPORT_PROPERTY.CCS_TRANSPORT_PROPERTY_TYPE_ID%TYPE,
        l_value IN CCS_TRANSPORT_PROPERTY.CCS_TRANSPORT_PROPERTY_VALUE%TYPE)
    IS
    BEGIN
    	UPDATE CCS_TRANSPORT_PROPERTY
           SET CCS_TRANSPORT_PROPERTY_VALUE = l_value
         WHERE CCS_TRANSPORT_PROPERTY_TYPE_ID = l_tpt_id
           AND CCS_TRANSPORT_ID = l_transport_id;
        IF SQL%ROWCOUNT = 0 THEN
            INSERT INTO CCS_TRANSPORT_PROPERTY(
                CCS_TRANSPORT_PROPERTY_ID,
                CCS_TRANSPORT_ID,
                CCS_TRANSPORT_PROPERTY_TYPE_ID,
                CCS_TRANSPORT_PROPERTY_VALUE)
              VALUES(
                CCS_TRANSPORT_PROPERTY_SEQ.NEXTVAL,
                l_transport_id,
                l_tpt_id,
                l_value
              );
        END IF;
    END;
    
    PROCEDURE REPORT_SENT_INS (
        l_user_report_id IN REPORT_SENT.USER_REPORT_ID%TYPE,
        l_report_request_id IN REPORT_SENT.REPORT_REQUEST_ID%TYPE,
        l_batch_id IN REPORT_SENT.BATCH_ID%TYPE,
        l_begin_date IN REPORT_SENT.BEGIN_DATE%TYPE,
        l_end_date IN REPORT_SENT.END_DATE%TYPE,
        l_rs_id OUT REPORT_SENT.REPORT_SENT_ID%TYPE,
        l_batch_type_id  IN REPORT_SENT.EXPORT_TYPE%TYPE  DEFAULT NULL)
    IS
        l_cnt PLS_INTEGER;
    BEGIN
        
        SELECT REPORT_SENT_SEQ.NEXTVAL INTO l_rs_id FROM DUAL;
        
        INSERT INTO REPORT_SENT(
            REPORT_SENT_ID,
            REPORT_REQUEST_ID,
            USER_REPORT_ID,
            BEGIN_DATE,
            END_DATE,
            BATCH_ID,
            EXPORT_TYPE,
            REPORT_SENT_STATE_ID,
            DETAILS)
          VALUES(
            l_rs_id,
            l_report_request_id,
            l_user_report_id,
            l_begin_date,
            l_end_date,
            l_batch_id,
            l_batch_type_id,
            0,
            'NEW');
        
        IF l_batch_id IS NULL THEN               
            DELETE FROM RESEND_REPORTS
             WHERE USER_REPORT_ID = l_user_report_id
               AND BATCH_ID IS NULL
               AND BEGIN_DATE = l_begin_date
               AND END_DATE = l_end_date;
        ELSE    
            DELETE FROM RESEND_REPORTS
             WHERE USER_REPORT_ID = l_user_report_id
               AND BATCH_ID = l_batch_id;
            /* NOT NEEDED - handled in report requester   
            SELECT COUNT(*)
              INTO l_cnt
              FROM RESEND_REPORTS
             WHERE BATCH_ID = l_batch_id;
            IF l_cnt = 0 THEN
                IF l_batch_type_id = 3 THEN -- EFT
                    SELECT COUNT(*)
                      INTO l_cnt
                      FROM VW_UNSENT_EFT
                     WHERE BATCH_ID = l_batch_id;
                    IF l_cnt = 0 THEN
                        UPDATE CORP.DOC SET STATUS = 'S'
                         WHERE DOC_ID = l_batch_id;
                    END IF;
                ELSIF l_batch_type_id = 2 THEN -- DEX
                    SELECT COUNT(*)
                      INTO l_cnt
                      FROM VW_UNSENT_DEX
                     WHERE BATCH_ID = l_batch_id;
                    IF l_cnt = 0 THEN
                        UPDATE G4OP.DEX_FILE SET SENT = 'Y'
                         WHERE DEX_FILE_ID = l_batch_id;
                    END IF;
               -- This was causing performance issues
                 ELSE -- TRANS
                    SELECT COUNT(*)
                      INTO l_cnt
                      FROM VW_UNSENT_TRANS
                     WHERE BATCH_ID = l_batch_id;
                    IF l_cnt = 0 THEN
                        UPDATE EXPORT_BATCH SET EXPORT_SENT = l_sent_date, STATUS = 'S'
                         WHERE BATCH_ID = l_batch_id AND EXPORT_TYPE = l_batch_type_id;
                    END IF;               
                END IF;
            END IF;
            --*/
        END IF;      
    END;
    
    -- pre usalive 1.9 signature
    PROCEDURE CCS_TRANSPORT_ERROR_INS(
        pn_report_sent_id IN REPORT_SENT.REPORT_SENT_ID%TYPE,
        pv_error_text IN CCS_TRANSPORT_ERROR.ERROR_TEXT%TYPE,
        pd_error_ts IN CCS_TRANSPORT_ERROR.ERROR_TS%TYPE,
        pn_error_id OUT CCS_TRANSPORT_ERROR.CCS_TRANSPORT_ERROR_ID%TYPE)
    IS
    BEGIN
        CCS_TRANSPORT_ERROR_INS(pn_report_sent_id,pv_error_text, pd_error_ts,1, '', pn_error_id);
    END;
    
    PROCEDURE CCS_TRANSPORT_ERROR_INS(
        pn_report_sent_id IN REPORT_SENT.REPORT_SENT_ID%TYPE,
        pv_error_text IN CCS_TRANSPORT_ERROR.ERROR_TEXT%TYPE,
        pd_error_ts IN CCS_TRANSPORT_ERROR.ERROR_TS%TYPE,
        pd_error_type_id IN CCS_TRANSPORT_ERROR.ERROR_TYPE_ID%TYPE,
        pv_retry_props IN CCS_TRANSPORT_ERROR.RETRY_PROPS%TYPE,
        pn_error_id OUT CCS_TRANSPORT_ERROR.CCS_TRANSPORT_ERROR_ID%TYPE)
    IS
    BEGIN
        UPDATE REPORT.CCS_TRANSPORT 
           SET CCS_TRANSPORT_STATUS_CD = decode(CCS_TRANSPORT_STATUS_CD, 'D', 'D','E', 'E', 'R') 
         WHERE CCS_TRANSPORT_ID = (
           SELECT UR.CCS_TRANSPORT_ID 
             FROM REPORT.REPORT_SENT RS
             JOIN REPORT.USER_REPORT UR ON UR.USER_REPORT_ID = RS.USER_REPORT_ID
            WHERE RS.REPORT_SENT_ID = pn_report_sent_id);
        SELECT REPORT.SEQ_CCS_TRANSPORT_ERROR_ID.NEXTVAL 
          INTO pn_error_id 
           FROM DUAL;
        INSERT INTO REPORT.CCS_TRANSPORT_ERROR(
           CCS_TRANSPORT_ERROR_ID, 
           REPORT_SENT_ID, 
           ERROR_TEXT, 
           ERROR_TS,
           ERROR_TYPE_ID,
           RETRY_PROPS)
         VALUES(
           pn_error_id, 
           pn_report_sent_id, 
           pv_error_text, 
           pd_error_ts,
           pd_error_type_id,
           pv_retry_props);
    END;
    
    PROCEDURE CCS_TRANSPORT_ERROR_RETRY(
        pn_ct_error_id IN CCS_TRANSPORT_ERROR.CCS_TRANSPORT_ERROR_ID%TYPE)
    IS
    BEGIN
        UPDATE CCS_TRANSPORT_ERROR 
           SET REATTEMPTED_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP), 
               REATTEMPTED_FLAG = 'Y'
         WHERE CCS_TRANSPORT_ERROR_ID = pn_ct_error_id;
    END;
    
    PROCEDURE CCS_TRANSPORT_RETRY_SUCCESS(
        pn_ccs_transport_id IN CCS_TRANSPORT.CCS_TRANSPORT_ID%TYPE, 
        pn_ct_error_id IN CCS_TRANSPORT_ERROR.CCS_TRANSPORT_ERROR_ID%TYPE)
    IS
    BEGIN
        UPDATE CCS_TRANSPORT_ERROR 
           SET REATTEMPTED_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP), 
               REATTEMPTED_FLAG = 'Y',
               RETRY_COUNT = RETRY_COUNT+1
         WHERE CCS_TRANSPORT_ERROR_ID = pn_ct_error_id;
    END;
    
    PROCEDURE CCS_TRANSPORT_ERROR_UPDATE(
        pn_ccs_transport_id IN CCS_TRANSPORT.CCS_TRANSPORT_ID%TYPE, 
        pn_ct_error_id IN CCS_TRANSPORT_ERROR.CCS_TRANSPORT_ERROR_ID%TYPE,
        pv_error_text IN CCS_TRANSPORT_ERROR.ERROR_TEXT%TYPE,
        pd_error_type_id IN CCS_TRANSPORT_ERROR.ERROR_TYPE_ID%TYPE,
        pv_retry_props IN CCS_TRANSPORT_ERROR.RETRY_PROPS%TYPE,
        pn_disabled_transport OUT NUMBER)
    IS
      l_reattempted_ts CCS_TRANSPORT_ERROR.REATTEMPTED_TS%TYPE:=SYS_EXTRACT_UTC(SYSTIMESTAMP);
      l_created_ts CCS_TRANSPORT_ERROR.CREATED_TS%TYPE;
      l_prev_error_text CCS_TRANSPORT_ERROR.ERROR_TEXT%TYPE;
      l_prev_reattempted_ts CCS_TRANSPORT_ERROR.REATTEMPTED_TS%TYPE;
      l_retry_index NUMBER;
      l_last_sent_ts REPORT.USER_REPORT.LAST_SENT_UTC_TS%TYPE;
    BEGIN
       select ERROR_TEXT, REATTEMPTED_TS into l_prev_error_text, l_prev_reattempted_ts 
       from REPORT.CCS_TRANSPORT_ERROR WHERE CCS_TRANSPORT_ERROR_ID = pn_ct_error_id;
       
       UPDATE REPORT.CCS_TRANSPORT_ERROR 
           SET REATTEMPTED_TS = l_reattempted_ts,
               REATTEMPTED_FLAG = decode(REATTEMPTED_FLAG, 'D', 'D','N'),
               ERROR_TEXT = pv_error_text,
               ERROR_TYPE_ID = pd_error_type_id,
               RETRY_COUNT = RETRY_COUNT+1,
               RETRY_PROPS=pv_retry_props
         WHERE CCS_TRANSPORT_ERROR_ID = pn_ct_error_id returning created_ts, retry_count into l_created_ts, l_retry_index;
         
       insert into REPORT.CCS_TRANSPORT_ERROR_HISTORY(CCS_TRANSPORT_ERROR_ID, RETRY_INDEX, ERROR_TEXT,REATTEMPTED_TS)
       values( pn_ct_error_id, l_retry_index, l_prev_error_text, l_prev_reattempted_ts);
       
      IF (pd_error_type_id = 1 or pd_error_type_id = 3) and l_reattempted_ts > l_created_ts + 7 THEN
        select max(NVL(LAST_SENT_UTC_TS, SYS_EXTRACT_UTC(CAST (MIN_DATE AS TIMESTAMP)))) into l_last_sent_ts
        from report.user_report 
        where ccs_transport_id=pn_ccs_transport_id;
        
        IF l_reattempted_ts > l_last_sent_ts + 7 THEN
          UPDATE REPORT.CCS_TRANSPORT 
             SET CCS_TRANSPORT_STATUS_CD = 'E' 
          WHERE CCS_TRANSPORT_ID = pn_ccs_transport_id;   
          pn_disabled_transport:=1;
        ELSE
          UPDATE REPORT.USER_REPORT 
          SET STATUS='E'
          WHERE USER_REPORT_ID = (SELECT USER_REPORT_ID FROM REPORT.REPORT_SENT WHERE REPORT_SENT_ID=(SELECT REPORT_SENT_ID FROM REPORT.CCS_TRANSPORT_ERROR WHERE CCS_TRANSPORT_ERROR_ID = pn_ct_error_id))
          AND l_reattempted_ts > NVL(LAST_SENT_UTC_TS, SYS_EXTRACT_UTC(CAST (MIN_DATE AS TIMESTAMP)))+7;
          
          pn_disabled_transport:=0;
        END IF;
        
        UPDATE REPORT.CCS_TRANSPORT_ERROR set REATTEMPTED_FLAG='D' 
        WHERE CCS_TRANSPORT_ERROR_ID = pn_ct_error_id;
        
        UPDATE REPORT.REPORT_SENT set REPORT_SENT_STATE_ID=5 
        WHERE REPORT_SENT_ID = (SELECT REPORT_SENT_ID FROM REPORT.CCS_TRANSPORT_ERROR WHERE CCS_TRANSPORT_ERROR_ID = pn_ct_error_id);
      ELSE
        pn_disabled_transport:=0;
      END IF;
    END;
    
    PROCEDURE CCS_UPDATE_REATTEMPTED_FLAG(
        pn_ct_error_id IN CCS_TRANSPORT_ERROR.CCS_TRANSPORT_ERROR_ID%TYPE,
        pn_reattempted_flag IN CCS_TRANSPORT_ERROR.REATTEMPTED_FLAG%TYPE)
    IS
    BEGIN
      UPDATE REPORT.CCS_TRANSPORT_ERROR SET REATTEMPTED_FLAG=pn_reattempted_flag WHERE CCS_TRANSPORT_ERROR_ID=pn_ct_error_id;
    END;
    
    -- BEGIN Device alerting additions
    -- -------------------------------
    -- R45 and above
    PROCEDURE ADD_EVENT_ALERT(
        pn_event_id IN REPORT.TERMINAL_ALERT.REFERENCE_ID%TYPE,
        pn_terminal_alert_id OUT REPORT.TERMINAL_ALERT.TERMINAL_ALERT_ID%TYPE,
        pn_terminal_id OUT REPORT.TERMINAL.TERMINAL_ID%TYPE,
        pv_device_serial_cd OUT REPORT.EPORT.EPORT_SERIAL_NUM%TYPE,
        pc_dont_send_flag OUT VARCHAR2)
    IS
        lc_alert_status REPORT.ALERT.STATUS%TYPE;
        ln_alert_id REPORT.ALERT.ALERT_ID%TYPE;
        ld_alert_ts REPORT.TERMINAL_ALERT.ALERT_DATE%TYPE;
        lv_alert_description REPORT.TERMINAL_ALERT.DETAILS%TYPE;
        ln_component REPORT.TERMINAL_ALERT.COMPONENT_ID%TYPE;
        ld_notification_ts REPORT.TERMINAL_ALERT.NOTIFICATION_DATE%TYPE;
    BEGIN
        SELECT TE.TERMINAL_ID, D.DEVICE_SERIAL_CD, A.STATUS, A.ALERT_ID, E.EVENT_START_TS, 
               CASE WHEN A.ALERT_SOURCE_ID IN(2,3) AND REGEXP_LIKE(ED_D.EVENT_DETAIL_VALUE, '^\d\d/\d\d/\d{4} \d\d:\d\d:\d\d - ') THEN SUBSTR(ED_D.EVENT_DETAIL_VALUE, 23, 4000) ELSE ED_D.EVENT_DETAIL_VALUE END, 
               H.HOST_PORT_NUM, E.EVENT_UPLOAD_COMPLETE_TS
          INTO pn_terminal_id, pv_device_serial_cd, lc_alert_status, ln_alert_id, ld_alert_ts, lv_alert_description, ln_component, ld_notification_ts
          FROM DEVICE.EVENT E
          JOIN DEVICE.HOST H ON E.HOST_ID = H.HOST_ID
          JOIN DEVICE.DEVICE D ON H.DEVICE_ID = D.DEVICE_ID
          LEFT OUTER JOIN REPORT.ALERT A ON E.EVENT_TYPE_ID = A.EVENT_TYPE_ID AND A.ALERT_SOURCE_ID IN(2,3)
          LEFT OUTER JOIN (REPORT.TERMINAL_EPORT TE
          JOIN REPORT.EPORT EP ON TE.EPORT_ID = EP.EPORT_ID) 
            ON D.DEVICE_SERIAL_CD = EP.EPORT_SERIAL_NUM 
           AND E.EVENT_START_TS >= NVL(TE.START_DATE, MIN_DATE)
           AND E.EVENT_START_TS < NVL(TE.END_DATE, MAX_DATE)
          LEFT OUTER JOIN DEVICE.EVENT_DETAIL ED_D ON E.EVENT_ID = ED_D.EVENT_ID AND ED_D.EVENT_DETAIL_TYPE_ID = 3
         WHERE E.EVENT_ID = pn_event_id;
        IF pn_terminal_id IS NOT NULL AND lc_alert_status = 'A' THEN
           IF ld_alert_ts >= SYSDATE - 20 THEN
	           SELECT CASE WHEN COUNT(*) > 0 THEN 'Y' ELSE 'N' END
	             INTO pc_dont_send_flag
	             FROM REPORT.TERMINAL_ALERT
	            WHERE ALERT_ID = ln_alert_id
	              AND TERMINAL_ID = pn_terminal_id
	              AND ALERT_DATE > ld_alert_ts - 0.5
	              AND DETAILS = lv_alert_description
	              AND RESPONSE_SENT IN('P', 'Y');
	        ELSE
	           pc_dont_send_flag := 'Y'; /*Don't send old alert*/
	        END IF;
            SELECT REPORT.TERMINAL_ALERT_SEQ.NEXTVAL 
              INTO pn_terminal_alert_id
              FROM DUAL;
            INSERT INTO REPORT.TERMINAL_ALERT(	
                TERMINAL_ALERT_ID, 
                ALERT_ID, 
                TERMINAL_ID, 
                ALERT_DATE, 
                DETAILS, 
                RESPONSE_SENT,
                REFERENCE_ID, 
                COMPONENT_ID, 
                NOTIFICATION_DATE)
            VALUES(
                pn_terminal_alert_id,
                ln_alert_id,
                pn_terminal_id,
                ld_alert_ts,
                lv_alert_description,
                CASE WHEN pc_dont_send_flag = 'Y' THEN 'I' ELSE 'P' END,
                pn_event_id,
                ln_component,
                ld_notification_ts
            );
       ELSE
           pc_dont_send_flag := 'Y'; 
       END IF;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            NULL;
    END;
    
    -- R44 and below
    PROCEDURE ADD_EVENT_ALERT(
        pn_event_id IN REPORT.TERMINAL_ALERT.REFERENCE_ID%TYPE,
        pn_terminal_alert_id OUT REPORT.TERMINAL_ALERT.TERMINAL_ALERT_ID%TYPE,
        pn_terminal_id OUT REPORT.TERMINAL.TERMINAL_ID%TYPE,
        pv_device_serial_cd OUT REPORT.EPORT.EPORT_SERIAL_NUM%TYPE)
    IS
        lc_dont_send_flag VARCHAR2(4000);
    BEGIN
        ADD_EVENT_ALERT(
            pn_event_id,
            pn_terminal_alert_id,
            pn_terminal_id,
            pv_device_serial_cd,
            lc_dont_send_flag);
    END;
    
	-- END Device alerting additions
END;
/

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/PSS/UPDATE_MERCHANT_GROUP_CD.prc?revision=HEAD
CREATE OR REPLACE PROCEDURE PSS.UPDATE_MERCHANT_GROUP_CD(
    pn_device_id PSS.POS.DEVICE_ID%TYPE,
    pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
    pv_old_merchant_group_cd PSS.MERCHANT.MERCHANT_GROUP_CD%TYPE,
    pv_new_merchant_group_cd PSS.MERCHANT.MERCHANT_GROUP_CD%TYPE,
    pc_count OUT PLS_INTEGER)
    AS
    CURSOR l_cur IS
        SELECT DISTINCT D.DEVICE_NAME, P.POS_ID, T0.TERMINAL_ID OLD_TERMINAL_ID, T.TERMINAL_ID, T.TERMINAL_CD, M.MERCHANT_ID, M0.MERCHANT_ID OLD_MERCHANT_ID, M.MERCHANT_GROUP_CD, A.AUTHORITY_ID, A.AUTHORITY_NAME, A.MIN_POS_PTA_NUM, A.MAX_POS_PTA_NUM, A.POS_PTA_CD_EXPRESSION
          FROM PSS.POS_PTA PP
          JOIN PSS.POS P ON PP.POS_ID = P.POS_ID
          JOIN DEVICE.DEVICE D ON P.DEVICE_ID = D.DEVICE_ID
          JOIN PSS.PAYMENT_SUBTYPE PS ON PP.PAYMENT_SUBTYPE_ID = PS.PAYMENT_SUBTYPE_ID
          JOIN AUTHORITY.HANDLER H ON PS.PAYMENT_SUBTYPE_CLASS = H.HANDLER_CLASS
          JOIN AUTHORITY.AUTHORITY_TYPE AUT ON H.HANDLER_ID = AUT.HANDLER_ID 
          JOIN AUTHORITY.AUTHORITY A ON A.AUTHORITY_TYPE_ID = AUT.AUTHORITY_TYPE_ID
          JOIN PSS.MERCHANT M ON M.AUTHORITY_ID = A.AUTHORITY_ID
          JOIN PSS.TERMINAL T ON T.MERCHANT_ID = M.MERCHANT_ID
          JOIN PSS.MERCHANT M0 ON M0.AUTHORITY_ID = A.AUTHORITY_ID
          JOIN PSS.TERMINAL T0 ON T0.MERCHANT_ID = M0.MERCHANT_ID AND PP.PAYMENT_SUBTYPE_KEY_ID = T0.TERMINAL_ID
          LEFT OUTER JOIN PSS.MERCHANT_POS_PTA_NUM MPP ON M.MERCHANT_ID = MPP.MERCHANT_ID AND MPP.DEVICE_NAME = D.DEVICE_NAME AND MPP.POS_PTA_NUM BETWEEN A.MIN_POS_PTA_NUM AND A.MAX_POS_PTA_NUM
         WHERE PS.PAYMENT_SUBTYPE_CLASS NOT IN('Authority::NOP', 'Cash', 'Aramark', 'BlackBoard')
           AND PS.PAYMENT_SUBTYPE_CLASS NOT LIKE 'Internal%'
           AND PP.POS_PTA_ACTIVATION_TS <= SYSDATE
           AND NVL(PP.POS_PTA_DEACTIVATION_TS, MAX_DATE) > SYSDATE
           AND PS.PAYMENT_SUBTYPE_CLASS = pv_payment_subtype_class
           AND P.DEVICE_ID = pn_device_id
           AND M0.MERCHANT_GROUP_CD = pv_old_merchant_group_cd          
           AND M.MERCHANT_GROUP_CD = pv_new_merchant_group_cd
           AND T0.TERMINAL_ID != T.TERMINAL_ID;
    ln_alternate_merchant_id PSS.MERCHANT.MERCHANT_ID%TYPE;
    ln_alternate_terminal_id PSS.TERMINAL.TERMINAL_CD%TYPE;
    ln_pos_pta_num PSS.MERCHANT_POS_PTA_NUM.POS_PTA_NUM%TYPE;
    ld_cutover_ts DATE;
    ln_upd_cnt PLS_INTEGER;
    ln_ins_cnt PLS_INTEGER;
    lv_device_name DEVICE.DEVICE_NAME%TYPE;
    lc_needs_alt CHAR(1);
BEGIN
    pc_count := 0;
    FOR l_rec IN l_cur LOOP
        ln_alternate_terminal_id := l_rec.TERMINAL_ID;
        ln_alternate_merchant_id := l_rec.MERCHANT_ID;
        IF l_rec.POS_PTA_CD_EXPRESSION IS NOT NULL THEN
            SELECT CASE WHEN MAX(CASE WHEN MPP.DEVICE_NAME = lv_device_name THEN 1 ELSE 0 END) = 1 THEN '-' 
                        WHEN COUNT(DISTINCT MPP.POS_PTA_NUM) <= l_rec.MAX_POS_PTA_NUM - l_rec.MIN_POS_PTA_NUM THEN 'N'
                        ELSE 'Y' END
              INTO lc_needs_alt
              FROM PSS.MERCHANT_POS_PTA_NUM MPP 
             WHERE MPP.MERCHANT_ID = l_rec.MERCHANT_ID
               AND MPP.POS_PTA_NUM BETWEEN l_rec.MIN_POS_PTA_NUM AND l_rec.MAX_POS_PTA_NUM;
            BEGIN
                IF lc_needs_alt = 'Y' THEN
                    PSS.PKG_POS_PTA.FIND_ALT_TERMINAL(l_rec.AUTHORITY_ID, l_rec.AUTHORITY_NAME, l_rec.MERCHANT_GROUP_CD, l_rec.TERMINAL_CD, l_rec.DEVICE_NAME, ln_alternate_merchant_id, ln_alternate_terminal_id);
                ELSIF lc_needs_alt = 'N' THEN
                    ln_pos_pta_num := PSS.PKG_POS_PTA.CREATE_POS_PTA_NUM(l_rec.MERCHANT_ID, l_rec.MIN_POS_PTA_NUM, l_rec.MAX_POS_PTA_NUM, l_rec.DEVICE_NAME);
                END IF;
            EXCEPTION
                WHEN PSS.PKG_POS_PTA.MERCHANT_FULL THEN
                    PSS.PKG_POS_PTA.FIND_ALT_TERMINAL(l_rec.AUTHORITY_ID, l_rec.AUTHORITY_NAME, l_rec.MERCHANT_GROUP_CD, l_rec.TERMINAL_CD, l_rec.DEVICE_NAME, ln_alternate_merchant_id, ln_alternate_terminal_id);
            END;
        END IF;
        IF ln_alternate_terminal_id IS NOT NULL THEN
            ld_cutover_ts := SYSDATE;
            INSERT INTO PSS.POS_PTA(POS_ID, PAYMENT_SUBTYPE_ID, POS_PTA_ENCRYPT_KEY, PAYMENT_SUBTYPE_KEY_ID, POS_PTA_REGEX, POS_PTA_REGEX_BREF, POS_PTA_ACTIVATION_TS, POS_PTA_DEACTIVATION_TS, POS_PTA_DEVICE_SERIAL_CD, POS_PTA_PIN_REQ_YN_FLAG, POS_PTA_ENCRYPT_KEY2, AUTHORITY_PAYMENT_MASK_ID, POS_PTA_PRIORITY, TERMINAL_ID, MERCHANT_BANK_ACCT_ID, CURRENCY_CD, POS_PTA_PASSTHRU_ALLOW_YN_FLAG, POS_PTA_PREF_AUTH_AMT, POS_PTA_PREF_AUTH_AMT_MAX, POS_PTA_DISABLE_DEBIT_DENIAL, NO_CONVENIENCE_FEE)
               SELECT POS_ID, PAYMENT_SUBTYPE_ID, POS_PTA_ENCRYPT_KEY, ln_alternate_terminal_id, POS_PTA_REGEX, POS_PTA_REGEX_BREF, ld_cutover_ts, POS_PTA_DEACTIVATION_TS, POS_PTA_DEVICE_SERIAL_CD, POS_PTA_PIN_REQ_YN_FLAG, POS_PTA_ENCRYPT_KEY2, AUTHORITY_PAYMENT_MASK_ID, POS_PTA_PRIORITY, TERMINAL_ID, MERCHANT_BANK_ACCT_ID, CURRENCY_CD, POS_PTA_PASSTHRU_ALLOW_YN_FLAG, POS_PTA_PREF_AUTH_AMT, POS_PTA_PREF_AUTH_AMT_MAX, POS_PTA_DISABLE_DEBIT_DENIAL, NO_CONVENIENCE_FEE
                 FROM PSS.POS_PTA
                WHERE POS_ID = l_rec.POS_ID
                  AND PAYMENT_SUBTYPE_KEY_ID = l_rec.OLD_TERMINAL_ID
                  AND POS_PTA_ACTIVATION_TS <= ld_cutover_ts
                  AND NVL(POS_PTA_DEACTIVATION_TS, MAX_DATE) > ld_cutover_ts
                  AND PAYMENT_SUBTYPE_ID IN(
                      SELECT PAYMENT_SUBTYPE_ID 
                        FROM PSS.PAYMENT_SUBTYPE
                       WHERE PAYMENT_SUBTYPE_CLASS = pv_payment_subtype_class);
            ln_ins_cnt := SQL%ROWCOUNT;
            UPDATE PSS.POS_PTA 
               SET POS_PTA_DEACTIVATION_TS = ld_cutover_ts
             WHERE POS_ID = l_rec.POS_ID
               AND PAYMENT_SUBTYPE_KEY_ID = l_rec.OLD_TERMINAL_ID
               AND POS_PTA_ACTIVATION_TS <= ld_cutover_ts
               AND NVL(POS_PTA_DEACTIVATION_TS, MAX_DATE) > ld_cutover_ts
               AND PAYMENT_SUBTYPE_ID IN(
                    SELECT PAYMENT_SUBTYPE_ID 
                      FROM PSS.PAYMENT_SUBTYPE
                     WHERE PAYMENT_SUBTYPE_CLASS = pv_payment_subtype_class);
            ln_upd_cnt := SQL%ROWCOUNT;
            pc_count := pc_count + ln_ins_cnt;
        DELETE
          FROM PSS.MERCHANT_POS_PTA_NUM MPP
         WHERE MPP.DEVICE_NAME = l_rec.DEVICE_NAME
           AND MPP.MERCHANT_ID = l_rec.OLD_MERCHANT_ID
           AND NOT EXISTS(SELECT 1
            FROM PSS.MERCHANT M
            JOIN AUTHORITY.AUTHORITY A ON A.AUTHORITY_ID = M.AUTHORITY_ID
            JOIN AUTHORITY.AUTHORITY_TYPE AUT ON A.AUTHORITY_TYPE_ID = AUT.AUTHORITY_TYPE_ID
            JOIN AUTHORITY.HANDLER H ON H.HANDLER_ID = AUT.HANDLER_ID 
            JOIN PSS.PAYMENT_SUBTYPE PS ON PS.PAYMENT_SUBTYPE_CLASS = H.HANDLER_CLASS
            JOIN DEVICE.DEVICE D ON D.DEVICE_ACTIVE_YN_FLAG = 'Y'
            JOIN PSS.POS P ON D.DEVICE_ID = P.DEVICE_ID 
            JOIN PSS.POS_PTA PP ON PP.POS_ID = P.POS_ID AND PS.PAYMENT_SUBTYPE_ID = PP.PAYMENT_SUBTYPE_ID
            JOIN PSS.TERMINAL T ON PP.PAYMENT_SUBTYPE_KEY_ID = T.TERMINAL_ID AND M.MERCHANT_ID = T.MERCHANT_ID 
           WHERE MPP.MERCHANT_ID = M.MERCHANT_ID
             AND MPP.DEVICE_NAME = D.DEVICE_NAME
             AND PS.PAYMENT_SUBTYPE_CLASS NOT IN('Authority::NOP', 'Cash', 'Aramark', 'BlackBoard')
             AND PS.PAYMENT_SUBTYPE_CLASS NOT LIKE 'Internal%'
             AND A.POS_PTA_CD_EXPRESSION IS NOT NULL
             AND (PP.POS_PTA_DEACTIVATION_TS IS NULL OR PP.POS_PTA_DEACTIVATION_TS > SYSDATE));
        END IF;
    END LOOP;
END;
/

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/PSS/VW_TRAN_COUNT_BY_DAY.vws?revision=1.6
CREATE OR REPLACE FORCE VIEW PSS.VW_TRAN_COUNT_BY_DAY (TRAN_UPLOAD_TS, UPLOAD_DAY, SOURCE_SYSTEM_CD, DEVICE_TYPE_ID, TRAN_TYPE, TRAN_ID, TRAN_GLOBAL_TRANS_CD, TRAN_LINE_ITEM_AMOUNT, TRAN_LINE_ITEM_QUANTITY) AS 
SELECT X.TRAN_UPLOAD_TS,
       TRUNC(X.TRAN_UPLOAD_TS) UPLOAD_DAY,
       'PSS' SOURCE_SYSTEM_CD, 
       D.DEVICE_TYPE_ID, 
       TT.TRANS_TYPE_NAME TRAN_TYPE,
       X.TRAN_ID, 
       X.TRAN_GLOBAL_TRANS_CD,
       TLI.TRAN_LINE_ITEM_AMOUNT, 
       TLI.TRAN_LINE_ITEM_QUANTITY
  FROM PSS.TRAN X
  JOIN PSS.POS_PTA PTA ON X.POS_PTA_ID = PTA.POS_PTA_ID 
  JOIN PSS.POS POS ON PTA.POS_ID = POS.POS_ID 
  JOIN DEVICE.DEVICE D ON POS.DEVICE_ID = D.DEVICE_ID 
  JOIN PSS.PAYMENT_SUBTYPE PST ON PTA.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
  JOIN PSS.TRAN_LINE_ITEM TLI ON X.TRAN_ID = TLI.TRAN_ID
  LEFT OUTER JOIN PSS.AUTH SA ON X.TRAN_ID = SA.TRAN_ID AND SA.AUTH_TYPE_CD IN('U','S','O') AND SA.OVERRIDE_TRANS_TYPE_ID IS NOT NULL
  LEFT OUTER JOIN PSS.AUTH AA ON X.TRAN_ID = AA.TRAN_ID AND AA.AUTH_TYPE_CD IN('L','N') AND AA.OVERRIDE_TRANS_TYPE_ID IS NOT NULL
  LEFT OUTER JOIN PSS.REFUND R ON X.TRAN_ID = R.TRAN_ID AND R.OVERRIDE_TRANS_TYPE_ID IS NOT NULL
  JOIN REPORT.TRANS_TYPE TT ON COALESCE(SA.OVERRIDE_TRANS_TYPE_ID, AA.OVERRIDE_TRANS_TYPE_ID, R.OVERRIDE_TRANS_TYPE_ID, PST.TRANS_TYPE_ID) = TT.TRANS_TYPE_ID
 WHERE TLI.TRAN_LINE_ITEM_BATCH_TYPE_CD = 'A'
   AND X.TRAN_STATE_CD IN('8', 'A', 'B', 'C', 'D', 'E', 'I', 'J', 'N', 'Q', 'R', 'S', 'T')
   AND NOT(TLI.TRAN_LINE_ITEM_AMOUNT = 0 /*See Bugzilla #497*/
              AND TLI.TRAN_LINE_ITEM_POSITION_CD = '00'
              AND TLI.TRAN_LINE_ITEM_TYPE_ID = 200
              AND D.DEVICE_TYPE_ID = 6)
/

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/PSS/PKG_SETTLEMENT.pbk?revision=1.89
CREATE OR REPLACE PACKAGE BODY PSS.PKG_SETTLEMENT AS
    FUNCTION AFTER_SETTLE_TRAN_STATE_CD(
        pc_auth_type_cd PSS.AUTH.AUTH_TYPE_CD%TYPE,
        pc_refund_type_cd PSS.REFUND.REFUND_TYPE_CD%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pc_device_result_type_cd PSS.TRAN.TRAN_DEVICE_RESULT_TYPE_CD%TYPE)
    RETURN PSS.TRAN.TRAN_STATE_CD%TYPE
    PARALLEL_ENABLE DETERMINISTIC
    IS       
    BEGIN
        IF pc_auth_result_cd = 'Y' THEN
            IF pc_auth_type_cd IS NOT NULL THEN
                IF pc_auth_type_cd IN('C', 'V', 'E', 'I') THEN
                    IF pc_device_result_type_cd IS NULL THEN
                        RETURN 'V'; -- processed reversal
                    ELSE
                        RETURN 'C'; -- Cancelled 
                    END IF;
                ELSE
                    RETURN 'D'; -- Complete
                END IF;                    
            ELSIF pc_refund_type_cd IS NOT NULL THEN
                IF pc_refund_type_cd IN('C') THEN
                    RETURN 'C'; -- Cancelled 
                ELSE
                    RETURN 'D'; -- Complete
                END IF;
            ELSE
                RAISE_APPLICATION_ERROR(-20560, 'Both auth type cd and refund type cd are null');
            END IF;
        ELSIF pc_auth_result_cd = 'P' THEN
            RETURN 'Q'; -- settlement processed
        ELSIF pc_auth_result_cd = 'N' THEN
            RETURN 'N'; -- PROCESSED_SERVER_SETTLEMENT_INCOMPLETE
        ELSIF pc_auth_result_cd = 'F' THEN
            RETURN 'R'; -- PROCESSED_SERVER_SETTLEMENT_ERROR
        ELSIF pc_auth_result_cd IN('O', 'R') THEN
            IF pc_auth_type_cd IS NOT NULL THEN
                IF pc_auth_type_cd IN('C', 'V', 'E', 'I') THEN
                    IF pc_device_result_type_cd IS NULL THEN
                        RETURN 'V'; -- processed reversal
                    ELSE
                        RETURN 'C'; -- Cancelled 
                    END IF;
                ELSE
                    RETURN 'E'; -- Complete - Error
                END IF;                    
            ELSIF pc_refund_type_cd IS NOT NULL THEN
                IF pc_refund_type_cd IN('C') THEN
                    RETURN 'C'; -- Cancelled 
                ELSE
                    RETURN 'E'; -- Complete - Error
                END IF;
            ELSE
                RAISE_APPLICATION_ERROR(-20560, 'Both auth type cd and refund type cd are null');
            END IF;
        END IF;
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    FUNCTION CREATE_TERMINAL_BATCH(
        pn_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE)
    RETURN PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE
    IS
        ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
    BEGIN
        SELECT PSS.SEQ_TERMINAL_BATCH_ID.NEXTVAL
          INTO ln_terminal_batch_id
          FROM DUAL;
        INSERT INTO PSS.TERMINAL_BATCH (
                TERMINAL_BATCH_ID,
                TERMINAL_ID,
                TERMINAL_BATCH_NUM,
                TERMINAL_BATCH_OPEN_TS,
                TERMINAL_BATCH_CYCLE_NUM,
                TERMINAL_CAPTURE_FLAG) 
         SELECT ln_terminal_batch_id, 
                pn_terminal_id, 
                T.TERMINAL_NEXT_BATCH_NUM,
                SYSDATE,
                T.TERMINAL_BATCH_CYCLE_NUM,
                A.TERMINAL_CAPTURE_FLAG
           FROM PSS.TERMINAL T
           JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
           JOIN AUTHORITY.AUTHORITY A ON M.AUTHORITY_ID = A.AUTHORITY_ID
          WHERE TERMINAL_ID = pn_terminal_id;
          
         UPDATE PSS.TERMINAL
            SET TERMINAL_NEXT_BATCH_NUM = CASE 
                    WHEN TERMINAL_NEXT_BATCH_NUM >= TERMINAL_MAX_BATCH_NUM THEN NVL(TERMINAL_MIN_BATCH_NUM, 1) /* reset batch num */ 
                    ELSE TERMINAL_NEXT_BATCH_NUM + 1 /* increment batch num */
                END,
                TERMINAL_BATCH_CYCLE_NUM = CASE 
                    WHEN TERMINAL_NEXT_BATCH_NUM >= TERMINAL_MAX_BATCH_NUM THEN TERMINAL_BATCH_CYCLE_NUM + 1 /* next cycle */ 
                    ELSE TERMINAL_BATCH_CYCLE_NUM /* same cycle */
                END
            WHERE TERMINAL_ID = pn_terminal_id;
        RETURN ln_terminal_batch_id;
    END;
            
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    FUNCTION GET_AVAILABLE_TERMINAL_BATCH(
        pn_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE,
        pn_allowed_trans OUT PLS_INTEGER,
        pb_create_if_needed BOOLEAN)
    RETURN PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE
    IS
        ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
        lc_is_closed CHAR(1);
        ln_max_tran PSS.TERMINAL.TERMINAL_BATCH_MAX_TRAN%TYPE; 
        ln_terminal_state_id PSS.TERMINAL.TERMINAL_STATE_ID%TYPE;
        lc_terminal_capture_flag PSS.TERMINAL_BATCH.TERMINAL_CAPTURE_FLAG%TYPE;
        ln_attempts PLS_INTEGER;
    BEGIN
        -- get last terminal batch record and verify that it is open       
        SELECT /*+ FIRST_ROWS */ MAX(TERMINAL_BATCH_ID), MAX(DECODE(TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y')), MAX(TERMINAL_BATCH_MAX_TRAN), MAX(TERMINAL_STATE_ID), MAX(NVL(TERMINAL_CAPTURE_FLAG, 'N'))
          INTO ln_terminal_batch_id, lc_is_closed, ln_max_tran, ln_terminal_state_id, lc_terminal_capture_flag
          FROM (SELECT TB.TERMINAL_BATCH_ID,
                       TB.TERMINAL_BATCH_NUM, 
                       TB.TERMINAL_BATCH_CYCLE_NUM, 
                       TB.TERMINAL_BATCH_CLOSE_TS,
                       COALESCE(T.TERMINAL_BATCH_MAX_TRAN, AU.AUTHORITY_BATCH_MAX_TRAN, 999) TERMINAL_BATCH_MAX_TRAN,
                       T.TERMINAL_STATE_ID,
                       TB.TERMINAL_CAPTURE_FLAG
                  FROM PSS.TERMINAL T
                  JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
                  JOIN AUTHORITY.AUTHORITY AU ON M.AUTHORITY_ID = AU.AUTHORITY_ID 
                  LEFT OUTER JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID                   
                 WHERE T.TERMINAL_ID = pn_terminal_id
                 ORDER BY TB.TERMINAL_BATCH_CYCLE_NUM DESC,
                          TB.TERMINAL_BATCH_NUM DESC)
         WHERE ROWNUM = 1;
        pn_allowed_trans := ln_max_tran;
        IF ln_terminal_state_id NOT IN(3) THEN
            RAISE_APPLICATION_ERROR(-20559, 'Terminal ' || pn_terminal_id || ' is not locked and a new terminal batch can not be created for it');
        ELSIF ln_terminal_batch_id IS NULL THEN
            IF pb_create_if_needed THEN
                ln_terminal_batch_id := CREATE_TERMINAL_BATCH(pn_terminal_id);
            END IF;
        ELSIF lc_is_closed  = 'Y' THEN
            IF pb_create_if_needed THEN
                ln_terminal_batch_id := CREATE_TERMINAL_BATCH(pn_terminal_id);
            END IF;
        ELSE
            -- do we need a new batch?
            SELECT ln_max_tran - COUNT(DISTINCT TRAN_ID)
              INTO pn_allowed_trans
              FROM (SELECT A.TRAN_ID
                      FROM PSS.AUTH A
                     WHERE A.TERMINAL_BATCH_ID = ln_terminal_batch_id
                    UNION ALL
                    SELECT R.TRAN_ID
                      FROM PSS.REFUND R
                     WHERE R.TERMINAL_BATCH_ID = ln_terminal_batch_id);
            IF lc_terminal_capture_flag = 'Y' THEN              
                IF pn_allowed_trans > 0 THEN
                    SELECT COUNT(*)
                      INTO ln_attempts
                      FROM PSS.SETTLEMENT_BATCH
                     WHERE TERMINAL_BATCH_ID = ln_terminal_batch_id;
                END IF;
                IF ln_attempts > 0 OR pn_allowed_trans <= 0 THEN
                    -- Create new terminal batch
                    IF pb_create_if_needed THEN
                        ln_terminal_batch_id := CREATE_TERMINAL_BATCH(pn_terminal_id);
                    END IF;
                    pn_allowed_trans := ln_max_tran;
                END IF;
            END IF;
        END IF;
        
        RETURN ln_terminal_batch_id;
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE GET_PENDING_SETTLEMENTS(
        pn_payment_subtype_key_id PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE, 
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pc_ignore_minimums_flag CHAR,
        pn_max_settlements PLS_INTEGER, 
        pn_pending_terminal_batch_ids OUT NUMBER_TABLE)
    IS
    BEGIN
         -- The following should never occur:
        /*
        -- Find retry settlements
        SELECT TERMINAL_BATCH_ID
          BULK COLLECT INTO pn_pending_terminal_batch_ids
          FROM (SELECT TB.TERMINAL_BATCH_ID
                  FROM PSS.SETTLEMENT_BATCH SB 
                  JOIN PSS.TERMINAL_BATCH TB ON TB.TERMINAL_BATCH_ID = SB.TERMINAL_BATCH_ID
                 WHERE TB.TERMINAL_ID = pn_payment_subtype_key_id 
                   AND SB.SETTLEMENT_BATCH_STATE_ID = 4 
                 ORDER BY TB.TERMINAL_BATCH_OPEN_TS ASC)
         WHERE ROWNUM <= pn_max_settlements;
     
        IF pn_pending_terminal_batch_ids.COUNT > 0 THEN
            RETURN;
        END IF;
        */
        -- Find new open batches
        SELECT TERMINAL_BATCH_ID
          BULK COLLECT INTO pn_pending_terminal_batch_ids
          FROM (
            SELECT C.TERMINAL_BATCH_ID
              FROM PSS.MERCHANT M
              JOIN AUTHORITY.AUTHORITY AU ON M.AUTHORITY_ID = AU.AUTHORITY_ID
              JOIN (SELECT TERMINAL_ID, TERMINAL_BATCH_ID, MERCHANT_ID, TERMINAL_BATCH_OPEN_TS, 
                           TERMINAL_BATCH_MIN_TRAN, TERMINAL_BATCH_MAX_TRAN, TERMINAL_MIN_BATCH_CLOSE_HR, TERMINAL_MAX_BATCH_CLOSE_HR, 
                           SUM(CASE WHEN TRAN_STATE_CD IN('T') THEN 1 ELSE 0 END) NUM_TRAN
                      FROM (SELECT T.TERMINAL_ID, 
                                   TB.TERMINAL_BATCH_ID,
                                   T.MERCHANT_ID,
                                   X.TRAN_ID,
                                   X.TRAN_STATE_CD,
                                   T.TERMINAL_BATCH_MIN_TRAN,
                                   T.TERMINAL_BATCH_MAX_TRAN,
                                   T.TERMINAL_MIN_BATCH_CLOSE_HR,
                                   T.TERMINAL_MAX_BATCH_CLOSE_HR,
                                   TB.TERMINAL_BATCH_OPEN_TS
                              FROM PSS.TERMINAL T
                              JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
                              JOIN PSS.AUTH A ON TB.TERMINAL_BATCH_ID = A.TERMINAL_BATCH_ID
                              JOIN PSS.TRAN X ON A.TRAN_ID = X.TRAN_ID 
                             WHERE TB.TERMINAL_BATCH_OPEN_TS IS NOT NULL
                               AND DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y') = 'N'
                               AND T.TERMINAL_ID = pn_payment_subtype_key_id
                             UNION 
                            SELECT T.TERMINAL_ID, 
                                   TB.TERMINAL_BATCH_ID,
                                   T.MERCHANT_ID,
                                   X.TRAN_ID,
                                   X.TRAN_STATE_CD,
                                   T.TERMINAL_BATCH_MIN_TRAN,
                                   T.TERMINAL_BATCH_MAX_TRAN,
                                   T.TERMINAL_MIN_BATCH_CLOSE_HR,
                                   T.TERMINAL_MAX_BATCH_CLOSE_HR,
                                   TB.TERMINAL_BATCH_OPEN_TS
                              FROM PSS.TERMINAL T
                              JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
                              JOIN PSS.REFUND R ON TB.TERMINAL_BATCH_ID = R.TERMINAL_BATCH_ID
                              JOIN PSS.TRAN X ON R.TRAN_ID = X.TRAN_ID 
                             WHERE TB.TERMINAL_BATCH_OPEN_TS IS NOT NULL
                               AND DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y') = 'N'
                               AND T.TERMINAL_ID = pn_payment_subtype_key_id) 
                     GROUP BY TERMINAL_ID, TERMINAL_BATCH_ID, MERCHANT_ID, TERMINAL_BATCH_OPEN_TS, TERMINAL_BATCH_MIN_TRAN, TERMINAL_BATCH_MAX_TRAN, TERMINAL_MIN_BATCH_CLOSE_HR, TERMINAL_MAX_BATCH_CLOSE_HR
                     HAVING COUNT(TRAN_STATE_CD) = SUM(CASE WHEN TRAN_STATE_CD IN('T', 'E') THEN 1 ELSE 0 END)) C  ON C.MERCHANT_ID = M.MERCHANT_ID 
             WHERE C.NUM_TRAN > 0
               AND (pc_ignore_minimums_flag = 'Y'
                OR C.NUM_TRAN >= COALESCE(C.TERMINAL_BATCH_MAX_TRAN, AU.AUTHORITY_BATCH_MAX_TRAN, 999) 
                OR SYSDATE >= C.TERMINAL_BATCH_OPEN_TS + COALESCE(C.TERMINAL_MAX_BATCH_CLOSE_HR/24, AU.AUTHORITY_MAX_BATCH_CLOSE_HR/24, 1)
                OR (SYSDATE >= C.TERMINAL_BATCH_OPEN_TS + COALESCE(C.TERMINAL_MIN_BATCH_CLOSE_HR/24, AU.AUTHORITY_MIN_BATCH_CLOSE_HR/24, 0)
                    AND C.NUM_TRAN >= COALESCE(C.TERMINAL_BATCH_MIN_TRAN, AU.AUTHORITY_BATCH_MIN_TRAN, 25)))
             ORDER BY c.TERMINAL_BATCH_OPEN_TS ASC)
         WHERE ROWNUM <= pn_max_settlements;
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE GET_RETRY_SETTLEMENTS(
        pn_payment_subtype_key_id PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE, 
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_settlement_retry_interval NUMBER,
        pn_max_settlements PLS_INTEGER, 
        pn_pending_terminal_batch_ids OUT NUMBER_TABLE)
    IS
    BEGIN
        -- Find retyable batches
        SELECT TERMINAL_BATCH_ID
          BULK COLLECT INTO pn_pending_terminal_batch_ids
          FROM (
            SELECT C.TERMINAL_BATCH_ID
              FROM (SELECT TERMINAL_ID, TERMINAL_BATCH_ID, TERMINAL_BATCH_OPEN_TS, 
                           SUM(CASE WHEN TRAN_STATE_CD IN('R', 'N') THEN 1 ELSE 0 END) NUM_TRAN
                      FROM (SELECT T.TERMINAL_ID, 
                                   TB.TERMINAL_BATCH_ID,
                                   X.TRAN_ID,
                                   X.TRAN_STATE_CD,
                                   TB.TERMINAL_BATCH_OPEN_TS
                              FROM PSS.TERMINAL T
                              JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
                              JOIN PSS.AUTH A ON TB.TERMINAL_BATCH_ID = A.TERMINAL_BATCH_ID
                              JOIN PSS.TRAN X ON A.TRAN_ID = X.TRAN_ID 
                             WHERE TB.TERMINAL_BATCH_OPEN_TS IS NOT NULL
                               AND DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y') = 'N'
                               AND T.TERMINAL_ID = pn_payment_subtype_key_id
                             UNION 
                            SELECT T.TERMINAL_ID, 
                                   TB.TERMINAL_BATCH_ID,
                                   X.TRAN_ID,
                                   X.TRAN_STATE_CD,
                                   TB.TERMINAL_BATCH_OPEN_TS
                              FROM PSS.TERMINAL T
                              JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
                              JOIN PSS.REFUND R ON TB.TERMINAL_BATCH_ID = R.TERMINAL_BATCH_ID
                              JOIN PSS.TRAN X ON R.TRAN_ID = X.TRAN_ID 
                             WHERE TB.TERMINAL_BATCH_OPEN_TS IS NOT NULL
                               AND DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y') = 'N'
                               AND T.TERMINAL_ID = pn_payment_subtype_key_id) 
                     GROUP BY TERMINAL_ID, TERMINAL_BATCH_ID, TERMINAL_BATCH_OPEN_TS
                     HAVING COUNT(TRAN_STATE_CD) = SUM(CASE WHEN TRAN_STATE_CD IN('R', 'N', 'E') THEN 1 ELSE 0 END)) C
             WHERE C.NUM_TRAN > 0
               AND SYSDATE >= pn_settlement_retry_interval + (
                        SELECT NVL(MAX(LA.SETTLEMENT_BATCH_START_TS), MIN_DATE)
                          FROM PSS.SETTLEMENT_BATCH LA
                          WHERE C.TERMINAL_BATCH_ID = LA.TERMINAL_BATCH_ID)
             ORDER BY c.TERMINAL_BATCH_OPEN_TS ASC)
         WHERE ROWNUM <= pn_max_settlements;
    END;

/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE FORCE_SETTLE_BATCH(
        pn_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE,
        pv_force_reason PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_RESP_DESC%TYPE)
    IS
        ln_settlement_batch_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE;
        ln_settlement_batch_state_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_STATE_ID%TYPE;
        ln_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE;
        ln_terminal_state_id PSS.TERMINAL.TERMINAL_STATE_ID%TYPE;
        ln_is_open PLS_INTEGER;
        lv_msg VARCHAR2(4000);
    BEGIN
        -- check terminal state
        SELECT T.TERMINAL_ID, T.TERMINAL_STATE_ID
          INTO ln_terminal_id, ln_terminal_state_id
          FROM PSS.TERMINAL T
          JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
         WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id;
        IF ln_terminal_state_id NOT IN(3,5) THEN
            SELECT 'Terminal ' || ln_terminal_id || ' is currently ' || DECODE(ln_terminal_state_id, 1, 'not locked', 2, 'disabled', 5, 'busy with retry', 'unavailable')
              INTO lv_msg
              FROM DUAL;
            RAISE_APPLICATION_ERROR(-20560, lv_msg);
        END IF;
        
        -- Check status of most recent settlement batch to ensure it is failue or decline
        SELECT MAX(SETTLEMENT_BATCH_ID), NVL(MAX(SETTLEMENT_BATCH_STATE_ID), 0)
          INTO ln_settlement_batch_id, ln_settlement_batch_state_id
          FROM (
            SELECT SETTLEMENT_BATCH_ID, SETTLEMENT_BATCH_STATE_ID
              FROM PSS.SETTLEMENT_BATCH
             WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id
             ORDER BY SETTLEMENT_BATCH_START_TS DESC, SETTLEMENT_BATCH_ID DESC)
         WHERE ROWNUM = 1;
        IF ln_settlement_batch_state_id NOT IN(2, 3) THEN
            SELECT 'Terminal Batch ' || pn_terminal_batch_id || DECODE(ln_settlement_batch_state_id, 0, ' has not yet been tried', 1, ' was successfully settled already', 4, ' is awaiting a retry', 7, 'was partially settled already', ' is not ready for forced settlement')
              INTO lv_msg
              FROM DUAL;
            RAISE_APPLICATION_ERROR(-20561, lv_msg);
        END IF;
        
        -- create new settlement batch
        SELECT PSS.SEQ_SETTLEMENT_BATCH_ID.NEXTVAL
          INTO ln_settlement_batch_id
          FROM DUAL;            
        
        INSERT INTO PSS.SETTLEMENT_BATCH(
            SETTLEMENT_BATCH_ID,
            SETTLEMENT_BATCH_STATE_ID,
            SETTLEMENT_BATCH_START_TS,
            TERMINAL_BATCH_ID,
            SETTLEMENT_BATCH_RESP_CD,
            SETTLEMENT_BATCH_RESP_DESC,
            SETTLEMENT_BATCH_REF_CD,
            SETTLEMENT_BATCH_END_TS
        ) VALUES (
            ln_settlement_batch_id,
            1,
            SYSDATE,
            pn_terminal_batch_id,
            0,
            pv_force_reason, 
            'FORCE_SETTLEMENT',
            SYSDATE);
        -- add all trans and update their tran state cd
        INSERT INTO PSS.TRAN_SETTLEMENT_BATCH(
            SETTLEMENT_BATCH_ID,
            AUTH_ID,
            TRAN_ID,
            TRAN_SETTLEMENT_B_AMT)
          SELECT DISTINCT 
                 ln_settlement_batch_id,
                 FIRST_VALUE(A.AUTH_ID) OVER (PARTITION BY T.TRAN_ID ORDER BY A.CREATED_TS DESC, A.AUTH_ID DESC) AUTH_ID, 
                 T.TRAN_ID, 
                 FIRST_VALUE(A.AUTH_AMT) OVER (PARTITION BY T.TRAN_ID ORDER BY A.CREATED_TS DESC, A.AUTH_ID DESC) AUTH_AMT
            FROM PSS.TRAN T
            JOIN PSS.AUTH A ON T.TRAN_ID = A.TRAN_ID
           WHERE A.TERMINAL_BATCH_ID = pn_terminal_batch_id
             AND A.AUTH_STATE_ID IN(2,6);
        INSERT INTO PSS.REFUND_SETTLEMENT_BATCH(
            SETTLEMENT_BATCH_ID,
            REFUND_ID,
            TRAN_ID,
            REFUND_SETTLEMENT_B_AMT)
          SELECT DISTINCT 
                 ln_settlement_batch_id,
                 FIRST_VALUE(R.REFUND_ID) OVER (PARTITION BY T.TRAN_ID ORDER BY R.CREATED_TS DESC, R.REFUND_ID DESC) AUTH_ID, 
                 T.TRAN_ID, 
                 FIRST_VALUE(R.REFUND_AMT) OVER (PARTITION BY T.TRAN_ID ORDER BY R.CREATED_TS DESC, R.REFUND_ID DESC) REFUND_AMT
            FROM PSS.TRAN T
            JOIN PSS.REFUND R ON T.TRAN_ID = R.TRAN_ID
           WHERE R.TERMINAL_BATCH_ID = pn_terminal_batch_id
             AND R.REFUND_STATE_ID IN(1);
        UPDATE PSS.TRAN 
           SET TRAN_STATE_CD = 'D'
         WHERE TRAN_STATE_CD IN('R', 'N', 'Q', 'T')
           AND TRAN_ID IN(
            SELECT A.TRAN_ID
              FROM PSS.AUTH A 
             WHERE A.TERMINAL_BATCH_ID = pn_terminal_batch_id
               AND A.AUTH_STATE_ID IN(2,6)
            UNION ALL
            SELECT R.TRAN_ID
              FROM PSS.REFUND R
             WHERE R.TERMINAL_BATCH_ID = pn_terminal_batch_id
               AND R.REFUND_STATE_ID IN(1));
                   
        -- update terminal batch
        UPDATE PSS.TERMINAL_BATCH
           SET TERMINAL_BATCH_CLOSE_TS = SYSDATE
         WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id;
    END;

/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE FORCE_TRAN(
        pn_payment_subtype_key_id PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE, 
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pv_force_reason PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_RESP_DESC%TYPE)
    IS
        lc_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
        ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
        ln_sale_auth_id PSS.AUTH.AUTH_ID%TYPE; 
        ln_force_auth_id PSS.AUTH.AUTH_ID%TYPE;
        ln_force_refund_id PSS.REFUND.REFUND_ID%TYPE;
        ln_allowed_trans PLS_INTEGER;
        ln_settlement_batch_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE;
        ln_auth_amt PSS.AUTH.AUTH_AMT%TYPE;
    BEGIN
        -- check terminal state
        SELECT T.TRAN_STATE_CD
          INTO lc_tran_state_cd
          FROM PSS.TRAN T
         WHERE T.TRAN_ID = pn_tran_id;
        IF lc_tran_state_cd NOT IN('I', 'J') THEN
            RAISE_APPLICATION_ERROR(-20562, 'Tran ' || pn_tran_id || ' is not in INCOMPLETE or INCOMPLETE_ERROR state and cannot be forced');
        END IF;
        
        UPDATE PSS.TRAN
           SET TRAN_STATE_CD = CASE WHEN pv_payment_subtype_class LIKE 'Authority::ISO8583%' THEN 'T' ELSE 'D' END
         WHERE TRAN_ID = pn_tran_id
           AND TRAN_STATE_CD = lc_tran_state_cd; -- make sure it didn't change
        
        IF SQL%ROWCOUNT < 1 THEN
            FORCE_TRAN(pn_payment_subtype_key_id, pv_payment_subtype_class, pn_tran_id, pv_force_reason);
        ELSE
            -- add to batch if necessary
            SELECT MAX(AUTH_ID), MAX(AUTH_AMT)
              INTO ln_sale_auth_id, ln_auth_amt
              FROM (
                SELECT AUTH_ID, AUTH_TS, AUTH_AMT
                  FROM PSS.AUTH
                 WHERE TRAN_ID = pn_tran_id
                   AND AUTH_TYPE_CD IN('U','S','O','A','D','C','E','V','I')
                 ORDER BY AUTH_TS DESC, AUTH_ID DESC)
             WHERE ROWNUM = 1;
            IF ln_sale_auth_id IS NULL THEN
                SELECT MAX(REFUND_ID), MAX(REFUND_AMT)
                  INTO ln_force_refund_id, ln_auth_amt
                  FROM (
                    SELECT CREATED_TS, REFUND_ID, REFUND_AMT
                      FROM PSS.REFUND
                     WHERE TRAN_ID = pn_tran_id
                     ORDER BY CREATED_TS DESC, REFUND_ID DESC)
                 WHERE ROWNUM = 1;
            END IF;             
            IF pv_payment_subtype_class LIKE 'Authority::ISO8583%' THEN
                IF ln_sale_auth_id IS NOT NULL THEN
                    SELECT MAX(TERMINAL_BATCH_ID)
                      INTO ln_terminal_batch_id
                      FROM PSS.AUTH
                     WHERE TRAN_ID = pn_tran_id;
                ELSIF ln_force_refund_id IS NOT NULL THEN
                    SELECT MAX(TERMINAL_BATCH_ID)
                      INTO ln_terminal_batch_id
                      FROM PSS.REFUND
                     WHERE TRAN_ID = pn_tran_id;
                END IF;
                IF ln_terminal_batch_id IS NULL THEN
                    ln_terminal_batch_id := GET_AVAILABLE_TERMINAL_BATCH(pn_payment_subtype_key_id, ln_allowed_trans, TRUE);
                    IF ln_allowed_trans <= 0 THEN
                        RAISE_APPLICATION_ERROR(-20556, 'Terminal Batch ' || ln_terminal_batch_id || ' is full for terminal ' || pn_payment_subtype_key_id || '; Can not add tran ' || pn_tran_id || ' to it');
                    END IF;
                END IF;
            ELSE
                -- create settlement batch record
                SELECT PSS.SEQ_SETTLEMENT_BATCH_ID.NEXTVAL
                  INTO ln_settlement_batch_id
                  FROM DUAL;
                INSERT INTO PSS.SETTLEMENT_BATCH(
                    SETTLEMENT_BATCH_ID,
                    SETTLEMENT_BATCH_STATE_ID,
                    SETTLEMENT_BATCH_START_TS,
                    SETTLEMENT_BATCH_END_TS,
                    SETTLEMENT_BATCH_RESP_CD,
                    SETTLEMENT_BATCH_RESP_DESC
                ) VALUES (
                    ln_settlement_batch_id,
                    1,
                    SYSDATE,
                    SYSDATE,
                    'MANUAL',
                    pv_force_reason);
            END IF;
            IF ln_sale_auth_id IS NOT NULL THEN  
                SELECT PSS.SEQ_AUTH_ID.NEXTVAL
                  INTO ln_force_auth_id
                  FROM DUAL;
                INSERT INTO PSS.AUTH (
                        AUTH_ID,
                        TRAN_ID,
                        AUTH_TYPE_CD,
                        AUTH_STATE_ID,
                        AUTH_PARSED_ACCT_DATA,
                        ACCT_ENTRY_METHOD_CD,
                        AUTH_AMT,
                        AUTH_TS,
                        TERMINAL_BATCH_ID,
                        TRACE_NUMBER,
                        AUTH_RESULT_CD,
                        AUTH_RESP_CD,
                        AUTH_RESP_DESC,
                        AUTH_AMT_APPROVED)
                 SELECT ln_force_auth_id,
                        pn_tran_id,
                        A.AUTH_TYPE_CD,
                        2,
                        a.AUTH_PARSED_ACCT_DATA,
                        a.ACCT_ENTRY_METHOD_CD,
                        a.AUTH_AMT,	    
                        SYSDATE,
                        ln_terminal_batch_id,
                        a.TRACE_NUMBER,
                        'Y',
                        'MANUAL',
                        pv_force_reason,
                        a.AUTH_AMT
                   FROM PSS.AUTH a
                  WHERE a.AUTH_ID = ln_sale_auth_id;
                IF ln_settlement_batch_id IS NOT NULL THEN
                    INSERT INTO PSS.TRAN_SETTLEMENT_BATCH(
                        SETTLEMENT_BATCH_ID,
                        AUTH_ID,
                        TRAN_ID,
                        TRAN_SETTLEMENT_B_AMT
                    ) VALUES (
                        ln_settlement_batch_id,
                        ln_force_auth_id,
                        pn_tran_id,
                        ln_auth_amt
                    );               
                END IF;
            ELSIF ln_force_refund_id IS NOT NULL THEN
                UPDATE PSS.REFUND
                   SET REFUND_STATE_ID = 1,
                       REFUND_RESP_CD = 'MANUAL',
                       REFUND_RESP_DESC = pv_force_reason,
                       REFUND_AUTHORITY_TS = SYSDATE,
                       TERMINAL_BATCH_ID = NVL(ln_terminal_batch_id, TERMINAL_BATCH_ID)
                 WHERE REFUND_ID = ln_force_refund_id;
                IF ln_settlement_batch_id IS NOT NULL THEN
                    INSERT INTO PSS.REFUND_SETTLEMENT_BATCH(
                        SETTLEMENT_BATCH_ID,
                        REFUND_ID,
                        TRAN_ID,
                        REFUND_SETTLEMENT_B_AMT
                    ) VALUES (
                        ln_settlement_batch_id,
                        ln_force_refund_id,
                        pn_tran_id,
                        ln_auth_amt
                    );               
                END IF;
            END IF;
        END IF;
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE ERROR_TRAN(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pv_force_reason PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_RESP_DESC%TYPE)
    IS
        lc_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
        lc_terminal_capture_flag PSS.TERMINAL_BATCH.TERMINAL_CAPTURE_FLAG%TYPE;
    BEGIN
        -- check terminal state
        SELECT TRAN_STATE_CD
          INTO lc_tran_state_cd
          FROM PSS.TRAN
         WHERE TRAN_ID = pn_tran_id;
        IF lc_tran_state_cd IN('T','N') THEN
            SELECT MIN(TB.TERMINAL_CAPTURE_FLAG)
              INTO lc_terminal_capture_flag
              FROM PSS.TERMINAL_BATCH TB
              JOIN (SELECT A.TERMINAL_BATCH_ID FROM PSS.AUTH A WHERE A.AUTH_TYPE_CD NOT IN('N', 'L') AND A.TRAN_ID = pn_tran_id
                    UNION ALL
                    SELECT R.TERMINAL_BATCH_ID FROM PSS.REFUND R WHERE R.TRAN_ID = pn_tran_id) X ON TB.TERMINAL_BATCH_ID = X.TERMINAL_BATCH_ID;
            IF lc_terminal_capture_flag != 'Y' THEN
                RAISE_APPLICATION_ERROR(-20562, 'Tran ' || pn_tran_id || ' is not in INCOMPLETE or INCOMPLETE_ERROR state and it is in a Host-Capture batch and cannot be errored');
            END IF;
        ELSIF lc_tran_state_cd NOT IN('I', 'J') THEN
            RAISE_APPLICATION_ERROR(-20562, 'Tran ' || pn_tran_id || ' is not in INCOMPLETE or INCOMPLETE_ERROR state and cannot be errored');
        END IF;
        
        UPDATE PSS.TRAN
           SET TRAN_STATE_CD = 'E'
         WHERE TRAN_ID = pn_tran_id
           AND TRAN_STATE_CD = lc_tran_state_cd; -- make sure it didn't change
           
        IF SQL%ROWCOUNT < 1 THEN
            ERROR_TRAN(pn_tran_id, pv_force_reason);
        END IF;
    END;
         
    PROCEDURE MARK_ADMIN_CMD_EXECUTED(
        pn_admin_cmd_id PSS.ADMIN_CMD.ADMIN_CMD_ID%TYPE)
    IS
    BEGIN
        UPDATE PSS.ADMIN_CMD
           SET ADMIN_CMD_STATE_ID = 2,
               CLOSED_UTC_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP)
         WHERE ADMIN_CMD_ID = pn_admin_cmd_id;
    END;

    PROCEDURE MARK_ADMIN_CMD_ERRORED(
        pn_admin_cmd_id PSS.ADMIN_CMD.ADMIN_CMD_ID%TYPE,
        pv_error_msg PSS.ADMIN_CMD.ERROR_MSG%TYPE)
    IS
    BEGIN
        UPDATE PSS.ADMIN_CMD
           SET ADMIN_CMD_STATE_ID = 4,
               CLOSED_UTC_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP),
               ERROR_MSG = pv_error_msg
         WHERE ADMIN_CMD_ID = pn_admin_cmd_id;
    END;
    
    PROCEDURE GET_ACTIONS_FROM_ADMIN_CMDS(
        pn_payment_subtype_key_id PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE, 
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_max_settlements PLS_INTEGER, 
        pn_max_trans PLS_INTEGER,
        pc_allow_add_to_batch CHAR,
        pc_retry_only CHAR,
        pc_settle_processing_enabled CHAR,
        pc_tran_processing_enabled CHAR,
        pn_pending_terminal_batch_ids OUT NUMBER_TABLE,
        pn_pending_tran_ids OUT NUMBER_TABLE)
    IS
        CURSOR l_cur IS
            SELECT ADMIN_CMD_ID, ADMIN_CMD_TYPE_ID
              FROM PSS.ADMIN_CMD
             WHERE PAYMENT_SUBTYPE_KEY_ID = pn_payment_subtype_key_id
               AND PAYMENT_SUBTYPE_CLASS =  pv_payment_subtype_class
               AND ADMIN_CMD_STATE_ID = 1
               AND (pc_settle_processing_enabled = 'Y' OR ADMIN_CMD_TYPE_ID NOT IN(1,2,3,6,8))
               AND (pc_tran_processing_enabled = 'Y' OR ADMIN_CMD_TYPE_ID NOT IN(4,5,7))
               AND (pc_retry_only != 'Y' OR ADMIN_CMD_TYPE_ID IN(2, 3, 4, 5, 6, 7))
               AND (pc_allow_add_to_batch = 'Y' OR pv_payment_subtype_class NOT LIKE 'Authority::ISO8583%' OR ADMIN_CMD_TYPE_ID NOT IN(5))
             ORDER BY PRIORITY ASC, CREATED_UTC_TS ASC;
    BEGIN
        -- get the first command and translate it into a terminal batch or tran to process
        FOR l_rec IN l_cur LOOP
            IF l_rec.ADMIN_CMD_TYPE_ID IN(1,2,3,6,5,8) AND pv_payment_subtype_class NOT LIKE 'Authority::ISO8583%' THEN
                MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Settlement commands are not allowed for Payment Subtype Class ''' || pv_payment_subtype_class || '''');
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 1 THEN -- Settle All
                GET_PENDING_SETTLEMENTS(
                    pn_payment_subtype_key_id, 
                    pv_payment_subtype_class, 
                    'Y', 
                    pn_max_settlements,
                    pn_pending_terminal_batch_ids);
                IF pn_pending_terminal_batch_ids IS NULL OR pn_pending_terminal_batch_ids.COUNT < pn_max_settlements THEN
                    -- Mark command executed
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                END IF;
                IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
                    RETURN; -- found something to process so exit
                END IF;
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 2 THEN -- Retry All
                GET_RETRY_SETTLEMENTS(
                    pn_payment_subtype_key_id, 
                    pv_payment_subtype_class,
                    0,
                    pn_max_settlements, 
                    pn_pending_terminal_batch_ids);
                IF pn_pending_terminal_batch_ids IS NULL OR pn_pending_terminal_batch_ids.COUNT < pn_max_settlements THEN
                    -- Mark command executed
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                END IF;
                IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
                    RETURN; -- found something to process so exit
                END IF;   
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 3 THEN -- Retry Batch
                SELECT TERMINAL_BATCH_ID
                  BULK COLLECT INTO pn_pending_terminal_batch_ids
                  FROM (
                    SELECT C.TERMINAL_BATCH_ID
                      FROM (SELECT TERMINAL_ID, TERMINAL_BATCH_ID, TERMINAL_BATCH_OPEN_TS, 
                                   SUM(CASE WHEN TRAN_STATE_CD IN('R','N') THEN 1 ELSE 0 END) NUM_TRAN
                              FROM (SELECT T.TERMINAL_ID, 
                                           TB.TERMINAL_BATCH_ID,
                                           X.TRAN_ID,
                                           X.TRAN_STATE_CD,
                                           TB.TERMINAL_BATCH_OPEN_TS
                                      FROM PSS.TERMINAL T
                                      JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
                                      JOIN PSS.AUTH A ON TB.TERMINAL_BATCH_ID = A.TERMINAL_BATCH_ID
                                      JOIN PSS.TRAN X ON A.TRAN_ID = X.TRAN_ID 
                                     WHERE TB.TERMINAL_BATCH_OPEN_TS IS NOT NULL
                                       AND DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y') = 'N'
                                       AND T.TERMINAL_ID = pn_payment_subtype_key_id
                                     UNION 
                                    SELECT T.TERMINAL_ID, 
                                           TB.TERMINAL_BATCH_ID,
                                           X.TRAN_ID,
                                           X.TRAN_STATE_CD,
                                           TB.TERMINAL_BATCH_OPEN_TS
                                      FROM PSS.TERMINAL T
                                      JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
                                      JOIN PSS.REFUND R ON TB.TERMINAL_BATCH_ID = R.TERMINAL_BATCH_ID
                                      JOIN PSS.TRAN X ON R.TRAN_ID = X.TRAN_ID 
                                     WHERE TB.TERMINAL_BATCH_OPEN_TS IS NOT NULL
                                       AND DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y') = 'N'
                                       AND T.TERMINAL_ID = pn_payment_subtype_key_id) 
                             GROUP BY TERMINAL_ID, TERMINAL_BATCH_ID, TERMINAL_BATCH_OPEN_TS
                             HAVING COUNT(TRAN_STATE_CD) = SUM(CASE WHEN TRAN_STATE_CD IN('R', 'N', 'E') THEN 1 ELSE 0 END)) C
                      JOIN PSS.ADMIN_CMD_PARAM ACP ON C.TERMINAL_BATCH_ID = TO_NUMBER_OR_NULL(ACP.PARAM_VALUE)
                     WHERE C.NUM_TRAN > 0
                       AND ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'TERMINAL_BATCH_ID'
                   ORDER BY c.TERMINAL_BATCH_OPEN_TS ASC)
                 WHERE ROWNUM <= pn_max_settlements;
                IF pn_pending_terminal_batch_ids IS NULL OR pn_pending_terminal_batch_ids.COUNT < pn_max_settlements THEN
                    -- Mark command executed
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                END IF;
                IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
                    RETURN; -- found something to process so exit
                END IF;   
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 4 THEN -- Retry Tran
                SELECT TRAN_ID
                  BULK COLLECT INTO pn_pending_tran_ids
                  FROM (SELECT T.TRAN_ID 
                          FROM PSS.TRAN T
                          LEFT OUTER JOIN PSS.AUTH A   ON T.TRAN_ID = A.TRAN_ID
                          LEFT OUTER JOIN PSS.REFUND R ON T.TRAN_ID = R.TRAN_ID
                          JOIN PSS.ADMIN_CMD_PARAM ACP ON T.TRAN_ID = TO_NUMBER_OR_NULL(ACP.PARAM_VALUE)
                         WHERE T.TRAN_STATE_CD IN('I', 'J') /* transaction_incomplete, transaction_incomplete_error */
                           AND T.PAYMENT_SUBTYPE_KEY_ID = pn_payment_subtype_key_id 
                           AND T.PAYMENT_SUBTYPE_CLASS  = pv_payment_subtype_class
                           AND ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                           AND ACP.PARAM_NAME = 'TRAN_ID'
                           AND (pc_allow_add_to_batch = 'Y' OR NVL(A.TERMINAL_BATCH_ID, R.TERMINAL_BATCH_ID) IS NOT NULL)
                         GROUP BY T.TRAN_ID
                         ORDER BY T.TRAN_ID)
                 WHERE ROWNUM <= pn_max_trans;
                IF pn_pending_tran_ids IS NULL OR pn_pending_tran_ids.COUNT < pn_max_trans THEN
                    -- Mark command executed
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                END IF;
                IF pn_pending_tran_ids IS NOT NULL AND pn_pending_tran_ids.COUNT > 0 THEN 
                    RETURN; -- found something to process so exit
                END IF;
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 5 THEN -- Force Tran
                 DECLARE
                    ln_tran_id PSS.TRAN.TRAN_ID%TYPE;
                    lv_force_reason PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_RESP_DESC%TYPE;
                BEGIN
                    SELECT TO_NUMBER_OR_NULL(ACP.PARAM_VALUE)
                      INTO ln_tran_id
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'TRAN_ID';
                    SELECT ACP.PARAM_VALUE
                      INTO lv_force_reason
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'FORCE_REASON';    
                    FORCE_TRAN(pn_payment_subtype_key_id, pv_payment_subtype_class, ln_tran_id, lv_force_reason);
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Parameter ''TRAN_ID'' not found');
                    WHEN WRONG_TRAN_STATE THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, SQLERRM);
                    WHEN OTHERS THEN
                        IF SQLCODE IN(1, 20556) THEN
                            MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, SQLERRM);
                        ELSE
                            RAISE;
                        END IF;
                END;
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 6 THEN -- Force Batch
                DECLARE
                    ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
                    lv_force_reason PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_RESP_DESC%TYPE;
                BEGIN
                    SELECT TO_NUMBER_OR_NULL(ACP.PARAM_VALUE)
                      INTO ln_terminal_batch_id
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'TERMINAL_BATCH_ID';
                    SELECT ACP.PARAM_VALUE
                      INTO lv_force_reason
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'FORCE_REASON';    
                    FORCE_SETTLE_BATCH(ln_terminal_batch_id, lv_force_reason);
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Parameter ''TERMINAL_BATCH_ID'' or ''FORCE_REASON'' not found');
                    WHEN WRONG_SETTLE_BATCH_STATE THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, SQLERRM);
                    WHEN OTHERS THEN
                        IF SQLCODE = 1 THEN
                            MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, SQLERRM);
                        ELSE
                            RAISE;
                        END IF;
                END;
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 7 THEN -- Error Tran
                 DECLARE
                    ln_tran_id PSS.TRAN.TRAN_ID%TYPE;
                    lv_error_reason PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_RESP_DESC%TYPE;
                BEGIN
                    SELECT TO_NUMBER_OR_NULL(ACP.PARAM_VALUE)
                      INTO ln_tran_id
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'TRAN_ID';
                    SELECT ACP.PARAM_VALUE
                      INTO lv_error_reason
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'ERROR_REASON';    
                    ERROR_TRAN(ln_tran_id, lv_error_reason);
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Parameter ''TRAN_ID'' not found');
                    WHEN WRONG_TRAN_STATE THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, SQLERRM);
                    WHEN OTHERS THEN
                        IF SQLCODE = 1 THEN
                            MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, SQLERRM);
                        ELSE
                            RAISE;
                        END IF;
                END;
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 8 THEN -- Settle Batch
                DECLARE
                    ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
                    lc_terminal_batch_closed CHAR;
                    ln_num_tran PLS_INTEGER;
                    ln_count_tran PLS_INTEGER;
                    ln_okay_tran PLS_INTEGER;          
                BEGIN
                    SELECT MAX(TO_NUMBER_OR_NULL(ACP.PARAM_VALUE))
                      INTO ln_terminal_batch_id
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'TERMINAL_BATCH_ID';
                    IF ln_terminal_batch_id IS NULL THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Parameter ''TERMINAL_BATCH_ID'' not found');
                    ELSE
                        SELECT DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y'), 
                           SUM(CASE WHEN X.TRAN_STATE_CD IN('T') THEN 1 ELSE 0 END) NUM_TRAN,
                           COUNT(X.TRAN_STATE_CD) COUNT_TRAN,
                           SUM(CASE WHEN X.TRAN_STATE_CD IN('T', 'E') THEN 1 ELSE 0 END) OKAY_TRAN
                          INTO lc_terminal_batch_closed,
                               ln_num_tran,
                               ln_count_tran,
                               ln_okay_tran
                          FROM PSS.TERMINAL_BATCH TB
                          JOIN (SELECT A.TRAN_ID, A.TERMINAL_BATCH_ID
                                  FROM PSS.AUTH A
                                UNION
                                SELECT R.TRAN_ID, R.TERMINAL_BATCH_ID
                                  FROM PSS.REFUND R) A ON TB.TERMINAL_BATCH_ID = A.TERMINAL_BATCH_ID
                          JOIN PSS.TRAN X ON A.TRAN_ID = X.TRAN_ID
                         WHERE TB.TERMINAL_BATCH_ID = ln_terminal_batch_id 
                         GROUP BY DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y');
                        IF lc_terminal_batch_closed != 'N' THEN
                            MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Terminal Batch '|| ln_terminal_batch_id ||' is already closed');
                        ELSIF ln_okay_tran != ln_count_tran THEN
                            NULL; -- Not ready for settlement yet
                        ELSIF ln_num_tran = 0 THEN
                            MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID); -- No non-error transaction, so mark as complete and continue
                        ELSE
                            SELECT ln_terminal_batch_id
                              BULK COLLECT INTO pn_pending_terminal_batch_ids
                              FROM DUAL;
                            -- Mark command executed                          
                            MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                            RETURN; -- found something to process so exit
                        END IF;
                    END IF;
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Terminal Batch ' ||ln_terminal_batch_id||' not found');
                END;
            ELSE
                -- Unknown cmd type
                NULL;
            END IF;
        END LOOP;
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE GET_PENDING_ACTIONS(
        pn_payment_subtype_key_id PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE, 
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_settlement_retry_interval NUMBER,
        pn_tran_retry_interval NUMBER,
        pn_max_settlements PLS_INTEGER, 
        pn_max_trans PLS_INTEGER,
        pn_terminal_state_id OUT PSS.TERMINAL.TERMINAL_STATE_ID%TYPE,
        pn_pending_terminal_batch_ids OUT NUMBER_TABLE,
        pn_pending_tran_ids OUT NUMBER_TABLE,
        pn_terminal_batch_id OUT PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE,
        pc_call_again_flag OUT VARCHAR2)
    IS
        ln_batch_max_trans PSS.TERMINAL.TERMINAL_BATCH_MAX_TRAN%TYPE;
        ln_allowed_trans PLS_INTEGER;
        lc_settle_processing_enabled CHAR(1);
        lc_tran_processing_enabled CHAR(1);
        lv_terminal_global_token_cd PSS.TERMINAL.GLOBAL_TOKEN_CD%TYPE;
        lc_terminal_capture_flag PSS.TERMINAL_BATCH.TERMINAL_CAPTURE_FLAG%TYPE;
        ln_cnt PLS_INTEGER;
    BEGIN
        SELECT NVL(MAX(POSM_SETTING_VALUE), 'N')
          INTO lc_tran_processing_enabled
          FROM PSS.POSM_SETTING
         WHERE POSM_SETTING_NAME = 'TRAN_PROCESSING_ENABLED';
        
        -- Get terminal state id
        IF pv_payment_subtype_class  LIKE 'Authority::ISO8583%' THEN -- this is same as in APP_LAYER.VW_PAYMENT_SUBTYPE_DETAIL
            SELECT NVL(MAX(POSM_SETTING_VALUE), 'N')
              INTO lc_settle_processing_enabled
              FROM PSS.POSM_SETTING
             WHERE POSM_SETTING_NAME = 'SETTLE_PROCESSING_ENABLED';
        
            SELECT T.TERMINAL_STATE_ID, COALESCE(T.TERMINAL_BATCH_MAX_TRAN, AU.AUTHORITY_BATCH_MAX_TRAN, 999), AU.TERMINAL_CAPTURE_FLAG
              INTO pn_terminal_state_id, ln_batch_max_trans, lc_terminal_capture_flag
              FROM PSS.TERMINAL T
              JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
              JOIN AUTHORITY.AUTHORITY AU ON M.AUTHORITY_ID = AU.AUTHORITY_ID                       
             WHERE T.TERMINAL_ID = pn_payment_subtype_key_id;
            IF pn_terminal_state_id = 5 THEN
                -- see if there are retry admin commands
                GET_ACTIONS_FROM_ADMIN_CMDS(
                    pn_payment_subtype_key_id, 
                    pv_payment_subtype_class, 
                    pn_max_settlements, 
                    0,
                    'N', -- pc_allow_add_to_batch
                    'Y', -- pc_retry_only
                    lc_settle_processing_enabled,
                    lc_tran_processing_enabled,
                    pn_pending_terminal_batch_ids,
                    pn_pending_tran_ids);
                IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
                    RETURN; -- Early Exit if we found something to process
                END IF;
                IF lc_settle_processing_enabled = 'Y' THEN
                    -- Only settlement retry is available
                    -- Find retyable batches
                    GET_RETRY_SETTLEMENTS(
                        pn_payment_subtype_key_id, 
                        pv_payment_subtype_class,
                        pn_settlement_retry_interval,
                        pn_max_settlements, 
                        pn_pending_terminal_batch_ids);
                    IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
                        RETURN; -- Early Exit if we found something to process
                    END IF;  
                    -- Make sure terminal should be in state 5 (that there is an unclosed batch)
                    SELECT COUNT(*)
                      INTO ln_cnt
                      FROM PSS.TERMINAL_BATCH TB
                      JOIN PSS.SETTLEMENT_BATCH SB ON TB.TERMINAL_BATCH_ID = SB.TERMINAL_BATCH_ID
                     WHERE TB.TERMINAL_BATCH_OPEN_TS IS NOT NULL
                       AND DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y') = 'N'
                       AND TB.TERMINAL_ID = pn_payment_subtype_key_id;
                    IF ln_cnt = 0 THEN
                        UPDATE PSS.TERMINAL 
                           SET TERMINAL_STATE_ID = 3
                         WHERE TERMINAL_ID = pn_payment_subtype_key_id;
                    END IF;
                END IF;
                RETURN; -- if terminal is in state 5, we don't process anything else
            ELSIF pn_terminal_state_id != 3 THEN
                RETURN; -- Terminal not locked for processing
            END IF;
        ELSE
            lv_terminal_global_token_cd := GET_TERMINAL_GLOBAL_TOKEN(pv_payment_subtype_class, pn_payment_subtype_key_id);
            IF lv_terminal_global_token_cd IS NULL THEN
                pn_terminal_state_id := 1;
                RETURN;  -- Terminal not locked for processing
            ELSE
                pn_terminal_state_id := 3;
                lc_settle_processing_enabled := 'N';
            END IF;
        END IF;
        
        IF lc_tran_processing_enabled != 'Y' AND lc_settle_processing_enabled != 'Y' THEN
            RETURN; -- we are not doing any processing right now
        END IF;
        
        -- limit num of trans if need be
        IF ln_batch_max_trans IS NOT NULL THEN
            pn_terminal_batch_id := GET_AVAILABLE_TERMINAL_BATCH(pn_payment_subtype_key_id, ln_batch_max_trans, TRUE);
            IF ln_batch_max_trans < pn_max_trans THEN
                ln_allowed_trans := ln_batch_max_trans;
            ELSE
                ln_allowed_trans := pn_max_trans;
            END IF;
        ELSE
            ln_allowed_trans := pn_max_trans;
        END IF;
        
        -- Check pending commands and process them first
        IF ln_allowed_trans <= 0 THEN            
            GET_ACTIONS_FROM_ADMIN_CMDS(
                pn_payment_subtype_key_id, 
                pv_payment_subtype_class, 
                pn_max_settlements, 
                pn_max_trans,
                'N', -- pc_allow_add_to_batch
                'N', -- pc_retry_only
                lc_settle_processing_enabled,
                lc_tran_processing_enabled,
                pn_pending_terminal_batch_ids,
                pn_pending_tran_ids);
        ELSE
            GET_ACTIONS_FROM_ADMIN_CMDS(
                pn_payment_subtype_key_id, 
                pv_payment_subtype_class, 
                pn_max_settlements, 
                ln_allowed_trans,
                'Y', -- pc_allow_add_to_batch
                'N', -- pc_retry_only
                lc_settle_processing_enabled,
                lc_tran_processing_enabled,
                pn_pending_terminal_batch_ids,
                pn_pending_tran_ids);
        END IF;
        IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
            RETURN; -- Early Exit if we found something to process
        ELSIF pn_pending_tran_ids IS NOT NULL AND pn_pending_tran_ids.COUNT > 0 THEN
            RETURN; -- Early Exit if we found something to process
        END IF;
        
        -- Check for Settlements
        IF lc_settle_processing_enabled = 'Y' THEN
            GET_PENDING_SETTLEMENTS(
                pn_payment_subtype_key_id, 
                pv_payment_subtype_class, 
                'N', 
                pn_max_settlements,
                pn_pending_terminal_batch_ids);
            IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
                RETURN; -- Early Exit if we found something to process
            END IF;
            GET_RETRY_SETTLEMENTS(
                pn_payment_subtype_key_id, 
                pv_payment_subtype_class,
                pn_settlement_retry_interval,
                pn_max_settlements, 
                pn_pending_terminal_batch_ids);
            IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
                RETURN; -- Early Exit if we found something to process
            END IF;
        END IF;
        
        IF lc_tran_processing_enabled != 'Y' THEN
            RETURN; -- we are not doing any tran processing right now
        END IF;
        IF lc_terminal_capture_flag = 'Y' AND pn_terminal_batch_id IS NOT NULL THEN
            SELECT TRAN_ID
              BULK COLLECT INTO pn_pending_tran_ids
              FROM (SELECT /*+ index(T IDX_TRAN_STATE_CD) */ T.TRAN_ID, T.TRAN_UPLOAD_TS
                      FROM PSS.TRAN T
                     WHERE T.TRAN_STATE_CD IN ('8', 'A', 'W') /* processed_server_batch, processing_server_tran, processed_server_auth_pending_reversal */
                       AND T.PAYMENT_SUBTYPE_KEY_ID = pn_payment_subtype_key_id
                       AND T.PAYMENT_SUBTYPE_CLASS  = pv_payment_subtype_class
                     ORDER BY T.TRAN_UPLOAD_TS ASC, T.TRAN_ID ASC)
             WHERE ROWNUM <= ln_allowed_trans;
            IF pn_pending_tran_ids IS NULL OR pn_pending_tran_ids.COUNT = 0 THEN
                RETURN; -- early exit
            END IF;
        
            -- prepare transactions
            -- NOTE: update tran first so as to lock it
            UPDATE PSS.TRAN X
			   SET TRAN_STATE_CD = CASE 
                    WHEN X.AUTH_HOLD_USED = 'Y' THEN 'T' 
                    WHEN 0 != (
                        SELECT NVL(SUM(TLI.TRAN_LINE_ITEM_AMOUNT * TLI.TRAN_LINE_ITEM_QUANTITY), 0) 
                          FROM PSS.TRAN_LINE_ITEM TLI 
                         WHERE TLI.TRAN_ID = X.TRAN_ID 
                           AND TLI.TRAN_LINE_ITEM_BATCH_TYPE_CD = 'A'
                    ) THEN 'T' 
                    ELSE 'C' END
			 WHERE X.TRAN_ID IN(SELECT COLUMN_VALUE FROM TABLE(pn_pending_tran_ids)); -- NOTE: using MEMBER OF causes a FTS			
            INSERT INTO PSS.AUTH (
                        AUTH_ID,
                        TRAN_ID,
                        AUTH_TYPE_CD,
                        AUTH_STATE_ID,
                        AUTH_PARSED_ACCT_DATA,
                        ACCT_ENTRY_METHOD_CD,
                        AUTH_AMT,
                        AUTH_TS,
                        TERMINAL_BATCH_ID,
                        TRACE_NUMBER)
                 SELECT PSS.SEQ_AUTH_ID.NEXTVAL,
                        TRAN_ID,
                        CASE 
                            WHEN 0 != SALE_AMOUNT THEN 'U' 
                            ELSE 'C' END,
                        6,
                        AUTH_PARSED_ACCT_DATA,
                        ACCT_ENTRY_METHOD_CD,
                        SALE_AMOUNT,	    
                        SYSDATE,
                        pn_terminal_batch_id,
                        TRACE_NUMBER
                   FROM (SELECT /*+INDEX(X PK_TRAN)*/
                        A.TRAN_ID,
                        NVL(S.SALE_AMOUNT, 0) SALE_AMOUNT,
                        MAX(a.AUTH_PARSED_ACCT_DATA) AUTH_PARSED_ACCT_DATA,
                        MAX(a.ACCT_ENTRY_METHOD_CD) ACCT_ENTRY_METHOD_CD,
                        MAX(a.TRACE_NUMBER) TRACE_NUMBER
                   FROM PSS.AUTH a
                   JOIN PSS.TRAN X ON A.TRAN_ID = X.TRAN_ID
                   LEFT OUTER JOIN PSS.SALE S ON S.TRAN_ID = A.TRAN_ID AND S.SALE_TYPE_CD = 'A'
                  WHERE X.TRAN_STATE_CD = 'T'
                    AND A.AUTH_TYPE_CD IN('L', 'N') AND (A.AUTH_RESULT_CD IN('Y', 'P', 'V', 'C', 'M') OR (A.AUTH_RESULT_CD IN('N', 'F') AND A.AUTH_HOLD_USED = 'Y' AND NVL(S.SALE_AMOUNT, 0) = 0))
                    AND A.TRAN_ID IN(SELECT COLUMN_VALUE FROM TABLE(pn_pending_tran_ids))  -- NOTE: using MEMBER OF causes a FTS
                    GROUP BY A.TRAN_ID, NVL(S.SALE_AMOUNT, 0));
            UPDATE PSS.REFUND
               SET TERMINAL_BATCH_ID = pn_terminal_batch_id,
                   REFUND_STATE_ID = 6
             WHERE TRAN_ID IN(SELECT COLUMN_VALUE FROM TABLE(pn_pending_tran_ids)); 
            IF pn_pending_tran_ids.COUNT >= ln_batch_max_trans THEN
                pn_pending_terminal_batch_ids := NUMBER_TABLE(pn_terminal_batch_id);
            ELSIF pn_pending_tran_ids.COUNT >= pn_max_trans THEN
                pc_call_again_flag := 'Y';
            END IF;
            pn_pending_tran_ids := NULL;
        ELSE
        -- Find retry trans
        IF ln_allowed_trans <= 0 THEN
             -- We can only process trans already in a batch
             SELECT DISTINCT TRAN_ID
              BULK COLLECT INTO pn_pending_tran_ids
              FROM (SELECT /*+ USE_NL(T,A,R) index(T IDX_TRAN_STATE_CD) index(A IDX_AUTH_TRAN_ID) */ T.TRAN_ID, T.TRAN_UPLOAD_TS
                      FROM PSS.TRAN T
                      LEFT OUTER JOIN PSS.AUTH A   ON T.TRAN_ID = A.TRAN_ID
                      LEFT OUTER JOIN PSS.REFUND R ON T.TRAN_ID = R.TRAN_ID
                     WHERE T.TRAN_STATE_CD = 'I' /* transaction_incomplete */
                       AND T.PAYMENT_SUBTYPE_KEY_ID = pn_payment_subtype_key_id 
                       AND T.PAYMENT_SUBTYPE_CLASS  = pv_payment_subtype_class
                       AND NVL(A.TERMINAL_BATCH_ID, R.TERMINAL_BATCH_ID) IS NOT NULL
                     GROUP BY T.TRAN_ID, T.TRAN_UPLOAD_TS
                    HAVING SYSDATE >= pn_tran_retry_interval + GREATEST(NVL(MAX(A.AUTH_TS), MIN_DATE), NVL(MAX(R.LAST_UPDATED_TS), MIN_DATE))
                     ORDER BY T.TRAN_UPLOAD_TS ASC, T.TRAN_ID ASC)
             WHERE ROWNUM <= pn_max_trans;
                pn_terminal_batch_id := NULL;
            RETURN; -- we can't process trans right now
        END IF;
        
        SELECT DISTINCT TRAN_ID
          BULK COLLECT INTO pn_pending_tran_ids
          FROM (SELECT /*+ USE_NL(T,A,R) index(T IDX_TRAN_STATE_CD) index(A IDX_AUTH_TRAN_ID) */ T.TRAN_ID, T.TRAN_UPLOAD_TS
                  FROM PSS.TRAN T
                  LEFT OUTER JOIN PSS.AUTH A   ON T.TRAN_ID = A.TRAN_ID
                  LEFT OUTER JOIN PSS.REFUND R ON T.TRAN_ID = R.TRAN_ID
                 WHERE T.TRAN_STATE_CD = 'I' /* transaction_incomplete */
                   AND T.PAYMENT_SUBTYPE_KEY_ID = pn_payment_subtype_key_id 
                   AND T.PAYMENT_SUBTYPE_CLASS  = pv_payment_subtype_class
                 GROUP BY T.TRAN_ID, T.TRAN_UPLOAD_TS
                HAVING SYSDATE >= pn_tran_retry_interval + GREATEST(NVL(MAX(A.AUTH_TS), MIN_DATE), NVL(MAX(R.LAST_UPDATED_TS), MIN_DATE))
                 ORDER BY T.TRAN_UPLOAD_TS ASC, T.TRAN_ID ASC)
         WHERE ROWNUM <= ln_allowed_trans;
        IF pn_pending_tran_ids.COUNT > 0 THEN
            RETURN; -- Early Exit if we found something to process
        END IF;
          
        -- get new pending trans
        SELECT TRAN_ID
          BULK COLLECT INTO pn_pending_tran_ids
          FROM (SELECT /*+ index(T IDX_TRAN_STATE_CD) */ T.TRAN_ID, T.TRAN_UPLOAD_TS
                  FROM PSS.TRAN T
                 WHERE T.TRAN_STATE_CD IN ('8', '9', 'W') /* processed_server_batch, processed_server_batch_intended, processed_server_auth_pending_reversal */
                   AND (T.TRAN_STATE_CD != '9' OR T.AUTH_HOLD_USED != 'Y') /* Filters out the Intended Transactions which have an Auth Hold */
                   AND T.PAYMENT_SUBTYPE_KEY_ID = pn_payment_subtype_key_id
                   AND T.PAYMENT_SUBTYPE_CLASS  = pv_payment_subtype_class
                 ORDER BY T.TRAN_UPLOAD_TS ASC, T.TRAN_ID ASC)
         WHERE ROWNUM <= ln_allowed_trans;        
        END IF;
    END;
    
    PROCEDURE SET_UNIQUE_SECONDS(
        pn_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE,
        pn_authority_id PSS.TERMINAL_BATCH.AUTHORITY_ID%TYPE,
        pn_unique_seconds IN OUT PSS.TERMINAL_BATCH.UNIQUE_SECONDS%TYPE)
    IS
    BEGIN
        UPDATE PSS.TERMINAL_BATCH
           SET UNIQUE_SECONDS = pn_unique_seconds,
               AUTHORITY_ID = pn_authority_id
         WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id;
    EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
            pn_unique_seconds := pn_unique_seconds + 1;
            SET_UNIQUE_SECONDS(pn_terminal_batch_id, pn_authority_id, pn_unique_seconds);
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE GET_OR_CREATE_SETTLEMENT_BATCH(
        pn_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE,
        pn_settlement_batch_id OUT PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_prior_attempts OUT PLS_INTEGER,
        pc_upload_needed_flag OUT VARCHAR2)
    IS
        lc_terminal_capture_flag PSS.TERMINAL_BATCH.TERMINAL_CAPTURE_FLAG%TYPE;
        ln_terminal_id PSS.TERMINAL_BATCH.TERMINAL_ID%TYPE;
        ln_unique_seconds PSS.TERMINAL_BATCH.UNIQUE_SECONDS%TYPE;
        ln_authority_id PSS.TERMINAL_BATCH.AUTHORITY_ID%TYPE;
    BEGIN
        -- check for an incomplete settlement_batch
        SELECT MAX(SB.SETTLEMENT_BATCH_ID), TB.TERMINAL_ID, TB.TERMINAL_CAPTURE_FLAG, TB.UNIQUE_SECONDS
          INTO pn_settlement_batch_id, ln_terminal_id, lc_terminal_capture_flag, ln_unique_seconds
          FROM PSS.TERMINAL_BATCH TB
          LEFT OUTER JOIN PSS.SETTLEMENT_BATCH SB ON TB.TERMINAL_BATCH_ID = SB.TERMINAL_BATCH_ID AND SB.SETTLEMENT_BATCH_STATE_ID = 4
         WHERE TB.TERMINAL_BATCH_ID = pn_terminal_batch_id
         GROUP BY TB.TERMINAL_ID, TB.TERMINAL_CAPTURE_FLAG, TB.UNIQUE_SECONDS;
        IF pn_settlement_batch_id IS NULL THEN
            SELECT PSS.SEQ_SETTLEMENT_BATCH_ID.NEXTVAL
              INTO pn_settlement_batch_id
              FROM DUAL;            
            
            INSERT INTO PSS.SETTLEMENT_BATCH(
                SETTLEMENT_BATCH_ID,
                SETTLEMENT_BATCH_STATE_ID,
                SETTLEMENT_BATCH_START_TS,
                TERMINAL_BATCH_ID
            ) VALUES (
                pn_settlement_batch_id,
                4,
                SYSDATE,
                pn_terminal_batch_id);
            /* Do this after transaction is uploaded (SALEd) with the Authority)    
            -- add all trans and update their tran state cd
            INSERT INTO PSS.TRAN_SETTLEMENT_BATCH(
                SETTLEMENT_BATCH_ID,
                AUTH_ID,
                TRAN_ID,
                TRAN_SETTLEMENT_B_AMT)
              SELECT DISTINCT 
                     pn_settlement_batch_id,
                     FIRST_VALUE(A.AUTH_ID) OVER (PARTITION BY T.TRAN_ID ORDER BY A.CREATED_TS DESC, A.AUTH_ID DESC) AUTH_ID, 
                     T.TRAN_ID, 
                     FIRST_VALUE(A.AUTH_AMT) OVER (PARTITION BY T.TRAN_ID ORDER BY A.CREATED_TS DESC, A.AUTH_ID DESC) AUTH_AMT
                FROM PSS.TRAN T
                JOIN PSS.AUTH A ON T.TRAN_ID = A.TRAN_ID
               WHERE A.TERMINAL_BATCH_ID = pn_terminal_batch_id
                 AND (A.AUTH_STATE_ID = 2
                      OR (A.AUTH_STATE_ID = 4 AND A.AUTH_TYPE_CD = 'N' AND A.AUTH_HOLD_USED = 'Y'));
            INSERT INTO PSS.REFUND_SETTLEMENT_BATCH(
                SETTLEMENT_BATCH_ID,
                REFUND_ID,
                TRAN_ID,
                REFUND_SETTLEMENT_B_AMT)
              SELECT DISTINCT 
                     pn_settlement_batch_id,
                     FIRST_VALUE(R.REFUND_ID) OVER (PARTITION BY T.TRAN_ID ORDER BY R.CREATED_TS DESC, R.REFUND_ID DESC) AUTH_ID, 
                     T.TRAN_ID, 
                     FIRST_VALUE(R.REFUND_AMT) OVER (PARTITION BY T.TRAN_ID ORDER BY R.CREATED_TS DESC, R.REFUND_ID DESC) REFUND_AMT
                FROM PSS.TRAN T
                JOIN PSS.REFUND R ON T.TRAN_ID = R.TRAN_ID
               WHERE R.TERMINAL_BATCH_ID = pn_terminal_batch_id
                 AND R.REFUND_STATE_ID IN(1);
            */
        END IF;
        UPDATE PSS.TRAN 
           SET TRAN_STATE_CD = 'S'
         WHERE TRAN_STATE_CD IN('R', 'N', 'T')
           AND TRAN_ID IN(
            SELECT A.TRAN_ID
              FROM PSS.AUTH A 
             WHERE A.TERMINAL_BATCH_ID = pn_terminal_batch_id
               AND (A.AUTH_STATE_ID IN(2, 5, 6)
                  OR (A.AUTH_STATE_ID IN(3, 4) AND A.AUTH_TYPE_CD = 'N' AND A.AUTH_HOLD_USED = 'Y'))
            UNION ALL
            SELECT R.TRAN_ID
              FROM PSS.REFUND R
             WHERE R.TERMINAL_BATCH_ID = pn_terminal_batch_id
               AND R.REFUND_STATE_ID IN(1, 6));
        IF lc_terminal_capture_flag != 'Y' THEN
            UPDATE PSS.TERMINAL
               SET TERMINAL_STATE_ID = 5
             WHERE TERMINAL_ID = ln_terminal_id;
        END IF;
        SELECT COUNT(*)
          INTO pn_prior_attempts
    	  FROM PSS.SETTLEMENT_BATCH  
    	 WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id 
    	   AND SETTLEMENT_BATCH_STATE_ID IN(2, 3);
        IF pn_prior_attempts > 0 THEN
            SELECT DECODE(SETTLEMENT_BATCH_STATE_ID, 2, 'N', 3, 'Y')
              INTO pc_upload_needed_flag
              FROM (
                SELECT SETTLEMENT_BATCH_END_TS, SETTLEMENT_BATCH_ID, SETTLEMENT_BATCH_STATE_ID
                  FROM PSS.SETTLEMENT_BATCH  
                 WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id 
                   AND SETTLEMENT_BATCH_STATE_ID IN(2, 3)           
                 ORDER BY SETTLEMENT_BATCH_END_TS DESC, SETTLEMENT_BATCH_ID DESC)
             WHERE ROWNUM = 1;
        ELSE
            pc_upload_needed_flag := 'N';
        END IF;
        IF lc_terminal_capture_flag = 'Y' AND ln_unique_seconds IS NULL THEN
            SELECT ROUND(DATE_TO_MILLIS(SYSDATE) / 1000), M.AUTHORITY_ID
              INTO ln_unique_seconds, ln_authority_id
              FROM PSS.TERMINAL T
              JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
             WHERE T.TERMINAL_ID = ln_terminal_id;
            SET_UNIQUE_SECONDS(pn_terminal_batch_id, ln_authority_id, ln_unique_seconds);
        END IF;
    END;
    /*
    FUNCTION IS_LAST_TERMINAL_BATCH_OPEN(
        pn_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE)
    RETURN PLS_INTEGER
    IS
        ln_is_open PLS_INTEGER;
    BEGIN
        SELECT DECODE(TERMINAL_BATCH_CLOSE_TS, NULL, 1, 0)
          INTO ln_is_open
          FROM (SELECT TERMINAL_BATCH_NUM, 
                       TERMINAL_BATCH_CYCLE_NUM, 
                       TERMINAL_BATCH_CLOSE_TS
                  FROM PSS.TERMINAL_BATCH
                 WHERE TERMINAL_ID = pn_terminal_id
                 ORDER BY TERMINAL_BATCH_CYCLE_NUM DESC,
                          TERMINAL_BATCH_NUM DESC)
         WHERE ROWNUM = 1;
        RETURN ln_is_open;
    END;
    */
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE UPDATE_PROCESSED_BATCH(
        pn_settlement_batch_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pv_authority_response_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
        pv_authority_response_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
        pd_batch_closed_ts PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_END_TS%TYPE)
    IS
        ln_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE;
        ln_settlement_batch_state_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_STATE_ID%TYPE;
        ln_terminal_capture_flag PSS.TERMINAL_BATCH.TERMINAL_CAPTURE_FLAG%TYPE;
    BEGIN
        SELECT SB.SETTLEMENT_BATCH_STATE_ID, TB.TERMINAL_CAPTURE_FLAG
          INTO ln_settlement_batch_state_id, ln_terminal_capture_flag
          FROM PSS.SETTLEMENT_BATCH SB
          JOIN PSS.TERMINAL_BATCH TB ON TB.TERMINAL_BATCH_ID = SB.TERMINAL_BATCH_ID
         WHERE SB.SETTLEMENT_BATCH_ID = pn_settlement_batch_id;
        IF ln_settlement_batch_state_id = 1 OR (ln_settlement_batch_state_id IN(1,2,3,7) AND pc_auth_result_cd IN('P')) THEN
            RAISE_APPLICATION_ERROR(-20100, 'Settlement batch ' || pn_settlement_batch_id || ' was already processed');
        END IF;
        -- update the settlement batch
        UPDATE PSS.SETTLEMENT_BATCH
           SET SETTLEMENT_BATCH_STATE_ID = DECODE(pc_auth_result_cd, 'Y', 1, 'P', 7, 'N', 2, 'F', 3, 'O', 2, 'R', 2),
               SETTLEMENT_BATCH_RESP_CD = pv_authority_response_cd,
               SETTLEMENT_BATCH_RESP_DESC = pv_authority_response_desc,
               SETTLEMENT_BATCH_REF_CD = pv_authority_ref_cd,
               SETTLEMENT_BATCH_END_TS = pd_batch_closed_ts
         WHERE SETTLEMENT_BATCH_ID = pn_settlement_batch_id;
            
        -- add tran settlement record for each tran in batch
        INSERT INTO PSS.TRAN_SETTLEMENT_BATCH(
               SETTLEMENT_BATCH_ID,
               AUTH_ID,
               TRAN_ID,
               TRAN_SETTLEMENT_B_AMT,
               TRAN_SETTLEMENT_B_RESP_CD,
               TRAN_SETTLEMENT_B_RESP_DESC)
        SELECT pn_settlement_batch_id,
               SA.AUTH_ID,
               X.TRAN_ID,
               DECODE(pc_auth_result_cd, 'Y', SA.AUTH_AMT_APPROVED),
               pv_authority_response_cd,
               pv_authority_response_desc
          FROM PSS.AUTH SA 
          JOIN PSS.TRAN X ON SA.TRAN_ID = X.TRAN_ID
         WHERE SA.TERMINAL_BATCH_ID = pn_terminal_batch_id
           AND SA.AUTH_TYPE_CD IN('U','S','O','A','D','C','E','V','I', 'N')
           AND ((SA.AUTH_STATE_ID IN(2, 5) AND SA.AUTH_TYPE_CD != 'N')
                OR (SA.AUTH_STATE_ID IN(6) AND ln_terminal_capture_flag = 'Y' AND SA.AUTH_TYPE_CD != 'N')
                OR (SA.AUTH_STATE_ID IN(3,4) AND SA.AUTH_TYPE_CD = 'N' AND SA.AUTH_HOLD_USED = 'Y'))
           AND X.TRAN_STATE_CD IN('S');
        -- update the tran_state_cd of all sales in the batch
        UPDATE PSS.TRAN X
           SET TRAN_STATE_CD = (SELECT AFTER_SETTLE_TRAN_STATE_CD(sa.AUTH_TYPE_CD, NULL, pc_auth_result_cd, X.TRAN_DEVICE_RESULT_TYPE_CD)
            FROM PSS.AUTH SA 
           WHERE SA.TRAN_ID = X.TRAN_ID 
		     AND SA.TERMINAL_BATCH_ID = pn_terminal_batch_id
             AND SA.AUTH_TYPE_CD IN('U','S','O','A','D','C','E','V','I', 'N')
             AND ((SA.AUTH_STATE_ID IN(2, 5) AND SA.AUTH_TYPE_CD != 'N')
                OR (SA.AUTH_STATE_ID IN(6) AND ln_terminal_capture_flag = 'Y' AND SA.AUTH_TYPE_CD != 'N')
                OR (SA.AUTH_STATE_ID IN(3,4) AND SA.AUTH_TYPE_CD = 'N' AND SA.AUTH_HOLD_USED = 'Y')))
         WHERE X.TRAN_ID IN(SELECT SA.TRAN_ID 
          FROM PSS.AUTH SA 
         WHERE SA.TERMINAL_BATCH_ID = pn_terminal_batch_id
           AND SA.AUTH_TYPE_CD IN('U','S','O','A','D','C','E','V','I', 'N')
           AND ((SA.AUTH_STATE_ID IN(2, 5) AND SA.AUTH_TYPE_CD != 'N')
                OR (SA.AUTH_STATE_ID IN(6) AND ln_terminal_capture_flag = 'Y' AND SA.AUTH_TYPE_CD != 'N')
                OR (SA.AUTH_STATE_ID IN(3,4) AND SA.AUTH_TYPE_CD = 'N' AND SA.AUTH_HOLD_USED = 'Y')))
           AND X.TRAN_STATE_CD IN('S');
       
        -- add tran settlement record for each tran in batch
        INSERT INTO PSS.REFUND_SETTLEMENT_BATCH(
               SETTLEMENT_BATCH_ID,
               REFUND_ID,
               TRAN_ID,
               REFUND_SETTLEMENT_B_AMT,
               REFUND_SETTLEMENT_B_RESP_CD,
               REFUND_SETTLEMENT_B_RESP_DESC)
        SELECT pn_settlement_batch_id,
               R.REFUND_ID,
               X.TRAN_ID,
               DECODE(pc_auth_result_cd, 'Y', R.REFUND_AMT),
               pv_authority_response_cd,
               pv_authority_response_desc
          FROM PSS.REFUND R  
          JOIN PSS.TRAN X ON R.TRAN_ID = X.TRAN_ID
         WHERE R.TERMINAL_BATCH_ID = pn_terminal_batch_id
           AND (R.REFUND_STATE_ID IN(1) OR (ln_terminal_capture_flag = 'Y' AND R.REFUND_STATE_ID IN(6)))
           AND X.TRAN_STATE_CD IN('S');
        -- Update refunds in batch
        UPDATE PSS.TRAN X
           SET TRAN_STATE_CD =           
           (SELECT AFTER_SETTLE_TRAN_STATE_CD(NULL, R.REFUND_TYPE_CD, pc_auth_result_cd, X.TRAN_DEVICE_RESULT_TYPE_CD)               
            FROM PSS.REFUND R
           WHERE R.TRAN_ID = X.TRAN_ID 
             AND (R.REFUND_STATE_ID IN(1) OR (ln_terminal_capture_flag = 'Y' AND R.REFUND_STATE_ID IN(6))))
         WHERE X.TRAN_ID IN(SELECT R.TRAN_ID 
          FROM PSS.REFUND R 
         WHERE R.TERMINAL_BATCH_ID = pn_terminal_batch_id
           AND (R.REFUND_STATE_ID IN(1) OR (ln_terminal_capture_flag = 'Y' AND R.REFUND_STATE_ID IN(6))))
           AND X.TRAN_STATE_CD IN('S');
        
        -- if settlement successful or permanently declined, close old terminal batch and create new terminal batch
        IF pn_terminal_batch_id IS NOT NULL AND pc_auth_result_cd IN('Y', 'P', 'O', 'R') THEN
            UPDATE PSS.TERMINAL_BATCH
               SET TERMINAL_BATCH_CLOSE_TS = pd_batch_closed_ts
             WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id
             RETURNING TERMINAL_ID INTO ln_terminal_id;
            UPDATE PSS.TERMINAL
               SET TERMINAL_STATE_ID = 3
             WHERE TERMINAL_ID = ln_terminal_id;
        END IF;
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE PREPARE_PENDING_TRAN(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pn_sale_auth_id OUT PSS.AUTH.AUTH_ID%TYPE,
        pn_refund_id OUT PSS.REFUND.REFUND_ID%TYPE,
        pc_sale_phase_cd OUT VARCHAR2,
        pn_prior_attempts OUT PLS_INTEGER,
        pn_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE)
    IS
        lv_tran_lock_cd VARCHAR2(128);
        lv_lock_handle VARCHAR2(128);
        ln_actual_amt PSS.AUTH.AUTH_AMT%TYPE;
        ln_intended_amt PSS.AUTH.AUTH_AMT%TYPE;
        lc_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
        lc_auth_type_cd PSS.AUTH.AUTH_TYPE_CD%TYPE;
        ln_previous_saled_amt PSS.AUTH.AUTH_AMT%TYPE;
        lc_auth_hold_used PSS.AUTH.AUTH_HOLD_USED%TYPE;
        ln_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE;
        lc_terminal_capture_flag PSS.TERMINAL_BATCH.TERMINAL_CAPTURE_FLAG%TYPE;
        ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
        ln_is_open PLS_INTEGER;   
        ln_auth_auth_id PSS.AUTH.AUTH_ID%TYPE;
        ln_allowed_trans PLS_INTEGER;
        lc_new_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
        ln_device_result_type_cd PSS.TRAN.TRAN_DEVICE_RESULT_TYPE_CD%TYPE;
    BEGIN
        SELECT DEVICE_NAME || ':' || TRAN_DEVICE_TRAN_CD, 
               TRAN_STATE_CD,
               NVL(AUTH_HOLD_USED, 'N'),
               TERMINAL_ID,
               NVL(TERMINAL_CAPTURE_FLAG, 'N'),
               AUTH_AUTH_ID,
               SALE_AUTH_ID,
               REFUND_ID,
               TRAN_DEVICE_RESULT_TYPE_CD
          INTO lv_tran_lock_cd,
               lc_tran_state_cd,
               lc_auth_hold_used,
               ln_terminal_id,
               lc_terminal_capture_flag,
               ln_auth_auth_id,
               pn_sale_auth_id,
               pn_refund_id,
               ln_device_result_type_cd
          FROM (SELECT T.DEVICE_NAME, 
                       T.TRAN_DEVICE_TRAN_CD, 
                       A.AUTH_ID AUTH_AUTH_ID,
                       SA.AUTH_ID SALE_AUTH_ID,
                       R.REFUND_ID, 
                       T.TRAN_STATE_CD,
                       T.AUTH_HOLD_USED,
                       TE.TERMINAL_ID,
                       AU.TERMINAL_CAPTURE_FLAG,
                       T.TRAN_DEVICE_RESULT_TYPE_CD
                  FROM PSS.TRAN T
				  LEFT OUTER JOIN PSS.AUTH RA  ON T.TRAN_ID = RA.TRAN_ID AND RA.AUTH_TYPE_CD IN ('C', 'E')
                  LEFT OUTER JOIN PSS.AUTH A   ON T.TRAN_ID = A.TRAN_ID AND A.AUTH_TYPE_CD IN('L', 'N') AND (A.AUTH_RESULT_CD IN('Y', 'P', 'V', 'C', 'M') OR T.TRAN_STATE_CD IN('W') OR RA.AUTH_ID IS NOT NULL)
                  LEFT OUTER JOIN PSS.REFUND R ON T.TRAN_ID = R.TRAN_ID
                  LEFT OUTER JOIN PSS.AUTH SA  ON T.TRAN_ID = SA.TRAN_ID AND SA.AUTH_TYPE_CD IN('U','S','O','A','D','C','E','V','I') AND SA.AUTH_STATE_ID = 6
                  LEFT OUTER JOIN (PSS.TERMINAL TE
                        JOIN PSS.MERCHANT M ON TE.MERCHANT_ID = M.MERCHANT_ID
                        JOIN AUTHORITY.AUTHORITY AU ON M.AUTHORITY_ID = AU.AUTHORITY_ID
                  ) ON T.PAYMENT_SUBTYPE_CLASS NOT IN ('Aramark', 'Authority::NOP', 'BlackBoard', 'Cash') AND T.PAYMENT_SUBTYPE_CLASS NOT LIKE 'Internal%' AND T.PAYMENT_SUBTYPE_KEY_ID = TE.TERMINAL_ID
                 WHERE T.TRAN_ID = pn_tran_id
                 ORDER BY A.AUTH_RESULT_CD DESC, A.AUTH_TS DESC, A.AUTH_ID DESC, SA.AUTH_RESULT_CD DESC, SA.AUTH_TS DESC, SA.AUTH_ID DESC, R.CREATED_TS DESC, R.REFUND_ID DESC, RA.AUTH_RESULT_CD DESC, RA.AUTH_TS DESC, RA.AUTH_ID DESC)
         WHERE ROWNUM = 1;
        IF NOT(lc_tran_state_cd IN('8', 'I', 'W')
            OR (lc_tran_state_cd IN('9') AND lc_auth_hold_used != 'Y' AND lc_terminal_capture_flag != 'Y')) THEN
            pc_sale_phase_cd := '-'; -- Do nothing
            pn_prior_attempts := -1;
            RETURN;
        END IF;
        -- Thus, "lc_tran_state_cd IN('8', 'I', 'W') OR (lc_tran_state_cd IN('9') AND lc_auth_hold_used != 'Y' AND lc_terminal_capture_flag != 'Y')"
		lc_new_tran_state_cd := lc_tran_state_cd;
        -- Lock transaction
        lv_lock_handle := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', lv_tran_lock_cd);
        IF ln_auth_auth_id IS NOT NULL THEN -- it's a sale or a reversal
            IF lc_terminal_capture_flag = 'Y' AND lc_tran_state_cd != '9' THEN
                -- get the open terminal batch
                IF pn_terminal_batch_id IS NULL THEN
                	ln_terminal_batch_id := GET_AVAILABLE_TERMINAL_BATCH(ln_terminal_id, ln_allowed_trans, TRUE);
                ELSE
                    ln_terminal_batch_id := pn_terminal_batch_id;
                END IF;
                SELECT NVL(SUM(DECODE(TLI.TRAN_LINE_ITEM_BATCH_TYPE_CD, 'A', TLI.TRAN_LINE_ITEM_AMOUNT * TLI.TRAN_LINE_ITEM_QUANTITY)), 0) ACTUAL_AMT
                  INTO ln_actual_amt
                  FROM PSS.TRAN_LINE_ITEM TLI
                 WHERE TLI.TRAN_ID = pn_tran_id;
                pc_sale_phase_cd := '-'; -- No further processing
                IF ln_actual_amt != 0 THEN
                    lc_auth_type_cd := 'U'; -- pre-auth sale
                    lc_new_tran_state_cd := 'T'; -- Processed Server Batch
                ELSIF lc_auth_hold_used = 'Y' THEN
                     lc_auth_type_cd := 'C'; -- Auth Reversal
                     lc_new_tran_state_cd := 'T'; -- Processed Server Batch
                ELSE
                    -- do not create sale auth record
                    lc_new_tran_state_cd := 'C'; -- client cancelled
                END IF;
            ELSIF lc_tran_state_cd = 'W' THEN
                pc_sale_phase_cd := 'R'; -- Reversal
                ln_actual_amt := 0;
                lc_auth_type_cd := 'C'; -- Auth reversal
                lc_new_tran_state_cd := 'A'; -- processing tran
            ELSE
                SELECT NVL(SUM(DECODE(TLI.TRAN_LINE_ITEM_BATCH_TYPE_CD, 'A', TLI.TRAN_LINE_ITEM_AMOUNT * TLI.TRAN_LINE_ITEM_QUANTITY)), 0) ACTUAL_AMT,
                       NVL(SUM(DECODE(TLI.TRAN_LINE_ITEM_BATCH_TYPE_CD, 'I', TLI.TRAN_LINE_ITEM_AMOUNT * TLI.TRAN_LINE_ITEM_QUANTITY)), 0) INTENDED_AMT,
                       T.TRAN_STATE_CD -- get this again now that transaction is locked
                  INTO ln_actual_amt, ln_intended_amt, lc_tran_state_cd
                  FROM PSS.TRAN_LINE_ITEM TLI
                 RIGHT OUTER JOIN PSS.TRAN T ON T.TRAN_ID = TLI.TRAN_ID
                 WHERE T.TRAN_ID = pn_tran_id 
                 GROUP BY T.TRAN_STATE_CD;
                IF lc_tran_state_cd = '9' THEN
                    IF ln_actual_amt > 0 THEN -- sanity check
                        RAISE_APPLICATION_ERROR(-20555, 'Invalid tran_state_cd for tran ' || pn_tran_id || ': Intended batch with Actual Amount');
                    ELSIF ln_intended_amt > 0 THEN
                        pc_sale_phase_cd := 'I'; -- needs processing of intended sale
                        ln_actual_amt := NULL; -- Use Intended Amt
                        lc_auth_type_cd := 'S'; -- network sale
                        lc_new_tran_state_cd := 'A'; -- processing tran
                    ELSE -- we can assume lc_auth_hold_used != 'Y' or it would not have gotten here
                        pc_sale_phase_cd := '-'; -- no processing needed
                        -- do not create sale auth record
                        lc_new_tran_state_cd := 'U'; -- processed_server_tran_intended
                    END IF;             
                ELSE
                    SELECT NVL(MAX(AUTH_AMT), 0)
                      INTO ln_previous_saled_amt
                      FROM (SELECT /*+ index(AT IDX_AUTH_TRAN_ID) */ AT.AUTH_AMT
                              FROM PSS.AUTH AT
                             WHERE AT.TRAN_ID = pn_tran_id
                               AND AT.AUTH_TYPE_CD IN('U', 'S')
                               AND AT.AUTH_STATE_ID IN(2)
                             ORDER BY AT.AUTH_TS DESC, AT.AUTH_ID DESC)
                     WHERE ROWNUM = 1;
                    IF ln_previous_saled_amt != ln_actual_amt THEN
                        pc_sale_phase_cd := 'A'; -- needs processing of actual sale
                        IF ln_actual_amt = 0 THEN -- had previous amount, but now cancelled
                            lc_auth_type_cd := 'E'; -- Sale Reversal
                        ELSIF ln_previous_saled_amt > 0 THEN
                            lc_auth_type_cd := 'D'; -- Sale Adjustment
                        ELSIF lc_auth_hold_used = 'Y' THEN                    
                            lc_auth_type_cd := 'U'; -- Pre-Authed Sale
                        ELSE
                            lc_auth_type_cd := 'S'; -- Sale
                        END IF;
                        lc_new_tran_state_cd := 'A'; -- processing tran
                    ELSIF ln_actual_amt = 0 THEN -- previous amount = actual amount = 0
                        IF ln_intended_amt > 0 AND lc_tran_state_cd != '8' THEN
                            -- This may have been an intended sale that failed, in which case we need to retry the intended amt
                            SELECT SALE_TYPE_CD
                              INTO pc_sale_phase_cd
                              FROM PSS.SALE
                             WHERE TRAN_ID = pn_tran_id;
                            IF pc_sale_phase_cd = 'I' THEN -- needs re-processing of intended sale
                                ln_actual_amt := NULL; -- Use Intended Amt
                                lc_auth_type_cd := 'S'; -- network sale
                                lc_new_tran_state_cd := 'A'; -- processing tran
                            ELSIF lc_auth_hold_used = 'Y' THEN
                                pc_sale_phase_cd := 'A'; -- Actual
                                lc_auth_type_cd := 'C'; -- Auth reversal
                                lc_new_tran_state_cd := 'A'; -- processing tran
                            ELSE
                                pc_sale_phase_cd := '-'; -- no processing needed
                                -- do not create sale auth record
                                lc_new_tran_state_cd := 'C'; -- client cancelled
                            END IF;
                        ELSIF lc_auth_hold_used = 'Y' THEN
                            IF lc_tran_state_cd IN('I') AND ln_device_result_type_cd IS NULL THEN
                                pc_sale_phase_cd := 'R'; -- Reversal
                            ELSE
                                pc_sale_phase_cd := 'A'; -- Actual
                            END IF;
                            lc_auth_type_cd := 'C'; -- Auth reversal
                            lc_new_tran_state_cd := 'A'; -- processing tran
                        ELSE
                            pc_sale_phase_cd := '-'; -- no processing needed
                            -- do not create sale auth record
                            lc_new_tran_state_cd := 'C'; -- client cancelled
                        END IF;
                    ELSE
                        pc_sale_phase_cd := '-'; -- no processing needed
                        -- do not create sale auth record
                        lc_new_tran_state_cd := 'D'; -- tran complete
                    END IF;
                END IF;
                IF pc_sale_phase_cd != '-' THEN
                    SELECT COUNT(*)
                      INTO pn_prior_attempts
                      FROM PSS.AUTH A
                     WHERE A.TRAN_ID = pn_tran_id
                       AND A.AUTH_TYPE_CD IN ('S','I','U','V')
                       AND A.AUTH_STATE_ID IN(3, 4);  	
                END IF;
            END IF;
            IF pn_sale_auth_id IS NULL AND lc_auth_type_cd IS NOT NULL THEN
                SELECT PSS.SEQ_AUTH_ID.NEXTVAL
                  INTO pn_sale_auth_id
                  FROM DUAL;
                INSERT INTO PSS.AUTH (
                        AUTH_ID,
                        TRAN_ID,
                        AUTH_TYPE_CD,
                        AUTH_STATE_ID,
                        AUTH_PARSED_ACCT_DATA,
                        ACCT_ENTRY_METHOD_CD,
                        AUTH_AMT,
                        AUTH_TS,
                        TERMINAL_BATCH_ID,
                        TRACE_NUMBER)
                 SELECT pn_sale_auth_id,
                        pn_tran_id,
                        lc_auth_type_cd,
                        6,
                        a.AUTH_PARSED_ACCT_DATA,
                        a.ACCT_ENTRY_METHOD_CD,
                        NVL(ln_actual_amt, ln_intended_amt),	    
                        SYSDATE,
                        ln_terminal_batch_id,
                        a.TRACE_NUMBER
                   FROM PSS.AUTH a
                  WHERE a.AUTH_ID = ln_auth_auth_id;
            END IF;
        ELSIF pn_refund_id IS NOT NULL THEN -- it's a refund
            IF lc_terminal_capture_flag = 'Y' THEN
                IF pn_terminal_batch_id IS NULL THEN
                	ln_terminal_batch_id := GET_AVAILABLE_TERMINAL_BATCH(ln_terminal_id, ln_allowed_trans, TRUE);
                ELSE
                    ln_terminal_batch_id := pn_terminal_batch_id;
                END IF;
                -- stick it in the open terminal batch
                UPDATE PSS.REFUND
                   SET TERMINAL_BATCH_ID = ln_terminal_batch_id,
                       REFUND_STATE_ID = 6
                 WHERE REFUND_ID = pn_refund_id;
                pc_sale_phase_cd := '-'; -- No further processing
                lc_new_tran_state_cd := 'T'; -- Processed Server Batch
            ELSE
                -- We could check the actual amount and the previous refunded amount for sanity, but that seems overkill. 
                -- Just assume actual amount > 0 and previous refunded amount = 0
                -- Also, assume REFUND_STATE_ID IN(6, 2, 3)
                pc_sale_phase_cd := 'A'; -- needs processing of actual sale
                lc_new_tran_state_cd := 'A'; -- processing tran
                SELECT COUNT(*)
                  INTO pn_prior_attempts
                  FROM PSS.REFUND R
                 WHERE R.TRAN_ID = pn_tran_id
                   AND R.REFUND_TYPE_CD IN ('G','C','V','S')
                   AND R.REFUND_STATE_ID IN(2, 3);
            END IF;
        ELSE -- neither authId nor refundId is found, set to error
            pc_sale_phase_cd := '-'; -- No further processing
            lc_new_tran_state_cd := 'E'; -- Complete Error
        END IF;
		IF lc_new_tran_state_cd != lc_tran_state_cd THEN
			UPDATE PSS.TRAN 
			   SET TRAN_STATE_CD = lc_new_tran_state_cd
			 WHERE TRAN_ID = pn_tran_id;
		END IF;
    END;
    
    -- R37 and above signature
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE UPDATE_PROCESSED_TRAN(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pn_sale_auth_id PSS.AUTH.AUTH_ID%TYPE,
        pn_refund_id PSS.REFUND.REFUND_ID%TYPE,
        pc_settled_flag CHAR,
        pc_sale_phase_cd CHAR,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
        pn_auth_approved_amt PSS.AUTH.AUTH_AMT_APPROVED%TYPE,
        pv_authority_response_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
        pv_authority_response_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
        pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
        pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
        pd_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
        pc_ignore_reprocess_flag CHAR,
        pv_tran_global_trans_cd OUT PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
        pv_auth_authority_tran_cd OUT PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
        pc_settle_update_needed OUT VARCHAR2,
        pv_source_system_cd OUT VARCHAR2,
        pc_auth_type_cd OUT VARCHAR2,
        pn_override_trans_type_id OUT PSS.PAYMENT_SUBTYPE.TRANS_TYPE_ID%TYPE,
        pc_partial_reversal_flag PSS.AUTH.PARTIAL_REVERSAL_FLAG%TYPE DEFAULT NULL)
    IS
        lv_tran_lock_cd VARCHAR2(128);
        lv_lock_handle VARCHAR2(128);
        ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
        lc_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
        ln_payment_subtype_key_id PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE;
        lv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE;
        ln_allowed_trans PLS_INTEGER;
        ln_auth_auth_id PSS.AUTH.AUTH_ID%TYPE;
        lc_auth_hold_used PSS.AUTH.AUTH_HOLD_USED%TYPE;
        lc_imported PSS.SALE.IMPORTED%TYPE;
        lc_sale_type_cd PSS.SALE.SALE_TYPE_CD%TYPE;
        ln_sale_result_id PSS.SALE.SALE_RESULT_ID%TYPE;
        pc_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
        lc_match_flag CHAR(1);
    BEGIN
		pc_settle_update_needed := 'Y';
        SELECT T.DEVICE_NAME || ':' || T.TRAN_DEVICE_TRAN_CD, T.PAYMENT_SUBTYPE_CLASS, T.PAYMENT_SUBTYPE_KEY_ID, T.TRAN_STATE_CD, T.TRAN_GLOBAL_TRANS_CD, AA.AUTH_AUTHORITY_TRAN_CD, AA.AUTH_ID, AA.AUTH_HOLD_USED, NVL(RT.SOURCE_SYSTEM_CD, 'PSS')
          INTO lv_tran_lock_cd, lv_payment_subtype_class, ln_payment_subtype_key_id, lc_tran_state_cd, pv_tran_global_trans_cd, pv_auth_authority_tran_cd, ln_auth_auth_id, lc_auth_hold_used, pv_source_system_cd
          FROM PSS.TRAN T
          LEFT OUTER JOIN PSS.AUTH AA ON T.TRAN_ID = AA.TRAN_ID AND AA.AUTH_TYPE_CD = 'N' AND (AA.AUTH_STATE_ID IN(2, 5, 6) OR (AA.AUTH_STATE_ID IN(3, 4) AND AA.AUTH_TYPE_CD = 'N' AND AA.AUTH_HOLD_USED = 'Y'))
		  LEFT OUTER JOIN REPORT.TRANS RT ON T.TRAN_GLOBAL_TRANS_CD = RT.MACHINE_TRANS_NO
         WHERE T.TRAN_ID = pn_tran_id;
        IF lc_tran_state_cd != 'A' THEN
            IF pc_ignore_reprocess_flag = 'Y' THEN
                IF pn_sale_auth_id IS NOT NULL THEN
                    SELECT AUTH_TYPE_CD, OVERRIDE_TRANS_TYPE_ID, CASE WHEN AUTH_RESULT_CD = pc_auth_result_cd 
                        AND (AUTH_RESP_CD = pv_authority_response_cd OR (AUTH_RESP_CD IS NULL AND pv_authority_response_cd IS NULL))
                        AND (AUTH_RESP_DESC = pv_authority_response_desc OR (AUTH_RESP_DESC IS NULL AND pv_authority_response_desc IS NULL))
                        AND (AUTH_AUTHORITY_TRAN_CD = pv_authority_tran_cd OR (AUTH_AUTHORITY_TRAN_CD IS NULL AND pv_authority_tran_cd IS NULL))
                        AND (AUTH_AUTHORITY_REF_CD = pv_authority_ref_cd OR (AUTH_AUTHORITY_REF_CD IS NULL AND pv_authority_ref_cd IS NULL))
                        AND AUTH_AUTHORITY_TS = pd_authority_ts
                        AND (AUTH_AMT_APPROVED = pn_auth_approved_amt / pn_minor_currency_factor OR (AUTH_AMT_APPROVED IS NULL AND pn_auth_approved_amt IS NULL))
                        AND (AUTH_AUTHORITY_MISC_DATA = pv_authority_misc_data OR (AUTH_AUTHORITY_MISC_DATA IS NULL AND pv_authority_misc_data IS NULL)) THEN 'Y' ELSE 'N' END
                      INTO pc_auth_type_cd, pn_override_trans_type_id, lc_match_flag
                      FROM PSS.AUTH 
                     WHERE AUTH_ID = pn_sale_auth_id;
                ELSIF pn_refund_id IS NOT NULL THEN
                    SELECT REFUND_TYPE_CD, OVERRIDE_TRANS_TYPE_ID, CASE WHEN REFUND_STATE_ID = DECODE(pc_auth_result_cd, 'Y', 1, 'P', 1, 'N', 2, 'O', 2, 'F', 3, 'R', 2)
                        AND (REFUND_RESP_CD = pv_authority_response_cd OR (REFUND_RESP_CD IS NULL AND pv_authority_response_cd IS NULL))
                        AND (REFUND_RESP_DESC = pv_authority_response_desc OR (REFUND_RESP_DESC IS NULL AND pv_authority_response_desc IS NULL))
                        AND (REFUND_AUTHORITY_TRAN_CD = pv_authority_tran_cd OR (REFUND_AUTHORITY_TRAN_CD IS NULL AND pv_authority_tran_cd IS NULL))
                        AND (REFUND_AUTHORITY_REF_CD = pv_authority_ref_cd OR (REFUND_AUTHORITY_REF_CD IS NULL AND pv_authority_ref_cd IS NULL))
                        AND REFUND_AUTHORITY_TS = pd_authority_ts
                        AND (REFUND_AUTHORITY_MISC_DATA = pv_authority_misc_data OR (REFUND_AUTHORITY_MISC_DATA IS NULL AND pv_authority_misc_data IS NULL)) THEN 'Y' ELSE 'N' END
                      INTO pc_auth_type_cd, pn_override_trans_type_id, lc_match_flag
                      FROM PSS.REFUND
                     WHERE REFUND_ID = pn_refund_id;               
                END IF;
                IF lc_match_flag = 'Y' THEN
                    RETURN;
                END IF;
            END IF;
            RAISE_APPLICATION_ERROR(-20554, 'Invalid tran_state_cd for tran ' || pn_tran_id || ' when updating processed tran');      
        END IF;
        IF NVL(pc_settled_flag, '-') != 'Y' AND pc_auth_result_cd IN('Y', 'P') AND (lv_payment_subtype_class  LIKE 'Authority::ISO8583%') THEN
            -- If this is a retry of a failure, it may already have a terminal_batch assigned
            IF pn_sale_auth_id IS NOT NULL THEN
                SELECT MAX(TERMINAL_BATCH_ID)
                  INTO ln_terminal_batch_id
                  FROM PSS.AUTH
                 WHERE TRAN_ID = pn_tran_id;
            ELSIF pn_refund_id IS NOT NULL THEN
                SELECT MAX(TERMINAL_BATCH_ID)
                  INTO ln_terminal_batch_id
                  FROM PSS.REFUND
                 WHERE TRAN_ID = pn_tran_id;
            END IF;
            IF ln_terminal_batch_id IS NULL THEN
                ln_terminal_batch_id := GET_AVAILABLE_TERMINAL_BATCH(ln_payment_subtype_key_id, ln_allowed_trans, TRUE);
                IF ln_allowed_trans <= 0 THEN
                    RAISE_APPLICATION_ERROR(-20556, 'Terminal Batch ' || ln_terminal_batch_id || ' is full for terminal ' || ln_payment_subtype_key_id || '; Can not add tran ' || pn_tran_id || ' to it');
                END IF;
            END IF;
        END IF;
        -- Lock transaction
        lv_lock_handle := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', lv_tran_lock_cd);
        IF pn_sale_auth_id IS NOT NULL THEN
            UPDATE PSS.AUTH 
               SET (
                AUTH_STATE_ID,
                AUTH_RESULT_CD,
                AUTH_RESP_CD,
                AUTH_RESP_DESC,
                AUTH_AUTHORITY_TRAN_CD,
                AUTH_AUTHORITY_REF_CD,
                AUTH_AUTHORITY_TS,
                TERMINAL_BATCH_ID,
                AUTH_AMT_APPROVED,
                AUTH_AUTHORITY_MISC_DATA,
                PARTIAL_REVERSAL_FLAG) =
            (SELECT 
                DECODE(pc_auth_result_cd, 'Y', 2, 'P', 5, 'N', 3, 'O', 7, 'F', 4, 'R', 7),
                pc_auth_result_cd,
                pv_authority_response_cd,
                pv_authority_response_desc,
                pv_authority_tran_cd,
                pv_authority_ref_cd,
                pd_authority_ts,
                ln_terminal_batch_id,
                pn_auth_approved_amt / pn_minor_currency_factor,
                pv_authority_misc_data,
                pc_partial_reversal_flag
             FROM DUAL)
             WHERE AUTH_ID = pn_sale_auth_id
             RETURNING AUTH_TYPE_CD, OVERRIDE_TRANS_TYPE_ID INTO pc_auth_type_cd, pn_override_trans_type_id;
            IF lc_auth_hold_used = 'Y' AND pc_auth_result_cd IN('O', 'R') THEN
                DELETE 
                  FROM PSS.CONSUMER_ACCT_AUTH_HOLD
                 WHERE AUTH_ID = ln_auth_auth_id;
            END IF;
        ELSIF pn_refund_id IS NOT NULL THEN
           UPDATE PSS.REFUND
               SET REFUND_STATE_ID = DECODE(pc_auth_result_cd, 'Y', 1, 'P', 1, 'N', 2, 'O', 2, 'F', 3, 'R', 2),
                REFUND_RESP_CD = NVL(pv_authority_response_cd, REFUND_RESP_CD),
                REFUND_RESP_DESC = NVL(pv_authority_response_desc, REFUND_RESP_DESC),
                REFUND_AUTHORITY_TRAN_CD = NVL(pv_authority_tran_cd, REFUND_AUTHORITY_TRAN_CD),
                REFUND_AUTHORITY_REF_CD = NVL(pv_authority_ref_cd, REFUND_AUTHORITY_REF_CD),
                REFUND_AUTHORITY_TS = NVL(pd_authority_ts, REFUND_AUTHORITY_TS),
                TERMINAL_BATCH_ID = NVL(ln_terminal_batch_id, TERMINAL_BATCH_ID),
                REFUND_AUTHORITY_MISC_DATA = NVL(pv_authority_misc_data, REFUND_AUTHORITY_MISC_DATA)
            WHERE REFUND_ID = pn_refund_id
            RETURNING REFUND_TYPE_CD, OVERRIDE_TRANS_TYPE_ID INTO pc_auth_type_cd, pn_override_trans_type_id;
        END IF;
        
        -- update tran_state_cd
        IF pc_auth_result_cd IN('F') THEN
            lc_tran_state_cd := 'J'; -- Incomplete Error: Manual intervention needed
        ELSIF pc_auth_result_cd IN('O', 'R') THEN
            IF pc_sale_phase_cd = 'R' THEN -- it was an auth reversal
                lc_tran_state_cd := 'V';
            ELSIF pc_auth_type_cd IN('C', 'E') THEN -- sale reversal or auth reversal (indicates actual amount = 0)
                lc_tran_state_cd := 'C'; -- Client Cancelled
            ELSIF pc_sale_phase_cd = 'I' THEN
                -- check If actuals have since been uploaded
                SELECT DECODE(SALE_TYPE_CD, 'I', 'K', 'A', '8')
                  INTO lc_tran_state_cd
                  FROM PSS.SALE
                WHERE TRAN_ID = pn_tran_id;         
            ELSE
                lc_tran_state_cd := 'E'; -- Complete Error: don't retry
            END IF;
        ELSIF pc_auth_result_cd IN('N') THEN
            lc_tran_state_cd := 'I'; -- Incomplete: will retry
        ELSIF pc_auth_result_cd IN('Y', 'P') THEN
            IF pc_sale_phase_cd = 'R' THEN -- it was an auth reversal
                IF pc_settled_flag = 'Y' THEN
                    lc_tran_state_cd := 'V';
                ELSE
                     lc_tran_state_cd := 'T';
                END IF;
            ELSIF pc_sale_phase_cd = 'I' THEN
                -- check If actuals have since been uploaded
                SELECT DECODE(SALE_TYPE_CD, 'I', 'U', 'A', '8')
                  INTO lc_tran_state_cd
                  FROM PSS.SALE
                WHERE TRAN_ID = pn_tran_id;
            ELSIF pc_settled_flag = 'Y' THEN
                IF pc_auth_type_cd IN('C', 'E') THEN -- sale reversal or auth reversal (indicates actual amount = 0)
                    lc_tran_state_cd := 'C'; -- Client Cancelled
                ELSE
                    lc_tran_state_cd := 'D'; -- Complete
                END IF;
            ELSE
                lc_tran_state_cd := 'T'; -- Processed Server Batch
            END IF;
        ELSE
            RAISE_APPLICATION_ERROR(-20557, 'Invalid auth result cd "' ||  pc_auth_result_cd + '" for tran ' || pn_tran_id);
        END IF;
        UPDATE PSS.TRAN
           SET TRAN_STATE_CD = lc_tran_state_cd
         WHERE TRAN_ID = pn_tran_id;
        select max(s.imported), max(CASE WHEN pn_refund_id IS NOT NULL THEN 'R' ELSE s.SALE_TYPE_CD END), max(CASE WHEN pn_refund_id IS NOT NULL THEN 0 ELSE s.sale_result_id END), max(x.tran_state_cd) 
        into lc_imported,lc_sale_type_cd,ln_sale_result_id,pc_tran_state_cd
        from pss.tran x left outer join pss.sale s on x.tran_id=s.tran_id 
        where x.tran_id=pn_tran_id;
        IF  (lc_imported is NULL or lc_imported != '-') AND lc_sale_type_cd IN(PKG_CONST.SALE_TYPE__ACTUAL, PKG_CONST.SALE_TYPE__CASH, 'R') AND ln_sale_result_id = 0 AND pc_tran_state_cd NOT IN('F', 'G', 'Z') THEN
            pc_settle_update_needed := 'Y';
        ELSE
            pc_settle_update_needed := 'N';
        END IF;
    END;      

    FUNCTION GET_TERMINAL_GLOBAL_TOKEN(
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_payment_subtype_key_id NUMBER)
    RETURN VARCHAR2
    IS
        lv_global_token PSS.TERMINAL.GLOBAL_TOKEN_CD%TYPE;
    BEGIN
        IF pv_payment_subtype_class IN('Authority::NOP', 'Cash') THEN
            RAISE_APPLICATION_ERROR(-20865, 'Payment subtype class ''' || pv_payment_subtype_class || ''' does not have terminals');
        ELSIF pv_payment_subtype_class = 'Aramark' THEN
            SELECT GLOBAL_TOKEN_CD
              INTO lv_global_token
              FROM PSS.ARAMARK_PAYMENT_TYPE
             WHERE ARAMARK_PAYMENT_TYPE_ID = pn_payment_subtype_key_id;
        ELSIF pv_payment_subtype_class = 'BlackBoard' THEN
            SELECT GLOBAL_TOKEN_CD
              INTO lv_global_token
              FROM PSS.BLACKBRD_TENDER
             WHERE BLACKBRD_TENDER_ID = pn_payment_subtype_key_id;
        ELSIF pv_payment_subtype_class LIKE 'Internal%' THEN
            SELECT GLOBAL_TOKEN_CD
              INTO lv_global_token
              FROM PSS.INTERNAL_PAYMENT_TYPE
             WHERE INTERNAL_PAYMENT_TYPE_ID = pn_payment_subtype_key_id;
        ELSE
            SELECT GLOBAL_TOKEN_CD
              INTO lv_global_token
              FROM PSS.TERMINAL
             WHERE TERMINAL_ID = pn_payment_subtype_key_id;
        END IF;
        RETURN lv_global_token;
    END;
    
    PROCEDURE UPDATE_TERMINAL_GLOBAL_TOKEN(
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_payment_subtype_key_id NUMBER,
        pv_old_global_token PSS.TERMINAL.GLOBAL_TOKEN_CD%TYPE,
        pv_new_global_token PSS.TERMINAL.GLOBAL_TOKEN_CD%TYPE,
        pn_update_count OUT PLS_INTEGER,
        pc_current_must_match CHAR DEFAULT 'N')
    IS
		ln_term_lock_max_duration_sec NUMBER := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('POSM_TERMINAL_LOCK_MAX_DURATION_SEC'));
		ld_sysdate DATE := SYSDATE;
		ln_forced_unlock_count NUMBER := 0;
    BEGIN
        IF pv_new_global_token IS NULL THEN
            RAISE_APPLICATION_ERROR(-20864, 'Global Token Code may not be null');
        ELSIF pv_payment_subtype_class IN('Authority::NOP', 'Cash') THEN
            RAISE_APPLICATION_ERROR(-20865, 'Payment subtype class ''' || pv_payment_subtype_class || ''' does not have terminals');
        ELSIF pv_payment_subtype_class = 'Aramark' THEN
            UPDATE PSS.ARAMARK_PAYMENT_TYPE
               SET GLOBAL_TOKEN_CD = pv_new_global_token
             WHERE ARAMARK_PAYMENT_TYPE_ID = pn_payment_subtype_key_id
               AND ((pc_current_must_match = 'N' AND GLOBAL_TOKEN_CD IS NULL) OR GLOBAL_TOKEN_CD = pv_old_global_token);
            pn_update_count := SQL%ROWCOUNT;
			IF pn_update_count = 0 THEN
				UPDATE PSS.ARAMARK_PAYMENT_TYPE
				   SET GLOBAL_TOKEN_CD = pv_new_global_token
				 WHERE ARAMARK_PAYMENT_TYPE_ID = pn_payment_subtype_key_id
					AND GLOBAL_TOKEN_CD IS NOT NULL
					AND LAST_UPDATED_TS < ld_sysdate - ln_term_lock_max_duration_sec/86400;
				pn_update_count := SQL%ROWCOUNT;
				ln_forced_unlock_count := pn_update_count;
			END IF;
        ELSIF pv_payment_subtype_class = 'BlackBoard' THEN
            UPDATE PSS.BLACKBRD_TENDER
               SET GLOBAL_TOKEN_CD = pv_new_global_token
             WHERE BLACKBRD_TENDER_ID = pn_payment_subtype_key_id
               AND ((pc_current_must_match = 'N' AND GLOBAL_TOKEN_CD IS NULL) OR GLOBAL_TOKEN_CD = pv_old_global_token);
            pn_update_count := SQL%ROWCOUNT;
			IF pn_update_count = 0 THEN
				UPDATE PSS.BLACKBRD_TENDER
				   SET GLOBAL_TOKEN_CD = pv_new_global_token
				 WHERE BLACKBRD_TENDER_ID = pn_payment_subtype_key_id
					AND GLOBAL_TOKEN_CD IS NOT NULL
					AND LAST_UPDATED_TS < ld_sysdate - ln_term_lock_max_duration_sec/86400;
				pn_update_count := SQL%ROWCOUNT;
				ln_forced_unlock_count := pn_update_count;
			END IF;
        ELSIF pv_payment_subtype_class LIKE 'Internal%' THEN
            UPDATE PSS.INTERNAL_PAYMENT_TYPE
               SET GLOBAL_TOKEN_CD = pv_new_global_token
             WHERE INTERNAL_PAYMENT_TYPE_ID = pn_payment_subtype_key_id
               AND ((pc_current_must_match = 'N' AND GLOBAL_TOKEN_CD IS NULL) OR GLOBAL_TOKEN_CD = pv_old_global_token);
            pn_update_count := SQL%ROWCOUNT;
			IF pn_update_count = 0 THEN
				UPDATE PSS.INTERNAL_PAYMENT_TYPE
				   SET GLOBAL_TOKEN_CD = pv_new_global_token
				 WHERE INTERNAL_PAYMENT_TYPE_ID = pn_payment_subtype_key_id
					AND GLOBAL_TOKEN_CD IS NOT NULL
					AND LAST_UPDATED_TS < ld_sysdate - ln_term_lock_max_duration_sec/86400;
				pn_update_count := SQL%ROWCOUNT;
				ln_forced_unlock_count := pn_update_count;
			END IF;
        ELSE
            UPDATE PSS.TERMINAL
               SET GLOBAL_TOKEN_CD = pv_new_global_token,
                   TERMINAL_STATE_ID =  DECODE(TERMINAL_STATE_ID, 5, 5, 3) -- terminal is busy or settlement retry now
             WHERE TERMINAL_ID = pn_payment_subtype_key_id
               AND ((pc_current_must_match = 'N' AND GLOBAL_TOKEN_CD IS NULL AND TERMINAL_STATE_ID IN(1, 5))
                OR (GLOBAL_TOKEN_CD = pv_old_global_token AND TERMINAL_STATE_ID IN(3, 5)));
            pn_update_count := SQL%ROWCOUNT;
			IF pn_update_count = 0 THEN
				UPDATE PSS.TERMINAL
				   SET GLOBAL_TOKEN_CD = pv_new_global_token,
					   TERMINAL_STATE_ID =  DECODE(TERMINAL_STATE_ID, 5, 5, 3) -- terminal is busy or settlement retry now
				 WHERE TERMINAL_ID = pn_payment_subtype_key_id
					AND GLOBAL_TOKEN_CD IS NOT NULL
					AND LAST_UPDATED_TS < ld_sysdate - ln_term_lock_max_duration_sec/86400;
				pn_update_count := SQL%ROWCOUNT;
				ln_forced_unlock_count := pn_update_count;
			END IF;
        END IF;
		
		IF ln_forced_unlock_count > 0 THEN
			pkg_exception_processor.sp_log_exception (
				pkg_app_exec_hist_globals.unknown_error_id,
				'POSM terminal ' || pn_payment_subtype_key_id || '(' || pv_payment_subtype_class || ') was locked for more than ' || ln_term_lock_max_duration_sec || ' seconds, forcefully unlocked',
				NULL,
				'PSS.PKG_SETTLEMENT.UPDATE_TERMINAL_GLOBAL_TOKEN'
			);
		END IF;
    END;
    
    PROCEDURE CLEAR_TERMINAL_GLOBAL_TOKEN(
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_payment_subtype_key_id NUMBER,
        pv_global_token PSS.TERMINAL.GLOBAL_TOKEN_CD%TYPE,
        pn_update_count OUT PLS_INTEGER)
    IS
    BEGIN
        IF pv_payment_subtype_class IN('Authority::NOP', 'Cash') THEN
            RAISE_APPLICATION_ERROR(-20865, 'Payment subtype class ''' || pv_payment_subtype_class || ''' does not have terminals');
        ELSIF pv_payment_subtype_class = 'Aramark' THEN
            UPDATE PSS.ARAMARK_PAYMENT_TYPE
               SET GLOBAL_TOKEN_CD = NULL,
                   LAST_RELEASED_UTC_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP)
             WHERE ARAMARK_PAYMENT_TYPE_ID = pn_payment_subtype_key_id
               AND GLOBAL_TOKEN_CD = pv_global_token;
            pn_update_count := SQL%ROWCOUNT;
        ELSIF pv_payment_subtype_class = 'BlackBoard' THEN
            UPDATE PSS.BLACKBRD_TENDER
               SET GLOBAL_TOKEN_CD = NULL,
                   LAST_RELEASED_UTC_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP)
             WHERE BLACKBRD_TENDER_ID = pn_payment_subtype_key_id
               AND GLOBAL_TOKEN_CD = pv_global_token;
            pn_update_count := SQL%ROWCOUNT;
        ELSIF pv_payment_subtype_class LIKE 'Internal%' THEN
            UPDATE PSS.INTERNAL_PAYMENT_TYPE
               SET GLOBAL_TOKEN_CD = NULL,
                   LAST_RELEASED_UTC_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP)
             WHERE INTERNAL_PAYMENT_TYPE_ID = pn_payment_subtype_key_id
               AND GLOBAL_TOKEN_CD = pv_global_token;
            pn_update_count := SQL%ROWCOUNT;
        ELSE
            UPDATE PSS.TERMINAL
               SET GLOBAL_TOKEN_CD = NULL,
                   TERMINAL_STATE_ID = DECODE(TERMINAL_STATE_ID, 5, 5, 1), -- terminal is active or settlement retry now
                   LAST_RELEASED_UTC_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP)
             WHERE TERMINAL_ID = pn_payment_subtype_key_id
               AND GLOBAL_TOKEN_CD = pv_global_token;
            pn_update_count := SQL%ROWCOUNT;
        END IF;
    END;

    FUNCTION ADD_ADMIN_CMD(
        pv_payment_subtype_class PSS.ADMIN_CMD.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_payment_subtype_key_id PSS.ADMIN_CMD.PAYMENT_SUBTYPE_KEY_ID%TYPE,
        pn_admin_cmd_type_id PSS.ADMIN_CMD.ADMIN_CMD_TYPE_ID%TYPE,
        pv_requested_by PSS.ADMIN_CMD.REQUESTED_BY%TYPE,
        pn_priority PSS.ADMIN_CMD.PRIORITY%TYPE)
    RETURN PSS.ADMIN_CMD.ADMIN_CMD_ID%TYPE
    IS
        ln_admin_cmd_id PSS.ADMIN_CMD.ADMIN_CMD_ID%TYPE;
    BEGIN
        SELECT PSS.SEQ_ADMIN_CMD_ID.NEXTVAL
          INTO ln_admin_cmd_id
          FROM DUAL;
        INSERT INTO PSS.ADMIN_CMD (
                ADMIN_CMD_ID,
                ADMIN_CMD_TYPE_ID,
                PAYMENT_SUBTYPE_KEY_ID,
                PAYMENT_SUBTYPE_CLASS,
                REQUESTED_BY,
                PRIORITY) 
         VALUES(ln_admin_cmd_id, 
                pn_admin_cmd_type_id, 
                pn_payment_subtype_key_id,
                pv_payment_subtype_class,
                pv_requested_by,
                pn_priority);
         RETURN ln_admin_cmd_id;
    END;
    
    FUNCTION ADD_ADMIN_CMD_PARAM(
        pn_admin_cmd_id PSS.ADMIN_CMD_PARAM.ADMIN_CMD_ID%TYPE,
        pv_param_name PSS.ADMIN_CMD_PARAM.PARAM_NAME%TYPE,
        pv_param_value PSS.ADMIN_CMD_PARAM.PARAM_VALUE%TYPE)
    RETURN PSS.ADMIN_CMD_PARAM.ADMIN_CMD_PARAM_ID%TYPE
    IS
        ln_admin_cmd_param_id PSS.ADMIN_CMD_PARAM.ADMIN_CMD_PARAM_ID%TYPE;
    BEGIN
        SELECT PSS.SEQ_ADMIN_CMD_PARAM_ID.NEXTVAL
          INTO ln_admin_cmd_param_id
          FROM DUAL;
        INSERT INTO PSS.ADMIN_CMD_PARAM (
                ADMIN_CMD_PARAM_ID,
                ADMIN_CMD_ID,
                PARAM_NAME,
                PARAM_VALUE) 
         VALUES(ln_admin_cmd_param_id, 
                pn_admin_cmd_id,
                pv_param_name, 
                pv_param_value);
         RETURN ln_admin_cmd_param_id;
    END;   
    
    PROCEDURE LOCK_PARTIAL_SETTLE_POLLING(
        pv_process_id PSS.POSM_SETTING.POSM_SETTING_VALUE%TYPE,
        pv_success_flag OUT VARCHAR2,
        pv_locked_by_process_id OUT PSS.POSM_SETTING.POSM_SETTING_VALUE%TYPE)
    IS
		ln_polling_max_duration_sec NUMBER := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('POSM_FEEDBACK_POLLING_MAX_DURATION_SEC'));
		lt_last_updated_utc_ts PSS.POSM_SETTING.LAST_UPDATED_UTC_TS%TYPE;
    BEGIN
        SELECT POSM_SETTING_VALUE, 'N', LAST_UPDATED_UTC_TS
          INTO pv_locked_by_process_id, pv_success_flag, lt_last_updated_utc_ts
          FROM PSS.POSM_SETTING
         WHERE POSM_SETTING_NAME = 'PARTIAL_SETTLEMENT_POLLING_LOCK';
		 
		IF lt_last_updated_utc_ts < SYS_EXTRACT_UTC(SYSTIMESTAMP) - ln_polling_max_duration_sec/86400 THEN
			pkg_exception_processor.sp_log_exception (
				pkg_app_exec_hist_globals.unknown_error_id,
				'POSM feedback polling has been locked by ' || pv_locked_by_process_id || ' since ' || lt_last_updated_utc_ts || ' UTC, unlocking',
				NULL,
				'PSS.PKG_SETTLEMENT.LOCK_PARTIAL_SETTLE_POLLING'
			);
			UNLOCK_PARTIAL_SETTLE_POLLING(pv_locked_by_process_id);
		END IF;
		
        SELECT POSM_SETTING_VALUE, 'N'
        INTO pv_locked_by_process_id, pv_success_flag
        FROM PSS.POSM_SETTING
        WHERE POSM_SETTING_NAME = 'PARTIAL_SETTLEMENT_POLLING_LOCK';
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            BEGIN
                INSERT INTO PSS.POSM_SETTING(POSM_SETTING_NAME, POSM_SETTING_VALUE)
                  VALUES('PARTIAL_SETTLEMENT_POLLING_LOCK', pv_process_id);
                pv_success_flag := 'Y';
            EXCEPTION
                WHEN DUP_VAL_ON_INDEX THEN
                    LOCK_PARTIAL_SETTLE_POLLING(pv_process_id, pv_success_flag, pv_locked_by_process_id);
            END;
    END;
    
    PROCEDURE UNLOCK_PARTIAL_SETTLE_POLLING(
        pv_process_id PSS.POSM_SETTING.POSM_SETTING_VALUE%TYPE)
    IS
    BEGIN
        DELETE 
          FROM PSS.POSM_SETTING
         WHERE POSM_SETTING_NAME = 'PARTIAL_SETTLEMENT_POLLING_LOCK'
           AND POSM_SETTING_VALUE = pv_process_id;
        IF SQL%ROWCOUNT != 1 THEN
            RAISE NO_DATA_FOUND;
        END IF;
    END;
    
    PROCEDURE UPDATE_BATCH_FEEDBACK_REF_CD(
        pv_settlement_batch_ref_cd PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_REF_CD%TYPE,
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pd_auth_authority_ts PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_END_TS%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pv_authority_resp_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pn_settlement_batch_id OUT PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_terminal_batch_id OUT PSS.SETTLEMENT_BATCH.TERMINAL_BATCH_ID%TYPE)
    IS
        ln_settlement_batch_state_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_STATE_ID%TYPE;
    BEGIN
        SELECT SETTLEMENT_BATCH_ID, TERMINAL_BATCH_ID, SETTLEMENT_BATCH_STATE_ID
          INTO pn_settlement_batch_id, pn_terminal_batch_id, ln_settlement_batch_state_id
          FROM (SELECT SB.SETTLEMENT_BATCH_ID, SB.TERMINAL_BATCH_ID, SB.SETTLEMENT_BATCH_STATE_ID
          FROM PSS.SETTLEMENT_BATCH SB
          JOIN PSS.TERMINAL_BATCH TB ON SB.TERMINAL_BATCH_ID = TB.TERMINAL_BATCH_ID
          JOIN PSS.TERMINAL T ON TB.TERMINAL_ID = T.TERMINAL_ID
          JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
          JOIN AUTHORITY.AUTHORITY A ON M.AUTHORITY_ID = A.AUTHORITY_ID
          JOIN AUTHORITY.AUTHORITY_TYPE AUT ON A.AUTHORITY_TYPE_ID = AUT.AUTHORITY_TYPE_ID
          JOIN AUTHORITY.HANDLER H ON H.HANDLER_ID = AUT.HANDLER_ID
         WHERE SB.SETTLEMENT_BATCH_REF_CD = pv_settlement_batch_ref_cd
           AND H.HANDLER_CLASS = pv_payment_subtype_class
           AND TB.TERMINAL_BATCH_CLOSE_TS IS NOT NULL
         ORDER BY DECODE(SB.SETTLEMENT_BATCH_STATE_ID, 7, 1, 5), TB.TERMINAL_BATCH_CYCLE_NUM DESC, SB.SETTLEMENT_BATCH_ID DESC)
         WHERE ROWNUM = 1; 
        
        IF ln_settlement_batch_state_id = 1 OR (ln_settlement_batch_state_id IN(1,2,3,7) AND pc_auth_result_cd IN('P')) THEN
            RETURN;
        END IF;
           
        UPDATE PSS.SETTLEMENT_BATCH SB
           SET SB.SETTLEMENT_BATCH_STATE_ID = DECODE(pc_auth_result_cd, 'Y', 1, 'P', 7, 'N', 2, 'F', 3, 'O', 2, 'R', 2),
               SB.SETTLEMENT_BATCH_END_TS = NVL(pd_auth_authority_ts, SYSDATE),
               SB.SETTLEMENT_BATCH_RESP_DESC = DECODE(TRIM(pv_authority_resp_desc), NULL, SETTLEMENT_BATCH_RESP_DESC, SUBSTR(SETTLEMENT_BATCH_RESP_DESC || ' => ' || TRIM(pv_authority_resp_desc) , 1, 2000))
         WHERE SB.SETTLEMENT_BATCH_ID = pn_settlement_batch_id;                 
    END;
    
    PROCEDURE UPDATE_BATCH_FEEDBACK(
        pv_merchant_cd PSS.MERCHANT.MERCHANT_CD%TYPE,
        pv_terminal_cd PSS.TERMINAL.TERMINAL_CD%TYPE,
        pv_terminal_batch_num PSS.TERMINAL_BATCH.TERMINAL_BATCH_NUM%TYPE,
        pd_auth_authority_ts PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_END_TS%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pv_authority_resp_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pn_settlement_batch_id OUT PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_terminal_batch_id OUT PSS.SETTLEMENT_BATCH.TERMINAL_BATCH_ID%TYPE)
    IS
        ln_settlement_batch_state_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_STATE_ID%TYPE;
    BEGIN
        SELECT SETTLEMENT_BATCH_ID, TERMINAL_BATCH_ID, SETTLEMENT_BATCH_STATE_ID
          INTO pn_settlement_batch_id, pn_terminal_batch_id, ln_settlement_batch_state_id
          FROM (SELECT SB.SETTLEMENT_BATCH_ID, SB.TERMINAL_BATCH_ID, SB.SETTLEMENT_BATCH_STATE_ID
          FROM PSS.SETTLEMENT_BATCH SB
          JOIN PSS.TERMINAL_BATCH TB ON SB.TERMINAL_BATCH_ID = TB.TERMINAL_BATCH_ID
          JOIN PSS.TERMINAL T ON TB.TERMINAL_ID = T.TERMINAL_ID
          JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
         WHERE M.MERCHANT_CD = pv_merchant_cd
           AND T.TERMINAL_CD = pv_terminal_cd
           AND TB.TERMINAL_BATCH_NUM = pv_terminal_batch_num
           AND TB.TERMINAL_BATCH_CLOSE_TS IS NOT NULL
         ORDER BY DECODE(SB.SETTLEMENT_BATCH_STATE_ID, 7, 1, 5), TB.TERMINAL_BATCH_CYCLE_NUM DESC, SB.SETTLEMENT_BATCH_ID DESC)
         WHERE ROWNUM = 1; 
           
        IF ln_settlement_batch_state_id = 1 OR (ln_settlement_batch_state_id IN(1,2,3,7) AND pc_auth_result_cd IN('P')) THEN
            RETURN;
        END IF;
        
        UPDATE PSS.SETTLEMENT_BATCH SB
           SET SB.SETTLEMENT_BATCH_STATE_ID = DECODE(pc_auth_result_cd, 'Y', 1, 'P', 7, 'N', 2, 'F', 3, 'O', 2, 'R', 2),
               SB.SETTLEMENT_BATCH_END_TS = NVL(pd_auth_authority_ts, SYSDATE),
               SB.SETTLEMENT_BATCH_RESP_DESC = DECODE(TRIM(pv_authority_resp_desc), NULL, SETTLEMENT_BATCH_RESP_DESC, SUBSTR(SETTLEMENT_BATCH_RESP_DESC || ' => ' || TRIM(pv_authority_resp_desc) , 1, 2000))
         WHERE SB.SETTLEMENT_BATCH_ID = pn_settlement_batch_id;                 
    END;
   
    -- R37 and above signature 
    PROCEDURE UPDATE_TRAN_FEEDBACK(
        pn_terminal_batch_id PSS.SETTLEMENT_BATCH.TERMINAL_BATCH_ID%TYPE,
        pn_settlement_batch_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_sale_auth_id PSS.AUTH.AUTH_ID%TYPE,
        pn_refund_id PSS.REFUND.REFUND_ID%TYPE,
        pc_tran_type_cd CHAR,
        pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
        pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pv_authority_response_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
        pv_authority_response_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
        pd_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
        pn_approved_amt PSS.AUTH.AUTH_AMT_APPROVED%TYPE,
        pn_sale_amt PSS.AUTH.AUTH_AMT%TYPE,
        pc_major_currency_used CHAR,
        pv_tran_global_trans_cd OUT PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
		pc_settle_update_needed OUT VARCHAR2,
		pc_auth_type_cd OUT VARCHAR2,
        pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
        pn_override_trans_type_id OUT PSS.PAYMENT_SUBTYPE.TRANS_TYPE_ID%TYPE)
    IS
        ln_original_auth_id PSS.AUTH.AUTH_ID%TYPE;
        ln_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE;
        lc_entry_method_cd PSS.AUTH.ACCT_ENTRY_METHOD_CD%TYPE;
        lv_trace_number PSS.AUTH.TRACE_NUMBER%TYPE;
        ln_auth_id PSS.AUTH.AUTH_ID%TYPE;
        ln_refund_id PSS.REFUND.REFUND_ID%TYPE;
        lc_imported PSS.SALE.IMPORTED%TYPE;
        lc_sale_type_cd PSS.SALE.SALE_TYPE_CD%TYPE;
        ln_sale_result_id PSS.SALE.SALE_RESULT_ID%TYPE;
        pc_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
    BEGIN
		pc_settle_update_needed := 'Y';
        IF pn_sale_auth_id IS NOT NULL THEN
            ln_auth_id := pn_sale_auth_id;
        ELSIF pn_refund_id IS NOT NULL THEN
            ln_refund_id := pn_refund_id;
        ELSIF pc_tran_type_cd = '+' THEN
            SELECT AUTH_ID
              INTO ln_auth_id
              FROM (SELECT SA.AUTH_ID
              FROM PSS.AUTH A
              JOIN PSS.AUTH SA ON A.TRAN_ID = SA.TRAN_ID
             WHERE SA.TERMINAL_BATCH_ID = pn_terminal_batch_id
               AND A.AUTH_AUTHORITY_TRAN_CD = pv_authority_tran_cd 
               AND A.AUTH_AUTHORITY_REF_CD = pv_authority_ref_cd
               AND SA.AUTH_TYPE_CD IN('U','S','O','A','D','C','E','V','I')
               AND A.AUTH_TYPE_CD IN('L','N')
             ORDER BY SA.AUTH_TS DESC, SA.AUTH_ID DESC)
             WHERE ROWNUM = 1;
        ELSIF pc_tran_type_cd = '-' THEN
            SELECT REFUND_ID
              INTO ln_refund_id
              FROM (SELECT R.REFUND_ID, R.TRAN_ID
                      FROM PSS.REFUND R
                     WHERE R.TERMINAL_BATCH_ID = pn_terminal_batch_id
                       AND R.REFUND_AUTHORITY_TRAN_CD = pv_authority_tran_cd 
                       AND R.REFUND_AUTHORITY_REF_CD = pv_authority_ref_cd
                       AND R.REFUND_STATE_ID IN(6, 1)
                     ORDER BY R.CREATED_TS DESC, R.REFUND_ID DESC)
             WHERE ROWNUM = 1;
        ELSE
            RAISE_APPLICATION_ERROR(-20558, 'Invalid tran type cd "' ||  pc_tran_type_cd + '"');
        END IF;
        
        IF ln_auth_id IS NOT NULL THEN
            IF pc_major_currency_used = 'Y' THEN
                ln_minor_currency_factor := 1;
            ELSE
                SELECT C.MINOR_CURRENCY_FACTOR
                  INTO ln_minor_currency_factor
                  FROM PSS.CURRENCY C
                  JOIN PSS.POS_PTA PTA ON C.CURRENCY_CD = NVL(pta.CURRENCY_CD, 'USD')
                  JOIN PSS.TRAN X ON PTA.POS_PTA_ID = X.POS_PTA_ID
                  JOIN PSS.AUTH A ON A.TRAN_ID = X.TRAN_ID
                 WHERE A.AUTH_ID = ln_auth_id;
            END IF;
            UPDATE PSS.AUTH 
               SET AUTH_STATE_ID = DECODE(pc_auth_result_cd, 'Y', 2, 'P', 5, 'N', 6, 'O', 6, 'F', 6, 'R', 6),
                   AUTH_RESULT_CD = pc_auth_result_cd,
                   AUTH_RESP_CD = NVL(pv_authority_response_cd, AUTH_RESP_CD),
                   AUTH_RESP_DESC = NVL(pv_authority_response_desc, AUTH_RESP_DESC),
                   AUTH_AUTHORITY_TRAN_CD = NVL(pv_authority_tran_cd, AUTH_AUTHORITY_TRAN_CD),
                   AUTH_AUTHORITY_REF_CD = NVL(pv_authority_ref_cd, AUTH_AUTHORITY_REF_CD),
                   AUTH_AUTHORITY_TS = NVL(pd_authority_ts, AUTH_AUTHORITY_TS),
                   AUTH_AMT_APPROVED = NVL(pn_approved_amt / ln_minor_currency_factor, AUTH_AMT_APPROVED),
                   AUTH_AUTHORITY_MISC_DATA = NVL(pv_authority_misc_data, AUTH_AUTHORITY_MISC_DATA)
             WHERE AUTH_ID = ln_auth_id
             RETURNING TRAN_ID, AUTH_TYPE_CD, OVERRIDE_TRANS_TYPE_ID INTO pn_tran_id, pc_auth_type_cd, pn_override_trans_type_id;
			IF pn_settlement_batch_id IS NOT NULL THEN
                UPDATE PSS.TRAN_SETTLEMENT_BATCH
                   SET TRAN_SETTLEMENT_B_AMT = NVL(pn_approved_amt / ln_minor_currency_factor, TRAN_SETTLEMENT_B_AMT),
                       TRAN_SETTLEMENT_B_RESP_CD = NVL(pv_authority_response_cd, TRAN_SETTLEMENT_B_RESP_CD),
                       TRAN_SETTLEMENT_B_RESP_DESC = NVL(pv_authority_response_desc, TRAN_SETTLEMENT_B_RESP_DESC)
                 WHERE SETTLEMENT_BATCH_ID = pn_settlement_batch_id
                   AND AUTH_ID = ln_auth_id;               
            END IF;
        ELSIF ln_refund_id IS NOT NULL THEN
            IF pc_major_currency_used = 'Y' THEN
                ln_minor_currency_factor := 1;
            ELSE
                SELECT C.MINOR_CURRENCY_FACTOR
                  INTO ln_minor_currency_factor
                  FROM PSS.CURRENCY C
                  JOIN PSS.POS_PTA PTA ON C.CURRENCY_CD = NVL(pta.CURRENCY_CD, 'USD')
                  JOIN PSS.TRAN X ON PTA.POS_PTA_ID = X.POS_PTA_ID
                  JOIN PSS.REFUND R ON X.TRAN_ID = R.TRAN_ID
                 WHERE R.REFUND_ID = ln_refund_id;
            END IF;          
            UPDATE PSS.REFUND
               SET REFUND_STATE_ID = DECODE(pc_auth_result_cd, 'Y', 1, 'P', 1, 'N', 2, 'O', 2, 'F', 3, 'R', 2),
                REFUND_RESP_CD = NVL(pv_authority_response_cd, REFUND_RESP_CD),
                REFUND_RESP_DESC = NVL(pv_authority_response_desc, REFUND_RESP_DESC),
                REFUND_AUTHORITY_TS = NVL(pd_authority_ts, REFUND_AUTHORITY_TS),
                REFUND_AUTHORITY_MISC_DATA = NVL(pv_authority_misc_data, REFUND_AUTHORITY_MISC_DATA)
             WHERE REFUND_ID = ln_refund_id
             RETURNING TRAN_ID, REFUND_TYPE_CD, OVERRIDE_TRANS_TYPE_ID INTO pn_tran_id, pc_auth_type_cd, pn_override_trans_type_id;
            IF pn_settlement_batch_id IS NOT NULL THEN 
                UPDATE PSS.REFUND_SETTLEMENT_BATCH
                   SET REFUND_SETTLEMENT_B_AMT = NVL(pn_approved_amt / ln_minor_currency_factor, REFUND_SETTLEMENT_B_AMT),
                       REFUND_SETTLEMENT_B_RESP_CD = NVL(pv_authority_response_cd, REFUND_SETTLEMENT_B_RESP_CD),
                       REFUND_SETTLEMENT_B_RESP_DESC = NVL(pv_authority_response_desc, REFUND_SETTLEMENT_B_RESP_DESC)
                 WHERE SETTLEMENT_BATCH_ID = pn_settlement_batch_id
                   AND REFUND_ID = ln_refund_id;   
            END IF;
        END IF;
        
        -- update tran_state_cd
        UPDATE PSS.TRAN
           SET TRAN_STATE_CD = AFTER_SETTLE_TRAN_STATE_CD(DECODE(ln_auth_id, NULL, NULL, pc_auth_type_cd), DECODE(ln_auth_id, NULL,pc_auth_type_cd), pc_auth_result_cd, TRAN_DEVICE_RESULT_TYPE_CD)
         WHERE TRAN_ID = pn_tran_id
         RETURNING TRAN_GLOBAL_TRANS_CD INTO pv_tran_global_trans_cd;
         
         select max(s.imported), max(s.SALE_TYPE_CD), max(s.sale_result_id), max(x.tran_state_cd) 
        into lc_imported,lc_sale_type_cd,ln_sale_result_id,pc_tran_state_cd
        from pss.tran x left outer join pss.sale s on x.tran_id=s.tran_id 
        where x.tran_id=pn_tran_id;
        IF  (lc_imported is NULL or lc_imported != '-') AND lc_sale_type_cd IN(PKG_CONST.SALE_TYPE__ACTUAL, PKG_CONST.SALE_TYPE__CASH) AND ln_sale_result_id = 0 AND pc_tran_state_cd NOT IN('F', 'G', 'Z') THEN
            pc_settle_update_needed := 'Y';
        ELSE
            pc_settle_update_needed := 'N';
        END IF;
    END;
    
    -- R36 and below
    PROCEDURE UPDATE_TRAN_FEEDBACK(
        pn_terminal_batch_id PSS.SETTLEMENT_BATCH.TERMINAL_BATCH_ID%TYPE,
        pn_settlement_batch_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_sale_auth_id PSS.AUTH.AUTH_ID%TYPE,
        pn_refund_id PSS.REFUND.REFUND_ID%TYPE,
        pc_tran_type_cd CHAR,
        pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
        pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pv_authority_response_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
        pv_authority_response_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
        pd_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
        pn_approved_amt PSS.AUTH.AUTH_AMT_APPROVED%TYPE,
        pn_sale_amt PSS.AUTH.AUTH_AMT%TYPE,
        pc_major_currency_used CHAR,
        pv_tran_global_trans_cd OUT PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
		pc_settle_update_needed OUT VARCHAR2,
		pc_auth_type_cd OUT VARCHAR2,
        pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE)
    IS
        ln_override_trans_type_id PSS.PAYMENT_SUBTYPE.TRANS_TYPE_ID%TYPE;
    BEGIN
        UPDATE_TRAN_FEEDBACK(
            pn_terminal_batch_id,
            pn_settlement_batch_id,
            pn_sale_auth_id,
            pn_refund_id,
            pc_tran_type_cd,
            pv_authority_tran_cd,
            pv_authority_ref_cd,
            pc_auth_result_cd,
            pv_authority_response_cd,
            pv_authority_response_desc,
            pv_authority_misc_data,
            pd_authority_ts,
            pn_approved_amt,
            pn_sale_amt,
            pc_major_currency_used,
            pv_tran_global_trans_cd,
            pc_settle_update_needed,
            pc_auth_type_cd ,
            pn_tran_id,
            ln_override_trans_type_id);
    END;
    
END PKG_SETTLEMENT;
/

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/PSS/PKG_REPORTS.pbk?revision=1.19
CREATE OR REPLACE PACKAGE BODY PSS.PKG_REPORTS IS

    FUNCTION GET_RECONCILIATION_SUMMARY
     ( l_location_id IN POS.LOCATION_ID%TYPE,
       l_start_ts IN TRAN.TRAN_START_TS%TYPE,
       l_end_ts IN TRAN.TRAN_START_TS%TYPE)
     RETURN REF_CURSOR
    IS
        l_cur REF_CURSOR;
    BEGIN
        OPEN l_cur FOR
            SELECT
                TR.CAMPUS_ID,
                TR.CAMPUS_NAME,
                TR.PAYMENT_SUBTYPE_KEY_NAME,
                TR.PAYMENT_SUBTYPE_KEY_ID,
                GET_AUTH_SERVICE_TYPE_ID(TR.PAYMENT_SUBTYPE_KEY_NAME, TR.PAYMENT_SUBTYPE_KEY_ID) AUTH_SERVICE_TYPE_ID,
                  GET_AUTH_NAME(TR.PAYMENT_SUBTYPE_KEY_NAME, TR.PAYMENT_SUBTYPE_KEY_ID) AUTH_NAME,
                SUM(TLI.TRAN_LINE_ITEM_AMOUNT * TLI.TRAN_LINE_ITEM_QUANTITY) TOT_AMOUNT,
                SUM(CASE WHEN TLI.TRAN_LINE_ITEM_TYPE_ID BETWEEN 1 AND 8 OR TLI.TRAN_LINE_ITEM_TYPE_ID BETWEEN 40 AND 41 THEN TLI.TRAN_LINE_ITEM_QUANTITY ELSE 0 END) TOT_CYCLES,
                SUM(DECODE(TLI.TRAN_LINE_ITEM_TYPE_ID, 2, TLI.TRAN_LINE_ITEM_QUANTITY, 0)) TOT_DETERG,
                SUM(DECODE(TLI.TRAN_LINE_ITEM_TYPE_ID, 3, TLI.TRAN_LINE_ITEM_QUANTITY, 0)) TOT_FABRIC_SOFT,
                -- Total wash cycles does not count Super Wash (9) or
				-- Top Off Dry (42), since they're added to existing cycles.
                SUM(CASE WHEN TLI.TRAN_LINE_ITEM_TYPE_ID BETWEEN 1 AND 8 THEN TLI.TRAN_LINE_ITEM_QUANTITY ELSE 0 END) TOT_WASH_CYCLES,
                SUM(CASE WHEN TLI.TRAN_LINE_ITEM_TYPE_ID BETWEEN 40 AND 41 THEN TLI.TRAN_LINE_ITEM_QUANTITY ELSE 0 END) TOT_DRY_CYCLES,
                -- However, we *do* count the amount charged for the add-ons!
                SUM(CASE WHEN TLI.TRAN_LINE_ITEM_TYPE_ID BETWEEN 1 AND 9 THEN TLI.TRAN_LINE_ITEM_AMOUNT * TLI.TRAN_LINE_ITEM_QUANTITY ELSE 0 END) TOT_WASH_AMOUNT,
                SUM(CASE WHEN TLI.TRAN_LINE_ITEM_TYPE_ID BETWEEN 40 AND 42 THEN TLI.TRAN_LINE_ITEM_AMOUNT * TLI.TRAN_LINE_ITEM_QUANTITY ELSE 0 END) TOT_DRY_AMOUNT
               FROM MV_ESUDS_TRAN_LINE_ITEM TLI
               JOIN (SELECT SCHOOL_ID, CAMPUS_ID, CAMPUS_NAME, PAYMENT_SUBTYPE_KEY_NAME, 
                        PAYMENT_SUBTYPE_KEY_ID, TRAN_ID, TRAN_STATE_CD, 
                        MAX(SETTLEMENT_BATCH_TS) AS SETTLEMENT_BATCH_TS 
                        FROM PSS.MV_ESUDS_SETTLED_TRAN
                        WHERE SETTLEMENT_BATCH_TS BETWEEN l_start_ts - 1 AND l_end_ts + 1
                        HAVING MAX(SETTLEMENT_BATCH_TS) BETWEEN L_START_TS AND L_END_TS
                         GROUP BY SCHOOL_ID, CAMPUS_ID, CAMPUS_NAME, 
                        PAYMENT_SUBTYPE_KEY_NAME, PAYMENT_SUBTYPE_KEY_ID, TRAN_ID, TRAN_STATE_CD) TR
                 ON TLI.TRAN_ID = TR.TRAN_ID
              WHERE TLI.TRAN_LINE_ITEM_BATCH_TYPE_CD = (CASE
                    WHEN TR.TRAN_STATE_CD IN('9','8','I','E','F','T') THEN 'I'
                    WHEN TR.TRAN_STATE_CD IN('D', 'C') AND NOT EXISTS(
                        SELECT 1 FROM MV_ESUDS_SETTLED_REFUND RF
                        WHERE TR.TRAN_ID = RF.TRAN_ID
                        AND RF.SETTLEMENT_BATCH_TS NOT BETWEEN l_start_ts
                          AND l_end_ts) THEN 'A'
                    ELSE 'I'
                   END)
                AND TR.SCHOOL_ID = l_location_id
           GROUP BY TR.CAMPUS_ID,
                    TR.CAMPUS_NAME,
                    TR.PAYMENT_SUBTYPE_KEY_NAME,
                    TR.PAYMENT_SUBTYPE_KEY_ID
           ORDER BY UPPER(TR.CAMPUS_NAME),
                    UPPER(GET_AUTH_NAME(TR.PAYMENT_SUBTYPE_KEY_NAME, TR.PAYMENT_SUBTYPE_KEY_ID));
 
        RETURN l_cur;
    END;


    FUNCTION GET_RECONCILIATION_DETAIL
     ( l_location_id IN POS.LOCATION_ID%TYPE,
       l_start_ts IN TRAN.TRAN_START_TS%TYPE,
       l_end_ts IN TRAN.TRAN_START_TS%TYPE,
       l_pst_key_id IN POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE,
       l_pst_key_name IN PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_KEY_NAME%TYPE)
     RETURN REF_CURSOR
    IS
        l_cur REF_CURSOR;
    BEGIN
        OPEN l_cur FOR
            SELECT
                TR.CAMPUS_ID,
                TR.CAMPUS_NAME,
                TR.DORM_ID,
                TR.DORM_NAME,
                TR.ROOM_ID,
                TR.ROOM_NAME,
                GET_AUTH_SERVICE_TYPE_ID(TR.PAYMENT_SUBTYPE_KEY_NAME, TR.PAYMENT_SUBTYPE_KEY_ID) AUTH_SERVICE_TYPE_ID,
    			GET_AUTH_NAME(TR.PAYMENT_SUBTYPE_KEY_NAME, TR.PAYMENT_SUBTYPE_KEY_ID) AUTH_NAME,
                TR.TRAN_SETTLEMENT_AMOUNT INITIAL_AMOUNT,
                -RF.REFUND_SETTLEMENT_B_AMT REFUND_AMOUNT,
                TR.TRAN_SETTLEMENT_AMOUNT + NVL(-RF.REFUND_SETTLEMENT_B_AMT, 0) TOT_AMOUNT,
                TR.TRAN_START_TS,
                TR.SETTLEMENT_BATCH_TS SETTLED_TS,
                RF.REFUND_DESC TRAN_REFUND_DESC,
                RF.SETTLEMENT_BATCH_TS TRAN_REFUND_SETTLEMENT_TS,
                SF_PIVOT_ESUDS_TRAN_LINE_ITEM (TR.TRAN_ID) TRAN_LINE_ITEM_DESC,
                NVL(CA.CONSUMER_ACCT_CD, TR.TRAN_MASKED_ACCT_NUM) CONSUMER_ACCT_CD,
                DECODE(CA.CONSUMER_ACCT_ID, NULL, 'Y', 'N') CONSUMER_MISSING
               FROM MV_ESUDS_SETTLED_TRAN TR
               LEFT OUTER JOIN CONSUMER_ACCT CA 
                 ON TR.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID 
               LEFT OUTER JOIN MV_ESUDS_SETTLED_REFUND RF 
                 ON TR.TRAN_ID = RF.TRAN_ID
                AND RF.SETTLEMENT_BATCH_TS BETWEEN l_start_ts AND l_end_ts
              WHERE TR.CAMPUS_ID = l_location_id
                AND NVL(TR.PAYMENT_SUBTYPE_KEY_ID, 0) = l_pst_key_id
                AND TR.PAYMENT_SUBTYPE_KEY_NAME = l_pst_key_name
                AND TR.SETTLEMENT_BATCH_TS BETWEEN l_start_ts AND l_end_ts
           ORDER BY TR.SETTLEMENT_BATCH_TS,
                    UPPER(TR.DORM_NAME),
                    UPPER(TR.ROOM_NAME),
                    NVL(CA.CONSUMER_ACCT_CD, TR.TRAN_MASKED_ACCT_NUM);
        RETURN l_cur;
    END;

    PROCEDURE ADD_BATCHLESS_MV_ESUDS_TRAN(
        pd_start_date DATE,
        pd_end_date DATE)
    IS
    BEGIN
        --Load Batchless
           INSERT INTO PSS.MV_ESUDS_SETTLED_TRAN (
               SETTLEMENT_BATCH_ID,
               SCHOOL_ID,
               SCHOOL_NAME,
               CAMPUS_ID,
               CAMPUS_NAME,
               DORM_ID,
               DORM_NAME,
               ROOM_ID,
               ROOM_NAME,
               PAYMENT_SUBTYPE_KEY_NAME,
               PAYMENT_SUBTYPE_KEY_ID,
               TRAN_ID,
               TRAN_START_TS,
               TRAN_STATE_CD,
               CONSUMER_ACCT_ID,
               POS_PTA_ID,
               TRAN_SETTLEMENT_AMOUNT,
               SETTLEMENT_BATCH_TS,
               OPERATOR_ID,
               CLIENT_PAYMENT_TYPE_CD,
               TRAN_MASKED_ACCT_NUM,
               DEVICE_ID,
               PAYMENT_SUBTYPE_ID,
               PAYMENT_SUBTYPE_NAME)
           SELECT
                NULL SETTLEMENT_BATCH_ID,
                LOC1.LOCATION_ID SCHOOL_ID,
                LOC1.LOCATION_NAME SCHOOL_NAME,
                LOC2.LOCATION_ID CAMPUS_ID,
                LOC2.LOCATION_NAME CAMPUS_NAME,
                LOC3.LOCATION_ID DORM_ID,
                LOC3.LOCATION_NAME DORM_NAME,
                LOC4.LOCATION_ID ROOM_ID,
                LOC4.LOCATION_NAME ROOM_NAME,
                PST.PAYMENT_SUBTYPE_KEY_NAME,
                PTA.PAYMENT_SUBTYPE_KEY_ID,
                TR.TRAN_ID,
                TR.TRAN_START_TS,
                TR.TRAN_STATE_CD,
                TR.CONSUMER_ACCT_ID,
                TR.POS_PTA_ID,
                SA.AUTH_AMT_APPROVED TRAN_SETTLEMENT_AMOUNT,
                SA.AUTH_AUTHORITY_TS SETTLEMENT_BATCH_TS,
                PS.CUSTOMER_ID,
                PST.CLIENT_PAYMENT_TYPE_CD,
                TR.TRAN_RECEIVED_RAW_ACCT_DATA,
                PS.DEVICE_ID,
                PTA.PAYMENT_SUBTYPE_ID,
                PST.PAYMENT_SUBTYPE_NAME
               FROM PSS.TRAN TR
               JOIN PSS.POS_PTA PTA ON TR.POS_PTA_ID = PTA.POS_PTA_ID
               JOIN PSS.POS PS ON PTA.POS_ID = PS.POS_ID
               JOIN LOCATION.LOCATION LOC4 ON PS.LOCATION_ID = LOC4.LOCATION_ID
               JOIN LOCATION.LOCATION LOC3 ON LOC4.PARENT_LOCATION_ID = LOC3.LOCATION_ID
               JOIN LOCATION.LOCATION LOC2 ON LOC3.PARENT_LOCATION_ID = LOC2.LOCATION_ID
               JOIN LOCATION.LOCATION LOC1 ON LOC2.PARENT_LOCATION_ID = LOC1.LOCATION_ID
               JOIN PSS.PAYMENT_SUBTYPE PST ON PTA.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
               JOIN PSS.AUTH SA ON TR.TRAN_ID = SA.TRAN_ID AND SA.AUTH_TYPE_CD NOT IN('N', 'L', 'A') AND SA.AUTH_STATE_ID IN(2,5)
               LEFT OUTER JOIN PSS.TRAN_SETTLEMENT_BATCH TS ON SA.AUTH_ID = TS.AUTH_ID
              WHERE LOC4.LOCATION_TYPE_ID = 18
                AND PST.CLIENT_PAYMENT_TYPE_CD IN('S', 'P')
                AND TS.SETTLEMENT_BATCH_ID IS NULL
                AND TR.TRAN_STATE_CD IN('C', 'D', 'E', 'V')
                AND SA.AUTH_AUTHORITY_TS BETWEEN pd_start_date AND pd_end_date
                AND NOT EXISTS(SELECT 1 FROM PSS.MV_ESUDS_SETTLED_TRAN O WHERE O.TRAN_ID = TR.TRAN_ID);
           INSERT INTO PSS.MV_ESUDS_TRAN_LINE_ITEM(
                   TRAN_LINE_ITEM_ID, 
                   TRAN_LINE_ITEM_AMOUNT, 
                   TRAN_LINE_ITEM_TAX, 
                   TRAN_LINE_ITEM_TS, 
                   TRAN_LINE_ITEM_POSITION_CD, 
                   TRAN_LINE_ITEM_DESC, 
                   TRAN_LINE_ITEM_TYPE_ID, 
                   TRAN_ID, 
                   TRAN_LINE_ITEM_QUANTITY, 
                   HOST_ID, 
                   TRAN_LINE_ITEM_BATCH_TYPE_CD)
                SELECT
                   tli.TRAN_LINE_ITEM_ID, 
                   tli.TRAN_LINE_ITEM_AMOUNT, 
                   tli.TRAN_LINE_ITEM_TAX, 
                   tli.TRAN_LINE_ITEM_TS, 
                   tli.TRAN_LINE_ITEM_POSITION_CD, 
                   tli.TRAN_LINE_ITEM_DESC, 
                   tli.TRAN_LINE_ITEM_TYPE_ID, 
                   tli.TRAN_ID, 
                   tli.TRAN_LINE_ITEM_QUANTITY, 
                   tli.HOST_ID, 
                   tli.TRAN_LINE_ITEM_BATCH_TYPE_CD
                   FROM PSS.TRAN_LINE_ITEM TLI
                   JOIN PSS.TRAN TR ON TLI.TRAN_ID = TR.TRAN_ID
                   JOIN PSS.POS_PTA PTA ON TR.POS_PTA_ID = PTA.POS_PTA_ID
                   JOIN PSS.POS PS ON PTA.POS_ID = PS.POS_ID
                   JOIN LOCATION.LOCATION LOC4 ON PS.LOCATION_ID = LOC4.LOCATION_ID
                   JOIN LOCATION.LOCATION LOC3 ON LOC4.PARENT_LOCATION_ID = LOC3.LOCATION_ID
                   JOIN LOCATION.LOCATION LOC2 ON LOC3.PARENT_LOCATION_ID = LOC2.LOCATION_ID
                   JOIN LOCATION.LOCATION LOC1 ON LOC2.PARENT_LOCATION_ID = LOC1.LOCATION_ID
                   JOIN PSS.PAYMENT_SUBTYPE PST ON PTA.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
                   JOIN PSS.AUTH SA ON TR.TRAN_ID = SA.TRAN_ID AND SA.AUTH_TYPE_CD NOT IN('N', 'L', 'A') AND SA.AUTH_STATE_ID IN(2,5)
                   LEFT OUTER JOIN PSS.TRAN_SETTLEMENT_BATCH TS ON SA.AUTH_ID = TS.AUTH_ID
                  WHERE LOC4.LOCATION_TYPE_ID = 18
                    AND PST.CLIENT_PAYMENT_TYPE_CD IN('S', 'P')
                    AND TS.SETTLEMENT_BATCH_ID IS NULL
                    AND TR.TRAN_STATE_CD IN('C', 'D', 'E', 'V')
                    AND SA.AUTH_AUTHORITY_TS BETWEEN pd_start_date AND pd_end_date 
                    AND NOT EXISTS(SELECT 1 FROM PSS.MV_ESUDS_TRAN_LINE_ITEM O WHERE O.TRAN_LINE_ITEM_ID = TLI.TRAN_LINE_ITEM_ID);
        INSERT INTO PSS.MV_ESUDS_SETTLED_REFUND (
               REFUND_ID, 
               SETTLEMENT_BATCH_ID,
               REFUND_SETTLEMENT_B_AMT, 
               REFUND_DESC, 
               SETTLEMENT_BATCH_TS, 
               TRAN_ID)
           SELECT
                R.REFUND_ID,
                NULL SETTLEMENT_BATCH_ID,
                REFUND_AMT TRAN_SETTLEMENT_AMOUNT,
                R.REFUND_DESC,
                R.REFUND_AUTHORITY_TS SETTLEMENT_BATCH_TS,
                R.TRAN_ID
               FROM PSS.TRAN TR
               JOIN PSS.POS_PTA PTA ON TR.POS_PTA_ID = PTA.POS_PTA_ID
               JOIN PSS.POS PS ON PTA.POS_ID = PS.POS_ID
               JOIN LOCATION.LOCATION LOC4 ON PS.LOCATION_ID = LOC4.LOCATION_ID
               JOIN LOCATION.LOCATION LOC3 ON LOC4.PARENT_LOCATION_ID = LOC3.LOCATION_ID
               JOIN LOCATION.LOCATION LOC2 ON LOC3.PARENT_LOCATION_ID = LOC2.LOCATION_ID
               JOIN LOCATION.LOCATION LOC1 ON LOC2.PARENT_LOCATION_ID = LOC1.LOCATION_ID
               JOIN PSS.PAYMENT_SUBTYPE PST ON PTA.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
               JOIN PSS.REFUND R ON TR.TRAN_ID = R.TRAN_ID AND R.REFUND_STATE_ID IN(1, 7)
               LEFT OUTER JOIN PSS.REFUND_SETTLEMENT_BATCH TS ON R.REFUND_ID = TS.REFUND_ID
              WHERE LOC4.LOCATION_TYPE_ID = 18
                AND PST.CLIENT_PAYMENT_TYPE_CD IN('S', 'P')
                AND TS.SETTLEMENT_BATCH_ID IS NULL
                AND TR.TRAN_STATE_CD IN('C', 'D', 'E', 'V')
                AND R.REFUND_AUTHORITY_TS BETWEEN pd_start_date AND pd_end_date;
    END;
   
   PROCEDURE UPDATE_MV_ESUDS_SETTLED_TRAN
   -- break this into parts:
   -- first load non-credit card transactions through PSS.AUTH by settlement_batch_end_ts
   -- then load non-credit card transactions through PSS.REFUND by settlement_batch_end_ts
   -- then load cash transactions by tran_upload_ts (as settlement_batch_ts)
   IS
     l_last_ts PSS.MV_ESUDS_SETTLED_TRAN.SETTLEMENT_BATCH_TS%TYPE;
     l_last_sb_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE;
     l_max_ts DATE;
     CURSOR l_cur IS
         SELECT S.SETTLEMENT_BATCH_ID,
                S.SETTLEMENT_BATCH_END_TS
           FROM PSS.SETTLEMENT_BATCH S
          WHERE S.SETTLEMENT_BATCH_STATE_ID IN(1)
            AND S.SETTLEMENT_BATCH_END_TS >= l_last_ts
            AND (S.SETTLEMENT_BATCH_END_TS > l_last_ts
                OR S.SETTLEMENT_BATCH_ID > l_last_sb_id)
          ORDER BY S.SETTLEMENT_BATCH_END_TS, S.SETTLEMENT_BATCH_ID;
   BEGIN
       -- load special cards (from first PSS.AUTH, then PSS.REFUND)        
       SELECT NVL(MAX(SETTLEMENT_BATCH_TS), MIN_DATE)
         INTO l_last_ts
         FROM PSS.MV_ESUDS_SETTLED_TRAN
        WHERE CLIENT_PAYMENT_TYPE_CD IN('S', 'P');
       SELECT GREATEST(l_last_ts, NVL(MAX(SETTLEMENT_BATCH_TS), MIN_DATE))
         INTO l_last_ts
         FROM PSS.MV_ESUDS_SETTLED_REFUND;
       SELECT NVL(MAX(sb.SETTLEMENT_BATCH_ID), 0)
         INTO l_last_sb_id
         FROM PSS.MV_ESUDS_SETTLED_TRAN mv
         JOIN PSS.SETTLEMENT_BATCH sb ON mv.SETTLEMENT_BATCH_ID = sb.SETTLEMENT_BATCH_ID
        WHERE mv.SETTLEMENT_BATCH_TS = l_last_ts
          AND mv.CLIENT_PAYMENT_TYPE_CD IN('S', 'P');
       SELECT GREATEST(l_last_sb_id, NVL(MAX(sb.SETTLEMENT_BATCH_ID), 0))
         INTO l_last_sb_id
         FROM MV_ESUDS_SETTLED_REFUND mv
         JOIN PSS.SETTLEMENT_BATCH sb ON mv.SETTLEMENT_BATCH_ID = sb.SETTLEMENT_BATCH_ID
        WHERE mv.SETTLEMENT_BATCH_TS = l_last_ts;
          
       FOR l_rec IN l_cur LOOP
           INSERT INTO MV_ESUDS_SETTLED_TRAN (
               SETTLEMENT_BATCH_ID,
               SCHOOL_ID,
               SCHOOL_NAME,
               CAMPUS_ID,
               CAMPUS_NAME,
               DORM_ID,
               DORM_NAME,
               ROOM_ID,
               ROOM_NAME,
               PAYMENT_SUBTYPE_KEY_NAME,
               PAYMENT_SUBTYPE_KEY_ID,
               TRAN_ID,
               TRAN_START_TS,
               TRAN_STATE_CD,
               CONSUMER_ACCT_ID,
               POS_PTA_ID,
               TRAN_SETTLEMENT_AMOUNT,
               SETTLEMENT_BATCH_TS,
               OPERATOR_ID,
               CLIENT_PAYMENT_TYPE_CD,
               TRAN_MASKED_ACCT_NUM,
               DEVICE_ID,
               PAYMENT_SUBTYPE_ID,
               PAYMENT_SUBTYPE_NAME)
           SELECT
                l_rec.SETTLEMENT_BATCH_ID SETTLEMENT_BATCH_ID,
                LOC1.LOCATION_ID SCHOOL_ID,
                LOC1.LOCATION_NAME SCHOOL_NAME,
                LOC2.LOCATION_ID CAMPUS_ID,
                LOC2.LOCATION_NAME CAMPUS_NAME,
                LOC3.LOCATION_ID DORM_ID,
                LOC3.LOCATION_NAME DORM_NAME,
                LOC4.LOCATION_ID ROOM_ID,
                LOC4.LOCATION_NAME ROOM_NAME,
                PST.PAYMENT_SUBTYPE_KEY_NAME,
				PTA.PAYMENT_SUBTYPE_KEY_ID,
                TR.TRAN_ID,
                TR.TRAN_START_TS,
                TR.TRAN_STATE_CD,
                TR.CONSUMER_ACCT_ID,
                TR.POS_PTA_ID,
                TS.TRAN_SETTLEMENT_B_AMT TRAN_SETTLEMENT_AMOUNT,
                l_rec.SETTLEMENT_BATCH_END_TS SETTLEMENT_BATCH_TS,
                PS.CUSTOMER_ID,
                PST.CLIENT_PAYMENT_TYPE_CD,
                TR.TRAN_RECEIVED_RAW_ACCT_DATA,
                PS.DEVICE_ID,
                PTA.PAYMENT_SUBTYPE_ID,
                PST.PAYMENT_SUBTYPE_NAME
               FROM PSS.TRAN TR,
                    PSS.POS PS,
                    PSS.POS_PTA PTA,
                    LOCATION.LOCATION LOC1,
                    LOCATION.LOCATION LOC2,
                    LOCATION.LOCATION LOC3,
                    LOCATION.LOCATION LOC4,
                    PSS.PAYMENT_SUBTYPE PST,
                    PSS.AUTH A,
                    PSS.TRAN_SETTLEMENT_BATCH TS
              WHERE TR.POS_PTA_ID = PTA.POS_PTA_ID
                AND PTA.POS_ID = PS.POS_ID
                AND PS.LOCATION_ID = LOC4.LOCATION_ID
                AND LOC4.LOCATION_TYPE_ID = 18
                AND LOC4.PARENT_LOCATION_ID = LOC3.LOCATION_ID
                AND LOC3.PARENT_LOCATION_ID = LOC2.LOCATION_ID
                AND LOC2.PARENT_LOCATION_ID = LOC1.LOCATION_ID
                AND PTA.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
                AND PST.CLIENT_PAYMENT_TYPE_CD IN('S', 'P')
                AND TR.TRAN_ID = A.TRAN_ID
                AND A.AUTH_ID = TS.AUTH_ID
                AND l_rec.SETTLEMENT_BATCH_ID = TS.SETTLEMENT_BATCH_ID;
           MERGE INTO MV_ESUDS_TRAN_LINE_ITEM O
                USING (SELECT
                           tli.TRAN_LINE_ITEM_ID, 
                           tli.TRAN_LINE_ITEM_AMOUNT, 
                           tli.TRAN_LINE_ITEM_TAX, 
                           tli.TRAN_LINE_ITEM_TS, 
                           tli.TRAN_LINE_ITEM_POSITION_CD, 
                           tli.TRAN_LINE_ITEM_DESC, 
                           tli.TRAN_LINE_ITEM_TYPE_ID, 
                           tli.TRAN_ID, 
                           tli.TRAN_LINE_ITEM_QUANTITY, 
                           tli.HOST_ID, 
                           tli.TRAN_LINE_ITEM_BATCH_TYPE_CD
                           FROM PSS.TRAN_LINE_ITEM TLI,
                                PSS.TRAN TR,
                                PSS.POS PS,
                                PSS.POS_PTA PTA,
                                LOCATION.LOCATION LOC1,
                                LOCATION.LOCATION LOC2,
                                LOCATION.LOCATION LOC3,
                                LOCATION.LOCATION LOC4,
                                PSS.PAYMENT_SUBTYPE PST,
                                PSS.AUTH A,
                                PSS.TRAN_SETTLEMENT_BATCH TS
                          WHERE TLI.TRAN_ID = A.TRAN_ID
                            AND TR.POS_PTA_ID = PTA.POS_PTA_ID
                            AND PTA.POS_ID = PS.POS_ID
                            AND PS.LOCATION_ID = LOC4.LOCATION_ID
                            AND LOC4.LOCATION_TYPE_ID = 18
                            AND LOC4.PARENT_LOCATION_ID = LOC3.LOCATION_ID
                            AND LOC3.PARENT_LOCATION_ID = LOC2.LOCATION_ID
                            AND LOC2.PARENT_LOCATION_ID = LOC1.LOCATION_ID
                            AND PTA.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
                            AND PST.CLIENT_PAYMENT_TYPE_CD IN('S', 'P')
                            AND TR.TRAN_ID = A.TRAN_ID
                            AND A.AUTH_ID = TS.AUTH_ID
                            AND l_rec.SETTLEMENT_BATCH_ID = TS.SETTLEMENT_BATCH_ID) N
                 ON (O.TRAN_LINE_ITEM_ID = N.TRAN_LINE_ITEM_ID)
               WHEN MATCHED THEN
               UPDATE SET 
                   TRAN_LINE_ITEM_AMOUNT = N.TRAN_LINE_ITEM_AMOUNT, 
                   TRAN_LINE_ITEM_TAX = N.TRAN_LINE_ITEM_TAX, 
                   TRAN_LINE_ITEM_TS = N.TRAN_LINE_ITEM_TS, 
                   TRAN_LINE_ITEM_POSITION_CD = N.TRAN_LINE_ITEM_POSITION_CD, 
                   TRAN_LINE_ITEM_DESC = N.TRAN_LINE_ITEM_DESC, 
                   TRAN_LINE_ITEM_TYPE_ID = N.TRAN_LINE_ITEM_TYPE_ID, 
                   TRAN_ID = N.TRAN_ID, 
                   TRAN_LINE_ITEM_QUANTITY = N.TRAN_LINE_ITEM_QUANTITY, 
                   HOST_ID = N.HOST_ID, 
                   TRAN_LINE_ITEM_BATCH_TYPE_CD = N.TRAN_LINE_ITEM_BATCH_TYPE_CD
               WHEN NOT MATCHED THEN
               INSERT (
                   TRAN_LINE_ITEM_ID, 
                   TRAN_LINE_ITEM_AMOUNT, 
                   TRAN_LINE_ITEM_TAX, 
                   TRAN_LINE_ITEM_TS, 
                   TRAN_LINE_ITEM_POSITION_CD, 
                   TRAN_LINE_ITEM_DESC, 
                   TRAN_LINE_ITEM_TYPE_ID, 
                   TRAN_ID, 
                   TRAN_LINE_ITEM_QUANTITY, 
                   HOST_ID, 
                   TRAN_LINE_ITEM_BATCH_TYPE_CD)
               VALUES (
                   N.TRAN_LINE_ITEM_ID, 
                   N.TRAN_LINE_ITEM_AMOUNT, 
                   N.TRAN_LINE_ITEM_TAX, 
                   N.TRAN_LINE_ITEM_TS, 
                   N.TRAN_LINE_ITEM_POSITION_CD, 
                   N.TRAN_LINE_ITEM_DESC, 
                   N.TRAN_LINE_ITEM_TYPE_ID, 
                   N.TRAN_ID, 
                   N.TRAN_LINE_ITEM_QUANTITY, 
                   N.HOST_ID, 
                   N.TRAN_LINE_ITEM_BATCH_TYPE_CD);

           --Load refunds
           INSERT INTO MV_ESUDS_SETTLED_REFUND (
               REFUND_ID, 
               SETTLEMENT_BATCH_ID,
               REFUND_SETTLEMENT_B_AMT, 
               REFUND_DESC, 
               SETTLEMENT_BATCH_TS, 
               TRAN_ID)
           SELECT
                R.REFUND_ID,
                l_rec.SETTLEMENT_BATCH_ID SETTLEMENT_BATCH_ID,
                TS.REFUND_SETTLEMENT_B_AMT TRAN_SETTLEMENT_AMOUNT,
                R.REFUND_DESC,
                l_rec.SETTLEMENT_BATCH_END_TS SETTLEMENT_BATCH_TS,
                R.TRAN_ID
               FROM PSS.TRAN TR,
                    PSS.POS PS,
                    PSS.POS_PTA PTA,
                    LOCATION.LOCATION LOC1,
                    LOCATION.LOCATION LOC2,
                    LOCATION.LOCATION LOC3,
                    LOCATION.LOCATION LOC4,
                    PSS.PAYMENT_SUBTYPE PST,
                    PSS.REFUND R,
                    PSS.REFUND_SETTLEMENT_BATCH TS
              WHERE TR.POS_PTA_ID = PTA.POS_PTA_ID
                AND PTA.POS_ID = PS.POS_ID
                AND PS.LOCATION_ID = LOC4.LOCATION_ID
                AND LOC4.LOCATION_TYPE_ID = 18
                AND LOC4.PARENT_LOCATION_ID = LOC3.LOCATION_ID
                AND LOC3.PARENT_LOCATION_ID = LOC2.LOCATION_ID
                AND LOC2.PARENT_LOCATION_ID = LOC1.LOCATION_ID
                AND PTA.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
                AND PST.CLIENT_PAYMENT_TYPE_CD IN('S', 'P')
                AND TR.TRAN_ID = R.TRAN_ID
                AND R.REFUND_ID = TS.REFUND_ID
                AND R.REFUND_STATE_ID IN(1, 7)
                AND l_rec.SETTLEMENT_BATCH_ID = TS.SETTLEMENT_BATCH_ID;
                
           --Load Batchless
           ADD_BATCHLESS_MV_ESUDS_TRAN(l_last_ts, l_rec.SETTLEMENT_BATCH_END_TS);

           COMMIT;
           l_last_ts := l_rec.SETTLEMENT_BATCH_END_TS + (1/24/60/60);
       END LOOP;
       
       -- load remaining pass/access
       ADD_BATCHLESS_MV_ESUDS_TRAN(l_last_ts, SYSDATE);

       -- Now load cash
       SELECT MAX(SETTLEMENT_BATCH_TS), SYSDATE
         INTO l_last_ts, l_max_ts
         FROM MV_ESUDS_SETTLED_TRAN
        WHERE CLIENT_PAYMENT_TYPE_CD IN('M');
       IF l_last_ts IS NULL THEN
          l_last_ts := MIN_DATE;
       END IF;
       SELECT NVL(MAX(mv.TRAN_ID), 0)
         INTO l_last_sb_id
         FROM MV_ESUDS_SETTLED_TRAN mv
        WHERE mv.SETTLEMENT_BATCH_TS = l_last_ts
          AND mv.CLIENT_PAYMENT_TYPE_CD IN('M');
       INSERT INTO MV_ESUDS_SETTLED_TRAN (
               SETTLEMENT_BATCH_ID,
               SCHOOL_ID,
               SCHOOL_NAME,
               CAMPUS_ID,
               CAMPUS_NAME,
               DORM_ID,
               DORM_NAME,
               ROOM_ID,
               ROOM_NAME,
               PAYMENT_SUBTYPE_KEY_NAME,
               PAYMENT_SUBTYPE_KEY_ID,
               TRAN_ID,
               TRAN_START_TS,
               TRAN_STATE_CD,
               CONSUMER_ACCT_ID,
               POS_PTA_ID,
               TRAN_SETTLEMENT_AMOUNT,
               SETTLEMENT_BATCH_TS,
               OPERATOR_ID,
               CLIENT_PAYMENT_TYPE_CD,
               TRAN_MASKED_ACCT_NUM,
               DEVICE_ID,
               PAYMENT_SUBTYPE_ID,
               PAYMENT_SUBTYPE_NAME)
           SELECT
                NULL,
                LOC1.LOCATION_ID SCHOOL_ID,
                LOC1.LOCATION_NAME SCHOOL_NAME,
                LOC2.LOCATION_ID CAMPUS_ID,
                LOC2.LOCATION_NAME CAMPUS_NAME,
                LOC3.LOCATION_ID DORM_ID,
                LOC3.LOCATION_NAME DORM_NAME,
                LOC4.LOCATION_ID ROOM_ID,
                LOC4.LOCATION_NAME ROOM_NAME,
                PST.PAYMENT_SUBTYPE_KEY_NAME,
				PTA.PAYMENT_SUBTYPE_KEY_ID,
                TR.TRAN_ID,
                TR.TRAN_START_TS,
                TR.TRAN_STATE_CD,
                TR.CONSUMER_ACCT_ID,
                TR.POS_PTA_ID,
                SUM(TLI.TRAN_LINE_ITEM_QUANTITY * TLI.TRAN_LINE_ITEM_AMOUNT) TRAN_SETTLEMENT_AMOUNT,
                TR.TRAN_UPLOAD_TS SETTLEMENT_BATCH_TS,
                PS.CUSTOMER_ID,
                PST.CLIENT_PAYMENT_TYPE_CD,
                TR.TRAN_RECEIVED_RAW_ACCT_DATA,
                PS.DEVICE_ID,
                PTA.PAYMENT_SUBTYPE_ID,
                PST.PAYMENT_SUBTYPE_NAME
               FROM PSS.TRAN TR,
                    PSS.POS PS,
                    PSS.POS_PTA PTA,
                    LOCATION.LOCATION LOC1,
                    LOCATION.LOCATION LOC2,
                    LOCATION.LOCATION LOC3,
                    LOCATION.LOCATION LOC4,
                    PSS.PAYMENT_SUBTYPE PST,
                    PSS.TRAN_LINE_ITEM TLI
              WHERE TR.POS_PTA_ID = PTA.POS_PTA_ID
                AND PTA.POS_ID = PS.POS_ID
                AND PS.LOCATION_ID = LOC4.LOCATION_ID
                AND LOC4.LOCATION_TYPE_ID = 18
                AND LOC4.PARENT_LOCATION_ID = LOC3.LOCATION_ID
                AND LOC3.PARENT_LOCATION_ID = LOC2.LOCATION_ID
                AND LOC2.PARENT_LOCATION_ID = LOC1.LOCATION_ID
                AND PTA.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
                AND PST.CLIENT_PAYMENT_TYPE_CD IN('M')
                AND TR.TRAN_STATE_CD IN('D', 'C', '8', 'E', 'A')
                AND TR.TRAN_ID = TLI.TRAN_ID
                AND TR.TRAN_UPLOAD_TS < l_max_ts
                AND TR.TRAN_UPLOAD_TS >= l_last_ts
                AND (TR.TRAN_UPLOAD_TS > l_last_ts
                    OR TR.TRAN_ID > l_last_sb_id)
              GROUP BY NULL, LOC1.LOCATION_ID, LOC1.LOCATION_NAME, LOC2.LOCATION_ID, LOC2.LOCATION_NAME, LOC3.LOCATION_ID, LOC3.LOCATION_NAME, LOC4.LOCATION_ID, LOC4.LOCATION_NAME, PST.PAYMENT_SUBTYPE_KEY_NAME, PTA.PAYMENT_SUBTYPE_KEY_ID, TR.TRAN_ID, TR.TRAN_START_TS, TR.TRAN_STATE_CD, TR.CONSUMER_ACCT_ID, TR.POS_PTA_ID, TR.TRAN_UPLOAD_TS, PS.CUSTOMER_ID, PST.CLIENT_PAYMENT_TYPE_CD, TR.TRAN_RECEIVED_RAW_ACCT_DATA, PS.DEVICE_ID, PTA.PAYMENT_SUBTYPE_ID, PST.PAYMENT_SUBTYPE_NAME;
         
       INSERT INTO MV_ESUDS_TRAN_LINE_ITEM (
               TRAN_LINE_ITEM_ID, 
               TRAN_LINE_ITEM_AMOUNT, 
               TRAN_LINE_ITEM_TAX, 
               TRAN_LINE_ITEM_TS, 
               TRAN_LINE_ITEM_POSITION_CD, 
               TRAN_LINE_ITEM_DESC, 
               TRAN_LINE_ITEM_TYPE_ID, 
               TRAN_ID, 
               TRAN_LINE_ITEM_QUANTITY, 
               HOST_ID, 
               TRAN_LINE_ITEM_BATCH_TYPE_CD)
           SELECT
               tli.TRAN_LINE_ITEM_ID, 
               tli.TRAN_LINE_ITEM_AMOUNT, 
               tli.TRAN_LINE_ITEM_TAX, 
               tli.TRAN_LINE_ITEM_TS, 
               tli.TRAN_LINE_ITEM_POSITION_CD, 
               tli.TRAN_LINE_ITEM_DESC, 
               tli.TRAN_LINE_ITEM_TYPE_ID, 
               tli.TRAN_ID, 
               tli.TRAN_LINE_ITEM_QUANTITY, 
               tli.HOST_ID, 
               tli.TRAN_LINE_ITEM_BATCH_TYPE_CD
               FROM PSS.TRAN_LINE_ITEM TLI,
                    PSS.TRAN TR,
                    PSS.POS PS,
                    PSS.POS_PTA PTA,
                    LOCATION.LOCATION LOC1,
                    LOCATION.LOCATION LOC2,
                    LOCATION.LOCATION LOC3,
                    LOCATION.LOCATION LOC4,
                    PSS.PAYMENT_SUBTYPE PST
              WHERE TLI.TRAN_ID = TR.TRAN_ID
                AND TR.POS_PTA_ID = PTA.POS_PTA_ID
                AND PTA.POS_ID = PS.POS_ID
                AND PS.LOCATION_ID = LOC4.LOCATION_ID
                AND LOC4.LOCATION_TYPE_ID = 18
                AND LOC4.PARENT_LOCATION_ID = LOC3.LOCATION_ID
                AND LOC3.PARENT_LOCATION_ID = LOC2.LOCATION_ID
                AND LOC2.PARENT_LOCATION_ID = LOC1.LOCATION_ID
                AND PTA.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
                AND PST.CLIENT_PAYMENT_TYPE_CD IN('M')
                AND TR.TRAN_STATE_CD IN('D', 'C', '8', 'E', 'A')
                AND TR.TRAN_UPLOAD_TS < l_max_ts
                AND TR.TRAN_UPLOAD_TS >= l_last_ts
                AND (TR.TRAN_UPLOAD_TS > l_last_ts
                    OR TR.TRAN_ID > l_last_sb_id);
           COMMIT;
   END;
END;
/