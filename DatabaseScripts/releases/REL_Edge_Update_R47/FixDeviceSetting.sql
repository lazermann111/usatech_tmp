set serveroutput on
     
BEGIN
	FOR l_dev in (select distinct device_id from yhe.tmp_device_setting_08252015) LOOP
		FOR l_rec IN (
			SELECT ds.device_setting_parameter_cd,DS.DEVICE_SETTING_VALUE, default_temp.config_template_setting_value default_DEVICE_SETTING_VALUE
	    from device.device_setting ds
	    JOIN (select CTS.device_setting_parameter_cd,cts.config_template_setting_value from device.config_template_setting cts join device.config_template ct on cts.config_template_id=ct.config_template_id
	where ct.config_template_name='G5-DEFAULT-CFG') default_temp
	    on ds.device_setting_parameter_cd=default_temp.device_setting_parameter_cd
			JOIN DEVICE.DEVICE D ON DS.DEVICE_ID = D.DEVICE_ID
			JOIN DEVICE.CONFIG_TEMPLATE_SETTING CTS ON DECODE(D.DEVICE_TYPE_ID, 1, 0, D.DEVICE_TYPE_ID) = CTS.DEVICE_TYPE_ID
				AND DS.DEVICE_SETTING_PARAMETER_CD = CTS.DEVICE_SETTING_PARAMETER_CD
				AND CTS.FIELD_OFFSET BETWEEN 0 AND 511
	      and NVL(length(DS.DEVICE_SETTING_VALUE),0)!=NVL(length(default_temp.config_template_setting_value),0)
	    where ds.device_id=l_dev.device_id
			ORDER BY CTS.FIELD_OFFSET
		) 
		LOOP
			update DEVICE.DEVICE_SETTING set DEVICE_SETTING_VALUE=l_rec.default_DEVICE_SETTING_VALUE
			where device_id=l_dev.device_id and device_setting_parameter_cd=l_rec.device_setting_parameter_cd;
			commit;
		END LOOP;
			DBMS_OUTPUT.PUT_LINE('Updated setting for ' || l_dev.device_id);
	END LOOP;
         
END;