GRANT SELECT,DELETE ON REPORT.RESEND_REPORTS TO USAT_RPT_REQ_ROLE;

ALTER TABLE REPORT.TRANS_TYPE ADD PROCESS_FEE_TRANS_TYPE_ID NUMBER;
UPDATE REPORT.TRANS_TYPE SET PROCESS_FEE_TRANS_TYPE_ID = TRANS_TYPE_ID;
COMMIT;
ALTER TABLE REPORT.TRANS_TYPE MODIFY (PROCESS_FEE_TRANS_TYPE_ID NOT NULL);
ALTER TABLE REPORT.TRANS_TYPE ADD CONSTRAINT FK_TRANS_TYPE_PF_TRANS_TYPE FOREIGN KEY(PROCESS_FEE_TRANS_TYPE_ID) REFERENCES REPORT.TRANS_TYPE(TRANS_TYPE_ID);

INSERT INTO REPORT.DISPLAY
VALUES(7,'Client','CLIENT',NULL);
INSERT INTO REPORT.DISPLAY
VALUES(8,'Specifics','DESCRIPTION',NULL);

COMMIT;

INSERT INTO FOLIO_CONF.JOIN_FILTER(JOIN_FILTER_ID,FROM_TABLE,TO_TABLE,JOIN_EXPRESSION,JOIN_TYPE,ACTIVE_FLAG,JOIN_CARDINALITY) 
    VALUES(2257,'REPORT.REGION[PARENT]','REPORT.REGION','REPORT.REGION[PARENT].REGION_ID = REPORT.REGION.PARENT_REGION_ID','RIGHT','Y','<');

INSERT INTO FOLIO_CONF.FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
 VALUES(9503, 'Parent Region Name','','REPORT.REGION[PARENT].REGION_NAME','UPPER(REPORT.REGION[PARENT].REGION_NAME)','','VARCHAR','VARCHAR',50,7,'Y');   

INSERT INTO FOLIO_CONF.FIELD(FIELD_ID, FIELD_LABEL, FIELD_DESC, DISPLAY_EXPRESSION, SORT_EXPRESSION,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
DISPLAY_FORMAT, DISPLAY_SQL_TYPE, SORT_SQL_TYPE, IMPORTANCE, FIELD_CATEGORY_ID, ACTIVE_FLAG)
 VALUES(9504, 'Terminal Client','','REPORT.TERMINAL.CLIENT','REPORT.TERMINAL.CLIENT','','VARCHAR','VARCHAR',50,7,'Y');   
 
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID, USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 1,9503,NULL);
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID, USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 2,9503,NULL);
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID, USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 3,9503,NULL);
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID, USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 4,9503,1);
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID, USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 5,9503,1);
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID, USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 8,9503,1);

INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID, USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 1,9504,NULL);
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID, USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 2,9504,NULL);
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID, USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 3,9504,NULL);
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID, USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 4,9504,1);
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID, USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 5,9504,1);
INSERT INTO FOLIO_CONF.FIELD_PRIV (FIELD_PRIV_ID, USER_GROUP_ID, FIELD_ID, FILTER_GROUP_ID)
 VALUES(FOLIO_CONF.SEQ_FIELD_PRIV_ID.NEXTVAL, 8,9504,1);

COMMIT;

INSERT INTO ENGINE.APP_SETTING(APP_SETTING_CD, APP_SETTING_VALUE, APP_SETTING_DESC)
SELECT 'G9_MIN_FIRMWARE_VERSION_FOR_CARD_READER_UPGRADES', '2.04.010', 'Minimum required G9 firmware version for internal file transfer card reader upgrades'
FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM ENGINE.APP_SETTING WHERE APP_SETTING_CD = 'G9_MIN_FIRMWARE_VERSION_FOR_CARD_READER_UPGRADES');
COMMIT;

INSERT INTO REPORT.FILE_CACHE_STATE(FILE_CACHE_STATE_ID, FILE_CACHE_STATE_DESC)
    SELECT 0, 'Loading' FROM DUAL
    WHERE NOT EXISTS(SELECT 1 FROM REPORT.FILE_CACHE_STATE WHERE FILE_CACHE_STATE_ID = 0);

INSERT INTO REPORT.FILE_CACHE_STATE(FILE_CACHE_STATE_ID, FILE_CACHE_STATE_DESC)
    SELECT 2, 'Processing' FROM DUAL
    WHERE NOT EXISTS(SELECT 1 FROM REPORT.FILE_CACHE_STATE WHERE FILE_CACHE_STATE_ID = 2);

INSERT INTO REPORT.FILE_CACHE_STATE(FILE_CACHE_STATE_ID, FILE_CACHE_STATE_DESC)
    SELECT 3, 'Complete' FROM DUAL
    WHERE NOT EXISTS(SELECT 1 FROM REPORT.FILE_CACHE_STATE WHERE FILE_CACHE_STATE_ID = 3);
     
COMMIT;

CREATE TABLE PSS.DOWNGRADE_REASON(
  DOWNGRADE_REASON_CD VARCHAR2(4) NOT NULL,
  DOWNGRADE_REASON_DESC VARCHAR2(100) NOT NULL,
  CONSTRAINT PK_DOWNGRADE_REASON PRIMARY KEY (DOWNGRADE_REASON_CD)
) TABLESPACE PSS_DATA;
    
ALTER TABLE PSS.AUTH ADD (
  PARTIAL_REVERSAL_FLAG CHAR(1),
  CARD_TYPE_CD CHAR(2),
  INTERCHANGE_CD CHAR(4),
  DOWNGRADE_REASON_CD VARCHAR2(4),
  FEE_DESC VARCHAR2(4000),
  FEE_AMOUNT NUMBER,
  DFR_FILE_CACHE_ID NUMBER(20),
  BATCH_DATE DATE,
  ENTITY_NUM NUMBER(6),
  SUBMISSION_NUM VARCHAR2(11), 
  RECORD_NUM VARCHAR2(10),
  CONSTRAINT FK_AUTH_DOWNGRADE_REASON_CD FOREIGN KEY(DOWNGRADE_REASON_CD) REFERENCES PSS.DOWNGRADE_REASON(DOWNGRADE_REASON_CD));

ALTER TABLE PSS.REFUND ADD (
  CARD_TYPE_CD CHAR(2),
  INTERCHANGE_CD CHAR(4),
  DOWNGRADE_REASON_CD VARCHAR2(4),
  FEE_DESC VARCHAR2(4000),
  FEE_AMOUNT NUMBER,
  DFR_FILE_CACHE_ID NUMBER(20),
  BATCH_DATE DATE,
  ENTITY_NUM NUMBER(6),
  SUBMISSION_NUM VARCHAR2(11),
  RECORD_NUM VARCHAR2(10),
  CONSTRAINT FK_REFUND_DOWNGRADE_REASON_CD FOREIGN KEY(DOWNGRADE_REASON_CD) REFERENCES PSS.DOWNGRADE_REASON(DOWNGRADE_REASON_CD));

GRANT SELECT ON DEVICE.APP_TYPE TO PSS;
GRANT SELECT ON DEVICE.DEVICE_SETTING TO PSS;

INSERT INTO PSS.SALE_RESULT(SALE_RESULT_ID, SALE_RESULT_DESC)
    SELECT 5, 'Cancelled by device' FROM DUAL
     WHERE NOT EXISTS(SELECT 1 FROM PSS.SALE_RESULT WHERE SALE_RESULT_ID = 5);

INSERT INTO PSS.REFUND_TYPE(REFUND_TYPE_CD, REFUND_TYPE_DESC)
    SELECT 'R', 'Reversal of Duplicate Submission' FROM DUAL
     WHERE NOT EXISTS(SELECT 1 FROM PSS.REFUND_TYPE WHERE REFUND_TYPE_CD = 'R');

INSERT INTO PSS.TRAN_LINE_ITEM_TYPE(TRAN_LINE_ITEM_TYPE_ID, TRAN_LINE_ITEM_TYPE_DESC, TRAN_LINE_ITEM_TYPE_SIGN_PN, TRAN_LINE_ITEM_TYPE_GROUP_CD, MDB_NUMBER_IND, TYPE_DESC_AS_PRODUCT_IND)
    SELECT 505, 'Reversal of Duplicate Submission', 'N', 'A', 'N', 'N'  FROM DUAL
     WHERE NOT EXISTS(SELECT 1 FROM PSS.TRAN_LINE_ITEM_TYPE WHERE TRAN_LINE_ITEM_TYPE_ID = 505);
                  
COMMIT;
