GRANT SELECT ON PSS.CAMPAIGN_CONSUMER_ACCT TO REPORT WITH GRANT OPTION;

CREATE SEQUENCE REPORT.SEQ_CONSUMER_ACCT_STAT_ID 
MINVALUE 1 MAXVALUE 999999999999999999999999999 
INCREMENT BY 1 START WITH 1 NOCACHE NOORDER NOCYCLE;

GRANT REFERENCES ON PSS.CONSUMER_ACCT TO REPORT;

CREATE TABLE REPORT.CONSUMER_ACCT_STAT(
  	CONSUMER_ACCT_STAT_ID NUMBER(20) NOT NULL, 
  	CONSUMER_ACCT_ID NUMBER(20) NOT NULL,
  	CAMPAIGN_ID NUMBER NOT NULL,
	TRAN_COUNT NUMBER,
	VEND_COUNT NUMBER,
	TRAN_AMOUNT NUMBER(15,2),
	CURRENCY_ID	NUMBER,
	CONVENIENCE_FEE NUMBER(15,2),
	LOYALTY_DISCOUNT NUMBER(15,2),
	CREATED_BY VARCHAR2(30) NOT NULL,
    CREATED_UTC_TS TIMESTAMP NOT NULL,
    LAST_UPDATED_BY VARCHAR2(30) NOT NULL,
    LAST_UPDATED_UTC_TS TIMESTAMP NOT NULL,
  CONSTRAINT PK_CONSUMER_ACCT_STAT PRIMARY KEY(CONSUMER_ACCT_STAT_ID),
  CONSTRAINT FK_CAS_CONSUMER_ACCT_ID FOREIGN KEY(CONSUMER_ACCT_ID) REFERENCES PSS.CONSUMER_ACCT(CONSUMER_ACCT_ID),
  CONSTRAINT FK_CAS_CAMPAIGN_ID FOREIGN KEY(CAMPAIGN_ID) REFERENCES REPORT.CAMPAIGN(CAMPAIGN_ID))
  TABLESPACE REPORT_DATA;
  
CREATE INDEX REPORT.IX_CAS_CONSUMER_ACCT_ID ON REPORT.CONSUMER_ACCT_STAT(CONSUMER_ACCT_ID) TABLESPACE REPORT_INDX ONLINE;
CREATE INDEX REPORT.IX_CAS_CAMPAIGN_ID ON REPORT.CONSUMER_ACCT_STAT(CAMPAIGN_ID) TABLESPACE REPORT_INDX ONLINE;

CREATE OR REPLACE TRIGGER REPORT.TRBI_CONSUMER_ACCT_STAT
BEFORE INSERT ON REPORT.CONSUMER_ACCT_STAT
FOR EACH ROW
BEGIN 
    IF :NEW.CONSUMER_ACCT_STAT_ID IS NULL THEN
        SELECT REPORT.SEQ_CONSUMER_ACCT_STAT_ID.NEXTVAL 
          INTO :NEW.CONSUMER_ACCT_STAT_ID FROM DUAL;
    END IF;
	
    SELECT 
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER,
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER
    INTO 
        :NEW.CREATED_UTC_TS,
        :NEW.CREATED_BY,
        :NEW.LAST_UPDATED_UTC_TS,
        :NEW.LAST_UPDATED_BY
    FROM DUAL;
	
END;
/

CREATE OR REPLACE TRIGGER REPORT.TRBU_CONSUMER_ACCT_STAT
BEFORE UPDATE ON REPORT.CONSUMER_ACCT_STAT 
FOR EACH ROW
BEGIN
     SELECT
        :OLD.CREATED_BY,
        :OLD.CREATED_UTC_TS,
        SYS_EXTRACT_UTC(SYSTIMESTAMP),
        USER
    INTO
        :NEW.CREATED_BY,
        :NEW.CREATED_UTC_TS,
        :NEW.LAST_UPDATED_UTC_TS,
        :NEW.LAST_UPDATED_BY
    FROM DUAL;
END;
/

GRANT SELECT ON REPORT.CONSUMER_ACCT_STAT to USALIVE_APP_ROLE;

BEGIN
	FOR l_rec IN (select ca.created_ts,cur.currency_id, cca.consumer_acct_id, cca.campaign_id from pss.campaign_consumer_acct cca join report.campaign c on cca.campaign_id=c.campaign_id 
join report.campaign cam on c.campaign_id=cam.campaign_id
join pss.consumer_acct ca on ca.consumer_acct_id=cca.consumer_acct_id
join corp.currency cur on ca.currency_cd=cur.currency_CODE
where cam.promo_code is not null) LOOP
LOOP
                   UPDATE REPORT.CONSUMER_ACCT_STAT cas
                       SET (TRAN_COUNT,VEND_COUNT,TRAN_AMOUNT,CONVENIENCE_FEE ,LOYALTY_DISCOUNT)=
		  (select count(1), sum(quantity), sum(TOTAL_AMOUNT), sum(convenience_fee), sum(loyalty_discount)
		  from report.activity_ref ar where ar.CONSUMER_ACCT_ID=cas.CONSUMER_ACCT_ID
                       and ar.CURRENCY_ID=cas.CURRENCY_ID
                       AND ar.CAMPAIGN_ID = cas.campaign_id and ar.tran_date>l_rec.created_ts)
                     WHERE cas.CONSUMER_ACCT_ID =l_rec.CONSUMER_ACCT_ID
                       AND cas.CURRENCY_ID = l_rec.currency_id
                       AND cas.CAMPAIGN_ID = l_rec.campaign_id;
                    EXIT WHEN SQL%ROWCOUNT > 0;
                    BEGIN
                        INSERT INTO REPORT.CONSUMER_ACCT_STAT(
                            CONSUMER_ACCT_ID,
                            TRAN_COUNT,
                            VEND_COUNT, 
                            TRAN_AMOUNT,
                            CURRENCY_ID, 
                            CONVENIENCE_FEE,
                            LOYALTY_DISCOUNT,
                            CAMPAIGN_ID) 
                        select 
                        l_rec.CONSUMER_ACCT_ID,
                        count(1), 
                        sum(quantity), 
                        sum(TOTAL_AMOUNT),
                        l_rec.currency_id,
                        sum(convenience_fee), 
                        sum(loyalty_discount),
                        l_rec.campaign_id
                       from report.activity_ref ar 
                       where ar.CONSUMER_ACCT_ID=l_rec.CONSUMER_ACCT_ID
                       and ar.CURRENCY_ID=l_rec.currency_id
                       AND ar.CAMPAIGN_ID = l_rec.campaign_id 
                       and ar.tran_date>l_rec.created_ts
                       group by l_rec.CONSUMER_ACCT_ID,l_rec.currency_id, l_rec.campaign_id;
                        EXIT;    
                    EXCEPTION
                        WHEN DUP_VAL_ON_INDEX THEN
                            NULL;
                    END;
                END LOOP; 
		 
                       
		 commit;
	END LOOP;
	
END;
/
   
DECLARE
	LN_REPORT_ID REPORT.REPORTS.REPORT_ID%TYPE := -1;
BEGIN
	SELECT MAX(REPORT_ID) INTO LN_REPORT_ID
	FROM REPORT.REPORTS WHERE REPORT_NAME = 'MORE Credit Accounts';
	
	IF LN_REPORT_ID > 0 THEN
		RETURN;
	END IF;

	SELECT REPORT.REPORTS_SEQ.NEXTVAL INTO LN_REPORT_ID FROM DUAL;
	
	INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
	VALUES( LN_REPORT_ID, 'MORE Credit Accounts', 6, 0, 'MORE Credit Accounts', 'Report that shows detailed more credit card accounts information', 'N', 0);
	INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
	VALUES( LN_REPORT_ID, 'query', 'SELECT C.CONSUMER_FNAME "First Name", C.CONSUMER_LNAME "Last Name", CA.NICK_NAME "Card Nickname", CA.CONSUMER_ACCT_CD "Card Number", C.CONSUMER_EMAIL_ADDR1 
"Email Address", C.CONSUMER_CELL_PHONE_NUM "Mobile Phone Number",
CA.CONSUMER_ACCT_ACTIVE_YN_FLAG "Account Enabled", DBADMIN.UTC_TO_LOCAL_DATE(CC.CREATED_UTC_TS) "Registration Time", CC.CREDENTIAL_ACTIVE_FLAG "Account 
Activated", 
CC.ACTIVATED_TS "Activation Time", CAS.CONSUMER_ACCT_SUB_TYPE_DESC "Account Sub Type",
SUM(CAS.LOYALTY_DISCOUNT) "Loyalty Discount Total",
SUM(CAS.TRAN_AMOUNT) "Total Tran Amount",
SUM(CAS.TRAN_COUNT) "Total Tran Count"
FROM PSS.CONSUMER C
JOIN PSS.CONSUMER_ACCT CA ON C.CONSUMER_ID = CA.CONSUMER_ID
JOIN PSS.CAMPAIGN_CONSUMER_ACCT CCA on CA.CONSUMER_ACCT_ID=CCA.CONSUMER_ACCT_ID
JOIN PSS.CONSUMER_ACCT_SUB_TYPE CAS ON CA.CONSUMER_ACCT_SUB_TYPE_ID = CAS.CONSUMER_ACCT_SUB_TYPE_ID
JOIN PSS.CONSUMER_CREDENTIAL CC ON C.CONSUMER_ID = CC.CONSUMER_ID
JOIN REPORT.VW_USER_CONSUMER_ACCT VUCA ON VUCA.CONSUMER_ACCT_ID=CA.CONSUMER_ACCT_ID
JOIN REPORT.USER_LOGIN UL ON UL.USER_ID=VUCA.USER_ID 
JOIN REPORT.CAMPAIGN CAM ON CCA.CAMPAIGN_ID=CAM.CAMPAIGN_ID and (UL.CUSTOMER_ID = 0 OR UL.CUSTOMER_ID = CAM.CUSTOMER_ID)
LEFT OUTER JOIN REPORT.CONSUMER_ACCT_STAT CAS ON CA.CONSUMER_ACCT_ID=CAS.CONSUMER_ACCT_ID and CAS.CAMPAIGN_ID=CAM.CAMPAIGN_ID
WHERE CA.CONSUMER_ACCT_TYPE_ID = 5 
AND UL.USER_ID = ? GROUP BY C.CONSUMER_FNAME, C.CONSUMER_LNAME, CA.NICK_NAME, CA.CONSUMER_ACCT_CD, C.CONSUMER_EMAIL_ADDR1, C.CONSUMER_CELL_PHONE_NUM, CA.CONSUMER_ACCT_ACTIVE_YN_FLAG, DBADMIN.UTC_TO_LOCAL_DATE(CC.CREATED_UTC_TS), CC.CREDENTIAL_ACTIVE_FLAG, CC.ACTIVATED_TS, CAS.CONSUMER_ACCT_SUB_TYPE_DESC
ORDER BY C.CONSUMER_FNAME, C.CONSUMER_LNAME');
	
	insert into report.report_param values(LN_REPORT_ID, 'params.userId','{u}');
	insert into report.report_param values(LN_REPORT_ID, 'paramNames','userId');
	insert into report.report_param values(LN_REPORT_ID, 'paramTypes','NUMERIC');
	insert into report.report_param values(LN_REPORT_ID, 'isSQLFolio','true');
	
	insert into web_content.web_link values(web_content.seq_web_link_id.nextval, 'MORE Credit Accounts', './run_sql_folio_report_async.i?basicReportId='||LN_REPORT_ID,'MORE Credit Accounts','Daily Operations','P', 0);
	insert into web_content.web_link_requirement values(web_content.seq_web_link_id.currval, 1);
	commit;

END;
/
