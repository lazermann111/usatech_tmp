set serveroutput on
DECLARE
	pn_host_port_num HOST.HOST_PORT_NUM%TYPE:=2;
	pn_host_position_num HOST.HOST_POSITION_NUM%TYPE:=0;
	pn_host_type_id HOST.HOST_TYPE_ID%TYPE:=206;
	pv_host_label_cd HOST.HOST_LABEL_CD%TYPE;
	pn_host_equipment_id HOST_EQUIPMENT.HOST_EQUIPMENT_ID%TYPE:=1051810;
	pn_host_est_complete_minut HOST.HOST_EST_COMPLETE_MINUT%TYPE:=0;
	pn_host_id HOST.HOST_ID%TYPE;
    pn_existing_cnt PLS_INTEGER;
    l_count NUMBER;
BEGIN
	FOR l_dev in (select d.device_id, cdma_d.device_serial_cd, cdma_d.host_serial_cd
from yhe.cdma_device_08272015 cdma_d join device.device d on cdma_d.device_serial_cd=d.device_serial_cd 
where d.device_active_yn_flag='Y'
and not exists(select 1 from device.host h where h.device_id=d.device_id and h.host_type_id in (204,206)))
		LOOP
			select count(1) into l_count from device.host where device_id=l_dev.device_id and host_type_id in (204,206);
			IF l_count > 0 THEN
				DBMS_OUTPUT.PUT_LINE('Skip device_id=' || l_dev.device_id||' device_serial_cd='||l_dev.device_serial_cd);
			ELSE
				PKG_DEVICE_CONFIGURATION.UPSERT_HOST(l_dev.device_id,pn_host_port_num,pn_host_position_num,pn_host_type_id,l_dev.host_serial_cd,pv_host_label_cd,pn_host_equipment_id,pn_host_est_complete_minut,pn_host_id,pn_existing_cnt);
				DBMS_OUTPUT.PUT_LINE('Updated setting for device_id=' || l_dev.device_id||' device_serial_cd='||l_dev.device_serial_cd||' host_serial_cd='||l_dev.host_serial_cd||' pn_host_id='||pn_host_id||' pn_existing_cnt='||pn_existing_cnt);
				commit;
			END IF;
	END LOOP;
         
END;