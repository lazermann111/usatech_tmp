ALTER TABLE REPORT.REGION ADD PARENT_REGION_ID NUMBER;
ALTER TABLE REPORT.REGION ADD CONSTRAINT FK_REGION_PARENT_REGION_ID FOREIGN KEY(PARENT_REGION_ID) REFERENCES REPORT.REGION(REGION_ID);
CREATE INDEX REPORT.IX_REGION_PARENT_REGION_ID ON REPORT.REGION(PARENT_REGION_ID) TABLESPACE REPORT_INDX ONLINE;

ALTER TABLE REPORT.TERMINAL ADD CLIENT varchar2(50);

CREATE INDEX REPORT.IX_TERMINAL_REGION_REGION_ID ON REPORT.TERMINAL_REGION(REGION_ID) TABLESPACE REPORT_INDX ONLINE;
CREATE INDEX REPORT.IX_TERMINAL_CLIENT ON REPORT.TERMINAL(CLIENT) TABLESPACE REPORT_INDX ONLINE;
CREATE INDEX REPORT.IX_LOCATION_DESCRIPTION ON REPORT.LOCATION(DESCRIPTION) TABLESPACE REPORT_INDX ONLINE;

CREATE INDEX PSS.IX_AUTH_STATE_AUTHORITY_TS ON PSS.AUTH(AUTH_STATE_ID, AUTH_AUTHORITY_TS) TABLESPACE PSS_INDX ONLINE LOCAL;
CREATE INDEX PSS.IX_REFUND_AUTHORITY_TS ON PSS.REFUND(REFUND_STATE_ID, REFUND_AUTHORITY_TS) TABLESPACE PSS_INDX ONLINE LOCAL;
