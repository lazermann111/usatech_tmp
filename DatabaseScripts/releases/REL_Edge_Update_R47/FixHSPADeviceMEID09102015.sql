set serveroutput on
DECLARE
	pn_host_port_num HOST.HOST_PORT_NUM%TYPE:=2;
	pn_host_position_num HOST.HOST_POSITION_NUM%TYPE:=0;
	pn_host_type_id HOST.HOST_TYPE_ID%TYPE:=211;
	pv_host_label_cd HOST.HOST_LABEL_CD%TYPE;
	pn_host_equipment_id HOST_EQUIPMENT.HOST_EQUIPMENT_ID%TYPE:=1047596;
	pn_host_est_complete_minut HOST.HOST_EST_COMPLETE_MINUT%TYPE:=0;
	pn_host_id HOST.HOST_ID%TYPE;
    pn_existing_cnt PLS_INTEGER;
    l_count NUMBER;
BEGIN
	FOR l_dev in (select d.device_id, hspa_d.device_serial_cd, hspa_d.host_serial_cd
from yhe.hspa_device_09102015 hspa_d join device.device d on hspa_d.device_serial_cd=d.device_serial_cd 
join device.host h on h.device_id = d.device_id
where d.device_active_yn_flag='Y'
and h.host_port_num = 2
and h.created_by = 'SYSTEM'
and h.last_updated_by = 'SYSTEM'
and h.last_updated_ts > sysdate - 30)
		LOOP
				PKG_DEVICE_CONFIGURATION.UPSERT_HOST(l_dev.device_id,pn_host_port_num,pn_host_position_num,pn_host_type_id,l_dev.host_serial_cd,pv_host_label_cd,pn_host_equipment_id,pn_host_est_complete_minut,pn_host_id,pn_existing_cnt);
				DBMS_OUTPUT.PUT_LINE('Updated setting for device_id=' || l_dev.device_id||' device_serial_cd='||l_dev.device_serial_cd||' host_serial_cd='||l_dev.host_serial_cd||' pn_host_id='||pn_host_id||' pn_existing_cnt='||pn_existing_cnt);
				commit;
	END LOOP;
         
END;