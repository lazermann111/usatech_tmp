-- make manage campaign and set batch close time both customer and internal privs
update report.priv set INTERNAL_EXTERNAL_FLAG='B' where priv_id in (22, 23);
commit;

-- make customer_service ability to give manage_campaign and set batch close time
BEGIN
	FOR l_cur in (select distinct user_id from report.user_privs where priv_id=16)
	LOOP
		insert into report.user_privs values(l_cur.user_id, 22);
		insert into report.user_privs values(l_cur.user_id, 23);
	END LOOP;
	commit;
END;