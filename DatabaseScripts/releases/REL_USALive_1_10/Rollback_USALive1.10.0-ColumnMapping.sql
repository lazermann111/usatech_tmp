drop trigger REPORT.TRBI_COLUMN_MAP_VALUE;
drop trigger REPORT.TRBU_COLUMN_MAP_VALUE;
drop table REPORT.COLUMN_MAP_VALUE;

drop trigger REPORT.TRBI_COLUMN_MAP;
drop trigger REPORT.TRBU_COLUMN_MAP;
drop sequence REPORT.SEQ_COLUMN_MAP_ID;
drop table REPORT.COLUMN_MAP;

ALTER TABLE
   REPORT.TERMINAL
DROP COLUMN COLUMN_MAP_ID;