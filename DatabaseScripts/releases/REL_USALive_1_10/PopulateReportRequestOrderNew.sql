-- this is a change that takes system requested report out of recent reports page
delete from report.report_request_order;

update report.report_request set request_order=null;

update report.report_request_user set request_order=null;

commit;


CREATE UNIQUE INDEX REPORT.UDX_REPORT_REQUEST_ORDER ON REPORT.REPORT_REQUEST(DECODE(TO_CHAR(REQUEST_ORDER), NULL, NULL, PROFILE_ID||'-'||REQUEST_ORDER)) TABLESPACE REPORT_INDX;

CREATE UNIQUE INDEX REPORT.UDX_REPORT_REQUEST_USER_ORDER ON REPORT.REPORT_REQUEST_USER(DECODE(TO_CHAR(REQUEST_ORDER), NULL, NULL, USER_ID||'-'||REQUEST_ORDER)) TABLESPACE REPORT_INDX;

DECLARE
  l_count NUMBER:=0;

BEGIN
	INSERT INTO REPORT.REPORT_REQUEST_ORDER (USER_ID, PROFILE_MAX_REQUEST_ORDER,USER_MAX_REQUEST_ORDER) 
	SELECT UL.USER_ID, NVL(P.PROFILE_MAX_REQUEST,0) PROFILE_MAX_REQUEST_ORDER, NVL(U.USER_MAX_REQUEST_ORDER,0) USER_MAX_REQUEST_ORDER FROM 
	REPORT.USER_LOGIN UL 
	LEFT OUTER JOIN
	(SELECT PROFILE_ID, count(*) PROFILE_MAX_REQUEST FROM REPORT.REPORT_REQUEST WHERE REPORT_REQUEST_TYPE_ID=1 GROUP BY PROFILE_ID) P ON UL.USER_ID=P.PROFILE_ID
	LEFT OUTER JOIN
	(SELECT USER_ID, count(*) USER_MAX_REQUEST_ORDER FROM REPORT.REPORT_REQUEST_USER RRU JOIN REPORT.REPORT_REQUEST RR on RRU.REPORT_REQUEST_ID=RR.REPORT_REQUEST_ID and RR.REPORT_REQUEST_TYPE_ID=1 WHERE PAGE_ID=0 GROUP BY USER_ID) U ON UL.USER_ID=U.USER_ID;
	COMMIT;
	
	FOR l_rec IN (select user_id, profile_max_request_order from report.report_request_order where profile_max_request_order>0 ) LOOP
		FOR l_rec_user IN (select report_request_id from (
select report_request_id from REPORT.REPORT_REQUEST where profile_id=l_rec.user_id and REPORT_REQUEST_TYPE_ID=1 order by report_request_id desc) where rownum <401) LOOP
			update report.report_request set REQUEST_ORDER=l_rec.profile_max_request_order-l_count
			where report_request_id=l_rec_user.report_request_id;
			l_count:=l_count+1;
		END LOOP;
		commit;
		l_count:=0;
	END LOOP;
	
	FOR l_rec IN (select user_id, user_max_request_order from report.report_request_order where user_max_request_order>0 ) LOOP
		FOR l_rec_user IN (select report_request_user_id from (
select report_request_user_id from REPORT.REPORT_REQUEST_USER RRU JOIN REPORT.REPORT_REQUEST RR on RRU.REPORT_REQUEST_ID=RR.REPORT_REQUEST_ID and RR.REPORT_REQUEST_TYPE_ID=1 where user_id=l_rec.user_id and page_id=0 order by report_request_user_id desc) where rownum <401) LOOP
			update report.report_request_user set REQUEST_ORDER=l_rec.user_max_request_order-l_count
			where report_request_user_id=l_rec_user.report_request_user_id;
			l_count:=l_count+1;
		END LOOP;
		commit;
		l_count:=0;
	END LOOP;
	
END;
/


