DELETE FROM ENGINE.APP_SETTING WHERE APP_SETTING_CD = 'EMV_PARAM_SYNC_LOCK';
DELETE FROM ENGINE.APP_SETTING WHERE APP_SETTING_CD = 'EMV_PARAM_SYNC_MAX_DURATION_SEC';

DROP TRIGGER DEVICE.TRAI_FILE_TRANSFER;
DROP TABLE DEVICE.EMV_PARAM_SYNC;
DROP SEQUENCE DEVICE.SEQ_EMV_PARAM_SYNC;

COMMIT;
