CREATE OR REPLACE FUNCTION cal_replication_lag()
  RETURNS integer AS
$$
DECLARE
    byteDiff integer;
    replay_pos text;
BEGIN
    select replay_location into replay_pos from pg_stat_replication;
    select pg_xlog_location_diff(pg_current_xlog_location(), replay_pos) into byteDiff;
    RETURN byteDiff;
END;
$$ LANGUAGE plpgsql;

-- psql -At -c "select cal_replication_lag();"
