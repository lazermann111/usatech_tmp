#!/bin/bash
set -e 
mkdir /opt/USAT/postgres/T2tblspace/main_data_t2
chmod 700 /opt/USAT/postgres/T2tblspace/main_data_t2
mkdir /opt/USAT/postgres/T3tblspace/main_data_t3
chmod 700 /opt/USAT/postgres/T3tblspace/main_data_t3
mkdir /opt/USAT/postgres/tblspace/rdw_data_t1
chmod 700 /opt/USAT/postgres/tblspace/rdw_data_t1
mkdir /opt/USAT/postgres/T2tblspace/rdw_data_t2
chmod 700 /opt/USAT/postgres/T2tblspace/rdw_data_t2
mkdir /opt/USAT/postgres/T3tblspace/rdw_data_t3
chmod 700 /opt/USAT/postgres/T3tblspace/rdw_data_t3

psql -d main -c "CREATE TABLESPACE main_data_t2 LOCATION '/opt/USAT/postgres/T2tblspace/main_data_t2';"
psql -d main -c "CREATE TABLESPACE main_data_t3 LOCATION '/opt/USAT/postgres/T3tblspace/main_data_t3';"
psql -d main -c "CREATE TABLESPACE rdw_data_t1 LOCATION '/opt/USAT/postgres/tblspace/rdw_data_t1';"
psql -d main -c "CREATE TABLESPACE rdw_data_t2 LOCATION '/opt/USAT/postgres/T2tblspace/rdw_data_t2';" 
psql -d main -c "CREATE TABLESPACE rdw_data_t3 LOCATION '/opt/USAT/postgres/T3tblspace/rdw_data_t3';"
