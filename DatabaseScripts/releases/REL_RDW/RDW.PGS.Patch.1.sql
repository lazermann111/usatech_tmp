drop function RDW.UPDATE_PAID_TS(timestamptz, bigint);

CREATE OR REPLACE FUNCTION RDW.UPDATE_PAID_TS(pd_paid_ts timestamptz, pn_batch_id bigint) RETURNS VOID
AS $$
DECLARE
	l_payment_batch_dim_id BIGINT;
BEGIN
select coalesce(max(payment_batch_dim_id),-1) into l_payment_batch_dim_id from rdw.payment_batch_dim where source_batch_id=pn_batch_id;	
IF l_payment_batch_dim_id >-1 THEN

	update rdw.ledger_item_fact set PAID_DATE_DIM_ID=GET_DATE_DIM_ID(pd_paid_ts), PAID_TS=pd_paid_ts 
	where payment_batch_dim_id=l_payment_batch_dim_id and PAID_DATE_DIM_ID=0;

	update rdw.ledger_item_detail_dim set PAID_TS=pd_paid_ts
	where item_detail_dim_id in (select item_detail_dim_id from rdw.ledger_item_fact where payment_batch_dim_id=l_payment_batch_dim_id)
	and PAID_TS IS NULL;
	

	update rdw.tran_item_fact set PAID_DATE_DIM_ID=GET_DATE_DIM_ID(pd_paid_ts), PAID_TS=pd_paid_ts 
	where payment_batch_dim_id=l_payment_batch_dim_id;
	
	update rdw.tran_item_detail_dim set PAID_TS=pd_paid_ts
	where item_detail_dim_id in (select item_detail_dim_id from rdw.tran_item_fact where payment_batch_dim_id=l_payment_batch_dim_id);
	

END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION RDW.UPDATE_DOC_ID(pn_batch_id bigint, pn_doc_id bigint) RETURNS VOID
AS $$
DECLARE
	l_payment_batch_dim_id BIGINT;
BEGIN
	select coalesce(max(payment_batch_dim_id),-1) into l_payment_batch_dim_id from rdw.payment_batch_dim where source_batch_id=pn_batch_id;
IF l_payment_batch_dim_id >-1 THEN
	update rdw.ledger_item_fact set payment_dim_id=pd.payment_dim_id
	from (select payment_dim_id from rdw.payment_dim where source_doc_id=pn_doc_id) pd
	where payment_batch_dim_id=l_payment_batch_dim_id;
	
	update rdw.tran_item_fact set payment_dim_id=pd.payment_dim_id
	from (select payment_dim_id from rdw.payment_dim where source_doc_id=pn_doc_id) pd
	where payment_batch_dim_id=l_payment_batch_dim_id;
END IF;
END;
$$ LANGUAGE plpgsql;