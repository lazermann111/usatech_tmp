-- 23 reports total
-- use this to find mapping select originalR.report_id, originalR.report_name, r.report_id, r.report_name from report.reports r join report.reports originalR on r.report_name='RDW - '||originalR.report_name where r.report_name like 'RDW -%'

SET DEFINE OFF;
ALTER SESSION SET CURRENT_SCHEMA = REPORT;
DECLARE
	l_env VARCHAR2(3); 
	l_user_id NUMBER;
	l_rdw_prefix VARCHAR2(6):='RDW - '; 
	l_report_name VARCHAR2(50 BYTE);
	l_report_id NUMBER;
BEGIN

select NVL(MAX(DECODE(substr(subdomain_url, length('hotchoice') + 1, length(subdomain_url) - length('hotchoice.usatech.com')), '-ecc', 'ECC', '-dev', 'DEV', '-int', 'INT', null, 'PRD')), 'DEV')
  into l_env
  from WEB_CONTENT.subdomain where subdomain_url like 'hotchoice%.usatech.com'
  and subdomain_url not IN('hotchoice-PRD.usatech.com','hotchoice-USA.usatech.com') ;
  
  IF l_env = 'INT' THEN
  	select user_id into l_user_id from report.user_login where user_name='aroyce@usatech.com';
  ELSIF l_env = 'ECC' THEN
  	select user_id into l_user_id from report.user_login where user_name='aroyce@usatech.com';
  ELSIF l_env = 'PRD' THEN
  	select user_id into l_user_id from report.user_login where user_name='aroyce@usatech.com';
  ELSE
  	select user_id into l_user_id from report.user_login where user_name='yhe@usatech.com';
  END IF;
  
l_report_name:='Transaction Data Included in EFT (Tran Id)';

select REPORT.REPORTS_SEQ.NEXTVAL into l_report_id from dual;

INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
select l_report_id, l_rdw_prefix||title, GENERATOR_ID, BATCH_TYPE_ID,l_rdw_prefix||report_name, DESCRIPTION, 'N', l_user_id
from report.reports where report_name=l_report_name;

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id, param_name, param_value from report.report_param where report_id=(select report_id from report.reports where report_name=l_report_name) and param_name<>'query';

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id,'query','SELECT pd_1.TERMINAL_NBR as TERMINAL_NBR,
ld.LOCATION_NAME as LOCATION_NAME,
tidd.REPORT_TRAN_ID as TRAN_ID,
tif.ITEM_TS as TRAN_DATE,
atd.TRAN_TYPE as TRANS_TYPE_NAME, 
cd.SAFE_CARD_NUMBER as CARD_NUMBER,
CASE WHEN add.SETTLE_STATE_ID =2 THEN add.AUTHORITY_TRAN_CD
WHEN add.SETTLE_STATE_ID =3 THEN add.AUTHORITY_TRAN_CD
ELSE NULL END as PAYMENT_APPR_CODE,
round(sum(tif.amount),2)as TOTAL_AMOUNT,
array_to_string(ARRAY_AGG(tlid.TRAN_LINE_ITEM_LABEL|| CASE WHEN tif.UNIT_PRICE is NOT NULL THEN 
''(''||
 CASE WHEN tif.PRODUCT_SERVICE_QUANTITY > 1 THEN
	tif.PRODUCT_SERVICE_QUANTITY ||''*''||cd_1.CURRENCY_SYMBOL||round(tif.UNIT_PRICE, 2)
 ELSE
	cd_1.CURRENCY_SYMBOL||round(tif.UNIT_PRICE, 2)
 END
||'')''
ELSE ''''END),'','') as VEND_COLUMN,
sum(tif.PRODUCT_SERVICE_QUANTITY) as QUANTITY
from RDW.AUTH_DETAIL_DIM add
    JOIN RDW.TRAN_ITEM_FACT tif ON tif.AUTH_DETAIL_DIM_ID = add.AUTH_DETAIL_DIM_ID
    JOIN RDW.POS_DIM pd_1 on tif.POS_DIM_ID=pd_1.POS_DIM_ID and pd_1.SOURCE_SYSTEM_CD=''TE''
	and pd_1.POS_START_TS <= CURRENT_TIMESTAMP AND pd_1.POS_END_TS > CURRENT_TIMESTAMP
    JOIN RDW.TRAN_ITEM_DETAIL_DIM tidd on tif.ITEM_DETAIL_DIM_ID= tidd.ITEM_DETAIL_DIM_ID
    JOIN RDW.TRAN_LINE_ITEM_DIM tlid ON tif.TRAN_LINE_ITEM_DIM_ID = tlid.TRAN_LINE_ITEM_DIM_ID
    JOIN RDW.AUTH_TYPE_DIM atd on tif.AUTH_TYPE_DIM_ID = atd.AUTH_TYPE_DIM_ID
    JOIN RDW.CONSUMER_DIM cd ON tif.CONSUMER_DIM_ID = cd.CONSUMER_DIM_ID
    JOIN RDW.LOCATION_DIM ld ON tif.LOCATION_DIM_ID = ld.LOCATION_DIM_ID
    JOIN RDW.CURRENCY_DIM cd_1 ON tif.CURRENCY_DIM_ID = cd_1.CURRENCY_DIM_ID
JOIN RDW.PAYMENT_DIM pd ON tif.PAYMENT_DIM_ID = pd.PAYMENT_DIM_ID
where pd.SOURCE_DOC_ID = {x}
group by pd_1.TERMINAL_NBR,
ld.LOCATION_NAME,
tidd.REPORT_TRAN_ID,
tif.ITEM_TS,
atd.TRAN_TYPE,
cd.SAFE_CARD_NUMBER,
CASE WHEN add.SETTLE_STATE_ID =2 THEN add.AUTHORITY_TRAN_CD
WHEN add.SETTLE_STATE_ID =3 THEN add.AUTHORITY_TRAN_CD
ELSE NULL END' 
from dual;

l_report_name:='Transaction Data Export (Tran Id)';

select REPORT.REPORTS_SEQ.NEXTVAL into l_report_id from dual;

INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
select l_report_id, l_rdw_prefix||title, GENERATOR_ID, BATCH_TYPE_ID,l_rdw_prefix||report_name, DESCRIPTION, 'N', l_user_id
from report.reports where report_name=l_report_name;

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id, param_name, param_value from report.report_param where report_id=(select report_id from report.reports where report_name=l_report_name) and param_name<>'query';

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id,'query','SELECT tidd.DEVICE_SERIAL_CD as EPORT_SERIAL_NUM,
tidd.REF_NBR as TRAN_ID,
atd.TRAN_TYPE_CODE, 
cd.SAFE_CARD_NUMBER as CARD_NUMBER,
round(sum(tif.amount),2) as TOTAL_AMOUNT,
array_to_string(ARRAY_AGG(tlid.TRAN_LINE_ITEM_LABEL),'','') as VEND_COLUMN,
sum(tif.PRODUCT_SERVICE_QUANTITY) as QUANTITY,
to_char(tif.ITEM_TS, ''MM/DD/YYYY'') as TRAN_DATE,
TO_CHAR(tif.ITEM_TS, ''HH24:MI:SS'') as TRAN_TIME
from RDW.AUTH_DETAIL_DIM add
    JOIN RDW.TRAN_ITEM_FACT tif ON tif.AUTH_DETAIL_DIM_ID = add.AUTH_DETAIL_DIM_ID
    JOIN RDW.TRAN_ITEM_DETAIL_DIM tidd ON tif.ITEM_DETAIL_DIM_ID = tidd.ITEM_DETAIL_DIM_ID
    JOIN RDW.TRAN_LINE_ITEM_DIM tlid ON tif.TRAN_LINE_ITEM_DIM_ID = tlid.TRAN_LINE_ITEM_DIM_ID
    JOIN RDW.CONSUMER_DIM cd ON tif.CONSUMER_DIM_ID = cd.CONSUMER_DIM_ID
    JOIN RDW.CURRENCY_DIM cd_1 ON tif.CURRENCY_DIM_ID = cd_1.CURRENCY_DIM_ID
    JOIN RDW.AUTH_TYPE_DIM atd on tif.AUTH_TYPE_DIM_ID =  atd.AUTH_TYPE_DIM_ID
JOIN RDW.USER_LOCATION_PRIV ulp join RDW.LOCATION_BRIDGE lb on ulp.LOCATION_DIM_ID=lb.ANCESTOR_LOCATION_DIM_ID and ulp.active_flag=''Yes''
JOIN RDW.LOCATION_DIM ld on lb.DESCENDENT_LOCATION_DIM_ID=ld.LOCATION_DIM_ID
ON tif.LOCATION_DIM_ID= ld.LOCATION_DIM_ID 
where tidd.EXPORT_BATCH_NUM = {x}
and ulp.user_id= {u}
group by
tidd.DEVICE_SERIAL_CD ,
tidd.REF_NBR,
atd.TRAN_TYPE_CODE,
cd.SAFE_CARD_NUMBER ,
to_char(tif.ITEM_TS, ''MM/DD/YYYY''),
TO_CHAR(tif.ITEM_TS, ''HH24:MI:SS'')' 
from dual;

l_report_name:='Zero Trans Report - No Headers';

select REPORT.REPORTS_SEQ.NEXTVAL into l_report_id from dual;

INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
select l_report_id, l_rdw_prefix||title, GENERATOR_ID, BATCH_TYPE_ID,l_rdw_prefix||report_name, DESCRIPTION, 'N', l_user_id
from report.reports where report_name=l_report_name;

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id, param_name, param_value from report.report_param where report_id=(select report_id from report.reports where report_name=l_report_name) and param_name<>'query';

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id,'query','select distinct ld.LOCATION_NAME as LOCATION_NAME, pd.TERMINAL_NBR as TERMINAL_NBR
from RDW.TRAN_ITEM_FACT tif JOIN RDW.POS_DIM pd on tif.POS_DIM_ID=pd.POS_DIM_ID and pd.SOURCE_SYSTEM_CD=''TE''
and pd.POS_START_TS <= CURRENT_TIMESTAMP AND pd.POS_END_TS > CURRENT_TIMESTAMP
JOIN RDW.USER_LOCATION_PRIV ulp join RDW.LOCATION_BRIDGE lb on ulp.LOCATION_DIM_ID=lb.ANCESTOR_LOCATION_DIM_ID and ulp.active_flag=''Yes''
JOIN RDW.LOCATION_DIM ld on lb.DESCENDENT_LOCATION_DIM_ID=ld.LOCATION_DIM_ID
ON tif.LOCATION_DIM_ID= ld.LOCATION_DIM_ID
where not exists ( select * from RDW.TRAN_ITEM_FACT tif2 where tif2.POS_DIM_ID=tif.POS_DIM_ID and tif2.ITEM_TS >date_trunc(''day'', CURRENT_TIMESTAMP - 
interval ''2 day''))
and ulp.user_id= {u}' 
from dual;

l_report_name:='EFT Data Export';

select REPORT.REPORTS_SEQ.NEXTVAL into l_report_id from dual;

INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
select l_report_id, l_rdw_prefix||title, GENERATOR_ID, BATCH_TYPE_ID,l_rdw_prefix||report_name, DESCRIPTION, 'N', l_user_id
from report.reports where report_name=l_report_name;

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id, param_name, param_value from report.report_param where report_id=(select report_id from report.reports where report_name=l_report_name) and param_name<>'query';

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id,'query','select p.SOURCE_DOC_ID as EFT_ID,
p.CUSTOMER_BANK_ID as CUSTOMER_BANK_ID,
SUM(CASE WHEN p.PAYMENT_ENTRY_TYPE_CD =''CC'' and p.ITEM_PAYABLE_FLAG = ''Yes'' THEN p.AMOUNT ELSE NULL END) as CREDIT_AMOUNT,
SUM(CASE WHEN p.PAYMENT_ENTRY_TYPE_CD IN( ''PF'',''SB'') and p.ITEM_PAYABLE_FLAG = ''Yes'' THEN p.AMOUNT ELSE NULL END) as PROCESS_FEE_AMOUNT,
SUM(NULLIF(COALESCE(CASE WHEN p.PAYMENT_ENTRY_TYPE_CD =''SF'' and p.ITEM_PAYABLE_FLAG = ''Yes'' THEN p.AMOUNT ELSE NULL 
END,0)+LEAST(
COALESCE(CASE WHEN p.PAYMENT_ENTRY_TYPE_CD =''NR'' and p.ITEM_PAYABLE_FLAG = ''Yes'' THEN p.AMOUNT ELSE NULL END,0),0),0)
) as SERVICE_FEE_AMOUNT,
SUM(CASE WHEN p.PAYMENT_ENTRY_TYPE_CD IN(''RF'', ''CB'') and p.ITEM_PAYABLE_FLAG = ''Yes'' THEN p.AMOUNT ELSE NULL END) as REFUND_CHARGEBACK_AMOUNT,
SUM(CASE WHEN p.PAYMENT_ENTRY_TYPE_CD =''AD'' and p.ITEM_PAYABLE_FLAG = ''Yes'' THEN p.AMOUNT ELSE NULL END) as ADJUST_AMOUNT,
SUM(CASE WHEN p.PAYMENT_ENTRY_TYPE_CD =''CC'' and p.ITEM_PAYABLE_FLAG = ''No'' THEN -p.AMOUNT ELSE NULL END) as FAILED_AMOUNT,
SUM(CASE WHEN p.PAYMENT_ENTRY_TYPE_CD <>''NR'' and p.ITEM_PAYABLE_FLAG = ''Yes'' THEN p.AMOUNT ELSE NULL END
+LEAST(COALESCE(CASE WHEN p.PAYMENT_ENTRY_TYPE_CD =''NR'' and p.ITEM_PAYABLE_FLAG = ''Yes'' THEN p.AMOUNT ELSE NULL END,0),0)) as NET_AMOUNT,
RDW.GET_ADJUST_DESC(p.SOURCE_DOC_ID)
from (SELECT pd.SOURCE_DOC_ID,
lidd.CUSTOMER_BANK_ID,
pbd.PAYMENT_SCHEDULE_ID,
CASE WHEN pbd.PAYMENT_SCHEDULE_ID = 5 and itd.PAYMENT_ENTRY_TYPE_CD IN(''CC'', ''PF'') THEN ''AD'' 
--WHEN lidd.FEE_FREQUENCY_ID = 6 THEN ''NR'' 
ELSE itd.PAYMENT_ENTRY_TYPE_CD END as PAYMENT_ENTRY_TYPE_CD,
lidd.ITEM_PAYABLE_FLAG,
SUM(lif.AMOUNT) AMOUNT
from RDW.ITEM_TYPE_DIM itd
    JOIN RDW.LEDGER_ITEM_FACT lif ON lif.ITEM_TYPE_DIM_ID = itd.ITEM_TYPE_DIM_ID
    JOIN RDW.LEDGER_ITEM_DETAIL_DIM lidd ON lif.ITEM_DETAIL_DIM_ID = lidd.ITEM_DETAIL_DIM_ID
    JOIN RDW.PAYMENT_DIM pd ON lif.PAYMENT_DIM_ID = pd.PAYMENT_DIM_ID
    JOIN RDW.PAYMENT_BATCH_DIM pbd on lif.PAYMENT_BATCH_DIM_ID=pbd.PAYMENT_BATCH_DIM_ID
    where pd.STATUS in (''P'', ''S'')  
    and (pbd.PAYMENT_SCHEDULE_ID<>7 or itd.PAYMENT_ENTRY_TYPE_CD <> ''AD'')
group by pd.SOURCE_DOC_ID,lidd.CUSTOMER_BANK_ID,pbd.PAYMENT_SCHEDULE_ID, itd.PAYMENT_ENTRY_TYPE_CD, lidd.ITEM_PAYABLE_FLAG) p
where p.SOURCE_DOC_ID = {x}
group by p.SOURCE_DOC_ID,
p.CUSTOMER_BANK_ID' 
from dual;

l_report_name:='EFT Trans Reference Export';

select REPORT.REPORTS_SEQ.NEXTVAL into l_report_id from dual;

INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
select l_report_id, l_rdw_prefix||title, GENERATOR_ID, BATCH_TYPE_ID,l_rdw_prefix||report_name, DESCRIPTION, 'N', l_user_id
from report.reports where report_name=l_report_name;

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id, param_name, param_value from report.report_param where report_id=(select report_id from report.reports where report_name=l_report_name) and param_name<>'query';

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id,'query','select pd.SOURCE_DOC_ID as EFT_ID,
tidd.REF_NBR as REF_NBR
from RDW.TRAN_ITEM_FACT tif JOIN RDW.TRAN_ITEM_DETAIL_DIM tidd ON tif.ITEM_DETAIL_DIM_ID = tidd.ITEM_DETAIL_DIM_ID
JOIN RDW.PAYMENT_DIM pd ON tif.PAYMENT_DIM_ID = pd.PAYMENT_DIM_ID
where pd.SOURCE_DOC_ID = {x}
and tidd.ITEM_PAYABLE_FLAG in (''Yes'', ''No'')' 
from dual;

l_report_name:='Transaction Data Included in EFT';

select REPORT.REPORTS_SEQ.NEXTVAL into l_report_id from dual;

INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
select l_report_id, l_rdw_prefix||title, GENERATOR_ID, BATCH_TYPE_ID,l_rdw_prefix||report_name, DESCRIPTION, 'N', l_user_id
from report.reports where report_name=l_report_name;

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id, param_name, param_value from report.report_param where report_id=(select report_id from report.reports where report_name=l_report_name) and param_name<>'query';

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id,'query','SELECT pd_1.TERMINAL_NBR as TERMINAL_NBR,
ld.LOCATION_NAME as LOCATION_NAME,
tidd.REF_NBR as REF_NBR,
tif.ITEM_TS as TRAN_DATE,
atd.TRAN_TYPE as TRANS_TYPE_NAME,
cd.SAFE_CARD_NUMBER as CARD_NUMBER,
add.AUTHORITY_TRAN_CD as PAYMENT_APPR_CODE,
round(sum(tif.amount),2) as TOTAL_AMOUNT,
array_to_string(ARRAY_AGG(tlid.TRAN_LINE_ITEM_LABEL|| CASE WHEN tif.UNIT_PRICE is NOT NULL THEN 
''(''||
 CASE WHEN tif.PRODUCT_SERVICE_QUANTITY > 1 THEN
	tif.PRODUCT_SERVICE_QUANTITY ||''*''||cd_1.CURRENCY_SYMBOL||round(tif.UNIT_PRICE, 2)
 ELSE
	cd_1.CURRENCY_SYMBOL||round(tif.UNIT_PRICE, 2)
 END
||'')''
ELSE ''''END),'','') as VEND_COLUMN,
sum(tif.PRODUCT_SERVICE_QUANTITY) as QUANTITY
from RDW.AUTH_DETAIL_DIM add
    JOIN RDW.TRAN_ITEM_FACT tif ON tif.AUTH_DETAIL_DIM_ID = add.AUTH_DETAIL_DIM_ID
    JOIN RDW.POS_DIM pd_1 on tif.POS_DIM_ID=pd_1.POS_DIM_ID and pd_1.SOURCE_SYSTEM_CD=''TE''
	and pd_1.POS_START_TS <= CURRENT_TIMESTAMP AND pd_1.POS_END_TS > CURRENT_TIMESTAMP
    JOIN RDW.AUTH_TYPE_DIM atd on tif.AUTH_TYPE_DIM_ID = atd.AUTH_TYPE_DIM_ID
    JOIN RDW.TRAN_ITEM_DETAIL_DIM tidd ON tif.ITEM_DETAIL_DIM_ID = tidd.ITEM_DETAIL_DIM_ID
    JOIN RDW.TRAN_LINE_ITEM_DIM tlid ON tif.TRAN_LINE_ITEM_DIM_ID = tlid.TRAN_LINE_ITEM_DIM_ID
    JOIN RDW.CONSUMER_DIM cd ON tif.CONSUMER_DIM_ID = cd.CONSUMER_DIM_ID
    JOIN RDW.CURRENCY_DIM cd_1 ON tif.CURRENCY_DIM_ID = cd_1.CURRENCY_DIM_ID
    JOIN RDW.LOCATION_DIM ld ON tif.LOCATION_DIM_ID = ld.LOCATION_DIM_ID
    JOIN RDW.PAYMENT_DIM pd ON tif.PAYMENT_DIM_ID = pd.PAYMENT_DIM_ID
where pd.SOURCE_DOC_ID = {x}
group by
pd_1.TERMINAL_NBR,
ld.LOCATION_NAME ,
tidd.REF_NBR ,
tif.ITEM_TS ,
atd.TRAN_TYPE ,
cd.SAFE_CARD_NUMBER ,
add.AUTHORITY_TRAN_CD' 
from dual;

l_report_name:='Zero Trans Report';

select REPORT.REPORTS_SEQ.NEXTVAL into l_report_id from dual;

INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
select l_report_id, l_rdw_prefix||title, GENERATOR_ID, BATCH_TYPE_ID,l_rdw_prefix||report_name, DESCRIPTION, 'N', l_user_id
from report.reports where report_name=l_report_name;

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id, param_name, param_value from report.report_param where report_id=(select report_id from report.reports where report_name=l_report_name) and param_name<>'query';

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id,'query','select distinct ld.LOCATION_NAME as Location, pd.TERMINAL_NBR as Terminal, tidd.DEVICE_SERIAL_CD as Device
from RDW.TRAN_ITEM_FACT tif JOIN RDW.POS_DIM pd on tif.POS_DIM_ID=pd.POS_DIM_ID and pd.SOURCE_SYSTEM_CD=''TE''
and pd.POS_START_TS <= CURRENT_TIMESTAMP AND pd.POS_END_TS > CURRENT_TIMESTAMP
JOIN RDW.TRAN_ITEM_DETAIL_DIM tidd on tif.ITEM_DETAIL_DIM_ID = tidd.ITEM_DETAIL_DIM_ID
JOIN RDW.USER_LOCATION_PRIV ulp join RDW.LOCATION_BRIDGE lb on ulp.LOCATION_DIM_ID=lb.ANCESTOR_LOCATION_DIM_ID and ulp.active_flag=''Yes''
JOIN RDW.LOCATION_DIM ld on lb.DESCENDENT_LOCATION_DIM_ID=ld.LOCATION_DIM_ID
ON tif.LOCATION_DIM_ID= ld.LOCATION_DIM_ID
where not exists ( select * from RDW.TRAN_ITEM_FACT tif2 where tif2.POS_DIM_ID=tif.POS_DIM_ID and tif2.ITEM_TS >date_trunc(''day'', CURRENT_TIMESTAMP - 
interval ''2 day''))
and ulp.user_id= {u}'
from dual;

l_report_name:='Transaction Data Included in EFT (by Device)';

select REPORT.REPORTS_SEQ.NEXTVAL into l_report_id from dual;

INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
select l_report_id, l_rdw_prefix||title, GENERATOR_ID, BATCH_TYPE_ID,l_rdw_prefix||report_name, DESCRIPTION, 'N', l_user_id
from report.reports where report_name=l_report_name;

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id, param_name, param_value from report.report_param where report_id=(select report_id from report.reports where report_name=l_report_name) and param_name<>'query';

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id,'query','SELECT 
tidd.DEVICE_SERIAL_CD as EPORT_SERIAL_NUM,
ld.LOCATION_NAME as LOCATION_NAME,
tidd.REF_NBR as REF_NBR,
tif.ITEM_TS as TRAN_DATE,
atd.TRAN_TYPE as TRANS_TYPE_NAME,
cd.SAFE_CARD_NUMBER as CARD_NUMBER,
add.AUTHORITY_TRAN_CD as PAYMENT_APPR_CODE,
round(sum(tif.amount),2) as TOTAL_AMOUNT,
array_to_string(ARRAY_AGG(tlid.TRAN_LINE_ITEM_LABEL|| CASE WHEN tif.UNIT_PRICE is NOT NULL THEN 
''(''||
 CASE WHEN tif.PRODUCT_SERVICE_QUANTITY > 1 THEN
	tif.PRODUCT_SERVICE_QUANTITY ||''*''||cd_1.CURRENCY_SYMBOL||round(tif.UNIT_PRICE, 2)
 ELSE
	cd_1.CURRENCY_SYMBOL||round(tif.UNIT_PRICE, 2)
 END
||'')''
ELSE ''''END),'','') as VEND_COLUMN,
sum(tif.PRODUCT_SERVICE_QUANTITY) as QUANTITY,
cd.GLOBAL_ACCOUNT_ID as CARD_ID
from RDW.AUTH_DETAIL_DIM add
    JOIN RDW.TRAN_ITEM_FACT tif ON tif.AUTH_DETAIL_DIM_ID = add.AUTH_DETAIL_DIM_ID
    JOIN RDW.AUTH_TYPE_DIM atd on tif.AUTH_TYPE_DIM_ID = atd.AUTH_TYPE_DIM_ID
    JOIN RDW.TRAN_ITEM_DETAIL_DIM tidd ON tif.ITEM_DETAIL_DIM_ID = tidd.ITEM_DETAIL_DIM_ID
    JOIN RDW.TRAN_LINE_ITEM_DIM tlid ON tif.TRAN_LINE_ITEM_DIM_ID = tlid.TRAN_LINE_ITEM_DIM_ID
    JOIN RDW.CONSUMER_DIM cd ON tif.CONSUMER_DIM_ID = cd.CONSUMER_DIM_ID
    JOIN RDW.CURRENCY_DIM cd_1 ON tif.CURRENCY_DIM_ID = cd_1.CURRENCY_DIM_ID
    JOIN RDW.LOCATION_DIM ld ON tif.LOCATION_DIM_ID = ld.LOCATION_DIM_ID
    JOIN RDW.PAYMENT_DIM pd ON tif.PAYMENT_DIM_ID = pd.PAYMENT_DIM_ID
where pd.SOURCE_DOC_ID = {x}
group by
tidd.DEVICE_SERIAL_CD ,
ld.LOCATION_NAME ,
tidd.REF_NBR ,
tif.ITEM_TS ,
atd.TRAN_TYPE ,
cd.SAFE_CARD_NUMBER ,
add.AUTHORITY_TRAN_CD,
cd.GLOBAL_ACCOUNT_ID' 
from dual;

l_report_name:='CDMA Device Locations';

select REPORT.REPORTS_SEQ.NEXTVAL into l_report_id from dual;

INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
select l_report_id, l_rdw_prefix||title, GENERATOR_ID, BATCH_TYPE_ID,l_rdw_prefix||report_name, DESCRIPTION, 'N', l_user_id
from report.reports where report_name=l_report_name;

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id, param_name, param_value from report.report_param where report_id=(select report_id from report.reports where report_name=l_report_name) and param_name<>'query';

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id,'query','SELECT dd.DEVICE_SERIAL_CD, 
REPLACE(dd.FIRMWARE_VERSION, ''"'', ''""'') FIRMWARE_VERSION, 
REPLACE(mhd.MEID, ''"'', ''""'') MEID, 
REPLACE(mhd.IMEI, ''"'', ''""'') IMEI, 
REPLACE(mhd.MDN, ''"'', ''""'') MDN, 
REPLACE(ld.CUSTOMER_NAME, ''"'', ''""'') CUSTOMER, 
REPLACE(ld.CITY, ''"'', ''""'') CITY, 
REPLACE(ld.STATE_ABBR, ''"'', ''""'') STATE, 
REPLACE(ld.POSTAL_CD, ''"'', ''""'') POSTAL,  
TO_CHAR(pd.LAST_CALL_UTC_TS AT TIME ZONE ''UTC''  AT TIME ZONE current_setting(''TIMEZONE''), ''MM/DD/YYYY HH24:MI:SS'') LAST_COMMUNICATION,
REPLACE(ld.LOCATION_DESC, ''"'', ''""'') LOCATION_DETAILS 
FROM RDW.DEVICE_DIM dd JOIN RDW.HOST_DIM hd on dd.MODEM_HOST_DIM_ID = hd.HOST_DIM_ID and hd.SOURCE_HOST_TYPE_ID=204
JOIN RDW.MODEM_HOST_DIM mhd on hd.HOST_DIM_ID=mhd.HOST_DIM_ID 
JOIN RDW.POS_DIM pd on dd.DEVICE_DIM_ID = pd.DEVICE_DIM_ID and pd.POS_START_TS <= CURRENT_TIMESTAMP AND pd.POS_END_TS > CURRENT_TIMESTAMP
and pd.SOURCE_SYSTEM_CD=''TE''
and pd.LAST_CALL_UTC_TS>CURRENT_TIMESTAMP - interval ''30 days''
JOIN RDW.LOCATION_DIM ld on pd.LOCATION_DIM_ID = ld.LOCATION_DIM_ID' 
from dual;

l_report_name:='EFT Summary 2 Weeks';

select REPORT.REPORTS_SEQ.NEXTVAL into l_report_id from dual;

INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
select l_report_id, l_rdw_prefix||title, GENERATOR_ID, BATCH_TYPE_ID,l_rdw_prefix||report_name, DESCRIPTION, 'N', l_user_id
from report.reports where report_name=l_report_name;

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id, param_name, param_value from report.report_param where report_id=(select report_id from report.reports where report_name=l_report_name) and param_name<>'query';

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id,'query','select itd.PAYMENT_ENTRY_TYPE_CD as entry_type,
CASE WHEN itd.PAYMENT_ENTRY_TYPE_CD =''SF'' THEN lidd.fee_name ELSE NULL END as service_fee_name,
pd.BUSINESS_UNIT as business_unit_name,
CASE WHEN pbd.PAYMENT_SCHEDULE_ID = 4 THEN ''Y'' ELSE ''N'' END as monthly_payment,
cd.CURRENCY_CODE,
pd.CUSTOMER_NAME,
pd.BANK_ACCT_NUM as bank_acct_nbr,
pd.BANK_ROUTING_NUM as bank_routing_nbr,
pd.BANK_ACCT_NAME as account_title,
pd.SOURCE_DOC_ID as eft_id,
TO_CHAR(pd.PAID_TS, ''mm/dd/yyyy hh:mi:ssAM'') as eft_date,
round(sum(lif.AMOUNT),2) as ledger_amount,
round(pd.total_amount,2) as eft_total_amount 
from RDW.ITEM_TYPE_DIM itd
    JOIN RDW.LEDGER_ITEM_FACT lif ON lif.ITEM_TYPE_DIM_ID = itd.ITEM_TYPE_DIM_ID
    JOIN RDW.PAYMENT_DIM pd ON lif.PAYMENT_DIM_ID = pd.PAYMENT_DIM_ID
    JOIN RDW.PAYMENT_BATCH_DIM pbd ON lif.PAYMENT_BATCH_DIM_ID = pbd.PAYMENT_BATCH_DIM_ID
    JOIN RDW.CURRENCY_DIM cd on lif.CURRENCY_DIM_ID = cd.CURRENCY_DIM_ID
    JOIN RDW.LEDGER_ITEM_DETAIL_DIM lidd ON lif.ITEM_DETAIL_DIM_ID = lidd.ITEM_DETAIL_DIM_ID
where pd.PAID_TS > CURRENT_TIMESTAMP - interval ''14 day'' 
and lidd.ITEM_PAYABLE_FLAG=''Yes''
group by itd.PAYMENT_ENTRY_TYPE_CD,
CASE WHEN itd.PAYMENT_ENTRY_TYPE_CD =''SF'' THEN lidd.fee_name ELSE NULL END,
pd.BUSINESS_UNIT ,
CASE WHEN pbd.PAYMENT_SCHEDULE_ID = 4 THEN ''Y'' ELSE ''N'' END,
cd.CURRENCY_CODE,
pd.CUSTOMER_NAME,
pd.BANK_ACCT_NUM,
pd.BANK_ROUTING_NUM,
pd.BANK_ACCT_NAME,
pd.SOURCE_DOC_ID,
pd.PAID_TS,
pd.total_amount' 
from dual;

l_report_name:='EFT Summary Accounting';

select REPORT.REPORTS_SEQ.NEXTVAL into l_report_id from dual;

INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
select l_report_id, l_rdw_prefix||title, GENERATOR_ID, BATCH_TYPE_ID,l_rdw_prefix||report_name, DESCRIPTION, 'N', l_user_id
from report.reports where report_name=l_report_name;

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id, param_name, param_value from report.report_param where report_id=(select report_id from report.reports where report_name=l_report_name) and param_name<>'query';

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id,'query','select itd.PAYMENT_ENTRY_TYPE_CD as entry_type,
CASE WHEN itd.PAYMENT_ENTRY_TYPE_CD =''SF'' THEN lidd.fee_name ELSE NULL END as service_fee_name,
pd.BUSINESS_UNIT as business_unit_name,
CASE WHEN pbd.BATCH_TYPE =''Monthly'' THEN ''Y'' ELSE ''N'' END as monthly_payment,
cd.CURRENCY_CODE,
pd.CUSTOMER_NAME,
pd.BANK_ACCT_NUM as bank_acct_nbr,
pd.BANK_ROUTING_NUM as bank_routing_nbr,
pd.BANK_ACCT_NAME as account_title,
pd.SOURCE_DOC_ID as eft_id,
TO_CHAR(pd.PAID_TS, ''mm/dd/yyyy hh:mi:ssAM'') as eft_date,
round(sum(lif.AMOUNT),2) as ledger_amount,
round(pd.total_amount,2) as eft_total_amount 
from RDW.ITEM_TYPE_DIM itd
    JOIN RDW.LEDGER_ITEM_FACT lif ON lif.ITEM_TYPE_DIM_ID = itd.ITEM_TYPE_DIM_ID
    JOIN RDW.PAYMENT_DIM pd ON lif.PAYMENT_DIM_ID = pd.PAYMENT_DIM_ID
    JOIN RDW.PAYMENT_BATCH_DIM pbd ON lif.PAYMENT_BATCH_DIM_ID = pbd.PAYMENT_BATCH_DIM_ID
    JOIN RDW.CURRENCY_DIM cd on lif.CURRENCY_DIM_ID = cd.CURRENCY_DIM_ID
    JOIN RDW.LEDGER_ITEM_DETAIL_DIM lidd ON lif.ITEM_DETAIL_DIM_ID = lidd.ITEM_DETAIL_DIM_ID
where pd.PAID_TS >= cast(? as timestamp) and pd.PAID_TS < cast(? as timestamp)+ interval ''1 day''
and lidd.ITEM_PAYABLE_FLAG=''Yes''
group by itd.PAYMENT_ENTRY_TYPE_CD,
CASE WHEN itd.PAYMENT_ENTRY_TYPE_CD =''SF'' THEN lidd.fee_name ELSE NULL END,
pd.BUSINESS_UNIT ,
CASE WHEN pbd.BATCH_TYPE =''Monthly'' THEN ''Y'' ELSE ''N'' END ,
cd.CURRENCY_CODE,
pd.CUSTOMER_NAME,
pd.BANK_ACCT_NUM,
pd.BANK_ROUTING_NUM,
pd.BANK_ACCT_NAME,
pd.SOURCE_DOC_ID,
pd.PAID_TS,
pd.total_amount'  
from dual;

l_report_name:='Orphan Device History';

select REPORT.REPORTS_SEQ.NEXTVAL into l_report_id from dual;

INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
select l_report_id, l_rdw_prefix||title, GENERATOR_ID, BATCH_TYPE_ID,l_rdw_prefix||report_name, DESCRIPTION, 'N', l_user_id
from report.reports where report_name=l_report_name;

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id, param_name, param_value from report.report_param where report_id=(select report_id from report.reports where report_name=l_report_name) and param_name<>'query';

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id,'query','select tidd.DEVICE_SERIAL_CD as eport_serial_num,
dd.DEVICE_TYPE as device_type_desc,
atd.TRAN_TYPE as trans_type_name,
add.SETTLE_STATE,
CASE WHEN ld.SOURCE_LOCATION_ID is NOT NULL THEN ''TB'' ELSE ''T'' END as orphan_type,
CASE WHEN tif.source_system_cd =''PSS'' THEN ''PSS (Internal)'' WHEN tif.source_system_cd =''RA'' THEN ''Refund Application'' ELSE ''~ Legacy'' END as source_system_name,
COALESCE(cd.CURRENCY_CODE, ''USD'') as currency_code,
TO_CHAR(date_trunc(''month'', tidd.SETTLED_TS),''mm/dd/yyyy'') as month_for,
sum(tif.TRAN_COUNT_FRACTION) as tran_count,
round(sum(tif.AMOUNT),2) as total_amount
from RDW.TRAN_ITEM_FACT tif JOIN RDW.CURRENCY_DIM cd ON tif.CURRENCY_DIM_ID = cd.CURRENCY_DIM_ID
    JOIN RDW.TRAN_ITEM_DETAIL_DIM tidd ON tif.ITEM_DETAIL_DIM_ID = tidd.ITEM_DETAIL_DIM_ID   
    JOIN RDW.AUTH_TYPE_DIM atd on tif.AUTH_TYPE_DIM_ID = atd.AUTH_TYPE_DIM_ID
    JOIN RDW.DEVICE_DIM dd on tif.DEVICE_DIM_ID = dd.DEVICE_DIM_ID
    JOIN RDW.AUTH_DETAIL_DIM add on tif.AUTH_DETAIL_DIM_ID = add.AUTH_DETAIL_DIM_ID
    JOIN RDW.LOCATION_DIM ld ON tif.LOCATION_DIM_ID = ld.LOCATION_DIM_ID and ld.SOURCE_SYSTEM_CD=''TERMINAL''
where tidd.SETTLED_TS < date_trunc(''month'', cast( ? as timestamp)+ interval ''1 day'')
and atd.TRAN_TYPE_ID  IN (14, 16, 19, 20)
and add.settle_state_id IN (2, 3)
and CASE WHEN tidd.CUSTOMER_BANK_ID =  NULL THEN 0 ELSE tidd.CUSTOMER_BANK_ID END = 0
group by
tidd.DEVICE_SERIAL_CD ,
dd.DEVICE_TYPE,
atd.TRAN_TYPE,
add.SETTLE_STATE,
CASE WHEN ld.SOURCE_LOCATION_ID is NOT NULL THEN ''TB'' ELSE ''T'' END,
CASE WHEN tif.source_system_cd =''PSS'' THEN ''PSS (Internal)'' WHEN tif.source_system_cd =''RA'' THEN ''Refund Application'' ELSE ''~ Legacy'' END,
COALESCE(cd.CURRENCY_CODE, ''USD''),
date_trunc(''month'', tidd.SETTLED_TS)' 
from dual;

  
l_report_name:='All Other Fee Entries';

select REPORT.REPORTS_SEQ.NEXTVAL into l_report_id from dual;

INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
select l_report_id, l_rdw_prefix||title, GENERATOR_ID, BATCH_TYPE_ID,l_rdw_prefix||report_name, DESCRIPTION, 'N', l_user_id
from report.reports where report_name=l_report_name;

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id, param_name, param_value from report.report_param where report_id=(select report_id from report.reports where report_name=l_report_name) and param_name<>'query';

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id,'query','select itd.PAYMENT_ENTRY_TYPE_CD as entry_type,
pd.BUSINESS_UNIT as business_unit_name,
CASE WHEN pbd.PAYMENT_SCHEDULE_ID = 4 THEN ''Y'' ELSE ''N'' END as monthly_payment,
cd.CURRENCY_CODE,
pd.CUSTOMER_NAME,
CASE WHEN itd.PAYMENT_ENTRY_TYPE_CD =''SF'' THEN lidd.fee_name ELSE NULL END as fee_name,
CASE WHEN pd.PAID_FLAG = ''Yes'' THEN ''Y'' ELSE ''N'' END as in_eft,
TO_CHAR(date_trunc(''month'', lif.ITEM_TS),''mm/dd/yyyy'') as month_for,
COUNT(1) entry_count,
round(sum(lif.amount),2) as total_entry_amount,
lidd.ITEM_DESC as description
from RDW.ITEM_TYPE_DIM itd
    JOIN RDW.LEDGER_ITEM_FACT lif ON lif.ITEM_TYPE_DIM_ID = itd.ITEM_TYPE_DIM_ID
    JOIN RDW.PAYMENT_DIM pd ON lif.PAYMENT_DIM_ID = pd.PAYMENT_DIM_ID
    JOIN RDW.PAYMENT_BATCH_DIM pbd ON lif.PAYMENT_BATCH_DIM_ID = pbd.PAYMENT_BATCH_DIM_ID
    JOIN RDW.LEDGER_ITEM_DETAIL_DIM lidd ON lif.ITEM_DETAIL_DIM_ID = lidd.ITEM_DETAIL_DIM_ID
    JOIN RDW.CURRENCY_DIM cd on lif.CURRENCY_DIM_ID = cd.CURRENCY_DIM_ID
    JOIN RDW.DATE_DIM dd on lif.UPLOAD_DATE_DIM_ID=dd.DATE_DIM_ID and dd.ACTUAL_DATE >= cast( ? as DATE) and dd.ACTUAL_DATE < cast( ? as DATE)+ interval ''1 day''
where itd.PAYMENT_ENTRY_TYPE_CD IN (''SF'', ''AD'', ''SB'')
and lidd.ITEM_PAYABLE_FLAG=''Yes''
group by 
itd.PAYMENT_ENTRY_TYPE_CD,
pd.BUSINESS_UNIT ,
CASE WHEN pbd.PAYMENT_SCHEDULE_ID = 4 THEN ''Y'' ELSE ''N'' END,
cd.CURRENCY_CODE,
pd.CUSTOMER_NAME,
CASE WHEN itd.PAYMENT_ENTRY_TYPE_CD =''SF'' THEN lidd.fee_name ELSE NULL END,
CASE WHEN pd.PAID_FLAG = ''Yes'' THEN ''Y'' ELSE ''N'' END ,
TO_CHAR(date_trunc(''month'', lif.ITEM_TS),''mm/dd/yyyy'') ,
lidd.ITEM_DESC' 
from dual;

l_report_name:='All Other Fee Entries With Device';

select REPORT.REPORTS_SEQ.NEXTVAL into l_report_id from dual;

INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
select l_report_id, l_rdw_prefix||title, GENERATOR_ID, BATCH_TYPE_ID,l_rdw_prefix||report_name, DESCRIPTION, 'N', l_user_id
from report.reports where report_name=l_report_name;

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id, param_name, param_value from report.report_param where report_id=(select report_id from report.reports where report_name=l_report_name) and param_name<>'query';

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id,'query','select itd.PAYMENT_ENTRY_TYPE_CD as entry_type,
pd.BUSINESS_UNIT as business_unit_name,
CASE WHEN pbd.PAYMENT_SCHEDULE_ID = 4 THEN ''Y'' ELSE ''N'' END as monthly_payment,
cd.CURRENCY_CODE,
pd.CUSTOMER_NAME,
pd_1.DEVICE_SERIAL_CD as eport_num,
CASE WHEN itd.PAYMENT_ENTRY_TYPE_CD =''SF'' THEN lidd.fee_name ELSE NULL END as fee_name,
CASE WHEN pd.PAID_FLAG = ''Yes'' THEN ''Y'' ELSE ''N'' END as in_eft,
TO_CHAR(date_trunc(''month'', lif.ITEM_TS),''mm/dd/yyyy'') as month_for,
COUNT(1) entry_count,
round(sum(lif.amount),2) as total_entry_amount,
lidd.ITEM_DESC as description,
pd_1.TERMINAL_NBR as terminal_nbr
from RDW.ITEM_TYPE_DIM itd
    JOIN RDW.LEDGER_ITEM_FACT lif ON lif.ITEM_TYPE_DIM_ID = itd.ITEM_TYPE_DIM_ID
    JOIN RDW.POS_DIM pd_1 on lif.POS_DIM_ID=pd_1.POS_DIM_ID and pd_1.SOURCE_SYSTEM_CD=''TE''
	and pd_1.POS_START_TS <= CURRENT_TIMESTAMP AND pd_1.POS_END_TS > CURRENT_TIMESTAMP
    JOIN RDW.PAYMENT_DIM pd ON lif.PAYMENT_DIM_ID = pd.PAYMENT_DIM_ID
    JOIN RDW.PAYMENT_BATCH_DIM pbd ON lif.PAYMENT_BATCH_DIM_ID = pbd.PAYMENT_BATCH_DIM_ID
    JOIN RDW.LEDGER_ITEM_DETAIL_DIM lidd ON lif.ITEM_DETAIL_DIM_ID = lidd.ITEM_DETAIL_DIM_ID
    JOIN RDW.CURRENCY_DIM cd on lif.CURRENCY_DIM_ID = cd.CURRENCY_DIM_ID
    JOIN RDW.LOCATION_DIM ld ON lif.LOCATION_DIM_ID = ld.LOCATION_DIM_ID 
    JOIN RDW.DATE_DIM dd on lif.UPLOAD_DATE_DIM_ID=dd.DATE_DIM_ID and dd.ACTUAL_DATE >= cast(? as DATE) and dd.ACTUAL_DATE < cast(? 
as DATE)+ interval ''1 day''
where itd.PAYMENT_ENTRY_TYPE_CD IN (''SF'', ''AD'', ''SB'')
and lidd.ITEM_PAYABLE_FLAG=''Yes''
group by 
itd.PAYMENT_ENTRY_TYPE_CD,
pd.BUSINESS_UNIT ,
CASE WHEN pbd.PAYMENT_SCHEDULE_ID = 4 THEN ''Y'' ELSE ''N'' END,
cd.CURRENCY_CODE,
pd.CUSTOMER_NAME,
CASE WHEN itd.PAYMENT_ENTRY_TYPE_CD =''SF'' THEN lidd.fee_name ELSE NULL END,
CASE WHEN pd.PAID_FLAG = ''Yes'' THEN ''Y'' ELSE ''N'' END ,
TO_CHAR(date_trunc(''month'', lif.ITEM_TS),''mm/dd/yyyy'') ,
lidd.ITEM_DESC,
pd_1.TERMINAL_NBR,
pd_1.DEVICE_SERIAL_CD' 
from dual;

l_report_name:='Transaction Processing Entry Summary';

select REPORT.REPORTS_SEQ.NEXTVAL into l_report_id from dual;

INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
select l_report_id, l_rdw_prefix||title, GENERATOR_ID, BATCH_TYPE_ID,l_rdw_prefix||report_name, DESCRIPTION, 'N', l_user_id
from report.reports where report_name=l_report_name;

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id, param_name, param_value from report.report_param where report_id=(select report_id from report.reports where report_name=l_report_name) and param_name<>'query';

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id,'query','select itd.PAYMENT_ENTRY_TYPE_CD as entry_type,
CASE WHEN pd.BUSINESS_UNIT is NULL THEN ''~ Terminal Orphan'' ELSE pd.BUSINESS_UNIT END as business_unit_name,
CASE WHEN pbd.BATCH_TYPE =''Monthly'' THEN ''Y'' ELSE ''N'' END as monthly_payment,
cd.CURRENCY_CODE,
CASE WHEN pd.CUSTOMER_NAME is NULL THEN ''~ Terminal Orphan'' ELSE pd.CUSTOMER_NAME END as customer_name,
TO_CHAR(dd.actual_month,''mm/dd/yyyy'') as month_for,
COUNT(1)  as tran_count,
round(sum(tidd.total_amount),2) as total_tran_amount,
round(sum(lif.amount),2) as total_ledger_amount
from RDW.LEDGER_ITEM_FACT lif JOIN RDW.PAYMENT_DIM pd ON lif.PAYMENT_DIM_ID = pd.PAYMENT_DIM_ID
    JOIN RDW.LEDGER_ITEM_DETAIL_DIM lidd ON lif.ITEM_DETAIL_DIM_ID = lidd.ITEM_DETAIL_DIM_ID
    LEFT OUTER JOIN RDW.TRAN_ITEM_DETAIL_DIM tidd on lif.TRAN_ITEM_DETAIL_DIM_ID=tidd.ITEM_DETAIL_DIM_ID
    JOIN RDW.PAYMENT_BATCH_DIM pbd ON lif.PAYMENT_BATCH_DIM_ID = pbd.PAYMENT_BATCH_DIM_ID
    JOIN RDW.ITEM_TYPE_DIM itd ON lif.ITEM_TYPE_DIM_ID = itd.ITEM_TYPE_DIM_ID
    JOIN RDW.CURRENCY_DIM cd ON lif.CURRENCY_DIM_ID = cd.CURRENCY_DIM_ID
    JOIN RDW.DATE_DIM dd on lif.SETTLED_DATE_DIM_ID = dd.DATE_DIM_ID
where lidd.ITEM_PAYABLE_FLAG=''Yes''
and lidd.tran_type_id in (14, 16, 19, 20, 21)
and itd.PAYMENT_ENTRY_TYPE_CD IN (''CC'', ''PF'', ''CB'', ''RF'') 
and dd.ACTUAL_DATE >= cast(? as DATE) and dd.ACTUAL_DATE < cast(? as DATE)+ interval ''1 day''
group by
itd.PAYMENT_ENTRY_TYPE_CD,
CASE WHEN pd.BUSINESS_UNIT is NULL THEN ''~ Terminal Orphan'' ELSE pd.BUSINESS_UNIT END,
CASE WHEN pbd.BATCH_TYPE =''Monthly'' THEN ''Y'' ELSE ''N'' END,
cd.CURRENCY_CODE,
CASE WHEN pd.CUSTOMER_NAME is NULL THEN ''~ Terminal Orphan'' ELSE pd.CUSTOMER_NAME END,
dd.actual_month' 
from dual;

l_report_name:='Unpaid Monies';

select REPORT.REPORTS_SEQ.NEXTVAL into l_report_id from dual;

INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
select l_report_id, l_rdw_prefix||title, GENERATOR_ID, BATCH_TYPE_ID,l_rdw_prefix||report_name, DESCRIPTION, 'N', l_user_id
from report.reports where report_name=l_report_name;

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id, param_name, param_value from report.report_param where report_id=(select report_id from report.reports where report_name=l_report_name) and param_name<>'query';

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id,'query','select itd.PAYMENT_ENTRY_TYPE_CD as entry_type,
pd.BUSINESS_UNIT as business_unit_name,
CASE WHEN pbd.PAYMENT_SCHEDULE_ID = 4 THEN ''Y'' ELSE ''N'' END as monthly_payment,
cd.CURRENCY_CODE,
pd.CUSTOMER_NAME,
CASE WHEN itd.PAYMENT_ENTRY_TYPE_CD=''CC'' THEN TO_CHAR(date_trunc(''month'', lidd.SETTLED_TS),''mm/dd/yyyy'') ELSE TO_CHAR(date_trunc(''month'', lif.ITEM_TS),''mm/dd/yyyy'') END as month_for,
count(1) as tran_count,
round(sum(lif.amount),2) as total_ledger_amount
from RDW.ITEM_TYPE_DIM itd
    JOIN RDW.LEDGER_ITEM_FACT lif ON lif.ITEM_TYPE_DIM_ID = itd.ITEM_TYPE_DIM_ID
    JOIN RDW.PAYMENT_DIM pd ON lif.PAYMENT_DIM_ID = pd.PAYMENT_DIM_ID
    JOIN RDW.PAYMENT_BATCH_DIM pbd ON lif.PAYMENT_BATCH_DIM_ID = pbd.PAYMENT_BATCH_DIM_ID
    JOIN RDW.CURRENCY_DIM cd on lif.CURRENCY_DIM_ID = cd.CURRENCY_DIM_ID
    JOIN RDW.LEDGER_ITEM_DETAIL_DIM lidd ON lif.ITEM_DETAIL_DIM_ID = lidd.ITEM_DETAIL_DIM_ID
where lidd.ITEM_PAYABLE_FLAG=''Yes''
and (lif.PAID_TS is NULL or lif.PAID_TS > date_trunc(''month'', CAST(? as DATE) + 1))
and lidd.settled_ts < date_trunc(''month'', CAST(? as DATE) + 1)
group by
itd.PAYMENT_ENTRY_TYPE_CD,
pd.BUSINESS_UNIT,
CASE WHEN pbd.PAYMENT_SCHEDULE_ID = 4 THEN ''Y'' ELSE ''N'' END,
cd.CURRENCY_CODE,
pd.CUSTOMER_NAME,
CASE WHEN itd.PAYMENT_ENTRY_TYPE_CD=''CC'' THEN TO_CHAR(date_trunc(''month'', lidd.SETTLED_TS),''mm/dd/yyyy'') ELSE TO_CHAR(date_trunc(''month'', lif.ITEM_TS),''mm/dd/yyyy'') END' 
from dual;

l_report_name:='eTaxi Device Activations';

select REPORT.REPORTS_SEQ.NEXTVAL into l_report_id from dual;

INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
select l_report_id, l_rdw_prefix||title, GENERATOR_ID, BATCH_TYPE_ID,l_rdw_prefix||report_name, DESCRIPTION, 'N', l_user_id
from report.reports where report_name=l_report_name;

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id, param_name, param_value from report.report_param where report_id=(select report_id from report.reports where report_name=l_report_name) and param_name<>'query';

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id,'query','select DEVICE_SERIAL_CD as EPORT_SERIAL_NUM, 
FIRST_CREDIT_TRAN_TS as FIRST_TRAN_DATE
from RDW.DEVICE_DIM 
where DEVICE_SERIAL_CD LIKE ''K3ET%''
and FIRST_CREDIT_TRAN_TS >= cast(? as DATE) AND FIRST_CREDIT_TRAN_TS < cast(? as DATE)+ 1
order by DEVICE_SERIAL_CD' 
from dual;

l_report_name:='eTaxi Royalty Transaction Counts';

select REPORT.REPORTS_SEQ.NEXTVAL into l_report_id from dual;

INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
select l_report_id, l_rdw_prefix||title, GENERATOR_ID, BATCH_TYPE_ID,l_rdw_prefix||report_name, DESCRIPTION, 'N', l_user_id
from report.reports where report_name=l_report_name;

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id, param_name, param_value from report.report_param where report_id=(select report_id from report.reports where report_name=l_report_name) and param_name<>'query';

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id,'query','select pd.DEVICE_SERIAL_CD as EPORT_SERIAL_NUM, round(sum(tif.TRAN_COUNT_FRACTION)) as TRAN_COUNT
FROM RDW.TRAN_ITEM_FACT tif JOIN RDW.POS_DIM pd on tif.POS_DIM_ID=pd.POS_DIM_ID
JOIN RDW.DATE_DIM dd on tif.SETTLED_DATE_DIM_ID = dd.DATE_DIM_ID
JOIN RDW.TRAN_ITEM_DETAIL_DIM tidd ON tif.ITEM_DETAIL_DIM_ID = tidd.ITEM_DETAIL_DIM_ID
where pd.DEVICE_SERIAL_CD LIKE ''K3ET%''
and dd.ACTUAL_DATE >= cast(? as DATE) and dd.ACTUAL_DATE < cast(? as DATE)+ interval ''1 day''
AND tidd.ROYALTY_FEE_FLAG = ''Yes''
GROUP BY pd.DEVICE_SERIAL_CD
ORDER BY pd.DEVICE_SERIAL_CD'
from dual;

l_report_name:='Isis 5th Purchase Free Transactions';

select REPORT.REPORTS_SEQ.NEXTVAL into l_report_id from dual;

INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
select l_report_id, l_rdw_prefix||title, GENERATOR_ID, BATCH_TYPE_ID,l_rdw_prefix||report_name, DESCRIPTION, 'N', l_user_id
from report.reports where report_name=l_report_name;

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id, param_name, param_value from report.report_param where report_id=(select report_id from report.reports where report_name=l_report_name) and param_name<>'query';

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id,'query','select TO_CHAR(tidd.SETTLED_TS,''MM/DD/YYYY HH24:MI:SS'') as SETTLE_DATE,
Null as OFFER_ID, 
cd_1.CONSUMER_IDENTIFIER as CONSUMER_ID,
tidd.DEVICE_SERIAL_CD as EPORT_SERIAL_NUM,
ld.CITY,
ld.STATE_ABBR as STATE,
round(sum(tif.amount),2) as REDEMPTION_AMOUNT,
cd.CURRENCY_CODE,
ld.ADDRESS1 as ADDRESS,
ld.POSTAL_CD as ZIP,
tidd.REPORT_TRAN_ID as TRAN_ID,
TO_CHAR(tidd.SETTLED_TS,''MM/DD/YYYY HH24:MI:SS'') as TRAN_DATE,
cd_1.CARD_TYPE as CARD_NAME,
cd_1.SAFE_CARD_NUMBER as CARD_NUMBER,
pd.TERMINAL_NBR as TERMINAL_NBR,
ld.CUSTOMER_NAME,
ld.LOCATION_NAME as LOCATION_NAME,
cd_1.GLOBAL_ACCOUNT_ID as CARD_ID
from RDW.TRAN_ITEM_FACT tif JOIN RDW.TRAN_ITEM_DETAIL_DIM tidd ON tif.ITEM_DETAIL_DIM_ID = tidd.ITEM_DETAIL_DIM_ID
JOIN RDW.POS_DIM pd on tif.POS_DIM_ID=pd.POS_DIM_ID and pd.SOURCE_SYSTEM_CD=''TE''
and pd.POS_START_TS <= CURRENT_TIMESTAMP AND pd.POS_END_TS > CURRENT_TIMESTAMP
JOIN RDW.CURRENCY_DIM cd ON tif.CURRENCY_DIM_ID = cd.CURRENCY_DIM_ID
JOIN RDW.CONSUMER_DIM cd_1 ON tif.CONSUMER_DIM_ID = cd_1.CONSUMER_DIM_ID
JOIN RDW.LOCATION_DIM ld ON tif.LOCATION_DIM_ID = ld.LOCATION_DIM_ID
JOIN RDW.AUTH_DETAIL_DIM add on tif.AUTH_DETAIL_DIM_ID=add.AUTH_DETAIL_DIM_ID
JOIN RDW.DATE_DIM dd on tif.SETTLED_DATE_DIM_ID = dd.DATE_DIM_ID
where dd.ACTUAL_DATE >= cast(? as DATE) and dd.ACTUAL_DATE < cast(? as DATE)+ interval ''1 day''
and add.AUTHORITY_TRAN_CD =''Isis Promo''
and add.settle_state_id =3
group by tidd.SETTLED_TS,cd_1.CONSUMER_IDENTIFIER,tidd.DEVICE_SERIAL_CD,ld.CITY,ld.STATE_ABBR,cd.CURRENCY_CODE,ld.ADDRESS1,ld.POSTAL_CD,
tidd.REPORT_TRAN_ID,cd_1.CARD_TYPE,cd_1.SAFE_CARD_NUMBER,pd.TERMINAL_NBR,ld.CUSTOMER_NAME,ld.LOCATION_NAME,cd_1.GLOBAL_ACCOUNT_ID
ORDER BY tidd.SETTLED_TS, tidd.REPORT_TRAN_ID' 
from dual;

l_report_name:='Isis 5th Purchase Free Summary';

select REPORT.REPORTS_SEQ.NEXTVAL into l_report_id from dual;

INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
select l_report_id, l_rdw_prefix||title, GENERATOR_ID, BATCH_TYPE_ID,l_rdw_prefix||report_name, DESCRIPTION, 'N', l_user_id
from report.reports where report_name=l_report_name;

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id, param_name, param_value from report.report_param where report_id=(select report_id from report.reports where report_name=l_report_name) and param_name<>'query';

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id,'query','select Null as OFFER_ID,
round(sum(tif.TRAN_COUNT_FRACTION)) as TRAN_COUNT,
round(sum(tif.AMOUNT),2) as TRAN_AMOUNT,
cd.CURRENCY_CODE
from RDW.TRAN_ITEM_FACT tif JOIN RDW.CURRENCY_DIM cd ON tif.CURRENCY_DIM_ID = cd.CURRENCY_DIM_ID
JOIN RDW.DATE_DIM dd on tif.SETTLED_DATE_DIM_ID = dd.DATE_DIM_ID
JOIN RDW.AUTH_DETAIL_DIM add on tif.AUTH_DETAIL_DIM_ID=add.AUTH_DETAIL_DIM_ID
where dd.ACTUAL_DATE >= cast(? as DATE) and dd.ACTUAL_DATE < cast(? as DATE)+ interval ''1 day''
and add.AUTHORITY_TRAN_CD =''Isis Promo''
and add.settle_state_id =3
group by cd.CURRENCY_CODE
order by 1, 2, 3, 4' 
from dual;

l_report_name:='Isis 5th Purchase Free Summary by Device';

select REPORT.REPORTS_SEQ.NEXTVAL into l_report_id from dual;

INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
select l_report_id, l_rdw_prefix||title, GENERATOR_ID, BATCH_TYPE_ID,l_rdw_prefix||report_name, DESCRIPTION, 'N', l_user_id
from report.reports where report_name=l_report_name;

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id, param_name, param_value from report.report_param where report_id=(select report_id from report.reports where report_name=l_report_name) and param_name<>'query';

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id,'query','select Null as OFFER_ID,
tidd.DEVICE_SERIAL_CD as EPORT_SERIAL_NUM,
round(sum(tif.TRAN_COUNT_FRACTION)) as TRAN_COUNT,
round(sum(tif.AMOUNT),2) as TRAN_AMOUNT,
cd.CURRENCY_CODE
from RDW.TRAN_ITEM_FACT tif JOIN RDW.CURRENCY_DIM cd ON tif.CURRENCY_DIM_ID = cd.CURRENCY_DIM_ID
JOIN RDW.TRAN_ITEM_DETAIL_DIM tidd on tif.ITEM_DETAIL_DIM_ID = tidd.ITEM_DETAIL_DIM_ID
JOIN RDW.DATE_DIM dd on tif.SETTLED_DATE_DIM_ID = dd.DATE_DIM_ID
JOIN RDW.AUTH_DETAIL_DIM add on tif.AUTH_DETAIL_DIM_ID=add.AUTH_DETAIL_DIM_ID
where dd.ACTUAL_DATE >= cast(? as DATE) and dd.ACTUAL_DATE < cast(? as DATE)+ interval ''1 day''
and add.AUTHORITY_TRAN_CD =''Isis Promo''
and add.settle_state_id =3
group by tidd.DEVICE_SERIAL_CD,
cd.CURRENCY_CODE
order by 1, 2' 
from dual;

l_report_name:='Declined MasterCard Debit Transaction Counts';

select REPORT.REPORTS_SEQ.NEXTVAL into l_report_id from dual;

INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
select l_report_id, l_rdw_prefix||title, GENERATOR_ID, BATCH_TYPE_ID,l_rdw_prefix||report_name, DESCRIPTION, 'N', l_user_id
from report.reports where report_name=l_report_name;

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id, param_name, param_value from report.report_param where report_id=(select report_id from report.reports where report_name=l_report_name) and param_name<>'query';

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id,'query','select ld.CUSTOMER_NAME,
round(sum(tif.TRAN_COUNT_FRACTION)) as TRAN_COUNT
FROM RDW.TRAN_ITEM_FACT tif JOIN RDW.LOCATION_DIM ld ON tif.LOCATION_DIM_ID = ld.LOCATION_DIM_ID
JOIN RDW.AUTH_DETAIL_DIM add ON tif.AUTH_DETAIL_DIM_ID = add.AUTH_DETAIL_DIM_ID
JOIN RDW.AUTH_TYPE_DIM atd on tif.AUTH_TYPE_DIM_ID =  atd.AUTH_TYPE_DIM_ID
WHERE add.AUTH_TS >= cast(? as DATE) and add.AUTH_TS < cast(? as DATE)+ interval ''1 day''
AND atd.AUTH_STATE_ID = 3
AND add.RESPONSE_CD=''DEBIT NOT ALLOWED''
group by ld.CUSTOMER_NAME
order by round(sum(tif.TRAN_COUNT_FRACTION)) desc, ld.CUSTOMER_NAME'
from dual;

l_report_name:='Isis Transactions';

select REPORT.REPORTS_SEQ.NEXTVAL into l_report_id from dual;

INSERT INTO report.reports(REPORT_ID, TITLE, GENERATOR_ID, BATCH_TYPE_ID, REPORT_NAME, DESCRIPTION, USAGE, USER_ID)
select l_report_id, l_rdw_prefix||title, GENERATOR_ID, BATCH_TYPE_ID,l_rdw_prefix||report_name, DESCRIPTION, 'N', l_user_id
from report.reports where report_name=l_report_name;

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id, param_name, param_value from report.report_param where report_id=(select report_id from report.reports where report_name=l_report_name) and param_name<>'query';

INSERT INTO report.report_param(REPORT_ID, PARAM_NAME, param_value)
select l_report_id,'query','select TO_CHAR(tidd.PROCESSED_TS,''MM/DD/YYYY HH24:MI:SS'') as CREATE_DATE,
TO_CHAR(tidd.ITEM_TS,''MM/DD/YYYY HH24:MI:SS'') as TRAN_DATE,
round(sum(tif.amount),2) as TRAN_AMOUNT,
cd.CURRENCY_CODE,
atd.TRAN_TYPE as TRANS_TYPE_NAME,
REPLACE(cd_1.CARD_TYPE, '' (Isis)'', '''') as CARD_NAME,
cd_1.SAFE_CARD_NUMBER as CARD_NUMBER,
cd_1.CONSUMER_IDENTIFIER as CONSUMER_ID,
Null as OFFER_ID, 
tidd.REPORT_TRAN_ID as TRAN_ID,
add.SETTLE_STATE,
tidd.DEVICE_SERIAL_CD as EPORT_SERIAL_NUM,
pd.TERMINAL_NBR as TERMINAL_NBR,
ld.CUSTOMER_NAME,
ld.LOCATION_NAME as LOCATION_NAME,
ld.ADDRESS1 as ADDRESS,
ld.CITY,
ld.STATE_ABBR as STATE,
ld.POSTAL_CD as ZIP,
cd_1.GLOBAL_ACCOUNT_ID as CARD_ID
from RDW.TRAN_ITEM_FACT tif JOIN RDW.TRAN_ITEM_DETAIL_DIM tidd ON tif.ITEM_DETAIL_DIM_ID = tidd.ITEM_DETAIL_DIM_ID
JOIN RDW.POS_DIM pd on tif.POS_DIM_ID=pd.POS_DIM_ID and pd.SOURCE_SYSTEM_CD=''TE''
and pd.POS_START_TS <= CURRENT_TIMESTAMP AND pd.POS_END_TS > CURRENT_TIMESTAMP
JOIN RDW.CURRENCY_DIM cd ON tif.CURRENCY_DIM_ID = cd.CURRENCY_DIM_ID
JOIN RDW.CONSUMER_DIM cd_1 ON tif.CONSUMER_DIM_ID = cd_1.CONSUMER_DIM_ID
JOIN RDW.LOCATION_DIM ld ON tif.LOCATION_DIM_ID = ld.LOCATION_DIM_ID
JOIN RDW.AUTH_TYPE_DIM atd on tif.AUTH_TYPE_DIM_ID = atd.AUTH_TYPE_DIM_ID
JOIN RDW.AUTH_DETAIL_DIM add on tif.AUTH_DETAIL_DIM_ID=add.AUTH_DETAIL_DIM_ID
where tidd.PROCESSED_TS >= cast(? as DATE) and tidd.PROCESSED_TS < cast(? as DATE)+ interval ''1 day''
and atd.entry_method=''Isis''
and atd.tran_type_id in (14,16)
group by tidd.PROCESSED_TS,tidd.ITEM_TS,cd.CURRENCY_CODE,atd.TRAN_TYPE,cd_1.CARD_TYPE,cd_1.SAFE_CARD_NUMBER,cd_1.CONSUMER_IDENTIFIER,tidd.REPORT_TRAN_ID,add.SETTLE_STATE,tidd.DEVICE_SERIAL_CD,
pd.TERMINAL_NBR,ld.CUSTOMER_NAME,ld.LOCATION_NAME,ld.ADDRESS1,ld.CITY,ld.STATE_ABBR,ld.POSTAL_CD,cd_1.GLOBAL_ACCOUNT_ID 
ORDER BY tidd.PROCESSED_TS,tidd.ITEM_TS,tidd.REPORT_TRAN_ID' 
from dual;

COMMIT;

END;
/