DROP TRIGGER LOCATION.TRAU_LOCATION;

-- Tranaction Details folioId=103 is not used confirmed by Amy remove the report web link
delete from report.user_link where link_id=(select web_link_id from web_content.web_link where web_link_url='run_report_async.i?folioId=103');
delete from web_content.web_link where web_link_id=(select web_link_id from web_content.web_link where web_link_url='run_report_async.i?folioId=103');
commit;

CREATE OR REPLACE FUNCTION          REPORT.GET_TE_FOR_TERMINAL (
	l_terminal_id REPORT.TERMINAL_EPORT.TERMINAL_ID%TYPE,
    l_date REPORT.TERMINAL_EPORT.START_DATE%TYPE)
RETURN REPORT.TERMINAL_EPORT.TERMINAL_EPORT_ID%TYPE IS
    l_terminal_eport_id REPORT.TERMINAL_EPORT.TERMINAL_EPORT_ID%TYPE;
BEGIN
    SELECT MAX(TERMINAL_EPORT_ID)
      INTO l_terminal_eport_id 
      FROM (
      SELECT TE.TERMINAL_EPORT_ID
      FROM REPORT.TERMINAL_EPORT TE
     WHERE TE.TERMINAL_ID = l_terminal_id
       AND l_date BETWEEN NVL(TE.START_DATE, MIN_DATE) AND NVL(TE.END_DATE, MAX_DATE)
     ORDER BY NVL(TE.START_DATE, MIN_DATE) DESC, TE.TERMINAL_EPORT_ID DESC)
     WHERE ROWNUM = 1;
   RETURN l_terminal_eport_id;
END;