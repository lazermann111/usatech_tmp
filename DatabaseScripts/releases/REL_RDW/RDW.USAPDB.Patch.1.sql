CREATE OR REPLACE TRIGGER CORP.TRAIUD_DOC 
AFTER INSERT OR UPDATE OR DELETE
ON CORP.DOC
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
BEGIN
	IF DBADMIN.PKG_GLOBAL.GET_APP_SETTING('RDW_ENABLED') = 'Y' THEN
		INSERT INTO ENGINE.DATA_UPDATE(DATA_UPDATE_PRODUCER, DATA_UPDATE_TABLE, DATA_DETAIL_LABEL_1, DATA_DETAIL_VALUE_1)
            SELECT 'CORP.TRAIUD_DOC', 'PAYMENT_DIM', 'SOURCE_DOC_ID', NVL(:NEW.DOC_ID, :OLD.DOC_ID) FROM DUAL;
	
		IF UPDATING THEN
            IF DBADMIN.PKG_UTL.EQL(:NEW.CUSTOMER_BANK_ID, :OLD.CUSTOMER_BANK_ID) = 'N' THEN
                INSERT INTO ENGINE.DATA_UPDATE(DATA_UPDATE_PRODUCER, DATA_UPDATE_ACTION, DATA_UPDATE_TABLE, DATA_DETAIL_LABEL_1, DATA_DETAIL_VALUE_1, DATA_DETAIL_LABEL_2, DATA_DETAIL_VALUE_2)
				VALUES('CORP.TRAIUD_DOC', 'UPSERT', 'UPDATE_LEDGER_CUSTOMER_BANK_ID', 'CUSTOMER_BANK_ID', :NEW.CUSTOMER_BANK_ID,'SOURCE_DOC_ID', :NEW.DOC_ID);
            END IF;
            IF :OLD.SENT_DATE IS NULL AND :NEW.SENT_DATE IS NOT NULL THEN
            	INSERT INTO ENGINE.DATA_UPDATE(DATA_UPDATE_PRODUCER, DATA_UPDATE_TABLE, DATA_DETAIL_LABEL_1, DATA_DETAIL_VALUE_1)
                VALUES('CORP.TRAIUD_DOC', 'PAID_ITEM_AGG', 'PAYMENT_DIM_ID$SOURCE_DOC_ID', :NEW.DOC_ID);
            END IF;
		END IF;
	END IF;
END;
/

CREATE OR REPLACE TRIGGER CORP.TRAU_BATCH 
AFTER UPDATE
ON CORP.BATCH
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
BEGIN
	IF DBADMIN.PKG_GLOBAL.GET_APP_SETTING('RDW_ENABLED') = 'Y' THEN
		INSERT INTO ENGINE.DATA_UPDATE(DATA_UPDATE_PRODUCER, DATA_UPDATE_TABLE, DATA_DETAIL_LABEL_1, DATA_DETAIL_VALUE_1)
            SELECT 'CORP.TRAU_BATCH', 'PAYMENT_BATCH_DIM', 'SOURCE_BATCH_ID', :NEW.BATCH_ID FROM DUAL;
	
		IF DBADMIN.PKG_UTL.EQL(:NEW.TERMINAL_ID, :OLD.TERMINAL_ID) = 'N' THEN
			INSERT INTO ENGINE.DATA_UPDATE(DATA_UPDATE_PRODUCER, DATA_UPDATE_ACTION, DATA_UPDATE_TABLE, DATA_UPDATE_SOURCE, DATA_DETAIL_LABEL_1, DATA_DETAIL_VALUE_1)
                SELECT 'CORP.TRAU_BATCH',  'PARTIAL', DAT.TGT, DAT.SRC, 'SOURCE_LEDGER_ID', L.LEDGER_ID
                FROM CORP.LEDGER L
                CROSS JOIN (SELECT '' TGT, '' SRC FROM DUAL WHERE 1=0
                UNION ALL SELECT 'LEDGER_ITEM_FACT', 'OPER.RDW_LOADER.VW_PARTIAL_LEDGER_ITEM_FACT' FROM DUAL
                UNION ALL SELECT 'LEDGER_ITEM_DETAIL_DIM', 'OPER.RDW_LOADER.VW_PARTIAL_LEDGER_ITEM_DIM' FROM DUAL) DAT
                WHERE L.BATCH_ID = :NEW.BATCH_ID;
            INSERT INTO ENGINE.DATA_UPDATE(DATA_UPDATE_PRODUCER, DATA_UPDATE_ACTION, DATA_UPDATE_TABLE, DATA_UPDATE_SOURCE, DATA_DETAIL_LABEL_1, DATA_DETAIL_VALUE_1, DATA_DETAIL_LABEL_2, DATA_DETAIL_VALUE_2)
                SELECT 'CORP.TRAU_BATCH', 'PARTIAL', DAT.TGT, DAT.SRC, 'REPORT_TRAN_ID', L.TRANS_ID, '@REPORT_TRAN_ID', L.TRANS_ID
                FROM CORP.LEDGER L
                CROSS JOIN (SELECT '' TGT, '' SRC FROM DUAL WHERE 1=0
                UNION ALL SELECT 'TRAN_ITEM_FACT', 'OPER.RDW_LOADER.VW_PARTIAL_TRAN_ITEM_FACT' FROM DUAL
                UNION ALL SELECT 'TRAN_ITEM_DETAIL_DIM', 'OPER.RDW_LOADER.VW_PARTIAL_TRAN_ITEM_DIM' FROM DUAL) DAT
                WHERE L.BATCH_ID = :NEW.BATCH_ID
                  AND L.TRANS_ID IS NOT NULL;
		END IF;
		
	   IF :NEW.DOC_ID != :OLD.DOC_ID THEN
	   		INSERT INTO ENGINE.DATA_UPDATE(DATA_UPDATE_PRODUCER, DATA_UPDATE_ACTION, DATA_UPDATE_TABLE, DATA_DETAIL_LABEL_1, DATA_DETAIL_VALUE_1, DATA_DETAIL_LABEL_2, DATA_DETAIL_VALUE_2)
				VALUES('CORP.TRAU_BATCH', 'UPSERT', 'UPDATE_DOC_ID', 'SOURCE_BATCH_ID', :NEW.BATCH_ID,'SOURCE_DOC_ID', :NEW.DOC_ID);
	   END IF;
	   
	   IF DBADMIN.PKG_UTL.EQL(:NEW.PAID_DATE, :OLD.PAID_DATE) = 'N' THEN
            	INSERT INTO ENGINE.DATA_UPDATE(DATA_UPDATE_PRODUCER, DATA_UPDATE_ACTION, DATA_UPDATE_TABLE, DATA_DETAIL_LABEL_1, DATA_DETAIL_VALUE_1, DATA_DETAIL_LABEL_2, DATA_DETAIL_VALUE_2)
				VALUES('CORP.TRAU_BATCH', 'UPSERT', 'UPDATE_PAID_TS', 'PAID_TS', :NEW.PAID_DATE,'SOURCE_BATCH_ID', :NEW.BATCH_ID);
        END IF;
		
	END IF;
END;
/
