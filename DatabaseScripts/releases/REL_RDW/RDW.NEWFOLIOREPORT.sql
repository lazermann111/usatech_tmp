-- new RDW Locked Payment orginal folio_conf.report report_id=2
insert into folio_conf.report (report_id, report_name) values (6, 'RDW Locked Payment');

insert into folio_conf.report_folio (report_folio_id, report_id, folio_id, folio_index)
values(folio_conf.seq_report_folio_id.nextval,6, 1403, 1);
insert into folio_conf.report_folio (report_folio_id, report_id, folio_id, folio_index)
values(folio_conf.seq_report_folio_id.nextval,6, 1405, 2);
insert into folio_conf.report_folio (report_folio_id, report_id, folio_id, folio_index)
values(folio_conf.seq_report_folio_id.nextval,6, 1411, 3);
insert into folio_conf.report_folio (report_folio_id, report_id, folio_id, folio_index)
values(folio_conf.seq_report_folio_id.nextval,6, 1409, 4);
insert into folio_conf.report_folio (report_folio_id, report_id, folio_id, folio_index)
values(folio_conf.seq_report_folio_id.nextval,6, 1407, 5);
insert into folio_conf.report_folio (report_folio_id, report_id, folio_id, folio_index)
values(folio_conf.seq_report_folio_id.nextval,6, 1421, 6);

-- new RDW Weekly Payment Reconciliation orginal folio_conf.report report_id=3
insert into folio_conf.report (report_id, report_name) values (7, 'RDW Weekly Payment Reconciliation');
insert into folio_conf.report_folio (report_folio_id, report_id, folio_id, folio_index)
values(folio_conf.seq_report_folio_id.nextval,7, 1361, 1);
insert into folio_conf.report_folio (report_folio_id, report_id, folio_id, folio_index)
values(folio_conf.seq_report_folio_id.nextval,7, 1369, 2);


-- new RDW CCE Payment Reconciliation orginal folio_conf.report report_id=4
insert into folio_conf.report (report_id, report_name) values (8, 'RDW CCE Payment Reconciliation');
insert into folio_conf.report_folio (report_folio_id, report_id, folio_id, folio_index)
values(folio_conf.seq_report_folio_id.nextval,8, 1365, 1);
insert into folio_conf.report_folio (report_folio_id, report_id, folio_id, folio_index)
values(folio_conf.seq_report_folio_id.nextval,8, 1369, 2);


insert into folio_conf.report (report_id, report_name) values (9, 'RDW Pending Payment');

insert into folio_conf.report_folio (report_folio_id, report_id, folio_id, folio_index)
values(folio_conf.seq_report_folio_id.nextval,9, 1454, 1);
insert into folio_conf.report_folio (report_folio_id, report_id, folio_id, folio_index)
values(folio_conf.seq_report_folio_id.nextval,9, 1452, 2);
insert into folio_conf.report_folio (report_folio_id, report_id, folio_id, folio_index)
values(folio_conf.seq_report_folio_id.nextval,9, 1456, 3);
insert into folio_conf.report_folio (report_folio_id, report_id, folio_id, folio_index)
values(folio_conf.seq_report_folio_id.nextval,9, 1450, 4);
insert into folio_conf.report_folio (report_folio_id, report_id, folio_id, folio_index)
values(folio_conf.seq_report_folio_id.nextval,9, 1458, 5);
insert into folio_conf.report_folio (report_folio_id, report_id, folio_id, folio_index)
values(folio_conf.seq_report_folio_id.nextval,9, 1460, 6);

commit;