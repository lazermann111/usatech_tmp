-- ======================================================================
-- ===   Sql Script for Database : Postgresql
-- ===
-- === Build : 560
-- ======================================================================
CREATE ROLE write_main;
CREATE ROLE read_main;

CREATE ROLE admin
  SUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;
GRANT read_main TO admin;
GRANT write_main TO admin;

CREATE SCHEMA MAIN
  AUTHORIZATION admin;
-- ======================================================================

CREATE OR REPLACE FUNCTION MAIN.FTRBU_AUDIT()
	RETURNS TRIGGER
	SECURITY DEFINER
AS $$
BEGIN
	NEW.CREATED_UTC_TS := OLD.CREATED_UTC_TS;
	NEW.CREATED_BY := OLD.CREATED_BY;
	NEW.LAST_UPDATED_UTC_TS := STATEMENT_TIMESTAMP() AT TIME ZONE 'UTC';
	NEW.LAST_UPDATED_BY := SESSION_USER;
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

-- ======================================================================

CREATE OR REPLACE FUNCTION MAIN.FTRBI_AUDIT()
	RETURNS TRIGGER
	SECURITY DEFINER
AS $$
BEGIN
	NEW.CREATED_UTC_TS := STATEMENT_TIMESTAMP() AT TIME ZONE 'UTC';
	NEW.CREATED_BY := SESSION_USER;
	NEW.LAST_UPDATED_UTC_TS := STATEMENT_TIMESTAMP() AT TIME ZONE 'UTC';
	NEW.LAST_UPDATED_BY := SESSION_USER;
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

-- ======================================================================

CREATE OR REPLACE FUNCTION MAIN.CONSTRUCT_NAME(pv_base_name VARCHAR, pv_suffix VARCHAR)
	RETURNS VARCHAR
	SECURITY DEFINER
AS $$
BEGIN
	IF LENGTH(pv_base_name) + LENGTH(pv_suffix) <= 60 THEN
		RETURN pv_base_name || '_' || pv_suffix;
	ELSIF LENGTH(pv_base_name) > 51 AND LENGTH(pv_suffix) > 51 THEN
		RETURN SUBSTR(pv_base_name, 1, 21) || '$' || SUBSTR(MD5(pv_base_name), 1, 8) || '_' || SUBSTR(pv_suffix, 1, 21) || '$' || SUBSTR(MD5(pv_suffix), 1, 8);
	ELSIF LENGTH(pv_base_name) > 21 THEN
		RETURN SUBSTR(pv_base_name, 1, 51 - LENGTH(pv_suffix)) || '$' || SUBSTR(MD5(pv_base_name), 1, 8) || '_' || pv_suffix;
	ELSE
		RETURN pv_base_name || '_' || SUBSTR(pv_suffix, 1,  51 - LENGTH(pv_base_name)) || '_$' || SUBSTR(MD5(pv_suffix), 1, 8);
	END IF;
END;
$$ LANGUAGE plpgsql;

-- ======================================================================

CREATE OR REPLACE FUNCTION MAIN.COPY_INDEXES(pv_source_table VARCHAR, pv_suffix VARCHAR)
    RETURNS SMALLINT
    SECURITY DEFINER
AS $$
DECLARE
    lv_sql TEXT;
    ln_cnt SMALLINT := 0;
BEGIN
    FOR lv_sql IN SELECT 'CREATE '||CASE WHEN x.indisunique THEN 'UNIQUE ' ELSE '' END ||'INDEX ' || MAIN.CONSTRUCT_NAME(CAST(i.relname AS VARCHAR), pv_suffix) || ' ON '
            || CASE WHEN n.nspname IS NOT NULL AND n.nspname != 'pg_catalog' THEN n.nspname || '.' ELSE '' END || MAIN.CONSTRUCT_NAME(CAST(c.relname AS VARCHAR), pv_suffix) || CASE WHEN a.amname IS NOT NULL AND a.amname != 'btree' THEN ' USING ' || a.amname || ' ' ELSE '' END || '('
            || (SELECT ARRAY_TO_STRING(ARRAY_AGG(t.col_desc), cast(', ' as text))
                  FROM (SELECT CASE WHEN b.attname IS NOT NULL THEN b.attname || CASE WHEN x.indoption[s.s-1] & 3 = 3 THEN ' DESC'  ELSE '' END WHEN x.indexprs IS NOT NULL THEN PG_GET_EXPR(x.indexprs, x.indrelid) ELSE '' END 
            || CASE WHEN o.collname IS NOT NULL AND o.collname != 'default' THEN ' COLLATE ' || o.collname ELSE '' END
            || CASE WHEN p.opcname IS NOT NULL AND y.typname || '_ops' != p.opcname THEN ' ' || p.opcname ELSE '' END col_desc
            FROM (
            SELECT generate_series(1,x.indnatts) s) s
            LEFT JOIN pg_attribute b on x.indrelid = b.attrelid and x.indkey[s.s-1] = b.attnum
            LEFT JOIN pg_type y on x.indclass[s.s-1] = y.oid
            LEFT JOIN pg_collation o on x.indcollation[s.s-1] = o.oid
            LEFT JOIN pg_opclass p on x.indclass[s.s-1] = p.oid AND NOT(p.opcintype = b.atttypid AND p.opcdefault)
            ORDER BY s.s) t)
            ||')' || CASE WHEN t.spcname IS NOT NULL THEN ' TABLESPACE ' || t.spcname ELSE '' END
            || CASE WHEN x.indpred IS NOT NULL THEN ' WHERE ' || x.indpred ELSE '' END
          FROM pg_index x 
          JOIN pg_class i ON i.oid = x.indexrelid 
          JOIN pg_class c ON c.oid = x.indrelid 
          LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
          LEFT JOIN pg_tablespace t ON t.oid = i.reltablespace
          LEFT JOIN pg_am a ON i.relam = a.oid
          LEFT JOIN (pg_index ox JOIN pg_class oi ON oi.oid = ox.indexrelid JOIN pg_class oc ON oc.oid = ox.indrelid) ON MAIN.CONSTRUCT_NAME(CAST(i.relname AS VARCHAR), pv_suffix) = oi.relname AND oc.relname = MAIN.CONSTRUCT_NAME(CAST(c.relname AS VARCHAR), pv_suffix)
        WHERE c.relkind = 'r'
          AND i.relkind = 'i'
          AND c.relname = LOWER(pv_source_table)
          AND n.nspname = 'main' 
          AND NOT x.indisprimary
          AND oi.oid IS NULL
    LOOP
        EXECUTE lv_sql;
        ln_cnt := ln_cnt + 1;
    END LOOP;
   RETURN ln_cnt;     
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MAIN.COPY_INDEXES_TO_CHILDREN(pv_source_table VARCHAR)
    RETURNS SMALLINT
    SECURITY DEFINER
AS $$
DECLARE
    ln_cnt SMALLINT := 0;
BEGIN
    SELECT SUM(MAIN.COPY_INDEXES(pv_source_table, 
           CASE 
               WHEN LOWER(LEFT(c.relname, LENGTH(p.relname) + 1)) = LOWER(p.relname) || '_' THEN SUBSTR(c.relname, LENGTH(p.relname) + 2)
               WHEN c.relname ~ '.+$\d{8}_.+' THEN (REGEXP_MATCHES(c.relname, '.+$\d{8}_(.+)'))[0]
           END))
      INTO ln_cnt
      FROM pg_class p
      JOIN pg_inherits h ON p.oid = h.inhparent
      JOIN pg_class c ON h.inhrelid = c.oid
      LEFT JOIN pg_namespace n ON n.oid = c.relnamespace              
     WHERE p.relkind = 'r'
       AND p.relname = LOWER(pv_source_table)
       AND n.nspname = 'main';         
    RETURN ln_cnt;     
END;
$$ LANGUAGE plpgsql;

-- ======================================================================

CREATE TABLE MAIN._
  (
    CREATED_UTC_TS       TIMESTAMP     NOT NULL,
    CREATED_BY           VARCHAR(60)   NOT NULL DEFAULT CURRENT_USER,
    LAST_UPDATED_UTC_TS  TIMESTAMP     NOT NULL,
    LAST_UPDATED_BY      VARCHAR(60)   NOT NULL DEFAULT CURRENT_USER
  );

DROP TRIGGER IF EXISTS TRBI__ ON MAIN._;
CREATE TRIGGER TRBI__
BEFORE INSERT ON MAIN._
	FOR EACH ROW
	EXECUTE PROCEDURE MAIN.FTRBI_AUDIT();

DROP TRIGGER IF EXISTS TRBU__ ON MAIN._;
CREATE TRIGGER TRBU__
BEFORE UPDATE ON MAIN._
	FOR EACH ROW
	EXECUTE PROCEDURE MAIN.FTRBU_AUDIT();



-- ======================================================================

CREATE TABLE MAIN.MESSAGE_STAGE
  (
    MESSAGE_STAGE_ID       INTEGER,
    MESSAGE_STAGE_NAME     VARCHAR(200)   NOT NULL,
    MESSAGE_CATEGORY       VARCHAR(200)   NOT NULL,
    FINAL_FLAG             BOOLEAN        NOT NULL,
    ERROR_FLAG             BOOLEAN        NOT NULL,
    MAX_LATENCY_TOLERATED  INTEGER        NOT NULL,

    PRIMARY KEY(MESSAGE_STAGE_ID)
  )
  INHERITS(MAIN._);

DROP TRIGGER IF EXISTS TRBI_MESSAGE_STAGE ON MAIN.MESSAGE_STAGE;
CREATE TRIGGER TRBI_MESSAGE_STAGE
BEFORE INSERT ON MAIN.MESSAGE_STAGE
	FOR EACH ROW
	EXECUTE PROCEDURE MAIN.FTRBI_AUDIT();

DROP TRIGGER IF EXISTS TRBU_MESSAGE_STAGE ON MAIN.MESSAGE_STAGE;
CREATE TRIGGER TRBU_MESSAGE_STAGE
BEFORE UPDATE ON MAIN.MESSAGE_STAGE
	FOR EACH ROW
	EXECUTE PROCEDURE MAIN.FTRBU_AUDIT();


CREATE OR REPLACE FUNCTION MAIN.UPSERT_MESSAGE_STAGE(
	p_message_stage_id INTEGER, 
	p_message_stage_name VARCHAR(200), 
	p_message_category VARCHAR(200), 
	p_final_flag BOOLEAN, 
	p_error_flag BOOLEAN, 
	p_max_latency_tolerated INTEGER,
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
BEGIN
	LOOP
		BEGIN
			UPDATE MAIN.MESSAGE_STAGE
			   SET MESSAGE_STAGE_NAME = p_message_stage_name,
			       MESSAGE_CATEGORY = p_message_category,
			       FINAL_FLAG = p_final_flag,
			       ERROR_FLAG = p_error_flag,
			       MAX_LATENCY_TOLERATED = p_max_latency_tolerated
			 WHERE MESSAGE_STAGE_ID IS NOT DISTINCT FROM p_message_stage_id;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				INSERT INTO MAIN.MESSAGE_STAGE(MESSAGE_STAGE_ID, MESSAGE_STAGE_NAME, MESSAGE_CATEGORY, FINAL_FLAG, ERROR_FLAG, MAX_LATENCY_TOLERATED)
				 VALUES(p_message_stage_id, p_message_stage_name, p_message_category, p_final_flag, p_error_flag, p_max_latency_tolerated);
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			END IF;
			EXIT;
		EXCEPTION
			WHEN unique_violation THEN
				NULL;
		END;
	END LOOP;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MAIN.UPSERT_MESSAGE_STAGE(
	p_message_stage_id INTEGER, 
	p_message_stage_name VARCHAR(200), 
	p_message_category VARCHAR(200), 
	p_final_flag BOOLEAN, 
	p_error_flag BOOLEAN, 
	p_max_latency_tolerated INTEGER,
	p_row_count OUT INTEGER) TO write_main;



-- ======================================================================

CREATE TABLE MAIN.NET_LAYER
  (
    NET_LAYER_ID              SERIAL,
    NET_LAYER_DESC            VARCHAR(60)   NOT NULL,
    NET_LAYER_ACTIVE_YN_FLAG  CHAR(1)       NOT NULL,
    SERVER_NAME               VARCHAR(60)   NOT NULL,
    SERVER_PORT_NUM           INTEGER       NOT NULL,

    PRIMARY KEY(NET_LAYER_ID)
  )
  INHERITS(MAIN._);

DROP TRIGGER IF EXISTS TRBI_NET_LAYER ON MAIN.NET_LAYER;
CREATE TRIGGER TRBI_NET_LAYER
BEFORE INSERT ON MAIN.NET_LAYER
	FOR EACH ROW
	EXECUTE PROCEDURE MAIN.FTRBI_AUDIT();

DROP TRIGGER IF EXISTS TRBU_NET_LAYER ON MAIN.NET_LAYER;
CREATE TRIGGER TRBU_NET_LAYER
BEFORE UPDATE ON MAIN.NET_LAYER
	FOR EACH ROW
	EXECUTE PROCEDURE MAIN.FTRBU_AUDIT();


CREATE OR REPLACE FUNCTION MAIN.UPSERT_NET_LAYER(
	p_net_layer_id OUT INTEGER, 
	p_net_layer_desc VARCHAR(60), 
	p_net_layer_active_yn_flag CHAR(1), 
	p_server_name VARCHAR(60), 
	p_server_port_num INTEGER,
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
BEGIN
	LOOP
		BEGIN
			UPDATE MAIN.NET_LAYER
			   SET NET_LAYER_ACTIVE_YN_FLAG = p_net_layer_active_yn_flag,
			       SERVER_NAME = p_server_name,
			       SERVER_PORT_NUM = p_server_port_num
			 WHERE NET_LAYER_DESC = p_net_layer_desc
			  RETURNING NET_LAYER_ID
			  INTO p_net_layer_id;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				INSERT INTO MAIN.NET_LAYER(NET_LAYER_DESC, NET_LAYER_ACTIVE_YN_FLAG, SERVER_NAME, SERVER_PORT_NUM)
				 VALUES(p_net_layer_desc, p_net_layer_active_yn_flag, p_server_name, p_server_port_num)
				 RETURNING NET_LAYER_ID
				      INTO p_net_layer_id;
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			END IF;
			EXIT;
		EXCEPTION
			WHEN unique_violation THEN
				NULL;
		END;
	END LOOP;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MAIN.UPSERT_NET_LAYER(
	p_net_layer_id OUT INTEGER, 
	p_net_layer_desc VARCHAR(60), 
	p_net_layer_active_yn_flag CHAR(1), 
	p_server_name VARCHAR(60), 
	p_server_port_num INTEGER,
	p_row_count OUT INTEGER) TO write_main;



CREATE UNIQUE INDEX AK_NET_LAYER ON MAIN.NET_LAYER(NET_LAYER_DESC);

-- ======================================================================

CREATE TABLE MAIN.SESSION_STATE
  (
    SESSION_STATE_ID    SMALLINT,
    SESSION_STATE_DESC  VARCHAR(100)   NOT NULL,

    PRIMARY KEY(SESSION_STATE_ID)
  )
  INHERITS(MAIN._);

DROP TRIGGER IF EXISTS TRBI_SESSION_STATE ON MAIN.SESSION_STATE;
CREATE TRIGGER TRBI_SESSION_STATE
BEFORE INSERT ON MAIN.SESSION_STATE
	FOR EACH ROW
	EXECUTE PROCEDURE MAIN.FTRBI_AUDIT();

DROP TRIGGER IF EXISTS TRBU_SESSION_STATE ON MAIN.SESSION_STATE;
CREATE TRIGGER TRBU_SESSION_STATE
BEFORE UPDATE ON MAIN.SESSION_STATE
	FOR EACH ROW
	EXECUTE PROCEDURE MAIN.FTRBU_AUDIT();


CREATE OR REPLACE FUNCTION MAIN.UPSERT_SESSION_STATE(
	p_session_state_id SMALLINT, 
	p_session_state_desc VARCHAR(100),
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
BEGIN
	LOOP
		BEGIN
			UPDATE MAIN.SESSION_STATE
			   SET SESSION_STATE_DESC = p_session_state_desc
			 WHERE SESSION_STATE_ID IS NOT DISTINCT FROM p_session_state_id;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				INSERT INTO MAIN.SESSION_STATE(SESSION_STATE_ID, SESSION_STATE_DESC)
				 VALUES(p_session_state_id, p_session_state_desc);
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			END IF;
			EXIT;
		EXCEPTION
			WHEN unique_violation THEN
				NULL;
		END;
	END LOOP;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MAIN.UPSERT_SESSION_STATE(
	p_session_state_id SMALLINT, 
	p_session_state_desc VARCHAR(100),
	p_row_count OUT INTEGER) TO write_main;



-- ----------------------------------------------------------------------

INSERT INTO MAIN.SESSION_STATE(SESSION_STATE_ID, SESSION_STATE_DESC) VALUES(1, 'Session Started');
INSERT INTO MAIN.SESSION_STATE(SESSION_STATE_ID, SESSION_STATE_DESC) VALUES(2, 'Session Completed');
INSERT INTO MAIN.SESSION_STATE(SESSION_STATE_ID, SESSION_STATE_DESC) VALUES(3, 'Session Aborted');
COMMIT;

-- ======================================================================

CREATE TABLE MAIN.DEVICE_TYPE
  (
    DEVICE_TYPE_ID    INTEGER,
    DEVICE_TYPE_DESC  VARCHAR(60)   NOT NULL,

    PRIMARY KEY(DEVICE_TYPE_ID)
  )
  INHERITS(MAIN._);

DROP TRIGGER IF EXISTS TRBI_DEVICE_TYPE ON MAIN.DEVICE_TYPE;
CREATE TRIGGER TRBI_DEVICE_TYPE
BEFORE INSERT ON MAIN.DEVICE_TYPE
	FOR EACH ROW
	EXECUTE PROCEDURE MAIN.FTRBI_AUDIT();

DROP TRIGGER IF EXISTS TRBU_DEVICE_TYPE ON MAIN.DEVICE_TYPE;
CREATE TRIGGER TRBU_DEVICE_TYPE
BEFORE UPDATE ON MAIN.DEVICE_TYPE
	FOR EACH ROW
	EXECUTE PROCEDURE MAIN.FTRBU_AUDIT();


CREATE OR REPLACE FUNCTION MAIN.UPSERT_DEVICE_TYPE(
	p_device_type_id INTEGER, 
	p_device_type_desc VARCHAR(60),
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
BEGIN
	LOOP
		BEGIN
			UPDATE MAIN.DEVICE_TYPE
			   SET DEVICE_TYPE_DESC = p_device_type_desc
			 WHERE DEVICE_TYPE_ID IS NOT DISTINCT FROM p_device_type_id;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				INSERT INTO MAIN.DEVICE_TYPE(DEVICE_TYPE_ID, DEVICE_TYPE_DESC)
				 VALUES(p_device_type_id, p_device_type_desc);
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			END IF;
			EXIT;
		EXCEPTION
			WHEN unique_violation THEN
				NULL;
		END;
	END LOOP;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MAIN.UPSERT_DEVICE_TYPE(
	p_device_type_id INTEGER, 
	p_device_type_desc VARCHAR(60),
	p_row_count OUT INTEGER) TO write_main;



-- ----------------------------------------------------------------------

INSERT INTO MAIN.DEVICE_TYPE(DEVICE_TYPE_ID, DEVICE_TYPE_DESC) VALUES(0, 'G4 ePort');
INSERT INTO MAIN.DEVICE_TYPE(DEVICE_TYPE_ID, DEVICE_TYPE_DESC) VALUES(1, 'Gx ePort');
INSERT INTO MAIN.DEVICE_TYPE(DEVICE_TYPE_ID, DEVICE_TYPE_DESC) VALUES(3, 'ePort NG');
INSERT INTO MAIN.DEVICE_TYPE(DEVICE_TYPE_ID, DEVICE_TYPE_DESC) VALUES(4, 'Legacy Kiosk (Sony DLL)');
INSERT INTO MAIN.DEVICE_TYPE(DEVICE_TYPE_ID, DEVICE_TYPE_DESC) VALUES(5, 'eSuds Room Controller');
INSERT INTO MAIN.DEVICE_TYPE(DEVICE_TYPE_ID, DEVICE_TYPE_DESC) VALUES(6, 'MEI ePort');
INSERT INTO MAIN.DEVICE_TYPE(DEVICE_TYPE_ID, DEVICE_TYPE_DESC) VALUES(7, 'EZ80 ePort Development');
INSERT INTO MAIN.DEVICE_TYPE(DEVICE_TYPE_ID, DEVICE_TYPE_DESC) VALUES(8, 'EZ80 ePort');
INSERT INTO MAIN.DEVICE_TYPE(DEVICE_TYPE_ID, DEVICE_TYPE_DESC) VALUES(9, 'Legacy G4 ePort');
INSERT INTO MAIN.DEVICE_TYPE(DEVICE_TYPE_ID, DEVICE_TYPE_DESC) VALUES(10, 'Transact');
INSERT INTO MAIN.DEVICE_TYPE(DEVICE_TYPE_ID, DEVICE_TYPE_DESC) VALUES(11, 'Kiosk');
INSERT INTO MAIN.DEVICE_TYPE(DEVICE_TYPE_ID, DEVICE_TYPE_DESC) VALUES(12, 'T2');
INSERT INTO MAIN.DEVICE_TYPE(DEVICE_TYPE_ID, DEVICE_TYPE_DESC) VALUES(13, 'ePort Edge');
INSERT INTO MAIN.DEVICE_TYPE(DEVICE_TYPE_ID, DEVICE_TYPE_DESC) VALUES(14, 'Prepaid Virtual');
COMMIT;

-- ======================================================================

CREATE TABLE MAIN.PARTITION_TYPE
  (
    PARTITION_TYPE_CD    CHAR(1)        NOT NULL,
    PARTITION_TYPE_NAME  VARCHAR(200)   NOT NULL,

    PRIMARY KEY(PARTITION_TYPE_CD)
  )
  INHERITS(MAIN._);

DROP TRIGGER IF EXISTS TRBI_PARTITION_TYPE ON MAIN.PARTITION_TYPE;
CREATE TRIGGER TRBI_PARTITION_TYPE
BEFORE INSERT ON MAIN.PARTITION_TYPE
	FOR EACH ROW
	EXECUTE PROCEDURE MAIN.FTRBI_AUDIT();

DROP TRIGGER IF EXISTS TRBU_PARTITION_TYPE ON MAIN.PARTITION_TYPE;
CREATE TRIGGER TRBU_PARTITION_TYPE
BEFORE UPDATE ON MAIN.PARTITION_TYPE
	FOR EACH ROW
	EXECUTE PROCEDURE MAIN.FTRBU_AUDIT();


CREATE OR REPLACE FUNCTION MAIN.UPSERT_PARTITION_TYPE(
	p_partition_type_cd CHAR(1), 
	p_partition_type_name VARCHAR(200),
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
BEGIN
	LOOP
		BEGIN
			UPDATE MAIN.PARTITION_TYPE
			   SET PARTITION_TYPE_NAME = p_partition_type_name
			 WHERE PARTITION_TYPE_CD = p_partition_type_cd;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				INSERT INTO MAIN.PARTITION_TYPE(PARTITION_TYPE_CD, PARTITION_TYPE_NAME)
				 VALUES(p_partition_type_cd, p_partition_type_name);
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			END IF;
			EXIT;
		EXCEPTION
			WHEN unique_violation THEN
				NULL;
		END;
	END LOOP;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MAIN.UPSERT_PARTITION_TYPE(
	p_partition_type_cd CHAR(1), 
	p_partition_type_name VARCHAR(200),
	p_row_count OUT INTEGER) TO write_main;



-- ----------------------------------------------------------------------

INSERT INTO MAIN.PARTITION_TYPE(PARTITION_TYPE_CD, PARTITION_TYPE_NAME) SELECT 'D', 'Daily' WHERE NOT EXISTS (SELECT 1 FROM MAIN.PARTITION_TYPE WHERE PARTITION_TYPE_CD = 'D');
INSERT INTO MAIN.PARTITION_TYPE(PARTITION_TYPE_CD, PARTITION_TYPE_NAME) SELECT 'M', 'Monthly' WHERE NOT EXISTS (SELECT 1 FROM MAIN.PARTITION_TYPE WHERE PARTITION_TYPE_CD = 'M');
COMMIT;

-- ======================================================================

CREATE TABLE MAIN.PARTITION_INFO
  (
    PARTITION_INFO_ID     INTEGER         NOT NULL,
    TABLE_NAME            VARCHAR(200)    NOT NULL,
    PARTITION_TYPE_CD     CHAR(1)         NOT NULL,
    PARTITION_NAME_REGEX  VARCHAR(4000)   NOT NULL,
    RETENTION             BIGINT          NOT NULL,
    RETENTION_GOLD        BIGINT,
    RETENTION_SILVER      BIGINT,
    TABLESPACE_DEFAULT    VARCHAR(200)    DEFAULT 'pg_default',
    TABLESPACE_SILVER     VARCHAR(200),
    TABLESPACE_BRONZE     VARCHAR(200),

    PRIMARY KEY(PARTITION_INFO_ID),

    FOREIGN KEY(PARTITION_TYPE_CD) REFERENCES MAIN.PARTITION_TYPE(PARTITION_TYPE_CD)
  )
  INHERITS(MAIN._);

DROP TRIGGER IF EXISTS TRBI_PARTITION_INFO ON MAIN.PARTITION_INFO;
CREATE TRIGGER TRBI_PARTITION_INFO
BEFORE INSERT ON MAIN.PARTITION_INFO
	FOR EACH ROW
	EXECUTE PROCEDURE MAIN.FTRBI_AUDIT();

DROP TRIGGER IF EXISTS TRBU_PARTITION_INFO ON MAIN.PARTITION_INFO;
CREATE TRIGGER TRBU_PARTITION_INFO
BEFORE UPDATE ON MAIN.PARTITION_INFO
	FOR EACH ROW
	EXECUTE PROCEDURE MAIN.FTRBU_AUDIT();


CREATE OR REPLACE FUNCTION MAIN.UPSERT_PARTITION_INFO(
	p_partition_info_id OUT INTEGER, 
	p_table_name VARCHAR(200), 
	p_partition_type_cd CHAR(1), 
	p_partition_name_regex VARCHAR(4000), 
	p_retention BIGINT,
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
BEGIN
	LOOP
		BEGIN
			UPDATE MAIN.PARTITION_INFO
			   SET PARTITION_TYPE_CD = p_partition_type_cd,
			       PARTITION_NAME_REGEX = p_partition_name_regex,
			       RETENTION = p_retention
			 WHERE TABLE_NAME = p_table_name
			  RETURNING PARTITION_INFO_ID
			  INTO p_partition_info_id;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				INSERT INTO MAIN.PARTITION_INFO(TABLE_NAME, PARTITION_TYPE_CD, PARTITION_NAME_REGEX, RETENTION)
				 VALUES(p_table_name, p_partition_type_cd, p_partition_name_regex, p_retention)
				 RETURNING PARTITION_INFO_ID
				      INTO p_partition_info_id;
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			END IF;
			EXIT;
		EXCEPTION
			WHEN unique_violation THEN
				NULL;
		END;
	END LOOP;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MAIN.UPSERT_PARTITION_INFO(
	p_partition_info_id OUT INTEGER, 
	p_table_name VARCHAR(200), 
	p_partition_type_cd CHAR(1), 
	p_partition_name_regex VARCHAR(4000), 
	p_retention BIGINT,
	p_row_count OUT INTEGER) TO write_main;



CREATE UNIQUE INDEX AK_PARTITION_INFO ON MAIN.PARTITION_INFO(TABLE_NAME);

-- ----------------------------------------------------------------------

INSERT INTO MAIN.PARTITION_INFO(PARTITION_INFO_ID, TABLE_NAME, PARTITION_TYPE_CD, PARTITION_NAME_REGEX, RETENTION) SELECT 1, 'device_session', 'D', '_device_session_([0-9]{4}_[0-9]{2}_[0-9]{2})', 180 WHERE NOT EXISTS (SELECT 1 FROM MAIN.PARTITION_INFO WHERE PARTITION_INFO_ID = 1);
INSERT INTO MAIN.PARTITION_INFO(PARTITION_INFO_ID, TABLE_NAME, PARTITION_TYPE_CD, PARTITION_NAME_REGEX, RETENTION) SELECT 2, 'device_message', 'D', '_device_message_([0-9]{4}_[0-9]{2}_[0-9]{2})', 180 WHERE NOT EXISTS (SELECT 1 FROM MAIN.PARTITION_INFO WHERE PARTITION_INFO_ID = 2);
INSERT INTO MAIN.PARTITION_INFO(PARTITION_INFO_ID, TABLE_NAME, PARTITION_TYPE_CD, PARTITION_NAME_REGEX, RETENTION) SELECT 3, 'app_request', 'M', '_app_request_([0-9]{4}_[0-9]{2})', 84 WHERE NOT EXISTS (SELECT 1 FROM MAIN.PARTITION_INFO WHERE PARTITION_INFO_ID = 3);
COMMIT;

-- ======================================================================

CREATE TABLE MAIN.USER_TERMINAL
  (
    USER_ID      BIGINT    NOT NULL,
    TERMINAL_ID  BIGINT    NOT NULL,
    ALLOW_EDIT   BOOLEAN   NOT NULL,

    PRIMARY KEY(USER_ID,TERMINAL_ID)
  )
  INHERITS(MAIN._);

DROP TRIGGER IF EXISTS TRBI_USER_TERMINAL ON MAIN.USER_TERMINAL;
CREATE TRIGGER TRBI_USER_TERMINAL
BEFORE INSERT ON MAIN.USER_TERMINAL
	FOR EACH ROW
	EXECUTE PROCEDURE MAIN.FTRBI_AUDIT();

DROP TRIGGER IF EXISTS TRBU_USER_TERMINAL ON MAIN.USER_TERMINAL;
CREATE TRIGGER TRBU_USER_TERMINAL
BEFORE UPDATE ON MAIN.USER_TERMINAL
	FOR EACH ROW
	EXECUTE PROCEDURE MAIN.FTRBU_AUDIT();


CREATE OR REPLACE FUNCTION MAIN.UPSERT_USER_TERMINAL(
	p_user_id BIGINT, 
	p_terminal_id BIGINT, 
	p_allow_edit BOOLEAN,
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
BEGIN
	LOOP
		BEGIN
			UPDATE MAIN.USER_TERMINAL
			   SET ALLOW_EDIT = p_allow_edit
			 WHERE USER_ID = p_user_id
			   AND TERMINAL_ID = p_terminal_id;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				INSERT INTO MAIN.USER_TERMINAL(USER_ID, TERMINAL_ID, ALLOW_EDIT)
				 VALUES(p_user_id, p_terminal_id, p_allow_edit);
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			END IF;
			EXIT;
		EXCEPTION
			WHEN unique_violation THEN
				NULL;
		END;
	END LOOP;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MAIN.UPSERT_USER_TERMINAL(
	p_user_id BIGINT, 
	p_terminal_id BIGINT, 
	p_allow_edit BOOLEAN,
	p_row_count OUT INTEGER) TO write_main;



-- ======================================================================

CREATE TABLE MAIN._APP_REQUEST
  (
    APP_REQUEST_ID      BIGSERIAL                  NOT NULL,
    APP_CD              VARCHAR(50)                NOT NULL,
    REQUEST_URL         TEXT                       NOT NULL,
    REQUEST_TS          TIMESTAMP WITH TIME ZONE   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    REMOTE_ADDR         VARCHAR(39),
    REMOTE_PORT         INTEGER,
    REFERER             TEXT,
    HTTP_USER_AGENT     TEXT,
    SESSION_ID          VARCHAR(100)               NOT NULL,
    USERNAME            VARCHAR(100),
    REQUEST_HEADERS     TEXT,
    REQUEST_PARAMETERS  TEXT,
    PROCESS_TIME_MS     INTEGER                    NOT NULL,
    EXCEPTION_FLAG      CHAR(1)                    NOT NULL,
    EXCEPTION_TEXT      TEXT,
    REQUEST_HANDLER     VARCHAR(100),
    FOLIO_ID            INTEGER,
    REPORT_ID           INTEGER,
    PROFILE_ID          INTEGER,
    ACTION_NAME         VARCHAR(100),
    OBJECT_TYPE_CD      VARCHAR(50),
    OBJECT_CD           VARCHAR(200),
    USER_ID             BIGINT,

    PRIMARY KEY(APP_REQUEST_ID)
  );

CREATE OR REPLACE FUNCTION MAIN.FRIIUD_APP_REQUEST()
	RETURNS TRIGGER
	SECURITY DEFINER
AS $$
DECLARE
	lv_old_partition_table VARCHAR(4000);
	lv_new_partition_table VARCHAR(4000);
	lv_new_suffix VARCHAR(4000);
	lv_check_exp VARCHAR(4000);
	lv_sql TEXT;
	lv_filter TEXT;
	ln_cnt INTEGER;
BEGIN
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		SELECT COALESCE(TO_CHAR(NEW.REQUEST_TS, 'YYYY_MM'),'') INTO lv_new_suffix;
		SELECT MAIN.CONSTRUCT_NAME('_APP_REQUEST', lv_new_suffix) INTO lv_new_partition_table;
	END IF;
	IF TG_OP IN('DELETE', 'UPDATE') THEN
		SELECT MAIN.CONSTRUCT_NAME('_APP_REQUEST', COALESCE(TO_CHAR(OLD.REQUEST_TS, 'YYYY_MM'), '')) INTO lv_old_partition_table;
	END IF;
	IF lv_new_partition_table IS NOT NULL AND lv_old_partition_table IS NOT NULL AND lv_new_partition_table = lv_old_partition_table THEN
		lv_sql := 'UPDATE MAIN.' || lv_new_partition_table || ' SET ';
		lv_filter := '';
		IF OLD.APP_REQUEST_ID != NEW.APP_REQUEST_ID THEN
			lv_sql := lv_sql || 'APP_REQUEST_ID = $2.APP_REQUEST_ID, ';
		END IF;
		IF OLD.APP_CD != NEW.APP_CD THEN
			lv_sql := lv_sql || 'APP_CD = $2.APP_CD, ';
			lv_filter := lv_filter || ' AND (APP_CD = $1.APP_CD OR APP_CD = $2.APP_CD)';
		END IF;
		IF OLD.REQUEST_URL != NEW.REQUEST_URL THEN
			lv_sql := lv_sql || 'REQUEST_URL = $2.REQUEST_URL, ';
			lv_filter := lv_filter || ' AND (REQUEST_URL = $1.REQUEST_URL OR REQUEST_URL = $2.REQUEST_URL)';
		END IF;
		IF OLD.REQUEST_TS != NEW.REQUEST_TS THEN
			lv_sql := lv_sql || 'REQUEST_TS = $2.REQUEST_TS, ';
			lv_filter := lv_filter || ' AND (REQUEST_TS = $1.REQUEST_TS OR REQUEST_TS = $2.REQUEST_TS)';
		END IF;
		IF OLD.REMOTE_ADDR IS DISTINCT FROM NEW.REMOTE_ADDR THEN
			lv_sql := lv_sql || 'REMOTE_ADDR = $2.REMOTE_ADDR, ';
			lv_filter := lv_filter || ' AND (REMOTE_ADDR IS NOT DISTINCT FROM $1.REMOTE_ADDR OR REMOTE_ADDR IS NOT DISTINCT FROM $2.REMOTE_ADDR)';
		END IF;
		IF OLD.REMOTE_PORT IS DISTINCT FROM NEW.REMOTE_PORT THEN
			lv_sql := lv_sql || 'REMOTE_PORT = $2.REMOTE_PORT, ';
			lv_filter := lv_filter || ' AND (REMOTE_PORT IS NOT DISTINCT FROM $1.REMOTE_PORT OR REMOTE_PORT IS NOT DISTINCT FROM $2.REMOTE_PORT)';
		END IF;
		IF OLD.REFERER IS DISTINCT FROM NEW.REFERER THEN
			lv_sql := lv_sql || 'REFERER = $2.REFERER, ';
			lv_filter := lv_filter || ' AND (REFERER IS NOT DISTINCT FROM $1.REFERER OR REFERER IS NOT DISTINCT FROM $2.REFERER)';
		END IF;
		IF OLD.HTTP_USER_AGENT IS DISTINCT FROM NEW.HTTP_USER_AGENT THEN
			lv_sql := lv_sql || 'HTTP_USER_AGENT = $2.HTTP_USER_AGENT, ';
			lv_filter := lv_filter || ' AND (HTTP_USER_AGENT IS NOT DISTINCT FROM $1.HTTP_USER_AGENT OR HTTP_USER_AGENT IS NOT DISTINCT FROM $2.HTTP_USER_AGENT)';
		END IF;
		IF OLD.SESSION_ID != NEW.SESSION_ID THEN
			lv_sql := lv_sql || 'SESSION_ID = $2.SESSION_ID, ';
			lv_filter := lv_filter || ' AND (SESSION_ID = $1.SESSION_ID OR SESSION_ID = $2.SESSION_ID)';
		END IF;
		IF OLD.USERNAME IS DISTINCT FROM NEW.USERNAME THEN
			lv_sql := lv_sql || 'USERNAME = $2.USERNAME, ';
			lv_filter := lv_filter || ' AND (USERNAME IS NOT DISTINCT FROM $1.USERNAME OR USERNAME IS NOT DISTINCT FROM $2.USERNAME)';
		END IF;
		IF OLD.REQUEST_HEADERS IS DISTINCT FROM NEW.REQUEST_HEADERS THEN
			lv_sql := lv_sql || 'REQUEST_HEADERS = $2.REQUEST_HEADERS, ';
			lv_filter := lv_filter || ' AND (REQUEST_HEADERS IS NOT DISTINCT FROM $1.REQUEST_HEADERS OR REQUEST_HEADERS IS NOT DISTINCT FROM $2.REQUEST_HEADERS)';
		END IF;
		IF OLD.REQUEST_PARAMETERS IS DISTINCT FROM NEW.REQUEST_PARAMETERS THEN
			lv_sql := lv_sql || 'REQUEST_PARAMETERS = $2.REQUEST_PARAMETERS, ';
			lv_filter := lv_filter || ' AND (REQUEST_PARAMETERS IS NOT DISTINCT FROM $1.REQUEST_PARAMETERS OR REQUEST_PARAMETERS IS NOT DISTINCT FROM $2.REQUEST_PARAMETERS)';
		END IF;
		IF OLD.PROCESS_TIME_MS != NEW.PROCESS_TIME_MS THEN
			lv_sql := lv_sql || 'PROCESS_TIME_MS = $2.PROCESS_TIME_MS, ';
			lv_filter := lv_filter || ' AND (PROCESS_TIME_MS = $1.PROCESS_TIME_MS OR PROCESS_TIME_MS = $2.PROCESS_TIME_MS)';
		END IF;
		IF OLD.EXCEPTION_FLAG != NEW.EXCEPTION_FLAG THEN
			lv_sql := lv_sql || 'EXCEPTION_FLAG = $2.EXCEPTION_FLAG, ';
			lv_filter := lv_filter || ' AND (EXCEPTION_FLAG = $1.EXCEPTION_FLAG OR EXCEPTION_FLAG = $2.EXCEPTION_FLAG)';
		END IF;
		IF OLD.EXCEPTION_TEXT IS DISTINCT FROM NEW.EXCEPTION_TEXT THEN
			lv_sql := lv_sql || 'EXCEPTION_TEXT = $2.EXCEPTION_TEXT, ';
			lv_filter := lv_filter || ' AND (EXCEPTION_TEXT IS NOT DISTINCT FROM $1.EXCEPTION_TEXT OR EXCEPTION_TEXT IS NOT DISTINCT FROM $2.EXCEPTION_TEXT)';
		END IF;
		IF OLD.REQUEST_HANDLER IS DISTINCT FROM NEW.REQUEST_HANDLER THEN
			lv_sql := lv_sql || 'REQUEST_HANDLER = $2.REQUEST_HANDLER, ';
			lv_filter := lv_filter || ' AND (REQUEST_HANDLER IS NOT DISTINCT FROM $1.REQUEST_HANDLER OR REQUEST_HANDLER IS NOT DISTINCT FROM $2.REQUEST_HANDLER)';
		END IF;
		IF OLD.FOLIO_ID IS DISTINCT FROM NEW.FOLIO_ID THEN
			lv_sql := lv_sql || 'FOLIO_ID = $2.FOLIO_ID, ';
			lv_filter := lv_filter || ' AND (FOLIO_ID IS NOT DISTINCT FROM $1.FOLIO_ID OR FOLIO_ID IS NOT DISTINCT FROM $2.FOLIO_ID)';
		END IF;
		IF OLD.REPORT_ID IS DISTINCT FROM NEW.REPORT_ID THEN
			lv_sql := lv_sql || 'REPORT_ID = $2.REPORT_ID, ';
			lv_filter := lv_filter || ' AND (REPORT_ID IS NOT DISTINCT FROM $1.REPORT_ID OR REPORT_ID IS NOT DISTINCT FROM $2.REPORT_ID)';
		END IF;
		IF OLD.PROFILE_ID IS DISTINCT FROM NEW.PROFILE_ID THEN
			lv_sql := lv_sql || 'PROFILE_ID = $2.PROFILE_ID, ';
			lv_filter := lv_filter || ' AND (PROFILE_ID IS NOT DISTINCT FROM $1.PROFILE_ID OR PROFILE_ID IS NOT DISTINCT FROM $2.PROFILE_ID)';
		END IF;
		IF OLD.ACTION_NAME IS DISTINCT FROM NEW.ACTION_NAME THEN
			lv_sql := lv_sql || 'ACTION_NAME = $2.ACTION_NAME, ';
			lv_filter := lv_filter || ' AND (ACTION_NAME IS NOT DISTINCT FROM $1.ACTION_NAME OR ACTION_NAME IS NOT DISTINCT FROM $2.ACTION_NAME)';
		END IF;
		IF OLD.OBJECT_TYPE_CD IS DISTINCT FROM NEW.OBJECT_TYPE_CD THEN
			lv_sql := lv_sql || 'OBJECT_TYPE_CD = $2.OBJECT_TYPE_CD, ';
			lv_filter := lv_filter || ' AND (OBJECT_TYPE_CD IS NOT DISTINCT FROM $1.OBJECT_TYPE_CD OR OBJECT_TYPE_CD IS NOT DISTINCT FROM $2.OBJECT_TYPE_CD)';
		END IF;
		IF OLD.OBJECT_CD IS DISTINCT FROM NEW.OBJECT_CD THEN
			lv_sql := lv_sql || 'OBJECT_CD = $2.OBJECT_CD, ';
			lv_filter := lv_filter || ' AND (OBJECT_CD IS NOT DISTINCT FROM $1.OBJECT_CD OR OBJECT_CD IS NOT DISTINCT FROM $2.OBJECT_CD)';
		END IF;
		IF OLD.USER_ID IS DISTINCT FROM NEW.USER_ID THEN
			lv_sql := lv_sql || 'USER_ID = $2.USER_ID, ';
			lv_filter := lv_filter || ' AND (USER_ID IS NOT DISTINCT FROM $1.USER_ID OR USER_ID IS NOT DISTINCT FROM $2.USER_ID)';
		END IF;
		lv_sql := LEFT(lv_sql, -2) || ' WHERE APP_REQUEST_ID = $1.APP_REQUEST_ID' || lv_filter;
		EXECUTE lv_sql USING OLD, NEW;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
		RETURN NEW;
	END IF;
	IF lv_new_partition_table IS NOT NULL THEN
		LOOP
			BEGIN
				EXECUTE 'INSERT INTO MAIN.' || lv_new_partition_table || ' SELECT $1.*' USING NEW;
				EXIT;
			EXCEPTION
			WHEN undefined_table THEN
				SELECT 'CONSTRAINT CK_' || lv_new_partition_table || ' CHECK(' ||'REQUEST_TS >= TIMESTAMP'''||TO_CHAR(NEW.REQUEST_TS, 'YYYY-MM-01')||''' AND REQUEST_TS < TIMESTAMP'''||TO_CHAR(NEW.REQUEST_TS + '1 month', 'YYYY-MM-01')||''''||'), ' INTO lv_check_exp;
				RAISE NOTICE 'Creating new partition table %', lv_new_partition_table;
				BEGIN
					EXECUTE 'CREATE TABLE MAIN.' || lv_new_partition_table || '(' || lv_check_exp ||'CONSTRAINT PK_' || lv_new_partition_table || ' PRIMARY KEY(APP_REQUEST_ID)) INHERITS(MAIN._APP_REQUEST)';
				EXCEPTION
					WHEN duplicate_table THEN
						CONTINUE;
				END;
				EXECUTE 'GRANT ALL ON TABLE MAIN.' || lv_new_partition_table || ' TO ' || CURRENT_USER;
				PERFORM MAIN.COPY_INDEXES('_APP_REQUEST', lv_new_suffix);
			END;
		END LOOP;
	END IF;
	IF lv_old_partition_table IS NOT NULL THEN
		EXECUTE 'DELETE FROM MAIN.' || lv_old_partition_table || ' WHERE APP_REQUEST_ID = $1.APP_REQUEST_ID' USING OLD;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		RETURN NEW;
	ELSE
		RETURN OLD;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE VIEW MAIN.APP_REQUEST
	AS SELECT * FROM MAIN._APP_REQUEST;

ALTER VIEW MAIN.APP_REQUEST ALTER APP_REQUEST_ID SET DEFAULT nextval('MAIN._app_request_app_request_id_seq'::regclass);
ALTER VIEW MAIN.APP_REQUEST ALTER REQUEST_TS SET DEFAULT CURRENT_TIMESTAMP;

GRANT SELECT ON MAIN.APP_REQUEST TO read_main;
GRANT INSERT, UPDATE, DELETE ON MAIN.APP_REQUEST TO write_main;

DROP TRIGGER IF EXISTS TRIIUD_APP_REQUEST ON MAIN.APP_REQUEST;
CREATE TRIGGER TRIIUD_APP_REQUEST
INSTEAD OF INSERT OR UPDATE OR DELETE ON MAIN.APP_REQUEST
	FOR EACH ROW
	EXECUTE PROCEDURE MAIN.FRIIUD_APP_REQUEST();


CREATE OR REPLACE FUNCTION MAIN.UPSERT_APP_REQUEST(
	p_app_request_id BIGINT, 
	p_app_cd VARCHAR(50), 
	p_request_url TEXT, 
	p_request_ts TIMESTAMP WITH TIME ZONE, 
	p_remote_addr VARCHAR(39), 
	p_remote_port INTEGER, 
	p_referer TEXT, 
	p_http_user_agent TEXT, 
	p_session_id VARCHAR(100), 
	p_username VARCHAR(100), 
	p_request_headers TEXT, 
	p_request_parameters TEXT, 
	p_process_time_ms INTEGER, 
	p_exception_flag CHAR(1), 
	p_exception_text TEXT, 
	p_request_handler VARCHAR(100), 
	p_folio_id INTEGER, 
	p_report_id INTEGER, 
	p_profile_id INTEGER, 
	p_action_name VARCHAR(100), 
	p_object_type_cd VARCHAR(50), 
	p_object_cd VARCHAR(200),
	p_user_id BIGINT,
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
BEGIN
	LOOP
		BEGIN
			UPDATE MAIN.APP_REQUEST
			   SET APP_CD = p_app_cd,
			       REQUEST_URL = p_request_url,
			       REQUEST_TS = COALESCE(p_request_ts, REQUEST_TS),
			       REMOTE_ADDR = p_remote_addr,
			       REMOTE_PORT = p_remote_port,
			       REFERER = p_referer,
			       HTTP_USER_AGENT = p_http_user_agent,
			       SESSION_ID = p_session_id,
			       USERNAME = p_username,
			       REQUEST_HEADERS = p_request_headers,
			       REQUEST_PARAMETERS = p_request_parameters,
			       PROCESS_TIME_MS = p_process_time_ms,
			       EXCEPTION_FLAG = p_exception_flag,
			       EXCEPTION_TEXT = p_exception_text,
			       REQUEST_HANDLER = p_request_handler,
			       FOLIO_ID = p_folio_id,
			       REPORT_ID = p_report_id,
			       PROFILE_ID = p_profile_id,
			       ACTION_NAME = p_action_name,
			       OBJECT_TYPE_CD = p_object_type_cd,
			       OBJECT_CD = p_object_cd,
			       USER_ID = p_user_id
			 WHERE APP_REQUEST_ID = p_app_request_id;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				INSERT INTO MAIN.APP_REQUEST(APP_REQUEST_ID, APP_CD, REQUEST_URL, REQUEST_TS, REMOTE_ADDR, REMOTE_PORT, REFERER, HTTP_USER_AGENT, SESSION_ID, USERNAME, REQUEST_HEADERS, REQUEST_PARAMETERS, PROCESS_TIME_MS, EXCEPTION_FLAG, EXCEPTION_TEXT, REQUEST_HANDLER, FOLIO_ID, REPORT_ID, PROFILE_ID, ACTION_NAME, OBJECT_TYPE_CD, OBJECT_CD, USER_ID)
				 VALUES(p_app_request_id, p_app_cd, p_request_url, COALESCE(p_request_ts, CURRENT_TIMESTAMP), p_remote_addr, p_remote_port, p_referer, p_http_user_agent, p_session_id, p_username, p_request_headers, p_request_parameters, p_process_time_ms, p_exception_flag, p_exception_text, p_request_handler, p_folio_id, p_report_id, p_profile_id, p_action_name, p_object_type_cd, p_object_cd, p_user_id);
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			END IF;
			EXIT;
		EXCEPTION
			WHEN unique_violation THEN
				NULL;
			WHEN serialization_failure THEN
				NULL;
		END;
	END LOOP;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MAIN.UPSERT_APP_REQUEST(
	p_app_request_id BIGINT, 
	p_app_cd VARCHAR(50), 
	p_request_url TEXT, 
	p_request_ts TIMESTAMP WITH TIME ZONE, 
	p_remote_addr VARCHAR(39), 
	p_remote_port INTEGER, 
	p_referer TEXT, 
	p_http_user_agent TEXT, 
	p_session_id VARCHAR(100), 
	p_username VARCHAR(100), 
	p_request_headers TEXT, 
	p_request_parameters TEXT, 
	p_process_time_ms INTEGER, 
	p_exception_flag CHAR(1), 
	p_exception_text TEXT, 
	p_request_handler VARCHAR(100), 
	p_folio_id INTEGER, 
	p_report_id INTEGER, 
	p_profile_id INTEGER, 
	p_action_name VARCHAR(100), 
	p_object_type_cd VARCHAR(50), 
	p_object_cd VARCHAR(200),
	p_user_id BIGINT,
	p_row_count OUT INTEGER) TO write_main;



-- ----------------------------------------------------------------------

CREATE INDEX ON MAIN._APP_REQUEST(REQUEST_TS, APP_CD, OBJECT_CD, OBJECT_TYPE_CD);

-- ======================================================================

CREATE TABLE MAIN.APP
  (
    APP_CD    VARCHAR(50)    NOT NULL,
    APP_NAME  VARCHAR(200)   NOT NULL,

    PRIMARY KEY(APP_CD)
  );


CREATE OR REPLACE FUNCTION MAIN.UPSERT_APP(
	p_app_cd VARCHAR(50), 
	p_app_name VARCHAR(200),
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
BEGIN
	LOOP
		BEGIN
			UPDATE MAIN.APP
			   SET APP_NAME = p_app_name
			 WHERE APP_CD = p_app_cd;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				INSERT INTO MAIN.APP(APP_CD, APP_NAME)
				 VALUES(p_app_cd, p_app_name);
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			END IF;
			EXIT;
		EXCEPTION
			WHEN unique_violation THEN
				NULL;
		END;
	END LOOP;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MAIN.UPSERT_APP(
	p_app_cd VARCHAR(50), 
	p_app_name VARCHAR(200),
	p_row_count OUT INTEGER) TO write_main;



-- ----------------------------------------------------------------------

INSERT INTO MAIN.APP(APP_CD, APP_NAME) SELECT 'DMS', 'DMS' WHERE NOT EXISTS (SELECT 1 FROM MAIN.APP WHERE APP_CD = 'DMS');
COMMIT;

-- ======================================================================

CREATE TABLE MAIN.OBJECT_TYPE
  (
    OBJECT_TYPE_CD    VARCHAR(50)    NOT NULL,
    OBJECT_TYPE_NAME  VARCHAR(200)   NOT NULL,

    PRIMARY KEY(OBJECT_TYPE_CD)
  );


CREATE OR REPLACE FUNCTION MAIN.UPSERT_OBJECT_TYPE(
	p_object_type_cd VARCHAR(50), 
	p_object_type_name VARCHAR(200),
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
BEGIN
	LOOP
		BEGIN
			UPDATE MAIN.OBJECT_TYPE
			   SET OBJECT_TYPE_NAME = p_object_type_name
			 WHERE OBJECT_TYPE_CD = p_object_type_cd;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				INSERT INTO MAIN.OBJECT_TYPE(OBJECT_TYPE_CD, OBJECT_TYPE_NAME)
				 VALUES(p_object_type_cd, p_object_type_name);
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			END IF;
			EXIT;
		EXCEPTION
			WHEN unique_violation THEN
				NULL;
		END;
	END LOOP;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MAIN.UPSERT_OBJECT_TYPE(
	p_object_type_cd VARCHAR(50), 
	p_object_type_name VARCHAR(200),
	p_row_count OUT INTEGER) TO write_main;



-- ----------------------------------------------------------------------

INSERT INTO MAIN.OBJECT_TYPE(OBJECT_TYPE_CD, OBJECT_TYPE_NAME) SELECT 'authority_merchant', 'Authority Merchant' WHERE NOT EXISTS (SELECT 1 FROM MAIN.OBJECT_TYPE WHERE OBJECT_TYPE_CD = 'authority_merchant');
INSERT INTO MAIN.OBJECT_TYPE(OBJECT_TYPE_CD, OBJECT_TYPE_NAME) SELECT 'authority_terminal', 'Authority Terminal' WHERE NOT EXISTS (SELECT 1 FROM MAIN.OBJECT_TYPE WHERE OBJECT_TYPE_CD = 'authority_terminal');
INSERT INTO MAIN.OBJECT_TYPE(OBJECT_TYPE_CD, OBJECT_TYPE_NAME) SELECT 'bank_account', 'Bank Account' WHERE NOT EXISTS (SELECT 1 FROM MAIN.OBJECT_TYPE WHERE OBJECT_TYPE_CD = 'bank_account');
INSERT INTO MAIN.OBJECT_TYPE(OBJECT_TYPE_CD, OBJECT_TYPE_NAME) SELECT 'consumer', 'Consumer' WHERE NOT EXISTS (SELECT 1 FROM MAIN.OBJECT_TYPE WHERE OBJECT_TYPE_CD = 'consumer');
INSERT INTO MAIN.OBJECT_TYPE(OBJECT_TYPE_CD, OBJECT_TYPE_NAME) SELECT 'consumer_acct', 'Consumer Account' WHERE NOT EXISTS (SELECT 1 FROM MAIN.OBJECT_TYPE WHERE OBJECT_TYPE_CD = 'consumer_acct');
INSERT INTO MAIN.OBJECT_TYPE(OBJECT_TYPE_CD, OBJECT_TYPE_NAME) SELECT 'customer', 'Customer' WHERE NOT EXISTS (SELECT 1 FROM MAIN.OBJECT_TYPE WHERE OBJECT_TYPE_CD = 'customer');
INSERT INTO MAIN.OBJECT_TYPE(OBJECT_TYPE_CD, OBJECT_TYPE_NAME) SELECT 'dealer', 'Dealer' WHERE NOT EXISTS (SELECT 1 FROM MAIN.OBJECT_TYPE WHERE OBJECT_TYPE_CD = 'dealer');
INSERT INTO MAIN.OBJECT_TYPE(OBJECT_TYPE_CD, OBJECT_TYPE_NAME) SELECT 'device', 'Device' WHERE NOT EXISTS (SELECT 1 FROM MAIN.OBJECT_TYPE WHERE OBJECT_TYPE_CD = 'device');
INSERT INTO MAIN.OBJECT_TYPE(OBJECT_TYPE_CD, OBJECT_TYPE_NAME) SELECT 'eft', 'EFT' WHERE NOT EXISTS (SELECT 1 FROM MAIN.OBJECT_TYPE WHERE OBJECT_TYPE_CD = 'eft');
INSERT INTO MAIN.OBJECT_TYPE(OBJECT_TYPE_CD, OBJECT_TYPE_NAME) SELECT 'file', 'File' WHERE NOT EXISTS (SELECT 1 FROM MAIN.OBJECT_TYPE WHERE OBJECT_TYPE_CD = 'file');
INSERT INTO MAIN.OBJECT_TYPE(OBJECT_TYPE_CD, OBJECT_TYPE_NAME) SELECT 'license', 'License Agreement' WHERE NOT EXISTS (SELECT 1 FROM MAIN.OBJECT_TYPE WHERE OBJECT_TYPE_CD = 'license');
INSERT INTO MAIN.OBJECT_TYPE(OBJECT_TYPE_CD, OBJECT_TYPE_NAME) SELECT 'location', 'Location' WHERE NOT EXISTS (SELECT 1 FROM MAIN.OBJECT_TYPE WHERE OBJECT_TYPE_CD = 'location');
INSERT INTO MAIN.OBJECT_TYPE(OBJECT_TYPE_CD, OBJECT_TYPE_NAME) SELECT 'reporting_customer', 'Reporting Customer' WHERE NOT EXISTS (SELECT 1 FROM MAIN.OBJECT_TYPE WHERE OBJECT_TYPE_CD = 'reporting_customer');
INSERT INTO MAIN.OBJECT_TYPE(OBJECT_TYPE_CD, OBJECT_TYPE_NAME) SELECT 'terminal', 'Terminal' WHERE NOT EXISTS (SELECT 1 FROM MAIN.OBJECT_TYPE WHERE OBJECT_TYPE_CD = 'terminal');
INSERT INTO MAIN.OBJECT_TYPE(OBJECT_TYPE_CD, OBJECT_TYPE_NAME) SELECT 'user', 'User' WHERE NOT EXISTS (SELECT 1 FROM MAIN.OBJECT_TYPE WHERE OBJECT_TYPE_CD = 'user');
COMMIT;

-- ======================================================================

CREATE TABLE MAIN.MESSAGE_CATEGORY
  (
    MESSAGE_CATEGORY_CD    VARCHAR(30)    NOT NULL,
    MESSAGE_CATEGORY_NAME  VARCHAR(100)   NOT NULL,

    PRIMARY KEY(MESSAGE_CATEGORY_CD)
  );


CREATE OR REPLACE FUNCTION MAIN.UPSERT_MESSAGE_CATEGORY(
	p_message_category_cd VARCHAR(30), 
	p_message_category_name VARCHAR(100),
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
BEGIN
	LOOP
		BEGIN
			UPDATE MAIN.MESSAGE_CATEGORY
			   SET MESSAGE_CATEGORY_NAME = p_message_category_name
			 WHERE MESSAGE_CATEGORY_CD = p_message_category_cd;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				INSERT INTO MAIN.MESSAGE_CATEGORY(MESSAGE_CATEGORY_CD, MESSAGE_CATEGORY_NAME)
				 VALUES(p_message_category_cd, p_message_category_name);
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			END IF;
			EXIT;
		EXCEPTION
			WHEN unique_violation THEN
				NULL;
		END;
	END LOOP;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MAIN.UPSERT_MESSAGE_CATEGORY(
	p_message_category_cd VARCHAR(30), 
	p_message_category_name VARCHAR(100),
	p_row_count OUT INTEGER) TO write_main;



-- ----------------------------------------------------------------------

INSERT INTO MAIN.MESSAGE_CATEGORY(MESSAGE_CATEGORY_CD, MESSAGE_CATEGORY_NAME) SELECT 'AUTH', 'Authorization' WHERE NOT EXISTS (SELECT 1 FROM MAIN.MESSAGE_CATEGORY WHERE MESSAGE_CATEGORY_CD = 'AUTH');
INSERT INTO MAIN.MESSAGE_CATEGORY(MESSAGE_CATEGORY_CD, MESSAGE_CATEGORY_NAME) SELECT 'AUTH_RESPONSE', 'Authorization Response' WHERE NOT EXISTS (SELECT 1 FROM MAIN.MESSAGE_CATEGORY WHERE MESSAGE_CATEGORY_CD = 'AUTH_RESPONSE');
INSERT INTO MAIN.MESSAGE_CATEGORY(MESSAGE_CATEGORY_CD, MESSAGE_CATEGORY_NAME) SELECT 'CASH', 'Cash' WHERE NOT EXISTS (SELECT 1 FROM MAIN.MESSAGE_CATEGORY WHERE MESSAGE_CATEGORY_CD = 'CASH');
INSERT INTO MAIN.MESSAGE_CATEGORY(MESSAGE_CATEGORY_CD, MESSAGE_CATEGORY_NAME) SELECT 'COMPONENT_INFO', 'Component Information' WHERE NOT EXISTS (SELECT 1 FROM MAIN.MESSAGE_CATEGORY WHERE MESSAGE_CATEGORY_CD = 'COMPONENT_INFO');
INSERT INTO MAIN.MESSAGE_CATEGORY(MESSAGE_CATEGORY_CD, MESSAGE_CATEGORY_NAME) SELECT 'DATA_LOG', 'Data Log' WHERE NOT EXISTS (SELECT 1 FROM MAIN.MESSAGE_CATEGORY WHERE MESSAGE_CATEGORY_CD = 'DATA_LOG');
INSERT INTO MAIN.MESSAGE_CATEGORY(MESSAGE_CATEGORY_CD, MESSAGE_CATEGORY_NAME) SELECT 'EVENT', 'Event' WHERE NOT EXISTS (SELECT 1 FROM MAIN.MESSAGE_CATEGORY WHERE MESSAGE_CATEGORY_CD = 'EVENT');
INSERT INTO MAIN.MESSAGE_CATEGORY(MESSAGE_CATEGORY_CD, MESSAGE_CATEGORY_NAME) SELECT 'FILE', 'File' WHERE NOT EXISTS (SELECT 1 FROM MAIN.MESSAGE_CATEGORY WHERE MESSAGE_CATEGORY_CD = 'FILE');
INSERT INTO MAIN.MESSAGE_CATEGORY(MESSAGE_CATEGORY_CD, MESSAGE_CATEGORY_NAME) SELECT 'FILE_BLOCK', 'File Block' WHERE NOT EXISTS (SELECT 1 FROM MAIN.MESSAGE_CATEGORY WHERE MESSAGE_CATEGORY_CD = 'FILE_BLOCK');
INSERT INTO MAIN.MESSAGE_CATEGORY(MESSAGE_CATEGORY_CD, MESSAGE_CATEGORY_NAME) SELECT 'FLOW', 'Flow Control' WHERE NOT EXISTS (SELECT 1 FROM MAIN.MESSAGE_CATEGORY WHERE MESSAGE_CATEGORY_CD = 'FLOW');
INSERT INTO MAIN.MESSAGE_CATEGORY(MESSAGE_CATEGORY_CD, MESSAGE_CATEGORY_NAME) SELECT 'INIT', 'Initialization' WHERE NOT EXISTS (SELECT 1 FROM MAIN.MESSAGE_CATEGORY WHERE MESSAGE_CATEGORY_CD = 'INIT');
INSERT INTO MAIN.MESSAGE_CATEGORY(MESSAGE_CATEGORY_CD, MESSAGE_CATEGORY_NAME) SELECT 'INVALID', 'Invalid' WHERE NOT EXISTS (SELECT 1 FROM MAIN.MESSAGE_CATEGORY WHERE MESSAGE_CATEGORY_CD = 'INVALID');
INSERT INTO MAIN.MESSAGE_CATEGORY(MESSAGE_CATEGORY_CD, MESSAGE_CATEGORY_NAME) SELECT 'LOCAL_AUTH', 'Local Auth' WHERE NOT EXISTS (SELECT 1 FROM MAIN.MESSAGE_CATEGORY WHERE MESSAGE_CATEGORY_CD = 'LOCAL_AUTH');
INSERT INTO MAIN.MESSAGE_CATEGORY(MESSAGE_CATEGORY_CD, MESSAGE_CATEGORY_NAME) SELECT 'MISC', 'Miscellaneous' WHERE NOT EXISTS (SELECT 1 FROM MAIN.MESSAGE_CATEGORY WHERE MESSAGE_CATEGORY_CD = 'MISC');
INSERT INTO MAIN.MESSAGE_CATEGORY(MESSAGE_CATEGORY_CD, MESSAGE_CATEGORY_NAME) SELECT 'SALE', 'Sale' WHERE NOT EXISTS (SELECT 1 FROM MAIN.MESSAGE_CATEGORY WHERE MESSAGE_CATEGORY_CD = 'SALE');
INSERT INTO MAIN.MESSAGE_CATEGORY(MESSAGE_CATEGORY_CD, MESSAGE_CATEGORY_NAME) SELECT 'SETTLEMENT', 'Settlement' WHERE NOT EXISTS (SELECT 1 FROM MAIN.MESSAGE_CATEGORY WHERE MESSAGE_CATEGORY_CD = 'SETTLEMENT');
INSERT INTO MAIN.MESSAGE_CATEGORY(MESSAGE_CATEGORY_CD, MESSAGE_CATEGORY_NAME) SELECT 'USR', 'Update Status Request' WHERE NOT EXISTS (SELECT 1 FROM MAIN.MESSAGE_CATEGORY WHERE MESSAGE_CATEGORY_CD = 'USR');
COMMIT;

-- ======================================================================

CREATE TABLE MAIN.COMM_METHOD
  (
    COMM_METHOD_CD    CHAR(1)         NOT NULL,
    COMM_METHOD_NAME  VARCHAR(60)     NOT NULL,
    IP_MATCH_ORDER    INTEGER         NOT NULL,
    IP_MATCH_REGEX    VARCHAR(4000)   NOT NULL,

    PRIMARY KEY(COMM_METHOD_CD)
  );


CREATE OR REPLACE FUNCTION MAIN.UPSERT_COMM_METHOD(
	p_comm_method_cd CHAR(1), 
	p_comm_method_name VARCHAR(60), 
	p_ip_match_order INTEGER, 
	p_ip_match_regex VARCHAR(4000),
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
BEGIN
	LOOP
		BEGIN
			UPDATE MAIN.COMM_METHOD
			   SET COMM_METHOD_NAME = p_comm_method_name,
			       IP_MATCH_ORDER = p_ip_match_order,
			       IP_MATCH_REGEX = p_ip_match_regex
			 WHERE COMM_METHOD_CD = p_comm_method_cd;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				INSERT INTO MAIN.COMM_METHOD(COMM_METHOD_CD, COMM_METHOD_NAME, IP_MATCH_ORDER, IP_MATCH_REGEX)
				 VALUES(p_comm_method_cd, p_comm_method_name, p_ip_match_order, p_ip_match_regex);
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			END IF;
			EXIT;
		EXCEPTION
			WHEN unique_violation THEN
				NULL;
		END;
	END LOOP;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MAIN.UPSERT_COMM_METHOD(
	p_comm_method_cd CHAR(1), 
	p_comm_method_name VARCHAR(60), 
	p_ip_match_order INTEGER, 
	p_ip_match_regex VARCHAR(4000),
	p_row_count OUT INTEGER) TO write_main;



-- ----------------------------------------------------------------------

INSERT INTO MAIN.COMM_METHOD(COMM_METHOD_CD, COMM_METHOD_NAME, IP_MATCH_ORDER, IP_MATCH_REGEX) SELECT 'C', 'CDMA', 20, '^10\.22[7-9]\.' WHERE NOT EXISTS (SELECT 1 FROM MAIN.COMM_METHOD WHERE COMM_METHOD_CD = 'C');
INSERT INTO MAIN.COMM_METHOD(COMM_METHOD_CD, COMM_METHOD_NAME, IP_MATCH_ORDER, IP_MATCH_REGEX) SELECT 'E', 'Ethernet', 40, '.*' WHERE NOT EXISTS (SELECT 1 FROM MAIN.COMM_METHOD WHERE COMM_METHOD_CD = 'E');
INSERT INTO MAIN.COMM_METHOD(COMM_METHOD_CD, COMM_METHOD_NAME, IP_MATCH_ORDER, IP_MATCH_REGEX) SELECT 'G', 'GPRS', 10, '^(172|166|32|74.198)\.' WHERE NOT EXISTS (SELECT 1 FROM MAIN.COMM_METHOD WHERE COMM_METHOD_CD = 'G');
INSERT INTO MAIN.COMM_METHOD(COMM_METHOD_CD, COMM_METHOD_NAME, IP_MATCH_ORDER, IP_MATCH_REGEX) SELECT 'P', 'POTS', 30, '^192\.168\.79\.162$' WHERE NOT EXISTS (SELECT 1 FROM MAIN.COMM_METHOD WHERE COMM_METHOD_CD = 'P');
COMMIT;

-- ======================================================================

CREATE TABLE MAIN._DEVICE_MESSAGE
  (
    GLOBAL_SESSION_CD         VARCHAR(100)               NOT NULL,
    MESSAGE_SEQUENCE          INTEGER                    NOT NULL,
    CALL_IN_START_TS          TIMESTAMP WITH TIME ZONE   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    INBOUND_MESSAGE_TS        TIMESTAMP WITH TIME ZONE,
    INBOUND_MESSAGE_TYPE      VARCHAR(10),
    INBOUND_MESSAGE           BYTEA,
    OUTBOUND_MESSAGE_TS       TIMESTAMP WITH TIME ZONE,
    OUTBOUND_MESSAGE_TYPE     VARCHAR(10),
    OUTBOUND_MESSAGE          BYTEA,
    MESSAGE_TRANSMITTED_FLAG  CHAR(1)                    NOT NULL,
    MESSAGE_DURATION_MILLIS   BIGINT                     NOT NULL,
    INBOUND_MESSAGE_LENGTH    INTEGER,
    OUTBOUND_MESSAGE_LENGTH   INTEGER,
    DEVICE_NAME               VARCHAR(60),
    NET_LAYER_ID              INTEGER                    NOT NULL,
    AUTH_ID                   BIGINT,
    FILE_TRANSFER_ID          BIGINT,
    EVENT_ID                  BIGINT,
    MESSAGE_STAGE_ID          INTEGER                    NOT NULL DEFAULT 0,
    DEVICE_SERIAL_CD          VARCHAR(60),

    PRIMARY KEY(GLOBAL_SESSION_CD,MESSAGE_SEQUENCE),

    FOREIGN KEY(NET_LAYER_ID) REFERENCES MAIN.NET_LAYER(NET_LAYER_ID),
    FOREIGN KEY(MESSAGE_STAGE_ID) REFERENCES MAIN.MESSAGE_STAGE(MESSAGE_STAGE_ID)
  )
  INHERITS(MAIN._);

CREATE OR REPLACE FUNCTION MAIN.FRIIUD_DEVICE_MESSAGE()
	RETURNS TRIGGER
	SECURITY DEFINER
AS $$
DECLARE
	lv_old_partition_table VARCHAR(4000);
	lv_new_partition_table VARCHAR(4000);
	lv_new_suffix VARCHAR(4000);
	lv_check_exp VARCHAR(4000);
	lv_sql TEXT;
	lv_filter TEXT;
	ln_cnt INTEGER;
BEGIN
	IF TG_OP = 'INSERT' THEN
			NEW.CREATED_UTC_TS := STATEMENT_TIMESTAMP() AT TIME ZONE 'UTC';
			NEW.CREATED_BY := CURRENT_USER;
			NEW.LAST_UPDATED_UTC_TS := STATEMENT_TIMESTAMP() AT TIME ZONE 'UTC';
			NEW.LAST_UPDATED_BY := CURRENT_USER;
	ELSIF TG_OP = 'UPDATE' THEN
			NEW.CREATED_UTC_TS := OLD.CREATED_UTC_TS;
			NEW.CREATED_BY := OLD.CREATED_BY;
			NEW.LAST_UPDATED_UTC_TS := STATEMENT_TIMESTAMP() AT TIME ZONE 'UTC';
			NEW.LAST_UPDATED_BY := CURRENT_USER;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		SELECT COALESCE(TO_CHAR(NEW.CALL_IN_START_TS, 'YYYY_MM_DD'),'') INTO lv_new_suffix;
		SELECT MAIN.CONSTRUCT_NAME('_DEVICE_MESSAGE', lv_new_suffix) INTO lv_new_partition_table;
	END IF;
	IF TG_OP IN('DELETE', 'UPDATE') THEN
		SELECT MAIN.CONSTRUCT_NAME('_DEVICE_MESSAGE', COALESCE(TO_CHAR(OLD.CALL_IN_START_TS, 'YYYY_MM_DD'), '')) INTO lv_old_partition_table;
	END IF;
	IF lv_new_partition_table IS NOT NULL AND lv_old_partition_table IS NOT NULL AND lv_new_partition_table = lv_old_partition_table THEN
		lv_sql := 'UPDATE MAIN.' || lv_new_partition_table || ' SET ';
		lv_filter := '';
		IF OLD.GLOBAL_SESSION_CD != NEW.GLOBAL_SESSION_CD THEN
			lv_sql := lv_sql || 'GLOBAL_SESSION_CD = $2.GLOBAL_SESSION_CD, ';
		END IF;
		IF OLD.MESSAGE_SEQUENCE != NEW.MESSAGE_SEQUENCE THEN
			lv_sql := lv_sql || 'MESSAGE_SEQUENCE = $2.MESSAGE_SEQUENCE, ';
		END IF;
		IF OLD.CALL_IN_START_TS != NEW.CALL_IN_START_TS THEN
			lv_sql := lv_sql || 'CALL_IN_START_TS = $2.CALL_IN_START_TS, ';
			lv_filter := lv_filter || ' AND (CALL_IN_START_TS = $1.CALL_IN_START_TS OR CALL_IN_START_TS = $2.CALL_IN_START_TS)';
		END IF;
		IF OLD.INBOUND_MESSAGE_TS IS DISTINCT FROM NEW.INBOUND_MESSAGE_TS THEN
			lv_sql := lv_sql || 'INBOUND_MESSAGE_TS = $2.INBOUND_MESSAGE_TS, ';
			lv_filter := lv_filter || ' AND (INBOUND_MESSAGE_TS IS NOT DISTINCT FROM $1.INBOUND_MESSAGE_TS OR INBOUND_MESSAGE_TS IS NOT DISTINCT FROM $2.INBOUND_MESSAGE_TS)';
		END IF;
		IF OLD.INBOUND_MESSAGE_TYPE IS DISTINCT FROM NEW.INBOUND_MESSAGE_TYPE THEN
			lv_sql := lv_sql || 'INBOUND_MESSAGE_TYPE = $2.INBOUND_MESSAGE_TYPE, ';
			lv_filter := lv_filter || ' AND (INBOUND_MESSAGE_TYPE IS NOT DISTINCT FROM $1.INBOUND_MESSAGE_TYPE OR INBOUND_MESSAGE_TYPE IS NOT DISTINCT FROM $2.INBOUND_MESSAGE_TYPE)';
		END IF;
		IF OLD.INBOUND_MESSAGE IS DISTINCT FROM NEW.INBOUND_MESSAGE THEN
			lv_sql := lv_sql || 'INBOUND_MESSAGE = $2.INBOUND_MESSAGE, ';
			lv_filter := lv_filter || ' AND (INBOUND_MESSAGE IS NOT DISTINCT FROM $1.INBOUND_MESSAGE OR INBOUND_MESSAGE IS NOT DISTINCT FROM $2.INBOUND_MESSAGE)';
		END IF;
		IF OLD.OUTBOUND_MESSAGE_TS IS DISTINCT FROM NEW.OUTBOUND_MESSAGE_TS THEN
			lv_sql := lv_sql || 'OUTBOUND_MESSAGE_TS = $2.OUTBOUND_MESSAGE_TS, ';
			lv_filter := lv_filter || ' AND (OUTBOUND_MESSAGE_TS IS NOT DISTINCT FROM $1.OUTBOUND_MESSAGE_TS OR OUTBOUND_MESSAGE_TS IS NOT DISTINCT FROM $2.OUTBOUND_MESSAGE_TS)';
		END IF;
		IF OLD.OUTBOUND_MESSAGE_TYPE IS DISTINCT FROM NEW.OUTBOUND_MESSAGE_TYPE THEN
			lv_sql := lv_sql || 'OUTBOUND_MESSAGE_TYPE = $2.OUTBOUND_MESSAGE_TYPE, ';
			lv_filter := lv_filter || ' AND (OUTBOUND_MESSAGE_TYPE IS NOT DISTINCT FROM $1.OUTBOUND_MESSAGE_TYPE OR OUTBOUND_MESSAGE_TYPE IS NOT DISTINCT FROM $2.OUTBOUND_MESSAGE_TYPE)';
		END IF;
		IF OLD.OUTBOUND_MESSAGE IS DISTINCT FROM NEW.OUTBOUND_MESSAGE THEN
			lv_sql := lv_sql || 'OUTBOUND_MESSAGE = $2.OUTBOUND_MESSAGE, ';
			lv_filter := lv_filter || ' AND (OUTBOUND_MESSAGE IS NOT DISTINCT FROM $1.OUTBOUND_MESSAGE OR OUTBOUND_MESSAGE IS NOT DISTINCT FROM $2.OUTBOUND_MESSAGE)';
		END IF;
		IF OLD.MESSAGE_TRANSMITTED_FLAG != NEW.MESSAGE_TRANSMITTED_FLAG THEN
			lv_sql := lv_sql || 'MESSAGE_TRANSMITTED_FLAG = $2.MESSAGE_TRANSMITTED_FLAG, ';
			lv_filter := lv_filter || ' AND (MESSAGE_TRANSMITTED_FLAG = $1.MESSAGE_TRANSMITTED_FLAG OR MESSAGE_TRANSMITTED_FLAG = $2.MESSAGE_TRANSMITTED_FLAG)';
		END IF;
		IF OLD.MESSAGE_DURATION_MILLIS != NEW.MESSAGE_DURATION_MILLIS THEN
			lv_sql := lv_sql || 'MESSAGE_DURATION_MILLIS = $2.MESSAGE_DURATION_MILLIS, ';
			lv_filter := lv_filter || ' AND (MESSAGE_DURATION_MILLIS = $1.MESSAGE_DURATION_MILLIS OR MESSAGE_DURATION_MILLIS = $2.MESSAGE_DURATION_MILLIS)';
		END IF;
		IF OLD.INBOUND_MESSAGE_LENGTH IS DISTINCT FROM NEW.INBOUND_MESSAGE_LENGTH THEN
			lv_sql := lv_sql || 'INBOUND_MESSAGE_LENGTH = $2.INBOUND_MESSAGE_LENGTH, ';
			lv_filter := lv_filter || ' AND (INBOUND_MESSAGE_LENGTH IS NOT DISTINCT FROM $1.INBOUND_MESSAGE_LENGTH OR INBOUND_MESSAGE_LENGTH IS NOT DISTINCT FROM $2.INBOUND_MESSAGE_LENGTH)';
		END IF;
		IF OLD.OUTBOUND_MESSAGE_LENGTH IS DISTINCT FROM NEW.OUTBOUND_MESSAGE_LENGTH THEN
			lv_sql := lv_sql || 'OUTBOUND_MESSAGE_LENGTH = $2.OUTBOUND_MESSAGE_LENGTH, ';
			lv_filter := lv_filter || ' AND (OUTBOUND_MESSAGE_LENGTH IS NOT DISTINCT FROM $1.OUTBOUND_MESSAGE_LENGTH OR OUTBOUND_MESSAGE_LENGTH IS NOT DISTINCT FROM $2.OUTBOUND_MESSAGE_LENGTH)';
		END IF;
		IF OLD.DEVICE_NAME IS DISTINCT FROM NEW.DEVICE_NAME THEN
			lv_sql := lv_sql || 'DEVICE_NAME = $2.DEVICE_NAME, ';
			lv_filter := lv_filter || ' AND (DEVICE_NAME IS NOT DISTINCT FROM $1.DEVICE_NAME OR DEVICE_NAME IS NOT DISTINCT FROM $2.DEVICE_NAME)';
		END IF;
		IF OLD.NET_LAYER_ID != NEW.NET_LAYER_ID THEN
			lv_sql := lv_sql || 'NET_LAYER_ID = $2.NET_LAYER_ID, ';
			lv_filter := lv_filter || ' AND (NET_LAYER_ID = $1.NET_LAYER_ID OR NET_LAYER_ID = $2.NET_LAYER_ID)';
		END IF;
		IF OLD.AUTH_ID IS DISTINCT FROM NEW.AUTH_ID THEN
			lv_sql := lv_sql || 'AUTH_ID = $2.AUTH_ID, ';
			lv_filter := lv_filter || ' AND (AUTH_ID IS NOT DISTINCT FROM $1.AUTH_ID OR AUTH_ID IS NOT DISTINCT FROM $2.AUTH_ID)';
		END IF;
		IF OLD.FILE_TRANSFER_ID IS DISTINCT FROM NEW.FILE_TRANSFER_ID THEN
			lv_sql := lv_sql || 'FILE_TRANSFER_ID = $2.FILE_TRANSFER_ID, ';
			lv_filter := lv_filter || ' AND (FILE_TRANSFER_ID IS NOT DISTINCT FROM $1.FILE_TRANSFER_ID OR FILE_TRANSFER_ID IS NOT DISTINCT FROM $2.FILE_TRANSFER_ID)';
		END IF;
		IF OLD.EVENT_ID IS DISTINCT FROM NEW.EVENT_ID THEN
			lv_sql := lv_sql || 'EVENT_ID = $2.EVENT_ID, ';
			lv_filter := lv_filter || ' AND (EVENT_ID IS NOT DISTINCT FROM $1.EVENT_ID OR EVENT_ID IS NOT DISTINCT FROM $2.EVENT_ID)';
		END IF;
		IF OLD.MESSAGE_STAGE_ID != NEW.MESSAGE_STAGE_ID THEN
			lv_sql := lv_sql || 'MESSAGE_STAGE_ID = $2.MESSAGE_STAGE_ID, ';
			lv_filter := lv_filter || ' AND (MESSAGE_STAGE_ID = $1.MESSAGE_STAGE_ID OR MESSAGE_STAGE_ID = $2.MESSAGE_STAGE_ID)';
		END IF;
		IF OLD.DEVICE_SERIAL_CD IS DISTINCT FROM NEW.DEVICE_SERIAL_CD THEN
			lv_sql := lv_sql || 'DEVICE_SERIAL_CD = $2.DEVICE_SERIAL_CD, ';
			lv_filter := lv_filter || ' AND (DEVICE_SERIAL_CD IS NOT DISTINCT FROM $1.DEVICE_SERIAL_CD OR DEVICE_SERIAL_CD IS NOT DISTINCT FROM $2.DEVICE_SERIAL_CD)';
		END IF;
		lv_sql := lv_sql || 'LAST_UPDATED_UTC_TS = $2.LAST_UPDATED_UTC_TS, LAST_UPDATED_BY = $2.LAST_UPDATED_BY, ';
		lv_sql := LEFT(lv_sql, -2) || ' WHERE GLOBAL_SESSION_CD = $1.GLOBAL_SESSION_CD AND MESSAGE_SEQUENCE = $1.MESSAGE_SEQUENCE' || lv_filter;
		EXECUTE lv_sql USING OLD, NEW;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
		RETURN NEW;
	END IF;
	IF lv_new_partition_table IS NOT NULL THEN
		LOOP
			BEGIN
				EXECUTE 'INSERT INTO MAIN.' || lv_new_partition_table || ' SELECT $1.*' USING NEW;
				EXIT;
			EXCEPTION
			WHEN undefined_table THEN
				SELECT 'CONSTRAINT CK_' || lv_new_partition_table || ' CHECK(' ||'CALL_IN_START_TS >= ''' || TO_CHAR(NEW.CALL_IN_START_TS, 'YYYY-MM-DD') || '''::TIMESTAMP WITH TIME ZONE AND CALL_IN_START_TS < ''' || TO_CHAR(NEW.CALL_IN_START_TS + '1 DAY'::INTERVAL, 'YYYY-MM-DD') || '''::TIMESTAMP WITH TIME ZONE'||'), ' INTO lv_check_exp;
				RAISE NOTICE 'Creating new partition table %', lv_new_partition_table;
				BEGIN
					EXECUTE 'CREATE TABLE MAIN.' || lv_new_partition_table || '(' || lv_check_exp ||'CONSTRAINT PK_' || lv_new_partition_table || ' PRIMARY KEY(GLOBAL_SESSION_CD, MESSAGE_SEQUENCE)) INHERITS(MAIN._DEVICE_MESSAGE)';
				EXCEPTION
					WHEN duplicate_table THEN
						CONTINUE;
				END;
				EXECUTE 'GRANT ALL ON TABLE MAIN.' || lv_new_partition_table || ' TO ' || CURRENT_USER;
				PERFORM MAIN.COPY_INDEXES('_DEVICE_MESSAGE', lv_new_suffix);
				EXECUTE 'ALTER TABLE MAIN.' || lv_new_partition_table || ' ADD FOREIGN KEY(NET_LAYER_ID) REFERENCES MAIN.NET_LAYER(NET_LAYER_ID)';
				EXECUTE 'ALTER TABLE MAIN.' || lv_new_partition_table || ' ADD FOREIGN KEY(MESSAGE_STAGE_ID) REFERENCES MAIN.MESSAGE_STAGE(MESSAGE_STAGE_ID)';
			END;
		END LOOP;
	END IF;
	IF lv_old_partition_table IS NOT NULL THEN
		EXECUTE 'DELETE FROM MAIN.' || lv_old_partition_table || ' WHERE GLOBAL_SESSION_CD = $1.GLOBAL_SESSION_CD AND MESSAGE_SEQUENCE = $1.MESSAGE_SEQUENCE' USING OLD;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		RETURN NEW;
	ELSE
		RETURN OLD;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE VIEW MAIN.DEVICE_MESSAGE
	AS SELECT * FROM MAIN._DEVICE_MESSAGE;

ALTER VIEW MAIN.DEVICE_MESSAGE ALTER CALL_IN_START_TS SET DEFAULT CURRENT_TIMESTAMP;
ALTER VIEW MAIN.DEVICE_MESSAGE ALTER MESSAGE_STAGE_ID SET DEFAULT 0;
ALTER VIEW MAIN.DEVICE_MESSAGE ALTER CREATED_BY SET DEFAULT CURRENT_USER;
ALTER VIEW MAIN.DEVICE_MESSAGE ALTER LAST_UPDATED_BY SET DEFAULT CURRENT_USER;

GRANT SELECT ON MAIN.DEVICE_MESSAGE TO read_main;
GRANT INSERT, UPDATE, DELETE ON MAIN.DEVICE_MESSAGE TO write_main;
REVOKE INSERT, UPDATE, DELETE, TRUNCATE ON MAIN._DEVICE_MESSAGE FROM postgres;

DROP TRIGGER IF EXISTS TRIIUD_DEVICE_MESSAGE ON MAIN.DEVICE_MESSAGE;
CREATE TRIGGER TRIIUD_DEVICE_MESSAGE
INSTEAD OF INSERT OR UPDATE OR DELETE ON MAIN.DEVICE_MESSAGE
	FOR EACH ROW
	EXECUTE PROCEDURE MAIN.FRIIUD_DEVICE_MESSAGE();


CREATE OR REPLACE FUNCTION MAIN.UPSERT_DEVICE_MESSAGE(
	p_global_session_cd VARCHAR(100), 
	p_message_sequence INTEGER, 
	p_call_in_start_ts TIMESTAMP WITH TIME ZONE, 
	p_inbound_message_ts TIMESTAMP WITH TIME ZONE, 
	p_inbound_message_type VARCHAR(10), 
	p_inbound_message BYTEA, 
	p_outbound_message_ts TIMESTAMP WITH TIME ZONE, 
	p_outbound_message_type VARCHAR(10), 
	p_outbound_message BYTEA, 
	p_message_transmitted_flag CHAR(1), 
	p_message_duration_millis BIGINT, 
	p_inbound_message_length INTEGER, 
	p_outbound_message_length INTEGER, 
	p_device_name VARCHAR(60), 
	p_net_layer_id INTEGER, 
	p_auth_id BIGINT, 
	p_file_transfer_id BIGINT, 
	p_event_id BIGINT, 
	p_message_stage_id INTEGER, 
	p_device_serial_cd VARCHAR(60),
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
BEGIN
	LOOP
		BEGIN
			UPDATE MAIN.DEVICE_MESSAGE
			   SET CALL_IN_START_TS = COALESCE(p_call_in_start_ts, CALL_IN_START_TS),
			       INBOUND_MESSAGE_TS = p_inbound_message_ts,
			       INBOUND_MESSAGE_TYPE = p_inbound_message_type,
			       INBOUND_MESSAGE = p_inbound_message,
			       OUTBOUND_MESSAGE_TS = p_outbound_message_ts,
			       OUTBOUND_MESSAGE_TYPE = p_outbound_message_type,
			       OUTBOUND_MESSAGE = p_outbound_message,
			       MESSAGE_TRANSMITTED_FLAG = p_message_transmitted_flag,
			       MESSAGE_DURATION_MILLIS = p_message_duration_millis,
			       INBOUND_MESSAGE_LENGTH = p_inbound_message_length,
			       OUTBOUND_MESSAGE_LENGTH = p_outbound_message_length,
			       DEVICE_NAME = p_device_name,
			       NET_LAYER_ID = p_net_layer_id,
			       AUTH_ID = p_auth_id,
			       FILE_TRANSFER_ID = p_file_transfer_id,
			       EVENT_ID = p_event_id,
			       MESSAGE_STAGE_ID = COALESCE(p_message_stage_id, MESSAGE_STAGE_ID),
			       DEVICE_SERIAL_CD = p_device_serial_cd
			 WHERE GLOBAL_SESSION_CD = p_global_session_cd
			   AND MESSAGE_SEQUENCE = p_message_sequence;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				INSERT INTO MAIN.DEVICE_MESSAGE(GLOBAL_SESSION_CD, MESSAGE_SEQUENCE, CALL_IN_START_TS, INBOUND_MESSAGE_TS, INBOUND_MESSAGE_TYPE, INBOUND_MESSAGE, OUTBOUND_MESSAGE_TS, OUTBOUND_MESSAGE_TYPE, OUTBOUND_MESSAGE, MESSAGE_TRANSMITTED_FLAG, MESSAGE_DURATION_MILLIS, INBOUND_MESSAGE_LENGTH, OUTBOUND_MESSAGE_LENGTH, DEVICE_NAME, NET_LAYER_ID, AUTH_ID, FILE_TRANSFER_ID, EVENT_ID, MESSAGE_STAGE_ID, DEVICE_SERIAL_CD)
				 VALUES(p_global_session_cd, p_message_sequence, COALESCE(p_call_in_start_ts, CURRENT_TIMESTAMP), p_inbound_message_ts, p_inbound_message_type, p_inbound_message, p_outbound_message_ts, p_outbound_message_type, p_outbound_message, p_message_transmitted_flag, p_message_duration_millis, p_inbound_message_length, p_outbound_message_length, p_device_name, p_net_layer_id, p_auth_id, p_file_transfer_id, p_event_id, COALESCE(p_message_stage_id, 0), p_device_serial_cd);
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			END IF;
			EXIT;
		EXCEPTION
			WHEN unique_violation THEN
				NULL;
			WHEN serialization_failure THEN
				NULL;
		END;
	END LOOP;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MAIN.UPSERT_DEVICE_MESSAGE(
	p_global_session_cd VARCHAR(100), 
	p_message_sequence INTEGER, 
	p_call_in_start_ts TIMESTAMP WITH TIME ZONE, 
	p_inbound_message_ts TIMESTAMP WITH TIME ZONE, 
	p_inbound_message_type VARCHAR(10), 
	p_inbound_message BYTEA, 
	p_outbound_message_ts TIMESTAMP WITH TIME ZONE, 
	p_outbound_message_type VARCHAR(10), 
	p_outbound_message BYTEA, 
	p_message_transmitted_flag CHAR(1), 
	p_message_duration_millis BIGINT, 
	p_inbound_message_length INTEGER, 
	p_outbound_message_length INTEGER, 
	p_device_name VARCHAR(60), 
	p_net_layer_id INTEGER, 
	p_auth_id BIGINT, 
	p_file_transfer_id BIGINT, 
	p_event_id BIGINT, 
	p_message_stage_id INTEGER, 
	p_device_serial_cd VARCHAR(60),
	p_row_count OUT INTEGER) TO write_main;



CREATE INDEX IX_DEVICE_MESSAGE_INBOUND_MESSAGE_TS_INBOUND_MESSAGE_TYPE_DEVICE_NAME ON MAIN._DEVICE_MESSAGE(INBOUND_MESSAGE_TS,INBOUND_MESSAGE_TYPE,DEVICE_NAME);

-- ======================================================================

CREATE TABLE MAIN._DEVICE_SESSION
  (
    GLOBAL_SESSION_CD        VARCHAR(100)               NOT NULL,
    CALL_IN_START_TS         TIMESTAMP WITH TIME ZONE   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    DEVICE_NAME              VARCHAR(60)                NOT NULL,
    NET_LAYER_ID             INTEGER,
    DEVICE_SERIAL_CD         VARCHAR(60),
    DEVICE_TYPE_ID           INTEGER,
    SESSION_STATE_ID         SMALLINT                   NOT NULL DEFAULT 1,
    CALL_IN_FINISH_TS        TIMESTAMP WITH TIME ZONE,
    CALL_IN_STATUS           CHAR(1)                    NOT NULL DEFAULT 'I',
    CLIENT_IP_ADDRESS        VARCHAR(16),
    CLIENT_PORT              INTEGER,
    INBOUND_MESSAGE_COUNT    INTEGER,
    OUTBOUND_MESSAGE_COUNT   INTEGER,
    INBOUND_BYTE_COUNT       INTEGER,
    OUTBOUND_BYTE_COUNT      INTEGER,
    DEVICE_INITIALIZED_FLAG  CHAR(1),
    DEVICE_SENT_CONFIG_FLAG  CHAR(1),
    SERVER_SENT_CONFIG_FLAG  CHAR(1),
    CALL_IN_TYPE             CHAR(1)                    DEFAULT 'U',
    AUTH_AMOUNT              DECIMAL(14,2),
    AUTH_APPROVED_FLAG       CHAR(1),
    AUTH_CARD_TYPE           CHAR(1),
    DEX_FILE_SIZE            BIGINT,
    DEX_RECEIVED_FLAG        CHAR(1),
    TERMINAL_ID              BIGINT,
    CUSTOMER_ID              BIGINT,
    LOCATION_NAME            VARCHAR(200),
    CUSTOMER_NAME            VARCHAR(200),
    CREDIT_TRANS_COUNT       INTEGER,
    CREDIT_TRANS_TOTAL       DECIMAL(14,2),
    CREDIT_VEND_COUNT        INTEGER,
    CASH_TRANS_COUNT         INTEGER,
    CASH_TRANS_TOTAL         DECIMAL(14,2),
    CASH_VEND_COUNT          INTEGER,
    PASSCARD_TRANS_COUNT     INTEGER,
    PASSCARD_TRANS_TOTAL     DECIMAL(14,2),
    PASSCARD_VEND_COUNT      INTEGER,
    LAST_AUTH_IN_TS          TIMESTAMP WITH TIME ZONE,
    LAST_TRANS_IN_TS         TIMESTAMP WITH TIME ZONE,
    FIRMWARE_VERSION         VARCHAR(200),
    COMM_METHOD_CD           CHAR(1),
    COMMENTS                 VARCHAR(4000),
	DEX_STATUS               VARCHAR(128),
    PRIMARY KEY(GLOBAL_SESSION_CD),
    FOREIGN KEY(NET_LAYER_ID) REFERENCES MAIN.NET_LAYER(NET_LAYER_ID),
    FOREIGN KEY(DEVICE_TYPE_ID) REFERENCES MAIN.DEVICE_TYPE(DEVICE_TYPE_ID),
    FOREIGN KEY(SESSION_STATE_ID) REFERENCES MAIN.SESSION_STATE(SESSION_STATE_ID),
    FOREIGN KEY(COMM_METHOD_CD) REFERENCES MAIN.COMM_METHOD(COMM_METHOD_CD)
  )
  INHERITS(MAIN._);

CREATE OR REPLACE FUNCTION MAIN.FRIIUD_DEVICE_SESSION()
	RETURNS TRIGGER
	SECURITY DEFINER
AS $$
DECLARE
	lv_old_partition_table VARCHAR(4000);
	lv_new_partition_table VARCHAR(4000);
	lv_new_suffix VARCHAR(4000);
	lv_check_exp VARCHAR(4000);
	lv_sql TEXT;
	lv_filter TEXT;
	ln_cnt INTEGER;
BEGIN
	IF TG_OP = 'INSERT' THEN
			NEW.CREATED_UTC_TS := STATEMENT_TIMESTAMP() AT TIME ZONE 'UTC';
			NEW.CREATED_BY := CURRENT_USER;
			NEW.LAST_UPDATED_UTC_TS := STATEMENT_TIMESTAMP() AT TIME ZONE 'UTC';
			NEW.LAST_UPDATED_BY := CURRENT_USER;
	ELSIF TG_OP = 'UPDATE' THEN
			NEW.CREATED_UTC_TS := OLD.CREATED_UTC_TS;
			NEW.CREATED_BY := OLD.CREATED_BY;
			NEW.LAST_UPDATED_UTC_TS := STATEMENT_TIMESTAMP() AT TIME ZONE 'UTC';
			NEW.LAST_UPDATED_BY := CURRENT_USER;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		SELECT COALESCE(TO_CHAR(NEW.CALL_IN_START_TS, 'YYYY_MM_DD'),'') INTO lv_new_suffix;
		SELECT MAIN.CONSTRUCT_NAME('_DEVICE_SESSION', lv_new_suffix) INTO lv_new_partition_table;
	END IF;
	IF TG_OP IN('DELETE', 'UPDATE') THEN
		SELECT MAIN.CONSTRUCT_NAME('_DEVICE_SESSION', COALESCE(TO_CHAR(OLD.CALL_IN_START_TS, 'YYYY_MM_DD'), '')) INTO lv_old_partition_table;
	END IF;
	IF lv_new_partition_table IS NOT NULL AND lv_old_partition_table IS NOT NULL AND lv_new_partition_table = lv_old_partition_table THEN
		lv_sql := 'UPDATE MAIN.' || lv_new_partition_table || ' SET ';
		lv_filter := '';
		IF OLD.GLOBAL_SESSION_CD != NEW.GLOBAL_SESSION_CD THEN
			lv_sql := lv_sql || 'GLOBAL_SESSION_CD = $2.GLOBAL_SESSION_CD, ';
		END IF;
		IF OLD.CALL_IN_START_TS != NEW.CALL_IN_START_TS THEN
			lv_sql := lv_sql || 'CALL_IN_START_TS = $2.CALL_IN_START_TS, ';
			lv_filter := lv_filter || ' AND (CALL_IN_START_TS = $1.CALL_IN_START_TS OR CALL_IN_START_TS = $2.CALL_IN_START_TS)';
		END IF;
		IF OLD.DEVICE_NAME != NEW.DEVICE_NAME THEN
			lv_sql := lv_sql || 'DEVICE_NAME = $2.DEVICE_NAME, ';
			lv_filter := lv_filter || ' AND (DEVICE_NAME = $1.DEVICE_NAME OR DEVICE_NAME = $2.DEVICE_NAME)';
		END IF;
		IF OLD.NET_LAYER_ID IS DISTINCT FROM NEW.NET_LAYER_ID THEN
			lv_sql := lv_sql || 'NET_LAYER_ID = $2.NET_LAYER_ID, ';
			lv_filter := lv_filter || ' AND (NET_LAYER_ID IS NOT DISTINCT FROM $1.NET_LAYER_ID OR NET_LAYER_ID IS NOT DISTINCT FROM $2.NET_LAYER_ID)';
		END IF;
		IF OLD.DEVICE_SERIAL_CD IS DISTINCT FROM NEW.DEVICE_SERIAL_CD THEN
			lv_sql := lv_sql || 'DEVICE_SERIAL_CD = $2.DEVICE_SERIAL_CD, ';
			lv_filter := lv_filter || ' AND (DEVICE_SERIAL_CD IS NOT DISTINCT FROM $1.DEVICE_SERIAL_CD OR DEVICE_SERIAL_CD IS NOT DISTINCT FROM $2.DEVICE_SERIAL_CD)';
		END IF;
		IF OLD.DEVICE_TYPE_ID IS DISTINCT FROM NEW.DEVICE_TYPE_ID THEN
			lv_sql := lv_sql || 'DEVICE_TYPE_ID = $2.DEVICE_TYPE_ID, ';
			lv_filter := lv_filter || ' AND (DEVICE_TYPE_ID IS NOT DISTINCT FROM $1.DEVICE_TYPE_ID OR DEVICE_TYPE_ID IS NOT DISTINCT FROM $2.DEVICE_TYPE_ID)';
		END IF;
		IF OLD.SESSION_STATE_ID != NEW.SESSION_STATE_ID THEN
			lv_sql := lv_sql || 'SESSION_STATE_ID = $2.SESSION_STATE_ID, ';
			lv_filter := lv_filter || ' AND (SESSION_STATE_ID = $1.SESSION_STATE_ID OR SESSION_STATE_ID = $2.SESSION_STATE_ID)';
		END IF;
		IF OLD.CALL_IN_FINISH_TS IS DISTINCT FROM NEW.CALL_IN_FINISH_TS THEN
			lv_sql := lv_sql || 'CALL_IN_FINISH_TS = $2.CALL_IN_FINISH_TS, ';
			lv_filter := lv_filter || ' AND (CALL_IN_FINISH_TS IS NOT DISTINCT FROM $1.CALL_IN_FINISH_TS OR CALL_IN_FINISH_TS IS NOT DISTINCT FROM $2.CALL_IN_FINISH_TS)';
		END IF;
		IF OLD.CALL_IN_STATUS != NEW.CALL_IN_STATUS THEN
			lv_sql := lv_sql || 'CALL_IN_STATUS = $2.CALL_IN_STATUS, ';
			lv_filter := lv_filter || ' AND (CALL_IN_STATUS = $1.CALL_IN_STATUS OR CALL_IN_STATUS = $2.CALL_IN_STATUS)';
		END IF;
		IF OLD.CLIENT_IP_ADDRESS IS DISTINCT FROM NEW.CLIENT_IP_ADDRESS THEN
			lv_sql := lv_sql || 'CLIENT_IP_ADDRESS = $2.CLIENT_IP_ADDRESS, ';
			lv_filter := lv_filter || ' AND (CLIENT_IP_ADDRESS IS NOT DISTINCT FROM $1.CLIENT_IP_ADDRESS OR CLIENT_IP_ADDRESS IS NOT DISTINCT FROM $2.CLIENT_IP_ADDRESS)';
		END IF;
		IF OLD.CLIENT_PORT IS DISTINCT FROM NEW.CLIENT_PORT THEN
			lv_sql := lv_sql || 'CLIENT_PORT = $2.CLIENT_PORT, ';
			lv_filter := lv_filter || ' AND (CLIENT_PORT IS NOT DISTINCT FROM $1.CLIENT_PORT OR CLIENT_PORT IS NOT DISTINCT FROM $2.CLIENT_PORT)';
		END IF;
		IF OLD.INBOUND_MESSAGE_COUNT IS DISTINCT FROM NEW.INBOUND_MESSAGE_COUNT THEN
			lv_sql := lv_sql || 'INBOUND_MESSAGE_COUNT = $2.INBOUND_MESSAGE_COUNT, ';
			lv_filter := lv_filter || ' AND (INBOUND_MESSAGE_COUNT IS NOT DISTINCT FROM $1.INBOUND_MESSAGE_COUNT OR INBOUND_MESSAGE_COUNT IS NOT DISTINCT FROM $2.INBOUND_MESSAGE_COUNT)';
		END IF;
		IF OLD.OUTBOUND_MESSAGE_COUNT IS DISTINCT FROM NEW.OUTBOUND_MESSAGE_COUNT THEN
			lv_sql := lv_sql || 'OUTBOUND_MESSAGE_COUNT = $2.OUTBOUND_MESSAGE_COUNT, ';
			lv_filter := lv_filter || ' AND (OUTBOUND_MESSAGE_COUNT IS NOT DISTINCT FROM $1.OUTBOUND_MESSAGE_COUNT OR OUTBOUND_MESSAGE_COUNT IS NOT DISTINCT FROM $2.OUTBOUND_MESSAGE_COUNT)';
		END IF;
		IF OLD.INBOUND_BYTE_COUNT IS DISTINCT FROM NEW.INBOUND_BYTE_COUNT THEN
			lv_sql := lv_sql || 'INBOUND_BYTE_COUNT = $2.INBOUND_BYTE_COUNT, ';
			lv_filter := lv_filter || ' AND (INBOUND_BYTE_COUNT IS NOT DISTINCT FROM $1.INBOUND_BYTE_COUNT OR INBOUND_BYTE_COUNT IS NOT DISTINCT FROM $2.INBOUND_BYTE_COUNT)';
		END IF;
		IF OLD.OUTBOUND_BYTE_COUNT IS DISTINCT FROM NEW.OUTBOUND_BYTE_COUNT THEN
			lv_sql := lv_sql || 'OUTBOUND_BYTE_COUNT = $2.OUTBOUND_BYTE_COUNT, ';
			lv_filter := lv_filter || ' AND (OUTBOUND_BYTE_COUNT IS NOT DISTINCT FROM $1.OUTBOUND_BYTE_COUNT OR OUTBOUND_BYTE_COUNT IS NOT DISTINCT FROM $2.OUTBOUND_BYTE_COUNT)';
		END IF;
		IF OLD.DEVICE_INITIALIZED_FLAG IS DISTINCT FROM NEW.DEVICE_INITIALIZED_FLAG THEN
			lv_sql := lv_sql || 'DEVICE_INITIALIZED_FLAG = $2.DEVICE_INITIALIZED_FLAG, ';
			lv_filter := lv_filter || ' AND (DEVICE_INITIALIZED_FLAG IS NOT DISTINCT FROM $1.DEVICE_INITIALIZED_FLAG OR DEVICE_INITIALIZED_FLAG IS NOT DISTINCT FROM $2.DEVICE_INITIALIZED_FLAG)';
		END IF;
		IF OLD.DEVICE_SENT_CONFIG_FLAG IS DISTINCT FROM NEW.DEVICE_SENT_CONFIG_FLAG THEN
			lv_sql := lv_sql || 'DEVICE_SENT_CONFIG_FLAG = $2.DEVICE_SENT_CONFIG_FLAG, ';
			lv_filter := lv_filter || ' AND (DEVICE_SENT_CONFIG_FLAG IS NOT DISTINCT FROM $1.DEVICE_SENT_CONFIG_FLAG OR DEVICE_SENT_CONFIG_FLAG IS NOT DISTINCT FROM $2.DEVICE_SENT_CONFIG_FLAG)';
		END IF;
		IF OLD.SERVER_SENT_CONFIG_FLAG IS DISTINCT FROM NEW.SERVER_SENT_CONFIG_FLAG THEN
			lv_sql := lv_sql || 'SERVER_SENT_CONFIG_FLAG = $2.SERVER_SENT_CONFIG_FLAG, ';
			lv_filter := lv_filter || ' AND (SERVER_SENT_CONFIG_FLAG IS NOT DISTINCT FROM $1.SERVER_SENT_CONFIG_FLAG OR SERVER_SENT_CONFIG_FLAG IS NOT DISTINCT FROM $2.SERVER_SENT_CONFIG_FLAG)';
		END IF;
		IF OLD.CALL_IN_TYPE IS DISTINCT FROM NEW.CALL_IN_TYPE THEN
			lv_sql := lv_sql || 'CALL_IN_TYPE = $2.CALL_IN_TYPE, ';
			lv_filter := lv_filter || ' AND (CALL_IN_TYPE IS NOT DISTINCT FROM $1.CALL_IN_TYPE OR CALL_IN_TYPE IS NOT DISTINCT FROM $2.CALL_IN_TYPE)';
		END IF;
		IF OLD.AUTH_AMOUNT IS DISTINCT FROM NEW.AUTH_AMOUNT THEN
			lv_sql := lv_sql || 'AUTH_AMOUNT = $2.AUTH_AMOUNT, ';
			lv_filter := lv_filter || ' AND (AUTH_AMOUNT IS NOT DISTINCT FROM $1.AUTH_AMOUNT OR AUTH_AMOUNT IS NOT DISTINCT FROM $2.AUTH_AMOUNT)';
		END IF;
		IF OLD.AUTH_APPROVED_FLAG IS DISTINCT FROM NEW.AUTH_APPROVED_FLAG THEN
			lv_sql := lv_sql || 'AUTH_APPROVED_FLAG = $2.AUTH_APPROVED_FLAG, ';
			lv_filter := lv_filter || ' AND (AUTH_APPROVED_FLAG IS NOT DISTINCT FROM $1.AUTH_APPROVED_FLAG OR AUTH_APPROVED_FLAG IS NOT DISTINCT FROM $2.AUTH_APPROVED_FLAG)';
		END IF;
		IF OLD.AUTH_CARD_TYPE IS DISTINCT FROM NEW.AUTH_CARD_TYPE THEN
			lv_sql := lv_sql || 'AUTH_CARD_TYPE = $2.AUTH_CARD_TYPE, ';
			lv_filter := lv_filter || ' AND (AUTH_CARD_TYPE IS NOT DISTINCT FROM $1.AUTH_CARD_TYPE OR AUTH_CARD_TYPE IS NOT DISTINCT FROM $2.AUTH_CARD_TYPE)';
		END IF;
		IF OLD.DEX_FILE_SIZE IS DISTINCT FROM NEW.DEX_FILE_SIZE THEN
			lv_sql := lv_sql || 'DEX_FILE_SIZE = $2.DEX_FILE_SIZE, ';
			lv_filter := lv_filter || ' AND (DEX_FILE_SIZE IS NOT DISTINCT FROM $1.DEX_FILE_SIZE OR DEX_FILE_SIZE IS NOT DISTINCT FROM $2.DEX_FILE_SIZE)';
		END IF;
		IF OLD.DEX_RECEIVED_FLAG IS DISTINCT FROM NEW.DEX_RECEIVED_FLAG THEN
			lv_sql := lv_sql || 'DEX_RECEIVED_FLAG = $2.DEX_RECEIVED_FLAG, ';
			lv_filter := lv_filter || ' AND (DEX_RECEIVED_FLAG IS NOT DISTINCT FROM $1.DEX_RECEIVED_FLAG OR DEX_RECEIVED_FLAG IS NOT DISTINCT FROM $2.DEX_RECEIVED_FLAG)';
		END IF;
		IF OLD.TERMINAL_ID IS DISTINCT FROM NEW.TERMINAL_ID THEN
			lv_sql := lv_sql || 'TERMINAL_ID = $2.TERMINAL_ID, ';
			lv_filter := lv_filter || ' AND (TERMINAL_ID IS NOT DISTINCT FROM $1.TERMINAL_ID OR TERMINAL_ID IS NOT DISTINCT FROM $2.TERMINAL_ID)';
		END IF;
		IF OLD.CUSTOMER_ID IS DISTINCT FROM NEW.CUSTOMER_ID THEN
			lv_sql := lv_sql || 'CUSTOMER_ID = $2.CUSTOMER_ID, ';
			lv_filter := lv_filter || ' AND (CUSTOMER_ID IS NOT DISTINCT FROM $1.CUSTOMER_ID OR CUSTOMER_ID IS NOT DISTINCT FROM $2.CUSTOMER_ID)';
		END IF;
		IF OLD.LOCATION_NAME IS DISTINCT FROM NEW.LOCATION_NAME THEN
			lv_sql := lv_sql || 'LOCATION_NAME = $2.LOCATION_NAME, ';
			lv_filter := lv_filter || ' AND (LOCATION_NAME IS NOT DISTINCT FROM $1.LOCATION_NAME OR LOCATION_NAME IS NOT DISTINCT FROM $2.LOCATION_NAME)';
		END IF;
		IF OLD.CUSTOMER_NAME IS DISTINCT FROM NEW.CUSTOMER_NAME THEN
			lv_sql := lv_sql || 'CUSTOMER_NAME = $2.CUSTOMER_NAME, ';
			lv_filter := lv_filter || ' AND (CUSTOMER_NAME IS NOT DISTINCT FROM $1.CUSTOMER_NAME OR CUSTOMER_NAME IS NOT DISTINCT FROM $2.CUSTOMER_NAME)';
		END IF;
		IF OLD.CREDIT_TRANS_COUNT IS DISTINCT FROM NEW.CREDIT_TRANS_COUNT THEN
			lv_sql := lv_sql || 'CREDIT_TRANS_COUNT = $2.CREDIT_TRANS_COUNT, ';
			lv_filter := lv_filter || ' AND (CREDIT_TRANS_COUNT IS NOT DISTINCT FROM $1.CREDIT_TRANS_COUNT OR CREDIT_TRANS_COUNT IS NOT DISTINCT FROM $2.CREDIT_TRANS_COUNT)';
		END IF;
		IF OLD.CREDIT_TRANS_TOTAL IS DISTINCT FROM NEW.CREDIT_TRANS_TOTAL THEN
			lv_sql := lv_sql || 'CREDIT_TRANS_TOTAL = $2.CREDIT_TRANS_TOTAL, ';
			lv_filter := lv_filter || ' AND (CREDIT_TRANS_TOTAL IS NOT DISTINCT FROM $1.CREDIT_TRANS_TOTAL OR CREDIT_TRANS_TOTAL IS NOT DISTINCT FROM $2.CREDIT_TRANS_TOTAL)';
		END IF;
		IF OLD.CREDIT_VEND_COUNT IS DISTINCT FROM NEW.CREDIT_VEND_COUNT THEN
			lv_sql := lv_sql || 'CREDIT_VEND_COUNT = $2.CREDIT_VEND_COUNT, ';
			lv_filter := lv_filter || ' AND (CREDIT_VEND_COUNT IS NOT DISTINCT FROM $1.CREDIT_VEND_COUNT OR CREDIT_VEND_COUNT IS NOT DISTINCT FROM $2.CREDIT_VEND_COUNT)';
		END IF;
		IF OLD.CASH_TRANS_COUNT IS DISTINCT FROM NEW.CASH_TRANS_COUNT THEN
			lv_sql := lv_sql || 'CASH_TRANS_COUNT = $2.CASH_TRANS_COUNT, ';
			lv_filter := lv_filter || ' AND (CASH_TRANS_COUNT IS NOT DISTINCT FROM $1.CASH_TRANS_COUNT OR CASH_TRANS_COUNT IS NOT DISTINCT FROM $2.CASH_TRANS_COUNT)';
		END IF;
		IF OLD.CASH_TRANS_TOTAL IS DISTINCT FROM NEW.CASH_TRANS_TOTAL THEN
			lv_sql := lv_sql || 'CASH_TRANS_TOTAL = $2.CASH_TRANS_TOTAL, ';
			lv_filter := lv_filter || ' AND (CASH_TRANS_TOTAL IS NOT DISTINCT FROM $1.CASH_TRANS_TOTAL OR CASH_TRANS_TOTAL IS NOT DISTINCT FROM $2.CASH_TRANS_TOTAL)';
		END IF;
		IF OLD.CASH_VEND_COUNT IS DISTINCT FROM NEW.CASH_VEND_COUNT THEN
			lv_sql := lv_sql || 'CASH_VEND_COUNT = $2.CASH_VEND_COUNT, ';
			lv_filter := lv_filter || ' AND (CASH_VEND_COUNT IS NOT DISTINCT FROM $1.CASH_VEND_COUNT OR CASH_VEND_COUNT IS NOT DISTINCT FROM $2.CASH_VEND_COUNT)';
		END IF;
		IF OLD.PASSCARD_TRANS_COUNT IS DISTINCT FROM NEW.PASSCARD_TRANS_COUNT THEN
			lv_sql := lv_sql || 'PASSCARD_TRANS_COUNT = $2.PASSCARD_TRANS_COUNT, ';
			lv_filter := lv_filter || ' AND (PASSCARD_TRANS_COUNT IS NOT DISTINCT FROM $1.PASSCARD_TRANS_COUNT OR PASSCARD_TRANS_COUNT IS NOT DISTINCT FROM $2.PASSCARD_TRANS_COUNT)';
		END IF;
		IF OLD.PASSCARD_TRANS_TOTAL IS DISTINCT FROM NEW.PASSCARD_TRANS_TOTAL THEN
			lv_sql := lv_sql || 'PASSCARD_TRANS_TOTAL = $2.PASSCARD_TRANS_TOTAL, ';
			lv_filter := lv_filter || ' AND (PASSCARD_TRANS_TOTAL IS NOT DISTINCT FROM $1.PASSCARD_TRANS_TOTAL OR PASSCARD_TRANS_TOTAL IS NOT DISTINCT FROM $2.PASSCARD_TRANS_TOTAL)';
		END IF;
		IF OLD.PASSCARD_VEND_COUNT IS DISTINCT FROM NEW.PASSCARD_VEND_COUNT THEN
			lv_sql := lv_sql || 'PASSCARD_VEND_COUNT = $2.PASSCARD_VEND_COUNT, ';
			lv_filter := lv_filter || ' AND (PASSCARD_VEND_COUNT IS NOT DISTINCT FROM $1.PASSCARD_VEND_COUNT OR PASSCARD_VEND_COUNT IS NOT DISTINCT FROM $2.PASSCARD_VEND_COUNT)';
		END IF;
		IF OLD.LAST_AUTH_IN_TS IS DISTINCT FROM NEW.LAST_AUTH_IN_TS THEN
			lv_sql := lv_sql || 'LAST_AUTH_IN_TS = $2.LAST_AUTH_IN_TS, ';
			lv_filter := lv_filter || ' AND (LAST_AUTH_IN_TS IS NOT DISTINCT FROM $1.LAST_AUTH_IN_TS OR LAST_AUTH_IN_TS IS NOT DISTINCT FROM $2.LAST_AUTH_IN_TS)';
		END IF;
		IF OLD.LAST_TRANS_IN_TS IS DISTINCT FROM NEW.LAST_TRANS_IN_TS THEN
			lv_sql := lv_sql || 'LAST_TRANS_IN_TS = $2.LAST_TRANS_IN_TS, ';
			lv_filter := lv_filter || ' AND (LAST_TRANS_IN_TS IS NOT DISTINCT FROM $1.LAST_TRANS_IN_TS OR LAST_TRANS_IN_TS IS NOT DISTINCT FROM $2.LAST_TRANS_IN_TS)';
		END IF;
		IF OLD.FIRMWARE_VERSION IS DISTINCT FROM NEW.FIRMWARE_VERSION THEN
			lv_sql := lv_sql || 'FIRMWARE_VERSION = $2.FIRMWARE_VERSION, ';
			lv_filter := lv_filter || ' AND (FIRMWARE_VERSION IS NOT DISTINCT FROM $1.FIRMWARE_VERSION OR FIRMWARE_VERSION IS NOT DISTINCT FROM $2.FIRMWARE_VERSION)';
		END IF;
		IF OLD.COMM_METHOD_CD IS DISTINCT FROM NEW.COMM_METHOD_CD THEN
			lv_sql := lv_sql || 'COMM_METHOD_CD = $2.COMM_METHOD_CD, ';
			lv_filter := lv_filter || ' AND (COMM_METHOD_CD IS NOT DISTINCT FROM $1.COMM_METHOD_CD OR COMM_METHOD_CD IS NOT DISTINCT FROM $2.COMM_METHOD_CD)';
		END IF;
		IF OLD.COMMENTS IS DISTINCT FROM NEW.COMMENTS THEN
            lv_sql := lv_sql || 'COMMENTS = $2.COMMENTS, ';
            lv_filter := lv_filter || ' AND (COMMENTS IS NOT DISTINCT FROM $1.COMMENTS OR COMMENTS IS NOT DISTINCT FROM $2.COMMENTS)';
        END IF;
        lv_sql := lv_sql || 'LAST_UPDATED_UTC_TS = $2.LAST_UPDATED_UTC_TS, LAST_UPDATED_BY = $2.LAST_UPDATED_BY, ';
		lv_sql := LEFT(lv_sql, -2) || ' WHERE GLOBAL_SESSION_CD = $1.GLOBAL_SESSION_CD' || lv_filter;
		EXECUTE lv_sql USING OLD, NEW;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
		RETURN NEW;
	END IF;
	IF lv_new_partition_table IS NOT NULL THEN
		LOOP
			BEGIN
				EXECUTE 'INSERT INTO MAIN.' || lv_new_partition_table || ' SELECT $1.*' USING NEW;
				EXIT;
			EXCEPTION
			WHEN undefined_table THEN
				SELECT 'CONSTRAINT CK_' || lv_new_partition_table || ' CHECK(' ||'CALL_IN_START_TS >= ''' || TO_CHAR(NEW.CALL_IN_START_TS, 'YYYY-MM-DD') || '''::TIMESTAMP WITH TIME ZONE AND CALL_IN_START_TS < ''' || TO_CHAR(NEW.CALL_IN_START_TS + '1 DAY'::INTERVAL, 'YYYY-MM-DD') || '''::TIMESTAMP WITH TIME ZONE'||'), ' INTO lv_check_exp;
				RAISE NOTICE 'Creating new partition table %', lv_new_partition_table;
				BEGIN
					EXECUTE 'CREATE TABLE MAIN.' || lv_new_partition_table || '(' || lv_check_exp ||'CONSTRAINT PK_' || lv_new_partition_table || ' PRIMARY KEY(GLOBAL_SESSION_CD)) INHERITS(MAIN._DEVICE_SESSION)';
				EXCEPTION
					WHEN duplicate_table THEN
						CONTINUE;
				END;
				EXECUTE 'GRANT ALL ON TABLE MAIN.' || lv_new_partition_table || ' TO ' || CURRENT_USER;
				PERFORM MAIN.COPY_INDEXES('_DEVICE_SESSION', lv_new_suffix);
				EXECUTE 'ALTER TABLE MAIN.' || lv_new_partition_table || ' ADD FOREIGN KEY(NET_LAYER_ID) REFERENCES MAIN.NET_LAYER(NET_LAYER_ID)';
				EXECUTE 'ALTER TABLE MAIN.' || lv_new_partition_table || ' ADD FOREIGN KEY(DEVICE_TYPE_ID) REFERENCES MAIN.DEVICE_TYPE(DEVICE_TYPE_ID)';
				EXECUTE 'ALTER TABLE MAIN.' || lv_new_partition_table || ' ADD FOREIGN KEY(SESSION_STATE_ID) REFERENCES MAIN.SESSION_STATE(SESSION_STATE_ID)';
				EXECUTE 'ALTER TABLE MAIN.' || lv_new_partition_table || ' ADD FOREIGN KEY(COMM_METHOD_CD) REFERENCES MAIN.COMM_METHOD(COMM_METHOD_CD)';
			END;
		END LOOP;
	END IF;
	IF lv_old_partition_table IS NOT NULL THEN
		EXECUTE 'DELETE FROM MAIN.' || lv_old_partition_table || ' WHERE GLOBAL_SESSION_CD = $1.GLOBAL_SESSION_CD' USING OLD;
		GET DIAGNOSTICS ln_cnt = ROW_COUNT;
		IF ln_cnt = 0 THEN
			RAISE serialization_failure;
		END IF;
	END IF;
	IF TG_OP IN('INSERT', 'UPDATE') THEN
		RETURN NEW;
	ELSE
		RETURN OLD;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE VIEW MAIN.DEVICE_SESSION
	AS SELECT * FROM MAIN._DEVICE_SESSION;

ALTER VIEW MAIN.DEVICE_SESSION ALTER CALL_IN_START_TS SET DEFAULT CURRENT_TIMESTAMP;
ALTER VIEW MAIN.DEVICE_SESSION ALTER SESSION_STATE_ID SET DEFAULT 1;
ALTER VIEW MAIN.DEVICE_SESSION ALTER CALL_IN_STATUS SET DEFAULT 'I';
ALTER VIEW MAIN.DEVICE_SESSION ALTER CALL_IN_TYPE SET DEFAULT 'U';
ALTER VIEW MAIN.DEVICE_SESSION ALTER CREATED_BY SET DEFAULT CURRENT_USER;
ALTER VIEW MAIN.DEVICE_SESSION ALTER LAST_UPDATED_BY SET DEFAULT CURRENT_USER;

GRANT SELECT ON MAIN.DEVICE_SESSION TO read_main;
GRANT INSERT, UPDATE, DELETE ON MAIN.DEVICE_SESSION TO write_main;
REVOKE INSERT, UPDATE, DELETE, TRUNCATE ON MAIN._DEVICE_SESSION FROM postgres;

DROP TRIGGER IF EXISTS TRIIUD_DEVICE_SESSION ON MAIN.DEVICE_SESSION;
CREATE TRIGGER TRIIUD_DEVICE_SESSION
INSTEAD OF INSERT OR UPDATE OR DELETE ON MAIN.DEVICE_SESSION
	FOR EACH ROW
	EXECUTE PROCEDURE MAIN.FRIIUD_DEVICE_SESSION();


CREATE OR REPLACE FUNCTION MAIN.UPSERT_DEVICE_SESSION(
	p_global_session_cd VARCHAR(100), 
	p_call_in_start_ts TIMESTAMP WITH TIME ZONE, 
	p_device_name VARCHAR(60), 
	p_net_layer_id INTEGER, 
	p_device_serial_cd VARCHAR(60), 
	p_device_type_id INTEGER, 
	p_session_state_id SMALLINT, 
	p_call_in_finish_ts TIMESTAMP WITH TIME ZONE, 
	p_call_in_status CHAR(1), 
	p_client_ip_address VARCHAR(16), 
	p_client_port INTEGER, 
	p_inbound_message_count INTEGER, 
	p_outbound_message_count INTEGER, 
	p_inbound_byte_count INTEGER, 
	p_outbound_byte_count INTEGER, 
	p_device_initialized_flag CHAR(1), 
	p_device_sent_config_flag CHAR(1), 
	p_server_sent_config_flag CHAR(1), 
	p_call_in_type CHAR(1), 
	p_auth_amount DECIMAL(14,2), 
	p_auth_approved_flag CHAR(1), 
	p_auth_card_type CHAR(1), 
	p_dex_file_size BIGINT, 
	p_dex_received_flag CHAR(1), 
	p_terminal_id BIGINT, 
	p_customer_id BIGINT, 
	p_location_name VARCHAR(200), 
	p_customer_name VARCHAR(200), 
	p_credit_trans_count INTEGER, 
	p_credit_trans_total DECIMAL(14,2), 
	p_credit_vend_count INTEGER, 
	p_cash_trans_count INTEGER, 
	p_cash_trans_total DECIMAL(14,2), 
	p_cash_vend_count INTEGER, 
	p_passcard_trans_count INTEGER, 
	p_passcard_trans_total DECIMAL(14,2), 
	p_passcard_vend_count INTEGER, 
	p_last_auth_in_ts TIMESTAMP WITH TIME ZONE, 
	p_last_trans_in_ts TIMESTAMP WITH TIME ZONE, 
	p_firmware_version VARCHAR(200), 
	p_comm_method_cd CHAR(1),
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
BEGIN
	LOOP
		BEGIN
			UPDATE MAIN.DEVICE_SESSION
			   SET CALL_IN_START_TS = COALESCE(p_call_in_start_ts, CALL_IN_START_TS),
			       DEVICE_NAME = p_device_name,
			       NET_LAYER_ID = p_net_layer_id,
			       DEVICE_SERIAL_CD = p_device_serial_cd,
			       DEVICE_TYPE_ID = p_device_type_id,
			       SESSION_STATE_ID = COALESCE(p_session_state_id, SESSION_STATE_ID),
			       CALL_IN_FINISH_TS = p_call_in_finish_ts,
			       CALL_IN_STATUS = COALESCE(p_call_in_status, CALL_IN_STATUS),
			       CLIENT_IP_ADDRESS = p_client_ip_address,
			       CLIENT_PORT = p_client_port,
			       INBOUND_MESSAGE_COUNT = p_inbound_message_count,
			       OUTBOUND_MESSAGE_COUNT = p_outbound_message_count,
			       INBOUND_BYTE_COUNT = p_inbound_byte_count,
			       OUTBOUND_BYTE_COUNT = p_outbound_byte_count,
			       DEVICE_INITIALIZED_FLAG = p_device_initialized_flag,
			       DEVICE_SENT_CONFIG_FLAG = p_device_sent_config_flag,
			       SERVER_SENT_CONFIG_FLAG = p_server_sent_config_flag,
			       CALL_IN_TYPE = p_call_in_type,
			       AUTH_AMOUNT = p_auth_amount,
			       AUTH_APPROVED_FLAG = p_auth_approved_flag,
			       AUTH_CARD_TYPE = p_auth_card_type,
			       DEX_FILE_SIZE = p_dex_file_size,
			       DEX_RECEIVED_FLAG = p_dex_received_flag,
			       TERMINAL_ID = p_terminal_id,
			       CUSTOMER_ID = p_customer_id,
			       LOCATION_NAME = p_location_name,
			       CUSTOMER_NAME = p_customer_name,
			       CREDIT_TRANS_COUNT = p_credit_trans_count,
			       CREDIT_TRANS_TOTAL = p_credit_trans_total,
			       CREDIT_VEND_COUNT = p_credit_vend_count,
			       CASH_TRANS_COUNT = p_cash_trans_count,
			       CASH_TRANS_TOTAL = p_cash_trans_total,
			       CASH_VEND_COUNT = p_cash_vend_count,
			       PASSCARD_TRANS_COUNT = p_passcard_trans_count,
			       PASSCARD_TRANS_TOTAL = p_passcard_trans_total,
			       PASSCARD_VEND_COUNT = p_passcard_vend_count,
			       LAST_AUTH_IN_TS = p_last_auth_in_ts,
			       LAST_TRANS_IN_TS = p_last_trans_in_ts,
			       FIRMWARE_VERSION = p_firmware_version,
			       COMM_METHOD_CD = p_comm_method_cd
			 WHERE GLOBAL_SESSION_CD = p_global_session_cd;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				INSERT INTO MAIN.DEVICE_SESSION(GLOBAL_SESSION_CD, CALL_IN_START_TS, DEVICE_NAME, NET_LAYER_ID, DEVICE_SERIAL_CD, DEVICE_TYPE_ID, SESSION_STATE_ID, CALL_IN_FINISH_TS, CALL_IN_STATUS, CLIENT_IP_ADDRESS, CLIENT_PORT, INBOUND_MESSAGE_COUNT, OUTBOUND_MESSAGE_COUNT, INBOUND_BYTE_COUNT, OUTBOUND_BYTE_COUNT, DEVICE_INITIALIZED_FLAG, DEVICE_SENT_CONFIG_FLAG, SERVER_SENT_CONFIG_FLAG, CALL_IN_TYPE, AUTH_AMOUNT, AUTH_APPROVED_FLAG, AUTH_CARD_TYPE, DEX_FILE_SIZE, DEX_RECEIVED_FLAG, TERMINAL_ID, CUSTOMER_ID, LOCATION_NAME, CUSTOMER_NAME, CREDIT_TRANS_COUNT, CREDIT_TRANS_TOTAL, CREDIT_VEND_COUNT, CASH_TRANS_COUNT, CASH_TRANS_TOTAL, CASH_VEND_COUNT, PASSCARD_TRANS_COUNT, PASSCARD_TRANS_TOTAL, PASSCARD_VEND_COUNT, LAST_AUTH_IN_TS, LAST_TRANS_IN_TS, FIRMWARE_VERSION, COMM_METHOD_CD)
				 VALUES(p_global_session_cd, COALESCE(p_call_in_start_ts, CURRENT_TIMESTAMP), p_device_name, p_net_layer_id, p_device_serial_cd, p_device_type_id, COALESCE(p_session_state_id, 1), p_call_in_finish_ts, COALESCE(p_call_in_status, 'I'), p_client_ip_address, p_client_port, p_inbound_message_count, p_outbound_message_count, p_inbound_byte_count, p_outbound_byte_count, p_device_initialized_flag, p_device_sent_config_flag, p_server_sent_config_flag, p_call_in_type, p_auth_amount, p_auth_approved_flag, p_auth_card_type, p_dex_file_size, p_dex_received_flag, p_terminal_id, p_customer_id, p_location_name, p_customer_name, p_credit_trans_count, p_credit_trans_total, p_credit_vend_count, p_cash_trans_count, p_cash_trans_total, p_cash_vend_count, p_passcard_trans_count, p_passcard_trans_total, p_passcard_vend_count, p_last_auth_in_ts, p_last_trans_in_ts, p_firmware_version, p_comm_method_cd);
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			END IF;
			EXIT;
		EXCEPTION
			WHEN unique_violation THEN
				NULL;
			WHEN serialization_failure THEN
				NULL;
		END;
	END LOOP;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MAIN.UPSERT_DEVICE_SESSION(
	p_global_session_cd VARCHAR(100), 
	p_call_in_start_ts TIMESTAMP WITH TIME ZONE, 
	p_device_name VARCHAR(60), 
	p_net_layer_id INTEGER, 
	p_device_serial_cd VARCHAR(60), 
	p_device_type_id INTEGER, 
	p_session_state_id SMALLINT, 
	p_call_in_finish_ts TIMESTAMP WITH TIME ZONE, 
	p_call_in_status CHAR(1), 
	p_client_ip_address VARCHAR(16), 
	p_client_port INTEGER, 
	p_inbound_message_count INTEGER, 
	p_outbound_message_count INTEGER, 
	p_inbound_byte_count INTEGER, 
	p_outbound_byte_count INTEGER, 
	p_device_initialized_flag CHAR(1), 
	p_device_sent_config_flag CHAR(1), 
	p_server_sent_config_flag CHAR(1), 
	p_call_in_type CHAR(1), 
	p_auth_amount DECIMAL(14,2), 
	p_auth_approved_flag CHAR(1), 
	p_auth_card_type CHAR(1), 
	p_dex_file_size BIGINT, 
	p_dex_received_flag CHAR(1), 
	p_terminal_id BIGINT, 
	p_customer_id BIGINT, 
	p_location_name VARCHAR(200), 
	p_customer_name VARCHAR(200), 
	p_credit_trans_count INTEGER, 
	p_credit_trans_total DECIMAL(14,2), 
	p_credit_vend_count INTEGER, 
	p_cash_trans_count INTEGER, 
	p_cash_trans_total DECIMAL(14,2), 
	p_cash_vend_count INTEGER, 
	p_passcard_trans_count INTEGER, 
	p_passcard_trans_total DECIMAL(14,2), 
	p_passcard_vend_count INTEGER, 
	p_last_auth_in_ts TIMESTAMP WITH TIME ZONE, 
	p_last_trans_in_ts TIMESTAMP WITH TIME ZONE, 
	p_firmware_version VARCHAR(200), 
	p_comm_method_cd CHAR(1),
	p_row_count OUT INTEGER) TO write_main;



CREATE INDEX IX_DEVICE_SESSION_DEVICE_NAME_SESSION_STATE_ID_CALL_IN_TYPE ON MAIN._DEVICE_SESSION(DEVICE_NAME,SESSION_STATE_ID,CALL_IN_TYPE);
CREATE INDEX IX_DEVICE_SESSION_CALL_IN_START_TS_DEVICE_NAME ON MAIN._DEVICE_SESSION(CALL_IN_START_TS,DEVICE_NAME);
CREATE INDEX IX_DEVICE_SESSION_TERMINAL_ID ON MAIN._DEVICE_SESSION(TERMINAL_ID);
CREATE INDEX IX_DEVICE_SESSION_CUSTOMER_ID ON MAIN._DEVICE_SESSION(CUSTOMER_ID);
CREATE INDEX IX_DEVICE_SESSION_DEVICE_NAME_CALL_IN_START_TS ON MAIN._DEVICE_SESSION(DEVICE_NAME, CALL_IN_START_TS);

-- ======================================================================

CREATE TABLE MAIN.MESSAGE_TYPE
  (
    MESSAGE_TYPE_CD      VARCHAR(10),
    MESSAGE_TYPE_NAME    VARCHAR(200)   NOT NULL,
    MESSAGE_CATEGORY_CD  VARCHAR(30)    NOT NULL,

    PRIMARY KEY(MESSAGE_TYPE_CD),

    FOREIGN KEY(MESSAGE_CATEGORY_CD) REFERENCES MAIN.MESSAGE_CATEGORY(MESSAGE_CATEGORY_CD)
  )
  INHERITS(MAIN._);

DROP TRIGGER IF EXISTS TRBI_MESSAGE_TYPE ON MAIN.MESSAGE_TYPE;
CREATE TRIGGER TRBI_MESSAGE_TYPE
BEFORE INSERT ON MAIN.MESSAGE_TYPE
	FOR EACH ROW
	EXECUTE PROCEDURE MAIN.FTRBI_AUDIT();

DROP TRIGGER IF EXISTS TRBU_MESSAGE_TYPE ON MAIN.MESSAGE_TYPE;
CREATE TRIGGER TRBU_MESSAGE_TYPE
BEFORE UPDATE ON MAIN.MESSAGE_TYPE
	FOR EACH ROW
	EXECUTE PROCEDURE MAIN.FTRBU_AUDIT();


CREATE OR REPLACE FUNCTION MAIN.UPSERT_MESSAGE_TYPE(
	p_message_type_cd VARCHAR(10), 
	p_message_type_name VARCHAR(200), 
	p_message_category_cd VARCHAR(30),
	p_row_count OUT INTEGER)
	SECURITY DEFINER
AS $$
BEGIN
	LOOP
		BEGIN
			UPDATE MAIN.MESSAGE_TYPE
			   SET MESSAGE_TYPE_NAME = p_message_type_name,
			       MESSAGE_CATEGORY_CD = p_message_category_cd
			 WHERE MESSAGE_TYPE_CD IS NOT DISTINCT FROM p_message_type_cd;
			IF FOUND THEN
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			ELSE
				INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD)
				 VALUES(p_message_type_cd, p_message_type_name, p_message_category_cd);
				GET DIAGNOSTICS p_row_count = ROW_COUNT;
			END IF;
			EXIT;
		EXCEPTION
			WHEN unique_violation THEN
				NULL;
		END;
	END LOOP;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MAIN.UPSERT_MESSAGE_TYPE(
	p_message_type_cd VARCHAR(10), 
	p_message_type_name VARCHAR(200), 
	p_message_category_cd VARCHAR(30),
	p_row_count OUT INTEGER) TO write_main;



-- ----------------------------------------------------------------------

INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('2A', '(2Ah) Net Authorization Batch V2', 'SALE');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('2B', '(2Bh) Local Authorization Batch V2', 'LOCAL_AUTH');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('2E', '(2Eh) Generic Data Log', 'MISC');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('2F', '(2Fh) Generic ACK', 'FLOW');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('5B', '(5Bh) Cash Sales Detail', 'CASH');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('5C', '(5Ch) Alive Alert', 'MISC');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('5E', '(5Eh) Authorization Request V2', 'AUTH');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('5F', '(5Fh) Sony Info', 'MISC');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('60', '(60h) Sony Info Old', 'MISC');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('71', '(71h) Batch ACK V2', 'FLOW');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('73', '(73h) Force Device Shutdown', 'MISC');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('75', '(75h) Device Control ACK', 'FLOW');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('76', '(76h) Device Reactivate', 'MISC');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('7C', '(7Ch) File Transfer Start', 'FILE');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('7D', '(7Dh) File Transfer Start ACK', 'FLOW');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('7E', '(7Eh) File Transfer', 'FILE_BLOCK');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('7F', '(7Fh) File Transfer ACK', 'FLOW');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('80', '(80h) File Transfer Kill', 'FLOW');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('81', '(81h) File Transfer Kill ACK', 'FLOW');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('82', '(82h) Client to Server Info Request', 'MISC');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('83', '(83h) Server to Client Info Request', 'MISC');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('84', '(84h) UNIX Time', 'MISC');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('85', '(85h) BCD Time', 'MISC');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('86', '(86h) Gx Counters', 'MISC');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('87', '(87h) Gx Peek', 'MISC');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('88', '(88h) Gx Poke', 'MISC');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('8E', '(8Eh) Initialization V3', 'INIT');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('8F', '(8Fh) Set ID Number and Key', 'MISC');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('90', '(90h) Terminal Update Status', 'MISC');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('91', '(91h) Generic NAK', 'FLOW');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('92', '(92h) Update Status Request', 'USR');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('93', '(93h) Local Authorization Batch V2 (BCD)', 'LOCAL_AUTH');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('94', '(94h) Cash Sales Detail V2 (BCD)', 'CASH');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('95', '(95h) Cash Sale Detail ACK V2', 'CASH');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('96', '(96h) Cash Sales Detail V3', 'CASH');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('97', '(96h) Cash Sales Detail V3 (BCD)', 'CASH');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('98', '(98h) Generic NAK V2', 'FLOW');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('99', '(99h) Client Version', 'MISC');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('9A41', '(9Ah)(41h) Room Status w/ Starting Port', 'COMPONENT_INFO');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('9A45', '(9Ah)(45h) Room Model Info/Layout', 'COMPONENT_INFO');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('9A46', '(9Ah)(46h) Room Model Info/Layout ACK', 'FLOW');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('9A47', '(9Ah)(47h) Room Status', 'COMPONENT_INFO');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('9A5E', '(9Ah)(5Eh) eSuds Network Authorization Batch (actual)', 'SALE');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('9A5F', '(9Ah)(5Fh) eSuds Local Authorization Batch', 'LOCAL_AUTH');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('9A60', '(9Ah)(60h) eSuds Network Authorization Batch (intended)', 'SALE');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('9A61', '(9Ah)(61h) Get eSuds Machine Diagnostics', 'MISC');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('9A62', '(9Ah)(62h) Washer/Dryer Diagnostics', 'MISC');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('9A63', '(9Ah)(63h) Room Model Info/Layout V2', 'COMPONENT_INFO');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('9A6A', '(9Ah)(6Ah) Washer/Dryer Labels', 'COMPONENT_INFO');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('9B', '(9Bh) File Transfer Request', 'MISC');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('??', '(??) Invalid message', 'INVALID');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('A0', '(A0h) Authorization Request V3', 'AUTH');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('A1', '(A1h) Authorization Response V3', 'AUTH_RESPONSE');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('A2', '(A2h) Net Authorization Batch V2.1', 'SALE');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('A3', '(A3h) Local Authorization Batch V2.2', 'LOCAL_AUTH');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('A4', '(A4h) File Transfer Start V1.1', 'FILE');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('A5', '(A5h) File Transfer Start ACK V1.1', 'FLOW');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('A6', '(A6h) File Transfer V1.1', 'FILE_BLOCK');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('A7', '(A7h) File Transfer ACK V1.1', 'FLOW');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('A8', '(A8h) Counters V2.0', 'SETTLEMENT');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('A9', '(A9h) External File Transfer Request', 'FILE');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('AA', '(AAh) Permission Request', 'AUTH');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('AB', '(ABh) Permission Response', 'AUTH_RESPONSE');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('AC', '(ACh) Authorization Request V3.1', 'AUTH');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('AD', '(ADh) Initialization V3.1', 'INIT');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('AE', '(AEh) Authorization Request V3.2', 'AUTH');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('AF', '(AFh) Authorization Response V3.2', 'AUTH_RESPONSE');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('B1', '(B1h) Initialization V2', 'INIT');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('B9', '(B9h) Sony Info V2', 'MISC');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('C0', '(C0h) Initialization V4.1', 'INIT');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('C1', '(C1h) Component Identification V4.1', 'COMPONENT_INFO');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('C2', '(C2h) Authorization Request V4.1', 'AUTH');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('C3', '(C3h) Authorization Response V4.1', 'AUTH_RESPONSE');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('C4', '(C4h) Sale V4.1', 'SALE');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('C5', '(C5h) Settlement V4.1', 'SETTLEMENT');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('C6', '(C6h) Generic Event V4.1', 'EVENT');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('C7', '(C7h) Short File Transfer V4.1', 'FILE');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('C8', '(C8h) File Transfer Start V4.1', 'FILE');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('C9', '(C9h) File Transfer V4.1', 'FILE_BLOCK');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('CA', '(CAh) Generic Request V4.1', 'MISC');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('CB', '(CBh) Generic Response V4.1', 'MISC');
INSERT INTO MAIN.MESSAGE_TYPE(MESSAGE_TYPE_CD, MESSAGE_TYPE_NAME, MESSAGE_CATEGORY_CD) VALUES('CC', '(CCh) Charge Request V4.1', 'AUTH');
COMMIT;

-- ======================================================================

GRANT SELECT ON ALL TABLES IN SCHEMA MAIN TO read_main;
REVOKE EXECUTE ON ALL FUNCTIONS IN SCHEMA MAIN FROM PUBLIC;
GRANT USAGE ON SCHEMA MAIN TO read_main;
GRANT USAGE ON SCHEMA MAIN TO write_main;
GRANT USAGE ON ALL SEQUENCES IN SCHEMA MAIN TO write_main;

INSERT INTO MAIN.MESSAGE_STAGE(MESSAGE_STAGE_ID, MESSAGE_STAGE_NAME, MESSAGE_CATEGORY, FINAL_FLAG, ERROR_FLAG, MAX_LATENCY_TOLERATED)
  VALUES
	(0, 'UNKNOWN', 'UNKNOWN', FALSE, FALSE, 60*60*4),
	(1, 'Not Auditted', 'Other', TRUE, FALSE, 0),
	(2, 'No Trans', 'Authorization', FALSE, FALSE, 60*60*4),
	(3, 'Auth Declined', 'Authorization', TRUE, FALSE, 0),
	(4, 'Auth Failed', 'Authorization', TRUE, FALSE, 0),
	(5, 'Event Processed', 'Authorization', TRUE, FALSE, 0),
	(6, 'No Event', 'Authorization', FALSE, FALSE, 60*60*4),
	(7, 'No Fill', 'Authorization', FALSE, FALSE, 60*60*4),
	(8, 'Fill Imported', 'Authorization', TRUE, FALSE, 0), 
	(9, 'No Fill-to-Fill Payment Batch', 'Authorization', FALSE, FALSE, 60*60*24*14),
	(10, 'Fill-to-Fill Unpaid', 'Authorization', FALSE, FALSE, 60*60*24*30),
	(11, 'Fill-to-Fill Paid', 'Authorization', TRUE, FALSE, 0),

	(12, 'No Sale', 'Authorization', FALSE, FALSE, 60*60*24*3),
	(13, 'No Sale Record', 'Authorization', FALSE, TRUE, 0),
	(14, 'Canceled', 'Authorization', TRUE, FALSE, 0),
	(15, 'Not Imported', 'Authorization', FALSE, FALSE, 60*60*4),
	(16, 'Unknown Imported', 'Authorization', FALSE, FALSE, 60*60*4),
	(17, 'Null Imported', 'Authorization', FALSE, FALSE, 60*60*4),
	(18, 'Weird Imported', 'Authorization', FALSE, TRUE, 0),
	(19, 'Not in Reporting', 'Authorization', TRUE, TRUE, 0),

	(20, 'eSuds - Terminal Orphan', 'Authorization', TRUE, FALSE, 0),
	(21, 'Terminal Orphan', 'Authorization', FALSE, FALSE, 60*60*4),
	(22, 'Bank Acct Orphan', 'Authorization', FALSE, FALSE, 60*60*4),
	(23, 'Not Reported', 'Authorization', FALSE, FALSE, 60*60*4),
	(24, 'Not in Export Batch', 'Authorization', FALSE, TRUE, 0),
	(25, 'In Open Export Batch', 'Authorization', FALSE, FALSE, 60*60*30),
	(26, 'In Closed Export Batch', 'Authorization', FALSE, FALSE, 60*60*24*10),
	(27, 'Weird Export Batch Status', 'Authorization', FALSE, TRUE, 0),
	(28, 'Not In Ledger', 'Authorization', FALSE, FALSE, 60*60*4),
	(29, 'Not Paid', 'Authorization', FALSE, FALSE, 60*60*24*10),
	(30, 'Reported and Sent', 'Authorization', TRUE, FALSE, 0),
	(31, 'Paid and Reported and Sent', 'Authorization', TRUE, FALSE, 0)
;

COMMIT;
-- ======================================================================

