CREATE OR REPLACE FUNCTION MAIN.MILLIS_TO_TIMESTAMP(millis BIGINT)
	RETURNS TIMESTAMP
	SECURITY DEFINER
AS $$
BEGIN
    RETURN cast('epoch' as timestamp) + cast('' || (millis / 1000.0) || ' seconds' as interval);
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MAIN.MILLIS_TO_TIMESTAMP(millis BIGINT) TO read_main;

CREATE OR REPLACE FUNCTION MAIN.UPDATE_SESSION_START(
      pv_global_session_cd MAIN.DEVICE_SESSION.GLOBAL_SESSION_CD%TYPE,
      pv_device_name MAIN.DEVICE_SESSION.DEVICE_NAME%TYPE,
      pv_device_serial_cd MAIN.DEVICE_SESSION.DEVICE_SERIAL_CD%TYPE,
	  pn_device_type_id MAIN.DEVICE_SESSION.DEVICE_TYPE_ID%TYPE,
      pn_net_layer_id MAIN.DEVICE_SESSION.NET_LAYER_ID%TYPE,
      pv_remote_address MAIN.DEVICE_SESSION.CLIENT_IP_ADDRESS%TYPE,
      pn_remote_port MAIN.DEVICE_SESSION.CLIENT_PORT%TYPE,
	  pt_call_in_start_ts MAIN.DEVICE_SESSION.CALL_IN_START_TS%TYPE,
      pn_terminal_id MAIN.DEVICE_SESSION.TERMINAL_ID%TYPE, 
	  pn_customer_id MAIN.DEVICE_SESSION.CUSTOMER_ID%TYPE, 
      pv_location_name MAIN.DEVICE_SESSION.LOCATION_NAME%TYPE,
      pv_customer_name MAIN.DEVICE_SESSION.CUSTOMER_NAME%TYPE
	  )
    RETURNS VOID
	SECURITY DEFINER
AS $$
BEGIN
	-- R33 version
	IF pv_global_session_cd IS NULL THEN
		RAISE EXCEPTION 'Invalid pv_global_session_cd: %', pv_global_session_cd USING ERRCODE = '20701';
	END IF;

	IF pv_device_name IS NULL THEN
		RAISE EXCEPTION 'Invalid pv_device_name: %', pv_device_name USING ERRCODE = '20701';
	END IF;
	
	LOOP
        BEGIN
	        UPDATE MAIN.DEVICE_SESSION
	           SET DEVICE_SERIAL_CD = CASE WHEN DEVICE_SERIAL_CD IS NULL THEN pv_device_serial_cd ELSE DEVICE_SERIAL_CD END,
	               DEVICE_TYPE_ID = CASE WHEN pn_device_type_id = -1 THEN DEVICE_TYPE_ID WHEN DEVICE_TYPE_ID IS NULL THEN pn_device_type_id ELSE DEVICE_TYPE_ID END,
	               NET_LAYER_ID = pn_net_layer_id,
	               CLIENT_IP_ADDRESS = pv_remote_address,
	               CLIENT_PORT = pn_remote_port,
	               CALL_IN_START_TS = pt_call_in_start_ts,
	               LOCATION_NAME = pv_location_name,
	               CUSTOMER_NAME = pv_customer_name,
	               TERMINAL_ID = pn_terminal_id,
	               CUSTOMER_ID = pn_customer_id
	         WHERE GLOBAL_SESSION_CD = pv_global_session_cd;
	        EXIT WHEN FOUND;
            INSERT INTO MAIN.DEVICE_SESSION(
                GLOBAL_SESSION_CD, 
                DEVICE_NAME,
                CALL_IN_START_TS,
                DEVICE_SERIAL_CD,
                DEVICE_TYPE_ID,
                NET_LAYER_ID,
                CLIENT_IP_ADDRESS,
                CLIENT_PORT,
                LOCATION_NAME,
                CUSTOMER_NAME,
                TERMINAL_ID,
                CUSTOMER_ID
            ) VALUES (
                pv_global_session_cd,
                pv_device_name,
                pt_call_in_start_ts,
                pv_device_serial_cd,
                CASE WHEN pn_device_type_id = -1 THEN NULL ELSE pn_device_type_id END,
                pn_net_layer_id,
                pv_remote_address,
                pn_remote_port,
                pv_location_name, 
                pv_customer_name,
                pn_terminal_id,
                pn_customer_id
            );
            EXIT;
        EXCEPTION 
            WHEN unique_violation THEN
                NULL;
            WHEN serialization_failure THEN
                NULL;
        END;
    END LOOP;
   	
	-- Mark any previous started sessions as aborted
	LOOP
        BEGIN
		    UPDATE MAIN.DEVICE_SESSION DS
			   SET SESSION_STATE_ID = 3,
			       CALL_IN_STATUS = CASE CALL_IN_STATUS WHEN 'I' THEN 'U' ELSE CALL_IN_STATUS END
			 WHERE DEVICE_NAME = pv_device_name
			   AND SESSION_STATE_ID = 1
			   AND GLOBAL_SESSION_CD != pv_global_session_cd
			   AND CALL_IN_START_TS < COALESCE(pt_call_in_start_ts, CURRENT_TIMESTAMP);
            EXIT;
        EXCEPTION 
            WHEN serialization_failure THEN
                NULL;
        END;
    END LOOP;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MAIN.UPDATE_SESSION_START(
      pv_global_session_cd MAIN.DEVICE_SESSION.GLOBAL_SESSION_CD%TYPE,
      pv_device_name MAIN.DEVICE_SESSION.DEVICE_NAME%TYPE,
      pv_device_serial_cd MAIN.DEVICE_SESSION.DEVICE_SERIAL_CD%TYPE,
	  pn_device_type_id MAIN.DEVICE_SESSION.DEVICE_TYPE_ID%TYPE,
      pn_net_layer_id MAIN.DEVICE_SESSION.NET_LAYER_ID%TYPE,
      pv_remote_address MAIN.DEVICE_SESSION.CLIENT_IP_ADDRESS%TYPE,
      pn_remote_port MAIN.DEVICE_SESSION.CLIENT_PORT%TYPE,
	  pt_call_in_start_ts MAIN.DEVICE_SESSION.CALL_IN_START_TS%TYPE,
      pn_terminal_id MAIN.DEVICE_SESSION.TERMINAL_ID%TYPE, 
      pn_customer_id MAIN.DEVICE_SESSION.CUSTOMER_ID%TYPE, 
      pv_location_name MAIN.DEVICE_SESSION.LOCATION_NAME%TYPE,
      pv_customer_name MAIN.DEVICE_SESSION.CUSTOMER_NAME%TYPE
	  ) TO write_main;
	  
CREATE OR REPLACE FUNCTION MAIN.UPDATE_SESSION_START(
      pv_global_session_cd MAIN.DEVICE_SESSION.GLOBAL_SESSION_CD%TYPE,
      pv_device_name MAIN.DEVICE_SESSION.DEVICE_NAME%TYPE,
      pv_device_serial_cd MAIN.DEVICE_SESSION.DEVICE_SERIAL_CD%TYPE,
	  pn_device_type_id MAIN.DEVICE_SESSION.DEVICE_TYPE_ID%TYPE,
      pn_net_layer_id MAIN.DEVICE_SESSION.NET_LAYER_ID%TYPE,
      pv_remote_address MAIN.DEVICE_SESSION.CLIENT_IP_ADDRESS%TYPE,
      pn_remote_port MAIN.DEVICE_SESSION.CLIENT_PORT%TYPE,
	  pt_call_in_start_ts MAIN.DEVICE_SESSION.CALL_IN_START_TS%TYPE,
      pn_terminal_id MAIN.DEVICE_SESSION.TERMINAL_ID%TYPE, 
	  pn_customer_id MAIN.DEVICE_SESSION.CUSTOMER_ID%TYPE, 
      pv_location_name MAIN.DEVICE_SESSION.LOCATION_NAME%TYPE,
      pv_customer_name MAIN.DEVICE_SESSION.CUSTOMER_NAME%TYPE,
	  pc_comm_method_cd MAIN.DEVICE_SESSION.COMM_METHOD_CD%TYPE
	  )
    RETURNS VOID
	SECURITY DEFINER
AS $$
BEGIN
	--R34+ version
	IF pv_global_session_cd IS NULL THEN
		RAISE EXCEPTION 'Invalid pv_global_session_cd: %', pv_global_session_cd USING ERRCODE = '20701';
	END IF;

	IF pv_device_name IS NULL THEN
		RAISE EXCEPTION 'Invalid pv_device_name: %', pv_device_name USING ERRCODE = '20701';
	END IF;
	
	LOOP
        BEGIN
	        UPDATE MAIN.DEVICE_SESSION
	           SET DEVICE_SERIAL_CD = CASE WHEN DEVICE_SERIAL_CD IS NULL THEN pv_device_serial_cd ELSE DEVICE_SERIAL_CD END,
	               DEVICE_TYPE_ID = CASE WHEN pn_device_type_id = -1 THEN DEVICE_TYPE_ID WHEN DEVICE_TYPE_ID IS NULL THEN pn_device_type_id ELSE DEVICE_TYPE_ID END,
	               NET_LAYER_ID = pn_net_layer_id,
	               CLIENT_IP_ADDRESS = pv_remote_address,
	               CLIENT_PORT = pn_remote_port,
	               CALL_IN_START_TS = pt_call_in_start_ts,
	               LOCATION_NAME = pv_location_name,
	               CUSTOMER_NAME = pv_customer_name,
	               TERMINAL_ID = pn_terminal_id,
	               CUSTOMER_ID = pn_customer_id,
				   COMM_METHOD_CD = pc_comm_method_cd
	         WHERE GLOBAL_SESSION_CD = pv_global_session_cd;
	        EXIT WHEN FOUND;
            INSERT INTO MAIN.DEVICE_SESSION(
                GLOBAL_SESSION_CD, 
                DEVICE_NAME,
                CALL_IN_START_TS,
                DEVICE_SERIAL_CD,
                DEVICE_TYPE_ID,
                NET_LAYER_ID,
                CLIENT_IP_ADDRESS,
                CLIENT_PORT,
                LOCATION_NAME,
                CUSTOMER_NAME,
                TERMINAL_ID,
                CUSTOMER_ID,
				COMM_METHOD_CD
            ) VALUES (
                pv_global_session_cd,
                pv_device_name,
                pt_call_in_start_ts,
                pv_device_serial_cd,
                CASE WHEN pn_device_type_id = -1 THEN NULL ELSE pn_device_type_id END,
                pn_net_layer_id,
                pv_remote_address,
                pn_remote_port,
                pv_location_name, 
                pv_customer_name,
                pn_terminal_id,
                pn_customer_id,
				pc_comm_method_cd
            );
            EXIT;
        EXCEPTION 
            WHEN unique_violation THEN
                NULL;
            WHEN serialization_failure THEN
                NULL;
        END;
    END LOOP;
   	
	-- Mark any previous started sessions as aborted
	LOOP
        BEGIN
		    UPDATE MAIN.DEVICE_SESSION DS
			   SET SESSION_STATE_ID = 3,
			       CALL_IN_STATUS = CASE CALL_IN_STATUS WHEN 'I' THEN 'U' ELSE CALL_IN_STATUS END
			 WHERE DEVICE_NAME = pv_device_name
			   AND SESSION_STATE_ID = 1
			   AND GLOBAL_SESSION_CD != pv_global_session_cd
			   AND CALL_IN_START_TS < COALESCE(pt_call_in_start_ts, CURRENT_TIMESTAMP);
            EXIT;
        EXCEPTION 
            WHEN serialization_failure THEN
                NULL;
        END;
    END LOOP;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MAIN.UPDATE_SESSION_START(
      pv_global_session_cd MAIN.DEVICE_SESSION.GLOBAL_SESSION_CD%TYPE,
      pv_device_name MAIN.DEVICE_SESSION.DEVICE_NAME%TYPE,
      pv_device_serial_cd MAIN.DEVICE_SESSION.DEVICE_SERIAL_CD%TYPE,
	  pn_device_type_id MAIN.DEVICE_SESSION.DEVICE_TYPE_ID%TYPE,
      pn_net_layer_id MAIN.DEVICE_SESSION.NET_LAYER_ID%TYPE,
      pv_remote_address MAIN.DEVICE_SESSION.CLIENT_IP_ADDRESS%TYPE,
      pn_remote_port MAIN.DEVICE_SESSION.CLIENT_PORT%TYPE,
	  pt_call_in_start_ts MAIN.DEVICE_SESSION.CALL_IN_START_TS%TYPE,
      pn_terminal_id MAIN.DEVICE_SESSION.TERMINAL_ID%TYPE, 
      pn_customer_id MAIN.DEVICE_SESSION.CUSTOMER_ID%TYPE, 
      pv_location_name MAIN.DEVICE_SESSION.LOCATION_NAME%TYPE,
      pv_customer_name MAIN.DEVICE_SESSION.CUSTOMER_NAME%TYPE,
	  pc_comm_method_cd MAIN.DEVICE_SESSION.COMM_METHOD_CD%TYPE
	  ) TO write_main;	  

--R46 version
CREATE OR REPLACE FUNCTION MAIN.UPDATE_SESSION_START(
      pv_global_session_cd MAIN.DEVICE_SESSION.GLOBAL_SESSION_CD%TYPE,
      pv_device_name MAIN.DEVICE_SESSION.DEVICE_NAME%TYPE,
      pv_device_serial_cd MAIN.DEVICE_SESSION.DEVICE_SERIAL_CD%TYPE,
      pn_device_type_id MAIN.DEVICE_SESSION.DEVICE_TYPE_ID%TYPE,
      pn_net_layer_id MAIN.DEVICE_SESSION.NET_LAYER_ID%TYPE,
      pv_remote_address MAIN.DEVICE_SESSION.CLIENT_IP_ADDRESS%TYPE,
      pn_remote_port MAIN.DEVICE_SESSION.CLIENT_PORT%TYPE,
      pt_call_in_start_ts MAIN.DEVICE_SESSION.CALL_IN_START_TS%TYPE,
      pn_terminal_id MAIN.DEVICE_SESSION.TERMINAL_ID%TYPE, 
      pn_customer_id MAIN.DEVICE_SESSION.CUSTOMER_ID%TYPE, 
      pv_location_name MAIN.DEVICE_SESSION.LOCATION_NAME%TYPE,
      pv_customer_name MAIN.DEVICE_SESSION.CUSTOMER_NAME%TYPE,
      pc_comm_method_cd MAIN.DEVICE_SESSION.COMM_METHOD_CD%TYPE,
      pv_comments MAIN.DEVICE_SESSION.COMMENTS%TYPE
      )
    RETURNS VOID
    SECURITY DEFINER
AS $$
BEGIN
    IF pv_global_session_cd IS NULL THEN
        RAISE EXCEPTION 'Invalid pv_global_session_cd: %', pv_global_session_cd USING ERRCODE = '20701';
    END IF;

    IF pv_device_name IS NULL THEN
        RAISE EXCEPTION 'Invalid pv_device_name: %', pv_device_name USING ERRCODE = '20701';
    END IF;
    
    LOOP
        BEGIN
            UPDATE MAIN.DEVICE_SESSION
               SET DEVICE_SERIAL_CD = CASE WHEN DEVICE_SERIAL_CD IS NULL THEN pv_device_serial_cd ELSE DEVICE_SERIAL_CD END,
                   DEVICE_TYPE_ID = CASE WHEN pn_device_type_id = -1 THEN DEVICE_TYPE_ID WHEN DEVICE_TYPE_ID IS NULL THEN pn_device_type_id ELSE DEVICE_TYPE_ID END,
                   NET_LAYER_ID = pn_net_layer_id,
                   CLIENT_IP_ADDRESS = pv_remote_address,
                   CLIENT_PORT = pn_remote_port,
                   CALL_IN_START_TS = pt_call_in_start_ts,
                   LOCATION_NAME = pv_location_name,
                   CUSTOMER_NAME = pv_customer_name,
                   TERMINAL_ID = pn_terminal_id,
                   CUSTOMER_ID = pn_customer_id,
                   COMM_METHOD_CD = pc_comm_method_cd,
                   COMMENTS = pv_comments
             WHERE GLOBAL_SESSION_CD = pv_global_session_cd;
            EXIT WHEN FOUND;
            INSERT INTO MAIN.DEVICE_SESSION(
                GLOBAL_SESSION_CD, 
                DEVICE_NAME,
                CALL_IN_START_TS,
                DEVICE_SERIAL_CD,
                DEVICE_TYPE_ID,
                NET_LAYER_ID,
                CLIENT_IP_ADDRESS,
                CLIENT_PORT,
                LOCATION_NAME,
                CUSTOMER_NAME,
                TERMINAL_ID,
                CUSTOMER_ID,
                COMM_METHOD_CD,
                COMMENTS
            ) VALUES (
                pv_global_session_cd,
                pv_device_name,
                pt_call_in_start_ts,
                pv_device_serial_cd,
                CASE WHEN pn_device_type_id = -1 THEN NULL ELSE pn_device_type_id END,
                pn_net_layer_id,
                pv_remote_address,
                pn_remote_port,
                pv_location_name, 
                pv_customer_name,
                pn_terminal_id,
                pn_customer_id,
                pc_comm_method_cd,
                pv_comments
            );
            EXIT;
        EXCEPTION 
            WHEN unique_violation THEN
                NULL;
            WHEN serialization_failure THEN
                NULL;
        END;
    END LOOP;
    
    -- Mark any previous started sessions as aborted
    LOOP
        BEGIN
            UPDATE MAIN.DEVICE_SESSION DS
               SET SESSION_STATE_ID = 3,
                   CALL_IN_STATUS = CASE CALL_IN_STATUS WHEN 'I' THEN 'U' ELSE CALL_IN_STATUS END
             WHERE DEVICE_NAME = pv_device_name
               AND SESSION_STATE_ID = 1
               AND GLOBAL_SESSION_CD != pv_global_session_cd
               AND CALL_IN_START_TS < COALESCE(pt_call_in_start_ts, CURRENT_TIMESTAMP);
            EXIT;
        EXCEPTION 
            WHEN serialization_failure THEN
                NULL;
        END;
    END LOOP;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MAIN.UPDATE_SESSION_START(
      pv_global_session_cd MAIN.DEVICE_SESSION.GLOBAL_SESSION_CD%TYPE,
      pv_device_name MAIN.DEVICE_SESSION.DEVICE_NAME%TYPE,
      pv_device_serial_cd MAIN.DEVICE_SESSION.DEVICE_SERIAL_CD%TYPE,
      pn_device_type_id MAIN.DEVICE_SESSION.DEVICE_TYPE_ID%TYPE,
      pn_net_layer_id MAIN.DEVICE_SESSION.NET_LAYER_ID%TYPE,
      pv_remote_address MAIN.DEVICE_SESSION.CLIENT_IP_ADDRESS%TYPE,
      pn_remote_port MAIN.DEVICE_SESSION.CLIENT_PORT%TYPE,
      pt_call_in_start_ts MAIN.DEVICE_SESSION.CALL_IN_START_TS%TYPE,
      pn_terminal_id MAIN.DEVICE_SESSION.TERMINAL_ID%TYPE, 
      pn_customer_id MAIN.DEVICE_SESSION.CUSTOMER_ID%TYPE, 
      pv_location_name MAIN.DEVICE_SESSION.LOCATION_NAME%TYPE,
      pv_customer_name MAIN.DEVICE_SESSION.CUSTOMER_NAME%TYPE,
      pc_comm_method_cd MAIN.DEVICE_SESSION.COMM_METHOD_CD%TYPE,
      pv_comments MAIN.DEVICE_SESSION.COMMENTS%TYPE
      ) to write_main;
      
CREATE OR REPLACE FUNCTION MAIN.UPDATE_SESSION_END(
	pv_global_session_cd MAIN.DEVICE_SESSION.GLOBAL_SESSION_CD%TYPE,
	pv_device_name MAIN.DEVICE_SESSION.DEVICE_NAME%TYPE,
	pv_device_serial_cd MAIN.DEVICE_SESSION.DEVICE_SERIAL_CD%TYPE,
	pn_device_type_id MAIN.DEVICE_SESSION.DEVICE_TYPE_ID%TYPE,
	pn_bytes_received MAIN.DEVICE_SESSION.INBOUND_BYTE_COUNT%TYPE,
	pn_bytes_sent MAIN.DEVICE_SESSION.OUTBOUND_BYTE_COUNT%TYPE,
	pn_messages_received MAIN.DEVICE_SESSION.INBOUND_MESSAGE_COUNT%TYPE,
	pn_messages_sent MAIN.DEVICE_SESSION.OUTBOUND_MESSAGE_COUNT%TYPE,
	pt_call_in_start_ts MAIN.DEVICE_SESSION.CALL_IN_START_TS%TYPE,
    pt_call_in_finish_ts MAIN.DEVICE_SESSION.CALL_IN_FINISH_TS%TYPE,
	pc_initialized MAIN.DEVICE_SESSION.DEVICE_INITIALIZED_FLAG%TYPE,
	pc_device_sent_config MAIN.DEVICE_SESSION.DEVICE_SENT_CONFIG_FLAG%TYPE,
	pc_server_sent_config MAIN.DEVICE_SESSION.SERVER_SENT_CONFIG_FLAG%TYPE,
	pn_dex_file_size MAIN.DEVICE_SESSION.DEX_FILE_SIZE%TYPE,
	pn_auth_amt MAIN.DEVICE_SESSION.AUTH_AMOUNT%TYPE,
	pc_auth_result_cd CHAR,
	pc_payment_type MAIN.DEVICE_SESSION.AUTH_CARD_TYPE%TYPE,
	pd_auth_ts MAIN.DEVICE_SESSION.LAST_AUTH_IN_TS%TYPE,
	pc_call_in_type MAIN.DEVICE_SESSION.CALL_IN_TYPE%TYPE,
	pc_call_in_status MAIN.DEVICE_SESSION.CALL_IN_STATUS%TYPE)
    RETURNS VOID
	SECURITY DEFINER
AS $$
BEGIN
	-- R33 version
	LOOP
        BEGIN
            UPDATE MAIN.DEVICE_SESSION
	           SET DEVICE_NAME = pv_device_name,
				   DEVICE_SERIAL_CD = CASE WHEN DEVICE_SERIAL_CD IS NULL THEN pv_device_serial_cd ELSE DEVICE_SERIAL_CD END,
	               DEVICE_TYPE_ID = CASE WHEN pn_device_type_id = -1 THEN DEVICE_TYPE_ID WHEN DEVICE_TYPE_ID IS NULL THEN pn_device_type_id ELSE DEVICE_TYPE_ID END,
	               SESSION_STATE_ID = CASE pc_call_in_status WHEN 'S' THEN 2 WHEN 'M' THEN 2 ELSE 3 END,
	               INBOUND_BYTE_COUNT = pn_bytes_received,
	               OUTBOUND_BYTE_COUNT = pn_bytes_sent,
	               INBOUND_MESSAGE_COUNT = pn_messages_received,
	               OUTBOUND_MESSAGE_COUNT = pn_messages_sent,
	               CALL_IN_FINISH_TS = pt_call_in_finish_ts,
	               DEVICE_INITIALIZED_FLAG = pc_initialized,
	               DEVICE_SENT_CONFIG_FLAG = pc_device_sent_config,
	               SERVER_SENT_CONFIG_FLAG = pc_server_sent_config,
	               DEX_FILE_SIZE = pn_dex_file_size,
	               DEX_RECEIVED_FLAG = CASE WHEN pn_dex_file_size IS NULL THEN NULL::CHAR WHEN pn_dex_file_size <= 0 THEN 'N' ELSE 'Y' END,
	               AUTH_AMOUNT = pn_auth_amt,
	               AUTH_APPROVED_FLAG = CASE pc_auth_result_cd WHEN NULL THEN NULL::CHAR WHEN 'Y' THEN 'Y' WHEN 'N' THEN 'N' WHEN 'P' THEN 'Y' WHEN 'O' THEN 'N' WHEN 'F' THEN 'N' END,
	               AUTH_CARD_TYPE = pc_payment_type,
	               LAST_AUTH_IN_TS = pd_auth_ts,
	               CALL_IN_TYPE = pc_call_in_type,
	               CALL_IN_STATUS = pc_call_in_status
	         WHERE GLOBAL_SESSION_CD = pv_global_session_cd;
	        EXIT WHEN FOUND;
            INSERT INTO MAIN.DEVICE_SESSION(
                GLOBAL_SESSION_CD, 
                DEVICE_NAME,
                CALL_IN_START_TS,
                DEVICE_SERIAL_CD,
                DEVICE_TYPE_ID,
                SESSION_STATE_ID,
                INBOUND_BYTE_COUNT,
                OUTBOUND_BYTE_COUNT,
                INBOUND_MESSAGE_COUNT,
                OUTBOUND_MESSAGE_COUNT,
                CALL_IN_FINISH_TS,
			    DEVICE_INITIALIZED_FLAG,
			    DEVICE_SENT_CONFIG_FLAG,
			    SERVER_SENT_CONFIG_FLAG,
			    DEX_FILE_SIZE,
			    DEX_RECEIVED_FLAG,
			    AUTH_AMOUNT,
			    AUTH_APPROVED_FLAG,
			    AUTH_CARD_TYPE,
			    LAST_AUTH_IN_TS,
			    CALL_IN_TYPE,
			    CALL_IN_STATUS
            ) VALUES (
                pv_global_session_cd,
                pv_device_name,
                COALESCE(pt_call_in_start_ts, pt_call_in_finish_ts),
                pv_device_serial_cd,
                CASE WHEN pn_device_type_id = -1 THEN NULL ELSE pn_device_type_id END,
                CASE pc_call_in_status WHEN 'S' THEN 2 WHEN 'M' THEN 2 ELSE 3 END,
                pn_bytes_received,
                pn_bytes_sent,
                pn_messages_received,
                pn_messages_sent,
                pt_call_in_finish_ts,
                pc_initialized,
                pc_device_sent_config,
                pc_server_sent_config,
                pn_dex_file_size,
                CASE WHEN pn_dex_file_size IS NULL THEN NULL::CHAR WHEN pn_dex_file_size <= 0 THEN 'N' ELSE 'Y' END,
                pn_auth_amt,
                CASE pc_auth_result_cd WHEN NULL THEN NULL::CHAR WHEN 'Y' THEN 'Y' WHEN 'N' THEN 'N' WHEN 'P' THEN 'Y' WHEN 'O' THEN 'N' WHEN 'F' THEN 'N' END,
                pc_payment_type,
                pd_auth_ts,
                pc_call_in_type,
                pc_call_in_status
            );
            EXIT;
        EXCEPTION 
            WHEN unique_violation THEN
                NULL;
            WHEN serialization_failure THEN
                NULL;
        END;
    END LOOP;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MAIN.UPDATE_SESSION_END(
	pv_global_session_cd MAIN.DEVICE_SESSION.GLOBAL_SESSION_CD%TYPE,
	pv_device_name MAIN.DEVICE_SESSION.DEVICE_NAME%TYPE,
	pv_device_serial_cd MAIN.DEVICE_SESSION.DEVICE_SERIAL_CD%TYPE,
	pn_device_type_id MAIN.DEVICE_SESSION.DEVICE_TYPE_ID%TYPE,
	pn_bytes_received MAIN.DEVICE_SESSION.INBOUND_BYTE_COUNT%TYPE,
	pn_bytes_sent MAIN.DEVICE_SESSION.OUTBOUND_BYTE_COUNT%TYPE,
	pn_messages_received MAIN.DEVICE_SESSION.INBOUND_MESSAGE_COUNT%TYPE,
	pn_messages_sent MAIN.DEVICE_SESSION.OUTBOUND_MESSAGE_COUNT%TYPE,
	pt_call_in_start_ts MAIN.DEVICE_SESSION.CALL_IN_START_TS%TYPE,
    pt_call_in_finish_ts MAIN.DEVICE_SESSION.CALL_IN_FINISH_TS%TYPE,
	pc_initialized MAIN.DEVICE_SESSION.DEVICE_INITIALIZED_FLAG%TYPE,
	pc_device_sent_config MAIN.DEVICE_SESSION.DEVICE_SENT_CONFIG_FLAG%TYPE,
	pc_server_sent_config MAIN.DEVICE_SESSION.SERVER_SENT_CONFIG_FLAG%TYPE,
	pn_dex_file_size MAIN.DEVICE_SESSION.DEX_FILE_SIZE%TYPE,
	pn_auth_amt MAIN.DEVICE_SESSION.AUTH_AMOUNT%TYPE,
	pc_auth_result_cd CHAR,
	pc_payment_type MAIN.DEVICE_SESSION.AUTH_CARD_TYPE%TYPE,
	pd_auth_ts MAIN.DEVICE_SESSION.LAST_AUTH_IN_TS%TYPE,
	pc_call_in_type MAIN.DEVICE_SESSION.CALL_IN_TYPE%TYPE,
	pc_call_in_status MAIN.DEVICE_SESSION.CALL_IN_STATUS%TYPE) TO write_main;
	
CREATE OR REPLACE FUNCTION MAIN.UPDATE_SESSION_END(
	pv_global_session_cd MAIN.DEVICE_SESSION.GLOBAL_SESSION_CD%TYPE,
	pv_device_name MAIN.DEVICE_SESSION.DEVICE_NAME%TYPE,
	pv_device_serial_cd MAIN.DEVICE_SESSION.DEVICE_SERIAL_CD%TYPE,
	pn_device_type_id MAIN.DEVICE_SESSION.DEVICE_TYPE_ID%TYPE,
	pn_bytes_received MAIN.DEVICE_SESSION.INBOUND_BYTE_COUNT%TYPE,
	pn_bytes_sent MAIN.DEVICE_SESSION.OUTBOUND_BYTE_COUNT%TYPE,
	pn_messages_received MAIN.DEVICE_SESSION.INBOUND_MESSAGE_COUNT%TYPE,
	pn_messages_sent MAIN.DEVICE_SESSION.OUTBOUND_MESSAGE_COUNT%TYPE,
	pt_call_in_start_ts MAIN.DEVICE_SESSION.CALL_IN_START_TS%TYPE,
    pt_call_in_finish_ts MAIN.DEVICE_SESSION.CALL_IN_FINISH_TS%TYPE,
	pc_initialized MAIN.DEVICE_SESSION.DEVICE_INITIALIZED_FLAG%TYPE,
	pc_device_sent_config MAIN.DEVICE_SESSION.DEVICE_SENT_CONFIG_FLAG%TYPE,
	pc_server_sent_config MAIN.DEVICE_SESSION.SERVER_SENT_CONFIG_FLAG%TYPE,
	pn_dex_file_size MAIN.DEVICE_SESSION.DEX_FILE_SIZE%TYPE,
	pn_auth_amt MAIN.DEVICE_SESSION.AUTH_AMOUNT%TYPE,
	pc_auth_result_cd CHAR,
	pc_payment_type MAIN.DEVICE_SESSION.AUTH_CARD_TYPE%TYPE,
	pd_auth_ts MAIN.DEVICE_SESSION.LAST_AUTH_IN_TS%TYPE,
	pc_call_in_type MAIN.DEVICE_SESSION.CALL_IN_TYPE%TYPE,
	pc_call_in_status MAIN.DEVICE_SESSION.CALL_IN_STATUS%TYPE,
	pv_firmware_version MAIN.DEVICE_SESSION.FIRMWARE_VERSION%TYPE)
    RETURNS VOID
	SECURITY DEFINER
AS $$
BEGIN
	-- R34+ version
	LOOP
        BEGIN
            UPDATE MAIN.DEVICE_SESSION
	           SET DEVICE_NAME = pv_device_name,
				   DEVICE_SERIAL_CD = CASE WHEN DEVICE_SERIAL_CD IS NULL THEN pv_device_serial_cd ELSE DEVICE_SERIAL_CD END,
	               DEVICE_TYPE_ID = CASE WHEN pn_device_type_id = -1 THEN DEVICE_TYPE_ID WHEN DEVICE_TYPE_ID IS NULL THEN pn_device_type_id ELSE DEVICE_TYPE_ID END,
	               SESSION_STATE_ID = CASE pc_call_in_status WHEN 'S' THEN 2 WHEN 'M' THEN 2 ELSE 3 END,
	               INBOUND_BYTE_COUNT = pn_bytes_received,
	               OUTBOUND_BYTE_COUNT = pn_bytes_sent,
	               INBOUND_MESSAGE_COUNT = pn_messages_received,
	               OUTBOUND_MESSAGE_COUNT = pn_messages_sent,
	               CALL_IN_FINISH_TS = pt_call_in_finish_ts,
	               DEVICE_INITIALIZED_FLAG = pc_initialized,
	               DEVICE_SENT_CONFIG_FLAG = pc_device_sent_config,
	               SERVER_SENT_CONFIG_FLAG = pc_server_sent_config,
	               DEX_FILE_SIZE = pn_dex_file_size,
	               DEX_RECEIVED_FLAG = CASE WHEN pn_dex_file_size IS NULL THEN NULL::CHAR WHEN pn_dex_file_size <= 0 THEN 'N' ELSE 'Y' END,
	               AUTH_AMOUNT = pn_auth_amt,
	               AUTH_APPROVED_FLAG = CASE pc_auth_result_cd WHEN NULL THEN NULL::CHAR WHEN 'Y' THEN 'Y' WHEN 'N' THEN 'N' WHEN 'P' THEN 'Y' WHEN 'O' THEN 'N' WHEN 'F' THEN 'N' END,
	               AUTH_CARD_TYPE = pc_payment_type,
	               LAST_AUTH_IN_TS = pd_auth_ts,
	               CALL_IN_TYPE = pc_call_in_type,
	               CALL_IN_STATUS = pc_call_in_status,
				   FIRMWARE_VERSION = pv_firmware_version
	         WHERE GLOBAL_SESSION_CD = pv_global_session_cd;
	        EXIT WHEN FOUND;
            INSERT INTO MAIN.DEVICE_SESSION(
                GLOBAL_SESSION_CD, 
                DEVICE_NAME,
                CALL_IN_START_TS,
                DEVICE_SERIAL_CD,
                DEVICE_TYPE_ID,
                SESSION_STATE_ID,
                INBOUND_BYTE_COUNT,
                OUTBOUND_BYTE_COUNT,
                INBOUND_MESSAGE_COUNT,
                OUTBOUND_MESSAGE_COUNT,
                CALL_IN_FINISH_TS,
			    DEVICE_INITIALIZED_FLAG,
			    DEVICE_SENT_CONFIG_FLAG,
			    SERVER_SENT_CONFIG_FLAG,
			    DEX_FILE_SIZE,
			    DEX_RECEIVED_FLAG,
			    AUTH_AMOUNT,
			    AUTH_APPROVED_FLAG,
			    AUTH_CARD_TYPE,
			    LAST_AUTH_IN_TS,
			    CALL_IN_TYPE,
			    CALL_IN_STATUS,
				FIRMWARE_VERSION
            ) VALUES (
                pv_global_session_cd,
                pv_device_name,
                COALESCE(pt_call_in_start_ts, pt_call_in_finish_ts),
                pv_device_serial_cd,
                CASE WHEN pn_device_type_id = -1 THEN NULL ELSE pn_device_type_id END,
                CASE pc_call_in_status WHEN 'S' THEN 2 WHEN 'M' THEN 2 ELSE 3 END,
                pn_bytes_received,
                pn_bytes_sent,
                pn_messages_received,
                pn_messages_sent,
                pt_call_in_finish_ts,
                pc_initialized,
                pc_device_sent_config,
                pc_server_sent_config,
                pn_dex_file_size,
                CASE WHEN pn_dex_file_size IS NULL THEN NULL::CHAR WHEN pn_dex_file_size <= 0 THEN 'N' ELSE 'Y' END,
                pn_auth_amt,
                CASE pc_auth_result_cd WHEN NULL THEN NULL::CHAR WHEN 'Y' THEN 'Y' WHEN 'N' THEN 'N' WHEN 'P' THEN 'Y' WHEN 'O' THEN 'N' WHEN 'F' THEN 'N' END,
                pc_payment_type,
                pd_auth_ts,
                pc_call_in_type,
                pc_call_in_status,
				pv_firmware_version
            );
            EXIT;
        EXCEPTION 
            WHEN unique_violation THEN
                NULL;
            WHEN serialization_failure THEN
                NULL;
        END;
    END LOOP;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MAIN.UPDATE_SESSION_END(
	pv_global_session_cd MAIN.DEVICE_SESSION.GLOBAL_SESSION_CD%TYPE,
	pv_device_name MAIN.DEVICE_SESSION.DEVICE_NAME%TYPE,
	pv_device_serial_cd MAIN.DEVICE_SESSION.DEVICE_SERIAL_CD%TYPE,
	pn_device_type_id MAIN.DEVICE_SESSION.DEVICE_TYPE_ID%TYPE,
	pn_bytes_received MAIN.DEVICE_SESSION.INBOUND_BYTE_COUNT%TYPE,
	pn_bytes_sent MAIN.DEVICE_SESSION.OUTBOUND_BYTE_COUNT%TYPE,
	pn_messages_received MAIN.DEVICE_SESSION.INBOUND_MESSAGE_COUNT%TYPE,
	pn_messages_sent MAIN.DEVICE_SESSION.OUTBOUND_MESSAGE_COUNT%TYPE,
	pt_call_in_start_ts MAIN.DEVICE_SESSION.CALL_IN_START_TS%TYPE,
    pt_call_in_finish_ts MAIN.DEVICE_SESSION.CALL_IN_FINISH_TS%TYPE,
	pc_initialized MAIN.DEVICE_SESSION.DEVICE_INITIALIZED_FLAG%TYPE,
	pc_device_sent_config MAIN.DEVICE_SESSION.DEVICE_SENT_CONFIG_FLAG%TYPE,
	pc_server_sent_config MAIN.DEVICE_SESSION.SERVER_SENT_CONFIG_FLAG%TYPE,
	pn_dex_file_size MAIN.DEVICE_SESSION.DEX_FILE_SIZE%TYPE,
	pn_auth_amt MAIN.DEVICE_SESSION.AUTH_AMOUNT%TYPE,
	pc_auth_result_cd CHAR,
	pc_payment_type MAIN.DEVICE_SESSION.AUTH_CARD_TYPE%TYPE,
	pd_auth_ts MAIN.DEVICE_SESSION.LAST_AUTH_IN_TS%TYPE,
	pc_call_in_type MAIN.DEVICE_SESSION.CALL_IN_TYPE%TYPE,
	pc_call_in_status MAIN.DEVICE_SESSION.CALL_IN_STATUS%TYPE,
	pv_firmware_version MAIN.DEVICE_SESSION.FIRMWARE_VERSION%TYPE) TO write_main;	
	
CREATE OR REPLACE FUNCTION MAIN.INSERT_MESSAGE_PROCESSED (
        pv_global_session_cd MAIN.DEVICE_MESSAGE.GLOBAL_SESSION_CD%TYPE,
        pn_message_sequence MAIN.DEVICE_MESSAGE.MESSAGE_SEQUENCE%TYPE,
		pn_net_layer_id MAIN.DEVICE_MESSAGE.NET_LAYER_ID%TYPE,        
        pv_device_name MAIN.DEVICE_MESSAGE.DEVICE_NAME%TYPE,
        pc_reply_complete CHAR,
        pc_reply_success CHAR,
        pv_inbound_message_type MAIN.DEVICE_MESSAGE.INBOUND_MESSAGE_TYPE%TYPE,
        pv_inbound_message MAIN.DEVICE_MESSAGE.INBOUND_MESSAGE%TYPE,
        pv_outbound_message_type MAIN.DEVICE_MESSAGE.OUTBOUND_MESSAGE_TYPE%TYPE,
        pv_outbound_message MAIN.DEVICE_MESSAGE.OUTBOUND_MESSAGE%TYPE,
		pn_message_start_time BIGINT,
        pn_message_end_time BIGINT,
        pt_inbound_message_ts MAIN.DEVICE_MESSAGE.INBOUND_MESSAGE_TS%TYPE,
        pt_outbound_message_ts MAIN.DEVICE_MESSAGE.OUTBOUND_MESSAGE_TS%TYPE,
        pn_inbound_message_length MAIN.DEVICE_MESSAGE.INBOUND_MESSAGE_LENGTH%TYPE,
        pn_outbound_message_length MAIN.DEVICE_MESSAGE.OUTBOUND_MESSAGE_LENGTH%TYPE,
		pv_device_serial_cd MAIN.DEVICE_MESSAGE.DEVICE_SERIAL_CD%TYPE,
        pc_check_for_dup CHAR DEFAULT 'Y'
    ) RETURNS VOID
	SECURITY DEFINER
AS $$
DECLARE
	ln_cnt INT;
BEGIN
	IF UPPER(pc_reply_complete) = 'T' THEN
		IF pc_check_for_dup = 'Y' THEN
			SELECT COUNT(*)
			 INTO ln_cnt
			 FROM MAIN.DEVICE_MESSAGE
			WHERE GLOBAL_SESSION_CD = pv_global_session_cd
			  AND MESSAGE_SEQUENCE = pn_message_sequence;
			IF ln_cnt > 0 THEN
				RETURN;
			END IF;
		END IF;
		INSERT INTO MAIN.DEVICE_MESSAGE(
			GLOBAL_SESSION_CD,
			MESSAGE_SEQUENCE,
			INBOUND_MESSAGE_TYPE,
			INBOUND_MESSAGE,
			OUTBOUND_MESSAGE_TYPE,
			OUTBOUND_MESSAGE,
			MESSAGE_TRANSMITTED_FLAG,
			MESSAGE_DURATION_MILLIS,
			INBOUND_MESSAGE_LENGTH,
			OUTBOUND_MESSAGE_LENGTH,
			DEVICE_NAME,
			NET_LAYER_ID,
			INBOUND_MESSAGE_TS,
			OUTBOUND_MESSAGE_TS,
			DEVICE_SERIAL_CD
		) VALUES (
			pv_global_session_cd,
			pn_message_sequence,
			pv_inbound_message_type,
			pv_inbound_message,
			pv_outbound_message_type,
			pv_outbound_message,
			CASE UPPER(pc_reply_success) WHEN 'T' THEN 'Y' ELSE 'N' END,
			pn_message_end_time - pn_message_start_time,
			pn_inbound_message_length,
			pn_outbound_message_length,
			pv_device_name,
			pn_net_layer_id,
			pt_inbound_message_ts,
			pt_outbound_message_ts,
			pv_device_serial_cd
		);
	END IF;
EXCEPTION 
    WHEN unique_violation THEN
        NULL;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MAIN.INSERT_MESSAGE_PROCESSED (
        pv_global_session_cd MAIN.DEVICE_MESSAGE.GLOBAL_SESSION_CD%TYPE,
        pn_message_sequence MAIN.DEVICE_MESSAGE.MESSAGE_SEQUENCE%TYPE,
		pn_net_layer_id MAIN.DEVICE_MESSAGE.NET_LAYER_ID%TYPE,        
        pv_device_name MAIN.DEVICE_MESSAGE.DEVICE_NAME%TYPE,
        pc_reply_complete CHAR,
        pc_reply_success CHAR,
        pv_inbound_message_type MAIN.DEVICE_MESSAGE.INBOUND_MESSAGE_TYPE%TYPE,
        pv_inbound_message MAIN.DEVICE_MESSAGE.INBOUND_MESSAGE%TYPE,
        pv_outbound_message_type MAIN.DEVICE_MESSAGE.OUTBOUND_MESSAGE_TYPE%TYPE,
        pv_outbound_message MAIN.DEVICE_MESSAGE.OUTBOUND_MESSAGE%TYPE,
		pn_message_start_time BIGINT,
        pn_message_end_time BIGINT,
        pt_inbound_message_ts MAIN.DEVICE_MESSAGE.INBOUND_MESSAGE_TS%TYPE,
        pt_outbound_message_ts MAIN.DEVICE_MESSAGE.OUTBOUND_MESSAGE_TS%TYPE,
        pn_inbound_message_length MAIN.DEVICE_MESSAGE.INBOUND_MESSAGE_LENGTH%TYPE,
        pn_outbound_message_length MAIN.DEVICE_MESSAGE.OUTBOUND_MESSAGE_LENGTH%TYPE,
		pv_device_serial_cd MAIN.DEVICE_MESSAGE.DEVICE_SERIAL_CD%TYPE,
        pc_check_for_dup CHAR
    ) TO write_main;

CREATE OR REPLACE FUNCTION MAIN.GET_OR_CREATE_NET_LAYER(
        lv_net_layer_desc MAIN.NET_LAYER.NET_LAYER_DESC%TYPE,
        lv_server_name MAIN.NET_LAYER.SERVER_NAME%TYPE,
        ln_server_port_num MAIN.NET_LAYER.SERVER_PORT_NUM%TYPE)
      RETURNS MAIN.NET_LAYER.NET_LAYER_ID%TYPE
	  SECURITY DEFINER
AS $$    
DECLARE
	ln_net_layer_id MAIN.NET_LAYER.NET_LAYER_ID%TYPE;
BEGIN
	SELECT MAX(NET_LAYER_ID)
	INTO ln_net_layer_id
	FROM MAIN.NET_LAYER
	WHERE NET_LAYER_DESC = lv_net_layer_desc;

	IF ln_net_layer_id IS NULL THEN
		BEGIN
			INSERT INTO MAIN.NET_LAYER(
				NET_LAYER_DESC,
				SERVER_NAME,
				SERVER_PORT_NUM,
				NET_LAYER_ACTIVE_YN_FLAG
			) VALUES (
				lv_net_layer_desc,
				lv_server_name,
				ln_server_port_num,
				'Y'
			) RETURNING NET_LAYER_ID INTO ln_net_layer_id;
		EXCEPTION 
            WHEN unique_violation THEN
				SELECT MAX(NET_LAYER_ID)
				  INTO ln_net_layer_id
				  FROM MAIN.NET_LAYER
				 WHERE NET_LAYER_DESC = lv_net_layer_desc;
		END;
	END IF;
	
	RETURN ln_net_layer_id;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MAIN.GET_OR_CREATE_NET_LAYER(
        lv_net_layer_desc MAIN.NET_LAYER.NET_LAYER_DESC%TYPE,
        lv_server_name MAIN.NET_LAYER.SERVER_NAME%TYPE,
        ln_server_port_num MAIN.NET_LAYER.SERVER_PORT_NUM%TYPE) TO write_main;

CREATE OR REPLACE FUNCTION MAIN.GET_OR_CREATE_DEVICE_SESSION(
    pv_global_session_cd MAIN.DEVICE_SESSION.GLOBAL_SESSION_CD%TYPE,
    pv_device_name MAIN.DEVICE_SESSION.DEVICE_NAME%TYPE,
    pt_call_in_start_ts MAIN.DEVICE_SESSION.CALL_IN_START_TS%TYPE)
      RETURNS VOID
      SECURITY DEFINER
AS $$    
DECLARE
    ln_cnt INTEGER;
BEGIN
	LOOP
	    SELECT COUNT(*)
	      INTO ln_cnt
	      FROM MAIN.DEVICE_SESSION
	     WHERE GLOBAL_SESSION_CD = pv_global_session_cd;
        EXIT WHEN ln_cnt > 0;
        BEGIN
            INSERT INTO MAIN.DEVICE_SESSION(
                GLOBAL_SESSION_CD, 
                DEVICE_NAME,
                CALL_IN_START_TS
            ) VALUES (
                pv_global_session_cd,
                pv_device_name,
                COALESCE(pt_call_in_start_ts, CURRENT_TIMESTAMP)
            );
            EXIT;
        EXCEPTION 
            WHEN unique_violation THEN
                NULL;
        END;
    END LOOP;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MAIN.ADD_TRAN_TO_DEVICE_SESSION (
	pv_global_session_cd main.device_session.global_session_cd%TYPE,
	pv_device_name main.device_session.device_name%TYPE,
	pt_call_in_start_ts MAIN.DEVICE_SESSION.CALL_IN_START_TS%TYPE,
	pc_card_type main.device_session.auth_card_type%TYPE,
	pn_sale_amount BIGINT,
	pn_minor_currency_factor INTEGER,
	pn_vend_count main.device_session.credit_vend_count%TYPE
   ) RETURNS VOID
	SECURITY DEFINER
AS $$
DECLARE
	ln_trans_amount DECIMAL(14,2) := CAST(pn_sale_amount AS DECIMAL(14,2)) / pn_minor_currency_factor;
BEGIN
	-- R33 version
	PERFORM MAIN.GET_OR_CREATE_DEVICE_SESSION(
        pv_global_session_cd,
        pv_device_name,
        pt_call_in_start_ts);
	LOOP
        BEGIN
			IF pc_card_type IN ('C', 'R', 'N') THEN
				UPDATE main.device_session
					SET credit_trans_count = COALESCE(credit_trans_count, 0) + 1,
						credit_trans_total = COALESCE(credit_trans_total, 0) + ln_trans_amount,
						credit_vend_count = COALESCE(credit_vend_count, 0) + pn_vend_count,
						last_trans_in_ts = CURRENT_TIMESTAMP,
						call_in_type = CASE WHEN call_in_type IN ('A', 'C') THEN 'C' ELSE 'B' END
				WHERE global_session_cd = pv_global_session_cd;
			ELSIF pc_card_type = 'M' THEN
				UPDATE main.device_session
					SET cash_trans_count = COALESCE(cash_trans_count, 0) + 1,
						cash_trans_total = COALESCE(cash_trans_total, 0) + ln_trans_amount,
						cash_vend_count = COALESCE(cash_vend_count, 0) + pn_vend_count,
						last_trans_in_ts = CURRENT_TIMESTAMP,
						call_in_type = CASE WHEN call_in_type IN ('A', 'C') THEN 'C' ELSE 'B' END
				WHERE global_session_cd = pv_global_session_cd;
			ELSIF pc_card_type IN ('S', 'P', 'T') THEN
				UPDATE main.device_session
					SET passcard_trans_count = COALESCE(passcard_trans_count, 0) + 1,
						passcard_trans_total = COALESCE(passcard_trans_total, 0) + ln_trans_amount,
						passcard_vend_count = COALESCE(passcard_vend_count, 0) + pn_vend_count,
						last_trans_in_ts = CURRENT_TIMESTAMP,
						call_in_type = CASE WHEN call_in_type IN ('A', 'C') THEN 'C' ELSE 'B' END
				WHERE global_session_cd = pv_global_session_cd;
			END IF;
			EXIT;
        EXCEPTION 
            WHEN serialization_failure THEN
                NULL;
        END;
    END LOOP;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MAIN.ADD_TRAN_TO_DEVICE_SESSION (
	pv_global_session_cd main.device_session.global_session_cd%TYPE,
	pv_device_name main.device_session.device_name%TYPE,
	pt_call_in_start_ts MAIN.DEVICE_SESSION.CALL_IN_START_TS%TYPE,
	pc_card_type main.device_session.auth_card_type%TYPE,
	pn_sale_amount BIGINT,
	pn_minor_currency_factor INTEGER,
	pn_vend_count main.device_session.credit_vend_count%TYPE
) TO write_main;

CREATE OR REPLACE FUNCTION MAIN.ADD_TRAN_TO_DEVICE_SESSION (
	pv_global_session_cd main.device_session.global_session_cd%TYPE,
	pv_device_name main.device_session.device_name%TYPE,
	pt_call_in_start_ts MAIN.DEVICE_SESSION.CALL_IN_START_TS%TYPE,
	pc_card_type main.device_session.auth_card_type%TYPE,
	pn_sale_amount BIGINT,
	pn_minor_currency_factor INTEGER,
	pn_vend_count main.device_session.credit_vend_count%TYPE,
	pc_payment_type VARCHAR(1)
   ) RETURNS VOID
	SECURITY DEFINER
AS $$
DECLARE
	ln_trans_amount DECIMAL(14,2) := CAST(pn_sale_amount AS DECIMAL(14,2)) / pn_minor_currency_factor;
BEGIN
	-- R34+ version
	IF pc_payment_type IS NULL THEN
		RETURN;
	END IF;
	PERFORM MAIN.GET_OR_CREATE_DEVICE_SESSION(
        pv_global_session_cd,
        pv_device_name,
        pt_call_in_start_ts);
	LOOP
        BEGIN
			IF pc_payment_type = 'C' THEN
				UPDATE main.device_session
					SET credit_trans_count = COALESCE(credit_trans_count, 0) + 1,
						credit_trans_total = COALESCE(credit_trans_total, 0) + ln_trans_amount,
						credit_vend_count = COALESCE(credit_vend_count, 0) + pn_vend_count,
						last_trans_in_ts = CURRENT_TIMESTAMP,
						call_in_type = CASE WHEN call_in_type IN ('A', 'C') THEN 'C' ELSE 'B' END
				WHERE global_session_cd = pv_global_session_cd;
			ELSIF pc_payment_type = 'M' THEN
				UPDATE main.device_session
					SET cash_trans_count = COALESCE(cash_trans_count, 0) + 1,
						cash_trans_total = COALESCE(cash_trans_total, 0) + ln_trans_amount,
						cash_vend_count = COALESCE(cash_vend_count, 0) + pn_vend_count,
						last_trans_in_ts = CURRENT_TIMESTAMP,
						call_in_type = CASE WHEN call_in_type IN ('A', 'C') THEN 'C' ELSE 'B' END
				WHERE global_session_cd = pv_global_session_cd;
			ELSIF pc_payment_type = 'S' THEN
				UPDATE main.device_session
					SET passcard_trans_count = COALESCE(passcard_trans_count, 0) + 1,
						passcard_trans_total = COALESCE(passcard_trans_total, 0) + ln_trans_amount,
						passcard_vend_count = COALESCE(passcard_vend_count, 0) + pn_vend_count,
						last_trans_in_ts = CURRENT_TIMESTAMP,
						call_in_type = CASE WHEN call_in_type IN ('A', 'C') THEN 'C' ELSE 'B' END
				WHERE global_session_cd = pv_global_session_cd;
			END IF;
			EXIT;
        EXCEPTION 
            WHEN serialization_failure THEN
                NULL;
        END;
    END LOOP;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MAIN.ADD_TRAN_TO_DEVICE_SESSION (
	pv_global_session_cd main.device_session.global_session_cd%TYPE,
	pv_device_name main.device_session.device_name%TYPE,
	pt_call_in_start_ts MAIN.DEVICE_SESSION.CALL_IN_START_TS%TYPE,
	pc_card_type main.device_session.auth_card_type%TYPE,
	pn_sale_amount BIGINT,
	pn_minor_currency_factor INTEGER,
	pn_vend_count main.device_session.credit_vend_count%TYPE,
	pc_payment_type VARCHAR(1)
) TO write_main;

DO $$
BEGIN
	CREATE EXTENSION dblink; 
EXCEPTION
	WHEN others THEN
		NULL;
END$$;

CREATE OR REPLACE FUNCTION MAIN.PARTITION_MAINTENANCE (
   ) RETURNS VOID
AS $$
DECLARE
	L_REC RECORD;
BEGIN
	FOR L_REC IN 
		SELECT PT.TABLENAME
		FROM PG_CATALOG.PG_TABLES PT
		JOIN MAIN.PARTITION_INFO PI ON PT.TABLENAME SIMILAR TO PI.PARTITION_NAME_REGEX
		WHERE PI.PARTITION_TYPE_CD = 'D' AND TO_TIMESTAMP(SUBSTRING(PT.TABLENAME FROM PI.PARTITION_NAME_REGEX), 'YYYY_MM_DD') < CURRENT_TIMESTAMP - CAST(PI.RETENTION + 1 || ' DAYS' AS INTERVAL)
			OR PI.PARTITION_TYPE_CD = 'M' AND TO_TIMESTAMP(SUBSTRING(PT.TABLENAME FROM PI.PARTITION_NAME_REGEX), 'YYYY_MM') < CURRENT_TIMESTAMP - CAST(PI.RETENTION + 1 || ' MONTHS' AS INTERVAL)
	LOOP
		PERFORM DBLINK_CONNECT('main', 'dbname=main');
		PERFORM DBLINK_EXEC('main', 'drop table main.' || L_REC.TABLENAME || '; commit;');
		PERFORM DBLINK_DISCONNECT('main');
	END LOOP;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION MAIN.CHECK_PERMISSION(
    pn_user_id BIGINT,
    pn_entity_id BIGINT,
    pv_entity_type VARCHAR
) RETURNS BOOLEAN
AS $$
DECLARE
    lc_allow_edit_flag CHAR(1);
BEGIN
   IF UPPER(pv_entity_type) = 'VIEW_TERMINAL' THEN
       SELECT * 
         INTO lc_allow_edit_flag
         FROM ODBCLINK.QUERY('DSN=Oracle_DB', 'SELECT ALLOW_EDIT FROM REPORT.USER_TERMINAL WHERE USER_ID = ' || pn_user_id || ' AND TERMINAL_ID = ' || pn_entity_id) AS T(ALLOW_EDIT_FLAG CHAR(1));
       RETURN lc_allow_edit_flag IS NOT NULL;
   ELSIF UPPER(pv_entity_type) = 'EDIT_TERMINAL' THEN
       SELECT * 
         INTO lc_allow_edit_flag
         FROM ODBCLINK.QUERY('DSN=Oracle_DB', 'SELECT ALLOW_EDIT FROM REPORT.USER_TERMINAL WHERE USER_ID = ' || pn_user_id || ' AND TERMINAL_ID = ' || pn_entity_id) AS T(ALLOW_EDIT_FLAG CHAR(1));
       RETURN lc_allow_edit_flag  = 'Y';
   ELSE
       RETURN FALSE;
   END IF;
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MAIN.CHECK_PERMISSION(
    pn_user_id BIGINT,
    pn_entity_id BIGINT,
    pv_entity_type VARCHAR
) TO read_main;

CREATE OR REPLACE FUNCTION MAIN.IS_DEVICE_REJECTED(
	pv_device_name VARCHAR
) RETURNS BOOLEAN
AS $$
DECLARE
    lc_device_rejected_flag CHAR(1);
BEGIN
	SELECT COALESCE(MAX(DEVICE_REJECTED_FLAG), 'Y')
	INTO lc_device_rejected_flag
	FROM ODBCLINK.QUERY('DSN=Oracle_DB', 'SELECT CASE WHEN reject_until_ts IS NULL OR SYSDATE > reject_until_ts THEN ''N'' ELSE ''Y'' END FROM device.device WHERE device_name = ''' || pv_device_name || ''' AND device_active_yn_flag = ''Y''')
	AS T(DEVICE_REJECTED_FLAG CHAR(1));
	RETURN lc_device_rejected_flag = 'Y';
END;
$$ LANGUAGE plpgsql;

GRANT EXECUTE ON FUNCTION MAIN.IS_DEVICE_REJECTED(
	pv_device_name VARCHAR
) TO read_main;
