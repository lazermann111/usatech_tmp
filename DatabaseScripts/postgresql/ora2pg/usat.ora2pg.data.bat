call ora2pg -c usat.ora2pg.conf -b OUT/DATA -t COPY -n DBADMIN -o USAT.DATA.1.DBADMIN.sql -e APP_LOCK, CREATE$JAVA$LOB$TABLE
call ora2pg -c usat.ora2pg.conf -b OUT/DATA -t COPY -n APP_USER -o USAT.DATA.3.APP_USER.sql
call ora2pg -c usat.ora2pg.conf -b OUT/DATA -t COPY -n AUTHORITY -o USAT.DATA.4.AUTHORITY.sql
call ora2pg -c usat.ora2pg.conf -b OUT/DATA -t COPY -n DEVICE -o USAT.DATA.5.DEVICE.sql
call ora2pg -c usat.ora2pg.conf -b OUT/DATA -t COPY -n ENGINE -o USAT.DATA.6.ENGINE.sql
call ora2pg -c usat.ora2pg.conf -b OUT/DATA -t COPY -n FOLIO_CONF -o USAT.DATA.7.FOLIO_CONF.sql
call ora2pg -c usat.ora2pg.conf -b OUT/DATA -t COPY -n FRONT -o USAT.DATA.8.FRONT.sql
call ora2pg -c usat.ora2pg.conf -b OUT/DATA -t COPY -n G4OP -o USAT.DATA.9.G4OP.sql
call ora2pg -c usat.ora2pg.conf -b OUT/DATA -t COPY -n PSS -o USAT.DATA.10.PSS.sql -e TRAN_STAT
call ora2pg -c usat.ora2pg.conf -b OUT/DATA -t COPY -n QUARTZ -o USAT.DATA.11.QUARTZ.sql
call ora2pg -c usat.ora2pg.conf -b OUT/DATA -t COPY -n REPORT -o USAT.DATA.14.REPORT.sql -e MSG_LOG, QT_SYNC_MSG, WEB_REQUEST
call ora2pg -c usat.ora2pg.conf -b OUT/DATA -t COPY -n USAT_CUSTOM -o USAT.DATA.16.USAT_CUSTOM.sql
call ora2pg -c usat.ora2pg.conf -b OUT/DATA -t COPY -n WEB_CONTENT -o USAT.DATA.17.WEB_CONTENT.sql
