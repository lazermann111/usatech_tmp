CREATE TABLE pss.authority_payment_mask(
	authority_payment_mask_id		NUMBER(20,0) PRIMARY KEY,
	payment_subtype_id				NUMBER(20,0),	--kfey of pss.payment_subtype.payment_subtype_id
	authority_payment_mask_name		VARCHAR2(60) NOT NULL,
	authority_payment_mask_desc		VARCHAR2(60),
	authority_payment_mask_regex	VARCHAR2(255),
	authority_payment_mask_bref		VARCHAR2(60),
	created_by						VARCHAR2(30) NOT NULL,
	created_ts						DATE NOT NULL,
	last_updated_by					VARCHAR2(30) NOT NULL,
	last_updated_ts					DATE NOT NULL
)
TABLESPACE pss_data;

ALTER TABLE pss.payment_subtype_regex_bref RENAME TO authority_payment_mask_bref;
ALTER TABLE pss.authority_payment_mask_bref RENAME COLUMN payment_subtype_regex_bref_id TO authority_payment_mask_bref_id;

ALTER TABLE pss.payment_subtype ADD (authority_payment_mask_id NUMBER(20,0));
ALTER TABLE pss.payment_subtype DROP COLUMN payment_subtype_regex;
ALTER TABLE pss.payment_subtype DROP COLUMN payment_subtype_regex_bref;

ALTER TABLE pss.pos_pta ADD (authority_payment_mask_id NUMBER(20,0));
ALTER TABLE pss.pos_pta ADD (pos_pta_priority INTEGER);
