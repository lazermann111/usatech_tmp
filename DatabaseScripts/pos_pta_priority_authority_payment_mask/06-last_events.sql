--1  Credit Card (Track 2)						 	([0-9]{16}[0-9]{0,3})=([0-9]{0,4})([0-9]{0,4})         	1:1|2:3|3:5
--2  Credit Card (Track 2, no Exp. Date)			([0-9]{16}[0-9]{0,3})=([0-9]{0,4})([0-9]{0,4})         	1:1|2:4|3:5
--3  RFID										 	(.+)                                                   
--4  Cash										 	(.*)                                                   
--5  Special Card - USA Tech Local Auth Passcard 	([0-9]+)                                               	1:1
--6  Special Card - USA Tech (Track 1)			 	(SEVEND1)([a-zA-Z]+)([0-9]+)\^(PRIMA\/)([a-zA-Z0-9]+)\^	1:8|2:7|3:1|5:9
--7  Special Card - USA Tech (Track 2)			 	(99[0-9]{4})([0-9]{10})=(0+)                           	1:7|2:1|3:8
--8  Special Card - eSuds Blackboard				([0-9]{15})                                            
--9  Special Card - eSuds Generic				 	(.+)                                                   
--10 Special Card - Aramark Generic				 	([0-9]{14})([0-9]{2})?                                 	1:1|2:7

-- defaults
INSERT INTO pss.authority_payment_mask (payment_subtype_id, authority_payment_mask_name, authority_payment_mask_desc, authority_payment_mask_regex, authority_payment_mask_bref)
	VALUES (1, 'Default', 'Credit Card (Track 2)', '^([0-9]{16}[0-9]{0,3})=([0-9]{0,4})([0-9]{0,4})$', '1:1|2:3|3:5');
INSERT INTO pss.authority_payment_mask (payment_subtype_id, authority_payment_mask_name, authority_payment_mask_desc, authority_payment_mask_regex, authority_payment_mask_bref)
	VALUES (2, 'Default', 'Credit Card (Track 2, no Exp. Date)', '^([0-9]{16}[0-9]{0,3})=([0-9]{0,4})([0-9]{0,4})$', '1:1|2:4|3:5');
INSERT INTO pss.authority_payment_mask (payment_subtype_id, authority_payment_mask_name, authority_payment_mask_desc, authority_payment_mask_regex, authority_payment_mask_bref)
	VALUES (3, 'Default', 'RFID', '^(.+)$', NULL);
INSERT INTO pss.authority_payment_mask (payment_subtype_id, authority_payment_mask_name, authority_payment_mask_desc, authority_payment_mask_regex, authority_payment_mask_bref)
	VALUES (4, 'Default', 'Cash', '^(.*)$', NULL);
INSERT INTO pss.authority_payment_mask (payment_subtype_id, authority_payment_mask_name, authority_payment_mask_desc, authority_payment_mask_regex, authority_payment_mask_bref)
	VALUES (5, 'Default', 'Special Card - USA Tech Local Auth Passcard', '^(9{6}[0-9]{3})$', '1:1');
INSERT INTO pss.authority_payment_mask (payment_subtype_id, authority_payment_mask_name, authority_payment_mask_desc, authority_payment_mask_regex, authority_payment_mask_bref)
	VALUES (6, 'Default', 'Special Card - USA Tech (Track 1)', '^(SEVEND1)([a-zA-Z]+)([0-9]+)\^(PRIMA\/)([a-zA-Z0-9]+)\^$', '1:8|2:7|3:1|5:9');
INSERT INTO pss.authority_payment_mask (payment_subtype_id, authority_payment_mask_name, authority_payment_mask_desc, authority_payment_mask_regex, authority_payment_mask_bref)
	VALUES (7, 'Default', 'Special Card - USA Tech (Track 2)', '^(99[0-9]{4})([0-9]{10})=(0+)$', '1:7|2:1|3:8');
INSERT INTO pss.authority_payment_mask (payment_subtype_id, authority_payment_mask_name, authority_payment_mask_desc, authority_payment_mask_regex, authority_payment_mask_bref)
	VALUES (8, 'Default', 'Special Card - eSuds Blackboard', '^([0-9]+)$', NULL);
INSERT INTO pss.authority_payment_mask (payment_subtype_id, authority_payment_mask_name, authority_payment_mask_desc, authority_payment_mask_regex, authority_payment_mask_bref)
	VALUES (9, 'Default', 'Special Card - eSuds Generic', '^(.+)$', NULL);
INSERT INTO pss.authority_payment_mask (payment_subtype_id, authority_payment_mask_name, authority_payment_mask_desc, authority_payment_mask_regex, authority_payment_mask_bref)
	VALUES (10, 'Default', 'Special Card - Aramark Generic', '^([0-9]{14,19})$', '1:1');

-- blackboard specific
INSERT INTO pss.authority_payment_mask (payment_subtype_id, authority_payment_mask_name, authority_payment_mask_desc, authority_payment_mask_regex, authority_payment_mask_bref)
	VALUES (8, 'Goucher Student Card', '', '^([0-9]{3})([0-9]{2})0*([0-9]+)([0-9]{1})$', '1:7|2:8|3:1|4:9');
INSERT INTO pss.authority_payment_mask (payment_subtype_id, authority_payment_mask_name, authority_payment_mask_desc, authority_payment_mask_regex, authority_payment_mask_bref)
	VALUES (8, 'Account Num = 1st set of numerical characters', '', '^([0-9]+)(.*)$', '1:1|2:7');
INSERT INTO pss.authority_payment_mask (payment_subtype_id, authority_payment_mask_name, authority_payment_mask_desc, authority_payment_mask_regex, authority_payment_mask_bref)
	VALUES (8, 'Account Num = 1st 9 characters', '', '^(.{1,9})(.*)$', '1:1|2:7');
-- internal specific
INSERT INTO pss.authority_payment_mask (payment_subtype_id, authority_payment_mask_name, authority_payment_mask_desc, authority_payment_mask_regex, authority_payment_mask_bref)
	VALUES (9, 'CMU Student Card', '', '^([0-9]{9,10})(=([0-9]{4}))?$', '1:1|3:7');
INSERT INTO pss.authority_payment_mask (payment_subtype_id, authority_payment_mask_name, authority_payment_mask_desc, authority_payment_mask_regex, authority_payment_mask_bref)
	VALUES (9, 'Account Num = 1st set of numerical characters', '', '^([0-9]+)(.*)$', '1:1|2:7');
INSERT INTO pss.authority_payment_mask (payment_subtype_id, authority_payment_mask_name, authority_payment_mask_desc, authority_payment_mask_regex, authority_payment_mask_bref)
	VALUES (9, 'Account Num = 1st 9 characters', '', '^(.{1,9})(.*)$', '1:1|2:7');
-- aramark
INSERT INTO pss.authority_payment_mask (payment_subtype_id, authority_payment_mask_name, authority_payment_mask_desc, authority_payment_mask_regex, authority_payment_mask_bref)
	VALUES (10, 'UNCG Student Card', '', '^(5054[0-9]{12})(=([0-9]+))?$', '1:1|3:7');
INSERT INTO pss.authority_payment_mask (payment_subtype_id, authority_payment_mask_name, authority_payment_mask_desc, authority_payment_mask_regex, authority_payment_mask_bref)
	VALUES (10, 'Account Num = 1st set of numerical characters', '', '^([0-9]+)(.*)$', '1:1|2:7');
INSERT INTO pss.authority_payment_mask (payment_subtype_id, authority_payment_mask_name, authority_payment_mask_desc, authority_payment_mask_regex, authority_payment_mask_bref)
	VALUES (10, 'Account Num = 1st 9 characters', '', '^(.{1,9})(.*)$', '1:1|2:7');

UPDATE pss.payment_subtype ps
SET authority_payment_mask_id = payment_subtype_id 
WHERE payment_subtype_id = (
    SELECT psx.payment_subtype_id
    FROM payment_subtype psx
    WHERE psx.payment_subtype_id = ps.payment_subtype_id
);
ALTER TABLE pss.payment_subtype MODIFY authority_payment_mask_id NOT NULL;

DELETE FROM pss.settlement_batch_state WHERE settlement_batch_state_id = 6;

UPDATE pss.payment_subtype SET payment_subtype_class = 'Aramark' 
	WHERE payment_subtype_class = 'ReRix::POS::Handler::Aramark';
UPDATE pss.payment_subtype SET payment_subtype_class = 'BlackBoard' 
	WHERE payment_subtype_class = 'ReRix::POS::Handler::BlackBoard';
UPDATE pss.payment_subtype SET payment_subtype_class = 'Cash' 
	WHERE payment_subtype_class = 'ReRix::POS::Handler::Cash';
UPDATE pss.payment_subtype SET payment_subtype_class = 'Internal' 
	WHERE payment_subtype_class = 'ReRix::POS::Handler::Internal';

-- priority settings
BEGIN
	DECLARE
		n_pos_pta_id pos_pta.pos_pta_id%TYPE;
		v_client_payment_type_cd payment_subtype.client_payment_type_cd%TYPE;
		n_pos_id pos_pta.pos_id%TYPE;
		CURSOR c_get_pos_pta IS
    		SELECT pos_pta_id, client_payment_type_cd, pos_id
    		FROM pss.pos_pta pp, pss.payment_subtype ps
    		WHERE ps.payment_subtype_id = pp.payment_subtype_id
    		ORDER BY client_payment_type_cd, pos_id;
	BEGIN
		OPEN c_get_pos_pta;
		LOOP
			FETCH c_get_pos_pta INTO n_pos_pta_id, v_client_payment_type_cd, n_pos_id;
			EXIT WHEN c_get_pos_pta%NOTFOUND;
			UPDATE pss.pos_pta pp
			SET pos_pta_priority = (
				SELECT NVL(MAX(pos_pta_priority), 0) + 1
				FROM pos_pta ppx, pss.payment_subtype psx
				WHERE psx.payment_subtype_id = ppx.payment_subtype_id
                AND client_payment_type_cd = v_client_payment_type_cd
				AND pos_id = n_pos_id
			)
            WHERE pos_pta_id = n_pos_pta_id
            AND pos_pta_priority IS NULL;
		END LOOP;
		COMMIT;
		CLOSE c_get_pos_pta;
	END;
END;
/

ALTER TABLE pss.pos_pta MODIFY pos_pta_priority NOT NULL;
ALTER TABLE pss.pos_pta ADD CONSTRAINT uk_pos_pta_1 UNIQUE (pos_id, pos_pta_priority, payment_subtype_id);
