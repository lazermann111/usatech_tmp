ALTER TABLE pss.authority_payment_mask ADD CONSTRAINT fk_authority_payment_mask_ps FOREIGN KEY (payment_subtype_id) REFERENCES PSS.payment_subtype (payment_subtype_id);
ALTER TABLE pss.authority_payment_mask ADD CONSTRAINT uk_authority_payment_mask_1 UNIQUE (payment_subtype_id, authority_payment_mask_regex, authority_payment_mask_bref);

ALTER TABLE pss.payment_subtype ADD CONSTRAINT fk_payment_subtype_apm FOREIGN KEY (authority_payment_mask_id) REFERENCES PSS.authority_payment_mask (authority_payment_mask_id);

ALTER TABLE pss.pos_pta ADD CONSTRAINT fk_pos_pta_apm FOREIGN KEY (authority_payment_mask_id) REFERENCES PSS.authority_payment_mask (authority_payment_mask_id);
