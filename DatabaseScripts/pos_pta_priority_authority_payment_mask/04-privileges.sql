GRANT ALL ON pss.authority_payment_mask TO web_user;
GRANT SELECT ON pss.seq_authority_payment_mask_id TO web_user;

CREATE PUBLIC SYNONYM authority_payment_mask FOR pss.authority_payment_mask;
DROP PUBLIC SYNONYM payment_subtype_regex_bref;
CREATE PUBLIC SYNONYM authority_payment_mask_bref FOR pss.authority_payment_mask_bref;
