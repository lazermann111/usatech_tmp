ALTER TRIGGER DEVICE.TRBU_CONFIG_TEMPLATE_CATEGORY DISABLE ;
UPDATE DEVICE.CONFIG_TEMPLATE_CATEGORY
  SET CREATED_BY = USER
 WHERE CREATED_BY IS NULL;

UPDATE DEVICE.CONFIG_TEMPLATE_CATEGORY
  SET LAST_UPDATED_BY = USER
 WHERE LAST_UPDATED_BY IS NULL;
 
UPDATE DEVICE.CONFIG_TEMPLATE_CATEGORY
  SET CREATED_TS = SYSDATE
 WHERE CREATED_TS IS NULL;
 
UPDATE DEVICE.CONFIG_TEMPLATE_CATEGORY
  SET LAST_UPDATED_TS = SYSDATE
 WHERE LAST_UPDATED_TS IS NULL;

COMMIT;
ALTER TABLE DEVICE.CONFIG_TEMPLATE_CATEGORY  	MODIFY(CREATED_BY VARCHAR2(30) NOT NULL)  	MODIFY(CREATED_TS DATE NOT NULL)  	MODIFY(LAST_UPDATED_BY VARCHAR2(30) NOT NULL)  	MODIFY(LAST_UPDATED_TS DATE NOT NULL)  ;  
ALTER TRIGGER DEVICE.TRBU_CONFIG_TEMPLATE_CATEGORY ENABLE;

ALTER TABLE DEVICE.DEVICE_TYPE  	DROP(TRAN_RECENT_ENABLED_YN_FLAG)  ;  

DROP TABLE DEVICE.DEVICE_TYPE_FILE_TRANSFER_TYPE;

DROP TABLE PSS.ADMIN_CMD_HIST;  

ALTER TABLE PSS.CONSUMER_ACCT  	DROP(SECURITY_CODE_HASH, SECURITY_CODE_SALT, SECURITY_CODE_HASH_ALG)  ;  														

DROP TABLE REPORT.TERMINAL_ALERT_BAK;  

CREATE OR REPLACE FUNCTION DBADMIN.GET_AUTHED_CARD (
	l_parsed_acct_data IN VARCHAR2)
RETURN VARCHAR2 PARALLEL_ENABLE DETERMINISTIC IS
/*
SELECT '''' || REGEXP_REPLACE(AUTHORITY_PAYMENT_MASK_REGEX, '([^\\])\(\?\:', '\1(') || ''', -- ' || AUTHORITY_PAYMENT_MASK_DESC 
  FROM PSS.AUTHORITY_PAYMENT_MASK 
 WHERE AUTHORITY_ASSN_ID IS NOT NULL
*/
    lt_cc_regex VARCHAR2_TABLE :=  VARCHAR2_TABLE(
        '^;?(4[0-9]{15})=([0-9]{4})([0-9]{3})([0-9]*)(\?([\x00-\xFF])?)?$', -- Visa Credit Card on Track 2
        '^;?(5[1-5][0-9]{14})=([0-9]{4})([0-9]{3})([0-9]*)(\?([\x00-\xFF])?)?$', -- MasterCard Credit Card on Track 2
        '^;?(3[47][0-9]{13})=([0-9]{4})([0-9]{3})([0-9]*)(\?([\x00-\xFF])?)?$', -- American Express Credit Card on Track 2
        '^;?((6011|622[0-9]|64[4-9][0-9]|65[0-9]{2})[0-9]{12})=([0-9]{4})([0-9]{3})([0-9]*)(\?([\x00-\xFF])?)?$', -- Discover Card Credit Card on Track 2
        '^;?(36[0-9]{12})=([0-9]{4})([0-9]{3})([0-9]*)(\?([\x00-\xFF])?)?$', -- Diners Club International Credit Card on Track 2
        '^;?(30[0-5][0-9]{11})=([0-9]{4})([0-9]{3})([0-9]*)(\?([\x00-\xFF])?)?$', -- Diners Club Carte Blanche Credit Card on Track 2
        '^%?B(4[0-9]{15})\^([A-Za-z0-9 \/]{2,26})\^([0-9]{4}?)([0-9]{3}?)([A-Za-z0-9 \/]*)(\?([\x00-\xFF])?)$', -- Visa Credit Card on Track 1
        '^%?B(5[1-5][0-9]{14})\^([A-Za-z0-9 \/]{2,26})\^([0-9]{4}?)([0-9]{3}?)([A-Za-z0-9 \/]*)(\?([\x00-\xFF])?)$', -- MasterCard Credit Card on Track 1
        '^%?B(3[47][0-9]{13})\^([A-Za-z0-9 \/]{2,26})\^([0-9]{4}?)([0-9]{3}?)([A-Za-z0-9 \/]*)(\?([\x00-\xFF])?)$', -- American Express Credit Card on Track 1
        '^%?B((6011|622[0-9]|64[4-9][0-9]|65[0-9]{2})[0-9]{12})\^([A-Za-z0-9 \/]{2,26})\^([0-9]{4}?)([0-9]{3}?)([A-Za-z0-9 \/]*)(\?([\x00-\xFF])?)$', -- Discover Card Credit Card on Track 1
        '^%?B(36[0-9]{12})\^([A-Za-z0-9 \/]{2,26})\^([0-9]{4}?)([0-9]{3}?)([A-Za-z0-9 \/]*)(\?([\x00-\xFF])?)$', -- Diners Club International Credit Card on Track 1
        '^%?B(30[0-5][0-9]{11})\^([A-Za-z0-9 \/]{2,26})\^([0-9]{4}?)([0-9]{3}?)([A-Za-z0-9 \/]*)(\?([\x00-\xFF])?)$' -- Diners Club Carte Blanche Credit Card on Track 1
    ); 
    lv_auth_card_data VARCHAR2(19);
BEGIN
    SELECT REGEXP_REPLACE(l_parsed_acct_data, a.COLUMN_VALUE, '\1')
      INTO lv_auth_card_data
      FROM TABLE(lt_cc_regex) a
     WHERE REGEXP_LIKE(l_parsed_acct_data, a.COLUMN_VALUE)
       AND ROWNUM = 1;
    RETURN lv_auth_card_data;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN l_parsed_acct_data;
END; 
/

CREATE OR REPLACE FUNCTION DBADMIN.HASH_CARD (
	l_card IN VARCHAR2)
RETURN VARCHAR2 PARALLEL_ENABLE DETERMINISTIC IS
BEGIN
     IF l_card IS NULL OR LENGTH(l_card) = 0 THEN
	    RETURN NULL;
     END IF;
     RETURN UTL_RAW.CAST_TO_VARCHAR2(HEXTORAW(UTL_ENCODE.BASE64_ENCODE(DBMS_CRYPTO.HASH(RAWTOHEX(UTL_RAW.CAST_TO_RAW(l_card)), DBMS_CRYPTO.HASH_SH1))));
END; 
/

DROP FUNCTION DBADMIN.USAT_ADD_WEEKS;
DROP FUNCTION DBADMIN.USAT_GET_DENC_VAL;
DROP FUNCTION DBADMIN.USAT_GET_ENC_VAL;  

DROP FUNCTION RDW.GET_ADJUST_DESC;

CREATE OR REPLACE FUNCTION REPORT.CAN_ADMIN_USER 
   (l_user_id USER_LOGIN.USER_ID%TYPE,
	l_admin_id USER_LOGIN.USER_ID%TYPE)
RETURN CHAR IS
	l_new_admin_id USER_LOGIN.USER_ID%TYPE;
	l_admin_type USER_LOGIN.USER_TYPE%TYPE;
	l_user_type USER_LOGIN.USER_TYPE%TYPE;
	l_user_cust_id USER_LOGIN.CUSTOMER_ID%TYPE;
	l_admin_cust_id USER_LOGIN.CUSTOMER_ID%TYPE;
BEGIN
	 IF l_user_id IS NULL THEN
	 	RETURN 'N';
	 ELSIF l_user_id  = 0 THEN
	 	RETURN 'N';
	 ELSIF l_user_id = l_admin_id THEN
	 	RETURN 'Y';
	 ELSE
	 	SELECT USER_TYPE, CUSTOMER_ID 
          INTO l_admin_type, l_admin_cust_id
          FROM USER_LOGIN
         WHERE USER_ID = l_admin_id;
	 	SELECT ADMIN_ID, USER_TYPE, CUSTOMER_ID 
          INTO l_new_admin_id, l_user_type, l_user_cust_id 
          FROM USER_LOGIN 
         WHERE USER_ID = l_user_id;
        IF l_admin_type = 8 THEN
            IF l_admin_cust_id = l_user_cust_id AND l_user_cust_id <> 0 THEN
                IF CHECK_PRIV(l_admin_id, 5) = 'Y' OR CHECK_PRIV(l_admin_id, 8) = 'Y' THEN
                    RETURN 'Y';
                ELSIF CHECK_PRIV(l_admin_id, 21) = 'Y' THEN
                    RETURN 'R'; -- read-only
                END IF;
            END IF;
        ELSIF (CHECK_PRIV(l_admin_id, 5) = 'Y' OR (l_user_type = 8 AND CHECK_PRIV(l_admin_id, 8) = 'Y')) THEN
		   RETURN 'Y';
		ELSIF l_user_type = 8 AND CHECK_PRIV(l_admin_id, 21) = 'Y' THEN
           RETURN 'R'; -- read-only
        END IF;
		IF l_new_admin_id = l_user_id THEN -- if a user's admin is themself
		   RETURN 'N';
		ELSE
		   RETURN CAN_ADMIN_USER(l_new_admin_id, l_admin_id);
		END IF;
	 END IF;
END;
/

CREATE OR REPLACE FUNCTION        REPORT.MASK_STR (
    l_str IN VARCHAR,
    l_lead IN NUMBER,
    l_end IN NUMBER
    )
RETURN VARCHAR PARALLEL_ENABLE DETERMINISTIC IS
BEGIN
    IF l_lead+l_end >= LENGTH(l_str) THEN
         RETURN RPAD('*',LENGTH(l_str), '*');
     ELSE
        RETURN SUBSTR(l_str, 1, l_lead)||RPAD('*', LENGTH(l_str) - l_lead-l_end, '*') || SUBSTR(l_str, -l_end);
     END IF;
END;
/

DROP PROCEDURE DBADMIN.USAT_ADD_WEEKLY_PARTITION ;
DROP PROCEDURE DBADMIN.USAT_ADD_WEEKLY_PART_BY_DATE ;
DROP PROCEDURE DEVICE.SP_SHOW_CONFIG;
DROP PROCEDURE FOLIO_CONF.ADD_FIELD_PRIV_TEST;

CREATE OR REPLACE FORCE VIEW REPORT.VW_TERMINAL (TERMINAL_ID, TERMINAL_NBR, TERMINAL_NAME, REGION_ID, REGION_NAME, LOCATION_ID, LOCATION_NAME, PRIMARY_CONTACT_ID, SECONDARY_CONTACT_ID, CUSTOMER_ID, STATUS, ASSET_NBR, MAKE, MODEL, EPORT_SERIAL_NUM, CUSTOMER_NAME) AS 
 SELECT T.TERMINAL_ID,
        TERMINAL_NBR,
        TERMINAL_NAME,
        R.REGION_ID,
        REGION_NAME,
        T.LOCATION_ID,
        LOCATION_NAME,
        PRIMARY_CONTACT_ID,
        SECONDARY_CONTACT_ID,
        T.CUSTOMER_ID,
        T.STATUS,
        T.ASSET_NBR,
        MAKE,
        MODEL,
        EPORT_SERIAL_NUM,
        CUSTOMER_NAME
   FROM TERMINAL T,
        LOCATION L,
        REGION R,
        TERMINAL_REGION TR,
        MACHINE M,
        EPORT E,
        CUSTOMER C
  WHERE T.LOCATION_ID = L.LOCATION_ID(+)
    AND T.TERMINAL_ID   = TR.TERMINAL_ID(+)
    AND TR.REGION_ID    = R.REGION_ID(+)
    AND T.STATUS       <> 'D'
    AND T.MACHINE_ID    = M.MACHINE_ID(+)
    AND T.EPORT_ID      = E.EPORT_ID(+)
    AND T.CUSTOMER_ID   = C.CUSTOMER_ID(+)
/

ALTER TABLE DEVICE.DEVICE_SETTING_PARAMETER DROP CONSTRAINT FK_DEVICE_SETTING_PARAMETER_CD;  

ALTER TABLE REPORT.TERMINAL DROP CONSTRAINT FK_TERMINAL_EPORT_ID;  

DROP TRIGGER DEVICE.TRBI_DEVICE_TYPE_FILE_XFER_TP;

DROP TRIGGER DEVICE.TRBU_DEVICE_TYPE_FILE_XFER_TP;

DROP TRIGGER PSS.TRBI_ADMIN_CMD_HIST;
DROP TRIGGER PSS.TRBU_ADMIN_CMD_HIST;

CREATE OR REPLACE TRIGGER location.trbi_state
BEFORE INSERT ON location.state
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
Declare

    CURSOR c_upper_state_name IS
          select nvl(state_name, :new.state_name)
        FROM state
       WHERE UPPER(:new.state_name) = UPPER(state_name);

Begin

  SELECT UPPER(:new.country_cd),
           UPPER(:new.state_cd),
           sysdate,
           user,
           sysdate,
           user
      into :new.country_cd,
           :new.state_cd,
           :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by
      FROM dual;

   --Try to match up the users state name with one that already exists
      OPEN c_upper_state_name;
      FETCH c_upper_state_name
        INTO :new.state_name;
      CLOSE c_upper_state_name;

End;
/

DROP SYNONYM REPORT.LOCATION_TYPE;

CREATE TABLE REPORT.LOCATION_TYPE
    (
        LOCATION_TYPE_ID   NUMBER NOT NULL,
        LOCATION_TYPE_NAME VARCHAR2(50) NOT NULL,
        STATUS             CHAR(1) DEFAULT 'A' NOT NULL,
        SPECIFY            CHAR(1) DEFAULT 'N' NOT NULL,
        PRIMARY KEY(LOCATION_TYPE_ID) USING INDEX TABLESPACE REPORT_INDX 
    )
    TABLESPACE REPORT_DATA ;
/*    
SELECT 'GRANT ' || PRIVILEGE || ' ON ' || OWNER || '.' || TABLE_NAME ||' TO ' || GRANTEE || ';', P.*
  FROM DBA_TAB_PRIVS P
 WHERE TABLE_NAME = 'LOCATION_TYPE'
   AND OWNER = 'REPORT';
*/
GRANT DELETE ON REPORT.LOCATION_TYPE TO USATECH_UPD_TRANS;
GRANT INSERT ON REPORT.LOCATION_TYPE TO USATECH_UPD_TRANS;
GRANT SELECT ON REPORT.LOCATION_TYPE TO USATECH_UPD_TRANS;
GRANT UPDATE ON REPORT.LOCATION_TYPE TO USATECH_UPD_TRANS;
GRANT SELECT ON REPORT.LOCATION_TYPE TO USAT_DEV_READ_ONLY;
GRANT DELETE ON REPORT.LOCATION_TYPE TO USAT_ADMIN_ROLE;
GRANT INSERT ON REPORT.LOCATION_TYPE TO USAT_ADMIN_ROLE;
GRANT SELECT ON REPORT.LOCATION_TYPE TO USAT_ADMIN_ROLE;
GRANT UPDATE ON REPORT.LOCATION_TYPE TO USAT_ADMIN_ROLE;
GRANT SELECT ON REPORT.LOCATION_TYPE TO USALIVE_APP_ROLE;
GRANT SELECT ON REPORT.LOCATION_TYPE TO RDW_LOADER;
GRANT SELECT ON REPORT.LOCATION_TYPE TO USALIVE_APP;
GRANT REFERENCES ON REPORT.LOCATION_TYPE TO USALIVE_APP;
GRANT SELECT ON REPORT.LOCATION_TYPE TO USARQA;
GRANT SELECT ON REPORT.LOCATION_TYPE TO USAT_DMS_ROLE;

--SELECT * FROM REPORT.LOCATION_TYPE;
SET DEFINE OFF;
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (1,'Business & Industry','I','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (2,'Industrial/Commercial','I','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (3,'Retail - Public Access','I','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (4,'Retail - Breakroom','I','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (5,'Education - Primary','I','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (6,'Education - Secondary','I','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (7,'Apartment','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (8,'Hotel','I','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (9,'Convention Center','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (10,'Airport','I','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (11,'Travel Center','I','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (12,'Government','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (13,'Hospital','I','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (14,'Military','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (15,'Other','A','Y');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (16,'Amusement Park','I','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (17,'Airport - Back of House','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (18,'Airport - Front of House','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (19,'Amusement Park - Back of House','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (20,'Amusement Park - Front of House','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (21,'Automotive','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (31,'Business / Professional Office','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (32,'Education','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (22,'Entertainment - Back of House','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (23,'Entertainment - Front of House','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (33,'Hospital / Healthcare','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (35,'Industrial','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (34,'Lodging','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (24,'Lodging - Casinos','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (25,'Retail - Malls / Outlets - Back of Office','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (26,'Retail - Malls / Outlets - Front of Office','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (27,'Retail - Specialty - Back of Office','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (28,'Retail - Specialty - Front of Office','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (29,'Retail - Supermarket - Back of Office','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (30,'Retail - Supermarket - Front of Office','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (36,'Travel - Other','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (0,'- Not Assigned -','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (38,'Car Wash/Air Service','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (39,'Amusement','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (40,'Office Equipment','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (41,'Kiosk','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (46,'Vending/Bulk Dispensing ','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (47,'Vending/Business/Customer Service','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (48,'Vending/Business/Financial Services','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (49,'Vending/Business/Distribution','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (50,'Vending/Business/Airport BOH & Airline','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (51,'Vending/Business/Technology','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (52,'Vending/Business/Manufacturing','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (53,'Vending/Business/Local Government','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (54,'Vending/Business/Retail BOH','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (58,'Vending/Business/Other','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (60,'Vending/Education/Technical & Specialized','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (61,'Vending/Education/University','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (62,'Vending/Education/College','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (63,'Vending/Education/High School','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (64,'Vending/Education/Other','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (65,'Vending/Entertainment/Amusement Park','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (66,'Vending/Entertainment/Zoo Aquarium Museum','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (67,'Vending/Entertainment/Sport & Gym','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (69,'Vending/Entertainment/Cinema Performing Arts','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (70,'Vending/Entertainment/Other','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (71,'Vending/Healthcare/Hospital','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (72,'Vending/Healthcare/Nursing Home & Rehabilitation','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (73,'Vending/Healthcare/Other','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (74,'Vending/Hospitality/Apartments','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (75,'Vending/Hospitality/Hotel','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (77,'Vending/Hospitality/Casino Hotel','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (78,'Vending/Hospitality/Other','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (79,'Vending/Military/Army','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (80,'Vending/Military/Navy','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (81,'Vending/Military/Air Force','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (82,'Vending/Military/Exchange','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (83,'Vending/Military/Other','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (84,'Vending/Retail FOH/Automotive','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (85,'Vending/Retail FOH/Mall','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (86,'Vending/Retail FOH/Outlet Mall','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (87,'Vending/Retail FOH/Hardware','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (88,'Vending/Retail FOH/Other','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (89,'Vending/Travel/Airport FOH','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (90,'Vending/Travel/Rail Bus','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (91,'Vending/Travel/Car Hire','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (92,'Vending/Travel/Rest Stop','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (93,'Vending/Travel/Other','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (94,'Laundry','A','N');
Insert into REPORT.LOCATION_TYPE (LOCATION_TYPE_ID,LOCATION_TYPE_NAME,STATUS,SPECIFY) values (95,'- Not Specified -','A','N');

COMMIT;

CREATE OR REPLACE PACKAGE BODY DEVICE.PKG_EVENT 
IS

FUNCTION SF_GET_GLOBAL_EVENT_CD
(
    pc_global_event_cd_prefix IN CHAR,
    pv_device_name IN device.device_name%TYPE,
    pv_device_tran_cd IN event.event_device_tran_cd%TYPE
) RETURN VARCHAR2
IS
BEGIN
    RETURN pc_global_event_cd_prefix || ':' || pv_device_name || ':' || pv_device_tran_cd;
END;

-- R33+ signature
PROCEDURE SP_GET_OR_CREATE_EVENT
(
    pc_global_event_cd_prefix IN CHAR,
    pv_global_session_cd IN VARCHAR2,
    pv_device_name IN device.device_name%TYPE,
    pn_host_port_num IN host.host_port_num%TYPE,
    pn_event_type_id IN event_type.event_type_id%TYPE,
    pn_event_type_host_type_num IN event_type_host_type.event_type_host_type_num%TYPE,
    pn_event_state_id IN event.event_state_id%TYPE,
    pv_event_device_tran_cd IN event.event_device_tran_cd%TYPE,
    pn_event_end_flag IN NUMBER,
    pn_event_upload_complete_flag IN NUMBER,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_event_id	OUT event.event_id%TYPE,
    pn_exists OUT NUMBER,
	pn_host_id OUT event.host_id%TYPE,
    pd_event_start_ts IN event.event_start_ts%TYPE DEFAULT SYSDATE,
    pd_event_end_ts IN event.event_end_ts%TYPE DEFAULT SYSDATE    
)
IS
    ln_event_type_id event_type.event_type_id%TYPE;
    lv_event_global_trans_cd event.event_global_trans_cd%TYPE;
    ln_db_event_state_id event.event_state_id%TYPE;
    ld_db_event_end_ts event.event_end_ts%TYPE;
    ld_db_event_upload_complete_ts event.event_upload_complete_ts%TYPE;
    ln_update_event_flag NUMBER := PKG_CONST.BOOLEAN__FALSE;
    ln_update_event_state_flag NUMBER := PKG_CONST.BOOLEAN__FALSE;
    ln_host_type_id host.host_type_id%type;
	ln_device_id device.device_id%TYPE;
BEGIN
	pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;
    pn_exists := PKG_CONST.BOOLEAN__FALSE;

    BEGIN
		ln_device_id := PKG_DEVICE_CONFIGURATION.GET_DEVICE_ID_BY_NAME(pv_device_name, pd_event_start_ts);
	
        SELECT HOST_TYPE_ID, HOST_ID
        INTO ln_host_type_id, pn_host_id
        FROM DEVICE.HOST
        WHERE DEVICE_ID = ln_device_id
			AND HOST_PORT_NUM = pn_host_port_num;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            DECLARE
                ln_new_host_count NUMBER;              
            BEGIN
                PKG_DEVICE_CONFIGURATION.SP_CREATE_DEFAULT_HOSTS(ln_device_id, ln_new_host_count, pn_result_cd, pv_error_message);
                IF pn_result_cd != PKG_CONST.RESULT__SUCCESS THEN
                    RETURN;
                END IF;
                SELECT HOST_TYPE_ID, HOST_ID
                  INTO ln_host_type_id, pn_host_id
                  FROM DEVICE.HOST
                 WHERE DEVICE_ID = ln_device_id
                   AND HOST_PORT_NUM = pn_host_port_num;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    pn_result_cd := PKG_CONST.RESULT__HOST_NOT_FOUND;
            	pv_error_message := 'Unable to create event. No host_id exists for device_name: ' || pv_device_name || ', host_port_num: ' || pn_host_port_num || '.';
            	RETURN;
    		END;
    END;
    IF pn_event_type_id > -1 THEN
        ln_event_type_id := pn_event_type_id;
    ELSE
        BEGIN
            SELECT EVENT_TYPE_ID 
              INTO ln_event_type_id
              FROM DEVICE.EVENT_TYPE_HOST_TYPE
             WHERE HOST_TYPE_ID = ln_host_type_id
    	       AND EVENT_TYPE_HOST_TYPE_NUM = pn_event_type_host_type_num;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                pv_error_message := 'Unable to create event. No event_type_id for host_type_id: ' || ln_host_type_id || ', event_type_host_type_num: ' || pn_event_type_host_type_num || ' in device.event_type_host_type.';
                RETURN;
        END;
    END IF;
    
    IF pv_event_device_tran_cd IS NOT NULL THEN
        lv_event_global_trans_cd := SF_GET_GLOBAL_EVENT_CD(pc_global_event_cd_prefix, pv_device_name, pv_event_device_tran_cd);
        
        BEGIN
            SELECT event_id, event_state_id, event_end_ts, event_upload_complete_ts
            INTO pn_event_id, ln_db_event_state_id, ld_db_event_end_ts, ld_db_event_upload_complete_ts
            FROM device.event
    		WHERE event_global_trans_cd = lv_event_global_trans_cd;
	
           	pn_exists := PKG_CONST.BOOLEAN__TRUE;
           	
            IF ln_db_event_state_id != pn_event_state_id
                AND ln_db_event_state_id NOT IN (PKG_CONST.EVENT_STATE__COMPLETE_FINAL,
                    PKG_CONST.EVENT_STATE__COMPLETE_ERROR) THEN
				ln_update_event_flag := PKG_CONST.BOOLEAN__TRUE;
				ln_update_event_state_flag := PKG_CONST.BOOLEAN__TRUE;
			ELSIF pn_event_end_flag = PKG_CONST.BOOLEAN__TRUE
                AND ld_db_event_end_ts IS NULL THEN
				ln_update_event_flag := PKG_CONST.BOOLEAN__TRUE;
			ELSIF pn_event_upload_complete_flag = PKG_CONST.BOOLEAN__TRUE
                AND ld_db_event_upload_complete_ts IS NULL THEN
				ln_update_event_flag := PKG_CONST.BOOLEAN__TRUE;
			END IF;
    	
    	    IF ln_update_event_flag = PKG_CONST.BOOLEAN__TRUE THEN
        		UPDATE device.event
        		SET event_state_id = CASE ln_update_event_state_flag
                        WHEN PKG_CONST.BOOLEAN__TRUE THEN pn_event_state_id
                        ELSE event_state_id END,
					event_start_ts = CASE WHEN pd_event_start_ts < event_start_ts THEN pd_event_start_ts
						ELSE event_start_ts END,
        			event_end_ts = CASE pn_event_end_flag
                        WHEN PKG_CONST.BOOLEAN__TRUE THEN pd_event_end_ts
                        ELSE event_end_ts END,
        			event_upload_complete_ts = CASE pn_event_upload_complete_flag
                        WHEN PKG_CONST.BOOLEAN__TRUE THEN SYSDATE
                        ELSE event_upload_complete_ts END
        		WHERE event_id = pn_event_id;
    	    END IF;
    	
            pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    	    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
    	    RETURN;
	    EXCEPTION
            WHEN NO_DATA_FOUND THEN
                IF INSTR(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('MASTER_ID_UPDATE_SALE_TYPES'), 'E') > 0 AND TO_NUMBER_OR_NULL(pv_event_device_tran_cd) < DBADMIN.DATE_TO_MILLIS(SYSDATE + 365) / 1000 THEN
                    UPDATE DEVICE.DEVICE_SETTING 
                       SET DEVICE_SETTING_VALUE = pv_event_device_tran_cd
                     WHERE DEVICE_ID = ln_device_id
                       AND DEVICE_SETTING_PARAMETER_CD = '60' -- Master Id
                       AND TO_NUMBER_OR_NULL(NVL(DEVICE_SETTING_VALUE, '0')) < TO_NUMBER_OR_NULL(pv_event_device_tran_cd);
                END IF;
	    END;
    END IF;
    
    SELECT seq_event_id.NEXTVAL
    INTO pn_event_id FROM DUAL;
    
	INSERT INTO device.event (
		event_id,
		event_type_id,
		event_state_id,
		event_device_tran_cd,
		event_global_trans_cd,
		host_id,
		event_start_ts,
		event_end_ts,
		event_upload_complete_ts,
		global_session_cd
	) VALUES (
		pn_event_id,
        ln_event_type_id,
        pn_event_state_id,
        pv_event_device_tran_cd,
        lv_event_global_trans_cd,
        pn_host_id,
        pd_event_start_ts,
		CASE pn_event_end_flag
            WHEN PKG_CONST.BOOLEAN__TRUE THEN pd_event_end_ts
            ELSE NULL END,
		CASE pn_event_upload_complete_flag
            WHEN PKG_CONST.BOOLEAN__TRUE THEN SYSDATE
            ELSE NULL END,
		pv_global_session_cd
	);	
    
    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;

-- R32 signature
PROCEDURE SP_GET_OR_CREATE_EVENT
(
    pc_global_event_cd_prefix IN CHAR,
    pn_session_id IN ENGINE.DEVICE_SESSION.DEVICE_SESSION_ID%TYPE,
    pv_device_name IN device.device_name%TYPE,
    pn_host_port_num IN host.host_port_num%TYPE,
    pn_event_type_id IN event_type.event_type_id%TYPE,
    pn_event_type_host_type_num IN event_type_host_type.event_type_host_type_num%TYPE,
    pn_event_state_id IN event.event_state_id%TYPE,
    pv_event_device_tran_cd IN event.event_device_tran_cd%TYPE,
    pn_event_end_flag IN NUMBER,
    pn_event_upload_complete_flag IN NUMBER,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_event_id	OUT event.event_id%TYPE,
    pn_exists OUT NUMBER,
    pd_event_start_ts IN event.event_start_ts%TYPE DEFAULT SYSDATE,
    pd_event_end_ts IN event.event_end_ts%TYPE DEFAULT SYSDATE    
)
IS
	lv_global_session_cd VARCHAR2(100);
BEGIN
	SELECT GLOBAL_SESSION_CD
	INTO lv_global_session_cd
	FROM ENGINE.DEVICE_SESSION
	WHERE DEVICE_SESSION_ID = pn_session_id;
	
	SP_GET_OR_CREATE_EVENT(
		pc_global_event_cd_prefix,
		lv_global_session_cd,
		pv_device_name,
		pn_host_port_num,
		pn_event_type_id,
		pn_event_type_host_type_num,
		pn_event_state_id,
		pv_event_device_tran_cd,
		pn_event_end_flag,
		pn_event_upload_complete_flag,
		pn_result_cd,
		pv_error_message,
		pn_event_id,
		pn_exists,
		pd_event_start_ts,
		pd_event_end_ts
	);
END;

PROCEDURE SP_GET_OR_CREATE_EVENT_DETAIL
(
    pn_event_id IN event.event_id%TYPE,
    pn_event_detail_type_id IN event_detail.event_detail_type_id%TYPE,
    pv_event_detail_value IN event_detail.event_detail_value%TYPE,
    pv_event_detail_value_ts_ms IN NUMBER,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_event_detail_id	OUT event_detail.event_detail_id%TYPE,
    pn_exists OUT NUMBER
)
IS
BEGIN
    pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;
    pn_exists := PKG_CONST.BOOLEAN__FALSE;
    
    BEGIN
        SELECT event_detail_id, PKG_CONST.BOOLEAN__TRUE
        INTO pn_event_detail_id, pn_exists
        FROM
        (
            SELECT event_detail_id
            FROM device.event_detail
            WHERE event_id = pn_event_id
                AND event_detail_type_id = pn_event_detail_type_id
            ORDER BY event_detail_id DESC
        )
        WHERE ROWNUM = 1;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            SELECT seq_event_detail_id.NEXTVAL
            INTO pn_event_detail_id FROM DUAL;

        	INSERT INTO device.event_detail (
        	    event_detail_id,
        		event_id,
        		event_detail_type_id,
        		event_detail_value,
        		event_detail_value_ts
        	) VALUES (
        	    pn_event_detail_id,
        		pn_event_id,
                pn_event_detail_type_id,
                pv_event_detail_value,
                CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pv_event_detail_value_ts_ms) AS DATE)
        	);
    END;

    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;

PROCEDURE SP_GET_OR_CREATE_STTLMT_EVENT
(
    pn_event_id IN event.event_id%TYPE,
    pn_device_batch_id IN settlement_event.device_batch_id%TYPE,
    pn_device_new_batch_id IN settlement_event.device_new_batch_id%TYPE,
    pn_device_event_utc_ts_ms IN NUMBER,
    pn_device_event_utc_offset_min IN settlement_event.device_event_utc_offset_min%TYPE,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_exists OUT NUMBER
)
IS
BEGIN
    pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;
    pn_exists := PKG_CONST.BOOLEAN__FALSE;
    
    BEGIN
        SELECT PKG_CONST.BOOLEAN__TRUE
        INTO pn_exists
        FROM device.settlement_event
        WHERE event_id = pn_event_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            INSERT INTO device.settlement_event (
                event_id,
                device_batch_id,
                device_new_batch_id,
                device_event_utc_ts,
                device_event_utc_offset_min
            ) VALUES (
                pn_event_id,
                pn_device_batch_id,
                pn_device_new_batch_id,
                CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_device_event_utc_ts_ms) AS DATE),
                pn_device_event_utc_offset_min
            );
    END;
    
    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;

PROCEDURE SP_GET_OR_CREATE_HOST_COUNTER
(
    pn_event_id IN event.event_id%TYPE,
    pn_host_port_num IN host.host_port_num%TYPE,
    pv_host_counter_parameter IN host_counter.host_counter_parameter%TYPE,
    pv_host_counter_value IN host_counter.host_counter_value%TYPE,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_host_counter_id OUT host_counter.host_counter_id%TYPE,
    pn_exists OUT NUMBER
)
IS
BEGIN
    pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;
    pn_exists := PKG_CONST.BOOLEAN__FALSE;

    BEGIN
        SELECT hc.host_counter_id, PKG_CONST.BOOLEAN__TRUE
          INTO pn_host_counter_id, pn_exists
		  FROM device.host_counter_event hce
		  JOIN device.host_counter hc
		    ON hce.host_counter_id = hc.host_counter_id
		 WHERE hce.event_id = pn_event_id
		   AND hc.host_counter_parameter = pv_host_counter_parameter;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            SELECT seq_host_counter_id.NEXTVAL
            INTO pn_host_counter_id FROM DUAL;
            
            INSERT INTO device.host_counter (
                host_counter_id,
                host_id,
                host_counter_parameter,
                host_counter_value)
            SELECT pn_host_counter_id,
                   e.host_id,
                   pv_host_counter_parameter,
                   pv_host_counter_value
              FROM DEVICE.EVENT e
             WHERE e.EVENT_ID = pn_event_id;
            
            INSERT INTO device.host_counter_event(event_id, host_counter_id)
            VALUES(pn_event_id, pn_host_counter_id);
    END;
    
    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;

FUNCTION SF_GET_PREV_FILL_SETTLEMENT(
    pn_event_id EVENT.EVENT_ID%TYPE,
    pv_device_name IN device.device_name%TYPE
) RETURN EVENT.EVENT_ID%TYPE
IS
    ln_curr_event_id EVENT.EVENT_ID%TYPE := pn_event_id;
    ln_event_type_id EVENT.EVENT_TYPE_ID%TYPE;
BEGIN
    LOOP
        SELECT E.EVENT_ID, E.EVENT_TYPE_ID
          INTO ln_curr_event_id, ln_event_type_id
          FROM DEVICE.SETTLEMENT_EVENT NSE
          JOIN DEVICE.SETTLEMENT_EVENT SE ON NSE.DEVICE_BATCH_ID = SE.DEVICE_NEW_BATCH_ID
          JOIN DEVICE.EVENT E on SE.EVENT_ID = E.EVENT_ID
          JOIN DEVICE.HOST h ON e.HOST_ID = h.HOST_ID
          JOIN DEVICE.DEVICE d ON h.DEVICE_ID = d.DEVICE_ID 
         WHERE NSE.EVENT_ID = ln_curr_event_id
           AND D.DEVICE_NAME = pv_device_name;
		IF ln_event_type_id = 2 THEN
            RETURN ln_curr_event_id;
        END IF;	
        IF ln_curr_event_id IS NULL THEN
            EXIT;
        END IF;
    END LOOP; 
END;

FUNCTION SF_GET_PREV_FILL_SETTLEMENT(
    pn_event_id EVENT.EVENT_ID%TYPE
) RETURN EVENT.EVENT_ID%TYPE
IS
    lv_device_name DEVICE.DEVICE_NAME%TYPE;
BEGIN
    SELECT D.DEVICE_NAME
      INTO lv_device_name
      FROM DEVICE.EVENT E 
      JOIN DEVICE.HOST h ON e.HOST_ID = h.HOST_ID
      JOIN DEVICE.DEVICE d ON h.DEVICE_ID = d.DEVICE_ID 
     WHERE E.EVENT_ID = pn_event_id;
    RETURN SF_GET_PREV_FILL_SETTLEMENT(pn_event_id, lv_device_name);
END;

    FUNCTION GET_FILL_COUNTER_RESET_CD(
        pv_event_global_trans_cd EVENT.EVENT_GLOBAL_TRANS_CD%TYPE,
        pn_fill_version PLS_INTEGER,
        pn_device_type_id DEVICE_TYPE.DEVICE_TYPE_ID%TYPE,
        pv_event_auth_type EVENT_DETAIL.EVENT_DETAIL_VALUE%TYPE)
    RETURN COUNTER_RESET.COUNTER_RESET_CD%TYPE
    IS
    BEGIN
        IF pn_fill_version = 1 THEN
            RETURN 'N';
        ELSIF pn_fill_version = 3 THEN
            RETURN 'Y';
        ELSIF pn_fill_version = 2 THEN
            IF UPPER(pv_event_auth_type) = 'LOCAL' THEN
                IF pn_device_type_id = 1 THEN
                    RETURN 'C';
                ELSE
                    RETURN 'N';
                END IF;
            ELSIF UPPER(pv_event_auth_type) = 'MANUAL' THEN
                RETURN 'N';
            ELSE
                DECLARE
                    lc_counter_reset_flag COUNTER_RESET.COUNTER_RESET_CD%TYPE;
                BEGIN
                    SELECT DECODE(PA.ACTION_ID, 1, 'N', 2, 'A', 3, 'N', 4, 'A', 5, 'N', 6, 'C')
                      INTO lc_counter_reset_flag
                      FROM PSS.TRAN T
                      JOIN PSS.CONSUMER_ACCT_PERMISSION CAP ON T.CONSUMER_ACCT_ID = CAP.CONSUMER_ACCT_ID
                      JOIN PSS.PERMISSION_ACTION PA ON CAP.PERMISSION_ACTION_ID = PA.PERMISSION_ACTION_ID
                      JOIN DEVICE.DEVICE_TYPE_ACTION DTA ON PA.ACTION_ID = DTA.ACTION_ID 
                     WHERE T.TRAN_GLOBAL_TRANS_CD = pv_event_global_trans_cd
                       AND DTA.DEVICE_TYPE_ID = pn_device_type_id;
                    RETURN lc_counter_reset_flag;
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        IF UPPER(pv_event_auth_type) = 'NETWORK' THEN
                            RETURN '?';
                        ELSIF pn_device_type_id = 1 THEN
                            RETURN 'C';
                        ELSE
                            RETURN 'N';
                        END IF;
                END;          
            END IF;
        END IF;
    END;
    
    FUNCTION GET_FILL_COUNTER_RESET_CD_REQ(
        pn_event_id EVENT.EVENT_ID%TYPE)
    RETURN COUNTER_RESET.COUNTER_RESET_CD%TYPE
    IS
        lv_event_global_trans_cd EVENT.EVENT_GLOBAL_TRANS_CD%TYPE;
        ln_fill_version PLS_INTEGER;
        ln_device_type_id DEVICE_TYPE.DEVICE_TYPE_ID%TYPE;
        lv_event_auth_type EVENT_DETAIL.EVENT_DETAIL_VALUE%TYPE;
        lc_counter_reset_cd CHAR(1);
    BEGIN
        SELECT E.EVENT_GLOBAL_TRANS_CD, TO_NUMBER_OR_NULL(ED_V.EVENT_DETAIL_VALUE), D.DEVICE_TYPE_ID, ED_A.EVENT_DETAIL_VALUE
          INTO lv_event_global_trans_cd, ln_fill_version, ln_device_type_id, lv_event_auth_type
          FROM DEVICE.EVENT E
          JOIN DEVICE.EVENT_DETAIL ED_V ON E.EVENT_ID = ED_V.EVENT_ID AND ED_V.EVENT_DETAIL_TYPE_ID = 1 /* Event Type Version */
          LEFT OUTER JOIN DEVICE.EVENT_DETAIL ED_A ON E.EVENT_ID = ED_A.EVENT_ID AND ED_A.EVENT_DETAIL_TYPE_ID = 4 /* Event Auth Type */
          JOIN DEVICE.HOST H ON E.HOST_ID = H.HOST_ID
          JOIN DEVICE.DEVICE D ON H.DEVICE_ID = D.DEVICE_ID
         WHERE E.EVENT_ID = pn_event_id;
        lc_counter_reset_cd := GET_FILL_COUNTER_RESET_CD(lv_event_global_trans_cd, ln_fill_version, ln_device_type_id, lv_event_auth_type);
        IF lc_counter_reset_cd = '?' THEN
    		RAISE_APPLICATION_ERROR(-20601, 'Authorization for Network Auth Fill Event ''' || lv_event_global_trans_cd || ''' not uploaded yet');
    	END IF;
        RETURN lc_counter_reset_cd;
    END;   
END;
/

CREATE OR REPLACE PACKAGE BODY REPORT.PKG_REPORT_SP
IS
--
-- To modify this template, edit file PKGBODY.TXT in TEMPLATE
-- directory of SQL Navigator
--
-- Purpose: Briefly explain the functionality of the package body
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------
   -- Enter procedure, function bodies as shown below
   PROCEDURE get_unsent_dexfiles_bycust (
      p_cust_id   IN       varchar,
      p_cursor    IN OUT   curs_ref
   )
   AS
   BEGIN
      OPEN p_cursor
       FOR
          SELECT   a.dex_file_id, a.file_name, a.file_path, a.dex_date,
                   a.dex_type, a.create_dt, a.sent, a.eport_id, a.processed,
                   a.upd_dt
              FROM g4op.dex_file a, report.terminal t, report.vw_terminal_eport te
             WHERE t.customer_id = p_cust_id
               AND t.terminal_id = te.terminal_id
               AND te.eport_id = a.eport_id
               AND a.sent = 'N'
               AND a.dex_file_id IN (
                      SELECT dex_file_id
                        FROM g4op.dex_file
                       WHERE eport_id = a.eport_id
                         AND dex_date = (SELECT MAX (dex_date)
                                           FROM g4op.dex_file
                                          WHERE eport_id = a.eport_id))
          ORDER BY a.eport_id, a.dex_date DESC;
   END;
END;
/
CREATE OR REPLACE FUNCTION         DBADMIN.GET_AUTHED_CARD(
	l_parsed_acct_data IN VARCHAR2)
RETURN VARCHAR2 PARALLEL_ENABLE DETERMINISTIC IS
/*
SELECT '''' || REGEXP_REPLACE(AUTHORITY_PAYMENT_MASK_REGEX, '([^\\])\(\?\:', '\1(') || ''', -- ' || AUTHORITY_PAYMENT_MASK_DESC 
  FROM PSS.AUTHORITY_PAYMENT_MASK 
 WHERE AUTHORITY_ASSN_ID IS NOT NULL
*/
    lt_cc_regex VARCHAR2_TABLE :=  VARCHAR2_TABLE(
        '^;?(4[0-9]{15})=([0-9]{4})([0-9]{3})([0-9]*)(\?([\x00-\xFF])?)?$', -- Visa Credit Card on Track 2
        '^;?(5[1-5][0-9]{14})=([0-9]{4})([0-9]{3})([0-9]*)(\?([\x00-\xFF])?)?$', -- MasterCard Credit Card on Track 2
        '^;?(3[47][0-9]{13})=([0-9]{4})([0-9]{3})([0-9]*)(\?([\x00-\xFF])?)?$', -- American Express Credit Card on Track 2
        '^;?((6011|622[0-9]|64[4-9][0-9]|65[0-9]{2})[0-9]{12})=([0-9]{4})([0-9]{3})([0-9]*)(\?([\x00-\xFF])?)?$', -- Discover Card Credit Card on Track 2
        '^;?(36[0-9]{12})=([0-9]{4})([0-9]{3})([0-9]*)(\?([\x00-\xFF])?)?$', -- Diners Club International Credit Card on Track 2
        '^;?(30[0-5][0-9]{11})=([0-9]{4})([0-9]{3})([0-9]*)(\?([\x00-\xFF])?)?$', -- Diners Club Carte Blanche Credit Card on Track 2
        '^%?B(4[0-9]{15})\^([A-Za-z0-9 \/]{2,26})\^([0-9]{4}?)([0-9]{3}?)([A-Za-z0-9 \/]*)(\?([\x00-\xFF])?)$', -- Visa Credit Card on Track 1
        '^%?B(5[1-5][0-9]{14})\^([A-Za-z0-9 \/]{2,26})\^([0-9]{4}?)([0-9]{3}?)([A-Za-z0-9 \/]*)(\?([\x00-\xFF])?)$', -- MasterCard Credit Card on Track 1
        '^%?B(3[47][0-9]{13})\^([A-Za-z0-9 \/]{2,26})\^([0-9]{4}?)([0-9]{3}?)([A-Za-z0-9 \/]*)(\?([\x00-\xFF])?)$', -- American Express Credit Card on Track 1
        '^%?B((6011|622[0-9]|64[4-9][0-9]|65[0-9]{2})[0-9]{12})\^([A-Za-z0-9 \/]{2,26})\^([0-9]{4}?)([0-9]{3}?)([A-Za-z0-9 \/]*)(\?([\x00-\xFF])?)$', -- Discover Card Credit Card on Track 1
        '^%?B(36[0-9]{12})\^([A-Za-z0-9 \/]{2,26})\^([0-9]{4}?)([0-9]{3}?)([A-Za-z0-9 \/]*)(\?([\x00-\xFF])?)$', -- Diners Club International Credit Card on Track 1
        '^%?B(30[0-5][0-9]{11})\^([A-Za-z0-9 \/]{2,26})\^([0-9]{4}?)([0-9]{3}?)([A-Za-z0-9 \/]*)(\?([\x00-\xFF])?)$' -- Diners Club Carte Blanche Credit Card on Track 1
    ); 
    lv_auth_card_data VARCHAR2(19);
BEGIN
    SELECT REGEXP_REPLACE(l_parsed_acct_data, a.COLUMN_VALUE, '\1')
      INTO lv_auth_card_data
      FROM TABLE(lt_cc_regex) a
     WHERE REGEXP_LIKE(l_parsed_acct_data, a.COLUMN_VALUE)
       AND ROWNUM = 1;
    RETURN lv_auth_card_data;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN l_parsed_acct_data;
END; 
 
/


CREATE OR REPLACE FUNCTION         DBADMIN.USAT_MASK_EXP_DATA(pv_exp_data VARCHAR2)
   RETURN VARCHAR2 DETERMINISTIC PARALLEL_ENABLE
IS
      lv_exp_num VARCHAR2(4000);
BEGIN
      IF pv_exp_data IS NULL THEN
            RETURN NULL;
      END IF;
      
      lv_exp_num := REGEXP_SUBSTR(pv_exp_data, '[0-9]{4,}');

      IF lv_exp_num IS NULL THEN
            RETURN pv_exp_data;
      ELSE
            RETURN SUBSTR('********',1,LENGTH(lv_exp_num));
      END IF;
END;
/

CREATE OR REPLACE TRIGGER REPORT.T_TERMINAL_ALERT
AFTER INSERT ON REPORT.TERMINAL_ALERT
REFERENCING NEW AS NEW OLD AS OLD

BEGIN
    REPORT.RESET_DATE_INTERVAL_ALERTS();
END;
/

DROP INDEX REPORT.IDX_CREATE_DT;
DROP INDEX REPORT.IX_ACTIVITY_REF_CC_APPR_CODE;
DROP INDEX REPORT.IX_ACTIVITY_REF_DAY_RANGE_FUNC;
DROP INDEX REPORT.IX_ACTIVITY_REF_LOCATION_ID;
DROP INDEX REPORT.IX_ACT_REF_MONTH_RANGE_FUNC;
DROP INDEX REPORT.IX_ACT_REF_WEEK_RANGE_FUNC;
ALTER TABLE REPORT.ACTIVITY_REF DROP CONSTRAINT SYS_C0014471;

DROP INDEX REPORT.UIX_ACTIVITY_REF_REF_NBR;
DROP INDEX REPORT.BIX_ACTIVITY_REF_TRANS_TYPE;
DROP INDEX REPORT.BIX_ACTIVITY_REF_SETTLE_STATE;

CREATE INDEX REPORT.IX_AR_CREATE_DT_CARD_COMPANY ON REPORT.ACTIVITY_REF (CREATE_DT, CARD_COMPANY) TABLESPACE REPORT_INDX LOCAL ONLINE;

SELECT 'UPDATE REPORT.CCS_TRANSPORT SET CCS_TRANSPORT_NAME = CCS_TRANSPORT_NAME || '' - '' || ' || ROWNUM || ' WHERE CCS_TRANSPORT_ID = ' ||T.CCS_TRANSPORT_ID || ';'
FROM REPORT.CCS_TRANSPORT T
JOIN (
SELECT USER_ID, CCS_TRANSPORT_NAME
FROM REPORT.CCS_TRANSPORT 
WHERE CCS_TRANSPORT_STATUS_CD !='D'
group by USER_ID, CCS_TRANSPORT_NAME
having count(*) > 1) D ON T.USER_ID = D.USER_ID AND T.CCS_TRANSPORT_NAME = D.CCS_TRANSPORT_NAME;

UPDATE REPORT.CCS_TRANSPORT SET CCS_TRANSPORT_NAME = CCS_TRANSPORT_NAME || ' - ' || 1 WHERE CCS_TRANSPORT_ID = 1856;
UPDATE REPORT.CCS_TRANSPORT SET CCS_TRANSPORT_NAME = CCS_TRANSPORT_NAME || ' - ' || 2 WHERE CCS_TRANSPORT_ID = 1713;
UPDATE REPORT.CCS_TRANSPORT SET CCS_TRANSPORT_NAME = CCS_TRANSPORT_NAME || ' - ' || 3 WHERE CCS_TRANSPORT_ID = 1837;
UPDATE REPORT.CCS_TRANSPORT SET CCS_TRANSPORT_NAME = CCS_TRANSPORT_NAME || ' - ' || 4 WHERE CCS_TRANSPORT_ID = 1793;
UPDATE REPORT.CCS_TRANSPORT SET CCS_TRANSPORT_NAME = CCS_TRANSPORT_NAME || ' - ' || 5 WHERE CCS_TRANSPORT_ID = 1814;

COMMIT;

CREATE UNIQUE INDEX REPORT.UDX_CCS_TRANSPORT_NAME ON REPORT.CCS_TRANSPORT (DECODE(CCS_TRANSPORT_STATUS_CD,'D',NULL,TO_CHAR(USER_ID)||CCS_TRANSPORT_NAME)) 
  TABLESPACE REPORT_INDX ONLINE;

DROP INDEX REPORT.IX_EXPORT_BATCH_COMBO;


ALTER TABLE APP_USER.APP_USER_OBJECT_PERMISSION ADD CONSTRAINT FK_A_U_O_P_APP_OBJECT_TYPE_ID FOREIGN KEY (	APP_OBJECT_TYPE_ID) REFERENCES APP_USER.APP_OBJECT_TYPE(	APP_OBJECT_TYPE_ID)
;
ALTER TABLE APP_USER.SUBDOMAIN_NAME ADD CONSTRAINT FK_S_NAME_APP_OBJECT_TYPE_ID FOREIGN KEY (	APP_OBJECT_TYPE_ID) REFERENCES APP_USER.APP_OBJECT_TYPE(	APP_OBJECT_TYPE_ID)
;

DROP INDEX REPORT.IX_AR_TERMINAL_FILL_DATE;

DROP TABLE APP_LOG.SERVICE_STATUS;

ALTER TABLE AUTHORITY.BANORTE_AUTHORITY_ASSN
	MODIFY(CREATED_TS DATE)
	MODIFY(LAST_UPDATED_TS DATE)
;
ALTER TABLE AUTHORITY.WEBPOS_AUTHORITY_ASSN
	MODIFY(CREATED_TS DATE)
	MODIFY(LAST_UPDATED_TS DATE)
;
ALTER TABLE CORP.CUSTOMER
	DROP(PROFILE_ID)
;
DROP TABLE CORP.CUSTOMER_DEVICE;
DROP TABLE CORP.CUSTOMER_SETTING;
DROP TABLE CORP.CUSTOMER_SETTING_TYPE;

CREATE INDEX CORP.IX_DOC_PART_TS ON CORP.DOC(PARTITION_TS) LOCAL ONLINE
;
DROP TABLE RDW.ALERT_FACT;
DROP TABLE RDW.AUTH_FACT;
DROP TABLE RDW.CONFIG_FACT;
DROP TABLE RDW.DEVICE_CALL_FACT;
DROP TABLE RDW.FILL_FACT;
DROP TABLE RDW.LOCATION_BRIDGE;
DROP TABLE RDW.LINE_ITEM_FACT;

DROP TABLE RDW.ALERT_DIM;
DROP TABLE RDW.ALERT_TYPE_DIM;
DROP TABLE RDW.AUTH_DETAIL_DIM;
DROP TABLE RDW.AUTH_TYPE_DIM;
DROP TABLE RDW.AUTHORITY_TERMINAL_DIM;
DROP TABLE RDW.CONFIG_DIM;
DROP TABLE RDW.CONSUMER_DIM;
DROP TABLE RDW.DATE_DIM;
DROP TABLE RDW.DEVICE_CALL_DIM;
DROP TABLE RDW.DEVICE_CALL_TYPE_DIM;
DROP TABLE RDW.DEVICE_DIM;
DROP TABLE RDW.FILL_DIM;
DROP TABLE RDW.HOST_DIM;
DROP TABLE RDW.LINE_ITEM_DIM;
DROP TABLE RDW.LOCATION_DIM;
DROP TABLE RDW.PAYMENT_BATCH_DIM;
DROP TABLE RDW.PAYMENT_DIM;
DROP TABLE RDW.POS_DIM;
DROP TABLE RDW.TIME_DIM;
DROP TABLE RDW.TRAN_DIM;
DROP TABLE RDW.TRAN_RESULT_DIM;

DROP VIEW RDW.VW_CURRENT_DEVICE_CONFIG;
DROP VIEW RDW.VW_CURRENT_HOST_CONFIG;

ALTER TABLE CORP.MERCHANT
	MODIFY(MERCHANT_NBR VARCHAR2(255))
;

DROP TABLE CORP.SCR_UNPAID_MONIES;

DROP TABLE CORP.SETTLE_STATE;
DROP PUBLIC SYNONYM SETTLE_STATE;

CREATE SYNONYM CORP.SETTLE_STATE FOR REPORT.SETTLE_STATE;

DROP PUBLIC SYNONYM SEQ_UNPAID_MONIES_RPT_ID;

DROP PUBLIC SYNONYM VW_BATCH_AMOUNTS;
DROP VIEW CORP.VW_BATCH_AMOUNTS;
DROP PROCEDURE CORP.LICENSE_INS;
DROP PROCEDURE CORP.LICENSE_UPD; 


CREATE OR REPLACE PROCEDURE         DBADMIN.ADD_DAILY_PARTITION (
          v_table_owner VARCHAR2,
          v_table_name VARCHAR2,
          v_until DATE)
AUTHID CURRENT_USER
AS
    v_prefix  VARCHAR2(30);
    v_last_value LONG;
    v_last_part VARCHAR2(30);
    v_sql VARCHAR2(4000);
    v_sql3 VARCHAR2(4000);
    v_high_value DATE;
    v_new_value DATE;
      BEGIN
    -- Determine the prefix, the last partition name, and the last partition high value
        SELECT *
      INTO v_prefix, v_last_part, v_last_value
      FROM (
        SELECT SUBSTR(TABLE_NAME,1,14)||'_',PARTITION_NAME,HIGH_VALUE
          FROM ALL_TAB_PARTITIONS
         WHERE TABLE_OWNER = v_table_owner
           AND TABLE_NAME = v_table_name
         ORDER BY PARTITION_POSITION DESC)
     WHERE ROWNUM = 1;

    -- Calculate high value as Date
    EXECUTE IMMEDIATE 'SELECT ' || READ_LONG(v_last_value) || ' FROM DUAL' INTO v_high_value;

    -- Add all need partitions
    WHILE v_until >= v_high_value LOOP
         v_high_value := v_high_value + 1;
        v_new_value := v_high_value -1;
         v_sql := 'ALTER TABLE '||v_table_owner||'.'||v_table_name||' ADD PARTITION ""'
             ||v_prefix||TO_CHAR(v_new_value, 'YYYY_MM_DD')
             ||'"" VALUES LESS THAN (TO_DATE ('''
             ||TO_CHAR(v_high_value, 'YYYY/MM/DD')||''', ''YYYY/MM/DD''))';
         DBMS_OUTPUT.PUT_LINE (v_sql);
         EXECUTE IMMEDIATE v_sql;
       --   v_sql3 := 'begin'||chr(10)||'sys.dbms_stats.gather_table_stats(ownname=>'''||v_table_owner||''',tabname=> '''||v_table_name||''',partname=> '''||v_prefix||TO_CHAR(v_new_value, 'YYYY_MM_DD')||''',estimate_percent=> DBMS_STATS.AUTO_SAMPLE_SIZE,cascade=> TRUE);'||chr(10)||'end;';
        -- DBMS_OUTPUT.PUT_LINE (v_sql3);
        -- EXECUTE IMMEDIATE v_sql3;
    END LOOP;
END;
/
CREATE OR REPLACE PROCEDURE         DBADMIN.ADD_RANGEID_PARTITION (
    pv_table_owner VARCHAR2,
    pv_table_name VARCHAR2,
    pn_increment NUMBER)

AUTHID CURRENT_USER
AS
    v_sql VARCHAR2(4000);
    v_prefix VARCHAR2(30);
    n_last_num NUMBER;
    l_last_value LONG;
    n_high_value NUMBER;
    v_sql3 VARCHAR2(4000);
BEGIN
    SELECT *
      INTO v_prefix, n_last_num, l_last_value
      FROM (
        SELECT SUBSTR(PARTITION_NAME, 1, INSTR(PARTITION_NAME, '_', -1)) PARTITION_PREFIX,
               TO_NUMBER(SUBSTR(PARTITION_NAME, INSTR(PARTITION_NAME, '_', -1) + 1, 100)) PARTITION_NUMBER,
               HIGH_VALUE
          FROM ALL_TAB_PARTITIONS
         WHERE TABLE_OWNER = pv_table_owner
           AND TABLE_NAME = pv_table_name
         ORDER BY PARTITION_POSITION DESC)
     WHERE ROWNUM = 1;

    -- Calculate high value as Number
    EXECUTE IMMEDIATE 'SELECT ' || READ_LONG(l_last_value) || ' FROM DUAL' INTO n_high_value;

    --Add partition
    n_high_value := n_high_value + pn_increment;
    n_last_num := n_last_num + 1;
    v_sql := 'ALTER TABLE '||pv_table_owner||'.'||pv_table_name||' ADD PARTITION ""'
             ||v_prefix||TO_CHAR(n_last_num, 'FM9999999999999990')
             ||'"" VALUES LESS THAN ('
             ||TO_CHAR(n_high_value)||')';
    DBMS_OUTPUT.PUT_LINE (v_sql);
    EXECUTE IMMEDIATE v_sql;
   -- v_sql3 := 'begin'||chr(10)||'sys.dbms_stats.gather_table_stats(ownname=>'''||pv_table_owner||''',tabname=> '''||pv_table_name||''',partname=> '''||v_prefix||TO_CHAR(n_last_num, 'FM9999999999999990')||''',estimate_percent=> DBMS_STATS.AUTO_SAMPLE_SIZE,cascade=> TRUE);'||chr(10)||'end;';
       --  DBMS_OUTPUT.PUT_LINE (v_sql3);
      --   EXECUTE IMMEDIATE v_sql3;
END;
/
CREATE OR REPLACE PROCEDURE         DBADMIN.ADD_WEEKLY_PARTITION (
          v_table_owner VARCHAR2,
          v_table_name VARCHAR2,
          v_until DATE)
AUTHID CURRENT_USER
AS
    v_prefix  VARCHAR2(30);
    v_last_num NUMBER;
    v_last_value LONG;
    v_sql VARCHAR2(4000);
    v_high_value DATE;
    v_sql3 VARCHAR2(4000);
BEGIN
    -- Determine the prefix, the last partition number, and the last partition high value
    SELECT *
      INTO v_prefix, v_last_num, v_last_value
      FROM (
        SELECT SUBSTR(PARTITION_NAME, 1, INSTR(PARTITION_NAME, 'P', -1)) PARTITION_PREFIX,
               TO_NUMBER(SUBSTR(PARTITION_NAME, INSTR(PARTITION_NAME, 'P', -1) + 1, 100))

PARTITION_NUMBER,
               HIGH_VALUE
          FROM ALL_TAB_PARTITIONS
         WHERE TABLE_OWNER = v_table_owner
           AND TABLE_NAME = v_table_name
         ORDER BY PARTITION_POSITION DESC)
     WHERE ROWNUM = 1;

    -- Calculate high value as Date
    EXECUTE IMMEDIATE 'SELECT ' || READ_LONG(v_last_value) || ' FROM DUAL' INTO v_high_value;

    -- Add all need partitions
    WHILE v_until >= v_high_value LOOP
         v_high_value := v_high_value + 7;
         v_last_num := v_last_num + 1;
         v_sql := 'ALTER TABLE '||v_table_owner||'.'||v_table_name||' ADD PARTITION ""'
             ||v_prefix||TO_CHAR(v_last_num, 'FM9999999999999990')
             ||'"" VALUES LESS THAN (TO_DATE ('''
             ||TO_CHAR(v_high_value, 'YYYY/MM/DD')||''', ''YYYY/MM/DD''))';
         DBMS_OUTPUT.PUT_LINE (v_sql);
         EXECUTE IMMEDIATE v_sql;
      --    v_sql3 := 'begin'||chr(10)||'sys.dbms_stats.gather_table_stats(ownname=>'''||v_table_owner||''',tabname=> '''||v_table_name||''',partname=> '''||v_prefix||TO_CHAR(v_last_num, 'FM9999999999999990')||''',estimate_percent=> DBMS_STATS.AUTO_SAMPLE_SIZE,cascade=> TRUE);'||chr(10)||'end;';
        -- DBMS_OUTPUT.PUT_LINE (v_sql3);
       --  EXECUTE IMMEDIATE v_sql3;
    END LOOP;
END;
/

CREATE OR REPLACE PROCEDURE         DBADMIN.GATHER_PART_STATISTICS AUTHID CURRENT_USER is
--Created by Marie Njanje on 31/05/11 to gather stats on partitions
Begin
DECLARE
   CURSOR part_list_cursor IS
          SELECT table_owner, table_name, partition_name
          FROM ALL_TAB_PARTITIONS
         WHERE table_owner not in('SYS', 'SYSTEM','REPORT_ARCH') and table_name not like 'BIN$%' and last_analyzed is null and table_name<>'FILE_TRANSFER';
      part_rec part_list_cursor%ROWTYPE;
      v_table_owner VARCHAR2(30);
      v_table_name VARCHAR2(30);
      v_part_name VARCHAR2(30);
      v_sql VARCHAR2(4000);
      BEGIN
      FOR part_rec IN part_list_cursor LOOP
      v_table_owner := part_rec.table_owner;
      v_table_name := part_rec.table_name;
      v_part_name := part_rec.partition_name;
         v_sql := 'begin'||chr(10)||'sys.dbms_stats.gather_table_stats(ownname=>'''||v_table_owner||''',tabname=> '''||v_table_name||''',partname=> ''""'||v_part_name||'""'',estimate_percent=> DBMS_STATS.AUTO_SAMPLE_SIZE,granularity=> ''PARTITION'', cascade=> TRUE);'||chr(10)||'end;';
         DBMS_OUTPUT.PUT_LINE (v_sql);
         EXECUTE IMMEDIATE v_sql;
    END LOOP;
END;
END;
/



CREATE OR REPLACE PROCEDURE         DBADMIN.USAT_CHECK_CREATE_MONTHLY_PART AUTHID CURRENT_USER is
--Created by Marie Njanje on 01/14/08 to check partitions daily
Begin
DECLARE
   CURSOR part_list_cursor IS
  select b.table_owner, b.table_name, partition_name, num_rows, high_value
    from dba_tab_partitions b,
    ( select T.table_owner, T.table_name, max(T.PARTITION_POSITION) part_pos
    from dba_tab_partitions T, DBA_PART_KEY_COLUMNS K
    where table_owner not in ('SYS', 'SYSTEM','REPORT_ARCH','RDW') and table_name not like 'BIN$%'
    AND (K.COLUMN_NAME like '%DATE' or  K.COLUMN_NAME like '%TS' or K.COLUMN_NAME like '%DAY')
    AND TABLE_NAME NOT IN ('MACHINE_CMD_OUTBOUND_HIST','MACHINE_CMD_INBOUND_HIST','APP_LOCK','DEVICE_CALL_IN_RECORD','HOST_COUNTER','STORED_FILE') AND T.TABLE_NAME=K.NAME AND TABLE_OWNER=K.OWNER
    group by table_owner, table_name ) a
    where a.part_pos = PARTITION_POSITION
    and a.table_owner = b.table_owner
    and a.table_name = b.table_name
    and b.table_owner not in('SYS', 'SYSTEM');

     part_rec part_list_cursor%ROWTYPE;
     v_table_owner  VARCHAR2(30);
     v_table_name  VARCHAR2(30);
     v_part_name VARCHAR2(30);
     high_value LONG;
     v_high_value LONG;
     v_highest_value DATE;
     v_sql2 VARCHAR2(4000);
     v_added_value constant VARCHAR2(30):= 'SYSDATE';

BEGIN
     FOR part_rec IN part_list_cursor LOOP
     v_table_owner := part_rec.table_owner;
     v_table_name := part_rec.table_name;
     v_part_name := part_rec.partition_name;
     v_high_value := part_rec.high_value;
            IF part_rec.HIGH_VALUE='MAXVALUE' THEN
            DBMS_OUTPUT.PUT_LINE ('MAXVALUE is REACHED for table '||upper(v_table_owner)||'.'||upper(v_table_name)||', please review table definition!');
            ELSE
                   EXECUTE IMMEDIATE 'SELECT ' || READ_LONG(part_rec.HIGH_VALUE) || ' FROM DUAL' INTO v_highest_value;
           IF v_highest_value < SYSDATE +14 THEN

                        v_sql2 := 'begin'||chr(10)||'DBADMIN.ADD_MONTHLY_PARTITION ('''||v_table_owner||''','''||v_table_name||''','||v_added_value||');'||chr(10)||'end;';

                      EXECUTE IMMEDIATE v_sql2;

           END IF;
       END IF;
    END LOOP;
END;
END;
/
CREATE OR REPLACE PROCEDURE         DBADMIN.USAT_CHECK_CREATE_PARTS_ID_DEV AUTHID CURRENT_USER is
--Created by Marie Njanje on 10/24/08 to check partitions with *ID as key daily
Begin
DECLARE

   CURSOR part_list_cursor IS
    select b.table_owner, b.table_name, partition_name, high_value, p.column_name
    from dba_tab_partitions b,DBA_PART_KEY_COLUMNS p,
    ( select T.table_owner, T.table_name, max(T.PARTITION_POSITION) part_pos,k.column_name
    from dba_tab_partitions T, DBA_PART_KEY_COLUMNS K
    where table_owner in('DEVICE') and table_name not like 'BIN$%'
    AND K.COLUMN_NAME like '%ID' AND T.TABLE_NAME=K.NAME AND T.TABLE_OWNER=K.OWNER
    group by table_owner, table_name,k.column_name) a
    where b.table_name=p.name and b.TABLE_OWNER=p.OWNER and  a.part_pos = PARTITION_POSITION
    and a.column_name=p.column_name
    and a.table_owner = b.table_owner
    and a.table_name = b.table_name
    and b.table_owner in('DEVICE') and b.table_name <>'ADMIN_CMD';

           part_rec part_list_cursor%ROWTYPE;
     v_table_owner  VARCHAR2(30);
     v_table_name  VARCHAR2(30);
     v_part_name VARCHAR2(30);
     v_col_name VARCHAR2(30);
     high_value LONG;
     v_high_value LONG;
     v_highest_value DATE;
     v_highest_value_num NUMBER;
     tab_high_value NUMBER;
     v_sql2 VARCHAR2(4000);
     v_added_value constant NUMBER (8) :=2000000;


BEGIN
     FOR part_rec IN part_list_cursor LOOP
     v_table_owner := part_rec.table_owner;
     v_table_name := part_rec.table_name;
     v_part_name := part_rec.partition_name;
     v_col_name := part_rec.column_name;
     v_high_value := part_rec.high_value;

            IF part_rec.HIGH_VALUE='MAXVALUE' THEN
            DBMS_OUTPUT.PUT_LINE ('MAXVALUE is REACHED for table '||upper(v_table_owner)||'.'||upper(v_table_name)||', please review table definition!');
            ELSE
                   EXECUTE IMMEDIATE 'SELECT ' || READ_LONG(part_rec.HIGH_VALUE) || ' FROM DUAL' INTO v_highest_value_num;
                   EXECUTE IMMEDIATE 'SELECT max('||v_col_name||') from '||v_table_owner||'.'||v_table_name||' ' INTO tab_high_value;

                    IF v_highest_value_num < tab_high_value + 5000000 THEN
                      v_sql2 := 'begin'||chr(10)||'DBADMIN.ADD_RANGEID_PARTITION ('''||v_table_owner||''','''||v_table_name||''','||v_added_value||');'||chr(10)||'end;';
                       EXECUTE IMMEDIATE v_sql2;
                      END IF;
       END IF;
    END LOOP;
END;
END;
/
CREATE OR REPLACE PROCEDURE         DBADMIN.USAT_CHECK_CREATE_PARTS_ID_ENG AUTHID CURRENT_USER is
--Created by Marie Njanje on 10/24/08 to check partitions with *ID as key daily
Begin
DECLARE

   CURSOR part_list_cursor IS
    select b.table_owner, b.table_name, partition_name, high_value, p.column_name
    from dba_tab_partitions b,DBA_PART_KEY_COLUMNS p,
    ( select T.table_owner, T.table_name, max(T.PARTITION_POSITION) part_pos,k.column_name
    from dba_tab_partitions T, DBA_PART_KEY_COLUMNS K
    where table_owner in('ENGINE') and table_name not like 'BIN$%'
    AND K.COLUMN_NAME like '%ID' AND T.TABLE_NAME=K.NAME AND T.TABLE_OWNER=K.OWNER
    group by table_owner, table_name,k.column_name) a
    where b.table_name=p.name and b.TABLE_OWNER=p.OWNER and  a.part_pos = PARTITION_POSITION
    and a.column_name=p.column_name
    and a.table_owner = b.table_owner
    and a.table_name = b.table_name
    and b.table_owner in('ENGINE') and b.table_name <>'ADMIN_CMD';

           part_rec part_list_cursor%ROWTYPE;
     v_table_owner  VARCHAR2(30);
     v_table_name  VARCHAR2(30);
     v_part_name VARCHAR2(30);
     v_col_name VARCHAR2(30);
     high_value LONG;
     v_high_value LONG;
     v_highest_value DATE;
     v_highest_value_num NUMBER;
     tab_high_value NUMBER;
     v_sql2 VARCHAR2(4000);
     v_added_value constant NUMBER (8) :=2000000;


BEGIN
     FOR part_rec IN part_list_cursor LOOP
     v_table_owner := part_rec.table_owner;
     v_table_name := part_rec.table_name;
     v_part_name := part_rec.partition_name;
     v_col_name := part_rec.column_name;
     v_high_value := part_rec.high_value;

            IF part_rec.HIGH_VALUE='MAXVALUE' THEN
            DBMS_OUTPUT.PUT_LINE ('MAXVALUE is REACHED for table '||upper(v_table_owner)||'.'||upper(v_table_name)||', please review table definition!');
            ELSE
                   EXECUTE IMMEDIATE 'SELECT ' || READ_LONG(part_rec.HIGH_VALUE) || ' FROM DUAL' INTO v_highest_value_num;
                   EXECUTE IMMEDIATE 'SELECT max('||v_col_name||') from '||v_table_owner||'.'||v_table_name||' ' INTO tab_high_value;

                    IF v_highest_value_num < tab_high_value + 5000000 THEN
                      v_sql2 := 'begin'||chr(10)||'DBADMIN.ADD_RANGEID_PARTITION ('''||v_table_owner||''','''||v_table_name||''','||v_added_value||');'||chr(10)||'end;';
                       EXECUTE IMMEDIATE v_sql2;
                      END IF;
       END IF;
    END LOOP;
END;
END;
/

DROP TABLE DEVICE.DEVICE_CALL_IN_RECORD;

DROP TABLE DEVICE.EXT_DEVICE_FILE_TRANS;

DROP TABLE DEVICE.EXT_FILE;

DROP TABLE DEVICE.EXT_FILE_CRC_TYPE;

DROP TABLE DEVICE.EXT_FILE_ENCAP;

DROP TABLE DEVICE.EXT_FILE_TRANS_SRV;

DROP TABLE DEVICE.EXT_FILE_TYPE;

DROP TABLE ENGINE.DEVICE_MESSAGE;

DROP TABLE ENGINE.DEVICE_SESSION;

ALTER TABLE FOLIO_CONF.FILTER_PARAM
	MODIFY(PARAM_NAME VARCHAR2(50))
;

ALTER TABLE FOLIO_CONF.FOLIO
	MODIFY(FOLIO_TITLE VARCHAR2(250))
	MODIFY(FOLIO_SUBTITLE VARCHAR2(250))
;

CREATE OR REPLACE PROCEDURE      G4OP.LOAD_DEX_FILES 
    IS
        CURSOR l_dex_cur IS
            SELECT DF.DEX_FILE_ID, TE.TERMINAL_ID, DF.DEX_DATE, DF.DEX_TYPE,
                   DECODE(DF.DEX_TYPE, 4, 'P', 'N') SENT
              FROM DEX_FILE DF, REPORT.TERMINAL_EPORT TE
             WHERE DF.PROCESSED = 'N'
               AND DF.EPORT_ID = TE.EPORT_ID
               AND WITHIN1(DF.DEX_DATE, TE.START_DATE,TE.END_DATE) = 1;
    BEGIN
    	FOR l_dex_rec IN l_dex_cur LOOP
    	    DECLARE
                CURSOR l_line_cur IS
                    SELECT DCA.ALERT_ID, DFL.DEX_CODE, DFL.DETAILS
                      FROM DEX_FILE_LINE DFL, DEX_CODE_ALERT DCA
                     WHERE DFL.DEX_FILE_ID = l_dex_rec.DEX_FILE_ID
                       AND DFL.DEX_CODE = DCA.DEX_CODE
                     GROUP BY DCA.ALERT_ID, DFL.DEX_CODE, DFL.DETAILS
                     HAVING COUNT(*) > (SELECT NVL(COUNT(*), 0)
                        FROM DEX_FILE_LINE DFL1
                        WHERE PREVIOUS_FILE_ID(l_dex_rec.DEX_FILE_ID) = DFL1.DEX_FILE_ID
                        AND DFL.DEX_CODE = DFL1.DEX_CODE
                        AND NVL(DFL.DETAILS, ' ') = NVL(DFL1.DETAILS, ' '));
            BEGIN
                FOR l_line_rec IN l_line_cur LOOP
                    REPORT.TERMINAL_ALERT_INS(
                        l_line_rec.ALERT_ID,
                        l_dex_rec.TERMINAL_ID,
                        l_dex_rec.DEX_DATE,
                        l_line_rec.DETAILS,
                        l_dex_rec.SENT);
    	        END LOOP;
    	        UPDATE DEX_FILE SET PROCESSED = 'Y' WHERE DEX_FILE_ID = l_dex_rec.DEX_FILE_ID;
    	        COMMIT;
            EXCEPTION
                WHEN OTHERS THEN
                    DECLARE
                        l_msg VARCHAR2(4000) := SQLERRM;
                        l_err_num NUMBER := SQLCODE;
                    BEGIN
                        ROLLBACK;
                        UPDATE DEX_FILE SET PROCESSED = 'E' WHERE DEX_FILE_ID = l_dex_rec.DEX_FILE_ID;
                        COMMIT;
                         INSERT INTO STG_PROCESSING_ERRORS(STG_TABLE_ID, TRANS_TYPE, ERROR_COLUMN, ERROR_MSG, ERROR_CODE)
                            VALUES(l_dex_rec.DEX_FILE_ID, 50, NULL, l_msg, l_err_num);
                            commit;
                    END;
            END;
         END LOOP;
     END; 
/

DROP TYPE G4OP.NUMBER_LIST;
DROP TYPE G4OP.STRING_LIST;

ALTER TABLE PSS.BANORTE_AUTHORITY
	MODIFY(CREATED_TS DATE)
	MODIFY(LAST_UPDATED_TS DATE)
;

ALTER TABLE PSS.BANORTE_TERMINAL
	MODIFY(CREATED_TS DATE)
	MODIFY(LAST_UPDATED_TS DATE)
;

ALTER TABLE PSS.BLACKBRD_AUTHORITY
	MODIFY(CREATED_TS DATE)
	MODIFY(LAST_UPDATED_TS DATE)
;
DROP FUNCTION PSS.CALC_LUHN;

CREATE UNIQUE INDEX PSS.UDX_CONSUMER_ACCT_PERMISSION_1 ON PSS.CONSUMER_ACCT_PERMISSION(
	CONSUMER_ACCT_ID,
	CONSUMER_ACCT_PERMISSION_ORDER,
	PERMISSION_ACTION_ID)
;
DROP FUNCTION PSS.GET_DEVICE_TYPE_ACTION_CD;
DROP PACKAGE PSS.PKG_MERCHANT_MAINT;
ALTER TABLE PSS.SALE
	MODIFY(HASH_TYPE_CD DEFAULT NULL)
	MODIFY(TRAN_LINE_ITEM_HASH DEFAULT NULL)
;

ALTER TABLE DEVICE.DEVICE
	MODIFY(DEVICE_ENCR_KEY_GEN_TS DEFAULT NULL)
;
DROP INDEX SYSTEM.IDX_SETTLEMENT_BATCH_1;
CREATE INDEX PSS.IDX_SETTLEMENT_BATCH_1 ON PSS.SETTLEMENT_BATCH(
	TERMINAL_BATCH_ID)
;
ALTER TABLE PSS.TERMINAL
	MODIFY(TERMINAL_NEXT_BATCH_NUM NUMBER(22))
;


DROP INDEX PSS.IDX_TRAN_DEVICE_TRAN_START_TS;

DROP INDEX PSS.TRANCONSACCTID;

CREATE INDEX PSS.INX_TRANCONSACCTID ON PSS.TRAN(
	CONSUMER_ACCT_ID) LOCAL
;


ALTER TABLE PSS.TRAN_LINE_ITEM rename COLUMN TRAN_LINE_ITEM_QUANTITY TO OLD_TRAN_LINE_ITEM_QUANTITY;
ALTER TABLE PSS.TRAN_LINE_ITEM ADD(TRAN_LINE_ITEM_QUANTITY NUMBER(22));
UPDATE PSS.TRAN_LINE_ITEM SET TRAN_LINE_ITEM_QUANTITY = OLD_TRAN_LINE_ITEM_QUANTITY where TRAN_LINE_ITEM_QUANTITY IS NULL;
COMMIT;
ALTER TABLE PSS.TRAN_LINE_ITEM DROP COLUMN OLD_TRAN_LINE_ITEM_QUANTITY;

ALTER TABLE PSS.WEBPOS_AUTHORITY
	MODIFY(CREATED_TS DATE)
	MODIFY(LAST_UPDATED_TS DATE)
;
ALTER TABLE PSS.WEBPOS_TERMINAL
	MODIFY(CREATED_TS DATE)
	MODIFY(LAST_UPDATED_TS DATE)
;

CREATE OR REPLACE TRIGGER PSS.TRBI_CONSUMER_ACCT_FMT
BEFORE INSERT ON PSS.CONSUMER_ACCT_FMT
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
BEGIN
   IF :NEW.consumer_acct_fmt_id IS NULL
   THEN
      SELECT SEQ_CONSUMER_ACCT_FMT_ID.NEXTVAL
        INTO :NEW.consumer_acct_fmt_id
        FROM DUAL;
   END IF;
   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER PSS.TRBI_TRAN_STAT_TYPE
BEFORE INSERT ON PSS.TRAN_STAT_TYPE
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
BEGIN
	IF :NEW.TRAN_STAT_TYPE_ID IS NULL
	THEN
		SELECT SEQ_TRAN_STAT_TYPE_ID.NEXTVAL
		INTO :NEW.TRAN_STAT_TYPE_ID
		FROM DUAL;
	END IF;
	SELECT
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

ALTER TABLE REPORT.ACTIVITY_REF
	ADD(MONTH_RANGE DATE)
	ADD(WEEK_RANGE DATE)
	ADD(DAY_RANGE DATE )
	MODIFY(CREATE_DT DEFAULT sysdate NULL)
;
ALTER TABLE REPORT.ACTIVITY_REF RENAME COLUMN CURRENCY_ID TO OLD_CURRENCY_ID;
ALTER TABLE REPORT.ACTIVITY_REF ADD (CURRENCY_ID NUMBER(22));
UPDATE REPORT.ACTIVITY_REF SET CURRENCY_ID = OLD_CURRENCY_ID WHERE CURRENCY_ID IS NULL;
COMMIT;
ALTER TABLE REPORT.ACTIVITY_REF DROP COLUMN OLD_CURRENCY_ID;

select * from report.activity_ref order by tran_id desc;

DROP INDEX REPORT.IX_ACTIVITY_REF_FILL_DATE;
CREATE INDEX REPORT.IX_AR_TERMINAL_FILL_DATE ON REPORT.ACTIVITY_REF(
	TERMINAL_ID,
	FILL_DATE)
;

ALTER TABLE PSS.TRAN_LINE_ITEM MODIFY(TRAN_LINE_ITEM_QUANTITY NUMBER);

UPDATE PSS.TRAN_LINE_ITEM XI
   SET XI.TRAN_LINE_ITEM_QUANTITY = NVL((SELECT S.SALE_AMOUNT / XI.TRAN_LINE_ITEM_AMOUNT FROM PSS.SALE S WHERE S.TRAN_ID = XI.TRAN_ID), XI.TRAN_LINE_ITEM_QUANTITY)
WHERE TRAN_LINE_ITEM_TYPE_ID IN(554,555);

ALTER TABLE PSS.BLACKBRD_AUTHORITY
	ADD(BLACKBRD_SEQUENCE_NUM NUMBER(6) DEFAULT 1)
;
DROP INDEX PSS.UDX_CONSUMER_ACCT_PERMISSION_1;
CREATE UNIQUE INDEX PSS.UDX_CONSUMER_ACCT_PERMISSION_1 ON PSS.CONSUMER_ACCT_PERMISSION(
	CONSUMER_ACCT_ID,
	PERMISSION_ACTION_ID,
	CONSUMER_ACCT_PERMISSION_ORDER)
;


DROP INDEX PSS.IX_MV_EST_CAMPUS_ID;
CREATE INDEX PSS.IX_MV_EST_CAMPUS_ID ON PSS.MV_ESUDS_SETTLED_TRAN(
	CAMPUS_ID)
LOCAL
;

DROP INDEX PSS.IX_MV_EST_CONSUMER_ACCT_ID;
CREATE INDEX PSS.IX_MV_EST_CONSUMER_ACCT_ID ON PSS.MV_ESUDS_SETTLED_TRAN(
	CONSUMER_ACCT_ID)
LOCAL
;

DROP INDEX PSS.IX_MV_EST_CPT_CD;
CREATE INDEX PSS.IX_MV_EST_CPT_CD ON PSS.MV_ESUDS_SETTLED_TRAN(
	CLIENT_PAYMENT_TYPE_CD)
LOCAL
;

DROP INDEX PSS.IX_MV_EST_DORM_ID;
CREATE INDEX PSS.IX_MV_EST_DORM_ID ON PSS.MV_ESUDS_SETTLED_TRAN(
	DORM_ID)
LOCAL
;

DROP INDEX PSS.IX_MV_EST_OPERATOR_ID;
CREATE INDEX PSS.IX_MV_EST_OPERATOR_ID ON PSS.MV_ESUDS_SETTLED_TRAN(
	OPERATOR_ID)
LOCAL
;

DROP INDEX PSS.IX_MV_EST_SB_TS;
CREATE INDEX PSS.IX_MV_EST_SB_TS ON PSS.MV_ESUDS_SETTLED_TRAN(
	SETTLEMENT_BATCH_TS)
LOCAL
;

DROP INDEX PSS.IX_MV_EST_SCHOOL_ID;
CREATE INDEX PSS.IX_MV_EST_SCHOOL_ID ON PSS.MV_ESUDS_SETTLED_TRAN(
	SCHOOL_ID)
LOCAL
;

DROP INDEX PSS.IX_MV_EST_SCHOOL_ID_SB_TS;
CREATE INDEX PSS.IX_MV_EST_SCHOOL_ID_SB_TS ON PSS.MV_ESUDS_SETTLED_TRAN(
	SCHOOL_ID,
	SETTLEMENT_BATCH_TS)
LOCAL
;

DROP INDEX PSS.IX_MV_EST_SETTLE_BATCH_ID;
CREATE INDEX PSS.IX_MV_EST_SETTLE_BATCH_ID ON PSS.MV_ESUDS_SETTLED_TRAN(
	SETTLEMENT_BATCH_ID)
LOCAL
;

DROP INDEX PSS.IX_MV_EST_TRAN_DAY;
CREATE INDEX PSS.IX_MV_EST_TRAN_DAY ON PSS.MV_ESUDS_SETTLED_TRAN(
	TRUNC(TRAN_START_TS,'fmdd'))
LOCAL
;

DROP INDEX PSS.IX_MV_EST_TRAN_START_TS;
CREATE INDEX PSS.IX_MV_EST_TRAN_START_TS ON PSS.MV_ESUDS_SETTLED_TRAN(
	TRAN_START_TS)
LOCAL
;

DROP TABLE RDW.LOCATION_DIM;

ALTER TABLE REPORT.ACTIVITY_REF MODIFY (CURRENCY_ID NUMBER);

ALTER TABLE REPORT.ACTIVITY_REF ADD CONSTRAINT FK_ACTIVITY_REF_CURRENCY_ID FOREIGN KEY (CURRENCY_ID) REFERENCES CORP.CURRENCY(CURRENCY_ID);

CREATE UNIQUE INDEX REPORT.AK_ALERT_NOTIF_ALERT_TYPE ON REPORT.ALERT_NOTIF_ALERT_TYPE(
	ALERT_NOTIF_ID,
	ALERT_TYPE_ID)
;

CREATE UNIQUE INDEX REPORT.AK_ALERT_NOTIF_TERMINAL ON REPORT.ALERT_NOTIF_TERMINAL(
	ALERT_NOTIF_ID,
	TERMINAL_ID)
;
CREATE TABLE REPORT.AUDIT_TERMINAL_EPORT(
	TERMINAL_EPORT_ID NUMBER(22),
	MODULE VARCHAR2(4000),
	HOST VARCHAR2(4000),
	IP_ADDRESS VARCHAR2(4000),
	NETWORK_PROTOCOL VARCHAR2(4000),
	OS_USER VARCHAR2(4000),
	SERVICE_NAME VARCHAR2(4000),
	CREATED_TS DATE DEFAULT SYSDATE)
;

CREATE OR REPLACE TRIGGER REPORT.TRAIUD_TERMINAL_EPORT_AUDIT
AFTER INSERT OR UPDATE OR DELETE ON REPORT.TERMINAL_EPORT
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
BEGIN
    INSERT INTO REPORT.AUDIT_TERMINAL_EPORT(
               TERMINAL_EPORT_ID,
               MODULE,
               HOST,
               IP_ADDRESS,
               NETWORK_PROTOCOL,
               OS_USER,
               SERVICE_NAME)
        SELECT NVL(:NEW.TERMINAL_EPORT_ID, :OLD.TERMINAL_EPORT_ID),
               SYS_CONTEXT ('USERENV', 'MODULE'),
               SYS_CONTEXT ('USERENV', 'HOST'),
               SYS_CONTEXT ('USERENV', 'IP_ADDRESS'),
               SYS_CONTEXT ('USERENV', 'NETWORK_PROTOCOL'),
               SYS_CONTEXT ('USERENV', 'OS_USER'),
               SYS_CONTEXT ('USERENV', 'SERVICE_NAME')
            FROM DUAL;
END;
/

DROP TABLE REPORT.CALL_IN_TRANS_EXCEPT;

DROP PROCEDURE REPORT.CCS_TRANSPORT_ERROR_INS;

DROP PROCEDURE REPORT.CCS_TRANSPORT_ERROR_UPD;

DROP TABLE REPORT.CONTROLLER_LEASE;

DROP PUBLIC SYNONYM CONVERT_TO_DATE;
CREATE SYNONYM DBADMIN.CONVERT_TO_DATE
	FOR REPORT.CONVERT_TO_DATE
;

DROP INDEX REPORT.UIX_EPORT_SERIAL_WITH_ID;

DROP TABLE REPORT.EVENT_DETAIL;


CREATE OR REPLACE FUNCTION          REPORT.FROM_CODE (
    l_code VARCHAR2)
RETURN NUMBER AS
   c_mult CONSTANT NUMBER := POWER(2,31) + 5231939;
   c_range CONSTANT PLS_INTEGER := POWER(2,31) - 1;
   c_radix CONSTANT PLS_INTEGER := 36;
   l_ret NUMBER := 0;
   l_ch CHAR(1);
   l_work NUMBER;
   l_rem NUMBER;
   l_digit PLS_INTEGER;
BEGIN
    FOR i IN 1..LENGTH(l_code) LOOP
        l_ch := SUBSTR(l_code, LENGTH(l_code) - i, 1);
        IF l_ch <= '9' THEN
           l_digit := ASCII(l_ch) - 48;
        ELSE
           l_digit := ASCII(l_ch) - 55;
        END IF;
        l_ret := l_ret * 36 + l_digit;
    END LOOP;
    l_work := l_ret / (c_mult - c_range);
    l_rem := 1 - REMAINDER(l_work, 1);
    FOR n IN 0..1000 LOOP
        IF REMAINDER(n, c_mult/c_range - 1) = l_rem THEN
           RETURN l_work + n / (c_mult/c_range - 1);
        END IF;
    END LOOP;
    RETURN NULL;
END;
/

DROP TABLE REPORT.MSG_LOG_BAK;

DROP TABLE REPORT.ORPHAN_HIST;

DROP TABLE REPORT.SCR_ORPHAN_HIST;

UPDATE REPORT.REPORT_REQUEST
  SET MAX_PAGE_ID = 0
 WHERE MAX_PAGE_ID IS NULL;
 
ALTER TABLE REPORT.REPORT_REQUEST
	MODIFY(MAX_PAGE_ID DEFAULT 0 NOT NULL)
;
CREATE INDEX REPORT.IDX_RR_CREATED_TS ON REPORT.REPORT_REQUEST(
	CREATED_TS)
LOCAL
;
CREATE INDEX REPORT.IDX$$_38560001 ON REPORT.REPORTS(
	BATCH_TYPE_ID)
;

DROP PROCEDURE REPORT.RESEND_UNSENT_REPORTS;

DROP PACKAGE REPORT.TEST_PKG;

CREATE OR REPLACE TRIGGER REPORT.TRBU_ALERT_NOTIF
BEFORE UPDATE ON REPORT.ALERT_NOTIF
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
BEGIN
    SELECT :OLD.CREATED_TS,
           :OLD.CREATED_BY,
           SYSDATE,
           USER
      INTO :NEW.CREATED_TS,
           :NEW.CREATED_BY,
           :NEW.LAST_UPDATED_TS,
           :NEW.LAST_UPDATED_BY
      FROM DUAL;
END;
/

CREATE INDEX REPORT.IX_FILL_PREV_SOURCE ON REPORT.FILL(
	PREV_SOURCE_FILL_ID,
	SOURCE_SYSTEM_CD) TABLESPACE REPORT_INDX ONLINE
;

ALTER TABLE REPORT.TRANS
	MODIFY(CREATE_DATE NULL)
	MODIFY(CREATED_BY NULL)
	MODIFY(LAST_UPDATED_BY NULL)
;
CREATE OR REPLACE SYNONYM CORP.SETTLE_STATE
	FOR REPORT.TRANS_STATE
;

DROP INDEX REPORT.IX_TRANS_EPORT_CLOSE_DATE;
CREATE INDEX REPORT.IX_TRANS_EPORT_CLOSE_DATE ON REPORT.TRANS(
	EPORT_ID,
	CLOSE_DATE)
LOCAL TABLESPACE REPORT_INDX
;

DROP INDEX REPORT.IX_TRANS_TERMINAL_CLOSE_DATE;
CREATE INDEX REPORT.IX_TRANS_TERMINAL_CLOSE_DATE ON REPORT.TRANS(
	TERMINAL_ID,
	CLOSE_DATE)
LOCAL TABLESPACE REPORT_INDX
;

CREATE OR REPLACE TRIGGER REPORT.TRBI_ALERT_NOTIF
BEFORE INSERT ON REPORT.ALERT_NOTIF
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
BEGIN
    IF :NEW.ALERT_NOTIF_ID IS NULL THEN
        SELECT SEQ_ALERT_NOTIF_ID.NEXTVAL
          INTO :NEW.ALERT_NOTIF_ID
          FROM DUAL;
    END IF;
    SELECT SYSDATE,
           USER,
           SYSDATE,
           USER
      INTO :NEW.CREATED_TS,
           :NEW.CREATED_BY,
           :NEW.LAST_UPDATED_TS,
           :NEW.LAST_UPDATED_BY
      FROM DUAL;
END;
/

CREATE OR REPLACE PROCEDURE          REPORT.USAT_CANCEL_OLD_REPORTS as
    CURSOR l_cur IS
    SELECT RR.REPORT_REQUEST_ID, RRU.USER_ID
      FROM REPORT.REPORT_REQUEST RR
      JOIN REPORT.REPORT_REQUEST_USER RRU ON RR.REPORT_REQUEST_ID = RRU.REPORT_REQUEST_ID
     WHERE RR.CREATED_TS < SYS_EXTRACT_UTC(SYSTIMESTAMP) - 6/24
       AND RR.REPORT_REQUEST_STATUS_ID = 0 and RR.REPORT_REQUEST_TYPE_ID <> 2
     ORDER BY RR.CREATED_TS ASC;
    ln_needed NUMBER;
BEGIN
    FOR l_rec IN l_cur LOOP
        REPORT.PKG_REPORT.CANCEL_REPORT_REQUEST(l_rec.USER_ID,l_rec.REPORT_REQUEST_ID,ln_needed);
        DBMS_OUTPUT.PUT_LINE('Canceled report request #' || l_rec.REPORT_REQUEST_ID || ' for user ' || l_rec.USER_ID);
        COMMIT;
    END LOOP;
END;
/

ALTER TABLE REPORT.USER_LOGIN
	DROP(PROFILE_ID)
;

ALTER INDEX REPORT.UIX_USER_LOGIN_COMBO1 RENAME TO IX_UL_CUST_USER;

DROP INDEX REPORT.UIX_USER_REPORT_COMBO1;


CREATE UNIQUE INDEX REPORT.UIX_UT_COMBO ON REPORT.USER_TERMINAL(
	TERMINAL_ID,
	USER_ID,
	ALLOW_EDIT)
TABLESPACE REPORT_INDX;

DROP VIEW REPORT.VW_CREDIT_TRANS_BYCUST;
DROP VIEW REPORT.VW_CREDIT_TRANS_ORPHANS;
DROP VIEW REPORT.VW_CREDIT_TRANS_PROCESSOR_CARD;
DROP VIEW REPORT.VW_EXPORT_BATCHES_IN_DOC;

ALTER TABLE REPORT.WEB_REQUEST RENAME COLUMN PROCESS_TIME_MS TO OLD_PROCESS_TIME_MS;
ALTER TABLE REPORT.WEB_REQUEST ADD (PROCESS_TIME_MS NUMBER(20));
UPDATE REPORT.WEB_REQUEST SET PROCESS_TIME_MS = OLD_PROCESS_TIME_MS WHERE PROCESS_TIME_MS IS NULL;
COMMIT;
ALTER TABLE REPORT.WEB_REQUEST MODIFY(PROCESS_TIME_MS NOT NULL);
ALTER TABLE REPORT.WEB_REQUEST DROP COLUMN OLD_PROCESS_TIME_MS;

ALTER TABLE REPORT.AUDIT_TERMINAL_EPORT
	MODIFY(TERMINAL_EPORT_ID NUMBER)
;


ALTER INDEX REPORT.USER_REPORT_PK01013462093041 RENAME TO USER_REPORT_PK01;
ALTER INDEX REPORT.USER_TYPE_PK01013462118658 RENAME TO USER_TYPE_PK01;

CREATE OR REPLACE FORCE VIEW APP_LAYER.VW_PAYMENT_SUBTYPE_DETAIL(
	PAYMENT_SUBTYPE_ID,
	PAYMENT_SUBTYPE_CLASS,
	PAYMENT_SUBTYPE_KEY_NAME,
	PAYMENT_SUBTYPE_KEY_ID,
	PAYMENT_SUBTYPE_PARENT_KEY_ID,
	AUTHORITY_ID,
	AUTHORITY_NAME,
	AUTHORITY_GATEWAY_QUEUE_KEY,
	AUTHORITY_SERVICE_ID,
	AUTHORITY_SERVICE_TYPE_ID,
	AUTHORITY_PAYMENT_MASK_ID,
	CLIENT_PAYMENT_TYPE_CD,
	CARD_REGEX,
	CARD_REGEX_BREF,
	REMOTE_SERVER_ADDR,
	REMOTE_SERVER_ADDR_ALT,
	REMOTE_SERVER_PORT,
	TERMINAL_ENCRYPT_KEY,
	TERMINAL_ENCRYPT_KEY2,
	TERMINAL_CD,
	MERCHANT_CD,
	AUTO_AUTH_RESULT_CD,
	AUTO_AUTH_RESP_DESC,
	AUTO_AUTH_ACTION_CD,
	AUTO_AUTH_ACTION_BITMAP,
	AUTHORITY_ASSN_ID,
	CARD_TYPE,
	PSD_TIMESTAMP,
	PSD_HASH)
 AS 
SELECT pst.PAYMENT_SUBTYPE_ID,
		pst.PAYMENT_SUBTYPE_CLASS,
		pst.PAYMENT_SUBTYPE_KEY_NAME,
        NULL PAYMENT_SUBTYPE_KEY_ID,
		NULL PAYMENT_SUBTYPE_PARENT_KEY_ID,
        pst.PAYMENT_SUBTYPE_ID AUTHORITY_ID,
		pst.PAYMENT_SUBTYPE_NAME AUTHORITY_NAME,
        ' ' AUTHORITY_GATEWAY_QUEUE_KEY,
        0 AUTHORITY_SERVICE_ID,
		2 AUTHORITY_SERVICE_TYPE_ID,
		pst.AUTHORITY_PAYMENT_MASK_ID,
        pst.CLIENT_PAYMENT_TYPE_CD,
        apm.AUTHORITY_PAYMENT_MASK_REGEX CARD_REGEX,
        apm.AUTHORITY_PAYMENT_MASK_BREF CARD_REGEX_BREF,
        NULL REMOTE_SERVER_ADDR,
        NULL REMOTE_SERVER_ADDR_ALT,
        NULL REMOTE_SERVER_PORT,
        NULL TERMINAL_ENCRYPT_KEY,
        NULL TERMINAL_ENCRYPT_KEY2,
        NULL TERMINAL_CD,
        NULL MERCHANT_CD,
        'R' AUTO_AUTH_RESULT_CD,
        'Invalid Payment Type' AUTO_AUTH_RESP_DESC,
        CAST(NULL AS NUMBER) AUTO_AUTH_ACTION_CD,
        CAST(NULL AS NUMBER) AUTO_AUTH_ACTION_BITMAP,
        APM.AUTHORITY_ASSN_ID,
        PST.CARD_TYPE_LABEL CARD_TYPE,
        DATE_TO_MILLIS(GREATEST(pst.LAST_UPDATED_TS, apm.LAST_UPDATED_TS)),
        DBMS_CRYPTO.HASH(UTL_RAW.CAST_TO_RAW(pst.PAYMENT_SUBTYPE_NAME) 
            || UTL_RAW.CAST_TO_RAW(pst.CLIENT_PAYMENT_TYPE_CD) 
            || UTL_RAW.CAST_TO_RAW(apm.AUTHORITY_PAYMENT_MASK_REGEX) 
            || UTL_RAW.CAST_TO_RAW(apm.AUTHORITY_PAYMENT_MASK_BREF) 
            || UTL_RAW.CAST_FROM_NUMBER(APM.AUTHORITY_ASSN_ID)
            , 2)
   FROM PSS.PAYMENT_SUBTYPE pst
   JOIN PSS.AUTHORITY_PAYMENT_MASK apm ON pst.AUTHORITY_PAYMENT_MASK_ID = apm.AUTHORITY_PAYMENT_MASK_ID
  WHERE pst.PAYMENT_SUBTYPE_CLASS IN('Authority::NOP', 'Cash') 
    AND (pst.PAYMENT_SUBTYPE_CLASS = 'Authority::NOP' OR pst.CLIENT_PAYMENT_TYPE_CD = 'M')
  UNION ALL
  SELECT pst.PAYMENT_SUBTYPE_ID,
		pst.PAYMENT_SUBTYPE_CLASS,
		pst.PAYMENT_SUBTYPE_KEY_NAME,
        NULL PAYMENT_SUBTYPE_KEY_ID,
		NULL PAYMENT_SUBTYPE_PARENT_KEY_ID,
        pst.PAYMENT_SUBTYPE_ID AUTHORITY_ID,
		pst.PAYMENT_SUBTYPE_NAME AUTHORITY_NAME,
        ' ' AUTHORITY_GATEWAY_QUEUE_KEY,
        0 AUTHORITY_SERVICE_ID,
		2 AUTHORITY_SERVICE_TYPE_ID,
		pst.AUTHORITY_PAYMENT_MASK_ID,
        pst.CLIENT_PAYMENT_TYPE_CD,
        apm.AUTHORITY_PAYMENT_MASK_REGEX CARD_REGEX,
        apm.AUTHORITY_PAYMENT_MASK_BREF CARD_REGEX_BREF,
        NULL REMOTE_SERVER_ADDR,
        NULL REMOTE_SERVER_ADDR_ALT,
        NULL REMOTE_SERVER_PORT,
        NULL TERMINAL_ENCRYPT_KEY,
        NULL TERMINAL_ENCRYPT_KEY2,
        NULL TERMINAL_CD,
        NULL MERCHANT_CD,
        'Y' AUTO_AUTH_RESULT_CD,
        'Accepted' AUTO_AUTH_RESP_DESC,
        CAST(NULL AS NUMBER) AUTO_AUTH_ACTION_CD,
        CAST(NULL AS NUMBER) AUTO_AUTH_ACTION_BITMAP,
        APM.AUTHORITY_ASSN_ID,
        PST.CARD_TYPE_LABEL CARD_TYPE,
        DATE_TO_MILLIS(GREATEST(pst.LAST_UPDATED_TS, apm.LAST_UPDATED_TS)),
        DBMS_CRYPTO.HASH(UTL_RAW.CAST_TO_RAW(pst.PAYMENT_SUBTYPE_NAME) 
            || UTL_RAW.CAST_TO_RAW(pst.CLIENT_PAYMENT_TYPE_CD) 
            || UTL_RAW.CAST_TO_RAW(apm.AUTHORITY_PAYMENT_MASK_REGEX) 
            || UTL_RAW.CAST_TO_RAW(apm.AUTHORITY_PAYMENT_MASK_BREF) 
            || UTL_RAW.CAST_FROM_NUMBER(APM.AUTHORITY_ASSN_ID)
            , 2)
   FROM PSS.PAYMENT_SUBTYPE pst
   JOIN PSS.AUTHORITY_PAYMENT_MASK apm ON pst.AUTHORITY_PAYMENT_MASK_ID = apm.AUTHORITY_PAYMENT_MASK_ID
  WHERE pst.PAYMENT_SUBTYPE_CLASS = 'Cash'
    AND pst.CLIENT_PAYMENT_TYPE_CD != 'M'
  UNION ALL
  SELECT pst.PAYMENT_SUBTYPE_ID,
		pst.PAYMENT_SUBTYPE_CLASS,
		pst.PAYMENT_SUBTYPE_KEY_NAME,
        t.TERMINAL_ID PAYMENT_SUBTYPE_KEY_ID,
		M.MERCHANT_ID PAYMENT_SUBTYPE_PARENT_KEY_ID,
        pst.PAYMENT_SUBTYPE_ID AUTHORITY_ID,
		A.AUTHORITY_NAME,
        LOWER(REPLACE(pst.PAYMENT_SUBTYPE_CLASS, '::', '_')) AUTHORITY_GATEWAY_QUEUE_KEY,
        A.AUTHORITY_SERVICE_ID,
		AUS.AUTHORITY_SERVICE_TYPE_ID,
		pst.AUTHORITY_PAYMENT_MASK_ID,
        pst.CLIENT_PAYMENT_TYPE_CD,
        apm.AUTHORITY_PAYMENT_MASK_REGEX CARD_REGEX,
        apm.AUTHORITY_PAYMENT_MASK_BREF CARD_REGEX_BREF,
        A.REMOTE_SERVER_ADDR,
        A.REMOTE_SERVER_ADDR_ALT,
        A.REMOTE_SERVER_PORT_NUM REMOTE_SERVER_PORT,
        T.TERMINAL_ENCRYPT_KEY,
        T.TERMINAL_ENCRYPT_KEY2,
        T.TERMINAL_CD,
        M.MERCHANT_CD,
        'Y' AUTO_AUTH_RESULT_CD,
        'Approved by USAT Demo' AUTO_AUTH_RESP_DESC,
        CAST(NULL AS NUMBER) AUTO_AUTH_ACTION_CD,
        CAST(NULL AS NUMBER) AUTO_AUTH_ACTION_BITMAP,
        APM.AUTHORITY_ASSN_ID,
        PST.CARD_TYPE_LABEL CARD_TYPE,
        DATE_TO_MILLIS(GREATEST(pst.LAST_UPDATED_TS, apm.LAST_UPDATED_TS, a.LAST_UPDATED_TS, m.LAST_UPDATED_TS, t.LAST_UPDATED_TS)),
        DBMS_CRYPTO.HASH(UTL_RAW.CAST_TO_RAW(A.AUTHORITY_NAME) 
            || UTL_RAW.CAST_TO_RAW(pst.PAYMENT_SUBTYPE_CLASS)
            || UTL_RAW.CAST_FROM_NUMBER(AUS.AUTHORITY_SERVICE_TYPE_ID)
            || UTL_RAW.CAST_TO_RAW(pst.CLIENT_PAYMENT_TYPE_CD) 
            || UTL_RAW.CAST_TO_RAW(apm.AUTHORITY_PAYMENT_MASK_REGEX) 
            || UTL_RAW.CAST_TO_RAW(apm.AUTHORITY_PAYMENT_MASK_BREF) 
            || UTL_RAW.CAST_TO_RAW(A.REMOTE_SERVER_ADDR) 
            || UTL_RAW.CAST_TO_RAW(A.REMOTE_SERVER_ADDR_ALT) 
            || UTL_RAW.CAST_FROM_NUMBER(A.REMOTE_SERVER_PORT_NUM)
            || UTL_RAW.CAST_TO_RAW(T.TERMINAL_ENCRYPT_KEY) 
            || UTL_RAW.CAST_TO_RAW(T.TERMINAL_ENCRYPT_KEY2) 
            || UTL_RAW.CAST_TO_RAW(T.TERMINAL_CD) 
            || UTL_RAW.CAST_TO_RAW(M.MERCHANT_CD) 
            || UTL_RAW.CAST_FROM_NUMBER(APM.AUTHORITY_ASSN_ID)
            , 2)
   FROM PSS.PAYMENT_SUBTYPE pst
   JOIN PSS.AUTHORITY_PAYMENT_MASK apm ON pst.AUTHORITY_PAYMENT_MASK_ID = apm.AUTHORITY_PAYMENT_MASK_ID
   JOIN AUTHORITY.HANDLER H ON PST.PAYMENT_SUBTYPE_CLASS = H.HANDLER_CLASS
   JOIN AUTHORITY.AUTHORITY_TYPE AUT ON H.HANDLER_ID = AUT.HANDLER_ID
   JOIN AUTHORITY.AUTHORITY A ON A.AUTHORITY_TYPE_ID = AUT.AUTHORITY_TYPE_ID
   JOIN AUTHORITY.AUTHORITY_SERVICE AUS ON A.AUTHORITY_SERVICE_ID = AUS.AUTHORITY_SERVICE_ID
   JOIN PSS.MERCHANT M ON M.AUTHORITY_ID = A.AUTHORITY_ID
   JOIN PSS.TERMINAL T ON T.MERCHANT_ID = M.MERCHANT_ID
  WHERE pst.PAYMENT_SUBTYPE_CLASS IN('Demo')
  UNION ALL
 SELECT pst.PAYMENT_SUBTYPE_ID,
		pst.PAYMENT_SUBTYPE_CLASS,
		pst.PAYMENT_SUBTYPE_KEY_NAME,
        t.TERMINAL_ID PAYMENT_SUBTYPE_KEY_ID,
		M.MERCHANT_ID PAYMENT_SUBTYPE_PARENT_KEY_ID,
        A.AUTHORITY_ID,
		A.AUTHORITY_NAME,
        LOWER(REPLACE(pst.PAYMENT_SUBTYPE_CLASS, '::', '_')) AUTHORITY_GATEWAY_QUEUE_KEY,
        A.AUTHORITY_SERVICE_ID,
		AUS.AUTHORITY_SERVICE_TYPE_ID,
		pst.AUTHORITY_PAYMENT_MASK_ID,
        pst.CLIENT_PAYMENT_TYPE_CD,
        apm.AUTHORITY_PAYMENT_MASK_REGEX CARD_REGEX,
        apm.AUTHORITY_PAYMENT_MASK_BREF CARD_REGEX_BREF,
        A.REMOTE_SERVER_ADDR REMOTE_SERVER_ADDR,
        A.REMOTE_SERVER_ADDR_ALT REMOTE_SERVER_ADDR_ALT,
        A.REMOTE_SERVER_PORT_NUM REMOTE_SERVER_PORT,
        T.TERMINAL_ENCRYPT_KEY TERMINAL_ENCRYPT_KEY,
        T.TERMINAL_ENCRYPT_KEY2 TERMINAL_ENCRYPT_KEY2,
        T.TERMINAL_CD TERMINAL_CD,
        M.MERCHANT_CD MERCHANT_CD,
        NULL AUTO_AUTH_RESULT_CD,
        NULL AUTO_AUTH_RESP_DESC,
        NULL AUTO_AUTH_ACTION_CD,
        NULL AUTO_AUTH_ACTION_BITMAP,
        APM.AUTHORITY_ASSN_ID,
        PST.CARD_TYPE_LABEL CARD_TYPE,
        DATE_TO_MILLIS(GREATEST(pst.LAST_UPDATED_TS, apm.LAST_UPDATED_TS, a.LAST_UPDATED_TS, m.LAST_UPDATED_TS, t.LAST_UPDATED_TS)),
        DBMS_CRYPTO.HASH(UTL_RAW.CAST_TO_RAW(A.AUTHORITY_NAME) 
            || UTL_RAW.CAST_TO_RAW(pst.PAYMENT_SUBTYPE_CLASS)
            || UTL_RAW.CAST_FROM_NUMBER(AUS.AUTHORITY_SERVICE_TYPE_ID)
            || UTL_RAW.CAST_TO_RAW(pst.CLIENT_PAYMENT_TYPE_CD) 
            || UTL_RAW.CAST_TO_RAW(apm.AUTHORITY_PAYMENT_MASK_REGEX) 
            || UTL_RAW.CAST_TO_RAW(apm.AUTHORITY_PAYMENT_MASK_BREF) 
            || UTL_RAW.CAST_TO_RAW(A.REMOTE_SERVER_ADDR) 
            || UTL_RAW.CAST_TO_RAW(A.REMOTE_SERVER_ADDR_ALT) 
            || UTL_RAW.CAST_FROM_NUMBER(A.REMOTE_SERVER_PORT_NUM)
            || UTL_RAW.CAST_TO_RAW(T.TERMINAL_ENCRYPT_KEY) 
            || UTL_RAW.CAST_TO_RAW(T.TERMINAL_ENCRYPT_KEY2) 
            || UTL_RAW.CAST_TO_RAW(T.TERMINAL_CD) 
            || UTL_RAW.CAST_TO_RAW(M.MERCHANT_CD) 
            || UTL_RAW.CAST_FROM_NUMBER(APM.AUTHORITY_ASSN_ID)
            , 2)
   FROM PSS.PAYMENT_SUBTYPE pst
   JOIN PSS.AUTHORITY_PAYMENT_MASK APM ON PST.AUTHORITY_PAYMENT_MASK_ID = apm.AUTHORITY_PAYMENT_MASK_ID
   JOIN AUTHORITY.HANDLER H ON PST.PAYMENT_SUBTYPE_CLASS = H.HANDLER_CLASS
   JOIN AUTHORITY.AUTHORITY_TYPE AUT ON H.HANDLER_ID = AUT.HANDLER_ID
   JOIN AUTHORITY.AUTHORITY A ON A.AUTHORITY_TYPE_ID = AUT.AUTHORITY_TYPE_ID
   JOIN AUTHORITY.AUTHORITY_SERVICE AUS ON A.AUTHORITY_SERVICE_ID = AUS.AUTHORITY_SERVICE_ID
   JOIN PSS.MERCHANT M ON M.AUTHORITY_ID = A.AUTHORITY_ID
   JOIN PSS.TERMINAL T ON T.MERCHANT_ID = M.MERCHANT_ID
  WHERE pst.PAYMENT_SUBTYPE_CLASS NOT IN('Authority::NOP', 'Cash', 'Demo') 
  UNION ALL
 SELECT pst.PAYMENT_SUBTYPE_ID,
		pst.PAYMENT_SUBTYPE_CLASS,
		pst.PAYMENT_SUBTYPE_KEY_NAME,
        IPT.INTERNAL_PAYMENT_TYPE_ID PAYMENT_SUBTYPE_KEY_ID,
		IA.INTERNAL_AUTHORITY_ID PAYMENT_SUBTYPE_PARENT_KEY_ID,
        IA.INTERNAL_AUTHORITY_ID,
		IA.INTERNAL_AUTHORITY_NAME,
        LOWER(REPLACE(pst.PAYMENT_SUBTYPE_CLASS, '::', '_')) AUTHORITY_GATEWAY_QUEUE_KEY,
        0 AUTHORITY_SERVICE_ID,
		IA.AUTHORITY_SERVICE_TYPE_ID,
		pst.AUTHORITY_PAYMENT_MASK_ID,
        pst.CLIENT_PAYMENT_TYPE_CD,
        apm.AUTHORITY_PAYMENT_MASK_REGEX CARD_REGEX,
        apm.AUTHORITY_PAYMENT_MASK_BREF CARD_REGEX_BREF,
        NULL REMOTE_SERVER_ADDR,
        NULL REMOTE_SERVER_ADDR_ALT,
        NULL REMOTE_SERVER_PORT,
        NULL TERMINAL_ENCRYPT_KEY,
        NULL TERMINAL_ENCRYPT_KEY2,
        NULL TERMINAL_CD,
        NULL MERCHANT_CD,
        NULL AUTO_AUTH_RESULT_CD,
        NULL AUTO_AUTH_RESP_DESC,
        NULL AUTO_AUTH_ACTION_CD,
        NULL AUTO_AUTH_ACTION_BITMAP,
        APM.AUTHORITY_ASSN_ID,
        PST.CARD_TYPE_LABEL CARD_TYPE,
        DATE_TO_MILLIS(GREATEST(pst.LAST_UPDATED_TS, apm.LAST_UPDATED_TS, ipt.LAST_UPDATED_TS, ia.LAST_UPDATED_TS)),
        DBMS_CRYPTO.HASH(UTL_RAW.CAST_TO_RAW(IA.INTERNAL_AUTHORITY_NAME) 
            || UTL_RAW.CAST_TO_RAW(pst.PAYMENT_SUBTYPE_CLASS)
            || UTL_RAW.CAST_FROM_NUMBER(IA.AUTHORITY_SERVICE_TYPE_ID)
            || UTL_RAW.CAST_TO_RAW(pst.CLIENT_PAYMENT_TYPE_CD) 
            || UTL_RAW.CAST_TO_RAW(apm.AUTHORITY_PAYMENT_MASK_REGEX) 
            || UTL_RAW.CAST_TO_RAW(apm.AUTHORITY_PAYMENT_MASK_BREF) 
            || UTL_RAW.CAST_FROM_NUMBER(APM.AUTHORITY_ASSN_ID)
            , 2)
   FROM PSS.PAYMENT_SUBTYPE pst
   JOIN PSS.AUTHORITY_PAYMENT_MASK apm ON pst.AUTHORITY_PAYMENT_MASK_ID = apm.AUTHORITY_PAYMENT_MASK_ID
   JOIN AUTHORITY.HANDLER H ON PST.PAYMENT_SUBTYPE_CLASS = H.HANDLER_CLASS
   JOIN AUTHORITY.AUTHORITY_TYPE AUT ON H.HANDLER_ID = AUT.HANDLER_ID
   JOIN AUTHORITY.AUTHORITY A ON A.AUTHORITY_TYPE_ID = AUT.AUTHORITY_TYPE_ID
   JOIN PSS.INTERNAL_AUTHORITY IA ON IA.AUTHORITY_ID = A.AUTHORITY_ID
   JOIN PSS.INTERNAL_PAYMENT_TYPE IPT ON IPT.INTERNAL_AUTHORITY_ID = IA.INTERNAL_AUTHORITY_ID
  WHERE pst.PAYMENT_SUBTYPE_CLASS LIKE 'Internal%'
  UNION ALL
 SELECT pst.PAYMENT_SUBTYPE_ID,
		pst.PAYMENT_SUBTYPE_CLASS,
		pst.PAYMENT_SUBTYPE_KEY_NAME,
        APT.ARAMARK_PAYMENT_TYPE_ID PAYMENT_SUBTYPE_KEY_ID,
		AA.ARAMARK_AUTHORITY_ID PAYMENT_SUBTYPE_PARENT_KEY_ID,
        AA.ARAMARK_AUTHORITY_ID,
		AA.ARAMARK_AUTHORITY_NAME,
        LOWER(REPLACE(pst.PAYMENT_SUBTYPE_CLASS, '::', '_')) AUTHORITY_GATEWAY_QUEUE_KEY,
        0 AUTHORITY_SERVICE_ID,
		AA.AUTHORITY_SERVICE_TYPE_ID,
		pst.AUTHORITY_PAYMENT_MASK_ID,
        pst.CLIENT_PAYMENT_TYPE_CD,
        apm.AUTHORITY_PAYMENT_MASK_REGEX CARD_REGEX,
        apm.AUTHORITY_PAYMENT_MASK_BREF CARD_REGEX_BREF,
        AA.ARAMARK_REMOTE_SERVER_ADDR REMOTE_SERVER_ADDR,
        NULL REMOTE_SERVER_ADDR_ALT,
        NULL REMOTE_SERVER_PORT,
        NULL TERMINAL_ENCRYPT_KEY,
        NULL TERMINAL_ENCRYPT_KEY2,
        NULL TERMINAL_CD,
        NULL MERCHANT_CD,
        NULL AUTO_AUTH_RESULT_CD,
        NULL AUTO_AUTH_RESP_DESC,
        NULL AUTO_AUTH_ACTION_CD,
        NULL AUTO_AUTH_ACTION_BITMAP,
        APM.AUTHORITY_ASSN_ID,
        PST.CARD_TYPE_LABEL CARD_TYPE,
        DATE_TO_MILLIS(GREATEST(pst.LAST_UPDATED_TS, apm.LAST_UPDATED_TS, apt.LAST_UPDATED_TS, aa.LAST_UPDATED_TS)),
        DBMS_CRYPTO.HASH(UTL_RAW.CAST_TO_RAW(AA.ARAMARK_AUTHORITY_NAME) 
            || UTL_RAW.CAST_TO_RAW(pst.PAYMENT_SUBTYPE_CLASS)
            || UTL_RAW.CAST_FROM_NUMBER(AA.AUTHORITY_SERVICE_TYPE_ID)
            || UTL_RAW.CAST_TO_RAW(pst.CLIENT_PAYMENT_TYPE_CD) 
            || UTL_RAW.CAST_TO_RAW(apm.AUTHORITY_PAYMENT_MASK_REGEX) 
            || UTL_RAW.CAST_TO_RAW(apm.AUTHORITY_PAYMENT_MASK_BREF) 
            || UTL_RAW.CAST_TO_RAW(AA.ARAMARK_REMOTE_SERVER_ADDR) 
            || UTL_RAW.CAST_FROM_NUMBER(APM.AUTHORITY_ASSN_ID)
            , 2)
   FROM PSS.PAYMENT_SUBTYPE pst
   JOIN PSS.AUTHORITY_PAYMENT_MASK apm  ON pst.AUTHORITY_PAYMENT_MASK_ID = apm.AUTHORITY_PAYMENT_MASK_ID
  CROSS JOIN PSS.ARAMARK_PAYMENT_TYPE APT
   JOIN PSS.ARAMARK_AUTHORITY AA ON APT.ARAMARK_AUTHORITY_ID = AA.ARAMARK_AUTHORITY_ID
  WHERE pst.PAYMENT_SUBTYPE_CLASS = 'Aramark' --pst.PAYMENT_SUBTYPE_KEY_NAME = 'ARAMARK_PAYMENT_TYPE_ID'
  UNION ALL
 SELECT pst.PAYMENT_SUBTYPE_ID,
		pst.PAYMENT_SUBTYPE_CLASS,
		pst.PAYMENT_SUBTYPE_KEY_NAME,
        BT.BLACKBRD_TENDER_ID PAYMENT_SUBTYPE_KEY_ID,
		BA.BLACKBRD_AUTHORITY_ID PAYMENT_SUBTYPE_PARENT_KEY_ID,
        BA.BLACKBRD_AUTHORITY_ID,
		BA.BLACKBRD_AUTHORITY_NAME,
        LOWER(REPLACE(pst.PAYMENT_SUBTYPE_CLASS, '::', '_')) AUTHORITY_GATEWAY_QUEUE_KEY,
        0 AUTHORITY_SERVICE_ID,
		BA.AUTHORITY_SERVICE_TYPE_ID,
		pst.AUTHORITY_PAYMENT_MASK_ID,
        pst.CLIENT_PAYMENT_TYPE_CD,
        apm.AUTHORITY_PAYMENT_MASK_REGEX CARD_REGEX,
        apm.AUTHORITY_PAYMENT_MASK_BREF CARD_REGEX_BREF,
        BA.REMOTE_SERVER_ADDR REMOTE_SERVER_ADDR,
        BA.REMOTE_SERVER_ADDR_ALT REMOTE_SERVER_ADDR_ALT,
        BA.REMOTE_SERVER_PORT_NUM REMOTE_SERVER_PORT,
        NULL TERMINAL_ENCRYPT_KEY,
        NULL TERMINAL_ENCRYPT_KEY2,
        NULL TERMINAL_CD,
        TO_CHAR(BT.BLACKBRD_TENDER_NUM) MERCHANT_CD,
        NULL AUTO_AUTH_RESULT_CD,
        NULL AUTO_AUTH_RESP_DESC,
        NULL AUTO_AUTH_ACTION_CD,
        NULL AUTO_AUTH_ACTION_BITMAP,
        APM.AUTHORITY_ASSN_ID,
        PST.CARD_TYPE_LABEL CARD_TYPE,
        DATE_TO_MILLIS(GREATEST(pst.LAST_UPDATED_TS, apm.LAST_UPDATED_TS, bt.LAST_UPDATED_TS, ba.LAST_UPDATED_TS)),
        DBMS_CRYPTO.HASH(UTL_RAW.CAST_TO_RAW(BA.BLACKBRD_AUTHORITY_NAME) 
            || UTL_RAW.CAST_TO_RAW(pst.PAYMENT_SUBTYPE_CLASS)
            || UTL_RAW.CAST_FROM_NUMBER(BA.AUTHORITY_SERVICE_TYPE_ID)
            || UTL_RAW.CAST_TO_RAW(pst.CLIENT_PAYMENT_TYPE_CD) 
            || UTL_RAW.CAST_TO_RAW(apm.AUTHORITY_PAYMENT_MASK_REGEX) 
            || UTL_RAW.CAST_TO_RAW(apm.AUTHORITY_PAYMENT_MASK_BREF) 
            || UTL_RAW.CAST_TO_RAW(BA.REMOTE_SERVER_ADDR)
            || UTL_RAW.CAST_TO_RAW(BA.REMOTE_SERVER_ADDR_ALT)
            || UTL_RAW.CAST_TO_RAW(BA.REMOTE_SERVER_PORT_NUM)        
            || UTL_RAW.CAST_FROM_NUMBER(BT.BLACKBRD_TENDER_NUM)
            || UTL_RAW.CAST_FROM_NUMBER(APM.AUTHORITY_ASSN_ID)
            , 2)
   FROM PSS.PAYMENT_SUBTYPE pst
   JOIN PSS.AUTHORITY_PAYMENT_MASK apm ON pst.AUTHORITY_PAYMENT_MASK_ID = apm.AUTHORITY_PAYMENT_MASK_ID
  CROSS JOIN PSS.BLACKBRD_TENDER BT
   JOIN PSS.BLACKBRD_AUTHORITY BA ON BT.BLACKBRD_AUTHORITY_ID = BA.BLACKBRD_AUTHORITY_ID
  WHERE pst.PAYMENT_SUBTYPE_CLASS = 'BlackBoard' --pst.PAYMENT_SUBTYPE_KEY_NAME = 'BLACKBRD_TENDER_ID'
/

CREATE OR REPLACE PROCEDURE             CORP.LICENSE_UPDATE(
    l_license_id IN LICENSE.LICENSE_ID%TYPE,
    l_title IN LICENSE.TITLE%TYPE,
	l_desc IN LICENSE.DESCRIPTION%TYPE)
IS
BEGIN
	 UPDATE LICENSE SET (TITLE, DESCRIPTION) =
	     (SELECT l_title, l_desc FROM DUAL) WHERE LICENSE_ID = l_license_id;
END;
/
DROP SEQUENCE PSS.SEQ_ADMIN_CMD_HIST_ID;
DROP SEQUENCE REPORT.SEQ_TRANS_STAT_BY_MONTH_ID;
DROP INDEX PSS.IDX_TRAN_DEVICE_TRAN_START_TS;

ALTER INDEX  REPORT.ALERT_PK01013461750379 RENAME TO ALERT_PK01;
ALTER INDEX  REPORT.EXPORT_TYPE_PK01013461833588 RENAME TO EXPORT_TYPE_PK01;

CREATE OR REPLACE FUNCTION         DBADMIN.GET_AUTHED_CARD (
	l_parsed_acct_data IN VARCHAR2)
RETURN VARCHAR2 PARALLEL_ENABLE DETERMINISTIC IS
/*
SELECT '''' || REGEXP_REPLACE(AUTHORITY_PAYMENT_MASK_REGEX, '([^\\])\(\?\:', '\1(') || ''', -- ' || AUTHORITY_PAYMENT_MASK_DESC
  FROM PSS.AUTHORITY_PAYMENT_MASK
 WHERE AUTHORITY_ASSN_ID IS NOT NULL
*/
    lt_cc_regex VARCHAR2_TABLE :=  VARCHAR2_TABLE(
        '^;?(4[0-9]{15})=([0-9]{4})([0-9]{3})([0-9]*)(\?([\x00-\xFF])?)?$', -- Visa Credit Card on Track 2
        '^;?(5[1-5][0-9]{14})=([0-9]{4})([0-9]{3})([0-9]*)(\?([\x00-\xFF])?)?$', -- MasterCard Credit Card on Track 2
        '^;?(3[47][0-9]{13})=([0-9]{4})([0-9]{3})([0-9]*)(\?([\x00-\xFF])?)?$', -- American Express Credit Card on Track 2
        '^;?((6011|622[0-9]|64[4-9][0-9]|65[0-9]{2})[0-9]{12})=([0-9]{4})([0-9]{3})([0-9]*)(\?([\x00-\xFF])?)?$', -- Discover Card Credit Card on Track 2
        '^;?(36[0-9]{12})=([0-9]{4})([0-9]{3})([0-9]*)(\?([\x00-\xFF])?)?$', -- Diners Club International Credit Card on Track 2
        '^;?(30[0-5][0-9]{11})=([0-9]{4})([0-9]{3})([0-9]*)(\?([\x00-\xFF])?)?$', -- Diners Club Carte Blanche Credit Card on Track 2
        '^%?B(4[0-9]{15})\^([A-Za-z0-9 \/]{2,26})\^([0-9]{4}?)([0-9]{3}?)([A-Za-z0-9 \/]*)(\?([\x00-\xFF])?)$', -- Visa Credit Card on Track 1
        '^%?B(5[1-5][0-9]{14})\^([A-Za-z0-9 \/]{2,26})\^([0-9]{4}?)([0-9]{3}?)([A-Za-z0-9 \/]*)(\?([\x00-\xFF])?)$', -- MasterCard Credit Card on Track 1
        '^%?B(3[47][0-9]{13})\^([A-Za-z0-9 \/]{2,26})\^([0-9]{4}?)([0-9]{3}?)([A-Za-z0-9 \/]*)(\?([\x00-\xFF])?)$', -- American Express Credit Card on Track 1
        '^%?B((6011|622[0-9]|64[4-9][0-9]|65[0-9]{2})[0-9]{12})\^([A-Za-z0-9 \/]{2,26})\^([0-9]{4}?)([0-9]{3}?)([A-Za-z0-9 \/]*)(\?([\x00-\xFF])?)$', -- Discover Card Credit Card on Track 1
        '^%?B(36[0-9]{12})\^([A-Za-z0-9 \/]{2,26})\^([0-9]{4}?)([0-9]{3}?)([A-Za-z0-9 \/]*)(\?([\x00-\xFF])?)$', -- Diners Club International Credit Card on Track 1
        '^%?B(30[0-5][0-9]{11})\^([A-Za-z0-9 \/]{2,26})\^([0-9]{4}?)([0-9]{3}?)([A-Za-z0-9 \/]*)(\?([\x00-\xFF])?)$' -- Diners Club Carte Blanche Credit Card on Track 1
    );
    lv_auth_card_data VARCHAR2(19);
BEGIN
    SELECT REGEXP_REPLACE(l_parsed_acct_data, a.COLUMN_VALUE, '\1')
      INTO lv_auth_card_data
      FROM TABLE(lt_cc_regex) a
     WHERE REGEXP_LIKE(l_parsed_acct_data, a.COLUMN_VALUE)
       AND ROWNUM = 1;
    RETURN lv_auth_card_data;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN l_parsed_acct_data;
END;
/

DROP SYNONYM RDW_LOADER.INT_AUTH;
DROP SYNONYM WEB_USER.INT_AUTH;
DROP SYNONYM WEB_USER1.INT_AUTH;

DROP VIEW REPORT.VW_AMEX_JT;
DROP VIEW REPORT.VW_MASTERCARD_JT;

