CREATE TABLE PSS.TRAN_LINE_ITEM_RECENT
  (
    HOST_ID                 NUMBER(20,0)   not null,
    TRAN_LINE_ITEM_TYPE_ID  NUMBER(20,0)   not null,
    TRAN_LINE_ITEM_ID       NUMBER(20,0)   not null,
    FKP_TRAN_ID             NUMBER(20,0)   not null,
    CONSTRAINT PK_TRAN_LINE_ITEM_RECENT primary key(HOST_ID,TRAN_LINE_ITEM_TYPE_ID) USING INDEX TABLESPACE PSS_INDX,
    CONSTRAINT FK_TRAN_LINE_ITEM_R_HOST_ID foreign key(HOST_ID) references DEVICE.HOST(HOST_ID),
    CONSTRAINT FK_TRAN_LINE_ITEM_R_TLIT_ID foreign key(TRAN_LINE_ITEM_TYPE_ID) references PSS.TRAN_LINE_ITEM_TYPE(TRAN_LINE_ITEM_TYPE_ID),
    CONSTRAINT FK_TRAN_LINE_ITEM_R_TLI_ID foreign key(TRAN_LINE_ITEM_ID) references PSS.TRAN_LINE_ITEM(TRAN_LINE_ITEM_ID)
  )
  TABLESPACE PSS_DATA;

REM ----------------------------------------------------------------------

COMMENT ON TABLE PSS.TRAN_LINE_ITEM_RECENT is 'Contains information about the the most recent transaction line item per line item type for each host, as recorded by the PSS system.';
COMMENT ON COLUMN PSS.TRAN_LINE_ITEM_RECENT.FKP_TRAN_ID is 'FKP = For(eign) Key for Partitioning only; do not use for referential purposes';
