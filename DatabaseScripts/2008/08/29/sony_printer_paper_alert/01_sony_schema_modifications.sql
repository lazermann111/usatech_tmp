/* create new table */
CREATE TABLE SONY.SONY_PRINT_RMN_ALERT
  (
    SONY_PRINTER_ID               NUMBER,
    SONY_PRINT_RMN_ALERT_NEW_TS   VARCHAR2(60),
    SONY_PRINT_RMN_ALERT_LAST_TS  VARCHAR2(255),
    CREATED_BY                    VARCHAR2(30)    not null,
    CREATED_TS                    DATE            not null,
    LAST_UPDATED_BY               VARCHAR2(30)    not null,
    LAST_UPDATED_TS               DATE            not null,
    CONSTRAINT PK_SONY_PRINT_RMN_ALERT primary key(SONY_PRINTER_ID) USING INDEX TABLESPACE SONY_INDX,
    CONSTRAINT FK_SONY_PRINT_RMN_ALERT_SP_ID foreign key(SONY_PRINTER_ID) references SONY_PRINTER(SONY_PRINTER_ID)
  )
  TABLESPACE SONY_DATA;

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER SONY.TRBI_SONY_PRINT_RMN_ALERT BEFORE INSERT ON SONY.SONY_PRINT_RMN_ALERT
  FOR EACH ROW 
BEGIN
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER SONY.TRBU_SONY_PRINT_RMN_ALERT BEFORE UPDATE ON SONY.SONY_PRINT_RMN_ALERT
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

COMMENT ON TABLE SONY.SONY_PRINT_RMN_ALERT is 'Print remaining alert status of a Sony PictureStation printer.';

/* migrate data */
INSERT INTO sony.sony_print_rmn_alert
            (sony_printer_id, sony_print_rmn_alert_new_ts,
             sony_print_rmn_alert_last_ts)
   (SELECT sony_printer_id, prints_remaining_new_alert_ts,
           prints_remaining_last_alert_ts
      FROM sony.sony_printer
     WHERE prints_remaining_new_alert_ts IS NOT NULL
        OR prints_remaining_new_alert_ts IS NOT NULL);

/* remove deprecated columns */
ALTER TABLE sony.sony_printer DROP COLUMN prints_remaining_last_alert_ts;
ALTER TABLE sony.sony_printer DROP COLUMN prints_remaining_new_alert_ts;
