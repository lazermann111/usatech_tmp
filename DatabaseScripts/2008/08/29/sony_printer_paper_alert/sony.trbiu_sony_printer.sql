-- Start of DDL Script for Trigger SONY.TRBIU_SONY_PRINTER
-- Generated 29-Aug-2008 20:42:42 from SONY@USADBD02.USATECH.COM

CREATE OR REPLACE TRIGGER sony.trbiu_sony_printer
 BEFORE
  INSERT OR UPDATE
 ON sony.sony_printer
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
DECLARE
   n_sony_printer_id   sony.sony_printer.sony_printer_id%TYPE;

   CURSOR c_get_sony_printer_id (n_printer_id IN n_sony_printer_id%TYPE)
   IS
      SELECT sony_printer_id
        FROM sony.sony_print_rmn_alert
       WHERE sony_printer_id = n_printer_id;
BEGIN
   IF (    :NEW.total_prints_remaining IS NOT NULL
       AND (       :NEW.total_prints_remaining < :NEW.prints_remaining_threshold
               AND (   :OLD.total_prints_remaining >= :NEW.prints_remaining_threshold
                    OR :OLD.total_prints_remaining IS NULL
                   )
            OR :NEW.total_prints_remaining <= 0
           )
      )
   THEN
      /* toggle alert for this printer */
      OPEN c_get_sony_printer_id (:NEW.sony_printer_id);

      FETCH c_get_sony_printer_id
       INTO n_sony_printer_id;

      IF c_get_sony_printer_id%NOTFOUND
      THEN
         INSERT INTO sony.sony_print_rmn_alert
                     (sony_printer_id, sony_print_rmn_alert_new_ts)
              VALUES (:NEW.sony_printer_id, SYSDATE);
      ELSE
         UPDATE sony.sony_print_rmn_alert
            SET sony_print_rmn_alert_new_ts = SYSDATE
          WHERE sony_printer_id = n_sony_printer_id;
      END IF;

      CLOSE c_get_sony_printer_id;
   END IF;
END;
/


-- End of DDL Script for Trigger SONY.TRBIU_SONY_PRINTER
