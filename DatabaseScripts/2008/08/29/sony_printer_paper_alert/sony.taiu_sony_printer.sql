-- Start of DDL Script for Trigger SONY.TAIU_SONY_PRINTER
-- Generated 29-Aug-2008 20:53:28 from SONY@USADBD02.USATECH.COM

CREATE OR REPLACE TRIGGER sony.taiu_sony_printer
 AFTER
  INSERT OR UPDATE
 ON sony.sony_printer
REFERENCING NEW AS NEW OLD AS OLD
DECLARE
   n_sony_printer_id             sony.sony_printer.sony_printer_id%TYPE;
   v_machine_id                  sony.sony_printer.machine_id%TYPE;
   n_total_prints_remaining      sony.sony_printer.total_prints_remaining%TYPE;
   n_printer_num                 sony.sony_printer.printer_num%TYPE;
   v_printer_label               sony.sony_printer.printer_label%TYPE;
   n_prints_remaining_threshold  sony.sony_printer.prints_remaining_threshold%TYPE;

   v_location_name      VARCHAR2 (50); /* device.device_cust_loc.location_name%TYPE */
   v_device_serial_cd   device.device_serial_cd%TYPE;
   v_alert_email_addr   sony.alert_email.alert_email_addr%TYPE;
   v_alert_email_name   sony.alert_email.alert_email_name%TYPE;
   v_email_subject      VARCHAR2 (4000);
   v_email_body         VARCHAR2 (4000);
   v_printer_status_txt VARCHAR2 (4000);

   CURSOR c_get_printers IS
      SELECT sp.sony_printer_id, machine_id, total_prints_remaining,
              printer_num, printer_label, prints_remaining_threshold
        FROM sony.sony_printer sp, sony.sony_print_rmn_alert spra
       WHERE sp.sony_printer_id = spra.sony_printer_id
         AND spra.sony_print_rmn_alert_new_ts IS NOT NULL
       ORDER BY machine_id, printer_num
       FOR UPDATE;

   CURSOR c_get_location_name (v_device_name IN device.device_name%TYPE) IS
      SELECT dcl.location_name, d.device_serial_cd
        FROM device.vw_device_last_active d,
             device.device_cust_loc dcl
       WHERE d.device_serial_cd = dcl.device_serial_cd
         AND d.device_name = v_device_name;

   CURSOR c_get_email_list
   (v_serial_cd IN v_device_serial_cd%TYPE) IS
      SELECT alert_email_addr, alert_email_name
        FROM sony.alert_email
       WHERE device_serial_cd = v_serial_cd;

   CURSOR c_get_printer_status_info
   (v_machine_id IN sony_printer.machine_id%TYPE) IS
      SELECT 'Printer'
        || CASE WHEN printer_label IS NULL THEN ' #' || printer_num ELSE ' "' || printer_label || '"' END
        || CASE WHEN sp.sony_printer_status_type_id != 1 THEN ' (' || spst.sony_printer_status_type_name || ')' ELSE '' END
        || ' paper level is'
        || CASE WHEN NVL(total_prints_remaining, 0) < prints_remaining_threshold THEN ' LOW' ELSE '' END
        || ': '
        || NVL(total_prints_remaining, 0) || ' (threshold ' || prints_remaining_threshold || ').' AS status_txt
        FROM sony.sony_printer sp,
             sony.sony_printer_status_type spst
       WHERE sp.sony_printer_status_type_id = spst.sony_printer_status_type_id
         AND machine_id = v_machine_id
         AND ( /* show active or status unknown printers only */
            sp.sony_printer_status_type_id = 1
            OR sp.sony_printer_status_type_id = 0
         )
       ORDER BY printer_label, sp.sony_printer_status_type_id DESC, printer_num;

BEGIN
   OPEN c_get_printers;
   LOOP
      FETCH c_get_printers
       INTO n_sony_printer_id, v_machine_id, n_total_prints_remaining, n_printer_num,
            v_printer_label, n_prints_remaining_threshold;
      EXIT WHEN c_get_printers%NOTFOUND;
      
      /* Update last alert time for this printer */
      UPDATE sony.sony_print_rmn_alert
         SET sony_print_rmn_alert_new_ts = NULL,
             sony_print_rmn_alert_last_ts = SYSDATE
       WHERE sony_printer_id = n_sony_printer_id;

      OPEN c_get_location_name (v_machine_id);
      FETCH c_get_location_name
       INTO v_location_name, v_device_serial_cd;
      CLOSE c_get_location_name;

      OPEN c_get_email_list (v_device_serial_cd);
      LOOP
         FETCH c_get_email_list
          INTO v_alert_email_addr, v_alert_email_name;
         EXIT WHEN c_get_email_list%NOTFOUND;

         v_email_subject := 'Paper Low at ' || v_location_name;
         v_email_body :=
               'The paper level (' || NVL(n_total_prints_remaining, 0)
            || ') of device S/N ' || v_device_serial_cd
            || ' printer'
            || CASE WHEN v_printer_label IS NULL THEN ' #' || n_printer_num ELSE ' "' || v_printer_label || '"' END
            || ' at location ' || v_location_name
            || ' is below the threshold (' || n_prints_remaining_threshold || ').'
            || CHR(13) || CHR(10)
            || CHR(13) || CHR(10);

         OPEN c_get_printer_status_info (v_machine_id);
         v_email_body := 
               v_email_body || 'Status for all active printers attached to this device:'
            || CHR(13) || CHR(10)
            || CHR(13) || CHR(10);
         LOOP
             FETCH c_get_printer_status_info
              INTO v_printer_status_txt;
             EXIT WHEN c_get_printer_status_info%NOTFOUND;

             v_email_body :=
                   v_email_body || v_printer_status_txt
                || CHR(13) || CHR(10);
         END LOOP;
         CLOSE c_get_printer_status_info;

         INSERT INTO ob_email_queue
                     (ob_email_from_email_addr, ob_email_from_name,
                      ob_email_to_email_addr, ob_email_to_name,
                      ob_email_subject,
                      ob_email_msg
                     )
              VALUES ('monitor@usatech.com', 'USA Tech Monitor',
                      --'sonykioskmonitor@usatech.com', 'Kiosk Monitor',
                      --'kiosk@am.sony.com', 'Kiosk Monitor',
                      v_alert_email_addr, v_alert_email_name,
                      v_email_subject,
                      v_email_body
                     );

      END LOOP;
      CLOSE c_get_email_list;

   END LOOP;
   CLOSE c_get_printers;
END;
/


-- End of DDL Script for Trigger SONY.TAIU_SONY_PRINTER
