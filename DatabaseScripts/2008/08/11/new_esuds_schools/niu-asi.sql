DECLARE
	l_school_name LOCATION.LOCATION_NAME%TYPE := 'Northern Illinois University';--<ENTER SCHOOL NAME>
	l_subdomain_name SUBDOMAIN_NAME.SUBDOMAIN_NAME_CD%TYPE := 'niu-asi'; --<ENTER SUBDOMAIN>
	
	l_school_id LOCATION.LOCATION_ID%TYPE;
BEGIN
	SELECT LOCATION_ID
	  INTO l_school_id
	  FROM LOCATION.LOCATION
	 WHERE UPPER(LOCATION_NAME) = UPPER(l_school_name)
	   AND LOCATION_TYPE_ID = 6;
	 
	INSERT INTO APP_USER.SUBDOMAIN_NAME(DOMAIN_NAME_URL, SUBDOMAIN_NAME_CD, APP_OBJECT_TYPE_ID, SUBDOMAIN_NAME_DESC, OBJECT_CD)
		SELECT DOMAIN_NAME_URL, l_subdomain_name, 6, l_school_name, l_school_id
		  FROM APP_USER.DOMAIN_NAME;
END;

INSERT INTO CUST_LOC (customer_id, location_id) (SELECT 3, location_id FROM location.location where location_name = 'Northern Illinois Main Campus');

