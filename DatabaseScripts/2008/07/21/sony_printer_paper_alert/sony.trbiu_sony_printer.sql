-- Start of DDL Script for Trigger SONY.TRAIU_SONY_PRINTER
-- Generated 7/20/2008 9:24:21 PM from SONY@USADBP.USATECH.COM

CREATE OR REPLACE TRIGGER sony.trbiu_sony_printer
 BEFORE
  INSERT OR UPDATE
 ON sony.sony_printer
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   IF (       NVL(:NEW.total_prints_remaining, 0) < :NEW.prints_remaining_threshold
          AND (   :OLD.total_prints_remaining >= :NEW.prints_remaining_threshold
               OR :OLD.total_prints_remaining IS NULL
              )
       OR (NVL(:NEW.total_prints_remaining, 0) <= 0)
      ) THEN

      /* toggle alert for this printer */
      :NEW.prints_remaining_new_alert_ts := SYSDATE;
   END IF;
END;
/


-- End of DDL Script for Trigger SONY.TRAIU_SONY_PRINTER
