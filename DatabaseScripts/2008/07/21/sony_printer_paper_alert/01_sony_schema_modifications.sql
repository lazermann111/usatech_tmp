alter table sony.sony_printer add (
	prints_remaining_new_alert_ts date,
	prints_remaining_last_alert_ts date
);

drop trigger sony.traiu_sony_printer;
