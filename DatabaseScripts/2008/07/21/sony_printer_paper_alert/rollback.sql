alter table sony.sony_printer drop column prints_remaining_new_alert_ts;
alter table sony.sony_printer drop column prints_remaining_last_alert_ts;

drop trigger sony.taiu_sony_printer;
drop trigger sony.trbiu_sony_printer;

-- Start of DDL Script for Trigger SONY.TRAIU_SONY_PRINTER
-- Generated 7/20/2008 11:33:35 PM from SONY@USADBP.USATECH.COM

CREATE OR REPLACE TRIGGER sony.traiu_sony_printer
 AFTER
  INSERT OR UPDATE
 ON sony.sony_printer
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
DECLARE
   n_return_code     FLOAT (10);
   v_error_message   VARCHAR2 (2000);
   v_location_name   location.location_name%TYPE;
   v_device_serial_cd   device.device_serial_cd%TYPE;
   v_alert_email_addr   sony.alert_email.alert_email_addr%TYPE;
   v_alert_email_name   sony.alert_email.alert_email_name%TYPE;

   CURSOR c_get_location_name IS
      SELECT l.location_name, d.device_serial_cd
        FROM location.location l,
             device.device d,
             pss.pos p
       WHERE d.device_name = :NEW.machine_id
         AND d.device_id = p.device_id
         AND p.location_id = l.location_id;

   CURSOR c_get_email_list
   (v_serial_cd IN v_device_serial_cd%TYPE) IS
      SELECT alert_email_addr, alert_email_name
        FROM sony.alert_email
       WHERE device_serial_cd = v_serial_cd;

BEGIN
   IF (       :NEW.total_prints_remaining < :NEW.prints_remaining_threshold
          AND (   :OLD.total_prints_remaining >= :NEW.prints_remaining_threshold
               OR :OLD.total_prints_remaining IS NULL
              )
       OR (:NEW.total_prints_remaining <= 0)
      ) THEN

      OPEN c_get_location_name;
      FETCH c_get_location_name
       INTO v_location_name, v_device_serial_cd;
      CLOSE c_get_location_name;

      OPEN c_get_email_list (v_device_serial_cd);
      LOOP
         FETCH c_get_email_list
          INTO v_alert_email_addr, v_alert_email_name;
         EXIT WHEN c_get_email_list%NOTFOUND;

         INSERT INTO ob_email_queue
                     (ob_email_from_email_addr, ob_email_from_name,
                      ob_email_to_email_addr, ob_email_to_name,
                      ob_email_subject,
                      ob_email_msg
                     )
              VALUES ('monitor@usatech.com', 'USA Tech Monitor',
                      --'sonykioskmonitor@usatech.com', 'Kiosk Monitor',
                      --'kiosk@am.sony.com', 'Kiosk Monitor',
                      v_alert_email_addr, v_alert_email_name,
                      'Paper Low',
                         'The paper level ('
                      || :NEW.total_prints_remaining
                      || ') of printer number '
                      || :NEW.printer_num
                      || ' at the location '
                      || v_location_name
                      || ' is below the threshold('
                      || :NEW.prints_remaining_threshold
                      || ').'
                     );

      END LOOP;
      CLOSE c_get_email_list;

   END IF;
END;
/


-- End of DDL Script for Trigger SONY.TRAIU_SONY_PRINTER
