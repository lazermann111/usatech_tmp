CREATE OR REPLACE PROCEDURE PSS.DEQUEUE_TRAN_MSG
(
    l_tran_id OUT NUMBER,
    l_dml_type OUT CHAR,
    l_device_serial_cd OUT VARCHAR2,
    l_location_name OUT VARCHAR2,
    l_tran_start_ts OUT DATE,
    l_tran_auth_amount OUT NUMBER,
    l_tran_parsed_acct_num OUT VARCHAR2,
    l_tran_state_cd OUT VARCHAR2,
    l_tran_auth_type_cd OUT VARCHAR2,
    l_payment_type OUT CHAR,
    l_machine_trans_no OUT VARCHAR2
    
)
   IS
    l_dequeue_options     DBMS_AQ.dequeue_options_t;
    l_message_properties  DBMS_AQ.message_properties_t;
    l_message_handle      RAW(16);
    l_tran_msg            t_tran_msg;
BEGIN
    DBMS_AQ.dequeue(queue_name          => 'q_tran_msg',
                    dequeue_options     => l_dequeue_options,
                    message_properties  => l_message_properties,
                    payload             => l_tran_msg,
                    msgid               => l_message_handle);

    l_tran_id := l_tran_msg.tran_id;
    l_dml_type := l_tran_msg.dml_type;
    
    IF l_tran_id > 0 THEN
        SELECT
            t.tran_start_ts,
            t.tran_auth_amount,
            t.tran_parsed_acct_num,
            t.tran_state_cd,
            t.tran_auth_type_cd,
            NULL, --t.machine_trans_no,
            d.device_serial_cd,
            l.location_name,
            DECODE(NVL(pp.payment_subtype_id, 1), 1, 'C', 'P')
        INTO
            l_tran_start_ts,
            l_tran_auth_amount,
            l_tran_parsed_acct_num,
            l_tran_state_cd,
            l_tran_auth_type_cd,
            l_machine_trans_no,
            l_device_serial_cd,
            l_location_name,
            l_payment_type
        FROM TRAN t, POS_PTA pp, POS p, DEVICE.DEVICE d, LOCATION.LOCATION l
        WHERE t.tran_id = l_tran_id
              AND t.pos_pta_id = pp.pos_pta_id (+)
              AND pp.pos_id = p.pos_id (+)
              AND p.device_id = d.device_id (+)
              AND p.location_id = l.location_id (+);
    END IF;
END;
