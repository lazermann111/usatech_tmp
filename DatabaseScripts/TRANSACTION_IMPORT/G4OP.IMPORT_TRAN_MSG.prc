CREATE OR REPLACE PROCEDURE IMPORT_TRAN_MSG
   IS
    l_cnt NUMBER;
    l_tran_id NUMBER(20,0);
    l_dml_type CHAR(1);
    l_device_serial_cd VARCHAR2(20);
    l_location_name VARCHAR2(60);
    l_tran_start_ts DATE;
    l_tran_auth_amount NUMBER(7,2);
    l_tran_parsed_acct_num VARCHAR2(19);
    l_tran_state_cd STG_CREDIT.tran_state_cd%TYPE;
    l_tran_auth_type_cd STG_CREDIT.tran_auth_type_cd%TYPE;
    l_machine_trans_no STG_CREDIT.machine_trans_no%TYPE;
    l_payment_type CHAR(1);
BEGIN
    LOOP
        BEGIN
            SELECT COUNT(1) INTO l_cnt FROM JOB_STATUS WHERE JOB_NAME = 'IMPORT_TRAN_MSG' AND STATUS = 'S';
            EXIT WHEN l_cnt > 0;
        
            UPDATE JOB_STATUS SET STATUS = 'R', UPD_DT = SYSDATE
            WHERE JOB_NAME = 'IMPORT_TRAN_MSG';
            COMMIT;

            DEQUEUE_TRAN_MSG@PSS_USADBD03
            (
                l_tran_id => l_tran_id,
                l_dml_type => l_dml_type,
                l_device_serial_cd => l_device_serial_cd,
                l_location_name => l_location_name,
                l_tran_start_ts => l_tran_start_ts,
                l_tran_auth_amount => l_tran_auth_amount,
                l_tran_parsed_acct_num => l_tran_parsed_acct_num,
                l_tran_state_cd => l_tran_state_cd,
                l_tran_auth_type_cd => l_tran_auth_type_cd,
                l_payment_type => l_payment_type,
                l_machine_trans_no => l_machine_trans_no
            );
            
            --DBMS_OUTPUT.put_line ('Tran id: ' || l_tran_id || ', DML Type: ' || l_dml_type);
            
            EXIT WHEN l_tran_id = 0;

            IF l_dml_type = 'I' THEN
                IF l_payment_type = 'C' THEN
                    INSERT INTO STG_CREDIT
                    (
                        express,
                        location,
                        trandate,
                        trantime,
                        apcode,
                        sale,
                        data1,
                        merch,
                        card,
                        data2,
                        startdate,
                        starttime,
                        data3,
                        data4,
                        tran_state_cd,
                        tran_auth_type_cd,
                        authority_cd,
                        import_file,
                        machine_trans_no
                    )
                    VALUES
                    (
                        SUBSTR(l_device_serial_cd, 1, 8),
                        SUBSTR(l_location_name, 1, 16),
                        TO_CHAR(l_tran_start_ts, 'MM/DD'),
                        TO_CHAR(l_tran_start_ts, 'HH24:MI'),
                        'APGReRix',
                        LPAD(TO_CHAR(l_tran_auth_amount * 100), 5, '0'),
                        '0000',
                        'E.179001415993577953',
                        l_tran_parsed_acct_num,
                        '~',
                        TO_CHAR(l_tran_start_ts, 'MM/DD'),
                        TO_CHAR(l_tran_start_ts, 'MI:SS'),
                        '0000',
                        '~',
                        l_tran_state_cd,
                        l_tran_auth_type_cd,
                        'H',
                        'RERIX_IMPORT',
                        l_machine_trans_no
                    );
                ELSE
                    INSERT INTO STG_PASSACCESS
                    (
                        express,
                        location,
                        trandate,
                        trantime,
                        card,
                        sale,
                        data3,
                        data4,
                        stopdate,
                        stoptime,
                        data5,
                        data6,
                        tran_state_cd,
                        import_file,
                        machine_trans_no
                    )
                    VALUES
                    (
                        SUBSTR(l_device_serial_cd, 1, 8),
                        SUBSTR(l_location_name, 1, 16),
                        TO_CHAR(l_tran_start_ts, 'MM/DD'),
                        TO_CHAR(l_tran_start_ts, 'HH24:MI'),
                        l_tran_parsed_acct_num,
                        LPAD(TO_CHAR(l_tran_auth_amount * 100), 5, '0'),
                        '0000',
                        '~',
                        TO_CHAR(l_tran_start_ts, 'MM/DD'),
                        TO_CHAR(l_tran_start_ts, 'MI:SS'),
                        '0000',
                        '~',
                        l_tran_state_cd,
                        'RERIX_IMPORT',
                        l_machine_trans_no
                    );
                END IF;
            ELSIF l_dml_type = 'U' THEN
                IF l_machine_trans_no IS NOT NULL THEN
                    IF l_payment_type = 'C' THEN
                        UPDATE STG_CREDIT
                        SET
                            express = SUBSTR(l_device_serial_cd, 1, 8),
                            location = SUBSTR(l_location_name, 1, 16),
                            trandate = TO_CHAR(l_tran_start_ts, 'MM/DD'),
                            trantime = TO_CHAR(l_tran_start_ts, 'HH24:MI'),
                            apcode = 'APGReRix',
                            sale = LPAD(TO_CHAR(l_tran_auth_amount * 100), 5, '0'),
                            data1 = '0000',
                            merch = 'E.179001415993577953',
                            card = l_tran_parsed_acct_num,
                            data2 = '~',
                            startdate = TO_CHAR(l_tran_start_ts, 'MM/DD'),
                            starttime = TO_CHAR(l_tran_start_ts, 'MI:SS'),
                            data3 = '0000',
                            data4 = '~',
                            tran_state_cd = l_tran_state_cd,
                            tran_auth_type_cd = l_tran_auth_type_cd,
                            authority_cd = 'H',
                            import_file = 'RERIX_IMPORT'
                        WHERE machine_trans_no = l_machine_trans_no;
                    ELSE
                        UPDATE STG_PASSACCESS
                        SET
                            express = SUBSTR(l_device_serial_cd, 1, 8),
                            location = SUBSTR(l_location_name, 1, 16),
                            trandate = TO_CHAR(l_tran_start_ts, 'MM/DD'),
                            trantime = TO_CHAR(l_tran_start_ts, 'HH24:MI'),
                            card = l_tran_parsed_acct_num,
                            sale = LPAD(TO_CHAR(l_tran_auth_amount * 100), 5, '0'),
                            data3 = '0000',
                            data4 = '~',
                            stopdate = TO_CHAR(l_tran_start_ts, 'MM/DD'),
                            stoptime = TO_CHAR(l_tran_start_ts, 'MI:SS'),
                            data5 = '0000',
                            data6 = '~',
                            tran_state_cd = l_tran_state_cd,
                            import_file = 'RERIX_IMPORT'
                        WHERE machine_trans_no = l_machine_trans_no;
                    END IF;
                END IF;
            ELSIF l_dml_type = 'D' THEN
                IF l_machine_trans_no IS NOT NULL THEN
                    IF l_payment_type = 'C' THEN
                        DELETE FROM STG_CREDIT
                        WHERE machine_trans_no = l_machine_trans_no;
                    ELSE
                        DELETE FROM STG_PASSACCESS
                        WHERE machine_trans_no = l_machine_trans_no;
                    END IF;
                END IF;
            END IF;

            COMMIT;
        EXCEPTION
            WHEN OTHERS THEN
                ROLLBACK;
        END;
    END LOOP;
    COMMIT;
END;
