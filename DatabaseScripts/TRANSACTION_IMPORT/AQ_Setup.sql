--PSS schema

CREATE type t_tran_msg as object (
tran_id     NUMBER(20,0),
dml_type    CHAR(1)); 

EXECUTE DBMS_AQADM.CREATE_QUEUE_TABLE (queue_table => 'qt_tran_msg', multiple_consumers => TRUE, queue_payload_type => 't_tran_msg');
EXECUTE DBMS_AQADM.CREATE_QUEUE (queue_name => 'q_tran_msg', queue_table => 'qt_tran_msg', retry_delay => 60, max_retries => 10);
EXECUTE DBMS_AQADM.START_QUEUE (queue_name => 'q_tran_msg');

--EXECUTE DBMS_AQADM.STOP_QUEUE (queue_name => 'q_tran_msg');
--EXECUTE DBMS_AQADM.DROP_QUEUE (queue_name => 'q_tran_msg');
--EXECUTE DBMS_AQADM.DROP_QUEUE_TABLE (queue_table => 'qt_tran_msg');
--DROP TYPE t_tran_msg

--------------------------------------------------
--G4OP schema

CREATE TABLE job_status
    (job_name                       VARCHAR2(20),
    status                         CHAR(1),
    upd_dt                         DATE)
  PCTFREE     10
  PCTUSED     40
  INITRANS    1
  MAXTRANS    255
  TABLESPACE  small_datafile
  STORAGE   (
    INITIAL     131072
    NEXT        131072
    PCTINCREASE 0
    MINEXTENTS  1
    MAXEXTENTS  2147483645
  )

INSERT INTO JOB_STATUS(job_name) VALUES('IMPORT_TRAN_MSG')