
ALTER TABLE LOCATION.COUNTRY ADD
  (
    CURRENCY_CD      VARCHAR2(3),
    CONSTRAINT FK_COUNTRY_CURRENCY_CD foreign key(CURRENCY_CD) references PSS.CURRENCY(CURRENCY_CD)
  );


COMMENT ON TABLE LOCATION.COUNTRY is 'Contains a listing of all the different countires a location can be assigned to.';
COMMENT ON COLUMN LOCATION.COUNTRY.CURRENCY_CD is 'Default currency used by country.';


INSERT INTO location.country (country_cd, country_name) VALUES ('AF','AFGHANISTAN');
INSERT INTO location.country (country_cd, country_name) VALUES ('AX','ALAND ISLANDS');
INSERT INTO location.country (country_cd, country_name) VALUES ('AL','ALBANIA');
INSERT INTO location.country (country_cd, country_name) VALUES ('DZ','ALGERIA');
INSERT INTO location.country (country_cd, country_name) VALUES ('AS','AMERICAN SAMOA');
INSERT INTO location.country (country_cd, country_name) VALUES ('AD','ANDORRA');
INSERT INTO location.country (country_cd, country_name) VALUES ('AO','ANGOLA');
INSERT INTO location.country (country_cd, country_name) VALUES ('AI','ANGUILLA');
INSERT INTO location.country (country_cd, country_name) VALUES ('AQ','ANTARCTICA');
INSERT INTO location.country (country_cd, country_name) VALUES ('AG','ANTIGUA AND BARBUDA');
INSERT INTO location.country (country_cd, country_name) VALUES ('AR','ARGENTINA');
INSERT INTO location.country (country_cd, country_name) VALUES ('AM','ARMENIA');
INSERT INTO location.country (country_cd, country_name) VALUES ('AW','ARUBA');
INSERT INTO location.country (country_cd, country_name) VALUES ('AU','AUSTRALIA');
INSERT INTO location.country (country_cd, country_name) VALUES ('AT','AUSTRIA');
INSERT INTO location.country (country_cd, country_name) VALUES ('AZ','AZERBAIJAN');
INSERT INTO location.country (country_cd, country_name) VALUES ('BS','BAHAMAS');
INSERT INTO location.country (country_cd, country_name) VALUES ('BH','BAHRAIN');
INSERT INTO location.country (country_cd, country_name) VALUES ('BD','BANGLADESH');
INSERT INTO location.country (country_cd, country_name) VALUES ('BB','BARBADOS');
INSERT INTO location.country (country_cd, country_name) VALUES ('BY','BELARUS');
INSERT INTO location.country (country_cd, country_name) VALUES ('BE','BELGIUM');
INSERT INTO location.country (country_cd, country_name) VALUES ('BZ','BELIZE');
INSERT INTO location.country (country_cd, country_name) VALUES ('BJ','BENIN');
INSERT INTO location.country (country_cd, country_name) VALUES ('BM','BERMUDA');
INSERT INTO location.country (country_cd, country_name) VALUES ('BT','BHUTAN');
INSERT INTO location.country (country_cd, country_name, currency_cd) VALUES ('BO','BOLIVIA','BOB');
INSERT INTO location.country (country_cd, country_name) VALUES ('BA','BOSNIA AND HERZEGOVINA');
INSERT INTO location.country (country_cd, country_name) VALUES ('BW','BOTSWANA');
INSERT INTO location.country (country_cd, country_name) VALUES ('BV','BOUVET ISLAND');
INSERT INTO location.country (country_cd, country_name) VALUES ('BR','BRAZIL');
INSERT INTO location.country (country_cd, country_name) VALUES ('IO','BRITISH INDIAN OCEAN TERRITORY');
INSERT INTO location.country (country_cd, country_name) VALUES ('BN','BRUNEI DARUSSALAM');
INSERT INTO location.country (country_cd, country_name) VALUES ('BG','BULGARIA');
INSERT INTO location.country (country_cd, country_name) VALUES ('BF','BURKINA FASO');
INSERT INTO location.country (country_cd, country_name) VALUES ('BI','BURUNDI');
INSERT INTO location.country (country_cd, country_name) VALUES ('KH','CAMBODIA');
INSERT INTO location.country (country_cd, country_name) VALUES ('CM','CAMEROON');
INSERT INTO location.country (country_cd, country_name) VALUES ('CA','CANADA');
INSERT INTO location.country (country_cd, country_name) VALUES ('CV','CAPE VERDE');
INSERT INTO location.country (country_cd, country_name) VALUES ('KY','CAYMAN ISLANDS');
INSERT INTO location.country (country_cd, country_name) VALUES ('CF','CENTRAL AFRICAN REPUBLIC');
INSERT INTO location.country (country_cd, country_name) VALUES ('TD','CHAD');
INSERT INTO location.country (country_cd, country_name, currency_cd) VALUES ('CL','CHILE','CLP');
INSERT INTO location.country (country_cd, country_name) VALUES ('CN','CHINA');
INSERT INTO location.country (country_cd, country_name) VALUES ('CX','CHRISTMAS ISLAND');
INSERT INTO location.country (country_cd, country_name) VALUES ('CC','COCOS (KEELING) ISLANDS');
INSERT INTO location.country (country_cd, country_name, currency_cd) VALUES ('CO','COLOMBIA','COP');
INSERT INTO location.country (country_cd, country_name) VALUES ('KM','COMOROS');
INSERT INTO location.country (country_cd, country_name) VALUES ('CG','CONGO');
INSERT INTO location.country (country_cd, country_name) VALUES ('CD','CONGO, THE DEMOCRATIC REPUBLIC OF THE');
INSERT INTO location.country (country_cd, country_name) VALUES ('CK','COOK ISLANDS');
INSERT INTO location.country (country_cd, country_name) VALUES ('CR','COSTA RICA');
INSERT INTO location.country (country_cd, country_name) VALUES ('HR','CROATIA');
INSERT INTO location.country (country_cd, country_name) VALUES ('CU','CUBA');
INSERT INTO location.country (country_cd, country_name) VALUES ('CY','CYPRUS');
INSERT INTO location.country (country_cd, country_name) VALUES ('CZ','CZECH REPUBLIC');
INSERT INTO location.country (country_cd, country_name) VALUES ('DK','DENMARK');
INSERT INTO location.country (country_cd, country_name) VALUES ('DJ','DJIBOUTI');
INSERT INTO location.country (country_cd, country_name) VALUES ('DM','DOMINICA');
INSERT INTO location.country (country_cd, country_name) VALUES ('DO','DOMINICAN REPUBLIC');
INSERT INTO location.country (country_cd, country_name) VALUES ('EC','ECUADOR');
INSERT INTO location.country (country_cd, country_name) VALUES ('EG','EGYPT');
INSERT INTO location.country (country_cd, country_name) VALUES ('SV','EL SALVADOR');
INSERT INTO location.country (country_cd, country_name) VALUES ('GQ','EQUATORIAL GUINEA');
INSERT INTO location.country (country_cd, country_name) VALUES ('ER','ERITREA');
INSERT INTO location.country (country_cd, country_name) VALUES ('EE','ESTONIA');
INSERT INTO location.country (country_cd, country_name) VALUES ('ET','ETHIOPIA');
INSERT INTO location.country (country_cd, country_name) VALUES ('FK','FALKLAND ISLANDS (MALVINAS)');
INSERT INTO location.country (country_cd, country_name) VALUES ('FO','FAROE ISLANDS');
INSERT INTO location.country (country_cd, country_name) VALUES ('FJ','FIJI');
INSERT INTO location.country (country_cd, country_name) VALUES ('FI','FINLAND');
INSERT INTO location.country (country_cd, country_name) VALUES ('FR','FRANCE');
INSERT INTO location.country (country_cd, country_name) VALUES ('GF','FRENCH GUIANA');
INSERT INTO location.country (country_cd, country_name) VALUES ('PF','FRENCH POLYNESIA');
INSERT INTO location.country (country_cd, country_name) VALUES ('TF','FRENCH SOUTHERN TERRITORIES');
INSERT INTO location.country (country_cd, country_name) VALUES ('GA','GABON');
INSERT INTO location.country (country_cd, country_name) VALUES ('GM','GAMBIA');
INSERT INTO location.country (country_cd, country_name) VALUES ('GE','GEORGIA');
INSERT INTO location.country (country_cd, country_name) VALUES ('DE','GERMANY');
INSERT INTO location.country (country_cd, country_name) VALUES ('GH','GHANA');
INSERT INTO location.country (country_cd, country_name) VALUES ('GI','GIBRALTAR');
INSERT INTO location.country (country_cd, country_name) VALUES ('GR','GREECE');
INSERT INTO location.country (country_cd, country_name) VALUES ('GL','GREENLAND');
INSERT INTO location.country (country_cd, country_name) VALUES ('GD','GRENADA');
INSERT INTO location.country (country_cd, country_name) VALUES ('GP','GUADELOUPE');
INSERT INTO location.country (country_cd, country_name) VALUES ('GU','GUAM');
INSERT INTO location.country (country_cd, country_name) VALUES ('GT','GUATEMALA');
INSERT INTO location.country (country_cd, country_name) VALUES ('GN','GUINEA');
INSERT INTO location.country (country_cd, country_name) VALUES ('GW','GUINEA-BISSAU');
INSERT INTO location.country (country_cd, country_name) VALUES ('GY','GUYANA');
INSERT INTO location.country (country_cd, country_name) VALUES ('HT','HAITI');
INSERT INTO location.country (country_cd, country_name) VALUES ('HM','HEARD ISLAND AND MCDONALD ISLANDS');
INSERT INTO location.country (country_cd, country_name) VALUES ('VA','HOLY SEE (VATICAN CITY STATE)');
INSERT INTO location.country (country_cd, country_name) VALUES ('HN','HONDURAS');
INSERT INTO location.country (country_cd, country_name) VALUES ('HK','HONG KONG');
INSERT INTO location.country (country_cd, country_name) VALUES ('HU','HUNGARY');
INSERT INTO location.country (country_cd, country_name) VALUES ('IS','ICELAND');
INSERT INTO location.country (country_cd, country_name) VALUES ('IN','INDIA');
INSERT INTO location.country (country_cd, country_name) VALUES ('ID','INDONESIA');
INSERT INTO location.country (country_cd, country_name) VALUES ('IR','IRAN, ISLAMIC REPUBLIC OF');
INSERT INTO location.country (country_cd, country_name) VALUES ('IQ','IRAQ');
INSERT INTO location.country (country_cd, country_name) VALUES ('IE','IRELAND');
INSERT INTO location.country (country_cd, country_name) VALUES ('IL','ISRAEL');
INSERT INTO location.country (country_cd, country_name) VALUES ('IT','ITALY');
INSERT INTO location.country (country_cd, country_name) VALUES ('JM','JAMAICA');
INSERT INTO location.country (country_cd, country_name) VALUES ('JP','JAPAN');
INSERT INTO location.country (country_cd, country_name) VALUES ('JO','JORDAN');
INSERT INTO location.country (country_cd, country_name) VALUES ('KZ','KAZAKHSTAN');
INSERT INTO location.country (country_cd, country_name) VALUES ('KE','KENYA');
INSERT INTO location.country (country_cd, country_name) VALUES ('KI','KIRIBATI');
INSERT INTO location.country (country_cd, country_name) VALUES ('KR','KOREA, REPUBLIC OF');
INSERT INTO location.country (country_cd, country_name) VALUES ('KW','KUWAIT');
INSERT INTO location.country (country_cd, country_name) VALUES ('KG','KYRGYZSTAN');
INSERT INTO location.country (country_cd, country_name) VALUES ('LV','LATVIA');
INSERT INTO location.country (country_cd, country_name) VALUES ('LB','LEBANON');
INSERT INTO location.country (country_cd, country_name) VALUES ('LS','LESOTHO');
INSERT INTO location.country (country_cd, country_name) VALUES ('LR','LIBERIA');
INSERT INTO location.country (country_cd, country_name) VALUES ('LY','LIBYAN ARAB JAMAHIRIYA');
INSERT INTO location.country (country_cd, country_name) VALUES ('LI','LIECHTENSTEIN');
INSERT INTO location.country (country_cd, country_name) VALUES ('LT','LITHUANIA');
INSERT INTO location.country (country_cd, country_name) VALUES ('LU','LUXEMBOURG');
INSERT INTO location.country (country_cd, country_name) VALUES ('MO','MACAO');
INSERT INTO location.country (country_cd, country_name) VALUES ('MK','MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF');
INSERT INTO location.country (country_cd, country_name) VALUES ('MG','MADAGASCAR');
INSERT INTO location.country (country_cd, country_name) VALUES ('MW','MALAWI');
INSERT INTO location.country (country_cd, country_name) VALUES ('MY','MALAYSIA');
INSERT INTO location.country (country_cd, country_name) VALUES ('MV','MALDIVES');
INSERT INTO location.country (country_cd, country_name) VALUES ('ML','MALI');
INSERT INTO location.country (country_cd, country_name) VALUES ('MT','MALTA');
INSERT INTO location.country (country_cd, country_name) VALUES ('MH','MARSHALL ISLANDS');
INSERT INTO location.country (country_cd, country_name) VALUES ('MQ','MARTINIQUE');
INSERT INTO location.country (country_cd, country_name) VALUES ('MR','MAURITANIA');
INSERT INTO location.country (country_cd, country_name) VALUES ('MU','MAURITIUS');
INSERT INTO location.country (country_cd, country_name) VALUES ('YT','MAYOTTE');
INSERT INTO location.country (country_cd, country_name, currency_cd) VALUES ('MX','MEXICO', 'MXN');
INSERT INTO location.country (country_cd, country_name) VALUES ('FM','MICRONESIA, FEDERATED STATES OF');
INSERT INTO location.country (country_cd, country_name) VALUES ('MD','MOLDOVA, REPUBLIC OF');
INSERT INTO location.country (country_cd, country_name) VALUES ('MC','MONACO');
INSERT INTO location.country (country_cd, country_name) VALUES ('MN','MONGOLIA');
INSERT INTO location.country (country_cd, country_name) VALUES ('MS','MONTSERRAT');
INSERT INTO location.country (country_cd, country_name) VALUES ('MA','MOROCCO');
INSERT INTO location.country (country_cd, country_name) VALUES ('MZ','MOZAMBIQUE');
INSERT INTO location.country (country_cd, country_name) VALUES ('MM','MYANMAR');
INSERT INTO location.country (country_cd, country_name) VALUES ('NA','NAMIBIA');
INSERT INTO location.country (country_cd, country_name) VALUES ('NR','NAURU');
INSERT INTO location.country (country_cd, country_name) VALUES ('NP','NEPAL');
INSERT INTO location.country (country_cd, country_name) VALUES ('NL','NETHERLANDS');
INSERT INTO location.country (country_cd, country_name) VALUES ('AN','NETHERLANDS ANTILLES');
INSERT INTO location.country (country_cd, country_name) VALUES ('NC','NEW CALEDONIA');
INSERT INTO location.country (country_cd, country_name) VALUES ('NZ','NEW ZEALAND');
INSERT INTO location.country (country_cd, country_name) VALUES ('NI','NICARAGUA');
INSERT INTO location.country (country_cd, country_name) VALUES ('NE','NIGER');
INSERT INTO location.country (country_cd, country_name) VALUES ('NG','NIGERIA');
INSERT INTO location.country (country_cd, country_name) VALUES ('NU','NIUE');
INSERT INTO location.country (country_cd, country_name) VALUES ('NF','NORFOLK ISLAND');
INSERT INTO location.country (country_cd, country_name) VALUES ('MP','NORTHERN MARIANA ISLANDS');
INSERT INTO location.country (country_cd, country_name) VALUES ('NO','NORWAY');
INSERT INTO location.country (country_cd, country_name) VALUES ('OM','OMAN');
INSERT INTO location.country (country_cd, country_name) VALUES ('PK','PAKISTAN');
INSERT INTO location.country (country_cd, country_name) VALUES ('PW','PALAU');
INSERT INTO location.country (country_cd, country_name) VALUES ('PS','PALESTINIAN TERRITORY, OCCUPIED');
INSERT INTO location.country (country_cd, country_name) VALUES ('PA','PANAMA');
INSERT INTO location.country (country_cd, country_name) VALUES ('PG','PAPUA NEW GUINEA');
INSERT INTO location.country (country_cd, country_name) VALUES ('PY','PARAGUAY');
INSERT INTO location.country (country_cd, country_name) VALUES ('PE','PERU');
INSERT INTO location.country (country_cd, country_name) VALUES ('PH','PHILIPPINES');
INSERT INTO location.country (country_cd, country_name) VALUES ('PN','PITCAIRN');
INSERT INTO location.country (country_cd, country_name) VALUES ('PL','POLAND');
INSERT INTO location.country (country_cd, country_name) VALUES ('PT','PORTUGAL');
INSERT INTO location.country (country_cd, country_name) VALUES ('PR','PUERTO RICO');
INSERT INTO location.country (country_cd, country_name) VALUES ('QA','QATAR');
INSERT INTO location.country (country_cd, country_name) VALUES ('RE','REUNION');
INSERT INTO location.country (country_cd, country_name) VALUES ('RO','ROMANIA');
INSERT INTO location.country (country_cd, country_name) VALUES ('RU','RUSSIAN FEDERATION');
INSERT INTO location.country (country_cd, country_name) VALUES ('RW','RWANDA');
INSERT INTO location.country (country_cd, country_name) VALUES ('SH','SAINT HELENA');
INSERT INTO location.country (country_cd, country_name) VALUES ('KN','SAINT KITTS AND NEVIS');
INSERT INTO location.country (country_cd, country_name) VALUES ('LC','SAINT LUCIA');
INSERT INTO location.country (country_cd, country_name) VALUES ('PM','SAINT PIERRE AND MIQUELON');
INSERT INTO location.country (country_cd, country_name) VALUES ('VC','SAINT VINCENT AND THE GRENADINES');
INSERT INTO location.country (country_cd, country_name) VALUES ('WS','SAMOA');
INSERT INTO location.country (country_cd, country_name) VALUES ('SM','SAN MARINO');
INSERT INTO location.country (country_cd, country_name) VALUES ('ST','SAO TOME AND PRINCIPE');
INSERT INTO location.country (country_cd, country_name) VALUES ('SA','SAUDI ARABIA');
INSERT INTO location.country (country_cd, country_name) VALUES ('SN','SENEGAL');
INSERT INTO location.country (country_cd, country_name) VALUES ('CS','SERBIA AND MONTENEGRO');
INSERT INTO location.country (country_cd, country_name) VALUES ('SC','SEYCHELLES');
INSERT INTO location.country (country_cd, country_name) VALUES ('SL','SIERRA LEONE');
INSERT INTO location.country (country_cd, country_name) VALUES ('SG','SINGAPORE');
INSERT INTO location.country (country_cd, country_name) VALUES ('SK','SLOVAKIA');
INSERT INTO location.country (country_cd, country_name) VALUES ('SI','SLOVENIA');
INSERT INTO location.country (country_cd, country_name) VALUES ('SB','SOLOMON ISLANDS');
INSERT INTO location.country (country_cd, country_name) VALUES ('SO','SOMALIA');
INSERT INTO location.country (country_cd, country_name) VALUES ('ZA','SOUTH AFRICA');
INSERT INTO location.country (country_cd, country_name) VALUES ('GS','SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS');
INSERT INTO location.country (country_cd, country_name) VALUES ('ES','SPAIN');
INSERT INTO location.country (country_cd, country_name) VALUES ('LK','SRI LANKA');
INSERT INTO location.country (country_cd, country_name) VALUES ('SD','SUDAN');
INSERT INTO location.country (country_cd, country_name) VALUES ('SR','SURINAME');
INSERT INTO location.country (country_cd, country_name) VALUES ('SJ','SVALBARD AND JAN MAYEN');
INSERT INTO location.country (country_cd, country_name) VALUES ('SZ','SWAZILAND');
INSERT INTO location.country (country_cd, country_name) VALUES ('SE','SWEDEN');
INSERT INTO location.country (country_cd, country_name, currency_cd) VALUES ('CH','SWITZERLAND','CHF');
INSERT INTO location.country (country_cd, country_name) VALUES ('SY','SYRIAN ARAB REPUBLIC');
INSERT INTO location.country (country_cd, country_name) VALUES ('TW','TAIWAN, PROVINCE OF CHINA');
INSERT INTO location.country (country_cd, country_name) VALUES ('TJ','TAJIKISTAN');
INSERT INTO location.country (country_cd, country_name) VALUES ('TZ','TANZANIA, UNITED REPUBLIC OF');
INSERT INTO location.country (country_cd, country_name) VALUES ('TH','THAILAND');
INSERT INTO location.country (country_cd, country_name) VALUES ('TL','TIMOR-LESTE');
INSERT INTO location.country (country_cd, country_name) VALUES ('TG','TOGO');
INSERT INTO location.country (country_cd, country_name) VALUES ('TK','TOKELAU');
INSERT INTO location.country (country_cd, country_name) VALUES ('TO','TONGA');
INSERT INTO location.country (country_cd, country_name) VALUES ('TT','TRINIDAD AND TOBAGO');
INSERT INTO location.country (country_cd, country_name) VALUES ('TN','TUNISIA');
INSERT INTO location.country (country_cd, country_name, currency_cd) VALUES ('TR','TURKEY','TRY');
INSERT INTO location.country (country_cd, country_name) VALUES ('TM','TURKMENISTAN');
INSERT INTO location.country (country_cd, country_name) VALUES ('TC','TURKS AND CAICOS ISLANDS');
INSERT INTO location.country (country_cd, country_name) VALUES ('TV','TUVALU');
INSERT INTO location.country (country_cd, country_name) VALUES ('UG','UGANDA');
INSERT INTO location.country (country_cd, country_name) VALUES ('UA','UKRAINE');
INSERT INTO location.country (country_cd, country_name) VALUES ('AE','UNITED ARAB EMIRATES');
INSERT INTO location.country (country_cd, country_name) VALUES ('GB','UNITED KINGDOM');
INSERT INTO location.country (country_cd, country_name, currency_cd) VALUES ('US','UNITED STATES', 'USD');
INSERT INTO location.country (country_cd, country_name) VALUES ('UM','UNITED STATES MINOR OUTLYING ISLANDS');
INSERT INTO location.country (country_cd, country_name) VALUES ('UY','URUGUAY');
INSERT INTO location.country (country_cd, country_name) VALUES ('UZ','UZBEKISTAN');
INSERT INTO location.country (country_cd, country_name) VALUES ('VU','VANUATU');
INSERT INTO location.country (country_cd, country_name) VALUES ('VE','VENEZUELA');
INSERT INTO location.country (country_cd, country_name) VALUES ('VN','VIETNAM');
INSERT INTO location.country (country_cd, country_name) VALUES ('VG','VIRGIN ISLANDS, BRITISH');
INSERT INTO location.country (country_cd, country_name) VALUES ('VI','VIRGIN ISLANDS, U.S.');
INSERT INTO location.country (country_cd, country_name) VALUES ('WF','WALLIS AND FUTUNA');
INSERT INTO location.country (country_cd, country_name) VALUES ('EH','WESTERN SAHARA');
INSERT INTO location.country (country_cd, country_name) VALUES ('YE','YEMEN');
INSERT INTO location.country (country_cd, country_name) VALUES ('ZM','ZAMBIA');
INSERT INTO location.country (country_cd, country_name) VALUES ('ZW','ZIMBABWE');
INSERT INTO location.country (country_cd, country_name) VALUES ('CI','COTE D''IVOIRE');
INSERT INTO location.country (country_cd, country_name) VALUES ('KP','KOREA, DEMOCRATIC PEOPLE''S REPUBLIC OF');
INSERT INTO location.country (country_cd, country_name) VALUES ('LA','LAO PEOPLE''S DEMOCRATIC REPUBLIC');

ALTER TRIGGER LOCATION.TRBU_COUNTRY DISABLE;

UPDATE LOCATION.country SET currency_cd = 'USD' WHERE country_cd = 'US';
UPDATE LOCATION.country SET currency_cd = 'CAD' WHERE country_cd = 'CA';

UPDATE LOCATION.country co
SET currency_cd = (
	SELECT currency_cd
	FROM pss.currency
	WHERE co.country_cd = SUBSTR(currency_cd, 0, 2)
)
WHERE currency_cd IS NULL AND country_cd = (
	SELECT SUBSTR(currency_cd, 0, 2)
	FROM pss.currency
	WHERE co.country_cd = SUBSTR(currency_cd, 0, 2)
);

ALTER TRIGGER LOCATION.TRBU_COUNTRY ENABLE;

CREATE TABLE LOCATION.COUNTRY_CURRENCY
  (
    COUNTRY_CD   VARCHAR2(2),
    CURRENCY_CD  VARCHAR2(3),
    CONSTRAINT PK_CNTRY_CUR_CNTRY_CD_CUR_CD primary key(COUNTRY_CD,CURRENCY_CD),
    CONSTRAINT FK_COUNTRY_CUR_COUNTRY_CD foreign key(COUNTRY_CD) references LOCATION.COUNTRY(COUNTRY_CD),
    CONSTRAINT FK_COUNTRY_CUR_CUR_CD foreign key(CURRENCY_CD) references PSS.CURRENCY(CURRENCY_CD)
  )
  TABLESPACE LOCATION_DATA;

CREATE PUBLIC SYNONYM COUNTRY_CURRENCY FOR LOCATION.COUNTRY_CURRENCY;

GRANT SELECT, INSERT, UPDATE, DELETE ON LOCATION.COUNTRY_CURRENCY TO WEB_USER;


CREATE TABLE LOCATION.CUSTOMER_MERCHANT
  (
    CUSTOMER_ID  NUMBER(20,0),
    MERCHANT_ID  NUMBER(20,0),
    CONSTRAINT PK_CUST_MERCH_CUST_ID_MERCH_ID primary key(CUSTOMER_ID,MERCHANT_ID),
    CONSTRAINT FK_CUSTOMER_MERCH_CUSTOMER_ID foreign key(CUSTOMER_ID) references LOCATION.CUSTOMER(CUSTOMER_ID),
    CONSTRAINT FK_CUSTOMER_MERCH_MERCHANT_ID foreign key(MERCHANT_ID) references PSS.MERCHANT(MERCHANT_ID)
  )
  TABLESPACE LOCATION_DATA;

CREATE PUBLIC SYNONYM CUSTOMER_MERCHANT FOR LOCATION.CUSTOMER_MERCHANT;

GRANT SELECT, INSERT, UPDATE, DELETE ON LOCATION.CUSTOMER_MERCHANT TO WEB_USER;


COMMIT;