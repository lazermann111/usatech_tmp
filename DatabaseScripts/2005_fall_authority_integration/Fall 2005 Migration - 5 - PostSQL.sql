
-- tran table 

-- comment out all of alter table tran drop columns statements


-- synch tran table
-- connect pss/pss;

INSERT  
INTO pss.tran_new 
(tran_id,
tran_start_ts,
tran_upload_ts,
tran_end_ts,
tran_state_cd,
tran_desc,
consumer_acct_id,
tran_device_tran_cd,
created_by,
created_ts,
last_updated_by,
last_updated_ts,
tran_account_pin,
tran_received_raw_acct_data,
tran_parsed_acct_name,
tran_parsed_acct_exp_date,
tran_parsed_acct_num,
tran_reportable_acct_num,
pos_pta_id,
tran_device_result_type_cd,
tran_global_trans_cd,
tran_legacy_trans_no
-- tran_parsed_acct_num_hash,
-- tran_parsed_acct_num_encr
)
SELECT  
tran_id,
tran_start_ts,
tran_upload_ts,
tran_end_ts,
tran_state_cd,
tran_desc,
consumer_acct_id,
device_tran_cd,   /* rename device_tran_cd to tran_device_tran_cd */
created_by,
created_ts,
last_updated_by,
last_updated_ts,
tran_account_pin,
tran_received_raw_acct_data,
tran_parsed_acct_name,
tran_parsed_acct_exp_date,
tran_parsed_acct_num,
tran_reportable_acct_num,
pos_pta_id,
tran_client_tran_cd, /* rename tran_client_tran_cd to tran_device_result_type_cd */
tran_global_trans_no, /* rename tran_global_trans_no to tran_global_trans_cd */
tran_legacy_trans_no
-- tran_parsed_acct_num_hash,
-- tran_parsed_acct_num_encr
FROM pss.tran
WHERE  tran_id > 850666;


DROP PUBLIC SYNONYM TRAN;
CREATE PUBLIC SYNONYM TRAN FOR PSS.TRAN;
-- rename tran table;

connect pss/pss;
RENAME tran TO tran_old;

RENAME tran_new TO tran;

-- rename index

ALTER INDEX inx_tranlegacytransno rename to inx_tranlegacytransno_3;
ALTER INDEX inx_tranlegacytransno_2 rename to inx_tranlegacytransno;

ALTER INDEX ix_tran_pos_pta_id  rename to ix_tran_pos_pta_id_3;
ALTER INDEX ix_tran_pos_pta_id_2  rename to ix_tran_pos_pta_id;


ALTER INDEX inx_tranconsacctid  rename to inx_tranconsacctid_3;
ALTER INDEX inx_tranconsacctid_2  rename to inx_tranconsacctid;


ALTER INDEX idx_device_tran_cd  rename to idx_device_tran_cd_3;
ALTER INDEX idx_device_tran_cd_2  rename to idx_device_tran_cd;

connect system/system;

CREATE PUBLIC SYNONYM TRAN FOR PSS.TRAN;

GRANT SELECT, INSERT, UPDATE, DELETE ON PSS.TRAN TO WEB_USER;



-- recreate constraints and other stuff

ALTER TABLE pss.tran_old
DROP CONSTRAINT fk_tran_tran_state;

ALTER TABLE pss.tran_old
DROP CONSTRAINT fk_tran_pos_payment_type_autho;

ALTER TABLE pss.tran_old
DROP CONSTRAINT fk_tran_tran;

ALTER TABLE pss.tran_old
DROP CONSTRAINT fk_tran_consumer_account; 

ALTER TABLE pss.tran_old
DROP CONSTRAINT fk_tran_tran_account_entry_met
/

ALTER TABLE pss.tran_old
DROP CONSTRAINT fk_tran_tran_auth_type 
/

ALTER TABLE pss.tran_old
DROP CONSTRAINT fk_tran_tran_client_tran 
/


-- ALTER TABLE pss.tran_old
-- DROP CONSTRAINT fk_tran_tran_dvc_res_type_cd; 

alter table pss.TRAN_ENCRYPTION_KEY drop constraint FK_TRAN_ENCRYPTION_KEY_TRAN;
alter table pss.TRAN_LINE_ITEM drop constraint FK_TRAN_LINE_ITEM_TRAN;
alter table pss.TRAN_REFUND drop  constraint FK_TRAN_REFUND_TRAN;
alter table pss.TRAN_SETTLEMENT_BATCH drop constraint FK_TRAN_SETTLEMENT_BATCH_TRAN;
alter table pss.AUTH drop constraint FK_AUTH_TRAN_ID;

ALTER TABLE pss.tran_old
DROP CONSTRAINT pk_tran;

DROP INDEX pss.pk_tran;
ALTER TABLE pss.tran
ADD CONSTRAINT pk_tran PRIMARY KEY (tran_id)
/

-- recreate constraint 

-- ???
ALTER TABLE pss.tran_encryption_key
ADD CONSTRAINT fk_tran_encryption_key_tran FOREIGN KEY (tran_id)
REFERENCES PSS.tran (tran_id)
/

ALTER TABLE pss.tran_line_item
ADD CONSTRAINT fk_tran_line_item_tran FOREIGN KEY (tran_id)
REFERENCES PSS.tran (tran_id)
/

ALTER TABLE pss.tran_refund
ADD CONSTRAINT fk_tran_refund_tran FOREIGN KEY (tran_id)
REFERENCES PSS.tran (tran_id)
/

ALTER TABLE pss.tran_settlement_batch_old
ADD CONSTRAINT fk_tran_settlement_batch_tran FOREIGN KEY (tran_id)
REFERENCES PSS.tran (tran_id)
/

ALTER TABLE pss.auth
ADD CONSTRAINT fk_auth_tran_id FOREIGN KEY (tran_id)
REFERENCES PSS.tran (tran_id)
/


-- Triggers for PSS.TRAN

DROP TRIGGER pss.trbu_tran;

CREATE OR REPLACE TRIGGER pss.trbu_tran
 BEFORE
  UPDATE
 ON pss.tran
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
     INTO :NEW.created_by,
          :NEW.created_ts,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/


DROP TRIGGER pss.trbi_tran;

CREATE OR REPLACE TRIGGER pss.trbi_tran
 BEFORE
  INSERT
 ON pss.tran
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
   IF :NEW.tran_id IS NULL
   THEN
      SELECT SEQ_tran_id.NEXTVAL
        INTO :NEW.tran_id
        FROM DUAL;
   END IF;

   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/


-- Comments for PSS.TRAN

COMMENT ON TABLE pss.tran IS 'Contains information about each transactions recorded and/or processed by the PSS system.  A transaction begins when a user or process initiates a transactions and ends when the user or process ends the transaction, a timeout occurs, or an error occurs.  A transaction does not contain information relating to what was purchased, only that a transaction was started and ended.'
/
COMMENT ON COLUMN pss.tran.tran_desc IS 'A description of the transaction that can be provided by the client.'
/
COMMENT ON COLUMN pss.tran.tran_device_tran_cd IS 'The transaction id/code generated by the device. The code will typically be unique for each client.  The code may be reset or start over causing duplicate order codes over time.'
/
COMMENT ON COLUMN pss.tran.tran_end_ts IS 'The date and time the transaction was ended. The value can be generated by the user pressing end, allowing the device to time out, etc.'
/
COMMENT ON COLUMN pss.tran.tran_parsed_acct_exp_date IS 'The experation data of the account used for the transaction.  Values will be encrypted. It should be in the form MMYYYY.  It is derived from TRAN_RECEIVED_RAW_ACCT_DATA.'
/
COMMENT ON COLUMN pss.tran.tran_parsed_acct_name IS 'The name of the person who executed the transaction.  Values will be encrypted.  Derived from the TRAN_RECEIVED_RAW_ACCT_DATA field.'
/
COMMENT ON COLUMN pss.tran.tran_parsed_acct_num IS 'The account number used for the transaction.  Values will be encrypted.  Derived from the TRAN_RECEIVED_RAW_ACCT_DATA field.'
/
COMMENT ON COLUMN pss.tran.tran_received_raw_acct_data IS 'The data received at the device to authorize the transactions.  Values will be encrypted. Will typically be the map stripe data on a swiped card.'
/
COMMENT ON COLUMN pss.tran.tran_reportable_acct_num IS 'A reportable representation of the account number used for the transactions.  An example would be XXXX-XXXX-XXXX-3234 (last 4 digits of a credit card).  Values will not be encrypted.'
/
COMMENT ON COLUMN pss.tran.tran_start_ts IS 'The date and time the transaction was initiated.  The value will typically be generated by a card swipe, fob swipe, etc.'
/
COMMENT ON COLUMN pss.tran.tran_upload_ts IS 'The date and time the transaction was received by the server.'
/

-- End of DDL Script for Table PSS.TRAN

-- Foreign Key




ALTER TABLE pss.tran
ADD CONSTRAINT fk_tran_tran_state FOREIGN KEY (tran_state_cd)
REFERENCES PSS.tran_state (tran_state_cd)
/


ALTER TABLE pss.tran
ADD CONSTRAINT fk_tran_pos_payment_type_autho FOREIGN KEY (pos_pta_id)
REFERENCES PSS.pos_pta (pos_pta_id)
/


ALTER TABLE pss.tran
ADD CONSTRAINT fk_tran_tran FOREIGN KEY (tran_id)
REFERENCES PSS.tran (tran_id)
/


ALTER TABLE pss.tran
ADD CONSTRAINT fk_tran_consumer_account FOREIGN KEY (consumer_acct_id)
REFERENCES PSS.consumer_acct (consumer_acct_id)
/

ALTER TABLE pss.tran
ADD CONSTRAINT fk_tran_tran_dvc_res_type_cd FOREIGN KEY (
  tran_device_result_type_cd)
REFERENCES PSS.tran_device_result_type (tran_device_result_type_cd)
/

ALTER TABLE PSS.REFUND ADD CONSTRAINT FK_REFUND_TRAN_ID foreign key(TRAN_ID) references PSS.TRAN(TRAN_ID)
/

-- End of DDL script for Foreign Key(s)

-- end tran table



-- auth table

-- synch auth table 


CREATE OR REPLACE TRIGGER PSS.TRBI_AUTH BEFORE INSERT ON PSS.AUTH
  FOR EACH ROW 
BEGIN
	IF :NEW.auth_id IS NULL
	THEN
		SELECT SEQ_auth_id.NEXTVAL
		INTO :NEW.auth_id
		FROM DUAL;
	END IF;
END;
/


-- create an auth record for each non-cash tran record
INSERT  
INTO PSS.AUTH
(
	TRAN_ID, 
	AUTH_TYPE_CD, 
	AUTH_STATE_ID, 
	AUTH_PARSED_ACCT_DATA, 
	ACCT_ENTRY_METHOD_CD, 
	AUTH_AMT, 
	AUTH_TS, 
	AUTH_RESULT_CD, 
	AUTH_AUTHORITY_TRAN_CD, 
	CREATED_BY, 
	CREATED_TS, 
	LAST_UPDATED_BY, 
	LAST_UPDATED_TS
)
SELECT 
	T.TRAN_ID, 
	TRAN_AUTH_TYPE_CD, 
--	DECODE(T.TRAN_STATE_CD, '2', 1, '6', 2, '7', 3, '5', 4, '0', 5, 1),
      DECODE(T.TRAN_STATE_CD, '2', 1, '6', 2, '7', 3, '5', 4, '0', 5, 'E', 4, 'I', 4, 2),
	T.TRAN_RECEIVED_RAW_ACCT_DATA, 
	T.TRAN_ACCOUNT_ENTRY_METHOD_CD, 
	NVL(TRAN_AUTH_AMOUNT, 0),
	NVL(T.TRAN_AUTH_TS, T.TRAN_START_TS), 
	DECODE(T.TRAN_AUTH_RESULT_CD, 'Y', 'Y', 'N'), 
	T.AUTHORITY_TRAN_CD, 
	T.CREATED_BY, 
	T.CREATED_TS, 
	T.LAST_UPDATED_BY, 
	T.LAST_UPDATED_TS 
FROM PSS.TRAN_OLD T, PSS.POS_PTA PP, PSS.PAYMENT_SUBTYPE PS 
WHERE 
	T.POS_PTA_ID = PP.POS_PTA_ID 
	AND PP.PAYMENT_SUBTYPE_ID = PS.PAYMENT_SUBTYPE_ID 
	AND PS.CLIENT_PAYMENT_TYPE_CD <> 'M'
        AND T.TRAN_ID > 8508666; 


-- create a post-auth sale record for each non-cash tran record
INSERT 
INTO PSS.AUTH
(
	TRAN_ID, 
	AUTH_TYPE_CD, 
	AUTH_STATE_ID, 
	AUTH_PARSED_ACCT_DATA, 
	ACCT_ENTRY_METHOD_CD, 
	AUTH_AMT, 
	AUTH_TS, 
	AUTH_RESULT_CD, 
	AUTH_AUTHORITY_TRAN_CD, 
	CREATED_BY, 
	CREATED_TS, 
	LAST_UPDATED_BY, 
	LAST_UPDATED_TS
)
SELECT 
	T.TRAN_ID, 
	'U', 
	-- DECODE(T.TRAN_STATE_CD, '2', 1, '6', 2, '7', 3, '5', 4, '0', 5, 1), 
      DECODE(T.TRAN_STATE_CD, '2', 1, '6', 2, '7', 3, '5', 4, '0', 5, 'E', 4, 'I', 4, 2),
	T.TRAN_RECEIVED_RAW_ACCT_DATA, 
	T.TRAN_ACCOUNT_ENTRY_METHOD_CD, 
	NVL(NVL((SELECT SUM((NVL(tran_line_item_amount, 0) + NVL(tran_line_item_tax, 0)) * NVL(tran_line_item_quantity, 0)) FROM PSS.TRAN_LINE_ITEM WHERE TRAN_ID = T.TRAN_ID AND TRAN_LINE_ITEM_BATCH_TYPE_CD = 'I'),
		(SELECT SUM((NVL(tran_line_item_amount, 0) + NVL(tran_line_item_tax, 0)) * NVL(tran_line_item_quantity, 0)) FROM PSS.TRAN_LINE_ITEM WHERE TRAN_ID = T.TRAN_ID AND TRAN_LINE_ITEM_BATCH_TYPE_CD = 'A')), 0),
	NVL(T.TRAN_AUTH_TS, T.TRAN_START_TS), 
	DECODE(T.TRAN_AUTH_RESULT_CD, 'Y', 'Y', 'N'), 
	T.AUTHORITY_TRAN_CD, 
	T.CREATED_BY, 
	T.CREATED_TS, 
	T.LAST_UPDATED_BY, 
	T.LAST_UPDATED_TS 
FROM PSS.TRAN_OLD T, PSS.POS_PTA PP, PSS.PAYMENT_SUBTYPE PS 
WHERE 
	T.POS_PTA_ID = PP.POS_PTA_ID 
	AND PP.PAYMENT_SUBTYPE_ID = PS.PAYMENT_SUBTYPE_ID 
	AND PS.CLIENT_PAYMENT_TYPE_CD <> 'M'
        AND T.TRAN_ID > 8508666; 


-- create a post-auth sale record for each cash tran record
INSERT   
INTO PSS.AUTH
(
	TRAN_ID, 
	AUTH_TYPE_CD, 
	AUTH_STATE_ID, 
	AUTH_PARSED_ACCT_DATA, 
	ACCT_ENTRY_METHOD_CD, 
	AUTH_AMT, 
	AUTH_TS, 
	AUTH_RESULT_CD, 
	AUTH_AUTHORITY_TRAN_CD, 
	CREATED_BY, 
	CREATED_TS, 
	LAST_UPDATED_BY, 
	LAST_UPDATED_TS
)
SELECT 
	T.TRAN_ID, 
	'U',
--	DECODE(T.TRAN_STATE_CD, '2', 1, '6', 2, '7', 3, '5', 4, '0', 5, 1), 
      DECODE(T.TRAN_STATE_CD, '2', 1, '6', 2, '7', 3, '5', 4, '0', 5, 'E', 4, 'I', 4, 2),
	T.TRAN_RECEIVED_RAW_ACCT_DATA, 
	T.TRAN_ACCOUNT_ENTRY_METHOD_CD, 
	NVL((SELECT SUM((NVL(tran_line_item_amount, 0) + NVL(tran_line_item_tax, 0)) * NVL(tran_line_item_quantity, 0)) FROM PSS.TRAN_LINE_ITEM WHERE TRAN_ID = T.TRAN_ID AND TRAN_LINE_ITEM_BATCH_TYPE_CD = 'A'), 0),
	NVL(T.TRAN_AUTH_TS, T.TRAN_START_TS), 
	DECODE(T.TRAN_AUTH_RESULT_CD, 'Y', 'Y', 'N'), 
	T.AUTHORITY_TRAN_CD, 
	T.CREATED_BY, 
	T.CREATED_TS, 
	T.LAST_UPDATED_BY, 
	T.LAST_UPDATED_TS 
FROM PSS.TRAN_OLD T, PSS.POS_PTA PP, PSS.PAYMENT_SUBTYPE PS 
WHERE 
	T.POS_PTA_ID = PP.POS_PTA_ID 
	AND PP.PAYMENT_SUBTYPE_ID = PS.PAYMENT_SUBTYPE_ID 
	AND PS.CLIENT_PAYMENT_TYPE_CD = 'M'
        AND T.TRAN_ID > 8508666; 
        

-- Triggers for PSS.AUTH
        
CREATE OR REPLACE TRIGGER PSS.TRBI_AUTH BEFORE INSERT ON PSS.AUTH
  FOR EACH ROW 
BEGIN
	IF :NEW.auth_id IS NULL
	THEN
		SELECT SEQ_auth_id.NEXTVAL
		INTO :NEW.auth_id
		FROM DUAL;
	END IF;

	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/


CREATE OR REPLACE TRIGGER PSS.TRBU_AUTH BEFORE UPDATE ON PSS.AUTH
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/


-- add constraint 

ALTER TABLE PSS.AUTH ADD CONSTRAINT FK_AUTH_TRAN_ID foreign key(TRAN_ID) references PSS.TRAN(TRAN_ID);
ALTER TABLE PSS.AUTH ADD CONSTRAINT FK_AUTH_AUTH_TYPE_CD foreign key(AUTH_TYPE_CD) references PSS.AUTH_TYPE(AUTH_TYPE_CD);
ALTER TABLE PSS.AUTH ADD CONSTRAINT FK_AUTH_AUTH_STATE_ID foreign key(AUTH_STATE_ID) references PSS.AUTH_STATE(AUTH_STATE_ID);
ALTER TABLE PSS.AUTH ADD CONSTRAINT FK_AUTH_ACCT_ENTRY_MTHD_CD foreign key(ACCT_ENTRY_METHOD_CD) references PSS.ACCT_ENTRY_METHOD(ACCT_ENTRY_METHOD_CD);
ALTER TABLE PSS.AUTH ADD CONSTRAINT FK_AUTH_AUTH_RESULT_CD foreign key(AUTH_RESULT_CD) references PSS.AUTH_RESULT(AUTH_RESULT_CD);


-- end auth table


-- tran_settlement_batch table

-- comment out update PSS.TRAN_SETTLEMENT_BATCH statement

--synch tran_settlement_batch

INSERT INTO PSS.TRAN_SETTLEMENT_BATCH_NEW
(SETTLEMENT_BATCH_ID, AUTH_ID, TRAN_SETTLEMENT_B_AMT, created_by, created_ts, last_updated_by, last_updated_ts)
SELECT TSB.SETTLEMENT_BATCH_ID, A.AUTH_ID, A.AUTH_AMT, a.created_by, a.created_ts,
a.last_updated_by, a.last_updated_ts
FROM PSS.TRAN_SETTLEMENT_BATCH TSB, PSS.AUTH A
WHERE TSB.TRAN_ID = A.TRAN_ID AND A.AUTH_TYPE_CD = 'U'
and A.TRAN_ID > 8508666;

-- rename table

DROP PUBLIC SYNONYM TRAN_SETTLEMENT_BATCH;

connect pss/pss;

RENAME TRAN_SETTLEMENT_BATCH TO TRAN_SETTLEMENT_BATCH_OLD;
RENAME TRAN_SETTLEMENT_BATCH_NEW TO TRAN_SETTLEMENT_BATCH;

connect system/system;

CREATE PUBLIC SYNONYM TRAN_SETTLEMENT_BATCH FOR PSS.TRAN_SETTLEMENT_BATCH;

GRANT SELECT, INSERT, UPDATE, DELETE ON PSS.TRAN_SETTLEMENT_BATCH TO WEB_USER;

-- recreate constraints and other stuff


--ALTER TABLE pss.tran_settlement_batch_old
--DROP CONSTRAINT pk_auth_id_settlement_batch_id; 


ALTER TABLE pss.tran_settlement_batch
ADD CONSTRAINT pk_auth_id_settlement_batch_id PRIMARY KEY (auth_id, 
  settlement_batch_id)
USING INDEX
  PCTFREE     10
  INITRANS    2
  MAXTRANS    255
  TABLESPACE  pss_data
  STORAGE   (
    INITIAL     5242880
    NEXT        5242880
    PCTINCREASE 0
    MINEXTENTS  1
    MAXEXTENTS  2147483645
  )
/



-- Triggers for PSS.TRAN_SETTLEMENT_BATCH

CREATE OR REPLACE TRIGGER pss.trbi_tran_settlement_batch
 BEFORE
  INSERT
 ON pss.tran_settlement_batch
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER pss.trbu_tran_settlement_batch
 BEFORE
  UPDATE
 ON pss.tran_settlement_batch
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/


-- Comments for PSS.TRAN_SETTLEMENT_BATCH

COMMENT ON TABLE pss.tran_settlement_batch IS 'Information about attempts to batch transactions.'
/
COMMENT ON COLUMN pss.tran_settlement_batch.tran_settlement_b_amt IS 'The amount (in dollars?) that was settled for the transaction.'
/

-- End of DDL Script for Table PSS.TRAN_SETTLEMENT_BATCH

-- Foreign Key

ALTER TABLE pss.tran_settlement_batch_old
DROP CONSTRAINT fk_tran_settlement_batch_settl
/


ALTER TABLE pss.tran_settlement_batch
ADD CONSTRAINT fk_tran_settlement_batch_settl FOREIGN KEY (settlement_batch_id)
REFERENCES PSS.settlement_batch (settlement_batch_id)
/



--ALTER TABLE pss.tran_settlement_batch_old
--DROP CONSTRAINT fk_tran_settlmt_batch_auth_id;


ALTER TABLE pss.tran_settlement_batch
ADD CONSTRAINT fk_tran_settlmt_batch_auth_id FOREIGN KEY (auth_id)
REFERENCES PSS.auth (auth_id)
/
-- End of DDL script for Foreign Key(s)

-- end tran_settlement_batch table


-- settlement_batch table

-- replace sb_ts_start to sb_ts_end
-- replace sb_ts_end to sb_ts_start
-- move the update pss.settlement_batch to after migration script.



--CREATE INDEX PSS.IDX_AUTH_TRAN_ID ON PSS.AUTH(TRAN_ID) TABLESPACE PSS_INDX;


/*ALTER TABLE PSS.TRAN_SETTLEMENT_BATCH ADD
(
	CONSTRAINT PK_AUTH_ID_SETTLEMENT_BATCH_ID primary key(AUTH_ID,SETTLEMENT_BATCH_ID)
);*/


INSERT INTO PSS.REFUND(REFUND_ID, TRAN_ID, REFUND_AMT, REFUND_DESC, REFUND_ISSUE_TS, REFUND_ISSUE_BY, REFUND_TYPE_CD, CREATED_BY, CREATED_TS, LAST_UPDATED_BY, LAST_UPDATED_TS, REFUND_STATE_ID)
SELECT TRAN_REFUND_ID, TRAN_ID, TRAN_REFUND_AMOUNT, TRAN_REFUND_DESC, TRAN_REFUND_TS, TRAN_REFUND_ISSED_BY, TRAN_REFUND_TYPE_CD, CREATED_BY, CREATED_TS, LAST_UPDATED_BY, LAST_UPDATED_TS, TRAN_REFUND_STATE_ID
FROM PSS.TRAN_REFUND;

CREATE OR REPLACE TRIGGER PSS.TRBI_REFUND BEFORE INSERT ON PSS.REFUND
  FOR EACH ROW 
BEGIN
   IF :NEW.refund_id IS NULL
   THEN
      SELECT seq_refund_id.NEXTVAL
        INTO :NEW.refund_id
        FROM DUAL;
   END IF;

   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER PSS.TRBU_REFUND BEFORE UPDATE ON PSS.REFUND
  FOR EACH ROW 
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
     INTO :NEW.created_by,
          :NEW.created_ts,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/

CREATE INDEX PSS.IDX_REFUND_REFUND_TYPE_CD ON PSS.REFUND(REFUND_TYPE_CD) TABLESPACE PSS_INDX;
CREATE INDEX PSS.IDX_REFUND_TRAN_ID ON PSS.REFUND(TRAN_ID) TABLESPACE PSS_INDX;


-- synchronize refund sequence
DECLARE 
    l_id NUMBER;
    l_max_id NUMBER;
BEGIN
    SELECT NVL(MAX(refund_id), 0) INTO l_max_id FROM pss.refund;
    --dbms_output.put_line('max refund_id: ' || TO_CHAR(l_max_id));
    
    LOOP
        SELECT pss.seq_refund_id.NEXTVAL INTO l_id FROM dual;
        
        IF l_id >= l_max_id THEN
            EXIT;
        END IF;
    END LOOP;
    
    --dbms_output.put_line('last seq_refund_id: ' || TO_CHAR(l_id));
END;
/


INSERT INTO PSS.REFUND_SETTLEMENT_BATCH(REFUND_ID, SETTLEMENT_BATCH_ID, REFUND_SETTLEMENT_B_AMT, CREATED_BY, CREATED_TS, LAST_UPDATED_BY, LAST_UPDATED_TS)
SELECT TR.TRAN_REFUND_ID, TR.SETTLEMENT_BATCH_ID, TR.TRAN_REFUND_AMOUNT, SB.CREATED_BY, SB.CREATED_TS, SB.LAST_UPDATED_BY, SB.LAST_UPDATED_TS 
FROM PSS.TRAN_REFUND TR, PSS.SETTLEMENT_BATCH SB
WHERE TR.SETTLEMENT_BATCH_ID = SB.SETTLEMENT_BATCH_ID;


CREATE OR REPLACE TRIGGER PSS.TRBI_REFUND_SETTLEMENT_BATCH BEFORE INSERT ON PSS.REFUND_SETTLEMENT_BATCH
  FOR EACH ROW 
BEGIN
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/


CREATE OR REPLACE TRIGGER PSS.TRBU_REFUND_SETTLEMENT_BATCH BEFORE UPDATE ON PSS.REFUND_SETTLEMENT_BATCH
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/




ALTER TABLE PSS.ARAMARK_AUTHORITY DROP CONSTRAINT FK_ARAMARK_AUTHORITY_AST;
ALTER TABLE PSS.ARAMARK_AUTHORITY ADD CONSTRAINT FK_ARMRK_AUTY_AUTY_SVC_TYPE_ID foreign key(AUTHORITY_SERVICE_TYPE_ID) references AUTHORITY.AUTHORITY_SERVICE_TYPE(AUTHORITY_SERVICE_TYPE_ID);

ALTER TABLE PSS.BLACKBRD_AUTHORITY DROP CONSTRAINT FK_AST_ID_BLACKBRD;
ALTER TABLE PSS.BLACKBRD_AUTHORITY ADD 
(
	BLACKBRD_SEQUENCE_NUM      NUMBER(6,0)     default 1,
	CONSTRAINT FK_BLKBRD_AUTY_AUTY_SVC_TYP_ID foreign key(AUTHORITY_SERVICE_TYPE_ID) references AUTHORITY.AUTHORITY_SERVICE_TYPE(AUTHORITY_SERVICE_TYPE_ID)
);

ALTER TABLE PSS.INTERNAL_AUTHORITY DROP CONSTRAINT FK_AST_ID_INTERNAL;
ALTER TABLE PSS.INTERNAL_AUTHORITY ADD CONSTRAINT FK_INTR_AUTY_AUTY_SVC_TYPE_ID foreign key(AUTHORITY_SERVICE_TYPE_ID) references AUTHORITY.AUTHORITY_SERVICE_TYPE(AUTHORITY_SERVICE_TYPE_ID);

DROP TABLE PSS.CC_TERMINAL;
DROP PUBLIC SYNONYM CC_TERMINAL;

DROP TABLE PSS.CC_MERCHANT;
DROP PUBLIC SYNONYM CC_MERCHANT;

DROP TABLE PSS.CC_AUTHORITY;
DROP PUBLIC SYNONYM CC_AUTHORITY;

DROP TABLE PSS.CC_PAYMENT_TYPE;
DROP PUBLIC SYNONYM CC_PAYMENT_TYPE;

DROP TABLE PSS.AUTHORITY_SERVICE_TYPE;
DROP PUBLIC SYNONYM AUTHORITY_SERVICE_TYPE;

CREATE PUBLIC SYNONYM AUTHORITY_SERVICE_TYPE FOR AUTHORITY.AUTHORITY_SERVICE_TYPE;


DROP TABLE PSS.TRAN_FOR_DUP_DETECT;

DROP TABLE PSS.TRAN_FILTERED_DUP;
DROP PUBLIC SYNONYM TRAN_FILTERED_DUP;

DROP TABLE PSS.TRAN_ENCRYPTION_KEY;

DROP TABLE PSS.TRAN_LOCAL_AUTH_BATCH_ARCHIVE;
DROP PUBLIC SYNONYM TRAN_LOCAL_AUTH_BATCH_ARCHIVE;

DROP TABLE PSS.TRAN_STAGING;

DROP TABLE PSS.TRAN_ACCOUNT_ENTRY_METHOD;

DROP TABLE PSS.TRAN_AUTH_TYPE;

DROP TABLE PSS.TRAN_REFUND;
DROP PUBLIC SYNONYM TRAN_REFUND;

DROP TABLE PSS.TRAN_REFUND_STATE;
DROP PUBLIC SYNONYM TRAN_REFUND_STATE;

DROP TABLE PSS.TRAN_BATCH_TYPE;

DROP TABLE PSS.TRAN_CLIENT_TRAN;
DROP PUBLIC SYNONYM TRAN_CLIENT_TRAN;

DROP TABLE PSS.TRAN_BATCH_ARCHIVE;

DROP SEQUENCE PSS.SEQ_TRAN_BATCH_ARCHIVE_ID;
DROP SEQUENCE PSS.SEQ_TRAN_FILTERED_DUP_ID;
DROP SEQUENCE PSS.SEQ_TRAN_FOR_DUP_DETECT_ID;
DROP SEQUENCE PSS.SEQ_TRAN_LOCAL_AUTH_BA_ID;


CREATE OR REPLACE 
PROCEDURE pss.sp_terminal_batch_num_inc (
	pn_terminal_id     IN       pss.terminal.terminal_id%TYPE,
	pn_batch_num_max   IN       pss.terminal.terminal_batch_num%TYPE,
	pn_nxt_batch_num   OUT      pss.terminal.terminal_batch_num%TYPE,
	pn_return_code     OUT      app_exec_hist.exception_code.exception_code_id%TYPE,
	pv_error_message   OUT      app_exec_hist.exception_data.additional_information%TYPE
) AS
--
-- Purpose: Gets next device batch num from a terminal
--
-- MODIFICATION HISTORY
-- Person       Date        Comments
-- ---------    ------      ---------------------------------------
-- erybski     05.10.06     Initial creation
--
	n_cur_batch_num			NUMBER;
	cv_package_name			CONSTANT VARCHAR2 (30)	:= '';
	cv_procedure_name		CONSTANT VARCHAR2 (30)	:= 'sp_terminal_batch_num_inc';

	CURSOR c_get_terminal_batch_num IS
		SELECT terminal_batch_num
		FROM pss.terminal
		WHERE terminal_id = pn_terminal_id
		FOR UPDATE;
		
	e_batch_num_max_null	EXCEPTION;
	e_terminal_id_notfound	EXCEPTION;

BEGIN
	pn_nxt_batch_num := NULL;

	-- validate parameters
	IF pn_batch_num_max IS NULL THEN
		RAISE e_batch_num_max_null;
	END IF;

	-- get current terminal batch num
	OPEN c_get_terminal_batch_num;
	FETCH c_get_terminal_batch_num INTO n_cur_batch_num;
	CLOSE c_get_terminal_batch_num;

	IF c_get_terminal_batch_num%NOTFOUND = TRUE THEN
		RAISE e_terminal_id_notfound;
	END IF;

	-- increment batch num
	IF n_cur_batch_num = pn_batch_num_max THEN
		pn_nxt_batch_num := 1;
	ELSE
		pn_nxt_batch_num := n_cur_batch_num + 1;
	END IF;

	UPDATE pss.terminal
		SET terminal_batch_num = pn_nxt_batch_num
		WHERE terminal_id = pn_terminal_id;

EXCEPTION
	WHEN e_batch_num_max_null THEN
		pv_error_message := 'Required parameter pn_batch_num_max not defined';
		pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution;
		pkg_exception_processor.sp_log_exception(
			pn_return_code,
			pv_error_message,
			pkg_app_exec_hist_globals.cv_server_name,
			cv_package_name || '.' || cv_procedure_name
		);
		pn_nxt_batch_num := NULL;
	WHEN e_terminal_id_notfound THEN
		pv_error_message := 'Could not find the following terminal id in the database: ' || pn_terminal_id;
		pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution;
		pkg_exception_processor.sp_log_exception(
			pn_return_code,
			pv_error_message,
			pkg_app_exec_hist_globals.cv_server_name,
			cv_package_name || '.' || cv_procedure_name
		);
		pn_nxt_batch_num := NULL;
	WHEN OTHERS THEN
		pv_error_message := 'An unknown exception occurred in ' || cv_procedure_name || ' = ' || SQLCODE || ', ' || SQLERRM;
		pn_return_code := pkg_app_exec_hist_globals.unknown_error_id;
		pkg_exception_processor.sp_log_exception(
			pn_return_code,
			pv_error_message,
			pkg_app_exec_hist_globals.cv_server_name,
			cv_package_name || '.' || cv_procedure_name
		);
		pn_nxt_batch_num := NULL;
END;
/

GRANT EXECUTE ON pss.sp_terminal_batch_num_inc TO WEB_USER;


--UPDATE PSS.SETTLEMENT_BATCH SET SETTLEMENT_BATCH_END_TS = SETTLEMENT_BATCH_START_TS;

COMMIT;
