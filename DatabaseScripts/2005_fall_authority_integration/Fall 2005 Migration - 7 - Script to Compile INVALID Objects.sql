/*EXEC DBMS_UTILITY.compile_schema('APP_USER');
EXEC DBMS_UTILITY.compile_schema('APP_EXEC_HIST');
EXEC DBMS_UTILITY.compile_schema('AUTHORITY');
EXEC DBMS_UTILITY.compile_schema('DEVICE');
EXEC DBMS_UTILITY.compile_schema('PSS');
EXEC DBMS_UTILITY.compile_schema('LOCATION');*/

SELECT 'ALTER ' || decode (object_type, 'PACKAGE BODY', 'PACKAGE', object_type) ||
	' ' || owner || '.' || object_name || ' COMPILE ' ||
       	decode (object_type, 'PACKAGE BODY', 'BODY', NULL) || ';'
FROM ALL_OBJECTS
WHERE status = 'INVALID'
AND owner in ('APP_EXEC_HIST', 'APP_USER', 'AUTHORITY', 'DEVICE', 'LOCATION', 'PSS')
AND UPPER(object_name) NOT LIKE 'BIN$%' AND object_type <> 'SYNONYM'
ORDER BY decode (object_type, 'VIEW',      '10',
                              'FUNCTION',  '20',
                              'PROCEDURE', '30',
                              'PACKAGE',   '40', object_type);
