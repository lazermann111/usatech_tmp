CREATE OR REPLACE VIEW pss.vw_tran_info
(
    tran_id
  , pre_auth_ts
  , pre_auth_amount
  , pre_auth_result_cd
  , pre_auth_tran_cd
  , account_entry_method_cd
  , settlement_amount
  , settlement_ts
  , settlement_result_cd
  , settlement_tran_cd
  , tran_amount
  , line_item_count
)
AS
SELECT tli.tran_id,
       a.auth_ts AS pre_auth_ts,
       a.auth_amt AS pre_auth_amount,
       a.auth_result_cd AS pre_auth_result_cd,
       a.auth_authority_tran_cd AS pre_auth_tran_cd,
       NVL(sa.acct_entry_method_cd, a.acct_entry_method_cd) AS account_entry_method_cd,
       tsb.tran_settlement_b_amt AS settlement_amount,
       sb.settlement_batch_end_ts AS settlement_ts,
       sa.auth_result_cd AS settlement_result_cd,
       sa.auth_authority_tran_cd AS settlement_tran_cd,
       tli.tran_line_tot_amount AS tran_amount,
       tli.line_item_count AS line_item_count
  FROM (
    SELECT tran_id, tran_line_item_batch_type_cd,
            SUM(tran_line_item_amount * tran_line_item_quantity) tran_line_tot_amount,
            SUM(tran_line_item_quantity) line_item_count,
            FIRST_VALUE(tran_line_item_batch_type_cd) 
            OVER (PARTITION BY tran_id ORDER BY tran_line_item_batch_type_cd DESC)
            AS use_batch_type_cd
        FROM tran_line_item
        GROUP BY tran_id, tran_line_item_batch_type_cd) tli, auth a, auth sa, tran_settlement_batch tsb, settlement_batch sb
 WHERE tli.tran_id = a.tran_id (+)
   AND NVL(a.auth_type_cd, ' ') IN('N', 'L', ' ')
   AND tli.tran_id = sa.tran_id (+)
   AND sa.auth_id = tsb.auth_id (+)
   AND tsb.settlement_batch_id = sb.settlement_batch_id (+)
   AND sb.settlement_batch_state_id (+) = 1
   AND tli.tran_line_item_batch_type_cd = tli.use_batch_type_cd
/   


CREATE OR REPLACE 
PACKAGE PSS.pkg_reports 
  IS
--
-- Holds various procedures for reports
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  ------------------------------------------       
-- B KRUG       05/05/05 NEW
-- D KOUZNETSOV 10/06/05 CHANGES FOR AUTHORITY INTEGRATION
    TYPE REF_CURSOR IS REF CURSOR;

  FUNCTION GET_RECONCILIATION_SUMMARY
     ( l_location_id IN POS.LOCATION_ID%TYPE,
       l_start_ts IN TRAN.TRAN_START_TS%TYPE,
       l_end_ts IN TRAN.TRAN_START_TS%TYPE)
     RETURN REF_CURSOR;
     
  FUNCTION GET_RECONCILIATION_DETAIL
     ( l_location_id IN POS.LOCATION_ID%TYPE,
       l_start_ts IN TRAN.TRAN_START_TS%TYPE,
       l_end_ts IN TRAN.TRAN_START_TS%TYPE,
       l_pst_key_id IN POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE,
       l_pst_key_name IN PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_KEY_NAME%TYPE)
     RETURN REF_CURSOR;
END; -- Package spec
/


CREATE OR REPLACE PACKAGE BODY PSS.PKG_REPORTS IS

    FUNCTION GET_RECONCILIATION_SUMMARY
     ( l_location_id IN POS.LOCATION_ID%TYPE,
       l_start_ts IN TRAN.TRAN_START_TS%TYPE,
       l_end_ts IN TRAN.TRAN_START_TS%TYPE)
     RETURN REF_CURSOR
    IS
        l_cur REF_CURSOR;
    BEGIN
        OPEN l_cur FOR
            SELECT
                TR.CAMPUS_ID,
                TR.CAMPUS_NAME,
                TR.PAYMENT_SUBTYPE_KEY_NAME,
                TR.PAYMENT_SUBTYPE_KEY_ID,
                GET_AUTH_SERVICE_TYPE_ID(TR.PAYMENT_SUBTYPE_KEY_NAME, TR.PAYMENT_SUBTYPE_KEY_ID) AUTH_SERVICE_TYPE_ID,
    			GET_AUTH_NAME(TR.PAYMENT_SUBTYPE_KEY_NAME, TR.PAYMENT_SUBTYPE_KEY_ID) AUTH_NAME,
                SUM(TLI.TRAN_LINE_ITEM_AMOUNT * TLI.TRAN_LINE_ITEM_QUANTITY) TOT_AMOUNT,
                SUM(TLI.TRAN_LINE_ITEM_QUANTITY) TOT_CYCLES,
                SUM(DECODE(TLI.TRAN_LINE_ITEM_TYPE_ID, 2, TLI.TRAN_LINE_ITEM_QUANTITY, 0)) TOT_DETERG,
                SUM(DECODE(TLI.TRAN_LINE_ITEM_TYPE_ID, 3, TLI.TRAN_LINE_ITEM_QUANTITY, 0)) TOT_FABRIC_SOFT,
                SUM(CASE WHEN TLI.TRAN_LINE_ITEM_TYPE_ID BETWEEN 1 AND 9 THEN TLI.TRAN_LINE_ITEM_QUANTITY ELSE 0 END) TOT_WASH_CYCLES,
                SUM(CASE WHEN TLI.TRAN_LINE_ITEM_TYPE_ID BETWEEN 40 AND 42 THEN TLI.TRAN_LINE_ITEM_QUANTITY ELSE 0 END) TOT_DRY_CYCLES,
                SUM(CASE WHEN TLI.TRAN_LINE_ITEM_TYPE_ID BETWEEN 1 AND 9 THEN TLI.TRAN_LINE_ITEM_AMOUNT * TLI.TRAN_LINE_ITEM_QUANTITY ELSE 0 END) TOT_WASH_AMOUNT,
                SUM(CASE WHEN TLI.TRAN_LINE_ITEM_TYPE_ID BETWEEN 40 AND 42 THEN TLI.TRAN_LINE_ITEM_AMOUNT * TLI.TRAN_LINE_ITEM_QUANTITY ELSE 0 END) TOT_DRY_AMOUNT
               FROM TRAN_LINE_ITEM TLI,
                    MV_ESUDS_SETTLED_TRAN TR
              WHERE TLI.TRAN_ID = TR.TRAN_ID
                AND TLI.TRAN_LINE_ITEM_BATCH_TYPE_CD = (CASE
                    WHEN TR.TRAN_STATE_CD IN('9','8','I','E','F','T') THEN 'I'
                    WHEN TR.TRAN_STATE_CD IN('D', 'C') AND NOT EXISTS(
                        SELECT 1 FROM REFUND RF, REFUND_SETTLEMENT_BATCH RSB, SETTLEMENT_BATCH SB
                        WHERE TR.TRAN_ID = RF.TRAN_ID
                        AND RF.REFUND_STATE_ID = 1
                        AND RF.REFUND_ID = RSB.REFUND_ID
                        AND RSB.SETTLEMENT_BATCH_ID = SB.SETTLEMENT_BATCH_ID
                        AND SB.SETTLEMENT_BATCH_END_TS NOT BETWEEN l_start_ts
                          AND l_end_ts) THEN 'A'
                    ELSE 'I'
                   END)
                AND TR.SCHOOL_ID = l_location_id
                AND TR.SETTLEMENT_BATCH_TS BETWEEN l_start_ts AND l_end_ts
           GROUP BY TR.CAMPUS_ID,
                    TR.CAMPUS_NAME,
                    TR.PAYMENT_SUBTYPE_KEY_NAME,
                    TR.PAYMENT_SUBTYPE_KEY_ID
           ORDER BY UPPER(TR.CAMPUS_NAME),
                    UPPER(GET_AUTH_NAME(TR.PAYMENT_SUBTYPE_KEY_NAME, TR.PAYMENT_SUBTYPE_KEY_ID));

        RETURN l_cur;
    END;

    FUNCTION GET_RECONCILIATION_DETAIL
     ( l_location_id IN POS.LOCATION_ID%TYPE,
       l_start_ts IN TRAN.TRAN_START_TS%TYPE,
       l_end_ts IN TRAN.TRAN_START_TS%TYPE,
       l_pst_key_id IN POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE,
       l_pst_key_name IN PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_KEY_NAME%TYPE)
     RETURN REF_CURSOR
    IS
        l_cur REF_CURSOR;
    BEGIN
        OPEN l_cur FOR
            SELECT
                TR.CAMPUS_ID,
                TR.CAMPUS_NAME,
                TR.DORM_ID,
                TR.DORM_NAME,
                TR.ROOM_ID,
                TR.ROOM_NAME,
                GET_AUTH_SERVICE_TYPE_ID(TR.PAYMENT_SUBTYPE_KEY_NAME, TR.PAYMENT_SUBTYPE_KEY_ID) AUTH_SERVICE_TYPE_ID,
    			GET_AUTH_NAME(TR.PAYMENT_SUBTYPE_KEY_NAME, TR.PAYMENT_SUBTYPE_KEY_ID) AUTH_NAME,
                TR.TRAN_SETTLEMENT_AMOUNT INITIAL_AMOUNT,
                -RSB.REFUND_SETTLEMENT_B_AMT REFUND_AMOUNT,
                TR.TRAN_SETTLEMENT_AMOUNT + NVL(-RSB.REFUND_SETTLEMENT_B_AMT, 0) TOT_AMOUNT,
                TR.TRAN_START_TS,
                TR.SETTLEMENT_BATCH_TS SETTLED_TS,
                RF.REFUND_DESC TRAN_REFUND_DESC,
                SB.SETTLEMENT_BATCH_END_TS TRAN_REFUND_SETTLEMENT_TS,
                SF_PIVOT_TRAN_LINE_ITEM (TR.TRAN_ID) TRAN_LINE_ITEM_DESC,
                NVL(CA.CONSUMER_ACCT_CD, T.TRAN_PARSED_ACCT_NUM) CONSUMER_ACCT_CD,
                DECODE(CA.CONSUMER_ACCT_ID, NULL, 'Y', 'N') CONSUMER_MISSING
               FROM MV_ESUDS_SETTLED_TRAN TR,
                    CONSUMER_ACCT CA,
                    TRAN T,
                    REFUND RF,
                    REFUND_SETTLEMENT_BATCH RSB,
                    SETTLEMENT_BATCH SB
              WHERE TR.TRAN_ID = RF.TRAN_ID (+)
                AND TR.CAMPUS_ID = l_location_id
                AND TR.CONSUMER_ACCT_ID = CA.CONSUMER_ACCT_ID (+)
                AND NVL(TR.PAYMENT_SUBTYPE_KEY_ID, 0) = l_pst_key_id
                AND TR.PAYMENT_SUBTYPE_KEY_NAME = l_pst_key_name
                AND TR.SETTLEMENT_BATCH_TS BETWEEN l_start_ts AND l_end_ts
                AND TR.TRAN_ROWID = T.ROWID
                AND RF.REFUND_ID = RSB.REFUND_ID (+)
                AND RSB.SETTLEMENT_BATCH_ID = SB.SETTLEMENT_BATCH_ID (+)
                AND RF.REFUND_STATE_ID (+) = 1
                AND SB.SETTLEMENT_BATCH_END_TS (+) BETWEEN l_start_ts AND l_end_ts
           ORDER BY TR.SETTLEMENT_BATCH_TS,
                    UPPER(TR.DORM_NAME),
                    UPPER(TR.ROOM_NAME),
                    CA.CONSUMER_ACCT_CD;
        RETURN l_cur;
    END;

   -- Enter further code below as specified in the Package spec.
END;
/



CREATE OR REPLACE VIEW pss.vw_tran_summary
(
    tran_id
  , tran_start_ts
  , consumer_id
  , consumer_acct_id
  , client_payment_type_cd
  , authority_service_type_id
  , tran_settlement_amount
  , tran_line_item_desc
  , customer_id
)
AS
SELECT t.tran_id,
          t.tran_start_ts,
          ca.consumer_id,
          t.consumer_acct_id,
          ps.client_payment_type_cd,
          GET_AUTH_SERVICE_TYPE_ID(ps.payment_subtype_key_name, pta.payment_subtype_key_id) authority_service_type_id,
          SUM(tli.tran_line_item_amount * tli.tran_line_item_quantity) AS tran_settlement_amount,
          sf_pivot_tran_line_item (t.tran_id),
          pos.customer_id
     FROM tran_line_item tli, tran t, pos_pta pta, payment_subtype ps, consumer_acct ca, pos
    WHERE t.tran_id = tli.tran_id
      AND t.pos_pta_id = pta.pos_pta_id
      AND pta.payment_subtype_id = ps.payment_subtype_id
      and t.consumer_acct_id = ca.consumer_acct_id
      and pta.pos_id = pos.pos_id
      and tli.tran_line_item_batch_type_cd = 'A'
      GROUP BY t.tran_id,
         t.tran_start_ts,
         ca.consumer_id,
         t.consumer_acct_id,
         ps.client_payment_type_cd,
         ps.payment_subtype_key_name,
         pta.payment_subtype_key_id,
         sf_pivot_tran_line_item (t.tran_id),
         pos.customer_id
/


CREATE OR REPLACE VIEW pss.vw_tran_line_item_dtl
(
    tran_id
  , tran_line_item_id
  , tran_start_ts
  , tran_line_item_desc
  , host_type_id
  , host_port_num
  , host_label_cd
  , location_id
  , customer_id
  , consumer_acct_id
  , tran_line_item_quantity
  , tran_line_item_amount
  , tran_line_item_tot_amount
)
AS
SELECT tr.tran_id,
       tli.tran_line_item_id,
       TR.TRAN_START_TS,
       tli.tran_line_item_desc,
       HOST.host_type_id,
       HOST.host_port_num,
       HOST.host_label_cd,
       pos.location_id,
       pos.customer_id,
       tr.consumer_acct_id,
       tli.tran_line_item_quantity,
       nvl(tli.tran_line_item_amount, 0) tran_line_item_amount,
              tli.tran_line_item_quantity*
       nvl(tli.tran_line_item_amount, 0) tran_line_item_tot_amount
  FROM tran tr,
       tran_line_item tli,
       pos,
       pos_pta pta,
       device dv,
       HOST,
       location dorm
 WHERE tr.tran_id = tli.tran_id
        and tr.pos_pta_id = pta.pos_pta_id
        AND pta.pos_id = pos.pos_id
   AND pos.device_id = dv.device_id
   AND HOST.device_id = dv.device_id
   and pos.location_id = dorm.location_id
   and tli.tran_line_item_batch_type_cd = 'A'
/


CREATE OR REPLACE VIEW pss.vw_total_sales_by_host
(
    laundry_room_id
  , laundry_room_name
  , dorm_id
  , dorm_name
  , campus_id
  , campus_name
  , school_id
  , school_name
  , customer_id
  , device_id
  , device_name
  , host_id
  , host_label_cd
  , host_type_id
  , host_type_desc
  , payment_subtype_id
  , payment_subtype_desc
  , client_payment_type_cd
  , client_payment_type_desc
  , tran_ts
  , tot_items
  , tot_amount
  , tot_cycles
  , tot_cycle_amount
  , tot_top_offs
  , tot_top_off_amount
  , tot_misc_items
  , tot_misc_item_amount
  , tot_reg_washes
  , tot_reg_wash_amount
  , tot_reg_cold_washes
  , tot_reg_cold_wash_amount
  , tot_warm_washes
  , tot_warm_wash_amount
  , tot_reg_hot_washes
  , tot_reg_hot_wash_amount
  , tot_spec_washes
  , tot_spec_wash_amount
  , tot_spec_cold_washes
  , tot_spec_cold_wash_amount
  , spec_warm_washes
  , spec_warm_wash_amount
  , spec_hot_washes
  , spec_hot_wash_amount
  , super_washes
  , super_wash_amount
  , reg_dries
  , reg_dry_amount
  , spec_dries
  , spec_dry_amount
  , top_off_dries
  , top_off_dry_amount
  , tot_unused_purch_adjs
  , tot_unused_purch_adj_amount
  , tot_leftover_credit_adjs
  , tot_leftover_credit_adj_amount
  , authority_service_type_id
)
AS
SELECT   loc4.location_id,
            loc4.location_name,
            loc3.location_id,
            loc3.location_name,
            loc2.location_id,
            loc2.location_name,
            loc1.location_id,
            loc1.location_name,
            ps.customer_id,
            dev.device_id,
            dev.device_name,
            hs.host_id,
            hs.host_label_cd,
            ht.host_type_id,
            ht.host_type_desc,
            pst.payment_subtype_id,
            pst.payment_subtype_name, -- added by BSK 11-05-04 because web app object is mapped to it
            pst.client_payment_type_cd,
            cpt.client_payment_type_desc,
            TRUNC (tr.tran_start_ts, 'DD') tran_ts,
            -- total tran line items per host
            SUM (tran_line_item_quantity) tot_items,
            SUM (NVL (tli.tran_line_item_quantity, 0) * NVL (tli.tran_line_item_amount, 0))
                                                                               tot_amount,
            -- total cycles
            SUM (DECODE (tran_line_item_type_id,
                         1, tran_line_item_quantity,
                         2, tran_line_item_quantity,
                         3, tran_line_item_quantity,
                         4, tran_line_item_quantity,
                         5, tran_line_item_quantity,
                         6, tran_line_item_quantity,
                         7, tran_line_item_quantity,
                         8, tran_line_item_quantity,
                         40, tran_line_item_quantity,
                         41, tran_line_item_quantity,
                         0
                        )
                ) tot_cycles,
            SUM (DECODE (tran_line_item_type_id,
                         1, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         2, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         3, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         4, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         5, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         6, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         7, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         8, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         40, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         41, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) tot_cycle_amount,
            -- total top offs
            SUM (DECODE (tran_line_item_type_id,
                         9, tran_line_item_quantity,
                         42, tran_line_item_quantity,
                         0
                        )
                ) tot_top_offs,
            SUM (DECODE (tran_line_item_type_id,
                         9, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         42, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) tot_top_off_amount,
            -- total misc items
            SUM (DECODE (tran_line_item_type_id, 0, tran_line_item_quantity, 0))
                                                                           tot_misc_items,
            SUM (DECODE (tran_line_item_type_id,
                         0, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) tot_misc_item_amount,
            -- totals by item type
            SUM (DECODE (tran_line_item_type_id, 1, tran_line_item_quantity, 0))
                                                                           tot_reg_washes,
            SUM (DECODE (tran_line_item_type_id,
                         1, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) tot_reg_wash_amount,
            SUM (DECODE (tran_line_item_type_id, 2, tran_line_item_quantity, 0))
                                                                      tot_reg_cold_washes,
            SUM (DECODE (tran_line_item_type_id,
                         2, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) tot_reg_cold_wash_amount,
            SUM (DECODE (tran_line_item_type_id, 3, tran_line_item_quantity, 0))
                                                                          tot_warm_washes,
            SUM (DECODE (tran_line_item_type_id,
                         3, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) tot_warm_wash_amount,
            SUM (DECODE (tran_line_item_type_id, 4, tran_line_item_quantity, 0))
                                                                       tot_reg_hot_washes,
            SUM (DECODE (tran_line_item_type_id,
                         4, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) tot_reg_hot_wash_amount,
            SUM (DECODE (tran_line_item_type_id, 5, tran_line_item_quantity, 0))
                                                                          tot_spec_washes,
            SUM (DECODE (tran_line_item_type_id,
                         5, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) tot_spec_wash_amount,
            SUM (DECODE (tran_line_item_type_id, 6, tran_line_item_quantity, 0))
                                                                     tot_spec_cold_washes,
            SUM (DECODE (tran_line_item_type_id,
                         6, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) tot_spec_cold_wash_amount,
            SUM (DECODE (tran_line_item_type_id, 7, tran_line_item_quantity, 0))
                                                                         spec_warm_washes,
            SUM (DECODE (tran_line_item_type_id,
                         7, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) spec_warm_wash_amount,
            SUM (DECODE (tran_line_item_type_id, 8, tran_line_item_quantity, 0))
                                                                          spec_hot_washes,
            SUM (DECODE (tran_line_item_type_id,
                         8, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) spec_hot_wash_amount,
            SUM (DECODE (tran_line_item_type_id, 9, tran_line_item_quantity, 0))
                                                                             super_washes,
            SUM (DECODE (tran_line_item_type_id,
                         9, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) super_wash_amount,
            SUM (DECODE (tran_line_item_type_id, 40, tran_line_item_quantity, 0))
                                                                                reg_dries,
            SUM (DECODE (tran_line_item_type_id,
                         40, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) reg_dry_amount,
            SUM (DECODE (tran_line_item_type_id, 41, tran_line_item_quantity, 0))
                                                                               spec_dries,
            SUM (DECODE (tran_line_item_type_id,
                         41, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) spec_dry_amount,
            SUM (DECODE (tran_line_item_type_id, 42, tran_line_item_quantity, 0))
                                                                            top_off_dries,
            SUM (DECODE (tran_line_item_type_id,
                         42, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) top_off_dry_amount,
            SUM (DECODE (tran_line_item_type_id, 80, tran_line_item_quantity, 0))
                                                                    tot_unused_purch_adjs,
            SUM (DECODE (tran_line_item_type_id,
                         80, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) tot_unused_purch_adj_amount,
            SUM (DECODE (tran_line_item_type_id, 81, tran_line_item_quantity, 0))
                                                                 tot_leftover_credit_adjs,
            SUM (DECODE (tran_line_item_type_id,
                         81, NVL (tli.tran_line_item_quantity, 0)
                          * NVL (tli.tran_line_item_amount, 0),
                         0
                        )
                ) tot_leftover_credit_adj_amount,
                GET_AUTH_SERVICE_TYPE_ID(pst.payment_subtype_key_name, pta.payment_subtype_key_id) authority_service_type_id
       FROM tran_line_item tli,
            tran tr,
            pos ps,
            pos_pta pta,
            LOCATION loc4,
            LOCATION loc3,
            LOCATION loc2,
            LOCATION loc1,
            device dev,
            HOST hs,
            host_type ht,
            payment_subtype pst,
            client_payment_type cpt
      WHERE tli.tran_id = tr.tran_id
        AND tr.pos_pta_id = pta.pos_pta_id
        AND pta.pos_id = ps.pos_id
        AND cpt.client_payment_type_cd = pst.client_payment_type_cd
        AND ps.location_id = loc4.location_id
        AND loc4.location_type_id = '18'
        AND loc4.parent_location_id = loc3.location_id
        AND loc3.parent_location_id = loc2.location_id
        AND loc2.parent_location_id = loc1.location_id
        AND ps.device_id = dev.device_id
        AND dev.device_id = hs.device_id
        AND hs.host_type_id = ht.host_type_id
        AND hs.host_id = tli.host_id
        and tli.tran_line_item_type_id not in (80, 81)
        and tli.tran_line_item_batch_type_cd = 'A'
        AND pta.payment_subtype_id = pst.payment_subtype_id
   GROUP BY loc4.location_id,
            loc4.location_name,
            loc3.location_id,
            loc3.location_name,
            loc2.location_id,
            loc2.location_name,
            loc1.location_id,
            loc1.location_name,
            ps.customer_id,
            dev.device_id,
            dev.device_name,
            hs.host_id,
            hs.host_label_cd,
            ht.host_type_id,
            ht.host_type_desc,
            pst.payment_subtype_id,
            pst.payment_subtype_name,
            pst.client_payment_type_cd,
            cpt.client_payment_type_desc,
            TRUNC (tr.tran_start_ts, 'DD'),
            pst.payment_subtype_key_name,
            pta.payment_subtype_key_id
/


CREATE OR REPLACE VIEW pss.vw_tot_sales_by_school
(
    school_id
  , school_name
  , operator_id
  , tran_ts
  , tot_amount
  , tot_cycles
  , tot_deterg
  , tot_fabric_soft
  , tot_wash_cycles
  , tot_dry_cycles
  , tot_wash_amount
  , tot_dry_amount
  , authority_service_type_id
)
AS
SELECT      loc1.location_id campus_id,
            loc1.location_name campus_name,
            ps.customer_id operator_id,
            TRUNC(tr.tran_start_ts, 'DD') tran_ts,
            SUM(tli.tran_line_item_amount * tli.tran_line_item_quantity) tot_amount,
            SUM(tli.tran_line_item_quantity) tot_cycles,
            SUM(DECODE(tli.tran_line_item_type_id, 2, tli.tran_line_item_quantity, 0)) tot_deterg,
            SUM(DECODE(tli.tran_line_item_type_id, 3, tli.tran_line_item_quantity, 0)) tot_fabric_soft,
            SUM(CASE WHEN tli.tran_line_item_type_id BETWEEN 1 AND 9 THEN tli.tran_line_item_quantity ELSE 0 END) tot_wash_cycle,
            SUM(CASE WHEN tli.tran_line_item_type_id BETWEEN 40 AND 42 THEN tli.tran_line_item_quantity ELSE 0 END) tot_dry_cycle,
            SUM(CASE WHEN tli.tran_line_item_type_id BETWEEN 1 AND 9 THEN tli.tran_line_item_amount * tli.tran_line_item_quantity ELSE 0 END) tot_wash_amount,
            SUM(CASE WHEN tli.tran_line_item_type_id BETWEEN 40 AND 42 THEN tli.tran_line_item_amount * tli.tran_line_item_quantity ELSE 0 END) tot_dry_amount,
            GET_AUTH_SERVICE_TYPE_ID(pst.payment_subtype_key_name, pta.payment_subtype_key_id) authority_service_type_id
       FROM tran_line_item tli,
            tran tr,
            pos ps,
            pos_pta pta,
            LOCATION loc1,
            LOCATION loc2,
            LOCATION loc3,
            LOCATION loc4,
            PAYMENT_SUBTYPE pst
      WHERE tli.tran_id = tr.tran_id
        AND tr.pos_pta_id = pta.pos_pta_id
        AND pta.pos_id = ps.pos_id
        AND ps.location_id = loc4.location_id
        AND loc4.location_type_id = 18
        AND loc4.parent_location_id = loc3.location_id
        AND loc3.parent_location_id = loc2.location_id
        AND loc2.parent_location_id = loc1.location_id
        and tli.tran_line_item_batch_type_cd = 'A'
        and pta.payment_subtype_id = pst.payment_subtype_id
   GROUP BY loc1.location_id,
            loc1.location_name,
            ps.customer_id,
            TRUNC(tr.tran_start_ts, 'DD'),
            pst.payment_subtype_key_name,
            pta.payment_subtype_key_id
/


CREATE OR REPLACE VIEW pss.vw_tot_sales_by_laundry_room
(
    laundry_room_id
  , laundry_room_name
  , dorm_id
  , operator_id
  , tran_ts
  , tot_amount
  , tot_cycles
  , tot_deterg
  , tot_fabric_soft
  , tot_wash_cycles
  , tot_dry_cycles
  , tot_wash_amount
  , tot_dry_amount
  , authority_service_type_id
)
AS
SELECT   loc4.location_id campus_id,
            loc4.location_name campus_name,
            loc4.parent_location_id school_id,
            ps.customer_id,
            TRUNC(tr.tran_start_ts, 'DD') tran_ts,
            SUM(tli.tran_line_item_amount * tli.tran_line_item_quantity) tot_amount,
            SUM(tli.tran_line_item_quantity) tot_cycles,
            SUM(DECODE(tli.tran_line_item_type_id, 2, tli.tran_line_item_quantity, 0)) tot_deterg,
            SUM(DECODE(tli.tran_line_item_type_id, 3, tli.tran_line_item_quantity, 0)) tot_fabric_soft,
            SUM(CASE WHEN tli.tran_line_item_type_id BETWEEN 1 AND 9 THEN tli.tran_line_item_quantity ELSE 0 END) tot_wash_cycle,
            SUM(CASE WHEN tli.tran_line_item_type_id BETWEEN 40 AND 42 THEN tli.tran_line_item_quantity ELSE 0 END) tot_dry_cycle,
            SUM(CASE WHEN tli.tran_line_item_type_id BETWEEN 1 AND 9 THEN tli.tran_line_item_amount * tli.tran_line_item_quantity ELSE 0 END) tot_wash_amount,
            SUM(CASE WHEN tli.tran_line_item_type_id BETWEEN 40 AND 42 THEN tli.tran_line_item_amount * tli.tran_line_item_quantity ELSE 0 END) tot_dry_amount,
            GET_AUTH_SERVICE_TYPE_ID(pst.payment_subtype_key_name, pta.payment_subtype_key_id) authority_service_type_id
       FROM tran_line_item tli, tran tr, pos ps, pos_pta pta, LOCATION loc4, PAYMENT_SUBTYPE pst
      WHERE tli.tran_id = tr.tran_id
        AND tr.pos_pta_id = pta.pos_pta_id
        AND pta.pos_id = ps.pos_id
        AND ps.location_id = loc4.location_id
        AND loc4.location_type_id = 18
        and tli.tran_line_item_batch_type_cd = 'A'
        AND pta.payment_subtype_id = pst.payment_subtype_id
   GROUP BY loc4.location_id,
            loc4.location_name,
            loc4.parent_location_id,
            ps.customer_id,
            ps.customer_id,
            TRUNC(tr.tran_start_ts, 'DD'),
            pst.payment_subtype_key_name,
            pta.payment_subtype_key_id
/


CREATE OR REPLACE VIEW pss.vw_tot_sales_by_dorm
(
    dorm_id
  , dorm_name
  , campus_id
  , operator_id
  , tran_ts
  , tot_amount
  , tot_cycles
  , tot_deterg
  , tot_fabric_soft
  , tot_wash_cycles
  , tot_dry_cycles
  , tot_wash_amount
  , tot_dry_amount
  , authority_service_type_id
)
AS
SELECT   loc3.location_id campus_id,
            loc3.location_name campus_name,
            loc3.parent_location_id school_id,
            ps.customer_id,
            TRUNC(tr.tran_start_ts, 'DD') tran_ts,
            SUM(tli.tran_line_item_amount * tli.tran_line_item_quantity) tot_amount,
            SUM(tli.tran_line_item_quantity) tot_cycles,
            SUM(DECODE(tli.tran_line_item_type_id, 2, tli.tran_line_item_quantity, 0)) tot_deterg,
            SUM(DECODE(tli.tran_line_item_type_id, 3, tli.tran_line_item_quantity, 0)) tot_fabric_soft,
            SUM(CASE WHEN tli.tran_line_item_type_id BETWEEN 1 AND 9 THEN tli.tran_line_item_quantity ELSE 0 END) tot_wash_cycle,
            SUM(CASE WHEN tli.tran_line_item_type_id BETWEEN 40 AND 42 THEN tli.tran_line_item_quantity ELSE 0 END) tot_dry_cycle,
            SUM(CASE WHEN tli.tran_line_item_type_id BETWEEN 1 AND 9 THEN tli.tran_line_item_amount * tli.tran_line_item_quantity ELSE 0 END) tot_wash_amount,
            SUM(CASE WHEN tli.tran_line_item_type_id BETWEEN 40 AND 42 THEN tli.tran_line_item_amount * tli.tran_line_item_quantity ELSE 0 END) tot_dry_amount,
            GET_AUTH_SERVICE_TYPE_ID(pst.payment_subtype_key_name, pta.payment_subtype_key_id) authority_service_type_id
       FROM tran_line_item tli,
            tran tr,
            pos ps,
            pos_pta pta,
            LOCATION loc3,
            LOCATION loc4,
            PAYMENT_SUBTYPE pst
      WHERE tli.tran_id = tr.tran_id
        AND tr.pos_pta_id = pta.pos_pta_id
        AND pta.pos_id = ps.pos_id
        AND ps.location_id = loc4.location_id
        AND loc4.location_type_id = 18
        AND loc4.parent_location_id = loc3.location_id
        and tli.tran_line_item_batch_type_cd = 'A'
        and pta.payment_subtype_id = pst.payment_subtype_id
   GROUP BY loc3.location_id,
            loc3.location_name,
            loc3.parent_location_id,
            ps.customer_id,
            TRUNC(tr.tran_start_ts, 'DD'),
            pst.payment_subtype_key_name,
            pta.payment_subtype_key_id
/


CREATE OR REPLACE VIEW pss.vw_tot_sales_by_campus
(
    campus_id
  , campus_name
  , school_id
  , operator_id
  , tran_ts
  , tot_amount
  , tot_cycles
  , tot_deterg
  , tot_fabric_soft
  , tot_wash_cycles
  , tot_dry_cycles
  , tot_wash_amount
  , tot_dry_amount
  , authority_service_type_id
)
AS
SELECT      loc2.location_id campus_id,
            loc2.location_name campus_name,
            loc2.parent_location_id school_id,
            ps.customer_id operator_id,
            TRUNC(tr.tran_start_ts, 'DD') tran_ts,
            SUM(tli.tran_line_item_amount * tli.tran_line_item_quantity) tot_amount,
            SUM(tli.tran_line_item_quantity) tot_cycles,
            SUM(DECODE(tli.tran_line_item_type_id, 2, tli.tran_line_item_quantity, 0)) tot_deterg,
            SUM(DECODE(tli.tran_line_item_type_id, 3, tli.tran_line_item_quantity, 0)) tot_fabric_soft,
            SUM(CASE WHEN tli.tran_line_item_type_id BETWEEN 1 AND 9 THEN tli.tran_line_item_quantity ELSE 0 END) tot_wash_cycle,
            SUM(CASE WHEN tli.tran_line_item_type_id BETWEEN 40 AND 42 THEN tli.tran_line_item_quantity ELSE 0 END) tot_dry_cycle,
            SUM(CASE WHEN tli.tran_line_item_type_id BETWEEN 1 AND 9 THEN tli.tran_line_item_amount * tli.tran_line_item_quantity ELSE 0 END) tot_wash_amount,
            SUM(CASE WHEN tli.tran_line_item_type_id BETWEEN 40 AND 42 THEN tli.tran_line_item_amount * tli.tran_line_item_quantity ELSE 0 END) tot_dry_amount,
            GET_AUTH_SERVICE_TYPE_ID(pst.payment_subtype_key_name, pta.payment_subtype_key_id) authority_service_type_id
       FROM tran_line_item tli,
            tran tr,
            pos ps,
            pos_pta pta,
            LOCATION loc1,
            LOCATION loc2,
            LOCATION loc3,
            LOCATION loc4,
            PAYMENT_SUBTYPE pst
      WHERE tli.tran_id = tr.tran_id
        AND tr.pos_pta_id = pta.pos_pta_id
        AND pta.pos_id = ps.pos_id
        AND ps.location_id = loc4.location_id
        AND loc4.location_type_id = 18
        AND loc4.parent_location_id = loc3.location_id
        AND loc3.parent_location_id = loc2.location_id
        AND loc2.parent_location_id = loc1.location_id
        and tli.tran_line_item_batch_type_cd = 'A'
        and pta.payment_subtype_id = pst.payment_subtype_id
   GROUP BY loc2.location_id,
            loc2.location_name,
            loc2.parent_location_id,
            ps.customer_id,
            TRUNC(tr.tran_start_ts, 'DD'),
            pst.payment_subtype_key_name,
            pta.payment_subtype_key_id
/


CREATE OR REPLACE VIEW pss.vw_tli_pivot_view
(
    tran_id
  , tran_start_ts
  , consumer_id
  , consumer_acct_id
  , customer_id
  , client_payment_type_cd
  , tran_settlement_amount
  , tran_line_item_desc
)
AS
SELECT t.tran_id,
          t.tran_start_ts,
          ca.consumer_id,
          t.consumer_acct_id,
          ps.client_payment_type_cd,
          SUM(tli.tran_line_item_amount * tli.tran_line_item_quantity) AS tran_settlement_amount,
          sf_pivot_tran_line_item (t.tran_id),
          pos.customer_id
     FROM tran_line_item tli, tran t, pos_pta pta, payment_subtype ps, consumer_acct ca, pos
    WHERE t.tran_id = tli.tran_id
      AND t.pos_pta_id = pta.pos_pta_id
      AND pta.payment_subtype_id = ps.payment_subtype_id
      and t.consumer_acct_id = ca.consumer_acct_id
      and pta.pos_id = pos.pos_id
      and tli.tran_line_item_batch_type_cd = 'A'
      GROUP BY t.tran_id,
         t.tran_start_ts,
         ca.consumer_id,
         t.consumer_acct_id,
         ps.client_payment_type_cd,
         sf_pivot_tran_line_item (t.tran_id),
         pos.customer_id
/


CREATE OR REPLACE VIEW pss.vw_student_account
(
    student_email_addr
  , student_account_cd
  , student_account_balance
  , student_fname
  , student_lname
  , student_addr1
  , student_addr2
  , student_city
  , student_state_cd
  , student_postal_cd
  , location_id
)
AS
SELECT c.consumer_email_addr1, ca.consumer_acct_cd,
ca.consumer_acct_balance, c.consumer_fname, c.consumer_lname, c.consumer_addr1,
c.consumer_addr2, c.consumer_city, c.consumer_state_cd, c.consumer_postal_cd, l.ancestor_location_id
FROM CONSUMER C, CONSUMER_ACCT CA, VW_LOCATION_HIERARCHY l
WHERE c.consumer_id = ca.consumer_id AND ca.location_id = l.descendent_location_id
AND ca.consumer_acct_active_yn_flag = 'Y'
/


CREATE OR REPLACE VIEW pss.vw_location_by_device_id
(
    location_id
  , location_name
  , device_id
  , device_serial_cd
  , location_timezone
)
AS
select a.location_id, a.location_name, b.device_id, c.device_serial_cd, a.location_time_zone_cd
from location.location a, pss.pos b, device.device c
where a.location_id = b.location_id
and c.device_id = b.device_id
/


CREATE OR REPLACE VIEW pss.vw_esuds_tran_line_item_dtl
(
    tran_id
  , tran_line_item_id
  , tran_start_ts
  , tran_line_item_desc
  , host_type_id
  , host_port_num
  , host_label_cd
  , customer_id
  , consumer_acct_id
  , tran_line_item_quantity
  , tran_line_item_amount
  , tran_line_item_tot_amount
  , campus_id
  , campus_name
  , dorm_id
  , dorm_name
  , room_id
  , room_name
  , client_payment_type_cd
  , authority_service_type_id
)
AS
(/* Formatted on 2004/08/31 10:09 (Formatter Plus v4.8.0) */
SELECT tr.tran_id,
       tli.tran_line_item_id,
       tr.tran_start_ts,
       tli.tran_line_item_desc,
       HOST.host_type_id,
       HOST.host_port_num,
       HOST.host_label_cd,
       pos.customer_id,
       tr.consumer_acct_id,
       tli.tran_line_item_quantity,
       NVL (tli.tran_line_item_amount, 0) tran_line_item_amount,
       tli.tran_line_item_quantity * NVL (tli.tran_line_item_amount, 0)
                                                                tran_line_item_tot_amount,
       campus.location_id campus_id,
       campus.location_name campus_name,
       dorm.location_id dorm_id,
       dorm.location_name dorm_name,
       room.location_id location_id,
       room.location_name room_name,
       pt.CLIENT_PAYMENT_TYPE_CD,
       GET_AUTH_SERVICE_TYPE_ID(pt.payment_subtype_key_name, pta.payment_subtype_key_id) authority_service_type_id
  FROM tran tr,
       tran_line_item tli,
       pos,
       device dv,
       HOST,
       LOCATION.LOCATION dorm,
       LOCATION.LOCATION room,
       LOCATION.LOCATION campus,
       payment_subtype pt,
       pos_pta pta
 WHERE tr.tran_id = tli.tran_id
   AND tr.pos_pta_id = pta.pos_pta_id
   AND pta.pos_id = pos.pos_id
   AND pos.device_id = dv.device_id
   AND HOST.device_id = dv.device_id
   AND tli.host_id = HOST.host_id
   AND pos.location_id = room.location_id
   AND room.parent_location_id = dorm.location_id
   AND dorm.parent_location_id = campus.location_id
   AND pta.payment_subtype_id = pt.payment_subtype_id
   and tli.tran_line_item_batch_type_cd = 'A'
)
/


