GRANT ALL ON DEVICE.HOST TO PSS;
GRANT ALL ON DEVICE.HOST_TYPE TO PSS;
GRANT SELECT, REFERENCES ON AUTHORITY.AUTHORITY_SERVICE_TYPE TO PSS;
GRANT SELECT, REFERENCES ON AUTHORITY.AUTHORITY TO PSS;

GRANT SELECT, REFERENCES ON AUTHORITY.AUTHORITY_ASSN TO PSS;

DROP MATERIALIZED VIEW PSS.MV_ESUDS_SETTLED_TRAN;
DROP MATERIALIZED VIEW LOG ON PSS.TRAN;
DROP MATERIALIZED VIEW LOG ON PSS.SETTLEMENT_BATCH;
DROP MATERIALIZED VIEW LOG ON PSS.TRAN_SETTLEMENT_BATCH;


CREATE SEQUENCE PSS.SEQ_AUTH_ID
    INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH 1;
    
CREATE PUBLIC SYNONYM SEQ_AUTH_ID FOR PSS.SEQ_AUTH_ID;

GRANT SELECT ON PSS.SEQ_AUTH_ID TO WEB_USER;


CREATE SEQUENCE PSS.SEQ_MERCHANT_BANK_ACCT_ID
    INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH 1;

CREATE PUBLIC SYNONYM SEQ_MERCHANT_BANK_ACCT_ID FOR PSS.SEQ_MERCHANT_BANK_ACCT_ID;

GRANT SELECT ON PSS.SEQ_MERCHANT_BANK_ACCT_ID TO WEB_USER;


CREATE SEQUENCE PSS.SEQ_MERCHANT_ID
    INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH 1;
    
CREATE PUBLIC SYNONYM SEQ_MERCHANT_ID FOR PSS.SEQ_MERCHANT_ID;

GRANT SELECT ON PSS.SEQ_MERCHANT_ID TO WEB_USER;


CREATE SEQUENCE PSS.SEQ_REFUND_ID 
    INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH 1;
    
CREATE PUBLIC SYNONYM SEQ_REFUND_ID FOR PSS.SEQ_REFUND_ID;

GRANT SELECT ON PSS.SEQ_REFUND_ID  TO WEB_USER;


CREATE SEQUENCE PSS.SEQ_REFUND_TYPE_ID
    INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH 1;
    
CREATE PUBLIC SYNONYM SEQ_REFUND_TYPE_ID FOR PSS.SEQ_REFUND_TYPE_ID;

GRANT SELECT ON PSS.SEQ_REFUND_TYPE_ID TO WEB_USER;


CREATE SEQUENCE PSS.SEQ_TERMINAL_ID
    INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH 1;
    
CREATE PUBLIC SYNONYM SEQ_TERMINAL_ID FOR PSS.SEQ_TERMINAL_ID;

GRANT SELECT ON PSS.SEQ_TERMINAL_ID TO WEB_USER;


CREATE SEQUENCE PSS.SEQ_BANORTE_AUTHORITY_ID 
    INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH 1;
    
CREATE PUBLIC SYNONYM SEQ_BANORTE_AUTHORITY_ID FOR PSS.SEQ_BANORTE_AUTHORITY_ID;

GRANT SELECT ON PSS.SEQ_BANORTE_AUTHORITY_ID TO WEB_USER;


CREATE SEQUENCE PSS.SEQ_BANORTE_TERMINAL_ID 
    INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH 1;

CREATE PUBLIC SYNONYM SEQ_BANORTE_TERMINAL_ID FOR PSS.SEQ_BANORTE_TERMINAL_ID;

GRANT SELECT ON PSS.SEQ_BANORTE_TERMINAL_ID TO WEB_USER;


CREATE SEQUENCE PSS.SEQ_WEBPOS_AUTHORITY_ID 
    INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH 1;

CREATE PUBLIC SYNONYM SEQ_WEBPOS_AUTHORITY_ID FOR PSS.SEQ_WEBPOS_AUTHORITY_ID;

GRANT SELECT ON PSS.SEQ_WEBPOS_AUTHORITY_ID TO WEB_USER;


CREATE SEQUENCE PSS.SEQ_WEBPOS_TERMINAL_ID 
    INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH 1;

CREATE PUBLIC SYNONYM SEQ_WEBPOS_TERMINAL_ID FOR PSS.SEQ_WEBPOS_TERMINAL_ID;

GRANT SELECT ON PSS.SEQ_WEBPOS_TERMINAL_ID TO WEB_USER;



CREATE TABLE PSS.ACCT_ENTRY_METHOD
  (
    ACCT_ENTRY_METHOD_CD    char(1),
    ACCT_ENTRY_METHOD_DESC  VARCHAR2(60)   not null,
    CONSTRAINT PK_ACCT_ENTRY_METHOD_CD primary key(ACCT_ENTRY_METHOD_CD)
  )
  TABLESPACE PSS_DATA;
 
CREATE PUBLIC SYNONYM ACCT_ENTRY_METHOD FOR PSS.ACCT_ENTRY_METHOD;

GRANT SELECT, INSERT, UPDATE, DELETE ON PSS.ACCT_ENTRY_METHOD TO WEB_USER;
 
INSERT INTO PSS.ACCT_ENTRY_METHOD(ACCT_ENTRY_METHOD_CD, ACCT_ENTRY_METHOD_DESC) VALUES('1', 'UNSPECIFIED');
INSERT INTO PSS.ACCT_ENTRY_METHOD(ACCT_ENTRY_METHOD_CD, ACCT_ENTRY_METHOD_DESC) VALUES('2', 'MANUAL');
INSERT INTO PSS.ACCT_ENTRY_METHOD(ACCT_ENTRY_METHOD_CD, ACCT_ENTRY_METHOD_DESC) VALUES('3', 'MAGNETIC_STRIPE');
INSERT INTO PSS.ACCT_ENTRY_METHOD(ACCT_ENTRY_METHOD_CD, ACCT_ENTRY_METHOD_DESC) VALUES('4', 'ICC_READ');
INSERT INTO PSS.ACCT_ENTRY_METHOD(ACCT_ENTRY_METHOD_CD, ACCT_ENTRY_METHOD_DESC) VALUES('5', 'MAGNETIC_STRIPE_ICC_CAPABLE');


CREATE TABLE PSS.AUTH_TYPE
  (
    AUTH_TYPE_CD    char(1),
    AUTH_TYPE_DESC  VARCHAR2(60)   not null,
    CONSTRAINT PK_AUTH_TYPE_CD primary key(AUTH_TYPE_CD)
  )
  TABLESPACE PSS_DATA;

CREATE PUBLIC SYNONYM AUTH_TYPE FOR PSS.AUTH_TYPE;

GRANT SELECT, INSERT, UPDATE, DELETE ON PSS.AUTH_TYPE TO WEB_USER;

COMMENT ON TABLE PSS.AUTH_TYPE is 'Contains a listing of all the different types of authorizations a transaction can be processed as.  Examples are network (live) authorizations and local authorizations.';

INSERT INTO PSS.AUTH_TYPE(AUTH_TYPE_CD, AUTH_TYPE_DESC) VALUES('N', 'Live auth, pre-settlement amount not known');
INSERT INTO PSS.AUTH_TYPE(AUTH_TYPE_CD, AUTH_TYPE_DESC) VALUES('L', 'Local auth, "virtual" offline sale');
INSERT INTO PSS.AUTH_TYPE(AUTH_TYPE_CD, AUTH_TYPE_DESC) VALUES('U', 'Post-auth sale, final amount known');
INSERT INTO PSS.AUTH_TYPE(AUTH_TYPE_CD, AUTH_TYPE_DESC) VALUES('S', 'Live auth, pre-settlement amount known');
INSERT INTO PSS.AUTH_TYPE(AUTH_TYPE_CD, AUTH_TYPE_DESC) VALUES('O', 'Offline auth, pre-settlement amount known');
INSERT INTO PSS.AUTH_TYPE(AUTH_TYPE_CD, AUTH_TYPE_DESC) VALUES('A', 'Auth adjustment, applies to last auth event');
INSERT INTO PSS.AUTH_TYPE(AUTH_TYPE_CD, AUTH_TYPE_DESC) VALUES('D', 'Sale adjustment, applies to last sale event');


CREATE TABLE PSS.REFUND_STATE
  (
    REFUND_STATE_ID    NUMBER(20,0),
    REFUND_STATE_NAME  VARCHAR2(60)   not null,
    CONSTRAINT PK_REFUND_STATE_ID primary key(REFUND_STATE_ID)
  )
  TABLESPACE PSS_DATA;
  
CREATE PUBLIC SYNONYM REFUND_STATE FOR PSS.REFUND_STATE;

GRANT SELECT, INSERT, UPDATE, DELETE ON PSS.REFUND_STATE TO WEB_USER;

INSERT INTO PSS.REFUND_STATE
SELECT * FROM PSS.TRAN_REFUND_STATE;


CREATE TABLE PSS.REFUND_TYPE
  (
    REFUND_TYPE_CD    VARCHAR2(1),
    REFUND_TYPE_DESC  VARCHAR2(60)   not null,
    CONSTRAINT PK_REFUND_TYPE_CD primary key(REFUND_TYPE_CD)
  )
  TABLESPACE PSS_DATA;

CREATE PUBLIC SYNONYM REFUND_TYPE FOR PSS.REFUND_TYPE;

GRANT SELECT, INSERT, UPDATE, DELETE ON PSS.REFUND_TYPE TO WEB_USER;


CREATE OR REPLACE TRIGGER PSS.TRBI_REFUND_TYPE BEFORE INSERT ON PSS.REFUND_TYPE
  FOR EACH ROW 
BEGIN
	IF :NEW.refund_type_cd IS NULL
	THEN
		SELECT SEQ_refund_type_id.NEXTVAL
		INTO :NEW.refund_type_cd
		FROM DUAL;
	END IF;
END;
/

INSERT INTO PSS.REFUND_TYPE
SELECT * FROM PSS.TRAN_REFUND_TYPE;


CREATE TABLE PSS.TRAN_LINE_ITEM_BATCH_TYPE
  (
    TRAN_LINE_ITEM_BATCH_TYPE_CD    char(1),
    TRAN_LINE_ITEM_BATCH_TYPE_DESC  VARCHAR2(255)   not null,
    CONSTRAINT PK_TRAN_LINE_ITEM_BATCH_TYP_CD primary key(TRAN_LINE_ITEM_BATCH_TYPE_CD)
  )
  TABLESPACE PSS_DATA;
  
CREATE PUBLIC SYNONYM TRAN_LINE_ITEM_BATCH_TYPE FOR PSS.TRAN_LINE_ITEM_BATCH_TYPE;

GRANT SELECT, INSERT, UPDATE, DELETE ON PSS.TRAN_LINE_ITEM_BATCH_TYPE TO WEB_USER;
  
INSERT INTO PSS.TRAN_LINE_ITEM_BATCH_TYPE
SELECT * FROM PSS.TRAN_BATCH_TYPE;


CREATE TABLE PSS.TRAN_DEVICE_RESULT_TYPE
  (
    TRAN_DEVICE_RESULT_TYPE_CD    VARCHAR2(1),
    TRAN_DEVICE_RESULT_TYPE_DESC  VARCHAR2(60)   not null,
    CONSTRAINT PK_TRAN_DEVICE_RESULT_TYPE_CD primary key(TRAN_DEVICE_RESULT_TYPE_CD)
  )
  TABLESPACE PSS_DATA;

CREATE PUBLIC SYNONYM TRAN_DEVICE_RESULT_TYPE FOR PSS.TRAN_DEVICE_RESULT_TYPE;

GRANT SELECT, INSERT, UPDATE, DELETE ON PSS.TRAN_DEVICE_RESULT_TYPE TO WEB_USER;

COMMENT ON TABLE PSS.TRAN_DEVICE_RESULT_TYPE is 'Contains a listing of all the different states a transaction can be reported as by a client device.';

INSERT INTO PSS.TRAN_DEVICE_RESULT_TYPE
SELECT * FROM PSS.TRAN_CLIENT_TRAN;



CREATE TABLE PSS.REFUND
  (
    REFUND_ID        NUMBER(20,0),
    TRAN_ID          NUMBER(20,0)	not null,
    REFUND_AMT       NUMBER(14,4),
    REFUND_DESC      VARCHAR2(60),
    REFUND_ISSUE_TS  DATE,
    REFUND_ISSUE_BY  VARCHAR2(60),
    REFUND_TYPE_CD   VARCHAR2(1)    not null,
    CREATED_BY       VARCHAR2(30)   not null,
    CREATED_TS       DATE           not null,
    LAST_UPDATED_BY  VARCHAR2(30)   not null,
    LAST_UPDATED_TS  DATE           not null,
    REFUND_STATE_ID  NUMBER(20,0)   not null,
    REFUND_PARSED_ACCT_DATA      VARCHAR2(107),
    ACCT_ENTRY_METHOD_CD      char(1),
    REFUND_BATCH_NUM NUMBER(20,0),
    REFUND_RESP_CD            VARCHAR2(255),
    REFUND_RESP_DESC          VARCHAR2(255),
    REFUND_AUTHORITY_TRAN_CD  VARCHAR2(255),
    REFUND_AUTHORITY_REF_CD   VARCHAR2(255),
    REFUND_AUTHORITY_TS       VARCHAR2(255),
    CONSTRAINT PK_REFUND_ID primary key(REFUND_ID),
    --CONSTRAINT FK_REFUND_TRAN_ID foreign key(TRAN_ID) references PSS.TRAN(TRAN_ID),
    CONSTRAINT FK_REFUND_REFUND_TYPE_CD foreign key(REFUND_TYPE_CD) references PSS.REFUND_TYPE(REFUND_TYPE_CD),
    CONSTRAINT FK_REFUND_REFUND_STATE_ID foreign key(REFUND_STATE_ID) references PSS.REFUND_STATE(REFUND_STATE_ID),
    CONSTRAINT FK_REFUND_ACCT_ENTRY_METHOD_CD foreign key(ACCT_ENTRY_METHOD_CD) references PSS.ACCT_ENTRY_METHOD(ACCT_ENTRY_METHOD_CD)
  )
  TABLESPACE PSS_DATA;
      
CREATE PUBLIC SYNONYM REFUND FOR PSS.REFUND;

GRANT SELECT, INSERT, UPDATE, DELETE ON PSS.REFUND TO WEB_USER;

CREATE TABLE PSS.AUTH_RESULT
  (
    AUTH_RESULT_CD    char(1),
    AUTH_RESULT_DESC  VARCHAR2(255)   not null,
    CONSTRAINT PK_AUTH_RESULT_CD primary key(AUTH_RESULT_CD)
  )
  TABLESPACE PSS_DATA;

CREATE PUBLIC SYNONYM AUTH_RESULT FOR PSS.AUTH_RESULT;

GRANT SELECT, INSERT, UPDATE, DELETE ON PSS.AUTH_RESULT TO WEB_USER;

COMMENT ON TABLE PSS.AUTH_RESULT is 'Internal authorization result state code based on authority response.';


INSERT INTO PSS.AUTH_RESULT VALUES ('Y', 'Authorization successful');
INSERT INTO PSS.AUTH_RESULT VALUES ('N', 'Authorization unsuccessful or denied');
INSERT INTO PSS.AUTH_RESULT VALUES ('F', 'Authorization failed');
INSERT INTO PSS.AUTH_RESULT VALUES ('P', 'Authorization partially successful -- authorized for amount less than original authorization');


CREATE TABLE PSS.AUTH_STATE
  (
    AUTH_STATE_ID    NUMBER(20,0),
    AUTH_STATE_NAME  VARCHAR2(60)   not null,
    CONSTRAINT PK_AUTH_STATE_ID primary key(AUTH_STATE_ID)
  )
  TABLESPACE PSS_DATA;
  
CREATE PUBLIC SYNONYM AUTH_STATE FOR PSS.AUTH_STATE;

GRANT SELECT, INSERT, UPDATE, DELETE ON PSS.AUTH_STATE TO WEB_USER;


INSERT INTO pss.auth_state (auth_state_id, auth_state_name) VALUES (1, 'WAITING_FOR_EVENT');
INSERT INTO pss.auth_state (auth_state_id, auth_state_name) VALUES (2, 'SUCCESS');
INSERT INTO pss.auth_state (auth_state_id, auth_state_name) VALUES (3, 'DECLINE');
INSERT INTO pss.auth_state (auth_state_id, auth_state_name) VALUES (4, 'FAILURE');
INSERT INTO pss.auth_state (auth_state_id, auth_state_name) VALUES (5, 'SUCCESS_CONDITIONAL');


CREATE PUBLIC SYNONYM AUTH FOR PSS.AUTH;

GRANT SELECT, INSERT, UPDATE, DELETE ON PSS.AUTH TO WEB_USER;


ALTER TABLE PSS.AUTHORITY_PAYMENT_MASK ADD 
(
	AUTHORITY_ASSN_ID NUMBER(20,0),
	CONSTRAINT FK_AUTY_PMNT_MASK_AUTY_ASSN_ID foreign key(AUTHORITY_ASSN_ID) references AUTHORITY.AUTHORITY_ASSN(AUTHORITY_ASSN_ID)
);



CREATE TABLE PSS.CURRENCY
  (
    CURRENCY_CD    VARCHAR2(3),
    CURRENCY_NUM   NUMBER(3,0),
    CURRENCY_NAME  VARCHAR2(60)   not null,
    CONSTRAINT PK_CURRENCY_CD primary key(CURRENCY_CD)
  )
  TABLESPACE PSS_DATA;

CREATE PUBLIC SYNONYM CURRENCY FOR PSS.CURRENCY;

GRANT SELECT, INSERT, UPDATE, DELETE ON PSS.CURRENCY TO WEB_USER;


INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('AED','784','UAE Dirham');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('AFN','971','Afghani');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('ALL','8','Lek');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('AMD','51','Armenian Dram');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('ANG','532','Netherlands Antillian Guikder');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('AOA','973','Kwanza');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('ARS','32','Argentine Peso');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('AUD','36','Australian Dollar');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('AWG','533','Aruban Guilder');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('AZM','31','Azerbaijanian Manat');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('BAM','977','Convertible Marks');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('BBD','52','Barbados Dollar');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('BDT','50','Taka');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('BGN','975','Bulgarian Lev');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('BHD','48','Bahraini Dinar');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('BIF','108','Burundi Franc');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('BMD','60','Bermudian Dollar (customarily known as Bermuda Dollar)');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('BND','96','Brunei Dollar');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('BOB','68','Boliviano');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('BOV','984','Mvdol');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('BRL','986','Brazilian Real');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('BSD','44','Bahamian Dollar');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('BTN','64','Ngultrum');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('BWP','72','Pula');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('BYR','974','Belarussian Ruble');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('BZD','84','Belize Dollar');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('CAD','124','Canadian Dollar');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('CDF','976','Franc Congolais');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('CHE','947','WIR Euro');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('CHF','756','Swiss Franc');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('CHW','948','WIR Franc');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('CLF','990','Unidades de formento');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('CLP','152','Chilean Peso');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('CNY','156','Yuan Renminbi');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('COP','170','Colombian Peso');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('COU','970','Unidad de Valor Real');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('CRC','188','Costa Rican Colon');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('CSD','891','Serbian Dinar');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('CUP','192','Cuban Peso');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('CVE','132','Cape Verde Escudo');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('CYP','196','Cyprus Pound');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('CZK','203','Czech Koruna');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('DJF','262','Djibouti Franc');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('DKK','208','Danish Krone');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('DOP','214','Dominican Peso');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('DZD','12','Algerian Dinar');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('EEK','233','Kroon');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('EGP','818','Egyptian Pound');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('ERN','232','Nakfa');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('ETB','230','Ethiopian Birr');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('EUR','978','Euro');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('FJD','242','Fiji Dollar');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('FKP','238','Falkland Islands Pound');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('GBP','826','Pound Sterling');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('GEL','981','Lari');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('GHC','288','Cedi');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('GIP','292','Gibraltar Pound');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('GMD','270','Dalasi');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('GNF','324','Guinea Franc');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('GTQ','320','Quetzal');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('GWP','624','Guinea-Bissau Peso');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('GYD','328','Guyana Dollar');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('HKD','344','Hong Kong Dollar');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('HNL','340','Lempira');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('HRK','191','Croatian Kuna');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('HTG','332','Gourde');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('HUF','348','Forint');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('IDR','360','Rupiah');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('ILS','376','New Israeli Sheqel');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('INR','356','Indian Rupee');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('IQD','368','Iraqi Dinar');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('IRR','364','Iranian Rial');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('ISK','352','Iceland Krona');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('JMD','388','Jamaican Dollar');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('JOD','400','Jordanian Dinar');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('JPY','392','Yen');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('KES','404','Kenyan Shilling');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('KGS','417','Som');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('KHR','116','Riel');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('KMF','174','Comoro Franc');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('KPW','408','North Korean Won');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('KRW','410','Won');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('KWD','414','Kuwaiti Dinar');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('KYD','136','Cayman Islands Dollar');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('KZT','398','Tenge');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('LAK','418','Kip');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('LBP','422','Lebanese Pound');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('LKR','144','Sri Lanka Rupee');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('LRD','430','Liberian Dollar');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('LSL','426','Loti');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('LTL','440','Lithuanian Litas');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('LVL','428','Latvian Lats');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('LYD','434','Libyan Dinar');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('MAD','504','Moroccan Dirham');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('MDL','498','Moldovan Leu');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('MGA','969','Malagasy Ariary');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('MKD','807','Denar');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('MMK','104','Kyat');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('MNT','496','Tugrik');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('MOP','446','Pataca');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('MRO','478','Ouguiya');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('MTL','470','Maltese Lira');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('MUR','480','Mauritius Rupee');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('MVR','462','Rufiyaa');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('MWK','454','Kwacha');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('MXN','484','Mexican Peso');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('MXV','979','Mexican Unidad de Inversion (UID)');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('MYR','458','Malaysian Ringgit');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('MZM','508','Merical');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('NAD','516','Namibian Dollar');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('NGN','566','Naira');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('NIO','558','Cordoba Oro');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('NOK','578','Norwegian Krone');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('NPR','524','Nepalese Rupee');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('NZD','554','New Zealand Dollar');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('OMR','512','Rial Omani');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('PAB','590','Balboa');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('PEN','604','Nuevo Sol');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('PGK','598','Kina');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('PHP','608','Philippine Peso');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('PKR','586','Pakistan Rupee');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('PLN','985','Zloty');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('PYG','600','Guarani');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('QAR','634','Qatari Rial');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('ROL','642','Leu');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('RUB','643','Russian Ruble');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('RWF','646','Rwanda Franc');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('SAR','682','Saudi Riyal');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('SBD','90','Solomon Islands Dollar');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('SCR','690','Seychelles Rupee');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('SDD','736','Sudanese Dinar');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('SEK','752','Swedish Krona');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('SGD','702','Singapore Dollar');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('SHP','654','Saint Helena Pound');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('SIT','705','Tolar');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('SKK','703','Slovak Koruna');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('SLL','694','Leone');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('SOS','706','Somali Shilling');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('SRD','968','Surinam Dollar');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('STD','678','Dobra');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('SVC','222','El Salvador Colon');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('SYP','760','Syrian Pound');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('SZL','748','Lilangeni');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('THB','764','Baht');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('TJS','972','Somoni');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('TMM','795','Manat');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('TND','788','Tunisian Dinar');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('TOP','776','Pa''anga');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('TRL','792','old Turkish Lira');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('TRY','949','new Turkish Lira');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('TTD','780','Trinidad and Tobago Dollar');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('TWD','901','New Taiwan Dollar');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('TZS','834','Tanzanian Shilling');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('UAH','980','Hryvnia');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('UGX','800','Uganda Shilling');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('USD','840','US Dollar');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('USN','997','(Next day)');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('USS','998','(Same day)');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('UYU','858','Peso Uruguayo');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('UZS','860','Uzbekistan Sum');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('VEB','862','Bolivar');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('VND','704','Dong');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('VUV','548','Vatu');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('WST','882','Tala');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('XAF','950','CFA Franc BEAC');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('XAG','961','Silver');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('XAU','959','Gold');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('XBA','955','Bond Markets Units European Composite Unit (EURCO)');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('XBB','956','European Monetary Unit (E.M.U.-6) ');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('XBC','957','European Unit of Account 9(E.U.A.-9)');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('XBD','958','European Unit of Account 17(E.U.A.-17)');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('XCD','951','East Caribbean Dollar');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('XDR','960','SDR');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('XFO',NULL,'Gold-Franc');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('XFU',NULL,'UIC-Franc');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('XOF','952','CFA Franc BCEAO');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('XPD','964','Palladium');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('XPF','953','CFP Franc');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('XPT','962','Platinum');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('XTS','963','Codes specifically reserved for testing purposes');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('XXX','999','Code assigned for transactions where no currency is involved');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('YER','886','Yemeni Rial');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('ZAR','710','Rand');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('ZMK','894','Kwacha');
INSERT INTO pss.currency (currency_cd, currency_num, currency_name) VALUES ('ZWD','716','Zimbabwe Dollar');


ALTER TABLE PSS.CONSUMER_ACCT ADD
(
		AUTHORITY_ID                    NUMBER(20,0),
    	CURRENCY_CD                     VARCHAR2(3)    default 'USD' not null,
    	CONSUMER_ACCT_BALREF_AMT        NUMBER(8,2),
    	CONSUMER_ACCT_BALREF_PERIOD_HR  NUMBER        default 0 not null,
    	CONSUMER_ACCT_LAST_BALREF_TS    DATE   default SYSDATE not null,
    	CONSTRAINT FK_CONSUMER_ACCT_AUTHORITY_ID foreign key(AUTHORITY_ID) references AUTHORITY.AUTHORITY(AUTHORITY_ID),
    	CONSTRAINT FK_CONSUMER_ACCT_CURRENCY_CD foreign key(CURRENCY_CD) references PSS.CURRENCY(CURRENCY_CD)
);


CREATE TABLE PSS.DUP_TRAN
  (
    TRAN_ID                      NUMBER(20,0),
    TRAN_START_TS                DATE    not null,
    TRAN_UPLOAD_TS               DATE,
    TRAN_END_TS                  DATE,
    TRAN_STATE_CD                VARCHAR2(1)     not null,
    TRAN_DESC                    VARCHAR2(60),
    CONSUMER_ACCT_ID             NUMBER(20,0),
    TRAN_DEVICE_TRAN_CD          VARCHAR2(20),
    CREATED_BY                   VARCHAR2(30)    not null,
    CREATED_TS                   DATE    not null,
    LAST_UPDATED_BY              VARCHAR2(30)    not null,
    LAST_UPDATED_TS              DATE    not null,
    TRAN_ACCOUNT_PIN             VARCHAR2(4),
    TRAN_RECEIVED_RAW_ACCT_DATA  VARCHAR2(107),
    TRAN_PARSED_ACCT_NAME        VARCHAR2(30),
    TRAN_PARSED_ACCT_EXP_DATE    VARCHAR2(6),
    TRAN_PARSED_ACCT_NUM         VARCHAR2(30),
    TRAN_REPORTABLE_ACCT_NUM     VARCHAR2(19),
    POS_PTA_ID                   NUMBER(20,0)    not null,
    TRAN_DEVICE_RESULT_TYPE_CD   VARCHAR2(1),
    TRAN_GLOBAL_TRANS_CD         VARCHAR2(255),
    TRAN_PARSED_ACCT_NUM_HASH    VARCHAR2(255),
    TRAN_PARSED_ACCT_NUM_ENCR    VARCHAR2(255),
    TRAN_LEGACY_TRANS_NO         VARCHAR2(16),
    CONSTRAINT PK_TRAN_ID primary key(TRAN_ID),
    CONSTRAINT FK_DUP_TRAN_TRAN_STATE_CD foreign key(TRAN_STATE_CD) references PSS.TRAN_STATE(TRAN_STATE_CD),
    CONSTRAINT FK_DUP_TRAN_CONSUMER_ACCT_ID foreign key(CONSUMER_ACCT_ID) references PSS.CONSUMER_ACCT(CONSUMER_ACCT_ID),
    CONSTRAINT FK_DUP_TRAN_POS_PTA_ID foreign key(POS_PTA_ID) references PSS.POS_PTA(POS_PTA_ID),
    CONSTRAINT FK_DUP_TRAN_TRAN_DVC_RES_TP_CD foreign key(TRAN_DEVICE_RESULT_TYPE_CD) references PSS.TRAN_DEVICE_RESULT_TYPE(TRAN_DEVICE_RESULT_TYPE_CD)
  )
  TABLESPACE PSS_DATA;
  
CREATE PUBLIC SYNONYM DUP_TRAN FOR PSS.DUP_TRAN;

GRANT SELECT, INSERT, UPDATE, DELETE ON PSS.DUP_TRAN TO WEB_USER;

CREATE UNIQUE INDEX PSS.UDX_DUP_TRAN_1 ON PSS.DUP_TRAN(POS_PTA_ID) TABLESPACE PSS_INDX;

CREATE UNIQUE INDEX PSS.UDX_DUP_TRAN_2 ON PSS.DUP_TRAN(TRAN_STATE_CD,LAST_UPDATED_TS,POS_PTA_ID) TABLESPACE PSS_INDX;



CREATE OR REPLACE TRIGGER PSS.TRBI_DUP_TRAN BEFORE INSERT ON PSS.DUP_TRAN
  FOR EACH ROW 
BEGIN
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/



CREATE OR REPLACE TRIGGER PSS.TRBU_DUP_TRAN BEFORE UPDATE ON PSS.DUP_TRAN
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/



COMMENT ON TABLE PSS.DUP_TRAN is 'Contains information about each transactions recorded andor processed by the PSS system.  A transaction begins when a user or process initiates a transactions and ends when the user or process ends the transaction, a timeout occurs, or an error occurs.  A transaction does not contain information relating to what was purchased, only that a transaction was started and ended.';

COMMENT ON COLUMN PSS.DUP_TRAN.TRAN_START_TS is 'The date and time the transaction was initiated.  The value will typically be generated by a card swipe, fob swipe, etc.';

COMMENT ON COLUMN PSS.DUP_TRAN.TRAN_UPLOAD_TS is 'The date and time the transaction was received by the server.';

COMMENT ON COLUMN PSS.DUP_TRAN.TRAN_END_TS is 'The date and time the transaction was ended. The value can be generated by the user pressing end, allowing the device to time out, etc.';

COMMENT ON COLUMN PSS.DUP_TRAN.TRAN_DESC is 'A description of the transaction that can be provided by the client.';

COMMENT ON COLUMN PSS.DUP_TRAN.TRAN_DEVICE_TRAN_CD is 'The transaction idcode generated by the device.  The code will typically be unique for each client.  The code may be reset or start over causing duplicate order codes over time.';

COMMENT ON COLUMN PSS.DUP_TRAN.TRAN_RECEIVED_RAW_ACCT_DATA is 'The data received at the device to authorize the transactions.  Values will be encrypted. Will typically be the map stripe data on a swiped card.';

COMMENT ON COLUMN PSS.DUP_TRAN.TRAN_PARSED_ACCT_NAME is 'The name of the person who executed the transaction.  Values will be encrypted.  Derived from the TRAN_RECEIVED_RAW_ACCT_DATA field.';

COMMENT ON COLUMN PSS.DUP_TRAN.TRAN_PARSED_ACCT_EXP_DATE is 'The experation data of the account used for the transaction.  Values will be encrypted. It should be in the form MMYYYY.  It is derived from TRAN_RECEIVED_RAW_ACCT_DATA.';

COMMENT ON COLUMN PSS.DUP_TRAN.TRAN_PARSED_ACCT_NUM is 'The account number used for the transaction.  Values will be encrypted.  Derived from the TRAN_RECEIVED_RAW_ACCT_DATA field.';

COMMENT ON COLUMN PSS.DUP_TRAN.TRAN_REPORTABLE_ACCT_NUM is 'A reportable representation of the account number used for the transactions.  An example would be XXXX-XXXX-XXXX-3234 (last 4 digits of a credit card).  Values will not be encrypted.';



CREATE TABLE PSS.DUP_TRAN_LINE_ITEM
  (
    TRAN_LINE_ITEM_ID             NUMBER(20,0),
    TRAN_LINE_ITEM_AMOUNT         NUMBER(14,4),
    TRAN_LINE_ITEM_TAX            NUMBER(14,4),
    TRAN_LINE_ITEM_TS             DATE,
    TRAN_LINE_ITEM_POSITION_CD    VARCHAR2(6),
    TRAN_LINE_ITEM_DESC           VARCHAR2(60),
    TRAN_LINE_ITEM_TYPE_ID        NUMBER(20,0)   not null,
    TRAN_ID                       NUMBER(20,0)   not null,
    TRAN_LINE_ITEM_QUANTITY       NUMBER(3,0),
    HOST_ID                       NUMBER(20,0),
    CREATED_BY                    VARCHAR2(30)   not null,
    CREATED_TS                    DATE   not null,
    LAST_UPDATED_BY               VARCHAR2(30)   not null,
    LAST_UPDATED_TS               DATE   not null,
    TRAN_LINE_ITEM_BATCH_TYPE_CD  char(1)        not null,
    CONSTRAINT PK_TRAN_LINE_ITEM_ID primary key(TRAN_LINE_ITEM_ID),
    CONSTRAINT FK_DUP_TLI_TLI_TYPE_ID foreign key(TRAN_LINE_ITEM_TYPE_ID) references PSS.TRAN_LINE_ITEM_TYPE(TRAN_LINE_ITEM_TYPE_ID),
    CONSTRAINT FK_DUP_TRAN_LINE_ITEM_TRAN_ID foreign key(TRAN_ID) references PSS.DUP_TRAN(TRAN_ID),
    CONSTRAINT FK_DUP_TRAN_LINE_ITEM_HOST_ID foreign key(HOST_ID) references DEVICE.HOST(HOST_ID),
    CONSTRAINT FK_DUP_TLI_TLI_BATCH_TYPE_CD foreign key(TRAN_LINE_ITEM_BATCH_TYPE_CD) references PSS.TRAN_LINE_ITEM_BATCH_TYPE(TRAN_LINE_ITEM_BATCH_TYPE_CD)
  )
  TABLESPACE PSS_DATA;
  
CREATE PUBLIC SYNONYM DUP_TRAN_LINE_ITEM FOR PSS.DUP_TRAN_LINE_ITEM;

GRANT SELECT, INSERT, UPDATE, DELETE ON PSS.DUP_TRAN_LINE_ITEM TO WEB_USER;

CREATE INDEX PSS.IDX_DUP_TRAN_LINE_ITEM_2 ON PSS.DUP_TRAN_LINE_ITEM(TRAN_LINE_ITEM_TYPE_ID) TABLESPACE PSS_INDX;

CREATE INDEX PSS.IDX_DUP_TRAN_LINE_ITEM_3 ON PSS.DUP_TRAN_LINE_ITEM(HOST_ID) TABLESPACE PSS_INDX;



CREATE OR REPLACE TRIGGER PSS.TRBI_DUP_TRAN_LINE_ITEM BEFORE INSERT ON PSS.DUP_TRAN_LINE_ITEM
  FOR EACH ROW 
BEGIN
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/



CREATE OR REPLACE TRIGGER PSS.TRBU_DUP_TRAN_LINE_ITEM BEFORE UPDATE ON PSS.DUP_TRAN_LINE_ITEM
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/



COMMENT ON TABLE PSS.DUP_TRAN_LINE_ITEM is 'Contains a information about all the items that make up a transaction.';



CREATE TABLE PSS.DUP_AUTH
  (
    AUTH_ID                    NUMBER(20,0),
    TRAN_ID                    NUMBER(20,0)    not null,
    AUTH_TYPE_CD               char(1)         not null,
    AUTH_STATE_ID              NUMBER(20,0)    not null,
    AUTH_PARSED_ACCT_DATA      VARCHAR2(107),
    ACCT_ENTRY_METHOD_CD       char(1),
    AUTH_AMT                   NUMBER(14,4)    not null,
    AUTH_AMT_APPROVED          NUMBER(14,4),
    AUTH_TS                    DATE            not null,
    AUTH_RESULT_CD             char(1),
    AUTH_RESP_CD               VARCHAR2(60),
    AUTH_RESP_DESC             VARCHAR2(255),
    AUTH_AUTHORITY_TRAN_CD     VARCHAR2(60),
    AUTH_AUTHORITY_REF_CD      VARCHAR2(255),
    AUTH_AUTHORITY_TS          DATE,
    CREATED_BY                 VARCHAR2(30)    not null,
    CREATED_TS                 DATE    not null,
    LAST_UPDATED_BY            VARCHAR2(30)    not null,
    LAST_UPDATED_TS            DATE    not null,
    CONSTRAINT PK_DUP_AUTH_ID primary key(AUTH_ID),
    CONSTRAINT FK_DUP_AUTH_TRAN_ID foreign key(TRAN_ID) references PSS.DUP_TRAN(TRAN_ID),
    CONSTRAINT FK_DUP_AUTH_AUTH_TYPE_CD foreign key(AUTH_TYPE_CD) references PSS.AUTH_TYPE(AUTH_TYPE_CD),
    CONSTRAINT FK_DUP_AUTH_AUTH_STATE_ID foreign key(AUTH_STATE_ID) references PSS.AUTH_STATE(AUTH_STATE_ID),
    CONSTRAINT FK_DUP_AUTH_ACCT_NTR_MTD_CD foreign key(ACCT_ENTRY_METHOD_CD) references PSS.ACCT_ENTRY_METHOD(ACCT_ENTRY_METHOD_CD),
    CONSTRAINT FK_DUP_AUTH_AUTH_RESULT_CD foreign key(AUTH_RESULT_CD) references PSS.AUTH_RESULT(AUTH_RESULT_CD)
  )
  TABLESPACE PSS_DATA;

CREATE PUBLIC SYNONYM DUP_AUTH FOR PSS.DUP_AUTH;

GRANT SELECT, INSERT, UPDATE, DELETE ON PSS.DUP_AUTH TO WEB_USER;

CREATE OR REPLACE TRIGGER PSS.TRBI_DUP_AUTH BEFORE INSERT ON PSS.DUP_AUTH
  FOR EACH ROW 
BEGIN
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/



CREATE OR REPLACE TRIGGER PSS.TRBU_DUP_AUTH BEFORE UPDATE ON PSS.DUP_AUTH
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/


CREATE TABLE PSS.MERCHANT
  (
    MERCHANT_ID        NUMBER(20,0),
    MERCHANT_CD        VARCHAR2(255)   not null,
    MERCHANT_NAME      VARCHAR2(255)   not null,
    MERCHANT_DESC      VARCHAR2(255),
    MERCHANT_BUS_NAME  VARCHAR2(255),
    AUTHORITY_ID       NUMBER(20,0)    not null,
    CREATED_BY         VARCHAR2(30)    not null,
    CREATED_TS         DATE    not null,
    LAST_UPDATED_BY    VARCHAR2(30)    not null,
    LAST_UPDATED_TS    DATE    not null,
    CONSTRAINT PK_MERCHANT_ID primary key(MERCHANT_ID),
    CONSTRAINT FK_MERCHANT_AUTHORITY_ID foreign key(AUTHORITY_ID) references AUTHORITY.AUTHORITY(AUTHORITY_ID)
  )
  TABLESPACE PSS_DATA;

CREATE PUBLIC SYNONYM MERCHANT FOR PSS.MERCHANT;

GRANT SELECT, INSERT, UPDATE, DELETE ON PSS.MERCHANT TO WEB_USER;

CREATE OR REPLACE TRIGGER PSS.TRBI_MERCHANT BEFORE INSERT ON PSS.MERCHANT
  FOR EACH ROW 
BEGIN
	IF :NEW.merchant_id IS NULL
	THEN
		SELECT SEQ_merchant_id.NEXTVAL
		INTO :NEW.merchant_id
		FROM DUAL;
	END IF;

	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/



CREATE OR REPLACE TRIGGER PSS.TRBU_MERCHANT BEFORE UPDATE ON PSS.MERCHANT
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/



CREATE TABLE PSS.MERCHANT_BANK_ACCT
  (
    MERCHANT_BANK_ACCT_ID    NUMBER(20,0),
    MERCHANT_BANK_ACCT_NAME  VARCHAR2(60)    not null,
    MERCHANT_BANK_ACCT_CD    VARCHAR2(255)   not null,
    MERCHANT_ID              NUMBER(20,0)    not null,
    CREATED_BY               VARCHAR2(30)    not null,
    CREATED_TS               DATE    not null,
    LAST_UPDATED_BY          VARCHAR2(30)    not null,
    LAST_UPDATED_TS          DATE    not null,
    CONSTRAINT PK_MERCHANT_BANK_ACCT_ID primary key(MERCHANT_BANK_ACCT_ID),
    CONSTRAINT FK_MERCH_BANK_ACCT_MERCH_ID foreign key(MERCHANT_ID) references PSS.MERCHANT(MERCHANT_ID)
  )
  TABLESPACE PSS_DATA;

CREATE PUBLIC SYNONYM MERCHANT_BANK_ACCT FOR PSS.MERCHANT_BANK_ACCT;

GRANT SELECT, INSERT, UPDATE, DELETE ON PSS.MERCHANT_BANK_ACCT TO WEB_USER;


CREATE OR REPLACE TRIGGER PSS.TRBI_MERCHANT_BANK_ACCT BEFORE INSERT ON PSS.MERCHANT_BANK_ACCT
  FOR EACH ROW 
BEGIN
	IF :NEW.merchant_bank_acct_id IS NULL
	THEN
		SELECT SEQ_merchant_bank_acct_id.NEXTVAL
		INTO :NEW.merchant_bank_acct_id
		FROM DUAL;
	END IF;

	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/



CREATE OR REPLACE TRIGGER PSS.TRBU_MERCHANT_BANK_ACCT BEFORE UPDATE ON PSS.MERCHANT_BANK_ACCT
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/



CREATE TABLE PSS.MERCHANT_CURRENCY
  (
    MERCHANT_ID      NUMBER(20,0),
    CURRENCY_CD      VARCHAR2(3),
    CREATED_BY       VARCHAR2(30)   not null,
    CREATED_TS       DATE   not null,
    LAST_UPDATED_BY  VARCHAR2(30)   not null,
    LAST_UPDATED_TS  DATE   not null,
    CONSTRAINT PK_MERCHANT_ID_CURRENCY_CD primary key(MERCHANT_ID,CURRENCY_CD),
    CONSTRAINT FK_MERCH_CURR_MERCH_ID foreign key(MERCHANT_ID) references PSS.MERCHANT(MERCHANT_ID),
    CONSTRAINT FK_MERCH_CURR_CURR_CD foreign key(CURRENCY_CD) references PSS.CURRENCY(CURRENCY_CD)
  )
  TABLESPACE PSS_DATA;

CREATE PUBLIC SYNONYM MERCHANT_CURRENCY FOR PSS.MERCHANT_CURRENCY;

GRANT SELECT, INSERT, UPDATE, DELETE ON PSS.MERCHANT_CURRENCY TO WEB_USER;


CREATE OR REPLACE TRIGGER PSS.TRBI_MERCHANT_CURRENCY BEFORE INSERT ON PSS.MERCHANT_CURRENCY
  FOR EACH ROW 
BEGIN
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/



CREATE OR REPLACE TRIGGER PSS.TRBU_MERCHANT_CURRENCY BEFORE UPDATE ON PSS.MERCHANT_CURRENCY
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/


CREATE TABLE PSS.TERMINAL
  (
    TERMINAL_ID            NUMBER(20,0),
    TERMINAL_CD            VARCHAR(255)    not null,
    TERMINAL_ENCRYPT_KEY   VARCHAR(2048),
    TERMINAL_ENCRTPY_KEY2  VARCHAR(2048),
    TERMINAL_BATCH_NUM     NUMBER(20,0)   not null,
    MERCHANT_ID            NUMBER(20,0)    not null,
    CREATED_BY             VARCHAR(30)     not null,
    CREATED_TS             DATE    not null,
    LAST_UPDATED_BY        VARCHAR(30)     not null,
    LAST_UPDATED_TS        DATE    not null,
    primary key(TERMINAL_ID),
    foreign key(MERCHANT_ID) references PSS.MERCHANT(MERCHANT_ID)
  );

CREATE PUBLIC SYNONYM TERMINAL FOR PSS.TERMINAL;

GRANT SELECT, INSERT, UPDATE, DELETE ON PSS.TERMINAL TO WEB_USER;

CREATE OR REPLACE TRIGGER PSS.TRBI_TERMINAL BEFORE INSERT ON PSS.TERMINAL
  FOR EACH ROW 
BEGIN
	IF :NEW.terminal_id IS NULL
	THEN
		SELECT SEQ_terminal_id.NEXTVAL
		INTO :NEW.terminal_id
		FROM DUAL;
	END IF;

	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/



CREATE OR REPLACE TRIGGER PSS.TRBU_TERMINAL BEFORE UPDATE ON PSS.TERMINAL
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/


ALTER TABLE PSS.POS_PTA ADD
(
    TERMINAL_ID                NUMBER(20,0),
    MERCHANT_BANK_ACCT_ID      NUMBER(20,0),
    CURRENCY_CD                VARCHAR2(3),
    CONSTRAINT FK_POS_PTA_TERMINAL_ID foreign key(TERMINAL_ID) references PSS.TERMINAL(TERMINAL_ID),
    CONSTRAINT FK_POS_PTA_MERCH_BANK_ACCT_ID foreign key(MERCHANT_BANK_ACCT_ID) references PSS.MERCHANT_BANK_ACCT(MERCHANT_BANK_ACCT_ID),
    CONSTRAINT FK_POS_PTA_CURR_CD foreign key(CURRENCY_CD) references PSS.CURRENCY(CURRENCY_CD)
);



CREATE TABLE PSS.REFUND_SETTLEMENT_BATCH
  (
    REFUND_ID                      NUMBER(20,0),
    SETTLEMENT_BATCH_ID            NUMBER(20,0),
    REFUND_SETTLEMENT_B_AMT        NUMBER(14,4),
    REFUND_SETTLEMENT_B_RESP_CD    VARCHAR2(60),
    REFUND_SETTLEMENT_B_RESP_DESC  VARCHAR2(255),
    CREATED_BY                     VARCHAR2(30)    not null,
    CREATED_TS                     DATE    not null,
    LAST_UPDATED_BY                VARCHAR2(30)    not null,
    LAST_UPDATED_TS                DATE    not null,
    CONSTRAINT PK_REFUND_ID_SETTLMT_BATCH_ID primary key(REFUND_ID,SETTLEMENT_BATCH_ID),
    CONSTRAINT FK_REFUND_STMT_BATCH_REFUND_ID foreign key(REFUND_ID) references PSS.REFUND(REFUND_ID),
    CONSTRAINT FK_REF_STMT_BTCH_STMT_BTCH_ID foreign key(SETTLEMENT_BATCH_ID) references PSS.SETTLEMENT_BATCH(SETTLEMENT_BATCH_ID)
  )
  TABLESPACE PSS_DATA;
  
CREATE PUBLIC SYNONYM REFUND_SETTLEMENT_BATCH FOR PSS.REFUND_SETTLEMENT_BATCH;

GRANT SELECT, INSERT, UPDATE, DELETE ON PSS.REFUND_SETTLEMENT_BATCH TO WEB_USER;


ALTER TABLE PSS.SETTLEMENT_BATCH RENAME COLUMN SETTLEMENT_BATCH_TS TO SETTLEMENT_BATCH_START_TS;

ALTER TABLE PSS.SETTLEMENT_BATCH RENAME COLUMN SETTLEMENT_BATCH_RESPONSE_CD TO SETTLEMENT_BATCH_RESP_CD;

ALTER TABLE PSS.SETTLEMENT_BATCH ADD
(
	SETTLEMENT_BATCH_END_TS   DATE,
	SETTLEMENT_BATCH_RESP_DESC  VARCHAR2(60),
	SETTLEMENT_BATCH_REF_CD     VARCHAR2(255),
	AUTHORITY_ID                NUMBER(20,0),
	CONSTRAINT FK_SETTLMT_BATCH_AUTHORITY_ID foreign key(AUTHORITY_ID) references AUTHORITY.AUTHORITY(AUTHORITY_ID)
);

CREATE OR REPLACE TRIGGER PSS.trbi_settlement_batch BEFORE INSERT ON pss.settlement_batch
 FOR EACH ROW
BEGIN
   IF :NEW.settlement_batch_id IS NULL
   THEN
      SELECT seq_settlement_batch_id.NEXTVAL
        INTO :NEW.settlement_batch_id
        FROM DUAL;
   END IF;

   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
     
    IF :NEW.settlement_batch_start_ts IS NULL
    THEN
        :NEW.settlement_batch_start_ts := SYSDATE;
    END IF;
END;
/

ALTER TABLE PSS.TRAN_LINE_ITEM DROP CONSTRAINT FK_TRAN_BATCH_TYPE_CD;
ALTER TABLE PSS.TRAN_LINE_ITEM RENAME COLUMN TRAN_BATCH_TYPE_CD TO TRAN_LINE_ITEM_BATCH_TYPE_CD;
ALTER TABLE PSS.TRAN_LINE_ITEM ADD CONSTRAINT FK_TLI_TLI_BATCH_TYPE_CD foreign key(TRAN_LINE_ITEM_BATCH_TYPE_CD) references PSS.TRAN_LINE_ITEM_BATCH_TYPE(TRAN_LINE_ITEM_BATCH_TYPE_CD);


CREATE TABLE PSS.TRAN_LINE_ITEM_TYPE_HOST_TYPE
  (
    TRAN_LINE_ITEM_TYPE_ID  NUMBER(20,0),
    HOST_TYPE_ID            NUMBER(20,0),
    CREATED_BY              VARCHAR2(30)   not null,
    CREATED_TS              DATE   not null,
    LAST_UPDATED_BY         VARCHAR2(30)   not null,
    LAST_UPDATED_TS         DATE   not null,
    CONSTRAINT PK_TLI_TYPE_ID_HOST_TYPE primary key(TRAN_LINE_ITEM_TYPE_ID,HOST_TYPE_ID),
    CONSTRAINT FK_TLI_TYP_HST_TYP_TLI_TYP_ID foreign key(TRAN_LINE_ITEM_TYPE_ID) references PSS.TRAN_LINE_ITEM_TYPE(TRAN_LINE_ITEM_TYPE_ID),
    CONSTRAINT FK_TLI_TYP_HOST_TYP_HOST_TYP foreign key(HOST_TYPE_ID) references DEVICE.HOST_TYPE(HOST_TYPE_ID)
  )
  TABLESPACE PSS_DATA;

CREATE PUBLIC SYNONYM TRAN_LINE_ITEM_TYPE_HOST_TYPE FOR PSS.TRAN_LINE_ITEM_TYPE_HOST_TYPE;

GRANT SELECT, INSERT, UPDATE, DELETE ON PSS.TRAN_LINE_ITEM_TYPE_HOST_TYPE TO WEB_USER;

CREATE OR REPLACE TRIGGER PSS.TRBI_TRAN_LINE_ITEM_TYPE_HOST BEFORE INSERT ON PSS.TRAN_LINE_ITEM_TYPE_HOST_TYPE
  FOR EACH ROW 
BEGIN
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/



CREATE OR REPLACE TRIGGER PSS.TRBU_TRAN_LINE_ITEM_TYPE_HOST BEFORE UPDATE ON PSS.TRAN_LINE_ITEM_TYPE_HOST_TYPE
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/


CREATE TABLE PSS.BANORTE_AUTHORITY
  (
    BANORTE_AUTHORITY_ID       NUMBER(20,0),
    BANORTE_AUTHORITY_NAME     VARCHAR2(60),
    BANORTE_GATEWAY_PORT_NUM   NUMBER(5,0),
    BANORTE_GATEWAY_ADDR       VARCHAR2(30),
    AUTHORITY_SERVICE_TYPE_ID  NUMBER(20,0)   not null,
    CREATED_BY                 VARCHAR2(30)   not null,
    CREATED_TS                 DATE   not null,
    LAST_UPDATED_BY            VARCHAR2(30)   not null,
    LAST_UPDATED_TS            DATE   not null,
    CONSTRAINT PK_BANORTE_AUTHORITY primary key(BANORTE_AUTHORITY_ID),
    CONSTRAINT FK_BANRT_AUTH_AUTH_SRV_TYPE_ID foreign key(AUTHORITY_SERVICE_TYPE_ID) references AUTHORITY.AUTHORITY_SERVICE_TYPE(AUTHORITY_SERVICE_TYPE_ID)
  )
  TABLESPACE PSS_DATA;

CREATE PUBLIC SYNONYM BANORTE_AUTHORITY FOR PSS.BANORTE_AUTHORITY;

GRANT SELECT, INSERT, UPDATE, DELETE ON PSS.BANORTE_AUTHORITY TO WEB_USER;

GRANT SELECT, REFERENCES ON PSS.BANORTE_AUTHORITY TO AUTHORITY;


CREATE OR REPLACE TRIGGER PSS.TRBI_BANORTE_AUTHORITY BEFORE INSERT ON PSS.BANORTE_AUTHORITY
  FOR EACH ROW 
BEGIN
    IF :new.banorte_authority_id IS NULL THEN
      SELECT pss.seq_banorte_authority_id.nextval
        into :new.banorte_authority_id
        FROM dual;
    END IF;
    
   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER PSS.TRBU_BANORTE_AUTHORITY BEFORE UPDATE ON PSS.BANORTE_AUTHORITY
  FOR EACH ROW 
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
     INTO :NEW.created_by,
          :NEW.created_ts,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/


CREATE TABLE PSS.BANORTE_TERMINAL
  (
    BANORTE_TERMINAL_ID           NUMBER(20,0),
    BANORTE_TERMINAL_DESC         VARCHAR2(60),
    BANORTE_AUTHORITY_ID          NUMBER(20,0)    not null,
    BANORTE_TERMINAL_MERCHANT_CD  VARCHAR2(60)    not null,
    BANORTE_TERMINAL_USERNAME     VARCHAR2(255)   not null,
    BANORTE_TERMINAL_PASSWORD     VARCHAR2(255)   not null,
    CREATED_BY                    VARCHAR2(30)    not null,
    CREATED_TS                    DATE    not null,
    LAST_UPDATED_BY               VARCHAR2(30)    not null,
    LAST_UPDATED_TS               DATE    not null,
    CONSTRAINT PK_BANORTE_TERMINAL primary key(BANORTE_TERMINAL_ID),
    CONSTRAINT FK_BANRT_TERM_BANRT_AUTH_ID foreign key(BANORTE_AUTHORITY_ID) references PSS.BANORTE_AUTHORITY(BANORTE_AUTHORITY_ID)
  )
  TABLESPACE PSS_DATA;

CREATE PUBLIC SYNONYM BANORTE_TERMINAL FOR PSS.BANORTE_TERMINAL;

GRANT SELECT, INSERT, UPDATE, DELETE ON PSS.BANORTE_TERMINAL TO WEB_USER;


CREATE OR REPLACE TRIGGER PSS.TRBI_BANORTE_TERMINAL BEFORE INSERT ON PSS.BANORTE_TERMINAL
  FOR EACH ROW 
BEGIN
    IF :new.banorte_terminal_id IS NULL THEN
      SELECT pss.seq_banorte_terminal_id.nextval
        into :new.banorte_terminal_id
        FROM dual;
    END IF;

   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/


CREATE OR REPLACE TRIGGER PSS.TRBU_BANORTE_TERMINAL BEFORE UPDATE ON PSS.BANORTE_TERMINAL
  FOR EACH ROW 
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
     INTO :NEW.created_by,
          :NEW.created_ts,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/


CREATE TABLE AUTHORITY.BANORTE_AUTHORITY_ASSN
  (
    BANORTE_AUTHORITY_ID  NUMBER(20,0),
    AUTHORITY_ASSN_ID     NUMBER(20,0),
    CREATED_BY            VARCHAR2(30)   not null,
    CREATED_TS            DATE   not null,
    LAST_UPDATED_BY       VARCHAR2(30)   not null,
    LAST_UPDATED_TS       DATE   not null,
    CONSTRAINT PK_BANORTE_AUTHORITY_ASSN primary key(BANORTE_AUTHORITY_ID,AUTHORITY_ASSN_ID),
    CONSTRAINT FK_BNRT_AUTH_ASSN_BNRT_AUTH_ID foreign key(BANORTE_AUTHORITY_ID) references PSS.BANORTE_AUTHORITY(BANORTE_AUTHORITY_ID),
    CONSTRAINT FK_BNRT_AUTH_ASSN_AUTH_ASSN_ID foreign key(AUTHORITY_ASSN_ID) references AUTHORITY.AUTHORITY_ASSN(AUTHORITY_ASSN_ID)
  )
  TABLESPACE AUTHORITY_DATA;

CREATE PUBLIC SYNONYM BANORTE_AUTHORITY_ASSN FOR AUTHORITY.BANORTE_AUTHORITY_ASSN;

GRANT SELECT, INSERT, UPDATE, DELETE ON AUTHORITY.BANORTE_AUTHORITY_ASSN TO WEB_USER;


CREATE OR REPLACE TRIGGER AUTHORITY.TRBI_BANORTE_AUTHORITY_ASSN BEFORE INSERT ON AUTHORITY.BANORTE_AUTHORITY_ASSN
  FOR EACH ROW 
BEGIN
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER AUTHORITY.TRBU_BANORTE_AUTHORITY_ASSN BEFORE UPDATE ON AUTHORITY.BANORTE_AUTHORITY_ASSN
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/


CREATE TABLE PSS.WEBPOS_AUTHORITY
  (
    WEBPOS_AUTHORITY_ID        NUMBER(20,0),
    WEBPOS_AUTHORITY_NAME      VARCHAR2(60),
    WEBPOS_GATEWAY_PORT_NUM    NUMBER(5,0),
    WEBPOS_GATEWAY_ADDR        VARCHAR2(30),
    AUTHORITY_SERVICE_TYPE_ID  NUMBER(20,0)   not null,
    CREATED_BY                 VARCHAR2(30)   not null,
    CREATED_TS                 DATE   not null,
    LAST_UPDATED_BY            VARCHAR2(30)   not null,
    LAST_UPDATED_TS            DATE   not null,
    CONSTRAINT PK_WEBPOS_AUTHORITY primary key(WEBPOS_AUTHORITY_ID),
    CONSTRAINT FK_WEBPOS_AUTH_AUTH_SRV_TYP_ID foreign key(AUTHORITY_SERVICE_TYPE_ID) references AUTHORITY.AUTHORITY_SERVICE_TYPE(AUTHORITY_SERVICE_TYPE_ID)
  )
  TABLESPACE PSS_DATA;

CREATE PUBLIC SYNONYM WEBPOS_AUTHORITY FOR PSS.WEBPOS_AUTHORITY;

GRANT SELECT, INSERT, UPDATE, DELETE ON PSS.WEBPOS_AUTHORITY TO WEB_USER;

GRANT SELECT, REFERENCES ON PSS.WEBPOS_AUTHORITY TO AUTHORITY;


CREATE OR REPLACE TRIGGER PSS.TRBI_WEBPOS_AUTHORITY BEFORE INSERT ON PSS.WEBPOS_AUTHORITY
  FOR EACH ROW 
BEGIN
    IF :new.webpos_authority_id IS NULL THEN
      SELECT pss.seq_webpos_authority_id.nextval
        into :new.webpos_authority_id
        FROM dual;
    END IF;
    
   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/


CREATE OR REPLACE TRIGGER PSS.TRBU_WEBPOS_AUTHORITY BEFORE UPDATE ON PSS.WEBPOS_AUTHORITY
  FOR EACH ROW 
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
     INTO :NEW.created_by,
          :NEW.created_ts,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/

CREATE TABLE PSS.WEBPOS_TERMINAL
  (
    WEBPOS_TERMINAL_ID           NUMBER(20,0),
    WEBPOS_TERMINAL_DESC         VARCHAR2(60),
    WEBPOS_AUTHORITY_ID          NUMBER(20,0)    not null,
    WEBPOS_TERMINAL_MERCHANT_CD  VARCHAR2(60)    not null,
    WEBPOS_TERMINAL_LOCALE_CD    VARCHAR2(60)    not null,
    WEBPOS_TERMINAL_USERNAME     VARCHAR2(255)   not null,
    WEBPOS_TERMINAL_PASSWORD     VARCHAR2(255)   not null,
    CREATED_BY                   VARCHAR2(30)    not null,
    CREATED_TS                   DATE    not null,
    LAST_UPDATED_BY              VARCHAR2(30)    not null,
    LAST_UPDATED_TS              DATE    not null,
    CONSTRAINT PK_WEBPOS_TERMINAL primary key(WEBPOS_TERMINAL_ID),
    CONSTRAINT FK_WEBPOS_TERM_WEBPOS_AUTH_ID foreign key(WEBPOS_AUTHORITY_ID) references PSS.WEBPOS_AUTHORITY(WEBPOS_AUTHORITY_ID)
  )
  TABLESPACE PSS_DATA;

CREATE PUBLIC SYNONYM WEBPOS_TERMINAL FOR PSS.WEBPOS_TERMINAL;

GRANT SELECT, INSERT, UPDATE, DELETE ON PSS.WEBPOS_TERMINAL TO WEB_USER;


CREATE OR REPLACE TRIGGER PSS.TRBI_WEBPOS_TERMINAL BEFORE INSERT ON PSS.WEBPOS_TERMINAL
  FOR EACH ROW 
BEGIN
    IF :new.webpos_terminal_id IS NULL THEN
      SELECT pss.seq_webpos_terminal_id.nextval
        into :new.webpos_terminal_id
        FROM dual;
    END IF;

   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER PSS.TRBU_WEBPOS_TERMINAL BEFORE UPDATE ON PSS.WEBPOS_TERMINAL
  FOR EACH ROW 
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
     INTO :NEW.created_by,
          :NEW.created_ts,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/


CREATE TABLE AUTHORITY.WEBPOS_AUTHORITY_ASSN
  (
    WEBPOS_AUTHORITY_ID  NUMBER(20,0),
    AUTHORITY_ASSN_ID    NUMBER(20,0),
    CREATED_BY           VARCHAR2(30)   not null,
    CREATED_TS           DATE   not null,
    LAST_UPDATED_BY      VARCHAR2(30)   not null,
    LAST_UPDATED_TS      DATE   not null,
    CONSTRAINT PK_WEBPOS_AUTHORITY_ASSN primary key(WEBPOS_AUTHORITY_ID,AUTHORITY_ASSN_ID),
    CONSTRAINT FK_WPOS_AUTH_ASSN_WPOS_AUTH_ID foreign key(WEBPOS_AUTHORITY_ID) references PSS.WEBPOS_AUTHORITY(WEBPOS_AUTHORITY_ID),
    CONSTRAINT FK_WPOS_AUTH_ASSN_AUTH_ASSN_ID foreign key(AUTHORITY_ASSN_ID) references AUTHORITY.AUTHORITY_ASSN(AUTHORITY_ASSN_ID)
  )
  TABLESPACE AUTHORITY_DATA;
  
CREATE PUBLIC SYNONYM WEBPOS_AUTHORITY_ASSN FOR AUTHORITY.WEBPOS_AUTHORITY_ASSN;

GRANT SELECT, INSERT, UPDATE, DELETE ON AUTHORITY.WEBPOS_AUTHORITY_ASSN TO WEB_USER;


CREATE OR REPLACE TRIGGER AUTHORITY.TRBI_WEBPOS_AUTHORITY_ASSN BEFORE INSERT ON AUTHORITY.WEBPOS_AUTHORITY_ASSN
  FOR EACH ROW 
BEGIN
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER AUTHORITY.TRBU_WEBPOS_AUTHORITY_ASSN BEFORE UPDATE ON AUTHORITY.WEBPOS_AUTHORITY_ASSN
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/


	
GRANT SELECT, REFERENCES ON PSS.CURRENCY TO LOCATION;

GRANT SELECT, REFERENCES ON PSS.MERCHANT TO LOCATION;


INSERT INTO AUTHORITY.AUTHORITY_SERVICE_TYPE
SELECT * FROM PSS.AUTHORITY_SERVICE_TYPE;

GRANT SELECT, REFERENCES ON AUTHORITY.AUTHORITY_SERVICE_TYPE TO PSS;


INSERT INTO PSS.TRAN_STATE VALUES('S', 'PROCESSED_SERVER_SALE');


CREATE OR REPLACE 
FUNCTION PSS.sf_pivot_tran_line_item (pn_tran_id IN tran.tran_id%TYPE)
   RETURN VARCHAR2
IS
   v_return_string   VARCHAR2 (4000) DEFAULT NULL;
   v_delimiter       VARCHAR2 (1)    DEFAULT NULL;
BEGIN
   FOR x IN (SELECT tran_line_item_desc
               FROM tran_line_item
              WHERE tran_id = pn_tran_id
                AND tran_line_item_batch_type_cd = 'A')
   LOOP
      v_return_string := v_return_string || v_delimiter || x.tran_line_item_desc;
      v_delimiter := CHR (13);
   END LOOP;

   RETURN v_return_string;
END;
/


CREATE OR REPLACE 
PROCEDURE PSS.load_tran_details 
(
    l_device_id IN device.device_id%TYPE,
    l_device_type_id IN device.device_type_id%TYPE,
    l_tran_id IN tran.tran_id%TYPE,
    l_tran_details IN VARCHAR2
)
   IS
    l_host_type_id host_type.host_type_id%TYPE;
    l_host_id host.host_id%TYPE;
    l_payload_code VARCHAR2(2);

    l_cnt NUMBER;
    l_pos NUMBER;
    l_list VARCHAR2(1000);

    TYPE payload_type IS TABLE OF VARCHAR2(100)
    INDEX BY BINARY_INTEGER;

    payload_rcd payload_type;
BEGIN
    -- currently transaction payload is implemented for BEX devices only
    IF l_device_type_id <> 11 OR NVL(LENGTH(l_tran_details), 0) < 4 THEN
        RETURN;
    END IF;
    
    DELETE FROM tran_line_item
    WHERE tran_id = l_tran_id;

    SELECT host_type_id INTO l_host_type_id
    FROM host_type
    WHERE host_type_cd = 'K' AND ROWNUM = 1;

    BEGIN
        SELECT host_id INTO l_host_id
        FROM host
        WHERE
            device_id = l_device_id
            AND host_type_id = l_host_type_id
            AND host_active_yn_flag = 'Y'
            AND ROWNUM = 1;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            INSERT INTO host
            (
                device_id,
                host_type_id,
                host_status_cd,
                host_serial_cd,
                host_est_complete_minut,
                host_port_num,
                host_setting_updated_yn_flag,
                host_position_num,
                host_active_yn_flag
            )
            VALUES
            (
                l_device_id,
                l_host_type_id,
                1,
                'Kiosk',
                0,
                0,
                'N',
                1,
                'Y'
            );
        
            SELECT host_id INTO l_host_id
            FROM host
            WHERE
                device_id = l_device_id
                AND host_type_id = l_host_type_id
                AND host_active_yn_flag = 'Y'
                AND ROWNUM = 1;
    END;
    
    l_cnt := 0;
    l_list := l_tran_details;

    LOOP
        l_pos := INSTR(l_list, '|');
        l_cnt := l_cnt + 1;
        IF l_pos > 0 THEN
            payload_rcd(l_cnt) := TRIM(SUBSTR(l_list, 1, l_pos - 1));
            l_list := SUBSTR(l_list, l_pos + 1);
        ELSE
            payload_rcd(l_cnt) := TRIM(l_list);
            EXIT;
        END IF;
    END LOOP;
    
    IF payload_rcd(1) IS NULL THEN
        RETURN;
    ELSE
        l_payload_code := SUBSTR(payload_rcd(1), 1, 2);
    END IF;
    
    IF l_payload_code = 'A2' THEN -- long format
        IF l_cnt = 1 THEN
            RETURN;
        ELSIF payload_rcd(2) IS NOT NULL THEN -- Start Date
            INSERT INTO tran_line_item(tran_id, host_id, tran_line_item_type_id, tran_line_item_desc, tran_line_item_batch_type_cd)
            VALUES(l_tran_id, l_host_id, 300, payload_rcd(2), 'A');
        END IF;
        
        IF l_cnt = 2 THEN
            RETURN;
        ELSIF payload_rcd(3) IS NOT NULL THEN -- Start Time
            INSERT INTO tran_line_item(tran_id, host_id, tran_line_item_type_id, tran_line_item_desc, tran_line_item_batch_type_cd)
            VALUES(l_tran_id, l_host_id, 301, payload_rcd(3), 'A');
        END IF;
        
        IF l_cnt < 5 THEN
            RETURN;
        ELSIF payload_rcd(4) IS NOT NULL AND payload_rcd(5) IS NOT NULL THEN -- Usage Minutes and Charge
            INSERT INTO tran_line_item(tran_id, host_id, tran_line_item_type_id, tran_line_item_desc, tran_line_item_quantity, tran_line_item_amount, tran_line_item_batch_type_cd)
            VALUES(l_tran_id, l_host_id, 302, 'Usage Minutes', TO_NUMBER(payload_rcd(4)), TO_NUMBER(payload_rcd(5)) / 100, 'A');
        END IF;
        
        IF l_cnt < 7 THEN
            RETURN;
        ELSIF payload_rcd(6) IS NOT NULL AND payload_rcd(7) IS NOT NULL THEN -- Printer 1 Count and Charge
            INSERT INTO tran_line_item(tran_id, host_id, tran_line_item_type_id, tran_line_item_desc, tran_line_item_quantity, tran_line_item_amount, tran_line_item_batch_type_cd)
            VALUES(l_tran_id, l_host_id, 303, 'Printer 1 Count', TO_NUMBER(payload_rcd(6)), TO_NUMBER(payload_rcd(7)) / 100, 'A');
        END IF;
        
        IF l_cnt < 9 THEN
            RETURN;
        ELSIF payload_rcd(8) IS NOT NULL AND payload_rcd(9) IS NOT NULL THEN -- Printer 2 Count and Charge
            INSERT INTO tran_line_item(tran_id, host_id, tran_line_item_type_id, tran_line_item_desc, tran_line_item_quantity, tran_line_item_amount, tran_line_item_batch_type_cd)
            VALUES(l_tran_id, l_host_id, 304, 'Printer 2 Count', TO_NUMBER(payload_rcd(8)), TO_NUMBER(payload_rcd(9)) / 100, 'A');
        END IF;
        
        IF l_cnt < 11 THEN
            RETURN;
        ELSIF payload_rcd(10) IS NOT NULL AND payload_rcd(11) IS NOT NULL THEN -- Service 1 Count and Charge
            INSERT INTO tran_line_item(tran_id, host_id, tran_line_item_type_id, tran_line_item_desc, tran_line_item_quantity, tran_line_item_amount, tran_line_item_batch_type_cd)
            VALUES(l_tran_id, l_host_id, 305, 'Service 1 Count', TO_NUMBER(payload_rcd(10)), TO_NUMBER(payload_rcd(11)) / 100, 'A');
        END IF;
        
        IF l_cnt < 13 THEN
            RETURN;
        ELSIF payload_rcd(12) IS NOT NULL AND payload_rcd(13) IS NOT NULL THEN -- Service 2 Count and Charge
            INSERT INTO tran_line_item(tran_id, host_id, tran_line_item_type_id, tran_line_item_desc, tran_line_item_quantity, tran_line_item_amount, tran_line_item_batch_type_cd)
            VALUES(l_tran_id, l_host_id, 306, 'Service 2 Count', TO_NUMBER(payload_rcd(12)), TO_NUMBER(payload_rcd(13)) / 100, 'A');
        END IF;
        
        IF l_cnt < 15 THEN
            RETURN;
        ELSIF payload_rcd(14) IS NOT NULL AND payload_rcd(15) IS NOT NULL THEN -- Service 3 Count and Charge
            INSERT INTO tran_line_item(tran_id, host_id, tran_line_item_type_id, tran_line_item_desc, tran_line_item_quantity, tran_line_item_amount, tran_line_item_batch_type_cd)
            VALUES(l_tran_id, l_host_id, 307, 'Service 3 Count', TO_NUMBER(payload_rcd(14)), TO_NUMBER(payload_rcd(15)) / 100, 'A');
        END IF;
        
        IF l_cnt < 17 THEN
            RETURN;
        ELSIF payload_rcd(16) IS NOT NULL AND payload_rcd(17) IS NOT NULL THEN -- Service 4 Count and Charge
            INSERT INTO tran_line_item(tran_id, host_id, tran_line_item_type_id, tran_line_item_desc, tran_line_item_quantity, tran_line_item_amount, tran_line_item_batch_type_cd)
            VALUES(l_tran_id, l_host_id, 308, 'Service 4 Count', TO_NUMBER(payload_rcd(16)), TO_NUMBER(payload_rcd(17)) / 100, 'A');
        END IF;
        
        IF l_cnt < 19 THEN
            RETURN;
        ELSIF payload_rcd(18) IS NOT NULL AND payload_rcd(19) IS NOT NULL THEN -- Service 5 Count and Charge
            INSERT INTO tran_line_item(tran_id, host_id, tran_line_item_type_id, tran_line_item_desc, tran_line_item_quantity, tran_line_item_amount, tran_line_item_batch_type_cd)
            VALUES(l_tran_id, l_host_id, 309, 'Service 5 Count', TO_NUMBER(payload_rcd(18)), TO_NUMBER(payload_rcd(19)) / 100, 'A');
        END IF;
        
        IF l_cnt < 21 THEN
            RETURN;
        ELSIF payload_rcd(20) IS NOT NULL AND payload_rcd(21) IS NOT NULL THEN -- Service 6 Count and Charge
            INSERT INTO tran_line_item(tran_id, host_id, tran_line_item_type_id, tran_line_item_desc, tran_line_item_quantity, tran_line_item_amount, tran_line_item_batch_type_cd)
            VALUES(l_tran_id, l_host_id, 310, 'Service 6 Count', TO_NUMBER(payload_rcd(20)), TO_NUMBER(payload_rcd(21)) / 100, 'A');
        END IF;
    ELSIF l_payload_code = 'A1' AND l_cnt > 1 THEN -- short format
        IF l_cnt = 1 THEN
            RETURN;
        ELSIF payload_rcd(2) IS NOT NULL THEN -- Usage Minutes
            INSERT INTO tran_line_item(tran_id, host_id, tran_line_item_type_id, tran_line_item_desc, tran_line_item_quantity, tran_line_item_batch_type_cd)
            VALUES(l_tran_id, l_host_id, 302, 'Usage Minutes', TO_NUMBER(payload_rcd(2)), 'A');
        END IF;

        IF l_cnt = 2 THEN
            RETURN;
        ELSIF payload_rcd(3) IS NOT NULL THEN -- Printer 1 Count
            INSERT INTO tran_line_item(tran_id, host_id, tran_line_item_type_id, tran_line_item_desc, tran_line_item_quantity, tran_line_item_batch_type_cd)
            VALUES(l_tran_id, l_host_id, 303, 'Printer 1 Count', TO_NUMBER(payload_rcd(3)), 'A');
        END IF;

        IF l_cnt = 3 THEN
            RETURN;
        ELSIF payload_rcd(4) IS NOT NULL THEN -- Printer 2 Count
            INSERT INTO tran_line_item(tran_id, host_id, tran_line_item_type_id, tran_line_item_desc, tran_line_item_quantity, tran_line_item_batch_type_cd)
            VALUES(l_tran_id, l_host_id, 304, 'Printer 2 Count', TO_NUMBER(payload_rcd(4)), 'A');
        END IF;

        IF l_cnt = 4 THEN
            RETURN;
        ELSIF payload_rcd(5) IS NOT NULL THEN -- Service 1 Count
            INSERT INTO tran_line_item(tran_id, host_id, tran_line_item_type_id, tran_line_item_desc, tran_line_item_quantity, tran_line_item_batch_type_cd)
            VALUES(l_tran_id, l_host_id, 305, 'Service 1 Count', TO_NUMBER(payload_rcd(5)), 'A');
        END IF;

        IF l_cnt = 5 THEN
            RETURN;
        ELSIF payload_rcd(6) IS NOT NULL THEN -- Service 2 Count
            INSERT INTO tran_line_item(tran_id, host_id, tran_line_item_type_id, tran_line_item_desc, tran_line_item_quantity, tran_line_item_batch_type_cd)
            VALUES(l_tran_id, l_host_id, 306, 'Service 2 Count', TO_NUMBER(payload_rcd(6)), 'A');
        END IF;

        IF l_cnt = 6 THEN
            RETURN;
        ELSIF payload_rcd(7) IS NOT NULL THEN -- Service 3 Count
            INSERT INTO tran_line_item(tran_id, host_id, tran_line_item_type_id, tran_line_item_desc, tran_line_item_quantity, tran_line_item_batch_type_cd)
            VALUES(l_tran_id, l_host_id, 307, 'Service 3 Count', TO_NUMBER(payload_rcd(7)), 'A');
        END IF;

        IF l_cnt = 7 THEN
            RETURN;
        ELSIF payload_rcd(8) IS NOT NULL THEN -- Service 4 Count
            INSERT INTO tran_line_item(tran_id, host_id, tran_line_item_type_id, tran_line_item_desc, tran_line_item_quantity, tran_line_item_batch_type_cd)
            VALUES(l_tran_id, l_host_id, 308, 'Service 4 Count', TO_NUMBER(payload_rcd(8)), 'A');
        END IF;

        IF l_cnt = 8 THEN
            RETURN;
        ELSIF payload_rcd(9) IS NOT NULL THEN -- Service 5 Count
            INSERT INTO tran_line_item(tran_id, host_id, tran_line_item_type_id, tran_line_item_desc, tran_line_item_quantity, tran_line_item_batch_type_cd)
            VALUES(l_tran_id, l_host_id, 309, 'Service 5 Count', TO_NUMBER(payload_rcd(9)), 'A');
        END IF;

        IF l_cnt = 9 THEN
            RETURN;
        ELSIF payload_rcd(10) IS NOT NULL THEN -- Service 6 Count
            INSERT INTO tran_line_item(tran_id, host_id, tran_line_item_type_id, tran_line_item_desc, tran_line_item_quantity, tran_line_item_batch_type_cd)
            VALUES(l_tran_id, l_host_id, 310, 'Service 6 Count', TO_NUMBER(payload_rcd(10)), 'A');
        END IF;
    END IF;
EXCEPTION
    WHEN OTHERS THEN
        NULL;
END;
/

COMMIT;
