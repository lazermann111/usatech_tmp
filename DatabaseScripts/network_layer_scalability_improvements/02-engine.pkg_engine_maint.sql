CREATE OR REPLACE 
PACKAGE BODY        pkg_engine_maint IS

   -- Logging contants
   cs_server_name         CONSTANT VARCHAR2 (30)                    := 'USADBP';
   cs_package_name        CONSTANT VARCHAR2 (30)                    := 'PKG_ENGINE_MAINT';
   -- Exception stuff
   vs_error_msg           app_exec_hist.exception_data.additional_information%TYPE;

   PROCEDURE sp_start_net_layer
   (
      pn_stored_net_layer_id IN net_layer.net_layer_id%TYPE,
      ps_net_layer_desc IN net_layer.net_layer_desc%TYPE,
      pn_new_net_layer_id OUT net_layer.net_layer_id%TYPE
   )
   AS
      cs_procedure_name             CONSTANT VARCHAR2 (50)                  := 'sp_start_net_layer';
      vn_net_layer_active_yn_flag   net_layer.net_layer_active_yn_flag%TYPE;

   BEGIN
      IF pn_stored_net_layer_id IS NULL OR pn_stored_net_layer_id <= 0 THEN
         SELECT SEQ_NET_LAYER_ID.NEXTVAL INTO pn_new_net_layer_id FROM dual;
         INSERT INTO net_layer(net_layer_id, net_layer_desc, net_layer_active_yn_flag) VALUES (pn_new_net_layer_id,ps_net_layer_desc, 'Y');

      ELSE
         SELECT net_layer.net_layer_active_yn_flag
         INTO vn_net_layer_active_yn_flag
         FROM net_layer
         WHERE net_layer.net_layer_id = pn_stored_net_layer_id;

         IF vn_net_layer_active_yn_flag = 'Y' THEN
            UPDATE net_layer
            SET net_layer_active_yn_flag = 'N'
            WHERE net_layer_id = pn_stored_net_layer_id;
         END IF;

         UPDATE net_layer
         SET net_layer_active_yn_flag = 'Y',
             net_layer_desc = ps_net_layer_desc
         WHERE net_layer.net_layer_id = pn_stored_net_layer_id;

         pn_new_net_layer_id := pn_stored_net_layer_id;
      END IF;

   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         SELECT SEQ_NET_LAYER_ID.NEXTVAL INTO pn_new_net_layer_id FROM dual;
         INSERT INTO net_layer(net_layer_id, net_layer_desc, net_layer_active_yn_flag) VALUES (pn_new_net_layer_id, ps_net_layer_desc, 'Y');

      WHEN OTHERS THEN
         vs_error_msg :=
               'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pkg_exception_processor.sp_log_exception
                                   (pkg_app_exec_hist_globals.unknown_error_id,
                                    vs_error_msg,
                                    cs_server_name,
                                    cs_package_name || '.' || cs_procedure_name
                                   );
    END;


   PROCEDURE sp_stop_net_layer
   (
      pn_stored_net_layer_id IN net_layer.net_layer_id%TYPE
   )
   AS
      cs_procedure_name             CONSTANT VARCHAR2 (50)                  := 'sp_stop_net_layer';

   BEGIN
      UPDATE net_layer
      SET net_layer.net_layer_active_yn_flag = 'N'
      WHERE net_layer.net_layer_id = pn_stored_net_layer_id;

   EXCEPTION
      WHEN OTHERS THEN
         vs_error_msg :=
               'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pkg_exception_processor.sp_log_exception
                                   (pkg_app_exec_hist_globals.unknown_error_id,
                                    vs_error_msg,
                                    cs_server_name,
                                    cs_package_name || '.' || cs_procedure_name
                                   );
   END;


   PROCEDURE sp_start_device_session
   (
      pn_net_layer_id IN net_layer_device_session.net_layer_id%TYPE,
      ps_device_name IN net_layer_device_session.device_name%TYPE,
      ps_client_ip_addr IN net_layer_device_session.session_client_ip_addr%TYPE,
      ps_client_port IN net_layer_device_session.session_client_port%TYPE,
      pn_session_id OUT net_layer_device_session.session_id%TYPE
   )
   AS
      cs_procedure_name             CONSTANT VARCHAR2 (50)                  := 'sp_start_device_session';
      v_device_type_id    device.device_type_id%TYPE;

   BEGIN
   
      SELECT seq_net_layer_device_sess_id.NEXTVAL
        INTO pn_session_id
        FROM dual;
        
      INSERT INTO net_layer_device_session
      (
         session_id,
         net_layer_id,
         device_name,
         session_client_ip_addr,
         session_client_port
      )
      VALUES
      (
         pn_session_id,
         pn_net_layer_id,
         ps_device_name,
         ps_client_ip_addr,
         ps_client_port
      );
     
   END;

   PROCEDURE sp_stop_device_session
   (
      pn_session_id IN net_layer_device_session.session_id%TYPE,
      pn_inbound_msg_cnt IN net_layer_device_session.session_inbound_msg_cnt%TYPE,
      pn_outbound_msg_cnt IN net_layer_device_session.session_outbound_msg_cnt%TYPE,
      pn_inbound_byte_cnt IN net_layer_device_session.session_inbound_byte_cnt%TYPE,
      pn_outbound_byte_cnt IN net_layer_device_session.session_outbound_byte_cnt%TYPE
   )
   AS
      cs_procedure_name             CONSTANT VARCHAR2 (50)                  := 'sp_start_device_session';

   BEGIN
      UPDATE net_layer_device_session
      SET session_active_yn_flag = 'N',
      session_inbound_msg_cnt = pn_inbound_msg_cnt,
      session_outbound_msg_cnt = pn_outbound_msg_cnt,
      session_inbound_byte_cnt = pn_inbound_byte_cnt,
      session_outbound_byte_cnt = pn_outbound_byte_cnt
      WHERE session_id = pn_session_id;

   EXCEPTION
      WHEN OTHERS THEN
         vs_error_msg :=
               'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pkg_exception_processor.sp_log_exception
                                   (pkg_app_exec_hist_globals.unknown_error_id,
                                    vs_error_msg,
                                    cs_server_name,
                                    cs_package_name || '.' || cs_procedure_name
                                   );
   END;
END;
/