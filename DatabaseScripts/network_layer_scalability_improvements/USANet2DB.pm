package USANet2DB;

use strict;

use IO::Select;
use USAT::NetLayer::Database;	# Access to the Database
use USAT::Database::SensitiveData;
use USAT::NetLayer::Session;
use Evend::ReRix::Shared qw(MakeHex MakeString getcrc16s getcrc16i uuencode uudecode);
use USAT::NetLayer::Utils qw(getPacket SecureLogIt);

sub Run ($$$$$$)
{
	my $sock = shift;
	$| = 1;

	my $dbconfig = shift;
	my $net_layer_id = shift;
	my $initBuffer = shift;
	my $logLevel = shift;
	my $debugFile = shift;
	
	my $DATABASE = USAT::NetLayer::Database->new
	(
		config 				=> $dbconfig,
		log_level 			=> $logLevel,
		debug_file_handle	=> $debugFile
	);
	
	my $add_init_buffer = 1;

	my $remote_addr = $sock->peerhost;
	my $port = $sock->peerport;
	my $call_id = "$remote_addr:$port";

	my $session_id;
	my $outbound_command_sth;
	my $started_log = 'N';
	my $in_msg_count = 0;
	my $out_msg_count = 0;
	my $in_byte_count = 0;
	my $out_byte_count = 0;

	my $TIMEOUT = 45;
	my $machine_id;
	my $ssn;
	my $unpackedkey;
	my $device_type = 4;

	my $rh_set;
	my $wh_set;
	my $rh;
	my $wh;
	my $rerix;
	my $msg_no;

	my $read_set = new IO::Select();
	my $write_set = new IO::Select();
	$read_set->add($sock);

	my $next_command_check = time() + 2;
	my $last_command_resend;
	my $outBuffer;
	my $inBuffer;
	my $outCommand;
	my $outCommandID;
	my $watchdog = time;	
	my $watchdog_valid_inbound_msg = time();

	SecureLogIt("$call_id\t" . "Connected from $remote_addr:$port\t" . "logLevel: $logLevel", $logLevel, $debugFile);
	while(1)
	{
		my $select_t;

		if (defined $outCommandID)
		{
			$select_t = ($last_command_resend - time()) > 0 ? $last_command_resend - time() : 0;
		}
		else
		{
			$select_t = ($next_command_check - time()) > 0 ? $next_command_check - time() : 0;
		}

		($rh_set, $wh_set) = IO::Select->select($read_set, $write_set, undef, $select_t);

		if ($watchdog + $TIMEOUT < time)
		{
			SecureLogIt ("Timeout " . localtime(), $logLevel, $debugFile);
			USAT::NetLayer::Session::finish($DATABASE, $session_id, $in_msg_count, $out_msg_count, $in_byte_count, $out_byte_count) if ($started_log eq 'Y');
			$sock->close();
			return;
		}

		foreach $rh (@$rh_set)
		{
			my $tempBuffer;
			$watchdog = time;
			$rh->recv($tempBuffer, 512);
			
			if ($add_init_buffer)
			{
				$tempBuffer = $initBuffer . $tempBuffer;
				$add_init_buffer = 0;
			}			

			if (length($tempBuffer) == 0)
			{
				SecureLogIt ("Connection Lost " . localtime(), $logLevel, $debugFile);
				USAT::NetLayer::Session::finish($DATABASE, $session_id, $in_msg_count, $out_msg_count, $in_byte_count, $out_byte_count) if ($started_log eq 'Y');
				$sock->close();
				return;
			}
			else
			{
				$inBuffer .= $tempBuffer;

				my $packet;
				my $result;
				my $message_size;
				my $loopcnt = 0;
				
				SecureLogIt ("inBuffer hex: ", $logLevel, $debugFile, unpack('H*', $inBuffer));
				SecureLogIt ("Got data... resetting inactivity timer to zero.", $logLevel, $debugFile);
				$watchdog_valid_inbound_msg = time();

				my $hangup_received = &check_hangup($inBuffer);
				if($hangup_received)
				{
					SecureLogIt("Caught hangup string ($hangup_received)... closing the connection.", $logLevel, $debugFile);
					USAT::NetLayer::Session::finish($DATABASE, $session_id, $in_msg_count, $out_msg_count, $in_byte_count, $out_byte_count) if ($started_log eq 'Y');
					$sock->close();
					return;
				}

				# 64-bit decodes the inbound the packet
				($packet, $inBuffer) = getPacket($inBuffer);
				if(not defined $packet)
				{
					SecureLogIt "Received short or invalid message... ending connection!", $logLevel, $debugFile;
					$sock->close();
					return;
				}
				$message_size = length($packet);
				$packet = uudecode($packet);
				SecureLogIt ("decoded inBuffer hex: ", $logLevel, $debugFile, unpack('H*', $packet));

				# the first 8 bytes of the uudecoded packet contains the SSN; use that to decrypt the packet
				$ssn = uc(unpack("H*",substr($packet, 0, 8)));

				if ($ssn !~ /^[0-9A-F]{8}(63){4}$/)
				{
					SecureLogIt("Received invalid serial number... ending connection", $logLevel, $debugFile);
					USAT::NetLayer::Session::finish($DATABASE, $session_id, $in_msg_count, $out_msg_count, $in_byte_count, $out_byte_count) if ($started_log eq 'Y');
					$sock->close();
					return;
				}

				SecureLogIt ("decryption key = $ssn", $logLevel, $debugFile);				
								
				if($started_log eq 'N')
				{
					# get the machine id 
					my $array_ref = $DATABASE->select(
						table			=> 'device',
						select_columns	=> 'device_name',
						order			=> 'device_active_yn_flag desc',
						where_columns	=> ['device_type_id=?', 'device_serial_cd=?'],
						where_values	=> [$device_type, $ssn]
					);

					if (defined $array_ref->[0])
					{
						$machine_id = $array_ref->[0][0];
						SecureLogIt("DB machine_id for $ssn is $machine_id", $logLevel, $debugFile);
					}
					else
					{
						$array_ref = $DATABASE->select(
						table			=> 'dual',
						select_columns	=> 'tazdba.rerix_machine_id');

						$machine_id = $array_ref->[0][0];					
						SecureLogIt("Generated new machine_id: $machine_id", $logLevel, $debugFile);

						my $key = '';						
						for (my $i=0; $i<16; $i++)
						{
							$key .= int(rand(10));
						}

						$DATABASE->insert(
							table			=> 'device',
							insert_columns	=> 'device_name, device_type_id, device_serial_cd, device_active_yn_flag, encryption_key',
							insert_values	=> [$machine_id, $device_type, $ssn, 'Y', $key] );						
					}
						
					SecureLogIt ("SSN# '" . $ssn . "', Machine ID# " . $machine_id, $logLevel, $debugFile);
				
					USAT::NetLayer::Session::start($DATABASE, \$session_id, $net_layer_id, $machine_id, $remote_addr, $port);
					$started_log = 'Y';
				}

				$in_msg_count++;
				$in_byte_count += $message_size;

				# decrypt the packet
				SecureLogIt ("about to unpack the data...", $logLevel, $debugFile);
				SecureLogIt ("packet (hex) = ", $logLevel, $debugFile, unpack("H*", substr($packet,8)));
				SecureLogIt ("key          = $ssn", $logLevel, $debugFile);
				($result, $packet) = @{UnpackEncrypted(substr($packet,8), $ssn)};
				SecureLogIt ("result       = $result", $logLevel, $debugFile);
				SecureLogIt ("packet (hex) = ", $logLevel, $debugFile, unpack("H*", $packet));				

				if ($result == 0)
				{
					SecureLogIt "Decryption failure for SSN " . $ssn, $logLevel, $debugFile;
				}
				
				while (length($packet))
				{
					my $msgnbr = substr($packet, 0, 1);
					SecureLogIt "*** New Command Received ***", $logLevel, $debugFile;
					SecureLogIt "The packet hex is       : ", $logLevel, $debugFile, unpack("H*", $packet);
					SecureLogIt "The msgnbr byte hex is  : " . unpack("H*", substr($packet, 0, 1)), $logLevel, $debugFile;
					SecureLogIt "The command byte hex is : " . unpack("H*", substr($packet, 1, 1)), $logLevel, $debugFile;
					SecureLogIt "The inBuffer hex is     : ", $logLevel, $debugFile, unpack("H*", $inBuffer);
					SecureLogIt "The inBuffer length is  : " . length($inBuffer), $logLevel, $debugFile;
					
					$rerix = unpack("H*", substr($packet,1));
					$msg_no = ord(substr($packet,0,1));
					if (defined $machine_id)
					{
						SecureLogIt ("About to insert command into database...", $logLevel, $debugFile);
						$DATABASE->insert(
							table			=>'machine_command_inbound',
							insert_columns	=> 'machine_id, inbound_command, inbound_msg_no, session_id',
							insert_values	=> [
								$machine_id,
								USAT::Database::SensitiveData->new($rerix, '*' x length $rerix),
								$msg_no,
								$session_id
							]
						);
						SecureLogIt ("...insert complete", $logLevel, $debugFile);
					}
					else
					{
						SecureLogIt "ReRix sent without identifying, closing", $logLevel, $debugFile;
						USAT::NetLayer::Session::finish($DATABASE, $session_id, $in_msg_count, $out_msg_count, $in_byte_count, $out_byte_count) if ($started_log eq 'Y');
						$sock->close();
						return;
					}
					
					$packet = '';
					
					if ($loopcnt++ > 2)
					{
						SecureLogIt ("Exceeded loopcnt... exiting loop", $logLevel, $debugFile);
						last;
					}
				}
			}
			$next_command_check = 0;
		}

		foreach $wh (@$wh_set)
		{
			SecureLogIt ("Sending reply to client (hex): " . unpack("H*", $outBuffer), $logLevel, $debugFile);
			my $sent = $wh->send($outBuffer);

			$out_msg_count++;
			$out_byte_count += $sent;

			$watchdog = time;

			SecureLogIt ("Sent (hex): " . unpack("H*", substr($outBuffer, 0, $sent)), $logLevel, $debugFile);
			$outBuffer = substr $outBuffer, $sent;

			# remove it from the select()
			if (length( $outBuffer ) == 0)
			{
				$write_set->remove( $wh );
			}
		}

		if (($next_command_check <= time()) and (!defined $outCommandID) and (defined $machine_id))
		{
			$next_command_check = time() + 0.25;

			$DATABASE->{print_query} = 1;
			SecureLogIt "Checking machine_command for $machine_id", $logLevel, $debugFile;

			my $commandref = $DATABASE->select(
				table			=> 'Machine_Command',
				select_columns	=> 'COMMAND_ID, MODEM_ID, COMMAND, Execute_Cd',
				order			=> 'Command_ID',
				where_columns	=> ['Execute_cd = ?', 'modem_id = ?'],
				where_values	=> ['N', $machine_id]
			);

			$DATABASE->{print_query} = 1;

			if (defined $commandref->[0])
			{
				my ($command_id, $modem_id, $command, $execute_cd) = @{$commandref->[0]};				

				SecureLogIt "Sending $command_id: " . unpack("H*", $command), $logLevel, $debugFile;

				$outCommand = pack('H*' ,$command);
				$outCommand = PackEncrypted($outCommand, $ssn);
				SecureLogIt "The unpacked encrypted data: " . unpack("H*", $outCommand), $logLevel, $debugFile;

				$outCommand = uuencode($outCommand) . "\x0a";
				SecureLogIt "The unpacked encoded data  : " . unpack("H*", $outCommand), $logLevel, $debugFile;

				$outBuffer .= $outCommand;
				$write_set->add( $sock );
				$last_command_resend = time() + 5;

				$DATABASE->update(
					table			=> 'Machine_Command',
					update_columns	=> 'Execute_Cd',
					update_values	=> ['Y'],
					where_columns	=> ['Command_ID = ?'],
					where_values	=> [$command_id]
				);
			}
		}
		elsif ($next_command_check <= time())
		{
			$next_command_check = time() + 0.25;
		}
		
		if ($watchdog_valid_inbound_msg + $TIMEOUT < time())
		{
			SecureLogIt("Exceeded inactivity timeout... closing the connection.", $logLevel, $debugFile);
			USAT::NetLayer::Session::finish($DATABASE, $session_id, $in_msg_count, $out_msg_count, $in_byte_count, $out_byte_count) if ($started_log eq 'Y');
			$sock->close();
			return;
		}
		else
		{
			sleep 0.1;
		}
	}
		
}

sub check_hangup ($)
{
	my $inBuffer = shift;

	if(not defined $inBuffer)
	{
		return undef;
	}

	if($inBuffer =~ /^\+\+\+/)
	{
		# standard +++
		return '+++';
	}
	elsif($inBuffer =~ /^AT\x0d/)
	{
		# AnyDATA CDMA modem
		return "41540d";
	}
	elsif($inBuffer =~ /\x9e\x86\x9e\x86\x9e\x86/)
	{
		# AS5300 
		return '9e869e869e86';
	}
	elsif($inBuffer =~ /^AT#CONNECTIONSTOP/)
	{
		# Multitech GSM/GPRS modem
		return 'AT#CONNECTIONSTOP';
	}
	
	return undef;
}

sub PackEncrypted ($$)
{
	my ($message, $ssn) = @_;
	my ($key, $len, $crc, @list_key, $tea);

	# Got the number now build a key in the right order
	$key = &MakeString($ssn);
	@list_key =( 
		(ord(substr($key,3,1))<<24) | 
		(ord(substr($key,2,1))<<16) | 
		(ord(substr($key,1,1))<<8) | 
		ord(substr($key,0,1))
		,
		(ord(substr($key,7,1))<<24) | 
		(ord(substr($key,6,1))<<16) | 
		(ord(substr($key,5,1))<<8) | 
		ord(substr($key,4,1))
	);
	# The Key has the duplicated part
	push @list_key, @list_key;

	# set the key
	$tea = Evend::Crypt::TEA->new(Key=>\@list_key);
	# get the length of the message as a character
	$len = chr(length $message);
	# Compute the CRC16 of the message
	$crc = getcrc16s($message);
	# build the message with the length and the crc
	$message = $len . $message . $crc;
	# Do magic
	return $tea->EncodeStream($message);
}

sub UnpackEncrypted ($$)
{
	my ($message, $ssn) = @_;
	my ($key, $len, $crc, $ccrc, @list_key, $tea, $out);

	# Got the number now build a key in the right order
	$key = &MakeString($ssn);
	@list_key =( 
		(ord(substr($key,3,1))<<24) | 
		(ord(substr($key,2,1))<<16) | 
		(ord(substr($key,1,1))<<8) | 
		ord(substr($key,0,1))
		,
		(ord(substr($key,7,1))<<24) | 
		(ord(substr($key,6,1))<<16) | 
		(ord(substr($key,5,1))<<8) | 
		ord(substr($key,4,1))
	);
	# The Key has the duplicated part
	push @list_key, @list_key;

	# set the key
	$tea = Evend::Crypt::TEA->new(Key=>\@list_key);
	$out = $tea->DecodeStream($message);

	$len = ord($out);
	$out = substr($out,1);
	$crc = substr($out,$len,2);
	$out = substr($out,0,$len);
	$ccrc = getcrc16s($out);

	return [($crc eq $ccrc)?1:0, $out];
}


1;
