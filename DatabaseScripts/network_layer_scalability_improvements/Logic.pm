package Logic;

use strict;

use IO::Select;
use Time::HiRes qw(sleep);
use USAT::NetLayer::Database;        # Access to the Database
use USAT::Database::SensitiveData;
use USAT::NetLayer::Session;
use Evend::ReRix::Shared qw(getcrc16s uuencode uudecode);
use USAT::NetLayer::Utils qw(getPacket SecureLogIt get_random_int);
use USAT::Common::CallInRecord;
use Math::Rand48 qw(nrand48);

my $DEFAULT_MACHINE_ID = "EV000000";
my $DEFAULT_ENCRYPTION_KEY = "9265027739274401";
my $TEMP_ID_PREFIX = "EV0002";

my $END_bug_device_list = '/opt/USANet3/END_bug_device_list';
my @END_bug_device_list;
 
sub Run ($$$$$$)
{
	my $sock = shift;
	$| = 1;

	my $dbconfig = shift;
	my $net_layer_id = shift;
	my $initBuffer = shift;
	my $logLevel = shift;
	my $debugFile = shift;

	my $DATABASE = USAT::NetLayer::Database->new
	(
		config 				=> $dbconfig,
		log_level 			=> $logLevel,
		debug_file_handle	=> $debugFile
	);

	my $add_init_buffer = 1;
	
	my $remote_addr = $sock->peerhost;
	my $port = $sock->peerport;
	my $call_id = "$remote_addr:$port";

	my $session_id;
	my $outbound_command_sth;
	my $started_log = 'N';
	my $in_msg_count = 0;
	my $out_msg_count = 0;
	my $in_byte_count = 0;
	my $out_byte_count = 0;
		
	my $TIMEOUT = 45;

	my $rh_set;
	my $wh_set;

	my $rh;
	my $wh;

	my $read_set = new IO::Select();
	my $write_set = new IO::Select();
	$read_set->add($sock);

	my $next_command_check = time() + 2;
	my $last_command_resend;

	my $watchdog = time;
	my $watchdog_valid_inbound_msg = time;

	my $inBuffer;
	my $outBuffer;

	my $machine_id;
	my $temp_machine_id;
	my $key;
	my $ssn;
	
	my $out_command_id;	
	my $read_incomplete_count = 0;
	my $received_line_noise = 0;
	
	@END_bug_device_list = load_end_bug_device_list();

	SecureLogIt("$call_id\t" . "Connected from $remote_addr:$port\t" . "logLevel: $logLevel", $logLevel, $debugFile);
	while(1)
	{
		my $select_t;

		if(defined $out_command_id)
		{
			$select_t = ($last_command_resend - time()) > 0 ? $last_command_resend - time() : 0;
		}
		else
		{
			$select_t = ($next_command_check - time()) > 0 ? $next_command_check - time() : 0;
		}

		($rh_set, $wh_set) = IO::Select->select($read_set, $write_set, undef, $select_t);

		if($watchdog + $TIMEOUT < time)
		{
			SecureLogIt("$call_id\t" . "Timeout " . localtime(), $logLevel, $debugFile);
			if(defined $machine_id)
			{
				&USAT::Common::CallInRecord::finish($DATABASE, $machine_id);
				USAT::NetLayer::Session::finish($DATABASE, $session_id, $in_msg_count, $out_msg_count, $in_byte_count, $out_byte_count) if($started_log eq 'Y');
			}
			$sock->close();
			return;
		}

		foreach $rh (@$rh_set)
		{
			my ($tempBuffer, $tempBufferLen);

			$watchdog = time;

			$rh->recv($tempBuffer, 2048);
			
			if ($add_init_buffer)
			{
				$tempBuffer = $initBuffer . $tempBuffer;
				$add_init_buffer = 0;
			}
			
			$tempBufferLen = length($tempBuffer);
			
			SecureLogIt("$call_id\t" . "Read $tempBufferLen bytes.", $logLevel, $debugFile);

			if($tempBufferLen == 0)
			{
				SecureLogIt("$call_id\t" . "Connection Lost " . localtime(), $logLevel, $debugFile);
				&USAT::Common::CallInRecord::finish($DATABASE, $machine_id);
				USAT::NetLayer::Session::finish($DATABASE, $session_id, $in_msg_count, $out_msg_count, $in_byte_count, $out_byte_count) if($started_log eq 'Y');
				$sock->close();
				return;
			}
			else
			{
				# filter out high ascii to attempt to compensate for line-noise
				my $high_ascii_buf = '';
				
				while ($tempBuffer =~ s/([\x80-\xff]+)//)
				{
					$high_ascii_buf .= $1;
				
					my $hangup_received1 = &check_hangup($1);
					if($hangup_received1)
					{
						SecureLogIt("$call_id\t" . "Filtered out " . length($high_ascii_buf) . " high ascii characters: "  . unpack('H*', $high_ascii_buf), $logLevel, $debugFile);
						SecureLogIt("$call_id\t" . "Caught hangup string ($hangup_received1)... closing the connection.", $logLevel, $debugFile);
						&USAT::Common::CallInRecord::finish($DATABASE, $machine_id) if($machine_id);
						USAT::NetLayer::Session::finish($DATABASE, $session_id, $in_msg_count, $out_msg_count, $in_byte_count, $out_byte_count) if($started_log eq 'Y');
						$sock->close();
						return;
					}
				}
				
				if (length($high_ascii_buf) > 0)
				{
					$received_line_noise = 1;
					SecureLogIt("$call_id\t" . "Filtered out " . length($high_ascii_buf) . " high ascii characters: "  . unpack('H*', $high_ascii_buf), $logLevel, $debugFile);
				}

				# filter out garbage data that some ePorts produce
				if ($tempBuffer =~ s/(.*)([\x0d][\x02][\x40][\x52].*)/$1/)
				{
					SecureLogIt("$call_id\t" . "Filtered out " . length($2) . " garbage characters: "  . unpack('H*',$2), $logLevel, $debugFile);

					my $hangup_received2 = &check_hangup($2);
					if($hangup_received2)
					{
						SecureLogIt("$call_id\t" . "Caught hangup string ($hangup_received2)... closing the connection.", $logLevel, $debugFile);
						&USAT::Common::CallInRecord::finish($DATABASE, $machine_id) if($machine_id);
						USAT::NetLayer::Session::finish($DATABASE, $session_id, $in_msg_count, $out_msg_count, $in_byte_count, $out_byte_count) if($started_log eq 'Y');
						$sock->close();
						return;
					}
				}

				$inBuffer .= $tempBuffer;
			}
		}

		my $data_valid = 1;
		while(defined $inBuffer && (length($inBuffer) > 0) && $data_valid)
		{
			SecureLogIt("$call_id\t" . ">---------------------------------------->", $logLevel, $debugFile);

			my $packet;
			my $result;
			my $message_size = 0;
			my $loopcnt = 0;
			
			SecureLogIt("$call_id\t" . "inBuffer hex: ", $logLevel, $debugFile, unpack('H*', $inBuffer));
			$watchdog_valid_inbound_msg = time();
			
			my $hangup_received = &check_hangup($inBuffer);
			if($hangup_received)
			{
				SecureLogIt("$call_id\t" . "Caught hangup string ($hangup_received)... closing the connection.", $logLevel, $debugFile);
				&USAT::Common::CallInRecord::finish($DATABASE, $machine_id);
				USAT::NetLayer::Session::finish($DATABASE, $session_id, $in_msg_count, $out_msg_count, $in_byte_count, $out_byte_count) if($started_log eq 'Y');
				$sock->close();
				return;
			}				
			
			# filter out garbage ascii to attempt to compensate for line-noise
			if ($inBuffer !~ m/^[\x31][\x35][\x38].*[\x0a]/) # hex 313538 is uuencoded EV
			{
				if ($inBuffer =~ s/(.+?)([\x31][\x35][\x38])(.*)([\x0a])(.*)/$2$3$4$5/s)
				{
					$received_line_noise = 1;
					SecureLogIt("$call_id\t" . "Filtered out " . length($1) . " garbage characters: "  . unpack('H*', $1), $logLevel, $debugFile);
				}			
			}

			# gets data until x0A
			($packet, $inBuffer) = getPacket($inBuffer);
			
			if((not defined $packet) || ($packet !~ m/^[\x31][\x35][\x38].*[\x0a]/))
			{			
				# if we get 0Ah but the packet doesn't start with hex 313538 after line noise filtering, it must be line noise, keep polling
				if(defined $packet)
				{
					$received_line_noise = 1;
					SecureLogIt("$call_id\t" . "Received packet delimiter with no EV, filtered out " . length($packet) . " garbage characters: "  . unpack('H*', $packet), $logLevel, $debugFile);
				}
			
				$read_incomplete_count++;
				if($read_incomplete_count > ($received_line_noise ? 30 : 8))
				{
					SecureLogIt("$call_id\t" . "Failed to receive complete packet in getPacket, max attempts reached... aborting!", $logLevel, $debugFile);
					&USAT::Common::CallInRecord::set_status($DATABASE, $machine_id, 'U');
					USAT::NetLayer::Session::finish($DATABASE, $session_id, $in_msg_count, $out_msg_count, $in_byte_count, $out_byte_count) if($started_log eq 'Y');
					$sock->close();
					return;
				}
				else
				{
					SecureLogIt("$call_id\t" . "Failed to receive complete packet in getPacket; attempt $read_incomplete_count", $logLevel, $debugFile);
					$data_valid = 0;
					next;
				}
			}
			
			SecureLogIt("$call_id\t" . "received packet: ", $logLevel, $debugFile, unpack("H*", $packet));
			SecureLogIt("$call_id\t" . length($inBuffer) . " bytes remaining in inBuffer", $logLevel, $debugFile);

			$message_size = length($packet);
			
			# do some simple validation in an effort to filter out junk data and line noise
			if($message_size < 16)
			{
				SecureLogIt("$call_id\t" . "Packet failed validation, message size = $message_size... aborting!", $logLevel, $debugFile);
				if(defined $machine_id)
				{
					&USAT::Common::CallInRecord::set_status($DATABASE, $machine_id, 'U');
					&USAT::Common::CallInRecord::finish($DATABASE, $machine_id);
					USAT::NetLayer::Session::finish($DATABASE, $session_id, $in_msg_count, $out_msg_count, $in_byte_count, $out_byte_count) if($started_log eq 'Y');
				}

				$sock->close();
				return;
			}
			
			$read_incomplete_count = 0;
			$packet = uudecode($packet);
				
			if(not defined $packet)
			{
				SecureLogIt("$call_id\t" . "Aborting... uudecode failed for: ", $logLevel, $debugFile, unpack("H*", $packet));
				if(defined $machine_id)
				{
					&USAT::Common::CallInRecord::set_status($DATABASE, $machine_id, 'U');
					&USAT::Common::CallInRecord::finish($DATABASE, $machine_id);
					USAT::NetLayer::Session::finish($DATABASE, $session_id, $in_msg_count, $out_msg_count, $in_byte_count, $out_byte_count) if($started_log eq 'Y');
				}
				
				$sock->close();
				return;
			}
			
			if(substr($packet, 0, 2) ne 'EV')
			{
				SecureLogIt("$call_id\t" . "Packet failed validation, does not start with EV... aborting!", $logLevel, $debugFile);
				if(defined $machine_id)
				{
					&USAT::Common::CallInRecord::set_status($DATABASE, $machine_id, 'U');
					&USAT::Common::CallInRecord::finish($DATABASE, $machine_id);
					USAT::NetLayer::Session::finish($DATABASE, $session_id, $in_msg_count, $out_msg_count, $in_byte_count, $out_byte_count) if($started_log eq 'Y');
				}
				
				$sock->close();
				return;				
			}
			
			SecureLogIt("$call_id\t" . "decoded packet hex: ", $logLevel, $debugFile, unpack('H*', $packet));
			#LogIt("$call_id\t" . "decoded packet str: '$packet'");

			# the first 8 bytes of the uudecoded packet contains the machine identifier
			$machine_id = substr($packet, 0, 8);
			SecureLogIt("$call_id\t" . "machine identifier: $machine_id", $logLevel, $debugFile);
			
			if(defined $machine_id && $started_log eq 'N')
			{
				if($machine_id ne $DEFAULT_MACHINE_ID)
				{
					&USAT::Common::CallInRecord::start($DATABASE, $machine_id, 'USANet3', "$remote_addr:$port");
				}
				
				USAT::NetLayer::Session::start($DATABASE, \$session_id, $net_layer_id, $machine_id, $remote_addr, $port);
				$started_log = 'Y';
			}
			
			&USAT::Common::CallInRecord::add_message($DATABASE, $machine_id, 'I', $message_size);
			$in_msg_count++;
			$in_byte_count += $message_size;
			
			# use the machine identifier to lookup the encryption key
			($key,$ssn) = &getKey($machine_id, $DATABASE, $call_id, $logLevel, $debugFile);
			if(not $key)
			{
				SecureLogIt("$call_id\t" . "No key found for $machine_id", $logLevel, $debugFile);
				&USAT::Common::CallInRecord::set_status($DATABASE, $machine_id, 'U');
				&USAT::Common::CallInRecord::finish($DATABASE, $machine_id);
				USAT::NetLayer::Session::finish($DATABASE, $session_id, $in_msg_count, $out_msg_count, $in_byte_count, $out_byte_count) if($started_log eq 'Y');
				$sock->close();
				return;
			}
			
			SecureLogIt("$call_id\t" . "Using encryption key: $key", $logLevel, $debugFile);
			
			# decrypt the packet
			#LogIt("$call_id\t" . "about to unpack the data...");
			($result, $packet) = @{UnpackEncrypted(substr($packet,8), $key, $logLevel, $debugFile, $call_id)};
			#LogIt("$call_id\t" . "Decrypt result = $result (0=BAD,1=GOOD)");
			#LogIt("$call_id\t" . "Decrypt packet = " . unpack("H*", $packet));
			
			my $decrypt_failure_handled = 0;
			if ($result == 0)
			{
				# END bug kludge - 06/02/2004
				$decrypt_failure_handled = &handle_decrypt_failure($DATABASE, $call_id, $packet, $machine_id, $ssn, $key, $logLevel, $debugFile);
				
				if(not $decrypt_failure_handled)
				{
					SecureLogIt("$call_id\t" . "Aborting... decrypt failed!", $logLevel, $debugFile);
					&USAT::Common::CallInRecord::set_status($DATABASE, $machine_id, 'U');
					&USAT::Common::CallInRecord::finish($DATABASE, $machine_id);
					USAT::NetLayer::Session::finish($DATABASE, $session_id, $in_msg_count, $out_msg_count, $in_byte_count, $out_byte_count) if($started_log eq 'Y');
					$sock->close();
					return;
				}
			}
			
			if(not $decrypt_failure_handled)
			{
				my $msg_no = substr($packet, 0, 1);
				
				SecureLogIt("$call_id\t" . "*** New Command Received ***", $logLevel, $debugFile);
				SecureLogIt("$call_id\t" . "The packet hex is       : ", $logLevel, $debugFile, unpack("H*", $packet));
				SecureLogIt("$call_id\t" . "The msg_no byte hex is  : " . unpack("H*", $msg_no), $logLevel, $debugFile);
				SecureLogIt("$call_id\t" . "The command type hex is : " . unpack("H*", substr($packet, 1, 1)), $logLevel, $debugFile);
				SecureLogIt("$call_id\t" . "The inBuffer hex is     : ", $logLevel, $debugFile, unpack("H*", $inBuffer));
				SecureLogIt("$call_id\t" . "The inBuffer length is  : " . length($inBuffer), $logLevel, $debugFile);
				
				my $rerix = unpack("H*", substr($packet,1));
				$msg_no = ord($msg_no);
				
				if(defined $machine_id)
				{
					# remove any existing responses for this ID
					
					# Removed 03/02/2006 - Flushing now done in stored proc called by USAT::NetLayer::Session::start
					#LogIt("$call_id\t" . "Flushing outbound command queue for $machine_id...");
					#$DATABASE->update(
					#			table			=> 'machine_command',
					#			update_columns	=> 'execute_cd',
					#			update_values	=> ['E'],
					#			where_columns  => ['modem_id = ?'],
					#			where_values   => [$machine_id]);
	
					if($machine_id eq $DEFAULT_MACHINE_ID)
					{
						# generate a random temp ID instead of putting the default ID in the database 
						# which causes a race condition
						$temp_machine_id = $TEMP_ID_PREFIX . get_random_int(10, 99, $remote_addr);
						SecureLogIt("$call_id\t" . "Using Temp ID : $temp_machine_id", $logLevel, $debugFile);
						
						# bug fix - remove any existing responses for this temp ID.  problem was created when an old 
						# message was sitting in the queue with the same temp EV number
						SecureLogIt("$call_id\t" . "Flushing outbound command queue for $temp_machine_id...", $logLevel, $debugFile);
						$DATABASE->update(
									table			=> 'machine_command',
									update_columns	=> 'execute_cd',
									update_values	=> ['E'],
									where_columns  => ['modem_id = ?'],
									where_values   => [$temp_machine_id]);
						
						SecureLogIt("$call_id\t" . "Inserting $temp_machine_id $msg_no ", $logLevel, $debugFile, $rerix);
						$DATABASE->insert(
									table			=> 'machine_command_inbound',
									insert_columns	=> 'machine_id, inbound_command, inbound_msg_no, session_id',
									insert_values	=> [
										$temp_machine_id,
										USAT::Database::SensitiveData->new($rerix, '*' x length $rerix),
										$msg_no,
										$session_id
									]);
					}
					else
					{
						if(defined $temp_machine_id)
						{
							SecureLogIt("$call_id\t" . "Resetting temp ID: $temp_machine_id", $logLevel, $debugFile);
							$temp_machine_id = undef;
						}
						
						SecureLogIt("$call_id\t" . "Inserting $machine_id $msg_no ", $logLevel, $debugFile, $rerix);
						$DATABASE->insert(
									table			=> 'machine_command_inbound',
									insert_columns	=> 'machine_id, inbound_command, inbound_msg_no, session_id',
									insert_values	=> [
										$machine_id, 
										USAT::Database::SensitiveData->new($rerix, '*' x length $rerix), 
										$msg_no, 
										$session_id
									]);
					}
				}
				else
				{
					SecureLogIt("$call_id\t" . "Message sent without machine identifier, aborting", $logLevel, $debugFile);
					USAT::NetLayer::Session::finish($DATABASE, $session_id, $in_msg_count, $out_msg_count, $in_byte_count, $out_byte_count) if($started_log eq 'Y');
					$sock->close();
					return;
				}
	
				$next_command_check = 0;
			}
		}
		
		foreach $wh (@$wh_set)
		{
			SecureLogIt("$call_id\t" . "Sending reply to client (hex): " . unpack("H*", $outBuffer), $logLevel, $debugFile);
			my $sent = $wh->send($outBuffer);
			
			&USAT::Common::CallInRecord::add_message($DATABASE, $machine_id, 'O', $sent);
			$out_msg_count++;
			$out_byte_count += $sent;

			$watchdog = time;

			$outBuffer = substr($outBuffer, $sent);

			# remove it from the select()
			if( length( $outBuffer ) == 0 )
			{
				$write_set->remove( $wh );
			}
		}
		
		if(($next_command_check <= time()) and (defined $machine_id))
		{
			$next_command_check = time() + 0.25;

			my $outbound_command_ref;

			if(defined $temp_machine_id)
			{
				#LogIt("$call_id\t" . "Checking machine_command for temp machine_id $temp_machine_id");
				$outbound_command_ref = $DATABASE->select(
					table			=> 'Machine_Command',
					select_columns	=> 'COMMAND_ID, COMMAND',
					order			=> 'Command_ID',
					where_columns	=> ['Execute_cd = ?', 'modem_id = ?'],
					where_values	=> ['N', $temp_machine_id],
					sth				=> \$outbound_command_sth
				);
			}
			else
			{
				#LogIt("$call_id\t" . "Checking machine_command for machine_id $machine_id");
				$outbound_command_ref = $DATABASE->select(
					table			=> 'Machine_Command',
					select_columns	=> 'COMMAND_ID, COMMAND',
					order			=> 'Command_ID',
					where_columns	=> ['Execute_cd = ?', 'modem_id = ?'],
					where_values	=> ['N', $machine_id],
					sth				=> \$outbound_command_sth
				);
			}
								
			if($outbound_command_ref->[0])
			{ 
				foreach my $outbound_row (@$outbound_command_ref)
				{
					SecureLogIt("$call_id\t" . "<----------------------------------------<", $logLevel, $debugFile);
					my $out_command;
					($out_command_id, $out_command) = @{$outbound_row};
	
					SecureLogIt("$call_id\t" . "Sending client:$machine_id id:$out_command_id cmd:$out_command", $logLevel, $debugFile);
					
					$out_command = pack('H*' ,$out_command);
					$out_command = PackEncrypted($out_command, $key, $logLevel, $debugFile, $call_id);
	
					SecureLogIt("$call_id\t" . "Sending encrypted client:$machine_id id:$out_command_id cmd:" . unpack("H*", $out_command), $logLevel, $debugFile);
				
					$out_command = uuencode($out_command) . "\x0a";
					SecureLogIt("$call_id\t" . "Sending encoded client:$machine_id id:$out_command_id cmd:" . unpack("H*", $out_command), $logLevel, $debugFile);
	
					$outBuffer .= $out_command;
					$write_set->add($sock);
					$last_command_resend = time() + 5;
	
					$DATABASE->update(
									table			=> 'Machine_Command',
									update_columns	=> 'Execute_Cd',
									update_values	=> ['Y'],
									where_columns	=> ['Command_ID = ?'],
									where_values	=> [$out_command_id]);
				}
			}
		}
		elsif($next_command_check <= time())
		{
			$next_command_check = time() + 0.25;
		}
		
		if ($watchdog_valid_inbound_msg + $TIMEOUT < time())
		{
			SecureLogIt("$call_id\t" . "Exceeded inactivity timeout... closing the connection.", $logLevel, $debugFile);
			&USAT::Common::CallInRecord::finish($DATABASE, $machine_id);
			USAT::NetLayer::Session::finish($DATABASE, $session_id, $in_msg_count, $out_msg_count, $in_byte_count, $out_byte_count) if($started_log eq 'Y');
			$sock->close();
			return;
		}
		else
		{
			sleep 0.2;
		}
	}
}

sub getKey ($$$$$)
{
	my ($machineID, $DATABASE, $call_id, $logLevel, $debugFile) = @_;
	SecureLogIt("$call_id\t" . "Looking up encryption key for $machineID", $logLevel, $debugFile);
	
	if($machineID eq $DEFAULT_MACHINE_ID)
	{
		SecureLogIt("$call_id\t" . "Returning DEFAULT encryption key", $logLevel, $debugFile);
		return $DEFAULT_ENCRYPTION_KEY;
	}
	
	my $array_ref = $DATABASE->select(
							table          => 'device',
							select_columns => 'encryption_key, device_serial_cd',
							where_columns  => [ 'device_name = ?', 'device_active_yn_flag = ?' ],
							where_values   => [ $machineID, 'Y' ] );
							
	if(not $array_ref->[0])
	{
		return (0,undef);
	}
	else
	{
		return ($array_ref->[0][0],$array_ref->[0][1]);
	}
}

sub check_hangup ($)
{
	my $inBuffer = shift;

	if(not defined $inBuffer)
	{
		return undef;
	}

	if($inBuffer =~ /^\+\+\+/)
	{
		# standard +++
		return '+++';
	}
	elsif($inBuffer =~ /^AT#CONNECTIONSTOP/)
	{
		# Multitech GSM/GPRS modem
		return 'AT#CONNECTIONSTOP';
	}
	elsif($inBuffer =~ /^\xff?Logout/)
	{
		# Lantronix
		return 'Logout';
	}
	elsif($inBuffer =~ /\x9e\x86\x9e\x86\x9e\x86/)
	{
		# AS5300 
		return '9e869e869e86';
	}
	
	return undef;
}

sub load_end_bug_device_list ()
{
	my $cache_lifetime = shift;
	my @res_arr;
	if (-f $END_bug_device_list)
	{
		open(DFILE, "<$END_bug_device_list") or die "Unable to open file '$END_bug_device_list'";
		while (<DFILE>) 
		{ 
			chomp $_; 
			push @res_arr, $_; 
		}
		close(DFILE);
	}
	else
	{
		warn "End bug device list file '$END_bug_device_list' not found";
	}
	return @res_arr;
}

sub handle_decrypt_failure ($$$$$$$$)
{
	my ($DATABASE, $call_id, $packet, $machine_id, $ssn, $orig_key, $logLevel, $debugFile) = @_;
	
	if (grep(/^$machine_id/, @END_bug_device_list)) 
	{ 
		SecureLogIt("$call_id\t" . "handle_decrypt_failure - $machine_id is on END bug list;  Attempting to send response!", $logLevel, $debugFile);
		
		my ($msg_no, $msg_type, $trans_id, undef) = unpack("H2H2Na*", $packet);
		
		my $new_key = gen_random_key($ssn);
		
		SecureLogIt("$call_id\t" . "handle_decrypt_failure - Original Key : $orig_key", $logLevel, $debugFile);
		SecureLogIt("$call_id\t" . "handle_decrypt_failure - New Key      : $new_key", $logLevel, $debugFile);
		
		my $response = pack("CH2a*a*", $msg_no, '8F', $new_key, $machine_id);
		
		SecureLogIt("$call_id\t" . "handle_decrypt_failure - Storing new key for $machine_id", $logLevel, $debugFile);
		$DATABASE->update(
					table			=> 'device',
					update_columns	=> 'encryption_key',
					update_values	=> [$new_key],
					where_columns  => ['device_name = ?', 'device_active_yn_flag = ?'],
					where_values   => [$machine_id, 'Y']);
		
		# remove any existing responses for this ID
		SecureLogIt("$call_id\t" . "Flushing outbound command queue for $machine_id...", $logLevel, $debugFile);
		$DATABASE->update(
					table			=> 'machine_command',
					update_columns	=> 'execute_cd',
					update_values	=> ['E'],
					where_columns  => ['modem_id = ?'],
					where_values   => [$machine_id]);
		
		SecureLogIt("$call_id\t" . "handle_decrypt_failure - Sending \"Set ID and Key\" (8Fh) : " . unpack("H*", $response), $logLevel, $debugFile);
		$DATABASE->insert(	table=> 'Machine_Command',
						insert_columns=> 'Modem_ID, Command',
						insert_values=> [$machine_id, unpack("H*", $response)] );
						
		
		
		return 1;
	}
	else
	{
		SecureLogIt("$call_id\t" . "handle_decrypt_failure - $machine_id is NOT on END bug list", $logLevel, $debugFile);
		return 0;
	}
}

#########################################################
# PackEncrypted($message, $key, $logLevel, $debugFile)
#########################################################
# Description:  Pack the message into encrypted form
#
# Inputs:       The message as a string and the key to
#				use for encryption.
#
# Outputs:      The message, length, and CRC as a string
#               in encrypted form
#
# History:
# Date			Author		Changes Made
# -----------------------------------------------
# 08/15/2003	pcowan		Adapted from older libraries
#########################################################
sub PackEncrypted ($$$$;$)
{
	my ($message, $key, $logLevel, $debugFile, $call_id) = @_;
	my ($len, $crc, @list_key, $tea);
    
	if (not defined $key)
	{
		# default key used in alot of places
		$key = "E4059999";
	}

	if(length($key) == 8)
	{
		@list_key =(
			(ord(substr($key,3,1))<<24) |
			(ord(substr($key,2,1))<<16) |
			(ord(substr($key,1,1))<<8)  |
			 ord(substr($key,0,1))
			,
			(ord(substr($key,7,1))<<24) |
			(ord(substr($key,6,1))<<16) |
			(ord(substr($key,5,1))<<8)  |
			 ord(substr($key,4,1))
		);
		
        # Duplicate it to get 16 bytes
	    push @list_key, @list_key;
	}
	elsif(length($key) == 16)
	{
		@list_key =(
			(ord(substr($key,3,1))<<24) |
			(ord(substr($key,2,1))<<16) |
			(ord(substr($key,1,1))<<8)  |
			 ord(substr($key,0,1))
			,
			(ord(substr($key,7,1))<<24) |
			(ord(substr($key,6,1))<<16) |
			(ord(substr($key,5,1))<<8)  |
			 ord(substr($key,4,1))
			,
			(ord(substr($key,11,1))<<24) |
			(ord(substr($key,10,1))<<16) |
			(ord(substr($key,9,1))<<8)  |
			 ord(substr($key,8,1))
			,
			(ord(substr($key,15,1))<<24) |
			(ord(substr($key,14,1))<<16) |
			(ord(substr($key,13,1))<<8)  |
			 ord(substr($key,12,1))
		);
	}
	else
	{
		SecureLogIt("PackEncrypted: Invalid Key Length: $key", $logLevel, $debugFile);
		return 0;
	}
	
    # set the key
    $tea = Evend::Crypt::TEA->new(Key=>\@list_key);
    
    # get the length of the message as a character
    $len = chr(length $message);
    
    # Compute the CRC16 of the message
    $crc = getcrc16s($message);
    
    SecureLogIt("$call_id\t" . "Encrypt: [".unpack("H*",ord($len))."][".unpack("H*",$message)."][".unpack("H*",$crc)."]", $logLevel, $debugFile);

    # build the message with the length and the crc
    $message = $len . $message . $crc;
    
    # Do magic
    return $tea->EncodeStream($message);
}

#########################################################
# UnpackEncrypted($message, $key, $logLevel, $debugFile)
#########################################################
# Description:  Unpack the message from encrypted form
#
# Inputs:       Encrypted Message, Key to use to decrypt
#
# Outputs:      ([0,1].message)
#               First argument is success of CRC match
#
# History:
# Date			Author		Changes Made
# -----------------------------------------------
# 08/15/2003	pcowan		Adapted from older libraries
#########################################################
sub UnpackEncrypted ($$$$;$)
{
	my ($message, $key, $logLevel, $debugFile, $call_id) = @_;
	my ($out, $len, $crc, $ccrc, @list_key, $tea);
    
	if (not defined $key)
	{
		# default key used in a lot of places
		$key = "E4059999";
	}

	if(length($key) == 8)
	{
		@list_key =(
			(ord(substr($key,3,1))<<24) |
			(ord(substr($key,2,1))<<16) |
			(ord(substr($key,1,1))<<8)  |
			 ord(substr($key,0,1))
			,
			(ord(substr($key,7,1))<<24) |
			(ord(substr($key,6,1))<<16) |
			(ord(substr($key,5,1))<<8)  |
			 ord(substr($key,4,1))
		);
		
        # Duplicate it to get 16 bytes
	    push @list_key, @list_key;
	}
	elsif(length($key) == 16)
	{
		@list_key =(
			(ord(substr($key,3,1))<<24) |
			(ord(substr($key,2,1))<<16) |
			(ord(substr($key,1,1))<<8)  |
			 ord(substr($key,0,1))
			,
			(ord(substr($key,7,1))<<24) |
			(ord(substr($key,6,1))<<16) |
			(ord(substr($key,5,1))<<8)  |
			 ord(substr($key,4,1))
			,
			(ord(substr($key,11,1))<<24) |
			(ord(substr($key,10,1))<<16) |
			(ord(substr($key,9,1))<<8)  |
			 ord(substr($key,8,1))
			,
			(ord(substr($key,15,1))<<24) |
			(ord(substr($key,14,1))<<16) |
			(ord(substr($key,13,1))<<8)  |
			 ord(substr($key,12,1))
		);
	}
	else
	{
		SecureLogIt("UnpackEncrypted: Invalid Key Length: $key", $logLevel, $debugFile);
		return 0;
	}
	
	# set the key
	$tea = Evend::Crypt::TEA->new(Key=>\@list_key);
    
	# decrypt
	$out = $tea->DecodeStream($message);
    
	# first byte is length    
	$len = ord($out);
	
	$out = substr($out,1);
	
	# last 2 bytes are CRC
	$crc = substr($out,$len,2);
	
	# string may be padded because that's what Tea does...
	$out = substr($out,0,$len);
	
	SecureLogIt("$call_id\t" . "Decrypt: [".unpack("H*",$len)."][", $logLevel, $debugFile, unpack("H*",$out), "][".unpack("H*",$crc)."]");
	
	# calculate our own crc to compare to saved one
	$ccrc = getcrc16s($out);
    
	return [($crc eq $ccrc)?1:0, $out];
}

sub gen_random_key (;$)
{
	my ($SSN) = @_;
	my $len = 16;
	my $seed;
	my $key;
	my $counter = 1;
	
	while(length($key) < $len)
	{
		$seed = $counter . time() . $SSN;
		$key = $key . nrand48($seed);
		$counter++;
	}
	
	$key = substr($key, 0, $len);
		
	return $key;
}

1;
