-- This migration plan step will add a new device column that represents the last time the device sent configuration data to us
-- We use this to know when configuration data for a device is stale and needs to be requested again

alter table device.device add ( received_config_file_ts date null ) ;

COMMENT ON COLUMN device.received_config_file_ts IS 'Timestamp of when the last configuration file was received from the device by the system.';

UPDATE DEVICE D SET RECEIVED_CONFIG_FILE_TS = (SELECT MAX(FT.LAST_UPDATED_TS) FROM DEVICE.FILE_TRANSFER FT WHERE FT.FILE_TRANSFER_NAME = D.DEVICE_NAME || '-CFG' ) WHERE D.DEVICE_ACTIVE_YN_FLAG = 'Y' AND d.DEVICE_TYPE_ID IN (0,1,6);
