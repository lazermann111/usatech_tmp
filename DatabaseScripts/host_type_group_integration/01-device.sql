--execute as user DEVICE

CREATE SEQUENCE DEVICE.SEQ_HOST_GROUP_TYPE_ID
    INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH 1;

CREATE TABLE DEVICE.HOST_GROUP_TYPE
  (
    HOST_GROUP_TYPE_ID    NUMBER(20,0),
    HOST_GROUP_TYPE_CD    VARCHAR2(60)    not null,
    HOST_GROUP_TYPE_NAME  VARCHAR2(60)    not null,
    HOST_GROUP_TYPE_DESC  VARCHAR2(255),
    CREATED_BY            VARCHAR2(30)    not null,
    CREATED_TS            DATE            not null,
    LAST_UPDATED_BY       VARCHAR2(30)    not null,
    LAST_UPDATED_TS       DATE            not null,
    CONSTRAINT PK_HOST_GROUP_TYPE primary key(HOST_GROUP_TYPE_ID)
  )
  TABLESPACE DEVICE_DATA;

CREATE UNIQUE INDEX UDX_HOST_GROUP_TYPE_1 ON HOST_GROUP_TYPE(HOST_GROUP_TYPE_CD) TABLESPACE DEVICE_INDX;

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER DEVICE.TRBI_HOST_GROUP_TYPE BEFORE INSERT ON HOST_GROUP_TYPE
  FOR EACH ROW 
BEGIN
	IF :NEW.HOST_GROUP_TYPE_ID IS NULL
	THEN
		SELECT SEQ_HOST_GROUP_TYPE_ID.NEXTVAL
		INTO :NEW.HOST_GROUP_TYPE_ID
		FROM DUAL;
	END IF;
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER DEVICE.TRBU_HOST_GROUP_TYPE BEFORE UPDATE ON HOST_GROUP_TYPE
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

COMMENT ON TABLE DEVICE.HOST_GROUP_TYPE is 'Any type of logical grouping of host_types, primarily for reporting purposes.';

CREATE TABLE DEVICE.HOST_TYPE_HOST_GROUP_TYPE
  (
    HOST_TYPE_ID        NUMBER(20,0),
    HOST_GROUP_TYPE_ID  NUMBER(20,0),
    CREATED_BY          VARCHAR2(30)   not null,
    CREATED_TS          DATE           not null,
    LAST_UPDATED_BY     VARCHAR2(30)   not null,
    LAST_UPDATED_TS     DATE           not null,
    CONSTRAINT PK_HOST_TYPE_HOST_GROUP_TYPE primary key(HOST_TYPE_ID,HOST_GROUP_TYPE_ID),
    CONSTRAINT FK_HOST_TYPE_ID foreign key(HOST_TYPE_ID) references HOST_TYPE(HOST_TYPE_ID),
    CONSTRAINT FK_HOST_GROUP_TYPE_ID foreign key(HOST_GROUP_TYPE_ID) references HOST_GROUP_TYPE(HOST_GROUP_TYPE_ID)
  )
  TABLESPACE DEVICE_DATA;

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER DEVICE.TRBI_HOST_TYPE_HOST_GROUP_TYPE BEFORE INSERT ON HOST_TYPE_HOST_GROUP_TYPE
  FOR EACH ROW 
BEGIN
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER DEVICE.TRBU_HOST_TYPE_HOST_GROUP_TYPE BEFORE UPDATE ON HOST_TYPE_HOST_GROUP_TYPE
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/


INSERT INTO DEVICE.HOST_GROUP_TYPE (HOST_GROUP_TYPE_ID,HOST_GROUP_TYPE_CD,HOST_GROUP_TYPE_NAME,HOST_GROUP_TYPE_DESC) 
	VALUES (1, 'ESUDS_WASHER', 'Washer', 'Any single washer type used by eSuds');
INSERT INTO DEVICE.HOST_GROUP_TYPE (HOST_GROUP_TYPE_ID,HOST_GROUP_TYPE_CD,HOST_GROUP_TYPE_NAME,HOST_GROUP_TYPE_DESC) 
	VALUES (2, 'ESUDS_DRYER', 'Dryer', 'Any single dryer type used by eSuds');
INSERT INTO DEVICE.HOST_GROUP_TYPE (HOST_GROUP_TYPE_ID,HOST_GROUP_TYPE_CD,HOST_GROUP_TYPE_NAME,HOST_GROUP_TYPE_DESC) 
	VALUES (3, 'ESUDS_STACKED_DRYER', 'Stacked Dryer', 'Any stacked dryer/dryer type used by eSuds');
INSERT INTO DEVICE.HOST_GROUP_TYPE (HOST_GROUP_TYPE_ID,HOST_GROUP_TYPE_CD,HOST_GROUP_TYPE_NAME,HOST_GROUP_TYPE_DESC) 
	VALUES (4, 'ESUDS_STACKED_WASHER_DRYER', 'Stacked Washer/Dryer', 'Any single dryer/washer type used by eSuds');

INSERT INTO DEVICE.HOST_TYPE_HOST_GROUP_TYPE (HOST_TYPE_ID, HOST_GROUP_TYPE_ID) (
	SELECT host_type_id, DECODE(
		host_type_desc, 
		'Dryer', 2,
		'Washer', 1,
		'Stacked Dryer', 3,
		'Stacked Washer/Dryer', 4,
		NULL
	) host_group_type_id
	FROM device.host_type
	WHERE host_type_desc IN (
		'Dryer',
		'Washer',
		'Stacked Dryer',
		'Stacked Washer/Dryer'
	)
);
