DECLARE
    CURSOR l_cur IS
select 'alter '||decode(object_type,'PACKAGE BODY'
,'package '||owner||'.'||object_name||
'  compile body'
,object_type||' '||owner||'.'||object_name
||' compile') sql_text
from dba_objects 
where status = 'INVALID' and object_type NOT IN('MATERIALIZED VIEW', 'SYNONYM')
and owner in (
	'APP_EXEC_HIST',
	'APP_LAYER',
	'APP_LOG',
	'APP_USER',
	'AUTHORITY',
	'CORP',
	'DBADMIN',
	'DEVICE',
	'ENGINE',
	'FOLIO_CONF',
	'FRONT',
	'G4OP',
	'LOCATION',
	'PSS',
	'RDW_LOADER',
	'RECON',
	'REPORT',
	'UPDATER',
	'WEB_CONTENT'
);
BEGIN
    FOR i IN 1..2 LOOP
        FOR l_rec in l_cur LOOP
            DBMS_OUTPUT.PUT_LINE('Executing "' || l_rec.sql_text || '"');
            BEGIN
                EXECUTE IMMEDIATE l_rec.sql_text;
            EXCEPTION
                WHEN OTHERS THEN
                    DBMS_OUTPUT.PUT_LINE('Failed "' || l_rec.sql_text || '":');
                    DBMS_OUTPUT.PUT_LINE(SQLERRM);
            END;
        END LOOP;
    END LOOP;
END;
/
