ALTER TABLE pss.consumer_acct ADD (location_id NUMBER(20,0));
ALTER TABLE pss.consumer_acct ADD (consumer_acct_issue_num NUMBER(9,0));
ALTER TABLE pss.consumer_acct ADD (consumer_acct_activation_ts DATE);
ALTER TABLE pss.consumer_acct ADD (consumer_acct_deactivation_ts DATE);

UPDATE pss.consumer_acct SET consumer_acct_deactivation_ts = last_updated_ts WHERE consumer_acct_active_yn_flag = 'N';
UPDATE pss.consumer_acct SET consumer_acct_activation_ts = created_ts;
BEGIN
	DECLARE
		n_consumer_acct_id consumer_acct.consumer_acct_id%TYPE;
		v_consumer_acct_cd consumer_acct.consumer_acct_cd%TYPE;
		v_consumer_acct_active_yn_flag consumer_acct.consumer_acct_active_yn_flag%TYPE;
		CURSOR c_get_consumer_acct_id IS
    		SELECT consumer_acct_id, consumer_acct_cd, consumer_acct_active_yn_flag
    		FROM consumer_acct
    		ORDER BY consumer_acct_active_yn_flag, consumer_acct_cd, consumer_acct_id;
	BEGIN
		OPEN c_get_consumer_acct_id;
		LOOP
			FETCH c_get_consumer_acct_id INTO n_consumer_acct_id, v_consumer_acct_cd, v_consumer_acct_active_yn_flag;
			EXIT WHEN c_get_consumer_acct_id%NOTFOUND;
			UPDATE pss.consumer_acct 
			SET consumer_acct_issue_num = (
				SELECT NVL(MAX(consumer_acct_issue_num), 0) + 1
				FROM consumer_acct
				WHERE consumer_acct_cd = v_consumer_acct_cd
			)
			WHERE consumer_acct_id = n_consumer_acct_id;
		END LOOP;
		COMMIT;
		CLOSE c_get_consumer_acct_id;
	END;
END;
/
UPDATE consumer_acct SET consumer_id = 1 WHERE consumer_acct_id IN (
    SELECT ca.consumer_acct_id 
    FROM consumer c, consumer_acct ca 
    WHERE ca.consumer_id = c.consumer_id(+) 
    AND c.location_id IS NULL
);
UPDATE pss.consumer_acct ca SET location_id = (
	SELECT c.location_id
	FROM consumer c
	WHERE c.consumer_id = ca.consumer_id
);

ALTER TABLE pss.consumer_acct MODIFY consumer_id NOT NULL;
ALTER TABLE pss.consumer_acct MODIFY consumer_acct_issue_num NOT NULL;
ALTER TABLE pss.consumer_acct ADD CONSTRAINT fk_consumer_acct_location FOREIGN KEY (location_id) REFERENCES location.location(location_id) ON DELETE SET NULL;
ALTER TABLE pss.consumer_acct ADD CONSTRAINT uk_consumer_acct_rel1 UNIQUE (consumer_acct_cd, consumer_acct_issue_num, location_id);

ALTER TABLE pss.consumer MODIFY location_id NULL; 
UPDATE pss.consumer SET location_id = NULL;
ALTER TABLE pss.consumer DROP COLUMN location_id CASCADE CONSTRAINTS;

-- Start of DDL Script for Package PSS.PKG_CONSUMER_MAINT
-- Generated 13-Dec-2004 11:39:39 from PSS@USADBD03.USATECH.COM

-- Drop the old instance of PKG_CONSUMER_MAINT
DROP PACKAGE pss.pkg_consumer_maint
/

CREATE OR REPLACE 
PACKAGE pss.pkg_consumer_maint IS
   PROCEDURE sp_add_consumer (
      pv_consumer_fname                IN       consumer.consumer_fname%TYPE,
      pv_consumer_lname                IN       consumer.consumer_lname%TYPE,
      pv_consumer_addr1                IN       consumer.consumer_addr1%TYPE,
      pv_consumer_addr2                IN       consumer.consumer_addr2%TYPE,
      pv_consumer_city                 IN       consumer.consumer_city%TYPE,
      pv_consumer_state                IN       consumer.consumer_state_cd%TYPE,
      pv_consumer_postal_cd            IN       consumer.consumer_postal_cd%TYPE,
      pv_consumer_email_addr1          IN       consumer.consumer_email_addr1%TYPE,
      pn_consumer_type_id              IN       consumer.consumer_type_id%TYPE,
      pn_location_id                   IN       consumer_acct.location_id%TYPE,
      pv_consumer_account_cd           IN       consumer_acct.consumer_acct_cd%TYPE,
      pv_consumer_accnt_actv_yn_flag   IN       consumer_acct.consumer_acct_active_yn_flag%TYPE,
      pv_consumer_account_balance      IN       consumer_acct.consumer_acct_balance%TYPE,
      pn_return_code                   OUT      exception_code.exception_code_id%TYPE,
      pv_error_message                 OUT      exception_data.additional_information%TYPE
   );

   PROCEDURE sp_deactivate_consumer (
      pv_consumer_email_addr1   IN       consumer.consumer_email_addr1%TYPE,
      pn_return_code            OUT      exception_code.exception_code_id%TYPE,
      pv_error_message          OUT      exception_data.additional_information%TYPE
   );
END pkg_consumer_maint;
/


CREATE OR REPLACE 
PACKAGE BODY     pss.pkg_consumer_maint IS
/* The following rules were applied to this procedure

1.       The only unique identifier we have is the student?s email address.
         We determine if a student already exists in our database by comparing the
         email address provided in the data file with the records in our database.

2.       If a student?s email address changes then a new student record will be
         created.  Transactions from the original student record will not be
         associated with the new student record.

3.       All fields, except the password and email address, will be updated when
         new information is sent about an existing student.   The only exception
         to this rule is:

        "When a student?s id number changes, and the password was never changed
        by the user, the password will be set to the last four digits of the new
        student id."

4.      Student accounts in the database will be deactivated if they are not
        present when loading the data file.

5.      When we receive data about a student that has been deactivated we will
        reactivate the account.  A new account will not be created and any
        historical data will be re-associated with the student.

*/
   cv_package_name   CONSTANT VARCHAR2 (30) := 'pkg_consumer_maint';

   PROCEDURE sp_add_consumer (
      pv_consumer_fname                IN       consumer.consumer_fname%TYPE,
      pv_consumer_lname                IN       consumer.consumer_lname%TYPE,
      pv_consumer_addr1                IN       consumer.consumer_addr1%TYPE,
      pv_consumer_addr2                IN       consumer.consumer_addr2%TYPE,
      pv_consumer_city                 IN       consumer.consumer_city%TYPE,
      pv_consumer_state                IN       consumer.consumer_state_cd%TYPE,
      pv_consumer_postal_cd            IN       consumer.consumer_postal_cd%TYPE,
      pv_consumer_email_addr1          IN       consumer.consumer_email_addr1%TYPE,
      pn_consumer_type_id              IN       consumer.consumer_type_id%TYPE,
      pn_location_id                   IN       consumer_acct.location_id%TYPE,
      pv_consumer_account_cd           IN       consumer_acct.consumer_acct_cd%TYPE,
      pv_consumer_accnt_actv_yn_flag   IN       consumer_acct.consumer_acct_active_yn_flag%TYPE,
      pv_consumer_account_balance      IN       consumer_acct.consumer_acct_balance%TYPE,
      pn_return_code                   OUT      exception_code.exception_code_id%TYPE,
      pv_error_message                 OUT      exception_data.additional_information%TYPE
   ) IS
      v_error_msg                  exception_data.additional_information%TYPE;
      cv_procedure_name   CONSTANT VARCHAR2 (30)                     := 'sp_add_consumer';
      n_return_cd                  NUMBER;
      n_issue_num                  NUMBER;
      n_app_user_count             NUMBER;
      n_consumer_id                consumer.consumer_id%TYPE;
      n_app_user_id                app_user.app_user_id%TYPE;
      v_app_user_pw                app_user.app_user_password%TYPE;
      v_old_consumer_acct_cd       consumer_acct.consumer_acct_cd%TYPE;
      v_new_consumer_acct_cd       consumer_acct.consumer_acct_cd%TYPE;
      d_effective_date             DATE := SYSDATE;
   BEGIN
      -- Check if the user already exists in the db.  If so we can not
      -- add a new one
      IF pv_consumer_email_addr1 IS NULL THEN
        RAISE_APPLICATION_ERROR(-20171, 'An email address must be provided');
      END IF;
         SELECT MAX(consumer_id)
           INTO n_consumer_id
           FROM consumer
          WHERE UPPER(consumer_email_addr1) = UPPER(pv_consumer_email_addr1);

      IF n_consumer_id IS NULL THEN
         SELECT seq_consumer_id.NEXTVAL
           INTO n_consumer_id
           FROM DUAL;

         INSERT INTO consumer
                     (consumer_id,
                      consumer_fname,
                      consumer_lname,
                      consumer_addr1,
                      consumer_addr2,
                      consumer_city,
                      consumer_state_cd,
                      consumer_postal_cd,
                      consumer_email_addr1,
                      consumer_type_id
                     )
              VALUES (n_consumer_id,
                      pv_consumer_fname,
                      pv_consumer_lname,
                      pv_consumer_addr1,
                      pv_consumer_addr2,
                      pv_consumer_city,
                      pv_consumer_state,
                      pv_consumer_postal_cd,
                      pv_consumer_email_addr1,
                      pn_consumer_type_id
                     );

         INSERT INTO consumer_acct
                     (consumer_acct_cd,
                      consumer_acct_active_yn_flag,
                      consumer_acct_balance,
                      consumer_id,
                      payment_subtype_id,
                      location_id,
                      consumer_acct_activation_ts
                     )
              VALUES (pv_consumer_account_cd,
                      pv_consumer_accnt_actv_yn_flag,
                      pv_consumer_account_balance,
                      n_consumer_id,
                      1,
                      pn_location_id,
                      SYSDATE
                     );
      ELSE
         UPDATE consumer
            SET consumer_fname = pv_consumer_fname,
                consumer_lname = pv_consumer_lname,
                consumer_addr1 = pv_consumer_addr1,
                consumer_addr2 = pv_consumer_addr2,
                consumer_city = pv_consumer_city,
                consumer_state_cd = pv_consumer_state,
                consumer_postal_cd = pv_consumer_postal_cd,
                consumer_type_id = pn_consumer_type_id
          WHERE consumer_id = n_consumer_id;

         UPDATE consumer_acct
            SET consumer_acct_deactivation_ts = d_effective_date,
                consumer_acct_active_yn_flag = 'N'
          WHERE consumer_id = n_consumer_id
            AND consumer_acct_cd = pv_consumer_account_cd
            AND location_id = pn_location_id
          RETURNING consumer_acct_issue_num + 1 INTO n_issue_num;

         INSERT INTO consumer_acct
                     (consumer_acct_cd,
                      consumer_acct_active_yn_flag,
                      consumer_acct_balance,
                      consumer_id,
                      payment_subtype_id,
                      location_id,
                      consumer_acct_issue_num,
                      consumer_acct_activation_ts
                     )
              VALUES (pv_consumer_account_cd,
                      pv_consumer_accnt_actv_yn_flag,
                      pv_consumer_account_balance,
                      n_consumer_id,
                      1,
                      pn_location_id,
                      NVL(n_issue_num, 1),
                      SYSDATE
                     );
      END IF;

      SELECT MAX(app_user_id)
        INTO n_app_user_id
        FROM app_user
       WHERE UPPER(app_user_email_addr) = UPPER(pv_consumer_email_addr1);

      IF n_app_user_id IS NULL THEN
         SELECT seq_app_user_id.NEXTVAL
           INTO n_app_user_id
           FROM DUAL;

         INSERT INTO app_user
                     (app_user_id,
                      app_user_password,
                      app_user_active_yn_flag,
                      force_pw_change_yn_flag,
                      app_user_name,
                      app_user_fname,
                      app_user_lname,
                      app_user_email_addr
                     )
              VALUES (n_app_user_id,
                      SUBSTR (pv_consumer_account_cd,
                              LENGTH (pv_consumer_account_cd) - 4,
                              4
                             ),
                      pv_consumer_accnt_actv_yn_flag,
                      'N',
                      pv_consumer_email_addr1,
                      pv_consumer_fname,
                      pv_consumer_lname,
                      pv_consumer_email_addr1
                     );

         INSERT INTO app_user_object_permission
                     (app_user_id,
                      app_id,
                      app_object_type_id,
                      allow_object_create_yn_flag,
                      allow_object_read_yn_flag,
                      allow_object_modify_yn_flag,
                      allow_object_delete_yn_flag,
                      object_cd
                     )
              VALUES (n_app_user_id,
                      1,
                      1,
                      'N',
                      'Y',
                      'Y',
                      'N',
                      n_consumer_id
                     );
      ELSE
         SELECT app_user_password,
                c.consumer_acct_cd
           INTO v_app_user_pw,
                v_old_consumer_acct_cd
           FROM app_user a,
                consumer b,
                consumer_acct c
          WHERE a.app_user_id = n_app_user_id
            AND UPPER (a.app_user_email_addr) = UPPER (b.consumer_email_addr1)
            AND b.consumer_id = c.consumer_id
            AND c.consumer_acct_deactivation_ts IS NULL;

         IF SUBSTR (v_old_consumer_acct_cd,
                    LENGTH (v_old_consumer_acct_cd) - 3,
                    4
                   ) = v_app_user_pw THEN
            v_new_consumer_acct_cd :=
                   SUBSTR (pv_consumer_account_cd,
                           LENGTH (pv_consumer_account_cd) - 3,
                           4
                          );
         ELSE
            v_new_consumer_acct_cd := v_app_user_pw;
         END IF;

         UPDATE app_user
            SET app_user_password = v_new_consumer_acct_cd,
                app_user_active_yn_flag = pv_consumer_accnt_actv_yn_flag,
                force_pw_change_yn_flag = 'N',
                app_user_name = pv_consumer_email_addr1,
                app_user_fname = pv_consumer_fname,
                app_user_lname = pv_consumer_lname
          WHERE UPPER (app_user_email_addr) = UPPER (pv_consumer_email_addr1);
      END IF;
   /* changed this to report exceptions (BSK 11-10-04)
   EXCEPTION
      WHEN OTHERS THEN
         -- changed this slightly to set pv_error_message to only SQLERRM and
         -- pn_return_code to SQLCODE for web file upload functionality (BSK 11-05-04)
         pv_error_message := SQLERRM;
         pn_return_code := SQLCODE;
         pkg_exception_processor.sp_log_exception
                                               (pkg_app_exec_hist_globals.unknown_error_id,
                                                v_error_msg,
                                                pkg_app_exec_hist_globals.cv_server_name,
                                                cv_package_name || '.'
                                                || cv_procedure_name
                                               );
   --*/
   END;

   PROCEDURE sp_deactivate_consumer (
      pv_consumer_email_addr1   IN       consumer.consumer_email_addr1%TYPE,
      pn_return_code            OUT      exception_code.exception_code_id%TYPE,
      pv_error_message          OUT      exception_data.additional_information%TYPE
   ) IS
      v_error_msg                  exception_data.additional_information%TYPE;
      cv_procedure_name   CONSTANT VARCHAR2 (30)             := 'sp_add_consumer_account';
      n_return_cd                  NUMBER;
      n_consumer_account_count     NUMBER;
   BEGIN
      -- Check if the user already exists in the db.  If so we can not
      -- add a new one
      UPDATE consumer_acct ca
         SET ca.consumer_acct_active_yn_flag = 'N',
             ca.consumer_acct_deactivation_ts = SYSDATE
       WHERE ca.consumer_id IN (
                       SELECT consumer_id
                         FROM consumer
                        WHERE UPPER (consumer_email_addr1) =
                                                           UPPER (pv_consumer_email_addr1) );

      UPDATE app_user ap
         SET ap.app_user_active_yn_flag = 'N'
       WHERE UPPER (ap.app_user_email_addr) = UPPER (pv_consumer_email_addr1);
   EXCEPTION
      WHEN OTHERS THEN
         pv_error_message :=
                         'An unknown exception occurred = ' || SQLCODE || ', ' || SQLERRM;
         pn_return_code := pkg_app_exec_hist_globals.unknown_error_id;
         pkg_exception_processor.sp_log_exception
                                               (pn_return_code,
                                                v_error_msg,
                                                pkg_app_exec_hist_globals.cv_server_name,
                                                cv_package_name || '.'
                                                || cv_procedure_name
                                               );
         pn_return_code := pkg_app_exec_hist_globals.unsuccessful_execution;
   END;
END;
/


-- End of DDL Script for Package PSS.PKG_CONSUMER_MAINT
