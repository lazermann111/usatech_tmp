CREATE TABLE device.device_decode_fail_type (
    device_decode_fail_type_cd      VARCHAR2(1) NOT NULL,
    device_decode_fail_type_desc    VARCHAR2(30) NOT NULL
) TABLESPACE device_data;

ALTER TABLE device.device_decode_fail_type ADD CONSTRAINT pk_device_decode_fail_type PRIMARY KEY (device_decode_fail_type_cd) USING INDEX TABLESPACE device_indx;

INSERT INTO device.device_decode_fail_type VALUES('D', 'DECRYPTION_FAILURE');


CREATE TABLE device.device_decode_fail_stat (
	device_name VARCHAR2(60) NOT NULL,
	device_decode_fail_type_cd VARCHAR2(1) NOT NULL, 
	first_occurred_ts DATE DEFAULT SYSDATE NOT NULL,
	event_counter NUMBER(20,0) DEFAULT 1 NOT NULL,
	created_by VARCHAR2(30) NOT NULL,
	created_ts DATE NOT NULL,
	last_updated_by VARCHAR2(30) NOT NULL,
	last_updated_ts DATE NOT NULL
) TABLESPACE device_data;

ALTER TABLE device.device_decode_fail_stat ADD CONSTRAINT pk_device_decode_fail_stat PRIMARY KEY (device_name, device_decode_fail_type_cd) USING INDEX TABLESPACE device_indx;
ALTER TABLE device.device_decode_fail_stat ADD CONSTRAINT fk_ddfs_device_decode_fail_typ FOREIGN KEY (device_decode_fail_type_cd) REFERENCES device.device_decode_fail_type (device_decode_fail_type_cd);

CREATE OR REPLACE TRIGGER device.trbi_device_decode_fail_stat
 BEFORE
  INSERT
 ON device.device_decode_fail_stat
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
    SELECT    
        SYSDATE,
        USER,
        SYSDATE,
        USER
    INTO 
        :new.created_ts,
        :new.created_by,
        :new.last_updated_ts,
        :new.last_updated_by
    FROM dual;
END;
/

CREATE OR REPLACE TRIGGER device.trbu_device_decode_fail_stat
 BEFORE
  UPDATE
 ON device.device_decode_fail_stat
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
    SELECT
        :old.created_by,
        :old.created_ts,
        SYSDATE,
        USER
    INTO
        :new.created_by,
        :new.created_ts,
        :new.last_updated_ts,
        :new.last_updated_by
    FROM dual;
END;
/

GRANT SELECT, INSERT, UPDATE ON device.device_decode_fail_stat TO web_user;
CREATE PUBLIC SYNONYM device_decode_fail_stat FOR device.device_decode_fail_stat;
