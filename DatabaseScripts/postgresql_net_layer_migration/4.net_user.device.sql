
CREATE SEQUENCE device.seq_device_type_id
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 100000000000;

CREATE TABLE device.device_type (
	device_type_id integer DEFAULT NEXTVAL('device.seq_device_type_id') NOT NULL,
	device_type_desc varchar(60) NOT NULL,
	created_by varchar(30) NOT NULL,
	created_ts timestamp NOT NULL,
	last_updated_by varchar(30) NOT NULL,
	last_updated_ts timestamp NOT NULL,
	device_type_serial_cd_regex varchar(255) NOT NULL,
	pos_pta_tmpl_id bigint
) TABLESPACE device_data;

ALTER TABLE device.device_type ADD PRIMARY KEY (device_type_id);

CREATE TRIGGER gtrbi_device_type BEFORE INSERT ON device.device_type
FOR EACH ROW EXECUTE PROCEDURE engine.trbi_generic_table();

CREATE TRIGGER gtrbu_device_type BEFORE UPDATE ON device.device_type
FOR EACH ROW EXECUTE PROCEDURE engine.trbu_generic_table();



CREATE SEQUENCE device.seq_device_id
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 1000000000000000000 RESTART 20000;

CREATE TABLE device.device (
	device_id bigint DEFAULT NEXTVAL('device.seq_device_id') NOT NULL,
	device_name varchar(60) NOT NULL,
	device_serial_cd varchar(20) NOT NULL,
	created_by varchar(30) NOT NULL,
	created_ts timestamp NOT NULL,
	last_updated_by varchar(30) NOT NULL,
	last_updated_ts timestamp NOT NULL,
	device_type_id integer NOT NULL,
	device_active_yn_flag varchar(1) DEFAULT 'Y' NOT NULL,
	encryption_key varchar(16),
	last_activity_ts timestamp,
	device_desc varchar(60),
	device_client_serial_cd varchar(20)
) TABLESPACE device_data;

ALTER TABLE device.device ADD PRIMARY KEY (device_id);
ALTER TABLE device.device ADD CONSTRAINT fk_device_device_type FOREIGN KEY (device_type_id) REFERENCES device.device_type (device_type_id) ON DELETE SET NULL ON UPDATE CASCADE INITIALLY IMMEDIATE;

CREATE INDEX idx_device_yn_flag_name ON device.device (device_active_yn_flag,device_name) TABLESPACE device_indx;
CREATE INDEX idx_device_name_yn_flag ON device.device (device_name,device_active_yn_flag) TABLESPACE device_indx;
CREATE INDEX idx_device_serial_cd ON device.device (device_serial_cd) TABLESPACE device_indx;

CREATE TRIGGER gtrbi_device BEFORE INSERT ON device.device
FOR EACH ROW EXECUTE PROCEDURE engine.trbi_generic_table();

CREATE TRIGGER gtrbu_device BEFORE UPDATE ON device.device
FOR EACH ROW EXECUTE PROCEDURE engine.trbu_generic_table();



CREATE SEQUENCE device.seq_device_call_in_record_id
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 1000000000000000000;

CREATE TABLE device.device_call_in_record (
	device_call_in_record_id bigint DEFAULT NEXTVAL('device.seq_device_call_in_record_id') NOT NULL,
	call_in_start_ts timestamp NOT NULL,
	call_in_finish_ts timestamp,
	call_in_status char(1) DEFAULT 'I' ,
	network_layer varchar(30),
	ip_address varchar(30),
	modem_id varchar(30),
	rssi varchar(30),
	credit_trans_count integer DEFAULT 0,
	credit_trans_total numeric(14,4) DEFAULT 0,
	cash_trans_count integer DEFAULT 0,
	cash_trans_total numeric(14,4) DEFAULT 0,
	passcard_trans_count integer DEFAULT 0,
	passcard_trans_total numeric(14,4) DEFAULT 0,
	device_initialized_flag char(1) DEFAULT 'N',
	device_sent_config_flag char(1) DEFAULT 'N',
	server_sent_config_flag char(1) DEFAULT 'N',
	created_by varchar(30) NOT NULL,
	created_ts timestamp NOT NULL,
	last_updated_by varchar(30) NOT NULL,
	last_updated_ts timestamp NOT NULL,
	call_in_type char(1) DEFAULT 'U',
	auth_amount numeric(14,4) DEFAULT NULL,
	auth_approved_flag char(1),
	auth_card_type char(1),
	inbound_message_count integer DEFAULT 0,
	outbound_message_count integer DEFAULT 0,
	dex_file_size integer DEFAULT NULL,
	dex_received_flag char(1) DEFAULT 'N',
	outbound_byte_count integer DEFAULT 0,
	location_name varchar(200),
	customer_name varchar(200),
	serial_number varchar(64) NOT NULL,
	inbound_byte_count integer DEFAULT 0,
	session_cd varchar(25),
	credit_vend_count integer DEFAULT 0,
	cash_vend_count integer DEFAULT 0,
	passcard_vend_count integer DEFAULT 0,
	last_auth_in_ts timestamp,
	last_trans_in_ts timestamp,
	last_message_out_ts timestamp,
	session_id bigint
) TABLESPACE device_data;

ALTER TABLE device.device_call_in_record ADD PRIMARY KEY (device_call_in_record_id);
ALTER TABLE device.device_call_in_record ADD CONSTRAINT uc_call_in_record_session_id UNIQUE (session_id);

CREATE INDEX idx_call_in_record_finish_ts_serial ON device.device_call_in_record (call_in_finish_ts,serial_number) TABLESPACE device_indx;
CREATE INDEX idx_call_in_record_status_serial ON device.device_call_in_record (call_in_status,serial_number) TABLESPACE device_indx;
CREATE INDEX idx_call_in_record_serial_start_ts ON device.device_call_in_record (serial_number,call_in_start_ts) TABLESPACE device_indx;
CREATE INDEX idx_call_in_record_start_ts ON device.device_call_in_record (call_in_start_ts) TABLESPACE device_indx;

CREATE TRIGGER gtrbi_device_call_in_record BEFORE INSERT ON device.device_call_in_record
FOR EACH ROW EXECUTE PROCEDURE engine.trbi_generic_table();

CREATE TRIGGER gtrbu_device_call_in_record BEFORE UPDATE ON device.device_call_in_record
FOR EACH ROW EXECUTE PROCEDURE engine.trbu_generic_table();



CREATE OR REPLACE FUNCTION device.fn_call_in_log_this_device (
	ps_ev_number IN device.device.device_name%TYPE
) RETURNS char AS $$
DECLARE
	vn_device_type_id device.device.device_type_id%TYPE;
BEGIN
	SELECT device_type_id
	INTO vn_device_type_id
	FROM device.device
	WHERE device_name = ps_ev_number
	 	AND device_active_yn_flag = 'Y';
	 	
	IF vn_device_type_id IS NULL THEN
		RAISE EXCEPTION 'No record could be found in the device table for device name %', ps_ev_number;
	ELSIF vn_device_type_id IN (0, 1, 6) THEN
		RETURN 'Y';
	ELSE
		RETURN 'N';
	END IF;
END;
$$ LANGUAGE plpgsql;



CREATE OR REPLACE FUNCTION device.sp_call_in_set_type (
      	pn_session_id   IN	device.device_call_in_record.session_id%TYPE,
      	pc_type_flag	IN	device.device_call_in_record.call_in_type%TYPE
) RETURNS void AS $$
DECLARE
	-- call type values: U - unknown, A - auth, B - batch, C - combo
	vc_current_type_flag	device.device_call_in_record.call_in_type%TYPE;
BEGIN
	SELECT call_in_type
	INTO vc_current_type_flag
	FROM device.device_call_in_record
	WHERE session_id = pn_session_id;

	IF vc_current_type_flag = 'A' AND pc_type_flag = 'B' THEN
		UPDATE device.device_call_in_record
		SET call_in_type = 'C'
		WHERE session_id = pn_session_id;
	ELSIF vc_current_type_flag = 'B' AND pc_type_flag = 'A' THEN
		UPDATE device.device_call_in_record
		SET call_in_type = 'C'
		WHERE session_id = pn_session_id;
	ELSE
		UPDATE device.device_call_in_record
		SET call_in_type = pc_type_flag
		WHERE session_id = pn_session_id;
	END IF;
END;
$$ LANGUAGE plpgsql;



CREATE OR REPLACE FUNCTION device.sp_call_in_start (
	pn_session_id      IN   device.device_call_in_record.session_id%TYPE,
	ps_ev_number       IN   device.device.device_name%TYPE,
	ps_network_layer   IN   device.device_call_in_record.network_layer%TYPE,
	ps_ip_address      IN   device.device_call_in_record.ip_address%TYPE,
	ps_modem_id        IN   device.device_call_in_record.modem_id%TYPE,
	ps_rssi            IN   device.device_call_in_record.rssi%TYPE
) RETURNS void AS $$
DECLARE
	vs_serial_number device.device.device_serial_cd%TYPE;
BEGIN	
	IF device.fn_call_in_log_this_device(ps_ev_number) = 'N' THEN
	 	RETURN;
	END IF;
	
	SELECT device_serial_cd
	INTO vs_serial_number
	FROM device.device
	WHERE device_name = ps_ev_number
	 	AND device_active_yn_flag = 'Y';	

	-- If any extra in-progress records exists, mark them unsuccessful
	UPDATE device.device_call_in_record
	SET call_in_status = 'U'
	WHERE serial_number = vs_serial_number 
		AND call_in_status = 'I';

         -- Start a new device_call_in_record
        INSERT INTO device.device_call_in_record(
        	serial_number, call_in_start_ts, call_in_status,
		network_layer, ip_address, modem_id, rssi, session_id)
         VALUES (vs_serial_number, NOW(), 'I',
		ps_network_layer, ps_ip_address, ps_modem_id, ps_rssi, pn_session_id);
END;
$$ LANGUAGE plpgsql;



CREATE OR REPLACE FUNCTION device.sp_call_in_add_trans (
	pn_session_id     IN   device.device_call_in_record.session_id%TYPE,
	ps_ev_number      IN   device.device.device_name%TYPE,
	pc_card_type      IN   device.device_call_in_record.auth_card_type%TYPE,
	pn_trans_amount   IN   device.device_call_in_record.credit_trans_total%TYPE,
	pn_vend_count     IN   device.device_call_in_record.credit_vend_count%TYPE
   ) RETURNS void AS $$
BEGIN
	IF device.fn_call_in_log_this_device(ps_ev_number) = 'N' THEN
		RETURN;
	END IF;

	IF pc_card_type IN ('C', 'R') THEN -- credit magstripe or RFID
		UPDATE device.device_call_in_record
		SET credit_trans_count = credit_trans_count + 1,
			credit_trans_total = credit_trans_total + pn_trans_amount,
			credit_vend_count = credit_vend_count + pn_vend_count,
			last_trans_in_ts = NOW()
		WHERE session_id = pn_session_id;
	ELSIF pc_card_type = 'M' THEN -- cash
		UPDATE device.device_call_in_record
		SET cash_trans_count = cash_trans_count + 1,
			cash_trans_total = cash_trans_total + pn_trans_amount,
			cash_vend_count = cash_vend_count + pn_vend_count,
			last_trans_in_ts = NOW()
		WHERE session_id = pn_session_id;
	ELSIF pc_card_type IN ('S', 'P') THEN -- pass card magstripe or RFID
		UPDATE device.device_call_in_record
		SET passcard_trans_count = passcard_trans_count + 1,
			passcard_trans_total = passcard_trans_total + pn_trans_amount,
			passcard_vend_count = passcard_vend_count + pn_vend_count,
			last_trans_in_ts = NOW()
		WHERE session_id = pn_session_id;
	END IF;

	PERFORM device.sp_call_in_set_type (pn_session_id, 'B'); -- batch
END;
$$ LANGUAGE plpgsql;



CREATE OR REPLACE FUNCTION device.sp_call_in_add_auth (
	pn_session_id      IN   device.device_call_in_record.session_id%TYPE,
	ps_ev_number       IN   device.device.device_name%TYPE,
	pc_card_type       IN   device.device_call_in_record.auth_card_type%TYPE,
	pn_auth_amount     IN   device.device_call_in_record.auth_amount%TYPE,
	pc_approved_flag   IN   device.device_call_in_record.auth_approved_flag%TYPE
) RETURNS void AS $$
BEGIN
	IF device.fn_call_in_log_this_device(ps_ev_number) = 'N' THEN
		RETURN;
	END IF;

	UPDATE device.device_call_in_record
	SET auth_amount = pn_auth_amount,
		auth_approved_flag = pc_approved_flag,
		auth_card_type = pc_card_type,
		last_auth_in_ts = NOW()
	WHERE session_id = pn_session_id;

	PERFORM device.sp_call_in_set_type (pn_session_id, 'A'); -- auth
END;
$$ LANGUAGE plpgsql;



CREATE OR REPLACE FUNCTION device.sp_call_in_set_dex_received (
	pn_session_id      IN   device.device_call_in_record.session_id%TYPE,
      	ps_ev_number       IN   device.device.device_name%TYPE,
      	pn_dex_file_size   IN   device.device_call_in_record.dex_file_size%TYPE
 ) RETURNS void AS $$
BEGIN
	IF device.fn_call_in_log_this_device(ps_ev_number) = 'N' THEN
		RETURN;
	END IF;

	UPDATE device.device_call_in_record
	SET dex_received_flag = 'Y',
		dex_file_size = pn_dex_file_size
	WHERE session_id = pn_session_id;

	PERFORM device.sp_call_in_set_type (pn_session_id, 'B'); -- batch
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION device.sp_call_in_set_initialized (
	pn_session_id	 IN   	device.device_call_in_record.session_id%TYPE,
	ps_ev_number	 IN 	device.device.device_name%TYPE
) RETURNS void AS $$
BEGIN
	IF device.fn_call_in_log_this_device(ps_ev_number) = 'N' THEN
		RETURN;
	END IF;

	UPDATE device.device_call_in_record
	SET device_initialized_flag = 'Y'
	WHERE session_id = pn_session_id;

	PERFORM device.sp_call_in_set_type (pn_session_id, 'B'); -- batch
END;
$$ LANGUAGE plpgsql;



CREATE OR REPLACE FUNCTION device.sp_call_in_set_device_sent_config (
      	pn_session_id      IN   device.device_call_in_record.session_id%TYPE,
      	ps_ev_number       IN   device.device.device_name%TYPE
) RETURNS void AS $$
BEGIN
	IF device.fn_call_in_log_this_device(ps_ev_number) = 'N' THEN
		RETURN;
	END IF;

	UPDATE device.device_call_in_record
	SET device_sent_config_flag = 'Y'
	WHERE session_id = pn_session_id;

	PERFORM device.sp_call_in_set_type (pn_session_id, 'B'); -- batch
END;
$$ LANGUAGE plpgsql;



CREATE OR REPLACE FUNCTION device.sp_call_in_set_server_sent_config (
      	pn_session_id      IN   device.device_call_in_record.session_id%TYPE,
      	ps_ev_number       IN   device.device.device_name%TYPE
) RETURNS void AS $$
BEGIN
	IF device.fn_call_in_log_this_device(ps_ev_number) = 'N' THEN
		RETURN;
	END IF;

	UPDATE device.device_call_in_record
	SET server_sent_config_flag = 'Y'
	WHERE session_id = pn_session_id;

	PERFORM device.sp_call_in_set_type (pn_session_id, 'B'); -- batch
END;
$$ LANGUAGE plpgsql;



CREATE OR REPLACE FUNCTION device.sp_call_in_set_status (
      	pn_session_id      IN   device.device_call_in_record.session_id%TYPE,
      	ps_ev_number       IN   device.device.device_name%TYPE,
      	pc_status_flag     IN   device.device_call_in_record.call_in_status%TYPE
) RETURNS void AS $$
BEGIN
	IF device.fn_call_in_log_this_device(ps_ev_number) = 'N' THEN
		RETURN;
	END IF;

	-- status values: I - incomplete, S - complete, U - failed, X - unknown
	-- if new_status is Success, overwrite all
	-- if new_status is Failed, overwrite all but success
	-- if new_status is Unknown, overwrite only incomplete
	-- if new_status is Incomplete, overwrite none
	IF pc_status_flag = 'S' THEN
		UPDATE device.device_call_in_record
		SET call_in_status = pc_status_flag
		WHERE session_id = pn_session_id;
	ELSIF pc_status_flag = 'U' THEN
		UPDATE device.device_call_in_record
		SET call_in_status =
		   	CASE call_in_status
				WHEN 'S' THEN call_in_status
				ELSE pc_status_flag
			END
		WHERE session_id = pn_session_id;
	ELSIF pc_status_flag = 'X' THEN
		UPDATE device.device_call_in_record
		SET call_in_status =
			CASE call_in_status
				WHEN 'I' THEN pc_status_flag
			   	ELSE call_in_status
			END
		WHERE session_id = pn_session_id;
	END IF;
END;
$$ LANGUAGE plpgsql;



CREATE OR REPLACE FUNCTION device.sp_call_in_finish (
      	pn_session_id      IN   device.device_call_in_record.session_id%TYPE,
      	ps_ev_number       IN   device.device.device_name%TYPE
) RETURNS void AS $$
BEGIN
	IF device.fn_call_in_log_this_device(ps_ev_number) = 'N' THEN
		RETURN;
	END IF;

	-- if call_in_status is Incomplete, update it to Unknown
	UPDATE device.device_call_in_record
	SET call_in_finish_ts = NOW(),
		call_in_status =
			CASE call_in_status
				WHEN 'I' THEN 'U'
				ELSE call_in_status
			END
	WHERE session_id = pn_session_id;
END;
$$ LANGUAGE plpgsql;



CREATE OR REPLACE FUNCTION device.sp_call_in_add_message (
      	pn_session_id      IN   device.device_call_in_record.session_id%TYPE,
      	ps_ev_number       IN   device.device.device_name%TYPE,
      	pc_direction   	   IN   char,
      	pn_num_bytes   	   IN   device.device_call_in_record.inbound_byte_count%TYPE
) RETURNS void AS $$
BEGIN
	IF device.fn_call_in_log_this_device(ps_ev_number) = 'N' THEN
		RETURN;
	END IF;

	-- message direction values: I - inbound, O - outbound
	IF pc_direction = 'I' THEN
		UPDATE device.device_call_in_record
		SET inbound_message_count = inbound_message_count + 1,
			inbound_byte_count = inbound_byte_count + pn_num_bytes
		WHERE session_id = pn_session_id;
	ELSIF pc_direction = 'O' THEN
		UPDATE device.device_call_in_record
		SET outbound_message_count = outbound_message_count + 1,
			outbound_byte_count = outbound_byte_count + pn_num_bytes,
			last_message_out_ts = NOW()
		WHERE session_id = pn_session_id;
	END IF;
END;
$$ LANGUAGE plpgsql;
