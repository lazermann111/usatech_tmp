#!/bin/sh

./configure
gmake
gmake install

adduser postgres
mkdir /usr/local/pgsql/data
mkdir /usr/local/pgsql/logs
mkdir /usr/local/pgsql/tblspace
mkdir /usr/local/pgsql/tblspace/engine_data
mkdir /usr/local/pgsql/tblspace/engine_indx
mkdir /usr/local/pgsql/tblspace/device_data
mkdir /usr/local/pgsql/tblspace/device_indx

chown postgres /usr/local/pgsql/data
chown postgres /usr/local/pgsql/logs
chown postgres /usr/local/pgsql/tblspace
chown postgres /usr/local/pgsql/tblspace/engine_data
chown postgres /usr/local/pgsql/tblspace/engine_indx
chown postgres /usr/local/pgsql/tblspace/device_data
chown postgres /usr/local/pgsql/tblspace/device_indx

su - postgres
/usr/local/pgsql/bin/initdb -D /usr/local/pgsql/data
/usr/local/pgsql/bin/postmaster -D /usr/local/pgsql/data >/usr/local/pgsql/logs/pgsql.log 2>&1 &
/usr/local/pgsql/bin/createdb usadbn

# edit postgresql.conf and change:
	listen_addresses 
	port
	password_encryption
	datestyle = 'sql, mdy'
# edit pg_hba.conf (set authentication mode to md5)

/usr/local/pgsql/bin/psql usadbn

# ALTER USER postgres WITH PASSWORD '<password>';
# restart postmaster for changes to take effect

./pg_ctl stop -D /usr/local/pgsql/data
./pg_ctl start -D /usr/local/pgsql/data >/usr/local/pgsql/logs/pgsql.log 2>&1 &