--ORACLE

DECLARE 
    l_id NUMBER;
BEGIN   
    LOOP
        SELECT engine.seq_net_layer_device_sess_id.NEXTVAL INTO l_id FROM DUAL;
        
        IF SUBSTR(TO_CHAR(l_id), -2) = '92' THEN
            EXIT;
        END IF;
    END LOOP;
    
    EXECUTE IMMEDIATE 'ALTER SEQUENCE engine.seq_net_layer_device_sess_id INCREMENT BY 100';
END;

SELECT engine.seq_net_layer_device_sess_id.NEXTVAL FROM dual;



DECLARE 
    l_id NUMBER;
BEGIN   
    LOOP
        SELECT engine.seq_machine_cmd_inbound_id.NEXTVAL INTO l_id FROM DUAL;
        
        IF SUBSTR(TO_CHAR(l_id), -2) = '92' THEN
            EXIT;
        END IF;
    END LOOP;
    
    EXECUTE IMMEDIATE 'ALTER SEQUENCE engine.seq_machine_cmd_inbound_id INCREMENT BY 100';
END;

SELECT engine.seq_machine_cmd_inbound_id.NEXTVAL FROM dual;



DECLARE 
    l_id NUMBER;
BEGIN   
    LOOP
        SELECT engine.seq_machine_cmd_outbound_id.NEXTVAL INTO l_id FROM DUAL;
        
        IF SUBSTR(TO_CHAR(l_id), -2) = '92' THEN
            EXIT;
        END IF;
    END LOOP;
    
    EXECUTE IMMEDIATE 'ALTER SEQUENCE engine.seq_machine_cmd_outbound_id INCREMENT BY 100';
END;

SELECT engine.seq_machine_cmd_outbound_id.NEXTVAL FROM dual;


--POSTGRES

ALTER SEQUENCE engine.seq_net_layer_device_session_id INCREMENT 1 START <>;
ALTER SEQUENCE engine.seq_net_layer_device_session_id INCREMENT 100 NO MAXVALUE;
SELECT NEXTVAL('engine.seq_net_layer_device_session_id');

ALTER SEQUENCE engine.seq_machine_cmd_inbound_id INCREMENT 1 START <>;
ALTER SEQUENCE engine.seq_machine_cmd_inbound_id INCREMENT 100 NO MAXVALUE;
SELECT NEXTVAL('engine.seq_machine_cmd_inbound_id');

ALTER SEQUENCE engine.seq_machine_cmd_outbound_id INCREMENT 1 START <>;
ALTER SEQUENCE engine.seq_machine_cmd_outbound_id INCREMENT 100 NO MAXVALUE;
SELECT NEXTVAL('engine.seq_machine_cmd_outbound_id');


--Oracle altered sequence values
--session 43356192
--mci	115120792
--mco	116734792

select distinct substr(to_char(session_id), -2) from engine.net_layer_device_session_hist 
where session_id > (select max(session_id) from engine.net_layer_device_session_hist) - 10000

select distinct substr(to_char(inbound_id), -2) from engine.machine_cmd_inbound_hist 
where inbound_id > (select max(inbound_id) from engine.machine_cmd_inbound_hist) - 10000

select distinct substr(to_char(command_id), -2) from engine.machine_cmd_outbound_hist 
where command_id > (select max(command_id) from engine.machine_cmd_outbound_hist) - 10000

	