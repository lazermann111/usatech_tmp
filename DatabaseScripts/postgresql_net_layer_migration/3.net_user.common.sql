CREATE OR REPLACE FUNCTION engine.trbi_generic_table() RETURNS TRIGGER AS $$
BEGIN
	SELECT NOW(),
		USER,
		NOW(),
		USER
	INTO NEW.created_ts,
		NEW.created_by,
		NEW.last_updated_ts,
		NEW.last_updated_by;

   	RETURN NEW;
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION engine.trbu_generic_table() RETURNS TRIGGER AS $$
BEGIN
	SELECT OLD.created_ts,
	  	OLD.created_by,
	  	NOW(),
	  	USER
	INTO NEW.created_ts,
	  	NEW.created_by,
	  	NEW.last_updated_ts,
	  	NEW.last_updated_by;

   	RETURN NEW;
END;
$$ LANGUAGE plpgsql;