CREATE OR REPLACE FUNCTION engine.trbu_logic_engine() RETURNS "trigger"
    AS $$
BEGIN
	SELECT OLD.created_ts,
	  	OLD.created_by,
	  	NOW(),
	  	USER
	INTO NEW.created_ts,
	  	NEW.created_by,
	  	NEW.last_updated_ts,
	  	NEW.last_updated_by;

	-- logic distributed design stats default values
	IF NEW.logic_engine_active_yn_flag = 'Y' THEN
		UPDATE engine.logic_engine_stats
		SET mci_last_row_cnt = 0,
			mci_last_row_cnt_activity_ts = NOW(),
			mci_consecutive_growth_ts = NOW(),
			mci_consecutive_growth_cnt = 0,
			engine_last_activity_ts = NOW(),
			engine_session_processed_msgs = 0
		WHERE logic_engine_id = OLD.logic_engine_id;
	END IF;

   	RETURN NEW;
END;
$$
    LANGUAGE plpgsql;

DROP TRIGGER trbug_logic_engine ON engine.logic_engine;



CREATE OR REPLACE FUNCTION engine.trbi_net_layer_device_session() RETURNS "trigger"
    AS $$
BEGIN
	SELECT NOW(),
		USER,
		NOW(),
		USER
	INTO NEW.created_ts,
		NEW.created_by,
		NEW.last_updated_ts,
		NEW.last_updated_by;

	-- logic distributed design engine choice
	NEW.logic_engine_id := engine.sf_select_best_logic_engine(0);

	RETURN NEW;
END;
$$
    LANGUAGE plpgsql;
    
DROP TRIGGER trbig_net_layer_device_session ON engine.net_layer_device_session;



CREATE OR REPLACE FUNCTION engine.trbu_net_layer_device_session() RETURNS "trigger"
    AS $$
BEGIN
	SELECT OLD.created_ts,
	  	OLD.created_by,
	  	NOW(),
	  	USER
	INTO NEW.created_ts,
	  	NEW.created_by,
	  	NEW.last_updated_ts,
	  	NEW.last_updated_by;

	/* Copy completed sessions to Hist table */
	IF (NEW.session_active_yn_flag = 'N') THEN
		IF NEW.session_end_ts IS NULL THEN
		    NEW.session_end_ts := NOW();
		END IF;
		
		INSERT INTO engine.net_layer_device_session_hist(
			session_id,
			net_layer_id,
			device_name,
			session_active_yn_flag,
			logic_engine_id,
			session_start_ts,
			session_end_ts,
			session_client_ip_addr,
			session_client_port,
			session_inbound_msg_cnt,
			session_outbound_msg_cnt,
			session_inbound_byte_cnt,
			session_outbound_byte_cnt,
			created_by,
			created_ts,
			last_updated_ts,
			last_updated_by
		) VALUES (
			NEW.session_id,
			NEW.net_layer_id,
			NEW.device_name,
			NEW.session_active_yn_flag,
			NEW.logic_engine_id,
			NEW.session_start_ts,
			NEW.session_end_ts,
			NEW.session_client_ip_addr,
			NEW.session_client_port,
			NEW.session_inbound_msg_cnt,
			NEW.session_outbound_msg_cnt,
			NEW.session_inbound_byte_cnt,
			NEW.session_outbound_byte_cnt,
			NEW.created_by,
			NEW.created_ts,
			NEW.last_updated_ts,
			NEW.last_updated_by
		);
	END IF;	

	RETURN NEW;
END;
$$
    LANGUAGE plpgsql;

DROP TRIGGER trbug_net_layer_device_session ON engine.net_layer_device_session;

