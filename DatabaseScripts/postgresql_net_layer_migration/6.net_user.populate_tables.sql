-- populate device.device_type table
INSERT INTO device.device_type(device_type_id, device_type_desc, device_type_serial_cd_regex, pos_pta_tmpl_id)
VALUES(0,'G4 ePort','^E4[0-9]{6}$',0);

INSERT INTO device.device_type(device_type_id, device_type_desc, device_type_serial_cd_regex, pos_pta_tmpl_id)
VALUES(1,'G5 ePort','^G5[0-9]{6}$',1);

INSERT INTO device.device_type(device_type_id, device_type_desc, device_type_serial_cd_regex, pos_pta_tmpl_id)
VALUES(3,'ePort NG','^$',3);

INSERT INTO device.device_type(device_type_id, device_type_desc, device_type_serial_cd_regex, pos_pta_tmpl_id)
VALUES(4,'Legacy Kiosk (Sony DLL)','^((([0-9A-F]{8})((63){4}))|(S1[0-9A-F]{16}))$',4);

INSERT INTO device.device_type(device_type_id, device_type_desc, device_type_serial_cd_regex, pos_pta_tmpl_id)
VALUES(5,'eSuds Room Controller','^[0-9]{12}$',5);

INSERT INTO device.device_type(device_type_id, device_type_desc, device_type_serial_cd_regex, pos_pta_tmpl_id)
VALUES(6,'MEI ePort','^M1[0-9]{6}$',6);

INSERT INTO device.device_type(device_type_id, device_type_desc, device_type_serial_cd_regex, pos_pta_tmpl_id)
VALUES(7,'EZ80 ePort Development','^$',7);

INSERT INTO device.device_type(device_type_id, device_type_desc, device_type_serial_cd_regex, pos_pta_tmpl_id)
VALUES(8,'EZ80 ePort','^$',8);

INSERT INTO device.device_type(device_type_id, device_type_desc, device_type_serial_cd_regex, pos_pta_tmpl_id)
VALUES(9,'Legacy G4 ePort','^E4[0-9]{6}$',9);

INSERT INTO device.device_type(device_type_id, device_type_desc, device_type_serial_cd_regex, pos_pta_tmpl_id)
VALUES(10,'Transact','^$',10);

INSERT INTO device.device_type(device_type_id, device_type_desc, device_type_serial_cd_regex, pos_pta_tmpl_id)
VALUES(11,'Kiosk','^K1[0-9A-F]{16}$',11);

INSERT INTO device.device_type(device_type_id, device_type_desc, device_type_serial_cd_regex, pos_pta_tmpl_id)
VALUES(12,'T2','^T2[0-9]{6}$',12);
