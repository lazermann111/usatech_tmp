
CREATE SEQUENCE engine.rerix_message_num_seq
  INCREMENT BY 1
  START WITH 0
  MINVALUE 0
  MAXVALUE 255
  CYCLE;

CREATE SEQUENCE engine.seq_logic_engine_id
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 1000000000;

CREATE TABLE engine.logic_engine (
	logic_engine_id integer DEFAULT NEXTVAL('engine.seq_logic_engine_id') NOT NULL,
	logic_engine_desc varchar(60),
	logic_engine_active_yn_flag char(1) DEFAULT 'N' NOT NULL,
	created_by varchar(30),
	created_ts timestamp,
	last_updated_by varchar(30),
	last_updated_ts timestamp
) TABLESPACE engine_data;

ALTER TABLE engine.logic_engine ADD PRIMARY KEY (logic_engine_id);

CREATE TRIGGER gtrbi_logic_engine BEFORE INSERT ON engine.logic_engine
FOR EACH ROW EXECUTE PROCEDURE engine.trbi_generic_table();

CREATE TRIGGER gtrbu_logic_engine BEFORE UPDATE ON engine.logic_engine
FOR EACH ROW EXECUTE PROCEDURE engine.trbu_generic_table();

CREATE OR REPLACE FUNCTION engine.trbu_logic_engine() RETURNS TRIGGER AS $$
BEGIN
	-- logic distributed design stats default values
	IF NEW.logic_engine_active_yn_flag = 'Y' THEN
		UPDATE engine.logic_engine_stats
		SET mci_last_row_cnt = 0,
			mci_last_row_cnt_activity_ts = NOW(),
			mci_consecutive_growth_ts = NOW(),
			mci_consecutive_growth_cnt = 0,
			engine_last_activity_ts = NOW(),
			engine_session_processed_msgs = 0
		WHERE logic_engine_id = OLD.logic_engine_id;
	END IF;

   	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trbu_logic_engine BEFORE UPDATE ON engine.logic_engine
FOR EACH ROW EXECUTE PROCEDURE engine.trbu_logic_engine();

CREATE OR REPLACE FUNCTION engine.trai_logic_engine() RETURNS TRIGGER AS $$
BEGIN
	-- logic distributed design stats default values
	INSERT INTO engine.logic_engine_stats (
		logic_engine_id,
		mci_last_row_cnt_activity_ts,
		mci_last_row_cnt,
		mci_consecutive_growth_ts,
		mci_consecutive_growth_cnt,
		engine_last_activity_ts,
		engine_session_processed_msgs
	)
	VALUES (
		NEW.logic_engine_id,
		NOW(),
		0,
		NOW(),
		0,
		NOW(),
		0
	); 

   	RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trai_logic_engine AFTER INSERT ON engine.logic_engine
FOR EACH ROW EXECUTE PROCEDURE engine.trai_logic_engine();



CREATE TABLE engine.logic_engine_device_type_excl (
	logic_engine_id integer,
	device_type_id integer,
	created_by varchar(30),
	created_ts timestamp,
	last_updated_by varchar(30),
	last_updated_ts timestamp
) TABLESPACE engine_data;

ALTER TABLE engine.logic_engine_device_type_excl ADD CONSTRAINT fk_logic_engine_logic_engine_device_type_excl FOREIGN KEY (logic_engine_id) REFERENCES engine.logic_engine (logic_engine_id) ON DELETE SET NULL ON UPDATE CASCADE INITIALLY IMMEDIATE;

CREATE TRIGGER gtrbi_logic_engine_device_type_excl BEFORE INSERT ON engine.logic_engine_device_type_excl
FOR EACH ROW EXECUTE PROCEDURE engine.trbi_generic_table();

CREATE TRIGGER gtrbu_logic_engine_device_type_excl BEFORE UPDATE ON engine.logic_engine_device_type_excl
FOR EACH ROW EXECUTE PROCEDURE engine.trbu_generic_table();



CREATE TABLE engine.logic_engine_stats (
	logic_engine_id integer NOT NULL,
	mci_last_row_cnt_activity_ts timestamp NOT NULL,
	mci_last_row_cnt bigint NOT NULL,
	mci_consecutive_growth_ts timestamp NOT NULL,
	mci_consecutive_growth_cnt bigint DEFAULT 0,
	engine_last_activity_ts timestamp NOT NULL,
	engine_session_processed_msgs bigint DEFAULT 0,
	created_by varchar(30),
	created_ts timestamp,
	last_updated_by varchar(30),
	last_updated_ts timestamp
) TABLESPACE engine_data;

ALTER TABLE engine.logic_engine_stats ADD CONSTRAINT fk_logic_engine_logic_engine_stats FOREIGN KEY (logic_engine_id) REFERENCES engine.logic_engine (logic_engine_id) ON DELETE SET NULL ON UPDATE CASCADE INITIALLY IMMEDIATE;

CREATE TRIGGER gtrbi_logic_engine_stats BEFORE INSERT ON engine.logic_engine_stats
FOR EACH ROW EXECUTE PROCEDURE engine.trbi_generic_table();

CREATE TRIGGER gtrbu_logic_engine_stats BEFORE UPDATE ON engine.logic_engine_stats
FOR EACH ROW EXECUTE PROCEDURE engine.trbu_generic_table();




CREATE SEQUENCE engine.seq_net_layer_id
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 1000000000;

CREATE TABLE engine.net_layer (
	net_layer_id integer DEFAULT NEXTVAL('engine.seq_net_layer_id') NOT NULL,
	net_layer_desc varchar(60),
	net_layer_active_yn_flag char(1) NOT NULL,
	created_by varchar(30),
	created_ts timestamp,
	last_updated_by varchar(30),
	last_updated_ts timestamp
) TABLESPACE engine_data;

ALTER TABLE engine.net_layer ADD PRIMARY KEY (net_layer_id);

CREATE TRIGGER gtrbi_net_layer BEFORE INSERT ON engine.net_layer
FOR EACH ROW EXECUTE PROCEDURE engine.trbi_generic_table();

CREATE TRIGGER gtrbu_net_layer BEFORE UPDATE ON engine.net_layer
FOR EACH ROW EXECUTE PROCEDURE engine.trbu_generic_table();



CREATE SEQUENCE engine.seq_net_layer_device_session_id
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 1000000000000000000;

CREATE TABLE engine.net_layer_device_session (
	session_id bigint DEFAULT NEXTVAL('engine.seq_net_layer_device_session_id') NOT NULL,
	net_layer_id integer,
	device_name varchar(60) NOT NULL,
	session_active_yn_flag char(1) DEFAULT 'Y',
	logic_engine_id integer,
	session_start_ts timestamp DEFAULT NOW(),
	session_end_ts timestamp,
	session_client_ip_addr varchar(16),
	session_client_port integer,
	session_inbound_msg_cnt integer DEFAULT 0,
	session_outbound_msg_cnt integer DEFAULT 0,
	session_inbound_byte_cnt integer DEFAULT 0,
	session_outbound_byte_cnt integer DEFAULT 0,
	created_by varchar(30),
	created_ts timestamp,
	last_updated_by varchar(30),
	last_updated_ts timestamp
) TABLESPACE engine_data;

ALTER TABLE engine.net_layer_device_session ADD PRIMARY KEY (session_id);
ALTER TABLE engine.net_layer_device_session ADD CONSTRAINT fk_net_layer_net_layer_device_session FOREIGN KEY (net_layer_id) REFERENCES engine.net_layer (net_layer_id) ON DELETE SET NULL ON UPDATE CASCADE INITIALLY IMMEDIATE;
ALTER TABLE engine.net_layer_device_session ADD CONSTRAINT fk_logic_engine_net_layer_device_session FOREIGN KEY (logic_engine_id) REFERENCES engine.logic_engine (logic_engine_id) ON DELETE SET NULL ON UPDATE CASCADE INITIALLY IMMEDIATE;

CREATE TRIGGER gtrbi_net_layer_device_session BEFORE INSERT ON engine.net_layer_device_session
FOR EACH ROW EXECUTE PROCEDURE engine.trbi_generic_table();

CREATE TRIGGER gtrbu_net_layer_device_session BEFORE UPDATE ON engine.net_layer_device_session
FOR EACH ROW EXECUTE PROCEDURE engine.trbu_generic_table();

CREATE OR REPLACE FUNCTION engine.trbi_net_layer_device_session() RETURNS TRIGGER AS $$
BEGIN
	-- logic distributed design engine choice
	NEW.logic_engine_id := engine.sf_select_best_logic_engine(0);

	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trbi_net_layer_device_session BEFORE INSERT ON engine.net_layer_device_session
FOR EACH ROW EXECUTE PROCEDURE engine.trbi_net_layer_device_session();

CREATE OR REPLACE FUNCTION engine.trbu_net_layer_device_session() RETURNS TRIGGER AS $$
BEGIN
	/* Copy completed sessions to Hist table */
	IF (NEW.session_active_yn_flag = 'N') THEN
		IF NEW.session_end_ts IS NULL THEN
		    NEW.session_end_ts := NOW();
		END IF;
		
		INSERT INTO engine.net_layer_device_session_hist(
			session_id,
			net_layer_id,
			device_name,
			session_active_yn_flag,
			logic_engine_id,
			session_start_ts,
			session_end_ts,
			session_client_ip_addr,
			session_client_port,
			session_inbound_msg_cnt,
			session_outbound_msg_cnt,
			session_inbound_byte_cnt,
			session_outbound_byte_cnt,
			created_by,
			created_ts,
			last_updated_ts,
			last_updated_by
		) VALUES (
			NEW.session_id,
			NEW.net_layer_id,
			NEW.device_name,
			NEW.session_active_yn_flag,
			NEW.logic_engine_id,
			NEW.session_start_ts,
			NEW.session_end_ts,
			NEW.session_client_ip_addr,
			NEW.session_client_port,
			NEW.session_inbound_msg_cnt,
			NEW.session_outbound_msg_cnt,
			NEW.session_inbound_byte_cnt,
			NEW.session_outbound_byte_cnt,
			NEW.created_by,
			NEW.created_ts,
			NEW.last_updated_ts,
			NEW.last_updated_by
		);
	END IF;	

	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trbu_net_layer_device_session BEFORE UPDATE ON engine.net_layer_device_session
FOR EACH ROW EXECUTE PROCEDURE engine.trbu_net_layer_device_session();

CREATE OR REPLACE FUNCTION engine.tau_net_layer_device_session() RETURNS TRIGGER AS $$
BEGIN
	/* Purge net_layer_device_session of inactive sessions */
	DELETE FROM engine.net_layer_device_session WHERE session_active_yn_flag = 'N';
	
	RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER tau_net_layer_device_session AFTER UPDATE ON engine.net_layer_device_session
EXECUTE PROCEDURE engine.tau_net_layer_device_session();



CREATE TABLE engine.net_layer_device_session_hist (
	session_id bigint NOT NULL,
	net_layer_id integer,
	device_name varchar(60),
	session_active_yn_flag char(1),
	logic_engine_id integer,
	session_start_ts timestamp,
	session_end_ts timestamp,
	session_client_ip_addr varchar(16),
	session_client_port integer,
	session_inbound_msg_cnt integer DEFAULT 0,
	session_outbound_msg_cnt integer DEFAULT 0,
	session_inbound_byte_cnt integer DEFAULT 0,
	session_outbound_byte_cnt integer DEFAULT 0,
	created_by varchar(30),
	created_ts timestamp,
	last_updated_by varchar(30),
	last_updated_ts timestamp
) TABLESPACE engine_data;

ALTER TABLE engine.net_layer_device_session_hist ADD PRIMARY KEY (session_id);
ALTER TABLE engine.net_layer_device_session_hist ADD CONSTRAINT fk_net_layer_net_layer_device_session_hist FOREIGN KEY (net_layer_id) REFERENCES engine.net_layer (net_layer_id) ON DELETE SET NULL ON UPDATE CASCADE INITIALLY IMMEDIATE;
ALTER TABLE engine.net_layer_device_session_hist ADD CONSTRAINT fk_logic_engine_net_layer_device_session_hist FOREIGN KEY (logic_engine_id) REFERENCES engine.logic_engine (logic_engine_id) ON DELETE SET NULL ON UPDATE CASCADE INITIALLY IMMEDIATE;



CREATE OR REPLACE FUNCTION engine.sf_select_best_logic_engine (
    pn_session_id IN engine.net_layer_device_session.session_id%TYPE
) RETURNS engine.logic_engine.logic_engine_id%TYPE AS $$
DECLARE
    n_logic_engine_id   engine.logic_engine.logic_engine_id%TYPE;
    n_mci_consecutive_growth_max engine.logic_engine_stats.mci_consecutive_growth_cnt%TYPE := 25;
BEGIN
	SELECT logic_engine_id
	INTO n_logic_engine_id
	FROM engine.net_layer_device_session
	WHERE session_id = pn_session_id;
	
	IF n_logic_engine_id IS NULL THEN --find next best engine alternative (alive or dead)
		SELECT logic_engine_id
		INTO n_logic_engine_id
		FROM (
			SELECT rex.logic_engine_id, rex.logic_engine_active_yn_flag, MIN(resx.mci_consecutive_growth_cnt) AS grow_cnt
			FROM engine.logic_engine rex
            INNER JOIN engine.logic_engine_stats resx
                  ON rex.logic_engine_id = resx.logic_engine_id
            LEFT OUTER JOIN engine.logic_engine_device_type_excl redtex
			     ON rex.logic_engine_id = redtex.logic_engine_id
            WHERE rex.logic_engine_active_yn_flag = 'Y'
			      AND NOT (	--check for inactive engines that were unable to set 'N' flag
				  resx.engine_last_activity_ts + INTERVAL '30 seconds' < NOW()
				  AND resx.mci_consecutive_growth_cnt > n_mci_consecutive_growth_max)
			GROUP BY rex.logic_engine_id, logic_engine_active_yn_flag
			ORDER BY logic_engine_active_yn_flag, grow_cnt, logic_engine_id
		) x;
	END IF;
	
	IF n_logic_engine_id IS NULL THEN --find next best engine alternative (alive or dead)
		SELECT logic_engine_id
		INTO n_logic_engine_id
		FROM (
			SELECT rex.logic_engine_id, rex.logic_engine_active_yn_flag, MIN(resx.mci_consecutive_growth_cnt) AS grow_cnt
			FROM engine.logic_engine rex, engine.logic_engine_stats resx
			WHERE rex.logic_engine_id = resx.logic_engine_id
			AND rex.logic_engine_active_yn_flag = 'Y'
			GROUP BY rex.logic_engine_id, logic_engine_active_yn_flag
			ORDER BY logic_engine_active_yn_flag, grow_cnt, logic_engine_id
		) x;
	END IF;
	
	RETURN COALESCE(n_logic_engine_id, 0);
END;
$$ LANGUAGE plpgsql;



CREATE SEQUENCE engine.seq_machine_cmd_inbound_id
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 1000000000000000000;

CREATE TABLE engine.machine_cmd_inbound (
	inbound_id bigint DEFAULT NEXTVAL('engine.seq_machine_cmd_inbound_id') NOT NULL,
	machine_id varchar(22),
	inbound_date timestamp DEFAULT NOW() NOT NULL,
	inbound_command varchar(4000),
	timestamp timestamp,
	execute_date timestamp,
	execute_cd varchar(3) DEFAULT 'N' NOT NULL,
	inbound_msg_no integer,
	insert_ts timestamp,
	update_ts timestamp,
	original_execute_cd varchar(3),
	num_times_executed integer,
	logic_engine_id integer,
	session_id bigint,
	net_layer_id integer
) TABLESPACE engine_data;

ALTER TABLE engine.machine_cmd_inbound ADD PRIMARY KEY (inbound_id);

CREATE OR REPLACE FUNCTION engine.trbi_machine_cmd_inbound() RETURNS TRIGGER AS $$
BEGIN
	NEW.num_times_executed := 0;
	NEW.original_execute_cd := NEW.execute_cd;
	NEW.insert_ts := NOW();
	NEW.logic_engine_id := 0;

	-- logic distributed design engine choice
	IF NEW.session_id IS NULL THEN
		SELECT MAX(session_id)
		INTO NEW.session_id
		FROM engine.net_layer_device_session
		WHERE device_name = NEW.machine_id;
	END IF;
	
	NEW.logic_engine_id := engine.sf_select_best_logic_engine(NEW.session_id);

	-- logic distributed design stats update
	/*UPDATE engine.logic_engine_stats
	SET mci_last_row_cnt_activity_ts = NOW(),
		mci_consecutive_growth_cnt = mci_consecutive_growth_cnt + 1
	WHERE logic_engine_id = NEW.logic_engine_id;*/	

   	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trbi_machine_cmd_inbound BEFORE INSERT ON engine.machine_cmd_inbound
FOR EACH ROW EXECUTE PROCEDURE engine.trbi_machine_cmd_inbound();

CREATE OR REPLACE FUNCTION engine.trbu_machine_cmd_inbound() RETURNS TRIGGER AS $$
DECLARE
    v_inbound_command engine.machine_cmd_inbound.inbound_command%TYPE; 
BEGIN
	NEW.update_ts := NOW();
	NEW.execute_date := NOW();

	/* Only allow 10 tries to execute a command */
	IF NEW.num_times_executed > 10 THEN
		NEW.execute_cd := 'X';
	END IF;

	-- logic distributed design stats update
	/*IF (:NEW.execute_cd IS NOT NULL) THEN
		UPDATE engine.logic_engine_stats
		SET mci_last_row_cnt_activity_ts = DECODE(    --update when row is scheduled to be deleted (by after update trigger)
                :NEW.execute_cd,
                'N',
                mci_last_row_cnt_activity_ts,
                'F',
                mci_last_row_cnt_activity_ts,
                SYSDATE
            ),
            mci_consecutive_growth_cnt = DECODE(  --update on states logic engine uses to indicate msg completion
				:NEW.execute_cd,
				'Y',
				0,
				'E',
				0,
				'P',
				0,
				mci_consecutive_growth_cnt
			),
			engine_last_activity_ts = DECODE(  --update on all states logic engine uses
				:NEW.execute_cd,
				'Y',
				SYSDATE,
				'E',
				SYSDATE,
				'P',
				SYSDATE,
				engine_last_activity_ts
			),
			engine_session_processed_msgs = DECODE(  --update on states logic engine uses to indicate successfully handled  msgs
				:NEW.execute_cd,
				'Y',
				engine_session_processed_msgs + 1,
				'E',
				engine_session_processed_msgs + 1,
				engine_session_processed_msgs
			)
		WHERE logic_engine_id = :OLD.logic_engine_id;
    END IF;
   */
   
	/* Copy Processed messages or Errors (X) to Hist table and mark to be deleted */
	IF (NEW.execute_cd NOT IN ('N', 'F')) THEN
		/* Get rid of sensitive info to comply with PCI rule */
		IF upper(substr(NEW.inbound_command, 1, 2)) IN ('5E', 'A0') THEN
		 	v_inbound_command :=  SUBSTR(NEW.inbound_command, 1, 12);
		ELSIF UPPER(SUBSTR(NEW.inbound_command, 1, 2)) IN ('2B', '9C', 'A3', '93') THEN
		 	v_inbound_command :=  SUBSTR(NEW.inbound_command, 1, 2);
		ELSIF UPPER(SUBSTR(NEW.inbound_command, 1, 4)) = '9A5F' THEN
		 	v_inbound_command :=  SUBSTR(NEW.inbound_command, 1, 4);
		ELSE
		 	v_inbound_command :=  NEW.inbound_command;
		END IF;

		INSERT INTO engine.machine_cmd_inbound_hist
			  (inbound_id,
			   machine_id,
			   inbound_date,
			   inbound_command,
			   timestamp,
			   execute_date,
			   execute_cd,
			   inbound_msg_no,
			   num_times_executed,
			   logic_engine_id,
			   session_id,
			   net_layer_id)
		   VALUES (NEW.inbound_id,
			   NEW.machine_id,
			   NEW.inbound_date,
			   v_inbound_command,
			   NEW.TIMESTAMP,
			   NEW.execute_date,
			   NEW.execute_cd,
			   NEW.inbound_msg_no,
			   NEW.num_times_executed,
			   NEW.logic_engine_id,
			   NEW.session_id,
			   NEW.net_layer_id);

		/* Mark message complete (to be deleted) */
		NEW.execute_cd := 'C';
	END IF;

	RETURN NEW;   
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trbu_machine_cmd_inbound BEFORE UPDATE ON engine.machine_cmd_inbound
FOR EACH ROW EXECUTE PROCEDURE engine.trbu_machine_cmd_inbound();

CREATE OR REPLACE FUNCTION engine.tau_machine_cmd_inbound() RETURNS TRIGGER AS $$
DECLARE
	n_logic_engine_id engine.logic_engine.logic_engine_id%TYPE;
	n_cur_row_cnt integer;
	c_logic_engine_info CURSOR FOR
		SELECT logic_engine_id, COUNT(logic_engine_id)
		FROM engine.machine_cmd_inbound
		GROUP BY logic_engine_id
		ORDER BY logic_engine_id;
BEGIN
	/* Purge machine_cmd_inbound of completed or error messages */
	DELETE FROM engine.machine_cmd_inbound WHERE execute_cd IN ('C', 'X');

	-- logic distributed design stats update (only mci row cnt)
	/*OPEN c_logic_engine_info;
	LOOP
		FETCH c_logic_engine_info INTO n_logic_engine_id, n_cur_row_cnt;
		EXIT WHEN c_logic_engine_info%NOTFOUND;

		UPDATE engine.logic_engine_stats
		SET mci_last_row_cnt = n_cur_row_cnt
		WHERE logic_engine_id = n_logic_engine_id;
	END LOOP;
	CLOSE c_logic_engine_info;*/
	
	RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER tau_machine_cmd_inbound AFTER UPDATE ON engine.machine_cmd_inbound
EXECUTE PROCEDURE engine.tau_machine_cmd_inbound();

CREATE OR REPLACE FUNCTION engine.tai_machine_cmd_inbound() RETURNS TRIGGER AS $$
/*
DECLARE	
	n_logic_engine_id logic_engine.logic_engine_id%TYPE;
	n_cur_row_cnt integer;
	CURSOR c_logic_engine_info IS
		SELECT le.logic_engine_id, COALESCE(COUNT(mci.logic_engine_id), 0)
		INTO n_logic_engine_id, n_cur_row_cnt
		FROM engine.machine_cmd_inbound mci, engine.logic_engine le
		WHERE le.logic_engine_id = mci.logic_engine_id(+)
		GROUP BY le.logic_engine_id
		ORDER BY le.logic_engine_id;
*/
BEGIN
/*
	-- logic distributed design stats update
	OPEN c_logic_engine_info;
	LOOP
	    FETCH c_logic_engine_info INTO n_logic_engine_id, n_cur_row_cnt;
	    EXIT WHEN c_logic_engine_info%NOTFOUND;

		UPDATE engine.logic_engine_stats
		SET mci_last_row_cnt = n_cur_row_cnt,
		mci_consecutive_growth_ts = DECODE( --(d1 - d2) - ABS(d1 - d2) ==> 0=d1>=d2, !0=d1<d2
					(mci_last_row_cnt - n_cur_row_cnt) - ABS(mci_last_row_cnt - n_cur_row_cnt),
					0,
					mci_consecutive_growth_ts,	--mci_last_row_cnt >= n_cur_row_cnt
					SYSDATE --mci_last_row_cnt < n_cur_row_cnt
				)
*/
		/*mci_consecutive_growth_cnt = DECODE(
		    mci_last_row_cnt - n_cur_row_cnt,
		    0,
		    mci_consecutive_growth_cnt,  --mci_last_row_cnt = n_cur_row_cnt
		    DECODE( --(d1 - d2) - ABS(d1 - d2) ==> 0=d1>=d2, !0=d1<d2
			(mci_last_row_cnt - n_cur_row_cnt) - ABS(mci_last_row_cnt - n_cur_row_cnt),
			0,
			0,	--mci_last_row_cnt > n_cur_row_cnt
			mci_consecutive_growth_cnt + 1	--mci_last_row_cnt < n_cur_row_cnt
		    )
		)*/
/*
		WHERE logic_engine_id = n_logic_engine_id;
	END LOOP;
	CLOSE c_logic_engine_info;
*/
	RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER tai_machine_cmd_inbound AFTER INSERT ON engine.machine_cmd_inbound
EXECUTE PROCEDURE engine.tai_machine_cmd_inbound();



CREATE TABLE engine.machine_cmd_inbound_hist (
	inbound_id bigint NOT NULL,
	machine_id varchar(22),
	inbound_date timestamp,
	inbound_command varchar(4000),
	timestamp timestamp,
	execute_date timestamp,
	execute_cd varchar(3),
	inbound_msg_no integer,
	num_times_executed integer,
	logic_engine_id integer,
	session_id bigint,
	net_layer_id integer
) TABLESPACE engine_data;

ALTER TABLE engine.machine_cmd_inbound_hist ADD PRIMARY KEY (inbound_id);
ALTER TABLE engine.machine_cmd_inbound_hist ADD CONSTRAINT fk_logic_engine_machine_cmd_inbound_hist FOREIGN KEY (logic_engine_id) REFERENCES engine.logic_engine (logic_engine_id) ON DELETE SET NULL ON UPDATE CASCADE INITIALLY IMMEDIATE;

CREATE INDEX idx_mcih_inbound_date ON engine.machine_cmd_inbound_hist (inbound_date) TABLESPACE engine_indx;
CREATE INDEX idx_mcih_machine_id ON engine.machine_cmd_inbound_hist (machine_id) TABLESPACE engine_indx;



CREATE SEQUENCE engine.seq_machine_cmd_outbound_id
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 1000000000000000000;

CREATE TABLE engine.machine_cmd_outbound (
	command_id bigint DEFAULT NEXTVAL('engine.seq_machine_cmd_outbound_id') NOT NULL,
	modem_id varchar(20),
	command varchar(4000),
	command_date timestamp,
	execute_date timestamp,
	execute_cd varchar(3) DEFAULT 'N' NOT NULL,
	session_id bigint,
	net_layer_id integer
) TABLESPACE engine_data;

ALTER TABLE engine.machine_cmd_outbound ADD PRIMARY KEY (command_id);

CREATE OR REPLACE FUNCTION engine.trbu_machine_cmd_outbound() RETURNS TRIGGER AS $$
BEGIN
	NEW.execute_date := NOW();
	
	/* Move Errors to Hist table */
	IF SUBSTR(NEW.execute_cd, 1, 1) = 'E' THEN
		INSERT INTO engine.machine_cmd_outbound_hist(
			command_id,
			modem_id,
			command,
			command_date,
			execute_date,
			execute_cd,
			session_id,
			net_layer_id
		) VALUES (
			NEW.command_id,
			NEW.modem_id,
			NEW.command,
			NEW.command_date,
			NEW.execute_date,
			NEW.execute_cd,
			NEW.session_id,
			NEW.net_layer_id
		);
		/* Change execute_cd to X */
		NEW.execute_cd := 'X';
	END IF;

   	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trbu_machine_cmd_outbound BEFORE UPDATE ON engine.machine_cmd_outbound
FOR EACH ROW EXECUTE PROCEDURE engine.trbu_machine_cmd_outbound();

CREATE OR REPLACE FUNCTION engine.trau_machine_cmd_outbound() RETURNS TRIGGER AS $$
BEGIN
	IF NEW.execute_cd = 'Y' THEN
		INSERT INTO engine.machine_cmd_outbound_hist (
			command_id,
			modem_id,
			command,
			command_date,
			execute_date,
			execute_cd,
			session_id,
			net_layer_id
		) VALUES (
			NEW.command_id,
			NEW.Modem_id,
			NEW.command,
			NEW.command_date,
			NEW.execute_date,
			NEW.execute_cd,
			NEW.session_id,
			NEW.net_layer_id
		);
	END IF;

   	RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trau_machine_cmd_outbound AFTER UPDATE ON engine.machine_cmd_outbound
FOR EACH ROW EXECUTE PROCEDURE engine.trau_machine_cmd_outbound();

CREATE OR REPLACE FUNCTION engine.tau_machine_cmd_outbound() RETURNS TRIGGER AS $$
BEGIN
	/* Y's will be saved in Machine_command_hist - C's will be deleted - X's are Errors*/
	DELETE FROM engine.machine_cmd_outbound WHERE execute_cd in  ('Y','C', 'X');
	
	RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER tau_machine_cmd_outbound AFTER UPDATE ON engine.machine_cmd_outbound
EXECUTE PROCEDURE engine.tau_machine_cmd_outbound();



CREATE TABLE engine.machine_cmd_outbound_hist (
	command_id bigint NOT NULL,
	modem_id varchar(20),
	command varchar(4000),
	command_date timestamp,
	execute_date timestamp,
	execute_cd varchar(3),
	session_id bigint,
	net_layer_id integer
) TABLESPACE engine_data;

ALTER TABLE engine.machine_cmd_outbound_hist ADD PRIMARY KEY (command_id);

CREATE INDEX idx_mcoh_modem_id ON engine.machine_cmd_outbound_hist (modem_id) TABLESPACE engine_indx;
CREATE INDEX idx_mcoh_execute_date ON engine.machine_cmd_outbound_hist (execute_date) TABLESPACE engine_indx;


/*
CREATE SEQUENCE engine.seq_machine_cmd_pending_id
  INCREMENT BY 1
  START WITH 1
  MINVALUE 1
  MAXVALUE 1000000000000000000;

CREATE TABLE engine.machine_cmd_pending (
	machine_command_pending_id bigint DEFAULT NEXTVAL('engine.seq_machine_cmd_pending_id') NOT NULL,
	machine_id varchar(22) NOT NULL,
	message_number integer,
	data_type varchar(8) NOT NULL,
	command varchar(4000),
	execute_date timestamp,
	execute_cd varchar(3) DEFAULT 'P',
	execute_order integer DEFAULT 999,
	created_by varchar(30),
	created_ts timestamp,
	last_updated_by varchar(30),
	last_updated_ts timestamp
) TABLESPACE engine_data;

ALTER TABLE engine.machine_cmd_pending ADD PRIMARY KEY (machine_command_pending_id);

CREATE INDEX idx_mcp_machine_id ON engine.machine_cmd_pending (machine_id) TABLESPACE engine_indx;

CREATE TRIGGER gtrbi_machine_cmd_pending BEFORE INSERT ON engine.machine_cmd_pending
FOR EACH ROW EXECUTE PROCEDURE engine.trbi_generic_table();

CREATE TRIGGER gtrbu_machine_cmd_pending BEFORE UPDATE ON engine.machine_cmd_pending
FOR EACH ROW EXECUTE PROCEDURE engine.trbu_generic_table();

CREATE OR REPLACE FUNCTION engine.trbu_machine_cmd_pending() RETURNS TRIGGER AS $$
BEGIN
	IF NEW.EXECUTE_CD IN ('S', 'A') THEN
	   NEW.EXECUTE_DATE = NOW();
    	END IF;
	
	IF NEW.EXECUTE_CD IN ('A', 'C') THEN
		INSERT INTO engine.machine_command_pending_hist
		(
		    MACHINE_COMMAND_PENDING_ID,
		    MACHINE_ID,
		    MESSAGE_NUMBER,
		    DATA_TYPE,
		    COMMAND,
		    EXECUTE_DATE,
		    EXECUTE_CD,
		    EXECUTE_ORDER,
		    CREATED_BY,
		    CREATED_TS,
		    LAST_UPDATED_BY,
		    LAST_UPDATED_TS
		)
		VALUES
		(
		    NEW.MACHINE_COMMAND_PENDING_ID,
		    NEW.MACHINE_ID,
		    NEW.MESSAGE_NUMBER,
		    NEW.DATA_TYPE,
		    NEW.COMMAND,
		    NEW.EXECUTE_DATE,
		    NEW.EXECUTE_CD,
		    NEW.EXECUTE_ORDER,
		    NEW.CREATED_BY,
		    NEW.CREATED_TS,
		    NEW.LAST_UPDATED_BY,
		    NEW.LAST_UPDATED_TS
		);
    	END IF;

   	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trbu_machine_cmd_pending BEFORE UPDATE ON engine.machine_cmd_pending
FOR EACH ROW EXECUTE PROCEDURE engine.trbu_machine_cmd_pending();

CREATE OR REPLACE FUNCTION engine.tau_machine_cmd_pending() RETURNS TRIGGER AS $$
BEGIN
	DELETE FROM engine.machine_command_pending WHERE execute_cd in ('A', 'C');
	
	RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER tau_machine_cmd_pending AFTER UPDATE ON engine.machine_cmd_pending
EXECUTE PROCEDURE engine.tau_machine_cmd_pending();



CREATE TABLE engine.machine_cmd_pending_hist (
	machine_command_pending_id bigint NOT NULL,
	machine_id varchar(22) NOT NULL,
	message_number integer,
	data_type varchar(8) NOT NULL,
	command varchar(4000),
	execute_date timestamp,
	execute_cd varchar(3) DEFAULT 'P',
	execute_order integer DEFAULT 999,
	created_by varchar(30),
	created_ts timestamp,
	last_updated_by varchar(30),
	last_updated_ts timestamp
) TABLESPACE engine_data;
*/


-- STORED PROCEDURES

CREATE OR REPLACE FUNCTION engine.sp_machine_command_in_cleanup() RETURNS void AS $$
BEGIN
	/* Move Errors to Hist table */
	INSERT INTO engine.machine_cmd_inbound_hist
		  (inbound_id,
		   machine_id,
		   inbound_date,
		   inbound_command,
		   timestamp,
		   execute_date,
		   execute_cd,
		   inbound_msg_no,
		   num_times_executed)
	SELECT inbound_id,
		  machine_id,
		  inbound_date,
		  inbound_command,
		  timestamp,
		  execute_date,
		  execute_cd,
		  inbound_msg_no,
		  num_times_executed
	FROM engine.machine_cmd_inbound
	WHERE SUBSTR(execute_cd, 1, 1) NOT IN ('N', 'F');

	/* Y's will be saved in Machine_command_hist - E's are Errors - P's are purged */
	DELETE FROM engine.machine_cmd_inbound
	WHERE execute_cd in ('Y', 'E', 'P')
	OR inbound_date < NOW() - INTERVAL '10 minutes';  --older than 10 minutes
END;
$$ LANGUAGE plpgsql;



CREATE OR REPLACE FUNCTION engine.sp_machine_command_out_cleanup() RETURNS void AS $$
BEGIN
	INSERT INTO engine.machine_cmd_outbound_hist
	(command_id, modem_id, command, command_date, execute_date, execute_cd)
	SELECT command_id, modem_id, command, command_date, execute_date, execute_cd
	FROM engine.machine_cmd_outbound
	WHERE execute_cd IN ('Y', 'E');

	/* Y's will be saved in Machine_command_hist - C's will be deleted - E's are Errors*/
	DELETE FROM engine.machine_cmd_outbound WHERE execute_cd in  ('Y', 'C', 'E');
END;
$$ LANGUAGE plpgsql;


-- PKG_ENGINE_MAINT

CREATE OR REPLACE FUNCTION engine.sp_start_net_layer(
      	pn_stored_net_layer_id IN engine.net_layer.net_layer_id%TYPE,
      	ps_net_layer_desc IN engine.net_layer.net_layer_desc%TYPE,
      	pn_new_net_layer_id OUT engine.net_layer.net_layer_id%TYPE
) AS $$
DECLARE
       	vn_net_layer_active_yn_flag engine.net_layer.net_layer_active_yn_flag%TYPE;
BEGIN
	IF pn_stored_net_layer_id IS NULL OR pn_stored_net_layer_id <= 0 THEN
	 	pn_new_net_layer_id := NEXTVAL('ENGINE.SEQ_NET_LAYER_ID');
	 	INSERT INTO engine.net_layer(net_layer_id, net_layer_desc, net_layer_active_yn_flag) VALUES (pn_new_net_layer_id,ps_net_layer_desc, 'Y');
	ELSE
		SELECT net_layer_active_yn_flag
		INTO vn_net_layer_active_yn_flag
		FROM engine.net_layer
		WHERE net_layer_id = pn_stored_net_layer_id;

		IF vn_net_layer_active_yn_flag = 'Y' THEN
			UPDATE engine.net_layer
			SET net_layer_active_yn_flag = 'N'
			WHERE net_layer_id = pn_stored_net_layer_id;
		END IF;

		UPDATE engine.net_layer
		SET net_layer_active_yn_flag = 'Y',
			net_layer_desc = ps_net_layer_desc
		WHERE net_layer_id = pn_stored_net_layer_id;

		pn_new_net_layer_id := pn_stored_net_layer_id;
	END IF;
END;
$$ LANGUAGE plpgsql;



CREATE OR REPLACE FUNCTION engine.sp_stop_net_layer(
	pn_stored_net_layer_id IN engine.net_layer.net_layer_id%TYPE
) RETURNS void AS $$
BEGIN
	UPDATE engine.net_layer
	SET net_layer_active_yn_flag = 'N'
	WHERE net_layer_id = pn_stored_net_layer_id;
END;
$$ LANGUAGE plpgsql;



CREATE OR REPLACE FUNCTION engine.sp_start_device_session(
	pn_net_layer_id IN engine.net_layer_device_session.net_layer_id%TYPE,
	ps_device_name IN engine.net_layer_device_session.device_name%TYPE,
	ps_client_ip_addr IN engine.net_layer_device_session.session_client_ip_addr%TYPE,
	ps_client_port IN engine.net_layer_device_session.session_client_port%TYPE,
	pn_session_id OUT engine.net_layer_device_session.session_id%TYPE
) AS $$
DECLARE
	--v_device_type_id    device.device_type_id%TYPE;
BEGIN
      	pn_session_id := NEXTVAL('engine.seq_net_layer_device_session_id');
        
	INSERT INTO engine.net_layer_device_session
	(
		 session_id,
		 net_layer_id,
		 device_name,
		 session_client_ip_addr,
		 session_client_port
	)
	VALUES
	(
		 pn_session_id,
		 pn_net_layer_id,
		 ps_device_name,
		 ps_client_ip_addr,
		 ps_client_port
	);
/*      
	-- Flush the outbound queue here to clear out any old messages

	-- EV000 indicates the default EV which will not be in the database
	-- We want to flush the outbound queue in ALL cases for the default EV
	IF ps_device_name LIKE 'EV000%' THEN
		v_device_type_id := -1;
	ELSE
	    	SELECT device_type_id
	      	INTO v_device_type_id
	      	FROM device.device
	     	WHERE device_name = ps_device_name
	       		AND device_active_yn_flag = 'Y';
	       		
	       	v_device_type_id := COALESCE(v_device_type_id, -1);
	END IF;

	-- dont't flush these types...
	IF v_device_type_id IN (0, 1, 4, 5) THEN
		RETURN;
	END IF;

	UPDATE ENGINE.MACHINE_CMD_OUTBOUND
	SET EXECUTE_CD = 'E'
	WHERE MODEM_ID = ps_device_name;
*/
END;
$$ LANGUAGE plpgsql;



CREATE OR REPLACE FUNCTION engine.sp_stop_device_session(
      	pn_session_id IN engine.net_layer_device_session.session_id%TYPE,
      	pn_inbound_msg_cnt IN engine.net_layer_device_session.session_inbound_msg_cnt%TYPE,
      	pn_outbound_msg_cnt IN engine.net_layer_device_session.session_outbound_msg_cnt%TYPE,
      	pn_inbound_byte_cnt IN engine.net_layer_device_session.session_inbound_byte_cnt%TYPE,
      	pn_outbound_byte_cnt IN engine.net_layer_device_session.session_outbound_byte_cnt%TYPE
) RETURNS void AS $$
BEGIN
	UPDATE engine.net_layer_device_session
	SET session_active_yn_flag = 'N',
		session_inbound_msg_cnt = pn_inbound_msg_cnt,
		session_outbound_msg_cnt = pn_outbound_msg_cnt,
		session_inbound_byte_cnt = pn_inbound_byte_cnt,
		session_outbound_byte_cnt = pn_outbound_byte_cnt
	WHERE session_id = pn_session_id;
END;
$$ LANGUAGE plpgsql;