CREATE LANGUAGE plpgsql;

CREATE USER net_user WITH LOGIN PASSWORD '<password>';

CREATE SCHEMA device AUTHORIZATION net_user;
CREATE SCHEMA engine AUTHORIZATION net_user;

CREATE TABLESPACE engine_data LOCATION '/usr/local/pgsql/tblspace/engine_data';
CREATE TABLESPACE engine_indx LOCATION '/usr/local/pgsql/tblspace/engine_indx';
CREATE TABLESPACE device_data LOCATION '/usr/local/pgsql/tblspace/device_data';
CREATE TABLESPACE device_indx LOCATION '/usr/local/pgsql/tblspace/device_indx';

GRANT CREATE ON TABLESPACE engine_data TO net_user;
GRANT CREATE ON TABLESPACE engine_indx TO net_user;
GRANT CREATE ON TABLESPACE device_data TO net_user;
GRANT CREATE ON TABLESPACE device_indx TO net_user;
