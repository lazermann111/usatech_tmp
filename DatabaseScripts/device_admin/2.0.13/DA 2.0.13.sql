ALTER TABLE PSS.AUTH ADD(AUTH_ACTION_ID NUMBER(20), AUTH_ACTION_BITMAP NUMBER, CONSTRAINT FK_AUTH_ACTION_ID FOREIGN KEY (AUTH_ACTION_ID) REFERENCES DEVICE.ACTION(ACTION_ID));

UPDATE device.file_transfer_type SET file_transfer_type_name = file_transfer_type_name || ' (automatic)' WHERE file_transfer_type_name = 'DEX File for Device Download';

INSERT INTO device.file_transfer_type(file_transfer_type_cd, file_transfer_type_name, file_transfer_type_desc)
VALUES(25, 'DEX File for Device Download (manual)', 'DEX file intended to be downloaded by the device and uploaded by the device to the vending machine. A manual user action is required to complete the upload.');

COMMIT;
