/* Formatted on 2006/05/31 12:41 (Formatter Plus v4.8.0) */
UPDATE aramark_payment_type
   SET aramark_payment_type_desc = 'UNCG Student Card'
 WHERE aramark_payment_type_desc = 'UNCG Special Card';

UPDATE pos_pta
   SET pos_pta_passthru_allow_yn_flag = 'N';
UPDATE pos_pta_tmpl_entry
   SET pos_pta_passthru_allow_yn_flag = 'N';

ALTER TABLE pos_pta MODIFY pos_pta_passthru_allow_yn_flag DEFAULT 'N' NOT NULL;
ALTER TABLE pos_pta_tmpl_entry MODIFY pos_pta_passthru_allow_yn_flag DEFAULT 'N' NOT NULL;

BEGIN
   DECLARE
      v_authority_payment_mask_id   authority_payment_mask.authority_payment_mask_id%TYPE;
      v_payment_subtype_id          payment_subtype.payment_subtype_id%TYPE;
   BEGIN
/* create payment_subtype for Aramark Debit */
      SELECT seq_authority_payment_mask_id.NEXTVAL
        INTO v_authority_payment_mask_id
        FROM DUAL;

      INSERT INTO authority_payment_mask
                  (authority_payment_mask_id, authority_payment_mask_name,
                   authority_payment_mask_desc,
                   authority_payment_mask_regex,
                   authority_payment_mask_bref
                  )
           VALUES (v_authority_payment_mask_id, 'Default',
                   'Credit Card (Track 2) - Aramark Debit',
                   '^(;?)([0-9]{1,19})=([0-9]{4})([0-9]*)(\??)$',
                   '1:7|2:1|3:3|4:7|5:7'
                  );

      SELECT seq_payment_subtype_id.NEXTVAL
        INTO v_payment_subtype_id
        FROM DUAL;

      INSERT INTO payment_subtype
                  (payment_subtype_id,
                   payment_subtype_name, payment_subtype_class,
                   payment_subtype_key_name, client_payment_type_cd,
                   payment_subtype_table_name, payment_subtype_key_desc_name,
                   authority_payment_mask_id
                  )
           VALUES (v_payment_subtype_id,
                   'Credit Card (Track 2) - Aramark Debit', 'Aramark',
                   'ARAMARK_PAYMENT_TYPE_ID', 'C',
                   'ARAMARK_PAYMENT_TYPE', 'ARAMARK_PAYMENT_TYPE_DESC',
                   v_authority_payment_mask_id
                  );

      UPDATE authority_payment_mask
         SET payment_subtype_id = v_payment_subtype_id
       WHERE authority_payment_mask_id = v_authority_payment_mask_id;

/* create payment_subtype for Aramark UNCG */
      SELECT seq_authority_payment_mask_id.NEXTVAL
        INTO v_authority_payment_mask_id
        FROM DUAL;

      INSERT INTO authority_payment_mask
                  (authority_payment_mask_id, payment_subtype_id,
                   authority_payment_mask_name, authority_payment_mask_desc,
                   authority_payment_mask_regex,
                   authority_payment_mask_bref
                  )
           VALUES (v_authority_payment_mask_id, v_payment_subtype_id,
                   'UNCG FirstCard Plus Debit', NULL,
                   '^(;?)(4385[0-9]{12})=([0-9]{4})([0-9]*)(\??)$',
                   '1:7|2:1|3:3|4:7|5:7'
                  );
   END;
END;
/
