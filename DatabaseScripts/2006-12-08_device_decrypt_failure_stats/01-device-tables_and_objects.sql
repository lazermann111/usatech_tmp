--add encr key last set ts to device table
ALTER TABLE DEVICE ADD (DEVICE_ENCR_KEY_GEN_TS DATE);

UPDATE DEVICE SET device_encr_key_gen_ts = created_ts;

ALTER TABLE DEVICE MODIFY DEVICE_ENCR_KEY_GEN_TS not null;
CREATE INDEX IDX_DEVICE_2 ON DEVICE(DEVICE_ID,CREATED_TS) TABLESPACE DEVICE_INDX;


-- update device table triggers
CREATE OR REPLACE TRIGGER TRBU_DEVICE BEFORE UPDATE ON DEVICE
  FOR EACH ROW 
BEGIN
   IF (:OLD.encryption_key IS NULL AND :NEW.encryption_key IS NOT NULL)
      OR (:OLD.encryption_key IS NOT NULL AND :NEW.encryption_key IS NULL)
      OR (:OLD.encryption_key <> :NEW.encryption_key)
   THEN
      :NEW.device_encr_key_gen_ts := SYSDATE;
   END IF;
   SELECT :OLD.created_by, :OLD.created_ts, SYSDATE,
          USER
     INTO :NEW.created_by, :NEW.created_ts, :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER TRBI_DEVICE BEFORE INSERT ON DEVICE
  FOR EACH ROW 
BEGIN
   IF :NEW.device_id IS NULL
   THEN
      SELECT seq_device_id.NEXTVAL
        INTO :NEW.device_id
        FROM DUAL;
   END IF;
   SELECT SYSDATE, USER, SYSDATE,
          USER, NVL (:NEW.device_active_yn_flag, 'Y'),
          SYSDATE
     INTO :NEW.created_ts, :NEW.created_by, :NEW.last_updated_ts,
          :NEW.last_updated_by, :NEW.device_active_yn_flag,
          :NEW.device_encr_key_gen_ts
     FROM DUAL;
END;
/


-- new failure type table
CREATE TABLE DEVICE.DEVICE_DECODE_FAIL_TYPE
  (
    DEVICE_DECODE_FAIL_TYPE_ID    NUMBER(20,0),
    DEVICE_DECODE_FAIL_TYPE_NAME  VARCHAR2(60)    not null,
    DEVICE_DECODE_FAIL_TYPE_DESC  VARCHAR2(255)   not null,
    CONSTRAINT PK_DEVICE_DECODE_FAIL_TYPE primary key(DEVICE_DECODE_FAIL_TYPE_ID) USING INDEX TABLESPACE DEVICE_INDX
  )
  TABLESPACE DEVICE_DATA;

REM ----------------------------------------------------------------------

COMMENT ON TABLE DEVICE_DECODE_FAIL_TYPE is 'Types of device message decoding errors (such as decryption or other errors).';

REM ----------------------------------------------------------------------

INSERT INTO DEVICE_DECODE_FAIL_TYPE VALUES (1, 'DECRYPTION_FAILURE', 'General decryption failure');


-- new sequence
CREATE SEQUENCE SEQ_DEVICE_DECODE_FAIL_ID
    INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH 1;


--new failure event table
CREATE TABLE DEVICE.DEVICE_DECODE_FAIL
  (
    DEVICE_DECODE_FAIL_ID       NUMBER(20,0),
    DEVICE_ID                   NUMBER(20,0)   not null,
    DEVICE_DECODE_FAIL_TYPE_ID  NUMBER(20,0)   not null,
    CREATED_BY                  VARCHAR2(30)   not null,
    CREATED_TS                  DATE           not null,
    LAST_UPDATED_BY             VARCHAR2(30)   not null,
    LAST_UPDATED_TS             DATE           not null,
    CONSTRAINT PK_DEVICE_DECODE_FAIL primary key(DEVICE_DECODE_FAIL_ID) USING INDEX TABLESPACE DEVICE_INDX,
    CONSTRAINT FK_DDF_DEVICE_ID foreign key(DEVICE_ID) references DEVICE(DEVICE_ID),
    CONSTRAINT FK_DDF_DDFT_ID foreign key(DEVICE_DECODE_FAIL_TYPE_ID) references DEVICE_DECODE_FAIL_TYPE(DEVICE_DECODE_FAIL_TYPE_ID)
  )
  TABLESPACE DEVICE_DATA;

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER TRBI_DEVICE_DECODE_FAIL BEFORE INSERT ON DEVICE_DECODE_FAIL
  FOR EACH ROW 
BEGIN
	IF :NEW.device_decode_fail_id IS NULL
	THEN
		SELECT SEQ_DEVICE_DECODE_FAIL_ID.NEXTVAL
		INTO :NEW.device_decode_fail_id
		FROM DUAL;
	END IF;
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

CREATE OR REPLACE TRIGGER TRBU_DEVICE_DECODE_FAIL BEFORE UPDATE ON DEVICE_DECODE_FAIL
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

REM ----------------------------------------------------------------------

COMMENT ON TABLE DEVICE_DECODE_FAIL is 'Table used to track events related to decryption or other message-related decoding failures.';

CREATE INDEX IDX_DEVICE_DECODE_FAIL_1 ON DEVICE_DECODE_FAIL(CREATED_TS,DEVICE_ID) TABLESPACE DEVICE_INDX;

COMMIT;

GRANT SELECT ON DEVICE.DEVICE_DECODE_FAIL_TYPE TO WEB_USER;
GRANT SELECT, INSERT, UPDATE ON DEVICE.DEVICE_DECODE_FAIL TO WEB_USER;
GRANT SELECT ON DEVICE.SEQ_DEVICE_DECODE_FAIL_ID TO WEB_USER;

GRANT SELECT ON DEVICE.DEVICE_DECODE_FAIL_TYPE TO USAT_WEB_ROLE;
GRANT SELECT, INSERT, UPDATE ON DEVICE.DEVICE_DECODE_FAIL TO USAT_WEB_ROLE;
GRANT SELECT ON DEVICE.SEQ_DEVICE_DECODE_FAIL_ID TO USAT_WEB_ROLE;
