CREATE INDEX PSS.IDX_TRAN_PARENT_TRAN_ID ON PSS.TRAN(PARENT_TRAN_ID) LOCAL TABLESPACE PSS_INDX ONLINE;

CREATE INDEX DEVICE.IDX_DEVICE_SETTING_PARAM_VALUE ON DEVICE.DEVICE_SETTING(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_VALUE) TABLESPACE DEVICE_INDX ONLINE;
DROP INDEX DEVICE.IDX_DEVICE_SETTING_PARAM_CD;

ALTER TABLE PSS.AUTH ADD(CARD_KEY VARCHAR2(100));
ALTER TABLE DEVICE.DEVICE_SETTING ADD (FILE_ORDER NUMBER(20, 0));
ALTER TABLE DEVICE.CONFIG_TEMPLATE_SETTING ADD (FILE_ORDER NUMBER(20, 0));

INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD)
   SELECT CTS.DEVICE_SETTING_PARAMETER_CD
     FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS
    WHERE CTS.DEVICE_TYPE_ID = 0 AND CTS.FIELD_OFFSET < 512 
      AND NOT EXISTS(SELECT 1 FROM DEVICE.DEVICE_SETTING_PARAMETER DSP WHERE DSP.DEVICE_SETTING_PARAMETER_CD = CTS.DEVICE_SETTING_PARAMETER_CD)
    ORDER BY CTS.FIELD_OFFSET;
COMMIT;

CREATE TABLE DEVICE.TMP_FT_HELP
AS
  SELECT SUBSTR(FT.FILE_TRANSFER_NAME, 1, LENGTH(FT.FILE_TRANSFER_NAME) - 4) DEVICE_NAME, FT.FILE_TRANSFER_ID
		FROM DEVICE.FILE_TRANSFER FT 
		WHERE FT.FILE_TRANSFER_NAME LIKE 'EV%-CFG'
        AND FT.FILE_TRANSFER_TYPE_CD IN(1, 101);
COMMIT;

CREATE INDEX DEVICE.IX_TFTH_DEVICE_NAME ON DEVICE.TMP_FT_HELP(DEVICE_NAME);

DECLARE
	CURSOR cur IS 
		select d.device_id, d.device_name, d.device_type_id
		from device.vw_device_last_active d
		where d.device_type_id in (0, 1)
		order by d.device_id;
		
	ll_default_content LONG;
	lv_default_content_g4 VARCHAR2(1024);
	lv_default_content_g5 VARCHAR2(1024);
	ll_content LONG;
	lv_content VARCHAR2(1024);	
BEGIN
	SELECT file_transfer_content
	INTO ll_default_content
	FROM device.file_transfer
	WHERE file_transfer_type_cd = 6 
		AND file_transfer_name = 'G4-DEFAULT-CFG';
		
	lv_default_content_g4 := UPPER(ll_default_content);

	SELECT file_transfer_content
	INTO ll_default_content
	FROM device.file_transfer
	WHERE file_transfer_type_cd = 6 
		AND file_transfer_name = 'G5-DEFAULT-CFG';
		
	lv_default_content_g5 := UPPER(ll_default_content);

	FOR rec_cur IN cur LOOP
		BEGIN
			ll_content := NULL;
		
			SELECT file_transfer_content
			INTO ll_content
			FROM (
				SELECT ft.file_transfer_content
				FROM device.file_transfer ft 
                JOIN DEVICE.TMP_FT_HELP h on ft.file_transfer_id = h.file_transfer_id
				WHERE h.device_name = rec_cur.device_name
					AND file_content_length(ft.rowid) = 1024
				ORDER BY ft.file_transfer_type_cd, ft.created_ts
			) WHERE ROWNUM = 1;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				NULL;
			WHEN VALUE_ERROR THEN
				NULL;
		END;
		
		IF ll_content IS NULL THEN
			IF rec_cur.device_type_id = 0 THEN 
				lv_content := lv_default_content_g4; 
			ELSE
				lv_content := lv_default_content_g5;
			END IF;
		ELSE
			lv_content := UPPER(ll_content);
		END IF;
			
		DELETE FROM device.device_setting
		WHERE device_id = rec_cur.device_id
			AND device_setting_parameter_cd IN (
				SELECT device_setting_parameter_cd
				FROM device.device_setting_parameter
				WHERE device_setting_ui_configurable = 'Y');
	
		MERGE INTO device.device_setting O
            USING (
              SELECT rec_cur.device_id DEVICE_ID,
                     device_setting_parameter_cd, field_offset * 2 + 1 hex_field_offset, field_size * 2 hex_field_size,
                     REPLACE(SUBSTR(lv_content, field_offset * 2 + 1, field_size * 2), ' ', '0') DEVICE_SETTING_VALUE
                from device.config_template_setting 
                where device_type_id = 0 and field_offset < 512) N
              ON (O.DEVICE_ID = N.DEVICE_ID AND O.device_setting_parameter_cd = N.device_setting_parameter_cd)
              WHEN MATCHED THEN
               UPDATE
                  SET O.DEVICE_SETTING_VALUE = N.DEVICE_SETTING_VALUE
              WHEN NOT MATCHED THEN
               INSERT (O.device_id, O.device_setting_parameter_cd, O.device_setting_value)
                VALUES(N.device_id, N.device_setting_parameter_cd, N.device_setting_value);			
		
		COMMIT;
	END LOOP;
END;
/

INSERT INTO DEVICE.DEVICE_SETTING_PARAMETER(DEVICE_SETTING_PARAMETER_CD)
   SELECT CTS.DEVICE_SETTING_PARAMETER_CD
     FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS
    WHERE CTS.DEVICE_TYPE_ID = 6 and CTS.FIELD_OFFSET < 57 
      AND NOT EXISTS(SELECT 1 FROM DEVICE.DEVICE_SETTING_PARAMETER DSP WHERE DSP.DEVICE_SETTING_PARAMETER_CD = CTS.DEVICE_SETTING_PARAMETER_CD)
    ORDER BY CTS.FIELD_OFFSET;
COMMIT;

DECLARE
	CURSOR cur IS 
		select d.device_id, d.device_name
		from device.vw_device_last_active d
		where d.device_type_id = 6
		order by d.device_id;
		
	ll_default_content LONG;
	lv_default_content VARCHAR2(114);
	ll_content LONG;
	lv_content VARCHAR2(114);	
BEGIN
	SELECT file_transfer_content
	INTO ll_default_content
	FROM device.file_transfer
	WHERE file_transfer_type_cd = 6 
		AND file_transfer_name = 'MEI-DEFAULT-CFG';
		
	lv_default_content := UPPER(ll_default_content);

	FOR rec_cur IN cur LOOP
		BEGIN
			ll_content := NULL;
		
			SELECT file_transfer_content
			INTO ll_content
			FROM (
				SELECT ft.file_transfer_content
				FROM device.file_transfer ft 
                JOIN DEVICE.TMP_FT_HELP h on ft.file_transfer_id = h.file_transfer_id
				WHERE h.device_name = rec_cur.device_name
					AND file_content_length(ft.rowid) = 114
				ORDER BY ft.file_transfer_type_cd, ft.created_ts
			) WHERE ROWNUM = 1;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				NULL;
			WHEN VALUE_ERROR THEN
				NULL;
		END;
		
		IF ll_content IS NULL THEN
			lv_content := lv_default_content;
		ELSE
			lv_content := UPPER(ll_content);
		END IF;
			
		DELETE FROM device.device_setting
		WHERE device_id = rec_cur.device_id
			AND device_setting_parameter_cd IN (
				SELECT device_setting_parameter_cd
				FROM device.device_setting_parameter
				WHERE device_setting_ui_configurable = 'Y');
	
		MERGE INTO device.device_setting O
            USING (
              SELECT rec_cur.device_id DEVICE_ID,
                     device_setting_parameter_cd, field_offset * 2 + 1 hex_field_offset, field_size * 2 hex_field_size,
                     REPLACE(SUBSTR(lv_content, field_offset * 2 + 1, field_size * 2), ' ', '0') DEVICE_SETTING_VALUE
                from device.config_template_setting 
                where device_type_id = 6 and field_offset < 57) N
              ON (O.DEVICE_ID = N.DEVICE_ID AND O.device_setting_parameter_cd = N.device_setting_parameter_cd)
              WHEN MATCHED THEN
               UPDATE
                  SET O.DEVICE_SETTING_VALUE = N.DEVICE_SETTING_VALUE
              WHEN NOT MATCHED THEN
               INSERT (O.device_id, O.device_setting_parameter_cd, O.device_setting_value)
                VALUES(N.device_id, N.device_setting_parameter_cd, N.device_setting_value);				
		
		COMMIT;
	END LOOP;
END;
/
DROP TABLE DEVICE.TMP_FT_HELP;

DECLARE
	CURSOR cur IS 
		select d.device_id, d.device_name
		from device.vw_device_last_active d
		where d.device_type_id in (11, 13)
		order by d.device_id;
		
	ln_file_transfer_id FILE_TRANSFER.FILE_TRANSFER_ID%TYPE;
	ln_file_transfer_type_cd FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE;
	ln_result_cd NUMBER;
    lv_error_message VARCHAR2(4000);
    ln_setting_count NUMBER;
	
	PROCEDURE SP_UPDATE_DEVICE_SETTINGS
	(
		pn_device_id IN device.device_id%TYPE,
		pn_file_transfer_id IN file_transfer.file_transfer_id%TYPE,
		pn_result_cd OUT NUMBER,
		pv_error_message OUT VARCHAR2,
		pn_setting_count OUT NUMBER
	)
	IS
		ll_file_transfer_content file_transfer.file_transfer_content%TYPE;
		ln_pos NUMBER := 1;
		ln_content_size NUMBER := 2000;
		ln_content_pos NUMBER := 1;
		ls_content VARCHAR2(4000);
		ls_record VARCHAR2(4000);
		ls_param device_setting.device_setting_parameter_cd%TYPE;
		ls_value VARCHAR2(4000);
		ln_return NUMBER := 0;
		ln_file_transfer_type_cd file_transfer.file_transfer_type_cd%TYPE;
		ln_file_order device_setting.file_order%TYPE := 0;
	BEGIN
		pn_result_cd := PKG_CONST.RESULT__FAILURE;
		pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;

		SELECT file_transfer_type_cd, file_transfer_content
		INTO ln_file_transfer_type_cd, ll_file_transfer_content
		FROM device.file_transfer
		WHERE file_transfer_id = pn_file_transfer_id;

		IF ln_file_transfer_type_cd = PKG_CONST.FILE_TYPE__CONFIG THEN
			DELETE FROM device.device_setting
			WHERE device_id = pn_device_id
				AND device_setting_parameter_cd IN (
					SELECT device_setting_parameter_cd
					FROM device.device_setting_parameter
					WHERE device_setting_ui_configurable = 'Y');
		ELSE
			DELETE FROM device.config_template_setting
			WHERE file_transfer_id = pn_file_transfer_id;
		END IF;

		LOOP
			ls_content := ls_content || SUBSTR(utl_raw.cast_to_varchar2(HEXTORAW(ll_file_transfer_content)), ln_content_pos, ln_content_size);
			ln_content_pos := ln_content_pos + ln_content_size;

			ln_pos := NVL(INSTR(ls_content, PKG_CONST.ASCII__LF), 0);
			IF ln_pos = 0 OR NVL(INSTR(ls_content, '='), 0) = 0 THEN
				ln_return := 1;
			END IF;

			LOOP
				ln_pos := NVL(INSTR(ls_content, PKG_CONST.ASCII__LF), 0);
				IF ln_pos = 0 AND ln_return = 0 THEN
					EXIT;
				END IF;

				IF ln_return = 0 THEN
					ls_record := SUBSTR(ls_content, 1, ln_pos - 1);
					ls_content := SUBSTR(ls_content, ln_pos + 1);
				ELSE
					ls_record := ls_content;
				END IF;

				ln_pos := NVL(INSTR(ls_record, '='), 0);
				IF ln_pos = 0 AND ln_return = 0 THEN
					EXIT;
				END IF;

				ls_param := TRIM(SUBSTR(ls_record, 1, ln_pos - 1));
				ls_value := REPLACE(TRIM(SUBSTR(ls_record, ln_pos + 1, LENGTH(ls_record) - ln_pos)), CHR(13), '');
				IF LENGTH(ls_value) > 200 THEN
					ls_value := NULL;
				END IF;

				IF NVL(LENGTH(ls_param), 0) > 0 THEN
					BEGIN
						INSERT INTO device.device_setting_parameter(device_setting_parameter_cd)
						SELECT ls_param FROM dual
						WHERE NOT EXISTS (
							SELECT 1 FROM device.device_setting_parameter
							WHERE device_setting_parameter_cd = ls_param);
					EXCEPTION
						WHEN DUP_VAL_ON_INDEX THEN
							NULL;
					END;
					
					ln_file_order := ln_file_order + 1;

					BEGIN
						IF ln_file_transfer_type_cd = PKG_CONST.FILE_TYPE__CONFIG THEN
							INSERT INTO device.device_setting(device_id, device_setting_parameter_cd, device_setting_value, file_order)
							VALUES(pn_device_id, ls_param, ls_value, ln_file_order);
						ELSE
							INSERT INTO device.config_template_setting(file_transfer_id, device_setting_parameter_cd, config_template_setting_value, file_order)
							VALUES(pn_file_transfer_id, ls_param, ls_value, ln_file_order);
						END IF;
					EXCEPTION
						WHEN DUP_VAL_ON_INDEX THEN
							NULL;
					END;					
				END IF;

				IF ln_return = 1 THEN
					IF ln_file_transfer_type_cd = PKG_CONST.FILE_TYPE__CONFIG THEN
						SELECT COUNT(1)
						INTO pn_setting_count
						FROM device.device_setting
						WHERE device_id = pn_device_id
							AND device_setting_parameter_cd IN (
								SELECT device_setting_parameter_cd
								FROM device.device_setting_parameter
								WHERE device_setting_ui_configurable = 'Y');
					ELSE
						SELECT COUNT(1)
						INTO pn_setting_count
						FROM device.config_template_setting
						WHERE file_transfer_id = pn_file_transfer_id
							AND device_setting_parameter_cd IN (
								SELECT device_setting_parameter_cd
								FROM device.device_setting_parameter
								WHERE device_setting_ui_configurable = 'Y');
					END IF;

					pn_result_cd := PKG_CONST.RESULT__SUCCESS;
					pv_error_message := PKG_CONST.ERROR__NO_ERROR;
					RETURN;
				END IF;
			END LOOP;

		END LOOP;
	END;
BEGIN
	FOR rec_cur IN cur LOOP
		BEGIN
			SELECT file_transfer_id, file_transfer_type_cd
			INTO ln_file_transfer_id, ln_file_transfer_type_cd
			FROM (
				SELECT /*+INDEX(ft INX_FILE_TRANSFER_TYPE_NAME)*/ file_transfer_id, file_transfer_type_cd
				FROM device.file_transfer ft 
				WHERE file_transfer_name = rec_cur.device_name || '-CFG'
				ORDER BY file_transfer_type_cd, created_ts
			) WHERE ROWNUM = 1;
			
			IF ln_file_transfer_type_cd != 1 THEN
				UPDATE DEVICE.FILE_TRANSFER
				SET FILE_TRANSFER_TYPE_CD = 1
				WHERE FILE_TRANSFER_ID = ln_file_transfer_id;
			END IF;

			SP_UPDATE_DEVICE_SETTINGS(rec_cur.device_id, ln_file_transfer_id, ln_result_cd, lv_error_message, ln_setting_count);
			COMMIT;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				NULL;
			WHEN VALUE_ERROR THEN
				NULL;
		END;
	END LOOP;
END;
/