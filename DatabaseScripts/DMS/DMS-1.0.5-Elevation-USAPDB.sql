WHENEVER SQLERROR EXIT FAILURE COMMIT;

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/DMS/DMS-1.0.5-USAPDB.sql?rev=HEAD
GRANT SELECT, INSERT, UPDATE, DELETE ON REPORT.USER_TERMINAL TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.STATE TO REPORT WITH GRANT OPTION;
GRANT SELECT ON FRONT.POSTAL TO REPORT WITH GRANT OPTION;

UPDATE REPORT.DEVICE_TYPE SET DEVICE_TYPE_DESC = 'G4 ePort' WHERE DEVICE_TYPE_ID = 0 AND DEVICE_TYPE_DESC != 'G4 ePort';
UPDATE REPORT.DEVICE_TYPE SET DEVICE_TYPE_DESC = 'Gx ePort' WHERE DEVICE_TYPE_ID = 1 AND DEVICE_TYPE_DESC != 'Gx ePort';
UPDATE REPORT.DEVICE_TYPE SET DEVICE_TYPE_DESC = 'ePort NG' WHERE DEVICE_TYPE_ID = 3 AND DEVICE_TYPE_DESC != 'ePort NG';
UPDATE REPORT.DEVICE_TYPE SET DEVICE_TYPE_DESC = 'Legacy Kiosk (Sony DLL)' WHERE DEVICE_TYPE_ID = 4 AND DEVICE_TYPE_DESC != 'Legacy Kiosk (Sony DLL)';
UPDATE REPORT.DEVICE_TYPE SET DEVICE_TYPE_DESC = 'eSuds Room Controller' WHERE DEVICE_TYPE_ID = 5 AND DEVICE_TYPE_DESC != 'eSuds Room Controller';
UPDATE REPORT.DEVICE_TYPE SET DEVICE_TYPE_DESC = 'MEI ePort' WHERE DEVICE_TYPE_ID = 6 AND DEVICE_TYPE_DESC != 'MEI ePort';
UPDATE REPORT.DEVICE_TYPE SET DEVICE_TYPE_DESC = 'EZ80 ePort Development' WHERE DEVICE_TYPE_ID = 7 AND DEVICE_TYPE_DESC != 'EZ80 ePort Development';
UPDATE REPORT.DEVICE_TYPE SET DEVICE_TYPE_DESC = 'EZ80 ePort' WHERE DEVICE_TYPE_ID = 8 AND DEVICE_TYPE_DESC != 'EZ80 ePort';
UPDATE REPORT.DEVICE_TYPE SET DEVICE_TYPE_DESC = 'Legacy G4 ePort' WHERE DEVICE_TYPE_ID = 9 AND DEVICE_TYPE_DESC != 'Legacy G4 ePort';
UPDATE REPORT.DEVICE_TYPE SET DEVICE_TYPE_DESC = 'Transact' WHERE DEVICE_TYPE_ID = 10 AND DEVICE_TYPE_DESC != 'Transact';
UPDATE REPORT.DEVICE_TYPE SET DEVICE_TYPE_DESC = 'Kiosk' WHERE DEVICE_TYPE_ID = 11 AND DEVICE_TYPE_DESC != 'Kiosk';
UPDATE REPORT.DEVICE_TYPE SET DEVICE_TYPE_DESC = 'T2' WHERE DEVICE_TYPE_ID = 12 AND DEVICE_TYPE_DESC != 'T2';
UPDATE REPORT.DEVICE_TYPE SET DEVICE_TYPE_DESC = 'ePort Edge' WHERE DEVICE_TYPE_ID = 13 AND DEVICE_TYPE_DESC != 'ePort Edge';
COMMIT;

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/VW_CURRENT_DEVICE_LOCATION.vws?rev=1.3
CREATE OR REPLACE VIEW report.vw_current_device_location
(
    eport_id
  , device_serial_cd
  , customer_id
  , customer_name
  , terminal_id
  , terminal_nbr
  , asset_nbr
  , region_name
  , location_name
  , install_ts
  , activation_ts
  , product_type_name
  , product_type_specify
  , location_type_name
  , location_type_specify
  , auth_mode
  , timezone_offset
  , timezone_name
  , timezone_abbrev
  , intended_avg_trans_amt
  , dex_flag
  , receipt_flag
  , vend_mode
  , location_details
  , address1
  , address2
  , city
  , state
  , zip
)
AS
SELECT e.eport_id,
  e.eport_serial_num device_serial_cd,
  c.customer_id,
  c.customer_name,
  t.terminal_id,
  t.terminal_nbr,
  t.asset_nbr,
  r.region_name,
  l.location_name,
  te.start_date install_ts,
  t.create_date activation_ts,
  pt.product_type_name,
  t.product_type_specify,
  lt.location_type_name,
  l.location_type_specify,
  DECODE (t.authorization_mode, 'N', 'NONE', 'L', 'LOCAL', 'F', 'LIVE' ) auth_mode,
  tz.offset timezone_offset,
  tz.NAME timezone_name,
  tz.abbrev timezone_abbrev,
  t.avg_trans_amt intended_avg_trans_amt,
  t.dex_data dex_flag,
  t.receipt receipt_flag,
  DECODE (t.vends, 1, 'SINGLE', 10, 'MULTI') vend_mode,
  l.description location_details,
  ta.address1,
  ta.address2,
  NVL(ta.city, ps.city),
  NVL(ta.state, ps.state_cd),
  ta.zip
FROM corp.customer c
INNER JOIN report.terminal t
ON t.customer_id = c.customer_id
LEFT JOIN (report.terminal_region tr
INNER JOIN report.region r
ON tr.region_id   = r.region_id)
ON tr.terminal_id = t.terminal_id
INNER JOIN report.terminal_eport te
ON t.terminal_id = te.terminal_id
AND SYSDATE     >= NVL (te.start_date, min_date)
AND SYSDATE      < NVL (te.end_date, max_date)
INNER JOIN report.eport e
ON e.eport_id = te.eport_id
LEFT JOIN (report.LOCATION l
INNER JOIN report.terminal_addr ta
ON l.address_id = ta.address_id
INNER JOIN report.location_type lt
ON l.location_type_id = lt.location_type_id)
ON t.location_id      = l.location_id
LEFT JOIN report.product_type pt
ON t.product_type_id = pt.product_type_id
INNER JOIN report.TIME_ZONE tz
ON t.time_zone_id = tz.time_zone_id
LEFT OUTER JOIN (
	SELECT p.city, p.postal_cd, s.state_cd, s.country_cd
	FROM FRONT.POSTAL p
	INNER JOIN CORP.STATE s
	ON p.state_id = s.state_id
) ps 
ON ta.zip = ps.postal_cd
AND ta.country_cd = ps.country_cd
;
-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/releases/REL_USALive_1_7_0/DeviceAlertReport.sql?rev=HEAD
-- New report 'Device Alerts'
INSERT INTO REPORT.EXPORT_TYPE ( EXPORT_TYPE_ID, NAME, DESCRIPTION, STATUS) 
values (6 , 'IMMED', 'No batching, immediate transmission', 'A');

INSERT INTO REPORT.GENERATOR (GENERATOR_ID, NAME, CMDLINE, CLASSNAME) values ( 9, 'Alert', null, null );

declare
 newval NUMBER;
 dropcheck NUMBER;
 tgtschema VARCHAR(20) := 'REPORT';
 seqname VARCHAR(20) := 'REPORTS_SEQ';
 srcmaxtable VARCHAR(40) := 'REPORT.REPORTS';
 srcmaxcol VARCHAR(40) := 'REPORT_ID';
 newseq VARCHAR(150);
BEGIN 
  execute immediate 'select MAX(' || srcmaxcol || ')+1 FROM ' || srcmaxtable into newval;

  newseq := 'CREATE SEQUENCE ' || tgtschema || '.' || seqname || ' MINVALUE 0 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH ' || newval || ' NOCACHE NOORDER NOCYCLE';

  DBMS_OUTPUT.PUT_LINE(newseq);

  select count(*) into dropcheck
  from dba_objects
  where object_type = 'SEQUENCE'
  and object_name = seqname
  and owner = tgtschema;

  if dropcheck > 0 THEN 
    DBMS_OUTPUT.PUT_LINE('Dropping existing sequence');
    execute immediate 'drop sequence ' || tgtschema || '.' || seqname ;
  END If;
 
  DBMS_OUTPUT.PUT_LINE('Creating new sequence starting at ' || newval);
  execute immediate newseq;
  DBMS_OUTPUT.PUT_LINE('Completed');
  DBMS_OUTPUT.NEW_LINE();
END; 
/
INSERT INTO REPORT.REPORTS
  (
    REPORT_ID,
    TITLE,
    GENERATOR_ID,
    UPD_DT,
    BATCH_TYPE_ID,
    REPORT_NAME,
    DESCRIPTION,
    USAGE,
    USER_ID,
    CATEGORY
  )
  VALUES
  (
    REPORT.REPORTS_SEQ.NEXTVAL,
    'Device Alert from {s}',
    9,
    SYSDATE,
    6,
    'Device Alerts',
    'Alerts will be sent upon detection',
    'E',
    0,
    ''
  );
  
UPDATE report.reports set TITLE = 'Device Alert from {s}', BATCH_TYPE_ID = 6, REPORT_NAME = 'Device Alerts' where REPORT_NAME = 'Device Alert' OR REPORT_NAME = 'Device Alerts';

COMMIT;

GRANT EXECUTE ON REPORT.CCS_PKG TO USAT_APP_LAYER_ROLE;



