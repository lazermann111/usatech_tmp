WHENEVER SQLERROR EXIT FAILURE COMMIT;

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/PSS/PKG_SETTLEMENT.pbk?rev=1.50.2.1
CREATE OR REPLACE PACKAGE BODY PSS.PKG_SETTLEMENT AS
    FUNCTION AFTER_SETTLE_TRAN_STATE_CD(
        pc_auth_type_cd PSS.AUTH.AUTH_TYPE_CD%TYPE,
        pc_refund_type_cd PSS.REFUND.REFUND_TYPE_CD%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pc_device_result_type_cd PSS.TRAN.TRAN_DEVICE_RESULT_TYPE_CD%TYPE)
    RETURN PSS.TRAN.TRAN_STATE_CD%TYPE
    PARALLEL_ENABLE DETERMINISTIC
    IS       
    BEGIN
        IF pc_auth_result_cd = 'Y' THEN
            IF pc_auth_type_cd IS NOT NULL THEN
                IF pc_auth_type_cd IN('C', 'V', 'E', 'I') THEN
                    IF pc_device_result_type_cd IS NULL THEN
                        RETURN 'V'; -- processed reversal
                    ELSE
                        RETURN 'C'; -- Cancelled 
                    END IF;
                ELSE
                    RETURN 'D'; -- Complete
                END IF;                    
            ELSIF pc_refund_type_cd IS NOT NULL THEN
                IF pc_refund_type_cd IN('C', 'V') THEN
                    RETURN 'C'; -- Cancelled 
                ELSE
                    RETURN 'D'; -- Complete
                END IF;
            ELSE
                RAISE_APPLICATION_ERROR(-20560, 'Both auth type cd and refund type cd are null');
            END IF;
        ELSIF pc_auth_result_cd = 'P' THEN
            RETURN 'Q'; -- settlement processed
        ELSIF pc_auth_result_cd = 'N' THEN
            RETURN 'N'; -- PROCESSED_SERVER_SETTLEMENT_INCOMPLETE
        ELSIF pc_auth_result_cd = 'F' THEN
            RETURN 'R'; -- PROCESSED_SERVER_SETTLEMENT_ERROR
        ELSIF pc_auth_result_cd = 'O' THEN
            IF pc_auth_type_cd IS NOT NULL THEN
                IF pc_auth_type_cd IN('C', 'V', 'E', 'I') THEN
                    IF pc_device_result_type_cd IS NULL THEN
                        RETURN 'V'; -- processed reversal
                    ELSE
                        RETURN 'C'; -- Cancelled 
                    END IF;
                ELSE
                    RETURN 'E'; -- Complete - Error
                END IF;                    
            ELSIF pc_refund_type_cd IS NOT NULL THEN
                IF pc_refund_type_cd IN('C', 'V') THEN
                    RETURN 'C'; -- Cancelled 
                ELSE
                    RETURN 'E'; -- Complete - Error
                END IF;
            ELSE
                RAISE_APPLICATION_ERROR(-20560, 'Both auth type cd and refund type cd are null');
            END IF;
        END IF;
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    FUNCTION CREATE_TERMINAL_BATCH(
        pn_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE)
    RETURN PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE
    IS
        ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
    BEGIN
        SELECT PSS.SEQ_TERMINAL_BATCH_ID.NEXTVAL
          INTO ln_terminal_batch_id
          FROM DUAL;
        INSERT INTO PSS.TERMINAL_BATCH (
                TERMINAL_BATCH_ID,
                TERMINAL_ID,
                TERMINAL_BATCH_NUM,
                TERMINAL_BATCH_OPEN_TS,
                TERMINAL_BATCH_CYCLE_NUM,
                TERMINAL_CAPTURE_FLAG) 
         SELECT ln_terminal_batch_id, 
                pn_terminal_id, 
                T.TERMINAL_NEXT_BATCH_NUM,
                SYSDATE,
                T.TERMINAL_BATCH_CYCLE_NUM,
                A.TERMINAL_CAPTURE_FLAG
           FROM PSS.TERMINAL T
           JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
           JOIN AUTHORITY.AUTHORITY A ON M.AUTHORITY_ID = A.AUTHORITY_ID
          WHERE TERMINAL_ID = pn_terminal_id;
          
         UPDATE PSS.TERMINAL
            SET TERMINAL_NEXT_BATCH_NUM = CASE 
                    WHEN TERMINAL_NEXT_BATCH_NUM >= TERMINAL_MAX_BATCH_NUM THEN NVL(TERMINAL_MIN_BATCH_NUM, 1) /* reset batch num */ 
                    ELSE TERMINAL_NEXT_BATCH_NUM + 1 /* increment batch num */
                END,
                TERMINAL_BATCH_CYCLE_NUM = CASE 
                    WHEN TERMINAL_NEXT_BATCH_NUM >= TERMINAL_MAX_BATCH_NUM THEN TERMINAL_BATCH_CYCLE_NUM + 1 /* next cycle */ 
                    ELSE TERMINAL_BATCH_CYCLE_NUM /* same cycle */
                END
            WHERE TERMINAL_ID = pn_terminal_id;
        RETURN ln_terminal_batch_id;
    END;
            
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    FUNCTION GET_AVAILABLE_TERMINAL_BATCH(
        pn_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE,
        pn_allowed_trans OUT PLS_INTEGER,
        pb_create_if_needed BOOLEAN)
    RETURN PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE
    IS
        ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
        lc_is_closed CHAR(1);
        ln_max_tran PSS.TERMINAL.TERMINAL_BATCH_MAX_TRAN%TYPE; 
        ln_terminal_state_id PSS.TERMINAL.TERMINAL_STATE_ID%TYPE;
        lc_terminal_capture_flag PSS.TERMINAL_BATCH.TERMINAL_CAPTURE_FLAG%TYPE;
        ln_attempts PLS_INTEGER;
    BEGIN
        -- get last terminal batch record and verify that it is open       
        SELECT /*+ FIRST_ROWS */ MAX(TERMINAL_BATCH_ID), MAX(DECODE(TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y')), MAX(TERMINAL_BATCH_MAX_TRAN), MAX(TERMINAL_STATE_ID), MAX(NVL(TERMINAL_CAPTURE_FLAG, 'N'))
          INTO ln_terminal_batch_id, lc_is_closed, ln_max_tran, ln_terminal_state_id, lc_terminal_capture_flag
          FROM (SELECT TB.TERMINAL_BATCH_ID,
                       TB.TERMINAL_BATCH_NUM, 
                       TB.TERMINAL_BATCH_CYCLE_NUM, 
                       TB.TERMINAL_BATCH_CLOSE_TS,
                       COALESCE(T.TERMINAL_BATCH_MAX_TRAN, AU.AUTHORITY_BATCH_MAX_TRAN, 999) TERMINAL_BATCH_MAX_TRAN,
                       T.TERMINAL_STATE_ID,
                       TB.TERMINAL_CAPTURE_FLAG
                  FROM PSS.TERMINAL T
                  JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
                  JOIN AUTHORITY.AUTHORITY AU ON M.AUTHORITY_ID = AU.AUTHORITY_ID 
                  LEFT OUTER JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID                   
                 WHERE T.TERMINAL_ID = pn_terminal_id
                 ORDER BY TB.TERMINAL_BATCH_CYCLE_NUM DESC,
                          TB.TERMINAL_BATCH_NUM DESC)
         WHERE ROWNUM = 1;
        pn_allowed_trans := ln_max_tran;
        IF ln_terminal_state_id NOT IN(3) THEN
            RAISE_APPLICATION_ERROR(-20559, 'Terminal ' || pn_terminal_id || ' is not locked and a new terminal batch can not be created for it');
        ELSIF ln_terminal_batch_id IS NULL THEN
            IF pb_create_if_needed THEN
                ln_terminal_batch_id := CREATE_TERMINAL_BATCH(pn_terminal_id);
            END IF;
        ELSIF lc_is_closed  = 'Y' THEN
            IF pb_create_if_needed THEN
                ln_terminal_batch_id := CREATE_TERMINAL_BATCH(pn_terminal_id);
            END IF;
        ELSE
            -- do we need a new batch?
            SELECT ln_max_tran - COUNT(DISTINCT TRAN_ID)
              INTO pn_allowed_trans
              FROM (SELECT A.TRAN_ID
                      FROM PSS.AUTH A
                     WHERE A.TERMINAL_BATCH_ID = ln_terminal_batch_id
                    UNION ALL
                    SELECT R.TRAN_ID
                      FROM PSS.REFUND R
                     WHERE R.TERMINAL_BATCH_ID = ln_terminal_batch_id);
            IF lc_terminal_capture_flag = 'Y' THEN              
                IF pn_allowed_trans > 0 THEN
                    SELECT COUNT(*)
                      INTO ln_attempts
                      FROM PSS.SETTLEMENT_BATCH
                     WHERE TERMINAL_BATCH_ID = ln_terminal_batch_id;
                END IF;
                IF ln_attempts > 0 OR pn_allowed_trans <= 0 THEN
                    -- Create new terminal batch
                    IF pb_create_if_needed THEN
                        ln_terminal_batch_id := CREATE_TERMINAL_BATCH(pn_terminal_id);
                    END IF;
                    pn_allowed_trans := ln_max_tran;
                END IF;
            END IF;
        END IF;
        
        RETURN ln_terminal_batch_id;
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE GET_PENDING_SETTLEMENTS(
        pn_payment_subtype_key_id PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE, 
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pc_ignore_minimums_flag CHAR,
        pn_max_settlements PLS_INTEGER, 
        pn_pending_terminal_batch_ids OUT NUMBER_TABLE)
    IS
    BEGIN
         -- The following should never occur:
        /*
        -- Find retry settlements
        SELECT TERMINAL_BATCH_ID
          BULK COLLECT INTO pn_pending_terminal_batch_ids
          FROM (SELECT TB.TERMINAL_BATCH_ID
                  FROM PSS.SETTLEMENT_BATCH SB 
                  JOIN PSS.TERMINAL_BATCH TB ON TB.TERMINAL_BATCH_ID = SB.TERMINAL_BATCH_ID
                 WHERE TB.TERMINAL_ID = pn_payment_subtype_key_id 
                   AND SB.SETTLEMENT_BATCH_STATE_ID = 4 
                 ORDER BY TB.TERMINAL_BATCH_OPEN_TS ASC)
         WHERE ROWNUM <= pn_max_settlements;
     
        IF pn_pending_terminal_batch_ids.COUNT > 0 THEN
            RETURN;
        END IF;
        */
        -- Find new open batches
        SELECT TERMINAL_BATCH_ID
          BULK COLLECT INTO pn_pending_terminal_batch_ids
          FROM (
            SELECT C.TERMINAL_BATCH_ID
              FROM PSS.MERCHANT M
              JOIN AUTHORITY.AUTHORITY AU ON M.AUTHORITY_ID = AU.AUTHORITY_ID
              JOIN (SELECT TERMINAL_ID, TERMINAL_BATCH_ID, MERCHANT_ID, TERMINAL_BATCH_OPEN_TS, 
                           TERMINAL_BATCH_MIN_TRAN, TERMINAL_BATCH_MAX_TRAN, TERMINAL_MIN_BATCH_CLOSE_HR, TERMINAL_MAX_BATCH_CLOSE_HR, 
                           SUM(CASE WHEN TRAN_STATE_CD IN('T') THEN 1 ELSE 0 END) NUM_TRAN
                      FROM (SELECT T.TERMINAL_ID, 
                                   TB.TERMINAL_BATCH_ID,
                                   T.MERCHANT_ID,
                                   X.TRAN_ID,
                                   X.TRAN_STATE_CD,
                                   T.TERMINAL_BATCH_MIN_TRAN,
                                   T.TERMINAL_BATCH_MAX_TRAN,
                                   T.TERMINAL_MIN_BATCH_CLOSE_HR,
                                   T.TERMINAL_MAX_BATCH_CLOSE_HR,
                                   TB.TERMINAL_BATCH_OPEN_TS
                              FROM PSS.TERMINAL T
                              JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
                              JOIN PSS.AUTH A ON TB.TERMINAL_BATCH_ID = A.TERMINAL_BATCH_ID
                              JOIN PSS.TRAN X ON A.TRAN_ID = X.TRAN_ID 
                             WHERE TB.TERMINAL_BATCH_OPEN_TS IS NOT NULL
                               AND DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y') = 'N'
                               AND T.TERMINAL_ID = pn_payment_subtype_key_id
                             UNION 
                            SELECT T.TERMINAL_ID, 
                                   TB.TERMINAL_BATCH_ID,
                                   T.MERCHANT_ID,
                                   X.TRAN_ID,
                                   X.TRAN_STATE_CD,
                                   T.TERMINAL_BATCH_MIN_TRAN,
                                   T.TERMINAL_BATCH_MAX_TRAN,
                                   T.TERMINAL_MIN_BATCH_CLOSE_HR,
                                   T.TERMINAL_MAX_BATCH_CLOSE_HR,
                                   TB.TERMINAL_BATCH_OPEN_TS
                              FROM PSS.TERMINAL T
                              JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
                              JOIN PSS.REFUND R ON TB.TERMINAL_BATCH_ID = R.TERMINAL_BATCH_ID
                              JOIN PSS.TRAN X ON R.TRAN_ID = X.TRAN_ID 
                             WHERE TB.TERMINAL_BATCH_OPEN_TS IS NOT NULL
                               AND DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y') = 'N'
                               AND T.TERMINAL_ID = pn_payment_subtype_key_id) 
                     GROUP BY TERMINAL_ID, TERMINAL_BATCH_ID, MERCHANT_ID, TERMINAL_BATCH_OPEN_TS, TERMINAL_BATCH_MIN_TRAN, TERMINAL_BATCH_MAX_TRAN, TERMINAL_MIN_BATCH_CLOSE_HR, TERMINAL_MAX_BATCH_CLOSE_HR
                     HAVING COUNT(TRAN_STATE_CD) = SUM(CASE WHEN TRAN_STATE_CD IN('T', 'E') THEN 1 ELSE 0 END)) C  ON C.MERCHANT_ID = M.MERCHANT_ID 
             WHERE C.NUM_TRAN > 0
               AND (pc_ignore_minimums_flag = 'Y'
                OR C.NUM_TRAN >= COALESCE(C.TERMINAL_BATCH_MAX_TRAN, AU.AUTHORITY_BATCH_MAX_TRAN, 999) 
                OR SYSDATE >= C.TERMINAL_BATCH_OPEN_TS + COALESCE(C.TERMINAL_MAX_BATCH_CLOSE_HR/24, AU.AUTHORITY_MAX_BATCH_CLOSE_HR/24, 1)
                OR (SYSDATE >= C.TERMINAL_BATCH_OPEN_TS + COALESCE(C.TERMINAL_MIN_BATCH_CLOSE_HR/24, AU.AUTHORITY_MIN_BATCH_CLOSE_HR/24, 0)
                    AND C.NUM_TRAN >= COALESCE(C.TERMINAL_BATCH_MIN_TRAN, AU.AUTHORITY_BATCH_MIN_TRAN, 25)))
             ORDER BY c.TERMINAL_BATCH_OPEN_TS ASC)
         WHERE ROWNUM <= pn_max_settlements;
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE GET_RETRY_SETTLEMENTS(
        pn_payment_subtype_key_id PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE, 
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_settlement_retry_interval NUMBER,
        pn_max_settlements PLS_INTEGER, 
        pn_pending_terminal_batch_ids OUT NUMBER_TABLE)
    IS
    BEGIN
        -- Find retyable batches
        SELECT TERMINAL_BATCH_ID
          BULK COLLECT INTO pn_pending_terminal_batch_ids
          FROM (
            SELECT C.TERMINAL_BATCH_ID
              FROM (SELECT TERMINAL_ID, TERMINAL_BATCH_ID, TERMINAL_BATCH_OPEN_TS, 
                           SUM(CASE WHEN TRAN_STATE_CD IN('R', 'N') THEN 1 ELSE 0 END) NUM_TRAN
                      FROM (SELECT T.TERMINAL_ID, 
                                   TB.TERMINAL_BATCH_ID,
                                   X.TRAN_ID,
                                   X.TRAN_STATE_CD,
                                   TB.TERMINAL_BATCH_OPEN_TS
                              FROM PSS.TERMINAL T
                              JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
                              JOIN PSS.AUTH A ON TB.TERMINAL_BATCH_ID = A.TERMINAL_BATCH_ID
                              JOIN PSS.TRAN X ON A.TRAN_ID = X.TRAN_ID 
                             WHERE TB.TERMINAL_BATCH_OPEN_TS IS NOT NULL
                               AND DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y') = 'N'
                               AND T.TERMINAL_ID = pn_payment_subtype_key_id
                             UNION 
                            SELECT T.TERMINAL_ID, 
                                   TB.TERMINAL_BATCH_ID,
                                   X.TRAN_ID,
                                   X.TRAN_STATE_CD,
                                   TB.TERMINAL_BATCH_OPEN_TS
                              FROM PSS.TERMINAL T
                              JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
                              JOIN PSS.REFUND R ON TB.TERMINAL_BATCH_ID = R.TERMINAL_BATCH_ID
                              JOIN PSS.TRAN X ON R.TRAN_ID = X.TRAN_ID 
                             WHERE TB.TERMINAL_BATCH_OPEN_TS IS NOT NULL
                               AND DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y') = 'N'
                               AND T.TERMINAL_ID = pn_payment_subtype_key_id) 
                     GROUP BY TERMINAL_ID, TERMINAL_BATCH_ID, TERMINAL_BATCH_OPEN_TS
                     HAVING COUNT(TRAN_STATE_CD) = SUM(CASE WHEN TRAN_STATE_CD IN('R', 'N', 'E') THEN 1 ELSE 0 END)) C
             WHERE C.NUM_TRAN > 0
               AND SYSDATE >= pn_settlement_retry_interval + (
                        SELECT NVL(MAX(LA.SETTLEMENT_BATCH_START_TS), MIN_DATE)
                          FROM PSS.SETTLEMENT_BATCH LA
                          WHERE C.TERMINAL_BATCH_ID = LA.TERMINAL_BATCH_ID)
             ORDER BY c.TERMINAL_BATCH_OPEN_TS ASC)
         WHERE ROWNUM <= pn_max_settlements;
    END;

/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE FORCE_SETTLE_BATCH(
        pn_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE,
        pv_force_reason PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_RESP_DESC%TYPE)
    IS
        ln_settlement_batch_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE;
        ln_settlement_batch_state_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_STATE_ID%TYPE;
        ln_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE;
        ln_terminal_state_id PSS.TERMINAL.TERMINAL_STATE_ID%TYPE;
        ln_is_open PLS_INTEGER;
        lv_msg VARCHAR2(4000);
    BEGIN
        -- check terminal state
        SELECT T.TERMINAL_ID, T.TERMINAL_STATE_ID
          INTO ln_terminal_id, ln_terminal_state_id
          FROM PSS.TERMINAL T
          JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
         WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id;
        IF ln_terminal_state_id NOT IN(3,5) THEN
            SELECT 'Terminal ' || ln_terminal_id || ' is currently ' || DECODE(ln_terminal_state_id, 1, 'not locked', 2, 'disabled', 5, 'busy with retry', 'unavailable')
              INTO lv_msg
              FROM DUAL;
            RAISE_APPLICATION_ERROR(-20560, lv_msg);
        END IF;
        
        -- Check status of most recent settlement batch to ensure it is failue or decline
        SELECT MAX(SETTLEMENT_BATCH_ID), NVL(MAX(SETTLEMENT_BATCH_STATE_ID), 0)
          INTO ln_settlement_batch_id, ln_settlement_batch_state_id
          FROM (
            SELECT SETTLEMENT_BATCH_ID, SETTLEMENT_BATCH_STATE_ID
              FROM PSS.SETTLEMENT_BATCH
             WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id
             ORDER BY SETTLEMENT_BATCH_START_TS DESC, SETTLEMENT_BATCH_ID DESC)
         WHERE ROWNUM = 1;
        IF ln_settlement_batch_state_id NOT IN(2, 3) THEN
            SELECT 'Terminal Batch ' || pn_terminal_batch_id || DECODE(ln_settlement_batch_state_id, 0, ' has not yet been tried', 1, ' was successfully settled already', 4, ' is awaiting a retry', 7, 'was partially settled already', ' is not ready for forced settlement')
              INTO lv_msg
              FROM DUAL;
            RAISE_APPLICATION_ERROR(-20561, lv_msg);
        END IF;
        
        -- create new settlement batch
        SELECT PSS.SEQ_SETTLEMENT_BATCH_ID.NEXTVAL
          INTO ln_settlement_batch_id
          FROM DUAL;            
        
        INSERT INTO PSS.SETTLEMENT_BATCH(
            SETTLEMENT_BATCH_ID,
            SETTLEMENT_BATCH_STATE_ID,
            SETTLEMENT_BATCH_START_TS,
            TERMINAL_BATCH_ID,
            SETTLEMENT_BATCH_RESP_CD,
            SETTLEMENT_BATCH_RESP_DESC,
            SETTLEMENT_BATCH_REF_CD,
            SETTLEMENT_BATCH_END_TS
        ) VALUES (
            ln_settlement_batch_id,
            1,
            SYSDATE,
            pn_terminal_batch_id,
            0,
            pv_force_reason, 
            'FORCE_SETTLEMENT',
            SYSDATE);
        -- add all trans and update their tran state cd
        INSERT INTO PSS.TRAN_SETTLEMENT_BATCH(
            SETTLEMENT_BATCH_ID,
            AUTH_ID,
            TRAN_ID,
            TRAN_SETTLEMENT_B_AMT)
          SELECT DISTINCT 
                 ln_settlement_batch_id,
                 FIRST_VALUE(A.AUTH_ID) OVER (PARTITION BY T.TRAN_ID ORDER BY A.CREATED_TS DESC, A.AUTH_ID DESC) AUTH_ID, 
                 T.TRAN_ID, 
                 FIRST_VALUE(A.AUTH_AMT) OVER (PARTITION BY T.TRAN_ID ORDER BY A.CREATED_TS DESC, A.AUTH_ID DESC) AUTH_AMT
            FROM PSS.TRAN T
            JOIN PSS.AUTH A ON T.TRAN_ID = A.TRAN_ID
           WHERE A.TERMINAL_BATCH_ID = pn_terminal_batch_id
             AND A.AUTH_STATE_ID IN(2,6);
        INSERT INTO PSS.REFUND_SETTLEMENT_BATCH(
            SETTLEMENT_BATCH_ID,
            REFUND_ID,
            TRAN_ID,
            REFUND_SETTLEMENT_B_AMT)
          SELECT DISTINCT 
                 ln_settlement_batch_id,
                 FIRST_VALUE(R.REFUND_ID) OVER (PARTITION BY T.TRAN_ID ORDER BY R.CREATED_TS DESC, R.REFUND_ID DESC) AUTH_ID, 
                 T.TRAN_ID, 
                 FIRST_VALUE(R.REFUND_AMT) OVER (PARTITION BY T.TRAN_ID ORDER BY R.CREATED_TS DESC, R.REFUND_ID DESC) REFUND_AMT
            FROM PSS.TRAN T
            JOIN PSS.REFUND R ON T.TRAN_ID = R.TRAN_ID
           WHERE R.TERMINAL_BATCH_ID = pn_terminal_batch_id
             AND R.REFUND_STATE_ID IN(1);
        UPDATE PSS.TRAN 
           SET TRAN_STATE_CD = 'D'
         WHERE TRAN_STATE_CD IN('R', 'N', 'Q', 'T')
           AND TRAN_ID IN(
            SELECT A.TRAN_ID
              FROM PSS.AUTH A 
             WHERE A.TERMINAL_BATCH_ID = pn_terminal_batch_id
               AND A.AUTH_STATE_ID IN(2,6)
            UNION ALL
            SELECT R.TRAN_ID
              FROM PSS.REFUND R
             WHERE R.TERMINAL_BATCH_ID = pn_terminal_batch_id
               AND R.REFUND_STATE_ID IN(1));
                   
        -- update terminal batch
        UPDATE PSS.TERMINAL_BATCH
           SET TERMINAL_BATCH_CLOSE_TS = SYSDATE
         WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id;
    END;

/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE FORCE_TRAN(
        pn_payment_subtype_key_id PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE, 
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pv_force_reason PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_RESP_DESC%TYPE)
    IS
        lc_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
        ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
        ln_sale_auth_id PSS.AUTH.AUTH_ID%TYPE; 
        ln_force_auth_id PSS.AUTH.AUTH_ID%TYPE;
        ln_force_refund_id PSS.REFUND.REFUND_ID%TYPE;
        ln_allowed_trans PLS_INTEGER;
        ln_settlement_batch_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE;
        ln_auth_amt PSS.AUTH.AUTH_AMT%TYPE;
    BEGIN
        -- check terminal state
        SELECT T.TRAN_STATE_CD
          INTO lc_tran_state_cd
          FROM PSS.TRAN T
         WHERE T.TRAN_ID = pn_tran_id;
        IF lc_tran_state_cd NOT IN('I', 'J') THEN
            RAISE_APPLICATION_ERROR(-20562, 'Tran ' || pn_tran_id || ' is not in INCOMPLETE or INCOMPLETE_ERROR state and cannot be forced');
        END IF;
        
        UPDATE PSS.TRAN
           SET TRAN_STATE_CD = CASE WHEN pv_payment_subtype_class LIKE 'Authority::ISO8583%' THEN 'T' ELSE 'D' END
         WHERE TRAN_ID = pn_tran_id
           AND TRAN_STATE_CD = lc_tran_state_cd; -- make sure it didn't change
        
        IF SQL%ROWCOUNT < 1 THEN
            FORCE_TRAN(pn_payment_subtype_key_id, pv_payment_subtype_class, pn_tran_id, pv_force_reason);
        ELSE
            -- add to batch if necessary
            SELECT MAX(AUTH_ID), MAX(AUTH_AMT)
              INTO ln_sale_auth_id, ln_auth_amt
              FROM (
                SELECT AUTH_ID, AUTH_TS, AUTH_AMT
                  FROM PSS.AUTH
                 WHERE TRAN_ID = pn_tran_id
                   AND AUTH_TYPE_CD IN('U','S','O','A','D','C','E','V','I')
                 ORDER BY AUTH_TS DESC, AUTH_ID DESC)
             WHERE ROWNUM = 1;
            IF ln_sale_auth_id IS NULL THEN
                SELECT MAX(REFUND_ID), MAX(REFUND_AMT)
                  INTO ln_force_refund_id, ln_auth_amt
                  FROM (
                    SELECT CREATED_TS, REFUND_ID, REFUND_AMT
                      FROM PSS.REFUND
                     WHERE TRAN_ID = pn_tran_id
                     ORDER BY CREATED_TS DESC, REFUND_ID DESC)
                 WHERE ROWNUM = 1;
            END IF;             
            IF pv_payment_subtype_class LIKE 'Authority::ISO8583%' THEN
                IF ln_sale_auth_id IS NOT NULL THEN
                    SELECT MAX(TERMINAL_BATCH_ID)
                      INTO ln_terminal_batch_id
                      FROM PSS.AUTH
                     WHERE TRAN_ID = pn_tran_id;
                ELSIF ln_force_refund_id IS NOT NULL THEN
                    SELECT MAX(TERMINAL_BATCH_ID)
                      INTO ln_terminal_batch_id
                      FROM PSS.REFUND
                     WHERE TRAN_ID = pn_tran_id;
                END IF;
                IF ln_terminal_batch_id IS NULL THEN
                    ln_terminal_batch_id := GET_AVAILABLE_TERMINAL_BATCH(pn_payment_subtype_key_id, ln_allowed_trans, TRUE);
                    IF ln_allowed_trans <= 0 THEN
                        RAISE_APPLICATION_ERROR(-20556, 'Terminal Batch ' || ln_terminal_batch_id || ' is full for terminal ' || pn_payment_subtype_key_id || '; Can not add tran ' || pn_tran_id || ' to it');
                    END IF;
                END IF;
            ELSE
                -- create settlement batch record
                SELECT PSS.SEQ_SETTLEMENT_BATCH_ID.NEXTVAL
                  INTO ln_settlement_batch_id
                  FROM DUAL;
                INSERT INTO PSS.SETTLEMENT_BATCH(
                    SETTLEMENT_BATCH_ID,
                    SETTLEMENT_BATCH_STATE_ID,
                    SETTLEMENT_BATCH_START_TS,
                    SETTLEMENT_BATCH_END_TS,
                    SETTLEMENT_BATCH_RESP_CD,
                    SETTLEMENT_BATCH_RESP_DESC
                ) VALUES (
                    ln_settlement_batch_id,
                    1,
                    SYSDATE,
                    SYSDATE,
                    'MANUAL',
                    pv_force_reason);
            END IF;
            IF ln_sale_auth_id IS NOT NULL THEN  
                SELECT PSS.SEQ_AUTH_ID.NEXTVAL
                  INTO ln_force_auth_id
                  FROM DUAL;
                INSERT INTO PSS.AUTH (
                        AUTH_ID,
                        TRAN_ID,
                        AUTH_TYPE_CD,
                        AUTH_STATE_ID,
                        AUTH_PARSED_ACCT_DATA,
                        ACCT_ENTRY_METHOD_CD,
                        AUTH_AMT,
                        AUTH_TS,
                        TERMINAL_BATCH_ID,
                        TRACE_NUMBER,
                        AUTH_RESULT_CD,
                        AUTH_RESP_CD,
                        AUTH_RESP_DESC,
                        AUTH_AMT_APPROVED)
                 SELECT ln_force_auth_id,
                        pn_tran_id,
                        A.AUTH_TYPE_CD,
                        2,
                        a.AUTH_PARSED_ACCT_DATA,
                        a.ACCT_ENTRY_METHOD_CD,
                        a.AUTH_AMT,	    
                        SYSDATE,
                        ln_terminal_batch_id,
                        a.TRACE_NUMBER,
                        'Y',
                        'MANUAL',
                        pv_force_reason,
                        a.AUTH_AMT
                   FROM PSS.AUTH a
                  WHERE a.AUTH_ID = ln_sale_auth_id;
                IF ln_settlement_batch_id IS NOT NULL THEN
                    INSERT INTO PSS.TRAN_SETTLEMENT_BATCH(
                        SETTLEMENT_BATCH_ID,
                        AUTH_ID,
                        TRAN_ID,
                        TRAN_SETTLEMENT_B_AMT
                    ) VALUES (
                        ln_settlement_batch_id,
                        ln_force_auth_id,
                        pn_tran_id,
                        ln_auth_amt
                    );               
                END IF;
            ELSIF ln_force_refund_id IS NOT NULL THEN
                UPDATE PSS.REFUND
                   SET REFUND_STATE_ID = 1,
                       REFUND_RESP_CD = 'MANUAL',
                       REFUND_RESP_DESC = pv_force_reason,
                       REFUND_AUTHORITY_TS = SYSDATE,
                       TERMINAL_BATCH_ID = NVL(ln_terminal_batch_id, TERMINAL_BATCH_ID)
                 WHERE REFUND_ID = ln_force_refund_id;
                IF ln_settlement_batch_id IS NOT NULL THEN
                    INSERT INTO PSS.REFUND_SETTLEMENT_BATCH(
                        SETTLEMENT_BATCH_ID,
                        REFUND_ID,
                        TRAN_ID,
                        REFUND_SETTLEMENT_B_AMT
                    ) VALUES (
                        ln_settlement_batch_id,
                        ln_force_refund_id,
                        pn_tran_id,
                        ln_auth_amt
                    );               
                END IF;
            END IF;
        END IF;
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE ERROR_TRAN(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pv_force_reason PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_RESP_DESC%TYPE)
    IS
        lc_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
    BEGIN
        -- check terminal state
        SELECT TRAN_STATE_CD
          INTO lc_tran_state_cd
          FROM PSS.TRAN
         WHERE TRAN_ID = pn_tran_id;
        IF lc_tran_state_cd NOT IN('I', 'J') THEN
            RAISE_APPLICATION_ERROR(-20562, 'Tran ' || pn_tran_id || ' is not in INCOMPLETE or INCOMPLETE_ERROR state and cannot be errored');
        END IF;
        
        UPDATE PSS.TRAN
           SET TRAN_STATE_CD = 'E'
         WHERE TRAN_ID = pn_tran_id
           AND TRAN_STATE_CD = lc_tran_state_cd; -- make sure it didn't change
           
        IF SQL%ROWCOUNT < 1 THEN
            ERROR_TRAN(pn_tran_id, pv_force_reason);
        END IF;
    END;
         
    PROCEDURE MARK_ADMIN_CMD_EXECUTED(
        pn_admin_cmd_id PSS.ADMIN_CMD.ADMIN_CMD_ID%TYPE)
    IS
    BEGIN
        UPDATE PSS.ADMIN_CMD
           SET ADMIN_CMD_STATE_ID = 2,
               CLOSED_UTC_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP)
         WHERE ADMIN_CMD_ID = pn_admin_cmd_id;
    END;

    PROCEDURE MARK_ADMIN_CMD_ERRORED(
        pn_admin_cmd_id PSS.ADMIN_CMD.ADMIN_CMD_ID%TYPE,
        pv_error_msg PSS.ADMIN_CMD.ERROR_MSG%TYPE)
    IS
    BEGIN
        UPDATE PSS.ADMIN_CMD
           SET ADMIN_CMD_STATE_ID = 4,
               CLOSED_UTC_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP),
               ERROR_MSG = pv_error_msg
         WHERE ADMIN_CMD_ID = pn_admin_cmd_id;
    END;
    
    PROCEDURE GET_ACTIONS_FROM_ADMIN_CMDS(
        pn_payment_subtype_key_id PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE, 
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_max_settlements PLS_INTEGER, 
        pn_max_trans PLS_INTEGER,
        pc_allow_add_to_batch CHAR,
        pc_retry_only CHAR,
        pc_settle_processing_enabled CHAR,
        pc_tran_processing_enabled CHAR,
        pn_pending_terminal_batch_ids OUT NUMBER_TABLE,
        pn_pending_tran_ids OUT NUMBER_TABLE)
    IS
        CURSOR l_cur IS
            SELECT ADMIN_CMD_ID, ADMIN_CMD_TYPE_ID
              FROM PSS.ADMIN_CMD
             WHERE PAYMENT_SUBTYPE_KEY_ID = pn_payment_subtype_key_id
               AND PAYMENT_SUBTYPE_CLASS =  pv_payment_subtype_class
               AND ADMIN_CMD_STATE_ID = 1
               AND (pc_settle_processing_enabled = 'Y' OR ADMIN_CMD_TYPE_ID NOT IN(1,2,3,6,8))
               AND (pc_tran_processing_enabled = 'Y' OR ADMIN_CMD_TYPE_ID NOT IN(4,5,7))
               AND (pc_retry_only != 'Y' OR ADMIN_CMD_TYPE_ID IN(2, 3, 4, 5, 6, 7))
               AND (pc_allow_add_to_batch = 'Y' OR pv_payment_subtype_class NOT LIKE 'Authority::ISO8583%' OR ADMIN_CMD_TYPE_ID NOT IN(5))
             ORDER BY PRIORITY ASC, CREATED_UTC_TS ASC;
    BEGIN
        -- get the first command and translate it into a terminal batch or tran to process
        FOR l_rec IN l_cur LOOP
            IF l_rec.ADMIN_CMD_TYPE_ID IN(1,2,3,6,5,8) AND pv_payment_subtype_class NOT LIKE 'Authority::ISO8583%' THEN
                MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Settlement commands are not allowed for Payment Subtype Class ''' || pv_payment_subtype_class || '''');
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 1 THEN -- Settle All
                GET_PENDING_SETTLEMENTS(
                    pn_payment_subtype_key_id, 
                    pv_payment_subtype_class, 
                    'Y', 
                    pn_max_settlements,
                    pn_pending_terminal_batch_ids);
                IF pn_pending_terminal_batch_ids IS NULL OR pn_pending_terminal_batch_ids.COUNT < pn_max_settlements THEN
                    -- Mark command executed
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                END IF;
                IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
                    RETURN; -- found something to process so exit
                END IF;
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 2 THEN -- Retry All
                GET_RETRY_SETTLEMENTS(
                    pn_payment_subtype_key_id, 
                    pv_payment_subtype_class,
                    0,
                    pn_max_settlements, 
                    pn_pending_terminal_batch_ids);
                IF pn_pending_terminal_batch_ids IS NULL OR pn_pending_terminal_batch_ids.COUNT < pn_max_settlements THEN
                    -- Mark command executed
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                END IF;
                IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
                    RETURN; -- found something to process so exit
                END IF;   
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 3 THEN -- Retry Batch
                SELECT TERMINAL_BATCH_ID
                  BULK COLLECT INTO pn_pending_terminal_batch_ids
                  FROM (
                    SELECT C.TERMINAL_BATCH_ID
                      FROM (SELECT TERMINAL_ID, TERMINAL_BATCH_ID, TERMINAL_BATCH_OPEN_TS, 
                                   SUM(CASE WHEN TRAN_STATE_CD IN('R','N') THEN 1 ELSE 0 END) NUM_TRAN
                              FROM (SELECT T.TERMINAL_ID, 
                                           TB.TERMINAL_BATCH_ID,
                                           X.TRAN_ID,
                                           X.TRAN_STATE_CD,
                                           TB.TERMINAL_BATCH_OPEN_TS
                                      FROM PSS.TERMINAL T
                                      JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
                                      JOIN PSS.AUTH A ON TB.TERMINAL_BATCH_ID = A.TERMINAL_BATCH_ID
                                      JOIN PSS.TRAN X ON A.TRAN_ID = X.TRAN_ID 
                                     WHERE TB.TERMINAL_BATCH_OPEN_TS IS NOT NULL
                                       AND DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y') = 'N'
                                       AND T.TERMINAL_ID = pn_payment_subtype_key_id
                                     UNION 
                                    SELECT T.TERMINAL_ID, 
                                           TB.TERMINAL_BATCH_ID,
                                           X.TRAN_ID,
                                           X.TRAN_STATE_CD,
                                           TB.TERMINAL_BATCH_OPEN_TS
                                      FROM PSS.TERMINAL T
                                      JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
                                      JOIN PSS.REFUND R ON TB.TERMINAL_BATCH_ID = R.TERMINAL_BATCH_ID
                                      JOIN PSS.TRAN X ON R.TRAN_ID = X.TRAN_ID 
                                     WHERE TB.TERMINAL_BATCH_OPEN_TS IS NOT NULL
                                       AND DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y') = 'N'
                                       AND T.TERMINAL_ID = pn_payment_subtype_key_id) 
                             GROUP BY TERMINAL_ID, TERMINAL_BATCH_ID, TERMINAL_BATCH_OPEN_TS
                             HAVING COUNT(TRAN_STATE_CD) = SUM(CASE WHEN TRAN_STATE_CD IN('R', 'N', 'E') THEN 1 ELSE 0 END)) C
                      JOIN PSS.ADMIN_CMD_PARAM ACP ON C.TERMINAL_BATCH_ID = TO_NUMBER_OR_NULL(ACP.PARAM_VALUE)
                     WHERE C.NUM_TRAN > 0
                       AND ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'TERMINAL_BATCH_ID'
                   ORDER BY c.TERMINAL_BATCH_OPEN_TS ASC)
                 WHERE ROWNUM <= pn_max_settlements;
                IF pn_pending_terminal_batch_ids IS NULL OR pn_pending_terminal_batch_ids.COUNT < pn_max_settlements THEN
                    -- Mark command executed
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                END IF;
                IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
                    RETURN; -- found something to process so exit
                END IF;   
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 4 THEN -- Retry Tran
                SELECT TRAN_ID
                  BULK COLLECT INTO pn_pending_tran_ids
                  FROM (SELECT T.TRAN_ID 
                          FROM PSS.TRAN T
                          LEFT OUTER JOIN PSS.AUTH A   ON T.TRAN_ID = A.TRAN_ID
                          LEFT OUTER JOIN PSS.REFUND R ON T.TRAN_ID = R.TRAN_ID
                          JOIN PSS.ADMIN_CMD_PARAM ACP ON T.TRAN_ID = TO_NUMBER_OR_NULL(ACP.PARAM_VALUE)
                         WHERE T.TRAN_STATE_CD IN('I', 'J') /* transaction_incomplete, transaction_incomplete_error */
                           AND T.PAYMENT_SUBTYPE_KEY_ID = pn_payment_subtype_key_id 
                           AND T.PAYMENT_SUBTYPE_CLASS  = pv_payment_subtype_class
                           AND ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                           AND ACP.PARAM_NAME = 'TRAN_ID'
                           AND (pc_allow_add_to_batch = 'Y' OR NVL(A.TERMINAL_BATCH_ID, R.TERMINAL_BATCH_ID) IS NOT NULL)
                         GROUP BY T.TRAN_ID
                         ORDER BY T.TRAN_UPLOAD_TS ASC)
                 WHERE ROWNUM <= pn_max_trans;
                IF pn_pending_tran_ids IS NULL OR pn_pending_tran_ids.COUNT < pn_max_trans THEN
                    -- Mark command executed
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                END IF;
                IF pn_pending_tran_ids IS NOT NULL AND pn_pending_tran_ids.COUNT > 0 THEN 
                    RETURN; -- found something to process so exit
                END IF;
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 5 THEN -- Force Tran
                 DECLARE
                    ln_tran_id PSS.TRAN.TRAN_ID%TYPE;
                    lv_force_reason PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_RESP_DESC%TYPE;
                BEGIN
                    SELECT TO_NUMBER_OR_NULL(ACP.PARAM_VALUE)
                      INTO ln_tran_id
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'TRAN_ID';
                    SELECT ACP.PARAM_VALUE
                      INTO lv_force_reason
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'FORCE_REASON';    
                    FORCE_TRAN(pn_payment_subtype_key_id, pv_payment_subtype_class, ln_tran_id, lv_force_reason);
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Parameter ''TRAN_ID'' not found');
                    WHEN WRONG_TRAN_STATE THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, SQLERRM);
                    WHEN OTHERS THEN
                        IF SQLCODE IN(1, 20556) THEN
                            MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, SQLERRM);
                        ELSE
                            RAISE;
                        END IF;
                END;
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 6 THEN -- Force Batch
                DECLARE
                    ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
                    lv_force_reason PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_RESP_DESC%TYPE;
                BEGIN
                    SELECT TO_NUMBER_OR_NULL(ACP.PARAM_VALUE)
                      INTO ln_terminal_batch_id
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'TERMINAL_BATCH_ID';
                    SELECT ACP.PARAM_VALUE
                      INTO lv_force_reason
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'FORCE_REASON';    
                    FORCE_SETTLE_BATCH(ln_terminal_batch_id, lv_force_reason);
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Parameter ''TERMINAL_BATCH_ID'' or ''FORCE_REASON'' not found');
                    WHEN WRONG_SETTLE_BATCH_STATE THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, SQLERRM);
                    WHEN OTHERS THEN
                        IF SQLCODE = 1 THEN
                            MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, SQLERRM);
                        ELSE
                            RAISE;
                        END IF;
                END;
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 7 THEN -- Error Tran
                 DECLARE
                    ln_tran_id PSS.TRAN.TRAN_ID%TYPE;
                    lv_error_reason PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_RESP_DESC%TYPE;
                BEGIN
                    SELECT TO_NUMBER_OR_NULL(ACP.PARAM_VALUE)
                      INTO ln_tran_id
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'TRAN_ID';
                    SELECT ACP.PARAM_VALUE
                      INTO lv_error_reason
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'ERROR_REASON';    
                    ERROR_TRAN(ln_tran_id, lv_error_reason);
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Parameter ''TRAN_ID'' not found');
                    WHEN WRONG_TRAN_STATE THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, SQLERRM);
                    WHEN OTHERS THEN
                        IF SQLCODE = 1 THEN
                            MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, SQLERRM);
                        ELSE
                            RAISE;
                        END IF;
                END;
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 8 THEN -- Settle Batch
                DECLARE
                    ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
                    lc_terminal_batch_closed CHAR;
                    ln_num_tran PLS_INTEGER;
                    ln_count_tran PLS_INTEGER;
                    ln_okay_tran PLS_INTEGER;          
                BEGIN
                    SELECT MAX(TO_NUMBER_OR_NULL(ACP.PARAM_VALUE))
                      INTO ln_terminal_batch_id
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'TERMINAL_BATCH_ID';
                    IF ln_terminal_batch_id IS NULL THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Parameter ''TERMINAL_BATCH_ID'' not found');
                    ELSE
                        SELECT DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y'), 
                           SUM(CASE WHEN X.TRAN_STATE_CD IN('T') THEN 1 ELSE 0 END) NUM_TRAN,
                           COUNT(X.TRAN_STATE_CD) COUNT_TRAN,
                           SUM(CASE WHEN X.TRAN_STATE_CD IN('T', 'E') THEN 1 ELSE 0 END) OKAY_TRAN
                          INTO lc_terminal_batch_closed,
                               ln_num_tran,
                               ln_count_tran,
                               ln_okay_tran
                          FROM PSS.TERMINAL_BATCH TB
                          JOIN (SELECT A.TRAN_ID, A.TERMINAL_BATCH_ID
                                  FROM PSS.AUTH A
                                UNION
                                SELECT R.TRAN_ID, R.TERMINAL_BATCH_ID
                                  FROM PSS.REFUND R) A ON TB.TERMINAL_BATCH_ID = A.TERMINAL_BATCH_ID
                          JOIN PSS.TRAN X ON A.TRAN_ID = X.TRAN_ID
                         WHERE TB.TERMINAL_BATCH_ID = ln_terminal_batch_id 
                         GROUP BY DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y');
                        IF lc_terminal_batch_closed != 'N' THEN
                            MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Terminal Batch '|| ln_terminal_batch_id ||' is already closed');
                        ELSIF ln_okay_tran != ln_count_tran THEN
                            NULL; -- Not ready for settlement yet
                        ELSIF ln_num_tran = 0 THEN
                            MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID); -- No non-error transaction, so mark as complete and continue
                        ELSE
                            SELECT ln_terminal_batch_id
                              BULK COLLECT INTO pn_pending_terminal_batch_ids
                              FROM DUAL;
                            -- Mark command executed                          
                            MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                            RETURN; -- found something to process so exit
                        END IF;
                    END IF;
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Terminal Batch ' ||ln_terminal_batch_id||' not found');
                END;
            ELSE
                -- Unknown cmd type
                NULL;
            END IF;
        END LOOP;
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE GET_PENDING_ACTIONS(
        pn_payment_subtype_key_id PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE, 
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_settlement_retry_interval NUMBER,
        pn_tran_retry_interval NUMBER,
        pn_max_settlements PLS_INTEGER, 
        pn_max_trans PLS_INTEGER,
        pn_terminal_state_id OUT PSS.TERMINAL.TERMINAL_STATE_ID%TYPE,
        pn_pending_terminal_batch_ids OUT NUMBER_TABLE,
        pn_pending_tran_ids OUT NUMBER_TABLE)
    IS
        ln_batch_max_trans PSS.TERMINAL.TERMINAL_BATCH_MAX_TRAN%TYPE;
        ln_allowed_trans PLS_INTEGER;
        ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
        lc_settle_processing_enabled CHAR(1);
        lc_tran_processing_enabled CHAR(1);
        lv_terminal_global_token_cd PSS.TERMINAL.GLOBAL_TOKEN_CD%TYPE;
    BEGIN
        SELECT NVL(MAX(POSM_SETTING_VALUE), 'N')
          INTO lc_tran_processing_enabled
          FROM PSS.POSM_SETTING
         WHERE POSM_SETTING_NAME = 'TRAN_PROCESSING_ENABLED';
        
        -- Get terminal state id
        IF pv_payment_subtype_class  LIKE 'Authority::ISO8583%' THEN -- this is same as in APP_LAYER.VW_PAYMENT_SUBTYPE_DETAIL
            SELECT NVL(MAX(POSM_SETTING_VALUE), 'N')
              INTO lc_settle_processing_enabled
              FROM PSS.POSM_SETTING
             WHERE POSM_SETTING_NAME = 'SETTLE_PROCESSING_ENABLED';
        
            SELECT T.TERMINAL_STATE_ID, COALESCE(T.TERMINAL_BATCH_MAX_TRAN, AU.AUTHORITY_BATCH_MAX_TRAN, 999)
              INTO pn_terminal_state_id, ln_batch_max_trans
              FROM PSS.TERMINAL T
              JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
              JOIN AUTHORITY.AUTHORITY AU ON M.AUTHORITY_ID = AU.AUTHORITY_ID                       
             WHERE T.TERMINAL_ID = pn_payment_subtype_key_id;
            IF pn_terminal_state_id = 5 THEN
                -- see if there are retry admin commands
                GET_ACTIONS_FROM_ADMIN_CMDS(
                    pn_payment_subtype_key_id, 
                    pv_payment_subtype_class, 
                    pn_max_settlements, 
                    0,
                    'N', -- pc_allow_add_to_batch
                    'Y', -- pc_retry_only
                    lc_settle_processing_enabled,
                    lc_tran_processing_enabled,
                    pn_pending_terminal_batch_ids,
                    pn_pending_tran_ids);
                IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
                    RETURN; -- Early Exit if we found something to process
                END IF;
                IF lc_settle_processing_enabled = 'Y' THEN
                    -- Only settlement retry is available
                    -- Find retyable batches
                    GET_RETRY_SETTLEMENTS(
                        pn_payment_subtype_key_id, 
                        pv_payment_subtype_class,
                        pn_settlement_retry_interval,
                        pn_max_settlements, 
                        pn_pending_terminal_batch_ids);
                END IF;
                RETURN; -- if terminal is in state 5, we don't process anything else
            ELSIF pn_terminal_state_id != 3 THEN
                RETURN; -- Terminal not locked for processing
            END IF;
        ELSE
            lv_terminal_global_token_cd := GET_TERMINAL_GLOBAL_TOKEN(pv_payment_subtype_class, pn_payment_subtype_key_id);
            IF lv_terminal_global_token_cd IS NULL THEN
                pn_terminal_state_id := 1;
                RETURN;  -- Terminal not locked for processing
            ELSE
                pn_terminal_state_id := 3;
                lc_settle_processing_enabled := 'N';
            END IF;
        END IF;
        
        IF lc_tran_processing_enabled != 'Y' AND lc_settle_processing_enabled != 'Y' THEN
            RETURN; -- we are not doing any processing right now
        END IF;
        
        -- limit num of trans if need be
        IF ln_batch_max_trans IS NOT NULL THEN
            ln_terminal_batch_id := GET_AVAILABLE_TERMINAL_BATCH(pn_payment_subtype_key_id, ln_allowed_trans, FALSE);
        ELSE
            ln_allowed_trans := pn_max_trans;
        END IF;
        
        -- Check pending commands and process them first
        IF ln_allowed_trans <= 0 THEN            
            GET_ACTIONS_FROM_ADMIN_CMDS(
                pn_payment_subtype_key_id, 
                pv_payment_subtype_class, 
                pn_max_settlements, 
                pn_max_trans,
                'N', -- pc_allow_add_to_batch
                'N', -- pc_retry_only
                lc_settle_processing_enabled,
                lc_tran_processing_enabled,
                pn_pending_terminal_batch_ids,
                pn_pending_tran_ids);
        ELSE
            GET_ACTIONS_FROM_ADMIN_CMDS(
                pn_payment_subtype_key_id, 
                pv_payment_subtype_class, 
                pn_max_settlements, 
                ln_allowed_trans,
                'Y', -- pc_allow_add_to_batch
                'N', -- pc_retry_only
                lc_settle_processing_enabled,
                lc_tran_processing_enabled,
                pn_pending_terminal_batch_ids,
                pn_pending_tran_ids);
        END IF;
        IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
            RETURN; -- Early Exit if we found something to process
        ELSIF pn_pending_tran_ids IS NOT NULL AND pn_pending_tran_ids.COUNT > 0 THEN
            RETURN; -- Early Exit if we found something to process
        END IF;
        
        -- Check for Settlements
        IF lc_settle_processing_enabled = 'Y' THEN
            GET_PENDING_SETTLEMENTS(
                pn_payment_subtype_key_id, 
                pv_payment_subtype_class, 
                'N', 
                pn_max_settlements,
                pn_pending_terminal_batch_ids);
            IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
                RETURN; -- Early Exit if we found something to process
            END IF;
            GET_RETRY_SETTLEMENTS(
                pn_payment_subtype_key_id, 
                pv_payment_subtype_class,
                pn_settlement_retry_interval,
                pn_max_settlements, 
                pn_pending_terminal_batch_ids);
            IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
                RETURN; -- Early Exit if we found something to process
            END IF;
        END IF;
        
        IF lc_tran_processing_enabled != 'Y' THEN
            RETURN; -- we are not doing any tran processing right now
        END IF;
        
        -- Find retry trans
        IF ln_allowed_trans <= 0 THEN
             -- We can only process trans already in a batch
             SELECT DISTINCT TRAN_ID
              BULK COLLECT INTO pn_pending_tran_ids
              FROM (SELECT /*+ USE_NL(T,A,R) index(T IDX_TRAN_STATE_CD) index(A IDX_AUTH_TRAN_ID) */ T.TRAN_ID, T.TRAN_UPLOAD_TS
                      FROM PSS.TRAN T
                      LEFT OUTER JOIN PSS.AUTH A   ON T.TRAN_ID = A.TRAN_ID
                      LEFT OUTER JOIN PSS.REFUND R ON T.TRAN_ID = R.TRAN_ID
                     WHERE T.TRAN_STATE_CD = 'I' /* transaction_incomplete */
                       AND T.PAYMENT_SUBTYPE_KEY_ID = pn_payment_subtype_key_id 
                       AND T.PAYMENT_SUBTYPE_CLASS  = pv_payment_subtype_class
                       AND NVL(A.TERMINAL_BATCH_ID, R.TERMINAL_BATCH_ID) IS NOT NULL
                     GROUP BY T.TRAN_ID, T.TRAN_UPLOAD_TS
                    HAVING SYSDATE >= pn_tran_retry_interval + GREATEST(NVL(MAX(A.AUTH_TS), MIN_DATE), NVL(MAX(R.LAST_UPDATED_TS), MIN_DATE))
                     ORDER BY T.TRAN_UPLOAD_TS ASC, T.TRAN_ID ASC)
             WHERE ROWNUM <= pn_max_trans;
            RETURN; -- we can't process trans right now
        END IF;
        
        SELECT DISTINCT TRAN_ID
          BULK COLLECT INTO pn_pending_tran_ids
          FROM (SELECT /*+ USE_NL(T,A,R) index(T IDX_TRAN_STATE_CD) index(A IDX_AUTH_TRAN_ID) */ T.TRAN_ID, T.TRAN_UPLOAD_TS
                  FROM PSS.TRAN T
                  LEFT OUTER JOIN PSS.AUTH A   ON T.TRAN_ID = A.TRAN_ID
                  LEFT OUTER JOIN PSS.REFUND R ON T.TRAN_ID = R.TRAN_ID
                 WHERE T.TRAN_STATE_CD = 'I' /* transaction_incomplete */
                   AND T.PAYMENT_SUBTYPE_KEY_ID = pn_payment_subtype_key_id 
                   AND T.PAYMENT_SUBTYPE_CLASS  = pv_payment_subtype_class
                 GROUP BY T.TRAN_ID, T.TRAN_UPLOAD_TS
                HAVING SYSDATE >= pn_tran_retry_interval + GREATEST(NVL(MAX(A.AUTH_TS), MIN_DATE), NVL(MAX(R.LAST_UPDATED_TS), MIN_DATE))
                 ORDER BY T.TRAN_UPLOAD_TS ASC, T.TRAN_ID ASC)
         WHERE ROWNUM <= ln_allowed_trans;
        IF pn_pending_tran_ids.COUNT > 0 THEN
            RETURN; -- Early Exit if we found something to process
        END IF;
          
        -- get new pending trans
        SELECT TRAN_ID
          BULK COLLECT INTO pn_pending_tran_ids
          FROM (SELECT /*+ index(T IDX_TRAN_STATE_CD) */ T.TRAN_ID, T.TRAN_UPLOAD_TS
                  FROM PSS.TRAN T
                 WHERE T.TRAN_STATE_CD IN ('8', '9', 'A', 'W') /* processed_server_batch, processed_server_batch_intended, processing_server_tran, processed_server_auth_pending_reversal */
                   AND (T.TRAN_STATE_CD != '9' OR T.AUTH_HOLD_USED != 'Y') /* Filters out the Intended Transactions which have an Auth Hold */
                   AND T.PAYMENT_SUBTYPE_KEY_ID = pn_payment_subtype_key_id
                   AND T.PAYMENT_SUBTYPE_CLASS  = pv_payment_subtype_class
                 ORDER BY T.TRAN_UPLOAD_TS ASC, T.TRAN_ID ASC)
         WHERE ROWNUM <= ln_allowed_trans;        
    END;
    
    PROCEDURE SET_UNIQUE_SECONDS(
        pn_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE,
        pn_authority_id PSS.TERMINAL_BATCH.AUTHORITY_ID%TYPE,
        pn_unique_seconds IN OUT PSS.TERMINAL_BATCH.UNIQUE_SECONDS%TYPE)
    IS
    BEGIN
        UPDATE PSS.TERMINAL_BATCH
           SET UNIQUE_SECONDS = pn_unique_seconds,
               AUTHORITY_ID = pn_authority_id
         WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id;
    EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
            pn_unique_seconds := pn_unique_seconds + 1;
            SET_UNIQUE_SECONDS(pn_terminal_batch_id, pn_authority_id, pn_unique_seconds);
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE GET_OR_CREATE_SETTLEMENT_BATCH(
        pn_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE,
        pn_settlement_batch_id OUT PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_prior_attempts OUT PLS_INTEGER,
        pc_upload_needed_flag OUT VARCHAR2)
    IS
        lc_terminal_capture_flag PSS.TERMINAL_BATCH.TERMINAL_CAPTURE_FLAG%TYPE;
        ln_terminal_id PSS.TERMINAL_BATCH.TERMINAL_ID%TYPE;
        ln_unique_seconds PSS.TERMINAL_BATCH.UNIQUE_SECONDS%TYPE;
        ln_authority_id PSS.TERMINAL_BATCH.AUTHORITY_ID%TYPE;
    BEGIN
        -- check for an incomplete settlement_batch
        SELECT MAX(SB.SETTLEMENT_BATCH_ID), TB.TERMINAL_ID, TB.TERMINAL_CAPTURE_FLAG, TB.UNIQUE_SECONDS
          INTO pn_settlement_batch_id, ln_terminal_id, lc_terminal_capture_flag, ln_unique_seconds
          FROM PSS.TERMINAL_BATCH TB
          LEFT OUTER JOIN PSS.SETTLEMENT_BATCH SB ON TB.TERMINAL_BATCH_ID = SB.TERMINAL_BATCH_ID AND SB.SETTLEMENT_BATCH_STATE_ID = 4
         WHERE TB.TERMINAL_BATCH_ID = pn_terminal_batch_id
         GROUP BY TB.TERMINAL_ID, TB.TERMINAL_CAPTURE_FLAG, TB.UNIQUE_SECONDS;
        IF pn_settlement_batch_id IS NULL THEN
            SELECT PSS.SEQ_SETTLEMENT_BATCH_ID.NEXTVAL
              INTO pn_settlement_batch_id
              FROM DUAL;            
            
            INSERT INTO PSS.SETTLEMENT_BATCH(
                SETTLEMENT_BATCH_ID,
                SETTLEMENT_BATCH_STATE_ID,
                SETTLEMENT_BATCH_START_TS,
                TERMINAL_BATCH_ID
            ) VALUES (
                pn_settlement_batch_id,
                4,
                SYSDATE,
                pn_terminal_batch_id);
            /* Do this after transaction is uploaded (SALEd) with the Authority)    
            -- add all trans and update their tran state cd
            INSERT INTO PSS.TRAN_SETTLEMENT_BATCH(
                SETTLEMENT_BATCH_ID,
                AUTH_ID,
                TRAN_ID,
                TRAN_SETTLEMENT_B_AMT)
              SELECT DISTINCT 
                     pn_settlement_batch_id,
                     FIRST_VALUE(A.AUTH_ID) OVER (PARTITION BY T.TRAN_ID ORDER BY A.CREATED_TS DESC, A.AUTH_ID DESC) AUTH_ID, 
                     T.TRAN_ID, 
                     FIRST_VALUE(A.AUTH_AMT) OVER (PARTITION BY T.TRAN_ID ORDER BY A.CREATED_TS DESC, A.AUTH_ID DESC) AUTH_AMT
                FROM PSS.TRAN T
                JOIN PSS.AUTH A ON T.TRAN_ID = A.TRAN_ID
               WHERE A.TERMINAL_BATCH_ID = pn_terminal_batch_id
                 AND (A.AUTH_STATE_ID = 2
                      OR (A.AUTH_STATE_ID = 4 AND A.AUTH_TYPE_CD = 'N' AND A.AUTH_HOLD_USED = 'Y'));
            INSERT INTO PSS.REFUND_SETTLEMENT_BATCH(
                SETTLEMENT_BATCH_ID,
                REFUND_ID,
                TRAN_ID,
                REFUND_SETTLEMENT_B_AMT)
              SELECT DISTINCT 
                     pn_settlement_batch_id,
                     FIRST_VALUE(R.REFUND_ID) OVER (PARTITION BY T.TRAN_ID ORDER BY R.CREATED_TS DESC, R.REFUND_ID DESC) AUTH_ID, 
                     T.TRAN_ID, 
                     FIRST_VALUE(R.REFUND_AMT) OVER (PARTITION BY T.TRAN_ID ORDER BY R.CREATED_TS DESC, R.REFUND_ID DESC) REFUND_AMT
                FROM PSS.TRAN T
                JOIN PSS.REFUND R ON T.TRAN_ID = R.TRAN_ID
               WHERE R.TERMINAL_BATCH_ID = pn_terminal_batch_id
                 AND R.REFUND_STATE_ID IN(1);
            */
        END IF;
        UPDATE PSS.TRAN 
           SET TRAN_STATE_CD = 'S'
         WHERE TRAN_STATE_CD IN('R', 'N', 'T')
           AND TRAN_ID IN(
            SELECT A.TRAN_ID
              FROM PSS.AUTH A 
             WHERE A.TERMINAL_BATCH_ID = pn_terminal_batch_id
               AND (A.AUTH_STATE_ID IN(2, 5, 6)
                  OR (A.AUTH_STATE_ID = 4 AND A.AUTH_TYPE_CD = 'N' AND A.AUTH_HOLD_USED = 'Y'))
            UNION ALL
            SELECT R.TRAN_ID
              FROM PSS.REFUND R
             WHERE R.TERMINAL_BATCH_ID = pn_terminal_batch_id
               AND R.REFUND_STATE_ID IN(1, 6));
        UPDATE PSS.TERMINAL
           SET TERMINAL_STATE_ID = 5
         WHERE TERMINAL_ID = ln_terminal_id;
        SELECT COUNT(*)
          INTO pn_prior_attempts
    	  FROM PSS.SETTLEMENT_BATCH  
    	 WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id 
    	   AND SETTLEMENT_BATCH_STATE_ID IN(2, 3);
        IF pn_prior_attempts > 0 THEN
            SELECT DECODE(SETTLEMENT_BATCH_STATE_ID, 2, 'N', 3, 'Y')
              INTO pc_upload_needed_flag
              FROM (
                SELECT SETTLEMENT_BATCH_END_TS, SETTLEMENT_BATCH_ID, SETTLEMENT_BATCH_STATE_ID
                  FROM PSS.SETTLEMENT_BATCH  
                 WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id 
                   AND SETTLEMENT_BATCH_STATE_ID IN(2, 3)           
                 ORDER BY SETTLEMENT_BATCH_END_TS DESC, SETTLEMENT_BATCH_ID DESC)
             WHERE ROWNUM = 1;
        ELSE
            pc_upload_needed_flag := 'N';
        END IF;
        IF lc_terminal_capture_flag = 'Y' AND ln_unique_seconds IS NULL THEN
            SELECT ROUND(DATE_TO_MILLIS(SYSDATE) / 1000), M.AUTHORITY_ID
              INTO ln_unique_seconds, ln_authority_id
              FROM PSS.TERMINAL T
              JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
             WHERE T.TERMINAL_ID = ln_terminal_id;
            SET_UNIQUE_SECONDS(pn_terminal_batch_id, ln_authority_id, ln_unique_seconds);
        END IF;
    END;
    /*
    FUNCTION IS_LAST_TERMINAL_BATCH_OPEN(
        pn_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE)
    RETURN PLS_INTEGER
    IS
        ln_is_open PLS_INTEGER;
    BEGIN
        SELECT DECODE(TERMINAL_BATCH_CLOSE_TS, NULL, 1, 0)
          INTO ln_is_open
          FROM (SELECT TERMINAL_BATCH_NUM, 
                       TERMINAL_BATCH_CYCLE_NUM, 
                       TERMINAL_BATCH_CLOSE_TS
                  FROM PSS.TERMINAL_BATCH
                 WHERE TERMINAL_ID = pn_terminal_id
                 ORDER BY TERMINAL_BATCH_CYCLE_NUM DESC,
                          TERMINAL_BATCH_NUM DESC)
         WHERE ROWNUM = 1;
        RETURN ln_is_open;
    END;
    */
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE UPDATE_PROCESSED_BATCH(
        pn_settlement_batch_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pv_authority_response_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
        pv_authority_response_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
        pd_batch_closed_ts PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_END_TS%TYPE)
    IS
        CURSOR lc_tran IS
            SELECT SA.TRAN_ID, SA.AUTH_ID, NULL REFUND_ID, SA.AUTH_AMT_APPROVED AMOUNT,
                   AFTER_SETTLE_TRAN_STATE_CD(sa.AUTH_TYPE_CD, NULL, pc_auth_result_cd, X.TRAN_DEVICE_RESULT_TYPE_CD) NEW_TRAN_STATE_CD
              FROM PSS.AUTH SA 
              JOIN PSS.TERMINAL_BATCH TB ON TB.TERMINAL_BATCH_ID = SA.TERMINAL_BATCH_ID
              JOIN PSS.TRAN X ON SA.TRAN_ID = X.TRAN_ID
             WHERE TB.TERMINAL_BATCH_ID = pn_terminal_batch_id
               AND SA.AUTH_TYPE_CD IN('U','S','O','A','D','C','E','V','I', 'N')
               AND ((SA.AUTH_STATE_ID IN(2, 5) AND SA.AUTH_TYPE_CD != 'N')
                    OR (SA.AUTH_STATE_ID IN(6) AND TB.TERMINAL_CAPTURE_FLAG = 'Y' AND SA.AUTH_TYPE_CD != 'N')
                    OR (SA.AUTH_STATE_ID = 4 AND SA.AUTH_TYPE_CD = 'N' AND SA.AUTH_HOLD_USED = 'Y'))
               AND X.TRAN_STATE_CD IN('S')
            UNION ALL
            SELECT R.TRAN_ID, NULL, R.REFUND_ID, R.REFUND_AMT,
                   AFTER_SETTLE_TRAN_STATE_CD(NULL, r.REFUND_TYPE_CD, pc_auth_result_cd, X.TRAN_DEVICE_RESULT_TYPE_CD)
              FROM PSS.REFUND R
              JOIN PSS.TERMINAL_BATCH TB ON TB.TERMINAL_BATCH_ID = R.TERMINAL_BATCH_ID
              JOIN PSS.TRAN X ON R.TRAN_ID = X.TRAN_ID
             WHERE TB.TERMINAL_BATCH_ID = pn_terminal_batch_id
               AND (R.REFUND_STATE_ID IN(1) OR (TB.TERMINAL_CAPTURE_FLAG = 'Y' AND R.REFUND_STATE_ID IN(6)))
               AND X.TRAN_STATE_CD IN('S');
        ln_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE;
        lc_retain_acct_data CHAR(1);
        ln_settlement_batch_state_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_STATE_ID%TYPE;
    BEGIN
        SELECT SETTLEMENT_BATCH_STATE_ID
          INTO ln_settlement_batch_state_id
          FROM PSS.SETTLEMENT_BATCH
         WHERE SETTLEMENT_BATCH_ID = pn_settlement_batch_id;
        IF ln_settlement_batch_state_id = 1 OR (ln_settlement_batch_state_id IN(1,2,3,7) AND pc_auth_result_cd IN('P')) THEN
            RAISE_APPLICATION_ERROR(-20100, 'Settlement batch ' || pn_settlement_batch_id || ' was already processed');
        END IF;
        -- update the settlement batch
        UPDATE PSS.SETTLEMENT_BATCH
           SET SETTLEMENT_BATCH_STATE_ID = DECODE(pc_auth_result_cd, 'Y', 1, 'P', 7, 'N', 2, 'F', 3, 'O', 2),
               SETTLEMENT_BATCH_RESP_CD = pv_authority_response_cd,
               SETTLEMENT_BATCH_RESP_DESC = pv_authority_response_desc,
               SETTLEMENT_BATCH_REF_CD = pv_authority_ref_cd,
               SETTLEMENT_BATCH_END_TS = pd_batch_closed_ts
         WHERE SETTLEMENT_BATCH_ID = pn_settlement_batch_id;
            
        -- update the tran_state_cd of all transactions in the batch
        FOR lr_tran IN lc_tran LOOP
            IF lr_tran.NEW_TRAN_STATE_CD IN('V') THEN
                lc_retain_acct_data := 'N';
            ELSE
                lc_retain_acct_data := 'Y';
            END IF;
            UPDATE PSS.TRAN
               SET TRAN_STATE_CD = lr_tran.NEW_TRAN_STATE_CD,
                   TRAN_PARSED_ACCT_EXP_DATE = DECODE(lc_retain_acct_data, 'Y', TRAN_PARSED_ACCT_EXP_DATE),
                   TRAN_PARSED_ACCT_NUM = DECODE(lc_retain_acct_data, 'Y', TRAN_PARSED_ACCT_NUM),
                   TRAN_PARSED_ACCT_NAME = DECODE(lc_retain_acct_data, 'Y', TRAN_PARSED_ACCT_NAME),
                   TRAN_ACCOUNT_PIN = DECODE(lc_retain_acct_data, 'Y', TRAN_ACCOUNT_PIN)
             WHERE TRAN_ID = lr_tran.TRAN_ID;
            IF lr_tran.AUTH_ID IS NOT NULL THEN
                INSERT INTO PSS.TRAN_SETTLEMENT_BATCH(
                    SETTLEMENT_BATCH_ID,
                    AUTH_ID,
                    TRAN_ID,
                    TRAN_SETTLEMENT_B_AMT,
                    TRAN_SETTLEMENT_B_RESP_CD,
                    TRAN_SETTLEMENT_B_RESP_DESC
                ) VALUES (
                    pn_settlement_batch_id,
                    lr_tran.AUTH_ID,
                    lr_tran.TRAN_ID,
                    DECODE(pc_auth_result_cd, 'Y', lr_tran.AMOUNT),
                    pv_authority_response_cd,
                    pv_authority_response_desc
                );   
            ELSIF lr_tran.REFUND_ID IS NOT NULL THEN
                INSERT INTO PSS.REFUND_SETTLEMENT_BATCH(
                    SETTLEMENT_BATCH_ID,
                    REFUND_ID,
                    TRAN_ID,
                    REFUND_SETTLEMENT_B_AMT,
                    REFUND_SETTLEMENT_B_RESP_CD,
                    REFUND_SETTLEMENT_B_RESP_DESC
                ) VALUES (
                    pn_settlement_batch_id,
                    lr_tran.REFUND_ID,
                    lr_tran.TRAN_ID,
                    DECODE(pc_auth_result_cd, 'Y', lr_tran.AMOUNT),
                    pv_authority_response_cd,
                    pv_authority_response_desc
                );   
            END IF;
        END LOOP;
          
        -- if settlement successful or permanently declined, close old terminal batch and create new terminal batch
        IF pn_terminal_batch_id IS NOT NULL AND pc_auth_result_cd IN('Y', 'P', 'O') THEN
            UPDATE PSS.TERMINAL_BATCH
               SET TERMINAL_BATCH_CLOSE_TS = pd_batch_closed_ts
             WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id
             RETURNING TERMINAL_ID INTO ln_terminal_id;
            UPDATE PSS.TERMINAL
               SET TERMINAL_STATE_ID = 3
             WHERE TERMINAL_ID = ln_terminal_id;
        END IF;
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE PREPARE_PENDING_TRAN(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pn_sale_auth_id OUT PSS.AUTH.AUTH_ID%TYPE,
        pn_refund_id OUT PSS.REFUND.REFUND_ID%TYPE,
        pc_sale_phase_cd OUT VARCHAR2,
        pn_prior_attempts OUT PLS_INTEGER)
    IS
        lv_tran_lock_cd VARCHAR2(128);
        lv_lock_handle VARCHAR2(128);
        ln_actual_amt PSS.AUTH.AUTH_AMT%TYPE;
        ln_intended_amt PSS.AUTH.AUTH_AMT%TYPE;
        lc_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
        lc_auth_type_cd PSS.AUTH.AUTH_TYPE_CD%TYPE;
        ln_previous_saled_amt PSS.AUTH.AUTH_AMT%TYPE;
        lc_auth_hold_used PSS.AUTH.AUTH_HOLD_USED%TYPE;
        ln_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE;
        lc_terminal_capture_flag PSS.TERMINAL_BATCH.TERMINAL_CAPTURE_FLAG%TYPE;
        ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
        ln_is_open PLS_INTEGER;   
        ln_auth_auth_id PSS.AUTH.AUTH_ID%TYPE;
        ln_allowed_trans PLS_INTEGER;
        lc_new_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
        ln_device_result_type_cd PSS.TRAN.TRAN_DEVICE_RESULT_TYPE_CD%TYPE;
    BEGIN
        SELECT DEVICE_NAME || ':' || TRAN_DEVICE_TRAN_CD, 
               TRAN_STATE_CD,
               NVL(AUTH_HOLD_USED, 'N'),
               TERMINAL_ID,
               NVL(TERMINAL_CAPTURE_FLAG, 'N'),
               AUTH_AUTH_ID,
               SALE_AUTH_ID,
               REFUND_ID,
               TRAN_DEVICE_RESULT_TYPE_CD
          INTO lv_tran_lock_cd,
               lc_tran_state_cd,
               lc_auth_hold_used,
               ln_terminal_id,
               lc_terminal_capture_flag,
               ln_auth_auth_id,
               pn_sale_auth_id,
               pn_refund_id,
               ln_device_result_type_cd
          FROM (SELECT T.DEVICE_NAME, 
                       T.TRAN_DEVICE_TRAN_CD, 
                       A.AUTH_ID AUTH_AUTH_ID,
                       SA.AUTH_ID SALE_AUTH_ID,
                       R.REFUND_ID, 
                       T.TRAN_STATE_CD,
                       T.AUTH_HOLD_USED,
                       TE.TERMINAL_ID,
                       AU.TERMINAL_CAPTURE_FLAG,
                       T.TRAN_DEVICE_RESULT_TYPE_CD
                  FROM PSS.TRAN T
				  LEFT OUTER JOIN PSS.AUTH RA  ON T.TRAN_ID = RA.TRAN_ID AND RA.AUTH_TYPE_CD IN ('C', 'E')
                  LEFT OUTER JOIN PSS.AUTH A   ON T.TRAN_ID = A.TRAN_ID AND A.AUTH_TYPE_CD IN('L', 'N') AND (A.AUTH_RESULT_CD IN('Y', 'P') OR T.TRAN_STATE_CD IN('W') OR RA.AUTH_ID IS NOT NULL)
                  LEFT OUTER JOIN PSS.REFUND R ON T.TRAN_ID = R.TRAN_ID
                  LEFT OUTER JOIN PSS.AUTH SA  ON T.TRAN_ID = SA.TRAN_ID AND SA.AUTH_TYPE_CD IN('U','S','O','A','D','C','E','V','I') AND SA.AUTH_STATE_ID = 6
                  LEFT OUTER JOIN (PSS.TERMINAL TE
                        JOIN PSS.MERCHANT M ON TE.MERCHANT_ID = M.MERCHANT_ID
                        JOIN AUTHORITY.AUTHORITY AU ON M.AUTHORITY_ID = AU.AUTHORITY_ID
                  ) ON (T.PAYMENT_SUBTYPE_CLASS LIKE 'Authority::ISO8583%' OR T.PAYMENT_SUBTYPE_CLASS LIKE 'POSGateway%') AND T.PAYMENT_SUBTYPE_KEY_ID = TE.TERMINAL_ID
                 WHERE T.TRAN_ID = pn_tran_id
                 ORDER BY A.AUTH_RESULT_CD DESC, A.AUTH_TS DESC, A.AUTH_ID DESC, SA.AUTH_RESULT_CD DESC, SA.AUTH_TS DESC, SA.AUTH_ID DESC, R.CREATED_TS DESC, R.REFUND_ID DESC, RA.AUTH_RESULT_CD DESC, RA.AUTH_TS DESC, RA.AUTH_ID DESC)
         WHERE ROWNUM = 1;
        IF NOT(lc_tran_state_cd IN('A', '8', 'I', 'W')
            OR (lc_tran_state_cd IN('9') AND lc_auth_hold_used != 'Y' AND lc_terminal_capture_flag != 'Y')) THEN
            pc_sale_phase_cd := '-'; -- Do nothing
            pn_prior_attempts := -1;
            RETURN;
        END IF;
        -- Thus, "lc_tran_state_cd IN('A', '8', 'I', 'W') OR (lc_tran_state_cd IN('9') AND lc_auth_hold_used != 'Y' AND lc_terminal_capture_flag != 'Y')"
		lc_new_tran_state_cd := lc_tran_state_cd;
        -- Lock transaction
        lv_lock_handle := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', lv_tran_lock_cd);
        IF ln_auth_auth_id IS NOT NULL THEN -- it's a sale or a reversal
            IF lc_terminal_capture_flag = 'Y' AND lc_tran_state_cd != '9' THEN
                -- get the open terminal batch
                ln_terminal_batch_id := GET_AVAILABLE_TERMINAL_BATCH(ln_terminal_id, ln_allowed_trans, TRUE);
                SELECT NVL(SUM(DECODE(TLI.TRAN_LINE_ITEM_BATCH_TYPE_CD, 'A', TLI.TRAN_LINE_ITEM_AMOUNT * TLI.TRAN_LINE_ITEM_QUANTITY)), 0) ACTUAL_AMT
                  INTO ln_actual_amt
                  FROM PSS.TRAN_LINE_ITEM TLI
                 WHERE TLI.TRAN_ID = pn_tran_id;
                pc_sale_phase_cd := '-'; -- No further processing
                IF ln_actual_amt != 0 THEN
                    lc_auth_type_cd := 'U'; -- pre-auth sale
                    lc_new_tran_state_cd := 'T'; -- Processed Server Batch
                ELSIF lc_auth_hold_used = 'Y' THEN
                     lc_auth_type_cd := 'C'; -- Auth Reversal
                     lc_new_tran_state_cd := 'T'; -- Processed Server Batch
                ELSE
                    -- do not create sale auth record
                    lc_new_tran_state_cd := 'C'; -- client cancelled
                END IF;
            ELSIF lc_tran_state_cd = 'W' THEN
                pc_sale_phase_cd := 'R'; -- Reversal
                ln_actual_amt := 0;
                lc_auth_type_cd := 'C'; -- Auth reversal
                lc_new_tran_state_cd := 'A'; -- processing tran
            ELSE
                SELECT NVL(SUM(DECODE(TLI.TRAN_LINE_ITEM_BATCH_TYPE_CD, 'A', TLI.TRAN_LINE_ITEM_AMOUNT * TLI.TRAN_LINE_ITEM_QUANTITY)), 0) ACTUAL_AMT,
                       NVL(SUM(DECODE(TLI.TRAN_LINE_ITEM_BATCH_TYPE_CD, 'I', TLI.TRAN_LINE_ITEM_AMOUNT * TLI.TRAN_LINE_ITEM_QUANTITY)), 0) INTENDED_AMT,
                       T.TRAN_STATE_CD -- get this again now that transaction is locked
                  INTO ln_actual_amt, ln_intended_amt, lc_tran_state_cd
                  FROM PSS.TRAN_LINE_ITEM TLI
                 RIGHT OUTER JOIN PSS.TRAN T ON T.TRAN_ID = TLI.TRAN_ID
                 WHERE T.TRAN_ID = pn_tran_id 
                 GROUP BY T.TRAN_STATE_CD;
                IF lc_tran_state_cd = '9' THEN
                    IF ln_actual_amt > 0 THEN -- sanity check
                        RAISE_APPLICATION_ERROR(-20555, 'Invalid tran_state_cd for tran ' || pn_tran_id || ': Intended batch with Actual Amount');
                    ELSIF ln_intended_amt > 0 THEN
                        pc_sale_phase_cd := 'I'; -- needs processing of intended sale
                        ln_actual_amt := NULL; -- Use Intended Amt
                        lc_auth_type_cd := 'S'; -- network sale
                        lc_new_tran_state_cd := 'A'; -- processing tran
                    ELSE -- we can assume lc_auth_hold_used != 'Y' or it would not have gotten here
                        pc_sale_phase_cd := '-'; -- no processing needed
                        -- do not create sale auth record
                        lc_new_tran_state_cd := 'U'; -- processed_server_tran_intended
                    END IF;             
                ELSE
                    SELECT NVL(MAX(AUTH_AMT), 0)
                      INTO ln_previous_saled_amt
                      FROM (SELECT /*+ index(AT IDX_AUTH_TRAN_ID) */ AT.AUTH_AMT
                              FROM PSS.AUTH AT
                             WHERE AT.TRAN_ID = pn_tran_id
                               AND AT.AUTH_TYPE_CD IN('U', 'S')
                               AND AT.AUTH_STATE_ID IN(2)
                             ORDER BY AT.AUTH_TS DESC, AT.AUTH_ID DESC)
                     WHERE ROWNUM = 1;
                    IF ln_previous_saled_amt != ln_actual_amt THEN
                        pc_sale_phase_cd := 'A'; -- needs processing of actual sale
                        IF ln_actual_amt = 0 THEN -- had previous amount, but now cancelled
                            lc_auth_type_cd := 'E'; -- Sale Reversal
                        ELSIF ln_previous_saled_amt > 0 THEN
                            lc_auth_type_cd := 'D'; -- Sale Adjustment
                        ELSIF lc_auth_hold_used = 'Y' THEN                    
                            lc_auth_type_cd := 'U'; -- Pre-Authed Sale
                        ELSE
                            lc_auth_type_cd := 'S'; -- Sale
                        END IF;
                        lc_new_tran_state_cd := 'A'; -- processing tran
                    ELSIF ln_actual_amt = 0 THEN -- previous amount = actual amount = 0
                        IF ln_intended_amt > 0 AND lc_tran_state_cd != '8' THEN
                            -- This may have been an intended sale that failed, in which case we need to retry the intended amt
                            SELECT SALE_TYPE_CD
                              INTO pc_sale_phase_cd
                              FROM PSS.SALE
                             WHERE TRAN_ID = pn_tran_id;
                            IF pc_sale_phase_cd = 'I' THEN -- needs re-processing of intended sale
                                ln_actual_amt := NULL; -- Use Intended Amt
                                lc_auth_type_cd := 'S'; -- network sale
                                lc_new_tran_state_cd := 'A'; -- processing tran
                            ELSIF lc_auth_hold_used = 'Y' THEN
                                pc_sale_phase_cd := 'A'; -- Actual
                                lc_auth_type_cd := 'C'; -- Auth reversal
                                lc_new_tran_state_cd := 'A'; -- processing tran
                            ELSE
                                pc_sale_phase_cd := '-'; -- no processing needed
                                -- do not create sale auth record
                                lc_new_tran_state_cd := 'C'; -- client cancelled
                            END IF;
                        ELSIF lc_auth_hold_used = 'Y' THEN
                            IF lc_tran_state_cd IN('I', 'A') AND ln_device_result_type_cd IS NULL THEN
                                pc_sale_phase_cd := 'R'; -- Reversal
                            ELSE
                                pc_sale_phase_cd := 'A'; -- Actual
                            END IF;
                            lc_auth_type_cd := 'C'; -- Auth reversal
                            lc_new_tran_state_cd := 'A'; -- processing tran
                        ELSE
                            pc_sale_phase_cd := '-'; -- no processing needed
                            -- do not create sale auth record
                            lc_new_tran_state_cd := 'C'; -- client cancelled
                        END IF;
                    ELSE
                        pc_sale_phase_cd := '-'; -- no processing needed
                        -- do not create sale auth record
                        lc_new_tran_state_cd := 'D'; -- tran complete
                    END IF;
                END IF;
                IF pc_sale_phase_cd != '-' THEN
                    SELECT COUNT(*)
                      INTO pn_prior_attempts
                      FROM PSS.AUTH A
                     WHERE A.TRAN_ID = pn_tran_id
                       AND A.AUTH_TYPE_CD IN ('S','I','U','V')
                       AND A.AUTH_STATE_ID IN(3, 4);  	
                END IF;
            END IF;
            IF pn_sale_auth_id IS NULL AND lc_auth_type_cd IS NOT NULL THEN
                SELECT PSS.SEQ_AUTH_ID.NEXTVAL
                  INTO pn_sale_auth_id
                  FROM DUAL;
                INSERT INTO PSS.AUTH (
                        AUTH_ID,
                        TRAN_ID,
                        AUTH_TYPE_CD,
                        AUTH_STATE_ID,
                        AUTH_PARSED_ACCT_DATA,
                        ACCT_ENTRY_METHOD_CD,
                        AUTH_AMT,
                        AUTH_TS,
                        TERMINAL_BATCH_ID,
                        TRACE_NUMBER)
                 SELECT pn_sale_auth_id,
                        pn_tran_id,
                        lc_auth_type_cd,
                        6,
                        a.AUTH_PARSED_ACCT_DATA,
                        a.ACCT_ENTRY_METHOD_CD,
                        NVL(ln_actual_amt, ln_intended_amt),	    
                        SYSDATE,
                        ln_terminal_batch_id,
                        a.TRACE_NUMBER
                   FROM PSS.AUTH a
                  WHERE a.AUTH_ID = ln_auth_auth_id;
            END IF;
        ELSIF pn_refund_id IS NOT NULL THEN -- it's a refund
            IF lc_terminal_capture_flag = 'Y' THEN
                ln_terminal_batch_id := GET_AVAILABLE_TERMINAL_BATCH(ln_terminal_id, ln_allowed_trans, TRUE);
                -- stick it in the open terminal batch
                UPDATE PSS.REFUND
                   SET TERMINAL_BATCH_ID = ln_terminal_batch_id,
                       REFUND_STATE_ID = 6
                 WHERE REFUND_ID = pn_refund_id;
                pc_sale_phase_cd := '-'; -- No further processing
                lc_new_tran_state_cd := 'T'; -- Processed Server Batch
            ELSE
                -- We could check the actual amount and the previous refunded amount for sanity, but that seems overkill. 
                -- Just assume actual amount > 0 and previous refunded amount = 0
                -- Also, assume REFUND_STATE_ID IN(6, 2, 3)
                pc_sale_phase_cd := 'A'; -- needs processing of actual sale
                lc_new_tran_state_cd := 'A'; -- processing tran
                SELECT COUNT(*)
                  INTO pn_prior_attempts
                  FROM PSS.REFUND R
                 WHERE R.TRAN_ID = pn_tran_id
                   AND R.REFUND_TYPE_CD IN ('G','C','V')
                   AND R.REFUND_STATE_ID IN(2, 3);
            END IF;
        ELSE -- neither authId nor refundId is found, set to error
            pc_sale_phase_cd := '-'; -- No further processing
            lc_new_tran_state_cd := 'E'; -- Complete Error
        END IF;
		IF lc_new_tran_state_cd != lc_tran_state_cd THEN
			UPDATE PSS.TRAN 
			   SET TRAN_STATE_CD = lc_new_tran_state_cd
			 WHERE TRAN_ID = pn_tran_id;
		END IF;
    END;
    
    -- R29+ signature
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE UPDATE_PROCESSED_TRAN(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pn_sale_auth_id PSS.AUTH.AUTH_ID%TYPE,
        pn_refund_id PSS.REFUND.REFUND_ID%TYPE,
        pc_settled_flag CHAR,
        pc_sale_phase_cd CHAR,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
        pn_auth_approved_amt PSS.AUTH.AUTH_AMT_APPROVED%TYPE,
        pv_authority_response_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
        pv_authority_response_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
        pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
        pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
        pd_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
        pc_ignore_reprocess_flag CHAR DEFAULT 'N',
        pv_tran_global_trans_cd OUT PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
        pv_auth_authority_tran_cd OUT PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE)
    IS
        lv_tran_lock_cd VARCHAR2(128);
        lv_lock_handle VARCHAR2(128);
        ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
        ln_settlement_batch_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE;
        lc_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
        ln_payment_subtype_key_id PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE;
        lv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE;
        ln_allowed_trans PLS_INTEGER;
        lc_auth_type_cd PSS.AUTH.AUTH_TYPE_CD%TYPE;
    BEGIN
        SELECT T.DEVICE_NAME || ':' || T.TRAN_DEVICE_TRAN_CD, T.PAYMENT_SUBTYPE_CLASS, T.PAYMENT_SUBTYPE_KEY_ID, T.TRAN_STATE_CD, T.TRAN_GLOBAL_TRANS_CD, AA.AUTH_AUTHORITY_TRAN_CD
          INTO lv_tran_lock_cd, lv_payment_subtype_class, ln_payment_subtype_key_id, lc_tran_state_cd, pv_tran_global_trans_cd, pv_auth_authority_tran_cd
          FROM PSS.TRAN T
          LEFT OUTER JOIN PSS.AUTH AA ON T.TRAN_ID = AA.TRAN_ID AND AA.AUTH_TYPE_CD = 'N' AND AA.AUTH_STATE_ID IN(2,5)
         WHERE T.TRAN_ID = pn_tran_id;
        IF lc_tran_state_cd != 'A' THEN
            IF pc_ignore_reprocess_flag = 'Y' THEN
                RETURN;
            ELSE
                RAISE_APPLICATION_ERROR(-20554, 'Invalid tran_state_cd for tran ' || pn_tran_id || ' when updating processed tran');      
            END IF;
        END IF;
        IF pc_settled_flag = 'Y' THEN
            -- create settlement batch record
            SELECT PSS.SEQ_SETTLEMENT_BATCH_ID.NEXTVAL
              INTO ln_settlement_batch_id
              FROM DUAL;
            INSERT INTO PSS.SETTLEMENT_BATCH(
                SETTLEMENT_BATCH_ID,
                SETTLEMENT_BATCH_STATE_ID,
                SETTLEMENT_BATCH_START_TS,
                SETTLEMENT_BATCH_END_TS,
                SETTLEMENT_BATCH_RESP_CD,
                SETTLEMENT_BATCH_RESP_DESC,
                SETTLEMENT_BATCH_REF_CD
            ) VALUES (
                ln_settlement_batch_id,
                DECODE(pc_auth_result_cd, 'Y', 1, 'P', 1, 'N', 2, 'O', 2, 'F', 3),
                pd_authority_ts,
                pd_authority_ts,
                pv_authority_response_cd,
                pv_authority_response_desc,
                pv_authority_ref_cd);
        ELSIF pc_auth_result_cd IN('Y', 'P') AND (lv_payment_subtype_class  LIKE 'Authority::ISO8583%') THEN
            -- If this is a retry of a failure, it may already have a terminal_batch assigned
            IF pn_sale_auth_id IS NOT NULL THEN
                SELECT MAX(TERMINAL_BATCH_ID)
                  INTO ln_terminal_batch_id
                  FROM PSS.AUTH
                 WHERE TRAN_ID = pn_tran_id;
            ELSIF pn_refund_id IS NOT NULL THEN
                SELECT MAX(TERMINAL_BATCH_ID)
                  INTO ln_terminal_batch_id
                  FROM PSS.REFUND
                 WHERE TRAN_ID = pn_tran_id;
            END IF;
            IF ln_terminal_batch_id IS NULL THEN
                ln_terminal_batch_id := GET_AVAILABLE_TERMINAL_BATCH(ln_payment_subtype_key_id, ln_allowed_trans, TRUE);
                IF ln_allowed_trans <= 0 THEN
                    RAISE_APPLICATION_ERROR(-20556, 'Terminal Batch ' || ln_terminal_batch_id || ' is full for terminal ' || ln_payment_subtype_key_id || '; Can not add tran ' || pn_tran_id || ' to it');
                END IF;
            END IF;
        END IF;
        -- Lock transaction
        lv_lock_handle := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', lv_tran_lock_cd);
        IF pn_sale_auth_id IS NOT NULL THEN
            UPDATE PSS.AUTH 
               SET (
			    AUTH_STATE_ID,
                AUTH_RESULT_CD,
                AUTH_RESP_CD,
                AUTH_RESP_DESC,
                AUTH_AUTHORITY_TRAN_CD,
                AUTH_AUTHORITY_REF_CD,
                AUTH_AUTHORITY_TS,
                TERMINAL_BATCH_ID,
                AUTH_AMT_APPROVED,
                AUTH_AUTHORITY_MISC_DATA) =
            (SELECT 
                DECODE(pc_auth_result_cd, 'Y', 2, 'P', 5, 'N', 3, 'O', 7, 'F', 4),
                pc_auth_result_cd,
                pv_authority_response_cd,
                pv_authority_response_desc,
                pv_authority_tran_cd,
                pv_authority_ref_cd,
                pd_authority_ts,
                ln_terminal_batch_id,
                pn_auth_approved_amt / pn_minor_currency_factor,
                pv_authority_misc_data
             FROM DUAL)
             WHERE AUTH_ID = pn_sale_auth_id
             RETURNING AUTH_TYPE_CD INTO lc_auth_type_cd;
            IF ln_settlement_batch_id IS NOT NULL THEN
                INSERT INTO PSS.TRAN_SETTLEMENT_BATCH(
                    SETTLEMENT_BATCH_ID,
                    AUTH_ID,
                    TRAN_ID,
                    TRAN_SETTLEMENT_B_AMT,
                    TRAN_SETTLEMENT_B_RESP_CD,
                    TRAN_SETTLEMENT_B_RESP_DESC
                ) VALUES (
                    ln_settlement_batch_id,
                    pn_sale_auth_id,
                    pn_tran_id,
                    pn_auth_approved_amt / pn_minor_currency_factor,
                    pv_authority_response_cd,
                    pv_authority_response_desc
                );               
            END IF;
        ELSIF pn_refund_id IS NOT NULL THEN
           UPDATE PSS.REFUND
               SET REFUND_STATE_ID = DECODE(pc_auth_result_cd, 'Y', 1, 'P', 1, 'N', 2, 'O', 2, 'F', 3),
                REFUND_RESP_CD = NVL(pv_authority_response_cd, REFUND_RESP_CD),
                REFUND_RESP_DESC = NVL(pv_authority_response_desc, REFUND_RESP_DESC),
                REFUND_AUTHORITY_TRAN_CD = NVL(pv_authority_tran_cd, REFUND_AUTHORITY_TRAN_CD),
                REFUND_AUTHORITY_REF_CD = NVL(pv_authority_ref_cd, REFUND_AUTHORITY_REF_CD),
                REFUND_AUTHORITY_TS = NVL(pd_authority_ts, REFUND_AUTHORITY_TS),
                TERMINAL_BATCH_ID = NVL(ln_terminal_batch_id, TERMINAL_BATCH_ID),
                REFUND_AUTHORITY_MISC_DATA = NVL(pv_authority_misc_data, REFUND_AUTHORITY_MISC_DATA)
            WHERE REFUND_ID = pn_refund_id;
            IF ln_settlement_batch_id IS NOT NULL THEN
                INSERT INTO PSS.REFUND_SETTLEMENT_BATCH(
                    SETTLEMENT_BATCH_ID,
                    REFUND_ID,
                    TRAN_ID,
                    REFUND_SETTLEMENT_B_AMT
                ) VALUES (
                    ln_settlement_batch_id,
                    pn_refund_id,
                    pn_tran_id,
                    pn_auth_approved_amt / pn_minor_currency_factor
                );               
            END IF;
        END IF;
        
        -- update tran_state_cd
        IF pc_auth_result_cd IN('F') THEN
            lc_tran_state_cd := 'J'; -- Incomplete Error: Manual intervention needed
        ELSIF pc_auth_result_cd IN('O') THEN
            IF pc_sale_phase_cd = 'R' THEN -- it was an auth reversal
                lc_tran_state_cd := 'V';
            ELSIF lc_auth_type_cd IN('C', 'E') THEN -- sale reversal or auth reversal (indicates actual amount = 0)
                lc_tran_state_cd := 'C'; -- Client Cancelled
            ELSIF pc_sale_phase_cd = 'I' THEN
                -- check If actuals have since been uploaded
                SELECT DECODE(SALE_TYPE_CD, 'I', 'K', 'A', '8')
                  INTO lc_tran_state_cd
                  FROM PSS.SALE
                WHERE TRAN_ID = pn_tran_id;         
            ELSE
                lc_tran_state_cd := 'E'; -- Complete Error: don't retry
            END IF;
        ELSIF pc_auth_result_cd IN('N') THEN
            lc_tran_state_cd := 'I'; -- Incomplete: will retry
        ELSIF pc_auth_result_cd IN('Y', 'P') THEN
            IF pc_sale_phase_cd = 'R' THEN -- it was an auth reversal
                IF pc_settled_flag = 'Y' THEN
                    lc_tran_state_cd := 'V';
                ELSE
                     lc_tran_state_cd := 'T';
                END IF;
            ELSIF pc_sale_phase_cd = 'I' THEN
                -- check If actuals have since been uploaded
                SELECT DECODE(SALE_TYPE_CD, 'I', 'U', 'A', '8')
                  INTO lc_tran_state_cd
                  FROM PSS.SALE
                WHERE TRAN_ID = pn_tran_id;
            ELSIF pc_settled_flag = 'Y' THEN
                IF lc_auth_type_cd IN('C', 'E') THEN -- sale reversal or auth reversal (indicates actual amount = 0)
                    lc_tran_state_cd := 'C'; -- Client Cancelled
                ELSE
                    lc_tran_state_cd := 'D'; -- Complete
                END IF;
            ELSE
                lc_tran_state_cd := 'T'; -- Processed Server Batch
            END IF;
        ELSE
            RAISE_APPLICATION_ERROR(-20557, 'Invalid auth result cd "' ||  pc_auth_result_cd + '" for tran ' || pn_tran_id);
        END IF;
        UPDATE PSS.TRAN
           SET TRAN_STATE_CD = lc_tran_state_cd
         WHERE TRAN_ID = pn_tran_id;
        IF lc_tran_state_cd IN('V') THEN
            UPDATE PSS.TRAN
               SET TRAN_PARSED_ACCT_EXP_DATE = NULL,
                   TRAN_PARSED_ACCT_NUM = NULL,
                   TRAN_PARSED_ACCT_NAME = NULL,
                   TRAN_ACCOUNT_PIN = NULL
             WHERE TRAN_ID = pn_tran_id;
        END IF;
    END;      

    -- R28 signature
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE UPDATE_PROCESSED_TRAN(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pn_sale_auth_id PSS.AUTH.AUTH_ID%TYPE,
        pn_refund_id PSS.REFUND.REFUND_ID%TYPE,
        pc_settled_flag CHAR,
        pc_sale_phase_cd CHAR,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
        pn_auth_approved_amt PSS.AUTH.AUTH_AMT_APPROVED%TYPE,
        pv_authority_response_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
        pv_authority_response_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
        pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
        pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
        pd_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
        pc_ignore_reprocess_flag CHAR DEFAULT 'N')
    IS
        lv_tran_global_trans_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE;
        lv_auth_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE;
    BEGIN
        UPDATE_PROCESSED_TRAN(
            pn_tran_id,
            pn_sale_auth_id,
            pn_refund_id,
            pc_settled_flag,
            pc_sale_phase_cd,
            pc_auth_result_cd,
            pn_minor_currency_factor,
            pn_auth_approved_amt,
            pv_authority_response_cd,
            pv_authority_response_desc,
            pv_authority_tran_cd,
            pv_authority_ref_cd,
            pv_authority_misc_data,
            pd_authority_ts,
            pc_ignore_reprocess_flag,
            lv_tran_global_trans_cd,
            lv_auth_authority_tran_cd);
    END;
    
    FUNCTION GET_TERMINAL_GLOBAL_TOKEN(
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_payment_subtype_key_id NUMBER)
    RETURN VARCHAR2
    IS
        lv_global_token PSS.TERMINAL.GLOBAL_TOKEN_CD%TYPE;
    BEGIN
        IF pv_payment_subtype_class IN('Authority::NOP', 'Cash') THEN
            RAISE_APPLICATION_ERROR(-20865, 'Payment subtype class ''' || pv_payment_subtype_class || ''' does not have terminals');
        ELSIF pv_payment_subtype_class LIKE 'Authority::ISO8583%' OR pv_payment_subtype_class LIKE 'POSGateway%' THEN
            SELECT GLOBAL_TOKEN_CD
              INTO lv_global_token
              FROM PSS.TERMINAL
             WHERE TERMINAL_ID = pn_payment_subtype_key_id;
        ELSIF pv_payment_subtype_class = 'Aramark' THEN
            SELECT GLOBAL_TOKEN_CD
              INTO lv_global_token
              FROM PSS.ARAMARK_PAYMENT_TYPE
             WHERE ARAMARK_PAYMENT_TYPE_ID = pn_payment_subtype_key_id;
        ELSIF pv_payment_subtype_class = 'BlackBoard' THEN
            SELECT GLOBAL_TOKEN_CD
              INTO lv_global_token
              FROM PSS.BLACKBRD_TENDER
             WHERE BLACKBRD_TENDER_ID = pn_payment_subtype_key_id;
        ELSIF pv_payment_subtype_class = 'Internal' THEN
            SELECT GLOBAL_TOKEN_CD
              INTO lv_global_token
              FROM PSS.INTERNAL_PAYMENT_TYPE
             WHERE INTERNAL_PAYMENT_TYPE_ID = pn_payment_subtype_key_id;
        ELSE
            RAISE_APPLICATION_ERROR(-20866, 'Payment subtype class ''' || pv_payment_subtype_class || ''' is not supported');
        END IF;
        RETURN lv_global_token;
    END;
    
    PROCEDURE UPDATE_TERMINAL_GLOBAL_TOKEN(
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_payment_subtype_key_id NUMBER,
        pv_old_global_token PSS.TERMINAL.GLOBAL_TOKEN_CD%TYPE,
        pv_new_global_token PSS.TERMINAL.GLOBAL_TOKEN_CD%TYPE,
        pn_update_count OUT PLS_INTEGER,
        pc_current_must_match CHAR DEFAULT 'N')
    IS
		ln_term_lock_max_duration_sec NUMBER := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('POSM_TERMINAL_LOCK_MAX_DURATION_SEC'));
		ld_sysdate DATE := SYSDATE;
		ln_forced_unlock_count NUMBER := 0;
    BEGIN
        IF pv_new_global_token IS NULL THEN
            RAISE_APPLICATION_ERROR(-20864, 'Global Token Code may not be null');
        ELSIF pv_payment_subtype_class IN('Authority::NOP', 'Cash') THEN
            RAISE_APPLICATION_ERROR(-20865, 'Payment subtype class ''' || pv_payment_subtype_class || ''' does not have terminals');
        ELSIF pv_payment_subtype_class LIKE 'Authority::ISO8583%' OR pv_payment_subtype_class LIKE 'POSGateway%' THEN
			UPDATE PSS.TERMINAL
               SET GLOBAL_TOKEN_CD = pv_new_global_token,
                   TERMINAL_STATE_ID =  DECODE(TERMINAL_STATE_ID, 5, 5, 3) -- terminal is busy or settlement retry now
             WHERE TERMINAL_ID = pn_payment_subtype_key_id
               AND ((pc_current_must_match = 'N' AND GLOBAL_TOKEN_CD IS NULL AND TERMINAL_STATE_ID IN(1, 5))
                OR (GLOBAL_TOKEN_CD = pv_old_global_token AND TERMINAL_STATE_ID IN(3, 5)));
            pn_update_count := SQL%ROWCOUNT;
			IF pn_update_count = 0 THEN
				UPDATE PSS.TERMINAL
				   SET GLOBAL_TOKEN_CD = pv_new_global_token,
					   TERMINAL_STATE_ID =  DECODE(TERMINAL_STATE_ID, 5, 5, 3) -- terminal is busy or settlement retry now
				 WHERE TERMINAL_ID = pn_payment_subtype_key_id
					AND GLOBAL_TOKEN_CD IS NOT NULL
					AND LAST_UPDATED_TS < ld_sysdate - ln_term_lock_max_duration_sec/86400;
				pn_update_count := SQL%ROWCOUNT;
				ln_forced_unlock_count := pn_update_count;
			END IF;
        ELSIF pv_payment_subtype_class = 'Aramark' THEN
            UPDATE PSS.ARAMARK_PAYMENT_TYPE
               SET GLOBAL_TOKEN_CD = pv_new_global_token
             WHERE ARAMARK_PAYMENT_TYPE_ID = pn_payment_subtype_key_id
               AND ((pc_current_must_match = 'N' AND GLOBAL_TOKEN_CD IS NULL) OR GLOBAL_TOKEN_CD = pv_old_global_token);
            pn_update_count := SQL%ROWCOUNT;
			IF pn_update_count = 0 THEN
				UPDATE PSS.ARAMARK_PAYMENT_TYPE
				   SET GLOBAL_TOKEN_CD = pv_new_global_token
				 WHERE ARAMARK_PAYMENT_TYPE_ID = pn_payment_subtype_key_id
					AND GLOBAL_TOKEN_CD IS NOT NULL
					AND LAST_UPDATED_TS < ld_sysdate - ln_term_lock_max_duration_sec/86400;
				pn_update_count := SQL%ROWCOUNT;
				ln_forced_unlock_count := pn_update_count;
			END IF;
        ELSIF pv_payment_subtype_class = 'BlackBoard' THEN
            UPDATE PSS.BLACKBRD_TENDER
               SET GLOBAL_TOKEN_CD = pv_new_global_token
             WHERE BLACKBRD_TENDER_ID = pn_payment_subtype_key_id
               AND ((pc_current_must_match = 'N' AND GLOBAL_TOKEN_CD IS NULL) OR GLOBAL_TOKEN_CD = pv_old_global_token);
            pn_update_count := SQL%ROWCOUNT;
			IF pn_update_count = 0 THEN
				UPDATE PSS.BLACKBRD_TENDER
				   SET GLOBAL_TOKEN_CD = pv_new_global_token
				 WHERE BLACKBRD_TENDER_ID = pn_payment_subtype_key_id
					AND GLOBAL_TOKEN_CD IS NOT NULL
					AND LAST_UPDATED_TS < ld_sysdate - ln_term_lock_max_duration_sec/86400;
				pn_update_count := SQL%ROWCOUNT;
				ln_forced_unlock_count := pn_update_count;
			END IF;
        ELSIF pv_payment_subtype_class = 'Internal' THEN
            UPDATE PSS.INTERNAL_PAYMENT_TYPE
               SET GLOBAL_TOKEN_CD = pv_new_global_token
             WHERE INTERNAL_PAYMENT_TYPE_ID = pn_payment_subtype_key_id
               AND ((pc_current_must_match = 'N' AND GLOBAL_TOKEN_CD IS NULL) OR GLOBAL_TOKEN_CD = pv_old_global_token);
            pn_update_count := SQL%ROWCOUNT;
			IF pn_update_count = 0 THEN
				UPDATE PSS.INTERNAL_PAYMENT_TYPE
				   SET GLOBAL_TOKEN_CD = pv_new_global_token
				 WHERE INTERNAL_PAYMENT_TYPE_ID = pn_payment_subtype_key_id
					AND GLOBAL_TOKEN_CD IS NOT NULL
					AND LAST_UPDATED_TS < ld_sysdate - ln_term_lock_max_duration_sec/86400;
				pn_update_count := SQL%ROWCOUNT;
				ln_forced_unlock_count := pn_update_count;
			END IF;
        ELSE
            RAISE_APPLICATION_ERROR(-20866, 'Payment subtype class ''' || pv_payment_subtype_class || ''' is not supported');
        END IF;
		
		IF ln_forced_unlock_count > 0 THEN
			pkg_exception_processor.sp_log_exception (
				pkg_app_exec_hist_globals.unknown_error_id,
				'POSM terminal ' || pn_payment_subtype_key_id || '(' || pv_payment_subtype_class || ') was locked for more than ' || ln_term_lock_max_duration_sec || ' seconds, forcefully unlocked',
				NULL,
				'PSS.PKG_SETTLEMENT.UPDATE_TERMINAL_GLOBAL_TOKEN'
			);
		END IF;
    END;
    
    PROCEDURE CLEAR_TERMINAL_GLOBAL_TOKEN(
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_payment_subtype_key_id NUMBER,
        pv_global_token PSS.TERMINAL.GLOBAL_TOKEN_CD%TYPE,
        pn_update_count OUT PLS_INTEGER)
    IS
    BEGIN
        IF pv_payment_subtype_class IN('Authority::NOP', 'Cash') THEN
            RAISE_APPLICATION_ERROR(-20865, 'Payment subtype class ''' || pv_payment_subtype_class || ''' does not have terminals');
        ELSIF pv_payment_subtype_class LIKE 'Authority::ISO8583%' OR pv_payment_subtype_class LIKE 'POSGateway%' THEN
            UPDATE PSS.TERMINAL
               SET GLOBAL_TOKEN_CD = NULL,
                   TERMINAL_STATE_ID = DECODE(TERMINAL_STATE_ID, 5, 5, 1), -- terminal is active or settlement retry now
                   LAST_RELEASED_UTC_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP)
             WHERE TERMINAL_ID = pn_payment_subtype_key_id
               AND GLOBAL_TOKEN_CD = pv_global_token;
            pn_update_count := SQL%ROWCOUNT;
        ELSIF pv_payment_subtype_class = 'Aramark' THEN
            UPDATE PSS.ARAMARK_PAYMENT_TYPE
               SET GLOBAL_TOKEN_CD = NULL,
                   LAST_RELEASED_UTC_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP)
             WHERE ARAMARK_PAYMENT_TYPE_ID = pn_payment_subtype_key_id
               AND GLOBAL_TOKEN_CD = pv_global_token;
            pn_update_count := SQL%ROWCOUNT;
        ELSIF pv_payment_subtype_class = 'BlackBoard' THEN
            UPDATE PSS.BLACKBRD_TENDER
               SET GLOBAL_TOKEN_CD = NULL,
                   LAST_RELEASED_UTC_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP)
             WHERE BLACKBRD_TENDER_ID = pn_payment_subtype_key_id
               AND GLOBAL_TOKEN_CD = pv_global_token;
            pn_update_count := SQL%ROWCOUNT;
        ELSIF pv_payment_subtype_class = 'Internal' THEN
            UPDATE PSS.INTERNAL_PAYMENT_TYPE
               SET GLOBAL_TOKEN_CD = NULL,
                   LAST_RELEASED_UTC_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP)
             WHERE INTERNAL_PAYMENT_TYPE_ID = pn_payment_subtype_key_id
               AND GLOBAL_TOKEN_CD = pv_global_token;
            pn_update_count := SQL%ROWCOUNT;
        ELSE
            RAISE_APPLICATION_ERROR(-20866, 'Payment subtype class ''' || pv_payment_subtype_class || ''' is not supported');
        END IF;
    END;

    FUNCTION ADD_ADMIN_CMD(
        pv_payment_subtype_class PSS.ADMIN_CMD.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_payment_subtype_key_id PSS.ADMIN_CMD.PAYMENT_SUBTYPE_KEY_ID%TYPE,
        pn_admin_cmd_type_id PSS.ADMIN_CMD.ADMIN_CMD_TYPE_ID%TYPE,
        pv_requested_by PSS.ADMIN_CMD.REQUESTED_BY%TYPE,
        pn_priority PSS.ADMIN_CMD.PRIORITY%TYPE)
    RETURN PSS.ADMIN_CMD.ADMIN_CMD_ID%TYPE
    IS
        ln_admin_cmd_id PSS.ADMIN_CMD.ADMIN_CMD_ID%TYPE;
    BEGIN
        SELECT PSS.SEQ_ADMIN_CMD_ID.NEXTVAL
          INTO ln_admin_cmd_id
          FROM DUAL;
        INSERT INTO PSS.ADMIN_CMD (
                ADMIN_CMD_ID,
                ADMIN_CMD_TYPE_ID,
                PAYMENT_SUBTYPE_KEY_ID,
                PAYMENT_SUBTYPE_CLASS,
                REQUESTED_BY,
                PRIORITY) 
         VALUES(ln_admin_cmd_id, 
                pn_admin_cmd_type_id, 
                pn_payment_subtype_key_id,
                pv_payment_subtype_class,
                pv_requested_by,
                pn_priority);
         RETURN ln_admin_cmd_id;
    END;
    
    FUNCTION ADD_ADMIN_CMD_PARAM(
        pn_admin_cmd_id PSS.ADMIN_CMD_PARAM.ADMIN_CMD_ID%TYPE,
        pv_param_name PSS.ADMIN_CMD_PARAM.PARAM_NAME%TYPE,
        pv_param_value PSS.ADMIN_CMD_PARAM.PARAM_VALUE%TYPE)
    RETURN PSS.ADMIN_CMD_PARAM.ADMIN_CMD_PARAM_ID%TYPE
    IS
        ln_admin_cmd_param_id PSS.ADMIN_CMD_PARAM.ADMIN_CMD_PARAM_ID%TYPE;
    BEGIN
        SELECT PSS.SEQ_ADMIN_CMD_PARAM_ID.NEXTVAL
          INTO ln_admin_cmd_param_id
          FROM DUAL;
        INSERT INTO PSS.ADMIN_CMD_PARAM (
                ADMIN_CMD_PARAM_ID,
                ADMIN_CMD_ID,
                PARAM_NAME,
                PARAM_VALUE) 
         VALUES(ln_admin_cmd_param_id, 
                pn_admin_cmd_id,
                pv_param_name, 
                pv_param_value);
         RETURN ln_admin_cmd_param_id;
    END;   
    
    PROCEDURE LOCK_PARTIAL_SETTLE_POLLING(
        pv_process_id PSS.POSM_SETTING.POSM_SETTING_VALUE%TYPE,
        pv_success_flag OUT VARCHAR2,
        pv_locked_by_process_id OUT PSS.POSM_SETTING.POSM_SETTING_VALUE%TYPE)
    IS
		ln_polling_max_duration_sec NUMBER := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('POSM_FEEDBACK_POLLING_MAX_DURATION_SEC'));
		lt_last_updated_utc_ts PSS.POSM_SETTING.LAST_UPDATED_UTC_TS%TYPE;
    BEGIN
        SELECT POSM_SETTING_VALUE, 'N', LAST_UPDATED_UTC_TS
          INTO pv_locked_by_process_id, pv_success_flag, lt_last_updated_utc_ts
          FROM PSS.POSM_SETTING
         WHERE POSM_SETTING_NAME = 'PARTIAL_SETTLEMENT_POLLING_LOCK';
		 
		IF lt_last_updated_utc_ts < SYS_EXTRACT_UTC(SYSTIMESTAMP) - ln_polling_max_duration_sec/86400 THEN
			pkg_exception_processor.sp_log_exception (
				pkg_app_exec_hist_globals.unknown_error_id,
				'POSM feedback polling has been locked by ' || pv_locked_by_process_id || ' since ' || lt_last_updated_utc_ts || ' UTC, unlocking',
				NULL,
				'PSS.PKG_SETTLEMENT.LOCK_PARTIAL_SETTLE_POLLING'
			);
			UNLOCK_PARTIAL_SETTLE_POLLING(pv_locked_by_process_id);
		END IF;
		
        SELECT POSM_SETTING_VALUE, 'N'
        INTO pv_locked_by_process_id, pv_success_flag
        FROM PSS.POSM_SETTING
        WHERE POSM_SETTING_NAME = 'PARTIAL_SETTLEMENT_POLLING_LOCK';
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            BEGIN
                INSERT INTO PSS.POSM_SETTING(POSM_SETTING_NAME, POSM_SETTING_VALUE)
                  VALUES('PARTIAL_SETTLEMENT_POLLING_LOCK', pv_process_id);
                pv_success_flag := 'Y';
            EXCEPTION
                WHEN DUP_VAL_ON_INDEX THEN
                    LOCK_PARTIAL_SETTLE_POLLING(pv_process_id, pv_success_flag, pv_locked_by_process_id);
            END;
    END;
    
    PROCEDURE UNLOCK_PARTIAL_SETTLE_POLLING(
        pv_process_id PSS.POSM_SETTING.POSM_SETTING_VALUE%TYPE)
    IS
    BEGIN
        DELETE 
          FROM PSS.POSM_SETTING
         WHERE POSM_SETTING_NAME = 'PARTIAL_SETTLEMENT_POLLING_LOCK'
           AND POSM_SETTING_VALUE = pv_process_id;
        IF SQL%ROWCOUNT != 1 THEN
            RAISE NO_DATA_FOUND;
        END IF;
    END;
    
    PROCEDURE UPDATE_BATCH_FEEDBACK_REF_CD(
        pv_settlement_batch_ref_cd PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_REF_CD%TYPE,
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pd_auth_authority_ts PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_END_TS%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pv_authority_resp_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pn_settlement_batch_id OUT PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_terminal_batch_id OUT PSS.SETTLEMENT_BATCH.TERMINAL_BATCH_ID%TYPE)
    IS
        ln_settlement_batch_state_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_STATE_ID%TYPE;
    BEGIN
        SELECT SETTLEMENT_BATCH_ID, TERMINAL_BATCH_ID, SETTLEMENT_BATCH_STATE_ID
          INTO pn_settlement_batch_id, pn_terminal_batch_id, ln_settlement_batch_state_id
          FROM (SELECT SB.SETTLEMENT_BATCH_ID, SB.TERMINAL_BATCH_ID, SB.SETTLEMENT_BATCH_STATE_ID
          FROM PSS.SETTLEMENT_BATCH SB
          JOIN PSS.TERMINAL_BATCH TB ON SB.TERMINAL_BATCH_ID = TB.TERMINAL_BATCH_ID
          JOIN PSS.TERMINAL T ON TB.TERMINAL_ID = T.TERMINAL_ID
          JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
          JOIN AUTHORITY.AUTHORITY A ON M.AUTHORITY_ID = A.AUTHORITY_ID
          JOIN AUTHORITY.AUTHORITY_TYPE AUT ON A.AUTHORITY_TYPE_ID = AUT.AUTHORITY_TYPE_ID
          JOIN AUTHORITY.HANDLER H ON H.HANDLER_ID = AUT.HANDLER_ID
         WHERE SB.SETTLEMENT_BATCH_REF_CD = pv_settlement_batch_ref_cd
           AND H.HANDLER_CLASS = pv_payment_subtype_class
           AND TB.TERMINAL_BATCH_CLOSE_TS IS NOT NULL
         ORDER BY DECODE(SB.SETTLEMENT_BATCH_STATE_ID, 7, 1, 5), TB.TERMINAL_BATCH_CYCLE_NUM DESC, SB.SETTLEMENT_BATCH_ID DESC)
         WHERE ROWNUM = 1; 
        
        IF ln_settlement_batch_state_id = 1 OR (ln_settlement_batch_state_id IN(1,2,3,7) AND pc_auth_result_cd IN('P')) THEN
            RAISE_APPLICATION_ERROR(-20100, 'Settlement batch ' || pn_settlement_batch_id || ' was already processed');
        END IF;
           
        UPDATE PSS.SETTLEMENT_BATCH SB
           SET SB.SETTLEMENT_BATCH_STATE_ID = DECODE(pc_auth_result_cd, 'Y', 1, 'P', 7, 'N', 2, 'F', 3, 'O', 2),
               SB.SETTLEMENT_BATCH_END_TS = NVL(pd_auth_authority_ts, SYSDATE),
               SB.SETTLEMENT_BATCH_RESP_DESC = DECODE(TRIM(pv_authority_resp_desc), NULL, SETTLEMENT_BATCH_RESP_DESC, SUBSTR(SETTLEMENT_BATCH_RESP_DESC || ' => ' || TRIM(pv_authority_resp_desc) , 1, 2000))
         WHERE SB.SETTLEMENT_BATCH_ID = pn_settlement_batch_id;                 
    END;
    
    PROCEDURE UPDATE_BATCH_FEEDBACK(
        pv_merchant_cd PSS.MERCHANT.MERCHANT_CD%TYPE,
        pv_terminal_cd PSS.TERMINAL.TERMINAL_CD%TYPE,
        pv_terminal_batch_num PSS.TERMINAL_BATCH.TERMINAL_BATCH_NUM%TYPE,
        pd_auth_authority_ts PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_END_TS%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pv_authority_resp_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pn_settlement_batch_id OUT PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_terminal_batch_id OUT PSS.SETTLEMENT_BATCH.TERMINAL_BATCH_ID%TYPE)
    IS
        ln_settlement_batch_state_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_STATE_ID%TYPE;
    BEGIN
        SELECT SETTLEMENT_BATCH_ID, TERMINAL_BATCH_ID, SETTLEMENT_BATCH_STATE_ID
          INTO pn_settlement_batch_id, pn_terminal_batch_id, ln_settlement_batch_state_id
          FROM (SELECT SB.SETTLEMENT_BATCH_ID, SB.TERMINAL_BATCH_ID, SB.SETTLEMENT_BATCH_STATE_ID
          FROM PSS.SETTLEMENT_BATCH SB
          JOIN PSS.TERMINAL_BATCH TB ON SB.TERMINAL_BATCH_ID = TB.TERMINAL_BATCH_ID
          JOIN PSS.TERMINAL T ON TB.TERMINAL_ID = T.TERMINAL_ID
          JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
         WHERE M.MERCHANT_CD = pv_merchant_cd
           AND T.TERMINAL_CD = pv_terminal_cd
           AND TB.TERMINAL_BATCH_NUM = pv_terminal_batch_num
           AND TB.TERMINAL_BATCH_CLOSE_TS IS NOT NULL
         ORDER BY DECODE(SB.SETTLEMENT_BATCH_STATE_ID, 7, 1, 5), TB.TERMINAL_BATCH_CYCLE_NUM DESC, SB.SETTLEMENT_BATCH_ID DESC)
         WHERE ROWNUM = 1; 
           
        IF ln_settlement_batch_state_id = 1 OR (ln_settlement_batch_state_id IN(1,2,3,7) AND pc_auth_result_cd IN('P')) THEN
            RAISE_APPLICATION_ERROR(-20100, 'Settlement batch ' || pn_settlement_batch_id || ' was already processed');
        END IF;
        
        UPDATE PSS.SETTLEMENT_BATCH SB
           SET SB.SETTLEMENT_BATCH_STATE_ID = DECODE(pc_auth_result_cd, 'Y', 1, 'P', 7, 'N', 2, 'F', 3, 'O', 2),
               SB.SETTLEMENT_BATCH_END_TS = NVL(pd_auth_authority_ts, SYSDATE),
               SB.SETTLEMENT_BATCH_RESP_DESC = DECODE(TRIM(pv_authority_resp_desc), NULL, SETTLEMENT_BATCH_RESP_DESC, SUBSTR(SETTLEMENT_BATCH_RESP_DESC || ' => ' || TRIM(pv_authority_resp_desc) , 1, 2000))
         WHERE SB.SETTLEMENT_BATCH_ID = pn_settlement_batch_id;                 
    END;
   
   -- R29+ signature 
    PROCEDURE UPDATE_TRAN_FEEDBACK(
        pn_terminal_batch_id PSS.SETTLEMENT_BATCH.TERMINAL_BATCH_ID%TYPE,
        pn_settlement_batch_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_sale_auth_id PSS.AUTH.AUTH_ID%TYPE,
        pn_refund_id PSS.REFUND.REFUND_ID%TYPE,
        pc_tran_type_cd CHAR,
        pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
        pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pv_authority_response_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
        pv_authority_response_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
        pd_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
        pn_approved_amt PSS.AUTH.AUTH_AMT_APPROVED%TYPE,
        pn_sale_amt PSS.AUTH.AUTH_AMT%TYPE,
        pc_major_currency_used CHAR,
        pv_tran_global_trans_cd OUT PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE)
    IS
        ln_original_auth_id PSS.AUTH.AUTH_ID%TYPE;
        ln_tran_id PSS.TRAN.TRAN_ID%TYPE;
        ln_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE;
        lc_entry_method_cd PSS.AUTH.ACCT_ENTRY_METHOD_CD%TYPE;
        lv_trace_number PSS.AUTH.TRACE_NUMBER%TYPE;
        ln_auth_id PSS.AUTH.AUTH_ID%TYPE;
        ln_refund_id PSS.REFUND.REFUND_ID%TYPE;
        lc_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
        lc_auth_type_cd PSS.AUTH.AUTH_TYPE_CD%TYPE;
        lc_refund_type_cd PSS.REFUND.REFUND_TYPE_CD%TYPE;
    BEGIN
        IF pn_sale_auth_id IS NOT NULL THEN
            ln_auth_id := pn_sale_auth_id;
        ELSIF pn_refund_id IS NOT NULL THEN
            ln_refund_id := pn_refund_id;
        ELSIF pc_tran_type_cd = '+' THEN
            SELECT AUTH_ID
              INTO ln_auth_id
              FROM (SELECT SA.AUTH_ID
              FROM PSS.AUTH A
              JOIN PSS.AUTH SA ON A.TRAN_ID = SA.TRAN_ID
             WHERE SA.TERMINAL_BATCH_ID = pn_terminal_batch_id
               AND A.AUTH_AUTHORITY_TRAN_CD = pv_authority_tran_cd 
               AND A.AUTH_AUTHORITY_REF_CD = pv_authority_ref_cd
               AND SA.AUTH_TYPE_CD IN('U','S','O','A','D','C','E','V','I')
               AND A.AUTH_TYPE_CD IN('L','N')
             ORDER BY SA.AUTH_TS DESC, SA.AUTH_ID DESC)
             WHERE ROWNUM = 1;
        ELSIF pc_tran_type_cd = '-' THEN
            SELECT REFUND_ID
              INTO ln_refund_id
              FROM (SELECT R.REFUND_ID, R.TRAN_ID
                      FROM PSS.REFUND R
                     WHERE R.TERMINAL_BATCH_ID = pn_terminal_batch_id
                       AND R.REFUND_AUTHORITY_TRAN_CD = pv_authority_tran_cd 
                       AND R.REFUND_AUTHORITY_REF_CD = pv_authority_ref_cd
                       AND R.REFUND_STATE_ID IN(6, 1)
                     ORDER BY R.CREATED_TS DESC, R.REFUND_ID DESC)
             WHERE ROWNUM = 1;
        ELSE
            RAISE_APPLICATION_ERROR(-20558, 'Invalid tran type cd "' ||  pc_tran_type_cd + '"');
        END IF;
        
        IF ln_auth_id IS NOT NULL THEN
            IF pc_major_currency_used = 'Y' THEN
                ln_minor_currency_factor := 1;
            ELSE
                SELECT C.MINOR_CURRENCY_FACTOR
                  INTO ln_minor_currency_factor
                  FROM PSS.CURRENCY C
                  JOIN PSS.POS_PTA PTA ON C.CURRENCY_CD = NVL(pta.CURRENCY_CD, 'USD')
                  JOIN PSS.TRAN X ON PTA.POS_PTA_ID = X.POS_PTA_ID
                  JOIN PSS.AUTH A ON A.TRAN_ID = X.TRAN_ID
                 WHERE A.AUTH_ID = ln_auth_id;
            END IF;
            UPDATE PSS.AUTH 
               SET AUTH_STATE_ID = DECODE(pc_auth_result_cd, 'Y', 2, 'P', 5, 'N', 3, 'O', 7, 'F', 4),
                   AUTH_RESULT_CD = pc_auth_result_cd,
                   AUTH_RESP_CD = NVL(pv_authority_response_cd, AUTH_RESP_CD),
                   AUTH_RESP_DESC = NVL(pv_authority_response_desc, AUTH_RESP_DESC),
                   AUTH_AUTHORITY_TRAN_CD = NVL(pv_authority_tran_cd, AUTH_AUTHORITY_TRAN_CD),
                   AUTH_AUTHORITY_REF_CD = NVL(pv_authority_ref_cd, AUTH_AUTHORITY_REF_CD),
                   AUTH_AUTHORITY_TS = NVL(pd_authority_ts, AUTH_AUTHORITY_TS),
                   AUTH_AMT_APPROVED = NVL(pn_approved_amt / ln_minor_currency_factor, AUTH_AMT_APPROVED),
                   AUTH_AUTHORITY_MISC_DATA = NVL(pv_authority_misc_data, AUTH_AUTHORITY_MISC_DATA)
             WHERE AUTH_ID = ln_auth_id
             RETURNING TRAN_ID, AUTH_TYPE_CD INTO ln_tran_id, lc_auth_type_cd;
            IF pn_settlement_batch_id IS NOT NULL THEN
                UPDATE PSS.TRAN_SETTLEMENT_BATCH
                   SET TRAN_SETTLEMENT_B_AMT = NVL(pn_approved_amt / ln_minor_currency_factor, TRAN_SETTLEMENT_B_AMT),
                       TRAN_SETTLEMENT_B_RESP_CD = NVL(pv_authority_response_cd, TRAN_SETTLEMENT_B_RESP_CD),
                       TRAN_SETTLEMENT_B_RESP_DESC = NVL(pv_authority_response_desc, TRAN_SETTLEMENT_B_RESP_DESC)
                 WHERE SETTLEMENT_BATCH_ID = pn_settlement_batch_id
                   AND AUTH_ID = ln_auth_id;               
            END IF;
        ELSIF ln_refund_id IS NOT NULL THEN
            IF pc_major_currency_used = 'Y' THEN
                ln_minor_currency_factor := 1;
            ELSE
                SELECT C.MINOR_CURRENCY_FACTOR
                  INTO ln_minor_currency_factor
                  FROM PSS.CURRENCY C
                  JOIN PSS.POS_PTA PTA ON C.CURRENCY_CD = NVL(pta.CURRENCY_CD, 'USD')
                  JOIN PSS.TRAN X ON PTA.POS_PTA_ID = X.POS_PTA_ID
                  JOIN PSS.REFUND R ON X.TRAN_ID = R.TRAN_ID
                 WHERE R.REFUND_ID = ln_refund_id;
            END IF;          
            UPDATE PSS.REFUND
               SET REFUND_STATE_ID = DECODE(pc_auth_result_cd, 'Y', 1, 'P', 1, 'N', 2, 'O', 2, 'F', 3),
                REFUND_RESP_CD = NVL(pv_authority_response_cd, REFUND_RESP_CD),
                REFUND_RESP_DESC = NVL(pv_authority_response_desc, REFUND_RESP_DESC),
                REFUND_AUTHORITY_TS = NVL(pd_authority_ts, REFUND_AUTHORITY_TS),
                REFUND_AUTHORITY_MISC_DATA = NVL(pv_authority_misc_data, REFUND_AUTHORITY_MISC_DATA)
             WHERE REFUND_ID = ln_refund_id
             RETURNING TRAN_ID, REFUND_TYPE_CD INTO ln_tran_id, lc_refund_type_cd;
            IF pn_settlement_batch_id IS NOT NULL THEN 
                UPDATE PSS.REFUND_SETTLEMENT_BATCH
                   SET REFUND_SETTLEMENT_B_AMT = NVL(pn_approved_amt / ln_minor_currency_factor, REFUND_SETTLEMENT_B_AMT),
                       REFUND_SETTLEMENT_B_RESP_CD = NVL(pv_authority_response_cd, REFUND_SETTLEMENT_B_RESP_CD),
                       REFUND_SETTLEMENT_B_RESP_DESC = NVL(pv_authority_response_desc, REFUND_SETTLEMENT_B_RESP_DESC)
                 WHERE SETTLEMENT_BATCH_ID = pn_settlement_batch_id
                   AND REFUND_ID = ln_refund_id;   
            END IF;
        END IF;
        
        -- update tran_state_cd
        UPDATE PSS.TRAN
           SET TRAN_STATE_CD = AFTER_SETTLE_TRAN_STATE_CD(lc_auth_type_cd, lc_refund_type_cd, pc_auth_result_cd, TRAN_DEVICE_RESULT_TYPE_CD)
         WHERE TRAN_ID = ln_tran_id
         RETURNING TRAN_GLOBAL_TRANS_CD INTO pv_tran_global_trans_cd;
    END;
    
    -- R28 signature
    PROCEDURE UPDATE_TRAN_FEEDBACK(
        pn_terminal_batch_id PSS.SETTLEMENT_BATCH.TERMINAL_BATCH_ID%TYPE,
        pn_settlement_batch_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_sale_auth_id PSS.AUTH.AUTH_ID%TYPE,
        pn_refund_id PSS.REFUND.REFUND_ID%TYPE,
        pc_tran_type_cd CHAR,
        pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
        pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pv_authority_response_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
        pv_authority_response_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
        pd_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
        pn_approved_amt PSS.AUTH.AUTH_AMT_APPROVED%TYPE,
        pn_sale_amt PSS.AUTH.AUTH_AMT%TYPE,
        pc_major_currency_used CHAR)
    IS
        lv_tran_global_trans_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE;
    BEGIN
        UPDATE_TRAN_FEEDBACK(
            pn_terminal_batch_id,
            pn_settlement_batch_id,
            pn_sale_auth_id,
            pn_refund_id,
            pc_tran_type_cd,
            pv_authority_tran_cd,
            pv_authority_ref_cd,
            pc_auth_result_cd,
            pv_authority_response_cd,
            pv_authority_response_desc,
            pv_authority_misc_data,
            pd_authority_ts,
            pn_approved_amt,
            pn_sale_amt,
            pc_major_currency_used,
            lv_tran_global_trans_cd);
    END;
    
END PKG_SETTLEMENT;
/

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/DBADMIN/PKG_CONST.psk?rev=1.19
CREATE OR REPLACE PACKAGE DBADMIN.PKG_CONST 
  IS

DB_TIME_ZONE CONSTANT VARCHAR2(60) := 'US/Eastern';
DB_TIME_ZONE_CD CONSTANT VARCHAR2(50) := 'EST';
GMT_TIME_ZONE CONSTANT VARCHAR2(60) := 'Europe/London';

-- conditional logic
BOOLEAN__FALSE CONSTANT NUMBER := 0;
BOOLEAN__TRUE CONSTANT NUMBER := 1;

-- ASCII characters
ASCII__NUL CONSTANT CHAR(1) := CHR(0);
ASCII__LF CONSTANT CHAR(1) := CHR(10);

-- client payment type codes
CLNT_PMNT_TYPE__CASH CONSTANT CHAR(1) := 'M';
CLNT_PMNT_TYPE__CREDIT CONSTANT CHAR(1) := 'C';
CLNT_PMNT_TYPE__RFID_CREDIT CONSTANT CHAR(1) := 'R';
CLNT_PMNT_TYPE__RFID_SPECIAL CONSTANT CHAR(1) := 'P';
CLNT_PMNT_TYPE__SPECIAL CONSTANT CHAR(1) := 'S';

-- device types
DEVICE_TYPE__G4 CONSTANT NUMBER := 0;
DEVICE_TYPE__GX CONSTANT NUMBER := 1;
DEVICE_TYPE__NG CONSTANT NUMBER := 3;
DEVICE_TYPE__LEGACY_KIOSK CONSTANT NUMBER := 4;
DEVICE_TYPE__ESUDS CONSTANT NUMBER := 5;
DEVICE_TYPE__MEI CONSTANT NUMBER := 6;
DEVICE_TYPE__EZ80_DEVELOPMENT CONSTANT NUMBER := 7;
DEVICE_TYPE__EZ80 CONSTANT NUMBER := 8;
DEVICE_TYPE__LEGACY_G4 CONSTANT NUMBER := 9;
DEVICE_TYPE__TRANSACT CONSTANT NUMBER := 10;
DEVICE_TYPE__KIOSK CONSTANT NUMBER := 11;
DEVICE_TYPE__T2 CONSTANT NUMBER := 12;
DEVICE_TYPE__EDGE CONSTANT NUMBER := 13;

DSP__SETTLEMENT_SCHEDULE CONSTANT VARCHAR2(60) := '85';
DSP__NON_ACTIV_CALL_IN_SCHED CONSTANT VARCHAR2(60) := '86';
DSP__ACTIVATED_CALL_IN_SCHED CONSTANT VARCHAR2(60) := '87';
DSP__DEX_SCHEDULE CONSTANT VARCHAR2(60) := '1101';
DSP__VMC_INTERFACE_TYPE CONSTANT VARCHAR2(60) := '1500';
DSP__PROPERTY_LIST_VERSION CONSTANT VARCHAR2(60) := 'Property List Version';

-- errors
ERROR__GENERIC_FAILURE CONSTANT VARCHAR2(255) := 'Generic Failure';
ERROR__NO_ERROR CONSTANT VARCHAR2(255) := NULL;

-- event code prefixes
EVENT_CODE_PREFIX__APP_LAYER CONSTANT CHAR := 'A';
EVENT_CODE_PREFIX__LEGACY CONSTANT CHAR := 'X';

-- event states (DEVICE.EVENT_STATE)
EVENT_STATE__INCOMPLETE CONSTANT NUMBER := 1;
EVENT_STATE__COMPLETE_FINAL CONSTANT NUMBER := 2;
EVENT_STATE__COMPLETE_ERROR CONSTANT NUMBER := 3;
EVENT_STATE__INCOMPLETE_ERROR CONSTANT NUMBER := 4;

-- file transfer types (DEVICE.FILE_TRANSFER_TYPE)
FILE_TYPE__DEX CONSTANT NUMBER := 0;
FILE_TYPE__CONFIG CONSTANT NUMBER := 1;
FILE_TYPE__APP_UPGRADE CONSTANT NUMBER := 5;
FILE_TYPE__PROPERTY_LIST CONSTANT NUMBER := 19;

-- Edge schedule reoccurrence types
REOCCURRENCE_TYPE__DAILY CONSTANT CHAR(1) := 'D';
REOCCURRENCE_TYPE__INTERVAL CONSTANT CHAR(1) := 'I';
REOCCURRENCE_TYPE__MONTHLY CONSTANT CHAR(1) := 'M';
REOCCURRENCE_TYPE__WEEKLY CONSTANT CHAR(1) := 'W';

-- result codes
RESULT__FAILURE CONSTANT NUMBER := 0;
RESULT__SUCCESS CONSTANT NUMBER := 1;
RESULT__TRAN_NOT_FOUND CONSTANT NUMBER := 2;
RESULT__ILLEGAL_STATE CONSTANT NUMBER := 3;
RESULT__INVALID_PARAMETER CONSTANT NUMBER := 4;
RESULT__DUPLICATE CONSTANT NUMBER := 5;
RESULT__PAYMENT_NOT_ACCEPTED CONSTANT NUMBER := 6;
RESULT__HOST_NOT_FOUND CONSTANT NUMBER := 7;
RESULT__ITEM_TYPE_NOT_FOUND CONSTANT NUMBER := 8;

-- sale results (PSS.SALE_RESULT)
SALE_RES__SUCCESS CONSTANT NUMBER := 0;
SALE_RES__CANCELLED_BY_USER CONSTANT NUMBER := 1;
SALE_RES__CNCLD_AUTH_TIMEOUT CONSTANT NUMBER := 2;
SALE_RES__CNCLD_AUTH_FAILURE CONSTANT NUMBER := 3;
SALE_RES__CNCLD_MACHINE_FAIL CONSTANT NUMBER := 4;

-- sale types (PSS.SALE_TYPE)
SALE_TYPE__ACTUAL CONSTANT CHAR(1) := 'A';
SALE_TYPE__CASH CONSTANT CHAR(1) := 'C';
SALE_TYPE__INTENDED CONSTANT CHAR(1) := 'I';

-- Edge schedule constants
SCHEDULE__FS CONSTANT CHAR(1) := '^';
SCHEDULE__SUNDAY CONSTANT CHAR(1) := '0';
SCHEDULE__MONDAY CONSTANT CHAR(1) := '1';
SCHEDULE__TUESDAY CONSTANT CHAR(1) := '2';
SCHEDULE__WEDNESDAY CONSTANT CHAR(1) := '3';
SCHEDULE__THURSDAY CONSTANT CHAR(1) := '4';
SCHEDULE__FRIDAY CONSTANT CHAR(1) := '5';
SCHEDULE__SATURDAY CONSTANT CHAR(1) := '6';

-- tran line item types (PSS.TRAN_LINE_ITEM_TYPE)
TLI__CANCELLATION_ADJMT CONSTANT NUMBER := 312;
TLI__NEG_DISCREPANCY_ADJMT CONSTANT NUMBER := 313;
TLI__POS_DISCREPANCY_ADJMT CONSTANT NUMBER := 314;

-- transaction batch types
TRAN_BATCH_TYPE__ACTUAL CONSTANT CHAR(1) := 'A';
TRAN_BATCH_TYPE__INTENDED CONSTANT CHAR(1) := 'I';

-- transaction result codes reported by device (PSS.TRAN_DEVICE_RESULT_TYPE)
TRAN_DEV_RES__AUTH_FAILURE CONSTANT VARCHAR2(1) := 'U';
TRAN_DEV_RES__CANCELLED CONSTANT VARCHAR2(1) := 'C';
TRAN_DEV_RES__FAILURE CONSTANT VARCHAR2(1) := 'F';
TRAN_DEV_RES__INCOMPLETE CONSTANT VARCHAR2(1) := 'I';
TRAN_DEV_RES__SUCCESS CONSTANT VARCHAR2(1) := 'S';
TRAN_DEV_RES__SUCCESS_NO_PRNTR CONSTANT VARCHAR2(1) := 'Q';
TRAN_DEV_RES__SUCCESS_NO_RCPT CONSTANT VARCHAR2(1) := 'N';
TRAN_DEV_RES__SUCCESS_RCPT_ERR CONSTANT VARCHAR2(1) := 'R';
TRAN_DEV_RES__TIMEOUT CONSTANT VARCHAR2(1) := 'T';

-- transaction states (PSS.TRAN_STATE)
TRAN_STATE__AUTH_DECLINE CONSTANT VARCHAR2(1) := '7';
TRAN_STATE__AUTH_EXPIRED CONSTANT VARCHAR2(1) := 'L';
TRAN_STATE__AUTH_COND_EXPIRED CONSTANT VARCHAR2(1) := 'M';
TRAN_STATE__AUTH_FAILURE CONSTANT VARCHAR2(1) := '5';
TRAN_STATE__AUTH_REVERSED CONSTANT VARCHAR2(1) := 'V';
TRAN_STATE__AUTH_PEND_RVRSL CONSTANT VARCHAR2(1) := 'W';
TRAN_STATE__AUTH_SUCCESS CONSTANT VARCHAR2(1) := '6';
TRAN_STATE__AUTH_SUCCESS_COND CONSTANT VARCHAR2(1) := '0';
TRAN_STATE__BATCH CONSTANT VARCHAR2(1) := '8';
TRAN_STATE__BATCH_INTENDED CONSTANT VARCHAR2(1) := '9';
TRAN_STATE__CLIENT_CANCELLED CONSTANT VARCHAR2(1) := 'C';
TRAN_STATE__COMPLETE CONSTANT VARCHAR2(1) := 'D';
TRAN_STATE__COMPLETE_ERROR CONSTANT VARCHAR2(1) := 'E';
TRAN_STATE__DUPLICATE CONSTANT VARCHAR2(1) := 'Z';
TRAN_STATE__INCOMPLETE CONSTANT VARCHAR2(1) := 'I';
TRAN_STATE__INCOMPLETE_ERROR CONSTANT VARCHAR2(1) := 'J';
TRAN_STATE__INTENDED_ERROR CONSTANT VARCHAR2(1) := 'K';
TRAN_STATE__PREPARED_TRAN CONSTANT VARCHAR2(1) := 'P';
TRAN_STATE__PROCESSED_TRAN CONSTANT VARCHAR2(1) := 'T';
TRAN_STATE__PROCESSED_TRAN_INT CONSTANT VARCHAR2(1) := 'U';
TRAN_STATE__PROCESSING_BATCH CONSTANT VARCHAR2(1) := '4';
TRAN_STATE__PRCSNG_BATCH_INTD CONSTANT VARCHAR2(1) := '3';
TRAN_STATE__PRCSNG_BATCH_LOCAL CONSTANT VARCHAR2(1) := '1';
TRAN_STATE__PROCESSING_STTLMT CONSTANT VARCHAR2(1) := 'S';
TRAN_STATE__PROCESSING_TRAN CONSTANT VARCHAR2(1) := 'A';
TRAN_STATE__PRCSNG_TRAN_RETRY CONSTANT VARCHAR2(1) := 'B';
TRAN_STATE__SALE_NO_AUTH CONSTANT VARCHAR2(1) := 'F';
TRAN_STATE__SALE_NO_AUTH_ERROR CONSTANT VARCHAR2(1) := 'G';
TRAN_STATE__SALE_NO_AUTH_INTND CONSTANT VARCHAR2(1) := 'H';
TRAN_STATE__SETTLEMENT CONSTANT VARCHAR2(1) := 'Q';
TRAN_STATE__STLMT_ERROR CONSTANT VARCHAR2(1) := 'R';
TRAN_STATE__STLMT_INCOMPLETE CONSTANT VARCHAR2(1) := 'N';
TRAN_STATE__WAITING_FOR_AUTH CONSTANT VARCHAR2(1) := '2';

TRANS_TYPE__CREDIT CONSTANT NUMBER := 16;
TRANS_TYPE__ACCESS CONSTANT NUMBER := 17;
TRANS_TYPE__PASS CONSTANT NUMBER := 18;
TRANS_TYPE__CASH CONSTANT NUMBER := 22;

VMC_INTERFACE__STANDARD_MDB CONSTANT VARCHAR2(200) := '1';
VMC_INTERFACE__ETRANS_MDB CONSTANT VARCHAR2(200) := '2';
VMC_INTERFACE__COIN_PULSE CONSTANT VARCHAR2(200) := '3';

END;
/

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/DMS/DMS-1.0.5-USADBP.1.sql?rev=HEAD
GRANT SELECT ON DEVICE.SETTING_PARAMETER_TYPE TO USAT_DMS_ROLE, USAT_APP_LAYER_ROLE;
GRANT EXECUTE ON DEVICE.SP_UPDATE_CONFIG_AND_RETURN TO USAT_DMS_ROLE;

UPDATE DEVICE.DEVICE_SETTING_PARAMETER
SET DEVICE_SETTING_UI_CONFIGURABLE = 'N'
WHERE DEVICE_SETTING_PARAMETER_CD IN ('60', '61');

UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
SET EDITOR = 'SELECT:1=1;2=2;3=3;4=4;5=5;6=6',
DESCRIPTION = '1 - Standard MDB<br />2 - eTrans MDB<br />3 - Coin Pulse<br />4 - Coin Pulse Dual Enable<br />5 - Top-off Coin Pulse<br />6 - Serial Edge'
WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1500';

UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
SET NAME = 'Disable Message Line 1', DESCRIPTION = 'The device will display this message on the top line of the display when disabled by the VMC'
WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1527';

UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
SET NAME = 'Disable Message Line 2', DESCRIPTION = 'The device will display this message on the bottom line of the display when disabled by the VMC'
WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1528';

UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET DESCRIPTION = 'SSN (silicone serial number), terminal serial number with the bytes in decimal format and a comma as the byte separator' WHERE DEVICE_TYPE_ID = 11 AND DEVICE_SETTING_PARAMETER_CD = 'SSN';
COMMIT;

DECLARE
  LV_SQL VARCHAR2(4000);
BEGIN
  SELECT 'CREATE SEQUENCE DEVICE.RERIX_MACHINE_ID_SEQ INCREMENT BY 1 MINVALUE ' || MIN_VALUE || ' MAXVALUE ' || MAX_VALUE 
    || ' START WITH ' || LAST_NUMBER || ' ORDER NOCYCLE' 
  INTO LV_SQL
  FROM DBA_SEQUENCES 
  WHERE SEQUENCE_OWNER = 'TAZDBA' AND SEQUENCE_NAME = 'RERIX_MACHINE_ID_SEQ';

  EXECUTE IMMEDIATE LV_SQL;
  
  SELECT 'CREATE SEQUENCE ENGINE.SEQ_OB_EMAIL_QUEUE_ID INCREMENT BY 1 MINVALUE ' || MIN_VALUE || ' MAXVALUE ' || MAX_VALUE 
    || ' START WITH ' || LAST_NUMBER || ' NOORDER NOCYCLE' 
  INTO LV_SQL
  FROM DBA_SEQUENCES 
  WHERE SEQUENCE_OWNER = 'TAZDBA' AND SEQUENCE_NAME = 'SEQ_OB_EMAIL_QUEUE_ID';

  EXECUTE IMMEDIATE LV_SQL;
END;
/

DROP PUBLIC SYNONYM RERIX_MACHINE_ID_SEQ;

ALTER TABLE TAZDBA.OB_EMAIL_QUEUE RENAME TO OB_EMAIL_QUEUE_OLD;

CREATE TABLE ENGINE.OB_EMAIL_QUEUE
(
  OB_EMAIL_QUEUE_ID              NUMBER(20),
  OB_EMAIL_FROM_EMAIL_ADDR       VARCHAR2(60) NOT NULL,
  OB_EMAIL_FROM_NAME             VARCHAR2(60) NOT NULL,
  OB_EMAIL_TO_EMAIL_ADDR         VARCHAR2(60) NOT NULL,
  OB_EMAIL_TO_NAME               VARCHAR2(60) NOT NULL,
  OB_EMAIL_SUBJECT               VARCHAR2(100) NOT NULL,
  OB_EMAIL_MSG                   VARCHAR2(4000) NOT NULL,
  OB_EMAIL_SCHEDULED_SEND_TS     DATE,
  OB_EMAIL_LAST_SEND_ATTEMPT_TS  DATE,
  OB_EMAIL_TOT_SEND_ATTEMP_NUM   NUMBER(2),
  OB_EMAIL_SENT_SUCCESS_TS       DATE,
  CREATED_BY                     VARCHAR2(30) NOT NULL,
  CREATED_TS                     DATE           NOT NULL,
  LAST_UPDATED_BY                VARCHAR2(30) NOT NULL,
  LAST_UPDATED_TS                DATE           NOT NULL
)
TABLESPACE ENGINE_DATA
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            BUFFER_POOL      DEFAULT
           )
LOGGING
PARTITION BY RANGE (CREATED_TS)
(  
  PARTITION "2012-02-01" VALUES LESS THAN (TO_DATE(' 2012-03-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN')), 
  PARTITION "2012-03-01" VALUES LESS THAN (TO_DATE(' 2012-04-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN')),  
  PARTITION "2012-04-01" VALUES LESS THAN (TO_DATE(' 2012-05-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN')),
  PARTITION "2012-05-01" VALUES LESS THAN (TO_DATE(' 2012-06-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN')),
  PARTITION "2012-06-01" VALUES LESS THAN (TO_DATE(' 2012-07-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN')),
  PARTITION "2012-07-01" VALUES LESS THAN (TO_DATE(' 2012-08-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN')),
  PARTITION "2012-08-01" VALUES LESS THAN (TO_DATE(' 2012-09-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN')),
  PARTITION "2012-09-01" VALUES LESS THAN (TO_DATE(' 2012-10-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN')),
  PARTITION "2012-10-01" VALUES LESS THAN (TO_DATE(' 2012-11-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN')),
  PARTITION "2012-11-01" VALUES LESS THAN (TO_DATE(' 2012-12-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN')),
  PARTITION "2012-12-01" VALUES LESS THAN (TO_DATE(' 2013-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN')) 
);

CREATE UNIQUE INDEX ENGINE.PK_OB_EMAIL_QUEUE ON ENGINE.OB_EMAIL_QUEUE
(OB_EMAIL_QUEUE_ID)
LOGGING
TABLESPACE ENGINE_INDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            FREELISTS        4
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

CREATE INDEX ENGINE.IDX_0B_00001 ON ENGINE.OB_EMAIL_QUEUE
(OB_EMAIL_TOT_SEND_ATTEMP_NUM)
LOGGING
TABLESPACE ENGINE_INDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            FREELISTS        4
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

CREATE INDEX ENGINE.IDX_OB_00002 ON ENGINE.OB_EMAIL_QUEUE
(OB_EMAIL_SCHEDULED_SEND_TS)
LOGGING
TABLESPACE ENGINE_INDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            FREELISTS        4
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

CREATE INDEX ENGINE.IX_OB_EMAIL_QUEUE_TS ON ENGINE.OB_EMAIL_QUEUE
(CREATED_TS)
  TABLESPACE ENGINE_INDX
  PCTFREE    10
  INITRANS   2
  MAXTRANS   255
  STORAGE    (
              INITIAL          1M
              NEXT             1M
              MINEXTENTS       1
              MAXEXTENTS       UNLIMITED
              PCTINCREASE      0
	      FREELISTS        1
              FREELIST GROUPS  1
              BUFFER_POOL      DEFAULT
             )
LOGGING
LOCAL NOPARALLEL;

CREATE OR REPLACE TRIGGER ENGINE.TRBU_OB_EMAIL_QUEUE
   BEFORE UPDATE
   ON ENGINE.OB_EMAIL_QUEUE
   FOR EACH ROW
BEGIN
	SELECT 
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
        USER
    INTO 
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
        :NEW.last_updated_by
    FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER ENGINE.TRBI_OB_EMAIL_QUEUE
 BEFORE
  INSERT
 ON ENGINE.OB_EMAIL_QUEUE
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
Begin
    IF :new.ob_email_queue_id IS NULL THEN
      SELECT seq_ob_email_queue_id.nextval
        into :new.ob_email_queue_id
        FROM dual;
    END IF;

    IF :new.ob_email_scheduled_send_ts is null THEN
        :new.ob_email_scheduled_send_ts := sysdate;
    END IF;

    -- this is a minor fix for java, it's inserting date with no time
    IF(:new.ob_email_scheduled_send_ts = trunc(sysdate)) THEN
        :new.ob_email_scheduled_send_ts := sysdate;
    END IF;

	SELECT
           sysdate,
           user,
           sysdate,
           user,
           0
      into
           :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by,
           :new.OB_EMAIL_TOT_SEND_ATTEMP_NUM
      FROM dual;
End;
/

ALTER TABLE ENGINE.OB_EMAIL_QUEUE ADD (
  CONSTRAINT PK_OB_EMAIL_QUEUE
  PRIMARY KEY
  (OB_EMAIL_QUEUE_ID)
  USING INDEX ENGINE.PK_OB_EMAIL_QUEUE
  ENABLE NOVALIDATE);

insert /*+append nologging */ into ENGINE.OB_EMAIL_QUEUE select * from tazdba.OB_EMAIL_QUEUE_OLD;
commit;

GRANT DELETE ON ENGINE.OB_EMAIL_QUEUE TO USAT_ESUDSOPER_ROLE;
GRANT DELETE ON ENGINE.OB_EMAIL_QUEUE TO ESUDS_DEVELOPER;
GRANT DELETE ON ENGINE.OB_EMAIL_QUEUE TO USAT_SECURITY_ROLE;
GRANT INSERT ON ENGINE.OB_EMAIL_QUEUE TO USAT_ESUDSOPER_ROLE;
GRANT INSERT ON ENGINE.OB_EMAIL_QUEUE TO ESUDS_DEVELOPER;
GRANT INSERT ON ENGINE.OB_EMAIL_QUEUE TO USAT_SECURITY_ROLE;
GRANT SELECT ON ENGINE.OB_EMAIL_QUEUE TO ESUDS_DEVELOPER;
GRANT SELECT ON ENGINE.OB_EMAIL_QUEUE TO USAT_ESUDSOPER_ROLE;
GRANT SELECT ON ENGINE.OB_EMAIL_QUEUE TO USAT_SECURITY_ROLE;
GRANT UPDATE ON ENGINE.OB_EMAIL_QUEUE TO USAT_SECURITY_ROLE;
GRANT UPDATE ON ENGINE.OB_EMAIL_QUEUE TO USAT_ESUDSOPER_ROLE;
GRANT UPDATE ON ENGINE.OB_EMAIL_QUEUE TO ESUDS_DEVELOPER;
GRANT SELECT ON ENGINE.SEQ_OB_EMAIL_QUEUE_ID TO USAT_SECURITY_ROLE;
GRANT SELECT ON ENGINE.SEQ_OB_EMAIL_QUEUE_ID TO USAT_ESUDSOPER_ROLE;
GRANT SELECT ON ENGINE.SEQ_OB_EMAIL_QUEUE_ID TO ESUDS_DEVELOPER;
GRANT DELETE, INSERT, UPDATE ON ENGINE.OB_EMAIL_QUEUE TO USATECH_UPD_TRANS;
GRANT SELECT ON ENGINE.OB_EMAIL_QUEUE TO USAT_DEV_READ_ONLY;
GRANT DELETE, INSERT, SELECT, UPDATE, DEBUG ON ENGINE.OB_EMAIL_QUEUE TO USAT_ADMIN_ROLE;
GRANT DELETE, INSERT, SELECT, UPDATE ON ENGINE.OB_EMAIL_QUEUE TO USAT_APP_LAYER_ROLE;

CREATE OR REPLACE PUBLIC SYNONYM SEQ_OB_EMAIL_QUEUE_ID FOR ENGINE.SEQ_OB_EMAIL_QUEUE_ID;
CREATE OR REPLACE PUBLIC SYNONYM OB_EMAIL_QUEUE FOR ENGINE.OB_EMAIL_QUEUE;

DECLARE
	CURSOR cur IS 
		select d.device_id, d.device_name, d.device_type_id
		from device.vw_device_last_active d
		where d.device_type_id in (0, 1)
			and dbadmin.to_number_or_null(substr(d.device_name, 3)) > :max_device_number
		order by d.device_id;
		
	ll_default_content LONG;
	lv_default_content_g4 VARCHAR2(1024);
	lv_default_content_g5 VARCHAR2(1024);
	ll_content LONG;
	lv_content VARCHAR2(1024);	
BEGIN
	SELECT file_transfer_content
	INTO ll_default_content
	FROM device.file_transfer_oldnopart
	WHERE file_transfer_type_cd = 6 
		AND file_transfer_name = 'G4-DEFAULT-CFG';
		
	lv_default_content_g4 := UPPER(ll_default_content);

	SELECT file_transfer_content
	INTO ll_default_content
	FROM device.file_transfer_oldnopart
	WHERE file_transfer_type_cd = 6 
		AND file_transfer_name = 'G5-DEFAULT-CFG';
		
	lv_default_content_g5 := UPPER(ll_default_content);

	FOR rec_cur IN cur LOOP
		BEGIN
			ll_content := NULL;
		
			SELECT file_transfer_content
			INTO ll_content
			FROM (
				SELECT /*+INDEX(ft INX_FILE_XFER_TYPE_NAME_OLD)*/ file_transfer_content
				FROM device.file_transfer_oldnopart ft 
				WHERE file_transfer_name = rec_cur.device_name || '-CFG'
					AND file_content_length(rowid) = 1024
				ORDER BY file_transfer_type_cd, created_ts
			) WHERE ROWNUM = 1;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				NULL;
			WHEN VALUE_ERROR THEN
				NULL;
		END;
		
		IF ll_content IS NULL THEN
			IF rec_cur.device_type_id = 0 THEN 
				lv_content := lv_default_content_g4; 
			ELSE
				lv_content := lv_default_content_g5;
			END IF;
		ELSE
			lv_content := UPPER(ll_content);
		END IF;
			
		DELETE FROM device.device_setting
		WHERE device_id = rec_cur.device_id
			AND device_setting_parameter_cd IN (
				SELECT device_setting_parameter_cd
				FROM device.device_setting_parameter
				WHERE device_setting_ui_configurable = 'Y');
	
		MERGE INTO device.device_setting O
            USING (
              SELECT rec_cur.device_id DEVICE_ID,
                     device_setting_parameter_cd, field_offset * 2 + 1 hex_field_offset, field_size * 2 hex_field_size,
                     REPLACE(SUBSTR(lv_content, field_offset * 2 + 1, field_size * 2), ' ', '0') DEVICE_SETTING_VALUE
                from device.config_template_setting 
                where device_type_id = 0 and field_offset < 512) N
              ON (O.DEVICE_ID = N.DEVICE_ID AND O.device_setting_parameter_cd = N.device_setting_parameter_cd)
              WHEN MATCHED THEN
               UPDATE
                  SET O.DEVICE_SETTING_VALUE = N.DEVICE_SETTING_VALUE
              WHEN NOT MATCHED THEN
               INSERT (O.device_id, O.device_setting_parameter_cd, O.device_setting_value)
                VALUES(N.device_id, N.device_setting_parameter_cd, N.device_setting_value);			
		
		COMMIT;
	END LOOP;
END;
/

DECLARE
	CURSOR cur IS 
		select d.device_id, d.device_name
		from device.vw_device_last_active d
		where d.device_type_id = 6
			and dbadmin.to_number_or_null(substr(d.device_name, 3)) > :max_device_number
		order by d.device_id;
		
	ll_default_content LONG;
	lv_default_content VARCHAR2(114);
	ll_content LONG;
	lv_content VARCHAR2(114);	
BEGIN
	SELECT file_transfer_content
	INTO ll_default_content
	FROM device.file_transfer_oldnopart
	WHERE file_transfer_type_cd = 6 
		AND file_transfer_name = 'MEI-DEFAULT-CFG';
		
	lv_default_content := UPPER(ll_default_content);

	FOR rec_cur IN cur LOOP
		BEGIN
			ll_content := NULL;
		
			SELECT file_transfer_content
			INTO ll_content
			FROM (
				SELECT /*+INDEX(ft INX_FILE_XFER_TYPE_NAME_OLD)*/ file_transfer_content
				FROM device.file_transfer_oldnopart ft 
				WHERE file_transfer_name = rec_cur.device_name || '-CFG'
					AND file_content_length(rowid) = 114
				ORDER BY file_transfer_type_cd, created_ts
			) WHERE ROWNUM = 1;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				NULL;
			WHEN VALUE_ERROR THEN
				NULL;
		END;
		
		IF ll_content IS NULL THEN
			lv_content := lv_default_content;
		ELSE
			lv_content := UPPER(ll_content);
		END IF;
			
		DELETE FROM device.device_setting
		WHERE device_id = rec_cur.device_id
			AND device_setting_parameter_cd IN (
				SELECT device_setting_parameter_cd
				FROM device.device_setting_parameter
				WHERE device_setting_ui_configurable = 'Y');
	
		MERGE INTO device.device_setting O
            USING (
              SELECT rec_cur.device_id DEVICE_ID,
                     device_setting_parameter_cd, field_offset * 2 + 1 hex_field_offset, field_size * 2 hex_field_size,
                     REPLACE(SUBSTR(lv_content, field_offset * 2 + 1, field_size * 2), ' ', '0') DEVICE_SETTING_VALUE
                from device.config_template_setting 
                where device_type_id = 6 and field_offset < 57) N
              ON (O.DEVICE_ID = N.DEVICE_ID AND O.device_setting_parameter_cd = N.device_setting_parameter_cd)
              WHEN MATCHED THEN
               UPDATE
                  SET O.DEVICE_SETTING_VALUE = N.DEVICE_SETTING_VALUE
              WHEN NOT MATCHED THEN
               INSERT (O.device_id, O.device_setting_parameter_cd, O.device_setting_value)
                VALUES(N.device_id, N.device_setting_parameter_cd, N.device_setting_value);				
		
		COMMIT;
	END LOOP;
END;
/

ALTER TABLE DEVICE.DEVICE_TYPE ADD (DEFAULT_CONFIG_TEMPLATE_NAME VARCHAR2(200));

UPDATE DEVICE.DEVICE_TYPE SET DEFAULT_CONFIG_TEMPLATE_NAME = 'G4-DEFAULT-CFG' WHERE DEVICE_TYPE_ID = 0;
UPDATE DEVICE.DEVICE_TYPE SET DEFAULT_CONFIG_TEMPLATE_NAME = 'G5-DEFAULT-CFG' WHERE DEVICE_TYPE_ID = 1;
UPDATE DEVICE.DEVICE_TYPE SET DEFAULT_CONFIG_TEMPLATE_NAME = 'MEI-DEFAULT-CFG' WHERE DEVICE_TYPE_ID = 6;
UPDATE DEVICE.DEVICE_TYPE SET DEFAULT_CONFIG_TEMPLATE_NAME = 'KIOSK-DEFAULT-CFG' WHERE DEVICE_TYPE_ID = 11;
UPDATE DEVICE.DEVICE_TYPE SET DEFAULT_CONFIG_TEMPLATE_NAME = 'T2-DEFAULT-CFG' WHERE DEVICE_TYPE_ID = 12;
UPDATE DEVICE.DEVICE_TYPE SET DEFAULT_CONFIG_TEMPLATE_NAME = 'DEFAULT-CFG-13-' WHERE DEVICE_TYPE_ID = 13;
COMMIT;


CREATE TABLE DEVICE.DEVICE_SETTING_BACKUP
(
    DEVICE_ID                   NUMBER(20,0) NOT NULL,
    DEVICE_SETTING_PARAMETER_CD VARCHAR2(60) NOT NULL,
    DEVICE_SETTING_VALUE        VARCHAR2(200),
    CREATED_BY                  VARCHAR2(30) NOT NULL,
    CREATED_TS 					DATE NOT NULL,
    LAST_UPDATED_BY 			VARCHAR2(30) NOT NULL,
    LAST_UPDATED_TS 			DATE NOT NULL,
    FILE_ORDER 					NUMBER(20,0),
	CONSTRAINT PK_DEVICE_SETTING_BACKUP PRIMARY KEY(DEVICE_ID, DEVICE_SETTING_PARAMETER_CD),
	CONSTRAINT FK_DEVICE_SETTING_BKP_DEVICE FOREIGN KEY (DEVICE_ID) REFERENCES DEVICE.DEVICE (DEVICE_ID),
	CONSTRAINT FK_DEVICE_SETTING_BKP_PARAM_CD FOREIGN KEY (DEVICE_SETTING_PARAMETER_CD) REFERENCES DEVICE.DEVICE_SETTING_PARAMETER (DEVICE_SETTING_PARAMETER_CD)
) TABLESPACE DEVICE_DATA;

CREATE INDEX DEVICE.IDX_DEVICE_SETING_BKP_VALUE ON DEVICE.DEVICE_SETTING_BACKUP(DEVICE_SETTING_VALUE) TABLESPACE DEVICE_INDX ONLINE;
CREATE INDEX DEVICE.IDX_DEVICE_SET_BKP_PARAM_VALUE ON DEVICE.DEVICE_SETTING_BACKUP(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_VALUE) TABLESPACE DEVICE_INDX ONLINE;

CREATE OR REPLACE TRIGGER DEVICE.TRBI_DEVICE_SETTING_BACKUP BEFORE INSERT ON DEVICE.DEVICE_SETTING_BACKUP
  FOR EACH ROW
BEGIN
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER DEVICE.TRBU_DEVICE_SETTING_BACKUP BEFORE UPDATE ON DEVICE.DEVICE_SETTING_BACKUP
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

GRANT SELECT, INSERT, UPDATE, DELETE ON DEVICE.DEVICE_SETTING_BACKUP TO USAT_DMS_ROLE;
GRANT SELECT ON DEVICE.DEVICE_SETTING_BACKUP TO USAT_DEV_READ_ONLY;
GRANT SELECT, INSERT, UPDATE, DELETE ON DEVICE.DEVICE_SETTING_BACKUP TO USATECH_UPD_TRANS;

CREATE OR REPLACE PUBLIC SYNONYM DEVICE_SETTING_BACKUP FOR DEVICE.DEVICE_SETTING_BACKUP;


CREATE TABLE DEVICE.CONFIG_TEMPLATE_TYPE
(
  CONFIG_TEMPLATE_TYPE_ID NUMBER(20, 0) NOT NULL,
  CONFIG_TEMPLATE_TYPE_NAME VARCHAR2(200) NOT NULL,  
  CREATED_BY	VARCHAR2(30) NOT NULL,
  CREATED_TS	DATE NOT NULL,
  LAST_UPDATED_BY	VARCHAR2(30) NOT NULL,
  LAST_UPDATED_TS	DATE NOT NULL, 
  CONSTRAINT PK_CONFIG_TEMPLATE_TYPE PRIMARY KEY(CONFIG_TEMPLATE_TYPE_ID)  
) TABLESPACE DEVICE_DATA;

CREATE OR REPLACE TRIGGER DEVICE.TRBI_CONFIG_TEMPLATE_TYPE BEFORE INSERT ON DEVICE.CONFIG_TEMPLATE_TYPE
  FOR EACH ROW
BEGIN
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER DEVICE.TRBU_CONFIG_TEMPLATE_TYPE BEFORE UPDATE ON DEVICE.CONFIG_TEMPLATE_TYPE
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

GRANT SELECT, INSERT, UPDATE ON DEVICE.CONFIG_TEMPLATE_TYPE TO USAT_DMS_ROLE;
GRANT SELECT ON DEVICE.CONFIG_TEMPLATE_TYPE TO PSS, USAT_APP_LAYER_ROLE, USAT_DEV_READ_ONLY;
GRANT SELECT, INSERT, UPDATE, DELETE ON DEVICE.CONFIG_TEMPLATE_TYPE TO USATECH_UPD_TRANS;

INSERT INTO DEVICE.CONFIG_TEMPLATE_TYPE(CONFIG_TEMPLATE_TYPE_ID, CONFIG_TEMPLATE_TYPE_NAME) VALUES(1, 'Default');
INSERT INTO DEVICE.CONFIG_TEMPLATE_TYPE(CONFIG_TEMPLATE_TYPE_ID, CONFIG_TEMPLATE_TYPE_NAME) VALUES(2, 'Custom');
INSERT INTO DEVICE.CONFIG_TEMPLATE_TYPE(CONFIG_TEMPLATE_TYPE_ID, CONFIG_TEMPLATE_TYPE_NAME) VALUES(3, 'Metadata');
COMMIT;

CREATE OR REPLACE PUBLIC SYNONYM CONFIG_TEMPLATE_TYPE FOR DEVICE.CONFIG_TEMPLATE_TYPE;


CREATE SEQUENCE DEVICE.SEQ_CONFIG_TEMPLATE_ID INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH 1 NOCACHE;
GRANT SELECT ON DEVICE.SEQ_CONFIG_TEMPLATE_ID TO USAT_DMS_ROLE;

CREATE TABLE DEVICE.CONFIG_TEMPLATE
(
  CONFIG_TEMPLATE_ID NUMBER(20, 0) NOT NULL,
  CONFIG_TEMPLATE_NAME VARCHAR2(200) NOT NULL,
  CONFIG_TEMPLATE_TYPE_ID NUMBER(20, 0) NOT NULL,
  DEVICE_TYPE_ID NUMBER(20, 0) NOT NULL,
  CREATED_BY	VARCHAR2(30) NOT NULL,
  CREATED_TS	DATE NOT NULL,
  LAST_UPDATED_BY	VARCHAR2(30) NOT NULL,
  LAST_UPDATED_TS	DATE NOT NULL, 
  PROPERTY_LIST_VERSION NUMBER(20, 0) DEFAULT 0 NOT NULL,
  CONSTRAINT PK_CONFIG_TEMPLATE PRIMARY KEY(CONFIG_TEMPLATE_ID),
  CONSTRAINT FK_CONFIG_TEMPLATE_CT_TYPE_ID FOREIGN KEY(CONFIG_TEMPLATE_TYPE_ID) REFERENCES DEVICE.CONFIG_TEMPLATE_TYPE(CONFIG_TEMPLATE_TYPE_ID),
  CONSTRAINT FK_CONFIG_TEMPLATE_DT_ID FOREIGN KEY(DEVICE_TYPE_ID) REFERENCES DEVICE.DEVICE_TYPE(DEVICE_TYPE_ID)
) TABLESPACE DEVICE_DATA;

CREATE UNIQUE INDEX DEVICE.UDX_CONFIG_TEMPLATE_CT_NAME ON DEVICE.CONFIG_TEMPLATE(CONFIG_TEMPLATE_NAME) TABLESPACE DEVICE_INDX;
CREATE UNIQUE INDEX DEVICE.UDX_CFG_TMPL_DEV_TYPE_TYPE_PLV ON DEVICE.CONFIG_TEMPLATE(DEVICE_TYPE_ID, CONFIG_TEMPLATE_TYPE_ID, DECODE(CONFIG_TEMPLATE_TYPE_ID, 2, CONFIG_TEMPLATE_ID, PROPERTY_LIST_VERSION)) TABLESPACE DEVICE_INDX;

CREATE OR REPLACE TRIGGER DEVICE.TRBI_CONFIG_TEMPLATE BEFORE INSERT ON DEVICE.CONFIG_TEMPLATE
  FOR EACH ROW
BEGIN
	IF :NEW.CONFIG_TEMPLATE_ID IS NULL THEN
		SELECT SEQ_CONFIG_TEMPLATE_ID.NEXTVAL
        INTO :NEW.CONFIG_TEMPLATE_ID
        FROM DUAL;
    END IF;

	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER DEVICE.TRBU_CONFIG_TEMPLATE BEFORE UPDATE ON DEVICE.CONFIG_TEMPLATE
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

GRANT SELECT, INSERT, UPDATE ON DEVICE.CONFIG_TEMPLATE TO USAT_DMS_ROLE;
GRANT SELECT ON DEVICE.CONFIG_TEMPLATE TO PSS, USAT_APP_LAYER_ROLE, USAT_DEV_READ_ONLY;
GRANT SELECT, INSERT, UPDATE, DELETE ON DEVICE.CONFIG_TEMPLATE TO USATECH_UPD_TRANS;

CREATE OR REPLACE PUBLIC SYNONYM CONFIG_TEMPLATE FOR DEVICE.CONFIG_TEMPLATE;

ALTER TABLE DEVICE.CONFIG_TEMPLATE_SETTING ADD (CONFIG_TEMPLATE_ID NUMBER(20, 0));
CREATE OR REPLACE PUBLIC SYNONYM CONFIG_TEMPLATE_SETTING FOR DEVICE.CONFIG_TEMPLATE_SETTING;

DECLARE
	CURSOR cur IS 
		select distinct ft.file_transfer_id, ft.file_transfer_type_cd, ft.file_transfer_name, 
			file_content_length(ft.rowid) file_content_length
		from device.config_template_setting cts
		join device.file_transfer_oldnopart ft on cts.file_transfer_id = ft.file_transfer_id
		where cts.config_template_id is null
			and not exists (select 1 from device.config_template where config_template_name = ft.file_transfer_name)
		order by ft.file_transfer_id;
		
	ln_config_template_type_id CONFIG_TEMPLATE.CONFIG_TEMPLATE_TYPE_ID%TYPE;
	ln_property_list_version CONFIG_TEMPLATE.PROPERTY_LIST_VERSION%TYPE;
	ln_device_type_id DEVICE_TYPE.DEVICE_TYPE_ID%TYPE;
BEGIN
	FOR rec_cur IN cur LOOP
		ln_property_list_version := 0;
		IF rec_cur.file_transfer_type_cd IN (1, 19) AND rec_cur.file_transfer_name = 'GX-GENERIC-MAP' THEN
			ln_config_template_type_id := 3;
			ln_device_type_id := 0;
		ELSIF rec_cur.file_transfer_type_cd IN (1, 19) AND rec_cur.file_transfer_name = 'MEI-GENERIC-MAP' THEN
			ln_config_template_type_id := 3;
			ln_device_type_id := 6;
		ELSIF rec_cur.file_transfer_type_cd IN (1, 19) AND rec_cur.file_transfer_name = 'CFG-METADATA-13' THEN
			ln_config_template_type_id := 3;
			ln_device_type_id := 13;
		ELSIF rec_cur.file_transfer_type_cd IN (1, 19) AND rec_cur.file_transfer_name = 'CFG-METADATA-11' THEN
			ln_config_template_type_id := 3;
			ln_device_type_id := 11;
		ELSIF rec_cur.file_transfer_type_cd = 6 THEN
			IF rec_cur.file_transfer_name = 'G4-DEFAULT-CFG' THEN
				ln_config_template_type_id := 1;
				ln_device_type_id := 0;
			ELSIF rec_cur.file_transfer_name = 'G5-DEFAULT-CFG' THEN
				ln_config_template_type_id := 1;
				ln_device_type_id := 1;
			ELSIF rec_cur.file_transfer_name = 'MEI-DEFAULT-CFG' THEN			
				ln_config_template_type_id := 1;
				ln_device_type_id := 6;
			ELSE
				ln_config_template_type_id := 2;
				IF rec_cur.file_content_length < 1024 THEN 
					ln_device_type_id := 6;
				ELSE
					ln_device_type_id := 0;
				END IF;
			END IF;
		ELSIF rec_cur.file_transfer_type_cd = 15 THEN
			IF rec_cur.file_transfer_name = 'KIOSK-DEFAULT-CFG' THEN
				ln_config_template_type_id := 1;
				ln_device_type_id := 11;
			ELSIF rec_cur.file_transfer_name = 'T2-DEFAULT-CFG' THEN
				ln_config_template_type_id := 1;
				ln_device_type_id := 12;
			ELSE
				ln_config_template_type_id := 2;
				ln_device_type_id := 11;
			END IF;
		ELSIF rec_cur.file_transfer_type_cd = 16 THEN
			ln_config_template_type_id := 2;
			ln_device_type_id := 11;
		ELSIF rec_cur.file_transfer_type_cd = 22 AND REGEXP_LIKE(rec_cur.file_transfer_name, '^DEFAULT-CFG-13-[0-9][0-9]*$') THEN
			ln_config_template_type_id := 1;
			ln_device_type_id := 13;
			ln_property_list_version := NVL(TO_NUMBER_OR_NULL(SUBSTR(rec_cur.file_transfer_name, INSTR(rec_cur.file_transfer_name, '-', 1, 3) + 1)), 0);
		ELSIF rec_cur.file_transfer_type_cd = 23 THEN
			ln_config_template_type_id := 2;
			ln_device_type_id := 13;
		ELSE
			ln_config_template_type_id := 2;
			ln_device_type_id := 13;
		END IF;		
		
		INSERT INTO DEVICE.CONFIG_TEMPLATE(CONFIG_TEMPLATE_NAME, CONFIG_TEMPLATE_TYPE_ID, DEVICE_TYPE_ID, PROPERTY_LIST_VERSION)
		SELECT rec_cur.file_transfer_name, ln_config_template_type_id, ln_device_type_id, ln_property_list_version FROM DUAL
		WHERE NOT EXISTS (SELECT 1 FROM DEVICE.CONFIG_TEMPLATE WHERE CONFIG_TEMPLATE_NAME = rec_cur.file_transfer_name);
		
		IF SQL%ROWCOUNT > 0 THEN
			UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
			SET CONFIG_TEMPLATE_ID = (SELECT CONFIG_TEMPLATE_ID FROM DEVICE.CONFIG_TEMPLATE WHERE CONFIG_TEMPLATE_NAME = rec_cur.file_transfer_name)
			WHERE FILE_TRANSFER_ID = rec_cur.file_transfer_id;
			
			COMMIT;
		END IF;
	END LOOP;
END;
/

ALTER TABLE DEVICE.CONFIG_TEMPLATE_SETTING MODIFY (CONFIG_TEMPLATE_ID NUMBER(20, 0) NOT NULL);
ALTER TABLE DEVICE.CONFIG_TEMPLATE_SETTING ADD CONSTRAINT FK_CFG_TMPL_CONFIG_TEMPLATE_ID FOREIGN KEY(CONFIG_TEMPLATE_ID) REFERENCES DEVICE.CONFIG_TEMPLATE(CONFIG_TEMPLATE_ID);
ALTER TABLE DEVICE.CONFIG_TEMPLATE_SETTING DROP CONSTRAINT PK_CONFIG_TEMPLATE_SETTING;
ALTER TABLE DEVICE.CONFIG_TEMPLATE_SETTING ADD CONSTRAINT PK_CONFIG_TEMPLATE_SETTING PRIMARY KEY (CONFIG_TEMPLATE_ID, DEVICE_SETTING_PARAMETER_CD);
ALTER TABLE DEVICE.CONFIG_TEMPLATE_SETTING DROP (FILE_TRANSFER_ID);


DECLARE
	CURSOR cur IS 
		select ft.file_transfer_id, ft.file_transfer_name
		from device.file_transfer_oldnopart ft
		where ft.file_transfer_type_cd = 6 
			and not exists (
				select 1 from device.config_template where config_template_name = ft.file_transfer_name
			) and file_content_length(ft.rowid) = 1024
		order by ft.file_transfer_name;
	
	CURSOR cur_map IS
		select device_setting_parameter_cd, field_offset * 2 + 1 hex_field_offset, field_size * 2 hex_field_size
		from device.config_template_setting 
		where device_type_id = 0 and field_offset < 512 
		order by field_offset;
	
	ll_content LONG;
	lv_content VARCHAR2(1024);	
	ln_config_template_id CONFIG_TEMPLATE.CONFIG_TEMPLATE_ID%TYPE;
	ln_exists NUMBER;
	
	PROCEDURE SP_UPSERT_CFG_TEMPLATE_SETTING(
		pn_config_template_id CONFIG_TEMPLATE_SETTING.CONFIG_TEMPLATE_ID%TYPE,
		pv_device_setting_parameter_cd CONFIG_TEMPLATE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_config_template_setting_val CONFIG_TEMPLATE_SETTING.CONFIG_TEMPLATE_SETTING_VALUE%TYPE,
		pn_exists OUT NUMBER
	) 
	IS
		ln_count NUMBER;
	BEGIN
		SELECT COUNT(1) INTO ln_count
		FROM device.device_setting_parameter
		WHERE device_setting_parameter_cd = pv_device_setting_parameter_cd;
			
		IF ln_count = 0 THEN
			BEGIN
				INSERT INTO device.device_setting_parameter(device_setting_parameter_cd)
				VALUES(pv_device_setting_parameter_cd);
			EXCEPTION
				WHEN DUP_VAL_ON_INDEX THEN
					NULL;
			END;	
		END IF;
		
		UPDATE device.config_template_setting
		SET config_template_setting_value = pv_config_template_setting_val
		WHERE config_template_id = pn_config_template_id
			AND device_setting_parameter_cd = pv_device_setting_parameter_cd;
		
		IF SQL%NOTFOUND THEN
			pn_exists := 0;
			BEGIN
				INSERT INTO device.config_template_setting(config_template_id, device_setting_parameter_cd, config_template_setting_value)
				VALUES(pn_config_template_id, pv_device_setting_parameter_cd, pv_config_template_setting_val);
			EXCEPTION
				WHEN DUP_VAL_ON_INDEX THEN
					SP_UPSERT_CFG_TEMPLATE_SETTING(pn_config_template_id, pv_device_setting_parameter_cd, pv_config_template_setting_val, pn_exists);
			END;
		ELSE
			pn_exists := 1;
		END IF;
	END;
BEGIN
	FOR rec_cur IN cur LOOP
		ll_content := NULL;
		
		BEGIN
			SELECT file_transfer_content
			INTO ll_content
			FROM device.file_transfer_oldnopart
			WHERE file_transfer_id = rec_cur.file_transfer_id;
		EXCEPTION
			WHEN VALUE_ERROR THEN
				NULL;
		END;
		
		IF ll_content IS NOT NULL THEN
			lv_content := UPPER(ll_content);
			
			INSERT INTO DEVICE.CONFIG_TEMPLATE(CONFIG_TEMPLATE_NAME, CONFIG_TEMPLATE_TYPE_ID, DEVICE_TYPE_ID)
			SELECT rec_cur.file_transfer_name, 2, 0 FROM DUAL
			WHERE NOT EXISTS (SELECT 1 FROM DEVICE.CONFIG_TEMPLATE WHERE CONFIG_TEMPLATE_NAME = rec_cur.file_transfer_name);
		
			IF SQL%ROWCOUNT > 0 THEN				
				SELECT CONFIG_TEMPLATE_ID 
				INTO ln_config_template_id
				FROM DEVICE.CONFIG_TEMPLATE 
				WHERE CONFIG_TEMPLATE_NAME = rec_cur.file_transfer_name;			
			
				FOR rec_cur_map IN cur_map LOOP
					SP_UPSERT_CFG_TEMPLATE_SETTING(ln_config_template_id, rec_cur_map.device_setting_parameter_cd, 
						REPLACE(SUBSTR(lv_content, rec_cur_map.hex_field_offset, rec_cur_map.hex_field_size), ' ', '0'), ln_exists);
				END LOOP;
				
				COMMIT;
			END IF;
		END IF;
	END LOOP;
END;
/

DECLARE
	CURSOR cur IS 
		select ft.file_transfer_id, ft.file_transfer_name
		from device.file_transfer_oldnopart ft
		where ft.file_transfer_type_cd = 6 
			and not exists (
				select 1 from device.config_template where config_template_name = ft.file_transfer_name
			) and file_content_length(ft.rowid) = 114
		order by ft.file_transfer_name;
	
	CURSOR cur_map IS
		select device_setting_parameter_cd, field_offset * 2 + 1 hex_field_offset, field_size * 2 hex_field_size
		from device.config_template_setting 
		where device_type_id = 6 and field_offset < 57 
		order by field_offset;
	
	ll_content LONG;
	lv_content VARCHAR2(114);	
	ln_config_template_id CONFIG_TEMPLATE.CONFIG_TEMPLATE_ID%TYPE;
	ln_exists NUMBER;
	
	PROCEDURE SP_UPSERT_CFG_TEMPLATE_SETTING(
		pn_config_template_id CONFIG_TEMPLATE_SETTING.CONFIG_TEMPLATE_ID%TYPE,
		pv_device_setting_parameter_cd CONFIG_TEMPLATE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_config_template_setting_val CONFIG_TEMPLATE_SETTING.CONFIG_TEMPLATE_SETTING_VALUE%TYPE,
		pn_exists OUT NUMBER
	) 
	IS
		ln_count NUMBER;
	BEGIN
		SELECT COUNT(1) INTO ln_count
		FROM device.device_setting_parameter
		WHERE device_setting_parameter_cd = pv_device_setting_parameter_cd;
			
		IF ln_count = 0 THEN
			BEGIN
				INSERT INTO device.device_setting_parameter(device_setting_parameter_cd)
				VALUES(pv_device_setting_parameter_cd);
			EXCEPTION
				WHEN DUP_VAL_ON_INDEX THEN
					NULL;
			END;	
		END IF;
		
		UPDATE device.config_template_setting
		SET config_template_setting_value = pv_config_template_setting_val
		WHERE config_template_id = pn_config_template_id
			AND device_setting_parameter_cd = pv_device_setting_parameter_cd;
		
		IF SQL%NOTFOUND THEN
			pn_exists := 0;
			BEGIN
				INSERT INTO device.config_template_setting(config_template_id, device_setting_parameter_cd, config_template_setting_value)
				VALUES(pn_config_template_id, pv_device_setting_parameter_cd, pv_config_template_setting_val);
			EXCEPTION
				WHEN DUP_VAL_ON_INDEX THEN
					SP_UPSERT_CFG_TEMPLATE_SETTING(pn_config_template_id, pv_device_setting_parameter_cd, pv_config_template_setting_val, pn_exists);
			END;
		ELSE
			pn_exists := 1;
		END IF;
	END;
BEGIN
	FOR rec_cur IN cur LOOP
		ll_content := NULL;
		
		BEGIN
			SELECT file_transfer_content
			INTO ll_content
			FROM device.file_transfer_oldnopart
			WHERE file_transfer_id = rec_cur.file_transfer_id;
		EXCEPTION
			WHEN VALUE_ERROR THEN
				NULL;
		END;
		
		IF ll_content IS NOT NULL THEN
			lv_content := UPPER(ll_content);
			
			INSERT INTO DEVICE.CONFIG_TEMPLATE(CONFIG_TEMPLATE_NAME, CONFIG_TEMPLATE_TYPE_ID, DEVICE_TYPE_ID)
			SELECT rec_cur.file_transfer_name, 2, 6 FROM DUAL
			WHERE NOT EXISTS (SELECT 1 FROM DEVICE.CONFIG_TEMPLATE WHERE CONFIG_TEMPLATE_NAME = rec_cur.file_transfer_name);
		
			IF SQL%ROWCOUNT > 0 THEN			
				SELECT CONFIG_TEMPLATE_ID 
				INTO ln_config_template_id
				FROM DEVICE.CONFIG_TEMPLATE 
				WHERE CONFIG_TEMPLATE_NAME = rec_cur.file_transfer_name;			
			
				FOR rec_cur_map IN cur_map LOOP
					SP_UPSERT_CFG_TEMPLATE_SETTING(ln_config_template_id, rec_cur_map.device_setting_parameter_cd, 
						REPLACE(SUBSTR(lv_content, rec_cur_map.hex_field_offset, rec_cur_map.hex_field_size), ' ', '0'), ln_exists);
				END LOOP;
				
				COMMIT;
			END IF;
		END IF;
	END LOOP;
END;
/

