WHENEVER SQLERROR EXIT FAILURE COMMIT;

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/FRONT/InitialPostalSetup.sql?rev=HEAD
CREATE TABLESPACE FRONT_DATA DATAFILE '+ECC_DATA_01(FLASHFILE)/eccdb/front_data01.dbf' SIZE 100M AUTOEXTEND ON NEXT 128M MAXSIZE 12288M LOGGING EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;
CREATE TABLESPACE FRONT_INDX DATAFILE '+ECC_INDX_01(FLASHFILE)/eccdb/front_indx01.dbf' SIZE 100M AUTOEXTEND ON NEXT 128M MAXSIZE 12288M LOGGING EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;

CREATE USER FRONT
IDENTIFIED BY FRONT 
DEFAULT TABLESPACE FRONT_DATA
PROFILE USAT_APPS
TEMPORARY TABLESPACE TEMPTS1;
GRANT RESOURCE TO FRONT;
ALTER USER FRONT DEFAULT ROLE ALL;
ALTER USER FRONT QUOTA UNLIMITED ON FRONT_INDX;
ALTER USER FRONT QUOTA UNLIMITED ON FRONT_DATA;

GRANT REFERENCES ON CORP.BUSINESS_UNIT TO FRONT;
GRANT REFERENCES ON CORP.CURRENCY TO FRONT;
GRANT REFERENCES ON CORP.CUSTOMER TO FRONT;
GRANT REFERENCES ON CORP.CUSTOMER_BANK TO FRONT;
GRANT REFERENCES ON CORP.PAYMENT_SCHEDULE TO FRONT;
GRANT REFERENCES ON CORP.STATE TO FRONT;
GRANT REFERENCES ON REPORT.DEVICE_TYPE TO FRONT;
GRANT REFERENCES ON REPORT.MACHINE TO FRONT;
GRANT REFERENCES ON REPORT.PRODUCT_TYPE TO FRONT;

CREATE SEQUENCE FRONT.SEQ_TIME_ZONE_ID START WITH 200;
CREATE SEQUENCE FRONT.SEQ_POSTAL_ID;

CREATE TABLE FRONT.TIME_ZONE(
        TIME_ZONE_ID       NUMBER NOT NULL,
        TIME_ZONE_NAME     VARCHAR2(90) NOT NULL,
        UTC_OFFSET_MS      NUMBER(8, 0) NOT NULL,
        DAYLIGHT_OFFSET_MS NUMBER(8, 0) NOT NULL,
        STANDARD_ABBR      VARCHAR2(10),
        DAYLIGHT_ABBR      VARCHAR2(10),
        EXAMPLE            VARCHAR2(4000),
        CREATED_BY         VARCHAR2(30) NOT NULL,
        CREATED_TS DATE NOT NULL,
        LAST_UPDATED_BY VARCHAR2(30) NOT NULL,
        LAST_UPDATED_TS DATE NOT NULL,
        CONSTRAINT PK_TIME_ZONE PRIMARY KEY(TIME_ZONE_ID) USING INDEX TABLESPACE FRONT_DATA
) TABLESPACE FRONT_DATA ;

CREATE UNIQUE INDEX FRONT.AK_TIME_ZONE ON FRONT.TIME_ZONE(TIME_ZONE_NAME) TABLESPACE FRONT_INDX ;

CREATE OR REPLACE TRIGGER FRONT.TRBI_TIME_ZONE 
    BEFORE INSERT ON FRONT.TIME_ZONE FOR EACH ROW 
BEGIN 
    IF :NEW.TIME_ZONE_ID IS NULL THEN
        SELECT SEQ_TIME_ZONE_ID.NEXTVAL INTO :NEW.TIME_ZONE_ID FROM DUAL;
    END IF;
    SELECT SYSDATE,
        USER,
        SYSDATE,
        USER
    INTO :NEW.CREATED_TS,
        :NEW.CREATED_BY,
        :NEW.LAST_UPDATED_TS,
        :NEW.LAST_UPDATED_BY
    FROM DUAL;
END;
/
CREATE OR REPLACE TRIGGER FRONT.TRBU_TIME_ZONE 
    BEFORE UPDATE ON FRONT.TIME_ZONE FOR EACH ROW 
BEGIN
    SELECT :OLD.CREATED_TS,
        :OLD.CREATED_BY,
        SYSDATE,
        USER
    INTO :NEW.CREATED_TS,
        :NEW.CREATED_BY,
        :NEW.LAST_UPDATED_TS,
        :NEW.LAST_UPDATED_BY
    FROM DUAL;
END;
/

CREATE TABLE FRONT.POSTAL (
        POSTAL_ID     NUMBER(22, 0),
        POSTAL_CD     VARCHAR2(10) NOT NULL,
        CITY          VARCHAR2(100) NOT NULL,
        STATE_ID      NUMBER(8, 0) NOT NULL,
        TIME_ZONE_ID  NUMBER(10, 0),
        PHONE_AREA_CD VARCHAR2(10),
        LONGITUDE     NUMBER(12, 9),
        LATITUDE      NUMBER(12, 9),
        CREATED_BY    VARCHAR2(30) NOT NULL,
        CREATED_TS DATE NOT NULL,
        LAST_UPDATED_BY VARCHAR2(30) NOT NULL,
        LAST_UPDATED_TS DATE NOT NULL,
        CONSTRAINT PK_POSTAL PRIMARY KEY(POSTAL_ID) USING INDEX TABLESPACE FRONT_DATA,
        CONSTRAINT FK_POSTAL_STATE_ID FOREIGN KEY(STATE_ID) REFERENCES CORP.STATE(STATE_ID),
        CONSTRAINT FK_POSTAL_TIME_ZONE_ID FOREIGN KEY(TIME_ZONE_ID) REFERENCES FRONT.TIME_ZONE(TIME_ZONE_ID) 
) TABLESPACE FRONT_DATA ;
CREATE INDEX FRONT.IX_POSTAL_POSTAL_CD ON FRONT.POSTAL(POSTAL_CD) TABLESPACE FRONT_INDX ;
CREATE INDEX FRONT.IX_POSTAL_STATE_ID ON FRONT.POSTAL(STATE_ID) TABLESPACE FRONT_INDX ;
CREATE UNIQUE INDEX FRONT.UIX_POSTAL_POSTAL_CD_STATE_ID ON FRONT.POSTAL(POSTAL_CD, STATE_ID) TABLESPACE FRONT_INDX ;

CREATE OR REPLACE TRIGGER FRONT.TRBI_POSTAL 
    BEFORE INSERT ON FRONT.POSTAL FOR EACH ROW 
BEGIN 
    IF :NEW.POSTAL_ID IS NULL THEN
        SELECT SEQ_POSTAL_ID.NEXTVAL INTO :NEW.POSTAL_ID FROM DUAL;
    END IF;
    SELECT SYSDATE,
        USER,
        SYSDATE,
        USER
    INTO :NEW.CREATED_TS,
        :NEW.CREATED_BY,
        :NEW.LAST_UPDATED_TS,
        :NEW.LAST_UPDATED_BY
    FROM DUAL;
END;
/
CREATE OR REPLACE TRIGGER FRONT.TRBU_POSTAL 
    BEFORE UPDATE ON FRONT.POSTAL FOR EACH ROW 
BEGIN
    SELECT :OLD.CREATED_TS,
        :OLD.CREATED_BY,
        SYSDATE,
        USER
    INTO :NEW.CREATED_TS,
        :NEW.CREATED_BY,
        :NEW.LAST_UPDATED_TS,
        :NEW.LAST_UPDATED_BY
    FROM DUAL;
END;
/
GRANT SELECT ON FRONT.TIME_ZONE TO USAT_DEV_READ_ONLY;
GRANT SELECT ON FRONT.POSTAL TO USAT_DEV_READ_ONLY;
GRANT SELECT ON FRONT.POSTAL TO USALIVE_APP_ROLE;
GRANT SELECT ON FRONT.POSTAL TO REPORT;

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/DMS/DMS-1.0.4-USARDB-AlterTables.sql?rev=HEAD
SET DEFINE OFF;

GRANT REFERENCES ON CORP.STATE TO REPORT;
GRANT REFERENCES ON CORP.COUNTRY TO REPORT;
GRANT SELECT ON CORP.CUSTOMER_BANK_SEQ TO REPORT;
GRANT SELECT ON FRONT.POSTAL TO REPORT;
GRANT SELECT ON FRONT.POSTAL TO USALIVE_APP_ROLE;

ALTER TABLE REPORT.TERMINAL_ADDR ADD(COUNTRY_CD VARCHAR2(2), CONSTRAINT FK_TA_COUNTRY_CD FOREIGN KEY(COUNTRY_CD) REFERENCES CORP.COUNTRY(COUNTRY_CD));
ALTER TABLE CORP.CUSTOMER_BANK ADD(BANK_COUNTRY_CD VARCHAR2(2), CONSTRAINT FK_CB_BANK_COUNTRY_CD FOREIGN KEY(BANK_COUNTRY_CD) REFERENCES CORP.COUNTRY(COUNTRY_CD));
ALTER TABLE CORP.CUSTOMER_ADDR ADD(COUNTRY_CD VARCHAR2(2), CONSTRAINT FK_CA_COUNTRY_CD FOREIGN KEY(COUNTRY_CD) REFERENCES CORP.COUNTRY(COUNTRY_CD));
ALTER TABLE CORP.COUNTRY ADD(POSTAL_REGEX VARCHAR2(100), POSTAL_MASK VARCHAR2(100), ALLOW_TERMINALS CHAR(1) DEFAULT 'N' NOT NULL, ALLOW_CUSTOMERS CHAR(1) DEFAULT 'N' NOT NULL, ALLOW_BANKS CHAR(1) DEFAULT 'N' NOT NULL);

UPDATE CORP.COUNTRY SET COUNTRY_NAME = INITCAP(COUNTRY_NAME)
 WHERE COUNTRY_CD != 'US';
UPDATE CORP.COUNTRY SET COUNTRY_NAME = 'USA', POSTAL_REGEX = '^(\d{5})(?:-(?:\d{4})?)?$', POSTAL_MASK = '99999-9999', ALLOW_TERMINALS = 'Y', ALLOW_CUSTOMERS = 'Y', ALLOW_BANKS = 'Y'
 WHERE COUNTRY_CD = 'US';
UPDATE CORP.COUNTRY SET POSTAL_REGEX = '^([A-Z][0-9][A-Z]) ([0-9][A-Z][0-9])$', POSTAL_MASK = 'U9U 9U9', ALLOW_TERMINALS = 'Y', ALLOW_CUSTOMERS = 'Y', ALLOW_BANKS = 'Y'
 WHERE COUNTRY_CD = 'CA'; 
UPDATE CORP.COUNTRY SET POSTAL_REGEX = '^(\d{5})$', POSTAL_MASK = '99999', ALLOW_TERMINALS = 'Y'
 WHERE COUNTRY_CD = 'GU';
UPDATE CORP.COUNTRY SET POSTAL_REGEX = '^([A-Z]{2}) (\d{2})$', POSTAL_MASK = 'UU 99', ALLOW_TERMINALS = 'Y'
 WHERE COUNTRY_CD = 'BM';
 
UPDATE REPORT.TERMINAL_ADDR
   SET COUNTRY_CD = CASE WHEN UPPER(STATE) LIKE '% - CANADA' THEN 'CA' ELSE 'US' END,
       STATE = CASE WHEN TRIM(UPPER(STATE)) LIKE '% - CANADA' THEN SUBSTR(TRIM(UPPER(STATE)), 1, 2) ELSE TRIM(UPPER(STATE)) END;
       
COMMIT;

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/PKG_CUSTOMER_MANAGEMENT.psk?rev=HEAD
CREATE OR REPLACE PACKAGE REPORT.PKG_CUSTOMER_MANAGEMENT AS
    PROCEDURE CREATE_TERMINAL(
        pn_terminal_id OUT REPORT.TERMINAL.TERMINAL_ID%TYPE,
        pv_device_serial_cd IN REPORT.EPORT.EPORT_SERIAL_NUM%TYPE,
        pn_dealer_id IN CORP.DEALER.DEALER_ID%TYPE,
        pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
        pn_machine_id IN REPORT.TERMINAL.MACHINE_ID%TYPE,
        pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
        pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
        pn_region_id IN REPORT.REGION.REGION_ID%TYPE,
        pv_region_name IN REPORT.REGION.REGION_NAME%TYPE,
        pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
        pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
        pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
        pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
        pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
        pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
        pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
        pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
        pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_pc_id IN REPORT.TERMINAL.PRIMARY_CONTACT_ID%TYPE,
        pn_sc_id IN REPORT.TERMINAL.SECONDARY_CONTACT_ID%TYPE,
        pn_location_type_id IN REPORT.LOCATION.LOCATION_TYPE_ID%TYPE,
        pv_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
        pn_product_type_id IN REPORT.TERMINAL.PRODUCT_TYPE_ID%TYPE,
        pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
        pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
        pn_avg_amt IN REPORT.TERMINAL.AVG_TRANS_AMT%TYPE,
        pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
        pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
        pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
        pn_vends REPORT.TERMINAL.VENDS%TYPE,
        pv_term_var_1 IN REPORT.TERMINAL.TERM_VAR_1%TYPE,
        pv_term_var_2 IN REPORT.TERMINAL.TERM_VAR_2%TYPE,
        pv_term_var_3 IN REPORT.TERMINAL.TERM_VAR_3%TYPE,
        pv_term_var_4 IN REPORT.TERMINAL.TERM_VAR_4%TYPE,
        pv_term_var_5 IN REPORT.TERMINAL.TERM_VAR_5%TYPE,
        pv_term_var_6 IN REPORT.TERMINAL.TERM_VAR_6%TYPE,
        pv_term_var_7 IN REPORT.TERMINAL.TERM_VAR_7%TYPE,
        pv_term_var_8 IN REPORT.TERMINAL.TERM_VAR_8%TYPE,
		pd_activate_date IN REPORT.TERMINAL_EPORT.START_DATE%TYPE DEFAULT SYSDATE);
    
    PROCEDURE CREATE_TERMINAL_MASS(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
		pn_terminal_id OUT REPORT.TERMINAL.TERMINAL_ID%TYPE,
        pv_device_serial_cd IN REPORT.EPORT.EPORT_SERIAL_NUM%TYPE,
        pn_dealer_id IN CORP.DEALER.DEALER_ID%TYPE,
        pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
        pv_machine_make REPORT.MACHINE.MAKE%TYPE,
		pv_machine_model REPORT.MACHINE.MODEL%TYPE,
        pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
        pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
        pv_region_name IN REPORT.REGION.REGION_NAME%TYPE,
        pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
        pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
        pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
        pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
        pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
        pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
        pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
        pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
        pv_location_type_name REPORT.LOCATION_TYPE.LOCATION_TYPE_NAME%TYPE,
        pn_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
        pv_product_type_name REPORT.PRODUCT_TYPE.PRODUCT_TYPE_NAME%TYPE,
        pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
        pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
        pn_avg_amt IN REPORT.TERMINAL.AVG_TRANS_AMT%TYPE,
        pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
        pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
        pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
        pn_vends REPORT.TERMINAL.VENDS%TYPE,
        pv_term_var_1 REPORT.TERMINAL.term_var_1%TYPE,
        pv_term_var_2 REPORT.TERMINAL.term_var_2%TYPE,
        pv_term_var_3 REPORT.TERMINAL.term_var_3%TYPE,
        pv_term_var_4 REPORT.TERMINAL.term_var_4%TYPE,
        pv_term_var_5 REPORT.TERMINAL.term_var_5%TYPE,
        pv_term_var_6 REPORT.TERMINAL.term_var_6%TYPE,
        pv_term_var_7 REPORT.TERMINAL.term_var_7%TYPE,
        pv_term_var_8 REPORT.TERMINAL.term_var_8%TYPE,
		pd_activate_date IN REPORT.TERMINAL_EPORT.START_DATE%TYPE DEFAULT SYSDATE);
        
    PROCEDURE UPDATE_TERMINAL(
       pn_terminal_id IN REPORT.TERMINAL.TERMINAL_ID%TYPE,
       pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
       pn_machine_id IN REPORT.TERMINAL.MACHINE_ID%TYPE,
       pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
       pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
       pn_region_id IN REPORT.REGION.REGION_ID%TYPE,
       pv_region_name IN REPORT.REGION.REGION_NAME%TYPE,
       pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
       pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
       pn_address_id REPORT.TERMINAL_ADDR.ADDRESS_ID%TYPE,
       pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
       pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
       pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
       pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
       pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
       pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
       pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
       pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
       pn_pc_id IN REPORT.TERMINAL.PRIMARY_CONTACT_ID%TYPE,
       pn_sc_id IN REPORT.TERMINAL.SECONDARY_CONTACT_ID%TYPE,
       pn_location_type_id IN REPORT.LOCATION.LOCATION_TYPE_ID%TYPE,
       pn_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
       pn_product_type_id IN REPORT.TERMINAL.PRODUCT_TYPE_ID%TYPE,
       pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
       pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
       pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
       pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
       pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
       pn_vends REPORT.TERMINAL.VENDS%TYPE,
       pv_term_var_1 REPORT.TERMINAL.term_var_1%TYPE,
       pv_term_var_2 REPORT.TERMINAL.term_var_2%TYPE,
       pv_term_var_3 REPORT.TERMINAL.term_var_3%TYPE,
       pv_term_var_4 REPORT.TERMINAL.term_var_4%TYPE,
       pv_term_var_5 REPORT.TERMINAL.term_var_5%TYPE,
       pv_term_var_6 REPORT.TERMINAL.term_var_6%TYPE,
       pv_term_var_7 REPORT.TERMINAL.term_var_7%TYPE,
       pv_term_var_8 REPORT.TERMINAL.term_var_8%TYPE);
       
   PROCEDURE UPDATE_TERMINAL_MASS(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
		pn_terminal_id IN REPORT.TERMINAL.TERMINAL_ID%TYPE,
        pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
        pv_machine_make REPORT.MACHINE.MAKE%TYPE,
		pv_machine_model REPORT.MACHINE.MODEL%TYPE,
        pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
        pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
        pv_region_name IN REPORT.REGION.REGION_NAME%TYPE,
        pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
        pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
        pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
        pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
        pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
        pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
        pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
        pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
        pn_location_type_id REPORT.LOCATION_TYPE.LOCATION_TYPE_ID%TYPE,
		pv_location_type_name REPORT.LOCATION_TYPE.LOCATION_TYPE_NAME%TYPE,
        pn_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
        pv_product_type_name REPORT.PRODUCT_TYPE.PRODUCT_TYPE_NAME%TYPE,
        pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
        pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
        pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
        pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
        pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
        pn_vends REPORT.TERMINAL.VENDS%TYPE,
        pv_term_var_1 REPORT.TERMINAL.term_var_1%TYPE,
        pv_term_var_2 REPORT.TERMINAL.term_var_2%TYPE,
        pv_term_var_3 REPORT.TERMINAL.term_var_3%TYPE,
        pv_term_var_4 REPORT.TERMINAL.term_var_4%TYPE,
        pv_term_var_5 REPORT.TERMINAL.term_var_5%TYPE,
        pv_term_var_6 REPORT.TERMINAL.term_var_6%TYPE,
        pv_term_var_7 REPORT.TERMINAL.term_var_7%TYPE,
        pv_term_var_8 REPORT.TERMINAL.term_var_8%TYPE);
        
    PROCEDURE CREATE_CUSTOMER_BANK(
        pn_customer_bank_id OUT CORP.CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        pn_customer_id IN CORP.CUSTOMER_BANK.CUSTOMER_ID%TYPE,
        pn_user_id IN CORP.CUSTOMER.USER_ID%TYPE,
        pv_bank_name IN CORP.CUSTOMER_BANK.BANK_NAME%TYPE,
        pv_bank_address1 IN CORP.CUSTOMER_BANK.BANK_ADDRESS%TYPE,
        pv_bank_city IN CORP.CUSTOMER_BANK.BANK_CITY%TYPE,
        pn_bank_state_id IN CORP.STATE.STATE_ID%TYPE,
        pv_bank_zip IN CORP.CUSTOMER_BANK.BANK_ZIP%TYPE,
        pv_account_title IN CORP.CUSTOMER_BANK.ACCOUNT_TITLE%TYPE,
        pv_account_type IN CORP.CUSTOMER_BANK.ACCOUNT_TYPE%TYPE,
        pv_bank_routing_nbr IN CORP.CUSTOMER_BANK.BANK_ROUTING_NBR%TYPE,
        pv_bank_acct_nbr IN CORP.CUSTOMER_BANK.BANK_ACCT_NBR%TYPE,
        pv_contact_name IN CORP.CUSTOMER_BANK.CONTACT_NAME%TYPE,
        pv_contact_title IN CORP.CUSTOMER_BANK.CONTACT_TITLE%TYPE,
        pv_contact_telephone IN CORP.CUSTOMER_BANK.CONTACT_TELEPHONE%TYPE,
        pv_contact_fax IN CORP.CUSTOMER_BANK.CONTACT_FAX%TYPE);
        
    PROCEDURE UPDATE_REGION_NAME(
        pn_region_id REPORT.REGION.REGION_ID%TYPE,
        pv_region_name REPORT.REGION.REGION_NAME%TYPE,
        pn_customer_id REPORT.TERMINAL.CUSTOMER_ID%TYPE);
END;
/
-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/REPTP/REPORT/PKG_CUSTOMER_MANAGEMENT.pbk?rev=HEAD
CREATE OR REPLACE PACKAGE BODY REPORT.PKG_CUSTOMER_MANAGEMENT AS
    PROCEDURE INTERNAL_CREATE_TERMINAL(
       pn_terminal_id OUT REPORT.TERMINAL.TERMINAL_ID%TYPE,
       pv_device_serial_cd IN REPORT.EPORT.EPORT_SERIAL_NUM%TYPE,
       pn_dealer_id IN CORP.DEALER.DEALER_ID%TYPE,
       pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
       pn_machine_id IN REPORT.TERMINAL.MACHINE_ID%TYPE,
       pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
       pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
       pn_region_id IN REPORT.REGION.REGION_ID%TYPE,
       pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
       pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
       pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
       pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
       pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
       pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
       pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
       pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
       pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
       pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
       pn_pc_id IN REPORT.TERMINAL.PRIMARY_CONTACT_ID%TYPE,
       pn_sc_id IN REPORT.TERMINAL.SECONDARY_CONTACT_ID%TYPE,
       pn_location_type_id IN REPORT.LOCATION.LOCATION_TYPE_ID%TYPE,
       pv_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
       pn_product_type_id IN REPORT.TERMINAL.PRODUCT_TYPE_ID%TYPE,
       pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
       pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
       pn_avg_amt IN REPORT.TERMINAL.AVG_TRANS_AMT%TYPE,
       pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
       pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
       pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
       pn_vends REPORT.TERMINAL.VENDS%TYPE,
       pv_term_var_1 IN REPORT.TERMINAL.TERM_VAR_1%TYPE,
       pv_term_var_2 IN REPORT.TERMINAL.TERM_VAR_2%TYPE,
       pv_term_var_3 IN REPORT.TERMINAL.TERM_VAR_3%TYPE,
       pv_term_var_4 IN REPORT.TERMINAL.TERM_VAR_4%TYPE,
       pv_term_var_5 IN REPORT.TERMINAL.TERM_VAR_5%TYPE,
       pv_term_var_6 IN REPORT.TERMINAL.TERM_VAR_6%TYPE,
       pv_term_var_7 IN REPORT.TERMINAL.TERM_VAR_7%TYPE,
       pv_term_var_8 IN REPORT.TERMINAL.TERM_VAR_8%TYPE,
	   pd_activate_date IN REPORT.TERMINAL_EPORT.START_DATE%TYPE)
    IS
      l_eport_id REPORT.EPORT.EPORT_ID%TYPE;
      l_dev_type_id REPORT.EPORT.DEVICE_TYPE_ID%TYPE;
      l_location_id REPORT.TERMINAL.LOCATION_ID%TYPE;
      l_terminal_nbr REPORT.TERMINAL.TERMINAL_NBR%TYPE;
      l_customer_id REPORT.TERMINAL.CUSTOMER_ID%TYPE;
      l_license_id NUMBER;
      l_addr_id REPORT.TERMINAL_ADDR.ADDRESS_ID%TYPE;
      l_activate_date DATE := NVL(pd_activate_date, SYSDATE);
      l_last_deactivate_date DATE;
      l_start_date DATE;
      l_cnt NUMBER;
      l_business_unit_id CORP.BUSINESS_UNIT.BUSINESS_UNIT_ID%TYPE;
	  ln_new_pay_sched_id REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE;
      l_lock VARCHAR2(128);
	  ln_user_id REPORT.USER_LOGIN.USER_ID%TYPE := pn_user_id;
	  ln_pc_id REPORT.TERMINAL.PRIMARY_CONTACT_ID%TYPE := pn_pc_id;
    BEGIN
         BEGIN
              SELECT TERMINAL_SEQ.NEXTVAL, EPORT_ID, DEVICE_TYPE_ID, TERMINAL_ADDR_SEQ.NEXTVAL
                 INTO pn_terminal_id, l_eport_id, l_dev_type_id, l_addr_id FROM EPORT WHERE EPORT_SERIAL_NUM = pv_device_serial_cd;
         EXCEPTION
              WHEN NO_DATA_FOUND THEN
                   RAISE_APPLICATION_ERROR(-20201, 'Invalid eport serial number');
              WHEN OTHERS THEN
                   RAISE;
         END;
		 
		 l_lock := GLOBALS_PKG.REQUEST_LOCK('EPORT.EPORT_ID', l_eport_id);		 
         -- CHECK THAT EPORT IS INACTIVE
             SELECT NVL(MAX(NVL(END_DATE, MAX_DATE)), MIN_DATE)
               INTO l_last_deactivate_date
               FROM REPORT.TERMINAL_EPORT 
               WHERE EPORT_ID = l_eport_id;
             /*
         SELECT COUNT(TERMINAL_ID) 
               INTO l_cnt 
               FROM TERMINAL_EPORT 
               WHERE EPORT_ID = l_eport_id 
               AND NVL(END_DATE, MAX_DATE) > l_activate_date;
               */
         IF l_last_deactivate_date > l_activate_date THEN
              SELECT COUNT(TERMINAL_ID) INTO l_cnt FROM USER_TERMINAL WHERE USER_ID = ln_user_id AND
                 TERMINAL_ID IN(SELECT TERMINAL_ID FROM TERMINAL_EPORT WHERE EPORT_ID = l_eport_id AND NVL(END_DATE, MAX_DATE) > l_activate_date);
             IF l_cnt > 0 THEN --USER CAN VIEW TERMINAL
                  RAISE_APPLICATION_ERROR(-20205, 'Eport has already been activated');
             ELSE -- THIS MAY INDICATE THAT SOMEONE ACTIVATED THE WRONG TERMINAL
                  RAISE_APPLICATION_ERROR(-20206, 'Eport already in use');
             END IF;
         END IF;
         --VERIFY DEALER
         SELECT COUNT(DEALER_ID) INTO l_cnt FROM CORP.DEALER_EPORT WHERE DEALER_ID = pn_dealer_id AND EPORT_ID = l_eport_id;
         IF l_cnt = 0 THEN
              RAISE_APPLICATION_ERROR(-20202, 'Invalid dealer specified');
         END IF;
         --CREATE TERMINAL_NBR
         SELECT 'T0'|| TO_CHAR(TERMINAL_NBR_SEQ.NEXTVAL, 'FM99000000') INTO l_terminal_nbr FROM DUAL;
         
         --INSERT INTO TERMINAL, LOCATION, TERMINAL_ADDR
         IF pn_customer_bank_id <> 0 THEN
             BEGIN
                  SELECT CUSTOMER_ID INTO l_customer_id FROM CORP.CUSTOMER_BANK WHERE CUSTOMER_BANK_ID = pn_customer_bank_id;
             EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                       RAISE_APPLICATION_ERROR(-20203, 'Invalid customer bank');
                  WHEN OTHERS THEN
                       RAISE;
             END;
         ELSE
              BEGIN
                  SELECT CUSTOMER_ID INTO l_customer_id FROM USER_LOGIN WHERE USER_ID = ln_pc_id AND CUSTOMER_ID <> 0;
             EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                       RAISE_APPLICATION_ERROR(-20203, 'Invalid primary contact');
                  WHEN OTHERS THEN
                       RAISE;
             END;
         END IF;
		 
		 IF ln_user_id = 0 THEN
			SELECT MAX(USER_ID)
			INTO ln_user_id
			FROM CORP.CUSTOMER
			WHERE CUSTOMER_ID = l_customer_id;
			
			ln_pc_id := ln_user_id;
		 END IF;
		 
         BEGIN
              SELECT LICENSE_ID INTO l_license_id FROM (SELECT LICENSE_ID FROM CORP.CUSTOMER_LICENSE CL, CORP.LICENSE_NBR LN
                  WHERE CL.LICENSE_NBR = LN.LICENSE_NBR AND CL.CUSTOMER_ID = l_customer_id ORDER BY RECEIVED DESC) WHERE ROWNUM = 1;
         EXCEPTION
              WHEN NO_DATA_FOUND THEN
                   RAISE_APPLICATION_ERROR(-20204, 'No license agreement found for this customer');
              WHEN OTHERS THEN
                   RAISE;
         END;
         BEGIN
              SELECT LICENSE_ID 
                INTO l_license_id 
                FROM (SELECT LICENSE_ID 
                        FROM CORP.DEALER_LICENSE 
                       WHERE DEALER_ID = pn_dealer_id
                         AND SYSDATE >= NVL(START_DATE, MIN_DATE)
                         AND SYSDATE < NVL(END_DATE, MAX_DATE)
                       ORDER BY START_DATE DESC, LICENSE_ID DESC)
               WHERE ROWNUM = 1;
         EXCEPTION
              WHEN NO_DATA_FOUND THEN
                   RAISE_APPLICATION_ERROR(-20204, 'No license agreement found for this dealer');
              WHEN OTHERS THEN
                   RAISE;
         END;    
         INSERT INTO TERMINAL_ADDR(ADDRESS_ID, CUSTOMER_ID, ADDRESS1, CITY, STATE, ZIP, COUNTRY_CD)
             VALUES(l_addr_id, l_customer_id, pv_address1, pv_city, pv_state_cd, pv_postal, pv_country_cd);
         BEGIN
             SELECT LOCATION_ID INTO l_location_id FROM LOCATION WHERE EQL(LOCATION_NAME, pv_location_name) = -1 AND EPORT_ID =  l_eport_id AND TERMINAL_ID = 0;
             UPDATE LOCATION SET (DESCRIPTION, ADDRESS_ID, UPD_DATE, TERMINAL_ID, LOCATION_TYPE_ID, LOCATION_TYPE_SPECIFY) =
                   (SELECT pv_location_details, l_addr_id, SYSDATE, pn_terminal_id, pn_location_type_id,pv_location_type_specific FROM DUAL) WHERE LOCATION_ID = l_location_id;
         EXCEPTION
              WHEN NO_DATA_FOUND THEN
                  SELECT LOCATION_SEQ.NEXTVAL INTO l_location_id FROM DUAL;
                   INSERT INTO LOCATION(LOCATION_ID, LOCATION_NAME, DESCRIPTION, ADDRESS_ID, CREATE_BY, TERMINAL_ID, EPORT_ID, LOCATION_TYPE_ID, LOCATION_TYPE_SPECIFY)
                          VALUES(l_location_id, pv_location_name, pv_location_details, l_addr_id, ln_user_id, pn_terminal_id, l_eport_id, pn_location_type_id, pv_location_type_specific);
              WHEN OTHERS THEN
                   RAISE;
         END;
         l_business_unit_id := FIND_BUSINESS_UNIT(l_eport_id, l_customer_id);
         SELECT MAX(PAYMENT_SCHEDULE_ID)
          INTO ln_new_pay_sched_id
          FROM CORP.PAYMENT_SCHEDULE
         WHERE PAYMENT_SCHEDULE_ID = pn_pay_sched_id
           AND SELECTABLE = 'Y';
         IF l_business_unit_id IN(2,3,5) OR ln_new_pay_sched_id IS NULL THEN
            SELECT DEFAULT_PAYMENT_SCHEDULE_ID
              INTO ln_new_pay_sched_id
              FROM CORP.BUSINESS_UNIT
             WHERE BUSINESS_UNIT_ID = l_business_unit_id;
         END IF;
                 
         INSERT INTO TERMINAL(
                TERMINAL_ID,
                TERMINAL_NBR,
                TERMINAL_NAME,
                EPORT_ID,
                CUSTOMER_ID,
                LOCATION_ID,
                ASSET_NBR,
                MACHINE_ID,
                TELEPHONE,
                PREFIX,
                PRODUCT_TYPE_ID,
                PRODUCT_TYPE_SPECIFY,
                PRIMARY_CONTACT_ID,
                SECONDARY_CONTACT_ID,
                AUTHORIZATION_MODE,
                AVG_TRANS_AMT,
                TIME_ZONE_ID,
                DEX_DATA,
                RECEIPT,
                VENDS,
                TERM_VAR_1,
                TERM_VAR_2,
                TERM_VAR_3,
                TERM_VAR_4,
                TERM_VAR_5,
                TERM_VAR_6,
                TERM_VAR_7,
                TERM_VAR_8,
                BUSINESS_UNIT_ID,
                PAYMENT_SCHEDULE_ID)
             SELECT
                pn_terminal_id,
                l_terminal_nbr,
                l_terminal_nbr,
                l_eport_id,
                l_customer_id,
                l_location_id,
                pv_asset_nbr,
                pn_machine_id,
                pv_telephone,
                pv_prefix,
                pn_product_type_id,
                pn_product_type_specific,
                ln_pc_id,
                pn_sc_id,
                pc_auth_mode,
                NVL(pn_avg_amt, 0),
                pn_time_zone_id,
                pc_dex_data,
                pc_receipt,
                pn_vends,
                pv_term_var_1,
                pv_term_var_2,
                pv_term_var_3,
                pv_term_var_4,
                pv_term_var_5,
                pv_term_var_6,
                pv_term_var_7,
                pv_term_var_8,
                l_business_unit_id,
                ln_new_pay_sched_id
              FROM DUAL;
              
            IF pn_region_id != 0 THEN
                INSERT INTO REPORT.TERMINAL_REGION(TERMINAL_ID, REGION_ID)
                  VALUES(pn_terminal_id, pn_region_id); 
            END IF;
            
            -- Find start date
            SELECT /*+INDEX(x IX_TRANS_EPORT_ID) */
                   GREATEST(l_last_deactivate_date, NVL(MIN(x.CLOSE_DATE), MIN_DATE), l_activate_date - 30)
              INTO l_start_date
              FROM REPORT.TRANS x
             WHERE x.EPORT_ID = l_eport_id
               AND x.CLOSE_DATE >= l_last_deactivate_date
               AND x.CLOSE_DATE > l_activate_date - 30; 
              
         --INSERT INTO TERMINAL_EPORT
         /* A much faster way - BSK 04/10/2007
         TERMINAL_EPORT_UPD(pn_terminal_id, l_eport_id, l_start_date, NULL, NULL, 'Y');*/
         INSERT INTO REPORT.TERMINAL_EPORT(EPORT_ID, TERMINAL_ID, START_DATE, END_DATE)
            VALUES(l_eport_id, pn_terminal_id, l_start_date, NULL);
        
         --UPDATE DEALER_EPORT
         UPDATE CORP.DEALER_EPORT SET ACTIVATE_DATE = l_activate_date WHERE DEALER_ID = pn_dealer_id AND EPORT_ID = l_eport_id;
         --INSERT INTO USER_TERMINAL
         INSERT INTO USER_TERMINAL(USER_ID, TERMINAL_ID, ALLOW_EDIT)
         /* A much faster way - BSK 04/10/2007
              SELECT USER_ID, pn_terminal_id, 'Y' FROM USER_LOGIN WHERE CAN_ADMIN_USER(ln_user_id, USER_ID) = 'Y'; */
             SELECT USER_ID, pn_terminal_id, 'Y'
               FROM REPORT.USER_LOGIN u
               CONNECT BY PRIOR u.admin_id = u.user_id
               START WITH u.user_id = ln_user_id
             UNION
             SELECT a.USER_ID, pn_terminal_id, 'Y'
               FROM REPORT.USER_LOGIN a
              INNER JOIN REPORT.USER_PRIVS up
                 ON a.USER_ID = up.user_id
              WHERE a.CUSTOMER_ID = l_customer_id
                AND up.priv_id IN(5,8);
    
         --INSERT INTO CORP.CUSTOMER_BANK_TERMINAL
         IF pn_customer_bank_id <> 0 THEN
             INSERT INTO CORP.CUSTOMER_BANK_TERMINAL(TERMINAL_ID, CUSTOMER_BANK_ID, START_DATE)
               VALUES(pn_terminal_id, pn_customer_bank_id, NULL); --l_start_date); : Use NULL (or MIN_DATE) instead
         END IF;
         --INSERT INTO SERVICE_FEES
         -- frequency interval so that first active day has a service fee (BSK 09-14-04)
         -- made start dates NULL (or MIN_DATE)
         INSERT INTO CORP.SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID, FEE_AMOUNT, FREQUENCY_ID, LAST_PAYMENT, START_DATE, FEE_PERCENT)
              SELECT CORP.SERVICE_FEE_SEQ.NEXTVAL, pn_terminal_id, lsf.FEE_ID,
                     DECODE(lsf.FEE_ID, 8, 0, lsf.AMOUNT), lsf.FREQUENCY_ID,
                     DECODE(lsf.START_IMMEDIATELY_FLAG, 'Y', l_start_date), l_start_date,
                     DECODE(lsf.FEE_ID, 8, lsf.AMOUNT, NULL)
                FROM CORP.LICENSE_SERVICE_FEES lsf, CORP.FREQUENCY f
               WHERE lsf.FREQUENCY_ID = f.FREQUENCY_ID
                 AND lsf.LICENSE_ID = l_license_id
                 AND (pc_dex_data IN('A', 'D') OR lsf.FEE_ID != 4);
         IF pc_dex_data IN('A', 'D') THEN
             INSERT INTO CORP.SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID, FEE_AMOUNT, FREQUENCY_ID, LAST_PAYMENT, START_DATE)
                 SELECT CORP.SERVICE_FEE_SEQ.NEXTVAL, pn_terminal_id, 4,
                       4.00, f.FREQUENCY_ID, NULL, l_start_date
                  FROM CORP.FREQUENCY f
                 WHERE f.FREQUENCY_ID = 2
                     AND NOT EXISTS(SELECT 1 
                                      FROM CORP.LICENSE_SERVICE_FEES lsf 
                                     WHERE lsf.LICENSE_ID = l_license_id
                                       AND lsf.FEE_ID = 4);
         END IF;
         --INSERT INTO PROCESS_FEES (Added FEE_AMOUNT - BSK 09-17-04)
         INSERT INTO CORP.PROCESS_FEES(PROCESS_FEE_ID, TERMINAL_ID, TRANS_TYPE_ID, FEE_PERCENT, FEE_AMOUNT, MIN_AMOUNT, START_DATE)
             SELECT CORP.PROCESS_FEE_SEQ.NEXTVAL, pn_terminal_id, TRANS_TYPE_ID, FEE_PERCENT, FEE_AMOUNT, MIN_AMOUNT, NULL
             FROM CORP.LICENSE_PROCESS_FEES WHERE LICENSE_ID = l_license_id;
    END;

    PROCEDURE INTERNAL_UPDATE_TERMINAL(
       pn_terminal_id IN REPORT.TERMINAL.TERMINAL_ID%TYPE,
       pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
       pn_machine_id IN REPORT.TERMINAL.MACHINE_ID%TYPE,
       pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
       pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
       pn_region_id IN REPORT.REGION.REGION_ID%TYPE,
       pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
       pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
       pn_address_id REPORT.TERMINAL_ADDR.ADDRESS_ID%TYPE,
       pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
       pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
       pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
       pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
       pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
       pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
       pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
       pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
       pn_pc_id IN REPORT.TERMINAL.PRIMARY_CONTACT_ID%TYPE,
       pn_sc_id IN REPORT.TERMINAL.SECONDARY_CONTACT_ID%TYPE,
       pn_location_type_id IN REPORT.LOCATION.LOCATION_TYPE_ID%TYPE,
       pn_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
       pn_product_type_id IN REPORT.TERMINAL.PRODUCT_TYPE_ID%TYPE,
       pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
       pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
       pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
       pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
       pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
       pn_vends REPORT.TERMINAL.VENDS%TYPE,
       pv_term_var_1 REPORT.TERMINAL.term_var_1%TYPE,
       pv_term_var_2 REPORT.TERMINAL.term_var_2%TYPE,
       pv_term_var_3 REPORT.TERMINAL.term_var_3%TYPE,
       pv_term_var_4 REPORT.TERMINAL.term_var_4%TYPE,
       pv_term_var_5 REPORT.TERMINAL.term_var_5%TYPE,
       pv_term_var_6 REPORT.TERMINAL.term_var_6%TYPE,
       pv_term_var_7 REPORT.TERMINAL.term_var_7%TYPE,
       pv_term_var_8 REPORT.TERMINAL.term_var_8%TYPE)    
    IS
      pv_location_name_id REPORT.TERMINAL.LOCATION_ID%TYPE;
      l_customer_id REPORT.TERMINAL.CUSTOMER_ID%TYPE;
      l_old_cb_id NUMBER;
      l_old_lname REPORT.LOCATION.LOCATION_NAME%TYPE;
      l_old_ldesc REPORT.LOCATION.DESCRIPTION%TYPE;
      l_new_address_id REPORT.TERMINAL_ADDR.ADDRESS_ID%TYPE;
      l_old_address_id REPORT.LOCATION.ADDRESS_ID%TYPE;
      l_old_dex REPORT.TERMINAL.DEX_DATA%TYPE;
      isnew BOOLEAN;
      l_date DATE := SYSDATE;
	  l_lock VARCHAR2(128);
      ln_business_unit_id REPORT.TERMINAL.BUSINESS_UNIT_ID%TYPE;
      ln_old_pay_sched_id REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE;
      ln_old_region_id REPORT.REGION.REGION_ID%TYPE;
	  ln_new_pay_sched_id REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE;
    BEGIN
		 l_lock := GLOBALS_PKG.REQUEST_LOCK('TERMINAL.TERMINAL_ID', pn_terminal_id);	
         -- if a new location is named than create it
         SELECT T.LOCATION_ID, T.CUSTOMER_ID, L.LOCATION_NAME, L.DESCRIPTION, L.ADDRESS_ID, CB.CUSTOMER_BANK_ID, T.DEX_DATA, T.BUSINESS_UNIT_ID, T.PAYMENT_SCHEDULE_ID, NVL(TR.REGION_ID, 0)
           INTO pv_location_name_id, l_customer_id, l_old_lname, l_old_ldesc, l_old_address_id, l_old_cb_id, l_old_dex, ln_business_unit_id, ln_old_pay_sched_id, ln_old_region_id
           FROM REPORT.TERMINAL T
           LEFT OUTER JOIN REPORT.LOCATION L ON T.LOCATION_ID = L.LOCATION_ID
           LEFT OUTER JOIN CORP.VW_CURRENT_BANK_ACCT CB ON T.TERMINAL_ID = CB.TERMINAL_ID
           LEFT OUTER JOIN REPORT.TERMINAL_REGION TR ON T.TERMINAL_ID = TR.TERMINAL_ID
          WHERE T.TERMINAL_ID = pn_terminal_id;
         IF EQL(l_old_lname, pv_location_name) <> -1 THEN  -- location has changed
             UPDATE REPORT.LOCATION SET status = 'D' WHERE LOCATION_ID = pv_location_name_id;
            INSERT INTO REPORT.TERMINAL_CHANGES (TERMINAL_ID, ATTRIBUTE, OLD_VALUE, NEW_VALUE)
                   VALUES(pn_terminal_id, 'LOCATION',l_old_lname, pv_location_name);
            BEGIN
               SELECT LOCATION_ID INTO pv_location_name_id FROM REPORT.LOCATION WHERE LOCATION_NAME = pv_location_name AND TERMINAL_ID = pn_terminal_id;
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                     isnew := true;
               WHEN OTHERS THEN
                  RAISE;
            END;
         END IF;
         --RECORD CHANGES (SO USALIVE CAN BE UPDATED)
         DECLARE
             l_t TERMINAL.TELEPHONE%TYPE;
             l_p TERMINAL.PREFIX%TYPE;
             l_a TERMINAL.AUTHORIZATION_MODE%TYPE;
             l_z TERMINAL.TIME_ZONE_ID%TYPE;
             l_d TERMINAL.DEX_DATA%TYPE;
             l_r TERMINAL.RECEIPT%TYPE;
             l_v TERMINAL.VENDS%TYPE;
             l_cnt NUMBER;
         BEGIN
              SELECT TELEPHONE, PREFIX, AUTHORIZATION_MODE, TIME_ZONE_ID, DEX_DATA, RECEIPT, VENDS 
                INTO l_t, l_p, l_a, l_z, l_d, l_r, l_v 
                FROM REPORT.TERMINAL 
               WHERE TERMINAL_ID = pn_terminal_id;
             IF EQL(l_t, pv_telephone) <> -1 THEN
                 UPDATE REPORT.TERMINAL_CHANGES SET NEW_VALUE = pv_telephone WHERE TERMINAL_ID = pn_terminal_id
                   AND ATTRIBUTE = 'TELEPHONE' RETURNING 1 INTO l_cnt;
                IF NVL(l_cnt, 0) = 0 THEN
                    INSERT INTO REPORT.TERMINAL_CHANGES (TERMINAL_ID, ATTRIBUTE, OLD_VALUE, NEW_VALUE)
                         VALUES(pn_terminal_id, 'TELEPHONE',l_t, pv_telephone);
                END IF;
             END IF;
             IF EQL(l_p, pv_prefix) <> -1 THEN
                 UPDATE REPORT.TERMINAL_CHANGES SET NEW_VALUE = pv_prefix WHERE TERMINAL_ID = pn_terminal_id
                   AND ATTRIBUTE = 'PREFIX' RETURNING 1 INTO l_cnt;
                IF NVL(l_cnt, 0) = 0 THEN
                    INSERT INTO REPORT.TERMINAL_CHANGES (TERMINAL_ID, ATTRIBUTE, OLD_VALUE, NEW_VALUE)
                   VALUES(pn_terminal_id, 'PREFIX',l_p, pv_prefix);
                END IF;
             END IF;
             IF EQL(l_a, pc_auth_mode) <> -1 THEN
                 UPDATE REPORT.TERMINAL_CHANGES SET NEW_VALUE = pc_auth_mode WHERE TERMINAL_ID = pn_terminal_id
                   AND ATTRIBUTE = 'AUTHORIZATION_MODE' RETURNING 1 INTO l_cnt;
                IF NVL(l_cnt, 0) = 0 THEN
                    INSERT INTO REPORT.TERMINAL_CHANGES (TERMINAL_ID, ATTRIBUTE, OLD_VALUE, NEW_VALUE)
                   VALUES(pn_terminal_id, 'AUTHORIZATION_MODE',l_a, pc_auth_mode);
                END IF;
             END IF;
             IF EQL(l_z, pn_time_zone_id) <> -1 THEN
                 UPDATE REPORT.TERMINAL_CHANGES SET NEW_VALUE = pn_time_zone_id WHERE TERMINAL_ID = pn_terminal_id
                   AND ATTRIBUTE = 'TIME_ZONE' RETURNING 1 INTO l_cnt;
                IF NVL(l_cnt, 0) = 0 THEN
                    INSERT INTO REPORT.TERMINAL_CHANGES (TERMINAL_ID, ATTRIBUTE, OLD_VALUE, NEW_VALUE)
                   VALUES(pn_terminal_id, 'TIME_ZONE',l_z, pn_time_zone_id);
                END IF;
             END IF;
             IF EQL(l_d, pc_dex_data) <> -1 THEN
                 UPDATE REPORT.TERMINAL_CHANGES SET NEW_VALUE = pc_dex_data WHERE TERMINAL_ID = pn_terminal_id
                   AND ATTRIBUTE = 'DEX_DATA' RETURNING 1 INTO l_cnt;
                IF NVL(l_cnt, 0) = 0 THEN
                    INSERT INTO REPORT.TERMINAL_CHANGES (TERMINAL_ID, ATTRIBUTE, OLD_VALUE, NEW_VALUE)
                   VALUES(pn_terminal_id, 'DEX_DATA',l_d, pc_dex_data);
                END IF;
             END IF;
             IF EQL(l_r, pc_receipt) <> -1 THEN
                 UPDATE REPORT.TERMINAL_CHANGES SET NEW_VALUE = pc_receipt WHERE TERMINAL_ID = pn_terminal_id
                   AND ATTRIBUTE = 'RECEIPT' RETURNING 1 INTO l_cnt;
                IF NVL(l_cnt, 0) = 0 THEN
                    INSERT INTO REPORT.TERMINAL_CHANGES (TERMINAL_ID, ATTRIBUTE, OLD_VALUE, NEW_VALUE)
                   VALUES(pn_terminal_id, 'RECEIPT',l_r, pc_receipt);
                END IF;
             END IF;
             IF EQL(l_v, pn_vends) <> -1 THEN
                 UPDATE REPORT.TERMINAL_CHANGES SET NEW_VALUE = pn_vends WHERE TERMINAL_ID = pn_terminal_id
                   AND ATTRIBUTE = 'VENDS' RETURNING 1 INTO l_cnt;
                IF NVL(l_cnt, 0) = 0 THEN
                    INSERT INTO REPORT.TERMINAL_CHANGES (TERMINAL_ID, ATTRIBUTE, OLD_VALUE, NEW_VALUE)
                   VALUES(pn_terminal_id, 'VENDS',l_v, pn_vends);
                END IF;
             END IF;
         END;
         -- UPDATE ADDRESS
         IF pn_address_id IS NULL THEN    -- none specified
            l_new_address_id := NULL;
         ELSIF pn_address_id = 0 THEN    --IS NEW
            SELECT REPORT.TERMINAL_ADDR_SEQ.NEXTVAL INTO l_new_address_id FROM DUAL;
            INSERT INTO REPORT.TERMINAL_ADDR(ADDRESS_ID, CUSTOMER_ID, ADDRESS1, CITY, STATE, ZIP, COUNTRY_CD)
                   VALUES(l_new_address_id, l_customer_id, pv_address1, pv_city, pv_state_cd, pv_postal, pv_country_cd);
         ELSE
             UPDATE REPORT.TERMINAL_ADDR SET ADDRESS1 = pv_address1, CITY = pv_city, STATE = pv_state_cd, ZIP = pv_postal, COUNTRY_CD = pv_country_cd
                WHERE ADDRESS_ID = pn_address_id AND (EQL(ADDRESS1, pv_address1) = 0
                OR EQL(CITY, pv_city) = 0 OR EQL(STATE, pv_state_cd) = 0 OR EQL(ZIP, pv_postal) = 0) OR EQL(COUNTRY_CD, pv_country_cd) = 0;
            l_new_address_id := pn_address_id;
         END IF;
         IF isnew THEN
             SELECT REPORT.LOCATION_SEQ.NEXTVAL INTO pv_location_name_id FROM DUAL;
             INSERT INTO REPORT.LOCATION(LOCATION_ID, LOCATION_NAME, DESCRIPTION, ADDRESS_ID, CREATE_BY, TERMINAL_ID, LOCATION_TYPE_ID, LOCATION_TYPE_SPECIFY)
                   VALUES(pv_location_name_id,pv_location_name,pv_location_details,l_new_address_id,pn_user_id,pn_terminal_id,pn_location_type_id,pn_location_type_specific);
         ELSE
             UPDATE REPORT.LOCATION L SET (DESCRIPTION, ADDRESS_ID, UPD_DATE, LOCATION_TYPE_ID, LOCATION_TYPE_SPECIFY) =
                   (SELECT pv_location_details,l_new_address_id,l_date,NVL(pn_location_type_id, L.LOCATION_TYPE_ID),DECODE(pn_location_type_id, NULL, L.LOCATION_TYPE_SPECIFY, pn_location_type_specific) FROM DUAL) WHERE location_id = pv_location_name_id;
         END IF;
  
         IF pn_pay_sched_id IS NULL THEN
            ln_new_pay_sched_id := ln_old_pay_sched_id;
         ELSIF pn_pay_sched_id != ln_old_pay_sched_id THEN
            SELECT MAX(PAYMENT_SCHEDULE_ID)
              INTO ln_new_pay_sched_id
              FROM CORP.PAYMENT_SCHEDULE
             WHERE PAYMENT_SCHEDULE_ID = pn_pay_sched_id
               AND SELECTABLE = 'Y';
            IF ln_new_pay_sched_id IS NULL THEN
                RAISE_APPLICATION_ERROR(-20217, 'Invalid payment schedule requested');
            ELSIF ln_business_unit_id IN(2,3,5) THEN
                RAISE_APPLICATION_ERROR(-20217, 'Not permitted to change payment schedule as requested');
            END IF;
         ELSE
            ln_new_pay_sched_id := ln_old_pay_sched_id;
         END IF;
         UPDATE REPORT.TERMINAL T SET (LOCATION_ID, ASSET_NBR, MACHINE_ID, TELEPHONE, PREFIX, PRODUCT_TYPE_ID, PRODUCT_TYPE_SPECIFY,
             PRIMARY_CONTACT_ID, SECONDARY_CONTACT_ID, AUTHORIZATION_MODE, TIME_ZONE_ID, DEX_DATA, RECEIPT, VENDS, PAYMENT_SCHEDULE_ID, STATUS,
             TERM_VAR_1, TERM_VAR_2, TERM_VAR_3, TERM_VAR_4, TERM_VAR_5, TERM_VAR_6, TERM_VAR_7, TERM_VAR_8) =
            (SELECT pv_location_name_id, pv_asset_nbr, pn_machine_id, pv_telephone, pv_prefix, pn_product_type_id, pn_product_type_specific,
             pn_pc_id, pn_sc_id, pc_auth_mode, pn_time_zone_id, NVL(pc_dex_data, T.DEX_DATA), NVL(pc_receipt, T.RECEIPT), pn_vends, ln_new_pay_sched_id, 'U',
             pv_term_var_1, pv_term_var_2, pv_term_var_3, pv_term_var_4, pv_term_var_5, pv_term_var_6, pv_term_var_7, pv_term_var_8 FROM DUAL) 
             WHERE TERMINAL_ID = pn_terminal_id;
         IF NVL(pn_region_id, 0) = ln_old_region_id THEN
            NULL;
         ELSIF ln_old_region_id = 0 THEN
            INSERT INTO REPORT.TERMINAL_REGION(TERMINAL_ID, REGION_ID)
                  VALUES(pn_terminal_id, pn_region_id); 
         ELSIF NVL(pn_region_id, 0) = 0 THEN
            DELETE FROM  REPORT.TERMINAL_REGION
             WHERE TERMINAL_ID = pn_terminal_id;
         ELSE
            UPDATE REPORT.TERMINAL_REGION
               SET REGION_ID = pn_region_id
             WHERE TERMINAL_ID = pn_terminal_id; 
         END IF;
         
         --UPDATE BANK ACCOUNT
         IF EQL(l_old_cb_id, pn_customer_bank_id) = 0 THEN
             DECLARE
                 l_cb_date DATE;
            BEGIN
                UPDATE CORP.CUSTOMER_BANK_TERMINAL SET END_DATE = CASE WHEN END_DATE < l_date THEN END_DATE ELSE l_date END
                 WHERE TERMINAL_ID = pn_terminal_id
                 RETURNING MAX(CASE WHEN END_DATE < l_date THEN END_DATE ELSE l_date END) INTO l_cb_date;
                IF NVL(pn_customer_bank_id, 0) <> 0 THEN
                       INSERT INTO CORP.CUSTOMER_BANK_TERMINAL(CUSTOMER_BANK_TERMINAL_ID, TERMINAL_ID, CUSTOMER_BANK_ID, START_DATE)
                        VALUES(CORP.CUSTOMER_BANK_TERMINAL_SEQ.NEXTVAL, pn_terminal_id, pn_customer_bank_id, l_cb_date);
                END IF;
            END;
         END IF;
         /*
         IF NVL(l_old_cb_id, 0) = 0 AND NVL(pn_customer_bank_id, 0) <> 0 THEN
             DECLARE
                 l_cb_date DATE;
            BEGIN
                 SELECT NVL(MIN(CLOSE_DATE), l_date) INTO l_cb_date FROM TRANS WHERE TERMINAL_ID = pn_terminal_id AND CLOSE_DATE >=
                     (SELECT NVL(MAX(END_DATE), CLOSE_DATE) FROM CORP.CUSTOMER_BANK_TERMINAL
                     WHERE TERMINAL_ID = pn_terminal_id AND END_DATE > NVL(START_DATE, END_DATE));
                 CORP.TERMINAL_REASSIGN_BANK_ACCOUNT(pn_terminal_id, pn_customer_bank_id, l_cb_date);
            END;
         ELSIF EQL(l_old_cb_id, pn_customer_bank_id) = 0 THEN
             CORP.TERMINAL_REASSIGN_BANK_ACCOUNT(pn_terminal_id, pn_customer_bank_id, l_date);
         END IF;
         */
         IF l_old_dex NOT IN('D','A') AND pc_dex_data IN('D','A') THEN
             DECLARE
                ln_service_fee_id CORP.SERVICE_FEES.SERVICE_FEE_ID%TYPE;
                lc_copy CHAR(1);         
             BEGIN
                SELECT SERVICE_FEE_ID, DECODE(END_DATE, NULL, 'N', 'Y')
                  INTO ln_service_fee_id, lc_copy
                  FROM (SELECT SERVICE_FEE_ID, END_DATE
                          FROM CORP.SERVICE_FEES
                         WHERE TERMINAL_ID = pn_terminal_id
                           AND FEE_ID = 4
                         ORDER BY NVL(END_DATE, MAX_DATE) DESC, LAST_PAYMENT DESC, SERVICE_FEE_ID DESC)
                 WHERE ROWNUM = 1;
                IF lc_copy = 'Y' THEN
                INSERT INTO CORP.SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID, FEE_AMOUNT, FREQUENCY_ID, LAST_PAYMENT, START_DATE)
                     SELECT CORP.SERVICE_FEE_SEQ.NEXTVAL, pn_terminal_id, 4, FEE_AMOUNT, FREQUENCY_ID, LAST_PAYMENT, GREATEST(l_date, END_DATE) 
                       FROM CORP.SERVICE_FEES
                      WHERE SERVICE_FEE_ID = ln_service_fee_id;
                END IF;          
             EXCEPTION
                WHEN NO_DATA_FOUND THEN
                     INSERT INTO CORP.SERVICE_FEES(SERVICE_FEE_ID, TERMINAL_ID, FEE_ID, FEE_AMOUNT, FREQUENCY_ID, LAST_PAYMENT, START_DATE)
                         SELECT CORP.SERVICE_FEE_SEQ.NEXTVAL, pn_terminal_id, 4, 4, 2, l_date, l_date FROM DUAL;
             END;
         ELSIF pc_dex_data NOT IN('D','A') AND l_old_dex IN('D','A') THEN
              UPDATE CORP.SERVICE_FEES SET END_DATE = l_date WHERE TERMINAL_ID = pn_terminal_id
                  AND FEE_ID = 4;
         END IF;
    EXCEPTION
         WHEN NO_DATA_FOUND THEN
              RAISE_APPLICATION_ERROR(-20100, 'No such terminal found');
         WHEN OTHERS THEN
             ROLLBACK;
             RAISE;
    END;
    
    PROCEDURE CREATE_TERMINAL(
        pn_terminal_id OUT REPORT.TERMINAL.TERMINAL_ID%TYPE,
        pv_device_serial_cd IN REPORT.EPORT.EPORT_SERIAL_NUM%TYPE,
        pn_dealer_id IN CORP.DEALER.DEALER_ID%TYPE,
        pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
        pn_machine_id IN REPORT.TERMINAL.MACHINE_ID%TYPE,
        pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
        pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
        pn_region_id IN REPORT.REGION.REGION_ID%TYPE,
        pv_region_name IN REPORT.REGION.REGION_NAME%TYPE,
        pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
        pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
        pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
        pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
        pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
        pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
        pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
        pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
        pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
        pn_pc_id IN REPORT.TERMINAL.PRIMARY_CONTACT_ID%TYPE,
        pn_sc_id IN REPORT.TERMINAL.SECONDARY_CONTACT_ID%TYPE,
        pn_location_type_id IN REPORT.LOCATION.LOCATION_TYPE_ID%TYPE,
        pv_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
        pn_product_type_id IN REPORT.TERMINAL.PRODUCT_TYPE_ID%TYPE,
        pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
        pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
        pn_avg_amt IN REPORT.TERMINAL.AVG_TRANS_AMT%TYPE,
        pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
        pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
        pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
        pn_vends REPORT.TERMINAL.VENDS%TYPE,
        pv_term_var_1 IN REPORT.TERMINAL.TERM_VAR_1%TYPE,
        pv_term_var_2 IN REPORT.TERMINAL.TERM_VAR_2%TYPE,
        pv_term_var_3 IN REPORT.TERMINAL.TERM_VAR_3%TYPE,
        pv_term_var_4 IN REPORT.TERMINAL.TERM_VAR_4%TYPE,
        pv_term_var_5 IN REPORT.TERMINAL.TERM_VAR_5%TYPE,
        pv_term_var_6 IN REPORT.TERMINAL.TERM_VAR_6%TYPE,
        pv_term_var_7 IN REPORT.TERMINAL.TERM_VAR_7%TYPE,
        pv_term_var_8 IN REPORT.TERMINAL.TERM_VAR_8%TYPE,
		pd_activate_date IN REPORT.TERMINAL_EPORT.START_DATE%TYPE DEFAULT SYSDATE)
    IS
        ln_cnt PLS_INTEGER;
        lv_city REPORT.TERMINAL_ADDR.CITY%TYPE;
        lv_state_cd REPORT.TERMINAL_ADDR.STATE%TYPE;     
        ln_new_region_id REPORT.REGION.REGION_ID%TYPE;        
    BEGIN
        IF pv_state_cd IS NULL OR pv_city IS NULL THEN
            SELECT MAX(CITY), MAX(STATE_CD), MAX(CNT)
              INTO lv_city, lv_state_cd, ln_cnt
              FROM (SELECT P.CITY, S.STATE_CD, 1 CNT
                  FROM FRONT.POSTAL P 
                  JOIN CORP.STATE S ON P.STATE_ID = S.STATE_ID
                WHERE P.POSTAL_CD = pv_postal
                  AND S.COUNTRY_CD = pv_country_cd
                ORDER BY P.POSTAL_ID ASC)
             WHERE ROWNUM = 1;
        ELSE
            SELECT COUNT(*), pv_city, pv_state_cd
              INTO ln_cnt, lv_city, lv_state_cd 
              FROM CORP.STATE 
             WHERE STATE_CD = pv_state_cd
               AND COUNTRY_CD = pv_country_cd; 
        END IF; 
        IF ln_cnt = 0 THEN
            RAISE_APPLICATION_ERROR(-20220, 'Invalid state/country');
            RETURN;
        END IF;
        IF pn_region_id != -1 THEN
            ln_new_region_id := pn_region_id;
        ELSIF TRIM(pv_region_name) IS NULL THEN
            ln_new_region_id := 0;
        ELSE
            REPORT.ADD_REGION(ln_new_region_id, pv_region_name);
        END IF;
        INTERNAL_CREATE_TERMINAL(
            pn_terminal_id,
            pv_device_serial_cd,
            pn_dealer_id,
            pv_asset_nbr,
            pn_machine_id,
            pv_telephone,
            pv_prefix,
            pn_region_id,
            pv_location_name,
            pv_location_details,
            pv_address1,
            lv_city,
            lv_state_cd,
            pv_postal,
            pv_country_cd,
            pn_customer_bank_id,
            pn_pay_sched_id,
            pn_user_id,
            pn_pc_id,
            pn_sc_id,
            pn_location_type_id,
            pv_location_type_specific,
            pn_product_type_id,
            pn_product_type_specific,
            pc_auth_mode,
            pn_avg_amt,
            pn_time_zone_id,
            pc_dex_data,
            pc_receipt,
            pn_vends,
            pv_term_var_1,
            pv_term_var_2,
            pv_term_var_3,
            pv_term_var_4,
            pv_term_var_5,
            pv_term_var_6,
            pv_term_var_7,
            pv_term_var_8,
			pd_activate_date);
    END;

    PROCEDURE UPDATE_TERMINAL(
       pn_terminal_id IN REPORT.TERMINAL.TERMINAL_ID%TYPE,
       pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
       pn_machine_id IN REPORT.TERMINAL.MACHINE_ID%TYPE,
       pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
       pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
       pn_region_id IN REPORT.REGION.REGION_ID%TYPE,
       pv_region_name IN REPORT.REGION.REGION_NAME%TYPE,
       pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
       pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
       pn_address_id REPORT.TERMINAL_ADDR.ADDRESS_ID%TYPE,
       pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
       pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
       pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
       pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
       pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
       pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
       pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
       pn_user_id IN REPORT.USER_LOGIN.USER_ID%TYPE,
       pn_pc_id IN REPORT.TERMINAL.PRIMARY_CONTACT_ID%TYPE,
       pn_sc_id IN REPORT.TERMINAL.SECONDARY_CONTACT_ID%TYPE,
       pn_location_type_id IN REPORT.LOCATION.LOCATION_TYPE_ID%TYPE,
       pn_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
       pn_product_type_id IN REPORT.TERMINAL.PRODUCT_TYPE_ID%TYPE,
       pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
       pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
       pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
       pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
       pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
       pn_vends REPORT.TERMINAL.VENDS%TYPE,
       pv_term_var_1 REPORT.TERMINAL.term_var_1%TYPE,
       pv_term_var_2 REPORT.TERMINAL.term_var_2%TYPE,
       pv_term_var_3 REPORT.TERMINAL.term_var_3%TYPE,
       pv_term_var_4 REPORT.TERMINAL.term_var_4%TYPE,
       pv_term_var_5 REPORT.TERMINAL.term_var_5%TYPE,
       pv_term_var_6 REPORT.TERMINAL.term_var_6%TYPE,
       pv_term_var_7 REPORT.TERMINAL.term_var_7%TYPE,
       pv_term_var_8 REPORT.TERMINAL.term_var_8%TYPE)    
    IS
        ln_cnt PLS_INTEGER;
        lv_city REPORT.TERMINAL_ADDR.CITY%TYPE;
        lv_state_cd REPORT.TERMINAL_ADDR.STATE%TYPE;   
        ln_new_region_id REPORT.REGION.REGION_ID%TYPE;        
    BEGIN
        IF pv_state_cd IS NULL OR pv_city IS NULL THEN
            SELECT MAX(CITY), MAX(STATE_CD), MAX(CNT)
              INTO lv_city, lv_state_cd, ln_cnt
              FROM (SELECT P.CITY, S.STATE_CD, 1 CNT
                  FROM FRONT.POSTAL P 
                  JOIN CORP.STATE S ON P.STATE_ID = S.STATE_ID
                WHERE P.POSTAL_CD = pv_postal
                  AND S.COUNTRY_CD = pv_country_cd
                ORDER BY P.POSTAL_ID ASC)
             WHERE ROWNUM = 1;
        ELSE
            SELECT COUNT(*), pv_city, pv_state_cd
              INTO ln_cnt, lv_city, lv_state_cd
              FROM CORP.STATE 
             WHERE STATE_CD = pv_state_cd
               AND COUNTRY_CD = pv_country_cd; 
        END IF;
        IF ln_cnt = 0 THEN
            RAISE_APPLICATION_ERROR(-20220, 'Invalid state/country');
        ELSE
            IF pn_region_id != -1 THEN
                ln_new_region_id := pn_region_id;
            ELSIF TRIM(pv_region_name) IS NULL THEN
                ln_new_region_id := 0;
            ELSE
                REPORT.ADD_REGION(ln_new_region_id, pv_region_name);
            END IF;
            INTERNAL_UPDATE_TERMINAL(
               pn_terminal_id,
               pv_asset_nbr,
               pn_machine_id,
               pv_telephone,
               pv_prefix,
               pn_region_id,
               pv_location_name,
               pv_location_details,
               pn_address_id,
               pv_address1,
               lv_city,
               lv_state_cd,
               pv_postal,
               pv_country_cd,
               pn_customer_bank_id,
               pn_pay_sched_id,
               pn_user_id,
               pn_pc_id,
               pn_sc_id,
               pn_location_type_id,
               pn_location_type_specific,
               pn_product_type_id,
               pn_product_type_specific,
               pc_auth_mode,
               pn_time_zone_id,
               pc_dex_data,
               pc_receipt,
               pn_vends,
               pv_term_var_1,
               pv_term_var_2,
               pv_term_var_3,
               pv_term_var_4,
               pv_term_var_5,
               pv_term_var_6,
               pv_term_var_7,
               pv_term_var_8);
        END IF;
    END;
        
    PROCEDURE CREATE_TERMINAL_MASS(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
		pn_terminal_id OUT REPORT.TERMINAL.TERMINAL_ID%TYPE,
        pv_device_serial_cd IN REPORT.EPORT.EPORT_SERIAL_NUM%TYPE,
        pn_dealer_id IN CORP.DEALER.DEALER_ID%TYPE,
        pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
        pv_machine_make REPORT.MACHINE.MAKE%TYPE,
		pv_machine_model REPORT.MACHINE.MODEL%TYPE,
        pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
        pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
        pv_region_name IN REPORT.REGION.REGION_NAME%TYPE,
        pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
        pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
        pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
        pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
        pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
        pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
        pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
        pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
        pv_location_type_name REPORT.LOCATION_TYPE.LOCATION_TYPE_NAME%TYPE,
        pn_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
        pv_product_type_name REPORT.PRODUCT_TYPE.PRODUCT_TYPE_NAME%TYPE,
        pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
        pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
        pn_avg_amt IN REPORT.TERMINAL.AVG_TRANS_AMT%TYPE,
        pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
        pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
        pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
        pn_vends REPORT.TERMINAL.VENDS%TYPE,
        pv_term_var_1 REPORT.TERMINAL.term_var_1%TYPE,
        pv_term_var_2 REPORT.TERMINAL.term_var_2%TYPE,
        pv_term_var_3 REPORT.TERMINAL.term_var_3%TYPE,
        pv_term_var_4 REPORT.TERMINAL.term_var_4%TYPE,
        pv_term_var_5 REPORT.TERMINAL.term_var_5%TYPE,
        pv_term_var_6 REPORT.TERMINAL.term_var_6%TYPE,
        pv_term_var_7 REPORT.TERMINAL.term_var_7%TYPE,
        pv_term_var_8 REPORT.TERMINAL.term_var_8%TYPE,
		pd_activate_date IN REPORT.TERMINAL_EPORT.START_DATE%TYPE DEFAULT SYSDATE)
    IS
        ln_machine_id REPORT.MACHINE.MACHINE_ID%TYPE;
        ln_product_type_id REPORT.PRODUCT_TYPE.PRODUCT_TYPE_ID%TYPE;
        ln_location_type_id REPORT.LOCATION_TYPE.LOCATION_TYPE_ID%TYPE;	
        ln_cnt PLS_INTEGER;
        lv_city REPORT.TERMINAL_ADDR.CITY%TYPE;
        lv_state_cd REPORT.TERMINAL_ADDR.STATE%TYPE;     
        ln_region_id REPORT.REGION.REGION_ID%TYPE;        
    BEGIN
        BEGIN
            SELECT MACHINE_ID
              INTO ln_machine_id
              FROM REPORT.MACHINE
             WHERE UPPER(MAKE) = UPPER(pv_machine_make)
               AND UPPER(MODEL) = UPPER(pv_machine_model)
               AND STATUS IN('A','I');
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                REPORT.MACHINE_INS(ln_machine_id, pv_machine_make, pv_machine_model, pn_user_id);
        END;
        BEGIN
            IF pv_location_type_name IS NOT NULL AND LENGTH(TRIM(pv_location_type_name)) > 0 THEN
                SELECT LOCATION_TYPE_ID
                  INTO ln_location_type_id
                  FROM REPORT.LOCATION_TYPE
                 WHERE UPPER(LOCATION_TYPE_NAME) = UPPER(pv_location_type_name)
                   AND STATUS IN('A','I');
            ELSE
                ln_location_type_id := 0;
            END IF;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                RAISE_APPLICATION_ERROR(-20211, 'Invalid location type name');
        END;
        BEGIN
            SELECT PRODUCT_TYPE_ID
              INTO ln_product_type_id
              FROM REPORT.PRODUCT_TYPE
             WHERE UPPER(PRODUCT_TYPE_NAME) = UPPER(pv_product_type_name)
               AND STATUS IN('A','I');
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                RAISE_APPLICATION_ERROR(-20212, 'Invalid product type name');
        END;

         IF pv_state_cd IS NULL OR pv_city IS NULL THEN
            SELECT MAX(CITY), MAX(STATE_CD), MAX(CNT)
              INTO lv_city, lv_state_cd, ln_cnt
              FROM (SELECT P.CITY, S.STATE_CD, 1 CNT
                  FROM FRONT.POSTAL P 
                  JOIN CORP.STATE S ON P.STATE_ID = S.STATE_ID
                WHERE P.POSTAL_CD = pv_postal
                  AND S.COUNTRY_CD = pv_country_cd
                ORDER BY P.POSTAL_ID ASC)
             WHERE ROWNUM = 1;
        ELSE
            SELECT COUNT(*), pv_city, pv_state_cd
              INTO ln_cnt, lv_city, lv_state_cd 
              FROM CORP.STATE 
             WHERE STATE_CD = pv_state_cd
               AND COUNTRY_CD = pv_country_cd; 
        END IF; 
        IF ln_cnt = 0 THEN
            RAISE_APPLICATION_ERROR(-20220, 'Invalid state/country');
            RETURN;
        END IF;
        IF TRIM(pv_region_name) IS NOT NULL THEN
            REPORT.ADD_REGION(ln_region_id, pv_region_name);
        ELSE
            ln_region_id := 0;
        END IF;
        INTERNAL_CREATE_TERMINAL(
               pn_terminal_id,
               pv_device_serial_cd,
               pn_dealer_id,
               pv_asset_nbr,
               ln_machine_id,
               pv_telephone,
               pv_prefix,
               ln_region_id,
               pv_location_name,
               pv_location_details,
               pv_address1,
               lv_city,
               lv_state_cd,
               pv_postal,
               pv_country_cd,
               pn_customer_bank_id,
               pn_pay_sched_id,
               pn_user_id,
               pn_user_id,
               NULL,
               ln_location_type_id,
               pn_location_type_specific,
               ln_product_type_id,
               pn_product_type_specific,
               pc_auth_mode,
               pn_avg_amt,
               pn_time_zone_id,
               pc_dex_data,
               pc_receipt,
               pn_vends,
               pv_term_var_1,
               pv_term_var_2,
               pv_term_var_3,
               pv_term_var_4,
               pv_term_var_5,
               pv_term_var_6,
               pv_term_var_7,
               pv_term_var_8,
			   pd_activate_date);
    END;
    
    PROCEDURE UPDATE_TERMINAL_MASS(
        pn_user_id REPORT.USER_LOGIN.USER_ID%TYPE,
		pn_terminal_id IN REPORT.TERMINAL.TERMINAL_ID%TYPE,
        pv_asset_nbr IN REPORT.TERMINAL.ASSET_NBR%TYPE,
        pv_machine_make REPORT.MACHINE.MAKE%TYPE,
		pv_machine_model REPORT.MACHINE.MODEL%TYPE,
        pv_telephone IN REPORT.TERMINAL.TELEPHONE%TYPE,
        pv_prefix IN REPORT.TERMINAL.PREFIX%TYPE,
        pv_region_name IN REPORT.REGION.REGION_NAME%TYPE,
        pv_location_name IN REPORT.LOCATION.LOCATION_NAME%TYPE,
        pv_location_details IN REPORT.LOCATION.DESCRIPTION%TYPE,
        pv_address1 IN REPORT.TERMINAL_ADDR.ADDRESS1%TYPE,
        pv_city IN REPORT.TERMINAL_ADDR.CITY%TYPE,
        pv_state_cd IN REPORT.TERMINAL_ADDR.STATE%TYPE,
        pv_postal IN REPORT.TERMINAL_ADDR.ZIP%TYPE,
        pv_country_cd IN REPORT.TERMINAL_ADDR.COUNTRY_CD%TYPE,
        pn_customer_bank_id IN REPORT.USER_CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        pn_pay_sched_id IN REPORT.TERMINAL.PAYMENT_SCHEDULE_ID%TYPE,
        pn_location_type_id REPORT.LOCATION_TYPE.LOCATION_TYPE_ID%TYPE,
		pv_location_type_name REPORT.LOCATION_TYPE.LOCATION_TYPE_NAME%TYPE,
        pn_location_type_specific IN REPORT.LOCATION.LOCATION_TYPE_SPECIFY%TYPE,
        pv_product_type_name REPORT.PRODUCT_TYPE.PRODUCT_TYPE_NAME%TYPE,
        pn_product_type_specific IN REPORT.TERMINAL.PRODUCT_TYPE_SPECIFY%TYPE,
        pc_auth_mode IN REPORT.TERMINAL.AUTHORIZATION_MODE%TYPE,
        pn_time_zone_id IN REPORT.TERMINAL.TIME_ZONE_ID%TYPE,
        pc_dex_data IN REPORT.TERMINAL.DEX_DATA%TYPE,
        pc_receipt REPORT.TERMINAL.RECEIPT%TYPE,
        pn_vends REPORT.TERMINAL.VENDS%TYPE,
        pv_term_var_1 REPORT.TERMINAL.term_var_1%TYPE,
        pv_term_var_2 REPORT.TERMINAL.term_var_2%TYPE,
        pv_term_var_3 REPORT.TERMINAL.term_var_3%TYPE,
        pv_term_var_4 REPORT.TERMINAL.term_var_4%TYPE,
        pv_term_var_5 REPORT.TERMINAL.term_var_5%TYPE,
        pv_term_var_6 REPORT.TERMINAL.term_var_6%TYPE,
        pv_term_var_7 REPORT.TERMINAL.term_var_7%TYPE,
        pv_term_var_8 REPORT.TERMINAL.term_var_8%TYPE)
    IS
        ln_machine_id REPORT.MACHINE.MACHINE_ID%TYPE;
        ln_product_type_id REPORT.PRODUCT_TYPE.PRODUCT_TYPE_ID%TYPE;
        ln_location_type_id REPORT.LOCATION_TYPE.LOCATION_TYPE_ID%TYPE;	
        ln_cnt PLS_INTEGER;
        lv_city REPORT.TERMINAL_ADDR.CITY%TYPE;
        lv_state_cd REPORT.TERMINAL_ADDR.STATE%TYPE;  
        ln_region_id REPORT.REGION.REGION_ID%TYPE;        
    BEGIN
        BEGIN
            SELECT MACHINE_ID
              INTO ln_machine_id
              FROM REPORT.MACHINE
             WHERE UPPER(MAKE) = UPPER(pv_machine_make)
               AND UPPER(MODEL) = UPPER(pv_machine_model)
               AND STATUS IN('A','I');
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                REPORT.MACHINE_INS(ln_machine_id, pv_machine_make, pv_machine_model, pn_user_id);
        END;
        BEGIN
            IF pv_location_type_name IS NOT NULL AND LENGTH(TRIM(pv_location_type_name)) > 0 THEN
                SELECT LOCATION_TYPE_ID
                  INTO ln_location_type_id
                  FROM REPORT.LOCATION_TYPE
                 WHERE UPPER(LOCATION_TYPE_NAME) = UPPER(pv_location_type_name)
                   AND STATUS IN('A','I');
            ELSIF pn_location_type_id IS NULL THEN
                ln_location_type_id := 0;
            ELSE
                ln_location_type_id := pn_location_type_id;
            END IF;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                RAISE_APPLICATION_ERROR(-20211, 'Invalid location type name');
        END;
        BEGIN
            SELECT PRODUCT_TYPE_ID
              INTO ln_product_type_id
              FROM REPORT.PRODUCT_TYPE
             WHERE UPPER(PRODUCT_TYPE_NAME) = UPPER(pv_product_type_name)
               AND STATUS IN('A','I');
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                RAISE_APPLICATION_ERROR(-20212, 'Invalid product type name');
        END;

         IF pv_state_cd IS NULL OR pv_city IS NULL THEN
            SELECT MAX(CITY), MAX(STATE_CD), MAX(CNT)
              INTO lv_city, lv_state_cd, ln_cnt
              FROM (SELECT P.CITY, S.STATE_CD, 1 CNT
                  FROM FRONT.POSTAL P 
                  JOIN CORP.STATE S ON P.STATE_ID = S.STATE_ID
                WHERE P.POSTAL_CD = pv_postal
                  AND S.COUNTRY_CD = pv_country_cd
                ORDER BY P.POSTAL_ID ASC)
             WHERE ROWNUM = 1;
        ELSE
            SELECT COUNT(*), pv_city, pv_state_cd
              INTO ln_cnt, lv_city, lv_state_cd
              FROM CORP.STATE 
             WHERE STATE_CD = pv_state_cd
               AND COUNTRY_CD = pv_country_cd; 
        END IF; 
        IF ln_cnt = 0 THEN
            RAISE_APPLICATION_ERROR(-20220, 'Invalid state/country');
            RETURN;
        END IF;
        IF TRIM(pv_region_name) IS NOT NULL THEN
            REPORT.ADD_REGION(ln_region_id, pv_region_name);
        ELSE
            ln_region_id := 0;
        END IF;
        INTERNAL_UPDATE_TERMINAL(
               pn_terminal_id,
               pv_asset_nbr,
               ln_machine_id,
               pv_telephone,
               pv_prefix,
               ln_region_id,
               pv_location_name,
               pv_location_details,
               0,  /* address id (0=create a new address)*/
               pv_address1,
               lv_city,
               lv_state_cd,
               pv_postal,
               pv_country_cd,
               pn_customer_bank_id,
               pn_pay_sched_id,
               pn_user_id,
               pn_user_id,
               NULL,
               ln_location_type_id,
               pn_location_type_specific,
               ln_product_type_id,
               pn_product_type_specific,
               pc_auth_mode,
               pn_time_zone_id,
               pc_dex_data,
               pc_receipt,
               pn_vends,
               pv_term_var_1,
               pv_term_var_2,
               pv_term_var_3,
               pv_term_var_4,
               pv_term_var_5,
               pv_term_var_6,
               pv_term_var_7,
               pv_term_var_8);
    END;

    PROCEDURE CREATE_CUSTOMER_BANK(
        pn_customer_bank_id OUT CORP.CUSTOMER_BANK.CUSTOMER_BANK_ID%TYPE,
        pn_customer_id IN CORP.CUSTOMER_BANK.CUSTOMER_ID%TYPE,
        pn_user_id IN CORP.CUSTOMER.USER_ID%TYPE,
        pv_bank_name IN CORP.CUSTOMER_BANK.BANK_NAME%TYPE,
        pv_bank_address1 IN CORP.CUSTOMER_BANK.BANK_ADDRESS%TYPE,
        pv_bank_city IN CORP.CUSTOMER_BANK.BANK_CITY%TYPE,
        pn_bank_state_id IN CORP.STATE.STATE_ID%TYPE,
        pv_bank_zip IN CORP.CUSTOMER_BANK.BANK_ZIP%TYPE,
        pv_account_title IN CORP.CUSTOMER_BANK.ACCOUNT_TITLE%TYPE,
        pv_account_type IN CORP.CUSTOMER_BANK.ACCOUNT_TYPE%TYPE,
        pv_bank_routing_nbr IN CORP.CUSTOMER_BANK.BANK_ROUTING_NBR%TYPE,
        pv_bank_acct_nbr IN CORP.CUSTOMER_BANK.BANK_ACCT_NBR%TYPE,
        pv_contact_name IN CORP.CUSTOMER_BANK.CONTACT_NAME%TYPE,
        pv_contact_title IN CORP.CUSTOMER_BANK.CONTACT_TITLE%TYPE,
        pv_contact_telephone IN CORP.CUSTOMER_BANK.CONTACT_TELEPHONE%TYPE,
        pv_contact_fax IN CORP.CUSTOMER_BANK.CONTACT_FAX%TYPE)
    IS
        lv_state_cd CORP.STATE.STATE_CD%TYPE;
        lv_country_cd CORP.COUNTRY.COUNTRY_CD%TYPE;     
    BEGIN
        SELECT STATE_CD, COUNTRY_CD 
          INTO lv_state_cd, lv_country_cd 
          FROM CORP.STATE 
         WHERE STATE_ID = pn_bank_state_id;
        SELECT CORP.CUSTOMER_BANK_SEQ.NEXTVAL INTO pn_customer_bank_id FROM DUAL;
        INSERT INTO CORP.CUSTOMER_BANK(CUSTOMER_BANK_ID, CUSTOMER_ID, BANK_NAME, BANK_ADDRESS, BANK_CITY, BANK_STATE, BANK_ZIP, BANK_COUNTRY_CD, CREATE_BY,
            ACCOUNT_TITLE, ACCOUNT_TYPE, BANK_ROUTING_NBR,	BANK_ACCT_NBR, CONTACT_NAME, CONTACT_TITLE, CONTACT_TELEPHONE, CONTACT_FAX)
            VALUES(pn_customer_bank_id, pn_customer_id, pv_bank_name, pv_bank_address1, pv_bank_city, lv_state_cd, pv_bank_zip, lv_country_cd, pn_user_id,
                   pv_account_title, pv_account_type, pv_bank_routing_nbr, pv_bank_acct_nbr, pv_contact_name, pv_contact_title, pv_contact_telephone, pv_contact_fax);
        INSERT INTO CORP.USER_CUSTOMER_BANK (CUSTOMER_BANK_ID, USER_ID, ALLOW_EDIT)
            VALUES(pn_customer_bank_id, pn_user_id, 'Y');
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RAISE_APPLICATION_ERROR(-20220, 'Invalid state/country');  
    END;
    
    PROCEDURE UPDATE_REGION_NAME(
        pn_region_id REPORT.REGION.REGION_ID%TYPE,
        pv_region_name REPORT.REGION.REGION_NAME%TYPE,
        pn_customer_id REPORT.TERMINAL.CUSTOMER_ID%TYPE)
    IS
        ln_new_region_id REPORT.REGION.REGION_ID%TYPE;
    BEGIN
        IF pv_region_name IS NULL THEN
            DELETE FROM REPORT.TERMINAL_REGION
             WHERE REGION_ID = pn_region_id
               AND TERMINAL_ID IN(SELECT TERMINAL_ID FROM REPORT.TERMINAL WHERE CUSTOMER_ID = pn_customer_id);
        ELSE
            REPORT.ADD_REGION(ln_new_region_id, pv_region_name);       
            IF pn_region_id = 0 THEN
                INSERT INTO REPORT.TERMINAL_REGION(TERMINAL_ID, REGION_ID)
                  SELECT T.TERMINAL_ID, ln_new_region_id
                    FROM REPORT.TERMINAL T
                   WHERE T.CUSTOMER_ID = pn_customer_id
                     AND NOT EXISTS(SELECT 1 FROM REPORT.TERMINAL_REGION TR WHERE T.TERMINAL_ID = TR.TERMINAL_ID);
            ELSE
                UPDATE REPORT.TERMINAL_REGION
                   SET REGION_ID = ln_new_region_id
                 WHERE REGION_ID = pn_region_id
                   AND TERMINAL_ID IN(SELECT TERMINAL_ID FROM REPORT.TERMINAL WHERE CUSTOMER_ID = pn_customer_id);
            END IF;
        END IF;
    END;        
END;
/
-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/DMS/DMS-1.0.4-USARDB.sql?rev=HEAD
GRANT SELECT ON CORP.COUNTRY TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.STATE TO USAT_DMS_ROLE;
GRANT SELECT ON FRONT.POSTAL TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.LOCATION_TYPE TO USAT_DMS_ROLE;
GRANT EXECUTE ON REPORT.PKG_CUSTOMER_MANAGEMENT TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.PRODUCT_TYPE TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.TIME_ZONE TO USAT_DMS_ROLE;

