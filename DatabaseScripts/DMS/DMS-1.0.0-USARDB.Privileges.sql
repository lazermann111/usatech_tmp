GRANT SELECT ON CORP.business_unit                             TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.currency                                  TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.customer                                  TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.customer_addr                             TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.customer_bank                             TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.customer_bank_terminal                    TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.dealer                                    TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.payment_schedule                          TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.terminal                                  TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.vw_current_bank_acct                      TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.vw_customer                               TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.vw_user                                   TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.vw_current_license                        TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.license_nbr                               TO USAT_DMS_ROLE;

GRANT SELECT ON REPORT.eport                                   TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.location                                TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.terminal                                TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.terminal_column_map                     TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.terminal_changes                        TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.terminal_eport                          TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.trans                                   TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.vw_new_terminals                        TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.vw_activity                             TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.VW_TERMINAL_COLUMN_MAP_COUNT            TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.vw_terminal_eport                       TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.vw_current_device_location              TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.VW_CURRENT_DEVICE_LOCATION			   TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.VW_UNASSIGNED_EPORTS			           TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.vw_terminal         			           TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.region         			               TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.terminal_region         			       TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.machine         			               TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.USER_LOGIN      			               TO USAT_DMS_ROLE;

GRANT SELECT ON CORP.vw_license                                TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.license                                   TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.dealer_license                            TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.vw_changed_customer_bank                  TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.vw_approved_payments                      TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.VW_PENDING_OR_LOCKED_PAYMENTS             TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.pay_cycle                                 TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.vw_no_license                             TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.vw_adjustments                            TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.vw_payment_process_fees                   TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.batch                                     TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.ledger                                    TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.vw_payment_service_fees                   TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.doc                                       TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.DEALER_EPORT                              TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.PROCESS_FEES                              TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.SERVICE_FEES                              TO USAT_DMS_ROLE;

GRANT INSERT ON CORP.customer_addr                             TO USAT_DMS_ROLE;
GRANT UPDATE ON CORP.customer_addr                             TO USAT_DMS_ROLE;

GRANT DELETE ON REPORT.TERMINAL_COLUMN_MAP                        TO USAT_DMS_ROLE;

GRANT EXECUTE ON CORP.accept_customer                          TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.customer_del                             TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.customer_upd                             TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.terminal_reassign_bank_account           TO USAT_DMS_ROLE;

GRANT EXECUTE ON REPORT.accept_new_terminal                    TO USAT_DMS_ROLE;
GRANT EXECUTE ON REPORT.accept_terminal_changes                TO USAT_DMS_ROLE;
GRANT EXECUTE ON REPORT.terminal_eport_upd                     TO USAT_DMS_ROLE;
GRANT EXECUTE ON REPORT.terminal_column_map_del                TO USAT_DMS_ROLE;
GRANT EXECUTE ON REPORT.terminal_column_map_ins                TO USAT_DMS_ROLE;
GRANT EXECUTE ON REPORT.TERMINAL_COLUMN_MAP_COPY_MODEL         TO USAT_DMS_ROLE;
GRANT EXECUTE ON REPORT.get_eports                             TO USAT_DMS_ROLE;

GRANT EXECUTE ON CORP.dealer_ins                               TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.dealer_upd                               TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.dealer_del                               TO USAT_DMS_ROLE;

GRANT EXECUTE ON CORP.license_ins                               TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.license_upd                               TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.license_del                               TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.accept_customer_bank                      TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.payments_pkg                              TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.dealer_license_ins                        TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.dealer_license_del                        TO USAT_DMS_ROLE;

GRANT EXECUTE ON CORP.customer_bank_del                         TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.customer_bank_ins                         TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.customer_bank_upd                         TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.received_custom_license                   TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.received_license                          TO USAT_DMS_ROLE;
GRANT EXECUTE ON report.GET_OR_CREATE_EPORT                     TO USAT_DMS_ROLE;
GRANT EXECUTE ON corp.DEALER_EPORT_UPD                        	TO USAT_DMS_ROLE;

GRANT SELECT ON CORP.business_unit                             TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.currency                                  TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.customer                                  TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.customer_bank                             TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.customer_bank_terminal                    TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.payment_schedule                          TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.device_type                             TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.eport                                   TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.location                                TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.terminal                                TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.terminal_changes                        TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.terminal_eport                          TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.vw_new_terminals                        TO USAT_DMS_ROLE;

GRANT EXECUTE ON REPORT.accept_new_terminal                    TO USAT_DMS_ROLE;
GRANT EXECUTE ON REPORT.accept_terminal_changes                TO USAT_DMS_ROLE;

GRANT SELECT ON CORP.vw_current_bank_acct                      TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.trans                                   TO USAT_DMS_ROLE;

GRANT UPDATE ON REPORT.terminal                                TO USAT_DMS_ROLE;

GRANT EXECUTE ON CORP.terminal_reassign_bank_account           TO USAT_DMS_ROLE;
GRANT EXECUTE ON REPORT.terminal_eport_upd                     TO USAT_DMS_ROLE;

GRANT SELECT ON CORP.customer_addr                             TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.customer_addr_seq                         TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.dealer                                    TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.license_nbr                               TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.terminal                                  TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.vw_current_license                        TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.vw_customer                               TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.vw_license                                TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.vw_user                                   TO USAT_DMS_ROLE;

GRANT INSERT ON CORP.customer_addr                             TO USAT_DMS_ROLE;

GRANT UPDATE ON CORP.customer                                  TO USAT_DMS_ROLE;
GRANT UPDATE ON CORP.customer_addr                             TO USAT_DMS_ROLE;

GRANT EXECUTE ON CORP.accept_customer                          TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.customer_del                             TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.customer_upd                             TO USAT_DMS_ROLE;

GRANT SELECT ON CORP.vw_current_bank_acct                      TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.trans                                   TO USAT_DMS_ROLE;

-- UPDATE PRIVILEGES
GRANT UPDATE ON REPORT.terminal                                TO USAT_DMS_ROLE;

-- EXECUTE PRIVILEGES
GRANT EXECUTE ON CORP.terminal_reassign_bank_account           TO USAT_DMS_ROLE;
GRANT EXECUTE ON REPORT.terminal_eport_upd                     TO USAT_DMS_ROLE;


--------------------------------------------------------------------------------
-- Grants for "Customer Management"

-- SELECT PRIVILEGES
GRANT SELECT ON CORP.customer_addr                             TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.customer_addr_seq                         TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.dealer                                    TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.license_nbr                               TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.terminal                                  TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.vw_current_license                        TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.vw_customer                               TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.vw_license                                TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.license                                   TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.license_process_fees                      TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.license_service_fees                      TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.FEES                                      TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.LICENSE_SERVICE_FEES                      TO USAT_DMS_ROLE;
GRANT SELECT ON CORP.vw_user                                   TO USAT_DMS_ROLE;
GRANT SELECT ON REPORT.trans_type                              TO USAT_DMS_ROLE;

GRANT INSERT ON CORP.customer_addr                             TO USAT_DMS_ROLE;

GRANT UPDATE ON CORP.customer                                  TO USAT_DMS_ROLE;
GRANT UPDATE ON CORP.customer_addr                             TO USAT_DMS_ROLE;

GRANT EXECUTE ON CORP.accept_customer                          TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.customer_del                             TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.customer_upd                             TO USAT_DMS_ROLE;

GRANT EXECUTE ON CORP.LICENSE_DEL                              TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.LICENSE_INSERT                           TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.LICENSE_UPDATE                           TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.LICENSE_PF_UPDATE                        TO USAT_DMS_ROLE;
GRANT EXECUTE ON CORP.LICENSE_SF_UPDATE                        TO USAT_DMS_ROLE;
