declare 
count_rows NUMBER;
procedure
  ins_device_setting_parameter (P_DEVICE_SETTING_PARAMETER_CD IN VARCHAR2,
    P_DEVICE_SETTING_UI_CONFIGURAB IN CHAR,
    P_DEVICE_SETTING_PARAMETER_NAM IN VARCHAR2)
   
  is
  begin
    
    select count(1) into count_rows from DEVICE.device_setting_parameter where device_setting_parameter_cd = P_DEVICE_SETTING_PARAMETER_CD;
    
    if count_rows <= 0 Then
        --dbms_output.put_line('### INSERTING! PARAMETER_CD='||P_DEVICE_SETTING_PARAMETER_CD);
        insert into device.device_setting_parameter      
        (DEVICE_SETTING_PARAMETER_CD,
        DEVICE_SETTING_UI_CONFIGURABLE,
        DEVICE_SETTING_PARAMETER_NAME)
        values(P_DEVICE_SETTING_PARAMETER_CD,
        P_DEVICE_SETTING_UI_CONFIGURAB,
        P_DEVICE_SETTING_PARAMETER_NAM);
      end if;
  end;
  begin
  ins_device_setting_parameter('Terminal Location','Y','Not Used');
  ins_device_setting_parameter('Display Line 1', 'Y','Display Line 1');
  ins_device_setting_parameter('Unused Splash Display Line 1', 'Y','Unused Splash Display Line 1');
  ins_device_setting_parameter('Display Line 2', 'Y','Display Line 2');
  ins_device_setting_parameter('Unused Splash Display Line 2', 'Y','Unused Splash Display Line 2');
  ins_device_setting_parameter('Machine ID', 'Y','<font color=#B78B16>PROT</font>: Machine ID');
  ins_device_setting_parameter('Network Format Select', 'Y','Network Format Select');
  ins_device_setting_parameter('International Dialing Prefix or IP Address', 'Y','<font color=red>COM</font>: International Dialing Prefix or IP Address');
  ins_device_setting_parameter('Phone Number or Port Number', 'Y','<font color=red>COM</font>: Phone Number or Port Number');
  ins_device_setting_parameter('USALive Phone In Time', 'Y','USALive Phone In Time');
  ins_device_setting_parameter('USALive Day-Cycle Interval', 'Y','USALive Day-Cycle Interval');
  ins_device_setting_parameter('USALive Retry Interval', 'Y','USALive Retry Interval');
  ins_device_setting_parameter('Device Name', 'Y','<font color=#B78B16>PROT</font>: Device Name');
  ins_device_setting_parameter('MDB Interval Speed Setting', 'Y','MDB Interval Speed Setting');
  ins_device_setting_parameter('MDB Response Setting', 'Y','MDB Response Setting');
  ins_device_setting_parameter('Transaction Auto Time-Out', 'Y','Transaction Auto Time-Out');
  ins_device_setting_parameter('After Use Auto Time-Out', 'Y','After Use Auto Time-Out');
  ins_device_setting_parameter('Single Vend Mode', 'Y','Single Vend Mode');
  ins_device_setting_parameter('Capture Currency Transactions', 'Y','Capture Currency Transactions');
  ins_device_setting_parameter('Authorization Selection Flag', 'Y','Authorization Selection Flag');
  ins_device_setting_parameter('Local Authorization Hours To Clear', 'Y','Local Authorization Hours To Clear');
  ins_device_setting_parameter('Local Authorization Warning Frequency', 'Y','Local Authorization Warning Frequency');
  ins_device_setting_parameter('Local Authorization Stop Frequency', 'Y','Local Authorization Stop Frequency');
  ins_device_setting_parameter('Card Authorization Amount', 'Y','Card Authorization Amount');
  ins_device_setting_parameter('Accelerated Follow Behind Settlement', 'Y','Accelerated Follow Behind Settlement');
  ins_device_setting_parameter('Maximum Vend Items Per Transaction', 'Y','Maximum Vend Items Per Transaction');
  ins_device_setting_parameter('Auth Special No Prefix Cards With SNET', 'Y','Auth Special No Prefix Cards With SNET');
  ins_device_setting_parameter('Convenience Fee', 'Y','Convenience Fee');
  ins_device_setting_parameter('American Express Mask 1', 'Y','American Express Mask');
  ins_device_setting_parameter('American Express Mask', 'Y','American Express Mask');
  ins_device_setting_parameter('American Express Mask 2', 'Y','American Express Mask');
  ins_device_setting_parameter('Visa Mask', 'Y','Visa Mask');
  ins_device_setting_parameter('Mastercard Mask', 'Y','Mastercard Mask');
  ins_device_setting_parameter('Mastercard Mask 1', 'Y','Mastercard Mask');
  ins_device_setting_parameter('Mastercard Mask 2', 'Y','Mastercard Mask');
  ins_device_setting_parameter('Mastercard Mask 3', 'Y','Mastercard Mask');
  ins_device_setting_parameter('Mastercard Mask 4', 'Y','Mastercard Mask');
  ins_device_setting_parameter('Mastercard Mask 5', 'Y','Mastercard Mask');
  ins_device_setting_parameter('Discover Card Mask', 'Y','Discover Card Mask');
  ins_device_setting_parameter('Service or View Totals Card Mask', 'Y','Service or View Totals Card Mask');
  ins_device_setting_parameter('Print Receipt Flag', 'Y','Print Receipt Flag');
  ins_device_setting_parameter('Print Destination Select', 'Y','Print Destination Select');
  ins_device_setting_parameter('Prompt for receipt print', 'Y','Prompt for receipt print');
  ins_device_setting_parameter('Type Of Card Data On Receipt', 'Y','Type Of Card Data On Receipt');
  ins_device_setting_parameter('Receipt Phone Number', 'Y','Receipt Phone Number');
  ins_device_setting_parameter('Poll DEX Flag', 'Y','Poll DEX Flag');
  ins_device_setting_parameter('DEX Hour Interval', 'Y','DEX Hour Interval');
  ins_device_setting_parameter('DEX Transaction Interval', 'Y','DEX Transaction Interval');
  ins_device_setting_parameter('DEX Query Mode Flag', 'Y','DEX Query Mode Flag');
  ins_device_setting_parameter('DEX Query Interval Minutes', 'Y','DEX Query Interval Minutes');
  ins_device_setting_parameter('DEX Poll and Plugin Attempts', 'Y','DEX Poll and Plugin Attempts');
  ins_device_setting_parameter('DEX Fill Button Action', 'Y','DEX Fill Button Action');
  ins_device_setting_parameter('DEX Plug-In Attempts', 'Y','DEX Plug-In Attempts');
  ins_device_setting_parameter('DEX Poll Attempts', 'Y','DEX Poll Attempts');
  ins_device_setting_parameter('Special Vend Character', 'Y','Special Vend Character');
  ins_device_setting_parameter('MDB Inventory Hex or BCD Format', 'Y','MDB Inventory Hex or BCD Format');
  ins_device_setting_parameter('Pass Card / Free Vend Print Flag', 'Y','Pass Card / Free Vend Print Flag');
  ins_device_setting_parameter('Credit Batch Location', 'Y','Credit Batch Location');
  ins_device_setting_parameter('Merchant ID in G4; IP Addres in MEI', 'Y','Merchant ID in G4; IP Addres in MEI');
  ins_device_setting_parameter('Merchant ID Prefix Link 0 of 9', 'Y','Merchant ID Prefix Link 0 of 9');
  ins_device_setting_parameter('Merchant ID Prefix Link 1 of 9', 'Y','Merchant ID Prefix Link 1 of 9');
  ins_device_setting_parameter('Merchant ID Prefix Link 2 of 9', 'Y','Merchant ID Prefix Link 2 of 9');
  ins_device_setting_parameter('Merchant ID Prefix Link 3 of 9', 'Y','Merchant ID Prefix Link 3 of 9');
  ins_device_setting_parameter('Merchant ID Prefix Link 4 of 9', 'Y','Merchant ID Prefix Link 4 of 9');
  ins_device_setting_parameter('Merchant ID Prefix Link 5 of 9', 'Y','Merchant ID Prefix Link 5 of 9');
  ins_device_setting_parameter('Merchant ID Prefix Link 6 of 9', 'Y','Merchant ID Prefix Link 6 of 9');
  ins_device_setting_parameter('Merchant ID Prefix Link 7 of 9', 'Y','Merchant ID Prefix Link 7 of 9');
  ins_device_setting_parameter('Merchant ID Prefix Link 8 of 9', 'Y','Merchant ID Prefix Link 8 of 9');
  ins_device_setting_parameter('Merchant ID Prefix Link 9 of 9', 'Y','Merchant ID Prefix Link 9 of 9');
  ins_device_setting_parameter('Primary Credit Bureau Phone Number', 'Y','Primary Credit Bureau Phone Number');
  ins_device_setting_parameter('Encryption Key', 'Y','<font color=#B78B16>PROT</font>: Encryption Key');
  ins_device_setting_parameter('Not Used', 'Y','Not Used');
  ins_device_setting_parameter('Not Used [single byte]', 'Y','Not Used');
  ins_device_setting_parameter('Tone Or Pulse Dial Flag', 'Y','Tone Or Pulse Dial Flag');
  ins_device_setting_parameter('Pass/Debit Receipt Print', 'Y','Pass/Debit Receipt Print');
  ins_device_setting_parameter('$ per count printer 2 above up to setting', 'Y','$ per count printer 2 above up to setting');
  ins_device_setting_parameter('Print Buffer Clear String', 'Y','Print Buffer Clear String');
  ins_device_setting_parameter('Total Counter 1 to Date', 'Y','Total Counter 1 to Date');
  ins_device_setting_parameter('Total Since Last Call In', 'Y','Total Since Last Call In');
  ins_device_setting_parameter('Driver maint card counter clear delay', 'Y','Driver maint card counter clear delay');
  ins_device_setting_parameter('Driver maint card counter display time', 'Y','Driver maint card counter display time');
  ins_device_setting_parameter('Modem Poll Rate', 'Y','Modem Poll Rate');
  ins_device_setting_parameter('Device Personality Type', 'Y','Device Personality Type');
  ins_device_setting_parameter('Total Transaction Records Before Call', 'Y','Total Transaction Records Before Call');
  ins_device_setting_parameter('USALive Retry Attempts', 'Y','USALive Retry Attempts');
  ins_device_setting_parameter('Dial Pause Flag', 'Y','Dial Pause Flag');
  ins_device_setting_parameter('IN or OUT of Service Flag', 'Y','IN or OUT of Service Flag');
  ins_device_setting_parameter('Terminal ID', 'Y','Terminal ID');
  ins_device_setting_parameter('Day Before Terminal OUT Of Service', 'Y','Day Before Terminal OUT Of Service');
  ins_device_setting_parameter('Init Printer String', 'Y','Init Printer String');
  ins_device_setting_parameter('Initialization Complete Flag', 'Y','Initialization Complete Flag');
  ins_device_setting_parameter('Allow Machine ID Change', 'Y','Allow Machine ID Change');
  ins_device_setting_parameter('Call Home Now Flag', 'Y','Call Home Now Flag');
  ins_device_setting_parameter('Clear Main Memory On Power UP Flag', 'Y','Clear Main Memory On Power UP Flag');
  ins_device_setting_parameter('Hours Between Transactions', 'Y','Hours Between Transactions');
  ins_device_setting_parameter('Terminal ID 2 or Port Number', 'Y','Terminal ID 2 or Port Number');
  ins_device_setting_parameter('Terminal ID 3', 'Y','Terminal ID 3');
  ins_device_setting_parameter('Minimum Batch Size', 'Y','Minimum Batch Size');
  ins_device_setting_parameter('Maximum Counter #1 before Auto-End', 'Y','Maximum Counter #1 before Auto-End');
  ins_device_setting_parameter('Receipt Time Out Seconds', 'Y','Receipt Time Out Seconds');
  ins_device_setting_parameter('Up to printer 1 count', 'Y','Up to printer 1 count');
  ins_device_setting_parameter('$ per count printer 1 above up to setting', 'Y','$ per count printer 1 above up to setting');
  ins_device_setting_parameter('Up to printer 2 count', 'Y','Up to printer 2 count');
  ins_device_setting_parameter('Allow Remote Terminal Polling', 'Y','Allow Remote Terminal Polling');
  ins_device_setting_parameter('External Modem Init String', 'Y','External Modem Init String');
  ins_device_setting_parameter('Service 2 Phone Number', 'Y','Service 2 Phone Number');
  ins_device_setting_parameter('Service 3 Phone Number', 'Y','Service 3 Phone Number');
  ins_device_setting_parameter('Service 4 Phone Number', 'Y','Service 4 Phone Number');
  ins_device_setting_parameter('Service 5 Phone Number', 'Y','Service 5 Phone Number');
  ins_device_setting_parameter('Service 6 Phone Number', 'Y','Service 6 Phone Number');
  ins_device_setting_parameter('Total Currency Transaction Counter', 'Y','<font color=blue>COUNTER</font>: Total Currency Transaction Counter');
  ins_device_setting_parameter('Total Currency Money Counter', 'Y','<font color=blue>COUNTER</font>: Total Currency Money Counter');
  ins_device_setting_parameter('Total Cashless Transaction Counter', 'Y','<font color=blue>COUNTER</font>: Total Cashless Transaction Counter');
  ins_device_setting_parameter('Total Cashless Money Counter', 'Y','<font color=blue>COUNTER</font>: Total Cashless Money Counter');
  ins_device_setting_parameter('Total Pass Card Transaction Counter', 'Y','<font color=blue>COUNTER</font>: Total Pass Card Transaction Counter');
  ins_device_setting_parameter('Total Pass Card Money Counter', 'Y','<font color=blue>COUNTER</font>: Total Pass Card Money Counter');
  ins_device_setting_parameter('Total Stitch Sessions Attepted Counter', 'Y','<font color=blue>COUNTER</font>: Total Stitch Sessions Attepted Counter');
  ins_device_setting_parameter('Total Stitch Transaction Counter', 'Y','<font color=blue>COUNTER</font>: Total Stitch Transaction Counter');
  ins_device_setting_parameter('Total Stitch Bytes Counter', 'Y','<font color=blue>COUNTER</font>: Total Stitch Bytes Counter');
  ins_device_setting_parameter('Coin Pulse Value', 'Y','Coin Pulse Value');
  ins_device_setting_parameter('Pulse Level Attributes', 'Y','Pulse Level Attributes');
  ins_device_setting_parameter('Vendor Interface Type', 'Y','Vendor Interface Type');
  ins_device_setting_parameter('Pulse Duration', 'Y','Pulse Duration');
  ins_device_setting_parameter('Pulse Spacing', 'Y','Pulse Spacing');
  ins_device_setting_parameter('Debit Card Cost #1', 'Y','Debit Card Cost #1');
  ins_device_setting_parameter('Debit Card Label #1', 'Y','Debit Card Label #1');
  ins_device_setting_parameter('Debit Card Cost #2', 'Y','Debit Card Cost #2');
  ins_device_setting_parameter('Debit Card Label #2', 'Y','Debit Card Label #2');
  ins_device_setting_parameter('Debit Card Cost #3', 'Y','Debit Card Cost #3');
  ins_device_setting_parameter('Debit Card Label #3', 'Y','Debit Card Label #3');
  ins_device_setting_parameter('Debit Card Cost #4', 'Y','Debit Card Cost #4');
  ins_device_setting_parameter('Debit Card Label #4', 'Y','Debit Card Label #4');
  ins_device_setting_parameter('Debit Card Cost #5', 'Y','Debit Card Cost #5');
  ins_device_setting_parameter('Debit Card Label #5', 'Y','Debit Card Label #5');
  ins_device_setting_parameter('Debit Card Cost #6', 'Y','Debit Card Cost #6');
  ins_device_setting_parameter('Debit Card Label #6', 'Y','Debit Card Label #6');
  ins_device_setting_parameter('Debit Card Cost #7', 'Y','Debit Card Cost #7');
  ins_device_setting_parameter('Debit Card Label #7', 'Y','Debit Card Label #7');
  ins_device_setting_parameter('Debit Card Cost #8', 'Y','Debit Card Cost #8');
  ins_device_setting_parameter('Debit Card Label #8', 'Y','Debit Card Label #8');
  ins_device_setting_parameter('Debit Card Bonus', 'Y','Debit Card Bonus');

	COMMIT;
  end;
  /
  
  declare 
count_rows NUMBER;
procedure
  ins_device_setting_parameter (P_DEVICE_SETTING_PARAMETER_CD IN VARCHAR2,
    P_DEVICE_SETTING_UI_CONFIGURAB IN CHAR,
    P_DEVICE_SETTING_PARAMETER_NAM IN VARCHAR2)
  is
  begin
    select count(1) into count_rows from DEVICE.device_setting_parameter where device_setting_parameter_cd = P_DEVICE_SETTING_PARAMETER_CD;
    
    if count_rows <= 0 Then
        --dbms_output.put_line('### INSERTING! PARAMETER_CD='||P_DEVICE_SETTING_PARAMETER_CD);
      insert into device.device_setting_parameter
    
      (DEVICE_SETTING_PARAMETER_CD,
      DEVICE_SETTING_UI_CONFIGURABLE,
      DEVICE_SETTING_PARAMETER_NAME)
      values(P_DEVICE_SETTING_PARAMETER_CD,
      P_DEVICE_SETTING_UI_CONFIGURAB,
      P_DEVICE_SETTING_PARAMETER_NAM);
    end if;
  end;
  begin
  ins_device_setting_parameter('IP Address', 'Y', 'IP Address');
  ins_device_setting_parameter('Server Port', 'Y', 'Server Port');
  ins_device_setting_parameter('Call In Time', 'Y', 'Call In Time');
  ins_device_setting_parameter('Retry Interval', 'Y', 'Retry Interval');
  ins_device_setting_parameter('Call In Num X Ctns', 'Y', 'Call In Num X Ctns');
  ins_device_setting_parameter('No Contact Disable Time', 'Y','No Contact Disable Time');
  ins_device_setting_parameter('Sim Pin', 'Y', 'Sim Pin');
  ins_device_setting_parameter('Sim Number', 'Y', 'Sim Number');
  ins_device_setting_parameter('Pass Card Mask', 'Y', 'Pass Card Mask');
  ins_device_setting_parameter('GSM Modem Phone Number', 'Y', 'GSM Modem Phone Number');

  COMMIT;
  end;
  /
  
declare
LV_CFG_METADATA_FT_NAME VARCHAR2(21) := 'CFG-METADATA-13';
LN_CFG_METADATA_FT_ID NUMBER;
count_rows NUMBER;
err_num NUMBER;
err_msg VARCHAR2(200);

procedure ins_edge_template
   (P_FILE_TRANSFER_ID IN NUMBER,
      P_DEVICE_SETTING_PARAMETER_CD IN VARCHAR2,	
      P_FIELD_OFFSET IN NUMBER,	
      P_FIELD_SIZE IN NUMBER,	
      P_ALIGN IN CHAR,	
      P_PAD_CHAR IN CHAR,	
      P_EDITOR IN VARCHAR2,	
      P_CONVERTER IN VARCHAR2,	
      P_DATA_MODE IN CHAR,	
      P_DISPLAY IN CHAR,	
      P_DESCRIPTION IN VARCHAR2,	
      P_CONFIG_TEMPLATE_SET_VALUE IN VARCHAR2,
      P_CLIENT_SEND IN CHAR,	
      P_DISPLAY_ORDER IN NUMBER,	
      P_DEVICE_TYPE_ID IN NUMBER,	
      P_PROPERTY_LIST_VERSION IN NUMBER,	
      P_CATEGORY_ID IN NUMBER,	
      P_ALT_NAME IN VARCHAR2,	
      P_DATA_MODE_AUX IN CHAR,	
      P_REGEX_ID IN NUMBER, 
      P_ACTIVE IN CHAR)
  is
  
   begin
   select count(1) into count_rows from DEVICE.CONFIG_TEMPLATE_SETTING where file_transfer_id = P_FILE_TRANSFER_ID and device_setting_parameter_cd = P_DEVICE_SETTING_PARAMETER_CD;
   if count_rows > 0 then
    --dbms_output.put_line('### UPDATING! PARAMETER_CD='||P_DEVICE_SETTING_PARAMETER_CD);
    update DEVICE.CONFIG_TEMPLATE_SETTING CTS
    SET 
      CTS.FIELD_OFFSET = P_FIELD_OFFSET,
      CTS.FIELD_SIZE = P_FIELD_SIZE,	
      CTS.ALIGN = P_ALIGN,	
      CTS.PAD_CHAR = P_PAD_CHAR,	
      CTS.EDITOR = P_EDITOR,
      CTS.CONVERTER = P_CONVERTER,	
      CTS.DATA_MODE = P_DATA_MODE,	
      CTS.DISPLAY = P_DISPLAY,	
      CTS.DESCRIPTION = P_DESCRIPTION,		
      CTS.CONFIG_TEMPLATE_SETTING_VALUE = P_CONFIG_TEMPLATE_SET_VALUE,
      CTS.CLIENT_SEND = P_CLIENT_SEND,	
      CTS.DISPLAY_ORDER = P_DISPLAY_ORDER,	
      CTS.DEVICE_TYPE_ID = P_DEVICE_TYPE_ID,	
      CTS.PROPERTY_LIST_VERSION = P_PROPERTY_LIST_VERSION,	
      CTS.CATEGORY_ID = P_CATEGORY_ID,	
      CTS.ALT_NAME = P_ALT_NAME,	
      CTS.DATA_MODE_AUX = P_DATA_MODE_AUX,	
      CTS.REGEX_ID = P_REGEX_ID, 
      CTS.ACTIVE = P_ACTIVE
      where cts.file_transfer_id = P_FILE_TRANSFER_ID and cts.device_setting_parameter_cd = P_DEVICE_SETTING_PARAMETER_CD;
      
   else
   --dbms_output.put('### INSERTING! PARAMETER_CD='||P_DEVICE_SETTING_PARAMETER_CD);
   count_rows := -1;
   select count(1) into count_rows from DEVICE.DEVICE_SETTING_PARAMETER where device_setting_parameter_cd = P_DEVICE_SETTING_PARAMETER_CD;
  
   if count_rows < 1 THEN
   
    insert into device.device_setting_parameter
  
      (DEVICE_SETTING_PARAMETER_CD,
      DEVICE_SETTING_PARAMETER_UBN,
      DEVICE_SETTING_PARAMETER_LBN,                                                                                                                                                                                             
      CREATED_BY,
      CREATED_TS,
      LAST_UPDATED_BY,
      LAST_UPDATED_TS,
      DEVICE_SETTING_UI_CONFIGURABLE,
      DEVICE_SETTING_PARAMETER_NAME,
      SETTING_PARAMETER_TYPE_ID)
      values(P_DEVICE_SETTING_PARAMETER_CD,
      null,
      null,                                                                                                                                                                                             
      'DMS_USER1',
      SYSDATE,
      'DMS_USER1',
      SYSDATE,
      'Y',
      'Not Used',
      null);
    --dbms_output.put('...ALSO INSERTING INTO DEVICE_SETTING_PARAMETER_CD table');
   End if;
    --dbms_output.put_line('.');
    insert into  DEVICE.CONFIG_TEMPLATE_SETTING CTS
      (CTS.FILE_TRANSFER_ID,
      CTS.DEVICE_SETTING_PARAMETER_CD,	
      CTS.FIELD_OFFSET,	
      CTS.FIELD_SIZE,	
      CTS.ALIGN,	
      CTS.PAD_CHAR,	
      CTS.EDITOR,	
      CTS.CONVERTER,	
      CTS.DATA_MODE,	
      CTS.DISPLAY,	
      CTS.DESCRIPTION,		
      CTS.CONFIG_TEMPLATE_SETTING_VALUE,	
      CTS.CLIENT_SEND,	
      CTS.DISPLAY_ORDER,	
      CTS.DEVICE_TYPE_ID,	
      CTS.PROPERTY_LIST_VERSION,	
      CTS.CATEGORY_ID,	
      CTS.ALT_NAME,	
      CTS.DATA_MODE_AUX,	
      CTS.REGEX_ID, 
       CTS.ACTIVE)
      values
      (P_FILE_TRANSFER_ID,
      P_DEVICE_SETTING_PARAMETER_CD,	
      P_FIELD_OFFSET,	
      P_FIELD_SIZE,	
      P_ALIGN,	
      P_PAD_CHAR,	
      P_EDITOR,	
      P_CONVERTER,	
      P_DATA_MODE,	
      P_DISPLAY,	
      P_DESCRIPTION,	
      P_CONFIG_TEMPLATE_SET_VALUE,	
      P_CLIENT_SEND,	
      P_DISPLAY_ORDER,	
      P_DEVICE_TYPE_ID,	
      P_PROPERTY_LIST_VERSION,	
      P_CATEGORY_ID,	
      P_ALT_NAME,	
      P_DATA_MODE_AUX,	
      P_REGEX_ID,
      P_ACTIVE);
    
    
   end if;
       
EXCEPTION
  WHEN OTHERS
  THEN
	err_num := SQLCODE;
	err_msg := SUBSTR(SQLERRM, 1, 100);
	dbms_output.put_line('### ALERT!EXCEPTION OCCURRED, Insert of config default did not happen DURING config_template UPDATE: CURRENT file_transfer_id: ' ||P_FILE_TRANSFER_ID|| ', ERR_NUM: '||err_num ||', ERR_MSG: '||err_msg||'.');
	
END;

BEGIN
INSERT INTO DEVICE.FILE_TRANSFER(FILE_TRANSFER_NAME, FILE_TRANSFER_TYPE_CD, FILE_TRANSFER_COMMENT)
SELECT LV_CFG_METADATA_FT_NAME, 19, 'Placeholder for the Edge master config template settings' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM DEVICE.FILE_TRANSFER WHERE FILE_TRANSFER_NAME = LV_CFG_METADATA_FT_NAME);

SELECT FILE_TRANSFER_ID INTO LN_CFG_METADATA_FT_ID 
FROM DEVICE.FILE_TRANSFER WHERE FILE_TRANSFER_NAME = LV_CFG_METADATA_FT_NAME;

UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
SET FILE_TRANSFER_ID = LN_CFG_METADATA_FT_ID
WHERE DEVICE_TYPE_ID = 13
	AND FILE_TRANSFER_ID != LN_CFG_METADATA_FT_ID;

ins_edge_template(LN_CFG_METADATA_FT_ID, '0' ,null,48,null, null , 'TEXT:7 to 48' , null, 'A' , 'N' , 'DNS host name for authorizations', null, 'Y', 1, 13, 0, 3, 'Auth IP Name', null, 19, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1' ,null,48,null, null , 'TEXT:7 to 48' , null, 'A' , 'N' , 'DNS host name for authorizations - backup 1', null, 'Y', 2, 13, 0, 3, 'Auth IP Name Backup 1', null, 19, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '2' ,null,15,null, null , 'TEXT:7 to 15' , null, 'A' , 'N' , 'IP address for authorizations  - backup 2', null, 'Y', 3, 13, 0, 3, 'Auth IPV4 Address Backup 2', null, 19, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '3' ,null,15,null, null , 'TEXT:7 to 15' , null, 'A' , 'N' , 'IP address for authorizations  - backup 3', null, 'Y', 4, 13, 0, 3, 'Auth IPV4 Address Backup 3', null, 19, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '4' ,null,5,null, null , 'TEXT:1 to 5' , null, 'A' , 'N' , 'Port for authorizations', null, 'Y', 5, 13, 0, 3, 'Auth IP Port', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '5' ,null,5,null, null , 'TEXT:1 to 5' , null, 'A' , 'N' , 'Port for authorizations - backup 1', null, 'Y', 6, 13, 0, 3, 'Auth IP Port Backup 1', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '6' ,null,5,null, null , 'TEXT:1 to 5' , null, 'A' , 'N' , 'Port for authorizations - backup 2', null, 'Y', 7, 13, 0, 3, 'Auth IP Port Backup 2', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '7' ,null,5,null, null , 'TEXT:1 to 5' , null, 'A' , 'N' , 'Port for authorizations - backup 3', null, 'Y', 8, 13, 0, 3, 'Auth IP Port Backup 3', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '10' ,null,5,null, null , 'TEXT:1 to 5' , null, 'A' , 'Y' , 'Authorization response timeout in seconds', '15', 'Y', 9, 13, 0, 3, 'Auth Response Timeout', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '20' ,null,48,null, null , 'TEXT:7 to 48' , null, 'A' , 'N' , 'DNS host name for batches', null, 'Y', 10, 13, 0, 3, 'Misc IP Name', null, 19, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '21' ,null,48,null, null , 'TEXT:7 to 48' , null, 'A' , 'N' , 'DNS host name for batches - backup 1', null, 'Y', 11, 13, 0, 3, 'Misc IP Name Backup 1', null, 19, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '22' ,null,15,null, null , 'TEXT:7 to 15' , null, 'A' , 'N' , 'IP address for batches - backup 2', null, 'Y', 12, 13, 0, 3, 'Misc IPV4 Address Backup 2', null, 19, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '23' ,null,15,null, null , 'TEXT:7 to 15' , null, 'A' , 'N' , 'IP address for batches - backup 3', null, 'Y', 13, 13, 0, 3, 'Misc IPV4 Address Backup 3', null, 19, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '24' ,null,5,null, null , 'TEXT:1 to 5' , null, 'A' , 'N' , 'Port for batches', null, 'Y', 14, 13, 0, 3, 'Misc IP Port', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '25' ,null,5,null, null , 'TEXT:1 to 5' , null, 'A' , 'N' , 'Port for batches - backup 1', null, 'Y', 15, 13, 0, 3, 'Misc IP Port Backup 1', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '26' ,null,5,null, null , 'TEXT:1 to 5' , null, 'A' , 'N' , 'Port for batches - backup 2', null, 'Y', 16, 13, 0, 3, 'Misc IP Port Backup 2', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '27' ,null,5,null, null , 'TEXT:1 to 5' , null, 'A' , 'N' , 'Port for batches - backup 3', null, 'Y', 17, 13, 0, 3, 'Misc IP Port Backup 3', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '30' ,null,3,null, null , 'TEXT:1 to 5' , null, 'A' , 'Y' , 'Batch response timeout in seconds', '20', 'Y', 18, 13, 0, 3, 'Misc Response Timeout', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '31' ,null,5,null, null , 'TEXT:1 to 5' , null, 'A' , 'N' , 'Retry interval in seconds for batches', '15', 'Y', 19, 13, 0, 3, 'Misc Retry Interval', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '32' ,null,5,null, null , 'TEXT:1 to 5' , null, 'A' , 'N' , 'Multiplier for Misc Retry Interval', '5', 'Y', 20, 13, 0, 3, 'Misc Retry Slope', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '33' ,null,5,null, null , 'TEXT:1 to 5' , null, 'A' , 'N' , 'The number of failed batch attempts before starting the recovery procedure', '20', 'Y', 21, 13, 0, 3, 'Misc Maximum Number Of Failed Connections', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '70' ,null,1,null, null , 'SELECT:0=0;1=1;2=2;3=3' , null, 'A' , 'Y' , '0: Factory, 1: Not-Activated, 2: Permission-to-Activate, 3: Activated', '0', 'Y', 1, 13, 0, 1, 'Activation Status', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '71' ,null,200,null, null , 'TEXT:0 to 200' , null, 'A' , 'N' , 'The list of properties that the device will upload to the server in Activation Requests', null, 'Y', 2, 13, 0, 1, 'Activation Additional Property Request', null, 19, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '85' ,null,30,null, null , 'TEXT:0 to 30' , null, 'A' , 'N' , 'Schedule for generating and sending Settlement messages to the server.<br>
   <b>Format:</b><br>
   Daily: <b>D^{TIME}</b> example: D^0300 (daily at 3 AM)<br>
   Weekly: <b>W^{TIME}^{WEEKDAYLIST}</b> example: W^2145^0 (every Sunday at 9:45 PM)<br>
   Monthly: <b>M^{TIME}^{DAYOFMONTH}^{MONTHLIST}</b> example: M^0930^1^1|7 (every January 1 and July 1 at 9:30 AM)<br>
   Interval: <b>I^{UTCTimeOffset}^{IntervalSeconds}</b> example: I^0^14400 (every four hours)<br>
   <b>Time Encoding:</b> 24-hour based, left two digits are hours, right two digits are minutes, the item must occupy two spaces always to support the absence of a delimiter, valid placeholder is 0, example: 2401<br>   
   <b>Day of Week Encoding:</b> 0: Sunday, 1: Monday, 2: Tuesday, 3: Wednesday, 4: Thursday, 5: Friday, 6: Saturday<br>
   <b>Weekday List:</b> variable size, list of Day of Week codes, pipe ""|"" is the delimiter between items, items ordered lowest to highest, example: 1|2|3|4|5 (Monday through Friday)<br>
   <b>Day of Month:</b> valid days of the month are 1 to 31 but 29, 30 and 31 may not be valid for the month(s) selected.<br>
   <b>Month List:</b> variable size, list of months (1 to 12), pipe ""|"" is the delimiter between items, items ordered lowest to highest, ""A"" can be used to specify ALL months, example: 3|6|9|12 (quarterly)<br>
   <b>Notes:</b><br>
   The characters ""{"" and ""}"" are used to represent a sub-definition, they are not literally placed within the format.<br>
   UTCTimeOffset and IntervalSeconds are both decimal numbers.<br>
   Please refer to ePort Network Protocol for more information.', 'W^0200^0', 'Y', 1, 13, 0, 7, 'Scheduled Settlement', null, 19, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '86' ,null,30,null, null , 'TEXT:0 to 30' , null, 'A' , 'N' , 'Schedule for server session call-ins when the device is not activated. Please refer to the format specified for Scheduled Settlement. ', 'I^7200^21600', 'Y', 2, 13, 0, 7, 'Scheduled Session - Non-Activated', null, 19, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '87' ,null,30,null, null , 'TEXT:0 to 30' , null, 'A' , 'N' , 'Schedule for server session call-ins when the device is activated. Please refer to the format specified for Scheduled Settlement. ', 'D^0300', 'Y', 3, 13, 0, 7, 'Scheduled Session - Activated', null, 19, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '215' ,null,3,null, null , 'TEXT:1 to 3' , null, 'A' , 'N' , 'Inactivity time in hours that will trigger a device reboot. The device will reboot due to inactivity only immediately before its scheduled session call-in so the inactivity reboot will not cause an extra call-in.', null, 'Y', 4, 13, 0, 7, 'Inactivity Reboot', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1001' ,null,30,null, null , 'TEXT:0 to 30' , null, 'A' , 'N' , 'This value is the number of seconds that must elapse after the VMC stops communicating with the Edge device before the LCD changes to Unable to Accept a Card (a vend cannot be made).<br>
   Note: Edge may change the improper setting of 0 to the value of 10 seconds.', '0', 'Y', 1, 13, 0, 6, 'MDB Setting No Response Timeout To Disable', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1002' ,null,30,null, null , 'TEXT:0 to 30' , null, 'A' , 'N' , 'This value is the number of seconds that must elapse after the VMC stops communicating with the Edge device before the MDB system will reset itself. This value must be higher than <b>MDB Setting No Response Timeout To Disable</b>.<br>
   Note: Edge may change the improper setting of 0 to the value of 100 seconds.', '0', 'Y', 2, 13, 0, 6, 'MDB Setting No Response Timeout To Reset', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1003' ,null,1,null, null , 'SELECT:1=1;2=2;3=3' , null, 'A' , 'Y' , 'This number is the MDB Feature Level that the ePort reports back to the VMC.<br>
   1 - Basic card reader support; the reader has no revaluation capability.<br>
   2 - Reader supports some advanced features. Reader will respond to Revalue Requests as Vend Denied.<br>
   3 - Reader can handle the advanced features including 32 bit currency and negative vend if the VMC also supports them. Reader will respond to negative vends as Vend Denied.<br>
   Note: Gx is fixed at Level 1', '10', 'Y', 3, 13, 0, 6, 'MDB Setting Preferred Feature Level', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1004' ,null,30,null, null , 'TEXT:0 to 30' , null, 'A' , 'N' , 'MDB setting reserved for the future', '0', 'Y', 4, 13, 0, 6, 'MDB Setting Reserved 04', null, 19, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1005' ,null,2,null, null , 'SELECT:1=1;2=2;3=3;4=4;5=5;6=6;7=7;8=8;9=9;10=10;11=11;12=12;13=13;14=14;15=15' , null, 'A' , 'N' , 'This number can range from 1 to 15.<br>
   The higher the number the harder the ePort will drive the opto-coupler for MDB transmission.<br>
   Some vending machines may be sensitive to the drive level as the drive can distort the signal.<br>
   If the drive is too high or too low, these machines may stop receiving transmissions from our ePorts.', '10', 'Y', 5, 13, 0, 6, 'MDB Setting Tx Drive Level', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1006' ,null,30,null, null , 'TEXT:0 to 30' , null, 'A' , 'N' , 'This number is a filter setting to help mis-behaved VMCs to work correctly.<br>
   This number is the number of polls after the reception of End Session before the Edge will allow a new session to start. The larger the number the more filtering is applied, more chance that the VMC will work correctly, but at the negative of slowing down the user experience on a multi-vend.<br>
   Example. When the value = 10 and the Vending machine did its first vend. The Edge will wait 10 polls before attempting the second vend of a multi-vend.<br>
   Note: Try the setting MDB Setting Session Filter Polls After Enable first before using this setting as it has a greater impact on the user experience.<br>
   Note: Edge may change the improper setting of 0 to the value of 4 polls.', '0', 'Y', 6, 13, 0, 6, 'MDB Setting Tx Drive Level', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1007' ,null,30,null, null , 'TEXT:0 to 30' , null, 'A' , 'N' , 'This number is a filter setting to help mis-behaved VMCs to work correctly.<br>
   This number is the number of polls after the reception of Enable or Disable before the Edge will allow a new session to start. The larger the number the more filtering is applied, more chance that the VMC will work correctly, but at the negative of slowing down the user experience on a multi-vend.<br>
   Example. When the value = 20 and the Vending machine did its first vend. The VMC disables the Edge and then a little later re-enables it, the Edge will wait 20 polls before attempting the second vend of a multi-vend.<br>
   Note: Edge may change the improper setting of 0 to the value of 20 polls.', '0', 'Y', 7, 13, 0, 6, 'MDB Setting Session Filter Polls After Enable', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1008' ,null,5,null, null , 'TEXT:1 to 5' , null, 'A' , 'Y' , 'This is the number of vends that the ePort will attempt in a multisession if funds are available.<br>
   To set single-vend (disable multi-vend) set this value to 1.', '3', 'Y', 8, 13, 0, 6, 'MDB Setting Max Number Of Items', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1009' ,null,1,null, null , 'SELECT:1=1;2=2' , null, 'A' , 'N' , 'This number represents the delay between transmitted bytes. It can either be:<br>
   1 Stop Bit - equivalent to no delay inserted between transmitted bytes.<br>
   2 Stop Bits - equivalent to 1 bit delay between transmitted bytes.<br>
   Note: This number replaces the Gx''s MDB Interval Speed Setting.', '0', 'Y', 9, 13, 0, 6, 'MDB Setting TXD Stop Bits', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1010' ,null,5,null, null , 'TEXT:1 to 5' , null, 'A' , 'N' , 'This number represents how fast the Edge device will respond to the VMC''s poll.<br>
   Too fast or too slow and the VMC will not be able to receive our transmitted response.<br>
   Response Time = This number x 0.2ms. The default of 12 = 2.4ms.<br>
   Note: the MDB spec has a max of 5ms, but most VMC''s prefer >2ms and work higher than the 5ms spec.', '10', 'Y', 10, 13, 0, 6, 'MDB Setting Response Delay 200us', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1011' ,null,5,null, null , 'TEXT:1 to 5' , null, 'A' , 'N' , 'This number represents how fast the Edge device will attempt to parse a new MDB message packet received by the VMC.
   Too fast and the Edge device will be wasting CPU time. Too slow will delay the response timing.
   It''s best to set this value greater than 1 bit time and less then Response Delay.
   Parse Delay Time = This number x 0.2ms. The default of 8 = 1.6 ms.', '8', 'Y', 11, 13, 0, 6, 'MDB Setting Parse Delay 200us', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1012' ,null,2,null, null , 'SELECT:16=16;96=96' , null, 'A' , 'N' , 'This value represents the address of our ePort on the MDB bus.<br>
   The MDB spec now supports 2 card readers.<br>
   The Primary reader address is 16 (default).<br>
   The Secondary reader address is 96.', '16', 'Y', 12, 13, 0, 6, 'MDB Setting Cardreader ID', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1013' ,null,1,null, null , 'SELECT:0=0;1=1' , null, 'A' , 'N' , '0 - Polled, 1 - Immediate<br>
   This setting allows the ePort to send replies back either immediately or on the next poll.', '0', 'Y', 13, 13, 0, 6, 'MDB Setting Response Type', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1014' ,null,30,null, null , 'TEXT:0 to 30' , null, 'A' , 'N' , 'MDB setting reserved for the future', '0', 'Y', 14, 13, 0, 6, 'MDB Setting Reserved 14', null, 19, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1015' ,null,30,null, null , 'TEXT:0 to 30' , null, 'A' , 'N' , 'MDB setting reserved for the future', '0', 'Y', 15, 13, 0, 6, 'MDB Setting Reserved 15', null, 19, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1016' ,null,30,null, null , 'TEXT:0 to 30' , null, 'A' , 'N' , 'MDB setting reserved for the future', '0', 'Y', 16, 13, 0, 6, 'MDB Setting Reserved 16', null, 19, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1017' ,null,30,null, null , 'TEXT:0 to 30' , null, 'A' , 'N' , 'MDB setting reserved for the future', '0', 'Y', 17, 13, 0, 6, 'MDB Setting Reserved 17', null, 19, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1018' ,null,5,null, null , 'TEXT:1 to 5' , null, 'A' , 'Y' , 'This value in seconds sets the timeout for the end-user to make their first selection.<br> Note: Edge will change the improper setting of 0 to the value of 20 seconds.', '15', 'Y', 18, 13, 0, 6, 'MDB Setting Timeout for 1st Selection', null, 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1019' ,null,5,null, null , 'TEXT:1 to 5' , null, 'A' , 'Y' , 'This value in seconds sets the timeout for the end-user to make a selection, after the first, in a multi-vend.<br>Note: Edge will change the improper setting of 0 to the value of 20 seconds.', '15', 'Y', 19, 13, 0, 6, 'MDB Setting Timeout for 2nd 3rd Etc Selections', null, 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1020' ,null,30,null, null , 'TEXT:0 to 30' , null, 'A' , 'N' , 'MDB setting reserved for the future', '5', 'Y', 20, 13, 0, 6, 'MDB Setting Reserved 20', null, 19, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1021' ,null,5,null, null , 'TEXT:1 to 5' , null, 'A' , 'Y' , 'This value in seconds sets the timeout for the vending machine to make a vend.<br> Note: Edge will change the improper setting of 0 to the value of 300 seconds (5 minutes).', '100', 'Y', 21, 13, 0, 6, 'MDB Setting Timeout For Vend Wait', null, 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1022' ,null,5,null, null , 'TEXT:1 to 5' , null, 'A' , 'Y' , '0 disables the recording of cash transactions.<br>1 enables the recording of cash transactions.<br>2 through 255 are reserved for the future use.', '0', 'Y', 22, 13, 0, 6, 'MDB Setting Capture Cash Transactions', null, 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1023' ,null,1,null, null , 'SELECT:0=0;1=1' , null, 'A' , 'N' , 'This number represents how high the gain of the MDB receiver is. The higher the number the higher the gain.<br> The Edge currently supports gains of 0 and 1. Changing this gain may help the ePort receive MDB data from certain VMCs.', '0', 'Y', 23, 13, 0, 6, 'MDB Setting Rx Gain Level', null, 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1024' ,null,1,null, null , 'SELECT:0=0;1=1' , null, 'A' , 'N' , '0 - Disabled<br>1 - Enabled', null, 'Y', 24, 13, 0, 6, 'MDB Snoop', null , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1025' ,null,5,null, null , 'TEXT:1 to 5' , null, 'A' , 'N' , 'The number of seconds for the device to detect that all peripherals have been disabled by the VMC', null, 'Y', 25, 13, 0, 6, 'Vending Machine Out-of-Service Detection Time', null , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1101' ,null,30,null, null , 'TEXT:0 to 30' , null, 'A' , 'N' , 'Schedule for DEX polling. Please refer to the format specified for Scheduled Settlement.', '0', 'Y', 1, 13, 0, 5, 'DEX Setting Schedule', null, 19, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1102' ,null,5,null, null , 'TEXT:1 to 5' , null, 'A' , 'Y' , 'Number of attempts before quitting DEX polling', '8', 'Y', 2, 13, 0, 5, 'DEX Setting Poll Attempts', null, 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1103',null,1,null,null,'SELECT:0=0;1=1',null,'A','Y','0: Most Common DEX Protocol. 1: Some Ice Cream DEX Protocol.','0','Y',3,13,0,5,'DEX Setting Protocol Type',null,3,'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1104' ,null,5,null, null , 'TEXT:1 to 5' , null, 'A' , 'Y' , '1 - use saved password to unlock the VMC if communication failed, 0 - never try password, just quit', '1', 'Y', 4, 13, 0, 5, 'DEX Setting Try password', null, 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1106',null,30,null,null,'TEXT:0 to 30',null,'A','Y','Schedule for DEX Query Alert. Please refer to the format specified for Scheduled Settlement.',null,'Y',5,13,0,5,'DEX Setting Alert Scheduled',null,19,'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1200' ,null,10,null, null , 'TEXT:1 to 10' , null, 'A' , 'Y' , 'Amount in pennies in Authorization Requests', '1000', 'Y', 1, 13, 0, 2, 'Authorization Amount', null, 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1201' ,null,1,null, null , 'SELECT:0=0;1=1' , null, 'A' , 'Y' , '0 - Sale Amount, authorization for the exact purchase amount that the sale message will be sent with<br>
	1 - Fixed Amount, authorization for the amount configured in the Authorization Amount property', null, 'Y', 2, 13, 0, 2, 'Authorization Mode', null , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1202' ,null,4,null, null , 'TEXT:1 to 4' , null, 'A' , 'Y' , 'Amount in pennies added to the sale amount per each vended item', null, 'Y', 3, 13, 0, 2, 'Convenience Fee', null, 3, 'Y');	
ins_edge_template(LN_CFG_METADATA_FT_ID, '1300' ,null,5,null, null , 'TEXT:1 to 5' , null, 'A' , 'Y' , 'Time in seconds a counter is displayed on the LCD before being cleared and the next counter is displayed', null, 'Y', 4, 13, 0, 2, 'Counter Display Time', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1400' ,null,10,null, null , 'TEXT:1 to 10' , null, 'A' , 'N' , 'eTransAct minimum charge in pennies. Valid range is 0 - 4294967295.', null, 'Y', 1, 13, 0, 38, 'eTransAct Minimum Charge', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1401' ,null,2,null, null , 'SELECT:0=0;1=1;2=2;3=3;4=4;5=5;6=6;7=7;8=8;9=9;10=10;11=11;12=12;13=13;14=14;15=15;16=16;17=17;18=18;19=19' , null, 'A' , 'N' , 'Connected copier type<br>
            0: Bizhub Vendor 1 (Konica Minolta)<br>
            1: Bizhub Vendor 2 (Konica Minolta)<br>
            2: Bizhub Vendor 2M (Konica Minolta)<br>
            3: Sharp Black and White<br>
            4: Sharp Color<br>
            5: Sharp Duplex<br>
            6: Sharp<br>
            7: MFP<br>
            8: Simple<br>
            9: Default Black and White<br>
            10: Default Color<br>
            11: Canon<br>
            12: Konica Minolta<br>
            13: Kyocera Mita<br>
            14: Ricoh 4 Black and White<br>
            15: OCE Varioprint<br>
            16: Toshiba Black and White<br>
            17: Toshiba Color<br>
            18: Ricoh 20 Black and White<br>
            19: Ricoh 20 Color', null, 'Y', 2, 13, 0, 38, 'Copier Type', null, 19, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1402',null,1,null,null,'SELECT:1=1;2=2;3=3;6=6;7=7',null,'A','N','Configure byte is one byte in decimal format which defines some settings of the copier controller<br>
   1: Only Bypass Key Enabled - No Receipt Printing<br>
   2: Receipt Printing (except Minolta Bizhub) - Disable Bypass Key<br>
   3: Receipt Printing (except Minolta Bizhub) - Enable Bypass Key<br>
   6: Minolta Bizhub Receipt Printing - Disable Bypass Key<br>
   7: Minolta Bizhub Receipt Printing - Enable Bypass Key',null,'Y',3,13,0,38,'Configure Byte',null,19,'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1403' ,null,32,null, null , 'TEXT:0 to 32' , null, 'A' , 'N' , 'Header line 1 on the receipt', null, 'Y', 4, 13, 0, 38, 'Receipt Header Line 1', null, 19, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1404' ,null,32,null, null , 'TEXT:0 to 32' , null, 'A' , 'N' , 'Header line 2 on the receipt', null, 'Y', 5, 13, 0, 38, 'Receipt Header Line 2', null, 19, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1405' ,null,32,null, null , 'TEXT:0 to 32' , null, 'A' , 'N' , 'Footer line 1 on the receipt.', null, 'Y', 6, 13, 0, 38, 'Receipt Footer Line 1', null, 19, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1406' ,null,32,null, null , 'TEXT:0 to 32' , null, 'A' , 'N' , 'Footer line 2 on the receipt.', null, 'Y', 7, 13, 0, 38, 'Receipt Footer Line 2', 'N' , 19, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1407' ,null,10,null, null , 'TEXT:1 to 10' , null, 'A' , 'N' , 'Price line 1 in pennies. Valid range is 0 - 4294967295.', null, 'Y', 8, 13, 0, 38, 'Price Line 1', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1408' ,null,10,null, null , 'TEXT:1 to 10' , null, 'A' , 'N' , 'Price line 2 in pennies. Valid range is 0 - 4294967295.', null, 'Y', 9, 13, 0, 38, 'Price Line 2', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1409' ,null,10,null, null , 'TEXT:1 to 10' , null, 'A' , 'N' , 'Price line 3 in pennies. Valid range is 0 - 4294967295.', null, 'Y', 10, 13, 0, 38, 'Price Line 3', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1410' ,null,10,null, null , 'TEXT:1 to 10' , null, 'A' , 'N' , 'Price line 4 in pennies. Valid range is 0 - 4294967295.', null, 'Y', 11, 13, 0, 38, 'Price Line 4', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1411' ,null,10,null, null , 'TEXT:1 to 10' , null, 'A' , 'N' , 'Price line 5 in pennies. Valid range is 0 - 4294967295.', null, 'Y', 12, 13, 0, 38, 'Price Line 5', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1412' ,null,10,null, null , 'TEXT:1 to 10' , null, 'A' , 'N' , 'Price line 6 in pennies. Valid range is 0 - 4294967295.', null, 'Y', 13, 13, 0, 38, 'Price Line 6', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1413' ,null,10,null, null , 'TEXT:1 to 10' , null, 'A' , 'N' , 'Price line 7 in pennies. Valid range is 0 - 4294967295.', null, 'Y', 14, 13, 0, 38, 'Price Line 7', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1414' ,null,10,null, null , 'TEXT:1 to 10' , null, 'A' , 'N' , 'Price line 8 in pennies. Valid range is 0 - 4294967295.', null, 'Y', 15, 13, 0, 38, 'Price Line 8', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1415' ,null,3,null, null , 'TEXT:1 to 3' , null, 'A' , 'N' , 'Display Time in seconds (0 - 255)', null, 'Y', 16, 13, 0, 38, 'Display Time', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1500' ,null,1,null, null , 'SELECT:1=1;2=2;3=3' , null, 'A' , 'Y' , '1 - Standard MDB<br>
   2 - eTrans MDB<br>
   3 - Coin Pulse Interface', '1', 'Y', 1, 13, 0, 4, 'VMC Interface Type', null, 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1501' ,null,3,null, null , 'TEXT:1 to 3' , null, 'A' , 'Y' , 'Used to setup how long each pulse will stay in the active state. (i.e. pulse width.) The pulse width duration is calculated by multiplying this parameter by 10ms.  A value of 10 would yield a pulse width of 100 ms. Valid values: 1-255.', null, 'Y', 2, 13, 0, 4, 'Coin Pulse Duration', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1502' ,null,4,null, null , 'TEXT:1 to 4' , null, 'A' , 'Y' , 'Used to setup how much time is required between pulses that the pulse output line will stay in the inactive state.  The duration of time that the pulse output will stay in the inactive state between consecutive pulses is calculated by multiplying this parameter by 10ms. A value of "25" would yield a delay between consecutive pulses of 250 ms.  Valid Values: 1 - 1000.', null, 'Y', 3, 13, 0, 4, 'Coin Pulse Spacing', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1503' ,null,5,null, null , 'TEXT:1 to 5' , null, 'A' , 'Y' , 'The equivalent monetary value of each output pulse in pennies.  If the VMC / connected equipment expect that each pulse is equal to 25 cents, then this value would be 25.  This field combined with the item price is used to calculate how many pulses to transmit when an item is purchased.  Valid Values: 1 - 65535.', null, 'Y', 4, 13, 0, 4, 'Coin Pulse Value', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1504' ,null,3,null, null , 'TEXT:1 to 3' , null, 'A' , 'Y' , 'The number of seconds in which a "Multi-Item Selection Session" will time out if there is no activity.  Valid Values: 1-255.', null, 'Y', 5, 13, 0, 4, 'Coin Auto Timeout', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1505' ,null,1,null, null , 'SELECT:1=1;2=2' , null, 'A' , 'Y' , 'This determines whether the Reader Enable input is active high or active low.<br>
   1 - Active Low<br>
   2 - Active High', null, 'Y', 6, 13, 0, 4, 'Coin Reader Enable Active State', null, 19, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1506' ,null,5,null, null , 'TEXT:1 to 5' , null, 'A' , 'Y' , 'This field is used with the coin pulse interface to set pricing in pennies for 1st button press prior to starting a transaction. An entry of 0 will cause the item to be skipped.', null, 'Y', 7, 13, 0, 4, 'Coin Item Price #1', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1507' ,null,16,null, null , 'TEXT:0 to 16' , null, 'A' , 'Y' , 'Used to display a description instead of a $ value for 1st button press prior to starting a transaction. Data entry example: Charge #1.', null, 'Y', 8, 13, 0, 4, 'Coin Item Price #1 Label', null, 19, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1508' ,null,5,null, null , 'TEXT:1 to 5' , null, 'A' , 'Y' , 'This field is used with the coin pulse interface to set pricing in pennies for 2nd button press prior to starting a transaction. An entry of 0 will cause the item to be skipped.', null, 'Y', 9, 13, 0, 4, 'Coin Item Price #2', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1509' ,null,16,null, null , 'TEXT:0 to 16' , null, 'A' , 'Y' , 'Used to display a description instead of a $ value for 2nd button press prior to starting a transaction. Data entry example: Charge #2.', null, 'Y', 10, 13, 0, 4, 'Coin Item Price #2 Label', null, 19, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1510' ,null,5,null, null , 'TEXT:1 to 5' , null, 'A' , 'Y' , 'This field is used with the coin pulse interface to set pricing in pennies for 3rd button press prior to starting a transaction. An entry of 0 will cause the item to be skipped.', null, 'Y', 11, 13, 0, 4, 'Coin Item Price #3', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1511' ,null,16,null, null , 'TEXT:0 to 16' , null, 'A' , 'Y' , 'Used to display a description instead of a $ value for 3rd button press prior to starting a transaction. Data entry example: Charge #3.', null, 'Y', 12, 13, 0, 4, 'Coin Item Price #3 Label', null, 19, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1512' ,null,5,null, null , 'TEXT:1 to 5' , null, 'A' , 'Y' , 'This field is used with the coin pulse interface to set pricing in pennies for 4th button press prior to starting a transaction. An entry of 0 will cause the item to be skipped.', null, 'Y', 13, 13, 0, 4, 'Coin Item Price #4', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1513' ,null,16,null, null , 'TEXT:0 to 16' , null, 'A' , 'Y' , 'Used to display a description instead of a $ value for 4th button press prior to starting a transaction. Data entry example: Charge #4.', null, 'Y', 14, 13, 0, 4, 'Coin Item Price #4 Label', null, 19, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1514' ,null,5,null, null , 'TEXT:1 to 5' , null, 'A' , 'Y' , 'This field is used with the coin pulse interface to set pricing in pennies for 5th button press prior to starting a transaction. An entry of 0 will cause the item to be skipped.', null, 'Y', 15, 13, 0, 4, 'Coin Item Price #5', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1515' ,null,16,null, null , 'TEXT:0 to 16' , null, 'A' , 'Y' , 'Used to display a description instead of a $ value for 5th button press prior to starting a transaction. Data entry example: Charge #5.', null, 'Y', 16, 13, 0, 4, 'Coin Item Price #5 Label', null, 19, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1516' ,null,5,null, null , 'TEXT:1 to 5' , null, 'A' , 'Y' , 'This field is used with the coin pulse interface to set pricing in pennies for 6th button press prior to starting a transaction. An entry of 0 will cause the item to be skipped.', null, 'Y', 17, 13, 0, 4, 'Coin Item Price #6', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1517' ,null,16,null, null , 'TEXT:0 to 16' , null, 'A' , 'Y' , 'Used to display a description instead of a $ value for 6th button press prior to starting a transaction. Data entry example: Charge #6.', null, 'Y', 18, 13, 0, 4, 'Coin Item Price #6 Label', null, 19, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1518' ,null,5,null, null , 'TEXT:1 to 5' , null, 'A' , 'Y' , 'This field is used with the coin pulse interface to set pricing in pennies for 7th button press prior to starting a transaction. An entry of 0 will cause the item to be skipped.', null, 'Y', 19, 13, 0, 4, 'Coin Item Price #7', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1519' ,null,16,null, null , 'TEXT:0 to 16' , null, 'A' , 'Y' , 'Used to display a description instead of a $ value for 7th button press prior to starting a transaction. Data entry example: Charge #7.', null, 'Y', 20, 13, 0, 4, 'Coin Item Price #7 Label', null, 19, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1520' ,null,5,null, null , 'TEXT:1 to 5' , null, 'A' , 'Y' , 'This field is used with the coin pulse interface to set pricing in pennies for 8th button press prior to starting a transaction. An entry of 0 will cause the item to be skipped.', null, 'Y', 21, 13, 0, 4, 'Coin Item Price #8', 'N' , 3, 'Y');
ins_edge_template(LN_CFG_METADATA_FT_ID, '1521' ,null,16,null, null , 'TEXT:0 to 16' , null, 'A' , 'Y' , 'Used to display a description instead of a $ value for 8th button press prior to starting a transaction. Data entry example: Charge #8.', null, 'Y', 22, 13, 0, 4, 'Coin Item Price #8 Label', null, 19, 'Y');

	COMMIT;
END;
/

declare
LV_CFG_METADATA_FT_NAME VARCHAR2(21) := 'GX-GENERIC-MAP';
LN_CFG_METADATA_FT_ID NUMBER;
err_num NUMBER;
err_msg VARCHAR2(200);
count_rows NUMBER;
   procedure ins_gx_template
      (P_FILE_TRANSFER_ID IN NUMBER,
      P_DEVICE_SETTING_PARAMETER_CD IN VARCHAR2,	
      P_FIELD_OFFSET IN NUMBER,	
      P_FIELD_SIZE IN NUMBER,	
      P_ALIGN IN CHAR,	
      P_PAD_CHAR IN CHAR,	
      P_EDITOR IN VARCHAR2,	
      P_CONVERTER IN VARCHAR2,	
      P_DATA_MODE IN CHAR,	
      P_DISPLAY IN CHAR,	
      P_DESCRIPTION IN VARCHAR2,	
      P_CONFIG_TEMPLATE_SET_VALUE IN VARCHAR2,
      P_CLIENT_SEND IN CHAR,	
      P_DISPLAY_ORDER IN NUMBER,	
      P_DEVICE_TYPE_ID IN NUMBER,	
      P_PROPERTY_LIST_VERSION IN NUMBER,	
      P_CATEGORY_ID IN NUMBER,	
      P_ALT_NAME IN VARCHAR2,	
      P_DATA_MODE_AUX IN CHAR,	
      P_REGEX_ID IN NUMBER, 
      P_ACTIVE IN CHAR)
  is
   begin
	    select count(1) into count_rows from DEVICE.CONFIG_TEMPLATE_SETTING where file_transfer_id = P_FILE_TRANSFER_ID and field_offset = P_FIELD_OFFSET;
   if count_rows > 0 then
    --dbms_output.put_line('### UPDATING! PARAMETER_CD='||P_DEVICE_SETTING_PARAMETER_CD);
    update DEVICE.CONFIG_TEMPLATE_SETTING CTS
    SET 
	  CTS.DEVICE_SETTING_PARAMETER_CD = P_DEVICE_SETTING_PARAMETER_CD,
      CTS.FIELD_OFFSET = P_FIELD_OFFSET,
      CTS.FIELD_SIZE = P_FIELD_SIZE,	
      CTS.ALIGN = P_ALIGN,	
      CTS.PAD_CHAR = P_PAD_CHAR,	
      CTS.EDITOR = P_EDITOR,
      CTS.CONVERTER = P_CONVERTER,	
      CTS.DATA_MODE = P_DATA_MODE,	
      CTS.DISPLAY = P_DISPLAY,	
      CTS.DESCRIPTION = P_DESCRIPTION,		
      CTS.CONFIG_TEMPLATE_SETTING_VALUE = P_CONFIG_TEMPLATE_SET_VALUE,
      CTS.CLIENT_SEND = P_CLIENT_SEND,	
      CTS.DISPLAY_ORDER = P_DISPLAY_ORDER,	
      CTS.DEVICE_TYPE_ID = P_DEVICE_TYPE_ID,	
      CTS.PROPERTY_LIST_VERSION = P_PROPERTY_LIST_VERSION,	
      CTS.CATEGORY_ID = P_CATEGORY_ID,	
      CTS.ALT_NAME = P_ALT_NAME,	
      CTS.DATA_MODE_AUX = P_DATA_MODE_AUX,	
      CTS.REGEX_ID = P_REGEX_ID, 
      CTS.ACTIVE = P_ACTIVE
      where cts.file_transfer_id = P_FILE_TRANSFER_ID and field_offset = P_FIELD_OFFSET;
      
   else
   	  --dbms_output.put_line('### INSERTING! PARAMETER_CD='||P_DEVICE_SETTING_PARAMETER_CD);
      insert into DEVICE.CONFIG_TEMPLATE_SETTING 
      (FILE_TRANSFER_ID,
      DEVICE_SETTING_PARAMETER_CD,	
      FIELD_OFFSET,	
      FIELD_SIZE,	
      ALIGN,	
      PAD_CHAR,	
      EDITOR,	
      CONVERTER,	
      DATA_MODE,	
      DISPLAY,	
      DESCRIPTION,		
      CONFIG_TEMPLATE_SETTING_VALUE,	
      CLIENT_SEND,	
      DISPLAY_ORDER,	
      DEVICE_TYPE_ID,	
      PROPERTY_LIST_VERSION,	
      CATEGORY_ID,	
      ALT_NAME,	
      DATA_MODE_AUX,	
      REGEX_ID, 
       ACTIVE)
      values
      (P_FILE_TRANSFER_ID,
      P_DEVICE_SETTING_PARAMETER_CD,	
      P_FIELD_OFFSET,	
      P_FIELD_SIZE,	
      P_ALIGN,	
      P_PAD_CHAR,	
      P_EDITOR,	
      P_CONVERTER,	
      P_DATA_MODE,	
      P_DISPLAY,	
      P_DESCRIPTION,	
      P_CONFIG_TEMPLATE_SET_VALUE,	
      P_CLIENT_SEND,	
      P_DISPLAY_ORDER,	
      P_DEVICE_TYPE_ID,	
      P_PROPERTY_LIST_VERSION,	
      P_CATEGORY_ID,	
      P_ALT_NAME,	
      P_DATA_MODE_AUX,	
      P_REGEX_ID,
      P_ACTIVE);
     end if;  
       EXCEPTION
          WHEN OTHERS
          THEN
            err_num := SQLCODE;
            err_msg := SUBSTR(SQLERRM, 1, 100);
            dbms_output.put_line('### ALERT!EXCEPTION OCCURRED, Insert of config default did not happen DURING config_template UPDATE: CURRENT file_transfer_id: ' ||P_FILE_TRANSFER_ID|| ', ERR_NUM: '||err_num ||', ERR_MSG: '||err_msg||'.');
      END;
BEGIN
INSERT INTO DEVICE.FILE_TRANSFER(FILE_TRANSFER_NAME, FILE_TRANSFER_TYPE_CD, FILE_TRANSFER_COMMENT)
SELECT LV_CFG_METADATA_FT_NAME, 19, 'Placeholder for the Gx generic map settings' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM DEVICE.FILE_TRANSFER WHERE FILE_TRANSFER_NAME = LV_CFG_METADATA_FT_NAME);

SELECT FILE_TRANSFER_ID INTO LN_CFG_METADATA_FT_ID 
FROM DEVICE.FILE_TRANSFER WHERE FILE_TRANSFER_NAME = LV_CFG_METADATA_FT_NAME;

UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
SET FILE_TRANSFER_ID = LN_CFG_METADATA_FT_ID
WHERE DEVICE_TYPE_ID = 0
	AND FILE_TRANSFER_ID != LN_CFG_METADATA_FT_ID;

ins_gx_template (LN_CFG_METADATA_FT_ID,'Terminal Location',6,16,'C',' ','TEXT:0 to 16',null,'A','Y','16 Character field denoting the location name.','Vend at e-Port','Y',1,0,null,null,null,null,19,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Display Line 1',42,16,'C',' ','TEXT:0 to 16',null,'A','Y','TOP line of display shown during attract mode (non-use). If this field is NOT to be seen or printed a ~ character can be entered as the first character.','Welcome to ePort','Y',2,0,null,null,null,null,19,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Unused Splash Display Line 1',58,4,'L',' ','TEXT:0 to 4',null,'A','N','Extra display space - not usable with standard L-Box',null,'Y',3,0,null,null,null,null,19,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Display Line 2',62,16,'C',' ','TEXT:0 to 16',null,'A','Y','BOTTOM line of display shown during attract mode (non-use).  If this field is NOT to be seen or printed a ~ character can be entered as the first character. ','Same as Cash','Y',4,0,null,null,null,null,19,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Unused Splash Display Line 2',78,4,'L',' ','TEXT:0 to 4',null,'A','N','Extra display space - not usable with standard L-Box',null,'Y',5,0,null,null,null,null,19,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Machine ID',0,6,'L','0','TEXT:0 to 6',null,'A','N','Client Protected: 6-digit machine ID embedded in a 6-byte field: 059999','059999','N',6,0,null,null,'<font color=#B78B16>PROT</font>: Machine ID',null,1,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Network Format Select',486,1,'L','0','RADIO:53=S','HEX','H','Y','This field selects the network format to use, S for USAT network. Other formats have been deprecated.','53','Y',7,0,null,null,null,null,5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'International Dialing Prefix or IP Address',284,6,'L','f','TEXT:0 to 12','HEX','H','Y','Communication Setting: This field is the International dialing prefix, if required, if using a POTS modem, or the USAT server IP Address if using a wireless modem (192.168.79.163). IP Address if using Ethernet modem (208.116.216.163)','208116216163','Y',8,0,null,null,'<font color=red>COM</font>: International Dialing Prefix or IP Address',null,5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Phone Number or Port Number',136,6,'L','f','TEXT:0 to 12','HEX','H','Y','Communication Setting: This field is the USAT server phone number if using a POTS modem, or the server TCP Port Number otherwise.','14107','Y',9,0,null,null,'<font color=red>COM</font>: Phone Number or Port Number',null,5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'USALive Phone In Time',172,2,'R','0','TEXT:0 to 4','HEX','H','Y','Enter a four digit military time in a packed BCD format to set the scheduled call-in to the server time. This field is used in conjunction with the USALive day-cycle field.','100','Y',10,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'USALive Day-Cycle Interval',174,1,'R','0','TEXT:0 to 2','HEX','H','Y','Number of days between call-ins to the USAT server. ','1','Y',11,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'USALive Retry Interval',175,1,'R','0','TEXT:0 to 2','HEX','H','Y','Enter a two digit minute re-try timer, how many minutes the terminal will wait before re-trying to make contact with the USA server. ','15','Y',12,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Device Name',162,8,'L',' ','TEXT:0 to 8',null,'A','N','Client Protected: This field is machine identifier used by the server to look up device communication settings.','000000','N',13,0,null,null,'<font color=#B78B16>PROT</font>: Device Name',null,19,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'MDB Interval Speed Setting',206,2,'R','0','TEXT:0 to 4','HEX','H','Y','The MDB interval speed-setting field is a delay inserted between the bytes sent from the ePort to the VMC. This field can range from 0000 to 0500 a preferable setting is either 0100 or 0125. Data entry example: Packed BCD 0150 = an MDB speed interval delay of 150. Range numeric 0-9.','125','Y',14,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'MDB Response Setting',356,2,'R','0','TEXT:0 to 4','HEX','H','Y','This field represents the amount of time in milliseconds the terminal will wait after receiving a message over MDB from the VMC before initiating a response message. Values can range from 0000-0030 where 0000 represents no delay in initiating a response to 0030 a three-millisecond delay. A preferred value for most VMC is 0005 = .5ms or 0010 = 1.0ms. Data entry example: 00010 = 1.0ms delay.','5','Y',15,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Transaction Auto Time-Out',368,1,'R','0','TEXT:0 to 2','HEX','H','Y','The transaction once started will automatically terminate if a vend item is not selected in the number of seconds set in this field. This field works in conjunction with the After Use Time-Out field. Data entry example: 15 = automatically terminate transaction if the user does not make a selection in 15 seconds.','20','Y',16,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'After Use Auto Time-Out',369,1,'R','0','TEXT:0 to 2','HEX','H','Y','The transaction after a vend has occurred will automatically terminate if a subsequent vend item is not selected in the number of seconds set in this field. This field works in conjunction with the Transaction Time-Out field. Data entry example: 20 = automatically terminate transaction if the user does not make a subsequent selection in 20 seconds.','15','Y',17,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Single Vend Mode',370,1,'L','0','RADIO:59=Y;4E=N','HEX','H','Y','If set to Y YES this field will limit a transaction to a single vend. If this field is set to a N NO the Maximum Vend Items and Authorization Amount fields will determine how many multi-vends will be allowed in the current transaction.','4E','Y',18,0,null,null,null,null,5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Capture Currency Transactions',203,1,'L',' ','TEXT:0 to 2','HEX','H','Y','55 = Yes , 00 = No Currency Transctions if reported by vendor.','00','Y',19,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Authorization Selection Flag',387,1,'L','0','RADIO:4E=N','HEX','H','Y','This field control what method(s) of transaction authorization will be utilized. N: perform remote authorization only. <font color=red> Local Authorization has been disabled.</font>','4E','Y',20,0,null,null,null,null,5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Local Authorization Hours To Clear',358,2,'R','0','TEXT:0 to 4','HEX','H','Y','This field represents the number of hour the terminal will wait before clearing the local APPROVAL database. Clearing the local APPROVAL database will allow cards that are being rejected due to excessive frequency of use in a time period to be used again. Data entry example: 0024 = clear the local APPROVAL database every 24 hours. Range numeric 0-9.',null,'Y',21,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Local Authorization Warning Frequency',364,1,'R','0','TEXT:0 to 2','HEX','H','Y','This field represents the number of times a card will be locally APPROVED before a remote authorization will be attempted. ',null,'Y',22,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Local Authorization Stop Frequency',365,1,'R','0','TEXT:0 to 2','HEX','H','Y','This field represents the number of times a card will be locally APPROVED if a remote authorization fails due to communication issues. ',null,'Y',23,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Card Authorization Amount',195,1,'R','0','TEXT:0 to 2','HEX','H','Y','This field is the authorization amount from a transaction. This value is the value passed to the credit bureau during remote authorizations. This value also servers to limit the transaction dollar amount. The transaction dollar amount is set to $3.00 for authorization amounts less than $3 dollars and set to the authorization amount minus $2 for authorization amounts greater that $3 dollars (i.e. a $1 authorization amount will limit a transaction to $3 dollars and a $20 authorization amount will limit the transaction to $18 dollars). ','5','Y',24,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Accelerated Follow Behind Settlement',360,1,'R',' ','TEXT:0 to 2','HEX','H','Y','When set to Yes, terminal will attempt to keep the connection to the server open after a live-auth, and then follow the live-auth with a single settlement batch as soon as the vend is complete. 00 = No, 55 = Yes.','55','Y',25,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Maximum Vend Items Per Transaction',204,1,'R','0','TEXT:0 to 2','HEX','H','Y','Number of items that can be vended per transaction. (1-10)','5','Y',26,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Auth Special No Prefix Cards With SNET',487,1,'L','0','RADIO:59=Y;4E=N','HEX','H','Y','If this field is set to Y and card data is presented that does not match credit card or pass/access card prefixes the card will be authorized with the server.','4E','Y',27,0,null,null,null,null,5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Convenience Fee',362,2,'R','0','TEXT:0 to 2','HEX','H','N','Amount in pennies added to the sale amount per each vended item',null,'Y',28,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'American Express Mask 1',82,4,'L','a','TEXT:0 to 8','HEX','H','N','Default = 34 or to disable replace with 8 ''''b''s''''','34','Y',29,0,null,null,null,null,8,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'American Express Mask 2',86,4,'L','a','TEXT:0 to 8','HEX','H','N','Default = 37 or to disable replace with 8 ''''b''s''''','37','Y',30,0,null,null,null,null,8,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Visa Mask',90,4,'L','a','TEXT:0 to 8','HEX','H','N','Default = 4 or to disable replace with 8 ''''b''s''''','4','Y',31,0,null,null,null,null,8,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Mastercard Mask 1',94,4,'L','a','TEXT:0 to 8','HEX','H','N','Default = 51 or to disable replace with 8 ''''b''s''''','51','Y',32,0,null,null,null,null,8,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Mastercard Mask 2',98,4,'L','a','TEXT:0 to 8','HEX','H','N','Default = 52 or to disable replace with 8 ''''b''s''''','52','Y',33,0,null,null,null,null,8,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Mastercard Mask 3',102,4,'L','a','TEXT:0 to 8','HEX','H','N','Default = 53 or to disable replace with 8 ''''b''s''''','53','Y',34,0,null,null,null,null,8,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Mastercard Mask 4',106,4,'L','a','TEXT:0 to 8','HEX','H','N','Default = 54 or to disable replace with 8 ''''b''s''''','54','Y',35,0,null,null,null,null,8,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Mastercard Mask 5',110,4,'L','a','TEXT:0 to 8','HEX','H','N','Default = 55 or to disable replace with 8 ''''b''s''''','55','Y',36,0,null,null,null,null,8,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Discover Card Mask',118,4,'L','a','TEXT:0 to 8','HEX','H','N','Default = 6011 or to disable replace with 8 ''''b''s''''','6011','Y',37,0,null,null,null,null,8,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Pass Card Mask',122,4,'L','a','TEXT:0 to 8','HEX','H','Y','Default = 0010000a or to disable replace with 8 ''''b''s''''','0010000 ','Y',38,0,null,null,null,null,8,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Service or View Totals Card Mask',114,4,'L','a','TEXT:0 to 8','HEX','H','Y','Eight digit = 0 Service Card do not prompt to clear totals, = 1 Service Card prompt to clear totals, = 2 View Totals Card display totals only do not clear totals, = 3 View and clear totals only,= 4 View cashless totals only do not clear totals, =5 Driver Fill Card (Coke Settlement Process). The 1st seven digits can not match the Pass Card Mask.','bbbbbbbb','Y',39,0,null,null,null,null,8,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Print Receipt Flag',171,1,'L','0','RADIO:59=Y;4E=N;43=C','HEX','H','Y','Y for allow receipt printing with partial cut. C for allow receipt printing with full cut (firmware rev 229G or later.) N for do not allow receipt printing. ','4E','Y',40,0,null,null,null,null,5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Print Destination Select',386,1,'L',' ','RADIO:50=P;55=U','HEX','H','Y','U= Printer port, P= Display port','50','Y',41,0,null,null,null,null,5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Prompt for receipt print',142,6,'L','0','TEXT:0 to 12','HEX','H','Y','Sets number of times user is prompted to ''''Push Yellow Button For Receipt'''' Range numeric 00-10. Set at 00 will automatically print without any prompting','03','Y',42,0,null,null,null,'N',8,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Type Of Card Data On Receipt',209,1,'L','0','RADIO:4E=N;53=S','HEX','H','Y','S=Last four digits of card, N=No data.','53','Y',43,0,null,null,null,null,5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Receipt Phone Number',226,14,'L','0','TEXT:0 to 14',null,'A','Y','This field is the phone number that is printed on the bottom of the receipt for customer service.  ','1-888-561-4748','Y',44,0,null,null,null,null,2,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Poll DEX Flag',385,1,'L','0','RADIO:59=Y;4E=N','HEX','H','Y','Setting this flag to YES Y will cause the terminal poll DEX from the VMC prior to contacting the server.','4E','Y',45,0,null,null,null,null,5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'DEX Hour Interval',256,1,'R','0','TEXT:0 to 2','HEX','H','Y','This field causes the terminal to DEX the VMC at a desired interval. Setting the interval to 00 disables this feature. Data entry example: 00 - disable feature',null,'Y',46,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'DEX Transaction Interval',257,1,'R','0','TEXT:0 to 2','HEX','H','Y','This field causes the terminal to DEX the VMC after a the preset number of transaction has occured. Setting the interval to 00 disables this feature. Data entry example: 00 - disable feature.',null,'Y',47,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'DEX Query Mode Flag',211,1,'L','0','RADIO:59=Y;4E=N','HEX','H','Y','This field determines if the DEX QUERY mode of operation is enabled (Y). (For KO Controllers Only)','4E','Y',48,0,null,null,null,null,5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'DEX Query Interval Minutes',255,1,'R','0','TEXT:0 to 2','HEX','H','Y','Number of minutes between DEX queries when DEX Query Mode Flag=Y',null,'Y',49,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'DEX Poll and Plugin Attempts',186,1,'R','0','TEXT:0 to 2','HEX','H','Y','5.00ET8 and higher: This is a Hexadecimal formatted number with the Left Nibble (digit) assigned to the number of ENQ polls per Plug-In attempt (1-F)x4 and the Right Nibble assigned to number of simulated Plug-Ins per DEX read (1-F). The default value of 52 means 20 ENQ polls and 2 Plug-Ins. Notes: 1) the PASSWORD is used only on EVEN Plug-In attempts, so if value is 51, the DEX password will never be used. 2) If accidentally set 00, the G6 interprets as 4 ENQ polls and 1 Plug-In attempt.',null,'Y',50,0,null,null,null,null,5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'DEX Fill Button Action',187,1,'R','0','RADIO:44=D;46=F;55=U;58=X','HEX','H','Y','5.00ET8 and higher: The DEX Fill Button Action defines what action the Gx will take when someone presses the FILL button. Values are: (F) defined as normal Fill record created, (D) defined as perform a DEX read (useful for learning the Password), (X) defined as perform a DEX read with Diagnostic Status Codes, and (U) means OFF or do nothing. Default is (U) or OFF.','55','Y',51,0,null,null,null,null,5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'DEX Plug-In Attempts',318,1,'R','0','TEXT:0 to 2','HEX','H','N','5.00ET7 Only: The number of simulated plug-ins per DEX read attempt. Default=02. Please enter this number in Hexadecimal format. Example if you want 9 enter 09. If you want 10 enter 0A)',null,'Y',52,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'DEX Poll Attempts',319,1,'R','0','TEXT:0 to 2','HEX','H','N','5.00ET7 Only: The number of ENQ polls per Plug-in attempt. Default=16 {20dec}. Please enter this number in Hexadecimal format. Example if you want 9 enter 09. If you want 10 enter 0A)',null,'Y',53,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Special Vend Character',388,1,'L','0','RADIO:59=Y;4E=N','HEX','H','Y','Setting this flag to YES Y will cause the terminal issue the $FFFE character (special vend character) for the price to post. Choices are YES Y to use the special vend character or NO N to post the max vend price as reported be the VMC during setup. NO N is the normal operational state.','4E','Y',54,0,null,null,null,null,5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'MDB Inventory Hex or BCD Format',389,1,'L','0','RADIO:59=Y','HEX','H','Y','Setting this flag to YES Y will cause the terminal to interpret the VMC inventory as HEX data (We convert to BCD). Setting this flag to NO N will cause the terminal to interpret the VMC inventory as BCD data (No conversion is performed). <font color=red>BCD Inventory Format has been disabled.</font>','59','Y',55,0,null,null,null,null,5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Pass Card / Free Vend Print Flag',361,1,'L','0','RADIO:59=Y;4E=N','HEX','H','Y','(Y)es Receipt , (N)o Receipt Print  When Pass Cards activate vend.','59','Y',56,0,null,null,null,null,5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Credit Batch Location',202,1,'L','0','RADIO:55=U','HEX','H','N','This field determines whether a credit card batch will be settled by the terminal (C) or by way of USAT server (U). Set this field to U to route the unsettled credit card to the USAT server for settlement. Option C has been deprecated.','55','Y',57,0,null,null,null,null,5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Merchant ID in G4; IP Addres in MEI',22,20,'R',' ','TEXT:0 to 20',null,'A','N','In MEI configuration of the G5 this field is used to store the IP address to access the Stitch.Net server.','PH14843592137','Y',58,0,null,null,null,null,17,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Merchant ID Prefix Link 0 of 9',126,1,'R','0','TEXT:0 to 2','HEX','H','N','Links card prefix 0 to the merchant number. This field should be set to 01. Data entry example: Enter 01 to change the byte. Range numeric 0-9 - set at 01.','1','Y',59,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Merchant ID Prefix Link 1 of 9',127,1,'R','0','TEXT:0 to 2','HEX','H','N','Links card prefix 1 to the merchant number. This field should be set to 01. Data entry example: Enter 01 to change the byte. Range numeric 0-9 - set at 01 ','1','Y',60,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Merchant ID Prefix Link 2 of 9',128,1,'R','0','TEXT:0 to 2','HEX','H','N','Links card prefix 2 to the merchant number. This field should be set to 01. Data entry example: Enter 01 to change the byte. Range numeric 0-9 - set at 01','1','Y',61,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Merchant ID Prefix Link 3 of 9',129,1,'R','0','TEXT:0 to 2','HEX','H','N','Links card prefix 3 to the merchant number. This field should be set to 01. Data entry example: Enter 01 to change the byte. Range numeric 0-9 - set at 01 ','1','Y',62,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Merchant ID Prefix Link 4 of 9',130,1,'R','0','TEXT:0 to 2','HEX','H','N','Links card prefix 4 to the merchant number. This field should be set to 01. Data entry example: Enter 01 to change the byte. Range numeric 0-9 - set at 01 ','1','Y',63,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Merchant ID Prefix Link 5 of 9',131,1,'R','0','TEXT:0 to 2','HEX','H','N','Links card prefix 5 to the merchant number. This field should be set to 01. Data entry example: Enter 01 to change the byte. Range numeric 0-9 - set at 01 ','1','Y',64,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Merchant ID Prefix Link 6 of 9',132,1,'R','0','TEXT:0 to 2','HEX','H','N','Links card prefix 6 to the merchant number. This field should be set to 01. Data entry example: Enter 01 to change the byte. Range numeric 0-9 - set at 01','1','Y',65,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Merchant ID Prefix Link 7 of 9',133,1,'R','0','TEXT:0 to 2','HEX','H','N','Links card prefix 7 to the merchant number. This field should be set to 01. Data entry example: Enter 01 to change the byte. Range numeric 0-9 - set at 01 ','1','Y',66,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Merchant ID Prefix Link 8 of 9',134,1,'R','0','TEXT:0 to 2','HEX','H','N','Links card prefix 8 to the merchant number. This field should be set to 01. Data entry example: Enter 01 to change the byte. Range numeric 0-9 - set at 01 ','1','Y',67,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Merchant ID Prefix Link 9 of 9',135,1,'R','0','TEXT:0 to 2','HEX','H','N','Links card prefix 9 to the merchant number. This field should be set to 01. Data entry example: Enter 01 to change the byte. Range numeric 0-9 - set at 01','1','Y',68,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Primary Credit Bureau Phone Number',148,6,'L','f','TEXT:0 to 12','HEX','H','N','This field is the primary credit bureau phone number. Enter an F to exclude a digit from being dialed. Data entry example: Enter 18884044192F to set the USALive call in phone number to 18001234567F with the last digit F being omitted from the dialing sequence. When viewing the phone number in the terminal service mode the F digits will be replaced with an x. Range numeric 0-9 and F. [12 bytes] Justification: Left justified with F added','180043152150','Y',69,0,null,null,null,null,5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Encryption Key',154,8,'L','0','TEXT:0 to 16','HEX','H','N','Client Protected: This field is the encryption key used to communicate with the server. Range numeric 0-9. Can not be change via server. This property has been deprecated.','5629772072931044','N',70,0,null,null,'<font color=#B78B16>PROT</font>: Encryption Key','N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Not Used',244,10,'L','0','TEXT:0 to 20','HEX','H','N','Unused space',null,'Y',71,0,null,null,null,null,1,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Not Used [single byte]',254,1,'L','0','TEXT:0 to 2','HEX','H','N','Not used single byte',null,'Y',72,0,null,null,null,null,1,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Tone Or Pulse Dial Flag',170,1,'L',' ','RADIO:54=T;50=P','HEX','H','N','Enter T for touch tone dialing or P for pulse dialing.','54','Y',73,0,null,null,null,null,5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Pass/Debit Receipt Print',384,1,'L','0','RADIO:59=Y;4E=N','HEX','H','N','Undefined','59','Y',74,0,null,null,null,null,5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'$ per count printer 2 above up to setting',270,2,'R','0','TEXT:0 to 4','HEX','H','N','Undefined',null,'Y',75,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Print Buffer Clear String',488,12,'L',' ','TEXT:0 to 12',null,'A','N','Undefined',null,'Y',76,0,null,null,null,null,19,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Total Counter 1 to Date',176,4,'L','0','TEXT:0 to 8','HEX','H','N','Undefined',null,'Y',77,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Total Since Last Call In',180,4,'R','0','TEXT:0 to 8','HEX','H','N','Undefined',null,'Y',78,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Driver maint card counter clear delay',188,1,'R','0','TEXT:0 to 2','HEX','H','N','(CCE Request) Number of minutes the last driver maintenance card counter value will be stored and available after the initial maintenance swipe.  Value in true HEX format: 00 - FF.  Enter 0A for 10 minutes.',null,'Y',79,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Driver maint card counter display time',189,1,'R','0','TEXT:0 to 2','HEX','H','N','(CCE Request) Number of minutes to display the driver maintanance card counter value on the UI.  Value in true HEX format: 00 - FF.  Enter 01 for 1 minute.',null,'Y',80,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Modem Poll Rate',190,1,'R','0','TEXT:0 to 2','HEX','H','N','The number of minutes to wait before background polling for GPRS modem disconnect.  Values 00-99.  Use 00 to disable polling.','55','Y',81,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Device Personality Type',191,1,'R',' ','TEXT:0 to 2','HEX','H','Y','Assigns a Personality Type to the client which changes device functionality for specific purposes. 40=USAtech. 44=Debitek. 4B=KRH. 53=Shredder Station. 46= Customer interface messages displayed in french (rev. 503A<).  Other codes will be added as needed.',null,'Y',82,0,null,null,null,null,19,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Total Transaction Records Before Call',192,2,'R','0','TEXT:0 to 4','HEX','H','Y','The field is the total records the terminal will record before placing a call to the USAT server. ','140','Y',83,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'USALive Retry Attempts',194,1,'R','0','TEXT:0 to 2','HEX','H','N','Undefined','10','Y',84,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Dial Pause Flag',196,1,'L','0','RADIO:59=Y;4E=N','HEX','H','Y','(Y)es = add 3 second pause after dialing the first digit of a phone number.','4E','Y',85,0,null,null,null,null,5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'IN or OUT of Service Flag',197,1,'L','0','RADIO:49=I;4F=O','HEX','H','Y','This is an IN or OUT field (I/O). Set this field to I to indicate the terminal is IN service. Set this field to a O to indicate the terminal is OUT of service. An OUT of service terminal will not perform cashless transactions.','49','Y',86,0,null,null,null,null,5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Terminal ID',198,4,'L','0','TEXT:0 to 8','HEX','H','N','This field is the terminal ID number and is used in conjunction with the Merchant ID number field. Data entry example: Packed ASCII 12345678. Range numeric 0-9.','577953','Y',87,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Day Before Terminal OUT Of Service',205,1,'R','0','TEXT:0 to 2','HEX','H','Y','# of days that the terminal will go without successfully connecting to the USAT server before placing itself OUT of service.','7','Y',88,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Init Printer String',500,11,'L',' ','TEXT:0 to 11',null,'A','N','Undefined',null,'Y',89,0,null,null,null,null,19,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Initialization Complete Flag',511,1,'L','0','TEXT:0 to 2','HEX','H','Y','This field should always be set to a value other than 55.  If it is set to 55, the terminal thinks it has not been fully initialized and will not accept cards.','2','Y',90,0,null,null,null,null,1,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Allow Machine ID Change',208,1,'L','0','RADIO:59=Y;4E=N','HEX','H','N','This field should be set to YES Y - NO N.','59','Y',91,0,null,null,null,null,5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Call Home Now Flag',210,1,'L','0','RADIO:59=Y;4E=N','HEX','H','Y','This field when set to Y for YES will initiate a call to the USALive. A delay of the USALive Re-try Interval will proceed the initiation of the call attempt. Choices are Y to start the USALive call-in process.','4E','Y',92,0,null,null,null,null,5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Clear Main Memory On Power UP Flag',212,1,'L','0','RADIO:59=Y;4E=N','HEX','H','N','Setting this flag to YES Y will cause the terminal to clear the main transaction memory on power-up (reset). Choices are YES Y to clear main memory on next power-up','59','Y',93,0,null,null,null,null,5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Hours Between Transactions',213,1,'R','0','TEXT:0 to 2','HEX','H','N','This field is the number of hours the terminal will wait between transaction before creating an ERROR-SERVICE record and initiating a call-in to the USALive server. This field can range from 00 to 99 a preferable setting is either 72. Data entry example: 72 = if no transaction occurs in 72 hours a call-in to the USALive server is made to report the ERROR - SERVICE condition.','72','Y',94,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Terminal ID 2 or Port Number',214,4,'R','0','TEXT:0 to 8','HEX','H','N','Undefined',null,'Y',95,0,null,null,null,null,5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Terminal ID 3',218,4,'R','0','TEXT:0 to 8','HEX','H','N','Undefined',null,'Y',96,0,null,null,null,null,5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Minimum Batch Size',222,2,'R','0','TEXT:0 to 4','HEX','H','N','This field sets the minimum batch size required before settlement with the credit bureau will occur. Batches not meeting the minimum batch size may be routed to the USALive server for settlement. Data entry example: 0015 = batch to credit bureau if at least 15 cards are in the batch and if Credit Batch Location = C.','1','Y',97,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Maximum Counter #1 before Auto-End',224,2,'R','0','TEXT:0 to 4','HEX','H','N','Undefined',null,'Y',98,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Receipt Time Out Seconds',366,2,'R','0','TEXT:0 to 4','HEX','H','N','Undefined',null,'Y',99,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Up to printer 1 count',262,2,'R','0','TEXT:0 to 4','HEX','H','N','Undefined',null,'Y',100,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'$ per count printer 1 above up to setting',264,2,'R','0','TEXT:0 to 4','HEX','H','N','Undefined',null,'Y',101,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Up to printer 2 count',268,2,'R','0','TEXT:0 to 4','HEX','H','N','Undefined',null,'Y',102,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Allow Remote Terminal Polling',371,1,'L','0','RADIO:59=Y;4E=N','HEX','H','N','If set to Y YES this field will allow the terminal to be polled (initiating a call-in to the USALive server) by a detected incoming ring signal (G4 terminal). If this field is set to a N NO the terminal will not respond to a polling request. Choices are YES Y Allow remote polling.','4E','Y',103,0,null,null,null,null,5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'External Modem Init String',372,12,'L',' ','TEXT:0 to 12',null,'A','N','Undefined',null,'Y',104,0,null,null,null,null,19,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Service 2 Phone Number',290,6,'L',' ','TEXT:0 to 12','HEX','H','N','Undefined','000000000000','Y',105,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Service 3 Phone Number',296,6,'L',' ','TEXT:0 to 12','HEX','H','N','Undefined','000000000000','Y',106,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Service 4 Phone Number',302,6,'L',' ','TEXT:0 to 12','HEX','H','N','Undefined','000000000000','Y',107,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Service 5 Phone Number',308,6,'L',' ','TEXT:0 to 12','HEX','H','N','Undefined','000000000000','Y',108,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Service 6 Phone Number',314,4,'L',' ','TEXT:0 to 8','HEX','H','N','Undefined','000000','Y',109,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Total Currency Transaction Counter',320,4,'R','0','TEXT:0 to 8','HEX','H','N','Counter: This field is a counter that tracks and increments to reflect the total number of currency transaction accrued (total count of coin and bill transactions). Data entry example: 01235678.',null,'N',110,0,null,null,'<font color=blue>COUNTER</font>: Total Currency Transaction Counter','N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Total Currency Money Counter',324,4,'R','0','TEXT:0 to 8','HEX','H','N','Counter: This field is a counter that tracks and increments to reflect the total amount of money accrued for the currency transactions (total money for coin and bill transactions). Data entry example: 01235678.',null,'N',111,0,null,null,'<font color=blue>COUNTER</font>: Total Currency Money Counter','N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Total Cashless Transaction Counter',328,4,'R','0','TEXT:0 to 8','HEX','H','N','Counter: This field is a counter that tracks and increments to reflect the total number of cashless transaction accrued (total count of non-coin and non-bill transactions). Data entry example: 01235678.',null,'N',112,0,null,null,'<font color=blue>COUNTER</font>: Total Cashless Transaction Counter','N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Total Cashless Money Counter',332,4,'R','0','TEXT:0 to 8','HEX','H','N','Counter: This field is a counter that tracks and increments to reflect the total amount of money accrued for the cashless transactions (total money for non-coin and non-bill transactions). Data entry example: 01235678.',null,'N',113,0,null,null,'<font color=blue>COUNTER</font>: Total Cashless Money Counter','N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Total Pass Card Transaction Counter',336,4,'R','0','TEXT:0 to 8','HEX','H','N','Counter: Undefined',null,'N',114,0,null,null,'<font color=blue>COUNTER</font>: Total Pass Card Transaction Counter','N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Total Pass Card Money Counter',340,4,'R','0','TEXT:0 to 8','HEX','H','N','Counter: Undefined',null,'N',115,0,null,null,'<font color=blue>COUNTER</font>: Total Pass Card Money Counter','N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Total Stitch Sessions Attepted Counter',352,4,'R','0','TEXT:0 to 8','HEX','H','N','Counter: Undefined',null,'N',116,0,null,null,'<font color=blue>COUNTER</font>: Total Stitch Sessions Attepted Counter','N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Total Stitch Transaction Counter',344,4,'R','0','TEXT:0 to 8','HEX','H','N','Counter: Undefined',null,'N',117,0,null,null,'<font color=blue>COUNTER</font>: Total Stitch Transaction Counter','N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Total Stitch Bytes Counter',348,4,'R','0','TEXT:0 to 8','HEX','H','N','Counter: Undefined',null,'N',118,0,null,null,'<font color=blue>COUNTER</font>: Total Stitch Bytes Counter','N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Coin Pulse Value',258,2,'R','0','TEXT:0 to 4','HEX','H','N','This field sets the value of a pulse (G4 terminal COIN INTERFACE) to effect a coin interface between the G4 and a Coin-Op piece of equipment. 0025 = one pulse equals $.25.',null,'Y',119,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Pulse Level Attributes',240,1,'R','0','TEXT:0 to 2','HEX','H','N','00 = Disable input active low, pulse output active low, 10 = Disable input active high, pulse output active low, 01 = Disable input active low, pulse output active high, 11 = Disable input active high, pulse output active high',null,'Y',120,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Vendor Interface Type',241,1,'R','0','TEXT:0 to 2','HEX','H','N','Used to select the vendor interface type: MDB = 01, G6 Pulse = 07',null,'Y',121,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Pulse Duration',242,1,'R','0','TEXT:0 to 2','HEX','H','N','Number of 10ms clock ticks for which a pulse is active in G6 pulse interface mode',null,'Y',122,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Pulse Spacing',243,1,'R','0','TEXT:0 to 2','HEX','H','N','Number of 10ms click ticks required betweeb pulses during transmission of a pulse train in G6 pulse interface mode',null,'Y',123,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Debit Card Cost #1',260,2,'R','0','TEXT:0 to 4','HEX','H','N','This field is used with the G4 BILL INTERFACE to set pricing for 1st button presses prior to starting a transaction. Data entry example               0100 = $1.','500','Y',124,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Debit Card Label #1',462,12,'L',' ','TEXT:0 to 12',null,'A','N','Used to display a discription instead of a $ value for 1st button press prior to starting a transaction. Data entry example: Charge #1','~','Y',125,0,null,null,null,null,19,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Debit Card Cost #2',266,2,'R','0','TEXT:0 to 4','HEX','H','N','This field is used with the G4 BILL INTERFACE to set pricing for 2nd button presses prior to starting a transaction. Data entry example: 0500 = $5.','1000','Y',126,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Debit Card Label #2',474,12,'L',' ','TEXT:0 to 12',null,'A','N','Used to display a discription instead of a $ value for 2nd button press prior to starting a transaction. Data entry example: Charge #2','~','Y',127,0,null,null,null,null,19,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Debit Card Cost #3',272,2,'R','0','TEXT:0 to 4','HEX','H','N','This field is used with the G4 BILL INTERFACE to set pricing for 3rd button presses prior to starting a transaction. Data entry example: 1000 = $10.','2000','Y',128,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Debit Card Label #3',390,12,'L',' ','TEXT:0 to 12',null,'A','N','Used to display a discription instead of a $ value for 3rd button press prior to starting a transaction. Data entry example: Charge #3','~','Y',129,0,null,null,null,null,19,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Debit Card Cost #4',274,2,'R','0','TEXT:0 to 4','HEX','H','N','This field is used with the G4 BILL INTERFACE to set pricing for 4th button presses prior to starting a transaction. Data entry example: 2000 = $20.','5000','Y',130,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Debit Card Label #4',402,12,'L',' ','TEXT:0 to 12',null,'A','N','Used to display a discription instead of a $ value for 4th button press prior to starting a transaction. Data entry example: Charge #4','~','Y',131,0,null,null,null,null,19,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Debit Card Cost #5',276,2,'R','0','TEXT:0 to 4','HEX','H','N','This field is used with the G4 BILL INTERFACE to set pricing for 5th button presses prior to starting a transaction. Data entry example: 5000 = $50.',null,'Y',132,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Debit Card Label #5',414,12,'L',' ','TEXT:0 to 12',null,'A','N','Used to display a discription instead of a $ value for 15th button press prior to starting a transaction. Data entry example: Charge #5',null,'Y',133,0,null,null,null,null,19,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Debit Card Cost #6',278,2,'R','0','TEXT:0 to 4','HEX','H','N','This field is used with the G4 BILL INTERFACE to set pricing for 6th button presses prior to starting a transaction. Data entry example: 9900 = $99.',null,'Y',134,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Debit Card Label #6',426,12,'L',' ','TEXT:0 to 12',null,'A','N','Used to display a discription instead of a $ value for 6th button press prior to starting a transaction. Data entry example: Charge #6',null,'Y',135,0,null,null,null,null,19,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Debit Card Cost #7',280,2,'R','0','TEXT:0 to 4','HEX','H','N','This field is used with the G4 BILL INTERFACE to set pricing for 7th button presses prior to starting a transaction. Data entry example: 2000 = $20.',null,'Y',136,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Debit Card Label #7',438,12,'L',' ','TEXT:0 to 12',null,'A','N','Used to display a discription instead of a $ value for 7th button press prior to starting a transaction. Data entry example: Charge #7',null,'Y',137,0,null,null,null,null,19,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Debit Card Cost #8',282,2,'R','0','TEXT:0 to 4','HEX','H','N','This field is used with the G4 BILL INTERFACE to set pricing for 8th button presses prior to starting a transaction. Data entry example: 2500 = $25.',null,'Y',138,0,null,null,null,'N',5,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Debit Card Label #8',450,12,'L',' ','TEXT:0 to 12',null,'A','N','Used to display a discription instead of a $ value for 8th button press prior to starting a transaction. Data entry example: Charge #8',null,'Y',139,0,null,null,null,null,19,'Y');
ins_gx_template (LN_CFG_METADATA_FT_ID,'Debit Card Bonus',184,2,'R','0','TEXT:0 to 4','HEX','H','N','Used only with Bill Pulse Interface to give a Bonus on a specified purchase ammount. Example 5 tokens for the price of 4',null,'Y',140,0,null,null,null,'N',5,'Y');

COMMIT;
END;
/

declare
LV_CFG_METADATA_FT_NAME VARCHAR2(21) := 'CFG-METADATA-11';
LN_CFG_METADATA_FT_ID NUMBER;
count_rows NUMBER;
   procedure ins_kiosk_template
   (P_FILE_TRANSFER_ID IN NUMBER,
      P_DEVICE_SETTING_PARAMETER_CD IN VARCHAR2,	
      P_FIELD_OFFSET IN NUMBER,	
      P_FIELD_SIZE IN NUMBER,	
      P_ALIGN IN CHAR,	
      P_PAD_CHAR IN CHAR,	
      P_EDITOR IN VARCHAR2,	
      P_CONVERTER IN VARCHAR2,	
      P_DATA_MODE IN CHAR,	
      P_DISPLAY IN CHAR,	
      P_DESCRIPTION IN VARCHAR2,	
      P_CONFIG_TEMPLATE_SET_VALUE IN VARCHAR2,
      P_CLIENT_SEND IN CHAR,	
      P_DISPLAY_ORDER IN NUMBER,	
      P_DEVICE_TYPE_ID IN NUMBER,	
      P_PROPERTY_LIST_VERSION IN NUMBER,	
      P_CATEGORY_ID IN NUMBER,	
      P_ALT_NAME IN VARCHAR2,	
      P_DATA_MODE_AUX IN CHAR,	
      P_REGEX_ID IN NUMBER, 
      P_ACTIVE IN CHAR)
  is
   begin
	   
	select count(1) into count_rows from DEVICE.CONFIG_TEMPLATE_SETTING where file_transfer_id = P_FILE_TRANSFER_ID and device_setting_parameter_cd = P_DEVICE_SETTING_PARAMETER_CD;
   	if count_rows > 0 then
    	--dbms_output.put_line('### UPDATING! PARAMETER_CD='||P_DEVICE_SETTING_PARAMETER_CD);
    	update DEVICE.CONFIG_TEMPLATE_SETTING CTS
    	SET 
	      CTS.FIELD_OFFSET = P_FIELD_OFFSET,
	      CTS.FIELD_SIZE = P_FIELD_SIZE,	
	      CTS.ALIGN = P_ALIGN,	
	      CTS.PAD_CHAR = P_PAD_CHAR,	
	      CTS.EDITOR = P_EDITOR,
	      CTS.CONVERTER = P_CONVERTER,	
	      CTS.DATA_MODE = P_DATA_MODE,	
	      CTS.DISPLAY = P_DISPLAY,	
	      CTS.DESCRIPTION = P_DESCRIPTION,		
	      CTS.CONFIG_TEMPLATE_SETTING_VALUE = P_CONFIG_TEMPLATE_SET_VALUE,
	      CTS.CLIENT_SEND = P_CLIENT_SEND,	
	      CTS.DISPLAY_ORDER = P_DISPLAY_ORDER,	
	      CTS.DEVICE_TYPE_ID = P_DEVICE_TYPE_ID,	
	      CTS.PROPERTY_LIST_VERSION = P_PROPERTY_LIST_VERSION,	
	      CTS.CATEGORY_ID = P_CATEGORY_ID,	
	      CTS.ALT_NAME = P_ALT_NAME,	
	      CTS.DATA_MODE_AUX = P_DATA_MODE_AUX,	
	      CTS.REGEX_ID = P_REGEX_ID, 
	      CTS.ACTIVE = P_ACTIVE
	      where cts.file_transfer_id = P_FILE_TRANSFER_ID and cts.device_setting_parameter_cd = P_DEVICE_SETTING_PARAMETER_CD;
      
   	else
	   	  --dbms_output.put_line('### INSERTING! PARAMETER_CD='||P_DEVICE_SETTING_PARAMETER_CD);
	      insert into DEVICE.CONFIG_TEMPLATE_SETTING 
	      (FILE_TRANSFER_ID,
	      DEVICE_SETTING_PARAMETER_CD,	
	      FIELD_OFFSET,	
	      FIELD_SIZE,	
	      ALIGN,	
	      PAD_CHAR,	
	      EDITOR,	
	      CONVERTER,	
	      DATA_MODE,	
	      DISPLAY,	
	      DESCRIPTION,		
	      CONFIG_TEMPLATE_SETTING_VALUE,	
	      CLIENT_SEND,	
	      DISPLAY_ORDER,	
	      DEVICE_TYPE_ID,	
	      PROPERTY_LIST_VERSION,	
	      CATEGORY_ID,	
	      ALT_NAME,	
	      DATA_MODE_AUX,	
	      REGEX_ID, 
	       ACTIVE)
	      values
	      (P_FILE_TRANSFER_ID,
	      P_DEVICE_SETTING_PARAMETER_CD,	
	      P_FIELD_OFFSET,	
	      P_FIELD_SIZE,	
	      P_ALIGN,	
	      P_PAD_CHAR,	
	      P_EDITOR,	
	      P_CONVERTER,	
	      P_DATA_MODE,	
	      P_DISPLAY,	
	      P_DESCRIPTION,	
	      P_CONFIG_TEMPLATE_SET_VALUE,	
	      P_CLIENT_SEND,	
	      P_DISPLAY_ORDER,	
	      P_DEVICE_TYPE_ID,	
	      P_PROPERTY_LIST_VERSION,	
	      P_CATEGORY_ID,	
	      P_ALT_NAME,	
	      P_DATA_MODE_AUX,	
	      P_REGEX_ID,
	      P_ACTIVE);
      
      end if;
       
       EXCEPTION
          WHEN OTHERS
          THEN
            DBMS_OUTPUT.put_line ( 'Insert of config default did not happen.' );
      END;
BEGIN
INSERT INTO DEVICE.FILE_TRANSFER(FILE_TRANSFER_NAME, FILE_TRANSFER_TYPE_CD, FILE_TRANSFER_COMMENT)
SELECT LV_CFG_METADATA_FT_NAME, 19, 'Placeholder for the Kiosk master config template settings' FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM DEVICE.FILE_TRANSFER WHERE FILE_TRANSFER_NAME = LV_CFG_METADATA_FT_NAME);

SELECT FILE_TRANSFER_ID INTO LN_CFG_METADATA_FT_ID 
FROM DEVICE.FILE_TRANSFER WHERE FILE_TRANSFER_NAME = LV_CFG_METADATA_FT_NAME;

UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
SET FILE_TRANSFER_ID = LN_CFG_METADATA_FT_ID
WHERE DEVICE_TYPE_ID = 11
	AND FILE_TRANSFER_ID != LN_CFG_METADATA_FT_ID;

ins_kiosk_template (LN_CFG_METADATA_FT_ID,'SSN',null,31,null,null,'TEXT:0 to 31',null,'A','N','SSN (silicone serial number), terminal serial number with the bytes in decimal format and a ca as the byte separator',null,'Y',1,11,null,8,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'SSNHex',null,16,null,null,'TEXT:0 to 16',null,'A','N','Terminal serial number in hex format',null,'Y',2,11,null,8,null,null,5,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'SSNMethod',null,8,null,null,'TEXT:0 to 8',null,'A','N','Serial number generation method. If MAC address is available, SSNMethod is set to HARDWARE, otherwise the DLL generates the SSN based on current date/time stamp and random number generator and sets SSNMethod to SOFTWARE.',null,'Y',3,11,null,8,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'VMC',null,8,null,null,'TEXT:0 to 8',null,'A','N','Device name, reporting name of the terminal',null,'Y',4,11,null,8,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'EncKey',null,32,null,null,'TEXT:0 to 32',null,'A','N','Encrypted encryption key',null,'Y',5,11,null,8,null,null,5,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'HostType',null,20,null,null,'TEXT:1 to 20',null,'A','N','Base host type','KIOSK','Y',6,11,null,8,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'SoftwareVersion',null,20,null,null,'TEXT:0 to 20',null,'A','N','DLL version',null,'Y',7,11,null,8,null,null,19,'Y');

ins_kiosk_template (LN_CFG_METADATA_FT_ID,'CommMethod',null,32,null,null,'SELECT:TCPIP_LAN=TCPIP_LAN;GPRS_MODEM=GPRS_MODEM;POTS_MODEM=POTS_MODEM;CDMA_MODEM=CDMA_MODEM;CDPD_MODEM=CDPD_MODEM','TCPIP_LAN','A','N','Current server communication method populated by DLL',null,'Y',1,11,null,9,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'ServerAddress',null,15,null,null,'TEXT:7 to 15',null,'A','N','Server primary IP address','208.116.216.163','Y',2,11,null,9,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'ServerPort',null,5,null,null,'TEXT:1 to 5',null,'A','N','Server primary port','443','Y',3,11,null,9,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'ServerSecondaryAddress',null,15,null,null,'TEXT:7 to 15',null,'A','N','Server secondary IP address','208.116.216.163','Y',4,11,null,9,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'ServerSecondaryPort',null,5,null,null,'TEXT:1 to 5',null,'A','N','Server secondary port','443','Y',5,11,null,9,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'WiredModemDialString',null,20,null,null,'TEXT:1 to 20',null,'A','N','Server primary phone number','18004315217','Y',6,11,null,9,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'WiredModemSecondaryDialString',null,20,null,null,'TEXT:1 to 20',null,'A','N','Server secondary phone number','18004315217','Y',7,11,null,9,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'WiredModemInitString',null,100,null,null,'TEXT:1 to 100',null,'A','N','POTS modem initialization string','E0Q0V1S0=0','Y',8,11,null,9,null,null,19,'Y');

ins_kiosk_template (LN_CFG_METADATA_FT_ID,'LoggingEnabled',null,5,null,null,'RADIO:true=true;false=false',null,'A','Y','Indicates whether the DLL should log internal activity when processing requests','true','Y',1,11,null,14,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'LogRotationNumFiles',null,3,null,null,'TEXT:1 to 3',null,'A','Y','Number of log files to retain','5','Y',2,11,null,14,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'LogRotationFileSizeKB',null,6,null,null,'TEXT:1 to 6',null,'A','Y','Size of each log file in kilobytes','3072','Y',3,11,null,14,null,'N',3,'Y');

ins_kiosk_template (LN_CFG_METADATA_FT_ID,'EthernetProbing',null,5,null,null,'RADIO:true=true;false=false',null,'A','N','Instructs the DLL to always use Ethernet communications, if available, before using the supplied wireless or POTS communication method','true','Y',1,11,null,21,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'SendRetryLimit',null,3,null,null,'TEXT:1 to 3',null,'A','N','Defines the number of times the DLL will attempt to send data to the server for each API command invoked','10','Y',2,11,null,21,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'RecvRetryLimit',null,3,null,null,'TEXT:1 to 3',null,'A','N','Defines the number of times the DLL will "listen" for data from the server','40','Y',3,11,null,21,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'EthernetBlockingTimeoutSeconds',null,2,null,null,'TEXT:1 to 2',null,'A','N','The amount of time in seconds to wait before timing out on a blocking TCP/IP call. Can be used in combination with EtherBlockingTimeoutMicroSeconds.','0','Y',4,11,null,21,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'EthernetBlockingTimeoutMicroSeconds',null,7,null,null,'TEXT:1 to 7',null,'A','N','The amount of time in microseconds to wait before timing out on a blocking TCP/IP call. Can be used in combination with EtherBlockingTimeoutSeconds.','500000','Y',5,11,null,21,null,'N',3,'Y');

ins_kiosk_template (LN_CFG_METADATA_FT_ID,'ModemProbing',null,5,null,null,'RADIO:true=true;false=false',null,'A','N','Instructs the DLL to probe the modem and detect U.S. Robotics POTS_MODEM, MultiTech GPRS_MODEM, AnyDATA CDMA_MODEM and AirLink CDPD_MODEM and set the communication method accordingly','false','Y',1,11,null,27,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'COMPort',null,4,null,null,'TEXT:1 to 4',null,'A','N','COM port for serial communications with server, format: COM1..COMn','COM1','Y',2,11,null,27,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'SerialPortBaud',null,10,null,null,'SELECT:1200=1200;2400=2400;4800=4800;9600=9600;14400=14400;19200=19200;28800=28800;38400=38400;56000=56000;57600=57600;128000=128000;256000=256000',null,'A','N','The serial baud rate','9600','Y',3,11,null,27,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'SerialPortBits',null,1,null,null,'SELECT:4=4;5=5;6=6;7=7;8=8',null,'A','N','The number of data bits','8','Y',4,11,null,27,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'SerialParity',null,10,null,null,'SELECT:none=none;even=even;odd=odd;mark=mark;space=space',null,'A','N','Parity for error checking','none','Y',5,11,null,27,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'SerialStopBits',null,3,null,null,'SELECT:1=1;1.5=1.5;2=2',null,'A','N','The number of bits between bytes','1','Y',6,11,null,27,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'ModemInitDelay',null,5,null,null,'TEXT:1 to 5',null,'A','N','The number of milliseconds to wait after a command is sent to the modem','100','Y',7,11,null,27,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'SerialConnectTimeoutSeconds',null,3,null,null,'TEXT:1 to 3',null,'A','N','Timeout for establishing a connection to the server','40','Y',8,11,null,27,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'SerialServerTimeoutSeconds',null,3,null,null,'TEXT:1 to 3',null,'A','N','Timeout for receiving messages from the server','20','Y',9,11,null,27,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'SerialLocalTimeoutSeconds',null,3,null,null,'TEXT:1 to 3',null,'A','N','Timeout for receiving modem replies','5','Y',10,11,null,27,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'SerialServerConnectAttempts',null,1,null,null,'TEXT:1 to 1',null,'A','N','Number of times the DLL will try to connect to the server','2','Y',11,11,null,27,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'SerialReadIntervalTimeout',null,3,null,null,'TEXT:1 to 3',null,'A','N','The number of milliseconds to wait for the arrival of two characters on the communication line','50','Y',12,11,null,27,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'SerialReadTotalTimeoutConstant',null,3,null,null,'TEXT:1 to 3',null,'A','N','This value is added to each read operation in addition to the value calculated above for SerialReadTotalTimeoutMultiplier','50','Y',13,11,null,27,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'SerialReadTotalTimeoutMultiplier',null,3,null,null,'TEXT:1 to 3',null,'A','N','The timeout calculated by the (number of milliseconds * #bytes requested/read)','10','Y',14,11,null,27,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'SerialWriteTotalTimeoutConstant',null,3,null,null,'TEXT:1 to 3',null,'A','N','This value is added to each write operation in addition to the value calculated for SerialWriteTotalTimeoutMultiplier','50','Y',15,11,null,27,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'SerialWriteTotalTimeoutMultiplier',null,3,null,null,'TEXT:1 to 3',null,'A','N','The timeout calculated by the (number of milliseconds * #bytes requested/write)','10','Y',16,11,null,27,null,'N',3,'Y');

ins_kiosk_template (LN_CFG_METADATA_FT_ID,'APIThreadSafety',null,5,null,null,'RADIO:true=true;false=false',null,'A','N','Instructs the DLL to lock and release a mutex on entry and exit of each API function call','false','Y',1,11,null,19,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'ConnectionThreadSafety',null,5,null,null,'RADIO:true=true;false=false',null,'A','N','Instructs the DLL to lock and release a mutex on SessionStart and SessionClose API function calls','false','Y',2,11,null,19,null,null,19,'Y');

ins_kiosk_template (LN_CFG_METADATA_FT_ID,'*ConfigHash',null,32,null,null,'TEXT:0 to 32',null,'A','N','MD5 hash calculated by the DLL to detect configuration changes',null,'Y',1,11,null,17,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'*UploadConfig',null,1,null,null,'RADIO:Y=Y;N=N',null,'A','N','Flag used by the DLL to indicate that configuration needs to be uploaded to the server','N','Y',2,11,null,17,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'*ConfigHashForLog',null,32,null,null,'TEXT:0 to 32',null,'A','N','MD5 hash calculated by the DLL to detect configuration changes since the last time configuration was logged',null,'Y',3,11,null,17,null,null,19,'Y');

ins_kiosk_template (LN_CFG_METADATA_FT_ID,'AUTHORIZATION_AMOUNT',null,10,null,null,'TEXT:1 to 10',null,'A','Y','Card authorization amount','15.00','Y',1,11,null,29,null,'N',20,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'PRICE_2_CHARGE',null,10,null,null,'TEXT:0 to 10',null,'A','Y','PC usage charge per minute, tier 1','0.50','Y',3,11,null,29,null,'N',20,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'PRICE_2_UP_TO_COUNT',null,5,null,null,'TEXT:1 to 5',null,'A','Y','PC usage charge tier 2 threshold (in minutes)','10','Y',4,11,null,29,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'PRICE_3_CHARGE',null,10,null,null,'TEXT:1 to 10',null,'A','Y','PC usage charge per minute, tier 2','0.40','Y',5,11,null,29,null,'N',20,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'COST_PRINTER_1',null,10,null,null,'TEXT:1 to 10',null,'A','Y','Printer 1 charge per page, tier 1','1.50','Y',6,11,null,29,null,'N',20,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'UP_TO_PRINTER_1',null,5,null,null,'TEXT:1 to 5',null,'A','Y','Printer 1 charge tier 2 threshold (number of pages)','20','Y',7,11,null,29,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'COST_PRINTER_1_ABOVE',null,10,null,null,'TEXT:1 to 10',null,'A','Y','Printer 1 charge per page, tier 2','1.00','Y',8,11,null,29,null,'N',20,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'DisplayPrinter2',null,5,null,null,'RADIO:True=True;False=False',null,'A','Y','Shows or hides Printer 2 on Pricing and Billing Details','True','Y',9,11,null,29,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'COST_PRINTER_2',null,10,null,null,'TEXT:1 to 10',null,'A','Y','Printer 2 charge per page, tier 1','1.00','Y',10,11,null,29,null,'N',20,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'UP_TO_PRINTER_2',null,5,null,null,'TEXT:1 to 5',null,'A','Y','Printer 2 charge tier 2 threshold (number of pages)','20','Y',11,11,null,29,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'COST_PRINTER_2_ABOVE',null,10,null,null,'TEXT:1 to 10',null,'A','Y','Printer 2 charge per page, tier 2','0.50','Y',12,11,null,29,null,'N',20,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'COST_SERVICE_1',null,10,null,null,'TEXT:1 to 10',null,'A','Y','Internet usage charge per minute, tier 1','0.10','Y',13,11,null,29,null,'N',20,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'UP_TO_SERVICE_1',null,5,null,null,'TEXT:1 to 5',null,'A','Y','Internet usage charge tier 2 threshold (in minutes)','99999','Y',14,11,null,29,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'COST_SERVICE_1_ABOVE',null,10,null,null,'TEXT:1 to 10',null,'A','Y','Internet usage charge per minute, tier 2','0','Y',15,11,null,29,null,'N',20,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Use_Transact_Pricing',null,1,null,null,'RADIO:Y=Y;N=N',null,'A','N','Enables TransAct pricing model. If TransAct pricing is disabled, PC will operate in Kiosk mode.','Y','Y',16,11,null,29,null,null,19,'Y');

ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Static_Title',null,100,null,null,'TEXT:1 to 100',null,'A','Y','Title message at the top of the main screen','USA Technologies','Y',1,11,null,24,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Static_Message_Text',null,200,null,null,'TEXT:0 to 200',null,'A','Y','Static message with additional information such as pricing displayed on the main screen. Carriage returns should be replaced with the pipe symbol |.',null,'Y',2,11,null,24,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Flash_Message_1',null,100,null,null,'TEXT:1 to 100',null,'A','Y','Flashing message 1 in the middle of the main screen','Accepts All Major Credit Cards','Y',2,11,null,24,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Flash_Message_2',null,100,null,null,'TEXT:1 to 100',null,'A','Y','Flashing message 2 in the middle of the main screen','Internet, Email and Printing','Y',3,11,null,24,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Authorizing_Message',null,60,null,null,'TEXT:1 to 60',null,'A','Y','Card authorizing message','One Moment Authorizing...','Y',4,11,null,24,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Approved_Message',null,60,null,null,'TEXT:1 to 60',null,'A','Y','Card approved message','Approved... Starting Session...','Y',5,11,null,24,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Declined_Message',null,60,null,null,'TEXT:1 to 60',null,'A','Y','Card declined messag','Authorization Unsuccessful... Please Try Again...','Y',6,11,null,24,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'NoServerSessionMessage',null,60,null,null,'TEXT:1 to 60',null,'A','Y','"Network Error" during card authorization message','Network Error... Please Try Again Later...','Y',7,11,null,24,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'AuthFailedMessage',null,60,null,null,'TEXT:1 to 60',null,'A','Y','"Authorization Failed" message','Authorization Failed... Please Try Again Later...','Y',8,11,null,24,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_One_Moment_Message',null,60,null,null,'TEXT:1 to 60',null,'A','Y','"One moment" message','One Moment...','Y',9,11,null,24,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Completing_Transaction',null,60,null,null,'TEXT:1 to 60',null,'A','Y','"Completing Transaction" message','Completing Transaction...','Y',10,11,null,24,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Server_Update_Message',null,60,null,null,'TEXT:1 to 60',null,'A','Y','Message to be displayed on the main screen while PC is processing updates with the server','Processing Updates... Please Wait...','Y',11,11,null,24,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_PIN_CARD_Caption',null,20,null,null,'TEXT:1 to 20',null,'A','Y','"PIN Login" button caption','PIN Login','Y',12,11,null,24,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Allow_PIN_Card_Entry',null,1,null,null,'RADIO:Y=Y;N=N',null,'A','N','Shows or hides the "Pin Login" button on the main form','N','Y',13,11,null,24,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Message_Text_Color',null,12,null,null,'TEXT:1 to 12',null,'A','N','Color of the static message on the main form','#cccc33','Y',14,11,null,24,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Desktop_Image',null,200,null,null,'TEXT:1 to 200',null,'A','N','Path to the desktop image file','C:\AED\PCExpressDesktop.jpg','Y',15,11,null,24,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'EndSuspSessTimeOutSec',null,3,null,null,'TEXT:1 to 3',null,'A','N','Suspended session timeout in seconds. It allows the user to swipe a card and resume their session that has been suspended because the total sale amount reached the authorization limit.','60','Y',16,11,null,24,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'EndSuspSessButtonFlag',null,1,null,null,'RADIO:Y=Y;N=N',null,'A','N','Shows or hides the "Click to End Session Now" button on the main form that allows the user to end their suspended session without waiting for it to time out (Y - show button, N - hide button)','Y','Y',17,11,null,24,null,null,19,'Y');

ins_kiosk_template (LN_CFG_METADATA_FT_ID,'AUTH_NO_PREFIX_CARDS',null,1,null,null,'RADIO:Y=Y;N=N',null,'A','Y','If this parameter is set to N, cards that do not match the configured prefixes will be declined','N','Y',1,11,null,12,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'PREFIX_0',null,8,null,null,'TEXT:1 to 8',null,'A','Y','Card prefix 0','34','Y',2,11,null,12,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'PREFIX_1',null,8,null,null,'TEXT:1 to 8',null,'A','Y','Card prefix 1','4','Y',3,11,null,12,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'PREFIX_2',null,8,null,null,'TEXT:1 to 8',null,'A','Y','Card prefix 2','51','Y',4,11,null,12,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'PREFIX_3',null,8,null,null,'TEXT:1 to 8',null,'A','Y','Card prefix 3','6011','Y',5,11,null,12,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'PREFIX_4',null,8,null,null,'TEXT:1 to 8',null,'A','Y','Card prefix 4','37','Y',6,11,null,12,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'PREFIX_5',null,8,null,null,'TEXT:1 to 8',null,'A','Y','Card prefix 5','52','Y',7,11,null,12,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'PREFIX_6',null,8,null,null,'TEXT:1 to 8',null,'A','Y','Card prefix 6','53','Y',8,11,null,12,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'PREFIX_7',null,8,null,null,'TEXT:1 to 8',null,'A','Y','Card prefix 7','54','Y',9,11,null,12,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'PREFIX_8',null,8,null,null,'TEXT:1 to 8',null,'A','Y','Card prefix 8','55','Y',10,11,null,12,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'PREFIX_9',null,8,null,null,'TEXT:0 to 8',null,'A','Y','Card prefix 9',null,'Y',11,11,null,12,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'PASS_CARD_PREFIX',null,8,null,null,'TEXT:0 to 8',null,'A','Y','Pass card prefixCard prefix 3',null,'Y',12,11,null,12,null,null,19,'Y');

ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Email_From',null,200,null,null,'TEXT:1 to 200',null,'A','N','Email address that PC will send email from','PublicPC@usatech.com','Y',1,11,null,20,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Enable_Email_Sending',null,1,null,null,'RADIO:Y=Y;N=N',null,'A','N','Enables emailing of transaction related files','N','Y',2,11,null,20,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Email_List',null,200,null,null,'TEXT:0 to 200',null,'A','N','Comma separated list of email recipients of transaction related files',null,'Y',3,11,null,20,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Save_Message_Feature_Flag',null,1,null,null,'RADIO:Y=Y;N=N',null,'A','N','Saves a copy of emailed messages on the PC','N','Y',4,11,null,20,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Email_Subject_Text',null,100,null,null,'TEXT:0 to 100',null,'A','N','This text will be appended to the subject of email with transaction related files',null,'Y',5,11,null,20,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Email_Message_Text',null,200,null,null,'TEXT:0 to 200',null,'A','N','The body of email with transaction related files. If not configured, the receipt with line item payload string will be sent in the body of the email.',null,'Y',6,11,null,20,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Sweep_Directory',null,200,null,null,'TEXT:1 to 200',null,'A','N','Path where files for emailing will accumulate','C:\AED\sweep','Y',7,11,null,20,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_File_Message_Sent_Directory',null,200,null,null,'TEXT:1 to 200',null,'A','N','Path to the folder where emailed transaction related files will be saved (applies only if Kiosk_Save_Message_Feature_Flag is set to Y)','C:\AED\SavedMessages','Y',8,11,null,20,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_File_Storage_Directory',null,200,null,null,'TEXT:1 to 200',null,'A','N','Path to the folder where unsuccessfully emailed transaction related files will be stored','C:\AED\storage','Y',9,11,null,20,null,null,19,'Y');

ins_kiosk_template (LN_CFG_METADATA_FT_ID,'ReceiptMode',null,10,null,null,'SELECT:On=On;Prompt=Prompt;Off=Off',null,'A','Y','Receipt mode: On - always print the receipt, Prompt - prompt for receipt (used in combination with PRINT_RECEIPT_FLAG and Kiosk_Prompt_To_Allow_Email_Receipt), Off - no receipt','Prompt','Y',1,11,null,31,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'PRINT_RECEIPT_FLAG',null,5,null,null,'RADIO:True=True;False=False',null,'A','Y','Indicates whether the user should be prompted to print a receipt at the end of a session if ReceiptMode is set to Prompt','True','Y',2,11,null,31,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Prompt_To_Allow_Email_Receipt',null,1,null,null,'RADIO:Y=Y;N=N',null,'A','Y','If enabled and ReceiptMode is set to Prompt, the user will be asked at the end of a session if an email receipt is desired','Y','Y',3,11,null,31,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'FREE_VEND_PRINT_FLAG',null,5,null,null,'RADIO:True=True;False=False',null,'A','Y','Enables receipt prompts for pass card, access card and ServiceMenu transactions','True','Y',4,11,null,31,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'RECEIPT_TIME_OUT',null,3,null,null,'TEXT:1 to 3',null,'A','Y','Number of seconds a receipt prompt is displayed for before it times out','30','Y',5,11,null,31,null,null,3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Print_Receipt_Caption',null,50,null,null,'TEXT:1 to 50',null,'A','Y','Message displayed on the print receipt prompt','Do you want to print a receipt?','Y',6,11,null,31,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Receipt_Header',null,50,null,null,'TEXT:1 to 50',null,'A','Y','Receipt header','USA Technologies PC Express','Y',7,11,null,31,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Receipt_Location_Caption',null,50,null,null,'TEXT:1 to 50',null,'A','N','Receipt "Location:" caption','Location:','Y',8,11,null,31,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Location',null,100,null,null,'TEXT:0 to 100',null,'A','Y','Terminal location name displayed on the receipt',null,'Y',9,11,null,31,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Receipt_Date_Caption',null,20,null,null,'TEXT:1 to 20',null,'A','N','Receipt "Date:" caption','Date:','Y',10,11,null,31,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Receipt_Time_Caption',null,20,null,null,'TEXT:1 to 20',null,'A','N','Receipt "Time:" caption','Time:','Y',11,11,null,31,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Receipt_Terminal_Caption',null,20,null,null,'TEXT:1 to 20',null,'A','N','Receipt "Terminal ID:" caption','Terminal ID:','Y',12,11,null,31,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Receipt_Transaction_ID',null,20,null,null,'TEXT:1 to 20',null,'A','N','Receipt "Transaction:" caption','Transaction:','Y',13,11,null,31,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Receipt_Card_Used_Info',null,20,null,null,'TEXT:1 to 20',null,'A','N','Receipt "Card:" caption','Card:','Y',14,11,null,31,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Receipt_Usage_Caption',null,20,null,null,'TEXT:1 to 20',null,'A','N','Receipt PC "Usage:" caption','Usage:','Y',15,11,null,31,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Receipt_Print_Use_Caption',null,20,null,null,'TEXT:1 to 20',null,'A','N','Receipt "Printer Usage" caption','Printer Usage','Y',16,11,null,31,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'PRINTER_1_NAME',null,20,null,null,'TEXT:1 to 20',null,'A','Y','Printer 1 name displayed on the receipt','Printer 1','Y',17,11,null,31,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'PRINTER_2_NAME',null,20,null,null,'TEXT:1 to 20',null,'A','Y','Printer 2 name displayed on the receipt','Printer 2','Y',18,11,null,31,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Receipt_Internet_Use_Caption',null,20,null,null,'TEXT:1 to 20',null,'A','N','Receipt "Internet:" usage caption','Internet:','Y',19,11,null,31,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Receipt_Minimum_Charge_Caption',null,50,null,null,'TEXT:1 to 50',null,'A','N','Receipt "Minimum Charge Adjustment: $" caption','Minimum Charge: $','Y',20,11,null,31,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Receipt_Total_Sale',null,20,null,null,'TEXT:1 to 20',null,'A','N','Receipt "Total Sale: $" caption','Total Sale: $','Y',21,11,null,31,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Receipt_Tax_Other_Caption1',null,20,null,null,'TEXT:0 to 20',null,'A','N','Receipt additional information 1',null,'Y',22,11,null,31,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Receipt_Tax_Other_Caption2',null,20,null,null,'TEXT:0 to 20',null,'A','N','Receipt additional information 2',null,'Y',23,11,null,31,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Receipt_Tax_Other_Caption3',null,20,null,null,'TEXT:0 to 20',null,'A','N','Receipt additional information 3',null,'Y',24,11,null,31,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Receipt_Service_Caption',null,20,null,null,'TEXT:0 to 20',null,'A','N','Receipt service caption',null,'Y',25,11,null,31,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Receipt_Footer_Caption',null,50,null,null,'TEXT:1 to 50',null,'A','Y','Receipt footer','Help Desk Phone # 1-888-561-4748','Y',26,11,null,31,null,null,19,'Y');

ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Max_Trans_Before_OOS',null,3,null,null,'TEXT:1 to 3',null,'A','Y','Number of pending transactions allowed to accumulate in the database before terminal is plased out of service','5','Y',1,11,null,28,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_OOS_Message',null,50,null,null,'TEXT:1 to 50',null,'A','Y','Out of Service message','Out Of Service','Y',2,11,null,28,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_IN_OUT_Service_Flag',null,1,null,null,'RADIO:I=I;O=O',null,'A','N','Puts the terminal in or out of service','I','Y',3,11,null,28,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Tran_OOS_Flag',null,1,null,null,'RADIO:Y=Y;N=N',null,'A','N','If the number of incomplete transactions in the local database reaches Kiosk_Max_Trans_Before_OOS, the PC Express software will set this flag to Y and put the terminal out of service','N','Y',4,11,null,28,null,null,19,'Y');

ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Instructions_Page_1',null,200,null,null,'TEXT:0 to 200',null,'A','Y','URL addresses or paths of usage instructions. Multiple items should be separated by asterisks *.',null,'Y',1,11,null,35,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Minimum_Instruction_Flip_Time',null,5,null,null,'TEXT:1 to 5',null,'A','Y','Minimum instruction flip time in milliseconds','30000','Y',2,11,null,35,null,null,3,'Y');

ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Survey_Flag',null,1,null,null,'RADIO:Y=Y;N=N',null,'A','Y','Indicates whether a survey should be displayed when a new user session is started','N','Y',1,11,null,34,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Activate_Survey_Exit',null,1,null,null,'RADIO:Y=Y;N=N',null,'A','Y','Shows or hides "Exit Survey" button on the survey form','Y','Y',2,11,null,34,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Survey_Look_For_Text',null,100,null,null,'TEXT:0 to 100',null,'A','Y','The survey form will be unloaded if this text appears on the page',null,'Y',3,11,null,34,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Survey_Location',null,200,null,null,'TEXT:0 to 200',null,'A','Y','URL address of the survey',null,'Y',4,11,null,34,null,null,19,'Y');

ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Transaction_Save_Flag',null,1,null,null,'RADIO:Y=Y;N=N',null,'A','Y','If set to Y, completed transactions will be saved in the local database','N','Y',1,11,null,13,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'RebootDays',null,27,null,null,'TEXT:0 to 27',null,'A','Y','Comma separated list of week days for scheduled maintenance PC reboots, i.e. Sun,Wed,Fri',null,'Y',2,11,null,13,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'RebootTime',null,5,null,null,'TEXT:5 to 5',null,'A','Y','Time of the scheduled maintenance PC reboot in military format, i.e. 23:30','03:00','Y',3,11,null,13,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Auto_Time_Out_Flag',null,1,null,null,'RADIO:Y=Y;N=N',null,'A','Y','Enables session auto timeout','Y','Y',4,11,null,13,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Auto_Time_Out_Time',null,5,null,null,'TEXT:1 to 5',null,'A','Y','Session inactivity auto timeout time in seconds','300','Y',5,11,null,13,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'ComputerName',null,30,null,null,'TEXT:0 to 30',null,'A','Y','Computer name populated by Kiosk',null,'Y',6,11,null,13,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'ScreenResolution',null,9,null,null,'TEXT:0 to 9',null,'A','Y','Computer screen resolution populated by Kiosk',null,'Y',7,11,null,13,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'LaptopPrinting',null,5,null,null,'RADIO:True=True;False=False',null,'A','N','Enables laptop printing features','False','Y',8,11,null,13,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Public_PC_Version',null,100,null,null,'TEXT:0 to 100',null,'A','N','Version of PC Express software and DLLs',null,'Y',9,11,null,13,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Server_Update_Frequency',null,4,null,null,'TEXT:1 to 4',null,'A','N','Number of minutes between attempts to connect to the server and process updates','30','Y',10,11,null,13,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Resume_Previous_Uncompleted_Session',null,1,null,null,'RADIO:Y=Y;N=N',null,'A','N','Enables resuming the active session after a PC reboot','Y','Y',11,11,null,13,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'UncompletedSessionTimeoutMin',null,5,null,null,'TEXT:1 to 5',null,'A','N','If this number of minutes has passed since the time when the PC was restarted while in session, the session will not be resumed','300','Y',12,11,null,13,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'DialUpInternet',null,1,null,null,'RADIO:Y=Y;N=N',null,'A','N','By default LAN is used for Internet access on the PC. If DialUpInternet is set to Y, the modem will be used to connect to the Internet.','N','Y',13,11,null,13,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'FailedSessionsBeforeReboot',null,2,null,null,'TEXT:1 to 2',null,'A','N','The number of unsuccessful attempts to connect to the server before the PC automatically reboots to try and correct any issues with DHCP, etc. The value should be > 2 and < 100.','3','Y',14,11,null,13,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Maintenance_Password',null,32,null,null,'TEXT:1 to 32',null,'A','N','MD5 hash of the maintenance password','6c64383bfd8d7b0ed84b7d78dadb4b98','Y',15,11,null,13,null,null,5,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Admin_Password',null,32,null,null,'TEXT:1 to 32',null,'A','N','MD5 hash of the admin password','aa45997477591e27601c436bcb228d6f','Y',16,11,null,13,null,null,5,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Master_Password',null,32,null,null,'TEXT:1 to 32',null,'A','N','MD5 hash of the master password','f48861ca24e26a23a923ca68657079f4','Y',17,11,null,13,null,null,5,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Delete_Files_Caption',null,50,null,null,'TEXT:1 to 50',null,'A','N','Message asking the user if they want to delete their files, if any, at the end of a session','Do you want to delete your files?','Y',18,11,null,13,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'CURRENCY_TYPE_SELECTED',null,3,null,null,'TEXT:1 to 3',null,'A','N','Currency displayed in Card Manager','USD','Y',19,11,null,13,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'LogLargePrintJobPages',null,3,null,null,'TEXT:1 to 3',null,'A','N','If the number of pages of a spooler print job exceeds this number, job information will be written to PrintJob.log file','50','Y',20,11,null,13,null,'N',3,'Y');

ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Card_Reader_COM_Port',null,2,null,null,'TEXT:1 to 2',null,'A','N','COM port of the card reader','2','Y',1,11,null,15,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Card_Reader_Baud_Rate',null,6,null,null,'SELECT:1200=1200;2400=2400;4800=4800;9600=9600;14400=14400;19200=19200;28800=28800;38400=38400;56000=56000;115200=115200;128000=128000;256000=256000',null,'A','N','Card reader baud rate','9600','Y',2,11,null,15,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Card_Reader_Data_Bits',null,1,null,null,'SELECT:4=4;5=5;6=6;7=7;8=8',null,'A','N','Card reader data bits','8','Y',3,11,null,15,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Card_Reader_Stop_Bits',null,3,null,null,'SELECT:1=1;1.5=1.5;2=2',null,'A','N','Card reader stop bits','1','Y',4,11,null,15,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Card_Reader_Parity',null,1,null,null,'SELECT:E=E;M=M;N=N;O=O;S=S',null,'A','N','Card reader parity','N','Y',5,11,null,15,null,null,19,'Y');

ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Terminate_Form_Caption',null,20,null,null,'TEXT:1 to 20',null,'A','N','Caption of the "Your Session" form','Your Session','Y',1,11,null,32,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Terminate_Form_Message_Text',null,30,null,null,'TEXT:1 to 30',null,'A','N','Text on the "Your Session" form','To end your session press END button','Y',2,11,null,32,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Terminate_Button_Caption',null,20,null,null,'TEXT:1 to 20',null,'A','N','Caption of the "End Session" button on the "Your Session" form','End Session','Y',3,11,null,32,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Usage_Caption',null,15,null,null,'TEXT:1 to 15',null,'A','N','Usage: caption on the "Your Session" form','Usage:','Y',4,11,null,32,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Usage_Unit',null,10,null,null,'TEXT:1 to 10',null,'A','N','Usage unit on the "Your Session" form ("minute")','minute','Y',5,11,null,32,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Print_Caption',null,15,null,null,'TEXT:1 to 15',null,'A','N','Prints: caption on the "Your Session" form','Prints:','Y',6,11,null,32,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Print_Unit',null,10,null,null,'TEXT:1 to 10',null,'A','N','Print unit on the "Your Session" form ("page")','page','Y',7,11,null,32,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Sale_Caption',null,15,null,null,'TEXT:1 to 15',null,'A','N','"Balance:" caption on the "Your Session" form','Total Sale:','Y',8,11,null,32,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Sale_Unit',null,3,null,null,'TEXT:1 to 3',null,'A','N','Currency displayed on the "Your Session" form','USD','Y',9,11,null,32,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Internet_Caption',null,15,null,null,'TEXT:1 to 15',null,'A','N','Internet: caption on the "Your Session" form','Internet:','Y',10,11,null,32,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Idle_Message',null,100,null,null,'TEXT:1 to 100',null,'A','N','Idle message displayed after the inactivity timeout has elapsed','To Continue Using Computer System Simply Move the Mouse','Y',11,11,null,32,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Idle_TimeOut_Message',null,50,null,null,'TEXT:1 to 50',null,'A','N','End Session In countdown message','End Session In','Y',12,11,null,32,null,null,19,'Y');

ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Printer_Watch_Flag',null,1,null,null,'RADIO:Y=Y;N=N',null,'A','N','Enables scanning of print jobs','Y','Y',1,11,null,30,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Printer_Scan_Rate',null,3,null,null,'TEXT:1 to 3',null,'A','N','Print job scan rate in milliseconds','35','Y',2,11,null,30,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'PrintWaitCounter',null,3,null,null,'TEXT:1 to 3',null,'A','N','Number of seconds to wait for unfinished print jobs at the end of a session','0','Y',3,11,null,30,null,'N',3,'Y');

ins_kiosk_template (LN_CFG_METADATA_FT_ID,'BizhubFlag',null,5,null,null,'RADIO:True=True;False=False',null,'A','N','Enables Konica Minolta bizhub scanning','False','Y',1,11,null,25,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'MFPIPAddress',null,15,null,null,'TEXT:1 to 15',null,'A','N','KM bizhub IP address','0.0.0.0','Y',2,11,null,25,null,null,4,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'MFPSSLEnabled',null,5,null,null,'RADIO:True=True;False=False',null,'A','N','Indicates whether SSL is enabled for OpenAPI on the bizhub','True','Y',3,11,null,25,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'OpenAPIPort',null,5,null,null,'TEXT:1 to 5',null,'A','N','Bizhub OpenAPI port','50003','Y',4,11,null,25,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'OpenAPIUserName',null,30,null,null,'TEXT:1 to 30',null,'A','N','Bizhub OpenAPI username','admin','Y',5,11,null,25,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'OpenAPIPassword',null,30,null,null,'TEXT:1 to 30',null,'A','N','Bizhub OpenAPI password','00000000','Y',6,11,null,25,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'MFPAdminPassword',null,30,null,null,'TEXT:1 to 30',null,'A','N','Bizhub admin password','00000000','Y',7,11,null,25,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'MFPConnectTimeoutMs',null,6,null,null,'TEXT:1 to 6',null,'A','N','Bizhub OpenAPI connection timeout in milliseconds','5000','Y',8,11,null,25,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'SNMPCommunity',null,30,null,null,'TEXT:1 to 30',null,'A','N','Bizhub SNMP community name','public','Y',9,11,null,25,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'SNMPTimeoutMs',null,6,null,null,'TEXT:1 to 6',null,'A','N','Bizhub SNMP timeout in milliseconds','3000','Y',10,11,null,25,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'SNMPRetries',null,2,null,null,'TEXT:1 to 2',null,'A','N','Bizhub SNMP query retries','1','Y',11,11,null,25,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'MFPUserName',null,30,null,null,'TEXT:1 to 30',null,'A','N','Bizhub username, used for job tracking','u1','Y',12,11,null,25,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'DirectPrintUser',null,30,null,null,'TEXT:1 to 30',null,'A','N','Bizhub Direct Print username, used for job tracking and printing receipts and free boarding passes','u1print','Y',13,11,null,25,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'MFPUserID',null,5,null,null,'TEXT:1 to 5',null,'A','N','Bizhub user ID, used for job tracking','9999','Y',14,11,null,25,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'DirectPrintUserID',null,5,null,null,'TEXT:1 to 5',null,'A','N','Bizhub Direct Print user ID, used for job tracking and printing receipts and free boarding passes','9999','Y',15,11,null,25,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'DirectPrinterName',null,80,null,null,'TEXT:1 to 80',null,'A','N','Bizhub "no password" printer name, used for printing receipts and boarding passes. It can also be used during a session for bizhub printing without entering a password.','All-In-One Printer - No Password','Y',16,11,null,25,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'DirectPrinterDir',null,200,null,null,'TEXT:1 to 200',null,'A','N','Path to KM Direct Print folder','C:\AED\MFP\MFPPrint','Y',17,11,null,25,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'DirectPrinterSentDir',null,200,null,null,'TEXT:1 to 200',null,'A','N','Path to KM Direct Print sent folder','C:\AED\MFP\MFPPrint\SentData','Y',18,11,null,25,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'DirectPrintConfigPath',null,200,null,null,'TEXT:1 to 200',null,'A','N','Relative or absolute path to KM Direct Print configuration folder','1\PublicPC','Y',19,11,null,25,null,null,19,'Y');

ins_kiosk_template (LN_CFG_METADATA_FT_ID,'DisplayMFPColorPrints',null,5,null,null,'RADIO:True=True;False=False',null,'A','Y','Shows or hides MFP Color Prints on Pricing and Billing Details','True','Y',1,11,null,26,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'MFPPrintColorPrice1',null,10,null,null,'TEXT:1 to 10',null,'A','Y','MFP color print charge per page, tier 1','1.50','Y',2,11,null,26,null,'N',20,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'MFPPrintColorTier1',null,5,null,null,'TEXT:1 to 5',null,'A','Y','MFP color print tier 2 threshold (number of pages)','20','Y',3,11,null,26,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'MFPPrintColorPrice2',null,10,null,null,'TEXT:1 to 10',null,'A','Y','MFP color print charge per page, tier 2','1.00','Y',4,11,null,26,null,'N',20,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'MFPPrintBWPrice1',null,10,null,null,'TEXT:1 to 10',null,'A','Y','MFP black and white print charge per page, tier 1','1.00','Y',5,11,null,26,null,'N',20,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'MFPPrintBWTier1',null,5,null,null,'TEXT:1 to 5',null,'A','Y','MFP black and white print tier 2 threshold (number of pages)','20','Y',6,11,null,26,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'MFPPrintBWPrice2',null,10,null,null,'TEXT:1 to 10',null,'A','Y','MFP black and white print charge per page, tier 2','0.50','Y',7,11,null,26,null,'N',20,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'DisplayMFPColorCopies',null,5,null,null,'RADIO:True=True;False=False',null,'A','Y','Shows or hides MFP Color Copies on Pricing and Billing Details','True','Y',8,11,null,26,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'MFPCopyColorPrice1',null,10,null,null,'TEXT:1 to 10',null,'A','Y','MFP color copy charge per page, tier 1','1.50','Y',9,11,null,26,null,'N',20,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'MFPCopyColorTier1',null,5,null,null,'TEXT:1 to 5',null,'A','Y','MFP color copy tier 2 threshold (number of pages)','20','Y',10,11,null,26,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'MFPCopyColorPrice2',null,10,null,null,'TEXT:1 to 10',null,'A','Y','MFP color copy charge per page, tier 2','1.00','Y',11,11,null,26,null,'N',20,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'MFPCopyBWPrice1',null,10,null,null,'TEXT:1 to 10',null,'A','Y','MFP black and white copy charge per page, tier 1','1.00','Y',12,11,null,26,null,'N',20,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'MFPCopyBWTier1',null,5,null,null,'TEXT:1 to 5',null,'A','Y','MFP black and white copy tier 2 threshold (number of pages)','20','Y',13,11,null,26,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'MFPCopyBWPrice2',null,10,null,null,'TEXT:1 to 10',null,'A','Y','MFP black and white copy charge per page, tier 2','0.50','Y',14,11,null,26,null,'N',20,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'MFPScanPrice1',null,10,null,null,'TEXT:1 to 10',null,'A','Y','MFP scan charge per page, tier 1','1.00','Y',15,11,null,26,null,'N',20,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'MFPScanTier1',null,5,null,null,'TEXT:1 to 5',null,'A','Y','MFP scan tier 2 threshold (number of pages)','20','Y',16,11,null,26,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'MFPScanPrice2',null,10,null,null,'TEXT:1 to 10',null,'A','Y','MFP scan charge per page, tier 2','0.50','Y',17,11,null,26,null,'N',20,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'MFPFaxIntlPrice1',null,10,null,null,'TEXT:1 to 10',null,'A','Y','MFP international fax charge per page, tier 1','1.50','Y',18,11,null,26,null,'N',20,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'MFPFaxIntlTier1',null,5,null,null,'TEXT:1 to 5',null,'A','Y','MFP international fax tier 2 threshold (number of pages)','20','Y',19,11,null,26,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'MFPFaxIntlPrice2',null,10,null,null,'TEXT:1 to 10',null,'A','Y','MFP international fax charge per page, tier 2','1.00','Y',20,11,null,26,null,'N',20,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'MFPFaxLDPrice1',null,10,null,null,'TEXT:1 to 10',null,'A','Y','MFP long distance fax charge per page, tier 1','0.80','Y',21,11,null,26,null,'N',20,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'MFPFaxLDTier1',null,5,null,null,'TEXT:1 to 5',null,'A','Y','MFP long distance fax tier 2 threshold (number of pages)','20','Y',22,11,null,26,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'MFPFaxLDPrice2',null,10,null,null,'TEXT:1 to 10',null,'A','Y','MFP long distance fax charge per page, tier 2','0.60','Y',23,11,null,26,null,'N',20,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'MFPFaxLocalPrice1',null,10,null,null,'TEXT:1 to 10',null,'A','Y','MFP local fax charge per page, tier 1','0.50','Y',24,11,null,26,null,'N',20,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'MFPFaxLocalTier1',null,5,null,null,'TEXT:1 to 5',null,'A','Y','MFP local fax tier 2 threshold (number of pages)','20','Y',25,11,null,26,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'MFPFaxLocalPrice2',null,10,null,null,'TEXT:1 to 10',null,'A','Y','MFP local fax charge per page, tier 2','0.30','Y',26,11,null,26,null,'N',20,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'FaxIntlPrefixList',null,200,null,null,'TEXT:0 to 200',null,'A','Y','International fax number prefixes. Multiple prefixes should be separated by the pipe symbol |.',null,'Y',27,11,null,26,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'FaxLocalPrefixList',null,200,null,null,'TEXT:0 to 200',null,'A','Y','Local fax number prefixes. Multiple prefixes should be separated by the pipe symbol |.',null,'Y',28,11,null,26,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'FaxLocalDigits',null,3,null,null,'TEXT:1 to 3',null,'A','Y','If the number of digits in the dialed fax number is <= FaxLocalDigits, it will be counted as a local fax','7','Y',29,11,null,26,null,'N',20,'Y');

ins_kiosk_template (LN_CFG_METADATA_FT_ID,'SnapLabFlag',null,5,null,null,'RADIO:True=True;False=False',null,'A','N','Enables SnapLab features','False','Y',1,11,null,33,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'SnapLabCOMPort',null,2,null,null,'TEXT:1 to 2',null,'A','N','SnapLab COM port number','1','Y',2,11,null,33,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'SnapLabCOMSettings',null,12,null,null,'TEXT:1 to 12',null,'A','N','SnapLab COM port baud rate, parity, data bit, stop bit','38400,n,8,1','Y',3,11,null,33,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'SnapLabAuthTimeout',null,3,null,null,'TEXT:1 to 3',null,'A','N','SnapLab authorization timeout in seconds, used as countdown on the main screen. Customer needs to swipe their card within this time period to pay for their pictures.','60','Y',4,11,null,33,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'AuthMultiplier',null,5,null,null,'TEXT:1 to 5',null,'A','N','SnapLab authorization multiplier. Intended SnapLab total is multiplied by this value to calculate the authorization amount.','1.50','Y',5,11,null,33,null,'N',20,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'MinAuthAmount',null,10,null,null,'TEXT:1 to 10',null,'A','N','SnapLab minimum authorization amount, used as authorization amount if calculated SnapLab total is less than this amount','1.00','Y',6,11,null,33,null,'N',20,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'SnapLabAlertFlag',null,5,null,null,'RADIO:True=True;False=False',null,'A','N','Enables prints remaining alerts sent to Kiosk_Email_List recipients','False','Y',7,11,null,33,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'SnapLabRemainingMin',null,5,null,null,'TEXT:1 to 5',null,'A','N','SnapLab minimum prints remaining alert threshold value','50','Y',8,11,null,33,null,'N',3,'Y');

ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Terminate_Form_Caption',null,100,null,null,'TEXT:1 to 100',null,'A','N','Caption of the "Your Session" form','Your Session','Y',1,11,null,22,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Terminate_Form_Message',null,200,null,null,'TEXT:1 to 200',null,'A','N','Message on the "Your Session" form','To end your session press END button','Y',2,11,null,22,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Usage_Fee',null,10,null,null,'TEXT:1 to 10',null,'A','N','Usage fee','3.00','Y',3,11,null,22,null,null,20,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Usage_Minutes',null,5,null,null,'TEXT:1 to 5',null,'A','N','Allowed usage minutes','5','Y',4,11,null,22,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Charge_On_Time_Flag',null,5,null,null,'RADIO:Y=Y;N=N',null,'A','N','Charge based on time or number of cycles','Y','Y',5,11,null,22,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Swings_Allowed',null,5,null,null,'TEXT:1 to 5',null,'A','N','Allowed number of cycles','3','Y',6,11,null,22,null,'N',3,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Time_Remaining_Message',null,50,null,null,'TEXT:1 to 50',null,'A','N','Remaining minute message','remaining minute','Y',7,11,null,22,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Time_Final_Minute',null,50,null,null,'TEXT:1 to 50',null,'A','N','Final minute message','Final Minute','Y',8,11,null,22,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Shots_Remaining_Message',null,50,null,null,'TEXT:1 to 50',null,'A','N','Remaining shot message','remaining shot','Y',9,11,null,22,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'Kiosk_Final_Shot_Message',null,50,null,null,'TEXT:1 to 50',null,'A','N','Final shot message','Final Shot','Y',10,11,null,22,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'P3P_Kiosk_Path',null,200,null,null,'TEXT:0 to 200',null,'A','N','Location of the application launched in Kiosk mode',null,'Y',11,11,null,22,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'P3P_Kiosk_Application',null,200,null,null,'TEXT:0 to 200',null,'A','N','Executable name of the application launched in Kiosk mode',null,'Y',12,11,null,22,null,null,19,'Y');

ins_kiosk_template (LN_CFG_METADATA_FT_ID,'RECEIPT_HELP_PHONE',null,20,null,null,'TEXT:1 to 20',null,'A','N','Receipt help phone number','1-888-561-4748','Y',1,11,null,16,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'SERVICE_1_NAME',null,20,null,null,'TEXT:1 to 20',null,'A','N','Service 1 name','Internet','Y',2,11,null,16,null,null,19,'Y');
ins_kiosk_template (LN_CFG_METADATA_FT_ID,'ACCESS_CARD_FIXED_FEE',null,10,null,null,'TEXT:1 to 10',null,'A','N','Access card fixed fee','5.00','Y',3,11,null,16,null,'N',20,'Y');

COMMIT;

END;
/

declare
LV_CFG_METADATA_FT_NAME VARCHAR2(21) := 'MEI-GENERIC-MAP';
LN_CFG_METADATA_FT_ID NUMBER;
count_rows NUMBER;
   procedure ins_mei_template
   (P_FILE_TRANSFER_ID IN NUMBER,
      P_DEVICE_SETTING_PARAMETER_CD IN VARCHAR2,	
      P_FIELD_OFFSET IN NUMBER,	
      P_FIELD_SIZE IN NUMBER,	
      P_ALIGN IN CHAR,	
      P_PAD_CHAR IN CHAR,	
      P_EDITOR IN VARCHAR2,	
      P_CONVERTER IN VARCHAR2,	
      P_DATA_MODE IN CHAR,	
      P_DISPLAY IN CHAR,	
      P_DESCRIPTION IN VARCHAR2,	
      P_CONFIG_TEMPLATE_SET_VALUE IN VARCHAR2,
      P_CLIENT_SEND IN CHAR,	
      P_DISPLAY_ORDER IN NUMBER,	
      P_DEVICE_TYPE_ID IN NUMBER,	
      P_PROPERTY_LIST_VERSION IN NUMBER,	
      P_CATEGORY_ID IN NUMBER,	
      P_ALT_NAME IN VARCHAR2,	
      P_DATA_MODE_AUX IN CHAR,	
      P_REGEX_ID IN NUMBER, 
      P_ACTIVE IN CHAR)
  is
   begin
		select count(1) into count_rows from DEVICE.CONFIG_TEMPLATE_SETTING where file_transfer_id = P_FILE_TRANSFER_ID and field_offset = P_FIELD_OFFSET;
   if count_rows > 0 then
    --dbms_output.put_line('### UPDATING! PARAMETER_CD='||P_DEVICE_SETTING_PARAMETER_CD);
    update DEVICE.CONFIG_TEMPLATE_SETTING CTS
    SET 
	  CTS.DEVICE_SETTING_PARAMETER_CD = P_DEVICE_SETTING_PARAMETER_CD,
      CTS.FIELD_OFFSET = P_FIELD_OFFSET,
      CTS.FIELD_SIZE = P_FIELD_SIZE,	
      CTS.ALIGN = P_ALIGN,	
      CTS.PAD_CHAR = P_PAD_CHAR,	
      CTS.EDITOR = P_EDITOR,
      CTS.CONVERTER = P_CONVERTER,	
      CTS.DATA_MODE = P_DATA_MODE,	
      CTS.DISPLAY = P_DISPLAY,	
      CTS.DESCRIPTION = P_DESCRIPTION,		
      CTS.CONFIG_TEMPLATE_SETTING_VALUE = P_CONFIG_TEMPLATE_SET_VALUE,
      CTS.CLIENT_SEND = P_CLIENT_SEND,	
      CTS.DISPLAY_ORDER = P_DISPLAY_ORDER,	
      CTS.DEVICE_TYPE_ID = P_DEVICE_TYPE_ID,	
      CTS.PROPERTY_LIST_VERSION = P_PROPERTY_LIST_VERSION,	
      CTS.CATEGORY_ID = P_CATEGORY_ID,	
      CTS.ALT_NAME = P_ALT_NAME,	
      CTS.DATA_MODE_AUX = P_DATA_MODE_AUX,	
      CTS.REGEX_ID = P_REGEX_ID, 
      CTS.ACTIVE = P_ACTIVE
      where cts.file_transfer_id = P_FILE_TRANSFER_ID and field_offset = P_FIELD_OFFSET;
   else		
      insert into DEVICE.CONFIG_TEMPLATE_SETTING 
      (FILE_TRANSFER_ID,
      DEVICE_SETTING_PARAMETER_CD,	
      FIELD_OFFSET,	
      FIELD_SIZE,	
      ALIGN,	
      PAD_CHAR,	
      EDITOR,	
      CONVERTER,	
      DATA_MODE,	
      DISPLAY,	
      DESCRIPTION,		
      CONFIG_TEMPLATE_SETTING_VALUE,	
      CLIENT_SEND,	
      DISPLAY_ORDER,	
      DEVICE_TYPE_ID,	
      PROPERTY_LIST_VERSION,	
      CATEGORY_ID,	
      ALT_NAME,	
      DATA_MODE_AUX,	
      REGEX_ID, 
       ACTIVE)
      values
      (P_FILE_TRANSFER_ID,
      P_DEVICE_SETTING_PARAMETER_CD,	
      P_FIELD_OFFSET,	
      P_FIELD_SIZE,	
      P_ALIGN,	
      P_PAD_CHAR,	
      P_EDITOR,	
      P_CONVERTER,	
      P_DATA_MODE,	
      P_DISPLAY,	
      P_DESCRIPTION,	
      P_CONFIG_TEMPLATE_SET_VALUE,	
      P_CLIENT_SEND,	
      P_DISPLAY_ORDER,	
      P_DEVICE_TYPE_ID,	
      P_PROPERTY_LIST_VERSION,	
      P_CATEGORY_ID,	
      P_ALT_NAME,	
      P_DATA_MODE_AUX,	
      P_REGEX_ID,
      P_ACTIVE);
	end if;
       
       EXCEPTION
          WHEN OTHERS
          THEN
            DBMS_OUTPUT.put_line ( 'Insert of config default did not happen.' );
      END;
BEGIN
	INSERT INTO DEVICE.FILE_TRANSFER(FILE_TRANSFER_NAME, FILE_TRANSFER_TYPE_CD, FILE_TRANSFER_COMMENT)
	SELECT LV_CFG_METADATA_FT_NAME, 19, 'Placeholder for the MEI master config template settings' FROM DUAL
	WHERE NOT EXISTS (SELECT 1 FROM DEVICE.FILE_TRANSFER WHERE FILE_TRANSFER_NAME = LV_CFG_METADATA_FT_NAME);

	SELECT FILE_TRANSFER_ID INTO LN_CFG_METADATA_FT_ID 
	FROM DEVICE.FILE_TRANSFER WHERE FILE_TRANSFER_NAME = LV_CFG_METADATA_FT_NAME;

	UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
	SET FILE_TRANSFER_ID = LN_CFG_METADATA_FT_ID
	WHERE DEVICE_TYPE_ID = 6
		AND FILE_TRANSFER_ID != LN_CFG_METADATA_FT_ID;

  	ins_mei_template (LN_CFG_METADATA_FT_ID,'IP Address',0,12,'L','f','TEXT:0 to 12',null,'A','Y','Server IP Address','129041132163','Y',1,6,null,null,null,null,1,'Y');
	ins_mei_template (LN_CFG_METADATA_FT_ID,'Server Port',12,5,'R','0','TEXT:0 to 5',null,'A','Y','Server TCP Port','443','Y',2,6,null,null,null,null,1,'Y');
	ins_mei_template (LN_CFG_METADATA_FT_ID,'Call In Time',17,2,'R','0','TEXT:0 to 4','HEX','H','Y','Enter number of minutes after midnight in packed BCD format to set the scheduled call-in to the server.','245','Y',3,6,null,null,null,'N',3,'Y');
	ins_mei_template (LN_CFG_METADATA_FT_ID,'Retry Interval',19,1,'R','0','TEXT:0 to 2','HEX','H','Y','Enter a two digit minute re-try timer in a packed BCD format to set how many minutes the terminal will wait (after failing to connect to the USALive server) before re-trying to make contact with/connection to the server.','15','Y',4,6,null,null,null,'N',3,'Y');
	ins_mei_template (LN_CFG_METADATA_FT_ID,'Firmware Version',20,2,'R','0','TEXT:0 to 4','HEX','H','Y','Firmware Version',null,'Y',5,6,null,null,null,null,1,'Y');
	ins_mei_template (LN_CFG_METADATA_FT_ID,'Call In Num X Ctns',22,2,'R','0','TEXT:0 to 4','HEX','H','Y','Undefined','200','Y',6,6,null,null,null,null,1,'Y');
	ins_mei_template (LN_CFG_METADATA_FT_ID,'No Contact Disable Time',24,2,'R','0','TEXT:0 to 4','HEX','H','Y','Undefined','7','Y',7,6,null,null,null,null,1,'Y');
	ins_mei_template (LN_CFG_METADATA_FT_ID,'Sim Pin',26,2,'R','0','TEXT:0 to 4','HEX','H','Y','Sim Card Pin Number','7221','Y',8,6,null,null,null,null,1,'Y');
	ins_mei_template (LN_CFG_METADATA_FT_ID,'Sim Number',28,10,'R','0','TEXT:0 to 20','HEX','H','Y','Undefined',null,'Y',9,6,null,null,null,'N',5,'Y');
	ins_mei_template (LN_CFG_METADATA_FT_ID,'Pass Card Mask',38,8,'L','a','TEXT:0 to 16',null,'A','Y','Undefined','bbbbbbbb','Y',10,6,null,null,null,null,8,'Y');
	ins_mei_template (LN_CFG_METADATA_FT_ID,'GSM Modem Phone Number',46,11,'L','a','TEXT:0 to 11',null,'A','Y','Undefined',null,'Y',11,6,null,null,null,null,19,'Y');

    COMMIT;
END;
/


ALTER TABLE DEVICE.DEVICE ADD (
	FIRMWARE_VERSION VARCHAR2(200)
);

CREATE INDEX DEVICE.IDX_DEVICE_FIRMWARE_VERSION ON DEVICE.DEVICE (FIRMWARE_VERSION) TABLESPACE DEVICE_INDX ONLINE;
