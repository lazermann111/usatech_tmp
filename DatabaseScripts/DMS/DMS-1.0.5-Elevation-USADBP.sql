WHENEVER SQLERROR EXIT FAILURE COMMIT;

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/PSS/PKG_SETTLEMENT.pbk?rev=1.50
CREATE OR REPLACE PACKAGE BODY PSS.PKG_SETTLEMENT AS
    FUNCTION AFTER_SETTLE_TRAN_STATE_CD(
        pc_auth_type_cd PSS.AUTH.AUTH_TYPE_CD%TYPE,
        pc_refund_type_cd PSS.REFUND.REFUND_TYPE_CD%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pc_device_result_type_cd PSS.TRAN.TRAN_DEVICE_RESULT_TYPE_CD%TYPE)
    RETURN PSS.TRAN.TRAN_STATE_CD%TYPE
    PARALLEL_ENABLE DETERMINISTIC
    IS       
    BEGIN
        IF pc_auth_result_cd = 'Y' THEN
            IF pc_auth_type_cd IS NOT NULL THEN
                IF pc_auth_type_cd IN('C', 'V', 'E', 'I') THEN
                    IF pc_device_result_type_cd IS NULL THEN
                        RETURN 'V'; -- processed reversal
                    ELSE
                        RETURN 'C'; -- Cancelled 
                    END IF;
                ELSE
                    RETURN 'D'; -- Complete
                END IF;                    
            ELSIF pc_refund_type_cd IS NOT NULL THEN
                IF pc_refund_type_cd IN('C', 'V') THEN
                    RETURN 'C'; -- Cancelled 
                ELSE
                    RETURN 'D'; -- Complete
                END IF;
            ELSE
                RAISE_APPLICATION_ERROR(-20560, 'Both auth type cd and refund type cd are null');
            END IF;
        ELSIF pc_auth_result_cd = 'P' THEN
            RETURN 'Q'; -- settlement processed
        ELSIF pc_auth_result_cd = 'N' THEN
            RETURN 'N'; -- PROCESSED_SERVER_SETTLEMENT_INCOMPLETE
        ELSIF pc_auth_result_cd = 'F' THEN
            RETURN 'R'; -- PROCESSED_SERVER_SETTLEMENT_ERROR
        ELSIF pc_auth_result_cd = 'O' THEN
            IF pc_auth_type_cd IS NOT NULL THEN
                IF pc_auth_type_cd IN('C', 'V', 'E', 'I') THEN
                    IF pc_device_result_type_cd IS NULL THEN
                        RETURN 'V'; -- processed reversal
                    ELSE
                        RETURN 'C'; -- Cancelled 
                    END IF;
                ELSE
                    RETURN 'E'; -- Complete - Error
                END IF;                    
            ELSIF pc_refund_type_cd IS NOT NULL THEN
                IF pc_refund_type_cd IN('C', 'V') THEN
                    RETURN 'C'; -- Cancelled 
                ELSE
                    RETURN 'E'; -- Complete - Error
                END IF;
            ELSE
                RAISE_APPLICATION_ERROR(-20560, 'Both auth type cd and refund type cd are null');
            END IF;
        END IF;
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    FUNCTION CREATE_TERMINAL_BATCH(
        pn_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE)
    RETURN PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE
    IS
        ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
    BEGIN
        SELECT PSS.SEQ_TERMINAL_BATCH_ID.NEXTVAL
          INTO ln_terminal_batch_id
          FROM DUAL;
        INSERT INTO PSS.TERMINAL_BATCH (
                TERMINAL_BATCH_ID,
                TERMINAL_ID,
                TERMINAL_BATCH_NUM,
                TERMINAL_BATCH_OPEN_TS,
                TERMINAL_BATCH_CYCLE_NUM,
                TERMINAL_CAPTURE_FLAG) 
         SELECT ln_terminal_batch_id, 
                pn_terminal_id, 
                T.TERMINAL_NEXT_BATCH_NUM,
                SYSDATE,
                T.TERMINAL_BATCH_CYCLE_NUM,
                A.TERMINAL_CAPTURE_FLAG
           FROM PSS.TERMINAL T
           JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
           JOIN AUTHORITY.AUTHORITY A ON M.AUTHORITY_ID = A.AUTHORITY_ID
          WHERE TERMINAL_ID = pn_terminal_id;
          
         UPDATE PSS.TERMINAL
            SET TERMINAL_NEXT_BATCH_NUM = CASE 
                    WHEN TERMINAL_NEXT_BATCH_NUM >= TERMINAL_MAX_BATCH_NUM THEN NVL(TERMINAL_MIN_BATCH_NUM, 1) /* reset batch num */ 
                    ELSE TERMINAL_NEXT_BATCH_NUM + 1 /* increment batch num */
                END,
                TERMINAL_BATCH_CYCLE_NUM = CASE 
                    WHEN TERMINAL_NEXT_BATCH_NUM >= TERMINAL_MAX_BATCH_NUM THEN TERMINAL_BATCH_CYCLE_NUM + 1 /* next cycle */ 
                    ELSE TERMINAL_BATCH_CYCLE_NUM /* same cycle */
                END
            WHERE TERMINAL_ID = pn_terminal_id;
        RETURN ln_terminal_batch_id;
    END;
            
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    FUNCTION GET_AVAILABLE_TERMINAL_BATCH(
        pn_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE,
        pn_allowed_trans OUT PLS_INTEGER,
        pb_create_if_needed BOOLEAN)
    RETURN PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE
    IS
        ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
        lc_is_closed CHAR(1);
        ln_max_tran PSS.TERMINAL.TERMINAL_BATCH_MAX_TRAN%TYPE; 
        ln_terminal_state_id PSS.TERMINAL.TERMINAL_STATE_ID%TYPE;
        lc_terminal_capture_flag PSS.TERMINAL_BATCH.TERMINAL_CAPTURE_FLAG%TYPE;
        ln_attempts PLS_INTEGER;
    BEGIN
        -- get last terminal batch record and verify that it is open       
        SELECT MAX(TERMINAL_BATCH_ID), MAX(DECODE(TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y')), MAX(TERMINAL_BATCH_MAX_TRAN), MAX(TERMINAL_STATE_ID), MAX(NVL(TERMINAL_CAPTURE_FLAG, 'N'))
          INTO ln_terminal_batch_id, lc_is_closed, ln_max_tran, ln_terminal_state_id, lc_terminal_capture_flag
          FROM (SELECT TB.TERMINAL_BATCH_ID,
                       TB.TERMINAL_BATCH_NUM, 
                       TB.TERMINAL_BATCH_CYCLE_NUM, 
                       TB.TERMINAL_BATCH_CLOSE_TS,
                       COALESCE(T.TERMINAL_BATCH_MAX_TRAN, AU.AUTHORITY_BATCH_MAX_TRAN, 999) TERMINAL_BATCH_MAX_TRAN,
                       T.TERMINAL_STATE_ID,
                       TB.TERMINAL_CAPTURE_FLAG
                  FROM PSS.TERMINAL T
                  JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
                  JOIN AUTHORITY.AUTHORITY AU ON M.AUTHORITY_ID = AU.AUTHORITY_ID 
                  LEFT OUTER JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID                   
                 WHERE T.TERMINAL_ID = pn_terminal_id
                 ORDER BY TB.TERMINAL_BATCH_CYCLE_NUM DESC,
                          TB.TERMINAL_BATCH_NUM DESC)
         WHERE ROWNUM = 1;
        pn_allowed_trans := ln_max_tran;
        IF ln_terminal_state_id NOT IN(3) THEN
            RAISE_APPLICATION_ERROR(-20559, 'Terminal ' || pn_terminal_id || ' is not locked and a new terminal batch can not be created for it');
        ELSIF ln_terminal_batch_id IS NULL THEN
            IF pb_create_if_needed THEN
                ln_terminal_batch_id := CREATE_TERMINAL_BATCH(pn_terminal_id);
            END IF;
        ELSIF lc_is_closed  = 'Y' THEN
            IF pb_create_if_needed THEN
                ln_terminal_batch_id := CREATE_TERMINAL_BATCH(pn_terminal_id);
            END IF;
        ELSE
            -- do we need a new batch?
            SELECT ln_max_tran - COUNT(DISTINCT TRAN_ID)
              INTO pn_allowed_trans
              FROM (SELECT A.TRAN_ID
                      FROM PSS.AUTH A
                     WHERE A.TERMINAL_BATCH_ID = ln_terminal_batch_id
                    UNION ALL
                    SELECT R.TRAN_ID
                      FROM PSS.REFUND R
                     WHERE R.TERMINAL_BATCH_ID = ln_terminal_batch_id);
            IF lc_terminal_capture_flag = 'Y' THEN              
                IF pn_allowed_trans > 0 THEN
                    SELECT COUNT(*)
                      INTO ln_attempts
                      FROM PSS.SETTLEMENT_BATCH
                     WHERE TERMINAL_BATCH_ID = ln_terminal_batch_id;
                END IF;
                IF ln_attempts > 0 OR pn_allowed_trans <= 0 THEN
                    -- Create new terminal batch
                    IF pb_create_if_needed THEN
                        ln_terminal_batch_id := CREATE_TERMINAL_BATCH(pn_terminal_id);
                    END IF;
                    pn_allowed_trans := ln_max_tran;
                END IF;
            END IF;
        END IF;
        
        RETURN ln_terminal_batch_id;
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE GET_PENDING_SETTLEMENTS(
        pn_payment_subtype_key_id PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE, 
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pc_ignore_minimums_flag CHAR,
        pn_max_settlements PLS_INTEGER, 
        pn_pending_terminal_batch_ids OUT NUMBER_TABLE)
    IS
    BEGIN
         -- The following should never occur:
        /*
        -- Find retry settlements
        SELECT TERMINAL_BATCH_ID
          BULK COLLECT INTO pn_pending_terminal_batch_ids
          FROM (SELECT TB.TERMINAL_BATCH_ID
                  FROM PSS.SETTLEMENT_BATCH SB 
                  JOIN PSS.TERMINAL_BATCH TB ON TB.TERMINAL_BATCH_ID = SB.TERMINAL_BATCH_ID
                 WHERE TB.TERMINAL_ID = pn_payment_subtype_key_id 
                   AND SB.SETTLEMENT_BATCH_STATE_ID = 4 
                 ORDER BY TB.TERMINAL_BATCH_OPEN_TS ASC)
         WHERE ROWNUM <= pn_max_settlements;
     
        IF pn_pending_terminal_batch_ids.COUNT > 0 THEN
            RETURN;
        END IF;
        */
        -- Find new open batches
        SELECT TERMINAL_BATCH_ID
          BULK COLLECT INTO pn_pending_terminal_batch_ids
          FROM (
            SELECT C.TERMINAL_BATCH_ID
              FROM PSS.MERCHANT M
              JOIN AUTHORITY.AUTHORITY AU ON M.AUTHORITY_ID = AU.AUTHORITY_ID
              JOIN (SELECT TERMINAL_ID, TERMINAL_BATCH_ID, MERCHANT_ID, TERMINAL_BATCH_OPEN_TS, 
                           TERMINAL_BATCH_MIN_TRAN, TERMINAL_BATCH_MAX_TRAN, TERMINAL_MIN_BATCH_CLOSE_HR, TERMINAL_MAX_BATCH_CLOSE_HR, 
                           SUM(CASE WHEN TRAN_STATE_CD IN('T') THEN 1 ELSE 0 END) NUM_TRAN
                      FROM (SELECT T.TERMINAL_ID, 
                                   TB.TERMINAL_BATCH_ID,
                                   T.MERCHANT_ID,
                                   X.TRAN_ID,
                                   X.TRAN_STATE_CD,
                                   T.TERMINAL_BATCH_MIN_TRAN,
                                   T.TERMINAL_BATCH_MAX_TRAN,
                                   T.TERMINAL_MIN_BATCH_CLOSE_HR,
                                   T.TERMINAL_MAX_BATCH_CLOSE_HR,
                                   TB.TERMINAL_BATCH_OPEN_TS
                              FROM PSS.TERMINAL T
                              JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
                              JOIN PSS.AUTH A ON TB.TERMINAL_BATCH_ID = A.TERMINAL_BATCH_ID
                              JOIN PSS.TRAN X ON A.TRAN_ID = X.TRAN_ID 
                             WHERE TB.TERMINAL_BATCH_OPEN_TS IS NOT NULL
                               AND DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y') = 'N'
                               AND T.TERMINAL_ID = pn_payment_subtype_key_id
                             UNION 
                            SELECT T.TERMINAL_ID, 
                                   TB.TERMINAL_BATCH_ID,
                                   T.MERCHANT_ID,
                                   X.TRAN_ID,
                                   X.TRAN_STATE_CD,
                                   T.TERMINAL_BATCH_MIN_TRAN,
                                   T.TERMINAL_BATCH_MAX_TRAN,
                                   T.TERMINAL_MIN_BATCH_CLOSE_HR,
                                   T.TERMINAL_MAX_BATCH_CLOSE_HR,
                                   TB.TERMINAL_BATCH_OPEN_TS
                              FROM PSS.TERMINAL T
                              JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
                              JOIN PSS.REFUND R ON TB.TERMINAL_BATCH_ID = R.TERMINAL_BATCH_ID
                              JOIN PSS.TRAN X ON R.TRAN_ID = X.TRAN_ID 
                             WHERE TB.TERMINAL_BATCH_OPEN_TS IS NOT NULL
                               AND DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y') = 'N'
                               AND T.TERMINAL_ID = pn_payment_subtype_key_id) 
                     GROUP BY TERMINAL_ID, TERMINAL_BATCH_ID, MERCHANT_ID, TERMINAL_BATCH_OPEN_TS, TERMINAL_BATCH_MIN_TRAN, TERMINAL_BATCH_MAX_TRAN, TERMINAL_MIN_BATCH_CLOSE_HR, TERMINAL_MAX_BATCH_CLOSE_HR
                     HAVING COUNT(TRAN_STATE_CD) = SUM(CASE WHEN TRAN_STATE_CD IN('T', 'E') THEN 1 ELSE 0 END)) C  ON C.MERCHANT_ID = M.MERCHANT_ID 
             WHERE C.NUM_TRAN > 0
               AND (pc_ignore_minimums_flag = 'Y'
                OR C.NUM_TRAN >= COALESCE(C.TERMINAL_BATCH_MAX_TRAN, AU.AUTHORITY_BATCH_MAX_TRAN, 999) 
                OR SYSDATE >= C.TERMINAL_BATCH_OPEN_TS + COALESCE(C.TERMINAL_MAX_BATCH_CLOSE_HR/24, AU.AUTHORITY_MAX_BATCH_CLOSE_HR/24, 1)
                OR (SYSDATE >= C.TERMINAL_BATCH_OPEN_TS + COALESCE(C.TERMINAL_MIN_BATCH_CLOSE_HR/24, AU.AUTHORITY_MIN_BATCH_CLOSE_HR/24, 0)
                    AND C.NUM_TRAN >= COALESCE(C.TERMINAL_BATCH_MIN_TRAN, AU.AUTHORITY_BATCH_MIN_TRAN, 25)))
             ORDER BY c.TERMINAL_BATCH_OPEN_TS ASC)
         WHERE ROWNUM <= pn_max_settlements;
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE GET_RETRY_SETTLEMENTS(
        pn_payment_subtype_key_id PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE, 
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_settlement_retry_interval NUMBER,
        pn_max_settlements PLS_INTEGER, 
        pn_pending_terminal_batch_ids OUT NUMBER_TABLE)
    IS
    BEGIN
        -- Find retyable batches
        SELECT TERMINAL_BATCH_ID
          BULK COLLECT INTO pn_pending_terminal_batch_ids
          FROM (
            SELECT C.TERMINAL_BATCH_ID
              FROM (SELECT TERMINAL_ID, TERMINAL_BATCH_ID, TERMINAL_BATCH_OPEN_TS, 
                           SUM(CASE WHEN TRAN_STATE_CD IN('R', 'N') THEN 1 ELSE 0 END) NUM_TRAN
                      FROM (SELECT T.TERMINAL_ID, 
                                   TB.TERMINAL_BATCH_ID,
                                   X.TRAN_ID,
                                   X.TRAN_STATE_CD,
                                   TB.TERMINAL_BATCH_OPEN_TS
                              FROM PSS.TERMINAL T
                              JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
                              JOIN PSS.AUTH A ON TB.TERMINAL_BATCH_ID = A.TERMINAL_BATCH_ID
                              JOIN PSS.TRAN X ON A.TRAN_ID = X.TRAN_ID 
                             WHERE TB.TERMINAL_BATCH_OPEN_TS IS NOT NULL
                               AND DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y') = 'N'
                               AND T.TERMINAL_ID = pn_payment_subtype_key_id
                             UNION 
                            SELECT T.TERMINAL_ID, 
                                   TB.TERMINAL_BATCH_ID,
                                   X.TRAN_ID,
                                   X.TRAN_STATE_CD,
                                   TB.TERMINAL_BATCH_OPEN_TS
                              FROM PSS.TERMINAL T
                              JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
                              JOIN PSS.REFUND R ON TB.TERMINAL_BATCH_ID = R.TERMINAL_BATCH_ID
                              JOIN PSS.TRAN X ON R.TRAN_ID = X.TRAN_ID 
                             WHERE TB.TERMINAL_BATCH_OPEN_TS IS NOT NULL
                               AND DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y') = 'N'
                               AND T.TERMINAL_ID = pn_payment_subtype_key_id) 
                     GROUP BY TERMINAL_ID, TERMINAL_BATCH_ID, TERMINAL_BATCH_OPEN_TS
                     HAVING COUNT(TRAN_STATE_CD) = SUM(CASE WHEN TRAN_STATE_CD IN('R', 'N', 'E') THEN 1 ELSE 0 END)) C
             WHERE C.NUM_TRAN > 0
               AND SYSDATE >= pn_settlement_retry_interval + (
                        SELECT NVL(MAX(LA.SETTLEMENT_BATCH_START_TS), MIN_DATE)
                          FROM PSS.SETTLEMENT_BATCH LA
                          WHERE C.TERMINAL_BATCH_ID = LA.TERMINAL_BATCH_ID)
             ORDER BY c.TERMINAL_BATCH_OPEN_TS ASC)
         WHERE ROWNUM <= pn_max_settlements;
    END;

/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE FORCE_SETTLE_BATCH(
        pn_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE,
        pv_force_reason PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_RESP_DESC%TYPE)
    IS
        ln_settlement_batch_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE;
        ln_settlement_batch_state_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_STATE_ID%TYPE;
        ln_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE;
        ln_terminal_state_id PSS.TERMINAL.TERMINAL_STATE_ID%TYPE;
        ln_is_open PLS_INTEGER;
        lv_msg VARCHAR2(4000);
    BEGIN
        -- check terminal state
        SELECT T.TERMINAL_ID, T.TERMINAL_STATE_ID
          INTO ln_terminal_id, ln_terminal_state_id
          FROM PSS.TERMINAL T
          JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
         WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id;
        IF ln_terminal_state_id NOT IN(3,5) THEN
            SELECT 'Terminal ' || ln_terminal_id || ' is currently ' || DECODE(ln_terminal_state_id, 1, 'not locked', 2, 'disabled', 5, 'busy with retry', 'unavailable')
              INTO lv_msg
              FROM DUAL;
            RAISE_APPLICATION_ERROR(-20560, lv_msg);
        END IF;
        
        -- Check status of most recent settlement batch to ensure it is failue or decline
        SELECT MAX(SETTLEMENT_BATCH_ID), NVL(MAX(SETTLEMENT_BATCH_STATE_ID), 0)
          INTO ln_settlement_batch_id, ln_settlement_batch_state_id
          FROM (
            SELECT SETTLEMENT_BATCH_ID, SETTLEMENT_BATCH_STATE_ID
              FROM PSS.SETTLEMENT_BATCH
             WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id
             ORDER BY SETTLEMENT_BATCH_START_TS DESC, SETTLEMENT_BATCH_ID DESC)
         WHERE ROWNUM = 1;
        IF ln_settlement_batch_state_id NOT IN(2, 3) THEN
            SELECT 'Terminal Batch ' || pn_terminal_batch_id || DECODE(ln_settlement_batch_state_id, 0, ' has not yet been tried', 1, ' was successfully settled already', 4, ' is awaiting a retry', 7, 'was partially settled already', ' is not ready for forced settlement')
              INTO lv_msg
              FROM DUAL;
            RAISE_APPLICATION_ERROR(-20561, lv_msg);
        END IF;
        
        -- create new settlement batch
        SELECT PSS.SEQ_SETTLEMENT_BATCH_ID.NEXTVAL
          INTO ln_settlement_batch_id
          FROM DUAL;            
        
        INSERT INTO PSS.SETTLEMENT_BATCH(
            SETTLEMENT_BATCH_ID,
            SETTLEMENT_BATCH_STATE_ID,
            SETTLEMENT_BATCH_START_TS,
            TERMINAL_BATCH_ID,
            SETTLEMENT_BATCH_RESP_CD,
            SETTLEMENT_BATCH_RESP_DESC,
            SETTLEMENT_BATCH_REF_CD,
            SETTLEMENT_BATCH_END_TS
        ) VALUES (
            ln_settlement_batch_id,
            1,
            SYSDATE,
            pn_terminal_batch_id,
            0,
            pv_force_reason, 
            'FORCE_SETTLEMENT',
            SYSDATE);
        -- add all trans and update their tran state cd
        INSERT INTO PSS.TRAN_SETTLEMENT_BATCH(
            SETTLEMENT_BATCH_ID,
            AUTH_ID,
            TRAN_ID,
            TRAN_SETTLEMENT_B_AMT)
          SELECT DISTINCT 
                 ln_settlement_batch_id,
                 FIRST_VALUE(A.AUTH_ID) OVER (PARTITION BY T.TRAN_ID ORDER BY A.CREATED_TS DESC, A.AUTH_ID DESC) AUTH_ID, 
                 T.TRAN_ID, 
                 FIRST_VALUE(A.AUTH_AMT) OVER (PARTITION BY T.TRAN_ID ORDER BY A.CREATED_TS DESC, A.AUTH_ID DESC) AUTH_AMT
            FROM PSS.TRAN T
            JOIN PSS.AUTH A ON T.TRAN_ID = A.TRAN_ID
           WHERE A.TERMINAL_BATCH_ID = pn_terminal_batch_id
             AND A.AUTH_STATE_ID IN(2,6);
        INSERT INTO PSS.REFUND_SETTLEMENT_BATCH(
            SETTLEMENT_BATCH_ID,
            REFUND_ID,
            TRAN_ID,
            REFUND_SETTLEMENT_B_AMT)
          SELECT DISTINCT 
                 ln_settlement_batch_id,
                 FIRST_VALUE(R.REFUND_ID) OVER (PARTITION BY T.TRAN_ID ORDER BY R.CREATED_TS DESC, R.REFUND_ID DESC) AUTH_ID, 
                 T.TRAN_ID, 
                 FIRST_VALUE(R.REFUND_AMT) OVER (PARTITION BY T.TRAN_ID ORDER BY R.CREATED_TS DESC, R.REFUND_ID DESC) REFUND_AMT
            FROM PSS.TRAN T
            JOIN PSS.REFUND R ON T.TRAN_ID = R.TRAN_ID
           WHERE R.TERMINAL_BATCH_ID = pn_terminal_batch_id
             AND R.REFUND_STATE_ID IN(1);
        UPDATE PSS.TRAN 
           SET TRAN_STATE_CD = 'D'
         WHERE TRAN_STATE_CD IN('R', 'N', 'Q', 'T')
           AND TRAN_ID IN(
            SELECT A.TRAN_ID
              FROM PSS.AUTH A 
             WHERE A.TERMINAL_BATCH_ID = pn_terminal_batch_id
               AND A.AUTH_STATE_ID IN(2,6)
            UNION ALL
            SELECT R.TRAN_ID
              FROM PSS.REFUND R
             WHERE R.TERMINAL_BATCH_ID = pn_terminal_batch_id
               AND R.REFUND_STATE_ID IN(1));
                   
        -- update terminal batch
        UPDATE PSS.TERMINAL_BATCH
           SET TERMINAL_BATCH_CLOSE_TS = SYSDATE
         WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id;
    END;

/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE FORCE_TRAN(
        pn_payment_subtype_key_id PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE, 
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pv_force_reason PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_RESP_DESC%TYPE)
    IS
        lc_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
        ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
        ln_sale_auth_id PSS.AUTH.AUTH_ID%TYPE; 
        ln_force_auth_id PSS.AUTH.AUTH_ID%TYPE;
        ln_force_refund_id PSS.REFUND.REFUND_ID%TYPE;
        ln_allowed_trans PLS_INTEGER;
        ln_settlement_batch_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE;
        ln_auth_amt PSS.AUTH.AUTH_AMT%TYPE;
    BEGIN
        -- check terminal state
        SELECT T.TRAN_STATE_CD
          INTO lc_tran_state_cd
          FROM PSS.TRAN T
         WHERE T.TRAN_ID = pn_tran_id;
        IF lc_tran_state_cd NOT IN('I', 'J') THEN
            RAISE_APPLICATION_ERROR(-20562, 'Tran ' || pn_tran_id || ' is not in INCOMPLETE or INCOMPLETE_ERROR state and cannot be forced');
        END IF;
        
        UPDATE PSS.TRAN
           SET TRAN_STATE_CD = CASE WHEN pv_payment_subtype_class LIKE 'Authority::ISO8583%' THEN 'T' ELSE 'D' END
         WHERE TRAN_ID = pn_tran_id
           AND TRAN_STATE_CD = lc_tran_state_cd; -- make sure it didn't change
        
        IF SQL%ROWCOUNT < 1 THEN
            FORCE_TRAN(pn_payment_subtype_key_id, pv_payment_subtype_class, pn_tran_id, pv_force_reason);
        ELSE
            -- add to batch if necessary
            SELECT MAX(AUTH_ID), MAX(AUTH_AMT)
              INTO ln_sale_auth_id, ln_auth_amt
              FROM (
                SELECT AUTH_ID, AUTH_TS, AUTH_AMT
                  FROM PSS.AUTH
                 WHERE TRAN_ID = pn_tran_id
                   AND AUTH_TYPE_CD IN('U','S','O','A','D','C','E','V','I')
                 ORDER BY AUTH_TS DESC, AUTH_ID DESC)
             WHERE ROWNUM = 1;
            IF ln_sale_auth_id IS NULL THEN
                SELECT MAX(REFUND_ID), MAX(REFUND_AMT)
                  INTO ln_force_refund_id, ln_auth_amt
                  FROM (
                    SELECT CREATED_TS, REFUND_ID, REFUND_AMT
                      FROM PSS.REFUND
                     WHERE TRAN_ID = pn_tran_id
                     ORDER BY CREATED_TS DESC, REFUND_ID DESC)
                 WHERE ROWNUM = 1;
            END IF;             
            IF pv_payment_subtype_class LIKE 'Authority::ISO8583%' THEN
                IF ln_sale_auth_id IS NOT NULL THEN
                    SELECT MAX(TERMINAL_BATCH_ID)
                      INTO ln_terminal_batch_id
                      FROM PSS.AUTH
                     WHERE TRAN_ID = pn_tran_id;
                ELSIF ln_force_refund_id IS NOT NULL THEN
                    SELECT MAX(TERMINAL_BATCH_ID)
                      INTO ln_terminal_batch_id
                      FROM PSS.REFUND
                     WHERE TRAN_ID = pn_tran_id;
                END IF;
                IF ln_terminal_batch_id IS NULL THEN
                    ln_terminal_batch_id := GET_AVAILABLE_TERMINAL_BATCH(pn_payment_subtype_key_id, ln_allowed_trans, TRUE);
                    IF ln_allowed_trans <= 0 THEN
                        RAISE_APPLICATION_ERROR(-20556, 'Terminal Batch ' || ln_terminal_batch_id || ' is full for terminal ' || pn_payment_subtype_key_id || '; Can not add tran ' || pn_tran_id || ' to it');
                    END IF;
                END IF;
            ELSE
                -- create settlement batch record
                SELECT PSS.SEQ_SETTLEMENT_BATCH_ID.NEXTVAL
                  INTO ln_settlement_batch_id
                  FROM DUAL;
                INSERT INTO PSS.SETTLEMENT_BATCH(
                    SETTLEMENT_BATCH_ID,
                    SETTLEMENT_BATCH_STATE_ID,
                    SETTLEMENT_BATCH_START_TS,
                    SETTLEMENT_BATCH_END_TS,
                    SETTLEMENT_BATCH_RESP_CD,
                    SETTLEMENT_BATCH_RESP_DESC
                ) VALUES (
                    ln_settlement_batch_id,
                    1,
                    SYSDATE,
                    SYSDATE,
                    'MANUAL',
                    pv_force_reason);
            END IF;
            IF ln_sale_auth_id IS NOT NULL THEN  
                SELECT PSS.SEQ_AUTH_ID.NEXTVAL
                  INTO ln_force_auth_id
                  FROM DUAL;
                INSERT INTO PSS.AUTH (
                        AUTH_ID,
                        TRAN_ID,
                        AUTH_TYPE_CD,
                        AUTH_STATE_ID,
                        AUTH_PARSED_ACCT_DATA,
                        ACCT_ENTRY_METHOD_CD,
                        AUTH_AMT,
                        AUTH_TS,
                        TERMINAL_BATCH_ID,
                        TRACE_NUMBER,
                        AUTH_RESULT_CD,
                        AUTH_RESP_CD,
                        AUTH_RESP_DESC,
                        AUTH_AMT_APPROVED)
                 SELECT ln_force_auth_id,
                        pn_tran_id,
                        A.AUTH_TYPE_CD,
                        2,
                        a.AUTH_PARSED_ACCT_DATA,
                        a.ACCT_ENTRY_METHOD_CD,
                        a.AUTH_AMT,	    
                        SYSDATE,
                        ln_terminal_batch_id,
                        a.TRACE_NUMBER,
                        'Y',
                        'MANUAL',
                        pv_force_reason,
                        a.AUTH_AMT
                   FROM PSS.AUTH a
                  WHERE a.AUTH_ID = ln_sale_auth_id;
                IF ln_settlement_batch_id IS NOT NULL THEN
                    INSERT INTO PSS.TRAN_SETTLEMENT_BATCH(
                        SETTLEMENT_BATCH_ID,
                        AUTH_ID,
                        TRAN_ID,
                        TRAN_SETTLEMENT_B_AMT
                    ) VALUES (
                        ln_settlement_batch_id,
                        ln_force_auth_id,
                        pn_tran_id,
                        ln_auth_amt
                    );               
                END IF;
            ELSIF ln_force_refund_id IS NOT NULL THEN
                UPDATE PSS.REFUND
                   SET REFUND_STATE_ID = 1,
                       REFUND_RESP_CD = 'MANUAL',
                       REFUND_RESP_DESC = pv_force_reason,
                       REFUND_AUTHORITY_TS = SYSDATE,
                       TERMINAL_BATCH_ID = NVL(ln_terminal_batch_id, TERMINAL_BATCH_ID)
                 WHERE REFUND_ID = ln_force_refund_id;
                IF ln_settlement_batch_id IS NOT NULL THEN
                    INSERT INTO PSS.REFUND_SETTLEMENT_BATCH(
                        SETTLEMENT_BATCH_ID,
                        REFUND_ID,
                        TRAN_ID,
                        REFUND_SETTLEMENT_B_AMT
                    ) VALUES (
                        ln_settlement_batch_id,
                        ln_force_refund_id,
                        pn_tran_id,
                        ln_auth_amt
                    );               
                END IF;
            END IF;
        END IF;
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE ERROR_TRAN(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pv_force_reason PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_RESP_DESC%TYPE)
    IS
        lc_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
    BEGIN
        -- check terminal state
        SELECT TRAN_STATE_CD
          INTO lc_tran_state_cd
          FROM PSS.TRAN
         WHERE TRAN_ID = pn_tran_id;
        IF lc_tran_state_cd NOT IN('I', 'J') THEN
            RAISE_APPLICATION_ERROR(-20562, 'Tran ' || pn_tran_id || ' is not in INCOMPLETE or INCOMPLETE_ERROR state and cannot be errored');
        END IF;
        
        UPDATE PSS.TRAN
           SET TRAN_STATE_CD = 'E'
         WHERE TRAN_ID = pn_tran_id
           AND TRAN_STATE_CD = lc_tran_state_cd; -- make sure it didn't change
           
        IF SQL%ROWCOUNT < 1 THEN
            ERROR_TRAN(pn_tran_id, pv_force_reason);
        END IF;
    END;
         
    PROCEDURE MARK_ADMIN_CMD_EXECUTED(
        pn_admin_cmd_id PSS.ADMIN_CMD.ADMIN_CMD_ID%TYPE)
    IS
    BEGIN
        UPDATE PSS.ADMIN_CMD
           SET ADMIN_CMD_STATE_ID = 2,
               CLOSED_UTC_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP)
         WHERE ADMIN_CMD_ID = pn_admin_cmd_id;
    END;

    PROCEDURE MARK_ADMIN_CMD_ERRORED(
        pn_admin_cmd_id PSS.ADMIN_CMD.ADMIN_CMD_ID%TYPE,
        pv_error_msg PSS.ADMIN_CMD.ERROR_MSG%TYPE)
    IS
    BEGIN
        UPDATE PSS.ADMIN_CMD
           SET ADMIN_CMD_STATE_ID = 4,
               CLOSED_UTC_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP),
               ERROR_MSG = pv_error_msg
         WHERE ADMIN_CMD_ID = pn_admin_cmd_id;
    END;
    
    PROCEDURE GET_ACTIONS_FROM_ADMIN_CMDS(
        pn_payment_subtype_key_id PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE, 
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_max_settlements PLS_INTEGER, 
        pn_max_trans PLS_INTEGER,
        pc_allow_add_to_batch CHAR,
        pc_retry_only CHAR,
        pc_settle_processing_enabled CHAR,
        pc_tran_processing_enabled CHAR,
        pn_pending_terminal_batch_ids OUT NUMBER_TABLE,
        pn_pending_tran_ids OUT NUMBER_TABLE)
    IS
        CURSOR l_cur IS
            SELECT ADMIN_CMD_ID, ADMIN_CMD_TYPE_ID
              FROM PSS.ADMIN_CMD
             WHERE PAYMENT_SUBTYPE_KEY_ID = pn_payment_subtype_key_id
               AND PAYMENT_SUBTYPE_CLASS =  pv_payment_subtype_class
               AND ADMIN_CMD_STATE_ID = 1
               AND (pc_settle_processing_enabled = 'Y' OR ADMIN_CMD_TYPE_ID NOT IN(1,2,3,6,8))
               AND (pc_tran_processing_enabled = 'Y' OR ADMIN_CMD_TYPE_ID NOT IN(4,5,7))
               AND (pc_retry_only != 'Y' OR ADMIN_CMD_TYPE_ID IN(2, 3, 4, 5, 6, 7))
               AND (pc_allow_add_to_batch = 'Y' OR pv_payment_subtype_class NOT LIKE 'Authority::ISO8583%' OR ADMIN_CMD_TYPE_ID NOT IN(5))
             ORDER BY PRIORITY ASC, CREATED_UTC_TS ASC;
    BEGIN
        -- get the first command and translate it into a terminal batch or tran to process
        FOR l_rec IN l_cur LOOP
            IF l_rec.ADMIN_CMD_TYPE_ID IN(1,2,3,6,5,8) AND pv_payment_subtype_class NOT LIKE 'Authority::ISO8583%' THEN
                MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Settlement commands are not allowed for Payment Subtype Class ''' || pv_payment_subtype_class || '''');
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 1 THEN -- Settle All
                GET_PENDING_SETTLEMENTS(
                    pn_payment_subtype_key_id, 
                    pv_payment_subtype_class, 
                    'Y', 
                    pn_max_settlements,
                    pn_pending_terminal_batch_ids);
                IF pn_pending_terminal_batch_ids IS NULL OR pn_pending_terminal_batch_ids.COUNT < pn_max_settlements THEN
                    -- Mark command executed
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                END IF;
                IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
                    RETURN; -- found something to process so exit
                END IF;
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 2 THEN -- Retry All
                GET_RETRY_SETTLEMENTS(
                    pn_payment_subtype_key_id, 
                    pv_payment_subtype_class,
                    0,
                    pn_max_settlements, 
                    pn_pending_terminal_batch_ids);
                IF pn_pending_terminal_batch_ids IS NULL OR pn_pending_terminal_batch_ids.COUNT < pn_max_settlements THEN
                    -- Mark command executed
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                END IF;
                IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
                    RETURN; -- found something to process so exit
                END IF;   
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 3 THEN -- Retry Batch
                SELECT TERMINAL_BATCH_ID
                  BULK COLLECT INTO pn_pending_terminal_batch_ids
                  FROM (
                    SELECT C.TERMINAL_BATCH_ID
                      FROM (SELECT TERMINAL_ID, TERMINAL_BATCH_ID, TERMINAL_BATCH_OPEN_TS, 
                                   SUM(CASE WHEN TRAN_STATE_CD IN('R','N') THEN 1 ELSE 0 END) NUM_TRAN
                              FROM (SELECT T.TERMINAL_ID, 
                                           TB.TERMINAL_BATCH_ID,
                                           X.TRAN_ID,
                                           X.TRAN_STATE_CD,
                                           TB.TERMINAL_BATCH_OPEN_TS
                                      FROM PSS.TERMINAL T
                                      JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
                                      JOIN PSS.AUTH A ON TB.TERMINAL_BATCH_ID = A.TERMINAL_BATCH_ID
                                      JOIN PSS.TRAN X ON A.TRAN_ID = X.TRAN_ID 
                                     WHERE TB.TERMINAL_BATCH_OPEN_TS IS NOT NULL
                                       AND DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y') = 'N'
                                       AND T.TERMINAL_ID = pn_payment_subtype_key_id
                                     UNION 
                                    SELECT T.TERMINAL_ID, 
                                           TB.TERMINAL_BATCH_ID,
                                           X.TRAN_ID,
                                           X.TRAN_STATE_CD,
                                           TB.TERMINAL_BATCH_OPEN_TS
                                      FROM PSS.TERMINAL T
                                      JOIN PSS.TERMINAL_BATCH TB ON T.TERMINAL_ID = TB.TERMINAL_ID
                                      JOIN PSS.REFUND R ON TB.TERMINAL_BATCH_ID = R.TERMINAL_BATCH_ID
                                      JOIN PSS.TRAN X ON R.TRAN_ID = X.TRAN_ID 
                                     WHERE TB.TERMINAL_BATCH_OPEN_TS IS NOT NULL
                                       AND DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y') = 'N'
                                       AND T.TERMINAL_ID = pn_payment_subtype_key_id) 
                             GROUP BY TERMINAL_ID, TERMINAL_BATCH_ID, TERMINAL_BATCH_OPEN_TS
                             HAVING COUNT(TRAN_STATE_CD) = SUM(CASE WHEN TRAN_STATE_CD IN('R', 'N', 'E') THEN 1 ELSE 0 END)) C
                      JOIN PSS.ADMIN_CMD_PARAM ACP ON C.TERMINAL_BATCH_ID = TO_NUMBER_OR_NULL(ACP.PARAM_VALUE)
                     WHERE C.NUM_TRAN > 0
                       AND ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'TERMINAL_BATCH_ID'
                   ORDER BY c.TERMINAL_BATCH_OPEN_TS ASC)
                 WHERE ROWNUM <= pn_max_settlements;
                IF pn_pending_terminal_batch_ids IS NULL OR pn_pending_terminal_batch_ids.COUNT < pn_max_settlements THEN
                    -- Mark command executed
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                END IF;
                IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
                    RETURN; -- found something to process so exit
                END IF;   
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 4 THEN -- Retry Tran
                SELECT TRAN_ID
                  BULK COLLECT INTO pn_pending_tran_ids
                  FROM (SELECT T.TRAN_ID 
                          FROM PSS.TRAN T
                          LEFT OUTER JOIN PSS.AUTH A   ON T.TRAN_ID = A.TRAN_ID
                          LEFT OUTER JOIN PSS.REFUND R ON T.TRAN_ID = R.TRAN_ID
                          JOIN PSS.ADMIN_CMD_PARAM ACP ON T.TRAN_ID = TO_NUMBER_OR_NULL(ACP.PARAM_VALUE)
                         WHERE T.TRAN_STATE_CD IN('I', 'J') /* transaction_incomplete, transaction_incomplete_error */
                           AND T.PAYMENT_SUBTYPE_KEY_ID = pn_payment_subtype_key_id 
                           AND T.PAYMENT_SUBTYPE_CLASS  = pv_payment_subtype_class
                           AND ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                           AND ACP.PARAM_NAME = 'TRAN_ID'
                           AND (pc_allow_add_to_batch = 'Y' OR NVL(A.TERMINAL_BATCH_ID, R.TERMINAL_BATCH_ID) IS NOT NULL)
                         GROUP BY T.TRAN_ID
                         ORDER BY T.TRAN_UPLOAD_TS ASC)
                 WHERE ROWNUM <= pn_max_trans;
                IF pn_pending_tran_ids IS NULL OR pn_pending_tran_ids.COUNT < pn_max_trans THEN
                    -- Mark command executed
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                END IF;
                IF pn_pending_tran_ids IS NOT NULL AND pn_pending_tran_ids.COUNT > 0 THEN 
                    RETURN; -- found something to process so exit
                END IF;
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 5 THEN -- Force Tran
                 DECLARE
                    ln_tran_id PSS.TRAN.TRAN_ID%TYPE;
                    lv_force_reason PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_RESP_DESC%TYPE;
                BEGIN
                    SELECT TO_NUMBER_OR_NULL(ACP.PARAM_VALUE)
                      INTO ln_tran_id
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'TRAN_ID';
                    SELECT ACP.PARAM_VALUE
                      INTO lv_force_reason
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'FORCE_REASON';    
                    FORCE_TRAN(pn_payment_subtype_key_id, pv_payment_subtype_class, ln_tran_id, lv_force_reason);
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Parameter ''TRAN_ID'' not found');
                    WHEN WRONG_TRAN_STATE THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, SQLERRM);
                    WHEN OTHERS THEN
                        IF SQLCODE IN(1, 20556) THEN
                            MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, SQLERRM);
                        ELSE
                            RAISE;
                        END IF;
                END;
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 6 THEN -- Force Batch
                DECLARE
                    ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
                    lv_force_reason PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_RESP_DESC%TYPE;
                BEGIN
                    SELECT TO_NUMBER_OR_NULL(ACP.PARAM_VALUE)
                      INTO ln_terminal_batch_id
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'TERMINAL_BATCH_ID';
                    SELECT ACP.PARAM_VALUE
                      INTO lv_force_reason
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'FORCE_REASON';    
                    FORCE_SETTLE_BATCH(ln_terminal_batch_id, lv_force_reason);
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Parameter ''TERMINAL_BATCH_ID'' or ''FORCE_REASON'' not found');
                    WHEN WRONG_SETTLE_BATCH_STATE THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, SQLERRM);
                    WHEN OTHERS THEN
                        IF SQLCODE = 1 THEN
                            MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, SQLERRM);
                        ELSE
                            RAISE;
                        END IF;
                END;
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 7 THEN -- Error Tran
                 DECLARE
                    ln_tran_id PSS.TRAN.TRAN_ID%TYPE;
                    lv_error_reason PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_RESP_DESC%TYPE;
                BEGIN
                    SELECT TO_NUMBER_OR_NULL(ACP.PARAM_VALUE)
                      INTO ln_tran_id
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'TRAN_ID';
                    SELECT ACP.PARAM_VALUE
                      INTO lv_error_reason
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'ERROR_REASON';    
                    ERROR_TRAN(ln_tran_id, lv_error_reason);
                    MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Parameter ''TRAN_ID'' not found');
                    WHEN WRONG_TRAN_STATE THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, SQLERRM);
                    WHEN OTHERS THEN
                        IF SQLCODE = 1 THEN
                            MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, SQLERRM);
                        ELSE
                            RAISE;
                        END IF;
                END;
            ELSIF l_rec.ADMIN_CMD_TYPE_ID = 8 THEN -- Settle Batch
                DECLARE
                    ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
                    lc_terminal_batch_closed CHAR;
                    ln_num_tran PLS_INTEGER;
                    ln_count_tran PLS_INTEGER;
                    ln_okay_tran PLS_INTEGER;          
                BEGIN
                    SELECT MAX(TO_NUMBER_OR_NULL(ACP.PARAM_VALUE))
                      INTO ln_terminal_batch_id
                      FROM PSS.ADMIN_CMD_PARAM ACP
                     WHERE ACP.ADMIN_CMD_ID = l_rec.ADMIN_CMD_ID
                       AND ACP.PARAM_NAME = 'TERMINAL_BATCH_ID';
                    IF ln_terminal_batch_id IS NULL THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Parameter ''TERMINAL_BATCH_ID'' not found');
                    ELSE
                        SELECT DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y'), 
                           SUM(CASE WHEN X.TRAN_STATE_CD IN('T') THEN 1 ELSE 0 END) NUM_TRAN,
                           COUNT(X.TRAN_STATE_CD) COUNT_TRAN,
                           SUM(CASE WHEN X.TRAN_STATE_CD IN('T', 'E') THEN 1 ELSE 0 END) OKAY_TRAN
                          INTO lc_terminal_batch_closed,
                               ln_num_tran,
                               ln_count_tran,
                               ln_okay_tran
                          FROM PSS.TERMINAL_BATCH TB
                          JOIN (SELECT A.TRAN_ID, A.TERMINAL_BATCH_ID
                                  FROM PSS.AUTH A
                                UNION
                                SELECT R.TRAN_ID, R.TERMINAL_BATCH_ID
                                  FROM PSS.REFUND R) A ON TB.TERMINAL_BATCH_ID = A.TERMINAL_BATCH_ID
                          JOIN PSS.TRAN X ON A.TRAN_ID = X.TRAN_ID
                         WHERE TB.TERMINAL_BATCH_ID = ln_terminal_batch_id 
                         GROUP BY DECODE(TB.TERMINAL_BATCH_CLOSE_TS, NULL, 'N', 'Y');
                        IF lc_terminal_batch_closed != 'N' THEN
                            MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Terminal Batch '|| ln_terminal_batch_id ||' is already closed');
                        ELSIF ln_okay_tran != ln_count_tran THEN
                            NULL; -- Not ready for settlement yet
                        ELSIF ln_num_tran = 0 THEN
                            MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID); -- No non-error transaction, so mark as complete and continue
                        ELSE
                            SELECT ln_terminal_batch_id
                              BULK COLLECT INTO pn_pending_terminal_batch_ids
                              FROM DUAL;
                            -- Mark command executed                          
                            MARK_ADMIN_CMD_EXECUTED(l_rec.ADMIN_CMD_ID);
                            RETURN; -- found something to process so exit
                        END IF;
                    END IF;
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        MARK_ADMIN_CMD_ERRORED(l_rec.ADMIN_CMD_ID, 'Terminal Batch ' ||ln_terminal_batch_id||' not found');
                END;
            ELSE
                -- Unknown cmd type
                NULL;
            END IF;
        END LOOP;
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE GET_PENDING_ACTIONS(
        pn_payment_subtype_key_id PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE, 
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_settlement_retry_interval NUMBER,
        pn_tran_retry_interval NUMBER,
        pn_max_settlements PLS_INTEGER, 
        pn_max_trans PLS_INTEGER,
        pn_terminal_state_id OUT PSS.TERMINAL.TERMINAL_STATE_ID%TYPE,
        pn_pending_terminal_batch_ids OUT NUMBER_TABLE,
        pn_pending_tran_ids OUT NUMBER_TABLE)
    IS
        ln_batch_max_trans PSS.TERMINAL.TERMINAL_BATCH_MAX_TRAN%TYPE;
        ln_allowed_trans PLS_INTEGER;
        ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
        lc_settle_processing_enabled CHAR(1);
        lc_tran_processing_enabled CHAR(1);
        lv_terminal_global_token_cd PSS.TERMINAL.GLOBAL_TOKEN_CD%TYPE;
    BEGIN
        SELECT NVL(MAX(POSM_SETTING_VALUE), 'N')
          INTO lc_tran_processing_enabled
          FROM PSS.POSM_SETTING
         WHERE POSM_SETTING_NAME = 'TRAN_PROCESSING_ENABLED';
        
        -- Get terminal state id
        IF pv_payment_subtype_class  LIKE 'Authority::ISO8583%' THEN -- this is same as in APP_LAYER.VW_PAYMENT_SUBTYPE_DETAIL
            SELECT NVL(MAX(POSM_SETTING_VALUE), 'N')
              INTO lc_settle_processing_enabled
              FROM PSS.POSM_SETTING
             WHERE POSM_SETTING_NAME = 'SETTLE_PROCESSING_ENABLED';
        
            SELECT T.TERMINAL_STATE_ID, COALESCE(T.TERMINAL_BATCH_MAX_TRAN, AU.AUTHORITY_BATCH_MAX_TRAN, 999)
              INTO pn_terminal_state_id, ln_batch_max_trans
              FROM PSS.TERMINAL T
              JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
              JOIN AUTHORITY.AUTHORITY AU ON M.AUTHORITY_ID = AU.AUTHORITY_ID                       
             WHERE T.TERMINAL_ID = pn_payment_subtype_key_id;
            IF pn_terminal_state_id = 5 THEN
                -- see if there are retry admin commands
                GET_ACTIONS_FROM_ADMIN_CMDS(
                    pn_payment_subtype_key_id, 
                    pv_payment_subtype_class, 
                    pn_max_settlements, 
                    0,
                    'N', -- pc_allow_add_to_batch
                    'Y', -- pc_retry_only
                    lc_settle_processing_enabled,
                    lc_tran_processing_enabled,
                    pn_pending_terminal_batch_ids,
                    pn_pending_tran_ids);
                IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
                    RETURN; -- Early Exit if we found something to process
                END IF;
                IF lc_settle_processing_enabled = 'Y' THEN
                    -- Only settlement retry is available
                    -- Find retyable batches
                    GET_RETRY_SETTLEMENTS(
                        pn_payment_subtype_key_id, 
                        pv_payment_subtype_class,
                        pn_settlement_retry_interval,
                        pn_max_settlements, 
                        pn_pending_terminal_batch_ids);
                END IF;
                RETURN; -- if terminal is in state 5, we don't process anything else
            ELSIF pn_terminal_state_id != 3 THEN
                RETURN; -- Terminal not locked for processing
            END IF;
        ELSE
            lv_terminal_global_token_cd := GET_TERMINAL_GLOBAL_TOKEN(pv_payment_subtype_class, pn_payment_subtype_key_id);
            IF lv_terminal_global_token_cd IS NULL THEN
                pn_terminal_state_id := 1;
                RETURN;  -- Terminal not locked for processing
            ELSE
                pn_terminal_state_id := 3;
                lc_settle_processing_enabled := 'N';
            END IF;
        END IF;
        
        IF lc_tran_processing_enabled != 'Y' AND lc_settle_processing_enabled != 'Y' THEN
            RETURN; -- we are not doing any processing right now
        END IF;
        
        -- limit num of trans if need be
        IF ln_batch_max_trans IS NOT NULL THEN
            ln_terminal_batch_id := GET_AVAILABLE_TERMINAL_BATCH(pn_payment_subtype_key_id, ln_allowed_trans, FALSE);
        ELSE
            ln_allowed_trans := pn_max_trans;
        END IF;
        
        -- Check pending commands and process them first
        IF ln_allowed_trans <= 0 THEN            
            GET_ACTIONS_FROM_ADMIN_CMDS(
                pn_payment_subtype_key_id, 
                pv_payment_subtype_class, 
                pn_max_settlements, 
                pn_max_trans,
                'N', -- pc_allow_add_to_batch
                'N', -- pc_retry_only
                lc_settle_processing_enabled,
                lc_tran_processing_enabled,
                pn_pending_terminal_batch_ids,
                pn_pending_tran_ids);
        ELSE
            GET_ACTIONS_FROM_ADMIN_CMDS(
                pn_payment_subtype_key_id, 
                pv_payment_subtype_class, 
                pn_max_settlements, 
                ln_allowed_trans,
                'Y', -- pc_allow_add_to_batch
                'N', -- pc_retry_only
                lc_settle_processing_enabled,
                lc_tran_processing_enabled,
                pn_pending_terminal_batch_ids,
                pn_pending_tran_ids);
        END IF;
        IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
            RETURN; -- Early Exit if we found something to process
        ELSIF pn_pending_tran_ids IS NOT NULL AND pn_pending_tran_ids.COUNT > 0 THEN
            RETURN; -- Early Exit if we found something to process
        END IF;
        
        -- Check for Settlements
        IF lc_settle_processing_enabled = 'Y' THEN
            GET_PENDING_SETTLEMENTS(
                pn_payment_subtype_key_id, 
                pv_payment_subtype_class, 
                'N', 
                pn_max_settlements,
                pn_pending_terminal_batch_ids);
            IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
                RETURN; -- Early Exit if we found something to process
            END IF;
            GET_RETRY_SETTLEMENTS(
                pn_payment_subtype_key_id, 
                pv_payment_subtype_class,
                pn_settlement_retry_interval,
                pn_max_settlements, 
                pn_pending_terminal_batch_ids);
            IF pn_pending_terminal_batch_ids IS NOT NULL AND pn_pending_terminal_batch_ids.COUNT > 0 THEN 
                RETURN; -- Early Exit if we found something to process
            END IF;
        END IF;
        
        IF lc_tran_processing_enabled != 'Y' THEN
            RETURN; -- we are not doing any tran processing right now
        END IF;
        
        -- Find retry trans
        IF ln_allowed_trans <= 0 THEN
             -- We can only process trans already in a batch
             SELECT DISTINCT TRAN_ID
              BULK COLLECT INTO pn_pending_tran_ids
              FROM (SELECT /*+ USE_NL(T,A,R) index(T IDX_TRAN_STATE_CD) index(A IDX_AUTH_TRAN_ID) */ T.TRAN_ID, T.TRAN_UPLOAD_TS
                      FROM PSS.TRAN T
                      LEFT OUTER JOIN PSS.AUTH A   ON T.TRAN_ID = A.TRAN_ID
                      LEFT OUTER JOIN PSS.REFUND R ON T.TRAN_ID = R.TRAN_ID
                     WHERE T.TRAN_STATE_CD = 'I' /* transaction_incomplete */
                       AND T.PAYMENT_SUBTYPE_KEY_ID = pn_payment_subtype_key_id 
                       AND T.PAYMENT_SUBTYPE_CLASS  = pv_payment_subtype_class
                       AND NVL(A.TERMINAL_BATCH_ID, R.TERMINAL_BATCH_ID) IS NOT NULL
                     GROUP BY T.TRAN_ID, T.TRAN_UPLOAD_TS
                    HAVING SYSDATE >= pn_tran_retry_interval + GREATEST(NVL(MAX(A.AUTH_TS), MIN_DATE), NVL(MAX(R.LAST_UPDATED_TS), MIN_DATE))
                     ORDER BY T.TRAN_UPLOAD_TS ASC, T.TRAN_ID ASC)
             WHERE ROWNUM <= pn_max_trans;
            RETURN; -- we can't process trans right now
        END IF;
        
        SELECT DISTINCT TRAN_ID
          BULK COLLECT INTO pn_pending_tran_ids
          FROM (SELECT /*+ USE_NL(T,A,R) index(T IDX_TRAN_STATE_CD) index(A IDX_AUTH_TRAN_ID) */ T.TRAN_ID, T.TRAN_UPLOAD_TS
                  FROM PSS.TRAN T
                  LEFT OUTER JOIN PSS.AUTH A   ON T.TRAN_ID = A.TRAN_ID
                  LEFT OUTER JOIN PSS.REFUND R ON T.TRAN_ID = R.TRAN_ID
                 WHERE T.TRAN_STATE_CD = 'I' /* transaction_incomplete */
                   AND T.PAYMENT_SUBTYPE_KEY_ID = pn_payment_subtype_key_id 
                   AND T.PAYMENT_SUBTYPE_CLASS  = pv_payment_subtype_class
                 GROUP BY T.TRAN_ID, T.TRAN_UPLOAD_TS
                HAVING SYSDATE >= pn_tran_retry_interval + GREATEST(NVL(MAX(A.AUTH_TS), MIN_DATE), NVL(MAX(R.LAST_UPDATED_TS), MIN_DATE))
                 ORDER BY T.TRAN_UPLOAD_TS ASC, T.TRAN_ID ASC)
         WHERE ROWNUM <= ln_allowed_trans;
        IF pn_pending_tran_ids.COUNT > 0 THEN
            RETURN; -- Early Exit if we found something to process
        END IF;
          
        -- get new pending trans
        SELECT TRAN_ID
          BULK COLLECT INTO pn_pending_tran_ids
          FROM (SELECT /*+ index(T IDX_TRAN_STATE_CD) */ T.TRAN_ID, T.TRAN_UPLOAD_TS
                  FROM PSS.TRAN T
                 WHERE T.TRAN_STATE_CD IN ('8', '9', 'A', 'W') /* processed_server_batch, processed_server_batch_intended, processing_server_tran, processed_server_auth_pending_reversal */
                   AND (T.TRAN_STATE_CD != '9' OR T.AUTH_HOLD_USED != 'Y') /* Filters out the Intended Transactions which have an Auth Hold */
                   AND T.PAYMENT_SUBTYPE_KEY_ID = pn_payment_subtype_key_id
                   AND T.PAYMENT_SUBTYPE_CLASS  = pv_payment_subtype_class
                 ORDER BY T.TRAN_UPLOAD_TS ASC, T.TRAN_ID ASC)
         WHERE ROWNUM <= ln_allowed_trans;        
    END;
    
    PROCEDURE SET_UNIQUE_SECONDS(
        pn_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE,
        pn_authority_id PSS.TERMINAL_BATCH.AUTHORITY_ID%TYPE,
        pn_unique_seconds IN OUT PSS.TERMINAL_BATCH.UNIQUE_SECONDS%TYPE)
    IS
    BEGIN
        UPDATE PSS.TERMINAL_BATCH
           SET UNIQUE_SECONDS = pn_unique_seconds,
               AUTHORITY_ID = pn_authority_id
         WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id;
    EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
            pn_unique_seconds := pn_unique_seconds + 1;
            SET_UNIQUE_SECONDS(pn_terminal_batch_id, pn_authority_id, pn_unique_seconds);
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE GET_OR_CREATE_SETTLEMENT_BATCH(
        pn_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE,
        pn_settlement_batch_id OUT PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_prior_attempts OUT PLS_INTEGER,
        pc_upload_needed_flag OUT VARCHAR2)
    IS
        lc_terminal_capture_flag PSS.TERMINAL_BATCH.TERMINAL_CAPTURE_FLAG%TYPE;
        ln_terminal_id PSS.TERMINAL_BATCH.TERMINAL_ID%TYPE;
        ln_unique_seconds PSS.TERMINAL_BATCH.UNIQUE_SECONDS%TYPE;
        ln_authority_id PSS.TERMINAL_BATCH.AUTHORITY_ID%TYPE;
    BEGIN
        -- check for an incomplete settlement_batch
        SELECT MAX(SB.SETTLEMENT_BATCH_ID), TB.TERMINAL_ID, TB.TERMINAL_CAPTURE_FLAG, TB.UNIQUE_SECONDS
          INTO pn_settlement_batch_id, ln_terminal_id, lc_terminal_capture_flag, ln_unique_seconds
          FROM PSS.TERMINAL_BATCH TB
          LEFT OUTER JOIN PSS.SETTLEMENT_BATCH SB ON TB.TERMINAL_BATCH_ID = SB.TERMINAL_BATCH_ID AND SB.SETTLEMENT_BATCH_STATE_ID = 4
         WHERE TB.TERMINAL_BATCH_ID = pn_terminal_batch_id
         GROUP BY TB.TERMINAL_ID, TB.TERMINAL_CAPTURE_FLAG, TB.UNIQUE_SECONDS;
        IF pn_settlement_batch_id IS NULL THEN
            SELECT PSS.SEQ_SETTLEMENT_BATCH_ID.NEXTVAL
              INTO pn_settlement_batch_id
              FROM DUAL;            
            
            INSERT INTO PSS.SETTLEMENT_BATCH(
                SETTLEMENT_BATCH_ID,
                SETTLEMENT_BATCH_STATE_ID,
                SETTLEMENT_BATCH_START_TS,
                TERMINAL_BATCH_ID
            ) VALUES (
                pn_settlement_batch_id,
                4,
                SYSDATE,
                pn_terminal_batch_id);
            /* Do this after transaction is uploaded (SALEd) with the Authority)    
            -- add all trans and update their tran state cd
            INSERT INTO PSS.TRAN_SETTLEMENT_BATCH(
                SETTLEMENT_BATCH_ID,
                AUTH_ID,
                TRAN_ID,
                TRAN_SETTLEMENT_B_AMT)
              SELECT DISTINCT 
                     pn_settlement_batch_id,
                     FIRST_VALUE(A.AUTH_ID) OVER (PARTITION BY T.TRAN_ID ORDER BY A.CREATED_TS DESC, A.AUTH_ID DESC) AUTH_ID, 
                     T.TRAN_ID, 
                     FIRST_VALUE(A.AUTH_AMT) OVER (PARTITION BY T.TRAN_ID ORDER BY A.CREATED_TS DESC, A.AUTH_ID DESC) AUTH_AMT
                FROM PSS.TRAN T
                JOIN PSS.AUTH A ON T.TRAN_ID = A.TRAN_ID
               WHERE A.TERMINAL_BATCH_ID = pn_terminal_batch_id
                 AND (A.AUTH_STATE_ID = 2
                      OR (A.AUTH_STATE_ID = 4 AND A.AUTH_TYPE_CD = 'N' AND A.AUTH_HOLD_USED = 'Y'));
            INSERT INTO PSS.REFUND_SETTLEMENT_BATCH(
                SETTLEMENT_BATCH_ID,
                REFUND_ID,
                TRAN_ID,
                REFUND_SETTLEMENT_B_AMT)
              SELECT DISTINCT 
                     pn_settlement_batch_id,
                     FIRST_VALUE(R.REFUND_ID) OVER (PARTITION BY T.TRAN_ID ORDER BY R.CREATED_TS DESC, R.REFUND_ID DESC) AUTH_ID, 
                     T.TRAN_ID, 
                     FIRST_VALUE(R.REFUND_AMT) OVER (PARTITION BY T.TRAN_ID ORDER BY R.CREATED_TS DESC, R.REFUND_ID DESC) REFUND_AMT
                FROM PSS.TRAN T
                JOIN PSS.REFUND R ON T.TRAN_ID = R.TRAN_ID
               WHERE R.TERMINAL_BATCH_ID = pn_terminal_batch_id
                 AND R.REFUND_STATE_ID IN(1);
            */
        END IF;
        UPDATE PSS.TRAN 
           SET TRAN_STATE_CD = 'S'
         WHERE TRAN_STATE_CD IN('R', 'N', 'T')
           AND TRAN_ID IN(
            SELECT A.TRAN_ID
              FROM PSS.AUTH A 
             WHERE A.TERMINAL_BATCH_ID = pn_terminal_batch_id
               AND (A.AUTH_STATE_ID IN(2, 5, 6)
                  OR (A.AUTH_STATE_ID = 4 AND A.AUTH_TYPE_CD = 'N' AND A.AUTH_HOLD_USED = 'Y'))
            UNION ALL
            SELECT R.TRAN_ID
              FROM PSS.REFUND R
             WHERE R.TERMINAL_BATCH_ID = pn_terminal_batch_id
               AND R.REFUND_STATE_ID IN(1, 6));
        UPDATE PSS.TERMINAL
           SET TERMINAL_STATE_ID = 5
         WHERE TERMINAL_ID = ln_terminal_id;
        SELECT COUNT(*)
          INTO pn_prior_attempts
    	  FROM PSS.SETTLEMENT_BATCH  
    	 WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id 
    	   AND SETTLEMENT_BATCH_STATE_ID IN(2, 3);
        IF pn_prior_attempts > 0 THEN
            SELECT DECODE(SETTLEMENT_BATCH_STATE_ID, 2, 'N', 3, 'Y')
              INTO pc_upload_needed_flag
              FROM (
                SELECT SETTLEMENT_BATCH_END_TS, SETTLEMENT_BATCH_ID, SETTLEMENT_BATCH_STATE_ID
                  FROM PSS.SETTLEMENT_BATCH  
                 WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id 
                   AND SETTLEMENT_BATCH_STATE_ID IN(2, 3)           
                 ORDER BY SETTLEMENT_BATCH_END_TS DESC, SETTLEMENT_BATCH_ID DESC)
             WHERE ROWNUM = 1;
        ELSE
            pc_upload_needed_flag := 'N';
        END IF;
        IF lc_terminal_capture_flag = 'Y' AND ln_unique_seconds IS NULL THEN
            SELECT ROUND(DATE_TO_MILLIS(SYSDATE) / 1000), M.AUTHORITY_ID
              INTO ln_unique_seconds, ln_authority_id
              FROM PSS.TERMINAL T
              JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
             WHERE T.TERMINAL_ID = ln_terminal_id;
            SET_UNIQUE_SECONDS(pn_terminal_batch_id, ln_authority_id, ln_unique_seconds);
        END IF;
    END;
    /*
    FUNCTION IS_LAST_TERMINAL_BATCH_OPEN(
        pn_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE)
    RETURN PLS_INTEGER
    IS
        ln_is_open PLS_INTEGER;
    BEGIN
        SELECT DECODE(TERMINAL_BATCH_CLOSE_TS, NULL, 1, 0)
          INTO ln_is_open
          FROM (SELECT TERMINAL_BATCH_NUM, 
                       TERMINAL_BATCH_CYCLE_NUM, 
                       TERMINAL_BATCH_CLOSE_TS
                  FROM PSS.TERMINAL_BATCH
                 WHERE TERMINAL_ID = pn_terminal_id
                 ORDER BY TERMINAL_BATCH_CYCLE_NUM DESC,
                          TERMINAL_BATCH_NUM DESC)
         WHERE ROWNUM = 1;
        RETURN ln_is_open;
    END;
    */
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE UPDATE_PROCESSED_BATCH(
        pn_settlement_batch_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pv_authority_response_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
        pv_authority_response_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
        pd_batch_closed_ts PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_END_TS%TYPE)
    IS
        CURSOR lc_tran IS
            SELECT SA.TRAN_ID, SA.AUTH_ID, NULL REFUND_ID, SA.AUTH_AMT_APPROVED AMOUNT,
                   AFTER_SETTLE_TRAN_STATE_CD(sa.AUTH_TYPE_CD, NULL, pc_auth_result_cd, X.TRAN_DEVICE_RESULT_TYPE_CD) NEW_TRAN_STATE_CD
              FROM PSS.AUTH SA 
              JOIN PSS.TERMINAL_BATCH TB ON TB.TERMINAL_BATCH_ID = SA.TERMINAL_BATCH_ID
              JOIN PSS.TRAN X ON SA.TRAN_ID = X.TRAN_ID
             WHERE TB.TERMINAL_BATCH_ID = pn_terminal_batch_id
               AND SA.AUTH_TYPE_CD IN('U','S','O','A','D','C','E','V','I', 'N')
               AND ((SA.AUTH_STATE_ID IN(2, 5) AND SA.AUTH_TYPE_CD != 'N')
                    OR (SA.AUTH_STATE_ID IN(6) AND TB.TERMINAL_CAPTURE_FLAG = 'Y' AND SA.AUTH_TYPE_CD != 'N')
                    OR (SA.AUTH_STATE_ID = 4 AND SA.AUTH_TYPE_CD = 'N' AND SA.AUTH_HOLD_USED = 'Y'))
               AND X.TRAN_STATE_CD IN('S')
            UNION ALL
            SELECT R.TRAN_ID, NULL, R.REFUND_ID, R.REFUND_AMT,
                   AFTER_SETTLE_TRAN_STATE_CD(NULL, r.REFUND_TYPE_CD, pc_auth_result_cd, X.TRAN_DEVICE_RESULT_TYPE_CD)
              FROM PSS.REFUND R
              JOIN PSS.TERMINAL_BATCH TB ON TB.TERMINAL_BATCH_ID = R.TERMINAL_BATCH_ID
              JOIN PSS.TRAN X ON R.TRAN_ID = X.TRAN_ID
             WHERE TB.TERMINAL_BATCH_ID = pn_terminal_batch_id
               AND (R.REFUND_STATE_ID IN(1) OR (TB.TERMINAL_CAPTURE_FLAG = 'Y' AND R.REFUND_STATE_ID IN(6)))
               AND X.TRAN_STATE_CD IN('S');
        ln_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE;
        lc_retain_acct_data CHAR(1);
        ln_settlement_batch_state_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_STATE_ID%TYPE;
    BEGIN
        SELECT SETTLEMENT_BATCH_STATE_ID
          INTO ln_settlement_batch_state_id
          FROM PSS.SETTLEMENT_BATCH
         WHERE SETTLEMENT_BATCH_ID = pn_settlement_batch_id;
        IF ln_settlement_batch_state_id = 1 OR (ln_settlement_batch_state_id IN(1,2,3,7) AND pc_auth_result_cd IN('P')) THEN
            RAISE_APPLICATION_ERROR(-20100, 'Settlement batch ' || pn_settlement_batch_id || ' was already processed');
        END IF;
        -- update the settlement batch
        UPDATE PSS.SETTLEMENT_BATCH
           SET SETTLEMENT_BATCH_STATE_ID = DECODE(pc_auth_result_cd, 'Y', 1, 'P', 7, 'N', 2, 'F', 3, 'O', 2),
               SETTLEMENT_BATCH_RESP_CD = pv_authority_response_cd,
               SETTLEMENT_BATCH_RESP_DESC = pv_authority_response_desc,
               SETTLEMENT_BATCH_REF_CD = pv_authority_ref_cd,
               SETTLEMENT_BATCH_END_TS = pd_batch_closed_ts
         WHERE SETTLEMENT_BATCH_ID = pn_settlement_batch_id;
            
        -- update the tran_state_cd of all transactions in the batch
        FOR lr_tran IN lc_tran LOOP
            IF lr_tran.NEW_TRAN_STATE_CD IN('V') THEN
                lc_retain_acct_data := 'N';
            ELSE
                lc_retain_acct_data := 'Y';
            END IF;
            UPDATE PSS.TRAN
               SET TRAN_STATE_CD = lr_tran.NEW_TRAN_STATE_CD,
                   TRAN_PARSED_ACCT_EXP_DATE = DECODE(lc_retain_acct_data, 'Y', TRAN_PARSED_ACCT_EXP_DATE),
                   TRAN_PARSED_ACCT_NUM = DECODE(lc_retain_acct_data, 'Y', TRAN_PARSED_ACCT_NUM),
                   TRAN_PARSED_ACCT_NAME = DECODE(lc_retain_acct_data, 'Y', TRAN_PARSED_ACCT_NAME),
                   TRAN_ACCOUNT_PIN = DECODE(lc_retain_acct_data, 'Y', TRAN_ACCOUNT_PIN)
             WHERE TRAN_ID = lr_tran.TRAN_ID;
            IF lr_tran.AUTH_ID IS NOT NULL THEN
                INSERT INTO PSS.TRAN_SETTLEMENT_BATCH(
                    SETTLEMENT_BATCH_ID,
                    AUTH_ID,
                    TRAN_ID,
                    TRAN_SETTLEMENT_B_AMT,
                    TRAN_SETTLEMENT_B_RESP_CD,
                    TRAN_SETTLEMENT_B_RESP_DESC
                ) VALUES (
                    pn_settlement_batch_id,
                    lr_tran.AUTH_ID,
                    lr_tran.TRAN_ID,
                    DECODE(pc_auth_result_cd, 'Y', lr_tran.AMOUNT),
                    pv_authority_response_cd,
                    pv_authority_response_desc
                );   
            ELSIF lr_tran.REFUND_ID IS NOT NULL THEN
                INSERT INTO PSS.REFUND_SETTLEMENT_BATCH(
                    SETTLEMENT_BATCH_ID,
                    REFUND_ID,
                    TRAN_ID,
                    REFUND_SETTLEMENT_B_AMT,
                    REFUND_SETTLEMENT_B_RESP_CD,
                    REFUND_SETTLEMENT_B_RESP_DESC
                ) VALUES (
                    pn_settlement_batch_id,
                    lr_tran.REFUND_ID,
                    lr_tran.TRAN_ID,
                    DECODE(pc_auth_result_cd, 'Y', lr_tran.AMOUNT),
                    pv_authority_response_cd,
                    pv_authority_response_desc
                );   
            END IF;
        END LOOP;
          
        -- if settlement successful or permanently declined, close old terminal batch and create new terminal batch
        IF pn_terminal_batch_id IS NOT NULL AND pc_auth_result_cd IN('Y', 'P', 'O') THEN
            UPDATE PSS.TERMINAL_BATCH
               SET TERMINAL_BATCH_CLOSE_TS = pd_batch_closed_ts
             WHERE TERMINAL_BATCH_ID = pn_terminal_batch_id
             RETURNING TERMINAL_ID INTO ln_terminal_id;
            UPDATE PSS.TERMINAL
               SET TERMINAL_STATE_ID = 3
             WHERE TERMINAL_ID = ln_terminal_id;
        END IF;
    END;
    
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE PREPARE_PENDING_TRAN(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pn_sale_auth_id OUT PSS.AUTH.AUTH_ID%TYPE,
        pn_refund_id OUT PSS.REFUND.REFUND_ID%TYPE,
        pc_sale_phase_cd OUT VARCHAR2,
        pn_prior_attempts OUT PLS_INTEGER)
    IS
        lv_tran_lock_cd VARCHAR2(128);
        lv_lock_handle VARCHAR2(128);
        ln_actual_amt PSS.AUTH.AUTH_AMT%TYPE;
        ln_intended_amt PSS.AUTH.AUTH_AMT%TYPE;
        lc_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
        lc_auth_type_cd PSS.AUTH.AUTH_TYPE_CD%TYPE;
        ln_previous_saled_amt PSS.AUTH.AUTH_AMT%TYPE;
        lc_auth_hold_used PSS.AUTH.AUTH_HOLD_USED%TYPE;
        ln_terminal_id PSS.TERMINAL.TERMINAL_ID%TYPE;
        lc_terminal_capture_flag PSS.TERMINAL_BATCH.TERMINAL_CAPTURE_FLAG%TYPE;
        ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
        ln_is_open PLS_INTEGER;   
        ln_auth_auth_id PSS.AUTH.AUTH_ID%TYPE;
        ln_allowed_trans PLS_INTEGER;
        lc_new_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
        ln_device_result_type_cd PSS.TRAN.TRAN_DEVICE_RESULT_TYPE_CD%TYPE;
    BEGIN
        SELECT DEVICE_NAME || ':' || TRAN_DEVICE_TRAN_CD, 
               TRAN_STATE_CD,
               NVL(AUTH_HOLD_USED, 'N'),
               TERMINAL_ID,
               NVL(TERMINAL_CAPTURE_FLAG, 'N'),
               AUTH_AUTH_ID,
               SALE_AUTH_ID,
               REFUND_ID,
               TRAN_DEVICE_RESULT_TYPE_CD
          INTO lv_tran_lock_cd,
               lc_tran_state_cd,
               lc_auth_hold_used,
               ln_terminal_id,
               lc_terminal_capture_flag,
               ln_auth_auth_id,
               pn_sale_auth_id,
               pn_refund_id,
               ln_device_result_type_cd
          FROM (SELECT T.DEVICE_NAME, 
                       T.TRAN_DEVICE_TRAN_CD, 
                       A.AUTH_ID AUTH_AUTH_ID,
                       SA.AUTH_ID SALE_AUTH_ID,
                       R.REFUND_ID, 
                       T.TRAN_STATE_CD,
                       T.AUTH_HOLD_USED,
                       TE.TERMINAL_ID,
                       AU.TERMINAL_CAPTURE_FLAG,
                       T.TRAN_DEVICE_RESULT_TYPE_CD
                  FROM PSS.TRAN T
				  LEFT OUTER JOIN PSS.AUTH RA  ON T.TRAN_ID = RA.TRAN_ID AND RA.AUTH_TYPE_CD IN ('C', 'E')
                  LEFT OUTER JOIN PSS.AUTH A   ON T.TRAN_ID = A.TRAN_ID AND A.AUTH_TYPE_CD IN('L', 'N') AND (A.AUTH_RESULT_CD IN('Y', 'P') OR T.TRAN_STATE_CD IN('W') OR RA.AUTH_ID IS NOT NULL)
                  LEFT OUTER JOIN PSS.REFUND R ON T.TRAN_ID = R.TRAN_ID
                  LEFT OUTER JOIN PSS.AUTH SA  ON T.TRAN_ID = SA.TRAN_ID AND SA.AUTH_TYPE_CD IN('U','S','O','A','D','C','E','V','I') AND SA.AUTH_STATE_ID = 6
                  LEFT OUTER JOIN (PSS.TERMINAL TE
                        JOIN PSS.MERCHANT M ON TE.MERCHANT_ID = M.MERCHANT_ID
                        JOIN AUTHORITY.AUTHORITY AU ON M.AUTHORITY_ID = AU.AUTHORITY_ID
                  ) ON (T.PAYMENT_SUBTYPE_CLASS LIKE 'Authority::ISO8583%' OR T.PAYMENT_SUBTYPE_CLASS LIKE 'POSGateway%') AND T.PAYMENT_SUBTYPE_KEY_ID = TE.TERMINAL_ID
                 WHERE T.TRAN_ID = pn_tran_id
                 ORDER BY A.AUTH_RESULT_CD DESC, A.AUTH_TS DESC, A.AUTH_ID DESC, SA.AUTH_RESULT_CD DESC, SA.AUTH_TS DESC, SA.AUTH_ID DESC, R.CREATED_TS DESC, R.REFUND_ID DESC, RA.AUTH_RESULT_CD DESC, RA.AUTH_TS DESC, RA.AUTH_ID DESC)
         WHERE ROWNUM = 1;
        IF NOT(lc_tran_state_cd IN('A', '8', 'I', 'W')
            OR (lc_tran_state_cd IN('9') AND lc_auth_hold_used != 'Y' AND lc_terminal_capture_flag != 'Y')) THEN
            pc_sale_phase_cd := '-'; -- Do nothing
            pn_prior_attempts := -1;
            RETURN;
        END IF;
        -- Thus, "lc_tran_state_cd IN('A', '8', 'I', 'W') OR (lc_tran_state_cd IN('9') AND lc_auth_hold_used != 'Y' AND lc_terminal_capture_flag != 'Y')"
		lc_new_tran_state_cd := lc_tran_state_cd;
        -- Lock transaction
        lv_lock_handle := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', lv_tran_lock_cd);
        IF ln_auth_auth_id IS NOT NULL THEN -- it's a sale or a reversal
            IF lc_terminal_capture_flag = 'Y' AND lc_tran_state_cd != '9' THEN
                -- get the open terminal batch
                ln_terminal_batch_id := GET_AVAILABLE_TERMINAL_BATCH(ln_terminal_id, ln_allowed_trans, TRUE);
                SELECT NVL(SUM(DECODE(TLI.TRAN_LINE_ITEM_BATCH_TYPE_CD, 'A', TLI.TRAN_LINE_ITEM_AMOUNT * TLI.TRAN_LINE_ITEM_QUANTITY)), 0) ACTUAL_AMT
                  INTO ln_actual_amt
                  FROM PSS.TRAN_LINE_ITEM TLI
                 WHERE TLI.TRAN_ID = pn_tran_id;
                pc_sale_phase_cd := '-'; -- No further processing
                IF ln_actual_amt != 0 THEN
                    lc_auth_type_cd := 'U'; -- pre-auth sale
                    lc_new_tran_state_cd := 'T'; -- Processed Server Batch
                ELSIF lc_auth_hold_used = 'Y' THEN
                     lc_auth_type_cd := 'C'; -- Auth Reversal
                     lc_new_tran_state_cd := 'T'; -- Processed Server Batch
                ELSE
                    -- do not create sale auth record
                    lc_new_tran_state_cd := 'C'; -- client cancelled
                END IF;
            ELSIF lc_tran_state_cd = 'W' THEN
                pc_sale_phase_cd := 'R'; -- Reversal
                ln_actual_amt := 0;
                lc_auth_type_cd := 'C'; -- Auth reversal
                lc_new_tran_state_cd := 'A'; -- processing tran
            ELSE
                SELECT NVL(SUM(DECODE(TLI.TRAN_LINE_ITEM_BATCH_TYPE_CD, 'A', TLI.TRAN_LINE_ITEM_AMOUNT * TLI.TRAN_LINE_ITEM_QUANTITY)), 0) ACTUAL_AMT,
                       NVL(SUM(DECODE(TLI.TRAN_LINE_ITEM_BATCH_TYPE_CD, 'I', TLI.TRAN_LINE_ITEM_AMOUNT * TLI.TRAN_LINE_ITEM_QUANTITY)), 0) INTENDED_AMT,
                       T.TRAN_STATE_CD -- get this again now that transaction is locked
                  INTO ln_actual_amt, ln_intended_amt, lc_tran_state_cd
                  FROM PSS.TRAN_LINE_ITEM TLI
                 RIGHT OUTER JOIN PSS.TRAN T ON T.TRAN_ID = TLI.TRAN_ID
                 WHERE T.TRAN_ID = pn_tran_id 
                 GROUP BY T.TRAN_STATE_CD;
                IF lc_tran_state_cd = '9' THEN
                    IF ln_actual_amt > 0 THEN -- sanity check
                        RAISE_APPLICATION_ERROR(-20555, 'Invalid tran_state_cd for tran ' || pn_tran_id || ': Intended batch with Actual Amount');
                    ELSIF ln_intended_amt > 0 THEN
                        pc_sale_phase_cd := 'I'; -- needs processing of intended sale
                        ln_actual_amt := NULL; -- Use Intended Amt
                        lc_auth_type_cd := 'S'; -- network sale
                        lc_new_tran_state_cd := 'A'; -- processing tran
                    ELSE -- we can assume lc_auth_hold_used != 'Y' or it would not have gotten here
                        pc_sale_phase_cd := '-'; -- no processing needed
                        -- do not create sale auth record
                        lc_new_tran_state_cd := 'U'; -- processed_server_tran_intended
                    END IF;             
                ELSE
                    SELECT NVL(MAX(AUTH_AMT), 0)
                      INTO ln_previous_saled_amt
                      FROM (SELECT /*+ index(AT IDX_AUTH_TRAN_ID) */ AT.AUTH_AMT
                              FROM PSS.AUTH AT
                             WHERE AT.TRAN_ID = pn_tran_id
                               AND AT.AUTH_TYPE_CD IN('U', 'S')
                               AND AT.AUTH_STATE_ID IN(2)
                             ORDER BY AT.AUTH_TS DESC, AT.AUTH_ID DESC)
                     WHERE ROWNUM = 1;
                    IF ln_previous_saled_amt != ln_actual_amt THEN
                        pc_sale_phase_cd := 'A'; -- needs processing of actual sale
                        IF ln_actual_amt = 0 THEN -- had previous amount, but now cancelled
                            lc_auth_type_cd := 'E'; -- Sale Reversal
                        ELSIF ln_previous_saled_amt > 0 THEN
                            lc_auth_type_cd := 'D'; -- Sale Adjustment
                        ELSIF lc_auth_hold_used = 'Y' THEN                    
                            lc_auth_type_cd := 'U'; -- Pre-Authed Sale
                        ELSE
                            lc_auth_type_cd := 'S'; -- Sale
                        END IF;
                        lc_new_tran_state_cd := 'A'; -- processing tran
                    ELSIF ln_actual_amt = 0 THEN -- previous amount = actual amount = 0
                        IF ln_intended_amt > 0 AND lc_tran_state_cd != '8' THEN
                            -- This may have been an intended sale that failed, in which case we need to retry the intended amt
                            SELECT SALE_TYPE_CD
                              INTO pc_sale_phase_cd
                              FROM PSS.SALE
                             WHERE TRAN_ID = pn_tran_id;
                            IF pc_sale_phase_cd = 'I' THEN -- needs re-processing of intended sale
                                ln_actual_amt := NULL; -- Use Intended Amt
                                lc_auth_type_cd := 'S'; -- network sale
                                lc_new_tran_state_cd := 'A'; -- processing tran
                            ELSIF lc_auth_hold_used = 'Y' THEN
                                pc_sale_phase_cd := 'A'; -- Actual
                                lc_auth_type_cd := 'C'; -- Auth reversal
                                lc_new_tran_state_cd := 'A'; -- processing tran
                            ELSE
                                pc_sale_phase_cd := '-'; -- no processing needed
                                -- do not create sale auth record
                                lc_new_tran_state_cd := 'C'; -- client cancelled
                            END IF;
                        ELSIF lc_auth_hold_used = 'Y' THEN
                            IF lc_tran_state_cd IN('I', 'A') AND ln_device_result_type_cd IS NULL THEN
                                pc_sale_phase_cd := 'R'; -- Reversal
                            ELSE
                                pc_sale_phase_cd := 'A'; -- Actual
                            END IF;
                            lc_auth_type_cd := 'C'; -- Auth reversal
                            lc_new_tran_state_cd := 'A'; -- processing tran
                        ELSE
                            pc_sale_phase_cd := '-'; -- no processing needed
                            -- do not create sale auth record
                            lc_new_tran_state_cd := 'C'; -- client cancelled
                        END IF;
                    ELSE
                        pc_sale_phase_cd := '-'; -- no processing needed
                        -- do not create sale auth record
                        lc_new_tran_state_cd := 'D'; -- tran complete
                    END IF;
                END IF;
                IF pc_sale_phase_cd != '-' THEN
                    SELECT COUNT(*)
                      INTO pn_prior_attempts
                      FROM PSS.AUTH A
                     WHERE A.TRAN_ID = pn_tran_id
                       AND A.AUTH_TYPE_CD IN ('S','I','U','V')
                       AND A.AUTH_STATE_ID IN(3, 4);  	
                END IF;
            END IF;
            IF pn_sale_auth_id IS NULL AND lc_auth_type_cd IS NOT NULL THEN
                SELECT PSS.SEQ_AUTH_ID.NEXTVAL
                  INTO pn_sale_auth_id
                  FROM DUAL;
                INSERT INTO PSS.AUTH (
                        AUTH_ID,
                        TRAN_ID,
                        AUTH_TYPE_CD,
                        AUTH_STATE_ID,
                        AUTH_PARSED_ACCT_DATA,
                        ACCT_ENTRY_METHOD_CD,
                        AUTH_AMT,
                        AUTH_TS,
                        TERMINAL_BATCH_ID,
                        TRACE_NUMBER)
                 SELECT pn_sale_auth_id,
                        pn_tran_id,
                        lc_auth_type_cd,
                        6,
                        a.AUTH_PARSED_ACCT_DATA,
                        a.ACCT_ENTRY_METHOD_CD,
                        NVL(ln_actual_amt, ln_intended_amt),	    
                        SYSDATE,
                        ln_terminal_batch_id,
                        a.TRACE_NUMBER
                   FROM PSS.AUTH a
                  WHERE a.AUTH_ID = ln_auth_auth_id;
            END IF;
        ELSIF pn_refund_id IS NOT NULL THEN -- it's a refund
            IF lc_terminal_capture_flag = 'Y' THEN
                ln_terminal_batch_id := GET_AVAILABLE_TERMINAL_BATCH(ln_terminal_id, ln_allowed_trans, TRUE);
                -- stick it in the open terminal batch
                UPDATE PSS.REFUND
                   SET TERMINAL_BATCH_ID = ln_terminal_batch_id,
                       REFUND_STATE_ID = 6
                 WHERE REFUND_ID = pn_refund_id;
                pc_sale_phase_cd := '-'; -- No further processing
                lc_new_tran_state_cd := 'T'; -- Processed Server Batch
            ELSE
                -- We could check the actual amount and the previous refunded amount for sanity, but that seems overkill. 
                -- Just assume actual amount > 0 and previous refunded amount = 0
                -- Also, assume REFUND_STATE_ID IN(6, 2, 3)
                pc_sale_phase_cd := 'A'; -- needs processing of actual sale
                lc_new_tran_state_cd := 'A'; -- processing tran
                SELECT COUNT(*)
                  INTO pn_prior_attempts
                  FROM PSS.REFUND R
                 WHERE R.TRAN_ID = pn_tran_id
                   AND R.REFUND_TYPE_CD IN ('G','C','V')
                   AND R.REFUND_STATE_ID IN(2, 3);
            END IF;
        ELSE -- neither authId nor refundId is found, set to error
            pc_sale_phase_cd := '-'; -- No further processing
            lc_new_tran_state_cd := 'E'; -- Complete Error
        END IF;
		IF lc_new_tran_state_cd != lc_tran_state_cd THEN
			UPDATE PSS.TRAN 
			   SET TRAN_STATE_CD = lc_new_tran_state_cd
			 WHERE TRAN_ID = pn_tran_id;
		END IF;
    END;
    
    -- R29+ signature
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE UPDATE_PROCESSED_TRAN(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pn_sale_auth_id PSS.AUTH.AUTH_ID%TYPE,
        pn_refund_id PSS.REFUND.REFUND_ID%TYPE,
        pc_settled_flag CHAR,
        pc_sale_phase_cd CHAR,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
        pn_auth_approved_amt PSS.AUTH.AUTH_AMT_APPROVED%TYPE,
        pv_authority_response_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
        pv_authority_response_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
        pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
        pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
        pd_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
        pc_ignore_reprocess_flag CHAR DEFAULT 'N',
        pv_tran_global_trans_cd OUT PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
        pv_auth_authority_tran_cd OUT PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE)
    IS
        lv_tran_lock_cd VARCHAR2(128);
        lv_lock_handle VARCHAR2(128);
        ln_terminal_batch_id PSS.TERMINAL_BATCH.TERMINAL_BATCH_ID%TYPE;
        ln_settlement_batch_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE;
        lc_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
        ln_payment_subtype_key_id PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE;
        lv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE;
        ln_allowed_trans PLS_INTEGER;
        lc_auth_type_cd PSS.AUTH.AUTH_TYPE_CD%TYPE;
    BEGIN
        SELECT T.DEVICE_NAME || ':' || T.TRAN_DEVICE_TRAN_CD, T.PAYMENT_SUBTYPE_CLASS, T.PAYMENT_SUBTYPE_KEY_ID, T.TRAN_STATE_CD, T.TRAN_GLOBAL_TRANS_CD, AA.AUTH_AUTHORITY_TRAN_CD
          INTO lv_tran_lock_cd, lv_payment_subtype_class, ln_payment_subtype_key_id, lc_tran_state_cd, pv_tran_global_trans_cd, pv_auth_authority_tran_cd
          FROM PSS.TRAN T
          LEFT OUTER JOIN PSS.AUTH AA ON T.TRAN_ID = AA.TRAN_ID AND AA.AUTH_TYPE_CD = 'N' AND AA.AUTH_STATE_ID IN(2,5)
         WHERE T.TRAN_ID = pn_tran_id;
        IF lc_tran_state_cd != 'A' THEN
            IF pc_ignore_reprocess_flag = 'Y' THEN
                RETURN;
            ELSE
                RAISE_APPLICATION_ERROR(-20554, 'Invalid tran_state_cd for tran ' || pn_tran_id || ' when updating processed tran');      
            END IF;
        END IF;
        IF pc_settled_flag = 'Y' THEN
            -- create settlement batch record
            SELECT PSS.SEQ_SETTLEMENT_BATCH_ID.NEXTVAL
              INTO ln_settlement_batch_id
              FROM DUAL;
            INSERT INTO PSS.SETTLEMENT_BATCH(
                SETTLEMENT_BATCH_ID,
                SETTLEMENT_BATCH_STATE_ID,
                SETTLEMENT_BATCH_START_TS,
                SETTLEMENT_BATCH_END_TS,
                SETTLEMENT_BATCH_RESP_CD,
                SETTLEMENT_BATCH_RESP_DESC,
                SETTLEMENT_BATCH_REF_CD
            ) VALUES (
                ln_settlement_batch_id,
                DECODE(pc_auth_result_cd, 'Y', 1, 'P', 1, 'N', 2, 'O', 2, 'F', 3),
                pd_authority_ts,
                pd_authority_ts,
                pv_authority_response_cd,
                pv_authority_response_desc,
                pv_authority_ref_cd);
        ELSIF pc_auth_result_cd IN('Y', 'P') AND (lv_payment_subtype_class  LIKE 'Authority::ISO8583%') THEN
            -- If this is a retry of a failure, it may already have a terminal_batch assigned
            IF pn_sale_auth_id IS NOT NULL THEN
                SELECT MAX(TERMINAL_BATCH_ID)
                  INTO ln_terminal_batch_id
                  FROM PSS.AUTH
                 WHERE TRAN_ID = pn_tran_id;
            ELSIF pn_refund_id IS NOT NULL THEN
                SELECT MAX(TERMINAL_BATCH_ID)
                  INTO ln_terminal_batch_id
                  FROM PSS.REFUND
                 WHERE TRAN_ID = pn_tran_id;
            END IF;
            IF ln_terminal_batch_id IS NULL THEN
                ln_terminal_batch_id := GET_AVAILABLE_TERMINAL_BATCH(ln_payment_subtype_key_id, ln_allowed_trans, TRUE);
                IF ln_allowed_trans <= 0 THEN
                    RAISE_APPLICATION_ERROR(-20556, 'Terminal Batch ' || ln_terminal_batch_id || ' is full for terminal ' || ln_payment_subtype_key_id || '; Can not add tran ' || pn_tran_id || ' to it');
                END IF;
            END IF;
        END IF;
        -- Lock transaction
        lv_lock_handle := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', lv_tran_lock_cd);
        IF pn_sale_auth_id IS NOT NULL THEN
            UPDATE PSS.AUTH 
               SET (
			    AUTH_STATE_ID,
                AUTH_RESULT_CD,
                AUTH_RESP_CD,
                AUTH_RESP_DESC,
                AUTH_AUTHORITY_TRAN_CD,
                AUTH_AUTHORITY_REF_CD,
                AUTH_AUTHORITY_TS,
                TERMINAL_BATCH_ID,
                AUTH_AMT_APPROVED,
                AUTH_AUTHORITY_MISC_DATA) =
            (SELECT 
                DECODE(pc_auth_result_cd, 'Y', 2, 'P', 5, 'N', 3, 'O', 7, 'F', 4),
                pc_auth_result_cd,
                pv_authority_response_cd,
                pv_authority_response_desc,
                pv_authority_tran_cd,
                pv_authority_ref_cd,
                pd_authority_ts,
                ln_terminal_batch_id,
                pn_auth_approved_amt / pn_minor_currency_factor,
                pv_authority_misc_data
             FROM DUAL)
             WHERE AUTH_ID = pn_sale_auth_id
             RETURNING AUTH_TYPE_CD INTO lc_auth_type_cd;
            IF ln_settlement_batch_id IS NOT NULL THEN
                INSERT INTO PSS.TRAN_SETTLEMENT_BATCH(
                    SETTLEMENT_BATCH_ID,
                    AUTH_ID,
                    TRAN_ID,
                    TRAN_SETTLEMENT_B_AMT,
                    TRAN_SETTLEMENT_B_RESP_CD,
                    TRAN_SETTLEMENT_B_RESP_DESC
                ) VALUES (
                    ln_settlement_batch_id,
                    pn_sale_auth_id,
                    pn_tran_id,
                    pn_auth_approved_amt / pn_minor_currency_factor,
                    pv_authority_response_cd,
                    pv_authority_response_desc
                );               
            END IF;
        ELSIF pn_refund_id IS NOT NULL THEN
           UPDATE PSS.REFUND
               SET REFUND_STATE_ID = DECODE(pc_auth_result_cd, 'Y', 1, 'P', 1, 'N', 2, 'O', 2, 'F', 3),
                REFUND_RESP_CD = NVL(pv_authority_response_cd, REFUND_RESP_CD),
                REFUND_RESP_DESC = NVL(pv_authority_response_desc, REFUND_RESP_DESC),
                REFUND_AUTHORITY_TRAN_CD = NVL(pv_authority_tran_cd, REFUND_AUTHORITY_TRAN_CD),
                REFUND_AUTHORITY_REF_CD = NVL(pv_authority_ref_cd, REFUND_AUTHORITY_REF_CD),
                REFUND_AUTHORITY_TS = NVL(pd_authority_ts, REFUND_AUTHORITY_TS),
                TERMINAL_BATCH_ID = NVL(ln_terminal_batch_id, TERMINAL_BATCH_ID),
                REFUND_AUTHORITY_MISC_DATA = NVL(pv_authority_misc_data, REFUND_AUTHORITY_MISC_DATA)
            WHERE REFUND_ID = pn_refund_id;
            IF ln_settlement_batch_id IS NOT NULL THEN
                INSERT INTO PSS.REFUND_SETTLEMENT_BATCH(
                    SETTLEMENT_BATCH_ID,
                    REFUND_ID,
                    TRAN_ID,
                    REFUND_SETTLEMENT_B_AMT
                ) VALUES (
                    ln_settlement_batch_id,
                    pn_refund_id,
                    pn_tran_id,
                    pn_auth_approved_amt / pn_minor_currency_factor
                );               
            END IF;
        END IF;
        
        -- update tran_state_cd
        IF pc_auth_result_cd IN('F') THEN
            lc_tran_state_cd := 'J'; -- Incomplete Error: Manual intervention needed
        ELSIF pc_auth_result_cd IN('O') THEN
            IF pc_sale_phase_cd = 'R' THEN -- it was an auth reversal
                lc_tran_state_cd := 'V';
            ELSIF lc_auth_type_cd IN('C', 'E') THEN -- sale reversal or auth reversal (indicates actual amount = 0)
                lc_tran_state_cd := 'C'; -- Client Cancelled
            ELSIF pc_sale_phase_cd = 'I' THEN
                -- check If actuals have since been uploaded
                SELECT DECODE(SALE_TYPE_CD, 'I', 'K', 'A', '8')
                  INTO lc_tran_state_cd
                  FROM PSS.SALE
                WHERE TRAN_ID = pn_tran_id;         
            ELSE
                lc_tran_state_cd := 'E'; -- Complete Error: don't retry
            END IF;
        ELSIF pc_auth_result_cd IN('N') THEN
            lc_tran_state_cd := 'I'; -- Incomplete: will retry
        ELSIF pc_auth_result_cd IN('Y', 'P') THEN
            IF pc_sale_phase_cd = 'R' THEN -- it was an auth reversal
                IF pc_settled_flag = 'Y' THEN
                    lc_tran_state_cd := 'V';
                ELSE
                     lc_tran_state_cd := 'T';
                END IF;
            ELSIF pc_sale_phase_cd = 'I' THEN
                -- check If actuals have since been uploaded
                SELECT DECODE(SALE_TYPE_CD, 'I', 'U', 'A', '8')
                  INTO lc_tran_state_cd
                  FROM PSS.SALE
                WHERE TRAN_ID = pn_tran_id;
            ELSIF pc_settled_flag = 'Y' THEN
                IF lc_auth_type_cd IN('C', 'E') THEN -- sale reversal or auth reversal (indicates actual amount = 0)
                    lc_tran_state_cd := 'C'; -- Client Cancelled
                ELSE
                    lc_tran_state_cd := 'D'; -- Complete
                END IF;
            ELSE
                lc_tran_state_cd := 'T'; -- Processed Server Batch
            END IF;
        ELSE
            RAISE_APPLICATION_ERROR(-20557, 'Invalid auth result cd "' ||  pc_auth_result_cd + '" for tran ' || pn_tran_id);
        END IF;
        UPDATE PSS.TRAN
           SET TRAN_STATE_CD = lc_tran_state_cd
         WHERE TRAN_ID = pn_tran_id;
        IF lc_tran_state_cd IN('V') THEN
            UPDATE PSS.TRAN
               SET TRAN_PARSED_ACCT_EXP_DATE = NULL,
                   TRAN_PARSED_ACCT_NUM = NULL,
                   TRAN_PARSED_ACCT_NAME = NULL,
                   TRAN_ACCOUNT_PIN = NULL
             WHERE TRAN_ID = pn_tran_id;
        END IF;
    END;      

    -- R28 signature
/* CAUTION: Only call this procedure if you've successfully obtained a lock on the terminal */
    PROCEDURE UPDATE_PROCESSED_TRAN(
        pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
        pn_sale_auth_id PSS.AUTH.AUTH_ID%TYPE,
        pn_refund_id PSS.REFUND.REFUND_ID%TYPE,
        pc_settled_flag CHAR,
        pc_sale_phase_cd CHAR,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
        pn_auth_approved_amt PSS.AUTH.AUTH_AMT_APPROVED%TYPE,
        pv_authority_response_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
        pv_authority_response_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
        pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
        pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
        pd_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
        pc_ignore_reprocess_flag CHAR DEFAULT 'N')
    IS
        lv_tran_global_trans_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE;
        lv_auth_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE;
    BEGIN
        UPDATE_PROCESSED_TRAN(
            pn_tran_id,
            pn_sale_auth_id,
            pn_refund_id,
            pc_settled_flag,
            pc_sale_phase_cd,
            pc_auth_result_cd,
            pn_minor_currency_factor,
            pn_auth_approved_amt,
            pv_authority_response_cd,
            pv_authority_response_desc,
            pv_authority_tran_cd,
            pv_authority_ref_cd,
            pv_authority_misc_data,
            pd_authority_ts,
            pc_ignore_reprocess_flag,
            lv_tran_global_trans_cd,
            lv_auth_authority_tran_cd);
    END;
    
    FUNCTION GET_TERMINAL_GLOBAL_TOKEN(
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_payment_subtype_key_id NUMBER)
    RETURN VARCHAR2
    IS
        lv_global_token PSS.TERMINAL.GLOBAL_TOKEN_CD%TYPE;
    BEGIN
        IF pv_payment_subtype_class IN('Authority::NOP', 'Cash') THEN
            RAISE_APPLICATION_ERROR(-20865, 'Payment subtype class ''' || pv_payment_subtype_class || ''' does not have terminals');
        ELSIF pv_payment_subtype_class LIKE 'Authority::ISO8583%' OR pv_payment_subtype_class LIKE 'POSGateway%' THEN
            SELECT GLOBAL_TOKEN_CD
              INTO lv_global_token
              FROM PSS.TERMINAL
             WHERE TERMINAL_ID = pn_payment_subtype_key_id;
        ELSIF pv_payment_subtype_class = 'Aramark' THEN
            SELECT GLOBAL_TOKEN_CD
              INTO lv_global_token
              FROM PSS.ARAMARK_PAYMENT_TYPE
             WHERE ARAMARK_PAYMENT_TYPE_ID = pn_payment_subtype_key_id;
        ELSIF pv_payment_subtype_class = 'BlackBoard' THEN
            SELECT GLOBAL_TOKEN_CD
              INTO lv_global_token
              FROM PSS.BLACKBRD_TENDER
             WHERE BLACKBRD_TENDER_ID = pn_payment_subtype_key_id;
        ELSIF pv_payment_subtype_class = 'Internal' THEN
            SELECT GLOBAL_TOKEN_CD
              INTO lv_global_token
              FROM PSS.INTERNAL_PAYMENT_TYPE
             WHERE INTERNAL_PAYMENT_TYPE_ID = pn_payment_subtype_key_id;
        ELSE
            RAISE_APPLICATION_ERROR(-20866, 'Payment subtype class ''' || pv_payment_subtype_class || ''' is not supported');
        END IF;
        RETURN lv_global_token;
    END;
    
    PROCEDURE UPDATE_TERMINAL_GLOBAL_TOKEN(
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_payment_subtype_key_id NUMBER,
        pv_old_global_token PSS.TERMINAL.GLOBAL_TOKEN_CD%TYPE,
        pv_new_global_token PSS.TERMINAL.GLOBAL_TOKEN_CD%TYPE,
        pn_update_count OUT PLS_INTEGER,
        pc_current_must_match CHAR DEFAULT 'N')
    IS
		ln_term_lock_max_duration_sec NUMBER := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('POSM_TERMINAL_LOCK_MAX_DURATION_SEC'));
		ld_sysdate DATE := SYSDATE;
		ln_forced_unlock_count NUMBER := 0;
    BEGIN
        IF pv_new_global_token IS NULL THEN
            RAISE_APPLICATION_ERROR(-20864, 'Global Token Code may not be null');
        ELSIF pv_payment_subtype_class IN('Authority::NOP', 'Cash') THEN
            RAISE_APPLICATION_ERROR(-20865, 'Payment subtype class ''' || pv_payment_subtype_class || ''' does not have terminals');
        ELSIF pv_payment_subtype_class LIKE 'Authority::ISO8583%' OR pv_payment_subtype_class LIKE 'POSGateway%' THEN
			UPDATE PSS.TERMINAL
               SET GLOBAL_TOKEN_CD = pv_new_global_token,
                   TERMINAL_STATE_ID =  DECODE(TERMINAL_STATE_ID, 5, 5, 3) -- terminal is busy or settlement retry now
             WHERE TERMINAL_ID = pn_payment_subtype_key_id
               AND ((pc_current_must_match = 'N' AND GLOBAL_TOKEN_CD IS NULL AND TERMINAL_STATE_ID IN(1, 5))
                OR (GLOBAL_TOKEN_CD = pv_old_global_token AND TERMINAL_STATE_ID IN(3, 5)));
            pn_update_count := SQL%ROWCOUNT;
			IF pn_update_count = 0 THEN
				UPDATE PSS.TERMINAL
				   SET GLOBAL_TOKEN_CD = pv_new_global_token,
					   TERMINAL_STATE_ID =  DECODE(TERMINAL_STATE_ID, 5, 5, 3) -- terminal is busy or settlement retry now
				 WHERE TERMINAL_ID = pn_payment_subtype_key_id
					AND GLOBAL_TOKEN_CD IS NOT NULL
					AND LAST_UPDATED_TS < ld_sysdate - ln_term_lock_max_duration_sec/86400;
				pn_update_count := SQL%ROWCOUNT;
				ln_forced_unlock_count := pn_update_count;
			END IF;
        ELSIF pv_payment_subtype_class = 'Aramark' THEN
            UPDATE PSS.ARAMARK_PAYMENT_TYPE
               SET GLOBAL_TOKEN_CD = pv_new_global_token
             WHERE ARAMARK_PAYMENT_TYPE_ID = pn_payment_subtype_key_id
               AND ((pc_current_must_match = 'N' AND GLOBAL_TOKEN_CD IS NULL) OR GLOBAL_TOKEN_CD = pv_old_global_token);
            pn_update_count := SQL%ROWCOUNT;
			IF pn_update_count = 0 THEN
				UPDATE PSS.ARAMARK_PAYMENT_TYPE
				   SET GLOBAL_TOKEN_CD = pv_new_global_token
				 WHERE ARAMARK_PAYMENT_TYPE_ID = pn_payment_subtype_key_id
					AND GLOBAL_TOKEN_CD IS NOT NULL
					AND LAST_UPDATED_TS < ld_sysdate - ln_term_lock_max_duration_sec/86400;
				pn_update_count := SQL%ROWCOUNT;
				ln_forced_unlock_count := pn_update_count;
			END IF;
        ELSIF pv_payment_subtype_class = 'BlackBoard' THEN
            UPDATE PSS.BLACKBRD_TENDER
               SET GLOBAL_TOKEN_CD = pv_new_global_token
             WHERE BLACKBRD_TENDER_ID = pn_payment_subtype_key_id
               AND ((pc_current_must_match = 'N' AND GLOBAL_TOKEN_CD IS NULL) OR GLOBAL_TOKEN_CD = pv_old_global_token);
            pn_update_count := SQL%ROWCOUNT;
			IF pn_update_count = 0 THEN
				UPDATE PSS.BLACKBRD_TENDER
				   SET GLOBAL_TOKEN_CD = pv_new_global_token
				 WHERE BLACKBRD_TENDER_ID = pn_payment_subtype_key_id
					AND GLOBAL_TOKEN_CD IS NOT NULL
					AND LAST_UPDATED_TS < ld_sysdate - ln_term_lock_max_duration_sec/86400;
				pn_update_count := SQL%ROWCOUNT;
				ln_forced_unlock_count := pn_update_count;
			END IF;
        ELSIF pv_payment_subtype_class = 'Internal' THEN
            UPDATE PSS.INTERNAL_PAYMENT_TYPE
               SET GLOBAL_TOKEN_CD = pv_new_global_token
             WHERE INTERNAL_PAYMENT_TYPE_ID = pn_payment_subtype_key_id
               AND ((pc_current_must_match = 'N' AND GLOBAL_TOKEN_CD IS NULL) OR GLOBAL_TOKEN_CD = pv_old_global_token);
            pn_update_count := SQL%ROWCOUNT;
			IF pn_update_count = 0 THEN
				UPDATE PSS.INTERNAL_PAYMENT_TYPE
				   SET GLOBAL_TOKEN_CD = pv_new_global_token
				 WHERE INTERNAL_PAYMENT_TYPE_ID = pn_payment_subtype_key_id
					AND GLOBAL_TOKEN_CD IS NOT NULL
					AND LAST_UPDATED_TS < ld_sysdate - ln_term_lock_max_duration_sec/86400;
				pn_update_count := SQL%ROWCOUNT;
				ln_forced_unlock_count := pn_update_count;
			END IF;
        ELSE
            RAISE_APPLICATION_ERROR(-20866, 'Payment subtype class ''' || pv_payment_subtype_class || ''' is not supported');
        END IF;
		
		IF ln_forced_unlock_count > 0 THEN
			pkg_exception_processor.sp_log_exception (
				pkg_app_exec_hist_globals.unknown_error_id,
				'POSM terminal ' || pn_payment_subtype_key_id || '(' || pv_payment_subtype_class || ') was locked for more than ' || ln_term_lock_max_duration_sec || ' seconds, forcefully unlocked',
				NULL,
				'PSS.PKG_SETTLEMENT.UPDATE_TERMINAL_GLOBAL_TOKEN'
			);
		END IF;
    END;
    
    PROCEDURE CLEAR_TERMINAL_GLOBAL_TOKEN(
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_payment_subtype_key_id NUMBER,
        pv_global_token PSS.TERMINAL.GLOBAL_TOKEN_CD%TYPE,
        pn_update_count OUT PLS_INTEGER)
    IS
    BEGIN
        IF pv_payment_subtype_class IN('Authority::NOP', 'Cash') THEN
            RAISE_APPLICATION_ERROR(-20865, 'Payment subtype class ''' || pv_payment_subtype_class || ''' does not have terminals');
        ELSIF pv_payment_subtype_class LIKE 'Authority::ISO8583%' OR pv_payment_subtype_class LIKE 'POSGateway%' THEN
            UPDATE PSS.TERMINAL
               SET GLOBAL_TOKEN_CD = NULL,
                   TERMINAL_STATE_ID = DECODE(TERMINAL_STATE_ID, 5, 5, 1), -- terminal is active or settlement retry now
                   LAST_RELEASED_UTC_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP)
             WHERE TERMINAL_ID = pn_payment_subtype_key_id
               AND GLOBAL_TOKEN_CD = pv_global_token;
            pn_update_count := SQL%ROWCOUNT;
        ELSIF pv_payment_subtype_class = 'Aramark' THEN
            UPDATE PSS.ARAMARK_PAYMENT_TYPE
               SET GLOBAL_TOKEN_CD = NULL,
                   LAST_RELEASED_UTC_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP)
             WHERE ARAMARK_PAYMENT_TYPE_ID = pn_payment_subtype_key_id
               AND GLOBAL_TOKEN_CD = pv_global_token;
            pn_update_count := SQL%ROWCOUNT;
        ELSIF pv_payment_subtype_class = 'BlackBoard' THEN
            UPDATE PSS.BLACKBRD_TENDER
               SET GLOBAL_TOKEN_CD = NULL,
                   LAST_RELEASED_UTC_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP)
             WHERE BLACKBRD_TENDER_ID = pn_payment_subtype_key_id
               AND GLOBAL_TOKEN_CD = pv_global_token;
            pn_update_count := SQL%ROWCOUNT;
        ELSIF pv_payment_subtype_class = 'Internal' THEN
            UPDATE PSS.INTERNAL_PAYMENT_TYPE
               SET GLOBAL_TOKEN_CD = NULL,
                   LAST_RELEASED_UTC_TS = SYS_EXTRACT_UTC(SYSTIMESTAMP)
             WHERE INTERNAL_PAYMENT_TYPE_ID = pn_payment_subtype_key_id
               AND GLOBAL_TOKEN_CD = pv_global_token;
            pn_update_count := SQL%ROWCOUNT;
        ELSE
            RAISE_APPLICATION_ERROR(-20866, 'Payment subtype class ''' || pv_payment_subtype_class || ''' is not supported');
        END IF;
    END;

    FUNCTION ADD_ADMIN_CMD(
        pv_payment_subtype_class PSS.ADMIN_CMD.PAYMENT_SUBTYPE_CLASS%TYPE,
        pn_payment_subtype_key_id PSS.ADMIN_CMD.PAYMENT_SUBTYPE_KEY_ID%TYPE,
        pn_admin_cmd_type_id PSS.ADMIN_CMD.ADMIN_CMD_TYPE_ID%TYPE,
        pv_requested_by PSS.ADMIN_CMD.REQUESTED_BY%TYPE,
        pn_priority PSS.ADMIN_CMD.PRIORITY%TYPE)
    RETURN PSS.ADMIN_CMD.ADMIN_CMD_ID%TYPE
    IS
        ln_admin_cmd_id PSS.ADMIN_CMD.ADMIN_CMD_ID%TYPE;
    BEGIN
        SELECT PSS.SEQ_ADMIN_CMD_ID.NEXTVAL
          INTO ln_admin_cmd_id
          FROM DUAL;
        INSERT INTO PSS.ADMIN_CMD (
                ADMIN_CMD_ID,
                ADMIN_CMD_TYPE_ID,
                PAYMENT_SUBTYPE_KEY_ID,
                PAYMENT_SUBTYPE_CLASS,
                REQUESTED_BY,
                PRIORITY) 
         VALUES(ln_admin_cmd_id, 
                pn_admin_cmd_type_id, 
                pn_payment_subtype_key_id,
                pv_payment_subtype_class,
                pv_requested_by,
                pn_priority);
         RETURN ln_admin_cmd_id;
    END;
    
    FUNCTION ADD_ADMIN_CMD_PARAM(
        pn_admin_cmd_id PSS.ADMIN_CMD_PARAM.ADMIN_CMD_ID%TYPE,
        pv_param_name PSS.ADMIN_CMD_PARAM.PARAM_NAME%TYPE,
        pv_param_value PSS.ADMIN_CMD_PARAM.PARAM_VALUE%TYPE)
    RETURN PSS.ADMIN_CMD_PARAM.ADMIN_CMD_PARAM_ID%TYPE
    IS
        ln_admin_cmd_param_id PSS.ADMIN_CMD_PARAM.ADMIN_CMD_PARAM_ID%TYPE;
    BEGIN
        SELECT PSS.SEQ_ADMIN_CMD_PARAM_ID.NEXTVAL
          INTO ln_admin_cmd_param_id
          FROM DUAL;
        INSERT INTO PSS.ADMIN_CMD_PARAM (
                ADMIN_CMD_PARAM_ID,
                ADMIN_CMD_ID,
                PARAM_NAME,
                PARAM_VALUE) 
         VALUES(ln_admin_cmd_param_id, 
                pn_admin_cmd_id,
                pv_param_name, 
                pv_param_value);
         RETURN ln_admin_cmd_param_id;
    END;   
    
    PROCEDURE LOCK_PARTIAL_SETTLE_POLLING(
        pv_process_id PSS.POSM_SETTING.POSM_SETTING_VALUE%TYPE,
        pv_success_flag OUT VARCHAR2,
        pv_locked_by_process_id OUT PSS.POSM_SETTING.POSM_SETTING_VALUE%TYPE)
    IS
		ln_polling_max_duration_sec NUMBER := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('POSM_FEEDBACK_POLLING_MAX_DURATION_SEC'));
		lt_last_updated_utc_ts PSS.POSM_SETTING.LAST_UPDATED_UTC_TS%TYPE;
    BEGIN
        SELECT POSM_SETTING_VALUE, 'N', LAST_UPDATED_UTC_TS
          INTO pv_locked_by_process_id, pv_success_flag, lt_last_updated_utc_ts
          FROM PSS.POSM_SETTING
         WHERE POSM_SETTING_NAME = 'PARTIAL_SETTLEMENT_POLLING_LOCK';
		 
		IF lt_last_updated_utc_ts < SYS_EXTRACT_UTC(SYSTIMESTAMP) - ln_polling_max_duration_sec/86400 THEN
			pkg_exception_processor.sp_log_exception (
				pkg_app_exec_hist_globals.unknown_error_id,
				'POSM feedback polling has been locked by ' || pv_locked_by_process_id || ' since ' || lt_last_updated_utc_ts || ' UTC, unlocking',
				NULL,
				'PSS.PKG_SETTLEMENT.LOCK_PARTIAL_SETTLE_POLLING'
			);
			UNLOCK_PARTIAL_SETTLE_POLLING(pv_locked_by_process_id);
		END IF;
		
        SELECT POSM_SETTING_VALUE, 'N'
        INTO pv_locked_by_process_id, pv_success_flag
        FROM PSS.POSM_SETTING
        WHERE POSM_SETTING_NAME = 'PARTIAL_SETTLEMENT_POLLING_LOCK';
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            BEGIN
                INSERT INTO PSS.POSM_SETTING(POSM_SETTING_NAME, POSM_SETTING_VALUE)
                  VALUES('PARTIAL_SETTLEMENT_POLLING_LOCK', pv_process_id);
                pv_success_flag := 'Y';
            EXCEPTION
                WHEN DUP_VAL_ON_INDEX THEN
                    LOCK_PARTIAL_SETTLE_POLLING(pv_process_id, pv_success_flag, pv_locked_by_process_id);
            END;
    END;
    
    PROCEDURE UNLOCK_PARTIAL_SETTLE_POLLING(
        pv_process_id PSS.POSM_SETTING.POSM_SETTING_VALUE%TYPE)
    IS
    BEGIN
        DELETE 
          FROM PSS.POSM_SETTING
         WHERE POSM_SETTING_NAME = 'PARTIAL_SETTLEMENT_POLLING_LOCK'
           AND POSM_SETTING_VALUE = pv_process_id;
        IF SQL%ROWCOUNT != 1 THEN
            RAISE NO_DATA_FOUND;
        END IF;
    END;
    
    PROCEDURE UPDATE_BATCH_FEEDBACK_REF_CD(
        pv_settlement_batch_ref_cd PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_REF_CD%TYPE,
        pv_payment_subtype_class PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE,
        pd_auth_authority_ts PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_END_TS%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pv_authority_resp_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pn_settlement_batch_id OUT PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_terminal_batch_id OUT PSS.SETTLEMENT_BATCH.TERMINAL_BATCH_ID%TYPE)
    IS
        ln_settlement_batch_state_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_STATE_ID%TYPE;
    BEGIN
        SELECT SETTLEMENT_BATCH_ID, TERMINAL_BATCH_ID, SETTLEMENT_BATCH_STATE_ID
          INTO pn_settlement_batch_id, pn_terminal_batch_id, ln_settlement_batch_state_id
          FROM (SELECT SB.SETTLEMENT_BATCH_ID, SB.TERMINAL_BATCH_ID, SB.SETTLEMENT_BATCH_STATE_ID
          FROM PSS.SETTLEMENT_BATCH SB
          JOIN PSS.TERMINAL_BATCH TB ON SB.TERMINAL_BATCH_ID = TB.TERMINAL_BATCH_ID
          JOIN PSS.TERMINAL T ON TB.TERMINAL_ID = T.TERMINAL_ID
          JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
          JOIN AUTHORITY.AUTHORITY A ON M.AUTHORITY_ID = A.AUTHORITY_ID
          JOIN AUTHORITY.AUTHORITY_TYPE AUT ON A.AUTHORITY_TYPE_ID = AUT.AUTHORITY_TYPE_ID
          JOIN AUTHORITY.HANDLER H ON H.HANDLER_ID = AUT.HANDLER_ID
         WHERE SB.SETTLEMENT_BATCH_REF_CD = pv_settlement_batch_ref_cd
           AND H.HANDLER_CLASS = pv_payment_subtype_class
           AND TB.TERMINAL_BATCH_CLOSE_TS IS NOT NULL
         ORDER BY DECODE(SB.SETTLEMENT_BATCH_STATE_ID, 7, 1, 5), TB.TERMINAL_BATCH_CYCLE_NUM DESC, SB.SETTLEMENT_BATCH_ID DESC)
         WHERE ROWNUM = 1; 
        
        IF ln_settlement_batch_state_id = 1 OR (ln_settlement_batch_state_id IN(1,2,3,7) AND pc_auth_result_cd IN('P')) THEN
            RAISE_APPLICATION_ERROR(-20100, 'Settlement batch ' || pn_settlement_batch_id || ' was already processed');
        END IF;
           
        UPDATE PSS.SETTLEMENT_BATCH SB
           SET SB.SETTLEMENT_BATCH_STATE_ID = DECODE(pc_auth_result_cd, 'Y', 1, 'P', 7, 'N', 2, 'F', 3, 'O', 2),
               SB.SETTLEMENT_BATCH_END_TS = NVL(pd_auth_authority_ts, SYSDATE),
               SB.SETTLEMENT_BATCH_RESP_DESC = DECODE(TRIM(pv_authority_resp_desc), NULL, SETTLEMENT_BATCH_RESP_DESC, SUBSTR(SETTLEMENT_BATCH_RESP_DESC || ' => ' || TRIM(pv_authority_resp_desc) , 1, 2000))
         WHERE SB.SETTLEMENT_BATCH_ID = pn_settlement_batch_id;                 
    END;
    
    PROCEDURE UPDATE_BATCH_FEEDBACK(
        pv_merchant_cd PSS.MERCHANT.MERCHANT_CD%TYPE,
        pv_terminal_cd PSS.TERMINAL.TERMINAL_CD%TYPE,
        pv_terminal_batch_num PSS.TERMINAL_BATCH.TERMINAL_BATCH_NUM%TYPE,
        pd_auth_authority_ts PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_END_TS%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pv_authority_resp_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pn_settlement_batch_id OUT PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_terminal_batch_id OUT PSS.SETTLEMENT_BATCH.TERMINAL_BATCH_ID%TYPE)
    IS
        ln_settlement_batch_state_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_STATE_ID%TYPE;
    BEGIN
        SELECT SETTLEMENT_BATCH_ID, TERMINAL_BATCH_ID, SETTLEMENT_BATCH_STATE_ID
          INTO pn_settlement_batch_id, pn_terminal_batch_id, ln_settlement_batch_state_id
          FROM (SELECT SB.SETTLEMENT_BATCH_ID, SB.TERMINAL_BATCH_ID, SB.SETTLEMENT_BATCH_STATE_ID
          FROM PSS.SETTLEMENT_BATCH SB
          JOIN PSS.TERMINAL_BATCH TB ON SB.TERMINAL_BATCH_ID = TB.TERMINAL_BATCH_ID
          JOIN PSS.TERMINAL T ON TB.TERMINAL_ID = T.TERMINAL_ID
          JOIN PSS.MERCHANT M ON T.MERCHANT_ID = M.MERCHANT_ID
         WHERE M.MERCHANT_CD = pv_merchant_cd
           AND T.TERMINAL_CD = pv_terminal_cd
           AND TB.TERMINAL_BATCH_NUM = pv_terminal_batch_num
           AND TB.TERMINAL_BATCH_CLOSE_TS IS NOT NULL
         ORDER BY DECODE(SB.SETTLEMENT_BATCH_STATE_ID, 7, 1, 5), TB.TERMINAL_BATCH_CYCLE_NUM DESC, SB.SETTLEMENT_BATCH_ID DESC)
         WHERE ROWNUM = 1; 
           
        IF ln_settlement_batch_state_id = 1 OR (ln_settlement_batch_state_id IN(1,2,3,7) AND pc_auth_result_cd IN('P')) THEN
            RAISE_APPLICATION_ERROR(-20100, 'Settlement batch ' || pn_settlement_batch_id || ' was already processed');
        END IF;
        
        UPDATE PSS.SETTLEMENT_BATCH SB
           SET SB.SETTLEMENT_BATCH_STATE_ID = DECODE(pc_auth_result_cd, 'Y', 1, 'P', 7, 'N', 2, 'F', 3, 'O', 2),
               SB.SETTLEMENT_BATCH_END_TS = NVL(pd_auth_authority_ts, SYSDATE),
               SB.SETTLEMENT_BATCH_RESP_DESC = DECODE(TRIM(pv_authority_resp_desc), NULL, SETTLEMENT_BATCH_RESP_DESC, SUBSTR(SETTLEMENT_BATCH_RESP_DESC || ' => ' || TRIM(pv_authority_resp_desc) , 1, 2000))
         WHERE SB.SETTLEMENT_BATCH_ID = pn_settlement_batch_id;                 
    END;
   
   -- R29+ signature 
    PROCEDURE UPDATE_TRAN_FEEDBACK(
        pn_terminal_batch_id PSS.SETTLEMENT_BATCH.TERMINAL_BATCH_ID%TYPE,
        pn_settlement_batch_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_sale_auth_id PSS.AUTH.AUTH_ID%TYPE,
        pn_refund_id PSS.REFUND.REFUND_ID%TYPE,
        pc_tran_type_cd CHAR,
        pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
        pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pv_authority_response_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
        pv_authority_response_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
        pd_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
        pn_approved_amt PSS.AUTH.AUTH_AMT_APPROVED%TYPE,
        pn_sale_amt PSS.AUTH.AUTH_AMT%TYPE,
        pc_major_currency_used CHAR,
        pv_tran_global_trans_cd OUT PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE)
    IS
        ln_original_auth_id PSS.AUTH.AUTH_ID%TYPE;
        ln_tran_id PSS.TRAN.TRAN_ID%TYPE;
        ln_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE;
        lc_entry_method_cd PSS.AUTH.ACCT_ENTRY_METHOD_CD%TYPE;
        lv_trace_number PSS.AUTH.TRACE_NUMBER%TYPE;
        ln_auth_id PSS.AUTH.AUTH_ID%TYPE;
        ln_refund_id PSS.REFUND.REFUND_ID%TYPE;
        lc_tran_state_cd PSS.TRAN.TRAN_STATE_CD%TYPE;
        lc_auth_type_cd PSS.AUTH.AUTH_TYPE_CD%TYPE;
        lc_refund_type_cd PSS.REFUND.REFUND_TYPE_CD%TYPE;
    BEGIN
        IF pn_sale_auth_id IS NOT NULL THEN
            ln_auth_id := pn_sale_auth_id;
        ELSIF pn_refund_id IS NOT NULL THEN
            ln_refund_id := pn_refund_id;
        ELSIF pc_tran_type_cd = '+' THEN
            SELECT AUTH_ID
              INTO ln_auth_id
              FROM (SELECT SA.AUTH_ID
              FROM PSS.AUTH A
              JOIN PSS.AUTH SA ON A.TRAN_ID = SA.TRAN_ID
             WHERE SA.TERMINAL_BATCH_ID = pn_terminal_batch_id
               AND A.AUTH_AUTHORITY_TRAN_CD = pv_authority_tran_cd 
               AND A.AUTH_AUTHORITY_REF_CD = pv_authority_ref_cd
               AND SA.AUTH_TYPE_CD IN('U','S','O','A','D','C','E','V','I')
               AND A.AUTH_TYPE_CD IN('L','N')
             ORDER BY SA.AUTH_TS DESC, SA.AUTH_ID DESC)
             WHERE ROWNUM = 1;
        ELSIF pc_tran_type_cd = '-' THEN
            SELECT REFUND_ID
              INTO ln_refund_id
              FROM (SELECT R.REFUND_ID, R.TRAN_ID
                      FROM PSS.REFUND R
                     WHERE R.TERMINAL_BATCH_ID = pn_terminal_batch_id
                       AND R.REFUND_AUTHORITY_TRAN_CD = pv_authority_tran_cd 
                       AND R.REFUND_AUTHORITY_REF_CD = pv_authority_ref_cd
                       AND R.REFUND_STATE_ID IN(6, 1)
                     ORDER BY R.CREATED_TS DESC, R.REFUND_ID DESC)
             WHERE ROWNUM = 1;
        ELSE
            RAISE_APPLICATION_ERROR(-20558, 'Invalid tran type cd "' ||  pc_tran_type_cd + '"');
        END IF;
        
        IF ln_auth_id IS NOT NULL THEN
            IF pc_major_currency_used = 'Y' THEN
                ln_minor_currency_factor := 1;
            ELSE
                SELECT C.MINOR_CURRENCY_FACTOR
                  INTO ln_minor_currency_factor
                  FROM PSS.CURRENCY C
                  JOIN PSS.POS_PTA PTA ON C.CURRENCY_CD = NVL(pta.CURRENCY_CD, 'USD')
                  JOIN PSS.TRAN X ON PTA.POS_PTA_ID = X.POS_PTA_ID
                  JOIN PSS.AUTH A ON A.TRAN_ID = X.TRAN_ID
                 WHERE A.AUTH_ID = ln_auth_id;
            END IF;
            UPDATE PSS.AUTH 
               SET AUTH_STATE_ID = DECODE(pc_auth_result_cd, 'Y', 2, 'P', 5, 'N', 3, 'O', 7, 'F', 4),
                   AUTH_RESULT_CD = pc_auth_result_cd,
                   AUTH_RESP_CD = NVL(pv_authority_response_cd, AUTH_RESP_CD),
                   AUTH_RESP_DESC = NVL(pv_authority_response_desc, AUTH_RESP_DESC),
                   AUTH_AUTHORITY_TRAN_CD = NVL(pv_authority_tran_cd, AUTH_AUTHORITY_TRAN_CD),
                   AUTH_AUTHORITY_REF_CD = NVL(pv_authority_ref_cd, AUTH_AUTHORITY_REF_CD),
                   AUTH_AUTHORITY_TS = NVL(pd_authority_ts, AUTH_AUTHORITY_TS),
                   AUTH_AMT_APPROVED = NVL(pn_approved_amt / ln_minor_currency_factor, AUTH_AMT_APPROVED),
                   AUTH_AUTHORITY_MISC_DATA = NVL(pv_authority_misc_data, AUTH_AUTHORITY_MISC_DATA)
             WHERE AUTH_ID = ln_auth_id
             RETURNING TRAN_ID, AUTH_TYPE_CD INTO ln_tran_id, lc_auth_type_cd;
            IF pn_settlement_batch_id IS NOT NULL THEN
                UPDATE PSS.TRAN_SETTLEMENT_BATCH
                   SET TRAN_SETTLEMENT_B_AMT = NVL(pn_approved_amt / ln_minor_currency_factor, TRAN_SETTLEMENT_B_AMT),
                       TRAN_SETTLEMENT_B_RESP_CD = NVL(pv_authority_response_cd, TRAN_SETTLEMENT_B_RESP_CD),
                       TRAN_SETTLEMENT_B_RESP_DESC = NVL(pv_authority_response_desc, TRAN_SETTLEMENT_B_RESP_DESC)
                 WHERE SETTLEMENT_BATCH_ID = pn_settlement_batch_id
                   AND AUTH_ID = ln_auth_id;               
            END IF;
        ELSIF ln_refund_id IS NOT NULL THEN
            IF pc_major_currency_used = 'Y' THEN
                ln_minor_currency_factor := 1;
            ELSE
                SELECT C.MINOR_CURRENCY_FACTOR
                  INTO ln_minor_currency_factor
                  FROM PSS.CURRENCY C
                  JOIN PSS.POS_PTA PTA ON C.CURRENCY_CD = NVL(pta.CURRENCY_CD, 'USD')
                  JOIN PSS.TRAN X ON PTA.POS_PTA_ID = X.POS_PTA_ID
                  JOIN PSS.REFUND R ON X.TRAN_ID = R.TRAN_ID
                 WHERE R.REFUND_ID = ln_refund_id;
            END IF;          
            UPDATE PSS.REFUND
               SET REFUND_STATE_ID = DECODE(pc_auth_result_cd, 'Y', 1, 'P', 1, 'N', 2, 'O', 2, 'F', 3),
                REFUND_RESP_CD = NVL(pv_authority_response_cd, REFUND_RESP_CD),
                REFUND_RESP_DESC = NVL(pv_authority_response_desc, REFUND_RESP_DESC),
                REFUND_AUTHORITY_TS = NVL(pd_authority_ts, REFUND_AUTHORITY_TS),
                REFUND_AUTHORITY_MISC_DATA = NVL(pv_authority_misc_data, REFUND_AUTHORITY_MISC_DATA)
             WHERE REFUND_ID = ln_refund_id
             RETURNING TRAN_ID, REFUND_TYPE_CD INTO ln_tran_id, lc_refund_type_cd;
            IF pn_settlement_batch_id IS NOT NULL THEN 
                UPDATE PSS.REFUND_SETTLEMENT_BATCH
                   SET REFUND_SETTLEMENT_B_AMT = NVL(pn_approved_amt / ln_minor_currency_factor, REFUND_SETTLEMENT_B_AMT),
                       REFUND_SETTLEMENT_B_RESP_CD = NVL(pv_authority_response_cd, REFUND_SETTLEMENT_B_RESP_CD),
                       REFUND_SETTLEMENT_B_RESP_DESC = NVL(pv_authority_response_desc, REFUND_SETTLEMENT_B_RESP_DESC)
                 WHERE SETTLEMENT_BATCH_ID = pn_settlement_batch_id
                   AND REFUND_ID = ln_refund_id;   
            END IF;
        END IF;
        
        -- update tran_state_cd
        UPDATE PSS.TRAN
           SET TRAN_STATE_CD = AFTER_SETTLE_TRAN_STATE_CD(lc_auth_type_cd, lc_refund_type_cd, pc_auth_result_cd, TRAN_DEVICE_RESULT_TYPE_CD)
         WHERE TRAN_ID = ln_tran_id
         RETURNING TRAN_GLOBAL_TRANS_CD INTO pv_tran_global_trans_cd;
    END;
    
    -- R28 signature
    PROCEDURE UPDATE_TRAN_FEEDBACK(
        pn_terminal_batch_id PSS.SETTLEMENT_BATCH.TERMINAL_BATCH_ID%TYPE,
        pn_settlement_batch_id PSS.SETTLEMENT_BATCH.SETTLEMENT_BATCH_ID%TYPE,
        pn_sale_auth_id PSS.AUTH.AUTH_ID%TYPE,
        pn_refund_id PSS.REFUND.REFUND_ID%TYPE,
        pc_tran_type_cd CHAR,
        pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
        pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
        pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
        pv_authority_response_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
        pv_authority_response_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
        pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
        pd_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
        pn_approved_amt PSS.AUTH.AUTH_AMT_APPROVED%TYPE,
        pn_sale_amt PSS.AUTH.AUTH_AMT%TYPE,
        pc_major_currency_used CHAR)
    IS
        lv_tran_global_trans_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE;
    BEGIN
        UPDATE_TRAN_FEEDBACK(
            pn_terminal_batch_id,
            pn_settlement_batch_id,
            pn_sale_auth_id,
            pn_refund_id,
            pc_tran_type_cd,
            pv_authority_tran_cd,
            pv_authority_ref_cd,
            pc_auth_result_cd,
            pv_authority_response_cd,
            pv_authority_response_desc,
            pv_authority_misc_data,
            pd_authority_ts,
            pn_approved_amt,
            pn_sale_amt,
            pc_major_currency_used,
            lv_tran_global_trans_cd);
    END;
    
END PKG_SETTLEMENT;
/

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/DBADMIN/PKG_CONST.psk?rev=1.19
CREATE OR REPLACE PACKAGE DBADMIN.PKG_CONST 
  IS

DB_TIME_ZONE CONSTANT VARCHAR2(60) := 'US/Eastern';
DB_TIME_ZONE_CD CONSTANT VARCHAR2(50) := 'EST';
GMT_TIME_ZONE CONSTANT VARCHAR2(60) := 'Europe/London';

-- conditional logic
BOOLEAN__FALSE CONSTANT NUMBER := 0;
BOOLEAN__TRUE CONSTANT NUMBER := 1;

-- ASCII characters
ASCII__NUL CONSTANT CHAR(1) := CHR(0);
ASCII__LF CONSTANT CHAR(1) := CHR(10);

-- client payment type codes
CLNT_PMNT_TYPE__CASH CONSTANT CHAR(1) := 'M';
CLNT_PMNT_TYPE__CREDIT CONSTANT CHAR(1) := 'C';
CLNT_PMNT_TYPE__RFID_CREDIT CONSTANT CHAR(1) := 'R';
CLNT_PMNT_TYPE__RFID_SPECIAL CONSTANT CHAR(1) := 'P';
CLNT_PMNT_TYPE__SPECIAL CONSTANT CHAR(1) := 'S';

-- device types
DEVICE_TYPE__G4 CONSTANT NUMBER := 0;
DEVICE_TYPE__GX CONSTANT NUMBER := 1;
DEVICE_TYPE__NG CONSTANT NUMBER := 3;
DEVICE_TYPE__LEGACY_KIOSK CONSTANT NUMBER := 4;
DEVICE_TYPE__ESUDS CONSTANT NUMBER := 5;
DEVICE_TYPE__MEI CONSTANT NUMBER := 6;
DEVICE_TYPE__EZ80_DEVELOPMENT CONSTANT NUMBER := 7;
DEVICE_TYPE__EZ80 CONSTANT NUMBER := 8;
DEVICE_TYPE__LEGACY_G4 CONSTANT NUMBER := 9;
DEVICE_TYPE__TRANSACT CONSTANT NUMBER := 10;
DEVICE_TYPE__KIOSK CONSTANT NUMBER := 11;
DEVICE_TYPE__T2 CONSTANT NUMBER := 12;
DEVICE_TYPE__EDGE CONSTANT NUMBER := 13;

DSP__SETTLEMENT_SCHEDULE CONSTANT VARCHAR2(60) := '85';
DSP__NON_ACTIV_CALL_IN_SCHED CONSTANT VARCHAR2(60) := '86';
DSP__ACTIVATED_CALL_IN_SCHED CONSTANT VARCHAR2(60) := '87';
DSP__DEX_SCHEDULE CONSTANT VARCHAR2(60) := '1101';
DSP__VMC_INTERFACE_TYPE CONSTANT VARCHAR2(60) := '1500';
DSP__PROPERTY_LIST_VERSION CONSTANT VARCHAR2(60) := 'Property List Version';

-- errors
ERROR__GENERIC_FAILURE CONSTANT VARCHAR2(255) := 'Generic Failure';
ERROR__NO_ERROR CONSTANT VARCHAR2(255) := NULL;

-- event code prefixes
EVENT_CODE_PREFIX__APP_LAYER CONSTANT CHAR := 'A';
EVENT_CODE_PREFIX__LEGACY CONSTANT CHAR := 'X';

-- event states (DEVICE.EVENT_STATE)
EVENT_STATE__INCOMPLETE CONSTANT NUMBER := 1;
EVENT_STATE__COMPLETE_FINAL CONSTANT NUMBER := 2;
EVENT_STATE__COMPLETE_ERROR CONSTANT NUMBER := 3;
EVENT_STATE__INCOMPLETE_ERROR CONSTANT NUMBER := 4;

-- file transfer types (DEVICE.FILE_TRANSFER_TYPE)
FILE_TYPE__DEX CONSTANT NUMBER := 0;
FILE_TYPE__CONFIG CONSTANT NUMBER := 1;
FILE_TYPE__APP_UPGRADE CONSTANT NUMBER := 5;
FILE_TYPE__PROPERTY_LIST CONSTANT NUMBER := 19;

-- Edge schedule reoccurrence types
REOCCURRENCE_TYPE__DAILY CONSTANT CHAR(1) := 'D';
REOCCURRENCE_TYPE__INTERVAL CONSTANT CHAR(1) := 'I';
REOCCURRENCE_TYPE__MONTHLY CONSTANT CHAR(1) := 'M';
REOCCURRENCE_TYPE__WEEKLY CONSTANT CHAR(1) := 'W';

-- result codes
RESULT__FAILURE CONSTANT NUMBER := 0;
RESULT__SUCCESS CONSTANT NUMBER := 1;
RESULT__TRAN_NOT_FOUND CONSTANT NUMBER := 2;
RESULT__ILLEGAL_STATE CONSTANT NUMBER := 3;
RESULT__INVALID_PARAMETER CONSTANT NUMBER := 4;
RESULT__DUPLICATE CONSTANT NUMBER := 5;
RESULT__PAYMENT_NOT_ACCEPTED CONSTANT NUMBER := 6;
RESULT__HOST_NOT_FOUND CONSTANT NUMBER := 7;
RESULT__ITEM_TYPE_NOT_FOUND CONSTANT NUMBER := 8;

-- sale results (PSS.SALE_RESULT)
SALE_RES__SUCCESS CONSTANT NUMBER := 0;
SALE_RES__CANCELLED_BY_USER CONSTANT NUMBER := 1;
SALE_RES__CNCLD_AUTH_TIMEOUT CONSTANT NUMBER := 2;
SALE_RES__CNCLD_AUTH_FAILURE CONSTANT NUMBER := 3;
SALE_RES__CNCLD_MACHINE_FAIL CONSTANT NUMBER := 4;

-- sale types (PSS.SALE_TYPE)
SALE_TYPE__ACTUAL CONSTANT CHAR(1) := 'A';
SALE_TYPE__CASH CONSTANT CHAR(1) := 'C';
SALE_TYPE__INTENDED CONSTANT CHAR(1) := 'I';

-- Edge schedule constants
SCHEDULE__FS CONSTANT CHAR(1) := '^';
SCHEDULE__SUNDAY CONSTANT CHAR(1) := '0';
SCHEDULE__MONDAY CONSTANT CHAR(1) := '1';
SCHEDULE__TUESDAY CONSTANT CHAR(1) := '2';
SCHEDULE__WEDNESDAY CONSTANT CHAR(1) := '3';
SCHEDULE__THURSDAY CONSTANT CHAR(1) := '4';
SCHEDULE__FRIDAY CONSTANT CHAR(1) := '5';
SCHEDULE__SATURDAY CONSTANT CHAR(1) := '6';

-- tran line item types (PSS.TRAN_LINE_ITEM_TYPE)
TLI__CANCELLATION_ADJMT CONSTANT NUMBER := 312;
TLI__NEG_DISCREPANCY_ADJMT CONSTANT NUMBER := 313;
TLI__POS_DISCREPANCY_ADJMT CONSTANT NUMBER := 314;

-- transaction batch types
TRAN_BATCH_TYPE__ACTUAL CONSTANT CHAR(1) := 'A';
TRAN_BATCH_TYPE__INTENDED CONSTANT CHAR(1) := 'I';

-- transaction result codes reported by device (PSS.TRAN_DEVICE_RESULT_TYPE)
TRAN_DEV_RES__AUTH_FAILURE CONSTANT VARCHAR2(1) := 'U';
TRAN_DEV_RES__CANCELLED CONSTANT VARCHAR2(1) := 'C';
TRAN_DEV_RES__FAILURE CONSTANT VARCHAR2(1) := 'F';
TRAN_DEV_RES__INCOMPLETE CONSTANT VARCHAR2(1) := 'I';
TRAN_DEV_RES__SUCCESS CONSTANT VARCHAR2(1) := 'S';
TRAN_DEV_RES__SUCCESS_NO_PRNTR CONSTANT VARCHAR2(1) := 'Q';
TRAN_DEV_RES__SUCCESS_NO_RCPT CONSTANT VARCHAR2(1) := 'N';
TRAN_DEV_RES__SUCCESS_RCPT_ERR CONSTANT VARCHAR2(1) := 'R';
TRAN_DEV_RES__TIMEOUT CONSTANT VARCHAR2(1) := 'T';

-- transaction states (PSS.TRAN_STATE)
TRAN_STATE__AUTH_DECLINE CONSTANT VARCHAR2(1) := '7';
TRAN_STATE__AUTH_EXPIRED CONSTANT VARCHAR2(1) := 'L';
TRAN_STATE__AUTH_COND_EXPIRED CONSTANT VARCHAR2(1) := 'M';
TRAN_STATE__AUTH_FAILURE CONSTANT VARCHAR2(1) := '5';
TRAN_STATE__AUTH_REVERSED CONSTANT VARCHAR2(1) := 'V';
TRAN_STATE__AUTH_PEND_RVRSL CONSTANT VARCHAR2(1) := 'W';
TRAN_STATE__AUTH_SUCCESS CONSTANT VARCHAR2(1) := '6';
TRAN_STATE__AUTH_SUCCESS_COND CONSTANT VARCHAR2(1) := '0';
TRAN_STATE__BATCH CONSTANT VARCHAR2(1) := '8';
TRAN_STATE__BATCH_INTENDED CONSTANT VARCHAR2(1) := '9';
TRAN_STATE__CLIENT_CANCELLED CONSTANT VARCHAR2(1) := 'C';
TRAN_STATE__COMPLETE CONSTANT VARCHAR2(1) := 'D';
TRAN_STATE__COMPLETE_ERROR CONSTANT VARCHAR2(1) := 'E';
TRAN_STATE__DUPLICATE CONSTANT VARCHAR2(1) := 'Z';
TRAN_STATE__INCOMPLETE CONSTANT VARCHAR2(1) := 'I';
TRAN_STATE__INCOMPLETE_ERROR CONSTANT VARCHAR2(1) := 'J';
TRAN_STATE__INTENDED_ERROR CONSTANT VARCHAR2(1) := 'K';
TRAN_STATE__PREPARED_TRAN CONSTANT VARCHAR2(1) := 'P';
TRAN_STATE__PROCESSED_TRAN CONSTANT VARCHAR2(1) := 'T';
TRAN_STATE__PROCESSED_TRAN_INT CONSTANT VARCHAR2(1) := 'U';
TRAN_STATE__PROCESSING_BATCH CONSTANT VARCHAR2(1) := '4';
TRAN_STATE__PRCSNG_BATCH_INTD CONSTANT VARCHAR2(1) := '3';
TRAN_STATE__PRCSNG_BATCH_LOCAL CONSTANT VARCHAR2(1) := '1';
TRAN_STATE__PROCESSING_STTLMT CONSTANT VARCHAR2(1) := 'S';
TRAN_STATE__PROCESSING_TRAN CONSTANT VARCHAR2(1) := 'A';
TRAN_STATE__PRCSNG_TRAN_RETRY CONSTANT VARCHAR2(1) := 'B';
TRAN_STATE__SALE_NO_AUTH CONSTANT VARCHAR2(1) := 'F';
TRAN_STATE__SALE_NO_AUTH_ERROR CONSTANT VARCHAR2(1) := 'G';
TRAN_STATE__SALE_NO_AUTH_INTND CONSTANT VARCHAR2(1) := 'H';
TRAN_STATE__SETTLEMENT CONSTANT VARCHAR2(1) := 'Q';
TRAN_STATE__STLMT_ERROR CONSTANT VARCHAR2(1) := 'R';
TRAN_STATE__STLMT_INCOMPLETE CONSTANT VARCHAR2(1) := 'N';
TRAN_STATE__WAITING_FOR_AUTH CONSTANT VARCHAR2(1) := '2';

TRANS_TYPE__CREDIT CONSTANT NUMBER := 16;
TRANS_TYPE__ACCESS CONSTANT NUMBER := 17;
TRANS_TYPE__PASS CONSTANT NUMBER := 18;
TRANS_TYPE__CASH CONSTANT NUMBER := 22;

VMC_INTERFACE__STANDARD_MDB CONSTANT VARCHAR2(200) := '1';
VMC_INTERFACE__ETRANS_MDB CONSTANT VARCHAR2(200) := '2';
VMC_INTERFACE__COIN_PULSE CONSTANT VARCHAR2(200) := '3';

END;
/

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/DMS/DMS-1.0.5-USADBP.1.sql?rev=HEAD
GRANT SELECT ON DEVICE.SETTING_PARAMETER_TYPE TO USAT_DMS_ROLE, USAT_APP_LAYER_ROLE;
GRANT EXECUTE ON DEVICE.SP_UPDATE_CONFIG_AND_RETURN TO USAT_DMS_ROLE;

UPDATE DEVICE.DEVICE_SETTING_PARAMETER
SET DEVICE_SETTING_UI_CONFIGURABLE = 'N'
WHERE DEVICE_SETTING_PARAMETER_CD IN ('60', '61');

UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
SET EDITOR = 'SELECT:1=1;2=2;3=3;4=4;5=5;6=6',
DESCRIPTION = '1 - Standard MDB<br />2 - eTrans MDB<br />3 - Coin Pulse<br />4 - Coin Pulse Dual Enable<br />5 - Top-off Coin Pulse<br />6 - Serial Edge'
WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1500';

UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
SET NAME = 'Disable Message Line 1', DESCRIPTION = 'The device will display this message on the top line of the display when disabled by the VMC'
WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1527';

UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
SET NAME = 'Disable Message Line 2', DESCRIPTION = 'The device will display this message on the bottom line of the display when disabled by the VMC'
WHERE DEVICE_TYPE_ID = 13 AND DEVICE_SETTING_PARAMETER_CD = '1528';

COMMIT;

ALTER TABLE PSS.AUTH ADD(CARD_KEY VARCHAR2(100));
ALTER TABLE DEVICE.DEVICE_SETTING ADD (FILE_ORDER NUMBER(20, 0));
ALTER TABLE DEVICE.CONFIG_TEMPLATE_SETTING ADD (FILE_ORDER NUMBER(20, 0));

UPDATE DEVICE.CONFIG_TEMPLATE_SETTING SET DESCRIPTION = 'SSN (silicone serial number), terminal serial number with the bytes in decimal format and a comma as the byte separator' WHERE DEVICE_TYPE_ID = 11 AND DEVICE_SETTING_PARAMETER_CD = 'SSN';
COMMIT;

DECLARE
  LV_SQL VARCHAR2(4000);
BEGIN
  SELECT 'CREATE SEQUENCE DEVICE.RERIX_MACHINE_ID_SEQ INCREMENT BY 1 MINVALUE ' || MIN_VALUE || ' MAXVALUE ' || MAX_VALUE 
    || ' START WITH ' || LAST_NUMBER || ' ORDER NOCYCLE' 
  INTO LV_SQL
  FROM DBA_SEQUENCES 
  WHERE SEQUENCE_OWNER = 'TAZDBA' AND SEQUENCE_NAME = 'RERIX_MACHINE_ID_SEQ';

  EXECUTE IMMEDIATE LV_SQL;
  
  SELECT 'CREATE SEQUENCE ENGINE.SEQ_OB_EMAIL_QUEUE_ID INCREMENT BY 1 MINVALUE ' || MIN_VALUE || ' MAXVALUE ' || MAX_VALUE 
    || ' START WITH ' || LAST_NUMBER || ' NOORDER NOCYCLE' 
  INTO LV_SQL
  FROM DBA_SEQUENCES 
  WHERE SEQUENCE_OWNER = 'TAZDBA' AND SEQUENCE_NAME = 'SEQ_OB_EMAIL_QUEUE_ID';

  EXECUTE IMMEDIATE LV_SQL;
END;
/

DROP PUBLIC SYNONYM RERIX_MACHINE_ID_SEQ;

ALTER TABLE TAZDBA.OB_EMAIL_QUEUE RENAME TO OB_EMAIL_QUEUE_OLD;

CREATE TABLE ENGINE.OB_EMAIL_QUEUE
(
  OB_EMAIL_QUEUE_ID              NUMBER(20),
  OB_EMAIL_FROM_EMAIL_ADDR       VARCHAR2(60) NOT NULL,
  OB_EMAIL_FROM_NAME             VARCHAR2(60) NOT NULL,
  OB_EMAIL_TO_EMAIL_ADDR         VARCHAR2(60) NOT NULL,
  OB_EMAIL_TO_NAME               VARCHAR2(60) NOT NULL,
  OB_EMAIL_SUBJECT               VARCHAR2(100) NOT NULL,
  OB_EMAIL_MSG                   VARCHAR2(4000) NOT NULL,
  OB_EMAIL_SCHEDULED_SEND_TS     DATE,
  OB_EMAIL_LAST_SEND_ATTEMPT_TS  DATE,
  OB_EMAIL_TOT_SEND_ATTEMP_NUM   NUMBER(2),
  OB_EMAIL_SENT_SUCCESS_TS       DATE,
  CREATED_BY                     VARCHAR2(30) NOT NULL,
  CREATED_TS                     DATE           NOT NULL,
  LAST_UPDATED_BY                VARCHAR2(30) NOT NULL,
  LAST_UPDATED_TS                DATE           NOT NULL
)
TABLESPACE ENGINE_DATA
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            BUFFER_POOL      DEFAULT
           )
LOGGING
PARTITION BY RANGE (CREATED_TS)
(  
  PARTITION "2012-02-01" VALUES LESS THAN (TO_DATE(' 2012-03-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN')), 
  PARTITION "2012-03-01" VALUES LESS THAN (TO_DATE(' 2012-04-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN')),  
  PARTITION "2012-04-01" VALUES LESS THAN (TO_DATE(' 2012-05-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN')),
PARTITION "2012-05-01" VALUES LESS THAN (TO_DATE(' 2012-06-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN')));

CREATE UNIQUE INDEX ENGINE.PK_OB_EMAIL_QUEUE ON ENGINE.OB_EMAIL_QUEUE
(OB_EMAIL_QUEUE_ID)
LOGGING
TABLESPACE ENGINE_INDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            FREELISTS        4
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

CREATE INDEX ENGINE.IDX_0B_00001 ON ENGINE.OB_EMAIL_QUEUE
(OB_EMAIL_TOT_SEND_ATTEMP_NUM)
LOGGING
TABLESPACE ENGINE_INDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            FREELISTS        4
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

CREATE INDEX ENGINE.IDX_OB_00002 ON ENGINE.OB_EMAIL_QUEUE
(OB_EMAIL_SCHEDULED_SEND_TS)
LOGGING
TABLESPACE ENGINE_INDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            FREELISTS        4
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

CREATE INDEX ENGINE.IX_OB_EMAIL_QUEUE_TS ON ENGINE.OB_EMAIL_QUEUE
(CREATED_TS)
  TABLESPACE ENGINE_INDX
  PCTFREE    10
  INITRANS   2
  MAXTRANS   255
  STORAGE    (
              INITIAL          1M
              NEXT             1M
              MINEXTENTS       1
              MAXEXTENTS       UNLIMITED
              PCTINCREASE      0
	      FREELISTS        1
              FREELIST GROUPS  1
              BUFFER_POOL      DEFAULT
             )
LOGGING
LOCAL NOPARALLEL;

CREATE OR REPLACE TRIGGER ENGINE.TRBU_OB_EMAIL_QUEUE
   BEFORE UPDATE
   ON ENGINE.OB_EMAIL_QUEUE
   FOR EACH ROW
BEGIN
	SELECT 
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
        USER
    INTO 
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
        :NEW.last_updated_by
    FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER ENGINE.TRBI_OB_EMAIL_QUEUE
 BEFORE
  INSERT
 ON ENGINE.OB_EMAIL_QUEUE
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
Begin
    IF :new.ob_email_queue_id IS NULL THEN
      SELECT seq_ob_email_queue_id.nextval
        into :new.ob_email_queue_id
        FROM dual;
    END IF;

    IF :new.ob_email_scheduled_send_ts is null THEN
        :new.ob_email_scheduled_send_ts := sysdate;
    END IF;

    -- this is a minor fix for java, it's inserting date with no time
    IF(:new.ob_email_scheduled_send_ts = trunc(sysdate)) THEN
        :new.ob_email_scheduled_send_ts := sysdate;
    END IF;

	SELECT
           sysdate,
           user,
           sysdate,
           user,
           0
      into
           :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by,
           :new.OB_EMAIL_TOT_SEND_ATTEMP_NUM
      FROM dual;
End;
/

ALTER TABLE ENGINE.OB_EMAIL_QUEUE ADD (
  CONSTRAINT PK_OB_EMAIL_QUEUE
  PRIMARY KEY
  (OB_EMAIL_QUEUE_ID)
  USING INDEX ENGINE.PK_OB_EMAIL_QUEUE
  ENABLE NOVALIDATE);

insert /*+append nologging */ into ENGINE.OB_EMAIL_QUEUE select * from tazdba.OB_EMAIL_QUEUE_OLD;
commit;

GRANT DELETE ON ENGINE.OB_EMAIL_QUEUE TO USAT_ESUDSOPER_ROLE;
GRANT DELETE ON ENGINE.OB_EMAIL_QUEUE TO ESUDS_DEVELOPER;
GRANT DELETE ON ENGINE.OB_EMAIL_QUEUE TO USAT_SECURITY_ROLE;
GRANT INSERT ON ENGINE.OB_EMAIL_QUEUE TO USAT_ESUDSOPER_ROLE;
GRANT INSERT ON ENGINE.OB_EMAIL_QUEUE TO ESUDS_DEVELOPER;
GRANT INSERT ON ENGINE.OB_EMAIL_QUEUE TO USAT_SECURITY_ROLE;
GRANT SELECT ON ENGINE.OB_EMAIL_QUEUE TO ESUDS_DEVELOPER;
GRANT SELECT ON ENGINE.OB_EMAIL_QUEUE TO USAT_ESUDSOPER_ROLE;
GRANT SELECT ON ENGINE.OB_EMAIL_QUEUE TO USAT_SECURITY_ROLE;
GRANT UPDATE ON ENGINE.OB_EMAIL_QUEUE TO USAT_SECURITY_ROLE;
GRANT UPDATE ON ENGINE.OB_EMAIL_QUEUE TO USAT_ESUDSOPER_ROLE;
GRANT UPDATE ON ENGINE.OB_EMAIL_QUEUE TO ESUDS_DEVELOPER;
GRANT SELECT ON ENGINE.SEQ_OB_EMAIL_QUEUE_ID TO USAT_SECURITY_ROLE;
GRANT SELECT ON ENGINE.SEQ_OB_EMAIL_QUEUE_ID TO USAT_ESUDSOPER_ROLE;
GRANT SELECT ON ENGINE.SEQ_OB_EMAIL_QUEUE_ID TO ESUDS_DEVELOPER;
GRANT DELETE, INSERT, UPDATE ON ENGINE.OB_EMAIL_QUEUE TO USATECH_UPD_TRANS;
GRANT SELECT ON ENGINE.OB_EMAIL_QUEUE TO USAT_DEV_READ_ONLY;
GRANT DELETE, INSERT, SELECT, UPDATE, DEBUG ON ENGINE.OB_EMAIL_QUEUE TO USAT_ADMIN_ROLE;
GRANT DELETE, INSERT, SELECT, UPDATE ON ENGINE.OB_EMAIL_QUEUE TO USAT_APP_LAYER_ROLE;

CREATE OR REPLACE PUBLIC SYNONYM SEQ_OB_EMAIL_QUEUE_ID FOR ENGINE.SEQ_OB_EMAIL_QUEUE_ID;
CREATE OR REPLACE PUBLIC SYNONYM OB_EMAIL_QUEUE FOR ENGINE.OB_EMAIL_QUEUE;

CREATE OR REPLACE FUNCTION DBADMIN.EPOCH_DATE RETURN DATE DETERMINISTIC
IS
    ld_epoch_date CONSTANT DATE := TO_DATE('01/01/1970', 'MM/DD/YYYY');
BEGIN
    RETURN ld_epoch_date;
END;
/

GRANT EXECUTE ON DBADMIN.EPOCH_DATE TO PUBLIC;
CREATE OR REPLACE PUBLIC SYNONYM EPOCH_DATE FOR DBADMIN.EPOCH_DATE;

DECLARE
	CURSOR cur IS 
		select d.device_id, d.device_name, d.device_type_id
		from device.vw_device_last_active d
		where d.device_type_id in (0, 1)
		order by d.device_id;
	
	CURSOR cur_map IS
		select device_setting_parameter_cd, field_offset * 2 + 1 hex_field_offset, field_size * 2 hex_field_size
		from device.config_template_setting 
		where device_type_id = 0 and field_offset < 512 
		order by field_offset;
	
	ll_default_content file_transfer.file_transfer_content%TYPE;
	lv_default_content_g4 VARCHAR2(1024);
	lv_default_content_g5 VARCHAR2(1024);
	ll_content file_transfer.file_transfer_content%TYPE;
	lv_content VARCHAR2(1024);	
	ln_exists NUMBER;
BEGIN
	SELECT file_transfer_content
	INTO ll_default_content
	FROM device.file_transfer
	WHERE file_transfer_type_cd = 6 
		AND file_transfer_name = 'G4-DEFAULT-CFG';
		
	lv_default_content_g4 := UPPER(ll_default_content);

	SELECT file_transfer_content
	INTO ll_default_content
	FROM device.file_transfer
	WHERE file_transfer_type_cd = 6 
		AND file_transfer_name = 'G5-DEFAULT-CFG';
		
	lv_default_content_g5 := UPPER(ll_default_content);

	FOR rec_cur IN cur LOOP
		BEGIN
			ll_content := NULL;
		
			SELECT file_transfer_content
			INTO ll_content
			FROM (
				SELECT /*+INDEX(ft INX_FILE_TRANSFER_TYPE_NAME)*/ file_transfer_content
				FROM device.file_transfer ft 
				WHERE file_transfer_name = rec_cur.device_name || '-CFG'
					AND file_content_length(rowid) = 1024
				ORDER BY file_transfer_type_cd, created_ts
			) WHERE ROWNUM = 1;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				NULL;
			WHEN VALUE_ERROR THEN
				NULL;
		END;
		
		IF ll_content IS NULL THEN
			IF rec_cur.device_type_id = 0 THEN 
				lv_content := lv_default_content_g4; 
			ELSE
				lv_content := lv_default_content_g5;
			END IF;
		ELSE
			lv_content := UPPER(ll_content);
		END IF;
			
		DELETE FROM device.device_setting
		WHERE device_id = rec_cur.device_id
			AND device_setting_parameter_cd IN (
				SELECT device_setting_parameter_cd
				FROM device.device_setting_parameter
				WHERE device_setting_ui_configurable = 'Y');
	
		FOR rec_cur_map IN cur_map LOOP
			PKG_DEVICE_CONFIGURATION.SP_UPSERT_DEVICE_SETTING(rec_cur.device_id, rec_cur_map.device_setting_parameter_cd, 
				REPLACE(SUBSTR(lv_content, rec_cur_map.hex_field_offset, rec_cur_map.hex_field_size), ' ', '0'), ln_exists);
		END LOOP;			
		
		COMMIT;
	END LOOP;
END;
/

DECLARE
	CURSOR cur IS 
		select d.device_id, d.device_name
		from device.vw_device_last_active d
		where d.device_type_id = 6
		order by d.device_id;
	
	CURSOR cur_map IS
		select device_setting_parameter_cd, field_offset * 2 + 1 hex_field_offset, field_size * 2 hex_field_size
		from device.config_template_setting 
		where device_type_id = 6 and field_offset < 57
		order by field_offset;
	
	ll_default_content file_transfer.file_transfer_content%TYPE;
	lv_default_content VARCHAR2(114);
	ll_content file_transfer.file_transfer_content%TYPE;
	lv_content VARCHAR2(114);	
	ln_exists NUMBER;
BEGIN
	SELECT file_transfer_content
	INTO ll_default_content
	FROM device.file_transfer
	WHERE file_transfer_type_cd = 6 
		AND file_transfer_name = 'MEI-DEFAULT-CFG';
		
	lv_default_content := UPPER(ll_default_content);

	FOR rec_cur IN cur LOOP
		BEGIN
			ll_content := NULL;
		
			SELECT file_transfer_content
			INTO ll_content
			FROM (
				SELECT /*+INDEX(ft INX_FILE_TRANSFER_TYPE_NAME)*/ file_transfer_content
				FROM device.file_transfer ft 
				WHERE file_transfer_name = rec_cur.device_name || '-CFG'
					AND file_content_length(rowid) = 114
				ORDER BY file_transfer_type_cd, created_ts
			) WHERE ROWNUM = 1;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				NULL;
			WHEN VALUE_ERROR THEN
				NULL;
		END;
		
		IF ll_content IS NULL THEN
			lv_content := lv_default_content;
		ELSE
			lv_content := UPPER(ll_content);
		END IF;
			
		DELETE FROM device.device_setting
		WHERE device_id = rec_cur.device_id
			AND device_setting_parameter_cd IN (
				SELECT device_setting_parameter_cd
				FROM device.device_setting_parameter
				WHERE device_setting_ui_configurable = 'Y');
	
		FOR rec_cur_map IN cur_map LOOP
			PKG_DEVICE_CONFIGURATION.SP_UPSERT_DEVICE_SETTING(rec_cur.device_id, rec_cur_map.device_setting_parameter_cd, 
				REPLACE(SUBSTR(lv_content, rec_cur_map.hex_field_offset, rec_cur_map.hex_field_size), ' ', '0'), ln_exists);
		END LOOP;			
		
		COMMIT;
	END LOOP;
END;
/

ALTER TABLE DEVICE.DEVICE_TYPE ADD (DEFAULT_CONFIG_TEMPLATE_NAME VARCHAR2(200));

UPDATE DEVICE.DEVICE_TYPE SET DEFAULT_CONFIG_TEMPLATE_NAME = 'G4-DEFAULT-CFG' WHERE DEVICE_TYPE_ID = 0;
UPDATE DEVICE.DEVICE_TYPE SET DEFAULT_CONFIG_TEMPLATE_NAME = 'G5-DEFAULT-CFG' WHERE DEVICE_TYPE_ID = 1;
UPDATE DEVICE.DEVICE_TYPE SET DEFAULT_CONFIG_TEMPLATE_NAME = 'MEI-DEFAULT-CFG' WHERE DEVICE_TYPE_ID = 6;
UPDATE DEVICE.DEVICE_TYPE SET DEFAULT_CONFIG_TEMPLATE_NAME = 'KIOSK-DEFAULT-CFG' WHERE DEVICE_TYPE_ID = 11;
UPDATE DEVICE.DEVICE_TYPE SET DEFAULT_CONFIG_TEMPLATE_NAME = 'T2-DEFAULT-CFG' WHERE DEVICE_TYPE_ID = 12;
UPDATE DEVICE.DEVICE_TYPE SET DEFAULT_CONFIG_TEMPLATE_NAME = 'DEFAULT-CFG-13-' WHERE DEVICE_TYPE_ID = 13;
COMMIT;


CREATE TABLE DEVICE.DEVICE_SETTING_BACKUP
(
    DEVICE_ID                   NUMBER(20,0) NOT NULL,
    DEVICE_SETTING_PARAMETER_CD VARCHAR2(60) NOT NULL,
    DEVICE_SETTING_VALUE        VARCHAR2(200),
    CREATED_BY                  VARCHAR2(30) NOT NULL,
    CREATED_TS 					DATE NOT NULL,
    LAST_UPDATED_BY 			VARCHAR2(30) NOT NULL,
    LAST_UPDATED_TS 			DATE NOT NULL,
    FILE_ORDER 					NUMBER(20,0),
	CONSTRAINT PK_DEVICE_SETTING_BACKUP PRIMARY KEY(DEVICE_ID, DEVICE_SETTING_PARAMETER_CD),
	CONSTRAINT FK_DEVICE_SETTING_BKP_DEVICE FOREIGN KEY (DEVICE_ID) REFERENCES DEVICE.DEVICE (DEVICE_ID),
	CONSTRAINT FK_DEVICE_SETTING_BKP_PARAM_CD FOREIGN KEY (DEVICE_SETTING_PARAMETER_CD) REFERENCES DEVICE.DEVICE_SETTING_PARAMETER (DEVICE_SETTING_PARAMETER_CD)
) TABLESPACE DEVICE_DATA;

CREATE INDEX DEVICE.IDX_DEVICE_SETING_BKP_VALUE ON DEVICE.DEVICE_SETTING_BACKUP(DEVICE_SETTING_VALUE) TABLESPACE DEVICE_INDX ONLINE;
CREATE INDEX DEVICE.IDX_DEVICE_SET_BKP_PARAM_VALUE ON DEVICE.DEVICE_SETTING_BACKUP(DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_VALUE) TABLESPACE DEVICE_INDX ONLINE;

CREATE OR REPLACE TRIGGER DEVICE.TRBI_DEVICE_SETTING_BACKUP BEFORE INSERT ON DEVICE.DEVICE_SETTING_BACKUP
  FOR EACH ROW
BEGIN
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER DEVICE.TRBU_DEVICE_SETTING_BACKUP BEFORE UPDATE ON DEVICE.DEVICE_SETTING_BACKUP
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

GRANT SELECT, INSERT, UPDATE, DELETE ON DEVICE.DEVICE_SETTING_BACKUP TO USAT_DMS_ROLE;
GRANT SELECT ON DEVICE.DEVICE_SETTING_BACKUP TO USAT_DEV_READ_ONLY;
GRANT SELECT, INSERT, UPDATE, DELETE ON DEVICE.DEVICE_SETTING_BACKUP TO USATECH_UPD_TRANS;

CREATE OR REPLACE PUBLIC SYNONYM DEVICE_SETTING_BACKUP FOR DEVICE.DEVICE_SETTING_BACKUP;


CREATE TABLE DEVICE.CONFIG_TEMPLATE_TYPE
(
  CONFIG_TEMPLATE_TYPE_ID NUMBER(20, 0) NOT NULL,
  CONFIG_TEMPLATE_TYPE_NAME VARCHAR2(200) NOT NULL,  
  CREATED_BY	VARCHAR2(30) NOT NULL,
  CREATED_TS	DATE NOT NULL,
  LAST_UPDATED_BY	VARCHAR2(30) NOT NULL,
  LAST_UPDATED_TS	DATE NOT NULL, 
  CONSTRAINT PK_CONFIG_TEMPLATE_TYPE PRIMARY KEY(CONFIG_TEMPLATE_TYPE_ID)  
) TABLESPACE DEVICE_DATA;

CREATE OR REPLACE TRIGGER DEVICE.TRBI_CONFIG_TEMPLATE_TYPE BEFORE INSERT ON DEVICE.CONFIG_TEMPLATE_TYPE
  FOR EACH ROW
BEGIN
	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER DEVICE.TRBU_CONFIG_TEMPLATE_TYPE BEFORE UPDATE ON DEVICE.CONFIG_TEMPLATE_TYPE
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

GRANT SELECT, INSERT, UPDATE ON DEVICE.CONFIG_TEMPLATE_TYPE TO USAT_DMS_ROLE;
GRANT SELECT ON DEVICE.CONFIG_TEMPLATE_TYPE TO PSS, USAT_APP_LAYER_ROLE, USAT_DEV_READ_ONLY;
GRANT SELECT, INSERT, UPDATE, DELETE ON DEVICE.CONFIG_TEMPLATE_TYPE TO USATECH_UPD_TRANS;

INSERT INTO DEVICE.CONFIG_TEMPLATE_TYPE(CONFIG_TEMPLATE_TYPE_ID, CONFIG_TEMPLATE_TYPE_NAME) VALUES(1, 'Default');
INSERT INTO DEVICE.CONFIG_TEMPLATE_TYPE(CONFIG_TEMPLATE_TYPE_ID, CONFIG_TEMPLATE_TYPE_NAME) VALUES(2, 'Custom');
INSERT INTO DEVICE.CONFIG_TEMPLATE_TYPE(CONFIG_TEMPLATE_TYPE_ID, CONFIG_TEMPLATE_TYPE_NAME) VALUES(3, 'Metadata');
COMMIT;

CREATE OR REPLACE PUBLIC SYNONYM CONFIG_TEMPLATE_TYPE FOR DEVICE.CONFIG_TEMPLATE_TYPE;


CREATE SEQUENCE DEVICE.SEQ_CONFIG_TEMPLATE_ID INCREMENT BY 1 MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH 1 NOCACHE;
GRANT SELECT ON DEVICE.SEQ_CONFIG_TEMPLATE_ID TO USAT_DMS_ROLE;

CREATE TABLE DEVICE.CONFIG_TEMPLATE
(
  CONFIG_TEMPLATE_ID NUMBER(20, 0) NOT NULL,
  CONFIG_TEMPLATE_NAME VARCHAR2(200) NOT NULL,
  CONFIG_TEMPLATE_TYPE_ID NUMBER(20, 0) NOT NULL,
  DEVICE_TYPE_ID NUMBER(20, 0) NOT NULL,
  CREATED_BY	VARCHAR2(30) NOT NULL,
  CREATED_TS	DATE NOT NULL,
  LAST_UPDATED_BY	VARCHAR2(30) NOT NULL,
  LAST_UPDATED_TS	DATE NOT NULL, 
  PROPERTY_LIST_VERSION NUMBER(20, 0) DEFAULT 0 NOT NULL,
  CONSTRAINT PK_CONFIG_TEMPLATE PRIMARY KEY(CONFIG_TEMPLATE_ID),
  CONSTRAINT FK_CONFIG_TEMPLATE_CT_TYPE_ID FOREIGN KEY(CONFIG_TEMPLATE_TYPE_ID) REFERENCES DEVICE.CONFIG_TEMPLATE_TYPE(CONFIG_TEMPLATE_TYPE_ID),
  CONSTRAINT FK_CONFIG_TEMPLATE_DT_ID FOREIGN KEY(DEVICE_TYPE_ID) REFERENCES DEVICE.DEVICE_TYPE(DEVICE_TYPE_ID)
) TABLESPACE DEVICE_DATA;

CREATE UNIQUE INDEX DEVICE.UDX_CONFIG_TEMPLATE_CT_NAME ON DEVICE.CONFIG_TEMPLATE(CONFIG_TEMPLATE_NAME) TABLESPACE DEVICE_INDX;
CREATE UNIQUE INDEX DEVICE.UDX_CFG_TMPL_DEV_TYPE_TYPE_PLV ON DEVICE.CONFIG_TEMPLATE(DEVICE_TYPE_ID, CONFIG_TEMPLATE_TYPE_ID, DECODE(CONFIG_TEMPLATE_TYPE_ID, 2, CONFIG_TEMPLATE_ID, PROPERTY_LIST_VERSION)) TABLESPACE DEVICE_INDX;

CREATE OR REPLACE TRIGGER DEVICE.TRBI_CONFIG_TEMPLATE BEFORE INSERT ON DEVICE.CONFIG_TEMPLATE
  FOR EACH ROW
BEGIN
	IF :NEW.CONFIG_TEMPLATE_ID IS NULL THEN
		SELECT SEQ_CONFIG_TEMPLATE_ID.NEXTVAL
        INTO :NEW.CONFIG_TEMPLATE_ID
        FROM DUAL;
    END IF;

	SELECT 
		SYSDATE,
		USER,
		SYSDATE,
		USER
	INTO 
		:NEW.created_ts,
		:NEW.created_by,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER DEVICE.TRBU_CONFIG_TEMPLATE BEFORE UPDATE ON DEVICE.CONFIG_TEMPLATE
  FOR EACH ROW 
BEGIN
	SELECT
		:OLD.created_by,
		:OLD.created_ts,
		SYSDATE,
		USER
	INTO
		:NEW.created_by,
		:NEW.created_ts,
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
END;
/

GRANT SELECT, INSERT, UPDATE ON DEVICE.CONFIG_TEMPLATE TO USAT_DMS_ROLE;
GRANT SELECT ON DEVICE.CONFIG_TEMPLATE TO PSS, USAT_APP_LAYER_ROLE, USAT_DEV_READ_ONLY;
GRANT SELECT, INSERT, UPDATE, DELETE ON DEVICE.CONFIG_TEMPLATE TO USATECH_UPD_TRANS;

CREATE OR REPLACE PUBLIC SYNONYM CONFIG_TEMPLATE FOR DEVICE.CONFIG_TEMPLATE;

ALTER TABLE DEVICE.CONFIG_TEMPLATE_SETTING ADD (CONFIG_TEMPLATE_ID NUMBER(20, 0));
CREATE OR REPLACE PUBLIC SYNONYM CONFIG_TEMPLATE_SETTING FOR DEVICE.CONFIG_TEMPLATE_SETTING;

DECLARE
	CURSOR cur IS 
		select distinct ft.file_transfer_id, ft.file_transfer_type_cd, ft.file_transfer_name, 
			file_content_length(ft.rowid) file_content_length
		from device.config_template_setting cts
		join device.file_transfer ft on cts.file_transfer_id = ft.file_transfer_id
		where cts.config_template_id is null
			and not exists (select 1 from device.config_template where config_template_name = ft.file_transfer_name)
		order by ft.file_transfer_id;
		
	ln_config_template_type_id CONFIG_TEMPLATE.CONFIG_TEMPLATE_TYPE_ID%TYPE;
	ln_property_list_version CONFIG_TEMPLATE.PROPERTY_LIST_VERSION%TYPE;
	ln_device_type_id DEVICE_TYPE.DEVICE_TYPE_ID%TYPE;
BEGIN
	FOR rec_cur IN cur LOOP
		ln_property_list_version := 0;
		IF rec_cur.file_transfer_type_cd IN (1, 19) AND rec_cur.file_transfer_name = 'GX-GENERIC-MAP' THEN
			ln_config_template_type_id := 3;
			ln_device_type_id := 0;
		ELSIF rec_cur.file_transfer_type_cd IN (1, 19) AND rec_cur.file_transfer_name = 'MEI-GENERIC-MAP' THEN
			ln_config_template_type_id := 3;
			ln_device_type_id := 6;
		ELSIF rec_cur.file_transfer_type_cd IN (1, 19) AND rec_cur.file_transfer_name = 'CFG-METADATA-13' THEN
			ln_config_template_type_id := 3;
			ln_device_type_id := 13;
		ELSIF rec_cur.file_transfer_type_cd IN (1, 19) AND rec_cur.file_transfer_name = 'CFG-METADATA-11' THEN
			ln_config_template_type_id := 3;
			ln_device_type_id := 11;
		ELSIF rec_cur.file_transfer_type_cd = 6 THEN
			IF rec_cur.file_transfer_name = 'G4-DEFAULT-CFG' THEN
				ln_config_template_type_id := 1;
				ln_device_type_id := 0;
			ELSIF rec_cur.file_transfer_name = 'G5-DEFAULT-CFG' THEN
				ln_config_template_type_id := 1;
				ln_device_type_id := 1;
			ELSIF rec_cur.file_transfer_name = 'MEI-DEFAULT-CFG' THEN			
				ln_config_template_type_id := 1;
				ln_device_type_id := 6;
			ELSE
				ln_config_template_type_id := 2;
				IF rec_cur.file_content_length < 1024 THEN 
					ln_device_type_id := 6;
				ELSE
					ln_device_type_id := 0;
				END IF;
			END IF;
		ELSIF rec_cur.file_transfer_type_cd = 15 THEN
			IF rec_cur.file_transfer_name = 'KIOSK-DEFAULT-CFG' THEN
				ln_config_template_type_id := 1;
				ln_device_type_id := 11;
			ELSIF rec_cur.file_transfer_name = 'T2-DEFAULT-CFG' THEN
				ln_config_template_type_id := 1;
				ln_device_type_id := 12;
			ELSE
				ln_config_template_type_id := 2;
				ln_device_type_id := 11;
			END IF;
		ELSIF rec_cur.file_transfer_type_cd = 16 THEN
			ln_config_template_type_id := 2;
			ln_device_type_id := 11;
		ELSIF rec_cur.file_transfer_type_cd = 22 AND rec_cur.file_transfer_name LIKE 'DEFAULT-CFG-13-%' THEN
			ln_config_template_type_id := 1;
			ln_device_type_id := 13;
			ln_property_list_version := NVL(TO_NUMBER_OR_NULL(SUBSTR(rec_cur.file_transfer_name, INSTR(rec_cur.file_transfer_name, '-', 1, 3) + 1)), 0);
		ELSIF rec_cur.file_transfer_type_cd = 23 THEN
			ln_config_template_type_id := 2;
			ln_device_type_id := 13;
		ELSE
			ln_config_template_type_id := 2;
			ln_device_type_id := 13;
		END IF;		
		
		INSERT INTO DEVICE.CONFIG_TEMPLATE(CONFIG_TEMPLATE_NAME, CONFIG_TEMPLATE_TYPE_ID, DEVICE_TYPE_ID, PROPERTY_LIST_VERSION)
		SELECT rec_cur.file_transfer_name, ln_config_template_type_id, ln_device_type_id, ln_property_list_version FROM DUAL
		WHERE NOT EXISTS (SELECT 1 FROM DEVICE.CONFIG_TEMPLATE WHERE CONFIG_TEMPLATE_NAME = rec_cur.file_transfer_name);
		
		IF SQL%ROWCOUNT > 0 THEN
			UPDATE DEVICE.CONFIG_TEMPLATE_SETTING
			SET CONFIG_TEMPLATE_ID = (SELECT CONFIG_TEMPLATE_ID FROM DEVICE.CONFIG_TEMPLATE WHERE CONFIG_TEMPLATE_NAME = rec_cur.file_transfer_name)
			WHERE FILE_TRANSFER_ID = rec_cur.file_transfer_id;
			
			COMMIT;
		END IF;
	END LOOP;
END;
/

ALTER TABLE DEVICE.CONFIG_TEMPLATE_SETTING MODIFY (CONFIG_TEMPLATE_ID NUMBER(20, 0) NOT NULL);
ALTER TABLE DEVICE.CONFIG_TEMPLATE_SETTING ADD CONSTRAINT FK_CFG_TMPL_CONFIG_TEMPLATE_ID FOREIGN KEY(CONFIG_TEMPLATE_ID) REFERENCES DEVICE.CONFIG_TEMPLATE(CONFIG_TEMPLATE_ID);
ALTER TABLE DEVICE.CONFIG_TEMPLATE_SETTING DROP CONSTRAINT PK_CONFIG_TEMPLATE_SETTING;
DROP INDEX DEVICE.PK_CONFIG_TEMPLATE_SETTING;
ALTER TABLE DEVICE.CONFIG_TEMPLATE_SETTING ADD CONSTRAINT PK_CONFIG_TEMPLATE_SETTING PRIMARY KEY (CONFIG_TEMPLATE_ID, DEVICE_SETTING_PARAMETER_CD);
ALTER TABLE DEVICE.CONFIG_TEMPLATE_SETTING DROP (FILE_TRANSFER_ID);


DECLARE
	CURSOR cur IS 
		select ft.file_transfer_id, ft.file_transfer_name
		from device.file_transfer ft
		where ft.file_transfer_type_cd = 6 
			and not exists (
				select 1 from device.config_template where config_template_name = ft.file_transfer_name
			) and file_content_length(ft.rowid) = 1024
		order by ft.file_transfer_name;
	
	CURSOR cur_map IS
		select device_setting_parameter_cd, field_offset * 2 + 1 hex_field_offset, field_size * 2 hex_field_size
		from device.config_template_setting 
		where device_type_id = 0 and field_offset < 512 
		order by field_offset;
	
	ll_content file_transfer.file_transfer_content%TYPE;
	lv_content VARCHAR2(1024);	
	ln_config_template_id CONFIG_TEMPLATE.CONFIG_TEMPLATE_ID%TYPE;
	ln_exists NUMBER;
	
	PROCEDURE SP_UPSERT_CFG_TEMPLATE_SETTING(
		pn_config_template_id CONFIG_TEMPLATE_SETTING.CONFIG_TEMPLATE_ID%TYPE,
		pv_device_setting_parameter_cd CONFIG_TEMPLATE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_config_template_setting_val CONFIG_TEMPLATE_SETTING.CONFIG_TEMPLATE_SETTING_VALUE%TYPE,
		pn_exists OUT NUMBER
	) 
	IS
		ln_count NUMBER;
	BEGIN
		SELECT COUNT(1) INTO ln_count
		FROM device.device_setting_parameter
		WHERE device_setting_parameter_cd = pv_device_setting_parameter_cd;
			
		IF ln_count = 0 THEN
			BEGIN
				INSERT INTO device.device_setting_parameter(device_setting_parameter_cd)
				VALUES(pv_device_setting_parameter_cd);
			EXCEPTION
				WHEN DUP_VAL_ON_INDEX THEN
					NULL;
			END;	
		END IF;
		
		UPDATE device.config_template_setting
		SET config_template_setting_value = pv_config_template_setting_val
		WHERE config_template_id = pn_config_template_id
			AND device_setting_parameter_cd = pv_device_setting_parameter_cd;
		
		IF SQL%NOTFOUND THEN
			pn_exists := 0;
			BEGIN
				INSERT INTO device.config_template_setting(config_template_id, device_setting_parameter_cd, config_template_setting_value)
				VALUES(pn_config_template_id, pv_device_setting_parameter_cd, pv_config_template_setting_val);
			EXCEPTION
				WHEN DUP_VAL_ON_INDEX THEN
					SP_UPSERT_CFG_TEMPLATE_SETTING(pn_config_template_id, pv_device_setting_parameter_cd, pv_config_template_setting_val, pn_exists);
			END;
		ELSE
			pn_exists := 1;
		END IF;
	END;
BEGIN
	FOR rec_cur IN cur LOOP
		ll_content := NULL;
		
		BEGIN
			SELECT file_transfer_content
			INTO ll_content
			FROM device.file_transfer
			WHERE file_transfer_id = rec_cur.file_transfer_id;
		EXCEPTION
			WHEN VALUE_ERROR THEN
				NULL;
		END;
		
		IF ll_content IS NOT NULL THEN
			lv_content := UPPER(ll_content);
			
			INSERT INTO DEVICE.CONFIG_TEMPLATE(CONFIG_TEMPLATE_NAME, CONFIG_TEMPLATE_TYPE_ID, DEVICE_TYPE_ID)
			SELECT rec_cur.file_transfer_name, 2, 0 FROM DUAL
			WHERE NOT EXISTS (SELECT 1 FROM DEVICE.CONFIG_TEMPLATE WHERE CONFIG_TEMPLATE_NAME = rec_cur.file_transfer_name);
		
			IF SQL%ROWCOUNT > 0 THEN				
				SELECT CONFIG_TEMPLATE_ID 
				INTO ln_config_template_id
				FROM DEVICE.CONFIG_TEMPLATE 
				WHERE CONFIG_TEMPLATE_NAME = rec_cur.file_transfer_name;			
			
				FOR rec_cur_map IN cur_map LOOP
					SP_UPSERT_CFG_TEMPLATE_SETTING(ln_config_template_id, rec_cur_map.device_setting_parameter_cd, 
						REPLACE(SUBSTR(lv_content, rec_cur_map.hex_field_offset, rec_cur_map.hex_field_size), ' ', '0'), ln_exists);
				END LOOP;
				
				COMMIT;
			END IF;
		END IF;
	END LOOP;
END;
/

DECLARE
	CURSOR cur IS 
		select ft.file_transfer_id, ft.file_transfer_name
		from device.file_transfer ft
		where ft.file_transfer_type_cd = 6 
			and not exists (
				select 1 from device.config_template where config_template_name = ft.file_transfer_name
			) and file_content_length(ft.rowid) = 114
		order by ft.file_transfer_name;
	
	CURSOR cur_map IS
		select device_setting_parameter_cd, field_offset * 2 + 1 hex_field_offset, field_size * 2 hex_field_size
		from device.config_template_setting 
		where device_type_id = 6 and field_offset < 57 
		order by field_offset;
	
	ll_content file_transfer.file_transfer_content%TYPE;
	lv_content VARCHAR2(114);	
	ln_config_template_id CONFIG_TEMPLATE.CONFIG_TEMPLATE_ID%TYPE;
	ln_exists NUMBER;
	
	PROCEDURE SP_UPSERT_CFG_TEMPLATE_SETTING(
		pn_config_template_id CONFIG_TEMPLATE_SETTING.CONFIG_TEMPLATE_ID%TYPE,
		pv_device_setting_parameter_cd CONFIG_TEMPLATE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_config_template_setting_val CONFIG_TEMPLATE_SETTING.CONFIG_TEMPLATE_SETTING_VALUE%TYPE,
		pn_exists OUT NUMBER
	) 
	IS
		ln_count NUMBER;
	BEGIN
		SELECT COUNT(1) INTO ln_count
		FROM device.device_setting_parameter
		WHERE device_setting_parameter_cd = pv_device_setting_parameter_cd;
			
		IF ln_count = 0 THEN
			BEGIN
				INSERT INTO device.device_setting_parameter(device_setting_parameter_cd)
				VALUES(pv_device_setting_parameter_cd);
			EXCEPTION
				WHEN DUP_VAL_ON_INDEX THEN
					NULL;
			END;	
		END IF;
		
		UPDATE device.config_template_setting
		SET config_template_setting_value = pv_config_template_setting_val
		WHERE config_template_id = pn_config_template_id
			AND device_setting_parameter_cd = pv_device_setting_parameter_cd;
		
		IF SQL%NOTFOUND THEN
			pn_exists := 0;
			BEGIN
				INSERT INTO device.config_template_setting(config_template_id, device_setting_parameter_cd, config_template_setting_value)
				VALUES(pn_config_template_id, pv_device_setting_parameter_cd, pv_config_template_setting_val);
			EXCEPTION
				WHEN DUP_VAL_ON_INDEX THEN
					SP_UPSERT_CFG_TEMPLATE_SETTING(pn_config_template_id, pv_device_setting_parameter_cd, pv_config_template_setting_val, pn_exists);
			END;
		ELSE
			pn_exists := 1;
		END IF;
	END;
BEGIN
	FOR rec_cur IN cur LOOP
		ll_content := NULL;
		
		BEGIN
			SELECT file_transfer_content
			INTO ll_content
			FROM device.file_transfer
			WHERE file_transfer_id = rec_cur.file_transfer_id;
		EXCEPTION
			WHEN VALUE_ERROR THEN
				NULL;
		END;
		
		IF ll_content IS NOT NULL THEN
			lv_content := UPPER(ll_content);
			
			INSERT INTO DEVICE.CONFIG_TEMPLATE(CONFIG_TEMPLATE_NAME, CONFIG_TEMPLATE_TYPE_ID, DEVICE_TYPE_ID)
			SELECT rec_cur.file_transfer_name, 2, 6 FROM DUAL
			WHERE NOT EXISTS (SELECT 1 FROM DEVICE.CONFIG_TEMPLATE WHERE CONFIG_TEMPLATE_NAME = rec_cur.file_transfer_name);
		
			IF SQL%ROWCOUNT > 0 THEN			
				SELECT CONFIG_TEMPLATE_ID 
				INTO ln_config_template_id
				FROM DEVICE.CONFIG_TEMPLATE 
				WHERE CONFIG_TEMPLATE_NAME = rec_cur.file_transfer_name;			
			
				FOR rec_cur_map IN cur_map LOOP
					SP_UPSERT_CFG_TEMPLATE_SETTING(ln_config_template_id, rec_cur_map.device_setting_parameter_cd, 
						REPLACE(SUBSTR(lv_content, rec_cur_map.hex_field_offset, rec_cur_map.hex_field_size), ' ', '0'), ln_exists);
				END LOOP;
				
				COMMIT;
			END IF;
		END IF;
	END LOOP;
END;
/

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/DMS/FileTransferConversion.sql?rev=HEAD
delete from device.file_transfer where file_transfer_type_cd = 17;
commit;

ALTER TABLE DEVICE.FILE_TRANSFER_TYPE ADD (DELETABLE VARCHAR2(1));
UPDATE DEVICE.FILE_TRANSFER_TYPE SET DELETABLE = CASE WHEN FILE_TRANSFER_TYPE_CD IN (0, 2, 3, 11, 12, 19, 20, 21, 101) THEN 'Y' ELSE 'N' END;
COMMIT;
ALTER TABLE DEVICE.FILE_TRANSFER_TYPE MODIFY DELETABLE VARCHAR2(1) NOT NULL;

DROP FUNCTION DEVICE.FILE_CONTENT_LENGTH;
DROP FUNCTION DEVICE.FILE_CONTENT_SUBSTR;
DROP PROCEDURE DEVICE.SP_UPDATE_CONFIG;

DROP PUBLIC SYNONYM FILE_CONTENT_LENGTH;
DROP PUBLIC SYNONYM FILE_CONTENT_SUBSTR;
DROP PUBLIC SYNONYM SP_UPDATE_CONFIG;

ALTER TRIGGER DEVICE.TRBI_DEVICE_FILE_TRANSFER RENAME TO TRBI_DEV_FILE_XFER_OLDNOPART;
ALTER TRIGGER DEVICE.TRBU_DEVICE_FILE_TRANSFER RENAME TO TRBU_DEV_FILE_XFER_OLDNOPART;
ALTER TABLE DEVICE.DEVICE_FILE_TRANSFER DISABLE CONSTRAINT FK_DEVICE_FILE_TRANSFER_FILE_T;
ALTER TABLE DEVICE.DEVICE_FILE_TRANSFER DISABLE CONSTRAINT FK_MACHINE_FILE_TRANSFER_DEVIC;
ALTER TABLE DEVICE.DEVICE_FILE_TRANSFER RENAME CONSTRAINT PK_DEVICE_FILE_TRANSFER TO PK_DEVICE_FILE_TRANSFER_OLD;
ALTER INDEX DEVICE.PK_DEVICE_FILE_TRANSFER RENAME TO PK_DEVICE_FILE_TRANSFER_OLD;
ALTER INDEX DEVICE.IDX_DEVICE_FILE_TRANSFER_ID RENAME TO IDX_DEVICE_FILE_XFER_ID_OLD;
ALTER INDEX DEVICE.IDX_DEVICE_FILE_TRNSFR_DEVICE RENAME TO IDX_DEVICE_FILE_XFR_DEVICE_OLD;
ALTER TABLE DEVICE.DEVICE_FILE_TRANSFER RENAME TO DEVICE_FILE_TRANSFER_OLDNOPART;

ALTER TRIGGER DEVICE.TRBI_FILE_TRANSFER RENAME TO TRBI_FILE_TRANSFER_OLDNOPART;
ALTER TRIGGER DEVICE.TRBU_FILE_TRANSFER RENAME TO TRBU_FILE_TRANSFER_OLDNOPART;
ALTER TABLE DEVICE.FILE_TRANSFER RENAME CONSTRAINT PK_FILE_TRANSFER TO PK_FILE_TRANSFER_OLD;
ALTER TABLE DEVICE.FILE_TRANSFER RENAME CONSTRAINT FK_FILE_TRANSFER_FTT_CD TO FK_FILE_TRANSFER_FTT_CD_OLD;
ALTER INDEX DEVICE.PK_FILE_TRANSFER RENAME TO PK_FILE_TRANSFER_OLD;
ALTER INDEX DEVICE.IDX_FILE_TRANSFER_TYPE_CD RENAME TO IDX_FILE_TRANSFER_TYPE_CD_OLD;
ALTER INDEX DEVICE.INX_FILE_TRANSFER_TYPE_NAME RENAME TO INX_FILE_XFER_TYPE_NAME_OLD;
ALTER INDEX DEVICE.IDX_FILE_TRANSFER_NAME RENAME TO IDX_FILE_XFER_NAME_OLD;
ALTER INDEX DEVICE.IDX_FILE_TFR_CREATED_TS RENAME TO IDX_FILE_TFR_CREATED_TS_OLD;
ALTER TABLE DEVICE.FILE_TRANSFER RENAME TO FILE_TRANSFER_OLDNOPART;

CREATE TABLE DEVICE.FILE_TRANSFER_NEW
  (
    FILE_TRANSFER_ID NUMBER(20,0) NOT NULL,
    FILE_TRANSFER_NAME VARCHAR2(1000) NOT NULL,
    FILE_TRANSFER_TYPE_CD NUMBER(3,0) NOT NULL,
	FILE_TRANSFER_CLOB CLOB,
	FILE_TRANSFER_CONTENT BLOB NOT NULL,
    CREATED_BY VARCHAR2(30) NOT NULL,
    CREATED_TS DATE NOT NULL,
    LAST_UPDATED_BY VARCHAR2(30) NOT NULL,
    LAST_UPDATED_TS DATE NOT NULL,
    FILE_TRANSFER_COMMENT VARCHAR2(200),
	PARTITION_TS DATE NOT NULL,
	CONSTRAINT PK_FILE_TRANSFER PRIMARY KEY (FILE_TRANSFER_ID)
  )
  LOB (FILE_TRANSFER_CONTENT) STORE AS BASICFILE (	
	TABLESPACE ENGINE_DATA
	PCTVERSION 5
	DISABLE STORAGE IN ROW
	NOCACHE NOLOGGING
  )
  PARTITION BY RANGE (PARTITION_TS)
  (
	PARTITION FILE_TRANSFER_DO_NOT_DROP VALUES LESS THAN (TO_DATE('01/02/1970','MM/DD/YYYY')),
	PARTITION FILE_TRANSFER_201203 VALUES LESS THAN (TO_DATE('03/01/2012','MM/DD/YYYY')),
	PARTITION FILE_TRANSFER_201204 VALUES LESS THAN (TO_DATE('04/01/2012','MM/DD/YYYY')),
	PARTITION FILE_TRANSFER_201205 VALUES LESS THAN (TO_DATE('05/01/2012','MM/DD/YYYY')),
	PARTITION FILE_TRANSFER_201206 VALUES LESS THAN (TO_DATE('06/01/2012','MM/DD/YYYY')),
	PARTITION FILE_TRANSFER_201207 VALUES LESS THAN (TO_DATE('07/01/2012','MM/DD/YYYY')),
	PARTITION FILE_TRANSFER_201208 VALUES LESS THAN (TO_DATE('08/01/2012','MM/DD/YYYY')),
	PARTITION FILE_TRANSFER_201209 VALUES LESS THAN (TO_DATE('09/01/2012','MM/DD/YYYY')),
	PARTITION FILE_TRANSFER_201210 VALUES LESS THAN (TO_DATE('10/01/2012','MM/DD/YYYY')),
	PARTITION FILE_TRANSFER_201211 VALUES LESS THAN (TO_DATE('11/01/2012','MM/DD/YYYY')),
	PARTITION FILE_TRANSFER_201212 VALUES LESS THAN (TO_DATE('12/01/2012','MM/DD/YYYY')),
	PARTITION FILE_TRANSFER_201301 VALUES LESS THAN (TO_DATE('01/01/2013','MM/DD/YYYY'))
  )  
  TABLESPACE DEVICE_DATA
  PCTUSED    40
  PCTFREE    10
  INITRANS   1
  MAXTRANS   255
  STORAGE    (
              FREELISTS        8
              BUFFER_POOL      DEFAULT
             );

ALTER TABLE DEVICE.FILE_TRANSFER_NEW NOLOGGING;
ALTER TABLE DEVICE.FILE_TRANSFER_NEW ENABLE ROW MOVEMENT;

CREATE TABLE DEVICE.DEVICE_FILE_TRANSFER_NEW
  (
    DEVICE_FILE_TRANSFER_ID NUMBER(20,0) NOT NULL,
    FILE_TRANSFER_ID        NUMBER(20,0) NOT NULL,
    DEVICE_FILE_TRANSFER_TS DATE,
    DEVICE_FILE_TRANSFER_DIRECT VARCHAR2(1) NOT NULL,
    CREATED_BY                  VARCHAR2(30) NOT NULL,
    CREATED_TS DATE NOT NULL,
    DEVICE_ID       NUMBER(20,0) NOT NULL,
    LAST_UPDATED_BY VARCHAR2(30) NOT NULL,
    LAST_UPDATED_TS DATE NOT NULL,
    DEVICE_FILE_TRANSFER_STATUS_CD NUMBER(1,0) DEFAULT 0 NOT NULL,
    DEVICE_FILE_TRANSFER_GROUP_NUM NUMBER(3,0),
    DEVICE_FILE_TRANSFER_PKT_SIZE  NUMBER(8,0),
    DEVICE_SESSION_ID              NUMBER(20,0),
	PARTITION_TS DATE NOT NULL
  ) 
  PARTITION BY RANGE (PARTITION_TS)
  (
	PARTITION DEVICE_FILE_XFER_DO_NOT_DROP VALUES LESS THAN (TO_DATE('01/02/1970','MM/DD/YYYY')),
	PARTITION DEVICE_FILE_XFER_201203 VALUES LESS THAN (TO_DATE('03/01/2012','MM/DD/YYYY')),
	PARTITION DEVICE_FILE_XFER_201204 VALUES LESS THAN (TO_DATE('04/01/2012','MM/DD/YYYY')),
	PARTITION DEVICE_FILE_XFER_201205 VALUES LESS THAN (TO_DATE('05/01/2012','MM/DD/YYYY')),
	PARTITION DEVICE_FILE_XFER_201206 VALUES LESS THAN (TO_DATE('06/01/2012','MM/DD/YYYY')),
	PARTITION DEVICE_FILE_XFER_201207 VALUES LESS THAN (TO_DATE('07/01/2012','MM/DD/YYYY')),
	PARTITION DEVICE_FILE_XFER_201208 VALUES LESS THAN (TO_DATE('08/01/2012','MM/DD/YYYY')),
	PARTITION DEVICE_FILE_XFER_201209 VALUES LESS THAN (TO_DATE('09/01/2012','MM/DD/YYYY')),
	PARTITION DEVICE_FILE_XFER_201210 VALUES LESS THAN (TO_DATE('10/01/2012','MM/DD/YYYY')),
	PARTITION DEVICE_FILE_XFER_201211 VALUES LESS THAN (TO_DATE('11/01/2012','MM/DD/YYYY')),
	PARTITION DEVICE_FILE_XFER_201212 VALUES LESS THAN (TO_DATE('12/01/2012','MM/DD/YYYY')),
	PARTITION DEVICE_FILE_XFER_201301 VALUES LESS THAN (TO_DATE('01/01/2013','MM/DD/YYYY'))
  )  
  TABLESPACE DEVICE_DATA
  PCTUSED    40
  PCTFREE    10
  INITRANS   1
  MAXTRANS   255
  STORAGE    (
              FREELISTS        8
              BUFFER_POOL      DEFAULT
             );
  
ALTER TABLE DEVICE.DEVICE_FILE_TRANSFER_NEW ENABLE ROW MOVEMENT;

INSERT /*+ APPEND nologging */ INTO DEVICE.FILE_TRANSFER_NEW (
	FILE_TRANSFER_ID, FILE_TRANSFER_NAME, FILE_TRANSFER_TYPE_CD, FILE_TRANSFER_CLOB, FILE_TRANSFER_CONTENT,
	CREATED_BY, CREATED_TS, LAST_UPDATED_BY, LAST_UPDATED_TS, FILE_TRANSFER_COMMENT, PARTITION_TS
)
SELECT /*+ INDEX(FT IDX_FILE_TRANSFER_TYPE_CD) */ FT.FILE_TRANSFER_ID, SUBSTR(FT.FILE_TRANSFER_NAME, 1, 1000), FT.FILE_TRANSFER_TYPE_CD, TO_LOB(FT.FILE_TRANSFER_CONTENT), EMPTY_BLOB(),
	FT.CREATED_BY, FT.CREATED_TS, FT.LAST_UPDATED_BY, FT.LAST_UPDATED_TS, SUBSTR(FT.FILE_TRANSFER_COMMENT, 1, 200),
	DECODE(FTT.DELETABLE, 'N', EPOCH_DATE, FT.CREATED_TS)
FROM DEVICE.FILE_TRANSFER_OLDNOPART FT
JOIN DEVICE.FILE_TRANSFER_TYPE FTT ON FT.FILE_TRANSFER_TYPE_CD = FTT.FILE_TRANSFER_TYPE_CD
WHERE FT.FILE_TRANSFER_TYPE_CD IN (1, 4, 5, 6, 7, 8, 9, 10, 13, 14, 15, 16, 17, 18, 22, 23, 24, 25);
COMMIT;

INSERT /*+ APPEND nologging */ INTO DEVICE.FILE_TRANSFER_NEW (
	FILE_TRANSFER_ID, FILE_TRANSFER_NAME, FILE_TRANSFER_TYPE_CD, FILE_TRANSFER_CLOB, FILE_TRANSFER_CONTENT,
	CREATED_BY, CREATED_TS, LAST_UPDATED_BY, LAST_UPDATED_TS, FILE_TRANSFER_COMMENT, PARTITION_TS
)
SELECT /*+ INDEX(FT IDX_FILE_TFR_CREATED_TS) */ FT.FILE_TRANSFER_ID, SUBSTR(FT.FILE_TRANSFER_NAME, 1, 1000), FT.FILE_TRANSFER_TYPE_CD, TO_LOB(FT.FILE_TRANSFER_CONTENT), EMPTY_BLOB(),
	FT.CREATED_BY, FT.CREATED_TS, FT.LAST_UPDATED_BY, FT.LAST_UPDATED_TS, SUBSTR(FT.FILE_TRANSFER_COMMENT, 1, 200),
	DECODE(FTT.DELETABLE, 'N', EPOCH_DATE, FT.CREATED_TS)
FROM DEVICE.FILE_TRANSFER_OLDNOPART FT
JOIN DEVICE.FILE_TRANSFER_TYPE FTT ON FT.FILE_TRANSFER_TYPE_CD = FTT.FILE_TRANSFER_TYPE_CD
WHERE FT.CREATED_TS > SYSDATE - (SELECT DECODE(REPLACE(GLOBAL_NAME, '.WORLD', ''), 'USADBP', 1, 180) FROM GLOBAL_NAME)
	AND FT.FILE_TRANSFER_TYPE_CD IN (0, 2, 3, 11, 12, 19, 20, 21)
	AND NOT EXISTS (SELECT 1 FROM DEVICE.FILE_TRANSFER_NEW WHERE FILE_TRANSFER_ID = FT.FILE_TRANSFER_ID);
COMMIT;

INSERT /*+ APPEND nologging */ INTO DEVICE.FILE_TRANSFER_NEW (
	FILE_TRANSFER_ID, FILE_TRANSFER_NAME, FILE_TRANSFER_TYPE_CD, FILE_TRANSFER_CLOB, FILE_TRANSFER_CONTENT,
	CREATED_BY, CREATED_TS, LAST_UPDATED_BY, LAST_UPDATED_TS, FILE_TRANSFER_COMMENT, PARTITION_TS
)
SELECT /*+ INDEX(FT PK_FILE_TRANSFER) INDEX(DFT PK_DEVICE_FILE_TRANSFER) */ FT.FILE_TRANSFER_ID, SUBSTR(FT.FILE_TRANSFER_NAME, 1, 1000), FT.FILE_TRANSFER_TYPE_CD, TO_LOB(FT.FILE_TRANSFER_CONTENT), EMPTY_BLOB(),
	FT.CREATED_BY, FT.CREATED_TS, FT.LAST_UPDATED_BY, FT.LAST_UPDATED_TS, SUBSTR(FT.FILE_TRANSFER_COMMENT, 1, 200),
	DECODE(FTT.DELETABLE, 'N', EPOCH_DATE, FT.CREATED_TS)
FROM DEVICE.FILE_TRANSFER_OLDNOPART FT
JOIN DEVICE.FILE_TRANSFER_TYPE FTT ON FT.FILE_TRANSFER_TYPE_CD = FTT.FILE_TRANSFER_TYPE_CD
JOIN DEVICE.DEVICE_FILE_TRANSFER_OLDNOPART DFT ON FT.FILE_TRANSFER_ID = DFT.FILE_TRANSFER_ID
JOIN ENGINE.MACHINE_CMD_PENDING MCP ON MCP.DATA_TYPE IN('7C', 'A4', 'C8', 'C7') AND REGEXP_LIKE(MCP.COMMAND, '^[0-9]+$') 
  AND TO_NUMBER(MCP.COMMAND) = DFT.DEVICE_FILE_TRANSFER_ID	
WHERE NOT EXISTS (SELECT 1 FROM DEVICE.FILE_TRANSFER_NEW WHERE FILE_TRANSFER_ID = FT.FILE_TRANSFER_ID);
COMMIT;
	
DECLARE
	CURSOR cur IS 
		SELECT FILE_TRANSFER_ID
		FROM DEVICE.FILE_TRANSFER_NEW;
		
	lb_file_transfer_content BLOB;
	lc_file_transfer_clob CLOB;
    ln_file_content_hex_length NUMBER;
    ln_position NUMBER;
    ln_max_size NUMBER := 32766; -- Oracle limitation
    ln_length NUMBER;
BEGIN
	FOR rec_cur IN cur LOOP		
		BEGIN		
			ln_position := 1;
			
			SELECT FILE_TRANSFER_CONTENT, FILE_TRANSFER_CLOB, dbms_lob.getlength(FILE_TRANSFER_CLOB)
			INTO lb_file_transfer_content, lc_file_transfer_clob, ln_file_content_hex_length
			FROM DEVICE.FILE_TRANSFER_NEW
			WHERE FILE_TRANSFER_ID = rec_cur.FILE_TRANSFER_ID
				AND FILE_TRANSFER_CLOB IS NOT NULL
			FOR UPDATE;

			WHILE ln_position <= ln_file_content_hex_length LOOP
				ln_length := LEAST(ln_max_size, ln_file_content_hex_length - ln_position + 1);
				dbms_lob.writeappend(lb_file_transfer_content, ln_length / 2, REPLACE(dbms_lob.substr(lc_file_transfer_clob, ln_length, ln_position), ' ', '0'));
				ln_position := ln_position + ln_length;
			END LOOP;
	
			COMMIT;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				ROLLBACK;
			WHEN VALUE_ERROR THEN
				ROLLBACK;
		END;
	END LOOP;
END;
/

ALTER TABLE DEVICE.FILE_TRANSFER_NEW DROP (FILE_TRANSFER_CLOB);
ALTER TABLE DEVICE.FILE_TRANSFER_NEW MODIFY LOB (FILE_TRANSFER_CONTENT) (NOCACHE LOGGING);
ALTER TABLE DEVICE.FILE_TRANSFER_NEW LOGGING;
  
CREATE OR REPLACE TRIGGER DEVICE.TRBI_FILE_TRANSFER BEFORE
INSERT ON DEVICE.FILE_TRANSFER_NEW
FOR EACH ROW
BEGIN 
	IF :new.file_transfer_id IS NULL THEN
		SELECT seq_file_transfer_id.nextval 
		INTO :new.file_transfer_id FROM dual;
	END IF;
	
	SELECT 
		sysdate,
		USER,
		sysdate,
		USER
	INTO 
		:new.created_ts,
		:new.created_by,
		:new.last_updated_ts,
		:new.last_updated_by
	FROM dual;
	
	IF :NEW.file_transfer_content IS NULL THEN
		:NEW.file_transfer_content := EMPTY_BLOB();
	END IF;
	
	SELECT DECODE(DELETABLE, 'N', EPOCH_DATE, :NEW.CREATED_TS)
	INTO :NEW.PARTITION_TS
	FROM DEVICE.FILE_TRANSFER_TYPE
	WHERE FILE_TRANSFER_TYPE_CD = :NEW.FILE_TRANSFER_TYPE_CD;
END;
/

CREATE OR REPLACE TRIGGER DEVICE.TRBU_FILE_TRANSFER BEFORE
UPDATE ON DEVICE.FILE_TRANSFER_NEW 
FOR EACH ROW
BEGIN
  SELECT 
	:old.created_by,
    :old.created_ts,
    sysdate,
    USER
  INTO 
	:new.created_by,
    :new.created_ts,
    :new.last_updated_ts,
    :new.last_updated_by
  FROM dual;
  
  IF :NEW.file_transfer_content IS NULL THEN
	:NEW.file_transfer_content := EMPTY_BLOB();
  END IF;
  
  IF :OLD.FILE_TRANSFER_TYPE_CD != :NEW.FILE_TRANSFER_TYPE_CD THEN
	SELECT DECODE(DELETABLE, 'N', EPOCH_DATE, :NEW.CREATED_TS)
	INTO :NEW.PARTITION_TS
	FROM DEVICE.FILE_TRANSFER_TYPE
	WHERE FILE_TRANSFER_TYPE_CD = :NEW.FILE_TRANSFER_TYPE_CD;
  END IF;
END;
/

ALTER TABLE DEVICE.FILE_TRANSFER_NEW ADD CONSTRAINT FK_FILE_TRANSFER_FTT_CD FOREIGN KEY (FILE_TRANSFER_TYPE_CD) REFERENCES DEVICE.FILE_TRANSFER_TYPE (FILE_TRANSFER_TYPE_CD);
CREATE INDEX DEVICE.INX_FILE_TRANSFER_TYPE_NAME ON DEVICE.FILE_TRANSFER_NEW(FILE_TRANSFER_TYPE_CD, FILE_TRANSFER_NAME) LOCAL TABLESPACE DEVICE_INDX ONLINE;
CREATE INDEX DEVICE.IDX_FILE_TRANSFER_NAME ON DEVICE.FILE_TRANSFER_NEW(FILE_TRANSFER_NAME) LOCAL TABLESPACE DEVICE_INDX ONLINE;
CREATE INDEX DEVICE.IDX_FILE_TFR_CREATED_TS ON DEVICE.FILE_TRANSFER_NEW(CREATED_TS) LOCAL TABLESPACE DEVICE_INDX ONLINE;

ALTER TABLE DEVICE.DEVICE_FILE_TRANSFER_NEW NOLOGGING;

INSERT INTO DEVICE.DEVICE_FILE_TRANSFER_NEW (
    DEVICE_FILE_TRANSFER_ID,
    FILE_TRANSFER_ID,
    DEVICE_FILE_TRANSFER_TS,
    DEVICE_FILE_TRANSFER_DIRECT,
    CREATED_BY,
    CREATED_TS,
    DEVICE_ID,
    LAST_UPDATED_BY,
    LAST_UPDATED_TS,
    DEVICE_FILE_TRANSFER_STATUS_CD,
    DEVICE_FILE_TRANSFER_GROUP_NUM,
    DEVICE_FILE_TRANSFER_PKT_SIZE,
    DEVICE_SESSION_ID,
	PARTITION_TS
)
SELECT
	DFT.DEVICE_FILE_TRANSFER_ID,
    DFT.FILE_TRANSFER_ID,
    DFT.DEVICE_FILE_TRANSFER_TS,
    DFT.DEVICE_FILE_TRANSFER_DIRECT,
    DFT.CREATED_BY,
    DFT.CREATED_TS,
    DFT.DEVICE_ID,
    DFT.LAST_UPDATED_BY,
    DFT.LAST_UPDATED_TS,
    DFT.DEVICE_FILE_TRANSFER_STATUS_CD,
    DFT.DEVICE_FILE_TRANSFER_GROUP_NUM,
    DFT.DEVICE_FILE_TRANSFER_PKT_SIZE,
    DFT.DEVICE_SESSION_ID,
	DECODE(DFT.device_file_transfer_status_cd, 
		0, DECODE(DFT.DEVICE_FILE_TRANSFER_DIRECT, 'I', DECODE(FTT.DELETABLE, 'N', DFT.CREATED_TS, FT.PARTITION_TS), EPOCH_DATE),
		DECODE(FTT.DELETABLE, 'N', NVL(DFT.DEVICE_FILE_TRANSFER_TS, DFT.CREATED_TS), FT.PARTITION_TS))
FROM DEVICE.DEVICE_FILE_TRANSFER_OLDNOPART DFT
JOIN DEVICE.FILE_TRANSFER_NEW FT ON DFT.FILE_TRANSFER_ID = FT.FILE_TRANSFER_ID
JOIN DEVICE.FILE_TRANSFER_TYPE FTT ON FT.FILE_TRANSFER_TYPE_CD = FTT.FILE_TRANSFER_TYPE_CD;
COMMIT;

ALTER TABLE DEVICE.DEVICE_FILE_TRANSFER_NEW LOGGING;

ALTER TABLE DEVICE.DEVICE_FILE_TRANSFER_NEW ADD CONSTRAINT PK_DEVICE_FILE_TRANSFER PRIMARY KEY (DEVICE_FILE_TRANSFER_ID) USING INDEX TABLESPACE DEVICE_DATA ONLINE;
ALTER TABLE DEVICE.DEVICE_FILE_TRANSFER_NEW ADD CONSTRAINT FK_DEVICE_FILE_TRANSFER_DEV_ID FOREIGN KEY (DEVICE_ID) REFERENCES DEVICE.DEVICE (DEVICE_ID);
CREATE INDEX DEVICE.IDX_DEVICE_FILE_TRANSFER_ID ON DEVICE.DEVICE_FILE_TRANSFER_NEW(FILE_TRANSFER_ID) LOCAL TABLESPACE DEVICE_INDX ONLINE;
CREATE INDEX DEVICE.IDX_DEVICE_FILE_TRNSFR_DEVICE ON DEVICE.DEVICE_FILE_TRANSFER_NEW(DEVICE_ID) LOCAL TABLESPACE DEVICE_INDX ONLINE;

ALTER TABLE DEVICE.FILE_TRANSFER_NEW RENAME TO FILE_TRANSFER;

CREATE OR REPLACE TRIGGER DEVICE.TRBI_DEVICE_FILE_TRANSFER
BEFORE INSERT
ON DEVICE.DEVICE_FILE_TRANSFER_NEW
FOR EACH ROW
BEGIN
   IF :NEW.device_file_transfer_id IS NULL THEN
      SELECT seq_device_file_transfer_id.NEXTVAL
        INTO :NEW.device_file_transfer_id
        FROM DUAL;
   END IF;
   
   SELECT SYSDATE,
          USER,
          SYSDATE,
          USER
     INTO :NEW.created_ts,
          :NEW.created_by,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;
	 
	IF :NEW.device_file_transfer_status_cd = 1 AND :NEW.device_file_transfer_ts IS NULL THEN
		:NEW.device_file_transfer_ts := SYSDATE;
    END IF;
	 
	IF :NEW.device_file_transfer_status_cd = 0 AND :NEW.device_file_transfer_direct = 'I' OR :NEW.device_file_transfer_status_cd = 1 THEN
		SELECT DECODE(PARTITION_TS, EPOCH_DATE, NVL(:NEW.DEVICE_FILE_TRANSFER_TS, :NEW.CREATED_TS), PARTITION_TS)
		INTO :NEW.PARTITION_TS
		FROM DEVICE.FILE_TRANSFER
		WHERE FILE_TRANSFER_ID = :NEW.FILE_TRANSFER_ID;
	ELSE
		:NEW.PARTITION_TS := EPOCH_DATE;
	END IF;
END;
/

CREATE OR REPLACE TRIGGER DEVICE.TRBU_DEVICE_FILE_TRANSFER
BEFORE UPDATE
ON DEVICE.DEVICE_FILE_TRANSFER_NEW
FOR EACH ROW
BEGIN
   SELECT :OLD.created_by,
          :OLD.created_ts,
          SYSDATE,
          USER
     INTO :NEW.created_by,
          :NEW.created_ts,
          :NEW.last_updated_ts,
          :NEW.last_updated_by
     FROM DUAL;

	IF :NEW.device_file_transfer_status_cd = 1 AND :OLD.device_file_transfer_status_cd = 0 THEN
		IF :NEW.device_file_transfer_ts IS NULL THEN
			:NEW.device_file_transfer_ts := SYSDATE;
		END IF;
		
		SELECT DECODE(PARTITION_TS, EPOCH_DATE, :NEW.DEVICE_FILE_TRANSFER_TS, PARTITION_TS)
		INTO :NEW.PARTITION_TS
		FROM DEVICE.FILE_TRANSFER
		WHERE FILE_TRANSFER_ID = :NEW.FILE_TRANSFER_ID;
	END IF;
END;
/

ALTER TABLE DEVICE.DEVICE_FILE_TRANSFER_NEW ADD CONSTRAINT FK_DEVICE_FILE_TRANSFER_FT_ID FOREIGN KEY (FILE_TRANSFER_ID) REFERENCES DEVICE.FILE_TRANSFER (FILE_TRANSFER_ID);
ALTER TABLE DEVICE.DEVICE_FILE_TRANSFER_NEW RENAME TO DEVICE_FILE_TRANSFER;

GRANT SELECT, INSERT, UPDATE, DELETE ON DEVICE.DEVICE_FILE_TRANSFER TO USAT_APP_LAYER_ROLE, USAT_DMS_ROLE, USATECH_UPD_TRANS;
GRANT SELECT ON DEVICE.DEVICE_FILE_TRANSFER TO USAT_DEV_READ_ONLY, WEB_USER;
GRANT SELECT, INSERT, UPDATE, DELETE ON DEVICE.FILE_TRANSFER TO USAT_APP_LAYER_ROLE, USAT_DMS_ROLE, USATECH_UPD_TRANS;
GRANT SELECT ON DEVICE.FILE_TRANSFER TO USAT_DEV_READ_ONLY, WEB_USER;
-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/DEVICE/SP_UPDATE_CONFIG_AND_RETURN.prc?rev=1.4
CREATE OR REPLACE PROCEDURE DEVICE.SP_UPDATE_CONFIG_AND_RETURN
(
    p_device_id         IN NUMBER,
    p_offset            IN INT,
    p_new_data          IN VARCHAR2,
    p_data_type         IN CHAR,
    p_device_type_id    IN NUMBER,
    p_debug             IN INT,
    p_return_code       OUT NUMBER,
    p_return_msg        OUT VARCHAR2,
    pn_command_id       OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
    pv_data_type        OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
    pl_command          OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE    
) IS
    l_device_name       DEVICE.DEVICE_NAME%TYPE;
	l_orig_data			DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
	l_orig_data_hex		DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
    l_replace_with_length INT;
    l_replace_with      DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
    l_poke_location     NUMBER(5,1);
    l_poke_length       INT;
    l_poke_command      ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE;
    l_pending_count     INT;
    l_execute_order     INT;
    
BEGIN
    BEGIN
        SELECT d.device_name, ds.device_setting_value
        INTO l_device_name, l_orig_data_hex
        FROM device.device d
		JOIN device.device_setting ds
		ON d.device_id = ds.device_id
			AND ds.device_setting_parameter_cd = TO_CHAR(p_offset)
        WHERE d.device_id = p_device_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            p_return_code := -1;
            p_return_msg := 'Device or device setting not found for device_id: ' || p_device_id || ', offset: ' || p_offset;
            RETURN;
        WHEN OTHERS THEN
            RAISE;
    END;
    
    p_return_msg := p_device_id || ',' || l_device_name || ',' || p_offset || ': ';

    -- expect hex data will be passed in hex encoded, ascii and choice data will not
    IF p_data_type = 'H' THEN
        l_replace_with := p_new_data;
		p_return_msg := p_return_msg || UPPER(l_orig_data_hex) || ' = ' || UPPER(l_replace_with);
    ELSE
        l_replace_with := pkg_conversions.string_to_hex(p_new_data);
		BEGIN
			l_orig_data := pkg_conversions.hex_to_string(l_orig_data_hex);
		EXCEPTION
			WHEN OTHERS THEN
				NULL;
		END;
		p_return_msg := p_return_msg || l_orig_data || ' = ' || p_new_data;
    END IF;
	
	l_replace_with_length := LENGTH(l_replace_with);

    -- compare the hex values to see if there was a change
    IF UPPER(l_orig_data_hex) = UPPER(l_replace_with) THEN
        p_return_code := 0;
        p_return_msg :=  p_return_msg || ': TRUE - NO CHANGE!';
        RETURN;
    END IF;

    p_return_msg :=  p_return_msg || ': FALSE - CHANGED!';
    p_return_code := 1;

    -- now we need to create a pending Poke for this change
    l_poke_length := l_replace_with_length/2;

    IF p_device_type_id IN (0,1) THEN
        -- G4/G5 devices addresses memory in words
        l_poke_location := trunc((p_offset/2));
        -- must send at least 2 bytes at a time to a G4/G5
        IF l_poke_length = 1 THEN
            l_poke_length := 2;
        END IF;
    ELSIF p_device_type_id IN (6) THEN
        -- MEI device should always Poke the entire config
        l_poke_location := 0;
        l_poke_length := 57;
    ELSE
        l_poke_location := p_offset;
    END IF;
    
    l_poke_command := '42' || lpad(pkg_conversions.to_hex(l_poke_location), 8, '0') || lpad(pkg_conversions.to_hex(l_poke_length), 8, '0');

    -- check if the Poke already exists; no need to send 2 pokes for the same memory segment
    SELECT count(1)
    INTO l_pending_count
    FROM engine.machine_cmd_pending
    WHERE machine_id = l_device_name
    AND data_type = '88'
    AND command = l_poke_command
    AND execute_cd IN ('P', 'S');

    -- query for the max pending command execute_order
    -- we want to put the Poke at the end of the pending list
    SELECT nvl(max(execute_order)+1, 0)
    INTO l_execute_order
    FROM engine.machine_cmd_pending
    WHERE machine_id = l_device_name
    AND execute_cd IN ('P', 'S');

    p_return_msg := p_return_msg || ' - POKE: ' || l_device_name || ' 88 ' || l_poke_command || ' P ' || l_execute_order;

    -- if not debugging then update the file contents and save the poke
    IF l_pending_count = 0 THEN
        p_return_msg := p_return_msg || ', poke does not exist';
        IF p_debug = 0 THEN
            INSERT INTO engine.machine_cmd_pending(machine_id, data_type, command, execute_cd, execute_order)
            VALUES (l_device_name, '88', l_poke_command, 'P', l_execute_order)
            RETURNING MACHINE_COMMAND_PENDING_ID, DATA_TYPE, COMMAND INTO pn_command_id, pv_data_type, pl_command;
            p_return_msg := p_return_msg || ', poke inserted';
        ELSE
            p_return_msg := p_return_msg || ', poke not inserted (debug on)';
        END IF;
    ELSE
        p_return_msg := p_return_msg || ', poke not inserted (already exists)';
    END IF;

    IF p_debug = 0 THEN
		UPDATE device.device_setting
		SET device_setting_value = l_replace_with
		WHERE device_id = p_device_id
			AND device_setting_parameter_cd = TO_CHAR(p_offset);
		p_return_msg := p_return_msg || ', device setting updated';
    ELSE
        p_return_msg := p_return_msg || ', device setting not updated (debug on)';
    END IF;
END;
/
-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/DEVICE/PKG_DEVICE_CONFIGURATION.psk?rev=1.35
CREATE OR REPLACE PACKAGE DEVICE.PKG_DEVICE_CONFIGURATION IS    

    PROCEDURE SP_GET_FILE_TRANSFER_BLOB
    (
        pn_file_transfer_id file_transfer.file_transfer_id%TYPE,
        pbl_file_transfer_content OUT file_transfer.file_transfer_content%TYPE
    );
	
	PROCEDURE SP_GET_MAP_CONFIG_FILE
	(
		pn_device_id device.device_id%TYPE,
		pv_file_content_hex OUT VARCHAR2
	);
	
	PROCEDURE SP_UPSERT_KIOSK_CONFIG_UPDATE
	(
		pv_device_name DEVICE.DEVICE_NAME%TYPE,
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pv_kiosk_config_name ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE
	);
	
	FUNCTION GET_DEVICE_SETTING(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pv_device_setting_parameter_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE
	) RETURN DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
	
    PROCEDURE SP_UPDATE_DEVICE_SETTING(
        pn_device_id IN device.device_id%TYPE,
        pv_device_setting_parameter_cd IN device_setting.device_setting_parameter_cd%TYPE,
		pv_device_setting_value IN device_setting.device_setting_value%TYPE
    );
	
	PROCEDURE SP_UPSERT_DEVICE_SETTING(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pv_device_setting_parameter_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_device_setting_value DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pn_exists OUT NUMBER
	);
	
	PROCEDURE SP_UPSERT_DEVICE_SETTING(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pv_device_setting_parameter_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_device_setting_value DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pn_file_order DEVICE_SETTING.FILE_ORDER%TYPE,
		pn_exists OUT NUMBER
	);
	
	PROCEDURE SP_UPSERT_CFG_TEMPLATE_SETTING(
		pn_config_template_id CONFIG_TEMPLATE_SETTING.CONFIG_TEMPLATE_ID%TYPE,
		pv_device_setting_parameter_cd CONFIG_TEMPLATE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_config_template_setting_val CONFIG_TEMPLATE_SETTING.CONFIG_TEMPLATE_SETTING_VALUE%TYPE,
		pn_exists OUT NUMBER
	);
    
	-- IS_DEVICE_SETTING_IN_CONFIG is deprecated in R30
    FUNCTION IS_DEVICE_SETTING_IN_CONFIG(
        pn_device_id IN device.device_id%TYPE,
        pv_device_setting_parameter_cd IN device_setting.device_setting_parameter_cd%TYPE,
        pv_device_setting_value IN device_setting.device_setting_value%TYPE
    )
    RETURN VARCHAR;

    PROCEDURE SP_UPDATE_DEVICE_SETTINGS(
        pn_device_id IN device.device_id%TYPE,
        pn_file_transfer_id IN file_transfer.file_transfer_id%TYPE,
        pn_result_cd OUT NUMBER,
        pv_error_message OUT VARCHAR2,
        pn_setting_count OUT NUMBER
    );

    PROCEDURE SP_UPDATE_CFG_TMPL_SETTINGS(
        pn_file_transfer_id IN file_transfer.file_transfer_id%TYPE,
        pn_result_cd OUT NUMBER,
        pv_error_message OUT VARCHAR2,    
        pn_setting_count OUT NUMBER
    );

    PROCEDURE SP_GET_HOST_BY_PORT_NUM(
        pv_device_name IN device.device_name%TYPE,
        pn_host_port_num IN host.host_port_num%TYPE,
        pt_utc_ts IN TIMESTAMP,
        pn_host_id OUT host.host_id%TYPE    
    );

    PROCEDURE SP_GET_HOST(
        pn_device_id IN device.device_id%TYPE,
        pv_device_name IN device.device_name%TYPE,
        pn_host_port_num IN host.host_port_num%TYPE,
        pt_utc_ts IN TIMESTAMP,
        pn_result_cd OUT NUMBER,
        pv_error_message OUT VARCHAR2,
        pn_host_id OUT host.host_id%TYPE    
    );

    PROCEDURE SP_CREATE_DEFAULT_HOSTS(
        pn_device_id        IN       device.device_id%TYPE,
        pn_new_host_count   OUT      NUMBER,
        pn_result_cd        OUT      NUMBER,
        pv_error_message    OUT      VARCHAR2
    );

    FUNCTION GET_OR_CREATE_HOST_EQUIPMENT(
        pv_host_equipment_mfgr IN HOST_EQUIPMENT.HOST_EQUIPMENT_MFGR%TYPE,
    	pv_host_equipment_model IN HOST_EQUIPMENT.HOST_EQUIPMENT_MODEL%TYPE,
        pn_existing_cnt OUT PLS_INTEGER)
        RETURN HOST_EQUIPMENT.HOST_EQUIPMENT_ID%TYPE;
    
    PROCEDURE UPSERT_HOST(
        pn_device_id IN DEVICE.DEVICE_ID%TYPE,
        pn_host_port_num IN HOST.HOST_PORT_NUM%TYPE,
        pn_host_position_num IN HOST.HOST_POSITION_NUM%TYPE,
        pn_host_type_id IN HOST.HOST_TYPE_ID%TYPE,
        pv_host_serial_cd IN HOST.HOST_SERIAL_CD%TYPE,
    	pv_host_label_cd IN HOST.HOST_LABEL_CD%TYPE,
    	pn_host_equipment_id HOST_EQUIPMENT.HOST_EQUIPMENT_ID%TYPE,
        pn_host_est_complete_minut IN HOST.HOST_EST_COMPLETE_MINUT%TYPE,
        pn_host_id OUT HOST.HOST_ID%TYPE,
        pn_existing_cnt OUT PLS_INTEGER);
        
    PROCEDURE UPSERT_HOST(
        pn_device_id IN DEVICE.DEVICE_ID%TYPE,
        pn_host_port_num IN HOST.HOST_PORT_NUM%TYPE,
        pn_host_position_num IN HOST.HOST_POSITION_NUM%TYPE,
        pn_host_type_id IN HOST.HOST_TYPE_ID%TYPE,
        pv_host_serial_cd IN HOST.HOST_SERIAL_CD%TYPE,
    	pv_host_label_cd IN HOST.HOST_LABEL_CD%TYPE,
    	pv_host_equipment_mfgr IN HOST_EQUIPMENT.HOST_EQUIPMENT_MFGR%TYPE,
    	pv_host_equipment_model IN HOST_EQUIPMENT.HOST_EQUIPMENT_MODEL%TYPE,
        pn_host_est_complete_minut IN HOST.HOST_EST_COMPLETE_MINUT%TYPE,
        pn_host_id OUT HOST.HOST_ID%TYPE,
        pn_existing_cnt OUT PLS_INTEGER);
    
    PROCEDURE UPSERT_HOST_SETTING(
		pn_host_id HOST.HOST_ID%TYPE,
		pv_host_setting_parameter_cd HOST_SETTING.HOST_SETTING_PARAMETER%TYPE,
		pv_host_setting_value HOST_SETTING.HOST_SETTING_VALUE%TYPE);
        
    PROCEDURE UPSERT_HOST_SETTING(
		pn_host_id HOST.HOST_ID%TYPE,
		pv_host_setting_parameter_cd HOST_SETTING.HOST_SETTING_PARAMETER%TYPE,
		pv_host_setting_value HOST_SETTING.HOST_SETTING_VALUE%TYPE,
        pv_old_host_setting_value OUT HOST_SETTING.HOST_SETTING_VALUE%TYPE);
    
    PROCEDURE UPDATE_COMM_STATS(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pn_rssi NUMBER,
		pn_ber NUMBER,
        pd_update_ts GPRS_DEVICE.RSSI_TS%TYPE,
        pv_modem_info GPRS_DEVICE.MODEM_INFO%TYPE);
        
	-- SP_CONSTRUCT_PROPERTIES_FILE is deprecated in R30
    PROCEDURE SP_CONSTRUCT_PROPERTIES_FILE(
        pn_device_id IN device.device_id%TYPE,
        pl_file OUT CLOB);
        
	-- SP_UPDATE_DEVICE_CONFIG_FILE is deprecated in R30
    PROCEDURE SP_UPDATE_DEVICE_CONFIG_FILE(
        pn_device_id DEVICE.DEVICE_ID%TYPE,
        pl_config_file LONG);
    
    PROCEDURE SP_INITIALIZE_CONFIG_FILE(
        pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
        pn_new_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
        pn_new_property_list_version NUMBER,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pn_updated OUT NUMBER,
        pn_old_property_list_version OUT NUMBER);
            
    PROCEDURE SP_INITIALIZE_CONFIG_FILE(
        pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
        pn_new_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
        pn_new_property_list_version NUMBER,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pn_updated OUT NUMBER);
        
    PROCEDURE ADD_PENDING_FILE_TRANSFER(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_id FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pc_data_type ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pn_file_transfer_group DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
        pn_file_transfer_packet_size DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
        pn_execute_order ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE);

    PROCEDURE ADD_PENDING_FILE_TRANSFER(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_id FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pc_data_type ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pn_file_transfer_group DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
        pn_file_transfer_packet_size DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
        pn_execute_order ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pl_command OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE);
        
	-- R30+
    PROCEDURE REQUEST_OUTBOUND_FILE_TRANSFER(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_id FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pc_data_type ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pn_file_transfer_group DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
        pn_file_transfer_packet_size DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
        pl_file_transfer_content OUT FILE_TRANSFER.FILE_TRANSFER_CONTENT%TYPE,
        pd_file_transfer_create_ts OUT FILE_TRANSFER.CREATED_TS%TYPE,
        pn_mcp_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
		pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE);		
		
	-- R29
    PROCEDURE REQUEST_OUTBOUND_FILE_TRANSFER(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_id FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pc_data_type ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pn_file_transfer_group DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
        pn_file_transfer_packet_size DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
        pl_file_transfer_content OUT FILE_TRANSFER.FILE_TRANSFER_CONTENT%TYPE,
        pd_file_transfer_create_ts OUT FILE_TRANSFER.CREATED_TS%TYPE,
        pn_mcp_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE);
        
    PROCEDURE SP_RECORD_FILE_TRANSFER(
        pv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_cd FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pv_event_global_trans_cd VARCHAR2,
        pc_overwrite_flag CHAR,
        pl_file_content FILE_TRANSFER.FILE_TRANSFER_CONTENT%TYPE,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pd_file_transfer_ts DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_TS%TYPE DEFAULT SYSDATE,
        pn_session_id ENGINE.DEVICE_SESSION.DEVICE_SESSION_ID%TYPE
    );

  PROCEDURE SP_NORMALIZE_CALL_IN_TIME(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pn_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE,
		pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
		pv_device_time_zone_guid VARCHAR2,
		pn_result_cd OUT NUMBER,
		pv_activated_call_in_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_non_activ_call_in_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_settlement_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_dex_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE
	);
/**
  * r29 Call-in normalization version
  */ 
	PROCEDURE SP_NORMALIZE_CALL_IN_TIME(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pn_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE,
		pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
		pv_device_time_zone_guid VARCHAR2,
		pn_result_cd OUT NUMBER,
		pv_activated_call_in_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_non_activ_call_in_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_settlement_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_dex_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
    pn_command_id       OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
    pv_data_type        OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
    pl_command          OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE
	);

  /**
    * r29 version of SP_NEXT_PENDING_COMMAND
    * This is the feed of commands from the UI tools to the App Layer and thus the devices
    */
    PROCEDURE SP_NEXT_PENDING_COMMAND(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pv_data_type OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pr_command_bytes OUT RAW,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pv_file_transfer_name OUT FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_id OUT FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pl_file_transfer_content OUT FILE_TRANSFER.FILE_TRANSFER_CONTENT%TYPE,
        pn_file_transfer_group_num OUT DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
        pn_file_transfer_pkt_size OUT DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
        pd_file_transfer_created_ts OUT DEVICE_FILE_TRANSFER.CREATED_TS%TYPE,
        pn_max_execute_order ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE DEFAULT 999,
        pn_priority_command_id ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE DEFAULT NULL,
        pv_session_attributes IN OUT VARCHAR2); -- contains character 'C' if configuration has been requested previously

    /**
      * r29+ version that doesn't return command id but does return row count
      */
    PROCEDURE UPSERT_PENDING_COMMAND(
         pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
         pv_date_type IN ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
         pv_command IN ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE,
         pv_execute_order IN ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE DEFAULT 999,
         pn_rows_inserted OUT PLS_INTEGER);

    /** r29+ version
      * Inserts a pending command into the table, and returns a command id,
      * IF it does not already exist.
      */      
    PROCEDURE UPSERT_PENDING_COMMAND(
        pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
        pv_data_type IN ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pv_command IN ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE,
        pv_execute_cd IN ENGINE.MACHINE_CMD_PENDING.EXECUTE_CD%TYPE,
        pv_execute_order IN ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE DEFAULT 999,
        pn_rows_inserted OUT PLS_INTEGER,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE);
         
    PROCEDURE CONFIG_POKE(pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
     pv_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE);
     
    FUNCTION GET_DEVICE_ID_BY_NAME(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pd_effective_date DEVICE.CREATED_TS%TYPE
    )
    RETURN DEVICE.DEVICE_ID%TYPE;
    
    FUNCTION GET_DEVICE_ID_BY_SERIAL(
        pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
        pd_effective_date DEVICE.CREATED_TS%TYPE
    )
    RETURN DEVICE.DEVICE_ID%TYPE;
	
	PROCEDURE CLONE_GX_CONFIG (
		pn_source_device_id IN DEVICE.DEVICE_ID%TYPE,
		pn_target_device_id IN DEVICE.DEVICE_ID%TYPE,
		pn_part1_changed_count OUT NUMBER,
		pn_part2_changed_count OUT NUMBER,
		pn_part3_changed_count OUT NUMBER,
		pn_counters_changed_count OUT NUMBER
	);	
END;
/

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/DEVICE/PKG_DEVICE_CONFIGURATION.pbk?rev=1.101
CREATE OR REPLACE PACKAGE BODY DEVICE.PKG_DEVICE_CONFIGURATION IS
    
PROCEDURE SP_GET_FILE_TRANSFER_BLOB
(
    pn_file_transfer_id file_transfer.file_transfer_id%TYPE,
    pbl_file_transfer_content OUT file_transfer.file_transfer_content%TYPE
)
IS
BEGIN
    SELECT file_transfer_content
	INTO pbl_file_transfer_content
    FROM device.file_transfer
    WHERE file_transfer_id = pn_file_transfer_id;
END;

PROCEDURE SP_GET_MAP_CONFIG_FILE
(
    pn_device_id device.device_id%TYPE,
    pv_file_content_hex OUT VARCHAR2
)
IS
BEGIN
    pv_file_content_hex := '';
	FOR rec IN (
		SELECT DS.DEVICE_SETTING_VALUE
		FROM DEVICE.DEVICE_SETTING DS
		JOIN DEVICE.DEVICE D ON DS.DEVICE_ID = D.DEVICE_ID
		JOIN DEVICE.CONFIG_TEMPLATE_SETTING CTS ON DECODE(D.DEVICE_TYPE_ID, PKG_CONST.DEVICE_TYPE__GX, PKG_CONST.DEVICE_TYPE__G4, D.DEVICE_TYPE_ID) = CTS.DEVICE_TYPE_ID
			AND DS.DEVICE_SETTING_PARAMETER_CD = CTS.DEVICE_SETTING_PARAMETER_CD
			AND CTS.FIELD_OFFSET BETWEEN 0 AND 511
		WHERE DS.DEVICE_ID = pn_device_id
		ORDER BY CTS.FIELD_OFFSET
	) 
	LOOP
		pv_file_content_hex := pv_file_content_hex || rec.DEVICE_SETTING_VALUE;
	END LOOP;
END;

PROCEDURE SP_UPSERT_KIOSK_CONFIG_UPDATE
(
	pv_device_name DEVICE.DEVICE_NAME%TYPE,
	pn_device_id DEVICE.DEVICE_ID%TYPE,
	pv_kiosk_config_name ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE
)
IS
	ln_file_transfer_id FILE_TRANSFER.FILE_TRANSFER_ID%TYPE;
	lb_file_transfer_content FILE_TRANSFER.FILE_TRANSFER_CONTENT%TYPE;
	ln_device_file_transfer_id DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_ID%TYPE;
	ln_required_fields NUMBER;
	ln_pending_file_transfer NUMBER;
	lv_line VARCHAR2(300);
BEGIN	
	SELECT COUNT(1) INTO ln_required_fields 
	FROM DEVICE.DEVICE_SETTING
	WHERE DEVICE_ID = pn_device_id
	AND DEVICE_SETTING_PARAMETER_CD IN ('SSN', 'VMC', 'EncKey')
	AND DECODE(DEVICE_SETTING_VALUE, NULL, 0, LENGTH(DEVICE_SETTING_VALUE)) > 6;

	IF ln_required_fields = 3 THEN
		BEGIN
			SELECT FILE_TRANSFER_ID
			INTO ln_file_transfer_id
			FROM (SELECT /*+index(FT IDX_FILE_TRANSFER_NAME)*/ FILE_TRANSFER_ID 
			FROM DEVICE.FILE_TRANSFER FT
			WHERE FILE_TRANSFER_NAME = pv_device_name || '-CFG'
				AND FILE_TRANSFER_TYPE_CD = PKG_CONST.FILE_TYPE__CONFIG
			ORDER BY CREATED_TS)
			WHERE ROWNUM = 1;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				SELECT seq_file_transfer_id.nextval INTO ln_file_transfer_id FROM DUAL;
				INSERT INTO DEVICE.FILE_TRANSFER(FILE_TRANSFER_ID, FILE_TRANSFER_NAME, FILE_TRANSFER_TYPE_CD)
				VALUES(ln_file_transfer_id, pv_device_name || '-CFG', PKG_CONST.FILE_TYPE__CONFIG);
		END;
		
		UPDATE DEVICE.FILE_TRANSFER
		SET FILE_TRANSFER_CONTENT = EMPTY_BLOB()
		WHERE FILE_TRANSFER_ID = ln_file_transfer_id;
		
		SELECT FILE_TRANSFER_CONTENT
		INTO lb_file_transfer_content
		FROM DEVICE.FILE_TRANSFER
		WHERE FILE_TRANSFER_ID = ln_file_transfer_id
		FOR UPDATE;
		
		FOR rec IN (
			SELECT DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_VALUE
			FROM DEVICE.DEVICE_SETTING
			WHERE DEVICE_ID = pn_device_id
			ORDER BY FILE_ORDER
		) 
		LOOP
			lv_line := rec.device_setting_parameter_cd || '=' || rec.device_setting_value || PKG_CONST.ASCII__LF;
			dbms_lob.writeappend(lb_file_transfer_content, LENGTH(lv_line), UTL_RAW.CAST_TO_RAW(lv_line));
		END LOOP;
	
		UPDATE ENGINE.MACHINE_CMD_PENDING
		SET EXECUTE_CD = 'A'
		WHERE MACHINE_ID = pv_device_name
		AND DATA_TYPE = '9B'
		AND UPPER(COMMAND) = UPPER(pv_kiosk_config_name);
		
		SELECT COUNT(1) 
		INTO ln_pending_file_transfer
		FROM DEVICE.DEVICE_FILE_TRANSFER DFT, ENGINE.MACHINE_CMD_PENDING MCP 
		WHERE DFT.DEVICE_ID = pn_device_id
		AND DFT.FILE_TRANSFER_ID = ln_file_transfer_id
		AND DFT.DEVICE_FILE_TRANSFER_STATUS_CD = 0 
		AND DFT.DEVICE_FILE_TRANSFER_DIRECT = 'O' 
		AND MCP.MACHINE_ID = pv_device_name
		AND MCP.DATA_TYPE = 'A4'
		AND DFT.DEVICE_FILE_TRANSFER_ID = MCP.COMMAND;
			
		IF ln_pending_file_transfer = 0 THEN
			SELECT SEQ_DEVICE_FILE_TRANSFER_ID.NEXTVAL INTO ln_device_file_transfer_id FROM DUAL;
			INSERT INTO DEVICE.DEVICE_FILE_TRANSFER(
				DEVICE_FILE_TRANSFER_ID, 
				DEVICE_ID, 
				FILE_TRANSFER_ID, 
				DEVICE_FILE_TRANSFER_DIRECT, 
				DEVICE_FILE_TRANSFER_STATUS_CD, 
				DEVICE_FILE_TRANSFER_PKT_SIZE 
			) 
			VALUES(
				ln_device_file_transfer_id,
				pn_device_id, 
				ln_file_transfer_id, 
				'O',
				0,
				1024);
				
			INSERT INTO ENGINE.MACHINE_CMD_PENDING(
				MACHINE_ID, 
				DATA_TYPE, 
				COMMAND, 
				EXECUTE_CD, 
				EXECUTE_ORDER
			) 
			VALUES(
				pv_device_name,
				'A4',
				ln_device_file_transfer_id, 
				'P',
				1
			);
		END IF;
	END IF;
END;

-- IS_DEVICE_SETTING_IN_CONFIG is deprecated in R30
FUNCTION IS_DEVICE_SETTING_IN_CONFIG(
	pn_device_id IN device.device_id%TYPE,
	pv_device_setting_parameter_cd IN device_setting.device_setting_parameter_cd%TYPE,
	pv_device_setting_value IN device_setting.device_setting_value%TYPE
)
RETURN VARCHAR
IS
BEGIN
    RETURN 'Y';
END;

	FUNCTION GET_DEVICE_SETTING(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pv_device_setting_parameter_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE
	) RETURN DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE
	IS
		lv_device_setting_value DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
	BEGIN
		SELECT DEVICE_SETTING_VALUE
		INTO lv_device_setting_value
		FROM DEVICE.DEVICE_SETTING
		WHERE DEVICE_ID = pn_device_id
			AND DEVICE_SETTING_PARAMETER_CD = pv_device_setting_parameter_cd;
		
		RETURN lv_device_setting_value;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			RETURN NULL;
	END;

PROCEDURE SP_UPDATE_DEVICE_SETTING(
	pn_device_id IN device.device_id%TYPE,
	pv_device_setting_parameter_cd IN device_setting.device_setting_parameter_cd%TYPE,
	pv_device_setting_value IN device_setting.device_setting_value%TYPE
)
IS
	ln_exists NUMBER;
BEGIN
	SP_UPSERT_DEVICE_SETTING(pn_device_id, pv_device_setting_parameter_cd, pv_device_setting_value, ln_exists);
END;

	PROCEDURE SP_UPSERT_DEVICE_SETTING(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pv_device_setting_parameter_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_device_setting_value DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pn_file_order DEVICE_SETTING.FILE_ORDER%TYPE,
		pn_exists OUT NUMBER
	)
	IS
		ln_count NUMBER;
	BEGIN
		SELECT COUNT(1) INTO ln_count
		FROM device.device_setting_parameter
		WHERE device_setting_parameter_cd = pv_device_setting_parameter_cd;
			
		IF ln_count = 0 THEN
			BEGIN
				INSERT INTO device.device_setting_parameter(device_setting_parameter_cd)
				VALUES(pv_device_setting_parameter_cd);
			EXCEPTION
	        	WHEN DUP_VAL_ON_INDEX THEN
	        		NULL;
	        END;	
		END IF;
		
		UPDATE device.device_setting
		SET device_setting_value = pv_device_setting_value
		WHERE device_id = pn_device_id
			AND device_setting_parameter_cd = pv_device_setting_parameter_cd;
		
		IF SQL%NOTFOUND THEN
			pn_exists := 0;
			BEGIN
				INSERT INTO device.device_setting(device_id, device_setting_parameter_cd, device_setting_value, file_order)
				VALUES(pn_device_id, pv_device_setting_parameter_cd, pv_device_setting_value, pn_file_order);
			EXCEPTION
	        	WHEN DUP_VAL_ON_INDEX THEN
	        		SP_UPSERT_DEVICE_SETTING(pn_device_id, pv_device_setting_parameter_cd, pv_device_setting_value, pn_file_order, pn_exists);
	        END;
		ELSE
			pn_exists := 1;
		END IF;
    END;
	
PROCEDURE SP_UPSERT_DEVICE_SETTING(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pv_device_setting_parameter_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_device_setting_value DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,		
		pn_exists OUT NUMBER
	)
	IS
	BEGIN
		SP_UPSERT_DEVICE_SETTING(pn_device_id, pv_device_setting_parameter_cd, pv_device_setting_value, NULL, pn_exists);
	END;

PROCEDURE SP_UPSERT_CFG_TEMPLATE_SETTING(
	pn_config_template_id CONFIG_TEMPLATE_SETTING.CONFIG_TEMPLATE_ID%TYPE,
	pv_device_setting_parameter_cd CONFIG_TEMPLATE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
	pv_config_template_setting_val CONFIG_TEMPLATE_SETTING.CONFIG_TEMPLATE_SETTING_VALUE%TYPE,
	pn_exists OUT NUMBER
) 
IS
	ln_count NUMBER;
BEGIN
	SELECT COUNT(1) INTO ln_count
	FROM device.device_setting_parameter
	WHERE device_setting_parameter_cd = pv_device_setting_parameter_cd;
		
	IF ln_count = 0 THEN
		BEGIN
			INSERT INTO device.device_setting_parameter(device_setting_parameter_cd)
			VALUES(pv_device_setting_parameter_cd);
		EXCEPTION
			WHEN DUP_VAL_ON_INDEX THEN
				NULL;
		END;	
	END IF;
	
	UPDATE device.config_template_setting
	SET config_template_setting_value = pv_config_template_setting_val
	WHERE config_template_id = pn_config_template_id
		AND device_setting_parameter_cd = pv_device_setting_parameter_cd;
	
	IF SQL%NOTFOUND THEN
		pn_exists := 0;
		BEGIN
			INSERT INTO device.config_template_setting(config_template_id, device_setting_parameter_cd, config_template_setting_value)
			VALUES(pn_config_template_id, pv_device_setting_parameter_cd, pv_config_template_setting_val);
		EXCEPTION
			WHEN DUP_VAL_ON_INDEX THEN
				SP_UPSERT_CFG_TEMPLATE_SETTING(pn_config_template_id, pv_device_setting_parameter_cd, pv_config_template_setting_val, pn_exists);
		END;
	ELSE
		pn_exists := 1;
	END IF;
END;
	
PROCEDURE SP_UPDATE_DEVICE_SETTINGS
(
    pn_device_id IN device.device_id%TYPE,
    pn_file_transfer_id IN file_transfer.file_transfer_id%TYPE,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_setting_count OUT NUMBER
)
IS
    ll_file_transfer_content file_transfer.file_transfer_content%TYPE;
    ln_pos NUMBER := 1;
    ln_content_size NUMBER := 2000;
    ln_content_pos NUMBER := 1;
    ls_content VARCHAR2(4000);
    ls_record VARCHAR2(4000);
    ls_param device_setting.device_setting_parameter_cd%TYPE;
    ls_value VARCHAR2(4000);
    ln_return NUMBER := 0;
    ln_file_transfer_type_cd file_transfer.file_transfer_type_cd%TYPE;
	lv_file_transfer_name file_transfer.file_transfer_name%TYPE;
	ln_file_order device_setting.file_order%TYPE := 0;
	ln_config_template_id CONFIG_TEMPLATE.CONFIG_TEMPLATE_ID%TYPE;
BEGIN
    pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;

    SELECT file_transfer_type_cd, file_transfer_content, file_transfer_name
    INTO ln_file_transfer_type_cd, ll_file_transfer_content, lv_file_transfer_name
    FROM device.file_transfer
    WHERE file_transfer_id = pn_file_transfer_id;

    IF ln_file_transfer_type_cd = PKG_CONST.FILE_TYPE__CONFIG THEN
        DELETE FROM device.device_setting
        WHERE device_id = pn_device_id
            AND device_setting_parameter_cd IN (
                SELECT device_setting_parameter_cd
                FROM device.device_setting_parameter
                WHERE device_setting_ui_configurable = 'Y');
    ELSE
		SELECT config_template_id
		INTO ln_config_template_id
		FROM device.config_template
		WHERE config_template_name = lv_file_transfer_name;
	
        DELETE FROM device.config_template_setting
        WHERE config_template_id = ln_config_template_id;
    END IF;

    LOOP
        ls_content := ls_content || utl_raw.cast_to_varchar2(HEXTORAW(DBMS_LOB.SUBSTR(ll_file_transfer_content, ln_content_size, ln_content_pos)));
        ln_content_pos := ln_content_pos + ln_content_size;

        ln_pos := NVL(INSTR(ls_content, PKG_CONST.ASCII__LF), 0);
        IF ln_pos = 0 OR NVL(INSTR(ls_content, '='), 0) = 0 THEN
            ln_return := 1;
        END IF;

        LOOP
            ln_pos := NVL(INSTR(ls_content, PKG_CONST.ASCII__LF), 0);
            IF ln_pos = 0 AND ln_return = 0 THEN
                EXIT;
            END IF;

            IF ln_return = 0 THEN
                ls_record := SUBSTR(ls_content, 1, ln_pos - 1);
                ls_content := SUBSTR(ls_content, ln_pos + 1);
            ELSE
                ls_record := ls_content;
            END IF;

            ln_pos := NVL(INSTR(ls_record, '='), 0);
            IF ln_pos = 0 AND ln_return = 0 THEN
                EXIT;
            END IF;

            ls_param := TRIM(SUBSTR(ls_record, 1, ln_pos - 1));
            ls_value := REPLACE(TRIM(SUBSTR(ls_record, ln_pos + 1, LENGTH(ls_record) - ln_pos)), CHR(13), '');
			IF LENGTH(ls_value) > 200 THEN
				ls_value := NULL;
			END IF;

            IF NVL(LENGTH(ls_param), 0) > 0 THEN
				BEGIN
					INSERT INTO device.device_setting_parameter(device_setting_parameter_cd)
					SELECT ls_param FROM dual
					WHERE NOT EXISTS (
						SELECT 1 FROM device.device_setting_parameter
						WHERE device_setting_parameter_cd = ls_param);
				EXCEPTION
					WHEN DUP_VAL_ON_INDEX THEN
						NULL;
				END;

				ln_file_order := ln_file_order + 1;
				
				BEGIN
					IF ln_file_transfer_type_cd = PKG_CONST.FILE_TYPE__CONFIG THEN
						INSERT INTO device.device_setting(device_id, device_setting_parameter_cd, device_setting_value, file_order)
						VALUES(pn_device_id, ls_param, ls_value, ln_file_order);
					ELSE
						INSERT INTO device.config_template_setting(config_template_id, device_setting_parameter_cd, config_template_setting_value, file_order)
						VALUES(ln_config_template_id, ls_param, ls_value, ln_file_order);
					END IF;
				EXCEPTION
					WHEN DUP_VAL_ON_INDEX THEN
						NULL;
				END;					
            END IF;

            IF ln_return = 1 THEN
                IF ln_file_transfer_type_cd = PKG_CONST.FILE_TYPE__CONFIG THEN
                    SELECT COUNT(1)
                    INTO pn_setting_count
                    FROM device.device_setting
                    WHERE device_id = pn_device_id
                        AND device_setting_parameter_cd IN (
                            SELECT device_setting_parameter_cd
                            FROM device.device_setting_parameter
                            WHERE device_setting_ui_configurable = 'Y');
                ELSE
                    SELECT COUNT(1)
                    INTO pn_setting_count
                    FROM device.config_template_setting
                    WHERE config_template_id = ln_config_template_id
                        AND device_setting_parameter_cd IN (
                            SELECT device_setting_parameter_cd
                            FROM device.device_setting_parameter
                            WHERE device_setting_ui_configurable = 'Y');
                END IF;

                pn_result_cd := PKG_CONST.RESULT__SUCCESS;
                pv_error_message := PKG_CONST.ERROR__NO_ERROR;
                RETURN;
            END IF;
        END LOOP;

    END LOOP;
END;

PROCEDURE SP_UPDATE_CFG_TMPL_SETTINGS
(
    pn_file_transfer_id IN file_transfer.file_transfer_id%TYPE,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_setting_count OUT NUMBER
)
IS
BEGIN
    SP_UPDATE_DEVICE_SETTINGS(0, pn_file_transfer_id, pn_result_cd, pv_error_message, pn_setting_count);
END;

PROCEDURE SP_GET_HOST_BY_PORT_NUM
(
    pv_device_name IN device.device_name%TYPE,
    pn_host_port_num IN host.host_port_num%TYPE,
    pt_utc_ts IN TIMESTAMP,
    pn_host_id OUT host.host_id%TYPE
)
IS
BEGIN
    SELECT host_id INTO pn_host_id FROM (
        SELECT host_id
        FROM device.host h
        INNER JOIN device.device d ON h.device_id = d.device_id
        WHERE d.device_name = pv_device_name
            AND h.host_port_num = pn_host_port_num
            AND SYS_EXTRACT_UTC(CAST(h.created_ts AS TIMESTAMP)) <= pt_utc_ts
        ORDER BY h.created_ts DESC
    ) WHERE ROWNUM = 1;
END;

PROCEDURE SP_GET_HOST
(
    pn_device_id IN device.device_id%TYPE,
    pv_device_name IN device.device_name%TYPE,
    pn_host_port_num IN host.host_port_num%TYPE,
    pt_utc_ts IN TIMESTAMP,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_host_id OUT host.host_id%TYPE
)
IS
    ln_new_host_count NUMBER;
BEGIN
    BEGIN
        pn_result_cd := PKG_CONST.RESULT__FAILURE;
        pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;

        sp_get_host_by_port_num(pv_device_name, pn_host_port_num, pt_utc_ts, pn_host_id);
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            BEGIN
                -- fail over to the base host
                sp_get_host_by_port_num(pv_device_name, 0, pt_utc_ts, pn_host_id);
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    BEGIN
                        sp_create_default_hosts(pn_device_id, ln_new_host_count, pn_result_cd, pv_error_message);
                        IF pn_result_cd != PKG_CONST.RESULT__SUCCESS THEN
                            RETURN;
                        END IF;

                        sp_get_host_by_port_num(pv_device_name, 0, SYS_EXTRACT_UTC(SYSTIMESTAMP), pn_host_id);
                    EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                            pn_result_cd := PKG_CONST.RESULT__HOST_NOT_FOUND;
                            pv_error_message := 'Unable to find host for device_name: ' || pv_device_name || ', host_port_num: ' || pn_host_port_num;
                            RETURN;
                    END;
            END;
    END;

    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;

PROCEDURE SP_CREATE_DEFAULT_HOSTS
(
    pn_device_id                            IN  device.device_id%TYPE,
    pn_new_host_count                       OUT NUMBER,
    pn_result_cd                            OUT NUMBER,
    pv_error_message                        OUT VARCHAR2
)
IS
    ln_base_host_count                      NUMBER;
    ln_other_host_count                     NUMBER;
    lv_device_serial_cd                     device.device_serial_cd%TYPE;
    ln_device_type_id                       device.device_type_id%TYPE;
    ln_def_base_host_type_id                device_type.def_base_host_type_id%TYPE;
       ln_def_base_host_equipment_id           device_type.def_base_host_equipment_id%TYPE;
       ln_def_prim_host_type_id                device_type.def_prim_host_type_id%TYPE;
       ln_def_prim_host_equipment_id           device_type.def_prim_host_equipment_id%TYPE;
BEGIN
    pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;
    pn_new_host_count := 0;

    SELECT COUNT(1)
    INTO ln_base_host_count
    FROM device.device d, device.host h, device.device_type_host_type dtht
    WHERE d.device_id = h.device_id
       AND h.host_type_id = dtht.host_type_id
       AND d.device_type_id = dtht.device_type_id
       AND d.device_id = pn_device_id
       AND (dtht.device_type_host_type_cd = 'B'
            OR h.host_port_num = 0);

    SELECT COUNT(1)
    INTO ln_other_host_count
    FROM device.device d, device.host h, device.device_type_host_type dtht
    WHERE d.device_id = h.device_id
       AND h.host_type_id = dtht.host_type_id
       AND d.device_type_id = dtht.device_type_id
       AND d.device_id = pn_device_id
       AND dtht.device_type_host_type_cd <> 'B'
       AND h.host_port_num <> 0;

    -- if exists both a base and another host, then there's nothing to do here
    IF ln_base_host_count > 0 AND ln_other_host_count > 0 THEN
        pn_result_cd := PKG_CONST.RESULT__SUCCESS;
        pv_error_message := PKG_CONST.ERROR__NO_ERROR;
        RETURN;
    END IF;

    SELECT
        d.device_serial_cd,
        d.device_type_id,
        dt.def_base_host_type_id,
        dt.def_base_host_equipment_id,
        dt.def_prim_host_type_id,
        dt.def_prim_host_equipment_id
    INTO
        lv_device_serial_cd,
        ln_device_type_id,
        ln_def_base_host_type_id,
           ln_def_base_host_equipment_id,
           ln_def_prim_host_type_id,
           ln_def_prim_host_equipment_id
    FROM device.device_type dt, device.device d
    WHERE d.device_type_id = dt.device_type_id
       AND d.device_id = pn_device_id;

    -- Create base host if it doesn't exist
    IF ln_base_host_count = 0 AND ln_def_base_host_type_id IS NOT NULL THEN
        INSERT INTO device.host
        (
            host_status_cd,
            host_serial_cd,
            host_est_complete_minut,
            host_port_num,
            host_setting_updated_yn_flag,
            device_id,
            host_position_num,
            host_active_yn_flag,
            host_type_id,
            host_equipment_id
        )
        VALUES
        (
            0,
            lv_device_serial_cd,
            0,
            0,
            'N',
            pn_device_id,
            0,
            'Y',
            ln_def_base_host_type_id,
            ln_def_base_host_equipment_id
        );

        pn_new_host_count := pn_new_host_count + 1;
    END IF;

    -- Create primary host if no other hosts exist
    IF ln_other_host_count = 0 AND ln_def_prim_host_type_id IS NOT NULL THEN
        INSERT INTO device.host
        (
            host_status_cd,
            host_est_complete_minut,
            host_port_num,
            host_setting_updated_yn_flag,
            device_id,
            host_position_num,
            host_active_yn_flag,
            host_type_id,
            host_equipment_id
        )
        VALUES
        (
            0,
            0,
            1,
            'N',
            pn_device_id,
            0,
            'Y',
            ln_def_prim_host_type_id,
            ln_def_prim_host_equipment_id
        );

        pn_new_host_count := pn_new_host_count + 1;
    END IF;

    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;

    FUNCTION GET_OR_CREATE_HOST_EQUIPMENT(
        pv_host_equipment_mfgr IN HOST_EQUIPMENT.HOST_EQUIPMENT_MFGR%TYPE,
    	pv_host_equipment_model IN HOST_EQUIPMENT.HOST_EQUIPMENT_MODEL%TYPE,
        pn_existing_cnt OUT PLS_INTEGER)
        RETURN HOST_EQUIPMENT.HOST_EQUIPMENT_ID%TYPE
    IS
        ln_host_equipment_id HOST_EQUIPMENT.HOST_EQUIPMENT_ID%TYPE;
    BEGIN
        SELECT MAX(HOST_EQUIPMENT_ID), 1
          INTO ln_host_equipment_id, pn_existing_cnt
          FROM DEVICE.HOST_EQUIPMENT
         WHERE HOST_EQUIPMENT_MFGR = pv_host_equipment_mfgr
           AND HOST_EQUIPMENT_MODEL = pv_host_equipment_model;
        IF ln_host_equipment_id IS NULL THEN
            SELECT SEQ_HOST_EQUIPMENT_ID.NEXTVAL, 0
              INTO ln_host_equipment_id, pn_existing_cnt
              FROM DUAL;
            INSERT INTO DEVICE.HOST_EQUIPMENT(HOST_EQUIPMENT_ID, HOST_EQUIPMENT_MFGR, HOST_EQUIPMENT_MODEL)
                 VALUES(ln_host_equipment_id, pv_host_equipment_mfgr, pv_host_equipment_model);
        END IF;
        RETURN ln_host_equipment_id;
    EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
            RETURN GET_OR_CREATE_HOST_EQUIPMENT(pv_host_equipment_mfgr, pv_host_equipment_model, pn_existing_cnt);
    END;
        
    PROCEDURE UPSERT_HOST(
        pn_device_id IN DEVICE.DEVICE_ID%TYPE,
        pn_host_port_num IN HOST.HOST_PORT_NUM%TYPE,
        pn_host_position_num IN HOST.HOST_POSITION_NUM%TYPE,
        pn_host_type_id IN HOST.HOST_TYPE_ID%TYPE,
        pv_host_serial_cd IN HOST.HOST_SERIAL_CD%TYPE,
    	pv_host_label_cd IN HOST.HOST_LABEL_CD%TYPE,
    	pn_host_equipment_id HOST_EQUIPMENT.HOST_EQUIPMENT_ID%TYPE,
        pn_host_est_complete_minut IN HOST.HOST_EST_COMPLETE_MINUT%TYPE,
        pn_host_id OUT HOST.HOST_ID%TYPE,
        pn_existing_cnt OUT PLS_INTEGER)
    IS
    BEGIN
        UPDATE DEVICE.HOST
           SET (HOST_TYPE_ID, HOST_SERIAL_CD, HOST_LABEL_CD, HOST_EQUIPMENT_ID, HOST_EST_COMPLETE_MINUT, HOST_ACTIVE_YN_FLAG) =
               (SELECT pn_host_type_id, pv_host_serial_cd, pv_host_label_cd, pn_host_equipment_id, pn_host_est_complete_minut, 'Y' FROM DUAL)
         WHERE DEVICE_ID = pn_device_id
           AND HOST_PORT_NUM = pn_host_port_num
           AND HOST_POSITION_NUM = pn_host_position_num
           RETURNING HOST_ID, 1 INTO pn_host_id, pn_existing_cnt;
        IF pn_host_id IS NULL THEN
            SELECT SEQ_HOST_ID.NEXTVAL, 0
              INTO pn_host_id, pn_existing_cnt
              FROM DUAL;
            INSERT INTO DEVICE.HOST(HOST_ID, DEVICE_ID, HOST_PORT_NUM, HOST_POSITION_NUM, HOST_TYPE_ID, HOST_SERIAL_CD, HOST_LABEL_CD, HOST_EQUIPMENT_ID, HOST_EST_COMPLETE_MINUT, HOST_SETTING_UPDATED_YN_FLAG, HOST_ACTIVE_YN_FLAG)
                 VALUES(pn_host_id, pn_device_id, pn_host_port_num, pn_host_position_num, pn_host_type_id, pv_host_serial_cd, pv_host_label_cd, pn_host_equipment_id, pn_host_est_complete_minut, 'N', 'Y');	        		
       END IF;
    EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
            UPSERT_HOST(pn_device_id, pn_host_port_num, pn_host_position_num, pn_host_type_id, pv_host_serial_cd, pv_host_label_cd, pn_host_equipment_id, pn_host_est_complete_minut, pn_host_id, pn_existing_cnt);
    END;
    
    PROCEDURE UPSERT_HOST(
        pn_device_id IN DEVICE.DEVICE_ID%TYPE,
        pn_host_port_num IN HOST.HOST_PORT_NUM%TYPE,
        pn_host_position_num IN HOST.HOST_POSITION_NUM%TYPE,
        pn_host_type_id IN HOST.HOST_TYPE_ID%TYPE,
        pv_host_serial_cd IN HOST.HOST_SERIAL_CD%TYPE,
    	pv_host_label_cd IN HOST.HOST_LABEL_CD%TYPE,
    	pv_host_equipment_mfgr IN HOST_EQUIPMENT.HOST_EQUIPMENT_MFGR%TYPE,
    	pv_host_equipment_model IN HOST_EQUIPMENT.HOST_EQUIPMENT_MODEL%TYPE,
        pn_host_est_complete_minut IN HOST.HOST_EST_COMPLETE_MINUT%TYPE,
        pn_host_id OUT HOST.HOST_ID%TYPE,
        pn_existing_cnt OUT PLS_INTEGER)
    IS
        ln_host_equipment_existing_cnt PLS_INTEGER;
    BEGIN
        UPSERT_HOST(pn_device_id, pn_host_port_num, pn_host_position_num, pn_host_type_id, pv_host_serial_cd, pv_host_label_cd, GET_OR_CREATE_HOST_EQUIPMENT(pv_host_equipment_mfgr, pv_host_equipment_model, ln_host_equipment_existing_cnt), pn_host_est_complete_minut, pn_host_id, pn_existing_cnt);
    END;

    PROCEDURE UPSERT_HOST_SETTING(
		pn_host_id HOST.HOST_ID%TYPE,
		pv_host_setting_parameter_cd HOST_SETTING.HOST_SETTING_PARAMETER%TYPE,
		pv_host_setting_value HOST_SETTING.HOST_SETTING_VALUE%TYPE)
    IS
    BEGIN
       UPDATE DEVICE.HOST_SETTING
           SET HOST_SETTING_VALUE = pv_host_setting_value
         WHERE HOST_ID = pn_host_id 
           AND HOST_SETTING_PARAMETER = pv_host_setting_parameter_cd;
	    IF SQL%NOTFOUND THEN
            INSERT INTO DEVICE.HOST_SETTING(HOST_ID, HOST_SETTING_PARAMETER, HOST_SETTING_VALUE)
                VALUES(pn_host_id, pv_host_setting_parameter_cd, pv_host_setting_value);
        END IF;
    EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
            UPSERT_HOST_SETTING(pn_host_id, pv_host_setting_parameter_cd, pv_host_setting_value);
    END;
    
    PROCEDURE UPSERT_HOST_SETTING(
		pn_host_id HOST.HOST_ID%TYPE,
		pv_host_setting_parameter_cd HOST_SETTING.HOST_SETTING_PARAMETER%TYPE,
		pv_host_setting_value HOST_SETTING.HOST_SETTING_VALUE%TYPE,
        pv_old_host_setting_value OUT HOST_SETTING.HOST_SETTING_VALUE%TYPE)
    IS
    BEGIN
        SELECT MAX(HOST_SETTING_VALUE)
          INTO pv_old_host_setting_value
          FROM DEVICE.HOST_SETTING
         WHERE HOST_ID = pn_host_id 
           AND HOST_SETTING_PARAMETER = pv_host_setting_parameter_cd;         
        UPSERT_HOST_SETTING(pn_host_id, pv_host_setting_parameter_cd, pv_host_setting_value);
    END;
    
     PROCEDURE UPDATE_COMM_STATS(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pn_rssi NUMBER,
		pn_ber NUMBER,
        pd_update_ts GPRS_DEVICE.RSSI_TS%TYPE,
        pv_modem_info GPRS_DEVICE.MODEM_INFO%TYPE)
    IS
        ln_host_id HOST.HOST_ID%TYPE;
    BEGIN
		UPDATE DEVICE.GPRS_DEVICE GD
		SET DEVICE_ID = NULL,
			GPRS_DEVICE_STATE_ID = DECODE(GPRS_DEVICE_STATE_ID, 5, 4, GPRS_DEVICE_STATE_ID)
		WHERE DEVICE_ID = pn_device_id
			AND ICCID NOT IN(SELECT TO_NUMBER_OR_NULL(HOST_SERIAL_CD) FROM DEVICE.HOST WHERE DEVICE_ID = GD.DEVICE_ID AND HOST_TYPE_ID = 202);
	
		UPDATE DEVICE.GPRS_DEVICE GD
		   SET RSSI = pn_rssi || ',' || pn_ber, 
               RSSI_TS = pd_update_ts, 
               LAST_FILE_TRANSFER_ID = NULL, 
               MODEM_INFO_RECEIVED_TS = SYSDATE, 
               MODEM_INFO = pv_modem_info,
		   	   DEVICE_ID = pn_device_id, 
               GPRS_DEVICE_STATE_ID = 5,
		       ASSIGNED_BY = DECODE(DEVICE_ID, pn_device_id, ASSIGNED_BY, 'APP_LAYER'), 
		   	   ASSIGNED_TS = DECODE(DEVICE_ID, pn_device_id, ASSIGNED_TS, SYSDATE)
		 WHERE ICCID IN(SELECT TO_NUMBER_OR_NULL(HOST_SERIAL_CD) FROM DEVICE.HOST WHERE DEVICE_ID = pn_device_id AND HOST_TYPE_ID = 202)
		   AND (RSSI_TS IS NULL OR RSSI_TS < pd_update_ts);
        SELECT MAX(HOST_ID)
          INTO ln_host_id
          FROM DEVICE.HOST
         WHERE DEVICE_ID = pn_device_id
           AND HOST_TYPE_ID IN(202,204);
        IF ln_host_id IS NOT NULL THEN
            UPSERT_HOST_SETTING(ln_host_id,'CSQ',pn_rssi || ',' || pn_ber);
        END IF;
	END;

	-- SP_CONSTRUCT_PROPERTIES_FILE is deprecated in R30
    PROCEDURE SP_CONSTRUCT_PROPERTIES_FILE(
        pn_device_id IN device.device_id%TYPE,
        pl_file OUT CLOB)
    IS
    BEGIN
        NULL;
    END;

	-- SP_UPDATE_DEVICE_CONFIG_FILE is deprecated in R30
    PROCEDURE SP_UPDATE_DEVICE_CONFIG_FILE(
        pn_device_id DEVICE.DEVICE_ID%TYPE,
        pl_config_file LONG)
    IS
    BEGIN
		NULL;
    END;

    PROCEDURE SP_INITIALIZE_CONFIG_FILE(
        pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
        pn_new_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
        pn_new_property_list_version NUMBER,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pn_updated OUT NUMBER,
        pn_old_property_list_version OUT NUMBER)
    IS
        ln_device_id DEVICE.DEVICE_ID%TYPE;
        lv_default_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE;
        lv_old_default_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE;
		lv_last_lock_utc_ts VARCHAR2(128);
     BEGIN
		lv_last_lock_utc_ts := PKG_GLOBAL.REQUEST_LOCK('DEVICE.DEVICE', pv_device_name);
	 
        SELECT MAX(DEVICE_ID)
          INTO ln_device_id
          FROM (SELECT DEVICE_ID
             FROM DEVICE.DEVICE
            WHERE DEVICE_NAME = pv_device_name
            ORDER BY DEVICE_ACTIVE_YN_FLAG DESC, CREATED_TS DESC)
         WHERE ROWNUM = 1;

		IF ln_device_id IS NULL THEN
			RETURN;
		END IF;
		
		SELECT DECODE(DBADMIN.PKG_UTL.COMPARE(pn_new_device_type_id, 13), 
			-1, default_config_template_name, default_config_template_name || pn_new_property_list_version)
		INTO lv_default_name
		FROM device.device_type
		WHERE device_type_id = pn_new_device_type_id;
		
		INSERT INTO DEVICE.DEVICE_SETTING(DEVICE_ID, DEVICE_SETTING_PARAMETER_CD, DEVICE_SETTING_VALUE)
		SELECT ln_device_id, CTS.DEVICE_SETTING_PARAMETER_CD, CTS.CONFIG_TEMPLATE_SETTING_VALUE
		FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS
		JOIN DEVICE.CONFIG_TEMPLATE CT ON CTS.CONFIG_TEMPLATE_ID = CT.CONFIG_TEMPLATE_ID
		WHERE CT.CONFIG_TEMPLATE_NAME = lv_default_name AND NOT EXISTS (
			SELECT 1 FROM DEVICE.DEVICE_SETTING
			WHERE DEVICE_ID = ln_device_id
				AND DEVICE_SETTING_PARAMETER_CD = CTS.DEVICE_SETTING_PARAMETER_CD
		);
		
		IF pn_new_device_type_id IN (0, 1) THEN
			-- set Call Home Now Flag to N
			UPDATE DEVICE.DEVICE_SETTING
			SET DEVICE_SETTING_VALUE = '4E'
			WHERE DEVICE_ID = ln_device_id
				AND DEVICE_SETTING_PARAMETER_CD = '210'
				AND DBADMIN.PKG_UTL.EQL(DEVICE_SETTING_VALUE, '4E') = 'N';
		END IF;
	 
		IF pn_new_device_type_id IN (0, 1, 6) THEN
			RETURN;
		END IF;

		SELECT MAX(TO_NUMBER(ds.DEVICE_SETTING_VALUE))
		  INTO pn_old_property_list_version
		  FROM DEVICE.DEVICE_SETTING ds
		 WHERE ds.DEVICE_ID = ln_device_id
		   AND ds.DEVICE_SETTING_PARAMETER_CD = PKG_CONST.DSP__PROPERTY_LIST_VERSION
		   AND REGEXP_LIKE(ds.DEVICE_SETTING_VALUE, '^[0-9]+$');
		IF pn_new_property_list_version IS NOT NULL AND pn_new_property_list_version <> NVL(pn_old_property_list_version, -1) THEN
			SP_UPDATE_DEVICE_SETTING(ln_device_id, PKG_CONST.DSP__PROPERTY_LIST_VERSION, pn_new_property_list_version);
			
		   -- Update all values that equal the default in the old version and where the default in the new version is differnt
		   lv_old_default_name := 'DEFAULT-CFG-' || pn_new_device_type_id || '-' || pn_old_property_list_version;
		   MERGE INTO DEVICE.DEVICE_SETTING O
			 USING (
				   SELECT ln_device_id DEVICE_ID, cts.DEVICE_SETTING_PARAMETER_CD SETTING_NAME, cts.CONFIG_TEMPLATE_SETTING_VALUE SETTING_VALUE
					FROM DEVICE.CONFIG_TEMPLATE ct
					JOIN DEVICE.CONFIG_TEMPLATE_SETTING cts
					  ON cts.CONFIG_TEMPLATE_ID = ct.CONFIG_TEMPLATE_ID
					JOIN DEVICE.CONFIG_TEMPLATE_SETTING cts0
					  ON cts0.DEVICE_SETTING_PARAMETER_CD = cts.DEVICE_SETTING_PARAMETER_CD
					JOIN DEVICE.CONFIG_TEMPLATE ct0
					  ON cts0.CONFIG_TEMPLATE_ID = ct0.CONFIG_TEMPLATE_ID
					JOIN DEVICE.DEVICE_SETTING ds
					  ON cts.DEVICE_SETTING_PARAMETER_CD = ds.DEVICE_SETTING_PARAMETER_CD
				   WHERE cts.CONFIG_TEMPLATE_SETTING_VALUE != cts0.CONFIG_TEMPLATE_SETTING_VALUE
					 AND cts0.CONFIG_TEMPLATE_SETTING_VALUE = ds.DEVICE_SETTING_VALUE
					 AND cts.DEVICE_SETTING_PARAMETER_CD NOT IN('50','51','52','60','61','62','63','64','80','81','70','100','101','102','103','104','105','106','107','108','200','201','202','203','204','205','206','207','208','300','301')
					 AND TO_NUMBER_OR_NULL(cts.DEVICE_SETTING_PARAMETER_CD) IS NOT NULL
					 AND ct.CONFIG_TEMPLATE_NAME = lv_default_name
					 AND ct0.CONFIG_TEMPLATE_NAME = lv_old_default_name
					 AND ds.DEVICE_ID = ln_device_id) N
				  ON (O.DEVICE_ID = N.DEVICE_ID AND O.DEVICE_SETTING_PARAMETER_CD = N.SETTING_NAME)
				  WHEN MATCHED THEN
				   UPDATE
					  SET O.DEVICE_SETTING_VALUE = N.SETTING_VALUE
				  WHEN NOT MATCHED THEN
				   INSERT (O.DEVICE_ID,
						   O.DEVICE_SETTING_PARAMETER_CD,
						   O.DEVICE_SETTING_VALUE)
					VALUES(N.DEVICE_ID,
						   N.SETTING_NAME,
						   N.SETTING_VALUE
					);
		END IF;

        pn_updated := PKG_CONST.BOOLEAN__TRUE;
    END;

    PROCEDURE SP_INITIALIZE_CONFIG_FILE(
        pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
        pn_new_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
        pn_new_property_list_version NUMBER,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pn_updated OUT NUMBER)
    IS
        ln_old_property_list_version NUMBER;
    BEGIN
        SP_INITIALIZE_CONFIG_FILE(pv_device_name, pn_new_device_type_id, pn_new_property_list_version, pn_file_transfer_id, pn_updated, ln_old_property_list_version);
    END;

    PROCEDURE ADD_PENDING_FILE_TRANSFER(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_id FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pc_data_type ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pn_file_transfer_group DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
        pn_file_transfer_packet_size DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
        pn_execute_order ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE
        )
    IS
        ll_command          ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE;
    BEGIN
      ADD_PENDING_FILE_TRANSFER(
        pv_device_name,pv_file_transfer_name,pn_file_transfer_type_id,
        pc_data_type,pn_file_transfer_group,pn_file_transfer_packet_size,pn_execute_order,pn_command_id, pn_file_transfer_id,
        ll_command);
        
    END; -- stub for ADD_PENDING_FILE_TRANFER without returned data parameters
    
    PROCEDURE ADD_PENDING_FILE_TRANSFER(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_id FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pc_data_type ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pn_file_transfer_group DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
        pn_file_transfer_packet_size DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
        pn_execute_order ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pl_command          OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE
        )
    IS
        ln_dft_id DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_ID%TYPE;
        ln_device_id DEVICE_FILE_TRANSFER.DEVICE_ID%TYPE;
    BEGIN
        SELECT SEQ_FILE_TRANSFER_ID.NEXTVAL, SEQ_DEVICE_FILE_TRANSFER_ID.NEXTVAL, ENGINE.SEQ_MACHINE_CMD_PENDING_ID.NEXTVAL, DEVICE_ID
          INTO pn_file_transfer_id, ln_dft_id, pn_command_id, ln_device_id
          FROM (SELECT DEVICE_ID
          FROM DEVICE.DEVICE
         WHERE DEVICE_NAME = pv_device_name
         ORDER BY DEVICE_ACTIVE_YN_FLAG DESC, DEVICE_ID DESC)
         WHERE ROWNUM = 1;
        INSERT INTO DEVICE.FILE_TRANSFER(FILE_TRANSFER_ID, FILE_TRANSFER_TYPE_CD, FILE_TRANSFER_NAME)
           VALUES(pn_file_transfer_id, pn_file_transfer_type_id, pv_file_transfer_name);
        INSERT INTO DEVICE.DEVICE_FILE_TRANSFER(DEVICE_FILE_TRANSFER_ID, DEVICE_ID, FILE_TRANSFER_ID, DEVICE_FILE_TRANSFER_DIRECT, DEVICE_FILE_TRANSFER_STATUS_CD, DEVICE_FILE_TRANSFER_GROUP_NUM, DEVICE_FILE_TRANSFER_PKT_SIZE)
            VALUES(ln_dft_id, ln_device_id, pn_file_transfer_id, 'O', 0, pn_file_transfer_group, pn_file_transfer_packet_size);
        INSERT INTO ENGINE.MACHINE_CMD_PENDING(MACHINE_COMMAND_PENDING_ID, MACHINE_ID, DATA_TYPE, COMMAND, EXECUTE_CD, EXECUTE_ORDER, EXECUTE_DATE)
           VALUES(pn_command_id, pv_device_name, pc_data_type, TO_CHAR(ln_dft_id), 'S', NVL(pn_execute_order, 999), SYSDATE)
           RETURNING TRIM(COMMAND) into pl_command;
    END;

	-- R30+
    PROCEDURE REQUEST_OUTBOUND_FILE_TRANSFER(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_id FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pc_data_type ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pn_file_transfer_group DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
        pn_file_transfer_packet_size DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
        pl_file_transfer_content OUT FILE_TRANSFER.FILE_TRANSFER_CONTENT%TYPE,
        pd_file_transfer_create_ts OUT FILE_TRANSFER.CREATED_TS%TYPE,
        pn_mcp_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
		pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE)
    IS
        l_dft_id DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_ID%TYPE;
        l_device_id DEVICE_FILE_TRANSFER.DEVICE_ID%TYPE;
    BEGIN
        SELECT FILE_TRANSFER_ID, CREATED_TS
        INTO pn_file_transfer_id, pd_file_transfer_create_ts
        FROM (
            SELECT FILE_TRANSFER_ID, CREATED_TS
            FROM DEVICE.FILE_TRANSFER
            WHERE FILE_TRANSFER_NAME = pv_file_transfer_name
                AND FILE_TRANSFER_TYPE_CD = pn_file_transfer_type_id
            ORDER BY CREATED_TS
        ) WHERE ROWNUM = 1;
        SELECT MAX(dft.DEVICE_FILE_TRANSFER_ID)
          INTO l_dft_id
          FROM DEVICE.DEVICE_FILE_TRANSFER dft
          JOIN DEVICE.DEVICE d ON dft.DEVICE_ID = d.DEVICE_ID
         WHERE d.DEVICE_NAME = pv_device_name
           AND dft.FILE_TRANSFER_ID = pn_file_transfer_id
           AND dft.DEVICE_FILE_TRANSFER_DIRECT = 'O'
           AND dft.DEVICE_FILE_TRANSFER_STATUS_CD = 1;
       IF l_dft_id IS NOT NULL THEN -- already exists, find pending command
         SELECT MAX(mcp.MACHINE_COMMAND_PENDING_ID) -- avoid NO_DATA_FOUND
           INTO pn_mcp_id
           FROM ENGINE.MACHINE_CMD_PENDING mcp
          WHERE mcp.MACHINE_ID = pv_device_name
            AND mcp.EXECUTE_CD IN('P', 'S')
            AND UPPER(mcp.DATA_TYPE) IN('7C', 'A4', 'C7', 'C8')
            AND TRIM(mcp.COMMAND) = TRIM(TO_CHAR(l_dft_id));
        ELSE
            SELECT SEQ_DEVICE_FILE_TRANSFER_ID.NEXTVAL, DEVICE_ID
              INTO l_dft_id, l_device_id
              FROM (SELECT DEVICE_ID
              FROM DEVICE.DEVICE
             WHERE DEVICE_NAME = pv_device_name
             ORDER BY DEVICE_ACTIVE_YN_FLAG DESC, DEVICE_ID DESC)
             WHERE ROWNUM = 1;
            INSERT INTO DEVICE.DEVICE_FILE_TRANSFER(DEVICE_FILE_TRANSFER_ID, DEVICE_ID, FILE_TRANSFER_ID, DEVICE_FILE_TRANSFER_DIRECT, DEVICE_FILE_TRANSFER_STATUS_CD, DEVICE_FILE_TRANSFER_GROUP_NUM, DEVICE_FILE_TRANSFER_PKT_SIZE)
              VALUES(l_dft_id, l_device_id, pn_file_transfer_id, 'O', 0, pn_file_transfer_group, pn_file_transfer_packet_size);
        END IF;
        IF pn_mcp_id IS NULL THEN
            SELECT ENGINE.SEQ_MACHINE_CMD_PENDING_ID.NEXTVAL
              INTO pn_mcp_id
              FROM DUAL;
            INSERT INTO ENGINE.MACHINE_CMD_PENDING(MACHINE_COMMAND_PENDING_ID, MACHINE_ID, DATA_TYPE, COMMAND, EXECUTE_CD, EXECUTE_ORDER, EXECUTE_DATE)
              VALUES(pn_mcp_id, pv_device_name, pc_data_type, TO_CHAR(l_dft_id), 'S', 999, SYSDATE);
        END IF;
        SP_GET_FILE_TRANSFER_BLOB(pn_file_transfer_id, pl_file_transfer_content);
    EXCEPTION
       WHEN NO_DATA_FOUND THEN
               NULL; -- let variables be null
    END;
	
	-- R29
	PROCEDURE REQUEST_OUTBOUND_FILE_TRANSFER(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_id FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pc_data_type ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pn_file_transfer_group DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
        pn_file_transfer_packet_size DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
        pl_file_transfer_content OUT FILE_TRANSFER.FILE_TRANSFER_CONTENT%TYPE,
        pd_file_transfer_create_ts OUT FILE_TRANSFER.CREATED_TS%TYPE,
        pn_mcp_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE)
	IS
		ln_file_transfer_id FILE_TRANSFER.FILE_TRANSFER_ID%TYPE;
	BEGIN
		REQUEST_OUTBOUND_FILE_TRANSFER(
        pv_device_name,
        pv_file_transfer_name,
        pn_file_transfer_type_id,
        pc_data_type,
        pn_file_transfer_group,
        pn_file_transfer_packet_size,
        pl_file_transfer_content,
        pd_file_transfer_create_ts,
        pn_mcp_id,
		ln_file_transfer_id);
	END;

    PROCEDURE SP_RECORD_FILE_TRANSFER(
        pv_file_transfer_name FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_cd FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pv_event_global_trans_cd VARCHAR2,
        pc_overwrite_flag CHAR,
        pl_file_content FILE_TRANSFER.FILE_TRANSFER_CONTENT%TYPE,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pd_file_transfer_ts DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_TS%TYPE DEFAULT SYSDATE,
        pn_session_id ENGINE.DEVICE_SESSION.DEVICE_SESSION_ID%TYPE
    )
    IS
        ln_device_file_transfer_id DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_ID%TYPE;
        ln_device_id DEVICE_FILE_TRANSFER.DEVICE_ID%TYPE;
    BEGIN
		ln_device_id := GET_DEVICE_ID_BY_NAME(pv_device_name, pd_file_transfer_ts);
		
		-- duplicate detection of already stored files re-delivered by QueueLayer
		SELECT MAX(DEVICE_FILE_TRANSFER_ID)
		INTO ln_device_file_transfer_id
		FROM DEVICE.DEVICE_FILE_TRANSFER
		WHERE DEVICE_ID = ln_device_id
			AND DEVICE_SESSION_ID = pn_session_id
			AND DEVICE_FILE_TRANSFER_TS = pd_file_transfer_ts;
		IF ln_device_file_transfer_id IS NOT NULL THEN
			RETURN;
		END IF;
		
        BEGIN
            SELECT FILE_TRANSFER_ID
              INTO pn_file_transfer_id
              FROM (SELECT /*+ index(FT INX_FILE_TRANSFER_TYPE_NAME) index(DFT IDX_DEVICE_FILE_TRANSFER_ID) */ ft.FILE_TRANSFER_ID
              FROM DEVICE.FILE_TRANSFER ft
              LEFT JOIN DEVICE.DEVICE_FILE_TRANSFER dft on ft.FILE_TRANSFER_ID = dft.FILE_TRANSFER_ID
             WHERE ft.FILE_TRANSFER_NAME = pv_file_transfer_name
               AND ft.FILE_TRANSFER_TYPE_CD = pn_file_transfer_type_cd
               AND NVL(dft.DEVICE_FILE_TRANSFER_DIRECT, 'I') = 'I'
               AND NVL(dft.DEVICE_FILE_TRANSFER_STATUS_CD, 1) = 1
               AND pc_overwrite_flag = 'Y'
             ORDER BY ft.FILE_TRANSFER_ID DESC)
             WHERE ROWNUM = 1;
            UPDATE DEVICE.FILE_TRANSFER SET FILE_TRANSFER_CONTENT = pl_file_content
             WHERE FILE_TRANSFER_ID = pn_file_transfer_id;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                SELECT SEQ_FILE_TRANSFER_ID.NEXTVAL
                  INTO pn_file_transfer_id
                  FROM DUAL;
                INSERT INTO DEVICE.FILE_TRANSFER (
                    FILE_TRANSFER_ID,
                    FILE_TRANSFER_NAME,
                    FILE_TRANSFER_TYPE_CD,
                    FILE_TRANSFER_CONTENT)
                  VALUES(
                    pn_file_transfer_id,
                    pv_file_transfer_name,
                    pn_file_transfer_type_cd,
                    pl_file_content);
        END;
        SELECT SEQ_DEVICE_FILE_TRANSFER_ID.NEXTVAL
          INTO ln_device_file_transfer_id
          FROM DUAL;
        INSERT INTO DEVICE.DEVICE_FILE_TRANSFER(
            DEVICE_FILE_TRANSFER_ID,
            DEVICE_ID,
            FILE_TRANSFER_ID,
            DEVICE_FILE_TRANSFER_DIRECT,
            DEVICE_FILE_TRANSFER_STATUS_CD,
            DEVICE_FILE_TRANSFER_TS,
            DEVICE_SESSION_ID)
         VALUES(
            ln_device_file_transfer_id,
            ln_device_id,
            pn_file_transfer_id,
            'I',
            1,
            pd_file_transfer_ts,
            pn_session_id);
        IF pv_event_global_trans_cd IS NOT NULL THEN
            INSERT INTO DEVICE.DEVICE_FILE_TRANSFER_EVENT(EVENT_ID, DEVICE_FILE_TRANSFER_ID)
              SELECT EVENT_ID, ln_device_file_transfer_id
                FROM DEVICE.EVENT
                WHERE EVENT_GLOBAL_TRANS_CD = pv_event_global_trans_cd;
        END IF;        
    END;
	
	FUNCTION GET_NORMALIZED_TIME(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
		pv_normalizer_start_min_cd ENGINE.APP_SETTING.APP_SETTING_CD%TYPE,
		pv_normalizer_end_min_cd ENGINE.APP_SETTING.APP_SETTING_CD%TYPE,
		pv_window_start_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_window_hours_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_device_time_zone_guid VARCHAR2
	) RETURN VARCHAR2
	IS
		ln_call_in_start_min NUMBER;
		ln_call_in_end_min NUMBER;
		ln_call_in_window_min NUMBER;
		ln_call_in_time_min NUMBER;
		ln_call_in_window_start NUMBER;
		ln_call_in_window_hours NUMBER;
		ln_seed_4 NUMBER := NVL(DBADMIN.TO_NUMBER_OR_NULL(SUBSTR(pv_device_serial_cd, -4)), 0);
		ln_convert_time_zone NUMBER := PKG_CONST.BOOLEAN__TRUE;
	BEGIN
		ln_call_in_window_start := ROUND(NVL(DBADMIN.TO_NUMBER_OR_NULL(GET_DEVICE_SETTING(pn_device_id, pv_window_start_cd)), -1));
		ln_call_in_window_hours := ROUND(NVL(DBADMIN.TO_NUMBER_OR_NULL(GET_DEVICE_SETTING(pn_device_id, pv_window_hours_cd)), 0));
		IF ln_call_in_window_start BETWEEN 0 AND 2359 AND ln_call_in_window_hours BETWEEN 1 AND 23 THEN
			ln_call_in_start_min := FLOOR(ln_call_in_window_start / 100) * 60 + MOD(ln_call_in_window_start, 100);
			ln_call_in_window_min := ln_call_in_window_hours * 60;
			ln_convert_time_zone := PKG_CONST.BOOLEAN__FALSE;
		ELSE
			ln_call_in_start_min := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING(pv_normalizer_start_min_cd));
			ln_call_in_end_min := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING(pv_normalizer_end_min_cd));
			
			IF ln_call_in_start_min < 0 OR ln_call_in_start_min = ln_call_in_end_min THEN
				RETURN NULL;
			END IF;

			ln_call_in_window_min := MOD(ln_call_in_end_min - ln_call_in_start_min, 1440);
			IF ln_call_in_window_min < 0 THEN
				ln_call_in_window_min := ln_call_in_window_min + 1440;
			END IF;
		END IF;
			
		-- use last 4 digits of device serial number to calculate its call-in time
		ln_call_in_time_min := MOD(ln_call_in_start_min + ROUND(ln_call_in_window_min * ln_seed_4 / 9999), 1440);
		
		IF ln_convert_time_zone = PKG_CONST.BOOLEAN__TRUE THEN
			-- convert call-in time from server time zone to device local time zone
			ln_call_in_time_min := MOD(ln_call_in_time_min + DBADMIN.PKG_UTL.TIME_ZONE_DIFF_MIN(pv_device_time_zone_guid, PKG_CONST.DB_TIME_ZONE), 1440);
			IF ln_call_in_time_min < 0 THEN
				ln_call_in_time_min := ln_call_in_time_min + 1440;
			END IF;
		END IF;
		
		RETURN LPAD(FLOOR(ln_call_in_time_min / 60), 2, '0') || LPAD(MOD(ln_call_in_time_min, 60), 2, '0');			
	END;
	
	FUNCTION GET_NORMALIZED_OFFSET(
		pv_normalized_time VARCHAR2,
		pv_device_time_zone_guid VARCHAR2
	) RETURN NUMBER
	IS
		ln_normalized_time_min NUMBER;
		ln_normalized_utc_time_min NUMBER;
	BEGIN
		ln_normalized_time_min := TO_NUMBER(SUBSTR(pv_normalized_time, 1, 2)) * 60 + TO_NUMBER(SUBSTR(pv_normalized_time, 3, 2));
		ln_normalized_utc_time_min := MOD(ln_normalized_time_min + DBADMIN.PKG_UTL.TIME_ZONE_DIFF_MIN(PKG_CONST.GMT_TIME_ZONE, pv_device_time_zone_guid), 1440);
		IF ln_normalized_utc_time_min < 0 THEN
			ln_normalized_utc_time_min := ln_normalized_utc_time_min + 1440;
		END IF;
		RETURN ln_normalized_utc_time_min * 60;
	END;

    PROCEDURE NORMALIZE_SCHEDULE(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
		pv_schedule_type DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_normalizer_start_min_cd ENGINE.APP_SETTING.APP_SETTING_CD%TYPE,
		pv_normalizer_end_min_cd ENGINE.APP_SETTING.APP_SETTING_CD%TYPE,
		pv_window_start_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_window_hours_cd DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE,
		pv_device_time_zone_guid VARCHAR2,
		pn_result_cd IN OUT NUMBER,
		pv_schedule IN OUT DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE
	)
    IS
		lv_current_schedule DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
		ln_current_interval NUMBER;
		ln_min_allowed_interval NUMBER;
		lc_reoccurrence_type CHAR(1);
		ln_exists NUMBER;
		lv_normalized_time VARCHAR2(4) := GET_NORMALIZED_TIME(pn_device_id, pv_device_serial_cd, pv_normalizer_start_min_cd, pv_normalizer_end_min_cd, pv_window_start_cd, pv_window_hours_cd, pv_device_time_zone_guid);
		ln_normalized_offset NUMBER := GET_NORMALIZED_OFFSET(lv_normalized_time, pv_device_time_zone_guid);
		lv_schedule DEVICE_SETTING.DEVICE_SETTING_PARAMETER_CD%TYPE := NULL;
    BEGIN
		lv_current_schedule := NVL(GET_DEVICE_SETTING(pn_device_id, pv_schedule_type), '0');
		IF lv_current_schedule = '0' THEN
			IF pv_schedule_type = PKG_CONST.DSP__NON_ACTIV_CALL_IN_SCHED THEN
				lv_schedule := PKG_CONST.REOCCURRENCE_TYPE__INTERVAL || PKG_CONST.SCHEDULE__FS || ln_normalized_offset || PKG_CONST.SCHEDULE__FS || DBADMIN.PKG_GLOBAL.GET_APP_SETTING('EDGE_NON_ACTIVATED_CALL_IN_INTERVAL_SEC');
			ELSIF pv_schedule_type = PKG_CONST.DSP__SETTLEMENT_SCHEDULE THEN
				IF lv_normalized_time IS NOT NULL THEN
					lv_schedule := PKG_CONST.REOCCURRENCE_TYPE__WEEKLY || PKG_CONST.SCHEDULE__FS || lv_normalized_time || PKG_CONST.SCHEDULE__FS || PKG_CONST.SCHEDULE__SUNDAY;
				END IF;
			ELSE
				IF lv_normalized_time IS NOT NULL THEN
					lv_schedule := PKG_CONST.REOCCURRENCE_TYPE__DAILY || PKG_CONST.SCHEDULE__FS || lv_normalized_time;
				END IF;
			END IF;
		ELSE
			lc_reoccurrence_type := SUBSTR(lv_current_schedule, 1, 1);
			IF lc_reoccurrence_type = PKG_CONST.REOCCURRENCE_TYPE__INTERVAL THEN
				ln_current_interval := NVL(DBADMIN.TO_NUMBER_OR_NULL(SUBSTR(lv_current_schedule, INSTR(lv_current_schedule, PKG_CONST.SCHEDULE__FS, -1, 1) + 1)), -1);
				ln_min_allowed_interval := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('MIN_INTERVAL_SCHEDULE_SEC'));
				IF ln_current_interval < ln_min_allowed_interval THEN
					ln_current_interval := ln_min_allowed_interval;
				END IF;
				lv_schedule := lc_reoccurrence_type || PKG_CONST.SCHEDULE__FS || ln_normalized_offset || PKG_CONST.SCHEDULE__FS || ln_current_interval;
			ELSIF lc_reoccurrence_type IN (PKG_CONST.REOCCURRENCE_TYPE__MONTHLY, PKG_CONST.REOCCURRENCE_TYPE__WEEKLY) THEN
				IF lv_normalized_time IS NOT NULL THEN
					lv_schedule := lc_reoccurrence_type || PKG_CONST.SCHEDULE__FS || lv_normalized_time || PKG_CONST.SCHEDULE__FS || SUBSTR(lv_current_schedule, INSTR(lv_current_schedule, PKG_CONST.SCHEDULE__FS, -1, 1) + 1);
				END IF;
			ELSE
				IF lv_normalized_time IS NOT NULL THEN
					lv_schedule := PKG_CONST.REOCCURRENCE_TYPE__DAILY || PKG_CONST.SCHEDULE__FS || lv_normalized_time;
				END IF;
			END IF;
		END IF;
		IF lv_schedule IS NOT NULL AND lv_schedule != lv_current_schedule THEN
			SP_UPSERT_DEVICE_SETTING(pn_device_id, pv_schedule_type, lv_schedule, ln_exists);
			pn_result_cd := 1;
			pv_schedule := lv_schedule;
		END IF;		
    END;
  
  PROCEDURE SP_NORMALIZE_CALL_IN_TIME(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pn_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE,
		pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
		pv_device_time_zone_guid VARCHAR2,
		pn_result_cd OUT NUMBER,
		pv_activated_call_in_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_non_activ_call_in_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_settlement_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_dex_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE
	)
	IS
    ln_command_id       ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE;
    lv_data_type        ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE;
    ll_command          ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE;
  BEGIN
    SP_NORMALIZE_CALL_IN_TIME(
      pn_device_id, 
      pn_device_type_id, 
      pv_device_serial_cd, 
      pv_device_time_zone_guid, 
      pn_result_cd, 
      pv_activated_call_in_schedule, 
      pv_non_activ_call_in_schedule, 
      pv_settlement_schedule, 
      pv_dex_schedule,
      ln_command_id, 
      lv_data_type, 
      ll_command);
  END; -- SP_NORMALIZE_CALL_IN_TIME stub
  
  /**
    * r29 Call-in normalization version
    */ 
	PROCEDURE SP_NORMALIZE_CALL_IN_TIME(
		pn_device_id DEVICE.DEVICE_ID%TYPE,
		pn_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE,
		pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
		pv_device_time_zone_guid VARCHAR2,
		pn_result_cd OUT NUMBER,
		pv_activated_call_in_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_non_activ_call_in_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_settlement_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
		pv_dex_schedule OUT DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE,
    pn_command_id       OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
    pv_data_type        OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
    pl_command          OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE
	)
	IS
		lv_return_msg VARCHAR2(2048);
		ln_exists NUMBER;
	BEGIN
		-- normalize device call-in time to avoid load peaks
		pn_result_cd := 0;
		pv_activated_call_in_schedule := PKG_CONST.ASCII__NUL;
		pv_non_activ_call_in_schedule := PKG_CONST.ASCII__NUL;
		pv_settlement_schedule := PKG_CONST.ASCII__NUL;
		pv_dex_schedule := PKG_CONST.ASCII__NUL;
		
		IF pn_device_type_id IN (PKG_CONST.DEVICE_TYPE__G4, PKG_CONST.DEVICE_TYPE__GX) THEN
			pv_activated_call_in_schedule := GET_NORMALIZED_TIME(pn_device_id, pv_device_serial_cd, 'GX_CALL_IN_NORMALIZER_START_MIN', 'GX_CALL_IN_NORMALIZER_END_MIN',
				'CALL_IN_TIME_WINDOW_START', 'CALL_IN_TIME_WINDOW_HOURS', pv_device_time_zone_guid);
            IF pv_activated_call_in_schedule BETWEEN '0000' AND '0004' THEN
                pv_activated_call_in_schedule := '001' || SUBSTR(pv_activated_call_in_schedule, 4, 1);
            ELSIF pv_activated_call_in_schedule BETWEEN '2356' AND '2359' THEN
                pv_activated_call_in_schedule := '234' || SUBSTR(pv_activated_call_in_schedule, 4, 1);
            END IF;
			SP_UPDATE_CONFIG_AND_RETURN(pn_device_id, 172, pv_activated_call_in_schedule, 'H', pn_device_type_id, 0, pn_result_cd, lv_return_msg, pn_command_id, pv_data_type, pl_command);
		ELSIF pn_device_type_id = PKG_CONST.DEVICE_TYPE__EDGE THEN
			NORMALIZE_SCHEDULE(pn_device_id, pv_device_serial_cd, PKG_CONST.DSP__ACTIVATED_CALL_IN_SCHED, 'EDGE_CALL_IN_NORMALIZER_START_MIN', 'EDGE_CALL_IN_NORMALIZER_END_MIN', 
				'CALL_IN_TIME_WINDOW_START_ACTIVATED', 'CALL_IN_TIME_WINDOW_HOURS_ACTIVATED', pv_device_time_zone_guid, pn_result_cd, pv_activated_call_in_schedule);
			NORMALIZE_SCHEDULE(pn_device_id, pv_device_serial_cd, PKG_CONST.DSP__NON_ACTIV_CALL_IN_SCHED, 'EDGE_CALL_IN_NORMALIZER_START_MIN', 'EDGE_CALL_IN_NORMALIZER_END_MIN', 
				'CALL_IN_TIME_WINDOW_START_NON_ACTIVATED', 'CALL_IN_TIME_WINDOW_HOURS_NON_ACTIVATED', pv_device_time_zone_guid, pn_result_cd, pv_non_activ_call_in_schedule);
			NORMALIZE_SCHEDULE(pn_device_id, pv_device_serial_cd, PKG_CONST.DSP__SETTLEMENT_SCHEDULE, 'EDGE_CALL_IN_NORMALIZER_START_MIN', 'EDGE_CALL_IN_NORMALIZER_END_MIN', 
				'CALL_IN_TIME_WINDOW_START_SETTLEMENT', 'CALL_IN_TIME_WINDOW_HOURS_SETTLEMENT', pv_device_time_zone_guid, pn_result_cd, pv_settlement_schedule);

			IF DBADMIN.PKG_GLOBAL.GET_APP_SETTING('NORMALIZE_EDGE_DEX_SCHEDULE_FLAG') = 'Y'
				AND NVL(GET_DEVICE_SETTING(pn_device_id, PKG_CONST.DSP__VMC_INTERFACE_TYPE), PKG_CONST.VMC_INTERFACE__STANDARD_MDB) = PKG_CONST.VMC_INTERFACE__STANDARD_MDB THEN
				NORMALIZE_SCHEDULE(pn_device_id, pv_device_serial_cd, PKG_CONST.DSP__DEX_SCHEDULE, 'EDGE_CALL_IN_NORMALIZER_START_MIN', 'EDGE_CALL_IN_NORMALIZER_END_MIN', 
					'CALL_IN_TIME_WINDOW_START_DEX', 'CALL_IN_TIME_WINDOW_HOURS_DEX', pv_device_time_zone_guid, pn_result_cd, pv_dex_schedule);
			END IF;
		END IF;
		
		UPDATE DEVICE.DEVICE
		SET CALL_IN_TIME_NORMALIZED_TS = SYSDATE
		WHERE DEVICE_ID = pn_device_id;
	END;
         
  /**
    * Part of the r29 version of SP_NEXT_PENDING_COMMAND
    * This non-public procedure adds commands for ESUDS Diagnostics, if existing diagnostics are beyond 16 hours old.
    */
    PROCEDURE SP_NPC_HELPER_ESUDS_DIAG(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pv_data_type OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pv_session_attributes IN OUT VARCHAR2, -- contains character 'E' if ESUDS diagnostics has been requested previously
        pn_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
        pn_commands_inserted OUT NUMBER,
        pl_command OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE
    )
    IS
        ld_oldest_diag DATE;
    BEGIN
		IF pn_device_type_id IN (PKG_CONST.DEVICE_TYPE__ESUDS) AND INSTR(NVL(pv_session_attributes,'-'),'E') = 0 THEN        
            pv_session_attributes := NVL(pv_session_attributes,'') || 'E';       
            -- if any records for the given device are old, (no records = old records)       
            SELECT MIN(MOST_RECENT_PORT_DIAG_DATE) 
              INTO ld_oldest_diag
              FROM (
                  SELECT d.device_id, h.host_port_num, MAX(NVL(hds.host_diag_last_reported_ts, MIN_DATE)) AS MOST_RECENT_PORT_DIAG_DATE
                    FROM device d  
                    LEFT OUTER JOIN host h ON (d.device_id = h.device_id) 
                    LEFT OUTER JOIN host_diag_status hds ON (hds.host_id = h.host_id) 
                    WHERE d.device_type_id = PKG_CONST.DEVICE_TYPE__ESUDS
                    AND d.device_name = pv_device_name
                    AND d.device_active_yn_flag = 'Y'
                    AND NVL(h.host_active_yn_flag,'Y') = 'Y'
                    AND h.host_port_num <> 0
                    GROUP BY d.device_id, h.host_port_num            
              );
    
            IF ld_oldest_diag < SYSDATE - 1 THEN           
                pv_data_type := '9A61';
                pl_command := '';
                UPSERT_PENDING_COMMAND(pv_device_name, pv_data_type, pl_command, 'S', 999, pn_commands_inserted, pn_command_id);                                   
            END IF;
		END IF;  
    END; -- SP_NPC_HELPER_ESUDS_DIAGNOSTICS

  /**
    * Part of the r29 version of SP_NEXT_PENDING_COMMAND
    * This non-public procedure adds commands for GPRS configuration files, if needed.
    */
    PROCEDURE SP_NPC_HELPER_MODEM(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pv_data_type OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pv_session_attributes IN OUT VARCHAR2, -- contains character 'C' if configuration test has been requested previously, R if configuration requested       
        pn_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
        pn_commands_inserted OUT NUMBER,
        pl_command OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE
    )
    IS
        ld_last_update_ts DATE;
    BEGIN
        -- For Gx and MEI, test to see if modem data is outdated by 7 days. If so, send the appropriate message (both happen to be 87)	
        -- Some G4's only send garbage for modem info, so don't keep requesting it for G4 (PKG_CONST.DEVICE_TYPE__G4)
    	IF pn_device_type_id IN (PKG_CONST.DEVICE_TYPE__GX, PKG_CONST.DEVICE_TYPE__MEI) AND INSTR(NVL(pv_session_attributes,'-'),'C') = 0 THEN             
            pv_session_attributes := NVL(pv_session_attributes,'') || 'C';
            SELECT NVL(MAX(H.LAST_UPDATED_TS), MIN_DATE)
              INTO ld_last_update_ts
              FROM DEVICE.DEVICE D
              JOIN DEVICE.HOST H ON D.DEVICE_ID = H.DEVICE_ID
             WHERE D.DEVICE_NAME = pv_device_name
               AND D.DEVICE_ACTIVE_YN_FLAG = 'Y'
               AND H.HOST_PORT_NUM = 2;
            IF ld_last_update_ts < SYSDATE - NVL(TO_NUMBER_OR_NULL(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DEVICE_MODEM_INFO_REFRESH_HR')) / 24, 7) THEN				
                pv_data_type := '87';					
                IF (pn_device_type_id = PKG_CONST.DEVICE_TYPE__MEI) THEN 
                    pl_command := '4200000000000000FF';							
                ELSIF (pn_device_type_id = PKG_CONST.DEVICE_TYPE__GX OR pn_device_type_id = PKG_CONST.DEVICE_TYPE__G4) THEN 
                    pl_command := '4400802C0000000190';
                END IF;
				pv_session_attributes := NVL(pv_session_attributes,'') || 'R';
                UPSERT_PENDING_COMMAND(pv_device_name, pv_data_type, pl_command, 'S', 999, pn_commands_inserted, pn_command_id);
            END IF;
        END IF;
    END;

  /**
    * Part of the r29 version of SP_NEXT_PENDING_COMMAND
    * This non-public procedure adds commands for G4 configuration files, if existing ones are stale.
    */
    PROCEDURE SP_NPC_HELPER_STALE_CONFIG(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pv_data_type OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pv_session_attributes IN OUT VARCHAR2, -- contains character 'G' if GX configuration has been requested previously
        pn_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
        pn_commands_inserted OUT NUMBER,
        pl_command OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE
    )
    IS
        ld_last_config_ts DATE;
    BEGIN
        -- For MEI/Gx test age of configuration data
		IF pn_device_type_id IN (PKG_CONST.DEVICE_TYPE__GX, PKG_CONST.DEVICE_TYPE__MEI, PKG_CONST.DEVICE_TYPE__G4) AND INSTR(NVL(pv_session_attributes,'-'),'G') = 0 THEN              
	        pv_session_attributes := NVL(pv_session_attributes,'') || 'G';	        
	        IF NOT(pn_device_type_id = PKG_CONST.DEVICE_TYPE__MEI AND INSTR(NVL(pv_session_attributes,'-'), 'R') > 0 ) THEN	        	 
	        	-- Now check last configuration received timestamp 	        	
		        SELECT NVL(D.RECEIVED_CONFIG_FILE_TS, MIN_DATE)
		          INTO ld_last_config_ts
		          FROM DEVICE.DEVICE D 
		         WHERE D.DEVICE_NAME = pv_device_name 
		           AND D.DEVICE_ACTIVE_YN_FLAG = 'Y';
                IF ld_last_config_ts < SYSDATE - NVL(TO_NUMBER_OR_NULL(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DEVICE_CONFIG_REFRESH_HR')) / 24, 30) THEN				
                    pv_data_type := '87';
					-- same command for all devices handled by this procedure					
					pl_command := '4200000000000000FF';		
					pv_session_attributes := NVL(pv_session_attributes,'') || 'R';	
					UPSERT_PENDING_COMMAND(pv_device_name, pv_data_type, pl_command, 'S', 999, pn_commands_inserted, pn_command_id);	                						
				END IF; -- actual test	
			END IF; -- 'R' flag			
    	END IF; -- 'G' flag    	
    END; -- SP_NPC_HELPER_STALE_CONFIG
    
  /**
    * Part of the r29 version of SP_NEXT_PENDING_COMMAND
    * This non-public procedure adds commands to set the Gx verbosity flag 
    * on a non-remotely updatable device.
    */
    PROCEDURE SP_NPC_HELPER_PHILLY_COKE(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pv_data_type OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pv_session_attributes IN OUT VARCHAR2, -- contains character 'P' if ESUDS diagnostics has been requested previously
        pn_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE,
        pv_firmware_version IN DEVICE.FIRMWARE_VERSION%TYPE,
        pn_commands_inserted OUT NUMBER,
        pl_command OUT ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE
    )
    IS
        l_is_philly_coke NUMBER;
    BEGIN
        -- For Gx test age of configuration data
        IF pn_device_type_id IN (PKG_CONST.DEVICE_TYPE__GX) AND INSTR(NVL(pv_session_attributes,'-'),'P') = 0 AND REGEXP_LIKE(pv_firmware_version, 'USA-G5[0-9] ?[vV]4\.2\.(0|1[A-S]).*') THEN             
            pv_session_attributes := NVL(pv_session_attributes,'') || 'P';      
            pv_data_type := '88';
            pl_command := '4400807FE001';
            UPSERT_PENDING_COMMAND(pv_device_name, pv_data_type, pl_command, 'S', 2, pn_commands_inserted, pn_command_id);
		END IF;   
    END; -- SP_NPC_HELPER_PHILLY_COKE

  /**
    * r29 version of SP_NEXT_PENDING_COMMAND
    * This is the feed of commands from the UI tools to the App Layer and thus the devices
    */
    PROCEDURE SP_NEXT_PENDING_COMMAND(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE,
        pv_data_type OUT ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pr_command_bytes OUT RAW,
        pn_file_transfer_id OUT FILE_TRANSFER.FILE_TRANSFER_ID%TYPE,
        pv_file_transfer_name OUT FILE_TRANSFER.FILE_TRANSFER_NAME%TYPE,
        pn_file_transfer_type_id OUT FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE,
        pl_file_transfer_content OUT FILE_TRANSFER.FILE_TRANSFER_CONTENT%TYPE,
        pn_file_transfer_group_num OUT DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_GROUP_NUM%TYPE,
        pn_file_transfer_pkt_size OUT DEVICE_FILE_TRANSFER.DEVICE_FILE_TRANSFER_PKT_SIZE%TYPE,
        pd_file_transfer_created_ts OUT DEVICE_FILE_TRANSFER.CREATED_TS%TYPE,
        pn_max_execute_order ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE DEFAULT 999,
        pn_priority_command_id ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE DEFAULT NULL,
        pv_session_attributes IN OUT VARCHAR2 -- contains character 'C' if configuration has been requested previously       
        )
    IS
        ll_command ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE;
		ln_device_id DEVICE.DEVICE_ID%TYPE;
        ln_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE;
        lv_firmware_version DEVICE.FIRMWARE_VERSION%TYPE;
		lv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE;
		ld_call_in_time_normalized_ts DEVICE.CALL_IN_TIME_NORMALIZED_TS%TYPE;
		lv_device_time_zone_guid VARCHAR2(60);
		ln_result_cd NUMBER;
		lv_activated_call_in_schedule DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
		lv_non_activ_call_in_schedule DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
		lv_settlement_schedule DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
		lv_dex_schedule DEVICE_SETTING.DEVICE_SETTING_VALUE%TYPE;
		ln_command_id ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE;
		ln_file_transfer_id FILE_TRANSFER.FILE_TRANSFER_ID%TYPE;
        l_addr NUMBER;
        l_len NUMBER;
        l_pos NUMBER;
        l_commands_inserted NUMBER;
		ln_sending_file_cnt NUMBER;
		ln_sending_file_limit NUMBER := TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DOWNLOAD_LIMIT_APP_UPGRADE'));
		lv_file_content_hex VARCHAR2(1024);
    BEGIN
		-- throttle the number of concurrent app upgrade file downloads
		SELECT COUNT(1) INTO ln_sending_file_cnt
		FROM engine.machine_cmd_pending mcp
		JOIN device.device_file_transfer dft ON mcp.command = dft.device_file_transfer_id
		JOIN device.file_transfer ft ON dft.file_transfer_id = ft.file_transfer_id
		WHERE mcp.execute_cd = 'S'
			AND mcp.execute_date > SYSDATE - 15/1440
			AND UPPER(mcp.data_type) IN('7C', 'A4', 'C8')
			AND REGEXP_LIKE(mcp.command, '^[0-9]+$')
			AND ft.file_transfer_type_cd = PKG_CONST.FILE_TYPE__APP_UPGRADE;
	
        UPDATE ENGINE.MACHINE_CMD_PENDING MCP
           SET EXECUTE_CD = 'S',
               EXECUTE_DATE = SYSDATE
         WHERE MCP.MACHINE_ID = pv_device_name
           AND MCP.EXECUTE_ORDER <= pn_max_execute_order
           AND MCP.EXECUTE_CD IN('S', 'P')
		   AND (ln_sending_file_cnt < ln_sending_file_limit
				OR UPPER(MCP.DATA_TYPE) NOT IN('7C', 'A4', 'C8')
				OR UPPER(MCP.DATA_TYPE) IN('7C', 'A4', 'C8')
					AND REGEXP_LIKE(MCP.COMMAND, '^[0-9]+$')
					AND EXISTS (
						SELECT 1 FROM DEVICE.DEVICE_FILE_TRANSFER DFT
						JOIN DEVICE.FILE_TRANSFER FT ON DFT.FILE_TRANSFER_ID = FT.FILE_TRANSFER_ID
						WHERE DFT.DEVICE_FILE_TRANSFER_ID = MCP.COMMAND
							AND FT.FILE_TRANSFER_TYPE_CD != PKG_CONST.FILE_TYPE__APP_UPGRADE
					)
			)
           AND NOT EXISTS(
                        SELECT 1
                          FROM ENGINE.MACHINE_CMD_PENDING MCP2
                         WHERE MCP.MACHINE_ID = MCP2.MACHINE_ID
                           AND MCP2.EXECUTE_CD IN('S', 'P')
                           AND MCP.MACHINE_COMMAND_PENDING_ID != MCP2.MACHINE_COMMAND_PENDING_ID
                           AND MCP.EXECUTE_ORDER >= MCP2.EXECUTE_ORDER
                           AND (MCP.EXECUTE_ORDER > MCP2.EXECUTE_ORDER
                               OR MCP2.MACHINE_COMMAND_PENDING_ID = pn_priority_command_id
                               OR (MCP.MACHINE_COMMAND_PENDING_ID != NVL(pn_priority_command_id, 0)
                                    AND MCP.CREATED_TS > MCP2.CREATED_TS
                                    OR (MCP.CREATED_TS = MCP2.CREATED_TS AND MCP.MACHINE_COMMAND_PENDING_ID > MCP2.MACHINE_COMMAND_PENDING_ID))))
            RETURNING MACHINE_COMMAND_PENDING_ID, DATA_TYPE, TRIM(COMMAND) INTO pn_command_id, pv_data_type, ll_command;
			
		IF pn_command_id IS NULL OR pv_data_type = '88' THEN
			SELECT D.DEVICE_ID, D.DEVICE_TYPE_ID, D.DEVICE_SERIAL_CD, D.CALL_IN_TIME_NORMALIZED_TS, TZ.TIME_ZONE_GUID, D.FIRMWARE_VERSION
			  INTO ln_device_id, ln_device_type_id, lv_device_serial_cd, ld_call_in_time_normalized_ts, lv_device_time_zone_guid, lv_firmware_version
              FROM DEVICE.DEVICE D
			  JOIN PSS.POS P ON D.DEVICE_ID = P.DEVICE_ID
			  JOIN LOCATION.LOCATION L ON P.LOCATION_ID = L.LOCATION_ID
			  JOIN LOCATION.TIME_ZONE TZ ON L.LOCATION_TIME_ZONE_CD = TZ.TIME_ZONE_CD
             WHERE D.DEVICE_NAME = pv_device_name
               AND D.DEVICE_ACTIVE_YN_FLAG = 'Y';
		END IF;
		
		IF pn_command_id IS NULL THEN
			IF ln_device_type_id IN (PKG_CONST.DEVICE_TYPE__G4, PKG_CONST.DEVICE_TYPE__GX, PKG_CONST.DEVICE_TYPE__EDGE)
				AND NVL(ld_call_in_time_normalized_ts, MIN_DATE) < SYSDATE - TO_NUMBER(DBADMIN.PKG_GLOBAL.GET_APP_SETTING('DEVICE_CALL_IN_NORMALIZATION_INTERVAL_HR')) / 24 THEN
				SP_NORMALIZE_CALL_IN_TIME(ln_device_id, ln_device_type_id, lv_device_serial_cd, lv_device_time_zone_guid, ln_result_cd, lv_activated_call_in_schedule, lv_non_activ_call_in_schedule, lv_settlement_schedule, lv_dex_schedule, pn_command_id, pv_data_type, ll_command);
				IF ln_result_cd = 1 THEN       
                    IF ln_device_type_id = PKG_CONST.DEVICE_TYPE__EDGE THEN
                        pv_data_type := 'C7';
                        ADD_PENDING_FILE_TRANSFER(pv_device_name, pv_device_name || '-CFG-' || DBADMIN.TIMESTAMP_TO_MILLIS(SYSTIMESTAMP), 
                          PKG_CONST.FILE_TYPE__PROPERTY_LIST, pv_data_type, 0, 1024, 1, ln_command_id, ln_file_transfer_id, ll_command);
                        pn_command_id := ln_command_id; 
                        
                        UPDATE DEVICE.FILE_TRANSFER
                        SET FILE_TRANSFER_CONTENT = RAWTOHEX(DECODE(lv_settlement_schedule, PKG_CONST.ASCII__NUL, NULL, PKG_CONST.DSP__SETTLEMENT_SCHEDULE || '=' || lv_settlement_schedule || PKG_CONST.ASCII__LF)
                          || DECODE(lv_non_activ_call_in_schedule, PKG_CONST.ASCII__NUL, NULL, PKG_CONST.DSP__NON_ACTIV_CALL_IN_SCHED || '=' || lv_non_activ_call_in_schedule || PKG_CONST.ASCII__LF)
                          || DECODE(lv_activated_call_in_schedule, PKG_CONST.ASCII__NUL, NULL, PKG_CONST.DSP__ACTIVATED_CALL_IN_SCHED || '=' || lv_activated_call_in_schedule || PKG_CONST.ASCII__LF)
                          || DECODE(lv_dex_schedule, PKG_CONST.ASCII__NUL, NULL, PKG_CONST.DSP__DEX_SCHEDULE || '=' || lv_dex_schedule || PKG_CONST.ASCII__LF))
                        WHERE FILE_TRANSFER_ID = ln_file_transfer_id;
                    ELSE
                        UPDATE ENGINE.MACHINE_CMD_PENDING MCP
                           SET EXECUTE_CD = 'S',
                               EXECUTE_DATE = SYSDATE
                         WHERE MACHINE_COMMAND_PENDING_ID = pn_command_id;
                    END IF;
                END IF;
            END IF;
            IF pn_command_id IS NULL THEN
                SP_NPC_HELPER_MODEM(pv_device_name, pn_command_id, pv_data_type, pv_session_attributes, ln_device_type_id, l_commands_inserted, ll_command); 
                IF pn_command_id IS NULL THEN
                    SP_NPC_HELPER_STALE_CONFIG(pv_device_name, pn_command_id, pv_data_type, pv_session_attributes, ln_device_type_id, l_commands_inserted, ll_command);
                    IF pn_command_id IS NULL THEN
                        SP_NPC_HELPER_ESUDS_DIAG(pv_device_name, pn_command_id, pv_data_type, pv_session_attributes, ln_device_type_id, l_commands_inserted, ll_command);
                        IF pn_command_id IS NULL THEN
                            SP_NPC_HELPER_PHILLY_COKE(pv_device_name, pn_command_id, pv_data_type, pv_session_attributes, ln_device_type_id, lv_firmware_version, l_commands_inserted, ll_command);
                            IF pn_command_id IS NULL THEN
                                RETURN;
                            END IF;
                        END IF;
                    END IF;
                END IF;
            END IF;
        END IF;
        IF UPPER(pv_data_type) IN('7C', 'A4', 'C8', 'C7') AND REGEXP_LIKE(ll_command, '^[0-9]+$') THEN
            SELECT ft.FILE_TRANSFER_ID, ft.FILE_TRANSFER_NAME, ft.FILE_TRANSFER_TYPE_CD,
                   dft.DEVICE_FILE_TRANSFER_GROUP_NUM, dft.DEVICE_FILE_TRANSFER_PKT_SIZE, dft.CREATED_TS
              INTO pn_file_transfer_id, pv_file_transfer_name, pn_file_transfer_type_id,
                   pn_file_transfer_group_num, pn_file_transfer_pkt_size, pd_file_transfer_created_ts
              FROM DEVICE.DEVICE_FILE_TRANSFER dft
              JOIN DEVICE.FILE_TRANSFER ft on dft.FILE_TRANSFER_ID = ft.FILE_TRANSFER_ID
             WHERE DEVICE_FILE_TRANSFER_ID = TO_NUMBER(ll_command);

            SP_GET_FILE_TRANSFER_BLOB(pn_file_transfer_id, pl_file_transfer_content);
        ELSIF UPPER(pv_data_type) = '88' THEN
            l_addr := to_number(substr(ll_command, 3, 8), 'XXXXXXXX');
            IF substr(ll_command, 1, 2) = '42' THEN -- EEROM: get data from device setting
                l_len := to_number(substr(ll_command, 11, 8), 'XXXXXXXX') * 2;
    
                IF ln_device_type_id in (PKG_CONST.DEVICE_TYPE__G4, PKG_CONST.DEVICE_TYPE__GX) THEN
                      l_pos := l_addr * 2;
                ELSE
                      l_pos := l_addr;
                END IF;
    
                l_pos := l_pos * 2 + 1;
    
				SP_GET_MAP_CONFIG_FILE(ln_device_id, lv_file_content_hex);
                pr_command_bytes := UTL_RAW.CONCAT(hextoraw(substr(ll_command, 1, 10)), SUBSTR(lv_file_content_hex, l_pos, l_len));
            ELSE
                pr_command_bytes := HEXTORAW(ll_command);
            END IF;
        ELSE
            pr_command_bytes := HEXTORAW(ll_command);
        END IF;
    END;
    
    /**
      * r29+ version that doesn't return command id but does return row count
      */
    PROCEDURE UPSERT_PENDING_COMMAND(
        pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
        pv_date_type IN ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pv_command IN ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE,
        pv_execute_order IN ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE DEFAULT 999,
        pn_rows_inserted OUT PLS_INTEGER)
    IS
        ln_command_id ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE;
    BEGIN
      UPSERT_PENDING_COMMAND(pv_device_name,pv_date_type,pv_command,'P',pv_execute_order,pn_rows_inserted,ln_command_id);
    END;
    
    /** r29+ version
      * Inserts a pending command into the table, and returns a command id,
      * IF it does not already exist.
      */      
    PROCEDURE UPSERT_PENDING_COMMAND(
        pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
        pv_data_type IN ENGINE.MACHINE_CMD_PENDING.DATA_TYPE%TYPE,
        pv_command IN ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE,
        pv_execute_cd IN ENGINE.MACHINE_CMD_PENDING.EXECUTE_CD%TYPE,
        pv_execute_order IN ENGINE.MACHINE_CMD_PENDING.EXECUTE_ORDER%TYPE DEFAULT 999,
        pn_rows_inserted OUT PLS_INTEGER,
        pn_command_id OUT ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE)
    IS
	  ld_execute_date ENGINE.MACHINE_CMD_PENDING.EXECUTE_CD%TYPE;
	  ln_cnt NUMBER;
    BEGIN
		SELECT COUNT(1)
		INTO ln_cnt
		FROM ENGINE.MACHINE_CMD_PENDING
		WHERE MACHINE_ID = pv_device_name
			AND UPPER(DATA_TYPE) = UPPER(pv_data_type)
			AND DBADMIN.PKG_UTL.EQL(UPPER(COMMAND), UPPER(pv_command)) = 'Y';
	
		IF ln_cnt = 0 THEN
			SELECT DECODE(pv_execute_cd, 'S', SYSDATE, NULL) INTO ld_execute_date FROM DUAL;
        
			-- This isn't atomic so could result in duplicate pending commands but it's not critical to avoid duplicates only nice
			INSERT INTO ENGINE.MACHINE_CMD_PENDING(MACHINE_COMMAND_PENDING_ID, MACHINE_ID, DATA_TYPE, COMMAND, EXECUTE_CD, EXECUTE_DATE, EXECUTE_ORDER)
			VALUES(SEQ_MACHINE_CMD_PENDING_ID.NEXTVAL, pv_device_name, pv_data_type, pv_command, pv_execute_cd, ld_execute_date, pv_execute_order)
			RETURNING MACHINE_COMMAND_PENDING_ID INTO pn_command_id;
        
			pn_rows_inserted := SQL%ROWCOUNT;
		ELSE
			pn_rows_inserted := 0;
		END IF;
    END;
    
    PROCEDURE CONFIG_POKE
    (pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
     pv_device_type_id IN DEVICE.DEVICE_TYPE_ID%TYPE)
    IS
        l_command ENGINE.MACHINE_CMD_PENDING.COMMAND%TYPE;
        l_file_len NUMBER;
        l_num_parts NUMBER;
        l_pos NUMBER;
        l_poke_size NUMBER:=200;
        l_len NUMBER;
		ln_rows_inserted PLS_INTEGER;
        ln_command_id ENGINE.MACHINE_CMD_PENDING.MACHINE_COMMAND_PENDING_ID%TYPE;
    BEGIN
        IF pv_device_type_id in (0, 1) THEN
            UPDATE ENGINE.MACHINE_CMD_PENDING
            SET EXECUTE_CD='C'
            WHERE MACHINE_ID = pv_device_name
            AND DATA_TYPE = '88'
            AND EXECUTE_ORDER > 2
            AND (EXECUTE_CD = 'P' OR (EXECUTE_CD = 'S' AND EXECUTE_DATE < (SYSDATE-(90/86400))));

            UPSERT_PENDING_COMMAND(pv_device_name, '88', '420000000000000088', 'P', 0, ln_rows_inserted, ln_command_id);
            UPSERT_PENDING_COMMAND(pv_device_name, '88', '42000000470000008E', 'P', 1, ln_rows_inserted, ln_command_id);
            UPSERT_PENDING_COMMAND(pv_device_name, '88', '42000000B20000009C', 'P', 2, ln_rows_inserted, ln_command_id);
        END IF;
    END;
    
    FUNCTION GET_DEVICE_ID_BY_NAME(
        pv_device_name DEVICE.DEVICE_NAME%TYPE,
        pd_effective_date DEVICE.CREATED_TS%TYPE
    )
    RETURN DEVICE.DEVICE_ID%TYPE
    IS
        ln_device_id DEVICE.DEVICE_ID%TYPE;
    BEGIN
        SELECT MAX(DEVICE_ID)
          INTO ln_device_id
          FROM DEVICE.DEVICE
         WHERE DEVICE_ACTIVE_YN_FLAG = 'Y'
           AND DEVICE_NAME = pv_device_name
           AND CREATED_TS <= pd_effective_date;
        IF ln_device_id IS NULL THEN
            SELECT MAX(DEVICE_ID)
              INTO ln_device_id
              FROM (SELECT DEVICE_ID, CREATED_TS, NVL(LEAD(CREATED_TS) OVER (PARTITION BY DEVICE_NAME ORDER BY CREATED_TS), MAX_DATE) NEXT_CREATED_TS
                      FROM DEVICE.DEVICE
                     WHERE DEVICE_NAME = pv_device_name)
             WHERE CREATED_TS <= pd_effective_date
               AND NEXT_CREATED_TS > pd_effective_date;
            IF ln_device_id IS NULL THEN
                SELECT MAX(DEVICE_ID)
                  INTO ln_device_id
                  FROM (SELECT DEVICE_ID
                          FROM DEVICE.DEVICE
                         WHERE DEVICE_NAME = pv_device_name
                           AND CREATED_TS > pd_effective_date
                         ORDER BY CREATED_TS ASC)
                 WHERE ROWNUM = 1;
            END IF;
        END IF;
        RETURN ln_device_id;        
    END;
    
    FUNCTION GET_DEVICE_ID_BY_SERIAL(
        pv_device_serial_cd DEVICE.DEVICE_SERIAL_CD%TYPE,
        pd_effective_date DEVICE.CREATED_TS%TYPE
    )
    RETURN DEVICE.DEVICE_ID%TYPE
    IS
        ln_device_id DEVICE.DEVICE_ID%TYPE;
    BEGIN
        SELECT MAX(DEVICE_ID)
          INTO ln_device_id
          FROM DEVICE.DEVICE
         WHERE DEVICE_ACTIVE_YN_FLAG = 'Y'
           AND DEVICE_SERIAL_CD = pv_device_serial_cd
           AND CREATED_TS <= pd_effective_date;
        IF ln_device_id IS NULL THEN
            SELECT MAX(DEVICE_ID)
              INTO ln_device_id
              FROM (SELECT DEVICE_ID, CREATED_TS, NVL(LEAD(CREATED_TS) OVER (PARTITION BY DEVICE_SERIAL_CD ORDER BY CREATED_TS), MAX_DATE) NEXT_CREATED_TS
                      FROM DEVICE.DEVICE
                     WHERE DEVICE_SERIAL_CD = pv_device_serial_cd)
             WHERE CREATED_TS <= pd_effective_date
               AND NEXT_CREATED_TS > pd_effective_date;
        END IF;
        RETURN ln_device_id;        
    END;
	
	PROCEDURE CLONE_GX_CONFIG (
		pn_source_device_id IN DEVICE.DEVICE_ID%TYPE,
		pn_target_device_id IN DEVICE.DEVICE_ID%TYPE,
		pn_part1_changed_count OUT NUMBER,
		pn_part2_changed_count OUT NUMBER,
		pn_part3_changed_count OUT NUMBER,
		pn_counters_changed_count OUT NUMBER
	)
	IS
	BEGIN
		/*
		 * 	# we are sending very specific sections of the config with the offsets below, not the whole thing for Gx
		 *	#	0 	- 135
		 *	#	142 - 283
		 *	#	356 - 511
		 */	
		MERGE INTO DEVICE.DEVICE_SETTING O
		USING (
			SELECT DS_T.DEVICE_ID, DS_T.DEVICE_SETTING_PARAMETER_CD, DS_S.DEVICE_SETTING_VALUE
			FROM DEVICE.DEVICE_SETTING DS_T
			JOIN DEVICE.DEVICE_SETTING DS_S ON DS_T.DEVICE_SETTING_PARAMETER_CD = DS_S.DEVICE_SETTING_PARAMETER_CD
			WHERE DS_T.DEVICE_ID = pn_target_device_id
				AND DS_S.DEVICE_ID = pn_source_device_id
				AND DS_S.DEVICE_SETTING_PARAMETER_CD IN (
					SELECT DEVICE_SETTING_PARAMETER_CD
					FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS
					WHERE DEVICE_TYPE_ID = 0
						AND FIELD_OFFSET BETWEEN 0 AND 135
				) AND DBADMIN.PKG_UTL.EQL(DS_S.DEVICE_SETTING_VALUE, DS_T.DEVICE_SETTING_VALUE) = 'N') N
		ON (O.DEVICE_ID = N.DEVICE_ID AND O.DEVICE_SETTING_PARAMETER_CD = N.DEVICE_SETTING_PARAMETER_CD)
		WHEN MATCHED THEN
			UPDATE
				SET O.DEVICE_SETTING_VALUE = N.DEVICE_SETTING_VALUE;
		pn_part1_changed_count := SQL%ROWCOUNT;
		
		MERGE INTO DEVICE.DEVICE_SETTING O
		USING (
			SELECT DS_T.DEVICE_ID, DS_T.DEVICE_SETTING_PARAMETER_CD, DS_S.DEVICE_SETTING_VALUE
			FROM DEVICE.DEVICE_SETTING DS_T
			JOIN DEVICE.DEVICE_SETTING DS_S ON DS_T.DEVICE_SETTING_PARAMETER_CD = DS_S.DEVICE_SETTING_PARAMETER_CD
			WHERE DS_T.DEVICE_ID = pn_target_device_id
				AND DS_S.DEVICE_ID = pn_source_device_id
				AND DS_S.DEVICE_SETTING_PARAMETER_CD IN (
					SELECT DEVICE_SETTING_PARAMETER_CD
					FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS
					WHERE DEVICE_TYPE_ID = 0
						AND FIELD_OFFSET BETWEEN 142 AND 283
				) AND DBADMIN.PKG_UTL.EQL(DS_S.DEVICE_SETTING_VALUE, DS_T.DEVICE_SETTING_VALUE) = 'N') N
		ON (O.DEVICE_ID = N.DEVICE_ID AND O.DEVICE_SETTING_PARAMETER_CD = N.DEVICE_SETTING_PARAMETER_CD)
		WHEN MATCHED THEN
			UPDATE
				SET O.DEVICE_SETTING_VALUE = N.DEVICE_SETTING_VALUE;
		pn_part2_changed_count := SQL%ROWCOUNT;
		
		MERGE INTO DEVICE.DEVICE_SETTING O
		USING (
			SELECT DS_T.DEVICE_ID, DS_T.DEVICE_SETTING_PARAMETER_CD, DS_S.DEVICE_SETTING_VALUE
			FROM DEVICE.DEVICE_SETTING DS_T
			JOIN DEVICE.DEVICE_SETTING DS_S ON DS_T.DEVICE_SETTING_PARAMETER_CD = DS_S.DEVICE_SETTING_PARAMETER_CD
			WHERE DS_T.DEVICE_ID = pn_target_device_id
				AND DS_S.DEVICE_ID = pn_source_device_id
				AND DS_S.DEVICE_SETTING_PARAMETER_CD IN (
					SELECT DEVICE_SETTING_PARAMETER_CD
					FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS
					WHERE DEVICE_TYPE_ID = 0
						AND FIELD_OFFSET BETWEEN 356 AND 511
				) AND DBADMIN.PKG_UTL.EQL(DS_S.DEVICE_SETTING_VALUE, DS_T.DEVICE_SETTING_VALUE) = 'N') N
		ON (O.DEVICE_ID = N.DEVICE_ID AND O.DEVICE_SETTING_PARAMETER_CD = N.DEVICE_SETTING_PARAMETER_CD)
		WHEN MATCHED THEN
			UPDATE
				SET O.DEVICE_SETTING_VALUE = N.DEVICE_SETTING_VALUE;
		pn_part3_changed_count := SQL%ROWCOUNT;
		
		UPDATE DEVICE.DEVICE_SETTING
		SET DEVICE_SETTING_VALUE = '00000000'
		WHERE DEVICE_ID = pn_target_device_id
			AND DEVICE_SETTING_PARAMETER_CD IN (
				SELECT DEVICE_SETTING_PARAMETER_CD
				FROM DEVICE.CONFIG_TEMPLATE_SETTING CTS
				WHERE DEVICE_TYPE_ID = 0
					AND FIELD_OFFSET BETWEEN 320 AND 352
			) AND DEVICE_SETTING_VALUE != '00000000';
		pn_counters_changed_count := SQL%ROWCOUNT;
	END;
END;
/

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/DEVICE/TRBI_DEVICE_SETTING.trg?rev=1.4
CREATE OR REPLACE TRIGGER device.trbi_device_setting
BEFORE INSERT ON device.device_setting
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
DECLARE
	LN_DEVICE_TYPE_ID DEVICE.DEVICE_TYPE_ID%TYPE;
Begin
	SELECT DEVICE_TYPE_ID
	INTO LN_DEVICE_TYPE_ID
	FROM DEVICE.DEVICE
	WHERE DEVICE_ID = :NEW.DEVICE_ID;

 SELECT    sysdate,
           user,
           sysdate,
           user		   
      into :new.created_ts,
           :new.created_by,
           :new.last_updated_ts,
           :new.last_updated_by		   
      FROM dual;
	  
	  IF :new.device_setting_parameter_cd NOT IN ('1203', '1204', '1205', '1206') THEN
		:new.device_setting_value := TRIM(:new.device_setting_value);
	  END IF;
	  
	  IF LN_DEVICE_TYPE_ID IN (0, 1) THEN
		IF :NEW.DEVICE_SETTING_PARAMETER_CD = '387' THEN
			-- disable local auth
			:NEW.DEVICE_SETTING_VALUE := '4E';
		ELSIF :NEW.DEVICE_SETTING_PARAMETER_CD = '389' THEN
			-- force hex MDB Inventory Format
			:NEW.DEVICE_SETTING_VALUE := '59';
		END IF;
	  END IF;
End;

/
-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/DEVICE/TRBU_DEVICE_SETTING.trg?rev=1.4
CREATE OR REPLACE TRIGGER device.trbu_device_setting
BEFORE UPDATE ON device.device_setting
REFERENCING new AS NEW old AS OLD
FOR EACH ROW
DECLARE
	LN_DEVICE_TYPE_ID DEVICE.DEVICE_TYPE_ID%TYPE;
Begin
	SELECT DEVICE_TYPE_ID
	INTO LN_DEVICE_TYPE_ID
	FROM DEVICE.DEVICE
	WHERE DEVICE_ID = :NEW.DEVICE_ID;

	SELECT 
		:OLD.created_by, 
		:OLD.created_ts, 
		SYSDATE,
		USER
	INTO
		:NEW.created_by, 
		:NEW.created_ts, 
		:NEW.last_updated_ts,
		:NEW.last_updated_by
	FROM DUAL;
	
	IF :NEW.device_setting_parameter_cd NOT IN ('1203', '1204', '1205', '1206') THEN
		:NEW.device_setting_value := TRIM(:NEW.device_setting_value);
	END IF;
	
	IF LN_DEVICE_TYPE_ID IN (0, 1) THEN
		IF :NEW.DEVICE_SETTING_PARAMETER_CD = '387' THEN
			-- disable local auth
			:NEW.DEVICE_SETTING_VALUE := '4E';
		ELSIF :NEW.DEVICE_SETTING_PARAMETER_CD = '389' THEN
			-- force hex MDB Inventory Format
			:NEW.DEVICE_SETTING_VALUE := '59';
		END IF;
	END IF;
End;

/
-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/DEVICE/VW_FANTASY_DEVICE_CFG_DETAIL.vws?rev=1.4
CREATE OR REPLACE FORCE VIEW DEVICE.VW_FANTASY_DEVICE_CFG_DETAIL (DEVICE_SERIAL_CD, LOCATION_NAME, LAST_ACTIVITY_TS, CUSTOMER_ID, COIN_PULSE_VALUE, DEBIT_CARD_COST_1) AS 
SELECT D.DEVICE_SERIAL_CD, L.LOCATION_NAME, D.LAST_ACTIVITY_TS, POS.CUSTOMER_ID, 
DS_258.DEVICE_SETTING_VALUE COIN_PULSE_VALUE, 
DS_260.DEVICE_SETTING_VALUE DEBIT_CARD_COST_1
FROM DEVICE.DEVICE D
JOIN PSS.POS POS ON D.DEVICE_ID = POS.DEVICE_ID
JOIN LOCATION.LOCATION L ON POS.LOCATION_ID = L.LOCATION_ID
LEFT OUTER JOIN DEVICE.DEVICE_SETTING DS_258 ON D.DEVICE_ID = DS_258.DEVICE_ID AND DS_258.DEVICE_SETTING_PARAMETER_CD = '258'
LEFT OUTER JOIN DEVICE.DEVICE_SETTING DS_260 ON D.DEVICE_ID = DS_260.DEVICE_ID AND DS_260.DEVICE_SETTING_PARAMETER_CD = '260'
WHERE D.DEVICE_ACTIVE_YN_FLAG = 'Y'
/

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/PSS/PKG_TRAN.psk?rev=1.24
CREATE OR REPLACE PACKAGE PSS.PKG_TRAN IS

PROCEDURE SP_CREATE_REFUND (
    pv_global_trans_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
    pv_orig_global_trans_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
    pn_refund_utc_ts_ms IN NUMBER,
    pn_orig_upload_utc_ts_ms IN NUMBER,
    pn_refund_amt PSS.REFUND.REFUND_AMT%TYPE,
    pn_refund_desc PSS.REFUND.REFUND_DESC%TYPE,
    pn_refund_issue_by PSS.REFUND.REFUND_ISSUE_BY%TYPE,
    pn_refund_type_cd PSS.REFUND.REFUND_TYPE_CD%TYPE,
    pc_entry_method_cd PSS.REFUND.ACCT_ENTRY_METHOD_CD%TYPE,
    pc_already_inserted_flag OUT VARCHAR2,
    pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
    pn_orig_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
    pn_minor_currency_factor OUT PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
    pn_payment_subtype_key_id OUT PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE,
    pv_payment_subtype_class OUT PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE
);

PROCEDURE sp_create_sale(
    pc_global_event_cd_prefix IN CHAR,
    pv_device_name IN device.device_name%TYPE,
    pv_device_tran_cd IN pss.tran.tran_device_tran_cd%TYPE,
    pn_device_batch_id IN pss.sale.device_batch_id%TYPE,
    pc_sale_type_cd IN pss.sale.sale_type_cd%TYPE,
    pn_sale_utc_ts_ms IN NUMBER,
    pn_sale_utc_offset_min IN pss.sale.sale_utc_offset_min%TYPE,
    pv_tran_device_result_type_cd IN pss.tran.tran_device_result_type_cd%TYPE,
    pn_sale_result_id IN pss.sale.sale_result_id%TYPE,
    pn_sale_amount IN NUMBER,
    pc_receipt_result_cd IN pss.sale.receipt_result_cd%TYPE,
    pc_tran_batch_type_cd IN pss.tran_line_item_batch_type.tran_line_item_batch_type_cd%TYPE,
    pv_hash_type_cd IN pss.sale.hash_type_cd%TYPE,
    pv_tran_line_item_hash IN pss.sale.tran_line_item_hash%TYPE,
    pn_session_id ENGINE.DEVICE_SESSION.DEVICE_SESSION_ID%TYPE,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_tran_id OUT pss.tran.tran_id%TYPE,
    pv_tran_state_cd OUT pss.tran.tran_state_cd%TYPE
);

PROCEDURE sp_create_tran_line_item
(
    pn_tran_id IN pss.tran_line_item.tran_id%TYPE,
    pn_host_port_num IN host.host_port_num%TYPE,
    pn_tli_type_id IN pss.tran_line_item.tran_line_item_type_id%TYPE,
    pn_tli_quantity IN pss.tran_line_item.tran_line_item_quantity%TYPE,    
    pn_tli_amount IN NUMBER,
    pn_tli_tax IN NUMBER,
    pv_tli_desc IN pss.tran_line_item.tran_line_item_desc%TYPE,    
    pn_tli_utc_ts_ms IN NUMBER,
    pn_tli_utc_offset_min IN NUMBER,
    pv_tli_position_cd IN pss.tran_line_item.tran_line_item_position_cd%TYPE,
    pc_tran_batch_type_cd IN pss.tran_line_item.tran_line_item_batch_type_cd%TYPE,
    pn_sale_result_id pss.tran_line_item.sale_result_id%TYPE DEFAULT NULL,
    pn_host_position_num host.host_position_num%TYPE DEFAULT 0
);

-- R26+ signature
PROCEDURE sp_finalize_sale
(
    pn_tran_id IN pss.tran.tran_id%TYPE,
    pn_session_id IN engine.device_session.device_session_id%TYPE,
    pv_tran_device_result_type_cd IN pss.tran.tran_device_result_type_cd%TYPE,
    pn_sale_result_id IN pss.sale.sale_result_id%TYPE,
    pn_sale_amount IN NUMBER,
    pn_sale_tax IN NUMBER,
    pc_tran_batch_type_cd IN pss.tran_line_item.tran_line_item_batch_type_cd%TYPE,
    pn_sale_utc_ts_ms IN NUMBER,
    pn_sale_duration_sec IN NUMBER,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pc_sale_type_cd pss.sale.sale_type_cd%TYPE DEFAULT NULL,
	pc_tran_import_needed OUT VARCHAR2
);

-- R25 signature
PROCEDURE sp_finalize_sale
(
    pn_tran_id IN pss.tran.tran_id%TYPE,
    pn_session_id IN engine.device_session.device_session_id%TYPE,
    pv_tran_device_result_type_cd IN pss.tran.tran_device_result_type_cd%TYPE,
    pn_sale_result_id IN pss.sale.sale_result_id%TYPE,
    pn_sale_amount IN NUMBER,
    pn_sale_tax IN NUMBER,
    pc_tran_batch_type_cd IN pss.tran_line_item.tran_line_item_batch_type_cd%TYPE,
    pn_sale_utc_ts_ms IN NUMBER,
    pn_sale_duration_sec IN NUMBER,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pc_sale_type_cd pss.sale.sale_type_cd%TYPE DEFAULT NULL
);

-- R29 signature
PROCEDURE SP_CREATE_AUTH(
   pv_global_event_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
   pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
   pn_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE,
   pv_device_event_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE,
   pc_invalid_device_event_cd CHAR,
   pn_tran_start_time NUMBER,
   pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
   pc_entry_method CHAR,
   pc_payment_type DEVICE_CALL_IN_RECORD.AUTH_CARD_TYPE%TYPE,
   pv_track_data PSS.TRAN.TRAN_RECEIVED_RAW_ACCT_DATA%TYPE,
   pv_auth_acct_data PSS.AUTH.AUTH_PARSED_ACCT_DATA%TYPE,
   pv_pan PSS.TRAN.TRAN_PARSED_ACCT_NUM%TYPE,
   pv_pan_sha1 PSS.TRAN_C.TRAN_PARSED_ACCT_NUM_H%TYPE,
   pv_card_holder PSS.TRAN.TRAN_PARSED_ACCT_NAME%TYPE,
   pv_expiration_date PSS.TRAN.TRAN_PARSED_ACCT_EXP_DATE%TYPE,
   pv_pin PSS.TRAN.TRAN_ACCOUNT_PIN%TYPE,
   pn_consumer_acct_id PSS.TRAN.CONSUMER_ACCT_ID%TYPE,
   pd_auth_ts PSS.AUTH.AUTH_TS%TYPE,
   pv_authority_resp_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
   pv_authority_resp_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
   pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
   pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
   pt_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
   pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
   pn_trace_number PSS.AUTH.TRACE_NUMBER%TYPE,
   pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
   pn_auth_amt PSS.AUTH.AUTH_AMT%TYPE,
   pn_balance_amt PSS.AUTH.AUTH_BALANCE_AMT%TYPE,
   pn_requested_amt PSS.AUTH.AUTH_AUTHORITY_AMT_RQST%TYPE,
   pn_received_amt PSS.AUTH.AUTH_AUTHORITY_AMT_RCVD%TYPE,
   pc_add_auth_hold CHAR,
   pc_auth_hold_used PSS.AUTH.AUTH_HOLD_USED%TYPE,
   pn_session_id DEVICE_CALL_IN_RECORD.SESSION_ID%TYPE,
   pc_ignore_dup CHAR,
   pn_auth_action_id PSS.AUTH.AUTH_ACTION_ID%TYPE,
   pn_auth_action_bitmap PSS.AUTH.AUTH_ACTION_BITMAP%TYPE,
   pc_sent_to_device CHAR,
   pc_pass_thru CHAR,
   pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
   pc_tran_state_cd OUT VARCHAR2,
   pc_tran_import_needed OUT VARCHAR2
);

-- R30 Signature
PROCEDURE SP_CREATE_AUTH(
   pv_global_event_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
   pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
   pn_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE,
   pv_device_event_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE,
   pc_invalid_device_event_cd CHAR,
   pn_tran_start_time NUMBER,
   pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
   pc_entry_method CHAR,
   pc_payment_type DEVICE_CALL_IN_RECORD.AUTH_CARD_TYPE%TYPE,
   pv_track_data PSS.TRAN.TRAN_RECEIVED_RAW_ACCT_DATA%TYPE,
   pv_auth_acct_data PSS.AUTH.AUTH_PARSED_ACCT_DATA%TYPE,
   pv_pan PSS.TRAN.TRAN_PARSED_ACCT_NUM%TYPE,
   pv_pan_sha1 PSS.TRAN_C.TRAN_PARSED_ACCT_NUM_H%TYPE,
   pv_card_holder PSS.TRAN.TRAN_PARSED_ACCT_NAME%TYPE,
   pv_expiration_date PSS.TRAN.TRAN_PARSED_ACCT_EXP_DATE%TYPE,
   pv_pin PSS.TRAN.TRAN_ACCOUNT_PIN%TYPE,
   pn_consumer_acct_id PSS.TRAN.CONSUMER_ACCT_ID%TYPE,
   pd_auth_ts PSS.AUTH.AUTH_TS%TYPE,
   pv_authority_resp_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
   pv_authority_resp_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
   pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
   pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
   pt_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
   pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
   pn_trace_number PSS.AUTH.TRACE_NUMBER%TYPE,
   pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
   pn_auth_amt PSS.AUTH.AUTH_AMT%TYPE,
   pn_balance_amt PSS.AUTH.AUTH_BALANCE_AMT%TYPE,
   pn_requested_amt PSS.AUTH.AUTH_AUTHORITY_AMT_RQST%TYPE,
   pn_received_amt PSS.AUTH.AUTH_AUTHORITY_AMT_RCVD%TYPE,
   pc_add_auth_hold CHAR,
   pc_auth_hold_used PSS.AUTH.AUTH_HOLD_USED%TYPE,
   pn_session_id DEVICE_CALL_IN_RECORD.SESSION_ID%TYPE,
   pc_ignore_dup CHAR,
   pn_auth_action_id PSS.AUTH.AUTH_ACTION_ID%TYPE,
   pn_auth_action_bitmap PSS.AUTH.AUTH_ACTION_BITMAP%TYPE,
   pc_sent_to_device CHAR,
   pc_pass_thru CHAR,
   pv_card_key PSS.AUTH.CARD_KEY%TYPE,
   pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
   pc_tran_state_cd OUT VARCHAR2,
   pc_tran_import_needed OUT VARCHAR2
);

PROCEDURE SP_INSERT_AUTH_STATS(
   pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
   pn_request_time NUMBER,
   pn_applayer_start_time NUMBER,
   pn_authority_start_time NUMBER,
   pn_authority_end_time NUMBER,
   pn_applayer_end_time NUMBER,
   pn_response_time NUMBER
);

PROCEDURE SP_PERMIT_CONSUMER_ACCT(
    pn_pos_pta_id PSS.POS_PTA.POS_PTA_ID%TYPE,
    pv_consumer_acct_cd PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD%TYPE,
    pt_auth_ts PSS.CONSUMER_ACCT.CONSUMER_ACCT_ACTIVATION_TS%TYPE,
    pv_currency_cd PSS.CURRENCY.CURRENCY_CD%TYPE,
    pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
    pn_action_id OUT PSS.PERMISSION_ACTION.ACTION_ID%TYPE,
    pn_action_code OUT NUMBER,
    pn_action_bitmap OUT NUMBER
);

PROCEDURE SP_CREATE_LOCAL_AUTH_SALE(
   pn_session_id DEVICE_CALL_IN_RECORD.SESSION_ID%TYPE,
   pc_global_event_cd_prefix IN CHAR,
   pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
   pn_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE,
   pv_device_tran_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE,
   pn_sale_utc_ts_ms NUMBER,
   pn_sale_utc_offset_min IN pss.sale.sale_utc_offset_min%TYPE,
   pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
   pc_entry_method CHAR,
   pc_payment_type DEVICE_CALL_IN_RECORD.AUTH_CARD_TYPE%TYPE,
   pv_track_data PSS.TRAN.TRAN_RECEIVED_RAW_ACCT_DATA%TYPE,
   pv_pan_sha1 PSS.TRAN_C.TRAN_PARSED_ACCT_NUM_H%TYPE,
   pn_consumer_acct_id PSS.TRAN.CONSUMER_ACCT_ID%TYPE,
   pd_auth_ts PSS.AUTH.AUTH_TS%TYPE,
   pn_trace_number PSS.AUTH.TRACE_NUMBER%TYPE,
   pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
   pn_auth_amt PSS.AUTH.AUTH_AMT%TYPE,
   pn_device_batch_id IN pss.sale.device_batch_id%TYPE,
   pc_sale_type_cd IN pss.sale.sale_type_cd%TYPE,
   pv_tran_device_result_type_cd IN pss.tran.tran_device_result_type_cd%TYPE,
   pn_sale_result_id IN pss.sale.sale_result_id%TYPE,
   pv_hash_type_cd IN pss.sale.hash_type_cd%TYPE,
   pv_tran_line_item_hash IN pss.sale.tran_line_item_hash%TYPE,
   pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
   pn_result_cd OUT NUMBER,
   pv_error_message OUT VARCHAR2
);
        
END;
/

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/PSS/PKG_TRAN.pbk?rev=1.76
CREATE OR REPLACE PACKAGE BODY PSS.PKG_TRAN IS

PROCEDURE SP_CREATE_REFUND (
    pv_global_trans_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
    pv_orig_global_trans_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
    pn_refund_utc_ts_ms IN NUMBER,
    pn_orig_upload_utc_ts_ms IN NUMBER,
    pn_refund_amt PSS.REFUND.REFUND_AMT%TYPE,
    pn_refund_desc PSS.REFUND.REFUND_DESC%TYPE,
    pn_refund_issue_by PSS.REFUND.REFUND_ISSUE_BY%TYPE,
    pn_refund_type_cd PSS.REFUND.REFUND_TYPE_CD%TYPE,
    pc_entry_method_cd PSS.REFUND.ACCT_ENTRY_METHOD_CD%TYPE,
    pc_already_inserted_flag OUT VARCHAR2,
    pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
    pn_orig_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
    pn_minor_currency_factor OUT PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
    pn_payment_subtype_key_id OUT PSS.POS_PTA.PAYMENT_SUBTYPE_KEY_ID%TYPE,
    pv_payment_subtype_class OUT PSS.PAYMENT_SUBTYPE.PAYMENT_SUBTYPE_CLASS%TYPE)
IS
    ld_orig_tran_upload_ts PSS.TRAN.TRAN_UPLOAD_TS%TYPE;
    ld_refund_ts PSS.TRAN.TRAN_START_TS%TYPE;
    lv_last_lock_utc_ts VARCHAR2(128);
    ln_cnt PLS_INTEGER;
    ln_orig_tran_id PSS.TRAN.TRAN_ID%TYPE;
    lv_lock_string VARCHAR2(100);
    ln_start INTEGER;
    ln_end INTEGER;
    lv_parsed_acct_data PSS.REFUND.REFUND_PARSED_ACCT_DATA%TYPE;
BEGIN
    ln_start := INSTR(pv_global_trans_cd, ':', 1, 1) + 1;
    ln_end := INSTR(pv_global_trans_cd, ':', 1, 3);
    IF ln_end <= 0 THEN
        ln_end := LENGTH(pv_global_trans_cd) + 1;
    END IF;
    lv_lock_string := SUBSTR(pv_global_trans_cd, ln_start,  ln_end -  ln_start);
    ld_orig_tran_upload_ts := DBADMIN.UTC_TO_LOCAL_DATE(DBADMIN.MILLIS_TO_TIMESTAMP(pn_orig_upload_utc_ts_ms));
    ld_refund_ts := DBADMIN.UTC_TO_LOCAL_DATE(DBADMIN.MILLIS_TO_TIMESTAMP(pn_refund_utc_ts_ms));
    lv_last_lock_utc_ts := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', lv_lock_string);
    -- check if refund already exists
    BEGIN
        SELECT X.TRAN_ID, c.MINOR_CURRENCY_FACTOR, pta.PAYMENT_SUBTYPE_KEY_ID, PST.PAYMENT_SUBTYPE_CLASS, 'Y'
          INTO pn_tran_id, pn_minor_currency_factor, pn_payment_subtype_key_id, pv_payment_subtype_class, pc_already_inserted_flag
          FROM PSS.TRAN X
          JOIN PSS.POS_PTA PTA ON PTA.POS_PTA_ID = X.POS_PTA_ID
          JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
          JOIN PSS.PAYMENT_SUBTYPE PST ON PTA.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
         WHERE X.TRAN_GLOBAL_TRANS_CD = pv_global_trans_cd;
        RETURN;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            pc_already_inserted_flag := 'N';
    END;
    -- Find original transaction
    BEGIN
        SELECT X.TRAN_ID, PSS.SEQ_TRAN_ID.NEXTVAL, c.MINOR_CURRENCY_FACTOR, pta.PAYMENT_SUBTYPE_KEY_ID, PST.PAYMENT_SUBTYPE_CLASS
          INTO pn_orig_tran_id, pn_tran_id, pn_minor_currency_factor, pn_payment_subtype_key_id, pv_payment_subtype_class
          FROM PSS.TRAN X
          JOIN PSS.POS_PTA PTA ON PTA.POS_PTA_ID = X.POS_PTA_ID
          JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
          JOIN PSS.PAYMENT_SUBTYPE PST ON PTA.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
         WHERE X.TRAN_GLOBAL_TRANS_CD = pv_orig_global_trans_cd
           AND X.TRAN_UPLOAD_TS = ld_orig_tran_upload_ts;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
             RAISE_APPLICATION_ERROR(-20381, 'Original Transaction ''' || pv_orig_global_trans_cd || ''', uploaded at ' || TO_CHAR(ld_orig_tran_upload_ts, 'MM/DD/YYYY HH24:MI:SS') || ' not found');
    END;
    
    INSERT INTO PSS.TRAN (
            TRAN_ID,
            PARENT_TRAN_ID,
            TRAN_START_TS,
            TRAN_END_TS,
            TRAN_UPLOAD_TS,
            TRAN_GLOBAL_TRANS_CD,
            TRAN_STATE_CD,
            CONSUMER_ACCT_ID,
            TRAN_DEVICE_TRAN_CD,
            POS_PTA_ID,
            TRAN_DEVICE_RESULT_TYPE_CD,
            TRAN_ACCOUNT_PIN,
            TRAN_RECEIVED_RAW_ACCT_DATA,
            TRAN_PARSED_ACCT_NAME,
            TRAN_PARSED_ACCT_EXP_DATE,
            TRAN_PARSED_ACCT_NUM,
            TRAN_REPORTABLE_ACCT_NUM,
            TRAN_PARSED_ACCT_NUM_HASH,
            TRAN_PARSED_ACCT_NUM_ENCR,
			PAYMENT_SUBTYPE_KEY_ID,
			PAYMENT_SUBTYPE_CLASS,
			CLIENT_PAYMENT_TYPE_CD,
			DEVICE_NAME
			)
     SELECT pn_tran_id,
            pn_orig_tran_id,
            ld_refund_ts,
            ld_refund_ts,
            NULL, /* Must be NULL so that PSSUpdater will not pick it up */
            pv_global_trans_cd,
            '8',
            O.CONSUMER_ACCT_ID,
            SUBSTR(pv_global_trans_cd, INSTR(pv_global_trans_cd, ':', 1, 2) + 1, LENGTH(pv_global_trans_cd)),
            O.POS_PTA_ID,
            O.TRAN_DEVICE_RESULT_TYPE_CD,
            O.TRAN_ACCOUNT_PIN,
            O.TRAN_RECEIVED_RAW_ACCT_DATA,
            O.TRAN_PARSED_ACCT_NAME,
            O.TRAN_PARSED_ACCT_EXP_DATE,
            O.TRAN_PARSED_ACCT_NUM,
            O.TRAN_REPORTABLE_ACCT_NUM,
            O.TRAN_PARSED_ACCT_NUM_HASH,
            O.TRAN_PARSED_ACCT_NUM_ENCR,
			pp.PAYMENT_SUBTYPE_KEY_ID,
			ps.PAYMENT_SUBTYPE_CLASS,
			ps.CLIENT_PAYMENT_TYPE_CD,
			d.DEVICE_NAME
	FROM PSS.TRAN O
	JOIN pss.pos_pta pp ON O.pos_pta_id = pp.pos_pta_id
	JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
	JOIN pss.pos p ON pp.pos_id = p.pos_id
	JOIN device.device d ON p.device_id = d.device_id
    WHERE O.TRAN_ID = pn_orig_tran_id;
    SELECT MAX(AUTH_PARSED_ACCT_DATA)
      INTO lv_parsed_acct_data
      FROM (SELECT AUTH_PARSED_ACCT_DATA
              FROM PSS.AUTH
             WHERE TRAN_ID = pn_orig_tran_id
               AND AUTH_PARSED_ACCT_DATA IS NOT NULL
             ORDER BY DECODE(AUTH_TYPE_CD, 'N', 1, 5), AUTH_RESULT_CD DESC, AUTH_TS, AUTH_ID)
     WHERE ROWNUM = 1;
    INSERT INTO PSS.REFUND (
            TRAN_ID,
            REFUND_AMT,
            REFUND_DESC,
            REFUND_ISSUE_TS,
            REFUND_ISSUE_BY,
            REFUND_TYPE_CD,
            REFUND_STATE_ID,
            REFUND_PARSED_ACCT_DATA,
            ACCT_ENTRY_METHOD_CD
        ) VALUES (
            pn_tran_id,
            -ABS(pn_refund_amt),
            pn_refund_desc,
            ld_refund_ts,
            pn_refund_issue_by,
            pn_refund_type_cd,
            6,
            lv_parsed_acct_data,
            pc_entry_method_cd);
END;

PROCEDURE sp_create_sale(
    pc_global_event_cd_prefix IN CHAR,
    pv_device_name IN device.device_name%TYPE,
    pv_device_tran_cd IN pss.tran.tran_device_tran_cd%TYPE,
    pn_device_batch_id IN pss.sale.device_batch_id%TYPE,
    pc_sale_type_cd IN pss.sale.sale_type_cd%TYPE,
    pn_sale_utc_ts_ms IN NUMBER,
    pn_sale_utc_offset_min IN pss.sale.sale_utc_offset_min%TYPE,
    pv_tran_device_result_type_cd IN pss.tran.tran_device_result_type_cd%TYPE,
    pn_sale_result_id IN pss.sale.sale_result_id%TYPE,
    pn_sale_amount IN NUMBER,
    pc_receipt_result_cd IN pss.sale.receipt_result_cd%TYPE,
    pc_tran_batch_type_cd IN pss.tran_line_item_batch_type.tran_line_item_batch_type_cd%TYPE,
    pv_hash_type_cd IN pss.sale.hash_type_cd%TYPE,
    pv_tran_line_item_hash IN pss.sale.tran_line_item_hash%TYPE,
    pn_session_id ENGINE.DEVICE_SESSION.DEVICE_SESSION_ID%TYPE,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pn_tran_id OUT pss.tran.tran_id%TYPE,
    pv_tran_state_cd OUT pss.tran.tran_state_cd%TYPE)
IS
/*
    Returned result codes:
        RESULT__SUCCESS
        RESULT__FAILURE
        RESULT__INVALID_PARAMETER
        RESULT__DUPLICATE
*/
    lv_global_trans_cd pss.tran.tran_global_trans_cd%TYPE;
    lv_global_trans_cd_app_layer pss.tran.tran_global_trans_cd%TYPE;
    lv_global_trans_cd_legacy pss.tran.tran_global_trans_cd%TYPE;
    ld_tran_upload_ts pss.tran.tran_upload_ts%TYPE;
    lt_sale_start_utc_ts pss.sale.sale_start_utc_ts%TYPE;
    ld_tran_start_ts pss.tran.tran_start_ts%TYPE;
    ld_original_tran_start_ts pss.tran.tran_start_ts%TYPE;
    lv_tran_parsed_acct_num pss.tran.tran_parsed_acct_num%TYPE;
    ld_tran_server_ts DATE;
    ln_device_id device.device_id%TYPE;
    ln_pos_pta_id pss.tran.pos_pta_id%TYPE;
    ld_current_ts DATE := SYSDATE;
    lc_client_payment_type_cd pss.client_payment_type.client_payment_type_cd%TYPE;
    ln_tran_exists NUMBER := PKG_CONST.BOOLEAN__FALSE;
    ln_tli_hash_match NUMBER;
    ln_insert_tran NUMBER := PKG_CONST.BOOLEAN__FALSE;
    ln_original_tran_id pss.tran.tran_id%TYPE;
    ln_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE;
    lc_sale_type_cd pss.sale.sale_type_cd%TYPE;
    lv_last_lock_utc_ts VARCHAR2(128);
    lc_auth_hold_used PSS.TRAN.AUTH_HOLD_USED%TYPE;
    lv_orig_tran_state_cd pss.tran.tran_state_cd%TYPE;
	ln_consumer_acct_id pss.tran.consumer_acct_id%TYPE;
BEGIN
    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
    pn_tran_id := 0;

    IF pc_global_event_cd_prefix NOT IN (PKG_CONST.EVENT_CODE_PREFIX__APP_LAYER,
        PKG_CONST.EVENT_CODE_PREFIX__LEGACY) THEN
        pn_result_cd := PKG_CONST.RESULT__INVALID_PARAMETER;
        pv_error_message := 'Invalid pc_global_event_cd_prefix: ' || pc_global_event_cd_prefix;
        RETURN;
    END IF;

    lv_global_trans_cd := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(pc_global_event_cd_prefix, pv_device_name, pv_device_tran_cd);
    lv_global_trans_cd_app_layer := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(PKG_CONST.EVENT_CODE_PREFIX__APP_LAYER, pv_device_name, pv_device_tran_cd);
    lv_global_trans_cd_legacy := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(PKG_CONST.EVENT_CODE_PREFIX__LEGACY, pv_device_name, pv_device_tran_cd);

    lt_sale_start_utc_ts := CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_sale_utc_ts_ms) AS DATE);
    ld_tran_start_ts := CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_sale_utc_ts_ms + pn_sale_utc_offset_min * 60 * 1000) AS DATE);
    ld_tran_server_ts := CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_sale_utc_ts_ms) - SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) + CURRENT_TIMESTAMP AS DATE);

    lv_last_lock_utc_ts := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', pv_device_name || ':' || pv_device_tran_cd);

    BEGIN
        SELECT tran_id, tran_state_cd, tran_start_ts, tran_upload_ts, PKG_CONST.BOOLEAN__TRUE, tli_hash_match, pos_pta_id, sale_type_cd, tran_parsed_acct_num, auth_hold_used, consumer_acct_id
        INTO pn_tran_id, pv_tran_state_cd, ld_original_tran_start_ts, ld_tran_upload_ts, ln_tran_exists, ln_tli_hash_match, ln_pos_pta_id, lc_sale_type_cd, lv_tran_parsed_acct_num, lc_auth_hold_used, ln_consumer_acct_id
        FROM
        (
            SELECT /*+ INDEX(t IDX_DEVICE_TRAN_CD) */ t.tran_id, t.tran_state_cd, t.tran_start_ts, t.tran_upload_ts,
                CASE WHEN s.hash_type_cd = pv_hash_type_cd
                    AND s.tran_line_item_hash = pv_tran_line_item_hash
                    AND s.sale_type_cd = pc_sale_type_cd THEN PKG_CONST.BOOLEAN__TRUE
                ELSE PKG_CONST.BOOLEAN__FALSE END AS tli_hash_match,
                t.pos_pta_id, s.sale_type_cd, t.tran_parsed_acct_num, NVL(t.auth_hold_used, 'N') auth_hold_used, t.consumer_acct_id
            FROM pss.tran t
			LEFT OUTER JOIN pss.sale s ON t.tran_id = s.tran_id
			WHERE t.tran_device_tran_cd = pv_device_tran_cd AND (
				t.tran_global_trans_cd IN (lv_global_trans_cd_app_layer, lv_global_trans_cd_legacy)
				OR t.tran_global_trans_cd LIKE lv_global_trans_cd_app_layer || ':%'
				OR t.tran_global_trans_cd LIKE lv_global_trans_cd_legacy || ':%'
			)
            ORDER BY CASE WHEN t.tran_global_trans_cd IN (lv_global_trans_cd_app_layer, lv_global_trans_cd_legacy) THEN 1 ELSE 2 END,
				CASE WHEN s.sale_type_cd = pc_sale_type_cd THEN 1 ELSE 2 END,
				tli_hash_match DESC, t.tran_start_ts, t.created_ts
        )
        WHERE ROWNUM = 1;
    
        ln_original_tran_id := pn_tran_id;
        lv_orig_tran_state_cd := pv_tran_state_cd;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            ln_insert_tran := PKG_CONST.BOOLEAN__TRUE;
    END;

    IF ln_tran_exists = PKG_CONST.BOOLEAN__TRUE AND (pc_sale_type_cd = PKG_CONST.SALE_TYPE__CASH OR ld_tran_upload_ts IS NOT NULL
        AND (pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED OR lc_sale_type_cd = pc_sale_type_cd)) THEN
        IF ln_tli_hash_match = PKG_CONST.BOOLEAN__TRUE OR pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED THEN
            UPDATE pss.sale
            SET duplicate_count = duplicate_count + 1
            WHERE tran_id = pn_tran_id;

            pv_tran_state_cd := PKG_CONST.TRAN_STATE__DUPLICATE;
            pn_result_cd := PKG_CONST.RESULT__DUPLICATE;
            pv_error_message := 'Duplicate sale, original tran_id: ' || pn_tran_id;
            pn_tran_id := 0;
            RETURN;
        END IF;

        ln_insert_tran := PKG_CONST.BOOLEAN__TRUE;
    END IF;

    IF ln_insert_tran = PKG_CONST.BOOLEAN__TRUE THEN
        IF pc_sale_type_cd = PKG_CONST.SALE_TYPE__CASH THEN
            lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CASH;
            pv_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE;
        ELSE
            lc_client_payment_type_cd := PKG_CONST.CLNT_PMNT_TYPE__CREDIT;
            IF ln_tran_exists = PKG_CONST.BOOLEAN__TRUE THEN
                SELECT CASE WHEN (lc_auth_hold_used = 'Y' OR NVL(SUM(DECODE(TLI.TRAN_LINE_ITEM_BATCH_TYPE_CD, 'I', TLI.TRAN_LINE_ITEM_AMOUNT * TLI.TRAN_LINE_ITEM_QUANTITY)), 0) > 0) AND NVL(SUM(DECODE(TLI.TRAN_LINE_ITEM_BATCH_TYPE_CD, 'A', TLI.TRAN_LINE_ITEM_AMOUNT * TLI.TRAN_LINE_ITEM_QUANTITY)), 0) = 0 
                            THEN PKG_CONST.TRAN_STATE__COMPLETE_ERROR
                            ELSE PKG_CONST.TRAN_STATE__DUPLICATE
                       END
                  INTO pv_tran_state_cd
                  FROM PSS.TRAN_LINE_ITEM TLI
                 WHERE TLI.TRAN_ID = pn_tran_id;
            ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED THEN
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND;
            ELSIF pn_sale_result_id != 0 /* Success */ AND NVL(pn_sale_amount, 0) = 0 THEN
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED;
            ELSE
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__SALE_NO_AUTH;
            END IF;
        END IF;

        SELECT pss.seq_tran_id.NEXTVAL INTO pn_tran_id FROM DUAL;

        IF ln_tran_exists = PKG_CONST.BOOLEAN__TRUE THEN
            lv_global_trans_cd := lv_global_trans_cd || ':' || pn_tran_id;
        END IF;
		
		ln_device_id := PKG_DEVICE_CONFIGURATION.GET_DEVICE_ID_BY_NAME(pv_device_name, ld_tran_server_ts);

        PKG_POS_PTA.SP_GET_OR_CREATE_POS_PTA(ln_device_id, pv_device_name, lc_client_payment_type_cd, lt_sale_start_utc_ts, pn_result_cd, pv_error_message, ln_pos_pta_id);

        INSERT INTO pss.tran (
            tran_id,
            tran_start_ts,
            tran_end_ts,
            tran_upload_ts,
            tran_state_cd,
            tran_device_tran_cd,
            pos_pta_id,
            tran_global_trans_cd,
            tran_device_result_type_cd,
			payment_subtype_key_id,
			payment_subtype_class,
			client_payment_type_cd,
			device_name
        ) SELECT
            pn_tran_id,
            ld_tran_start_ts,
            ld_tran_start_ts,
            ld_current_ts,
            pv_tran_state_cd,
            pv_device_tran_cd,
            ln_pos_pta_id,
            lv_global_trans_cd,
            pv_tran_device_result_type_cd,
			pp.payment_subtype_key_id,
			ps.payment_subtype_class,
			ps.client_payment_type_cd,
			pv_device_name
		FROM pss.pos_pta pp
		JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
		WHERE pp.pos_pta_id = ln_pos_pta_id;

        IF pv_tran_state_cd = PKG_CONST.TRAN_STATE__DUPLICATE THEN
            pn_result_cd := PKG_CONST.RESULT__DUPLICATE;
            pv_error_message := 'Duplicate sale with different line items, original tran_id: ' || ln_original_tran_id;
        END IF;
    ELSIF pc_sale_type_cd != PKG_CONST.SALE_TYPE__CASH THEN
        IF pv_tran_device_result_type_cd IN (PKG_CONST.TRAN_DEV_RES__CANCELLED,
                PKG_CONST.TRAN_DEV_RES__AUTH_FAILURE,
                PKG_CONST.TRAN_DEV_RES__FAILURE,
                PKG_CONST.TRAN_DEV_RES__TIMEOUT) OR pn_sale_amount <= 0 THEN
            IF pv_tran_state_cd IN (
                    PKG_CONST.TRAN_STATE__AUTH_SUCCESS,
                    PKG_CONST.TRAN_STATE__AUTH_SUCCESS_COND) THEN
                IF pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED THEN
                    pv_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH_INTENDED;
                ELSE
                    pv_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH;
                END IF;
            ELSIF pv_tran_state_cd IN (
                    PKG_CONST.TRAN_STATE__AUTH_EXPIRED,
                    PKG_CONST.TRAN_STATE__AUTH_COND_EXPIRED) THEN
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED; -- Reversal not available since auth is expired
            ELSIF pv_tran_state_cd IN (
                    PKG_CONST.TRAN_STATE__AUTH_DECLINE,
                    PKG_CONST.TRAN_STATE__AUTH_FAILURE) THEN
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED; -- We already determined that that no reversal is needed
            ELSIF pv_tran_state_cd IN (
                    PKG_CONST.TRAN_STATE__AUTH_REVERSED,
                    PKG_CONST.TRAN_STATE__AUTH_PEND_RVRSL,
                    PKG_CONST.TRAN_STATE__COMPLETE_ERROR,
                    PKG_CONST.TRAN_STATE__PROCESSED_TRAN,
                    PKG_CONST.TRAN_STATE__PROCESSING_TRAN,
                    PKG_CONST.TRAN_STATE__INCOMPLETE,
                    PKG_CONST.TRAN_STATE__INCOMPLETE_ERROR,
                    PKG_CONST.TRAN_STATE__PROCESSING_STTLMT,
                    PKG_CONST.TRAN_STATE__STLMT_INCOMPLETE,
                    PKG_CONST.TRAN_STATE__STLMT_ERROR) THEN
               -- don't change it
               NULL;
            ELSE                
                pv_error_message := 'Bad tran state for a cancelled cashless sale, tran_id: ' || pn_tran_id || ', tran_state_cd: ' || pv_tran_state_cd;
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED;
            END IF;
        ELSIF pv_tran_state_cd IN (
                PKG_CONST.TRAN_STATE__AUTH_EXPIRED,
                PKG_CONST.TRAN_STATE__AUTH_COND_EXPIRED)
             OR (pv_tran_state_cd IN (
                PKG_CONST.TRAN_STATE__AUTH_SUCCESS,
                PKG_CONST.TRAN_STATE__AUTH_SUCCESS_COND)
                AND ld_original_tran_start_ts < ld_current_ts - 8) THEN
            pv_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE_ERROR;
            
            IF lv_tran_parsed_acct_num != NULL THEN
                INSERT INTO PSS.TRAN_C(TRAN_ID, KID, HASH_TYPE_CD, TRAN_PARSED_ACCT_NUM_H)
                VALUES(pn_tran_id, -1, 'SHA1', DBADMIN.HASH_CARD(lv_tran_parsed_acct_num));
                
                UPDATE PSS.TRAN
                SET TRAN_PARSED_ACCT_NUM = NULL,
                    TRAN_PARSED_ACCT_NAME = NULL,
                    TRAN_PARSED_ACCT_EXP_DATE = NULL
                WHERE TRAN_ID = pn_tran_id;
            END IF;
        ELSIF pv_tran_state_cd IN (
                PKG_CONST.TRAN_STATE__AUTH_DECLINE,
                PKG_CONST.TRAN_STATE__AUTH_FAILURE,
                PKG_CONST.TRAN_STATE__AUTH_REVERSED,
                PKG_CONST.TRAN_STATE__AUTH_PEND_RVRSL,
                PKG_CONST.TRAN_STATE__INTENDED_ERROR) THEN
            pv_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE_ERROR;
            pv_error_message := 'Received a cashless sale for an unsuccessful auth, tran_id: ' || pn_tran_id || ', tran_state_cd: ' || pv_tran_state_cd;
        ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED
            AND pv_tran_device_result_type_cd IN (
                PKG_CONST.TRAN_DEV_RES__INCOMPLETE,
                PKG_CONST.TRAN_DEV_RES__SUCCESS,
                PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_PRNTR,
                PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_RCPT,
                PKG_CONST.TRAN_DEV_RES__SUCCESS_RCPT_ERR) THEN
            IF pv_tran_state_cd IN (PKG_CONST.TRAN_STATE__AUTH_SUCCESS, PKG_CONST.TRAN_STATE__AUTH_SUCCESS_COND) THEN
                -- normal case
                pv_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH_INTENDED;
                -- insert sale record
            ELSE
                -- sale actual uploaded
                -- don't change tran_state_cd
                -- don't update sale record
                pn_result_cd := PKG_CONST.RESULT__SUCCESS;
                pv_error_message := 'Actual uploaded before intended';
                RETURN;
            END IF;
        -- we must let POSM processed cancelled sales too to do auth reversal
        ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__ACTUAL
            AND pv_tran_state_cd IN (
                PKG_CONST.TRAN_STATE__AUTH_SUCCESS,
                PKG_CONST.TRAN_STATE__AUTH_SUCCESS_COND,
                PKG_CONST.TRAN_STATE__PROCESSING_BATCH,
                PKG_CONST.TRAN_STATE__PRCSNG_BATCH_INTD,
                PKG_CONST.TRAN_STATE__PRCSNG_BATCH_LOCAL,
                PKG_CONST.TRAN_STATE__PROCESSED_TRAN_INT,
                PKG_CONST.TRAN_STATE__BATCH_INTENDED)
            AND pv_tran_device_result_type_cd IN (
                PKG_CONST.TRAN_DEV_RES__AUTH_FAILURE,
                PKG_CONST.TRAN_DEV_RES__CANCELLED,
                PKG_CONST.TRAN_DEV_RES__FAILURE,
                PKG_CONST.TRAN_DEV_RES__INCOMPLETE,
                PKG_CONST.TRAN_DEV_RES__SUCCESS,
                PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_PRNTR,
                PKG_CONST.TRAN_DEV_RES__SUCCESS_NO_RCPT,
                PKG_CONST.TRAN_DEV_RES__SUCCESS_RCPT_ERR,
                PKG_CONST.TRAN_DEV_RES__TIMEOUT) THEN
            pv_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH;
        ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__ACTUAL
            AND pv_tran_state_cd = PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND THEN
            pv_tran_state_cd := PKG_CONST.TRAN_STATE__SALE_NO_AUTH;
        ELSIF pc_sale_type_cd = PKG_CONST.SALE_TYPE__ACTUAL
            AND pv_tran_state_cd = PKG_CONST.TRAN_STATE__PROCESSING_TRAN THEN
            NULL;-- don't change tran_state_cd
        ELSIF pv_tran_state_cd != PKG_CONST.TRAN_STATE__SALE_NO_AUTH THEN
             pv_error_message := 'Unusual tran state for a cashless sale, tran_id: ' || pn_tran_id || ', tran_state_cd: ' || pv_tran_state_cd;
        END IF;

        UPDATE pss.tran
        SET tran_state_cd = DECODE(TRAN_STATE_CD, lv_orig_tran_state_cd, pv_tran_state_cd, TRAN_STATE_CD), -- it might have changed if POSMLayer is processing it
            tran_end_ts = tran_start_ts,
            tran_upload_ts = ld_current_ts,
            tran_device_result_type_cd = pv_tran_device_result_type_cd
        WHERE tran_id = pn_tran_id;

        DELETE FROM pss.tran_line_item
        WHERE tran_id = pn_tran_id
            AND tran_line_item_batch_type_cd = pc_tran_batch_type_cd;
    END IF;

    SELECT c.MINOR_CURRENCY_FACTOR
      INTO ln_minor_currency_factor
      FROM PSS.POS_PTA PTA
      JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
     WHERE PTA.POS_PTA_ID = ln_pos_pta_id;

    UPDATE pss.sale
    SET device_batch_id = pn_device_batch_id,
        sale_type_cd = pc_sale_type_cd,
        sale_start_utc_ts = lt_sale_start_utc_ts,
        sale_end_utc_ts = lt_sale_start_utc_ts,
        sale_utc_offset_min = pn_sale_utc_offset_min,
        sale_result_id = pn_sale_result_id,
        sale_amount = pn_sale_amount / ln_minor_currency_factor,
        receipt_result_cd = pc_receipt_result_cd,
        hash_type_cd = pv_hash_type_cd,
        tran_line_item_hash = pv_tran_line_item_hash,
        sale_device_session_id = pn_session_id
    WHERE tran_id = pn_tran_id;

    IF SQL%NOTFOUND THEN
        INSERT INTO pss.sale (
            tran_id,
            device_batch_id,
            sale_type_cd,
            sale_start_utc_ts,
            sale_end_utc_ts,
            sale_utc_offset_min,
            sale_result_id,
            sale_amount,
            receipt_result_cd,
            hash_type_cd,
            tran_line_item_hash,
            sale_device_session_id
        ) VALUES (
            pn_tran_id,
            pn_device_batch_id,
            pc_sale_type_cd,
            lt_sale_start_utc_ts,
            lt_sale_start_utc_ts,
            pn_sale_utc_offset_min,
            pn_sale_result_id,
            pn_sale_amount / ln_minor_currency_factor,
            pc_receipt_result_cd,
            pv_hash_type_cd,
            pv_tran_line_item_hash,
            pn_session_id
        );
    END IF;
	
	IF pn_sale_result_id != 0 AND ln_consumer_acct_id IS NOT NULL AND lc_auth_hold_used = 'N' THEN
		UPDATE pss.last_device_action
		SET device_action_utc_ts = device_action_utc_ts - INTERVAL '1' YEAR
		WHERE device_name = pv_device_name
			AND consumer_acct_id = ln_consumer_acct_id;
	END IF;
END;

FUNCTION sf_find_host_id(
    pn_device_id DEVICE.DEVICE_ID%TYPE,
    pn_host_port_num IN HOST.HOST_PORT_NUM%TYPE,
    pn_host_position_num HOST.HOST_POSITION_NUM%TYPE)
  RETURN HOST.HOST_ID%TYPE
IS
    ln_host_id HOST.HOST_ID%TYPE;
BEGIN
    SELECT MAX(H.HOST_ID)
      INTO ln_host_id
      FROM DEVICE.HOST H
     WHERE H.DEVICE_ID = pn_device_id
       AND H.HOST_PORT_NUM = pn_host_port_num
       AND H.HOST_POSITION_NUM = pn_host_position_num;

    IF ln_host_id IS NULL THEN
        -- Use base host
        SELECT MAX(H.HOST_ID)
          INTO ln_host_id
          FROM DEVICE.HOST H
         WHERE H.DEVICE_ID = pn_device_id
           AND H.HOST_PORT_NUM = 0;
    END IF;
    
    RETURN ln_host_id;
END;

PROCEDURE sp_create_tran_line_item
(
    pn_tran_id IN pss.tran_line_item.tran_id%TYPE,
    pn_host_port_num IN host.host_port_num%TYPE,
    pn_tli_type_id IN pss.tran_line_item.tran_line_item_type_id%TYPE,
    pn_tli_quantity IN pss.tran_line_item.tran_line_item_quantity%TYPE,
    pn_tli_amount IN NUMBER,
    pn_tli_tax IN NUMBER,
    pv_tli_desc IN pss.tran_line_item.tran_line_item_desc%TYPE,
    pn_tli_utc_ts_ms IN NUMBER,
    pn_tli_utc_offset_min IN NUMBER,
    pv_tli_position_cd IN pss.tran_line_item.tran_line_item_position_cd%TYPE,
    pc_tran_batch_type_cd IN pss.tran_line_item.tran_line_item_batch_type_cd%TYPE,
    pn_sale_result_id pss.tran_line_item.sale_result_id%TYPE DEFAULT NULL,
    pn_host_position_num host.host_position_num%TYPE DEFAULT 0
)
IS
    ln_host_id pss.tran_line_item.host_id%TYPE;
    ln_device_id host.device_id%TYPE;
    ln_new_host_count NUMBER;
    ln_result_cd NUMBER;
    lv_error_message VARCHAR2(255);
    ln_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE;
    ln_tran_line_item_id pss.tran_line_item.tran_line_item_id%TYPE;
    ln_tli_desc pss.tran_line_item.tran_line_item_desc%TYPE;
    ln_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE;
	lc_tli_type_group_cd PSS.TRAN_LINE_ITEM_TYPE.TRAN_LINE_ITEM_TYPE_GROUP_CD%TYPE;
BEGIN
    SELECT POS.DEVICE_ID, c.MINOR_CURRENCY_FACTOR, D.DEVICE_TYPE_ID
      INTO ln_device_id, ln_minor_currency_factor, ln_device_type_id
      FROM PSS.POS POS
      JOIN DEVICE.DEVICE D ON POS.DEVICE_ID = D.DEVICE_ID
      JOIN PSS.POS_PTA PTA ON POS.POS_ID = PTA.POS_ID
      JOIN PSS.TRAN X ON PTA.POS_PTA_ID = X.POS_PTA_ID
      JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
    WHERE X.TRAN_ID = pn_tran_id;

    ln_host_id := sf_find_host_id(ln_device_id, pn_host_port_num, pn_host_position_num);
    IF ln_host_id IS NULL THEN
        -- create default hosts
        pkg_device_configuration.sp_create_default_hosts(ln_device_id, ln_new_host_count, ln_result_cd, lv_error_message);
        IF ln_result_cd != PKG_CONST.RESULT__SUCCESS THEN
            RETURN;
        END IF;
        ln_host_id := sf_find_host_id(ln_device_id, pn_host_port_num, pn_host_position_num);
    END IF;

    SELECT PSS.SEQ_TLI_ID.NEXTVAL
        INTO ln_tran_line_item_id
        FROM DUAL;

    -- For Kiosk type, use tran_line_item_type to find description
    IF ln_device_type_id = 11 THEN
        SELECT TRIM(SUBSTR(tran_line_item_type_desc || ' ' || pv_tli_desc, 1, 60))
          INTO ln_tli_desc
          FROM pss.tran_line_item_type
         WHERE tran_line_item_type_id = pn_tli_type_id;
    ELSIF ln_device_type_id = 5 THEN -- eSuds
        SELECT TLIT.TRAN_LINE_ITEM_TYPE_DESC || ', ' || CASE WHEN DTHT.DEVICE_TYPE_HOST_TYPE_CD IN('S', 'U', 'G', 'H', 'I', 'J') THEN DECODE(H.HOST_POSITION_NUM, 0, 'Bottom ', 1, 'Top ') END
                || GT.HOST_GROUP_TYPE_NAME || ' ' || H.HOST_LABEL_CD
          INTO ln_tli_desc
          FROM PSS.TRAN_LINE_ITEM_TYPE tlit
         CROSS JOIN DEVICE.HOST H
          JOIN DEVICE.DEVICE_TYPE_HOST_TYPE dtht ON DTHT.HOST_TYPE_ID = H.HOST_TYPE_ID AND DTHT.DEVICE_TYPE_ID = 5
          LEFT OUTER JOIN (DEVICE.HOST_TYPE_HOST_GROUP_TYPE HTGT
          JOIN DEVICE.HOST_GROUP_TYPE GT ON HTGT.HOST_GROUP_TYPE_ID = GT.HOST_GROUP_TYPE_ID)
            ON H.HOST_TYPE_ID = HTGT.HOST_TYPE_ID
         WHERE tlit.TRAN_LINE_ITEM_TYPE_ID = pn_tli_type_id
           AND H.HOST_ID = ln_host_id;
    ELSE
        ln_tli_desc := pv_tli_desc;
    END IF;

    INSERT INTO pss.tran_line_item (
        tran_line_item_id,
        tran_id,
        tran_line_item_amount,
        tran_line_item_position_cd,
        tran_line_item_tax,
        tran_line_item_type_id,
        tran_line_item_quantity,
        tran_line_item_desc,
        host_id,
        tran_line_item_batch_type_cd,
        tran_line_item_ts,
        sale_result_id
    )
    SELECT
        ln_tran_line_item_id,
        pn_tran_id,
        pn_tli_amount * CASE tran_line_item_type_sign_pn
            WHEN 'N' THEN -1
            ELSE 1
        END / ln_minor_currency_factor,
        pv_tli_position_cd,
        pn_tli_tax * CASE tran_line_item_type_sign_pn
            WHEN 'N' THEN -1
            ELSE 1
        END / ln_minor_currency_factor,
        pn_tli_type_id,
        pn_tli_quantity,
        ln_tli_desc,
        ln_host_id,
        pc_tran_batch_type_cd,
        CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_tli_utc_ts_ms + pn_tli_utc_offset_min * 60 * 1000) AS DATE),
        pn_sale_result_id
    FROM tran_line_item_type
    WHERE tran_line_item_type_id = pn_tli_type_id;

    -- For all device actual batch type only
    IF ln_host_id IS NOT NULL AND pc_tran_batch_type_cd = PKG_CONST.TRAN_BATCH_TYPE__ACTUAL THEN
		SELECT tran_line_item_type_group_cd
		INTO lc_tli_type_group_cd
		FROM pss.tran_line_item_type
		WHERE tran_line_item_type_id = pn_tli_type_id;
	
		IF lc_tli_type_group_cd IN ('P', 'S') THEN
			UPDATE PSS.TRAN_LINE_ITEM_RECENT
			SET tran_line_item_id = ln_tran_line_item_id,
				fkp_tran_id = pn_tran_id
			WHERE host_id = ln_host_id
				AND tran_line_item_type_id = pn_tli_type_id;
				
			IF SQL%NOTFOUND THEN
				BEGIN
					INSERT INTO PSS.TRAN_LINE_ITEM_RECENT (
						TRAN_LINE_ITEM_ID,
						HOST_ID,
						FKP_TRAN_ID,
						TRAN_LINE_ITEM_TYPE_ID
					) VALUES (
						ln_tran_line_item_id,
						ln_host_id,
						pn_tran_id,
						pn_tli_type_id
					);
				EXCEPTION
					WHEN DUP_VAL_ON_INDEX THEN
						UPDATE PSS.TRAN_LINE_ITEM_RECENT
						SET tran_line_item_id = ln_tran_line_item_id,
							fkp_tran_id = pn_tran_id
						WHERE host_id = ln_host_id
							AND tran_line_item_type_id = pn_tli_type_id;
				END;
			END IF;
		END IF;
    END IF;

END;

FUNCTION sf_tran_import_needed (
    pn_tran_id PSS.TRAN.TRAN_ID%TYPE
)
  RETURN VARCHAR2
IS
	lc_tran_import_needed VARCHAR2(1) := 'N';
BEGIN
	BEGIN
		SELECT DECODE(S.IMPORTED, 'Y', 'N', 'Y')
		INTO lc_tran_import_needed
		FROM PSS.TRAN T
		JOIN PSS.SALE S ON T.TRAN_ID = S.TRAN_ID
		WHERE T.TRAN_ID = pn_tran_id
			AND T.TRAN_STATE_CD != PKG_CONST.TRAN_STATE__DUPLICATE
			AND S.SALE_TYPE_CD != PKG_CONST.SALE_TYPE__INTENDED
			AND DECODE(S.SALE_TYPE_CD, PKG_CONST.SALE_TYPE__CASH, 'N', DBADMIN.PKG_UTL.EQL(T.TRAN_RECEIVED_RAW_ACCT_DATA, NULL)) = 'N';
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			NULL;
	END;
	RETURN lc_tran_import_needed;
END;

-- R26+ signature
PROCEDURE sp_finalize_sale
(
    pn_tran_id IN pss.tran.tran_id%TYPE,
    pn_session_id IN engine.device_session.device_session_id%TYPE,
    pv_tran_device_result_type_cd IN pss.tran.tran_device_result_type_cd%TYPE,
    pn_sale_result_id IN pss.sale.sale_result_id%TYPE,
    pn_sale_amount IN NUMBER,
    pn_sale_tax IN NUMBER,
    pc_tran_batch_type_cd IN pss.tran_line_item.tran_line_item_batch_type_cd%TYPE,
    pn_sale_utc_ts_ms IN NUMBER,
    pn_sale_duration_sec IN NUMBER,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pc_sale_type_cd pss.sale.sale_type_cd%TYPE DEFAULT NULL,
	pc_tran_import_needed OUT VARCHAR2
)
IS
/*
    Returned result codes:
        RESULT__SUCCESS
        RESULT__FAILURE
        RESULT__HOST_NOT_FOUND
*/
    ln_tli_total pss.tran_line_item.tran_line_item_amount%TYPE;
    ln_tli_count NUMBER;
    ln_adj_amt pss.tran_line_item.tran_line_item_amount%TYPE;
    ln_adj_tli pss.tran_line_item.tran_line_item_type_id%TYPE := -1;
    ln_base_host_id pss.tran_line_item.host_id%TYPE;
    lc_tli_batch_type_cd pss.tran_line_item.tran_line_item_batch_type_cd%TYPE;
    ln_new_host_count NUMBER;
    lc_client_payment_type_cd pss.client_payment_type.client_payment_type_cd%TYPE;
    ln_device_id DEVICE.DEVICE_ID%TYPE;
    ln_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE;
    ln_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE;
BEGIN
    pn_result_cd := PKG_CONST.RESULT__FAILURE;
    pv_error_message := PKG_CONST.ERROR__GENERIC_FAILURE;
	pc_tran_import_needed := 'N';

    IF pn_sale_duration_sec > 0 AND pc_tran_batch_type_cd = PKG_CONST.TRAN_BATCH_TYPE__ACTUAL THEN
        UPDATE pss.tran
        SET tran_end_ts = tran_start_ts + pn_sale_duration_sec / 86400
        WHERE tran_id = pn_tran_id;

        UPDATE pss.sale
        SET sale_end_utc_ts = sale_start_utc_ts + pn_sale_duration_sec / 86400
        WHERE tran_id = pn_tran_id;
    END IF;

    SELECT NVL(SUM((NVL(TRAN_LINE_ITEM_AMOUNT, 0) + NVL(TRAN_LINE_ITEM_TAX, 0)) * NVL(TRAN_LINE_ITEM_QUANTITY, 0)), 0),
           COUNT(1)
      INTO ln_tli_total, ln_tli_count
      FROM PSS.TRAN_LINE_ITEM
     WHERE TRAN_ID = pn_tran_id
       AND TRAN_LINE_ITEM_BATCH_TYPE_CD = pc_tran_batch_type_cd;

    SELECT POS.DEVICE_ID, c.MINOR_CURRENCY_FACTOR, PST.CLIENT_PAYMENT_TYPE_CD, D.DEVICE_TYPE_ID
      INTO ln_device_id, ln_minor_currency_factor, lc_client_payment_type_cd, ln_device_type_id
      FROM PSS.POS POS
      JOIN DEVICE.DEVICE D ON POS.DEVICE_ID = D.DEVICE_ID
      JOIN PSS.POS_PTA PTA ON POS.POS_ID = PTA.POS_ID
      JOIN PSS.TRAN X ON PTA.POS_PTA_ID = X.POS_PTA_ID
      JOIN PSS.CURRENCY c ON NVL(pta.CURRENCY_CD, 'USD') = c.CURRENCY_CD
      JOIN PSS.PAYMENT_SUBTYPE PST ON PTA.PAYMENT_SUBTYPE_ID = PST.PAYMENT_SUBTYPE_ID
    WHERE X.TRAN_ID = pn_tran_id;

    IF pv_tran_device_result_type_cd IN (PKG_CONST.TRAN_DEV_RES__CANCELLED,
            PKG_CONST.TRAN_DEV_RES__AUTH_FAILURE,
            PKG_CONST.TRAN_DEV_RES__FAILURE,
            PKG_CONST.TRAN_DEV_RES__TIMEOUT)
        OR NVL(pn_sale_result_id, -1) != PKG_CONST.SALE_RES__SUCCESS
        OR NVL(pn_sale_amount, 0) = 0 THEN
        IF ln_tli_total != 0 THEN
            ln_adj_tli := PKG_CONST.TLI__CANCELLATION_ADJMT;
            ln_adj_amt := -ln_tli_total;
        END IF;
    ELSE
        ln_adj_amt := NVL(pn_sale_amount / ln_minor_currency_factor, 0) - ln_tli_total;
        IF ln_adj_amt > 0 THEN
            ln_adj_tli := PKG_CONST.TLI__POS_DISCREPANCY_ADJMT;
        ELSIF ln_adj_amt < 0 THEN
            ln_adj_tli := PKG_CONST.TLI__NEG_DISCREPANCY_ADJMT;
        END IF;
    END IF;
    
    IF ln_device_type_id IN (0, 1) AND pc_sale_type_cd != PKG_CONST.SALE_TYPE__CASH THEN
		IF ln_tli_count > 0 AND ln_adj_amt != 0 THEN
            IF ln_adj_amt > 0 THEN
                -- create Transaction Amount Summary record
                INSERT INTO pss.tran_line_item (
                    tran_id,
                    host_id,
                    tran_line_item_type_id,
                    tran_line_item_amount,
                    tran_line_item_quantity,
                    tran_line_item_desc,
                    tran_line_item_batch_type_cd,
                    tran_line_item_tax)
                VALUES(
                    pn_tran_id,
                    ln_base_host_id,
                    201,
                    ln_adj_amt,
                    1,
                    'Transaction Amount Summary',
                    pc_tran_batch_type_cd,
                    pn_sale_tax);
            ELSE
                INSERT INTO pss.tran_line_item (
                    tran_id,
                    host_id,
                    tran_line_item_type_id,
                    tran_line_item_amount,
                    tran_line_item_quantity,
                    tran_line_item_desc,
                    tran_line_item_batch_type_cd,
                    tran_line_item_tax)
                SELECT
                    pn_tran_id,
                    ln_base_host_id,
                    tran_line_item_type_id,
                    ln_adj_amt,
                    1,
                    tran_line_item_type_desc,
                    pc_tran_batch_type_cd,
                    pn_sale_tax
                FROM pss.tran_line_item_type
                WHERE tran_line_item_type_id = PKG_CONST.TLI__NEG_DISCREPANCY_ADJMT;
            END IF;
		END IF;
   ELSE
        IF ln_adj_tli > -1 THEN
            -- use the base host for adjustments
            SELECT MAX(H.HOST_ID)
            INTO ln_base_host_id
            FROM DEVICE.HOST H
            WHERE H.DEVICE_ID = ln_device_id
            AND H.HOST_PORT_NUM = 0;
            IF ln_base_host_id IS NULL THEN
                pkg_device_configuration.sp_create_default_hosts(ln_device_id, ln_new_host_count, pn_result_cd, pv_error_message);
                IF pn_result_cd != PKG_CONST.RESULT__SUCCESS THEN
                    RETURN;
                END IF;
                SELECT H.HOST_ID
                INTO ln_base_host_id
                FROM DEVICE.HOST H
                WHERE H.DEVICE_ID = ln_device_id
                AND H.HOST_PORT_NUM = 0;
            END IF;
            INSERT INTO pss.tran_line_item (
                tran_id,
                host_id,
                tran_line_item_type_id,
                tran_line_item_amount,
                tran_line_item_quantity,
                tran_line_item_desc,
                tran_line_item_batch_type_cd,
                tran_line_item_tax)
            SELECT
                pn_tran_id,
                ln_base_host_id,
                ln_adj_tli,
                ln_adj_amt,
                1,
                tran_line_item_type_desc,
                pc_tran_batch_type_cd,
                pn_sale_tax
            FROM pss.tran_line_item_type
            WHERE tran_line_item_type_id = ln_adj_tli;
        END IF;
            
   END IF;

    pkg_call_in_record.sp_add_trans(pn_session_id, lc_client_payment_type_cd, pn_sale_amount / ln_minor_currency_factor, ln_tli_count);

	pc_tran_import_needed := sf_tran_import_needed(pn_tran_id);
    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;

-- R25 signature
PROCEDURE sp_finalize_sale
(
    pn_tran_id IN pss.tran.tran_id%TYPE,
    pn_session_id IN engine.device_session.device_session_id%TYPE,
    pv_tran_device_result_type_cd IN pss.tran.tran_device_result_type_cd%TYPE,
    pn_sale_result_id IN pss.sale.sale_result_id%TYPE,
    pn_sale_amount IN NUMBER,
    pn_sale_tax IN NUMBER,
    pc_tran_batch_type_cd IN pss.tran_line_item.tran_line_item_batch_type_cd%TYPE,
    pn_sale_utc_ts_ms IN NUMBER,
    pn_sale_duration_sec IN NUMBER,
    pn_result_cd OUT NUMBER,
    pv_error_message OUT VARCHAR2,
    pc_sale_type_cd pss.sale.sale_type_cd%TYPE DEFAULT NULL
)
IS
	lc_tran_import_needed VARCHAR2(1);
BEGIN
	sp_finalize_sale (
    pn_tran_id,
    pn_session_id,
    pv_tran_device_result_type_cd,
    pn_sale_result_id,
    pn_sale_amount,
    pn_sale_tax,
    pc_tran_batch_type_cd,
    pn_sale_utc_ts_ms,
    pn_sale_duration_sec,
    pn_result_cd,
    pv_error_message,
    pc_sale_type_cd,
	lc_tran_import_needed);
END;

-- R30 signature
PROCEDURE SP_CREATE_AUTH(
   pv_global_event_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
   pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
   pn_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE,
   pv_device_event_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE,
   pc_invalid_device_event_cd CHAR,
   pn_tran_start_time NUMBER,
   pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
   pc_entry_method CHAR,
   pc_payment_type DEVICE_CALL_IN_RECORD.AUTH_CARD_TYPE%TYPE,
   pv_track_data PSS.TRAN.TRAN_RECEIVED_RAW_ACCT_DATA%TYPE,
   pv_auth_acct_data PSS.AUTH.AUTH_PARSED_ACCT_DATA%TYPE,
   pv_pan PSS.TRAN.TRAN_PARSED_ACCT_NUM%TYPE,
   pv_pan_sha1 PSS.TRAN_C.TRAN_PARSED_ACCT_NUM_H%TYPE,
   pv_card_holder PSS.TRAN.TRAN_PARSED_ACCT_NAME%TYPE,
   pv_expiration_date PSS.TRAN.TRAN_PARSED_ACCT_EXP_DATE%TYPE,
   pv_pin PSS.TRAN.TRAN_ACCOUNT_PIN%TYPE,
   pn_consumer_acct_id PSS.TRAN.CONSUMER_ACCT_ID%TYPE,
   pd_auth_ts PSS.AUTH.AUTH_TS%TYPE,
   pv_authority_resp_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
   pv_authority_resp_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
   pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
   pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
   pt_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
   pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
   pn_trace_number PSS.AUTH.TRACE_NUMBER%TYPE,
   pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
   pn_auth_amt PSS.AUTH.AUTH_AMT%TYPE,
   pn_balance_amt PSS.AUTH.AUTH_BALANCE_AMT%TYPE,
   pn_requested_amt PSS.AUTH.AUTH_AUTHORITY_AMT_RQST%TYPE,
   pn_received_amt PSS.AUTH.AUTH_AUTHORITY_AMT_RCVD%TYPE,
   pc_add_auth_hold CHAR,
   pc_auth_hold_used PSS.AUTH.AUTH_HOLD_USED%TYPE,
   pn_session_id DEVICE_CALL_IN_RECORD.SESSION_ID%TYPE,
   pc_ignore_dup CHAR,
   pn_auth_action_id PSS.AUTH.AUTH_ACTION_ID%TYPE,
   pn_auth_action_bitmap PSS.AUTH.AUTH_ACTION_BITMAP%TYPE,
   pc_sent_to_device CHAR,
   pc_pass_thru CHAR,
   pv_card_key PSS.AUTH.CARD_KEY%TYPE,
   pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
   pc_tran_state_cd OUT VARCHAR2,
   pc_tran_import_needed OUT VARCHAR2
) IS
   ln_auth_id PSS.AUTH.AUTH_ID%TYPE;
   lc_sale_type_cd PSS.SALE.SALE_TYPE_CD%TYPE;
   ln_sale_result_id PSS.SALE.SALE_RESULT_ID%TYPE;
   ld_orig_trace_number PSS.AUTH.TRACE_NUMBER%TYPE;
   lv_last_lock_utc_ts VARCHAR2(128);
   lv_pan PSS.TRAN.TRAN_PARSED_ACCT_NUM%TYPE := pv_pan;
   lv_card_holder PSS.TRAN.TRAN_PARSED_ACCT_NAME%TYPE := pv_card_holder;
   lv_expiration_date PSS.TRAN.TRAN_PARSED_ACCT_EXP_DATE%TYPE := pv_expiration_date;
   lv_pin PSS.TRAN.TRAN_ACCOUNT_PIN%TYPE := pv_pin;
   lv_tran_reportable_acct_num PSS.TRAN.TRAN_REPORTABLE_ACCT_NUM%TYPE := SUBSTR(pv_pan, LENGTH(pv_pan) - 3, LENGTH(pv_pan));
   lc_invalid_device_event_cd CHAR := pc_invalid_device_event_cd;
   ld_tran_start_ts PSS.TRAN.TRAN_START_TS%TYPE := CAST(MILLIS_TO_TIMESTAMP(pn_tran_start_time) AS DATE);
   lv_global_trans_cd_app_layer pss.tran.tran_global_trans_cd%TYPE := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(PKG_CONST.EVENT_CODE_PREFIX__APP_LAYER, pv_device_name, pv_device_event_cd);
   lv_global_trans_cd_legacy pss.tran.tran_global_trans_cd%TYPE := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(PKG_CONST.EVENT_CODE_PREFIX__LEGACY, pv_device_name, pv_device_event_cd);
BEGIN
	pc_tran_import_needed := 'N';
    lv_last_lock_utc_ts := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', pv_device_name || ':' || pv_device_event_cd);
	
    BEGIN
        SELECT tran_id, sale_type_cd, tran_state_cd, trace_number, sale_result_id
        INTO pn_tran_id, lc_sale_type_cd, pc_tran_state_cd, ld_orig_trace_number, ln_sale_result_id
        FROM
        (
			SELECT /*+ INDEX(t IDX_DEVICE_TRAN_CD) */ t.tran_id, s.sale_type_cd, t.tran_state_cd, a.trace_number, s.sale_result_id
			FROM pss.tran t
			LEFT OUTER JOIN pss.sale s ON t.tran_id = s.tran_id
			LEFT OUTER JOIN pss.auth a ON t.tran_id = a.tran_id AND a.auth_type_cd = 'N'
			WHERE t.tran_device_tran_cd = pv_device_event_cd AND (
					t.tran_global_trans_cd IN (lv_global_trans_cd_app_layer, lv_global_trans_cd_legacy)
					OR t.tran_global_trans_cd LIKE lv_global_trans_cd_app_layer || ':%'
					OR t.tran_global_trans_cd LIKE lv_global_trans_cd_legacy || ':%'
				)
            ORDER BY CASE WHEN a.trace_number = pn_trace_number THEN 1 ELSE 2 END,
				CASE WHEN t.tran_global_trans_cd IN (lv_global_trans_cd_app_layer, lv_global_trans_cd_legacy) THEN 1 ELSE 2 END,
				t.tran_start_ts, t.created_ts, a.created_ts
        )
        WHERE ROWNUM = 1;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            pn_tran_id := NULL;
    END;
	
    IF pn_tran_id IS NOT NULL THEN
		IF ld_orig_trace_number = pn_trace_number THEN
			pc_tran_import_needed := sf_tran_import_needed(pn_tran_id);
            RETURN;
		END IF;
		
		IF pc_pass_thru = 'N' THEN -- This allows saving pass-thru auths
            IF pc_tran_state_cd IN(PKG_CONST.TRAN_STATE__SALE_NO_AUTH, PKG_CONST.TRAN_STATE__SALE_NO_AUTH_ERROR, PKG_CONST.TRAN_STATE__SALE_NO_AUTH_INTND)
                OR pc_tran_state_cd = PKG_CONST.TRAN_STATE__CLIENT_CANCELLED AND ln_sale_result_id != 0 /*Not 'Success'*/ THEN
				
				IF pc_auth_result_cd IN('Y', 'P') THEN
					IF lc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED THEN
						pc_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH_INTENDED;
					ELSE
						pc_tran_state_cd := PKG_CONST.TRAN_STATE__BATCH;
					END IF;
				ELSE
					IF lc_sale_type_cd = PKG_CONST.SALE_TYPE__INTENDED THEN
						pc_tran_state_cd := PKG_CONST.TRAN_STATE__INTENDED_ERROR;
					ELSE
						pc_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE_ERROR;
					END IF;
				END IF;
				
				UPDATE PSS.TRAN
				   SET (TRAN_START_TS,
						TRAN_END_TS,
						TRAN_STATE_CD,
						TRAN_ACCOUNT_PIN,
						TRAN_RECEIVED_RAW_ACCT_DATA,
						TRAN_PARSED_ACCT_NAME,
						TRAN_PARSED_ACCT_EXP_DATE,
						TRAN_PARSED_ACCT_NUM,
						TRAN_PARSED_ACCT_NUM_HASH,
						TRAN_REPORTABLE_ACCT_NUM,
						POS_PTA_ID,
						CONSUMER_ACCT_ID,
						AUTH_DEVICE_SESSION_ID,
						PAYMENT_SUBTYPE_KEY_ID,
						PAYMENT_SUBTYPE_CLASS,
						CLIENT_PAYMENT_TYPE_CD,						
						AUTH_HOLD_USED,
						DEVICE_NAME) =
					(SELECT
						ld_tran_start_ts,  /* TRAN_START_TS */
						TRAN_END_TS - TRAN_START_TS + ld_tran_start_ts,  /* TRAN_END_TS */
						pc_tran_state_cd,  /* TRAN_STATE_CD */
						lv_pin,  /* TRAN_ACCOUNT_PIN */
						pv_track_data,  /* TRAN_RECEIVED_RAW_ACCT_DATA */
						lv_card_holder,  /* TRAN_PARSED_ACCT_NAME */
						lv_expiration_date,  /* TRAN_PARSED_ACCT_EXP_DATE */
						lv_pan,  /* TRAN_PARSED_ACCT_NUM */
						pv_pan_sha1, /* TRAN_PARSED_ACCT_NUM_HASH */
						lv_tran_reportable_acct_num,  /* TRAN_REPORTABLE_ACCT_NUM */
						pn_pos_pta_id, /* POS_PTA_ID */
						pn_consumer_acct_id  /* CONSUMER_ACCT_ID */,
						pn_session_id,
						pp.payment_subtype_key_id,
						ps.payment_subtype_class,
						ps.client_payment_type_cd,
						pc_auth_hold_used,
						pv_device_name
					FROM pss.pos_pta pp
					JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
					WHERE pp.pos_pta_id = pn_pos_pta_id)
					WHERE TRAN_ID = pn_tran_id;
			ELSE
				pc_tran_state_cd := PKG_CONST.TRAN_STATE__DUPLICATE;
				lc_invalid_device_event_cd := 'Y';
				lv_pin := NULL;
				lv_card_holder := NULL;
				lv_expiration_date := NULL;
				lv_pan := NULL;			
			END IF;
        END IF;
	ELSE
        IF pc_auth_result_cd IN('Y', 'P') AND pc_sent_to_device = 'N' AND pc_auth_hold_used = 'Y' THEN
            pc_tran_state_cd := 'W'; -- Pending Reversal
        ELSIF pc_auth_result_cd IN('F') AND pc_auth_hold_used = 'Y' THEN
            pc_tran_state_cd := 'W'; -- Pending Reversal
        ELSIF pc_auth_result_cd IN('Y') THEN
            pc_tran_state_cd := '6'; -- Auth Success
        ELSIF pc_auth_result_cd IN('P') THEN
            pc_tran_state_cd := '0'; -- Auth Success Conditional
        ELSIF pc_auth_result_cd IN('N', 'O') THEN
            pc_tran_state_cd := '7'; -- Auth Decline
        ELSIF pc_auth_result_cd IN('F') THEN
            pc_tran_state_cd := '5'; -- Auth Failure
        END IF;
	END IF;
		
    IF pn_tran_id IS NULL OR pc_tran_state_cd = PKG_CONST.TRAN_STATE__DUPLICATE THEN
        SELECT PSS.SEQ_TRAN_ID.NEXTVAL
          INTO pn_tran_id
          FROM DUAL;    
        INSERT INTO PSS.TRAN (
            TRAN_ID,
            TRAN_START_TS,
			TRAN_END_TS,
            TRAN_STATE_CD,
            TRAN_DEVICE_TRAN_CD,
            TRAN_ACCOUNT_PIN,
            TRAN_RECEIVED_RAW_ACCT_DATA,
            TRAN_PARSED_ACCT_NAME,
            TRAN_PARSED_ACCT_EXP_DATE,
            TRAN_PARSED_ACCT_NUM,
            TRAN_PARSED_ACCT_NUM_HASH,
            TRAN_REPORTABLE_ACCT_NUM,
            POS_PTA_ID,
            TRAN_GLOBAL_TRANS_CD,
            CONSUMER_ACCT_ID,
            AUTH_DEVICE_SESSION_ID,
			PAYMENT_SUBTYPE_KEY_ID,
			PAYMENT_SUBTYPE_CLASS,
			CLIENT_PAYMENT_TYPE_CD,
			AUTH_HOLD_USED,
			DEVICE_NAME)
        SELECT
            pn_tran_id, /* TRAN_ID */
            ld_tran_start_ts,  /* TRAN_START_TS */
			ld_tran_start_ts,  /* TRAN_END_TS */
            pc_tran_state_cd,  /* TRAN_STATE_CD */
            pv_device_event_cd,  /* TRAN_DEVICE_TRAN_CD */
            lv_pin,  /* TRAN_ACCOUNT_PIN */
            pv_track_data,  /* TRAN_RECEIVED_RAW_ACCT_DATA */
            lv_card_holder,  /* TRAN_PARSED_ACCT_NAME */
            lv_expiration_date,  /* TRAN_PARSED_ACCT_EXP_DATE */
            lv_pan,  /* TRAN_PARSED_ACCT_NUM */
            pv_pan_sha1, /* TRAN_PARSED_ACCT_NUM_HASH */
            lv_tran_reportable_acct_num,  /* TRAN_REPORTABLE_ACCT_NUM */
            pn_pos_pta_id, /* POS_PTA_ID */
            DECODE(lc_invalid_device_event_cd, 'Y', pv_global_event_cd || ':' || pn_tran_id, pv_global_event_cd), /* TRAN_GLOBAL_TRANS_CD */
            pn_consumer_acct_id  /* CONSUMER_ACCT_ID */,
            pn_session_id,
			pp.payment_subtype_key_id,
			ps.payment_subtype_class,
			ps.client_payment_type_cd,
			pc_auth_hold_used,
			pv_device_name
		FROM pss.pos_pta pp
		JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
		WHERE pp.pos_pta_id = pn_pos_pta_id;
    END IF;

    SELECT PSS.SEQ_AUTH_ID.NEXTVAL
      INTO ln_auth_id
      FROM DUAL;
    INSERT INTO PSS.AUTH (
        AUTH_ID,
        TRAN_ID,
        AUTH_STATE_ID,
        AUTH_TYPE_CD,
        AUTH_PARSED_ACCT_DATA,
        ACCT_ENTRY_METHOD_CD,
        AUTH_TS,
        AUTH_RESULT_CD,
        AUTH_RESP_CD,
        AUTH_RESP_DESC,
        AUTH_AUTHORITY_TRAN_CD,
        AUTH_AUTHORITY_REF_CD,
        AUTH_AUTHORITY_TS,
        AUTH_AUTHORITY_MISC_DATA,
        AUTH_AMT,
        AUTH_AMT_APPROVED,
        AUTH_AUTHORITY_AMT_RQST,
        AUTH_AUTHORITY_AMT_RCVD,
        AUTH_BALANCE_AMT,
        TRACE_NUMBER,
        AUTH_ACTION_ID,
        AUTH_ACTION_BITMAP,
        AUTH_HOLD_USED,
        CARD_KEY)
     VALUES(
        ln_auth_id, /* AUTH_ID */
        pn_tran_id, /* TRAN_ID */
        DECODE(pc_auth_result_cd, 'Y', 2, 'N', 3, 'P', 5, 'O', 7, 'F', 4), /* AUTH_STATE_ID */
        'N', /* AUTH_TYPE_CD */
        pv_auth_acct_data, /* AUTH_PARSED_ACCT_DATA */
        DECODE(pc_entry_method, 'B', 7, 'C', 6, 'M', 2, 'S', 3, 1), /* ACCT_ENTRY_METHOD_CD */
        pd_auth_ts, /* AUTH_TS */
        pc_auth_result_cd, /* AUTH_RESULT_CD */
        pv_authority_resp_cd, /* AUTH_RESP_CD */
        pv_authority_resp_desc, /* AUTH_RESP_DESC */
        pv_authority_tran_cd, /* AUTH_AUTHORITY_TRAN_CD */
        pv_authority_ref_cd, /* AUTH_AUTHORITY_REF_CD */
        pt_authority_ts, /* AUTH_AUTHORITY_TS */
        pv_authority_misc_data, /* AUTH_AUTHORITY_MISC_DATA */
        NVL(pn_auth_amt / pn_minor_currency_factor, 0), /* AUTH_AMT */
        DECODE(pc_auth_result_cd, 'Y', pn_auth_amt, 'P', pn_received_amt) / pn_minor_currency_factor, /* AUTH_AMT_APPROVED */
        pn_requested_amt / pn_minor_currency_factor, /* AUTH_AUTHORITY_AMT_RQST */
        pn_received_amt / pn_minor_currency_factor,  /* AUTH_AUTHORITY_AMT_RCVD */
        pn_balance_amt / pn_minor_currency_factor, /* AUTH_BALANCE_AMT */
        pn_trace_number, /* TRACE_NUMBER */
        pn_auth_action_id,
        pn_auth_action_bitmap,
        pc_auth_hold_used,
        pv_card_key);

    IF pc_add_auth_hold = 'Y' THEN
        INSERT INTO PSS.CONSUMER_ACCT_AUTH_HOLD (CONSUMER_ACCT_ID, AUTH_ID, TRAN_ID)
          VALUES(pn_consumer_acct_id, ln_auth_id, pn_tran_id);
    END IF;
	
	pc_tran_import_needed := sf_tran_import_needed(pn_tran_id);
END;

-- R29 signature
PROCEDURE SP_CREATE_AUTH(
   pv_global_event_cd PSS.TRAN.TRAN_GLOBAL_TRANS_CD%TYPE,
   pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
   pn_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE,
   pv_device_event_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE,
   pc_invalid_device_event_cd CHAR,
   pn_tran_start_time NUMBER,
   pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
   pc_entry_method CHAR,
   pc_payment_type DEVICE_CALL_IN_RECORD.AUTH_CARD_TYPE%TYPE,
   pv_track_data PSS.TRAN.TRAN_RECEIVED_RAW_ACCT_DATA%TYPE,
   pv_auth_acct_data PSS.AUTH.AUTH_PARSED_ACCT_DATA%TYPE,
   pv_pan PSS.TRAN.TRAN_PARSED_ACCT_NUM%TYPE,
   pv_pan_sha1 PSS.TRAN_C.TRAN_PARSED_ACCT_NUM_H%TYPE,
   pv_card_holder PSS.TRAN.TRAN_PARSED_ACCT_NAME%TYPE,
   pv_expiration_date PSS.TRAN.TRAN_PARSED_ACCT_EXP_DATE%TYPE,
   pv_pin PSS.TRAN.TRAN_ACCOUNT_PIN%TYPE,
   pn_consumer_acct_id PSS.TRAN.CONSUMER_ACCT_ID%TYPE,
   pd_auth_ts PSS.AUTH.AUTH_TS%TYPE,
   pv_authority_resp_cd PSS.AUTH.AUTH_RESP_CD%TYPE,
   pv_authority_resp_desc PSS.AUTH.AUTH_RESP_DESC%TYPE,
   pv_authority_tran_cd PSS.AUTH.AUTH_AUTHORITY_TRAN_CD%TYPE,
   pv_authority_ref_cd PSS.AUTH.AUTH_AUTHORITY_REF_CD%TYPE,
   pt_authority_ts PSS.AUTH.AUTH_AUTHORITY_TS%TYPE,
   pv_authority_misc_data PSS.AUTH.AUTH_AUTHORITY_MISC_DATA%TYPE,
   pn_trace_number PSS.AUTH.TRACE_NUMBER%TYPE,
   pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
   pn_auth_amt PSS.AUTH.AUTH_AMT%TYPE,
   pn_balance_amt PSS.AUTH.AUTH_BALANCE_AMT%TYPE,
   pn_requested_amt PSS.AUTH.AUTH_AUTHORITY_AMT_RQST%TYPE,
   pn_received_amt PSS.AUTH.AUTH_AUTHORITY_AMT_RCVD%TYPE,
   pc_add_auth_hold CHAR,
   pc_auth_hold_used PSS.AUTH.AUTH_HOLD_USED%TYPE,
   pn_session_id DEVICE_CALL_IN_RECORD.SESSION_ID%TYPE,
   pc_ignore_dup CHAR,
   pn_auth_action_id PSS.AUTH.AUTH_ACTION_ID%TYPE,
   pn_auth_action_bitmap PSS.AUTH.AUTH_ACTION_BITMAP%TYPE,
   pc_sent_to_device CHAR,
   pc_pass_thru CHAR,
   pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
   pc_tran_state_cd OUT VARCHAR2,
   pc_tran_import_needed OUT VARCHAR2
) IS
BEGIN
	SP_CREATE_AUTH(
   pv_global_event_cd,
   pv_device_name,
   pn_pos_pta_id,
   pv_device_event_cd,
   pc_invalid_device_event_cd,
   pn_tran_start_time,
   pc_auth_result_cd,
   pc_entry_method,
   pc_payment_type,
   pv_track_data,
   pv_auth_acct_data,
   pv_pan,
   pv_pan_sha1,
   pv_card_holder,
   pv_expiration_date,
   pv_pin,
   pn_consumer_acct_id,
   pd_auth_ts,
   pv_authority_resp_cd,
   pv_authority_resp_desc,
   pv_authority_tran_cd,
   pv_authority_ref_cd,
   pt_authority_ts,
   pv_authority_misc_data,
   pn_trace_number,
   pn_minor_currency_factor,
   pn_auth_amt,
   pn_balance_amt,
   pn_requested_amt,
   pn_received_amt,
   pc_add_auth_hold,
   pc_auth_hold_used,
   pn_session_id,
   pc_ignore_dup,
   pn_auth_action_id,
   pn_auth_action_bitmap,
   pc_sent_to_device,
   pc_pass_thru,
   NULL,
   pn_tran_id,
   pc_tran_state_cd,
   pc_tran_import_needed); 
END;

PROCEDURE SP_INSERT_AUTH_STATS(
   pn_tran_id PSS.TRAN.TRAN_ID%TYPE,
   pn_request_time NUMBER,
   pn_applayer_start_time NUMBER,
   pn_authority_start_time NUMBER,
   pn_authority_end_time NUMBER,
   pn_applayer_end_time NUMBER,
   pn_response_time NUMBER
) IS
BEGIN
    INSERT INTO PSS.TRAN_STAT(TRAN_ID, TRAN_STAT_TYPE_ID, TRAN_STAT_VALUE)
      SELECT * FROM (
        SELECT pn_tran_id TRAN_ID, 2 /* live auth "POSM" time*/ TRAN_STAT_TYPE_ID, (pn_applayer_end_time - pn_applayer_start_time) / 1000 TRAN_STAT_VALUE FROM DUAL WHERE pn_applayer_start_time IS NOT NULL AND pn_applayer_end_time IS NOT NULL
        UNION ALL SELECT pn_tran_id, 4 /* live auth network time*/, (pn_response_time - pn_request_time) / 1000 FROM DUAL WHERE pn_request_time IS NOT NULL AND pn_response_time IS NOT NULL
        UNION ALL SELECT pn_tran_id, 1 /* live auth gateway time*/, (pn_authority_end_time - pn_authority_start_time) / 1000 FROM DUAL WHERE pn_authority_start_time IS NOT NULL AND pn_authority_end_time IS NOT NULL) a
     WHERE NOT EXISTS(SELECT 1 FROM PSS.TRAN_STAT TS WHERE A.TRAN_ID = TS.TRAN_ID AND A.TRAN_STAT_TYPE_ID = TS.TRAN_STAT_TYPE_ID);    
END;

PROCEDURE SP_PERMIT_CONSUMER_ACCT(
    pn_pos_pta_id PSS.POS_PTA.POS_PTA_ID%TYPE,
    pv_consumer_acct_cd PSS.CONSUMER_ACCT.CONSUMER_ACCT_CD%TYPE,
    pt_auth_ts PSS.CONSUMER_ACCT.CONSUMER_ACCT_ACTIVATION_TS%TYPE,
    pv_currency_cd PSS.CURRENCY.CURRENCY_CD%TYPE,
    pn_consumer_acct_id OUT PSS.CONSUMER_ACCT.CONSUMER_ACCT_ID%TYPE,
    pn_action_id OUT PSS.PERMISSION_ACTION.ACTION_ID%TYPE,
    pn_action_code OUT NUMBER,
    pn_action_bitmap OUT NUMBER
)
IS
    lc_store_action CHAR(1);
    lv_device_name DEVICE.DEVICE_NAME%TYPE;
    ln_device_id DEVICE.DEVICE_ID%TYPE;
    ln_device_type_id DEVICE.DEVICE_TYPE_ID%TYPE;
BEGIN
    SELECT CONSUMER_ACCT_ID, DEVICE_ID, DEVICE_TYPE_ID, DEVICE_NAME
      INTO pn_consumer_acct_id, ln_device_id, ln_device_type_id, lv_device_name
      FROM (
         SELECT CA.CONSUMER_ACCT_ID, D.DEVICE_ID, D.DEVICE_TYPE_ID, D.DEVICE_NAME, VLH.ANCESTOR_LOCATION_ID, MAX(CA.CONSUMER_ACCT_ISSUE_NUM) MAX_ISSUE_NUM, VLH.DEPTH
           FROM PSS.POS_PTA PTA
           JOIN PSS.POS POS ON PTA.POS_ID = POS.POS_ID
           JOIN DEVICE.DEVICE D ON D.DEVICE_ID = POS.DEVICE_ID
           JOIN LOCATION.VW_LOCATION_HIERARCHY VLH ON VLH.DESCENDENT_LOCATION_ID = POS.LOCATION_ID
           JOIN PSS.CONSUMER_ACCT CA ON VLH.ANCESTOR_LOCATION_ID = CA.LOCATION_ID
          WHERE PTA.POS_PTA_ID = pn_pos_pta_id
            AND CA.CONSUMER_ACCT_CD = pv_consumer_acct_cd
            AND CA.CONSUMER_ACCT_ACTIVE_YN_FLAG = 'Y'
            AND NVL(CA.CONSUMER_ACCT_ACTIVATION_TS, MIN_DATE) <= pt_auth_ts
            AND NVL(CA.CONSUMER_ACCT_DEACTIVATION_TS, MAX_DATE) > pt_auth_ts
            AND CA.CURRENCY_CD = pv_currency_cd
            GROUP BY CA.CONSUMER_ACCT_ID, VLH.ANCESTOR_LOCATION_ID, VLH.DEPTH, D.DEVICE_ID, D.DEVICE_TYPE_ID, D.DEVICE_NAME
            ORDER BY VLH.DEPTH, MAX_ISSUE_NUM DESC    /* DEPTH IS ASCENDING, AS IT IS THE DIFFERENCE BETWEEN LOCATION AND ANCESTOR */
    ) WHERE ROWNUM = 1;

    SELECT A.ACTION_ID, DTA.DEVICE_TYPE_ACTION_CD,
           DECODE(A.ACTION_PARAM_TYPE_CD, 'B', SUM(POWER(2, AP.PROTOCOL_BIT_INDEX))) PROTOCOL_BITMAP,
           DECODE(A.ACTION_CLEAR_PARAMETER_CD, NULL, 'N', 'Y') STORE_LAST_ACTION
      INTO pn_action_id, pn_action_code, pn_action_bitmap, lc_store_action
      FROM (SELECT * FROM (
         SELECT CAP.PERMISSION_ACTION_ID, DTA.DEVICE_TYPE_ACTION_CD PROTOCOL_ACTION_CD,
                CASE WHEN LDA.DEVICE_ACTION_UTC_TS IS NOT NULL
                          AND CURRENT_TIMESTAMP < LDA.DEVICE_ACTION_UTC_TS
                          + NUMTODSINTERVAL(COALESCE(TO_NUMBER_OR_NULL(DS_T.DEVICE_SETTING_VALUE),
                          TO_NUMBER_OR_NULL(cts.CONFIG_TEMPLATE_SETTING_VALUE), 3600), 'SECOND') THEN 10
                     ELSE DTA.ACTION_ID END ACTION_ID
           FROM PSS.CONSUMER_ACCT_PERMISSION CAP
           JOIN PSS.PERMISSION_ACTION PA ON CAP.PERMISSION_ACTION_ID = PA.PERMISSION_ACTION_ID
           JOIN DEVICE.DEVICE_TYPE_ACTION DTA ON DTA.ACTION_ID = PA.ACTION_ID
		   JOIN DEVICE.DEVICE_TYPE DT ON DTA.DEVICE_TYPE_ID = DT.DEVICE_TYPE_ID
           LEFT OUTER JOIN PSS.LAST_DEVICE_ACTION LDA
             ON LDA.DEVICE_NAME = lv_device_name
            AND CAP.CONSUMER_ACCT_ID = LDA.CONSUMER_ACCT_ID
            AND PA.ACTION_ID = LDA.DEVICE_ACTION_ID
           JOIN DEVICE.ACTION A ON PA.ACTION_ID = A.ACTION_ID
           LEFT OUTER JOIN DEVICE.DEVICE_SETTING DS_T ON ln_device_id = DS_T.DEVICE_ID AND DS_T.DEVICE_SETTING_PARAMETER_CD = A.ACTION_CLEAR_PARAMETER_CD
           LEFT OUTER JOIN DEVICE.DEVICE_SETTING ds_v ON ln_device_id = ds_v.DEVICE_ID AND ds_v.DEVICE_SETTING_PARAMETER_CD = 'Property List Version'
           LEFT OUTER JOIN DEVICE.CONFIG_TEMPLATE ct
             ON ct.CONFIG_TEMPLATE_NAME = DECODE(DBADMIN.PKG_UTL.COMPARE(DT.DEVICE_TYPE_ID, 13),
				-1, DT.DEFAULT_CONFIG_TEMPLATE_NAME, DT.DEFAULT_CONFIG_TEMPLATE_NAME || ds_v.DEVICE_SETTING_VALUE)
           LEFT OUTER JOIN DEVICE.CONFIG_TEMPLATE_SETTING cts
             ON cts.CONFIG_TEMPLATE_ID = ct.CONFIG_TEMPLATE_ID
            AND cts.DEVICE_SETTING_PARAMETER_CD = A.ACTION_CLEAR_PARAMETER_CD
          WHERE CAP.CONSUMER_ACCT_ID = pn_consumer_acct_id
            AND DTA.DEVICE_TYPE_ID = ln_device_type_id
          ORDER BY CAP.CONSUMER_ACCT_PERMISSION_ORDER
      ) WHERE ROWNUM = 1) O
      LEFT OUTER JOIN (PSS.PERMISSION_ACTION_PARAM PAP
      JOIN DEVICE.ACTION_PARAM AP ON PAP.ACTION_PARAM_ID = AP.ACTION_PARAM_ID)
        ON O.PERMISSION_ACTION_ID = PAP.PERMISSION_ACTION_ID
      JOIN DEVICE.ACTION A ON O.ACTION_ID = A.ACTION_ID
      JOIN DEVICE.DEVICE_TYPE_ACTION DTA ON DTA.ACTION_ID = O.ACTION_ID
     WHERE DTA.DEVICE_TYPE_ID = ln_device_type_id
      GROUP BY A.ACTION_ID, DTA.DEVICE_TYPE_ACTION_CD, A.ACTION_CLEAR_PARAMETER_CD, A.ACTION_PARAM_TYPE_CD;
    IF lc_store_action = 'Y' THEN
        MERGE INTO PSS.LAST_DEVICE_ACTION O
         USING (
              SELECT lv_device_name DEVICE_NAME,
                     pn_consumer_acct_id CONSUMER_ACCT_ID,
                     pn_action_id DEVICE_ACTION_ID,
                     CURRENT_TIMESTAMP DEVICE_ACTION_UTC_TS
                FROM DUAL) N
              ON (O.DEVICE_NAME = N.DEVICE_NAME)
              WHEN MATCHED THEN
               UPDATE
                  SET O.CONSUMER_ACCT_ID = N.CONSUMER_ACCT_ID,
                      O.DEVICE_ACTION_ID = N.DEVICE_ACTION_ID,
                      O.DEVICE_ACTION_UTC_TS = N.DEVICE_ACTION_UTC_TS
              WHEN NOT MATCHED THEN
               INSERT (O.DEVICE_NAME,
                       O.CONSUMER_ACCT_ID,
                       O.DEVICE_ACTION_ID,
                       O.DEVICE_ACTION_UTC_TS)
                VALUES(N.DEVICE_NAME,
                       N.CONSUMER_ACCT_ID,
                       N.DEVICE_ACTION_ID,
                       N.DEVICE_ACTION_UTC_TS
                );
    END IF;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN;
END;

PROCEDURE SP_CREATE_LOCAL_AUTH_SALE(
   pn_session_id DEVICE_CALL_IN_RECORD.SESSION_ID%TYPE,
   pc_global_event_cd_prefix IN CHAR,
   pv_device_name IN DEVICE.DEVICE_NAME%TYPE,
   pn_pos_pta_id PSS.TRAN.POS_PTA_ID%TYPE,
   pv_device_tran_cd PSS.TRAN.TRAN_DEVICE_TRAN_CD%TYPE,
   pn_sale_utc_ts_ms NUMBER,
   pn_sale_utc_offset_min IN pss.sale.sale_utc_offset_min%TYPE,
   pc_auth_result_cd PSS.AUTH.AUTH_RESULT_CD%TYPE,
   pc_entry_method CHAR,
   pc_payment_type DEVICE_CALL_IN_RECORD.AUTH_CARD_TYPE%TYPE,
   pv_track_data PSS.TRAN.TRAN_RECEIVED_RAW_ACCT_DATA%TYPE,
   pv_pan_sha1 PSS.TRAN_C.TRAN_PARSED_ACCT_NUM_H%TYPE,
   pn_consumer_acct_id PSS.TRAN.CONSUMER_ACCT_ID%TYPE,
   pd_auth_ts PSS.AUTH.AUTH_TS%TYPE,
   pn_trace_number PSS.AUTH.TRACE_NUMBER%TYPE,
   pn_minor_currency_factor PSS.CURRENCY.MINOR_CURRENCY_FACTOR%TYPE,
   pn_auth_amt PSS.AUTH.AUTH_AMT%TYPE,
   pn_device_batch_id IN pss.sale.device_batch_id%TYPE,
   pc_sale_type_cd IN pss.sale.sale_type_cd%TYPE,
   pv_tran_device_result_type_cd IN pss.tran.tran_device_result_type_cd%TYPE,
   pn_sale_result_id IN pss.sale.sale_result_id%TYPE,
   pv_hash_type_cd IN pss.sale.hash_type_cd%TYPE,
   pv_tran_line_item_hash IN pss.sale.tran_line_item_hash%TYPE,
   pn_tran_id OUT PSS.TRAN.TRAN_ID%TYPE,
   pn_result_cd OUT NUMBER,
   pv_error_message OUT VARCHAR2
) IS
   ln_auth_id PSS.AUTH.AUTH_ID%TYPE;
   lv_global_trans_cd pss.tran.tran_global_trans_cd%TYPE;
   lv_global_trans_cd_app_layer pss.tran.tran_global_trans_cd%TYPE;
   lv_global_trans_cd_legacy pss.tran.tran_global_trans_cd%TYPE;
   ld_tran_upload_ts pss.tran.tran_upload_ts%TYPE;
   ln_tran_exists NUMBER := PKG_CONST.BOOLEAN__FALSE;
   ln_tli_hash_match NUMBER;
   ld_current_ts DATE := SYSDATE;
   ld_tran_start_ts pss.tran.tran_start_ts%TYPE;
   lt_sale_start_utc_ts pss.sale.sale_start_utc_ts%TYPE;
   lv_last_lock_utc_ts VARCHAR2(128);
   ln_insert_tran NUMBER := PKG_CONST.BOOLEAN__FALSE;
   lv_tran_state_cd pss.tran.tran_state_cd%TYPE := PKG_CONST.TRAN_STATE__COMPLETE_ERROR;
BEGIN
    IF pc_global_event_cd_prefix NOT IN (PKG_CONST.EVENT_CODE_PREFIX__APP_LAYER,
        PKG_CONST.EVENT_CODE_PREFIX__LEGACY) THEN
        pn_result_cd := PKG_CONST.RESULT__INVALID_PARAMETER;
        pv_error_message := 'Invalid pc_global_event_cd_prefix: ' || pc_global_event_cd_prefix;
        RETURN;
    END IF;

    lv_global_trans_cd := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(pc_global_event_cd_prefix, pv_device_name, pv_device_tran_cd);
    lv_global_trans_cd_app_layer := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(PKG_CONST.EVENT_CODE_PREFIX__APP_LAYER, pv_device_name, pv_device_tran_cd);
    lv_global_trans_cd_legacy := PKG_EVENT.SF_GET_GLOBAL_EVENT_CD(PKG_CONST.EVENT_CODE_PREFIX__LEGACY, pv_device_name, pv_device_tran_cd);
    ld_tran_start_ts := CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_sale_utc_ts_ms + pn_sale_utc_offset_min * 60 * 1000) AS DATE);
    lt_sale_start_utc_ts := CAST(DBADMIN.MILLIS_TO_TIMESTAMP(pn_sale_utc_ts_ms) AS DATE);
    lv_last_lock_utc_ts := PKG_GLOBAL.REQUEST_LOCK('PSS.TRAN', pv_device_name || ':' || pv_device_tran_cd);
    BEGIN
        SELECT tran_id, tran_upload_ts, PKG_CONST.BOOLEAN__TRUE, tli_hash_match
        INTO pn_tran_id, ld_tran_upload_ts, ln_tran_exists, ln_tli_hash_match
        FROM
        (
            SELECT /*+ INDEX(t IDX_DEVICE_TRAN_CD) */ t.tran_id, t.tran_upload_ts,
                CASE WHEN s.hash_type_cd = pv_hash_type_cd
                    AND s.tran_line_item_hash = pv_tran_line_item_hash
                    AND s.sale_type_cd = pc_sale_type_cd THEN PKG_CONST.BOOLEAN__TRUE
                ELSE PKG_CONST.BOOLEAN__FALSE END AS tli_hash_match
            FROM pss.tran t
            LEFT OUTER JOIN pss.sale s ON t.tran_id = s.tran_id
			WHERE t.tran_device_tran_cd = pv_device_tran_cd AND (
				t.tran_global_trans_cd IN (lv_global_trans_cd_app_layer, lv_global_trans_cd_legacy)
				OR t.tran_global_trans_cd LIKE lv_global_trans_cd_app_layer || ':%'
				OR t.tran_global_trans_cd LIKE lv_global_trans_cd_legacy || ':%'
			)
            ORDER BY CASE WHEN t.tran_global_trans_cd IN (lv_global_trans_cd_app_layer, lv_global_trans_cd_legacy) THEN 1 ELSE 2 END,
				CASE WHEN s.sale_type_cd = pc_sale_type_cd THEN 1 ELSE 2 END,
				tli_hash_match DESC, t.tran_start_ts, t.created_ts
        )
        WHERE ROWNUM = 1;

    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            ln_insert_tran := PKG_CONST.BOOLEAN__TRUE;
    END;

    IF ln_tran_exists = PKG_CONST.BOOLEAN__TRUE AND ld_tran_upload_ts IS NOT NULL THEN
        IF ln_tli_hash_match = PKG_CONST.BOOLEAN__TRUE THEN
            UPDATE pss.sale
            SET duplicate_count = duplicate_count + 1
            WHERE tran_id = pn_tran_id;

            pn_result_cd := PKG_CONST.RESULT__DUPLICATE;
            pv_error_message := 'Duplicate sale, original tran_id: ' || pn_tran_id;
            pn_tran_id := 0;
            RETURN;
        END IF;
	
		ln_insert_tran := PKG_CONST.BOOLEAN__TRUE;
    END IF;

    IF ln_insert_tran = PKG_CONST.BOOLEAN__TRUE THEN
        IF pc_sale_type_cd = PKG_CONST.SALE_TYPE__CASH THEN
			lv_tran_state_cd := PKG_CONST.TRAN_STATE__COMPLETE;
		ELSIF pv_tran_device_result_type_cd IN (PKG_CONST.TRAN_DEV_RES__CANCELLED,
                PKG_CONST.TRAN_DEV_RES__AUTH_FAILURE,
                PKG_CONST.TRAN_DEV_RES__FAILURE,
                PKG_CONST.TRAN_DEV_RES__TIMEOUT) OR pn_auth_amt <= 0 THEN
			lv_tran_state_cd := PKG_CONST.TRAN_STATE__CLIENT_CANCELLED;
		END IF;
		
		SELECT PSS.SEQ_TRAN_ID.NEXTVAL
		INTO pn_tran_id
		FROM DUAL;

        IF ln_tran_exists = PKG_CONST.BOOLEAN__TRUE THEN
            lv_global_trans_cd := lv_global_trans_cd || ':' || pn_tran_id;
        END IF;		

		INSERT INTO PSS.TRAN (
			TRAN_ID,
			TRAN_START_TS,
			TRAN_END_TS,
			TRAN_UPLOAD_TS,
			TRAN_STATE_CD,
			TRAN_DEVICE_TRAN_CD,
			TRAN_RECEIVED_RAW_ACCT_DATA,
			TRAN_PARSED_ACCT_NUM_HASH,
			TRAN_REPORTABLE_ACCT_NUM,
			POS_PTA_ID,
			TRAN_GLOBAL_TRANS_CD,
			CONSUMER_ACCT_ID,
			TRAN_DEVICE_RESULT_TYPE_CD,
			PAYMENT_SUBTYPE_KEY_ID,
			PAYMENT_SUBTYPE_CLASS,
			CLIENT_PAYMENT_TYPE_CD,
			DEVICE_NAME)
		SELECT
			pn_tran_id, /* TRAN_ID */
			ld_tran_start_ts,  /* TRAN_START_TS */
			ld_tran_start_ts, /* TRAN_END_TS */
			ld_current_ts,
			lv_tran_state_cd,  /* TRAN_STATE_CD */
			pv_device_tran_cd,  /* TRAN_DEVICE_TRAN_CD */
			pv_track_data,  /* TRAN_RECEIVED_RAW_ACCT_DATA */
			pv_pan_sha1, /* TRAN_PARSED_ACCT_NUM_HASH */
			SUBSTR(pv_track_data, -4, 4),  /* TRAN_REPORTABLE_ACCT_NUM */
			pn_pos_pta_id, /* POS_PTA_ID */
			lv_global_trans_cd, /* TRAN_GLOBAL_TRANS_CD */
			pn_consumer_acct_id,  /* CONSUMER_ACCT_ID */
			pv_tran_device_result_type_cd,
			pp.payment_subtype_key_id,
			ps.payment_subtype_class,
			ps.client_payment_type_cd,
			pv_device_name
		FROM pss.pos_pta pp
		JOIN pss.payment_subtype ps ON pp.payment_subtype_id = ps.payment_subtype_id
		WHERE pp.pos_pta_id = pn_pos_pta_id;

		SELECT PSS.SEQ_AUTH_ID.NEXTVAL
		  INTO ln_auth_id
		  FROM DUAL;
		INSERT INTO PSS.AUTH (
			AUTH_ID,
			TRAN_ID,
			AUTH_STATE_ID,
			AUTH_TYPE_CD,
			AUTH_PARSED_ACCT_DATA,
			ACCT_ENTRY_METHOD_CD,
			AUTH_TS,
			AUTH_RESULT_CD,
			AUTH_RESP_CD,
			AUTH_RESP_DESC,
			AUTH_AUTHORITY_TS,
			AUTH_AMT,
			AUTH_AUTHORITY_AMT_RQST,
			TRACE_NUMBER)
		 VALUES(
			ln_auth_id, /* AUTH_ID */
			pn_tran_id, /* TRAN_ID */
			DECODE(pc_auth_result_cd, 'Y', 2, 'N', 3, 'P', 5, 'O', 7, 'F', 4), /* AUTH_STATE_ID */
			'L', /* AUTH_TYPE_CD */
			pv_track_data, /* AUTH_PARSED_ACCT_DATA */
			DECODE(pc_entry_method, 'B', 7, 'C', 6, 'M', 2, 'S', 3, 1), /* ACCT_ENTRY_METHOD_CD */
			pd_auth_ts, /* AUTH_TS */
			pc_auth_result_cd, /* AUTH_RESULT_CD */
			'LOCAL', /* AUTH_RESP_CD */
			'Local authorization not accepted', /* AUTH_RESP_DESC */
			pd_auth_ts, /* AUTH_AUTHORITY_TS */
			NVL(pn_auth_amt / pn_minor_currency_factor, 0), /* AUTH_AMT */
			pn_auth_amt / pn_minor_currency_factor, /* AUTH_AUTHORITY_AMT_RQST */
			pn_trace_number /* TRACE_NUMBER */
			);

		INSERT INTO pss.sale (
			tran_id,
			device_batch_id,
			sale_type_cd,
			sale_start_utc_ts,
			sale_end_utc_ts,
			sale_utc_offset_min,
			sale_result_id,
			sale_amount,
			receipt_result_cd,
			hash_type_cd,
			tran_line_item_hash
		) VALUES (
			pn_tran_id,
			pn_device_batch_id,
			pc_sale_type_cd,
			lt_sale_start_utc_ts,
			lt_sale_start_utc_ts,
			pn_sale_utc_offset_min,
			pn_sale_result_id,
			pn_auth_amt / pn_minor_currency_factor,
			'U',
			pv_hash_type_cd,
			pv_tran_line_item_hash
		);
	END IF;

    pn_result_cd := PKG_CONST.RESULT__SUCCESS;
    pv_error_message := PKG_CONST.ERROR__NO_ERROR;
END;
END;
/

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/DMS/DMS-1.0.5-USADBP.2.sql?rev=HEAD
DECLARE
	CURSOR cur IS 
		select ft.file_transfer_id, ft.file_transfer_name
		from device.file_transfer ft
		where ft.file_transfer_type_cd = 15
			and not exists (
				select 1 from device.config_template where config_template_name = ft.file_transfer_name
			) and file_transfer_name in ('KIOSK-DEFAULT-CFG', 'PUBLICPC-DEFAULT-CFG')
		order by ft.file_transfer_name;	
	
	ln_config_template_id CONFIG_TEMPLATE.CONFIG_TEMPLATE_ID%TYPE;
	ln_result_cd NUMBER;
    lv_error_message VARCHAR2(4000);
    ln_setting_count NUMBER;
BEGIN
	FOR rec_cur IN cur LOOP					
		INSERT INTO DEVICE.CONFIG_TEMPLATE(CONFIG_TEMPLATE_NAME, CONFIG_TEMPLATE_TYPE_ID, DEVICE_TYPE_ID)
		SELECT rec_cur.file_transfer_name, DECODE(rec_cur.file_transfer_name, 'KIOSK-DEFAULT-CFG', 1, 2), 11 FROM DUAL
		WHERE NOT EXISTS (SELECT 1 FROM DEVICE.CONFIG_TEMPLATE WHERE CONFIG_TEMPLATE_NAME = rec_cur.file_transfer_name);
		
		IF SQL%ROWCOUNT > 0 THEN		
			SELECT CONFIG_TEMPLATE_ID 
			INTO ln_config_template_id
			FROM DEVICE.CONFIG_TEMPLATE 
			WHERE CONFIG_TEMPLATE_NAME = rec_cur.file_transfer_name;			
		
			PKG_DEVICE_CONFIGURATION.sp_update_cfg_tmpl_settings(rec_cur.file_transfer_id, ln_result_cd, lv_error_message, ln_setting_count);
			
			COMMIT;
		END IF;
	END LOOP;
END;
/

DECLARE
	CURSOR cur IS 
		select d.device_id, d.device_name
		from device.vw_device_last_active d
		where d.device_type_id in (11, 13)
		order by d.device_id;
		
	ln_file_transfer_id FILE_TRANSFER.FILE_TRANSFER_ID%TYPE;
	ln_file_transfer_type_cd FILE_TRANSFER.FILE_TRANSFER_TYPE_CD%TYPE;
	ln_result_cd NUMBER;
    lv_error_message VARCHAR2(4000);
    ln_setting_count NUMBER;	
BEGIN
	FOR rec_cur IN cur LOOP
		BEGIN
			SELECT file_transfer_id, file_transfer_type_cd
			INTO ln_file_transfer_id, ln_file_transfer_type_cd
			FROM (
				SELECT /*+INDEX(ft INX_FILE_TRANSFER_TYPE_NAME)*/ file_transfer_id, file_transfer_type_cd
				FROM device.file_transfer ft 
				WHERE file_transfer_name = rec_cur.device_name || '-CFG'
				ORDER BY file_transfer_type_cd, created_ts
			) WHERE ROWNUM = 1;
			
			IF ln_file_transfer_type_cd != 1 THEN
				UPDATE DEVICE.FILE_TRANSFER
				SET FILE_TRANSFER_TYPE_CD = 1
				WHERE FILE_TRANSFER_ID = ln_file_transfer_id;
			END IF;

			PKG_DEVICE_CONFIGURATION.SP_UPDATE_DEVICE_SETTINGS(rec_cur.device_id, ln_file_transfer_id, ln_result_cd, lv_error_message, ln_setting_count);
			COMMIT;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				NULL;
			WHEN VALUE_ERROR THEN
				NULL;
		END;
	END LOOP;
END;
/

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/APP_LAYER/VW_DEVICE_INFO.vws?rev=1.31
CREATE OR REPLACE VIEW APP_LAYER.VW_DEVICE_INFO (DEVICE_NAME, DEVICE_SERIAL_CD, ENCRYPTION_KEY, PREVIOUS_ENCRYPTION_KEY, DEVICE_TYPE_ID, ACTIVATION_STATUS_CD, TIME_ZONE_CD, TIME_ZONE_GUID, MASTER_ID, REJECT_UNTIL_TS, REJECT_UNTIL_TS_TIMESTAMP, LOCALE, INIT_ONLY, ACTION_CD, PROPS_TO_REQUEST, ENCRYPTION_KEY_TIMESTAMP, PREV_ENCR_KEY_TIMESTAMP, ACT_STATUS_TIMESTAMP, TIME_ZONE_TIMESTAMP, MASTER_ID_TIMESTAMP, ACTION_CD_TIMESTAMP, PROPS_TO_REQUEST_TIMESTAMP, LAST_ACTIVITY_TS) AS 
 SELECT D.DEVICE_NAME,
        D.DEVICE_SERIAL_CD,
        UTL_RAW.CAST_TO_RAW(D.ENCRYPTION_KEY) ENCRYPTION_KEY,
        D.PREVIOUS_ENCRYPTION_KEY,
        D.DEVICE_TYPE_ID,
        DECODE(D.DEVICE_TYPE_ID, 13, NVL(TO_NUMBER_OR_NULL(DS_A.DEVICE_SETTING_VALUE), 0), 3) ACTIVATION_STATUS_CD,
        NVL(L.LOCATION_TIME_ZONE_CD, 'EST') TIME_ZONE_CD,
        NVL(tz.TIME_ZONE_GUID, 'US/Eastern') TIME_ZONE_GUID,
        NVL(TO_NUMBER_OR_NULL(DS_MT.DEVICE_SETTING_VALUE), 0) MASTER_ID,
        NVL(DATE_TO_MILLIS(D.REJECT_UNTIL_TS), 0) REJECT_UNTIL_TS,
		NVL(DATE_TO_MILLIS(D.REJECT_UNTIL_UPDATED_TS), 0) REJECT_UNTIL_TS_TIMESTAMP,
        'en_US' LOCALE,
        'N' INIT_ONLY,
        DECODE(NVL(TO_NUMBER_OR_NULL(DS_A.DEVICE_SETTING_VALUE), 0), 2, 2, 3, DECODE(D.CMD_PENDING_CNT, 0, DECODE(D.CALL_IN_TIME_NORMALIZED_TS, NULL, 1, 0), 1), 0) ACTION_CD,
        NVL(REGEXP_REPLACE(NVL(DECODE(DBADMIN.PKG_UTL.COMPARE(SYSDATE - 1, NVL(MIN(DS_300.LAST_UPDATED_TS), MIN_DATE)), 1, DECODE(MAX(H.HOST_TYPE_ID), 202, '300|301|', 204, '300|301|')), '') || DS_P.DEVICE_SETTING_VALUE, '[|]$', ''), '99999') PROPS_TO_REQUEST,
        NVL(DATE_TO_MILLIS(D.DEVICE_ENCR_KEY_GEN_TS), 0) ENCRYPTION_KEY_TIMESTAMP,
        NVL(DATE_TO_MILLIS(D.DEVICE_ENCR_KEY_GEN_TS), 0) PREV_ENCR_KEY_TIMESTAMP,
        NVL(DATE_TO_MILLIS(DS_A.LAST_UPDATED_TS), 0) ACT_STATUS_TIMESTAMP,
        GREATEST(NVL(DATE_TO_MILLIS(P.LAST_UPDATED_TS), 0), NVL(DATE_TO_MILLIS(L.LAST_UPDATED_TS), 0)) TIME_ZONE_TIMESTAMP,
        NVL(DATE_TO_MILLIS(DS_MT.LAST_UPDATED_TS), 0) MASTER_ID_TIMESTAMP,
        DECODE(TO_NUMBER_OR_NULL(DS_A.DEVICE_SETTING_VALUE), 3, GREATEST(NVL(DATE_TO_MILLIS(DS_A.LAST_UPDATED_TS), 0), NVL(DATE_TO_MILLIS(D.CMD_PENDING_UPDATED_TS), 0)), NVL(DATE_TO_MILLIS(DS_A.LAST_UPDATED_TS), 0)) ACTION_CD_TIMESTAMP,
        GREATEST(DECODE(MAX(DS_300.LAST_UPDATED_TS), NULL, 0, DATE_TO_MILLIS(MAX(DS_300.LAST_UPDATED_TS)) + 24 * 60 * 60 * 1000 * DECODE(DBADMIN.PKG_UTL.COMPARE(SYSDATE - 1, MAX(DS_300.LAST_UPDATED_TS)), 1, 1, 0)), NVL(DATE_TO_MILLIS(MAX(H.CREATED_TS)), NVL(DATE_TO_MILLIS(D.CREATED_TS), 0)), NVL(DATE_TO_MILLIS(DS_P.LAST_UPDATED_TS), 0)) PROPS_TO_REQUEST_TIMESTAMP,
		D.LAST_ACTIVITY_TS
   FROM DEVICE.DEVICE D
   LEFT OUTER JOIN DEVICE.HOST H ON D.DEVICE_ID = H.DEVICE_ID AND H.HOST_TYPE_ID IN(202,204)
   LEFT OUTER JOIN PSS.POS P ON D.DEVICE_ID = P.DEVICE_ID
   LEFT OUTER JOIN LOCATION.LOCATION L ON P.LOCATION_ID = L.LOCATION_ID
   LEFT OUTER JOIN LOCATION.TIME_ZONE TZ ON L.LOCATION_TIME_ZONE_CD = TZ.TIME_ZONE_CD
   LEFT OUTER JOIN DEVICE.DEVICE_SETTING DS_A 
     ON D.DEVICE_ID = DS_A.DEVICE_ID
    AND DS_A.DEVICE_SETTING_PARAMETER_CD = '70'
    AND REGEXP_LIKE (DS_A.DEVICE_SETTING_VALUE, '^[0-9]+$')
   LEFT OUTER JOIN DEVICE.DEVICE_SETTING DS_MT
     ON D.DEVICE_ID = DS_MT.DEVICE_ID
    AND DS_MT.DEVICE_SETTING_PARAMETER_CD = '60'
    AND REGEXP_LIKE (DS_MT.DEVICE_SETTING_VALUE, '^[0-9]+$')
   LEFT OUTER JOIN DEVICE.DEVICE_SETTING DS_P
     ON D.DEVICE_ID = DS_P.DEVICE_ID
    AND DS_P.DEVICE_SETTING_PARAMETER_CD = 'PROPS_TO_REQUEST'
    AND REGEXP_LIKE (DS_P.DEVICE_SETTING_VALUE, '^([0-9]+)([|-][0-9]+)*$')
   LEFT OUTER JOIN DEVICE.DEVICE_SETTING DS_300 
     ON D.DEVICE_ID = DS_300.DEVICE_ID
    AND DS_300.DEVICE_SETTING_PARAMETER_CD = '300'
    AND REGEXP_LIKE (DS_300.DEVICE_SETTING_VALUE, '^[0-9]+$')   
  WHERE D.DEVICE_ACTIVE_YN_FLAG = 'Y'
  GROUP BY 
        D.DEVICE_NAME,
        D.DEVICE_SERIAL_CD,
        D.ENCRYPTION_KEY,
        D.PREVIOUS_ENCRYPTION_KEY,
        D.DEVICE_TYPE_ID,
        DS_A.DEVICE_SETTING_VALUE,
        L.LOCATION_TIME_ZONE_CD,
        tz.TIME_ZONE_GUID,
        DS_MT.DEVICE_SETTING_VALUE,
        DS_P.DEVICE_SETTING_VALUE,
        D.DEVICE_ENCR_KEY_GEN_TS,
        DS_A.LAST_UPDATED_TS,
        P.LAST_UPDATED_TS,
        L.LAST_UPDATED_TS,
        DS_MT.LAST_UPDATED_TS,
        D.CREATED_TS,
        DS_P.LAST_UPDATED_TS,
		D.CMD_PENDING_CNT,
		D.CALL_IN_TIME_NORMALIZED_TS,
		D.CMD_PENDING_UPDATED_TS,
		D.REJECT_UNTIL_TS,
		D.REJECT_UNTIL_UPDATED_TS,
		D.LAST_ACTIVITY_TS
  UNION ALL
 SELECT DEVICE_DEFAULT_NAME DEVICE_NAME,
	    DEVICE_DEFAULT_NAME DEVICE_SERIAL_CD,
	    ENCRYPTION_KEY,
	    NULL PREVIOUS_ENCRYPTION_KEY,
	    -1 DEVICE_TYPE_ID,
	    0 ACTIVATION_STATUS_CD,
	    'EST' TIME_ZONE_CD,
	    'US/Eastern' TIME_ZONE_GUID,
	    0 MASTER_ID,
	    0 REJECT_UNTIL_TS,
		0 REJECT_UNTIL_TS_TIMESTAMP,
	    'en_US' LOCALE,
	    'Y' INIT_ONLY,
	    0 ACTION_CD,
	    NULL,
	    0,
	    0,
	    0,
	    0,
	    0,
	    0,
	    0,
		SYSDATE LAST_ACTIVITY_TS
   FROM DEVICE.DEVICE_DEFAULT
  WHERE DEVICE_DEFAULT_ACTIVE_YN_FLAG = 'Y'
/

-- Resource: http://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/USADBP01/DBADMIN/DATE_TO_MILLIS.fnc?rev=1.8
CREATE OR REPLACE FUNCTION DBADMIN.DATE_TO_MILLIS (PD_DATE DATE)
   RETURN NUMBER DETERMINISTIC PARALLEL_ENABLE
IS
    li_diff INTERVAL DAY(9) TO SECOND(0);
    lt_epoch TIMESTAMP WITH TIME ZONE := TIMESTAMP '1970-01-01 0:00:00 +0:00';
    INVALID_DATE EXCEPTION;   
    PRAGMA EXCEPTION_INIT(INVALID_DATE, -1878);
BEGIN
    IF pd_date IS NULL THEN
        RETURN NULL;
    ELSE
        li_diff := TO_TIMESTAMP_TZ(TO_CHAR(pd_date, 'YYYY-MM-DD HH24:MI:SS') || ' ' || PKG_CONST.DB_TIME_ZONE, 'YYYY-MM-DD HH24:MI:SS TZR') - lt_epoch;
        RETURN EXTRACT(DAY FROM li_diff) * 86400000 
             + EXTRACT(HOUR FROM li_diff) * 3600000
             + EXTRACT(MINUTE FROM li_diff) * 60000
             + EXTRACT(SECOND FROM li_diff)  * 1000;
    END IF;
EXCEPTION
    WHEN INVALID_DATE THEN
        RETURN DBADMIN.DATE_TO_MILLIS(pd_date + (1/24));
END;  
/
-- Resource: https://cvs.usatech.com/viewcvs/viewcvs.cgi/NetworkServices/DatabaseScripts/releases/REL_Edge_Update_R30/bin_range_refresh_automation.sql?rev=HEAD
insert into ENGINE.APP_SETTING (app_setting_cd,app_setting_value, app_setting_desc)
values('IND_DB_FILE_MOD_TIME','1329512520000','The modification time string for ind_db_ardef.txt');

insert into ENGINE.APP_SETTING (app_setting_cd,app_setting_value, app_setting_desc)
values('IND_DB_FILE_MOD_TIME_PREAPPROVE','1329512520000','The modification time string for ind_db_ardef.txt that needs manual approval.');

insert into ENGINE.APP_SETTING (app_setting_cd,app_setting_value, app_setting_desc)
values('IND_DB_FILE_LAST_UPLOAD_DIFF_PERCENTAGE','0','The last uploaded ind_db_ardef.txt percentage difference from the previous.');
commit;

GRANT SELECT, UPDATE ON ENGINE.APP_SETTING TO USAT_POSM_ROLE;
