/**
* DMS_BADCKOUT_DROP_ALL_OBJECTS
*
* This script will run through all the commands below and drop/remove all changes from the DMS alter/modification scripts.
*
* NOTE: Sometimes during testinf an error would get raised saying the table does not exist (DMS_USER1.TMP_drop_DMS_object_list),
* after this script tries to create DMS_USER1.TMP_drop_DMS_object_list table, once it attempts to go thru the FOR LOOP.  
* Just comment out the FOR LOOP code below, re-run the script, and then un-comment out 
* For Loop code and re-run again.
*
*
*/
SET SERVEROUTPUT ON
declare
the_command VARCHAR2(200);
err_num NUMBER;
  err_msg VARCHAR2(200);
begin

  begin execute immediate 'delete from DEVICE.CONFIG_TEMPLATE_SETTING WHERE DEVICE_TYPE_ID is not null'; exception when others then null; end;
  begin execute immediate 'drop table DMS_USER1.TMP_drop_DMS_object_list'; exception when others then null; end;
  commit;
  begin 
    execute immediate 
  'CREATE TABLE DMS_USER1.TMP_drop_DMS_object_list ' ||
  ' as ' ||
  ' select ''drop trigger DEVICE.TRBU_CONFIG_TEMPLATE_CATEGORY'' as command from dual union all ' ||
  'select ''drop trigger DEVICE.TRBI_CONFIG_TEMPLATE_CATEGORY'' as command from dual union all ' ||
  'select ''ALTER TABLE device.CONFIG_TEMPLATE_SETTING drop CONSTRAINT FK_CFG_TMPL_SET_CAT_ID'' as command from dual union all ' ||
  'select ''drop table device.CONFIG_TEMPLATE_CATEGORY'' as command from dual union all ' ||
  'select ''alter table DEVICE.CONFIG_TEMPLATE_SETTING set unused column CATEGORY_ID'' as command from dual union all ' ||
  'select ''alter table DEVICE.CONFIG_TEMPLATE_SETTING set unused column DISPLAY'' as command from dual union all ' ||
  'select ''alter table DEVICE.CONFIG_TEMPLATE_SETTING set unused column DESCRIPTION'' as command from dual union all ' ||
  'select ''alter table DEVICE.CONFIG_TEMPLATE_SETTING set unused column EDITOR'' as command from dual union all ' ||
  'select ''alter table DEVICE.CONFIG_TEMPLATE_SETTING set unused column CONVERTER'' as command from dual union all ' ||
  'select ''alter table DEVICE.CONFIG_TEMPLATE_SETTING set unused column CLIENT_SEND'' as command from dual union all ' ||
  'select ''alter table DEVICE.CONFIG_TEMPLATE_SETTING set unused column ALIGN'' as command from dual union all ' ||
  'select ''alter table DEVICE.CONFIG_TEMPLATE_SETTING set unused column FIELD_SIZE'' as command from dual union all ' ||
  'select ''alter table DEVICE.CONFIG_TEMPLATE_SETTING set unused column FIELD_OFFSET'' as command from dual union all ' ||
  'select ''alter table DEVICE.CONFIG_TEMPLATE_SETTING set unused column PAD_CHAR'' as command from dual union all ' ||
  'select ''alter table DEVICE.CONFIG_TEMPLATE_SETTING set unused column PROPERTY_LIST_VERSION'' as command from dual union all ' ||
  'select ''alter table DEVICE.CONFIG_TEMPLATE_SETTING set unused column DEVICE_TYPE_ID'' as command from dual union all ' ||
  'select ''alter table DEVICE.CONFIG_TEMPLATE_SETTING set unused column DISPLAY_ORDER'' as command from dual union all ' ||
  'select ''alter table DEVICE.CONFIG_TEMPLATE_SETTING set unused column ALT_NAME'' as command from dual union all ' ||
  'select ''alter table DEVICE.CONFIG_TEMPLATE_SETTING set unused column DATA_MODE'' as command from dual union all ' ||
  'select ''alter table DEVICE.CONFIG_TEMPLATE_SETTING set unused column DATA_MODE_AUX'' as command from dual union all ' ||
  'select ''alter table DEVICE.CONFIG_TEMPLATE_SETTING set unused column REGEX_ID'' as command from dual union all ' ||
  'select ''alter table DEVICE.CONFIG_TEMPLATE_SETTING set unused column ACTIVE'' as command from dual union all ' ||
  'select ''alter table DEVICE.CONFIG_TEMPLATE_SETTING drop unused columns'' as command from dual union all ' ||
  'select ''ALTER TABLE device.CONFIG_TEMPLATE_SETTING drop CONSTRAINT FK_CFG_TMPL_DEV_TYPE_ID'' as command from dual union all ' ||
  'select ''drop INDEX DEVICE.IDX_CONF_TEMP_SET_DEV_TYPE_ID'' as command from dual union all ' ||
  'select ''drop trigger DEVICE.TRBI_CONFIG_TEMPLATE_SETTING'' as command from dual union all ' ||
  'select ''drop trigger DEVICE.TRBU_CONFIG_TEMPLATE_SETTING'' as command from dual union all ' ||
  'select ''drop sequence DEVICE.SEQ_CONFIG_TEMPLATE_REGEX_ID'' as command from dual union all ' ||
  'select ''drop trigger DEVICE.TRBU_CONFIG_TEMPLATE_REGEX'' as command from dual union all ' ||
  'select ''drop trigger DEVICE.TRBI_CONFIG_TEMPLATE_REGEX'' as command from dual union all ' || 
  'select ''drop table DEVICE.CONFIG_TEMPLATE_REGEX'' as command from dual union all ' || 
  'select ''drop trigger DEVICE.TRBU_CONFIG_TEMPLATE_CUST_SETT'' as command from dual union all ' || 
  'select ''drop trigger DEVICE.TRBI_CONFIG_TEMPLATE_CUST_SETT'' as command from dual union all ' || 
  'select ''drop sequence DEVICE.SEQ_CONFIG_TEMPLATE_CUS_SET_ID'' as command from dual union all ' ||
  'select ''ALTER TABLE DEVICE.CONFIG_TEMPLATE_CUSTOM_SETTING drop CONSTRAINT FK_CONFIG_TEMP_CUSTOM_ID'' as command from dual union all ' ||
  'select ''ALTER TABLE DEVICE.CONFIG_TEMPLATE_CUSTOM_SETTING drop FK_CUST_TMP_CNF_DEV_SET_PRM_CD'' as command from dual union all ' ||
  'select ''drop trigger DEVICE.TRBU_CONFIG_TEMPLATE_CUSTOM'' as command from dual union all ' || 
  'select ''drop trigger DEVICE.TRBI_CONFIG_TEMPLATE_CUSTOM'' as command from dual union all ' || 
  'select ''drop sequence DEVICE.SEQ_CONFIG_TEMPLATE_CUSTOM_ID'' as command from dual union all ' ||
  'select ''drop table DEVICE.CONFIG_TEMPLATE_CUSTOM_SETTING'' as command from dual union all ' ||
  'select ''drop table DEVICE.CONFIG_TEMPLATE_CUSTOM'' as command from dual union all ' ||
  'select ''drop trigger DEVICE.TRBI_CONFIG_TEMPLATE_CAT_DISP'' as command from dual union all ' || 
  'select ''drop trigger DEVICE.TRBU_CONFIG_TEMPLATE_CAT_DISP'' as command from dual union all ' || 
  ' select ''drop TABLE DEVICE.CONFIG_TEMPLATE_CATEGORY_DISP'' as command from dual '; 
  
  exception when others then 
      err_num := SQLCODE;
      err_msg := SUBSTR(SQLERRM, 1, 100);
      dbms_output.put_line('### ALERT! EXCEPTION ON TABLE CREATE WAS ERR_NUM: '||err_num ||', ERR_MSG: '||err_msg||'.');
  end;
  commit;
  for r in (select * from DMS_USER1.TMP_drop_DMS_object_list) LOOP
      the_command := r.command;
      dbms_output.put_line('command='||the_command); 
      begin execute immediate the_command; exception when others then null; end;
  end LOOP;  
  commit;
end;
/