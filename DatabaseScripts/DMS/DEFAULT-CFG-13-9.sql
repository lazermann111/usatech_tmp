DECLARE
	ln_property_list_version NUMBER := 9;
	ln_device_type_id device_type.device_type_id%TYPE := 13;
	ln_file_transfer_type_cd file_transfer_type.file_transfer_type_cd%TYPE := 22;
	lv_file_transfer_name file_transfer.file_transfer_name%TYPE := 'DEFAULT-CFG-' || ln_device_type_id || '-' || ln_property_list_version;
	ln_file_transfer_id file_transfer.file_transfer_id%TYPE;
    ln_result_cd NUMBER;
    lv_error_message VARCHAR2(255);
    ln_setting_count NUMBER;
	lv_db_name VARCHAR2(255);
BEGIN
	SELECT MAX(file_transfer_id) INTO ln_file_transfer_id
	FROM device.file_transfer
	WHERE file_transfer_name = lv_file_transfer_name;
	
	IF ln_file_transfer_id IS NULL THEN
		INSERT INTO DEVICE.FILE_TRANSFER(FILE_TRANSFER_TYPE_CD, FILE_TRANSFER_NAME)
		VALUES(ln_file_transfer_type_cd, lv_file_transfer_name);
		
		SELECT MAX(file_transfer_id) INTO ln_file_transfer_id
		FROM device.file_transfer
		WHERE file_transfer_name = lv_file_transfer_name;
	END IF;
	
	SELECT REPLACE(GLOBAL_NAME, '.WORLD', '') INTO lv_db_name FROM GLOBAL_NAME;
	
	UPDATE DEVICE.FILE_TRANSFER
	SET FILE_TRANSFER_TYPE_CD = ln_file_transfer_type_cd,
		FILE_TRANSFER_CONTENT = RAWTOHEX(
		'0=' || DECODE(lv_db_name, 'USADBP', 'a1.eport-usatech.net', 'USASTT01', 'a1.ecc.eport-usatech.net', 'USADEV02', 'a1.int.eport-usatech.net', 'a1.dev.eport-usatech.net') || PKG_CONST.ASCII__LF ||
		'1=' || DECODE(lv_db_name, 'USADBP', 'a2.eport-usatech.net', 'USASTT01', 'a2.ecc.eport-usatech.net', 'USADEV02', 'a2.int.eport-usatech.net', 'a2.dev.eport-usatech.net') || PKG_CONST.ASCII__LF ||
		'2=' || DECODE(lv_db_name, 'USADBP', '192.168.79.163', 'USASTT01', '192.168.79.61', 'USADEV02', '192.168.79.245', '192.168.79.253') || PKG_CONST.ASCII__LF ||
		'3=' || DECODE(lv_db_name, 'USADBP', '208.116.216.163', 'USASTT01', '192.168.79.61', 'USADEV02', '192.168.79.245', '192.168.79.253') || PKG_CONST.ASCII__LF ||
		'4=14109' || PKG_CONST.ASCII__LF ||
		'5=14109' || PKG_CONST.ASCII__LF ||
		'6=14109' || PKG_CONST.ASCII__LF ||
		'7=14109' || PKG_CONST.ASCII__LF ||
		'10=15' || PKG_CONST.ASCII__LF ||
		'20=' || DECODE(lv_db_name, 'USADBP', 'b1.eport-usatech.net', 'USASTT01', 'b1.ecc.eport-usatech.net', 'USADEV02', 'b1.int.eport-usatech.net', 'b1.dev.eport-usatech.net') || PKG_CONST.ASCII__LF ||
		'21=' || DECODE(lv_db_name, 'USADBP', 'b2.eport-usatech.net', 'USASTT01', 'b2.ecc.eport-usatech.net', 'USADEV02', 'b2.int.eport-usatech.net', 'b2.dev.eport-usatech.net') || PKG_CONST.ASCII__LF ||
		'22=' || DECODE(lv_db_name, 'USADBP', '192.168.79.163', 'USASTT01', '192.168.79.61', 'USADEV02', '192.168.79.245', '192.168.79.253') || PKG_CONST.ASCII__LF ||
		'23=' || DECODE(lv_db_name, 'USADBP', '208.116.216.163', 'USASTT01', '192.168.79.61', 'USADEV02', '192.168.79.245', '192.168.79.253') || PKG_CONST.ASCII__LF ||
		'24=14108' || PKG_CONST.ASCII__LF ||
		'25=14108' || PKG_CONST.ASCII__LF ||
		'26=14108' || PKG_CONST.ASCII__LF ||
		'27=14108' || PKG_CONST.ASCII__LF ||
		'30=20' || PKG_CONST.ASCII__LF ||
		'31=50' || PKG_CONST.ASCII__LF ||
		'32=2' || PKG_CONST.ASCII__LF ||
		'33=27' || PKG_CONST.ASCII__LF ||
		'70=0' || PKG_CONST.ASCII__LF ||
		'71=' || PKG_CONST.ASCII__LF ||
		'85=W^0200^0' || PKG_CONST.ASCII__LF ||
		'86=I^7200^21600' || PKG_CONST.ASCII__LF ||
		'87=D^0300' || PKG_CONST.ASCII__LF ||
		'215=20' || PKG_CONST.ASCII__LF ||
		'1001=10' || PKG_CONST.ASCII__LF ||
		'1002=100' || PKG_CONST.ASCII__LF ||
		'1003=1' || PKG_CONST.ASCII__LF ||
		'1004=0' || PKG_CONST.ASCII__LF ||
		'1005=10' || PKG_CONST.ASCII__LF ||
		'1006=4' || PKG_CONST.ASCII__LF ||
		'1007=20' || PKG_CONST.ASCII__LF ||
		'1008=3' || PKG_CONST.ASCII__LF ||
		'1009=2' || PKG_CONST.ASCII__LF ||
		'1010=12' || PKG_CONST.ASCII__LF ||
		'1011=8' || PKG_CONST.ASCII__LF ||
		'1012=16' || PKG_CONST.ASCII__LF ||
		'1013=0' || PKG_CONST.ASCII__LF ||
		'1014=0' || PKG_CONST.ASCII__LF ||
		'1015=0' || PKG_CONST.ASCII__LF ||
		'1016=0' || PKG_CONST.ASCII__LF ||
		'1017=0' || PKG_CONST.ASCII__LF ||
		'1018=20' || PKG_CONST.ASCII__LF ||
		'1019=15' || PKG_CONST.ASCII__LF ||
		'1020=5' || PKG_CONST.ASCII__LF ||
		'1021=300' || PKG_CONST.ASCII__LF ||
		'1022=0' || PKG_CONST.ASCII__LF ||
		'1023=1' || PKG_CONST.ASCII__LF ||
		'1024=0' || PKG_CONST.ASCII__LF ||
		'1025=360' || PKG_CONST.ASCII__LF ||
		'1101=' || PKG_CONST.ASCII__LF ||
		'1102=2' || PKG_CONST.ASCII__LF ||
		'1103=0' || PKG_CONST.ASCII__LF ||
		'1104=1' || PKG_CONST.ASCII__LF ||
		'1106=' || PKG_CONST.ASCII__LF ||
		'1200=1000' || PKG_CONST.ASCII__LF ||
		'1201=1' || PKG_CONST.ASCII__LF ||
		'1202=0' || PKG_CONST.ASCII__LF ||
		'1300=60' || PKG_CONST.ASCII__LF ||
		'1400=100' || PKG_CONST.ASCII__LF ||
		'1401=17' || PKG_CONST.ASCII__LF ||
		'1402=3' || PKG_CONST.ASCII__LF ||
		'1403=Hotel Business Express Center' || PKG_CONST.ASCII__LF ||
		'1404=' || PKG_CONST.ASCII__LF ||
		'1405=---Thank You---' || PKG_CONST.ASCII__LF ||
		'1406=Inquiries 1 888 561 4748' || PKG_CONST.ASCII__LF ||
		'1407=25' || PKG_CONST.ASCII__LF ||
		'1408=50' || PKG_CONST.ASCII__LF ||
		'1409=100' || PKG_CONST.ASCII__LF ||
		'1410=100' || PKG_CONST.ASCII__LF ||
		'1411=100' || PKG_CONST.ASCII__LF ||
		'1412=0' || PKG_CONST.ASCII__LF ||
		'1413=0' || PKG_CONST.ASCII__LF ||
		'1414=0' || PKG_CONST.ASCII__LF ||
		'1415=2' || PKG_CONST.ASCII__LF ||
		'1500=1' || PKG_CONST.ASCII__LF ||
		'1501=15' || PKG_CONST.ASCII__LF ||
		'1502=25' || PKG_CONST.ASCII__LF ||
		'1503=25' || PKG_CONST.ASCII__LF ||
		'1504=20' || PKG_CONST.ASCII__LF ||
		'1505=2' || PKG_CONST.ASCII__LF ||
		'1506=25' || PKG_CONST.ASCII__LF ||
		'1507=' || PKG_CONST.ASCII__LF ||
		'1508=0' || PKG_CONST.ASCII__LF ||
		'1509=' || PKG_CONST.ASCII__LF ||
		'1510=0' || PKG_CONST.ASCII__LF ||
		'1511=' || PKG_CONST.ASCII__LF ||
		'1512=0' || PKG_CONST.ASCII__LF ||
		'1513=' || PKG_CONST.ASCII__LF ||
		'1514=0' || PKG_CONST.ASCII__LF ||
		'1515=' || PKG_CONST.ASCII__LF ||
		'1516=0' || PKG_CONST.ASCII__LF ||
		'1517=' || PKG_CONST.ASCII__LF ||
		'1518=0' || PKG_CONST.ASCII__LF ||
		'1519=' || PKG_CONST.ASCII__LF ||
		'1520=0' || PKG_CONST.ASCII__LF ||
		'1521=' || PKG_CONST.ASCII__LF ||
		'1522=30' || PKG_CONST.ASCII__LF ||
		'1523=100' || PKG_CONST.ASCII__LF ||
		'1524=2' || PKG_CONST.ASCII__LF ||
		'1525=10' || PKG_CONST.ASCII__LF ||
		'1526=25' || PKG_CONST.ASCII__LF
	) WHERE FILE_TRANSFER_ID = ln_file_transfer_id;

	pkg_device_configuration.sp_update_cfg_tmpl_settings(ln_file_transfer_id, ln_result_cd, lv_error_message, ln_setting_count);
	
	DELETE FROM DEVICE.DEVICE_TYPE_PROPERTY_LIST
	WHERE device_type_id = ln_device_type_id
		AND property_list_version = ln_property_list_version;
	
	INSERT INTO DEVICE.DEVICE_TYPE_PROPERTY_LIST(device_type_id, property_list_version, all_property_list_ids, configurable_property_list_ids)
	VALUES(ln_device_type_id, ln_property_list_version, 
		'0-7|10|20-27|30-33|50-52|60-64|70|71|80|81|85-87|100-108|200-208|215|300|301|1001-1025|1101-1106|1200-1202|1300|1400-1415|1500-1526',
		'0-7|10|20-27|30-33|70|71|85-87|215|1001-1025|1101-1106|1200-1202|1300|1400-1415|1500-1526');
		
	COMMIT;
END;
/
